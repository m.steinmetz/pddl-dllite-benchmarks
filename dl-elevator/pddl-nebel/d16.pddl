(define (domain grounded-SIMPLE-ADL-ELEVATORS)
(:requirements
:strips
)
(:predicates
(Flag213-)
(Flag212-)
(Flag211-)
(Flag210-)
(Flag209-)
(Flag208-)
(Flag32-)
(NOT-LIFT_AT-I)
(LIFT_AT-J)
(CHECKCONSISTENCY-)
(LIFT_AT-H)
(ERROR-)
(Flag19-)
(Flag17-)
(Flag15-)
(Flag13-)
(Flag11-)
(Flag9-)
(Flag7-)
(Flag5-)
(Flag3-)
(Flag1-)
(BOARDED-PASS_L)
(SERVED-PASS_M)
(Flag31-)
(Flag29-)
(LIFT_AT-K)
(LIFT_AT-G)
(Flag20-)
(Flag18-)
(Flag16-)
(Flag14-)
(Flag12-)
(Flag10-)
(Flag8-)
(Flag6-)
(Flag4-)
(Flag2-)
(BOARDED-PASS_E)
(BOARDED-PASS_A)
(SERVED-PASS_B)
(BOARDED-PASS_B)
(BOARDED-PASS_M)
(Flag34-)
(Flag30-)
(LIFT_AT-O)
(LIFT_AT-P)
(LIFT_AT-F)
(SERVED-PASS_E)
(SERVED-PASS_A)
(Flag36-)
(Flag33-)
(Flag28-)
(LIFT_AT-D)
(SERVED-PASS_L)
(SERVED-PASS_N)
(Flag35-)
(LIFT_AT-C)
(Flag21-)
(Flag37-)
(BOARDED-PASS_N)
(NOT-BOARDED-PASS_N)
(NOT-CHECKCONSISTENCY-)
(NOT-LIFT_AT-D)
(NOT-LIFT_AT-C)
(NOT-LIFT_AT-F)
(NOT-SERVED-PASS_N)
(NOT-SERVED-PASS_L)
(NOT-LIFT_AT-G)
(NOT-SERVED-PASS_A)
(NOT-SERVED-PASS_E)
(NOT-LIFT_AT-O)
(NOT-LIFT_AT-K)
(NOT-LIFT_AT-H)
(NOT-LIFT_AT-P)
(NOT-BOARDED-PASS_M)
(NOT-SERVED-PASS_B)
(NOT-BOARDED-PASS_B)
(NOT-BOARDED-PASS_A)
(NOT-BOARDED-PASS_E)
(NOT-LIFT_AT-J)
(NOT-SERVED-PASS_M)
(NOT-BOARDED-PASS_L)
(NOT-ERROR-)
(LIFT_AT-I)
(Flag213prime-)
(Flag212prime-)
(Flag211prime-)
(Flag210prime-)
(Flag209prime-)
(Flag208prime-)
(Flag32prime-)
(Flag19prime-)
(Flag17prime-)
(Flag15prime-)
(Flag13prime-)
(Flag11prime-)
(Flag9prime-)
(Flag7prime-)
(Flag5prime-)
(Flag3prime-)
(Flag1prime-)
(Flag31prime-)
(Flag29prime-)
(Flag20prime-)
(Flag18prime-)
(Flag16prime-)
(Flag14prime-)
(Flag12prime-)
(Flag10prime-)
(Flag8prime-)
(Flag6prime-)
(Flag4prime-)
(Flag2prime-)
(Flag34prime-)
(Flag30prime-)
(Flag36prime-)
(Flag33prime-)
(Flag28prime-)
(Flag35prime-)
(Flag21prime-)
(Flag37prime-)
)
(:action STOP-PASS_N-C
:parameters ()
:precondition
(and
(Flag37-)
(Flag37prime-)
(Flag213prime-)
)
:effect
(and
(when
(and
(Flag213-)
)
(and
(BOARDED-PASS_N)
(not (NOT-BOARDED-PASS_N))
)
)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag213prime-))
(not (Flag212prime-))
(not (Flag211prime-))
(not (Flag210prime-))
(not (Flag209prime-))
(not (Flag208prime-))
(not (Flag32prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag34prime-))
(not (Flag30prime-))
(not (Flag36prime-))
(not (Flag33prime-))
(not (Flag28prime-))
(not (Flag35prime-))
(not (Flag21prime-))
(not (Flag37prime-))
)
)
(:action MOVEUP-C-D
:parameters ()
:precondition
(and
(LIFT_AT-C)
(NOT-ERROR-)
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(NOT-LIFT_AT-C)
(LIFT_AT-D)
(CHECKCONSISTENCY-)
(not (LIFT_AT-C))
(not (NOT-LIFT_AT-D))
(not (NOT-CHECKCONSISTENCY-))
(not (Flag213prime-))
(not (Flag212prime-))
(not (Flag211prime-))
(not (Flag210prime-))
(not (Flag209prime-))
(not (Flag208prime-))
(not (Flag32prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag34prime-))
(not (Flag30prime-))
(not (Flag36prime-))
(not (Flag33prime-))
(not (Flag28prime-))
(not (Flag35prime-))
(not (Flag21prime-))
(not (Flag37prime-))
)
)
(:action Flag37Action
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-C)
)
:effect
(and
(Flag37-)
(Flag37prime-)
)

)
(:action Prim37Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-C))
)
:effect
(and
(Flag37prime-)
(not (Flag37-))
)

)
(:action Prim54Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-C))
)
:effect
(and
)

)
(:action Prim71Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-C))
)
:effect
(and
)

)
(:action Prim88Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-C))
)
:effect
(and
)

)
(:action Prim105Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-C))
)
:effect
(and
)

)
(:action Prim122Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-C))
)
:effect
(and
)

)
(:action Prim139Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-C))
)
:effect
(and
)

)
(:action Prim156Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-C))
)
:effect
(and
)

)
(:action Prim173Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-C))
)
:effect
(and
)

)
(:action Prim190Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-C))
)
:effect
(and
)

)
(:action Prim207Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-C))
)
:effect
(and
)

)
(:action STOP-PASS_N-D
:parameters ()
:precondition
(and
(Flag35-)
(Flag35prime-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag213prime-))
(not (Flag212prime-))
(not (Flag211prime-))
(not (Flag210prime-))
(not (Flag209prime-))
(not (Flag208prime-))
(not (Flag32prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag34prime-))
(not (Flag30prime-))
(not (Flag36prime-))
(not (Flag33prime-))
(not (Flag28prime-))
(not (Flag35prime-))
(not (Flag21prime-))
(not (Flag37prime-))
)
)
(:action STOP-PASS_M-C
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-C)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag213prime-))
(not (Flag212prime-))
(not (Flag211prime-))
(not (Flag210prime-))
(not (Flag209prime-))
(not (Flag208prime-))
(not (Flag32prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag34prime-))
(not (Flag30prime-))
(not (Flag36prime-))
(not (Flag33prime-))
(not (Flag28prime-))
(not (Flag35prime-))
(not (Flag21prime-))
(not (Flag37prime-))
)
)
(:action STOP-PASS_L-C
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-C)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag213prime-))
(not (Flag212prime-))
(not (Flag211prime-))
(not (Flag210prime-))
(not (Flag209prime-))
(not (Flag208prime-))
(not (Flag32prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag34prime-))
(not (Flag30prime-))
(not (Flag36prime-))
(not (Flag33prime-))
(not (Flag28prime-))
(not (Flag35prime-))
(not (Flag21prime-))
(not (Flag37prime-))
)
)
(:action STOP-PASS_B-C
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-C)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag213prime-))
(not (Flag212prime-))
(not (Flag211prime-))
(not (Flag210prime-))
(not (Flag209prime-))
(not (Flag208prime-))
(not (Flag32prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag34prime-))
(not (Flag30prime-))
(not (Flag36prime-))
(not (Flag33prime-))
(not (Flag28prime-))
(not (Flag35prime-))
(not (Flag21prime-))
(not (Flag37prime-))
)
)
(:action STOP-PASS_A-C
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-C)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag213prime-))
(not (Flag212prime-))
(not (Flag211prime-))
(not (Flag210prime-))
(not (Flag209prime-))
(not (Flag208prime-))
(not (Flag32prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag34prime-))
(not (Flag30prime-))
(not (Flag36prime-))
(not (Flag33prime-))
(not (Flag28prime-))
(not (Flag35prime-))
(not (Flag21prime-))
(not (Flag37prime-))
)
)
(:action STOP-PASS_E-C
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-C)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag213prime-))
(not (Flag212prime-))
(not (Flag211prime-))
(not (Flag210prime-))
(not (Flag209prime-))
(not (Flag208prime-))
(not (Flag32prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag34prime-))
(not (Flag30prime-))
(not (Flag36prime-))
(not (Flag33prime-))
(not (Flag28prime-))
(not (Flag35prime-))
(not (Flag21prime-))
(not (Flag37prime-))
)
)
(:action Flag21Action
:parameters ()
:precondition
(and
(Flag2-)
(Flag2prime-)
(Flag4-)
(Flag4prime-)
(Flag6-)
(Flag6prime-)
(Flag8-)
(Flag8prime-)
(Flag10-)
(Flag10prime-)
(Flag12-)
(Flag12prime-)
(Flag14-)
(Flag14prime-)
(Flag16-)
(Flag16prime-)
(Flag18-)
(Flag18prime-)
(Flag20-)
(Flag20prime-)
(SERVED-PASS_E)
(SERVED-PASS_A)
(SERVED-PASS_B)
(SERVED-PASS_L)
(SERVED-PASS_M)
(SERVED-PASS_N)
(NOT-ERROR-)
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(Flag21-)
(Flag21prime-)
)

)
(:action Prim21Action-13
:parameters ()
:precondition
(and
(not (SERVED-PASS_L))
)
:effect
(and
(Flag21prime-)
(not (Flag21-))
)

)
(:action Prim21Action-15
:parameters ()
:precondition
(and
(not (SERVED-PASS_N))
)
:effect
(and
(Flag21prime-)
(not (Flag21-))
)

)
(:action MOVEDOWN-D-C
:parameters ()
:precondition
(and
(LIFT_AT-D)
(NOT-ERROR-)
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(NOT-LIFT_AT-D)
(LIFT_AT-C)
(CHECKCONSISTENCY-)
(not (LIFT_AT-D))
(not (NOT-LIFT_AT-C))
(not (NOT-CHECKCONSISTENCY-))
(not (Flag213prime-))
(not (Flag212prime-))
(not (Flag211prime-))
(not (Flag210prime-))
(not (Flag209prime-))
(not (Flag208prime-))
(not (Flag32prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag34prime-))
(not (Flag30prime-))
(not (Flag36prime-))
(not (Flag33prime-))
(not (Flag28prime-))
(not (Flag35prime-))
(not (Flag21prime-))
(not (Flag37prime-))
)
)
(:action MOVEUP-D-F
:parameters ()
:precondition
(and
(LIFT_AT-D)
(NOT-ERROR-)
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(NOT-LIFT_AT-D)
(LIFT_AT-F)
(CHECKCONSISTENCY-)
(not (LIFT_AT-D))
(not (NOT-LIFT_AT-F))
(not (NOT-CHECKCONSISTENCY-))
(not (Flag213prime-))
(not (Flag212prime-))
(not (Flag211prime-))
(not (Flag210prime-))
(not (Flag209prime-))
(not (Flag208prime-))
(not (Flag32prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag34prime-))
(not (Flag30prime-))
(not (Flag36prime-))
(not (Flag33prime-))
(not (Flag28prime-))
(not (Flag35prime-))
(not (Flag21prime-))
(not (Flag37prime-))
)
)
(:action Flag35Action
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-D)
)
:effect
(and
(Flag35-)
(Flag35prime-)
)

)
(:action Prim35Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-D))
)
:effect
(and
(Flag35prime-)
(not (Flag35-))
)

)
(:action Prim52Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-D))
)
:effect
(and
)

)
(:action Prim69Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-D))
)
:effect
(and
)

)
(:action Prim86Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-D))
)
:effect
(and
)

)
(:action Prim103Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-D))
)
:effect
(and
)

)
(:action Prim120Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-D))
)
:effect
(and
)

)
(:action Prim137Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-D))
)
:effect
(and
)

)
(:action Prim154Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-D))
)
:effect
(and
)

)
(:action Prim171Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-D))
)
:effect
(and
)

)
(:action Prim188Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-D))
)
:effect
(and
)

)
(:action Prim205Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-D))
)
:effect
(and
)

)
(:action STOP-PASS_N-O
:parameters ()
:precondition
(and
(Flag28-)
(Flag28prime-)
)
:effect
(and
(when
(and
(BOARDED-PASS_N)
)
(and
(SERVED-PASS_N)
(NOT-BOARDED-PASS_N)
(not (NOT-SERVED-PASS_N))
(not (BOARDED-PASS_N))
)
)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag213prime-))
(not (Flag212prime-))
(not (Flag211prime-))
(not (Flag210prime-))
(not (Flag209prime-))
(not (Flag208prime-))
(not (Flag32prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag34prime-))
(not (Flag30prime-))
(not (Flag36prime-))
(not (Flag33prime-))
(not (Flag28prime-))
(not (Flag35prime-))
(not (Flag21prime-))
(not (Flag37prime-))
)
)
(:action STOP-PASS_N-F
:parameters ()
:precondition
(and
(Flag33-)
(Flag33prime-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag213prime-))
(not (Flag212prime-))
(not (Flag211prime-))
(not (Flag210prime-))
(not (Flag209prime-))
(not (Flag208prime-))
(not (Flag32prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag34prime-))
(not (Flag30prime-))
(not (Flag36prime-))
(not (Flag33prime-))
(not (Flag28prime-))
(not (Flag35prime-))
(not (Flag21prime-))
(not (Flag37prime-))
)
)
(:action STOP-PASS_N-P
:parameters ()
:precondition
(and
(Flag36-)
(Flag36prime-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag213prime-))
(not (Flag212prime-))
(not (Flag211prime-))
(not (Flag210prime-))
(not (Flag209prime-))
(not (Flag208prime-))
(not (Flag32prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag34prime-))
(not (Flag30prime-))
(not (Flag36prime-))
(not (Flag33prime-))
(not (Flag28prime-))
(not (Flag35prime-))
(not (Flag21prime-))
(not (Flag37prime-))
)
)
(:action STOP-PASS_M-D
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-D)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag213prime-))
(not (Flag212prime-))
(not (Flag211prime-))
(not (Flag210prime-))
(not (Flag209prime-))
(not (Flag208prime-))
(not (Flag32prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag34prime-))
(not (Flag30prime-))
(not (Flag36prime-))
(not (Flag33prime-))
(not (Flag28prime-))
(not (Flag35prime-))
(not (Flag21prime-))
(not (Flag37prime-))
)
)
(:action STOP-PASS_L-D
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-D)
)
:effect
(and
(when
(and
(BOARDED-PASS_L)
)
(and
(SERVED-PASS_L)
(NOT-BOARDED-PASS_L)
(not (NOT-SERVED-PASS_L))
(not (BOARDED-PASS_L))
)
)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag213prime-))
(not (Flag212prime-))
(not (Flag211prime-))
(not (Flag210prime-))
(not (Flag209prime-))
(not (Flag208prime-))
(not (Flag32prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag34prime-))
(not (Flag30prime-))
(not (Flag36prime-))
(not (Flag33prime-))
(not (Flag28prime-))
(not (Flag35prime-))
(not (Flag21prime-))
(not (Flag37prime-))
)
)
(:action STOP-PASS_B-D
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-D)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag213prime-))
(not (Flag212prime-))
(not (Flag211prime-))
(not (Flag210prime-))
(not (Flag209prime-))
(not (Flag208prime-))
(not (Flag32prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag34prime-))
(not (Flag30prime-))
(not (Flag36prime-))
(not (Flag33prime-))
(not (Flag28prime-))
(not (Flag35prime-))
(not (Flag21prime-))
(not (Flag37prime-))
)
)
(:action STOP-PASS_A-D
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-D)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag213prime-))
(not (Flag212prime-))
(not (Flag211prime-))
(not (Flag210prime-))
(not (Flag209prime-))
(not (Flag208prime-))
(not (Flag32prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag34prime-))
(not (Flag30prime-))
(not (Flag36prime-))
(not (Flag33prime-))
(not (Flag28prime-))
(not (Flag35prime-))
(not (Flag21prime-))
(not (Flag37prime-))
)
)
(:action STOP-PASS_E-D
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-D)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag213prime-))
(not (Flag212prime-))
(not (Flag211prime-))
(not (Flag210prime-))
(not (Flag209prime-))
(not (Flag208prime-))
(not (Flag32prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag34prime-))
(not (Flag30prime-))
(not (Flag36prime-))
(not (Flag33prime-))
(not (Flag28prime-))
(not (Flag35prime-))
(not (Flag21prime-))
(not (Flag37prime-))
)
)
(:action Prim21Action-10
:parameters ()
:precondition
(and
(not (SERVED-PASS_E))
)
:effect
(and
(Flag21prime-)
(not (Flag21-))
)

)
(:action Prim21Action-11
:parameters ()
:precondition
(and
(not (SERVED-PASS_A))
)
:effect
(and
(Flag21prime-)
(not (Flag21-))
)

)
(:action MOVEDOWN-F-D
:parameters ()
:precondition
(and
(LIFT_AT-F)
(NOT-ERROR-)
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(NOT-LIFT_AT-F)
(LIFT_AT-D)
(CHECKCONSISTENCY-)
(not (LIFT_AT-F))
(not (NOT-LIFT_AT-D))
(not (NOT-CHECKCONSISTENCY-))
(not (Flag213prime-))
(not (Flag212prime-))
(not (Flag211prime-))
(not (Flag210prime-))
(not (Flag209prime-))
(not (Flag208prime-))
(not (Flag32prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag34prime-))
(not (Flag30prime-))
(not (Flag36prime-))
(not (Flag33prime-))
(not (Flag28prime-))
(not (Flag35prime-))
(not (Flag21prime-))
(not (Flag37prime-))
)
)
(:action MOVEUP-F-G
:parameters ()
:precondition
(and
(LIFT_AT-F)
(NOT-ERROR-)
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(NOT-LIFT_AT-F)
(LIFT_AT-G)
(CHECKCONSISTENCY-)
(not (LIFT_AT-F))
(not (NOT-LIFT_AT-G))
(not (NOT-CHECKCONSISTENCY-))
(not (Flag213prime-))
(not (Flag212prime-))
(not (Flag211prime-))
(not (Flag210prime-))
(not (Flag209prime-))
(not (Flag208prime-))
(not (Flag32prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag34prime-))
(not (Flag30prime-))
(not (Flag36prime-))
(not (Flag33prime-))
(not (Flag28prime-))
(not (Flag35prime-))
(not (Flag21prime-))
(not (Flag37prime-))
)
)
(:action Flag28Action
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-O)
)
:effect
(and
(Flag28-)
(Flag28prime-)
)

)
(:action Prim28Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-O))
)
:effect
(and
(Flag28prime-)
(not (Flag28-))
)

)
(:action Flag33Action
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-F)
)
:effect
(and
(Flag33-)
(Flag33prime-)
)

)
(:action Prim33Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-F))
)
:effect
(and
(Flag33prime-)
(not (Flag33-))
)

)
(:action Flag36Action
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-P)
)
:effect
(and
(Flag36-)
(Flag36prime-)
)

)
(:action Prim36Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-P))
)
:effect
(and
(Flag36prime-)
(not (Flag36-))
)

)
(:action Prim45Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-O))
)
:effect
(and
)

)
(:action Prim50Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-F))
)
:effect
(and
)

)
(:action Prim53Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-P))
)
:effect
(and
)

)
(:action Prim62Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-O))
)
:effect
(and
)

)
(:action Prim67Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-F))
)
:effect
(and
)

)
(:action Prim70Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-P))
)
:effect
(and
)

)
(:action Prim79Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-O))
)
:effect
(and
)

)
(:action Prim84Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-F))
)
:effect
(and
)

)
(:action Prim87Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-P))
)
:effect
(and
)

)
(:action Prim96Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-O))
)
:effect
(and
)

)
(:action Prim101Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-F))
)
:effect
(and
)

)
(:action Prim104Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-P))
)
:effect
(and
)

)
(:action Prim113Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-O))
)
:effect
(and
)

)
(:action Prim118Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-F))
)
:effect
(and
)

)
(:action Prim121Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-P))
)
:effect
(and
)

)
(:action Prim130Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-O))
)
:effect
(and
)

)
(:action Prim135Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-F))
)
:effect
(and
)

)
(:action Prim138Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-P))
)
:effect
(and
)

)
(:action Prim147Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-O))
)
:effect
(and
)

)
(:action Prim152Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-F))
)
:effect
(and
)

)
(:action Prim155Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-P))
)
:effect
(and
)

)
(:action Prim164Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-O))
)
:effect
(and
)

)
(:action Prim169Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-F))
)
:effect
(and
)

)
(:action Prim172Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-P))
)
:effect
(and
)

)
(:action Prim181Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-O))
)
:effect
(and
)

)
(:action Prim186Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-F))
)
:effect
(and
)

)
(:action Prim189Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-P))
)
:effect
(and
)

)
(:action Prim198Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-O))
)
:effect
(and
)

)
(:action Prim203Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-F))
)
:effect
(and
)

)
(:action Prim206Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-P))
)
:effect
(and
)

)
(:action STOP-PASS_N-K
:parameters ()
:precondition
(and
(Flag30-)
(Flag30prime-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag213prime-))
(not (Flag212prime-))
(not (Flag211prime-))
(not (Flag210prime-))
(not (Flag209prime-))
(not (Flag208prime-))
(not (Flag32prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag34prime-))
(not (Flag30prime-))
(not (Flag36prime-))
(not (Flag33prime-))
(not (Flag28prime-))
(not (Flag35prime-))
(not (Flag21prime-))
(not (Flag37prime-))
)
)
(:action STOP-PASS_N-G
:parameters ()
:precondition
(and
(Flag34-)
(Flag34prime-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag213prime-))
(not (Flag212prime-))
(not (Flag211prime-))
(not (Flag210prime-))
(not (Flag209prime-))
(not (Flag208prime-))
(not (Flag32prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag34prime-))
(not (Flag30prime-))
(not (Flag36prime-))
(not (Flag33prime-))
(not (Flag28prime-))
(not (Flag35prime-))
(not (Flag21prime-))
(not (Flag37prime-))
)
)
(:action STOP-PASS_M-O
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-O)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag213prime-))
(not (Flag212prime-))
(not (Flag211prime-))
(not (Flag210prime-))
(not (Flag209prime-))
(not (Flag208prime-))
(not (Flag32prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag34prime-))
(not (Flag30prime-))
(not (Flag36prime-))
(not (Flag33prime-))
(not (Flag28prime-))
(not (Flag35prime-))
(not (Flag21prime-))
(not (Flag37prime-))
)
)
(:action STOP-PASS_M-F
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-F)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag213prime-))
(not (Flag212prime-))
(not (Flag211prime-))
(not (Flag210prime-))
(not (Flag209prime-))
(not (Flag208prime-))
(not (Flag32prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag34prime-))
(not (Flag30prime-))
(not (Flag36prime-))
(not (Flag33prime-))
(not (Flag28prime-))
(not (Flag35prime-))
(not (Flag21prime-))
(not (Flag37prime-))
)
)
(:action STOP-PASS_M-P
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-P)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag213prime-))
(not (Flag212prime-))
(not (Flag211prime-))
(not (Flag210prime-))
(not (Flag209prime-))
(not (Flag208prime-))
(not (Flag32prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag34prime-))
(not (Flag30prime-))
(not (Flag36prime-))
(not (Flag33prime-))
(not (Flag28prime-))
(not (Flag35prime-))
(not (Flag21prime-))
(not (Flag37prime-))
)
)
(:action STOP-PASS_L-O
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-O)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag213prime-))
(not (Flag212prime-))
(not (Flag211prime-))
(not (Flag210prime-))
(not (Flag209prime-))
(not (Flag208prime-))
(not (Flag32prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag34prime-))
(not (Flag30prime-))
(not (Flag36prime-))
(not (Flag33prime-))
(not (Flag28prime-))
(not (Flag35prime-))
(not (Flag21prime-))
(not (Flag37prime-))
)
)
(:action STOP-PASS_L-F
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-F)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag213prime-))
(not (Flag212prime-))
(not (Flag211prime-))
(not (Flag210prime-))
(not (Flag209prime-))
(not (Flag208prime-))
(not (Flag32prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag34prime-))
(not (Flag30prime-))
(not (Flag36prime-))
(not (Flag33prime-))
(not (Flag28prime-))
(not (Flag35prime-))
(not (Flag21prime-))
(not (Flag37prime-))
)
)
(:action STOP-PASS_L-P
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-P)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag213prime-))
(not (Flag212prime-))
(not (Flag211prime-))
(not (Flag210prime-))
(not (Flag209prime-))
(not (Flag208prime-))
(not (Flag32prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag34prime-))
(not (Flag30prime-))
(not (Flag36prime-))
(not (Flag33prime-))
(not (Flag28prime-))
(not (Flag35prime-))
(not (Flag21prime-))
(not (Flag37prime-))
)
)
(:action STOP-PASS_B-O
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-O)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag213prime-))
(not (Flag212prime-))
(not (Flag211prime-))
(not (Flag210prime-))
(not (Flag209prime-))
(not (Flag208prime-))
(not (Flag32prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag34prime-))
(not (Flag30prime-))
(not (Flag36prime-))
(not (Flag33prime-))
(not (Flag28prime-))
(not (Flag35prime-))
(not (Flag21prime-))
(not (Flag37prime-))
)
)
(:action STOP-PASS_B-F
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-F)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag213prime-))
(not (Flag212prime-))
(not (Flag211prime-))
(not (Flag210prime-))
(not (Flag209prime-))
(not (Flag208prime-))
(not (Flag32prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag34prime-))
(not (Flag30prime-))
(not (Flag36prime-))
(not (Flag33prime-))
(not (Flag28prime-))
(not (Flag35prime-))
(not (Flag21prime-))
(not (Flag37prime-))
)
)
(:action STOP-PASS_B-P
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-P)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag213prime-))
(not (Flag212prime-))
(not (Flag211prime-))
(not (Flag210prime-))
(not (Flag209prime-))
(not (Flag208prime-))
(not (Flag32prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag34prime-))
(not (Flag30prime-))
(not (Flag36prime-))
(not (Flag33prime-))
(not (Flag28prime-))
(not (Flag35prime-))
(not (Flag21prime-))
(not (Flag37prime-))
)
)
(:action STOP-PASS_A-O
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-O)
)
:effect
(and
(when
(and
(BOARDED-PASS_A)
)
(and
(SERVED-PASS_A)
(NOT-BOARDED-PASS_A)
(not (NOT-SERVED-PASS_A))
(not (BOARDED-PASS_A))
)
)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag213prime-))
(not (Flag212prime-))
(not (Flag211prime-))
(not (Flag210prime-))
(not (Flag209prime-))
(not (Flag208prime-))
(not (Flag32prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag34prime-))
(not (Flag30prime-))
(not (Flag36prime-))
(not (Flag33prime-))
(not (Flag28prime-))
(not (Flag35prime-))
(not (Flag21prime-))
(not (Flag37prime-))
)
)
(:action STOP-PASS_A-F
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-F)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag213prime-))
(not (Flag212prime-))
(not (Flag211prime-))
(not (Flag210prime-))
(not (Flag209prime-))
(not (Flag208prime-))
(not (Flag32prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag34prime-))
(not (Flag30prime-))
(not (Flag36prime-))
(not (Flag33prime-))
(not (Flag28prime-))
(not (Flag35prime-))
(not (Flag21prime-))
(not (Flag37prime-))
)
)
(:action STOP-PASS_A-P
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-P)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag213prime-))
(not (Flag212prime-))
(not (Flag211prime-))
(not (Flag210prime-))
(not (Flag209prime-))
(not (Flag208prime-))
(not (Flag32prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag34prime-))
(not (Flag30prime-))
(not (Flag36prime-))
(not (Flag33prime-))
(not (Flag28prime-))
(not (Flag35prime-))
(not (Flag21prime-))
(not (Flag37prime-))
)
)
(:action STOP-PASS_E-O
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-O)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag213prime-))
(not (Flag212prime-))
(not (Flag211prime-))
(not (Flag210prime-))
(not (Flag209prime-))
(not (Flag208prime-))
(not (Flag32prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag34prime-))
(not (Flag30prime-))
(not (Flag36prime-))
(not (Flag33prime-))
(not (Flag28prime-))
(not (Flag35prime-))
(not (Flag21prime-))
(not (Flag37prime-))
)
)
(:action STOP-PASS_E-F
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-F)
)
:effect
(and
(when
(and
(BOARDED-PASS_E)
)
(and
(SERVED-PASS_E)
(NOT-BOARDED-PASS_E)
(not (NOT-SERVED-PASS_E))
(not (BOARDED-PASS_E))
)
)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag213prime-))
(not (Flag212prime-))
(not (Flag211prime-))
(not (Flag210prime-))
(not (Flag209prime-))
(not (Flag208prime-))
(not (Flag32prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag34prime-))
(not (Flag30prime-))
(not (Flag36prime-))
(not (Flag33prime-))
(not (Flag28prime-))
(not (Flag35prime-))
(not (Flag21prime-))
(not (Flag37prime-))
)
)
(:action STOP-PASS_E-P
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-P)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag213prime-))
(not (Flag212prime-))
(not (Flag211prime-))
(not (Flag210prime-))
(not (Flag209prime-))
(not (Flag208prime-))
(not (Flag32prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag34prime-))
(not (Flag30prime-))
(not (Flag36prime-))
(not (Flag33prime-))
(not (Flag28prime-))
(not (Flag35prime-))
(not (Flag21prime-))
(not (Flag37prime-))
)
)
(:action Prim21Action-0
:parameters ()
:precondition
(and
(not (Flag2-))
(Flag2prime-)
)
:effect
(and
(Flag21prime-)
(not (Flag21-))
)

)
(:action Prim21Action-1
:parameters ()
:precondition
(and
(not (Flag4-))
(Flag4prime-)
)
:effect
(and
(Flag21prime-)
(not (Flag21-))
)

)
(:action Prim21Action-2
:parameters ()
:precondition
(and
(not (Flag6-))
(Flag6prime-)
)
:effect
(and
(Flag21prime-)
(not (Flag21-))
)

)
(:action Prim21Action-3
:parameters ()
:precondition
(and
(not (Flag8-))
(Flag8prime-)
)
:effect
(and
(Flag21prime-)
(not (Flag21-))
)

)
(:action Prim21Action-4
:parameters ()
:precondition
(and
(not (Flag10-))
(Flag10prime-)
)
:effect
(and
(Flag21prime-)
(not (Flag21-))
)

)
(:action Prim21Action-5
:parameters ()
:precondition
(and
(not (Flag12-))
(Flag12prime-)
)
:effect
(and
(Flag21prime-)
(not (Flag21-))
)

)
(:action Prim21Action-6
:parameters ()
:precondition
(and
(not (Flag14-))
(Flag14prime-)
)
:effect
(and
(Flag21prime-)
(not (Flag21-))
)

)
(:action Prim21Action-7
:parameters ()
:precondition
(and
(not (Flag16-))
(Flag16prime-)
)
:effect
(and
(Flag21prime-)
(not (Flag21-))
)

)
(:action Prim21Action-8
:parameters ()
:precondition
(and
(not (Flag18-))
(Flag18prime-)
)
:effect
(and
(Flag21prime-)
(not (Flag21-))
)

)
(:action Prim21Action-9
:parameters ()
:precondition
(and
(not (Flag20-))
(Flag20prime-)
)
:effect
(and
(Flag21prime-)
(not (Flag21-))
)

)
(:action Prim21Action-12
:parameters ()
:precondition
(and
(not (SERVED-PASS_B))
)
:effect
(and
(Flag21prime-)
(not (Flag21-))
)

)
(:action MOVEDOWN-G-F
:parameters ()
:precondition
(and
(LIFT_AT-G)
(NOT-ERROR-)
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(NOT-LIFT_AT-G)
(LIFT_AT-F)
(CHECKCONSISTENCY-)
(not (LIFT_AT-G))
(not (NOT-LIFT_AT-F))
(not (NOT-CHECKCONSISTENCY-))
(not (Flag213prime-))
(not (Flag212prime-))
(not (Flag211prime-))
(not (Flag210prime-))
(not (Flag209prime-))
(not (Flag208prime-))
(not (Flag32prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag34prime-))
(not (Flag30prime-))
(not (Flag36prime-))
(not (Flag33prime-))
(not (Flag28prime-))
(not (Flag35prime-))
(not (Flag21prime-))
(not (Flag37prime-))
)
)
(:action MOVEDOWN-P-O
:parameters ()
:precondition
(and
(LIFT_AT-P)
(NOT-ERROR-)
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(NOT-LIFT_AT-P)
(LIFT_AT-O)
(CHECKCONSISTENCY-)
(not (LIFT_AT-P))
(not (NOT-LIFT_AT-O))
(not (NOT-CHECKCONSISTENCY-))
(not (Flag213prime-))
(not (Flag212prime-))
(not (Flag211prime-))
(not (Flag210prime-))
(not (Flag209prime-))
(not (Flag208prime-))
(not (Flag32prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag34prime-))
(not (Flag30prime-))
(not (Flag36prime-))
(not (Flag33prime-))
(not (Flag28prime-))
(not (Flag35prime-))
(not (Flag21prime-))
(not (Flag37prime-))
)
)
(:action MOVEDOWN-O-K
:parameters ()
:precondition
(and
(LIFT_AT-O)
(NOT-ERROR-)
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(NOT-LIFT_AT-O)
(LIFT_AT-K)
(CHECKCONSISTENCY-)
(not (LIFT_AT-O))
(not (NOT-LIFT_AT-K))
(not (NOT-CHECKCONSISTENCY-))
(not (Flag213prime-))
(not (Flag212prime-))
(not (Flag211prime-))
(not (Flag210prime-))
(not (Flag209prime-))
(not (Flag208prime-))
(not (Flag32prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag34prime-))
(not (Flag30prime-))
(not (Flag36prime-))
(not (Flag33prime-))
(not (Flag28prime-))
(not (Flag35prime-))
(not (Flag21prime-))
(not (Flag37prime-))
)
)
(:action MOVEUP-G-H
:parameters ()
:precondition
(and
(LIFT_AT-G)
(NOT-ERROR-)
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(NOT-LIFT_AT-G)
(LIFT_AT-H)
(CHECKCONSISTENCY-)
(not (LIFT_AT-G))
(not (NOT-LIFT_AT-H))
(not (NOT-CHECKCONSISTENCY-))
(not (Flag213prime-))
(not (Flag212prime-))
(not (Flag211prime-))
(not (Flag210prime-))
(not (Flag209prime-))
(not (Flag208prime-))
(not (Flag32prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag34prime-))
(not (Flag30prime-))
(not (Flag36prime-))
(not (Flag33prime-))
(not (Flag28prime-))
(not (Flag35prime-))
(not (Flag21prime-))
(not (Flag37prime-))
)
)
(:action MOVEUP-O-P
:parameters ()
:precondition
(and
(LIFT_AT-O)
(NOT-ERROR-)
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(NOT-LIFT_AT-O)
(LIFT_AT-P)
(CHECKCONSISTENCY-)
(not (LIFT_AT-O))
(not (NOT-LIFT_AT-P))
(not (NOT-CHECKCONSISTENCY-))
(not (Flag213prime-))
(not (Flag212prime-))
(not (Flag211prime-))
(not (Flag210prime-))
(not (Flag209prime-))
(not (Flag208prime-))
(not (Flag32prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag34prime-))
(not (Flag30prime-))
(not (Flag36prime-))
(not (Flag33prime-))
(not (Flag28prime-))
(not (Flag35prime-))
(not (Flag21prime-))
(not (Flag37prime-))
)
)
(:action MOVEUP-K-O
:parameters ()
:precondition
(and
(LIFT_AT-K)
(NOT-ERROR-)
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(NOT-LIFT_AT-K)
(LIFT_AT-O)
(CHECKCONSISTENCY-)
(not (LIFT_AT-K))
(not (NOT-LIFT_AT-O))
(not (NOT-CHECKCONSISTENCY-))
(not (Flag213prime-))
(not (Flag212prime-))
(not (Flag211prime-))
(not (Flag210prime-))
(not (Flag209prime-))
(not (Flag208prime-))
(not (Flag32prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag34prime-))
(not (Flag30prime-))
(not (Flag36prime-))
(not (Flag33prime-))
(not (Flag28prime-))
(not (Flag35prime-))
(not (Flag21prime-))
(not (Flag37prime-))
)
)
(:action Flag30Action
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-K)
)
:effect
(and
(Flag30-)
(Flag30prime-)
)

)
(:action Prim30Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-K))
)
:effect
(and
(Flag30prime-)
(not (Flag30-))
)

)
(:action Flag34Action
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-G)
)
:effect
(and
(Flag34-)
(Flag34prime-)
)

)
(:action Prim34Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-G))
)
:effect
(and
(Flag34prime-)
(not (Flag34-))
)

)
(:action Prim47Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-K))
)
:effect
(and
)

)
(:action Prim51Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-G))
)
:effect
(and
)

)
(:action Prim64Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-K))
)
:effect
(and
)

)
(:action Prim68Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-G))
)
:effect
(and
)

)
(:action Prim81Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-K))
)
:effect
(and
)

)
(:action Prim85Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-G))
)
:effect
(and
)

)
(:action Prim98Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-K))
)
:effect
(and
)

)
(:action Prim102Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-G))
)
:effect
(and
)

)
(:action Prim115Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-K))
)
:effect
(and
)

)
(:action Prim119Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-G))
)
:effect
(and
)

)
(:action Prim132Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-K))
)
:effect
(and
)

)
(:action Prim136Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-G))
)
:effect
(and
)

)
(:action Prim149Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-K))
)
:effect
(and
)

)
(:action Prim153Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-G))
)
:effect
(and
)

)
(:action Prim166Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-K))
)
:effect
(and
)

)
(:action Prim170Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-G))
)
:effect
(and
)

)
(:action Prim183Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-K))
)
:effect
(and
)

)
(:action Prim187Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-G))
)
:effect
(and
)

)
(:action Prim200Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-K))
)
:effect
(and
)

)
(:action Prim204Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-G))
)
:effect
(and
)

)
(:action STOP-PASS_N-J
:parameters ()
:precondition
(and
(Flag29-)
(Flag29prime-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag213prime-))
(not (Flag212prime-))
(not (Flag211prime-))
(not (Flag210prime-))
(not (Flag209prime-))
(not (Flag208prime-))
(not (Flag32prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag34prime-))
(not (Flag30prime-))
(not (Flag36prime-))
(not (Flag33prime-))
(not (Flag28prime-))
(not (Flag35prime-))
(not (Flag21prime-))
(not (Flag37prime-))
)
)
(:action STOP-PASS_N-H
:parameters ()
:precondition
(and
(Flag31-)
(Flag31prime-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag213prime-))
(not (Flag212prime-))
(not (Flag211prime-))
(not (Flag210prime-))
(not (Flag209prime-))
(not (Flag208prime-))
(not (Flag32prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag34prime-))
(not (Flag30prime-))
(not (Flag36prime-))
(not (Flag33prime-))
(not (Flag28prime-))
(not (Flag35prime-))
(not (Flag21prime-))
(not (Flag37prime-))
)
)
(:action STOP-PASS_M-K
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-K)
(Flag212prime-)
)
:effect
(and
(when
(and
(Flag212-)
)
(and
(BOARDED-PASS_M)
(not (NOT-BOARDED-PASS_M))
)
)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag213prime-))
(not (Flag212prime-))
(not (Flag211prime-))
(not (Flag210prime-))
(not (Flag209prime-))
(not (Flag208prime-))
(not (Flag32prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag34prime-))
(not (Flag30prime-))
(not (Flag36prime-))
(not (Flag33prime-))
(not (Flag28prime-))
(not (Flag35prime-))
(not (Flag21prime-))
(not (Flag37prime-))
)
)
(:action STOP-PASS_M-G
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-G)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag213prime-))
(not (Flag212prime-))
(not (Flag211prime-))
(not (Flag210prime-))
(not (Flag209prime-))
(not (Flag208prime-))
(not (Flag32prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag34prime-))
(not (Flag30prime-))
(not (Flag36prime-))
(not (Flag33prime-))
(not (Flag28prime-))
(not (Flag35prime-))
(not (Flag21prime-))
(not (Flag37prime-))
)
)
(:action STOP-PASS_L-K
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-K)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag213prime-))
(not (Flag212prime-))
(not (Flag211prime-))
(not (Flag210prime-))
(not (Flag209prime-))
(not (Flag208prime-))
(not (Flag32prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag34prime-))
(not (Flag30prime-))
(not (Flag36prime-))
(not (Flag33prime-))
(not (Flag28prime-))
(not (Flag35prime-))
(not (Flag21prime-))
(not (Flag37prime-))
)
)
(:action STOP-PASS_L-G
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-G)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag213prime-))
(not (Flag212prime-))
(not (Flag211prime-))
(not (Flag210prime-))
(not (Flag209prime-))
(not (Flag208prime-))
(not (Flag32prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag34prime-))
(not (Flag30prime-))
(not (Flag36prime-))
(not (Flag33prime-))
(not (Flag28prime-))
(not (Flag35prime-))
(not (Flag21prime-))
(not (Flag37prime-))
)
)
(:action STOP-PASS_B-K
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-K)
(Flag210prime-)
)
:effect
(and
(when
(and
(BOARDED-PASS_B)
)
(and
(SERVED-PASS_B)
(NOT-BOARDED-PASS_B)
(not (NOT-SERVED-PASS_B))
(not (BOARDED-PASS_B))
)
)
(when
(and
(Flag210-)
)
(and
(BOARDED-PASS_B)
(not (NOT-BOARDED-PASS_B))
)
)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag213prime-))
(not (Flag212prime-))
(not (Flag211prime-))
(not (Flag210prime-))
(not (Flag209prime-))
(not (Flag208prime-))
(not (Flag32prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag34prime-))
(not (Flag30prime-))
(not (Flag36prime-))
(not (Flag33prime-))
(not (Flag28prime-))
(not (Flag35prime-))
(not (Flag21prime-))
(not (Flag37prime-))
)
)
(:action STOP-PASS_B-G
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-G)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag213prime-))
(not (Flag212prime-))
(not (Flag211prime-))
(not (Flag210prime-))
(not (Flag209prime-))
(not (Flag208prime-))
(not (Flag32prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag34prime-))
(not (Flag30prime-))
(not (Flag36prime-))
(not (Flag33prime-))
(not (Flag28prime-))
(not (Flag35prime-))
(not (Flag21prime-))
(not (Flag37prime-))
)
)
(:action STOP-PASS_A-K
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-K)
(Flag209prime-)
)
:effect
(and
(when
(and
(Flag209-)
)
(and
(BOARDED-PASS_A)
(not (NOT-BOARDED-PASS_A))
)
)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag213prime-))
(not (Flag212prime-))
(not (Flag211prime-))
(not (Flag210prime-))
(not (Flag209prime-))
(not (Flag208prime-))
(not (Flag32prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag34prime-))
(not (Flag30prime-))
(not (Flag36prime-))
(not (Flag33prime-))
(not (Flag28prime-))
(not (Flag35prime-))
(not (Flag21prime-))
(not (Flag37prime-))
)
)
(:action STOP-PASS_A-G
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-G)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag213prime-))
(not (Flag212prime-))
(not (Flag211prime-))
(not (Flag210prime-))
(not (Flag209prime-))
(not (Flag208prime-))
(not (Flag32prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag34prime-))
(not (Flag30prime-))
(not (Flag36prime-))
(not (Flag33prime-))
(not (Flag28prime-))
(not (Flag35prime-))
(not (Flag21prime-))
(not (Flag37prime-))
)
)
(:action STOP-PASS_E-K
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-K)
(Flag208prime-)
)
:effect
(and
(when
(and
(Flag208-)
)
(and
(BOARDED-PASS_E)
(not (NOT-BOARDED-PASS_E))
)
)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag213prime-))
(not (Flag212prime-))
(not (Flag211prime-))
(not (Flag210prime-))
(not (Flag209prime-))
(not (Flag208prime-))
(not (Flag32prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag34prime-))
(not (Flag30prime-))
(not (Flag36prime-))
(not (Flag33prime-))
(not (Flag28prime-))
(not (Flag35prime-))
(not (Flag21prime-))
(not (Flag37prime-))
)
)
(:action STOP-PASS_E-G
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-G)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag213prime-))
(not (Flag212prime-))
(not (Flag211prime-))
(not (Flag210prime-))
(not (Flag209prime-))
(not (Flag208prime-))
(not (Flag32prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag34prime-))
(not (Flag30prime-))
(not (Flag36prime-))
(not (Flag33prime-))
(not (Flag28prime-))
(not (Flag35prime-))
(not (Flag21prime-))
(not (Flag37prime-))
)
)
(:action Flag2Action-0
:parameters ()
:precondition
(and
(Flag1-)
(Flag1prime-)
)
:effect
(and
(Flag2-)
(Flag2prime-)
)

)
(:action Flag4Action-0
:parameters ()
:precondition
(and
(Flag3-)
(Flag3prime-)
)
:effect
(and
(Flag4-)
(Flag4prime-)
)

)
(:action Flag6Action-0
:parameters ()
:precondition
(and
(Flag5-)
(Flag5prime-)
)
:effect
(and
(Flag6-)
(Flag6prime-)
)

)
(:action Flag8Action-0
:parameters ()
:precondition
(and
(Flag7-)
(Flag7prime-)
)
:effect
(and
(Flag8-)
(Flag8prime-)
)

)
(:action Flag10Action-0
:parameters ()
:precondition
(and
(Flag9-)
(Flag9prime-)
)
:effect
(and
(Flag10-)
(Flag10prime-)
)

)
(:action Flag12Action-0
:parameters ()
:precondition
(and
(Flag11-)
(Flag11prime-)
)
:effect
(and
(Flag12-)
(Flag12prime-)
)

)
(:action Flag14Action-0
:parameters ()
:precondition
(and
(Flag13-)
(Flag13prime-)
)
:effect
(and
(Flag14-)
(Flag14prime-)
)

)
(:action Flag16Action-0
:parameters ()
:precondition
(and
(Flag15-)
(Flag15prime-)
)
:effect
(and
(Flag16-)
(Flag16prime-)
)

)
(:action Flag18Action-0
:parameters ()
:precondition
(and
(Flag17-)
(Flag17prime-)
)
:effect
(and
(Flag18-)
(Flag18prime-)
)

)
(:action Flag20Action-0
:parameters ()
:precondition
(and
(Flag19-)
(Flag19prime-)
)
:effect
(and
(Flag20-)
(Flag20prime-)
)

)
(:action Prim21Action-14
:parameters ()
:precondition
(and
(not (SERVED-PASS_M))
)
:effect
(and
(Flag21prime-)
(not (Flag21-))
)

)
(:action MOVEDOWN-K-J
:parameters ()
:precondition
(and
(LIFT_AT-K)
(NOT-ERROR-)
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(NOT-LIFT_AT-K)
(LIFT_AT-J)
(CHECKCONSISTENCY-)
(not (LIFT_AT-K))
(not (NOT-LIFT_AT-J))
(not (NOT-CHECKCONSISTENCY-))
(not (Flag213prime-))
(not (Flag212prime-))
(not (Flag211prime-))
(not (Flag210prime-))
(not (Flag209prime-))
(not (Flag208prime-))
(not (Flag32prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag34prime-))
(not (Flag30prime-))
(not (Flag36prime-))
(not (Flag33prime-))
(not (Flag28prime-))
(not (Flag35prime-))
(not (Flag21prime-))
(not (Flag37prime-))
)
)
(:action MOVEDOWN-H-G
:parameters ()
:precondition
(and
(LIFT_AT-H)
(NOT-ERROR-)
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(NOT-LIFT_AT-H)
(LIFT_AT-G)
(CHECKCONSISTENCY-)
(not (LIFT_AT-H))
(not (NOT-LIFT_AT-G))
(not (NOT-CHECKCONSISTENCY-))
(not (Flag213prime-))
(not (Flag212prime-))
(not (Flag211prime-))
(not (Flag210prime-))
(not (Flag209prime-))
(not (Flag208prime-))
(not (Flag32prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag34prime-))
(not (Flag30prime-))
(not (Flag36prime-))
(not (Flag33prime-))
(not (Flag28prime-))
(not (Flag35prime-))
(not (Flag21prime-))
(not (Flag37prime-))
)
)
(:action MOVEUP-H-I
:parameters ()
:precondition
(and
(LIFT_AT-H)
(NOT-ERROR-)
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(NOT-LIFT_AT-H)
(LIFT_AT-I)
(CHECKCONSISTENCY-)
(not (LIFT_AT-H))
(not (NOT-LIFT_AT-I))
(not (NOT-CHECKCONSISTENCY-))
(not (Flag213prime-))
(not (Flag212prime-))
(not (Flag211prime-))
(not (Flag210prime-))
(not (Flag209prime-))
(not (Flag208prime-))
(not (Flag32prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag34prime-))
(not (Flag30prime-))
(not (Flag36prime-))
(not (Flag33prime-))
(not (Flag28prime-))
(not (Flag35prime-))
(not (Flag21prime-))
(not (Flag37prime-))
)
)
(:action MOVEUP-J-K
:parameters ()
:precondition
(and
(LIFT_AT-J)
(NOT-ERROR-)
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(NOT-LIFT_AT-J)
(LIFT_AT-K)
(CHECKCONSISTENCY-)
(not (LIFT_AT-J))
(not (NOT-LIFT_AT-K))
(not (NOT-CHECKCONSISTENCY-))
(not (Flag213prime-))
(not (Flag212prime-))
(not (Flag211prime-))
(not (Flag210prime-))
(not (Flag209prime-))
(not (Flag208prime-))
(not (Flag32prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag34prime-))
(not (Flag30prime-))
(not (Flag36prime-))
(not (Flag33prime-))
(not (Flag28prime-))
(not (Flag35prime-))
(not (Flag21prime-))
(not (Flag37prime-))
)
)
(:action Flag29Action
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-J)
)
:effect
(and
(Flag29-)
(Flag29prime-)
)

)
(:action Prim29Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-J))
)
:effect
(and
(Flag29prime-)
(not (Flag29-))
)

)
(:action Flag31Action
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-H)
)
:effect
(and
(Flag31-)
(Flag31prime-)
)

)
(:action Prim31Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-H))
)
:effect
(and
(Flag31prime-)
(not (Flag31-))
)

)
(:action Prim46Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-J))
)
:effect
(and
)

)
(:action Prim48Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-H))
)
:effect
(and
)

)
(:action Prim63Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-J))
)
:effect
(and
)

)
(:action Prim65Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-H))
)
:effect
(and
)

)
(:action Prim80Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-J))
)
:effect
(and
)

)
(:action Prim82Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-H))
)
:effect
(and
)

)
(:action Prim97Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-J))
)
:effect
(and
)

)
(:action Prim99Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-H))
)
:effect
(and
)

)
(:action Prim114Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-J))
)
:effect
(and
)

)
(:action Prim116Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-H))
)
:effect
(and
)

)
(:action Prim131Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-J))
)
:effect
(and
)

)
(:action Prim133Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-H))
)
:effect
(and
)

)
(:action Prim148Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-J))
)
:effect
(and
)

)
(:action Prim150Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-H))
)
:effect
(and
)

)
(:action Prim165Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-J))
)
:effect
(and
)

)
(:action Prim167Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-H))
)
:effect
(and
)

)
(:action Prim182Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-J))
)
:effect
(and
)

)
(:action Prim184Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-H))
)
:effect
(and
)

)
(:action Prim199Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-J))
)
:effect
(and
)

)
(:action Prim201Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-H))
)
:effect
(and
)

)
(:action STOP-PASS_N-I
:parameters ()
:precondition
(and
(Flag32-)
(Flag32prime-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag213prime-))
(not (Flag212prime-))
(not (Flag211prime-))
(not (Flag210prime-))
(not (Flag209prime-))
(not (Flag208prime-))
(not (Flag32prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag34prime-))
(not (Flag30prime-))
(not (Flag36prime-))
(not (Flag33prime-))
(not (Flag28prime-))
(not (Flag35prime-))
(not (Flag21prime-))
(not (Flag37prime-))
)
)
(:action STOP-PASS_M-J
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-J)
)
:effect
(and
(when
(and
(BOARDED-PASS_M)
)
(and
(SERVED-PASS_M)
(NOT-BOARDED-PASS_M)
(not (NOT-SERVED-PASS_M))
(not (BOARDED-PASS_M))
)
)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag213prime-))
(not (Flag212prime-))
(not (Flag211prime-))
(not (Flag210prime-))
(not (Flag209prime-))
(not (Flag208prime-))
(not (Flag32prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag34prime-))
(not (Flag30prime-))
(not (Flag36prime-))
(not (Flag33prime-))
(not (Flag28prime-))
(not (Flag35prime-))
(not (Flag21prime-))
(not (Flag37prime-))
)
)
(:action STOP-PASS_M-H
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-H)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag213prime-))
(not (Flag212prime-))
(not (Flag211prime-))
(not (Flag210prime-))
(not (Flag209prime-))
(not (Flag208prime-))
(not (Flag32prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag34prime-))
(not (Flag30prime-))
(not (Flag36prime-))
(not (Flag33prime-))
(not (Flag28prime-))
(not (Flag35prime-))
(not (Flag21prime-))
(not (Flag37prime-))
)
)
(:action STOP-PASS_M-I
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-I)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag213prime-))
(not (Flag212prime-))
(not (Flag211prime-))
(not (Flag210prime-))
(not (Flag209prime-))
(not (Flag208prime-))
(not (Flag32prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag34prime-))
(not (Flag30prime-))
(not (Flag36prime-))
(not (Flag33prime-))
(not (Flag28prime-))
(not (Flag35prime-))
(not (Flag21prime-))
(not (Flag37prime-))
)
)
(:action STOP-PASS_L-J
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-J)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag213prime-))
(not (Flag212prime-))
(not (Flag211prime-))
(not (Flag210prime-))
(not (Flag209prime-))
(not (Flag208prime-))
(not (Flag32prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag34prime-))
(not (Flag30prime-))
(not (Flag36prime-))
(not (Flag33prime-))
(not (Flag28prime-))
(not (Flag35prime-))
(not (Flag21prime-))
(not (Flag37prime-))
)
)
(:action STOP-PASS_L-H
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-H)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag213prime-))
(not (Flag212prime-))
(not (Flag211prime-))
(not (Flag210prime-))
(not (Flag209prime-))
(not (Flag208prime-))
(not (Flag32prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag34prime-))
(not (Flag30prime-))
(not (Flag36prime-))
(not (Flag33prime-))
(not (Flag28prime-))
(not (Flag35prime-))
(not (Flag21prime-))
(not (Flag37prime-))
)
)
(:action STOP-PASS_L-I
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-I)
(Flag211prime-)
)
:effect
(and
(when
(and
(Flag211-)
)
(and
(BOARDED-PASS_L)
(not (NOT-BOARDED-PASS_L))
)
)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag213prime-))
(not (Flag212prime-))
(not (Flag211prime-))
(not (Flag210prime-))
(not (Flag209prime-))
(not (Flag208prime-))
(not (Flag32prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag34prime-))
(not (Flag30prime-))
(not (Flag36prime-))
(not (Flag33prime-))
(not (Flag28prime-))
(not (Flag35prime-))
(not (Flag21prime-))
(not (Flag37prime-))
)
)
(:action STOP-PASS_B-J
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-J)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag213prime-))
(not (Flag212prime-))
(not (Flag211prime-))
(not (Flag210prime-))
(not (Flag209prime-))
(not (Flag208prime-))
(not (Flag32prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag34prime-))
(not (Flag30prime-))
(not (Flag36prime-))
(not (Flag33prime-))
(not (Flag28prime-))
(not (Flag35prime-))
(not (Flag21prime-))
(not (Flag37prime-))
)
)
(:action STOP-PASS_B-H
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-H)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag213prime-))
(not (Flag212prime-))
(not (Flag211prime-))
(not (Flag210prime-))
(not (Flag209prime-))
(not (Flag208prime-))
(not (Flag32prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag34prime-))
(not (Flag30prime-))
(not (Flag36prime-))
(not (Flag33prime-))
(not (Flag28prime-))
(not (Flag35prime-))
(not (Flag21prime-))
(not (Flag37prime-))
)
)
(:action STOP-PASS_B-I
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-I)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag213prime-))
(not (Flag212prime-))
(not (Flag211prime-))
(not (Flag210prime-))
(not (Flag209prime-))
(not (Flag208prime-))
(not (Flag32prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag34prime-))
(not (Flag30prime-))
(not (Flag36prime-))
(not (Flag33prime-))
(not (Flag28prime-))
(not (Flag35prime-))
(not (Flag21prime-))
(not (Flag37prime-))
)
)
(:action STOP-PASS_A-J
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-J)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag213prime-))
(not (Flag212prime-))
(not (Flag211prime-))
(not (Flag210prime-))
(not (Flag209prime-))
(not (Flag208prime-))
(not (Flag32prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag34prime-))
(not (Flag30prime-))
(not (Flag36prime-))
(not (Flag33prime-))
(not (Flag28prime-))
(not (Flag35prime-))
(not (Flag21prime-))
(not (Flag37prime-))
)
)
(:action STOP-PASS_A-H
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-H)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag213prime-))
(not (Flag212prime-))
(not (Flag211prime-))
(not (Flag210prime-))
(not (Flag209prime-))
(not (Flag208prime-))
(not (Flag32prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag34prime-))
(not (Flag30prime-))
(not (Flag36prime-))
(not (Flag33prime-))
(not (Flag28prime-))
(not (Flag35prime-))
(not (Flag21prime-))
(not (Flag37prime-))
)
)
(:action STOP-PASS_A-I
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-I)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag213prime-))
(not (Flag212prime-))
(not (Flag211prime-))
(not (Flag210prime-))
(not (Flag209prime-))
(not (Flag208prime-))
(not (Flag32prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag34prime-))
(not (Flag30prime-))
(not (Flag36prime-))
(not (Flag33prime-))
(not (Flag28prime-))
(not (Flag35prime-))
(not (Flag21prime-))
(not (Flag37prime-))
)
)
(:action STOP-PASS_E-J
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-J)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag213prime-))
(not (Flag212prime-))
(not (Flag211prime-))
(not (Flag210prime-))
(not (Flag209prime-))
(not (Flag208prime-))
(not (Flag32prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag34prime-))
(not (Flag30prime-))
(not (Flag36prime-))
(not (Flag33prime-))
(not (Flag28prime-))
(not (Flag35prime-))
(not (Flag21prime-))
(not (Flag37prime-))
)
)
(:action STOP-PASS_E-H
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-H)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag213prime-))
(not (Flag212prime-))
(not (Flag211prime-))
(not (Flag210prime-))
(not (Flag209prime-))
(not (Flag208prime-))
(not (Flag32prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag34prime-))
(not (Flag30prime-))
(not (Flag36prime-))
(not (Flag33prime-))
(not (Flag28prime-))
(not (Flag35prime-))
(not (Flag21prime-))
(not (Flag37prime-))
)
)
(:action STOP-PASS_E-I
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-I)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag213prime-))
(not (Flag212prime-))
(not (Flag211prime-))
(not (Flag210prime-))
(not (Flag209prime-))
(not (Flag208prime-))
(not (Flag32prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag34prime-))
(not (Flag30prime-))
(not (Flag36prime-))
(not (Flag33prime-))
(not (Flag28prime-))
(not (Flag35prime-))
(not (Flag21prime-))
(not (Flag37prime-))
)
)
(:action Flag1Action
:parameters ()
:effect
(and
(Flag1-)
(Flag1prime-)
)

)
(:action Flag3Action
:parameters ()
:effect
(and
(Flag3-)
(Flag3prime-)
)

)
(:action Flag5Action
:parameters ()
:effect
(and
(Flag5-)
(Flag5prime-)
)

)
(:action Flag7Action
:parameters ()
:effect
(and
(Flag7-)
(Flag7prime-)
)

)
(:action Flag9Action
:parameters ()
:effect
(and
(Flag9-)
(Flag9prime-)
)

)
(:action Flag11Action
:parameters ()
:effect
(and
(Flag11-)
(Flag11prime-)
)

)
(:action Flag13Action
:parameters ()
:effect
(and
(Flag13-)
(Flag13prime-)
)

)
(:action Flag15Action
:parameters ()
:effect
(and
(Flag15-)
(Flag15prime-)
)

)
(:action Flag17Action
:parameters ()
:effect
(and
(Flag17-)
(Flag17prime-)
)

)
(:action Flag19Action
:parameters ()
:effect
(and
(Flag19-)
(Flag19prime-)
)

)
(:action Prim21Action-16
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
(Flag21prime-)
(not (Flag21-))
)

)
(:action Prim21Action-17
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
(Flag21prime-)
(not (Flag21-))
)

)
(:action CHECKCONSISTENCYACTION
:parameters ()
:precondition
(and
(NOT-ERROR-)
(CHECKCONSISTENCY-)
)
:effect
(and
(NOT-CHECKCONSISTENCY-)
(not (CHECKCONSISTENCY-))
(not (Flag213prime-))
(not (Flag212prime-))
(not (Flag211prime-))
(not (Flag210prime-))
(not (Flag209prime-))
(not (Flag208prime-))
(not (Flag32prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag34prime-))
(not (Flag30prime-))
(not (Flag36prime-))
(not (Flag33prime-))
(not (Flag28prime-))
(not (Flag35prime-))
(not (Flag21prime-))
(not (Flag37prime-))
)
)
(:action MOVEDOWN-J-I
:parameters ()
:precondition
(and
(LIFT_AT-J)
(NOT-ERROR-)
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(NOT-LIFT_AT-J)
(LIFT_AT-I)
(CHECKCONSISTENCY-)
(not (LIFT_AT-J))
(not (NOT-LIFT_AT-I))
(not (NOT-CHECKCONSISTENCY-))
(not (Flag213prime-))
(not (Flag212prime-))
(not (Flag211prime-))
(not (Flag210prime-))
(not (Flag209prime-))
(not (Flag208prime-))
(not (Flag32prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag34prime-))
(not (Flag30prime-))
(not (Flag36prime-))
(not (Flag33prime-))
(not (Flag28prime-))
(not (Flag35prime-))
(not (Flag21prime-))
(not (Flag37prime-))
)
)
(:action MOVEDOWN-I-H
:parameters ()
:precondition
(and
(LIFT_AT-I)
(NOT-ERROR-)
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(NOT-LIFT_AT-I)
(LIFT_AT-H)
(CHECKCONSISTENCY-)
(not (LIFT_AT-I))
(not (NOT-LIFT_AT-H))
(not (NOT-CHECKCONSISTENCY-))
(not (Flag213prime-))
(not (Flag212prime-))
(not (Flag211prime-))
(not (Flag210prime-))
(not (Flag209prime-))
(not (Flag208prime-))
(not (Flag32prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag34prime-))
(not (Flag30prime-))
(not (Flag36prime-))
(not (Flag33prime-))
(not (Flag28prime-))
(not (Flag35prime-))
(not (Flag21prime-))
(not (Flag37prime-))
)
)
(:action MOVEUP-I-J
:parameters ()
:precondition
(and
(LIFT_AT-I)
(NOT-ERROR-)
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(NOT-LIFT_AT-I)
(LIFT_AT-J)
(CHECKCONSISTENCY-)
(not (LIFT_AT-I))
(not (NOT-LIFT_AT-J))
(not (NOT-CHECKCONSISTENCY-))
(not (Flag213prime-))
(not (Flag212prime-))
(not (Flag211prime-))
(not (Flag210prime-))
(not (Flag209prime-))
(not (Flag208prime-))
(not (Flag32prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag34prime-))
(not (Flag30prime-))
(not (Flag36prime-))
(not (Flag33prime-))
(not (Flag28prime-))
(not (Flag35prime-))
(not (Flag21prime-))
(not (Flag37prime-))
)
)
(:action Prim22Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim22Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim23Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim23Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim24Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim24Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim25Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim25Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim26Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim26Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim27Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim27Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim28Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
(Flag28prime-)
(not (Flag28-))
)

)
(:action Prim28Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
(Flag28prime-)
(not (Flag28-))
)

)
(:action Prim29Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
(Flag29prime-)
(not (Flag29-))
)

)
(:action Prim29Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
(Flag29prime-)
(not (Flag29-))
)

)
(:action Prim30Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
(Flag30prime-)
(not (Flag30-))
)

)
(:action Prim30Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
(Flag30prime-)
(not (Flag30-))
)

)
(:action Prim31Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
(Flag31prime-)
(not (Flag31-))
)

)
(:action Prim31Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
(Flag31prime-)
(not (Flag31-))
)

)
(:action Flag32Action
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-I)
)
:effect
(and
(Flag32-)
(Flag32prime-)
)

)
(:action Prim32Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
(Flag32prime-)
(not (Flag32-))
)

)
(:action Prim32Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
(Flag32prime-)
(not (Flag32-))
)

)
(:action Prim32Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-I))
)
:effect
(and
(Flag32prime-)
(not (Flag32-))
)

)
(:action Prim33Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
(Flag33prime-)
(not (Flag33-))
)

)
(:action Prim33Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
(Flag33prime-)
(not (Flag33-))
)

)
(:action Prim34Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
(Flag34prime-)
(not (Flag34-))
)

)
(:action Prim34Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
(Flag34prime-)
(not (Flag34-))
)

)
(:action Prim35Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
(Flag35prime-)
(not (Flag35-))
)

)
(:action Prim35Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
(Flag35prime-)
(not (Flag35-))
)

)
(:action Prim36Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
(Flag36prime-)
(not (Flag36-))
)

)
(:action Prim36Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
(Flag36prime-)
(not (Flag36-))
)

)
(:action Prim37Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
(Flag37prime-)
(not (Flag37-))
)

)
(:action Prim37Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
(Flag37prime-)
(not (Flag37-))
)

)
(:action Prim39Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim39Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim40Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim40Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim41Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim41Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim42Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim42Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim43Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim43Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim44Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim44Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim45Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim45Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim46Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim46Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim47Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim47Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim48Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim48Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim49Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim49Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim49Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-I))
)
:effect
(and
)

)
(:action Prim50Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim50Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim51Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim51Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim52Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim52Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim53Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim53Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim54Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim54Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim56Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim56Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim57Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim57Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim58Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim58Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim59Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim59Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim60Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim60Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim61Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim61Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim62Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim62Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim63Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim63Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim64Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim64Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim65Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim65Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim66Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim66Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim66Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-I))
)
:effect
(and
)

)
(:action Prim67Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim67Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim68Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim68Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim69Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim69Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim70Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim70Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim71Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim71Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim73Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim73Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim74Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim74Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim75Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim75Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim76Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim76Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim77Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim77Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim78Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim78Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim79Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim79Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim80Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim80Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim81Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim81Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim82Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim82Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim83Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim83Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim83Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-I))
)
:effect
(and
)

)
(:action Prim84Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim84Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim85Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim85Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim86Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim86Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim87Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim87Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim88Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim88Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim90Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim90Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim91Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim91Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim92Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim92Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim93Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim93Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim94Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim94Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim95Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim95Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim96Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim96Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim97Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim97Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim98Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim98Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim99Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim99Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim100Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim100Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim100Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-I))
)
:effect
(and
)

)
(:action Prim101Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim101Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim102Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim102Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim103Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim103Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim104Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim104Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim105Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim105Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim107Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim107Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim108Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim108Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim109Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim109Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim110Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim110Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim111Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim111Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim112Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim112Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim113Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim113Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim114Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim114Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim115Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim115Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim116Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim116Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim117Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim117Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim117Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-I))
)
:effect
(and
)

)
(:action Prim118Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim118Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim119Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim119Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim120Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim120Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim121Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim121Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim122Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim122Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim124Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim124Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim125Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim125Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim126Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim126Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim127Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim127Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim128Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim128Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim129Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim129Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim130Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim130Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim131Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim131Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim132Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim132Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim133Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim133Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim134Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim134Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim134Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-I))
)
:effect
(and
)

)
(:action Prim135Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim135Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim136Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim136Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim137Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim137Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim138Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim138Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim139Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim139Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim141Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim141Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim142Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim142Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim143Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim143Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim144Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim144Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim145Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim145Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim146Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim146Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim147Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim147Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim148Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim148Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim149Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim149Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim150Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim150Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim151Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim151Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim151Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-I))
)
:effect
(and
)

)
(:action Prim152Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim152Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim153Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim153Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim154Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim154Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim155Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim155Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim156Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim156Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim158Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim158Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim159Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim159Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim160Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim160Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim161Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim161Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim162Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim162Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim163Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim163Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim164Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim164Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim165Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim165Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim166Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim166Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim167Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim167Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim168Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim168Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim168Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-I))
)
:effect
(and
)

)
(:action Prim169Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim169Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim170Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim170Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim171Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim171Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim172Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim172Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim173Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim173Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim175Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim175Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim176Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim176Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim177Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim177Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim178Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim178Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim179Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim179Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim180Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim180Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim181Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim181Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim182Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim182Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim183Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim183Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim184Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim184Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim185Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim185Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim185Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-I))
)
:effect
(and
)

)
(:action Prim186Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim186Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim187Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim187Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim188Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim188Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim189Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim189Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim190Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim190Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim192Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim192Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim193Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim193Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim194Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim194Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim195Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim195Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim196Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim196Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim197Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim197Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim198Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim198Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim199Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim199Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim200Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim200Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim201Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim201Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim202Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim202Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim202Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-I))
)
:effect
(and
)

)
(:action Prim203Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim203Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim204Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim204Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim205Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim205Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim206Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim206Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim207Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim207Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Flag208Action
:parameters ()
:precondition
(and
(NOT-BOARDED-PASS_E)
(NOT-SERVED-PASS_E)
)
:effect
(and
(Flag208-)
(Flag208prime-)
)

)
(:action Prim208Action-0
:parameters ()
:precondition
(and
(not (NOT-BOARDED-PASS_E))
)
:effect
(and
(Flag208prime-)
(not (Flag208-))
)

)
(:action Prim208Action-1
:parameters ()
:precondition
(and
(not (NOT-SERVED-PASS_E))
)
:effect
(and
(Flag208prime-)
(not (Flag208-))
)

)
(:action Flag209Action
:parameters ()
:precondition
(and
(NOT-BOARDED-PASS_A)
(NOT-SERVED-PASS_A)
)
:effect
(and
(Flag209-)
(Flag209prime-)
)

)
(:action Prim209Action-0
:parameters ()
:precondition
(and
(not (NOT-BOARDED-PASS_A))
)
:effect
(and
(Flag209prime-)
(not (Flag209-))
)

)
(:action Prim209Action-1
:parameters ()
:precondition
(and
(not (NOT-SERVED-PASS_A))
)
:effect
(and
(Flag209prime-)
(not (Flag209-))
)

)
(:action Flag210Action
:parameters ()
:precondition
(and
(NOT-BOARDED-PASS_B)
(NOT-SERVED-PASS_B)
)
:effect
(and
(Flag210-)
(Flag210prime-)
)

)
(:action Prim210Action-0
:parameters ()
:precondition
(and
(not (NOT-BOARDED-PASS_B))
)
:effect
(and
(Flag210prime-)
(not (Flag210-))
)

)
(:action Prim210Action-1
:parameters ()
:precondition
(and
(not (NOT-SERVED-PASS_B))
)
:effect
(and
(Flag210prime-)
(not (Flag210-))
)

)
(:action Flag211Action
:parameters ()
:precondition
(and
(NOT-BOARDED-PASS_L)
(NOT-SERVED-PASS_L)
)
:effect
(and
(Flag211-)
(Flag211prime-)
)

)
(:action Prim211Action-0
:parameters ()
:precondition
(and
(not (NOT-BOARDED-PASS_L))
)
:effect
(and
(Flag211prime-)
(not (Flag211-))
)

)
(:action Prim211Action-1
:parameters ()
:precondition
(and
(not (NOT-SERVED-PASS_L))
)
:effect
(and
(Flag211prime-)
(not (Flag211-))
)

)
(:action Flag212Action
:parameters ()
:precondition
(and
(NOT-BOARDED-PASS_M)
(NOT-SERVED-PASS_M)
)
:effect
(and
(Flag212-)
(Flag212prime-)
)

)
(:action Prim212Action-0
:parameters ()
:precondition
(and
(not (NOT-BOARDED-PASS_M))
)
:effect
(and
(Flag212prime-)
(not (Flag212-))
)

)
(:action Prim212Action-1
:parameters ()
:precondition
(and
(not (NOT-SERVED-PASS_M))
)
:effect
(and
(Flag212prime-)
(not (Flag212-))
)

)
(:action Flag213Action
:parameters ()
:precondition
(and
(NOT-BOARDED-PASS_N)
(NOT-SERVED-PASS_N)
)
:effect
(and
(Flag213-)
(Flag213prime-)
)

)
(:action Prim213Action-0
:parameters ()
:precondition
(and
(not (NOT-BOARDED-PASS_N))
)
:effect
(and
(Flag213prime-)
(not (Flag213-))
)

)
(:action Prim213Action-1
:parameters ()
:precondition
(and
(not (NOT-SERVED-PASS_N))
)
:effect
(and
(Flag213prime-)
(not (Flag213-))
)

)
)
