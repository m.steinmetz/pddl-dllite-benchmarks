(define (problem grounded-SIMPLE-ADL-ELEVATORS_PROBLEM)
(:domain grounded-SIMPLE-ADL-ELEVATORS)
(:init
(NOT-LIFT_AT-A)
(NOT-LIFT_AT-E)
(NOT-LIFT_AT-D)
(NOT-LIFT_AT-G)
(NOT-LIFT_AT-F)
(NOT-LIFT_AT-H)
(NOT-LIFT_AT-K)
(NOT-LIFT_AT-J)
(NOT-LIFT_AT-M)
(NOT-LIFT_AT-O)
(NOT-LIFT_AT-U)
(NOT-LIFT_AT-T)
(NOT-LIFT_AT-W)
(NOT-LIFT_AT-V)
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(NOT-BOARDED-PASS_Q)
(NOT-BOARDED-PASS_R)
(NOT-BOARDED-PASS_S)
(NOT-BOARDED-PASS_X)
(NOT-BOARDED-PASS_B)
(NOT-BOARDED-PASS_C)
(NOT-BOARDED-PASS_L)
(NOT-BOARDED-PASS_N)
(NOT-BOARDED-PASS_I)
(NOT-SERVED-PASS_Q)
(NOT-SERVED-PASS_R)
(NOT-SERVED-PASS_S)
(NOT-SERVED-PASS_X)
(NOT-SERVED-PASS_B)
(NOT-SERVED-PASS_C)
(NOT-SERVED-PASS_L)
(NOT-SERVED-PASS_N)
(NOT-SERVED-PASS_I)
(LIFT_AT-P)
)
(:goal
(and
(Flag31-)
)
)
)
