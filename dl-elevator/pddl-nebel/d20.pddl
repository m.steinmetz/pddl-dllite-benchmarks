(define (domain grounded-SIMPLE-ADL-ELEVATORS)
(:requirements
:strips
)
(:predicates
(Flag305-)
(Flag304-)
(Flag303-)
(Flag302-)
(Flag301-)
(Flag300-)
(Flag299-)
(Flag298-)
(Flag64-)
(NOT-LIFT_AT-E)
(LIFT_AT-D)
(CHECKCONSISTENCY-)
(LIFT_AT-F)
(LIFT_AT-G)
(ERROR-)
(Flag23-)
(Flag21-)
(Flag19-)
(Flag17-)
(Flag15-)
(Flag13-)
(Flag11-)
(Flag9-)
(Flag7-)
(Flag5-)
(Flag3-)
(Flag1-)
(BOARDED-PASS_L)
(BOARDED-PASS_M)
(Flag58-)
(Flag54-)
(Flag47-)
(LIFT_AT-B)
(LIFT_AT-I)
(LIFT_AT-J)
(LIFT_AT-K)
(Flag24-)
(Flag22-)
(Flag20-)
(Flag18-)
(Flag16-)
(Flag14-)
(Flag12-)
(Flag10-)
(Flag8-)
(Flag6-)
(Flag4-)
(Flag2-)
(BOARDED-PASS_R)
(SERVED-PASS_L)
(SERVED-PASS_M)
(BOARDED-PASS_N)
(SERVED-PASS_N)
(Flag65-)
(Flag60-)
(Flag59-)
(Flag55-)
(LIFT_AT-A)
(LIFT_AT-O)
(LIFT_AT-P)
(BOARDED-PASS_Q)
(SERVED-PASS_Q)
(SERVED-PASS_R)
(SERVED-PASS_S)
(BOARDED-PASS_S)
(BOARDED-PASS_C)
(SERVED-PASS_C)
(Flag66-)
(Flag57-)
(Flag56-)
(LIFT_AT-T)
(BOARDED-PASS_H)
(SERVED-PASS_H)
(Flag53-)
(Flag25-)
(NOT-CHECKCONSISTENCY-)
(NOT-LIFT_AT-P)
(NOT-SERVED-PASS_H)
(NOT-BOARDED-PASS_H)
(NOT-LIFT_AT-T)
(NOT-LIFT_AT-O)
(NOT-LIFT_AT-K)
(NOT-SERVED-PASS_C)
(NOT-BOARDED-PASS_C)
(NOT-BOARDED-PASS_S)
(NOT-SERVED-PASS_S)
(NOT-SERVED-PASS_R)
(NOT-SERVED-PASS_Q)
(NOT-BOARDED-PASS_Q)
(NOT-LIFT_AT-B)
(NOT-LIFT_AT-J)
(NOT-LIFT_AT-A)
(NOT-LIFT_AT-I)
(NOT-LIFT_AT-G)
(NOT-SERVED-PASS_N)
(NOT-BOARDED-PASS_N)
(NOT-SERVED-PASS_M)
(NOT-SERVED-PASS_L)
(NOT-BOARDED-PASS_R)
(NOT-LIFT_AT-D)
(NOT-LIFT_AT-F)
(NOT-BOARDED-PASS_M)
(NOT-BOARDED-PASS_L)
(NOT-ERROR-)
(LIFT_AT-E)
(Flag305prime-)
(Flag304prime-)
(Flag303prime-)
(Flag302prime-)
(Flag301prime-)
(Flag300prime-)
(Flag299prime-)
(Flag298prime-)
(Flag64prime-)
(Flag23prime-)
(Flag21prime-)
(Flag19prime-)
(Flag17prime-)
(Flag15prime-)
(Flag13prime-)
(Flag11prime-)
(Flag9prime-)
(Flag7prime-)
(Flag5prime-)
(Flag3prime-)
(Flag1prime-)
(Flag58prime-)
(Flag54prime-)
(Flag47prime-)
(Flag24prime-)
(Flag22prime-)
(Flag20prime-)
(Flag18prime-)
(Flag16prime-)
(Flag14prime-)
(Flag12prime-)
(Flag10prime-)
(Flag8prime-)
(Flag6prime-)
(Flag4prime-)
(Flag2prime-)
(Flag65prime-)
(Flag60prime-)
(Flag59prime-)
(Flag55prime-)
(Flag66prime-)
(Flag57prime-)
(Flag56prime-)
(Flag53prime-)
(Flag25prime-)
)
(:action STOP-PASS_H-T
:parameters ()
:precondition
(and
(Flag53-)
(Flag53prime-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag305prime-))
(not (Flag304prime-))
(not (Flag303prime-))
(not (Flag302prime-))
(not (Flag301prime-))
(not (Flag300prime-))
(not (Flag299prime-))
(not (Flag298prime-))
(not (Flag64prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag58prime-))
(not (Flag54prime-))
(not (Flag47prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag65prime-))
(not (Flag60prime-))
(not (Flag59prime-))
(not (Flag55prime-))
(not (Flag66prime-))
(not (Flag57prime-))
(not (Flag56prime-))
(not (Flag53prime-))
(not (Flag25prime-))
)
)
(:action Flag25Action
:parameters ()
:precondition
(and
(Flag2-)
(Flag2prime-)
(Flag4-)
(Flag4prime-)
(Flag6-)
(Flag6prime-)
(SERVED-PASS_Q)
(SERVED-PASS_R)
(SERVED-PASS_S)
(Flag8-)
(Flag8prime-)
(Flag10-)
(Flag10prime-)
(Flag12-)
(Flag12prime-)
(Flag14-)
(Flag14prime-)
(Flag16-)
(Flag16prime-)
(Flag18-)
(Flag18prime-)
(Flag20-)
(Flag20prime-)
(Flag22-)
(Flag22prime-)
(SERVED-PASS_C)
(SERVED-PASS_L)
(SERVED-PASS_M)
(SERVED-PASS_N)
(SERVED-PASS_H)
(Flag24-)
(Flag24prime-)
(NOT-ERROR-)
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(Flag25-)
(Flag25prime-)
)

)
(:action Prim25Action-18
:parameters ()
:precondition
(and
(not (SERVED-PASS_H))
)
:effect
(and
(Flag25prime-)
(not (Flag25-))
)

)
(:action MOVEDOWN-T-P
:parameters ()
:precondition
(and
(LIFT_AT-T)
(NOT-ERROR-)
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(NOT-LIFT_AT-T)
(LIFT_AT-P)
(CHECKCONSISTENCY-)
(not (LIFT_AT-T))
(not (NOT-LIFT_AT-P))
(not (NOT-CHECKCONSISTENCY-))
(not (Flag305prime-))
(not (Flag304prime-))
(not (Flag303prime-))
(not (Flag302prime-))
(not (Flag301prime-))
(not (Flag300prime-))
(not (Flag299prime-))
(not (Flag298prime-))
(not (Flag64prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag58prime-))
(not (Flag54prime-))
(not (Flag47prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag65prime-))
(not (Flag60prime-))
(not (Flag59prime-))
(not (Flag55prime-))
(not (Flag66prime-))
(not (Flag57prime-))
(not (Flag56prime-))
(not (Flag53prime-))
(not (Flag25prime-))
)
)
(:action Prim33Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-T))
)
:effect
(and
)

)
(:action Flag53Action
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-T)
)
:effect
(and
(Flag53-)
(Flag53prime-)
)

)
(:action Prim53Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-T))
)
:effect
(and
(Flag53prime-)
(not (Flag53-))
)

)
(:action Prim74Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-T))
)
:effect
(and
)

)
(:action Prim95Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-T))
)
:effect
(and
)

)
(:action Prim116Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-T))
)
:effect
(and
)

)
(:action Prim137Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-T))
)
:effect
(and
)

)
(:action Prim158Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-T))
)
:effect
(and
)

)
(:action Prim179Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-T))
)
:effect
(and
)

)
(:action Prim200Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-T))
)
:effect
(and
)

)
(:action Prim221Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-T))
)
:effect
(and
)

)
(:action Prim242Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-T))
)
:effect
(and
)

)
(:action Prim263Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-T))
)
:effect
(and
)

)
(:action Prim284Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-T))
)
:effect
(and
)

)
(:action STOP-PASS_H-O
:parameters ()
:precondition
(and
(Flag56-)
(Flag56prime-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag305prime-))
(not (Flag304prime-))
(not (Flag303prime-))
(not (Flag302prime-))
(not (Flag301prime-))
(not (Flag300prime-))
(not (Flag299prime-))
(not (Flag298prime-))
(not (Flag64prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag58prime-))
(not (Flag54prime-))
(not (Flag47prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag65prime-))
(not (Flag60prime-))
(not (Flag59prime-))
(not (Flag55prime-))
(not (Flag66prime-))
(not (Flag57prime-))
(not (Flag56prime-))
(not (Flag53prime-))
(not (Flag25prime-))
)
)
(:action STOP-PASS_H-P
:parameters ()
:precondition
(and
(Flag57-)
(Flag57prime-)
)
:effect
(and
(when
(and
(BOARDED-PASS_H)
)
(and
(SERVED-PASS_H)
(NOT-BOARDED-PASS_H)
(not (NOT-SERVED-PASS_H))
(not (BOARDED-PASS_H))
)
)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag305prime-))
(not (Flag304prime-))
(not (Flag303prime-))
(not (Flag302prime-))
(not (Flag301prime-))
(not (Flag300prime-))
(not (Flag299prime-))
(not (Flag298prime-))
(not (Flag64prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag58prime-))
(not (Flag54prime-))
(not (Flag47prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag65prime-))
(not (Flag60prime-))
(not (Flag59prime-))
(not (Flag55prime-))
(not (Flag66prime-))
(not (Flag57prime-))
(not (Flag56prime-))
(not (Flag53prime-))
(not (Flag25prime-))
)
)
(:action STOP-PASS_H-A
:parameters ()
:precondition
(and
(Flag66-)
(Flag66prime-)
(Flag305prime-)
)
:effect
(and
(when
(and
(Flag305-)
)
(and
(BOARDED-PASS_H)
(not (NOT-BOARDED-PASS_H))
)
)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag305prime-))
(not (Flag304prime-))
(not (Flag303prime-))
(not (Flag302prime-))
(not (Flag301prime-))
(not (Flag300prime-))
(not (Flag299prime-))
(not (Flag298prime-))
(not (Flag64prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag58prime-))
(not (Flag54prime-))
(not (Flag47prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag65prime-))
(not (Flag60prime-))
(not (Flag59prime-))
(not (Flag55prime-))
(not (Flag66prime-))
(not (Flag57prime-))
(not (Flag56prime-))
(not (Flag53prime-))
(not (Flag25prime-))
)
)
(:action STOP-PASS_N-T
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-T)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag305prime-))
(not (Flag304prime-))
(not (Flag303prime-))
(not (Flag302prime-))
(not (Flag301prime-))
(not (Flag300prime-))
(not (Flag299prime-))
(not (Flag298prime-))
(not (Flag64prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag58prime-))
(not (Flag54prime-))
(not (Flag47prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag65prime-))
(not (Flag60prime-))
(not (Flag59prime-))
(not (Flag55prime-))
(not (Flag66prime-))
(not (Flag57prime-))
(not (Flag56prime-))
(not (Flag53prime-))
(not (Flag25prime-))
)
)
(:action STOP-PASS_M-T
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-T)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag305prime-))
(not (Flag304prime-))
(not (Flag303prime-))
(not (Flag302prime-))
(not (Flag301prime-))
(not (Flag300prime-))
(not (Flag299prime-))
(not (Flag298prime-))
(not (Flag64prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag58prime-))
(not (Flag54prime-))
(not (Flag47prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag65prime-))
(not (Flag60prime-))
(not (Flag59prime-))
(not (Flag55prime-))
(not (Flag66prime-))
(not (Flag57prime-))
(not (Flag56prime-))
(not (Flag53prime-))
(not (Flag25prime-))
)
)
(:action STOP-PASS_L-T
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-T)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag305prime-))
(not (Flag304prime-))
(not (Flag303prime-))
(not (Flag302prime-))
(not (Flag301prime-))
(not (Flag300prime-))
(not (Flag299prime-))
(not (Flag298prime-))
(not (Flag64prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag58prime-))
(not (Flag54prime-))
(not (Flag47prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag65prime-))
(not (Flag60prime-))
(not (Flag59prime-))
(not (Flag55prime-))
(not (Flag66prime-))
(not (Flag57prime-))
(not (Flag56prime-))
(not (Flag53prime-))
(not (Flag25prime-))
)
)
(:action STOP-PASS_C-T
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-T)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag305prime-))
(not (Flag304prime-))
(not (Flag303prime-))
(not (Flag302prime-))
(not (Flag301prime-))
(not (Flag300prime-))
(not (Flag299prime-))
(not (Flag298prime-))
(not (Flag64prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag58prime-))
(not (Flag54prime-))
(not (Flag47prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag65prime-))
(not (Flag60prime-))
(not (Flag59prime-))
(not (Flag55prime-))
(not (Flag66prime-))
(not (Flag57prime-))
(not (Flag56prime-))
(not (Flag53prime-))
(not (Flag25prime-))
)
)
(:action STOP-PASS_S-T
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-T)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag305prime-))
(not (Flag304prime-))
(not (Flag303prime-))
(not (Flag302prime-))
(not (Flag301prime-))
(not (Flag300prime-))
(not (Flag299prime-))
(not (Flag298prime-))
(not (Flag64prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag58prime-))
(not (Flag54prime-))
(not (Flag47prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag65prime-))
(not (Flag60prime-))
(not (Flag59prime-))
(not (Flag55prime-))
(not (Flag66prime-))
(not (Flag57prime-))
(not (Flag56prime-))
(not (Flag53prime-))
(not (Flag25prime-))
)
)
(:action STOP-PASS_R-T
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-T)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag305prime-))
(not (Flag304prime-))
(not (Flag303prime-))
(not (Flag302prime-))
(not (Flag301prime-))
(not (Flag300prime-))
(not (Flag299prime-))
(not (Flag298prime-))
(not (Flag64prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag58prime-))
(not (Flag54prime-))
(not (Flag47prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag65prime-))
(not (Flag60prime-))
(not (Flag59prime-))
(not (Flag55prime-))
(not (Flag66prime-))
(not (Flag57prime-))
(not (Flag56prime-))
(not (Flag53prime-))
(not (Flag25prime-))
)
)
(:action STOP-PASS_Q-T
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-T)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag305prime-))
(not (Flag304prime-))
(not (Flag303prime-))
(not (Flag302prime-))
(not (Flag301prime-))
(not (Flag300prime-))
(not (Flag299prime-))
(not (Flag298prime-))
(not (Flag64prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag58prime-))
(not (Flag54prime-))
(not (Flag47prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag65prime-))
(not (Flag60prime-))
(not (Flag59prime-))
(not (Flag55prime-))
(not (Flag66prime-))
(not (Flag57prime-))
(not (Flag56prime-))
(not (Flag53prime-))
(not (Flag25prime-))
)
)
(:action Prim25Action-3
:parameters ()
:precondition
(and
(not (SERVED-PASS_Q))
)
:effect
(and
(Flag25prime-)
(not (Flag25-))
)

)
(:action Prim25Action-4
:parameters ()
:precondition
(and
(not (SERVED-PASS_R))
)
:effect
(and
(Flag25prime-)
(not (Flag25-))
)

)
(:action Prim25Action-5
:parameters ()
:precondition
(and
(not (SERVED-PASS_S))
)
:effect
(and
(Flag25prime-)
(not (Flag25-))
)

)
(:action Prim25Action-14
:parameters ()
:precondition
(and
(not (SERVED-PASS_C))
)
:effect
(and
(Flag25prime-)
(not (Flag25-))
)

)
(:action MOVEUP-P-T
:parameters ()
:precondition
(and
(LIFT_AT-P)
(NOT-ERROR-)
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(NOT-LIFT_AT-P)
(LIFT_AT-T)
(CHECKCONSISTENCY-)
(not (LIFT_AT-P))
(not (NOT-LIFT_AT-T))
(not (NOT-CHECKCONSISTENCY-))
(not (Flag305prime-))
(not (Flag304prime-))
(not (Flag303prime-))
(not (Flag302prime-))
(not (Flag301prime-))
(not (Flag300prime-))
(not (Flag299prime-))
(not (Flag298prime-))
(not (Flag64prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag58prime-))
(not (Flag54prime-))
(not (Flag47prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag65prime-))
(not (Flag60prime-))
(not (Flag59prime-))
(not (Flag55prime-))
(not (Flag66prime-))
(not (Flag57prime-))
(not (Flag56prime-))
(not (Flag53prime-))
(not (Flag25prime-))
)
)
(:action MOVEDOWN-P-O
:parameters ()
:precondition
(and
(LIFT_AT-P)
(NOT-ERROR-)
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(NOT-LIFT_AT-P)
(LIFT_AT-O)
(CHECKCONSISTENCY-)
(not (LIFT_AT-P))
(not (NOT-LIFT_AT-O))
(not (NOT-CHECKCONSISTENCY-))
(not (Flag305prime-))
(not (Flag304prime-))
(not (Flag303prime-))
(not (Flag302prime-))
(not (Flag301prime-))
(not (Flag300prime-))
(not (Flag299prime-))
(not (Flag298prime-))
(not (Flag64prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag58prime-))
(not (Flag54prime-))
(not (Flag47prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag65prime-))
(not (Flag60prime-))
(not (Flag59prime-))
(not (Flag55prime-))
(not (Flag66prime-))
(not (Flag57prime-))
(not (Flag56prime-))
(not (Flag53prime-))
(not (Flag25prime-))
)
)
(:action MOVEDOWN-O-K
:parameters ()
:precondition
(and
(LIFT_AT-O)
(NOT-ERROR-)
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(NOT-LIFT_AT-O)
(LIFT_AT-K)
(CHECKCONSISTENCY-)
(not (LIFT_AT-O))
(not (NOT-LIFT_AT-K))
(not (NOT-CHECKCONSISTENCY-))
(not (Flag305prime-))
(not (Flag304prime-))
(not (Flag303prime-))
(not (Flag302prime-))
(not (Flag301prime-))
(not (Flag300prime-))
(not (Flag299prime-))
(not (Flag298prime-))
(not (Flag64prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag58prime-))
(not (Flag54prime-))
(not (Flag47prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag65prime-))
(not (Flag60prime-))
(not (Flag59prime-))
(not (Flag55prime-))
(not (Flag66prime-))
(not (Flag57prime-))
(not (Flag56prime-))
(not (Flag53prime-))
(not (Flag25prime-))
)
)
(:action Prim36Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-O))
)
:effect
(and
)

)
(:action Prim37Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-P))
)
:effect
(and
)

)
(:action Prim46Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-A))
)
:effect
(and
)

)
(:action Flag56Action
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-O)
)
:effect
(and
(Flag56-)
(Flag56prime-)
)

)
(:action Prim56Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-O))
)
:effect
(and
(Flag56prime-)
(not (Flag56-))
)

)
(:action Flag57Action
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-P)
)
:effect
(and
(Flag57-)
(Flag57prime-)
)

)
(:action Prim57Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-P))
)
:effect
(and
(Flag57prime-)
(not (Flag57-))
)

)
(:action Flag66Action
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-A)
)
:effect
(and
(Flag66-)
(Flag66prime-)
)

)
(:action Prim66Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-A))
)
:effect
(and
(Flag66prime-)
(not (Flag66-))
)

)
(:action Prim77Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-O))
)
:effect
(and
)

)
(:action Prim78Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-P))
)
:effect
(and
)

)
(:action Prim87Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-A))
)
:effect
(and
)

)
(:action Prim98Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-O))
)
:effect
(and
)

)
(:action Prim99Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-P))
)
:effect
(and
)

)
(:action Prim108Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-A))
)
:effect
(and
)

)
(:action Prim119Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-O))
)
:effect
(and
)

)
(:action Prim120Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-P))
)
:effect
(and
)

)
(:action Prim129Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-A))
)
:effect
(and
)

)
(:action Prim140Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-O))
)
:effect
(and
)

)
(:action Prim141Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-P))
)
:effect
(and
)

)
(:action Prim150Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-A))
)
:effect
(and
)

)
(:action Prim161Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-O))
)
:effect
(and
)

)
(:action Prim162Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-P))
)
:effect
(and
)

)
(:action Prim171Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-A))
)
:effect
(and
)

)
(:action Prim182Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-O))
)
:effect
(and
)

)
(:action Prim183Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-P))
)
:effect
(and
)

)
(:action Prim192Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-A))
)
:effect
(and
)

)
(:action Prim203Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-O))
)
:effect
(and
)

)
(:action Prim204Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-P))
)
:effect
(and
)

)
(:action Prim213Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-A))
)
:effect
(and
)

)
(:action Prim224Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-O))
)
:effect
(and
)

)
(:action Prim225Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-P))
)
:effect
(and
)

)
(:action Prim234Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-A))
)
:effect
(and
)

)
(:action Prim245Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-O))
)
:effect
(and
)

)
(:action Prim246Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-P))
)
:effect
(and
)

)
(:action Prim255Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-A))
)
:effect
(and
)

)
(:action Prim266Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-O))
)
:effect
(and
)

)
(:action Prim267Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-P))
)
:effect
(and
)

)
(:action Prim276Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-A))
)
:effect
(and
)

)
(:action Prim287Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-O))
)
:effect
(and
)

)
(:action Prim288Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-P))
)
:effect
(and
)

)
(:action Prim297Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-A))
)
:effect
(and
)

)
(:action STOP-PASS_H-J
:parameters ()
:precondition
(and
(Flag55-)
(Flag55prime-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag305prime-))
(not (Flag304prime-))
(not (Flag303prime-))
(not (Flag302prime-))
(not (Flag301prime-))
(not (Flag300prime-))
(not (Flag299prime-))
(not (Flag298prime-))
(not (Flag64prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag58prime-))
(not (Flag54prime-))
(not (Flag47prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag65prime-))
(not (Flag60prime-))
(not (Flag59prime-))
(not (Flag55prime-))
(not (Flag66prime-))
(not (Flag57prime-))
(not (Flag56prime-))
(not (Flag53prime-))
(not (Flag25prime-))
)
)
(:action STOP-PASS_H-K
:parameters ()
:precondition
(and
(Flag59-)
(Flag59prime-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag305prime-))
(not (Flag304prime-))
(not (Flag303prime-))
(not (Flag302prime-))
(not (Flag301prime-))
(not (Flag300prime-))
(not (Flag299prime-))
(not (Flag298prime-))
(not (Flag64prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag58prime-))
(not (Flag54prime-))
(not (Flag47prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag65prime-))
(not (Flag60prime-))
(not (Flag59prime-))
(not (Flag55prime-))
(not (Flag66prime-))
(not (Flag57prime-))
(not (Flag56prime-))
(not (Flag53prime-))
(not (Flag25prime-))
)
)
(:action STOP-PASS_H-I
:parameters ()
:precondition
(and
(Flag60-)
(Flag60prime-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag305prime-))
(not (Flag304prime-))
(not (Flag303prime-))
(not (Flag302prime-))
(not (Flag301prime-))
(not (Flag300prime-))
(not (Flag299prime-))
(not (Flag298prime-))
(not (Flag64prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag58prime-))
(not (Flag54prime-))
(not (Flag47prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag65prime-))
(not (Flag60prime-))
(not (Flag59prime-))
(not (Flag55prime-))
(not (Flag66prime-))
(not (Flag57prime-))
(not (Flag56prime-))
(not (Flag53prime-))
(not (Flag25prime-))
)
)
(:action STOP-PASS_H-B
:parameters ()
:precondition
(and
(Flag65-)
(Flag65prime-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag305prime-))
(not (Flag304prime-))
(not (Flag303prime-))
(not (Flag302prime-))
(not (Flag301prime-))
(not (Flag300prime-))
(not (Flag299prime-))
(not (Flag298prime-))
(not (Flag64prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag58prime-))
(not (Flag54prime-))
(not (Flag47prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag65prime-))
(not (Flag60prime-))
(not (Flag59prime-))
(not (Flag55prime-))
(not (Flag66prime-))
(not (Flag57prime-))
(not (Flag56prime-))
(not (Flag53prime-))
(not (Flag25prime-))
)
)
(:action STOP-PASS_N-O
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-O)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag305prime-))
(not (Flag304prime-))
(not (Flag303prime-))
(not (Flag302prime-))
(not (Flag301prime-))
(not (Flag300prime-))
(not (Flag299prime-))
(not (Flag298prime-))
(not (Flag64prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag58prime-))
(not (Flag54prime-))
(not (Flag47prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag65prime-))
(not (Flag60prime-))
(not (Flag59prime-))
(not (Flag55prime-))
(not (Flag66prime-))
(not (Flag57prime-))
(not (Flag56prime-))
(not (Flag53prime-))
(not (Flag25prime-))
)
)
(:action STOP-PASS_N-P
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-P)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag305prime-))
(not (Flag304prime-))
(not (Flag303prime-))
(not (Flag302prime-))
(not (Flag301prime-))
(not (Flag300prime-))
(not (Flag299prime-))
(not (Flag298prime-))
(not (Flag64prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag58prime-))
(not (Flag54prime-))
(not (Flag47prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag65prime-))
(not (Flag60prime-))
(not (Flag59prime-))
(not (Flag55prime-))
(not (Flag66prime-))
(not (Flag57prime-))
(not (Flag56prime-))
(not (Flag53prime-))
(not (Flag25prime-))
)
)
(:action STOP-PASS_N-A
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-A)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag305prime-))
(not (Flag304prime-))
(not (Flag303prime-))
(not (Flag302prime-))
(not (Flag301prime-))
(not (Flag300prime-))
(not (Flag299prime-))
(not (Flag298prime-))
(not (Flag64prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag58prime-))
(not (Flag54prime-))
(not (Flag47prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag65prime-))
(not (Flag60prime-))
(not (Flag59prime-))
(not (Flag55prime-))
(not (Flag66prime-))
(not (Flag57prime-))
(not (Flag56prime-))
(not (Flag53prime-))
(not (Flag25prime-))
)
)
(:action STOP-PASS_M-O
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-O)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag305prime-))
(not (Flag304prime-))
(not (Flag303prime-))
(not (Flag302prime-))
(not (Flag301prime-))
(not (Flag300prime-))
(not (Flag299prime-))
(not (Flag298prime-))
(not (Flag64prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag58prime-))
(not (Flag54prime-))
(not (Flag47prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag65prime-))
(not (Flag60prime-))
(not (Flag59prime-))
(not (Flag55prime-))
(not (Flag66prime-))
(not (Flag57prime-))
(not (Flag56prime-))
(not (Flag53prime-))
(not (Flag25prime-))
)
)
(:action STOP-PASS_M-P
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-P)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag305prime-))
(not (Flag304prime-))
(not (Flag303prime-))
(not (Flag302prime-))
(not (Flag301prime-))
(not (Flag300prime-))
(not (Flag299prime-))
(not (Flag298prime-))
(not (Flag64prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag58prime-))
(not (Flag54prime-))
(not (Flag47prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag65prime-))
(not (Flag60prime-))
(not (Flag59prime-))
(not (Flag55prime-))
(not (Flag66prime-))
(not (Flag57prime-))
(not (Flag56prime-))
(not (Flag53prime-))
(not (Flag25prime-))
)
)
(:action STOP-PASS_M-A
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-A)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag305prime-))
(not (Flag304prime-))
(not (Flag303prime-))
(not (Flag302prime-))
(not (Flag301prime-))
(not (Flag300prime-))
(not (Flag299prime-))
(not (Flag298prime-))
(not (Flag64prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag58prime-))
(not (Flag54prime-))
(not (Flag47prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag65prime-))
(not (Flag60prime-))
(not (Flag59prime-))
(not (Flag55prime-))
(not (Flag66prime-))
(not (Flag57prime-))
(not (Flag56prime-))
(not (Flag53prime-))
(not (Flag25prime-))
)
)
(:action STOP-PASS_L-O
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-O)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag305prime-))
(not (Flag304prime-))
(not (Flag303prime-))
(not (Flag302prime-))
(not (Flag301prime-))
(not (Flag300prime-))
(not (Flag299prime-))
(not (Flag298prime-))
(not (Flag64prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag58prime-))
(not (Flag54prime-))
(not (Flag47prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag65prime-))
(not (Flag60prime-))
(not (Flag59prime-))
(not (Flag55prime-))
(not (Flag66prime-))
(not (Flag57prime-))
(not (Flag56prime-))
(not (Flag53prime-))
(not (Flag25prime-))
)
)
(:action STOP-PASS_L-P
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-P)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag305prime-))
(not (Flag304prime-))
(not (Flag303prime-))
(not (Flag302prime-))
(not (Flag301prime-))
(not (Flag300prime-))
(not (Flag299prime-))
(not (Flag298prime-))
(not (Flag64prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag58prime-))
(not (Flag54prime-))
(not (Flag47prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag65prime-))
(not (Flag60prime-))
(not (Flag59prime-))
(not (Flag55prime-))
(not (Flag66prime-))
(not (Flag57prime-))
(not (Flag56prime-))
(not (Flag53prime-))
(not (Flag25prime-))
)
)
(:action STOP-PASS_L-A
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-A)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag305prime-))
(not (Flag304prime-))
(not (Flag303prime-))
(not (Flag302prime-))
(not (Flag301prime-))
(not (Flag300prime-))
(not (Flag299prime-))
(not (Flag298prime-))
(not (Flag64prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag58prime-))
(not (Flag54prime-))
(not (Flag47prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag65prime-))
(not (Flag60prime-))
(not (Flag59prime-))
(not (Flag55prime-))
(not (Flag66prime-))
(not (Flag57prime-))
(not (Flag56prime-))
(not (Flag53prime-))
(not (Flag25prime-))
)
)
(:action STOP-PASS_C-O
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-O)
)
:effect
(and
(when
(and
(BOARDED-PASS_C)
)
(and
(SERVED-PASS_C)
(NOT-BOARDED-PASS_C)
(not (NOT-SERVED-PASS_C))
(not (BOARDED-PASS_C))
)
)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag305prime-))
(not (Flag304prime-))
(not (Flag303prime-))
(not (Flag302prime-))
(not (Flag301prime-))
(not (Flag300prime-))
(not (Flag299prime-))
(not (Flag298prime-))
(not (Flag64prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag58prime-))
(not (Flag54prime-))
(not (Flag47prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag65prime-))
(not (Flag60prime-))
(not (Flag59prime-))
(not (Flag55prime-))
(not (Flag66prime-))
(not (Flag57prime-))
(not (Flag56prime-))
(not (Flag53prime-))
(not (Flag25prime-))
)
)
(:action STOP-PASS_C-P
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-P)
(Flag301prime-)
)
:effect
(and
(when
(and
(Flag301-)
)
(and
(BOARDED-PASS_C)
(not (NOT-BOARDED-PASS_C))
)
)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag305prime-))
(not (Flag304prime-))
(not (Flag303prime-))
(not (Flag302prime-))
(not (Flag301prime-))
(not (Flag300prime-))
(not (Flag299prime-))
(not (Flag298prime-))
(not (Flag64prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag58prime-))
(not (Flag54prime-))
(not (Flag47prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag65prime-))
(not (Flag60prime-))
(not (Flag59prime-))
(not (Flag55prime-))
(not (Flag66prime-))
(not (Flag57prime-))
(not (Flag56prime-))
(not (Flag53prime-))
(not (Flag25prime-))
)
)
(:action STOP-PASS_C-A
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-A)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag305prime-))
(not (Flag304prime-))
(not (Flag303prime-))
(not (Flag302prime-))
(not (Flag301prime-))
(not (Flag300prime-))
(not (Flag299prime-))
(not (Flag298prime-))
(not (Flag64prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag58prime-))
(not (Flag54prime-))
(not (Flag47prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag65prime-))
(not (Flag60prime-))
(not (Flag59prime-))
(not (Flag55prime-))
(not (Flag66prime-))
(not (Flag57prime-))
(not (Flag56prime-))
(not (Flag53prime-))
(not (Flag25prime-))
)
)
(:action STOP-PASS_S-O
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-O)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag305prime-))
(not (Flag304prime-))
(not (Flag303prime-))
(not (Flag302prime-))
(not (Flag301prime-))
(not (Flag300prime-))
(not (Flag299prime-))
(not (Flag298prime-))
(not (Flag64prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag58prime-))
(not (Flag54prime-))
(not (Flag47prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag65prime-))
(not (Flag60prime-))
(not (Flag59prime-))
(not (Flag55prime-))
(not (Flag66prime-))
(not (Flag57prime-))
(not (Flag56prime-))
(not (Flag53prime-))
(not (Flag25prime-))
)
)
(:action STOP-PASS_S-P
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-P)
(Flag300prime-)
)
:effect
(and
(when
(and
(Flag300-)
)
(and
(BOARDED-PASS_S)
(not (NOT-BOARDED-PASS_S))
)
)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag305prime-))
(not (Flag304prime-))
(not (Flag303prime-))
(not (Flag302prime-))
(not (Flag301prime-))
(not (Flag300prime-))
(not (Flag299prime-))
(not (Flag298prime-))
(not (Flag64prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag58prime-))
(not (Flag54prime-))
(not (Flag47prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag65prime-))
(not (Flag60prime-))
(not (Flag59prime-))
(not (Flag55prime-))
(not (Flag66prime-))
(not (Flag57prime-))
(not (Flag56prime-))
(not (Flag53prime-))
(not (Flag25prime-))
)
)
(:action STOP-PASS_S-A
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-A)
)
:effect
(and
(when
(and
(BOARDED-PASS_S)
)
(and
(SERVED-PASS_S)
(NOT-BOARDED-PASS_S)
(not (NOT-SERVED-PASS_S))
(not (BOARDED-PASS_S))
)
)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag305prime-))
(not (Flag304prime-))
(not (Flag303prime-))
(not (Flag302prime-))
(not (Flag301prime-))
(not (Flag300prime-))
(not (Flag299prime-))
(not (Flag298prime-))
(not (Flag64prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag58prime-))
(not (Flag54prime-))
(not (Flag47prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag65prime-))
(not (Flag60prime-))
(not (Flag59prime-))
(not (Flag55prime-))
(not (Flag66prime-))
(not (Flag57prime-))
(not (Flag56prime-))
(not (Flag53prime-))
(not (Flag25prime-))
)
)
(:action STOP-PASS_R-O
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-O)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag305prime-))
(not (Flag304prime-))
(not (Flag303prime-))
(not (Flag302prime-))
(not (Flag301prime-))
(not (Flag300prime-))
(not (Flag299prime-))
(not (Flag298prime-))
(not (Flag64prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag58prime-))
(not (Flag54prime-))
(not (Flag47prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag65prime-))
(not (Flag60prime-))
(not (Flag59prime-))
(not (Flag55prime-))
(not (Flag66prime-))
(not (Flag57prime-))
(not (Flag56prime-))
(not (Flag53prime-))
(not (Flag25prime-))
)
)
(:action STOP-PASS_R-P
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-P)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag305prime-))
(not (Flag304prime-))
(not (Flag303prime-))
(not (Flag302prime-))
(not (Flag301prime-))
(not (Flag300prime-))
(not (Flag299prime-))
(not (Flag298prime-))
(not (Flag64prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag58prime-))
(not (Flag54prime-))
(not (Flag47prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag65prime-))
(not (Flag60prime-))
(not (Flag59prime-))
(not (Flag55prime-))
(not (Flag66prime-))
(not (Flag57prime-))
(not (Flag56prime-))
(not (Flag53prime-))
(not (Flag25prime-))
)
)
(:action STOP-PASS_R-A
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-A)
)
:effect
(and
(when
(and
(BOARDED-PASS_R)
)
(and
(SERVED-PASS_R)
(NOT-BOARDED-PASS_R)
(not (NOT-SERVED-PASS_R))
(not (BOARDED-PASS_R))
)
)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag305prime-))
(not (Flag304prime-))
(not (Flag303prime-))
(not (Flag302prime-))
(not (Flag301prime-))
(not (Flag300prime-))
(not (Flag299prime-))
(not (Flag298prime-))
(not (Flag64prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag58prime-))
(not (Flag54prime-))
(not (Flag47prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag65prime-))
(not (Flag60prime-))
(not (Flag59prime-))
(not (Flag55prime-))
(not (Flag66prime-))
(not (Flag57prime-))
(not (Flag56prime-))
(not (Flag53prime-))
(not (Flag25prime-))
)
)
(:action STOP-PASS_Q-O
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-O)
)
:effect
(and
(when
(and
(BOARDED-PASS_Q)
)
(and
(SERVED-PASS_Q)
(NOT-BOARDED-PASS_Q)
(not (NOT-SERVED-PASS_Q))
(not (BOARDED-PASS_Q))
)
)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag305prime-))
(not (Flag304prime-))
(not (Flag303prime-))
(not (Flag302prime-))
(not (Flag301prime-))
(not (Flag300prime-))
(not (Flag299prime-))
(not (Flag298prime-))
(not (Flag64prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag58prime-))
(not (Flag54prime-))
(not (Flag47prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag65prime-))
(not (Flag60prime-))
(not (Flag59prime-))
(not (Flag55prime-))
(not (Flag66prime-))
(not (Flag57prime-))
(not (Flag56prime-))
(not (Flag53prime-))
(not (Flag25prime-))
)
)
(:action STOP-PASS_Q-P
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-P)
(Flag298prime-)
)
:effect
(and
(when
(and
(Flag298-)
)
(and
(BOARDED-PASS_Q)
(not (NOT-BOARDED-PASS_Q))
)
)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag305prime-))
(not (Flag304prime-))
(not (Flag303prime-))
(not (Flag302prime-))
(not (Flag301prime-))
(not (Flag300prime-))
(not (Flag299prime-))
(not (Flag298prime-))
(not (Flag64prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag58prime-))
(not (Flag54prime-))
(not (Flag47prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag65prime-))
(not (Flag60prime-))
(not (Flag59prime-))
(not (Flag55prime-))
(not (Flag66prime-))
(not (Flag57prime-))
(not (Flag56prime-))
(not (Flag53prime-))
(not (Flag25prime-))
)
)
(:action STOP-PASS_Q-A
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-A)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag305prime-))
(not (Flag304prime-))
(not (Flag303prime-))
(not (Flag302prime-))
(not (Flag301prime-))
(not (Flag300prime-))
(not (Flag299prime-))
(not (Flag298prime-))
(not (Flag64prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag58prime-))
(not (Flag54prime-))
(not (Flag47prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag65prime-))
(not (Flag60prime-))
(not (Flag59prime-))
(not (Flag55prime-))
(not (Flag66prime-))
(not (Flag57prime-))
(not (Flag56prime-))
(not (Flag53prime-))
(not (Flag25prime-))
)
)
(:action Prim25Action-0
:parameters ()
:precondition
(and
(not (Flag2-))
(Flag2prime-)
)
:effect
(and
(Flag25prime-)
(not (Flag25-))
)

)
(:action Prim25Action-1
:parameters ()
:precondition
(and
(not (Flag4-))
(Flag4prime-)
)
:effect
(and
(Flag25prime-)
(not (Flag25-))
)

)
(:action Prim25Action-2
:parameters ()
:precondition
(and
(not (Flag6-))
(Flag6prime-)
)
:effect
(and
(Flag25prime-)
(not (Flag25-))
)

)
(:action Prim25Action-6
:parameters ()
:precondition
(and
(not (Flag8-))
(Flag8prime-)
)
:effect
(and
(Flag25prime-)
(not (Flag25-))
)

)
(:action Prim25Action-7
:parameters ()
:precondition
(and
(not (Flag10-))
(Flag10prime-)
)
:effect
(and
(Flag25prime-)
(not (Flag25-))
)

)
(:action Prim25Action-8
:parameters ()
:precondition
(and
(not (Flag12-))
(Flag12prime-)
)
:effect
(and
(Flag25prime-)
(not (Flag25-))
)

)
(:action Prim25Action-9
:parameters ()
:precondition
(and
(not (Flag14-))
(Flag14prime-)
)
:effect
(and
(Flag25prime-)
(not (Flag25-))
)

)
(:action Prim25Action-10
:parameters ()
:precondition
(and
(not (Flag16-))
(Flag16prime-)
)
:effect
(and
(Flag25prime-)
(not (Flag25-))
)

)
(:action Prim25Action-11
:parameters ()
:precondition
(and
(not (Flag18-))
(Flag18prime-)
)
:effect
(and
(Flag25prime-)
(not (Flag25-))
)

)
(:action Prim25Action-12
:parameters ()
:precondition
(and
(not (Flag20-))
(Flag20prime-)
)
:effect
(and
(Flag25prime-)
(not (Flag25-))
)

)
(:action Prim25Action-13
:parameters ()
:precondition
(and
(not (Flag22-))
(Flag22prime-)
)
:effect
(and
(Flag25prime-)
(not (Flag25-))
)

)
(:action Prim25Action-15
:parameters ()
:precondition
(and
(not (SERVED-PASS_L))
)
:effect
(and
(Flag25prime-)
(not (Flag25-))
)

)
(:action Prim25Action-16
:parameters ()
:precondition
(and
(not (SERVED-PASS_M))
)
:effect
(and
(Flag25prime-)
(not (Flag25-))
)

)
(:action Prim25Action-17
:parameters ()
:precondition
(and
(not (SERVED-PASS_N))
)
:effect
(and
(Flag25prime-)
(not (Flag25-))
)

)
(:action Prim25Action-19
:parameters ()
:precondition
(and
(not (Flag24-))
(Flag24prime-)
)
:effect
(and
(Flag25prime-)
(not (Flag25-))
)

)
(:action MOVEUP-O-P
:parameters ()
:precondition
(and
(LIFT_AT-O)
(NOT-ERROR-)
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(NOT-LIFT_AT-O)
(LIFT_AT-P)
(CHECKCONSISTENCY-)
(not (LIFT_AT-O))
(not (NOT-LIFT_AT-P))
(not (NOT-CHECKCONSISTENCY-))
(not (Flag305prime-))
(not (Flag304prime-))
(not (Flag303prime-))
(not (Flag302prime-))
(not (Flag301prime-))
(not (Flag300prime-))
(not (Flag299prime-))
(not (Flag298prime-))
(not (Flag64prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag58prime-))
(not (Flag54prime-))
(not (Flag47prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag65prime-))
(not (Flag60prime-))
(not (Flag59prime-))
(not (Flag55prime-))
(not (Flag66prime-))
(not (Flag57prime-))
(not (Flag56prime-))
(not (Flag53prime-))
(not (Flag25prime-))
)
)
(:action MOVEUP-K-O
:parameters ()
:precondition
(and
(LIFT_AT-K)
(NOT-ERROR-)
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(NOT-LIFT_AT-K)
(LIFT_AT-O)
(CHECKCONSISTENCY-)
(not (LIFT_AT-K))
(not (NOT-LIFT_AT-O))
(not (NOT-CHECKCONSISTENCY-))
(not (Flag305prime-))
(not (Flag304prime-))
(not (Flag303prime-))
(not (Flag302prime-))
(not (Flag301prime-))
(not (Flag300prime-))
(not (Flag299prime-))
(not (Flag298prime-))
(not (Flag64prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag58prime-))
(not (Flag54prime-))
(not (Flag47prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag65prime-))
(not (Flag60prime-))
(not (Flag59prime-))
(not (Flag55prime-))
(not (Flag66prime-))
(not (Flag57prime-))
(not (Flag56prime-))
(not (Flag53prime-))
(not (Flag25prime-))
)
)
(:action MOVEUP-A-B
:parameters ()
:precondition
(and
(LIFT_AT-A)
(NOT-ERROR-)
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(NOT-LIFT_AT-A)
(LIFT_AT-B)
(CHECKCONSISTENCY-)
(not (LIFT_AT-A))
(not (NOT-LIFT_AT-B))
(not (NOT-CHECKCONSISTENCY-))
(not (Flag305prime-))
(not (Flag304prime-))
(not (Flag303prime-))
(not (Flag302prime-))
(not (Flag301prime-))
(not (Flag300prime-))
(not (Flag299prime-))
(not (Flag298prime-))
(not (Flag64prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag58prime-))
(not (Flag54prime-))
(not (Flag47prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag65prime-))
(not (Flag60prime-))
(not (Flag59prime-))
(not (Flag55prime-))
(not (Flag66prime-))
(not (Flag57prime-))
(not (Flag56prime-))
(not (Flag53prime-))
(not (Flag25prime-))
)
)
(:action MOVEDOWN-K-J
:parameters ()
:precondition
(and
(LIFT_AT-K)
(NOT-ERROR-)
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(NOT-LIFT_AT-K)
(LIFT_AT-J)
(CHECKCONSISTENCY-)
(not (LIFT_AT-K))
(not (NOT-LIFT_AT-J))
(not (NOT-CHECKCONSISTENCY-))
(not (Flag305prime-))
(not (Flag304prime-))
(not (Flag303prime-))
(not (Flag302prime-))
(not (Flag301prime-))
(not (Flag300prime-))
(not (Flag299prime-))
(not (Flag298prime-))
(not (Flag64prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag58prime-))
(not (Flag54prime-))
(not (Flag47prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag65prime-))
(not (Flag60prime-))
(not (Flag59prime-))
(not (Flag55prime-))
(not (Flag66prime-))
(not (Flag57prime-))
(not (Flag56prime-))
(not (Flag53prime-))
(not (Flag25prime-))
)
)
(:action MOVEDOWN-B-A
:parameters ()
:precondition
(and
(LIFT_AT-B)
(NOT-ERROR-)
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(NOT-LIFT_AT-B)
(LIFT_AT-A)
(CHECKCONSISTENCY-)
(not (LIFT_AT-B))
(not (NOT-LIFT_AT-A))
(not (NOT-CHECKCONSISTENCY-))
(not (Flag305prime-))
(not (Flag304prime-))
(not (Flag303prime-))
(not (Flag302prime-))
(not (Flag301prime-))
(not (Flag300prime-))
(not (Flag299prime-))
(not (Flag298prime-))
(not (Flag64prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag58prime-))
(not (Flag54prime-))
(not (Flag47prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag65prime-))
(not (Flag60prime-))
(not (Flag59prime-))
(not (Flag55prime-))
(not (Flag66prime-))
(not (Flag57prime-))
(not (Flag56prime-))
(not (Flag53prime-))
(not (Flag25prime-))
)
)
(:action MOVEDOWN-J-I
:parameters ()
:precondition
(and
(LIFT_AT-J)
(NOT-ERROR-)
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(NOT-LIFT_AT-J)
(LIFT_AT-I)
(CHECKCONSISTENCY-)
(not (LIFT_AT-J))
(not (NOT-LIFT_AT-I))
(not (NOT-CHECKCONSISTENCY-))
(not (Flag305prime-))
(not (Flag304prime-))
(not (Flag303prime-))
(not (Flag302prime-))
(not (Flag301prime-))
(not (Flag300prime-))
(not (Flag299prime-))
(not (Flag298prime-))
(not (Flag64prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag58prime-))
(not (Flag54prime-))
(not (Flag47prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag65prime-))
(not (Flag60prime-))
(not (Flag59prime-))
(not (Flag55prime-))
(not (Flag66prime-))
(not (Flag57prime-))
(not (Flag56prime-))
(not (Flag53prime-))
(not (Flag25prime-))
)
)
(:action MOVEDOWN-I-G
:parameters ()
:precondition
(and
(LIFT_AT-I)
(NOT-ERROR-)
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(NOT-LIFT_AT-I)
(LIFT_AT-G)
(CHECKCONSISTENCY-)
(not (LIFT_AT-I))
(not (NOT-LIFT_AT-G))
(not (NOT-CHECKCONSISTENCY-))
(not (Flag305prime-))
(not (Flag304prime-))
(not (Flag303prime-))
(not (Flag302prime-))
(not (Flag301prime-))
(not (Flag300prime-))
(not (Flag299prime-))
(not (Flag298prime-))
(not (Flag64prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag58prime-))
(not (Flag54prime-))
(not (Flag47prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag65prime-))
(not (Flag60prime-))
(not (Flag59prime-))
(not (Flag55prime-))
(not (Flag66prime-))
(not (Flag57prime-))
(not (Flag56prime-))
(not (Flag53prime-))
(not (Flag25prime-))
)
)
(:action Prim35Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-J))
)
:effect
(and
)

)
(:action Prim39Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-K))
)
:effect
(and
)

)
(:action Prim40Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-I))
)
:effect
(and
)

)
(:action Prim45Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-B))
)
:effect
(and
)

)
(:action Flag55Action
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-J)
)
:effect
(and
(Flag55-)
(Flag55prime-)
)

)
(:action Prim55Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-J))
)
:effect
(and
(Flag55prime-)
(not (Flag55-))
)

)
(:action Flag59Action
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-K)
)
:effect
(and
(Flag59-)
(Flag59prime-)
)

)
(:action Prim59Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-K))
)
:effect
(and
(Flag59prime-)
(not (Flag59-))
)

)
(:action Flag60Action
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-I)
)
:effect
(and
(Flag60-)
(Flag60prime-)
)

)
(:action Prim60Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-I))
)
:effect
(and
(Flag60prime-)
(not (Flag60-))
)

)
(:action Flag65Action
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-B)
)
:effect
(and
(Flag65-)
(Flag65prime-)
)

)
(:action Prim65Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-B))
)
:effect
(and
(Flag65prime-)
(not (Flag65-))
)

)
(:action Prim76Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-J))
)
:effect
(and
)

)
(:action Prim80Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-K))
)
:effect
(and
)

)
(:action Prim81Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-I))
)
:effect
(and
)

)
(:action Prim86Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-B))
)
:effect
(and
)

)
(:action Prim97Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-J))
)
:effect
(and
)

)
(:action Prim101Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-K))
)
:effect
(and
)

)
(:action Prim102Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-I))
)
:effect
(and
)

)
(:action Prim107Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-B))
)
:effect
(and
)

)
(:action Prim118Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-J))
)
:effect
(and
)

)
(:action Prim122Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-K))
)
:effect
(and
)

)
(:action Prim123Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-I))
)
:effect
(and
)

)
(:action Prim128Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-B))
)
:effect
(and
)

)
(:action Prim139Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-J))
)
:effect
(and
)

)
(:action Prim143Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-K))
)
:effect
(and
)

)
(:action Prim144Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-I))
)
:effect
(and
)

)
(:action Prim149Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-B))
)
:effect
(and
)

)
(:action Prim160Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-J))
)
:effect
(and
)

)
(:action Prim164Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-K))
)
:effect
(and
)

)
(:action Prim165Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-I))
)
:effect
(and
)

)
(:action Prim170Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-B))
)
:effect
(and
)

)
(:action Prim181Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-J))
)
:effect
(and
)

)
(:action Prim185Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-K))
)
:effect
(and
)

)
(:action Prim186Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-I))
)
:effect
(and
)

)
(:action Prim191Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-B))
)
:effect
(and
)

)
(:action Prim202Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-J))
)
:effect
(and
)

)
(:action Prim206Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-K))
)
:effect
(and
)

)
(:action Prim207Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-I))
)
:effect
(and
)

)
(:action Prim212Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-B))
)
:effect
(and
)

)
(:action Prim223Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-J))
)
:effect
(and
)

)
(:action Prim227Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-K))
)
:effect
(and
)

)
(:action Prim228Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-I))
)
:effect
(and
)

)
(:action Prim233Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-B))
)
:effect
(and
)

)
(:action Prim244Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-J))
)
:effect
(and
)

)
(:action Prim248Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-K))
)
:effect
(and
)

)
(:action Prim249Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-I))
)
:effect
(and
)

)
(:action Prim254Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-B))
)
:effect
(and
)

)
(:action Prim265Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-J))
)
:effect
(and
)

)
(:action Prim269Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-K))
)
:effect
(and
)

)
(:action Prim270Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-I))
)
:effect
(and
)

)
(:action Prim275Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-B))
)
:effect
(and
)

)
(:action Prim286Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-J))
)
:effect
(and
)

)
(:action Prim290Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-K))
)
:effect
(and
)

)
(:action Prim291Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-I))
)
:effect
(and
)

)
(:action Prim296Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-B))
)
:effect
(and
)

)
(:action STOP-PASS_H-D
:parameters ()
:precondition
(and
(Flag47-)
(Flag47prime-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag305prime-))
(not (Flag304prime-))
(not (Flag303prime-))
(not (Flag302prime-))
(not (Flag301prime-))
(not (Flag300prime-))
(not (Flag299prime-))
(not (Flag298prime-))
(not (Flag64prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag58prime-))
(not (Flag54prime-))
(not (Flag47prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag65prime-))
(not (Flag60prime-))
(not (Flag59prime-))
(not (Flag55prime-))
(not (Flag66prime-))
(not (Flag57prime-))
(not (Flag56prime-))
(not (Flag53prime-))
(not (Flag25prime-))
)
)
(:action STOP-PASS_H-G
:parameters ()
:precondition
(and
(Flag54-)
(Flag54prime-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag305prime-))
(not (Flag304prime-))
(not (Flag303prime-))
(not (Flag302prime-))
(not (Flag301prime-))
(not (Flag300prime-))
(not (Flag299prime-))
(not (Flag298prime-))
(not (Flag64prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag58prime-))
(not (Flag54prime-))
(not (Flag47prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag65prime-))
(not (Flag60prime-))
(not (Flag59prime-))
(not (Flag55prime-))
(not (Flag66prime-))
(not (Flag57prime-))
(not (Flag56prime-))
(not (Flag53prime-))
(not (Flag25prime-))
)
)
(:action STOP-PASS_H-F
:parameters ()
:precondition
(and
(Flag58-)
(Flag58prime-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag305prime-))
(not (Flag304prime-))
(not (Flag303prime-))
(not (Flag302prime-))
(not (Flag301prime-))
(not (Flag300prime-))
(not (Flag299prime-))
(not (Flag298prime-))
(not (Flag64prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag58prime-))
(not (Flag54prime-))
(not (Flag47prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag65prime-))
(not (Flag60prime-))
(not (Flag59prime-))
(not (Flag55prime-))
(not (Flag66prime-))
(not (Flag57prime-))
(not (Flag56prime-))
(not (Flag53prime-))
(not (Flag25prime-))
)
)
(:action STOP-PASS_N-J
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-J)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag305prime-))
(not (Flag304prime-))
(not (Flag303prime-))
(not (Flag302prime-))
(not (Flag301prime-))
(not (Flag300prime-))
(not (Flag299prime-))
(not (Flag298prime-))
(not (Flag64prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag58prime-))
(not (Flag54prime-))
(not (Flag47prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag65prime-))
(not (Flag60prime-))
(not (Flag59prime-))
(not (Flag55prime-))
(not (Flag66prime-))
(not (Flag57prime-))
(not (Flag56prime-))
(not (Flag53prime-))
(not (Flag25prime-))
)
)
(:action STOP-PASS_N-K
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-K)
)
:effect
(and
(when
(and
(BOARDED-PASS_N)
)
(and
(SERVED-PASS_N)
(NOT-BOARDED-PASS_N)
(not (NOT-SERVED-PASS_N))
(not (BOARDED-PASS_N))
)
)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag305prime-))
(not (Flag304prime-))
(not (Flag303prime-))
(not (Flag302prime-))
(not (Flag301prime-))
(not (Flag300prime-))
(not (Flag299prime-))
(not (Flag298prime-))
(not (Flag64prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag58prime-))
(not (Flag54prime-))
(not (Flag47prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag65prime-))
(not (Flag60prime-))
(not (Flag59prime-))
(not (Flag55prime-))
(not (Flag66prime-))
(not (Flag57prime-))
(not (Flag56prime-))
(not (Flag53prime-))
(not (Flag25prime-))
)
)
(:action STOP-PASS_N-I
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-I)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag305prime-))
(not (Flag304prime-))
(not (Flag303prime-))
(not (Flag302prime-))
(not (Flag301prime-))
(not (Flag300prime-))
(not (Flag299prime-))
(not (Flag298prime-))
(not (Flag64prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag58prime-))
(not (Flag54prime-))
(not (Flag47prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag65prime-))
(not (Flag60prime-))
(not (Flag59prime-))
(not (Flag55prime-))
(not (Flag66prime-))
(not (Flag57prime-))
(not (Flag56prime-))
(not (Flag53prime-))
(not (Flag25prime-))
)
)
(:action STOP-PASS_N-B
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-B)
(Flag304prime-)
)
:effect
(and
(when
(and
(Flag304-)
)
(and
(BOARDED-PASS_N)
(not (NOT-BOARDED-PASS_N))
)
)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag305prime-))
(not (Flag304prime-))
(not (Flag303prime-))
(not (Flag302prime-))
(not (Flag301prime-))
(not (Flag300prime-))
(not (Flag299prime-))
(not (Flag298prime-))
(not (Flag64prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag58prime-))
(not (Flag54prime-))
(not (Flag47prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag65prime-))
(not (Flag60prime-))
(not (Flag59prime-))
(not (Flag55prime-))
(not (Flag66prime-))
(not (Flag57prime-))
(not (Flag56prime-))
(not (Flag53prime-))
(not (Flag25prime-))
)
)
(:action STOP-PASS_M-J
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-J)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag305prime-))
(not (Flag304prime-))
(not (Flag303prime-))
(not (Flag302prime-))
(not (Flag301prime-))
(not (Flag300prime-))
(not (Flag299prime-))
(not (Flag298prime-))
(not (Flag64prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag58prime-))
(not (Flag54prime-))
(not (Flag47prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag65prime-))
(not (Flag60prime-))
(not (Flag59prime-))
(not (Flag55prime-))
(not (Flag66prime-))
(not (Flag57prime-))
(not (Flag56prime-))
(not (Flag53prime-))
(not (Flag25prime-))
)
)
(:action STOP-PASS_M-K
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-K)
)
:effect
(and
(when
(and
(BOARDED-PASS_M)
)
(and
(SERVED-PASS_M)
(NOT-BOARDED-PASS_M)
(not (NOT-SERVED-PASS_M))
(not (BOARDED-PASS_M))
)
)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag305prime-))
(not (Flag304prime-))
(not (Flag303prime-))
(not (Flag302prime-))
(not (Flag301prime-))
(not (Flag300prime-))
(not (Flag299prime-))
(not (Flag298prime-))
(not (Flag64prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag58prime-))
(not (Flag54prime-))
(not (Flag47prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag65prime-))
(not (Flag60prime-))
(not (Flag59prime-))
(not (Flag55prime-))
(not (Flag66prime-))
(not (Flag57prime-))
(not (Flag56prime-))
(not (Flag53prime-))
(not (Flag25prime-))
)
)
(:action STOP-PASS_M-I
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-I)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag305prime-))
(not (Flag304prime-))
(not (Flag303prime-))
(not (Flag302prime-))
(not (Flag301prime-))
(not (Flag300prime-))
(not (Flag299prime-))
(not (Flag298prime-))
(not (Flag64prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag58prime-))
(not (Flag54prime-))
(not (Flag47prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag65prime-))
(not (Flag60prime-))
(not (Flag59prime-))
(not (Flag55prime-))
(not (Flag66prime-))
(not (Flag57prime-))
(not (Flag56prime-))
(not (Flag53prime-))
(not (Flag25prime-))
)
)
(:action STOP-PASS_M-B
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-B)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag305prime-))
(not (Flag304prime-))
(not (Flag303prime-))
(not (Flag302prime-))
(not (Flag301prime-))
(not (Flag300prime-))
(not (Flag299prime-))
(not (Flag298prime-))
(not (Flag64prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag58prime-))
(not (Flag54prime-))
(not (Flag47prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag65prime-))
(not (Flag60prime-))
(not (Flag59prime-))
(not (Flag55prime-))
(not (Flag66prime-))
(not (Flag57prime-))
(not (Flag56prime-))
(not (Flag53prime-))
(not (Flag25prime-))
)
)
(:action STOP-PASS_L-J
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-J)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag305prime-))
(not (Flag304prime-))
(not (Flag303prime-))
(not (Flag302prime-))
(not (Flag301prime-))
(not (Flag300prime-))
(not (Flag299prime-))
(not (Flag298prime-))
(not (Flag64prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag58prime-))
(not (Flag54prime-))
(not (Flag47prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag65prime-))
(not (Flag60prime-))
(not (Flag59prime-))
(not (Flag55prime-))
(not (Flag66prime-))
(not (Flag57prime-))
(not (Flag56prime-))
(not (Flag53prime-))
(not (Flag25prime-))
)
)
(:action STOP-PASS_L-K
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-K)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag305prime-))
(not (Flag304prime-))
(not (Flag303prime-))
(not (Flag302prime-))
(not (Flag301prime-))
(not (Flag300prime-))
(not (Flag299prime-))
(not (Flag298prime-))
(not (Flag64prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag58prime-))
(not (Flag54prime-))
(not (Flag47prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag65prime-))
(not (Flag60prime-))
(not (Flag59prime-))
(not (Flag55prime-))
(not (Flag66prime-))
(not (Flag57prime-))
(not (Flag56prime-))
(not (Flag53prime-))
(not (Flag25prime-))
)
)
(:action STOP-PASS_L-I
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-I)
)
:effect
(and
(when
(and
(BOARDED-PASS_L)
)
(and
(SERVED-PASS_L)
(NOT-BOARDED-PASS_L)
(not (NOT-SERVED-PASS_L))
(not (BOARDED-PASS_L))
)
)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag305prime-))
(not (Flag304prime-))
(not (Flag303prime-))
(not (Flag302prime-))
(not (Flag301prime-))
(not (Flag300prime-))
(not (Flag299prime-))
(not (Flag298prime-))
(not (Flag64prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag58prime-))
(not (Flag54prime-))
(not (Flag47prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag65prime-))
(not (Flag60prime-))
(not (Flag59prime-))
(not (Flag55prime-))
(not (Flag66prime-))
(not (Flag57prime-))
(not (Flag56prime-))
(not (Flag53prime-))
(not (Flag25prime-))
)
)
(:action STOP-PASS_L-B
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-B)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag305prime-))
(not (Flag304prime-))
(not (Flag303prime-))
(not (Flag302prime-))
(not (Flag301prime-))
(not (Flag300prime-))
(not (Flag299prime-))
(not (Flag298prime-))
(not (Flag64prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag58prime-))
(not (Flag54prime-))
(not (Flag47prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag65prime-))
(not (Flag60prime-))
(not (Flag59prime-))
(not (Flag55prime-))
(not (Flag66prime-))
(not (Flag57prime-))
(not (Flag56prime-))
(not (Flag53prime-))
(not (Flag25prime-))
)
)
(:action STOP-PASS_C-J
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-J)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag305prime-))
(not (Flag304prime-))
(not (Flag303prime-))
(not (Flag302prime-))
(not (Flag301prime-))
(not (Flag300prime-))
(not (Flag299prime-))
(not (Flag298prime-))
(not (Flag64prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag58prime-))
(not (Flag54prime-))
(not (Flag47prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag65prime-))
(not (Flag60prime-))
(not (Flag59prime-))
(not (Flag55prime-))
(not (Flag66prime-))
(not (Flag57prime-))
(not (Flag56prime-))
(not (Flag53prime-))
(not (Flag25prime-))
)
)
(:action STOP-PASS_C-K
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-K)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag305prime-))
(not (Flag304prime-))
(not (Flag303prime-))
(not (Flag302prime-))
(not (Flag301prime-))
(not (Flag300prime-))
(not (Flag299prime-))
(not (Flag298prime-))
(not (Flag64prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag58prime-))
(not (Flag54prime-))
(not (Flag47prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag65prime-))
(not (Flag60prime-))
(not (Flag59prime-))
(not (Flag55prime-))
(not (Flag66prime-))
(not (Flag57prime-))
(not (Flag56prime-))
(not (Flag53prime-))
(not (Flag25prime-))
)
)
(:action STOP-PASS_C-I
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-I)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag305prime-))
(not (Flag304prime-))
(not (Flag303prime-))
(not (Flag302prime-))
(not (Flag301prime-))
(not (Flag300prime-))
(not (Flag299prime-))
(not (Flag298prime-))
(not (Flag64prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag58prime-))
(not (Flag54prime-))
(not (Flag47prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag65prime-))
(not (Flag60prime-))
(not (Flag59prime-))
(not (Flag55prime-))
(not (Flag66prime-))
(not (Flag57prime-))
(not (Flag56prime-))
(not (Flag53prime-))
(not (Flag25prime-))
)
)
(:action STOP-PASS_C-B
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-B)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag305prime-))
(not (Flag304prime-))
(not (Flag303prime-))
(not (Flag302prime-))
(not (Flag301prime-))
(not (Flag300prime-))
(not (Flag299prime-))
(not (Flag298prime-))
(not (Flag64prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag58prime-))
(not (Flag54prime-))
(not (Flag47prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag65prime-))
(not (Flag60prime-))
(not (Flag59prime-))
(not (Flag55prime-))
(not (Flag66prime-))
(not (Flag57prime-))
(not (Flag56prime-))
(not (Flag53prime-))
(not (Flag25prime-))
)
)
(:action STOP-PASS_S-J
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-J)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag305prime-))
(not (Flag304prime-))
(not (Flag303prime-))
(not (Flag302prime-))
(not (Flag301prime-))
(not (Flag300prime-))
(not (Flag299prime-))
(not (Flag298prime-))
(not (Flag64prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag58prime-))
(not (Flag54prime-))
(not (Flag47prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag65prime-))
(not (Flag60prime-))
(not (Flag59prime-))
(not (Flag55prime-))
(not (Flag66prime-))
(not (Flag57prime-))
(not (Flag56prime-))
(not (Flag53prime-))
(not (Flag25prime-))
)
)
(:action STOP-PASS_S-K
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-K)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag305prime-))
(not (Flag304prime-))
(not (Flag303prime-))
(not (Flag302prime-))
(not (Flag301prime-))
(not (Flag300prime-))
(not (Flag299prime-))
(not (Flag298prime-))
(not (Flag64prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag58prime-))
(not (Flag54prime-))
(not (Flag47prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag65prime-))
(not (Flag60prime-))
(not (Flag59prime-))
(not (Flag55prime-))
(not (Flag66prime-))
(not (Flag57prime-))
(not (Flag56prime-))
(not (Flag53prime-))
(not (Flag25prime-))
)
)
(:action STOP-PASS_S-I
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-I)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag305prime-))
(not (Flag304prime-))
(not (Flag303prime-))
(not (Flag302prime-))
(not (Flag301prime-))
(not (Flag300prime-))
(not (Flag299prime-))
(not (Flag298prime-))
(not (Flag64prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag58prime-))
(not (Flag54prime-))
(not (Flag47prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag65prime-))
(not (Flag60prime-))
(not (Flag59prime-))
(not (Flag55prime-))
(not (Flag66prime-))
(not (Flag57prime-))
(not (Flag56prime-))
(not (Flag53prime-))
(not (Flag25prime-))
)
)
(:action STOP-PASS_S-B
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-B)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag305prime-))
(not (Flag304prime-))
(not (Flag303prime-))
(not (Flag302prime-))
(not (Flag301prime-))
(not (Flag300prime-))
(not (Flag299prime-))
(not (Flag298prime-))
(not (Flag64prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag58prime-))
(not (Flag54prime-))
(not (Flag47prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag65prime-))
(not (Flag60prime-))
(not (Flag59prime-))
(not (Flag55prime-))
(not (Flag66prime-))
(not (Flag57prime-))
(not (Flag56prime-))
(not (Flag53prime-))
(not (Flag25prime-))
)
)
(:action STOP-PASS_R-J
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-J)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag305prime-))
(not (Flag304prime-))
(not (Flag303prime-))
(not (Flag302prime-))
(not (Flag301prime-))
(not (Flag300prime-))
(not (Flag299prime-))
(not (Flag298prime-))
(not (Flag64prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag58prime-))
(not (Flag54prime-))
(not (Flag47prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag65prime-))
(not (Flag60prime-))
(not (Flag59prime-))
(not (Flag55prime-))
(not (Flag66prime-))
(not (Flag57prime-))
(not (Flag56prime-))
(not (Flag53prime-))
(not (Flag25prime-))
)
)
(:action STOP-PASS_R-K
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-K)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag305prime-))
(not (Flag304prime-))
(not (Flag303prime-))
(not (Flag302prime-))
(not (Flag301prime-))
(not (Flag300prime-))
(not (Flag299prime-))
(not (Flag298prime-))
(not (Flag64prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag58prime-))
(not (Flag54prime-))
(not (Flag47prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag65prime-))
(not (Flag60prime-))
(not (Flag59prime-))
(not (Flag55prime-))
(not (Flag66prime-))
(not (Flag57prime-))
(not (Flag56prime-))
(not (Flag53prime-))
(not (Flag25prime-))
)
)
(:action STOP-PASS_R-I
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-I)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag305prime-))
(not (Flag304prime-))
(not (Flag303prime-))
(not (Flag302prime-))
(not (Flag301prime-))
(not (Flag300prime-))
(not (Flag299prime-))
(not (Flag298prime-))
(not (Flag64prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag58prime-))
(not (Flag54prime-))
(not (Flag47prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag65prime-))
(not (Flag60prime-))
(not (Flag59prime-))
(not (Flag55prime-))
(not (Flag66prime-))
(not (Flag57prime-))
(not (Flag56prime-))
(not (Flag53prime-))
(not (Flag25prime-))
)
)
(:action STOP-PASS_R-B
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-B)
(Flag299prime-)
)
:effect
(and
(when
(and
(Flag299-)
)
(and
(BOARDED-PASS_R)
(not (NOT-BOARDED-PASS_R))
)
)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag305prime-))
(not (Flag304prime-))
(not (Flag303prime-))
(not (Flag302prime-))
(not (Flag301prime-))
(not (Flag300prime-))
(not (Flag299prime-))
(not (Flag298prime-))
(not (Flag64prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag58prime-))
(not (Flag54prime-))
(not (Flag47prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag65prime-))
(not (Flag60prime-))
(not (Flag59prime-))
(not (Flag55prime-))
(not (Flag66prime-))
(not (Flag57prime-))
(not (Flag56prime-))
(not (Flag53prime-))
(not (Flag25prime-))
)
)
(:action STOP-PASS_Q-J
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-J)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag305prime-))
(not (Flag304prime-))
(not (Flag303prime-))
(not (Flag302prime-))
(not (Flag301prime-))
(not (Flag300prime-))
(not (Flag299prime-))
(not (Flag298prime-))
(not (Flag64prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag58prime-))
(not (Flag54prime-))
(not (Flag47prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag65prime-))
(not (Flag60prime-))
(not (Flag59prime-))
(not (Flag55prime-))
(not (Flag66prime-))
(not (Flag57prime-))
(not (Flag56prime-))
(not (Flag53prime-))
(not (Flag25prime-))
)
)
(:action STOP-PASS_Q-K
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-K)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag305prime-))
(not (Flag304prime-))
(not (Flag303prime-))
(not (Flag302prime-))
(not (Flag301prime-))
(not (Flag300prime-))
(not (Flag299prime-))
(not (Flag298prime-))
(not (Flag64prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag58prime-))
(not (Flag54prime-))
(not (Flag47prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag65prime-))
(not (Flag60prime-))
(not (Flag59prime-))
(not (Flag55prime-))
(not (Flag66prime-))
(not (Flag57prime-))
(not (Flag56prime-))
(not (Flag53prime-))
(not (Flag25prime-))
)
)
(:action STOP-PASS_Q-I
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-I)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag305prime-))
(not (Flag304prime-))
(not (Flag303prime-))
(not (Flag302prime-))
(not (Flag301prime-))
(not (Flag300prime-))
(not (Flag299prime-))
(not (Flag298prime-))
(not (Flag64prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag58prime-))
(not (Flag54prime-))
(not (Flag47prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag65prime-))
(not (Flag60prime-))
(not (Flag59prime-))
(not (Flag55prime-))
(not (Flag66prime-))
(not (Flag57prime-))
(not (Flag56prime-))
(not (Flag53prime-))
(not (Flag25prime-))
)
)
(:action STOP-PASS_Q-B
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-B)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag305prime-))
(not (Flag304prime-))
(not (Flag303prime-))
(not (Flag302prime-))
(not (Flag301prime-))
(not (Flag300prime-))
(not (Flag299prime-))
(not (Flag298prime-))
(not (Flag64prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag58prime-))
(not (Flag54prime-))
(not (Flag47prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag65prime-))
(not (Flag60prime-))
(not (Flag59prime-))
(not (Flag55prime-))
(not (Flag66prime-))
(not (Flag57prime-))
(not (Flag56prime-))
(not (Flag53prime-))
(not (Flag25prime-))
)
)
(:action Flag2Action-0
:parameters ()
:precondition
(and
(Flag1-)
(Flag1prime-)
)
:effect
(and
(Flag2-)
(Flag2prime-)
)

)
(:action Flag4Action-0
:parameters ()
:precondition
(and
(Flag3-)
(Flag3prime-)
)
:effect
(and
(Flag4-)
(Flag4prime-)
)

)
(:action Flag6Action-0
:parameters ()
:precondition
(and
(Flag5-)
(Flag5prime-)
)
:effect
(and
(Flag6-)
(Flag6prime-)
)

)
(:action Flag8Action-0
:parameters ()
:precondition
(and
(Flag7-)
(Flag7prime-)
)
:effect
(and
(Flag8-)
(Flag8prime-)
)

)
(:action Flag10Action-0
:parameters ()
:precondition
(and
(Flag9-)
(Flag9prime-)
)
:effect
(and
(Flag10-)
(Flag10prime-)
)

)
(:action Flag12Action-0
:parameters ()
:precondition
(and
(Flag11-)
(Flag11prime-)
)
:effect
(and
(Flag12-)
(Flag12prime-)
)

)
(:action Flag14Action-0
:parameters ()
:precondition
(and
(Flag13-)
(Flag13prime-)
)
:effect
(and
(Flag14-)
(Flag14prime-)
)

)
(:action Flag16Action-0
:parameters ()
:precondition
(and
(Flag15-)
(Flag15prime-)
)
:effect
(and
(Flag16-)
(Flag16prime-)
)

)
(:action Flag18Action-0
:parameters ()
:precondition
(and
(Flag17-)
(Flag17prime-)
)
:effect
(and
(Flag18-)
(Flag18prime-)
)

)
(:action Flag20Action-0
:parameters ()
:precondition
(and
(Flag19-)
(Flag19prime-)
)
:effect
(and
(Flag20-)
(Flag20prime-)
)

)
(:action Flag22Action-0
:parameters ()
:precondition
(and
(Flag21-)
(Flag21prime-)
)
:effect
(and
(Flag22-)
(Flag22prime-)
)

)
(:action Flag24Action-0
:parameters ()
:precondition
(and
(Flag23-)
(Flag23prime-)
)
:effect
(and
(Flag24-)
(Flag24prime-)
)

)
(:action MOVEUP-B-D
:parameters ()
:precondition
(and
(LIFT_AT-B)
(NOT-ERROR-)
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(NOT-LIFT_AT-B)
(LIFT_AT-D)
(CHECKCONSISTENCY-)
(not (LIFT_AT-B))
(not (NOT-LIFT_AT-D))
(not (NOT-CHECKCONSISTENCY-))
(not (Flag305prime-))
(not (Flag304prime-))
(not (Flag303prime-))
(not (Flag302prime-))
(not (Flag301prime-))
(not (Flag300prime-))
(not (Flag299prime-))
(not (Flag298prime-))
(not (Flag64prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag58prime-))
(not (Flag54prime-))
(not (Flag47prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag65prime-))
(not (Flag60prime-))
(not (Flag59prime-))
(not (Flag55prime-))
(not (Flag66prime-))
(not (Flag57prime-))
(not (Flag56prime-))
(not (Flag53prime-))
(not (Flag25prime-))
)
)
(:action MOVEUP-J-K
:parameters ()
:precondition
(and
(LIFT_AT-J)
(NOT-ERROR-)
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(NOT-LIFT_AT-J)
(LIFT_AT-K)
(CHECKCONSISTENCY-)
(not (LIFT_AT-J))
(not (NOT-LIFT_AT-K))
(not (NOT-CHECKCONSISTENCY-))
(not (Flag305prime-))
(not (Flag304prime-))
(not (Flag303prime-))
(not (Flag302prime-))
(not (Flag301prime-))
(not (Flag300prime-))
(not (Flag299prime-))
(not (Flag298prime-))
(not (Flag64prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag58prime-))
(not (Flag54prime-))
(not (Flag47prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag65prime-))
(not (Flag60prime-))
(not (Flag59prime-))
(not (Flag55prime-))
(not (Flag66prime-))
(not (Flag57prime-))
(not (Flag56prime-))
(not (Flag53prime-))
(not (Flag25prime-))
)
)
(:action MOVEUP-I-J
:parameters ()
:precondition
(and
(LIFT_AT-I)
(NOT-ERROR-)
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(NOT-LIFT_AT-I)
(LIFT_AT-J)
(CHECKCONSISTENCY-)
(not (LIFT_AT-I))
(not (NOT-LIFT_AT-J))
(not (NOT-CHECKCONSISTENCY-))
(not (Flag305prime-))
(not (Flag304prime-))
(not (Flag303prime-))
(not (Flag302prime-))
(not (Flag301prime-))
(not (Flag300prime-))
(not (Flag299prime-))
(not (Flag298prime-))
(not (Flag64prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag58prime-))
(not (Flag54prime-))
(not (Flag47prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag65prime-))
(not (Flag60prime-))
(not (Flag59prime-))
(not (Flag55prime-))
(not (Flag66prime-))
(not (Flag57prime-))
(not (Flag56prime-))
(not (Flag53prime-))
(not (Flag25prime-))
)
)
(:action MOVEUP-G-I
:parameters ()
:precondition
(and
(LIFT_AT-G)
(NOT-ERROR-)
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(NOT-LIFT_AT-G)
(LIFT_AT-I)
(CHECKCONSISTENCY-)
(not (LIFT_AT-G))
(not (NOT-LIFT_AT-I))
(not (NOT-CHECKCONSISTENCY-))
(not (Flag305prime-))
(not (Flag304prime-))
(not (Flag303prime-))
(not (Flag302prime-))
(not (Flag301prime-))
(not (Flag300prime-))
(not (Flag299prime-))
(not (Flag298prime-))
(not (Flag64prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag58prime-))
(not (Flag54prime-))
(not (Flag47prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag65prime-))
(not (Flag60prime-))
(not (Flag59prime-))
(not (Flag55prime-))
(not (Flag66prime-))
(not (Flag57prime-))
(not (Flag56prime-))
(not (Flag53prime-))
(not (Flag25prime-))
)
)
(:action MOVEDOWN-G-F
:parameters ()
:precondition
(and
(LIFT_AT-G)
(NOT-ERROR-)
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(NOT-LIFT_AT-G)
(LIFT_AT-F)
(CHECKCONSISTENCY-)
(not (LIFT_AT-G))
(not (NOT-LIFT_AT-F))
(not (NOT-CHECKCONSISTENCY-))
(not (Flag305prime-))
(not (Flag304prime-))
(not (Flag303prime-))
(not (Flag302prime-))
(not (Flag301prime-))
(not (Flag300prime-))
(not (Flag299prime-))
(not (Flag298prime-))
(not (Flag64prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag58prime-))
(not (Flag54prime-))
(not (Flag47prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag65prime-))
(not (Flag60prime-))
(not (Flag59prime-))
(not (Flag55prime-))
(not (Flag66prime-))
(not (Flag57prime-))
(not (Flag56prime-))
(not (Flag53prime-))
(not (Flag25prime-))
)
)
(:action MOVEDOWN-D-B
:parameters ()
:precondition
(and
(LIFT_AT-D)
(NOT-ERROR-)
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(NOT-LIFT_AT-D)
(LIFT_AT-B)
(CHECKCONSISTENCY-)
(not (LIFT_AT-D))
(not (NOT-LIFT_AT-B))
(not (NOT-CHECKCONSISTENCY-))
(not (Flag305prime-))
(not (Flag304prime-))
(not (Flag303prime-))
(not (Flag302prime-))
(not (Flag301prime-))
(not (Flag300prime-))
(not (Flag299prime-))
(not (Flag298prime-))
(not (Flag64prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag58prime-))
(not (Flag54prime-))
(not (Flag47prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag65prime-))
(not (Flag60prime-))
(not (Flag59prime-))
(not (Flag55prime-))
(not (Flag66prime-))
(not (Flag57prime-))
(not (Flag56prime-))
(not (Flag53prime-))
(not (Flag25prime-))
)
)
(:action MOVEDOWN-F-E
:parameters ()
:precondition
(and
(LIFT_AT-F)
(NOT-ERROR-)
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(NOT-LIFT_AT-F)
(LIFT_AT-E)
(CHECKCONSISTENCY-)
(not (LIFT_AT-F))
(not (NOT-LIFT_AT-E))
(not (NOT-CHECKCONSISTENCY-))
(not (Flag305prime-))
(not (Flag304prime-))
(not (Flag303prime-))
(not (Flag302prime-))
(not (Flag301prime-))
(not (Flag300prime-))
(not (Flag299prime-))
(not (Flag298prime-))
(not (Flag64prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag58prime-))
(not (Flag54prime-))
(not (Flag47prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag65prime-))
(not (Flag60prime-))
(not (Flag59prime-))
(not (Flag55prime-))
(not (Flag66prime-))
(not (Flag57prime-))
(not (Flag56prime-))
(not (Flag53prime-))
(not (Flag25prime-))
)
)
(:action Prim27Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-D))
)
:effect
(and
)

)
(:action Prim34Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-G))
)
:effect
(and
)

)
(:action Prim38Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-F))
)
:effect
(and
)

)
(:action Flag47Action
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-D)
)
:effect
(and
(Flag47-)
(Flag47prime-)
)

)
(:action Prim47Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-D))
)
:effect
(and
(Flag47prime-)
(not (Flag47-))
)

)
(:action Flag54Action
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-G)
)
:effect
(and
(Flag54-)
(Flag54prime-)
)

)
(:action Prim54Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-G))
)
:effect
(and
(Flag54prime-)
(not (Flag54-))
)

)
(:action Flag58Action
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-F)
)
:effect
(and
(Flag58-)
(Flag58prime-)
)

)
(:action Prim58Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-F))
)
:effect
(and
(Flag58prime-)
(not (Flag58-))
)

)
(:action Prim68Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-D))
)
:effect
(and
)

)
(:action Prim75Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-G))
)
:effect
(and
)

)
(:action Prim79Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-F))
)
:effect
(and
)

)
(:action Prim89Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-D))
)
:effect
(and
)

)
(:action Prim96Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-G))
)
:effect
(and
)

)
(:action Prim100Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-F))
)
:effect
(and
)

)
(:action Prim110Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-D))
)
:effect
(and
)

)
(:action Prim117Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-G))
)
:effect
(and
)

)
(:action Prim121Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-F))
)
:effect
(and
)

)
(:action Prim131Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-D))
)
:effect
(and
)

)
(:action Prim138Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-G))
)
:effect
(and
)

)
(:action Prim142Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-F))
)
:effect
(and
)

)
(:action Prim152Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-D))
)
:effect
(and
)

)
(:action Prim159Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-G))
)
:effect
(and
)

)
(:action Prim163Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-F))
)
:effect
(and
)

)
(:action Prim173Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-D))
)
:effect
(and
)

)
(:action Prim180Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-G))
)
:effect
(and
)

)
(:action Prim184Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-F))
)
:effect
(and
)

)
(:action Prim194Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-D))
)
:effect
(and
)

)
(:action Prim201Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-G))
)
:effect
(and
)

)
(:action Prim205Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-F))
)
:effect
(and
)

)
(:action Prim215Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-D))
)
:effect
(and
)

)
(:action Prim222Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-G))
)
:effect
(and
)

)
(:action Prim226Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-F))
)
:effect
(and
)

)
(:action Prim236Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-D))
)
:effect
(and
)

)
(:action Prim243Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-G))
)
:effect
(and
)

)
(:action Prim247Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-F))
)
:effect
(and
)

)
(:action Prim257Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-D))
)
:effect
(and
)

)
(:action Prim264Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-G))
)
:effect
(and
)

)
(:action Prim268Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-F))
)
:effect
(and
)

)
(:action Prim278Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-D))
)
:effect
(and
)

)
(:action Prim285Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-G))
)
:effect
(and
)

)
(:action Prim289Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-F))
)
:effect
(and
)

)
(:action STOP-PASS_H-E
:parameters ()
:precondition
(and
(Flag64-)
(Flag64prime-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag305prime-))
(not (Flag304prime-))
(not (Flag303prime-))
(not (Flag302prime-))
(not (Flag301prime-))
(not (Flag300prime-))
(not (Flag299prime-))
(not (Flag298prime-))
(not (Flag64prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag58prime-))
(not (Flag54prime-))
(not (Flag47prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag65prime-))
(not (Flag60prime-))
(not (Flag59prime-))
(not (Flag55prime-))
(not (Flag66prime-))
(not (Flag57prime-))
(not (Flag56prime-))
(not (Flag53prime-))
(not (Flag25prime-))
)
)
(:action STOP-PASS_N-D
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-D)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag305prime-))
(not (Flag304prime-))
(not (Flag303prime-))
(not (Flag302prime-))
(not (Flag301prime-))
(not (Flag300prime-))
(not (Flag299prime-))
(not (Flag298prime-))
(not (Flag64prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag58prime-))
(not (Flag54prime-))
(not (Flag47prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag65prime-))
(not (Flag60prime-))
(not (Flag59prime-))
(not (Flag55prime-))
(not (Flag66prime-))
(not (Flag57prime-))
(not (Flag56prime-))
(not (Flag53prime-))
(not (Flag25prime-))
)
)
(:action STOP-PASS_N-G
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-G)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag305prime-))
(not (Flag304prime-))
(not (Flag303prime-))
(not (Flag302prime-))
(not (Flag301prime-))
(not (Flag300prime-))
(not (Flag299prime-))
(not (Flag298prime-))
(not (Flag64prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag58prime-))
(not (Flag54prime-))
(not (Flag47prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag65prime-))
(not (Flag60prime-))
(not (Flag59prime-))
(not (Flag55prime-))
(not (Flag66prime-))
(not (Flag57prime-))
(not (Flag56prime-))
(not (Flag53prime-))
(not (Flag25prime-))
)
)
(:action STOP-PASS_N-F
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-F)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag305prime-))
(not (Flag304prime-))
(not (Flag303prime-))
(not (Flag302prime-))
(not (Flag301prime-))
(not (Flag300prime-))
(not (Flag299prime-))
(not (Flag298prime-))
(not (Flag64prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag58prime-))
(not (Flag54prime-))
(not (Flag47prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag65prime-))
(not (Flag60prime-))
(not (Flag59prime-))
(not (Flag55prime-))
(not (Flag66prime-))
(not (Flag57prime-))
(not (Flag56prime-))
(not (Flag53prime-))
(not (Flag25prime-))
)
)
(:action STOP-PASS_N-E
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-E)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag305prime-))
(not (Flag304prime-))
(not (Flag303prime-))
(not (Flag302prime-))
(not (Flag301prime-))
(not (Flag300prime-))
(not (Flag299prime-))
(not (Flag298prime-))
(not (Flag64prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag58prime-))
(not (Flag54prime-))
(not (Flag47prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag65prime-))
(not (Flag60prime-))
(not (Flag59prime-))
(not (Flag55prime-))
(not (Flag66prime-))
(not (Flag57prime-))
(not (Flag56prime-))
(not (Flag53prime-))
(not (Flag25prime-))
)
)
(:action STOP-PASS_M-D
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-D)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag305prime-))
(not (Flag304prime-))
(not (Flag303prime-))
(not (Flag302prime-))
(not (Flag301prime-))
(not (Flag300prime-))
(not (Flag299prime-))
(not (Flag298prime-))
(not (Flag64prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag58prime-))
(not (Flag54prime-))
(not (Flag47prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag65prime-))
(not (Flag60prime-))
(not (Flag59prime-))
(not (Flag55prime-))
(not (Flag66prime-))
(not (Flag57prime-))
(not (Flag56prime-))
(not (Flag53prime-))
(not (Flag25prime-))
)
)
(:action STOP-PASS_M-G
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-G)
(Flag303prime-)
)
:effect
(and
(when
(and
(Flag303-)
)
(and
(BOARDED-PASS_M)
(not (NOT-BOARDED-PASS_M))
)
)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag305prime-))
(not (Flag304prime-))
(not (Flag303prime-))
(not (Flag302prime-))
(not (Flag301prime-))
(not (Flag300prime-))
(not (Flag299prime-))
(not (Flag298prime-))
(not (Flag64prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag58prime-))
(not (Flag54prime-))
(not (Flag47prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag65prime-))
(not (Flag60prime-))
(not (Flag59prime-))
(not (Flag55prime-))
(not (Flag66prime-))
(not (Flag57prime-))
(not (Flag56prime-))
(not (Flag53prime-))
(not (Flag25prime-))
)
)
(:action STOP-PASS_M-F
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-F)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag305prime-))
(not (Flag304prime-))
(not (Flag303prime-))
(not (Flag302prime-))
(not (Flag301prime-))
(not (Flag300prime-))
(not (Flag299prime-))
(not (Flag298prime-))
(not (Flag64prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag58prime-))
(not (Flag54prime-))
(not (Flag47prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag65prime-))
(not (Flag60prime-))
(not (Flag59prime-))
(not (Flag55prime-))
(not (Flag66prime-))
(not (Flag57prime-))
(not (Flag56prime-))
(not (Flag53prime-))
(not (Flag25prime-))
)
)
(:action STOP-PASS_M-E
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-E)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag305prime-))
(not (Flag304prime-))
(not (Flag303prime-))
(not (Flag302prime-))
(not (Flag301prime-))
(not (Flag300prime-))
(not (Flag299prime-))
(not (Flag298prime-))
(not (Flag64prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag58prime-))
(not (Flag54prime-))
(not (Flag47prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag65prime-))
(not (Flag60prime-))
(not (Flag59prime-))
(not (Flag55prime-))
(not (Flag66prime-))
(not (Flag57prime-))
(not (Flag56prime-))
(not (Flag53prime-))
(not (Flag25prime-))
)
)
(:action STOP-PASS_L-D
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-D)
(Flag302prime-)
)
:effect
(and
(when
(and
(Flag302-)
)
(and
(BOARDED-PASS_L)
(not (NOT-BOARDED-PASS_L))
)
)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag305prime-))
(not (Flag304prime-))
(not (Flag303prime-))
(not (Flag302prime-))
(not (Flag301prime-))
(not (Flag300prime-))
(not (Flag299prime-))
(not (Flag298prime-))
(not (Flag64prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag58prime-))
(not (Flag54prime-))
(not (Flag47prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag65prime-))
(not (Flag60prime-))
(not (Flag59prime-))
(not (Flag55prime-))
(not (Flag66prime-))
(not (Flag57prime-))
(not (Flag56prime-))
(not (Flag53prime-))
(not (Flag25prime-))
)
)
(:action STOP-PASS_L-G
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-G)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag305prime-))
(not (Flag304prime-))
(not (Flag303prime-))
(not (Flag302prime-))
(not (Flag301prime-))
(not (Flag300prime-))
(not (Flag299prime-))
(not (Flag298prime-))
(not (Flag64prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag58prime-))
(not (Flag54prime-))
(not (Flag47prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag65prime-))
(not (Flag60prime-))
(not (Flag59prime-))
(not (Flag55prime-))
(not (Flag66prime-))
(not (Flag57prime-))
(not (Flag56prime-))
(not (Flag53prime-))
(not (Flag25prime-))
)
)
(:action STOP-PASS_L-F
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-F)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag305prime-))
(not (Flag304prime-))
(not (Flag303prime-))
(not (Flag302prime-))
(not (Flag301prime-))
(not (Flag300prime-))
(not (Flag299prime-))
(not (Flag298prime-))
(not (Flag64prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag58prime-))
(not (Flag54prime-))
(not (Flag47prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag65prime-))
(not (Flag60prime-))
(not (Flag59prime-))
(not (Flag55prime-))
(not (Flag66prime-))
(not (Flag57prime-))
(not (Flag56prime-))
(not (Flag53prime-))
(not (Flag25prime-))
)
)
(:action STOP-PASS_L-E
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-E)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag305prime-))
(not (Flag304prime-))
(not (Flag303prime-))
(not (Flag302prime-))
(not (Flag301prime-))
(not (Flag300prime-))
(not (Flag299prime-))
(not (Flag298prime-))
(not (Flag64prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag58prime-))
(not (Flag54prime-))
(not (Flag47prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag65prime-))
(not (Flag60prime-))
(not (Flag59prime-))
(not (Flag55prime-))
(not (Flag66prime-))
(not (Flag57prime-))
(not (Flag56prime-))
(not (Flag53prime-))
(not (Flag25prime-))
)
)
(:action STOP-PASS_C-D
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-D)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag305prime-))
(not (Flag304prime-))
(not (Flag303prime-))
(not (Flag302prime-))
(not (Flag301prime-))
(not (Flag300prime-))
(not (Flag299prime-))
(not (Flag298prime-))
(not (Flag64prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag58prime-))
(not (Flag54prime-))
(not (Flag47prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag65prime-))
(not (Flag60prime-))
(not (Flag59prime-))
(not (Flag55prime-))
(not (Flag66prime-))
(not (Flag57prime-))
(not (Flag56prime-))
(not (Flag53prime-))
(not (Flag25prime-))
)
)
(:action STOP-PASS_C-G
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-G)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag305prime-))
(not (Flag304prime-))
(not (Flag303prime-))
(not (Flag302prime-))
(not (Flag301prime-))
(not (Flag300prime-))
(not (Flag299prime-))
(not (Flag298prime-))
(not (Flag64prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag58prime-))
(not (Flag54prime-))
(not (Flag47prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag65prime-))
(not (Flag60prime-))
(not (Flag59prime-))
(not (Flag55prime-))
(not (Flag66prime-))
(not (Flag57prime-))
(not (Flag56prime-))
(not (Flag53prime-))
(not (Flag25prime-))
)
)
(:action STOP-PASS_C-F
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-F)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag305prime-))
(not (Flag304prime-))
(not (Flag303prime-))
(not (Flag302prime-))
(not (Flag301prime-))
(not (Flag300prime-))
(not (Flag299prime-))
(not (Flag298prime-))
(not (Flag64prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag58prime-))
(not (Flag54prime-))
(not (Flag47prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag65prime-))
(not (Flag60prime-))
(not (Flag59prime-))
(not (Flag55prime-))
(not (Flag66prime-))
(not (Flag57prime-))
(not (Flag56prime-))
(not (Flag53prime-))
(not (Flag25prime-))
)
)
(:action STOP-PASS_C-E
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-E)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag305prime-))
(not (Flag304prime-))
(not (Flag303prime-))
(not (Flag302prime-))
(not (Flag301prime-))
(not (Flag300prime-))
(not (Flag299prime-))
(not (Flag298prime-))
(not (Flag64prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag58prime-))
(not (Flag54prime-))
(not (Flag47prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag65prime-))
(not (Flag60prime-))
(not (Flag59prime-))
(not (Flag55prime-))
(not (Flag66prime-))
(not (Flag57prime-))
(not (Flag56prime-))
(not (Flag53prime-))
(not (Flag25prime-))
)
)
(:action STOP-PASS_S-D
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-D)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag305prime-))
(not (Flag304prime-))
(not (Flag303prime-))
(not (Flag302prime-))
(not (Flag301prime-))
(not (Flag300prime-))
(not (Flag299prime-))
(not (Flag298prime-))
(not (Flag64prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag58prime-))
(not (Flag54prime-))
(not (Flag47prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag65prime-))
(not (Flag60prime-))
(not (Flag59prime-))
(not (Flag55prime-))
(not (Flag66prime-))
(not (Flag57prime-))
(not (Flag56prime-))
(not (Flag53prime-))
(not (Flag25prime-))
)
)
(:action STOP-PASS_S-G
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-G)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag305prime-))
(not (Flag304prime-))
(not (Flag303prime-))
(not (Flag302prime-))
(not (Flag301prime-))
(not (Flag300prime-))
(not (Flag299prime-))
(not (Flag298prime-))
(not (Flag64prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag58prime-))
(not (Flag54prime-))
(not (Flag47prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag65prime-))
(not (Flag60prime-))
(not (Flag59prime-))
(not (Flag55prime-))
(not (Flag66prime-))
(not (Flag57prime-))
(not (Flag56prime-))
(not (Flag53prime-))
(not (Flag25prime-))
)
)
(:action STOP-PASS_S-F
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-F)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag305prime-))
(not (Flag304prime-))
(not (Flag303prime-))
(not (Flag302prime-))
(not (Flag301prime-))
(not (Flag300prime-))
(not (Flag299prime-))
(not (Flag298prime-))
(not (Flag64prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag58prime-))
(not (Flag54prime-))
(not (Flag47prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag65prime-))
(not (Flag60prime-))
(not (Flag59prime-))
(not (Flag55prime-))
(not (Flag66prime-))
(not (Flag57prime-))
(not (Flag56prime-))
(not (Flag53prime-))
(not (Flag25prime-))
)
)
(:action STOP-PASS_S-E
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-E)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag305prime-))
(not (Flag304prime-))
(not (Flag303prime-))
(not (Flag302prime-))
(not (Flag301prime-))
(not (Flag300prime-))
(not (Flag299prime-))
(not (Flag298prime-))
(not (Flag64prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag58prime-))
(not (Flag54prime-))
(not (Flag47prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag65prime-))
(not (Flag60prime-))
(not (Flag59prime-))
(not (Flag55prime-))
(not (Flag66prime-))
(not (Flag57prime-))
(not (Flag56prime-))
(not (Flag53prime-))
(not (Flag25prime-))
)
)
(:action STOP-PASS_R-D
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-D)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag305prime-))
(not (Flag304prime-))
(not (Flag303prime-))
(not (Flag302prime-))
(not (Flag301prime-))
(not (Flag300prime-))
(not (Flag299prime-))
(not (Flag298prime-))
(not (Flag64prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag58prime-))
(not (Flag54prime-))
(not (Flag47prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag65prime-))
(not (Flag60prime-))
(not (Flag59prime-))
(not (Flag55prime-))
(not (Flag66prime-))
(not (Flag57prime-))
(not (Flag56prime-))
(not (Flag53prime-))
(not (Flag25prime-))
)
)
(:action STOP-PASS_R-G
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-G)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag305prime-))
(not (Flag304prime-))
(not (Flag303prime-))
(not (Flag302prime-))
(not (Flag301prime-))
(not (Flag300prime-))
(not (Flag299prime-))
(not (Flag298prime-))
(not (Flag64prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag58prime-))
(not (Flag54prime-))
(not (Flag47prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag65prime-))
(not (Flag60prime-))
(not (Flag59prime-))
(not (Flag55prime-))
(not (Flag66prime-))
(not (Flag57prime-))
(not (Flag56prime-))
(not (Flag53prime-))
(not (Flag25prime-))
)
)
(:action STOP-PASS_R-F
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-F)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag305prime-))
(not (Flag304prime-))
(not (Flag303prime-))
(not (Flag302prime-))
(not (Flag301prime-))
(not (Flag300prime-))
(not (Flag299prime-))
(not (Flag298prime-))
(not (Flag64prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag58prime-))
(not (Flag54prime-))
(not (Flag47prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag65prime-))
(not (Flag60prime-))
(not (Flag59prime-))
(not (Flag55prime-))
(not (Flag66prime-))
(not (Flag57prime-))
(not (Flag56prime-))
(not (Flag53prime-))
(not (Flag25prime-))
)
)
(:action STOP-PASS_R-E
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-E)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag305prime-))
(not (Flag304prime-))
(not (Flag303prime-))
(not (Flag302prime-))
(not (Flag301prime-))
(not (Flag300prime-))
(not (Flag299prime-))
(not (Flag298prime-))
(not (Flag64prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag58prime-))
(not (Flag54prime-))
(not (Flag47prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag65prime-))
(not (Flag60prime-))
(not (Flag59prime-))
(not (Flag55prime-))
(not (Flag66prime-))
(not (Flag57prime-))
(not (Flag56prime-))
(not (Flag53prime-))
(not (Flag25prime-))
)
)
(:action STOP-PASS_Q-D
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-D)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag305prime-))
(not (Flag304prime-))
(not (Flag303prime-))
(not (Flag302prime-))
(not (Flag301prime-))
(not (Flag300prime-))
(not (Flag299prime-))
(not (Flag298prime-))
(not (Flag64prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag58prime-))
(not (Flag54prime-))
(not (Flag47prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag65prime-))
(not (Flag60prime-))
(not (Flag59prime-))
(not (Flag55prime-))
(not (Flag66prime-))
(not (Flag57prime-))
(not (Flag56prime-))
(not (Flag53prime-))
(not (Flag25prime-))
)
)
(:action STOP-PASS_Q-G
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-G)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag305prime-))
(not (Flag304prime-))
(not (Flag303prime-))
(not (Flag302prime-))
(not (Flag301prime-))
(not (Flag300prime-))
(not (Flag299prime-))
(not (Flag298prime-))
(not (Flag64prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag58prime-))
(not (Flag54prime-))
(not (Flag47prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag65prime-))
(not (Flag60prime-))
(not (Flag59prime-))
(not (Flag55prime-))
(not (Flag66prime-))
(not (Flag57prime-))
(not (Flag56prime-))
(not (Flag53prime-))
(not (Flag25prime-))
)
)
(:action STOP-PASS_Q-F
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-F)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag305prime-))
(not (Flag304prime-))
(not (Flag303prime-))
(not (Flag302prime-))
(not (Flag301prime-))
(not (Flag300prime-))
(not (Flag299prime-))
(not (Flag298prime-))
(not (Flag64prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag58prime-))
(not (Flag54prime-))
(not (Flag47prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag65prime-))
(not (Flag60prime-))
(not (Flag59prime-))
(not (Flag55prime-))
(not (Flag66prime-))
(not (Flag57prime-))
(not (Flag56prime-))
(not (Flag53prime-))
(not (Flag25prime-))
)
)
(:action STOP-PASS_Q-E
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-E)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag305prime-))
(not (Flag304prime-))
(not (Flag303prime-))
(not (Flag302prime-))
(not (Flag301prime-))
(not (Flag300prime-))
(not (Flag299prime-))
(not (Flag298prime-))
(not (Flag64prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag58prime-))
(not (Flag54prime-))
(not (Flag47prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag65prime-))
(not (Flag60prime-))
(not (Flag59prime-))
(not (Flag55prime-))
(not (Flag66prime-))
(not (Flag57prime-))
(not (Flag56prime-))
(not (Flag53prime-))
(not (Flag25prime-))
)
)
(:action Flag1Action
:parameters ()
:effect
(and
(Flag1-)
(Flag1prime-)
)

)
(:action Flag3Action
:parameters ()
:effect
(and
(Flag3-)
(Flag3prime-)
)

)
(:action Flag5Action
:parameters ()
:effect
(and
(Flag5-)
(Flag5prime-)
)

)
(:action Flag7Action
:parameters ()
:effect
(and
(Flag7-)
(Flag7prime-)
)

)
(:action Flag9Action
:parameters ()
:effect
(and
(Flag9-)
(Flag9prime-)
)

)
(:action Flag11Action
:parameters ()
:effect
(and
(Flag11-)
(Flag11prime-)
)

)
(:action Flag13Action
:parameters ()
:effect
(and
(Flag13-)
(Flag13prime-)
)

)
(:action Flag15Action
:parameters ()
:effect
(and
(Flag15-)
(Flag15prime-)
)

)
(:action Flag17Action
:parameters ()
:effect
(and
(Flag17-)
(Flag17prime-)
)

)
(:action Flag19Action
:parameters ()
:effect
(and
(Flag19-)
(Flag19prime-)
)

)
(:action Flag21Action
:parameters ()
:effect
(and
(Flag21-)
(Flag21prime-)
)

)
(:action Flag23Action
:parameters ()
:effect
(and
(Flag23-)
(Flag23prime-)
)

)
(:action Prim25Action-20
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
(Flag25prime-)
(not (Flag25-))
)

)
(:action Prim25Action-21
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
(Flag25prime-)
(not (Flag25-))
)

)
(:action CHECKCONSISTENCYACTION
:parameters ()
:precondition
(and
(NOT-ERROR-)
(CHECKCONSISTENCY-)
)
:effect
(and
(NOT-CHECKCONSISTENCY-)
(not (CHECKCONSISTENCY-))
(not (Flag305prime-))
(not (Flag304prime-))
(not (Flag303prime-))
(not (Flag302prime-))
(not (Flag301prime-))
(not (Flag300prime-))
(not (Flag299prime-))
(not (Flag298prime-))
(not (Flag64prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag58prime-))
(not (Flag54prime-))
(not (Flag47prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag65prime-))
(not (Flag60prime-))
(not (Flag59prime-))
(not (Flag55prime-))
(not (Flag66prime-))
(not (Flag57prime-))
(not (Flag56prime-))
(not (Flag53prime-))
(not (Flag25prime-))
)
)
(:action MOVEUP-F-G
:parameters ()
:precondition
(and
(LIFT_AT-F)
(NOT-ERROR-)
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(NOT-LIFT_AT-F)
(LIFT_AT-G)
(CHECKCONSISTENCY-)
(not (LIFT_AT-F))
(not (NOT-LIFT_AT-G))
(not (NOT-CHECKCONSISTENCY-))
(not (Flag305prime-))
(not (Flag304prime-))
(not (Flag303prime-))
(not (Flag302prime-))
(not (Flag301prime-))
(not (Flag300prime-))
(not (Flag299prime-))
(not (Flag298prime-))
(not (Flag64prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag58prime-))
(not (Flag54prime-))
(not (Flag47prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag65prime-))
(not (Flag60prime-))
(not (Flag59prime-))
(not (Flag55prime-))
(not (Flag66prime-))
(not (Flag57prime-))
(not (Flag56prime-))
(not (Flag53prime-))
(not (Flag25prime-))
)
)
(:action MOVEUP-D-E
:parameters ()
:precondition
(and
(LIFT_AT-D)
(NOT-ERROR-)
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(NOT-LIFT_AT-D)
(LIFT_AT-E)
(CHECKCONSISTENCY-)
(not (LIFT_AT-D))
(not (NOT-LIFT_AT-E))
(not (NOT-CHECKCONSISTENCY-))
(not (Flag305prime-))
(not (Flag304prime-))
(not (Flag303prime-))
(not (Flag302prime-))
(not (Flag301prime-))
(not (Flag300prime-))
(not (Flag299prime-))
(not (Flag298prime-))
(not (Flag64prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag58prime-))
(not (Flag54prime-))
(not (Flag47prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag65prime-))
(not (Flag60prime-))
(not (Flag59prime-))
(not (Flag55prime-))
(not (Flag66prime-))
(not (Flag57prime-))
(not (Flag56prime-))
(not (Flag53prime-))
(not (Flag25prime-))
)
)
(:action MOVEUP-E-F
:parameters ()
:precondition
(and
(LIFT_AT-E)
(NOT-ERROR-)
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(NOT-LIFT_AT-E)
(LIFT_AT-F)
(CHECKCONSISTENCY-)
(not (LIFT_AT-E))
(not (NOT-LIFT_AT-F))
(not (NOT-CHECKCONSISTENCY-))
(not (Flag305prime-))
(not (Flag304prime-))
(not (Flag303prime-))
(not (Flag302prime-))
(not (Flag301prime-))
(not (Flag300prime-))
(not (Flag299prime-))
(not (Flag298prime-))
(not (Flag64prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag58prime-))
(not (Flag54prime-))
(not (Flag47prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag65prime-))
(not (Flag60prime-))
(not (Flag59prime-))
(not (Flag55prime-))
(not (Flag66prime-))
(not (Flag57prime-))
(not (Flag56prime-))
(not (Flag53prime-))
(not (Flag25prime-))
)
)
(:action MOVEDOWN-E-D
:parameters ()
:precondition
(and
(LIFT_AT-E)
(NOT-ERROR-)
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(NOT-LIFT_AT-E)
(LIFT_AT-D)
(CHECKCONSISTENCY-)
(not (LIFT_AT-E))
(not (NOT-LIFT_AT-D))
(not (NOT-CHECKCONSISTENCY-))
(not (Flag305prime-))
(not (Flag304prime-))
(not (Flag303prime-))
(not (Flag302prime-))
(not (Flag301prime-))
(not (Flag300prime-))
(not (Flag299prime-))
(not (Flag298prime-))
(not (Flag64prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag58prime-))
(not (Flag54prime-))
(not (Flag47prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag65prime-))
(not (Flag60prime-))
(not (Flag59prime-))
(not (Flag55prime-))
(not (Flag66prime-))
(not (Flag57prime-))
(not (Flag56prime-))
(not (Flag53prime-))
(not (Flag25prime-))
)
)
(:action Prim27Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim27Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim28Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim28Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim29Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim29Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim30Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim30Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim31Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim31Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim32Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim32Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim33Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim33Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim34Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim34Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim35Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim35Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim36Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim36Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim37Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim37Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim38Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim38Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim39Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim39Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim40Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim40Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim41Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim41Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim42Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim42Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim43Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim43Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim44Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim44Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim44Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-E))
)
:effect
(and
)

)
(:action Prim45Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim45Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim46Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim46Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim47Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
(Flag47prime-)
(not (Flag47-))
)

)
(:action Prim47Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
(Flag47prime-)
(not (Flag47-))
)

)
(:action Prim48Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim48Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim49Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim49Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim50Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim50Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim51Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim51Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim52Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim52Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim53Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
(Flag53prime-)
(not (Flag53-))
)

)
(:action Prim53Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
(Flag53prime-)
(not (Flag53-))
)

)
(:action Prim54Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
(Flag54prime-)
(not (Flag54-))
)

)
(:action Prim54Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
(Flag54prime-)
(not (Flag54-))
)

)
(:action Prim55Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
(Flag55prime-)
(not (Flag55-))
)

)
(:action Prim55Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
(Flag55prime-)
(not (Flag55-))
)

)
(:action Prim56Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
(Flag56prime-)
(not (Flag56-))
)

)
(:action Prim56Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
(Flag56prime-)
(not (Flag56-))
)

)
(:action Prim57Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
(Flag57prime-)
(not (Flag57-))
)

)
(:action Prim57Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
(Flag57prime-)
(not (Flag57-))
)

)
(:action Prim58Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
(Flag58prime-)
(not (Flag58-))
)

)
(:action Prim58Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
(Flag58prime-)
(not (Flag58-))
)

)
(:action Prim59Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
(Flag59prime-)
(not (Flag59-))
)

)
(:action Prim59Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
(Flag59prime-)
(not (Flag59-))
)

)
(:action Prim60Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
(Flag60prime-)
(not (Flag60-))
)

)
(:action Prim60Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
(Flag60prime-)
(not (Flag60-))
)

)
(:action Prim61Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim61Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim62Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim62Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim63Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim63Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Flag64Action
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-E)
)
:effect
(and
(Flag64-)
(Flag64prime-)
)

)
(:action Prim64Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
(Flag64prime-)
(not (Flag64-))
)

)
(:action Prim64Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
(Flag64prime-)
(not (Flag64-))
)

)
(:action Prim64Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-E))
)
:effect
(and
(Flag64prime-)
(not (Flag64-))
)

)
(:action Prim65Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
(Flag65prime-)
(not (Flag65-))
)

)
(:action Prim65Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
(Flag65prime-)
(not (Flag65-))
)

)
(:action Prim66Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
(Flag66prime-)
(not (Flag66-))
)

)
(:action Prim66Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
(Flag66prime-)
(not (Flag66-))
)

)
(:action Prim68Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim68Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim69Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim69Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim70Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim70Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim71Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim71Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim72Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim72Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim73Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim73Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim74Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim74Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim75Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim75Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim76Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim76Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim77Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim77Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim78Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim78Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim79Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim79Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim80Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim80Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim81Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim81Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim82Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim82Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim83Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim83Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim84Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim84Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim85Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim85Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim85Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-E))
)
:effect
(and
)

)
(:action Prim86Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim86Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim87Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim87Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim89Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim89Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim90Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim90Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim91Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim91Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim92Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim92Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim93Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim93Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim94Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim94Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim95Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim95Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim96Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim96Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim97Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim97Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim98Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim98Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim99Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim99Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim100Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim100Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim101Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim101Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim102Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim102Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim103Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim103Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim104Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim104Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim105Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim105Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim106Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim106Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim106Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-E))
)
:effect
(and
)

)
(:action Prim107Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim107Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim108Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim108Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim110Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim110Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim111Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim111Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim112Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim112Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim113Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim113Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim114Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim114Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim115Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim115Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim116Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim116Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim117Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim117Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim118Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim118Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim119Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim119Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim120Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim120Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim121Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim121Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim122Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim122Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim123Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim123Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim124Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim124Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim125Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim125Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim126Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim126Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim127Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim127Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim127Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-E))
)
:effect
(and
)

)
(:action Prim128Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim128Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim129Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim129Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim131Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim131Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim132Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim132Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim133Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim133Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim134Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim134Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim135Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim135Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim136Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim136Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim137Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim137Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim138Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim138Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim139Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim139Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim140Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim140Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim141Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim141Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim142Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim142Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim143Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim143Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim144Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim144Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim145Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim145Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim146Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim146Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim147Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim147Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim148Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim148Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim148Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-E))
)
:effect
(and
)

)
(:action Prim149Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim149Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim150Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim150Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim152Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim152Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim153Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim153Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim154Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim154Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim155Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim155Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim156Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim156Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim157Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim157Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim158Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim158Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim159Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim159Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim160Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim160Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim161Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim161Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim162Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim162Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim163Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim163Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim164Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim164Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim165Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim165Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim166Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim166Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim167Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim167Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim168Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim168Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim169Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim169Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim169Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-E))
)
:effect
(and
)

)
(:action Prim170Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim170Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim171Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim171Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim173Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim173Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim174Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim174Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim175Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim175Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim176Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim176Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim177Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim177Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim178Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim178Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim179Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim179Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim180Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim180Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim181Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim181Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim182Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim182Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim183Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim183Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim184Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim184Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim185Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim185Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim186Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim186Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim187Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim187Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim188Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim188Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim189Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim189Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim190Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim190Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim190Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-E))
)
:effect
(and
)

)
(:action Prim191Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim191Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim192Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim192Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim194Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim194Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim195Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim195Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim196Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim196Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim197Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim197Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim198Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim198Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim199Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim199Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim200Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim200Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim201Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim201Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim202Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim202Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim203Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim203Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim204Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim204Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim205Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim205Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim206Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim206Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim207Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim207Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim208Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim208Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim209Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim209Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim210Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim210Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim211Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim211Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim211Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-E))
)
:effect
(and
)

)
(:action Prim212Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim212Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim213Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim213Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim215Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim215Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim216Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim216Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim217Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim217Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim218Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim218Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim219Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim219Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim220Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim220Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim221Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim221Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim222Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim222Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim223Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim223Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim224Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim224Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim225Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim225Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim226Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim226Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim227Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim227Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim228Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim228Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim229Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim229Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim230Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim230Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim231Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim231Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim232Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim232Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim232Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-E))
)
:effect
(and
)

)
(:action Prim233Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim233Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim234Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim234Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim236Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim236Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim237Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim237Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim238Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim238Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim239Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim239Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim240Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim240Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim241Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim241Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim242Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim242Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim243Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim243Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim244Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim244Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim245Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim245Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim246Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim246Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim247Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim247Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim248Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim248Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim249Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim249Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim250Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim250Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim251Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim251Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim252Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim252Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim253Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim253Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim253Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-E))
)
:effect
(and
)

)
(:action Prim254Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim254Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim255Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim255Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim257Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim257Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim258Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim258Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim259Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim259Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim260Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim260Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim261Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim261Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim262Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim262Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim263Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim263Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim264Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim264Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim265Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim265Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim266Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim266Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim267Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim267Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim268Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim268Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim269Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim269Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim270Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim270Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim271Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim271Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim272Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim272Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim273Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim273Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim274Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim274Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim274Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-E))
)
:effect
(and
)

)
(:action Prim275Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim275Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim276Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim276Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim278Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim278Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim279Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim279Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim280Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim280Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim281Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim281Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim282Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim282Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim283Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim283Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim284Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim284Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim285Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim285Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim286Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim286Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim287Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim287Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim288Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim288Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim289Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim289Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim290Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim290Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim291Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim291Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim292Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim292Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim293Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim293Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim294Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim294Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim295Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim295Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim295Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-E))
)
:effect
(and
)

)
(:action Prim296Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim296Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim297Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim297Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Flag298Action
:parameters ()
:precondition
(and
(NOT-BOARDED-PASS_Q)
(NOT-SERVED-PASS_Q)
)
:effect
(and
(Flag298-)
(Flag298prime-)
)

)
(:action Prim298Action-0
:parameters ()
:precondition
(and
(not (NOT-BOARDED-PASS_Q))
)
:effect
(and
(Flag298prime-)
(not (Flag298-))
)

)
(:action Prim298Action-1
:parameters ()
:precondition
(and
(not (NOT-SERVED-PASS_Q))
)
:effect
(and
(Flag298prime-)
(not (Flag298-))
)

)
(:action Flag299Action
:parameters ()
:precondition
(and
(NOT-BOARDED-PASS_R)
(NOT-SERVED-PASS_R)
)
:effect
(and
(Flag299-)
(Flag299prime-)
)

)
(:action Prim299Action-0
:parameters ()
:precondition
(and
(not (NOT-BOARDED-PASS_R))
)
:effect
(and
(Flag299prime-)
(not (Flag299-))
)

)
(:action Prim299Action-1
:parameters ()
:precondition
(and
(not (NOT-SERVED-PASS_R))
)
:effect
(and
(Flag299prime-)
(not (Flag299-))
)

)
(:action Flag300Action
:parameters ()
:precondition
(and
(NOT-BOARDED-PASS_S)
(NOT-SERVED-PASS_S)
)
:effect
(and
(Flag300-)
(Flag300prime-)
)

)
(:action Prim300Action-0
:parameters ()
:precondition
(and
(not (NOT-BOARDED-PASS_S))
)
:effect
(and
(Flag300prime-)
(not (Flag300-))
)

)
(:action Prim300Action-1
:parameters ()
:precondition
(and
(not (NOT-SERVED-PASS_S))
)
:effect
(and
(Flag300prime-)
(not (Flag300-))
)

)
(:action Flag301Action
:parameters ()
:precondition
(and
(NOT-BOARDED-PASS_C)
(NOT-SERVED-PASS_C)
)
:effect
(and
(Flag301-)
(Flag301prime-)
)

)
(:action Prim301Action-0
:parameters ()
:precondition
(and
(not (NOT-BOARDED-PASS_C))
)
:effect
(and
(Flag301prime-)
(not (Flag301-))
)

)
(:action Prim301Action-1
:parameters ()
:precondition
(and
(not (NOT-SERVED-PASS_C))
)
:effect
(and
(Flag301prime-)
(not (Flag301-))
)

)
(:action Flag302Action
:parameters ()
:precondition
(and
(NOT-BOARDED-PASS_L)
(NOT-SERVED-PASS_L)
)
:effect
(and
(Flag302-)
(Flag302prime-)
)

)
(:action Prim302Action-0
:parameters ()
:precondition
(and
(not (NOT-BOARDED-PASS_L))
)
:effect
(and
(Flag302prime-)
(not (Flag302-))
)

)
(:action Prim302Action-1
:parameters ()
:precondition
(and
(not (NOT-SERVED-PASS_L))
)
:effect
(and
(Flag302prime-)
(not (Flag302-))
)

)
(:action Flag303Action
:parameters ()
:precondition
(and
(NOT-BOARDED-PASS_M)
(NOT-SERVED-PASS_M)
)
:effect
(and
(Flag303-)
(Flag303prime-)
)

)
(:action Prim303Action-0
:parameters ()
:precondition
(and
(not (NOT-BOARDED-PASS_M))
)
:effect
(and
(Flag303prime-)
(not (Flag303-))
)

)
(:action Prim303Action-1
:parameters ()
:precondition
(and
(not (NOT-SERVED-PASS_M))
)
:effect
(and
(Flag303prime-)
(not (Flag303-))
)

)
(:action Flag304Action
:parameters ()
:precondition
(and
(NOT-BOARDED-PASS_N)
(NOT-SERVED-PASS_N)
)
:effect
(and
(Flag304-)
(Flag304prime-)
)

)
(:action Prim304Action-0
:parameters ()
:precondition
(and
(not (NOT-BOARDED-PASS_N))
)
:effect
(and
(Flag304prime-)
(not (Flag304-))
)

)
(:action Prim304Action-1
:parameters ()
:precondition
(and
(not (NOT-SERVED-PASS_N))
)
:effect
(and
(Flag304prime-)
(not (Flag304-))
)

)
(:action Flag305Action
:parameters ()
:precondition
(and
(NOT-BOARDED-PASS_H)
(NOT-SERVED-PASS_H)
)
:effect
(and
(Flag305-)
(Flag305prime-)
)

)
(:action Prim305Action-0
:parameters ()
:precondition
(and
(not (NOT-BOARDED-PASS_H))
)
:effect
(and
(Flag305prime-)
(not (Flag305-))
)

)
(:action Prim305Action-1
:parameters ()
:precondition
(and
(not (NOT-SERVED-PASS_H))
)
:effect
(and
(Flag305prime-)
(not (Flag305-))
)

)
)
