(define (domain grounded-SIMPLE-ADL-ELEVATORS)
(:requirements
:strips
)
(:predicates
(Flag637-)
(Flag636-)
(Flag635-)
(Flag634-)
(Flag633-)
(Flag632-)
(Flag631-)
(Flag630-)
(Flag629-)
(Flag628-)
(Flag627-)
(Flag626-)
(Flag87-)
(NOT-LIFT_AT-AP)
(LIFT_AT-AO)
(CHECKCONSISTENCY-)
(LIFT_AT-AM)
(LIFT_AT-AK)
(LIFT_AT-AQ)
(ERROR-)
(Flag35-)
(Flag33-)
(Flag31-)
(Flag29-)
(Flag27-)
(Flag25-)
(Flag23-)
(Flag21-)
(Flag19-)
(Flag17-)
(Flag15-)
(Flag13-)
(Flag11-)
(Flag9-)
(Flag7-)
(Flag5-)
(Flag3-)
(Flag1-)
(SERVED-PASS_AN)
(SERVED-PASS_AL)
(SERVED-PASS_AE)
(SERVED-PASS_AB)
(BOARDED-PASS_AA)
(SERVED-PASS_AX)
(SERVED-PASS_AU)
(BOARDED-PASS_BC)
(BOARDED-PASS_BB)
(Flag93-)
(Flag91-)
(Flag89-)
(Flag69-)
(LIFT_AT-AJ)
(LIFT_AT-AR)
(LIFT_AT-AT)
(Flag36-)
(Flag34-)
(Flag32-)
(Flag30-)
(Flag28-)
(Flag26-)
(Flag24-)
(Flag22-)
(Flag20-)
(Flag18-)
(Flag16-)
(Flag14-)
(Flag12-)
(Flag10-)
(Flag8-)
(Flag6-)
(Flag4-)
(Flag2-)
(BOARDED-PASS_AB)
(SERVED-PASS_BC)
(Flag92-)
(Flag85-)
(Flag84-)
(LIFT_AT-AI)
(LIFT_AT-AH)
(LIFT_AT-AG)
(LIFT_AT-AV)
(SERVED-PASS_AS)
(BOARDED-PASS_AL)
(SERVED-PASS_AD)
(SERVED-PASS_BA)
(Flag97-)
(Flag95-)
(Flag94-)
(Flag82-)
(LIFT_AT-AF)
(LIFT_AT-AC)
(LIFT_AT-AW)
(LIFT_AT-AY)
(LIFT_AT-AZ)
(LIFT_AT-BD)
(BOARDED-PASS_AS)
(BOARDED-PASS_AN)
(BOARDED-PASS_AD)
(BOARDED-PASS_AE)
(SERVED-PASS_AA)
(BOARDED-PASS_AX)
(BOARDED-PASS_AU)
(SERVED-PASS_BB)
(Flag98-)
(Flag96-)
(Flag83-)
(Flag81-)
(Flag76-)
(Flag74-)
(Flag37-)
(BOARDED-PASS_BA)
(NOT-CHECKCONSISTENCY-)
(NOT-BOARDED-PASS_BA)
(NOT-LIFT_AT-AZ)
(NOT-LIFT_AT-AY)
(NOT-LIFT_AT-AW)
(NOT-LIFT_AT-AV)
(NOT-SERVED-PASS_BB)
(NOT-BOARDED-PASS_AU)
(NOT-BOARDED-PASS_AX)
(NOT-SERVED-PASS_AA)
(NOT-BOARDED-PASS_AE)
(NOT-BOARDED-PASS_AD)
(NOT-BOARDED-PASS_AN)
(NOT-BOARDED-PASS_AS)
(NOT-LIFT_AT-BD)
(NOT-LIFT_AT-AF)
(NOT-LIFT_AT-AG)
(NOT-LIFT_AT-AT)
(NOT-LIFT_AT-AC)
(NOT-SERVED-PASS_BA)
(NOT-SERVED-PASS_AD)
(NOT-BOARDED-PASS_AL)
(NOT-SERVED-PASS_AS)
(NOT-LIFT_AT-AH)
(NOT-LIFT_AT-AI)
(NOT-LIFT_AT-AJ)
(NOT-LIFT_AT-AR)
(NOT-LIFT_AT-AQ)
(NOT-SERVED-PASS_BC)
(NOT-BOARDED-PASS_AB)
(NOT-LIFT_AT-AK)
(NOT-BOARDED-PASS_BB)
(NOT-BOARDED-PASS_BC)
(NOT-SERVED-PASS_AU)
(NOT-SERVED-PASS_AX)
(NOT-BOARDED-PASS_AA)
(NOT-SERVED-PASS_AB)
(NOT-SERVED-PASS_AE)
(NOT-SERVED-PASS_AL)
(NOT-SERVED-PASS_AN)
(NOT-ERROR-)
(NOT-LIFT_AT-AM)
(NOT-LIFT_AT-AO)
(LIFT_AT-AP)
(Flag637prime-)
(Flag636prime-)
(Flag635prime-)
(Flag634prime-)
(Flag633prime-)
(Flag632prime-)
(Flag631prime-)
(Flag630prime-)
(Flag629prime-)
(Flag628prime-)
(Flag627prime-)
(Flag626prime-)
(Flag87prime-)
(Flag35prime-)
(Flag33prime-)
(Flag31prime-)
(Flag29prime-)
(Flag27prime-)
(Flag25prime-)
(Flag23prime-)
(Flag21prime-)
(Flag19prime-)
(Flag17prime-)
(Flag15prime-)
(Flag13prime-)
(Flag11prime-)
(Flag9prime-)
(Flag7prime-)
(Flag5prime-)
(Flag3prime-)
(Flag1prime-)
(Flag93prime-)
(Flag91prime-)
(Flag89prime-)
(Flag69prime-)
(Flag36prime-)
(Flag34prime-)
(Flag32prime-)
(Flag30prime-)
(Flag28prime-)
(Flag26prime-)
(Flag24prime-)
(Flag22prime-)
(Flag20prime-)
(Flag18prime-)
(Flag16prime-)
(Flag14prime-)
(Flag12prime-)
(Flag10prime-)
(Flag8prime-)
(Flag6prime-)
(Flag4prime-)
(Flag2prime-)
(Flag92prime-)
(Flag85prime-)
(Flag84prime-)
(Flag97prime-)
(Flag95prime-)
(Flag94prime-)
(Flag82prime-)
(Flag98prime-)
(Flag96prime-)
(Flag83prime-)
(Flag81prime-)
(Flag76prime-)
(Flag74prime-)
(Flag37prime-)
)
(:action STOP-PASS_BA-BD
:parameters ()
:precondition
(and
(Flag74-)
(Flag74prime-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag637prime-))
(not (Flag636prime-))
(not (Flag635prime-))
(not (Flag634prime-))
(not (Flag633prime-))
(not (Flag632prime-))
(not (Flag631prime-))
(not (Flag630prime-))
(not (Flag629prime-))
(not (Flag628prime-))
(not (Flag627prime-))
(not (Flag626prime-))
(not (Flag87prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag93prime-))
(not (Flag91prime-))
(not (Flag89prime-))
(not (Flag69prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag92prime-))
(not (Flag85prime-))
(not (Flag84prime-))
(not (Flag97prime-))
(not (Flag95prime-))
(not (Flag94prime-))
(not (Flag82prime-))
(not (Flag98prime-))
(not (Flag96prime-))
(not (Flag83prime-))
(not (Flag81prime-))
(not (Flag76prime-))
(not (Flag74prime-))
(not (Flag37prime-))
)
)
(:action STOP-PASS_BA-AZ
:parameters ()
:precondition
(and
(Flag76-)
(Flag76prime-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag637prime-))
(not (Flag636prime-))
(not (Flag635prime-))
(not (Flag634prime-))
(not (Flag633prime-))
(not (Flag632prime-))
(not (Flag631prime-))
(not (Flag630prime-))
(not (Flag629prime-))
(not (Flag628prime-))
(not (Flag627prime-))
(not (Flag626prime-))
(not (Flag87prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag93prime-))
(not (Flag91prime-))
(not (Flag89prime-))
(not (Flag69prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag92prime-))
(not (Flag85prime-))
(not (Flag84prime-))
(not (Flag97prime-))
(not (Flag95prime-))
(not (Flag94prime-))
(not (Flag82prime-))
(not (Flag98prime-))
(not (Flag96prime-))
(not (Flag83prime-))
(not (Flag81prime-))
(not (Flag76prime-))
(not (Flag74prime-))
(not (Flag37prime-))
)
)
(:action STOP-PASS_BA-AY
:parameters ()
:precondition
(and
(Flag81-)
(Flag81prime-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag637prime-))
(not (Flag636prime-))
(not (Flag635prime-))
(not (Flag634prime-))
(not (Flag633prime-))
(not (Flag632prime-))
(not (Flag631prime-))
(not (Flag630prime-))
(not (Flag629prime-))
(not (Flag628prime-))
(not (Flag627prime-))
(not (Flag626prime-))
(not (Flag87prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag93prime-))
(not (Flag91prime-))
(not (Flag89prime-))
(not (Flag69prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag92prime-))
(not (Flag85prime-))
(not (Flag84prime-))
(not (Flag97prime-))
(not (Flag95prime-))
(not (Flag94prime-))
(not (Flag82prime-))
(not (Flag98prime-))
(not (Flag96prime-))
(not (Flag83prime-))
(not (Flag81prime-))
(not (Flag76prime-))
(not (Flag74prime-))
(not (Flag37prime-))
)
)
(:action STOP-PASS_BA-AW
:parameters ()
:precondition
(and
(Flag83-)
(Flag83prime-)
(Flag637prime-)
)
:effect
(and
(when
(and
(Flag637-)
)
(and
(BOARDED-PASS_BA)
(not (NOT-BOARDED-PASS_BA))
)
)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag637prime-))
(not (Flag636prime-))
(not (Flag635prime-))
(not (Flag634prime-))
(not (Flag633prime-))
(not (Flag632prime-))
(not (Flag631prime-))
(not (Flag630prime-))
(not (Flag629prime-))
(not (Flag628prime-))
(not (Flag627prime-))
(not (Flag626prime-))
(not (Flag87prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag93prime-))
(not (Flag91prime-))
(not (Flag89prime-))
(not (Flag69prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag92prime-))
(not (Flag85prime-))
(not (Flag84prime-))
(not (Flag97prime-))
(not (Flag95prime-))
(not (Flag94prime-))
(not (Flag82prime-))
(not (Flag98prime-))
(not (Flag96prime-))
(not (Flag83prime-))
(not (Flag81prime-))
(not (Flag76prime-))
(not (Flag74prime-))
(not (Flag37prime-))
)
)
(:action STOP-PASS_BA-AF
:parameters ()
:precondition
(and
(Flag96-)
(Flag96prime-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag637prime-))
(not (Flag636prime-))
(not (Flag635prime-))
(not (Flag634prime-))
(not (Flag633prime-))
(not (Flag632prime-))
(not (Flag631prime-))
(not (Flag630prime-))
(not (Flag629prime-))
(not (Flag628prime-))
(not (Flag627prime-))
(not (Flag626prime-))
(not (Flag87prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag93prime-))
(not (Flag91prime-))
(not (Flag89prime-))
(not (Flag69prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag92prime-))
(not (Flag85prime-))
(not (Flag84prime-))
(not (Flag97prime-))
(not (Flag95prime-))
(not (Flag94prime-))
(not (Flag82prime-))
(not (Flag98prime-))
(not (Flag96prime-))
(not (Flag83prime-))
(not (Flag81prime-))
(not (Flag76prime-))
(not (Flag74prime-))
(not (Flag37prime-))
)
)
(:action STOP-PASS_BA-AC
:parameters ()
:precondition
(and
(Flag98-)
(Flag98prime-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag637prime-))
(not (Flag636prime-))
(not (Flag635prime-))
(not (Flag634prime-))
(not (Flag633prime-))
(not (Flag632prime-))
(not (Flag631prime-))
(not (Flag630prime-))
(not (Flag629prime-))
(not (Flag628prime-))
(not (Flag627prime-))
(not (Flag626prime-))
(not (Flag87prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag93prime-))
(not (Flag91prime-))
(not (Flag89prime-))
(not (Flag69prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag92prime-))
(not (Flag85prime-))
(not (Flag84prime-))
(not (Flag97prime-))
(not (Flag95prime-))
(not (Flag94prime-))
(not (Flag82prime-))
(not (Flag98prime-))
(not (Flag96prime-))
(not (Flag83prime-))
(not (Flag81prime-))
(not (Flag76prime-))
(not (Flag74prime-))
(not (Flag37prime-))
)
)
(:action Flag37Action
:parameters ()
:precondition
(and
(Flag2-)
(Flag2prime-)
(Flag4-)
(Flag4prime-)
(Flag6-)
(Flag6prime-)
(Flag8-)
(Flag8prime-)
(Flag10-)
(Flag10prime-)
(Flag12-)
(Flag12prime-)
(Flag14-)
(Flag14prime-)
(Flag16-)
(Flag16prime-)
(SERVED-PASS_AS)
(Flag18-)
(Flag18prime-)
(SERVED-PASS_AN)
(Flag20-)
(Flag20prime-)
(SERVED-PASS_AL)
(Flag22-)
(Flag22prime-)
(Flag24-)
(Flag24prime-)
(Flag26-)
(Flag26prime-)
(Flag28-)
(Flag28prime-)
(Flag30-)
(Flag30prime-)
(SERVED-PASS_AD)
(SERVED-PASS_AE)
(SERVED-PASS_AB)
(SERVED-PASS_AA)
(Flag32-)
(Flag32prime-)
(SERVED-PASS_AX)
(Flag34-)
(Flag34prime-)
(SERVED-PASS_AU)
(SERVED-PASS_BC)
(SERVED-PASS_BB)
(SERVED-PASS_BA)
(Flag36-)
(Flag36prime-)
(NOT-ERROR-)
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(Flag37-)
(Flag37prime-)
)

)
(:action Prim37Action-21
:parameters ()
:precondition
(and
(not (SERVED-PASS_AA))
)
:effect
(and
(Flag37prime-)
(not (Flag37-))
)

)
(:action Prim37Action-27
:parameters ()
:precondition
(and
(not (SERVED-PASS_BB))
)
:effect
(and
(Flag37prime-)
(not (Flag37-))
)

)
(:action MOVEDOWN-BD-AZ
:parameters ()
:precondition
(and
(LIFT_AT-BD)
(NOT-ERROR-)
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(NOT-LIFT_AT-BD)
(LIFT_AT-AZ)
(CHECKCONSISTENCY-)
(not (LIFT_AT-BD))
(not (NOT-LIFT_AT-AZ))
(not (NOT-CHECKCONSISTENCY-))
(not (Flag637prime-))
(not (Flag636prime-))
(not (Flag635prime-))
(not (Flag634prime-))
(not (Flag633prime-))
(not (Flag632prime-))
(not (Flag631prime-))
(not (Flag630prime-))
(not (Flag629prime-))
(not (Flag628prime-))
(not (Flag627prime-))
(not (Flag626prime-))
(not (Flag87prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag93prime-))
(not (Flag91prime-))
(not (Flag89prime-))
(not (Flag69prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag92prime-))
(not (Flag85prime-))
(not (Flag84prime-))
(not (Flag97prime-))
(not (Flag95prime-))
(not (Flag94prime-))
(not (Flag82prime-))
(not (Flag98prime-))
(not (Flag96prime-))
(not (Flag83prime-))
(not (Flag81prime-))
(not (Flag76prime-))
(not (Flag74prime-))
(not (Flag37prime-))
)
)
(:action MOVEDOWN-AZ-AY
:parameters ()
:precondition
(and
(LIFT_AT-AZ)
(NOT-ERROR-)
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(NOT-LIFT_AT-AZ)
(LIFT_AT-AY)
(CHECKCONSISTENCY-)
(not (LIFT_AT-AZ))
(not (NOT-LIFT_AT-AY))
(not (NOT-CHECKCONSISTENCY-))
(not (Flag637prime-))
(not (Flag636prime-))
(not (Flag635prime-))
(not (Flag634prime-))
(not (Flag633prime-))
(not (Flag632prime-))
(not (Flag631prime-))
(not (Flag630prime-))
(not (Flag629prime-))
(not (Flag628prime-))
(not (Flag627prime-))
(not (Flag626prime-))
(not (Flag87prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag93prime-))
(not (Flag91prime-))
(not (Flag89prime-))
(not (Flag69prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag92prime-))
(not (Flag85prime-))
(not (Flag84prime-))
(not (Flag97prime-))
(not (Flag95prime-))
(not (Flag94prime-))
(not (Flag82prime-))
(not (Flag98prime-))
(not (Flag96prime-))
(not (Flag83prime-))
(not (Flag81prime-))
(not (Flag76prime-))
(not (Flag74prime-))
(not (Flag37prime-))
)
)
(:action MOVEDOWN-AY-AW
:parameters ()
:precondition
(and
(LIFT_AT-AY)
(NOT-ERROR-)
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(NOT-LIFT_AT-AY)
(LIFT_AT-AW)
(CHECKCONSISTENCY-)
(not (LIFT_AT-AY))
(not (NOT-LIFT_AT-AW))
(not (NOT-CHECKCONSISTENCY-))
(not (Flag637prime-))
(not (Flag636prime-))
(not (Flag635prime-))
(not (Flag634prime-))
(not (Flag633prime-))
(not (Flag632prime-))
(not (Flag631prime-))
(not (Flag630prime-))
(not (Flag629prime-))
(not (Flag628prime-))
(not (Flag627prime-))
(not (Flag626prime-))
(not (Flag87prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag93prime-))
(not (Flag91prime-))
(not (Flag89prime-))
(not (Flag69prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag92prime-))
(not (Flag85prime-))
(not (Flag84prime-))
(not (Flag97prime-))
(not (Flag95prime-))
(not (Flag94prime-))
(not (Flag82prime-))
(not (Flag98prime-))
(not (Flag96prime-))
(not (Flag83prime-))
(not (Flag81prime-))
(not (Flag76prime-))
(not (Flag74prime-))
(not (Flag37prime-))
)
)
(:action MOVEDOWN-AW-AV
:parameters ()
:precondition
(and
(LIFT_AT-AW)
(NOT-ERROR-)
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(NOT-LIFT_AT-AW)
(LIFT_AT-AV)
(CHECKCONSISTENCY-)
(not (LIFT_AT-AW))
(not (NOT-LIFT_AT-AV))
(not (NOT-CHECKCONSISTENCY-))
(not (Flag637prime-))
(not (Flag636prime-))
(not (Flag635prime-))
(not (Flag634prime-))
(not (Flag633prime-))
(not (Flag632prime-))
(not (Flag631prime-))
(not (Flag630prime-))
(not (Flag629prime-))
(not (Flag628prime-))
(not (Flag627prime-))
(not (Flag626prime-))
(not (Flag87prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag93prime-))
(not (Flag91prime-))
(not (Flag89prime-))
(not (Flag69prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag92prime-))
(not (Flag85prime-))
(not (Flag84prime-))
(not (Flag97prime-))
(not (Flag95prime-))
(not (Flag94prime-))
(not (Flag82prime-))
(not (Flag98prime-))
(not (Flag96prime-))
(not (Flag83prime-))
(not (Flag81prime-))
(not (Flag76prime-))
(not (Flag74prime-))
(not (Flag37prime-))
)
)
(:action Prim44Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-BD))
)
:effect
(and
)

)
(:action Prim46Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AZ))
)
:effect
(and
)

)
(:action Prim51Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AY))
)
:effect
(and
)

)
(:action Prim53Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AW))
)
:effect
(and
)

)
(:action Prim66Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AF))
)
:effect
(and
)

)
(:action Prim68Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AC))
)
:effect
(and
)

)
(:action Flag74Action
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-BD)
)
:effect
(and
(Flag74-)
(Flag74prime-)
)

)
(:action Prim74Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-BD))
)
:effect
(and
(Flag74prime-)
(not (Flag74-))
)

)
(:action Flag76Action
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AZ)
)
:effect
(and
(Flag76-)
(Flag76prime-)
)

)
(:action Prim76Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AZ))
)
:effect
(and
(Flag76prime-)
(not (Flag76-))
)

)
(:action Flag81Action
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AY)
)
:effect
(and
(Flag81-)
(Flag81prime-)
)

)
(:action Prim81Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AY))
)
:effect
(and
(Flag81prime-)
(not (Flag81-))
)

)
(:action Flag83Action
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AW)
)
:effect
(and
(Flag83-)
(Flag83prime-)
)

)
(:action Prim83Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AW))
)
:effect
(and
(Flag83prime-)
(not (Flag83-))
)

)
(:action Flag96Action
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AF)
)
:effect
(and
(Flag96-)
(Flag96prime-)
)

)
(:action Prim96Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AF))
)
:effect
(and
(Flag96prime-)
(not (Flag96-))
)

)
(:action Flag98Action
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AC)
)
:effect
(and
(Flag98-)
(Flag98prime-)
)

)
(:action Prim98Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AC))
)
:effect
(and
(Flag98prime-)
(not (Flag98-))
)

)
(:action Prim105Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-BD))
)
:effect
(and
)

)
(:action Prim107Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AZ))
)
:effect
(and
)

)
(:action Prim112Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AY))
)
:effect
(and
)

)
(:action Prim114Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AW))
)
:effect
(and
)

)
(:action Prim127Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AF))
)
:effect
(and
)

)
(:action Prim129Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AC))
)
:effect
(and
)

)
(:action Prim136Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-BD))
)
:effect
(and
)

)
(:action Prim138Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AZ))
)
:effect
(and
)

)
(:action Prim143Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AY))
)
:effect
(and
)

)
(:action Prim145Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AW))
)
:effect
(and
)

)
(:action Prim158Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AF))
)
:effect
(and
)

)
(:action Prim160Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AC))
)
:effect
(and
)

)
(:action Prim167Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-BD))
)
:effect
(and
)

)
(:action Prim169Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AZ))
)
:effect
(and
)

)
(:action Prim174Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AY))
)
:effect
(and
)

)
(:action Prim176Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AW))
)
:effect
(and
)

)
(:action Prim189Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AF))
)
:effect
(and
)

)
(:action Prim191Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AC))
)
:effect
(and
)

)
(:action Prim198Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-BD))
)
:effect
(and
)

)
(:action Prim200Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AZ))
)
:effect
(and
)

)
(:action Prim205Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AY))
)
:effect
(and
)

)
(:action Prim207Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AW))
)
:effect
(and
)

)
(:action Prim220Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AF))
)
:effect
(and
)

)
(:action Prim222Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AC))
)
:effect
(and
)

)
(:action Prim229Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-BD))
)
:effect
(and
)

)
(:action Prim231Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AZ))
)
:effect
(and
)

)
(:action Prim236Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AY))
)
:effect
(and
)

)
(:action Prim238Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AW))
)
:effect
(and
)

)
(:action Prim251Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AF))
)
:effect
(and
)

)
(:action Prim253Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AC))
)
:effect
(and
)

)
(:action Prim260Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-BD))
)
:effect
(and
)

)
(:action Prim262Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AZ))
)
:effect
(and
)

)
(:action Prim267Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AY))
)
:effect
(and
)

)
(:action Prim269Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AW))
)
:effect
(and
)

)
(:action Prim282Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AF))
)
:effect
(and
)

)
(:action Prim284Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AC))
)
:effect
(and
)

)
(:action Prim291Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-BD))
)
:effect
(and
)

)
(:action Prim293Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AZ))
)
:effect
(and
)

)
(:action Prim298Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AY))
)
:effect
(and
)

)
(:action Prim300Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AW))
)
:effect
(and
)

)
(:action Prim313Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AF))
)
:effect
(and
)

)
(:action Prim315Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AC))
)
:effect
(and
)

)
(:action Prim322Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-BD))
)
:effect
(and
)

)
(:action Prim324Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AZ))
)
:effect
(and
)

)
(:action Prim329Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AY))
)
:effect
(and
)

)
(:action Prim331Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AW))
)
:effect
(and
)

)
(:action Prim344Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AF))
)
:effect
(and
)

)
(:action Prim346Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AC))
)
:effect
(and
)

)
(:action Prim353Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-BD))
)
:effect
(and
)

)
(:action Prim355Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AZ))
)
:effect
(and
)

)
(:action Prim360Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AY))
)
:effect
(and
)

)
(:action Prim362Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AW))
)
:effect
(and
)

)
(:action Prim375Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AF))
)
:effect
(and
)

)
(:action Prim377Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AC))
)
:effect
(and
)

)
(:action Prim384Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-BD))
)
:effect
(and
)

)
(:action Prim386Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AZ))
)
:effect
(and
)

)
(:action Prim391Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AY))
)
:effect
(and
)

)
(:action Prim393Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AW))
)
:effect
(and
)

)
(:action Prim406Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AF))
)
:effect
(and
)

)
(:action Prim408Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AC))
)
:effect
(and
)

)
(:action Prim415Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-BD))
)
:effect
(and
)

)
(:action Prim417Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AZ))
)
:effect
(and
)

)
(:action Prim422Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AY))
)
:effect
(and
)

)
(:action Prim424Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AW))
)
:effect
(and
)

)
(:action Prim437Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AF))
)
:effect
(and
)

)
(:action Prim439Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AC))
)
:effect
(and
)

)
(:action Prim446Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-BD))
)
:effect
(and
)

)
(:action Prim448Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AZ))
)
:effect
(and
)

)
(:action Prim453Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AY))
)
:effect
(and
)

)
(:action Prim455Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AW))
)
:effect
(and
)

)
(:action Prim468Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AF))
)
:effect
(and
)

)
(:action Prim470Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AC))
)
:effect
(and
)

)
(:action Prim477Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-BD))
)
:effect
(and
)

)
(:action Prim479Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AZ))
)
:effect
(and
)

)
(:action Prim484Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AY))
)
:effect
(and
)

)
(:action Prim486Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AW))
)
:effect
(and
)

)
(:action Prim499Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AF))
)
:effect
(and
)

)
(:action Prim501Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AC))
)
:effect
(and
)

)
(:action Prim508Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-BD))
)
:effect
(and
)

)
(:action Prim510Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AZ))
)
:effect
(and
)

)
(:action Prim515Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AY))
)
:effect
(and
)

)
(:action Prim517Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AW))
)
:effect
(and
)

)
(:action Prim530Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AF))
)
:effect
(and
)

)
(:action Prim532Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AC))
)
:effect
(and
)

)
(:action Prim539Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-BD))
)
:effect
(and
)

)
(:action Prim541Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AZ))
)
:effect
(and
)

)
(:action Prim546Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AY))
)
:effect
(and
)

)
(:action Prim548Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AW))
)
:effect
(and
)

)
(:action Prim561Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AF))
)
:effect
(and
)

)
(:action Prim563Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AC))
)
:effect
(and
)

)
(:action Prim570Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-BD))
)
:effect
(and
)

)
(:action Prim572Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AZ))
)
:effect
(and
)

)
(:action Prim577Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AY))
)
:effect
(and
)

)
(:action Prim579Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AW))
)
:effect
(and
)

)
(:action Prim592Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AF))
)
:effect
(and
)

)
(:action Prim594Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AC))
)
:effect
(and
)

)
(:action Prim601Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-BD))
)
:effect
(and
)

)
(:action Prim603Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AZ))
)
:effect
(and
)

)
(:action Prim608Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AY))
)
:effect
(and
)

)
(:action Prim610Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AW))
)
:effect
(and
)

)
(:action Prim623Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AF))
)
:effect
(and
)

)
(:action Prim625Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AC))
)
:effect
(and
)

)
(:action STOP-PASS_BA-AV
:parameters ()
:precondition
(and
(Flag82-)
(Flag82prime-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag637prime-))
(not (Flag636prime-))
(not (Flag635prime-))
(not (Flag634prime-))
(not (Flag633prime-))
(not (Flag632prime-))
(not (Flag631prime-))
(not (Flag630prime-))
(not (Flag629prime-))
(not (Flag628prime-))
(not (Flag627prime-))
(not (Flag626prime-))
(not (Flag87prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag93prime-))
(not (Flag91prime-))
(not (Flag89prime-))
(not (Flag69prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag92prime-))
(not (Flag85prime-))
(not (Flag84prime-))
(not (Flag97prime-))
(not (Flag95prime-))
(not (Flag94prime-))
(not (Flag82prime-))
(not (Flag98prime-))
(not (Flag96prime-))
(not (Flag83prime-))
(not (Flag81prime-))
(not (Flag76prime-))
(not (Flag74prime-))
(not (Flag37prime-))
)
)
(:action STOP-PASS_BA-AH
:parameters ()
:precondition
(and
(Flag94-)
(Flag94prime-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag637prime-))
(not (Flag636prime-))
(not (Flag635prime-))
(not (Flag634prime-))
(not (Flag633prime-))
(not (Flag632prime-))
(not (Flag631prime-))
(not (Flag630prime-))
(not (Flag629prime-))
(not (Flag628prime-))
(not (Flag627prime-))
(not (Flag626prime-))
(not (Flag87prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag93prime-))
(not (Flag91prime-))
(not (Flag89prime-))
(not (Flag69prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag92prime-))
(not (Flag85prime-))
(not (Flag84prime-))
(not (Flag97prime-))
(not (Flag95prime-))
(not (Flag94prime-))
(not (Flag82prime-))
(not (Flag98prime-))
(not (Flag96prime-))
(not (Flag83prime-))
(not (Flag81prime-))
(not (Flag76prime-))
(not (Flag74prime-))
(not (Flag37prime-))
)
)
(:action STOP-PASS_BA-AI
:parameters ()
:precondition
(and
(Flag95-)
(Flag95prime-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag637prime-))
(not (Flag636prime-))
(not (Flag635prime-))
(not (Flag634prime-))
(not (Flag633prime-))
(not (Flag632prime-))
(not (Flag631prime-))
(not (Flag630prime-))
(not (Flag629prime-))
(not (Flag628prime-))
(not (Flag627prime-))
(not (Flag626prime-))
(not (Flag87prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag93prime-))
(not (Flag91prime-))
(not (Flag89prime-))
(not (Flag69prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag92prime-))
(not (Flag85prime-))
(not (Flag84prime-))
(not (Flag97prime-))
(not (Flag95prime-))
(not (Flag94prime-))
(not (Flag82prime-))
(not (Flag98prime-))
(not (Flag96prime-))
(not (Flag83prime-))
(not (Flag81prime-))
(not (Flag76prime-))
(not (Flag74prime-))
(not (Flag37prime-))
)
)
(:action STOP-PASS_BA-AG
:parameters ()
:precondition
(and
(Flag97-)
(Flag97prime-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag637prime-))
(not (Flag636prime-))
(not (Flag635prime-))
(not (Flag634prime-))
(not (Flag633prime-))
(not (Flag632prime-))
(not (Flag631prime-))
(not (Flag630prime-))
(not (Flag629prime-))
(not (Flag628prime-))
(not (Flag627prime-))
(not (Flag626prime-))
(not (Flag87prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag93prime-))
(not (Flag91prime-))
(not (Flag89prime-))
(not (Flag69prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag92prime-))
(not (Flag85prime-))
(not (Flag84prime-))
(not (Flag97prime-))
(not (Flag95prime-))
(not (Flag94prime-))
(not (Flag82prime-))
(not (Flag98prime-))
(not (Flag96prime-))
(not (Flag83prime-))
(not (Flag81prime-))
(not (Flag76prime-))
(not (Flag74prime-))
(not (Flag37prime-))
)
)
(:action STOP-PASS_BB-BD
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-BD)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag637prime-))
(not (Flag636prime-))
(not (Flag635prime-))
(not (Flag634prime-))
(not (Flag633prime-))
(not (Flag632prime-))
(not (Flag631prime-))
(not (Flag630prime-))
(not (Flag629prime-))
(not (Flag628prime-))
(not (Flag627prime-))
(not (Flag626prime-))
(not (Flag87prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag93prime-))
(not (Flag91prime-))
(not (Flag89prime-))
(not (Flag69prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag92prime-))
(not (Flag85prime-))
(not (Flag84prime-))
(not (Flag97prime-))
(not (Flag95prime-))
(not (Flag94prime-))
(not (Flag82prime-))
(not (Flag98prime-))
(not (Flag96prime-))
(not (Flag83prime-))
(not (Flag81prime-))
(not (Flag76prime-))
(not (Flag74prime-))
(not (Flag37prime-))
)
)
(:action STOP-PASS_BB-AZ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AZ)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag637prime-))
(not (Flag636prime-))
(not (Flag635prime-))
(not (Flag634prime-))
(not (Flag633prime-))
(not (Flag632prime-))
(not (Flag631prime-))
(not (Flag630prime-))
(not (Flag629prime-))
(not (Flag628prime-))
(not (Flag627prime-))
(not (Flag626prime-))
(not (Flag87prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag93prime-))
(not (Flag91prime-))
(not (Flag89prime-))
(not (Flag69prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag92prime-))
(not (Flag85prime-))
(not (Flag84prime-))
(not (Flag97prime-))
(not (Flag95prime-))
(not (Flag94prime-))
(not (Flag82prime-))
(not (Flag98prime-))
(not (Flag96prime-))
(not (Flag83prime-))
(not (Flag81prime-))
(not (Flag76prime-))
(not (Flag74prime-))
(not (Flag37prime-))
)
)
(:action STOP-PASS_BB-AY
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AY)
)
:effect
(and
(when
(and
(BOARDED-PASS_BB)
)
(and
(SERVED-PASS_BB)
(NOT-BOARDED-PASS_BB)
(not (NOT-SERVED-PASS_BB))
(not (BOARDED-PASS_BB))
)
)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag637prime-))
(not (Flag636prime-))
(not (Flag635prime-))
(not (Flag634prime-))
(not (Flag633prime-))
(not (Flag632prime-))
(not (Flag631prime-))
(not (Flag630prime-))
(not (Flag629prime-))
(not (Flag628prime-))
(not (Flag627prime-))
(not (Flag626prime-))
(not (Flag87prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag93prime-))
(not (Flag91prime-))
(not (Flag89prime-))
(not (Flag69prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag92prime-))
(not (Flag85prime-))
(not (Flag84prime-))
(not (Flag97prime-))
(not (Flag95prime-))
(not (Flag94prime-))
(not (Flag82prime-))
(not (Flag98prime-))
(not (Flag96prime-))
(not (Flag83prime-))
(not (Flag81prime-))
(not (Flag76prime-))
(not (Flag74prime-))
(not (Flag37prime-))
)
)
(:action STOP-PASS_BB-AW
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AW)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag637prime-))
(not (Flag636prime-))
(not (Flag635prime-))
(not (Flag634prime-))
(not (Flag633prime-))
(not (Flag632prime-))
(not (Flag631prime-))
(not (Flag630prime-))
(not (Flag629prime-))
(not (Flag628prime-))
(not (Flag627prime-))
(not (Flag626prime-))
(not (Flag87prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag93prime-))
(not (Flag91prime-))
(not (Flag89prime-))
(not (Flag69prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag92prime-))
(not (Flag85prime-))
(not (Flag84prime-))
(not (Flag97prime-))
(not (Flag95prime-))
(not (Flag94prime-))
(not (Flag82prime-))
(not (Flag98prime-))
(not (Flag96prime-))
(not (Flag83prime-))
(not (Flag81prime-))
(not (Flag76prime-))
(not (Flag74prime-))
(not (Flag37prime-))
)
)
(:action STOP-PASS_BB-AF
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AF)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag637prime-))
(not (Flag636prime-))
(not (Flag635prime-))
(not (Flag634prime-))
(not (Flag633prime-))
(not (Flag632prime-))
(not (Flag631prime-))
(not (Flag630prime-))
(not (Flag629prime-))
(not (Flag628prime-))
(not (Flag627prime-))
(not (Flag626prime-))
(not (Flag87prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag93prime-))
(not (Flag91prime-))
(not (Flag89prime-))
(not (Flag69prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag92prime-))
(not (Flag85prime-))
(not (Flag84prime-))
(not (Flag97prime-))
(not (Flag95prime-))
(not (Flag94prime-))
(not (Flag82prime-))
(not (Flag98prime-))
(not (Flag96prime-))
(not (Flag83prime-))
(not (Flag81prime-))
(not (Flag76prime-))
(not (Flag74prime-))
(not (Flag37prime-))
)
)
(:action STOP-PASS_BB-AC
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AC)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag637prime-))
(not (Flag636prime-))
(not (Flag635prime-))
(not (Flag634prime-))
(not (Flag633prime-))
(not (Flag632prime-))
(not (Flag631prime-))
(not (Flag630prime-))
(not (Flag629prime-))
(not (Flag628prime-))
(not (Flag627prime-))
(not (Flag626prime-))
(not (Flag87prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag93prime-))
(not (Flag91prime-))
(not (Flag89prime-))
(not (Flag69prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag92prime-))
(not (Flag85prime-))
(not (Flag84prime-))
(not (Flag97prime-))
(not (Flag95prime-))
(not (Flag94prime-))
(not (Flag82prime-))
(not (Flag98prime-))
(not (Flag96prime-))
(not (Flag83prime-))
(not (Flag81prime-))
(not (Flag76prime-))
(not (Flag74prime-))
(not (Flag37prime-))
)
)
(:action STOP-PASS_BC-BD
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-BD)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag637prime-))
(not (Flag636prime-))
(not (Flag635prime-))
(not (Flag634prime-))
(not (Flag633prime-))
(not (Flag632prime-))
(not (Flag631prime-))
(not (Flag630prime-))
(not (Flag629prime-))
(not (Flag628prime-))
(not (Flag627prime-))
(not (Flag626prime-))
(not (Flag87prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag93prime-))
(not (Flag91prime-))
(not (Flag89prime-))
(not (Flag69prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag92prime-))
(not (Flag85prime-))
(not (Flag84prime-))
(not (Flag97prime-))
(not (Flag95prime-))
(not (Flag94prime-))
(not (Flag82prime-))
(not (Flag98prime-))
(not (Flag96prime-))
(not (Flag83prime-))
(not (Flag81prime-))
(not (Flag76prime-))
(not (Flag74prime-))
(not (Flag37prime-))
)
)
(:action STOP-PASS_BC-AZ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AZ)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag637prime-))
(not (Flag636prime-))
(not (Flag635prime-))
(not (Flag634prime-))
(not (Flag633prime-))
(not (Flag632prime-))
(not (Flag631prime-))
(not (Flag630prime-))
(not (Flag629prime-))
(not (Flag628prime-))
(not (Flag627prime-))
(not (Flag626prime-))
(not (Flag87prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag93prime-))
(not (Flag91prime-))
(not (Flag89prime-))
(not (Flag69prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag92prime-))
(not (Flag85prime-))
(not (Flag84prime-))
(not (Flag97prime-))
(not (Flag95prime-))
(not (Flag94prime-))
(not (Flag82prime-))
(not (Flag98prime-))
(not (Flag96prime-))
(not (Flag83prime-))
(not (Flag81prime-))
(not (Flag76prime-))
(not (Flag74prime-))
(not (Flag37prime-))
)
)
(:action STOP-PASS_BC-AY
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AY)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag637prime-))
(not (Flag636prime-))
(not (Flag635prime-))
(not (Flag634prime-))
(not (Flag633prime-))
(not (Flag632prime-))
(not (Flag631prime-))
(not (Flag630prime-))
(not (Flag629prime-))
(not (Flag628prime-))
(not (Flag627prime-))
(not (Flag626prime-))
(not (Flag87prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag93prime-))
(not (Flag91prime-))
(not (Flag89prime-))
(not (Flag69prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag92prime-))
(not (Flag85prime-))
(not (Flag84prime-))
(not (Flag97prime-))
(not (Flag95prime-))
(not (Flag94prime-))
(not (Flag82prime-))
(not (Flag98prime-))
(not (Flag96prime-))
(not (Flag83prime-))
(not (Flag81prime-))
(not (Flag76prime-))
(not (Flag74prime-))
(not (Flag37prime-))
)
)
(:action STOP-PASS_BC-AW
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AW)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag637prime-))
(not (Flag636prime-))
(not (Flag635prime-))
(not (Flag634prime-))
(not (Flag633prime-))
(not (Flag632prime-))
(not (Flag631prime-))
(not (Flag630prime-))
(not (Flag629prime-))
(not (Flag628prime-))
(not (Flag627prime-))
(not (Flag626prime-))
(not (Flag87prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag93prime-))
(not (Flag91prime-))
(not (Flag89prime-))
(not (Flag69prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag92prime-))
(not (Flag85prime-))
(not (Flag84prime-))
(not (Flag97prime-))
(not (Flag95prime-))
(not (Flag94prime-))
(not (Flag82prime-))
(not (Flag98prime-))
(not (Flag96prime-))
(not (Flag83prime-))
(not (Flag81prime-))
(not (Flag76prime-))
(not (Flag74prime-))
(not (Flag37prime-))
)
)
(:action STOP-PASS_BC-AF
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AF)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag637prime-))
(not (Flag636prime-))
(not (Flag635prime-))
(not (Flag634prime-))
(not (Flag633prime-))
(not (Flag632prime-))
(not (Flag631prime-))
(not (Flag630prime-))
(not (Flag629prime-))
(not (Flag628prime-))
(not (Flag627prime-))
(not (Flag626prime-))
(not (Flag87prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag93prime-))
(not (Flag91prime-))
(not (Flag89prime-))
(not (Flag69prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag92prime-))
(not (Flag85prime-))
(not (Flag84prime-))
(not (Flag97prime-))
(not (Flag95prime-))
(not (Flag94prime-))
(not (Flag82prime-))
(not (Flag98prime-))
(not (Flag96prime-))
(not (Flag83prime-))
(not (Flag81prime-))
(not (Flag76prime-))
(not (Flag74prime-))
(not (Flag37prime-))
)
)
(:action STOP-PASS_BC-AC
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AC)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag637prime-))
(not (Flag636prime-))
(not (Flag635prime-))
(not (Flag634prime-))
(not (Flag633prime-))
(not (Flag632prime-))
(not (Flag631prime-))
(not (Flag630prime-))
(not (Flag629prime-))
(not (Flag628prime-))
(not (Flag627prime-))
(not (Flag626prime-))
(not (Flag87prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag93prime-))
(not (Flag91prime-))
(not (Flag89prime-))
(not (Flag69prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag92prime-))
(not (Flag85prime-))
(not (Flag84prime-))
(not (Flag97prime-))
(not (Flag95prime-))
(not (Flag94prime-))
(not (Flag82prime-))
(not (Flag98prime-))
(not (Flag96prime-))
(not (Flag83prime-))
(not (Flag81prime-))
(not (Flag76prime-))
(not (Flag74prime-))
(not (Flag37prime-))
)
)
(:action STOP-PASS_AU-BD
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-BD)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag637prime-))
(not (Flag636prime-))
(not (Flag635prime-))
(not (Flag634prime-))
(not (Flag633prime-))
(not (Flag632prime-))
(not (Flag631prime-))
(not (Flag630prime-))
(not (Flag629prime-))
(not (Flag628prime-))
(not (Flag627prime-))
(not (Flag626prime-))
(not (Flag87prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag93prime-))
(not (Flag91prime-))
(not (Flag89prime-))
(not (Flag69prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag92prime-))
(not (Flag85prime-))
(not (Flag84prime-))
(not (Flag97prime-))
(not (Flag95prime-))
(not (Flag94prime-))
(not (Flag82prime-))
(not (Flag98prime-))
(not (Flag96prime-))
(not (Flag83prime-))
(not (Flag81prime-))
(not (Flag76prime-))
(not (Flag74prime-))
(not (Flag37prime-))
)
)
(:action STOP-PASS_AU-AZ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AZ)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag637prime-))
(not (Flag636prime-))
(not (Flag635prime-))
(not (Flag634prime-))
(not (Flag633prime-))
(not (Flag632prime-))
(not (Flag631prime-))
(not (Flag630prime-))
(not (Flag629prime-))
(not (Flag628prime-))
(not (Flag627prime-))
(not (Flag626prime-))
(not (Flag87prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag93prime-))
(not (Flag91prime-))
(not (Flag89prime-))
(not (Flag69prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag92prime-))
(not (Flag85prime-))
(not (Flag84prime-))
(not (Flag97prime-))
(not (Flag95prime-))
(not (Flag94prime-))
(not (Flag82prime-))
(not (Flag98prime-))
(not (Flag96prime-))
(not (Flag83prime-))
(not (Flag81prime-))
(not (Flag76prime-))
(not (Flag74prime-))
(not (Flag37prime-))
)
)
(:action STOP-PASS_AU-AY
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AY)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag637prime-))
(not (Flag636prime-))
(not (Flag635prime-))
(not (Flag634prime-))
(not (Flag633prime-))
(not (Flag632prime-))
(not (Flag631prime-))
(not (Flag630prime-))
(not (Flag629prime-))
(not (Flag628prime-))
(not (Flag627prime-))
(not (Flag626prime-))
(not (Flag87prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag93prime-))
(not (Flag91prime-))
(not (Flag89prime-))
(not (Flag69prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag92prime-))
(not (Flag85prime-))
(not (Flag84prime-))
(not (Flag97prime-))
(not (Flag95prime-))
(not (Flag94prime-))
(not (Flag82prime-))
(not (Flag98prime-))
(not (Flag96prime-))
(not (Flag83prime-))
(not (Flag81prime-))
(not (Flag76prime-))
(not (Flag74prime-))
(not (Flag37prime-))
)
)
(:action STOP-PASS_AU-AW
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AW)
(Flag634prime-)
)
:effect
(and
(when
(and
(Flag634-)
)
(and
(BOARDED-PASS_AU)
(not (NOT-BOARDED-PASS_AU))
)
)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag637prime-))
(not (Flag636prime-))
(not (Flag635prime-))
(not (Flag634prime-))
(not (Flag633prime-))
(not (Flag632prime-))
(not (Flag631prime-))
(not (Flag630prime-))
(not (Flag629prime-))
(not (Flag628prime-))
(not (Flag627prime-))
(not (Flag626prime-))
(not (Flag87prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag93prime-))
(not (Flag91prime-))
(not (Flag89prime-))
(not (Flag69prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag92prime-))
(not (Flag85prime-))
(not (Flag84prime-))
(not (Flag97prime-))
(not (Flag95prime-))
(not (Flag94prime-))
(not (Flag82prime-))
(not (Flag98prime-))
(not (Flag96prime-))
(not (Flag83prime-))
(not (Flag81prime-))
(not (Flag76prime-))
(not (Flag74prime-))
(not (Flag37prime-))
)
)
(:action STOP-PASS_AU-AF
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AF)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag637prime-))
(not (Flag636prime-))
(not (Flag635prime-))
(not (Flag634prime-))
(not (Flag633prime-))
(not (Flag632prime-))
(not (Flag631prime-))
(not (Flag630prime-))
(not (Flag629prime-))
(not (Flag628prime-))
(not (Flag627prime-))
(not (Flag626prime-))
(not (Flag87prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag93prime-))
(not (Flag91prime-))
(not (Flag89prime-))
(not (Flag69prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag92prime-))
(not (Flag85prime-))
(not (Flag84prime-))
(not (Flag97prime-))
(not (Flag95prime-))
(not (Flag94prime-))
(not (Flag82prime-))
(not (Flag98prime-))
(not (Flag96prime-))
(not (Flag83prime-))
(not (Flag81prime-))
(not (Flag76prime-))
(not (Flag74prime-))
(not (Flag37prime-))
)
)
(:action STOP-PASS_AU-AC
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AC)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag637prime-))
(not (Flag636prime-))
(not (Flag635prime-))
(not (Flag634prime-))
(not (Flag633prime-))
(not (Flag632prime-))
(not (Flag631prime-))
(not (Flag630prime-))
(not (Flag629prime-))
(not (Flag628prime-))
(not (Flag627prime-))
(not (Flag626prime-))
(not (Flag87prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag93prime-))
(not (Flag91prime-))
(not (Flag89prime-))
(not (Flag69prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag92prime-))
(not (Flag85prime-))
(not (Flag84prime-))
(not (Flag97prime-))
(not (Flag95prime-))
(not (Flag94prime-))
(not (Flag82prime-))
(not (Flag98prime-))
(not (Flag96prime-))
(not (Flag83prime-))
(not (Flag81prime-))
(not (Flag76prime-))
(not (Flag74prime-))
(not (Flag37prime-))
)
)
(:action STOP-PASS_AX-BD
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-BD)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag637prime-))
(not (Flag636prime-))
(not (Flag635prime-))
(not (Flag634prime-))
(not (Flag633prime-))
(not (Flag632prime-))
(not (Flag631prime-))
(not (Flag630prime-))
(not (Flag629prime-))
(not (Flag628prime-))
(not (Flag627prime-))
(not (Flag626prime-))
(not (Flag87prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag93prime-))
(not (Flag91prime-))
(not (Flag89prime-))
(not (Flag69prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag92prime-))
(not (Flag85prime-))
(not (Flag84prime-))
(not (Flag97prime-))
(not (Flag95prime-))
(not (Flag94prime-))
(not (Flag82prime-))
(not (Flag98prime-))
(not (Flag96prime-))
(not (Flag83prime-))
(not (Flag81prime-))
(not (Flag76prime-))
(not (Flag74prime-))
(not (Flag37prime-))
)
)
(:action STOP-PASS_AX-AZ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AZ)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag637prime-))
(not (Flag636prime-))
(not (Flag635prime-))
(not (Flag634prime-))
(not (Flag633prime-))
(not (Flag632prime-))
(not (Flag631prime-))
(not (Flag630prime-))
(not (Flag629prime-))
(not (Flag628prime-))
(not (Flag627prime-))
(not (Flag626prime-))
(not (Flag87prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag93prime-))
(not (Flag91prime-))
(not (Flag89prime-))
(not (Flag69prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag92prime-))
(not (Flag85prime-))
(not (Flag84prime-))
(not (Flag97prime-))
(not (Flag95prime-))
(not (Flag94prime-))
(not (Flag82prime-))
(not (Flag98prime-))
(not (Flag96prime-))
(not (Flag83prime-))
(not (Flag81prime-))
(not (Flag76prime-))
(not (Flag74prime-))
(not (Flag37prime-))
)
)
(:action STOP-PASS_AX-AY
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AY)
(Flag633prime-)
)
:effect
(and
(when
(and
(Flag633-)
)
(and
(BOARDED-PASS_AX)
(not (NOT-BOARDED-PASS_AX))
)
)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag637prime-))
(not (Flag636prime-))
(not (Flag635prime-))
(not (Flag634prime-))
(not (Flag633prime-))
(not (Flag632prime-))
(not (Flag631prime-))
(not (Flag630prime-))
(not (Flag629prime-))
(not (Flag628prime-))
(not (Flag627prime-))
(not (Flag626prime-))
(not (Flag87prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag93prime-))
(not (Flag91prime-))
(not (Flag89prime-))
(not (Flag69prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag92prime-))
(not (Flag85prime-))
(not (Flag84prime-))
(not (Flag97prime-))
(not (Flag95prime-))
(not (Flag94prime-))
(not (Flag82prime-))
(not (Flag98prime-))
(not (Flag96prime-))
(not (Flag83prime-))
(not (Flag81prime-))
(not (Flag76prime-))
(not (Flag74prime-))
(not (Flag37prime-))
)
)
(:action STOP-PASS_AX-AW
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AW)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag637prime-))
(not (Flag636prime-))
(not (Flag635prime-))
(not (Flag634prime-))
(not (Flag633prime-))
(not (Flag632prime-))
(not (Flag631prime-))
(not (Flag630prime-))
(not (Flag629prime-))
(not (Flag628prime-))
(not (Flag627prime-))
(not (Flag626prime-))
(not (Flag87prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag93prime-))
(not (Flag91prime-))
(not (Flag89prime-))
(not (Flag69prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag92prime-))
(not (Flag85prime-))
(not (Flag84prime-))
(not (Flag97prime-))
(not (Flag95prime-))
(not (Flag94prime-))
(not (Flag82prime-))
(not (Flag98prime-))
(not (Flag96prime-))
(not (Flag83prime-))
(not (Flag81prime-))
(not (Flag76prime-))
(not (Flag74prime-))
(not (Flag37prime-))
)
)
(:action STOP-PASS_AX-AF
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AF)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag637prime-))
(not (Flag636prime-))
(not (Flag635prime-))
(not (Flag634prime-))
(not (Flag633prime-))
(not (Flag632prime-))
(not (Flag631prime-))
(not (Flag630prime-))
(not (Flag629prime-))
(not (Flag628prime-))
(not (Flag627prime-))
(not (Flag626prime-))
(not (Flag87prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag93prime-))
(not (Flag91prime-))
(not (Flag89prime-))
(not (Flag69prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag92prime-))
(not (Flag85prime-))
(not (Flag84prime-))
(not (Flag97prime-))
(not (Flag95prime-))
(not (Flag94prime-))
(not (Flag82prime-))
(not (Flag98prime-))
(not (Flag96prime-))
(not (Flag83prime-))
(not (Flag81prime-))
(not (Flag76prime-))
(not (Flag74prime-))
(not (Flag37prime-))
)
)
(:action STOP-PASS_AX-AC
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AC)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag637prime-))
(not (Flag636prime-))
(not (Flag635prime-))
(not (Flag634prime-))
(not (Flag633prime-))
(not (Flag632prime-))
(not (Flag631prime-))
(not (Flag630prime-))
(not (Flag629prime-))
(not (Flag628prime-))
(not (Flag627prime-))
(not (Flag626prime-))
(not (Flag87prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag93prime-))
(not (Flag91prime-))
(not (Flag89prime-))
(not (Flag69prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag92prime-))
(not (Flag85prime-))
(not (Flag84prime-))
(not (Flag97prime-))
(not (Flag95prime-))
(not (Flag94prime-))
(not (Flag82prime-))
(not (Flag98prime-))
(not (Flag96prime-))
(not (Flag83prime-))
(not (Flag81prime-))
(not (Flag76prime-))
(not (Flag74prime-))
(not (Flag37prime-))
)
)
(:action STOP-PASS_AA-BD
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-BD)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag637prime-))
(not (Flag636prime-))
(not (Flag635prime-))
(not (Flag634prime-))
(not (Flag633prime-))
(not (Flag632prime-))
(not (Flag631prime-))
(not (Flag630prime-))
(not (Flag629prime-))
(not (Flag628prime-))
(not (Flag627prime-))
(not (Flag626prime-))
(not (Flag87prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag93prime-))
(not (Flag91prime-))
(not (Flag89prime-))
(not (Flag69prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag92prime-))
(not (Flag85prime-))
(not (Flag84prime-))
(not (Flag97prime-))
(not (Flag95prime-))
(not (Flag94prime-))
(not (Flag82prime-))
(not (Flag98prime-))
(not (Flag96prime-))
(not (Flag83prime-))
(not (Flag81prime-))
(not (Flag76prime-))
(not (Flag74prime-))
(not (Flag37prime-))
)
)
(:action STOP-PASS_AA-AZ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AZ)
)
:effect
(and
(when
(and
(BOARDED-PASS_AA)
)
(and
(SERVED-PASS_AA)
(NOT-BOARDED-PASS_AA)
(not (NOT-SERVED-PASS_AA))
(not (BOARDED-PASS_AA))
)
)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag637prime-))
(not (Flag636prime-))
(not (Flag635prime-))
(not (Flag634prime-))
(not (Flag633prime-))
(not (Flag632prime-))
(not (Flag631prime-))
(not (Flag630prime-))
(not (Flag629prime-))
(not (Flag628prime-))
(not (Flag627prime-))
(not (Flag626prime-))
(not (Flag87prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag93prime-))
(not (Flag91prime-))
(not (Flag89prime-))
(not (Flag69prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag92prime-))
(not (Flag85prime-))
(not (Flag84prime-))
(not (Flag97prime-))
(not (Flag95prime-))
(not (Flag94prime-))
(not (Flag82prime-))
(not (Flag98prime-))
(not (Flag96prime-))
(not (Flag83prime-))
(not (Flag81prime-))
(not (Flag76prime-))
(not (Flag74prime-))
(not (Flag37prime-))
)
)
(:action STOP-PASS_AA-AY
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AY)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag637prime-))
(not (Flag636prime-))
(not (Flag635prime-))
(not (Flag634prime-))
(not (Flag633prime-))
(not (Flag632prime-))
(not (Flag631prime-))
(not (Flag630prime-))
(not (Flag629prime-))
(not (Flag628prime-))
(not (Flag627prime-))
(not (Flag626prime-))
(not (Flag87prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag93prime-))
(not (Flag91prime-))
(not (Flag89prime-))
(not (Flag69prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag92prime-))
(not (Flag85prime-))
(not (Flag84prime-))
(not (Flag97prime-))
(not (Flag95prime-))
(not (Flag94prime-))
(not (Flag82prime-))
(not (Flag98prime-))
(not (Flag96prime-))
(not (Flag83prime-))
(not (Flag81prime-))
(not (Flag76prime-))
(not (Flag74prime-))
(not (Flag37prime-))
)
)
(:action STOP-PASS_AA-AW
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AW)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag637prime-))
(not (Flag636prime-))
(not (Flag635prime-))
(not (Flag634prime-))
(not (Flag633prime-))
(not (Flag632prime-))
(not (Flag631prime-))
(not (Flag630prime-))
(not (Flag629prime-))
(not (Flag628prime-))
(not (Flag627prime-))
(not (Flag626prime-))
(not (Flag87prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag93prime-))
(not (Flag91prime-))
(not (Flag89prime-))
(not (Flag69prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag92prime-))
(not (Flag85prime-))
(not (Flag84prime-))
(not (Flag97prime-))
(not (Flag95prime-))
(not (Flag94prime-))
(not (Flag82prime-))
(not (Flag98prime-))
(not (Flag96prime-))
(not (Flag83prime-))
(not (Flag81prime-))
(not (Flag76prime-))
(not (Flag74prime-))
(not (Flag37prime-))
)
)
(:action STOP-PASS_AA-AF
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AF)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag637prime-))
(not (Flag636prime-))
(not (Flag635prime-))
(not (Flag634prime-))
(not (Flag633prime-))
(not (Flag632prime-))
(not (Flag631prime-))
(not (Flag630prime-))
(not (Flag629prime-))
(not (Flag628prime-))
(not (Flag627prime-))
(not (Flag626prime-))
(not (Flag87prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag93prime-))
(not (Flag91prime-))
(not (Flag89prime-))
(not (Flag69prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag92prime-))
(not (Flag85prime-))
(not (Flag84prime-))
(not (Flag97prime-))
(not (Flag95prime-))
(not (Flag94prime-))
(not (Flag82prime-))
(not (Flag98prime-))
(not (Flag96prime-))
(not (Flag83prime-))
(not (Flag81prime-))
(not (Flag76prime-))
(not (Flag74prime-))
(not (Flag37prime-))
)
)
(:action STOP-PASS_AA-AC
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AC)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag637prime-))
(not (Flag636prime-))
(not (Flag635prime-))
(not (Flag634prime-))
(not (Flag633prime-))
(not (Flag632prime-))
(not (Flag631prime-))
(not (Flag630prime-))
(not (Flag629prime-))
(not (Flag628prime-))
(not (Flag627prime-))
(not (Flag626prime-))
(not (Flag87prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag93prime-))
(not (Flag91prime-))
(not (Flag89prime-))
(not (Flag69prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag92prime-))
(not (Flag85prime-))
(not (Flag84prime-))
(not (Flag97prime-))
(not (Flag95prime-))
(not (Flag94prime-))
(not (Flag82prime-))
(not (Flag98prime-))
(not (Flag96prime-))
(not (Flag83prime-))
(not (Flag81prime-))
(not (Flag76prime-))
(not (Flag74prime-))
(not (Flag37prime-))
)
)
(:action STOP-PASS_AB-BD
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-BD)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag637prime-))
(not (Flag636prime-))
(not (Flag635prime-))
(not (Flag634prime-))
(not (Flag633prime-))
(not (Flag632prime-))
(not (Flag631prime-))
(not (Flag630prime-))
(not (Flag629prime-))
(not (Flag628prime-))
(not (Flag627prime-))
(not (Flag626prime-))
(not (Flag87prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag93prime-))
(not (Flag91prime-))
(not (Flag89prime-))
(not (Flag69prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag92prime-))
(not (Flag85prime-))
(not (Flag84prime-))
(not (Flag97prime-))
(not (Flag95prime-))
(not (Flag94prime-))
(not (Flag82prime-))
(not (Flag98prime-))
(not (Flag96prime-))
(not (Flag83prime-))
(not (Flag81prime-))
(not (Flag76prime-))
(not (Flag74prime-))
(not (Flag37prime-))
)
)
(:action STOP-PASS_AB-AZ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AZ)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag637prime-))
(not (Flag636prime-))
(not (Flag635prime-))
(not (Flag634prime-))
(not (Flag633prime-))
(not (Flag632prime-))
(not (Flag631prime-))
(not (Flag630prime-))
(not (Flag629prime-))
(not (Flag628prime-))
(not (Flag627prime-))
(not (Flag626prime-))
(not (Flag87prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag93prime-))
(not (Flag91prime-))
(not (Flag89prime-))
(not (Flag69prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag92prime-))
(not (Flag85prime-))
(not (Flag84prime-))
(not (Flag97prime-))
(not (Flag95prime-))
(not (Flag94prime-))
(not (Flag82prime-))
(not (Flag98prime-))
(not (Flag96prime-))
(not (Flag83prime-))
(not (Flag81prime-))
(not (Flag76prime-))
(not (Flag74prime-))
(not (Flag37prime-))
)
)
(:action STOP-PASS_AB-AY
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AY)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag637prime-))
(not (Flag636prime-))
(not (Flag635prime-))
(not (Flag634prime-))
(not (Flag633prime-))
(not (Flag632prime-))
(not (Flag631prime-))
(not (Flag630prime-))
(not (Flag629prime-))
(not (Flag628prime-))
(not (Flag627prime-))
(not (Flag626prime-))
(not (Flag87prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag93prime-))
(not (Flag91prime-))
(not (Flag89prime-))
(not (Flag69prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag92prime-))
(not (Flag85prime-))
(not (Flag84prime-))
(not (Flag97prime-))
(not (Flag95prime-))
(not (Flag94prime-))
(not (Flag82prime-))
(not (Flag98prime-))
(not (Flag96prime-))
(not (Flag83prime-))
(not (Flag81prime-))
(not (Flag76prime-))
(not (Flag74prime-))
(not (Flag37prime-))
)
)
(:action STOP-PASS_AB-AW
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AW)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag637prime-))
(not (Flag636prime-))
(not (Flag635prime-))
(not (Flag634prime-))
(not (Flag633prime-))
(not (Flag632prime-))
(not (Flag631prime-))
(not (Flag630prime-))
(not (Flag629prime-))
(not (Flag628prime-))
(not (Flag627prime-))
(not (Flag626prime-))
(not (Flag87prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag93prime-))
(not (Flag91prime-))
(not (Flag89prime-))
(not (Flag69prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag92prime-))
(not (Flag85prime-))
(not (Flag84prime-))
(not (Flag97prime-))
(not (Flag95prime-))
(not (Flag94prime-))
(not (Flag82prime-))
(not (Flag98prime-))
(not (Flag96prime-))
(not (Flag83prime-))
(not (Flag81prime-))
(not (Flag76prime-))
(not (Flag74prime-))
(not (Flag37prime-))
)
)
(:action STOP-PASS_AB-AF
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AF)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag637prime-))
(not (Flag636prime-))
(not (Flag635prime-))
(not (Flag634prime-))
(not (Flag633prime-))
(not (Flag632prime-))
(not (Flag631prime-))
(not (Flag630prime-))
(not (Flag629prime-))
(not (Flag628prime-))
(not (Flag627prime-))
(not (Flag626prime-))
(not (Flag87prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag93prime-))
(not (Flag91prime-))
(not (Flag89prime-))
(not (Flag69prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag92prime-))
(not (Flag85prime-))
(not (Flag84prime-))
(not (Flag97prime-))
(not (Flag95prime-))
(not (Flag94prime-))
(not (Flag82prime-))
(not (Flag98prime-))
(not (Flag96prime-))
(not (Flag83prime-))
(not (Flag81prime-))
(not (Flag76prime-))
(not (Flag74prime-))
(not (Flag37prime-))
)
)
(:action STOP-PASS_AB-AC
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AC)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag637prime-))
(not (Flag636prime-))
(not (Flag635prime-))
(not (Flag634prime-))
(not (Flag633prime-))
(not (Flag632prime-))
(not (Flag631prime-))
(not (Flag630prime-))
(not (Flag629prime-))
(not (Flag628prime-))
(not (Flag627prime-))
(not (Flag626prime-))
(not (Flag87prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag93prime-))
(not (Flag91prime-))
(not (Flag89prime-))
(not (Flag69prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag92prime-))
(not (Flag85prime-))
(not (Flag84prime-))
(not (Flag97prime-))
(not (Flag95prime-))
(not (Flag94prime-))
(not (Flag82prime-))
(not (Flag98prime-))
(not (Flag96prime-))
(not (Flag83prime-))
(not (Flag81prime-))
(not (Flag76prime-))
(not (Flag74prime-))
(not (Flag37prime-))
)
)
(:action STOP-PASS_AE-BD
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-BD)
(Flag630prime-)
)
:effect
(and
(when
(and
(Flag630-)
)
(and
(BOARDED-PASS_AE)
(not (NOT-BOARDED-PASS_AE))
)
)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag637prime-))
(not (Flag636prime-))
(not (Flag635prime-))
(not (Flag634prime-))
(not (Flag633prime-))
(not (Flag632prime-))
(not (Flag631prime-))
(not (Flag630prime-))
(not (Flag629prime-))
(not (Flag628prime-))
(not (Flag627prime-))
(not (Flag626prime-))
(not (Flag87prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag93prime-))
(not (Flag91prime-))
(not (Flag89prime-))
(not (Flag69prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag92prime-))
(not (Flag85prime-))
(not (Flag84prime-))
(not (Flag97prime-))
(not (Flag95prime-))
(not (Flag94prime-))
(not (Flag82prime-))
(not (Flag98prime-))
(not (Flag96prime-))
(not (Flag83prime-))
(not (Flag81prime-))
(not (Flag76prime-))
(not (Flag74prime-))
(not (Flag37prime-))
)
)
(:action STOP-PASS_AE-AZ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AZ)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag637prime-))
(not (Flag636prime-))
(not (Flag635prime-))
(not (Flag634prime-))
(not (Flag633prime-))
(not (Flag632prime-))
(not (Flag631prime-))
(not (Flag630prime-))
(not (Flag629prime-))
(not (Flag628prime-))
(not (Flag627prime-))
(not (Flag626prime-))
(not (Flag87prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag93prime-))
(not (Flag91prime-))
(not (Flag89prime-))
(not (Flag69prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag92prime-))
(not (Flag85prime-))
(not (Flag84prime-))
(not (Flag97prime-))
(not (Flag95prime-))
(not (Flag94prime-))
(not (Flag82prime-))
(not (Flag98prime-))
(not (Flag96prime-))
(not (Flag83prime-))
(not (Flag81prime-))
(not (Flag76prime-))
(not (Flag74prime-))
(not (Flag37prime-))
)
)
(:action STOP-PASS_AE-AY
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AY)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag637prime-))
(not (Flag636prime-))
(not (Flag635prime-))
(not (Flag634prime-))
(not (Flag633prime-))
(not (Flag632prime-))
(not (Flag631prime-))
(not (Flag630prime-))
(not (Flag629prime-))
(not (Flag628prime-))
(not (Flag627prime-))
(not (Flag626prime-))
(not (Flag87prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag93prime-))
(not (Flag91prime-))
(not (Flag89prime-))
(not (Flag69prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag92prime-))
(not (Flag85prime-))
(not (Flag84prime-))
(not (Flag97prime-))
(not (Flag95prime-))
(not (Flag94prime-))
(not (Flag82prime-))
(not (Flag98prime-))
(not (Flag96prime-))
(not (Flag83prime-))
(not (Flag81prime-))
(not (Flag76prime-))
(not (Flag74prime-))
(not (Flag37prime-))
)
)
(:action STOP-PASS_AE-AW
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AW)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag637prime-))
(not (Flag636prime-))
(not (Flag635prime-))
(not (Flag634prime-))
(not (Flag633prime-))
(not (Flag632prime-))
(not (Flag631prime-))
(not (Flag630prime-))
(not (Flag629prime-))
(not (Flag628prime-))
(not (Flag627prime-))
(not (Flag626prime-))
(not (Flag87prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag93prime-))
(not (Flag91prime-))
(not (Flag89prime-))
(not (Flag69prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag92prime-))
(not (Flag85prime-))
(not (Flag84prime-))
(not (Flag97prime-))
(not (Flag95prime-))
(not (Flag94prime-))
(not (Flag82prime-))
(not (Flag98prime-))
(not (Flag96prime-))
(not (Flag83prime-))
(not (Flag81prime-))
(not (Flag76prime-))
(not (Flag74prime-))
(not (Flag37prime-))
)
)
(:action STOP-PASS_AE-AF
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AF)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag637prime-))
(not (Flag636prime-))
(not (Flag635prime-))
(not (Flag634prime-))
(not (Flag633prime-))
(not (Flag632prime-))
(not (Flag631prime-))
(not (Flag630prime-))
(not (Flag629prime-))
(not (Flag628prime-))
(not (Flag627prime-))
(not (Flag626prime-))
(not (Flag87prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag93prime-))
(not (Flag91prime-))
(not (Flag89prime-))
(not (Flag69prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag92prime-))
(not (Flag85prime-))
(not (Flag84prime-))
(not (Flag97prime-))
(not (Flag95prime-))
(not (Flag94prime-))
(not (Flag82prime-))
(not (Flag98prime-))
(not (Flag96prime-))
(not (Flag83prime-))
(not (Flag81prime-))
(not (Flag76prime-))
(not (Flag74prime-))
(not (Flag37prime-))
)
)
(:action STOP-PASS_AE-AC
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AC)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag637prime-))
(not (Flag636prime-))
(not (Flag635prime-))
(not (Flag634prime-))
(not (Flag633prime-))
(not (Flag632prime-))
(not (Flag631prime-))
(not (Flag630prime-))
(not (Flag629prime-))
(not (Flag628prime-))
(not (Flag627prime-))
(not (Flag626prime-))
(not (Flag87prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag93prime-))
(not (Flag91prime-))
(not (Flag89prime-))
(not (Flag69prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag92prime-))
(not (Flag85prime-))
(not (Flag84prime-))
(not (Flag97prime-))
(not (Flag95prime-))
(not (Flag94prime-))
(not (Flag82prime-))
(not (Flag98prime-))
(not (Flag96prime-))
(not (Flag83prime-))
(not (Flag81prime-))
(not (Flag76prime-))
(not (Flag74prime-))
(not (Flag37prime-))
)
)
(:action STOP-PASS_AD-BD
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-BD)
(Flag629prime-)
)
:effect
(and
(when
(and
(Flag629-)
)
(and
(BOARDED-PASS_AD)
(not (NOT-BOARDED-PASS_AD))
)
)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag637prime-))
(not (Flag636prime-))
(not (Flag635prime-))
(not (Flag634prime-))
(not (Flag633prime-))
(not (Flag632prime-))
(not (Flag631prime-))
(not (Flag630prime-))
(not (Flag629prime-))
(not (Flag628prime-))
(not (Flag627prime-))
(not (Flag626prime-))
(not (Flag87prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag93prime-))
(not (Flag91prime-))
(not (Flag89prime-))
(not (Flag69prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag92prime-))
(not (Flag85prime-))
(not (Flag84prime-))
(not (Flag97prime-))
(not (Flag95prime-))
(not (Flag94prime-))
(not (Flag82prime-))
(not (Flag98prime-))
(not (Flag96prime-))
(not (Flag83prime-))
(not (Flag81prime-))
(not (Flag76prime-))
(not (Flag74prime-))
(not (Flag37prime-))
)
)
(:action STOP-PASS_AD-AZ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AZ)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag637prime-))
(not (Flag636prime-))
(not (Flag635prime-))
(not (Flag634prime-))
(not (Flag633prime-))
(not (Flag632prime-))
(not (Flag631prime-))
(not (Flag630prime-))
(not (Flag629prime-))
(not (Flag628prime-))
(not (Flag627prime-))
(not (Flag626prime-))
(not (Flag87prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag93prime-))
(not (Flag91prime-))
(not (Flag89prime-))
(not (Flag69prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag92prime-))
(not (Flag85prime-))
(not (Flag84prime-))
(not (Flag97prime-))
(not (Flag95prime-))
(not (Flag94prime-))
(not (Flag82prime-))
(not (Flag98prime-))
(not (Flag96prime-))
(not (Flag83prime-))
(not (Flag81prime-))
(not (Flag76prime-))
(not (Flag74prime-))
(not (Flag37prime-))
)
)
(:action STOP-PASS_AD-AY
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AY)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag637prime-))
(not (Flag636prime-))
(not (Flag635prime-))
(not (Flag634prime-))
(not (Flag633prime-))
(not (Flag632prime-))
(not (Flag631prime-))
(not (Flag630prime-))
(not (Flag629prime-))
(not (Flag628prime-))
(not (Flag627prime-))
(not (Flag626prime-))
(not (Flag87prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag93prime-))
(not (Flag91prime-))
(not (Flag89prime-))
(not (Flag69prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag92prime-))
(not (Flag85prime-))
(not (Flag84prime-))
(not (Flag97prime-))
(not (Flag95prime-))
(not (Flag94prime-))
(not (Flag82prime-))
(not (Flag98prime-))
(not (Flag96prime-))
(not (Flag83prime-))
(not (Flag81prime-))
(not (Flag76prime-))
(not (Flag74prime-))
(not (Flag37prime-))
)
)
(:action STOP-PASS_AD-AW
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AW)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag637prime-))
(not (Flag636prime-))
(not (Flag635prime-))
(not (Flag634prime-))
(not (Flag633prime-))
(not (Flag632prime-))
(not (Flag631prime-))
(not (Flag630prime-))
(not (Flag629prime-))
(not (Flag628prime-))
(not (Flag627prime-))
(not (Flag626prime-))
(not (Flag87prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag93prime-))
(not (Flag91prime-))
(not (Flag89prime-))
(not (Flag69prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag92prime-))
(not (Flag85prime-))
(not (Flag84prime-))
(not (Flag97prime-))
(not (Flag95prime-))
(not (Flag94prime-))
(not (Flag82prime-))
(not (Flag98prime-))
(not (Flag96prime-))
(not (Flag83prime-))
(not (Flag81prime-))
(not (Flag76prime-))
(not (Flag74prime-))
(not (Flag37prime-))
)
)
(:action STOP-PASS_AD-AF
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AF)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag637prime-))
(not (Flag636prime-))
(not (Flag635prime-))
(not (Flag634prime-))
(not (Flag633prime-))
(not (Flag632prime-))
(not (Flag631prime-))
(not (Flag630prime-))
(not (Flag629prime-))
(not (Flag628prime-))
(not (Flag627prime-))
(not (Flag626prime-))
(not (Flag87prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag93prime-))
(not (Flag91prime-))
(not (Flag89prime-))
(not (Flag69prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag92prime-))
(not (Flag85prime-))
(not (Flag84prime-))
(not (Flag97prime-))
(not (Flag95prime-))
(not (Flag94prime-))
(not (Flag82prime-))
(not (Flag98prime-))
(not (Flag96prime-))
(not (Flag83prime-))
(not (Flag81prime-))
(not (Flag76prime-))
(not (Flag74prime-))
(not (Flag37prime-))
)
)
(:action STOP-PASS_AD-AC
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AC)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag637prime-))
(not (Flag636prime-))
(not (Flag635prime-))
(not (Flag634prime-))
(not (Flag633prime-))
(not (Flag632prime-))
(not (Flag631prime-))
(not (Flag630prime-))
(not (Flag629prime-))
(not (Flag628prime-))
(not (Flag627prime-))
(not (Flag626prime-))
(not (Flag87prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag93prime-))
(not (Flag91prime-))
(not (Flag89prime-))
(not (Flag69prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag92prime-))
(not (Flag85prime-))
(not (Flag84prime-))
(not (Flag97prime-))
(not (Flag95prime-))
(not (Flag94prime-))
(not (Flag82prime-))
(not (Flag98prime-))
(not (Flag96prime-))
(not (Flag83prime-))
(not (Flag81prime-))
(not (Flag76prime-))
(not (Flag74prime-))
(not (Flag37prime-))
)
)
(:action STOP-PASS_AL-BD
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-BD)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag637prime-))
(not (Flag636prime-))
(not (Flag635prime-))
(not (Flag634prime-))
(not (Flag633prime-))
(not (Flag632prime-))
(not (Flag631prime-))
(not (Flag630prime-))
(not (Flag629prime-))
(not (Flag628prime-))
(not (Flag627prime-))
(not (Flag626prime-))
(not (Flag87prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag93prime-))
(not (Flag91prime-))
(not (Flag89prime-))
(not (Flag69prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag92prime-))
(not (Flag85prime-))
(not (Flag84prime-))
(not (Flag97prime-))
(not (Flag95prime-))
(not (Flag94prime-))
(not (Flag82prime-))
(not (Flag98prime-))
(not (Flag96prime-))
(not (Flag83prime-))
(not (Flag81prime-))
(not (Flag76prime-))
(not (Flag74prime-))
(not (Flag37prime-))
)
)
(:action STOP-PASS_AL-AZ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AZ)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag637prime-))
(not (Flag636prime-))
(not (Flag635prime-))
(not (Flag634prime-))
(not (Flag633prime-))
(not (Flag632prime-))
(not (Flag631prime-))
(not (Flag630prime-))
(not (Flag629prime-))
(not (Flag628prime-))
(not (Flag627prime-))
(not (Flag626prime-))
(not (Flag87prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag93prime-))
(not (Flag91prime-))
(not (Flag89prime-))
(not (Flag69prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag92prime-))
(not (Flag85prime-))
(not (Flag84prime-))
(not (Flag97prime-))
(not (Flag95prime-))
(not (Flag94prime-))
(not (Flag82prime-))
(not (Flag98prime-))
(not (Flag96prime-))
(not (Flag83prime-))
(not (Flag81prime-))
(not (Flag76prime-))
(not (Flag74prime-))
(not (Flag37prime-))
)
)
(:action STOP-PASS_AL-AY
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AY)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag637prime-))
(not (Flag636prime-))
(not (Flag635prime-))
(not (Flag634prime-))
(not (Flag633prime-))
(not (Flag632prime-))
(not (Flag631prime-))
(not (Flag630prime-))
(not (Flag629prime-))
(not (Flag628prime-))
(not (Flag627prime-))
(not (Flag626prime-))
(not (Flag87prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag93prime-))
(not (Flag91prime-))
(not (Flag89prime-))
(not (Flag69prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag92prime-))
(not (Flag85prime-))
(not (Flag84prime-))
(not (Flag97prime-))
(not (Flag95prime-))
(not (Flag94prime-))
(not (Flag82prime-))
(not (Flag98prime-))
(not (Flag96prime-))
(not (Flag83prime-))
(not (Flag81prime-))
(not (Flag76prime-))
(not (Flag74prime-))
(not (Flag37prime-))
)
)
(:action STOP-PASS_AL-AW
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AW)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag637prime-))
(not (Flag636prime-))
(not (Flag635prime-))
(not (Flag634prime-))
(not (Flag633prime-))
(not (Flag632prime-))
(not (Flag631prime-))
(not (Flag630prime-))
(not (Flag629prime-))
(not (Flag628prime-))
(not (Flag627prime-))
(not (Flag626prime-))
(not (Flag87prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag93prime-))
(not (Flag91prime-))
(not (Flag89prime-))
(not (Flag69prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag92prime-))
(not (Flag85prime-))
(not (Flag84prime-))
(not (Flag97prime-))
(not (Flag95prime-))
(not (Flag94prime-))
(not (Flag82prime-))
(not (Flag98prime-))
(not (Flag96prime-))
(not (Flag83prime-))
(not (Flag81prime-))
(not (Flag76prime-))
(not (Flag74prime-))
(not (Flag37prime-))
)
)
(:action STOP-PASS_AL-AF
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AF)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag637prime-))
(not (Flag636prime-))
(not (Flag635prime-))
(not (Flag634prime-))
(not (Flag633prime-))
(not (Flag632prime-))
(not (Flag631prime-))
(not (Flag630prime-))
(not (Flag629prime-))
(not (Flag628prime-))
(not (Flag627prime-))
(not (Flag626prime-))
(not (Flag87prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag93prime-))
(not (Flag91prime-))
(not (Flag89prime-))
(not (Flag69prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag92prime-))
(not (Flag85prime-))
(not (Flag84prime-))
(not (Flag97prime-))
(not (Flag95prime-))
(not (Flag94prime-))
(not (Flag82prime-))
(not (Flag98prime-))
(not (Flag96prime-))
(not (Flag83prime-))
(not (Flag81prime-))
(not (Flag76prime-))
(not (Flag74prime-))
(not (Flag37prime-))
)
)
(:action STOP-PASS_AL-AC
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AC)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag637prime-))
(not (Flag636prime-))
(not (Flag635prime-))
(not (Flag634prime-))
(not (Flag633prime-))
(not (Flag632prime-))
(not (Flag631prime-))
(not (Flag630prime-))
(not (Flag629prime-))
(not (Flag628prime-))
(not (Flag627prime-))
(not (Flag626prime-))
(not (Flag87prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag93prime-))
(not (Flag91prime-))
(not (Flag89prime-))
(not (Flag69prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag92prime-))
(not (Flag85prime-))
(not (Flag84prime-))
(not (Flag97prime-))
(not (Flag95prime-))
(not (Flag94prime-))
(not (Flag82prime-))
(not (Flag98prime-))
(not (Flag96prime-))
(not (Flag83prime-))
(not (Flag81prime-))
(not (Flag76prime-))
(not (Flag74prime-))
(not (Flag37prime-))
)
)
(:action STOP-PASS_AN-BD
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-BD)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag637prime-))
(not (Flag636prime-))
(not (Flag635prime-))
(not (Flag634prime-))
(not (Flag633prime-))
(not (Flag632prime-))
(not (Flag631prime-))
(not (Flag630prime-))
(not (Flag629prime-))
(not (Flag628prime-))
(not (Flag627prime-))
(not (Flag626prime-))
(not (Flag87prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag93prime-))
(not (Flag91prime-))
(not (Flag89prime-))
(not (Flag69prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag92prime-))
(not (Flag85prime-))
(not (Flag84prime-))
(not (Flag97prime-))
(not (Flag95prime-))
(not (Flag94prime-))
(not (Flag82prime-))
(not (Flag98prime-))
(not (Flag96prime-))
(not (Flag83prime-))
(not (Flag81prime-))
(not (Flag76prime-))
(not (Flag74prime-))
(not (Flag37prime-))
)
)
(:action STOP-PASS_AN-AZ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AZ)
(Flag627prime-)
)
:effect
(and
(when
(and
(Flag627-)
)
(and
(BOARDED-PASS_AN)
(not (NOT-BOARDED-PASS_AN))
)
)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag637prime-))
(not (Flag636prime-))
(not (Flag635prime-))
(not (Flag634prime-))
(not (Flag633prime-))
(not (Flag632prime-))
(not (Flag631prime-))
(not (Flag630prime-))
(not (Flag629prime-))
(not (Flag628prime-))
(not (Flag627prime-))
(not (Flag626prime-))
(not (Flag87prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag93prime-))
(not (Flag91prime-))
(not (Flag89prime-))
(not (Flag69prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag92prime-))
(not (Flag85prime-))
(not (Flag84prime-))
(not (Flag97prime-))
(not (Flag95prime-))
(not (Flag94prime-))
(not (Flag82prime-))
(not (Flag98prime-))
(not (Flag96prime-))
(not (Flag83prime-))
(not (Flag81prime-))
(not (Flag76prime-))
(not (Flag74prime-))
(not (Flag37prime-))
)
)
(:action STOP-PASS_AN-AY
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AY)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag637prime-))
(not (Flag636prime-))
(not (Flag635prime-))
(not (Flag634prime-))
(not (Flag633prime-))
(not (Flag632prime-))
(not (Flag631prime-))
(not (Flag630prime-))
(not (Flag629prime-))
(not (Flag628prime-))
(not (Flag627prime-))
(not (Flag626prime-))
(not (Flag87prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag93prime-))
(not (Flag91prime-))
(not (Flag89prime-))
(not (Flag69prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag92prime-))
(not (Flag85prime-))
(not (Flag84prime-))
(not (Flag97prime-))
(not (Flag95prime-))
(not (Flag94prime-))
(not (Flag82prime-))
(not (Flag98prime-))
(not (Flag96prime-))
(not (Flag83prime-))
(not (Flag81prime-))
(not (Flag76prime-))
(not (Flag74prime-))
(not (Flag37prime-))
)
)
(:action STOP-PASS_AN-AW
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AW)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag637prime-))
(not (Flag636prime-))
(not (Flag635prime-))
(not (Flag634prime-))
(not (Flag633prime-))
(not (Flag632prime-))
(not (Flag631prime-))
(not (Flag630prime-))
(not (Flag629prime-))
(not (Flag628prime-))
(not (Flag627prime-))
(not (Flag626prime-))
(not (Flag87prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag93prime-))
(not (Flag91prime-))
(not (Flag89prime-))
(not (Flag69prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag92prime-))
(not (Flag85prime-))
(not (Flag84prime-))
(not (Flag97prime-))
(not (Flag95prime-))
(not (Flag94prime-))
(not (Flag82prime-))
(not (Flag98prime-))
(not (Flag96prime-))
(not (Flag83prime-))
(not (Flag81prime-))
(not (Flag76prime-))
(not (Flag74prime-))
(not (Flag37prime-))
)
)
(:action STOP-PASS_AN-AF
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AF)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag637prime-))
(not (Flag636prime-))
(not (Flag635prime-))
(not (Flag634prime-))
(not (Flag633prime-))
(not (Flag632prime-))
(not (Flag631prime-))
(not (Flag630prime-))
(not (Flag629prime-))
(not (Flag628prime-))
(not (Flag627prime-))
(not (Flag626prime-))
(not (Flag87prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag93prime-))
(not (Flag91prime-))
(not (Flag89prime-))
(not (Flag69prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag92prime-))
(not (Flag85prime-))
(not (Flag84prime-))
(not (Flag97prime-))
(not (Flag95prime-))
(not (Flag94prime-))
(not (Flag82prime-))
(not (Flag98prime-))
(not (Flag96prime-))
(not (Flag83prime-))
(not (Flag81prime-))
(not (Flag76prime-))
(not (Flag74prime-))
(not (Flag37prime-))
)
)
(:action STOP-PASS_AN-AC
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AC)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag637prime-))
(not (Flag636prime-))
(not (Flag635prime-))
(not (Flag634prime-))
(not (Flag633prime-))
(not (Flag632prime-))
(not (Flag631prime-))
(not (Flag630prime-))
(not (Flag629prime-))
(not (Flag628prime-))
(not (Flag627prime-))
(not (Flag626prime-))
(not (Flag87prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag93prime-))
(not (Flag91prime-))
(not (Flag89prime-))
(not (Flag69prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag92prime-))
(not (Flag85prime-))
(not (Flag84prime-))
(not (Flag97prime-))
(not (Flag95prime-))
(not (Flag94prime-))
(not (Flag82prime-))
(not (Flag98prime-))
(not (Flag96prime-))
(not (Flag83prime-))
(not (Flag81prime-))
(not (Flag76prime-))
(not (Flag74prime-))
(not (Flag37prime-))
)
)
(:action STOP-PASS_AS-BD
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-BD)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag637prime-))
(not (Flag636prime-))
(not (Flag635prime-))
(not (Flag634prime-))
(not (Flag633prime-))
(not (Flag632prime-))
(not (Flag631prime-))
(not (Flag630prime-))
(not (Flag629prime-))
(not (Flag628prime-))
(not (Flag627prime-))
(not (Flag626prime-))
(not (Flag87prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag93prime-))
(not (Flag91prime-))
(not (Flag89prime-))
(not (Flag69prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag92prime-))
(not (Flag85prime-))
(not (Flag84prime-))
(not (Flag97prime-))
(not (Flag95prime-))
(not (Flag94prime-))
(not (Flag82prime-))
(not (Flag98prime-))
(not (Flag96prime-))
(not (Flag83prime-))
(not (Flag81prime-))
(not (Flag76prime-))
(not (Flag74prime-))
(not (Flag37prime-))
)
)
(:action STOP-PASS_AS-AZ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AZ)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag637prime-))
(not (Flag636prime-))
(not (Flag635prime-))
(not (Flag634prime-))
(not (Flag633prime-))
(not (Flag632prime-))
(not (Flag631prime-))
(not (Flag630prime-))
(not (Flag629prime-))
(not (Flag628prime-))
(not (Flag627prime-))
(not (Flag626prime-))
(not (Flag87prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag93prime-))
(not (Flag91prime-))
(not (Flag89prime-))
(not (Flag69prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag92prime-))
(not (Flag85prime-))
(not (Flag84prime-))
(not (Flag97prime-))
(not (Flag95prime-))
(not (Flag94prime-))
(not (Flag82prime-))
(not (Flag98prime-))
(not (Flag96prime-))
(not (Flag83prime-))
(not (Flag81prime-))
(not (Flag76prime-))
(not (Flag74prime-))
(not (Flag37prime-))
)
)
(:action STOP-PASS_AS-AY
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AY)
(Flag626prime-)
)
:effect
(and
(when
(and
(Flag626-)
)
(and
(BOARDED-PASS_AS)
(not (NOT-BOARDED-PASS_AS))
)
)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag637prime-))
(not (Flag636prime-))
(not (Flag635prime-))
(not (Flag634prime-))
(not (Flag633prime-))
(not (Flag632prime-))
(not (Flag631prime-))
(not (Flag630prime-))
(not (Flag629prime-))
(not (Flag628prime-))
(not (Flag627prime-))
(not (Flag626prime-))
(not (Flag87prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag93prime-))
(not (Flag91prime-))
(not (Flag89prime-))
(not (Flag69prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag92prime-))
(not (Flag85prime-))
(not (Flag84prime-))
(not (Flag97prime-))
(not (Flag95prime-))
(not (Flag94prime-))
(not (Flag82prime-))
(not (Flag98prime-))
(not (Flag96prime-))
(not (Flag83prime-))
(not (Flag81prime-))
(not (Flag76prime-))
(not (Flag74prime-))
(not (Flag37prime-))
)
)
(:action STOP-PASS_AS-AW
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AW)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag637prime-))
(not (Flag636prime-))
(not (Flag635prime-))
(not (Flag634prime-))
(not (Flag633prime-))
(not (Flag632prime-))
(not (Flag631prime-))
(not (Flag630prime-))
(not (Flag629prime-))
(not (Flag628prime-))
(not (Flag627prime-))
(not (Flag626prime-))
(not (Flag87prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag93prime-))
(not (Flag91prime-))
(not (Flag89prime-))
(not (Flag69prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag92prime-))
(not (Flag85prime-))
(not (Flag84prime-))
(not (Flag97prime-))
(not (Flag95prime-))
(not (Flag94prime-))
(not (Flag82prime-))
(not (Flag98prime-))
(not (Flag96prime-))
(not (Flag83prime-))
(not (Flag81prime-))
(not (Flag76prime-))
(not (Flag74prime-))
(not (Flag37prime-))
)
)
(:action STOP-PASS_AS-AF
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AF)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag637prime-))
(not (Flag636prime-))
(not (Flag635prime-))
(not (Flag634prime-))
(not (Flag633prime-))
(not (Flag632prime-))
(not (Flag631prime-))
(not (Flag630prime-))
(not (Flag629prime-))
(not (Flag628prime-))
(not (Flag627prime-))
(not (Flag626prime-))
(not (Flag87prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag93prime-))
(not (Flag91prime-))
(not (Flag89prime-))
(not (Flag69prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag92prime-))
(not (Flag85prime-))
(not (Flag84prime-))
(not (Flag97prime-))
(not (Flag95prime-))
(not (Flag94prime-))
(not (Flag82prime-))
(not (Flag98prime-))
(not (Flag96prime-))
(not (Flag83prime-))
(not (Flag81prime-))
(not (Flag76prime-))
(not (Flag74prime-))
(not (Flag37prime-))
)
)
(:action STOP-PASS_AS-AC
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AC)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag637prime-))
(not (Flag636prime-))
(not (Flag635prime-))
(not (Flag634prime-))
(not (Flag633prime-))
(not (Flag632prime-))
(not (Flag631prime-))
(not (Flag630prime-))
(not (Flag629prime-))
(not (Flag628prime-))
(not (Flag627prime-))
(not (Flag626prime-))
(not (Flag87prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag93prime-))
(not (Flag91prime-))
(not (Flag89prime-))
(not (Flag69prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag92prime-))
(not (Flag85prime-))
(not (Flag84prime-))
(not (Flag97prime-))
(not (Flag95prime-))
(not (Flag94prime-))
(not (Flag82prime-))
(not (Flag98prime-))
(not (Flag96prime-))
(not (Flag83prime-))
(not (Flag81prime-))
(not (Flag76prime-))
(not (Flag74prime-))
(not (Flag37prime-))
)
)
(:action Prim37Action-8
:parameters ()
:precondition
(and
(not (SERVED-PASS_AS))
)
:effect
(and
(Flag37prime-)
(not (Flag37-))
)

)
(:action Prim37Action-18
:parameters ()
:precondition
(and
(not (SERVED-PASS_AD))
)
:effect
(and
(Flag37prime-)
(not (Flag37-))
)

)
(:action Prim37Action-28
:parameters ()
:precondition
(and
(not (SERVED-PASS_BA))
)
:effect
(and
(Flag37prime-)
(not (Flag37-))
)

)
(:action MOVEUP-AZ-BD
:parameters ()
:precondition
(and
(LIFT_AT-AZ)
(NOT-ERROR-)
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(NOT-LIFT_AT-AZ)
(LIFT_AT-BD)
(CHECKCONSISTENCY-)
(not (LIFT_AT-AZ))
(not (NOT-LIFT_AT-BD))
(not (NOT-CHECKCONSISTENCY-))
(not (Flag637prime-))
(not (Flag636prime-))
(not (Flag635prime-))
(not (Flag634prime-))
(not (Flag633prime-))
(not (Flag632prime-))
(not (Flag631prime-))
(not (Flag630prime-))
(not (Flag629prime-))
(not (Flag628prime-))
(not (Flag627prime-))
(not (Flag626prime-))
(not (Flag87prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag93prime-))
(not (Flag91prime-))
(not (Flag89prime-))
(not (Flag69prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag92prime-))
(not (Flag85prime-))
(not (Flag84prime-))
(not (Flag97prime-))
(not (Flag95prime-))
(not (Flag94prime-))
(not (Flag82prime-))
(not (Flag98prime-))
(not (Flag96prime-))
(not (Flag83prime-))
(not (Flag81prime-))
(not (Flag76prime-))
(not (Flag74prime-))
(not (Flag37prime-))
)
)
(:action MOVEUP-AC-AF
:parameters ()
:precondition
(and
(LIFT_AT-AC)
(NOT-ERROR-)
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(NOT-LIFT_AT-AC)
(LIFT_AT-AF)
(CHECKCONSISTENCY-)
(not (LIFT_AT-AC))
(not (NOT-LIFT_AT-AF))
(not (NOT-CHECKCONSISTENCY-))
(not (Flag637prime-))
(not (Flag636prime-))
(not (Flag635prime-))
(not (Flag634prime-))
(not (Flag633prime-))
(not (Flag632prime-))
(not (Flag631prime-))
(not (Flag630prime-))
(not (Flag629prime-))
(not (Flag628prime-))
(not (Flag627prime-))
(not (Flag626prime-))
(not (Flag87prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag93prime-))
(not (Flag91prime-))
(not (Flag89prime-))
(not (Flag69prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag92prime-))
(not (Flag85prime-))
(not (Flag84prime-))
(not (Flag97prime-))
(not (Flag95prime-))
(not (Flag94prime-))
(not (Flag82prime-))
(not (Flag98prime-))
(not (Flag96prime-))
(not (Flag83prime-))
(not (Flag81prime-))
(not (Flag76prime-))
(not (Flag74prime-))
(not (Flag37prime-))
)
)
(:action MOVEUP-AF-AG
:parameters ()
:precondition
(and
(LIFT_AT-AF)
(NOT-ERROR-)
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(NOT-LIFT_AT-AF)
(LIFT_AT-AG)
(CHECKCONSISTENCY-)
(not (LIFT_AT-AF))
(not (NOT-LIFT_AT-AG))
(not (NOT-CHECKCONSISTENCY-))
(not (Flag637prime-))
(not (Flag636prime-))
(not (Flag635prime-))
(not (Flag634prime-))
(not (Flag633prime-))
(not (Flag632prime-))
(not (Flag631prime-))
(not (Flag630prime-))
(not (Flag629prime-))
(not (Flag628prime-))
(not (Flag627prime-))
(not (Flag626prime-))
(not (Flag87prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag93prime-))
(not (Flag91prime-))
(not (Flag89prime-))
(not (Flag69prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag92prime-))
(not (Flag85prime-))
(not (Flag84prime-))
(not (Flag97prime-))
(not (Flag95prime-))
(not (Flag94prime-))
(not (Flag82prime-))
(not (Flag98prime-))
(not (Flag96prime-))
(not (Flag83prime-))
(not (Flag81prime-))
(not (Flag76prime-))
(not (Flag74prime-))
(not (Flag37prime-))
)
)
(:action MOVEUP-AY-AZ
:parameters ()
:precondition
(and
(LIFT_AT-AY)
(NOT-ERROR-)
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(NOT-LIFT_AT-AY)
(LIFT_AT-AZ)
(CHECKCONSISTENCY-)
(not (LIFT_AT-AY))
(not (NOT-LIFT_AT-AZ))
(not (NOT-CHECKCONSISTENCY-))
(not (Flag637prime-))
(not (Flag636prime-))
(not (Flag635prime-))
(not (Flag634prime-))
(not (Flag633prime-))
(not (Flag632prime-))
(not (Flag631prime-))
(not (Flag630prime-))
(not (Flag629prime-))
(not (Flag628prime-))
(not (Flag627prime-))
(not (Flag626prime-))
(not (Flag87prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag93prime-))
(not (Flag91prime-))
(not (Flag89prime-))
(not (Flag69prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag92prime-))
(not (Flag85prime-))
(not (Flag84prime-))
(not (Flag97prime-))
(not (Flag95prime-))
(not (Flag94prime-))
(not (Flag82prime-))
(not (Flag98prime-))
(not (Flag96prime-))
(not (Flag83prime-))
(not (Flag81prime-))
(not (Flag76prime-))
(not (Flag74prime-))
(not (Flag37prime-))
)
)
(:action MOVEUP-AW-AY
:parameters ()
:precondition
(and
(LIFT_AT-AW)
(NOT-ERROR-)
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(NOT-LIFT_AT-AW)
(LIFT_AT-AY)
(CHECKCONSISTENCY-)
(not (LIFT_AT-AW))
(not (NOT-LIFT_AT-AY))
(not (NOT-CHECKCONSISTENCY-))
(not (Flag637prime-))
(not (Flag636prime-))
(not (Flag635prime-))
(not (Flag634prime-))
(not (Flag633prime-))
(not (Flag632prime-))
(not (Flag631prime-))
(not (Flag630prime-))
(not (Flag629prime-))
(not (Flag628prime-))
(not (Flag627prime-))
(not (Flag626prime-))
(not (Flag87prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag93prime-))
(not (Flag91prime-))
(not (Flag89prime-))
(not (Flag69prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag92prime-))
(not (Flag85prime-))
(not (Flag84prime-))
(not (Flag97prime-))
(not (Flag95prime-))
(not (Flag94prime-))
(not (Flag82prime-))
(not (Flag98prime-))
(not (Flag96prime-))
(not (Flag83prime-))
(not (Flag81prime-))
(not (Flag76prime-))
(not (Flag74prime-))
(not (Flag37prime-))
)
)
(:action MOVEUP-AV-AW
:parameters ()
:precondition
(and
(LIFT_AT-AV)
(NOT-ERROR-)
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(NOT-LIFT_AT-AV)
(LIFT_AT-AW)
(CHECKCONSISTENCY-)
(not (LIFT_AT-AV))
(not (NOT-LIFT_AT-AW))
(not (NOT-CHECKCONSISTENCY-))
(not (Flag637prime-))
(not (Flag636prime-))
(not (Flag635prime-))
(not (Flag634prime-))
(not (Flag633prime-))
(not (Flag632prime-))
(not (Flag631prime-))
(not (Flag630prime-))
(not (Flag629prime-))
(not (Flag628prime-))
(not (Flag627prime-))
(not (Flag626prime-))
(not (Flag87prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag93prime-))
(not (Flag91prime-))
(not (Flag89prime-))
(not (Flag69prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag92prime-))
(not (Flag85prime-))
(not (Flag84prime-))
(not (Flag97prime-))
(not (Flag95prime-))
(not (Flag94prime-))
(not (Flag82prime-))
(not (Flag98prime-))
(not (Flag96prime-))
(not (Flag83prime-))
(not (Flag81prime-))
(not (Flag76prime-))
(not (Flag74prime-))
(not (Flag37prime-))
)
)
(:action MOVEDOWN-AV-AT
:parameters ()
:precondition
(and
(LIFT_AT-AV)
(NOT-ERROR-)
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(NOT-LIFT_AT-AV)
(LIFT_AT-AT)
(CHECKCONSISTENCY-)
(not (LIFT_AT-AV))
(not (NOT-LIFT_AT-AT))
(not (NOT-CHECKCONSISTENCY-))
(not (Flag637prime-))
(not (Flag636prime-))
(not (Flag635prime-))
(not (Flag634prime-))
(not (Flag633prime-))
(not (Flag632prime-))
(not (Flag631prime-))
(not (Flag630prime-))
(not (Flag629prime-))
(not (Flag628prime-))
(not (Flag627prime-))
(not (Flag626prime-))
(not (Flag87prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag93prime-))
(not (Flag91prime-))
(not (Flag89prime-))
(not (Flag69prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag92prime-))
(not (Flag85prime-))
(not (Flag84prime-))
(not (Flag97prime-))
(not (Flag95prime-))
(not (Flag94prime-))
(not (Flag82prime-))
(not (Flag98prime-))
(not (Flag96prime-))
(not (Flag83prime-))
(not (Flag81prime-))
(not (Flag76prime-))
(not (Flag74prime-))
(not (Flag37prime-))
)
)
(:action MOVEDOWN-AF-AC
:parameters ()
:precondition
(and
(LIFT_AT-AF)
(NOT-ERROR-)
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(NOT-LIFT_AT-AF)
(LIFT_AT-AC)
(CHECKCONSISTENCY-)
(not (LIFT_AT-AF))
(not (NOT-LIFT_AT-AC))
(not (NOT-CHECKCONSISTENCY-))
(not (Flag637prime-))
(not (Flag636prime-))
(not (Flag635prime-))
(not (Flag634prime-))
(not (Flag633prime-))
(not (Flag632prime-))
(not (Flag631prime-))
(not (Flag630prime-))
(not (Flag629prime-))
(not (Flag628prime-))
(not (Flag627prime-))
(not (Flag626prime-))
(not (Flag87prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag93prime-))
(not (Flag91prime-))
(not (Flag89prime-))
(not (Flag69prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag92prime-))
(not (Flag85prime-))
(not (Flag84prime-))
(not (Flag97prime-))
(not (Flag95prime-))
(not (Flag94prime-))
(not (Flag82prime-))
(not (Flag98prime-))
(not (Flag96prime-))
(not (Flag83prime-))
(not (Flag81prime-))
(not (Flag76prime-))
(not (Flag74prime-))
(not (Flag37prime-))
)
)
(:action MOVEDOWN-AG-AF
:parameters ()
:precondition
(and
(LIFT_AT-AG)
(NOT-ERROR-)
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(NOT-LIFT_AT-AG)
(LIFT_AT-AF)
(CHECKCONSISTENCY-)
(not (LIFT_AT-AG))
(not (NOT-LIFT_AT-AF))
(not (NOT-CHECKCONSISTENCY-))
(not (Flag637prime-))
(not (Flag636prime-))
(not (Flag635prime-))
(not (Flag634prime-))
(not (Flag633prime-))
(not (Flag632prime-))
(not (Flag631prime-))
(not (Flag630prime-))
(not (Flag629prime-))
(not (Flag628prime-))
(not (Flag627prime-))
(not (Flag626prime-))
(not (Flag87prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag93prime-))
(not (Flag91prime-))
(not (Flag89prime-))
(not (Flag69prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag92prime-))
(not (Flag85prime-))
(not (Flag84prime-))
(not (Flag97prime-))
(not (Flag95prime-))
(not (Flag94prime-))
(not (Flag82prime-))
(not (Flag98prime-))
(not (Flag96prime-))
(not (Flag83prime-))
(not (Flag81prime-))
(not (Flag76prime-))
(not (Flag74prime-))
(not (Flag37prime-))
)
)
(:action Prim52Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AV))
)
:effect
(and
)

)
(:action Prim64Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AH))
)
:effect
(and
)

)
(:action Prim65Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AI))
)
:effect
(and
)

)
(:action Prim67Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AG))
)
:effect
(and
)

)
(:action Flag82Action
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AV)
)
:effect
(and
(Flag82-)
(Flag82prime-)
)

)
(:action Prim82Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AV))
)
:effect
(and
(Flag82prime-)
(not (Flag82-))
)

)
(:action Flag94Action
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AH)
)
:effect
(and
(Flag94-)
(Flag94prime-)
)

)
(:action Prim94Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AH))
)
:effect
(and
(Flag94prime-)
(not (Flag94-))
)

)
(:action Flag95Action
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AI)
)
:effect
(and
(Flag95-)
(Flag95prime-)
)

)
(:action Prim95Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AI))
)
:effect
(and
(Flag95prime-)
(not (Flag95-))
)

)
(:action Flag97Action
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AG)
)
:effect
(and
(Flag97-)
(Flag97prime-)
)

)
(:action Prim97Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AG))
)
:effect
(and
(Flag97prime-)
(not (Flag97-))
)

)
(:action Prim113Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AV))
)
:effect
(and
)

)
(:action Prim125Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AH))
)
:effect
(and
)

)
(:action Prim126Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AI))
)
:effect
(and
)

)
(:action Prim128Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AG))
)
:effect
(and
)

)
(:action Prim144Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AV))
)
:effect
(and
)

)
(:action Prim156Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AH))
)
:effect
(and
)

)
(:action Prim157Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AI))
)
:effect
(and
)

)
(:action Prim159Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AG))
)
:effect
(and
)

)
(:action Prim175Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AV))
)
:effect
(and
)

)
(:action Prim187Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AH))
)
:effect
(and
)

)
(:action Prim188Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AI))
)
:effect
(and
)

)
(:action Prim190Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AG))
)
:effect
(and
)

)
(:action Prim206Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AV))
)
:effect
(and
)

)
(:action Prim218Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AH))
)
:effect
(and
)

)
(:action Prim219Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AI))
)
:effect
(and
)

)
(:action Prim221Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AG))
)
:effect
(and
)

)
(:action Prim237Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AV))
)
:effect
(and
)

)
(:action Prim249Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AH))
)
:effect
(and
)

)
(:action Prim250Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AI))
)
:effect
(and
)

)
(:action Prim252Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AG))
)
:effect
(and
)

)
(:action Prim268Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AV))
)
:effect
(and
)

)
(:action Prim280Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AH))
)
:effect
(and
)

)
(:action Prim281Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AI))
)
:effect
(and
)

)
(:action Prim283Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AG))
)
:effect
(and
)

)
(:action Prim299Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AV))
)
:effect
(and
)

)
(:action Prim311Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AH))
)
:effect
(and
)

)
(:action Prim312Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AI))
)
:effect
(and
)

)
(:action Prim314Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AG))
)
:effect
(and
)

)
(:action Prim330Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AV))
)
:effect
(and
)

)
(:action Prim342Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AH))
)
:effect
(and
)

)
(:action Prim343Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AI))
)
:effect
(and
)

)
(:action Prim345Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AG))
)
:effect
(and
)

)
(:action Prim361Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AV))
)
:effect
(and
)

)
(:action Prim373Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AH))
)
:effect
(and
)

)
(:action Prim374Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AI))
)
:effect
(and
)

)
(:action Prim376Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AG))
)
:effect
(and
)

)
(:action Prim392Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AV))
)
:effect
(and
)

)
(:action Prim404Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AH))
)
:effect
(and
)

)
(:action Prim405Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AI))
)
:effect
(and
)

)
(:action Prim407Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AG))
)
:effect
(and
)

)
(:action Prim423Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AV))
)
:effect
(and
)

)
(:action Prim435Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AH))
)
:effect
(and
)

)
(:action Prim436Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AI))
)
:effect
(and
)

)
(:action Prim438Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AG))
)
:effect
(and
)

)
(:action Prim454Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AV))
)
:effect
(and
)

)
(:action Prim466Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AH))
)
:effect
(and
)

)
(:action Prim467Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AI))
)
:effect
(and
)

)
(:action Prim469Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AG))
)
:effect
(and
)

)
(:action Prim485Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AV))
)
:effect
(and
)

)
(:action Prim497Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AH))
)
:effect
(and
)

)
(:action Prim498Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AI))
)
:effect
(and
)

)
(:action Prim500Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AG))
)
:effect
(and
)

)
(:action Prim516Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AV))
)
:effect
(and
)

)
(:action Prim528Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AH))
)
:effect
(and
)

)
(:action Prim529Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AI))
)
:effect
(and
)

)
(:action Prim531Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AG))
)
:effect
(and
)

)
(:action Prim547Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AV))
)
:effect
(and
)

)
(:action Prim559Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AH))
)
:effect
(and
)

)
(:action Prim560Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AI))
)
:effect
(and
)

)
(:action Prim562Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AG))
)
:effect
(and
)

)
(:action Prim578Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AV))
)
:effect
(and
)

)
(:action Prim590Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AH))
)
:effect
(and
)

)
(:action Prim591Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AI))
)
:effect
(and
)

)
(:action Prim593Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AG))
)
:effect
(and
)

)
(:action Prim609Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AV))
)
:effect
(and
)

)
(:action Prim621Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AH))
)
:effect
(and
)

)
(:action Prim622Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AI))
)
:effect
(and
)

)
(:action Prim624Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AG))
)
:effect
(and
)

)
(:action STOP-PASS_BA-AT
:parameters ()
:precondition
(and
(Flag84-)
(Flag84prime-)
)
:effect
(and
(when
(and
(BOARDED-PASS_BA)
)
(and
(SERVED-PASS_BA)
(NOT-BOARDED-PASS_BA)
(not (NOT-SERVED-PASS_BA))
(not (BOARDED-PASS_BA))
)
)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag637prime-))
(not (Flag636prime-))
(not (Flag635prime-))
(not (Flag634prime-))
(not (Flag633prime-))
(not (Flag632prime-))
(not (Flag631prime-))
(not (Flag630prime-))
(not (Flag629prime-))
(not (Flag628prime-))
(not (Flag627prime-))
(not (Flag626prime-))
(not (Flag87prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag93prime-))
(not (Flag91prime-))
(not (Flag89prime-))
(not (Flag69prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag92prime-))
(not (Flag85prime-))
(not (Flag84prime-))
(not (Flag97prime-))
(not (Flag95prime-))
(not (Flag94prime-))
(not (Flag82prime-))
(not (Flag98prime-))
(not (Flag96prime-))
(not (Flag83prime-))
(not (Flag81prime-))
(not (Flag76prime-))
(not (Flag74prime-))
(not (Flag37prime-))
)
)
(:action STOP-PASS_BA-AR
:parameters ()
:precondition
(and
(Flag85-)
(Flag85prime-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag637prime-))
(not (Flag636prime-))
(not (Flag635prime-))
(not (Flag634prime-))
(not (Flag633prime-))
(not (Flag632prime-))
(not (Flag631prime-))
(not (Flag630prime-))
(not (Flag629prime-))
(not (Flag628prime-))
(not (Flag627prime-))
(not (Flag626prime-))
(not (Flag87prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag93prime-))
(not (Flag91prime-))
(not (Flag89prime-))
(not (Flag69prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag92prime-))
(not (Flag85prime-))
(not (Flag84prime-))
(not (Flag97prime-))
(not (Flag95prime-))
(not (Flag94prime-))
(not (Flag82prime-))
(not (Flag98prime-))
(not (Flag96prime-))
(not (Flag83prime-))
(not (Flag81prime-))
(not (Flag76prime-))
(not (Flag74prime-))
(not (Flag37prime-))
)
)
(:action STOP-PASS_BA-AJ
:parameters ()
:precondition
(and
(Flag92-)
(Flag92prime-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag637prime-))
(not (Flag636prime-))
(not (Flag635prime-))
(not (Flag634prime-))
(not (Flag633prime-))
(not (Flag632prime-))
(not (Flag631prime-))
(not (Flag630prime-))
(not (Flag629prime-))
(not (Flag628prime-))
(not (Flag627prime-))
(not (Flag626prime-))
(not (Flag87prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag93prime-))
(not (Flag91prime-))
(not (Flag89prime-))
(not (Flag69prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag92prime-))
(not (Flag85prime-))
(not (Flag84prime-))
(not (Flag97prime-))
(not (Flag95prime-))
(not (Flag94prime-))
(not (Flag82prime-))
(not (Flag98prime-))
(not (Flag96prime-))
(not (Flag83prime-))
(not (Flag81prime-))
(not (Flag76prime-))
(not (Flag74prime-))
(not (Flag37prime-))
)
)
(:action STOP-PASS_BB-AV
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AV)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag637prime-))
(not (Flag636prime-))
(not (Flag635prime-))
(not (Flag634prime-))
(not (Flag633prime-))
(not (Flag632prime-))
(not (Flag631prime-))
(not (Flag630prime-))
(not (Flag629prime-))
(not (Flag628prime-))
(not (Flag627prime-))
(not (Flag626prime-))
(not (Flag87prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag93prime-))
(not (Flag91prime-))
(not (Flag89prime-))
(not (Flag69prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag92prime-))
(not (Flag85prime-))
(not (Flag84prime-))
(not (Flag97prime-))
(not (Flag95prime-))
(not (Flag94prime-))
(not (Flag82prime-))
(not (Flag98prime-))
(not (Flag96prime-))
(not (Flag83prime-))
(not (Flag81prime-))
(not (Flag76prime-))
(not (Flag74prime-))
(not (Flag37prime-))
)
)
(:action STOP-PASS_BB-AH
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AH)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag637prime-))
(not (Flag636prime-))
(not (Flag635prime-))
(not (Flag634prime-))
(not (Flag633prime-))
(not (Flag632prime-))
(not (Flag631prime-))
(not (Flag630prime-))
(not (Flag629prime-))
(not (Flag628prime-))
(not (Flag627prime-))
(not (Flag626prime-))
(not (Flag87prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag93prime-))
(not (Flag91prime-))
(not (Flag89prime-))
(not (Flag69prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag92prime-))
(not (Flag85prime-))
(not (Flag84prime-))
(not (Flag97prime-))
(not (Flag95prime-))
(not (Flag94prime-))
(not (Flag82prime-))
(not (Flag98prime-))
(not (Flag96prime-))
(not (Flag83prime-))
(not (Flag81prime-))
(not (Flag76prime-))
(not (Flag74prime-))
(not (Flag37prime-))
)
)
(:action STOP-PASS_BB-AI
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AI)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag637prime-))
(not (Flag636prime-))
(not (Flag635prime-))
(not (Flag634prime-))
(not (Flag633prime-))
(not (Flag632prime-))
(not (Flag631prime-))
(not (Flag630prime-))
(not (Flag629prime-))
(not (Flag628prime-))
(not (Flag627prime-))
(not (Flag626prime-))
(not (Flag87prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag93prime-))
(not (Flag91prime-))
(not (Flag89prime-))
(not (Flag69prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag92prime-))
(not (Flag85prime-))
(not (Flag84prime-))
(not (Flag97prime-))
(not (Flag95prime-))
(not (Flag94prime-))
(not (Flag82prime-))
(not (Flag98prime-))
(not (Flag96prime-))
(not (Flag83prime-))
(not (Flag81prime-))
(not (Flag76prime-))
(not (Flag74prime-))
(not (Flag37prime-))
)
)
(:action STOP-PASS_BB-AG
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AG)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag637prime-))
(not (Flag636prime-))
(not (Flag635prime-))
(not (Flag634prime-))
(not (Flag633prime-))
(not (Flag632prime-))
(not (Flag631prime-))
(not (Flag630prime-))
(not (Flag629prime-))
(not (Flag628prime-))
(not (Flag627prime-))
(not (Flag626prime-))
(not (Flag87prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag93prime-))
(not (Flag91prime-))
(not (Flag89prime-))
(not (Flag69prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag92prime-))
(not (Flag85prime-))
(not (Flag84prime-))
(not (Flag97prime-))
(not (Flag95prime-))
(not (Flag94prime-))
(not (Flag82prime-))
(not (Flag98prime-))
(not (Flag96prime-))
(not (Flag83prime-))
(not (Flag81prime-))
(not (Flag76prime-))
(not (Flag74prime-))
(not (Flag37prime-))
)
)
(:action STOP-PASS_BC-AV
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AV)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag637prime-))
(not (Flag636prime-))
(not (Flag635prime-))
(not (Flag634prime-))
(not (Flag633prime-))
(not (Flag632prime-))
(not (Flag631prime-))
(not (Flag630prime-))
(not (Flag629prime-))
(not (Flag628prime-))
(not (Flag627prime-))
(not (Flag626prime-))
(not (Flag87prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag93prime-))
(not (Flag91prime-))
(not (Flag89prime-))
(not (Flag69prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag92prime-))
(not (Flag85prime-))
(not (Flag84prime-))
(not (Flag97prime-))
(not (Flag95prime-))
(not (Flag94prime-))
(not (Flag82prime-))
(not (Flag98prime-))
(not (Flag96prime-))
(not (Flag83prime-))
(not (Flag81prime-))
(not (Flag76prime-))
(not (Flag74prime-))
(not (Flag37prime-))
)
)
(:action STOP-PASS_BC-AH
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AH)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag637prime-))
(not (Flag636prime-))
(not (Flag635prime-))
(not (Flag634prime-))
(not (Flag633prime-))
(not (Flag632prime-))
(not (Flag631prime-))
(not (Flag630prime-))
(not (Flag629prime-))
(not (Flag628prime-))
(not (Flag627prime-))
(not (Flag626prime-))
(not (Flag87prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag93prime-))
(not (Flag91prime-))
(not (Flag89prime-))
(not (Flag69prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag92prime-))
(not (Flag85prime-))
(not (Flag84prime-))
(not (Flag97prime-))
(not (Flag95prime-))
(not (Flag94prime-))
(not (Flag82prime-))
(not (Flag98prime-))
(not (Flag96prime-))
(not (Flag83prime-))
(not (Flag81prime-))
(not (Flag76prime-))
(not (Flag74prime-))
(not (Flag37prime-))
)
)
(:action STOP-PASS_BC-AI
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AI)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag637prime-))
(not (Flag636prime-))
(not (Flag635prime-))
(not (Flag634prime-))
(not (Flag633prime-))
(not (Flag632prime-))
(not (Flag631prime-))
(not (Flag630prime-))
(not (Flag629prime-))
(not (Flag628prime-))
(not (Flag627prime-))
(not (Flag626prime-))
(not (Flag87prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag93prime-))
(not (Flag91prime-))
(not (Flag89prime-))
(not (Flag69prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag92prime-))
(not (Flag85prime-))
(not (Flag84prime-))
(not (Flag97prime-))
(not (Flag95prime-))
(not (Flag94prime-))
(not (Flag82prime-))
(not (Flag98prime-))
(not (Flag96prime-))
(not (Flag83prime-))
(not (Flag81prime-))
(not (Flag76prime-))
(not (Flag74prime-))
(not (Flag37prime-))
)
)
(:action STOP-PASS_BC-AG
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AG)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag637prime-))
(not (Flag636prime-))
(not (Flag635prime-))
(not (Flag634prime-))
(not (Flag633prime-))
(not (Flag632prime-))
(not (Flag631prime-))
(not (Flag630prime-))
(not (Flag629prime-))
(not (Flag628prime-))
(not (Flag627prime-))
(not (Flag626prime-))
(not (Flag87prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag93prime-))
(not (Flag91prime-))
(not (Flag89prime-))
(not (Flag69prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag92prime-))
(not (Flag85prime-))
(not (Flag84prime-))
(not (Flag97prime-))
(not (Flag95prime-))
(not (Flag94prime-))
(not (Flag82prime-))
(not (Flag98prime-))
(not (Flag96prime-))
(not (Flag83prime-))
(not (Flag81prime-))
(not (Flag76prime-))
(not (Flag74prime-))
(not (Flag37prime-))
)
)
(:action STOP-PASS_AU-AV
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AV)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag637prime-))
(not (Flag636prime-))
(not (Flag635prime-))
(not (Flag634prime-))
(not (Flag633prime-))
(not (Flag632prime-))
(not (Flag631prime-))
(not (Flag630prime-))
(not (Flag629prime-))
(not (Flag628prime-))
(not (Flag627prime-))
(not (Flag626prime-))
(not (Flag87prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag93prime-))
(not (Flag91prime-))
(not (Flag89prime-))
(not (Flag69prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag92prime-))
(not (Flag85prime-))
(not (Flag84prime-))
(not (Flag97prime-))
(not (Flag95prime-))
(not (Flag94prime-))
(not (Flag82prime-))
(not (Flag98prime-))
(not (Flag96prime-))
(not (Flag83prime-))
(not (Flag81prime-))
(not (Flag76prime-))
(not (Flag74prime-))
(not (Flag37prime-))
)
)
(:action STOP-PASS_AU-AH
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AH)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag637prime-))
(not (Flag636prime-))
(not (Flag635prime-))
(not (Flag634prime-))
(not (Flag633prime-))
(not (Flag632prime-))
(not (Flag631prime-))
(not (Flag630prime-))
(not (Flag629prime-))
(not (Flag628prime-))
(not (Flag627prime-))
(not (Flag626prime-))
(not (Flag87prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag93prime-))
(not (Flag91prime-))
(not (Flag89prime-))
(not (Flag69prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag92prime-))
(not (Flag85prime-))
(not (Flag84prime-))
(not (Flag97prime-))
(not (Flag95prime-))
(not (Flag94prime-))
(not (Flag82prime-))
(not (Flag98prime-))
(not (Flag96prime-))
(not (Flag83prime-))
(not (Flag81prime-))
(not (Flag76prime-))
(not (Flag74prime-))
(not (Flag37prime-))
)
)
(:action STOP-PASS_AU-AI
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AI)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag637prime-))
(not (Flag636prime-))
(not (Flag635prime-))
(not (Flag634prime-))
(not (Flag633prime-))
(not (Flag632prime-))
(not (Flag631prime-))
(not (Flag630prime-))
(not (Flag629prime-))
(not (Flag628prime-))
(not (Flag627prime-))
(not (Flag626prime-))
(not (Flag87prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag93prime-))
(not (Flag91prime-))
(not (Flag89prime-))
(not (Flag69prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag92prime-))
(not (Flag85prime-))
(not (Flag84prime-))
(not (Flag97prime-))
(not (Flag95prime-))
(not (Flag94prime-))
(not (Flag82prime-))
(not (Flag98prime-))
(not (Flag96prime-))
(not (Flag83prime-))
(not (Flag81prime-))
(not (Flag76prime-))
(not (Flag74prime-))
(not (Flag37prime-))
)
)
(:action STOP-PASS_AU-AG
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AG)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag637prime-))
(not (Flag636prime-))
(not (Flag635prime-))
(not (Flag634prime-))
(not (Flag633prime-))
(not (Flag632prime-))
(not (Flag631prime-))
(not (Flag630prime-))
(not (Flag629prime-))
(not (Flag628prime-))
(not (Flag627prime-))
(not (Flag626prime-))
(not (Flag87prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag93prime-))
(not (Flag91prime-))
(not (Flag89prime-))
(not (Flag69prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag92prime-))
(not (Flag85prime-))
(not (Flag84prime-))
(not (Flag97prime-))
(not (Flag95prime-))
(not (Flag94prime-))
(not (Flag82prime-))
(not (Flag98prime-))
(not (Flag96prime-))
(not (Flag83prime-))
(not (Flag81prime-))
(not (Flag76prime-))
(not (Flag74prime-))
(not (Flag37prime-))
)
)
(:action STOP-PASS_AX-AV
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AV)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag637prime-))
(not (Flag636prime-))
(not (Flag635prime-))
(not (Flag634prime-))
(not (Flag633prime-))
(not (Flag632prime-))
(not (Flag631prime-))
(not (Flag630prime-))
(not (Flag629prime-))
(not (Flag628prime-))
(not (Flag627prime-))
(not (Flag626prime-))
(not (Flag87prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag93prime-))
(not (Flag91prime-))
(not (Flag89prime-))
(not (Flag69prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag92prime-))
(not (Flag85prime-))
(not (Flag84prime-))
(not (Flag97prime-))
(not (Flag95prime-))
(not (Flag94prime-))
(not (Flag82prime-))
(not (Flag98prime-))
(not (Flag96prime-))
(not (Flag83prime-))
(not (Flag81prime-))
(not (Flag76prime-))
(not (Flag74prime-))
(not (Flag37prime-))
)
)
(:action STOP-PASS_AX-AH
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AH)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag637prime-))
(not (Flag636prime-))
(not (Flag635prime-))
(not (Flag634prime-))
(not (Flag633prime-))
(not (Flag632prime-))
(not (Flag631prime-))
(not (Flag630prime-))
(not (Flag629prime-))
(not (Flag628prime-))
(not (Flag627prime-))
(not (Flag626prime-))
(not (Flag87prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag93prime-))
(not (Flag91prime-))
(not (Flag89prime-))
(not (Flag69prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag92prime-))
(not (Flag85prime-))
(not (Flag84prime-))
(not (Flag97prime-))
(not (Flag95prime-))
(not (Flag94prime-))
(not (Flag82prime-))
(not (Flag98prime-))
(not (Flag96prime-))
(not (Flag83prime-))
(not (Flag81prime-))
(not (Flag76prime-))
(not (Flag74prime-))
(not (Flag37prime-))
)
)
(:action STOP-PASS_AX-AI
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AI)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag637prime-))
(not (Flag636prime-))
(not (Flag635prime-))
(not (Flag634prime-))
(not (Flag633prime-))
(not (Flag632prime-))
(not (Flag631prime-))
(not (Flag630prime-))
(not (Flag629prime-))
(not (Flag628prime-))
(not (Flag627prime-))
(not (Flag626prime-))
(not (Flag87prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag93prime-))
(not (Flag91prime-))
(not (Flag89prime-))
(not (Flag69prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag92prime-))
(not (Flag85prime-))
(not (Flag84prime-))
(not (Flag97prime-))
(not (Flag95prime-))
(not (Flag94prime-))
(not (Flag82prime-))
(not (Flag98prime-))
(not (Flag96prime-))
(not (Flag83prime-))
(not (Flag81prime-))
(not (Flag76prime-))
(not (Flag74prime-))
(not (Flag37prime-))
)
)
(:action STOP-PASS_AX-AG
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AG)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag637prime-))
(not (Flag636prime-))
(not (Flag635prime-))
(not (Flag634prime-))
(not (Flag633prime-))
(not (Flag632prime-))
(not (Flag631prime-))
(not (Flag630prime-))
(not (Flag629prime-))
(not (Flag628prime-))
(not (Flag627prime-))
(not (Flag626prime-))
(not (Flag87prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag93prime-))
(not (Flag91prime-))
(not (Flag89prime-))
(not (Flag69prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag92prime-))
(not (Flag85prime-))
(not (Flag84prime-))
(not (Flag97prime-))
(not (Flag95prime-))
(not (Flag94prime-))
(not (Flag82prime-))
(not (Flag98prime-))
(not (Flag96prime-))
(not (Flag83prime-))
(not (Flag81prime-))
(not (Flag76prime-))
(not (Flag74prime-))
(not (Flag37prime-))
)
)
(:action STOP-PASS_AA-AV
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AV)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag637prime-))
(not (Flag636prime-))
(not (Flag635prime-))
(not (Flag634prime-))
(not (Flag633prime-))
(not (Flag632prime-))
(not (Flag631prime-))
(not (Flag630prime-))
(not (Flag629prime-))
(not (Flag628prime-))
(not (Flag627prime-))
(not (Flag626prime-))
(not (Flag87prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag93prime-))
(not (Flag91prime-))
(not (Flag89prime-))
(not (Flag69prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag92prime-))
(not (Flag85prime-))
(not (Flag84prime-))
(not (Flag97prime-))
(not (Flag95prime-))
(not (Flag94prime-))
(not (Flag82prime-))
(not (Flag98prime-))
(not (Flag96prime-))
(not (Flag83prime-))
(not (Flag81prime-))
(not (Flag76prime-))
(not (Flag74prime-))
(not (Flag37prime-))
)
)
(:action STOP-PASS_AA-AH
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AH)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag637prime-))
(not (Flag636prime-))
(not (Flag635prime-))
(not (Flag634prime-))
(not (Flag633prime-))
(not (Flag632prime-))
(not (Flag631prime-))
(not (Flag630prime-))
(not (Flag629prime-))
(not (Flag628prime-))
(not (Flag627prime-))
(not (Flag626prime-))
(not (Flag87prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag93prime-))
(not (Flag91prime-))
(not (Flag89prime-))
(not (Flag69prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag92prime-))
(not (Flag85prime-))
(not (Flag84prime-))
(not (Flag97prime-))
(not (Flag95prime-))
(not (Flag94prime-))
(not (Flag82prime-))
(not (Flag98prime-))
(not (Flag96prime-))
(not (Flag83prime-))
(not (Flag81prime-))
(not (Flag76prime-))
(not (Flag74prime-))
(not (Flag37prime-))
)
)
(:action STOP-PASS_AA-AI
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AI)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag637prime-))
(not (Flag636prime-))
(not (Flag635prime-))
(not (Flag634prime-))
(not (Flag633prime-))
(not (Flag632prime-))
(not (Flag631prime-))
(not (Flag630prime-))
(not (Flag629prime-))
(not (Flag628prime-))
(not (Flag627prime-))
(not (Flag626prime-))
(not (Flag87prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag93prime-))
(not (Flag91prime-))
(not (Flag89prime-))
(not (Flag69prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag92prime-))
(not (Flag85prime-))
(not (Flag84prime-))
(not (Flag97prime-))
(not (Flag95prime-))
(not (Flag94prime-))
(not (Flag82prime-))
(not (Flag98prime-))
(not (Flag96prime-))
(not (Flag83prime-))
(not (Flag81prime-))
(not (Flag76prime-))
(not (Flag74prime-))
(not (Flag37prime-))
)
)
(:action STOP-PASS_AA-AG
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AG)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag637prime-))
(not (Flag636prime-))
(not (Flag635prime-))
(not (Flag634prime-))
(not (Flag633prime-))
(not (Flag632prime-))
(not (Flag631prime-))
(not (Flag630prime-))
(not (Flag629prime-))
(not (Flag628prime-))
(not (Flag627prime-))
(not (Flag626prime-))
(not (Flag87prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag93prime-))
(not (Flag91prime-))
(not (Flag89prime-))
(not (Flag69prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag92prime-))
(not (Flag85prime-))
(not (Flag84prime-))
(not (Flag97prime-))
(not (Flag95prime-))
(not (Flag94prime-))
(not (Flag82prime-))
(not (Flag98prime-))
(not (Flag96prime-))
(not (Flag83prime-))
(not (Flag81prime-))
(not (Flag76prime-))
(not (Flag74prime-))
(not (Flag37prime-))
)
)
(:action STOP-PASS_AB-AV
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AV)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag637prime-))
(not (Flag636prime-))
(not (Flag635prime-))
(not (Flag634prime-))
(not (Flag633prime-))
(not (Flag632prime-))
(not (Flag631prime-))
(not (Flag630prime-))
(not (Flag629prime-))
(not (Flag628prime-))
(not (Flag627prime-))
(not (Flag626prime-))
(not (Flag87prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag93prime-))
(not (Flag91prime-))
(not (Flag89prime-))
(not (Flag69prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag92prime-))
(not (Flag85prime-))
(not (Flag84prime-))
(not (Flag97prime-))
(not (Flag95prime-))
(not (Flag94prime-))
(not (Flag82prime-))
(not (Flag98prime-))
(not (Flag96prime-))
(not (Flag83prime-))
(not (Flag81prime-))
(not (Flag76prime-))
(not (Flag74prime-))
(not (Flag37prime-))
)
)
(:action STOP-PASS_AB-AH
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AH)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag637prime-))
(not (Flag636prime-))
(not (Flag635prime-))
(not (Flag634prime-))
(not (Flag633prime-))
(not (Flag632prime-))
(not (Flag631prime-))
(not (Flag630prime-))
(not (Flag629prime-))
(not (Flag628prime-))
(not (Flag627prime-))
(not (Flag626prime-))
(not (Flag87prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag93prime-))
(not (Flag91prime-))
(not (Flag89prime-))
(not (Flag69prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag92prime-))
(not (Flag85prime-))
(not (Flag84prime-))
(not (Flag97prime-))
(not (Flag95prime-))
(not (Flag94prime-))
(not (Flag82prime-))
(not (Flag98prime-))
(not (Flag96prime-))
(not (Flag83prime-))
(not (Flag81prime-))
(not (Flag76prime-))
(not (Flag74prime-))
(not (Flag37prime-))
)
)
(:action STOP-PASS_AB-AI
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AI)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag637prime-))
(not (Flag636prime-))
(not (Flag635prime-))
(not (Flag634prime-))
(not (Flag633prime-))
(not (Flag632prime-))
(not (Flag631prime-))
(not (Flag630prime-))
(not (Flag629prime-))
(not (Flag628prime-))
(not (Flag627prime-))
(not (Flag626prime-))
(not (Flag87prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag93prime-))
(not (Flag91prime-))
(not (Flag89prime-))
(not (Flag69prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag92prime-))
(not (Flag85prime-))
(not (Flag84prime-))
(not (Flag97prime-))
(not (Flag95prime-))
(not (Flag94prime-))
(not (Flag82prime-))
(not (Flag98prime-))
(not (Flag96prime-))
(not (Flag83prime-))
(not (Flag81prime-))
(not (Flag76prime-))
(not (Flag74prime-))
(not (Flag37prime-))
)
)
(:action STOP-PASS_AB-AG
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AG)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag637prime-))
(not (Flag636prime-))
(not (Flag635prime-))
(not (Flag634prime-))
(not (Flag633prime-))
(not (Flag632prime-))
(not (Flag631prime-))
(not (Flag630prime-))
(not (Flag629prime-))
(not (Flag628prime-))
(not (Flag627prime-))
(not (Flag626prime-))
(not (Flag87prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag93prime-))
(not (Flag91prime-))
(not (Flag89prime-))
(not (Flag69prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag92prime-))
(not (Flag85prime-))
(not (Flag84prime-))
(not (Flag97prime-))
(not (Flag95prime-))
(not (Flag94prime-))
(not (Flag82prime-))
(not (Flag98prime-))
(not (Flag96prime-))
(not (Flag83prime-))
(not (Flag81prime-))
(not (Flag76prime-))
(not (Flag74prime-))
(not (Flag37prime-))
)
)
(:action STOP-PASS_AE-AV
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AV)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag637prime-))
(not (Flag636prime-))
(not (Flag635prime-))
(not (Flag634prime-))
(not (Flag633prime-))
(not (Flag632prime-))
(not (Flag631prime-))
(not (Flag630prime-))
(not (Flag629prime-))
(not (Flag628prime-))
(not (Flag627prime-))
(not (Flag626prime-))
(not (Flag87prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag93prime-))
(not (Flag91prime-))
(not (Flag89prime-))
(not (Flag69prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag92prime-))
(not (Flag85prime-))
(not (Flag84prime-))
(not (Flag97prime-))
(not (Flag95prime-))
(not (Flag94prime-))
(not (Flag82prime-))
(not (Flag98prime-))
(not (Flag96prime-))
(not (Flag83prime-))
(not (Flag81prime-))
(not (Flag76prime-))
(not (Flag74prime-))
(not (Flag37prime-))
)
)
(:action STOP-PASS_AE-AH
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AH)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag637prime-))
(not (Flag636prime-))
(not (Flag635prime-))
(not (Flag634prime-))
(not (Flag633prime-))
(not (Flag632prime-))
(not (Flag631prime-))
(not (Flag630prime-))
(not (Flag629prime-))
(not (Flag628prime-))
(not (Flag627prime-))
(not (Flag626prime-))
(not (Flag87prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag93prime-))
(not (Flag91prime-))
(not (Flag89prime-))
(not (Flag69prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag92prime-))
(not (Flag85prime-))
(not (Flag84prime-))
(not (Flag97prime-))
(not (Flag95prime-))
(not (Flag94prime-))
(not (Flag82prime-))
(not (Flag98prime-))
(not (Flag96prime-))
(not (Flag83prime-))
(not (Flag81prime-))
(not (Flag76prime-))
(not (Flag74prime-))
(not (Flag37prime-))
)
)
(:action STOP-PASS_AE-AI
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AI)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag637prime-))
(not (Flag636prime-))
(not (Flag635prime-))
(not (Flag634prime-))
(not (Flag633prime-))
(not (Flag632prime-))
(not (Flag631prime-))
(not (Flag630prime-))
(not (Flag629prime-))
(not (Flag628prime-))
(not (Flag627prime-))
(not (Flag626prime-))
(not (Flag87prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag93prime-))
(not (Flag91prime-))
(not (Flag89prime-))
(not (Flag69prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag92prime-))
(not (Flag85prime-))
(not (Flag84prime-))
(not (Flag97prime-))
(not (Flag95prime-))
(not (Flag94prime-))
(not (Flag82prime-))
(not (Flag98prime-))
(not (Flag96prime-))
(not (Flag83prime-))
(not (Flag81prime-))
(not (Flag76prime-))
(not (Flag74prime-))
(not (Flag37prime-))
)
)
(:action STOP-PASS_AE-AG
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AG)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag637prime-))
(not (Flag636prime-))
(not (Flag635prime-))
(not (Flag634prime-))
(not (Flag633prime-))
(not (Flag632prime-))
(not (Flag631prime-))
(not (Flag630prime-))
(not (Flag629prime-))
(not (Flag628prime-))
(not (Flag627prime-))
(not (Flag626prime-))
(not (Flag87prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag93prime-))
(not (Flag91prime-))
(not (Flag89prime-))
(not (Flag69prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag92prime-))
(not (Flag85prime-))
(not (Flag84prime-))
(not (Flag97prime-))
(not (Flag95prime-))
(not (Flag94prime-))
(not (Flag82prime-))
(not (Flag98prime-))
(not (Flag96prime-))
(not (Flag83prime-))
(not (Flag81prime-))
(not (Flag76prime-))
(not (Flag74prime-))
(not (Flag37prime-))
)
)
(:action STOP-PASS_AD-AV
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AV)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag637prime-))
(not (Flag636prime-))
(not (Flag635prime-))
(not (Flag634prime-))
(not (Flag633prime-))
(not (Flag632prime-))
(not (Flag631prime-))
(not (Flag630prime-))
(not (Flag629prime-))
(not (Flag628prime-))
(not (Flag627prime-))
(not (Flag626prime-))
(not (Flag87prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag93prime-))
(not (Flag91prime-))
(not (Flag89prime-))
(not (Flag69prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag92prime-))
(not (Flag85prime-))
(not (Flag84prime-))
(not (Flag97prime-))
(not (Flag95prime-))
(not (Flag94prime-))
(not (Flag82prime-))
(not (Flag98prime-))
(not (Flag96prime-))
(not (Flag83prime-))
(not (Flag81prime-))
(not (Flag76prime-))
(not (Flag74prime-))
(not (Flag37prime-))
)
)
(:action STOP-PASS_AD-AH
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AH)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag637prime-))
(not (Flag636prime-))
(not (Flag635prime-))
(not (Flag634prime-))
(not (Flag633prime-))
(not (Flag632prime-))
(not (Flag631prime-))
(not (Flag630prime-))
(not (Flag629prime-))
(not (Flag628prime-))
(not (Flag627prime-))
(not (Flag626prime-))
(not (Flag87prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag93prime-))
(not (Flag91prime-))
(not (Flag89prime-))
(not (Flag69prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag92prime-))
(not (Flag85prime-))
(not (Flag84prime-))
(not (Flag97prime-))
(not (Flag95prime-))
(not (Flag94prime-))
(not (Flag82prime-))
(not (Flag98prime-))
(not (Flag96prime-))
(not (Flag83prime-))
(not (Flag81prime-))
(not (Flag76prime-))
(not (Flag74prime-))
(not (Flag37prime-))
)
)
(:action STOP-PASS_AD-AI
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AI)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag637prime-))
(not (Flag636prime-))
(not (Flag635prime-))
(not (Flag634prime-))
(not (Flag633prime-))
(not (Flag632prime-))
(not (Flag631prime-))
(not (Flag630prime-))
(not (Flag629prime-))
(not (Flag628prime-))
(not (Flag627prime-))
(not (Flag626prime-))
(not (Flag87prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag93prime-))
(not (Flag91prime-))
(not (Flag89prime-))
(not (Flag69prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag92prime-))
(not (Flag85prime-))
(not (Flag84prime-))
(not (Flag97prime-))
(not (Flag95prime-))
(not (Flag94prime-))
(not (Flag82prime-))
(not (Flag98prime-))
(not (Flag96prime-))
(not (Flag83prime-))
(not (Flag81prime-))
(not (Flag76prime-))
(not (Flag74prime-))
(not (Flag37prime-))
)
)
(:action STOP-PASS_AD-AG
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AG)
)
:effect
(and
(when
(and
(BOARDED-PASS_AD)
)
(and
(SERVED-PASS_AD)
(NOT-BOARDED-PASS_AD)
(not (NOT-SERVED-PASS_AD))
(not (BOARDED-PASS_AD))
)
)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag637prime-))
(not (Flag636prime-))
(not (Flag635prime-))
(not (Flag634prime-))
(not (Flag633prime-))
(not (Flag632prime-))
(not (Flag631prime-))
(not (Flag630prime-))
(not (Flag629prime-))
(not (Flag628prime-))
(not (Flag627prime-))
(not (Flag626prime-))
(not (Flag87prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag93prime-))
(not (Flag91prime-))
(not (Flag89prime-))
(not (Flag69prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag92prime-))
(not (Flag85prime-))
(not (Flag84prime-))
(not (Flag97prime-))
(not (Flag95prime-))
(not (Flag94prime-))
(not (Flag82prime-))
(not (Flag98prime-))
(not (Flag96prime-))
(not (Flag83prime-))
(not (Flag81prime-))
(not (Flag76prime-))
(not (Flag74prime-))
(not (Flag37prime-))
)
)
(:action STOP-PASS_AL-AV
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AV)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag637prime-))
(not (Flag636prime-))
(not (Flag635prime-))
(not (Flag634prime-))
(not (Flag633prime-))
(not (Flag632prime-))
(not (Flag631prime-))
(not (Flag630prime-))
(not (Flag629prime-))
(not (Flag628prime-))
(not (Flag627prime-))
(not (Flag626prime-))
(not (Flag87prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag93prime-))
(not (Flag91prime-))
(not (Flag89prime-))
(not (Flag69prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag92prime-))
(not (Flag85prime-))
(not (Flag84prime-))
(not (Flag97prime-))
(not (Flag95prime-))
(not (Flag94prime-))
(not (Flag82prime-))
(not (Flag98prime-))
(not (Flag96prime-))
(not (Flag83prime-))
(not (Flag81prime-))
(not (Flag76prime-))
(not (Flag74prime-))
(not (Flag37prime-))
)
)
(:action STOP-PASS_AL-AH
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AH)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag637prime-))
(not (Flag636prime-))
(not (Flag635prime-))
(not (Flag634prime-))
(not (Flag633prime-))
(not (Flag632prime-))
(not (Flag631prime-))
(not (Flag630prime-))
(not (Flag629prime-))
(not (Flag628prime-))
(not (Flag627prime-))
(not (Flag626prime-))
(not (Flag87prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag93prime-))
(not (Flag91prime-))
(not (Flag89prime-))
(not (Flag69prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag92prime-))
(not (Flag85prime-))
(not (Flag84prime-))
(not (Flag97prime-))
(not (Flag95prime-))
(not (Flag94prime-))
(not (Flag82prime-))
(not (Flag98prime-))
(not (Flag96prime-))
(not (Flag83prime-))
(not (Flag81prime-))
(not (Flag76prime-))
(not (Flag74prime-))
(not (Flag37prime-))
)
)
(:action STOP-PASS_AL-AI
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AI)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag637prime-))
(not (Flag636prime-))
(not (Flag635prime-))
(not (Flag634prime-))
(not (Flag633prime-))
(not (Flag632prime-))
(not (Flag631prime-))
(not (Flag630prime-))
(not (Flag629prime-))
(not (Flag628prime-))
(not (Flag627prime-))
(not (Flag626prime-))
(not (Flag87prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag93prime-))
(not (Flag91prime-))
(not (Flag89prime-))
(not (Flag69prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag92prime-))
(not (Flag85prime-))
(not (Flag84prime-))
(not (Flag97prime-))
(not (Flag95prime-))
(not (Flag94prime-))
(not (Flag82prime-))
(not (Flag98prime-))
(not (Flag96prime-))
(not (Flag83prime-))
(not (Flag81prime-))
(not (Flag76prime-))
(not (Flag74prime-))
(not (Flag37prime-))
)
)
(:action STOP-PASS_AL-AG
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AG)
(Flag628prime-)
)
:effect
(and
(when
(and
(Flag628-)
)
(and
(BOARDED-PASS_AL)
(not (NOT-BOARDED-PASS_AL))
)
)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag637prime-))
(not (Flag636prime-))
(not (Flag635prime-))
(not (Flag634prime-))
(not (Flag633prime-))
(not (Flag632prime-))
(not (Flag631prime-))
(not (Flag630prime-))
(not (Flag629prime-))
(not (Flag628prime-))
(not (Flag627prime-))
(not (Flag626prime-))
(not (Flag87prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag93prime-))
(not (Flag91prime-))
(not (Flag89prime-))
(not (Flag69prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag92prime-))
(not (Flag85prime-))
(not (Flag84prime-))
(not (Flag97prime-))
(not (Flag95prime-))
(not (Flag94prime-))
(not (Flag82prime-))
(not (Flag98prime-))
(not (Flag96prime-))
(not (Flag83prime-))
(not (Flag81prime-))
(not (Flag76prime-))
(not (Flag74prime-))
(not (Flag37prime-))
)
)
(:action STOP-PASS_AN-AV
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AV)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag637prime-))
(not (Flag636prime-))
(not (Flag635prime-))
(not (Flag634prime-))
(not (Flag633prime-))
(not (Flag632prime-))
(not (Flag631prime-))
(not (Flag630prime-))
(not (Flag629prime-))
(not (Flag628prime-))
(not (Flag627prime-))
(not (Flag626prime-))
(not (Flag87prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag93prime-))
(not (Flag91prime-))
(not (Flag89prime-))
(not (Flag69prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag92prime-))
(not (Flag85prime-))
(not (Flag84prime-))
(not (Flag97prime-))
(not (Flag95prime-))
(not (Flag94prime-))
(not (Flag82prime-))
(not (Flag98prime-))
(not (Flag96prime-))
(not (Flag83prime-))
(not (Flag81prime-))
(not (Flag76prime-))
(not (Flag74prime-))
(not (Flag37prime-))
)
)
(:action STOP-PASS_AN-AH
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AH)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag637prime-))
(not (Flag636prime-))
(not (Flag635prime-))
(not (Flag634prime-))
(not (Flag633prime-))
(not (Flag632prime-))
(not (Flag631prime-))
(not (Flag630prime-))
(not (Flag629prime-))
(not (Flag628prime-))
(not (Flag627prime-))
(not (Flag626prime-))
(not (Flag87prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag93prime-))
(not (Flag91prime-))
(not (Flag89prime-))
(not (Flag69prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag92prime-))
(not (Flag85prime-))
(not (Flag84prime-))
(not (Flag97prime-))
(not (Flag95prime-))
(not (Flag94prime-))
(not (Flag82prime-))
(not (Flag98prime-))
(not (Flag96prime-))
(not (Flag83prime-))
(not (Flag81prime-))
(not (Flag76prime-))
(not (Flag74prime-))
(not (Flag37prime-))
)
)
(:action STOP-PASS_AN-AI
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AI)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag637prime-))
(not (Flag636prime-))
(not (Flag635prime-))
(not (Flag634prime-))
(not (Flag633prime-))
(not (Flag632prime-))
(not (Flag631prime-))
(not (Flag630prime-))
(not (Flag629prime-))
(not (Flag628prime-))
(not (Flag627prime-))
(not (Flag626prime-))
(not (Flag87prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag93prime-))
(not (Flag91prime-))
(not (Flag89prime-))
(not (Flag69prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag92prime-))
(not (Flag85prime-))
(not (Flag84prime-))
(not (Flag97prime-))
(not (Flag95prime-))
(not (Flag94prime-))
(not (Flag82prime-))
(not (Flag98prime-))
(not (Flag96prime-))
(not (Flag83prime-))
(not (Flag81prime-))
(not (Flag76prime-))
(not (Flag74prime-))
(not (Flag37prime-))
)
)
(:action STOP-PASS_AN-AG
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AG)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag637prime-))
(not (Flag636prime-))
(not (Flag635prime-))
(not (Flag634prime-))
(not (Flag633prime-))
(not (Flag632prime-))
(not (Flag631prime-))
(not (Flag630prime-))
(not (Flag629prime-))
(not (Flag628prime-))
(not (Flag627prime-))
(not (Flag626prime-))
(not (Flag87prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag93prime-))
(not (Flag91prime-))
(not (Flag89prime-))
(not (Flag69prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag92prime-))
(not (Flag85prime-))
(not (Flag84prime-))
(not (Flag97prime-))
(not (Flag95prime-))
(not (Flag94prime-))
(not (Flag82prime-))
(not (Flag98prime-))
(not (Flag96prime-))
(not (Flag83prime-))
(not (Flag81prime-))
(not (Flag76prime-))
(not (Flag74prime-))
(not (Flag37prime-))
)
)
(:action STOP-PASS_AS-AV
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AV)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag637prime-))
(not (Flag636prime-))
(not (Flag635prime-))
(not (Flag634prime-))
(not (Flag633prime-))
(not (Flag632prime-))
(not (Flag631prime-))
(not (Flag630prime-))
(not (Flag629prime-))
(not (Flag628prime-))
(not (Flag627prime-))
(not (Flag626prime-))
(not (Flag87prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag93prime-))
(not (Flag91prime-))
(not (Flag89prime-))
(not (Flag69prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag92prime-))
(not (Flag85prime-))
(not (Flag84prime-))
(not (Flag97prime-))
(not (Flag95prime-))
(not (Flag94prime-))
(not (Flag82prime-))
(not (Flag98prime-))
(not (Flag96prime-))
(not (Flag83prime-))
(not (Flag81prime-))
(not (Flag76prime-))
(not (Flag74prime-))
(not (Flag37prime-))
)
)
(:action STOP-PASS_AS-AH
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AH)
)
:effect
(and
(when
(and
(BOARDED-PASS_AS)
)
(and
(SERVED-PASS_AS)
(NOT-BOARDED-PASS_AS)
(not (NOT-SERVED-PASS_AS))
(not (BOARDED-PASS_AS))
)
)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag637prime-))
(not (Flag636prime-))
(not (Flag635prime-))
(not (Flag634prime-))
(not (Flag633prime-))
(not (Flag632prime-))
(not (Flag631prime-))
(not (Flag630prime-))
(not (Flag629prime-))
(not (Flag628prime-))
(not (Flag627prime-))
(not (Flag626prime-))
(not (Flag87prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag93prime-))
(not (Flag91prime-))
(not (Flag89prime-))
(not (Flag69prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag92prime-))
(not (Flag85prime-))
(not (Flag84prime-))
(not (Flag97prime-))
(not (Flag95prime-))
(not (Flag94prime-))
(not (Flag82prime-))
(not (Flag98prime-))
(not (Flag96prime-))
(not (Flag83prime-))
(not (Flag81prime-))
(not (Flag76prime-))
(not (Flag74prime-))
(not (Flag37prime-))
)
)
(:action STOP-PASS_AS-AI
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AI)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag637prime-))
(not (Flag636prime-))
(not (Flag635prime-))
(not (Flag634prime-))
(not (Flag633prime-))
(not (Flag632prime-))
(not (Flag631prime-))
(not (Flag630prime-))
(not (Flag629prime-))
(not (Flag628prime-))
(not (Flag627prime-))
(not (Flag626prime-))
(not (Flag87prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag93prime-))
(not (Flag91prime-))
(not (Flag89prime-))
(not (Flag69prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag92prime-))
(not (Flag85prime-))
(not (Flag84prime-))
(not (Flag97prime-))
(not (Flag95prime-))
(not (Flag94prime-))
(not (Flag82prime-))
(not (Flag98prime-))
(not (Flag96prime-))
(not (Flag83prime-))
(not (Flag81prime-))
(not (Flag76prime-))
(not (Flag74prime-))
(not (Flag37prime-))
)
)
(:action STOP-PASS_AS-AG
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AG)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag637prime-))
(not (Flag636prime-))
(not (Flag635prime-))
(not (Flag634prime-))
(not (Flag633prime-))
(not (Flag632prime-))
(not (Flag631prime-))
(not (Flag630prime-))
(not (Flag629prime-))
(not (Flag628prime-))
(not (Flag627prime-))
(not (Flag626prime-))
(not (Flag87prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag93prime-))
(not (Flag91prime-))
(not (Flag89prime-))
(not (Flag69prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag92prime-))
(not (Flag85prime-))
(not (Flag84prime-))
(not (Flag97prime-))
(not (Flag95prime-))
(not (Flag94prime-))
(not (Flag82prime-))
(not (Flag98prime-))
(not (Flag96prime-))
(not (Flag83prime-))
(not (Flag81prime-))
(not (Flag76prime-))
(not (Flag74prime-))
(not (Flag37prime-))
)
)
(:action Prim37Action-0
:parameters ()
:precondition
(and
(not (Flag2-))
(Flag2prime-)
)
:effect
(and
(Flag37prime-)
(not (Flag37-))
)

)
(:action Prim37Action-1
:parameters ()
:precondition
(and
(not (Flag4-))
(Flag4prime-)
)
:effect
(and
(Flag37prime-)
(not (Flag37-))
)

)
(:action Prim37Action-2
:parameters ()
:precondition
(and
(not (Flag6-))
(Flag6prime-)
)
:effect
(and
(Flag37prime-)
(not (Flag37-))
)

)
(:action Prim37Action-3
:parameters ()
:precondition
(and
(not (Flag8-))
(Flag8prime-)
)
:effect
(and
(Flag37prime-)
(not (Flag37-))
)

)
(:action Prim37Action-4
:parameters ()
:precondition
(and
(not (Flag10-))
(Flag10prime-)
)
:effect
(and
(Flag37prime-)
(not (Flag37-))
)

)
(:action Prim37Action-5
:parameters ()
:precondition
(and
(not (Flag12-))
(Flag12prime-)
)
:effect
(and
(Flag37prime-)
(not (Flag37-))
)

)
(:action Prim37Action-6
:parameters ()
:precondition
(and
(not (Flag14-))
(Flag14prime-)
)
:effect
(and
(Flag37prime-)
(not (Flag37-))
)

)
(:action Prim37Action-7
:parameters ()
:precondition
(and
(not (Flag16-))
(Flag16prime-)
)
:effect
(and
(Flag37prime-)
(not (Flag37-))
)

)
(:action Prim37Action-9
:parameters ()
:precondition
(and
(not (Flag18-))
(Flag18prime-)
)
:effect
(and
(Flag37prime-)
(not (Flag37-))
)

)
(:action Prim37Action-11
:parameters ()
:precondition
(and
(not (Flag20-))
(Flag20prime-)
)
:effect
(and
(Flag37prime-)
(not (Flag37-))
)

)
(:action Prim37Action-13
:parameters ()
:precondition
(and
(not (Flag22-))
(Flag22prime-)
)
:effect
(and
(Flag37prime-)
(not (Flag37-))
)

)
(:action Prim37Action-14
:parameters ()
:precondition
(and
(not (Flag24-))
(Flag24prime-)
)
:effect
(and
(Flag37prime-)
(not (Flag37-))
)

)
(:action Prim37Action-15
:parameters ()
:precondition
(and
(not (Flag26-))
(Flag26prime-)
)
:effect
(and
(Flag37prime-)
(not (Flag37-))
)

)
(:action Prim37Action-16
:parameters ()
:precondition
(and
(not (Flag28-))
(Flag28prime-)
)
:effect
(and
(Flag37prime-)
(not (Flag37-))
)

)
(:action Prim37Action-17
:parameters ()
:precondition
(and
(not (Flag30-))
(Flag30prime-)
)
:effect
(and
(Flag37prime-)
(not (Flag37-))
)

)
(:action Prim37Action-22
:parameters ()
:precondition
(and
(not (Flag32-))
(Flag32prime-)
)
:effect
(and
(Flag37prime-)
(not (Flag37-))
)

)
(:action Prim37Action-24
:parameters ()
:precondition
(and
(not (Flag34-))
(Flag34prime-)
)
:effect
(and
(Flag37prime-)
(not (Flag37-))
)

)
(:action Prim37Action-26
:parameters ()
:precondition
(and
(not (SERVED-PASS_BC))
)
:effect
(and
(Flag37prime-)
(not (Flag37-))
)

)
(:action Prim37Action-29
:parameters ()
:precondition
(and
(not (Flag36-))
(Flag36prime-)
)
:effect
(and
(Flag37prime-)
(not (Flag37-))
)

)
(:action MOVEUP-AT-AV
:parameters ()
:precondition
(and
(LIFT_AT-AT)
(NOT-ERROR-)
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(NOT-LIFT_AT-AT)
(LIFT_AT-AV)
(CHECKCONSISTENCY-)
(not (LIFT_AT-AT))
(not (NOT-LIFT_AT-AV))
(not (NOT-CHECKCONSISTENCY-))
(not (Flag637prime-))
(not (Flag636prime-))
(not (Flag635prime-))
(not (Flag634prime-))
(not (Flag633prime-))
(not (Flag632prime-))
(not (Flag631prime-))
(not (Flag630prime-))
(not (Flag629prime-))
(not (Flag628prime-))
(not (Flag627prime-))
(not (Flag626prime-))
(not (Flag87prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag93prime-))
(not (Flag91prime-))
(not (Flag89prime-))
(not (Flag69prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag92prime-))
(not (Flag85prime-))
(not (Flag84prime-))
(not (Flag97prime-))
(not (Flag95prime-))
(not (Flag94prime-))
(not (Flag82prime-))
(not (Flag98prime-))
(not (Flag96prime-))
(not (Flag83prime-))
(not (Flag81prime-))
(not (Flag76prime-))
(not (Flag74prime-))
(not (Flag37prime-))
)
)
(:action MOVEUP-AG-AH
:parameters ()
:precondition
(and
(LIFT_AT-AG)
(NOT-ERROR-)
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(NOT-LIFT_AT-AG)
(LIFT_AT-AH)
(CHECKCONSISTENCY-)
(not (LIFT_AT-AG))
(not (NOT-LIFT_AT-AH))
(not (NOT-CHECKCONSISTENCY-))
(not (Flag637prime-))
(not (Flag636prime-))
(not (Flag635prime-))
(not (Flag634prime-))
(not (Flag633prime-))
(not (Flag632prime-))
(not (Flag631prime-))
(not (Flag630prime-))
(not (Flag629prime-))
(not (Flag628prime-))
(not (Flag627prime-))
(not (Flag626prime-))
(not (Flag87prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag93prime-))
(not (Flag91prime-))
(not (Flag89prime-))
(not (Flag69prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag92prime-))
(not (Flag85prime-))
(not (Flag84prime-))
(not (Flag97prime-))
(not (Flag95prime-))
(not (Flag94prime-))
(not (Flag82prime-))
(not (Flag98prime-))
(not (Flag96prime-))
(not (Flag83prime-))
(not (Flag81prime-))
(not (Flag76prime-))
(not (Flag74prime-))
(not (Flag37prime-))
)
)
(:action MOVEUP-AH-AI
:parameters ()
:precondition
(and
(LIFT_AT-AH)
(NOT-ERROR-)
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(NOT-LIFT_AT-AH)
(LIFT_AT-AI)
(CHECKCONSISTENCY-)
(not (LIFT_AT-AH))
(not (NOT-LIFT_AT-AI))
(not (NOT-CHECKCONSISTENCY-))
(not (Flag637prime-))
(not (Flag636prime-))
(not (Flag635prime-))
(not (Flag634prime-))
(not (Flag633prime-))
(not (Flag632prime-))
(not (Flag631prime-))
(not (Flag630prime-))
(not (Flag629prime-))
(not (Flag628prime-))
(not (Flag627prime-))
(not (Flag626prime-))
(not (Flag87prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag93prime-))
(not (Flag91prime-))
(not (Flag89prime-))
(not (Flag69prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag92prime-))
(not (Flag85prime-))
(not (Flag84prime-))
(not (Flag97prime-))
(not (Flag95prime-))
(not (Flag94prime-))
(not (Flag82prime-))
(not (Flag98prime-))
(not (Flag96prime-))
(not (Flag83prime-))
(not (Flag81prime-))
(not (Flag76prime-))
(not (Flag74prime-))
(not (Flag37prime-))
)
)
(:action MOVEUP-AI-AJ
:parameters ()
:precondition
(and
(LIFT_AT-AI)
(NOT-ERROR-)
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(NOT-LIFT_AT-AI)
(LIFT_AT-AJ)
(CHECKCONSISTENCY-)
(not (LIFT_AT-AI))
(not (NOT-LIFT_AT-AJ))
(not (NOT-CHECKCONSISTENCY-))
(not (Flag637prime-))
(not (Flag636prime-))
(not (Flag635prime-))
(not (Flag634prime-))
(not (Flag633prime-))
(not (Flag632prime-))
(not (Flag631prime-))
(not (Flag630prime-))
(not (Flag629prime-))
(not (Flag628prime-))
(not (Flag627prime-))
(not (Flag626prime-))
(not (Flag87prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag93prime-))
(not (Flag91prime-))
(not (Flag89prime-))
(not (Flag69prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag92prime-))
(not (Flag85prime-))
(not (Flag84prime-))
(not (Flag97prime-))
(not (Flag95prime-))
(not (Flag94prime-))
(not (Flag82prime-))
(not (Flag98prime-))
(not (Flag96prime-))
(not (Flag83prime-))
(not (Flag81prime-))
(not (Flag76prime-))
(not (Flag74prime-))
(not (Flag37prime-))
)
)
(:action MOVEDOWN-AT-AR
:parameters ()
:precondition
(and
(LIFT_AT-AT)
(NOT-ERROR-)
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(NOT-LIFT_AT-AT)
(LIFT_AT-AR)
(CHECKCONSISTENCY-)
(not (LIFT_AT-AT))
(not (NOT-LIFT_AT-AR))
(not (NOT-CHECKCONSISTENCY-))
(not (Flag637prime-))
(not (Flag636prime-))
(not (Flag635prime-))
(not (Flag634prime-))
(not (Flag633prime-))
(not (Flag632prime-))
(not (Flag631prime-))
(not (Flag630prime-))
(not (Flag629prime-))
(not (Flag628prime-))
(not (Flag627prime-))
(not (Flag626prime-))
(not (Flag87prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag93prime-))
(not (Flag91prime-))
(not (Flag89prime-))
(not (Flag69prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag92prime-))
(not (Flag85prime-))
(not (Flag84prime-))
(not (Flag97prime-))
(not (Flag95prime-))
(not (Flag94prime-))
(not (Flag82prime-))
(not (Flag98prime-))
(not (Flag96prime-))
(not (Flag83prime-))
(not (Flag81prime-))
(not (Flag76prime-))
(not (Flag74prime-))
(not (Flag37prime-))
)
)
(:action MOVEDOWN-AH-AG
:parameters ()
:precondition
(and
(LIFT_AT-AH)
(NOT-ERROR-)
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(NOT-LIFT_AT-AH)
(LIFT_AT-AG)
(CHECKCONSISTENCY-)
(not (LIFT_AT-AH))
(not (NOT-LIFT_AT-AG))
(not (NOT-CHECKCONSISTENCY-))
(not (Flag637prime-))
(not (Flag636prime-))
(not (Flag635prime-))
(not (Flag634prime-))
(not (Flag633prime-))
(not (Flag632prime-))
(not (Flag631prime-))
(not (Flag630prime-))
(not (Flag629prime-))
(not (Flag628prime-))
(not (Flag627prime-))
(not (Flag626prime-))
(not (Flag87prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag93prime-))
(not (Flag91prime-))
(not (Flag89prime-))
(not (Flag69prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag92prime-))
(not (Flag85prime-))
(not (Flag84prime-))
(not (Flag97prime-))
(not (Flag95prime-))
(not (Flag94prime-))
(not (Flag82prime-))
(not (Flag98prime-))
(not (Flag96prime-))
(not (Flag83prime-))
(not (Flag81prime-))
(not (Flag76prime-))
(not (Flag74prime-))
(not (Flag37prime-))
)
)
(:action MOVEDOWN-AI-AH
:parameters ()
:precondition
(and
(LIFT_AT-AI)
(NOT-ERROR-)
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(NOT-LIFT_AT-AI)
(LIFT_AT-AH)
(CHECKCONSISTENCY-)
(not (LIFT_AT-AI))
(not (NOT-LIFT_AT-AH))
(not (NOT-CHECKCONSISTENCY-))
(not (Flag637prime-))
(not (Flag636prime-))
(not (Flag635prime-))
(not (Flag634prime-))
(not (Flag633prime-))
(not (Flag632prime-))
(not (Flag631prime-))
(not (Flag630prime-))
(not (Flag629prime-))
(not (Flag628prime-))
(not (Flag627prime-))
(not (Flag626prime-))
(not (Flag87prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag93prime-))
(not (Flag91prime-))
(not (Flag89prime-))
(not (Flag69prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag92prime-))
(not (Flag85prime-))
(not (Flag84prime-))
(not (Flag97prime-))
(not (Flag95prime-))
(not (Flag94prime-))
(not (Flag82prime-))
(not (Flag98prime-))
(not (Flag96prime-))
(not (Flag83prime-))
(not (Flag81prime-))
(not (Flag76prime-))
(not (Flag74prime-))
(not (Flag37prime-))
)
)
(:action MOVEDOWN-AJ-AI
:parameters ()
:precondition
(and
(LIFT_AT-AJ)
(NOT-ERROR-)
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(NOT-LIFT_AT-AJ)
(LIFT_AT-AI)
(CHECKCONSISTENCY-)
(not (LIFT_AT-AJ))
(not (NOT-LIFT_AT-AI))
(not (NOT-CHECKCONSISTENCY-))
(not (Flag637prime-))
(not (Flag636prime-))
(not (Flag635prime-))
(not (Flag634prime-))
(not (Flag633prime-))
(not (Flag632prime-))
(not (Flag631prime-))
(not (Flag630prime-))
(not (Flag629prime-))
(not (Flag628prime-))
(not (Flag627prime-))
(not (Flag626prime-))
(not (Flag87prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag93prime-))
(not (Flag91prime-))
(not (Flag89prime-))
(not (Flag69prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag92prime-))
(not (Flag85prime-))
(not (Flag84prime-))
(not (Flag97prime-))
(not (Flag95prime-))
(not (Flag94prime-))
(not (Flag82prime-))
(not (Flag98prime-))
(not (Flag96prime-))
(not (Flag83prime-))
(not (Flag81prime-))
(not (Flag76prime-))
(not (Flag74prime-))
(not (Flag37prime-))
)
)
(:action MOVEDOWN-AR-AQ
:parameters ()
:precondition
(and
(LIFT_AT-AR)
(NOT-ERROR-)
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(NOT-LIFT_AT-AR)
(LIFT_AT-AQ)
(CHECKCONSISTENCY-)
(not (LIFT_AT-AR))
(not (NOT-LIFT_AT-AQ))
(not (NOT-CHECKCONSISTENCY-))
(not (Flag637prime-))
(not (Flag636prime-))
(not (Flag635prime-))
(not (Flag634prime-))
(not (Flag633prime-))
(not (Flag632prime-))
(not (Flag631prime-))
(not (Flag630prime-))
(not (Flag629prime-))
(not (Flag628prime-))
(not (Flag627prime-))
(not (Flag626prime-))
(not (Flag87prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag93prime-))
(not (Flag91prime-))
(not (Flag89prime-))
(not (Flag69prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag92prime-))
(not (Flag85prime-))
(not (Flag84prime-))
(not (Flag97prime-))
(not (Flag95prime-))
(not (Flag94prime-))
(not (Flag82prime-))
(not (Flag98prime-))
(not (Flag96prime-))
(not (Flag83prime-))
(not (Flag81prime-))
(not (Flag76prime-))
(not (Flag74prime-))
(not (Flag37prime-))
)
)
(:action Prim54Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AT))
)
:effect
(and
)

)
(:action Prim55Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AR))
)
:effect
(and
)

)
(:action Prim62Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AJ))
)
:effect
(and
)

)
(:action Flag84Action
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AT)
)
:effect
(and
(Flag84-)
(Flag84prime-)
)

)
(:action Prim84Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AT))
)
:effect
(and
(Flag84prime-)
(not (Flag84-))
)

)
(:action Flag85Action
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AR)
)
:effect
(and
(Flag85-)
(Flag85prime-)
)

)
(:action Prim85Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AR))
)
:effect
(and
(Flag85prime-)
(not (Flag85-))
)

)
(:action Flag92Action
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AJ)
)
:effect
(and
(Flag92-)
(Flag92prime-)
)

)
(:action Prim92Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AJ))
)
:effect
(and
(Flag92prime-)
(not (Flag92-))
)

)
(:action Prim115Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AT))
)
:effect
(and
)

)
(:action Prim116Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AR))
)
:effect
(and
)

)
(:action Prim123Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AJ))
)
:effect
(and
)

)
(:action Prim146Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AT))
)
:effect
(and
)

)
(:action Prim147Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AR))
)
:effect
(and
)

)
(:action Prim154Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AJ))
)
:effect
(and
)

)
(:action Prim177Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AT))
)
:effect
(and
)

)
(:action Prim178Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AR))
)
:effect
(and
)

)
(:action Prim185Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AJ))
)
:effect
(and
)

)
(:action Prim208Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AT))
)
:effect
(and
)

)
(:action Prim209Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AR))
)
:effect
(and
)

)
(:action Prim216Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AJ))
)
:effect
(and
)

)
(:action Prim239Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AT))
)
:effect
(and
)

)
(:action Prim240Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AR))
)
:effect
(and
)

)
(:action Prim247Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AJ))
)
:effect
(and
)

)
(:action Prim270Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AT))
)
:effect
(and
)

)
(:action Prim271Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AR))
)
:effect
(and
)

)
(:action Prim278Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AJ))
)
:effect
(and
)

)
(:action Prim301Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AT))
)
:effect
(and
)

)
(:action Prim302Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AR))
)
:effect
(and
)

)
(:action Prim309Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AJ))
)
:effect
(and
)

)
(:action Prim332Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AT))
)
:effect
(and
)

)
(:action Prim333Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AR))
)
:effect
(and
)

)
(:action Prim340Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AJ))
)
:effect
(and
)

)
(:action Prim363Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AT))
)
:effect
(and
)

)
(:action Prim364Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AR))
)
:effect
(and
)

)
(:action Prim371Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AJ))
)
:effect
(and
)

)
(:action Prim394Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AT))
)
:effect
(and
)

)
(:action Prim395Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AR))
)
:effect
(and
)

)
(:action Prim402Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AJ))
)
:effect
(and
)

)
(:action Prim425Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AT))
)
:effect
(and
)

)
(:action Prim426Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AR))
)
:effect
(and
)

)
(:action Prim433Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AJ))
)
:effect
(and
)

)
(:action Prim456Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AT))
)
:effect
(and
)

)
(:action Prim457Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AR))
)
:effect
(and
)

)
(:action Prim464Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AJ))
)
:effect
(and
)

)
(:action Prim487Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AT))
)
:effect
(and
)

)
(:action Prim488Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AR))
)
:effect
(and
)

)
(:action Prim495Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AJ))
)
:effect
(and
)

)
(:action Prim518Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AT))
)
:effect
(and
)

)
(:action Prim519Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AR))
)
:effect
(and
)

)
(:action Prim526Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AJ))
)
:effect
(and
)

)
(:action Prim549Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AT))
)
:effect
(and
)

)
(:action Prim550Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AR))
)
:effect
(and
)

)
(:action Prim557Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AJ))
)
:effect
(and
)

)
(:action Prim580Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AT))
)
:effect
(and
)

)
(:action Prim581Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AR))
)
:effect
(and
)

)
(:action Prim588Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AJ))
)
:effect
(and
)

)
(:action Prim611Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AT))
)
:effect
(and
)

)
(:action Prim612Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AR))
)
:effect
(and
)

)
(:action Prim619Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AJ))
)
:effect
(and
)

)
(:action STOP-PASS_BA-AQ
:parameters ()
:precondition
(and
(Flag69-)
(Flag69prime-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag637prime-))
(not (Flag636prime-))
(not (Flag635prime-))
(not (Flag634prime-))
(not (Flag633prime-))
(not (Flag632prime-))
(not (Flag631prime-))
(not (Flag630prime-))
(not (Flag629prime-))
(not (Flag628prime-))
(not (Flag627prime-))
(not (Flag626prime-))
(not (Flag87prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag93prime-))
(not (Flag91prime-))
(not (Flag89prime-))
(not (Flag69prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag92prime-))
(not (Flag85prime-))
(not (Flag84prime-))
(not (Flag97prime-))
(not (Flag95prime-))
(not (Flag94prime-))
(not (Flag82prime-))
(not (Flag98prime-))
(not (Flag96prime-))
(not (Flag83prime-))
(not (Flag81prime-))
(not (Flag76prime-))
(not (Flag74prime-))
(not (Flag37prime-))
)
)
(:action STOP-PASS_BA-AO
:parameters ()
:precondition
(and
(Flag89-)
(Flag89prime-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag637prime-))
(not (Flag636prime-))
(not (Flag635prime-))
(not (Flag634prime-))
(not (Flag633prime-))
(not (Flag632prime-))
(not (Flag631prime-))
(not (Flag630prime-))
(not (Flag629prime-))
(not (Flag628prime-))
(not (Flag627prime-))
(not (Flag626prime-))
(not (Flag87prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag93prime-))
(not (Flag91prime-))
(not (Flag89prime-))
(not (Flag69prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag92prime-))
(not (Flag85prime-))
(not (Flag84prime-))
(not (Flag97prime-))
(not (Flag95prime-))
(not (Flag94prime-))
(not (Flag82prime-))
(not (Flag98prime-))
(not (Flag96prime-))
(not (Flag83prime-))
(not (Flag81prime-))
(not (Flag76prime-))
(not (Flag74prime-))
(not (Flag37prime-))
)
)
(:action STOP-PASS_BA-AM
:parameters ()
:precondition
(and
(Flag91-)
(Flag91prime-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag637prime-))
(not (Flag636prime-))
(not (Flag635prime-))
(not (Flag634prime-))
(not (Flag633prime-))
(not (Flag632prime-))
(not (Flag631prime-))
(not (Flag630prime-))
(not (Flag629prime-))
(not (Flag628prime-))
(not (Flag627prime-))
(not (Flag626prime-))
(not (Flag87prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag93prime-))
(not (Flag91prime-))
(not (Flag89prime-))
(not (Flag69prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag92prime-))
(not (Flag85prime-))
(not (Flag84prime-))
(not (Flag97prime-))
(not (Flag95prime-))
(not (Flag94prime-))
(not (Flag82prime-))
(not (Flag98prime-))
(not (Flag96prime-))
(not (Flag83prime-))
(not (Flag81prime-))
(not (Flag76prime-))
(not (Flag74prime-))
(not (Flag37prime-))
)
)
(:action STOP-PASS_BA-AK
:parameters ()
:precondition
(and
(Flag93-)
(Flag93prime-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag637prime-))
(not (Flag636prime-))
(not (Flag635prime-))
(not (Flag634prime-))
(not (Flag633prime-))
(not (Flag632prime-))
(not (Flag631prime-))
(not (Flag630prime-))
(not (Flag629prime-))
(not (Flag628prime-))
(not (Flag627prime-))
(not (Flag626prime-))
(not (Flag87prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag93prime-))
(not (Flag91prime-))
(not (Flag89prime-))
(not (Flag69prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag92prime-))
(not (Flag85prime-))
(not (Flag84prime-))
(not (Flag97prime-))
(not (Flag95prime-))
(not (Flag94prime-))
(not (Flag82prime-))
(not (Flag98prime-))
(not (Flag96prime-))
(not (Flag83prime-))
(not (Flag81prime-))
(not (Flag76prime-))
(not (Flag74prime-))
(not (Flag37prime-))
)
)
(:action STOP-PASS_BB-AT
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AT)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag637prime-))
(not (Flag636prime-))
(not (Flag635prime-))
(not (Flag634prime-))
(not (Flag633prime-))
(not (Flag632prime-))
(not (Flag631prime-))
(not (Flag630prime-))
(not (Flag629prime-))
(not (Flag628prime-))
(not (Flag627prime-))
(not (Flag626prime-))
(not (Flag87prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag93prime-))
(not (Flag91prime-))
(not (Flag89prime-))
(not (Flag69prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag92prime-))
(not (Flag85prime-))
(not (Flag84prime-))
(not (Flag97prime-))
(not (Flag95prime-))
(not (Flag94prime-))
(not (Flag82prime-))
(not (Flag98prime-))
(not (Flag96prime-))
(not (Flag83prime-))
(not (Flag81prime-))
(not (Flag76prime-))
(not (Flag74prime-))
(not (Flag37prime-))
)
)
(:action STOP-PASS_BB-AR
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AR)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag637prime-))
(not (Flag636prime-))
(not (Flag635prime-))
(not (Flag634prime-))
(not (Flag633prime-))
(not (Flag632prime-))
(not (Flag631prime-))
(not (Flag630prime-))
(not (Flag629prime-))
(not (Flag628prime-))
(not (Flag627prime-))
(not (Flag626prime-))
(not (Flag87prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag93prime-))
(not (Flag91prime-))
(not (Flag89prime-))
(not (Flag69prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag92prime-))
(not (Flag85prime-))
(not (Flag84prime-))
(not (Flag97prime-))
(not (Flag95prime-))
(not (Flag94prime-))
(not (Flag82prime-))
(not (Flag98prime-))
(not (Flag96prime-))
(not (Flag83prime-))
(not (Flag81prime-))
(not (Flag76prime-))
(not (Flag74prime-))
(not (Flag37prime-))
)
)
(:action STOP-PASS_BB-AJ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AJ)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag637prime-))
(not (Flag636prime-))
(not (Flag635prime-))
(not (Flag634prime-))
(not (Flag633prime-))
(not (Flag632prime-))
(not (Flag631prime-))
(not (Flag630prime-))
(not (Flag629prime-))
(not (Flag628prime-))
(not (Flag627prime-))
(not (Flag626prime-))
(not (Flag87prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag93prime-))
(not (Flag91prime-))
(not (Flag89prime-))
(not (Flag69prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag92prime-))
(not (Flag85prime-))
(not (Flag84prime-))
(not (Flag97prime-))
(not (Flag95prime-))
(not (Flag94prime-))
(not (Flag82prime-))
(not (Flag98prime-))
(not (Flag96prime-))
(not (Flag83prime-))
(not (Flag81prime-))
(not (Flag76prime-))
(not (Flag74prime-))
(not (Flag37prime-))
)
)
(:action STOP-PASS_BC-AT
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AT)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag637prime-))
(not (Flag636prime-))
(not (Flag635prime-))
(not (Flag634prime-))
(not (Flag633prime-))
(not (Flag632prime-))
(not (Flag631prime-))
(not (Flag630prime-))
(not (Flag629prime-))
(not (Flag628prime-))
(not (Flag627prime-))
(not (Flag626prime-))
(not (Flag87prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag93prime-))
(not (Flag91prime-))
(not (Flag89prime-))
(not (Flag69prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag92prime-))
(not (Flag85prime-))
(not (Flag84prime-))
(not (Flag97prime-))
(not (Flag95prime-))
(not (Flag94prime-))
(not (Flag82prime-))
(not (Flag98prime-))
(not (Flag96prime-))
(not (Flag83prime-))
(not (Flag81prime-))
(not (Flag76prime-))
(not (Flag74prime-))
(not (Flag37prime-))
)
)
(:action STOP-PASS_BC-AR
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AR)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag637prime-))
(not (Flag636prime-))
(not (Flag635prime-))
(not (Flag634prime-))
(not (Flag633prime-))
(not (Flag632prime-))
(not (Flag631prime-))
(not (Flag630prime-))
(not (Flag629prime-))
(not (Flag628prime-))
(not (Flag627prime-))
(not (Flag626prime-))
(not (Flag87prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag93prime-))
(not (Flag91prime-))
(not (Flag89prime-))
(not (Flag69prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag92prime-))
(not (Flag85prime-))
(not (Flag84prime-))
(not (Flag97prime-))
(not (Flag95prime-))
(not (Flag94prime-))
(not (Flag82prime-))
(not (Flag98prime-))
(not (Flag96prime-))
(not (Flag83prime-))
(not (Flag81prime-))
(not (Flag76prime-))
(not (Flag74prime-))
(not (Flag37prime-))
)
)
(:action STOP-PASS_BC-AJ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AJ)
)
:effect
(and
(when
(and
(BOARDED-PASS_BC)
)
(and
(SERVED-PASS_BC)
(NOT-BOARDED-PASS_BC)
(not (NOT-SERVED-PASS_BC))
(not (BOARDED-PASS_BC))
)
)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag637prime-))
(not (Flag636prime-))
(not (Flag635prime-))
(not (Flag634prime-))
(not (Flag633prime-))
(not (Flag632prime-))
(not (Flag631prime-))
(not (Flag630prime-))
(not (Flag629prime-))
(not (Flag628prime-))
(not (Flag627prime-))
(not (Flag626prime-))
(not (Flag87prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag93prime-))
(not (Flag91prime-))
(not (Flag89prime-))
(not (Flag69prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag92prime-))
(not (Flag85prime-))
(not (Flag84prime-))
(not (Flag97prime-))
(not (Flag95prime-))
(not (Flag94prime-))
(not (Flag82prime-))
(not (Flag98prime-))
(not (Flag96prime-))
(not (Flag83prime-))
(not (Flag81prime-))
(not (Flag76prime-))
(not (Flag74prime-))
(not (Flag37prime-))
)
)
(:action STOP-PASS_AU-AT
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AT)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag637prime-))
(not (Flag636prime-))
(not (Flag635prime-))
(not (Flag634prime-))
(not (Flag633prime-))
(not (Flag632prime-))
(not (Flag631prime-))
(not (Flag630prime-))
(not (Flag629prime-))
(not (Flag628prime-))
(not (Flag627prime-))
(not (Flag626prime-))
(not (Flag87prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag93prime-))
(not (Flag91prime-))
(not (Flag89prime-))
(not (Flag69prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag92prime-))
(not (Flag85prime-))
(not (Flag84prime-))
(not (Flag97prime-))
(not (Flag95prime-))
(not (Flag94prime-))
(not (Flag82prime-))
(not (Flag98prime-))
(not (Flag96prime-))
(not (Flag83prime-))
(not (Flag81prime-))
(not (Flag76prime-))
(not (Flag74prime-))
(not (Flag37prime-))
)
)
(:action STOP-PASS_AU-AR
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AR)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag637prime-))
(not (Flag636prime-))
(not (Flag635prime-))
(not (Flag634prime-))
(not (Flag633prime-))
(not (Flag632prime-))
(not (Flag631prime-))
(not (Flag630prime-))
(not (Flag629prime-))
(not (Flag628prime-))
(not (Flag627prime-))
(not (Flag626prime-))
(not (Flag87prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag93prime-))
(not (Flag91prime-))
(not (Flag89prime-))
(not (Flag69prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag92prime-))
(not (Flag85prime-))
(not (Flag84prime-))
(not (Flag97prime-))
(not (Flag95prime-))
(not (Flag94prime-))
(not (Flag82prime-))
(not (Flag98prime-))
(not (Flag96prime-))
(not (Flag83prime-))
(not (Flag81prime-))
(not (Flag76prime-))
(not (Flag74prime-))
(not (Flag37prime-))
)
)
(:action STOP-PASS_AU-AJ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AJ)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag637prime-))
(not (Flag636prime-))
(not (Flag635prime-))
(not (Flag634prime-))
(not (Flag633prime-))
(not (Flag632prime-))
(not (Flag631prime-))
(not (Flag630prime-))
(not (Flag629prime-))
(not (Flag628prime-))
(not (Flag627prime-))
(not (Flag626prime-))
(not (Flag87prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag93prime-))
(not (Flag91prime-))
(not (Flag89prime-))
(not (Flag69prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag92prime-))
(not (Flag85prime-))
(not (Flag84prime-))
(not (Flag97prime-))
(not (Flag95prime-))
(not (Flag94prime-))
(not (Flag82prime-))
(not (Flag98prime-))
(not (Flag96prime-))
(not (Flag83prime-))
(not (Flag81prime-))
(not (Flag76prime-))
(not (Flag74prime-))
(not (Flag37prime-))
)
)
(:action STOP-PASS_AX-AT
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AT)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag637prime-))
(not (Flag636prime-))
(not (Flag635prime-))
(not (Flag634prime-))
(not (Flag633prime-))
(not (Flag632prime-))
(not (Flag631prime-))
(not (Flag630prime-))
(not (Flag629prime-))
(not (Flag628prime-))
(not (Flag627prime-))
(not (Flag626prime-))
(not (Flag87prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag93prime-))
(not (Flag91prime-))
(not (Flag89prime-))
(not (Flag69prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag92prime-))
(not (Flag85prime-))
(not (Flag84prime-))
(not (Flag97prime-))
(not (Flag95prime-))
(not (Flag94prime-))
(not (Flag82prime-))
(not (Flag98prime-))
(not (Flag96prime-))
(not (Flag83prime-))
(not (Flag81prime-))
(not (Flag76prime-))
(not (Flag74prime-))
(not (Flag37prime-))
)
)
(:action STOP-PASS_AX-AR
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AR)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag637prime-))
(not (Flag636prime-))
(not (Flag635prime-))
(not (Flag634prime-))
(not (Flag633prime-))
(not (Flag632prime-))
(not (Flag631prime-))
(not (Flag630prime-))
(not (Flag629prime-))
(not (Flag628prime-))
(not (Flag627prime-))
(not (Flag626prime-))
(not (Flag87prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag93prime-))
(not (Flag91prime-))
(not (Flag89prime-))
(not (Flag69prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag92prime-))
(not (Flag85prime-))
(not (Flag84prime-))
(not (Flag97prime-))
(not (Flag95prime-))
(not (Flag94prime-))
(not (Flag82prime-))
(not (Flag98prime-))
(not (Flag96prime-))
(not (Flag83prime-))
(not (Flag81prime-))
(not (Flag76prime-))
(not (Flag74prime-))
(not (Flag37prime-))
)
)
(:action STOP-PASS_AX-AJ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AJ)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag637prime-))
(not (Flag636prime-))
(not (Flag635prime-))
(not (Flag634prime-))
(not (Flag633prime-))
(not (Flag632prime-))
(not (Flag631prime-))
(not (Flag630prime-))
(not (Flag629prime-))
(not (Flag628prime-))
(not (Flag627prime-))
(not (Flag626prime-))
(not (Flag87prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag93prime-))
(not (Flag91prime-))
(not (Flag89prime-))
(not (Flag69prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag92prime-))
(not (Flag85prime-))
(not (Flag84prime-))
(not (Flag97prime-))
(not (Flag95prime-))
(not (Flag94prime-))
(not (Flag82prime-))
(not (Flag98prime-))
(not (Flag96prime-))
(not (Flag83prime-))
(not (Flag81prime-))
(not (Flag76prime-))
(not (Flag74prime-))
(not (Flag37prime-))
)
)
(:action STOP-PASS_AA-AT
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AT)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag637prime-))
(not (Flag636prime-))
(not (Flag635prime-))
(not (Flag634prime-))
(not (Flag633prime-))
(not (Flag632prime-))
(not (Flag631prime-))
(not (Flag630prime-))
(not (Flag629prime-))
(not (Flag628prime-))
(not (Flag627prime-))
(not (Flag626prime-))
(not (Flag87prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag93prime-))
(not (Flag91prime-))
(not (Flag89prime-))
(not (Flag69prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag92prime-))
(not (Flag85prime-))
(not (Flag84prime-))
(not (Flag97prime-))
(not (Flag95prime-))
(not (Flag94prime-))
(not (Flag82prime-))
(not (Flag98prime-))
(not (Flag96prime-))
(not (Flag83prime-))
(not (Flag81prime-))
(not (Flag76prime-))
(not (Flag74prime-))
(not (Flag37prime-))
)
)
(:action STOP-PASS_AA-AR
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AR)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag637prime-))
(not (Flag636prime-))
(not (Flag635prime-))
(not (Flag634prime-))
(not (Flag633prime-))
(not (Flag632prime-))
(not (Flag631prime-))
(not (Flag630prime-))
(not (Flag629prime-))
(not (Flag628prime-))
(not (Flag627prime-))
(not (Flag626prime-))
(not (Flag87prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag93prime-))
(not (Flag91prime-))
(not (Flag89prime-))
(not (Flag69prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag92prime-))
(not (Flag85prime-))
(not (Flag84prime-))
(not (Flag97prime-))
(not (Flag95prime-))
(not (Flag94prime-))
(not (Flag82prime-))
(not (Flag98prime-))
(not (Flag96prime-))
(not (Flag83prime-))
(not (Flag81prime-))
(not (Flag76prime-))
(not (Flag74prime-))
(not (Flag37prime-))
)
)
(:action STOP-PASS_AA-AJ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AJ)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag637prime-))
(not (Flag636prime-))
(not (Flag635prime-))
(not (Flag634prime-))
(not (Flag633prime-))
(not (Flag632prime-))
(not (Flag631prime-))
(not (Flag630prime-))
(not (Flag629prime-))
(not (Flag628prime-))
(not (Flag627prime-))
(not (Flag626prime-))
(not (Flag87prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag93prime-))
(not (Flag91prime-))
(not (Flag89prime-))
(not (Flag69prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag92prime-))
(not (Flag85prime-))
(not (Flag84prime-))
(not (Flag97prime-))
(not (Flag95prime-))
(not (Flag94prime-))
(not (Flag82prime-))
(not (Flag98prime-))
(not (Flag96prime-))
(not (Flag83prime-))
(not (Flag81prime-))
(not (Flag76prime-))
(not (Flag74prime-))
(not (Flag37prime-))
)
)
(:action STOP-PASS_AB-AT
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AT)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag637prime-))
(not (Flag636prime-))
(not (Flag635prime-))
(not (Flag634prime-))
(not (Flag633prime-))
(not (Flag632prime-))
(not (Flag631prime-))
(not (Flag630prime-))
(not (Flag629prime-))
(not (Flag628prime-))
(not (Flag627prime-))
(not (Flag626prime-))
(not (Flag87prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag93prime-))
(not (Flag91prime-))
(not (Flag89prime-))
(not (Flag69prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag92prime-))
(not (Flag85prime-))
(not (Flag84prime-))
(not (Flag97prime-))
(not (Flag95prime-))
(not (Flag94prime-))
(not (Flag82prime-))
(not (Flag98prime-))
(not (Flag96prime-))
(not (Flag83prime-))
(not (Flag81prime-))
(not (Flag76prime-))
(not (Flag74prime-))
(not (Flag37prime-))
)
)
(:action STOP-PASS_AB-AR
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AR)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag637prime-))
(not (Flag636prime-))
(not (Flag635prime-))
(not (Flag634prime-))
(not (Flag633prime-))
(not (Flag632prime-))
(not (Flag631prime-))
(not (Flag630prime-))
(not (Flag629prime-))
(not (Flag628prime-))
(not (Flag627prime-))
(not (Flag626prime-))
(not (Flag87prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag93prime-))
(not (Flag91prime-))
(not (Flag89prime-))
(not (Flag69prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag92prime-))
(not (Flag85prime-))
(not (Flag84prime-))
(not (Flag97prime-))
(not (Flag95prime-))
(not (Flag94prime-))
(not (Flag82prime-))
(not (Flag98prime-))
(not (Flag96prime-))
(not (Flag83prime-))
(not (Flag81prime-))
(not (Flag76prime-))
(not (Flag74prime-))
(not (Flag37prime-))
)
)
(:action STOP-PASS_AB-AJ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AJ)
(Flag631prime-)
)
:effect
(and
(when
(and
(Flag631-)
)
(and
(BOARDED-PASS_AB)
(not (NOT-BOARDED-PASS_AB))
)
)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag637prime-))
(not (Flag636prime-))
(not (Flag635prime-))
(not (Flag634prime-))
(not (Flag633prime-))
(not (Flag632prime-))
(not (Flag631prime-))
(not (Flag630prime-))
(not (Flag629prime-))
(not (Flag628prime-))
(not (Flag627prime-))
(not (Flag626prime-))
(not (Flag87prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag93prime-))
(not (Flag91prime-))
(not (Flag89prime-))
(not (Flag69prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag92prime-))
(not (Flag85prime-))
(not (Flag84prime-))
(not (Flag97prime-))
(not (Flag95prime-))
(not (Flag94prime-))
(not (Flag82prime-))
(not (Flag98prime-))
(not (Flag96prime-))
(not (Flag83prime-))
(not (Flag81prime-))
(not (Flag76prime-))
(not (Flag74prime-))
(not (Flag37prime-))
)
)
(:action STOP-PASS_AE-AT
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AT)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag637prime-))
(not (Flag636prime-))
(not (Flag635prime-))
(not (Flag634prime-))
(not (Flag633prime-))
(not (Flag632prime-))
(not (Flag631prime-))
(not (Flag630prime-))
(not (Flag629prime-))
(not (Flag628prime-))
(not (Flag627prime-))
(not (Flag626prime-))
(not (Flag87prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag93prime-))
(not (Flag91prime-))
(not (Flag89prime-))
(not (Flag69prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag92prime-))
(not (Flag85prime-))
(not (Flag84prime-))
(not (Flag97prime-))
(not (Flag95prime-))
(not (Flag94prime-))
(not (Flag82prime-))
(not (Flag98prime-))
(not (Flag96prime-))
(not (Flag83prime-))
(not (Flag81prime-))
(not (Flag76prime-))
(not (Flag74prime-))
(not (Flag37prime-))
)
)
(:action STOP-PASS_AE-AR
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AR)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag637prime-))
(not (Flag636prime-))
(not (Flag635prime-))
(not (Flag634prime-))
(not (Flag633prime-))
(not (Flag632prime-))
(not (Flag631prime-))
(not (Flag630prime-))
(not (Flag629prime-))
(not (Flag628prime-))
(not (Flag627prime-))
(not (Flag626prime-))
(not (Flag87prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag93prime-))
(not (Flag91prime-))
(not (Flag89prime-))
(not (Flag69prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag92prime-))
(not (Flag85prime-))
(not (Flag84prime-))
(not (Flag97prime-))
(not (Flag95prime-))
(not (Flag94prime-))
(not (Flag82prime-))
(not (Flag98prime-))
(not (Flag96prime-))
(not (Flag83prime-))
(not (Flag81prime-))
(not (Flag76prime-))
(not (Flag74prime-))
(not (Flag37prime-))
)
)
(:action STOP-PASS_AE-AJ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AJ)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag637prime-))
(not (Flag636prime-))
(not (Flag635prime-))
(not (Flag634prime-))
(not (Flag633prime-))
(not (Flag632prime-))
(not (Flag631prime-))
(not (Flag630prime-))
(not (Flag629prime-))
(not (Flag628prime-))
(not (Flag627prime-))
(not (Flag626prime-))
(not (Flag87prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag93prime-))
(not (Flag91prime-))
(not (Flag89prime-))
(not (Flag69prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag92prime-))
(not (Flag85prime-))
(not (Flag84prime-))
(not (Flag97prime-))
(not (Flag95prime-))
(not (Flag94prime-))
(not (Flag82prime-))
(not (Flag98prime-))
(not (Flag96prime-))
(not (Flag83prime-))
(not (Flag81prime-))
(not (Flag76prime-))
(not (Flag74prime-))
(not (Flag37prime-))
)
)
(:action STOP-PASS_AD-AT
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AT)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag637prime-))
(not (Flag636prime-))
(not (Flag635prime-))
(not (Flag634prime-))
(not (Flag633prime-))
(not (Flag632prime-))
(not (Flag631prime-))
(not (Flag630prime-))
(not (Flag629prime-))
(not (Flag628prime-))
(not (Flag627prime-))
(not (Flag626prime-))
(not (Flag87prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag93prime-))
(not (Flag91prime-))
(not (Flag89prime-))
(not (Flag69prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag92prime-))
(not (Flag85prime-))
(not (Flag84prime-))
(not (Flag97prime-))
(not (Flag95prime-))
(not (Flag94prime-))
(not (Flag82prime-))
(not (Flag98prime-))
(not (Flag96prime-))
(not (Flag83prime-))
(not (Flag81prime-))
(not (Flag76prime-))
(not (Flag74prime-))
(not (Flag37prime-))
)
)
(:action STOP-PASS_AD-AR
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AR)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag637prime-))
(not (Flag636prime-))
(not (Flag635prime-))
(not (Flag634prime-))
(not (Flag633prime-))
(not (Flag632prime-))
(not (Flag631prime-))
(not (Flag630prime-))
(not (Flag629prime-))
(not (Flag628prime-))
(not (Flag627prime-))
(not (Flag626prime-))
(not (Flag87prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag93prime-))
(not (Flag91prime-))
(not (Flag89prime-))
(not (Flag69prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag92prime-))
(not (Flag85prime-))
(not (Flag84prime-))
(not (Flag97prime-))
(not (Flag95prime-))
(not (Flag94prime-))
(not (Flag82prime-))
(not (Flag98prime-))
(not (Flag96prime-))
(not (Flag83prime-))
(not (Flag81prime-))
(not (Flag76prime-))
(not (Flag74prime-))
(not (Flag37prime-))
)
)
(:action STOP-PASS_AD-AJ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AJ)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag637prime-))
(not (Flag636prime-))
(not (Flag635prime-))
(not (Flag634prime-))
(not (Flag633prime-))
(not (Flag632prime-))
(not (Flag631prime-))
(not (Flag630prime-))
(not (Flag629prime-))
(not (Flag628prime-))
(not (Flag627prime-))
(not (Flag626prime-))
(not (Flag87prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag93prime-))
(not (Flag91prime-))
(not (Flag89prime-))
(not (Flag69prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag92prime-))
(not (Flag85prime-))
(not (Flag84prime-))
(not (Flag97prime-))
(not (Flag95prime-))
(not (Flag94prime-))
(not (Flag82prime-))
(not (Flag98prime-))
(not (Flag96prime-))
(not (Flag83prime-))
(not (Flag81prime-))
(not (Flag76prime-))
(not (Flag74prime-))
(not (Flag37prime-))
)
)
(:action STOP-PASS_AL-AT
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AT)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag637prime-))
(not (Flag636prime-))
(not (Flag635prime-))
(not (Flag634prime-))
(not (Flag633prime-))
(not (Flag632prime-))
(not (Flag631prime-))
(not (Flag630prime-))
(not (Flag629prime-))
(not (Flag628prime-))
(not (Flag627prime-))
(not (Flag626prime-))
(not (Flag87prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag93prime-))
(not (Flag91prime-))
(not (Flag89prime-))
(not (Flag69prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag92prime-))
(not (Flag85prime-))
(not (Flag84prime-))
(not (Flag97prime-))
(not (Flag95prime-))
(not (Flag94prime-))
(not (Flag82prime-))
(not (Flag98prime-))
(not (Flag96prime-))
(not (Flag83prime-))
(not (Flag81prime-))
(not (Flag76prime-))
(not (Flag74prime-))
(not (Flag37prime-))
)
)
(:action STOP-PASS_AL-AR
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AR)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag637prime-))
(not (Flag636prime-))
(not (Flag635prime-))
(not (Flag634prime-))
(not (Flag633prime-))
(not (Flag632prime-))
(not (Flag631prime-))
(not (Flag630prime-))
(not (Flag629prime-))
(not (Flag628prime-))
(not (Flag627prime-))
(not (Flag626prime-))
(not (Flag87prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag93prime-))
(not (Flag91prime-))
(not (Flag89prime-))
(not (Flag69prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag92prime-))
(not (Flag85prime-))
(not (Flag84prime-))
(not (Flag97prime-))
(not (Flag95prime-))
(not (Flag94prime-))
(not (Flag82prime-))
(not (Flag98prime-))
(not (Flag96prime-))
(not (Flag83prime-))
(not (Flag81prime-))
(not (Flag76prime-))
(not (Flag74prime-))
(not (Flag37prime-))
)
)
(:action STOP-PASS_AL-AJ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AJ)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag637prime-))
(not (Flag636prime-))
(not (Flag635prime-))
(not (Flag634prime-))
(not (Flag633prime-))
(not (Flag632prime-))
(not (Flag631prime-))
(not (Flag630prime-))
(not (Flag629prime-))
(not (Flag628prime-))
(not (Flag627prime-))
(not (Flag626prime-))
(not (Flag87prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag93prime-))
(not (Flag91prime-))
(not (Flag89prime-))
(not (Flag69prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag92prime-))
(not (Flag85prime-))
(not (Flag84prime-))
(not (Flag97prime-))
(not (Flag95prime-))
(not (Flag94prime-))
(not (Flag82prime-))
(not (Flag98prime-))
(not (Flag96prime-))
(not (Flag83prime-))
(not (Flag81prime-))
(not (Flag76prime-))
(not (Flag74prime-))
(not (Flag37prime-))
)
)
(:action STOP-PASS_AN-AT
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AT)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag637prime-))
(not (Flag636prime-))
(not (Flag635prime-))
(not (Flag634prime-))
(not (Flag633prime-))
(not (Flag632prime-))
(not (Flag631prime-))
(not (Flag630prime-))
(not (Flag629prime-))
(not (Flag628prime-))
(not (Flag627prime-))
(not (Flag626prime-))
(not (Flag87prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag93prime-))
(not (Flag91prime-))
(not (Flag89prime-))
(not (Flag69prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag92prime-))
(not (Flag85prime-))
(not (Flag84prime-))
(not (Flag97prime-))
(not (Flag95prime-))
(not (Flag94prime-))
(not (Flag82prime-))
(not (Flag98prime-))
(not (Flag96prime-))
(not (Flag83prime-))
(not (Flag81prime-))
(not (Flag76prime-))
(not (Flag74prime-))
(not (Flag37prime-))
)
)
(:action STOP-PASS_AN-AR
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AR)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag637prime-))
(not (Flag636prime-))
(not (Flag635prime-))
(not (Flag634prime-))
(not (Flag633prime-))
(not (Flag632prime-))
(not (Flag631prime-))
(not (Flag630prime-))
(not (Flag629prime-))
(not (Flag628prime-))
(not (Flag627prime-))
(not (Flag626prime-))
(not (Flag87prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag93prime-))
(not (Flag91prime-))
(not (Flag89prime-))
(not (Flag69prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag92prime-))
(not (Flag85prime-))
(not (Flag84prime-))
(not (Flag97prime-))
(not (Flag95prime-))
(not (Flag94prime-))
(not (Flag82prime-))
(not (Flag98prime-))
(not (Flag96prime-))
(not (Flag83prime-))
(not (Flag81prime-))
(not (Flag76prime-))
(not (Flag74prime-))
(not (Flag37prime-))
)
)
(:action STOP-PASS_AN-AJ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AJ)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag637prime-))
(not (Flag636prime-))
(not (Flag635prime-))
(not (Flag634prime-))
(not (Flag633prime-))
(not (Flag632prime-))
(not (Flag631prime-))
(not (Flag630prime-))
(not (Flag629prime-))
(not (Flag628prime-))
(not (Flag627prime-))
(not (Flag626prime-))
(not (Flag87prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag93prime-))
(not (Flag91prime-))
(not (Flag89prime-))
(not (Flag69prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag92prime-))
(not (Flag85prime-))
(not (Flag84prime-))
(not (Flag97prime-))
(not (Flag95prime-))
(not (Flag94prime-))
(not (Flag82prime-))
(not (Flag98prime-))
(not (Flag96prime-))
(not (Flag83prime-))
(not (Flag81prime-))
(not (Flag76prime-))
(not (Flag74prime-))
(not (Flag37prime-))
)
)
(:action STOP-PASS_AS-AT
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AT)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag637prime-))
(not (Flag636prime-))
(not (Flag635prime-))
(not (Flag634prime-))
(not (Flag633prime-))
(not (Flag632prime-))
(not (Flag631prime-))
(not (Flag630prime-))
(not (Flag629prime-))
(not (Flag628prime-))
(not (Flag627prime-))
(not (Flag626prime-))
(not (Flag87prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag93prime-))
(not (Flag91prime-))
(not (Flag89prime-))
(not (Flag69prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag92prime-))
(not (Flag85prime-))
(not (Flag84prime-))
(not (Flag97prime-))
(not (Flag95prime-))
(not (Flag94prime-))
(not (Flag82prime-))
(not (Flag98prime-))
(not (Flag96prime-))
(not (Flag83prime-))
(not (Flag81prime-))
(not (Flag76prime-))
(not (Flag74prime-))
(not (Flag37prime-))
)
)
(:action STOP-PASS_AS-AR
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AR)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag637prime-))
(not (Flag636prime-))
(not (Flag635prime-))
(not (Flag634prime-))
(not (Flag633prime-))
(not (Flag632prime-))
(not (Flag631prime-))
(not (Flag630prime-))
(not (Flag629prime-))
(not (Flag628prime-))
(not (Flag627prime-))
(not (Flag626prime-))
(not (Flag87prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag93prime-))
(not (Flag91prime-))
(not (Flag89prime-))
(not (Flag69prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag92prime-))
(not (Flag85prime-))
(not (Flag84prime-))
(not (Flag97prime-))
(not (Flag95prime-))
(not (Flag94prime-))
(not (Flag82prime-))
(not (Flag98prime-))
(not (Flag96prime-))
(not (Flag83prime-))
(not (Flag81prime-))
(not (Flag76prime-))
(not (Flag74prime-))
(not (Flag37prime-))
)
)
(:action STOP-PASS_AS-AJ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AJ)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag637prime-))
(not (Flag636prime-))
(not (Flag635prime-))
(not (Flag634prime-))
(not (Flag633prime-))
(not (Flag632prime-))
(not (Flag631prime-))
(not (Flag630prime-))
(not (Flag629prime-))
(not (Flag628prime-))
(not (Flag627prime-))
(not (Flag626prime-))
(not (Flag87prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag93prime-))
(not (Flag91prime-))
(not (Flag89prime-))
(not (Flag69prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag92prime-))
(not (Flag85prime-))
(not (Flag84prime-))
(not (Flag97prime-))
(not (Flag95prime-))
(not (Flag94prime-))
(not (Flag82prime-))
(not (Flag98prime-))
(not (Flag96prime-))
(not (Flag83prime-))
(not (Flag81prime-))
(not (Flag76prime-))
(not (Flag74prime-))
(not (Flag37prime-))
)
)
(:action Flag2Action-0
:parameters ()
:precondition
(and
(Flag1-)
(Flag1prime-)
)
:effect
(and
(Flag2-)
(Flag2prime-)
)

)
(:action Flag4Action-0
:parameters ()
:precondition
(and
(Flag3-)
(Flag3prime-)
)
:effect
(and
(Flag4-)
(Flag4prime-)
)

)
(:action Flag6Action-0
:parameters ()
:precondition
(and
(Flag5-)
(Flag5prime-)
)
:effect
(and
(Flag6-)
(Flag6prime-)
)

)
(:action Flag8Action-0
:parameters ()
:precondition
(and
(Flag7-)
(Flag7prime-)
)
:effect
(and
(Flag8-)
(Flag8prime-)
)

)
(:action Flag10Action-0
:parameters ()
:precondition
(and
(Flag9-)
(Flag9prime-)
)
:effect
(and
(Flag10-)
(Flag10prime-)
)

)
(:action Flag12Action-0
:parameters ()
:precondition
(and
(Flag11-)
(Flag11prime-)
)
:effect
(and
(Flag12-)
(Flag12prime-)
)

)
(:action Flag14Action-0
:parameters ()
:precondition
(and
(Flag13-)
(Flag13prime-)
)
:effect
(and
(Flag14-)
(Flag14prime-)
)

)
(:action Flag16Action-0
:parameters ()
:precondition
(and
(Flag15-)
(Flag15prime-)
)
:effect
(and
(Flag16-)
(Flag16prime-)
)

)
(:action Flag18Action-0
:parameters ()
:precondition
(and
(Flag17-)
(Flag17prime-)
)
:effect
(and
(Flag18-)
(Flag18prime-)
)

)
(:action Flag20Action-0
:parameters ()
:precondition
(and
(Flag19-)
(Flag19prime-)
)
:effect
(and
(Flag20-)
(Flag20prime-)
)

)
(:action Flag22Action-0
:parameters ()
:precondition
(and
(Flag21-)
(Flag21prime-)
)
:effect
(and
(Flag22-)
(Flag22prime-)
)

)
(:action Flag24Action-0
:parameters ()
:precondition
(and
(Flag23-)
(Flag23prime-)
)
:effect
(and
(Flag24-)
(Flag24prime-)
)

)
(:action Flag26Action-0
:parameters ()
:precondition
(and
(Flag25-)
(Flag25prime-)
)
:effect
(and
(Flag26-)
(Flag26prime-)
)

)
(:action Flag28Action-0
:parameters ()
:precondition
(and
(Flag27-)
(Flag27prime-)
)
:effect
(and
(Flag28-)
(Flag28prime-)
)

)
(:action Flag30Action-0
:parameters ()
:precondition
(and
(Flag29-)
(Flag29prime-)
)
:effect
(and
(Flag30-)
(Flag30prime-)
)

)
(:action Flag32Action-0
:parameters ()
:precondition
(and
(Flag31-)
(Flag31prime-)
)
:effect
(and
(Flag32-)
(Flag32prime-)
)

)
(:action Flag34Action-0
:parameters ()
:precondition
(and
(Flag33-)
(Flag33prime-)
)
:effect
(and
(Flag34-)
(Flag34prime-)
)

)
(:action Flag36Action-0
:parameters ()
:precondition
(and
(Flag35-)
(Flag35prime-)
)
:effect
(and
(Flag36-)
(Flag36prime-)
)

)
(:action Prim37Action-10
:parameters ()
:precondition
(and
(not (SERVED-PASS_AN))
)
:effect
(and
(Flag37prime-)
(not (Flag37-))
)

)
(:action Prim37Action-12
:parameters ()
:precondition
(and
(not (SERVED-PASS_AL))
)
:effect
(and
(Flag37prime-)
(not (Flag37-))
)

)
(:action Prim37Action-19
:parameters ()
:precondition
(and
(not (SERVED-PASS_AE))
)
:effect
(and
(Flag37prime-)
(not (Flag37-))
)

)
(:action Prim37Action-20
:parameters ()
:precondition
(and
(not (SERVED-PASS_AB))
)
:effect
(and
(Flag37prime-)
(not (Flag37-))
)

)
(:action Prim37Action-23
:parameters ()
:precondition
(and
(not (SERVED-PASS_AX))
)
:effect
(and
(Flag37prime-)
(not (Flag37-))
)

)
(:action Prim37Action-25
:parameters ()
:precondition
(and
(not (SERVED-PASS_AU))
)
:effect
(and
(Flag37prime-)
(not (Flag37-))
)

)
(:action MOVEUP-AR-AT
:parameters ()
:precondition
(and
(LIFT_AT-AR)
(NOT-ERROR-)
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(NOT-LIFT_AT-AR)
(LIFT_AT-AT)
(CHECKCONSISTENCY-)
(not (LIFT_AT-AR))
(not (NOT-LIFT_AT-AT))
(not (NOT-CHECKCONSISTENCY-))
(not (Flag637prime-))
(not (Flag636prime-))
(not (Flag635prime-))
(not (Flag634prime-))
(not (Flag633prime-))
(not (Flag632prime-))
(not (Flag631prime-))
(not (Flag630prime-))
(not (Flag629prime-))
(not (Flag628prime-))
(not (Flag627prime-))
(not (Flag626prime-))
(not (Flag87prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag93prime-))
(not (Flag91prime-))
(not (Flag89prime-))
(not (Flag69prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag92prime-))
(not (Flag85prime-))
(not (Flag84prime-))
(not (Flag97prime-))
(not (Flag95prime-))
(not (Flag94prime-))
(not (Flag82prime-))
(not (Flag98prime-))
(not (Flag96prime-))
(not (Flag83prime-))
(not (Flag81prime-))
(not (Flag76prime-))
(not (Flag74prime-))
(not (Flag37prime-))
)
)
(:action MOVEUP-AJ-AK
:parameters ()
:precondition
(and
(LIFT_AT-AJ)
(NOT-ERROR-)
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(NOT-LIFT_AT-AJ)
(LIFT_AT-AK)
(CHECKCONSISTENCY-)
(not (LIFT_AT-AJ))
(not (NOT-LIFT_AT-AK))
(not (NOT-CHECKCONSISTENCY-))
(not (Flag637prime-))
(not (Flag636prime-))
(not (Flag635prime-))
(not (Flag634prime-))
(not (Flag633prime-))
(not (Flag632prime-))
(not (Flag631prime-))
(not (Flag630prime-))
(not (Flag629prime-))
(not (Flag628prime-))
(not (Flag627prime-))
(not (Flag626prime-))
(not (Flag87prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag93prime-))
(not (Flag91prime-))
(not (Flag89prime-))
(not (Flag69prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag92prime-))
(not (Flag85prime-))
(not (Flag84prime-))
(not (Flag97prime-))
(not (Flag95prime-))
(not (Flag94prime-))
(not (Flag82prime-))
(not (Flag98prime-))
(not (Flag96prime-))
(not (Flag83prime-))
(not (Flag81prime-))
(not (Flag76prime-))
(not (Flag74prime-))
(not (Flag37prime-))
)
)
(:action MOVEUP-AQ-AR
:parameters ()
:precondition
(and
(LIFT_AT-AQ)
(NOT-ERROR-)
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(NOT-LIFT_AT-AQ)
(LIFT_AT-AR)
(CHECKCONSISTENCY-)
(not (LIFT_AT-AQ))
(not (NOT-LIFT_AT-AR))
(not (NOT-CHECKCONSISTENCY-))
(not (Flag637prime-))
(not (Flag636prime-))
(not (Flag635prime-))
(not (Flag634prime-))
(not (Flag633prime-))
(not (Flag632prime-))
(not (Flag631prime-))
(not (Flag630prime-))
(not (Flag629prime-))
(not (Flag628prime-))
(not (Flag627prime-))
(not (Flag626prime-))
(not (Flag87prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag93prime-))
(not (Flag91prime-))
(not (Flag89prime-))
(not (Flag69prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag92prime-))
(not (Flag85prime-))
(not (Flag84prime-))
(not (Flag97prime-))
(not (Flag95prime-))
(not (Flag94prime-))
(not (Flag82prime-))
(not (Flag98prime-))
(not (Flag96prime-))
(not (Flag83prime-))
(not (Flag81prime-))
(not (Flag76prime-))
(not (Flag74prime-))
(not (Flag37prime-))
)
)
(:action MOVEDOWN-AK-AJ
:parameters ()
:precondition
(and
(LIFT_AT-AK)
(NOT-ERROR-)
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(NOT-LIFT_AT-AK)
(LIFT_AT-AJ)
(CHECKCONSISTENCY-)
(not (LIFT_AT-AK))
(not (NOT-LIFT_AT-AJ))
(not (NOT-CHECKCONSISTENCY-))
(not (Flag637prime-))
(not (Flag636prime-))
(not (Flag635prime-))
(not (Flag634prime-))
(not (Flag633prime-))
(not (Flag632prime-))
(not (Flag631prime-))
(not (Flag630prime-))
(not (Flag629prime-))
(not (Flag628prime-))
(not (Flag627prime-))
(not (Flag626prime-))
(not (Flag87prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag93prime-))
(not (Flag91prime-))
(not (Flag89prime-))
(not (Flag69prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag92prime-))
(not (Flag85prime-))
(not (Flag84prime-))
(not (Flag97prime-))
(not (Flag95prime-))
(not (Flag94prime-))
(not (Flag82prime-))
(not (Flag98prime-))
(not (Flag96prime-))
(not (Flag83prime-))
(not (Flag81prime-))
(not (Flag76prime-))
(not (Flag74prime-))
(not (Flag37prime-))
)
)
(:action MOVEDOWN-AQ-AP
:parameters ()
:precondition
(and
(LIFT_AT-AQ)
(NOT-ERROR-)
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(NOT-LIFT_AT-AQ)
(LIFT_AT-AP)
(CHECKCONSISTENCY-)
(not (LIFT_AT-AQ))
(not (NOT-LIFT_AT-AP))
(not (NOT-CHECKCONSISTENCY-))
(not (Flag637prime-))
(not (Flag636prime-))
(not (Flag635prime-))
(not (Flag634prime-))
(not (Flag633prime-))
(not (Flag632prime-))
(not (Flag631prime-))
(not (Flag630prime-))
(not (Flag629prime-))
(not (Flag628prime-))
(not (Flag627prime-))
(not (Flag626prime-))
(not (Flag87prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag93prime-))
(not (Flag91prime-))
(not (Flag89prime-))
(not (Flag69prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag92prime-))
(not (Flag85prime-))
(not (Flag84prime-))
(not (Flag97prime-))
(not (Flag95prime-))
(not (Flag94prime-))
(not (Flag82prime-))
(not (Flag98prime-))
(not (Flag96prime-))
(not (Flag83prime-))
(not (Flag81prime-))
(not (Flag76prime-))
(not (Flag74prime-))
(not (Flag37prime-))
)
)
(:action Prim39Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AQ))
)
:effect
(and
)

)
(:action Prim59Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AO))
)
:effect
(and
)

)
(:action Prim61Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AM))
)
:effect
(and
)

)
(:action Prim63Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AK))
)
:effect
(and
)

)
(:action Flag69Action
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AQ)
)
:effect
(and
(Flag69-)
(Flag69prime-)
)

)
(:action Prim69Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AQ))
)
:effect
(and
(Flag69prime-)
(not (Flag69-))
)

)
(:action Flag89Action
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AO)
)
:effect
(and
(Flag89-)
(Flag89prime-)
)

)
(:action Prim89Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AO))
)
:effect
(and
(Flag89prime-)
(not (Flag89-))
)

)
(:action Flag91Action
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AM)
)
:effect
(and
(Flag91-)
(Flag91prime-)
)

)
(:action Prim91Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AM))
)
:effect
(and
(Flag91prime-)
(not (Flag91-))
)

)
(:action Flag93Action
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AK)
)
:effect
(and
(Flag93-)
(Flag93prime-)
)

)
(:action Prim93Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AK))
)
:effect
(and
(Flag93prime-)
(not (Flag93-))
)

)
(:action Prim100Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AQ))
)
:effect
(and
)

)
(:action Prim120Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AO))
)
:effect
(and
)

)
(:action Prim122Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AM))
)
:effect
(and
)

)
(:action Prim124Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AK))
)
:effect
(and
)

)
(:action Prim131Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AQ))
)
:effect
(and
)

)
(:action Prim151Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AO))
)
:effect
(and
)

)
(:action Prim153Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AM))
)
:effect
(and
)

)
(:action Prim155Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AK))
)
:effect
(and
)

)
(:action Prim162Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AQ))
)
:effect
(and
)

)
(:action Prim182Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AO))
)
:effect
(and
)

)
(:action Prim184Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AM))
)
:effect
(and
)

)
(:action Prim186Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AK))
)
:effect
(and
)

)
(:action Prim193Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AQ))
)
:effect
(and
)

)
(:action Prim213Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AO))
)
:effect
(and
)

)
(:action Prim215Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AM))
)
:effect
(and
)

)
(:action Prim217Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AK))
)
:effect
(and
)

)
(:action Prim224Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AQ))
)
:effect
(and
)

)
(:action Prim244Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AO))
)
:effect
(and
)

)
(:action Prim246Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AM))
)
:effect
(and
)

)
(:action Prim248Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AK))
)
:effect
(and
)

)
(:action Prim255Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AQ))
)
:effect
(and
)

)
(:action Prim275Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AO))
)
:effect
(and
)

)
(:action Prim277Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AM))
)
:effect
(and
)

)
(:action Prim279Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AK))
)
:effect
(and
)

)
(:action Prim286Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AQ))
)
:effect
(and
)

)
(:action Prim306Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AO))
)
:effect
(and
)

)
(:action Prim308Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AM))
)
:effect
(and
)

)
(:action Prim310Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AK))
)
:effect
(and
)

)
(:action Prim317Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AQ))
)
:effect
(and
)

)
(:action Prim337Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AO))
)
:effect
(and
)

)
(:action Prim339Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AM))
)
:effect
(and
)

)
(:action Prim341Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AK))
)
:effect
(and
)

)
(:action Prim348Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AQ))
)
:effect
(and
)

)
(:action Prim368Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AO))
)
:effect
(and
)

)
(:action Prim370Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AM))
)
:effect
(and
)

)
(:action Prim372Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AK))
)
:effect
(and
)

)
(:action Prim379Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AQ))
)
:effect
(and
)

)
(:action Prim399Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AO))
)
:effect
(and
)

)
(:action Prim401Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AM))
)
:effect
(and
)

)
(:action Prim403Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AK))
)
:effect
(and
)

)
(:action Prim410Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AQ))
)
:effect
(and
)

)
(:action Prim430Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AO))
)
:effect
(and
)

)
(:action Prim432Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AM))
)
:effect
(and
)

)
(:action Prim434Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AK))
)
:effect
(and
)

)
(:action Prim441Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AQ))
)
:effect
(and
)

)
(:action Prim461Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AO))
)
:effect
(and
)

)
(:action Prim463Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AM))
)
:effect
(and
)

)
(:action Prim465Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AK))
)
:effect
(and
)

)
(:action Prim472Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AQ))
)
:effect
(and
)

)
(:action Prim492Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AO))
)
:effect
(and
)

)
(:action Prim494Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AM))
)
:effect
(and
)

)
(:action Prim496Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AK))
)
:effect
(and
)

)
(:action Prim503Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AQ))
)
:effect
(and
)

)
(:action Prim523Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AO))
)
:effect
(and
)

)
(:action Prim525Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AM))
)
:effect
(and
)

)
(:action Prim527Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AK))
)
:effect
(and
)

)
(:action Prim534Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AQ))
)
:effect
(and
)

)
(:action Prim554Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AO))
)
:effect
(and
)

)
(:action Prim556Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AM))
)
:effect
(and
)

)
(:action Prim558Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AK))
)
:effect
(and
)

)
(:action Prim565Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AQ))
)
:effect
(and
)

)
(:action Prim585Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AO))
)
:effect
(and
)

)
(:action Prim587Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AM))
)
:effect
(and
)

)
(:action Prim589Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AK))
)
:effect
(and
)

)
(:action Prim596Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AQ))
)
:effect
(and
)

)
(:action Prim616Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AO))
)
:effect
(and
)

)
(:action Prim618Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AM))
)
:effect
(and
)

)
(:action Prim620Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AK))
)
:effect
(and
)

)
(:action STOP-PASS_BA-AP
:parameters ()
:precondition
(and
(Flag87-)
(Flag87prime-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag637prime-))
(not (Flag636prime-))
(not (Flag635prime-))
(not (Flag634prime-))
(not (Flag633prime-))
(not (Flag632prime-))
(not (Flag631prime-))
(not (Flag630prime-))
(not (Flag629prime-))
(not (Flag628prime-))
(not (Flag627prime-))
(not (Flag626prime-))
(not (Flag87prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag93prime-))
(not (Flag91prime-))
(not (Flag89prime-))
(not (Flag69prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag92prime-))
(not (Flag85prime-))
(not (Flag84prime-))
(not (Flag97prime-))
(not (Flag95prime-))
(not (Flag94prime-))
(not (Flag82prime-))
(not (Flag98prime-))
(not (Flag96prime-))
(not (Flag83prime-))
(not (Flag81prime-))
(not (Flag76prime-))
(not (Flag74prime-))
(not (Flag37prime-))
)
)
(:action STOP-PASS_BB-AQ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AQ)
(Flag636prime-)
)
:effect
(and
(when
(and
(Flag636-)
)
(and
(BOARDED-PASS_BB)
(not (NOT-BOARDED-PASS_BB))
)
)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag637prime-))
(not (Flag636prime-))
(not (Flag635prime-))
(not (Flag634prime-))
(not (Flag633prime-))
(not (Flag632prime-))
(not (Flag631prime-))
(not (Flag630prime-))
(not (Flag629prime-))
(not (Flag628prime-))
(not (Flag627prime-))
(not (Flag626prime-))
(not (Flag87prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag93prime-))
(not (Flag91prime-))
(not (Flag89prime-))
(not (Flag69prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag92prime-))
(not (Flag85prime-))
(not (Flag84prime-))
(not (Flag97prime-))
(not (Flag95prime-))
(not (Flag94prime-))
(not (Flag82prime-))
(not (Flag98prime-))
(not (Flag96prime-))
(not (Flag83prime-))
(not (Flag81prime-))
(not (Flag76prime-))
(not (Flag74prime-))
(not (Flag37prime-))
)
)
(:action STOP-PASS_BB-AP
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AP)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag637prime-))
(not (Flag636prime-))
(not (Flag635prime-))
(not (Flag634prime-))
(not (Flag633prime-))
(not (Flag632prime-))
(not (Flag631prime-))
(not (Flag630prime-))
(not (Flag629prime-))
(not (Flag628prime-))
(not (Flag627prime-))
(not (Flag626prime-))
(not (Flag87prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag93prime-))
(not (Flag91prime-))
(not (Flag89prime-))
(not (Flag69prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag92prime-))
(not (Flag85prime-))
(not (Flag84prime-))
(not (Flag97prime-))
(not (Flag95prime-))
(not (Flag94prime-))
(not (Flag82prime-))
(not (Flag98prime-))
(not (Flag96prime-))
(not (Flag83prime-))
(not (Flag81prime-))
(not (Flag76prime-))
(not (Flag74prime-))
(not (Flag37prime-))
)
)
(:action STOP-PASS_BB-AO
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AO)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag637prime-))
(not (Flag636prime-))
(not (Flag635prime-))
(not (Flag634prime-))
(not (Flag633prime-))
(not (Flag632prime-))
(not (Flag631prime-))
(not (Flag630prime-))
(not (Flag629prime-))
(not (Flag628prime-))
(not (Flag627prime-))
(not (Flag626prime-))
(not (Flag87prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag93prime-))
(not (Flag91prime-))
(not (Flag89prime-))
(not (Flag69prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag92prime-))
(not (Flag85prime-))
(not (Flag84prime-))
(not (Flag97prime-))
(not (Flag95prime-))
(not (Flag94prime-))
(not (Flag82prime-))
(not (Flag98prime-))
(not (Flag96prime-))
(not (Flag83prime-))
(not (Flag81prime-))
(not (Flag76prime-))
(not (Flag74prime-))
(not (Flag37prime-))
)
)
(:action STOP-PASS_BB-AM
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AM)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag637prime-))
(not (Flag636prime-))
(not (Flag635prime-))
(not (Flag634prime-))
(not (Flag633prime-))
(not (Flag632prime-))
(not (Flag631prime-))
(not (Flag630prime-))
(not (Flag629prime-))
(not (Flag628prime-))
(not (Flag627prime-))
(not (Flag626prime-))
(not (Flag87prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag93prime-))
(not (Flag91prime-))
(not (Flag89prime-))
(not (Flag69prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag92prime-))
(not (Flag85prime-))
(not (Flag84prime-))
(not (Flag97prime-))
(not (Flag95prime-))
(not (Flag94prime-))
(not (Flag82prime-))
(not (Flag98prime-))
(not (Flag96prime-))
(not (Flag83prime-))
(not (Flag81prime-))
(not (Flag76prime-))
(not (Flag74prime-))
(not (Flag37prime-))
)
)
(:action STOP-PASS_BB-AK
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AK)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag637prime-))
(not (Flag636prime-))
(not (Flag635prime-))
(not (Flag634prime-))
(not (Flag633prime-))
(not (Flag632prime-))
(not (Flag631prime-))
(not (Flag630prime-))
(not (Flag629prime-))
(not (Flag628prime-))
(not (Flag627prime-))
(not (Flag626prime-))
(not (Flag87prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag93prime-))
(not (Flag91prime-))
(not (Flag89prime-))
(not (Flag69prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag92prime-))
(not (Flag85prime-))
(not (Flag84prime-))
(not (Flag97prime-))
(not (Flag95prime-))
(not (Flag94prime-))
(not (Flag82prime-))
(not (Flag98prime-))
(not (Flag96prime-))
(not (Flag83prime-))
(not (Flag81prime-))
(not (Flag76prime-))
(not (Flag74prime-))
(not (Flag37prime-))
)
)
(:action STOP-PASS_BC-AQ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AQ)
(Flag635prime-)
)
:effect
(and
(when
(and
(Flag635-)
)
(and
(BOARDED-PASS_BC)
(not (NOT-BOARDED-PASS_BC))
)
)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag637prime-))
(not (Flag636prime-))
(not (Flag635prime-))
(not (Flag634prime-))
(not (Flag633prime-))
(not (Flag632prime-))
(not (Flag631prime-))
(not (Flag630prime-))
(not (Flag629prime-))
(not (Flag628prime-))
(not (Flag627prime-))
(not (Flag626prime-))
(not (Flag87prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag93prime-))
(not (Flag91prime-))
(not (Flag89prime-))
(not (Flag69prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag92prime-))
(not (Flag85prime-))
(not (Flag84prime-))
(not (Flag97prime-))
(not (Flag95prime-))
(not (Flag94prime-))
(not (Flag82prime-))
(not (Flag98prime-))
(not (Flag96prime-))
(not (Flag83prime-))
(not (Flag81prime-))
(not (Flag76prime-))
(not (Flag74prime-))
(not (Flag37prime-))
)
)
(:action STOP-PASS_BC-AP
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AP)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag637prime-))
(not (Flag636prime-))
(not (Flag635prime-))
(not (Flag634prime-))
(not (Flag633prime-))
(not (Flag632prime-))
(not (Flag631prime-))
(not (Flag630prime-))
(not (Flag629prime-))
(not (Flag628prime-))
(not (Flag627prime-))
(not (Flag626prime-))
(not (Flag87prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag93prime-))
(not (Flag91prime-))
(not (Flag89prime-))
(not (Flag69prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag92prime-))
(not (Flag85prime-))
(not (Flag84prime-))
(not (Flag97prime-))
(not (Flag95prime-))
(not (Flag94prime-))
(not (Flag82prime-))
(not (Flag98prime-))
(not (Flag96prime-))
(not (Flag83prime-))
(not (Flag81prime-))
(not (Flag76prime-))
(not (Flag74prime-))
(not (Flag37prime-))
)
)
(:action STOP-PASS_BC-AO
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AO)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag637prime-))
(not (Flag636prime-))
(not (Flag635prime-))
(not (Flag634prime-))
(not (Flag633prime-))
(not (Flag632prime-))
(not (Flag631prime-))
(not (Flag630prime-))
(not (Flag629prime-))
(not (Flag628prime-))
(not (Flag627prime-))
(not (Flag626prime-))
(not (Flag87prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag93prime-))
(not (Flag91prime-))
(not (Flag89prime-))
(not (Flag69prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag92prime-))
(not (Flag85prime-))
(not (Flag84prime-))
(not (Flag97prime-))
(not (Flag95prime-))
(not (Flag94prime-))
(not (Flag82prime-))
(not (Flag98prime-))
(not (Flag96prime-))
(not (Flag83prime-))
(not (Flag81prime-))
(not (Flag76prime-))
(not (Flag74prime-))
(not (Flag37prime-))
)
)
(:action STOP-PASS_BC-AM
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AM)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag637prime-))
(not (Flag636prime-))
(not (Flag635prime-))
(not (Flag634prime-))
(not (Flag633prime-))
(not (Flag632prime-))
(not (Flag631prime-))
(not (Flag630prime-))
(not (Flag629prime-))
(not (Flag628prime-))
(not (Flag627prime-))
(not (Flag626prime-))
(not (Flag87prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag93prime-))
(not (Flag91prime-))
(not (Flag89prime-))
(not (Flag69prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag92prime-))
(not (Flag85prime-))
(not (Flag84prime-))
(not (Flag97prime-))
(not (Flag95prime-))
(not (Flag94prime-))
(not (Flag82prime-))
(not (Flag98prime-))
(not (Flag96prime-))
(not (Flag83prime-))
(not (Flag81prime-))
(not (Flag76prime-))
(not (Flag74prime-))
(not (Flag37prime-))
)
)
(:action STOP-PASS_BC-AK
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AK)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag637prime-))
(not (Flag636prime-))
(not (Flag635prime-))
(not (Flag634prime-))
(not (Flag633prime-))
(not (Flag632prime-))
(not (Flag631prime-))
(not (Flag630prime-))
(not (Flag629prime-))
(not (Flag628prime-))
(not (Flag627prime-))
(not (Flag626prime-))
(not (Flag87prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag93prime-))
(not (Flag91prime-))
(not (Flag89prime-))
(not (Flag69prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag92prime-))
(not (Flag85prime-))
(not (Flag84prime-))
(not (Flag97prime-))
(not (Flag95prime-))
(not (Flag94prime-))
(not (Flag82prime-))
(not (Flag98prime-))
(not (Flag96prime-))
(not (Flag83prime-))
(not (Flag81prime-))
(not (Flag76prime-))
(not (Flag74prime-))
(not (Flag37prime-))
)
)
(:action STOP-PASS_AU-AQ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AQ)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag637prime-))
(not (Flag636prime-))
(not (Flag635prime-))
(not (Flag634prime-))
(not (Flag633prime-))
(not (Flag632prime-))
(not (Flag631prime-))
(not (Flag630prime-))
(not (Flag629prime-))
(not (Flag628prime-))
(not (Flag627prime-))
(not (Flag626prime-))
(not (Flag87prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag93prime-))
(not (Flag91prime-))
(not (Flag89prime-))
(not (Flag69prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag92prime-))
(not (Flag85prime-))
(not (Flag84prime-))
(not (Flag97prime-))
(not (Flag95prime-))
(not (Flag94prime-))
(not (Flag82prime-))
(not (Flag98prime-))
(not (Flag96prime-))
(not (Flag83prime-))
(not (Flag81prime-))
(not (Flag76prime-))
(not (Flag74prime-))
(not (Flag37prime-))
)
)
(:action STOP-PASS_AU-AP
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AP)
)
:effect
(and
(when
(and
(BOARDED-PASS_AU)
)
(and
(SERVED-PASS_AU)
(NOT-BOARDED-PASS_AU)
(not (NOT-SERVED-PASS_AU))
(not (BOARDED-PASS_AU))
)
)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag637prime-))
(not (Flag636prime-))
(not (Flag635prime-))
(not (Flag634prime-))
(not (Flag633prime-))
(not (Flag632prime-))
(not (Flag631prime-))
(not (Flag630prime-))
(not (Flag629prime-))
(not (Flag628prime-))
(not (Flag627prime-))
(not (Flag626prime-))
(not (Flag87prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag93prime-))
(not (Flag91prime-))
(not (Flag89prime-))
(not (Flag69prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag92prime-))
(not (Flag85prime-))
(not (Flag84prime-))
(not (Flag97prime-))
(not (Flag95prime-))
(not (Flag94prime-))
(not (Flag82prime-))
(not (Flag98prime-))
(not (Flag96prime-))
(not (Flag83prime-))
(not (Flag81prime-))
(not (Flag76prime-))
(not (Flag74prime-))
(not (Flag37prime-))
)
)
(:action STOP-PASS_AU-AO
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AO)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag637prime-))
(not (Flag636prime-))
(not (Flag635prime-))
(not (Flag634prime-))
(not (Flag633prime-))
(not (Flag632prime-))
(not (Flag631prime-))
(not (Flag630prime-))
(not (Flag629prime-))
(not (Flag628prime-))
(not (Flag627prime-))
(not (Flag626prime-))
(not (Flag87prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag93prime-))
(not (Flag91prime-))
(not (Flag89prime-))
(not (Flag69prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag92prime-))
(not (Flag85prime-))
(not (Flag84prime-))
(not (Flag97prime-))
(not (Flag95prime-))
(not (Flag94prime-))
(not (Flag82prime-))
(not (Flag98prime-))
(not (Flag96prime-))
(not (Flag83prime-))
(not (Flag81prime-))
(not (Flag76prime-))
(not (Flag74prime-))
(not (Flag37prime-))
)
)
(:action STOP-PASS_AU-AM
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AM)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag637prime-))
(not (Flag636prime-))
(not (Flag635prime-))
(not (Flag634prime-))
(not (Flag633prime-))
(not (Flag632prime-))
(not (Flag631prime-))
(not (Flag630prime-))
(not (Flag629prime-))
(not (Flag628prime-))
(not (Flag627prime-))
(not (Flag626prime-))
(not (Flag87prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag93prime-))
(not (Flag91prime-))
(not (Flag89prime-))
(not (Flag69prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag92prime-))
(not (Flag85prime-))
(not (Flag84prime-))
(not (Flag97prime-))
(not (Flag95prime-))
(not (Flag94prime-))
(not (Flag82prime-))
(not (Flag98prime-))
(not (Flag96prime-))
(not (Flag83prime-))
(not (Flag81prime-))
(not (Flag76prime-))
(not (Flag74prime-))
(not (Flag37prime-))
)
)
(:action STOP-PASS_AU-AK
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AK)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag637prime-))
(not (Flag636prime-))
(not (Flag635prime-))
(not (Flag634prime-))
(not (Flag633prime-))
(not (Flag632prime-))
(not (Flag631prime-))
(not (Flag630prime-))
(not (Flag629prime-))
(not (Flag628prime-))
(not (Flag627prime-))
(not (Flag626prime-))
(not (Flag87prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag93prime-))
(not (Flag91prime-))
(not (Flag89prime-))
(not (Flag69prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag92prime-))
(not (Flag85prime-))
(not (Flag84prime-))
(not (Flag97prime-))
(not (Flag95prime-))
(not (Flag94prime-))
(not (Flag82prime-))
(not (Flag98prime-))
(not (Flag96prime-))
(not (Flag83prime-))
(not (Flag81prime-))
(not (Flag76prime-))
(not (Flag74prime-))
(not (Flag37prime-))
)
)
(:action STOP-PASS_AX-AQ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AQ)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag637prime-))
(not (Flag636prime-))
(not (Flag635prime-))
(not (Flag634prime-))
(not (Flag633prime-))
(not (Flag632prime-))
(not (Flag631prime-))
(not (Flag630prime-))
(not (Flag629prime-))
(not (Flag628prime-))
(not (Flag627prime-))
(not (Flag626prime-))
(not (Flag87prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag93prime-))
(not (Flag91prime-))
(not (Flag89prime-))
(not (Flag69prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag92prime-))
(not (Flag85prime-))
(not (Flag84prime-))
(not (Flag97prime-))
(not (Flag95prime-))
(not (Flag94prime-))
(not (Flag82prime-))
(not (Flag98prime-))
(not (Flag96prime-))
(not (Flag83prime-))
(not (Flag81prime-))
(not (Flag76prime-))
(not (Flag74prime-))
(not (Flag37prime-))
)
)
(:action STOP-PASS_AX-AP
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AP)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag637prime-))
(not (Flag636prime-))
(not (Flag635prime-))
(not (Flag634prime-))
(not (Flag633prime-))
(not (Flag632prime-))
(not (Flag631prime-))
(not (Flag630prime-))
(not (Flag629prime-))
(not (Flag628prime-))
(not (Flag627prime-))
(not (Flag626prime-))
(not (Flag87prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag93prime-))
(not (Flag91prime-))
(not (Flag89prime-))
(not (Flag69prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag92prime-))
(not (Flag85prime-))
(not (Flag84prime-))
(not (Flag97prime-))
(not (Flag95prime-))
(not (Flag94prime-))
(not (Flag82prime-))
(not (Flag98prime-))
(not (Flag96prime-))
(not (Flag83prime-))
(not (Flag81prime-))
(not (Flag76prime-))
(not (Flag74prime-))
(not (Flag37prime-))
)
)
(:action STOP-PASS_AX-AO
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AO)
)
:effect
(and
(when
(and
(BOARDED-PASS_AX)
)
(and
(SERVED-PASS_AX)
(NOT-BOARDED-PASS_AX)
(not (NOT-SERVED-PASS_AX))
(not (BOARDED-PASS_AX))
)
)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag637prime-))
(not (Flag636prime-))
(not (Flag635prime-))
(not (Flag634prime-))
(not (Flag633prime-))
(not (Flag632prime-))
(not (Flag631prime-))
(not (Flag630prime-))
(not (Flag629prime-))
(not (Flag628prime-))
(not (Flag627prime-))
(not (Flag626prime-))
(not (Flag87prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag93prime-))
(not (Flag91prime-))
(not (Flag89prime-))
(not (Flag69prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag92prime-))
(not (Flag85prime-))
(not (Flag84prime-))
(not (Flag97prime-))
(not (Flag95prime-))
(not (Flag94prime-))
(not (Flag82prime-))
(not (Flag98prime-))
(not (Flag96prime-))
(not (Flag83prime-))
(not (Flag81prime-))
(not (Flag76prime-))
(not (Flag74prime-))
(not (Flag37prime-))
)
)
(:action STOP-PASS_AX-AM
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AM)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag637prime-))
(not (Flag636prime-))
(not (Flag635prime-))
(not (Flag634prime-))
(not (Flag633prime-))
(not (Flag632prime-))
(not (Flag631prime-))
(not (Flag630prime-))
(not (Flag629prime-))
(not (Flag628prime-))
(not (Flag627prime-))
(not (Flag626prime-))
(not (Flag87prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag93prime-))
(not (Flag91prime-))
(not (Flag89prime-))
(not (Flag69prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag92prime-))
(not (Flag85prime-))
(not (Flag84prime-))
(not (Flag97prime-))
(not (Flag95prime-))
(not (Flag94prime-))
(not (Flag82prime-))
(not (Flag98prime-))
(not (Flag96prime-))
(not (Flag83prime-))
(not (Flag81prime-))
(not (Flag76prime-))
(not (Flag74prime-))
(not (Flag37prime-))
)
)
(:action STOP-PASS_AX-AK
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AK)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag637prime-))
(not (Flag636prime-))
(not (Flag635prime-))
(not (Flag634prime-))
(not (Flag633prime-))
(not (Flag632prime-))
(not (Flag631prime-))
(not (Flag630prime-))
(not (Flag629prime-))
(not (Flag628prime-))
(not (Flag627prime-))
(not (Flag626prime-))
(not (Flag87prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag93prime-))
(not (Flag91prime-))
(not (Flag89prime-))
(not (Flag69prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag92prime-))
(not (Flag85prime-))
(not (Flag84prime-))
(not (Flag97prime-))
(not (Flag95prime-))
(not (Flag94prime-))
(not (Flag82prime-))
(not (Flag98prime-))
(not (Flag96prime-))
(not (Flag83prime-))
(not (Flag81prime-))
(not (Flag76prime-))
(not (Flag74prime-))
(not (Flag37prime-))
)
)
(:action STOP-PASS_AA-AQ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AQ)
(Flag632prime-)
)
:effect
(and
(when
(and
(Flag632-)
)
(and
(BOARDED-PASS_AA)
(not (NOT-BOARDED-PASS_AA))
)
)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag637prime-))
(not (Flag636prime-))
(not (Flag635prime-))
(not (Flag634prime-))
(not (Flag633prime-))
(not (Flag632prime-))
(not (Flag631prime-))
(not (Flag630prime-))
(not (Flag629prime-))
(not (Flag628prime-))
(not (Flag627prime-))
(not (Flag626prime-))
(not (Flag87prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag93prime-))
(not (Flag91prime-))
(not (Flag89prime-))
(not (Flag69prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag92prime-))
(not (Flag85prime-))
(not (Flag84prime-))
(not (Flag97prime-))
(not (Flag95prime-))
(not (Flag94prime-))
(not (Flag82prime-))
(not (Flag98prime-))
(not (Flag96prime-))
(not (Flag83prime-))
(not (Flag81prime-))
(not (Flag76prime-))
(not (Flag74prime-))
(not (Flag37prime-))
)
)
(:action STOP-PASS_AA-AP
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AP)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag637prime-))
(not (Flag636prime-))
(not (Flag635prime-))
(not (Flag634prime-))
(not (Flag633prime-))
(not (Flag632prime-))
(not (Flag631prime-))
(not (Flag630prime-))
(not (Flag629prime-))
(not (Flag628prime-))
(not (Flag627prime-))
(not (Flag626prime-))
(not (Flag87prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag93prime-))
(not (Flag91prime-))
(not (Flag89prime-))
(not (Flag69prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag92prime-))
(not (Flag85prime-))
(not (Flag84prime-))
(not (Flag97prime-))
(not (Flag95prime-))
(not (Flag94prime-))
(not (Flag82prime-))
(not (Flag98prime-))
(not (Flag96prime-))
(not (Flag83prime-))
(not (Flag81prime-))
(not (Flag76prime-))
(not (Flag74prime-))
(not (Flag37prime-))
)
)
(:action STOP-PASS_AA-AO
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AO)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag637prime-))
(not (Flag636prime-))
(not (Flag635prime-))
(not (Flag634prime-))
(not (Flag633prime-))
(not (Flag632prime-))
(not (Flag631prime-))
(not (Flag630prime-))
(not (Flag629prime-))
(not (Flag628prime-))
(not (Flag627prime-))
(not (Flag626prime-))
(not (Flag87prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag93prime-))
(not (Flag91prime-))
(not (Flag89prime-))
(not (Flag69prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag92prime-))
(not (Flag85prime-))
(not (Flag84prime-))
(not (Flag97prime-))
(not (Flag95prime-))
(not (Flag94prime-))
(not (Flag82prime-))
(not (Flag98prime-))
(not (Flag96prime-))
(not (Flag83prime-))
(not (Flag81prime-))
(not (Flag76prime-))
(not (Flag74prime-))
(not (Flag37prime-))
)
)
(:action STOP-PASS_AA-AM
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AM)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag637prime-))
(not (Flag636prime-))
(not (Flag635prime-))
(not (Flag634prime-))
(not (Flag633prime-))
(not (Flag632prime-))
(not (Flag631prime-))
(not (Flag630prime-))
(not (Flag629prime-))
(not (Flag628prime-))
(not (Flag627prime-))
(not (Flag626prime-))
(not (Flag87prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag93prime-))
(not (Flag91prime-))
(not (Flag89prime-))
(not (Flag69prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag92prime-))
(not (Flag85prime-))
(not (Flag84prime-))
(not (Flag97prime-))
(not (Flag95prime-))
(not (Flag94prime-))
(not (Flag82prime-))
(not (Flag98prime-))
(not (Flag96prime-))
(not (Flag83prime-))
(not (Flag81prime-))
(not (Flag76prime-))
(not (Flag74prime-))
(not (Flag37prime-))
)
)
(:action STOP-PASS_AA-AK
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AK)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag637prime-))
(not (Flag636prime-))
(not (Flag635prime-))
(not (Flag634prime-))
(not (Flag633prime-))
(not (Flag632prime-))
(not (Flag631prime-))
(not (Flag630prime-))
(not (Flag629prime-))
(not (Flag628prime-))
(not (Flag627prime-))
(not (Flag626prime-))
(not (Flag87prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag93prime-))
(not (Flag91prime-))
(not (Flag89prime-))
(not (Flag69prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag92prime-))
(not (Flag85prime-))
(not (Flag84prime-))
(not (Flag97prime-))
(not (Flag95prime-))
(not (Flag94prime-))
(not (Flag82prime-))
(not (Flag98prime-))
(not (Flag96prime-))
(not (Flag83prime-))
(not (Flag81prime-))
(not (Flag76prime-))
(not (Flag74prime-))
(not (Flag37prime-))
)
)
(:action STOP-PASS_AB-AQ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AQ)
)
:effect
(and
(when
(and
(BOARDED-PASS_AB)
)
(and
(SERVED-PASS_AB)
(NOT-BOARDED-PASS_AB)
(not (NOT-SERVED-PASS_AB))
(not (BOARDED-PASS_AB))
)
)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag637prime-))
(not (Flag636prime-))
(not (Flag635prime-))
(not (Flag634prime-))
(not (Flag633prime-))
(not (Flag632prime-))
(not (Flag631prime-))
(not (Flag630prime-))
(not (Flag629prime-))
(not (Flag628prime-))
(not (Flag627prime-))
(not (Flag626prime-))
(not (Flag87prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag93prime-))
(not (Flag91prime-))
(not (Flag89prime-))
(not (Flag69prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag92prime-))
(not (Flag85prime-))
(not (Flag84prime-))
(not (Flag97prime-))
(not (Flag95prime-))
(not (Flag94prime-))
(not (Flag82prime-))
(not (Flag98prime-))
(not (Flag96prime-))
(not (Flag83prime-))
(not (Flag81prime-))
(not (Flag76prime-))
(not (Flag74prime-))
(not (Flag37prime-))
)
)
(:action STOP-PASS_AB-AP
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AP)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag637prime-))
(not (Flag636prime-))
(not (Flag635prime-))
(not (Flag634prime-))
(not (Flag633prime-))
(not (Flag632prime-))
(not (Flag631prime-))
(not (Flag630prime-))
(not (Flag629prime-))
(not (Flag628prime-))
(not (Flag627prime-))
(not (Flag626prime-))
(not (Flag87prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag93prime-))
(not (Flag91prime-))
(not (Flag89prime-))
(not (Flag69prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag92prime-))
(not (Flag85prime-))
(not (Flag84prime-))
(not (Flag97prime-))
(not (Flag95prime-))
(not (Flag94prime-))
(not (Flag82prime-))
(not (Flag98prime-))
(not (Flag96prime-))
(not (Flag83prime-))
(not (Flag81prime-))
(not (Flag76prime-))
(not (Flag74prime-))
(not (Flag37prime-))
)
)
(:action STOP-PASS_AB-AO
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AO)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag637prime-))
(not (Flag636prime-))
(not (Flag635prime-))
(not (Flag634prime-))
(not (Flag633prime-))
(not (Flag632prime-))
(not (Flag631prime-))
(not (Flag630prime-))
(not (Flag629prime-))
(not (Flag628prime-))
(not (Flag627prime-))
(not (Flag626prime-))
(not (Flag87prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag93prime-))
(not (Flag91prime-))
(not (Flag89prime-))
(not (Flag69prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag92prime-))
(not (Flag85prime-))
(not (Flag84prime-))
(not (Flag97prime-))
(not (Flag95prime-))
(not (Flag94prime-))
(not (Flag82prime-))
(not (Flag98prime-))
(not (Flag96prime-))
(not (Flag83prime-))
(not (Flag81prime-))
(not (Flag76prime-))
(not (Flag74prime-))
(not (Flag37prime-))
)
)
(:action STOP-PASS_AB-AM
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AM)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag637prime-))
(not (Flag636prime-))
(not (Flag635prime-))
(not (Flag634prime-))
(not (Flag633prime-))
(not (Flag632prime-))
(not (Flag631prime-))
(not (Flag630prime-))
(not (Flag629prime-))
(not (Flag628prime-))
(not (Flag627prime-))
(not (Flag626prime-))
(not (Flag87prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag93prime-))
(not (Flag91prime-))
(not (Flag89prime-))
(not (Flag69prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag92prime-))
(not (Flag85prime-))
(not (Flag84prime-))
(not (Flag97prime-))
(not (Flag95prime-))
(not (Flag94prime-))
(not (Flag82prime-))
(not (Flag98prime-))
(not (Flag96prime-))
(not (Flag83prime-))
(not (Flag81prime-))
(not (Flag76prime-))
(not (Flag74prime-))
(not (Flag37prime-))
)
)
(:action STOP-PASS_AB-AK
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AK)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag637prime-))
(not (Flag636prime-))
(not (Flag635prime-))
(not (Flag634prime-))
(not (Flag633prime-))
(not (Flag632prime-))
(not (Flag631prime-))
(not (Flag630prime-))
(not (Flag629prime-))
(not (Flag628prime-))
(not (Flag627prime-))
(not (Flag626prime-))
(not (Flag87prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag93prime-))
(not (Flag91prime-))
(not (Flag89prime-))
(not (Flag69prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag92prime-))
(not (Flag85prime-))
(not (Flag84prime-))
(not (Flag97prime-))
(not (Flag95prime-))
(not (Flag94prime-))
(not (Flag82prime-))
(not (Flag98prime-))
(not (Flag96prime-))
(not (Flag83prime-))
(not (Flag81prime-))
(not (Flag76prime-))
(not (Flag74prime-))
(not (Flag37prime-))
)
)
(:action STOP-PASS_AE-AQ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AQ)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag637prime-))
(not (Flag636prime-))
(not (Flag635prime-))
(not (Flag634prime-))
(not (Flag633prime-))
(not (Flag632prime-))
(not (Flag631prime-))
(not (Flag630prime-))
(not (Flag629prime-))
(not (Flag628prime-))
(not (Flag627prime-))
(not (Flag626prime-))
(not (Flag87prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag93prime-))
(not (Flag91prime-))
(not (Flag89prime-))
(not (Flag69prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag92prime-))
(not (Flag85prime-))
(not (Flag84prime-))
(not (Flag97prime-))
(not (Flag95prime-))
(not (Flag94prime-))
(not (Flag82prime-))
(not (Flag98prime-))
(not (Flag96prime-))
(not (Flag83prime-))
(not (Flag81prime-))
(not (Flag76prime-))
(not (Flag74prime-))
(not (Flag37prime-))
)
)
(:action STOP-PASS_AE-AP
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AP)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag637prime-))
(not (Flag636prime-))
(not (Flag635prime-))
(not (Flag634prime-))
(not (Flag633prime-))
(not (Flag632prime-))
(not (Flag631prime-))
(not (Flag630prime-))
(not (Flag629prime-))
(not (Flag628prime-))
(not (Flag627prime-))
(not (Flag626prime-))
(not (Flag87prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag93prime-))
(not (Flag91prime-))
(not (Flag89prime-))
(not (Flag69prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag92prime-))
(not (Flag85prime-))
(not (Flag84prime-))
(not (Flag97prime-))
(not (Flag95prime-))
(not (Flag94prime-))
(not (Flag82prime-))
(not (Flag98prime-))
(not (Flag96prime-))
(not (Flag83prime-))
(not (Flag81prime-))
(not (Flag76prime-))
(not (Flag74prime-))
(not (Flag37prime-))
)
)
(:action STOP-PASS_AE-AO
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AO)
)
:effect
(and
(when
(and
(BOARDED-PASS_AE)
)
(and
(SERVED-PASS_AE)
(NOT-BOARDED-PASS_AE)
(not (NOT-SERVED-PASS_AE))
(not (BOARDED-PASS_AE))
)
)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag637prime-))
(not (Flag636prime-))
(not (Flag635prime-))
(not (Flag634prime-))
(not (Flag633prime-))
(not (Flag632prime-))
(not (Flag631prime-))
(not (Flag630prime-))
(not (Flag629prime-))
(not (Flag628prime-))
(not (Flag627prime-))
(not (Flag626prime-))
(not (Flag87prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag93prime-))
(not (Flag91prime-))
(not (Flag89prime-))
(not (Flag69prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag92prime-))
(not (Flag85prime-))
(not (Flag84prime-))
(not (Flag97prime-))
(not (Flag95prime-))
(not (Flag94prime-))
(not (Flag82prime-))
(not (Flag98prime-))
(not (Flag96prime-))
(not (Flag83prime-))
(not (Flag81prime-))
(not (Flag76prime-))
(not (Flag74prime-))
(not (Flag37prime-))
)
)
(:action STOP-PASS_AE-AM
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AM)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag637prime-))
(not (Flag636prime-))
(not (Flag635prime-))
(not (Flag634prime-))
(not (Flag633prime-))
(not (Flag632prime-))
(not (Flag631prime-))
(not (Flag630prime-))
(not (Flag629prime-))
(not (Flag628prime-))
(not (Flag627prime-))
(not (Flag626prime-))
(not (Flag87prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag93prime-))
(not (Flag91prime-))
(not (Flag89prime-))
(not (Flag69prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag92prime-))
(not (Flag85prime-))
(not (Flag84prime-))
(not (Flag97prime-))
(not (Flag95prime-))
(not (Flag94prime-))
(not (Flag82prime-))
(not (Flag98prime-))
(not (Flag96prime-))
(not (Flag83prime-))
(not (Flag81prime-))
(not (Flag76prime-))
(not (Flag74prime-))
(not (Flag37prime-))
)
)
(:action STOP-PASS_AE-AK
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AK)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag637prime-))
(not (Flag636prime-))
(not (Flag635prime-))
(not (Flag634prime-))
(not (Flag633prime-))
(not (Flag632prime-))
(not (Flag631prime-))
(not (Flag630prime-))
(not (Flag629prime-))
(not (Flag628prime-))
(not (Flag627prime-))
(not (Flag626prime-))
(not (Flag87prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag93prime-))
(not (Flag91prime-))
(not (Flag89prime-))
(not (Flag69prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag92prime-))
(not (Flag85prime-))
(not (Flag84prime-))
(not (Flag97prime-))
(not (Flag95prime-))
(not (Flag94prime-))
(not (Flag82prime-))
(not (Flag98prime-))
(not (Flag96prime-))
(not (Flag83prime-))
(not (Flag81prime-))
(not (Flag76prime-))
(not (Flag74prime-))
(not (Flag37prime-))
)
)
(:action STOP-PASS_AD-AQ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AQ)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag637prime-))
(not (Flag636prime-))
(not (Flag635prime-))
(not (Flag634prime-))
(not (Flag633prime-))
(not (Flag632prime-))
(not (Flag631prime-))
(not (Flag630prime-))
(not (Flag629prime-))
(not (Flag628prime-))
(not (Flag627prime-))
(not (Flag626prime-))
(not (Flag87prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag93prime-))
(not (Flag91prime-))
(not (Flag89prime-))
(not (Flag69prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag92prime-))
(not (Flag85prime-))
(not (Flag84prime-))
(not (Flag97prime-))
(not (Flag95prime-))
(not (Flag94prime-))
(not (Flag82prime-))
(not (Flag98prime-))
(not (Flag96prime-))
(not (Flag83prime-))
(not (Flag81prime-))
(not (Flag76prime-))
(not (Flag74prime-))
(not (Flag37prime-))
)
)
(:action STOP-PASS_AD-AP
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AP)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag637prime-))
(not (Flag636prime-))
(not (Flag635prime-))
(not (Flag634prime-))
(not (Flag633prime-))
(not (Flag632prime-))
(not (Flag631prime-))
(not (Flag630prime-))
(not (Flag629prime-))
(not (Flag628prime-))
(not (Flag627prime-))
(not (Flag626prime-))
(not (Flag87prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag93prime-))
(not (Flag91prime-))
(not (Flag89prime-))
(not (Flag69prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag92prime-))
(not (Flag85prime-))
(not (Flag84prime-))
(not (Flag97prime-))
(not (Flag95prime-))
(not (Flag94prime-))
(not (Flag82prime-))
(not (Flag98prime-))
(not (Flag96prime-))
(not (Flag83prime-))
(not (Flag81prime-))
(not (Flag76prime-))
(not (Flag74prime-))
(not (Flag37prime-))
)
)
(:action STOP-PASS_AD-AO
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AO)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag637prime-))
(not (Flag636prime-))
(not (Flag635prime-))
(not (Flag634prime-))
(not (Flag633prime-))
(not (Flag632prime-))
(not (Flag631prime-))
(not (Flag630prime-))
(not (Flag629prime-))
(not (Flag628prime-))
(not (Flag627prime-))
(not (Flag626prime-))
(not (Flag87prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag93prime-))
(not (Flag91prime-))
(not (Flag89prime-))
(not (Flag69prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag92prime-))
(not (Flag85prime-))
(not (Flag84prime-))
(not (Flag97prime-))
(not (Flag95prime-))
(not (Flag94prime-))
(not (Flag82prime-))
(not (Flag98prime-))
(not (Flag96prime-))
(not (Flag83prime-))
(not (Flag81prime-))
(not (Flag76prime-))
(not (Flag74prime-))
(not (Flag37prime-))
)
)
(:action STOP-PASS_AD-AM
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AM)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag637prime-))
(not (Flag636prime-))
(not (Flag635prime-))
(not (Flag634prime-))
(not (Flag633prime-))
(not (Flag632prime-))
(not (Flag631prime-))
(not (Flag630prime-))
(not (Flag629prime-))
(not (Flag628prime-))
(not (Flag627prime-))
(not (Flag626prime-))
(not (Flag87prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag93prime-))
(not (Flag91prime-))
(not (Flag89prime-))
(not (Flag69prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag92prime-))
(not (Flag85prime-))
(not (Flag84prime-))
(not (Flag97prime-))
(not (Flag95prime-))
(not (Flag94prime-))
(not (Flag82prime-))
(not (Flag98prime-))
(not (Flag96prime-))
(not (Flag83prime-))
(not (Flag81prime-))
(not (Flag76prime-))
(not (Flag74prime-))
(not (Flag37prime-))
)
)
(:action STOP-PASS_AD-AK
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AK)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag637prime-))
(not (Flag636prime-))
(not (Flag635prime-))
(not (Flag634prime-))
(not (Flag633prime-))
(not (Flag632prime-))
(not (Flag631prime-))
(not (Flag630prime-))
(not (Flag629prime-))
(not (Flag628prime-))
(not (Flag627prime-))
(not (Flag626prime-))
(not (Flag87prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag93prime-))
(not (Flag91prime-))
(not (Flag89prime-))
(not (Flag69prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag92prime-))
(not (Flag85prime-))
(not (Flag84prime-))
(not (Flag97prime-))
(not (Flag95prime-))
(not (Flag94prime-))
(not (Flag82prime-))
(not (Flag98prime-))
(not (Flag96prime-))
(not (Flag83prime-))
(not (Flag81prime-))
(not (Flag76prime-))
(not (Flag74prime-))
(not (Flag37prime-))
)
)
(:action STOP-PASS_AL-AQ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AQ)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag637prime-))
(not (Flag636prime-))
(not (Flag635prime-))
(not (Flag634prime-))
(not (Flag633prime-))
(not (Flag632prime-))
(not (Flag631prime-))
(not (Flag630prime-))
(not (Flag629prime-))
(not (Flag628prime-))
(not (Flag627prime-))
(not (Flag626prime-))
(not (Flag87prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag93prime-))
(not (Flag91prime-))
(not (Flag89prime-))
(not (Flag69prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag92prime-))
(not (Flag85prime-))
(not (Flag84prime-))
(not (Flag97prime-))
(not (Flag95prime-))
(not (Flag94prime-))
(not (Flag82prime-))
(not (Flag98prime-))
(not (Flag96prime-))
(not (Flag83prime-))
(not (Flag81prime-))
(not (Flag76prime-))
(not (Flag74prime-))
(not (Flag37prime-))
)
)
(:action STOP-PASS_AL-AP
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AP)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag637prime-))
(not (Flag636prime-))
(not (Flag635prime-))
(not (Flag634prime-))
(not (Flag633prime-))
(not (Flag632prime-))
(not (Flag631prime-))
(not (Flag630prime-))
(not (Flag629prime-))
(not (Flag628prime-))
(not (Flag627prime-))
(not (Flag626prime-))
(not (Flag87prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag93prime-))
(not (Flag91prime-))
(not (Flag89prime-))
(not (Flag69prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag92prime-))
(not (Flag85prime-))
(not (Flag84prime-))
(not (Flag97prime-))
(not (Flag95prime-))
(not (Flag94prime-))
(not (Flag82prime-))
(not (Flag98prime-))
(not (Flag96prime-))
(not (Flag83prime-))
(not (Flag81prime-))
(not (Flag76prime-))
(not (Flag74prime-))
(not (Flag37prime-))
)
)
(:action STOP-PASS_AL-AO
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AO)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag637prime-))
(not (Flag636prime-))
(not (Flag635prime-))
(not (Flag634prime-))
(not (Flag633prime-))
(not (Flag632prime-))
(not (Flag631prime-))
(not (Flag630prime-))
(not (Flag629prime-))
(not (Flag628prime-))
(not (Flag627prime-))
(not (Flag626prime-))
(not (Flag87prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag93prime-))
(not (Flag91prime-))
(not (Flag89prime-))
(not (Flag69prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag92prime-))
(not (Flag85prime-))
(not (Flag84prime-))
(not (Flag97prime-))
(not (Flag95prime-))
(not (Flag94prime-))
(not (Flag82prime-))
(not (Flag98prime-))
(not (Flag96prime-))
(not (Flag83prime-))
(not (Flag81prime-))
(not (Flag76prime-))
(not (Flag74prime-))
(not (Flag37prime-))
)
)
(:action STOP-PASS_AL-AM
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AM)
)
:effect
(and
(when
(and
(BOARDED-PASS_AL)
)
(and
(SERVED-PASS_AL)
(NOT-BOARDED-PASS_AL)
(not (NOT-SERVED-PASS_AL))
(not (BOARDED-PASS_AL))
)
)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag637prime-))
(not (Flag636prime-))
(not (Flag635prime-))
(not (Flag634prime-))
(not (Flag633prime-))
(not (Flag632prime-))
(not (Flag631prime-))
(not (Flag630prime-))
(not (Flag629prime-))
(not (Flag628prime-))
(not (Flag627prime-))
(not (Flag626prime-))
(not (Flag87prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag93prime-))
(not (Flag91prime-))
(not (Flag89prime-))
(not (Flag69prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag92prime-))
(not (Flag85prime-))
(not (Flag84prime-))
(not (Flag97prime-))
(not (Flag95prime-))
(not (Flag94prime-))
(not (Flag82prime-))
(not (Flag98prime-))
(not (Flag96prime-))
(not (Flag83prime-))
(not (Flag81prime-))
(not (Flag76prime-))
(not (Flag74prime-))
(not (Flag37prime-))
)
)
(:action STOP-PASS_AL-AK
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AK)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag637prime-))
(not (Flag636prime-))
(not (Flag635prime-))
(not (Flag634prime-))
(not (Flag633prime-))
(not (Flag632prime-))
(not (Flag631prime-))
(not (Flag630prime-))
(not (Flag629prime-))
(not (Flag628prime-))
(not (Flag627prime-))
(not (Flag626prime-))
(not (Flag87prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag93prime-))
(not (Flag91prime-))
(not (Flag89prime-))
(not (Flag69prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag92prime-))
(not (Flag85prime-))
(not (Flag84prime-))
(not (Flag97prime-))
(not (Flag95prime-))
(not (Flag94prime-))
(not (Flag82prime-))
(not (Flag98prime-))
(not (Flag96prime-))
(not (Flag83prime-))
(not (Flag81prime-))
(not (Flag76prime-))
(not (Flag74prime-))
(not (Flag37prime-))
)
)
(:action STOP-PASS_AN-AQ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AQ)
)
:effect
(and
(when
(and
(BOARDED-PASS_AN)
)
(and
(SERVED-PASS_AN)
(NOT-BOARDED-PASS_AN)
(not (NOT-SERVED-PASS_AN))
(not (BOARDED-PASS_AN))
)
)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag637prime-))
(not (Flag636prime-))
(not (Flag635prime-))
(not (Flag634prime-))
(not (Flag633prime-))
(not (Flag632prime-))
(not (Flag631prime-))
(not (Flag630prime-))
(not (Flag629prime-))
(not (Flag628prime-))
(not (Flag627prime-))
(not (Flag626prime-))
(not (Flag87prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag93prime-))
(not (Flag91prime-))
(not (Flag89prime-))
(not (Flag69prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag92prime-))
(not (Flag85prime-))
(not (Flag84prime-))
(not (Flag97prime-))
(not (Flag95prime-))
(not (Flag94prime-))
(not (Flag82prime-))
(not (Flag98prime-))
(not (Flag96prime-))
(not (Flag83prime-))
(not (Flag81prime-))
(not (Flag76prime-))
(not (Flag74prime-))
(not (Flag37prime-))
)
)
(:action STOP-PASS_AN-AP
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AP)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag637prime-))
(not (Flag636prime-))
(not (Flag635prime-))
(not (Flag634prime-))
(not (Flag633prime-))
(not (Flag632prime-))
(not (Flag631prime-))
(not (Flag630prime-))
(not (Flag629prime-))
(not (Flag628prime-))
(not (Flag627prime-))
(not (Flag626prime-))
(not (Flag87prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag93prime-))
(not (Flag91prime-))
(not (Flag89prime-))
(not (Flag69prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag92prime-))
(not (Flag85prime-))
(not (Flag84prime-))
(not (Flag97prime-))
(not (Flag95prime-))
(not (Flag94prime-))
(not (Flag82prime-))
(not (Flag98prime-))
(not (Flag96prime-))
(not (Flag83prime-))
(not (Flag81prime-))
(not (Flag76prime-))
(not (Flag74prime-))
(not (Flag37prime-))
)
)
(:action STOP-PASS_AN-AO
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AO)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag637prime-))
(not (Flag636prime-))
(not (Flag635prime-))
(not (Flag634prime-))
(not (Flag633prime-))
(not (Flag632prime-))
(not (Flag631prime-))
(not (Flag630prime-))
(not (Flag629prime-))
(not (Flag628prime-))
(not (Flag627prime-))
(not (Flag626prime-))
(not (Flag87prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag93prime-))
(not (Flag91prime-))
(not (Flag89prime-))
(not (Flag69prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag92prime-))
(not (Flag85prime-))
(not (Flag84prime-))
(not (Flag97prime-))
(not (Flag95prime-))
(not (Flag94prime-))
(not (Flag82prime-))
(not (Flag98prime-))
(not (Flag96prime-))
(not (Flag83prime-))
(not (Flag81prime-))
(not (Flag76prime-))
(not (Flag74prime-))
(not (Flag37prime-))
)
)
(:action STOP-PASS_AN-AM
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AM)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag637prime-))
(not (Flag636prime-))
(not (Flag635prime-))
(not (Flag634prime-))
(not (Flag633prime-))
(not (Flag632prime-))
(not (Flag631prime-))
(not (Flag630prime-))
(not (Flag629prime-))
(not (Flag628prime-))
(not (Flag627prime-))
(not (Flag626prime-))
(not (Flag87prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag93prime-))
(not (Flag91prime-))
(not (Flag89prime-))
(not (Flag69prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag92prime-))
(not (Flag85prime-))
(not (Flag84prime-))
(not (Flag97prime-))
(not (Flag95prime-))
(not (Flag94prime-))
(not (Flag82prime-))
(not (Flag98prime-))
(not (Flag96prime-))
(not (Flag83prime-))
(not (Flag81prime-))
(not (Flag76prime-))
(not (Flag74prime-))
(not (Flag37prime-))
)
)
(:action STOP-PASS_AN-AK
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AK)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag637prime-))
(not (Flag636prime-))
(not (Flag635prime-))
(not (Flag634prime-))
(not (Flag633prime-))
(not (Flag632prime-))
(not (Flag631prime-))
(not (Flag630prime-))
(not (Flag629prime-))
(not (Flag628prime-))
(not (Flag627prime-))
(not (Flag626prime-))
(not (Flag87prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag93prime-))
(not (Flag91prime-))
(not (Flag89prime-))
(not (Flag69prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag92prime-))
(not (Flag85prime-))
(not (Flag84prime-))
(not (Flag97prime-))
(not (Flag95prime-))
(not (Flag94prime-))
(not (Flag82prime-))
(not (Flag98prime-))
(not (Flag96prime-))
(not (Flag83prime-))
(not (Flag81prime-))
(not (Flag76prime-))
(not (Flag74prime-))
(not (Flag37prime-))
)
)
(:action STOP-PASS_AS-AQ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AQ)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag637prime-))
(not (Flag636prime-))
(not (Flag635prime-))
(not (Flag634prime-))
(not (Flag633prime-))
(not (Flag632prime-))
(not (Flag631prime-))
(not (Flag630prime-))
(not (Flag629prime-))
(not (Flag628prime-))
(not (Flag627prime-))
(not (Flag626prime-))
(not (Flag87prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag93prime-))
(not (Flag91prime-))
(not (Flag89prime-))
(not (Flag69prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag92prime-))
(not (Flag85prime-))
(not (Flag84prime-))
(not (Flag97prime-))
(not (Flag95prime-))
(not (Flag94prime-))
(not (Flag82prime-))
(not (Flag98prime-))
(not (Flag96prime-))
(not (Flag83prime-))
(not (Flag81prime-))
(not (Flag76prime-))
(not (Flag74prime-))
(not (Flag37prime-))
)
)
(:action STOP-PASS_AS-AP
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AP)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag637prime-))
(not (Flag636prime-))
(not (Flag635prime-))
(not (Flag634prime-))
(not (Flag633prime-))
(not (Flag632prime-))
(not (Flag631prime-))
(not (Flag630prime-))
(not (Flag629prime-))
(not (Flag628prime-))
(not (Flag627prime-))
(not (Flag626prime-))
(not (Flag87prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag93prime-))
(not (Flag91prime-))
(not (Flag89prime-))
(not (Flag69prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag92prime-))
(not (Flag85prime-))
(not (Flag84prime-))
(not (Flag97prime-))
(not (Flag95prime-))
(not (Flag94prime-))
(not (Flag82prime-))
(not (Flag98prime-))
(not (Flag96prime-))
(not (Flag83prime-))
(not (Flag81prime-))
(not (Flag76prime-))
(not (Flag74prime-))
(not (Flag37prime-))
)
)
(:action STOP-PASS_AS-AO
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AO)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag637prime-))
(not (Flag636prime-))
(not (Flag635prime-))
(not (Flag634prime-))
(not (Flag633prime-))
(not (Flag632prime-))
(not (Flag631prime-))
(not (Flag630prime-))
(not (Flag629prime-))
(not (Flag628prime-))
(not (Flag627prime-))
(not (Flag626prime-))
(not (Flag87prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag93prime-))
(not (Flag91prime-))
(not (Flag89prime-))
(not (Flag69prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag92prime-))
(not (Flag85prime-))
(not (Flag84prime-))
(not (Flag97prime-))
(not (Flag95prime-))
(not (Flag94prime-))
(not (Flag82prime-))
(not (Flag98prime-))
(not (Flag96prime-))
(not (Flag83prime-))
(not (Flag81prime-))
(not (Flag76prime-))
(not (Flag74prime-))
(not (Flag37prime-))
)
)
(:action STOP-PASS_AS-AM
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AM)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag637prime-))
(not (Flag636prime-))
(not (Flag635prime-))
(not (Flag634prime-))
(not (Flag633prime-))
(not (Flag632prime-))
(not (Flag631prime-))
(not (Flag630prime-))
(not (Flag629prime-))
(not (Flag628prime-))
(not (Flag627prime-))
(not (Flag626prime-))
(not (Flag87prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag93prime-))
(not (Flag91prime-))
(not (Flag89prime-))
(not (Flag69prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag92prime-))
(not (Flag85prime-))
(not (Flag84prime-))
(not (Flag97prime-))
(not (Flag95prime-))
(not (Flag94prime-))
(not (Flag82prime-))
(not (Flag98prime-))
(not (Flag96prime-))
(not (Flag83prime-))
(not (Flag81prime-))
(not (Flag76prime-))
(not (Flag74prime-))
(not (Flag37prime-))
)
)
(:action STOP-PASS_AS-AK
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AK)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag637prime-))
(not (Flag636prime-))
(not (Flag635prime-))
(not (Flag634prime-))
(not (Flag633prime-))
(not (Flag632prime-))
(not (Flag631prime-))
(not (Flag630prime-))
(not (Flag629prime-))
(not (Flag628prime-))
(not (Flag627prime-))
(not (Flag626prime-))
(not (Flag87prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag93prime-))
(not (Flag91prime-))
(not (Flag89prime-))
(not (Flag69prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag92prime-))
(not (Flag85prime-))
(not (Flag84prime-))
(not (Flag97prime-))
(not (Flag95prime-))
(not (Flag94prime-))
(not (Flag82prime-))
(not (Flag98prime-))
(not (Flag96prime-))
(not (Flag83prime-))
(not (Flag81prime-))
(not (Flag76prime-))
(not (Flag74prime-))
(not (Flag37prime-))
)
)
(:action Flag1Action
:parameters ()
:effect
(and
(Flag1-)
(Flag1prime-)
)

)
(:action Flag3Action
:parameters ()
:effect
(and
(Flag3-)
(Flag3prime-)
)

)
(:action Flag5Action
:parameters ()
:effect
(and
(Flag5-)
(Flag5prime-)
)

)
(:action Flag7Action
:parameters ()
:effect
(and
(Flag7-)
(Flag7prime-)
)

)
(:action Flag9Action
:parameters ()
:effect
(and
(Flag9-)
(Flag9prime-)
)

)
(:action Flag11Action
:parameters ()
:effect
(and
(Flag11-)
(Flag11prime-)
)

)
(:action Flag13Action
:parameters ()
:effect
(and
(Flag13-)
(Flag13prime-)
)

)
(:action Flag15Action
:parameters ()
:effect
(and
(Flag15-)
(Flag15prime-)
)

)
(:action Flag17Action
:parameters ()
:effect
(and
(Flag17-)
(Flag17prime-)
)

)
(:action Flag19Action
:parameters ()
:effect
(and
(Flag19-)
(Flag19prime-)
)

)
(:action Flag21Action
:parameters ()
:effect
(and
(Flag21-)
(Flag21prime-)
)

)
(:action Flag23Action
:parameters ()
:effect
(and
(Flag23-)
(Flag23prime-)
)

)
(:action Flag25Action
:parameters ()
:effect
(and
(Flag25-)
(Flag25prime-)
)

)
(:action Flag27Action
:parameters ()
:effect
(and
(Flag27-)
(Flag27prime-)
)

)
(:action Flag29Action
:parameters ()
:effect
(and
(Flag29-)
(Flag29prime-)
)

)
(:action Flag31Action
:parameters ()
:effect
(and
(Flag31-)
(Flag31prime-)
)

)
(:action Flag33Action
:parameters ()
:effect
(and
(Flag33-)
(Flag33prime-)
)

)
(:action Flag35Action
:parameters ()
:effect
(and
(Flag35-)
(Flag35prime-)
)

)
(:action Prim37Action-30
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
(Flag37prime-)
(not (Flag37-))
)

)
(:action Prim37Action-31
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
(Flag37prime-)
(not (Flag37-))
)

)
(:action CHECKCONSISTENCYACTION
:parameters ()
:precondition
(and
(NOT-ERROR-)
(CHECKCONSISTENCY-)
)
:effect
(and
(NOT-CHECKCONSISTENCY-)
(not (CHECKCONSISTENCY-))
(not (Flag637prime-))
(not (Flag636prime-))
(not (Flag635prime-))
(not (Flag634prime-))
(not (Flag633prime-))
(not (Flag632prime-))
(not (Flag631prime-))
(not (Flag630prime-))
(not (Flag629prime-))
(not (Flag628prime-))
(not (Flag627prime-))
(not (Flag626prime-))
(not (Flag87prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag93prime-))
(not (Flag91prime-))
(not (Flag89prime-))
(not (Flag69prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag92prime-))
(not (Flag85prime-))
(not (Flag84prime-))
(not (Flag97prime-))
(not (Flag95prime-))
(not (Flag94prime-))
(not (Flag82prime-))
(not (Flag98prime-))
(not (Flag96prime-))
(not (Flag83prime-))
(not (Flag81prime-))
(not (Flag76prime-))
(not (Flag74prime-))
(not (Flag37prime-))
)
)
(:action MOVEUP-AK-AM
:parameters ()
:precondition
(and
(LIFT_AT-AK)
(NOT-ERROR-)
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(NOT-LIFT_AT-AK)
(LIFT_AT-AM)
(CHECKCONSISTENCY-)
(not (LIFT_AT-AK))
(not (NOT-LIFT_AT-AM))
(not (NOT-CHECKCONSISTENCY-))
(not (Flag637prime-))
(not (Flag636prime-))
(not (Flag635prime-))
(not (Flag634prime-))
(not (Flag633prime-))
(not (Flag632prime-))
(not (Flag631prime-))
(not (Flag630prime-))
(not (Flag629prime-))
(not (Flag628prime-))
(not (Flag627prime-))
(not (Flag626prime-))
(not (Flag87prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag93prime-))
(not (Flag91prime-))
(not (Flag89prime-))
(not (Flag69prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag92prime-))
(not (Flag85prime-))
(not (Flag84prime-))
(not (Flag97prime-))
(not (Flag95prime-))
(not (Flag94prime-))
(not (Flag82prime-))
(not (Flag98prime-))
(not (Flag96prime-))
(not (Flag83prime-))
(not (Flag81prime-))
(not (Flag76prime-))
(not (Flag74prime-))
(not (Flag37prime-))
)
)
(:action MOVEUP-AM-AO
:parameters ()
:precondition
(and
(LIFT_AT-AM)
(NOT-ERROR-)
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(NOT-LIFT_AT-AM)
(LIFT_AT-AO)
(CHECKCONSISTENCY-)
(not (LIFT_AT-AM))
(not (NOT-LIFT_AT-AO))
(not (NOT-CHECKCONSISTENCY-))
(not (Flag637prime-))
(not (Flag636prime-))
(not (Flag635prime-))
(not (Flag634prime-))
(not (Flag633prime-))
(not (Flag632prime-))
(not (Flag631prime-))
(not (Flag630prime-))
(not (Flag629prime-))
(not (Flag628prime-))
(not (Flag627prime-))
(not (Flag626prime-))
(not (Flag87prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag93prime-))
(not (Flag91prime-))
(not (Flag89prime-))
(not (Flag69prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag92prime-))
(not (Flag85prime-))
(not (Flag84prime-))
(not (Flag97prime-))
(not (Flag95prime-))
(not (Flag94prime-))
(not (Flag82prime-))
(not (Flag98prime-))
(not (Flag96prime-))
(not (Flag83prime-))
(not (Flag81prime-))
(not (Flag76prime-))
(not (Flag74prime-))
(not (Flag37prime-))
)
)
(:action MOVEUP-AP-AQ
:parameters ()
:precondition
(and
(LIFT_AT-AP)
(NOT-ERROR-)
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(NOT-LIFT_AT-AP)
(LIFT_AT-AQ)
(CHECKCONSISTENCY-)
(not (LIFT_AT-AP))
(not (NOT-LIFT_AT-AQ))
(not (NOT-CHECKCONSISTENCY-))
(not (Flag637prime-))
(not (Flag636prime-))
(not (Flag635prime-))
(not (Flag634prime-))
(not (Flag633prime-))
(not (Flag632prime-))
(not (Flag631prime-))
(not (Flag630prime-))
(not (Flag629prime-))
(not (Flag628prime-))
(not (Flag627prime-))
(not (Flag626prime-))
(not (Flag87prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag93prime-))
(not (Flag91prime-))
(not (Flag89prime-))
(not (Flag69prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag92prime-))
(not (Flag85prime-))
(not (Flag84prime-))
(not (Flag97prime-))
(not (Flag95prime-))
(not (Flag94prime-))
(not (Flag82prime-))
(not (Flag98prime-))
(not (Flag96prime-))
(not (Flag83prime-))
(not (Flag81prime-))
(not (Flag76prime-))
(not (Flag74prime-))
(not (Flag37prime-))
)
)
(:action MOVEUP-AO-AP
:parameters ()
:precondition
(and
(LIFT_AT-AO)
(NOT-ERROR-)
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(NOT-LIFT_AT-AO)
(LIFT_AT-AP)
(CHECKCONSISTENCY-)
(not (LIFT_AT-AO))
(not (NOT-LIFT_AT-AP))
(not (NOT-CHECKCONSISTENCY-))
(not (Flag637prime-))
(not (Flag636prime-))
(not (Flag635prime-))
(not (Flag634prime-))
(not (Flag633prime-))
(not (Flag632prime-))
(not (Flag631prime-))
(not (Flag630prime-))
(not (Flag629prime-))
(not (Flag628prime-))
(not (Flag627prime-))
(not (Flag626prime-))
(not (Flag87prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag93prime-))
(not (Flag91prime-))
(not (Flag89prime-))
(not (Flag69prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag92prime-))
(not (Flag85prime-))
(not (Flag84prime-))
(not (Flag97prime-))
(not (Flag95prime-))
(not (Flag94prime-))
(not (Flag82prime-))
(not (Flag98prime-))
(not (Flag96prime-))
(not (Flag83prime-))
(not (Flag81prime-))
(not (Flag76prime-))
(not (Flag74prime-))
(not (Flag37prime-))
)
)
(:action MOVEDOWN-AM-AK
:parameters ()
:precondition
(and
(LIFT_AT-AM)
(NOT-ERROR-)
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(NOT-LIFT_AT-AM)
(LIFT_AT-AK)
(CHECKCONSISTENCY-)
(not (LIFT_AT-AM))
(not (NOT-LIFT_AT-AK))
(not (NOT-CHECKCONSISTENCY-))
(not (Flag637prime-))
(not (Flag636prime-))
(not (Flag635prime-))
(not (Flag634prime-))
(not (Flag633prime-))
(not (Flag632prime-))
(not (Flag631prime-))
(not (Flag630prime-))
(not (Flag629prime-))
(not (Flag628prime-))
(not (Flag627prime-))
(not (Flag626prime-))
(not (Flag87prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag93prime-))
(not (Flag91prime-))
(not (Flag89prime-))
(not (Flag69prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag92prime-))
(not (Flag85prime-))
(not (Flag84prime-))
(not (Flag97prime-))
(not (Flag95prime-))
(not (Flag94prime-))
(not (Flag82prime-))
(not (Flag98prime-))
(not (Flag96prime-))
(not (Flag83prime-))
(not (Flag81prime-))
(not (Flag76prime-))
(not (Flag74prime-))
(not (Flag37prime-))
)
)
(:action MOVEDOWN-AO-AM
:parameters ()
:precondition
(and
(LIFT_AT-AO)
(NOT-ERROR-)
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(NOT-LIFT_AT-AO)
(LIFT_AT-AM)
(CHECKCONSISTENCY-)
(not (LIFT_AT-AO))
(not (NOT-LIFT_AT-AM))
(not (NOT-CHECKCONSISTENCY-))
(not (Flag637prime-))
(not (Flag636prime-))
(not (Flag635prime-))
(not (Flag634prime-))
(not (Flag633prime-))
(not (Flag632prime-))
(not (Flag631prime-))
(not (Flag630prime-))
(not (Flag629prime-))
(not (Flag628prime-))
(not (Flag627prime-))
(not (Flag626prime-))
(not (Flag87prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag93prime-))
(not (Flag91prime-))
(not (Flag89prime-))
(not (Flag69prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag92prime-))
(not (Flag85prime-))
(not (Flag84prime-))
(not (Flag97prime-))
(not (Flag95prime-))
(not (Flag94prime-))
(not (Flag82prime-))
(not (Flag98prime-))
(not (Flag96prime-))
(not (Flag83prime-))
(not (Flag81prime-))
(not (Flag76prime-))
(not (Flag74prime-))
(not (Flag37prime-))
)
)
(:action MOVEDOWN-AP-AO
:parameters ()
:precondition
(and
(LIFT_AT-AP)
(NOT-ERROR-)
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(NOT-LIFT_AT-AP)
(LIFT_AT-AO)
(CHECKCONSISTENCY-)
(not (LIFT_AT-AP))
(not (NOT-LIFT_AT-AO))
(not (NOT-CHECKCONSISTENCY-))
(not (Flag637prime-))
(not (Flag636prime-))
(not (Flag635prime-))
(not (Flag634prime-))
(not (Flag633prime-))
(not (Flag632prime-))
(not (Flag631prime-))
(not (Flag630prime-))
(not (Flag629prime-))
(not (Flag628prime-))
(not (Flag627prime-))
(not (Flag626prime-))
(not (Flag87prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag93prime-))
(not (Flag91prime-))
(not (Flag89prime-))
(not (Flag69prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag92prime-))
(not (Flag85prime-))
(not (Flag84prime-))
(not (Flag97prime-))
(not (Flag95prime-))
(not (Flag94prime-))
(not (Flag82prime-))
(not (Flag98prime-))
(not (Flag96prime-))
(not (Flag83prime-))
(not (Flag81prime-))
(not (Flag76prime-))
(not (Flag74prime-))
(not (Flag37prime-))
)
)
(:action Prim39Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim39Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim40Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim40Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim41Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim41Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim42Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim42Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim43Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim43Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim44Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim44Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim45Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim45Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim46Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim46Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim47Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim47Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim48Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim48Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim49Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim49Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim50Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim50Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim51Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim51Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim52Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim52Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim53Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim53Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim54Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim54Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim55Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim55Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim56Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim56Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim57Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim57Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim57Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AP))
)
:effect
(and
)

)
(:action Prim58Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim58Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim59Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim59Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim60Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim60Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim61Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim61Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim62Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim62Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim63Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim63Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim64Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim64Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim65Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim65Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim66Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim66Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim67Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim67Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim68Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim68Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim69Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
(Flag69prime-)
(not (Flag69-))
)

)
(:action Prim69Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
(Flag69prime-)
(not (Flag69-))
)

)
(:action Prim70Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim70Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim71Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim71Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim72Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim72Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim73Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim73Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim74Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
(Flag74prime-)
(not (Flag74-))
)

)
(:action Prim74Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
(Flag74prime-)
(not (Flag74-))
)

)
(:action Prim75Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim75Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim76Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
(Flag76prime-)
(not (Flag76-))
)

)
(:action Prim76Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
(Flag76prime-)
(not (Flag76-))
)

)
(:action Prim77Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim77Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim78Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim78Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim79Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim79Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim80Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim80Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim81Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
(Flag81prime-)
(not (Flag81-))
)

)
(:action Prim81Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
(Flag81prime-)
(not (Flag81-))
)

)
(:action Prim82Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
(Flag82prime-)
(not (Flag82-))
)

)
(:action Prim82Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
(Flag82prime-)
(not (Flag82-))
)

)
(:action Prim83Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
(Flag83prime-)
(not (Flag83-))
)

)
(:action Prim83Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
(Flag83prime-)
(not (Flag83-))
)

)
(:action Prim84Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
(Flag84prime-)
(not (Flag84-))
)

)
(:action Prim84Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
(Flag84prime-)
(not (Flag84-))
)

)
(:action Prim85Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
(Flag85prime-)
(not (Flag85-))
)

)
(:action Prim85Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
(Flag85prime-)
(not (Flag85-))
)

)
(:action Prim86Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim86Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Flag87Action
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AP)
)
:effect
(and
(Flag87-)
(Flag87prime-)
)

)
(:action Prim87Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
(Flag87prime-)
(not (Flag87-))
)

)
(:action Prim87Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
(Flag87prime-)
(not (Flag87-))
)

)
(:action Prim87Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AP))
)
:effect
(and
(Flag87prime-)
(not (Flag87-))
)

)
(:action Prim88Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim88Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim89Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
(Flag89prime-)
(not (Flag89-))
)

)
(:action Prim89Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
(Flag89prime-)
(not (Flag89-))
)

)
(:action Prim90Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim90Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim91Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
(Flag91prime-)
(not (Flag91-))
)

)
(:action Prim91Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
(Flag91prime-)
(not (Flag91-))
)

)
(:action Prim92Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
(Flag92prime-)
(not (Flag92-))
)

)
(:action Prim92Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
(Flag92prime-)
(not (Flag92-))
)

)
(:action Prim93Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
(Flag93prime-)
(not (Flag93-))
)

)
(:action Prim93Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
(Flag93prime-)
(not (Flag93-))
)

)
(:action Prim94Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
(Flag94prime-)
(not (Flag94-))
)

)
(:action Prim94Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
(Flag94prime-)
(not (Flag94-))
)

)
(:action Prim95Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
(Flag95prime-)
(not (Flag95-))
)

)
(:action Prim95Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
(Flag95prime-)
(not (Flag95-))
)

)
(:action Prim96Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
(Flag96prime-)
(not (Flag96-))
)

)
(:action Prim96Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
(Flag96prime-)
(not (Flag96-))
)

)
(:action Prim97Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
(Flag97prime-)
(not (Flag97-))
)

)
(:action Prim97Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
(Flag97prime-)
(not (Flag97-))
)

)
(:action Prim98Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
(Flag98prime-)
(not (Flag98-))
)

)
(:action Prim98Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
(Flag98prime-)
(not (Flag98-))
)

)
(:action Prim100Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim100Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim101Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim101Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim102Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim102Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim103Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim103Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim104Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim104Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim105Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim105Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim106Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim106Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim107Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim107Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim108Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim108Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim109Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim109Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim110Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim110Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim111Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim111Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim112Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim112Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim113Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim113Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim114Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim114Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim115Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim115Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim116Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim116Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim117Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim117Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim118Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim118Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim118Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AP))
)
:effect
(and
)

)
(:action Prim119Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim119Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim120Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim120Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim121Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim121Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim122Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim122Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim123Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim123Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim124Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim124Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim125Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim125Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim126Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim126Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim127Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim127Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim128Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim128Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim129Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim129Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim131Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim131Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim132Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim132Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim133Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim133Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim134Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim134Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim135Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim135Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim136Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim136Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim137Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim137Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim138Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim138Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim139Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim139Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim140Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim140Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim141Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim141Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim142Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim142Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim143Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim143Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim144Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim144Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim145Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim145Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim146Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim146Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim147Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim147Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim148Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim148Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim149Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim149Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim149Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AP))
)
:effect
(and
)

)
(:action Prim150Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim150Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim151Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim151Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim152Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim152Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim153Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim153Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim154Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim154Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim155Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim155Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim156Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim156Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim157Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim157Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim158Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim158Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim159Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim159Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim160Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim160Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim162Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim162Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim163Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim163Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim164Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim164Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim165Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim165Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim166Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim166Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim167Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim167Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim168Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim168Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim169Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim169Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim170Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim170Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim171Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim171Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim172Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim172Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim173Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim173Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim174Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim174Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim175Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim175Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim176Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim176Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim177Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim177Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim178Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim178Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim179Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim179Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim180Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim180Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim180Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AP))
)
:effect
(and
)

)
(:action Prim181Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim181Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim182Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim182Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim183Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim183Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim184Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim184Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim185Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim185Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim186Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim186Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim187Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim187Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim188Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim188Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim189Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim189Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim190Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim190Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim191Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim191Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim193Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim193Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim194Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim194Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim195Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim195Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim196Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim196Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim197Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim197Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim198Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim198Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim199Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim199Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim200Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim200Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim201Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim201Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim202Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim202Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim203Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim203Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim204Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim204Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim205Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim205Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim206Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim206Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim207Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim207Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim208Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim208Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim209Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim209Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim210Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim210Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim211Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim211Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim211Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AP))
)
:effect
(and
)

)
(:action Prim212Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim212Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim213Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim213Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim214Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim214Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim215Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim215Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim216Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim216Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim217Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim217Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim218Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim218Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim219Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim219Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim220Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim220Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim221Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim221Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim222Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim222Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim224Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim224Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim225Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim225Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim226Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim226Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim227Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim227Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim228Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim228Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim229Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim229Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim230Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim230Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim231Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim231Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim232Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim232Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim233Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim233Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim234Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim234Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim235Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim235Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim236Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim236Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim237Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim237Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim238Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim238Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim239Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim239Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim240Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim240Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim241Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim241Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim242Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim242Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim242Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AP))
)
:effect
(and
)

)
(:action Prim243Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim243Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim244Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim244Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim245Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim245Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim246Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim246Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim247Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim247Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim248Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim248Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim249Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim249Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim250Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim250Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim251Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim251Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim252Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim252Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim253Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim253Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim255Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim255Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim256Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim256Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim257Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim257Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim258Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim258Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim259Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim259Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim260Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim260Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim261Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim261Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim262Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim262Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim263Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim263Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim264Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim264Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim265Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim265Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim266Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim266Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim267Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim267Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim268Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim268Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim269Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim269Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim270Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim270Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim271Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim271Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim272Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim272Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim273Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim273Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim273Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AP))
)
:effect
(and
)

)
(:action Prim274Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim274Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim275Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim275Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim276Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim276Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim277Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim277Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim278Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim278Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim279Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim279Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim280Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim280Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim281Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim281Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim282Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim282Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim283Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim283Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim284Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim284Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim286Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim286Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim287Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim287Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim288Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim288Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim289Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim289Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim290Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim290Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim291Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim291Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim292Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim292Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim293Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim293Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim294Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim294Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim295Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim295Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim296Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim296Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim297Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim297Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim298Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim298Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim299Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim299Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim300Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim300Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim301Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim301Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim302Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim302Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim303Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim303Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim304Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim304Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim304Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AP))
)
:effect
(and
)

)
(:action Prim305Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim305Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim306Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim306Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim307Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim307Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim308Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim308Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim309Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim309Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim310Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim310Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim311Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim311Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim312Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim312Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim313Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim313Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim314Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim314Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim315Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim315Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim317Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim317Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim318Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim318Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim319Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim319Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim320Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim320Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim321Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim321Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim322Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim322Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim323Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim323Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim324Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim324Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim325Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim325Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim326Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim326Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim327Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim327Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim328Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim328Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim329Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim329Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim330Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim330Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim331Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim331Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim332Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim332Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim333Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim333Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim334Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim334Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim335Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim335Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim335Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AP))
)
:effect
(and
)

)
(:action Prim336Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim336Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim337Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim337Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim338Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim338Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim339Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim339Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim340Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim340Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim341Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim341Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim342Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim342Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim343Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim343Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim344Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim344Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim345Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim345Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim346Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim346Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim348Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim348Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim349Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim349Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim350Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim350Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim351Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim351Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim352Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim352Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim353Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim353Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim354Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim354Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim355Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim355Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim356Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim356Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim357Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim357Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim358Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim358Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim359Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim359Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim360Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim360Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim361Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim361Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim362Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim362Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim363Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim363Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim364Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim364Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim365Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim365Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim366Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim366Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim366Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AP))
)
:effect
(and
)

)
(:action Prim367Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim367Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim368Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim368Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim369Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim369Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim370Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim370Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim371Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim371Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim372Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim372Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim373Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim373Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim374Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim374Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim375Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim375Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim376Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim376Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim377Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim377Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim379Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim379Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim380Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim380Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim381Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim381Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim382Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim382Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim383Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim383Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim384Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim384Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim385Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim385Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim386Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim386Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim387Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim387Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim388Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim388Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim389Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim389Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim390Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim390Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim391Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim391Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim392Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim392Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim393Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim393Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim394Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim394Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim395Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim395Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim396Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim396Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim397Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim397Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim397Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AP))
)
:effect
(and
)

)
(:action Prim398Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim398Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim399Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim399Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim400Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim400Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim401Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim401Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim402Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim402Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim403Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim403Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim404Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim404Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim405Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim405Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim406Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim406Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim407Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim407Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim408Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim408Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim410Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim410Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim411Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim411Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim412Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim412Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim413Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim413Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim414Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim414Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim415Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim415Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim416Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim416Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim417Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim417Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim418Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim418Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim419Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim419Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim420Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim420Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim421Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim421Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim422Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim422Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim423Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim423Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim424Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim424Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim425Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim425Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim426Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim426Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim427Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim427Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim428Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim428Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim428Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AP))
)
:effect
(and
)

)
(:action Prim429Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim429Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim430Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim430Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim431Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim431Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim432Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim432Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim433Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim433Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim434Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim434Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim435Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim435Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim436Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim436Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim437Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim437Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim438Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim438Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim439Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim439Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim441Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim441Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim442Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim442Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim443Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim443Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim444Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim444Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim445Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim445Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim446Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim446Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim447Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim447Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim448Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim448Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim449Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim449Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim450Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim450Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim451Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim451Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim452Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim452Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim453Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim453Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim454Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim454Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim455Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim455Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim456Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim456Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim457Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim457Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim458Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim458Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim459Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim459Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim459Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AP))
)
:effect
(and
)

)
(:action Prim460Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim460Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim461Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim461Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim462Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim462Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim463Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim463Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim464Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim464Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim465Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim465Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim466Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim466Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim467Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim467Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim468Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim468Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim469Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim469Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim470Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim470Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim472Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim472Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim473Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim473Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim474Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim474Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim475Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim475Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim476Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim476Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim477Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim477Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim478Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim478Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim479Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim479Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim480Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim480Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim481Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim481Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim482Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim482Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim483Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim483Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim484Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim484Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim485Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim485Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim486Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim486Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim487Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim487Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim488Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim488Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim489Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim489Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim490Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim490Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim490Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AP))
)
:effect
(and
)

)
(:action Prim491Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim491Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim492Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim492Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim493Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim493Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim494Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim494Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim495Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim495Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim496Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim496Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim497Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim497Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim498Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim498Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim499Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim499Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim500Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim500Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim501Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim501Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim503Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim503Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim504Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim504Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim505Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim505Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim506Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim506Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim507Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim507Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim508Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim508Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim509Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim509Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim510Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim510Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim511Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim511Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim512Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim512Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim513Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim513Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim514Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim514Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim515Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim515Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim516Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim516Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim517Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim517Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim518Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim518Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim519Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim519Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim520Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim520Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim521Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim521Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim521Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AP))
)
:effect
(and
)

)
(:action Prim522Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim522Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim523Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim523Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim524Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim524Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim525Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim525Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim526Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim526Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim527Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim527Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim528Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim528Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim529Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim529Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim530Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim530Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim531Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim531Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim532Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim532Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim534Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim534Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim535Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim535Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim536Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim536Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim537Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim537Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim538Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim538Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim539Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim539Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim540Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim540Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim541Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim541Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim542Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim542Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim543Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim543Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim544Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim544Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim545Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim545Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim546Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim546Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim547Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim547Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim548Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim548Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim549Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim549Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim550Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim550Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim551Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim551Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim552Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim552Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim552Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AP))
)
:effect
(and
)

)
(:action Prim553Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim553Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim554Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim554Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim555Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim555Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim556Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim556Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim557Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim557Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim558Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim558Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim559Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim559Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim560Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim560Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim561Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim561Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim562Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim562Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim563Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim563Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim565Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim565Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim566Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim566Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim567Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim567Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim568Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim568Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim569Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim569Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim570Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim570Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim571Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim571Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim572Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim572Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim573Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim573Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim574Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim574Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim575Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim575Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim576Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim576Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim577Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim577Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim578Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim578Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim579Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim579Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim580Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim580Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim581Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim581Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim582Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim582Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim583Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim583Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim583Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AP))
)
:effect
(and
)

)
(:action Prim584Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim584Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim585Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim585Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim586Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim586Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim587Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim587Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim588Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim588Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim589Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim589Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim590Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim590Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim591Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim591Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim592Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim592Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim593Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim593Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim594Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim594Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim596Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim596Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim597Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim597Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim598Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim598Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim599Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim599Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim600Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim600Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim601Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim601Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim602Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim602Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim603Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim603Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim604Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim604Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim605Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim605Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim606Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim606Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim607Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim607Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim608Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim608Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim609Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim609Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim610Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim610Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim611Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim611Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim612Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim612Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim613Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim613Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim614Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim614Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim614Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AP))
)
:effect
(and
)

)
(:action Prim615Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim615Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim616Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim616Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim617Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim617Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim618Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim618Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim619Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim619Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim620Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim620Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim621Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim621Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim622Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim622Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim623Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim623Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim624Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim624Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim625Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim625Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Flag626Action
:parameters ()
:precondition
(and
(NOT-SERVED-PASS_AS)
(NOT-BOARDED-PASS_AS)
)
:effect
(and
(Flag626-)
(Flag626prime-)
)

)
(:action Prim626Action-0
:parameters ()
:precondition
(and
(not (NOT-SERVED-PASS_AS))
)
:effect
(and
(Flag626prime-)
(not (Flag626-))
)

)
(:action Prim626Action-1
:parameters ()
:precondition
(and
(not (NOT-BOARDED-PASS_AS))
)
:effect
(and
(Flag626prime-)
(not (Flag626-))
)

)
(:action Flag627Action
:parameters ()
:precondition
(and
(NOT-SERVED-PASS_AN)
(NOT-BOARDED-PASS_AN)
)
:effect
(and
(Flag627-)
(Flag627prime-)
)

)
(:action Prim627Action-0
:parameters ()
:precondition
(and
(not (NOT-SERVED-PASS_AN))
)
:effect
(and
(Flag627prime-)
(not (Flag627-))
)

)
(:action Prim627Action-1
:parameters ()
:precondition
(and
(not (NOT-BOARDED-PASS_AN))
)
:effect
(and
(Flag627prime-)
(not (Flag627-))
)

)
(:action Flag628Action
:parameters ()
:precondition
(and
(NOT-SERVED-PASS_AL)
(NOT-BOARDED-PASS_AL)
)
:effect
(and
(Flag628-)
(Flag628prime-)
)

)
(:action Prim628Action-0
:parameters ()
:precondition
(and
(not (NOT-SERVED-PASS_AL))
)
:effect
(and
(Flag628prime-)
(not (Flag628-))
)

)
(:action Prim628Action-1
:parameters ()
:precondition
(and
(not (NOT-BOARDED-PASS_AL))
)
:effect
(and
(Flag628prime-)
(not (Flag628-))
)

)
(:action Flag629Action
:parameters ()
:precondition
(and
(NOT-SERVED-PASS_AD)
(NOT-BOARDED-PASS_AD)
)
:effect
(and
(Flag629-)
(Flag629prime-)
)

)
(:action Prim629Action-0
:parameters ()
:precondition
(and
(not (NOT-SERVED-PASS_AD))
)
:effect
(and
(Flag629prime-)
(not (Flag629-))
)

)
(:action Prim629Action-1
:parameters ()
:precondition
(and
(not (NOT-BOARDED-PASS_AD))
)
:effect
(and
(Flag629prime-)
(not (Flag629-))
)

)
(:action Flag630Action
:parameters ()
:precondition
(and
(NOT-SERVED-PASS_AE)
(NOT-BOARDED-PASS_AE)
)
:effect
(and
(Flag630-)
(Flag630prime-)
)

)
(:action Prim630Action-0
:parameters ()
:precondition
(and
(not (NOT-SERVED-PASS_AE))
)
:effect
(and
(Flag630prime-)
(not (Flag630-))
)

)
(:action Prim630Action-1
:parameters ()
:precondition
(and
(not (NOT-BOARDED-PASS_AE))
)
:effect
(and
(Flag630prime-)
(not (Flag630-))
)

)
(:action Flag631Action
:parameters ()
:precondition
(and
(NOT-SERVED-PASS_AB)
(NOT-BOARDED-PASS_AB)
)
:effect
(and
(Flag631-)
(Flag631prime-)
)

)
(:action Prim631Action-0
:parameters ()
:precondition
(and
(not (NOT-SERVED-PASS_AB))
)
:effect
(and
(Flag631prime-)
(not (Flag631-))
)

)
(:action Prim631Action-1
:parameters ()
:precondition
(and
(not (NOT-BOARDED-PASS_AB))
)
:effect
(and
(Flag631prime-)
(not (Flag631-))
)

)
(:action Flag632Action
:parameters ()
:precondition
(and
(NOT-SERVED-PASS_AA)
(NOT-BOARDED-PASS_AA)
)
:effect
(and
(Flag632-)
(Flag632prime-)
)

)
(:action Prim632Action-0
:parameters ()
:precondition
(and
(not (NOT-SERVED-PASS_AA))
)
:effect
(and
(Flag632prime-)
(not (Flag632-))
)

)
(:action Prim632Action-1
:parameters ()
:precondition
(and
(not (NOT-BOARDED-PASS_AA))
)
:effect
(and
(Flag632prime-)
(not (Flag632-))
)

)
(:action Flag633Action
:parameters ()
:precondition
(and
(NOT-SERVED-PASS_AX)
(NOT-BOARDED-PASS_AX)
)
:effect
(and
(Flag633-)
(Flag633prime-)
)

)
(:action Prim633Action-0
:parameters ()
:precondition
(and
(not (NOT-SERVED-PASS_AX))
)
:effect
(and
(Flag633prime-)
(not (Flag633-))
)

)
(:action Prim633Action-1
:parameters ()
:precondition
(and
(not (NOT-BOARDED-PASS_AX))
)
:effect
(and
(Flag633prime-)
(not (Flag633-))
)

)
(:action Flag634Action
:parameters ()
:precondition
(and
(NOT-SERVED-PASS_AU)
(NOT-BOARDED-PASS_AU)
)
:effect
(and
(Flag634-)
(Flag634prime-)
)

)
(:action Prim634Action-0
:parameters ()
:precondition
(and
(not (NOT-SERVED-PASS_AU))
)
:effect
(and
(Flag634prime-)
(not (Flag634-))
)

)
(:action Prim634Action-1
:parameters ()
:precondition
(and
(not (NOT-BOARDED-PASS_AU))
)
:effect
(and
(Flag634prime-)
(not (Flag634-))
)

)
(:action Flag635Action
:parameters ()
:precondition
(and
(NOT-SERVED-PASS_BC)
(NOT-BOARDED-PASS_BC)
)
:effect
(and
(Flag635-)
(Flag635prime-)
)

)
(:action Prim635Action-0
:parameters ()
:precondition
(and
(not (NOT-SERVED-PASS_BC))
)
:effect
(and
(Flag635prime-)
(not (Flag635-))
)

)
(:action Prim635Action-1
:parameters ()
:precondition
(and
(not (NOT-BOARDED-PASS_BC))
)
:effect
(and
(Flag635prime-)
(not (Flag635-))
)

)
(:action Flag636Action
:parameters ()
:precondition
(and
(NOT-SERVED-PASS_BB)
(NOT-BOARDED-PASS_BB)
)
:effect
(and
(Flag636-)
(Flag636prime-)
)

)
(:action Prim636Action-0
:parameters ()
:precondition
(and
(not (NOT-SERVED-PASS_BB))
)
:effect
(and
(Flag636prime-)
(not (Flag636-))
)

)
(:action Prim636Action-1
:parameters ()
:precondition
(and
(not (NOT-BOARDED-PASS_BB))
)
:effect
(and
(Flag636prime-)
(not (Flag636-))
)

)
(:action Flag637Action
:parameters ()
:precondition
(and
(NOT-SERVED-PASS_BA)
(NOT-BOARDED-PASS_BA)
)
:effect
(and
(Flag637-)
(Flag637prime-)
)

)
(:action Prim637Action-0
:parameters ()
:precondition
(and
(not (NOT-SERVED-PASS_BA))
)
:effect
(and
(Flag637prime-)
(not (Flag637-))
)

)
(:action Prim637Action-1
:parameters ()
:precondition
(and
(not (NOT-BOARDED-PASS_BA))
)
:effect
(and
(Flag637prime-)
(not (Flag637-))
)

)
)
