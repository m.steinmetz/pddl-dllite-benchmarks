(define (domain grounded-SIMPLE-ADL-ELEVATORS)
(:requirements
:strips
)
(:predicates
(Flag244-)
(Flag243-)
(Flag242-)
(Flag241-)
(Flag240-)
(Flag239-)
(Flag38-)
(NOT-LIFT_AT-B)
(LIFT_AT-A)
(CHECKCONSISTENCY-)
(LIFT_AT-C)
(ERROR-)
(Flag21-)
(Flag19-)
(Flag17-)
(Flag15-)
(Flag13-)
(Flag11-)
(Flag9-)
(Flag7-)
(Flag5-)
(Flag3-)
(Flag1-)
(Flag40-)
(Flag39-)
(LIFT_AT-D)
(LIFT_AT-E)
(Flag22-)
(Flag20-)
(Flag18-)
(Flag16-)
(Flag14-)
(Flag12-)
(Flag10-)
(Flag8-)
(Flag6-)
(Flag4-)
(Flag2-)
(BOARDED-PASS_H)
(Flag37-)
(Flag36-)
(LIFT_AT-F)
(LIFT_AT-G)
(SERVED-PASS_N)
(BOARDED-PASS_N)
(BOARDED-PASS_I)
(Flag35-)
(Flag34-)
(LIFT_AT-L)
(LIFT_AT-M)
(SERVED-PASS_O)
(SERVED-PASS_H)
(SERVED-PASS_I)
(BOARDED-PASS_J)
(Flag33-)
(Flag32-)
(LIFT_AT-P)
(LIFT_AT-Q)
(BOARDED-PASS_O)
(SERVED-PASS_J)
(SERVED-PASS_K)
(Flag31-)
(Flag30-)
(Flag23-)
(BOARDED-PASS_K)
(NOT-BOARDED-PASS_K)
(NOT-CHECKCONSISTENCY-)
(NOT-LIFT_AT-P)
(NOT-LIFT_AT-M)
(NOT-SERVED-PASS_K)
(NOT-SERVED-PASS_J)
(NOT-BOARDED-PASS_O)
(NOT-LIFT_AT-Q)
(NOT-LIFT_AT-L)
(NOT-LIFT_AT-G)
(NOT-BOARDED-PASS_J)
(NOT-SERVED-PASS_I)
(NOT-SERVED-PASS_H)
(NOT-SERVED-PASS_O)
(NOT-LIFT_AT-F)
(NOT-LIFT_AT-E)
(NOT-BOARDED-PASS_I)
(NOT-BOARDED-PASS_N)
(NOT-SERVED-PASS_N)
(NOT-LIFT_AT-D)
(NOT-LIFT_AT-C)
(NOT-BOARDED-PASS_H)
(NOT-ERROR-)
(LIFT_AT-B)
(NOT-LIFT_AT-A)
)
(:action STOP-PASS_K-P
:parameters ()
:precondition
(and
(Flag30-)
)
:effect
(and
(when
(and
(Flag244-)
)
(and
(BOARDED-PASS_K)
(not (NOT-BOARDED-PASS_K))
)
)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action STOP-PASS_K-Q
:parameters ()
:precondition
(and
(Flag31-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:derived (Flag23-)(and (Flag2-)(Flag4-)(Flag6-)(Flag8-)(Flag10-)(Flag12-)(Flag14-)(Flag16-)(Flag18-)(Flag20-)(Flag22-)(SERVED-PASS_N)(SERVED-PASS_O)(SERVED-PASS_H)(SERVED-PASS_I)(SERVED-PASS_J)(SERVED-PASS_K)(NOT-ERROR-)(NOT-CHECKCONSISTENCY-)))

(:action MOVEDOWN-Q-P
:parameters ()
:precondition
(and
(LIFT_AT-Q)
(NOT-ERROR-)
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(NOT-LIFT_AT-Q)
(LIFT_AT-P)
(CHECKCONSISTENCY-)
(not (LIFT_AT-Q))
(not (NOT-LIFT_AT-P))
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action MOVEDOWN-P-M
:parameters ()
:precondition
(and
(LIFT_AT-P)
(NOT-ERROR-)
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(NOT-LIFT_AT-P)
(LIFT_AT-M)
(CHECKCONSISTENCY-)
(not (LIFT_AT-P))
(not (NOT-LIFT_AT-M))
(not (NOT-CHECKCONSISTENCY-))
)
)
(:derived (Flag30-)(and (NOT-CHECKCONSISTENCY-)(NOT-ERROR-)(LIFT_AT-P)))

(:derived (Flag31-)(and (NOT-CHECKCONSISTENCY-)(NOT-ERROR-)(LIFT_AT-Q)))

(:action STOP-PASS_K-L
:parameters ()
:precondition
(and
(Flag32-)
)
:effect
(and
(when
(and
(BOARDED-PASS_K)
)
(and
(SERVED-PASS_K)
(NOT-BOARDED-PASS_K)
(not (NOT-SERVED-PASS_K))
(not (BOARDED-PASS_K))
)
)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action STOP-PASS_K-M
:parameters ()
:precondition
(and
(Flag33-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action STOP-PASS_J-P
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-P)
)
:effect
(and
(when
(and
(BOARDED-PASS_J)
)
(and
(SERVED-PASS_J)
(NOT-BOARDED-PASS_J)
(not (NOT-SERVED-PASS_J))
(not (BOARDED-PASS_J))
)
)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action STOP-PASS_J-Q
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-Q)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action STOP-PASS_I-P
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-P)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action STOP-PASS_I-Q
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-Q)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action STOP-PASS_H-P
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-P)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action STOP-PASS_H-Q
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-Q)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action STOP-PASS_O-P
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-P)
)
:effect
(and
(when
(and
(Flag240-)
)
(and
(BOARDED-PASS_O)
(not (NOT-BOARDED-PASS_O))
)
)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action STOP-PASS_O-Q
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-Q)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action STOP-PASS_N-P
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-P)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action STOP-PASS_N-Q
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-Q)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action MOVEUP-P-Q
:parameters ()
:precondition
(and
(LIFT_AT-P)
(NOT-ERROR-)
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(NOT-LIFT_AT-P)
(LIFT_AT-Q)
(CHECKCONSISTENCY-)
(not (LIFT_AT-P))
(not (NOT-LIFT_AT-Q))
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action MOVEUP-M-P
:parameters ()
:precondition
(and
(LIFT_AT-M)
(NOT-ERROR-)
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(NOT-LIFT_AT-M)
(LIFT_AT-P)
(CHECKCONSISTENCY-)
(not (LIFT_AT-M))
(not (NOT-LIFT_AT-P))
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action MOVEDOWN-M-L
:parameters ()
:precondition
(and
(LIFT_AT-M)
(NOT-ERROR-)
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(NOT-LIFT_AT-M)
(LIFT_AT-L)
(CHECKCONSISTENCY-)
(not (LIFT_AT-M))
(not (NOT-LIFT_AT-L))
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action MOVEDOWN-L-G
:parameters ()
:precondition
(and
(LIFT_AT-L)
(NOT-ERROR-)
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(NOT-LIFT_AT-L)
(LIFT_AT-G)
(CHECKCONSISTENCY-)
(not (LIFT_AT-L))
(not (NOT-LIFT_AT-G))
(not (NOT-CHECKCONSISTENCY-))
)
)
(:derived (Flag32-)(and (NOT-CHECKCONSISTENCY-)(NOT-ERROR-)(LIFT_AT-L)))

(:derived (Flag33-)(and (NOT-CHECKCONSISTENCY-)(NOT-ERROR-)(LIFT_AT-M)))

(:action STOP-PASS_K-F
:parameters ()
:precondition
(and
(Flag34-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action STOP-PASS_K-G
:parameters ()
:precondition
(and
(Flag35-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action STOP-PASS_J-L
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-L)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action STOP-PASS_J-M
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-M)
)
:effect
(and
(when
(and
(Flag243-)
)
(and
(BOARDED-PASS_J)
(not (NOT-BOARDED-PASS_J))
)
)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action STOP-PASS_I-L
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-L)
)
:effect
(and
(when
(and
(BOARDED-PASS_I)
)
(and
(SERVED-PASS_I)
(NOT-BOARDED-PASS_I)
(not (NOT-SERVED-PASS_I))
(not (BOARDED-PASS_I))
)
)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action STOP-PASS_I-M
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-M)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action STOP-PASS_H-L
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-L)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action STOP-PASS_H-M
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-M)
)
:effect
(and
(when
(and
(BOARDED-PASS_H)
)
(and
(SERVED-PASS_H)
(NOT-BOARDED-PASS_H)
(not (NOT-SERVED-PASS_H))
(not (BOARDED-PASS_H))
)
)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action STOP-PASS_O-L
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-L)
)
:effect
(and
(when
(and
(BOARDED-PASS_O)
)
(and
(SERVED-PASS_O)
(NOT-BOARDED-PASS_O)
(not (NOT-SERVED-PASS_O))
(not (BOARDED-PASS_O))
)
)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action STOP-PASS_O-M
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-M)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action STOP-PASS_N-L
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-L)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action STOP-PASS_N-M
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-M)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action MOVEUP-L-M
:parameters ()
:precondition
(and
(LIFT_AT-L)
(NOT-ERROR-)
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(NOT-LIFT_AT-L)
(LIFT_AT-M)
(CHECKCONSISTENCY-)
(not (LIFT_AT-L))
(not (NOT-LIFT_AT-M))
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action MOVEUP-G-L
:parameters ()
:precondition
(and
(LIFT_AT-G)
(NOT-ERROR-)
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(NOT-LIFT_AT-G)
(LIFT_AT-L)
(CHECKCONSISTENCY-)
(not (LIFT_AT-G))
(not (NOT-LIFT_AT-L))
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action MOVEDOWN-G-F
:parameters ()
:precondition
(and
(LIFT_AT-G)
(NOT-ERROR-)
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(NOT-LIFT_AT-G)
(LIFT_AT-F)
(CHECKCONSISTENCY-)
(not (LIFT_AT-G))
(not (NOT-LIFT_AT-F))
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action MOVEDOWN-F-E
:parameters ()
:precondition
(and
(LIFT_AT-F)
(NOT-ERROR-)
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(NOT-LIFT_AT-F)
(LIFT_AT-E)
(CHECKCONSISTENCY-)
(not (LIFT_AT-F))
(not (NOT-LIFT_AT-E))
(not (NOT-CHECKCONSISTENCY-))
)
)
(:derived (Flag34-)(and (NOT-CHECKCONSISTENCY-)(NOT-ERROR-)(LIFT_AT-F)))

(:derived (Flag35-)(and (NOT-CHECKCONSISTENCY-)(NOT-ERROR-)(LIFT_AT-G)))

(:action STOP-PASS_K-D
:parameters ()
:precondition
(and
(Flag36-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action STOP-PASS_K-E
:parameters ()
:precondition
(and
(Flag37-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action STOP-PASS_J-F
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-F)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action STOP-PASS_J-G
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-G)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action STOP-PASS_I-F
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-F)
)
:effect
(and
(when
(and
(Flag242-)
)
(and
(BOARDED-PASS_I)
(not (NOT-BOARDED-PASS_I))
)
)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action STOP-PASS_I-G
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-G)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action STOP-PASS_H-F
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-F)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action STOP-PASS_H-G
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-G)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action STOP-PASS_O-F
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-F)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action STOP-PASS_O-G
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-G)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action STOP-PASS_N-F
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-F)
)
:effect
(and
(when
(and
(Flag239-)
)
(and
(BOARDED-PASS_N)
(not (NOT-BOARDED-PASS_N))
)
)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action STOP-PASS_N-G
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-G)
)
:effect
(and
(when
(and
(BOARDED-PASS_N)
)
(and
(SERVED-PASS_N)
(NOT-BOARDED-PASS_N)
(not (NOT-SERVED-PASS_N))
(not (BOARDED-PASS_N))
)
)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action MOVEUP-F-G
:parameters ()
:precondition
(and
(LIFT_AT-F)
(NOT-ERROR-)
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(NOT-LIFT_AT-F)
(LIFT_AT-G)
(CHECKCONSISTENCY-)
(not (LIFT_AT-F))
(not (NOT-LIFT_AT-G))
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action MOVEUP-E-F
:parameters ()
:precondition
(and
(LIFT_AT-E)
(NOT-ERROR-)
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(NOT-LIFT_AT-E)
(LIFT_AT-F)
(CHECKCONSISTENCY-)
(not (LIFT_AT-E))
(not (NOT-LIFT_AT-F))
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action MOVEDOWN-E-D
:parameters ()
:precondition
(and
(LIFT_AT-E)
(NOT-ERROR-)
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(NOT-LIFT_AT-E)
(LIFT_AT-D)
(CHECKCONSISTENCY-)
(not (LIFT_AT-E))
(not (NOT-LIFT_AT-D))
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action MOVEDOWN-D-C
:parameters ()
:precondition
(and
(LIFT_AT-D)
(NOT-ERROR-)
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(NOT-LIFT_AT-D)
(LIFT_AT-C)
(CHECKCONSISTENCY-)
(not (LIFT_AT-D))
(not (NOT-LIFT_AT-C))
(not (NOT-CHECKCONSISTENCY-))
)
)
(:derived (Flag36-)(and (NOT-CHECKCONSISTENCY-)(NOT-ERROR-)(LIFT_AT-D)))

(:derived (Flag37-)(and (NOT-CHECKCONSISTENCY-)(NOT-ERROR-)(LIFT_AT-E)))

(:action STOP-PASS_K-C
:parameters ()
:precondition
(and
(Flag39-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action STOP-PASS_K-A
:parameters ()
:precondition
(and
(Flag40-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action STOP-PASS_J-D
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-D)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action STOP-PASS_J-E
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-E)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action STOP-PASS_I-D
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-D)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action STOP-PASS_I-E
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-E)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action STOP-PASS_H-D
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-D)
)
:effect
(and
(when
(and
(Flag241-)
)
(and
(BOARDED-PASS_H)
(not (NOT-BOARDED-PASS_H))
)
)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action STOP-PASS_H-E
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-E)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action STOP-PASS_O-D
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-D)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action STOP-PASS_O-E
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-E)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action STOP-PASS_N-D
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-D)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action STOP-PASS_N-E
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-E)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:derived (Flag2-)(and (Flag1-)))

(:derived (Flag4-)(and (Flag3-)))

(:derived (Flag6-)(and (Flag5-)))

(:derived (Flag8-)(and (Flag7-)))

(:derived (Flag10-)(and (Flag9-)))

(:derived (Flag12-)(and (Flag11-)))

(:derived (Flag14-)(and (Flag13-)))

(:derived (Flag16-)(and (Flag15-)))

(:derived (Flag18-)(and (Flag17-)))

(:derived (Flag20-)(and (Flag19-)))

(:derived (Flag22-)(and (Flag21-)))

(:action MOVEUP-D-E
:parameters ()
:precondition
(and
(LIFT_AT-D)
(NOT-ERROR-)
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(NOT-LIFT_AT-D)
(LIFT_AT-E)
(CHECKCONSISTENCY-)
(not (LIFT_AT-D))
(not (NOT-LIFT_AT-E))
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action MOVEUP-C-D
:parameters ()
:precondition
(and
(LIFT_AT-C)
(NOT-ERROR-)
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(NOT-LIFT_AT-C)
(LIFT_AT-D)
(CHECKCONSISTENCY-)
(not (LIFT_AT-C))
(not (NOT-LIFT_AT-D))
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action MOVEDOWN-C-B
:parameters ()
:precondition
(and
(LIFT_AT-C)
(NOT-ERROR-)
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(NOT-LIFT_AT-C)
(LIFT_AT-B)
(CHECKCONSISTENCY-)
(not (LIFT_AT-C))
(not (NOT-LIFT_AT-B))
(not (NOT-CHECKCONSISTENCY-))
)
)
(:derived (Flag39-)(and (NOT-CHECKCONSISTENCY-)(NOT-ERROR-)(LIFT_AT-C)))

(:derived (Flag40-)(and (NOT-CHECKCONSISTENCY-)(NOT-ERROR-)(LIFT_AT-A)))

(:action STOP-PASS_K-B
:parameters ()
:precondition
(and
(Flag38-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action STOP-PASS_J-B
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-B)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action STOP-PASS_J-C
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-C)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action STOP-PASS_J-A
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-A)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action STOP-PASS_I-B
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-B)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action STOP-PASS_I-C
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-C)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action STOP-PASS_I-A
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-A)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action STOP-PASS_H-B
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-B)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action STOP-PASS_H-C
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-C)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action STOP-PASS_H-A
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-A)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action STOP-PASS_O-B
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-B)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action STOP-PASS_O-C
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-C)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action STOP-PASS_O-A
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-A)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action STOP-PASS_N-B
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-B)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action STOP-PASS_N-C
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-C)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action STOP-PASS_N-A
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-A)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:derived (Flag1-)(and ))

(:derived (Flag3-)(and ))

(:derived (Flag5-)(and ))

(:derived (Flag7-)(and ))

(:derived (Flag9-)(and ))

(:derived (Flag11-)(and ))

(:derived (Flag13-)(and ))

(:derived (Flag15-)(and ))

(:derived (Flag17-)(and ))

(:derived (Flag19-)(and ))

(:derived (Flag21-)(and ))

(:action CHECKCONSISTENCYACTION
:parameters ()
:precondition
(and
(NOT-ERROR-)
(CHECKCONSISTENCY-)
)
:effect
(and
(NOT-CHECKCONSISTENCY-)
(not (CHECKCONSISTENCY-))
)
)
(:action MOVEUP-B-C
:parameters ()
:precondition
(and
(LIFT_AT-B)
(NOT-ERROR-)
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(NOT-LIFT_AT-B)
(LIFT_AT-C)
(CHECKCONSISTENCY-)
(not (LIFT_AT-B))
(not (NOT-LIFT_AT-C))
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action MOVEUP-A-B
:parameters ()
:precondition
(and
(LIFT_AT-A)
(NOT-ERROR-)
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(NOT-LIFT_AT-A)
(LIFT_AT-B)
(CHECKCONSISTENCY-)
(not (LIFT_AT-A))
(not (NOT-LIFT_AT-B))
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action MOVEDOWN-B-A
:parameters ()
:precondition
(and
(LIFT_AT-B)
(NOT-ERROR-)
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(NOT-LIFT_AT-B)
(LIFT_AT-A)
(CHECKCONSISTENCY-)
(not (LIFT_AT-B))
(not (NOT-LIFT_AT-A))
(not (NOT-CHECKCONSISTENCY-))
)
)
(:derived (Flag38-)(and (NOT-CHECKCONSISTENCY-)(NOT-ERROR-)(LIFT_AT-B)))

(:derived (Flag239-)(and (NOT-BOARDED-PASS_N)(NOT-SERVED-PASS_N)))

(:derived (Flag240-)(and (NOT-BOARDED-PASS_O)(NOT-SERVED-PASS_O)))

(:derived (Flag241-)(and (NOT-BOARDED-PASS_H)(NOT-SERVED-PASS_H)))

(:derived (Flag242-)(and (NOT-BOARDED-PASS_I)(NOT-SERVED-PASS_I)))

(:derived (Flag243-)(and (NOT-BOARDED-PASS_J)(NOT-SERVED-PASS_J)))

(:derived (Flag244-)(and (NOT-BOARDED-PASS_K)(NOT-SERVED-PASS_K)))

)
