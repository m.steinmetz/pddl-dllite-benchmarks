(define (domain grounded-SIMPLE-ADL-ELEVATORS)
(:requirements
:strips
)
(:predicates
(Flag567-)
(Flag566-)
(Flag565-)
(Flag564-)
(Flag563-)
(Flag562-)
(Flag561-)
(Flag560-)
(Flag559-)
(Flag558-)
(Flag557-)
(Flag121-)
(NOT-LIFT_AT-AA)
(LIFT_AT-AB)
(CHECKCONSISTENCY-)
(LIFT_AT-AC)
(LIFT_AT-AE)
(ERROR-)
(Flag33-)
(Flag31-)
(Flag29-)
(Flag27-)
(Flag25-)
(Flag23-)
(Flag21-)
(Flag19-)
(Flag17-)
(Flag15-)
(Flag13-)
(Flag11-)
(Flag9-)
(Flag7-)
(Flag5-)
(Flag3-)
(Flag1-)
(SERVED-PASS_AY)
(BOARDED-PASS_AT)
(SERVED-PASS_AT)
(SERVED-PASS_AP)
(BOARDED-PASS_AJ)
(BOARDED-PASS_AK)
(BOARDED-PASS_AD)
(Flag120-)
(Flag119-)
(Flag104-)
(LIFT_AT-AF)
(Flag34-)
(Flag32-)
(Flag30-)
(Flag28-)
(Flag26-)
(Flag24-)
(Flag22-)
(Flag20-)
(Flag18-)
(Flag16-)
(Flag14-)
(Flag12-)
(Flag10-)
(Flag8-)
(Flag6-)
(Flag4-)
(Flag2-)
(BOARDED-PASS_AR)
(Flag101-)
(LIFT_AT-AG)
(LIFT_AT-AH)
(LIFT_AT-AI)
(SERVED-PASS_AW)
(SERVED-PASS_AR)
(BOARDED-PASS_AS)
(SERVED-PASS_AS)
(SERVED-PASS_AK)
(Flag117-)
(Flag115-)
(Flag100-)
(LIFT_AT-AL)
(LIFT_AT-AM)
(LIFT_AT-AN)
(BOARDED-PASS_AZ)
(BOARDED-PASS_BB)
(Flag109-)
(Flag98-)
(Flag97-)
(LIFT_AT-AO)
(LIFT_AT-AQ)
(SERVED-PASS_AD)
(Flag108-)
(Flag102-)
(LIFT_AT-AU)
(LIFT_AT-AV)
(SERVED-PASS_AZ)
(BOARDED-PASS_AY)
(BOARDED-PASS_AP)
(SERVED-PASS_BB)
(Flag105-)
(Flag94-)
(LIFT_AT-AX)
(LIFT_AT-BA)
(BOARDED-PASS_AW)
(SERVED-PASS_AJ)
(Flag99-)
(Flag95-)
(Flag35-)
(NOT-CHECKCONSISTENCY-)
(NOT-LIFT_AT-AX)
(NOT-LIFT_AT-AV)
(NOT-SERVED-PASS_AJ)
(NOT-BOARDED-PASS_AW)
(NOT-LIFT_AT-BA)
(NOT-LIFT_AT-AU)
(NOT-LIFT_AT-AQ)
(NOT-SERVED-PASS_BB)
(NOT-BOARDED-PASS_AP)
(NOT-BOARDED-PASS_AY)
(NOT-SERVED-PASS_AZ)
(NOT-LIFT_AT-AO)
(NOT-LIFT_AT-AN)
(NOT-SERVED-PASS_AD)
(NOT-LIFT_AT-AM)
(NOT-LIFT_AT-AL)
(NOT-LIFT_AT-AI)
(NOT-BOARDED-PASS_BB)
(NOT-BOARDED-PASS_AZ)
(NOT-LIFT_AT-AH)
(NOT-LIFT_AT-AG)
(NOT-LIFT_AT-AF)
(NOT-SERVED-PASS_AK)
(NOT-SERVED-PASS_AS)
(NOT-BOARDED-PASS_AS)
(NOT-SERVED-PASS_AR)
(NOT-SERVED-PASS_AW)
(NOT-LIFT_AT-AE)
(NOT-BOARDED-PASS_AR)
(NOT-LIFT_AT-AC)
(NOT-LIFT_AT-AB)
(NOT-BOARDED-PASS_AD)
(NOT-BOARDED-PASS_AK)
(NOT-BOARDED-PASS_AJ)
(NOT-SERVED-PASS_AP)
(NOT-SERVED-PASS_AT)
(NOT-BOARDED-PASS_AT)
(NOT-SERVED-PASS_AY)
(NOT-ERROR-)
(LIFT_AT-AA)
)
(:action STOP-PASS_BB-AX
:parameters ()
:precondition
(and
(Flag95-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action STOP-PASS_BB-BA
:parameters ()
:precondition
(and
(Flag99-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:derived (Flag35-)(and (Flag2-)(Flag4-)(Flag6-)(SERVED-PASS_AZ)(Flag8-)(SERVED-PASS_AY)(Flag10-)(SERVED-PASS_AW)(SERVED-PASS_AT)(SERVED-PASS_AR)(SERVED-PASS_AS)(SERVED-PASS_AP)(Flag12-)(Flag14-)(SERVED-PASS_AJ)(SERVED-PASS_AK)(Flag16-)(Flag18-)(SERVED-PASS_AD)(Flag20-)(Flag22-)(Flag24-)(Flag26-)(Flag28-)(Flag30-)(SERVED-PASS_BB)(Flag32-)(Flag34-)(NOT-ERROR-)(NOT-CHECKCONSISTENCY-)))

(:action MOVEDOWN-BA-AX
:parameters ()
:precondition
(and
(LIFT_AT-BA)
(NOT-ERROR-)
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(NOT-LIFT_AT-BA)
(LIFT_AT-AX)
(CHECKCONSISTENCY-)
(not (LIFT_AT-BA))
(not (NOT-LIFT_AT-AX))
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action MOVEDOWN-AX-AV
:parameters ()
:precondition
(and
(LIFT_AT-AX)
(NOT-ERROR-)
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(NOT-LIFT_AT-AX)
(LIFT_AT-AV)
(CHECKCONSISTENCY-)
(not (LIFT_AT-AX))
(not (NOT-LIFT_AT-AV))
(not (NOT-CHECKCONSISTENCY-))
)
)
(:derived (Flag95-)(and (NOT-CHECKCONSISTENCY-)(NOT-ERROR-)(LIFT_AT-AX)))

(:derived (Flag99-)(and (NOT-CHECKCONSISTENCY-)(NOT-ERROR-)(LIFT_AT-BA)))

(:action STOP-PASS_BB-AU
:parameters ()
:precondition
(and
(Flag94-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action STOP-PASS_BB-AV
:parameters ()
:precondition
(and
(Flag105-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action STOP-PASS_AD-AX
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AX)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action STOP-PASS_AD-BA
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-BA)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action STOP-PASS_AK-AX
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AX)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action STOP-PASS_AK-BA
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-BA)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action STOP-PASS_AJ-AX
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AX)
)
:effect
(and
(when
(and
(BOARDED-PASS_AJ)
)
(and
(SERVED-PASS_AJ)
(NOT-BOARDED-PASS_AJ)
(not (NOT-SERVED-PASS_AJ))
(not (BOARDED-PASS_AJ))
)
)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action STOP-PASS_AJ-BA
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-BA)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action STOP-PASS_AP-AX
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AX)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action STOP-PASS_AP-BA
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-BA)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action STOP-PASS_AS-AX
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AX)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action STOP-PASS_AS-BA
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-BA)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action STOP-PASS_AR-AX
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AX)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action STOP-PASS_AR-BA
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-BA)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action STOP-PASS_AT-AX
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AX)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action STOP-PASS_AT-BA
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-BA)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action STOP-PASS_AW-AX
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AX)
)
:effect
(and
(when
(and
(Flag559-)
)
(and
(BOARDED-PASS_AW)
(not (NOT-BOARDED-PASS_AW))
)
)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action STOP-PASS_AW-BA
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-BA)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action STOP-PASS_AY-AX
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AX)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action STOP-PASS_AY-BA
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-BA)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action STOP-PASS_AZ-AX
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AX)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action STOP-PASS_AZ-BA
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-BA)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action MOVEUP-AX-BA
:parameters ()
:precondition
(and
(LIFT_AT-AX)
(NOT-ERROR-)
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(NOT-LIFT_AT-AX)
(LIFT_AT-BA)
(CHECKCONSISTENCY-)
(not (LIFT_AT-AX))
(not (NOT-LIFT_AT-BA))
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action MOVEUP-AV-AX
:parameters ()
:precondition
(and
(LIFT_AT-AV)
(NOT-ERROR-)
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(NOT-LIFT_AT-AV)
(LIFT_AT-AX)
(CHECKCONSISTENCY-)
(not (LIFT_AT-AV))
(not (NOT-LIFT_AT-AX))
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action MOVEDOWN-AV-AU
:parameters ()
:precondition
(and
(LIFT_AT-AV)
(NOT-ERROR-)
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(NOT-LIFT_AT-AV)
(LIFT_AT-AU)
(CHECKCONSISTENCY-)
(not (LIFT_AT-AV))
(not (NOT-LIFT_AT-AU))
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action MOVEDOWN-AU-AQ
:parameters ()
:precondition
(and
(LIFT_AT-AU)
(NOT-ERROR-)
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(NOT-LIFT_AT-AU)
(LIFT_AT-AQ)
(CHECKCONSISTENCY-)
(not (LIFT_AT-AU))
(not (NOT-LIFT_AT-AQ))
(not (NOT-CHECKCONSISTENCY-))
)
)
(:derived (Flag94-)(and (NOT-CHECKCONSISTENCY-)(NOT-ERROR-)(LIFT_AT-AU)))

(:derived (Flag105-)(and (NOT-CHECKCONSISTENCY-)(NOT-ERROR-)(LIFT_AT-AV)))

(:action STOP-PASS_BB-AO
:parameters ()
:precondition
(and
(Flag102-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action STOP-PASS_BB-AQ
:parameters ()
:precondition
(and
(Flag108-)
)
:effect
(and
(when
(and
(BOARDED-PASS_BB)
)
(and
(SERVED-PASS_BB)
(NOT-BOARDED-PASS_BB)
(not (NOT-SERVED-PASS_BB))
(not (BOARDED-PASS_BB))
)
)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action STOP-PASS_AD-AU
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AU)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action STOP-PASS_AD-AV
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AV)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action STOP-PASS_AK-AU
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AU)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action STOP-PASS_AK-AV
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AV)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action STOP-PASS_AJ-AU
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AU)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action STOP-PASS_AJ-AV
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AV)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action STOP-PASS_AP-AU
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AU)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action STOP-PASS_AP-AV
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AV)
)
:effect
(and
(when
(and
(Flag563-)
)
(and
(BOARDED-PASS_AP)
(not (NOT-BOARDED-PASS_AP))
)
)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action STOP-PASS_AS-AU
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AU)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action STOP-PASS_AS-AV
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AV)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action STOP-PASS_AR-AU
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AU)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action STOP-PASS_AR-AV
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AV)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action STOP-PASS_AT-AU
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AU)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action STOP-PASS_AT-AV
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AV)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action STOP-PASS_AW-AU
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AU)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action STOP-PASS_AW-AV
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AV)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action STOP-PASS_AY-AU
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AU)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action STOP-PASS_AY-AV
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AV)
)
:effect
(and
(when
(and
(Flag558-)
)
(and
(BOARDED-PASS_AY)
(not (NOT-BOARDED-PASS_AY))
)
)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action STOP-PASS_AZ-AU
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AU)
)
:effect
(and
(when
(and
(BOARDED-PASS_AZ)
)
(and
(SERVED-PASS_AZ)
(NOT-BOARDED-PASS_AZ)
(not (NOT-SERVED-PASS_AZ))
(not (BOARDED-PASS_AZ))
)
)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action STOP-PASS_AZ-AV
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AV)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action MOVEUP-AU-AV
:parameters ()
:precondition
(and
(LIFT_AT-AU)
(NOT-ERROR-)
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(NOT-LIFT_AT-AU)
(LIFT_AT-AV)
(CHECKCONSISTENCY-)
(not (LIFT_AT-AU))
(not (NOT-LIFT_AT-AV))
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action MOVEUP-AQ-AU
:parameters ()
:precondition
(and
(LIFT_AT-AQ)
(NOT-ERROR-)
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(NOT-LIFT_AT-AQ)
(LIFT_AT-AU)
(CHECKCONSISTENCY-)
(not (LIFT_AT-AQ))
(not (NOT-LIFT_AT-AU))
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action MOVEDOWN-AQ-AO
:parameters ()
:precondition
(and
(LIFT_AT-AQ)
(NOT-ERROR-)
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(NOT-LIFT_AT-AQ)
(LIFT_AT-AO)
(CHECKCONSISTENCY-)
(not (LIFT_AT-AQ))
(not (NOT-LIFT_AT-AO))
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action MOVEDOWN-AO-AN
:parameters ()
:precondition
(and
(LIFT_AT-AO)
(NOT-ERROR-)
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(NOT-LIFT_AT-AO)
(LIFT_AT-AN)
(CHECKCONSISTENCY-)
(not (LIFT_AT-AO))
(not (NOT-LIFT_AT-AN))
(not (NOT-CHECKCONSISTENCY-))
)
)
(:derived (Flag102-)(and (NOT-CHECKCONSISTENCY-)(NOT-ERROR-)(LIFT_AT-AO)))

(:derived (Flag108-)(and (NOT-CHECKCONSISTENCY-)(NOT-ERROR-)(LIFT_AT-AQ)))

(:action STOP-PASS_BB-AL
:parameters ()
:precondition
(and
(Flag97-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action STOP-PASS_BB-AM
:parameters ()
:precondition
(and
(Flag98-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action STOP-PASS_BB-AN
:parameters ()
:precondition
(and
(Flag109-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action STOP-PASS_AD-AO
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AO)
)
:effect
(and
(when
(and
(BOARDED-PASS_AD)
)
(and
(SERVED-PASS_AD)
(NOT-BOARDED-PASS_AD)
(not (NOT-SERVED-PASS_AD))
(not (BOARDED-PASS_AD))
)
)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action STOP-PASS_AD-AQ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AQ)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action STOP-PASS_AK-AO
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AO)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action STOP-PASS_AK-AQ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AQ)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action STOP-PASS_AJ-AO
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AO)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action STOP-PASS_AJ-AQ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AQ)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action STOP-PASS_AP-AO
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AO)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action STOP-PASS_AP-AQ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AQ)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action STOP-PASS_AS-AO
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AO)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action STOP-PASS_AS-AQ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AQ)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action STOP-PASS_AR-AO
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AO)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action STOP-PASS_AR-AQ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AQ)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action STOP-PASS_AT-AO
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AO)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action STOP-PASS_AT-AQ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AQ)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action STOP-PASS_AW-AO
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AO)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action STOP-PASS_AW-AQ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AQ)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action STOP-PASS_AY-AO
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AO)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action STOP-PASS_AY-AQ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AQ)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action STOP-PASS_AZ-AO
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AO)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action STOP-PASS_AZ-AQ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AQ)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action MOVEUP-AO-AQ
:parameters ()
:precondition
(and
(LIFT_AT-AO)
(NOT-ERROR-)
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(NOT-LIFT_AT-AO)
(LIFT_AT-AQ)
(CHECKCONSISTENCY-)
(not (LIFT_AT-AO))
(not (NOT-LIFT_AT-AQ))
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action MOVEUP-AN-AO
:parameters ()
:precondition
(and
(LIFT_AT-AN)
(NOT-ERROR-)
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(NOT-LIFT_AT-AN)
(LIFT_AT-AO)
(CHECKCONSISTENCY-)
(not (LIFT_AT-AN))
(not (NOT-LIFT_AT-AO))
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action MOVEDOWN-AN-AM
:parameters ()
:precondition
(and
(LIFT_AT-AN)
(NOT-ERROR-)
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(NOT-LIFT_AT-AN)
(LIFT_AT-AM)
(CHECKCONSISTENCY-)
(not (LIFT_AT-AN))
(not (NOT-LIFT_AT-AM))
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action MOVEDOWN-AM-AL
:parameters ()
:precondition
(and
(LIFT_AT-AM)
(NOT-ERROR-)
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(NOT-LIFT_AT-AM)
(LIFT_AT-AL)
(CHECKCONSISTENCY-)
(not (LIFT_AT-AM))
(not (NOT-LIFT_AT-AL))
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action MOVEDOWN-AL-AI
:parameters ()
:precondition
(and
(LIFT_AT-AL)
(NOT-ERROR-)
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(NOT-LIFT_AT-AL)
(LIFT_AT-AI)
(CHECKCONSISTENCY-)
(not (LIFT_AT-AL))
(not (NOT-LIFT_AT-AI))
(not (NOT-CHECKCONSISTENCY-))
)
)
(:derived (Flag97-)(and (NOT-CHECKCONSISTENCY-)(NOT-ERROR-)(LIFT_AT-AL)))

(:derived (Flag98-)(and (NOT-CHECKCONSISTENCY-)(NOT-ERROR-)(LIFT_AT-AM)))

(:derived (Flag109-)(and (NOT-CHECKCONSISTENCY-)(NOT-ERROR-)(LIFT_AT-AN)))

(:action STOP-PASS_BB-AH
:parameters ()
:precondition
(and
(Flag100-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action STOP-PASS_BB-AI
:parameters ()
:precondition
(and
(Flag115-)
)
:effect
(and
(when
(and
(Flag567-)
)
(and
(BOARDED-PASS_BB)
(not (NOT-BOARDED-PASS_BB))
)
)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action STOP-PASS_BB-AG
:parameters ()
:precondition
(and
(Flag117-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action STOP-PASS_AD-AL
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AL)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action STOP-PASS_AD-AM
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AM)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action STOP-PASS_AD-AN
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AN)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action STOP-PASS_AK-AL
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AL)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action STOP-PASS_AK-AM
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AM)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action STOP-PASS_AK-AN
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AN)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action STOP-PASS_AJ-AL
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AL)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action STOP-PASS_AJ-AM
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AM)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action STOP-PASS_AJ-AN
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AN)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action STOP-PASS_AP-AL
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AL)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action STOP-PASS_AP-AM
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AM)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action STOP-PASS_AP-AN
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AN)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action STOP-PASS_AS-AL
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AL)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action STOP-PASS_AS-AM
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AM)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action STOP-PASS_AS-AN
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AN)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action STOP-PASS_AR-AL
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AL)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action STOP-PASS_AR-AM
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AM)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action STOP-PASS_AR-AN
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AN)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action STOP-PASS_AT-AL
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AL)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action STOP-PASS_AT-AM
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AM)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action STOP-PASS_AT-AN
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AN)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action STOP-PASS_AW-AL
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AL)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action STOP-PASS_AW-AM
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AM)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action STOP-PASS_AW-AN
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AN)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action STOP-PASS_AY-AL
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AL)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action STOP-PASS_AY-AM
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AM)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action STOP-PASS_AY-AN
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AN)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action STOP-PASS_AZ-AL
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AL)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action STOP-PASS_AZ-AM
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AM)
)
:effect
(and
(when
(and
(Flag557-)
)
(and
(BOARDED-PASS_AZ)
(not (NOT-BOARDED-PASS_AZ))
)
)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action STOP-PASS_AZ-AN
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AN)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action MOVEUP-AM-AN
:parameters ()
:precondition
(and
(LIFT_AT-AM)
(NOT-ERROR-)
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(NOT-LIFT_AT-AM)
(LIFT_AT-AN)
(CHECKCONSISTENCY-)
(not (LIFT_AT-AM))
(not (NOT-LIFT_AT-AN))
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action MOVEUP-AL-AM
:parameters ()
:precondition
(and
(LIFT_AT-AL)
(NOT-ERROR-)
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(NOT-LIFT_AT-AL)
(LIFT_AT-AM)
(CHECKCONSISTENCY-)
(not (LIFT_AT-AL))
(not (NOT-LIFT_AT-AM))
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action MOVEUP-AI-AL
:parameters ()
:precondition
(and
(LIFT_AT-AI)
(NOT-ERROR-)
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(NOT-LIFT_AT-AI)
(LIFT_AT-AL)
(CHECKCONSISTENCY-)
(not (LIFT_AT-AI))
(not (NOT-LIFT_AT-AL))
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action MOVEDOWN-AI-AH
:parameters ()
:precondition
(and
(LIFT_AT-AI)
(NOT-ERROR-)
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(NOT-LIFT_AT-AI)
(LIFT_AT-AH)
(CHECKCONSISTENCY-)
(not (LIFT_AT-AI))
(not (NOT-LIFT_AT-AH))
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action MOVEDOWN-AH-AG
:parameters ()
:precondition
(and
(LIFT_AT-AH)
(NOT-ERROR-)
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(NOT-LIFT_AT-AH)
(LIFT_AT-AG)
(CHECKCONSISTENCY-)
(not (LIFT_AT-AH))
(not (NOT-LIFT_AT-AG))
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action MOVEDOWN-AG-AF
:parameters ()
:precondition
(and
(LIFT_AT-AG)
(NOT-ERROR-)
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(NOT-LIFT_AT-AG)
(LIFT_AT-AF)
(CHECKCONSISTENCY-)
(not (LIFT_AT-AG))
(not (NOT-LIFT_AT-AF))
(not (NOT-CHECKCONSISTENCY-))
)
)
(:derived (Flag100-)(and (NOT-CHECKCONSISTENCY-)(NOT-ERROR-)(LIFT_AT-AH)))

(:derived (Flag115-)(and (NOT-CHECKCONSISTENCY-)(NOT-ERROR-)(LIFT_AT-AI)))

(:derived (Flag117-)(and (NOT-CHECKCONSISTENCY-)(NOT-ERROR-)(LIFT_AT-AG)))

(:action STOP-PASS_BB-AF
:parameters ()
:precondition
(and
(Flag101-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action STOP-PASS_AD-AH
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AH)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action STOP-PASS_AD-AI
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AI)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action STOP-PASS_AD-AG
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AG)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action STOP-PASS_AK-AH
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AH)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action STOP-PASS_AK-AI
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AI)
)
:effect
(and
(when
(and
(BOARDED-PASS_AK)
)
(and
(SERVED-PASS_AK)
(NOT-BOARDED-PASS_AK)
(not (NOT-SERVED-PASS_AK))
(not (BOARDED-PASS_AK))
)
)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action STOP-PASS_AK-AG
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AG)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action STOP-PASS_AJ-AH
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AH)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action STOP-PASS_AJ-AI
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AI)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action STOP-PASS_AJ-AG
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AG)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action STOP-PASS_AP-AH
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AH)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action STOP-PASS_AP-AI
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AI)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action STOP-PASS_AP-AG
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AG)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action STOP-PASS_AS-AH
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AH)
)
:effect
(and
(when
(and
(BOARDED-PASS_AS)
)
(and
(SERVED-PASS_AS)
(NOT-BOARDED-PASS_AS)
(not (NOT-SERVED-PASS_AS))
(not (BOARDED-PASS_AS))
)
)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action STOP-PASS_AS-AI
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AI)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action STOP-PASS_AS-AG
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AG)
)
:effect
(and
(when
(and
(Flag562-)
)
(and
(BOARDED-PASS_AS)
(not (NOT-BOARDED-PASS_AS))
)
)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action STOP-PASS_AR-AH
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AH)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action STOP-PASS_AR-AI
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AI)
)
:effect
(and
(when
(and
(BOARDED-PASS_AR)
)
(and
(SERVED-PASS_AR)
(NOT-BOARDED-PASS_AR)
(not (NOT-SERVED-PASS_AR))
(not (BOARDED-PASS_AR))
)
)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action STOP-PASS_AR-AG
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AG)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action STOP-PASS_AT-AH
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AH)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action STOP-PASS_AT-AI
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AI)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action STOP-PASS_AT-AG
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AG)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action STOP-PASS_AW-AH
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AH)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action STOP-PASS_AW-AI
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AI)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action STOP-PASS_AW-AG
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AG)
)
:effect
(and
(when
(and
(BOARDED-PASS_AW)
)
(and
(SERVED-PASS_AW)
(NOT-BOARDED-PASS_AW)
(not (NOT-SERVED-PASS_AW))
(not (BOARDED-PASS_AW))
)
)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action STOP-PASS_AY-AH
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AH)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action STOP-PASS_AY-AI
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AI)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action STOP-PASS_AY-AG
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AG)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action STOP-PASS_AZ-AH
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AH)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action STOP-PASS_AZ-AI
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AI)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action STOP-PASS_AZ-AG
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AG)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action MOVEUP-AH-AI
:parameters ()
:precondition
(and
(LIFT_AT-AH)
(NOT-ERROR-)
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(NOT-LIFT_AT-AH)
(LIFT_AT-AI)
(CHECKCONSISTENCY-)
(not (LIFT_AT-AH))
(not (NOT-LIFT_AT-AI))
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action MOVEUP-AG-AH
:parameters ()
:precondition
(and
(LIFT_AT-AG)
(NOT-ERROR-)
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(NOT-LIFT_AT-AG)
(LIFT_AT-AH)
(CHECKCONSISTENCY-)
(not (LIFT_AT-AG))
(not (NOT-LIFT_AT-AH))
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action MOVEUP-AF-AG
:parameters ()
:precondition
(and
(LIFT_AT-AF)
(NOT-ERROR-)
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(NOT-LIFT_AT-AF)
(LIFT_AT-AG)
(CHECKCONSISTENCY-)
(not (LIFT_AT-AF))
(not (NOT-LIFT_AT-AG))
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action MOVEDOWN-AF-AE
:parameters ()
:precondition
(and
(LIFT_AT-AF)
(NOT-ERROR-)
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(NOT-LIFT_AT-AF)
(LIFT_AT-AE)
(CHECKCONSISTENCY-)
(not (LIFT_AT-AF))
(not (NOT-LIFT_AT-AE))
(not (NOT-CHECKCONSISTENCY-))
)
)
(:derived (Flag101-)(and (NOT-CHECKCONSISTENCY-)(NOT-ERROR-)(LIFT_AT-AF)))

(:action STOP-PASS_BB-AE
:parameters ()
:precondition
(and
(Flag104-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action STOP-PASS_BB-AB
:parameters ()
:precondition
(and
(Flag119-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action STOP-PASS_BB-AC
:parameters ()
:precondition
(and
(Flag120-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action STOP-PASS_AD-AF
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AF)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action STOP-PASS_AK-AF
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AF)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action STOP-PASS_AJ-AF
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AF)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action STOP-PASS_AP-AF
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AF)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action STOP-PASS_AS-AF
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AF)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action STOP-PASS_AR-AF
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AF)
)
:effect
(and
(when
(and
(Flag561-)
)
(and
(BOARDED-PASS_AR)
(not (NOT-BOARDED-PASS_AR))
)
)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action STOP-PASS_AT-AF
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AF)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action STOP-PASS_AW-AF
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AF)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action STOP-PASS_AY-AF
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AF)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action STOP-PASS_AZ-AF
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AF)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:derived (Flag2-)(and (Flag1-)))

(:derived (Flag4-)(and (Flag3-)))

(:derived (Flag6-)(and (Flag5-)))

(:derived (Flag8-)(and (Flag7-)))

(:derived (Flag10-)(and (Flag9-)))

(:derived (Flag12-)(and (Flag11-)))

(:derived (Flag14-)(and (Flag13-)))

(:derived (Flag16-)(and (Flag15-)))

(:derived (Flag18-)(and (Flag17-)))

(:derived (Flag20-)(and (Flag19-)))

(:derived (Flag22-)(and (Flag21-)))

(:derived (Flag24-)(and (Flag23-)))

(:derived (Flag26-)(and (Flag25-)))

(:derived (Flag28-)(and (Flag27-)))

(:derived (Flag30-)(and (Flag29-)))

(:derived (Flag32-)(and (Flag31-)))

(:derived (Flag34-)(and (Flag33-)))

(:action MOVEUP-AE-AF
:parameters ()
:precondition
(and
(LIFT_AT-AE)
(NOT-ERROR-)
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(NOT-LIFT_AT-AE)
(LIFT_AT-AF)
(CHECKCONSISTENCY-)
(not (LIFT_AT-AE))
(not (NOT-LIFT_AT-AF))
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action MOVEDOWN-AE-AC
:parameters ()
:precondition
(and
(LIFT_AT-AE)
(NOT-ERROR-)
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(NOT-LIFT_AT-AE)
(LIFT_AT-AC)
(CHECKCONSISTENCY-)
(not (LIFT_AT-AE))
(not (NOT-LIFT_AT-AC))
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action MOVEDOWN-AC-AB
:parameters ()
:precondition
(and
(LIFT_AT-AC)
(NOT-ERROR-)
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(NOT-LIFT_AT-AC)
(LIFT_AT-AB)
(CHECKCONSISTENCY-)
(not (LIFT_AT-AC))
(not (NOT-LIFT_AT-AB))
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action MOVEDOWN-AB-AA
:parameters ()
:precondition
(and
(LIFT_AT-AB)
(NOT-ERROR-)
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(NOT-LIFT_AT-AB)
(LIFT_AT-AA)
(CHECKCONSISTENCY-)
(not (LIFT_AT-AB))
(not (NOT-LIFT_AT-AA))
(not (NOT-CHECKCONSISTENCY-))
)
)
(:derived (Flag104-)(and (NOT-CHECKCONSISTENCY-)(NOT-ERROR-)(LIFT_AT-AE)))

(:derived (Flag119-)(and (NOT-CHECKCONSISTENCY-)(NOT-ERROR-)(LIFT_AT-AB)))

(:derived (Flag120-)(and (NOT-CHECKCONSISTENCY-)(NOT-ERROR-)(LIFT_AT-AC)))

(:action STOP-PASS_BB-AA
:parameters ()
:precondition
(and
(Flag121-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action STOP-PASS_AD-AE
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AE)
)
:effect
(and
(when
(and
(Flag566-)
)
(and
(BOARDED-PASS_AD)
(not (NOT-BOARDED-PASS_AD))
)
)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action STOP-PASS_AD-AB
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AB)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action STOP-PASS_AD-AC
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AC)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action STOP-PASS_AD-AA
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AA)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action STOP-PASS_AK-AE
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AE)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action STOP-PASS_AK-AB
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AB)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action STOP-PASS_AK-AC
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AC)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action STOP-PASS_AK-AA
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AA)
)
:effect
(and
(when
(and
(Flag565-)
)
(and
(BOARDED-PASS_AK)
(not (NOT-BOARDED-PASS_AK))
)
)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action STOP-PASS_AJ-AE
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AE)
)
:effect
(and
(when
(and
(Flag564-)
)
(and
(BOARDED-PASS_AJ)
(not (NOT-BOARDED-PASS_AJ))
)
)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action STOP-PASS_AJ-AB
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AB)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action STOP-PASS_AJ-AC
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AC)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action STOP-PASS_AJ-AA
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AA)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action STOP-PASS_AP-AE
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AE)
)
:effect
(and
(when
(and
(BOARDED-PASS_AP)
)
(and
(SERVED-PASS_AP)
(NOT-BOARDED-PASS_AP)
(not (NOT-SERVED-PASS_AP))
(not (BOARDED-PASS_AP))
)
)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action STOP-PASS_AP-AB
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AB)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action STOP-PASS_AP-AC
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AC)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action STOP-PASS_AP-AA
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AA)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action STOP-PASS_AS-AE
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AE)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action STOP-PASS_AS-AB
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AB)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action STOP-PASS_AS-AC
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AC)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action STOP-PASS_AS-AA
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AA)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action STOP-PASS_AR-AE
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AE)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action STOP-PASS_AR-AB
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AB)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action STOP-PASS_AR-AC
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AC)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action STOP-PASS_AR-AA
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AA)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action STOP-PASS_AT-AE
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AE)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action STOP-PASS_AT-AB
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AB)
)
:effect
(and
(when
(and
(BOARDED-PASS_AT)
)
(and
(SERVED-PASS_AT)
(NOT-BOARDED-PASS_AT)
(not (NOT-SERVED-PASS_AT))
(not (BOARDED-PASS_AT))
)
)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action STOP-PASS_AT-AC
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AC)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action STOP-PASS_AT-AA
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AA)
)
:effect
(and
(when
(and
(Flag560-)
)
(and
(BOARDED-PASS_AT)
(not (NOT-BOARDED-PASS_AT))
)
)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action STOP-PASS_AW-AE
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AE)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action STOP-PASS_AW-AB
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AB)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action STOP-PASS_AW-AC
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AC)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action STOP-PASS_AW-AA
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AA)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action STOP-PASS_AY-AE
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AE)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action STOP-PASS_AY-AB
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AB)
)
:effect
(and
(when
(and
(BOARDED-PASS_AY)
)
(and
(SERVED-PASS_AY)
(NOT-BOARDED-PASS_AY)
(not (NOT-SERVED-PASS_AY))
(not (BOARDED-PASS_AY))
)
)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action STOP-PASS_AY-AC
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AC)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action STOP-PASS_AY-AA
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AA)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action STOP-PASS_AZ-AE
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AE)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action STOP-PASS_AZ-AB
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AB)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action STOP-PASS_AZ-AC
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AC)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action STOP-PASS_AZ-AA
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AA)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:derived (Flag1-)(and ))

(:derived (Flag3-)(and ))

(:derived (Flag5-)(and ))

(:derived (Flag7-)(and ))

(:derived (Flag9-)(and ))

(:derived (Flag11-)(and ))

(:derived (Flag13-)(and ))

(:derived (Flag15-)(and ))

(:derived (Flag17-)(and ))

(:derived (Flag19-)(and ))

(:derived (Flag21-)(and ))

(:derived (Flag23-)(and ))

(:derived (Flag25-)(and ))

(:derived (Flag27-)(and ))

(:derived (Flag29-)(and ))

(:derived (Flag31-)(and ))

(:derived (Flag33-)(and ))

(:action CHECKCONSISTENCYACTION
:parameters ()
:precondition
(and
(NOT-ERROR-)
(CHECKCONSISTENCY-)
)
:effect
(and
(NOT-CHECKCONSISTENCY-)
(not (CHECKCONSISTENCY-))
)
)
(:action MOVEUP-AC-AE
:parameters ()
:precondition
(and
(LIFT_AT-AC)
(NOT-ERROR-)
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(NOT-LIFT_AT-AC)
(LIFT_AT-AE)
(CHECKCONSISTENCY-)
(not (LIFT_AT-AC))
(not (NOT-LIFT_AT-AE))
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action MOVEUP-AB-AC
:parameters ()
:precondition
(and
(LIFT_AT-AB)
(NOT-ERROR-)
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(NOT-LIFT_AT-AB)
(LIFT_AT-AC)
(CHECKCONSISTENCY-)
(not (LIFT_AT-AB))
(not (NOT-LIFT_AT-AC))
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action MOVEUP-AA-AB
:parameters ()
:precondition
(and
(LIFT_AT-AA)
(NOT-ERROR-)
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(NOT-LIFT_AT-AA)
(LIFT_AT-AB)
(CHECKCONSISTENCY-)
(not (LIFT_AT-AA))
(not (NOT-LIFT_AT-AB))
(not (NOT-CHECKCONSISTENCY-))
)
)
(:derived (Flag121-)(and (NOT-CHECKCONSISTENCY-)(NOT-ERROR-)(LIFT_AT-AA)))

(:derived (Flag557-)(and (NOT-SERVED-PASS_AZ)(NOT-BOARDED-PASS_AZ)))

(:derived (Flag558-)(and (NOT-SERVED-PASS_AY)(NOT-BOARDED-PASS_AY)))

(:derived (Flag559-)(and (NOT-SERVED-PASS_AW)(NOT-BOARDED-PASS_AW)))

(:derived (Flag560-)(and (NOT-SERVED-PASS_AT)(NOT-BOARDED-PASS_AT)))

(:derived (Flag561-)(and (NOT-SERVED-PASS_AR)(NOT-BOARDED-PASS_AR)))

(:derived (Flag562-)(and (NOT-SERVED-PASS_AS)(NOT-BOARDED-PASS_AS)))

(:derived (Flag563-)(and (NOT-SERVED-PASS_AP)(NOT-BOARDED-PASS_AP)))

(:derived (Flag564-)(and (NOT-SERVED-PASS_AJ)(NOT-BOARDED-PASS_AJ)))

(:derived (Flag565-)(and (NOT-SERVED-PASS_AK)(NOT-BOARDED-PASS_AK)))

(:derived (Flag566-)(and (NOT-SERVED-PASS_AD)(NOT-BOARDED-PASS_AD)))

(:derived (Flag567-)(and (NOT-SERVED-PASS_BB)(NOT-BOARDED-PASS_BB)))

)
