(define (domain grounded-SIMPLE-ADL-ELEVATORS)
(:requirements
:strips
)
(:predicates
(Flag291-)
(Flag290-)
(Flag289-)
(Flag288-)
(Flag287-)
(Flag286-)
(Flag285-)
(Flag37-)
(NOT-LIFT_AT-M)
(LIFT_AT-J)
(CHECKCONSISTENCY-)
(LIFT_AT-H)
(LIFT_AT-N)
(ERROR-)
(Flag23-)
(Flag21-)
(Flag19-)
(Flag17-)
(Flag15-)
(Flag13-)
(Flag11-)
(Flag9-)
(Flag7-)
(Flag5-)
(Flag3-)
(Flag1-)
(SERVED-PASS_S)
(BOARDED-PASS_A)
(SERVED-PASS_A)
(BOARDED-PASS_C)
(SERVED-PASS_L)
(BOARDED-PASS_L)
(SERVED-PASS_I)
(BOARDED-PASS_I)
(Flag39-)
(Flag35-)
(Flag29-)
(LIFT_AT-G)
(LIFT_AT-F)
(LIFT_AT-O)
(LIFT_AT-P)
(Flag24-)
(Flag22-)
(Flag20-)
(Flag18-)
(Flag16-)
(Flag14-)
(Flag12-)
(Flag10-)
(Flag8-)
(Flag6-)
(Flag4-)
(Flag2-)
(BOARDED-PASS_R)
(SERVED-PASS_R)
(BOARDED-PASS_S)
(Flag38-)
(Flag36-)
(Flag33-)
(Flag28-)
(LIFT_AT-E)
(LIFT_AT-D)
(LIFT_AT-Q)
(SERVED-PASS_C)
(SERVED-PASS_K)
(Flag43-)
(Flag42-)
(Flag34-)
(LIFT_AT-B)
(Flag25-)
(BOARDED-PASS_K)
(Flag44-)
(NOT-CHECKCONSISTENCY-)
(NOT-BOARDED-PASS_K)
(NOT-LIFT_AT-D)
(NOT-LIFT_AT-B)
(NOT-LIFT_AT-P)
(NOT-SERVED-PASS_K)
(NOT-SERVED-PASS_C)
(NOT-LIFT_AT-E)
(NOT-LIFT_AT-Q)
(NOT-LIFT_AT-F)
(NOT-LIFT_AT-O)
(NOT-LIFT_AT-N)
(NOT-BOARDED-PASS_S)
(NOT-SERVED-PASS_R)
(NOT-BOARDED-PASS_R)
(NOT-LIFT_AT-G)
(NOT-LIFT_AT-H)
(NOT-SERVED-PASS_I)
(NOT-BOARDED-PASS_I)
(NOT-SERVED-PASS_L)
(NOT-BOARDED-PASS_L)
(NOT-BOARDED-PASS_C)
(NOT-SERVED-PASS_A)
(NOT-BOARDED-PASS_A)
(NOT-SERVED-PASS_S)
(NOT-ERROR-)
(NOT-LIFT_AT-J)
(LIFT_AT-M)
)
(:action STOP-PASS_K-B
:parameters ()
:precondition
(and
(Flag44-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:derived (Flag44-)(and (NOT-CHECKCONSISTENCY-)(NOT-ERROR-)(LIFT_AT-B)))

(:action STOP-PASS_K-Q
:parameters ()
:precondition
(and
(Flag34-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action STOP-PASS_K-D
:parameters ()
:precondition
(and
(Flag42-)
)
:effect
(and
(when
(and
(Flag291-)
)
(and
(BOARDED-PASS_K)
(not (NOT-BOARDED-PASS_K))
)
)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action STOP-PASS_K-E
:parameters ()
:precondition
(and
(Flag43-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action STOP-PASS_I-B
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-B)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action STOP-PASS_L-B
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-B)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action STOP-PASS_C-B
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-B)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action STOP-PASS_A-B
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-B)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action STOP-PASS_S-B
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-B)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action STOP-PASS_R-B
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-B)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:derived (Flag25-)(and (Flag2-)(Flag4-)(Flag6-)(SERVED-PASS_R)(SERVED-PASS_S)(Flag8-)(Flag10-)(Flag12-)(Flag14-)(Flag16-)(Flag18-)(Flag20-)(SERVED-PASS_A)(SERVED-PASS_C)(SERVED-PASS_L)(Flag22-)(Flag24-)(SERVED-PASS_I)(SERVED-PASS_K)(NOT-ERROR-)(NOT-CHECKCONSISTENCY-)))

(:action MOVEUP-B-D
:parameters ()
:precondition
(and
(LIFT_AT-B)
(NOT-ERROR-)
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(NOT-LIFT_AT-B)
(LIFT_AT-D)
(CHECKCONSISTENCY-)
(not (LIFT_AT-B))
(not (NOT-LIFT_AT-D))
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action MOVEDOWN-D-B
:parameters ()
:precondition
(and
(LIFT_AT-D)
(NOT-ERROR-)
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(NOT-LIFT_AT-D)
(LIFT_AT-B)
(CHECKCONSISTENCY-)
(not (LIFT_AT-D))
(not (NOT-LIFT_AT-B))
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action MOVEDOWN-Q-P
:parameters ()
:precondition
(and
(LIFT_AT-Q)
(NOT-ERROR-)
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(NOT-LIFT_AT-Q)
(LIFT_AT-P)
(CHECKCONSISTENCY-)
(not (LIFT_AT-Q))
(not (NOT-LIFT_AT-P))
(not (NOT-CHECKCONSISTENCY-))
)
)
(:derived (Flag34-)(and (NOT-CHECKCONSISTENCY-)(NOT-ERROR-)(LIFT_AT-Q)))

(:derived (Flag42-)(and (NOT-CHECKCONSISTENCY-)(NOT-ERROR-)(LIFT_AT-D)))

(:derived (Flag43-)(and (NOT-CHECKCONSISTENCY-)(NOT-ERROR-)(LIFT_AT-E)))

(:action STOP-PASS_K-P
:parameters ()
:precondition
(and
(Flag28-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action STOP-PASS_K-G
:parameters ()
:precondition
(and
(Flag33-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action STOP-PASS_K-O
:parameters ()
:precondition
(and
(Flag36-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action STOP-PASS_K-F
:parameters ()
:precondition
(and
(Flag38-)
)
:effect
(and
(when
(and
(BOARDED-PASS_K)
)
(and
(SERVED-PASS_K)
(NOT-BOARDED-PASS_K)
(not (NOT-SERVED-PASS_K))
(not (BOARDED-PASS_K))
)
)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action STOP-PASS_I-Q
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-Q)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action STOP-PASS_I-D
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-D)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action STOP-PASS_I-E
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-E)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action STOP-PASS_L-Q
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-Q)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action STOP-PASS_L-D
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-D)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action STOP-PASS_L-E
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-E)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action STOP-PASS_C-Q
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-Q)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action STOP-PASS_C-D
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-D)
)
:effect
(and
(when
(and
(BOARDED-PASS_C)
)
(and
(SERVED-PASS_C)
(NOT-BOARDED-PASS_C)
(not (NOT-SERVED-PASS_C))
(not (BOARDED-PASS_C))
)
)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action STOP-PASS_C-E
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-E)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action STOP-PASS_A-Q
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-Q)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action STOP-PASS_A-D
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-D)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action STOP-PASS_A-E
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-E)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action STOP-PASS_S-Q
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-Q)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action STOP-PASS_S-D
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-D)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action STOP-PASS_S-E
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-E)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action STOP-PASS_R-Q
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-Q)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action STOP-PASS_R-D
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-D)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action STOP-PASS_R-E
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-E)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action MOVEUP-D-E
:parameters ()
:precondition
(and
(LIFT_AT-D)
(NOT-ERROR-)
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(NOT-LIFT_AT-D)
(LIFT_AT-E)
(CHECKCONSISTENCY-)
(not (LIFT_AT-D))
(not (NOT-LIFT_AT-E))
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action MOVEUP-P-Q
:parameters ()
:precondition
(and
(LIFT_AT-P)
(NOT-ERROR-)
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(NOT-LIFT_AT-P)
(LIFT_AT-Q)
(CHECKCONSISTENCY-)
(not (LIFT_AT-P))
(not (NOT-LIFT_AT-Q))
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action MOVEUP-E-F
:parameters ()
:precondition
(and
(LIFT_AT-E)
(NOT-ERROR-)
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(NOT-LIFT_AT-E)
(LIFT_AT-F)
(CHECKCONSISTENCY-)
(not (LIFT_AT-E))
(not (NOT-LIFT_AT-F))
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action MOVEDOWN-E-D
:parameters ()
:precondition
(and
(LIFT_AT-E)
(NOT-ERROR-)
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(NOT-LIFT_AT-E)
(LIFT_AT-D)
(CHECKCONSISTENCY-)
(not (LIFT_AT-E))
(not (NOT-LIFT_AT-D))
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action MOVEDOWN-P-O
:parameters ()
:precondition
(and
(LIFT_AT-P)
(NOT-ERROR-)
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(NOT-LIFT_AT-P)
(LIFT_AT-O)
(CHECKCONSISTENCY-)
(not (LIFT_AT-P))
(not (NOT-LIFT_AT-O))
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action MOVEDOWN-O-N
:parameters ()
:precondition
(and
(LIFT_AT-O)
(NOT-ERROR-)
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(NOT-LIFT_AT-O)
(LIFT_AT-N)
(CHECKCONSISTENCY-)
(not (LIFT_AT-O))
(not (NOT-LIFT_AT-N))
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action MOVEDOWN-F-E
:parameters ()
:precondition
(and
(LIFT_AT-F)
(NOT-ERROR-)
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(NOT-LIFT_AT-F)
(LIFT_AT-E)
(CHECKCONSISTENCY-)
(not (LIFT_AT-F))
(not (NOT-LIFT_AT-E))
(not (NOT-CHECKCONSISTENCY-))
)
)
(:derived (Flag28-)(and (NOT-CHECKCONSISTENCY-)(NOT-ERROR-)(LIFT_AT-P)))

(:derived (Flag33-)(and (NOT-CHECKCONSISTENCY-)(NOT-ERROR-)(LIFT_AT-G)))

(:derived (Flag36-)(and (NOT-CHECKCONSISTENCY-)(NOT-ERROR-)(LIFT_AT-O)))

(:derived (Flag38-)(and (NOT-CHECKCONSISTENCY-)(NOT-ERROR-)(LIFT_AT-F)))

(:action STOP-PASS_K-N
:parameters ()
:precondition
(and
(Flag29-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action STOP-PASS_K-J
:parameters ()
:precondition
(and
(Flag35-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action STOP-PASS_K-H
:parameters ()
:precondition
(and
(Flag39-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action STOP-PASS_I-P
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-P)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action STOP-PASS_I-G
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-G)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action STOP-PASS_I-O
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-O)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action STOP-PASS_I-F
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-F)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action STOP-PASS_L-P
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-P)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action STOP-PASS_L-G
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-G)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action STOP-PASS_L-O
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-O)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action STOP-PASS_L-F
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-F)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action STOP-PASS_C-P
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-P)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action STOP-PASS_C-G
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-G)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action STOP-PASS_C-O
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-O)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action STOP-PASS_C-F
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-F)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action STOP-PASS_A-P
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-P)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action STOP-PASS_A-G
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-G)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action STOP-PASS_A-O
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-O)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action STOP-PASS_A-F
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-F)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action STOP-PASS_S-P
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-P)
)
:effect
(and
(when
(and
(Flag286-)
)
(and
(BOARDED-PASS_S)
(not (NOT-BOARDED-PASS_S))
)
)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action STOP-PASS_S-G
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-G)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action STOP-PASS_S-O
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-O)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action STOP-PASS_S-F
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-F)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action STOP-PASS_R-P
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-P)
)
:effect
(and
(when
(and
(BOARDED-PASS_R)
)
(and
(SERVED-PASS_R)
(NOT-BOARDED-PASS_R)
(not (NOT-SERVED-PASS_R))
(not (BOARDED-PASS_R))
)
)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action STOP-PASS_R-G
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-G)
)
:effect
(and
(when
(and
(Flag285-)
)
(and
(BOARDED-PASS_R)
(not (NOT-BOARDED-PASS_R))
)
)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action STOP-PASS_R-O
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-O)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action STOP-PASS_R-F
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-F)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:derived (Flag2-)(and (Flag1-)))

(:derived (Flag4-)(and (Flag3-)))

(:derived (Flag6-)(and (Flag5-)))

(:derived (Flag8-)(and (Flag7-)))

(:derived (Flag10-)(and (Flag9-)))

(:derived (Flag12-)(and (Flag11-)))

(:derived (Flag14-)(and (Flag13-)))

(:derived (Flag16-)(and (Flag15-)))

(:derived (Flag18-)(and (Flag17-)))

(:derived (Flag20-)(and (Flag19-)))

(:derived (Flag22-)(and (Flag21-)))

(:derived (Flag24-)(and (Flag23-)))

(:action MOVEUP-F-G
:parameters ()
:precondition
(and
(LIFT_AT-F)
(NOT-ERROR-)
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(NOT-LIFT_AT-F)
(LIFT_AT-G)
(CHECKCONSISTENCY-)
(not (LIFT_AT-F))
(not (NOT-LIFT_AT-G))
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action MOVEUP-O-P
:parameters ()
:precondition
(and
(LIFT_AT-O)
(NOT-ERROR-)
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(NOT-LIFT_AT-O)
(LIFT_AT-P)
(CHECKCONSISTENCY-)
(not (LIFT_AT-O))
(not (NOT-LIFT_AT-P))
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action MOVEUP-N-O
:parameters ()
:precondition
(and
(LIFT_AT-N)
(NOT-ERROR-)
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(NOT-LIFT_AT-N)
(LIFT_AT-O)
(CHECKCONSISTENCY-)
(not (LIFT_AT-N))
(not (NOT-LIFT_AT-O))
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action MOVEUP-G-H
:parameters ()
:precondition
(and
(LIFT_AT-G)
(NOT-ERROR-)
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(NOT-LIFT_AT-G)
(LIFT_AT-H)
(CHECKCONSISTENCY-)
(not (LIFT_AT-G))
(not (NOT-LIFT_AT-H))
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action MOVEDOWN-G-F
:parameters ()
:precondition
(and
(LIFT_AT-G)
(NOT-ERROR-)
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(NOT-LIFT_AT-G)
(LIFT_AT-F)
(CHECKCONSISTENCY-)
(not (LIFT_AT-G))
(not (NOT-LIFT_AT-F))
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action MOVEDOWN-N-M
:parameters ()
:precondition
(and
(LIFT_AT-N)
(NOT-ERROR-)
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(NOT-LIFT_AT-N)
(LIFT_AT-M)
(CHECKCONSISTENCY-)
(not (LIFT_AT-N))
(not (NOT-LIFT_AT-M))
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action MOVEDOWN-H-G
:parameters ()
:precondition
(and
(LIFT_AT-H)
(NOT-ERROR-)
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(NOT-LIFT_AT-H)
(LIFT_AT-G)
(CHECKCONSISTENCY-)
(not (LIFT_AT-H))
(not (NOT-LIFT_AT-G))
(not (NOT-CHECKCONSISTENCY-))
)
)
(:derived (Flag29-)(and (NOT-CHECKCONSISTENCY-)(NOT-ERROR-)(LIFT_AT-N)))

(:derived (Flag35-)(and (NOT-CHECKCONSISTENCY-)(NOT-ERROR-)(LIFT_AT-J)))

(:derived (Flag39-)(and (NOT-CHECKCONSISTENCY-)(NOT-ERROR-)(LIFT_AT-H)))

(:action STOP-PASS_K-M
:parameters ()
:precondition
(and
(Flag37-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action STOP-PASS_I-N
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-N)
)
:effect
(and
(when
(and
(BOARDED-PASS_I)
)
(and
(SERVED-PASS_I)
(NOT-BOARDED-PASS_I)
(not (NOT-SERVED-PASS_I))
(not (BOARDED-PASS_I))
)
)
(when
(and
(Flag290-)
)
(and
(BOARDED-PASS_I)
(not (NOT-BOARDED-PASS_I))
)
)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action STOP-PASS_I-J
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-J)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action STOP-PASS_I-M
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-M)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action STOP-PASS_I-H
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-H)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action STOP-PASS_L-N
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-N)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action STOP-PASS_L-J
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-J)
)
:effect
(and
(when
(and
(BOARDED-PASS_L)
)
(and
(SERVED-PASS_L)
(NOT-BOARDED-PASS_L)
(not (NOT-SERVED-PASS_L))
(not (BOARDED-PASS_L))
)
)
(when
(and
(Flag289-)
)
(and
(BOARDED-PASS_L)
(not (NOT-BOARDED-PASS_L))
)
)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action STOP-PASS_L-M
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-M)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action STOP-PASS_L-H
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-H)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action STOP-PASS_C-N
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-N)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action STOP-PASS_C-J
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-J)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action STOP-PASS_C-M
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-M)
)
:effect
(and
(when
(and
(Flag288-)
)
(and
(BOARDED-PASS_C)
(not (NOT-BOARDED-PASS_C))
)
)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action STOP-PASS_C-H
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-H)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action STOP-PASS_A-N
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-N)
)
:effect
(and
(when
(and
(BOARDED-PASS_A)
)
(and
(SERVED-PASS_A)
(NOT-BOARDED-PASS_A)
(not (NOT-SERVED-PASS_A))
(not (BOARDED-PASS_A))
)
)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action STOP-PASS_A-J
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-J)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action STOP-PASS_A-M
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-M)
)
:effect
(and
(when
(and
(Flag287-)
)
(and
(BOARDED-PASS_A)
(not (NOT-BOARDED-PASS_A))
)
)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action STOP-PASS_A-H
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-H)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action STOP-PASS_S-N
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-N)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action STOP-PASS_S-J
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-J)
)
:effect
(and
(when
(and
(BOARDED-PASS_S)
)
(and
(SERVED-PASS_S)
(NOT-BOARDED-PASS_S)
(not (NOT-SERVED-PASS_S))
(not (BOARDED-PASS_S))
)
)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action STOP-PASS_S-M
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-M)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action STOP-PASS_S-H
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-H)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action STOP-PASS_R-N
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-N)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action STOP-PASS_R-J
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-J)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action STOP-PASS_R-M
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-M)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action STOP-PASS_R-H
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-H)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:derived (Flag1-)(and ))

(:derived (Flag3-)(and ))

(:derived (Flag5-)(and ))

(:derived (Flag7-)(and ))

(:derived (Flag9-)(and ))

(:derived (Flag11-)(and ))

(:derived (Flag13-)(and ))

(:derived (Flag15-)(and ))

(:derived (Flag17-)(and ))

(:derived (Flag19-)(and ))

(:derived (Flag21-)(and ))

(:derived (Flag23-)(and ))

(:action CHECKCONSISTENCYACTION
:parameters ()
:precondition
(and
(NOT-ERROR-)
(CHECKCONSISTENCY-)
)
:effect
(and
(NOT-CHECKCONSISTENCY-)
(not (CHECKCONSISTENCY-))
)
)
(:action MOVEUP-H-J
:parameters ()
:precondition
(and
(LIFT_AT-H)
(NOT-ERROR-)
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(NOT-LIFT_AT-H)
(LIFT_AT-J)
(CHECKCONSISTENCY-)
(not (LIFT_AT-H))
(not (NOT-LIFT_AT-J))
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action MOVEUP-M-N
:parameters ()
:precondition
(and
(LIFT_AT-M)
(NOT-ERROR-)
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(NOT-LIFT_AT-M)
(LIFT_AT-N)
(CHECKCONSISTENCY-)
(not (LIFT_AT-M))
(not (NOT-LIFT_AT-N))
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action MOVEUP-J-M
:parameters ()
:precondition
(and
(LIFT_AT-J)
(NOT-ERROR-)
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(NOT-LIFT_AT-J)
(LIFT_AT-M)
(CHECKCONSISTENCY-)
(not (LIFT_AT-J))
(not (NOT-LIFT_AT-M))
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action MOVEDOWN-J-H
:parameters ()
:precondition
(and
(LIFT_AT-J)
(NOT-ERROR-)
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(NOT-LIFT_AT-J)
(LIFT_AT-H)
(CHECKCONSISTENCY-)
(not (LIFT_AT-J))
(not (NOT-LIFT_AT-H))
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action MOVEDOWN-M-J
:parameters ()
:precondition
(and
(LIFT_AT-M)
(NOT-ERROR-)
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(NOT-LIFT_AT-M)
(LIFT_AT-J)
(CHECKCONSISTENCY-)
(not (LIFT_AT-M))
(not (NOT-LIFT_AT-J))
(not (NOT-CHECKCONSISTENCY-))
)
)
(:derived (Flag37-)(and (NOT-CHECKCONSISTENCY-)(NOT-ERROR-)(LIFT_AT-M)))

(:derived (Flag285-)(and (NOT-BOARDED-PASS_R)(NOT-SERVED-PASS_R)))

(:derived (Flag286-)(and (NOT-BOARDED-PASS_S)(NOT-SERVED-PASS_S)))

(:derived (Flag287-)(and (NOT-BOARDED-PASS_A)(NOT-SERVED-PASS_A)))

(:derived (Flag288-)(and (NOT-BOARDED-PASS_C)(NOT-SERVED-PASS_C)))

(:derived (Flag289-)(and (NOT-BOARDED-PASS_L)(NOT-SERVED-PASS_L)))

(:derived (Flag290-)(and (NOT-BOARDED-PASS_I)(NOT-SERVED-PASS_I)))

(:derived (Flag291-)(and (NOT-BOARDED-PASS_K)(NOT-SERVED-PASS_K)))

)
