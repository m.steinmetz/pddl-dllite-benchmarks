(define (domain grounded-SIMPLE-ADL-ELEVATORS)
(:requirements
:strips
)
(:predicates
(Flag381-)
(Flag380-)
(Flag379-)
(Flag378-)
(Flag377-)
(Flag376-)
(Flag375-)
(Flag374-)
(Flag356-)
(NOT-LIFT_AT-N)
(LIFT_AT-M)
(CHECKCONSISTENCY-)
(LIFT_AT-P)
(ERROR-)
(Flag27-)
(Flag25-)
(Flag23-)
(Flag21-)
(Flag19-)
(Flag17-)
(Flag15-)
(Flag13-)
(Flag11-)
(Flag9-)
(Flag7-)
(Flag5-)
(Flag3-)
(Flag1-)
(BOARDED-PASS_U)
(BOARDED-PASS_V)
(SERVED-PASS_C)
(SERVED-PASS_O)
(BOARDED-PASS_O)
(Flag357-)
(Flag354-)
(LIFT_AT-K)
(LIFT_AT-J)
(LIFT_AT-Q)
(LIFT_AT-S)
(LIFT_AT-T)
(Flag28-)
(Flag26-)
(Flag24-)
(Flag22-)
(Flag20-)
(Flag18-)
(Flag16-)
(Flag14-)
(Flag12-)
(Flag10-)
(Flag8-)
(Flag6-)
(Flag4-)
(Flag2-)
(SERVED-PASS_V)
(BOARDED-PASS_R)
(SERVED-PASS_B)
(SERVED-PASS_L)
(Flag359-)
(Flag358-)
(Flag355-)
(Flag353-)
(Flag352-)
(LIFT_AT-I)
(LIFT_AT-H)
(Flag361-)
(Flag360-)
(LIFT_AT-G)
(LIFT_AT-F)
(SERVED-PASS_U)
(SERVED-PASS_D)
(BOARDED-PASS_D)
(Flag363-)
(Flag362-)
(LIFT_AT-E)
(LIFT_AT-A)
(SERVED-PASS_R)
(BOARDED-PASS_B)
(BOARDED-PASS_C)
(BOARDED-PASS_L)
(Flag365-)
(Flag364-)
(Flag29-)
(NOT-CHECKCONSISTENCY-)
(NOT-BOARDED-PASS_L)
(NOT-BOARDED-PASS_C)
(NOT-BOARDED-PASS_B)
(NOT-SERVED-PASS_R)
(NOT-LIFT_AT-E)
(NOT-LIFT_AT-F)
(NOT-LIFT_AT-A)
(NOT-SERVED-PASS_D)
(NOT-BOARDED-PASS_D)
(NOT-SERVED-PASS_U)
(NOT-LIFT_AT-G)
(NOT-LIFT_AT-H)
(NOT-LIFT_AT-I)
(NOT-LIFT_AT-J)
(NOT-LIFT_AT-S)
(NOT-LIFT_AT-Q)
(NOT-LIFT_AT-P)
(NOT-SERVED-PASS_L)
(NOT-SERVED-PASS_B)
(NOT-BOARDED-PASS_R)
(NOT-SERVED-PASS_V)
(NOT-LIFT_AT-K)
(NOT-LIFT_AT-T)
(NOT-LIFT_AT-M)
(NOT-SERVED-PASS_O)
(NOT-BOARDED-PASS_O)
(NOT-SERVED-PASS_C)
(NOT-BOARDED-PASS_V)
(NOT-BOARDED-PASS_U)
(NOT-ERROR-)
(LIFT_AT-N)
)
(:action STOP-PASS_O-E
:parameters ()
:precondition
(and
(Flag364-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action STOP-PASS_O-A
:parameters ()
:precondition
(and
(Flag365-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:derived (Flag29-)(and (SERVED-PASS_U)(SERVED-PASS_V)(SERVED-PASS_R)(SERVED-PASS_D)(SERVED-PASS_B)(SERVED-PASS_C)(SERVED-PASS_L)(SERVED-PASS_O)(Flag2-)(Flag4-)(Flag6-)(Flag8-)(Flag10-)(Flag12-)(Flag14-)(Flag16-)(Flag18-)(Flag20-)(Flag22-)(Flag24-)(Flag26-)(Flag28-)(NOT-ERROR-)(NOT-CHECKCONSISTENCY-)))

(:derived (Flag364-)(and (NOT-CHECKCONSISTENCY-)(NOT-ERROR-)(LIFT_AT-E)))

(:derived (Flag365-)(and (NOT-CHECKCONSISTENCY-)(NOT-ERROR-)(LIFT_AT-A)))

(:action STOP-PASS_O-F
:parameters ()
:precondition
(and
(Flag362-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action STOP-PASS_O-G
:parameters ()
:precondition
(and
(Flag363-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action STOP-PASS_L-E
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-E)
)
:effect
(and
(when
(and
(Flag380-)
)
(and
(BOARDED-PASS_L)
(not (NOT-BOARDED-PASS_L))
)
)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action STOP-PASS_L-A
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-A)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action STOP-PASS_C-E
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-E)
)
:effect
(and
(when
(and
(Flag379-)
)
(and
(BOARDED-PASS_C)
(not (NOT-BOARDED-PASS_C))
)
)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action STOP-PASS_C-A
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-A)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action STOP-PASS_B-E
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-E)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action STOP-PASS_B-A
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-A)
)
:effect
(and
(when
(and
(Flag378-)
)
(and
(BOARDED-PASS_B)
(not (NOT-BOARDED-PASS_B))
)
)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action STOP-PASS_D-E
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-E)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action STOP-PASS_D-A
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-A)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action STOP-PASS_R-E
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-E)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action STOP-PASS_R-A
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-A)
)
:effect
(and
(when
(and
(BOARDED-PASS_R)
)
(and
(SERVED-PASS_R)
(NOT-BOARDED-PASS_R)
(not (NOT-SERVED-PASS_R))
(not (BOARDED-PASS_R))
)
)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action STOP-PASS_V-E
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-E)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action STOP-PASS_V-A
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-A)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action STOP-PASS_U-E
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-E)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action STOP-PASS_U-A
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-A)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action MOVEUP-A-E
:parameters ()
:precondition
(and
(LIFT_AT-A)
(NOT-ERROR-)
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(NOT-LIFT_AT-A)
(LIFT_AT-E)
(CHECKCONSISTENCY-)
(not (LIFT_AT-A))
(not (NOT-LIFT_AT-E))
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action MOVEUP-E-F
:parameters ()
:precondition
(and
(LIFT_AT-E)
(NOT-ERROR-)
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(NOT-LIFT_AT-E)
(LIFT_AT-F)
(CHECKCONSISTENCY-)
(not (LIFT_AT-E))
(not (NOT-LIFT_AT-F))
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action MOVEDOWN-E-A
:parameters ()
:precondition
(and
(LIFT_AT-E)
(NOT-ERROR-)
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(NOT-LIFT_AT-E)
(LIFT_AT-A)
(CHECKCONSISTENCY-)
(not (LIFT_AT-E))
(not (NOT-LIFT_AT-A))
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action MOVEDOWN-F-E
:parameters ()
:precondition
(and
(LIFT_AT-F)
(NOT-ERROR-)
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(NOT-LIFT_AT-F)
(LIFT_AT-E)
(CHECKCONSISTENCY-)
(not (LIFT_AT-F))
(not (NOT-LIFT_AT-E))
(not (NOT-CHECKCONSISTENCY-))
)
)
(:derived (Flag362-)(and (NOT-CHECKCONSISTENCY-)(NOT-ERROR-)(LIFT_AT-F)))

(:derived (Flag363-)(and (NOT-CHECKCONSISTENCY-)(NOT-ERROR-)(LIFT_AT-G)))

(:action STOP-PASS_O-H
:parameters ()
:precondition
(and
(Flag360-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action STOP-PASS_O-I
:parameters ()
:precondition
(and
(Flag361-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action STOP-PASS_L-F
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-F)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action STOP-PASS_L-G
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-G)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action STOP-PASS_C-F
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-F)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action STOP-PASS_C-G
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-G)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action STOP-PASS_B-F
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-F)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action STOP-PASS_B-G
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-G)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action STOP-PASS_D-F
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-F)
)
:effect
(and
(when
(and
(BOARDED-PASS_D)
)
(and
(SERVED-PASS_D)
(NOT-BOARDED-PASS_D)
(not (NOT-SERVED-PASS_D))
(not (BOARDED-PASS_D))
)
)
(when
(and
(Flag377-)
)
(and
(BOARDED-PASS_D)
(not (NOT-BOARDED-PASS_D))
)
)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action STOP-PASS_D-G
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-G)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action STOP-PASS_R-F
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-F)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action STOP-PASS_R-G
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-G)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action STOP-PASS_V-F
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-F)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action STOP-PASS_V-G
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-G)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action STOP-PASS_U-F
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-F)
)
:effect
(and
(when
(and
(BOARDED-PASS_U)
)
(and
(SERVED-PASS_U)
(NOT-BOARDED-PASS_U)
(not (NOT-SERVED-PASS_U))
(not (BOARDED-PASS_U))
)
)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action STOP-PASS_U-G
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-G)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action MOVEUP-F-G
:parameters ()
:precondition
(and
(LIFT_AT-F)
(NOT-ERROR-)
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(NOT-LIFT_AT-F)
(LIFT_AT-G)
(CHECKCONSISTENCY-)
(not (LIFT_AT-F))
(not (NOT-LIFT_AT-G))
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action MOVEUP-G-H
:parameters ()
:precondition
(and
(LIFT_AT-G)
(NOT-ERROR-)
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(NOT-LIFT_AT-G)
(LIFT_AT-H)
(CHECKCONSISTENCY-)
(not (LIFT_AT-G))
(not (NOT-LIFT_AT-H))
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action MOVEDOWN-G-F
:parameters ()
:precondition
(and
(LIFT_AT-G)
(NOT-ERROR-)
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(NOT-LIFT_AT-G)
(LIFT_AT-F)
(CHECKCONSISTENCY-)
(not (LIFT_AT-G))
(not (NOT-LIFT_AT-F))
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action MOVEDOWN-H-G
:parameters ()
:precondition
(and
(LIFT_AT-H)
(NOT-ERROR-)
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(NOT-LIFT_AT-H)
(LIFT_AT-G)
(CHECKCONSISTENCY-)
(not (LIFT_AT-H))
(not (NOT-LIFT_AT-G))
(not (NOT-CHECKCONSISTENCY-))
)
)
(:derived (Flag360-)(and (NOT-CHECKCONSISTENCY-)(NOT-ERROR-)(LIFT_AT-H)))

(:derived (Flag361-)(and (NOT-CHECKCONSISTENCY-)(NOT-ERROR-)(LIFT_AT-I)))

(:action STOP-PASS_O-T
:parameters ()
:precondition
(and
(Flag352-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action STOP-PASS_O-S
:parameters ()
:precondition
(and
(Flag353-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action STOP-PASS_O-Q
:parameters ()
:precondition
(and
(Flag355-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action STOP-PASS_O-J
:parameters ()
:precondition
(and
(Flag358-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action STOP-PASS_O-K
:parameters ()
:precondition
(and
(Flag359-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action STOP-PASS_L-H
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-H)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action STOP-PASS_L-I
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-I)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action STOP-PASS_C-H
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-H)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action STOP-PASS_C-I
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-I)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action STOP-PASS_B-H
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-H)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action STOP-PASS_B-I
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-I)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action STOP-PASS_D-H
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-H)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action STOP-PASS_D-I
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-I)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action STOP-PASS_R-H
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-H)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action STOP-PASS_R-I
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-I)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action STOP-PASS_V-H
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-H)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action STOP-PASS_V-I
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-I)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action STOP-PASS_U-H
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-H)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action STOP-PASS_U-I
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-I)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action MOVEUP-H-I
:parameters ()
:precondition
(and
(LIFT_AT-H)
(NOT-ERROR-)
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(NOT-LIFT_AT-H)
(LIFT_AT-I)
(CHECKCONSISTENCY-)
(not (LIFT_AT-H))
(not (NOT-LIFT_AT-I))
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action MOVEUP-I-J
:parameters ()
:precondition
(and
(LIFT_AT-I)
(NOT-ERROR-)
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(NOT-LIFT_AT-I)
(LIFT_AT-J)
(CHECKCONSISTENCY-)
(not (LIFT_AT-I))
(not (NOT-LIFT_AT-J))
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action MOVEDOWN-I-H
:parameters ()
:precondition
(and
(LIFT_AT-I)
(NOT-ERROR-)
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(NOT-LIFT_AT-I)
(LIFT_AT-H)
(CHECKCONSISTENCY-)
(not (LIFT_AT-I))
(not (NOT-LIFT_AT-H))
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action MOVEDOWN-T-S
:parameters ()
:precondition
(and
(LIFT_AT-T)
(NOT-ERROR-)
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(NOT-LIFT_AT-T)
(LIFT_AT-S)
(CHECKCONSISTENCY-)
(not (LIFT_AT-T))
(not (NOT-LIFT_AT-S))
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action MOVEDOWN-S-Q
:parameters ()
:precondition
(and
(LIFT_AT-S)
(NOT-ERROR-)
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(NOT-LIFT_AT-S)
(LIFT_AT-Q)
(CHECKCONSISTENCY-)
(not (LIFT_AT-S))
(not (NOT-LIFT_AT-Q))
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action MOVEDOWN-J-I
:parameters ()
:precondition
(and
(LIFT_AT-J)
(NOT-ERROR-)
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(NOT-LIFT_AT-J)
(LIFT_AT-I)
(CHECKCONSISTENCY-)
(not (LIFT_AT-J))
(not (NOT-LIFT_AT-I))
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action MOVEDOWN-Q-P
:parameters ()
:precondition
(and
(LIFT_AT-Q)
(NOT-ERROR-)
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(NOT-LIFT_AT-Q)
(LIFT_AT-P)
(CHECKCONSISTENCY-)
(not (LIFT_AT-Q))
(not (NOT-LIFT_AT-P))
(not (NOT-CHECKCONSISTENCY-))
)
)
(:derived (Flag352-)(and (NOT-CHECKCONSISTENCY-)(NOT-ERROR-)(LIFT_AT-T)))

(:derived (Flag353-)(and (NOT-CHECKCONSISTENCY-)(NOT-ERROR-)(LIFT_AT-S)))

(:derived (Flag355-)(and (NOT-CHECKCONSISTENCY-)(NOT-ERROR-)(LIFT_AT-Q)))

(:derived (Flag358-)(and (NOT-CHECKCONSISTENCY-)(NOT-ERROR-)(LIFT_AT-J)))

(:derived (Flag359-)(and (NOT-CHECKCONSISTENCY-)(NOT-ERROR-)(LIFT_AT-K)))

(:action STOP-PASS_O-P
:parameters ()
:precondition
(and
(Flag354-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action STOP-PASS_O-M
:parameters ()
:precondition
(and
(Flag357-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action STOP-PASS_L-T
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-T)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action STOP-PASS_L-S
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-S)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action STOP-PASS_L-Q
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-Q)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action STOP-PASS_L-J
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-J)
)
:effect
(and
(when
(and
(BOARDED-PASS_L)
)
(and
(SERVED-PASS_L)
(NOT-BOARDED-PASS_L)
(not (NOT-SERVED-PASS_L))
(not (BOARDED-PASS_L))
)
)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action STOP-PASS_L-K
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-K)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action STOP-PASS_C-T
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-T)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action STOP-PASS_C-S
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-S)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action STOP-PASS_C-Q
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-Q)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action STOP-PASS_C-J
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-J)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action STOP-PASS_C-K
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-K)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action STOP-PASS_B-T
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-T)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action STOP-PASS_B-S
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-S)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action STOP-PASS_B-Q
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-Q)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action STOP-PASS_B-J
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-J)
)
:effect
(and
(when
(and
(BOARDED-PASS_B)
)
(and
(SERVED-PASS_B)
(NOT-BOARDED-PASS_B)
(not (NOT-SERVED-PASS_B))
(not (BOARDED-PASS_B))
)
)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action STOP-PASS_B-K
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-K)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action STOP-PASS_D-T
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-T)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action STOP-PASS_D-S
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-S)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action STOP-PASS_D-Q
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-Q)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action STOP-PASS_D-J
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-J)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action STOP-PASS_D-K
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-K)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action STOP-PASS_R-T
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-T)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action STOP-PASS_R-S
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-S)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action STOP-PASS_R-Q
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-Q)
)
:effect
(and
(when
(and
(Flag376-)
)
(and
(BOARDED-PASS_R)
(not (NOT-BOARDED-PASS_R))
)
)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action STOP-PASS_R-J
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-J)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action STOP-PASS_R-K
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-K)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action STOP-PASS_V-T
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-T)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action STOP-PASS_V-S
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-S)
)
:effect
(and
(when
(and
(BOARDED-PASS_V)
)
(and
(SERVED-PASS_V)
(NOT-BOARDED-PASS_V)
(not (NOT-SERVED-PASS_V))
(not (BOARDED-PASS_V))
)
)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action STOP-PASS_V-Q
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-Q)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action STOP-PASS_V-J
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-J)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action STOP-PASS_V-K
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-K)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action STOP-PASS_U-T
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-T)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action STOP-PASS_U-S
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-S)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action STOP-PASS_U-Q
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-Q)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action STOP-PASS_U-J
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-J)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action STOP-PASS_U-K
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-K)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:derived (Flag2-)(and (Flag1-)))

(:derived (Flag4-)(and (Flag3-)))

(:derived (Flag6-)(and (Flag5-)))

(:derived (Flag8-)(and (Flag7-)))

(:derived (Flag10-)(and (Flag9-)))

(:derived (Flag12-)(and (Flag11-)))

(:derived (Flag14-)(and (Flag13-)))

(:derived (Flag16-)(and (Flag15-)))

(:derived (Flag18-)(and (Flag17-)))

(:derived (Flag20-)(and (Flag19-)))

(:derived (Flag22-)(and (Flag21-)))

(:derived (Flag24-)(and (Flag23-)))

(:derived (Flag26-)(and (Flag25-)))

(:derived (Flag28-)(and (Flag27-)))

(:action MOVEUP-J-K
:parameters ()
:precondition
(and
(LIFT_AT-J)
(NOT-ERROR-)
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(NOT-LIFT_AT-J)
(LIFT_AT-K)
(CHECKCONSISTENCY-)
(not (LIFT_AT-J))
(not (NOT-LIFT_AT-K))
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action MOVEUP-S-T
:parameters ()
:precondition
(and
(LIFT_AT-S)
(NOT-ERROR-)
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(NOT-LIFT_AT-S)
(LIFT_AT-T)
(CHECKCONSISTENCY-)
(not (LIFT_AT-S))
(not (NOT-LIFT_AT-T))
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action MOVEUP-Q-S
:parameters ()
:precondition
(and
(LIFT_AT-Q)
(NOT-ERROR-)
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(NOT-LIFT_AT-Q)
(LIFT_AT-S)
(CHECKCONSISTENCY-)
(not (LIFT_AT-Q))
(not (NOT-LIFT_AT-S))
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action MOVEUP-K-M
:parameters ()
:precondition
(and
(LIFT_AT-K)
(NOT-ERROR-)
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(NOT-LIFT_AT-K)
(LIFT_AT-M)
(CHECKCONSISTENCY-)
(not (LIFT_AT-K))
(not (NOT-LIFT_AT-M))
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action MOVEUP-P-Q
:parameters ()
:precondition
(and
(LIFT_AT-P)
(NOT-ERROR-)
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(NOT-LIFT_AT-P)
(LIFT_AT-Q)
(CHECKCONSISTENCY-)
(not (LIFT_AT-P))
(not (NOT-LIFT_AT-Q))
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action MOVEDOWN-K-J
:parameters ()
:precondition
(and
(LIFT_AT-K)
(NOT-ERROR-)
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(NOT-LIFT_AT-K)
(LIFT_AT-J)
(CHECKCONSISTENCY-)
(not (LIFT_AT-K))
(not (NOT-LIFT_AT-J))
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action MOVEDOWN-P-N
:parameters ()
:precondition
(and
(LIFT_AT-P)
(NOT-ERROR-)
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(NOT-LIFT_AT-P)
(LIFT_AT-N)
(CHECKCONSISTENCY-)
(not (LIFT_AT-P))
(not (NOT-LIFT_AT-N))
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action MOVEDOWN-M-K
:parameters ()
:precondition
(and
(LIFT_AT-M)
(NOT-ERROR-)
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(NOT-LIFT_AT-M)
(LIFT_AT-K)
(CHECKCONSISTENCY-)
(not (LIFT_AT-M))
(not (NOT-LIFT_AT-K))
(not (NOT-CHECKCONSISTENCY-))
)
)
(:derived (Flag354-)(and (NOT-CHECKCONSISTENCY-)(NOT-ERROR-)(LIFT_AT-P)))

(:derived (Flag357-)(and (NOT-CHECKCONSISTENCY-)(NOT-ERROR-)(LIFT_AT-M)))

(:action STOP-PASS_O-N
:parameters ()
:precondition
(and
(Flag356-)
)
:effect
(and
(when
(and
(BOARDED-PASS_O)
)
(and
(SERVED-PASS_O)
(NOT-BOARDED-PASS_O)
(not (NOT-SERVED-PASS_O))
(not (BOARDED-PASS_O))
)
)
(when
(and
(Flag381-)
)
(and
(BOARDED-PASS_O)
(not (NOT-BOARDED-PASS_O))
)
)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action STOP-PASS_L-P
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-P)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action STOP-PASS_L-N
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-N)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action STOP-PASS_L-M
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-M)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action STOP-PASS_C-P
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-P)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action STOP-PASS_C-N
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-N)
)
:effect
(and
(when
(and
(BOARDED-PASS_C)
)
(and
(SERVED-PASS_C)
(NOT-BOARDED-PASS_C)
(not (NOT-SERVED-PASS_C))
(not (BOARDED-PASS_C))
)
)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action STOP-PASS_C-M
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-M)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action STOP-PASS_B-P
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-P)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action STOP-PASS_B-N
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-N)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action STOP-PASS_B-M
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-M)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action STOP-PASS_D-P
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-P)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action STOP-PASS_D-N
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-N)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action STOP-PASS_D-M
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-M)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action STOP-PASS_R-P
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-P)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action STOP-PASS_R-N
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-N)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action STOP-PASS_R-M
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-M)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action STOP-PASS_V-P
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-P)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action STOP-PASS_V-N
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-N)
)
:effect
(and
(when
(and
(Flag375-)
)
(and
(BOARDED-PASS_V)
(not (NOT-BOARDED-PASS_V))
)
)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action STOP-PASS_V-M
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-M)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action STOP-PASS_U-P
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-P)
)
:effect
(and
(when
(and
(Flag374-)
)
(and
(BOARDED-PASS_U)
(not (NOT-BOARDED-PASS_U))
)
)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action STOP-PASS_U-N
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-N)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action STOP-PASS_U-M
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-M)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:derived (Flag1-)(and ))

(:derived (Flag3-)(and ))

(:derived (Flag5-)(and ))

(:derived (Flag7-)(and ))

(:derived (Flag9-)(and ))

(:derived (Flag11-)(and ))

(:derived (Flag13-)(and ))

(:derived (Flag15-)(and ))

(:derived (Flag17-)(and ))

(:derived (Flag19-)(and ))

(:derived (Flag21-)(and ))

(:derived (Flag23-)(and ))

(:derived (Flag25-)(and ))

(:derived (Flag27-)(and ))

(:action CHECKCONSISTENCYACTION
:parameters ()
:precondition
(and
(NOT-ERROR-)
(CHECKCONSISTENCY-)
)
:effect
(and
(NOT-CHECKCONSISTENCY-)
(not (CHECKCONSISTENCY-))
)
)
(:action MOVEUP-N-P
:parameters ()
:precondition
(and
(LIFT_AT-N)
(NOT-ERROR-)
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(NOT-LIFT_AT-N)
(LIFT_AT-P)
(CHECKCONSISTENCY-)
(not (LIFT_AT-N))
(not (NOT-LIFT_AT-P))
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action MOVEUP-M-N
:parameters ()
:precondition
(and
(LIFT_AT-M)
(NOT-ERROR-)
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(NOT-LIFT_AT-M)
(LIFT_AT-N)
(CHECKCONSISTENCY-)
(not (LIFT_AT-M))
(not (NOT-LIFT_AT-N))
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action MOVEDOWN-N-M
:parameters ()
:precondition
(and
(LIFT_AT-N)
(NOT-ERROR-)
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(NOT-LIFT_AT-N)
(LIFT_AT-M)
(CHECKCONSISTENCY-)
(not (LIFT_AT-N))
(not (NOT-LIFT_AT-M))
(not (NOT-CHECKCONSISTENCY-))
)
)
(:derived (Flag356-)(and (NOT-CHECKCONSISTENCY-)(NOT-ERROR-)(LIFT_AT-N)))

(:derived (Flag374-)(and (NOT-BOARDED-PASS_U)(NOT-SERVED-PASS_U)))

(:derived (Flag375-)(and (NOT-BOARDED-PASS_V)(NOT-SERVED-PASS_V)))

(:derived (Flag376-)(and (NOT-BOARDED-PASS_R)(NOT-SERVED-PASS_R)))

(:derived (Flag377-)(and (NOT-BOARDED-PASS_D)(NOT-SERVED-PASS_D)))

(:derived (Flag378-)(and (NOT-BOARDED-PASS_B)(NOT-SERVED-PASS_B)))

(:derived (Flag379-)(and (NOT-BOARDED-PASS_C)(NOT-SERVED-PASS_C)))

(:derived (Flag380-)(and (NOT-BOARDED-PASS_L)(NOT-SERVED-PASS_L)))

(:derived (Flag381-)(and (NOT-BOARDED-PASS_O)(NOT-SERVED-PASS_O)))

)
