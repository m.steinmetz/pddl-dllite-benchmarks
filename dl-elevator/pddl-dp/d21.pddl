(define (domain grounded-SIMPLE-ADL-ELEVATORS)
(:requirements
:strips
)
(:predicates
(Flag342-)
(Flag341-)
(Flag340-)
(Flag339-)
(Flag338-)
(Flag337-)
(Flag336-)
(Flag335-)
(Flag61-)
(NOT-LIFT_AT-M)
(LIFT_AT-N)
(CHECKCONSISTENCY-)
(LIFT_AT-S)
(LIFT_AT-L)
(ERROR-)
(Flag25-)
(Flag23-)
(Flag21-)
(Flag19-)
(Flag17-)
(Flag15-)
(Flag13-)
(Flag11-)
(Flag9-)
(Flag7-)
(Flag5-)
(Flag3-)
(Flag1-)
(SERVED-PASS_R)
(SERVED-PASS_E)
(BOARDED-PASS_E)
(BOARDED-PASS_O)
(Flag60-)
(Flag59-)
(Flag57-)
(LIFT_AT-T)
(LIFT_AT-U)
(LIFT_AT-K)
(LIFT_AT-J)
(Flag26-)
(Flag24-)
(Flag22-)
(Flag20-)
(Flag18-)
(Flag16-)
(Flag14-)
(Flag12-)
(Flag10-)
(Flag8-)
(Flag6-)
(Flag4-)
(Flag2-)
(BOARDED-PASS_P)
(SERVED-PASS_Q)
(BOARDED-PASS_G)
(SERVED-PASS_G)
(BOARDED-PASS_H)
(Flag63-)
(Flag62-)
(Flag55-)
(Flag53-)
(LIFT_AT-I)
(LIFT_AT-F)
(LIFT_AT-D)
(SERVED-PASS_P)
(BOARDED-PASS_R)
(BOARDED-PASS_A)
(SERVED-PASS_O)
(Flag65-)
(Flag64-)
(Flag50-)
(LIFT_AT-C)
(LIFT_AT-B)
(BOARDED-PASS_Q)
(SERVED-PASS_A)
(Flag70-)
(Flag69-)
(SERVED-PASS_H)
(Flag27-)
(NOT-SERVED-PASS_H)
(NOT-CHECKCONSISTENCY-)
(NOT-LIFT_AT-C)
(NOT-LIFT_AT-D)
(NOT-SERVED-PASS_A)
(NOT-BOARDED-PASS_Q)
(NOT-LIFT_AT-B)
(NOT-LIFT_AT-F)
(NOT-LIFT_AT-I)
(NOT-LIFT_AT-J)
(NOT-SERVED-PASS_O)
(NOT-BOARDED-PASS_A)
(NOT-BOARDED-PASS_R)
(NOT-SERVED-PASS_P)
(NOT-LIFT_AT-K)
(NOT-LIFT_AT-L)
(NOT-BOARDED-PASS_H)
(NOT-SERVED-PASS_G)
(NOT-BOARDED-PASS_G)
(NOT-SERVED-PASS_Q)
(NOT-BOARDED-PASS_P)
(NOT-LIFT_AT-T)
(NOT-LIFT_AT-S)
(NOT-LIFT_AT-U)
(NOT-BOARDED-PASS_O)
(NOT-BOARDED-PASS_E)
(NOT-SERVED-PASS_E)
(NOT-SERVED-PASS_R)
(NOT-ERROR-)
(NOT-LIFT_AT-N)
(LIFT_AT-M)
)
(:derived (Flag27-)(and (Flag2-)(Flag4-)(SERVED-PASS_P)(SERVED-PASS_Q)(SERVED-PASS_R)(Flag6-)(Flag8-)(Flag10-)(Flag12-)(Flag14-)(Flag16-)(Flag18-)(SERVED-PASS_E)(Flag20-)(SERVED-PASS_G)(Flag22-)(SERVED-PASS_A)(Flag24-)(SERVED-PASS_O)(SERVED-PASS_H)(Flag26-)(NOT-ERROR-)(NOT-CHECKCONSISTENCY-)))

(:action STOP-PASS_H-B
:parameters ()
:precondition
(and
(Flag69-)
)
:effect
(and
(when
(and
(BOARDED-PASS_H)
)
(and
(SERVED-PASS_H)
(NOT-BOARDED-PASS_H)
(not (NOT-SERVED-PASS_H))
(not (BOARDED-PASS_H))
)
)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action STOP-PASS_H-C
:parameters ()
:precondition
(and
(Flag70-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action MOVEUP-B-C
:parameters ()
:precondition
(and
(LIFT_AT-B)
(NOT-ERROR-)
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(NOT-LIFT_AT-B)
(LIFT_AT-C)
(CHECKCONSISTENCY-)
(not (LIFT_AT-B))
(not (NOT-LIFT_AT-C))
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action MOVEUP-C-D
:parameters ()
:precondition
(and
(LIFT_AT-C)
(NOT-ERROR-)
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(NOT-LIFT_AT-C)
(LIFT_AT-D)
(CHECKCONSISTENCY-)
(not (LIFT_AT-C))
(not (NOT-LIFT_AT-D))
(not (NOT-CHECKCONSISTENCY-))
)
)
(:derived (Flag69-)(and (NOT-CHECKCONSISTENCY-)(NOT-ERROR-)(LIFT_AT-B)))

(:derived (Flag70-)(and (NOT-CHECKCONSISTENCY-)(NOT-ERROR-)(LIFT_AT-C)))

(:action STOP-PASS_H-D
:parameters ()
:precondition
(and
(Flag50-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action STOP-PASS_H-I
:parameters ()
:precondition
(and
(Flag64-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action STOP-PASS_H-F
:parameters ()
:precondition
(and
(Flag65-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action STOP-PASS_O-B
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-B)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action STOP-PASS_O-C
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-C)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action STOP-PASS_A-B
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-B)
)
:effect
(and
(when
(and
(BOARDED-PASS_A)
)
(and
(SERVED-PASS_A)
(NOT-BOARDED-PASS_A)
(not (NOT-SERVED-PASS_A))
(not (BOARDED-PASS_A))
)
)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action STOP-PASS_A-C
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-C)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action STOP-PASS_G-B
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-B)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action STOP-PASS_G-C
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-C)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action STOP-PASS_E-B
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-B)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action STOP-PASS_E-C
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-C)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action STOP-PASS_R-B
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-B)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action STOP-PASS_R-C
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-C)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action STOP-PASS_Q-B
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-B)
)
:effect
(and
(when
(and
(Flag336-)
)
(and
(BOARDED-PASS_Q)
(not (NOT-BOARDED-PASS_Q))
)
)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action STOP-PASS_Q-C
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-C)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action STOP-PASS_P-B
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-B)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action STOP-PASS_P-C
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-C)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action MOVEDOWN-C-B
:parameters ()
:precondition
(and
(LIFT_AT-C)
(NOT-ERROR-)
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(NOT-LIFT_AT-C)
(LIFT_AT-B)
(CHECKCONSISTENCY-)
(not (LIFT_AT-C))
(not (NOT-LIFT_AT-B))
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action MOVEDOWN-D-C
:parameters ()
:precondition
(and
(LIFT_AT-D)
(NOT-ERROR-)
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(NOT-LIFT_AT-D)
(LIFT_AT-C)
(CHECKCONSISTENCY-)
(not (LIFT_AT-D))
(not (NOT-LIFT_AT-C))
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action MOVEUP-D-F
:parameters ()
:precondition
(and
(LIFT_AT-D)
(NOT-ERROR-)
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(NOT-LIFT_AT-D)
(LIFT_AT-F)
(CHECKCONSISTENCY-)
(not (LIFT_AT-D))
(not (NOT-LIFT_AT-F))
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action MOVEUP-F-I
:parameters ()
:precondition
(and
(LIFT_AT-F)
(NOT-ERROR-)
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(NOT-LIFT_AT-F)
(LIFT_AT-I)
(CHECKCONSISTENCY-)
(not (LIFT_AT-F))
(not (NOT-LIFT_AT-I))
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action MOVEUP-I-J
:parameters ()
:precondition
(and
(LIFT_AT-I)
(NOT-ERROR-)
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(NOT-LIFT_AT-I)
(LIFT_AT-J)
(CHECKCONSISTENCY-)
(not (LIFT_AT-I))
(not (NOT-LIFT_AT-J))
(not (NOT-CHECKCONSISTENCY-))
)
)
(:derived (Flag50-)(and (NOT-CHECKCONSISTENCY-)(NOT-ERROR-)(LIFT_AT-D)))

(:derived (Flag64-)(and (NOT-CHECKCONSISTENCY-)(NOT-ERROR-)(LIFT_AT-I)))

(:derived (Flag65-)(and (NOT-CHECKCONSISTENCY-)(NOT-ERROR-)(LIFT_AT-F)))

(:action STOP-PASS_H-T
:parameters ()
:precondition
(and
(Flag53-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action STOP-PASS_H-U
:parameters ()
:precondition
(and
(Flag55-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action STOP-PASS_H-J
:parameters ()
:precondition
(and
(Flag62-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action STOP-PASS_H-K
:parameters ()
:precondition
(and
(Flag63-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action STOP-PASS_O-D
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-D)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action STOP-PASS_O-I
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-I)
)
:effect
(and
(when
(and
(BOARDED-PASS_O)
)
(and
(SERVED-PASS_O)
(NOT-BOARDED-PASS_O)
(not (NOT-SERVED-PASS_O))
(not (BOARDED-PASS_O))
)
)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action STOP-PASS_O-F
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-F)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action STOP-PASS_A-D
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-D)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action STOP-PASS_A-I
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-I)
)
:effect
(and
(when
(and
(Flag340-)
)
(and
(BOARDED-PASS_A)
(not (NOT-BOARDED-PASS_A))
)
)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action STOP-PASS_A-F
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-F)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action STOP-PASS_G-D
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-D)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action STOP-PASS_G-I
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-I)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action STOP-PASS_G-F
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-F)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action STOP-PASS_E-D
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-D)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action STOP-PASS_E-I
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-I)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action STOP-PASS_E-F
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-F)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action STOP-PASS_R-D
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-D)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action STOP-PASS_R-I
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-I)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action STOP-PASS_R-F
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-F)
)
:effect
(and
(when
(and
(Flag337-)
)
(and
(BOARDED-PASS_R)
(not (NOT-BOARDED-PASS_R))
)
)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action STOP-PASS_Q-D
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-D)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action STOP-PASS_Q-I
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-I)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action STOP-PASS_Q-F
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-F)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action STOP-PASS_P-D
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-D)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action STOP-PASS_P-I
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-I)
)
:effect
(and
(when
(and
(BOARDED-PASS_P)
)
(and
(SERVED-PASS_P)
(NOT-BOARDED-PASS_P)
(not (NOT-SERVED-PASS_P))
(not (BOARDED-PASS_P))
)
)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action STOP-PASS_P-F
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-F)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action MOVEDOWN-F-D
:parameters ()
:precondition
(and
(LIFT_AT-F)
(NOT-ERROR-)
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(NOT-LIFT_AT-F)
(LIFT_AT-D)
(CHECKCONSISTENCY-)
(not (LIFT_AT-F))
(not (NOT-LIFT_AT-D))
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action MOVEDOWN-I-F
:parameters ()
:precondition
(and
(LIFT_AT-I)
(NOT-ERROR-)
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(NOT-LIFT_AT-I)
(LIFT_AT-F)
(CHECKCONSISTENCY-)
(not (LIFT_AT-I))
(not (NOT-LIFT_AT-F))
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action MOVEDOWN-J-I
:parameters ()
:precondition
(and
(LIFT_AT-J)
(NOT-ERROR-)
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(NOT-LIFT_AT-J)
(LIFT_AT-I)
(CHECKCONSISTENCY-)
(not (LIFT_AT-J))
(not (NOT-LIFT_AT-I))
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action MOVEUP-J-K
:parameters ()
:precondition
(and
(LIFT_AT-J)
(NOT-ERROR-)
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(NOT-LIFT_AT-J)
(LIFT_AT-K)
(CHECKCONSISTENCY-)
(not (LIFT_AT-J))
(not (NOT-LIFT_AT-K))
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action MOVEUP-K-L
:parameters ()
:precondition
(and
(LIFT_AT-K)
(NOT-ERROR-)
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(NOT-LIFT_AT-K)
(LIFT_AT-L)
(CHECKCONSISTENCY-)
(not (LIFT_AT-K))
(not (NOT-LIFT_AT-L))
(not (NOT-CHECKCONSISTENCY-))
)
)
(:derived (Flag53-)(and (NOT-CHECKCONSISTENCY-)(NOT-ERROR-)(LIFT_AT-T)))

(:derived (Flag55-)(and (NOT-CHECKCONSISTENCY-)(NOT-ERROR-)(LIFT_AT-U)))

(:derived (Flag62-)(and (NOT-CHECKCONSISTENCY-)(NOT-ERROR-)(LIFT_AT-J)))

(:derived (Flag63-)(and (NOT-CHECKCONSISTENCY-)(NOT-ERROR-)(LIFT_AT-K)))

(:action STOP-PASS_H-S
:parameters ()
:precondition
(and
(Flag57-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action STOP-PASS_H-N
:parameters ()
:precondition
(and
(Flag59-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action STOP-PASS_H-L
:parameters ()
:precondition
(and
(Flag60-)
)
:effect
(and
(when
(and
(Flag342-)
)
(and
(BOARDED-PASS_H)
(not (NOT-BOARDED-PASS_H))
)
)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action STOP-PASS_O-T
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-T)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action STOP-PASS_O-U
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-U)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action STOP-PASS_O-J
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-J)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action STOP-PASS_O-K
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-K)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action STOP-PASS_A-T
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-T)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action STOP-PASS_A-U
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-U)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action STOP-PASS_A-J
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-J)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action STOP-PASS_A-K
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-K)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action STOP-PASS_G-T
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-T)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action STOP-PASS_G-U
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-U)
)
:effect
(and
(when
(and
(BOARDED-PASS_G)
)
(and
(SERVED-PASS_G)
(NOT-BOARDED-PASS_G)
(not (NOT-SERVED-PASS_G))
(not (BOARDED-PASS_G))
)
)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action STOP-PASS_G-J
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-J)
)
:effect
(and
(when
(and
(Flag339-)
)
(and
(BOARDED-PASS_G)
(not (NOT-BOARDED-PASS_G))
)
)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action STOP-PASS_G-K
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-K)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action STOP-PASS_E-T
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-T)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action STOP-PASS_E-U
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-U)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action STOP-PASS_E-J
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-J)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action STOP-PASS_E-K
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-K)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action STOP-PASS_R-T
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-T)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action STOP-PASS_R-U
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-U)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action STOP-PASS_R-J
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-J)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action STOP-PASS_R-K
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-K)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action STOP-PASS_Q-T
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-T)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action STOP-PASS_Q-U
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-U)
)
:effect
(and
(when
(and
(BOARDED-PASS_Q)
)
(and
(SERVED-PASS_Q)
(NOT-BOARDED-PASS_Q)
(not (NOT-SERVED-PASS_Q))
(not (BOARDED-PASS_Q))
)
)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action STOP-PASS_Q-J
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-J)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action STOP-PASS_Q-K
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-K)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action STOP-PASS_P-T
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-T)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action STOP-PASS_P-U
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-U)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action STOP-PASS_P-J
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-J)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action STOP-PASS_P-K
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-K)
)
:effect
(and
(when
(and
(Flag335-)
)
(and
(BOARDED-PASS_P)
(not (NOT-BOARDED-PASS_P))
)
)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:derived (Flag2-)(and (Flag1-)))

(:derived (Flag4-)(and (Flag3-)))

(:derived (Flag6-)(and (Flag5-)))

(:derived (Flag8-)(and (Flag7-)))

(:derived (Flag10-)(and (Flag9-)))

(:derived (Flag12-)(and (Flag11-)))

(:derived (Flag14-)(and (Flag13-)))

(:derived (Flag16-)(and (Flag15-)))

(:derived (Flag18-)(and (Flag17-)))

(:derived (Flag20-)(and (Flag19-)))

(:derived (Flag22-)(and (Flag21-)))

(:derived (Flag24-)(and (Flag23-)))

(:derived (Flag26-)(and (Flag25-)))

(:action MOVEDOWN-K-J
:parameters ()
:precondition
(and
(LIFT_AT-K)
(NOT-ERROR-)
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(NOT-LIFT_AT-K)
(LIFT_AT-J)
(CHECKCONSISTENCY-)
(not (LIFT_AT-K))
(not (NOT-LIFT_AT-J))
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action MOVEDOWN-U-T
:parameters ()
:precondition
(and
(LIFT_AT-U)
(NOT-ERROR-)
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(NOT-LIFT_AT-U)
(LIFT_AT-T)
(CHECKCONSISTENCY-)
(not (LIFT_AT-U))
(not (NOT-LIFT_AT-T))
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action MOVEDOWN-T-S
:parameters ()
:precondition
(and
(LIFT_AT-T)
(NOT-ERROR-)
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(NOT-LIFT_AT-T)
(LIFT_AT-S)
(CHECKCONSISTENCY-)
(not (LIFT_AT-T))
(not (NOT-LIFT_AT-S))
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action MOVEDOWN-L-K
:parameters ()
:precondition
(and
(LIFT_AT-L)
(NOT-ERROR-)
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(NOT-LIFT_AT-L)
(LIFT_AT-K)
(CHECKCONSISTENCY-)
(not (LIFT_AT-L))
(not (NOT-LIFT_AT-K))
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action MOVEUP-L-M
:parameters ()
:precondition
(and
(LIFT_AT-L)
(NOT-ERROR-)
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(NOT-LIFT_AT-L)
(LIFT_AT-M)
(CHECKCONSISTENCY-)
(not (LIFT_AT-L))
(not (NOT-LIFT_AT-M))
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action MOVEUP-T-U
:parameters ()
:precondition
(and
(LIFT_AT-T)
(NOT-ERROR-)
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(NOT-LIFT_AT-T)
(LIFT_AT-U)
(CHECKCONSISTENCY-)
(not (LIFT_AT-T))
(not (NOT-LIFT_AT-U))
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action MOVEUP-S-T
:parameters ()
:precondition
(and
(LIFT_AT-S)
(NOT-ERROR-)
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(NOT-LIFT_AT-S)
(LIFT_AT-T)
(CHECKCONSISTENCY-)
(not (LIFT_AT-S))
(not (NOT-LIFT_AT-T))
(not (NOT-CHECKCONSISTENCY-))
)
)
(:derived (Flag57-)(and (NOT-CHECKCONSISTENCY-)(NOT-ERROR-)(LIFT_AT-S)))

(:derived (Flag59-)(and (NOT-CHECKCONSISTENCY-)(NOT-ERROR-)(LIFT_AT-N)))

(:derived (Flag60-)(and (NOT-CHECKCONSISTENCY-)(NOT-ERROR-)(LIFT_AT-L)))

(:action STOP-PASS_H-M
:parameters ()
:precondition
(and
(Flag61-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action STOP-PASS_O-S
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-S)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action STOP-PASS_O-N
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-N)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action STOP-PASS_O-L
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-L)
)
:effect
(and
(when
(and
(Flag341-)
)
(and
(BOARDED-PASS_O)
(not (NOT-BOARDED-PASS_O))
)
)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action STOP-PASS_O-M
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-M)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action STOP-PASS_A-S
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-S)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action STOP-PASS_A-N
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-N)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action STOP-PASS_A-L
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-L)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action STOP-PASS_A-M
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-M)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action STOP-PASS_G-S
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-S)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action STOP-PASS_G-N
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-N)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action STOP-PASS_G-L
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-L)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action STOP-PASS_G-M
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-M)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action STOP-PASS_E-S
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-S)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action STOP-PASS_E-N
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-N)
)
:effect
(and
(when
(and
(Flag338-)
)
(and
(BOARDED-PASS_E)
(not (NOT-BOARDED-PASS_E))
)
)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action STOP-PASS_E-L
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-L)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action STOP-PASS_E-M
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-M)
)
:effect
(and
(when
(and
(BOARDED-PASS_E)
)
(and
(SERVED-PASS_E)
(NOT-BOARDED-PASS_E)
(not (NOT-SERVED-PASS_E))
(not (BOARDED-PASS_E))
)
)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action STOP-PASS_R-S
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-S)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action STOP-PASS_R-N
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-N)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action STOP-PASS_R-L
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-L)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action STOP-PASS_R-M
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-M)
)
:effect
(and
(when
(and
(BOARDED-PASS_R)
)
(and
(SERVED-PASS_R)
(NOT-BOARDED-PASS_R)
(not (NOT-SERVED-PASS_R))
(not (BOARDED-PASS_R))
)
)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action STOP-PASS_Q-S
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-S)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action STOP-PASS_Q-N
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-N)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action STOP-PASS_Q-L
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-L)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action STOP-PASS_Q-M
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-M)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action STOP-PASS_P-S
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-S)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action STOP-PASS_P-N
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-N)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action STOP-PASS_P-L
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-L)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action STOP-PASS_P-M
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-M)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:derived (Flag1-)(and ))

(:derived (Flag3-)(and ))

(:derived (Flag5-)(and ))

(:derived (Flag7-)(and ))

(:derived (Flag9-)(and ))

(:derived (Flag11-)(and ))

(:derived (Flag13-)(and ))

(:derived (Flag15-)(and ))

(:derived (Flag17-)(and ))

(:derived (Flag19-)(and ))

(:derived (Flag21-)(and ))

(:derived (Flag23-)(and ))

(:derived (Flag25-)(and ))

(:action CHECKCONSISTENCYACTION
:parameters ()
:precondition
(and
(NOT-ERROR-)
(CHECKCONSISTENCY-)
)
:effect
(and
(NOT-CHECKCONSISTENCY-)
(not (CHECKCONSISTENCY-))
)
)
(:action MOVEDOWN-S-N
:parameters ()
:precondition
(and
(LIFT_AT-S)
(NOT-ERROR-)
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(NOT-LIFT_AT-S)
(LIFT_AT-N)
(CHECKCONSISTENCY-)
(not (LIFT_AT-S))
(not (NOT-LIFT_AT-N))
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action MOVEDOWN-M-L
:parameters ()
:precondition
(and
(LIFT_AT-M)
(NOT-ERROR-)
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(NOT-LIFT_AT-M)
(LIFT_AT-L)
(CHECKCONSISTENCY-)
(not (LIFT_AT-M))
(not (NOT-LIFT_AT-L))
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action MOVEDOWN-N-M
:parameters ()
:precondition
(and
(LIFT_AT-N)
(NOT-ERROR-)
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(NOT-LIFT_AT-N)
(LIFT_AT-M)
(CHECKCONSISTENCY-)
(not (LIFT_AT-N))
(not (NOT-LIFT_AT-M))
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action MOVEUP-N-S
:parameters ()
:precondition
(and
(LIFT_AT-N)
(NOT-ERROR-)
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(NOT-LIFT_AT-N)
(LIFT_AT-S)
(CHECKCONSISTENCY-)
(not (LIFT_AT-N))
(not (NOT-LIFT_AT-S))
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action MOVEUP-M-N
:parameters ()
:precondition
(and
(LIFT_AT-M)
(NOT-ERROR-)
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(NOT-LIFT_AT-M)
(LIFT_AT-N)
(CHECKCONSISTENCY-)
(not (LIFT_AT-M))
(not (NOT-LIFT_AT-N))
(not (NOT-CHECKCONSISTENCY-))
)
)
(:derived (Flag61-)(and (NOT-CHECKCONSISTENCY-)(NOT-ERROR-)(LIFT_AT-M)))

(:derived (Flag335-)(and (NOT-SERVED-PASS_P)(NOT-BOARDED-PASS_P)))

(:derived (Flag336-)(and (NOT-SERVED-PASS_Q)(NOT-BOARDED-PASS_Q)))

(:derived (Flag337-)(and (NOT-SERVED-PASS_R)(NOT-BOARDED-PASS_R)))

(:derived (Flag338-)(and (NOT-SERVED-PASS_E)(NOT-BOARDED-PASS_E)))

(:derived (Flag339-)(and (NOT-SERVED-PASS_G)(NOT-BOARDED-PASS_G)))

(:derived (Flag340-)(and (NOT-SERVED-PASS_A)(NOT-BOARDED-PASS_A)))

(:derived (Flag341-)(and (NOT-SERVED-PASS_O)(NOT-BOARDED-PASS_O)))

(:derived (Flag342-)(and (NOT-SERVED-PASS_H)(NOT-BOARDED-PASS_H)))

)
