(define (domain grounded-SIMPLE-ADL-ELEVATORS)
(:requirements
:strips
)
(:predicates
(Flag257-)
(Flag256-)
(Flag255-)
(Flag254-)
(Flag253-)
(Flag252-)
(Flag251-)
(Flag59-)
(NOT-LIFT_AT-C)
(LIFT_AT-B)
(CHECKCONSISTENCY-)
(LIFT_AT-D)
(LIFT_AT-E)
(ERROR-)
(Flag21-)
(Flag19-)
(Flag17-)
(Flag15-)
(Flag13-)
(Flag11-)
(Flag9-)
(Flag7-)
(Flag5-)
(Flag3-)
(Flag1-)
(SERVED-PASS_M)
(BOARDED-PASS_O)
(SERVED-PASS_I)
(BOARDED-PASS_I)
(Flag58-)
(Flag56-)
(Flag43-)
(LIFT_AT-A)
(LIFT_AT-G)
(LIFT_AT-H)
(Flag22-)
(Flag20-)
(Flag18-)
(Flag16-)
(Flag14-)
(Flag12-)
(Flag10-)
(Flag8-)
(Flag6-)
(Flag4-)
(Flag2-)
(BOARDED-PASS_P)
(SERVED-PASS_L)
(BOARDED-PASS_J)
(Flag60-)
(Flag55-)
(Flag54-)
(LIFT_AT-K)
(SERVED-PASS_F)
(SERVED-PASS_J)
(Flag53-)
(LIFT_AT-N)
(LIFT_AT-Q)
(BOARDED-PASS_F)
(BOARDED-PASS_L)
(BOARDED-PASS_M)
(Flag52-)
(Flag51-)
(LIFT_AT-R)
(SERVED-PASS_P)
(SERVED-PASS_O)
(Flag49-)
(Flag23-)
(NOT-CHECKCONSISTENCY-)
(NOT-LIFT_AT-Q)
(NOT-SERVED-PASS_O)
(NOT-SERVED-PASS_P)
(NOT-LIFT_AT-R)
(NOT-LIFT_AT-N)
(NOT-LIFT_AT-K)
(NOT-BOARDED-PASS_M)
(NOT-BOARDED-PASS_L)
(NOT-BOARDED-PASS_F)
(NOT-LIFT_AT-H)
(NOT-SERVED-PASS_J)
(NOT-SERVED-PASS_F)
(NOT-LIFT_AT-G)
(NOT-LIFT_AT-E)
(NOT-BOARDED-PASS_J)
(NOT-SERVED-PASS_L)
(NOT-BOARDED-PASS_P)
(NOT-LIFT_AT-B)
(NOT-LIFT_AT-D)
(NOT-LIFT_AT-A)
(NOT-SERVED-PASS_I)
(NOT-BOARDED-PASS_I)
(NOT-BOARDED-PASS_O)
(NOT-SERVED-PASS_M)
(NOT-ERROR-)
(LIFT_AT-C)
)
(:action STOP-PASS_J-R
:parameters ()
:precondition
(and
(Flag49-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:derived (Flag23-)(and (Flag2-)(Flag4-)(Flag6-)(SERVED-PASS_P)(Flag8-)(Flag10-)(Flag12-)(Flag14-)(Flag16-)(Flag18-)(SERVED-PASS_F)(Flag20-)(SERVED-PASS_L)(SERVED-PASS_M)(SERVED-PASS_O)(SERVED-PASS_I)(SERVED-PASS_J)(Flag22-)(NOT-ERROR-)(NOT-CHECKCONSISTENCY-)))

(:action MOVEDOWN-R-Q
:parameters ()
:precondition
(and
(LIFT_AT-R)
(NOT-ERROR-)
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(NOT-LIFT_AT-R)
(LIFT_AT-Q)
(CHECKCONSISTENCY-)
(not (LIFT_AT-R))
(not (NOT-LIFT_AT-Q))
(not (NOT-CHECKCONSISTENCY-))
)
)
(:derived (Flag49-)(and (NOT-CHECKCONSISTENCY-)(NOT-ERROR-)(LIFT_AT-R)))

(:action STOP-PASS_J-Q
:parameters ()
:precondition
(and
(Flag51-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action STOP-PASS_J-N
:parameters ()
:precondition
(and
(Flag52-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action STOP-PASS_I-R
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-R)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action STOP-PASS_O-R
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-R)
)
:effect
(and
(when
(and
(BOARDED-PASS_O)
)
(and
(SERVED-PASS_O)
(NOT-BOARDED-PASS_O)
(not (NOT-SERVED-PASS_O))
(not (BOARDED-PASS_O))
)
)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action STOP-PASS_M-R
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-R)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action STOP-PASS_L-R
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-R)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action STOP-PASS_F-R
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-R)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action STOP-PASS_P-R
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-R)
)
:effect
(and
(when
(and
(BOARDED-PASS_P)
)
(and
(SERVED-PASS_P)
(NOT-BOARDED-PASS_P)
(not (NOT-SERVED-PASS_P))
(not (BOARDED-PASS_P))
)
)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action MOVEUP-Q-R
:parameters ()
:precondition
(and
(LIFT_AT-Q)
(NOT-ERROR-)
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(NOT-LIFT_AT-Q)
(LIFT_AT-R)
(CHECKCONSISTENCY-)
(not (LIFT_AT-Q))
(not (NOT-LIFT_AT-R))
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action MOVEDOWN-Q-N
:parameters ()
:precondition
(and
(LIFT_AT-Q)
(NOT-ERROR-)
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(NOT-LIFT_AT-Q)
(LIFT_AT-N)
(CHECKCONSISTENCY-)
(not (LIFT_AT-Q))
(not (NOT-LIFT_AT-N))
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action MOVEDOWN-N-K
:parameters ()
:precondition
(and
(LIFT_AT-N)
(NOT-ERROR-)
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(NOT-LIFT_AT-N)
(LIFT_AT-K)
(CHECKCONSISTENCY-)
(not (LIFT_AT-N))
(not (NOT-LIFT_AT-K))
(not (NOT-CHECKCONSISTENCY-))
)
)
(:derived (Flag51-)(and (NOT-CHECKCONSISTENCY-)(NOT-ERROR-)(LIFT_AT-Q)))

(:derived (Flag52-)(and (NOT-CHECKCONSISTENCY-)(NOT-ERROR-)(LIFT_AT-N)))

(:action STOP-PASS_J-K
:parameters ()
:precondition
(and
(Flag53-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action STOP-PASS_I-Q
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-Q)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action STOP-PASS_I-N
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-N)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action STOP-PASS_O-Q
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-Q)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action STOP-PASS_O-N
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-N)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action STOP-PASS_M-Q
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-Q)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action STOP-PASS_M-N
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-N)
)
:effect
(and
(when
(and
(Flag254-)
)
(and
(BOARDED-PASS_M)
(not (NOT-BOARDED-PASS_M))
)
)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action STOP-PASS_L-Q
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-Q)
)
:effect
(and
(when
(and
(Flag253-)
)
(and
(BOARDED-PASS_L)
(not (NOT-BOARDED-PASS_L))
)
)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action STOP-PASS_L-N
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-N)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action STOP-PASS_F-Q
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-Q)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action STOP-PASS_F-N
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-N)
)
:effect
(and
(when
(and
(Flag252-)
)
(and
(BOARDED-PASS_F)
(not (NOT-BOARDED-PASS_F))
)
)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action STOP-PASS_P-Q
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-Q)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action STOP-PASS_P-N
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-N)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action MOVEUP-N-Q
:parameters ()
:precondition
(and
(LIFT_AT-N)
(NOT-ERROR-)
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(NOT-LIFT_AT-N)
(LIFT_AT-Q)
(CHECKCONSISTENCY-)
(not (LIFT_AT-N))
(not (NOT-LIFT_AT-Q))
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action MOVEUP-K-N
:parameters ()
:precondition
(and
(LIFT_AT-K)
(NOT-ERROR-)
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(NOT-LIFT_AT-K)
(LIFT_AT-N)
(CHECKCONSISTENCY-)
(not (LIFT_AT-K))
(not (NOT-LIFT_AT-N))
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action MOVEDOWN-K-H
:parameters ()
:precondition
(and
(LIFT_AT-K)
(NOT-ERROR-)
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(NOT-LIFT_AT-K)
(LIFT_AT-H)
(CHECKCONSISTENCY-)
(not (LIFT_AT-K))
(not (NOT-LIFT_AT-H))
(not (NOT-CHECKCONSISTENCY-))
)
)
(:derived (Flag53-)(and (NOT-CHECKCONSISTENCY-)(NOT-ERROR-)(LIFT_AT-K)))

(:action STOP-PASS_J-H
:parameters ()
:precondition
(and
(Flag54-)
)
:effect
(and
(when
(and
(BOARDED-PASS_J)
)
(and
(SERVED-PASS_J)
(NOT-BOARDED-PASS_J)
(not (NOT-SERVED-PASS_J))
(not (BOARDED-PASS_J))
)
)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action STOP-PASS_J-G
:parameters ()
:precondition
(and
(Flag55-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action STOP-PASS_J-A
:parameters ()
:precondition
(and
(Flag60-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action STOP-PASS_I-K
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-K)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action STOP-PASS_O-K
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-K)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action STOP-PASS_M-K
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-K)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action STOP-PASS_L-K
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-K)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action STOP-PASS_F-K
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-K)
)
:effect
(and
(when
(and
(BOARDED-PASS_F)
)
(and
(SERVED-PASS_F)
(NOT-BOARDED-PASS_F)
(not (NOT-SERVED-PASS_F))
(not (BOARDED-PASS_F))
)
)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action STOP-PASS_P-K
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-K)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action MOVEUP-H-K
:parameters ()
:precondition
(and
(LIFT_AT-H)
(NOT-ERROR-)
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(NOT-LIFT_AT-H)
(LIFT_AT-K)
(CHECKCONSISTENCY-)
(not (LIFT_AT-H))
(not (NOT-LIFT_AT-K))
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action MOVEDOWN-H-G
:parameters ()
:precondition
(and
(LIFT_AT-H)
(NOT-ERROR-)
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(NOT-LIFT_AT-H)
(LIFT_AT-G)
(CHECKCONSISTENCY-)
(not (LIFT_AT-H))
(not (NOT-LIFT_AT-G))
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action MOVEDOWN-G-E
:parameters ()
:precondition
(and
(LIFT_AT-G)
(NOT-ERROR-)
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(NOT-LIFT_AT-G)
(LIFT_AT-E)
(CHECKCONSISTENCY-)
(not (LIFT_AT-G))
(not (NOT-LIFT_AT-E))
(not (NOT-CHECKCONSISTENCY-))
)
)
(:derived (Flag54-)(and (NOT-CHECKCONSISTENCY-)(NOT-ERROR-)(LIFT_AT-H)))

(:derived (Flag55-)(and (NOT-CHECKCONSISTENCY-)(NOT-ERROR-)(LIFT_AT-G)))

(:derived (Flag60-)(and (NOT-CHECKCONSISTENCY-)(NOT-ERROR-)(LIFT_AT-A)))

(:action STOP-PASS_J-E
:parameters ()
:precondition
(and
(Flag43-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action STOP-PASS_J-D
:parameters ()
:precondition
(and
(Flag56-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action STOP-PASS_J-B
:parameters ()
:precondition
(and
(Flag58-)
)
:effect
(and
(when
(and
(Flag257-)
)
(and
(BOARDED-PASS_J)
(not (NOT-BOARDED-PASS_J))
)
)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action STOP-PASS_I-H
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-H)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action STOP-PASS_I-G
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-G)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action STOP-PASS_I-A
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-A)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action STOP-PASS_O-H
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-H)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action STOP-PASS_O-G
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-G)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action STOP-PASS_O-A
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-A)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action STOP-PASS_M-H
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-H)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action STOP-PASS_M-G
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-G)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action STOP-PASS_M-A
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-A)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action STOP-PASS_L-H
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-H)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action STOP-PASS_L-G
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-G)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action STOP-PASS_L-A
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-A)
)
:effect
(and
(when
(and
(BOARDED-PASS_L)
)
(and
(SERVED-PASS_L)
(NOT-BOARDED-PASS_L)
(not (NOT-SERVED-PASS_L))
(not (BOARDED-PASS_L))
)
)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action STOP-PASS_F-H
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-H)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action STOP-PASS_F-G
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-G)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action STOP-PASS_F-A
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-A)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action STOP-PASS_P-H
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-H)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action STOP-PASS_P-G
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-G)
)
:effect
(and
(when
(and
(Flag251-)
)
(and
(BOARDED-PASS_P)
(not (NOT-BOARDED-PASS_P))
)
)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action STOP-PASS_P-A
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-A)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:derived (Flag2-)(and (Flag1-)))

(:derived (Flag4-)(and (Flag3-)))

(:derived (Flag6-)(and (Flag5-)))

(:derived (Flag8-)(and (Flag7-)))

(:derived (Flag10-)(and (Flag9-)))

(:derived (Flag12-)(and (Flag11-)))

(:derived (Flag14-)(and (Flag13-)))

(:derived (Flag16-)(and (Flag15-)))

(:derived (Flag18-)(and (Flag17-)))

(:derived (Flag20-)(and (Flag19-)))

(:derived (Flag22-)(and (Flag21-)))

(:action MOVEUP-G-H
:parameters ()
:precondition
(and
(LIFT_AT-G)
(NOT-ERROR-)
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(NOT-LIFT_AT-G)
(LIFT_AT-H)
(CHECKCONSISTENCY-)
(not (LIFT_AT-G))
(not (NOT-LIFT_AT-H))
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action MOVEUP-A-B
:parameters ()
:precondition
(and
(LIFT_AT-A)
(NOT-ERROR-)
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(NOT-LIFT_AT-A)
(LIFT_AT-B)
(CHECKCONSISTENCY-)
(not (LIFT_AT-A))
(not (NOT-LIFT_AT-B))
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action MOVEUP-E-G
:parameters ()
:precondition
(and
(LIFT_AT-E)
(NOT-ERROR-)
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(NOT-LIFT_AT-E)
(LIFT_AT-G)
(CHECKCONSISTENCY-)
(not (LIFT_AT-E))
(not (NOT-LIFT_AT-G))
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action MOVEDOWN-E-D
:parameters ()
:precondition
(and
(LIFT_AT-E)
(NOT-ERROR-)
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(NOT-LIFT_AT-E)
(LIFT_AT-D)
(CHECKCONSISTENCY-)
(not (LIFT_AT-E))
(not (NOT-LIFT_AT-D))
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action MOVEDOWN-B-A
:parameters ()
:precondition
(and
(LIFT_AT-B)
(NOT-ERROR-)
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(NOT-LIFT_AT-B)
(LIFT_AT-A)
(CHECKCONSISTENCY-)
(not (LIFT_AT-B))
(not (NOT-LIFT_AT-A))
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action MOVEDOWN-D-C
:parameters ()
:precondition
(and
(LIFT_AT-D)
(NOT-ERROR-)
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(NOT-LIFT_AT-D)
(LIFT_AT-C)
(CHECKCONSISTENCY-)
(not (LIFT_AT-D))
(not (NOT-LIFT_AT-C))
(not (NOT-CHECKCONSISTENCY-))
)
)
(:derived (Flag43-)(and (NOT-CHECKCONSISTENCY-)(NOT-ERROR-)(LIFT_AT-E)))

(:derived (Flag56-)(and (NOT-CHECKCONSISTENCY-)(NOT-ERROR-)(LIFT_AT-D)))

(:derived (Flag58-)(and (NOT-CHECKCONSISTENCY-)(NOT-ERROR-)(LIFT_AT-B)))

(:action STOP-PASS_J-C
:parameters ()
:precondition
(and
(Flag59-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action STOP-PASS_I-E
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-E)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action STOP-PASS_I-D
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-D)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action STOP-PASS_I-B
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-B)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action STOP-PASS_I-C
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-C)
)
:effect
(and
(when
(and
(BOARDED-PASS_I)
)
(and
(SERVED-PASS_I)
(NOT-BOARDED-PASS_I)
(not (NOT-SERVED-PASS_I))
(not (BOARDED-PASS_I))
)
)
(when
(and
(Flag256-)
)
(and
(BOARDED-PASS_I)
(not (NOT-BOARDED-PASS_I))
)
)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action STOP-PASS_O-E
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-E)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action STOP-PASS_O-D
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-D)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action STOP-PASS_O-B
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-B)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action STOP-PASS_O-C
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-C)
)
:effect
(and
(when
(and
(Flag255-)
)
(and
(BOARDED-PASS_O)
(not (NOT-BOARDED-PASS_O))
)
)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action STOP-PASS_M-E
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-E)
)
:effect
(and
(when
(and
(BOARDED-PASS_M)
)
(and
(SERVED-PASS_M)
(NOT-BOARDED-PASS_M)
(not (NOT-SERVED-PASS_M))
(not (BOARDED-PASS_M))
)
)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action STOP-PASS_M-D
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-D)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action STOP-PASS_M-B
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-B)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action STOP-PASS_M-C
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-C)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action STOP-PASS_L-E
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-E)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action STOP-PASS_L-D
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-D)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action STOP-PASS_L-B
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-B)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action STOP-PASS_L-C
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-C)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action STOP-PASS_F-E
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-E)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action STOP-PASS_F-D
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-D)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action STOP-PASS_F-B
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-B)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action STOP-PASS_F-C
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-C)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action STOP-PASS_P-E
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-E)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action STOP-PASS_P-D
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-D)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action STOP-PASS_P-B
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-B)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action STOP-PASS_P-C
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-C)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:derived (Flag1-)(and ))

(:derived (Flag3-)(and ))

(:derived (Flag5-)(and ))

(:derived (Flag7-)(and ))

(:derived (Flag9-)(and ))

(:derived (Flag11-)(and ))

(:derived (Flag13-)(and ))

(:derived (Flag15-)(and ))

(:derived (Flag17-)(and ))

(:derived (Flag19-)(and ))

(:derived (Flag21-)(and ))

(:action CHECKCONSISTENCYACTION
:parameters ()
:precondition
(and
(NOT-ERROR-)
(CHECKCONSISTENCY-)
)
:effect
(and
(NOT-CHECKCONSISTENCY-)
(not (CHECKCONSISTENCY-))
)
)
(:action MOVEUP-D-E
:parameters ()
:precondition
(and
(LIFT_AT-D)
(NOT-ERROR-)
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(NOT-LIFT_AT-D)
(LIFT_AT-E)
(CHECKCONSISTENCY-)
(not (LIFT_AT-D))
(not (NOT-LIFT_AT-E))
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action MOVEUP-B-C
:parameters ()
:precondition
(and
(LIFT_AT-B)
(NOT-ERROR-)
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(NOT-LIFT_AT-B)
(LIFT_AT-C)
(CHECKCONSISTENCY-)
(not (LIFT_AT-B))
(not (NOT-LIFT_AT-C))
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action MOVEUP-C-D
:parameters ()
:precondition
(and
(LIFT_AT-C)
(NOT-ERROR-)
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(NOT-LIFT_AT-C)
(LIFT_AT-D)
(CHECKCONSISTENCY-)
(not (LIFT_AT-C))
(not (NOT-LIFT_AT-D))
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action MOVEDOWN-C-B
:parameters ()
:precondition
(and
(LIFT_AT-C)
(NOT-ERROR-)
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(NOT-LIFT_AT-C)
(LIFT_AT-B)
(CHECKCONSISTENCY-)
(not (LIFT_AT-C))
(not (NOT-LIFT_AT-B))
(not (NOT-CHECKCONSISTENCY-))
)
)
(:derived (Flag59-)(and (NOT-CHECKCONSISTENCY-)(NOT-ERROR-)(LIFT_AT-C)))

(:derived (Flag251-)(and (NOT-SERVED-PASS_P)(NOT-BOARDED-PASS_P)))

(:derived (Flag252-)(and (NOT-SERVED-PASS_F)(NOT-BOARDED-PASS_F)))

(:derived (Flag253-)(and (NOT-SERVED-PASS_L)(NOT-BOARDED-PASS_L)))

(:derived (Flag254-)(and (NOT-SERVED-PASS_M)(NOT-BOARDED-PASS_M)))

(:derived (Flag255-)(and (NOT-SERVED-PASS_O)(NOT-BOARDED-PASS_O)))

(:derived (Flag256-)(and (NOT-SERVED-PASS_I)(NOT-BOARDED-PASS_I)))

(:derived (Flag257-)(and (NOT-SERVED-PASS_J)(NOT-BOARDED-PASS_J)))

)
