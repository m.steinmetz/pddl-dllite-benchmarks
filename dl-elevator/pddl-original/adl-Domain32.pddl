(define
  (domain elevators)
  (:requirements :adl)
  (:predicates
    (passenger ?x )
    (floor ?x )
    (boarded ?x )
    (lift_at ?x )
    (served ?x )
    (origin ?x ?y )
    (next ?x ?y )
    (destin ?x ?y )
    (CheckConsistency)
    (Error)
  )
  (:action moveDown
    :parameters ( ?f1 ?f2 )
    :precondition ( and
      (not (CheckConsistency))
      (not (Error))
      (and
        (lift_at ?f1)
        (next ?f2 ?f1)
      )
    )
    :effect ( and
      (CheckConsistency)
      (when
        (and
          (lift_at ?f1)
          (not
            (=?f1 ?f2 )
          )
        )
        (and
          (lift_at ?f2)
          (not (lift_at ?f1))
        )
      )
    )
  )
  (:action stop
    :parameters ( ?p ?f )
    :precondition ( and
      (not (CheckConsistency))
      (not (Error))
      (and
        (or
          (boarded ?p)
          (passenger ?p)
          (exists (?z_0 )
            (destin ?p ?z_0)
          )
          (exists (?z_0 )
            (origin ?p ?z_0)
          )
          (served ?p)
        )
        (lift_at ?f)
      )
    )
    :effect ( and
      (CheckConsistency)
      (when
        (and
          (origin ?p ?f)
          (not
            (boarded ?p)
          )
          (not
            (served ?p)
          )
        )
        (and
          (boarded ?p)
        )
      )
      (when
        (and
          (destin ?p ?f)
          (boarded ?p)
        )
        (and
          (served ?p)
          (not (boarded ?p))
        )
      )
    )
  )
  (:action moveUp
    :parameters ( ?f1 ?f2 )
    :precondition ( and
      (not (CheckConsistency))
      (not (Error))
      (and
        (next ?f1 ?f2)
        (lift_at ?f1)
      )
    )
    :effect ( and
      (CheckConsistency)
      (when
        (and
          (not
            (=?f1 ?f2 )
          )
          (lift_at ?f1)
        )
        (and
          (lift_at ?f2)
          (not (lift_at ?f1))
        )
      )
    )
  )
  (:action CheckConsistencyAction
    :parameters ( )
    :precondition ( and
      (CheckConsistency)
      (not (Error))
    )
    :effect ( and
      (not (CheckConsistency))
      (when
        (or
          (exists (?x_0 ?z_0 )
            (and
              (destin ?z_0 ?x_0)
              (boarded ?x_0)
            )
          )
          (exists (?x_0 ?z_0 )
            (and
              (destin ?x_0 ?z_0)
              (floor ?x_0)
            )
          )
          (exists (?x_0 ?z_0 ?z_1 )
            (and
              (destin ?z_0 ?x_0)
              (destin ?x_0 ?z_1)
            )
          )
          (exists (?x_0 )
            (and
              (floor ?x_0)
              (passenger ?x_0)
            )
          )
          (exists (?x_0 ?z_0 )
            (and
              (floor ?x_0)
              (origin ?x_0 ?z_0)
            )
          )
          (exists (?y_2 ?y_1 ?x_1 )
            (and
              (next ?x_1 ?y_1)
              (not (= ?y_1 ?y_2))
              (next ?x_1 ?y_2)
            )
          )
          (exists (?x_0 ?z_0 )
            (and
              (origin ?z_0 ?x_0)
              (boarded ?x_0)
            )
          )
          (exists (?z_0 )
            (origin ?z_0 ?z_0)
          )
          (exists (?x_0 )
            (and
              (floor ?x_0)
              (boarded ?x_0)
            )
          )
          (exists (?z_0 )
            (destin ?z_0 ?z_0)
          )
          (exists (?x_0 ?z_0 ?z_1 )
            (and
              (origin ?z_0 ?x_0)
              (origin ?x_0 ?z_1)
            )
          )
          (exists (?x_0 )
            (and
              (lift_at ?x_0)
              (passenger ?x_0)
            )
          )
          (exists (?x_0 )
            (and
              (lift_at ?x_0)
              (served ?x_0)
            )
          )
          (exists (?x_0 ?z_0 ?z_1 )
            (and
              (origin ?z_0 ?x_0)
              (destin ?x_0 ?z_1)
            )
          )
          (exists (?x_0 ?z_0 )
            (and
              (destin ?x_0 ?z_0)
              (lift_at ?x_0)
            )
          )
          (exists (?x_0 ?z_0 )
            (and
              (origin ?z_0 ?x_0)
              (passenger ?x_0)
            )
          )
          (exists (?x_0 ?z_0 )
            (and
              (lift_at ?x_0)
              (origin ?x_0 ?z_0)
            )
          )
          (exists (?x_0 )
            (and
              (lift_at ?x_0)
              (boarded ?x_0)
            )
          )
          (exists (?x_0 )
            (and
              (floor ?x_0)
              (served ?x_0)
            )
          )
          (exists (?x_0 ?z_0 )
            (and
              (origin ?z_0 ?x_0)
              (served ?x_0)
            )
          )
          (exists (?x_0 ?z_0 ?z_1 )
            (and
              (origin ?z_1 ?x_0)
              (origin ?x_0 ?z_0)
            )
          )
          (exists (?x_0 ?z_0 )
            (and
              (destin ?z_0 ?x_0)
              (passenger ?x_0)
            )
          )
          (exists (?x_0 ?z_0 ?z_1 )
            (and
              (destin ?z_1 ?x_0)
              (origin ?x_0 ?z_0)
            )
          )
          (exists (?x_0 ?z_0 )
            (and
              (destin ?z_0 ?x_0)
              (served ?x_0)
            )
          )
        )
        (Error)
      )
    )
  )
)