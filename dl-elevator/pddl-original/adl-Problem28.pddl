(define (problem elevators_problem )
  (:domain elevators)
  (:objects
    aa ac ab pass_az ag pass_ay ai pass_aw pass_at pass_ar pass_as pass_ap an aq pass_aj pass_ak av ae pass_ad ao af ah ba am al pass_bb ax au 
  )
  (:init
    (origin pass_aj ae)
    (destin pass_ay ab)
    (origin pass_ak aa)
    (destin pass_ad ao)
    (origin pass_bb ai)
    (next ao aq)
    (origin pass_ad ae)
    (next am an)
    (next al am)
    (origin pass_ay av)
    (next ah ai)
    (next ac ae)
    (next ab ac)
    (next ax ba)
    (destin pass_ar ai)
    (destin pass_at ab)
    (destin pass_bb aq)
    (next an ao)
    (origin pass_az am)
    (next ai al)
    (next aa ab)
    (next ag ah)
    (destin pass_aw ag)
    (origin pass_as ag)
    (destin pass_ap ae)
    (origin pass_at aa)
    (destin pass_az au)
    (destin pass_as ah)
    (lift_at aa)
    (next ae af)
    (next au av)
    (next aq au)
    (next af ag)
    (next av ax)
    (destin pass_ak ai)
    (destin pass_aj ax)
    (origin pass_ar af)
    (origin pass_aw ax)
    (origin pass_ap av)
  )
  (:goal
    (and
      (not (CheckConsistency))
      (not (Error))
      (not
        (exists ( ?x )
          (and
            (not
              (served ?x)
            )
            (or
              (exists (?z_0 )
                (destin ?x ?z_0)
              )
              (served ?x)
              (passenger ?x)
              (exists (?z_0 )
                (origin ?x ?z_0)
              )
              (boarded ?x)
            )
          )
        )
      )
    )
  )
)