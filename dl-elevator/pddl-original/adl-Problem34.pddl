(define (problem elevators_problem )
  (:domain elevators)
  (:objects
    ad ag af ah ak aj pass_as pass_ap aq pass_ao ar bg at aw pass_ai ay ax pass_ae pass_ab pass_ac pass_aa az av bd be bf pass_au bb bh al pass_bc pass_ba pass_an pass_am 
  )
  (:init
    (next ar at)
    (destin pass_ba ar)
    (next al aq)
    (origin pass_bc ar)
    (origin pass_am ar)
    (origin pass_ba ah)
    (lift_at ay)
    (next ak al)
    (next aj ak)
    (next at av)
    (destin pass_as ay)
    (destin pass_aa bg)
    (next bf bg)
    (origin pass_ac al)
    (next ah aj)
    (next bd be)
    (origin pass_an bd)
    (next af ag)
    (destin pass_au af)
    (next ag ah)
    (next bg bh)
    (destin pass_ab al)
    (next az bb)
    (destin pass_bc at)
    (origin pass_ab bd)
    (next ax ay)
    (destin pass_ai aw)
    (origin pass_au aw)
    (origin pass_as af)
    (next ay az)
    (next aq ar)
    (next ad af)
    (origin pass_ae be)
    (destin pass_an ar)
    (destin pass_ap aq)
    (origin pass_ai av)
    (next aw ax)
    (next av aw)
    (destin pass_ao aw)
    (destin pass_ac aq)
    (origin pass_ao ag)
    (next be bf)
    (next bb bd)
    (origin pass_aa ag)
    (destin pass_ae ad)
    (origin pass_ap aw)
    (destin pass_am aj)
  )
  (:goal
    (and
      (not (CheckConsistency))
      (not (Error))
      (not
        (exists ( ?x )
          (and
            (not
              (served ?x)
            )
            (or
              (exists (?z_0 )
                (destin ?x ?z_0)
              )
              (served ?x)
              (boarded ?x)
              (exists (?z_0 )
                (origin ?x ?z_0)
              )
              (passenger ?x)
            )
          )
        )
      )
    )
  )
)