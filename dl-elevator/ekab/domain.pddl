(define (domain elevators)
  (:requirements :ekab)

(:predicates 
	(passenger ?p)
	(boarded ?p)
	(served ?p)
	(floor ?p)
	(origin ?p ?f)
	(destin ?p ?f)
 	(lift_at ?f)
	(next ?f1 ?f2)
)

(:axioms
  (isA lift_at floor)
  (isA boarded passenger)
  (isA served passenger)
  (isA passenger (not floor))
  (isA (exists origin) passenger)
  (isA (exists destin) passenger)
  (isA (exists (inverse origin)) floor)
  (isA (exists (inverse destin)) floor)
  (funct next)
)

(:rule rule_stop
:condition (and (mko (lift_at ?f)) (mko (passenger ?p)))
:action stop
)

(:rule rule_moveUp
:condition (and (mko (lift_at ?f1)) (mko (next ?f1 ?f2)))
:action moveUp
)

(:rule rule_moveDown
:condition (and (mko (lift_at ?f1)) (mko (next ?f2 ?f1)))
:action moveDown
)

(:action stop
  :parameters (?p ?f)
  :effects (
    :condition (and (mko (origin ?p ?f)) (not (mko (served ?p))) (not (mko (boarded ?p))))
    :add ((boarded ?p))
  )
  (
    :condition (mko (and (destin ?p ?f) (boarded ?p)))
    :add ((served ?p))
    :delete ((boarded ?p))
  )
)

(:action moveUp
  :parameters (?f1 ?f2)
  :effects (
    :condition (and (mko (lift_at ?f1)) (not (mko-eq ?f1 ?f2)))
    :add ((lift_at ?f2))
    :delete ((lift_at ?f1))
)
)

(:action moveDown
  :parameters (?f1 ?f2)
  :effects (
    :condition (and (mko (lift_at ?f1)) (not (mko-eq ?f1 ?f2)))
    :add ((lift_at ?f2))
    :delete ((lift_at ?f1))
)
)
  
)
