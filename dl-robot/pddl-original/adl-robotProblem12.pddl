(define (problem robotProblem )
  (:domain robot)
  (:objects
    robot 
  )
  (:init
    (BelowOf11 robot)
    (AboveOf0 robot)
    (RightOf2 robot)
    (LeftOf11 robot)
  )
  (:goal
    (and
      (not (CheckConsistency))
      (not (Error))
      (Row1 robot)
      (Column2 robot)
    )
  )
)