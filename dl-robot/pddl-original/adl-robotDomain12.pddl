(define
  (domain robot)
  (:requirements :adl)
  (:predicates
    (LeftOf3 ?x )
    (LeftOf2 ?x )
    (RightOf9 ?x )
    (Rows ?x )
    (LeftOf7 ?x )
    (LeftOf6 ?x )
    (LeftOf1 ?x )
    (LeftOf4 ?x )
    (RightOf3 ?x )
    (RightOf2 ?x )
    (RightOf1 ?x )
    (RightOf0 ?x )
    (RightOf7 ?x )
    (RightOf6 ?x )
    (RightOf5 ?x )
    (RightOf4 ?x )
    (BelowOf8 ?x )
    (RightOf8 ?x )
    (Row3 ?x )
    (BelowOf9 ?x )
    (Columns ?x )
    (BelowOf12 ?x )
    (LeftOf5 ?x )
    (AboveOf11 ?x )
    (BelowOf10 ?x )
    (BelowOf11 ?x )
    (Row10 ?x )
    (Row11 ?x )
    (LeftOf9 ?x )
    (RightOf11 ?x )
    (RightOf10 ?x )
    (Row0 ?x )
    (Row1 ?x )
    (Row2 ?x )
    (LeftOf8 ?x )
    (Row4 ?x )
    (Row5 ?x )
    (Row6 ?x )
    (Row7 ?x )
    (Row8 ?x )
    (Row9 ?x )
    (AboveOf3 ?x )
    (AboveOf1 ?x )
    (BelowOf1 ?x )
    (AboveOf10 ?x )
    (BelowOf3 ?x )
    (Column9 ?x )
    (Column8 ?x )
    (BelowOf6 ?x )
    (BelowOf7 ?x )
    (Column5 ?x )
    (Column4 ?x )
    (Column7 ?x )
    (Column6 ?x )
    (Column1 ?x )
    (Column0 ?x )
    (Column3 ?x )
    (Column2 ?x )
    (AboveOf2 ?x )
    (Column11 ?x )
    (Column10 ?x )
    (BelowOf2 ?x )
    (AboveOf4 ?x )
    (AboveOf5 ?x )
    (AboveOf6 ?x )
    (AboveOf7 ?x )
    (AboveOf0 ?x )
    (LeftOf12 ?x )
    (LeftOf11 ?x )
    (LeftOf10 ?x )
    (BelowOf4 ?x )
    (AboveOf8 ?x )
    (AboveOf9 ?x )
    (BelowOf5 ?x )
    (CheckConsistency)
    (Error)
  )
  (:action moveDown
    :parameters ( ?x )
    :precondition ( and
      (not (CheckConsistency))
      (not (Error))
      (or
        (BelowOf7 ?x)
        (AboveOf9 ?x)
        (BelowOf1 ?x)
        (AboveOf7 ?x)
        (AboveOf11 ?x)
        (AboveOf1 ?x)
        (BelowOf8 ?x)
        (BelowOf3 ?x)
        (AboveOf3 ?x)
        (BelowOf9 ?x)
        (BelowOf2 ?x)
        (BelowOf5 ?x)
        (BelowOf11 ?x)
        (BelowOf10 ?x)
        (BelowOf4 ?x)
        (AboveOf10 ?x)
        (AboveOf0 ?x)
        (AboveOf8 ?x)
        (AboveOf6 ?x)
        (AboveOf5 ?x)
        (Rows ?x)
        (AboveOf4 ?x)
        (BelowOf12 ?x)
        (BelowOf6 ?x)
        (AboveOf2 ?x)
      )
    )
    :effect ( and
      (CheckConsistency)
      (when
        (or
          (BelowOf1 ?x)
          (BelowOf2 ?x)
        )
        (and
          (BelowOf1 ?x)
        )
      )
      (when
        (or
          (BelowOf3 ?x)
          (BelowOf1 ?x)
          (BelowOf2 ?x)
        )
        (and
          (BelowOf2 ?x)
        )
      )
      (when
        (or
          (BelowOf4 ?x)
          (BelowOf3 ?x)
          (BelowOf1 ?x)
          (BelowOf2 ?x)
        )
        (and
          (BelowOf3 ?x)
        )
      )
      (when
        (or
          (BelowOf5 ?x)
          (BelowOf4 ?x)
          (BelowOf3 ?x)
          (BelowOf1 ?x)
          (BelowOf2 ?x)
        )
        (and
          (BelowOf4 ?x)
        )
      )
      (when
        (or
          (BelowOf3 ?x)
          (BelowOf4 ?x)
          (BelowOf1 ?x)
          (BelowOf2 ?x)
          (BelowOf5 ?x)
          (BelowOf6 ?x)
        )
        (and
          (BelowOf5 ?x)
        )
      )
      (when
        (or
          (BelowOf3 ?x)
          (BelowOf7 ?x)
          (BelowOf4 ?x)
          (BelowOf1 ?x)
          (BelowOf2 ?x)
          (BelowOf5 ?x)
          (BelowOf6 ?x)
        )
        (and
          (BelowOf6 ?x)
        )
      )
      (when
        (or
          (BelowOf8 ?x)
          (BelowOf3 ?x)
          (BelowOf7 ?x)
          (BelowOf4 ?x)
          (BelowOf1 ?x)
          (BelowOf2 ?x)
          (BelowOf5 ?x)
          (BelowOf6 ?x)
        )
        (and
          (BelowOf7 ?x)
        )
      )
      (when
        (or
          (BelowOf8 ?x)
          (BelowOf3 ?x)
          (BelowOf9 ?x)
          (BelowOf7 ?x)
          (BelowOf4 ?x)
          (BelowOf1 ?x)
          (BelowOf2 ?x)
          (BelowOf5 ?x)
          (BelowOf6 ?x)
        )
        (and
          (BelowOf8 ?x)
        )
      )
      (when
        (or
          (BelowOf8 ?x)
          (BelowOf3 ?x)
          (BelowOf9 ?x)
          (BelowOf7 ?x)
          (BelowOf4 ?x)
          (BelowOf1 ?x)
          (BelowOf2 ?x)
          (BelowOf5 ?x)
          (BelowOf6 ?x)
          (BelowOf10 ?x)
        )
        (and
          (BelowOf9 ?x)
        )
      )
      (when
        (or
          (BelowOf8 ?x)
          (BelowOf3 ?x)
          (BelowOf9 ?x)
          (BelowOf7 ?x)
          (BelowOf4 ?x)
          (BelowOf1 ?x)
          (BelowOf2 ?x)
          (BelowOf5 ?x)
          (BelowOf6 ?x)
          (BelowOf11 ?x)
          (BelowOf10 ?x)
        )
        (and
          (BelowOf10 ?x)
        )
      )
      (when
        (or
          (BelowOf8 ?x)
          (BelowOf3 ?x)
          (BelowOf9 ?x)
          (BelowOf7 ?x)
          (BelowOf12 ?x)
          (BelowOf4 ?x)
          (BelowOf1 ?x)
          (BelowOf2 ?x)
          (BelowOf5 ?x)
          (BelowOf6 ?x)
          (BelowOf11 ?x)
          (BelowOf10 ?x)
        )
        (and
          (BelowOf11 ?x)
        )
      )
      (when
        (or
          (AboveOf1 ?x)
          (AboveOf5 ?x)
          (AboveOf3 ?x)
          (AboveOf4 ?x)
          (AboveOf9 ?x)
          (AboveOf8 ?x)
          (AboveOf7 ?x)
          (AboveOf2 ?x)
          (AboveOf10 ?x)
          (AboveOf11 ?x)
          (AboveOf6 ?x)
        )
        (and
          (AboveOf0 ?x)
          (not (AboveOf1 ?x))
        )
      )
      (when
        (or
          (AboveOf5 ?x)
          (AboveOf3 ?x)
          (AboveOf4 ?x)
          (AboveOf9 ?x)
          (AboveOf8 ?x)
          (AboveOf7 ?x)
          (AboveOf2 ?x)
          (AboveOf10 ?x)
          (AboveOf6 ?x)
          (AboveOf11 ?x)
        )
        (and
          (AboveOf1 ?x)
          (not (AboveOf2 ?x))
        )
      )
      (when
        (or
          (AboveOf5 ?x)
          (AboveOf3 ?x)
          (AboveOf4 ?x)
          (AboveOf9 ?x)
          (AboveOf7 ?x)
          (AboveOf8 ?x)
          (AboveOf10 ?x)
          (AboveOf6 ?x)
          (AboveOf11 ?x)
        )
        (and
          (AboveOf2 ?x)
          (not (AboveOf3 ?x))
        )
      )
      (when
        (or
          (AboveOf5 ?x)
          (AboveOf4 ?x)
          (AboveOf9 ?x)
          (AboveOf7 ?x)
          (AboveOf8 ?x)
          (AboveOf10 ?x)
          (AboveOf6 ?x)
          (AboveOf11 ?x)
        )
        (and
          (AboveOf3 ?x)
          (not (AboveOf4 ?x))
        )
      )
      (when
        (or
          (AboveOf5 ?x)
          (AboveOf9 ?x)
          (AboveOf10 ?x)
          (AboveOf8 ?x)
          (AboveOf7 ?x)
          (AboveOf6 ?x)
          (AboveOf11 ?x)
        )
        (and
          (AboveOf4 ?x)
          (not (AboveOf5 ?x))
        )
      )
      (when
        (or
          (AboveOf9 ?x)
          (AboveOf10 ?x)
          (AboveOf8 ?x)
          (AboveOf7 ?x)
          (AboveOf6 ?x)
          (AboveOf11 ?x)
        )
        (and
          (AboveOf5 ?x)
          (not (AboveOf6 ?x))
        )
      )
      (when
        (or
          (AboveOf10 ?x)
          (AboveOf9 ?x)
          (AboveOf7 ?x)
          (AboveOf11 ?x)
          (AboveOf8 ?x)
        )
        (and
          (AboveOf6 ?x)
          (not (AboveOf7 ?x))
        )
      )
      (when
        (or
          (AboveOf9 ?x)
          (AboveOf10 ?x)
          (AboveOf11 ?x)
          (AboveOf8 ?x)
        )
        (and
          (AboveOf7 ?x)
          (not (AboveOf8 ?x))
        )
      )
      (when
        (or
          (AboveOf9 ?x)
          (AboveOf10 ?x)
          (AboveOf11 ?x)
        )
        (and
          (AboveOf8 ?x)
          (not (AboveOf9 ?x))
        )
      )
      (when
        (or
          (AboveOf11 ?x)
          (AboveOf10 ?x)
        )
        (and
          (AboveOf9 ?x)
          (not (AboveOf10 ?x))
        )
      )
      (when
        (AboveOf11 ?x)
        (and
          (AboveOf10 ?x)
          (not (AboveOf11 ?x))
        )
      )
      (when
        (Row1 ?x)
        (and
          (Row0 ?x)
          (not (Row1 ?x))
        )
      )
      (when
        (Row2 ?x)
        (and
          (Row1 ?x)
          (not (Row2 ?x))
        )
      )
      (when
        (Row3 ?x)
        (and
          (Row2 ?x)
          (not (Row3 ?x))
        )
      )
      (when
        (Row4 ?x)
        (and
          (Row3 ?x)
          (not (Row4 ?x))
        )
      )
      (when
        (Row5 ?x)
        (and
          (Row4 ?x)
          (not (Row5 ?x))
        )
      )
      (when
        (Row6 ?x)
        (and
          (Row5 ?x)
          (not (Row6 ?x))
        )
      )
      (when
        (Row7 ?x)
        (and
          (Row6 ?x)
          (not (Row7 ?x))
        )
      )
      (when
        (Row8 ?x)
        (and
          (Row7 ?x)
          (not (Row8 ?x))
        )
      )
      (when
        (Row9 ?x)
        (and
          (Row8 ?x)
          (not (Row9 ?x))
        )
      )
      (when
        (Row10 ?x)
        (and
          (Row9 ?x)
          (not (Row10 ?x))
        )
      )
      (when
        (Row11 ?x)
        (and
          (Row10 ?x)
          (not (Row11 ?x))
        )
      )
      (when
        (or
          (and
            (BelowOf2 ?x)
            (AboveOf3 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf8 ?x)
          )
          (and
            (AboveOf5 ?x)
            (BelowOf2 ?x)
          )
          (and
            (AboveOf4 ?x)
            (BelowOf1 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf8 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf10 ?x)
          )
          (and
            (AboveOf9 ?x)
            (BelowOf1 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf3 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf11 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf10 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf6 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf9 ?x)
          )
          (and
            (AboveOf2 ?x)
            (BelowOf1 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf6 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf4 ?x)
          )
          (and
            (AboveOf5 ?x)
            (BelowOf1 ?x)
          )
          (and
            (AboveOf1 ?x)
            (BelowOf1 ?x)
          )
          (and
            (AboveOf7 ?x)
            (BelowOf1 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf7 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf1 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf11 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf2 ?x)
          )
        )
        (and
          (Row0 ?x)
        )
      )
      (when
        (or
          (and
            (BelowOf2 ?x)
            (AboveOf3 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf8 ?x)
          )
          (and
            (AboveOf5 ?x)
            (BelowOf2 ?x)
          )
          (and
            (BelowOf3 ?x)
            (AboveOf11 ?x)
          )
          (and
            (AboveOf4 ?x)
            (BelowOf1 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf8 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf10 ?x)
          )
          (and
            (AboveOf9 ?x)
            (BelowOf1 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf3 ?x)
          )
          (and
            (AboveOf2 ?x)
            (BelowOf2 ?x)
          )
          (and
            (AboveOf10 ?x)
            (BelowOf3 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf10 ?x)
          )
          (and
            (AboveOf4 ?x)
            (BelowOf3 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf6 ?x)
          )
          (and
            (BelowOf3 ?x)
            (AboveOf8 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf9 ?x)
          )
          (and
            (BelowOf3 ?x)
            (AboveOf3 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf6 ?x)
          )
          (and
            (AboveOf2 ?x)
            (BelowOf3 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf4 ?x)
          )
          (and
            (AboveOf9 ?x)
            (BelowOf3 ?x)
          )
          (and
            (AboveOf5 ?x)
            (BelowOf1 ?x)
          )
          (and
            (AboveOf7 ?x)
            (BelowOf1 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf7 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf11 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf11 ?x)
          )
          (and
            (AboveOf7 ?x)
            (BelowOf3 ?x)
          )
          (and
            (AboveOf2 ?x)
            (BelowOf1 ?x)
          )
          (and
            (AboveOf6 ?x)
            (BelowOf3 ?x)
          )
          (and
            (AboveOf5 ?x)
            (BelowOf3 ?x)
          )
        )
        (and
          (Row1 ?x)
        )
      )
      (when
        (or
          (and
            (BelowOf2 ?x)
            (AboveOf3 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf8 ?x)
          )
          (and
            (AboveOf5 ?x)
            (BelowOf2 ?x)
          )
          (and
            (BelowOf3 ?x)
            (AboveOf11 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf11 ?x)
          )
          (and
            (AboveOf11 ?x)
            (BelowOf3 ?x)
          )
          (and
            (AboveOf4 ?x)
            (BelowOf1 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf8 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf10 ?x)
          )
          (and
            (AboveOf9 ?x)
            (BelowOf1 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf3 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf10 ?x)
          )
          (and
            (AboveOf10 ?x)
            (BelowOf3 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf10 ?x)
          )
          (and
            (AboveOf4 ?x)
            (BelowOf3 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf6 ?x)
          )
          (and
            (BelowOf3 ?x)
            (AboveOf8 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf9 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf6 ?x)
          )
          (and
            (AboveOf3 ?x)
            (BelowOf3 ?x)
          )
          (and
            (AboveOf5 ?x)
            (BelowOf4 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf4 ?x)
          )
          (and
            (AboveOf7 ?x)
            (BelowOf4 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf8 ?x)
          )
          (and
            (AboveOf4 ?x)
            (BelowOf4 ?x)
          )
          (and
            (AboveOf5 ?x)
            (BelowOf1 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf9 ?x)
          )
          (and
            (AboveOf7 ?x)
            (BelowOf1 ?x)
          )
          (and
            (AboveOf9 ?x)
            (BelowOf3 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf7 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf11 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf11 ?x)
          )
          (and
            (AboveOf7 ?x)
            (BelowOf3 ?x)
          )
          (and
            (AboveOf6 ?x)
            (BelowOf3 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf6 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf3 ?x)
          )
          (and
            (AboveOf5 ?x)
            (BelowOf3 ?x)
          )
        )
        (and
          (Row2 ?x)
        )
      )
      (when
        (or
          (and
            (AboveOf10 ?x)
            (BelowOf3 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf8 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf6 ?x)
          )
          (and
            (BelowOf3 ?x)
            (AboveOf11 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf11 ?x)
          )
          (and
            (AboveOf11 ?x)
            (BelowOf3 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf9 ?x)
          )
          (and
            (AboveOf4 ?x)
            (BelowOf1 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf8 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf10 ?x)
          )
          (and
            (AboveOf9 ?x)
            (BelowOf1 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf6 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf10 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf11 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf10 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf7 ?x)
          )
          (and
            (AboveOf4 ?x)
            (BelowOf3 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf5 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf6 ?x)
          )
          (and
            (BelowOf3 ?x)
            (AboveOf8 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf9 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf6 ?x)
          )
          (and
            (AboveOf5 ?x)
            (BelowOf4 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf4 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf11 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf10 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf8 ?x)
          )
          (and
            (AboveOf4 ?x)
            (BelowOf4 ?x)
          )
          (and
            (AboveOf5 ?x)
            (BelowOf1 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf9 ?x)
          )
          (and
            (AboveOf5 ?x)
            (BelowOf2 ?x)
          )
          (and
            (AboveOf7 ?x)
            (BelowOf1 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf8 ?x)
          )
          (and
            (AboveOf9 ?x)
            (BelowOf3 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf7 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf11 ?x)
          )
          (and
            (AboveOf7 ?x)
            (BelowOf3 ?x)
          )
          (and
            (AboveOf6 ?x)
            (BelowOf3 ?x)
          )
          (and
            (AboveOf7 ?x)
            (BelowOf4 ?x)
          )
          (and
            (AboveOf5 ?x)
            (BelowOf3 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf4 ?x)
          )
        )
        (and
          (Row3 ?x)
        )
      )
      (when
        (or
          (and
            (AboveOf10 ?x)
            (BelowOf3 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf8 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf6 ?x)
          )
          (and
            (BelowOf3 ?x)
            (AboveOf11 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf11 ?x)
          )
          (and
            (AboveOf10 ?x)
            (BelowOf6 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf11 ?x)
          )
          (and
            (AboveOf11 ?x)
            (BelowOf3 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf9 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf8 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf10 ?x)
          )
          (and
            (AboveOf9 ?x)
            (BelowOf6 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf6 ?x)
          )
          (and
            (BelowOf6 ?x)
            (AboveOf8 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf10 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf11 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf10 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf7 ?x)
          )
          (and
            (AboveOf6 ?x)
            (BelowOf6 ?x)
          )
          (and
            (BelowOf3 ?x)
            (AboveOf8 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf6 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf6 ?x)
          )
          (and
            (AboveOf5 ?x)
            (BelowOf4 ?x)
          )
          (and
            (AboveOf7 ?x)
            (BelowOf4 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf10 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf8 ?x)
          )
          (and
            (AboveOf9 ?x)
            (BelowOf3 ?x)
          )
          (and
            (AboveOf5 ?x)
            (BelowOf5 ?x)
          )
          (and
            (AboveOf7 ?x)
            (BelowOf6 ?x)
          )
          (and
            (AboveOf5 ?x)
            (BelowOf1 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf9 ?x)
          )
          (and
            (AboveOf5 ?x)
            (BelowOf2 ?x)
          )
          (and
            (AboveOf7 ?x)
            (BelowOf1 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf9 ?x)
          )
          (and
            (AboveOf9 ?x)
            (BelowOf1 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf8 ?x)
          )
          (and
            (AboveOf5 ?x)
            (BelowOf6 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf7 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf11 ?x)
          )
          (and
            (AboveOf7 ?x)
            (BelowOf3 ?x)
          )
          (and
            (AboveOf6 ?x)
            (BelowOf3 ?x)
          )
          (and
            (BelowOf6 ?x)
            (AboveOf11 ?x)
          )
          (and
            (AboveOf5 ?x)
            (BelowOf3 ?x)
          )
        )
        (and
          (Row4 ?x)
        )
      )
      (when
        (or
          (and
            (AboveOf10 ?x)
            (BelowOf3 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf8 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf6 ?x)
          )
          (and
            (BelowOf3 ?x)
            (AboveOf11 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf11 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf11 ?x)
          )
          (and
            (BelowOf7 ?x)
            (AboveOf11 ?x)
          )
          (and
            (BelowOf7 ?x)
            (AboveOf8 ?x)
          )
          (and
            (BelowOf7 ?x)
            (AboveOf7 ?x)
          )
          (and
            (AboveOf11 ?x)
            (BelowOf3 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf9 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf8 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf10 ?x)
          )
          (and
            (AboveOf9 ?x)
            (BelowOf6 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf6 ?x)
          )
          (and
            (BelowOf6 ?x)
            (AboveOf8 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf10 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf11 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf10 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf7 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf6 ?x)
          )
          (and
            (BelowOf3 ?x)
            (AboveOf8 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf9 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf6 ?x)
          )
          (and
            (BelowOf6 ?x)
            (AboveOf10 ?x)
          )
          (and
            (AboveOf7 ?x)
            (BelowOf4 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf10 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf8 ?x)
          )
          (and
            (AboveOf9 ?x)
            (BelowOf3 ?x)
          )
          (and
            (AboveOf7 ?x)
            (BelowOf6 ?x)
          )
          (and
            (BelowOf7 ?x)
            (AboveOf10 ?x)
          )
          (and
            (AboveOf7 ?x)
            (BelowOf1 ?x)
          )
          (and
            (AboveOf9 ?x)
            (BelowOf1 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf8 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf7 ?x)
          )
          (and
            (BelowOf6 ?x)
            (AboveOf6 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf11 ?x)
          )
          (and
            (BelowOf7 ?x)
            (AboveOf9 ?x)
          )
          (and
            (AboveOf7 ?x)
            (BelowOf3 ?x)
          )
          (and
            (AboveOf10 ?x)
            (BelowOf6 ?x)
          )
          (and
            (AboveOf6 ?x)
            (BelowOf3 ?x)
          )
          (and
            (BelowOf6 ?x)
            (AboveOf11 ?x)
          )
          (and
            (BelowOf7 ?x)
            (AboveOf6 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf9 ?x)
          )
        )
        (and
          (Row5 ?x)
        )
      )
      (when
        (or
          (and
            (AboveOf11 ?x)
            (BelowOf8 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf8 ?x)
          )
          (and
            (BelowOf3 ?x)
            (AboveOf11 ?x)
          )
          (and
            (AboveOf7 ?x)
            (BelowOf8 ?x)
          )
          (and
            (BelowOf7 ?x)
            (AboveOf11 ?x)
          )
          (and
            (BelowOf7 ?x)
            (AboveOf8 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf11 ?x)
          )
          (and
            (AboveOf11 ?x)
            (BelowOf3 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf9 ?x)
          )
          (and
            (AboveOf7 ?x)
            (BelowOf7 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf8 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf10 ?x)
          )
          (and
            (AboveOf9 ?x)
            (BelowOf6 ?x)
          )
          (and
            (BelowOf6 ?x)
            (AboveOf8 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf10 ?x)
          )
          (and
            (AboveOf10 ?x)
            (BelowOf3 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf10 ?x)
          )
          (and
            (AboveOf9 ?x)
            (BelowOf8 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf7 ?x)
          )
          (and
            (BelowOf3 ?x)
            (AboveOf8 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf9 ?x)
          )
          (and
            (BelowOf8 ?x)
            (AboveOf8 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf11 ?x)
          )
          (and
            (BelowOf6 ?x)
            (AboveOf10 ?x)
          )
          (and
            (AboveOf7 ?x)
            (BelowOf4 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf10 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf8 ?x)
          )
          (and
            (AboveOf9 ?x)
            (BelowOf3 ?x)
          )
          (and
            (AboveOf10 ?x)
            (BelowOf8 ?x)
          )
          (and
            (AboveOf7 ?x)
            (BelowOf6 ?x)
          )
          (and
            (BelowOf7 ?x)
            (AboveOf10 ?x)
          )
          (and
            (AboveOf7 ?x)
            (BelowOf1 ?x)
          )
          (and
            (AboveOf9 ?x)
            (BelowOf1 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf8 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf7 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf11 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf11 ?x)
          )
          (and
            (BelowOf7 ?x)
            (AboveOf9 ?x)
          )
          (and
            (AboveOf7 ?x)
            (BelowOf3 ?x)
          )
          (and
            (AboveOf10 ?x)
            (BelowOf6 ?x)
          )
          (and
            (BelowOf6 ?x)
            (AboveOf11 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf9 ?x)
          )
        )
        (and
          (Row6 ?x)
        )
      )
      (when
        (or
          (and
            (AboveOf11 ?x)
            (BelowOf8 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf8 ?x)
          )
          (and
            (AboveOf9 ?x)
            (BelowOf9 ?x)
          )
          (and
            (BelowOf3 ?x)
            (AboveOf11 ?x)
          )
          (and
            (BelowOf7 ?x)
            (AboveOf11 ?x)
          )
          (and
            (BelowOf9 ?x)
            (AboveOf11 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf11 ?x)
          )
          (and
            (BelowOf9 ?x)
            (AboveOf8 ?x)
          )
          (and
            (AboveOf11 ?x)
            (BelowOf3 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf9 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf8 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf10 ?x)
          )
          (and
            (AboveOf9 ?x)
            (BelowOf6 ?x)
          )
          (and
            (BelowOf6 ?x)
            (AboveOf8 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf10 ?x)
          )
          (and
            (AboveOf10 ?x)
            (BelowOf3 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf10 ?x)
          )
          (and
            (AboveOf9 ?x)
            (BelowOf8 ?x)
          )
          (and
            (BelowOf3 ?x)
            (AboveOf8 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf9 ?x)
          )
          (and
            (BelowOf6 ?x)
            (AboveOf10 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf11 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf10 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf8 ?x)
          )
          (and
            (AboveOf9 ?x)
            (BelowOf3 ?x)
          )
          (and
            (AboveOf10 ?x)
            (BelowOf8 ?x)
          )
          (and
            (BelowOf7 ?x)
            (AboveOf10 ?x)
          )
          (and
            (AboveOf9 ?x)
            (BelowOf1 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf8 ?x)
          )
          (and
            (AboveOf8 ?x)
            (BelowOf8 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf11 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf11 ?x)
          )
          (and
            (BelowOf7 ?x)
            (AboveOf9 ?x)
          )
          (and
            (BelowOf9 ?x)
            (AboveOf10 ?x)
          )
          (and
            (AboveOf10 ?x)
            (BelowOf6 ?x)
          )
          (and
            (BelowOf6 ?x)
            (AboveOf11 ?x)
          )
          (and
            (BelowOf7 ?x)
            (AboveOf8 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf9 ?x)
          )
        )
        (and
          (Row7 ?x)
        )
      )
      (when
        (or
          (and
            (AboveOf11 ?x)
            (BelowOf8 ?x)
          )
          (and
            (BelowOf3 ?x)
            (AboveOf11 ?x)
          )
          (and
            (AboveOf10 ?x)
            (BelowOf6 ?x)
          )
          (and
            (BelowOf9 ?x)
            (AboveOf11 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf11 ?x)
          )
          (and
            (AboveOf11 ?x)
            (BelowOf3 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf9 ?x)
          )
          (and
            (BelowOf10 ?x)
            (AboveOf11 ?x)
          )
          (and
            (BelowOf9 ?x)
            (AboveOf9 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf10 ?x)
          )
          (and
            (AboveOf9 ?x)
            (BelowOf6 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf10 ?x)
          )
          (and
            (AboveOf10 ?x)
            (BelowOf3 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf10 ?x)
          )
          (and
            (AboveOf9 ?x)
            (BelowOf8 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf9 ?x)
          )
          (and
            (AboveOf10 ?x)
            (BelowOf10 ?x)
          )
          (and
            (BelowOf6 ?x)
            (AboveOf10 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf11 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf10 ?x)
          )
          (and
            (AboveOf9 ?x)
            (BelowOf3 ?x)
          )
          (and
            (AboveOf10 ?x)
            (BelowOf8 ?x)
          )
          (and
            (BelowOf7 ?x)
            (AboveOf10 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf11 ?x)
          )
          (and
            (AboveOf9 ?x)
            (BelowOf1 ?x)
          )
          (and
            (AboveOf9 ?x)
            (BelowOf10 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf11 ?x)
          )
          (and
            (BelowOf7 ?x)
            (AboveOf9 ?x)
          )
          (and
            (BelowOf9 ?x)
            (AboveOf10 ?x)
          )
          (and
            (BelowOf7 ?x)
            (AboveOf11 ?x)
          )
          (and
            (BelowOf6 ?x)
            (AboveOf11 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf9 ?x)
          )
        )
        (and
          (Row8 ?x)
        )
      )
      (when
        (or
          (and
            (AboveOf11 ?x)
            (BelowOf8 ?x)
          )
          (and
            (BelowOf3 ?x)
            (AboveOf11 ?x)
          )
          (and
            (BelowOf7 ?x)
            (AboveOf11 ?x)
          )
          (and
            (BelowOf9 ?x)
            (AboveOf11 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf11 ?x)
          )
          (and
            (AboveOf11 ?x)
            (BelowOf3 ?x)
          )
          (and
            (BelowOf10 ?x)
            (AboveOf11 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf10 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf10 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf11 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf10 ?x)
          )
          (and
            (AboveOf10 ?x)
            (BelowOf10 ?x)
          )
          (and
            (BelowOf6 ?x)
            (AboveOf10 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf11 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf10 ?x)
          )
          (and
            (AboveOf10 ?x)
            (BelowOf8 ?x)
          )
          (and
            (BelowOf7 ?x)
            (AboveOf10 ?x)
          )
          (and
            (BelowOf11 ?x)
            (AboveOf11 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf11 ?x)
          )
          (and
            (AboveOf10 ?x)
            (BelowOf11 ?x)
          )
          (and
            (BelowOf9 ?x)
            (AboveOf10 ?x)
          )
          (and
            (BelowOf6 ?x)
            (AboveOf11 ?x)
          )
          (and
            (AboveOf10 ?x)
            (BelowOf3 ?x)
          )
        )
        (and
          (Row9 ?x)
        )
      )
      (when
        (or
          (and
            (BelowOf11 ?x)
            (AboveOf11 ?x)
          )
          (and
            (AboveOf11 ?x)
            (BelowOf8 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf11 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf11 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf11 ?x)
          )
          (and
            (BelowOf6 ?x)
            (AboveOf11 ?x)
          )
          (and
            (BelowOf7 ?x)
            (AboveOf11 ?x)
          )
          (and
            (BelowOf9 ?x)
            (AboveOf11 ?x)
          )
          (and
            (AboveOf11 ?x)
            (BelowOf12 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf11 ?x)
          )
          (and
            (AboveOf11 ?x)
            (BelowOf3 ?x)
          )
          (and
            (BelowOf10 ?x)
            (AboveOf11 ?x)
          )
        )
        (and
          (Row10 ?x)
        )
      )
    )
  )
  (:action moveLeft
    :parameters ( ?x )
    :precondition ( and
      (not (CheckConsistency))
      (not (Error))
      (or
        (LeftOf3 ?x)
        (RightOf7 ?x)
        (RightOf8 ?x)
        (LeftOf2 ?x)
        (LeftOf9 ?x)
        (RightOf2 ?x)
        (RightOf0 ?x)
        (LeftOf8 ?x)
        (LeftOf5 ?x)
        (RightOf1 ?x)
        (LeftOf12 ?x)
        (Columns ?x)
        (RightOf6 ?x)
        (RightOf9 ?x)
        (LeftOf6 ?x)
        (RightOf10 ?x)
        (RightOf11 ?x)
        (LeftOf11 ?x)
        (RightOf5 ?x)
        (LeftOf10 ?x)
        (LeftOf4 ?x)
        (RightOf4 ?x)
        (LeftOf1 ?x)
        (LeftOf7 ?x)
        (RightOf3 ?x)
      )
    )
    :effect ( and
      (CheckConsistency)
      (when
        (or
          (LeftOf2 ?x)
          (LeftOf1 ?x)
        )
        (and
          (LeftOf1 ?x)
        )
      )
      (when
        (or
          (LeftOf2 ?x)
          (LeftOf3 ?x)
          (LeftOf1 ?x)
        )
        (and
          (LeftOf2 ?x)
        )
      )
      (when
        (or
          (LeftOf2 ?x)
          (LeftOf4 ?x)
          (LeftOf3 ?x)
          (LeftOf1 ?x)
        )
        (and
          (LeftOf3 ?x)
        )
      )
      (when
        (or
          (LeftOf2 ?x)
          (LeftOf4 ?x)
          (LeftOf3 ?x)
          (LeftOf1 ?x)
          (LeftOf5 ?x)
        )
        (and
          (LeftOf4 ?x)
        )
      )
      (when
        (or
          (LeftOf4 ?x)
          (LeftOf3 ?x)
          (LeftOf5 ?x)
          (LeftOf6 ?x)
          (LeftOf1 ?x)
          (LeftOf2 ?x)
        )
        (and
          (LeftOf5 ?x)
        )
      )
      (when
        (or
          (LeftOf4 ?x)
          (LeftOf3 ?x)
          (LeftOf5 ?x)
          (LeftOf6 ?x)
          (LeftOf1 ?x)
          (LeftOf7 ?x)
          (LeftOf2 ?x)
        )
        (and
          (LeftOf6 ?x)
        )
      )
      (when
        (or
          (LeftOf8 ?x)
          (LeftOf4 ?x)
          (LeftOf3 ?x)
          (LeftOf5 ?x)
          (LeftOf6 ?x)
          (LeftOf1 ?x)
          (LeftOf7 ?x)
          (LeftOf2 ?x)
        )
        (and
          (LeftOf7 ?x)
        )
      )
      (when
        (or
          (LeftOf8 ?x)
          (LeftOf4 ?x)
          (LeftOf3 ?x)
          (LeftOf5 ?x)
          (LeftOf6 ?x)
          (LeftOf1 ?x)
          (LeftOf7 ?x)
          (LeftOf2 ?x)
          (LeftOf9 ?x)
        )
        (and
          (LeftOf8 ?x)
        )
      )
      (when
        (or
          (LeftOf8 ?x)
          (LeftOf4 ?x)
          (LeftOf3 ?x)
          (LeftOf5 ?x)
          (LeftOf6 ?x)
          (LeftOf1 ?x)
          (LeftOf7 ?x)
          (LeftOf2 ?x)
          (LeftOf9 ?x)
          (LeftOf10 ?x)
        )
        (and
          (LeftOf9 ?x)
        )
      )
      (when
        (or
          (LeftOf8 ?x)
          (LeftOf4 ?x)
          (LeftOf3 ?x)
          (LeftOf5 ?x)
          (LeftOf6 ?x)
          (LeftOf1 ?x)
          (LeftOf7 ?x)
          (LeftOf2 ?x)
          (LeftOf9 ?x)
          (LeftOf11 ?x)
          (LeftOf10 ?x)
        )
        (and
          (LeftOf10 ?x)
        )
      )
      (when
        (or
          (LeftOf8 ?x)
          (LeftOf4 ?x)
          (LeftOf3 ?x)
          (LeftOf5 ?x)
          (LeftOf6 ?x)
          (LeftOf12 ?x)
          (LeftOf1 ?x)
          (LeftOf7 ?x)
          (LeftOf2 ?x)
          (LeftOf9 ?x)
          (LeftOf11 ?x)
          (LeftOf10 ?x)
        )
        (and
          (LeftOf11 ?x)
        )
      )
      (when
        (or
          (RightOf6 ?x)
          (RightOf9 ?x)
          (RightOf7 ?x)
          (RightOf1 ?x)
          (RightOf4 ?x)
          (RightOf10 ?x)
          (RightOf11 ?x)
          (RightOf2 ?x)
          (RightOf5 ?x)
          (RightOf8 ?x)
          (RightOf3 ?x)
        )
        (and
          (RightOf0 ?x)
          (not (RightOf1 ?x))
        )
      )
      (when
        (or
          (RightOf6 ?x)
          (RightOf9 ?x)
          (RightOf7 ?x)
          (RightOf8 ?x)
          (RightOf4 ?x)
          (RightOf10 ?x)
          (RightOf11 ?x)
          (RightOf2 ?x)
          (RightOf5 ?x)
          (RightOf3 ?x)
        )
        (and
          (RightOf1 ?x)
          (not (RightOf2 ?x))
        )
      )
      (when
        (or
          (RightOf6 ?x)
          (RightOf9 ?x)
          (RightOf7 ?x)
          (RightOf8 ?x)
          (RightOf4 ?x)
          (RightOf10 ?x)
          (RightOf11 ?x)
          (RightOf5 ?x)
          (RightOf3 ?x)
        )
        (and
          (RightOf2 ?x)
          (not (RightOf3 ?x))
        )
      )
      (when
        (or
          (RightOf6 ?x)
          (RightOf9 ?x)
          (RightOf7 ?x)
          (RightOf8 ?x)
          (RightOf4 ?x)
          (RightOf10 ?x)
          (RightOf11 ?x)
          (RightOf5 ?x)
        )
        (and
          (RightOf3 ?x)
          (not (RightOf4 ?x))
        )
      )
      (when
        (or
          (RightOf6 ?x)
          (RightOf9 ?x)
          (RightOf7 ?x)
          (RightOf8 ?x)
          (RightOf10 ?x)
          (RightOf11 ?x)
          (RightOf5 ?x)
        )
        (and
          (RightOf4 ?x)
          (not (RightOf5 ?x))
        )
      )
      (when
        (or
          (RightOf6 ?x)
          (RightOf9 ?x)
          (RightOf7 ?x)
          (RightOf8 ?x)
          (RightOf10 ?x)
          (RightOf11 ?x)
        )
        (and
          (RightOf5 ?x)
          (not (RightOf6 ?x))
        )
      )
      (when
        (or
          (RightOf11 ?x)
          (RightOf8 ?x)
          (RightOf9 ?x)
          (RightOf7 ?x)
          (RightOf10 ?x)
        )
        (and
          (RightOf6 ?x)
          (not (RightOf7 ?x))
        )
      )
      (when
        (or
          (RightOf11 ?x)
          (RightOf8 ?x)
          (RightOf9 ?x)
          (RightOf10 ?x)
        )
        (and
          (RightOf7 ?x)
          (not (RightOf8 ?x))
        )
      )
      (when
        (or
          (RightOf11 ?x)
          (RightOf9 ?x)
          (RightOf10 ?x)
        )
        (and
          (RightOf8 ?x)
          (not (RightOf9 ?x))
        )
      )
      (when
        (or
          (RightOf11 ?x)
          (RightOf10 ?x)
        )
        (and
          (RightOf9 ?x)
          (not (RightOf10 ?x))
        )
      )
      (when
        (RightOf11 ?x)
        (and
          (RightOf10 ?x)
          (not (RightOf11 ?x))
        )
      )
      (when
        (Column1 ?x)
        (and
          (Column0 ?x)
          (not (Column1 ?x))
        )
      )
      (when
        (Column2 ?x)
        (and
          (Column1 ?x)
          (not (Column2 ?x))
        )
      )
      (when
        (Column3 ?x)
        (and
          (Column2 ?x)
          (not (Column3 ?x))
        )
      )
      (when
        (Column4 ?x)
        (and
          (Column3 ?x)
          (not (Column4 ?x))
        )
      )
      (when
        (Column5 ?x)
        (and
          (Column4 ?x)
          (not (Column5 ?x))
        )
      )
      (when
        (Column6 ?x)
        (and
          (Column5 ?x)
          (not (Column6 ?x))
        )
      )
      (when
        (Column7 ?x)
        (and
          (Column6 ?x)
          (not (Column7 ?x))
        )
      )
      (when
        (Column8 ?x)
        (and
          (Column7 ?x)
          (not (Column8 ?x))
        )
      )
      (when
        (Column9 ?x)
        (and
          (Column8 ?x)
          (not (Column9 ?x))
        )
      )
      (when
        (Column10 ?x)
        (and
          (Column9 ?x)
          (not (Column10 ?x))
        )
      )
      (when
        (Column11 ?x)
        (and
          (Column10 ?x)
          (not (Column11 ?x))
        )
      )
      (when
        (or
          (and
            (RightOf4 ?x)
            (LeftOf2 ?x)
          )
          (and
            (RightOf6 ?x)
            (LeftOf2 ?x)
          )
          (and
            (RightOf6 ?x)
            (LeftOf1 ?x)
          )
          (and
            (RightOf2 ?x)
            (LeftOf1 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf2 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf8 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf11 ?x)
          )
          (and
            (LeftOf1 ?x)
            (RightOf8 ?x)
          )
          (and
            (RightOf5 ?x)
            (LeftOf1 ?x)
          )
          (and
            (RightOf7 ?x)
            (LeftOf1 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf10 ?x)
          )
          (and
            (RightOf4 ?x)
            (LeftOf1 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf3 ?x)
          )
          (and
            (LeftOf1 ?x)
            (RightOf11 ?x)
          )
          (and
            (LeftOf1 ?x)
            (RightOf5 ?x)
          )
          (and
            (RightOf7 ?x)
            (LeftOf2 ?x)
          )
          (and
            (RightOf1 ?x)
            (LeftOf1 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf5 ?x)
          )
          (and
            (RightOf1 ?x)
            (LeftOf2 ?x)
          )
          (and
            (LeftOf1 ?x)
            (RightOf10 ?x)
          )
          (and
            (LeftOf1 ?x)
            (RightOf3 ?x)
          )
          (and
            (RightOf9 ?x)
            (LeftOf1 ?x)
          )
          (and
            (RightOf9 ?x)
            (LeftOf2 ?x)
          )
        )
        (and
          (Column0 ?x)
        )
      )
      (when
        (or
          (and
            (RightOf2 ?x)
            (LeftOf3 ?x)
          )
          (and
            (RightOf4 ?x)
            (LeftOf2 ?x)
          )
          (and
            (RightOf9 ?x)
            (LeftOf3 ?x)
          )
          (and
            (RightOf6 ?x)
            (LeftOf2 ?x)
          )
          (and
            (RightOf6 ?x)
            (LeftOf1 ?x)
          )
          (and
            (RightOf2 ?x)
            (LeftOf1 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf2 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf8 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf11 ?x)
          )
          (and
            (RightOf11 ?x)
            (LeftOf3 ?x)
          )
          (and
            (LeftOf3 ?x)
            (RightOf3 ?x)
          )
          (and
            (LeftOf1 ?x)
            (RightOf8 ?x)
          )
          (and
            (RightOf5 ?x)
            (LeftOf3 ?x)
          )
          (and
            (RightOf7 ?x)
            (LeftOf2 ?x)
          )
          (and
            (RightOf8 ?x)
            (LeftOf3 ?x)
          )
          (and
            (RightOf10 ?x)
            (LeftOf3 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf10 ?x)
          )
          (and
            (RightOf4 ?x)
            (LeftOf1 ?x)
          )
          (and
            (RightOf5 ?x)
            (LeftOf1 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf3 ?x)
          )
          (and
            (LeftOf1 ?x)
            (RightOf11 ?x)
          )
          (and
            (RightOf7 ?x)
            (LeftOf3 ?x)
          )
          (and
            (LeftOf1 ?x)
            (RightOf5 ?x)
          )
          (and
            (RightOf7 ?x)
            (LeftOf1 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf5 ?x)
          )
          (and
            (LeftOf1 ?x)
            (RightOf10 ?x)
          )
          (and
            (RightOf4 ?x)
            (LeftOf3 ?x)
          )
          (and
            (LeftOf1 ?x)
            (RightOf3 ?x)
          )
          (and
            (RightOf9 ?x)
            (LeftOf1 ?x)
          )
          (and
            (RightOf6 ?x)
            (LeftOf3 ?x)
          )
          (and
            (RightOf9 ?x)
            (LeftOf2 ?x)
          )
        )
        (and
          (Column1 ?x)
        )
      )
      (when
        (or
          (and
            (RightOf4 ?x)
            (LeftOf2 ?x)
          )
          (and
            (RightOf9 ?x)
            (LeftOf3 ?x)
          )
          (and
            (RightOf6 ?x)
            (LeftOf2 ?x)
          )
          (and
            (RightOf11 ?x)
            (LeftOf4 ?x)
          )
          (and
            (LeftOf4 ?x)
            (RightOf3 ?x)
          )
          (and
            (RightOf6 ?x)
            (LeftOf1 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf8 ?x)
          )
          (and
            (RightOf5 ?x)
            (LeftOf4 ?x)
          )
          (and
            (RightOf6 ?x)
            (LeftOf4 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf11 ?x)
          )
          (and
            (RightOf9 ?x)
            (LeftOf4 ?x)
          )
          (and
            (LeftOf4 ?x)
            (RightOf10 ?x)
          )
          (and
            (RightOf11 ?x)
            (LeftOf3 ?x)
          )
          (and
            (LeftOf1 ?x)
            (RightOf8 ?x)
          )
          (and
            (RightOf5 ?x)
            (LeftOf3 ?x)
          )
          (and
            (LeftOf4 ?x)
            (RightOf8 ?x)
          )
          (and
            (RightOf7 ?x)
            (LeftOf2 ?x)
          )
          (and
            (RightOf3 ?x)
            (LeftOf3 ?x)
          )
          (and
            (RightOf10 ?x)
            (LeftOf3 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf10 ?x)
          )
          (and
            (RightOf4 ?x)
            (LeftOf1 ?x)
          )
          (and
            (RightOf5 ?x)
            (LeftOf1 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf3 ?x)
          )
          (and
            (LeftOf1 ?x)
            (RightOf11 ?x)
          )
          (and
            (RightOf7 ?x)
            (LeftOf3 ?x)
          )
          (and
            (LeftOf1 ?x)
            (RightOf5 ?x)
          )
          (and
            (RightOf7 ?x)
            (LeftOf1 ?x)
          )
          (and
            (RightOf8 ?x)
            (LeftOf3 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf5 ?x)
          )
          (and
            (RightOf7 ?x)
            (LeftOf4 ?x)
          )
          (and
            (RightOf4 ?x)
            (LeftOf4 ?x)
          )
          (and
            (LeftOf1 ?x)
            (RightOf10 ?x)
          )
          (and
            (RightOf4 ?x)
            (LeftOf3 ?x)
          )
          (and
            (LeftOf1 ?x)
            (RightOf3 ?x)
          )
          (and
            (RightOf9 ?x)
            (LeftOf1 ?x)
          )
          (and
            (RightOf6 ?x)
            (LeftOf3 ?x)
          )
          (and
            (RightOf9 ?x)
            (LeftOf2 ?x)
          )
        )
        (and
          (Column2 ?x)
        )
      )
      (when
        (or
          (and
            (RightOf4 ?x)
            (LeftOf2 ?x)
          )
          (and
            (RightOf9 ?x)
            (LeftOf3 ?x)
          )
          (and
            (RightOf6 ?x)
            (LeftOf2 ?x)
          )
          (and
            (RightOf11 ?x)
            (LeftOf4 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf5 ?x)
          )
          (and
            (RightOf6 ?x)
            (LeftOf1 ?x)
          )
          (and
            (RightOf5 ?x)
            (LeftOf3 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf8 ?x)
          )
          (and
            (RightOf5 ?x)
            (LeftOf4 ?x)
          )
          (and
            (RightOf6 ?x)
            (LeftOf4 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf11 ?x)
          )
          (and
            (RightOf9 ?x)
            (LeftOf4 ?x)
          )
          (and
            (LeftOf4 ?x)
            (RightOf10 ?x)
          )
          (and
            (RightOf11 ?x)
            (LeftOf3 ?x)
          )
          (and
            (LeftOf1 ?x)
            (RightOf8 ?x)
          )
          (and
            (RightOf5 ?x)
            (LeftOf1 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf8 ?x)
          )
          (and
            (LeftOf4 ?x)
            (RightOf8 ?x)
          )
          (and
            (RightOf7 ?x)
            (LeftOf2 ?x)
          )
          (and
            (RightOf8 ?x)
            (LeftOf3 ?x)
          )
          (and
            (RightOf10 ?x)
            (LeftOf3 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf10 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf10 ?x)
          )
          (and
            (RightOf4 ?x)
            (LeftOf1 ?x)
          )
          (and
            (RightOf6 ?x)
            (LeftOf5 ?x)
          )
          (and
            (RightOf8 ?x)
            (LeftOf4 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf4 ?x)
          )
          (and
            (LeftOf1 ?x)
            (RightOf11 ?x)
          )
          (and
            (RightOf7 ?x)
            (LeftOf3 ?x)
          )
          (and
            (LeftOf1 ?x)
            (RightOf5 ?x)
          )
          (and
            (RightOf7 ?x)
            (LeftOf1 ?x)
          )
          (and
            (RightOf10 ?x)
            (LeftOf4 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf5 ?x)
          )
          (and
            (RightOf7 ?x)
            (LeftOf4 ?x)
          )
          (and
            (RightOf4 ?x)
            (LeftOf4 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf11 ?x)
          )
          (and
            (LeftOf1 ?x)
            (RightOf10 ?x)
          )
          (and
            (RightOf4 ?x)
            (LeftOf3 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf7 ?x)
          )
          (and
            (RightOf9 ?x)
            (LeftOf1 ?x)
          )
          (and
            (RightOf6 ?x)
            (LeftOf3 ?x)
          )
          (and
            (RightOf9 ?x)
            (LeftOf2 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf9 ?x)
          )
        )
        (and
          (Column3 ?x)
        )
      )
      (when
        (or
          (and
            (LeftOf6 ?x)
            (RightOf9 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf11 ?x)
          )
          (and
            (RightOf6 ?x)
            (LeftOf2 ?x)
          )
          (and
            (RightOf11 ?x)
            (LeftOf4 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf5 ?x)
          )
          (and
            (RightOf6 ?x)
            (LeftOf1 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf8 ?x)
          )
          (and
            (RightOf5 ?x)
            (LeftOf4 ?x)
          )
          (and
            (RightOf6 ?x)
            (LeftOf4 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf11 ?x)
          )
          (and
            (RightOf9 ?x)
            (LeftOf4 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf6 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf10 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf7 ?x)
          )
          (and
            (LeftOf4 ?x)
            (RightOf10 ?x)
          )
          (and
            (RightOf11 ?x)
            (LeftOf3 ?x)
          )
          (and
            (LeftOf1 ?x)
            (RightOf8 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf5 ?x)
          )
          (and
            (RightOf5 ?x)
            (LeftOf3 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf8 ?x)
          )
          (and
            (LeftOf4 ?x)
            (RightOf8 ?x)
          )
          (and
            (RightOf7 ?x)
            (LeftOf1 ?x)
          )
          (and
            (RightOf8 ?x)
            (LeftOf3 ?x)
          )
          (and
            (RightOf10 ?x)
            (LeftOf3 ?x)
          )
          (and
            (RightOf6 ?x)
            (LeftOf3 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf10 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf10 ?x)
          )
          (and
            (RightOf6 ?x)
            (LeftOf5 ?x)
          )
          (and
            (RightOf8 ?x)
            (LeftOf4 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf11 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf8 ?x)
          )
          (and
            (LeftOf1 ?x)
            (RightOf11 ?x)
          )
          (and
            (RightOf7 ?x)
            (LeftOf3 ?x)
          )
          (and
            (LeftOf1 ?x)
            (RightOf5 ?x)
          )
          (and
            (RightOf7 ?x)
            (LeftOf2 ?x)
          )
          (and
            (RightOf10 ?x)
            (LeftOf4 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf5 ?x)
          )
          (and
            (RightOf7 ?x)
            (LeftOf4 ?x)
          )
          (and
            (LeftOf1 ?x)
            (RightOf10 ?x)
          )
          (and
            (RightOf9 ?x)
            (LeftOf3 ?x)
          )
          (and
            (RightOf9 ?x)
            (LeftOf1 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf7 ?x)
          )
          (and
            (RightOf9 ?x)
            (LeftOf2 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf9 ?x)
          )
        )
        (and
          (Column4 ?x)
        )
      )
      (when
        (or
          (and
            (LeftOf6 ?x)
            (RightOf9 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf11 ?x)
          )
          (and
            (RightOf6 ?x)
            (LeftOf2 ?x)
          )
          (and
            (RightOf11 ?x)
            (LeftOf4 ?x)
          )
          (and
            (RightOf6 ?x)
            (LeftOf1 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf8 ?x)
          )
          (and
            (LeftOf7 ?x)
            (RightOf10 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf11 ?x)
          )
          (and
            (RightOf9 ?x)
            (LeftOf4 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf10 ?x)
          )
          (and
            (RightOf6 ?x)
            (LeftOf6 ?x)
          )
          (and
            (RightOf9 ?x)
            (LeftOf7 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf7 ?x)
          )
          (and
            (LeftOf4 ?x)
            (RightOf10 ?x)
          )
          (and
            (RightOf11 ?x)
            (LeftOf3 ?x)
          )
          (and
            (LeftOf1 ?x)
            (RightOf8 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf8 ?x)
          )
          (and
            (LeftOf4 ?x)
            (RightOf8 ?x)
          )
          (and
            (RightOf7 ?x)
            (LeftOf2 ?x)
          )
          (and
            (RightOf8 ?x)
            (LeftOf3 ?x)
          )
          (and
            (RightOf10 ?x)
            (LeftOf3 ?x)
          )
          (and
            (RightOf6 ?x)
            (LeftOf7 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf10 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf10 ?x)
          )
          (and
            (RightOf6 ?x)
            (LeftOf5 ?x)
          )
          (and
            (RightOf8 ?x)
            (LeftOf4 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf11 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf8 ?x)
          )
          (and
            (LeftOf1 ?x)
            (RightOf11 ?x)
          )
          (and
            (RightOf7 ?x)
            (LeftOf3 ?x)
          )
          (and
            (RightOf7 ?x)
            (LeftOf1 ?x)
          )
          (and
            (RightOf10 ?x)
            (LeftOf4 ?x)
          )
          (and
            (RightOf7 ?x)
            (LeftOf4 ?x)
          )
          (and
            (RightOf6 ?x)
            (LeftOf3 ?x)
          )
          (and
            (LeftOf1 ?x)
            (RightOf10 ?x)
          )
          (and
            (RightOf9 ?x)
            (LeftOf3 ?x)
          )
          (and
            (LeftOf7 ?x)
            (RightOf8 ?x)
          )
          (and
            (RightOf7 ?x)
            (LeftOf7 ?x)
          )
          (and
            (RightOf6 ?x)
            (LeftOf4 ?x)
          )
          (and
            (LeftOf7 ?x)
            (RightOf11 ?x)
          )
          (and
            (RightOf9 ?x)
            (LeftOf1 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf7 ?x)
          )
          (and
            (RightOf9 ?x)
            (LeftOf2 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf9 ?x)
          )
        )
        (and
          (Column5 ?x)
        )
      )
      (when
        (or
          (and
            (LeftOf6 ?x)
            (RightOf9 ?x)
          )
          (and
            (RightOf9 ?x)
            (LeftOf3 ?x)
          )
          (and
            (RightOf11 ?x)
            (LeftOf4 ?x)
          )
          (and
            (LeftOf8 ?x)
            (RightOf11 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf8 ?x)
          )
          (and
            (LeftOf7 ?x)
            (RightOf10 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf11 ?x)
          )
          (and
            (RightOf9 ?x)
            (LeftOf4 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf10 ?x)
          )
          (and
            (RightOf9 ?x)
            (LeftOf7 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf7 ?x)
          )
          (and
            (LeftOf4 ?x)
            (RightOf10 ?x)
          )
          (and
            (RightOf11 ?x)
            (LeftOf3 ?x)
          )
          (and
            (LeftOf1 ?x)
            (RightOf8 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf8 ?x)
          )
          (and
            (LeftOf4 ?x)
            (RightOf8 ?x)
          )
          (and
            (RightOf7 ?x)
            (LeftOf2 ?x)
          )
          (and
            (RightOf8 ?x)
            (LeftOf3 ?x)
          )
          (and
            (RightOf10 ?x)
            (LeftOf3 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf10 ?x)
          )
          (and
            (LeftOf7 ?x)
            (RightOf7 ?x)
          )
          (and
            (LeftOf8 ?x)
            (RightOf8 ?x)
          )
          (and
            (LeftOf8 ?x)
            (RightOf10 ?x)
          )
          (and
            (RightOf8 ?x)
            (LeftOf4 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf11 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf8 ?x)
          )
          (and
            (LeftOf1 ?x)
            (RightOf11 ?x)
          )
          (and
            (RightOf7 ?x)
            (LeftOf3 ?x)
          )
          (and
            (RightOf9 ?x)
            (LeftOf2 ?x)
          )
          (and
            (RightOf7 ?x)
            (LeftOf1 ?x)
          )
          (and
            (RightOf10 ?x)
            (LeftOf4 ?x)
          )
          (and
            (RightOf7 ?x)
            (LeftOf4 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf11 ?x)
          )
          (and
            (LeftOf1 ?x)
            (RightOf10 ?x)
          )
          (and
            (RightOf9 ?x)
            (LeftOf8 ?x)
          )
          (and
            (LeftOf7 ?x)
            (RightOf8 ?x)
          )
          (and
            (LeftOf7 ?x)
            (RightOf11 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf10 ?x)
          )
          (and
            (RightOf9 ?x)
            (LeftOf1 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf7 ?x)
          )
          (and
            (LeftOf8 ?x)
            (RightOf7 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf9 ?x)
          )
        )
        (and
          (Column6 ?x)
        )
      )
      (when
        (or
          (and
            (LeftOf6 ?x)
            (RightOf9 ?x)
          )
          (and
            (RightOf9 ?x)
            (LeftOf3 ?x)
          )
          (and
            (RightOf11 ?x)
            (LeftOf4 ?x)
          )
          (and
            (LeftOf8 ?x)
            (RightOf11 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf8 ?x)
          )
          (and
            (LeftOf7 ?x)
            (RightOf10 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf11 ?x)
          )
          (and
            (RightOf9 ?x)
            (LeftOf4 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf10 ?x)
          )
          (and
            (RightOf9 ?x)
            (LeftOf7 ?x)
          )
          (and
            (LeftOf4 ?x)
            (RightOf10 ?x)
          )
          (and
            (RightOf11 ?x)
            (LeftOf3 ?x)
          )
          (and
            (LeftOf1 ?x)
            (RightOf8 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf8 ?x)
          )
          (and
            (LeftOf9 ?x)
            (RightOf11 ?x)
          )
          (and
            (RightOf8 ?x)
            (LeftOf3 ?x)
          )
          (and
            (RightOf10 ?x)
            (LeftOf3 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf10 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf10 ?x)
          )
          (and
            (LeftOf8 ?x)
            (RightOf10 ?x)
          )
          (and
            (LeftOf9 ?x)
            (RightOf10 ?x)
          )
          (and
            (RightOf8 ?x)
            (LeftOf4 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf11 ?x)
          )
          (and
            (LeftOf1 ?x)
            (RightOf11 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf8 ?x)
          )
          (and
            (RightOf9 ?x)
            (LeftOf9 ?x)
          )
          (and
            (RightOf10 ?x)
            (LeftOf4 ?x)
          )
          (and
            (LeftOf9 ?x)
            (RightOf8 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf11 ?x)
          )
          (and
            (LeftOf1 ?x)
            (RightOf10 ?x)
          )
          (and
            (RightOf9 ?x)
            (LeftOf8 ?x)
          )
          (and
            (LeftOf7 ?x)
            (RightOf8 ?x)
          )
          (and
            (LeftOf7 ?x)
            (RightOf11 ?x)
          )
          (and
            (LeftOf8 ?x)
            (RightOf8 ?x)
          )
          (and
            (RightOf9 ?x)
            (LeftOf1 ?x)
          )
          (and
            (RightOf9 ?x)
            (LeftOf2 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf9 ?x)
          )
        )
        (and
          (Column7 ?x)
        )
      )
      (when
        (or
          (and
            (LeftOf6 ?x)
            (RightOf9 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf11 ?x)
          )
          (and
            (RightOf11 ?x)
            (LeftOf4 ?x)
          )
          (and
            (LeftOf8 ?x)
            (RightOf11 ?x)
          )
          (and
            (RightOf9 ?x)
            (LeftOf3 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf11 ?x)
          )
          (and
            (RightOf9 ?x)
            (LeftOf4 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf10 ?x)
          )
          (and
            (RightOf9 ?x)
            (LeftOf7 ?x)
          )
          (and
            (LeftOf4 ?x)
            (RightOf10 ?x)
          )
          (and
            (RightOf11 ?x)
            (LeftOf3 ?x)
          )
          (and
            (LeftOf9 ?x)
            (RightOf11 ?x)
          )
          (and
            (RightOf9 ?x)
            (LeftOf10 ?x)
          )
          (and
            (RightOf10 ?x)
            (LeftOf4 ?x)
          )
          (and
            (RightOf10 ?x)
            (LeftOf3 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf10 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf10 ?x)
          )
          (and
            (LeftOf8 ?x)
            (RightOf10 ?x)
          )
          (and
            (LeftOf9 ?x)
            (RightOf10 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf11 ?x)
          )
          (and
            (LeftOf1 ?x)
            (RightOf11 ?x)
          )
          (and
            (RightOf9 ?x)
            (LeftOf9 ?x)
          )
          (and
            (LeftOf1 ?x)
            (RightOf10 ?x)
          )
          (and
            (RightOf9 ?x)
            (LeftOf8 ?x)
          )
          (and
            (LeftOf10 ?x)
            (RightOf10 ?x)
          )
          (and
            (LeftOf10 ?x)
            (RightOf11 ?x)
          )
          (and
            (LeftOf7 ?x)
            (RightOf10 ?x)
          )
          (and
            (LeftOf7 ?x)
            (RightOf11 ?x)
          )
          (and
            (RightOf9 ?x)
            (LeftOf1 ?x)
          )
          (and
            (RightOf9 ?x)
            (LeftOf2 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf9 ?x)
          )
        )
        (and
          (Column8 ?x)
        )
      )
      (when
        (or
          (and
            (LeftOf5 ?x)
            (RightOf11 ?x)
          )
          (and
            (RightOf11 ?x)
            (LeftOf4 ?x)
          )
          (and
            (LeftOf8 ?x)
            (RightOf11 ?x)
          )
          (and
            (LeftOf11 ?x)
            (RightOf10 ?x)
          )
          (and
            (LeftOf7 ?x)
            (RightOf10 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf11 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf10 ?x)
          )
          (and
            (RightOf11 ?x)
            (LeftOf3 ?x)
          )
          (and
            (LeftOf9 ?x)
            (RightOf11 ?x)
          )
          (and
            (RightOf10 ?x)
            (LeftOf4 ?x)
          )
          (and
            (RightOf10 ?x)
            (LeftOf3 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf10 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf10 ?x)
          )
          (and
            (LeftOf8 ?x)
            (RightOf10 ?x)
          )
          (and
            (LeftOf9 ?x)
            (RightOf10 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf11 ?x)
          )
          (and
            (LeftOf1 ?x)
            (RightOf11 ?x)
          )
          (and
            (LeftOf1 ?x)
            (RightOf10 ?x)
          )
          (and
            (LeftOf10 ?x)
            (RightOf10 ?x)
          )
          (and
            (LeftOf10 ?x)
            (RightOf11 ?x)
          )
          (and
            (LeftOf11 ?x)
            (RightOf11 ?x)
          )
          (and
            (LeftOf7 ?x)
            (RightOf11 ?x)
          )
        )
        (and
          (Column9 ?x)
        )
      )
      (when
        (or
          (and
            (LeftOf12 ?x)
            (RightOf11 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf11 ?x)
          )
          (and
            (RightOf11 ?x)
            (LeftOf4 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf11 ?x)
          )
          (and
            (RightOf11 ?x)
            (LeftOf3 ?x)
          )
          (and
            (LeftOf1 ?x)
            (RightOf11 ?x)
          )
          (and
            (LeftOf8 ?x)
            (RightOf11 ?x)
          )
          (and
            (LeftOf10 ?x)
            (RightOf11 ?x)
          )
          (and
            (LeftOf9 ?x)
            (RightOf11 ?x)
          )
          (and
            (LeftOf11 ?x)
            (RightOf11 ?x)
          )
          (and
            (LeftOf7 ?x)
            (RightOf11 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf11 ?x)
          )
        )
        (and
          (Column10 ?x)
        )
      )
    )
  )
  (:action moveRight
    :parameters ( ?x )
    :precondition ( and
      (not (CheckConsistency))
      (not (Error))
      (or
        (LeftOf3 ?x)
        (RightOf7 ?x)
        (RightOf8 ?x)
        (LeftOf2 ?x)
        (LeftOf9 ?x)
        (RightOf2 ?x)
        (RightOf0 ?x)
        (LeftOf8 ?x)
        (LeftOf5 ?x)
        (RightOf1 ?x)
        (LeftOf12 ?x)
        (Columns ?x)
        (RightOf6 ?x)
        (RightOf9 ?x)
        (LeftOf6 ?x)
        (RightOf10 ?x)
        (RightOf11 ?x)
        (LeftOf11 ?x)
        (RightOf5 ?x)
        (LeftOf10 ?x)
        (LeftOf4 ?x)
        (RightOf4 ?x)
        (LeftOf1 ?x)
        (LeftOf7 ?x)
        (RightOf3 ?x)
      )
    )
    :effect ( and
      (CheckConsistency)
      (when
        (or
          (RightOf6 ?x)
          (RightOf9 ?x)
          (RightOf7 ?x)
          (RightOf1 ?x)
          (RightOf4 ?x)
          (RightOf0 ?x)
          (RightOf11 ?x)
          (RightOf2 ?x)
          (RightOf5 ?x)
          (RightOf10 ?x)
          (RightOf8 ?x)
          (RightOf3 ?x)
        )
        (and
          (RightOf1 ?x)
        )
      )
      (when
        (or
          (RightOf6 ?x)
          (RightOf9 ?x)
          (RightOf7 ?x)
          (RightOf1 ?x)
          (RightOf4 ?x)
          (RightOf10 ?x)
          (RightOf11 ?x)
          (RightOf2 ?x)
          (RightOf5 ?x)
          (RightOf8 ?x)
          (RightOf3 ?x)
        )
        (and
          (RightOf2 ?x)
        )
      )
      (when
        (or
          (RightOf6 ?x)
          (RightOf9 ?x)
          (RightOf7 ?x)
          (RightOf8 ?x)
          (RightOf4 ?x)
          (RightOf10 ?x)
          (RightOf11 ?x)
          (RightOf2 ?x)
          (RightOf5 ?x)
          (RightOf3 ?x)
        )
        (and
          (RightOf3 ?x)
        )
      )
      (when
        (or
          (RightOf6 ?x)
          (RightOf9 ?x)
          (RightOf7 ?x)
          (RightOf8 ?x)
          (RightOf4 ?x)
          (RightOf10 ?x)
          (RightOf11 ?x)
          (RightOf5 ?x)
          (RightOf3 ?x)
        )
        (and
          (RightOf4 ?x)
        )
      )
      (when
        (or
          (RightOf6 ?x)
          (RightOf9 ?x)
          (RightOf7 ?x)
          (RightOf8 ?x)
          (RightOf4 ?x)
          (RightOf10 ?x)
          (RightOf11 ?x)
          (RightOf5 ?x)
        )
        (and
          (RightOf5 ?x)
        )
      )
      (when
        (or
          (RightOf6 ?x)
          (RightOf9 ?x)
          (RightOf7 ?x)
          (RightOf8 ?x)
          (RightOf10 ?x)
          (RightOf11 ?x)
          (RightOf5 ?x)
        )
        (and
          (RightOf6 ?x)
        )
      )
      (when
        (or
          (RightOf6 ?x)
          (RightOf9 ?x)
          (RightOf7 ?x)
          (RightOf8 ?x)
          (RightOf10 ?x)
          (RightOf11 ?x)
        )
        (and
          (RightOf7 ?x)
        )
      )
      (when
        (or
          (RightOf11 ?x)
          (RightOf8 ?x)
          (RightOf9 ?x)
          (RightOf7 ?x)
          (RightOf10 ?x)
        )
        (and
          (RightOf8 ?x)
        )
      )
      (when
        (or
          (RightOf11 ?x)
          (RightOf8 ?x)
          (RightOf9 ?x)
          (RightOf10 ?x)
        )
        (and
          (RightOf9 ?x)
        )
      )
      (when
        (or
          (RightOf11 ?x)
          (RightOf9 ?x)
          (RightOf10 ?x)
        )
        (and
          (RightOf10 ?x)
        )
      )
      (when
        (or
          (RightOf11 ?x)
          (RightOf10 ?x)
        )
        (and
          (RightOf11 ?x)
        )
      )
      (when
        (LeftOf1 ?x)
        (and
          (LeftOf2 ?x)
          (not (LeftOf1 ?x))
        )
      )
      (when
        (or
          (LeftOf2 ?x)
          (LeftOf1 ?x)
        )
        (and
          (LeftOf3 ?x)
          (not (LeftOf2 ?x))
        )
      )
      (when
        (or
          (LeftOf2 ?x)
          (LeftOf3 ?x)
          (LeftOf1 ?x)
        )
        (and
          (LeftOf4 ?x)
          (not (LeftOf3 ?x))
        )
      )
      (when
        (or
          (LeftOf2 ?x)
          (LeftOf4 ?x)
          (LeftOf3 ?x)
          (LeftOf1 ?x)
        )
        (and
          (LeftOf5 ?x)
          (not (LeftOf4 ?x))
        )
      )
      (when
        (or
          (LeftOf2 ?x)
          (LeftOf4 ?x)
          (LeftOf3 ?x)
          (LeftOf1 ?x)
          (LeftOf5 ?x)
        )
        (and
          (LeftOf6 ?x)
          (not (LeftOf5 ?x))
        )
      )
      (when
        (or
          (LeftOf4 ?x)
          (LeftOf3 ?x)
          (LeftOf5 ?x)
          (LeftOf6 ?x)
          (LeftOf1 ?x)
          (LeftOf2 ?x)
        )
        (and
          (LeftOf7 ?x)
          (not (LeftOf6 ?x))
        )
      )
      (when
        (or
          (LeftOf4 ?x)
          (LeftOf3 ?x)
          (LeftOf5 ?x)
          (LeftOf6 ?x)
          (LeftOf1 ?x)
          (LeftOf7 ?x)
          (LeftOf2 ?x)
        )
        (and
          (LeftOf8 ?x)
          (not (LeftOf7 ?x))
        )
      )
      (when
        (or
          (LeftOf8 ?x)
          (LeftOf4 ?x)
          (LeftOf3 ?x)
          (LeftOf5 ?x)
          (LeftOf6 ?x)
          (LeftOf1 ?x)
          (LeftOf7 ?x)
          (LeftOf2 ?x)
        )
        (and
          (LeftOf9 ?x)
          (not (LeftOf8 ?x))
        )
      )
      (when
        (or
          (LeftOf8 ?x)
          (LeftOf4 ?x)
          (LeftOf3 ?x)
          (LeftOf5 ?x)
          (LeftOf6 ?x)
          (LeftOf1 ?x)
          (LeftOf7 ?x)
          (LeftOf2 ?x)
          (LeftOf9 ?x)
        )
        (and
          (LeftOf10 ?x)
          (not (LeftOf9 ?x))
        )
      )
      (when
        (or
          (LeftOf8 ?x)
          (LeftOf4 ?x)
          (LeftOf3 ?x)
          (LeftOf5 ?x)
          (LeftOf6 ?x)
          (LeftOf1 ?x)
          (LeftOf7 ?x)
          (LeftOf2 ?x)
          (LeftOf9 ?x)
          (LeftOf10 ?x)
        )
        (and
          (LeftOf11 ?x)
          (not (LeftOf10 ?x))
        )
      )
      (when
        (or
          (LeftOf8 ?x)
          (LeftOf4 ?x)
          (LeftOf3 ?x)
          (LeftOf5 ?x)
          (LeftOf6 ?x)
          (LeftOf1 ?x)
          (LeftOf7 ?x)
          (LeftOf2 ?x)
          (LeftOf9 ?x)
          (LeftOf11 ?x)
          (LeftOf10 ?x)
        )
        (and
          (LeftOf12 ?x)
          (not (LeftOf11 ?x))
        )
      )
      (when
        (Column0 ?x)
        (and
          (Column1 ?x)
          (not (Column0 ?x))
        )
      )
      (when
        (Column1 ?x)
        (and
          (Column2 ?x)
          (not (Column1 ?x))
        )
      )
      (when
        (Column2 ?x)
        (and
          (Column3 ?x)
          (not (Column2 ?x))
        )
      )
      (when
        (Column3 ?x)
        (and
          (Column4 ?x)
          (not (Column3 ?x))
        )
      )
      (when
        (Column4 ?x)
        (and
          (Column5 ?x)
          (not (Column4 ?x))
        )
      )
      (when
        (Column5 ?x)
        (and
          (Column6 ?x)
          (not (Column5 ?x))
        )
      )
      (when
        (Column6 ?x)
        (and
          (Column7 ?x)
          (not (Column6 ?x))
        )
      )
      (when
        (Column7 ?x)
        (and
          (Column8 ?x)
          (not (Column7 ?x))
        )
      )
      (when
        (Column8 ?x)
        (and
          (Column9 ?x)
          (not (Column8 ?x))
        )
      )
      (when
        (Column9 ?x)
        (and
          (Column10 ?x)
          (not (Column9 ?x))
        )
      )
      (when
        (Column10 ?x)
        (and
          (Column11 ?x)
          (not (Column10 ?x))
        )
      )
      (when
        (or
          (and
            (LeftOf1 ?x)
            (RightOf10 ?x)
          )
          (and
            (RightOf6 ?x)
            (LeftOf1 ?x)
          )
          (and
            (LeftOf1 ?x)
            (RightOf8 ?x)
          )
          (and
            (LeftOf1 ?x)
            (RightOf11 ?x)
          )
          (and
            (RightOf2 ?x)
            (LeftOf1 ?x)
          )
          (and
            (RightOf5 ?x)
            (LeftOf1 ?x)
          )
          (and
            (LeftOf1 ?x)
            (RightOf0 ?x)
          )
          (and
            (LeftOf1 ?x)
            (RightOf3 ?x)
          )
          (and
            (RightOf1 ?x)
            (LeftOf1 ?x)
          )
          (and
            (RightOf9 ?x)
            (LeftOf1 ?x)
          )
          (and
            (RightOf7 ?x)
            (LeftOf1 ?x)
          )
          (and
            (RightOf4 ?x)
            (LeftOf1 ?x)
          )
        )
        (and
          (Column1 ?x)
        )
      )
      (when
        (or
          (and
            (RightOf4 ?x)
            (LeftOf2 ?x)
          )
          (and
            (RightOf6 ?x)
            (LeftOf2 ?x)
          )
          (and
            (RightOf6 ?x)
            (LeftOf1 ?x)
          )
          (and
            (RightOf2 ?x)
            (LeftOf1 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf2 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf8 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf11 ?x)
          )
          (and
            (LeftOf1 ?x)
            (RightOf8 ?x)
          )
          (and
            (RightOf5 ?x)
            (LeftOf1 ?x)
          )
          (and
            (RightOf7 ?x)
            (LeftOf1 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf10 ?x)
          )
          (and
            (RightOf4 ?x)
            (LeftOf1 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf3 ?x)
          )
          (and
            (LeftOf1 ?x)
            (RightOf11 ?x)
          )
          (and
            (LeftOf1 ?x)
            (RightOf5 ?x)
          )
          (and
            (RightOf7 ?x)
            (LeftOf2 ?x)
          )
          (and
            (RightOf1 ?x)
            (LeftOf1 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf5 ?x)
          )
          (and
            (RightOf1 ?x)
            (LeftOf2 ?x)
          )
          (and
            (LeftOf1 ?x)
            (RightOf10 ?x)
          )
          (and
            (LeftOf1 ?x)
            (RightOf3 ?x)
          )
          (and
            (RightOf9 ?x)
            (LeftOf1 ?x)
          )
          (and
            (RightOf9 ?x)
            (LeftOf2 ?x)
          )
        )
        (and
          (Column2 ?x)
        )
      )
      (when
        (or
          (and
            (RightOf2 ?x)
            (LeftOf3 ?x)
          )
          (and
            (RightOf4 ?x)
            (LeftOf2 ?x)
          )
          (and
            (RightOf9 ?x)
            (LeftOf3 ?x)
          )
          (and
            (RightOf6 ?x)
            (LeftOf2 ?x)
          )
          (and
            (RightOf6 ?x)
            (LeftOf1 ?x)
          )
          (and
            (RightOf2 ?x)
            (LeftOf1 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf2 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf8 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf11 ?x)
          )
          (and
            (RightOf11 ?x)
            (LeftOf3 ?x)
          )
          (and
            (LeftOf3 ?x)
            (RightOf3 ?x)
          )
          (and
            (LeftOf1 ?x)
            (RightOf8 ?x)
          )
          (and
            (RightOf5 ?x)
            (LeftOf3 ?x)
          )
          (and
            (RightOf7 ?x)
            (LeftOf2 ?x)
          )
          (and
            (RightOf8 ?x)
            (LeftOf3 ?x)
          )
          (and
            (RightOf10 ?x)
            (LeftOf3 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf10 ?x)
          )
          (and
            (RightOf4 ?x)
            (LeftOf1 ?x)
          )
          (and
            (RightOf5 ?x)
            (LeftOf1 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf3 ?x)
          )
          (and
            (LeftOf1 ?x)
            (RightOf11 ?x)
          )
          (and
            (RightOf7 ?x)
            (LeftOf3 ?x)
          )
          (and
            (LeftOf1 ?x)
            (RightOf5 ?x)
          )
          (and
            (RightOf7 ?x)
            (LeftOf1 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf5 ?x)
          )
          (and
            (LeftOf1 ?x)
            (RightOf10 ?x)
          )
          (and
            (RightOf4 ?x)
            (LeftOf3 ?x)
          )
          (and
            (LeftOf1 ?x)
            (RightOf3 ?x)
          )
          (and
            (RightOf9 ?x)
            (LeftOf1 ?x)
          )
          (and
            (RightOf6 ?x)
            (LeftOf3 ?x)
          )
          (and
            (RightOf9 ?x)
            (LeftOf2 ?x)
          )
        )
        (and
          (Column3 ?x)
        )
      )
      (when
        (or
          (and
            (RightOf4 ?x)
            (LeftOf2 ?x)
          )
          (and
            (RightOf9 ?x)
            (LeftOf3 ?x)
          )
          (and
            (RightOf6 ?x)
            (LeftOf2 ?x)
          )
          (and
            (RightOf11 ?x)
            (LeftOf4 ?x)
          )
          (and
            (LeftOf4 ?x)
            (RightOf3 ?x)
          )
          (and
            (RightOf6 ?x)
            (LeftOf1 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf8 ?x)
          )
          (and
            (RightOf5 ?x)
            (LeftOf4 ?x)
          )
          (and
            (RightOf6 ?x)
            (LeftOf4 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf11 ?x)
          )
          (and
            (RightOf9 ?x)
            (LeftOf4 ?x)
          )
          (and
            (LeftOf4 ?x)
            (RightOf10 ?x)
          )
          (and
            (RightOf11 ?x)
            (LeftOf3 ?x)
          )
          (and
            (LeftOf1 ?x)
            (RightOf8 ?x)
          )
          (and
            (RightOf5 ?x)
            (LeftOf3 ?x)
          )
          (and
            (LeftOf4 ?x)
            (RightOf8 ?x)
          )
          (and
            (RightOf7 ?x)
            (LeftOf2 ?x)
          )
          (and
            (RightOf3 ?x)
            (LeftOf3 ?x)
          )
          (and
            (RightOf10 ?x)
            (LeftOf3 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf10 ?x)
          )
          (and
            (RightOf4 ?x)
            (LeftOf1 ?x)
          )
          (and
            (RightOf5 ?x)
            (LeftOf1 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf3 ?x)
          )
          (and
            (LeftOf1 ?x)
            (RightOf11 ?x)
          )
          (and
            (RightOf7 ?x)
            (LeftOf3 ?x)
          )
          (and
            (LeftOf1 ?x)
            (RightOf5 ?x)
          )
          (and
            (RightOf7 ?x)
            (LeftOf1 ?x)
          )
          (and
            (RightOf8 ?x)
            (LeftOf3 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf5 ?x)
          )
          (and
            (RightOf7 ?x)
            (LeftOf4 ?x)
          )
          (and
            (RightOf4 ?x)
            (LeftOf4 ?x)
          )
          (and
            (LeftOf1 ?x)
            (RightOf10 ?x)
          )
          (and
            (RightOf4 ?x)
            (LeftOf3 ?x)
          )
          (and
            (LeftOf1 ?x)
            (RightOf3 ?x)
          )
          (and
            (RightOf9 ?x)
            (LeftOf1 ?x)
          )
          (and
            (RightOf6 ?x)
            (LeftOf3 ?x)
          )
          (and
            (RightOf9 ?x)
            (LeftOf2 ?x)
          )
        )
        (and
          (Column4 ?x)
        )
      )
      (when
        (or
          (and
            (RightOf4 ?x)
            (LeftOf2 ?x)
          )
          (and
            (RightOf9 ?x)
            (LeftOf3 ?x)
          )
          (and
            (RightOf6 ?x)
            (LeftOf2 ?x)
          )
          (and
            (RightOf11 ?x)
            (LeftOf4 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf5 ?x)
          )
          (and
            (RightOf6 ?x)
            (LeftOf1 ?x)
          )
          (and
            (RightOf5 ?x)
            (LeftOf3 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf8 ?x)
          )
          (and
            (RightOf5 ?x)
            (LeftOf4 ?x)
          )
          (and
            (RightOf6 ?x)
            (LeftOf4 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf11 ?x)
          )
          (and
            (RightOf9 ?x)
            (LeftOf4 ?x)
          )
          (and
            (LeftOf4 ?x)
            (RightOf10 ?x)
          )
          (and
            (RightOf11 ?x)
            (LeftOf3 ?x)
          )
          (and
            (LeftOf1 ?x)
            (RightOf8 ?x)
          )
          (and
            (RightOf5 ?x)
            (LeftOf1 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf8 ?x)
          )
          (and
            (LeftOf4 ?x)
            (RightOf8 ?x)
          )
          (and
            (RightOf7 ?x)
            (LeftOf2 ?x)
          )
          (and
            (RightOf8 ?x)
            (LeftOf3 ?x)
          )
          (and
            (RightOf10 ?x)
            (LeftOf3 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf10 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf10 ?x)
          )
          (and
            (RightOf4 ?x)
            (LeftOf1 ?x)
          )
          (and
            (RightOf6 ?x)
            (LeftOf5 ?x)
          )
          (and
            (RightOf8 ?x)
            (LeftOf4 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf4 ?x)
          )
          (and
            (LeftOf1 ?x)
            (RightOf11 ?x)
          )
          (and
            (RightOf7 ?x)
            (LeftOf3 ?x)
          )
          (and
            (LeftOf1 ?x)
            (RightOf5 ?x)
          )
          (and
            (RightOf7 ?x)
            (LeftOf1 ?x)
          )
          (and
            (RightOf10 ?x)
            (LeftOf4 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf5 ?x)
          )
          (and
            (RightOf7 ?x)
            (LeftOf4 ?x)
          )
          (and
            (RightOf4 ?x)
            (LeftOf4 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf11 ?x)
          )
          (and
            (LeftOf1 ?x)
            (RightOf10 ?x)
          )
          (and
            (RightOf4 ?x)
            (LeftOf3 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf7 ?x)
          )
          (and
            (RightOf9 ?x)
            (LeftOf1 ?x)
          )
          (and
            (RightOf6 ?x)
            (LeftOf3 ?x)
          )
          (and
            (RightOf9 ?x)
            (LeftOf2 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf9 ?x)
          )
        )
        (and
          (Column5 ?x)
        )
      )
      (when
        (or
          (and
            (LeftOf6 ?x)
            (RightOf9 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf11 ?x)
          )
          (and
            (RightOf6 ?x)
            (LeftOf2 ?x)
          )
          (and
            (RightOf11 ?x)
            (LeftOf4 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf5 ?x)
          )
          (and
            (RightOf6 ?x)
            (LeftOf1 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf8 ?x)
          )
          (and
            (RightOf5 ?x)
            (LeftOf4 ?x)
          )
          (and
            (RightOf6 ?x)
            (LeftOf4 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf11 ?x)
          )
          (and
            (RightOf9 ?x)
            (LeftOf4 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf6 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf10 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf7 ?x)
          )
          (and
            (LeftOf4 ?x)
            (RightOf10 ?x)
          )
          (and
            (RightOf11 ?x)
            (LeftOf3 ?x)
          )
          (and
            (LeftOf1 ?x)
            (RightOf8 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf5 ?x)
          )
          (and
            (RightOf5 ?x)
            (LeftOf3 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf8 ?x)
          )
          (and
            (LeftOf4 ?x)
            (RightOf8 ?x)
          )
          (and
            (RightOf7 ?x)
            (LeftOf1 ?x)
          )
          (and
            (RightOf8 ?x)
            (LeftOf3 ?x)
          )
          (and
            (RightOf10 ?x)
            (LeftOf3 ?x)
          )
          (and
            (RightOf6 ?x)
            (LeftOf3 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf10 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf10 ?x)
          )
          (and
            (RightOf6 ?x)
            (LeftOf5 ?x)
          )
          (and
            (RightOf8 ?x)
            (LeftOf4 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf11 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf8 ?x)
          )
          (and
            (LeftOf1 ?x)
            (RightOf11 ?x)
          )
          (and
            (RightOf7 ?x)
            (LeftOf3 ?x)
          )
          (and
            (LeftOf1 ?x)
            (RightOf5 ?x)
          )
          (and
            (RightOf7 ?x)
            (LeftOf2 ?x)
          )
          (and
            (RightOf10 ?x)
            (LeftOf4 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf5 ?x)
          )
          (and
            (RightOf7 ?x)
            (LeftOf4 ?x)
          )
          (and
            (LeftOf1 ?x)
            (RightOf10 ?x)
          )
          (and
            (RightOf9 ?x)
            (LeftOf3 ?x)
          )
          (and
            (RightOf9 ?x)
            (LeftOf1 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf7 ?x)
          )
          (and
            (RightOf9 ?x)
            (LeftOf2 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf9 ?x)
          )
        )
        (and
          (Column6 ?x)
        )
      )
      (when
        (or
          (and
            (LeftOf6 ?x)
            (RightOf9 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf11 ?x)
          )
          (and
            (RightOf6 ?x)
            (LeftOf2 ?x)
          )
          (and
            (RightOf11 ?x)
            (LeftOf4 ?x)
          )
          (and
            (RightOf6 ?x)
            (LeftOf1 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf8 ?x)
          )
          (and
            (LeftOf7 ?x)
            (RightOf10 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf11 ?x)
          )
          (and
            (RightOf9 ?x)
            (LeftOf4 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf10 ?x)
          )
          (and
            (RightOf6 ?x)
            (LeftOf6 ?x)
          )
          (and
            (RightOf9 ?x)
            (LeftOf7 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf7 ?x)
          )
          (and
            (LeftOf4 ?x)
            (RightOf10 ?x)
          )
          (and
            (RightOf11 ?x)
            (LeftOf3 ?x)
          )
          (and
            (LeftOf1 ?x)
            (RightOf8 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf8 ?x)
          )
          (and
            (LeftOf4 ?x)
            (RightOf8 ?x)
          )
          (and
            (RightOf7 ?x)
            (LeftOf2 ?x)
          )
          (and
            (RightOf8 ?x)
            (LeftOf3 ?x)
          )
          (and
            (RightOf10 ?x)
            (LeftOf3 ?x)
          )
          (and
            (RightOf6 ?x)
            (LeftOf7 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf10 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf10 ?x)
          )
          (and
            (RightOf6 ?x)
            (LeftOf5 ?x)
          )
          (and
            (RightOf8 ?x)
            (LeftOf4 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf11 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf8 ?x)
          )
          (and
            (LeftOf1 ?x)
            (RightOf11 ?x)
          )
          (and
            (RightOf7 ?x)
            (LeftOf3 ?x)
          )
          (and
            (RightOf7 ?x)
            (LeftOf1 ?x)
          )
          (and
            (RightOf10 ?x)
            (LeftOf4 ?x)
          )
          (and
            (RightOf7 ?x)
            (LeftOf4 ?x)
          )
          (and
            (RightOf6 ?x)
            (LeftOf3 ?x)
          )
          (and
            (LeftOf1 ?x)
            (RightOf10 ?x)
          )
          (and
            (RightOf9 ?x)
            (LeftOf3 ?x)
          )
          (and
            (LeftOf7 ?x)
            (RightOf8 ?x)
          )
          (and
            (RightOf7 ?x)
            (LeftOf7 ?x)
          )
          (and
            (RightOf6 ?x)
            (LeftOf4 ?x)
          )
          (and
            (LeftOf7 ?x)
            (RightOf11 ?x)
          )
          (and
            (RightOf9 ?x)
            (LeftOf1 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf7 ?x)
          )
          (and
            (RightOf9 ?x)
            (LeftOf2 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf9 ?x)
          )
        )
        (and
          (Column7 ?x)
        )
      )
      (when
        (or
          (and
            (LeftOf6 ?x)
            (RightOf9 ?x)
          )
          (and
            (RightOf9 ?x)
            (LeftOf3 ?x)
          )
          (and
            (RightOf11 ?x)
            (LeftOf4 ?x)
          )
          (and
            (LeftOf8 ?x)
            (RightOf11 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf8 ?x)
          )
          (and
            (LeftOf7 ?x)
            (RightOf10 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf11 ?x)
          )
          (and
            (RightOf9 ?x)
            (LeftOf4 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf10 ?x)
          )
          (and
            (RightOf9 ?x)
            (LeftOf7 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf7 ?x)
          )
          (and
            (LeftOf4 ?x)
            (RightOf10 ?x)
          )
          (and
            (RightOf11 ?x)
            (LeftOf3 ?x)
          )
          (and
            (LeftOf1 ?x)
            (RightOf8 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf8 ?x)
          )
          (and
            (LeftOf4 ?x)
            (RightOf8 ?x)
          )
          (and
            (RightOf7 ?x)
            (LeftOf2 ?x)
          )
          (and
            (RightOf8 ?x)
            (LeftOf3 ?x)
          )
          (and
            (RightOf10 ?x)
            (LeftOf3 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf10 ?x)
          )
          (and
            (LeftOf7 ?x)
            (RightOf7 ?x)
          )
          (and
            (LeftOf8 ?x)
            (RightOf8 ?x)
          )
          (and
            (LeftOf8 ?x)
            (RightOf10 ?x)
          )
          (and
            (RightOf8 ?x)
            (LeftOf4 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf11 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf8 ?x)
          )
          (and
            (LeftOf1 ?x)
            (RightOf11 ?x)
          )
          (and
            (RightOf7 ?x)
            (LeftOf3 ?x)
          )
          (and
            (RightOf9 ?x)
            (LeftOf2 ?x)
          )
          (and
            (RightOf7 ?x)
            (LeftOf1 ?x)
          )
          (and
            (RightOf10 ?x)
            (LeftOf4 ?x)
          )
          (and
            (RightOf7 ?x)
            (LeftOf4 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf11 ?x)
          )
          (and
            (LeftOf1 ?x)
            (RightOf10 ?x)
          )
          (and
            (RightOf9 ?x)
            (LeftOf8 ?x)
          )
          (and
            (LeftOf7 ?x)
            (RightOf8 ?x)
          )
          (and
            (LeftOf7 ?x)
            (RightOf11 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf10 ?x)
          )
          (and
            (RightOf9 ?x)
            (LeftOf1 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf7 ?x)
          )
          (and
            (LeftOf8 ?x)
            (RightOf7 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf9 ?x)
          )
        )
        (and
          (Column8 ?x)
        )
      )
      (when
        (or
          (and
            (LeftOf6 ?x)
            (RightOf9 ?x)
          )
          (and
            (RightOf9 ?x)
            (LeftOf3 ?x)
          )
          (and
            (RightOf11 ?x)
            (LeftOf4 ?x)
          )
          (and
            (LeftOf8 ?x)
            (RightOf11 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf8 ?x)
          )
          (and
            (LeftOf7 ?x)
            (RightOf10 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf11 ?x)
          )
          (and
            (RightOf9 ?x)
            (LeftOf4 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf10 ?x)
          )
          (and
            (RightOf9 ?x)
            (LeftOf7 ?x)
          )
          (and
            (LeftOf4 ?x)
            (RightOf10 ?x)
          )
          (and
            (RightOf11 ?x)
            (LeftOf3 ?x)
          )
          (and
            (LeftOf1 ?x)
            (RightOf8 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf8 ?x)
          )
          (and
            (LeftOf9 ?x)
            (RightOf11 ?x)
          )
          (and
            (RightOf8 ?x)
            (LeftOf3 ?x)
          )
          (and
            (RightOf10 ?x)
            (LeftOf3 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf10 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf10 ?x)
          )
          (and
            (LeftOf8 ?x)
            (RightOf10 ?x)
          )
          (and
            (LeftOf9 ?x)
            (RightOf10 ?x)
          )
          (and
            (RightOf8 ?x)
            (LeftOf4 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf11 ?x)
          )
          (and
            (LeftOf1 ?x)
            (RightOf11 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf8 ?x)
          )
          (and
            (RightOf9 ?x)
            (LeftOf9 ?x)
          )
          (and
            (RightOf10 ?x)
            (LeftOf4 ?x)
          )
          (and
            (LeftOf9 ?x)
            (RightOf8 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf11 ?x)
          )
          (and
            (LeftOf1 ?x)
            (RightOf10 ?x)
          )
          (and
            (RightOf9 ?x)
            (LeftOf8 ?x)
          )
          (and
            (LeftOf7 ?x)
            (RightOf8 ?x)
          )
          (and
            (LeftOf7 ?x)
            (RightOf11 ?x)
          )
          (and
            (LeftOf8 ?x)
            (RightOf8 ?x)
          )
          (and
            (RightOf9 ?x)
            (LeftOf1 ?x)
          )
          (and
            (RightOf9 ?x)
            (LeftOf2 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf9 ?x)
          )
        )
        (and
          (Column9 ?x)
        )
      )
      (when
        (or
          (and
            (LeftOf6 ?x)
            (RightOf9 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf11 ?x)
          )
          (and
            (RightOf11 ?x)
            (LeftOf4 ?x)
          )
          (and
            (LeftOf8 ?x)
            (RightOf11 ?x)
          )
          (and
            (RightOf9 ?x)
            (LeftOf3 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf11 ?x)
          )
          (and
            (RightOf9 ?x)
            (LeftOf4 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf10 ?x)
          )
          (and
            (RightOf9 ?x)
            (LeftOf7 ?x)
          )
          (and
            (LeftOf4 ?x)
            (RightOf10 ?x)
          )
          (and
            (RightOf11 ?x)
            (LeftOf3 ?x)
          )
          (and
            (LeftOf9 ?x)
            (RightOf11 ?x)
          )
          (and
            (RightOf9 ?x)
            (LeftOf10 ?x)
          )
          (and
            (RightOf10 ?x)
            (LeftOf4 ?x)
          )
          (and
            (RightOf10 ?x)
            (LeftOf3 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf10 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf10 ?x)
          )
          (and
            (LeftOf8 ?x)
            (RightOf10 ?x)
          )
          (and
            (LeftOf9 ?x)
            (RightOf10 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf11 ?x)
          )
          (and
            (LeftOf1 ?x)
            (RightOf11 ?x)
          )
          (and
            (RightOf9 ?x)
            (LeftOf9 ?x)
          )
          (and
            (LeftOf1 ?x)
            (RightOf10 ?x)
          )
          (and
            (RightOf9 ?x)
            (LeftOf8 ?x)
          )
          (and
            (LeftOf10 ?x)
            (RightOf10 ?x)
          )
          (and
            (LeftOf10 ?x)
            (RightOf11 ?x)
          )
          (and
            (LeftOf7 ?x)
            (RightOf10 ?x)
          )
          (and
            (LeftOf7 ?x)
            (RightOf11 ?x)
          )
          (and
            (RightOf9 ?x)
            (LeftOf1 ?x)
          )
          (and
            (RightOf9 ?x)
            (LeftOf2 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf9 ?x)
          )
        )
        (and
          (Column10 ?x)
        )
      )
      (when
        (or
          (and
            (LeftOf5 ?x)
            (RightOf11 ?x)
          )
          (and
            (RightOf11 ?x)
            (LeftOf4 ?x)
          )
          (and
            (LeftOf8 ?x)
            (RightOf11 ?x)
          )
          (and
            (LeftOf11 ?x)
            (RightOf10 ?x)
          )
          (and
            (LeftOf7 ?x)
            (RightOf10 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf11 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf10 ?x)
          )
          (and
            (RightOf11 ?x)
            (LeftOf3 ?x)
          )
          (and
            (LeftOf9 ?x)
            (RightOf11 ?x)
          )
          (and
            (RightOf10 ?x)
            (LeftOf4 ?x)
          )
          (and
            (RightOf10 ?x)
            (LeftOf3 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf10 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf10 ?x)
          )
          (and
            (LeftOf8 ?x)
            (RightOf10 ?x)
          )
          (and
            (LeftOf9 ?x)
            (RightOf10 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf11 ?x)
          )
          (and
            (LeftOf1 ?x)
            (RightOf11 ?x)
          )
          (and
            (LeftOf1 ?x)
            (RightOf10 ?x)
          )
          (and
            (LeftOf10 ?x)
            (RightOf10 ?x)
          )
          (and
            (LeftOf10 ?x)
            (RightOf11 ?x)
          )
          (and
            (LeftOf11 ?x)
            (RightOf11 ?x)
          )
          (and
            (LeftOf7 ?x)
            (RightOf11 ?x)
          )
        )
        (and
          (Column11 ?x)
        )
      )
    )
  )
  (:action moveUp
    :parameters ( ?x )
    :precondition ( and
      (not (CheckConsistency))
      (not (Error))
      (or
        (BelowOf7 ?x)
        (AboveOf9 ?x)
        (BelowOf1 ?x)
        (AboveOf7 ?x)
        (AboveOf11 ?x)
        (AboveOf1 ?x)
        (BelowOf8 ?x)
        (BelowOf3 ?x)
        (AboveOf3 ?x)
        (BelowOf9 ?x)
        (BelowOf2 ?x)
        (BelowOf5 ?x)
        (BelowOf11 ?x)
        (BelowOf10 ?x)
        (BelowOf4 ?x)
        (AboveOf10 ?x)
        (AboveOf0 ?x)
        (AboveOf8 ?x)
        (AboveOf6 ?x)
        (AboveOf5 ?x)
        (Rows ?x)
        (AboveOf4 ?x)
        (BelowOf12 ?x)
        (BelowOf6 ?x)
        (AboveOf2 ?x)
      )
    )
    :effect ( and
      (CheckConsistency)
      (when
        (or
          (AboveOf1 ?x)
          (AboveOf5 ?x)
          (AboveOf3 ?x)
          (AboveOf4 ?x)
          (AboveOf9 ?x)
          (AboveOf8 ?x)
          (AboveOf7 ?x)
          (AboveOf0 ?x)
          (AboveOf2 ?x)
          (AboveOf10 ?x)
          (AboveOf11 ?x)
          (AboveOf6 ?x)
        )
        (and
          (AboveOf1 ?x)
        )
      )
      (when
        (or
          (AboveOf1 ?x)
          (AboveOf5 ?x)
          (AboveOf3 ?x)
          (AboveOf4 ?x)
          (AboveOf9 ?x)
          (AboveOf8 ?x)
          (AboveOf7 ?x)
          (AboveOf2 ?x)
          (AboveOf10 ?x)
          (AboveOf11 ?x)
          (AboveOf6 ?x)
        )
        (and
          (AboveOf2 ?x)
        )
      )
      (when
        (or
          (AboveOf5 ?x)
          (AboveOf3 ?x)
          (AboveOf4 ?x)
          (AboveOf9 ?x)
          (AboveOf8 ?x)
          (AboveOf7 ?x)
          (AboveOf2 ?x)
          (AboveOf10 ?x)
          (AboveOf6 ?x)
          (AboveOf11 ?x)
        )
        (and
          (AboveOf3 ?x)
        )
      )
      (when
        (or
          (AboveOf5 ?x)
          (AboveOf3 ?x)
          (AboveOf4 ?x)
          (AboveOf9 ?x)
          (AboveOf7 ?x)
          (AboveOf8 ?x)
          (AboveOf10 ?x)
          (AboveOf6 ?x)
          (AboveOf11 ?x)
        )
        (and
          (AboveOf4 ?x)
        )
      )
      (when
        (or
          (AboveOf5 ?x)
          (AboveOf4 ?x)
          (AboveOf9 ?x)
          (AboveOf7 ?x)
          (AboveOf8 ?x)
          (AboveOf10 ?x)
          (AboveOf6 ?x)
          (AboveOf11 ?x)
        )
        (and
          (AboveOf5 ?x)
        )
      )
      (when
        (or
          (AboveOf5 ?x)
          (AboveOf9 ?x)
          (AboveOf10 ?x)
          (AboveOf8 ?x)
          (AboveOf7 ?x)
          (AboveOf6 ?x)
          (AboveOf11 ?x)
        )
        (and
          (AboveOf6 ?x)
        )
      )
      (when
        (or
          (AboveOf9 ?x)
          (AboveOf10 ?x)
          (AboveOf8 ?x)
          (AboveOf7 ?x)
          (AboveOf6 ?x)
          (AboveOf11 ?x)
        )
        (and
          (AboveOf7 ?x)
        )
      )
      (when
        (or
          (AboveOf10 ?x)
          (AboveOf9 ?x)
          (AboveOf7 ?x)
          (AboveOf11 ?x)
          (AboveOf8 ?x)
        )
        (and
          (AboveOf8 ?x)
        )
      )
      (when
        (or
          (AboveOf9 ?x)
          (AboveOf10 ?x)
          (AboveOf11 ?x)
          (AboveOf8 ?x)
        )
        (and
          (AboveOf9 ?x)
        )
      )
      (when
        (or
          (AboveOf9 ?x)
          (AboveOf10 ?x)
          (AboveOf11 ?x)
        )
        (and
          (AboveOf10 ?x)
        )
      )
      (when
        (or
          (AboveOf11 ?x)
          (AboveOf10 ?x)
        )
        (and
          (AboveOf11 ?x)
        )
      )
      (when
        (BelowOf1 ?x)
        (and
          (BelowOf2 ?x)
          (not (BelowOf1 ?x))
        )
      )
      (when
        (or
          (BelowOf1 ?x)
          (BelowOf2 ?x)
        )
        (and
          (BelowOf3 ?x)
          (not (BelowOf2 ?x))
        )
      )
      (when
        (or
          (BelowOf3 ?x)
          (BelowOf1 ?x)
          (BelowOf2 ?x)
        )
        (and
          (BelowOf4 ?x)
          (not (BelowOf3 ?x))
        )
      )
      (when
        (or
          (BelowOf4 ?x)
          (BelowOf3 ?x)
          (BelowOf1 ?x)
          (BelowOf2 ?x)
        )
        (and
          (BelowOf5 ?x)
          (not (BelowOf4 ?x))
        )
      )
      (when
        (or
          (BelowOf5 ?x)
          (BelowOf4 ?x)
          (BelowOf3 ?x)
          (BelowOf1 ?x)
          (BelowOf2 ?x)
        )
        (and
          (BelowOf6 ?x)
          (not (BelowOf5 ?x))
        )
      )
      (when
        (or
          (BelowOf3 ?x)
          (BelowOf4 ?x)
          (BelowOf1 ?x)
          (BelowOf2 ?x)
          (BelowOf5 ?x)
          (BelowOf6 ?x)
        )
        (and
          (BelowOf7 ?x)
          (not (BelowOf6 ?x))
        )
      )
      (when
        (or
          (BelowOf3 ?x)
          (BelowOf7 ?x)
          (BelowOf4 ?x)
          (BelowOf1 ?x)
          (BelowOf2 ?x)
          (BelowOf5 ?x)
          (BelowOf6 ?x)
        )
        (and
          (BelowOf8 ?x)
          (not (BelowOf7 ?x))
        )
      )
      (when
        (or
          (BelowOf8 ?x)
          (BelowOf3 ?x)
          (BelowOf7 ?x)
          (BelowOf4 ?x)
          (BelowOf1 ?x)
          (BelowOf2 ?x)
          (BelowOf5 ?x)
          (BelowOf6 ?x)
        )
        (and
          (BelowOf9 ?x)
          (not (BelowOf8 ?x))
        )
      )
      (when
        (or
          (BelowOf8 ?x)
          (BelowOf3 ?x)
          (BelowOf9 ?x)
          (BelowOf7 ?x)
          (BelowOf4 ?x)
          (BelowOf1 ?x)
          (BelowOf2 ?x)
          (BelowOf5 ?x)
          (BelowOf6 ?x)
        )
        (and
          (BelowOf10 ?x)
          (not (BelowOf9 ?x))
        )
      )
      (when
        (or
          (BelowOf8 ?x)
          (BelowOf3 ?x)
          (BelowOf9 ?x)
          (BelowOf7 ?x)
          (BelowOf4 ?x)
          (BelowOf1 ?x)
          (BelowOf2 ?x)
          (BelowOf5 ?x)
          (BelowOf6 ?x)
          (BelowOf10 ?x)
        )
        (and
          (BelowOf11 ?x)
          (not (BelowOf10 ?x))
        )
      )
      (when
        (or
          (BelowOf8 ?x)
          (BelowOf3 ?x)
          (BelowOf9 ?x)
          (BelowOf7 ?x)
          (BelowOf4 ?x)
          (BelowOf1 ?x)
          (BelowOf2 ?x)
          (BelowOf5 ?x)
          (BelowOf6 ?x)
          (BelowOf11 ?x)
          (BelowOf10 ?x)
        )
        (and
          (BelowOf12 ?x)
          (not (BelowOf11 ?x))
        )
      )
      (when
        (Row0 ?x)
        (and
          (Row1 ?x)
          (not (Row0 ?x))
        )
      )
      (when
        (Row1 ?x)
        (and
          (Row2 ?x)
          (not (Row1 ?x))
        )
      )
      (when
        (Row2 ?x)
        (and
          (Row3 ?x)
          (not (Row2 ?x))
        )
      )
      (when
        (Row3 ?x)
        (and
          (Row4 ?x)
          (not (Row3 ?x))
        )
      )
      (when
        (Row4 ?x)
        (and
          (Row5 ?x)
          (not (Row4 ?x))
        )
      )
      (when
        (Row5 ?x)
        (and
          (Row6 ?x)
          (not (Row5 ?x))
        )
      )
      (when
        (Row6 ?x)
        (and
          (Row7 ?x)
          (not (Row6 ?x))
        )
      )
      (when
        (Row7 ?x)
        (and
          (Row8 ?x)
          (not (Row7 ?x))
        )
      )
      (when
        (Row8 ?x)
        (and
          (Row9 ?x)
          (not (Row8 ?x))
        )
      )
      (when
        (Row9 ?x)
        (and
          (Row10 ?x)
          (not (Row9 ?x))
        )
      )
      (when
        (Row10 ?x)
        (and
          (Row11 ?x)
          (not (Row10 ?x))
        )
      )
      (when
        (or
          (and
            (AboveOf9 ?x)
            (BelowOf1 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf3 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf8 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf0 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf10 ?x)
          )
          (and
            (AboveOf5 ?x)
            (BelowOf1 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf6 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf11 ?x)
          )
          (and
            (AboveOf7 ?x)
            (BelowOf1 ?x)
          )
          (and
            (AboveOf1 ?x)
            (BelowOf1 ?x)
          )
          (and
            (AboveOf2 ?x)
            (BelowOf1 ?x)
          )
          (and
            (AboveOf4 ?x)
            (BelowOf1 ?x)
          )
        )
        (and
          (Row1 ?x)
        )
      )
      (when
        (or
          (and
            (BelowOf2 ?x)
            (AboveOf3 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf8 ?x)
          )
          (and
            (AboveOf5 ?x)
            (BelowOf2 ?x)
          )
          (and
            (AboveOf4 ?x)
            (BelowOf1 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf8 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf10 ?x)
          )
          (and
            (AboveOf9 ?x)
            (BelowOf1 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf3 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf11 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf10 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf6 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf9 ?x)
          )
          (and
            (AboveOf2 ?x)
            (BelowOf1 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf6 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf4 ?x)
          )
          (and
            (AboveOf5 ?x)
            (BelowOf1 ?x)
          )
          (and
            (AboveOf1 ?x)
            (BelowOf1 ?x)
          )
          (and
            (AboveOf7 ?x)
            (BelowOf1 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf7 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf1 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf11 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf2 ?x)
          )
        )
        (and
          (Row2 ?x)
        )
      )
      (when
        (or
          (and
            (BelowOf2 ?x)
            (AboveOf3 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf8 ?x)
          )
          (and
            (AboveOf5 ?x)
            (BelowOf2 ?x)
          )
          (and
            (BelowOf3 ?x)
            (AboveOf11 ?x)
          )
          (and
            (AboveOf4 ?x)
            (BelowOf1 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf8 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf10 ?x)
          )
          (and
            (AboveOf9 ?x)
            (BelowOf1 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf3 ?x)
          )
          (and
            (AboveOf2 ?x)
            (BelowOf2 ?x)
          )
          (and
            (AboveOf10 ?x)
            (BelowOf3 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf10 ?x)
          )
          (and
            (AboveOf4 ?x)
            (BelowOf3 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf6 ?x)
          )
          (and
            (BelowOf3 ?x)
            (AboveOf8 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf9 ?x)
          )
          (and
            (BelowOf3 ?x)
            (AboveOf3 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf6 ?x)
          )
          (and
            (AboveOf2 ?x)
            (BelowOf3 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf4 ?x)
          )
          (and
            (AboveOf9 ?x)
            (BelowOf3 ?x)
          )
          (and
            (AboveOf5 ?x)
            (BelowOf1 ?x)
          )
          (and
            (AboveOf7 ?x)
            (BelowOf1 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf7 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf11 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf11 ?x)
          )
          (and
            (AboveOf7 ?x)
            (BelowOf3 ?x)
          )
          (and
            (AboveOf2 ?x)
            (BelowOf1 ?x)
          )
          (and
            (AboveOf6 ?x)
            (BelowOf3 ?x)
          )
          (and
            (AboveOf5 ?x)
            (BelowOf3 ?x)
          )
        )
        (and
          (Row3 ?x)
        )
      )
      (when
        (or
          (and
            (BelowOf2 ?x)
            (AboveOf3 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf8 ?x)
          )
          (and
            (AboveOf5 ?x)
            (BelowOf2 ?x)
          )
          (and
            (BelowOf3 ?x)
            (AboveOf11 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf11 ?x)
          )
          (and
            (AboveOf11 ?x)
            (BelowOf3 ?x)
          )
          (and
            (AboveOf4 ?x)
            (BelowOf1 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf8 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf10 ?x)
          )
          (and
            (AboveOf9 ?x)
            (BelowOf1 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf3 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf10 ?x)
          )
          (and
            (AboveOf10 ?x)
            (BelowOf3 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf10 ?x)
          )
          (and
            (AboveOf4 ?x)
            (BelowOf3 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf6 ?x)
          )
          (and
            (BelowOf3 ?x)
            (AboveOf8 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf9 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf6 ?x)
          )
          (and
            (AboveOf3 ?x)
            (BelowOf3 ?x)
          )
          (and
            (AboveOf5 ?x)
            (BelowOf4 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf4 ?x)
          )
          (and
            (AboveOf7 ?x)
            (BelowOf4 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf8 ?x)
          )
          (and
            (AboveOf4 ?x)
            (BelowOf4 ?x)
          )
          (and
            (AboveOf5 ?x)
            (BelowOf1 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf9 ?x)
          )
          (and
            (AboveOf7 ?x)
            (BelowOf1 ?x)
          )
          (and
            (AboveOf9 ?x)
            (BelowOf3 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf7 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf11 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf11 ?x)
          )
          (and
            (AboveOf7 ?x)
            (BelowOf3 ?x)
          )
          (and
            (AboveOf6 ?x)
            (BelowOf3 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf6 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf3 ?x)
          )
          (and
            (AboveOf5 ?x)
            (BelowOf3 ?x)
          )
        )
        (and
          (Row4 ?x)
        )
      )
      (when
        (or
          (and
            (AboveOf10 ?x)
            (BelowOf3 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf8 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf6 ?x)
          )
          (and
            (BelowOf3 ?x)
            (AboveOf11 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf11 ?x)
          )
          (and
            (AboveOf11 ?x)
            (BelowOf3 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf9 ?x)
          )
          (and
            (AboveOf4 ?x)
            (BelowOf1 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf8 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf10 ?x)
          )
          (and
            (AboveOf9 ?x)
            (BelowOf1 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf6 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf10 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf11 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf10 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf7 ?x)
          )
          (and
            (AboveOf4 ?x)
            (BelowOf3 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf5 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf6 ?x)
          )
          (and
            (BelowOf3 ?x)
            (AboveOf8 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf9 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf6 ?x)
          )
          (and
            (AboveOf5 ?x)
            (BelowOf4 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf4 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf11 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf10 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf8 ?x)
          )
          (and
            (AboveOf4 ?x)
            (BelowOf4 ?x)
          )
          (and
            (AboveOf5 ?x)
            (BelowOf1 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf9 ?x)
          )
          (and
            (AboveOf5 ?x)
            (BelowOf2 ?x)
          )
          (and
            (AboveOf7 ?x)
            (BelowOf1 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf8 ?x)
          )
          (and
            (AboveOf9 ?x)
            (BelowOf3 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf7 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf11 ?x)
          )
          (and
            (AboveOf7 ?x)
            (BelowOf3 ?x)
          )
          (and
            (AboveOf6 ?x)
            (BelowOf3 ?x)
          )
          (and
            (AboveOf7 ?x)
            (BelowOf4 ?x)
          )
          (and
            (AboveOf5 ?x)
            (BelowOf3 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf4 ?x)
          )
        )
        (and
          (Row5 ?x)
        )
      )
      (when
        (or
          (and
            (AboveOf10 ?x)
            (BelowOf3 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf8 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf6 ?x)
          )
          (and
            (BelowOf3 ?x)
            (AboveOf11 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf11 ?x)
          )
          (and
            (AboveOf10 ?x)
            (BelowOf6 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf11 ?x)
          )
          (and
            (AboveOf11 ?x)
            (BelowOf3 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf9 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf8 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf10 ?x)
          )
          (and
            (AboveOf9 ?x)
            (BelowOf6 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf6 ?x)
          )
          (and
            (BelowOf6 ?x)
            (AboveOf8 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf10 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf11 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf10 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf7 ?x)
          )
          (and
            (AboveOf6 ?x)
            (BelowOf6 ?x)
          )
          (and
            (BelowOf3 ?x)
            (AboveOf8 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf6 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf6 ?x)
          )
          (and
            (AboveOf5 ?x)
            (BelowOf4 ?x)
          )
          (and
            (AboveOf7 ?x)
            (BelowOf4 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf10 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf8 ?x)
          )
          (and
            (AboveOf9 ?x)
            (BelowOf3 ?x)
          )
          (and
            (AboveOf5 ?x)
            (BelowOf5 ?x)
          )
          (and
            (AboveOf7 ?x)
            (BelowOf6 ?x)
          )
          (and
            (AboveOf5 ?x)
            (BelowOf1 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf9 ?x)
          )
          (and
            (AboveOf5 ?x)
            (BelowOf2 ?x)
          )
          (and
            (AboveOf7 ?x)
            (BelowOf1 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf9 ?x)
          )
          (and
            (AboveOf9 ?x)
            (BelowOf1 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf8 ?x)
          )
          (and
            (AboveOf5 ?x)
            (BelowOf6 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf7 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf11 ?x)
          )
          (and
            (AboveOf7 ?x)
            (BelowOf3 ?x)
          )
          (and
            (AboveOf6 ?x)
            (BelowOf3 ?x)
          )
          (and
            (BelowOf6 ?x)
            (AboveOf11 ?x)
          )
          (and
            (AboveOf5 ?x)
            (BelowOf3 ?x)
          )
        )
        (and
          (Row6 ?x)
        )
      )
      (when
        (or
          (and
            (AboveOf10 ?x)
            (BelowOf3 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf8 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf6 ?x)
          )
          (and
            (BelowOf3 ?x)
            (AboveOf11 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf11 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf11 ?x)
          )
          (and
            (BelowOf7 ?x)
            (AboveOf11 ?x)
          )
          (and
            (BelowOf7 ?x)
            (AboveOf8 ?x)
          )
          (and
            (BelowOf7 ?x)
            (AboveOf7 ?x)
          )
          (and
            (AboveOf11 ?x)
            (BelowOf3 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf9 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf8 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf10 ?x)
          )
          (and
            (AboveOf9 ?x)
            (BelowOf6 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf6 ?x)
          )
          (and
            (BelowOf6 ?x)
            (AboveOf8 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf10 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf11 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf10 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf7 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf6 ?x)
          )
          (and
            (BelowOf3 ?x)
            (AboveOf8 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf9 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf6 ?x)
          )
          (and
            (BelowOf6 ?x)
            (AboveOf10 ?x)
          )
          (and
            (AboveOf7 ?x)
            (BelowOf4 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf10 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf8 ?x)
          )
          (and
            (AboveOf9 ?x)
            (BelowOf3 ?x)
          )
          (and
            (AboveOf7 ?x)
            (BelowOf6 ?x)
          )
          (and
            (BelowOf7 ?x)
            (AboveOf10 ?x)
          )
          (and
            (AboveOf7 ?x)
            (BelowOf1 ?x)
          )
          (and
            (AboveOf9 ?x)
            (BelowOf1 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf8 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf7 ?x)
          )
          (and
            (BelowOf6 ?x)
            (AboveOf6 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf11 ?x)
          )
          (and
            (BelowOf7 ?x)
            (AboveOf9 ?x)
          )
          (and
            (AboveOf7 ?x)
            (BelowOf3 ?x)
          )
          (and
            (AboveOf10 ?x)
            (BelowOf6 ?x)
          )
          (and
            (AboveOf6 ?x)
            (BelowOf3 ?x)
          )
          (and
            (BelowOf6 ?x)
            (AboveOf11 ?x)
          )
          (and
            (BelowOf7 ?x)
            (AboveOf6 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf9 ?x)
          )
        )
        (and
          (Row7 ?x)
        )
      )
      (when
        (or
          (and
            (AboveOf11 ?x)
            (BelowOf8 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf8 ?x)
          )
          (and
            (BelowOf3 ?x)
            (AboveOf11 ?x)
          )
          (and
            (AboveOf7 ?x)
            (BelowOf8 ?x)
          )
          (and
            (BelowOf7 ?x)
            (AboveOf11 ?x)
          )
          (and
            (BelowOf7 ?x)
            (AboveOf8 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf11 ?x)
          )
          (and
            (AboveOf11 ?x)
            (BelowOf3 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf9 ?x)
          )
          (and
            (AboveOf7 ?x)
            (BelowOf7 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf8 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf10 ?x)
          )
          (and
            (AboveOf9 ?x)
            (BelowOf6 ?x)
          )
          (and
            (BelowOf6 ?x)
            (AboveOf8 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf10 ?x)
          )
          (and
            (AboveOf10 ?x)
            (BelowOf3 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf10 ?x)
          )
          (and
            (AboveOf9 ?x)
            (BelowOf8 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf7 ?x)
          )
          (and
            (BelowOf3 ?x)
            (AboveOf8 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf9 ?x)
          )
          (and
            (BelowOf8 ?x)
            (AboveOf8 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf11 ?x)
          )
          (and
            (BelowOf6 ?x)
            (AboveOf10 ?x)
          )
          (and
            (AboveOf7 ?x)
            (BelowOf4 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf10 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf8 ?x)
          )
          (and
            (AboveOf9 ?x)
            (BelowOf3 ?x)
          )
          (and
            (AboveOf10 ?x)
            (BelowOf8 ?x)
          )
          (and
            (AboveOf7 ?x)
            (BelowOf6 ?x)
          )
          (and
            (BelowOf7 ?x)
            (AboveOf10 ?x)
          )
          (and
            (AboveOf7 ?x)
            (BelowOf1 ?x)
          )
          (and
            (AboveOf9 ?x)
            (BelowOf1 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf8 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf7 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf11 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf11 ?x)
          )
          (and
            (BelowOf7 ?x)
            (AboveOf9 ?x)
          )
          (and
            (AboveOf7 ?x)
            (BelowOf3 ?x)
          )
          (and
            (AboveOf10 ?x)
            (BelowOf6 ?x)
          )
          (and
            (BelowOf6 ?x)
            (AboveOf11 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf9 ?x)
          )
        )
        (and
          (Row8 ?x)
        )
      )
      (when
        (or
          (and
            (AboveOf11 ?x)
            (BelowOf8 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf8 ?x)
          )
          (and
            (AboveOf9 ?x)
            (BelowOf9 ?x)
          )
          (and
            (BelowOf3 ?x)
            (AboveOf11 ?x)
          )
          (and
            (BelowOf7 ?x)
            (AboveOf11 ?x)
          )
          (and
            (BelowOf9 ?x)
            (AboveOf11 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf11 ?x)
          )
          (and
            (BelowOf9 ?x)
            (AboveOf8 ?x)
          )
          (and
            (AboveOf11 ?x)
            (BelowOf3 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf9 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf8 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf10 ?x)
          )
          (and
            (AboveOf9 ?x)
            (BelowOf6 ?x)
          )
          (and
            (BelowOf6 ?x)
            (AboveOf8 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf10 ?x)
          )
          (and
            (AboveOf10 ?x)
            (BelowOf3 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf10 ?x)
          )
          (and
            (AboveOf9 ?x)
            (BelowOf8 ?x)
          )
          (and
            (BelowOf3 ?x)
            (AboveOf8 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf9 ?x)
          )
          (and
            (BelowOf6 ?x)
            (AboveOf10 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf11 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf10 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf8 ?x)
          )
          (and
            (AboveOf9 ?x)
            (BelowOf3 ?x)
          )
          (and
            (AboveOf10 ?x)
            (BelowOf8 ?x)
          )
          (and
            (BelowOf7 ?x)
            (AboveOf10 ?x)
          )
          (and
            (AboveOf9 ?x)
            (BelowOf1 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf8 ?x)
          )
          (and
            (AboveOf8 ?x)
            (BelowOf8 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf11 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf11 ?x)
          )
          (and
            (BelowOf7 ?x)
            (AboveOf9 ?x)
          )
          (and
            (BelowOf9 ?x)
            (AboveOf10 ?x)
          )
          (and
            (AboveOf10 ?x)
            (BelowOf6 ?x)
          )
          (and
            (BelowOf6 ?x)
            (AboveOf11 ?x)
          )
          (and
            (BelowOf7 ?x)
            (AboveOf8 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf9 ?x)
          )
        )
        (and
          (Row9 ?x)
        )
      )
      (when
        (or
          (and
            (AboveOf11 ?x)
            (BelowOf8 ?x)
          )
          (and
            (BelowOf3 ?x)
            (AboveOf11 ?x)
          )
          (and
            (AboveOf10 ?x)
            (BelowOf6 ?x)
          )
          (and
            (BelowOf9 ?x)
            (AboveOf11 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf11 ?x)
          )
          (and
            (AboveOf11 ?x)
            (BelowOf3 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf9 ?x)
          )
          (and
            (BelowOf10 ?x)
            (AboveOf11 ?x)
          )
          (and
            (BelowOf9 ?x)
            (AboveOf9 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf10 ?x)
          )
          (and
            (AboveOf9 ?x)
            (BelowOf6 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf10 ?x)
          )
          (and
            (AboveOf10 ?x)
            (BelowOf3 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf10 ?x)
          )
          (and
            (AboveOf9 ?x)
            (BelowOf8 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf9 ?x)
          )
          (and
            (AboveOf10 ?x)
            (BelowOf10 ?x)
          )
          (and
            (BelowOf6 ?x)
            (AboveOf10 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf11 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf10 ?x)
          )
          (and
            (AboveOf9 ?x)
            (BelowOf3 ?x)
          )
          (and
            (AboveOf10 ?x)
            (BelowOf8 ?x)
          )
          (and
            (BelowOf7 ?x)
            (AboveOf10 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf11 ?x)
          )
          (and
            (AboveOf9 ?x)
            (BelowOf1 ?x)
          )
          (and
            (AboveOf9 ?x)
            (BelowOf10 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf11 ?x)
          )
          (and
            (BelowOf7 ?x)
            (AboveOf9 ?x)
          )
          (and
            (BelowOf9 ?x)
            (AboveOf10 ?x)
          )
          (and
            (BelowOf7 ?x)
            (AboveOf11 ?x)
          )
          (and
            (BelowOf6 ?x)
            (AboveOf11 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf9 ?x)
          )
        )
        (and
          (Row10 ?x)
        )
      )
      (when
        (or
          (and
            (AboveOf11 ?x)
            (BelowOf8 ?x)
          )
          (and
            (BelowOf3 ?x)
            (AboveOf11 ?x)
          )
          (and
            (BelowOf7 ?x)
            (AboveOf11 ?x)
          )
          (and
            (BelowOf9 ?x)
            (AboveOf11 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf11 ?x)
          )
          (and
            (AboveOf11 ?x)
            (BelowOf3 ?x)
          )
          (and
            (BelowOf10 ?x)
            (AboveOf11 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf10 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf10 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf11 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf10 ?x)
          )
          (and
            (AboveOf10 ?x)
            (BelowOf10 ?x)
          )
          (and
            (BelowOf6 ?x)
            (AboveOf10 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf11 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf10 ?x)
          )
          (and
            (AboveOf10 ?x)
            (BelowOf8 ?x)
          )
          (and
            (BelowOf7 ?x)
            (AboveOf10 ?x)
          )
          (and
            (BelowOf11 ?x)
            (AboveOf11 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf11 ?x)
          )
          (and
            (AboveOf10 ?x)
            (BelowOf11 ?x)
          )
          (and
            (BelowOf9 ?x)
            (AboveOf10 ?x)
          )
          (and
            (BelowOf6 ?x)
            (AboveOf11 ?x)
          )
          (and
            (AboveOf10 ?x)
            (BelowOf3 ?x)
          )
        )
        (and
          (Row11 ?x)
        )
      )
    )
  )
  (:action CheckConsistencyAction
    :parameters ( )
    :precondition ( and
      (CheckConsistency)
      (not (Error))
    )
    :effect ( and
      (not (CheckConsistency))
      (when
        (or
          (exists (?x_9 )
            (and
              (LeftOf3 ?x_9)
              (RightOf11 ?x_9)
            )
          )
          (exists (?x_14 )
            (and
              (LeftOf1 ?x_14)
              (RightOf11 ?x_14)
            )
          )
          (exists (?x_21 )
            (and
              (LeftOf1 ?x_21)
              (RightOf8 ?x_21)
            )
          )
          (exists (?x_13 )
            (and
              (AboveOf6 ?x_13)
              (BelowOf4 ?x_13)
            )
          )
          (exists (?x_18 )
            (and
              (RightOf6 ?x_18)
              (LeftOf4 ?x_18)
            )
          )
          (exists (?x_17 )
            (and
              (RightOf3 ?x_17)
              (LeftOf1 ?x_17)
            )
          )
          (exists (?x_12 )
            (and
              (BelowOf3 ?x_12)
              (AboveOf8 ?x_12)
            )
          )
          (exists (?x_21 )
            (and
              (LeftOf5 ?x_21)
              (RightOf11 ?x_21)
            )
          )
          (exists (?x_19 )
            (and
              (LeftOf1 ?x_19)
              (RightOf11 ?x_19)
            )
          )
          (exists (?x_2 )
            (and
              (LeftOf2 ?x_2)
              (RightOf4 ?x_2)
            )
          )
          (exists (?x_6 )
            (and
              (BelowOf2 ?x_6)
              (AboveOf8 ?x_6)
            )
          )
          (exists (?x_12 )
            (and
              (BelowOf2 ?x_12)
              (AboveOf7 ?x_12)
            )
          )
          (exists (?x_13 )
            (and
              (AboveOf11 ?x_13)
              (BelowOf3 ?x_13)
            )
          )
          (exists (?x_2 )
            (and
              (LeftOf1 ?x_2)
              (RightOf5 ?x_2)
            )
          )
          (exists (?x_4 )
            (and
              (BelowOf1 ?x_4)
              (AboveOf5 ?x_4)
            )
          )
          (exists (?x_7 )
            (and
              (BelowOf3 ?x_7)
              (AboveOf6 ?x_7)
            )
          )
          (exists (?x_3 )
            (and
              (LeftOf7 ?x_3)
              (RightOf11 ?x_3)
            )
          )
          (exists (?x_18 )
            (and
              (RightOf8 ?x_18)
              (LeftOf1 ?x_18)
            )
          )
          (exists (?x_19 )
            (and
              (RightOf11 ?x_19)
              (LeftOf4 ?x_19)
            )
          )
          (exists (?x_18 )
            (and
              (LeftOf2 ?x_18)
              (RightOf7 ?x_18)
            )
          )
          (exists (?x_13 )
            (and
              (AboveOf5 ?x_13)
              (BelowOf2 ?x_13)
            )
          )
          (exists (?x_11 )
            (and
              (BelowOf7 ?x_11)
              (AboveOf10 ?x_11)
            )
          )
          (exists (?x_18 )
            (and
              (RightOf10 ?x_18)
              (LeftOf1 ?x_18)
            )
          )
          (exists (?x_10 )
            (and
              (LeftOf1 ?x_10)
              (RightOf10 ?x_10)
            )
          )
          (exists (?x_8 )
            (and
              (BelowOf6 ?x_8)
              (AboveOf11 ?x_8)
            )
          )
          (exists (?x_9 )
            (and
              (RightOf6 ?x_9)
              (LeftOf2 ?x_9)
            )
          )
          (exists (?x_1 )
            (and
              (RightOf11 ?x_1)
              (LeftOf6 ?x_1)
            )
          )
          (exists (?x_17 )
            (and
              (LeftOf2 ?x_17)
              (RightOf11 ?x_17)
            )
          )
          (exists (?x_6 )
            (and
              (BelowOf1 ?x_6)
              (AboveOf8 ?x_6)
            )
          )
          (exists (?x_15 )
            (and
              (AboveOf10 ?x_15)
              (BelowOf1 ?x_15)
            )
          )
          (exists (?x_20 )
            (and
              (RightOf8 ?x_20)
              (LeftOf3 ?x_20)
            )
          )
          (exists (?x_5 )
            (and
              (BelowOf2 ?x_5)
              (AboveOf11 ?x_5)
            )
          )
          (exists (?x_17 )
            (and
              (RightOf3 ?x_17)
              (LeftOf2 ?x_17)
            )
          )
          (exists (?x_2 )
            (and
              (LeftOf1 ?x_2)
              (RightOf9 ?x_2)
            )
          )
          (exists (?x_9 )
            (and
              (RightOf6 ?x_9)
              (LeftOf1 ?x_9)
            )
          )
          (exists (?x_13 )
            (and
              (AboveOf6 ?x_13)
              (BelowOf1 ?x_13)
            )
          )
          (exists (?x_21 )
            (and
              (LeftOf2 ?x_21)
              (RightOf8 ?x_21)
            )
          )
          (exists (?x_21 )
            (and
              (RightOf7 ?x_21)
              (LeftOf6 ?x_21)
            )
          )
          (exists (?x_2 )
            (and
              (LeftOf1 ?x_2)
              (RightOf4 ?x_2)
            )
          )
          (exists (?x_18 )
            (and
              (RightOf9 ?x_18)
              (LeftOf1 ?x_18)
            )
          )
          (exists (?x_14 )
            (and
              (RightOf10 ?x_14)
              (LeftOf8 ?x_14)
            )
          )
          (exists (?x_2 )
            (and
              (LeftOf2 ?x_2)
              (RightOf3 ?x_2)
            )
          )
          (exists (?x_20 )
            (and
              (RightOf6 ?x_20)
              (LeftOf3 ?x_20)
            )
          )
          (exists (?x_19 )
            (and
              (RightOf10 ?x_19)
              (LeftOf6 ?x_19)
            )
          )
          (exists (?x_13 )
            (and
              (BelowOf2 ?x_13)
              (AboveOf9 ?x_13)
            )
          )
          (exists (?x_12 )
            (and
              (AboveOf5 ?x_12)
              (BelowOf2 ?x_12)
            )
          )
          (exists (?x_3 )
            (and
              (LeftOf9 ?x_3)
              (RightOf9 ?x_3)
            )
          )
          (exists (?x_11 )
            (and
              (BelowOf8 ?x_11)
              (AboveOf11 ?x_11)
            )
          )
          (exists (?x_14 )
            (and
              (LeftOf9 ?x_14)
              (RightOf10 ?x_14)
            )
          )
          (exists (?x_19 )
            (and
              (RightOf11 ?x_19)
              (LeftOf6 ?x_19)
            )
          )
          (exists (?x_19 )
            (and
              (LeftOf2 ?x_19)
              (RightOf8 ?x_19)
            )
          )
          (exists (?x_6 )
            (and
              (AboveOf9 ?x_6)
              (BelowOf1 ?x_6)
            )
          )
          (exists (?x_13 )
            (and
              (AboveOf6 ?x_13)
              (BelowOf2 ?x_13)
            )
          )
          (exists (?x_4 )
            (and
              (BelowOf1 ?x_4)
              (AboveOf4 ?x_4)
            )
          )
          (exists (?x_5 )
            (and
              (BelowOf3 ?x_5)
              (AboveOf8 ?x_5)
            )
          )
          (exists (?x_18 )
            (and
              (LeftOf1 ?x_18)
              (RightOf11 ?x_18)
            )
          )
          (exists (?x_17 )
            (and
              (RightOf11 ?x_17)
              (LeftOf1 ?x_17)
            )
          )
          (exists (?x_3 )
            (and
              (LeftOf1 ?x_3)
              (RightOf10 ?x_3)
            )
          )
          (exists (?x_13 )
            (and
              (AboveOf8 ?x_13)
              (BelowOf4 ?x_13)
            )
          )
          (exists (?x_20 )
            (and
              (LeftOf2 ?x_20)
              (RightOf11 ?x_20)
            )
          )
          (exists (?x_18 )
            (and
              (LeftOf2 ?x_18)
              (RightOf5 ?x_18)
            )
          )
          (exists (?x_21 )
            (and
              (LeftOf1 ?x_21)
              (RightOf7 ?x_21)
            )
          )
          (exists (?x_0 )
            (and
              (BelowOf7 ?x_0)
              (AboveOf11 ?x_0)
            )
          )
          (exists (?x_21 )
            (and
              (LeftOf2 ?x_21)
              (RightOf9 ?x_21)
            )
          )
          (exists (?x_15 )
            (and
              (BelowOf2 ?x_15)
              (AboveOf11 ?x_15)
            )
          )
          (exists (?x_18 )
            (and
              (RightOf10 ?x_18)
              (LeftOf2 ?x_18)
            )
          )
          (exists (?x_9 )
            (and
              (LeftOf2 ?x_9)
              (RightOf11 ?x_9)
            )
          )
          (exists (?x_9 )
            (and
              (RightOf10 ?x_9)
              (LeftOf6 ?x_9)
            )
          )
          (exists (?x_18 )
            (and
              (RightOf8 ?x_18)
              (LeftOf3 ?x_18)
            )
          )
          (exists (?x_19 )
            (and
              (RightOf11 ?x_19)
              (LeftOf3 ?x_19)
            )
          )
          (exists (?x_10 )
            (and
              (LeftOf1 ?x_10)
              (RightOf4 ?x_10)
            )
          )
          (exists (?x_10 )
            (and
              (LeftOf1 ?x_10)
              (RightOf11 ?x_10)
            )
          )
          (exists (?x_21 )
            (and
              (RightOf7 ?x_21)
              (LeftOf5 ?x_21)
            )
          )
          (exists (?x_21 )
            (and
              (LeftOf7 ?x_21)
              (RightOf8 ?x_21)
            )
          )
          (exists (?x_19 )
            (and
              (RightOf11 ?x_19)
              (LeftOf1 ?x_19)
            )
          )
          (exists (?x_19 )
            (and
              (LeftOf2 ?x_19)
              (RightOf11 ?x_19)
            )
          )
          (exists (?x_0 )
            (and
              (AboveOf8 ?x_0)
              (BelowOf2 ?x_0)
            )
          )
          (exists (?x_21 )
            (and
              (RightOf7 ?x_21)
              (LeftOf3 ?x_21)
            )
          )
          (exists (?x_12 )
            (and
              (AboveOf10 ?x_12)
              (BelowOf4 ?x_12)
            )
          )
          (exists (?x_15 )
            (and
              (BelowOf6 ?x_15)
              (AboveOf11 ?x_15)
            )
          )
          (exists (?x_15 )
            (and
              (BelowOf3 ?x_15)
              (AboveOf11 ?x_15)
            )
          )
          (exists (?x_15 )
            (and
              (BelowOf6 ?x_15)
              (AboveOf9 ?x_15)
            )
          )
          (exists (?x_11 )
            (and
              (BelowOf9 ?x_11)
              (AboveOf9 ?x_11)
            )
          )
          (exists (?x_19 )
            (and
              (RightOf9 ?x_19)
              (LeftOf2 ?x_19)
            )
          )
          (exists (?x_7 )
            (and
              (AboveOf8 ?x_7)
              (BelowOf2 ?x_7)
            )
          )
          (exists (?x_12 )
            (and
              (AboveOf10 ?x_12)
              (BelowOf2 ?x_12)
            )
          )
          (exists (?x_5 )
            (and
              (BelowOf3 ?x_5)
              (AboveOf11 ?x_5)
            )
          )
          (exists (?x_18 )
            (and
              (RightOf5 ?x_18)
              (LeftOf3 ?x_18)
            )
          )
          (exists (?x_0 )
            (and
              (AboveOf11 ?x_0)
              (BelowOf1 ?x_0)
            )
          )
          (exists (?x_21 )
            (and
              (RightOf8 ?x_21)
              (LeftOf6 ?x_21)
            )
          )
          (exists (?x_13 )
            (and
              (AboveOf8 ?x_13)
              (BelowOf2 ?x_13)
            )
          )
          (exists (?x_9 )
            (and
              (LeftOf4 ?x_9)
              (RightOf11 ?x_9)
            )
          )
          (exists (?x_18 )
            (and
              (RightOf6 ?x_18)
              (LeftOf3 ?x_18)
            )
          )
          (exists (?x_0 )
            (and
              (AboveOf9 ?x_0)
              (BelowOf2 ?x_0)
            )
          )
          (exists (?x_7 )
            (and
              (BelowOf2 ?x_7)
              (AboveOf9 ?x_7)
            )
          )
          (exists (?x_9 )
            (and
              (RightOf7 ?x_9)
              (LeftOf1 ?x_9)
            )
          )
          (exists (?x_19 )
            (and
              (RightOf9 ?x_19)
              (LeftOf5 ?x_19)
            )
          )
          (exists (?x_5 )
            (and
              (BelowOf3 ?x_5)
              (AboveOf10 ?x_5)
            )
          )
          (exists (?x_6 )
            (and
              (BelowOf2 ?x_6)
              (AboveOf6 ?x_6)
            )
          )
          (exists (?x_9 )
            (and
              (RightOf6 ?x_9)
              (LeftOf6 ?x_9)
            )
          )
          (exists (?x_3 )
            (and
              (LeftOf3 ?x_3)
              (RightOf9 ?x_3)
            )
          )
          (exists (?x_19 )
            (and
              (RightOf9 ?x_19)
              (LeftOf4 ?x_19)
            )
          )
          (exists (?x_1 )
            (and
              (RightOf11 ?x_1)
              (LeftOf8 ?x_1)
            )
          )
          (exists (?x_6 )
            (and
              (BelowOf2 ?x_6)
              (AboveOf7 ?x_6)
            )
          )
          (exists (?x_20 )
            (and
              (LeftOf4 ?x_20)
              (RightOf9 ?x_20)
            )
          )
          (exists (?x_7 )
            (and
              (AboveOf9 ?x_7)
              (BelowOf1 ?x_7)
            )
          )
          (exists (?x_9 )
            (and
              (LeftOf6 ?x_9)
              (RightOf11 ?x_9)
            )
          )
          (exists (?x_21 )
            (and
              (RightOf10 ?x_21)
              (LeftOf4 ?x_21)
            )
          )
          (exists (?x_12 )
            (and
              (AboveOf6 ?x_12)
              (BelowOf4 ?x_12)
            )
          )
          (exists (?x_13 )
            (and
              (AboveOf7 ?x_13)
              (BelowOf4 ?x_13)
            )
          )
          (exists (?x_6 )
            (and
              (BelowOf2 ?x_6)
              (AboveOf5 ?x_6)
            )
          )
          (exists (?x_4 )
            (and
              (AboveOf8 ?x_4)
              (BelowOf1 ?x_4)
            )
          )
          (exists (?x_9 )
            (and
              (LeftOf4 ?x_9)
              (RightOf6 ?x_9)
            )
          )
          (exists (?x_15 )
            (and
              (BelowOf4 ?x_15)
              (AboveOf9 ?x_15)
            )
          )
          (exists (?x_18 )
            (and
              (RightOf6 ?x_18)
              (LeftOf1 ?x_18)
            )
          )
          (exists (?x_21 )
            (and
              (LeftOf4 ?x_21)
              (RightOf11 ?x_21)
            )
          )
          (exists (?x_21 )
            (and
              (RightOf11 ?x_21)
              (LeftOf5 ?x_21)
            )
          )
          (exists (?x_15 )
            (and
              (AboveOf8 ?x_15)
              (BelowOf5 ?x_15)
            )
          )
          (exists (?x_19 )
            (and
              (RightOf10 ?x_19)
              (LeftOf1 ?x_19)
            )
          )
          (exists (?x_0 )
            (and
              (BelowOf5 ?x_0)
              (AboveOf11 ?x_0)
            )
          )
          (exists (?x_5 )
            (and
              (AboveOf6 ?x_5)
              (BelowOf2 ?x_5)
            )
          )
          (exists (?x_13 )
            (and
              (AboveOf5 ?x_13)
              (BelowOf4 ?x_13)
            )
          )
          (exists (?x_9 )
            (and
              (RightOf9 ?x_9)
              (LeftOf1 ?x_9)
            )
          )
          (exists (?x_13 )
            (and
              (BelowOf1 ?x_13)
              (AboveOf9 ?x_13)
            )
          )
          (exists (?x_16 )
            (and
              (BelowOf8 ?x_16)
              (AboveOf10 ?x_16)
            )
          )
          (exists (?x_11 )
            (and
              (BelowOf6 ?x_11)
              (AboveOf10 ?x_11)
            )
          )
          (exists (?x_16 )
            (and
              (BelowOf7 ?x_16)
              (AboveOf11 ?x_16)
            )
          )
          (exists (?x_16 )
            (and
              (AboveOf11 ?x_16)
              (BelowOf1 ?x_16)
            )
          )
          (exists (?x_14 )
            (and
              (RightOf10 ?x_14)
              (LeftOf7 ?x_14)
            )
          )
          (exists (?x_11 )
            (and
              (BelowOf1 ?x_11)
              (AboveOf9 ?x_11)
            )
          )
          (exists (?x_1 )
            (and
              (RightOf11 ?x_1)
              (LeftOf10 ?x_1)
            )
          )
          (exists (?x_11 )
            (and
              (AboveOf11 ?x_11)
              (BelowOf4 ?x_11)
            )
          )
          (exists (?x_11 )
            (and
              (AboveOf11 ?x_11)
              (BelowOf6 ?x_11)
            )
          )
          (exists (?x_0 )
            (and
              (BelowOf5 ?x_0)
              (AboveOf9 ?x_0)
            )
          )
          (exists (?x_13 )
            (and
              (BelowOf3 ?x_13)
              (AboveOf10 ?x_13)
            )
          )
          (exists (?x_20 )
            (and
              (LeftOf4 ?x_20)
              (RightOf4 ?x_20)
            )
          )
          (exists (?x_10 )
            (and
              (LeftOf1 ?x_10)
              (RightOf8 ?x_10)
            )
          )
          (exists (?x_18 )
            (and
              (LeftOf5 ?x_18)
              (RightOf11 ?x_18)
            )
          )
          (exists (?x_20 )
            (and
              (RightOf6 ?x_20)
              (LeftOf1 ?x_20)
            )
          )
          (exists (?x_0 )
            (and
              (BelowOf7 ?x_0)
              (AboveOf10 ?x_0)
            )
          )
          (exists (?x_4 )
            (and
              (AboveOf1 ?x_4)
              (BelowOf1 ?x_4)
            )
          )
          (exists (?x_6 )
            (and
              (BelowOf1 ?x_6)
              (AboveOf3 ?x_6)
            )
          )
          (exists (?x_16 )
            (and
              (AboveOf11 ?x_16)
              (BelowOf4 ?x_16)
            )
          )
          (exists (?x_5 )
            (and
              (BelowOf4 ?x_5)
              (AboveOf7 ?x_5)
            )
          )
          (exists (?x_0 )
            (and
              (BelowOf7 ?x_0)
              (AboveOf8 ?x_0)
            )
          )
          (exists (?x_5 )
            (and
              (AboveOf6 ?x_5)
              (BelowOf1 ?x_5)
            )
          )
          (exists (?x_10 )
            (and
              (RightOf3 ?x_10)
              (LeftOf1 ?x_10)
            )
          )
          (exists (?x_7 )
            (and
              (BelowOf1 ?x_7)
              (AboveOf3 ?x_7)
            )
          )
          (exists (?x_9 )
            (and
              (LeftOf5 ?x_9)
              (RightOf11 ?x_9)
            )
          )
          (exists (?x_17 )
            (and
              (RightOf4 ?x_17)
              (LeftOf3 ?x_17)
            )
          )
          (exists (?x_14 )
            (and
              (RightOf11 ?x_14)
              (LeftOf10 ?x_14)
            )
          )
          (exists (?x_12 )
            (and
              (AboveOf4 ?x_12)
              (BelowOf1 ?x_12)
            )
          )
          (exists (?x_20 )
            (and
              (RightOf5 ?x_20)
              (LeftOf1 ?x_20)
            )
          )
          (exists (?x_9 )
            (and
              (LeftOf4 ?x_9)
              (RightOf8 ?x_9)
            )
          )
          (exists (?x_12 )
            (and
              (AboveOf11 ?x_12)
              (BelowOf2 ?x_12)
            )
          )
          (exists (?x_17 )
            (and
              (RightOf10 ?x_17)
              (LeftOf3 ?x_17)
            )
          )
          (exists (?x_15 )
            (and
              (AboveOf10 ?x_15)
              (BelowOf7 ?x_15)
            )
          )
          (exists (?x_15 )
            (and
              (AboveOf10 ?x_15)
              (BelowOf5 ?x_15)
            )
          )
          (exists (?x_16 )
            (and
              (BelowOf5 ?x_16)
              (AboveOf10 ?x_16)
            )
          )
          (exists (?x_5 )
            (and
              (BelowOf4 ?x_5)
              (AboveOf10 ?x_5)
            )
          )
          (exists (?x_0 )
            (and
              (BelowOf3 ?x_0)
              (AboveOf8 ?x_0)
            )
          )
          (exists (?x_14 )
            (and
              (RightOf10 ?x_14)
              (LeftOf5 ?x_14)
            )
          )
          (exists (?x_8 )
            (and
              (AboveOf11 ?x_8)
              (BelowOf4 ?x_8)
            )
          )
          (exists (?x_7 )
            (and
              (BelowOf3 ?x_7)
              (AboveOf5 ?x_7)
            )
          )
          (exists (?x_5 )
            (and
              (BelowOf2 ?x_5)
              (AboveOf8 ?x_5)
            )
          )
          (exists (?x_19 )
            (and
              (RightOf8 ?x_19)
              (LeftOf3 ?x_19)
            )
          )
          (exists (?x_5 )
            (and
              (BelowOf4 ?x_5)
              (AboveOf8 ?x_5)
            )
          )
          (exists (?x_13 )
            (and
              (AboveOf11 ?x_13)
              (BelowOf1 ?x_13)
            )
          )
          (exists (?x_7 )
            (and
              (AboveOf8 ?x_7)
              (BelowOf3 ?x_7)
            )
          )
          (exists (?x_16 )
            (and
              (AboveOf11 ?x_16)
              (BelowOf6 ?x_16)
            )
          )
          (exists (?x_19 )
            (and
              (LeftOf8 ?x_19)
              (RightOf11 ?x_19)
            )
          )
          (exists (?x_4 )
            (and
              (BelowOf1 ?x_4)
              (AboveOf3 ?x_4)
            )
          )
          (exists (?x_12 )
            (and
              (AboveOf7 ?x_12)
              (BelowOf4 ?x_12)
            )
          )
          (exists (?x_11 )
            (and
              (AboveOf9 ?x_11)
              (BelowOf4 ?x_11)
            )
          )
          (exists (?x_15 )
            (and
              (AboveOf9 ?x_15)
              (BelowOf1 ?x_15)
            )
          )
          (exists (?x_20 )
            (and
              (LeftOf2 ?x_20)
              (RightOf5 ?x_20)
            )
          )
          (exists (?x_5 )
            (and
              (BelowOf3 ?x_5)
              (AboveOf9 ?x_5)
            )
          )
          (exists (?x_5 )
            (and
              (BelowOf6 ?x_5)
              (AboveOf10 ?x_5)
            )
          )
          (exists (?x_13 )
            (and
              (AboveOf6 ?x_13)
              (BelowOf5 ?x_13)
            )
          )
          (exists (?x_12 )
            (and
              (BelowOf4 ?x_12)
              (AboveOf4 ?x_12)
            )
          )
          (exists (?x_12 )
            (and
              (BelowOf3 ?x_12)
              (AboveOf5 ?x_12)
            )
          )
          (exists (?x_5 )
            (and
              (BelowOf2 ?x_5)
              (AboveOf9 ?x_5)
            )
          )
          (exists (?x_17 )
            (and
              (LeftOf2 ?x_17)
              (RightOf4 ?x_17)
            )
          )
          (exists (?x_18 )
            (and
              (LeftOf4 ?x_18)
              (RightOf7 ?x_18)
            )
          )
          (exists (?x_5 )
            (and
              (BelowOf3 ?x_5)
              (AboveOf7 ?x_5)
            )
          )
          (exists (?x_2 )
            (and
              (LeftOf1 ?x_2)
              (RightOf8 ?x_2)
            )
          )
          (exists (?x_19 )
            (and
              (RightOf10 ?x_19)
              (LeftOf8 ?x_19)
            )
          )
          (exists (?x_20 )
            (and
              (LeftOf2 ?x_20)
              (RightOf6 ?x_20)
            )
          )
          (exists (?x_7 )
            (and
              (BelowOf2 ?x_7)
              (AboveOf4 ?x_7)
            )
          )
          (exists (?x_11 )
            (and
              (AboveOf9 ?x_11)
              (BelowOf2 ?x_11)
            )
          )
          (exists (?x_7 )
            (and
              (AboveOf11 ?x_7)
              (BelowOf2 ?x_7)
            )
          )
          (exists (?x_5 )
            (and
              (BelowOf6 ?x_5)
              (AboveOf11 ?x_5)
            )
          )
          (exists (?x_0 )
            (and
              (BelowOf1 ?x_0)
              (AboveOf10 ?x_0)
            )
          )
          (exists (?x_19 )
            (and
              (RightOf9 ?x_19)
              (LeftOf1 ?x_19)
            )
          )
          (exists (?x_20 )
            (and
              (LeftOf3 ?x_20)
              (RightOf9 ?x_20)
            )
          )
          (exists (?x_0 )
            (and
              (BelowOf6 ?x_0)
              (AboveOf10 ?x_0)
            )
          )
          (exists (?x_12 )
            (and
              (BelowOf3 ?x_12)
              (AboveOf9 ?x_12)
            )
          )
          (exists (?x_7 )
            (and
              (AboveOf9 ?x_7)
              (BelowOf3 ?x_7)
            )
          )
          (exists (?x_0 )
            (and
              (BelowOf5 ?x_0)
              (AboveOf8 ?x_0)
            )
          )
          (exists (?x_5 )
            (and
              (BelowOf1 ?x_5)
              (AboveOf7 ?x_5)
            )
          )
          (exists (?x_6 )
            (and
              (AboveOf2 ?x_6)
              (BelowOf2 ?x_6)
            )
          )
          (exists (?x_0 )
            (and
              (BelowOf2 ?x_0)
              (AboveOf10 ?x_0)
            )
          )
          (exists (?x_2 )
            (and
              (LeftOf1 ?x_2)
              (RightOf2 ?x_2)
            )
          )
          (exists (?x_7 )
            (and
              (BelowOf1 ?x_7)
              (AboveOf4 ?x_7)
            )
          )
          (exists (?x_21 )
            (and
              (LeftOf2 ?x_21)
              (RightOf11 ?x_21)
            )
          )
          (exists (?x_13 )
            (and
              (AboveOf10 ?x_13)
              (BelowOf4 ?x_13)
            )
          )
          (exists (?x_5 )
            (and
              (BelowOf1 ?x_5)
              (AboveOf11 ?x_5)
            )
          )
          (exists (?x_19 )
            (and
              (LeftOf7 ?x_19)
              (RightOf8 ?x_19)
            )
          )
          (exists (?x_13 )
            (and
              (AboveOf9 ?x_13)
              (BelowOf1 ?x_13)
            )
          )
          (exists (?x_13 )
            (and
              (AboveOf11 ?x_13)
              (BelowOf2 ?x_13)
            )
          )
          (exists (?x_4 )
            (and
              (AboveOf7 ?x_4)
              (BelowOf1 ?x_4)
            )
          )
          (exists (?x_18 )
            (and
              (RightOf9 ?x_18)
              (LeftOf4 ?x_18)
            )
          )
          (exists (?x_20 )
            (and
              (LeftOf1 ?x_20)
              (RightOf9 ?x_20)
            )
          )
          (exists (?x_18 )
            (and
              (LeftOf4 ?x_18)
              (RightOf5 ?x_18)
            )
          )
          (exists (?x_5 )
            (and
              (BelowOf6 ?x_5)
              (AboveOf7 ?x_5)
            )
          )
          (exists (?x_1 )
            (and
              (RightOf11 ?x_1)
              (LeftOf7 ?x_1)
            )
          )
          (exists (?x_20 )
            (and
              (RightOf5 ?x_20)
              (LeftOf3 ?x_20)
            )
          )
          (exists (?x_7 )
            (and
              (BelowOf2 ?x_7)
              (AboveOf10 ?x_7)
            )
          )
          (exists (?x_15 )
            (and
              (AboveOf10 ?x_15)
              (BelowOf2 ?x_15)
            )
          )
          (exists (?x_9 )
            (and
              (RightOf10 ?x_9)
              (LeftOf1 ?x_9)
            )
          )
          (exists (?x_21 )
            (and
              (LeftOf4 ?x_21)
              (RightOf7 ?x_21)
            )
          )
          (exists (?x_18 )
            (and
              (RightOf9 ?x_18)
              (LeftOf3 ?x_18)
            )
          )
          (exists (?x_15 )
            (and
              (BelowOf2 ?x_15)
              (AboveOf7 ?x_15)
            )
          )
          (exists (?x_13 )
            (and
              (BelowOf3 ?x_13)
              (AboveOf9 ?x_13)
            )
          )
          (exists (?x_13 )
            (and
              (BelowOf3 ?x_13)
              (AboveOf6 ?x_13)
            )
          )
          (exists (?x_11 )
            (and
              (AboveOf11 ?x_11)
              (BelowOf1 ?x_11)
            )
          )
          (exists (?x_13 )
            (and
              (BelowOf3 ?x_13)
              (AboveOf5 ?x_13)
            )
          )
          (exists (?x_16 )
            (and
              (BelowOf7 ?x_16)
              (AboveOf10 ?x_16)
            )
          )
          (exists (?x_0 )
            (and
              (AboveOf8 ?x_0)
              (BelowOf8 ?x_0)
            )
          )
          (exists (?x_21 )
            (and
              (RightOf10 ?x_21)
              (LeftOf3 ?x_21)
            )
          )
          (exists (?x_6 )
            (and
              (AboveOf10 ?x_6)
              (BelowOf2 ?x_6)
            )
          )
          (exists (?x_1 )
            (and
              (RightOf11 ?x_1)
              (LeftOf1 ?x_1)
            )
          )
          (exists (?x_9 )
            (and
              (LeftOf1 ?x_9)
              (RightOf9 ?x_9)
            )
          )
          (exists (?x_8 )
            (and
              (AboveOf11 ?x_8)
              (BelowOf10 ?x_8)
            )
          )
          (exists (?x_13 )
            (and
              (BelowOf3 ?x_13)
              (AboveOf11 ?x_13)
            )
          )
          (exists (?x_17 )
            (and
              (RightOf9 ?x_17)
              (LeftOf1 ?x_17)
            )
          )
          (exists (?x_15 )
            (and
              (AboveOf11 ?x_15)
              (BelowOf7 ?x_15)
            )
          )
          (exists (?x_11 )
            (and
              (AboveOf10 ?x_11)
              (BelowOf2 ?x_11)
            )
          )
          (exists (?x_13 )
            (and
              (AboveOf11 ?x_13)
              (BelowOf5 ?x_13)
            )
          )
          (exists (?x_13 )
            (and
              (AboveOf5 ?x_13)
              (BelowOf1 ?x_13)
            )
          )
          (exists (?x_3 )
            (and
              (LeftOf1 ?x_3)
              (RightOf11 ?x_3)
            )
          )
          (exists (?x_19 )
            (and
              (RightOf9 ?x_19)
              (LeftOf8 ?x_19)
            )
          )
          (exists (?x_6 )
            (and
              (AboveOf9 ?x_6)
              (BelowOf2 ?x_6)
            )
          )
          (exists (?x_12 )
            (and
              (BelowOf4 ?x_12)
              (AboveOf9 ?x_12)
            )
          )
          (exists (?x_5 )
            (and
              (AboveOf10 ?x_5)
              (BelowOf1 ?x_5)
            )
          )
          (exists (?x_18 )
            (and
              (LeftOf2 ?x_18)
              (RightOf11 ?x_18)
            )
          )
          (exists (?x_7 )
            (and
              (AboveOf3 ?x_7)
              (BelowOf3 ?x_7)
            )
          )
          (exists (?x_15 )
            (and
              (BelowOf3 ?x_15)
              (AboveOf8 ?x_15)
            )
          )
          (exists (?x_3 )
            (and
              (LeftOf9 ?x_3)
              (RightOf10 ?x_3)
            )
          )
          (exists (?x_0 )
            (and
              (BelowOf4 ?x_0)
              (AboveOf9 ?x_0)
            )
          )
          (exists (?x_18 )
            (and
              (LeftOf4 ?x_18)
              (RightOf11 ?x_18)
            )
          )
          (exists (?x_3 )
            (and
              (RightOf10 ?x_3)
              (LeftOf2 ?x_3)
            )
          )
          (exists (?x_9 )
            (and
              (LeftOf4 ?x_9)
              (RightOf9 ?x_9)
            )
          )
          (exists (?x_4 )
            (and
              (BelowOf1 ?x_4)
              (AboveOf10 ?x_4)
            )
          )
          (exists (?x_15 )
            (and
              (AboveOf11 ?x_15)
              (BelowOf5 ?x_15)
            )
          )
          (exists (?x_21 )
            (and
              (LeftOf7 ?x_21)
              (RightOf10 ?x_21)
            )
          )
          (exists (?x_18 )
            (and
              (LeftOf5 ?x_18)
              (RightOf5 ?x_18)
            )
          )
          (exists (?x_17 )
            (and
              (RightOf6 ?x_17)
              (LeftOf1 ?x_17)
            )
          )
          (exists (?x_12 )
            (and
              (BelowOf3 ?x_12)
              (AboveOf7 ?x_12)
            )
          )
          (exists (?x_19 )
            (and
              (RightOf8 ?x_19)
              (LeftOf6 ?x_19)
            )
          )
          (exists (?x_19 )
            (and
              (LeftOf5 ?x_19)
              (RightOf8 ?x_19)
            )
          )
          (exists (?x_11 )
            (and
              (BelowOf9 ?x_11)
              (AboveOf10 ?x_11)
            )
          )
          (exists (?x_17 )
            (and
              (LeftOf1 ?x_17)
              (RightOf5 ?x_17)
            )
          )
          (exists (?x_21 )
            (and
              (LeftOf2 ?x_21)
              (RightOf10 ?x_21)
            )
          )
          (exists (?x_15 )
            (and
              (BelowOf1 ?x_15)
              (AboveOf11 ?x_15)
            )
          )
          (exists (?x_18 )
            (and
              (RightOf10 ?x_18)
              (LeftOf3 ?x_18)
            )
          )
          (exists (?x_12 )
            (and
              (AboveOf8 ?x_12)
              (BelowOf2 ?x_12)
            )
          )
          (exists (?x_11 )
            (and
              (AboveOf11 ?x_11)
              (BelowOf9 ?x_11)
            )
          )
          (exists (?x_10 )
            (and
              (LeftOf1 ?x_10)
              (RightOf9 ?x_10)
            )
          )
          (exists (?x_3 )
            (and
              (LeftOf8 ?x_3)
              (RightOf10 ?x_3)
            )
          )
          (exists (?x_21 )
            (and
              (RightOf9 ?x_21)
              (LeftOf5 ?x_21)
            )
          )
          (exists (?x_0 )
            (and
              (AboveOf11 ?x_0)
              (BelowOf2 ?x_0)
            )
          )
          (exists (?x_2 )
            (and
              (RightOf5 ?x_2)
              (LeftOf1 ?x_2)
            )
          )
          (exists (?x_17 )
            (and
              (LeftOf1 ?x_17)
              (RightOf11 ?x_17)
            )
          )
          (exists (?x_9 )
            (and
              (LeftOf2 ?x_9)
              (RightOf7 ?x_9)
            )
          )
          (exists (?x_21 )
            (and
              (RightOf8 ?x_21)
              (LeftOf5 ?x_21)
            )
          )
          (exists (?x_20 )
            (and
              (LeftOf1 ?x_20)
              (RightOf11 ?x_20)
            )
          )
          (exists (?x_0 )
            (and
              (AboveOf8 ?x_0)
              (BelowOf1 ?x_0)
            )
          )
          (exists (?x_18 )
            (and
              (RightOf8 ?x_18)
              (LeftOf4 ?x_18)
            )
          )
          (exists (?x_1 )
            (and
              (RightOf11 ?x_1)
              (LeftOf11 ?x_1)
            )
          )
          (exists (?x_12 )
            (and
              (BelowOf3 ?x_12)
              (AboveOf11 ?x_12)
            )
          )
          (exists (?x_13 )
            (and
              (AboveOf8 ?x_13)
              (BelowOf1 ?x_13)
            )
          )
          (exists (?x_21 )
            (and
              (LeftOf1 ?x_21)
              (RightOf9 ?x_21)
            )
          )
          (exists (?x_5 )
            (and
              (BelowOf5 ?x_5)
              (AboveOf8 ?x_5)
            )
          )
          (exists (?x_12 )
            (and
              (AboveOf11 ?x_12)
              (BelowOf1 ?x_12)
            )
          )
          (exists (?x_2 )
            (and
              (LeftOf1 ?x_2)
              (RightOf11 ?x_2)
            )
          )
          (exists (?x_20 )
            (and
              (RightOf8 ?x_20)
              (LeftOf1 ?x_20)
            )
          )
          (exists (?x_8 )
            (and
              (AboveOf11 ?x_8)
              (BelowOf1 ?x_8)
            )
          )
          (exists (?x_17 )
            (and
              (LeftOf2 ?x_17)
              (RightOf7 ?x_17)
            )
          )
          (exists (?x_1 )
            (and
              (RightOf11 ?x_1)
              (LeftOf5 ?x_1)
            )
          )
          (exists (?x_3 )
            (and
              (LeftOf3 ?x_3)
              (RightOf10 ?x_3)
            )
          )
          (exists (?x_9 )
            (and
              (LeftOf3 ?x_9)
              (RightOf9 ?x_9)
            )
          )
          (exists (?x_6 )
            (and
              (BelowOf1 ?x_6)
              (AboveOf11 ?x_6)
            )
          )
          (exists (?x_5 )
            (and
              (BelowOf6 ?x_5)
              (AboveOf9 ?x_5)
            )
          )
          (exists (?x_17 )
            (and
              (RightOf9 ?x_17)
              (LeftOf3 ?x_17)
            )
          )
          (exists (?x_18 )
            (and
              (RightOf9 ?x_18)
              (LeftOf2 ?x_18)
            )
          )
          (exists (?x_4 )
            (and
              (BelowOf1 ?x_4)
              (AboveOf2 ?x_4)
            )
          )
          (exists (?x_20 )
            (and
              (LeftOf4 ?x_20)
              (RightOf5 ?x_20)
            )
          )
          (exists (?x_0 )
            (and
              (BelowOf1 ?x_0)
              (AboveOf9 ?x_0)
            )
          )
          (exists (?x_5 )
            (and
              (BelowOf5 ?x_5)
              (AboveOf10 ?x_5)
            )
          )
          (exists (?x_12 )
            (and
              (BelowOf3 ?x_12)
              (AboveOf10 ?x_12)
            )
          )
          (exists (?x_15 )
            (and
              (BelowOf6 ?x_15)
              (AboveOf7 ?x_15)
            )
          )
          (exists (?x_21 )
            (and
              (LeftOf7 ?x_21)
              (RightOf9 ?x_21)
            )
          )
          (exists (?x_5 )
            (and
              (AboveOf6 ?x_5)
              (BelowOf3 ?x_5)
            )
          )
          (exists (?x_3 )
            (and
              (LeftOf6 ?x_3)
              (RightOf10 ?x_3)
            )
          )
          (exists (?x_0 )
            (and
              (BelowOf5 ?x_0)
              (AboveOf10 ?x_0)
            )
          )
          (exists (?x_13 )
            (and
              (AboveOf7 ?x_13)
              (BelowOf2 ?x_13)
            )
          )
          (exists (?x_11 )
            (and
              (AboveOf11 ?x_11)
              (BelowOf3 ?x_11)
            )
          )
          (exists (?x_21 )
            (and
              (LeftOf2 ?x_21)
              (RightOf7 ?x_21)
            )
          )
          (exists (?x_21 )
            (and
              (RightOf10 ?x_21)
              (LeftOf6 ?x_21)
            )
          )
          (exists (?x_4 )
            (and
              (AboveOf9 ?x_4)
              (BelowOf1 ?x_4)
            )
          )
          (exists (?x_18 )
            (and
              (LeftOf1 ?x_18)
              (RightOf7 ?x_18)
            )
          )
          (exists (?x_0 )
            (and
              (BelowOf8 ?x_0)
              (AboveOf9 ?x_0)
            )
          )
          (exists (?x_4 )
            (and
              (BelowOf1 ?x_4)
              (AboveOf6 ?x_4)
            )
          )
          (exists (?x_0 )
            (and
              (BelowOf3 ?x_0)
              (AboveOf9 ?x_0)
            )
          )
          (exists (?x_7 )
            (and
              (BelowOf2 ?x_7)
              (AboveOf5 ?x_7)
            )
          )
          (exists (?x_2 )
            (and
              (LeftOf1 ?x_2)
              (RightOf7 ?x_2)
            )
          )
          (exists (?x_7 )
            (and
              (AboveOf10 ?x_7)
              (BelowOf2 ?x_7)
            )
          )
          (exists (?x_12 )
            (and
              (AboveOf5 ?x_12)
              (BelowOf1 ?x_12)
            )
          )
          (exists (?x_15 )
            (and
              (BelowOf4 ?x_15)
              (AboveOf7 ?x_15)
            )
          )
          (exists (?x_19 )
            (and
              (LeftOf5 ?x_19)
              (RightOf10 ?x_19)
            )
          )
          (exists (?x_21 )
            (and
              (LeftOf4 ?x_21)
              (RightOf8 ?x_21)
            )
          )
          (exists (?x_14 )
            (and
              (RightOf11 ?x_14)
              (LeftOf6 ?x_14)
            )
          )
          (exists (?x_5 )
            (and
              (AboveOf6 ?x_5)
              (BelowOf5 ?x_5)
            )
          )
          (exists (?x_15 )
            (and
              (BelowOf3 ?x_15)
              (AboveOf7 ?x_15)
            )
          )
          (exists (?x_14 )
            (and
              (LeftOf6 ?x_14)
              (RightOf10 ?x_14)
            )
          )
          (exists (?x_9 )
            (and
              (LeftOf4 ?x_9)
              (RightOf10 ?x_9)
            )
          )
          (exists (?x_15 )
            (and
              (AboveOf7 ?x_15)
              (BelowOf5 ?x_15)
            )
          )
          (exists (?x_21 )
            (and
              (LeftOf1 ?x_21)
              (RightOf10 ?x_21)
            )
          )
          (exists (?x_20 )
            (and
              (LeftOf2 ?x_20)
              (RightOf7 ?x_20)
            )
          )
          (exists (?x_14 )
            (and
              (LeftOf3 ?x_14)
              (RightOf10 ?x_14)
            )
          )
          (exists (?x_2 )
            (and
              (LeftOf2 ?x_2)
              (RightOf11 ?x_2)
            )
          )
          (exists (?x_11 )
            (and
              (BelowOf2 ?x_11)
              (AboveOf10 ?x_11)
            )
          )
          (exists (?x_20 )
            (and
              (LeftOf1 ?x_20)
              (RightOf4 ?x_20)
            )
          )
          (exists (?x_6 )
            (and
              (BelowOf1 ?x_6)
              (AboveOf6 ?x_6)
            )
          )
          (exists (?x_11 )
            (and
              (AboveOf11 ?x_11)
              (BelowOf2 ?x_11)
            )
          )
          (exists (?x_20 )
            (and
              (LeftOf2 ?x_20)
              (RightOf10 ?x_20)
            )
          )
          (exists (?x_3 )
            (and
              (LeftOf9 ?x_3)
              (RightOf11 ?x_3)
            )
          )
          (exists (?x_16 )
            (and
              (BelowOf9 ?x_16)
              (AboveOf10 ?x_16)
            )
          )
          (exists (?x_21 )
            (and
              (LeftOf3 ?x_21)
              (RightOf9 ?x_21)
            )
          )
          (exists (?x_6 )
            (and
              (BelowOf2 ?x_6)
              (AboveOf3 ?x_6)
            )
          )
          (exists (?x_12 )
            (and
              (AboveOf6 ?x_12)
              (BelowOf2 ?x_12)
            )
          )
          (exists (?x_18 )
            (and
              (RightOf6 ?x_18)
              (LeftOf2 ?x_18)
            )
          )
          (exists (?x_9 )
            (and
              (LeftOf5 ?x_9)
              (RightOf7 ?x_9)
            )
          )
          (exists (?x_8 )
            (and
              (BelowOf11 ?x_8)
              (AboveOf11 ?x_8)
            )
          )
          (exists (?x_17 )
            (and
              (LeftOf2 ?x_17)
              (RightOf9 ?x_17)
            )
          )
          (exists (?x_11 )
            (and
              (BelowOf3 ?x_11)
              (AboveOf10 ?x_11)
            )
          )
          (exists (?x_15 )
            (and
              (BelowOf1 ?x_15)
              (AboveOf7 ?x_15)
            )
          )
          (exists (?x_19 )
            (and
              (RightOf9 ?x_19)
              (LeftOf7 ?x_19)
            )
          )
          (exists (?x_17 )
            (and
              (LeftOf3 ?x_17)
              (RightOf8 ?x_17)
            )
          )
          (exists (?x_0 )
            (and
              (BelowOf7 ?x_0)
              (AboveOf9 ?x_0)
            )
          )
          (exists (?x_14 )
            (and
              (RightOf11 ?x_14)
              (LeftOf3 ?x_14)
            )
          )
          (exists (?x_19 )
            (and
              (RightOf9 ?x_19)
              (LeftOf6 ?x_19)
            )
          )
          (exists (?x_12 )
            (and
              (AboveOf11 ?x_12)
              (BelowOf3 ?x_12)
            )
          )
          (exists (?x_3 )
            (and
              (LeftOf6 ?x_3)
              (RightOf9 ?x_3)
            )
          )
          (exists (?x_14 )
            (and
              (RightOf11 ?x_14)
              (LeftOf9 ?x_14)
            )
          )
          (exists (?x_14 )
            (and
              (LeftOf1 ?x_14)
              (RightOf10 ?x_14)
            )
          )
          (exists (?x_5 )
            (and
              (BelowOf5 ?x_5)
              (AboveOf9 ?x_5)
            )
          )
          (exists (?x_13 )
            (and
              (BelowOf5 ?x_13)
              (AboveOf8 ?x_13)
            )
          )
          (exists (?x_16 )
            (and
              (BelowOf4 ?x_16)
              (AboveOf10 ?x_16)
            )
          )
          (exists (?x_14 )
            (and
              (LeftOf9 ?x_14)
              (RightOf11 ?x_14)
            )
          )
          (exists (?x_19 )
            (and
              (RightOf9 ?x_19)
              (LeftOf3 ?x_19)
            )
          )
          (exists (?x_19 )
            (and
              (LeftOf1 ?x_19)
              (RightOf8 ?x_19)
            )
          )
          (exists (?x_12 )
            (and
              (AboveOf6 ?x_12)
              (BelowOf3 ?x_12)
            )
          )
          (exists (?x_18 )
            (and
              (LeftOf5 ?x_18)
              (RightOf8 ?x_18)
            )
          )
          (exists (?x_15 )
            (and
              (BelowOf3 ?x_15)
              (AboveOf9 ?x_15)
            )
          )
          (exists (?x_18 )
            (and
              (LeftOf2 ?x_18)
              (RightOf8 ?x_18)
            )
          )
          (exists (?x_3 )
            (and
              (LeftOf4 ?x_3)
              (RightOf11 ?x_3)
            )
          )
          (exists (?x_1 )
            (and
              (RightOf11 ?x_1)
              (LeftOf4 ?x_1)
            )
          )
          (exists (?x_9 )
            (and
              (RightOf10 ?x_9)
              (LeftOf3 ?x_9)
            )
          )
          (exists (?x_20 )
            (and
              (RightOf6 ?x_20)
              (LeftOf2 ?x_20)
            )
          )
          (exists (?x_2 )
            (and
              (LeftOf2 ?x_2)
              (RightOf9 ?x_2)
            )
          )
          (exists (?x_8 )
            (and
              (AboveOf11 ?x_8)
              (BelowOf9 ?x_8)
            )
          )
          (exists (?x_0 )
            (and
              (BelowOf8 ?x_0)
              (AboveOf10 ?x_0)
            )
          )
          (exists (?x_20 )
            (and
              (LeftOf3 ?x_20)
              (RightOf11 ?x_20)
            )
          )
          (exists (?x_9 )
            (and
              (RightOf10 ?x_9)
              (LeftOf2 ?x_9)
            )
          )
          (exists (?x_9 )
            (and
              (LeftOf1 ?x_9)
              (RightOf11 ?x_9)
            )
          )
          (exists (?x_20 )
            (and
              (LeftOf2 ?x_20)
              (RightOf4 ?x_20)
            )
          )
          (exists (?x_18 )
            (and
              (RightOf7 ?x_18)
              (LeftOf3 ?x_18)
            )
          )
          (exists (?x_9 )
            (and
              (RightOf6 ?x_9)
              (LeftOf3 ?x_9)
            )
          )
          (exists (?x_13 )
            (and
              (BelowOf5 ?x_13)
              (AboveOf7 ?x_13)
            )
          )
          (exists (?x_18 )
            (and
              (LeftOf5 ?x_18)
              (RightOf6 ?x_18)
            )
          )
          (exists (?x_14 )
            (and
              (RightOf11 ?x_14)
              (LeftOf2 ?x_14)
            )
          )
          (exists (?x_7 )
            (and
              (BelowOf1 ?x_7)
              (AboveOf9 ?x_7)
            )
          )
          (exists (?x_15 )
            (and
              (AboveOf9 ?x_15)
              (BelowOf5 ?x_15)
            )
          )
          (exists (?x_5 )
            (and
              (BelowOf5 ?x_5)
              (AboveOf7 ?x_5)
            )
          )
          (exists (?x_7 )
            (and
              (BelowOf1 ?x_7)
              (AboveOf6 ?x_7)
            )
          )
          (exists (?x_11 )
            (and
              (BelowOf3 ?x_11)
              (AboveOf11 ?x_11)
            )
          )
          (exists (?x_18 )
            (and
              (LeftOf5 ?x_18)
              (RightOf7 ?x_18)
            )
          )
          (exists (?x_9 )
            (and
              (RightOf8 ?x_9)
              (LeftOf1 ?x_9)
            )
          )
          (exists (?x_7 )
            (and
              (AboveOf10 ?x_7)
              (BelowOf1 ?x_7)
            )
          )
          (exists (?x_15 )
            (and
              (BelowOf7 ?x_15)
              (AboveOf8 ?x_15)
            )
          )
          (exists (?x_18 )
            (and
              (RightOf11 ?x_18)
              (LeftOf3 ?x_18)
            )
          )
          (exists (?x_20 )
            (and
              (LeftOf4 ?x_20)
              (RightOf10 ?x_20)
            )
          )
          (exists (?x_3 )
            (and
              (LeftOf5 ?x_3)
              (RightOf11 ?x_3)
            )
          )
          (exists (?x_9 )
            (and
              (LeftOf3 ?x_9)
              (RightOf7 ?x_9)
            )
          )
          (exists (?x_2 )
            (and
              (LeftOf1 ?x_2)
              (RightOf10 ?x_2)
            )
          )
          (exists (?x_0 )
            (and
              (BelowOf6 ?x_0)
              (AboveOf9 ?x_0)
            )
          )
          (exists (?x_10 )
            (and
              (LeftOf1 ?x_10)
              (RightOf7 ?x_10)
            )
          )
          (exists (?x_0 )
            (and
              (BelowOf4 ?x_0)
              (AboveOf11 ?x_0)
            )
          )
          (exists (?x_20 )
            (and
              (LeftOf2 ?x_20)
              (RightOf9 ?x_20)
            )
          )
          (exists (?x_1 )
            (and
              (RightOf11 ?x_1)
              (LeftOf2 ?x_1)
            )
          )
          (exists (?x_7 )
            (and
              (AboveOf7 ?x_7)
              (BelowOf2 ?x_7)
            )
          )
          (exists (?x_5 )
            (and
              (BelowOf1 ?x_5)
              (AboveOf9 ?x_5)
            )
          )
          (exists (?x_3 )
            (and
              (LeftOf3 ?x_3)
              (RightOf11 ?x_3)
            )
          )
          (exists (?x_10 )
            (and
              (LeftOf1 ?x_10)
              (RightOf2 ?x_10)
            )
          )
          (exists (?x_9 )
            (and
              (RightOf11 ?x_9)
              (LeftOf1 ?x_9)
            )
          )
          (exists (?x_0 )
            (and
              (BelowOf3 ?x_0)
              (AboveOf10 ?x_0)
            )
          )
          (exists (?x_12 )
            (and
              (AboveOf5 ?x_12)
              (BelowOf4 ?x_12)
            )
          )
          (exists (?x_19 )
            (and
              (LeftOf7 ?x_19)
              (RightOf11 ?x_19)
            )
          )
          (exists (?x_2 )
            (and
              (LeftOf2 ?x_2)
              (RightOf10 ?x_2)
            )
          )
          (exists (?x_5 )
            (and
              (BelowOf4 ?x_5)
              (AboveOf9 ?x_5)
            )
          )
          (exists (?x_13 )
            (and
              (BelowOf2 ?x_13)
              (AboveOf10 ?x_13)
            )
          )
          (exists (?x_15 )
            (and
              (AboveOf10 ?x_15)
              (BelowOf6 ?x_15)
            )
          )
          (exists (?x_15 )
            (and
              (AboveOf9 ?x_15)
              (BelowOf7 ?x_15)
            )
          )
          (exists (?x_14 )
            (and
              (LeftOf4 ?x_14)
              (RightOf11 ?x_14)
            )
          )
          (exists (?x_3 )
            (and
              (LeftOf4 ?x_3)
              (RightOf9 ?x_3)
            )
          )
          (exists (?x_15 )
            (and
              (AboveOf7 ?x_15)
              (BelowOf7 ?x_15)
            )
          )
          (exists (?x_13 )
            (and
              (AboveOf9 ?x_13)
              (BelowOf4 ?x_13)
            )
          )
          (exists (?x_3 )
            (and
              (RightOf11 ?x_3)
              (LeftOf2 ?x_3)
            )
          )
          (exists (?x_12 )
            (and
              (BelowOf3 ?x_12)
              (AboveOf4 ?x_12)
            )
          )
          (exists (?x_18 )
            (and
              (LeftOf3 ?x_18)
              (RightOf7 ?x_18)
            )
          )
          (exists (?x_15 )
            (and
              (AboveOf10 ?x_15)
              (BelowOf4 ?x_15)
            )
          )
          (exists (?x_8 )
            (and
              (BelowOf5 ?x_8)
              (AboveOf11 ?x_8)
            )
          )
          (exists (?x_2 )
            (and
              (RightOf7 ?x_2)
              (LeftOf2 ?x_2)
            )
          )
          (exists (?x_2 )
            (and
              (RightOf5 ?x_2)
              (LeftOf2 ?x_2)
            )
          )
          (exists (?x_19 )
            (and
              (RightOf10 ?x_19)
              (LeftOf7 ?x_19)
            )
          )
          (exists (?x_7 )
            (and
              (AboveOf4 ?x_7)
              (BelowOf3 ?x_7)
            )
          )
          (exists (?x_16 )
            (and
              (BelowOf1 ?x_16)
              (AboveOf10 ?x_16)
            )
          )
          (exists (?x_5 )
            (and
              (AboveOf6 ?x_5)
              (BelowOf4 ?x_5)
            )
          )
          (exists (?x_2 )
            (and
              (RightOf6 ?x_2)
              (LeftOf2 ?x_2)
            )
          )
          (exists (?x_17 )
            (and
              (LeftOf3 ?x_17)
              (RightOf11 ?x_17)
            )
          )
          (exists (?x_11 )
            (and
              (BelowOf1 ?x_11)
              (AboveOf10 ?x_11)
            )
          )
          (exists (?x_21 )
            (and
              (LeftOf7 ?x_21)
              (RightOf7 ?x_21)
            )
          )
          (exists (?x_13 )
            (and
              (AboveOf11 ?x_13)
              (BelowOf4 ?x_13)
            )
          )
          (exists (?x_3 )
            (and
              (LeftOf1 ?x_3)
              (RightOf9 ?x_3)
            )
          )
          (exists (?x_11 )
            (and
              (BelowOf5 ?x_11)
              (AboveOf9 ?x_11)
            )
          )
          (exists (?x_3 )
            (and
              (RightOf9 ?x_3)
              (LeftOf2 ?x_3)
            )
          )
          (exists (?x_12 )
            (and
              (AboveOf7 ?x_12)
              (BelowOf1 ?x_12)
            )
          )
          (exists (?x_17 )
            (and
              (LeftOf2 ?x_17)
              (RightOf6 ?x_17)
            )
          )
          (exists (?x_15 )
            (and
              (BelowOf2 ?x_15)
              (AboveOf8 ?x_15)
            )
          )
          (exists (?x_18 )
            (and
              (RightOf11 ?x_18)
              (LeftOf1 ?x_18)
            )
          )
          (exists (?x_6 )
            (and
              (BelowOf1 ?x_6)
              (AboveOf5 ?x_6)
            )
          )
          (exists (?x_20 )
            (and
              (RightOf10 ?x_20)
              (LeftOf3 ?x_20)
            )
          )
          (exists (?x_12 )
            (and
              (BelowOf2 ?x_12)
              (AboveOf9 ?x_12)
            )
          )
          (exists (?x_15 )
            (and
              (AboveOf10 ?x_15)
              (BelowOf3 ?x_15)
            )
          )
          (exists (?x_10 )
            (and
              (RightOf6 ?x_10)
              (LeftOf1 ?x_10)
            )
          )
          (exists (?x_13 )
            (and
              (AboveOf5 ?x_13)
              (BelowOf5 ?x_13)
            )
          )
          (exists (?x_18 )
            (and
              (LeftOf1 ?x_18)
              (RightOf5 ?x_18)
            )
          )
          (exists (?x_16 )
            (and
              (BelowOf10 ?x_16)
              (AboveOf10 ?x_16)
            )
          )
          (exists (?x_9 )
            (and
              (RightOf6 ?x_9)
              (LeftOf5 ?x_9)
            )
          )
          (exists (?x_6 )
            (and
              (BelowOf2 ?x_6)
              (AboveOf4 ?x_6)
            )
          )
          (exists (?x_7 )
            (and
              (BelowOf2 ?x_7)
              (AboveOf3 ?x_7)
            )
          )
          (exists (?x_16 )
            (and
              (BelowOf3 ?x_16)
              (AboveOf10 ?x_16)
            )
          )
          (exists (?x_17 )
            (and
              (LeftOf2 ?x_17)
              (RightOf8 ?x_17)
            )
          )
          (exists (?x_1 )
            (and
              (RightOf11 ?x_1)
              (LeftOf3 ?x_1)
            )
          )
          (exists (?x_11 )
            (and
              (AboveOf11 ?x_11)
              (BelowOf7 ?x_11)
            )
          )
          (exists (?x_2 )
            (and
              (RightOf8 ?x_2)
              (LeftOf2 ?x_2)
            )
          )
          (exists (?x_6 )
            (and
              (BelowOf1 ?x_6)
              (AboveOf9 ?x_6)
            )
          )
          (exists (?x_19 )
            (and
              (LeftOf5 ?x_19)
              (RightOf11 ?x_19)
            )
          )
          (exists (?x_17 )
            (and
              (LeftOf2 ?x_17)
              (RightOf5 ?x_17)
            )
          )
          (exists (?x_9 )
            (and
              (LeftOf4 ?x_9)
              (RightOf7 ?x_9)
            )
          )
          (exists (?x_6 )
            (and
              (BelowOf1 ?x_6)
              (AboveOf10 ?x_6)
            )
          )
          (exists (?x_21 )
            (and
              (LeftOf1 ?x_21)
              (RightOf11 ?x_21)
            )
          )
          (exists (?x_19 )
            (and
              (RightOf10 ?x_19)
              (LeftOf2 ?x_19)
            )
          )
          (exists (?x_14 )
            (and
              (RightOf11 ?x_14)
              (LeftOf1 ?x_14)
            )
          )
          (exists (?x_17 )
            (and
              (RightOf10 ?x_17)
              (LeftOf1 ?x_17)
            )
          )
          (exists (?x_21 )
            (and
              (RightOf8 ?x_21)
              (LeftOf3 ?x_21)
            )
          )
          (exists (?x_14 )
            (and
              (RightOf10 ?x_14)
              (LeftOf10 ?x_14)
            )
          )
          (exists (?x_11 )
            (and
              (BelowOf4 ?x_11)
              (AboveOf10 ?x_11)
            )
          )
          (exists (?x_9 )
            (and
              (LeftOf2 ?x_9)
              (RightOf8 ?x_9)
            )
          )
          (exists (?x_20 )
            (and
              (RightOf7 ?x_20)
              (LeftOf3 ?x_20)
            )
          )
          (exists (?x_0 )
            (and
              (BelowOf3 ?x_0)
              (AboveOf11 ?x_0)
            )
          )
          (exists (?x_3 )
            (and
              (RightOf9 ?x_3)
              (LeftOf5 ?x_3)
            )
          )
          (exists (?x_20 )
            (and
              (LeftOf3 ?x_20)
              (RightOf7 ?x_20)
            )
          )
          (exists (?x_7 )
            (and
              (AboveOf7 ?x_7)
              (BelowOf1 ?x_7)
            )
          )
          (exists (?x_7 )
            (and
              (BelowOf2 ?x_7)
              (AboveOf6 ?x_7)
            )
          )
          (exists (?x_17 )
            (and
              (LeftOf1 ?x_17)
              (RightOf8 ?x_17)
            )
          )
          (exists (?x_12 )
            (and
              (AboveOf8 ?x_12)
              (BelowOf4 ?x_12)
            )
          )
          (exists (?x_19 )
            (and
              (LeftOf4 ?x_19)
              (RightOf8 ?x_19)
            )
          )
          (exists (?x_21 )
            (and
              (LeftOf3 ?x_21)
              (RightOf11 ?x_21)
            )
          )
          (exists (?x_15 )
            (and
              (BelowOf6 ?x_15)
              (AboveOf8 ?x_15)
            )
          )
          (exists (?x_20 )
            (and
              (RightOf6 ?x_20)
              (LeftOf4 ?x_20)
            )
          )
          (exists (?x_5 )
            (and
              (BelowOf6 ?x_5)
              (AboveOf8 ?x_5)
            )
          )
          (exists (?x_7 )
            (and
              (BelowOf1 ?x_7)
              (AboveOf5 ?x_7)
            )
          )
          (exists (?x_0 )
            (and
              (BelowOf6 ?x_0)
              (AboveOf8 ?x_0)
            )
          )
          (exists (?x_7 )
            (and
              (AboveOf7 ?x_7)
              (BelowOf3 ?x_7)
            )
          )
          (exists (?x_9 )
            (and
              (RightOf7 ?x_9)
              (LeftOf6 ?x_9)
            )
          )
          (exists (?x_14 )
            (and
              (LeftOf4 ?x_14)
              (RightOf10 ?x_14)
            )
          )
          (exists (?x_9 )
            (and
              (LeftOf2 ?x_9)
              (RightOf9 ?x_9)
            )
          )
          (exists (?x_6 )
            (and
              (BelowOf2 ?x_6)
              (AboveOf11 ?x_6)
            )
          )
          (exists (?x_21 )
            (and
              (RightOf10 ?x_21)
              (LeftOf5 ?x_21)
            )
          )
          (exists (?x_17 )
            (and
              (RightOf7 ?x_17)
              (LeftOf1 ?x_17)
            )
          )
          (exists (?x_21 )
            (and
              (LeftOf6 ?x_21)
              (RightOf9 ?x_21)
            )
          )
          (exists (?x_17 )
            (and
              (RightOf5 ?x_17)
              (LeftOf1 ?x_17)
            )
          )
          (exists (?x_20 )
            (and
              (LeftOf2 ?x_20)
              (RightOf8 ?x_20)
            )
          )
          (exists (?x_11 )
            (and
              (BelowOf8 ?x_11)
              (AboveOf9 ?x_11)
            )
          )
          (exists (?x_12 )
            (and
              (AboveOf9 ?x_12)
              (BelowOf1 ?x_12)
            )
          )
          (exists (?x_3 )
            (and
              (LeftOf8 ?x_3)
              (RightOf9 ?x_3)
            )
          )
          (exists (?x_13 )
            (and
              (BelowOf3 ?x_13)
              (AboveOf7 ?x_13)
            )
          )
          (exists (?x_15 )
            (and
              (BelowOf1 ?x_15)
              (AboveOf8 ?x_15)
            )
          )
          (exists (?x_5 )
            (and
              (AboveOf6 ?x_5)
              (BelowOf6 ?x_5)
            )
          )
          (exists (?x_18 )
            (and
              (RightOf10 ?x_18)
              (LeftOf4 ?x_18)
            )
          )
          (exists (?x_11 )
            (and
              (BelowOf6 ?x_11)
              (AboveOf9 ?x_11)
            )
          )
          (exists (?x_20 )
            (and
              (LeftOf3 ?x_20)
              (RightOf4 ?x_20)
            )
          )
          (exists (?x_0 )
            (and
              (BelowOf4 ?x_0)
              (AboveOf10 ?x_0)
            )
          )
          (exists (?x_19 )
            (and
              (RightOf10 ?x_19)
              (LeftOf3 ?x_19)
            )
          )
          (exists (?x_3 )
            (and
              (LeftOf7 ?x_3)
              (RightOf10 ?x_3)
            )
          )
          (exists (?x_14 )
            (and
              (RightOf10 ?x_14)
              (LeftOf2 ?x_14)
            )
          )
          (exists (?x_17 )
            (and
              (LeftOf3 ?x_17)
              (RightOf7 ?x_17)
            )
          )
          (exists (?x_18 )
            (and
              (LeftOf5 ?x_18)
              (RightOf9 ?x_18)
            )
          )
          (exists (?x_13 )
            (and
              (AboveOf10 ?x_13)
              (BelowOf1 ?x_13)
            )
          )
          (exists (?x_17 )
            (and
              (RightOf6 ?x_17)
              (LeftOf2 ?x_17)
            )
          )
          (exists (?x_16 )
            (and
              (BelowOf2 ?x_16)
              (AboveOf10 ?x_16)
            )
          )
          (exists (?x_9 )
            (and
              (LeftOf5 ?x_9)
              (RightOf9 ?x_9)
            )
          )
          (exists (?x_13 )
            (and
              (BelowOf5 ?x_13)
              (AboveOf10 ?x_13)
            )
          )
          (exists (?x_8 )
            (and
              (AboveOf11 ?x_8)
              (BelowOf7 ?x_8)
            )
          )
          (exists (?x_9 )
            (and
              (RightOf7 ?x_9)
              (LeftOf3 ?x_9)
            )
          )
          (exists (?x_9 )
            (and
              (RightOf10 ?x_9)
              (LeftOf5 ?x_9)
            )
          )
          (exists (?x_11 )
            (and
              (BelowOf5 ?x_11)
              (AboveOf10 ?x_11)
            )
          )
          (exists (?x_16 )
            (and
              (BelowOf8 ?x_16)
              (AboveOf11 ?x_16)
            )
          )
          (exists (?x_17 )
            (and
              (RightOf6 ?x_17)
              (LeftOf3 ?x_17)
            )
          )
          (exists (?x_16 )
            (and
              (AboveOf11 ?x_16)
              (BelowOf2 ?x_16)
            )
          )
          (exists (?x_12 )
            (and
              (AboveOf11 ?x_12)
              (BelowOf4 ?x_12)
            )
          )
          (exists (?x_17 )
            (and
              (RightOf4 ?x_17)
              (LeftOf1 ?x_17)
            )
          )
          (exists (?x_15 )
            (and
              (BelowOf4 ?x_15)
              (AboveOf8 ?x_15)
            )
          )
          (exists (?x_19 )
            (and
              (LeftOf8 ?x_19)
              (RightOf8 ?x_19)
            )
          )
          (exists (?x_7 )
            (and
              (AboveOf11 ?x_7)
              (BelowOf1 ?x_7)
            )
          )
          (exists (?x_15 )
            (and
              (BelowOf6 ?x_15)
              (AboveOf10 ?x_15)
            )
          )
          (exists (?x_3 )
            (and
              (RightOf10 ?x_3)
              (LeftOf5 ?x_3)
            )
          )
          (exists (?x_0 )
            (and
              (BelowOf6 ?x_0)
              (AboveOf11 ?x_0)
            )
          )
          (exists (?x_9 )
            (and
              (LeftOf6 ?x_9)
              (RightOf9 ?x_9)
            )
          )
          (exists (?x_17 )
            (and
              (RightOf3 ?x_17)
              (LeftOf3 ?x_17)
            )
          )
          (exists (?x_16 )
            (and
              (AboveOf10 ?x_16)
              (BelowOf6 ?x_16)
            )
          )
          (exists (?x_12 )
            (and
              (BelowOf2 ?x_12)
              (AboveOf4 ?x_12)
            )
          )
          (exists (?x_11 )
            (and
              (AboveOf9 ?x_11)
              (BelowOf7 ?x_11)
            )
          )
          (exists (?x_9 )
            (and
              (RightOf8 ?x_9)
              (LeftOf3 ?x_9)
            )
          )
          (exists (?x_11 )
            (and
              (BelowOf8 ?x_11)
              (AboveOf10 ?x_11)
            )
          )
          (exists (?x_15 )
            (and
              (AboveOf9 ?x_15)
              (BelowOf2 ?x_15)
            )
          )
          (exists (?x_16 )
            (and
              (AboveOf11 ?x_16)
              (BelowOf3 ?x_16)
            )
          )
          (exists (?x_21 )
            (and
              (LeftOf5 ?x_21)
              (RightOf9 ?x_21)
            )
          )
          (exists (?x_6 )
            (and
              (BelowOf1 ?x_6)
              (AboveOf7 ?x_6)
            )
          )
          (exists (?x_10 )
            (and
              (LeftOf1 ?x_10)
              (RightOf5 ?x_10)
            )
          )
          (exists (?x_20 )
            (and
              (LeftOf4 ?x_20)
              (RightOf7 ?x_20)
            )
          )
          (exists (?x_12 )
            (and
              (AboveOf10 ?x_12)
              (BelowOf1 ?x_12)
            )
          )
          (exists (?x_15 )
            (and
              (BelowOf4 ?x_15)
              (AboveOf11 ?x_15)
            )
          )
          (exists (?x_3 )
            (and
              (LeftOf8 ?x_3)
              (RightOf11 ?x_3)
            )
          )
          (exists (?x_21 )
            (and
              (LeftOf6 ?x_21)
              (RightOf11 ?x_21)
            )
          )
          (exists (?x_11 )
            (and
              (BelowOf5 ?x_11)
              (AboveOf11 ?x_11)
            )
          )
          (exists (?x_13 )
            (and
              (BelowOf5 ?x_13)
              (AboveOf9 ?x_13)
            )
          )
          (exists (?x_15 )
            (and
              (BelowOf1 ?x_15)
              (AboveOf9 ?x_15)
            )
          )
          (exists (?x_21 )
            (and
              (LeftOf4 ?x_21)
              (RightOf10 ?x_21)
            )
          )
          (exists (?x_2 )
            (and
              (RightOf2 ?x_2)
              (LeftOf2 ?x_2)
            )
          )
          (exists (?x_2 )
            (and
              (LeftOf1 ?x_2)
              (RightOf3 ?x_2)
            )
          )
          (exists (?x_20 )
            (and
              (LeftOf4 ?x_20)
              (RightOf11 ?x_20)
            )
          )
          (exists (?x_8 )
            (and
              (AboveOf11 ?x_8)
              (BelowOf2 ?x_8)
            )
          )
          (exists (?x_12 )
            (and
              (AboveOf6 ?x_12)
              (BelowOf1 ?x_12)
            )
          )
          (exists (?x_21 )
            (and
              (LeftOf4 ?x_21)
              (RightOf9 ?x_21)
            )
          )
          (exists (?x_7 )
            (and
              (AboveOf10 ?x_7)
              (BelowOf3 ?x_7)
            )
          )
          (exists (?x_9 )
            (and
              (LeftOf5 ?x_9)
              (RightOf8 ?x_9)
            )
          )
          (exists (?x_18 )
            (and
              (LeftOf2 ?x_18)
              (RightOf6 ?x_18)
            )
          )
          (exists (?x_18 )
            (and
              (RightOf10 ?x_18)
              (LeftOf5 ?x_18)
            )
          )
          (exists (?x_0 )
            (and
              (AboveOf11 ?x_0)
              (BelowOf8 ?x_0)
            )
          )
          (exists (?x_19 )
            (and
              (LeftOf5 ?x_19)
              (RightOf9 ?x_19)
            )
          )
          (exists (?x_10 )
            (and
              (LeftOf1 ?x_10)
              (RightOf1 ?x_10)
            )
          )
          (exists (?x_11 )
            (and
              (BelowOf3 ?x_11)
              (AboveOf9 ?x_11)
            )
          )
          (exists (?x_21 )
            (and
              (LeftOf7 ?x_21)
              (RightOf11 ?x_21)
            )
          )
          (exists (?x_12 )
            (and
              (BelowOf1 ?x_12)
              (AboveOf9 ?x_12)
            )
          )
          (exists (?x_14 )
            (and
              (RightOf11 ?x_14)
              (LeftOf7 ?x_14)
            )
          )
          (exists (?x_6 )
            (and
              (BelowOf1 ?x_6)
              (AboveOf2 ?x_6)
            )
          )
          (exists (?x_17 )
            (and
              (LeftOf2 ?x_17)
              (RightOf10 ?x_17)
            )
          )
          (exists (?x_2 )
            (and
              (RightOf6 ?x_2)
              (LeftOf1 ?x_2)
            )
          )
          (exists (?x_7 )
            (and
              (AboveOf8 ?x_7)
              (BelowOf1 ?x_7)
            )
          )
          (exists (?x_8 )
            (and
              (AboveOf11 ?x_8)
              (BelowOf3 ?x_8)
            )
          )
          (exists (?x_5 )
            (and
              (BelowOf5 ?x_5)
              (AboveOf11 ?x_5)
            )
          )
          (exists (?x_14 )
            (and
              (RightOf11 ?x_14)
              (LeftOf8 ?x_14)
            )
          )
          (exists (?x_3 )
            (and
              (LeftOf4 ?x_3)
              (RightOf10 ?x_3)
            )
          )
          (exists (?x_0 )
            (and
              (BelowOf4 ?x_0)
              (AboveOf8 ?x_0)
            )
          )
          (exists (?x_7 )
            (and
              (AboveOf11 ?x_7)
              (BelowOf3 ?x_7)
            )
          )
          (exists (?x_16 )
            (and
              (AboveOf11 ?x_16)
              (BelowOf9 ?x_16)
            )
          )
          (exists (?x_0 )
            (and
              (AboveOf11 ?x_0)
              (BelowOf7 ?x_0)
            )
          )
          (exists (?x_5 )
            (and
              (BelowOf1 ?x_5)
              (AboveOf8 ?x_5)
            )
          )
          (exists (?x_20 )
            (and
              (LeftOf4 ?x_20)
              (RightOf8 ?x_20)
            )
          )
          (exists (?x_20 )
            (and
              (LeftOf1 ?x_20)
              (RightOf7 ?x_20)
            )
          )
          (exists (?x_9 )
            (and
              (RightOf8 ?x_9)
              (LeftOf6 ?x_9)
            )
          )
          (exists (?x_1 )
            (and
              (RightOf11 ?x_1)
              (LeftOf9 ?x_1)
            )
          )
          (exists (?x_14 )
            (and
              (RightOf11 ?x_14)
              (LeftOf5 ?x_14)
            )
          )
          (exists (?x_5 )
            (and
              (BelowOf4 ?x_5)
              (AboveOf11 ?x_5)
            )
          )
          (exists (?x_5 )
            (and
              (AboveOf10 ?x_5)
              (BelowOf2 ?x_5)
            )
          )
          (exists (?x_20 )
            (and
              (RightOf10 ?x_20)
              (LeftOf1 ?x_20)
            )
          )
          (exists (?x_12 )
            (and
              (AboveOf8 ?x_12)
              (BelowOf1 ?x_12)
            )
          )
          (exists (?x_0 )
            (and
              (AboveOf10 ?x_0)
              (BelowOf2 ?x_0)
            )
          )
          (exists (?x_3 )
            (and
              (LeftOf7 ?x_3)
              (RightOf9 ?x_3)
            )
          )
          (exists (?x_16 )
            (and
              (BelowOf5 ?x_16)
              (AboveOf11 ?x_16)
            )
          )
          (exists (?x_3 )
            (and
              (RightOf11 ?x_3)
              (LeftOf5 ?x_3)
            )
          )
          (exists (?x_3 )
            (and
              (LeftOf6 ?x_3)
              (RightOf11 ?x_3)
            )
          )
          (exists (?x_8 )
            (and
              (BelowOf8 ?x_8)
              (AboveOf11 ?x_8)
            )
          )
          (exists (?x_13 )
            (and
              (BelowOf3 ?x_13)
              (AboveOf8 ?x_13)
            )
          )
          (exists (?x_9 )
            (and
              (RightOf10 ?x_9)
              (LeftOf4 ?x_9)
            )
          )
          (exists (?x_16 )
            (and
              (AboveOf11 ?x_16)
              (BelowOf10 ?x_16)
            )
          )
          (exists (?x_19 )
            (and
              (RightOf10 ?x_19)
              (LeftOf4 ?x_19)
            )
          )
          (exists (?x_5 )
            (and
              (BelowOf2 ?x_5)
              (AboveOf7 ?x_5)
            )
          )
          (exists (?x_6 )
            (and
              (BelowOf1 ?x_6)
              (AboveOf4 ?x_6)
            )
          )
          (exists (?x_17 )
            (and
              (LeftOf3 ?x_17)
              (RightOf5 ?x_17)
            )
          )
          (exists (?x_4 )
            (and
              (AboveOf11 ?x_4)
              (BelowOf1 ?x_4)
            )
          )
          (exists (?x_13 )
            (and
              (AboveOf7 ?x_13)
              (BelowOf1 ?x_13)
            )
          )
        )
        (Error)
      )
    )
  )
)