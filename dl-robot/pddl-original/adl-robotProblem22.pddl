(define (problem robotProblem )
  (:domain robot)
  (:objects
    robot 
  )
  (:init
    (AboveOf0 robot)
    (RightOf2 robot)
    (BelowOf21 robot)
    (LeftOf21 robot)
  )
  (:goal
    (and
      (not (CheckConsistency))
      (not (Error))
      (Row1 robot)
      (Column2 robot)
    )
  )
)