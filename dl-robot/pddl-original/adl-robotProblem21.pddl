(define (problem robotProblem )
  (:domain robot)
  (:objects
    robot 
  )
  (:init
    (BelowOf20 robot)
    (AboveOf0 robot)
    (RightOf2 robot)
    (LeftOf20 robot)
  )
  (:goal
    (and
      (not (CheckConsistency))
      (not (Error))
      (Row1 robot)
      (Column2 robot)
    )
  )
)