(define
  (domain robot)
  (:requirements :adl)
  (:predicates
    (LeftOf3 ?x )
    (LeftOf2 ?x )
    (LeftOf1 ?x )
    (Rows ?x )
    (LeftOf4 ?x )
    (RightOf3 ?x )
    (RightOf2 ?x )
    (RightOf1 ?x )
    (RightOf0 ?x )
    (Columns ?x )
    (Row0 ?x )
    (Row1 ?x )
    (Row2 ?x )
    (Row3 ?x )
    (BelowOf1 ?x )
    (BelowOf2 ?x )
    (BelowOf3 ?x )
    (BelowOf4 ?x )
    (Column1 ?x )
    (Column0 ?x )
    (Column3 ?x )
    (Column2 ?x )
    (AboveOf0 ?x )
    (AboveOf1 ?x )
    (AboveOf2 ?x )
    (AboveOf3 ?x )
    (CheckConsistency)
    (Error)
  )
  (:action moveUp
    :parameters ( ?x )
    :precondition ( and
      (not (CheckConsistency))
      (not (Error))
      (or
        (BelowOf3 ?x)
        (AboveOf3 ?x)
        (Rows ?x)
        (BelowOf1 ?x)
        (BelowOf2 ?x)
        (BelowOf4 ?x)
        (AboveOf0 ?x)
        (AboveOf2 ?x)
        (AboveOf1 ?x)
      )
    )
    :effect ( and
      (CheckConsistency)
      (when
        (or
          (AboveOf1 ?x)
          (AboveOf3 ?x)
          (AboveOf0 ?x)
          (AboveOf2 ?x)
        )
        (and
          (AboveOf1 ?x)
        )
      )
      (when
        (or
          (AboveOf1 ?x)
          (AboveOf3 ?x)
          (AboveOf2 ?x)
        )
        (and
          (AboveOf2 ?x)
        )
      )
      (when
        (or
          (AboveOf3 ?x)
          (AboveOf2 ?x)
        )
        (and
          (AboveOf3 ?x)
        )
      )
      (when
        (BelowOf1 ?x)
        (and
          (BelowOf2 ?x)
          (not (BelowOf1 ?x))
        )
      )
      (when
        (or
          (BelowOf1 ?x)
          (BelowOf2 ?x)
        )
        (and
          (BelowOf3 ?x)
          (not (BelowOf2 ?x))
        )
      )
      (when
        (or
          (BelowOf3 ?x)
          (BelowOf1 ?x)
          (BelowOf2 ?x)
        )
        (and
          (BelowOf4 ?x)
          (not (BelowOf3 ?x))
        )
      )
      (when
        (Row0 ?x)
        (and
          (Row1 ?x)
          (not (Row0 ?x))
        )
      )
      (when
        (Row1 ?x)
        (and
          (Row2 ?x)
          (not (Row1 ?x))
        )
      )
      (when
        (Row2 ?x)
        (and
          (Row3 ?x)
          (not (Row2 ?x))
        )
      )
      (when
        (or
          (and
            (BelowOf1 ?x)
            (AboveOf3 ?x)
          )
          (and
            (AboveOf1 ?x)
            (BelowOf1 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf0 ?x)
          )
          (and
            (AboveOf2 ?x)
            (BelowOf1 ?x)
          )
        )
        (and
          (Row1 ?x)
        )
      )
      (when
        (or
          (and
            (BelowOf2 ?x)
            (AboveOf1 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf3 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf3 ?x)
          )
          (and
            (AboveOf1 ?x)
            (BelowOf1 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf2 ?x)
          )
          (and
            (AboveOf2 ?x)
            (BelowOf1 ?x)
          )
        )
        (and
          (Row2 ?x)
        )
      )
      (when
        (or
          (and
            (BelowOf2 ?x)
            (AboveOf3 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf3 ?x)
          )
          (and
            (AboveOf2 ?x)
            (BelowOf2 ?x)
          )
          (and
            (BelowOf3 ?x)
            (AboveOf3 ?x)
          )
          (and
            (AboveOf2 ?x)
            (BelowOf3 ?x)
          )
          (and
            (AboveOf2 ?x)
            (BelowOf1 ?x)
          )
        )
        (and
          (Row3 ?x)
        )
      )
    )
  )
  (:action moveRight
    :parameters ( ?x )
    :precondition ( and
      (not (CheckConsistency))
      (not (Error))
      (or
        (LeftOf4 ?x)
        (LeftOf3 ?x)
        (RightOf1 ?x)
        (RightOf0 ?x)
        (LeftOf2 ?x)
        (Columns ?x)
        (RightOf2 ?x)
        (LeftOf1 ?x)
        (RightOf3 ?x)
      )
    )
    :effect ( and
      (CheckConsistency)
      (when
        (or
          (RightOf2 ?x)
          (RightOf1 ?x)
          (RightOf3 ?x)
          (RightOf0 ?x)
        )
        (and
          (RightOf1 ?x)
        )
      )
      (when
        (or
          (RightOf2 ?x)
          (RightOf1 ?x)
          (RightOf3 ?x)
        )
        (and
          (RightOf2 ?x)
        )
      )
      (when
        (or
          (RightOf2 ?x)
          (RightOf3 ?x)
        )
        (and
          (RightOf3 ?x)
        )
      )
      (when
        (LeftOf1 ?x)
        (and
          (LeftOf2 ?x)
          (not (LeftOf1 ?x))
        )
      )
      (when
        (or
          (LeftOf2 ?x)
          (LeftOf1 ?x)
        )
        (and
          (LeftOf3 ?x)
          (not (LeftOf2 ?x))
        )
      )
      (when
        (or
          (LeftOf2 ?x)
          (LeftOf3 ?x)
          (LeftOf1 ?x)
        )
        (and
          (LeftOf4 ?x)
          (not (LeftOf3 ?x))
        )
      )
      (when
        (Column0 ?x)
        (and
          (Column1 ?x)
          (not (Column0 ?x))
        )
      )
      (when
        (Column1 ?x)
        (and
          (Column2 ?x)
          (not (Column1 ?x))
        )
      )
      (when
        (Column2 ?x)
        (and
          (Column3 ?x)
          (not (Column2 ?x))
        )
      )
      (when
        (or
          (and
            (LeftOf1 ?x)
            (RightOf3 ?x)
          )
          (and
            (RightOf2 ?x)
            (LeftOf1 ?x)
          )
          (and
            (RightOf1 ?x)
            (LeftOf1 ?x)
          )
          (and
            (LeftOf1 ?x)
            (RightOf0 ?x)
          )
        )
        (and
          (Column1 ?x)
        )
      )
      (when
        (or
          (and
            (LeftOf2 ?x)
            (RightOf3 ?x)
          )
          (and
            (RightOf2 ?x)
            (LeftOf1 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf2 ?x)
          )
          (and
            (LeftOf1 ?x)
            (RightOf3 ?x)
          )
          (and
            (RightOf1 ?x)
            (LeftOf1 ?x)
          )
          (and
            (RightOf1 ?x)
            (LeftOf2 ?x)
          )
        )
        (and
          (Column2 ?x)
        )
      )
      (when
        (or
          (and
            (RightOf2 ?x)
            (LeftOf3 ?x)
          )
          (and
            (LeftOf3 ?x)
            (RightOf3 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf3 ?x)
          )
          (and
            (RightOf2 ?x)
            (LeftOf1 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf2 ?x)
          )
          (and
            (LeftOf1 ?x)
            (RightOf3 ?x)
          )
        )
        (and
          (Column3 ?x)
        )
      )
    )
  )
  (:action moveDown
    :parameters ( ?x )
    :precondition ( and
      (not (CheckConsistency))
      (not (Error))
      (or
        (BelowOf3 ?x)
        (AboveOf3 ?x)
        (Rows ?x)
        (BelowOf1 ?x)
        (BelowOf2 ?x)
        (BelowOf4 ?x)
        (AboveOf0 ?x)
        (AboveOf2 ?x)
        (AboveOf1 ?x)
      )
    )
    :effect ( and
      (CheckConsistency)
      (when
        (or
          (BelowOf1 ?x)
          (BelowOf2 ?x)
        )
        (and
          (BelowOf1 ?x)
        )
      )
      (when
        (or
          (BelowOf3 ?x)
          (BelowOf1 ?x)
          (BelowOf2 ?x)
        )
        (and
          (BelowOf2 ?x)
        )
      )
      (when
        (or
          (BelowOf4 ?x)
          (BelowOf3 ?x)
          (BelowOf1 ?x)
          (BelowOf2 ?x)
        )
        (and
          (BelowOf3 ?x)
        )
      )
      (when
        (or
          (AboveOf1 ?x)
          (AboveOf3 ?x)
          (AboveOf2 ?x)
        )
        (and
          (AboveOf0 ?x)
          (not (AboveOf1 ?x))
        )
      )
      (when
        (or
          (AboveOf3 ?x)
          (AboveOf2 ?x)
        )
        (and
          (AboveOf1 ?x)
          (not (AboveOf2 ?x))
        )
      )
      (when
        (AboveOf3 ?x)
        (and
          (AboveOf2 ?x)
          (not (AboveOf3 ?x))
        )
      )
      (when
        (Row1 ?x)
        (and
          (Row0 ?x)
          (not (Row1 ?x))
        )
      )
      (when
        (Row2 ?x)
        (and
          (Row1 ?x)
          (not (Row2 ?x))
        )
      )
      (when
        (Row3 ?x)
        (and
          (Row2 ?x)
          (not (Row3 ?x))
        )
      )
      (when
        (or
          (and
            (BelowOf2 ?x)
            (AboveOf1 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf3 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf3 ?x)
          )
          (and
            (AboveOf1 ?x)
            (BelowOf1 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf2 ?x)
          )
          (and
            (AboveOf2 ?x)
            (BelowOf1 ?x)
          )
        )
        (and
          (Row0 ?x)
        )
      )
      (when
        (or
          (and
            (BelowOf2 ?x)
            (AboveOf3 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf3 ?x)
          )
          (and
            (AboveOf2 ?x)
            (BelowOf2 ?x)
          )
          (and
            (BelowOf3 ?x)
            (AboveOf3 ?x)
          )
          (and
            (AboveOf2 ?x)
            (BelowOf3 ?x)
          )
          (and
            (AboveOf2 ?x)
            (BelowOf1 ?x)
          )
        )
        (and
          (Row1 ?x)
        )
      )
      (when
        (or
          (and
            (BelowOf1 ?x)
            (AboveOf3 ?x)
          )
          (and
            (AboveOf3 ?x)
            (BelowOf3 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf3 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf3 ?x)
          )
        )
        (and
          (Row2 ?x)
        )
      )
    )
  )
  (:action moveLeft
    :parameters ( ?x )
    :precondition ( and
      (not (CheckConsistency))
      (not (Error))
      (or
        (LeftOf4 ?x)
        (LeftOf3 ?x)
        (RightOf1 ?x)
        (RightOf0 ?x)
        (LeftOf2 ?x)
        (Columns ?x)
        (RightOf2 ?x)
        (LeftOf1 ?x)
        (RightOf3 ?x)
      )
    )
    :effect ( and
      (CheckConsistency)
      (when
        (or
          (LeftOf2 ?x)
          (LeftOf1 ?x)
        )
        (and
          (LeftOf1 ?x)
        )
      )
      (when
        (or
          (LeftOf2 ?x)
          (LeftOf3 ?x)
          (LeftOf1 ?x)
        )
        (and
          (LeftOf2 ?x)
        )
      )
      (when
        (or
          (LeftOf2 ?x)
          (LeftOf4 ?x)
          (LeftOf3 ?x)
          (LeftOf1 ?x)
        )
        (and
          (LeftOf3 ?x)
        )
      )
      (when
        (or
          (RightOf2 ?x)
          (RightOf1 ?x)
          (RightOf3 ?x)
        )
        (and
          (RightOf0 ?x)
          (not (RightOf1 ?x))
        )
      )
      (when
        (or
          (RightOf2 ?x)
          (RightOf3 ?x)
        )
        (and
          (RightOf1 ?x)
          (not (RightOf2 ?x))
        )
      )
      (when
        (RightOf3 ?x)
        (and
          (RightOf2 ?x)
          (not (RightOf3 ?x))
        )
      )
      (when
        (Column1 ?x)
        (and
          (Column0 ?x)
          (not (Column1 ?x))
        )
      )
      (when
        (Column2 ?x)
        (and
          (Column1 ?x)
          (not (Column2 ?x))
        )
      )
      (when
        (Column3 ?x)
        (and
          (Column2 ?x)
          (not (Column3 ?x))
        )
      )
      (when
        (or
          (and
            (LeftOf2 ?x)
            (RightOf3 ?x)
          )
          (and
            (RightOf2 ?x)
            (LeftOf1 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf2 ?x)
          )
          (and
            (LeftOf1 ?x)
            (RightOf3 ?x)
          )
          (and
            (RightOf1 ?x)
            (LeftOf1 ?x)
          )
          (and
            (RightOf1 ?x)
            (LeftOf2 ?x)
          )
        )
        (and
          (Column0 ?x)
        )
      )
      (when
        (or
          (and
            (RightOf2 ?x)
            (LeftOf3 ?x)
          )
          (and
            (LeftOf3 ?x)
            (RightOf3 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf3 ?x)
          )
          (and
            (RightOf2 ?x)
            (LeftOf1 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf2 ?x)
          )
          (and
            (LeftOf1 ?x)
            (RightOf3 ?x)
          )
        )
        (and
          (Column1 ?x)
        )
      )
      (when
        (or
          (and
            (LeftOf1 ?x)
            (RightOf3 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf3 ?x)
          )
          (and
            (RightOf3 ?x)
            (LeftOf3 ?x)
          )
          (and
            (LeftOf4 ?x)
            (RightOf3 ?x)
          )
        )
        (and
          (Column2 ?x)
        )
      )
    )
  )
  (:action CheckConsistencyAction
    :parameters ( )
    :precondition ( and
      (CheckConsistency)
      (not (Error))
    )
    :effect ( and
      (not (CheckConsistency))
      (when
        (or
          (exists (?x_3 )
            (and
              (LeftOf1 ?x_3)
              (RightOf1 ?x_3)
            )
          )
          (exists (?x_5 )
            (and
              (AboveOf1 ?x_5)
              (BelowOf1 ?x_5)
            )
          )
          (exists (?x_0 )
            (and
              (LeftOf1 ?x_0)
              (RightOf3 ?x_0)
            )
          )
          (exists (?x_2 )
            (and
              (LeftOf1 ?x_2)
              (RightOf3 ?x_2)
            )
          )
          (exists (?x_2 )
            (and
              (RightOf2 ?x_2)
              (LeftOf2 ?x_2)
            )
          )
          (exists (?x_3 )
            (and
              (LeftOf1 ?x_3)
              (RightOf2 ?x_3)
            )
          )
          (exists (?x_4 )
            (and
              (BelowOf1 ?x_4)
              (AboveOf3 ?x_4)
            )
          )
          (exists (?x_5 )
            (and
              (AboveOf3 ?x_5)
              (BelowOf1 ?x_5)
            )
          )
          (exists (?x_0 )
            (and
              (LeftOf3 ?x_0)
              (RightOf3 ?x_0)
            )
          )
          (exists (?x_5 )
            (and
              (AboveOf2 ?x_5)
              (BelowOf1 ?x_5)
            )
          )
          (exists (?x_2 )
            (and
              (LeftOf1 ?x_2)
              (RightOf2 ?x_2)
            )
          )
          (exists (?x_3 )
            (and
              (LeftOf1 ?x_3)
              (RightOf3 ?x_3)
            )
          )
          (exists (?x_1 )
            (and
              (BelowOf1 ?x_1)
              (AboveOf3 ?x_1)
            )
          )
          (exists (?x_0 )
            (and
              (RightOf3 ?x_0)
              (LeftOf2 ?x_0)
            )
          )
          (exists (?x_4 )
            (and
              (BelowOf2 ?x_4)
              (AboveOf2 ?x_4)
            )
          )
          (exists (?x_4 )
            (and
              (BelowOf1 ?x_4)
              (AboveOf2 ?x_4)
            )
          )
          (exists (?x_1 )
            (and
              (BelowOf2 ?x_1)
              (AboveOf3 ?x_1)
            )
          )
          (exists (?x_2 )
            (and
              (LeftOf2 ?x_2)
              (RightOf3 ?x_2)
            )
          )
          (exists (?x_4 )
            (and
              (BelowOf2 ?x_4)
              (AboveOf3 ?x_4)
            )
          )
          (exists (?x_1 )
            (and
              (BelowOf3 ?x_1)
              (AboveOf3 ?x_1)
            )
          )
        )
        (Error)
      )
    )
  )
)