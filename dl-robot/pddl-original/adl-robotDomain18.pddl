(define
  (domain robot)
  (:requirements :adl)
  (:predicates
    (AboveOf16 ?x )
    (AboveOf17 ?x )
    (AboveOf14 ?x )
    (AboveOf15 ?x )
    (AboveOf12 ?x )
    (AboveOf13 ?x )
    (AboveOf10 ?x )
    (AboveOf11 ?x )
    (BelowOf8 ?x )
    (BelowOf9 ?x )
    (BelowOf12 ?x )
    (BelowOf13 ?x )
    (BelowOf10 ?x )
    (BelowOf11 ?x )
    (BelowOf16 ?x )
    (BelowOf17 ?x )
    (BelowOf14 ?x )
    (BelowOf15 ?x )
    (BelowOf18 ?x )
    (LeftOf3 ?x )
    (LeftOf2 ?x )
    (LeftOf1 ?x )
    (LeftOf7 ?x )
    (LeftOf6 ?x )
    (LeftOf5 ?x )
    (LeftOf4 ?x )
    (LeftOf9 ?x )
    (LeftOf8 ?x )
    (Row12 ?x )
    (Row13 ?x )
    (Row10 ?x )
    (Row11 ?x )
    (Row16 ?x )
    (Row17 ?x )
    (Row14 ?x )
    (Row15 ?x )
    (LeftOf13 ?x )
    (AboveOf2 ?x )
    (LeftOf12 ?x )
    (Column11 ?x )
    (Column10 ?x )
    (Column13 ?x )
    (Column12 ?x )
    (Column15 ?x )
    (Column14 ?x )
    (Column17 ?x )
    (Column16 ?x )
    (AboveOf4 ?x )
    (LeftOf16 ?x )
    (AboveOf6 ?x )
    (AboveOf7 ?x )
    (AboveOf0 ?x )
    (AboveOf1 ?x )
    (LeftOf11 ?x )
    (AboveOf3 ?x )
    (AboveOf8 ?x )
    (AboveOf9 ?x )
    (LeftOf18 ?x )
    (RightOf9 ?x )
    (RightOf8 ?x )
    (RightOf3 ?x )
    (RightOf2 ?x )
    (RightOf1 ?x )
    (RightOf0 ?x )
    (RightOf7 ?x )
    (RightOf6 ?x )
    (RightOf5 ?x )
    (RightOf4 ?x )
    (Columns ?x )
    (BelowOf1 ?x )
    (BelowOf2 ?x )
    (BelowOf3 ?x )
    (Column9 ?x )
    (Column8 ?x )
    (BelowOf6 ?x )
    (BelowOf7 ?x )
    (Column5 ?x )
    (Column4 ?x )
    (Column7 ?x )
    (Column6 ?x )
    (Column1 ?x )
    (Column0 ?x )
    (Column3 ?x )
    (Column2 ?x )
    (Rows ?x )
    (LeftOf17 ?x )
    (AboveOf5 ?x )
    (LeftOf15 ?x )
    (LeftOf14 ?x )
    (RightOf17 ?x )
    (RightOf16 ?x )
    (RightOf15 ?x )
    (RightOf14 ?x )
    (RightOf13 ?x )
    (RightOf12 ?x )
    (RightOf11 ?x )
    (RightOf10 ?x )
    (Row0 ?x )
    (Row1 ?x )
    (Row2 ?x )
    (Row3 ?x )
    (Row4 ?x )
    (Row5 ?x )
    (Row6 ?x )
    (Row7 ?x )
    (Row8 ?x )
    (Row9 ?x )
    (LeftOf10 ?x )
    (BelowOf4 ?x )
    (BelowOf5 ?x )
    (CheckConsistency)
    (Error)
  )
  (:action moveDown
    :parameters ( ?x )
    :precondition ( and
      (not (CheckConsistency))
      (not (Error))
      (or
        (BelowOf7 ?x)
        (AboveOf9 ?x)
        (BelowOf18 ?x)
        (BelowOf1 ?x)
        (BelowOf17 ?x)
        (AboveOf7 ?x)
        (BelowOf16 ?x)
        (AboveOf1 ?x)
        (AboveOf11 ?x)
        (BelowOf14 ?x)
        (BelowOf8 ?x)
        (BelowOf3 ?x)
        (AboveOf14 ?x)
        (AboveOf3 ?x)
        (BelowOf9 ?x)
        (AboveOf16 ?x)
        (BelowOf2 ?x)
        (BelowOf5 ?x)
        (BelowOf11 ?x)
        (BelowOf10 ?x)
        (BelowOf13 ?x)
        (AboveOf17 ?x)
        (BelowOf4 ?x)
        (AboveOf10 ?x)
        (AboveOf0 ?x)
        (AboveOf8 ?x)
        (AboveOf6 ?x)
        (AboveOf5 ?x)
        (AboveOf13 ?x)
        (Rows ?x)
        (AboveOf4 ?x)
        (BelowOf12 ?x)
        (AboveOf15 ?x)
        (AboveOf12 ?x)
        (BelowOf6 ?x)
        (BelowOf15 ?x)
        (AboveOf2 ?x)
      )
    )
    :effect ( and
      (CheckConsistency)
      (when
        (or
          (BelowOf1 ?x)
          (BelowOf2 ?x)
        )
        (and
          (BelowOf1 ?x)
        )
      )
      (when
        (or
          (BelowOf3 ?x)
          (BelowOf1 ?x)
          (BelowOf2 ?x)
        )
        (and
          (BelowOf2 ?x)
        )
      )
      (when
        (or
          (BelowOf4 ?x)
          (BelowOf3 ?x)
          (BelowOf1 ?x)
          (BelowOf2 ?x)
        )
        (and
          (BelowOf3 ?x)
        )
      )
      (when
        (or
          (BelowOf5 ?x)
          (BelowOf4 ?x)
          (BelowOf3 ?x)
          (BelowOf1 ?x)
          (BelowOf2 ?x)
        )
        (and
          (BelowOf4 ?x)
        )
      )
      (when
        (or
          (BelowOf3 ?x)
          (BelowOf4 ?x)
          (BelowOf1 ?x)
          (BelowOf2 ?x)
          (BelowOf5 ?x)
          (BelowOf6 ?x)
        )
        (and
          (BelowOf5 ?x)
        )
      )
      (when
        (or
          (BelowOf3 ?x)
          (BelowOf7 ?x)
          (BelowOf4 ?x)
          (BelowOf1 ?x)
          (BelowOf2 ?x)
          (BelowOf5 ?x)
          (BelowOf6 ?x)
        )
        (and
          (BelowOf6 ?x)
        )
      )
      (when
        (or
          (BelowOf8 ?x)
          (BelowOf3 ?x)
          (BelowOf7 ?x)
          (BelowOf4 ?x)
          (BelowOf1 ?x)
          (BelowOf2 ?x)
          (BelowOf5 ?x)
          (BelowOf6 ?x)
        )
        (and
          (BelowOf7 ?x)
        )
      )
      (when
        (or
          (BelowOf8 ?x)
          (BelowOf3 ?x)
          (BelowOf9 ?x)
          (BelowOf7 ?x)
          (BelowOf4 ?x)
          (BelowOf1 ?x)
          (BelowOf2 ?x)
          (BelowOf5 ?x)
          (BelowOf6 ?x)
        )
        (and
          (BelowOf8 ?x)
        )
      )
      (when
        (or
          (BelowOf8 ?x)
          (BelowOf3 ?x)
          (BelowOf9 ?x)
          (BelowOf7 ?x)
          (BelowOf4 ?x)
          (BelowOf1 ?x)
          (BelowOf2 ?x)
          (BelowOf5 ?x)
          (BelowOf6 ?x)
          (BelowOf10 ?x)
        )
        (and
          (BelowOf9 ?x)
        )
      )
      (when
        (or
          (BelowOf8 ?x)
          (BelowOf3 ?x)
          (BelowOf9 ?x)
          (BelowOf7 ?x)
          (BelowOf4 ?x)
          (BelowOf1 ?x)
          (BelowOf2 ?x)
          (BelowOf5 ?x)
          (BelowOf6 ?x)
          (BelowOf11 ?x)
          (BelowOf10 ?x)
        )
        (and
          (BelowOf10 ?x)
        )
      )
      (when
        (or
          (BelowOf8 ?x)
          (BelowOf3 ?x)
          (BelowOf9 ?x)
          (BelowOf7 ?x)
          (BelowOf12 ?x)
          (BelowOf4 ?x)
          (BelowOf1 ?x)
          (BelowOf2 ?x)
          (BelowOf5 ?x)
          (BelowOf6 ?x)
          (BelowOf11 ?x)
          (BelowOf10 ?x)
        )
        (and
          (BelowOf11 ?x)
        )
      )
      (when
        (or
          (BelowOf8 ?x)
          (BelowOf13 ?x)
          (BelowOf3 ?x)
          (BelowOf9 ?x)
          (BelowOf7 ?x)
          (BelowOf12 ?x)
          (BelowOf4 ?x)
          (BelowOf1 ?x)
          (BelowOf2 ?x)
          (BelowOf5 ?x)
          (BelowOf6 ?x)
          (BelowOf11 ?x)
          (BelowOf10 ?x)
        )
        (and
          (BelowOf12 ?x)
        )
      )
      (when
        (or
          (BelowOf14 ?x)
          (BelowOf8 ?x)
          (BelowOf13 ?x)
          (BelowOf3 ?x)
          (BelowOf9 ?x)
          (BelowOf7 ?x)
          (BelowOf12 ?x)
          (BelowOf4 ?x)
          (BelowOf1 ?x)
          (BelowOf2 ?x)
          (BelowOf5 ?x)
          (BelowOf6 ?x)
          (BelowOf11 ?x)
          (BelowOf10 ?x)
        )
        (and
          (BelowOf13 ?x)
        )
      )
      (when
        (or
          (BelowOf14 ?x)
          (BelowOf8 ?x)
          (BelowOf13 ?x)
          (BelowOf3 ?x)
          (BelowOf9 ?x)
          (BelowOf7 ?x)
          (BelowOf12 ?x)
          (BelowOf4 ?x)
          (BelowOf1 ?x)
          (BelowOf2 ?x)
          (BelowOf5 ?x)
          (BelowOf6 ?x)
          (BelowOf15 ?x)
          (BelowOf11 ?x)
          (BelowOf10 ?x)
        )
        (and
          (BelowOf14 ?x)
        )
      )
      (when
        (or
          (BelowOf14 ?x)
          (BelowOf8 ?x)
          (BelowOf13 ?x)
          (BelowOf3 ?x)
          (BelowOf9 ?x)
          (BelowOf7 ?x)
          (BelowOf12 ?x)
          (BelowOf4 ?x)
          (BelowOf1 ?x)
          (BelowOf2 ?x)
          (BelowOf5 ?x)
          (BelowOf11 ?x)
          (BelowOf6 ?x)
          (BelowOf15 ?x)
          (BelowOf16 ?x)
          (BelowOf10 ?x)
        )
        (and
          (BelowOf15 ?x)
        )
      )
      (when
        (or
          (BelowOf14 ?x)
          (BelowOf8 ?x)
          (BelowOf13 ?x)
          (BelowOf3 ?x)
          (BelowOf9 ?x)
          (BelowOf7 ?x)
          (BelowOf12 ?x)
          (BelowOf11 ?x)
          (BelowOf4 ?x)
          (BelowOf1 ?x)
          (BelowOf2 ?x)
          (BelowOf5 ?x)
          (BelowOf17 ?x)
          (BelowOf6 ?x)
          (BelowOf15 ?x)
          (BelowOf16 ?x)
          (BelowOf10 ?x)
        )
        (and
          (BelowOf16 ?x)
        )
      )
      (when
        (or
          (BelowOf14 ?x)
          (BelowOf8 ?x)
          (BelowOf13 ?x)
          (BelowOf3 ?x)
          (BelowOf9 ?x)
          (BelowOf7 ?x)
          (BelowOf12 ?x)
          (BelowOf18 ?x)
          (BelowOf11 ?x)
          (BelowOf4 ?x)
          (BelowOf1 ?x)
          (BelowOf2 ?x)
          (BelowOf5 ?x)
          (BelowOf17 ?x)
          (BelowOf6 ?x)
          (BelowOf15 ?x)
          (BelowOf16 ?x)
          (BelowOf10 ?x)
        )
        (and
          (BelowOf17 ?x)
        )
      )
      (when
        (or
          (AboveOf1 ?x)
          (AboveOf5 ?x)
          (AboveOf14 ?x)
          (AboveOf13 ?x)
          (AboveOf3 ?x)
          (AboveOf4 ?x)
          (AboveOf16 ?x)
          (AboveOf9 ?x)
          (AboveOf8 ?x)
          (AboveOf15 ?x)
          (AboveOf17 ?x)
          (AboveOf12 ?x)
          (AboveOf7 ?x)
          (AboveOf2 ?x)
          (AboveOf10 ?x)
          (AboveOf11 ?x)
          (AboveOf6 ?x)
        )
        (and
          (AboveOf0 ?x)
          (not (AboveOf1 ?x))
        )
      )
      (when
        (or
          (AboveOf5 ?x)
          (AboveOf14 ?x)
          (AboveOf13 ?x)
          (AboveOf3 ?x)
          (AboveOf4 ?x)
          (AboveOf16 ?x)
          (AboveOf9 ?x)
          (AboveOf8 ?x)
          (AboveOf15 ?x)
          (AboveOf17 ?x)
          (AboveOf12 ?x)
          (AboveOf7 ?x)
          (AboveOf2 ?x)
          (AboveOf10 ?x)
          (AboveOf6 ?x)
          (AboveOf11 ?x)
        )
        (and
          (AboveOf1 ?x)
          (not (AboveOf2 ?x))
        )
      )
      (when
        (or
          (AboveOf5 ?x)
          (AboveOf14 ?x)
          (AboveOf13 ?x)
          (AboveOf3 ?x)
          (AboveOf4 ?x)
          (AboveOf16 ?x)
          (AboveOf9 ?x)
          (AboveOf15 ?x)
          (AboveOf17 ?x)
          (AboveOf12 ?x)
          (AboveOf7 ?x)
          (AboveOf8 ?x)
          (AboveOf10 ?x)
          (AboveOf6 ?x)
          (AboveOf11 ?x)
        )
        (and
          (AboveOf2 ?x)
          (not (AboveOf3 ?x))
        )
      )
      (when
        (or
          (AboveOf5 ?x)
          (AboveOf14 ?x)
          (AboveOf13 ?x)
          (AboveOf4 ?x)
          (AboveOf16 ?x)
          (AboveOf9 ?x)
          (AboveOf15 ?x)
          (AboveOf17 ?x)
          (AboveOf12 ?x)
          (AboveOf7 ?x)
          (AboveOf8 ?x)
          (AboveOf10 ?x)
          (AboveOf6 ?x)
          (AboveOf11 ?x)
        )
        (and
          (AboveOf3 ?x)
          (not (AboveOf4 ?x))
        )
      )
      (when
        (or
          (AboveOf5 ?x)
          (AboveOf14 ?x)
          (AboveOf13 ?x)
          (AboveOf16 ?x)
          (AboveOf9 ?x)
          (AboveOf15 ?x)
          (AboveOf17 ?x)
          (AboveOf12 ?x)
          (AboveOf10 ?x)
          (AboveOf8 ?x)
          (AboveOf7 ?x)
          (AboveOf6 ?x)
          (AboveOf11 ?x)
        )
        (and
          (AboveOf4 ?x)
          (not (AboveOf5 ?x))
        )
      )
      (when
        (or
          (AboveOf14 ?x)
          (AboveOf13 ?x)
          (AboveOf16 ?x)
          (AboveOf9 ?x)
          (AboveOf15 ?x)
          (AboveOf17 ?x)
          (AboveOf12 ?x)
          (AboveOf10 ?x)
          (AboveOf8 ?x)
          (AboveOf7 ?x)
          (AboveOf6 ?x)
          (AboveOf11 ?x)
        )
        (and
          (AboveOf5 ?x)
          (not (AboveOf6 ?x))
        )
      )
      (when
        (or
          (AboveOf14 ?x)
          (AboveOf13 ?x)
          (AboveOf16 ?x)
          (AboveOf9 ?x)
          (AboveOf15 ?x)
          (AboveOf17 ?x)
          (AboveOf12 ?x)
          (AboveOf10 ?x)
          (AboveOf8 ?x)
          (AboveOf7 ?x)
          (AboveOf11 ?x)
        )
        (and
          (AboveOf6 ?x)
          (not (AboveOf7 ?x))
        )
      )
      (when
        (or
          (AboveOf14 ?x)
          (AboveOf13 ?x)
          (AboveOf16 ?x)
          (AboveOf9 ?x)
          (AboveOf15 ?x)
          (AboveOf17 ?x)
          (AboveOf12 ?x)
          (AboveOf10 ?x)
          (AboveOf8 ?x)
          (AboveOf11 ?x)
        )
        (and
          (AboveOf7 ?x)
          (not (AboveOf8 ?x))
        )
      )
      (when
        (or
          (AboveOf14 ?x)
          (AboveOf13 ?x)
          (AboveOf16 ?x)
          (AboveOf9 ?x)
          (AboveOf15 ?x)
          (AboveOf17 ?x)
          (AboveOf12 ?x)
          (AboveOf10 ?x)
          (AboveOf11 ?x)
        )
        (and
          (AboveOf8 ?x)
          (not (AboveOf9 ?x))
        )
      )
      (when
        (or
          (AboveOf14 ?x)
          (AboveOf13 ?x)
          (AboveOf16 ?x)
          (AboveOf15 ?x)
          (AboveOf17 ?x)
          (AboveOf12 ?x)
          (AboveOf10 ?x)
          (AboveOf11 ?x)
        )
        (and
          (AboveOf9 ?x)
          (not (AboveOf10 ?x))
        )
      )
      (when
        (or
          (AboveOf14 ?x)
          (AboveOf13 ?x)
          (AboveOf16 ?x)
          (AboveOf15 ?x)
          (AboveOf17 ?x)
          (AboveOf12 ?x)
          (AboveOf11 ?x)
        )
        (and
          (AboveOf10 ?x)
          (not (AboveOf11 ?x))
        )
      )
      (when
        (or
          (AboveOf14 ?x)
          (AboveOf13 ?x)
          (AboveOf16 ?x)
          (AboveOf15 ?x)
          (AboveOf17 ?x)
          (AboveOf12 ?x)
        )
        (and
          (AboveOf11 ?x)
          (not (AboveOf12 ?x))
        )
      )
      (when
        (or
          (AboveOf16 ?x)
          (AboveOf14 ?x)
          (AboveOf13 ?x)
          (AboveOf17 ?x)
          (AboveOf15 ?x)
        )
        (and
          (AboveOf12 ?x)
          (not (AboveOf13 ?x))
        )
      )
      (when
        (or
          (AboveOf15 ?x)
          (AboveOf14 ?x)
          (AboveOf17 ?x)
          (AboveOf16 ?x)
        )
        (and
          (AboveOf13 ?x)
          (not (AboveOf14 ?x))
        )
      )
      (when
        (or
          (AboveOf16 ?x)
          (AboveOf15 ?x)
          (AboveOf17 ?x)
        )
        (and
          (AboveOf14 ?x)
          (not (AboveOf15 ?x))
        )
      )
      (when
        (or
          (AboveOf16 ?x)
          (AboveOf17 ?x)
        )
        (and
          (AboveOf15 ?x)
          (not (AboveOf16 ?x))
        )
      )
      (when
        (AboveOf17 ?x)
        (and
          (AboveOf16 ?x)
          (not (AboveOf17 ?x))
        )
      )
      (when
        (Row1 ?x)
        (and
          (Row0 ?x)
          (not (Row1 ?x))
        )
      )
      (when
        (Row2 ?x)
        (and
          (Row1 ?x)
          (not (Row2 ?x))
        )
      )
      (when
        (Row3 ?x)
        (and
          (Row2 ?x)
          (not (Row3 ?x))
        )
      )
      (when
        (Row4 ?x)
        (and
          (Row3 ?x)
          (not (Row4 ?x))
        )
      )
      (when
        (Row5 ?x)
        (and
          (Row4 ?x)
          (not (Row5 ?x))
        )
      )
      (when
        (Row6 ?x)
        (and
          (Row5 ?x)
          (not (Row6 ?x))
        )
      )
      (when
        (Row7 ?x)
        (and
          (Row6 ?x)
          (not (Row7 ?x))
        )
      )
      (when
        (Row8 ?x)
        (and
          (Row7 ?x)
          (not (Row8 ?x))
        )
      )
      (when
        (Row9 ?x)
        (and
          (Row8 ?x)
          (not (Row9 ?x))
        )
      )
      (when
        (Row10 ?x)
        (and
          (Row9 ?x)
          (not (Row10 ?x))
        )
      )
      (when
        (Row11 ?x)
        (and
          (Row10 ?x)
          (not (Row11 ?x))
        )
      )
      (when
        (Row12 ?x)
        (and
          (Row11 ?x)
          (not (Row12 ?x))
        )
      )
      (when
        (Row13 ?x)
        (and
          (Row12 ?x)
          (not (Row13 ?x))
        )
      )
      (when
        (Row14 ?x)
        (and
          (Row13 ?x)
          (not (Row14 ?x))
        )
      )
      (when
        (Row15 ?x)
        (and
          (Row14 ?x)
          (not (Row15 ?x))
        )
      )
      (when
        (Row16 ?x)
        (and
          (Row15 ?x)
          (not (Row16 ?x))
        )
      )
      (when
        (Row17 ?x)
        (and
          (Row16 ?x)
          (not (Row17 ?x))
        )
      )
      (when
        (or
          (and
            (BelowOf2 ?x)
            (AboveOf3 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf8 ?x)
          )
          (and
            (AboveOf15 ?x)
            (BelowOf1 ?x)
          )
          (and
            (AboveOf5 ?x)
            (BelowOf2 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf12 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf12 ?x)
          )
          (and
            (AboveOf4 ?x)
            (BelowOf1 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf8 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf10 ?x)
          )
          (and
            (AboveOf9 ?x)
            (BelowOf1 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf3 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf11 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf10 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf17 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf1 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf6 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf1 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf9 ?x)
          )
          (and
            (AboveOf2 ?x)
            (BelowOf1 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf6 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf2 ?x)
          )
          (and
            (AboveOf17 ?x)
            (BelowOf1 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf4 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf16 ?x)
          )
          (and
            (AboveOf5 ?x)
            (BelowOf1 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf2 ?x)
          )
          (and
            (AboveOf7 ?x)
            (BelowOf1 ?x)
          )
          (and
            (AboveOf16 ?x)
            (BelowOf1 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf2 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf15 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf7 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf1 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf11 ?x)
          )
          (and
            (AboveOf1 ?x)
            (BelowOf1 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf17 ?x)
          )
        )
        (and
          (Row0 ?x)
        )
      )
      (when
        (or
          (and
            (BelowOf2 ?x)
            (AboveOf3 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf8 ?x)
          )
          (and
            (AboveOf15 ?x)
            (BelowOf1 ?x)
          )
          (and
            (AboveOf5 ?x)
            (BelowOf2 ?x)
          )
          (and
            (BelowOf3 ?x)
            (AboveOf11 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf3 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf12 ?x)
          )
          (and
            (BelowOf3 ?x)
            (AboveOf12 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf12 ?x)
          )
          (and
            (AboveOf4 ?x)
            (BelowOf1 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf8 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf10 ?x)
          )
          (and
            (AboveOf9 ?x)
            (BelowOf1 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf3 ?x)
          )
          (and
            (AboveOf2 ?x)
            (BelowOf2 ?x)
          )
          (and
            (AboveOf10 ?x)
            (BelowOf3 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf1 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf10 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf17 ?x)
          )
          (and
            (AboveOf4 ?x)
            (BelowOf3 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf6 ?x)
          )
          (and
            (BelowOf3 ?x)
            (AboveOf8 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf1 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf9 ?x)
          )
          (and
            (BelowOf3 ?x)
            (AboveOf3 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf6 ?x)
          )
          (and
            (AboveOf2 ?x)
            (BelowOf3 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf2 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf4 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf16 ?x)
          )
          (and
            (AboveOf15 ?x)
            (BelowOf3 ?x)
          )
          (and
            (AboveOf9 ?x)
            (BelowOf3 ?x)
          )
          (and
            (AboveOf5 ?x)
            (BelowOf1 ?x)
          )
          (and
            (AboveOf16 ?x)
            (BelowOf3 ?x)
          )
          (and
            (AboveOf7 ?x)
            (BelowOf1 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf2 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf3 ?x)
          )
          (and
            (AboveOf17 ?x)
            (BelowOf3 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf15 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf7 ?x)
          )
          (and
            (AboveOf17 ?x)
            (BelowOf1 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf11 ?x)
          )
          (and
            (AboveOf7 ?x)
            (BelowOf3 ?x)
          )
          (and
            (AboveOf2 ?x)
            (BelowOf1 ?x)
          )
          (and
            (AboveOf6 ?x)
            (BelowOf3 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf11 ?x)
          )
          (and
            (AboveOf16 ?x)
            (BelowOf1 ?x)
          )
          (and
            (AboveOf5 ?x)
            (BelowOf3 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf17 ?x)
          )
        )
        (and
          (Row1 ?x)
        )
      )
      (when
        (or
          (and
            (BelowOf2 ?x)
            (AboveOf3 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf8 ?x)
          )
          (and
            (AboveOf15 ?x)
            (BelowOf1 ?x)
          )
          (and
            (AboveOf5 ?x)
            (BelowOf2 ?x)
          )
          (and
            (BelowOf3 ?x)
            (AboveOf11 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf12 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf2 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf3 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf4 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf11 ?x)
          )
          (and
            (BelowOf3 ?x)
            (AboveOf12 ?x)
          )
          (and
            (AboveOf7 ?x)
            (BelowOf4 ?x)
          )
          (and
            (AboveOf11 ?x)
            (BelowOf3 ?x)
          )
          (and
            (AboveOf4 ?x)
            (BelowOf1 ?x)
          )
          (and
            (AboveOf15 ?x)
            (BelowOf4 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf10 ?x)
          )
          (and
            (AboveOf9 ?x)
            (BelowOf1 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf3 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf10 ?x)
          )
          (and
            (AboveOf10 ?x)
            (BelowOf3 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf1 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf10 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf12 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf17 ?x)
          )
          (and
            (AboveOf4 ?x)
            (BelowOf3 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf6 ?x)
          )
          (and
            (BelowOf3 ?x)
            (AboveOf8 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf1 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf9 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf6 ?x)
          )
          (and
            (AboveOf3 ?x)
            (BelowOf3 ?x)
          )
          (and
            (AboveOf5 ?x)
            (BelowOf4 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf12 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf4 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf16 ?x)
          )
          (and
            (AboveOf15 ?x)
            (BelowOf3 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf8 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf8 ?x)
          )
          (and
            (AboveOf5 ?x)
            (BelowOf1 ?x)
          )
          (and
            (AboveOf16 ?x)
            (BelowOf3 ?x)
          )
          (and
            (AboveOf7 ?x)
            (BelowOf1 ?x)
          )
          (and
            (AboveOf16 ?x)
            (BelowOf4 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf2 ?x)
          )
          (and
            (AboveOf4 ?x)
            (BelowOf4 ?x)
          )
          (and
            (AboveOf17 ?x)
            (BelowOf3 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf15 ?x)
          )
          (and
            (AboveOf9 ?x)
            (BelowOf3 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf7 ?x)
          )
          (and
            (AboveOf17 ?x)
            (BelowOf1 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf11 ?x)
          )
          (and
            (AboveOf16 ?x)
            (BelowOf1 ?x)
          )
          (and
            (AboveOf7 ?x)
            (BelowOf3 ?x)
          )
          (and
            (AboveOf6 ?x)
            (BelowOf3 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf11 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf6 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf17 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf3 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf4 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf9 ?x)
          )
          (and
            (AboveOf5 ?x)
            (BelowOf3 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf3 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf17 ?x)
          )
        )
        (and
          (Row2 ?x)
        )
      )
      (when
        (or
          (and
            (BelowOf5 ?x)
            (AboveOf14 ?x)
          )
          (and
            (AboveOf10 ?x)
            (BelowOf3 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf8 ?x)
          )
          (and
            (AboveOf15 ?x)
            (BelowOf1 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf6 ?x)
          )
          (and
            (BelowOf3 ?x)
            (AboveOf11 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf11 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf12 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf2 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf3 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf4 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf12 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf11 ?x)
          )
          (and
            (BelowOf3 ?x)
            (AboveOf12 ?x)
          )
          (and
            (AboveOf11 ?x)
            (BelowOf3 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf9 ?x)
          )
          (and
            (AboveOf4 ?x)
            (BelowOf1 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf8 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf10 ?x)
          )
          (and
            (AboveOf9 ?x)
            (BelowOf1 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf6 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf10 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf11 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf1 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf10 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf12 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf7 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf17 ?x)
          )
          (and
            (AboveOf4 ?x)
            (BelowOf3 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf5 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf6 ?x)
          )
          (and
            (BelowOf3 ?x)
            (AboveOf8 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf1 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf9 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf16 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf6 ?x)
          )
          (and
            (AboveOf9 ?x)
            (BelowOf3 ?x)
          )
          (and
            (AboveOf5 ?x)
            (BelowOf4 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf12 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf4 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf16 ?x)
          )
          (and
            (AboveOf15 ?x)
            (BelowOf3 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf10 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf8 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf13 ?x)
          )
          (and
            (AboveOf5 ?x)
            (BelowOf1 ?x)
          )
          (and
            (AboveOf16 ?x)
            (BelowOf3 ?x)
          )
          (and
            (AboveOf5 ?x)
            (BelowOf2 ?x)
          )
          (and
            (AboveOf7 ?x)
            (BelowOf1 ?x)
          )
          (and
            (AboveOf16 ?x)
            (BelowOf4 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf2 ?x)
          )
          (and
            (AboveOf15 ?x)
            (BelowOf4 ?x)
          )
          (and
            (AboveOf17 ?x)
            (BelowOf3 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf15 ?x)
          )
          (and
            (AboveOf4 ?x)
            (BelowOf4 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf7 ?x)
          )
          (and
            (AboveOf17 ?x)
            (BelowOf1 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf11 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf16 ?x)
          )
          (and
            (AboveOf16 ?x)
            (BelowOf1 ?x)
          )
          (and
            (AboveOf7 ?x)
            (BelowOf3 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf8 ?x)
          )
          (and
            (AboveOf6 ?x)
            (BelowOf3 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf15 ?x)
          )
          (and
            (AboveOf7 ?x)
            (BelowOf4 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf4 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf17 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf9 ?x)
          )
          (and
            (AboveOf5 ?x)
            (BelowOf3 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf4 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf3 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf17 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf17 ?x)
          )
        )
        (and
          (Row3 ?x)
        )
      )
      (when
        (or
          (and
            (BelowOf5 ?x)
            (AboveOf14 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf11 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf8 ?x)
          )
          (and
            (AboveOf15 ?x)
            (BelowOf1 ?x)
          )
          (and
            (AboveOf16 ?x)
            (BelowOf6 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf6 ?x)
          )
          (and
            (BelowOf3 ?x)
            (AboveOf11 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf11 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf12 ?x)
          )
          (and
            (AboveOf10 ?x)
            (BelowOf6 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf2 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf3 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf6 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf11 ?x)
          )
          (and
            (BelowOf3 ?x)
            (AboveOf12 ?x)
          )
          (and
            (AboveOf7 ?x)
            (BelowOf4 ?x)
          )
          (and
            (AboveOf11 ?x)
            (BelowOf3 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf9 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf16 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf8 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf10 ?x)
          )
          (and
            (AboveOf9 ?x)
            (BelowOf6 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf6 ?x)
          )
          (and
            (BelowOf6 ?x)
            (AboveOf8 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf10 ?x)
          )
          (and
            (AboveOf17 ?x)
            (BelowOf6 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf10 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf12 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf7 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf1 ?x)
          )
          (and
            (AboveOf6 ?x)
            (BelowOf6 ?x)
          )
          (and
            (BelowOf3 ?x)
            (AboveOf8 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf1 ?x)
          )
          (and
            (AboveOf16 ?x)
            (BelowOf4 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf6 ?x)
          )
          (and
            (AboveOf5 ?x)
            (BelowOf4 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf12 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf17 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf16 ?x)
          )
          (and
            (AboveOf15 ?x)
            (BelowOf3 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf10 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf8 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf13 ?x)
          )
          (and
            (AboveOf5 ?x)
            (BelowOf5 ?x)
          )
          (and
            (AboveOf7 ?x)
            (BelowOf6 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf17 ?x)
          )
          (and
            (AboveOf16 ?x)
            (BelowOf3 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf12 ?x)
          )
          (and
            (AboveOf7 ?x)
            (BelowOf1 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf9 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf2 ?x)
          )
          (and
            (AboveOf9 ?x)
            (BelowOf1 ?x)
          )
          (and
            (AboveOf15 ?x)
            (BelowOf4 ?x)
          )
          (and
            (AboveOf10 ?x)
            (BelowOf3 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf15 ?x)
          )
          (and
            (AboveOf9 ?x)
            (BelowOf3 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf7 ?x)
          )
          (and
            (AboveOf17 ?x)
            (BelowOf1 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf6 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf11 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf16 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf5 ?x)
          )
          (and
            (BelowOf6 ?x)
            (AboveOf12 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf6 ?x)
          )
          (and
            (AboveOf16 ?x)
            (BelowOf1 ?x)
          )
          (and
            (AboveOf7 ?x)
            (BelowOf3 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf8 ?x)
          )
          (and
            (AboveOf6 ?x)
            (BelowOf3 ?x)
          )
          (and
            (BelowOf6 ?x)
            (AboveOf11 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf15 ?x)
          )
          (and
            (AboveOf5 ?x)
            (BelowOf6 ?x)
          )
          (and
            (AboveOf5 ?x)
            (BelowOf1 ?x)
          )
          (and
            (AboveOf17 ?x)
            (BelowOf3 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf4 ?x)
          )
          (and
            (AboveOf15 ?x)
            (BelowOf6 ?x)
          )
          (and
            (AboveOf5 ?x)
            (BelowOf2 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf9 ?x)
          )
          (and
            (AboveOf5 ?x)
            (BelowOf3 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf3 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf17 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf4 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf17 ?x)
          )
        )
        (and
          (Row4 ?x)
        )
      )
      (when
        (or
          (and
            (BelowOf3 ?x)
            (AboveOf12 ?x)
          )
          (and
            (BelowOf7 ?x)
            (AboveOf12 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf11 ?x)
          )
          (and
            (AboveOf6 ?x)
            (BelowOf3 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf14 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf8 ?x)
          )
          (and
            (AboveOf7 ?x)
            (BelowOf6 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf15 ?x)
          )
          (and
            (BelowOf6 ?x)
            (AboveOf6 ?x)
          )
          (and
            (AboveOf17 ?x)
            (BelowOf1 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf6 ?x)
          )
          (and
            (BelowOf7 ?x)
            (AboveOf9 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf4 ?x)
          )
          (and
            (AboveOf16 ?x)
            (BelowOf6 ?x)
          )
          (and
            (BelowOf3 ?x)
            (AboveOf11 ?x)
          )
          (and
            (BelowOf7 ?x)
            (AboveOf11 ?x)
          )
          (and
            (BelowOf7 ?x)
            (AboveOf8 ?x)
          )
          (and
            (BelowOf7 ?x)
            (AboveOf17 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf11 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf16 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf8 ?x)
          )
          (and
            (AboveOf9 ?x)
            (BelowOf6 ?x)
          )
          (and
            (BelowOf6 ?x)
            (AboveOf8 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf12 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf7 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf6 ?x)
          )
          (and
            (BelowOf3 ?x)
            (AboveOf8 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf6 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf2 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf12 ?x)
          )
          (and
            (AboveOf7 ?x)
            (BelowOf4 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf9 ?x)
          )
          (and
            (AboveOf17 ?x)
            (BelowOf3 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf11 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf16 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf6 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf7 ?x)
          )
          (and
            (AboveOf15 ?x)
            (BelowOf6 ?x)
          )
          (and
            (BelowOf7 ?x)
            (AboveOf10 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf17 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf17 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf8 ?x)
          )
          (and
            (AboveOf15 ?x)
            (BelowOf1 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf6 ?x)
          )
          (and
            (AboveOf10 ?x)
            (BelowOf6 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf3 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf12 ?x)
          )
          (and
            (AboveOf11 ?x)
            (BelowOf3 ?x)
          )
          (and
            (AboveOf15 ?x)
            (BelowOf4 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf10 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf6 ?x)
          )
          (and
            (AboveOf17 ?x)
            (BelowOf6 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf10 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf1 ?x)
          )
          (and
            (AboveOf16 ?x)
            (BelowOf4 ?x)
          )
          (and
            (BelowOf6 ?x)
            (AboveOf10 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf16 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf13 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf3 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf5 ?x)
          )
          (and
            (BelowOf6 ?x)
            (AboveOf12 ?x)
          )
          (and
            (AboveOf7 ?x)
            (BelowOf3 ?x)
          )
          (and
            (BelowOf7 ?x)
            (AboveOf16 ?x)
          )
          (and
            (BelowOf6 ?x)
            (AboveOf11 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf4 ?x)
          )
          (and
            (AboveOf16 ?x)
            (BelowOf1 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf1 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf12 ?x)
          )
          (and
            (BelowOf7 ?x)
            (AboveOf7 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf9 ?x)
          )
          (and
            (AboveOf9 ?x)
            (BelowOf1 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf15 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf10 ?x)
          )
          (and
            (AboveOf10 ?x)
            (BelowOf3 ?x)
          )
          (and
            (AboveOf15 ?x)
            (BelowOf3 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf9 ?x)
          )
          (and
            (AboveOf15 ?x)
            (BelowOf7 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf7 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf17 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf11 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf10 ?x)
          )
          (and
            (AboveOf9 ?x)
            (BelowOf3 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf17 ?x)
          )
          (and
            (AboveOf16 ?x)
            (BelowOf3 ?x)
          )
          (and
            (AboveOf7 ?x)
            (BelowOf1 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf2 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf8 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf7 ?x)
          )
          (and
            (BelowOf7 ?x)
            (AboveOf6 ?x)
          )
        )
        (and
          (Row5 ?x)
        )
      )
      (when
        (or
          (and
            (BelowOf3 ?x)
            (AboveOf12 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf7 ?x)
          )
          (and
            (BelowOf7 ?x)
            (AboveOf12 ?x)
          )
          (and
            (AboveOf16 ?x)
            (BelowOf8 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf11 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf14 ?x)
          )
          (and
            (BelowOf8 ?x)
            (AboveOf12 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf8 ?x)
          )
          (and
            (AboveOf7 ?x)
            (BelowOf6 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf15 ?x)
          )
          (and
            (AboveOf17 ?x)
            (BelowOf1 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf6 ?x)
          )
          (and
            (BelowOf7 ?x)
            (AboveOf9 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf4 ?x)
          )
          (and
            (AboveOf15 ?x)
            (BelowOf8 ?x)
          )
          (and
            (AboveOf16 ?x)
            (BelowOf6 ?x)
          )
          (and
            (BelowOf3 ?x)
            (AboveOf11 ?x)
          )
          (and
            (BelowOf7 ?x)
            (AboveOf11 ?x)
          )
          (and
            (BelowOf7 ?x)
            (AboveOf8 ?x)
          )
          (and
            (BelowOf7 ?x)
            (AboveOf17 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf11 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf16 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf8 ?x)
          )
          (and
            (AboveOf9 ?x)
            (BelowOf6 ?x)
          )
          (and
            (BelowOf6 ?x)
            (AboveOf8 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf12 ?x)
          )
          (and
            (AboveOf7 ?x)
            (BelowOf7 ?x)
          )
          (and
            (BelowOf3 ?x)
            (AboveOf8 ?x)
          )
          (and
            (BelowOf8 ?x)
            (AboveOf8 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf16 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf2 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf12 ?x)
          )
          (and
            (AboveOf7 ?x)
            (BelowOf4 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf9 ?x)
          )
          (and
            (AboveOf17 ?x)
            (BelowOf3 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf11 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf16 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf6 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf7 ?x)
          )
          (and
            (AboveOf15 ?x)
            (BelowOf6 ?x)
          )
          (and
            (BelowOf7 ?x)
            (AboveOf10 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf17 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf17 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf8 ?x)
          )
          (and
            (AboveOf15 ?x)
            (BelowOf1 ?x)
          )
          (and
            (AboveOf7 ?x)
            (BelowOf8 ?x)
          )
          (and
            (AboveOf10 ?x)
            (BelowOf6 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf3 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf12 ?x)
          )
          (and
            (AboveOf11 ?x)
            (BelowOf3 ?x)
          )
          (and
            (AboveOf15 ?x)
            (BelowOf4 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf10 ?x)
          )
          (and
            (AboveOf17 ?x)
            (BelowOf6 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf10 ?x)
          )
          (and
            (AboveOf9 ?x)
            (BelowOf8 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf1 ?x)
          )
          (and
            (AboveOf16 ?x)
            (BelowOf4 ?x)
          )
          (and
            (BelowOf6 ?x)
            (AboveOf10 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf13 ?x)
          )
          (and
            (AboveOf10 ?x)
            (BelowOf8 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf3 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf5 ?x)
          )
          (and
            (BelowOf6 ?x)
            (AboveOf12 ?x)
          )
          (and
            (AboveOf7 ?x)
            (BelowOf3 ?x)
          )
          (and
            (BelowOf7 ?x)
            (AboveOf16 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf8 ?x)
          )
          (and
            (BelowOf6 ?x)
            (AboveOf11 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf4 ?x)
          )
          (and
            (AboveOf17 ?x)
            (BelowOf8 ?x)
          )
          (and
            (AboveOf16 ?x)
            (BelowOf1 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf8 ?x)
          )
          (and
            (AboveOf11 ?x)
            (BelowOf8 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf1 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf12 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf9 ?x)
          )
          (and
            (AboveOf9 ?x)
            (BelowOf1 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf15 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf10 ?x)
          )
          (and
            (AboveOf10 ?x)
            (BelowOf3 ?x)
          )
          (and
            (AboveOf15 ?x)
            (BelowOf3 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf9 ?x)
          )
          (and
            (AboveOf15 ?x)
            (BelowOf7 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf7 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf17 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf11 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf10 ?x)
          )
          (and
            (AboveOf9 ?x)
            (BelowOf3 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf17 ?x)
          )
          (and
            (AboveOf16 ?x)
            (BelowOf3 ?x)
          )
          (and
            (AboveOf7 ?x)
            (BelowOf1 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf2 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf8 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf7 ?x)
          )
        )
        (and
          (Row6 ?x)
        )
      )
      (when
        (or
          (and
            (BelowOf9 ?x)
            (AboveOf12 ?x)
          )
          (and
            (BelowOf3 ?x)
            (AboveOf12 ?x)
          )
          (and
            (BelowOf9 ?x)
            (AboveOf8 ?x)
          )
          (and
            (BelowOf7 ?x)
            (AboveOf12 ?x)
          )
          (and
            (AboveOf16 ?x)
            (BelowOf8 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf11 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf14 ?x)
          )
          (and
            (BelowOf8 ?x)
            (AboveOf12 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf8 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf15 ?x)
          )
          (and
            (AboveOf17 ?x)
            (BelowOf1 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf6 ?x)
          )
          (and
            (BelowOf7 ?x)
            (AboveOf9 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf4 ?x)
          )
          (and
            (AboveOf15 ?x)
            (BelowOf8 ?x)
          )
          (and
            (AboveOf15 ?x)
            (BelowOf9 ?x)
          )
          (and
            (BelowOf3 ?x)
            (AboveOf11 ?x)
          )
          (and
            (BelowOf7 ?x)
            (AboveOf11 ?x)
          )
          (and
            (BelowOf7 ?x)
            (AboveOf8 ?x)
          )
          (and
            (BelowOf7 ?x)
            (AboveOf17 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf11 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf16 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf8 ?x)
          )
          (and
            (AboveOf9 ?x)
            (BelowOf6 ?x)
          )
          (and
            (BelowOf6 ?x)
            (AboveOf8 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf12 ?x)
          )
          (and
            (BelowOf3 ?x)
            (AboveOf8 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf16 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf2 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf12 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf9 ?x)
          )
          (and
            (AboveOf8 ?x)
            (BelowOf8 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf11 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf16 ?x)
          )
          (and
            (AboveOf16 ?x)
            (BelowOf9 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf6 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf7 ?x)
          )
          (and
            (AboveOf15 ?x)
            (BelowOf6 ?x)
          )
          (and
            (AboveOf16 ?x)
            (BelowOf3 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf17 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf17 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf8 ?x)
          )
          (and
            (AboveOf15 ?x)
            (BelowOf1 ?x)
          )
          (and
            (AboveOf9 ?x)
            (BelowOf9 ?x)
          )
          (and
            (AboveOf10 ?x)
            (BelowOf6 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf3 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf12 ?x)
          )
          (and
            (AboveOf11 ?x)
            (BelowOf3 ?x)
          )
          (and
            (AboveOf15 ?x)
            (BelowOf4 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf10 ?x)
          )
          (and
            (AboveOf17 ?x)
            (BelowOf3 ?x)
          )
          (and
            (AboveOf17 ?x)
            (BelowOf6 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf10 ?x)
          )
          (and
            (AboveOf9 ?x)
            (BelowOf8 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf1 ?x)
          )
          (and
            (AboveOf16 ?x)
            (BelowOf4 ?x)
          )
          (and
            (BelowOf6 ?x)
            (AboveOf10 ?x)
          )
          (and
            (AboveOf16 ?x)
            (BelowOf6 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf13 ?x)
          )
          (and
            (AboveOf10 ?x)
            (BelowOf8 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf9 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf3 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf5 ?x)
          )
          (and
            (BelowOf6 ?x)
            (AboveOf12 ?x)
          )
          (and
            (BelowOf9 ?x)
            (AboveOf10 ?x)
          )
          (and
            (BelowOf7 ?x)
            (AboveOf16 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf8 ?x)
          )
          (and
            (BelowOf6 ?x)
            (AboveOf11 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf4 ?x)
          )
          (and
            (AboveOf17 ?x)
            (BelowOf8 ?x)
          )
          (and
            (AboveOf16 ?x)
            (BelowOf1 ?x)
          )
          (and
            (AboveOf12 ?x)
            (BelowOf8 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf8 ?x)
          )
          (and
            (AboveOf11 ?x)
            (BelowOf8 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf1 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf12 ?x)
          )
          (and
            (BelowOf9 ?x)
            (AboveOf11 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf9 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf9 ?x)
          )
          (and
            (AboveOf9 ?x)
            (BelowOf1 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf15 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf10 ?x)
          )
          (and
            (AboveOf10 ?x)
            (BelowOf3 ?x)
          )
          (and
            (AboveOf15 ?x)
            (BelowOf3 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf9 ?x)
          )
          (and
            (AboveOf15 ?x)
            (BelowOf7 ?x)
          )
          (and
            (BelowOf9 ?x)
            (AboveOf17 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf7 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf17 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf11 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf10 ?x)
          )
          (and
            (AboveOf9 ?x)
            (BelowOf3 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf17 ?x)
          )
          (and
            (BelowOf7 ?x)
            (AboveOf10 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf2 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf8 ?x)
          )
        )
        (and
          (Row7 ?x)
        )
      )
      (when
        (or
          (and
            (BelowOf9 ?x)
            (AboveOf12 ?x)
          )
          (and
            (BelowOf3 ?x)
            (AboveOf12 ?x)
          )
          (and
            (BelowOf10 ?x)
            (AboveOf11 ?x)
          )
          (and
            (BelowOf7 ?x)
            (AboveOf12 ?x)
          )
          (and
            (AboveOf16 ?x)
            (BelowOf8 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf11 ?x)
          )
          (and
            (AboveOf15 ?x)
            (BelowOf1 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf14 ?x)
          )
          (and
            (AboveOf17 ?x)
            (BelowOf1 ?x)
          )
          (and
            (BelowOf8 ?x)
            (AboveOf12 ?x)
          )
          (and
            (AboveOf9 ?x)
            (BelowOf3 ?x)
          )
          (and
            (AboveOf17 ?x)
            (BelowOf10 ?x)
          )
          (and
            (AboveOf15 ?x)
            (BelowOf10 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf15 ?x)
          )
          (and
            (AboveOf9 ?x)
            (BelowOf10 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf6 ?x)
          )
          (and
            (BelowOf7 ?x)
            (AboveOf9 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf4 ?x)
          )
          (and
            (AboveOf15 ?x)
            (BelowOf8 ?x)
          )
          (and
            (AboveOf15 ?x)
            (BelowOf9 ?x)
          )
          (and
            (BelowOf3 ?x)
            (AboveOf11 ?x)
          )
          (and
            (BelowOf7 ?x)
            (AboveOf11 ?x)
          )
          (and
            (BelowOf7 ?x)
            (AboveOf17 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf11 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf16 ?x)
          )
          (and
            (AboveOf9 ?x)
            (BelowOf6 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf12 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf16 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf2 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf12 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf9 ?x)
          )
          (and
            (AboveOf16 ?x)
            (BelowOf10 ?x)
          )
          (and
            (AboveOf17 ?x)
            (BelowOf3 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf11 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf16 ?x)
          )
          (and
            (AboveOf16 ?x)
            (BelowOf9 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf6 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf7 ?x)
          )
          (and
            (AboveOf15 ?x)
            (BelowOf6 ?x)
          )
          (and
            (AboveOf16 ?x)
            (BelowOf3 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf17 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf17 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf10 ?x)
          )
          (and
            (AboveOf10 ?x)
            (BelowOf6 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf3 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf12 ?x)
          )
          (and
            (AboveOf11 ?x)
            (BelowOf3 ?x)
          )
          (and
            (BelowOf9 ?x)
            (AboveOf9 ?x)
          )
          (and
            (AboveOf15 ?x)
            (BelowOf4 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf10 ?x)
          )
          (and
            (AboveOf17 ?x)
            (BelowOf6 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf10 ?x)
          )
          (and
            (AboveOf9 ?x)
            (BelowOf8 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf1 ?x)
          )
          (and
            (AboveOf16 ?x)
            (BelowOf4 ?x)
          )
          (and
            (BelowOf6 ?x)
            (AboveOf10 ?x)
          )
          (and
            (AboveOf16 ?x)
            (BelowOf6 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf13 ?x)
          )
          (and
            (AboveOf10 ?x)
            (BelowOf8 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf9 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf10 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf5 ?x)
          )
          (and
            (BelowOf6 ?x)
            (AboveOf12 ?x)
          )
          (and
            (BelowOf9 ?x)
            (AboveOf10 ?x)
          )
          (and
            (BelowOf7 ?x)
            (AboveOf16 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf8 ?x)
          )
          (and
            (BelowOf6 ?x)
            (AboveOf11 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf4 ?x)
          )
          (and
            (AboveOf17 ?x)
            (BelowOf8 ?x)
          )
          (and
            (AboveOf16 ?x)
            (BelowOf1 ?x)
          )
          (and
            (AboveOf12 ?x)
            (BelowOf8 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf8 ?x)
          )
          (and
            (AboveOf11 ?x)
            (BelowOf8 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf1 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf12 ?x)
          )
          (and
            (BelowOf9 ?x)
            (AboveOf11 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf9 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf9 ?x)
          )
          (and
            (AboveOf9 ?x)
            (BelowOf1 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf15 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf10 ?x)
          )
          (and
            (AboveOf10 ?x)
            (BelowOf3 ?x)
          )
          (and
            (BelowOf10 ?x)
            (AboveOf12 ?x)
          )
          (and
            (AboveOf15 ?x)
            (BelowOf3 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf9 ?x)
          )
          (and
            (AboveOf10 ?x)
            (BelowOf10 ?x)
          )
          (and
            (AboveOf15 ?x)
            (BelowOf7 ?x)
          )
          (and
            (BelowOf9 ?x)
            (AboveOf17 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf7 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf17 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf11 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf10 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf17 ?x)
          )
          (and
            (BelowOf7 ?x)
            (AboveOf10 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf2 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf3 ?x)
          )
        )
        (and
          (Row8 ?x)
        )
      )
      (when
        (or
          (and
            (BelowOf9 ?x)
            (AboveOf12 ?x)
          )
          (and
            (BelowOf11 ?x)
            (AboveOf12 ?x)
          )
          (and
            (BelowOf3 ?x)
            (AboveOf12 ?x)
          )
          (and
            (BelowOf10 ?x)
            (AboveOf11 ?x)
          )
          (and
            (BelowOf7 ?x)
            (AboveOf12 ?x)
          )
          (and
            (AboveOf16 ?x)
            (BelowOf8 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf11 ?x)
          )
          (and
            (AboveOf15 ?x)
            (BelowOf1 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf14 ?x)
          )
          (and
            (BelowOf8 ?x)
            (AboveOf12 ?x)
          )
          (and
            (AboveOf17 ?x)
            (BelowOf10 ?x)
          )
          (and
            (AboveOf15 ?x)
            (BelowOf10 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf15 ?x)
          )
          (and
            (AboveOf17 ?x)
            (BelowOf1 ?x)
          )
          (and
            (BelowOf11 ?x)
            (AboveOf11 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf6 ?x)
          )
          (and
            (AboveOf10 ?x)
            (BelowOf11 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf4 ?x)
          )
          (and
            (AboveOf15 ?x)
            (BelowOf8 ?x)
          )
          (and
            (AboveOf15 ?x)
            (BelowOf9 ?x)
          )
          (and
            (BelowOf3 ?x)
            (AboveOf11 ?x)
          )
          (and
            (BelowOf7 ?x)
            (AboveOf11 ?x)
          )
          (and
            (BelowOf7 ?x)
            (AboveOf17 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf11 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf12 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf16 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf12 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf16 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf2 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf11 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf11 ?x)
          )
          (and
            (AboveOf16 ?x)
            (BelowOf10 ?x)
          )
          (and
            (AboveOf17 ?x)
            (BelowOf3 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf11 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf16 ?x)
          )
          (and
            (AboveOf16 ?x)
            (BelowOf9 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf6 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf7 ?x)
          )
          (and
            (AboveOf15 ?x)
            (BelowOf6 ?x)
          )
          (and
            (AboveOf16 ?x)
            (BelowOf3 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf17 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf17 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf10 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf3 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf12 ?x)
          )
          (and
            (AboveOf11 ?x)
            (BelowOf3 ?x)
          )
          (and
            (AboveOf15 ?x)
            (BelowOf4 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf10 ?x)
          )
          (and
            (AboveOf17 ?x)
            (BelowOf6 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf10 ?x)
          )
          (and
            (AboveOf17 ?x)
            (BelowOf11 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf1 ?x)
          )
          (and
            (AboveOf16 ?x)
            (BelowOf4 ?x)
          )
          (and
            (AboveOf15 ?x)
            (BelowOf11 ?x)
          )
          (and
            (BelowOf6 ?x)
            (AboveOf10 ?x)
          )
          (and
            (AboveOf16 ?x)
            (BelowOf6 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf13 ?x)
          )
          (and
            (AboveOf10 ?x)
            (BelowOf8 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf9 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf10 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf5 ?x)
          )
          (and
            (BelowOf6 ?x)
            (AboveOf12 ?x)
          )
          (and
            (BelowOf9 ?x)
            (AboveOf10 ?x)
          )
          (and
            (BelowOf7 ?x)
            (AboveOf16 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf8 ?x)
          )
          (and
            (BelowOf6 ?x)
            (AboveOf11 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf4 ?x)
          )
          (and
            (AboveOf17 ?x)
            (BelowOf8 ?x)
          )
          (and
            (AboveOf16 ?x)
            (BelowOf11 ?x)
          )
          (and
            (AboveOf16 ?x)
            (BelowOf1 ?x)
          )
          (and
            (AboveOf12 ?x)
            (BelowOf8 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf8 ?x)
          )
          (and
            (AboveOf11 ?x)
            (BelowOf8 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf1 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf12 ?x)
          )
          (and
            (BelowOf9 ?x)
            (AboveOf11 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf9 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf15 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf10 ?x)
          )
          (and
            (AboveOf10 ?x)
            (BelowOf3 ?x)
          )
          (and
            (BelowOf10 ?x)
            (AboveOf12 ?x)
          )
          (and
            (AboveOf15 ?x)
            (BelowOf3 ?x)
          )
          (and
            (AboveOf10 ?x)
            (BelowOf10 ?x)
          )
          (and
            (AboveOf15 ?x)
            (BelowOf7 ?x)
          )
          (and
            (BelowOf9 ?x)
            (AboveOf17 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf7 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf17 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf11 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf10 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf17 ?x)
          )
          (and
            (BelowOf7 ?x)
            (AboveOf10 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf2 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf3 ?x)
          )
        )
        (and
          (Row9 ?x)
        )
      )
      (when
        (or
          (and
            (BelowOf9 ?x)
            (AboveOf12 ?x)
          )
          (and
            (BelowOf11 ?x)
            (AboveOf12 ?x)
          )
          (and
            (BelowOf3 ?x)
            (AboveOf12 ?x)
          )
          (and
            (BelowOf10 ?x)
            (AboveOf11 ?x)
          )
          (and
            (BelowOf7 ?x)
            (AboveOf12 ?x)
          )
          (and
            (AboveOf16 ?x)
            (BelowOf8 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf11 ?x)
          )
          (and
            (AboveOf16 ?x)
            (BelowOf12 ?x)
          )
          (and
            (AboveOf15 ?x)
            (BelowOf1 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf14 ?x)
          )
          (and
            (BelowOf8 ?x)
            (AboveOf12 ?x)
          )
          (and
            (AboveOf17 ?x)
            (BelowOf10 ?x)
          )
          (and
            (AboveOf15 ?x)
            (BelowOf10 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf15 ?x)
          )
          (and
            (BelowOf12 ?x)
            (AboveOf12 ?x)
          )
          (and
            (AboveOf17 ?x)
            (BelowOf1 ?x)
          )
          (and
            (BelowOf11 ?x)
            (AboveOf11 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf6 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf4 ?x)
          )
          (and
            (AboveOf15 ?x)
            (BelowOf8 ?x)
          )
          (and
            (AboveOf15 ?x)
            (BelowOf9 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf12 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf3 ?x)
          )
          (and
            (BelowOf7 ?x)
            (AboveOf11 ?x)
          )
          (and
            (BelowOf7 ?x)
            (AboveOf17 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf11 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf12 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf16 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf12 ?x)
          )
          (and
            (AboveOf11 ?x)
            (BelowOf12 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf11 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf2 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf11 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf11 ?x)
          )
          (and
            (AboveOf16 ?x)
            (BelowOf10 ?x)
          )
          (and
            (AboveOf17 ?x)
            (BelowOf3 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf11 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf16 ?x)
          )
          (and
            (AboveOf16 ?x)
            (BelowOf9 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf6 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf7 ?x)
          )
          (and
            (AboveOf15 ?x)
            (BelowOf6 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf17 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf17 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf10 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf3 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf12 ?x)
          )
          (and
            (AboveOf17 ?x)
            (BelowOf12 ?x)
          )
          (and
            (AboveOf11 ?x)
            (BelowOf3 ?x)
          )
          (and
            (AboveOf15 ?x)
            (BelowOf4 ?x)
          )
          (and
            (AboveOf17 ?x)
            (BelowOf6 ?x)
          )
          (and
            (AboveOf17 ?x)
            (BelowOf11 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf1 ?x)
          )
          (and
            (AboveOf16 ?x)
            (BelowOf4 ?x)
          )
          (and
            (AboveOf15 ?x)
            (BelowOf11 ?x)
          )
          (and
            (AboveOf16 ?x)
            (BelowOf6 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf13 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf9 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf10 ?x)
          )
          (and
            (AboveOf15 ?x)
            (BelowOf12 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf5 ?x)
          )
          (and
            (BelowOf6 ?x)
            (AboveOf12 ?x)
          )
          (and
            (BelowOf7 ?x)
            (AboveOf16 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf8 ?x)
          )
          (and
            (BelowOf6 ?x)
            (AboveOf11 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf4 ?x)
          )
          (and
            (AboveOf17 ?x)
            (BelowOf8 ?x)
          )
          (and
            (AboveOf16 ?x)
            (BelowOf11 ?x)
          )
          (and
            (AboveOf16 ?x)
            (BelowOf1 ?x)
          )
          (and
            (AboveOf12 ?x)
            (BelowOf8 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf8 ?x)
          )
          (and
            (AboveOf11 ?x)
            (BelowOf8 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf1 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf12 ?x)
          )
          (and
            (BelowOf9 ?x)
            (AboveOf11 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf9 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf15 ?x)
          )
          (and
            (BelowOf10 ?x)
            (AboveOf12 ?x)
          )
          (and
            (AboveOf15 ?x)
            (BelowOf3 ?x)
          )
          (and
            (AboveOf15 ?x)
            (BelowOf7 ?x)
          )
          (and
            (BelowOf9 ?x)
            (AboveOf17 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf12 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf7 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf17 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf16 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf17 ?x)
          )
          (and
            (AboveOf16 ?x)
            (BelowOf3 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf2 ?x)
          )
        )
        (and
          (Row10 ?x)
        )
      )
      (when
        (or
          (and
            (BelowOf5 ?x)
            (AboveOf14 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf8 ?x)
          )
          (and
            (AboveOf15 ?x)
            (BelowOf8 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf10 ?x)
          )
          (and
            (AboveOf15 ?x)
            (BelowOf9 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf12 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf3 ?x)
          )
          (and
            (AboveOf16 ?x)
            (BelowOf12 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf2 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf3 ?x)
          )
          (and
            (BelowOf9 ?x)
            (AboveOf12 ?x)
          )
          (and
            (BelowOf11 ?x)
            (AboveOf12 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf12 ?x)
          )
          (and
            (BelowOf3 ?x)
            (AboveOf12 ?x)
          )
          (and
            (AboveOf17 ?x)
            (BelowOf12 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf12 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf16 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf9 ?x)
          )
          (and
            (AboveOf15 ?x)
            (BelowOf4 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf6 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf11 ?x)
          )
          (and
            (BelowOf7 ?x)
            (AboveOf12 ?x)
          )
          (and
            (BelowOf13 ?x)
            (AboveOf16 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf15 ?x)
          )
          (and
            (AboveOf16 ?x)
            (BelowOf8 ?x)
          )
          (and
            (AboveOf17 ?x)
            (BelowOf6 ?x)
          )
          (and
            (AboveOf16 ?x)
            (BelowOf10 ?x)
          )
          (and
            (BelowOf10 ?x)
            (AboveOf12 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf4 ?x)
          )
          (and
            (AboveOf17 ?x)
            (BelowOf11 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf1 ?x)
          )
          (and
            (BelowOf13 ?x)
            (AboveOf17 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf12 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf1 ?x)
          )
          (and
            (BelowOf13 ?x)
            (AboveOf12 ?x)
          )
          (and
            (AboveOf16 ?x)
            (BelowOf4 ?x)
          )
          (and
            (AboveOf15 ?x)
            (BelowOf11 ?x)
          )
          (and
            (AboveOf15 ?x)
            (BelowOf1 ?x)
          )
          (and
            (BelowOf9 ?x)
            (AboveOf17 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf17 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf12 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf7 ?x)
          )
          (and
            (AboveOf15 ?x)
            (BelowOf6 ?x)
          )
          (and
            (BelowOf13 ?x)
            (AboveOf15 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf17 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf16 ?x)
          )
          (and
            (AboveOf15 ?x)
            (BelowOf3 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf13 ?x)
          )
          (and
            (BelowOf6 ?x)
            (AboveOf12 ?x)
          )
          (and
            (AboveOf17 ?x)
            (BelowOf10 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf9 ?x)
          )
          (and
            (AboveOf16 ?x)
            (BelowOf3 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf12 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf11 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf2 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf10 ?x)
          )
          (and
            (AboveOf17 ?x)
            (BelowOf3 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf15 ?x)
          )
          (and
            (AboveOf16 ?x)
            (BelowOf6 ?x)
          )
          (and
            (AboveOf17 ?x)
            (BelowOf1 ?x)
          )
          (and
            (AboveOf15 ?x)
            (BelowOf10 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf16 ?x)
          )
          (and
            (AboveOf15 ?x)
            (BelowOf12 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf5 ?x)
          )
          (and
            (AboveOf16 ?x)
            (BelowOf9 ?x)
          )
          (and
            (AboveOf15 ?x)
            (BelowOf7 ?x)
          )
          (and
            (AboveOf16 ?x)
            (BelowOf1 ?x)
          )
          (and
            (BelowOf13 ?x)
            (AboveOf13 ?x)
          )
          (and
            (BelowOf7 ?x)
            (AboveOf16 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf8 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf7 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf6 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf17 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf4 ?x)
          )
          (and
            (AboveOf17 ?x)
            (BelowOf8 ?x)
          )
          (and
            (AboveOf16 ?x)
            (BelowOf11 ?x)
          )
          (and
            (AboveOf12 ?x)
            (BelowOf12 ?x)
          )
          (and
            (BelowOf13 ?x)
            (AboveOf14 ?x)
          )
          (and
            (AboveOf12 ?x)
            (BelowOf8 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf17 ?x)
          )
          (and
            (BelowOf7 ?x)
            (AboveOf17 ?x)
          )
        )
        (and
          (Row11 ?x)
        )
      )
      (when
        (or
          (and
            (AboveOf14 ?x)
            (BelowOf8 ?x)
          )
          (and
            (AboveOf15 ?x)
            (BelowOf8 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf10 ?x)
          )
          (and
            (AboveOf15 ?x)
            (BelowOf9 ?x)
          )
          (and
            (AboveOf16 ?x)
            (BelowOf4 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf12 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf3 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf2 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf3 ?x)
          )
          (and
            (BelowOf7 ?x)
            (AboveOf17 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf14 ?x)
          )
          (and
            (AboveOf17 ?x)
            (BelowOf12 ?x)
          )
          (and
            (BelowOf14 ?x)
            (AboveOf16 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf16 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf9 ?x)
          )
          (and
            (AboveOf15 ?x)
            (BelowOf4 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf6 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf11 ?x)
          )
          (and
            (AboveOf16 ?x)
            (BelowOf10 ?x)
          )
          (and
            (BelowOf13 ?x)
            (AboveOf16 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf15 ?x)
          )
          (and
            (AboveOf16 ?x)
            (BelowOf8 ?x)
          )
          (and
            (AboveOf17 ?x)
            (BelowOf6 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf4 ?x)
          )
          (and
            (AboveOf17 ?x)
            (BelowOf11 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf14 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf1 ?x)
          )
          (and
            (AboveOf16 ?x)
            (BelowOf12 ?x)
          )
          (and
            (AboveOf16 ?x)
            (BelowOf6 ?x)
          )
          (and
            (AboveOf15 ?x)
            (BelowOf3 ?x)
          )
          (and
            (BelowOf13 ?x)
            (AboveOf17 ?x)
          )
          (and
            (AboveOf15 ?x)
            (BelowOf11 ?x)
          )
          (and
            (AboveOf15 ?x)
            (BelowOf1 ?x)
          )
          (and
            (BelowOf9 ?x)
            (AboveOf17 ?x)
          )
          (and
            (BelowOf14 ?x)
            (AboveOf17 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf12 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf7 ?x)
          )
          (and
            (AboveOf15 ?x)
            (BelowOf6 ?x)
          )
          (and
            (BelowOf13 ?x)
            (AboveOf15 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf17 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf16 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf13 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf1 ?x)
          )
          (and
            (AboveOf17 ?x)
            (BelowOf10 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf9 ?x)
          )
          (and
            (AboveOf16 ?x)
            (BelowOf3 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf11 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf2 ?x)
          )
          (and
            (BelowOf14 ?x)
            (AboveOf15 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf10 ?x)
          )
          (and
            (AboveOf17 ?x)
            (BelowOf3 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf15 ?x)
          )
          (and
            (AboveOf17 ?x)
            (BelowOf1 ?x)
          )
          (and
            (AboveOf15 ?x)
            (BelowOf10 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf14 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf16 ?x)
          )
          (and
            (AboveOf15 ?x)
            (BelowOf12 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf5 ?x)
          )
          (and
            (AboveOf16 ?x)
            (BelowOf9 ?x)
          )
          (and
            (AboveOf15 ?x)
            (BelowOf7 ?x)
          )
          (and
            (BelowOf7 ?x)
            (AboveOf16 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf8 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf7 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf6 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf17 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf4 ?x)
          )
          (and
            (AboveOf17 ?x)
            (BelowOf8 ?x)
          )
          (and
            (AboveOf16 ?x)
            (BelowOf11 ?x)
          )
          (and
            (AboveOf16 ?x)
            (BelowOf1 ?x)
          )
          (and
            (BelowOf13 ?x)
            (AboveOf14 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf17 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf17 ?x)
          )
        )
        (and
          (Row12 ?x)
        )
      )
      (when
        (or
          (and
            (BelowOf5 ?x)
            (AboveOf14 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf8 ?x)
          )
          (and
            (AboveOf15 ?x)
            (BelowOf8 ?x)
          )
          (and
            (AboveOf15 ?x)
            (BelowOf1 ?x)
          )
          (and
            (AboveOf15 ?x)
            (BelowOf9 ?x)
          )
          (and
            (BelowOf15 ?x)
            (AboveOf17 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf12 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf3 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf2 ?x)
          )
          (and
            (BelowOf7 ?x)
            (AboveOf17 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf11 ?x)
          )
          (and
            (AboveOf17 ?x)
            (BelowOf12 ?x)
          )
          (and
            (BelowOf14 ?x)
            (AboveOf16 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf16 ?x)
          )
          (and
            (AboveOf15 ?x)
            (BelowOf4 ?x)
          )
          (and
            (AboveOf16 ?x)
            (BelowOf10 ?x)
          )
          (and
            (BelowOf13 ?x)
            (AboveOf16 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf15 ?x)
          )
          (and
            (AboveOf16 ?x)
            (BelowOf8 ?x)
          )
          (and
            (AboveOf17 ?x)
            (BelowOf6 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf4 ?x)
          )
          (and
            (AboveOf17 ?x)
            (BelowOf11 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf1 ?x)
          )
          (and
            (AboveOf16 ?x)
            (BelowOf12 ?x)
          )
          (and
            (AboveOf15 ?x)
            (BelowOf3 ?x)
          )
          (and
            (BelowOf13 ?x)
            (AboveOf17 ?x)
          )
          (and
            (AboveOf15 ?x)
            (BelowOf11 ?x)
          )
          (and
            (BelowOf9 ?x)
            (AboveOf17 ?x)
          )
          (and
            (BelowOf14 ?x)
            (AboveOf17 ?x)
          )
          (and
            (AboveOf16 ?x)
            (BelowOf6 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf7 ?x)
          )
          (and
            (AboveOf15 ?x)
            (BelowOf6 ?x)
          )
          (and
            (BelowOf13 ?x)
            (AboveOf15 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf17 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf16 ?x)
          )
          (and
            (AboveOf17 ?x)
            (BelowOf10 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf9 ?x)
          )
          (and
            (AboveOf16 ?x)
            (BelowOf3 ?x)
          )
          (and
            (BelowOf15 ?x)
            (AboveOf16 ?x)
          )
          (and
            (AboveOf16 ?x)
            (BelowOf4 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf14 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf10 ?x)
          )
          (and
            (AboveOf17 ?x)
            (BelowOf3 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf15 ?x)
          )
          (and
            (AboveOf15 ?x)
            (BelowOf15 ?x)
          )
          (and
            (AboveOf17 ?x)
            (BelowOf1 ?x)
          )
          (and
            (AboveOf15 ?x)
            (BelowOf10 ?x)
          )
          (and
            (BelowOf14 ?x)
            (AboveOf15 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf16 ?x)
          )
          (and
            (AboveOf15 ?x)
            (BelowOf12 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf15 ?x)
          )
          (and
            (AboveOf16 ?x)
            (BelowOf9 ?x)
          )
          (and
            (AboveOf15 ?x)
            (BelowOf7 ?x)
          )
          (and
            (BelowOf7 ?x)
            (AboveOf16 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf6 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf17 ?x)
          )
          (and
            (AboveOf17 ?x)
            (BelowOf8 ?x)
          )
          (and
            (AboveOf16 ?x)
            (BelowOf11 ?x)
          )
          (and
            (AboveOf16 ?x)
            (BelowOf1 ?x)
          )
          (and
            (BelowOf13 ?x)
            (AboveOf14 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf17 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf17 ?x)
          )
        )
        (and
          (Row13 ?x)
        )
      )
      (when
        (or
          (and
            (AboveOf15 ?x)
            (BelowOf8 ?x)
          )
          (and
            (AboveOf15 ?x)
            (BelowOf1 ?x)
          )
          (and
            (AboveOf15 ?x)
            (BelowOf9 ?x)
          )
          (and
            (BelowOf15 ?x)
            (AboveOf17 ?x)
          )
          (and
            (AboveOf15 ?x)
            (BelowOf6 ?x)
          )
          (and
            (BelowOf7 ?x)
            (AboveOf17 ?x)
          )
          (and
            (AboveOf17 ?x)
            (BelowOf12 ?x)
          )
          (and
            (AboveOf15 ?x)
            (BelowOf16 ?x)
          )
          (and
            (BelowOf14 ?x)
            (AboveOf16 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf16 ?x)
          )
          (and
            (AboveOf15 ?x)
            (BelowOf4 ?x)
          )
          (and
            (AboveOf16 ?x)
            (BelowOf10 ?x)
          )
          (and
            (BelowOf13 ?x)
            (AboveOf16 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf15 ?x)
          )
          (and
            (AboveOf16 ?x)
            (BelowOf8 ?x)
          )
          (and
            (AboveOf17 ?x)
            (BelowOf6 ?x)
          )
          (and
            (AboveOf17 ?x)
            (BelowOf11 ?x)
          )
          (and
            (AboveOf16 ?x)
            (BelowOf12 ?x)
          )
          (and
            (AboveOf15 ?x)
            (BelowOf3 ?x)
          )
          (and
            (BelowOf13 ?x)
            (AboveOf17 ?x)
          )
          (and
            (AboveOf15 ?x)
            (BelowOf11 ?x)
          )
          (and
            (BelowOf16 ?x)
            (AboveOf16 ?x)
          )
          (and
            (BelowOf9 ?x)
            (AboveOf17 ?x)
          )
          (and
            (BelowOf14 ?x)
            (AboveOf17 ?x)
          )
          (and
            (AboveOf16 ?x)
            (BelowOf6 ?x)
          )
          (and
            (BelowOf16 ?x)
            (AboveOf17 ?x)
          )
          (and
            (BelowOf13 ?x)
            (AboveOf15 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf17 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf16 ?x)
          )
          (and
            (AboveOf17 ?x)
            (BelowOf10 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf17 ?x)
          )
          (and
            (AboveOf16 ?x)
            (BelowOf3 ?x)
          )
          (and
            (BelowOf15 ?x)
            (AboveOf16 ?x)
          )
          (and
            (AboveOf16 ?x)
            (BelowOf4 ?x)
          )
          (and
            (AboveOf17 ?x)
            (BelowOf3 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf15 ?x)
          )
          (and
            (AboveOf15 ?x)
            (BelowOf15 ?x)
          )
          (and
            (AboveOf17 ?x)
            (BelowOf1 ?x)
          )
          (and
            (AboveOf15 ?x)
            (BelowOf10 ?x)
          )
          (and
            (BelowOf14 ?x)
            (AboveOf15 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf16 ?x)
          )
          (and
            (AboveOf15 ?x)
            (BelowOf12 ?x)
          )
          (and
            (AboveOf16 ?x)
            (BelowOf9 ?x)
          )
          (and
            (AboveOf15 ?x)
            (BelowOf7 ?x)
          )
          (and
            (BelowOf7 ?x)
            (AboveOf16 ?x)
          )
          (and
            (AboveOf17 ?x)
            (BelowOf8 ?x)
          )
          (and
            (AboveOf16 ?x)
            (BelowOf11 ?x)
          )
          (and
            (AboveOf16 ?x)
            (BelowOf1 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf17 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf17 ?x)
          )
        )
        (and
          (Row14 ?x)
        )
      )
      (when
        (or
          (and
            (AboveOf16 ?x)
            (BelowOf6 ?x)
          )
          (and
            (BelowOf15 ?x)
            (AboveOf17 ?x)
          )
          (and
            (BelowOf7 ?x)
            (AboveOf17 ?x)
          )
          (and
            (AboveOf17 ?x)
            (BelowOf12 ?x)
          )
          (and
            (BelowOf14 ?x)
            (AboveOf16 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf16 ?x)
          )
          (and
            (BelowOf17 ?x)
            (AboveOf17 ?x)
          )
          (and
            (BelowOf13 ?x)
            (AboveOf16 ?x)
          )
          (and
            (AboveOf16 ?x)
            (BelowOf8 ?x)
          )
          (and
            (AboveOf17 ?x)
            (BelowOf6 ?x)
          )
          (and
            (AboveOf17 ?x)
            (BelowOf11 ?x)
          )
          (and
            (AboveOf16 ?x)
            (BelowOf12 ?x)
          )
          (and
            (BelowOf13 ?x)
            (AboveOf17 ?x)
          )
          (and
            (BelowOf16 ?x)
            (AboveOf16 ?x)
          )
          (and
            (BelowOf9 ?x)
            (AboveOf17 ?x)
          )
          (and
            (BelowOf14 ?x)
            (AboveOf17 ?x)
          )
          (and
            (BelowOf16 ?x)
            (AboveOf17 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf17 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf16 ?x)
          )
          (and
            (AboveOf17 ?x)
            (BelowOf10 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf17 ?x)
          )
          (and
            (AboveOf16 ?x)
            (BelowOf3 ?x)
          )
          (and
            (BelowOf15 ?x)
            (AboveOf16 ?x)
          )
          (and
            (AboveOf17 ?x)
            (BelowOf3 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf16 ?x)
          )
          (and
            (AboveOf17 ?x)
            (BelowOf1 ?x)
          )
          (and
            (AboveOf16 ?x)
            (BelowOf17 ?x)
          )
          (and
            (AboveOf16 ?x)
            (BelowOf10 ?x)
          )
          (and
            (AboveOf16 ?x)
            (BelowOf9 ?x)
          )
          (and
            (BelowOf7 ?x)
            (AboveOf16 ?x)
          )
          (and
            (AboveOf17 ?x)
            (BelowOf8 ?x)
          )
          (and
            (AboveOf16 ?x)
            (BelowOf11 ?x)
          )
          (and
            (AboveOf16 ?x)
            (BelowOf1 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf17 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf17 ?x)
          )
        )
        (and
          (Row15 ?x)
        )
      )
      (when
        (or
          (and
            (BelowOf9 ?x)
            (AboveOf17 ?x)
          )
          (and
            (BelowOf14 ?x)
            (AboveOf17 ?x)
          )
          (and
            (BelowOf16 ?x)
            (AboveOf17 ?x)
          )
          (and
            (BelowOf17 ?x)
            (AboveOf17 ?x)
          )
          (and
            (AboveOf17 ?x)
            (BelowOf1 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf17 ?x)
          )
          (and
            (AboveOf17 ?x)
            (BelowOf18 ?x)
          )
          (and
            (AboveOf17 ?x)
            (BelowOf6 ?x)
          )
          (and
            (BelowOf15 ?x)
            (AboveOf17 ?x)
          )
          (and
            (AboveOf17 ?x)
            (BelowOf11 ?x)
          )
          (and
            (AboveOf17 ?x)
            (BelowOf10 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf17 ?x)
          )
          (and
            (BelowOf7 ?x)
            (AboveOf17 ?x)
          )
          (and
            (AboveOf17 ?x)
            (BelowOf8 ?x)
          )
          (and
            (BelowOf13 ?x)
            (AboveOf17 ?x)
          )
          (and
            (AboveOf17 ?x)
            (BelowOf12 ?x)
          )
          (and
            (AboveOf17 ?x)
            (BelowOf3 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf17 ?x)
          )
        )
        (and
          (Row16 ?x)
        )
      )
    )
  )
  (:action moveLeft
    :parameters ( ?x )
    :precondition ( and
      (not (CheckConsistency))
      (not (Error))
      (or
        (LeftOf3 ?x)
        (RightOf7 ?x)
        (RightOf8 ?x)
        (RightOf15 ?x)
        (LeftOf2 ?x)
        (LeftOf9 ?x)
        (RightOf2 ?x)
        (RightOf0 ?x)
        (LeftOf8 ?x)
        (LeftOf7 ?x)
        (LeftOf14 ?x)
        (LeftOf5 ?x)
        (RightOf1 ?x)
        (LeftOf12 ?x)
        (Columns ?x)
        (RightOf13 ?x)
        (RightOf14 ?x)
        (RightOf6 ?x)
        (RightOf9 ?x)
        (LeftOf6 ?x)
        (RightOf10 ?x)
        (RightOf11 ?x)
        (RightOf17 ?x)
        (LeftOf11 ?x)
        (RightOf5 ?x)
        (LeftOf10 ?x)
        (LeftOf15 ?x)
        (LeftOf4 ?x)
        (LeftOf16 ?x)
        (RightOf16 ?x)
        (RightOf4 ?x)
        (LeftOf1 ?x)
        (RightOf12 ?x)
        (LeftOf13 ?x)
        (LeftOf17 ?x)
        (LeftOf18 ?x)
        (RightOf3 ?x)
      )
    )
    :effect ( and
      (CheckConsistency)
      (when
        (or
          (LeftOf2 ?x)
          (LeftOf1 ?x)
        )
        (and
          (LeftOf1 ?x)
        )
      )
      (when
        (or
          (LeftOf2 ?x)
          (LeftOf3 ?x)
          (LeftOf1 ?x)
        )
        (and
          (LeftOf2 ?x)
        )
      )
      (when
        (or
          (LeftOf2 ?x)
          (LeftOf4 ?x)
          (LeftOf3 ?x)
          (LeftOf1 ?x)
        )
        (and
          (LeftOf3 ?x)
        )
      )
      (when
        (or
          (LeftOf2 ?x)
          (LeftOf4 ?x)
          (LeftOf3 ?x)
          (LeftOf1 ?x)
          (LeftOf5 ?x)
        )
        (and
          (LeftOf4 ?x)
        )
      )
      (when
        (or
          (LeftOf4 ?x)
          (LeftOf3 ?x)
          (LeftOf5 ?x)
          (LeftOf6 ?x)
          (LeftOf1 ?x)
          (LeftOf2 ?x)
        )
        (and
          (LeftOf5 ?x)
        )
      )
      (when
        (or
          (LeftOf4 ?x)
          (LeftOf3 ?x)
          (LeftOf5 ?x)
          (LeftOf6 ?x)
          (LeftOf1 ?x)
          (LeftOf7 ?x)
          (LeftOf2 ?x)
        )
        (and
          (LeftOf6 ?x)
        )
      )
      (when
        (or
          (LeftOf8 ?x)
          (LeftOf4 ?x)
          (LeftOf3 ?x)
          (LeftOf5 ?x)
          (LeftOf6 ?x)
          (LeftOf1 ?x)
          (LeftOf7 ?x)
          (LeftOf2 ?x)
        )
        (and
          (LeftOf7 ?x)
        )
      )
      (when
        (or
          (LeftOf8 ?x)
          (LeftOf4 ?x)
          (LeftOf3 ?x)
          (LeftOf5 ?x)
          (LeftOf6 ?x)
          (LeftOf1 ?x)
          (LeftOf7 ?x)
          (LeftOf2 ?x)
          (LeftOf9 ?x)
        )
        (and
          (LeftOf8 ?x)
        )
      )
      (when
        (or
          (LeftOf8 ?x)
          (LeftOf4 ?x)
          (LeftOf3 ?x)
          (LeftOf5 ?x)
          (LeftOf6 ?x)
          (LeftOf1 ?x)
          (LeftOf7 ?x)
          (LeftOf2 ?x)
          (LeftOf9 ?x)
          (LeftOf10 ?x)
        )
        (and
          (LeftOf9 ?x)
        )
      )
      (when
        (or
          (LeftOf8 ?x)
          (LeftOf4 ?x)
          (LeftOf3 ?x)
          (LeftOf5 ?x)
          (LeftOf6 ?x)
          (LeftOf1 ?x)
          (LeftOf7 ?x)
          (LeftOf2 ?x)
          (LeftOf9 ?x)
          (LeftOf11 ?x)
          (LeftOf10 ?x)
        )
        (and
          (LeftOf10 ?x)
        )
      )
      (when
        (or
          (LeftOf8 ?x)
          (LeftOf4 ?x)
          (LeftOf3 ?x)
          (LeftOf5 ?x)
          (LeftOf6 ?x)
          (LeftOf12 ?x)
          (LeftOf1 ?x)
          (LeftOf7 ?x)
          (LeftOf2 ?x)
          (LeftOf9 ?x)
          (LeftOf11 ?x)
          (LeftOf10 ?x)
        )
        (and
          (LeftOf11 ?x)
        )
      )
      (when
        (or
          (LeftOf8 ?x)
          (LeftOf7 ?x)
          (LeftOf4 ?x)
          (LeftOf3 ?x)
          (LeftOf5 ?x)
          (LeftOf6 ?x)
          (LeftOf12 ?x)
          (LeftOf1 ?x)
          (LeftOf13 ?x)
          (LeftOf2 ?x)
          (LeftOf9 ?x)
          (LeftOf11 ?x)
          (LeftOf10 ?x)
        )
        (and
          (LeftOf12 ?x)
        )
      )
      (when
        (or
          (LeftOf8 ?x)
          (LeftOf7 ?x)
          (LeftOf4 ?x)
          (LeftOf3 ?x)
          (LeftOf14 ?x)
          (LeftOf5 ?x)
          (LeftOf6 ?x)
          (LeftOf12 ?x)
          (LeftOf1 ?x)
          (LeftOf13 ?x)
          (LeftOf2 ?x)
          (LeftOf9 ?x)
          (LeftOf11 ?x)
          (LeftOf10 ?x)
        )
        (and
          (LeftOf13 ?x)
        )
      )
      (when
        (or
          (LeftOf8 ?x)
          (LeftOf7 ?x)
          (LeftOf4 ?x)
          (LeftOf3 ?x)
          (LeftOf14 ?x)
          (LeftOf5 ?x)
          (LeftOf6 ?x)
          (LeftOf12 ?x)
          (LeftOf1 ?x)
          (LeftOf13 ?x)
          (LeftOf2 ?x)
          (LeftOf9 ?x)
          (LeftOf11 ?x)
          (LeftOf10 ?x)
          (LeftOf15 ?x)
        )
        (and
          (LeftOf14 ?x)
        )
      )
      (when
        (or
          (LeftOf8 ?x)
          (LeftOf7 ?x)
          (LeftOf4 ?x)
          (LeftOf3 ?x)
          (LeftOf14 ?x)
          (LeftOf16 ?x)
          (LeftOf5 ?x)
          (LeftOf6 ?x)
          (LeftOf12 ?x)
          (LeftOf1 ?x)
          (LeftOf13 ?x)
          (LeftOf2 ?x)
          (LeftOf9 ?x)
          (LeftOf11 ?x)
          (LeftOf10 ?x)
          (LeftOf15 ?x)
        )
        (and
          (LeftOf15 ?x)
        )
      )
      (when
        (or
          (LeftOf8 ?x)
          (LeftOf7 ?x)
          (LeftOf4 ?x)
          (LeftOf3 ?x)
          (LeftOf14 ?x)
          (LeftOf16 ?x)
          (LeftOf5 ?x)
          (LeftOf6 ?x)
          (LeftOf12 ?x)
          (LeftOf1 ?x)
          (LeftOf13 ?x)
          (LeftOf2 ?x)
          (LeftOf9 ?x)
          (LeftOf11 ?x)
          (LeftOf17 ?x)
          (LeftOf10 ?x)
          (LeftOf15 ?x)
        )
        (and
          (LeftOf16 ?x)
        )
      )
      (when
        (or
          (LeftOf8 ?x)
          (LeftOf7 ?x)
          (LeftOf4 ?x)
          (LeftOf3 ?x)
          (LeftOf14 ?x)
          (LeftOf16 ?x)
          (LeftOf5 ?x)
          (LeftOf6 ?x)
          (LeftOf12 ?x)
          (LeftOf1 ?x)
          (LeftOf13 ?x)
          (LeftOf2 ?x)
          (LeftOf9 ?x)
          (LeftOf11 ?x)
          (LeftOf17 ?x)
          (LeftOf18 ?x)
          (LeftOf10 ?x)
          (LeftOf15 ?x)
        )
        (and
          (LeftOf17 ?x)
        )
      )
      (when
        (or
          (RightOf6 ?x)
          (RightOf15 ?x)
          (RightOf11 ?x)
          (RightOf7 ?x)
          (RightOf16 ?x)
          (RightOf1 ?x)
          (RightOf4 ?x)
          (RightOf8 ?x)
          (RightOf10 ?x)
          (RightOf12 ?x)
          (RightOf13 ?x)
          (RightOf14 ?x)
          (RightOf17 ?x)
          (RightOf2 ?x)
          (RightOf9 ?x)
          (RightOf5 ?x)
          (RightOf3 ?x)
        )
        (and
          (RightOf0 ?x)
          (not (RightOf1 ?x))
        )
      )
      (when
        (or
          (RightOf6 ?x)
          (RightOf15 ?x)
          (RightOf11 ?x)
          (RightOf7 ?x)
          (RightOf16 ?x)
          (RightOf8 ?x)
          (RightOf4 ?x)
          (RightOf10 ?x)
          (RightOf12 ?x)
          (RightOf13 ?x)
          (RightOf14 ?x)
          (RightOf17 ?x)
          (RightOf2 ?x)
          (RightOf9 ?x)
          (RightOf5 ?x)
          (RightOf3 ?x)
        )
        (and
          (RightOf1 ?x)
          (not (RightOf2 ?x))
        )
      )
      (when
        (or
          (RightOf6 ?x)
          (RightOf15 ?x)
          (RightOf11 ?x)
          (RightOf7 ?x)
          (RightOf16 ?x)
          (RightOf8 ?x)
          (RightOf4 ?x)
          (RightOf10 ?x)
          (RightOf12 ?x)
          (RightOf13 ?x)
          (RightOf14 ?x)
          (RightOf17 ?x)
          (RightOf9 ?x)
          (RightOf5 ?x)
          (RightOf3 ?x)
        )
        (and
          (RightOf2 ?x)
          (not (RightOf3 ?x))
        )
      )
      (when
        (or
          (RightOf6 ?x)
          (RightOf15 ?x)
          (RightOf11 ?x)
          (RightOf7 ?x)
          (RightOf16 ?x)
          (RightOf8 ?x)
          (RightOf4 ?x)
          (RightOf10 ?x)
          (RightOf12 ?x)
          (RightOf13 ?x)
          (RightOf14 ?x)
          (RightOf17 ?x)
          (RightOf9 ?x)
          (RightOf5 ?x)
        )
        (and
          (RightOf3 ?x)
          (not (RightOf4 ?x))
        )
      )
      (when
        (or
          (RightOf6 ?x)
          (RightOf15 ?x)
          (RightOf11 ?x)
          (RightOf7 ?x)
          (RightOf16 ?x)
          (RightOf8 ?x)
          (RightOf10 ?x)
          (RightOf12 ?x)
          (RightOf13 ?x)
          (RightOf14 ?x)
          (RightOf17 ?x)
          (RightOf9 ?x)
          (RightOf5 ?x)
        )
        (and
          (RightOf4 ?x)
          (not (RightOf5 ?x))
        )
      )
      (when
        (or
          (RightOf6 ?x)
          (RightOf15 ?x)
          (RightOf11 ?x)
          (RightOf7 ?x)
          (RightOf16 ?x)
          (RightOf8 ?x)
          (RightOf10 ?x)
          (RightOf12 ?x)
          (RightOf13 ?x)
          (RightOf14 ?x)
          (RightOf17 ?x)
          (RightOf9 ?x)
        )
        (and
          (RightOf5 ?x)
          (not (RightOf6 ?x))
        )
      )
      (when
        (or
          (RightOf11 ?x)
          (RightOf9 ?x)
          (RightOf7 ?x)
          (RightOf16 ?x)
          (RightOf12 ?x)
          (RightOf8 ?x)
          (RightOf10 ?x)
          (RightOf15 ?x)
          (RightOf13 ?x)
          (RightOf14 ?x)
          (RightOf17 ?x)
        )
        (and
          (RightOf6 ?x)
          (not (RightOf7 ?x))
        )
      )
      (when
        (or
          (RightOf11 ?x)
          (RightOf9 ?x)
          (RightOf12 ?x)
          (RightOf16 ?x)
          (RightOf8 ?x)
          (RightOf10 ?x)
          (RightOf15 ?x)
          (RightOf13 ?x)
          (RightOf14 ?x)
          (RightOf17 ?x)
        )
        (and
          (RightOf7 ?x)
          (not (RightOf8 ?x))
        )
      )
      (when
        (or
          (RightOf11 ?x)
          (RightOf9 ?x)
          (RightOf12 ?x)
          (RightOf16 ?x)
          (RightOf10 ?x)
          (RightOf15 ?x)
          (RightOf13 ?x)
          (RightOf14 ?x)
          (RightOf17 ?x)
        )
        (and
          (RightOf8 ?x)
          (not (RightOf9 ?x))
        )
      )
      (when
        (or
          (RightOf11 ?x)
          (RightOf12 ?x)
          (RightOf16 ?x)
          (RightOf10 ?x)
          (RightOf15 ?x)
          (RightOf13 ?x)
          (RightOf14 ?x)
          (RightOf17 ?x)
        )
        (and
          (RightOf9 ?x)
          (not (RightOf10 ?x))
        )
      )
      (when
        (or
          (RightOf11 ?x)
          (RightOf12 ?x)
          (RightOf16 ?x)
          (RightOf15 ?x)
          (RightOf13 ?x)
          (RightOf14 ?x)
          (RightOf17 ?x)
        )
        (and
          (RightOf10 ?x)
          (not (RightOf11 ?x))
        )
      )
      (when
        (or
          (RightOf15 ?x)
          (RightOf16 ?x)
          (RightOf12 ?x)
          (RightOf13 ?x)
          (RightOf14 ?x)
          (RightOf17 ?x)
        )
        (and
          (RightOf11 ?x)
          (not (RightOf12 ?x))
        )
      )
      (when
        (or
          (RightOf15 ?x)
          (RightOf13 ?x)
          (RightOf14 ?x)
          (RightOf17 ?x)
          (RightOf16 ?x)
        )
        (and
          (RightOf12 ?x)
          (not (RightOf13 ?x))
        )
      )
      (when
        (or
          (RightOf15 ?x)
          (RightOf14 ?x)
          (RightOf17 ?x)
          (RightOf16 ?x)
        )
        (and
          (RightOf13 ?x)
          (not (RightOf14 ?x))
        )
      )
      (when
        (or
          (RightOf15 ?x)
          (RightOf17 ?x)
          (RightOf16 ?x)
        )
        (and
          (RightOf14 ?x)
          (not (RightOf15 ?x))
        )
      )
      (when
        (or
          (RightOf17 ?x)
          (RightOf16 ?x)
        )
        (and
          (RightOf15 ?x)
          (not (RightOf16 ?x))
        )
      )
      (when
        (RightOf17 ?x)
        (and
          (RightOf16 ?x)
          (not (RightOf17 ?x))
        )
      )
      (when
        (Column1 ?x)
        (and
          (Column0 ?x)
          (not (Column1 ?x))
        )
      )
      (when
        (Column2 ?x)
        (and
          (Column1 ?x)
          (not (Column2 ?x))
        )
      )
      (when
        (Column3 ?x)
        (and
          (Column2 ?x)
          (not (Column3 ?x))
        )
      )
      (when
        (Column4 ?x)
        (and
          (Column3 ?x)
          (not (Column4 ?x))
        )
      )
      (when
        (Column5 ?x)
        (and
          (Column4 ?x)
          (not (Column5 ?x))
        )
      )
      (when
        (Column6 ?x)
        (and
          (Column5 ?x)
          (not (Column6 ?x))
        )
      )
      (when
        (Column7 ?x)
        (and
          (Column6 ?x)
          (not (Column7 ?x))
        )
      )
      (when
        (Column8 ?x)
        (and
          (Column7 ?x)
          (not (Column8 ?x))
        )
      )
      (when
        (Column9 ?x)
        (and
          (Column8 ?x)
          (not (Column9 ?x))
        )
      )
      (when
        (Column10 ?x)
        (and
          (Column9 ?x)
          (not (Column10 ?x))
        )
      )
      (when
        (Column11 ?x)
        (and
          (Column10 ?x)
          (not (Column11 ?x))
        )
      )
      (when
        (Column12 ?x)
        (and
          (Column11 ?x)
          (not (Column12 ?x))
        )
      )
      (when
        (Column13 ?x)
        (and
          (Column12 ?x)
          (not (Column13 ?x))
        )
      )
      (when
        (Column14 ?x)
        (and
          (Column13 ?x)
          (not (Column14 ?x))
        )
      )
      (when
        (Column15 ?x)
        (and
          (Column14 ?x)
          (not (Column15 ?x))
        )
      )
      (when
        (Column16 ?x)
        (and
          (Column15 ?x)
          (not (Column16 ?x))
        )
      )
      (when
        (Column17 ?x)
        (and
          (Column16 ?x)
          (not (Column17 ?x))
        )
      )
      (when
        (or
          (and
            (RightOf4 ?x)
            (LeftOf2 ?x)
          )
          (and
            (RightOf6 ?x)
            (LeftOf2 ?x)
          )
          (and
            (RightOf16 ?x)
            (LeftOf1 ?x)
          )
          (and
            (RightOf14 ?x)
            (LeftOf1 ?x)
          )
          (and
            (RightOf6 ?x)
            (LeftOf1 ?x)
          )
          (and
            (RightOf2 ?x)
            (LeftOf1 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf2 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf8 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf11 ?x)
          )
          (and
            (LeftOf1 ?x)
            (RightOf13 ?x)
          )
          (and
            (RightOf17 ?x)
            (LeftOf1 ?x)
          )
          (and
            (LeftOf1 ?x)
            (RightOf8 ?x)
          )
          (and
            (RightOf5 ?x)
            (LeftOf1 ?x)
          )
          (and
            (RightOf12 ?x)
            (LeftOf2 ?x)
          )
          (and
            (RightOf7 ?x)
            (LeftOf1 ?x)
          )
          (and
            (RightOf12 ?x)
            (LeftOf1 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf10 ?x)
          )
          (and
            (RightOf4 ?x)
            (LeftOf1 ?x)
          )
          (and
            (RightOf14 ?x)
            (LeftOf2 ?x)
          )
          (and
            (RightOf17 ?x)
            (LeftOf2 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf3 ?x)
          )
          (and
            (LeftOf1 ?x)
            (RightOf11 ?x)
          )
          (and
            (LeftOf1 ?x)
            (RightOf5 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf16 ?x)
          )
          (and
            (RightOf7 ?x)
            (LeftOf2 ?x)
          )
          (and
            (RightOf1 ?x)
            (LeftOf1 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf5 ?x)
          )
          (and
            (RightOf1 ?x)
            (LeftOf2 ?x)
          )
          (and
            (LeftOf1 ?x)
            (RightOf10 ?x)
          )
          (and
            (RightOf15 ?x)
            (LeftOf1 ?x)
          )
          (and
            (LeftOf1 ?x)
            (RightOf3 ?x)
          )
          (and
            (RightOf9 ?x)
            (LeftOf1 ?x)
          )
          (and
            (RightOf9 ?x)
            (LeftOf2 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf13 ?x)
          )
          (and
            (RightOf15 ?x)
            (LeftOf2 ?x)
          )
        )
        (and
          (Column0 ?x)
        )
      )
      (when
        (or
          (and
            (RightOf2 ?x)
            (LeftOf3 ?x)
          )
          (and
            (RightOf4 ?x)
            (LeftOf2 ?x)
          )
          (and
            (RightOf9 ?x)
            (LeftOf3 ?x)
          )
          (and
            (RightOf6 ?x)
            (LeftOf2 ?x)
          )
          (and
            (RightOf16 ?x)
            (LeftOf1 ?x)
          )
          (and
            (RightOf14 ?x)
            (LeftOf1 ?x)
          )
          (and
            (RightOf6 ?x)
            (LeftOf1 ?x)
          )
          (and
            (RightOf2 ?x)
            (LeftOf1 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf2 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf8 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf11 ?x)
          )
          (and
            (LeftOf1 ?x)
            (RightOf13 ?x)
          )
          (and
            (RightOf17 ?x)
            (LeftOf3 ?x)
          )
          (and
            (LeftOf3 ?x)
            (RightOf13 ?x)
          )
          (and
            (RightOf11 ?x)
            (LeftOf3 ?x)
          )
          (and
            (LeftOf3 ?x)
            (RightOf3 ?x)
          )
          (and
            (LeftOf1 ?x)
            (RightOf8 ?x)
          )
          (and
            (RightOf15 ?x)
            (LeftOf3 ?x)
          )
          (and
            (RightOf5 ?x)
            (LeftOf3 ?x)
          )
          (and
            (RightOf12 ?x)
            (LeftOf2 ?x)
          )
          (and
            (RightOf7 ?x)
            (LeftOf2 ?x)
          )
          (and
            (RightOf8 ?x)
            (LeftOf3 ?x)
          )
          (and
            (RightOf10 ?x)
            (LeftOf3 ?x)
          )
          (and
            (RightOf12 ?x)
            (LeftOf1 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf10 ?x)
          )
          (and
            (RightOf4 ?x)
            (LeftOf1 ?x)
          )
          (and
            (RightOf14 ?x)
            (LeftOf2 ?x)
          )
          (and
            (RightOf12 ?x)
            (LeftOf3 ?x)
          )
          (and
            (RightOf17 ?x)
            (LeftOf2 ?x)
          )
          (and
            (RightOf5 ?x)
            (LeftOf1 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf3 ?x)
          )
          (and
            (LeftOf1 ?x)
            (RightOf11 ?x)
          )
          (and
            (RightOf7 ?x)
            (LeftOf3 ?x)
          )
          (and
            (LeftOf1 ?x)
            (RightOf5 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf16 ?x)
          )
          (and
            (RightOf7 ?x)
            (LeftOf1 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf5 ?x)
          )
          (and
            (RightOf16 ?x)
            (LeftOf3 ?x)
          )
          (and
            (LeftOf1 ?x)
            (RightOf10 ?x)
          )
          (and
            (RightOf15 ?x)
            (LeftOf1 ?x)
          )
          (and
            (RightOf4 ?x)
            (LeftOf3 ?x)
          )
          (and
            (RightOf17 ?x)
            (LeftOf1 ?x)
          )
          (and
            (RightOf14 ?x)
            (LeftOf3 ?x)
          )
          (and
            (LeftOf1 ?x)
            (RightOf3 ?x)
          )
          (and
            (RightOf9 ?x)
            (LeftOf1 ?x)
          )
          (and
            (RightOf6 ?x)
            (LeftOf3 ?x)
          )
          (and
            (RightOf9 ?x)
            (LeftOf2 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf13 ?x)
          )
          (and
            (RightOf15 ?x)
            (LeftOf2 ?x)
          )
        )
        (and
          (Column1 ?x)
        )
      )
      (when
        (or
          (and
            (RightOf4 ?x)
            (LeftOf2 ?x)
          )
          (and
            (RightOf9 ?x)
            (LeftOf3 ?x)
          )
          (and
            (RightOf6 ?x)
            (LeftOf2 ?x)
          )
          (and
            (RightOf11 ?x)
            (LeftOf4 ?x)
          )
          (and
            (LeftOf4 ?x)
            (RightOf3 ?x)
          )
          (and
            (RightOf6 ?x)
            (LeftOf1 ?x)
          )
          (and
            (LeftOf4 ?x)
            (RightOf13 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf8 ?x)
          )
          (and
            (RightOf5 ?x)
            (LeftOf4 ?x)
          )
          (and
            (RightOf6 ?x)
            (LeftOf4 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf11 ?x)
          )
          (and
            (LeftOf1 ?x)
            (RightOf13 ?x)
          )
          (and
            (RightOf16 ?x)
            (LeftOf4 ?x)
          )
          (and
            (RightOf9 ?x)
            (LeftOf4 ?x)
          )
          (and
            (RightOf17 ?x)
            (LeftOf3 ?x)
          )
          (and
            (LeftOf3 ?x)
            (RightOf13 ?x)
          )
          (and
            (LeftOf4 ?x)
            (RightOf10 ?x)
          )
          (and
            (RightOf12 ?x)
            (LeftOf4 ?x)
          )
          (and
            (RightOf11 ?x)
            (LeftOf3 ?x)
          )
          (and
            (RightOf17 ?x)
            (LeftOf1 ?x)
          )
          (and
            (LeftOf1 ?x)
            (RightOf8 ?x)
          )
          (and
            (RightOf15 ?x)
            (LeftOf3 ?x)
          )
          (and
            (RightOf5 ?x)
            (LeftOf3 ?x)
          )
          (and
            (RightOf16 ?x)
            (LeftOf1 ?x)
          )
          (and
            (LeftOf4 ?x)
            (RightOf8 ?x)
          )
          (and
            (RightOf12 ?x)
            (LeftOf2 ?x)
          )
          (and
            (RightOf7 ?x)
            (LeftOf2 ?x)
          )
          (and
            (RightOf3 ?x)
            (LeftOf3 ?x)
          )
          (and
            (RightOf10 ?x)
            (LeftOf3 ?x)
          )
          (and
            (RightOf12 ?x)
            (LeftOf1 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf10 ?x)
          )
          (and
            (RightOf14 ?x)
            (LeftOf4 ?x)
          )
          (and
            (RightOf4 ?x)
            (LeftOf1 ?x)
          )
          (and
            (RightOf14 ?x)
            (LeftOf2 ?x)
          )
          (and
            (RightOf12 ?x)
            (LeftOf3 ?x)
          )
          (and
            (RightOf17 ?x)
            (LeftOf2 ?x)
          )
          (and
            (RightOf5 ?x)
            (LeftOf1 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf3 ?x)
          )
          (and
            (LeftOf1 ?x)
            (RightOf11 ?x)
          )
          (and
            (RightOf7 ?x)
            (LeftOf3 ?x)
          )
          (and
            (RightOf15 ?x)
            (LeftOf4 ?x)
          )
          (and
            (LeftOf1 ?x)
            (RightOf5 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf16 ?x)
          )
          (and
            (RightOf7 ?x)
            (LeftOf1 ?x)
          )
          (and
            (RightOf8 ?x)
            (LeftOf3 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf5 ?x)
          )
          (and
            (RightOf7 ?x)
            (LeftOf4 ?x)
          )
          (and
            (RightOf16 ?x)
            (LeftOf3 ?x)
          )
          (and
            (RightOf4 ?x)
            (LeftOf4 ?x)
          )
          (and
            (RightOf17 ?x)
            (LeftOf4 ?x)
          )
          (and
            (LeftOf1 ?x)
            (RightOf10 ?x)
          )
          (and
            (RightOf15 ?x)
            (LeftOf1 ?x)
          )
          (and
            (RightOf4 ?x)
            (LeftOf3 ?x)
          )
          (and
            (RightOf13 ?x)
            (LeftOf3 ?x)
          )
          (and
            (RightOf14 ?x)
            (LeftOf1 ?x)
          )
          (and
            (RightOf14 ?x)
            (LeftOf3 ?x)
          )
          (and
            (LeftOf1 ?x)
            (RightOf3 ?x)
          )
          (and
            (RightOf9 ?x)
            (LeftOf1 ?x)
          )
          (and
            (RightOf6 ?x)
            (LeftOf3 ?x)
          )
          (and
            (RightOf9 ?x)
            (LeftOf2 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf13 ?x)
          )
          (and
            (RightOf15 ?x)
            (LeftOf2 ?x)
          )
        )
        (and
          (Column2 ?x)
        )
      )
      (when
        (or
          (and
            (LeftOf5 ?x)
            (RightOf17 ?x)
          )
          (and
            (RightOf4 ?x)
            (LeftOf2 ?x)
          )
          (and
            (RightOf9 ?x)
            (LeftOf3 ?x)
          )
          (and
            (RightOf6 ?x)
            (LeftOf2 ?x)
          )
          (and
            (RightOf11 ?x)
            (LeftOf4 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf5 ?x)
          )
          (and
            (RightOf6 ?x)
            (LeftOf1 ?x)
          )
          (and
            (RightOf5 ?x)
            (LeftOf3 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf14 ?x)
          )
          (and
            (LeftOf4 ?x)
            (RightOf13 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf8 ?x)
          )
          (and
            (RightOf5 ?x)
            (LeftOf4 ?x)
          )
          (and
            (RightOf6 ?x)
            (LeftOf4 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf11 ?x)
          )
          (and
            (LeftOf1 ?x)
            (RightOf13 ?x)
          )
          (and
            (RightOf16 ?x)
            (LeftOf4 ?x)
          )
          (and
            (RightOf9 ?x)
            (LeftOf4 ?x)
          )
          (and
            (RightOf17 ?x)
            (LeftOf3 ?x)
          )
          (and
            (LeftOf3 ?x)
            (RightOf13 ?x)
          )
          (and
            (LeftOf4 ?x)
            (RightOf10 ?x)
          )
          (and
            (RightOf12 ?x)
            (LeftOf4 ?x)
          )
          (and
            (RightOf11 ?x)
            (LeftOf3 ?x)
          )
          (and
            (RightOf17 ?x)
            (LeftOf1 ?x)
          )
          (and
            (RightOf14 ?x)
            (LeftOf2 ?x)
          )
          (and
            (RightOf12 ?x)
            (LeftOf3 ?x)
          )
          (and
            (RightOf5 ?x)
            (LeftOf1 ?x)
          )
          (and
            (RightOf16 ?x)
            (LeftOf1 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf8 ?x)
          )
          (and
            (RightOf12 ?x)
            (LeftOf5 ?x)
          )
          (and
            (LeftOf4 ?x)
            (RightOf8 ?x)
          )
          (and
            (RightOf12 ?x)
            (LeftOf2 ?x)
          )
          (and
            (RightOf7 ?x)
            (LeftOf2 ?x)
          )
          (and
            (RightOf10 ?x)
            (LeftOf4 ?x)
          )
          (and
            (RightOf10 ?x)
            (LeftOf3 ?x)
          )
          (and
            (RightOf12 ?x)
            (LeftOf1 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf10 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf10 ?x)
          )
          (and
            (RightOf4 ?x)
            (LeftOf1 ?x)
          )
          (and
            (RightOf6 ?x)
            (LeftOf5 ?x)
          )
          (and
            (RightOf8 ?x)
            (LeftOf4 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf16 ?x)
          )
          (and
            (RightOf15 ?x)
            (LeftOf3 ?x)
          )
          (and
            (RightOf17 ?x)
            (LeftOf2 ?x)
          )
          (and
            (RightOf14 ?x)
            (LeftOf4 ?x)
          )
          (and
            (LeftOf1 ?x)
            (RightOf11 ?x)
          )
          (and
            (RightOf7 ?x)
            (LeftOf3 ?x)
          )
          (and
            (RightOf15 ?x)
            (LeftOf4 ?x)
          )
          (and
            (LeftOf1 ?x)
            (RightOf5 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf16 ?x)
          )
          (and
            (RightOf7 ?x)
            (LeftOf1 ?x)
          )
          (and
            (RightOf8 ?x)
            (LeftOf3 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf5 ?x)
          )
          (and
            (RightOf7 ?x)
            (LeftOf4 ?x)
          )
          (and
            (RightOf15 ?x)
            (LeftOf2 ?x)
          )
          (and
            (RightOf16 ?x)
            (LeftOf3 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf13 ?x)
          )
          (and
            (RightOf4 ?x)
            (LeftOf4 ?x)
          )
          (and
            (RightOf17 ?x)
            (LeftOf4 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf11 ?x)
          )
          (and
            (LeftOf1 ?x)
            (RightOf8 ?x)
          )
          (and
            (LeftOf1 ?x)
            (RightOf10 ?x)
          )
          (and
            (RightOf15 ?x)
            (LeftOf1 ?x)
          )
          (and
            (RightOf4 ?x)
            (LeftOf3 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf7 ?x)
          )
          (and
            (RightOf13 ?x)
            (LeftOf3 ?x)
          )
          (and
            (RightOf14 ?x)
            (LeftOf1 ?x)
          )
          (and
            (RightOf14 ?x)
            (LeftOf3 ?x)
          )
          (and
            (RightOf9 ?x)
            (LeftOf1 ?x)
          )
          (and
            (RightOf6 ?x)
            (LeftOf3 ?x)
          )
          (and
            (RightOf9 ?x)
            (LeftOf2 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf4 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf13 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf15 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf9 ?x)
          )
        )
        (and
          (Column3 ?x)
        )
      )
      (when
        (or
          (and
            (LeftOf5 ?x)
            (RightOf17 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf9 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf11 ?x)
          )
          (and
            (RightOf6 ?x)
            (LeftOf2 ?x)
          )
          (and
            (RightOf11 ?x)
            (LeftOf4 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf5 ?x)
          )
          (and
            (RightOf6 ?x)
            (LeftOf1 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf14 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf14 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf8 ?x)
          )
          (and
            (RightOf5 ?x)
            (LeftOf4 ?x)
          )
          (and
            (RightOf6 ?x)
            (LeftOf4 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf13 ?x)
          )
          (and
            (LeftOf1 ?x)
            (RightOf13 ?x)
          )
          (and
            (RightOf16 ?x)
            (LeftOf4 ?x)
          )
          (and
            (RightOf9 ?x)
            (LeftOf4 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf6 ?x)
          )
          (and
            (RightOf17 ?x)
            (LeftOf3 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf10 ?x)
          )
          (and
            (LeftOf3 ?x)
            (RightOf13 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf7 ?x)
          )
          (and
            (LeftOf4 ?x)
            (RightOf10 ?x)
          )
          (and
            (RightOf12 ?x)
            (LeftOf4 ?x)
          )
          (and
            (RightOf11 ?x)
            (LeftOf3 ?x)
          )
          (and
            (RightOf17 ?x)
            (LeftOf1 ?x)
          )
          (and
            (LeftOf1 ?x)
            (RightOf8 ?x)
          )
          (and
            (RightOf15 ?x)
            (LeftOf3 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf5 ?x)
          )
          (and
            (RightOf5 ?x)
            (LeftOf3 ?x)
          )
          (and
            (RightOf16 ?x)
            (LeftOf1 ?x)
          )
          (and
            (RightOf14 ?x)
            (LeftOf2 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf8 ?x)
          )
          (and
            (RightOf12 ?x)
            (LeftOf5 ?x)
          )
          (and
            (LeftOf4 ?x)
            (RightOf8 ?x)
          )
          (and
            (RightOf12 ?x)
            (LeftOf2 ?x)
          )
          (and
            (RightOf7 ?x)
            (LeftOf1 ?x)
          )
          (and
            (RightOf10 ?x)
            (LeftOf4 ?x)
          )
          (and
            (RightOf10 ?x)
            (LeftOf3 ?x)
          )
          (and
            (RightOf12 ?x)
            (LeftOf1 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf10 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf10 ?x)
          )
          (and
            (RightOf6 ?x)
            (LeftOf5 ?x)
          )
          (and
            (LeftOf1 ?x)
            (RightOf5 ?x)
          )
          (and
            (RightOf6 ?x)
            (LeftOf3 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf17 ?x)
          )
          (and
            (RightOf8 ?x)
            (LeftOf4 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf11 ?x)
          )
          (and
            (RightOf12 ?x)
            (LeftOf3 ?x)
          )
          (and
            (RightOf17 ?x)
            (LeftOf2 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf8 ?x)
          )
          (and
            (RightOf14 ?x)
            (LeftOf4 ?x)
          )
          (and
            (LeftOf1 ?x)
            (RightOf11 ?x)
          )
          (and
            (RightOf7 ?x)
            (LeftOf3 ?x)
          )
          (and
            (RightOf15 ?x)
            (LeftOf4 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf12 ?x)
          )
          (and
            (LeftOf4 ?x)
            (RightOf13 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf16 ?x)
          )
          (and
            (RightOf7 ?x)
            (LeftOf2 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf15 ?x)
          )
          (and
            (RightOf8 ?x)
            (LeftOf3 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf16 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf5 ?x)
          )
          (and
            (RightOf7 ?x)
            (LeftOf4 ?x)
          )
          (and
            (RightOf15 ?x)
            (LeftOf2 ?x)
          )
          (and
            (RightOf16 ?x)
            (LeftOf3 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf13 ?x)
          )
          (and
            (LeftOf1 ?x)
            (RightOf10 ?x)
          )
          (and
            (RightOf15 ?x)
            (LeftOf1 ?x)
          )
          (and
            (RightOf9 ?x)
            (LeftOf3 ?x)
          )
          (and
            (RightOf17 ?x)
            (LeftOf4 ?x)
          )
          (and
            (RightOf13 ?x)
            (LeftOf3 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf16 ?x)
          )
          (and
            (RightOf14 ?x)
            (LeftOf1 ?x)
          )
          (and
            (RightOf14 ?x)
            (LeftOf3 ?x)
          )
          (and
            (RightOf9 ?x)
            (LeftOf1 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf7 ?x)
          )
          (and
            (RightOf9 ?x)
            (LeftOf2 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf9 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf13 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf15 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf11 ?x)
          )
        )
        (and
          (Column4 ?x)
        )
      )
      (when
        (or
          (and
            (RightOf11 ?x)
            (LeftOf4 ?x)
          )
          (and
            (LeftOf7 ?x)
            (RightOf10 ?x)
          )
          (and
            (RightOf15 ?x)
            (LeftOf2 ?x)
          )
          (and
            (RightOf9 ?x)
            (LeftOf4 ?x)
          )
          (and
            (LeftOf3 ?x)
            (RightOf13 ?x)
          )
          (and
            (RightOf12 ?x)
            (LeftOf4 ?x)
          )
          (and
            (RightOf11 ?x)
            (LeftOf3 ?x)
          )
          (and
            (LeftOf1 ?x)
            (RightOf8 ?x)
          )
          (and
            (RightOf14 ?x)
            (LeftOf7 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf8 ?x)
          )
          (and
            (LeftOf7 ?x)
            (RightOf16 ?x)
          )
          (and
            (LeftOf4 ?x)
            (RightOf8 ?x)
          )
          (and
            (RightOf16 ?x)
            (LeftOf3 ?x)
          )
          (and
            (LeftOf1 ?x)
            (RightOf11 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf8 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf16 ?x)
          )
          (and
            (RightOf10 ?x)
            (LeftOf4 ?x)
          )
          (and
            (RightOf17 ?x)
            (LeftOf4 ?x)
          )
          (and
            (RightOf6 ?x)
            (LeftOf3 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf16 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf11 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf17 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf9 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf11 ?x)
          )
          (and
            (RightOf6 ?x)
            (LeftOf2 ?x)
          )
          (and
            (RightOf6 ?x)
            (LeftOf4 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf13 ?x)
          )
          (and
            (LeftOf1 ?x)
            (RightOf13 ?x)
          )
          (and
            (LeftOf4 ?x)
            (RightOf10 ?x)
          )
          (and
            (RightOf17 ?x)
            (LeftOf1 ?x)
          )
          (and
            (RightOf14 ?x)
            (LeftOf2 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf10 ?x)
          )
          (and
            (RightOf12 ?x)
            (LeftOf3 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf7 ?x)
          )
          (and
            (LeftOf7 ?x)
            (RightOf17 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf14 ?x)
          )
          (and
            (RightOf16 ?x)
            (LeftOf4 ?x)
          )
          (and
            (RightOf7 ?x)
            (LeftOf4 ?x)
          )
          (and
            (LeftOf7 ?x)
            (RightOf8 ?x)
          )
          (and
            (RightOf14 ?x)
            (LeftOf3 ?x)
          )
          (and
            (LeftOf7 ?x)
            (RightOf11 ?x)
          )
          (and
            (RightOf9 ?x)
            (LeftOf1 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf15 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf9 ?x)
          )
          (and
            (RightOf15 ?x)
            (LeftOf7 ?x)
          )
          (and
            (RightOf9 ?x)
            (LeftOf3 ?x)
          )
          (and
            (RightOf16 ?x)
            (LeftOf1 ?x)
          )
          (and
            (RightOf14 ?x)
            (LeftOf1 ?x)
          )
          (and
            (LeftOf4 ?x)
            (RightOf13 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf10 ?x)
          )
          (and
            (RightOf12 ?x)
            (LeftOf6 ?x)
          )
          (and
            (RightOf6 ?x)
            (LeftOf7 ?x)
          )
          (and
            (RightOf15 ?x)
            (LeftOf4 ?x)
          )
          (and
            (RightOf12 ?x)
            (LeftOf5 ?x)
          )
          (and
            (RightOf12 ?x)
            (LeftOf7 ?x)
          )
          (and
            (RightOf7 ?x)
            (LeftOf1 ?x)
          )
          (and
            (RightOf12 ?x)
            (LeftOf1 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf17 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf16 ?x)
          )
          (and
            (RightOf15 ?x)
            (LeftOf3 ?x)
          )
          (and
            (RightOf17 ?x)
            (LeftOf2 ?x)
          )
          (and
            (RightOf14 ?x)
            (LeftOf4 ?x)
          )
          (and
            (RightOf7 ?x)
            (LeftOf3 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf12 ?x)
          )
          (and
            (RightOf7 ?x)
            (LeftOf2 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf7 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf13 ?x)
          )
          (and
            (LeftOf1 ?x)
            (RightOf10 ?x)
          )
          (and
            (RightOf13 ?x)
            (LeftOf3 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf13 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf14 ?x)
          )
          (and
            (RightOf6 ?x)
            (LeftOf1 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf8 ?x)
          )
          (and
            (RightOf17 ?x)
            (LeftOf3 ?x)
          )
          (and
            (RightOf6 ?x)
            (LeftOf6 ?x)
          )
          (and
            (RightOf9 ?x)
            (LeftOf7 ?x)
          )
          (and
            (RightOf9 ?x)
            (LeftOf2 ?x)
          )
          (and
            (RightOf12 ?x)
            (LeftOf2 ?x)
          )
          (and
            (RightOf8 ?x)
            (LeftOf3 ?x)
          )
          (and
            (RightOf10 ?x)
            (LeftOf3 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf10 ?x)
          )
          (and
            (RightOf6 ?x)
            (LeftOf5 ?x)
          )
          (and
            (RightOf8 ?x)
            (LeftOf4 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf11 ?x)
          )
          (and
            (LeftOf7 ?x)
            (RightOf13 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf15 ?x)
          )
          (and
            (RightOf7 ?x)
            (LeftOf7 ?x)
          )
          (and
            (RightOf15 ?x)
            (LeftOf1 ?x)
          )
        )
        (and
          (Column5 ?x)
        )
      )
      (when
        (or
          (and
            (RightOf11 ?x)
            (LeftOf4 ?x)
          )
          (and
            (LeftOf7 ?x)
            (RightOf10 ?x)
          )
          (and
            (RightOf15 ?x)
            (LeftOf2 ?x)
          )
          (and
            (RightOf9 ?x)
            (LeftOf4 ?x)
          )
          (and
            (LeftOf3 ?x)
            (RightOf13 ?x)
          )
          (and
            (RightOf12 ?x)
            (LeftOf4 ?x)
          )
          (and
            (RightOf11 ?x)
            (LeftOf3 ?x)
          )
          (and
            (LeftOf1 ?x)
            (RightOf8 ?x)
          )
          (and
            (RightOf14 ?x)
            (LeftOf7 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf8 ?x)
          )
          (and
            (LeftOf7 ?x)
            (RightOf16 ?x)
          )
          (and
            (LeftOf4 ?x)
            (RightOf8 ?x)
          )
          (and
            (RightOf16 ?x)
            (LeftOf3 ?x)
          )
          (and
            (LeftOf8 ?x)
            (RightOf8 ?x)
          )
          (and
            (LeftOf1 ?x)
            (RightOf11 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf8 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf16 ?x)
          )
          (and
            (RightOf10 ?x)
            (LeftOf4 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf10 ?x)
          )
          (and
            (RightOf17 ?x)
            (LeftOf4 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf7 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf16 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf11 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf17 ?x)
          )
          (and
            (LeftOf8 ?x)
            (RightOf17 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf9 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf11 ?x)
          )
          (and
            (LeftOf8 ?x)
            (RightOf11 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf13 ?x)
          )
          (and
            (LeftOf1 ?x)
            (RightOf13 ?x)
          )
          (and
            (LeftOf4 ?x)
            (RightOf10 ?x)
          )
          (and
            (RightOf17 ?x)
            (LeftOf1 ?x)
          )
          (and
            (RightOf12 ?x)
            (LeftOf8 ?x)
          )
          (and
            (RightOf12 ?x)
            (LeftOf3 ?x)
          )
          (and
            (LeftOf7 ?x)
            (RightOf17 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf14 ?x)
          )
          (and
            (LeftOf8 ?x)
            (RightOf14 ?x)
          )
          (and
            (RightOf16 ?x)
            (LeftOf4 ?x)
          )
          (and
            (RightOf7 ?x)
            (LeftOf4 ?x)
          )
          (and
            (LeftOf8 ?x)
            (RightOf7 ?x)
          )
          (and
            (RightOf9 ?x)
            (LeftOf8 ?x)
          )
          (and
            (LeftOf7 ?x)
            (RightOf8 ?x)
          )
          (and
            (RightOf14 ?x)
            (LeftOf3 ?x)
          )
          (and
            (LeftOf7 ?x)
            (RightOf11 ?x)
          )
          (and
            (RightOf14 ?x)
            (LeftOf2 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf15 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf9 ?x)
          )
          (and
            (RightOf15 ?x)
            (LeftOf7 ?x)
          )
          (and
            (RightOf9 ?x)
            (LeftOf3 ?x)
          )
          (and
            (RightOf16 ?x)
            (LeftOf1 ?x)
          )
          (and
            (RightOf14 ?x)
            (LeftOf1 ?x)
          )
          (and
            (LeftOf4 ?x)
            (RightOf13 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf10 ?x)
          )
          (and
            (RightOf12 ?x)
            (LeftOf6 ?x)
          )
          (and
            (RightOf15 ?x)
            (LeftOf4 ?x)
          )
          (and
            (RightOf15 ?x)
            (LeftOf8 ?x)
          )
          (and
            (RightOf12 ?x)
            (LeftOf5 ?x)
          )
          (and
            (RightOf12 ?x)
            (LeftOf7 ?x)
          )
          (and
            (RightOf7 ?x)
            (LeftOf2 ?x)
          )
          (and
            (RightOf12 ?x)
            (LeftOf1 ?x)
          )
          (and
            (LeftOf7 ?x)
            (RightOf7 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf17 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf16 ?x)
          )
          (and
            (RightOf15 ?x)
            (LeftOf3 ?x)
          )
          (and
            (RightOf17 ?x)
            (LeftOf2 ?x)
          )
          (and
            (RightOf14 ?x)
            (LeftOf4 ?x)
          )
          (and
            (RightOf7 ?x)
            (LeftOf3 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf12 ?x)
          )
          (and
            (RightOf7 ?x)
            (LeftOf1 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf7 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf13 ?x)
          )
          (and
            (LeftOf8 ?x)
            (RightOf13 ?x)
          )
          (and
            (LeftOf1 ?x)
            (RightOf10 ?x)
          )
          (and
            (RightOf13 ?x)
            (LeftOf3 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf13 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf14 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf8 ?x)
          )
          (and
            (RightOf17 ?x)
            (LeftOf3 ?x)
          )
          (and
            (RightOf9 ?x)
            (LeftOf7 ?x)
          )
          (and
            (RightOf9 ?x)
            (LeftOf2 ?x)
          )
          (and
            (LeftOf8 ?x)
            (RightOf16 ?x)
          )
          (and
            (RightOf12 ?x)
            (LeftOf2 ?x)
          )
          (and
            (RightOf8 ?x)
            (LeftOf3 ?x)
          )
          (and
            (RightOf10 ?x)
            (LeftOf3 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf10 ?x)
          )
          (and
            (LeftOf8 ?x)
            (RightOf10 ?x)
          )
          (and
            (RightOf8 ?x)
            (LeftOf4 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf11 ?x)
          )
          (and
            (RightOf9 ?x)
            (LeftOf1 ?x)
          )
          (and
            (LeftOf7 ?x)
            (RightOf13 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf15 ?x)
          )
          (and
            (RightOf15 ?x)
            (LeftOf1 ?x)
          )
        )
        (and
          (Column6 ?x)
        )
      )
      (when
        (or
          (and
            (RightOf14 ?x)
            (LeftOf9 ?x)
          )
          (and
            (LeftOf7 ?x)
            (RightOf10 ?x)
          )
          (and
            (RightOf15 ?x)
            (LeftOf2 ?x)
          )
          (and
            (RightOf9 ?x)
            (LeftOf4 ?x)
          )
          (and
            (LeftOf3 ?x)
            (RightOf13 ?x)
          )
          (and
            (RightOf11 ?x)
            (LeftOf4 ?x)
          )
          (and
            (RightOf12 ?x)
            (LeftOf4 ?x)
          )
          (and
            (RightOf17 ?x)
            (LeftOf9 ?x)
          )
          (and
            (LeftOf1 ?x)
            (RightOf8 ?x)
          )
          (and
            (RightOf14 ?x)
            (LeftOf7 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf8 ?x)
          )
          (and
            (LeftOf7 ?x)
            (RightOf16 ?x)
          )
          (and
            (RightOf16 ?x)
            (LeftOf3 ?x)
          )
          (and
            (LeftOf8 ?x)
            (RightOf8 ?x)
          )
          (and
            (LeftOf1 ?x)
            (RightOf11 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf8 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf16 ?x)
          )
          (and
            (RightOf10 ?x)
            (LeftOf4 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf10 ?x)
          )
          (and
            (RightOf17 ?x)
            (LeftOf4 ?x)
          )
          (and
            (RightOf16 ?x)
            (LeftOf9 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf16 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf11 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf17 ?x)
          )
          (and
            (LeftOf8 ?x)
            (RightOf17 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf9 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf11 ?x)
          )
          (and
            (LeftOf8 ?x)
            (RightOf11 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf13 ?x)
          )
          (and
            (LeftOf1 ?x)
            (RightOf13 ?x)
          )
          (and
            (LeftOf4 ?x)
            (RightOf10 ?x)
          )
          (and
            (RightOf17 ?x)
            (LeftOf1 ?x)
          )
          (and
            (RightOf12 ?x)
            (LeftOf8 ?x)
          )
          (and
            (LeftOf9 ?x)
            (RightOf10 ?x)
          )
          (and
            (RightOf12 ?x)
            (LeftOf3 ?x)
          )
          (and
            (LeftOf7 ?x)
            (RightOf17 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf14 ?x)
          )
          (and
            (LeftOf8 ?x)
            (RightOf14 ?x)
          )
          (and
            (RightOf16 ?x)
            (LeftOf4 ?x)
          )
          (and
            (RightOf9 ?x)
            (LeftOf8 ?x)
          )
          (and
            (RightOf11 ?x)
            (LeftOf3 ?x)
          )
          (and
            (RightOf15 ?x)
            (LeftOf9 ?x)
          )
          (and
            (RightOf14 ?x)
            (LeftOf3 ?x)
          )
          (and
            (LeftOf7 ?x)
            (RightOf11 ?x)
          )
          (and
            (RightOf14 ?x)
            (LeftOf2 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf15 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf9 ?x)
          )
          (and
            (RightOf15 ?x)
            (LeftOf7 ?x)
          )
          (and
            (RightOf9 ?x)
            (LeftOf3 ?x)
          )
          (and
            (RightOf16 ?x)
            (LeftOf1 ?x)
          )
          (and
            (RightOf14 ?x)
            (LeftOf1 ?x)
          )
          (and
            (LeftOf4 ?x)
            (RightOf13 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf10 ?x)
          )
          (and
            (RightOf12 ?x)
            (LeftOf6 ?x)
          )
          (and
            (RightOf15 ?x)
            (LeftOf4 ?x)
          )
          (and
            (RightOf15 ?x)
            (LeftOf8 ?x)
          )
          (and
            (LeftOf9 ?x)
            (RightOf11 ?x)
          )
          (and
            (RightOf12 ?x)
            (LeftOf5 ?x)
          )
          (and
            (RightOf12 ?x)
            (LeftOf7 ?x)
          )
          (and
            (RightOf14 ?x)
            (LeftOf8 ?x)
          )
          (and
            (RightOf12 ?x)
            (LeftOf1 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf17 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf16 ?x)
          )
          (and
            (RightOf15 ?x)
            (LeftOf3 ?x)
          )
          (and
            (RightOf17 ?x)
            (LeftOf2 ?x)
          )
          (and
            (RightOf14 ?x)
            (LeftOf4 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf12 ?x)
          )
          (and
            (RightOf9 ?x)
            (LeftOf9 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf13 ?x)
          )
          (and
            (LeftOf8 ?x)
            (RightOf13 ?x)
          )
          (and
            (LeftOf1 ?x)
            (RightOf10 ?x)
          )
          (and
            (RightOf13 ?x)
            (LeftOf3 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf13 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf14 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf8 ?x)
          )
          (and
            (LeftOf7 ?x)
            (RightOf8 ?x)
          )
          (and
            (RightOf17 ?x)
            (LeftOf3 ?x)
          )
          (and
            (RightOf9 ?x)
            (LeftOf7 ?x)
          )
          (and
            (RightOf9 ?x)
            (LeftOf2 ?x)
          )
          (and
            (LeftOf8 ?x)
            (RightOf16 ?x)
          )
          (and
            (RightOf12 ?x)
            (LeftOf9 ?x)
          )
          (and
            (RightOf12 ?x)
            (LeftOf2 ?x)
          )
          (and
            (RightOf8 ?x)
            (LeftOf3 ?x)
          )
          (and
            (RightOf10 ?x)
            (LeftOf3 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf10 ?x)
          )
          (and
            (LeftOf8 ?x)
            (RightOf10 ?x)
          )
          (and
            (RightOf8 ?x)
            (LeftOf4 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf11 ?x)
          )
          (and
            (RightOf9 ?x)
            (LeftOf1 ?x)
          )
          (and
            (LeftOf7 ?x)
            (RightOf13 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf15 ?x)
          )
          (and
            (LeftOf9 ?x)
            (RightOf8 ?x)
          )
          (and
            (LeftOf9 ?x)
            (RightOf13 ?x)
          )
          (and
            (RightOf15 ?x)
            (LeftOf1 ?x)
          )
        )
        (and
          (Column7 ?x)
        )
      )
      (when
        (or
          (and
            (RightOf14 ?x)
            (LeftOf9 ?x)
          )
          (and
            (LeftOf7 ?x)
            (RightOf10 ?x)
          )
          (and
            (RightOf15 ?x)
            (LeftOf10 ?x)
          )
          (and
            (RightOf15 ?x)
            (LeftOf2 ?x)
          )
          (and
            (RightOf9 ?x)
            (LeftOf4 ?x)
          )
          (and
            (LeftOf3 ?x)
            (RightOf13 ?x)
          )
          (and
            (RightOf11 ?x)
            (LeftOf4 ?x)
          )
          (and
            (RightOf12 ?x)
            (LeftOf4 ?x)
          )
          (and
            (RightOf17 ?x)
            (LeftOf9 ?x)
          )
          (and
            (RightOf14 ?x)
            (LeftOf2 ?x)
          )
          (and
            (LeftOf10 ?x)
            (RightOf16 ?x)
          )
          (and
            (RightOf14 ?x)
            (LeftOf7 ?x)
          )
          (and
            (LeftOf7 ?x)
            (RightOf16 ?x)
          )
          (and
            (RightOf16 ?x)
            (LeftOf3 ?x)
          )
          (and
            (LeftOf1 ?x)
            (RightOf11 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf16 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf10 ?x)
          )
          (and
            (RightOf17 ?x)
            (LeftOf4 ?x)
          )
          (and
            (RightOf16 ?x)
            (LeftOf9 ?x)
          )
          (and
            (LeftOf10 ?x)
            (RightOf10 ?x)
          )
          (and
            (LeftOf10 ?x)
            (RightOf11 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf16 ?x)
          )
          (and
            (LeftOf7 ?x)
            (RightOf11 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf11 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf17 ?x)
          )
          (and
            (LeftOf8 ?x)
            (RightOf17 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf9 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf11 ?x)
          )
          (and
            (LeftOf8 ?x)
            (RightOf11 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf13 ?x)
          )
          (and
            (LeftOf1 ?x)
            (RightOf13 ?x)
          )
          (and
            (LeftOf4 ?x)
            (RightOf10 ?x)
          )
          (and
            (RightOf17 ?x)
            (LeftOf1 ?x)
          )
          (and
            (RightOf9 ?x)
            (LeftOf10 ?x)
          )
          (and
            (RightOf12 ?x)
            (LeftOf8 ?x)
          )
          (and
            (LeftOf9 ?x)
            (RightOf10 ?x)
          )
          (and
            (RightOf12 ?x)
            (LeftOf3 ?x)
          )
          (and
            (LeftOf7 ?x)
            (RightOf17 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf14 ?x)
          )
          (and
            (LeftOf8 ?x)
            (RightOf14 ?x)
          )
          (and
            (RightOf16 ?x)
            (LeftOf4 ?x)
          )
          (and
            (LeftOf10 ?x)
            (RightOf13 ?x)
          )
          (and
            (RightOf9 ?x)
            (LeftOf8 ?x)
          )
          (and
            (RightOf11 ?x)
            (LeftOf3 ?x)
          )
          (and
            (RightOf15 ?x)
            (LeftOf9 ?x)
          )
          (and
            (RightOf14 ?x)
            (LeftOf3 ?x)
          )
          (and
            (LeftOf10 ?x)
            (RightOf14 ?x)
          )
          (and
            (RightOf9 ?x)
            (LeftOf1 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf15 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf9 ?x)
          )
          (and
            (RightOf15 ?x)
            (LeftOf7 ?x)
          )
          (and
            (RightOf9 ?x)
            (LeftOf3 ?x)
          )
          (and
            (RightOf16 ?x)
            (LeftOf1 ?x)
          )
          (and
            (RightOf14 ?x)
            (LeftOf1 ?x)
          )
          (and
            (LeftOf4 ?x)
            (RightOf13 ?x)
          )
          (and
            (LeftOf10 ?x)
            (RightOf17 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf10 ?x)
          )
          (and
            (RightOf12 ?x)
            (LeftOf6 ?x)
          )
          (and
            (RightOf15 ?x)
            (LeftOf4 ?x)
          )
          (and
            (RightOf15 ?x)
            (LeftOf8 ?x)
          )
          (and
            (LeftOf9 ?x)
            (RightOf11 ?x)
          )
          (and
            (RightOf12 ?x)
            (LeftOf5 ?x)
          )
          (and
            (RightOf12 ?x)
            (LeftOf7 ?x)
          )
          (and
            (RightOf14 ?x)
            (LeftOf8 ?x)
          )
          (and
            (RightOf12 ?x)
            (LeftOf1 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf17 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf16 ?x)
          )
          (and
            (RightOf15 ?x)
            (LeftOf3 ?x)
          )
          (and
            (RightOf17 ?x)
            (LeftOf2 ?x)
          )
          (and
            (RightOf14 ?x)
            (LeftOf4 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf12 ?x)
          )
          (and
            (RightOf9 ?x)
            (LeftOf9 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf13 ?x)
          )
          (and
            (LeftOf8 ?x)
            (RightOf13 ?x)
          )
          (and
            (LeftOf1 ?x)
            (RightOf10 ?x)
          )
          (and
            (RightOf13 ?x)
            (LeftOf3 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf13 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf14 ?x)
          )
          (and
            (RightOf17 ?x)
            (LeftOf3 ?x)
          )
          (and
            (RightOf9 ?x)
            (LeftOf7 ?x)
          )
          (and
            (RightOf9 ?x)
            (LeftOf2 ?x)
          )
          (and
            (LeftOf8 ?x)
            (RightOf16 ?x)
          )
          (and
            (RightOf12 ?x)
            (LeftOf9 ?x)
          )
          (and
            (RightOf12 ?x)
            (LeftOf2 ?x)
          )
          (and
            (RightOf10 ?x)
            (LeftOf4 ?x)
          )
          (and
            (RightOf10 ?x)
            (LeftOf3 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf10 ?x)
          )
          (and
            (LeftOf8 ?x)
            (RightOf10 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf11 ?x)
          )
          (and
            (LeftOf7 ?x)
            (RightOf13 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf15 ?x)
          )
          (and
            (LeftOf9 ?x)
            (RightOf13 ?x)
          )
          (and
            (RightOf12 ?x)
            (LeftOf10 ?x)
          )
          (and
            (RightOf15 ?x)
            (LeftOf1 ?x)
          )
        )
        (and
          (Column8 ?x)
        )
      )
      (when
        (or
          (and
            (RightOf14 ?x)
            (LeftOf9 ?x)
          )
          (and
            (LeftOf11 ?x)
            (RightOf10 ?x)
          )
          (and
            (RightOf17 ?x)
            (LeftOf11 ?x)
          )
          (and
            (LeftOf7 ?x)
            (RightOf10 ?x)
          )
          (and
            (RightOf15 ?x)
            (LeftOf10 ?x)
          )
          (and
            (RightOf15 ?x)
            (LeftOf2 ?x)
          )
          (and
            (LeftOf3 ?x)
            (RightOf13 ?x)
          )
          (and
            (RightOf11 ?x)
            (LeftOf4 ?x)
          )
          (and
            (RightOf12 ?x)
            (LeftOf4 ?x)
          )
          (and
            (RightOf17 ?x)
            (LeftOf9 ?x)
          )
          (and
            (RightOf14 ?x)
            (LeftOf2 ?x)
          )
          (and
            (LeftOf10 ?x)
            (RightOf16 ?x)
          )
          (and
            (RightOf14 ?x)
            (LeftOf7 ?x)
          )
          (and
            (LeftOf7 ?x)
            (RightOf16 ?x)
          )
          (and
            (RightOf16 ?x)
            (LeftOf3 ?x)
          )
          (and
            (LeftOf1 ?x)
            (RightOf11 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf16 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf10 ?x)
          )
          (and
            (RightOf17 ?x)
            (LeftOf4 ?x)
          )
          (and
            (RightOf16 ?x)
            (LeftOf9 ?x)
          )
          (and
            (LeftOf10 ?x)
            (RightOf10 ?x)
          )
          (and
            (LeftOf10 ?x)
            (RightOf11 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf16 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf11 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf17 ?x)
          )
          (and
            (LeftOf8 ?x)
            (RightOf17 ?x)
          )
          (and
            (RightOf12 ?x)
            (LeftOf11 ?x)
          )
          (and
            (LeftOf8 ?x)
            (RightOf11 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf13 ?x)
          )
          (and
            (LeftOf1 ?x)
            (RightOf13 ?x)
          )
          (and
            (LeftOf7 ?x)
            (RightOf11 ?x)
          )
          (and
            (RightOf17 ?x)
            (LeftOf1 ?x)
          )
          (and
            (RightOf12 ?x)
            (LeftOf8 ?x)
          )
          (and
            (LeftOf9 ?x)
            (RightOf10 ?x)
          )
          (and
            (RightOf12 ?x)
            (LeftOf3 ?x)
          )
          (and
            (LeftOf7 ?x)
            (RightOf17 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf14 ?x)
          )
          (and
            (LeftOf8 ?x)
            (RightOf14 ?x)
          )
          (and
            (RightOf16 ?x)
            (LeftOf4 ?x)
          )
          (and
            (LeftOf10 ?x)
            (RightOf13 ?x)
          )
          (and
            (RightOf11 ?x)
            (LeftOf3 ?x)
          )
          (and
            (RightOf15 ?x)
            (LeftOf9 ?x)
          )
          (and
            (RightOf14 ?x)
            (LeftOf3 ?x)
          )
          (and
            (LeftOf10 ?x)
            (RightOf14 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf15 ?x)
          )
          (and
            (RightOf14 ?x)
            (LeftOf11 ?x)
          )
          (and
            (RightOf15 ?x)
            (LeftOf7 ?x)
          )
          (and
            (RightOf16 ?x)
            (LeftOf1 ?x)
          )
          (and
            (RightOf14 ?x)
            (LeftOf1 ?x)
          )
          (and
            (LeftOf4 ?x)
            (RightOf13 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf11 ?x)
          )
          (and
            (LeftOf10 ?x)
            (RightOf17 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf10 ?x)
          )
          (and
            (RightOf12 ?x)
            (LeftOf6 ?x)
          )
          (and
            (RightOf15 ?x)
            (LeftOf4 ?x)
          )
          (and
            (RightOf15 ?x)
            (LeftOf8 ?x)
          )
          (and
            (LeftOf11 ?x)
            (RightOf13 ?x)
          )
          (and
            (LeftOf9 ?x)
            (RightOf11 ?x)
          )
          (and
            (RightOf12 ?x)
            (LeftOf5 ?x)
          )
          (and
            (RightOf12 ?x)
            (LeftOf7 ?x)
          )
          (and
            (RightOf14 ?x)
            (LeftOf8 ?x)
          )
          (and
            (RightOf12 ?x)
            (LeftOf1 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf17 ?x)
          )
          (and
            (RightOf14 ?x)
            (LeftOf10 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf16 ?x)
          )
          (and
            (RightOf15 ?x)
            (LeftOf3 ?x)
          )
          (and
            (RightOf17 ?x)
            (LeftOf2 ?x)
          )
          (and
            (RightOf14 ?x)
            (LeftOf4 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf12 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf13 ?x)
          )
          (and
            (LeftOf8 ?x)
            (RightOf13 ?x)
          )
          (and
            (LeftOf1 ?x)
            (RightOf10 ?x)
          )
          (and
            (RightOf13 ?x)
            (LeftOf3 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf13 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf14 ?x)
          )
          (and
            (RightOf17 ?x)
            (LeftOf3 ?x)
          )
          (and
            (LeftOf8 ?x)
            (RightOf16 ?x)
          )
          (and
            (RightOf12 ?x)
            (LeftOf9 ?x)
          )
          (and
            (LeftOf11 ?x)
            (RightOf11 ?x)
          )
          (and
            (RightOf12 ?x)
            (LeftOf2 ?x)
          )
          (and
            (RightOf10 ?x)
            (LeftOf4 ?x)
          )
          (and
            (RightOf10 ?x)
            (LeftOf3 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf10 ?x)
          )
          (and
            (LeftOf8 ?x)
            (RightOf10 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf11 ?x)
          )
          (and
            (LeftOf7 ?x)
            (RightOf13 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf15 ?x)
          )
          (and
            (RightOf16 ?x)
            (LeftOf11 ?x)
          )
          (and
            (LeftOf9 ?x)
            (RightOf13 ?x)
          )
          (and
            (RightOf12 ?x)
            (LeftOf10 ?x)
          )
          (and
            (RightOf15 ?x)
            (LeftOf11 ?x)
          )
          (and
            (RightOf15 ?x)
            (LeftOf1 ?x)
          )
        )
        (and
          (Column9 ?x)
        )
      )
      (when
        (or
          (and
            (RightOf14 ?x)
            (LeftOf9 ?x)
          )
          (and
            (RightOf17 ?x)
            (LeftOf11 ?x)
          )
          (and
            (RightOf15 ?x)
            (LeftOf10 ?x)
          )
          (and
            (RightOf15 ?x)
            (LeftOf2 ?x)
          )
          (and
            (LeftOf12 ?x)
            (RightOf13 ?x)
          )
          (and
            (LeftOf3 ?x)
            (RightOf13 ?x)
          )
          (and
            (RightOf11 ?x)
            (LeftOf4 ?x)
          )
          (and
            (RightOf12 ?x)
            (LeftOf4 ?x)
          )
          (and
            (RightOf17 ?x)
            (LeftOf9 ?x)
          )
          (and
            (RightOf14 ?x)
            (LeftOf2 ?x)
          )
          (and
            (LeftOf10 ?x)
            (RightOf16 ?x)
          )
          (and
            (RightOf14 ?x)
            (LeftOf7 ?x)
          )
          (and
            (LeftOf7 ?x)
            (RightOf16 ?x)
          )
          (and
            (RightOf16 ?x)
            (LeftOf3 ?x)
          )
          (and
            (LeftOf12 ?x)
            (RightOf15 ?x)
          )
          (and
            (LeftOf1 ?x)
            (RightOf11 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf16 ?x)
          )
          (and
            (RightOf17 ?x)
            (LeftOf4 ?x)
          )
          (and
            (RightOf16 ?x)
            (LeftOf9 ?x)
          )
          (and
            (LeftOf10 ?x)
            (RightOf11 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf16 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf11 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf17 ?x)
          )
          (and
            (LeftOf8 ?x)
            (RightOf17 ?x)
          )
          (and
            (RightOf12 ?x)
            (LeftOf11 ?x)
          )
          (and
            (LeftOf8 ?x)
            (RightOf11 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf13 ?x)
          )
          (and
            (LeftOf1 ?x)
            (RightOf13 ?x)
          )
          (and
            (RightOf17 ?x)
            (LeftOf1 ?x)
          )
          (and
            (RightOf12 ?x)
            (LeftOf8 ?x)
          )
          (and
            (LeftOf12 ?x)
            (RightOf17 ?x)
          )
          (and
            (RightOf12 ?x)
            (LeftOf3 ?x)
          )
          (and
            (LeftOf7 ?x)
            (RightOf17 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf14 ?x)
          )
          (and
            (LeftOf8 ?x)
            (RightOf14 ?x)
          )
          (and
            (RightOf16 ?x)
            (LeftOf4 ?x)
          )
          (and
            (LeftOf12 ?x)
            (RightOf16 ?x)
          )
          (and
            (LeftOf10 ?x)
            (RightOf13 ?x)
          )
          (and
            (RightOf11 ?x)
            (LeftOf3 ?x)
          )
          (and
            (LeftOf12 ?x)
            (RightOf12 ?x)
          )
          (and
            (RightOf15 ?x)
            (LeftOf9 ?x)
          )
          (and
            (RightOf14 ?x)
            (LeftOf3 ?x)
          )
          (and
            (LeftOf7 ?x)
            (RightOf11 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf15 ?x)
          )
          (and
            (RightOf14 ?x)
            (LeftOf11 ?x)
          )
          (and
            (RightOf15 ?x)
            (LeftOf7 ?x)
          )
          (and
            (RightOf16 ?x)
            (LeftOf1 ?x)
          )
          (and
            (RightOf14 ?x)
            (LeftOf1 ?x)
          )
          (and
            (LeftOf4 ?x)
            (RightOf13 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf11 ?x)
          )
          (and
            (LeftOf10 ?x)
            (RightOf17 ?x)
          )
          (and
            (RightOf12 ?x)
            (LeftOf6 ?x)
          )
          (and
            (RightOf15 ?x)
            (LeftOf4 ?x)
          )
          (and
            (RightOf15 ?x)
            (LeftOf8 ?x)
          )
          (and
            (LeftOf11 ?x)
            (RightOf13 ?x)
          )
          (and
            (LeftOf9 ?x)
            (RightOf11 ?x)
          )
          (and
            (RightOf12 ?x)
            (LeftOf5 ?x)
          )
          (and
            (RightOf12 ?x)
            (LeftOf7 ?x)
          )
          (and
            (RightOf14 ?x)
            (LeftOf8 ?x)
          )
          (and
            (RightOf12 ?x)
            (LeftOf1 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf17 ?x)
          )
          (and
            (RightOf14 ?x)
            (LeftOf10 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf16 ?x)
          )
          (and
            (RightOf15 ?x)
            (LeftOf3 ?x)
          )
          (and
            (RightOf17 ?x)
            (LeftOf2 ?x)
          )
          (and
            (RightOf14 ?x)
            (LeftOf4 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf12 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf13 ?x)
          )
          (and
            (LeftOf8 ?x)
            (RightOf13 ?x)
          )
          (and
            (RightOf13 ?x)
            (LeftOf3 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf13 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf14 ?x)
          )
          (and
            (RightOf17 ?x)
            (LeftOf3 ?x)
          )
          (and
            (LeftOf8 ?x)
            (RightOf16 ?x)
          )
          (and
            (RightOf12 ?x)
            (LeftOf9 ?x)
          )
          (and
            (LeftOf11 ?x)
            (RightOf11 ?x)
          )
          (and
            (RightOf12 ?x)
            (LeftOf2 ?x)
          )
          (and
            (LeftOf10 ?x)
            (RightOf14 ?x)
          )
          (and
            (LeftOf12 ?x)
            (RightOf11 ?x)
          )
          (and
            (LeftOf12 ?x)
            (RightOf14 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf11 ?x)
          )
          (and
            (LeftOf7 ?x)
            (RightOf13 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf15 ?x)
          )
          (and
            (RightOf16 ?x)
            (LeftOf11 ?x)
          )
          (and
            (LeftOf9 ?x)
            (RightOf13 ?x)
          )
          (and
            (RightOf12 ?x)
            (LeftOf10 ?x)
          )
          (and
            (RightOf15 ?x)
            (LeftOf11 ?x)
          )
          (and
            (RightOf15 ?x)
            (LeftOf1 ?x)
          )
        )
        (and
          (Column10 ?x)
        )
      )
      (when
        (or
          (and
            (RightOf15 ?x)
            (LeftOf7 ?x)
          )
          (and
            (LeftOf8 ?x)
            (RightOf17 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf14 ?x)
          )
          (and
            (RightOf12 ?x)
            (LeftOf11 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf13 ?x)
          )
          (and
            (RightOf14 ?x)
            (LeftOf9 ?x)
          )
          (and
            (RightOf14 ?x)
            (LeftOf1 ?x)
          )
          (and
            (RightOf12 ?x)
            (LeftOf12 ?x)
          )
          (and
            (RightOf13 ?x)
            (LeftOf3 ?x)
          )
          (and
            (LeftOf13 ?x)
            (RightOf16 ?x)
          )
          (and
            (LeftOf4 ?x)
            (RightOf13 ?x)
          )
          (and
            (RightOf17 ?x)
            (LeftOf11 ?x)
          )
          (and
            (LeftOf13 ?x)
            (RightOf17 ?x)
          )
          (and
            (RightOf15 ?x)
            (LeftOf10 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf13 ?x)
          )
          (and
            (LeftOf1 ?x)
            (RightOf13 ?x)
          )
          (and
            (LeftOf13 ?x)
            (RightOf13 ?x)
          )
          (and
            (LeftOf12 ?x)
            (RightOf13 ?x)
          )
          (and
            (LeftOf10 ?x)
            (RightOf17 ?x)
          )
          (and
            (RightOf15 ?x)
            (LeftOf2 ?x)
          )
          (and
            (RightOf17 ?x)
            (LeftOf3 ?x)
          )
          (and
            (RightOf12 ?x)
            (LeftOf6 ?x)
          )
          (and
            (RightOf14 ?x)
            (LeftOf7 ?x)
          )
          (and
            (LeftOf3 ?x)
            (RightOf13 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf16 ?x)
          )
          (and
            (RightOf12 ?x)
            (LeftOf4 ?x)
          )
          (and
            (RightOf17 ?x)
            (LeftOf9 ?x)
          )
          (and
            (RightOf17 ?x)
            (LeftOf1 ?x)
          )
          (and
            (RightOf14 ?x)
            (LeftOf2 ?x)
          )
          (and
            (RightOf12 ?x)
            (LeftOf3 ?x)
          )
          (and
            (RightOf15 ?x)
            (LeftOf8 ?x)
          )
          (and
            (LeftOf11 ?x)
            (RightOf13 ?x)
          )
          (and
            (LeftOf10 ?x)
            (RightOf16 ?x)
          )
          (and
            (RightOf14 ?x)
            (LeftOf13 ?x)
          )
          (and
            (RightOf12 ?x)
            (LeftOf9 ?x)
          )
          (and
            (RightOf12 ?x)
            (LeftOf5 ?x)
          )
          (and
            (RightOf12 ?x)
            (LeftOf13 ?x)
          )
          (and
            (RightOf12 ?x)
            (LeftOf2 ?x)
          )
          (and
            (RightOf14 ?x)
            (LeftOf8 ?x)
          )
          (and
            (RightOf12 ?x)
            (LeftOf7 ?x)
          )
          (and
            (RightOf16 ?x)
            (LeftOf3 ?x)
          )
          (and
            (LeftOf8 ?x)
            (RightOf13 ?x)
          )
          (and
            (RightOf12 ?x)
            (LeftOf8 ?x)
          )
          (and
            (LeftOf8 ?x)
            (RightOf16 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf17 ?x)
          )
          (and
            (RightOf14 ?x)
            (LeftOf10 ?x)
          )
          (and
            (LeftOf12 ?x)
            (RightOf14 ?x)
          )
          (and
            (LeftOf12 ?x)
            (RightOf17 ?x)
          )
          (and
            (RightOf15 ?x)
            (LeftOf3 ?x)
          )
          (and
            (RightOf17 ?x)
            (LeftOf2 ?x)
          )
          (and
            (RightOf14 ?x)
            (LeftOf4 ?x)
          )
          (and
            (RightOf16 ?x)
            (LeftOf1 ?x)
          )
          (and
            (RightOf15 ?x)
            (LeftOf4 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf16 ?x)
          )
          (and
            (LeftOf7 ?x)
            (RightOf16 ?x)
          )
          (and
            (LeftOf7 ?x)
            (RightOf13 ?x)
          )
          (and
            (LeftOf7 ?x)
            (RightOf17 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf15 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf14 ?x)
          )
          (and
            (LeftOf8 ?x)
            (RightOf14 ?x)
          )
          (and
            (RightOf16 ?x)
            (LeftOf4 ?x)
          )
          (and
            (LeftOf12 ?x)
            (RightOf16 ?x)
          )
          (and
            (RightOf16 ?x)
            (LeftOf11 ?x)
          )
          (and
            (LeftOf12 ?x)
            (RightOf15 ?x)
          )
          (and
            (RightOf15 ?x)
            (LeftOf13 ?x)
          )
          (and
            (LeftOf9 ?x)
            (RightOf13 ?x)
          )
          (and
            (RightOf12 ?x)
            (LeftOf1 ?x)
          )
          (and
            (LeftOf10 ?x)
            (RightOf13 ?x)
          )
          (and
            (RightOf12 ?x)
            (LeftOf10 ?x)
          )
          (and
            (RightOf17 ?x)
            (LeftOf4 ?x)
          )
          (and
            (RightOf16 ?x)
            (LeftOf9 ?x)
          )
          (and
            (RightOf15 ?x)
            (LeftOf9 ?x)
          )
          (and
            (RightOf15 ?x)
            (LeftOf11 ?x)
          )
          (and
            (RightOf14 ?x)
            (LeftOf3 ?x)
          )
          (and
            (LeftOf10 ?x)
            (RightOf14 ?x)
          )
          (and
            (RightOf15 ?x)
            (LeftOf1 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf16 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf17 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf13 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf15 ?x)
          )
          (and
            (RightOf14 ?x)
            (LeftOf11 ?x)
          )
        )
        (and
          (Column11 ?x)
        )
      )
      (when
        (or
          (and
            (RightOf15 ?x)
            (LeftOf7 ?x)
          )
          (and
            (LeftOf8 ?x)
            (RightOf17 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf14 ?x)
          )
          (and
            (RightOf14 ?x)
            (LeftOf9 ?x)
          )
          (and
            (RightOf14 ?x)
            (LeftOf1 ?x)
          )
          (and
            (RightOf13 ?x)
            (LeftOf3 ?x)
          )
          (and
            (LeftOf13 ?x)
            (RightOf16 ?x)
          )
          (and
            (LeftOf4 ?x)
            (RightOf13 ?x)
          )
          (and
            (RightOf17 ?x)
            (LeftOf11 ?x)
          )
          (and
            (RightOf15 ?x)
            (LeftOf14 ?x)
          )
          (and
            (LeftOf8 ?x)
            (RightOf13 ?x)
          )
          (and
            (LeftOf13 ?x)
            (RightOf17 ?x)
          )
          (and
            (RightOf15 ?x)
            (LeftOf10 ?x)
          )
          (and
            (RightOf17 ?x)
            (LeftOf14 ?x)
          )
          (and
            (LeftOf1 ?x)
            (RightOf13 ?x)
          )
          (and
            (LeftOf13 ?x)
            (RightOf13 ?x)
          )
          (and
            (LeftOf12 ?x)
            (RightOf13 ?x)
          )
          (and
            (LeftOf10 ?x)
            (RightOf17 ?x)
          )
          (and
            (LeftOf10 ?x)
            (RightOf16 ?x)
          )
          (and
            (RightOf17 ?x)
            (LeftOf3 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf13 ?x)
          )
          (and
            (RightOf14 ?x)
            (LeftOf7 ?x)
          )
          (and
            (RightOf15 ?x)
            (LeftOf4 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf16 ?x)
          )
          (and
            (RightOf17 ?x)
            (LeftOf9 ?x)
          )
          (and
            (RightOf17 ?x)
            (LeftOf1 ?x)
          )
          (and
            (RightOf14 ?x)
            (LeftOf2 ?x)
          )
          (and
            (LeftOf8 ?x)
            (RightOf16 ?x)
          )
          (and
            (RightOf15 ?x)
            (LeftOf8 ?x)
          )
          (and
            (LeftOf11 ?x)
            (RightOf13 ?x)
          )
          (and
            (RightOf16 ?x)
            (LeftOf14 ?x)
          )
          (and
            (RightOf14 ?x)
            (LeftOf13 ?x)
          )
          (and
            (LeftOf7 ?x)
            (RightOf16 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf13 ?x)
          )
          (and
            (RightOf14 ?x)
            (LeftOf8 ?x)
          )
          (and
            (LeftOf10 ?x)
            (RightOf14 ?x)
          )
          (and
            (RightOf16 ?x)
            (LeftOf3 ?x)
          )
          (and
            (LeftOf14 ?x)
            (RightOf13 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf17 ?x)
          )
          (and
            (RightOf14 ?x)
            (LeftOf10 ?x)
          )
          (and
            (LeftOf12 ?x)
            (RightOf14 ?x)
          )
          (and
            (LeftOf12 ?x)
            (RightOf17 ?x)
          )
          (and
            (RightOf15 ?x)
            (LeftOf3 ?x)
          )
          (and
            (RightOf17 ?x)
            (LeftOf2 ?x)
          )
          (and
            (RightOf14 ?x)
            (LeftOf4 ?x)
          )
          (and
            (RightOf16 ?x)
            (LeftOf1 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf16 ?x)
          )
          (and
            (LeftOf7 ?x)
            (RightOf13 ?x)
          )
          (and
            (LeftOf7 ?x)
            (RightOf17 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf15 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf14 ?x)
          )
          (and
            (LeftOf8 ?x)
            (RightOf14 ?x)
          )
          (and
            (RightOf16 ?x)
            (LeftOf4 ?x)
          )
          (and
            (LeftOf12 ?x)
            (RightOf16 ?x)
          )
          (and
            (RightOf16 ?x)
            (LeftOf11 ?x)
          )
          (and
            (LeftOf12 ?x)
            (RightOf15 ?x)
          )
          (and
            (RightOf15 ?x)
            (LeftOf13 ?x)
          )
          (and
            (LeftOf9 ?x)
            (RightOf13 ?x)
          )
          (and
            (LeftOf10 ?x)
            (RightOf13 ?x)
          )
          (and
            (RightOf17 ?x)
            (LeftOf4 ?x)
          )
          (and
            (RightOf16 ?x)
            (LeftOf9 ?x)
          )
          (and
            (RightOf15 ?x)
            (LeftOf9 ?x)
          )
          (and
            (RightOf15 ?x)
            (LeftOf11 ?x)
          )
          (and
            (RightOf14 ?x)
            (LeftOf3 ?x)
          )
          (and
            (RightOf14 ?x)
            (LeftOf14 ?x)
          )
          (and
            (RightOf15 ?x)
            (LeftOf1 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf16 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf17 ?x)
          )
          (and
            (RightOf14 ?x)
            (LeftOf11 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf13 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf15 ?x)
          )
          (and
            (RightOf15 ?x)
            (LeftOf2 ?x)
          )
        )
        (and
          (Column12 ?x)
        )
      )
      (when
        (or
          (and
            (RightOf15 ?x)
            (LeftOf7 ?x)
          )
          (and
            (LeftOf8 ?x)
            (RightOf17 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf14 ?x)
          )
          (and
            (LeftOf15 ?x)
            (RightOf14 ?x)
          )
          (and
            (RightOf14 ?x)
            (LeftOf9 ?x)
          )
          (and
            (RightOf14 ?x)
            (LeftOf1 ?x)
          )
          (and
            (LeftOf13 ?x)
            (RightOf16 ?x)
          )
          (and
            (RightOf17 ?x)
            (LeftOf11 ?x)
          )
          (and
            (RightOf15 ?x)
            (LeftOf10 ?x)
          )
          (and
            (RightOf17 ?x)
            (LeftOf14 ?x)
          )
          (and
            (RightOf15 ?x)
            (LeftOf14 ?x)
          )
          (and
            (LeftOf10 ?x)
            (RightOf17 ?x)
          )
          (and
            (LeftOf10 ?x)
            (RightOf16 ?x)
          )
          (and
            (RightOf17 ?x)
            (LeftOf3 ?x)
          )
          (and
            (LeftOf15 ?x)
            (RightOf15 ?x)
          )
          (and
            (RightOf14 ?x)
            (LeftOf7 ?x)
          )
          (and
            (RightOf15 ?x)
            (LeftOf4 ?x)
          )
          (and
            (RightOf16 ?x)
            (LeftOf1 ?x)
          )
          (and
            (RightOf17 ?x)
            (LeftOf9 ?x)
          )
          (and
            (RightOf17 ?x)
            (LeftOf1 ?x)
          )
          (and
            (RightOf14 ?x)
            (LeftOf2 ?x)
          )
          (and
            (RightOf15 ?x)
            (LeftOf3 ?x)
          )
          (and
            (RightOf15 ?x)
            (LeftOf8 ?x)
          )
          (and
            (RightOf16 ?x)
            (LeftOf14 ?x)
          )
          (and
            (RightOf14 ?x)
            (LeftOf13 ?x)
          )
          (and
            (LeftOf7 ?x)
            (RightOf16 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf16 ?x)
          )
          (and
            (RightOf14 ?x)
            (LeftOf8 ?x)
          )
          (and
            (RightOf16 ?x)
            (LeftOf3 ?x)
          )
          (and
            (LeftOf15 ?x)
            (RightOf17 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf17 ?x)
          )
          (and
            (RightOf14 ?x)
            (LeftOf10 ?x)
          )
          (and
            (LeftOf12 ?x)
            (RightOf14 ?x)
          )
          (and
            (LeftOf12 ?x)
            (RightOf17 ?x)
          )
          (and
            (LeftOf8 ?x)
            (RightOf16 ?x)
          )
          (and
            (RightOf17 ?x)
            (LeftOf2 ?x)
          )
          (and
            (RightOf14 ?x)
            (LeftOf4 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf16 ?x)
          )
          (and
            (LeftOf7 ?x)
            (RightOf17 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf15 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf14 ?x)
          )
          (and
            (RightOf16 ?x)
            (LeftOf4 ?x)
          )
          (and
            (LeftOf12 ?x)
            (RightOf16 ?x)
          )
          (and
            (RightOf16 ?x)
            (LeftOf11 ?x)
          )
          (and
            (LeftOf12 ?x)
            (RightOf15 ?x)
          )
          (and
            (RightOf15 ?x)
            (LeftOf13 ?x)
          )
          (and
            (LeftOf13 ?x)
            (RightOf17 ?x)
          )
          (and
            (RightOf15 ?x)
            (LeftOf2 ?x)
          )
          (and
            (LeftOf15 ?x)
            (RightOf16 ?x)
          )
          (and
            (RightOf17 ?x)
            (LeftOf4 ?x)
          )
          (and
            (RightOf16 ?x)
            (LeftOf9 ?x)
          )
          (and
            (RightOf15 ?x)
            (LeftOf9 ?x)
          )
          (and
            (RightOf15 ?x)
            (LeftOf11 ?x)
          )
          (and
            (RightOf14 ?x)
            (LeftOf3 ?x)
          )
          (and
            (RightOf14 ?x)
            (LeftOf14 ?x)
          )
          (and
            (RightOf15 ?x)
            (LeftOf1 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf16 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf17 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf15 ?x)
          )
          (and
            (RightOf14 ?x)
            (LeftOf11 ?x)
          )
        )
        (and
          (Column13 ?x)
        )
      )
      (when
        (or
          (and
            (RightOf15 ?x)
            (LeftOf7 ?x)
          )
          (and
            (RightOf15 ?x)
            (LeftOf16 ?x)
          )
          (and
            (RightOf16 ?x)
            (LeftOf1 ?x)
          )
          (and
            (LeftOf8 ?x)
            (RightOf17 ?x)
          )
          (and
            (LeftOf13 ?x)
            (RightOf16 ?x)
          )
          (and
            (RightOf17 ?x)
            (LeftOf11 ?x)
          )
          (and
            (LeftOf13 ?x)
            (RightOf17 ?x)
          )
          (and
            (RightOf15 ?x)
            (LeftOf10 ?x)
          )
          (and
            (RightOf17 ?x)
            (LeftOf14 ?x)
          )
          (and
            (RightOf15 ?x)
            (LeftOf14 ?x)
          )
          (and
            (LeftOf10 ?x)
            (RightOf17 ?x)
          )
          (and
            (RightOf16 ?x)
            (LeftOf14 ?x)
          )
          (and
            (LeftOf16 ?x)
            (RightOf16 ?x)
          )
          (and
            (RightOf15 ?x)
            (LeftOf4 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf16 ?x)
          )
          (and
            (RightOf17 ?x)
            (LeftOf9 ?x)
          )
          (and
            (RightOf17 ?x)
            (LeftOf1 ?x)
          )
          (and
            (RightOf17 ?x)
            (LeftOf3 ?x)
          )
          (and
            (LeftOf8 ?x)
            (RightOf16 ?x)
          )
          (and
            (RightOf15 ?x)
            (LeftOf8 ?x)
          )
          (and
            (LeftOf10 ?x)
            (RightOf16 ?x)
          )
          (and
            (LeftOf7 ?x)
            (RightOf16 ?x)
          )
          (and
            (RightOf16 ?x)
            (LeftOf3 ?x)
          )
          (and
            (LeftOf15 ?x)
            (RightOf17 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf17 ?x)
          )
          (and
            (LeftOf12 ?x)
            (RightOf17 ?x)
          )
          (and
            (RightOf15 ?x)
            (LeftOf3 ?x)
          )
          (and
            (RightOf17 ?x)
            (LeftOf2 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf16 ?x)
          )
          (and
            (LeftOf7 ?x)
            (RightOf17 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf15 ?x)
          )
          (and
            (RightOf16 ?x)
            (LeftOf4 ?x)
          )
          (and
            (LeftOf12 ?x)
            (RightOf16 ?x)
          )
          (and
            (RightOf16 ?x)
            (LeftOf11 ?x)
          )
          (and
            (LeftOf12 ?x)
            (RightOf15 ?x)
          )
          (and
            (RightOf15 ?x)
            (LeftOf13 ?x)
          )
          (and
            (RightOf15 ?x)
            (LeftOf15 ?x)
          )
          (and
            (LeftOf15 ?x)
            (RightOf16 ?x)
          )
          (and
            (RightOf17 ?x)
            (LeftOf4 ?x)
          )
          (and
            (RightOf16 ?x)
            (LeftOf9 ?x)
          )
          (and
            (RightOf15 ?x)
            (LeftOf9 ?x)
          )
          (and
            (RightOf15 ?x)
            (LeftOf11 ?x)
          )
          (and
            (RightOf17 ?x)
            (LeftOf16 ?x)
          )
          (and
            (RightOf15 ?x)
            (LeftOf1 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf16 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf17 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf15 ?x)
          )
          (and
            (RightOf15 ?x)
            (LeftOf2 ?x)
          )
        )
        (and
          (Column14 ?x)
        )
      )
      (when
        (or
          (and
            (LeftOf5 ?x)
            (RightOf17 ?x)
          )
          (and
            (LeftOf8 ?x)
            (RightOf17 ?x)
          )
          (and
            (RightOf16 ?x)
            (LeftOf1 ?x)
          )
          (and
            (LeftOf13 ?x)
            (RightOf16 ?x)
          )
          (and
            (RightOf17 ?x)
            (LeftOf11 ?x)
          )
          (and
            (RightOf17 ?x)
            (LeftOf14 ?x)
          )
          (and
            (LeftOf10 ?x)
            (RightOf17 ?x)
          )
          (and
            (LeftOf10 ?x)
            (RightOf16 ?x)
          )
          (and
            (LeftOf16 ?x)
            (RightOf16 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf16 ?x)
          )
          (and
            (RightOf17 ?x)
            (LeftOf9 ?x)
          )
          (and
            (RightOf17 ?x)
            (LeftOf1 ?x)
          )
          (and
            (RightOf17 ?x)
            (LeftOf3 ?x)
          )
          (and
            (RightOf16 ?x)
            (LeftOf14 ?x)
          )
          (and
            (RightOf16 ?x)
            (LeftOf17 ?x)
          )
          (and
            (LeftOf7 ?x)
            (RightOf16 ?x)
          )
          (and
            (RightOf16 ?x)
            (LeftOf3 ?x)
          )
          (and
            (LeftOf15 ?x)
            (RightOf17 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf17 ?x)
          )
          (and
            (LeftOf12 ?x)
            (RightOf17 ?x)
          )
          (and
            (LeftOf8 ?x)
            (RightOf16 ?x)
          )
          (and
            (RightOf17 ?x)
            (LeftOf2 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf16 ?x)
          )
          (and
            (LeftOf7 ?x)
            (RightOf17 ?x)
          )
          (and
            (RightOf17 ?x)
            (LeftOf17 ?x)
          )
          (and
            (RightOf16 ?x)
            (LeftOf4 ?x)
          )
          (and
            (LeftOf12 ?x)
            (RightOf16 ?x)
          )
          (and
            (RightOf16 ?x)
            (LeftOf11 ?x)
          )
          (and
            (LeftOf13 ?x)
            (RightOf17 ?x)
          )
          (and
            (LeftOf15 ?x)
            (RightOf16 ?x)
          )
          (and
            (RightOf17 ?x)
            (LeftOf4 ?x)
          )
          (and
            (RightOf16 ?x)
            (LeftOf9 ?x)
          )
          (and
            (RightOf17 ?x)
            (LeftOf16 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf16 ?x)
          )
        )
        (and
          (Column15 ?x)
        )
      )
      (when
        (or
          (and
            (LeftOf5 ?x)
            (RightOf17 ?x)
          )
          (and
            (LeftOf13 ?x)
            (RightOf17 ?x)
          )
          (and
            (LeftOf15 ?x)
            (RightOf17 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf17 ?x)
          )
          (and
            (LeftOf12 ?x)
            (RightOf17 ?x)
          )
          (and
            (RightOf17 ?x)
            (LeftOf9 ?x)
          )
          (and
            (LeftOf8 ?x)
            (RightOf17 ?x)
          )
          (and
            (LeftOf18 ?x)
            (RightOf17 ?x)
          )
          (and
            (RightOf17 ?x)
            (LeftOf11 ?x)
          )
          (and
            (RightOf17 ?x)
            (LeftOf2 ?x)
          )
          (and
            (RightOf17 ?x)
            (LeftOf16 ?x)
          )
          (and
            (RightOf17 ?x)
            (LeftOf1 ?x)
          )
          (and
            (RightOf17 ?x)
            (LeftOf14 ?x)
          )
          (and
            (RightOf17 ?x)
            (LeftOf4 ?x)
          )
          (and
            (RightOf17 ?x)
            (LeftOf17 ?x)
          )
          (and
            (LeftOf10 ?x)
            (RightOf17 ?x)
          )
          (and
            (RightOf17 ?x)
            (LeftOf3 ?x)
          )
          (and
            (LeftOf7 ?x)
            (RightOf17 ?x)
          )
        )
        (and
          (Column16 ?x)
        )
      )
    )
  )
  (:action moveUp
    :parameters ( ?x )
    :precondition ( and
      (not (CheckConsistency))
      (not (Error))
      (or
        (BelowOf7 ?x)
        (AboveOf9 ?x)
        (BelowOf18 ?x)
        (BelowOf1 ?x)
        (BelowOf17 ?x)
        (AboveOf7 ?x)
        (BelowOf16 ?x)
        (AboveOf1 ?x)
        (AboveOf11 ?x)
        (BelowOf14 ?x)
        (BelowOf8 ?x)
        (BelowOf3 ?x)
        (AboveOf14 ?x)
        (AboveOf3 ?x)
        (BelowOf9 ?x)
        (AboveOf16 ?x)
        (BelowOf2 ?x)
        (BelowOf5 ?x)
        (BelowOf11 ?x)
        (BelowOf10 ?x)
        (BelowOf13 ?x)
        (AboveOf17 ?x)
        (BelowOf4 ?x)
        (AboveOf10 ?x)
        (AboveOf0 ?x)
        (AboveOf8 ?x)
        (AboveOf6 ?x)
        (AboveOf5 ?x)
        (AboveOf13 ?x)
        (Rows ?x)
        (AboveOf4 ?x)
        (BelowOf12 ?x)
        (AboveOf15 ?x)
        (AboveOf12 ?x)
        (BelowOf6 ?x)
        (BelowOf15 ?x)
        (AboveOf2 ?x)
      )
    )
    :effect ( and
      (CheckConsistency)
      (when
        (or
          (AboveOf1 ?x)
          (AboveOf5 ?x)
          (AboveOf14 ?x)
          (AboveOf13 ?x)
          (AboveOf3 ?x)
          (AboveOf4 ?x)
          (AboveOf16 ?x)
          (AboveOf9 ?x)
          (AboveOf8 ?x)
          (AboveOf15 ?x)
          (AboveOf17 ?x)
          (AboveOf12 ?x)
          (AboveOf7 ?x)
          (AboveOf0 ?x)
          (AboveOf2 ?x)
          (AboveOf10 ?x)
          (AboveOf11 ?x)
          (AboveOf6 ?x)
        )
        (and
          (AboveOf1 ?x)
        )
      )
      (when
        (or
          (AboveOf1 ?x)
          (AboveOf5 ?x)
          (AboveOf14 ?x)
          (AboveOf13 ?x)
          (AboveOf3 ?x)
          (AboveOf4 ?x)
          (AboveOf16 ?x)
          (AboveOf9 ?x)
          (AboveOf8 ?x)
          (AboveOf15 ?x)
          (AboveOf17 ?x)
          (AboveOf12 ?x)
          (AboveOf7 ?x)
          (AboveOf2 ?x)
          (AboveOf10 ?x)
          (AboveOf11 ?x)
          (AboveOf6 ?x)
        )
        (and
          (AboveOf2 ?x)
        )
      )
      (when
        (or
          (AboveOf5 ?x)
          (AboveOf14 ?x)
          (AboveOf13 ?x)
          (AboveOf3 ?x)
          (AboveOf4 ?x)
          (AboveOf16 ?x)
          (AboveOf9 ?x)
          (AboveOf8 ?x)
          (AboveOf15 ?x)
          (AboveOf17 ?x)
          (AboveOf12 ?x)
          (AboveOf7 ?x)
          (AboveOf2 ?x)
          (AboveOf10 ?x)
          (AboveOf6 ?x)
          (AboveOf11 ?x)
        )
        (and
          (AboveOf3 ?x)
        )
      )
      (when
        (or
          (AboveOf5 ?x)
          (AboveOf14 ?x)
          (AboveOf13 ?x)
          (AboveOf3 ?x)
          (AboveOf4 ?x)
          (AboveOf16 ?x)
          (AboveOf9 ?x)
          (AboveOf15 ?x)
          (AboveOf17 ?x)
          (AboveOf12 ?x)
          (AboveOf7 ?x)
          (AboveOf8 ?x)
          (AboveOf10 ?x)
          (AboveOf6 ?x)
          (AboveOf11 ?x)
        )
        (and
          (AboveOf4 ?x)
        )
      )
      (when
        (or
          (AboveOf5 ?x)
          (AboveOf14 ?x)
          (AboveOf13 ?x)
          (AboveOf4 ?x)
          (AboveOf16 ?x)
          (AboveOf9 ?x)
          (AboveOf15 ?x)
          (AboveOf17 ?x)
          (AboveOf12 ?x)
          (AboveOf7 ?x)
          (AboveOf8 ?x)
          (AboveOf10 ?x)
          (AboveOf6 ?x)
          (AboveOf11 ?x)
        )
        (and
          (AboveOf5 ?x)
        )
      )
      (when
        (or
          (AboveOf5 ?x)
          (AboveOf14 ?x)
          (AboveOf13 ?x)
          (AboveOf16 ?x)
          (AboveOf9 ?x)
          (AboveOf15 ?x)
          (AboveOf17 ?x)
          (AboveOf12 ?x)
          (AboveOf10 ?x)
          (AboveOf8 ?x)
          (AboveOf7 ?x)
          (AboveOf6 ?x)
          (AboveOf11 ?x)
        )
        (and
          (AboveOf6 ?x)
        )
      )
      (when
        (or
          (AboveOf14 ?x)
          (AboveOf13 ?x)
          (AboveOf16 ?x)
          (AboveOf9 ?x)
          (AboveOf15 ?x)
          (AboveOf17 ?x)
          (AboveOf12 ?x)
          (AboveOf10 ?x)
          (AboveOf8 ?x)
          (AboveOf7 ?x)
          (AboveOf6 ?x)
          (AboveOf11 ?x)
        )
        (and
          (AboveOf7 ?x)
        )
      )
      (when
        (or
          (AboveOf14 ?x)
          (AboveOf13 ?x)
          (AboveOf16 ?x)
          (AboveOf9 ?x)
          (AboveOf15 ?x)
          (AboveOf17 ?x)
          (AboveOf12 ?x)
          (AboveOf10 ?x)
          (AboveOf8 ?x)
          (AboveOf7 ?x)
          (AboveOf11 ?x)
        )
        (and
          (AboveOf8 ?x)
        )
      )
      (when
        (or
          (AboveOf14 ?x)
          (AboveOf13 ?x)
          (AboveOf16 ?x)
          (AboveOf9 ?x)
          (AboveOf15 ?x)
          (AboveOf17 ?x)
          (AboveOf12 ?x)
          (AboveOf10 ?x)
          (AboveOf8 ?x)
          (AboveOf11 ?x)
        )
        (and
          (AboveOf9 ?x)
        )
      )
      (when
        (or
          (AboveOf14 ?x)
          (AboveOf13 ?x)
          (AboveOf16 ?x)
          (AboveOf9 ?x)
          (AboveOf15 ?x)
          (AboveOf17 ?x)
          (AboveOf12 ?x)
          (AboveOf10 ?x)
          (AboveOf11 ?x)
        )
        (and
          (AboveOf10 ?x)
        )
      )
      (when
        (or
          (AboveOf14 ?x)
          (AboveOf13 ?x)
          (AboveOf16 ?x)
          (AboveOf15 ?x)
          (AboveOf17 ?x)
          (AboveOf12 ?x)
          (AboveOf10 ?x)
          (AboveOf11 ?x)
        )
        (and
          (AboveOf11 ?x)
        )
      )
      (when
        (or
          (AboveOf14 ?x)
          (AboveOf13 ?x)
          (AboveOf16 ?x)
          (AboveOf15 ?x)
          (AboveOf17 ?x)
          (AboveOf12 ?x)
          (AboveOf11 ?x)
        )
        (and
          (AboveOf12 ?x)
        )
      )
      (when
        (or
          (AboveOf14 ?x)
          (AboveOf13 ?x)
          (AboveOf16 ?x)
          (AboveOf15 ?x)
          (AboveOf17 ?x)
          (AboveOf12 ?x)
        )
        (and
          (AboveOf13 ?x)
        )
      )
      (when
        (or
          (AboveOf16 ?x)
          (AboveOf14 ?x)
          (AboveOf13 ?x)
          (AboveOf17 ?x)
          (AboveOf15 ?x)
        )
        (and
          (AboveOf14 ?x)
        )
      )
      (when
        (or
          (AboveOf15 ?x)
          (AboveOf14 ?x)
          (AboveOf17 ?x)
          (AboveOf16 ?x)
        )
        (and
          (AboveOf15 ?x)
        )
      )
      (when
        (or
          (AboveOf16 ?x)
          (AboveOf15 ?x)
          (AboveOf17 ?x)
        )
        (and
          (AboveOf16 ?x)
        )
      )
      (when
        (or
          (AboveOf16 ?x)
          (AboveOf17 ?x)
        )
        (and
          (AboveOf17 ?x)
        )
      )
      (when
        (BelowOf1 ?x)
        (and
          (BelowOf2 ?x)
          (not (BelowOf1 ?x))
        )
      )
      (when
        (or
          (BelowOf1 ?x)
          (BelowOf2 ?x)
        )
        (and
          (BelowOf3 ?x)
          (not (BelowOf2 ?x))
        )
      )
      (when
        (or
          (BelowOf3 ?x)
          (BelowOf1 ?x)
          (BelowOf2 ?x)
        )
        (and
          (BelowOf4 ?x)
          (not (BelowOf3 ?x))
        )
      )
      (when
        (or
          (BelowOf4 ?x)
          (BelowOf3 ?x)
          (BelowOf1 ?x)
          (BelowOf2 ?x)
        )
        (and
          (BelowOf5 ?x)
          (not (BelowOf4 ?x))
        )
      )
      (when
        (or
          (BelowOf5 ?x)
          (BelowOf4 ?x)
          (BelowOf3 ?x)
          (BelowOf1 ?x)
          (BelowOf2 ?x)
        )
        (and
          (BelowOf6 ?x)
          (not (BelowOf5 ?x))
        )
      )
      (when
        (or
          (BelowOf3 ?x)
          (BelowOf4 ?x)
          (BelowOf1 ?x)
          (BelowOf2 ?x)
          (BelowOf5 ?x)
          (BelowOf6 ?x)
        )
        (and
          (BelowOf7 ?x)
          (not (BelowOf6 ?x))
        )
      )
      (when
        (or
          (BelowOf3 ?x)
          (BelowOf7 ?x)
          (BelowOf4 ?x)
          (BelowOf1 ?x)
          (BelowOf2 ?x)
          (BelowOf5 ?x)
          (BelowOf6 ?x)
        )
        (and
          (BelowOf8 ?x)
          (not (BelowOf7 ?x))
        )
      )
      (when
        (or
          (BelowOf8 ?x)
          (BelowOf3 ?x)
          (BelowOf7 ?x)
          (BelowOf4 ?x)
          (BelowOf1 ?x)
          (BelowOf2 ?x)
          (BelowOf5 ?x)
          (BelowOf6 ?x)
        )
        (and
          (BelowOf9 ?x)
          (not (BelowOf8 ?x))
        )
      )
      (when
        (or
          (BelowOf8 ?x)
          (BelowOf3 ?x)
          (BelowOf9 ?x)
          (BelowOf7 ?x)
          (BelowOf4 ?x)
          (BelowOf1 ?x)
          (BelowOf2 ?x)
          (BelowOf5 ?x)
          (BelowOf6 ?x)
        )
        (and
          (BelowOf10 ?x)
          (not (BelowOf9 ?x))
        )
      )
      (when
        (or
          (BelowOf8 ?x)
          (BelowOf3 ?x)
          (BelowOf9 ?x)
          (BelowOf7 ?x)
          (BelowOf4 ?x)
          (BelowOf1 ?x)
          (BelowOf2 ?x)
          (BelowOf5 ?x)
          (BelowOf6 ?x)
          (BelowOf10 ?x)
        )
        (and
          (BelowOf11 ?x)
          (not (BelowOf10 ?x))
        )
      )
      (when
        (or
          (BelowOf8 ?x)
          (BelowOf3 ?x)
          (BelowOf9 ?x)
          (BelowOf7 ?x)
          (BelowOf4 ?x)
          (BelowOf1 ?x)
          (BelowOf2 ?x)
          (BelowOf5 ?x)
          (BelowOf6 ?x)
          (BelowOf11 ?x)
          (BelowOf10 ?x)
        )
        (and
          (BelowOf12 ?x)
          (not (BelowOf11 ?x))
        )
      )
      (when
        (or
          (BelowOf8 ?x)
          (BelowOf3 ?x)
          (BelowOf9 ?x)
          (BelowOf7 ?x)
          (BelowOf12 ?x)
          (BelowOf4 ?x)
          (BelowOf1 ?x)
          (BelowOf2 ?x)
          (BelowOf5 ?x)
          (BelowOf6 ?x)
          (BelowOf11 ?x)
          (BelowOf10 ?x)
        )
        (and
          (BelowOf13 ?x)
          (not (BelowOf12 ?x))
        )
      )
      (when
        (or
          (BelowOf8 ?x)
          (BelowOf13 ?x)
          (BelowOf3 ?x)
          (BelowOf9 ?x)
          (BelowOf7 ?x)
          (BelowOf12 ?x)
          (BelowOf4 ?x)
          (BelowOf1 ?x)
          (BelowOf2 ?x)
          (BelowOf5 ?x)
          (BelowOf6 ?x)
          (BelowOf11 ?x)
          (BelowOf10 ?x)
        )
        (and
          (BelowOf14 ?x)
          (not (BelowOf13 ?x))
        )
      )
      (when
        (or
          (BelowOf14 ?x)
          (BelowOf8 ?x)
          (BelowOf13 ?x)
          (BelowOf3 ?x)
          (BelowOf9 ?x)
          (BelowOf7 ?x)
          (BelowOf12 ?x)
          (BelowOf4 ?x)
          (BelowOf1 ?x)
          (BelowOf2 ?x)
          (BelowOf5 ?x)
          (BelowOf6 ?x)
          (BelowOf11 ?x)
          (BelowOf10 ?x)
        )
        (and
          (BelowOf15 ?x)
          (not (BelowOf14 ?x))
        )
      )
      (when
        (or
          (BelowOf14 ?x)
          (BelowOf8 ?x)
          (BelowOf13 ?x)
          (BelowOf3 ?x)
          (BelowOf9 ?x)
          (BelowOf7 ?x)
          (BelowOf12 ?x)
          (BelowOf4 ?x)
          (BelowOf1 ?x)
          (BelowOf2 ?x)
          (BelowOf5 ?x)
          (BelowOf6 ?x)
          (BelowOf15 ?x)
          (BelowOf11 ?x)
          (BelowOf10 ?x)
        )
        (and
          (BelowOf16 ?x)
          (not (BelowOf15 ?x))
        )
      )
      (when
        (or
          (BelowOf14 ?x)
          (BelowOf8 ?x)
          (BelowOf13 ?x)
          (BelowOf3 ?x)
          (BelowOf9 ?x)
          (BelowOf7 ?x)
          (BelowOf12 ?x)
          (BelowOf4 ?x)
          (BelowOf1 ?x)
          (BelowOf2 ?x)
          (BelowOf5 ?x)
          (BelowOf11 ?x)
          (BelowOf6 ?x)
          (BelowOf15 ?x)
          (BelowOf16 ?x)
          (BelowOf10 ?x)
        )
        (and
          (BelowOf17 ?x)
          (not (BelowOf16 ?x))
        )
      )
      (when
        (or
          (BelowOf14 ?x)
          (BelowOf8 ?x)
          (BelowOf13 ?x)
          (BelowOf3 ?x)
          (BelowOf9 ?x)
          (BelowOf7 ?x)
          (BelowOf12 ?x)
          (BelowOf11 ?x)
          (BelowOf4 ?x)
          (BelowOf1 ?x)
          (BelowOf2 ?x)
          (BelowOf5 ?x)
          (BelowOf17 ?x)
          (BelowOf6 ?x)
          (BelowOf15 ?x)
          (BelowOf16 ?x)
          (BelowOf10 ?x)
        )
        (and
          (BelowOf18 ?x)
          (not (BelowOf17 ?x))
        )
      )
      (when
        (Row0 ?x)
        (and
          (Row1 ?x)
          (not (Row0 ?x))
        )
      )
      (when
        (Row1 ?x)
        (and
          (Row2 ?x)
          (not (Row1 ?x))
        )
      )
      (when
        (Row2 ?x)
        (and
          (Row3 ?x)
          (not (Row2 ?x))
        )
      )
      (when
        (Row3 ?x)
        (and
          (Row4 ?x)
          (not (Row3 ?x))
        )
      )
      (when
        (Row4 ?x)
        (and
          (Row5 ?x)
          (not (Row4 ?x))
        )
      )
      (when
        (Row5 ?x)
        (and
          (Row6 ?x)
          (not (Row5 ?x))
        )
      )
      (when
        (Row6 ?x)
        (and
          (Row7 ?x)
          (not (Row6 ?x))
        )
      )
      (when
        (Row7 ?x)
        (and
          (Row8 ?x)
          (not (Row7 ?x))
        )
      )
      (when
        (Row8 ?x)
        (and
          (Row9 ?x)
          (not (Row8 ?x))
        )
      )
      (when
        (Row9 ?x)
        (and
          (Row10 ?x)
          (not (Row9 ?x))
        )
      )
      (when
        (Row10 ?x)
        (and
          (Row11 ?x)
          (not (Row10 ?x))
        )
      )
      (when
        (Row11 ?x)
        (and
          (Row12 ?x)
          (not (Row11 ?x))
        )
      )
      (when
        (Row12 ?x)
        (and
          (Row13 ?x)
          (not (Row12 ?x))
        )
      )
      (when
        (Row13 ?x)
        (and
          (Row14 ?x)
          (not (Row13 ?x))
        )
      )
      (when
        (Row14 ?x)
        (and
          (Row15 ?x)
          (not (Row14 ?x))
        )
      )
      (when
        (Row15 ?x)
        (and
          (Row16 ?x)
          (not (Row15 ?x))
        )
      )
      (when
        (Row16 ?x)
        (and
          (Row17 ?x)
          (not (Row16 ?x))
        )
      )
      (when
        (or
          (and
            (BelowOf1 ?x)
            (AboveOf17 ?x)
          )
          (and
            (AboveOf9 ?x)
            (BelowOf1 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf3 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf8 ?x)
          )
          (and
            (AboveOf15 ?x)
            (BelowOf1 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf0 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf10 ?x)
          )
          (and
            (AboveOf16 ?x)
            (BelowOf1 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf1 ?x)
          )
          (and
            (AboveOf5 ?x)
            (BelowOf1 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf6 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf11 ?x)
          )
          (and
            (AboveOf7 ?x)
            (BelowOf1 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf1 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf12 ?x)
          )
          (and
            (AboveOf1 ?x)
            (BelowOf1 ?x)
          )
          (and
            (AboveOf2 ?x)
            (BelowOf1 ?x)
          )
          (and
            (AboveOf4 ?x)
            (BelowOf1 ?x)
          )
        )
        (and
          (Row1 ?x)
        )
      )
      (when
        (or
          (and
            (BelowOf2 ?x)
            (AboveOf3 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf8 ?x)
          )
          (and
            (AboveOf15 ?x)
            (BelowOf1 ?x)
          )
          (and
            (AboveOf5 ?x)
            (BelowOf2 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf12 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf12 ?x)
          )
          (and
            (AboveOf4 ?x)
            (BelowOf1 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf8 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf10 ?x)
          )
          (and
            (AboveOf9 ?x)
            (BelowOf1 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf3 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf11 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf10 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf17 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf1 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf6 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf1 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf9 ?x)
          )
          (and
            (AboveOf2 ?x)
            (BelowOf1 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf6 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf2 ?x)
          )
          (and
            (AboveOf17 ?x)
            (BelowOf1 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf4 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf16 ?x)
          )
          (and
            (AboveOf5 ?x)
            (BelowOf1 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf2 ?x)
          )
          (and
            (AboveOf7 ?x)
            (BelowOf1 ?x)
          )
          (and
            (AboveOf16 ?x)
            (BelowOf1 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf2 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf15 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf7 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf1 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf11 ?x)
          )
          (and
            (AboveOf1 ?x)
            (BelowOf1 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf17 ?x)
          )
        )
        (and
          (Row2 ?x)
        )
      )
      (when
        (or
          (and
            (BelowOf2 ?x)
            (AboveOf3 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf8 ?x)
          )
          (and
            (AboveOf15 ?x)
            (BelowOf1 ?x)
          )
          (and
            (AboveOf5 ?x)
            (BelowOf2 ?x)
          )
          (and
            (BelowOf3 ?x)
            (AboveOf11 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf3 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf12 ?x)
          )
          (and
            (BelowOf3 ?x)
            (AboveOf12 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf12 ?x)
          )
          (and
            (AboveOf4 ?x)
            (BelowOf1 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf8 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf10 ?x)
          )
          (and
            (AboveOf9 ?x)
            (BelowOf1 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf3 ?x)
          )
          (and
            (AboveOf2 ?x)
            (BelowOf2 ?x)
          )
          (and
            (AboveOf10 ?x)
            (BelowOf3 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf1 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf10 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf17 ?x)
          )
          (and
            (AboveOf4 ?x)
            (BelowOf3 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf6 ?x)
          )
          (and
            (BelowOf3 ?x)
            (AboveOf8 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf1 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf9 ?x)
          )
          (and
            (BelowOf3 ?x)
            (AboveOf3 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf6 ?x)
          )
          (and
            (AboveOf2 ?x)
            (BelowOf3 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf2 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf4 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf16 ?x)
          )
          (and
            (AboveOf15 ?x)
            (BelowOf3 ?x)
          )
          (and
            (AboveOf9 ?x)
            (BelowOf3 ?x)
          )
          (and
            (AboveOf5 ?x)
            (BelowOf1 ?x)
          )
          (and
            (AboveOf16 ?x)
            (BelowOf3 ?x)
          )
          (and
            (AboveOf7 ?x)
            (BelowOf1 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf2 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf3 ?x)
          )
          (and
            (AboveOf17 ?x)
            (BelowOf3 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf15 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf7 ?x)
          )
          (and
            (AboveOf17 ?x)
            (BelowOf1 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf11 ?x)
          )
          (and
            (AboveOf7 ?x)
            (BelowOf3 ?x)
          )
          (and
            (AboveOf2 ?x)
            (BelowOf1 ?x)
          )
          (and
            (AboveOf6 ?x)
            (BelowOf3 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf11 ?x)
          )
          (and
            (AboveOf16 ?x)
            (BelowOf1 ?x)
          )
          (and
            (AboveOf5 ?x)
            (BelowOf3 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf17 ?x)
          )
        )
        (and
          (Row3 ?x)
        )
      )
      (when
        (or
          (and
            (BelowOf2 ?x)
            (AboveOf3 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf8 ?x)
          )
          (and
            (AboveOf15 ?x)
            (BelowOf1 ?x)
          )
          (and
            (AboveOf5 ?x)
            (BelowOf2 ?x)
          )
          (and
            (BelowOf3 ?x)
            (AboveOf11 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf12 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf2 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf3 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf4 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf11 ?x)
          )
          (and
            (BelowOf3 ?x)
            (AboveOf12 ?x)
          )
          (and
            (AboveOf7 ?x)
            (BelowOf4 ?x)
          )
          (and
            (AboveOf11 ?x)
            (BelowOf3 ?x)
          )
          (and
            (AboveOf4 ?x)
            (BelowOf1 ?x)
          )
          (and
            (AboveOf15 ?x)
            (BelowOf4 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf10 ?x)
          )
          (and
            (AboveOf9 ?x)
            (BelowOf1 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf3 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf10 ?x)
          )
          (and
            (AboveOf10 ?x)
            (BelowOf3 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf1 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf10 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf12 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf17 ?x)
          )
          (and
            (AboveOf4 ?x)
            (BelowOf3 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf6 ?x)
          )
          (and
            (BelowOf3 ?x)
            (AboveOf8 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf1 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf9 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf6 ?x)
          )
          (and
            (AboveOf3 ?x)
            (BelowOf3 ?x)
          )
          (and
            (AboveOf5 ?x)
            (BelowOf4 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf12 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf4 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf16 ?x)
          )
          (and
            (AboveOf15 ?x)
            (BelowOf3 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf8 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf8 ?x)
          )
          (and
            (AboveOf5 ?x)
            (BelowOf1 ?x)
          )
          (and
            (AboveOf16 ?x)
            (BelowOf3 ?x)
          )
          (and
            (AboveOf7 ?x)
            (BelowOf1 ?x)
          )
          (and
            (AboveOf16 ?x)
            (BelowOf4 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf2 ?x)
          )
          (and
            (AboveOf4 ?x)
            (BelowOf4 ?x)
          )
          (and
            (AboveOf17 ?x)
            (BelowOf3 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf15 ?x)
          )
          (and
            (AboveOf9 ?x)
            (BelowOf3 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf7 ?x)
          )
          (and
            (AboveOf17 ?x)
            (BelowOf1 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf11 ?x)
          )
          (and
            (AboveOf16 ?x)
            (BelowOf1 ?x)
          )
          (and
            (AboveOf7 ?x)
            (BelowOf3 ?x)
          )
          (and
            (AboveOf6 ?x)
            (BelowOf3 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf11 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf6 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf17 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf3 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf4 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf9 ?x)
          )
          (and
            (AboveOf5 ?x)
            (BelowOf3 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf3 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf17 ?x)
          )
        )
        (and
          (Row4 ?x)
        )
      )
      (when
        (or
          (and
            (BelowOf5 ?x)
            (AboveOf14 ?x)
          )
          (and
            (AboveOf10 ?x)
            (BelowOf3 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf8 ?x)
          )
          (and
            (AboveOf15 ?x)
            (BelowOf1 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf6 ?x)
          )
          (and
            (BelowOf3 ?x)
            (AboveOf11 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf11 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf12 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf2 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf3 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf4 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf12 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf11 ?x)
          )
          (and
            (BelowOf3 ?x)
            (AboveOf12 ?x)
          )
          (and
            (AboveOf11 ?x)
            (BelowOf3 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf9 ?x)
          )
          (and
            (AboveOf4 ?x)
            (BelowOf1 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf8 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf10 ?x)
          )
          (and
            (AboveOf9 ?x)
            (BelowOf1 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf6 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf10 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf11 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf1 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf10 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf12 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf7 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf17 ?x)
          )
          (and
            (AboveOf4 ?x)
            (BelowOf3 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf5 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf6 ?x)
          )
          (and
            (BelowOf3 ?x)
            (AboveOf8 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf1 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf9 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf16 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf6 ?x)
          )
          (and
            (AboveOf9 ?x)
            (BelowOf3 ?x)
          )
          (and
            (AboveOf5 ?x)
            (BelowOf4 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf12 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf4 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf16 ?x)
          )
          (and
            (AboveOf15 ?x)
            (BelowOf3 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf10 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf8 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf13 ?x)
          )
          (and
            (AboveOf5 ?x)
            (BelowOf1 ?x)
          )
          (and
            (AboveOf16 ?x)
            (BelowOf3 ?x)
          )
          (and
            (AboveOf5 ?x)
            (BelowOf2 ?x)
          )
          (and
            (AboveOf7 ?x)
            (BelowOf1 ?x)
          )
          (and
            (AboveOf16 ?x)
            (BelowOf4 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf2 ?x)
          )
          (and
            (AboveOf15 ?x)
            (BelowOf4 ?x)
          )
          (and
            (AboveOf17 ?x)
            (BelowOf3 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf15 ?x)
          )
          (and
            (AboveOf4 ?x)
            (BelowOf4 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf7 ?x)
          )
          (and
            (AboveOf17 ?x)
            (BelowOf1 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf11 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf16 ?x)
          )
          (and
            (AboveOf16 ?x)
            (BelowOf1 ?x)
          )
          (and
            (AboveOf7 ?x)
            (BelowOf3 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf8 ?x)
          )
          (and
            (AboveOf6 ?x)
            (BelowOf3 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf15 ?x)
          )
          (and
            (AboveOf7 ?x)
            (BelowOf4 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf4 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf17 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf9 ?x)
          )
          (and
            (AboveOf5 ?x)
            (BelowOf3 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf4 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf3 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf17 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf17 ?x)
          )
        )
        (and
          (Row5 ?x)
        )
      )
      (when
        (or
          (and
            (BelowOf5 ?x)
            (AboveOf14 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf11 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf8 ?x)
          )
          (and
            (AboveOf15 ?x)
            (BelowOf1 ?x)
          )
          (and
            (AboveOf16 ?x)
            (BelowOf6 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf6 ?x)
          )
          (and
            (BelowOf3 ?x)
            (AboveOf11 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf11 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf12 ?x)
          )
          (and
            (AboveOf10 ?x)
            (BelowOf6 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf2 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf3 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf6 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf11 ?x)
          )
          (and
            (BelowOf3 ?x)
            (AboveOf12 ?x)
          )
          (and
            (AboveOf7 ?x)
            (BelowOf4 ?x)
          )
          (and
            (AboveOf11 ?x)
            (BelowOf3 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf9 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf16 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf8 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf10 ?x)
          )
          (and
            (AboveOf9 ?x)
            (BelowOf6 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf6 ?x)
          )
          (and
            (BelowOf6 ?x)
            (AboveOf8 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf10 ?x)
          )
          (and
            (AboveOf17 ?x)
            (BelowOf6 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf10 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf12 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf7 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf1 ?x)
          )
          (and
            (AboveOf6 ?x)
            (BelowOf6 ?x)
          )
          (and
            (BelowOf3 ?x)
            (AboveOf8 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf1 ?x)
          )
          (and
            (AboveOf16 ?x)
            (BelowOf4 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf6 ?x)
          )
          (and
            (AboveOf5 ?x)
            (BelowOf4 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf12 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf17 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf16 ?x)
          )
          (and
            (AboveOf15 ?x)
            (BelowOf3 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf10 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf8 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf13 ?x)
          )
          (and
            (AboveOf5 ?x)
            (BelowOf5 ?x)
          )
          (and
            (AboveOf7 ?x)
            (BelowOf6 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf17 ?x)
          )
          (and
            (AboveOf16 ?x)
            (BelowOf3 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf12 ?x)
          )
          (and
            (AboveOf7 ?x)
            (BelowOf1 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf9 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf2 ?x)
          )
          (and
            (AboveOf9 ?x)
            (BelowOf1 ?x)
          )
          (and
            (AboveOf15 ?x)
            (BelowOf4 ?x)
          )
          (and
            (AboveOf10 ?x)
            (BelowOf3 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf15 ?x)
          )
          (and
            (AboveOf9 ?x)
            (BelowOf3 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf7 ?x)
          )
          (and
            (AboveOf17 ?x)
            (BelowOf1 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf6 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf11 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf16 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf5 ?x)
          )
          (and
            (BelowOf6 ?x)
            (AboveOf12 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf6 ?x)
          )
          (and
            (AboveOf16 ?x)
            (BelowOf1 ?x)
          )
          (and
            (AboveOf7 ?x)
            (BelowOf3 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf8 ?x)
          )
          (and
            (AboveOf6 ?x)
            (BelowOf3 ?x)
          )
          (and
            (BelowOf6 ?x)
            (AboveOf11 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf15 ?x)
          )
          (and
            (AboveOf5 ?x)
            (BelowOf6 ?x)
          )
          (and
            (AboveOf5 ?x)
            (BelowOf1 ?x)
          )
          (and
            (AboveOf17 ?x)
            (BelowOf3 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf4 ?x)
          )
          (and
            (AboveOf15 ?x)
            (BelowOf6 ?x)
          )
          (and
            (AboveOf5 ?x)
            (BelowOf2 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf9 ?x)
          )
          (and
            (AboveOf5 ?x)
            (BelowOf3 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf3 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf17 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf4 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf17 ?x)
          )
        )
        (and
          (Row6 ?x)
        )
      )
      (when
        (or
          (and
            (BelowOf3 ?x)
            (AboveOf12 ?x)
          )
          (and
            (BelowOf7 ?x)
            (AboveOf12 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf11 ?x)
          )
          (and
            (AboveOf6 ?x)
            (BelowOf3 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf14 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf8 ?x)
          )
          (and
            (AboveOf7 ?x)
            (BelowOf6 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf15 ?x)
          )
          (and
            (BelowOf6 ?x)
            (AboveOf6 ?x)
          )
          (and
            (AboveOf17 ?x)
            (BelowOf1 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf6 ?x)
          )
          (and
            (BelowOf7 ?x)
            (AboveOf9 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf4 ?x)
          )
          (and
            (AboveOf16 ?x)
            (BelowOf6 ?x)
          )
          (and
            (BelowOf3 ?x)
            (AboveOf11 ?x)
          )
          (and
            (BelowOf7 ?x)
            (AboveOf11 ?x)
          )
          (and
            (BelowOf7 ?x)
            (AboveOf8 ?x)
          )
          (and
            (BelowOf7 ?x)
            (AboveOf17 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf11 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf16 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf8 ?x)
          )
          (and
            (AboveOf9 ?x)
            (BelowOf6 ?x)
          )
          (and
            (BelowOf6 ?x)
            (AboveOf8 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf12 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf7 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf6 ?x)
          )
          (and
            (BelowOf3 ?x)
            (AboveOf8 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf6 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf2 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf12 ?x)
          )
          (and
            (AboveOf7 ?x)
            (BelowOf4 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf9 ?x)
          )
          (and
            (AboveOf17 ?x)
            (BelowOf3 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf11 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf16 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf6 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf7 ?x)
          )
          (and
            (AboveOf15 ?x)
            (BelowOf6 ?x)
          )
          (and
            (BelowOf7 ?x)
            (AboveOf10 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf17 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf17 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf8 ?x)
          )
          (and
            (AboveOf15 ?x)
            (BelowOf1 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf6 ?x)
          )
          (and
            (AboveOf10 ?x)
            (BelowOf6 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf3 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf12 ?x)
          )
          (and
            (AboveOf11 ?x)
            (BelowOf3 ?x)
          )
          (and
            (AboveOf15 ?x)
            (BelowOf4 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf10 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf6 ?x)
          )
          (and
            (AboveOf17 ?x)
            (BelowOf6 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf10 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf1 ?x)
          )
          (and
            (AboveOf16 ?x)
            (BelowOf4 ?x)
          )
          (and
            (BelowOf6 ?x)
            (AboveOf10 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf16 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf13 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf3 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf5 ?x)
          )
          (and
            (BelowOf6 ?x)
            (AboveOf12 ?x)
          )
          (and
            (AboveOf7 ?x)
            (BelowOf3 ?x)
          )
          (and
            (BelowOf7 ?x)
            (AboveOf16 ?x)
          )
          (and
            (BelowOf6 ?x)
            (AboveOf11 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf4 ?x)
          )
          (and
            (AboveOf16 ?x)
            (BelowOf1 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf1 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf12 ?x)
          )
          (and
            (BelowOf7 ?x)
            (AboveOf7 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf9 ?x)
          )
          (and
            (AboveOf9 ?x)
            (BelowOf1 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf15 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf10 ?x)
          )
          (and
            (AboveOf10 ?x)
            (BelowOf3 ?x)
          )
          (and
            (AboveOf15 ?x)
            (BelowOf3 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf9 ?x)
          )
          (and
            (AboveOf15 ?x)
            (BelowOf7 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf7 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf17 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf11 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf10 ?x)
          )
          (and
            (AboveOf9 ?x)
            (BelowOf3 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf17 ?x)
          )
          (and
            (AboveOf16 ?x)
            (BelowOf3 ?x)
          )
          (and
            (AboveOf7 ?x)
            (BelowOf1 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf2 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf8 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf7 ?x)
          )
          (and
            (BelowOf7 ?x)
            (AboveOf6 ?x)
          )
        )
        (and
          (Row7 ?x)
        )
      )
      (when
        (or
          (and
            (BelowOf3 ?x)
            (AboveOf12 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf7 ?x)
          )
          (and
            (BelowOf7 ?x)
            (AboveOf12 ?x)
          )
          (and
            (AboveOf16 ?x)
            (BelowOf8 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf11 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf14 ?x)
          )
          (and
            (BelowOf8 ?x)
            (AboveOf12 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf8 ?x)
          )
          (and
            (AboveOf7 ?x)
            (BelowOf6 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf15 ?x)
          )
          (and
            (AboveOf17 ?x)
            (BelowOf1 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf6 ?x)
          )
          (and
            (BelowOf7 ?x)
            (AboveOf9 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf4 ?x)
          )
          (and
            (AboveOf15 ?x)
            (BelowOf8 ?x)
          )
          (and
            (AboveOf16 ?x)
            (BelowOf6 ?x)
          )
          (and
            (BelowOf3 ?x)
            (AboveOf11 ?x)
          )
          (and
            (BelowOf7 ?x)
            (AboveOf11 ?x)
          )
          (and
            (BelowOf7 ?x)
            (AboveOf8 ?x)
          )
          (and
            (BelowOf7 ?x)
            (AboveOf17 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf11 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf16 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf8 ?x)
          )
          (and
            (AboveOf9 ?x)
            (BelowOf6 ?x)
          )
          (and
            (BelowOf6 ?x)
            (AboveOf8 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf12 ?x)
          )
          (and
            (AboveOf7 ?x)
            (BelowOf7 ?x)
          )
          (and
            (BelowOf3 ?x)
            (AboveOf8 ?x)
          )
          (and
            (BelowOf8 ?x)
            (AboveOf8 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf16 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf2 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf12 ?x)
          )
          (and
            (AboveOf7 ?x)
            (BelowOf4 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf9 ?x)
          )
          (and
            (AboveOf17 ?x)
            (BelowOf3 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf11 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf16 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf6 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf7 ?x)
          )
          (and
            (AboveOf15 ?x)
            (BelowOf6 ?x)
          )
          (and
            (BelowOf7 ?x)
            (AboveOf10 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf17 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf17 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf8 ?x)
          )
          (and
            (AboveOf15 ?x)
            (BelowOf1 ?x)
          )
          (and
            (AboveOf7 ?x)
            (BelowOf8 ?x)
          )
          (and
            (AboveOf10 ?x)
            (BelowOf6 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf3 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf12 ?x)
          )
          (and
            (AboveOf11 ?x)
            (BelowOf3 ?x)
          )
          (and
            (AboveOf15 ?x)
            (BelowOf4 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf10 ?x)
          )
          (and
            (AboveOf17 ?x)
            (BelowOf6 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf10 ?x)
          )
          (and
            (AboveOf9 ?x)
            (BelowOf8 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf1 ?x)
          )
          (and
            (AboveOf16 ?x)
            (BelowOf4 ?x)
          )
          (and
            (BelowOf6 ?x)
            (AboveOf10 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf13 ?x)
          )
          (and
            (AboveOf10 ?x)
            (BelowOf8 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf3 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf5 ?x)
          )
          (and
            (BelowOf6 ?x)
            (AboveOf12 ?x)
          )
          (and
            (AboveOf7 ?x)
            (BelowOf3 ?x)
          )
          (and
            (BelowOf7 ?x)
            (AboveOf16 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf8 ?x)
          )
          (and
            (BelowOf6 ?x)
            (AboveOf11 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf4 ?x)
          )
          (and
            (AboveOf17 ?x)
            (BelowOf8 ?x)
          )
          (and
            (AboveOf16 ?x)
            (BelowOf1 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf8 ?x)
          )
          (and
            (AboveOf11 ?x)
            (BelowOf8 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf1 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf12 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf9 ?x)
          )
          (and
            (AboveOf9 ?x)
            (BelowOf1 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf15 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf10 ?x)
          )
          (and
            (AboveOf10 ?x)
            (BelowOf3 ?x)
          )
          (and
            (AboveOf15 ?x)
            (BelowOf3 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf9 ?x)
          )
          (and
            (AboveOf15 ?x)
            (BelowOf7 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf7 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf17 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf11 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf10 ?x)
          )
          (and
            (AboveOf9 ?x)
            (BelowOf3 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf17 ?x)
          )
          (and
            (AboveOf16 ?x)
            (BelowOf3 ?x)
          )
          (and
            (AboveOf7 ?x)
            (BelowOf1 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf2 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf8 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf7 ?x)
          )
        )
        (and
          (Row8 ?x)
        )
      )
      (when
        (or
          (and
            (BelowOf9 ?x)
            (AboveOf12 ?x)
          )
          (and
            (BelowOf3 ?x)
            (AboveOf12 ?x)
          )
          (and
            (BelowOf9 ?x)
            (AboveOf8 ?x)
          )
          (and
            (BelowOf7 ?x)
            (AboveOf12 ?x)
          )
          (and
            (AboveOf16 ?x)
            (BelowOf8 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf11 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf14 ?x)
          )
          (and
            (BelowOf8 ?x)
            (AboveOf12 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf8 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf15 ?x)
          )
          (and
            (AboveOf17 ?x)
            (BelowOf1 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf6 ?x)
          )
          (and
            (BelowOf7 ?x)
            (AboveOf9 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf4 ?x)
          )
          (and
            (AboveOf15 ?x)
            (BelowOf8 ?x)
          )
          (and
            (AboveOf15 ?x)
            (BelowOf9 ?x)
          )
          (and
            (BelowOf3 ?x)
            (AboveOf11 ?x)
          )
          (and
            (BelowOf7 ?x)
            (AboveOf11 ?x)
          )
          (and
            (BelowOf7 ?x)
            (AboveOf8 ?x)
          )
          (and
            (BelowOf7 ?x)
            (AboveOf17 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf11 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf16 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf8 ?x)
          )
          (and
            (AboveOf9 ?x)
            (BelowOf6 ?x)
          )
          (and
            (BelowOf6 ?x)
            (AboveOf8 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf12 ?x)
          )
          (and
            (BelowOf3 ?x)
            (AboveOf8 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf16 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf2 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf12 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf9 ?x)
          )
          (and
            (AboveOf8 ?x)
            (BelowOf8 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf11 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf16 ?x)
          )
          (and
            (AboveOf16 ?x)
            (BelowOf9 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf6 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf7 ?x)
          )
          (and
            (AboveOf15 ?x)
            (BelowOf6 ?x)
          )
          (and
            (AboveOf16 ?x)
            (BelowOf3 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf17 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf17 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf8 ?x)
          )
          (and
            (AboveOf15 ?x)
            (BelowOf1 ?x)
          )
          (and
            (AboveOf9 ?x)
            (BelowOf9 ?x)
          )
          (and
            (AboveOf10 ?x)
            (BelowOf6 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf3 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf12 ?x)
          )
          (and
            (AboveOf11 ?x)
            (BelowOf3 ?x)
          )
          (and
            (AboveOf15 ?x)
            (BelowOf4 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf10 ?x)
          )
          (and
            (AboveOf17 ?x)
            (BelowOf3 ?x)
          )
          (and
            (AboveOf17 ?x)
            (BelowOf6 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf10 ?x)
          )
          (and
            (AboveOf9 ?x)
            (BelowOf8 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf1 ?x)
          )
          (and
            (AboveOf16 ?x)
            (BelowOf4 ?x)
          )
          (and
            (BelowOf6 ?x)
            (AboveOf10 ?x)
          )
          (and
            (AboveOf16 ?x)
            (BelowOf6 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf13 ?x)
          )
          (and
            (AboveOf10 ?x)
            (BelowOf8 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf9 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf3 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf5 ?x)
          )
          (and
            (BelowOf6 ?x)
            (AboveOf12 ?x)
          )
          (and
            (BelowOf9 ?x)
            (AboveOf10 ?x)
          )
          (and
            (BelowOf7 ?x)
            (AboveOf16 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf8 ?x)
          )
          (and
            (BelowOf6 ?x)
            (AboveOf11 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf4 ?x)
          )
          (and
            (AboveOf17 ?x)
            (BelowOf8 ?x)
          )
          (and
            (AboveOf16 ?x)
            (BelowOf1 ?x)
          )
          (and
            (AboveOf12 ?x)
            (BelowOf8 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf8 ?x)
          )
          (and
            (AboveOf11 ?x)
            (BelowOf8 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf1 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf12 ?x)
          )
          (and
            (BelowOf9 ?x)
            (AboveOf11 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf9 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf9 ?x)
          )
          (and
            (AboveOf9 ?x)
            (BelowOf1 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf15 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf10 ?x)
          )
          (and
            (AboveOf10 ?x)
            (BelowOf3 ?x)
          )
          (and
            (AboveOf15 ?x)
            (BelowOf3 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf9 ?x)
          )
          (and
            (AboveOf15 ?x)
            (BelowOf7 ?x)
          )
          (and
            (BelowOf9 ?x)
            (AboveOf17 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf7 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf17 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf11 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf10 ?x)
          )
          (and
            (AboveOf9 ?x)
            (BelowOf3 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf17 ?x)
          )
          (and
            (BelowOf7 ?x)
            (AboveOf10 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf2 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf8 ?x)
          )
        )
        (and
          (Row9 ?x)
        )
      )
      (when
        (or
          (and
            (BelowOf9 ?x)
            (AboveOf12 ?x)
          )
          (and
            (BelowOf3 ?x)
            (AboveOf12 ?x)
          )
          (and
            (BelowOf10 ?x)
            (AboveOf11 ?x)
          )
          (and
            (BelowOf7 ?x)
            (AboveOf12 ?x)
          )
          (and
            (AboveOf16 ?x)
            (BelowOf8 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf11 ?x)
          )
          (and
            (AboveOf15 ?x)
            (BelowOf1 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf14 ?x)
          )
          (and
            (AboveOf17 ?x)
            (BelowOf1 ?x)
          )
          (and
            (BelowOf8 ?x)
            (AboveOf12 ?x)
          )
          (and
            (AboveOf9 ?x)
            (BelowOf3 ?x)
          )
          (and
            (AboveOf17 ?x)
            (BelowOf10 ?x)
          )
          (and
            (AboveOf15 ?x)
            (BelowOf10 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf15 ?x)
          )
          (and
            (AboveOf9 ?x)
            (BelowOf10 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf6 ?x)
          )
          (and
            (BelowOf7 ?x)
            (AboveOf9 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf4 ?x)
          )
          (and
            (AboveOf15 ?x)
            (BelowOf8 ?x)
          )
          (and
            (AboveOf15 ?x)
            (BelowOf9 ?x)
          )
          (and
            (BelowOf3 ?x)
            (AboveOf11 ?x)
          )
          (and
            (BelowOf7 ?x)
            (AboveOf11 ?x)
          )
          (and
            (BelowOf7 ?x)
            (AboveOf17 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf11 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf16 ?x)
          )
          (and
            (AboveOf9 ?x)
            (BelowOf6 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf12 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf16 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf2 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf12 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf9 ?x)
          )
          (and
            (AboveOf16 ?x)
            (BelowOf10 ?x)
          )
          (and
            (AboveOf17 ?x)
            (BelowOf3 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf11 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf16 ?x)
          )
          (and
            (AboveOf16 ?x)
            (BelowOf9 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf6 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf7 ?x)
          )
          (and
            (AboveOf15 ?x)
            (BelowOf6 ?x)
          )
          (and
            (AboveOf16 ?x)
            (BelowOf3 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf17 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf17 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf10 ?x)
          )
          (and
            (AboveOf10 ?x)
            (BelowOf6 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf3 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf12 ?x)
          )
          (and
            (AboveOf11 ?x)
            (BelowOf3 ?x)
          )
          (and
            (BelowOf9 ?x)
            (AboveOf9 ?x)
          )
          (and
            (AboveOf15 ?x)
            (BelowOf4 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf10 ?x)
          )
          (and
            (AboveOf17 ?x)
            (BelowOf6 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf10 ?x)
          )
          (and
            (AboveOf9 ?x)
            (BelowOf8 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf1 ?x)
          )
          (and
            (AboveOf16 ?x)
            (BelowOf4 ?x)
          )
          (and
            (BelowOf6 ?x)
            (AboveOf10 ?x)
          )
          (and
            (AboveOf16 ?x)
            (BelowOf6 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf13 ?x)
          )
          (and
            (AboveOf10 ?x)
            (BelowOf8 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf9 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf10 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf5 ?x)
          )
          (and
            (BelowOf6 ?x)
            (AboveOf12 ?x)
          )
          (and
            (BelowOf9 ?x)
            (AboveOf10 ?x)
          )
          (and
            (BelowOf7 ?x)
            (AboveOf16 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf8 ?x)
          )
          (and
            (BelowOf6 ?x)
            (AboveOf11 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf4 ?x)
          )
          (and
            (AboveOf17 ?x)
            (BelowOf8 ?x)
          )
          (and
            (AboveOf16 ?x)
            (BelowOf1 ?x)
          )
          (and
            (AboveOf12 ?x)
            (BelowOf8 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf8 ?x)
          )
          (and
            (AboveOf11 ?x)
            (BelowOf8 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf1 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf12 ?x)
          )
          (and
            (BelowOf9 ?x)
            (AboveOf11 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf9 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf9 ?x)
          )
          (and
            (AboveOf9 ?x)
            (BelowOf1 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf15 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf10 ?x)
          )
          (and
            (AboveOf10 ?x)
            (BelowOf3 ?x)
          )
          (and
            (BelowOf10 ?x)
            (AboveOf12 ?x)
          )
          (and
            (AboveOf15 ?x)
            (BelowOf3 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf9 ?x)
          )
          (and
            (AboveOf10 ?x)
            (BelowOf10 ?x)
          )
          (and
            (AboveOf15 ?x)
            (BelowOf7 ?x)
          )
          (and
            (BelowOf9 ?x)
            (AboveOf17 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf7 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf17 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf11 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf10 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf17 ?x)
          )
          (and
            (BelowOf7 ?x)
            (AboveOf10 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf2 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf3 ?x)
          )
        )
        (and
          (Row10 ?x)
        )
      )
      (when
        (or
          (and
            (BelowOf9 ?x)
            (AboveOf12 ?x)
          )
          (and
            (BelowOf11 ?x)
            (AboveOf12 ?x)
          )
          (and
            (BelowOf3 ?x)
            (AboveOf12 ?x)
          )
          (and
            (BelowOf10 ?x)
            (AboveOf11 ?x)
          )
          (and
            (BelowOf7 ?x)
            (AboveOf12 ?x)
          )
          (and
            (AboveOf16 ?x)
            (BelowOf8 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf11 ?x)
          )
          (and
            (AboveOf15 ?x)
            (BelowOf1 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf14 ?x)
          )
          (and
            (BelowOf8 ?x)
            (AboveOf12 ?x)
          )
          (and
            (AboveOf17 ?x)
            (BelowOf10 ?x)
          )
          (and
            (AboveOf15 ?x)
            (BelowOf10 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf15 ?x)
          )
          (and
            (AboveOf17 ?x)
            (BelowOf1 ?x)
          )
          (and
            (BelowOf11 ?x)
            (AboveOf11 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf6 ?x)
          )
          (and
            (AboveOf10 ?x)
            (BelowOf11 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf4 ?x)
          )
          (and
            (AboveOf15 ?x)
            (BelowOf8 ?x)
          )
          (and
            (AboveOf15 ?x)
            (BelowOf9 ?x)
          )
          (and
            (BelowOf3 ?x)
            (AboveOf11 ?x)
          )
          (and
            (BelowOf7 ?x)
            (AboveOf11 ?x)
          )
          (and
            (BelowOf7 ?x)
            (AboveOf17 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf11 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf12 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf16 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf12 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf16 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf2 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf11 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf11 ?x)
          )
          (and
            (AboveOf16 ?x)
            (BelowOf10 ?x)
          )
          (and
            (AboveOf17 ?x)
            (BelowOf3 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf11 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf16 ?x)
          )
          (and
            (AboveOf16 ?x)
            (BelowOf9 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf6 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf7 ?x)
          )
          (and
            (AboveOf15 ?x)
            (BelowOf6 ?x)
          )
          (and
            (AboveOf16 ?x)
            (BelowOf3 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf17 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf17 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf10 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf3 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf12 ?x)
          )
          (and
            (AboveOf11 ?x)
            (BelowOf3 ?x)
          )
          (and
            (AboveOf15 ?x)
            (BelowOf4 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf10 ?x)
          )
          (and
            (AboveOf17 ?x)
            (BelowOf6 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf10 ?x)
          )
          (and
            (AboveOf17 ?x)
            (BelowOf11 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf1 ?x)
          )
          (and
            (AboveOf16 ?x)
            (BelowOf4 ?x)
          )
          (and
            (AboveOf15 ?x)
            (BelowOf11 ?x)
          )
          (and
            (BelowOf6 ?x)
            (AboveOf10 ?x)
          )
          (and
            (AboveOf16 ?x)
            (BelowOf6 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf13 ?x)
          )
          (and
            (AboveOf10 ?x)
            (BelowOf8 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf9 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf10 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf5 ?x)
          )
          (and
            (BelowOf6 ?x)
            (AboveOf12 ?x)
          )
          (and
            (BelowOf9 ?x)
            (AboveOf10 ?x)
          )
          (and
            (BelowOf7 ?x)
            (AboveOf16 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf8 ?x)
          )
          (and
            (BelowOf6 ?x)
            (AboveOf11 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf4 ?x)
          )
          (and
            (AboveOf17 ?x)
            (BelowOf8 ?x)
          )
          (and
            (AboveOf16 ?x)
            (BelowOf11 ?x)
          )
          (and
            (AboveOf16 ?x)
            (BelowOf1 ?x)
          )
          (and
            (AboveOf12 ?x)
            (BelowOf8 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf8 ?x)
          )
          (and
            (AboveOf11 ?x)
            (BelowOf8 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf1 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf12 ?x)
          )
          (and
            (BelowOf9 ?x)
            (AboveOf11 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf9 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf15 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf10 ?x)
          )
          (and
            (AboveOf10 ?x)
            (BelowOf3 ?x)
          )
          (and
            (BelowOf10 ?x)
            (AboveOf12 ?x)
          )
          (and
            (AboveOf15 ?x)
            (BelowOf3 ?x)
          )
          (and
            (AboveOf10 ?x)
            (BelowOf10 ?x)
          )
          (and
            (AboveOf15 ?x)
            (BelowOf7 ?x)
          )
          (and
            (BelowOf9 ?x)
            (AboveOf17 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf7 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf17 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf11 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf10 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf17 ?x)
          )
          (and
            (BelowOf7 ?x)
            (AboveOf10 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf2 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf3 ?x)
          )
        )
        (and
          (Row11 ?x)
        )
      )
      (when
        (or
          (and
            (BelowOf9 ?x)
            (AboveOf12 ?x)
          )
          (and
            (BelowOf11 ?x)
            (AboveOf12 ?x)
          )
          (and
            (BelowOf3 ?x)
            (AboveOf12 ?x)
          )
          (and
            (BelowOf10 ?x)
            (AboveOf11 ?x)
          )
          (and
            (BelowOf7 ?x)
            (AboveOf12 ?x)
          )
          (and
            (AboveOf16 ?x)
            (BelowOf8 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf11 ?x)
          )
          (and
            (AboveOf16 ?x)
            (BelowOf12 ?x)
          )
          (and
            (AboveOf15 ?x)
            (BelowOf1 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf14 ?x)
          )
          (and
            (BelowOf8 ?x)
            (AboveOf12 ?x)
          )
          (and
            (AboveOf17 ?x)
            (BelowOf10 ?x)
          )
          (and
            (AboveOf15 ?x)
            (BelowOf10 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf15 ?x)
          )
          (and
            (BelowOf12 ?x)
            (AboveOf12 ?x)
          )
          (and
            (AboveOf17 ?x)
            (BelowOf1 ?x)
          )
          (and
            (BelowOf11 ?x)
            (AboveOf11 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf6 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf4 ?x)
          )
          (and
            (AboveOf15 ?x)
            (BelowOf8 ?x)
          )
          (and
            (AboveOf15 ?x)
            (BelowOf9 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf12 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf3 ?x)
          )
          (and
            (BelowOf7 ?x)
            (AboveOf11 ?x)
          )
          (and
            (BelowOf7 ?x)
            (AboveOf17 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf11 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf12 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf16 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf12 ?x)
          )
          (and
            (AboveOf11 ?x)
            (BelowOf12 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf11 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf2 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf11 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf11 ?x)
          )
          (and
            (AboveOf16 ?x)
            (BelowOf10 ?x)
          )
          (and
            (AboveOf17 ?x)
            (BelowOf3 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf11 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf16 ?x)
          )
          (and
            (AboveOf16 ?x)
            (BelowOf9 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf6 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf7 ?x)
          )
          (and
            (AboveOf15 ?x)
            (BelowOf6 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf17 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf17 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf10 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf3 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf12 ?x)
          )
          (and
            (AboveOf17 ?x)
            (BelowOf12 ?x)
          )
          (and
            (AboveOf11 ?x)
            (BelowOf3 ?x)
          )
          (and
            (AboveOf15 ?x)
            (BelowOf4 ?x)
          )
          (and
            (AboveOf17 ?x)
            (BelowOf6 ?x)
          )
          (and
            (AboveOf17 ?x)
            (BelowOf11 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf1 ?x)
          )
          (and
            (AboveOf16 ?x)
            (BelowOf4 ?x)
          )
          (and
            (AboveOf15 ?x)
            (BelowOf11 ?x)
          )
          (and
            (AboveOf16 ?x)
            (BelowOf6 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf13 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf9 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf10 ?x)
          )
          (and
            (AboveOf15 ?x)
            (BelowOf12 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf5 ?x)
          )
          (and
            (BelowOf6 ?x)
            (AboveOf12 ?x)
          )
          (and
            (BelowOf7 ?x)
            (AboveOf16 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf8 ?x)
          )
          (and
            (BelowOf6 ?x)
            (AboveOf11 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf4 ?x)
          )
          (and
            (AboveOf17 ?x)
            (BelowOf8 ?x)
          )
          (and
            (AboveOf16 ?x)
            (BelowOf11 ?x)
          )
          (and
            (AboveOf16 ?x)
            (BelowOf1 ?x)
          )
          (and
            (AboveOf12 ?x)
            (BelowOf8 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf8 ?x)
          )
          (and
            (AboveOf11 ?x)
            (BelowOf8 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf1 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf12 ?x)
          )
          (and
            (BelowOf9 ?x)
            (AboveOf11 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf9 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf15 ?x)
          )
          (and
            (BelowOf10 ?x)
            (AboveOf12 ?x)
          )
          (and
            (AboveOf15 ?x)
            (BelowOf3 ?x)
          )
          (and
            (AboveOf15 ?x)
            (BelowOf7 ?x)
          )
          (and
            (BelowOf9 ?x)
            (AboveOf17 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf12 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf7 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf17 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf16 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf17 ?x)
          )
          (and
            (AboveOf16 ?x)
            (BelowOf3 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf2 ?x)
          )
        )
        (and
          (Row12 ?x)
        )
      )
      (when
        (or
          (and
            (BelowOf5 ?x)
            (AboveOf14 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf8 ?x)
          )
          (and
            (AboveOf15 ?x)
            (BelowOf8 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf10 ?x)
          )
          (and
            (AboveOf15 ?x)
            (BelowOf9 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf12 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf3 ?x)
          )
          (and
            (AboveOf16 ?x)
            (BelowOf12 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf2 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf3 ?x)
          )
          (and
            (BelowOf9 ?x)
            (AboveOf12 ?x)
          )
          (and
            (BelowOf11 ?x)
            (AboveOf12 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf12 ?x)
          )
          (and
            (BelowOf3 ?x)
            (AboveOf12 ?x)
          )
          (and
            (AboveOf17 ?x)
            (BelowOf12 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf12 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf16 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf9 ?x)
          )
          (and
            (AboveOf15 ?x)
            (BelowOf4 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf6 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf11 ?x)
          )
          (and
            (BelowOf7 ?x)
            (AboveOf12 ?x)
          )
          (and
            (BelowOf13 ?x)
            (AboveOf16 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf15 ?x)
          )
          (and
            (AboveOf16 ?x)
            (BelowOf8 ?x)
          )
          (and
            (AboveOf17 ?x)
            (BelowOf6 ?x)
          )
          (and
            (AboveOf16 ?x)
            (BelowOf10 ?x)
          )
          (and
            (BelowOf10 ?x)
            (AboveOf12 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf4 ?x)
          )
          (and
            (AboveOf17 ?x)
            (BelowOf11 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf1 ?x)
          )
          (and
            (BelowOf13 ?x)
            (AboveOf17 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf12 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf1 ?x)
          )
          (and
            (BelowOf13 ?x)
            (AboveOf12 ?x)
          )
          (and
            (AboveOf16 ?x)
            (BelowOf4 ?x)
          )
          (and
            (AboveOf15 ?x)
            (BelowOf11 ?x)
          )
          (and
            (AboveOf15 ?x)
            (BelowOf1 ?x)
          )
          (and
            (BelowOf9 ?x)
            (AboveOf17 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf17 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf12 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf7 ?x)
          )
          (and
            (AboveOf15 ?x)
            (BelowOf6 ?x)
          )
          (and
            (BelowOf13 ?x)
            (AboveOf15 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf17 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf16 ?x)
          )
          (and
            (AboveOf15 ?x)
            (BelowOf3 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf13 ?x)
          )
          (and
            (BelowOf6 ?x)
            (AboveOf12 ?x)
          )
          (and
            (AboveOf17 ?x)
            (BelowOf10 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf9 ?x)
          )
          (and
            (AboveOf16 ?x)
            (BelowOf3 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf12 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf11 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf2 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf10 ?x)
          )
          (and
            (AboveOf17 ?x)
            (BelowOf3 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf15 ?x)
          )
          (and
            (AboveOf16 ?x)
            (BelowOf6 ?x)
          )
          (and
            (AboveOf17 ?x)
            (BelowOf1 ?x)
          )
          (and
            (AboveOf15 ?x)
            (BelowOf10 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf16 ?x)
          )
          (and
            (AboveOf15 ?x)
            (BelowOf12 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf5 ?x)
          )
          (and
            (AboveOf16 ?x)
            (BelowOf9 ?x)
          )
          (and
            (AboveOf15 ?x)
            (BelowOf7 ?x)
          )
          (and
            (AboveOf16 ?x)
            (BelowOf1 ?x)
          )
          (and
            (BelowOf13 ?x)
            (AboveOf13 ?x)
          )
          (and
            (BelowOf7 ?x)
            (AboveOf16 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf8 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf7 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf6 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf17 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf4 ?x)
          )
          (and
            (AboveOf17 ?x)
            (BelowOf8 ?x)
          )
          (and
            (AboveOf16 ?x)
            (BelowOf11 ?x)
          )
          (and
            (AboveOf12 ?x)
            (BelowOf12 ?x)
          )
          (and
            (BelowOf13 ?x)
            (AboveOf14 ?x)
          )
          (and
            (AboveOf12 ?x)
            (BelowOf8 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf17 ?x)
          )
          (and
            (BelowOf7 ?x)
            (AboveOf17 ?x)
          )
        )
        (and
          (Row13 ?x)
        )
      )
      (when
        (or
          (and
            (AboveOf14 ?x)
            (BelowOf8 ?x)
          )
          (and
            (AboveOf15 ?x)
            (BelowOf8 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf10 ?x)
          )
          (and
            (AboveOf15 ?x)
            (BelowOf9 ?x)
          )
          (and
            (AboveOf16 ?x)
            (BelowOf4 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf12 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf3 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf2 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf3 ?x)
          )
          (and
            (BelowOf7 ?x)
            (AboveOf17 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf14 ?x)
          )
          (and
            (AboveOf17 ?x)
            (BelowOf12 ?x)
          )
          (and
            (BelowOf14 ?x)
            (AboveOf16 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf16 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf9 ?x)
          )
          (and
            (AboveOf15 ?x)
            (BelowOf4 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf6 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf11 ?x)
          )
          (and
            (AboveOf16 ?x)
            (BelowOf10 ?x)
          )
          (and
            (BelowOf13 ?x)
            (AboveOf16 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf15 ?x)
          )
          (and
            (AboveOf16 ?x)
            (BelowOf8 ?x)
          )
          (and
            (AboveOf17 ?x)
            (BelowOf6 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf4 ?x)
          )
          (and
            (AboveOf17 ?x)
            (BelowOf11 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf14 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf1 ?x)
          )
          (and
            (AboveOf16 ?x)
            (BelowOf12 ?x)
          )
          (and
            (AboveOf16 ?x)
            (BelowOf6 ?x)
          )
          (and
            (AboveOf15 ?x)
            (BelowOf3 ?x)
          )
          (and
            (BelowOf13 ?x)
            (AboveOf17 ?x)
          )
          (and
            (AboveOf15 ?x)
            (BelowOf11 ?x)
          )
          (and
            (AboveOf15 ?x)
            (BelowOf1 ?x)
          )
          (and
            (BelowOf9 ?x)
            (AboveOf17 ?x)
          )
          (and
            (BelowOf14 ?x)
            (AboveOf17 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf12 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf7 ?x)
          )
          (and
            (AboveOf15 ?x)
            (BelowOf6 ?x)
          )
          (and
            (BelowOf13 ?x)
            (AboveOf15 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf17 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf16 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf13 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf1 ?x)
          )
          (and
            (AboveOf17 ?x)
            (BelowOf10 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf9 ?x)
          )
          (and
            (AboveOf16 ?x)
            (BelowOf3 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf11 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf2 ?x)
          )
          (and
            (BelowOf14 ?x)
            (AboveOf15 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf10 ?x)
          )
          (and
            (AboveOf17 ?x)
            (BelowOf3 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf15 ?x)
          )
          (and
            (AboveOf17 ?x)
            (BelowOf1 ?x)
          )
          (and
            (AboveOf15 ?x)
            (BelowOf10 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf14 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf16 ?x)
          )
          (and
            (AboveOf15 ?x)
            (BelowOf12 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf5 ?x)
          )
          (and
            (AboveOf16 ?x)
            (BelowOf9 ?x)
          )
          (and
            (AboveOf15 ?x)
            (BelowOf7 ?x)
          )
          (and
            (BelowOf7 ?x)
            (AboveOf16 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf8 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf7 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf6 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf17 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf4 ?x)
          )
          (and
            (AboveOf17 ?x)
            (BelowOf8 ?x)
          )
          (and
            (AboveOf16 ?x)
            (BelowOf11 ?x)
          )
          (and
            (AboveOf16 ?x)
            (BelowOf1 ?x)
          )
          (and
            (BelowOf13 ?x)
            (AboveOf14 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf17 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf17 ?x)
          )
        )
        (and
          (Row14 ?x)
        )
      )
      (when
        (or
          (and
            (BelowOf5 ?x)
            (AboveOf14 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf8 ?x)
          )
          (and
            (AboveOf15 ?x)
            (BelowOf8 ?x)
          )
          (and
            (AboveOf15 ?x)
            (BelowOf1 ?x)
          )
          (and
            (AboveOf15 ?x)
            (BelowOf9 ?x)
          )
          (and
            (BelowOf15 ?x)
            (AboveOf17 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf12 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf3 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf2 ?x)
          )
          (and
            (BelowOf7 ?x)
            (AboveOf17 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf11 ?x)
          )
          (and
            (AboveOf17 ?x)
            (BelowOf12 ?x)
          )
          (and
            (BelowOf14 ?x)
            (AboveOf16 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf16 ?x)
          )
          (and
            (AboveOf15 ?x)
            (BelowOf4 ?x)
          )
          (and
            (AboveOf16 ?x)
            (BelowOf10 ?x)
          )
          (and
            (BelowOf13 ?x)
            (AboveOf16 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf15 ?x)
          )
          (and
            (AboveOf16 ?x)
            (BelowOf8 ?x)
          )
          (and
            (AboveOf17 ?x)
            (BelowOf6 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf4 ?x)
          )
          (and
            (AboveOf17 ?x)
            (BelowOf11 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf1 ?x)
          )
          (and
            (AboveOf16 ?x)
            (BelowOf12 ?x)
          )
          (and
            (AboveOf15 ?x)
            (BelowOf3 ?x)
          )
          (and
            (BelowOf13 ?x)
            (AboveOf17 ?x)
          )
          (and
            (AboveOf15 ?x)
            (BelowOf11 ?x)
          )
          (and
            (BelowOf9 ?x)
            (AboveOf17 ?x)
          )
          (and
            (BelowOf14 ?x)
            (AboveOf17 ?x)
          )
          (and
            (AboveOf16 ?x)
            (BelowOf6 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf7 ?x)
          )
          (and
            (AboveOf15 ?x)
            (BelowOf6 ?x)
          )
          (and
            (BelowOf13 ?x)
            (AboveOf15 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf17 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf16 ?x)
          )
          (and
            (AboveOf17 ?x)
            (BelowOf10 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf9 ?x)
          )
          (and
            (AboveOf16 ?x)
            (BelowOf3 ?x)
          )
          (and
            (BelowOf15 ?x)
            (AboveOf16 ?x)
          )
          (and
            (AboveOf16 ?x)
            (BelowOf4 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf14 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf10 ?x)
          )
          (and
            (AboveOf17 ?x)
            (BelowOf3 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf15 ?x)
          )
          (and
            (AboveOf15 ?x)
            (BelowOf15 ?x)
          )
          (and
            (AboveOf17 ?x)
            (BelowOf1 ?x)
          )
          (and
            (AboveOf15 ?x)
            (BelowOf10 ?x)
          )
          (and
            (BelowOf14 ?x)
            (AboveOf15 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf16 ?x)
          )
          (and
            (AboveOf15 ?x)
            (BelowOf12 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf15 ?x)
          )
          (and
            (AboveOf16 ?x)
            (BelowOf9 ?x)
          )
          (and
            (AboveOf15 ?x)
            (BelowOf7 ?x)
          )
          (and
            (BelowOf7 ?x)
            (AboveOf16 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf6 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf17 ?x)
          )
          (and
            (AboveOf17 ?x)
            (BelowOf8 ?x)
          )
          (and
            (AboveOf16 ?x)
            (BelowOf11 ?x)
          )
          (and
            (AboveOf16 ?x)
            (BelowOf1 ?x)
          )
          (and
            (BelowOf13 ?x)
            (AboveOf14 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf17 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf17 ?x)
          )
        )
        (and
          (Row15 ?x)
        )
      )
      (when
        (or
          (and
            (AboveOf15 ?x)
            (BelowOf8 ?x)
          )
          (and
            (AboveOf15 ?x)
            (BelowOf1 ?x)
          )
          (and
            (AboveOf15 ?x)
            (BelowOf9 ?x)
          )
          (and
            (BelowOf15 ?x)
            (AboveOf17 ?x)
          )
          (and
            (AboveOf15 ?x)
            (BelowOf6 ?x)
          )
          (and
            (BelowOf7 ?x)
            (AboveOf17 ?x)
          )
          (and
            (AboveOf17 ?x)
            (BelowOf12 ?x)
          )
          (and
            (AboveOf15 ?x)
            (BelowOf16 ?x)
          )
          (and
            (BelowOf14 ?x)
            (AboveOf16 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf16 ?x)
          )
          (and
            (AboveOf15 ?x)
            (BelowOf4 ?x)
          )
          (and
            (AboveOf16 ?x)
            (BelowOf10 ?x)
          )
          (and
            (BelowOf13 ?x)
            (AboveOf16 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf15 ?x)
          )
          (and
            (AboveOf16 ?x)
            (BelowOf8 ?x)
          )
          (and
            (AboveOf17 ?x)
            (BelowOf6 ?x)
          )
          (and
            (AboveOf17 ?x)
            (BelowOf11 ?x)
          )
          (and
            (AboveOf16 ?x)
            (BelowOf12 ?x)
          )
          (and
            (AboveOf15 ?x)
            (BelowOf3 ?x)
          )
          (and
            (BelowOf13 ?x)
            (AboveOf17 ?x)
          )
          (and
            (AboveOf15 ?x)
            (BelowOf11 ?x)
          )
          (and
            (BelowOf16 ?x)
            (AboveOf16 ?x)
          )
          (and
            (BelowOf9 ?x)
            (AboveOf17 ?x)
          )
          (and
            (BelowOf14 ?x)
            (AboveOf17 ?x)
          )
          (and
            (AboveOf16 ?x)
            (BelowOf6 ?x)
          )
          (and
            (BelowOf16 ?x)
            (AboveOf17 ?x)
          )
          (and
            (BelowOf13 ?x)
            (AboveOf15 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf17 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf16 ?x)
          )
          (and
            (AboveOf17 ?x)
            (BelowOf10 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf17 ?x)
          )
          (and
            (AboveOf16 ?x)
            (BelowOf3 ?x)
          )
          (and
            (BelowOf15 ?x)
            (AboveOf16 ?x)
          )
          (and
            (AboveOf16 ?x)
            (BelowOf4 ?x)
          )
          (and
            (AboveOf17 ?x)
            (BelowOf3 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf15 ?x)
          )
          (and
            (AboveOf15 ?x)
            (BelowOf15 ?x)
          )
          (and
            (AboveOf17 ?x)
            (BelowOf1 ?x)
          )
          (and
            (AboveOf15 ?x)
            (BelowOf10 ?x)
          )
          (and
            (BelowOf14 ?x)
            (AboveOf15 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf16 ?x)
          )
          (and
            (AboveOf15 ?x)
            (BelowOf12 ?x)
          )
          (and
            (AboveOf16 ?x)
            (BelowOf9 ?x)
          )
          (and
            (AboveOf15 ?x)
            (BelowOf7 ?x)
          )
          (and
            (BelowOf7 ?x)
            (AboveOf16 ?x)
          )
          (and
            (AboveOf17 ?x)
            (BelowOf8 ?x)
          )
          (and
            (AboveOf16 ?x)
            (BelowOf11 ?x)
          )
          (and
            (AboveOf16 ?x)
            (BelowOf1 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf17 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf17 ?x)
          )
        )
        (and
          (Row16 ?x)
        )
      )
      (when
        (or
          (and
            (AboveOf16 ?x)
            (BelowOf6 ?x)
          )
          (and
            (BelowOf15 ?x)
            (AboveOf17 ?x)
          )
          (and
            (BelowOf7 ?x)
            (AboveOf17 ?x)
          )
          (and
            (AboveOf17 ?x)
            (BelowOf12 ?x)
          )
          (and
            (BelowOf14 ?x)
            (AboveOf16 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf16 ?x)
          )
          (and
            (BelowOf17 ?x)
            (AboveOf17 ?x)
          )
          (and
            (BelowOf13 ?x)
            (AboveOf16 ?x)
          )
          (and
            (AboveOf16 ?x)
            (BelowOf8 ?x)
          )
          (and
            (AboveOf17 ?x)
            (BelowOf6 ?x)
          )
          (and
            (AboveOf17 ?x)
            (BelowOf11 ?x)
          )
          (and
            (AboveOf16 ?x)
            (BelowOf12 ?x)
          )
          (and
            (BelowOf13 ?x)
            (AboveOf17 ?x)
          )
          (and
            (BelowOf16 ?x)
            (AboveOf16 ?x)
          )
          (and
            (BelowOf9 ?x)
            (AboveOf17 ?x)
          )
          (and
            (BelowOf14 ?x)
            (AboveOf17 ?x)
          )
          (and
            (BelowOf16 ?x)
            (AboveOf17 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf17 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf16 ?x)
          )
          (and
            (AboveOf17 ?x)
            (BelowOf10 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf17 ?x)
          )
          (and
            (AboveOf16 ?x)
            (BelowOf3 ?x)
          )
          (and
            (BelowOf15 ?x)
            (AboveOf16 ?x)
          )
          (and
            (AboveOf17 ?x)
            (BelowOf3 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf16 ?x)
          )
          (and
            (AboveOf17 ?x)
            (BelowOf1 ?x)
          )
          (and
            (AboveOf16 ?x)
            (BelowOf17 ?x)
          )
          (and
            (AboveOf16 ?x)
            (BelowOf10 ?x)
          )
          (and
            (AboveOf16 ?x)
            (BelowOf9 ?x)
          )
          (and
            (BelowOf7 ?x)
            (AboveOf16 ?x)
          )
          (and
            (AboveOf17 ?x)
            (BelowOf8 ?x)
          )
          (and
            (AboveOf16 ?x)
            (BelowOf11 ?x)
          )
          (and
            (AboveOf16 ?x)
            (BelowOf1 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf17 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf17 ?x)
          )
        )
        (and
          (Row17 ?x)
        )
      )
    )
  )
  (:action moveRight
    :parameters ( ?x )
    :precondition ( and
      (not (CheckConsistency))
      (not (Error))
      (or
        (LeftOf3 ?x)
        (RightOf7 ?x)
        (RightOf8 ?x)
        (RightOf15 ?x)
        (LeftOf2 ?x)
        (LeftOf9 ?x)
        (RightOf2 ?x)
        (RightOf0 ?x)
        (LeftOf8 ?x)
        (LeftOf7 ?x)
        (LeftOf14 ?x)
        (LeftOf5 ?x)
        (RightOf1 ?x)
        (LeftOf12 ?x)
        (Columns ?x)
        (RightOf13 ?x)
        (RightOf14 ?x)
        (RightOf6 ?x)
        (RightOf9 ?x)
        (LeftOf6 ?x)
        (RightOf10 ?x)
        (RightOf11 ?x)
        (RightOf17 ?x)
        (LeftOf11 ?x)
        (RightOf5 ?x)
        (LeftOf10 ?x)
        (LeftOf15 ?x)
        (LeftOf4 ?x)
        (LeftOf16 ?x)
        (RightOf16 ?x)
        (RightOf4 ?x)
        (LeftOf1 ?x)
        (RightOf12 ?x)
        (LeftOf13 ?x)
        (LeftOf17 ?x)
        (LeftOf18 ?x)
        (RightOf3 ?x)
      )
    )
    :effect ( and
      (CheckConsistency)
      (when
        (or
          (RightOf6 ?x)
          (RightOf15 ?x)
          (RightOf11 ?x)
          (RightOf7 ?x)
          (RightOf16 ?x)
          (RightOf1 ?x)
          (RightOf4 ?x)
          (RightOf8 ?x)
          (RightOf0 ?x)
          (RightOf12 ?x)
          (RightOf13 ?x)
          (RightOf14 ?x)
          (RightOf17 ?x)
          (RightOf2 ?x)
          (RightOf9 ?x)
          (RightOf5 ?x)
          (RightOf10 ?x)
          (RightOf3 ?x)
        )
        (and
          (RightOf1 ?x)
        )
      )
      (when
        (or
          (RightOf6 ?x)
          (RightOf15 ?x)
          (RightOf11 ?x)
          (RightOf7 ?x)
          (RightOf16 ?x)
          (RightOf1 ?x)
          (RightOf4 ?x)
          (RightOf8 ?x)
          (RightOf10 ?x)
          (RightOf12 ?x)
          (RightOf13 ?x)
          (RightOf14 ?x)
          (RightOf17 ?x)
          (RightOf2 ?x)
          (RightOf9 ?x)
          (RightOf5 ?x)
          (RightOf3 ?x)
        )
        (and
          (RightOf2 ?x)
        )
      )
      (when
        (or
          (RightOf6 ?x)
          (RightOf15 ?x)
          (RightOf11 ?x)
          (RightOf7 ?x)
          (RightOf16 ?x)
          (RightOf8 ?x)
          (RightOf4 ?x)
          (RightOf10 ?x)
          (RightOf12 ?x)
          (RightOf13 ?x)
          (RightOf14 ?x)
          (RightOf17 ?x)
          (RightOf2 ?x)
          (RightOf9 ?x)
          (RightOf5 ?x)
          (RightOf3 ?x)
        )
        (and
          (RightOf3 ?x)
        )
      )
      (when
        (or
          (RightOf6 ?x)
          (RightOf15 ?x)
          (RightOf11 ?x)
          (RightOf7 ?x)
          (RightOf16 ?x)
          (RightOf8 ?x)
          (RightOf4 ?x)
          (RightOf10 ?x)
          (RightOf12 ?x)
          (RightOf13 ?x)
          (RightOf14 ?x)
          (RightOf17 ?x)
          (RightOf9 ?x)
          (RightOf5 ?x)
          (RightOf3 ?x)
        )
        (and
          (RightOf4 ?x)
        )
      )
      (when
        (or
          (RightOf6 ?x)
          (RightOf15 ?x)
          (RightOf11 ?x)
          (RightOf7 ?x)
          (RightOf16 ?x)
          (RightOf8 ?x)
          (RightOf4 ?x)
          (RightOf10 ?x)
          (RightOf12 ?x)
          (RightOf13 ?x)
          (RightOf14 ?x)
          (RightOf17 ?x)
          (RightOf9 ?x)
          (RightOf5 ?x)
        )
        (and
          (RightOf5 ?x)
        )
      )
      (when
        (or
          (RightOf6 ?x)
          (RightOf15 ?x)
          (RightOf11 ?x)
          (RightOf7 ?x)
          (RightOf16 ?x)
          (RightOf8 ?x)
          (RightOf10 ?x)
          (RightOf12 ?x)
          (RightOf13 ?x)
          (RightOf14 ?x)
          (RightOf17 ?x)
          (RightOf9 ?x)
          (RightOf5 ?x)
        )
        (and
          (RightOf6 ?x)
        )
      )
      (when
        (or
          (RightOf6 ?x)
          (RightOf15 ?x)
          (RightOf11 ?x)
          (RightOf7 ?x)
          (RightOf16 ?x)
          (RightOf8 ?x)
          (RightOf10 ?x)
          (RightOf12 ?x)
          (RightOf13 ?x)
          (RightOf14 ?x)
          (RightOf17 ?x)
          (RightOf9 ?x)
        )
        (and
          (RightOf7 ?x)
        )
      )
      (when
        (or
          (RightOf11 ?x)
          (RightOf9 ?x)
          (RightOf7 ?x)
          (RightOf16 ?x)
          (RightOf12 ?x)
          (RightOf8 ?x)
          (RightOf10 ?x)
          (RightOf15 ?x)
          (RightOf13 ?x)
          (RightOf14 ?x)
          (RightOf17 ?x)
        )
        (and
          (RightOf8 ?x)
        )
      )
      (when
        (or
          (RightOf11 ?x)
          (RightOf9 ?x)
          (RightOf12 ?x)
          (RightOf16 ?x)
          (RightOf8 ?x)
          (RightOf10 ?x)
          (RightOf15 ?x)
          (RightOf13 ?x)
          (RightOf14 ?x)
          (RightOf17 ?x)
        )
        (and
          (RightOf9 ?x)
        )
      )
      (when
        (or
          (RightOf11 ?x)
          (RightOf9 ?x)
          (RightOf12 ?x)
          (RightOf16 ?x)
          (RightOf10 ?x)
          (RightOf15 ?x)
          (RightOf13 ?x)
          (RightOf14 ?x)
          (RightOf17 ?x)
        )
        (and
          (RightOf10 ?x)
        )
      )
      (when
        (or
          (RightOf11 ?x)
          (RightOf12 ?x)
          (RightOf16 ?x)
          (RightOf10 ?x)
          (RightOf15 ?x)
          (RightOf13 ?x)
          (RightOf14 ?x)
          (RightOf17 ?x)
        )
        (and
          (RightOf11 ?x)
        )
      )
      (when
        (or
          (RightOf11 ?x)
          (RightOf12 ?x)
          (RightOf16 ?x)
          (RightOf15 ?x)
          (RightOf13 ?x)
          (RightOf14 ?x)
          (RightOf17 ?x)
        )
        (and
          (RightOf12 ?x)
        )
      )
      (when
        (or
          (RightOf15 ?x)
          (RightOf16 ?x)
          (RightOf12 ?x)
          (RightOf13 ?x)
          (RightOf14 ?x)
          (RightOf17 ?x)
        )
        (and
          (RightOf13 ?x)
        )
      )
      (when
        (or
          (RightOf15 ?x)
          (RightOf13 ?x)
          (RightOf14 ?x)
          (RightOf17 ?x)
          (RightOf16 ?x)
        )
        (and
          (RightOf14 ?x)
        )
      )
      (when
        (or
          (RightOf15 ?x)
          (RightOf14 ?x)
          (RightOf17 ?x)
          (RightOf16 ?x)
        )
        (and
          (RightOf15 ?x)
        )
      )
      (when
        (or
          (RightOf15 ?x)
          (RightOf17 ?x)
          (RightOf16 ?x)
        )
        (and
          (RightOf16 ?x)
        )
      )
      (when
        (or
          (RightOf17 ?x)
          (RightOf16 ?x)
        )
        (and
          (RightOf17 ?x)
        )
      )
      (when
        (LeftOf1 ?x)
        (and
          (LeftOf2 ?x)
          (not (LeftOf1 ?x))
        )
      )
      (when
        (or
          (LeftOf2 ?x)
          (LeftOf1 ?x)
        )
        (and
          (LeftOf3 ?x)
          (not (LeftOf2 ?x))
        )
      )
      (when
        (or
          (LeftOf2 ?x)
          (LeftOf3 ?x)
          (LeftOf1 ?x)
        )
        (and
          (LeftOf4 ?x)
          (not (LeftOf3 ?x))
        )
      )
      (when
        (or
          (LeftOf2 ?x)
          (LeftOf4 ?x)
          (LeftOf3 ?x)
          (LeftOf1 ?x)
        )
        (and
          (LeftOf5 ?x)
          (not (LeftOf4 ?x))
        )
      )
      (when
        (or
          (LeftOf2 ?x)
          (LeftOf4 ?x)
          (LeftOf3 ?x)
          (LeftOf1 ?x)
          (LeftOf5 ?x)
        )
        (and
          (LeftOf6 ?x)
          (not (LeftOf5 ?x))
        )
      )
      (when
        (or
          (LeftOf4 ?x)
          (LeftOf3 ?x)
          (LeftOf5 ?x)
          (LeftOf6 ?x)
          (LeftOf1 ?x)
          (LeftOf2 ?x)
        )
        (and
          (LeftOf7 ?x)
          (not (LeftOf6 ?x))
        )
      )
      (when
        (or
          (LeftOf4 ?x)
          (LeftOf3 ?x)
          (LeftOf5 ?x)
          (LeftOf6 ?x)
          (LeftOf1 ?x)
          (LeftOf7 ?x)
          (LeftOf2 ?x)
        )
        (and
          (LeftOf8 ?x)
          (not (LeftOf7 ?x))
        )
      )
      (when
        (or
          (LeftOf8 ?x)
          (LeftOf4 ?x)
          (LeftOf3 ?x)
          (LeftOf5 ?x)
          (LeftOf6 ?x)
          (LeftOf1 ?x)
          (LeftOf7 ?x)
          (LeftOf2 ?x)
        )
        (and
          (LeftOf9 ?x)
          (not (LeftOf8 ?x))
        )
      )
      (when
        (or
          (LeftOf8 ?x)
          (LeftOf4 ?x)
          (LeftOf3 ?x)
          (LeftOf5 ?x)
          (LeftOf6 ?x)
          (LeftOf1 ?x)
          (LeftOf7 ?x)
          (LeftOf2 ?x)
          (LeftOf9 ?x)
        )
        (and
          (LeftOf10 ?x)
          (not (LeftOf9 ?x))
        )
      )
      (when
        (or
          (LeftOf8 ?x)
          (LeftOf4 ?x)
          (LeftOf3 ?x)
          (LeftOf5 ?x)
          (LeftOf6 ?x)
          (LeftOf1 ?x)
          (LeftOf7 ?x)
          (LeftOf2 ?x)
          (LeftOf9 ?x)
          (LeftOf10 ?x)
        )
        (and
          (LeftOf11 ?x)
          (not (LeftOf10 ?x))
        )
      )
      (when
        (or
          (LeftOf8 ?x)
          (LeftOf4 ?x)
          (LeftOf3 ?x)
          (LeftOf5 ?x)
          (LeftOf6 ?x)
          (LeftOf1 ?x)
          (LeftOf7 ?x)
          (LeftOf2 ?x)
          (LeftOf9 ?x)
          (LeftOf11 ?x)
          (LeftOf10 ?x)
        )
        (and
          (LeftOf12 ?x)
          (not (LeftOf11 ?x))
        )
      )
      (when
        (or
          (LeftOf8 ?x)
          (LeftOf4 ?x)
          (LeftOf3 ?x)
          (LeftOf5 ?x)
          (LeftOf6 ?x)
          (LeftOf12 ?x)
          (LeftOf1 ?x)
          (LeftOf7 ?x)
          (LeftOf2 ?x)
          (LeftOf9 ?x)
          (LeftOf11 ?x)
          (LeftOf10 ?x)
        )
        (and
          (LeftOf13 ?x)
          (not (LeftOf12 ?x))
        )
      )
      (when
        (or
          (LeftOf8 ?x)
          (LeftOf7 ?x)
          (LeftOf4 ?x)
          (LeftOf3 ?x)
          (LeftOf5 ?x)
          (LeftOf6 ?x)
          (LeftOf12 ?x)
          (LeftOf1 ?x)
          (LeftOf13 ?x)
          (LeftOf2 ?x)
          (LeftOf9 ?x)
          (LeftOf11 ?x)
          (LeftOf10 ?x)
        )
        (and
          (LeftOf14 ?x)
          (not (LeftOf13 ?x))
        )
      )
      (when
        (or
          (LeftOf8 ?x)
          (LeftOf7 ?x)
          (LeftOf4 ?x)
          (LeftOf3 ?x)
          (LeftOf14 ?x)
          (LeftOf5 ?x)
          (LeftOf6 ?x)
          (LeftOf12 ?x)
          (LeftOf1 ?x)
          (LeftOf13 ?x)
          (LeftOf2 ?x)
          (LeftOf9 ?x)
          (LeftOf11 ?x)
          (LeftOf10 ?x)
        )
        (and
          (LeftOf15 ?x)
          (not (LeftOf14 ?x))
        )
      )
      (when
        (or
          (LeftOf8 ?x)
          (LeftOf7 ?x)
          (LeftOf4 ?x)
          (LeftOf3 ?x)
          (LeftOf14 ?x)
          (LeftOf5 ?x)
          (LeftOf6 ?x)
          (LeftOf12 ?x)
          (LeftOf1 ?x)
          (LeftOf13 ?x)
          (LeftOf2 ?x)
          (LeftOf9 ?x)
          (LeftOf11 ?x)
          (LeftOf10 ?x)
          (LeftOf15 ?x)
        )
        (and
          (LeftOf16 ?x)
          (not (LeftOf15 ?x))
        )
      )
      (when
        (or
          (LeftOf8 ?x)
          (LeftOf7 ?x)
          (LeftOf4 ?x)
          (LeftOf3 ?x)
          (LeftOf14 ?x)
          (LeftOf16 ?x)
          (LeftOf5 ?x)
          (LeftOf6 ?x)
          (LeftOf12 ?x)
          (LeftOf1 ?x)
          (LeftOf13 ?x)
          (LeftOf2 ?x)
          (LeftOf9 ?x)
          (LeftOf11 ?x)
          (LeftOf10 ?x)
          (LeftOf15 ?x)
        )
        (and
          (LeftOf17 ?x)
          (not (LeftOf16 ?x))
        )
      )
      (when
        (or
          (LeftOf8 ?x)
          (LeftOf7 ?x)
          (LeftOf4 ?x)
          (LeftOf3 ?x)
          (LeftOf14 ?x)
          (LeftOf16 ?x)
          (LeftOf5 ?x)
          (LeftOf6 ?x)
          (LeftOf12 ?x)
          (LeftOf1 ?x)
          (LeftOf13 ?x)
          (LeftOf2 ?x)
          (LeftOf9 ?x)
          (LeftOf11 ?x)
          (LeftOf17 ?x)
          (LeftOf10 ?x)
          (LeftOf15 ?x)
        )
        (and
          (LeftOf18 ?x)
          (not (LeftOf17 ?x))
        )
      )
      (when
        (Column0 ?x)
        (and
          (Column1 ?x)
          (not (Column0 ?x))
        )
      )
      (when
        (Column1 ?x)
        (and
          (Column2 ?x)
          (not (Column1 ?x))
        )
      )
      (when
        (Column2 ?x)
        (and
          (Column3 ?x)
          (not (Column2 ?x))
        )
      )
      (when
        (Column3 ?x)
        (and
          (Column4 ?x)
          (not (Column3 ?x))
        )
      )
      (when
        (Column4 ?x)
        (and
          (Column5 ?x)
          (not (Column4 ?x))
        )
      )
      (when
        (Column5 ?x)
        (and
          (Column6 ?x)
          (not (Column5 ?x))
        )
      )
      (when
        (Column6 ?x)
        (and
          (Column7 ?x)
          (not (Column6 ?x))
        )
      )
      (when
        (Column7 ?x)
        (and
          (Column8 ?x)
          (not (Column7 ?x))
        )
      )
      (when
        (Column8 ?x)
        (and
          (Column9 ?x)
          (not (Column8 ?x))
        )
      )
      (when
        (Column9 ?x)
        (and
          (Column10 ?x)
          (not (Column9 ?x))
        )
      )
      (when
        (Column10 ?x)
        (and
          (Column11 ?x)
          (not (Column10 ?x))
        )
      )
      (when
        (Column11 ?x)
        (and
          (Column12 ?x)
          (not (Column11 ?x))
        )
      )
      (when
        (Column12 ?x)
        (and
          (Column13 ?x)
          (not (Column12 ?x))
        )
      )
      (when
        (Column13 ?x)
        (and
          (Column14 ?x)
          (not (Column13 ?x))
        )
      )
      (when
        (Column14 ?x)
        (and
          (Column15 ?x)
          (not (Column14 ?x))
        )
      )
      (when
        (Column15 ?x)
        (and
          (Column16 ?x)
          (not (Column15 ?x))
        )
      )
      (when
        (Column16 ?x)
        (and
          (Column17 ?x)
          (not (Column16 ?x))
        )
      )
      (when
        (or
          (and
            (RightOf16 ?x)
            (LeftOf1 ?x)
          )
          (and
            (LeftOf1 ?x)
            (RightOf10 ?x)
          )
          (and
            (RightOf14 ?x)
            (LeftOf1 ?x)
          )
          (and
            (RightOf6 ?x)
            (LeftOf1 ?x)
          )
          (and
            (LeftOf1 ?x)
            (RightOf8 ?x)
          )
          (and
            (LeftOf1 ?x)
            (RightOf11 ?x)
          )
          (and
            (RightOf2 ?x)
            (LeftOf1 ?x)
          )
          (and
            (RightOf5 ?x)
            (LeftOf1 ?x)
          )
          (and
            (LeftOf1 ?x)
            (RightOf0 ?x)
          )
          (and
            (RightOf17 ?x)
            (LeftOf1 ?x)
          )
          (and
            (RightOf15 ?x)
            (LeftOf1 ?x)
          )
          (and
            (LeftOf1 ?x)
            (RightOf3 ?x)
          )
          (and
            (RightOf1 ?x)
            (LeftOf1 ?x)
          )
          (and
            (LeftOf1 ?x)
            (RightOf13 ?x)
          )
          (and
            (RightOf9 ?x)
            (LeftOf1 ?x)
          )
          (and
            (RightOf7 ?x)
            (LeftOf1 ?x)
          )
          (and
            (RightOf12 ?x)
            (LeftOf1 ?x)
          )
          (and
            (RightOf4 ?x)
            (LeftOf1 ?x)
          )
        )
        (and
          (Column1 ?x)
        )
      )
      (when
        (or
          (and
            (RightOf4 ?x)
            (LeftOf2 ?x)
          )
          (and
            (RightOf6 ?x)
            (LeftOf2 ?x)
          )
          (and
            (RightOf16 ?x)
            (LeftOf1 ?x)
          )
          (and
            (RightOf14 ?x)
            (LeftOf1 ?x)
          )
          (and
            (RightOf6 ?x)
            (LeftOf1 ?x)
          )
          (and
            (RightOf2 ?x)
            (LeftOf1 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf2 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf8 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf11 ?x)
          )
          (and
            (LeftOf1 ?x)
            (RightOf13 ?x)
          )
          (and
            (RightOf17 ?x)
            (LeftOf1 ?x)
          )
          (and
            (LeftOf1 ?x)
            (RightOf8 ?x)
          )
          (and
            (RightOf5 ?x)
            (LeftOf1 ?x)
          )
          (and
            (RightOf12 ?x)
            (LeftOf2 ?x)
          )
          (and
            (RightOf7 ?x)
            (LeftOf1 ?x)
          )
          (and
            (RightOf12 ?x)
            (LeftOf1 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf10 ?x)
          )
          (and
            (RightOf4 ?x)
            (LeftOf1 ?x)
          )
          (and
            (RightOf14 ?x)
            (LeftOf2 ?x)
          )
          (and
            (RightOf17 ?x)
            (LeftOf2 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf3 ?x)
          )
          (and
            (LeftOf1 ?x)
            (RightOf11 ?x)
          )
          (and
            (LeftOf1 ?x)
            (RightOf5 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf16 ?x)
          )
          (and
            (RightOf7 ?x)
            (LeftOf2 ?x)
          )
          (and
            (RightOf1 ?x)
            (LeftOf1 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf5 ?x)
          )
          (and
            (RightOf1 ?x)
            (LeftOf2 ?x)
          )
          (and
            (LeftOf1 ?x)
            (RightOf10 ?x)
          )
          (and
            (RightOf15 ?x)
            (LeftOf1 ?x)
          )
          (and
            (LeftOf1 ?x)
            (RightOf3 ?x)
          )
          (and
            (RightOf9 ?x)
            (LeftOf1 ?x)
          )
          (and
            (RightOf9 ?x)
            (LeftOf2 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf13 ?x)
          )
          (and
            (RightOf15 ?x)
            (LeftOf2 ?x)
          )
        )
        (and
          (Column2 ?x)
        )
      )
      (when
        (or
          (and
            (RightOf2 ?x)
            (LeftOf3 ?x)
          )
          (and
            (RightOf4 ?x)
            (LeftOf2 ?x)
          )
          (and
            (RightOf9 ?x)
            (LeftOf3 ?x)
          )
          (and
            (RightOf6 ?x)
            (LeftOf2 ?x)
          )
          (and
            (RightOf16 ?x)
            (LeftOf1 ?x)
          )
          (and
            (RightOf14 ?x)
            (LeftOf1 ?x)
          )
          (and
            (RightOf6 ?x)
            (LeftOf1 ?x)
          )
          (and
            (RightOf2 ?x)
            (LeftOf1 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf2 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf8 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf11 ?x)
          )
          (and
            (LeftOf1 ?x)
            (RightOf13 ?x)
          )
          (and
            (RightOf17 ?x)
            (LeftOf3 ?x)
          )
          (and
            (LeftOf3 ?x)
            (RightOf13 ?x)
          )
          (and
            (RightOf11 ?x)
            (LeftOf3 ?x)
          )
          (and
            (LeftOf3 ?x)
            (RightOf3 ?x)
          )
          (and
            (LeftOf1 ?x)
            (RightOf8 ?x)
          )
          (and
            (RightOf15 ?x)
            (LeftOf3 ?x)
          )
          (and
            (RightOf5 ?x)
            (LeftOf3 ?x)
          )
          (and
            (RightOf12 ?x)
            (LeftOf2 ?x)
          )
          (and
            (RightOf7 ?x)
            (LeftOf2 ?x)
          )
          (and
            (RightOf8 ?x)
            (LeftOf3 ?x)
          )
          (and
            (RightOf10 ?x)
            (LeftOf3 ?x)
          )
          (and
            (RightOf12 ?x)
            (LeftOf1 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf10 ?x)
          )
          (and
            (RightOf4 ?x)
            (LeftOf1 ?x)
          )
          (and
            (RightOf14 ?x)
            (LeftOf2 ?x)
          )
          (and
            (RightOf12 ?x)
            (LeftOf3 ?x)
          )
          (and
            (RightOf17 ?x)
            (LeftOf2 ?x)
          )
          (and
            (RightOf5 ?x)
            (LeftOf1 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf3 ?x)
          )
          (and
            (LeftOf1 ?x)
            (RightOf11 ?x)
          )
          (and
            (RightOf7 ?x)
            (LeftOf3 ?x)
          )
          (and
            (LeftOf1 ?x)
            (RightOf5 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf16 ?x)
          )
          (and
            (RightOf7 ?x)
            (LeftOf1 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf5 ?x)
          )
          (and
            (RightOf16 ?x)
            (LeftOf3 ?x)
          )
          (and
            (LeftOf1 ?x)
            (RightOf10 ?x)
          )
          (and
            (RightOf15 ?x)
            (LeftOf1 ?x)
          )
          (and
            (RightOf4 ?x)
            (LeftOf3 ?x)
          )
          (and
            (RightOf17 ?x)
            (LeftOf1 ?x)
          )
          (and
            (RightOf14 ?x)
            (LeftOf3 ?x)
          )
          (and
            (LeftOf1 ?x)
            (RightOf3 ?x)
          )
          (and
            (RightOf9 ?x)
            (LeftOf1 ?x)
          )
          (and
            (RightOf6 ?x)
            (LeftOf3 ?x)
          )
          (and
            (RightOf9 ?x)
            (LeftOf2 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf13 ?x)
          )
          (and
            (RightOf15 ?x)
            (LeftOf2 ?x)
          )
        )
        (and
          (Column3 ?x)
        )
      )
      (when
        (or
          (and
            (RightOf4 ?x)
            (LeftOf2 ?x)
          )
          (and
            (RightOf9 ?x)
            (LeftOf3 ?x)
          )
          (and
            (RightOf6 ?x)
            (LeftOf2 ?x)
          )
          (and
            (RightOf11 ?x)
            (LeftOf4 ?x)
          )
          (and
            (LeftOf4 ?x)
            (RightOf3 ?x)
          )
          (and
            (RightOf6 ?x)
            (LeftOf1 ?x)
          )
          (and
            (LeftOf4 ?x)
            (RightOf13 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf8 ?x)
          )
          (and
            (RightOf5 ?x)
            (LeftOf4 ?x)
          )
          (and
            (RightOf6 ?x)
            (LeftOf4 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf11 ?x)
          )
          (and
            (LeftOf1 ?x)
            (RightOf13 ?x)
          )
          (and
            (RightOf16 ?x)
            (LeftOf4 ?x)
          )
          (and
            (RightOf9 ?x)
            (LeftOf4 ?x)
          )
          (and
            (RightOf17 ?x)
            (LeftOf3 ?x)
          )
          (and
            (LeftOf3 ?x)
            (RightOf13 ?x)
          )
          (and
            (LeftOf4 ?x)
            (RightOf10 ?x)
          )
          (and
            (RightOf12 ?x)
            (LeftOf4 ?x)
          )
          (and
            (RightOf11 ?x)
            (LeftOf3 ?x)
          )
          (and
            (RightOf17 ?x)
            (LeftOf1 ?x)
          )
          (and
            (LeftOf1 ?x)
            (RightOf8 ?x)
          )
          (and
            (RightOf15 ?x)
            (LeftOf3 ?x)
          )
          (and
            (RightOf5 ?x)
            (LeftOf3 ?x)
          )
          (and
            (RightOf16 ?x)
            (LeftOf1 ?x)
          )
          (and
            (LeftOf4 ?x)
            (RightOf8 ?x)
          )
          (and
            (RightOf12 ?x)
            (LeftOf2 ?x)
          )
          (and
            (RightOf7 ?x)
            (LeftOf2 ?x)
          )
          (and
            (RightOf3 ?x)
            (LeftOf3 ?x)
          )
          (and
            (RightOf10 ?x)
            (LeftOf3 ?x)
          )
          (and
            (RightOf12 ?x)
            (LeftOf1 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf10 ?x)
          )
          (and
            (RightOf14 ?x)
            (LeftOf4 ?x)
          )
          (and
            (RightOf4 ?x)
            (LeftOf1 ?x)
          )
          (and
            (RightOf14 ?x)
            (LeftOf2 ?x)
          )
          (and
            (RightOf12 ?x)
            (LeftOf3 ?x)
          )
          (and
            (RightOf17 ?x)
            (LeftOf2 ?x)
          )
          (and
            (RightOf5 ?x)
            (LeftOf1 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf3 ?x)
          )
          (and
            (LeftOf1 ?x)
            (RightOf11 ?x)
          )
          (and
            (RightOf7 ?x)
            (LeftOf3 ?x)
          )
          (and
            (RightOf15 ?x)
            (LeftOf4 ?x)
          )
          (and
            (LeftOf1 ?x)
            (RightOf5 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf16 ?x)
          )
          (and
            (RightOf7 ?x)
            (LeftOf1 ?x)
          )
          (and
            (RightOf8 ?x)
            (LeftOf3 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf5 ?x)
          )
          (and
            (RightOf7 ?x)
            (LeftOf4 ?x)
          )
          (and
            (RightOf16 ?x)
            (LeftOf3 ?x)
          )
          (and
            (RightOf4 ?x)
            (LeftOf4 ?x)
          )
          (and
            (RightOf17 ?x)
            (LeftOf4 ?x)
          )
          (and
            (LeftOf1 ?x)
            (RightOf10 ?x)
          )
          (and
            (RightOf15 ?x)
            (LeftOf1 ?x)
          )
          (and
            (RightOf4 ?x)
            (LeftOf3 ?x)
          )
          (and
            (RightOf13 ?x)
            (LeftOf3 ?x)
          )
          (and
            (RightOf14 ?x)
            (LeftOf1 ?x)
          )
          (and
            (RightOf14 ?x)
            (LeftOf3 ?x)
          )
          (and
            (LeftOf1 ?x)
            (RightOf3 ?x)
          )
          (and
            (RightOf9 ?x)
            (LeftOf1 ?x)
          )
          (and
            (RightOf6 ?x)
            (LeftOf3 ?x)
          )
          (and
            (RightOf9 ?x)
            (LeftOf2 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf13 ?x)
          )
          (and
            (RightOf15 ?x)
            (LeftOf2 ?x)
          )
        )
        (and
          (Column4 ?x)
        )
      )
      (when
        (or
          (and
            (LeftOf5 ?x)
            (RightOf17 ?x)
          )
          (and
            (RightOf4 ?x)
            (LeftOf2 ?x)
          )
          (and
            (RightOf9 ?x)
            (LeftOf3 ?x)
          )
          (and
            (RightOf6 ?x)
            (LeftOf2 ?x)
          )
          (and
            (RightOf11 ?x)
            (LeftOf4 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf5 ?x)
          )
          (and
            (RightOf6 ?x)
            (LeftOf1 ?x)
          )
          (and
            (RightOf5 ?x)
            (LeftOf3 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf14 ?x)
          )
          (and
            (LeftOf4 ?x)
            (RightOf13 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf8 ?x)
          )
          (and
            (RightOf5 ?x)
            (LeftOf4 ?x)
          )
          (and
            (RightOf6 ?x)
            (LeftOf4 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf11 ?x)
          )
          (and
            (LeftOf1 ?x)
            (RightOf13 ?x)
          )
          (and
            (RightOf16 ?x)
            (LeftOf4 ?x)
          )
          (and
            (RightOf9 ?x)
            (LeftOf4 ?x)
          )
          (and
            (RightOf17 ?x)
            (LeftOf3 ?x)
          )
          (and
            (LeftOf3 ?x)
            (RightOf13 ?x)
          )
          (and
            (LeftOf4 ?x)
            (RightOf10 ?x)
          )
          (and
            (RightOf12 ?x)
            (LeftOf4 ?x)
          )
          (and
            (RightOf11 ?x)
            (LeftOf3 ?x)
          )
          (and
            (RightOf17 ?x)
            (LeftOf1 ?x)
          )
          (and
            (RightOf14 ?x)
            (LeftOf2 ?x)
          )
          (and
            (RightOf12 ?x)
            (LeftOf3 ?x)
          )
          (and
            (RightOf5 ?x)
            (LeftOf1 ?x)
          )
          (and
            (RightOf16 ?x)
            (LeftOf1 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf8 ?x)
          )
          (and
            (RightOf12 ?x)
            (LeftOf5 ?x)
          )
          (and
            (LeftOf4 ?x)
            (RightOf8 ?x)
          )
          (and
            (RightOf12 ?x)
            (LeftOf2 ?x)
          )
          (and
            (RightOf7 ?x)
            (LeftOf2 ?x)
          )
          (and
            (RightOf10 ?x)
            (LeftOf4 ?x)
          )
          (and
            (RightOf10 ?x)
            (LeftOf3 ?x)
          )
          (and
            (RightOf12 ?x)
            (LeftOf1 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf10 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf10 ?x)
          )
          (and
            (RightOf4 ?x)
            (LeftOf1 ?x)
          )
          (and
            (RightOf6 ?x)
            (LeftOf5 ?x)
          )
          (and
            (RightOf8 ?x)
            (LeftOf4 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf16 ?x)
          )
          (and
            (RightOf15 ?x)
            (LeftOf3 ?x)
          )
          (and
            (RightOf17 ?x)
            (LeftOf2 ?x)
          )
          (and
            (RightOf14 ?x)
            (LeftOf4 ?x)
          )
          (and
            (LeftOf1 ?x)
            (RightOf11 ?x)
          )
          (and
            (RightOf7 ?x)
            (LeftOf3 ?x)
          )
          (and
            (RightOf15 ?x)
            (LeftOf4 ?x)
          )
          (and
            (LeftOf1 ?x)
            (RightOf5 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf16 ?x)
          )
          (and
            (RightOf7 ?x)
            (LeftOf1 ?x)
          )
          (and
            (RightOf8 ?x)
            (LeftOf3 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf5 ?x)
          )
          (and
            (RightOf7 ?x)
            (LeftOf4 ?x)
          )
          (and
            (RightOf15 ?x)
            (LeftOf2 ?x)
          )
          (and
            (RightOf16 ?x)
            (LeftOf3 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf13 ?x)
          )
          (and
            (RightOf4 ?x)
            (LeftOf4 ?x)
          )
          (and
            (RightOf17 ?x)
            (LeftOf4 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf11 ?x)
          )
          (and
            (LeftOf1 ?x)
            (RightOf8 ?x)
          )
          (and
            (LeftOf1 ?x)
            (RightOf10 ?x)
          )
          (and
            (RightOf15 ?x)
            (LeftOf1 ?x)
          )
          (and
            (RightOf4 ?x)
            (LeftOf3 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf7 ?x)
          )
          (and
            (RightOf13 ?x)
            (LeftOf3 ?x)
          )
          (and
            (RightOf14 ?x)
            (LeftOf1 ?x)
          )
          (and
            (RightOf14 ?x)
            (LeftOf3 ?x)
          )
          (and
            (RightOf9 ?x)
            (LeftOf1 ?x)
          )
          (and
            (RightOf6 ?x)
            (LeftOf3 ?x)
          )
          (and
            (RightOf9 ?x)
            (LeftOf2 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf4 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf13 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf15 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf9 ?x)
          )
        )
        (and
          (Column5 ?x)
        )
      )
      (when
        (or
          (and
            (LeftOf5 ?x)
            (RightOf17 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf9 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf11 ?x)
          )
          (and
            (RightOf6 ?x)
            (LeftOf2 ?x)
          )
          (and
            (RightOf11 ?x)
            (LeftOf4 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf5 ?x)
          )
          (and
            (RightOf6 ?x)
            (LeftOf1 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf14 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf14 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf8 ?x)
          )
          (and
            (RightOf5 ?x)
            (LeftOf4 ?x)
          )
          (and
            (RightOf6 ?x)
            (LeftOf4 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf13 ?x)
          )
          (and
            (LeftOf1 ?x)
            (RightOf13 ?x)
          )
          (and
            (RightOf16 ?x)
            (LeftOf4 ?x)
          )
          (and
            (RightOf9 ?x)
            (LeftOf4 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf6 ?x)
          )
          (and
            (RightOf17 ?x)
            (LeftOf3 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf10 ?x)
          )
          (and
            (LeftOf3 ?x)
            (RightOf13 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf7 ?x)
          )
          (and
            (LeftOf4 ?x)
            (RightOf10 ?x)
          )
          (and
            (RightOf12 ?x)
            (LeftOf4 ?x)
          )
          (and
            (RightOf11 ?x)
            (LeftOf3 ?x)
          )
          (and
            (RightOf17 ?x)
            (LeftOf1 ?x)
          )
          (and
            (LeftOf1 ?x)
            (RightOf8 ?x)
          )
          (and
            (RightOf15 ?x)
            (LeftOf3 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf5 ?x)
          )
          (and
            (RightOf5 ?x)
            (LeftOf3 ?x)
          )
          (and
            (RightOf16 ?x)
            (LeftOf1 ?x)
          )
          (and
            (RightOf14 ?x)
            (LeftOf2 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf8 ?x)
          )
          (and
            (RightOf12 ?x)
            (LeftOf5 ?x)
          )
          (and
            (LeftOf4 ?x)
            (RightOf8 ?x)
          )
          (and
            (RightOf12 ?x)
            (LeftOf2 ?x)
          )
          (and
            (RightOf7 ?x)
            (LeftOf1 ?x)
          )
          (and
            (RightOf10 ?x)
            (LeftOf4 ?x)
          )
          (and
            (RightOf10 ?x)
            (LeftOf3 ?x)
          )
          (and
            (RightOf12 ?x)
            (LeftOf1 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf10 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf10 ?x)
          )
          (and
            (RightOf6 ?x)
            (LeftOf5 ?x)
          )
          (and
            (LeftOf1 ?x)
            (RightOf5 ?x)
          )
          (and
            (RightOf6 ?x)
            (LeftOf3 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf17 ?x)
          )
          (and
            (RightOf8 ?x)
            (LeftOf4 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf11 ?x)
          )
          (and
            (RightOf12 ?x)
            (LeftOf3 ?x)
          )
          (and
            (RightOf17 ?x)
            (LeftOf2 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf8 ?x)
          )
          (and
            (RightOf14 ?x)
            (LeftOf4 ?x)
          )
          (and
            (LeftOf1 ?x)
            (RightOf11 ?x)
          )
          (and
            (RightOf7 ?x)
            (LeftOf3 ?x)
          )
          (and
            (RightOf15 ?x)
            (LeftOf4 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf12 ?x)
          )
          (and
            (LeftOf4 ?x)
            (RightOf13 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf16 ?x)
          )
          (and
            (RightOf7 ?x)
            (LeftOf2 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf15 ?x)
          )
          (and
            (RightOf8 ?x)
            (LeftOf3 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf16 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf5 ?x)
          )
          (and
            (RightOf7 ?x)
            (LeftOf4 ?x)
          )
          (and
            (RightOf15 ?x)
            (LeftOf2 ?x)
          )
          (and
            (RightOf16 ?x)
            (LeftOf3 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf13 ?x)
          )
          (and
            (LeftOf1 ?x)
            (RightOf10 ?x)
          )
          (and
            (RightOf15 ?x)
            (LeftOf1 ?x)
          )
          (and
            (RightOf9 ?x)
            (LeftOf3 ?x)
          )
          (and
            (RightOf17 ?x)
            (LeftOf4 ?x)
          )
          (and
            (RightOf13 ?x)
            (LeftOf3 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf16 ?x)
          )
          (and
            (RightOf14 ?x)
            (LeftOf1 ?x)
          )
          (and
            (RightOf14 ?x)
            (LeftOf3 ?x)
          )
          (and
            (RightOf9 ?x)
            (LeftOf1 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf7 ?x)
          )
          (and
            (RightOf9 ?x)
            (LeftOf2 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf9 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf13 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf15 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf11 ?x)
          )
        )
        (and
          (Column6 ?x)
        )
      )
      (when
        (or
          (and
            (RightOf11 ?x)
            (LeftOf4 ?x)
          )
          (and
            (LeftOf7 ?x)
            (RightOf10 ?x)
          )
          (and
            (RightOf15 ?x)
            (LeftOf2 ?x)
          )
          (and
            (RightOf9 ?x)
            (LeftOf4 ?x)
          )
          (and
            (LeftOf3 ?x)
            (RightOf13 ?x)
          )
          (and
            (RightOf12 ?x)
            (LeftOf4 ?x)
          )
          (and
            (RightOf11 ?x)
            (LeftOf3 ?x)
          )
          (and
            (LeftOf1 ?x)
            (RightOf8 ?x)
          )
          (and
            (RightOf14 ?x)
            (LeftOf7 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf8 ?x)
          )
          (and
            (LeftOf7 ?x)
            (RightOf16 ?x)
          )
          (and
            (LeftOf4 ?x)
            (RightOf8 ?x)
          )
          (and
            (RightOf16 ?x)
            (LeftOf3 ?x)
          )
          (and
            (LeftOf1 ?x)
            (RightOf11 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf8 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf16 ?x)
          )
          (and
            (RightOf10 ?x)
            (LeftOf4 ?x)
          )
          (and
            (RightOf17 ?x)
            (LeftOf4 ?x)
          )
          (and
            (RightOf6 ?x)
            (LeftOf3 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf16 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf11 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf17 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf9 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf11 ?x)
          )
          (and
            (RightOf6 ?x)
            (LeftOf2 ?x)
          )
          (and
            (RightOf6 ?x)
            (LeftOf4 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf13 ?x)
          )
          (and
            (LeftOf1 ?x)
            (RightOf13 ?x)
          )
          (and
            (LeftOf4 ?x)
            (RightOf10 ?x)
          )
          (and
            (RightOf17 ?x)
            (LeftOf1 ?x)
          )
          (and
            (RightOf14 ?x)
            (LeftOf2 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf10 ?x)
          )
          (and
            (RightOf12 ?x)
            (LeftOf3 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf7 ?x)
          )
          (and
            (LeftOf7 ?x)
            (RightOf17 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf14 ?x)
          )
          (and
            (RightOf16 ?x)
            (LeftOf4 ?x)
          )
          (and
            (RightOf7 ?x)
            (LeftOf4 ?x)
          )
          (and
            (LeftOf7 ?x)
            (RightOf8 ?x)
          )
          (and
            (RightOf14 ?x)
            (LeftOf3 ?x)
          )
          (and
            (LeftOf7 ?x)
            (RightOf11 ?x)
          )
          (and
            (RightOf9 ?x)
            (LeftOf1 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf15 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf9 ?x)
          )
          (and
            (RightOf15 ?x)
            (LeftOf7 ?x)
          )
          (and
            (RightOf9 ?x)
            (LeftOf3 ?x)
          )
          (and
            (RightOf16 ?x)
            (LeftOf1 ?x)
          )
          (and
            (RightOf14 ?x)
            (LeftOf1 ?x)
          )
          (and
            (LeftOf4 ?x)
            (RightOf13 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf10 ?x)
          )
          (and
            (RightOf12 ?x)
            (LeftOf6 ?x)
          )
          (and
            (RightOf6 ?x)
            (LeftOf7 ?x)
          )
          (and
            (RightOf15 ?x)
            (LeftOf4 ?x)
          )
          (and
            (RightOf12 ?x)
            (LeftOf5 ?x)
          )
          (and
            (RightOf12 ?x)
            (LeftOf7 ?x)
          )
          (and
            (RightOf7 ?x)
            (LeftOf1 ?x)
          )
          (and
            (RightOf12 ?x)
            (LeftOf1 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf17 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf16 ?x)
          )
          (and
            (RightOf15 ?x)
            (LeftOf3 ?x)
          )
          (and
            (RightOf17 ?x)
            (LeftOf2 ?x)
          )
          (and
            (RightOf14 ?x)
            (LeftOf4 ?x)
          )
          (and
            (RightOf7 ?x)
            (LeftOf3 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf12 ?x)
          )
          (and
            (RightOf7 ?x)
            (LeftOf2 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf7 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf13 ?x)
          )
          (and
            (LeftOf1 ?x)
            (RightOf10 ?x)
          )
          (and
            (RightOf13 ?x)
            (LeftOf3 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf13 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf14 ?x)
          )
          (and
            (RightOf6 ?x)
            (LeftOf1 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf8 ?x)
          )
          (and
            (RightOf17 ?x)
            (LeftOf3 ?x)
          )
          (and
            (RightOf6 ?x)
            (LeftOf6 ?x)
          )
          (and
            (RightOf9 ?x)
            (LeftOf7 ?x)
          )
          (and
            (RightOf9 ?x)
            (LeftOf2 ?x)
          )
          (and
            (RightOf12 ?x)
            (LeftOf2 ?x)
          )
          (and
            (RightOf8 ?x)
            (LeftOf3 ?x)
          )
          (and
            (RightOf10 ?x)
            (LeftOf3 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf10 ?x)
          )
          (and
            (RightOf6 ?x)
            (LeftOf5 ?x)
          )
          (and
            (RightOf8 ?x)
            (LeftOf4 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf11 ?x)
          )
          (and
            (LeftOf7 ?x)
            (RightOf13 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf15 ?x)
          )
          (and
            (RightOf7 ?x)
            (LeftOf7 ?x)
          )
          (and
            (RightOf15 ?x)
            (LeftOf1 ?x)
          )
        )
        (and
          (Column7 ?x)
        )
      )
      (when
        (or
          (and
            (RightOf11 ?x)
            (LeftOf4 ?x)
          )
          (and
            (LeftOf7 ?x)
            (RightOf10 ?x)
          )
          (and
            (RightOf15 ?x)
            (LeftOf2 ?x)
          )
          (and
            (RightOf9 ?x)
            (LeftOf4 ?x)
          )
          (and
            (LeftOf3 ?x)
            (RightOf13 ?x)
          )
          (and
            (RightOf12 ?x)
            (LeftOf4 ?x)
          )
          (and
            (RightOf11 ?x)
            (LeftOf3 ?x)
          )
          (and
            (LeftOf1 ?x)
            (RightOf8 ?x)
          )
          (and
            (RightOf14 ?x)
            (LeftOf7 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf8 ?x)
          )
          (and
            (LeftOf7 ?x)
            (RightOf16 ?x)
          )
          (and
            (LeftOf4 ?x)
            (RightOf8 ?x)
          )
          (and
            (RightOf16 ?x)
            (LeftOf3 ?x)
          )
          (and
            (LeftOf8 ?x)
            (RightOf8 ?x)
          )
          (and
            (LeftOf1 ?x)
            (RightOf11 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf8 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf16 ?x)
          )
          (and
            (RightOf10 ?x)
            (LeftOf4 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf10 ?x)
          )
          (and
            (RightOf17 ?x)
            (LeftOf4 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf7 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf16 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf11 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf17 ?x)
          )
          (and
            (LeftOf8 ?x)
            (RightOf17 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf9 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf11 ?x)
          )
          (and
            (LeftOf8 ?x)
            (RightOf11 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf13 ?x)
          )
          (and
            (LeftOf1 ?x)
            (RightOf13 ?x)
          )
          (and
            (LeftOf4 ?x)
            (RightOf10 ?x)
          )
          (and
            (RightOf17 ?x)
            (LeftOf1 ?x)
          )
          (and
            (RightOf12 ?x)
            (LeftOf8 ?x)
          )
          (and
            (RightOf12 ?x)
            (LeftOf3 ?x)
          )
          (and
            (LeftOf7 ?x)
            (RightOf17 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf14 ?x)
          )
          (and
            (LeftOf8 ?x)
            (RightOf14 ?x)
          )
          (and
            (RightOf16 ?x)
            (LeftOf4 ?x)
          )
          (and
            (RightOf7 ?x)
            (LeftOf4 ?x)
          )
          (and
            (LeftOf8 ?x)
            (RightOf7 ?x)
          )
          (and
            (RightOf9 ?x)
            (LeftOf8 ?x)
          )
          (and
            (LeftOf7 ?x)
            (RightOf8 ?x)
          )
          (and
            (RightOf14 ?x)
            (LeftOf3 ?x)
          )
          (and
            (LeftOf7 ?x)
            (RightOf11 ?x)
          )
          (and
            (RightOf14 ?x)
            (LeftOf2 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf15 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf9 ?x)
          )
          (and
            (RightOf15 ?x)
            (LeftOf7 ?x)
          )
          (and
            (RightOf9 ?x)
            (LeftOf3 ?x)
          )
          (and
            (RightOf16 ?x)
            (LeftOf1 ?x)
          )
          (and
            (RightOf14 ?x)
            (LeftOf1 ?x)
          )
          (and
            (LeftOf4 ?x)
            (RightOf13 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf10 ?x)
          )
          (and
            (RightOf12 ?x)
            (LeftOf6 ?x)
          )
          (and
            (RightOf15 ?x)
            (LeftOf4 ?x)
          )
          (and
            (RightOf15 ?x)
            (LeftOf8 ?x)
          )
          (and
            (RightOf12 ?x)
            (LeftOf5 ?x)
          )
          (and
            (RightOf12 ?x)
            (LeftOf7 ?x)
          )
          (and
            (RightOf7 ?x)
            (LeftOf2 ?x)
          )
          (and
            (RightOf12 ?x)
            (LeftOf1 ?x)
          )
          (and
            (LeftOf7 ?x)
            (RightOf7 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf17 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf16 ?x)
          )
          (and
            (RightOf15 ?x)
            (LeftOf3 ?x)
          )
          (and
            (RightOf17 ?x)
            (LeftOf2 ?x)
          )
          (and
            (RightOf14 ?x)
            (LeftOf4 ?x)
          )
          (and
            (RightOf7 ?x)
            (LeftOf3 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf12 ?x)
          )
          (and
            (RightOf7 ?x)
            (LeftOf1 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf7 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf13 ?x)
          )
          (and
            (LeftOf8 ?x)
            (RightOf13 ?x)
          )
          (and
            (LeftOf1 ?x)
            (RightOf10 ?x)
          )
          (and
            (RightOf13 ?x)
            (LeftOf3 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf13 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf14 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf8 ?x)
          )
          (and
            (RightOf17 ?x)
            (LeftOf3 ?x)
          )
          (and
            (RightOf9 ?x)
            (LeftOf7 ?x)
          )
          (and
            (RightOf9 ?x)
            (LeftOf2 ?x)
          )
          (and
            (LeftOf8 ?x)
            (RightOf16 ?x)
          )
          (and
            (RightOf12 ?x)
            (LeftOf2 ?x)
          )
          (and
            (RightOf8 ?x)
            (LeftOf3 ?x)
          )
          (and
            (RightOf10 ?x)
            (LeftOf3 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf10 ?x)
          )
          (and
            (LeftOf8 ?x)
            (RightOf10 ?x)
          )
          (and
            (RightOf8 ?x)
            (LeftOf4 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf11 ?x)
          )
          (and
            (RightOf9 ?x)
            (LeftOf1 ?x)
          )
          (and
            (LeftOf7 ?x)
            (RightOf13 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf15 ?x)
          )
          (and
            (RightOf15 ?x)
            (LeftOf1 ?x)
          )
        )
        (and
          (Column8 ?x)
        )
      )
      (when
        (or
          (and
            (RightOf14 ?x)
            (LeftOf9 ?x)
          )
          (and
            (LeftOf7 ?x)
            (RightOf10 ?x)
          )
          (and
            (RightOf15 ?x)
            (LeftOf2 ?x)
          )
          (and
            (RightOf9 ?x)
            (LeftOf4 ?x)
          )
          (and
            (LeftOf3 ?x)
            (RightOf13 ?x)
          )
          (and
            (RightOf11 ?x)
            (LeftOf4 ?x)
          )
          (and
            (RightOf12 ?x)
            (LeftOf4 ?x)
          )
          (and
            (RightOf17 ?x)
            (LeftOf9 ?x)
          )
          (and
            (LeftOf1 ?x)
            (RightOf8 ?x)
          )
          (and
            (RightOf14 ?x)
            (LeftOf7 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf8 ?x)
          )
          (and
            (LeftOf7 ?x)
            (RightOf16 ?x)
          )
          (and
            (RightOf16 ?x)
            (LeftOf3 ?x)
          )
          (and
            (LeftOf8 ?x)
            (RightOf8 ?x)
          )
          (and
            (LeftOf1 ?x)
            (RightOf11 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf8 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf16 ?x)
          )
          (and
            (RightOf10 ?x)
            (LeftOf4 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf10 ?x)
          )
          (and
            (RightOf17 ?x)
            (LeftOf4 ?x)
          )
          (and
            (RightOf16 ?x)
            (LeftOf9 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf16 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf11 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf17 ?x)
          )
          (and
            (LeftOf8 ?x)
            (RightOf17 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf9 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf11 ?x)
          )
          (and
            (LeftOf8 ?x)
            (RightOf11 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf13 ?x)
          )
          (and
            (LeftOf1 ?x)
            (RightOf13 ?x)
          )
          (and
            (LeftOf4 ?x)
            (RightOf10 ?x)
          )
          (and
            (RightOf17 ?x)
            (LeftOf1 ?x)
          )
          (and
            (RightOf12 ?x)
            (LeftOf8 ?x)
          )
          (and
            (LeftOf9 ?x)
            (RightOf10 ?x)
          )
          (and
            (RightOf12 ?x)
            (LeftOf3 ?x)
          )
          (and
            (LeftOf7 ?x)
            (RightOf17 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf14 ?x)
          )
          (and
            (LeftOf8 ?x)
            (RightOf14 ?x)
          )
          (and
            (RightOf16 ?x)
            (LeftOf4 ?x)
          )
          (and
            (RightOf9 ?x)
            (LeftOf8 ?x)
          )
          (and
            (RightOf11 ?x)
            (LeftOf3 ?x)
          )
          (and
            (RightOf15 ?x)
            (LeftOf9 ?x)
          )
          (and
            (RightOf14 ?x)
            (LeftOf3 ?x)
          )
          (and
            (LeftOf7 ?x)
            (RightOf11 ?x)
          )
          (and
            (RightOf14 ?x)
            (LeftOf2 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf15 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf9 ?x)
          )
          (and
            (RightOf15 ?x)
            (LeftOf7 ?x)
          )
          (and
            (RightOf9 ?x)
            (LeftOf3 ?x)
          )
          (and
            (RightOf16 ?x)
            (LeftOf1 ?x)
          )
          (and
            (RightOf14 ?x)
            (LeftOf1 ?x)
          )
          (and
            (LeftOf4 ?x)
            (RightOf13 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf10 ?x)
          )
          (and
            (RightOf12 ?x)
            (LeftOf6 ?x)
          )
          (and
            (RightOf15 ?x)
            (LeftOf4 ?x)
          )
          (and
            (RightOf15 ?x)
            (LeftOf8 ?x)
          )
          (and
            (LeftOf9 ?x)
            (RightOf11 ?x)
          )
          (and
            (RightOf12 ?x)
            (LeftOf5 ?x)
          )
          (and
            (RightOf12 ?x)
            (LeftOf7 ?x)
          )
          (and
            (RightOf14 ?x)
            (LeftOf8 ?x)
          )
          (and
            (RightOf12 ?x)
            (LeftOf1 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf17 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf16 ?x)
          )
          (and
            (RightOf15 ?x)
            (LeftOf3 ?x)
          )
          (and
            (RightOf17 ?x)
            (LeftOf2 ?x)
          )
          (and
            (RightOf14 ?x)
            (LeftOf4 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf12 ?x)
          )
          (and
            (RightOf9 ?x)
            (LeftOf9 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf13 ?x)
          )
          (and
            (LeftOf8 ?x)
            (RightOf13 ?x)
          )
          (and
            (LeftOf1 ?x)
            (RightOf10 ?x)
          )
          (and
            (RightOf13 ?x)
            (LeftOf3 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf13 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf14 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf8 ?x)
          )
          (and
            (LeftOf7 ?x)
            (RightOf8 ?x)
          )
          (and
            (RightOf17 ?x)
            (LeftOf3 ?x)
          )
          (and
            (RightOf9 ?x)
            (LeftOf7 ?x)
          )
          (and
            (RightOf9 ?x)
            (LeftOf2 ?x)
          )
          (and
            (LeftOf8 ?x)
            (RightOf16 ?x)
          )
          (and
            (RightOf12 ?x)
            (LeftOf9 ?x)
          )
          (and
            (RightOf12 ?x)
            (LeftOf2 ?x)
          )
          (and
            (RightOf8 ?x)
            (LeftOf3 ?x)
          )
          (and
            (RightOf10 ?x)
            (LeftOf3 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf10 ?x)
          )
          (and
            (LeftOf8 ?x)
            (RightOf10 ?x)
          )
          (and
            (RightOf8 ?x)
            (LeftOf4 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf11 ?x)
          )
          (and
            (RightOf9 ?x)
            (LeftOf1 ?x)
          )
          (and
            (LeftOf7 ?x)
            (RightOf13 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf15 ?x)
          )
          (and
            (LeftOf9 ?x)
            (RightOf8 ?x)
          )
          (and
            (LeftOf9 ?x)
            (RightOf13 ?x)
          )
          (and
            (RightOf15 ?x)
            (LeftOf1 ?x)
          )
        )
        (and
          (Column9 ?x)
        )
      )
      (when
        (or
          (and
            (RightOf14 ?x)
            (LeftOf9 ?x)
          )
          (and
            (LeftOf7 ?x)
            (RightOf10 ?x)
          )
          (and
            (RightOf15 ?x)
            (LeftOf10 ?x)
          )
          (and
            (RightOf15 ?x)
            (LeftOf2 ?x)
          )
          (and
            (RightOf9 ?x)
            (LeftOf4 ?x)
          )
          (and
            (LeftOf3 ?x)
            (RightOf13 ?x)
          )
          (and
            (RightOf11 ?x)
            (LeftOf4 ?x)
          )
          (and
            (RightOf12 ?x)
            (LeftOf4 ?x)
          )
          (and
            (RightOf17 ?x)
            (LeftOf9 ?x)
          )
          (and
            (RightOf14 ?x)
            (LeftOf2 ?x)
          )
          (and
            (LeftOf10 ?x)
            (RightOf16 ?x)
          )
          (and
            (RightOf14 ?x)
            (LeftOf7 ?x)
          )
          (and
            (LeftOf7 ?x)
            (RightOf16 ?x)
          )
          (and
            (RightOf16 ?x)
            (LeftOf3 ?x)
          )
          (and
            (LeftOf1 ?x)
            (RightOf11 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf16 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf10 ?x)
          )
          (and
            (RightOf17 ?x)
            (LeftOf4 ?x)
          )
          (and
            (RightOf16 ?x)
            (LeftOf9 ?x)
          )
          (and
            (LeftOf10 ?x)
            (RightOf10 ?x)
          )
          (and
            (LeftOf10 ?x)
            (RightOf11 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf16 ?x)
          )
          (and
            (LeftOf7 ?x)
            (RightOf11 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf11 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf17 ?x)
          )
          (and
            (LeftOf8 ?x)
            (RightOf17 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf9 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf11 ?x)
          )
          (and
            (LeftOf8 ?x)
            (RightOf11 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf13 ?x)
          )
          (and
            (LeftOf1 ?x)
            (RightOf13 ?x)
          )
          (and
            (LeftOf4 ?x)
            (RightOf10 ?x)
          )
          (and
            (RightOf17 ?x)
            (LeftOf1 ?x)
          )
          (and
            (RightOf9 ?x)
            (LeftOf10 ?x)
          )
          (and
            (RightOf12 ?x)
            (LeftOf8 ?x)
          )
          (and
            (LeftOf9 ?x)
            (RightOf10 ?x)
          )
          (and
            (RightOf12 ?x)
            (LeftOf3 ?x)
          )
          (and
            (LeftOf7 ?x)
            (RightOf17 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf14 ?x)
          )
          (and
            (LeftOf8 ?x)
            (RightOf14 ?x)
          )
          (and
            (RightOf16 ?x)
            (LeftOf4 ?x)
          )
          (and
            (LeftOf10 ?x)
            (RightOf13 ?x)
          )
          (and
            (RightOf9 ?x)
            (LeftOf8 ?x)
          )
          (and
            (RightOf11 ?x)
            (LeftOf3 ?x)
          )
          (and
            (RightOf15 ?x)
            (LeftOf9 ?x)
          )
          (and
            (RightOf14 ?x)
            (LeftOf3 ?x)
          )
          (and
            (LeftOf10 ?x)
            (RightOf14 ?x)
          )
          (and
            (RightOf9 ?x)
            (LeftOf1 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf15 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf9 ?x)
          )
          (and
            (RightOf15 ?x)
            (LeftOf7 ?x)
          )
          (and
            (RightOf9 ?x)
            (LeftOf3 ?x)
          )
          (and
            (RightOf16 ?x)
            (LeftOf1 ?x)
          )
          (and
            (RightOf14 ?x)
            (LeftOf1 ?x)
          )
          (and
            (LeftOf4 ?x)
            (RightOf13 ?x)
          )
          (and
            (LeftOf10 ?x)
            (RightOf17 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf10 ?x)
          )
          (and
            (RightOf12 ?x)
            (LeftOf6 ?x)
          )
          (and
            (RightOf15 ?x)
            (LeftOf4 ?x)
          )
          (and
            (RightOf15 ?x)
            (LeftOf8 ?x)
          )
          (and
            (LeftOf9 ?x)
            (RightOf11 ?x)
          )
          (and
            (RightOf12 ?x)
            (LeftOf5 ?x)
          )
          (and
            (RightOf12 ?x)
            (LeftOf7 ?x)
          )
          (and
            (RightOf14 ?x)
            (LeftOf8 ?x)
          )
          (and
            (RightOf12 ?x)
            (LeftOf1 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf17 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf16 ?x)
          )
          (and
            (RightOf15 ?x)
            (LeftOf3 ?x)
          )
          (and
            (RightOf17 ?x)
            (LeftOf2 ?x)
          )
          (and
            (RightOf14 ?x)
            (LeftOf4 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf12 ?x)
          )
          (and
            (RightOf9 ?x)
            (LeftOf9 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf13 ?x)
          )
          (and
            (LeftOf8 ?x)
            (RightOf13 ?x)
          )
          (and
            (LeftOf1 ?x)
            (RightOf10 ?x)
          )
          (and
            (RightOf13 ?x)
            (LeftOf3 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf13 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf14 ?x)
          )
          (and
            (RightOf17 ?x)
            (LeftOf3 ?x)
          )
          (and
            (RightOf9 ?x)
            (LeftOf7 ?x)
          )
          (and
            (RightOf9 ?x)
            (LeftOf2 ?x)
          )
          (and
            (LeftOf8 ?x)
            (RightOf16 ?x)
          )
          (and
            (RightOf12 ?x)
            (LeftOf9 ?x)
          )
          (and
            (RightOf12 ?x)
            (LeftOf2 ?x)
          )
          (and
            (RightOf10 ?x)
            (LeftOf4 ?x)
          )
          (and
            (RightOf10 ?x)
            (LeftOf3 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf10 ?x)
          )
          (and
            (LeftOf8 ?x)
            (RightOf10 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf11 ?x)
          )
          (and
            (LeftOf7 ?x)
            (RightOf13 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf15 ?x)
          )
          (and
            (LeftOf9 ?x)
            (RightOf13 ?x)
          )
          (and
            (RightOf12 ?x)
            (LeftOf10 ?x)
          )
          (and
            (RightOf15 ?x)
            (LeftOf1 ?x)
          )
        )
        (and
          (Column10 ?x)
        )
      )
      (when
        (or
          (and
            (RightOf14 ?x)
            (LeftOf9 ?x)
          )
          (and
            (LeftOf11 ?x)
            (RightOf10 ?x)
          )
          (and
            (RightOf17 ?x)
            (LeftOf11 ?x)
          )
          (and
            (LeftOf7 ?x)
            (RightOf10 ?x)
          )
          (and
            (RightOf15 ?x)
            (LeftOf10 ?x)
          )
          (and
            (RightOf15 ?x)
            (LeftOf2 ?x)
          )
          (and
            (LeftOf3 ?x)
            (RightOf13 ?x)
          )
          (and
            (RightOf11 ?x)
            (LeftOf4 ?x)
          )
          (and
            (RightOf12 ?x)
            (LeftOf4 ?x)
          )
          (and
            (RightOf17 ?x)
            (LeftOf9 ?x)
          )
          (and
            (RightOf14 ?x)
            (LeftOf2 ?x)
          )
          (and
            (LeftOf10 ?x)
            (RightOf16 ?x)
          )
          (and
            (RightOf14 ?x)
            (LeftOf7 ?x)
          )
          (and
            (LeftOf7 ?x)
            (RightOf16 ?x)
          )
          (and
            (RightOf16 ?x)
            (LeftOf3 ?x)
          )
          (and
            (LeftOf1 ?x)
            (RightOf11 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf16 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf10 ?x)
          )
          (and
            (RightOf17 ?x)
            (LeftOf4 ?x)
          )
          (and
            (RightOf16 ?x)
            (LeftOf9 ?x)
          )
          (and
            (LeftOf10 ?x)
            (RightOf10 ?x)
          )
          (and
            (LeftOf10 ?x)
            (RightOf11 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf16 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf11 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf17 ?x)
          )
          (and
            (LeftOf8 ?x)
            (RightOf17 ?x)
          )
          (and
            (RightOf12 ?x)
            (LeftOf11 ?x)
          )
          (and
            (LeftOf8 ?x)
            (RightOf11 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf13 ?x)
          )
          (and
            (LeftOf1 ?x)
            (RightOf13 ?x)
          )
          (and
            (LeftOf7 ?x)
            (RightOf11 ?x)
          )
          (and
            (RightOf17 ?x)
            (LeftOf1 ?x)
          )
          (and
            (RightOf12 ?x)
            (LeftOf8 ?x)
          )
          (and
            (LeftOf9 ?x)
            (RightOf10 ?x)
          )
          (and
            (RightOf12 ?x)
            (LeftOf3 ?x)
          )
          (and
            (LeftOf7 ?x)
            (RightOf17 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf14 ?x)
          )
          (and
            (LeftOf8 ?x)
            (RightOf14 ?x)
          )
          (and
            (RightOf16 ?x)
            (LeftOf4 ?x)
          )
          (and
            (LeftOf10 ?x)
            (RightOf13 ?x)
          )
          (and
            (RightOf11 ?x)
            (LeftOf3 ?x)
          )
          (and
            (RightOf15 ?x)
            (LeftOf9 ?x)
          )
          (and
            (RightOf14 ?x)
            (LeftOf3 ?x)
          )
          (and
            (LeftOf10 ?x)
            (RightOf14 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf15 ?x)
          )
          (and
            (RightOf14 ?x)
            (LeftOf11 ?x)
          )
          (and
            (RightOf15 ?x)
            (LeftOf7 ?x)
          )
          (and
            (RightOf16 ?x)
            (LeftOf1 ?x)
          )
          (and
            (RightOf14 ?x)
            (LeftOf1 ?x)
          )
          (and
            (LeftOf4 ?x)
            (RightOf13 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf11 ?x)
          )
          (and
            (LeftOf10 ?x)
            (RightOf17 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf10 ?x)
          )
          (and
            (RightOf12 ?x)
            (LeftOf6 ?x)
          )
          (and
            (RightOf15 ?x)
            (LeftOf4 ?x)
          )
          (and
            (RightOf15 ?x)
            (LeftOf8 ?x)
          )
          (and
            (LeftOf11 ?x)
            (RightOf13 ?x)
          )
          (and
            (LeftOf9 ?x)
            (RightOf11 ?x)
          )
          (and
            (RightOf12 ?x)
            (LeftOf5 ?x)
          )
          (and
            (RightOf12 ?x)
            (LeftOf7 ?x)
          )
          (and
            (RightOf14 ?x)
            (LeftOf8 ?x)
          )
          (and
            (RightOf12 ?x)
            (LeftOf1 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf17 ?x)
          )
          (and
            (RightOf14 ?x)
            (LeftOf10 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf16 ?x)
          )
          (and
            (RightOf15 ?x)
            (LeftOf3 ?x)
          )
          (and
            (RightOf17 ?x)
            (LeftOf2 ?x)
          )
          (and
            (RightOf14 ?x)
            (LeftOf4 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf12 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf13 ?x)
          )
          (and
            (LeftOf8 ?x)
            (RightOf13 ?x)
          )
          (and
            (LeftOf1 ?x)
            (RightOf10 ?x)
          )
          (and
            (RightOf13 ?x)
            (LeftOf3 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf13 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf14 ?x)
          )
          (and
            (RightOf17 ?x)
            (LeftOf3 ?x)
          )
          (and
            (LeftOf8 ?x)
            (RightOf16 ?x)
          )
          (and
            (RightOf12 ?x)
            (LeftOf9 ?x)
          )
          (and
            (LeftOf11 ?x)
            (RightOf11 ?x)
          )
          (and
            (RightOf12 ?x)
            (LeftOf2 ?x)
          )
          (and
            (RightOf10 ?x)
            (LeftOf4 ?x)
          )
          (and
            (RightOf10 ?x)
            (LeftOf3 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf10 ?x)
          )
          (and
            (LeftOf8 ?x)
            (RightOf10 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf11 ?x)
          )
          (and
            (LeftOf7 ?x)
            (RightOf13 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf15 ?x)
          )
          (and
            (RightOf16 ?x)
            (LeftOf11 ?x)
          )
          (and
            (LeftOf9 ?x)
            (RightOf13 ?x)
          )
          (and
            (RightOf12 ?x)
            (LeftOf10 ?x)
          )
          (and
            (RightOf15 ?x)
            (LeftOf11 ?x)
          )
          (and
            (RightOf15 ?x)
            (LeftOf1 ?x)
          )
        )
        (and
          (Column11 ?x)
        )
      )
      (when
        (or
          (and
            (RightOf14 ?x)
            (LeftOf9 ?x)
          )
          (and
            (RightOf17 ?x)
            (LeftOf11 ?x)
          )
          (and
            (RightOf15 ?x)
            (LeftOf10 ?x)
          )
          (and
            (RightOf15 ?x)
            (LeftOf2 ?x)
          )
          (and
            (LeftOf12 ?x)
            (RightOf13 ?x)
          )
          (and
            (LeftOf3 ?x)
            (RightOf13 ?x)
          )
          (and
            (RightOf11 ?x)
            (LeftOf4 ?x)
          )
          (and
            (RightOf12 ?x)
            (LeftOf4 ?x)
          )
          (and
            (RightOf17 ?x)
            (LeftOf9 ?x)
          )
          (and
            (RightOf14 ?x)
            (LeftOf2 ?x)
          )
          (and
            (LeftOf10 ?x)
            (RightOf16 ?x)
          )
          (and
            (RightOf14 ?x)
            (LeftOf7 ?x)
          )
          (and
            (LeftOf7 ?x)
            (RightOf16 ?x)
          )
          (and
            (RightOf16 ?x)
            (LeftOf3 ?x)
          )
          (and
            (LeftOf12 ?x)
            (RightOf15 ?x)
          )
          (and
            (LeftOf1 ?x)
            (RightOf11 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf16 ?x)
          )
          (and
            (RightOf17 ?x)
            (LeftOf4 ?x)
          )
          (and
            (RightOf16 ?x)
            (LeftOf9 ?x)
          )
          (and
            (LeftOf10 ?x)
            (RightOf11 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf16 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf11 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf17 ?x)
          )
          (and
            (LeftOf8 ?x)
            (RightOf17 ?x)
          )
          (and
            (RightOf12 ?x)
            (LeftOf11 ?x)
          )
          (and
            (LeftOf8 ?x)
            (RightOf11 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf13 ?x)
          )
          (and
            (LeftOf1 ?x)
            (RightOf13 ?x)
          )
          (and
            (RightOf17 ?x)
            (LeftOf1 ?x)
          )
          (and
            (RightOf12 ?x)
            (LeftOf8 ?x)
          )
          (and
            (LeftOf12 ?x)
            (RightOf17 ?x)
          )
          (and
            (RightOf12 ?x)
            (LeftOf3 ?x)
          )
          (and
            (LeftOf7 ?x)
            (RightOf17 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf14 ?x)
          )
          (and
            (LeftOf8 ?x)
            (RightOf14 ?x)
          )
          (and
            (RightOf16 ?x)
            (LeftOf4 ?x)
          )
          (and
            (LeftOf12 ?x)
            (RightOf16 ?x)
          )
          (and
            (LeftOf10 ?x)
            (RightOf13 ?x)
          )
          (and
            (RightOf11 ?x)
            (LeftOf3 ?x)
          )
          (and
            (LeftOf12 ?x)
            (RightOf12 ?x)
          )
          (and
            (RightOf15 ?x)
            (LeftOf9 ?x)
          )
          (and
            (RightOf14 ?x)
            (LeftOf3 ?x)
          )
          (and
            (LeftOf7 ?x)
            (RightOf11 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf15 ?x)
          )
          (and
            (RightOf14 ?x)
            (LeftOf11 ?x)
          )
          (and
            (RightOf15 ?x)
            (LeftOf7 ?x)
          )
          (and
            (RightOf16 ?x)
            (LeftOf1 ?x)
          )
          (and
            (RightOf14 ?x)
            (LeftOf1 ?x)
          )
          (and
            (LeftOf4 ?x)
            (RightOf13 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf11 ?x)
          )
          (and
            (LeftOf10 ?x)
            (RightOf17 ?x)
          )
          (and
            (RightOf12 ?x)
            (LeftOf6 ?x)
          )
          (and
            (RightOf15 ?x)
            (LeftOf4 ?x)
          )
          (and
            (RightOf15 ?x)
            (LeftOf8 ?x)
          )
          (and
            (LeftOf11 ?x)
            (RightOf13 ?x)
          )
          (and
            (LeftOf9 ?x)
            (RightOf11 ?x)
          )
          (and
            (RightOf12 ?x)
            (LeftOf5 ?x)
          )
          (and
            (RightOf12 ?x)
            (LeftOf7 ?x)
          )
          (and
            (RightOf14 ?x)
            (LeftOf8 ?x)
          )
          (and
            (RightOf12 ?x)
            (LeftOf1 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf17 ?x)
          )
          (and
            (RightOf14 ?x)
            (LeftOf10 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf16 ?x)
          )
          (and
            (RightOf15 ?x)
            (LeftOf3 ?x)
          )
          (and
            (RightOf17 ?x)
            (LeftOf2 ?x)
          )
          (and
            (RightOf14 ?x)
            (LeftOf4 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf12 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf13 ?x)
          )
          (and
            (LeftOf8 ?x)
            (RightOf13 ?x)
          )
          (and
            (RightOf13 ?x)
            (LeftOf3 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf13 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf14 ?x)
          )
          (and
            (RightOf17 ?x)
            (LeftOf3 ?x)
          )
          (and
            (LeftOf8 ?x)
            (RightOf16 ?x)
          )
          (and
            (RightOf12 ?x)
            (LeftOf9 ?x)
          )
          (and
            (LeftOf11 ?x)
            (RightOf11 ?x)
          )
          (and
            (RightOf12 ?x)
            (LeftOf2 ?x)
          )
          (and
            (LeftOf10 ?x)
            (RightOf14 ?x)
          )
          (and
            (LeftOf12 ?x)
            (RightOf11 ?x)
          )
          (and
            (LeftOf12 ?x)
            (RightOf14 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf11 ?x)
          )
          (and
            (LeftOf7 ?x)
            (RightOf13 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf15 ?x)
          )
          (and
            (RightOf16 ?x)
            (LeftOf11 ?x)
          )
          (and
            (LeftOf9 ?x)
            (RightOf13 ?x)
          )
          (and
            (RightOf12 ?x)
            (LeftOf10 ?x)
          )
          (and
            (RightOf15 ?x)
            (LeftOf11 ?x)
          )
          (and
            (RightOf15 ?x)
            (LeftOf1 ?x)
          )
        )
        (and
          (Column12 ?x)
        )
      )
      (when
        (or
          (and
            (RightOf15 ?x)
            (LeftOf7 ?x)
          )
          (and
            (LeftOf8 ?x)
            (RightOf17 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf14 ?x)
          )
          (and
            (RightOf12 ?x)
            (LeftOf11 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf13 ?x)
          )
          (and
            (RightOf14 ?x)
            (LeftOf9 ?x)
          )
          (and
            (RightOf14 ?x)
            (LeftOf1 ?x)
          )
          (and
            (RightOf12 ?x)
            (LeftOf12 ?x)
          )
          (and
            (RightOf13 ?x)
            (LeftOf3 ?x)
          )
          (and
            (LeftOf13 ?x)
            (RightOf16 ?x)
          )
          (and
            (LeftOf4 ?x)
            (RightOf13 ?x)
          )
          (and
            (RightOf17 ?x)
            (LeftOf11 ?x)
          )
          (and
            (LeftOf13 ?x)
            (RightOf17 ?x)
          )
          (and
            (RightOf15 ?x)
            (LeftOf10 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf13 ?x)
          )
          (and
            (LeftOf1 ?x)
            (RightOf13 ?x)
          )
          (and
            (LeftOf13 ?x)
            (RightOf13 ?x)
          )
          (and
            (LeftOf12 ?x)
            (RightOf13 ?x)
          )
          (and
            (LeftOf10 ?x)
            (RightOf17 ?x)
          )
          (and
            (RightOf15 ?x)
            (LeftOf2 ?x)
          )
          (and
            (RightOf17 ?x)
            (LeftOf3 ?x)
          )
          (and
            (RightOf12 ?x)
            (LeftOf6 ?x)
          )
          (and
            (RightOf14 ?x)
            (LeftOf7 ?x)
          )
          (and
            (LeftOf3 ?x)
            (RightOf13 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf16 ?x)
          )
          (and
            (RightOf12 ?x)
            (LeftOf4 ?x)
          )
          (and
            (RightOf17 ?x)
            (LeftOf9 ?x)
          )
          (and
            (RightOf17 ?x)
            (LeftOf1 ?x)
          )
          (and
            (RightOf14 ?x)
            (LeftOf2 ?x)
          )
          (and
            (RightOf12 ?x)
            (LeftOf3 ?x)
          )
          (and
            (RightOf15 ?x)
            (LeftOf8 ?x)
          )
          (and
            (LeftOf11 ?x)
            (RightOf13 ?x)
          )
          (and
            (LeftOf10 ?x)
            (RightOf16 ?x)
          )
          (and
            (RightOf14 ?x)
            (LeftOf13 ?x)
          )
          (and
            (RightOf12 ?x)
            (LeftOf9 ?x)
          )
          (and
            (RightOf12 ?x)
            (LeftOf5 ?x)
          )
          (and
            (RightOf12 ?x)
            (LeftOf13 ?x)
          )
          (and
            (RightOf12 ?x)
            (LeftOf2 ?x)
          )
          (and
            (RightOf14 ?x)
            (LeftOf8 ?x)
          )
          (and
            (RightOf12 ?x)
            (LeftOf7 ?x)
          )
          (and
            (RightOf16 ?x)
            (LeftOf3 ?x)
          )
          (and
            (LeftOf8 ?x)
            (RightOf13 ?x)
          )
          (and
            (RightOf12 ?x)
            (LeftOf8 ?x)
          )
          (and
            (LeftOf8 ?x)
            (RightOf16 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf17 ?x)
          )
          (and
            (RightOf14 ?x)
            (LeftOf10 ?x)
          )
          (and
            (LeftOf12 ?x)
            (RightOf14 ?x)
          )
          (and
            (LeftOf12 ?x)
            (RightOf17 ?x)
          )
          (and
            (RightOf15 ?x)
            (LeftOf3 ?x)
          )
          (and
            (RightOf17 ?x)
            (LeftOf2 ?x)
          )
          (and
            (RightOf14 ?x)
            (LeftOf4 ?x)
          )
          (and
            (RightOf16 ?x)
            (LeftOf1 ?x)
          )
          (and
            (RightOf15 ?x)
            (LeftOf4 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf16 ?x)
          )
          (and
            (LeftOf7 ?x)
            (RightOf16 ?x)
          )
          (and
            (LeftOf7 ?x)
            (RightOf13 ?x)
          )
          (and
            (LeftOf7 ?x)
            (RightOf17 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf15 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf14 ?x)
          )
          (and
            (LeftOf8 ?x)
            (RightOf14 ?x)
          )
          (and
            (RightOf16 ?x)
            (LeftOf4 ?x)
          )
          (and
            (LeftOf12 ?x)
            (RightOf16 ?x)
          )
          (and
            (RightOf16 ?x)
            (LeftOf11 ?x)
          )
          (and
            (LeftOf12 ?x)
            (RightOf15 ?x)
          )
          (and
            (RightOf15 ?x)
            (LeftOf13 ?x)
          )
          (and
            (LeftOf9 ?x)
            (RightOf13 ?x)
          )
          (and
            (RightOf12 ?x)
            (LeftOf1 ?x)
          )
          (and
            (LeftOf10 ?x)
            (RightOf13 ?x)
          )
          (and
            (RightOf12 ?x)
            (LeftOf10 ?x)
          )
          (and
            (RightOf17 ?x)
            (LeftOf4 ?x)
          )
          (and
            (RightOf16 ?x)
            (LeftOf9 ?x)
          )
          (and
            (RightOf15 ?x)
            (LeftOf9 ?x)
          )
          (and
            (RightOf15 ?x)
            (LeftOf11 ?x)
          )
          (and
            (RightOf14 ?x)
            (LeftOf3 ?x)
          )
          (and
            (LeftOf10 ?x)
            (RightOf14 ?x)
          )
          (and
            (RightOf15 ?x)
            (LeftOf1 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf16 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf17 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf13 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf15 ?x)
          )
          (and
            (RightOf14 ?x)
            (LeftOf11 ?x)
          )
        )
        (and
          (Column13 ?x)
        )
      )
      (when
        (or
          (and
            (RightOf15 ?x)
            (LeftOf7 ?x)
          )
          (and
            (LeftOf8 ?x)
            (RightOf17 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf14 ?x)
          )
          (and
            (RightOf14 ?x)
            (LeftOf9 ?x)
          )
          (and
            (RightOf14 ?x)
            (LeftOf1 ?x)
          )
          (and
            (RightOf13 ?x)
            (LeftOf3 ?x)
          )
          (and
            (LeftOf13 ?x)
            (RightOf16 ?x)
          )
          (and
            (LeftOf4 ?x)
            (RightOf13 ?x)
          )
          (and
            (RightOf17 ?x)
            (LeftOf11 ?x)
          )
          (and
            (RightOf15 ?x)
            (LeftOf14 ?x)
          )
          (and
            (LeftOf8 ?x)
            (RightOf13 ?x)
          )
          (and
            (LeftOf13 ?x)
            (RightOf17 ?x)
          )
          (and
            (RightOf15 ?x)
            (LeftOf10 ?x)
          )
          (and
            (RightOf17 ?x)
            (LeftOf14 ?x)
          )
          (and
            (LeftOf1 ?x)
            (RightOf13 ?x)
          )
          (and
            (LeftOf13 ?x)
            (RightOf13 ?x)
          )
          (and
            (LeftOf12 ?x)
            (RightOf13 ?x)
          )
          (and
            (LeftOf10 ?x)
            (RightOf17 ?x)
          )
          (and
            (LeftOf10 ?x)
            (RightOf16 ?x)
          )
          (and
            (RightOf17 ?x)
            (LeftOf3 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf13 ?x)
          )
          (and
            (RightOf14 ?x)
            (LeftOf7 ?x)
          )
          (and
            (RightOf15 ?x)
            (LeftOf4 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf16 ?x)
          )
          (and
            (RightOf17 ?x)
            (LeftOf9 ?x)
          )
          (and
            (RightOf17 ?x)
            (LeftOf1 ?x)
          )
          (and
            (RightOf14 ?x)
            (LeftOf2 ?x)
          )
          (and
            (LeftOf8 ?x)
            (RightOf16 ?x)
          )
          (and
            (RightOf15 ?x)
            (LeftOf8 ?x)
          )
          (and
            (LeftOf11 ?x)
            (RightOf13 ?x)
          )
          (and
            (RightOf16 ?x)
            (LeftOf14 ?x)
          )
          (and
            (RightOf14 ?x)
            (LeftOf13 ?x)
          )
          (and
            (LeftOf7 ?x)
            (RightOf16 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf13 ?x)
          )
          (and
            (RightOf14 ?x)
            (LeftOf8 ?x)
          )
          (and
            (LeftOf10 ?x)
            (RightOf14 ?x)
          )
          (and
            (RightOf16 ?x)
            (LeftOf3 ?x)
          )
          (and
            (LeftOf14 ?x)
            (RightOf13 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf17 ?x)
          )
          (and
            (RightOf14 ?x)
            (LeftOf10 ?x)
          )
          (and
            (LeftOf12 ?x)
            (RightOf14 ?x)
          )
          (and
            (LeftOf12 ?x)
            (RightOf17 ?x)
          )
          (and
            (RightOf15 ?x)
            (LeftOf3 ?x)
          )
          (and
            (RightOf17 ?x)
            (LeftOf2 ?x)
          )
          (and
            (RightOf14 ?x)
            (LeftOf4 ?x)
          )
          (and
            (RightOf16 ?x)
            (LeftOf1 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf16 ?x)
          )
          (and
            (LeftOf7 ?x)
            (RightOf13 ?x)
          )
          (and
            (LeftOf7 ?x)
            (RightOf17 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf15 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf14 ?x)
          )
          (and
            (LeftOf8 ?x)
            (RightOf14 ?x)
          )
          (and
            (RightOf16 ?x)
            (LeftOf4 ?x)
          )
          (and
            (LeftOf12 ?x)
            (RightOf16 ?x)
          )
          (and
            (RightOf16 ?x)
            (LeftOf11 ?x)
          )
          (and
            (LeftOf12 ?x)
            (RightOf15 ?x)
          )
          (and
            (RightOf15 ?x)
            (LeftOf13 ?x)
          )
          (and
            (LeftOf9 ?x)
            (RightOf13 ?x)
          )
          (and
            (LeftOf10 ?x)
            (RightOf13 ?x)
          )
          (and
            (RightOf17 ?x)
            (LeftOf4 ?x)
          )
          (and
            (RightOf16 ?x)
            (LeftOf9 ?x)
          )
          (and
            (RightOf15 ?x)
            (LeftOf9 ?x)
          )
          (and
            (RightOf15 ?x)
            (LeftOf11 ?x)
          )
          (and
            (RightOf14 ?x)
            (LeftOf3 ?x)
          )
          (and
            (RightOf14 ?x)
            (LeftOf14 ?x)
          )
          (and
            (RightOf15 ?x)
            (LeftOf1 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf16 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf17 ?x)
          )
          (and
            (RightOf14 ?x)
            (LeftOf11 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf13 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf15 ?x)
          )
          (and
            (RightOf15 ?x)
            (LeftOf2 ?x)
          )
        )
        (and
          (Column14 ?x)
        )
      )
      (when
        (or
          (and
            (RightOf15 ?x)
            (LeftOf7 ?x)
          )
          (and
            (LeftOf8 ?x)
            (RightOf17 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf14 ?x)
          )
          (and
            (LeftOf15 ?x)
            (RightOf14 ?x)
          )
          (and
            (RightOf14 ?x)
            (LeftOf9 ?x)
          )
          (and
            (RightOf14 ?x)
            (LeftOf1 ?x)
          )
          (and
            (LeftOf13 ?x)
            (RightOf16 ?x)
          )
          (and
            (RightOf17 ?x)
            (LeftOf11 ?x)
          )
          (and
            (RightOf15 ?x)
            (LeftOf10 ?x)
          )
          (and
            (RightOf17 ?x)
            (LeftOf14 ?x)
          )
          (and
            (RightOf15 ?x)
            (LeftOf14 ?x)
          )
          (and
            (LeftOf10 ?x)
            (RightOf17 ?x)
          )
          (and
            (LeftOf10 ?x)
            (RightOf16 ?x)
          )
          (and
            (RightOf17 ?x)
            (LeftOf3 ?x)
          )
          (and
            (LeftOf15 ?x)
            (RightOf15 ?x)
          )
          (and
            (RightOf14 ?x)
            (LeftOf7 ?x)
          )
          (and
            (RightOf15 ?x)
            (LeftOf4 ?x)
          )
          (and
            (RightOf16 ?x)
            (LeftOf1 ?x)
          )
          (and
            (RightOf17 ?x)
            (LeftOf9 ?x)
          )
          (and
            (RightOf17 ?x)
            (LeftOf1 ?x)
          )
          (and
            (RightOf14 ?x)
            (LeftOf2 ?x)
          )
          (and
            (RightOf15 ?x)
            (LeftOf3 ?x)
          )
          (and
            (RightOf15 ?x)
            (LeftOf8 ?x)
          )
          (and
            (RightOf16 ?x)
            (LeftOf14 ?x)
          )
          (and
            (RightOf14 ?x)
            (LeftOf13 ?x)
          )
          (and
            (LeftOf7 ?x)
            (RightOf16 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf16 ?x)
          )
          (and
            (RightOf14 ?x)
            (LeftOf8 ?x)
          )
          (and
            (RightOf16 ?x)
            (LeftOf3 ?x)
          )
          (and
            (LeftOf15 ?x)
            (RightOf17 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf17 ?x)
          )
          (and
            (RightOf14 ?x)
            (LeftOf10 ?x)
          )
          (and
            (LeftOf12 ?x)
            (RightOf14 ?x)
          )
          (and
            (LeftOf12 ?x)
            (RightOf17 ?x)
          )
          (and
            (LeftOf8 ?x)
            (RightOf16 ?x)
          )
          (and
            (RightOf17 ?x)
            (LeftOf2 ?x)
          )
          (and
            (RightOf14 ?x)
            (LeftOf4 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf16 ?x)
          )
          (and
            (LeftOf7 ?x)
            (RightOf17 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf15 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf14 ?x)
          )
          (and
            (RightOf16 ?x)
            (LeftOf4 ?x)
          )
          (and
            (LeftOf12 ?x)
            (RightOf16 ?x)
          )
          (and
            (RightOf16 ?x)
            (LeftOf11 ?x)
          )
          (and
            (LeftOf12 ?x)
            (RightOf15 ?x)
          )
          (and
            (RightOf15 ?x)
            (LeftOf13 ?x)
          )
          (and
            (LeftOf13 ?x)
            (RightOf17 ?x)
          )
          (and
            (RightOf15 ?x)
            (LeftOf2 ?x)
          )
          (and
            (LeftOf15 ?x)
            (RightOf16 ?x)
          )
          (and
            (RightOf17 ?x)
            (LeftOf4 ?x)
          )
          (and
            (RightOf16 ?x)
            (LeftOf9 ?x)
          )
          (and
            (RightOf15 ?x)
            (LeftOf9 ?x)
          )
          (and
            (RightOf15 ?x)
            (LeftOf11 ?x)
          )
          (and
            (RightOf14 ?x)
            (LeftOf3 ?x)
          )
          (and
            (RightOf14 ?x)
            (LeftOf14 ?x)
          )
          (and
            (RightOf15 ?x)
            (LeftOf1 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf16 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf17 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf15 ?x)
          )
          (and
            (RightOf14 ?x)
            (LeftOf11 ?x)
          )
        )
        (and
          (Column15 ?x)
        )
      )
      (when
        (or
          (and
            (RightOf15 ?x)
            (LeftOf7 ?x)
          )
          (and
            (RightOf15 ?x)
            (LeftOf16 ?x)
          )
          (and
            (RightOf16 ?x)
            (LeftOf1 ?x)
          )
          (and
            (LeftOf8 ?x)
            (RightOf17 ?x)
          )
          (and
            (LeftOf13 ?x)
            (RightOf16 ?x)
          )
          (and
            (RightOf17 ?x)
            (LeftOf11 ?x)
          )
          (and
            (LeftOf13 ?x)
            (RightOf17 ?x)
          )
          (and
            (RightOf15 ?x)
            (LeftOf10 ?x)
          )
          (and
            (RightOf17 ?x)
            (LeftOf14 ?x)
          )
          (and
            (RightOf15 ?x)
            (LeftOf14 ?x)
          )
          (and
            (LeftOf10 ?x)
            (RightOf17 ?x)
          )
          (and
            (RightOf16 ?x)
            (LeftOf14 ?x)
          )
          (and
            (LeftOf16 ?x)
            (RightOf16 ?x)
          )
          (and
            (RightOf15 ?x)
            (LeftOf4 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf16 ?x)
          )
          (and
            (RightOf17 ?x)
            (LeftOf9 ?x)
          )
          (and
            (RightOf17 ?x)
            (LeftOf1 ?x)
          )
          (and
            (RightOf17 ?x)
            (LeftOf3 ?x)
          )
          (and
            (LeftOf8 ?x)
            (RightOf16 ?x)
          )
          (and
            (RightOf15 ?x)
            (LeftOf8 ?x)
          )
          (and
            (LeftOf10 ?x)
            (RightOf16 ?x)
          )
          (and
            (LeftOf7 ?x)
            (RightOf16 ?x)
          )
          (and
            (RightOf16 ?x)
            (LeftOf3 ?x)
          )
          (and
            (LeftOf15 ?x)
            (RightOf17 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf17 ?x)
          )
          (and
            (LeftOf12 ?x)
            (RightOf17 ?x)
          )
          (and
            (RightOf15 ?x)
            (LeftOf3 ?x)
          )
          (and
            (RightOf17 ?x)
            (LeftOf2 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf16 ?x)
          )
          (and
            (LeftOf7 ?x)
            (RightOf17 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf15 ?x)
          )
          (and
            (RightOf16 ?x)
            (LeftOf4 ?x)
          )
          (and
            (LeftOf12 ?x)
            (RightOf16 ?x)
          )
          (and
            (RightOf16 ?x)
            (LeftOf11 ?x)
          )
          (and
            (LeftOf12 ?x)
            (RightOf15 ?x)
          )
          (and
            (RightOf15 ?x)
            (LeftOf13 ?x)
          )
          (and
            (RightOf15 ?x)
            (LeftOf15 ?x)
          )
          (and
            (LeftOf15 ?x)
            (RightOf16 ?x)
          )
          (and
            (RightOf17 ?x)
            (LeftOf4 ?x)
          )
          (and
            (RightOf16 ?x)
            (LeftOf9 ?x)
          )
          (and
            (RightOf15 ?x)
            (LeftOf9 ?x)
          )
          (and
            (RightOf15 ?x)
            (LeftOf11 ?x)
          )
          (and
            (RightOf17 ?x)
            (LeftOf16 ?x)
          )
          (and
            (RightOf15 ?x)
            (LeftOf1 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf16 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf17 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf15 ?x)
          )
          (and
            (RightOf15 ?x)
            (LeftOf2 ?x)
          )
        )
        (and
          (Column16 ?x)
        )
      )
      (when
        (or
          (and
            (LeftOf5 ?x)
            (RightOf17 ?x)
          )
          (and
            (LeftOf8 ?x)
            (RightOf17 ?x)
          )
          (and
            (RightOf16 ?x)
            (LeftOf1 ?x)
          )
          (and
            (LeftOf13 ?x)
            (RightOf16 ?x)
          )
          (and
            (RightOf17 ?x)
            (LeftOf11 ?x)
          )
          (and
            (RightOf17 ?x)
            (LeftOf14 ?x)
          )
          (and
            (LeftOf10 ?x)
            (RightOf17 ?x)
          )
          (and
            (LeftOf10 ?x)
            (RightOf16 ?x)
          )
          (and
            (LeftOf16 ?x)
            (RightOf16 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf16 ?x)
          )
          (and
            (RightOf17 ?x)
            (LeftOf9 ?x)
          )
          (and
            (RightOf17 ?x)
            (LeftOf1 ?x)
          )
          (and
            (RightOf17 ?x)
            (LeftOf3 ?x)
          )
          (and
            (RightOf16 ?x)
            (LeftOf14 ?x)
          )
          (and
            (RightOf16 ?x)
            (LeftOf17 ?x)
          )
          (and
            (LeftOf7 ?x)
            (RightOf16 ?x)
          )
          (and
            (RightOf16 ?x)
            (LeftOf3 ?x)
          )
          (and
            (LeftOf15 ?x)
            (RightOf17 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf17 ?x)
          )
          (and
            (LeftOf12 ?x)
            (RightOf17 ?x)
          )
          (and
            (LeftOf8 ?x)
            (RightOf16 ?x)
          )
          (and
            (RightOf17 ?x)
            (LeftOf2 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf16 ?x)
          )
          (and
            (LeftOf7 ?x)
            (RightOf17 ?x)
          )
          (and
            (RightOf17 ?x)
            (LeftOf17 ?x)
          )
          (and
            (RightOf16 ?x)
            (LeftOf4 ?x)
          )
          (and
            (LeftOf12 ?x)
            (RightOf16 ?x)
          )
          (and
            (RightOf16 ?x)
            (LeftOf11 ?x)
          )
          (and
            (LeftOf13 ?x)
            (RightOf17 ?x)
          )
          (and
            (LeftOf15 ?x)
            (RightOf16 ?x)
          )
          (and
            (RightOf17 ?x)
            (LeftOf4 ?x)
          )
          (and
            (RightOf16 ?x)
            (LeftOf9 ?x)
          )
          (and
            (RightOf17 ?x)
            (LeftOf16 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf16 ?x)
          )
        )
        (and
          (Column17 ?x)
        )
      )
    )
  )
  (:action CheckConsistencyAction
    :parameters ( )
    :precondition ( and
      (CheckConsistency)
      (not (Error))
    )
    :effect ( and
      (not (CheckConsistency))
      (when
        (or
          (exists (?x_26 )
            (and
              (LeftOf10 ?x_26)
              (RightOf17 ?x_26)
            )
          )
          (exists (?x_16 )
            (and
              (LeftOf2 ?x_16)
              (RightOf11 ?x_16)
            )
          )
          (exists (?x_33 )
            (and
              (LeftOf1 ?x_33)
              (RightOf16 ?x_33)
            )
          )
          (exists (?x_28 )
            (and
              (RightOf11 ?x_28)
              (LeftOf1 ?x_28)
            )
          )
          (exists (?x_10 )
            (and
              (LeftOf3 ?x_10)
              (RightOf14 ?x_10)
            )
          )
          (exists (?x_1 )
            (and
              (BelowOf4 ?x_1)
              (AboveOf12 ?x_1)
            )
          )
          (exists (?x_25 )
            (and
              (AboveOf14 ?x_25)
              (BelowOf3 ?x_25)
            )
          )
          (exists (?x_11 )
            (and
              (AboveOf14 ?x_11)
              (BelowOf4 ?x_11)
            )
          )
          (exists (?x_21 )
            (and
              (BelowOf6 ?x_21)
              (AboveOf15 ?x_21)
            )
          )
          (exists (?x_2 )
            (and
              (LeftOf1 ?x_2)
              (RightOf5 ?x_2)
            )
          )
          (exists (?x_33 )
            (and
              (RightOf15 ?x_33)
              (LeftOf4 ?x_33)
            )
          )
          (exists (?x_1 )
            (and
              (BelowOf6 ?x_1)
              (AboveOf14 ?x_1)
            )
          )
          (exists (?x_10 )
            (and
              (LeftOf7 ?x_10)
              (RightOf16 ?x_10)
            )
          )
          (exists (?x_30 )
            (and
              (BelowOf4 ?x_30)
              (AboveOf15 ?x_30)
            )
          )
          (exists (?x_12 )
            (and
              (RightOf17 ?x_12)
              (LeftOf3 ?x_12)
            )
          )
          (exists (?x_0 )
            (and
              (RightOf14 ?x_0)
              (LeftOf7 ?x_0)
            )
          )
          (exists (?x_29 )
            (and
              (RightOf15 ?x_29)
              (LeftOf2 ?x_29)
            )
          )
          (exists (?x_24 )
            (and
              (RightOf13 ?x_24)
              (LeftOf1 ?x_24)
            )
          )
          (exists (?x_6 )
            (and
              (BelowOf6 ?x_6)
              (AboveOf12 ?x_6)
            )
          )
          (exists (?x_20 )
            (and
              (BelowOf1 ?x_20)
              (AboveOf13 ?x_20)
            )
          )
          (exists (?x_26 )
            (and
              (LeftOf13 ?x_26)
              (RightOf16 ?x_26)
            )
          )
          (exists (?x_30 )
            (and
              (BelowOf3 ?x_30)
              (AboveOf11 ?x_30)
            )
          )
          (exists (?x_15 )
            (and
              (LeftOf3 ?x_15)
              (RightOf10 ?x_15)
            )
          )
          (exists (?x_32 )
            (and
              (RightOf17 ?x_32)
              (LeftOf4 ?x_32)
            )
          )
          (exists (?x_29 )
            (and
              (LeftOf2 ?x_29)
              (RightOf11 ?x_29)
            )
          )
          (exists (?x_13 )
            (and
              (LeftOf1 ?x_13)
              (RightOf11 ?x_13)
            )
          )
          (exists (?x_7 )
            (and
              (AboveOf16 ?x_7)
              (BelowOf1 ?x_7)
            )
          )
          (exists (?x_3 )
            (and
              (RightOf3 ?x_3)
              (LeftOf2 ?x_3)
            )
          )
          (exists (?x_32 )
            (and
              (LeftOf1 ?x_32)
              (RightOf16 ?x_32)
            )
          )
          (exists (?x_24 )
            (and
              (LeftOf3 ?x_24)
              (RightOf4 ?x_24)
            )
          )
          (exists (?x_16 )
            (and
              (LeftOf2 ?x_16)
              (RightOf16 ?x_16)
            )
          )
          (exists (?x_10 )
            (and
              (LeftOf1 ?x_10)
              (RightOf10 ?x_10)
            )
          )
          (exists (?x_9 )
            (and
              (LeftOf5 ?x_9)
              (RightOf14 ?x_9)
            )
          )
          (exists (?x_22 )
            (and
              (AboveOf11 ?x_22)
              (BelowOf6 ?x_22)
            )
          )
          (exists (?x_29 )
            (and
              (RightOf16 ?x_29)
              (LeftOf3 ?x_29)
            )
          )
          (exists (?x_2 )
            (and
              (LeftOf1 ?x_2)
              (RightOf1 ?x_2)
            )
          )
          (exists (?x_18 )
            (and
              (AboveOf13 ?x_18)
              (BelowOf4 ?x_18)
            )
          )
          (exists (?x_8 )
            (and
              (RightOf17 ?x_8)
              (LeftOf14 ?x_8)
            )
          )
          (exists (?x_1 )
            (and
              (AboveOf11 ?x_1)
              (BelowOf4 ?x_1)
            )
          )
          (exists (?x_27 )
            (and
              (BelowOf9 ?x_27)
              (AboveOf17 ?x_27)
            )
          )
          (exists (?x_33 )
            (and
              (RightOf14 ?x_33)
              (LeftOf3 ?x_33)
            )
          )
          (exists (?x_2 )
            (and
              (LeftOf1 ?x_2)
              (RightOf4 ?x_2)
            )
          )
          (exists (?x_1 )
            (and
              (BelowOf8 ?x_1)
              (AboveOf17 ?x_1)
            )
          )
          (exists (?x_30 )
            (and
              (AboveOf17 ?x_30)
              (BelowOf3 ?x_30)
            )
          )
          (exists (?x_18 )
            (and
              (AboveOf17 ?x_18)
              (BelowOf4 ?x_18)
            )
          )
          (exists (?x_26 )
            (and
              (RightOf16 ?x_26)
              (LeftOf7 ?x_26)
            )
          )
          (exists (?x_21 )
            (and
              (BelowOf3 ?x_21)
              (AboveOf11 ?x_21)
            )
          )
          (exists (?x_27 )
            (and
              (BelowOf14 ?x_27)
              (AboveOf17 ?x_27)
            )
          )
          (exists (?x_27 )
            (and
              (AboveOf15 ?x_27)
              (BelowOf11 ?x_27)
            )
          )
          (exists (?x_29 )
            (and
              (LeftOf6 ?x_29)
              (RightOf14 ?x_29)
            )
          )
          (exists (?x_28 )
            (and
              (LeftOf2 ?x_28)
              (RightOf11 ?x_28)
            )
          )
          (exists (?x_10 )
            (and
              (LeftOf6 ?x_10)
              (RightOf11 ?x_10)
            )
          )
          (exists (?x_8 )
            (and
              (RightOf16 ?x_8)
              (LeftOf10 ?x_8)
            )
          )
          (exists (?x_13 )
            (and
              (LeftOf10 ?x_13)
              (RightOf17 ?x_13)
            )
          )
          (exists (?x_30 )
            (and
              (BelowOf1 ?x_30)
              (AboveOf16 ?x_30)
            )
          )
          (exists (?x_23 )
            (and
              (AboveOf14 ?x_23)
              (BelowOf2 ?x_23)
            )
          )
          (exists (?x_25 )
            (and
              (AboveOf16 ?x_25)
              (BelowOf3 ?x_25)
            )
          )
          (exists (?x_6 )
            (and
              (AboveOf9 ?x_6)
              (BelowOf4 ?x_6)
            )
          )
          (exists (?x_16 )
            (and
              (RightOf13 ?x_16)
              (LeftOf4 ?x_16)
            )
          )
          (exists (?x_25 )
            (and
              (AboveOf16 ?x_25)
              (BelowOf5 ?x_25)
            )
          )
          (exists (?x_0 )
            (and
              (LeftOf13 ?x_0)
              (RightOf16 ?x_0)
            )
          )
          (exists (?x_28 )
            (and
              (RightOf5 ?x_28)
              (LeftOf2 ?x_28)
            )
          )
          (exists (?x_23 )
            (and
              (BelowOf5 ?x_23)
              (AboveOf15 ?x_23)
            )
          )
          (exists (?x_31 )
            (and
              (BelowOf4 ?x_31)
              (AboveOf13 ?x_31)
            )
          )
          (exists (?x_18 )
            (and
              (BelowOf3 ?x_18)
              (AboveOf17 ?x_18)
            )
          )
          (exists (?x_21 )
            (and
              (BelowOf2 ?x_21)
              (AboveOf10 ?x_21)
            )
          )
          (exists (?x_5 )
            (and
              (BelowOf1 ?x_5)
              (AboveOf16 ?x_5)
            )
          )
          (exists (?x_9 )
            (and
              (LeftOf8 ?x_9)
              (RightOf11 ?x_9)
            )
          )
          (exists (?x_18 )
            (and
              (BelowOf5 ?x_18)
              (AboveOf15 ?x_18)
            )
          )
          (exists (?x_29 )
            (and
              (RightOf13 ?x_29)
              (LeftOf8 ?x_29)
            )
          )
          (exists (?x_29 )
            (and
              (LeftOf8 ?x_29)
              (RightOf11 ?x_29)
            )
          )
          (exists (?x_9 )
            (and
              (LeftOf7 ?x_9)
              (RightOf12 ?x_9)
            )
          )
          (exists (?x_6 )
            (and
              (BelowOf1 ?x_6)
              (AboveOf14 ?x_6)
            )
          )
          (exists (?x_31 )
            (and
              (BelowOf4 ?x_31)
              (AboveOf7 ?x_31)
            )
          )
          (exists (?x_18 )
            (and
              (AboveOf17 ?x_18)
              (BelowOf12 ?x_18)
            )
          )
          (exists (?x_28 )
            (and
              (LeftOf2 ?x_28)
              (RightOf9 ?x_28)
            )
          )
          (exists (?x_22 )
            (and
              (AboveOf17 ?x_22)
              (BelowOf2 ?x_22)
            )
          )
          (exists (?x_1 )
            (and
              (AboveOf13 ?x_1)
              (BelowOf5 ?x_1)
            )
          )
          (exists (?x_13 )
            (and
              (RightOf14 ?x_13)
              (LeftOf8 ?x_13)
            )
          )
          (exists (?x_8 )
            (and
              (RightOf17 ?x_8)
              (LeftOf13 ?x_8)
            )
          )
          (exists (?x_7 )
            (and
              (BelowOf11 ?x_7)
              (AboveOf17 ?x_7)
            )
          )
          (exists (?x_26 )
            (and
              (LeftOf8 ?x_26)
              (RightOf17 ?x_26)
            )
          )
          (exists (?x_21 )
            (and
              (BelowOf6 ?x_21)
              (AboveOf16 ?x_21)
            )
          )
          (exists (?x_3 )
            (and
              (RightOf8 ?x_3)
              (LeftOf1 ?x_3)
            )
          )
          (exists (?x_0 )
            (and
              (RightOf13 ?x_0)
              (LeftOf8 ?x_0)
            )
          )
          (exists (?x_26 )
            (and
              (LeftOf5 ?x_26)
              (RightOf16 ?x_26)
            )
          )
          (exists (?x_23 )
            (and
              (AboveOf14 ?x_23)
              (BelowOf12 ?x_23)
            )
          )
          (exists (?x_32 )
            (and
              (LeftOf9 ?x_32)
              (RightOf16 ?x_32)
            )
          )
          (exists (?x_31 )
            (and
              (BelowOf1 ?x_31)
              (AboveOf17 ?x_31)
            )
          )
          (exists (?x_5 )
            (and
              (AboveOf13 ?x_5)
              (BelowOf1 ?x_5)
            )
          )
          (exists (?x_24 )
            (and
              (RightOf9 ?x_24)
              (LeftOf1 ?x_24)
            )
          )
          (exists (?x_30 )
            (and
              (AboveOf13 ?x_30)
              (BelowOf2 ?x_30)
            )
          )
          (exists (?x_21 )
            (and
              (BelowOf8 ?x_21)
              (AboveOf13 ?x_21)
            )
          )
          (exists (?x_0 )
            (and
              (RightOf14 ?x_0)
              (LeftOf4 ?x_0)
            )
          )
          (exists (?x_28 )
            (and
              (RightOf5 ?x_28)
              (LeftOf1 ?x_28)
            )
          )
          (exists (?x_28 )
            (and
              (RightOf9 ?x_28)
              (LeftOf1 ?x_28)
            )
          )
          (exists (?x_10 )
            (and
              (RightOf13 ?x_10)
              (LeftOf4 ?x_10)
            )
          )
          (exists (?x_9 )
            (and
              (LeftOf7 ?x_9)
              (RightOf15 ?x_9)
            )
          )
          (exists (?x_23 )
            (and
              (BelowOf4 ?x_23)
              (AboveOf16 ?x_23)
            )
          )
          (exists (?x_1 )
            (and
              (BelowOf2 ?x_1)
              (AboveOf12 ?x_1)
            )
          )
          (exists (?x_11 )
            (and
              (BelowOf3 ?x_11)
              (AboveOf16 ?x_11)
            )
          )
          (exists (?x_15 )
            (and
              (LeftOf3 ?x_15)
              (RightOf9 ?x_15)
            )
          )
          (exists (?x_29 )
            (and
              (LeftOf7 ?x_29)
              (RightOf11 ?x_29)
            )
          )
          (exists (?x_9 )
            (and
              (RightOf13 ?x_9)
              (LeftOf6 ?x_9)
            )
          )
          (exists (?x_32 )
            (and
              (LeftOf5 ?x_32)
              (RightOf16 ?x_32)
            )
          )
          (exists (?x_1 )
            (and
              (BelowOf9 ?x_1)
              (AboveOf12 ?x_1)
            )
          )
          (exists (?x_27 )
            (and
              (AboveOf15 ?x_27)
              (BelowOf8 ?x_27)
            )
          )
          (exists (?x_14 )
            (and
              (BelowOf6 ?x_14)
              (AboveOf10 ?x_14)
            )
          )
          (exists (?x_6 )
            (and
              (AboveOf7 ?x_6)
              (BelowOf5 ?x_6)
            )
          )
          (exists (?x_24 )
            (and
              (RightOf11 ?x_24)
              (LeftOf2 ?x_24)
            )
          )
          (exists (?x_18 )
            (and
              (AboveOf16 ?x_18)
              (BelowOf9 ?x_18)
            )
          )
          (exists (?x_3 )
            (and
              (RightOf2 ?x_3)
              (LeftOf2 ?x_3)
            )
          )
          (exists (?x_11 )
            (and
              (BelowOf11 ?x_11)
              (AboveOf13 ?x_11)
            )
          )
          (exists (?x_13 )
            (and
              (RightOf10 ?x_13)
              (LeftOf5 ?x_13)
            )
          )
          (exists (?x_13 )
            (and
              (RightOf10 ?x_13)
              (LeftOf6 ?x_13)
            )
          )
          (exists (?x_27 )
            (and
              (AboveOf16 ?x_27)
              (BelowOf13 ?x_27)
            )
          )
          (exists (?x_11 )
            (and
              (BelowOf1 ?x_11)
              (AboveOf16 ?x_11)
            )
          )
          (exists (?x_15 )
            (and
              (LeftOf1 ?x_15)
              (RightOf16 ?x_15)
            )
          )
          (exists (?x_25 )
            (and
              (AboveOf8 ?x_25)
              (BelowOf3 ?x_25)
            )
          )
          (exists (?x_26 )
            (and
              (RightOf15 ?x_26)
              (LeftOf1 ?x_26)
            )
          )
          (exists (?x_16 )
            (and
              (RightOf8 ?x_16)
              (LeftOf4 ?x_16)
            )
          )
          (exists (?x_13 )
            (and
              (LeftOf9 ?x_13)
              (RightOf12 ?x_13)
            )
          )
          (exists (?x_10 )
            (and
              (LeftOf1 ?x_10)
              (RightOf17 ?x_10)
            )
          )
          (exists (?x_22 )
            (and
              (BelowOf3 ?x_22)
              (AboveOf12 ?x_22)
            )
          )
          (exists (?x_11 )
            (and
              (BelowOf5 ?x_11)
              (AboveOf14 ?x_11)
            )
          )
          (exists (?x_10 )
            (and
              (LeftOf7 ?x_10)
              (RightOf15 ?x_10)
            )
          )
          (exists (?x_13 )
            (and
              (RightOf15 ?x_13)
              (LeftOf3 ?x_13)
            )
          )
          (exists (?x_18 )
            (and
              (BelowOf7 ?x_18)
              (AboveOf16 ?x_18)
            )
          )
          (exists (?x_0 )
            (and
              (RightOf17 ?x_0)
              (LeftOf2 ?x_0)
            )
          )
          (exists (?x_29 )
            (and
              (RightOf14 ?x_29)
              (LeftOf4 ?x_29)
            )
          )
          (exists (?x_31 )
            (and
              (AboveOf7 ?x_31)
              (BelowOf6 ?x_31)
            )
          )
          (exists (?x_33 )
            (and
              (RightOf14 ?x_33)
              (LeftOf1 ?x_33)
            )
          )
          (exists (?x_31 )
            (and
              (BelowOf4 ?x_31)
              (AboveOf15 ?x_31)
            )
          )
          (exists (?x_32 )
            (and
              (RightOf12 ?x_32)
              (LeftOf6 ?x_32)
            )
          )
          (exists (?x_27 )
            (and
              (AboveOf15 ?x_27)
              (BelowOf7 ?x_27)
            )
          )
          (exists (?x_31 )
            (and
              (BelowOf6 ?x_31)
              (AboveOf11 ?x_31)
            )
          )
          (exists (?x_24 )
            (and
              (RightOf16 ?x_24)
              (LeftOf2 ?x_24)
            )
          )
          (exists (?x_16 )
            (and
              (LeftOf4 ?x_16)
              (RightOf8 ?x_16)
            )
          )
          (exists (?x_31 )
            (and
              (BelowOf7 ?x_31)
              (AboveOf17 ?x_31)
            )
          )
          (exists (?x_17 )
            (and
              (AboveOf14 ?x_17)
              (BelowOf1 ?x_17)
            )
          )
          (exists (?x_7 )
            (and
              (AboveOf17 ?x_7)
              (BelowOf5 ?x_7)
            )
          )
          (exists (?x_21 )
            (and
              (BelowOf3 ?x_21)
              (AboveOf16 ?x_21)
            )
          )
          (exists (?x_5 )
            (and
              (AboveOf17 ?x_5)
              (BelowOf1 ?x_5)
            )
          )
          (exists (?x_1 )
            (and
              (BelowOf7 ?x_1)
              (AboveOf15 ?x_1)
            )
          )
          (exists (?x_21 )
            (and
              (AboveOf11 ?x_21)
              (BelowOf1 ?x_21)
            )
          )
          (exists (?x_25 )
            (and
              (AboveOf13 ?x_25)
              (BelowOf1 ?x_25)
            )
          )
          (exists (?x_8 )
            (and
              (RightOf17 ?x_8)
              (LeftOf10 ?x_8)
            )
          )
          (exists (?x_18 )
            (and
              (BelowOf5 ?x_18)
              (AboveOf16 ?x_18)
            )
          )
          (exists (?x_24 )
            (and
              (LeftOf3 ?x_24)
              (RightOf5 ?x_24)
            )
          )
          (exists (?x_1 )
            (and
              (AboveOf13 ?x_1)
              (BelowOf8 ?x_1)
            )
          )
          (exists (?x_11 )
            (and
              (BelowOf11 ?x_11)
              (AboveOf16 ?x_11)
            )
          )
          (exists (?x_13 )
            (and
              (LeftOf3 ?x_13)
              (RightOf17 ?x_13)
            )
          )
          (exists (?x_31 )
            (and
              (AboveOf13 ?x_31)
              (BelowOf5 ?x_31)
            )
          )
          (exists (?x_12 )
            (and
              (RightOf16 ?x_12)
              (LeftOf6 ?x_12)
            )
          )
          (exists (?x_13 )
            (and
              (LeftOf9 ?x_13)
              (RightOf13 ?x_13)
            )
          )
          (exists (?x_0 )
            (and
              (RightOf14 ?x_0)
              (LeftOf8 ?x_0)
            )
          )
          (exists (?x_10 )
            (and
              (RightOf13 ?x_10)
              (LeftOf2 ?x_10)
            )
          )
          (exists (?x_31 )
            (and
              (AboveOf17 ?x_31)
              (BelowOf3 ?x_31)
            )
          )
          (exists (?x_0 )
            (and
              (LeftOf6 ?x_0)
              (RightOf15 ?x_0)
            )
          )
          (exists (?x_4 )
            (and
              (BelowOf8 ?x_4)
              (AboveOf17 ?x_4)
            )
          )
          (exists (?x_10 )
            (and
              (LeftOf2 ?x_10)
              (RightOf17 ?x_10)
            )
          )
          (exists (?x_13 )
            (and
              (RightOf15 ?x_13)
              (LeftOf9 ?x_13)
            )
          )
          (exists (?x_20 )
            (and
              (AboveOf13 ?x_20)
              (BelowOf2 ?x_20)
            )
          )
          (exists (?x_14 )
            (and
              (AboveOf17 ?x_14)
              (BelowOf4 ?x_14)
            )
          )
          (exists (?x_11 )
            (and
              (BelowOf13 ?x_11)
              (AboveOf16 ?x_11)
            )
          )
          (exists (?x_10 )
            (and
              (RightOf8 ?x_10)
              (LeftOf6 ?x_10)
            )
          )
          (exists (?x_18 )
            (and
              (BelowOf7 ?x_18)
              (AboveOf17 ?x_18)
            )
          )
          (exists (?x_8 )
            (and
              (RightOf17 ?x_8)
              (LeftOf9 ?x_8)
            )
          )
          (exists (?x_31 )
            (and
              (BelowOf4 ?x_31)
              (AboveOf16 ?x_31)
            )
          )
          (exists (?x_21 )
            (and
              (BelowOf1 ?x_21)
              (AboveOf10 ?x_21)
            )
          )
          (exists (?x_32 )
            (and
              (LeftOf2 ?x_32)
              (RightOf16 ?x_32)
            )
          )
          (exists (?x_24 )
            (and
              (RightOf12 ?x_24)
              (LeftOf3 ?x_24)
            )
          )
          (exists (?x_9 )
            (and
              (RightOf17 ?x_9)
              (LeftOf2 ?x_9)
            )
          )
          (exists (?x_14 )
            (and
              (AboveOf15 ?x_14)
              (BelowOf5 ?x_14)
            )
          )
          (exists (?x_14 )
            (and
              (BelowOf3 ?x_14)
              (AboveOf9 ?x_14)
            )
          )
          (exists (?x_18 )
            (and
              (BelowOf9 ?x_18)
              (AboveOf17 ?x_18)
            )
          )
          (exists (?x_15 )
            (and
              (LeftOf9 ?x_15)
              (RightOf9 ?x_15)
            )
          )
          (exists (?x_16 )
            (and
              (RightOf11 ?x_16)
              (LeftOf1 ?x_16)
            )
          )
          (exists (?x_18 )
            (and
              (BelowOf12 ?x_18)
              (AboveOf12 ?x_18)
            )
          )
          (exists (?x_16 )
            (and
              (LeftOf6 ?x_16)
              (RightOf13 ?x_16)
            )
          )
          (exists (?x_13 )
            (and
              (RightOf11 ?x_13)
              (LeftOf3 ?x_13)
            )
          )
          (exists (?x_32 )
            (and
              (LeftOf12 ?x_32)
              (RightOf13 ?x_32)
            )
          )
          (exists (?x_0 )
            (and
              (LeftOf4 ?x_0)
              (RightOf17 ?x_0)
            )
          )
          (exists (?x_27 )
            (and
              (BelowOf15 ?x_27)
              (AboveOf17 ?x_27)
            )
          )
          (exists (?x_26 )
            (and
              (LeftOf5 ?x_26)
              (RightOf17 ?x_26)
            )
          )
          (exists (?x_28 )
            (and
              (RightOf15 ?x_28)
              (LeftOf4 ?x_28)
            )
          )
          (exists (?x_16 )
            (and
              (LeftOf6 ?x_16)
              (RightOf7 ?x_16)
            )
          )
          (exists (?x_32 )
            (and
              (LeftOf7 ?x_32)
              (RightOf13 ?x_32)
            )
          )
          (exists (?x_15 )
            (and
              (RightOf16 ?x_15)
              (LeftOf5 ?x_15)
            )
          )
          (exists (?x_6 )
            (and
              (BelowOf5 ?x_6)
              (AboveOf12 ?x_6)
            )
          )
          (exists (?x_24 )
            (and
              (LeftOf3 ?x_24)
              (RightOf16 ?x_24)
            )
          )
          (exists (?x_7 )
            (and
              (AboveOf16 ?x_7)
              (BelowOf11 ?x_7)
            )
          )
          (exists (?x_29 )
            (and
              (LeftOf11 ?x_29)
              (RightOf13 ?x_29)
            )
          )
          (exists (?x_16 )
            (and
              (RightOf14 ?x_16)
              (LeftOf1 ?x_16)
            )
          )
          (exists (?x_15 )
            (and
              (RightOf10 ?x_15)
              (LeftOf8 ?x_15)
            )
          )
          (exists (?x_21 )
            (and
              (AboveOf12 ?x_21)
              (BelowOf3 ?x_21)
            )
          )
          (exists (?x_22 )
            (and
              (AboveOf17 ?x_22)
              (BelowOf10 ?x_22)
            )
          )
          (exists (?x_6 )
            (and
              (AboveOf9 ?x_6)
              (BelowOf2 ?x_6)
            )
          )
          (exists (?x_23 )
            (and
              (BelowOf11 ?x_23)
              (AboveOf15 ?x_23)
            )
          )
          (exists (?x_30 )
            (and
              (BelowOf4 ?x_30)
              (AboveOf10 ?x_30)
            )
          )
          (exists (?x_5 )
            (and
              (BelowOf1 ?x_5)
              (AboveOf13 ?x_5)
            )
          )
          (exists (?x_5 )
            (and
              (AboveOf17 ?x_5)
              (BelowOf3 ?x_5)
            )
          )
          (exists (?x_7 )
            (and
              (AboveOf16 ?x_7)
              (BelowOf8 ?x_7)
            )
          )
          (exists (?x_26 )
            (and
              (RightOf17 ?x_26)
              (LeftOf13 ?x_26)
            )
          )
          (exists (?x_32 )
            (and
              (LeftOf4 ?x_32)
              (RightOf15 ?x_32)
            )
          )
          (exists (?x_22 )
            (and
              (BelowOf5 ?x_22)
              (AboveOf13 ?x_22)
            )
          )
          (exists (?x_22 )
            (and
              (BelowOf5 ?x_22)
              (AboveOf12 ?x_22)
            )
          )
          (exists (?x_27 )
            (and
              (AboveOf16 ?x_27)
              (BelowOf4 ?x_27)
            )
          )
          (exists (?x_32 )
            (and
              (LeftOf8 ?x_32)
              (RightOf13 ?x_32)
            )
          )
          (exists (?x_19 )
            (and
              (RightOf17 ?x_19)
              (LeftOf14 ?x_19)
            )
          )
          (exists (?x_0 )
            (and
              (RightOf13 ?x_0)
              (LeftOf12 ?x_0)
            )
          )
          (exists (?x_21 )
            (and
              (BelowOf5 ?x_21)
              (AboveOf10 ?x_21)
            )
          )
          (exists (?x_30 )
            (and
              (AboveOf8 ?x_30)
              (BelowOf3 ?x_30)
            )
          )
          (exists (?x_24 )
            (and
              (RightOf11 ?x_24)
              (LeftOf1 ?x_24)
            )
          )
          (exists (?x_5 )
            (and
              (AboveOf4 ?x_5)
              (BelowOf2 ?x_5)
            )
          )
          (exists (?x_10 )
            (and
              (RightOf17 ?x_10)
              (LeftOf3 ?x_10)
            )
          )
          (exists (?x_0 )
            (and
              (LeftOf1 ?x_0)
              (RightOf17 ?x_0)
            )
          )
          (exists (?x_13 )
            (and
              (RightOf15 ?x_13)
              (LeftOf6 ?x_13)
            )
          )
          (exists (?x_1 )
            (and
              (BelowOf8 ?x_1)
              (AboveOf12 ?x_1)
            )
          )
          (exists (?x_14 )
            (and
              (AboveOf10 ?x_14)
              (BelowOf5 ?x_14)
            )
          )
          (exists (?x_27 )
            (and
              (AboveOf15 ?x_27)
              (BelowOf13 ?x_27)
            )
          )
          (exists (?x_3 )
            (and
              (RightOf15 ?x_3)
              (LeftOf1 ?x_3)
            )
          )
          (exists (?x_31 )
            (and
              (BelowOf5 ?x_31)
              (AboveOf14 ?x_31)
            )
          )
          (exists (?x_33 )
            (and
              (RightOf7 ?x_33)
              (LeftOf4 ?x_33)
            )
          )
          (exists (?x_31 )
            (and
              (AboveOf15 ?x_31)
              (BelowOf3 ?x_31)
            )
          )
          (exists (?x_32 )
            (and
              (RightOf12 ?x_32)
              (LeftOf9 ?x_32)
            )
          )
          (exists (?x_29 )
            (and
              (LeftOf7 ?x_29)
              (RightOf12 ?x_29)
            )
          )
          (exists (?x_33 )
            (and
              (LeftOf3 ?x_33)
              (RightOf13 ?x_33)
            )
          )
          (exists (?x_24 )
            (and
              (LeftOf2 ?x_24)
              (RightOf13 ?x_24)
            )
          )
          (exists (?x_11 )
            (and
              (BelowOf7 ?x_11)
              (AboveOf15 ?x_11)
            )
          )
          (exists (?x_14 )
            (and
              (BelowOf7 ?x_14)
              (AboveOf14 ?x_14)
            )
          )
          (exists (?x_32 )
            (and
              (LeftOf1 ?x_32)
              (RightOf13 ?x_32)
            )
          )
          (exists (?x_18 )
            (and
              (AboveOf14 ?x_18)
              (BelowOf3 ?x_18)
            )
          )
          (exists (?x_15 )
            (and
              (LeftOf6 ?x_15)
              (RightOf12 ?x_15)
            )
          )
          (exists (?x_32 )
            (and
              (RightOf14 ?x_32)
              (LeftOf3 ?x_32)
            )
          )
          (exists (?x_12 )
            (and
              (LeftOf13 ?x_12)
              (RightOf17 ?x_12)
            )
          )
          (exists (?x_29 )
            (and
              (LeftOf5 ?x_29)
              (RightOf11 ?x_29)
            )
          )
          (exists (?x_28 )
            (and
              (LeftOf1 ?x_28)
              (RightOf10 ?x_28)
            )
          )
          (exists (?x_26 )
            (and
              (LeftOf11 ?x_26)
              (RightOf16 ?x_26)
            )
          )
          (exists (?x_18 )
            (and
              (BelowOf2 ?x_18)
              (AboveOf14 ?x_18)
            )
          )
          (exists (?x_31 )
            (and
              (BelowOf7 ?x_31)
              (AboveOf14 ?x_31)
            )
          )
          (exists (?x_22 )
            (and
              (BelowOf8 ?x_22)
              (AboveOf13 ?x_22)
            )
          )
          (exists (?x_12 )
            (and
              (LeftOf11 ?x_12)
              (RightOf17 ?x_12)
            )
          )
          (exists (?x_32 )
            (and
              (RightOf15 ?x_32)
              (LeftOf1 ?x_32)
            )
          )
          (exists (?x_9 )
            (and
              (LeftOf3 ?x_9)
              (RightOf9 ?x_9)
            )
          )
          (exists (?x_25 )
            (and
              (BelowOf4 ?x_25)
              (AboveOf9 ?x_25)
            )
          )
          (exists (?x_23 )
            (and
              (AboveOf15 ?x_23)
              (BelowOf9 ?x_23)
            )
          )
          (exists (?x_21 )
            (and
              (AboveOf14 ?x_21)
              (BelowOf4 ?x_21)
            )
          )
          (exists (?x_3 )
            (and
              (RightOf6 ?x_3)
              (LeftOf2 ?x_3)
            )
          )
          (exists (?x_25 )
            (and
              (BelowOf4 ?x_25)
              (AboveOf8 ?x_25)
            )
          )
          (exists (?x_31 )
            (and
              (AboveOf12 ?x_31)
              (BelowOf5 ?x_31)
            )
          )
          (exists (?x_23 )
            (and
              (AboveOf17 ?x_23)
              (BelowOf7 ?x_23)
            )
          )
          (exists (?x_32 )
            (and
              (LeftOf6 ?x_32)
              (RightOf17 ?x_32)
            )
          )
          (exists (?x_31 )
            (and
              (AboveOf16 ?x_31)
              (BelowOf1 ?x_31)
            )
          )
          (exists (?x_7 )
            (and
              (AboveOf16 ?x_7)
              (BelowOf14 ?x_7)
            )
          )
          (exists (?x_7 )
            (and
              (AboveOf16 ?x_7)
              (BelowOf10 ?x_7)
            )
          )
          (exists (?x_13 )
            (and
              (LeftOf4 ?x_13)
              (RightOf17 ?x_13)
            )
          )
          (exists (?x_10 )
            (and
              (RightOf13 ?x_10)
              (LeftOf5 ?x_10)
            )
          )
          (exists (?x_8 )
            (and
              (RightOf16 ?x_8)
              (LeftOf6 ?x_8)
            )
          )
          (exists (?x_18 )
            (and
              (AboveOf14 ?x_18)
              (BelowOf9 ?x_18)
            )
          )
          (exists (?x_19 )
            (and
              (RightOf17 ?x_19)
              (LeftOf4 ?x_19)
            )
          )
          (exists (?x_10 )
            (and
              (LeftOf5 ?x_10)
              (RightOf14 ?x_10)
            )
          )
          (exists (?x_5 )
            (and
              (BelowOf2 ?x_5)
              (AboveOf16 ?x_5)
            )
          )
          (exists (?x_21 )
            (and
              (BelowOf4 ?x_21)
              (AboveOf10 ?x_21)
            )
          )
          (exists (?x_22 )
            (and
              (BelowOf5 ?x_22)
              (AboveOf15 ?x_22)
            )
          )
          (exists (?x_27 )
            (and
              (AboveOf15 ?x_27)
              (BelowOf9 ?x_27)
            )
          )
          (exists (?x_22 )
            (and
              (AboveOf11 ?x_22)
              (BelowOf8 ?x_22)
            )
          )
          (exists (?x_33 )
            (and
              (RightOf10 ?x_33)
              (LeftOf4 ?x_33)
            )
          )
          (exists (?x_29 )
            (and
              (LeftOf4 ?x_29)
              (RightOf13 ?x_29)
            )
          )
          (exists (?x_23 )
            (and
              (BelowOf8 ?x_23)
              (AboveOf16 ?x_23)
            )
          )
          (exists (?x_6 )
            (and
              (AboveOf10 ?x_6)
              (BelowOf5 ?x_6)
            )
          )
          (exists (?x_15 )
            (and
              (LeftOf1 ?x_15)
              (RightOf15 ?x_15)
            )
          )
          (exists (?x_29 )
            (and
              (LeftOf6 ?x_29)
              (RightOf11 ?x_29)
            )
          )
          (exists (?x_25 )
            (and
              (AboveOf9 ?x_25)
              (BelowOf1 ?x_25)
            )
          )
          (exists (?x_12 )
            (and
              (RightOf15 ?x_12)
              (LeftOf4 ?x_12)
            )
          )
          (exists (?x_20 )
            (and
              (AboveOf16 ?x_20)
              (BelowOf2 ?x_20)
            )
          )
          (exists (?x_0 )
            (and
              (RightOf16 ?x_0)
              (LeftOf10 ?x_0)
            )
          )
          (exists (?x_33 )
            (and
              (LeftOf3 ?x_33)
              (RightOf17 ?x_33)
            )
          )
          (exists (?x_11 )
            (and
              (AboveOf17 ?x_11)
              (BelowOf2 ?x_11)
            )
          )
          (exists (?x_20 )
            (and
              (AboveOf14 ?x_20)
              (BelowOf1 ?x_20)
            )
          )
          (exists (?x_18 )
            (and
              (AboveOf13 ?x_18)
              (BelowOf1 ?x_18)
            )
          )
          (exists (?x_20 )
            (and
              (BelowOf1 ?x_20)
              (AboveOf7 ?x_20)
            )
          )
          (exists (?x_9 )
            (and
              (RightOf10 ?x_9)
              (LeftOf7 ?x_9)
            )
          )
          (exists (?x_3 )
            (and
              (LeftOf1 ?x_3)
              (RightOf5 ?x_3)
            )
          )
          (exists (?x_8 )
            (and
              (LeftOf5 ?x_8)
              (RightOf16 ?x_8)
            )
          )
          (exists (?x_14 )
            (and
              (AboveOf15 ?x_14)
              (BelowOf4 ?x_14)
            )
          )
          (exists (?x_25 )
            (and
              (BelowOf4 ?x_25)
              (AboveOf6 ?x_25)
            )
          )
          (exists (?x_5 )
            (and
              (AboveOf14 ?x_5)
              (BelowOf2 ?x_5)
            )
          )
          (exists (?x_0 )
            (and
              (RightOf13 ?x_0)
              (LeftOf10 ?x_0)
            )
          )
          (exists (?x_22 )
            (and
              (AboveOf11 ?x_22)
              (BelowOf11 ?x_22)
            )
          )
          (exists (?x_9 )
            (and
              (LeftOf4 ?x_9)
              (RightOf15 ?x_9)
            )
          )
          (exists (?x_32 )
            (and
              (RightOf12 ?x_32)
              (LeftOf11 ?x_32)
            )
          )
          (exists (?x_14 )
            (and
              (AboveOf9 ?x_14)
              (BelowOf9 ?x_14)
            )
          )
          (exists (?x_11 )
            (and
              (AboveOf17 ?x_11)
              (BelowOf5 ?x_11)
            )
          )
          (exists (?x_33 )
            (and
              (LeftOf1 ?x_33)
              (RightOf13 ?x_33)
            )
          )
          (exists (?x_16 )
            (and
              (RightOf7 ?x_16)
              (LeftOf4 ?x_16)
            )
          )
          (exists (?x_29 )
            (and
              (RightOf14 ?x_29)
              (LeftOf10 ?x_29)
            )
          )
          (exists (?x_10 )
            (and
              (LeftOf2 ?x_10)
              (RightOf14 ?x_10)
            )
          )
          (exists (?x_21 )
            (and
              (BelowOf4 ?x_21)
              (AboveOf15 ?x_21)
            )
          )
          (exists (?x_25 )
            (and
              (BelowOf5 ?x_25)
              (AboveOf14 ?x_25)
            )
          )
          (exists (?x_16 )
            (and
              (RightOf14 ?x_16)
              (LeftOf4 ?x_16)
            )
          )
          (exists (?x_24 )
            (and
              (LeftOf1 ?x_24)
              (RightOf5 ?x_24)
            )
          )
          (exists (?x_18 )
            (and
              (BelowOf11 ?x_18)
              (AboveOf14 ?x_18)
            )
          )
          (exists (?x_21 )
            (and
              (AboveOf13 ?x_21)
              (BelowOf7 ?x_21)
            )
          )
          (exists (?x_0 )
            (and
              (RightOf16 ?x_0)
              (LeftOf9 ?x_0)
            )
          )
          (exists (?x_19 )
            (and
              (RightOf17 ?x_19)
              (LeftOf3 ?x_19)
            )
          )
          (exists (?x_12 )
            (and
              (RightOf15 ?x_12)
              (LeftOf3 ?x_12)
            )
          )
          (exists (?x_12 )
            (and
              (LeftOf14 ?x_12)
              (RightOf17 ?x_12)
            )
          )
          (exists (?x_16 )
            (and
              (RightOf8 ?x_16)
              (LeftOf1 ?x_16)
            )
          )
          (exists (?x_18 )
            (and
              (AboveOf13 ?x_18)
              (BelowOf6 ?x_18)
            )
          )
          (exists (?x_18 )
            (and
              (AboveOf15 ?x_18)
              (BelowOf1 ?x_18)
            )
          )
          (exists (?x_31 )
            (and
              (AboveOf15 ?x_31)
              (BelowOf5 ?x_31)
            )
          )
          (exists (?x_14 )
            (and
              (BelowOf3 ?x_14)
              (AboveOf14 ?x_14)
            )
          )
          (exists (?x_14 )
            (and
              (BelowOf3 ?x_14)
              (AboveOf13 ?x_14)
            )
          )
          (exists (?x_13 )
            (and
              (LeftOf2 ?x_13)
              (RightOf14 ?x_13)
            )
          )
          (exists (?x_18 )
            (and
              (BelowOf1 ?x_18)
              (AboveOf12 ?x_18)
            )
          )
          (exists (?x_32 )
            (and
              (LeftOf10 ?x_32)
              (RightOf12 ?x_32)
            )
          )
          (exists (?x_5 )
            (and
              (BelowOf3 ?x_5)
              (AboveOf15 ?x_5)
            )
          )
          (exists (?x_22 )
            (and
              (BelowOf11 ?x_22)
              (AboveOf17 ?x_22)
            )
          )
          (exists (?x_10 )
            (and
              (LeftOf1 ?x_10)
              (RightOf12 ?x_10)
            )
          )
          (exists (?x_23 )
            (and
              (AboveOf17 ?x_23)
              (BelowOf1 ?x_23)
            )
          )
          (exists (?x_14 )
            (and
              (AboveOf15 ?x_14)
              (BelowOf8 ?x_14)
            )
          )
          (exists (?x_30 )
            (and
              (AboveOf9 ?x_30)
              (BelowOf4 ?x_30)
            )
          )
          (exists (?x_11 )
            (and
              (AboveOf14 ?x_11)
              (BelowOf7 ?x_11)
            )
          )
          (exists (?x_25 )
            (and
              (AboveOf9 ?x_25)
              (BelowOf3 ?x_25)
            )
          )
          (exists (?x_30 )
            (and
              (AboveOf12 ?x_30)
              (BelowOf3 ?x_30)
            )
          )
          (exists (?x_11 )
            (and
              (AboveOf14 ?x_11)
              (BelowOf1 ?x_11)
            )
          )
          (exists (?x_1 )
            (and
              (AboveOf10 ?x_1)
              (BelowOf2 ?x_1)
            )
          )
          (exists (?x_27 )
            (and
              (BelowOf13 ?x_27)
              (AboveOf17 ?x_27)
            )
          )
          (exists (?x_13 )
            (and
              (RightOf17 ?x_13)
              (LeftOf3 ?x_13)
            )
          )
          (exists (?x_10 )
            (and
              (RightOf17 ?x_10)
              (LeftOf5 ?x_10)
            )
          )
          (exists (?x_31 )
            (and
              (BelowOf1 ?x_31)
              (AboveOf13 ?x_31)
            )
          )
          (exists (?x_32 )
            (and
              (LeftOf3 ?x_32)
              (RightOf13 ?x_32)
            )
          )
          (exists (?x_21 )
            (and
              (BelowOf3 ?x_21)
              (AboveOf10 ?x_21)
            )
          )
          (exists (?x_33 )
            (and
              (RightOf14 ?x_33)
              (LeftOf2 ?x_33)
            )
          )
          (exists (?x_17 )
            (and
              (AboveOf12 ?x_17)
              (BelowOf1 ?x_17)
            )
          )
          (exists (?x_9 )
            (and
              (RightOf11 ?x_9)
              (LeftOf1 ?x_9)
            )
          )
          (exists (?x_7 )
            (and
              (AboveOf16 ?x_7)
              (BelowOf15 ?x_7)
            )
          )
          (exists (?x_1 )
            (and
              (AboveOf16 ?x_1)
              (BelowOf6 ?x_1)
            )
          )
          (exists (?x_24 )
            (and
              (LeftOf2 ?x_24)
              (RightOf15 ?x_24)
            )
          )
          (exists (?x_26 )
            (and
              (RightOf16 ?x_26)
              (LeftOf2 ?x_26)
            )
          )
          (exists (?x_4 )
            (and
              (BelowOf16 ?x_4)
              (AboveOf17 ?x_4)
            )
          )
          (exists (?x_23 )
            (and
              (AboveOf14 ?x_23)
              (BelowOf9 ?x_23)
            )
          )
          (exists (?x_0 )
            (and
              (RightOf16 ?x_0)
              (LeftOf3 ?x_0)
            )
          )
          (exists (?x_31 )
            (and
              (BelowOf2 ?x_31)
              (AboveOf8 ?x_31)
            )
          )
          (exists (?x_9 )
            (and
              (RightOf10 ?x_9)
              (LeftOf8 ?x_9)
            )
          )
          (exists (?x_8 )
            (and
              (LeftOf2 ?x_8)
              (RightOf17 ?x_8)
            )
          )
          (exists (?x_30 )
            (and
              (BelowOf3 ?x_30)
              (AboveOf14 ?x_30)
            )
          )
          (exists (?x_15 )
            (and
              (LeftOf9 ?x_15)
              (RightOf10 ?x_15)
            )
          )
          (exists (?x_22 )
            (and
              (AboveOf16 ?x_22)
              (BelowOf4 ?x_22)
            )
          )
          (exists (?x_3 )
            (and
              (RightOf11 ?x_3)
              (LeftOf2 ?x_3)
            )
          )
          (exists (?x_20 )
            (and
              (AboveOf6 ?x_20)
              (BelowOf1 ?x_20)
            )
          )
          (exists (?x_13 )
            (and
              (LeftOf2 ?x_13)
              (RightOf11 ?x_13)
            )
          )
          (exists (?x_0 )
            (and
              (LeftOf11 ?x_0)
              (RightOf17 ?x_0)
            )
          )
          (exists (?x_24 )
            (and
              (RightOf5 ?x_24)
              (LeftOf1 ?x_24)
            )
          )
          (exists (?x_26 )
            (and
              (RightOf17 ?x_26)
              (LeftOf9 ?x_26)
            )
          )
          (exists (?x_14 )
            (and
              (AboveOf9 ?x_14)
              (BelowOf8 ?x_14)
            )
          )
          (exists (?x_21 )
            (and
              (AboveOf12 ?x_21)
              (BelowOf4 ?x_21)
            )
          )
          (exists (?x_22 )
            (and
              (BelowOf3 ?x_22)
              (AboveOf17 ?x_22)
            )
          )
          (exists (?x_0 )
            (and
              (RightOf14 ?x_0)
              (LeftOf10 ?x_0)
            )
          )
          (exists (?x_8 )
            (and
              (RightOf16 ?x_8)
              (LeftOf12 ?x_8)
            )
          )
          (exists (?x_22 )
            (and
              (AboveOf15 ?x_22)
              (BelowOf10 ?x_22)
            )
          )
          (exists (?x_27 )
            (and
              (AboveOf15 ?x_27)
              (BelowOf6 ?x_27)
            )
          )
          (exists (?x_11 )
            (and
              (BelowOf13 ?x_11)
              (AboveOf13 ?x_11)
            )
          )
          (exists (?x_0 )
            (and
              (LeftOf7 ?x_0)
              (RightOf15 ?x_0)
            )
          )
          (exists (?x_12 )
            (and
              (RightOf16 ?x_12)
              (LeftOf13 ?x_12)
            )
          )
          (exists (?x_18 )
            (and
              (AboveOf14 ?x_18)
              (BelowOf2 ?x_18)
            )
          )
          (exists (?x_13 )
            (and
              (RightOf16 ?x_13)
              (LeftOf5 ?x_13)
            )
          )
          (exists (?x_11 )
            (and
              (BelowOf1 ?x_11)
              (AboveOf15 ?x_11)
            )
          )
          (exists (?x_3 )
            (and
              (RightOf7 ?x_3)
              (LeftOf1 ?x_3)
            )
          )
          (exists (?x_14 )
            (and
              (BelowOf3 ?x_14)
              (AboveOf12 ?x_14)
            )
          )
          (exists (?x_1 )
            (and
              (AboveOf17 ?x_1)
              (BelowOf4 ?x_1)
            )
          )
          (exists (?x_2 )
            (and
              (LeftOf1 ?x_2)
              (RightOf12 ?x_2)
            )
          )
          (exists (?x_7 )
            (and
              (AboveOf16 ?x_7)
              (BelowOf9 ?x_7)
            )
          )
          (exists (?x_14 )
            (and
              (AboveOf17 ?x_14)
              (BelowOf1 ?x_14)
            )
          )
          (exists (?x_29 )
            (and
              (RightOf14 ?x_29)
              (LeftOf3 ?x_29)
            )
          )
          (exists (?x_26 )
            (and
              (LeftOf6 ?x_26)
              (RightOf17 ?x_26)
            )
          )
          (exists (?x_9 )
            (and
              (RightOf14 ?x_9)
              (LeftOf3 ?x_9)
            )
          )
          (exists (?x_24 )
            (and
              (LeftOf3 ?x_24)
              (RightOf8 ?x_24)
            )
          )
          (exists (?x_5 )
            (and
              (AboveOf15 ?x_5)
              (BelowOf2 ?x_5)
            )
          )
          (exists (?x_31 )
            (and
              (BelowOf2 ?x_31)
              (AboveOf14 ?x_31)
            )
          )
          (exists (?x_25 )
            (and
              (BelowOf2 ?x_25)
              (AboveOf11 ?x_25)
            )
          )
          (exists (?x_29 )
            (and
              (RightOf12 ?x_29)
              (LeftOf8 ?x_29)
            )
          )
          (exists (?x_21 )
            (and
              (AboveOf13 ?x_21)
              (BelowOf2 ?x_21)
            )
          )
          (exists (?x_30 )
            (and
              (BelowOf4 ?x_30)
              (AboveOf8 ?x_30)
            )
          )
          (exists (?x_15 )
            (and
              (LeftOf9 ?x_15)
              (RightOf14 ?x_15)
            )
          )
          (exists (?x_10 )
            (and
              (RightOf16 ?x_10)
              (LeftOf1 ?x_10)
            )
          )
          (exists (?x_23 )
            (and
              (AboveOf14 ?x_23)
              (BelowOf11 ?x_23)
            )
          )
          (exists (?x_10 )
            (and
              (RightOf7 ?x_10)
              (LeftOf5 ?x_10)
            )
          )
          (exists (?x_7 )
            (and
              (BelowOf16 ?x_7)
              (AboveOf17 ?x_7)
            )
          )
          (exists (?x_13 )
            (and
              (LeftOf10 ?x_13)
              (RightOf12 ?x_13)
            )
          )
          (exists (?x_1 )
            (and
              (BelowOf9 ?x_1)
              (AboveOf16 ?x_1)
            )
          )
          (exists (?x_32 )
            (and
              (RightOf16 ?x_32)
              (LeftOf7 ?x_32)
            )
          )
          (exists (?x_31 )
            (and
              (AboveOf13 ?x_31)
              (BelowOf6 ?x_31)
            )
          )
          (exists (?x_8 )
            (and
              (RightOf16 ?x_8)
              (LeftOf16 ?x_8)
            )
          )
          (exists (?x_12 )
            (and
              (RightOf16 ?x_12)
              (LeftOf3 ?x_12)
            )
          )
          (exists (?x_10 )
            (and
              (LeftOf2 ?x_10)
              (RightOf11 ?x_10)
            )
          )
          (exists (?x_28 )
            (and
              (LeftOf3 ?x_28)
              (RightOf10 ?x_28)
            )
          )
          (exists (?x_25 )
            (and
              (AboveOf6 ?x_25)
              (BelowOf3 ?x_25)
            )
          )
          (exists (?x_9 )
            (and
              (RightOf16 ?x_9)
              (LeftOf5 ?x_9)
            )
          )
          (exists (?x_0 )
            (and
              (LeftOf12 ?x_0)
              (RightOf15 ?x_0)
            )
          )
          (exists (?x_31 )
            (and
              (AboveOf16 ?x_31)
              (BelowOf2 ?x_31)
            )
          )
          (exists (?x_28 )
            (and
              (LeftOf4 ?x_28)
              (RightOf11 ?x_28)
            )
          )
          (exists (?x_11 )
            (and
              (BelowOf3 ?x_11)
              (AboveOf13 ?x_11)
            )
          )
          (exists (?x_18 )
            (and
              (AboveOf17 ?x_18)
              (BelowOf9 ?x_18)
            )
          )
          (exists (?x_6 )
            (and
              (AboveOf13 ?x_6)
              (BelowOf5 ?x_6)
            )
          )
          (exists (?x_21 )
            (and
              (AboveOf8 ?x_21)
              (BelowOf6 ?x_21)
            )
          )
          (exists (?x_33 )
            (and
              (LeftOf4 ?x_33)
              (RightOf8 ?x_33)
            )
          )
          (exists (?x_32 )
            (and
              (RightOf12 ?x_32)
              (LeftOf3 ?x_32)
            )
          )
          (exists (?x_23 )
            (and
              (AboveOf16 ?x_23)
              (BelowOf13 ?x_23)
            )
          )
          (exists (?x_11 )
            (and
              (AboveOf17 ?x_11)
              (BelowOf8 ?x_11)
            )
          )
          (exists (?x_24 )
            (and
              (RightOf7 ?x_24)
              (LeftOf2 ?x_24)
            )
          )
          (exists (?x_20 )
            (and
              (AboveOf11 ?x_20)
              (BelowOf1 ?x_20)
            )
          )
          (exists (?x_3 )
            (and
              (LeftOf1 ?x_3)
              (RightOf12 ?x_3)
            )
          )
          (exists (?x_12 )
            (and
              (LeftOf11 ?x_12)
              (RightOf14 ?x_12)
            )
          )
          (exists (?x_1 )
            (and
              (BelowOf8 ?x_1)
              (AboveOf14 ?x_1)
            )
          )
          (exists (?x_28 )
            (and
              (RightOf13 ?x_28)
              (LeftOf1 ?x_28)
            )
          )
          (exists (?x_22 )
            (and
              (AboveOf11 ?x_22)
              (BelowOf3 ?x_22)
            )
          )
          (exists (?x_8 )
            (and
              (LeftOf5 ?x_8)
              (RightOf17 ?x_8)
            )
          )
          (exists (?x_23 )
            (and
              (AboveOf15 ?x_23)
              (BelowOf7 ?x_23)
            )
          )
          (exists (?x_31 )
            (and
              (AboveOf9 ?x_31)
              (BelowOf7 ?x_31)
            )
          )
          (exists (?x_16 )
            (and
              (RightOf16 ?x_16)
              (LeftOf1 ?x_16)
            )
          )
          (exists (?x_29 )
            (and
              (LeftOf10 ?x_29)
              (RightOf13 ?x_29)
            )
          )
          (exists (?x_14 )
            (and
              (BelowOf7 ?x_14)
              (AboveOf16 ?x_14)
            )
          )
          (exists (?x_30 )
            (and
              (AboveOf9 ?x_30)
              (BelowOf3 ?x_30)
            )
          )
          (exists (?x_16 )
            (and
              (LeftOf1 ?x_16)
              (RightOf11 ?x_16)
            )
          )
          (exists (?x_18 )
            (and
              (BelowOf11 ?x_18)
              (AboveOf16 ?x_18)
            )
          )
          (exists (?x_31 )
            (and
              (AboveOf17 ?x_31)
              (BelowOf5 ?x_31)
            )
          )
          (exists (?x_10 )
            (and
              (LeftOf3 ?x_10)
              (RightOf12 ?x_10)
            )
          )
          (exists (?x_15 )
            (and
              (RightOf13 ?x_15)
              (LeftOf8 ?x_15)
            )
          )
          (exists (?x_18 )
            (and
              (BelowOf8 ?x_18)
              (AboveOf12 ?x_18)
            )
          )
          (exists (?x_29 )
            (and
              (RightOf17 ?x_29)
              (LeftOf5 ?x_29)
            )
          )
          (exists (?x_18 )
            (and
              (BelowOf11 ?x_18)
              (AboveOf15 ?x_18)
            )
          )
          (exists (?x_7 )
            (and
              (BelowOf6 ?x_7)
              (AboveOf17 ?x_7)
            )
          )
          (exists (?x_15 )
            (and
              (RightOf11 ?x_15)
              (LeftOf2 ?x_15)
            )
          )
          (exists (?x_5 )
            (and
              (BelowOf3 ?x_5)
              (AboveOf11 ?x_5)
            )
          )
          (exists (?x_31 )
            (and
              (AboveOf16 ?x_31)
              (BelowOf3 ?x_31)
            )
          )
          (exists (?x_22 )
            (and
              (BelowOf3 ?x_22)
              (AboveOf13 ?x_22)
            )
          )
          (exists (?x_9 )
            (and
              (RightOf10 ?x_9)
              (LeftOf5 ?x_9)
            )
          )
          (exists (?x_22 )
            (and
              (AboveOf15 ?x_22)
              (BelowOf4 ?x_22)
            )
          )
          (exists (?x_7 )
            (and
              (AboveOf16 ?x_7)
              (BelowOf3 ?x_7)
            )
          )
          (exists (?x_15 )
            (and
              (LeftOf5 ?x_15)
              (RightOf15 ?x_15)
            )
          )
          (exists (?x_15 )
            (and
              (LeftOf9 ?x_15)
              (RightOf12 ?x_15)
            )
          )
          (exists (?x_19 )
            (and
              (LeftOf7 ?x_19)
              (RightOf17 ?x_19)
            )
          )
          (exists (?x_32 )
            (and
              (RightOf15 ?x_32)
              (LeftOf9 ?x_32)
            )
          )
          (exists (?x_27 )
            (and
              (BelowOf10 ?x_27)
              (AboveOf17 ?x_27)
            )
          )
          (exists (?x_5 )
            (and
              (AboveOf5 ?x_5)
              (BelowOf1 ?x_5)
            )
          )
          (exists (?x_10 )
            (and
              (LeftOf3 ?x_10)
              (RightOf17 ?x_10)
            )
          )
          (exists (?x_9 )
            (and
              (RightOf8 ?x_9)
              (LeftOf8 ?x_9)
            )
          )
          (exists (?x_6 )
            (and
              (AboveOf13 ?x_6)
              (BelowOf4 ?x_6)
            )
          )
          (exists (?x_27 )
            (and
              (AboveOf16 ?x_27)
              (BelowOf12 ?x_27)
            )
          )
          (exists (?x_17 )
            (and
              (BelowOf1 ?x_17)
              (AboveOf15 ?x_17)
            )
          )
          (exists (?x_33 )
            (and
              (RightOf10 ?x_33)
              (LeftOf2 ?x_33)
            )
          )
          (exists (?x_21 )
            (and
              (AboveOf8 ?x_21)
              (BelowOf7 ?x_21)
            )
          )
          (exists (?x_26 )
            (and
              (LeftOf4 ?x_26)
              (RightOf16 ?x_26)
            )
          )
          (exists (?x_24 )
            (and
              (LeftOf3 ?x_24)
              (RightOf14 ?x_24)
            )
          )
          (exists (?x_33 )
            (and
              (RightOf11 ?x_33)
              (LeftOf4 ?x_33)
            )
          )
          (exists (?x_0 )
            (and
              (LeftOf4 ?x_0)
              (RightOf15 ?x_0)
            )
          )
          (exists (?x_9 )
            (and
              (LeftOf4 ?x_9)
              (RightOf13 ?x_9)
            )
          )
          (exists (?x_23 )
            (and
              (BelowOf5 ?x_23)
              (AboveOf16 ?x_23)
            )
          )
          (exists (?x_10 )
            (and
              (RightOf13 ?x_10)
              (LeftOf1 ?x_10)
            )
          )
          (exists (?x_21 )
            (and
              (BelowOf5 ?x_21)
              (AboveOf16 ?x_21)
            )
          )
          (exists (?x_1 )
            (and
              (AboveOf11 ?x_1)
              (BelowOf6 ?x_1)
            )
          )
          (exists (?x_24 )
            (and
              (LeftOf2 ?x_24)
              (RightOf8 ?x_24)
            )
          )
          (exists (?x_29 )
            (and
              (LeftOf6 ?x_29)
              (RightOf15 ?x_29)
            )
          )
          (exists (?x_1 )
            (and
              (BelowOf10 ?x_1)
              (AboveOf16 ?x_1)
            )
          )
          (exists (?x_14 )
            (and
              (AboveOf12 ?x_14)
              (BelowOf4 ?x_14)
            )
          )
          (exists (?x_14 )
            (and
              (BelowOf7 ?x_14)
              (AboveOf13 ?x_14)
            )
          )
          (exists (?x_12 )
            (and
              (LeftOf9 ?x_12)
              (RightOf17 ?x_12)
            )
          )
          (exists (?x_23 )
            (and
              (BelowOf12 ?x_23)
              (AboveOf16 ?x_23)
            )
          )
          (exists (?x_20 )
            (and
              (AboveOf15 ?x_20)
              (BelowOf2 ?x_20)
            )
          )
          (exists (?x_3 )
            (and
              (RightOf13 ?x_3)
              (LeftOf1 ?x_3)
            )
          )
          (exists (?x_13 )
            (and
              (LeftOf2 ?x_13)
              (RightOf17 ?x_13)
            )
          )
          (exists (?x_14 )
            (and
              (AboveOf9 ?x_14)
              (BelowOf1 ?x_14)
            )
          )
          (exists (?x_20 )
            (and
              (BelowOf1 ?x_20)
              (AboveOf15 ?x_20)
            )
          )
          (exists (?x_12 )
            (and
              (RightOf15 ?x_12)
              (LeftOf5 ?x_12)
            )
          )
          (exists (?x_26 )
            (and
              (LeftOf8 ?x_26)
              (RightOf16 ?x_26)
            )
          )
          (exists (?x_29 )
            (and
              (LeftOf6 ?x_29)
              (RightOf13 ?x_29)
            )
          )
          (exists (?x_23 )
            (and
              (AboveOf15 ?x_23)
              (BelowOf4 ?x_23)
            )
          )
          (exists (?x_32 )
            (and
              (RightOf16 ?x_32)
              (LeftOf2 ?x_32)
            )
          )
          (exists (?x_18 )
            (and
              (AboveOf13 ?x_18)
              (BelowOf3 ?x_18)
            )
          )
          (exists (?x_25 )
            (and
              (AboveOf17 ?x_25)
              (BelowOf5 ?x_25)
            )
          )
          (exists (?x_33 )
            (and
              (RightOf11 ?x_33)
              (LeftOf2 ?x_33)
            )
          )
          (exists (?x_23 )
            (and
              (BelowOf3 ?x_23)
              (AboveOf15 ?x_23)
            )
          )
          (exists (?x_22 )
            (and
              (AboveOf13 ?x_22)
              (BelowOf9 ?x_22)
            )
          )
          (exists (?x_5 )
            (and
              (BelowOf2 ?x_5)
              (AboveOf13 ?x_5)
            )
          )
          (exists (?x_22 )
            (and
              (AboveOf11 ?x_22)
              (BelowOf7 ?x_22)
            )
          )
          (exists (?x_33 )
            (and
              (RightOf17 ?x_33)
              (LeftOf1 ?x_33)
            )
          )
          (exists (?x_5 )
            (and
              (AboveOf10 ?x_5)
              (BelowOf2 ?x_5)
            )
          )
          (exists (?x_23 )
            (and
              (AboveOf17 ?x_23)
              (BelowOf2 ?x_23)
            )
          )
          (exists (?x_11 )
            (and
              (AboveOf15 ?x_11)
              (BelowOf10 ?x_11)
            )
          )
          (exists (?x_10 )
            (and
              (LeftOf4 ?x_10)
              (RightOf11 ?x_10)
            )
          )
          (exists (?x_21 )
            (and
              (AboveOf9 ?x_21)
              (BelowOf2 ?x_21)
            )
          )
          (exists (?x_20 )
            (and
              (AboveOf14 ?x_20)
              (BelowOf2 ?x_20)
            )
          )
          (exists (?x_23 )
            (and
              (AboveOf17 ?x_23)
              (BelowOf10 ?x_23)
            )
          )
          (exists (?x_5 )
            (and
              (BelowOf2 ?x_5)
              (AboveOf7 ?x_5)
            )
          )
          (exists (?x_22 )
            (and
              (BelowOf9 ?x_22)
              (AboveOf16 ?x_22)
            )
          )
          (exists (?x_14 )
            (and
              (AboveOf9 ?x_14)
              (BelowOf4 ?x_14)
            )
          )
          (exists (?x_31 )
            (and
              (BelowOf4 ?x_31)
              (AboveOf11 ?x_31)
            )
          )
          (exists (?x_24 )
            (and
              (RightOf10 ?x_24)
              (LeftOf1 ?x_24)
            )
          )
          (exists (?x_16 )
            (and
              (LeftOf2 ?x_16)
              (RightOf13 ?x_16)
            )
          )
          (exists (?x_1 )
            (and
              (BelowOf1 ?x_1)
              (AboveOf17 ?x_1)
            )
          )
          (exists (?x_25 )
            (and
              (AboveOf10 ?x_25)
              (BelowOf2 ?x_25)
            )
          )
          (exists (?x_0 )
            (and
              (LeftOf12 ?x_0)
              (RightOf16 ?x_0)
            )
          )
          (exists (?x_6 )
            (and
              (BelowOf1 ?x_6)
              (AboveOf8 ?x_6)
            )
          )
          (exists (?x_25 )
            (and
              (AboveOf8 ?x_25)
              (BelowOf5 ?x_25)
            )
          )
          (exists (?x_21 )
            (and
              (AboveOf8 ?x_21)
              (BelowOf8 ?x_21)
            )
          )
          (exists (?x_16 )
            (and
              (RightOf9 ?x_16)
              (LeftOf3 ?x_16)
            )
          )
          (exists (?x_21 )
            (and
              (BelowOf5 ?x_21)
              (AboveOf17 ?x_21)
            )
          )
          (exists (?x_21 )
            (and
              (AboveOf11 ?x_21)
              (BelowOf2 ?x_21)
            )
          )
          (exists (?x_32 )
            (and
              (LeftOf10 ?x_32)
              (RightOf14 ?x_32)
            )
          )
          (exists (?x_12 )
            (and
              (LeftOf8 ?x_12)
              (RightOf17 ?x_12)
            )
          )
          (exists (?x_0 )
            (and
              (LeftOf6 ?x_0)
              (RightOf17 ?x_0)
            )
          )
          (exists (?x_16 )
            (and
              (RightOf12 ?x_16)
              (LeftOf5 ?x_16)
            )
          )
          (exists (?x_14 )
            (and
              (BelowOf1 ?x_14)
              (AboveOf16 ?x_14)
            )
          )
          (exists (?x_10 )
            (and
              (LeftOf7 ?x_10)
              (RightOf7 ?x_10)
            )
          )
          (exists (?x_16 )
            (and
              (RightOf16 ?x_16)
              (LeftOf4 ?x_16)
            )
          )
          (exists (?x_1 )
            (and
              (AboveOf16 ?x_1)
              (BelowOf5 ?x_1)
            )
          )
          (exists (?x_13 )
            (and
              (RightOf16 ?x_13)
              (LeftOf8 ?x_13)
            )
          )
          (exists (?x_14 )
            (and
              (BelowOf3 ?x_14)
              (AboveOf11 ?x_14)
            )
          )
          (exists (?x_13 )
            (and
              (RightOf13 ?x_13)
              (LeftOf8 ?x_13)
            )
          )
          (exists (?x_12 )
            (and
              (RightOf14 ?x_12)
              (LeftOf13 ?x_12)
            )
          )
          (exists (?x_33 )
            (and
              (RightOf5 ?x_33)
              (LeftOf2 ?x_33)
            )
          )
          (exists (?x_3 )
            (and
              (RightOf14 ?x_3)
              (LeftOf2 ?x_3)
            )
          )
          (exists (?x_24 )
            (and
              (RightOf12 ?x_24)
              (LeftOf1 ?x_24)
            )
          )
          (exists (?x_14 )
            (and
              (BelowOf6 ?x_14)
              (AboveOf14 ?x_14)
            )
          )
          (exists (?x_30 )
            (and
              (AboveOf7 ?x_30)
              (BelowOf3 ?x_30)
            )
          )
          (exists (?x_28 )
            (and
              (LeftOf4 ?x_28)
              (RightOf10 ?x_28)
            )
          )
          (exists (?x_14 )
            (and
              (BelowOf4 ?x_14)
              (AboveOf13 ?x_14)
            )
          )
          (exists (?x_30 )
            (and
              (BelowOf2 ?x_30)
              (AboveOf14 ?x_30)
            )
          )
          (exists (?x_18 )
            (and
              (AboveOf15 ?x_18)
              (BelowOf10 ?x_18)
            )
          )
          (exists (?x_7 )
            (and
              (AboveOf16 ?x_7)
              (BelowOf16 ?x_7)
            )
          )
          (exists (?x_25 )
            (and
              (AboveOf13 ?x_25)
              (BelowOf5 ?x_25)
            )
          )
          (exists (?x_28 )
            (and
              (RightOf14 ?x_28)
              (LeftOf1 ?x_28)
            )
          )
          (exists (?x_1 )
            (and
              (AboveOf10 ?x_1)
              (BelowOf3 ?x_1)
            )
          )
          (exists (?x_1 )
            (and
              (BelowOf7 ?x_1)
              (AboveOf17 ?x_1)
            )
          )
          (exists (?x_30 )
            (and
              (BelowOf1 ?x_30)
              (AboveOf7 ?x_30)
            )
          )
          (exists (?x_22 )
            (and
              (AboveOf11 ?x_22)
              (BelowOf10 ?x_22)
            )
          )
          (exists (?x_29 )
            (and
              (LeftOf6 ?x_29)
              (RightOf17 ?x_29)
            )
          )
          (exists (?x_31 )
            (and
              (BelowOf3 ?x_31)
              (AboveOf11 ?x_31)
            )
          )
          (exists (?x_6 )
            (and
              (AboveOf16 ?x_6)
              (BelowOf5 ?x_6)
            )
          )
          (exists (?x_7 )
            (and
              (AboveOf16 ?x_7)
              (BelowOf5 ?x_7)
            )
          )
          (exists (?x_14 )
            (and
              (BelowOf9 ?x_14)
              (AboveOf12 ?x_14)
            )
          )
          (exists (?x_31 )
            (and
              (BelowOf4 ?x_31)
              (AboveOf12 ?x_31)
            )
          )
          (exists (?x_30 )
            (and
              (BelowOf3 ?x_30)
              (AboveOf6 ?x_30)
            )
          )
          (exists (?x_16 )
            (and
              (LeftOf5 ?x_16)
              (RightOf14 ?x_16)
            )
          )
          (exists (?x_33 )
            (and
              (LeftOf3 ?x_33)
              (RightOf7 ?x_33)
            )
          )
          (exists (?x_0 )
            (and
              (RightOf16 ?x_0)
              (LeftOf8 ?x_0)
            )
          )
          (exists (?x_29 )
            (and
              (RightOf13 ?x_29)
              (LeftOf9 ?x_29)
            )
          )
          (exists (?x_32 )
            (and
              (LeftOf8 ?x_32)
              (RightOf14 ?x_32)
            )
          )
          (exists (?x_22 )
            (and
              (AboveOf12 ?x_22)
              (BelowOf1 ?x_22)
            )
          )
          (exists (?x_18 )
            (and
              (AboveOf16 ?x_18)
              (BelowOf4 ?x_18)
            )
          )
          (exists (?x_3 )
            (and
              (RightOf5 ?x_3)
              (LeftOf2 ?x_3)
            )
          )
          (exists (?x_6 )
            (and
              (AboveOf7 ?x_6)
              (BelowOf3 ?x_6)
            )
          )
          (exists (?x_3 )
            (and
              (RightOf8 ?x_3)
              (LeftOf2 ?x_3)
            )
          )
          (exists (?x_29 )
            (and
              (RightOf16 ?x_29)
              (LeftOf10 ?x_29)
            )
          )
          (exists (?x_13 )
            (and
              (LeftOf2 ?x_13)
              (RightOf12 ?x_13)
            )
          )
          (exists (?x_7 )
            (and
              (BelowOf3 ?x_7)
              (AboveOf17 ?x_7)
            )
          )
          (exists (?x_24 )
            (and
              (RightOf14 ?x_24)
              (LeftOf2 ?x_24)
            )
          )
          (exists (?x_21 )
            (and
              (BelowOf5 ?x_21)
              (AboveOf13 ?x_21)
            )
          )
          (exists (?x_10 )
            (and
              (RightOf16 ?x_10)
              (LeftOf3 ?x_10)
            )
          )
          (exists (?x_23 )
            (and
              (AboveOf16 ?x_23)
              (BelowOf9 ?x_23)
            )
          )
          (exists (?x_18 )
            (and
              (AboveOf14 ?x_18)
              (BelowOf5 ?x_18)
            )
          )
          (exists (?x_22 )
            (and
              (AboveOf12 ?x_22)
              (BelowOf9 ?x_22)
            )
          )
          (exists (?x_5 )
            (and
              (AboveOf15 ?x_5)
              (BelowOf1 ?x_5)
            )
          )
          (exists (?x_13 )
            (and
              (LeftOf4 ?x_13)
              (RightOf16 ?x_13)
            )
          )
          (exists (?x_29 )
            (and
              (RightOf17 ?x_29)
              (LeftOf9 ?x_29)
            )
          )
          (exists (?x_14 )
            (and
              (BelowOf6 ?x_14)
              (AboveOf9 ?x_14)
            )
          )
          (exists (?x_8 )
            (and
              (RightOf16 ?x_8)
              (LeftOf8 ?x_8)
            )
          )
          (exists (?x_16 )
            (and
              (RightOf16 ?x_16)
              (LeftOf3 ?x_16)
            )
          )
          (exists (?x_33 )
            (and
              (LeftOf1 ?x_33)
              (RightOf15 ?x_33)
            )
          )
          (exists (?x_12 )
            (and
              (RightOf16 ?x_12)
              (LeftOf5 ?x_12)
            )
          )
          (exists (?x_18 )
            (and
              (AboveOf13 ?x_18)
              (BelowOf5 ?x_18)
            )
          )
          (exists (?x_13 )
            (and
              (RightOf10 ?x_13)
              (LeftOf4 ?x_13)
            )
          )
          (exists (?x_26 )
            (and
              (LeftOf10 ?x_26)
              (RightOf15 ?x_26)
            )
          )
          (exists (?x_22 )
            (and
              (BelowOf3 ?x_22)
              (AboveOf16 ?x_22)
            )
          )
          (exists (?x_9 )
            (and
              (RightOf17 ?x_9)
              (LeftOf8 ?x_9)
            )
          )
          (exists (?x_1 )
            (and
              (BelowOf7 ?x_1)
              (AboveOf12 ?x_1)
            )
          )
          (exists (?x_0 )
            (and
              (RightOf15 ?x_0)
              (LeftOf10 ?x_0)
            )
          )
          (exists (?x_15 )
            (and
              (LeftOf4 ?x_15)
              (RightOf13 ?x_15)
            )
          )
          (exists (?x_11 )
            (and
              (BelowOf6 ?x_11)
              (AboveOf16 ?x_11)
            )
          )
          (exists (?x_31 )
            (and
              (AboveOf10 ?x_31)
              (BelowOf7 ?x_31)
            )
          )
          (exists (?x_14 )
            (and
              (BelowOf3 ?x_14)
              (AboveOf16 ?x_14)
            )
          )
          (exists (?x_9 )
            (and
              (RightOf14 ?x_9)
              (LeftOf8 ?x_9)
            )
          )
          (exists (?x_25 )
            (and
              (BelowOf4 ?x_25)
              (AboveOf12 ?x_25)
            )
          )
          (exists (?x_1 )
            (and
              (AboveOf13 ?x_1)
              (BelowOf7 ?x_1)
            )
          )
          (exists (?x_23 )
            (and
              (BelowOf12 ?x_23)
              (AboveOf17 ?x_23)
            )
          )
          (exists (?x_11 )
            (and
              (BelowOf12 ?x_11)
              (AboveOf15 ?x_11)
            )
          )
          (exists (?x_14 )
            (and
              (AboveOf10 ?x_14)
              (BelowOf2 ?x_14)
            )
          )
          (exists (?x_5 )
            (and
              (AboveOf12 ?x_5)
              (BelowOf3 ?x_5)
            )
          )
          (exists (?x_18 )
            (and
              (BelowOf5 ?x_18)
              (AboveOf13 ?x_18)
            )
          )
          (exists (?x_28 )
            (and
              (LeftOf4 ?x_28)
              (RightOf4 ?x_28)
            )
          )
          (exists (?x_33 )
            (and
              (RightOf11 ?x_33)
              (LeftOf3 ?x_33)
            )
          )
          (exists (?x_26 )
            (and
              (LeftOf8 ?x_26)
              (RightOf15 ?x_26)
            )
          )
          (exists (?x_14 )
            (and
              (AboveOf15 ?x_14)
              (BelowOf3 ?x_14)
            )
          )
          (exists (?x_0 )
            (and
              (LeftOf13 ?x_0)
              (RightOf13 ?x_0)
            )
          )
          (exists (?x_22 )
            (and
              (BelowOf7 ?x_22)
              (AboveOf16 ?x_22)
            )
          )
          (exists (?x_9 )
            (and
              (LeftOf7 ?x_9)
              (RightOf8 ?x_9)
            )
          )
          (exists (?x_30 )
            (and
              (BelowOf4 ?x_30)
              (AboveOf17 ?x_30)
            )
          )
          (exists (?x_24 )
            (and
              (RightOf16 ?x_24)
              (LeftOf1 ?x_24)
            )
          )
          (exists (?x_29 )
            (and
              (RightOf15 ?x_29)
              (LeftOf5 ?x_29)
            )
          )
          (exists (?x_10 )
            (and
              (RightOf8 ?x_10)
              (LeftOf4 ?x_10)
            )
          )
          (exists (?x_1 )
            (and
              (AboveOf16 ?x_1)
              (BelowOf7 ?x_1)
            )
          )
          (exists (?x_29 )
            (and
              (RightOf14 ?x_29)
              (LeftOf1 ?x_29)
            )
          )
          (exists (?x_1 )
            (and
              (AboveOf17 ?x_1)
              (BelowOf3 ?x_1)
            )
          )
          (exists (?x_25 )
            (and
              (BelowOf2 ?x_25)
              (AboveOf15 ?x_25)
            )
          )
          (exists (?x_22 )
            (and
              (BelowOf7 ?x_22)
              (AboveOf13 ?x_22)
            )
          )
          (exists (?x_22 )
            (and
              (AboveOf15 ?x_22)
              (BelowOf7 ?x_22)
            )
          )
          (exists (?x_15 )
            (and
              (RightOf9 ?x_15)
              (LeftOf2 ?x_15)
            )
          )
          (exists (?x_16 )
            (and
              (LeftOf4 ?x_16)
              (RightOf14 ?x_16)
            )
          )
          (exists (?x_11 )
            (and
              (AboveOf14 ?x_11)
              (BelowOf6 ?x_11)
            )
          )
          (exists (?x_33 )
            (and
              (LeftOf3 ?x_33)
              (RightOf6 ?x_33)
            )
          )
          (exists (?x_10 )
            (and
              (LeftOf4 ?x_10)
              (RightOf14 ?x_10)
            )
          )
          (exists (?x_18 )
            (and
              (BelowOf1 ?x_18)
              (AboveOf17 ?x_18)
            )
          )
          (exists (?x_10 )
            (and
              (RightOf15 ?x_10)
              (LeftOf3 ?x_10)
            )
          )
          (exists (?x_26 )
            (and
              (LeftOf14 ?x_26)
              (RightOf16 ?x_26)
            )
          )
          (exists (?x_31 )
            (and
              (AboveOf13 ?x_31)
              (BelowOf3 ?x_31)
            )
          )
          (exists (?x_26 )
            (and
              (RightOf17 ?x_26)
              (LeftOf7 ?x_26)
            )
          )
          (exists (?x_21 )
            (and
              (BelowOf7 ?x_21)
              (AboveOf15 ?x_21)
            )
          )
          (exists (?x_15 )
            (and
              (RightOf17 ?x_15)
              (LeftOf2 ?x_15)
            )
          )
          (exists (?x_14 )
            (and
              (AboveOf15 ?x_14)
              (BelowOf1 ?x_14)
            )
          )
          (exists (?x_27 )
            (and
              (BelowOf3 ?x_27)
              (AboveOf17 ?x_27)
            )
          )
          (exists (?x_12 )
            (and
              (RightOf15 ?x_12)
              (LeftOf6 ?x_12)
            )
          )
          (exists (?x_9 )
            (and
              (RightOf17 ?x_9)
              (LeftOf7 ?x_9)
            )
          )
          (exists (?x_13 )
            (and
              (LeftOf7 ?x_13)
              (RightOf17 ?x_13)
            )
          )
          (exists (?x_6 )
            (and
              (BelowOf1 ?x_6)
              (AboveOf13 ?x_6)
            )
          )
          (exists (?x_33 )
            (and
              (LeftOf1 ?x_33)
              (RightOf10 ?x_33)
            )
          )
          (exists (?x_28 )
            (and
              (RightOf4 ?x_28)
              (LeftOf1 ?x_28)
            )
          )
          (exists (?x_26 )
            (and
              (LeftOf15 ?x_26)
              (RightOf15 ?x_26)
            )
          )
          (exists (?x_25 )
            (and
              (BelowOf1 ?x_25)
              (AboveOf13 ?x_25)
            )
          )
          (exists (?x_9 )
            (and
              (LeftOf8 ?x_9)
              (RightOf9 ?x_9)
            )
          )
          (exists (?x_4 )
            (and
              (BelowOf1 ?x_4)
              (AboveOf17 ?x_4)
            )
          )
          (exists (?x_1 )
            (and
              (BelowOf9 ?x_1)
              (AboveOf14 ?x_1)
            )
          )
          (exists (?x_26 )
            (and
              (LeftOf10 ?x_26)
              (RightOf16 ?x_26)
            )
          )
          (exists (?x_11 )
            (and
              (BelowOf5 ?x_11)
              (AboveOf17 ?x_11)
            )
          )
          (exists (?x_7 )
            (and
              (BelowOf13 ?x_7)
              (AboveOf17 ?x_7)
            )
          )
          (exists (?x_18 )
            (and
              (AboveOf17 ?x_18)
              (BelowOf1 ?x_18)
            )
          )
          (exists (?x_24 )
            (and
              (LeftOf3 ?x_24)
              (RightOf15 ?x_24)
            )
          )
          (exists (?x_1 )
            (and
              (BelowOf5 ?x_1)
              (AboveOf15 ?x_1)
            )
          )
          (exists (?x_31 )
            (and
              (AboveOf13 ?x_31)
              (BelowOf2 ?x_31)
            )
          )
          (exists (?x_29 )
            (and
              (RightOf17 ?x_29)
              (LeftOf11 ?x_29)
            )
          )
          (exists (?x_8 )
            (and
              (RightOf16 ?x_8)
              (LeftOf9 ?x_8)
            )
          )
          (exists (?x_15 )
            (and
              (LeftOf1 ?x_15)
              (RightOf11 ?x_15)
            )
          )
          (exists (?x_21 )
            (and
              (AboveOf17 ?x_21)
              (BelowOf2 ?x_21)
            )
          )
          (exists (?x_28 )
            (and
              (LeftOf4 ?x_28)
              (RightOf13 ?x_28)
            )
          )
          (exists (?x_29 )
            (and
              (RightOf13 ?x_29)
              (LeftOf1 ?x_29)
            )
          )
          (exists (?x_5 )
            (and
              (BelowOf3 ?x_5)
              (AboveOf7 ?x_5)
            )
          )
          (exists (?x_6 )
            (and
              (BelowOf3 ?x_6)
              (AboveOf12 ?x_6)
            )
          )
          (exists (?x_12 )
            (and
              (RightOf14 ?x_12)
              (LeftOf3 ?x_12)
            )
          )
          (exists (?x_21 )
            (and
              (BelowOf3 ?x_21)
              (AboveOf9 ?x_21)
            )
          )
          (exists (?x_33 )
            (and
              (LeftOf1 ?x_33)
              (RightOf6 ?x_33)
            )
          )
          (exists (?x_1 )
            (and
              (AboveOf16 ?x_1)
              (BelowOf4 ?x_1)
            )
          )
          (exists (?x_1 )
            (and
              (BelowOf2 ?x_1)
              (AboveOf16 ?x_1)
            )
          )
          (exists (?x_27 )
            (and
              (AboveOf15 ?x_27)
              (BelowOf15 ?x_27)
            )
          )
          (exists (?x_24 )
            (and
              (LeftOf3 ?x_24)
              (RightOf7 ?x_24)
            )
          )
          (exists (?x_30 )
            (and
              (BelowOf1 ?x_30)
              (AboveOf6 ?x_30)
            )
          )
          (exists (?x_10 )
            (and
              (RightOf15 ?x_10)
              (LeftOf4 ?x_10)
            )
          )
          (exists (?x_33 )
            (and
              (RightOf17 ?x_33)
              (LeftOf2 ?x_33)
            )
          )
          (exists (?x_13 )
            (and
              (LeftOf1 ?x_13)
              (RightOf13 ?x_13)
            )
          )
          (exists (?x_30 )
            (and
              (AboveOf4 ?x_30)
              (BelowOf2 ?x_30)
            )
          )
          (exists (?x_20 )
            (and
              (AboveOf12 ?x_20)
              (BelowOf1 ?x_20)
            )
          )
          (exists (?x_15 )
            (and
              (LeftOf6 ?x_15)
              (RightOf9 ?x_15)
            )
          )
          (exists (?x_33 )
            (and
              (LeftOf5 ?x_33)
              (RightOf7 ?x_33)
            )
          )
          (exists (?x_16 )
            (and
              (LeftOf2 ?x_16)
              (RightOf15 ?x_16)
            )
          )
          (exists (?x_11 )
            (and
              (BelowOf9 ?x_11)
              (AboveOf16 ?x_11)
            )
          )
          (exists (?x_10 )
            (and
              (RightOf15 ?x_10)
              (LeftOf6 ?x_10)
            )
          )
          (exists (?x_21 )
            (and
              (AboveOf8 ?x_21)
              (BelowOf5 ?x_21)
            )
          )
          (exists (?x_25 )
            (and
              (AboveOf16 ?x_25)
              (BelowOf1 ?x_25)
            )
          )
          (exists (?x_29 )
            (and
              (LeftOf6 ?x_29)
              (RightOf12 ?x_29)
            )
          )
          (exists (?x_24 )
            (and
              (RightOf9 ?x_24)
              (LeftOf2 ?x_24)
            )
          )
          (exists (?x_33 )
            (and
              (RightOf9 ?x_33)
              (LeftOf3 ?x_33)
            )
          )
          (exists (?x_18 )
            (and
              (BelowOf5 ?x_18)
              (AboveOf17 ?x_18)
            )
          )
          (exists (?x_23 )
            (and
              (BelowOf3 ?x_23)
              (AboveOf17 ?x_23)
            )
          )
          (exists (?x_9 )
            (and
              (LeftOf2 ?x_9)
              (RightOf12 ?x_9)
            )
          )
          (exists (?x_15 )
            (and
              (RightOf16 ?x_15)
              (LeftOf2 ?x_15)
            )
          )
          (exists (?x_18 )
            (and
              (BelowOf3 ?x_18)
              (AboveOf15 ?x_18)
            )
          )
          (exists (?x_21 )
            (and
              (BelowOf2 ?x_21)
              (AboveOf15 ?x_21)
            )
          )
          (exists (?x_6 )
            (and
              (AboveOf9 ?x_6)
              (BelowOf3 ?x_6)
            )
          )
          (exists (?x_15 )
            (and
              (RightOf17 ?x_15)
              (LeftOf7 ?x_15)
            )
          )
          (exists (?x_17 )
            (and
              (AboveOf2 ?x_17)
              (BelowOf1 ?x_17)
            )
          )
          (exists (?x_5 )
            (and
              (BelowOf2 ?x_5)
              (AboveOf8 ?x_5)
            )
          )
          (exists (?x_13 )
            (and
              (RightOf11 ?x_13)
              (LeftOf6 ?x_13)
            )
          )
          (exists (?x_13 )
            (and
              (RightOf10 ?x_13)
              (LeftOf2 ?x_13)
            )
          )
          (exists (?x_24 )
            (and
              (RightOf12 ?x_24)
              (LeftOf2 ?x_24)
            )
          )
          (exists (?x_9 )
            (and
              (RightOf12 ?x_9)
              (LeftOf1 ?x_9)
            )
          )
          (exists (?x_3 )
            (and
              (RightOf7 ?x_3)
              (LeftOf2 ?x_3)
            )
          )
          (exists (?x_31 )
            (and
              (AboveOf9 ?x_31)
              (BelowOf6 ?x_31)
            )
          )
          (exists (?x_6 )
            (and
              (BelowOf2 ?x_6)
              (AboveOf14 ?x_6)
            )
          )
          (exists (?x_15 )
            (and
              (LeftOf1 ?x_15)
              (RightOf9 ?x_15)
            )
          )
          (exists (?x_11 )
            (and
              (AboveOf17 ?x_11)
              (BelowOf3 ?x_11)
            )
          )
          (exists (?x_21 )
            (and
              (AboveOf16 ?x_21)
              (BelowOf2 ?x_21)
            )
          )
          (exists (?x_9 )
            (and
              (LeftOf4 ?x_9)
              (RightOf16 ?x_9)
            )
          )
          (exists (?x_22 )
            (and
              (AboveOf17 ?x_22)
              (BelowOf1 ?x_22)
            )
          )
          (exists (?x_13 )
            (and
              (LeftOf5 ?x_13)
              (RightOf17 ?x_13)
            )
          )
          (exists (?x_1 )
            (and
              (AboveOf14 ?x_1)
              (BelowOf6 ?x_1)
            )
          )
          (exists (?x_3 )
            (and
              (LeftOf1 ?x_3)
              (RightOf11 ?x_3)
            )
          )
          (exists (?x_12 )
            (and
              (RightOf15 ?x_12)
              (LeftOf2 ?x_12)
            )
          )
          (exists (?x_29 )
            (and
              (RightOf17 ?x_29)
              (LeftOf4 ?x_29)
            )
          )
          (exists (?x_12 )
            (and
              (RightOf15 ?x_12)
              (LeftOf12 ?x_12)
            )
          )
          (exists (?x_22 )
            (and
              (BelowOf11 ?x_22)
              (AboveOf12 ?x_22)
            )
          )
          (exists (?x_5 )
            (and
              (AboveOf3 ?x_5)
              (BelowOf1 ?x_5)
            )
          )
          (exists (?x_13 )
            (and
              (LeftOf9 ?x_13)
              (RightOf15 ?x_13)
            )
          )
          (exists (?x_9 )
            (and
              (RightOf8 ?x_9)
              (LeftOf1 ?x_9)
            )
          )
          (exists (?x_9 )
            (and
              (RightOf15 ?x_9)
              (LeftOf8 ?x_9)
            )
          )
          (exists (?x_10 )
            (and
              (RightOf16 ?x_10)
              (LeftOf2 ?x_10)
            )
          )
          (exists (?x_0 )
            (and
              (LeftOf1 ?x_0)
              (RightOf15 ?x_0)
            )
          )
          (exists (?x_21 )
            (and
              (AboveOf14 ?x_21)
              (BelowOf2 ?x_21)
            )
          )
          (exists (?x_1 )
            (and
              (AboveOf12 ?x_1)
              (BelowOf3 ?x_1)
            )
          )
          (exists (?x_0 )
            (and
              (RightOf14 ?x_0)
              (LeftOf11 ?x_0)
            )
          )
          (exists (?x_27 )
            (and
              (AboveOf16 ?x_27)
              (BelowOf8 ?x_27)
            )
          )
          (exists (?x_24 )
            (and
              (RightOf6 ?x_24)
              (LeftOf1 ?x_24)
            )
          )
          (exists (?x_18 )
            (and
              (BelowOf7 ?x_18)
              (AboveOf12 ?x_18)
            )
          )
          (exists (?x_13 )
            (and
              (RightOf11 ?x_13)
              (LeftOf5 ?x_13)
            )
          )
          (exists (?x_21 )
            (and
              (AboveOf8 ?x_21)
              (BelowOf2 ?x_21)
            )
          )
          (exists (?x_21 )
            (and
              (AboveOf12 ?x_21)
              (BelowOf7 ?x_21)
            )
          )
          (exists (?x_1 )
            (and
              (BelowOf1 ?x_1)
              (AboveOf15 ?x_1)
            )
          )
          (exists (?x_33 )
            (and
              (RightOf13 ?x_33)
              (LeftOf2 ?x_33)
            )
          )
          (exists (?x_15 )
            (and
              (LeftOf6 ?x_15)
              (RightOf15 ?x_15)
            )
          )
          (exists (?x_28 )
            (and
              (RightOf16 ?x_28)
              (LeftOf3 ?x_28)
            )
          )
          (exists (?x_8 )
            (and
              (RightOf16 ?x_8)
              (LeftOf1 ?x_8)
            )
          )
          (exists (?x_29 )
            (and
              (RightOf11 ?x_29)
              (LeftOf9 ?x_29)
            )
          )
          (exists (?x_20 )
            (and
              (AboveOf11 ?x_20)
              (BelowOf2 ?x_20)
            )
          )
          (exists (?x_29 )
            (and
              (RightOf17 ?x_29)
              (LeftOf3 ?x_29)
            )
          )
          (exists (?x_10 )
            (and
              (LeftOf1 ?x_10)
              (RightOf9 ?x_10)
            )
          )
          (exists (?x_20 )
            (and
              (AboveOf17 ?x_20)
              (BelowOf2 ?x_20)
            )
          )
          (exists (?x_28 )
            (and
              (LeftOf4 ?x_28)
              (RightOf9 ?x_28)
            )
          )
          (exists (?x_1 )
            (and
              (AboveOf14 ?x_1)
              (BelowOf3 ?x_1)
            )
          )
          (exists (?x_3 )
            (and
              (RightOf13 ?x_3)
              (LeftOf2 ?x_3)
            )
          )
          (exists (?x_18 )
            (and
              (BelowOf11 ?x_18)
              (AboveOf17 ?x_18)
            )
          )
          (exists (?x_3 )
            (and
              (LeftOf1 ?x_3)
              (RightOf9 ?x_3)
            )
          )
          (exists (?x_17 )
            (and
              (AboveOf5 ?x_17)
              (BelowOf1 ?x_17)
            )
          )
          (exists (?x_32 )
            (and
              (RightOf13 ?x_32)
              (LeftOf7 ?x_32)
            )
          )
          (exists (?x_25 )
            (and
              (BelowOf1 ?x_25)
              (AboveOf10 ?x_25)
            )
          )
          (exists (?x_9 )
            (and
              (RightOf16 ?x_9)
              (LeftOf3 ?x_9)
            )
          )
          (exists (?x_21 )
            (and
              (AboveOf16 ?x_21)
              (BelowOf1 ?x_21)
            )
          )
          (exists (?x_16 )
            (and
              (RightOf10 ?x_16)
              (LeftOf4 ?x_16)
            )
          )
          (exists (?x_16 )
            (and
              (LeftOf2 ?x_16)
              (RightOf9 ?x_16)
            )
          )
          (exists (?x_30 )
            (and
              (AboveOf16 ?x_30)
              (BelowOf3 ?x_30)
            )
          )
          (exists (?x_13 )
            (and
              (RightOf12 ?x_13)
              (LeftOf6 ?x_13)
            )
          )
          (exists (?x_22 )
            (and
              (BelowOf6 ?x_22)
              (AboveOf15 ?x_22)
            )
          )
          (exists (?x_13 )
            (and
              (LeftOf1 ?x_13)
              (RightOf15 ?x_13)
            )
          )
          (exists (?x_1 )
            (and
              (BelowOf7 ?x_1)
              (AboveOf14 ?x_1)
            )
          )
          (exists (?x_26 )
            (and
              (LeftOf5 ?x_26)
              (RightOf15 ?x_26)
            )
          )
          (exists (?x_1 )
            (and
              (BelowOf2 ?x_1)
              (AboveOf15 ?x_1)
            )
          )
          (exists (?x_6 )
            (and
              (AboveOf16 ?x_6)
              (BelowOf4 ?x_6)
            )
          )
          (exists (?x_11 )
            (and
              (BelowOf9 ?x_11)
              (AboveOf13 ?x_11)
            )
          )
          (exists (?x_32 )
            (and
              (RightOf14 ?x_32)
              (LeftOf1 ?x_32)
            )
          )
          (exists (?x_13 )
            (and
              (RightOf13 ?x_13)
              (LeftOf10 ?x_13)
            )
          )
          (exists (?x_32 )
            (and
              (LeftOf9 ?x_32)
              (RightOf15 ?x_32)
            )
          )
          (exists (?x_9 )
            (and
              (RightOf16 ?x_9)
              (LeftOf1 ?x_9)
            )
          )
          (exists (?x_25 )
            (and
              (BelowOf1 ?x_25)
              (AboveOf17 ?x_25)
            )
          )
          (exists (?x_29 )
            (and
              (RightOf14 ?x_29)
              (LeftOf2 ?x_29)
            )
          )
          (exists (?x_31 )
            (and
              (AboveOf15 ?x_31)
              (BelowOf7 ?x_31)
            )
          )
          (exists (?x_5 )
            (and
              (BelowOf3 ?x_5)
              (AboveOf3 ?x_5)
            )
          )
          (exists (?x_12 )
            (and
              (RightOf15 ?x_12)
              (LeftOf1 ?x_12)
            )
          )
          (exists (?x_16 )
            (and
              (RightOf17 ?x_16)
              (LeftOf6 ?x_16)
            )
          )
          (exists (?x_14 )
            (and
              (AboveOf17 ?x_14)
              (BelowOf2 ?x_14)
            )
          )
          (exists (?x_22 )
            (and
              (AboveOf11 ?x_22)
              (BelowOf5 ?x_22)
            )
          )
          (exists (?x_21 )
            (and
              (BelowOf8 ?x_21)
              (AboveOf12 ?x_21)
            )
          )
          (exists (?x_16 )
            (and
              (RightOf6 ?x_16)
              (LeftOf4 ?x_16)
            )
          )
          (exists (?x_31 )
            (and
              (BelowOf7 ?x_31)
              (AboveOf8 ?x_31)
            )
          )
          (exists (?x_21 )
            (and
              (AboveOf10 ?x_21)
              (BelowOf2 ?x_21)
            )
          )
          (exists (?x_31 )
            (and
              (AboveOf10 ?x_31)
              (BelowOf2 ?x_31)
            )
          )
          (exists (?x_18 )
            (and
              (AboveOf15 ?x_18)
              (BelowOf9 ?x_18)
            )
          )
          (exists (?x_6 )
            (and
              (BelowOf1 ?x_6)
              (AboveOf12 ?x_6)
            )
          )
          (exists (?x_5 )
            (and
              (AboveOf6 ?x_5)
              (BelowOf3 ?x_5)
            )
          )
          (exists (?x_15 )
            (and
              (LeftOf3 ?x_15)
              (RightOf17 ?x_15)
            )
          )
          (exists (?x_20 )
            (and
              (AboveOf3 ?x_20)
              (BelowOf2 ?x_20)
            )
          )
          (exists (?x_20 )
            (and
              (AboveOf8 ?x_20)
              (BelowOf2 ?x_20)
            )
          )
          (exists (?x_33 )
            (and
              (RightOf8 ?x_33)
              (LeftOf4 ?x_33)
            )
          )
          (exists (?x_31 )
            (and
              (BelowOf1 ?x_31)
              (AboveOf14 ?x_31)
            )
          )
          (exists (?x_20 )
            (and
              (AboveOf9 ?x_20)
              (BelowOf2 ?x_20)
            )
          )
          (exists (?x_30 )
            (and
              (AboveOf5 ?x_30)
              (BelowOf3 ?x_30)
            )
          )
          (exists (?x_6 )
            (and
              (AboveOf14 ?x_6)
              (BelowOf4 ?x_6)
            )
          )
          (exists (?x_6 )
            (and
              (AboveOf8 ?x_6)
              (BelowOf6 ?x_6)
            )
          )
          (exists (?x_10 )
            (and
              (RightOf13 ?x_10)
              (LeftOf6 ?x_10)
            )
          )
          (exists (?x_3 )
            (and
              (RightOf10 ?x_3)
              (LeftOf2 ?x_3)
            )
          )
          (exists (?x_11 )
            (and
              (BelowOf8 ?x_11)
              (AboveOf16 ?x_11)
            )
          )
          (exists (?x_23 )
            (and
              (AboveOf16 ?x_23)
              (BelowOf1 ?x_23)
            )
          )
          (exists (?x_6 )
            (and
              (AboveOf9 ?x_6)
              (BelowOf6 ?x_6)
            )
          )
          (exists (?x_1 )
            (and
              (AboveOf15 ?x_1)
              (BelowOf3 ?x_1)
            )
          )
          (exists (?x_10 )
            (and
              (RightOf17 ?x_10)
              (LeftOf6 ?x_10)
            )
          )
          (exists (?x_9 )
            (and
              (LeftOf4 ?x_9)
              (RightOf10 ?x_9)
            )
          )
          (exists (?x_6 )
            (and
              (AboveOf14 ?x_6)
              (BelowOf6 ?x_6)
            )
          )
          (exists (?x_18 )
            (and
              (AboveOf16 ?x_18)
              (BelowOf10 ?x_18)
            )
          )
          (exists (?x_8 )
            (and
              (RightOf16 ?x_8)
              (LeftOf13 ?x_8)
            )
          )
          (exists (?x_9 )
            (and
              (RightOf16 ?x_9)
              (LeftOf6 ?x_9)
            )
          )
          (exists (?x_1 )
            (and
              (AboveOf11 ?x_1)
              (BelowOf7 ?x_1)
            )
          )
          (exists (?x_0 )
            (and
              (RightOf16 ?x_0)
              (LeftOf1 ?x_0)
            )
          )
          (exists (?x_10 )
            (and
              (LeftOf6 ?x_10)
              (RightOf14 ?x_10)
            )
          )
          (exists (?x_5 )
            (and
              (BelowOf3 ?x_5)
              (AboveOf13 ?x_5)
            )
          )
          (exists (?x_18 )
            (and
              (AboveOf14 ?x_18)
              (BelowOf10 ?x_18)
            )
          )
          (exists (?x_31 )
            (and
              (AboveOf8 ?x_31)
              (BelowOf6 ?x_31)
            )
          )
          (exists (?x_21 )
            (and
              (BelowOf8 ?x_21)
              (AboveOf17 ?x_21)
            )
          )
          (exists (?x_15 )
            (and
              (RightOf11 ?x_15)
              (LeftOf4 ?x_15)
            )
          )
          (exists (?x_9 )
            (and
              (LeftOf8 ?x_9)
              (RightOf12 ?x_9)
            )
          )
          (exists (?x_28 )
            (and
              (RightOf7 ?x_28)
              (LeftOf4 ?x_28)
            )
          )
          (exists (?x_9 )
            (and
              (RightOf16 ?x_9)
              (LeftOf2 ?x_9)
            )
          )
          (exists (?x_28 )
            (and
              (RightOf6 ?x_28)
              (LeftOf3 ?x_28)
            )
          )
          (exists (?x_16 )
            (and
              (LeftOf6 ?x_16)
              (RightOf14 ?x_16)
            )
          )
          (exists (?x_24 )
            (and
              (RightOf7 ?x_24)
              (LeftOf1 ?x_24)
            )
          )
          (exists (?x_9 )
            (and
              (RightOf17 ?x_9)
              (LeftOf5 ?x_9)
            )
          )
          (exists (?x_18 )
            (and
              (BelowOf5 ?x_18)
              (AboveOf12 ?x_18)
            )
          )
          (exists (?x_33 )
            (and
              (LeftOf3 ?x_33)
              (RightOf5 ?x_33)
            )
          )
          (exists (?x_1 )
            (and
              (BelowOf3 ?x_1)
              (AboveOf15 ?x_1)
            )
          )
          (exists (?x_33 )
            (and
              (LeftOf5 ?x_33)
              (RightOf12 ?x_33)
            )
          )
          (exists (?x_25 )
            (and
              (AboveOf12 ?x_25)
              (BelowOf5 ?x_25)
            )
          )
          (exists (?x_14 )
            (and
              (BelowOf1 ?x_14)
              (AboveOf17 ?x_14)
            )
          )
          (exists (?x_25 )
            (and
              (BelowOf4 ?x_25)
              (AboveOf15 ?x_25)
            )
          )
          (exists (?x_29 )
            (and
              (LeftOf5 ?x_29)
              (RightOf15 ?x_29)
            )
          )
          (exists (?x_20 )
            (and
              (AboveOf3 ?x_20)
              (BelowOf1 ?x_20)
            )
          )
          (exists (?x_12 )
            (and
              (RightOf16 ?x_12)
              (LeftOf2 ?x_12)
            )
          )
          (exists (?x_6 )
            (and
              (AboveOf17 ?x_6)
              (BelowOf3 ?x_6)
            )
          )
          (exists (?x_24 )
            (and
              (RightOf17 ?x_24)
              (LeftOf2 ?x_24)
            )
          )
          (exists (?x_21 )
            (and
              (AboveOf8 ?x_21)
              (BelowOf1 ?x_21)
            )
          )
          (exists (?x_27 )
            (and
              (AboveOf15 ?x_27)
              (BelowOf5 ?x_27)
            )
          )
          (exists (?x_14 )
            (and
              (AboveOf12 ?x_14)
              (BelowOf7 ?x_14)
            )
          )
          (exists (?x_31 )
            (and
              (AboveOf10 ?x_31)
              (BelowOf5 ?x_31)
            )
          )
          (exists (?x_3 )
            (and
              (LeftOf1 ?x_3)
              (RightOf16 ?x_3)
            )
          )
          (exists (?x_32 )
            (and
              (LeftOf10 ?x_32)
              (RightOf17 ?x_32)
            )
          )
          (exists (?x_11 )
            (and
              (BelowOf3 ?x_11)
              (AboveOf15 ?x_11)
            )
          )
          (exists (?x_12 )
            (and
              (LeftOf9 ?x_12)
              (RightOf16 ?x_12)
            )
          )
          (exists (?x_20 )
            (and
              (AboveOf4 ?x_20)
              (BelowOf2 ?x_20)
            )
          )
          (exists (?x_13 )
            (and
              (RightOf16 ?x_13)
              (LeftOf6 ?x_13)
            )
          )
          (exists (?x_23 )
            (and
              (BelowOf5 ?x_23)
              (AboveOf17 ?x_23)
            )
          )
          (exists (?x_9 )
            (and
              (RightOf10 ?x_9)
              (LeftOf2 ?x_9)
            )
          )
          (exists (?x_9 )
            (and
              (LeftOf1 ?x_9)
              (RightOf11 ?x_9)
            )
          )
          (exists (?x_6 )
            (and
              (AboveOf15 ?x_6)
              (BelowOf4 ?x_6)
            )
          )
          (exists (?x_26 )
            (and
              (LeftOf15 ?x_26)
              (RightOf16 ?x_26)
            )
          )
          (exists (?x_19 )
            (and
              (LeftOf15 ?x_19)
              (RightOf17 ?x_19)
            )
          )
          (exists (?x_5 )
            (and
              (BelowOf1 ?x_5)
              (AboveOf11 ?x_5)
            )
          )
          (exists (?x_16 )
            (and
              (RightOf10 ?x_16)
              (LeftOf6 ?x_16)
            )
          )
          (exists (?x_11 )
            (and
              (BelowOf8 ?x_11)
              (AboveOf15 ?x_11)
            )
          )
          (exists (?x_3 )
            (and
              (LeftOf1 ?x_3)
              (RightOf2 ?x_3)
            )
          )
          (exists (?x_10 )
            (and
              (RightOf7 ?x_10)
              (LeftOf6 ?x_10)
            )
          )
          (exists (?x_0 )
            (and
              (RightOf13 ?x_0)
              (LeftOf1 ?x_0)
            )
          )
          (exists (?x_1 )
            (and
              (BelowOf6 ?x_1)
              (AboveOf17 ?x_1)
            )
          )
          (exists (?x_24 )
            (and
              (RightOf3 ?x_24)
              (LeftOf1 ?x_24)
            )
          )
          (exists (?x_31 )
            (and
              (BelowOf2 ?x_31)
              (AboveOf15 ?x_31)
            )
          )
          (exists (?x_11 )
            (and
              (AboveOf17 ?x_11)
              (BelowOf9 ?x_11)
            )
          )
          (exists (?x_15 )
            (and
              (LeftOf1 ?x_15)
              (RightOf10 ?x_15)
            )
          )
          (exists (?x_30 )
            (and
              (BelowOf2 ?x_30)
              (AboveOf17 ?x_30)
            )
          )
          (exists (?x_0 )
            (and
              (RightOf14 ?x_0)
              (LeftOf13 ?x_0)
            )
          )
          (exists (?x_13 )
            (and
              (RightOf13 ?x_13)
              (LeftOf3 ?x_13)
            )
          )
          (exists (?x_32 )
            (and
              (RightOf15 ?x_32)
              (LeftOf11 ?x_32)
            )
          )
          (exists (?x_25 )
            (and
              (AboveOf15 ?x_25)
              (BelowOf5 ?x_25)
            )
          )
          (exists (?x_15 )
            (and
              (LeftOf3 ?x_15)
              (RightOf12 ?x_15)
            )
          )
          (exists (?x_15 )
            (and
              (LeftOf2 ?x_15)
              (RightOf12 ?x_15)
            )
          )
          (exists (?x_33 )
            (and
              (RightOf9 ?x_33)
              (LeftOf1 ?x_33)
            )
          )
          (exists (?x_25 )
            (and
              (AboveOf16 ?x_25)
              (BelowOf2 ?x_25)
            )
          )
          (exists (?x_33 )
            (and
              (LeftOf5 ?x_33)
              (RightOf13 ?x_33)
            )
          )
          (exists (?x_22 )
            (and
              (AboveOf17 ?x_22)
              (BelowOf4 ?x_22)
            )
          )
          (exists (?x_32 )
            (and
              (RightOf17 ?x_32)
              (LeftOf7 ?x_32)
            )
          )
          (exists (?x_11 )
            (and
              (AboveOf17 ?x_11)
              (BelowOf13 ?x_11)
            )
          )
          (exists (?x_16 )
            (and
              (RightOf12 ?x_16)
              (LeftOf3 ?x_16)
            )
          )
          (exists (?x_6 )
            (and
              (BelowOf2 ?x_6)
              (AboveOf17 ?x_6)
            )
          )
          (exists (?x_32 )
            (and
              (RightOf14 ?x_32)
              (LeftOf5 ?x_32)
            )
          )
          (exists (?x_22 )
            (and
              (BelowOf6 ?x_22)
              (AboveOf12 ?x_22)
            )
          )
          (exists (?x_9 )
            (and
              (LeftOf4 ?x_9)
              (RightOf12 ?x_9)
            )
          )
          (exists (?x_14 )
            (and
              (BelowOf9 ?x_14)
              (AboveOf11 ?x_14)
            )
          )
          (exists (?x_17 )
            (and
              (AboveOf16 ?x_17)
              (BelowOf1 ?x_17)
            )
          )
          (exists (?x_21 )
            (and
              (BelowOf1 ?x_21)
              (AboveOf15 ?x_21)
            )
          )
          (exists (?x_12 )
            (and
              (LeftOf10 ?x_12)
              (RightOf14 ?x_12)
            )
          )
          (exists (?x_13 )
            (and
              (LeftOf9 ?x_13)
              (RightOf14 ?x_13)
            )
          )
          (exists (?x_18 )
            (and
              (BelowOf3 ?x_18)
              (AboveOf12 ?x_18)
            )
          )
          (exists (?x_4 )
            (and
              (BelowOf15 ?x_4)
              (AboveOf17 ?x_4)
            )
          )
          (exists (?x_22 )
            (and
              (BelowOf6 ?x_22)
              (AboveOf17 ?x_22)
            )
          )
          (exists (?x_30 )
            (and
              (BelowOf1 ?x_30)
              (AboveOf5 ?x_30)
            )
          )
          (exists (?x_1 )
            (and
              (AboveOf10 ?x_1)
              (BelowOf4 ?x_1)
            )
          )
          (exists (?x_18 )
            (and
              (BelowOf7 ?x_18)
              (AboveOf15 ?x_18)
            )
          )
          (exists (?x_23 )
            (and
              (BelowOf11 ?x_23)
              (AboveOf16 ?x_23)
            )
          )
          (exists (?x_6 )
            (and
              (AboveOf7 ?x_6)
              (BelowOf4 ?x_6)
            )
          )
          (exists (?x_5 )
            (and
              (AboveOf3 ?x_5)
              (BelowOf2 ?x_5)
            )
          )
          (exists (?x_28 )
            (and
              (RightOf6 ?x_28)
              (LeftOf2 ?x_28)
            )
          )
          (exists (?x_12 )
            (and
              (LeftOf1 ?x_12)
              (RightOf16 ?x_12)
            )
          )
          (exists (?x_27 )
            (and
              (BelowOf7 ?x_27)
              (AboveOf17 ?x_27)
            )
          )
          (exists (?x_15 )
            (and
              (LeftOf9 ?x_15)
              (RightOf16 ?x_15)
            )
          )
          (exists (?x_25 )
            (and
              (AboveOf12 ?x_25)
              (BelowOf3 ?x_25)
            )
          )
          (exists (?x_23 )
            (and
              (AboveOf14 ?x_23)
              (BelowOf6 ?x_23)
            )
          )
          (exists (?x_24 )
            (and
              (RightOf6 ?x_24)
              (LeftOf2 ?x_24)
            )
          )
          (exists (?x_15 )
            (and
              (RightOf11 ?x_15)
              (LeftOf6 ?x_15)
            )
          )
          (exists (?x_25 )
            (and
              (BelowOf1 ?x_25)
              (AboveOf8 ?x_25)
            )
          )
          (exists (?x_30 )
            (and
              (BelowOf2 ?x_30)
              (AboveOf8 ?x_30)
            )
          )
          (exists (?x_13 )
            (and
              (RightOf15 ?x_13)
              (LeftOf5 ?x_13)
            )
          )
          (exists (?x_29 )
            (and
              (RightOf14 ?x_29)
              (LeftOf5 ?x_29)
            )
          )
          (exists (?x_24 )
            (and
              (RightOf14 ?x_24)
              (LeftOf1 ?x_24)
            )
          )
          (exists (?x_10 )
            (and
              (RightOf7 ?x_10)
              (LeftOf3 ?x_10)
            )
          )
          (exists (?x_15 )
            (and
              (RightOf14 ?x_15)
              (LeftOf3 ?x_15)
            )
          )
          (exists (?x_32 )
            (and
              (RightOf14 ?x_32)
              (LeftOf10 ?x_32)
            )
          )
          (exists (?x_23 )
            (and
              (BelowOf3 ?x_23)
              (AboveOf16 ?x_23)
            )
          )
          (exists (?x_17 )
            (and
              (BelowOf1 ?x_17)
              (AboveOf3 ?x_17)
            )
          )
          (exists (?x_29 )
            (and
              (RightOf16 ?x_29)
              (LeftOf9 ?x_29)
            )
          )
          (exists (?x_27 )
            (and
              (BelowOf2 ?x_27)
              (AboveOf16 ?x_27)
            )
          )
          (exists (?x_13 )
            (and
              (LeftOf9 ?x_13)
              (RightOf11 ?x_13)
            )
          )
          (exists (?x_7 )
            (and
              (AboveOf16 ?x_7)
              (BelowOf7 ?x_7)
            )
          )
          (exists (?x_12 )
            (and
              (RightOf15 ?x_12)
              (LeftOf9 ?x_12)
            )
          )
          (exists (?x_6 )
            (and
              (BelowOf5 ?x_6)
              (AboveOf15 ?x_6)
            )
          )
          (exists (?x_33 )
            (and
              (LeftOf5 ?x_33)
              (RightOf17 ?x_33)
            )
          )
          (exists (?x_26 )
            (and
              (RightOf15 ?x_26)
              (LeftOf12 ?x_26)
            )
          )
          (exists (?x_28 )
            (and
              (LeftOf3 ?x_28)
              (RightOf12 ?x_28)
            )
          )
          (exists (?x_32 )
            (and
              (RightOf12 ?x_32)
              (LeftOf1 ?x_32)
            )
          )
          (exists (?x_1 )
            (and
              (BelowOf10 ?x_1)
              (AboveOf17 ?x_1)
            )
          )
          (exists (?x_14 )
            (and
              (AboveOf17 ?x_14)
              (BelowOf7 ?x_14)
            )
          )
          (exists (?x_33 )
            (and
              (LeftOf3 ?x_33)
              (RightOf15 ?x_33)
            )
          )
          (exists (?x_27 )
            (and
              (BelowOf6 ?x_27)
              (AboveOf17 ?x_27)
            )
          )
          (exists (?x_16 )
            (and
              (RightOf13 ?x_16)
              (LeftOf3 ?x_16)
            )
          )
          (exists (?x_25 )
            (and
              (AboveOf9 ?x_25)
              (BelowOf5 ?x_25)
            )
          )
          (exists (?x_15 )
            (and
              (LeftOf3 ?x_15)
              (RightOf15 ?x_15)
            )
          )
          (exists (?x_30 )
            (and
              (BelowOf1 ?x_30)
              (AboveOf12 ?x_30)
            )
          )
          (exists (?x_31 )
            (and
              (BelowOf6 ?x_31)
              (AboveOf14 ?x_31)
            )
          )
          (exists (?x_14 )
            (and
              (BelowOf1 ?x_14)
              (AboveOf12 ?x_14)
            )
          )
          (exists (?x_25 )
            (and
              (AboveOf9 ?x_25)
              (BelowOf2 ?x_25)
            )
          )
          (exists (?x_11 )
            (and
              (BelowOf4 ?x_11)
              (AboveOf15 ?x_11)
            )
          )
          (exists (?x_24 )
            (and
              (LeftOf3 ?x_24)
              (RightOf3 ?x_24)
            )
          )
          (exists (?x_9 )
            (and
              (LeftOf3 ?x_9)
              (RightOf11 ?x_9)
            )
          )
          (exists (?x_32 )
            (and
              (LeftOf4 ?x_32)
              (RightOf16 ?x_32)
            )
          )
          (exists (?x_25 )
            (and
              (BelowOf1 ?x_25)
              (AboveOf12 ?x_25)
            )
          )
          (exists (?x_16 )
            (and
              (RightOf6 ?x_16)
              (LeftOf3 ?x_16)
            )
          )
          (exists (?x_10 )
            (and
              (RightOf15 ?x_10)
              (LeftOf5 ?x_10)
            )
          )
          (exists (?x_22 )
            (and
              (BelowOf2 ?x_22)
              (AboveOf13 ?x_22)
            )
          )
          (exists (?x_11 )
            (and
              (AboveOf14 ?x_11)
              (BelowOf13 ?x_11)
            )
          )
          (exists (?x_24 )
            (and
              (LeftOf3 ?x_24)
              (RightOf11 ?x_24)
            )
          )
          (exists (?x_6 )
            (and
              (AboveOf11 ?x_6)
              (BelowOf4 ?x_6)
            )
          )
          (exists (?x_11 )
            (and
              (AboveOf15 ?x_11)
              (BelowOf2 ?x_11)
            )
          )
          (exists (?x_31 )
            (and
              (AboveOf9 ?x_31)
              (BelowOf2 ?x_31)
            )
          )
          (exists (?x_6 )
            (and
              (AboveOf17 ?x_6)
              (BelowOf6 ?x_6)
            )
          )
          (exists (?x_9 )
            (and
              (LeftOf5 ?x_9)
              (RightOf9 ?x_9)
            )
          )
          (exists (?x_15 )
            (and
              (LeftOf1 ?x_15)
              (RightOf14 ?x_15)
            )
          )
          (exists (?x_31 )
            (and
              (AboveOf12 ?x_31)
              (BelowOf6 ?x_31)
            )
          )
          (exists (?x_15 )
            (and
              (RightOf11 ?x_15)
              (LeftOf7 ?x_15)
            )
          )
          (exists (?x_11 )
            (and
              (BelowOf6 ?x_11)
              (AboveOf13 ?x_11)
            )
          )
          (exists (?x_15 )
            (and
              (RightOf15 ?x_15)
              (LeftOf5 ?x_15)
            )
          )
          (exists (?x_21 )
            (and
              (AboveOf14 ?x_21)
              (BelowOf3 ?x_21)
            )
          )
          (exists (?x_14 )
            (and
              (BelowOf5 ?x_14)
              (AboveOf13 ?x_14)
            )
          )
          (exists (?x_31 )
            (and
              (AboveOf7 ?x_31)
              (BelowOf5 ?x_31)
            )
          )
          (exists (?x_33 )
            (and
              (RightOf15 ?x_33)
              (LeftOf2 ?x_33)
            )
          )
          (exists (?x_23 )
            (and
              (BelowOf6 ?x_23)
              (AboveOf16 ?x_23)
            )
          )
          (exists (?x_15 )
            (and
              (LeftOf6 ?x_15)
              (RightOf16 ?x_15)
            )
          )
          (exists (?x_33 )
            (and
              (LeftOf3 ?x_33)
              (RightOf12 ?x_33)
            )
          )
          (exists (?x_30 )
            (and
              (BelowOf4 ?x_30)
              (AboveOf6 ?x_30)
            )
          )
          (exists (?x_6 )
            (and
              (BelowOf5 ?x_6)
              (AboveOf6 ?x_6)
            )
          )
          (exists (?x_2 )
            (and
              (RightOf16 ?x_2)
              (LeftOf1 ?x_2)
            )
          )
          (exists (?x_25 )
            (and
              (AboveOf13 ?x_25)
              (BelowOf2 ?x_25)
            )
          )
          (exists (?x_13 )
            (and
              (RightOf10 ?x_13)
              (LeftOf9 ?x_13)
            )
          )
          (exists (?x_8 )
            (and
              (LeftOf11 ?x_8)
              (RightOf16 ?x_8)
            )
          )
          (exists (?x_5 )
            (and
              (AboveOf5 ?x_5)
              (BelowOf3 ?x_5)
            )
          )
          (exists (?x_33 )
            (and
              (LeftOf1 ?x_33)
              (RightOf8 ?x_33)
            )
          )
          (exists (?x_0 )
            (and
              (RightOf15 ?x_0)
              (LeftOf8 ?x_0)
            )
          )
          (exists (?x_22 )
            (and
              (BelowOf8 ?x_22)
              (AboveOf16 ?x_22)
            )
          )
          (exists (?x_32 )
            (and
              (RightOf14 ?x_32)
              (LeftOf7 ?x_32)
            )
          )
          (exists (?x_2 )
            (and
              (LeftOf1 ?x_2)
              (RightOf3 ?x_2)
            )
          )
          (exists (?x_23 )
            (and
              (BelowOf2 ?x_23)
              (AboveOf15 ?x_23)
            )
          )
          (exists (?x_26 )
            (and
              (RightOf17 ?x_26)
              (LeftOf12 ?x_26)
            )
          )
          (exists (?x_7 )
            (and
              (BelowOf14 ?x_7)
              (AboveOf17 ?x_7)
            )
          )
          (exists (?x_21 )
            (and
              (AboveOf12 ?x_21)
              (BelowOf6 ?x_21)
            )
          )
          (exists (?x_23 )
            (and
              (AboveOf14 ?x_23)
              (BelowOf7 ?x_23)
            )
          )
          (exists (?x_7 )
            (and
              (BelowOf2 ?x_7)
              (AboveOf17 ?x_7)
            )
          )
          (exists (?x_10 )
            (and
              (LeftOf4 ?x_10)
              (RightOf12 ?x_10)
            )
          )
          (exists (?x_7 )
            (and
              (AboveOf16 ?x_7)
              (BelowOf12 ?x_7)
            )
          )
          (exists (?x_16 )
            (and
              (RightOf17 ?x_16)
              (LeftOf1 ?x_16)
            )
          )
          (exists (?x_11 )
            (and
              (AboveOf16 ?x_11)
              (BelowOf2 ?x_11)
            )
          )
          (exists (?x_11 )
            (and
              (AboveOf17 ?x_11)
              (BelowOf10 ?x_11)
            )
          )
          (exists (?x_16 )
            (and
              (LeftOf3 ?x_16)
              (RightOf8 ?x_16)
            )
          )
          (exists (?x_22 )
            (and
              (AboveOf17 ?x_22)
              (BelowOf9 ?x_22)
            )
          )
          (exists (?x_28 )
            (and
              (RightOf5 ?x_28)
              (LeftOf4 ?x_28)
            )
          )
          (exists (?x_11 )
            (and
              (BelowOf6 ?x_11)
              (AboveOf15 ?x_11)
            )
          )
          (exists (?x_6 )
            (and
              (BelowOf2 ?x_6)
              (AboveOf15 ?x_6)
            )
          )
          (exists (?x_7 )
            (and
              (BelowOf7 ?x_7)
              (AboveOf17 ?x_7)
            )
          )
          (exists (?x_15 )
            (and
              (RightOf13 ?x_15)
              (LeftOf5 ?x_15)
            )
          )
          (exists (?x_23 )
            (and
              (BelowOf8 ?x_23)
              (AboveOf17 ?x_23)
            )
          )
          (exists (?x_16 )
            (and
              (LeftOf4 ?x_16)
              (RightOf11 ?x_16)
            )
          )
          (exists (?x_6 )
            (and
              (BelowOf1 ?x_6)
              (AboveOf16 ?x_6)
            )
          )
          (exists (?x_13 )
            (and
              (LeftOf7 ?x_13)
              (RightOf12 ?x_13)
            )
          )
          (exists (?x_33 )
            (and
              (RightOf17 ?x_33)
              (LeftOf3 ?x_33)
            )
          )
          (exists (?x_9 )
            (and
              (RightOf8 ?x_9)
              (LeftOf6 ?x_9)
            )
          )
          (exists (?x_6 )
            (and
              (BelowOf2 ?x_6)
              (AboveOf11 ?x_6)
            )
          )
          (exists (?x_10 )
            (and
              (LeftOf3 ?x_10)
              (RightOf9 ?x_10)
            )
          )
          (exists (?x_1 )
            (and
              (AboveOf13 ?x_1)
              (BelowOf3 ?x_1)
            )
          )
          (exists (?x_7 )
            (and
              (BelowOf15 ?x_7)
              (AboveOf17 ?x_7)
            )
          )
          (exists (?x_1 )
            (and
              (BelowOf1 ?x_1)
              (AboveOf12 ?x_1)
            )
          )
          (exists (?x_16 )
            (and
              (LeftOf5 ?x_16)
              (RightOf9 ?x_16)
            )
          )
          (exists (?x_23 )
            (and
              (AboveOf16 ?x_23)
              (BelowOf4 ?x_23)
            )
          )
          (exists (?x_1 )
            (and
              (BelowOf8 ?x_1)
              (AboveOf15 ?x_1)
            )
          )
          (exists (?x_18 )
            (and
              (AboveOf13 ?x_18)
              (BelowOf9 ?x_18)
            )
          )
          (exists (?x_18 )
            (and
              (BelowOf6 ?x_18)
              (AboveOf12 ?x_18)
            )
          )
          (exists (?x_18 )
            (and
              (AboveOf16 ?x_18)
              (BelowOf12 ?x_18)
            )
          )
          (exists (?x_31 )
            (and
              (AboveOf8 ?x_31)
              (BelowOf3 ?x_31)
            )
          )
          (exists (?x_10 )
            (and
              (RightOf8 ?x_10)
              (LeftOf3 ?x_10)
            )
          )
          (exists (?x_32 )
            (and
              (RightOf14 ?x_32)
              (LeftOf6 ?x_32)
            )
          )
          (exists (?x_11 )
            (and
              (BelowOf12 ?x_11)
              (AboveOf16 ?x_11)
            )
          )
          (exists (?x_12 )
            (and
              (LeftOf2 ?x_12)
              (RightOf14 ?x_12)
            )
          )
          (exists (?x_22 )
            (and
              (BelowOf5 ?x_22)
              (AboveOf17 ?x_22)
            )
          )
          (exists (?x_15 )
            (and
              (LeftOf8 ?x_15)
              (RightOf10 ?x_15)
            )
          )
          (exists (?x_12 )
            (and
              (LeftOf1 ?x_12)
              (RightOf17 ?x_12)
            )
          )
          (exists (?x_6 )
            (and
              (BelowOf6 ?x_6)
              (AboveOf15 ?x_6)
            )
          )
          (exists (?x_29 )
            (and
              (LeftOf4 ?x_29)
              (RightOf12 ?x_29)
            )
          )
          (exists (?x_14 )
            (and
              (AboveOf10 ?x_14)
              (BelowOf7 ?x_14)
            )
          )
          (exists (?x_17 )
            (and
              (AboveOf17 ?x_17)
              (BelowOf1 ?x_17)
            )
          )
          (exists (?x_14 )
            (and
              (BelowOf4 ?x_14)
              (AboveOf14 ?x_14)
            )
          )
          (exists (?x_0 )
            (and
              (RightOf15 ?x_0)
              (LeftOf2 ?x_0)
            )
          )
          (exists (?x_25 )
            (and
              (BelowOf4 ?x_25)
              (AboveOf11 ?x_25)
            )
          )
          (exists (?x_10 )
            (and
              (RightOf10 ?x_10)
              (LeftOf6 ?x_10)
            )
          )
          (exists (?x_10 )
            (and
              (LeftOf7 ?x_10)
              (RightOf17 ?x_10)
            )
          )
          (exists (?x_32 )
            (and
              (LeftOf8 ?x_32)
              (RightOf17 ?x_32)
            )
          )
          (exists (?x_30 )
            (and
              (AboveOf15 ?x_30)
              (BelowOf3 ?x_30)
            )
          )
          (exists (?x_33 )
            (and
              (RightOf5 ?x_33)
              (LeftOf4 ?x_33)
            )
          )
          (exists (?x_29 )
            (and
              (LeftOf6 ?x_29)
              (RightOf16 ?x_29)
            )
          )
          (exists (?x_16 )
            (and
              (RightOf15 ?x_16)
              (LeftOf3 ?x_16)
            )
          )
          (exists (?x_20 )
            (and
              (BelowOf2 ?x_20)
              (AboveOf10 ?x_20)
            )
          )
          (exists (?x_0 )
            (and
              (RightOf15 ?x_0)
              (LeftOf5 ?x_0)
            )
          )
          (exists (?x_1 )
            (and
              (BelowOf10 ?x_1)
              (AboveOf13 ?x_1)
            )
          )
          (exists (?x_29 )
            (and
              (RightOf16 ?x_29)
              (LeftOf8 ?x_29)
            )
          )
          (exists (?x_32 )
            (and
              (RightOf14 ?x_32)
              (LeftOf11 ?x_32)
            )
          )
          (exists (?x_18 )
            (and
              (BelowOf7 ?x_18)
              (AboveOf14 ?x_18)
            )
          )
          (exists (?x_24 )
            (and
              (RightOf4 ?x_24)
              (LeftOf1 ?x_24)
            )
          )
          (exists (?x_12 )
            (and
              (LeftOf4 ?x_12)
              (RightOf16 ?x_12)
            )
          )
          (exists (?x_28 )
            (and
              (RightOf17 ?x_28)
              (LeftOf1 ?x_28)
            )
          )
          (exists (?x_4 )
            (and
              (BelowOf2 ?x_4)
              (AboveOf17 ?x_4)
            )
          )
          (exists (?x_16 )
            (and
              (LeftOf6 ?x_16)
              (RightOf11 ?x_16)
            )
          )
          (exists (?x_19 )
            (and
              (RightOf17 ?x_19)
              (LeftOf11 ?x_19)
            )
          )
          (exists (?x_29 )
            (and
              (RightOf16 ?x_29)
              (LeftOf11 ?x_29)
            )
          )
          (exists (?x_21 )
            (and
              (BelowOf3 ?x_21)
              (AboveOf13 ?x_21)
            )
          )
          (exists (?x_9 )
            (and
              (LeftOf5 ?x_9)
              (RightOf13 ?x_9)
            )
          )
          (exists (?x_16 )
            (and
              (RightOf7 ?x_16)
              (LeftOf3 ?x_16)
            )
          )
          (exists (?x_25 )
            (and
              (BelowOf4 ?x_25)
              (AboveOf16 ?x_25)
            )
          )
          (exists (?x_24 )
            (and
              (RightOf15 ?x_24)
              (LeftOf1 ?x_24)
            )
          )
          (exists (?x_0 )
            (and
              (LeftOf4 ?x_0)
              (RightOf14 ?x_0)
            )
          )
          (exists (?x_5 )
            (and
              (BelowOf2 ?x_5)
              (AboveOf11 ?x_5)
            )
          )
          (exists (?x_33 )
            (and
              (RightOf12 ?x_33)
              (LeftOf1 ?x_33)
            )
          )
          (exists (?x_21 )
            (and
              (AboveOf10 ?x_21)
              (BelowOf7 ?x_21)
            )
          )
          (exists (?x_14 )
            (and
              (AboveOf11 ?x_14)
              (BelowOf8 ?x_14)
            )
          )
          (exists (?x_1 )
            (and
              (AboveOf10 ?x_1)
              (BelowOf10 ?x_1)
            )
          )
          (exists (?x_5 )
            (and
              (AboveOf4 ?x_5)
              (BelowOf1 ?x_5)
            )
          )
          (exists (?x_10 )
            (and
              (LeftOf6 ?x_10)
              (RightOf12 ?x_10)
            )
          )
          (exists (?x_12 )
            (and
              (LeftOf11 ?x_12)
              (RightOf15 ?x_12)
            )
          )
          (exists (?x_26 )
            (and
              (RightOf16 ?x_26)
              (LeftOf12 ?x_26)
            )
          )
          (exists (?x_12 )
            (and
              (LeftOf5 ?x_12)
              (RightOf14 ?x_12)
            )
          )
          (exists (?x_10 )
            (and
              (LeftOf4 ?x_10)
              (RightOf7 ?x_10)
            )
          )
          (exists (?x_6 )
            (and
              (BelowOf1 ?x_6)
              (AboveOf6 ?x_6)
            )
          )
          (exists (?x_29 )
            (and
              (LeftOf5 ?x_29)
              (RightOf13 ?x_29)
            )
          )
          (exists (?x_26 )
            (and
              (RightOf15 ?x_26)
              (LeftOf7 ?x_26)
            )
          )
          (exists (?x_21 )
            (and
              (BelowOf6 ?x_21)
              (AboveOf14 ?x_21)
            )
          )
          (exists (?x_8 )
            (and
              (RightOf16 ?x_8)
              (LeftOf2 ?x_8)
            )
          )
          (exists (?x_16 )
            (and
              (RightOf15 ?x_16)
              (LeftOf1 ?x_16)
            )
          )
          (exists (?x_26 )
            (and
              (RightOf17 ?x_26)
              (LeftOf4 ?x_26)
            )
          )
          (exists (?x_29 )
            (and
              (LeftOf10 ?x_29)
              (RightOf14 ?x_29)
            )
          )
          (exists (?x_32 )
            (and
              (LeftOf3 ?x_32)
              (RightOf17 ?x_32)
            )
          )
          (exists (?x_9 )
            (and
              (LeftOf2 ?x_9)
              (RightOf11 ?x_9)
            )
          )
          (exists (?x_21 )
            (and
              (AboveOf14 ?x_21)
              (BelowOf1 ?x_21)
            )
          )
          (exists (?x_16 )
            (and
              (LeftOf2 ?x_16)
              (RightOf8 ?x_16)
            )
          )
          (exists (?x_13 )
            (and
              (LeftOf8 ?x_13)
              (RightOf12 ?x_13)
            )
          )
          (exists (?x_27 )
            (and
              (BelowOf11 ?x_27)
              (AboveOf17 ?x_27)
            )
          )
          (exists (?x_11 )
            (and
              (AboveOf14 ?x_11)
              (BelowOf2 ?x_11)
            )
          )
          (exists (?x_14 )
            (and
              (BelowOf2 ?x_14)
              (AboveOf16 ?x_14)
            )
          )
          (exists (?x_32 )
            (and
              (LeftOf10 ?x_32)
              (RightOf13 ?x_32)
            )
          )
          (exists (?x_10 )
            (and
              (LeftOf7 ?x_10)
              (RightOf10 ?x_10)
            )
          )
          (exists (?x_18 )
            (and
              (BelowOf2 ?x_18)
              (AboveOf13 ?x_18)
            )
          )
          (exists (?x_26 )
            (and
              (LeftOf6 ?x_26)
              (RightOf16 ?x_26)
            )
          )
          (exists (?x_5 )
            (and
              (AboveOf12 ?x_5)
              (BelowOf1 ?x_5)
            )
          )
          (exists (?x_6 )
            (and
              (AboveOf17 ?x_6)
              (BelowOf4 ?x_6)
            )
          )
          (exists (?x_11 )
            (and
              (AboveOf13 ?x_11)
              (BelowOf4 ?x_11)
            )
          )
          (exists (?x_15 )
            (and
              (RightOf9 ?x_15)
              (LeftOf7 ?x_15)
            )
          )
          (exists (?x_31 )
            (and
              (AboveOf17 ?x_31)
              (BelowOf6 ?x_31)
            )
          )
          (exists (?x_0 )
            (and
              (RightOf13 ?x_0)
              (LeftOf2 ?x_0)
            )
          )
          (exists (?x_12 )
            (and
              (LeftOf5 ?x_12)
              (RightOf17 ?x_12)
            )
          )
          (exists (?x_1 )
            (and
              (AboveOf10 ?x_1)
              (BelowOf8 ?x_1)
            )
          )
          (exists (?x_25 )
            (and
              (BelowOf5 ?x_25)
              (AboveOf11 ?x_25)
            )
          )
          (exists (?x_13 )
            (and
              (LeftOf7 ?x_13)
              (RightOf14 ?x_13)
            )
          )
          (exists (?x_20 )
            (and
              (AboveOf17 ?x_20)
              (BelowOf1 ?x_20)
            )
          )
          (exists (?x_30 )
            (and
              (BelowOf2 ?x_30)
              (AboveOf12 ?x_30)
            )
          )
          (exists (?x_25 )
            (and
              (BelowOf2 ?x_25)
              (AboveOf12 ?x_25)
            )
          )
          (exists (?x_14 )
            (and
              (BelowOf6 ?x_14)
              (AboveOf16 ?x_14)
            )
          )
          (exists (?x_30 )
            (and
              (BelowOf1 ?x_30)
              (AboveOf10 ?x_30)
            )
          )
          (exists (?x_31 )
            (and
              (AboveOf14 ?x_31)
              (BelowOf6 ?x_31)
            )
          )
          (exists (?x_20 )
            (and
              (BelowOf1 ?x_20)
              (AboveOf9 ?x_20)
            )
          )
          (exists (?x_11 )
            (and
              (AboveOf17 ?x_11)
              (BelowOf4 ?x_11)
            )
          )
          (exists (?x_27 )
            (and
              (AboveOf16 ?x_27)
              (BelowOf1 ?x_27)
            )
          )
          (exists (?x_15 )
            (and
              (RightOf14 ?x_15)
              (LeftOf7 ?x_15)
            )
          )
          (exists (?x_14 )
            (and
              (BelowOf3 ?x_14)
              (AboveOf10 ?x_14)
            )
          )
          (exists (?x_16 )
            (and
              (RightOf9 ?x_16)
              (LeftOf6 ?x_16)
            )
          )
          (exists (?x_26 )
            (and
              (LeftOf1 ?x_26)
              (RightOf16 ?x_26)
            )
          )
          (exists (?x_30 )
            (and
              (BelowOf4 ?x_30)
              (AboveOf5 ?x_30)
            )
          )
          (exists (?x_5 )
            (and
              (BelowOf1 ?x_5)
              (AboveOf8 ?x_5)
            )
          )
          (exists (?x_2 )
            (and
              (LeftOf1 ?x_2)
              (RightOf13 ?x_2)
            )
          )
          (exists (?x_1 )
            (and
              (AboveOf12 ?x_1)
              (BelowOf4 ?x_1)
            )
          )
          (exists (?x_7 )
            (and
              (AboveOf16 ?x_7)
              (BelowOf4 ?x_7)
            )
          )
          (exists (?x_32 )
            (and
              (RightOf17 ?x_32)
              (LeftOf2 ?x_32)
            )
          )
          (exists (?x_6 )
            (and
              (BelowOf2 ?x_6)
              (AboveOf7 ?x_6)
            )
          )
          (exists (?x_33 )
            (and
              (RightOf7 ?x_33)
              (LeftOf3 ?x_33)
            )
          )
          (exists (?x_8 )
            (and
              (RightOf17 ?x_8)
              (LeftOf6 ?x_8)
            )
          )
          (exists (?x_14 )
            (and
              (BelowOf5 ?x_14)
              (AboveOf16 ?x_14)
            )
          )
          (exists (?x_0 )
            (and
              (LeftOf13 ?x_0)
              (RightOf15 ?x_0)
            )
          )
          (exists (?x_9 )
            (and
              (LeftOf6 ?x_9)
              (RightOf11 ?x_9)
            )
          )
          (exists (?x_29 )
            (and
              (LeftOf11 ?x_29)
              (RightOf12 ?x_29)
            )
          )
          (exists (?x_26 )
            (and
              (LeftOf6 ?x_26)
              (RightOf15 ?x_26)
            )
          )
          (exists (?x_25 )
            (and
              (BelowOf4 ?x_25)
              (AboveOf14 ?x_25)
            )
          )
          (exists (?x_23 )
            (and
              (AboveOf17 ?x_23)
              (BelowOf13 ?x_23)
            )
          )
          (exists (?x_7 )
            (and
              (BelowOf10 ?x_7)
              (AboveOf17 ?x_7)
            )
          )
          (exists (?x_32 )
            (and
              (RightOf17 ?x_32)
              (LeftOf1 ?x_32)
            )
          )
          (exists (?x_21 )
            (and
              (BelowOf6 ?x_21)
              (AboveOf13 ?x_21)
            )
          )
          (exists (?x_25 )
            (and
              (BelowOf1 ?x_25)
              (AboveOf11 ?x_25)
            )
          )
          (exists (?x_4 )
            (and
              (BelowOf13 ?x_4)
              (AboveOf17 ?x_4)
            )
          )
          (exists (?x_28 )
            (and
              (LeftOf1 ?x_28)
              (RightOf8 ?x_28)
            )
          )
          (exists (?x_18 )
            (and
              (AboveOf13 ?x_18)
              (BelowOf8 ?x_18)
            )
          )
          (exists (?x_1 )
            (and
              (BelowOf9 ?x_1)
              (AboveOf17 ?x_1)
            )
          )
          (exists (?x_9 )
            (and
              (LeftOf2 ?x_9)
              (RightOf13 ?x_9)
            )
          )
          (exists (?x_1 )
            (and
              (BelowOf1 ?x_1)
              (AboveOf16 ?x_1)
            )
          )
          (exists (?x_15 )
            (and
              (LeftOf1 ?x_15)
              (RightOf13 ?x_15)
            )
          )
          (exists (?x_7 )
            (and
              (AboveOf16 ?x_7)
              (BelowOf6 ?x_7)
            )
          )
          (exists (?x_14 )
            (and
              (BelowOf9 ?x_14)
              (AboveOf14 ?x_14)
            )
          )
          (exists (?x_10 )
            (and
              (LeftOf2 ?x_10)
              (RightOf10 ?x_10)
            )
          )
          (exists (?x_12 )
            (and
              (LeftOf10 ?x_12)
              (RightOf17 ?x_12)
            )
          )
          (exists (?x_23 )
            (and
              (AboveOf14 ?x_23)
              (BelowOf13 ?x_23)
            )
          )
          (exists (?x_21 )
            (and
              (BelowOf5 ?x_21)
              (AboveOf14 ?x_21)
            )
          )
          (exists (?x_21 )
            (and
              (AboveOf17 ?x_21)
              (BelowOf4 ?x_21)
            )
          )
          (exists (?x_12 )
            (and
              (LeftOf3 ?x_12)
              (RightOf17 ?x_12)
            )
          )
          (exists (?x_14 )
            (and
              (BelowOf4 ?x_14)
              (AboveOf16 ?x_14)
            )
          )
          (exists (?x_1 )
            (and
              (BelowOf10 ?x_1)
              (AboveOf11 ?x_1)
            )
          )
          (exists (?x_16 )
            (and
              (RightOf17 ?x_16)
              (LeftOf5 ?x_16)
            )
          )
          (exists (?x_17 )
            (and
              (BelowOf1 ?x_17)
              (AboveOf6 ?x_17)
            )
          )
          (exists (?x_0 )
            (and
              (RightOf14 ?x_0)
              (LeftOf2 ?x_0)
            )
          )
          (exists (?x_11 )
            (and
              (BelowOf11 ?x_11)
              (AboveOf14 ?x_11)
            )
          )
          (exists (?x_29 )
            (and
              (RightOf11 ?x_29)
              (LeftOf1 ?x_29)
            )
          )
          (exists (?x_11 )
            (and
              (BelowOf12 ?x_11)
              (AboveOf14 ?x_11)
            )
          )
          (exists (?x_31 )
            (and
              (AboveOf13 ?x_31)
              (BelowOf1 ?x_31)
            )
          )
          (exists (?x_21 )
            (and
              (AboveOf15 ?x_21)
              (BelowOf7 ?x_21)
            )
          )
          (exists (?x_20 )
            (and
              (AboveOf12 ?x_20)
              (BelowOf2 ?x_20)
            )
          )
          (exists (?x_29 )
            (and
              (LeftOf10 ?x_29)
              (RightOf15 ?x_29)
            )
          )
          (exists (?x_32 )
            (and
              (LeftOf2 ?x_32)
              (RightOf13 ?x_32)
            )
          )
          (exists (?x_11 )
            (and
              (BelowOf5 ?x_11)
              (AboveOf15 ?x_11)
            )
          )
          (exists (?x_15 )
            (and
              (LeftOf4 ?x_15)
              (RightOf12 ?x_15)
            )
          )
          (exists (?x_15 )
            (and
              (LeftOf4 ?x_15)
              (RightOf17 ?x_15)
            )
          )
          (exists (?x_26 )
            (and
              (LeftOf9 ?x_26)
              (RightOf16 ?x_26)
            )
          )
          (exists (?x_30 )
            (and
              (AboveOf9 ?x_30)
              (BelowOf2 ?x_30)
            )
          )
          (exists (?x_21 )
            (and
              (AboveOf16 ?x_21)
              (BelowOf4 ?x_21)
            )
          )
          (exists (?x_10 )
            (and
              (RightOf8 ?x_10)
              (LeftOf5 ?x_10)
            )
          )
          (exists (?x_9 )
            (and
              (LeftOf5 ?x_9)
              (RightOf11 ?x_9)
            )
          )
          (exists (?x_29 )
            (and
              (RightOf17 ?x_29)
              (LeftOf7 ?x_29)
            )
          )
          (exists (?x_18 )
            (and
              (BelowOf2 ?x_18)
              (AboveOf15 ?x_18)
            )
          )
          (exists (?x_0 )
            (and
              (RightOf14 ?x_0)
              (LeftOf1 ?x_0)
            )
          )
          (exists (?x_21 )
            (and
              (BelowOf8 ?x_21)
              (AboveOf15 ?x_21)
            )
          )
          (exists (?x_25 )
            (and
              (AboveOf17 ?x_25)
              (BelowOf3 ?x_25)
            )
          )
          (exists (?x_18 )
            (and
              (BelowOf8 ?x_18)
              (AboveOf16 ?x_18)
            )
          )
          (exists (?x_31 )
            (and
              (AboveOf16 ?x_31)
              (BelowOf7 ?x_31)
            )
          )
          (exists (?x_2 )
            (and
              (LeftOf1 ?x_2)
              (RightOf15 ?x_2)
            )
          )
          (exists (?x_30 )
            (and
              (BelowOf4 ?x_30)
              (AboveOf16 ?x_30)
            )
          )
          (exists (?x_0 )
            (and
              (LeftOf3 ?x_0)
              (RightOf17 ?x_0)
            )
          )
          (exists (?x_22 )
            (and
              (AboveOf14 ?x_22)
              (BelowOf6 ?x_22)
            )
          )
          (exists (?x_14 )
            (and
              (AboveOf13 ?x_14)
              (BelowOf8 ?x_14)
            )
          )
          (exists (?x_33 )
            (and
              (LeftOf5 ?x_33)
              (RightOf5 ?x_33)
            )
          )
          (exists (?x_29 )
            (and
              (RightOf12 ?x_29)
              (LeftOf9 ?x_29)
            )
          )
          (exists (?x_15 )
            (and
              (RightOf10 ?x_15)
              (LeftOf2 ?x_15)
            )
          )
          (exists (?x_14 )
            (and
              (AboveOf9 ?x_14)
              (BelowOf5 ?x_14)
            )
          )
          (exists (?x_13 )
            (and
              (RightOf15 ?x_13)
              (LeftOf2 ?x_13)
            )
          )
          (exists (?x_14 )
            (and
              (BelowOf2 ?x_14)
              (AboveOf13 ?x_14)
            )
          )
          (exists (?x_14 )
            (and
              (BelowOf9 ?x_14)
              (AboveOf13 ?x_14)
            )
          )
          (exists (?x_15 )
            (and
              (RightOf14 ?x_15)
              (LeftOf4 ?x_15)
            )
          )
          (exists (?x_15 )
            (and
              (RightOf11 ?x_15)
              (LeftOf3 ?x_15)
            )
          )
          (exists (?x_25 )
            (and
              (BelowOf2 ?x_25)
              (AboveOf14 ?x_25)
            )
          )
          (exists (?x_2 )
            (and
              (LeftOf1 ?x_2)
              (RightOf17 ?x_2)
            )
          )
          (exists (?x_22 )
            (and
              (AboveOf15 ?x_22)
              (BelowOf9 ?x_22)
            )
          )
          (exists (?x_12 )
            (and
              (RightOf15 ?x_12)
              (LeftOf11 ?x_12)
            )
          )
          (exists (?x_1 )
            (and
              (AboveOf10 ?x_1)
              (BelowOf6 ?x_1)
            )
          )
          (exists (?x_33 )
            (and
              (RightOf16 ?x_33)
              (LeftOf2 ?x_33)
            )
          )
          (exists (?x_9 )
            (and
              (RightOf17 ?x_9)
              (LeftOf3 ?x_9)
            )
          )
          (exists (?x_6 )
            (and
              (AboveOf16 ?x_6)
              (BelowOf6 ?x_6)
            )
          )
          (exists (?x_5 )
            (and
              (BelowOf3 ?x_5)
              (AboveOf9 ?x_5)
            )
          )
          (exists (?x_10 )
            (and
              (LeftOf5 ?x_10)
              (RightOf11 ?x_10)
            )
          )
          (exists (?x_13 )
            (and
              (RightOf15 ?x_13)
              (LeftOf1 ?x_13)
            )
          )
          (exists (?x_14 )
            (and
              (AboveOf11 ?x_14)
              (BelowOf5 ?x_14)
            )
          )
          (exists (?x_8 )
            (and
              (RightOf17 ?x_8)
              (LeftOf16 ?x_8)
            )
          )
          (exists (?x_2 )
            (and
              (LeftOf1 ?x_2)
              (RightOf8 ?x_2)
            )
          )
          (exists (?x_7 )
            (and
              (BelowOf8 ?x_7)
              (AboveOf17 ?x_7)
            )
          )
          (exists (?x_0 )
            (and
              (RightOf16 ?x_0)
              (LeftOf2 ?x_0)
            )
          )
          (exists (?x_9 )
            (and
              (LeftOf1 ?x_9)
              (RightOf9 ?x_9)
            )
          )
          (exists (?x_0 )
            (and
              (RightOf14 ?x_0)
              (LeftOf9 ?x_0)
            )
          )
          (exists (?x_32 )
            (and
              (RightOf14 ?x_32)
              (LeftOf4 ?x_32)
            )
          )
          (exists (?x_29 )
            (and
              (RightOf15 ?x_29)
              (LeftOf8 ?x_29)
            )
          )
          (exists (?x_26 )
            (and
              (RightOf17 ?x_26)
              (LeftOf2 ?x_26)
            )
          )
          (exists (?x_33 )
            (and
              (RightOf12 ?x_33)
              (LeftOf2 ?x_33)
            )
          )
          (exists (?x_23 )
            (and
              (AboveOf17 ?x_23)
              (BelowOf4 ?x_23)
            )
          )
          (exists (?x_13 )
            (and
              (LeftOf10 ?x_13)
              (RightOf11 ?x_13)
            )
          )
          (exists (?x_16 )
            (and
              (LeftOf2 ?x_16)
              (RightOf17 ?x_16)
            )
          )
          (exists (?x_24 )
            (and
              (LeftOf2 ?x_24)
              (RightOf10 ?x_24)
            )
          )
          (exists (?x_28 )
            (and
              (RightOf6 ?x_28)
              (LeftOf1 ?x_28)
            )
          )
          (exists (?x_15 )
            (and
              (RightOf14 ?x_15)
              (LeftOf2 ?x_15)
            )
          )
          (exists (?x_29 )
            (and
              (RightOf17 ?x_29)
              (LeftOf1 ?x_29)
            )
          )
          (exists (?x_1 )
            (and
              (BelowOf1 ?x_1)
              (AboveOf13 ?x_1)
            )
          )
          (exists (?x_25 )
            (and
              (BelowOf1 ?x_25)
              (AboveOf6 ?x_25)
            )
          )
          (exists (?x_12 )
            (and
              (LeftOf2 ?x_12)
              (RightOf17 ?x_12)
            )
          )
          (exists (?x_22 )
            (and
              (BelowOf2 ?x_22)
              (AboveOf16 ?x_22)
            )
          )
          (exists (?x_22 )
            (and
              (BelowOf4 ?x_22)
              (AboveOf16 ?x_22)
            )
          )
          (exists (?x_10 )
            (and
              (LeftOf4 ?x_10)
              (RightOf17 ?x_10)
            )
          )
          (exists (?x_6 )
            (and
              (BelowOf3 ?x_6)
              (AboveOf15 ?x_6)
            )
          )
          (exists (?x_13 )
            (and
              (LeftOf7 ?x_13)
              (RightOf11 ?x_13)
            )
          )
          (exists (?x_25 )
            (and
              (BelowOf4 ?x_25)
              (AboveOf7 ?x_25)
            )
          )
          (exists (?x_0 )
            (and
              (RightOf13 ?x_0)
              (LeftOf3 ?x_0)
            )
          )
          (exists (?x_9 )
            (and
              (LeftOf4 ?x_9)
              (RightOf11 ?x_9)
            )
          )
          (exists (?x_9 )
            (and
              (RightOf10 ?x_9)
              (LeftOf1 ?x_9)
            )
          )
          (exists (?x_27 )
            (and
              (BelowOf8 ?x_27)
              (AboveOf17 ?x_27)
            )
          )
          (exists (?x_10 )
            (and
              (LeftOf2 ?x_10)
              (RightOf8 ?x_10)
            )
          )
          (exists (?x_13 )
            (and
              (LeftOf10 ?x_13)
              (RightOf14 ?x_13)
            )
          )
          (exists (?x_0 )
            (and
              (RightOf13 ?x_0)
              (LeftOf5 ?x_0)
            )
          )
          (exists (?x_21 )
            (and
              (AboveOf14 ?x_21)
              (BelowOf6 ?x_21)
            )
          )
          (exists (?x_23 )
            (and
              (AboveOf14 ?x_23)
              (BelowOf3 ?x_23)
            )
          )
          (exists (?x_4 )
            (and
              (BelowOf12 ?x_4)
              (AboveOf17 ?x_4)
            )
          )
          (exists (?x_6 )
            (and
              (AboveOf10 ?x_6)
              (BelowOf2 ?x_6)
            )
          )
          (exists (?x_31 )
            (and
              (BelowOf4 ?x_31)
              (AboveOf9 ?x_31)
            )
          )
          (exists (?x_14 )
            (and
              (AboveOf12 ?x_14)
              (BelowOf5 ?x_14)
            )
          )
          (exists (?x_28 )
            (and
              (RightOf14 ?x_28)
              (LeftOf4 ?x_28)
            )
          )
          (exists (?x_10 )
            (and
              (LeftOf7 ?x_10)
              (RightOf8 ?x_10)
            )
          )
          (exists (?x_13 )
            (and
              (RightOf14 ?x_13)
              (LeftOf6 ?x_13)
            )
          )
          (exists (?x_19 )
            (and
              (RightOf17 ?x_19)
              (LeftOf16 ?x_19)
            )
          )
          (exists (?x_8 )
            (and
              (RightOf16 ?x_8)
              (LeftOf4 ?x_8)
            )
          )
          (exists (?x_29 )
            (and
              (RightOf15 ?x_29)
              (LeftOf1 ?x_29)
            )
          )
          (exists (?x_15 )
            (and
              (RightOf14 ?x_15)
              (LeftOf8 ?x_15)
            )
          )
          (exists (?x_29 )
            (and
              (RightOf16 ?x_29)
              (LeftOf7 ?x_29)
            )
          )
          (exists (?x_16 )
            (and
              (RightOf10 ?x_16)
              (LeftOf1 ?x_16)
            )
          )
          (exists (?x_29 )
            (and
              (LeftOf11 ?x_29)
              (RightOf11 ?x_29)
            )
          )
          (exists (?x_33 )
            (and
              (RightOf17 ?x_33)
              (LeftOf4 ?x_33)
            )
          )
          (exists (?x_8 )
            (and
              (RightOf17 ?x_8)
              (LeftOf15 ?x_8)
            )
          )
          (exists (?x_19 )
            (and
              (RightOf17 ?x_19)
              (LeftOf1 ?x_19)
            )
          )
          (exists (?x_5 )
            (and
              (AboveOf10 ?x_5)
              (BelowOf1 ?x_5)
            )
          )
          (exists (?x_19 )
            (and
              (RightOf17 ?x_19)
              (LeftOf12 ?x_19)
            )
          )
          (exists (?x_2 )
            (and
              (LeftOf1 ?x_2)
              (RightOf9 ?x_2)
            )
          )
          (exists (?x_14 )
            (and
              (AboveOf10 ?x_14)
              (BelowOf6 ?x_14)
            )
          )
          (exists (?x_3 )
            (and
              (RightOf16 ?x_3)
              (LeftOf2 ?x_3)
            )
          )
          (exists (?x_0 )
            (and
              (LeftOf9 ?x_0)
              (RightOf17 ?x_0)
            )
          )
          (exists (?x_21 )
            (and
              (AboveOf8 ?x_21)
              (BelowOf4 ?x_21)
            )
          )
          (exists (?x_22 )
            (and
              (BelowOf5 ?x_22)
              (AboveOf16 ?x_22)
            )
          )
          (exists (?x_1 )
            (and
              (AboveOf11 ?x_1)
              (BelowOf5 ?x_1)
            )
          )
          (exists (?x_12 )
            (and
              (LeftOf9 ?x_12)
              (RightOf15 ?x_12)
            )
          )
          (exists (?x_33 )
            (and
              (RightOf9 ?x_33)
              (LeftOf2 ?x_33)
            )
          )
          (exists (?x_32 )
            (and
              (RightOf12 ?x_32)
              (LeftOf4 ?x_32)
            )
          )
          (exists (?x_14 )
            (and
              (BelowOf4 ?x_14)
              (AboveOf11 ?x_14)
            )
          )
          (exists (?x_23 )
            (and
              (BelowOf7 ?x_23)
              (AboveOf16 ?x_23)
            )
          )
          (exists (?x_31 )
            (and
              (BelowOf4 ?x_31)
              (AboveOf17 ?x_31)
            )
          )
          (exists (?x_14 )
            (and
              (BelowOf9 ?x_14)
              (AboveOf17 ?x_14)
            )
          )
          (exists (?x_14 )
            (and
              (BelowOf8 ?x_14)
              (AboveOf16 ?x_14)
            )
          )
          (exists (?x_11 )
            (and
              (AboveOf17 ?x_11)
              (BelowOf6 ?x_11)
            )
          )
          (exists (?x_24 )
            (and
              (LeftOf3 ?x_24)
              (RightOf17 ?x_24)
            )
          )
          (exists (?x_21 )
            (and
              (BelowOf8 ?x_21)
              (AboveOf11 ?x_21)
            )
          )
          (exists (?x_0 )
            (and
              (RightOf13 ?x_0)
              (LeftOf9 ?x_0)
            )
          )
          (exists (?x_25 )
            (and
              (BelowOf2 ?x_25)
              (AboveOf6 ?x_25)
            )
          )
          (exists (?x_1 )
            (and
              (BelowOf5 ?x_1)
              (AboveOf12 ?x_1)
            )
          )
          (exists (?x_6 )
            (and
              (AboveOf9 ?x_6)
              (BelowOf5 ?x_6)
            )
          )
          (exists (?x_30 )
            (and
              (AboveOf4 ?x_30)
              (BelowOf4 ?x_30)
            )
          )
          (exists (?x_9 )
            (and
              (RightOf14 ?x_9)
              (LeftOf6 ?x_9)
            )
          )
          (exists (?x_2 )
            (and
              (LeftOf1 ?x_2)
              (RightOf11 ?x_2)
            )
          )
          (exists (?x_20 )
            (and
              (BelowOf1 ?x_20)
              (AboveOf16 ?x_20)
            )
          )
          (exists (?x_30 )
            (and
              (AboveOf4 ?x_30)
              (BelowOf3 ?x_30)
            )
          )
          (exists (?x_26 )
            (and
              (LeftOf3 ?x_26)
              (RightOf15 ?x_26)
            )
          )
          (exists (?x_32 )
            (and
              (LeftOf3 ?x_32)
              (RightOf16 ?x_32)
            )
          )
          (exists (?x_32 )
            (and
              (RightOf14 ?x_32)
              (LeftOf2 ?x_32)
            )
          )
          (exists (?x_10 )
            (and
              (LeftOf1 ?x_10)
              (RightOf14 ?x_10)
            )
          )
          (exists (?x_26 )
            (and
              (LeftOf3 ?x_26)
              (RightOf16 ?x_26)
            )
          )
          (exists (?x_11 )
            (and
              (BelowOf11 ?x_11)
              (AboveOf15 ?x_11)
            )
          )
          (exists (?x_18 )
            (and
              (AboveOf14 ?x_18)
              (BelowOf6 ?x_18)
            )
          )
          (exists (?x_29 )
            (and
              (LeftOf5 ?x_29)
              (RightOf12 ?x_29)
            )
          )
          (exists (?x_10 )
            (and
              (LeftOf1 ?x_10)
              (RightOf15 ?x_10)
            )
          )
          (exists (?x_32 )
            (and
              (LeftOf8 ?x_32)
              (RightOf16 ?x_32)
            )
          )
          (exists (?x_1 )
            (and
              (AboveOf17 ?x_1)
              (BelowOf5 ?x_1)
            )
          )
          (exists (?x_29 )
            (and
              (RightOf14 ?x_29)
              (LeftOf8 ?x_29)
            )
          )
          (exists (?x_3 )
            (and
              (LeftOf1 ?x_3)
              (RightOf3 ?x_3)
            )
          )
          (exists (?x_16 )
            (and
              (RightOf12 ?x_16)
              (LeftOf6 ?x_16)
            )
          )
          (exists (?x_12 )
            (and
              (LeftOf12 ?x_12)
              (RightOf17 ?x_12)
            )
          )
          (exists (?x_26 )
            (and
              (RightOf17 ?x_26)
              (LeftOf11 ?x_26)
            )
          )
          (exists (?x_3 )
            (and
              (LeftOf1 ?x_3)
              (RightOf6 ?x_3)
            )
          )
          (exists (?x_23 )
            (and
              (BelowOf14 ?x_23)
              (AboveOf16 ?x_23)
            )
          )
          (exists (?x_11 )
            (and
              (AboveOf17 ?x_11)
              (BelowOf7 ?x_11)
            )
          )
          (exists (?x_13 )
            (and
              (RightOf13 ?x_13)
              (LeftOf5 ?x_13)
            )
          )
          (exists (?x_33 )
            (and
              (LeftOf5 ?x_33)
              (RightOf9 ?x_33)
            )
          )
          (exists (?x_27 )
            (and
              (BelowOf4 ?x_27)
              (AboveOf17 ?x_27)
            )
          )
          (exists (?x_14 )
            (and
              (AboveOf10 ?x_14)
              (BelowOf9 ?x_14)
            )
          )
          (exists (?x_18 )
            (and
              (AboveOf13 ?x_18)
              (BelowOf10 ?x_18)
            )
          )
          (exists (?x_23 )
            (and
              (AboveOf14 ?x_23)
              (BelowOf4 ?x_23)
            )
          )
          (exists (?x_6 )
            (and
              (AboveOf10 ?x_6)
              (BelowOf6 ?x_6)
            )
          )
          (exists (?x_32 )
            (and
              (LeftOf10 ?x_32)
              (RightOf16 ?x_32)
            )
          )
          (exists (?x_5 )
            (and
              (AboveOf4 ?x_5)
              (BelowOf3 ?x_5)
            )
          )
          (exists (?x_2 )
            (and
              (LeftOf1 ?x_2)
              (RightOf7 ?x_2)
            )
          )
          (exists (?x_30 )
            (and
              (BelowOf4 ?x_30)
              (AboveOf7 ?x_30)
            )
          )
          (exists (?x_22 )
            (and
              (AboveOf13 ?x_22)
              (BelowOf4 ?x_22)
            )
          )
          (exists (?x_28 )
            (and
              (RightOf15 ?x_28)
              (LeftOf3 ?x_28)
            )
          )
          (exists (?x_33 )
            (and
              (LeftOf5 ?x_33)
              (RightOf8 ?x_33)
            )
          )
          (exists (?x_23 )
            (and
              (BelowOf2 ?x_23)
              (AboveOf16 ?x_23)
            )
          )
          (exists (?x_11 )
            (and
              (BelowOf9 ?x_11)
              (AboveOf15 ?x_11)
            )
          )
          (exists (?x_33 )
            (and
              (LeftOf5 ?x_33)
              (RightOf14 ?x_33)
            )
          )
          (exists (?x_31 )
            (and
              (BelowOf1 ?x_31)
              (AboveOf10 ?x_31)
            )
          )
          (exists (?x_25 )
            (and
              (AboveOf15 ?x_25)
              (BelowOf3 ?x_25)
            )
          )
          (exists (?x_32 )
            (and
              (RightOf17 ?x_32)
              (LeftOf11 ?x_32)
            )
          )
          (exists (?x_14 )
            (and
              (AboveOf14 ?x_14)
              (BelowOf5 ?x_14)
            )
          )
          (exists (?x_7 )
            (and
              (BelowOf12 ?x_7)
              (AboveOf17 ?x_7)
            )
          )
          (exists (?x_22 )
            (and
              (AboveOf15 ?x_22)
              (BelowOf2 ?x_22)
            )
          )
          (exists (?x_30 )
            (and
              (AboveOf13 ?x_30)
              (BelowOf4 ?x_30)
            )
          )
          (exists (?x_13 )
            (and
              (RightOf14 ?x_13)
              (LeftOf5 ?x_13)
            )
          )
          (exists (?x_23 )
            (and
              (BelowOf14 ?x_23)
              (AboveOf17 ?x_23)
            )
          )
          (exists (?x_19 )
            (and
              (RightOf17 ?x_19)
              (LeftOf9 ?x_19)
            )
          )
          (exists (?x_33 )
            (and
              (LeftOf3 ?x_33)
              (RightOf8 ?x_33)
            )
          )
          (exists (?x_21 )
            (and
              (AboveOf17 ?x_21)
              (BelowOf6 ?x_21)
            )
          )
          (exists (?x_28 )
            (and
              (RightOf7 ?x_28)
              (LeftOf2 ?x_28)
            )
          )
          (exists (?x_12 )
            (and
              (LeftOf7 ?x_12)
              (RightOf17 ?x_12)
            )
          )
          (exists (?x_32 )
            (and
              (LeftOf6 ?x_32)
              (RightOf13 ?x_32)
            )
          )
          (exists (?x_29 )
            (and
              (LeftOf3 ?x_29)
              (RightOf13 ?x_29)
            )
          )
          (exists (?x_3 )
            (and
              (LeftOf1 ?x_3)
              (RightOf10 ?x_3)
            )
          )
          (exists (?x_18 )
            (and
              (BelowOf8 ?x_18)
              (AboveOf15 ?x_18)
            )
          )
          (exists (?x_13 )
            (and
              (RightOf15 ?x_13)
              (LeftOf7 ?x_13)
            )
          )
          (exists (?x_4 )
            (and
              (AboveOf17 ?x_4)
              (BelowOf14 ?x_4)
            )
          )
          (exists (?x_11 )
            (and
              (BelowOf4 ?x_11)
              (AboveOf16 ?x_11)
            )
          )
          (exists (?x_20 )
            (and
              (AboveOf5 ?x_20)
              (BelowOf2 ?x_20)
            )
          )
          (exists (?x_16 )
            (and
              (LeftOf2 ?x_16)
              (RightOf10 ?x_16)
            )
          )
          (exists (?x_23 )
            (and
              (BelowOf6 ?x_23)
              (AboveOf15 ?x_23)
            )
          )
          (exists (?x_9 )
            (and
              (LeftOf7 ?x_9)
              (RightOf9 ?x_9)
            )
          )
          (exists (?x_20 )
            (and
              (AboveOf5 ?x_20)
              (BelowOf1 ?x_20)
            )
          )
          (exists (?x_10 )
            (and
              (LeftOf2 ?x_10)
              (RightOf12 ?x_10)
            )
          )
          (exists (?x_12 )
            (and
              (LeftOf1 ?x_12)
              (RightOf15 ?x_12)
            )
          )
          (exists (?x_23 )
            (and
              (BelowOf10 ?x_23)
              (AboveOf16 ?x_23)
            )
          )
          (exists (?x_30 )
            (and
              (AboveOf13 ?x_30)
              (BelowOf3 ?x_30)
            )
          )
          (exists (?x_30 )
            (and
              (BelowOf1 ?x_30)
              (AboveOf4 ?x_30)
            )
          )
          (exists (?x_32 )
            (and
              (RightOf17 ?x_32)
              (LeftOf3 ?x_32)
            )
          )
          (exists (?x_16 )
            (and
              (RightOf9 ?x_16)
              (LeftOf4 ?x_16)
            )
          )
          (exists (?x_14 )
            (and
              (BelowOf9 ?x_14)
              (AboveOf16 ?x_14)
            )
          )
          (exists (?x_13 )
            (and
              (RightOf16 ?x_13)
              (LeftOf3 ?x_13)
            )
          )
          (exists (?x_15 )
            (and
              (RightOf11 ?x_15)
              (LeftOf5 ?x_15)
            )
          )
          (exists (?x_1 )
            (and
              (BelowOf10 ?x_1)
              (AboveOf15 ?x_1)
            )
          )
          (exists (?x_1 )
            (and
              (BelowOf2 ?x_1)
              (AboveOf14 ?x_1)
            )
          )
          (exists (?x_24 )
            (and
              (LeftOf1 ?x_24)
              (RightOf15 ?x_24)
            )
          )
          (exists (?x_12 )
            (and
              (RightOf14 ?x_12)
              (LeftOf6 ?x_12)
            )
          )
          (exists (?x_29 )
            (and
              (LeftOf11 ?x_29)
              (RightOf15 ?x_29)
            )
          )
          (exists (?x_12 )
            (and
              (LeftOf8 ?x_12)
              (RightOf14 ?x_12)
            )
          )
          (exists (?x_7 )
            (and
              (BelowOf5 ?x_7)
              (AboveOf17 ?x_7)
            )
          )
          (exists (?x_4 )
            (and
              (BelowOf5 ?x_4)
              (AboveOf17 ?x_4)
            )
          )
          (exists (?x_10 )
            (and
              (LeftOf2 ?x_10)
              (RightOf7 ?x_10)
            )
          )
          (exists (?x_28 )
            (and
              (RightOf5 ?x_28)
              (LeftOf3 ?x_28)
            )
          )
          (exists (?x_31 )
            (and
              (AboveOf12 ?x_31)
              (BelowOf3 ?x_31)
            )
          )
          (exists (?x_1 )
            (and
              (BelowOf1 ?x_1)
              (AboveOf11 ?x_1)
            )
          )
          (exists (?x_19 )
            (and
              (LeftOf8 ?x_19)
              (RightOf17 ?x_19)
            )
          )
          (exists (?x_15 )
            (and
              (RightOf9 ?x_15)
              (LeftOf5 ?x_15)
            )
          )
          (exists (?x_25 )
            (and
              (AboveOf7 ?x_25)
              (BelowOf3 ?x_25)
            )
          )
          (exists (?x_28 )
            (and
              (LeftOf2 ?x_28)
              (RightOf12 ?x_28)
            )
          )
          (exists (?x_0 )
            (and
              (LeftOf9 ?x_0)
              (RightOf15 ?x_0)
            )
          )
          (exists (?x_1 )
            (and
              (BelowOf4 ?x_1)
              (AboveOf15 ?x_1)
            )
          )
          (exists (?x_21 )
            (and
              (BelowOf8 ?x_21)
              (AboveOf16 ?x_21)
            )
          )
          (exists (?x_2 )
            (and
              (LeftOf1 ?x_2)
              (RightOf10 ?x_2)
            )
          )
          (exists (?x_0 )
            (and
              (RightOf16 ?x_0)
              (LeftOf5 ?x_0)
            )
          )
          (exists (?x_9 )
            (and
              (LeftOf7 ?x_9)
              (RightOf11 ?x_9)
            )
          )
          (exists (?x_14 )
            (and
              (BelowOf3 ?x_14)
              (AboveOf17 ?x_14)
            )
          )
          (exists (?x_10 )
            (and
              (LeftOf1 ?x_10)
              (RightOf7 ?x_10)
            )
          )
          (exists (?x_16 )
            (and
              (LeftOf5 ?x_16)
              (RightOf16 ?x_16)
            )
          )
          (exists (?x_5 )
            (and
              (AboveOf12 ?x_5)
              (BelowOf2 ?x_5)
            )
          )
          (exists (?x_5 )
            (and
              (BelowOf1 ?x_5)
              (AboveOf9 ?x_5)
            )
          )
          (exists (?x_22 )
            (and
              (AboveOf14 ?x_22)
              (BelowOf4 ?x_22)
            )
          )
          (exists (?x_32 )
            (and
              (LeftOf9 ?x_32)
              (RightOf13 ?x_32)
            )
          )
          (exists (?x_8 )
            (and
              (RightOf17 ?x_8)
              (LeftOf4 ?x_8)
            )
          )
          (exists (?x_19 )
            (and
              (LeftOf17 ?x_19)
              (RightOf17 ?x_19)
            )
          )
          (exists (?x_29 )
            (and
              (LeftOf10 ?x_29)
              (RightOf11 ?x_29)
            )
          )
          (exists (?x_1 )
            (and
              (AboveOf10 ?x_1)
              (BelowOf9 ?x_1)
            )
          )
          (exists (?x_1 )
            (and
              (BelowOf8 ?x_1)
              (AboveOf11 ?x_1)
            )
          )
          (exists (?x_25 )
            (and
              (BelowOf2 ?x_25)
              (AboveOf8 ?x_25)
            )
          )
          (exists (?x_30 )
            (and
              (BelowOf1 ?x_30)
              (AboveOf15 ?x_30)
            )
          )
          (exists (?x_9 )
            (and
              (RightOf15 ?x_9)
              (LeftOf1 ?x_9)
            )
          )
          (exists (?x_12 )
            (and
              (RightOf15 ?x_12)
              (LeftOf10 ?x_12)
            )
          )
          (exists (?x_18 )
            (and
              (AboveOf16 ?x_18)
              (BelowOf3 ?x_18)
            )
          )
          (exists (?x_13 )
            (and
              (RightOf16 ?x_13)
              (LeftOf7 ?x_13)
            )
          )
          (exists (?x_26 )
            (and
              (RightOf17 ?x_26)
              (LeftOf14 ?x_26)
            )
          )
          (exists (?x_14 )
            (and
              (AboveOf10 ?x_14)
              (BelowOf8 ?x_14)
            )
          )
          (exists (?x_31 )
            (and
              (BelowOf4 ?x_31)
              (AboveOf8 ?x_31)
            )
          )
          (exists (?x_27 )
            (and
              (AboveOf17 ?x_27)
              (BelowOf5 ?x_27)
            )
          )
          (exists (?x_15 )
            (and
              (RightOf12 ?x_15)
              (LeftOf2 ?x_15)
            )
          )
          (exists (?x_0 )
            (and
              (RightOf14 ?x_0)
              (LeftOf5 ?x_0)
            )
          )
          (exists (?x_18 )
            (and
              (BelowOf9 ?x_18)
              (AboveOf12 ?x_18)
            )
          )
          (exists (?x_31 )
            (and
              (AboveOf11 ?x_31)
              (BelowOf3 ?x_31)
            )
          )
          (exists (?x_21 )
            (and
              (BelowOf5 ?x_21)
              (AboveOf9 ?x_21)
            )
          )
          (exists (?x_22 )
            (and
              (AboveOf17 ?x_22)
              (BelowOf8 ?x_22)
            )
          )
          (exists (?x_16 )
            (and
              (RightOf15 ?x_16)
              (LeftOf6 ?x_16)
            )
          )
          (exists (?x_15 )
            (and
              (LeftOf9 ?x_15)
              (RightOf15 ?x_15)
            )
          )
          (exists (?x_29 )
            (and
              (RightOf16 ?x_29)
              (LeftOf1 ?x_29)
            )
          )
          (exists (?x_15 )
            (and
              (RightOf16 ?x_15)
              (LeftOf8 ?x_15)
            )
          )
          (exists (?x_9 )
            (and
              (LeftOf2 ?x_9)
              (RightOf16 ?x_9)
            )
          )
          (exists (?x_13 )
            (and
              (RightOf12 ?x_13)
              (LeftOf3 ?x_13)
            )
          )
          (exists (?x_32 )
            (and
              (RightOf12 ?x_32)
              (LeftOf12 ?x_32)
            )
          )
          (exists (?x_22 )
            (and
              (AboveOf12 ?x_22)
              (BelowOf2 ?x_22)
            )
          )
          (exists (?x_14 )
            (and
              (AboveOf10 ?x_14)
              (BelowOf4 ?x_14)
            )
          )
          (exists (?x_26 )
            (and
              (RightOf15 ?x_26)
              (LeftOf11 ?x_26)
            )
          )
          (exists (?x_0 )
            (and
              (RightOf14 ?x_0)
              (LeftOf12 ?x_0)
            )
          )
          (exists (?x_6 )
            (and
              (AboveOf13 ?x_6)
              (BelowOf2 ?x_6)
            )
          )
          (exists (?x_0 )
            (and
              (LeftOf12 ?x_0)
              (RightOf17 ?x_0)
            )
          )
          (exists (?x_21 )
            (and
              (AboveOf9 ?x_21)
              (BelowOf1 ?x_21)
            )
          )
          (exists (?x_29 )
            (and
              (RightOf16 ?x_29)
              (LeftOf2 ?x_29)
            )
          )
          (exists (?x_27 )
            (and
              (BelowOf2 ?x_27)
              (AboveOf17 ?x_27)
            )
          )
          (exists (?x_10 )
            (and
              (RightOf16 ?x_10)
              (LeftOf4 ?x_10)
            )
          )
          (exists (?x_6 )
            (and
              (BelowOf1 ?x_6)
              (AboveOf9 ?x_6)
            )
          )
          (exists (?x_1 )
            (and
              (BelowOf10 ?x_1)
              (AboveOf14 ?x_1)
            )
          )
          (exists (?x_25 )
            (and
              (BelowOf1 ?x_25)
              (AboveOf15 ?x_25)
            )
          )
          (exists (?x_16 )
            (and
              (LeftOf6 ?x_16)
              (RightOf8 ?x_16)
            )
          )
          (exists (?x_30 )
            (and
              (AboveOf16 ?x_30)
              (BelowOf2 ?x_30)
            )
          )
          (exists (?x_6 )
            (and
              (BelowOf1 ?x_6)
              (AboveOf10 ?x_6)
            )
          )
          (exists (?x_22 )
            (and
              (AboveOf14 ?x_22)
              (BelowOf9 ?x_22)
            )
          )
          (exists (?x_25 )
            (and
              (BelowOf3 ?x_25)
              (AboveOf11 ?x_25)
            )
          )
          (exists (?x_1 )
            (and
              (BelowOf9 ?x_1)
              (AboveOf11 ?x_1)
            )
          )
          (exists (?x_15 )
            (and
              (RightOf13 ?x_15)
              (LeftOf7 ?x_15)
            )
          )
          (exists (?x_12 )
            (and
              (RightOf15 ?x_12)
              (LeftOf14 ?x_12)
            )
          )
          (exists (?x_10 )
            (and
              (LeftOf2 ?x_10)
              (RightOf15 ?x_10)
            )
          )
          (exists (?x_21 )
            (and
              (AboveOf12 ?x_21)
              (BelowOf1 ?x_21)
            )
          )
          (exists (?x_9 )
            (and
              (LeftOf2 ?x_9)
              (RightOf8 ?x_9)
            )
          )
          (exists (?x_28 )
            (and
              (RightOf16 ?x_28)
              (LeftOf2 ?x_28)
            )
          )
          (exists (?x_27 )
            (and
              (BelowOf7 ?x_27)
              (AboveOf16 ?x_27)
            )
          )
          (exists (?x_25 )
            (and
              (AboveOf10 ?x_25)
              (BelowOf3 ?x_25)
            )
          )
          (exists (?x_13 )
            (and
              (LeftOf6 ?x_13)
              (RightOf17 ?x_13)
            )
          )
          (exists (?x_10 )
            (and
              (RightOf12 ?x_10)
              (LeftOf5 ?x_10)
            )
          )
          (exists (?x_6 )
            (and
              (BelowOf1 ?x_6)
              (AboveOf15 ?x_6)
            )
          )
          (exists (?x_21 )
            (and
              (BelowOf8 ?x_21)
              (AboveOf10 ?x_21)
            )
          )
          (exists (?x_29 )
            (and
              (RightOf16 ?x_29)
              (LeftOf4 ?x_29)
            )
          )
          (exists (?x_14 )
            (and
              (AboveOf12 ?x_14)
              (BelowOf8 ?x_14)
            )
          )
          (exists (?x_9 )
            (and
              (LeftOf3 ?x_9)
              (RightOf13 ?x_9)
            )
          )
          (exists (?x_12 )
            (and
              (LeftOf11 ?x_12)
              (RightOf16 ?x_12)
            )
          )
          (exists (?x_18 )
            (and
              (AboveOf14 ?x_18)
              (BelowOf12 ?x_18)
            )
          )
          (exists (?x_30 )
            (and
              (BelowOf2 ?x_30)
              (AboveOf11 ?x_30)
            )
          )
          (exists (?x_10 )
            (and
              (LeftOf7 ?x_10)
              (RightOf11 ?x_10)
            )
          )
          (exists (?x_21 )
            (and
              (BelowOf5 ?x_21)
              (AboveOf11 ?x_21)
            )
          )
          (exists (?x_9 )
            (and
              (LeftOf2 ?x_9)
              (RightOf9 ?x_9)
            )
          )
          (exists (?x_11 )
            (and
              (AboveOf14 ?x_11)
              (BelowOf10 ?x_11)
            )
          )
          (exists (?x_32 )
            (and
              (RightOf14 ?x_32)
              (LeftOf12 ?x_32)
            )
          )
          (exists (?x_33 )
            (and
              (LeftOf1 ?x_33)
              (RightOf5 ?x_33)
            )
          )
          (exists (?x_0 )
            (and
              (LeftOf6 ?x_0)
              (RightOf16 ?x_0)
            )
          )
          (exists (?x_4 )
            (and
              (BelowOf9 ?x_4)
              (AboveOf17 ?x_4)
            )
          )
          (exists (?x_30 )
            (and
              (BelowOf2 ?x_30)
              (AboveOf5 ?x_30)
            )
          )
          (exists (?x_19 )
            (and
              (RightOf17 ?x_19)
              (LeftOf2 ?x_19)
            )
          )
          (exists (?x_26 )
            (and
              (LeftOf4 ?x_26)
              (RightOf15 ?x_26)
            )
          )
          (exists (?x_32 )
            (and
              (RightOf14 ?x_32)
              (LeftOf9 ?x_32)
            )
          )
          (exists (?x_21 )
            (and
              (BelowOf6 ?x_21)
              (AboveOf9 ?x_21)
            )
          )
          (exists (?x_12 )
            (and
              (LeftOf12 ?x_12)
              (RightOf14 ?x_12)
            )
          )
          (exists (?x_5 )
            (and
              (AboveOf5 ?x_5)
              (BelowOf2 ?x_5)
            )
          )
          (exists (?x_4 )
            (and
              (BelowOf10 ?x_4)
              (AboveOf17 ?x_4)
            )
          )
          (exists (?x_1 )
            (and
              (AboveOf10 ?x_1)
              (BelowOf7 ?x_1)
            )
          )
          (exists (?x_29 )
            (and
              (LeftOf4 ?x_29)
              (RightOf11 ?x_29)
            )
          )
          (exists (?x_30 )
            (and
              (BelowOf1 ?x_30)
              (AboveOf11 ?x_30)
            )
          )
          (exists (?x_27 )
            (and
              (AboveOf15 ?x_27)
              (BelowOf3 ?x_27)
            )
          )
          (exists (?x_31 )
            (and
              (BelowOf1 ?x_31)
              (AboveOf11 ?x_31)
            )
          )
          (exists (?x_12 )
            (and
              (RightOf15 ?x_12)
              (LeftOf8 ?x_12)
            )
          )
          (exists (?x_6 )
            (and
              (AboveOf11 ?x_6)
              (BelowOf5 ?x_6)
            )
          )
          (exists (?x_18 )
            (and
              (BelowOf7 ?x_18)
              (AboveOf13 ?x_18)
            )
          )
          (exists (?x_28 )
            (and
              (RightOf16 ?x_28)
              (LeftOf4 ?x_28)
            )
          )
          (exists (?x_1 )
            (and
              (AboveOf16 ?x_1)
              (BelowOf3 ?x_1)
            )
          )
          (exists (?x_14 )
            (and
              (BelowOf4 ?x_14)
              (AboveOf12 ?x_14)
            )
          )
          (exists (?x_11 )
            (and
              (BelowOf12 ?x_11)
              (AboveOf13 ?x_11)
            )
          )
          (exists (?x_11 )
            (and
              (AboveOf16 ?x_11)
              (BelowOf7 ?x_11)
            )
          )
          (exists (?x_32 )
            (and
              (LeftOf5 ?x_32)
              (RightOf12 ?x_32)
            )
          )
          (exists (?x_11 )
            (and
              (AboveOf13 ?x_11)
              (BelowOf2 ?x_11)
            )
          )
          (exists (?x_15 )
            (and
              (LeftOf5 ?x_15)
              (RightOf17 ?x_15)
            )
          )
          (exists (?x_11 )
            (and
              (BelowOf13 ?x_11)
              (AboveOf15 ?x_11)
            )
          )
          (exists (?x_14 )
            (and
              (BelowOf1 ?x_14)
              (AboveOf14 ?x_14)
            )
          )
          (exists (?x_21 )
            (and
              (AboveOf12 ?x_21)
              (BelowOf5 ?x_21)
            )
          )
          (exists (?x_7 )
            (and
              (BelowOf1 ?x_7)
              (AboveOf17 ?x_7)
            )
          )
          (exists (?x_21 )
            (and
              (AboveOf17 ?x_21)
              (BelowOf7 ?x_21)
            )
          )
          (exists (?x_13 )
            (and
              (RightOf15 ?x_13)
              (LeftOf10 ?x_13)
            )
          )
          (exists (?x_21 )
            (and
              (BelowOf3 ?x_21)
              (AboveOf15 ?x_21)
            )
          )
          (exists (?x_33 )
            (and
              (RightOf7 ?x_33)
              (LeftOf2 ?x_33)
            )
          )
          (exists (?x_6 )
            (and
              (AboveOf13 ?x_6)
              (BelowOf1 ?x_6)
            )
          )
          (exists (?x_31 )
            (and
              (BelowOf2 ?x_31)
              (AboveOf17 ?x_31)
            )
          )
          (exists (?x_33 )
            (and
              (RightOf12 ?x_33)
              (LeftOf4 ?x_33)
            )
          )
          (exists (?x_15 )
            (and
              (LeftOf1 ?x_15)
              (RightOf12 ?x_15)
            )
          )
          (exists (?x_12 )
            (and
              (LeftOf14 ?x_12)
              (RightOf14 ?x_12)
            )
          )
          (exists (?x_18 )
            (and
              (AboveOf15 ?x_18)
              (BelowOf6 ?x_18)
            )
          )
          (exists (?x_27 )
            (and
              (BelowOf12 ?x_27)
              (AboveOf17 ?x_27)
            )
          )
          (exists (?x_25 )
            (and
              (AboveOf13 ?x_25)
              (BelowOf3 ?x_25)
            )
          )
          (exists (?x_16 )
            (and
              (RightOf10 ?x_16)
              (LeftOf3 ?x_16)
            )
          )
          (exists (?x_11 )
            (and
              (AboveOf17 ?x_11)
              (BelowOf11 ?x_11)
            )
          )
          (exists (?x_33 )
            (and
              (LeftOf5 ?x_33)
              (RightOf16 ?x_33)
            )
          )
          (exists (?x_6 )
            (and
              (BelowOf1 ?x_6)
              (AboveOf17 ?x_6)
            )
          )
          (exists (?x_9 )
            (and
              (LeftOf6 ?x_9)
              (RightOf9 ?x_9)
            )
          )
          (exists (?x_26 )
            (and
              (LeftOf13 ?x_26)
              (RightOf15 ?x_26)
            )
          )
          (exists (?x_33 )
            (and
              (LeftOf5 ?x_33)
              (RightOf11 ?x_33)
            )
          )
          (exists (?x_3 )
            (and
              (RightOf14 ?x_3)
              (LeftOf1 ?x_3)
            )
          )
          (exists (?x_9 )
            (and
              (RightOf8 ?x_9)
              (LeftOf3 ?x_9)
            )
          )
          (exists (?x_0 )
            (and
              (LeftOf4 ?x_0)
              (RightOf13 ?x_0)
            )
          )
          (exists (?x_14 )
            (and
              (AboveOf9 ?x_14)
              (BelowOf2 ?x_14)
            )
          )
          (exists (?x_6 )
            (and
              (BelowOf1 ?x_6)
              (AboveOf7 ?x_6)
            )
          )
          (exists (?x_27 )
            (and
              (AboveOf16 ?x_27)
              (BelowOf6 ?x_27)
            )
          )
          (exists (?x_8 )
            (and
              (RightOf17 ?x_8)
              (LeftOf3 ?x_8)
            )
          )
          (exists (?x_33 )
            (and
              (LeftOf4 ?x_33)
              (RightOf16 ?x_33)
            )
          )
          (exists (?x_13 )
            (and
              (LeftOf8 ?x_13)
              (RightOf17 ?x_13)
            )
          )
          (exists (?x_16 )
            (and
              (RightOf13 ?x_16)
              (LeftOf1 ?x_16)
            )
          )
          (exists (?x_22 )
            (and
              (AboveOf13 ?x_22)
              (BelowOf1 ?x_22)
            )
          )
          (exists (?x_28 )
            (and
              (LeftOf4 ?x_28)
              (RightOf12 ?x_28)
            )
          )
          (exists (?x_16 )
            (and
              (RightOf10 ?x_16)
              (LeftOf5 ?x_16)
            )
          )
          (exists (?x_10 )
            (and
              (LeftOf6 ?x_10)
              (RightOf9 ?x_10)
            )
          )
          (exists (?x_12 )
            (and
              (RightOf16 ?x_12)
              (LeftOf8 ?x_12)
            )
          )
          (exists (?x_10 )
            (and
              (LeftOf2 ?x_10)
              (RightOf9 ?x_10)
            )
          )
          (exists (?x_27 )
            (and
              (AboveOf16 ?x_27)
              (BelowOf11 ?x_27)
            )
          )
          (exists (?x_18 )
            (and
              (BelowOf2 ?x_18)
              (AboveOf12 ?x_18)
            )
          )
          (exists (?x_23 )
            (and
              (AboveOf15 ?x_23)
              (BelowOf13 ?x_23)
            )
          )
          (exists (?x_18 )
            (and
              (BelowOf11 ?x_18)
              (AboveOf13 ?x_18)
            )
          )
          (exists (?x_20 )
            (and
              (AboveOf6 ?x_20)
              (BelowOf2 ?x_20)
            )
          )
          (exists (?x_33 )
            (and
              (LeftOf5 ?x_33)
              (RightOf10 ?x_33)
            )
          )
          (exists (?x_28 )
            (and
              (RightOf15 ?x_28)
              (LeftOf2 ?x_28)
            )
          )
          (exists (?x_20 )
            (and
              (BelowOf1 ?x_20)
              (AboveOf2 ?x_20)
            )
          )
          (exists (?x_28 )
            (and
              (RightOf7 ?x_28)
              (LeftOf1 ?x_28)
            )
          )
          (exists (?x_6 )
            (and
              (AboveOf12 ?x_6)
              (BelowOf4 ?x_6)
            )
          )
          (exists (?x_31 )
            (and
              (BelowOf1 ?x_31)
              (AboveOf8 ?x_31)
            )
          )
          (exists (?x_6 )
            (and
              (AboveOf14 ?x_6)
              (BelowOf3 ?x_6)
            )
          )
          (exists (?x_25 )
            (and
              (BelowOf1 ?x_25)
              (AboveOf9 ?x_25)
            )
          )
          (exists (?x_1 )
            (and
              (AboveOf14 ?x_1)
              (BelowOf5 ?x_1)
            )
          )
          (exists (?x_15 )
            (and
              (LeftOf3 ?x_15)
              (RightOf13 ?x_15)
            )
          )
          (exists (?x_11 )
            (and
              (AboveOf16 ?x_11)
              (BelowOf10 ?x_11)
            )
          )
          (exists (?x_24 )
            (and
              (RightOf4 ?x_24)
              (LeftOf2 ?x_24)
            )
          )
          (exists (?x_9 )
            (and
              (RightOf15 ?x_9)
              (LeftOf3 ?x_9)
            )
          )
          (exists (?x_32 )
            (and
              (LeftOf4 ?x_32)
              (RightOf13 ?x_32)
            )
          )
          (exists (?x_10 )
            (and
              (RightOf13 ?x_10)
              (LeftOf3 ?x_10)
            )
          )
          (exists (?x_29 )
            (and
              (LeftOf2 ?x_29)
              (RightOf12 ?x_29)
            )
          )
          (exists (?x_30 )
            (and
              (BelowOf2 ?x_30)
              (AboveOf10 ?x_30)
            )
          )
          (exists (?x_12 )
            (and
              (LeftOf7 ?x_12)
              (RightOf16 ?x_12)
            )
          )
          (exists (?x_31 )
            (and
              (AboveOf9 ?x_31)
              (BelowOf5 ?x_31)
            )
          )
          (exists (?x_33 )
            (and
              (LeftOf2 ?x_33)
              (RightOf6 ?x_33)
            )
          )
          (exists (?x_10 )
            (and
              (LeftOf7 ?x_10)
              (RightOf12 ?x_10)
            )
          )
          (exists (?x_32 )
            (and
              (LeftOf10 ?x_32)
              (RightOf15 ?x_32)
            )
          )
          (exists (?x_12 )
            (and
              (RightOf17 ?x_12)
              (LeftOf13 ?x_12)
            )
          )
          (exists (?x_27 )
            (and
              (BelowOf10 ?x_27)
              (AboveOf16 ?x_27)
            )
          )
          (exists (?x_15 )
            (and
              (LeftOf7 ?x_15)
              (RightOf12 ?x_15)
            )
          )
          (exists (?x_23 )
            (and
              (BelowOf12 ?x_23)
              (AboveOf15 ?x_23)
            )
          )
          (exists (?x_26 )
            (and
              (LeftOf14 ?x_26)
              (RightOf15 ?x_26)
            )
          )
          (exists (?x_29 )
            (and
              (RightOf17 ?x_29)
              (LeftOf8 ?x_29)
            )
          )
          (exists (?x_6 )
            (and
              (AboveOf16 ?x_6)
              (BelowOf3 ?x_6)
            )
          )
          (exists (?x_22 )
            (and
              (BelowOf10 ?x_22)
              (AboveOf16 ?x_22)
            )
          )
          (exists (?x_24 )
            (and
              (LeftOf3 ?x_24)
              (RightOf6 ?x_24)
            )
          )
          (exists (?x_9 )
            (and
              (RightOf13 ?x_9)
              (LeftOf1 ?x_9)
            )
          )
          (exists (?x_18 )
            (and
              (AboveOf15 ?x_18)
              (BelowOf12 ?x_18)
            )
          )
          (exists (?x_24 )
            (and
              (RightOf17 ?x_24)
              (LeftOf1 ?x_24)
            )
          )
          (exists (?x_22 )
            (and
              (BelowOf9 ?x_22)
              (AboveOf13 ?x_22)
            )
          )
          (exists (?x_32 )
            (and
              (LeftOf5 ?x_32)
              (RightOf15 ?x_32)
            )
          )
          (exists (?x_22 )
            (and
              (BelowOf6 ?x_22)
              (AboveOf14 ?x_22)
            )
          )
          (exists (?x_3 )
            (and
              (RightOf4 ?x_3)
              (LeftOf2 ?x_3)
            )
          )
          (exists (?x_13 )
            (and
              (RightOf16 ?x_13)
              (LeftOf10 ?x_13)
            )
          )
          (exists (?x_27 )
            (and
              (AboveOf16 ?x_27)
              (BelowOf5 ?x_27)
            )
          )
          (exists (?x_9 )
            (and
              (LeftOf4 ?x_9)
              (RightOf17 ?x_9)
            )
          )
          (exists (?x_14 )
            (and
              (BelowOf6 ?x_14)
              (AboveOf17 ?x_14)
            )
          )
          (exists (?x_2 )
            (and
              (LeftOf1 ?x_2)
              (RightOf14 ?x_2)
            )
          )
          (exists (?x_32 )
            (and
              (LeftOf8 ?x_32)
              (RightOf15 ?x_32)
            )
          )
          (exists (?x_31 )
            (and
              (AboveOf9 ?x_31)
              (BelowOf3 ?x_31)
            )
          )
          (exists (?x_23 )
            (and
              (AboveOf14 ?x_23)
              (BelowOf5 ?x_23)
            )
          )
          (exists (?x_13 )
            (and
              (RightOf10 ?x_13)
              (LeftOf8 ?x_13)
            )
          )
          (exists (?x_21 )
            (and
              (BelowOf5 ?x_21)
              (AboveOf15 ?x_21)
            )
          )
          (exists (?x_22 )
            (and
              (AboveOf14 ?x_22)
              (BelowOf5 ?x_22)
            )
          )
          (exists (?x_9 )
            (and
              (LeftOf2 ?x_9)
              (RightOf15 ?x_9)
            )
          )
          (exists (?x_26 )
            (and
              (LeftOf9 ?x_26)
              (RightOf15 ?x_26)
            )
          )
          (exists (?x_17 )
            (and
              (AboveOf4 ?x_17)
              (BelowOf1 ?x_17)
            )
          )
          (exists (?x_1 )
            (and
              (BelowOf9 ?x_1)
              (AboveOf15 ?x_1)
            )
          )
          (exists (?x_29 )
            (and
              (LeftOf2 ?x_29)
              (RightOf13 ?x_29)
            )
          )
          (exists (?x_11 )
            (and
              (BelowOf5 ?x_11)
              (AboveOf13 ?x_11)
            )
          )
          (exists (?x_21 )
            (and
              (BelowOf6 ?x_21)
              (AboveOf10 ?x_21)
            )
          )
          (exists (?x_1 )
            (and
              (BelowOf2 ?x_1)
              (AboveOf13 ?x_1)
            )
          )
          (exists (?x_31 )
            (and
              (AboveOf14 ?x_31)
              (BelowOf3 ?x_31)
            )
          )
          (exists (?x_21 )
            (and
              (AboveOf17 ?x_21)
              (BelowOf1 ?x_21)
            )
          )
          (exists (?x_11 )
            (and
              (BelowOf8 ?x_11)
              (AboveOf13 ?x_11)
            )
          )
          (exists (?x_8 )
            (and
              (RightOf16 ?x_8)
              (LeftOf14 ?x_8)
            )
          )
          (exists (?x_31 )
            (and
              (BelowOf1 ?x_31)
              (AboveOf7 ?x_31)
            )
          )
          (exists (?x_3 )
            (and
              (LeftOf2 ?x_3)
              (RightOf12 ?x_3)
            )
          )
          (exists (?x_13 )
            (and
              (LeftOf1 ?x_13)
              (RightOf12 ?x_13)
            )
          )
          (exists (?x_22 )
            (and
              (AboveOf11 ?x_22)
              (BelowOf9 ?x_22)
            )
          )
          (exists (?x_12 )
            (and
              (RightOf17 ?x_12)
              (LeftOf6 ?x_12)
            )
          )
          (exists (?x_15 )
            (and
              (RightOf10 ?x_15)
              (LeftOf7 ?x_15)
            )
          )
          (exists (?x_22 )
            (and
              (BelowOf6 ?x_22)
              (AboveOf13 ?x_22)
            )
          )
          (exists (?x_33 )
            (and
              (LeftOf4 ?x_33)
              (RightOf6 ?x_33)
            )
          )
          (exists (?x_30 )
            (and
              (AboveOf15 ?x_30)
              (BelowOf2 ?x_30)
            )
          )
          (exists (?x_6 )
            (and
              (BelowOf2 ?x_6)
              (AboveOf12 ?x_6)
            )
          )
          (exists (?x_33 )
            (and
              (RightOf14 ?x_33)
              (LeftOf4 ?x_33)
            )
          )
          (exists (?x_18 )
            (and
              (AboveOf14 ?x_18)
              (BelowOf8 ?x_18)
            )
          )
          (exists (?x_28 )
            (and
              (RightOf16 ?x_28)
              (LeftOf1 ?x_28)
            )
          )
          (exists (?x_15 )
            (and
              (RightOf15 ?x_15)
              (LeftOf8 ?x_15)
            )
          )
          (exists (?x_6 )
            (and
              (AboveOf9 ?x_6)
              (BelowOf1 ?x_6)
            )
          )
          (exists (?x_5 )
            (and
              (BelowOf3 ?x_5)
              (AboveOf8 ?x_5)
            )
          )
          (exists (?x_28 )
            (and
              (RightOf14 ?x_28)
              (LeftOf2 ?x_28)
            )
          )
          (exists (?x_13 )
            (and
              (LeftOf6 ?x_13)
              (RightOf12 ?x_13)
            )
          )
          (exists (?x_13 )
            (and
              (LeftOf4 ?x_13)
              (RightOf14 ?x_13)
            )
          )
          (exists (?x_17 )
            (and
              (AboveOf1 ?x_17)
              (BelowOf1 ?x_17)
            )
          )
          (exists (?x_6 )
            (and
              (AboveOf6 ?x_6)
              (BelowOf6 ?x_6)
            )
          )
          (exists (?x_16 )
            (and
              (RightOf15 ?x_16)
              (LeftOf5 ?x_16)
            )
          )
          (exists (?x_9 )
            (and
              (RightOf10 ?x_9)
              (LeftOf6 ?x_9)
            )
          )
          (exists (?x_29 )
            (and
              (RightOf14 ?x_29)
              (LeftOf11 ?x_29)
            )
          )
          (exists (?x_27 )
            (and
              (BelowOf5 ?x_27)
              (AboveOf17 ?x_27)
            )
          )
          (exists (?x_12 )
            (and
              (LeftOf4 ?x_12)
              (RightOf17 ?x_12)
            )
          )
          (exists (?x_4 )
            (and
              (BelowOf11 ?x_4)
              (AboveOf17 ?x_4)
            )
          )
          (exists (?x_10 )
            (and
              (LeftOf1 ?x_10)
              (RightOf11 ?x_10)
            )
          )
          (exists (?x_10 )
            (and
              (LeftOf5 ?x_10)
              (RightOf9 ?x_10)
            )
          )
          (exists (?x_21 )
            (and
              (BelowOf4 ?x_21)
              (AboveOf13 ?x_21)
            )
          )
          (exists (?x_32 )
            (and
              (LeftOf6 ?x_32)
              (RightOf15 ?x_32)
            )
          )
          (exists (?x_1 )
            (and
              (AboveOf11 ?x_1)
              (BelowOf3 ?x_1)
            )
          )
          (exists (?x_4 )
            (and
              (BelowOf3 ?x_4)
              (AboveOf17 ?x_4)
            )
          )
          (exists (?x_8 )
            (and
              (LeftOf12 ?x_8)
              (RightOf17 ?x_8)
            )
          )
          (exists (?x_25 )
            (and
              (AboveOf16 ?x_25)
              (BelowOf4 ?x_25)
            )
          )
          (exists (?x_9 )
            (and
              (RightOf15 ?x_9)
              (LeftOf6 ?x_9)
            )
          )
          (exists (?x_5 )
            (and
              (AboveOf17 ?x_5)
              (BelowOf2 ?x_5)
            )
          )
          (exists (?x_27 )
            (and
              (BelowOf1 ?x_27)
              (AboveOf17 ?x_27)
            )
          )
          (exists (?x_15 )
            (and
              (LeftOf4 ?x_15)
              (RightOf9 ?x_15)
            )
          )
          (exists (?x_31 )
            (and
              (BelowOf1 ?x_31)
              (AboveOf12 ?x_31)
            )
          )
          (exists (?x_22 )
            (and
              (AboveOf12 ?x_22)
              (BelowOf4 ?x_22)
            )
          )
          (exists (?x_22 )
            (and
              (BelowOf11 ?x_22)
              (AboveOf16 ?x_22)
            )
          )
          (exists (?x_11 )
            (and
              (AboveOf15 ?x_11)
              (BelowOf7 ?x_11)
            )
          )
          (exists (?x_6 )
            (and
              (AboveOf11 ?x_6)
              (BelowOf3 ?x_6)
            )
          )
          (exists (?x_9 )
            (and
              (RightOf17 ?x_9)
              (LeftOf1 ?x_9)
            )
          )
          (exists (?x_5 )
            (and
              (BelowOf3 ?x_5)
              (AboveOf10 ?x_5)
            )
          )
          (exists (?x_31 )
            (and
              (BelowOf4 ?x_31)
              (AboveOf14 ?x_31)
            )
          )
          (exists (?x_6 )
            (and
              (BelowOf2 ?x_6)
              (AboveOf6 ?x_6)
            )
          )
          (exists (?x_14 )
            (and
              (BelowOf7 ?x_14)
              (AboveOf11 ?x_14)
            )
          )
          (exists (?x_25 )
            (and
              (BelowOf1 ?x_25)
              (AboveOf7 ?x_25)
            )
          )
          (exists (?x_25 )
            (and
              (BelowOf2 ?x_25)
              (AboveOf5 ?x_25)
            )
          )
          (exists (?x_28 )
            (and
              (RightOf12 ?x_28)
              (LeftOf1 ?x_28)
            )
          )
          (exists (?x_5 )
            (and
              (BelowOf3 ?x_5)
              (AboveOf16 ?x_5)
            )
          )
          (exists (?x_18 )
            (and
              (BelowOf2 ?x_18)
              (AboveOf16 ?x_18)
            )
          )
          (exists (?x_9 )
            (and
              (RightOf12 ?x_9)
              (LeftOf6 ?x_9)
            )
          )
          (exists (?x_32 )
            (and
              (LeftOf5 ?x_32)
              (RightOf17 ?x_32)
            )
          )
          (exists (?x_32 )
            (and
              (LeftOf11 ?x_32)
              (RightOf16 ?x_32)
            )
          )
          (exists (?x_9 )
            (and
              (LeftOf7 ?x_9)
              (RightOf14 ?x_9)
            )
          )
          (exists (?x_6 )
            (and
              (AboveOf8 ?x_6)
              (BelowOf4 ?x_6)
            )
          )
          (exists (?x_11 )
            (and
              (AboveOf13 ?x_11)
              (BelowOf10 ?x_11)
            )
          )
          (exists (?x_16 )
            (and
              (LeftOf5 ?x_16)
              (RightOf7 ?x_16)
            )
          )
          (exists (?x_8 )
            (and
              (RightOf16 ?x_8)
              (LeftOf7 ?x_8)
            )
          )
          (exists (?x_31 )
            (and
              (AboveOf7 ?x_31)
              (BelowOf7 ?x_31)
            )
          )
          (exists (?x_3 )
            (and
              (RightOf5 ?x_3)
              (LeftOf1 ?x_3)
            )
          )
          (exists (?x_21 )
            (and
              (AboveOf12 ?x_21)
              (BelowOf2 ?x_21)
            )
          )
          (exists (?x_5 )
            (and
              (AboveOf6 ?x_5)
              (BelowOf2 ?x_5)
            )
          )
          (exists (?x_18 )
            (and
              (BelowOf6 ?x_18)
              (AboveOf17 ?x_18)
            )
          )
          (exists (?x_14 )
            (and
              (BelowOf2 ?x_14)
              (AboveOf14 ?x_14)
            )
          )
          (exists (?x_21 )
            (and
              (BelowOf6 ?x_21)
              (AboveOf11 ?x_21)
            )
          )
          (exists (?x_27 )
            (and
              (AboveOf16 ?x_27)
              (BelowOf3 ?x_27)
            )
          )
          (exists (?x_10 )
            (and
              (LeftOf1 ?x_10)
              (RightOf8 ?x_10)
            )
          )
          (exists (?x_26 )
            (and
              (RightOf17 ?x_26)
              (LeftOf1 ?x_26)
            )
          )
          (exists (?x_31 )
            (and
              (AboveOf7 ?x_31)
              (BelowOf3 ?x_31)
            )
          )
          (exists (?x_14 )
            (and
              (AboveOf17 ?x_14)
              (BelowOf5 ?x_14)
            )
          )
          (exists (?x_16 )
            (and
              (RightOf9 ?x_16)
              (LeftOf1 ?x_16)
            )
          )
          (exists (?x_30 )
            (and
              (AboveOf10 ?x_30)
              (BelowOf3 ?x_30)
            )
          )
          (exists (?x_16 )
            (and
              (RightOf6 ?x_16)
              (LeftOf1 ?x_16)
            )
          )
          (exists (?x_10 )
            (and
              (RightOf9 ?x_10)
              (LeftOf5 ?x_10)
            )
          )
          (exists (?x_30 )
            (and
              (BelowOf1 ?x_30)
              (AboveOf17 ?x_30)
            )
          )
          (exists (?x_1 )
            (and
              (AboveOf13 ?x_1)
              (BelowOf4 ?x_1)
            )
          )
          (exists (?x_18 )
            (and
              (BelowOf10 ?x_18)
              (AboveOf12 ?x_18)
            )
          )
          (exists (?x_28 )
            (and
              (RightOf13 ?x_28)
              (LeftOf2 ?x_28)
            )
          )
          (exists (?x_8 )
            (and
              (RightOf16 ?x_8)
              (LeftOf15 ?x_8)
            )
          )
          (exists (?x_5 )
            (and
              (AboveOf6 ?x_5)
              (BelowOf1 ?x_5)
            )
          )
          (exists (?x_22 )
            (and
              (BelowOf1 ?x_22)
              (AboveOf13 ?x_22)
            )
          )
          (exists (?x_10 )
            (and
              (LeftOf7 ?x_10)
              (RightOf13 ?x_10)
            )
          )
          (exists (?x_30 )
            (and
              (BelowOf4 ?x_30)
              (AboveOf14 ?x_30)
            )
          )
          (exists (?x_28 )
            (and
              (LeftOf3 ?x_28)
              (RightOf8 ?x_28)
            )
          )
          (exists (?x_7 )
            (and
              (AboveOf16 ?x_7)
              (BelowOf2 ?x_7)
            )
          )
          (exists (?x_6 )
            (and
              (BelowOf4 ?x_6)
              (AboveOf12 ?x_6)
            )
          )
          (exists (?x_32 )
            (and
              (RightOf15 ?x_32)
              (LeftOf7 ?x_32)
            )
          )
          (exists (?x_15 )
            (and
              (LeftOf1 ?x_15)
              (RightOf17 ?x_15)
            )
          )
          (exists (?x_15 )
            (and
              (LeftOf4 ?x_15)
              (RightOf10 ?x_15)
            )
          )
          (exists (?x_4 )
            (and
              (BelowOf4 ?x_4)
              (AboveOf17 ?x_4)
            )
          )
          (exists (?x_22 )
            (and
              (AboveOf12 ?x_22)
              (BelowOf10 ?x_22)
            )
          )
          (exists (?x_25 )
            (and
              (BelowOf1 ?x_25)
              (AboveOf5 ?x_25)
            )
          )
          (exists (?x_0 )
            (and
              (LeftOf11 ?x_0)
              (RightOf15 ?x_0)
            )
          )
          (exists (?x_26 )
            (and
              (LeftOf2 ?x_26)
              (RightOf16 ?x_26)
            )
          )
          (exists (?x_16 )
            (and
              (RightOf12 ?x_16)
              (LeftOf2 ?x_16)
            )
          )
          (exists (?x_27 )
            (and
              (BelowOf10 ?x_27)
              (AboveOf15 ?x_27)
            )
          )
          (exists (?x_7 )
            (and
              (BelowOf4 ?x_7)
              (AboveOf17 ?x_7)
            )
          )
          (exists (?x_5 )
            (and
              (AboveOf14 ?x_5)
              (BelowOf1 ?x_5)
            )
          )
          (exists (?x_19 )
            (and
              (LeftOf10 ?x_19)
              (RightOf17 ?x_19)
            )
          )
          (exists (?x_10 )
            (and
              (RightOf16 ?x_10)
              (LeftOf6 ?x_10)
            )
          )
          (exists (?x_15 )
            (and
              (RightOf11 ?x_15)
              (LeftOf1 ?x_15)
            )
          )
          (exists (?x_25 )
            (and
              (BelowOf4 ?x_25)
              (AboveOf17 ?x_25)
            )
          )
          (exists (?x_31 )
            (and
              (BelowOf2 ?x_31)
              (AboveOf11 ?x_31)
            )
          )
          (exists (?x_25 )
            (and
              (AboveOf5 ?x_25)
              (BelowOf5 ?x_25)
            )
          )
          (exists (?x_22 )
            (and
              (AboveOf11 ?x_22)
              (BelowOf4 ?x_22)
            )
          )
          (exists (?x_28 )
            (and
              (RightOf17 ?x_28)
              (LeftOf3 ?x_28)
            )
          )
          (exists (?x_16 )
            (and
              (RightOf9 ?x_16)
              (LeftOf5 ?x_16)
            )
          )
          (exists (?x_30 )
            (and
              (BelowOf2 ?x_30)
              (AboveOf6 ?x_30)
            )
          )
          (exists (?x_5 )
            (and
              (BelowOf2 ?x_5)
              (AboveOf9 ?x_5)
            )
          )
          (exists (?x_29 )
            (and
              (LeftOf3 ?x_29)
              (RightOf11 ?x_29)
            )
          )
          (exists (?x_16 )
            (and
              (LeftOf2 ?x_16)
              (RightOf12 ?x_16)
            )
          )
          (exists (?x_21 )
            (and
              (BelowOf8 ?x_21)
              (AboveOf14 ?x_21)
            )
          )
          (exists (?x_13 )
            (and
              (LeftOf5 ?x_13)
              (RightOf12 ?x_13)
            )
          )
          (exists (?x_6 )
            (and
              (AboveOf8 ?x_6)
              (BelowOf5 ?x_6)
            )
          )
          (exists (?x_23 )
            (and
              (BelowOf10 ?x_23)
              (AboveOf15 ?x_23)
            )
          )
          (exists (?x_32 )
            (and
              (LeftOf11 ?x_32)
              (RightOf13 ?x_32)
            )
          )
          (exists (?x_21 )
            (and
              (AboveOf11 ?x_21)
              (BelowOf7 ?x_21)
            )
          )
          (exists (?x_15 )
            (and
              (LeftOf9 ?x_15)
              (RightOf17 ?x_15)
            )
          )
          (exists (?x_22 )
            (and
              (AboveOf14 ?x_22)
              (BelowOf10 ?x_22)
            )
          )
          (exists (?x_11 )
            (and
              (BelowOf5 ?x_11)
              (AboveOf16 ?x_11)
            )
          )
          (exists (?x_24 )
            (and
              (LeftOf3 ?x_24)
              (RightOf10 ?x_24)
            )
          )
          (exists (?x_1 )
            (and
              (BelowOf9 ?x_1)
              (AboveOf13 ?x_1)
            )
          )
          (exists (?x_1 )
            (and
              (BelowOf10 ?x_1)
              (AboveOf12 ?x_1)
            )
          )
          (exists (?x_16 )
            (and
              (RightOf12 ?x_16)
              (LeftOf1 ?x_16)
            )
          )
          (exists (?x_28 )
            (and
              (LeftOf4 ?x_28)
              (RightOf8 ?x_28)
            )
          )
          (exists (?x_6 )
            (and
              (BelowOf2 ?x_6)
              (AboveOf16 ?x_6)
            )
          )
          (exists (?x_6 )
            (and
              (BelowOf5 ?x_6)
              (AboveOf14 ?x_6)
            )
          )
          (exists (?x_1 )
            (and
              (BelowOf8 ?x_1)
              (AboveOf16 ?x_1)
            )
          )
          (exists (?x_32 )
            (and
              (RightOf12 ?x_32)
              (LeftOf7 ?x_32)
            )
          )
          (exists (?x_25 )
            (and
              (BelowOf4 ?x_25)
              (AboveOf5 ?x_25)
            )
          )
          (exists (?x_2 )
            (and
              (LeftOf1 ?x_2)
              (RightOf2 ?x_2)
            )
          )
          (exists (?x_31 )
            (and
              (AboveOf10 ?x_31)
              (BelowOf3 ?x_31)
            )
          )
          (exists (?x_22 )
            (and
              (BelowOf11 ?x_22)
              (AboveOf15 ?x_22)
            )
          )
          (exists (?x_32 )
            (and
              (RightOf12 ?x_32)
              (LeftOf2 ?x_32)
            )
          )
          (exists (?x_0 )
            (and
              (LeftOf10 ?x_0)
              (RightOf17 ?x_0)
            )
          )
          (exists (?x_28 )
            (and
              (RightOf7 ?x_28)
              (LeftOf3 ?x_28)
            )
          )
          (exists (?x_22 )
            (and
              (BelowOf3 ?x_22)
              (AboveOf15 ?x_22)
            )
          )
          (exists (?x_12 )
            (and
              (LeftOf7 ?x_12)
              (RightOf14 ?x_12)
            )
          )
          (exists (?x_24 )
            (and
              (LeftOf3 ?x_24)
              (RightOf13 ?x_24)
            )
          )
          (exists (?x_6 )
            (and
              (AboveOf7 ?x_6)
              (BelowOf6 ?x_6)
            )
          )
          (exists (?x_32 )
            (and
              (RightOf15 ?x_32)
              (LeftOf2 ?x_32)
            )
          )
          (exists (?x_0 )
            (and
              (RightOf14 ?x_0)
              (LeftOf3 ?x_0)
            )
          )
          (exists (?x_25 )
            (and
              (BelowOf2 ?x_25)
              (AboveOf7 ?x_25)
            )
          )
          (exists (?x_9 )
            (and
              (LeftOf5 ?x_9)
              (RightOf15 ?x_9)
            )
          )
          (exists (?x_22 )
            (and
              (AboveOf14 ?x_22)
              (BelowOf1 ?x_22)
            )
          )
          (exists (?x_28 )
            (and
              (RightOf17 ?x_28)
              (LeftOf4 ?x_28)
            )
          )
          (exists (?x_15 )
            (and
              (RightOf15 ?x_15)
              (LeftOf7 ?x_15)
            )
          )
          (exists (?x_26 )
            (and
              (RightOf17 ?x_26)
              (LeftOf3 ?x_26)
            )
          )
          (exists (?x_9 )
            (and
              (LeftOf7 ?x_9)
              (RightOf16 ?x_9)
            )
          )
          (exists (?x_3 )
            (and
              (LeftOf1 ?x_3)
              (RightOf4 ?x_3)
            )
          )
          (exists (?x_13 )
            (and
              (LeftOf4 ?x_13)
              (RightOf11 ?x_13)
            )
          )
          (exists (?x_11 )
            (and
              (AboveOf13 ?x_11)
              (BelowOf7 ?x_11)
            )
          )
          (exists (?x_31 )
            (and
              (BelowOf1 ?x_31)
              (AboveOf9 ?x_31)
            )
          )
          (exists (?x_15 )
            (and
              (LeftOf4 ?x_15)
              (RightOf14 ?x_15)
            )
          )
          (exists (?x_10 )
            (and
              (LeftOf4 ?x_10)
              (RightOf9 ?x_10)
            )
          )
          (exists (?x_4 )
            (and
              (AboveOf17 ?x_4)
              (BelowOf6 ?x_4)
            )
          )
          (exists (?x_15 )
            (and
              (LeftOf9 ?x_15)
              (RightOf13 ?x_15)
            )
          )
          (exists (?x_21 )
            (and
              (AboveOf8 ?x_21)
              (BelowOf3 ?x_21)
            )
          )
          (exists (?x_17 )
            (and
              (AboveOf7 ?x_17)
              (BelowOf1 ?x_17)
            )
          )
          (exists (?x_32 )
            (and
              (RightOf17 ?x_32)
              (LeftOf12 ?x_32)
            )
          )
          (exists (?x_27 )
            (and
              (AboveOf15 ?x_27)
              (BelowOf1 ?x_27)
            )
          )
          (exists (?x_31 )
            (and
              (AboveOf10 ?x_31)
              (BelowOf6 ?x_31)
            )
          )
          (exists (?x_7 )
            (and
              (AboveOf16 ?x_7)
              (BelowOf13 ?x_7)
            )
          )
          (exists (?x_31 )
            (and
              (BelowOf1 ?x_31)
              (AboveOf15 ?x_31)
            )
          )
          (exists (?x_16 )
            (and
              (LeftOf5 ?x_16)
              (RightOf11 ?x_16)
            )
          )
          (exists (?x_1 )
            (and
              (AboveOf13 ?x_1)
              (BelowOf6 ?x_1)
            )
          )
          (exists (?x_13 )
            (and
              (LeftOf4 ?x_13)
              (RightOf13 ?x_13)
            )
          )
          (exists (?x_29 )
            (and
              (RightOf12 ?x_29)
              (LeftOf2 ?x_29)
            )
          )
          (exists (?x_15 )
            (and
              (LeftOf7 ?x_15)
              (RightOf17 ?x_15)
            )
          )
          (exists (?x_13 )
            (and
              (LeftOf8 ?x_13)
              (RightOf14 ?x_13)
            )
          )
          (exists (?x_22 )
            (and
              (BelowOf3 ?x_22)
              (AboveOf14 ?x_22)
            )
          )
          (exists (?x_29 )
            (and
              (RightOf12 ?x_29)
              (LeftOf1 ?x_29)
            )
          )
          (exists (?x_1 )
            (and
              (BelowOf7 ?x_1)
              (AboveOf11 ?x_1)
            )
          )
          (exists (?x_29 )
            (and
              (RightOf13 ?x_29)
              (LeftOf3 ?x_29)
            )
          )
          (exists (?x_0 )
            (and
              (RightOf14 ?x_0)
              (LeftOf6 ?x_0)
            )
          )
          (exists (?x_10 )
            (and
              (LeftOf2 ?x_10)
              (RightOf16 ?x_10)
            )
          )
          (exists (?x_16 )
            (and
              (LeftOf6 ?x_16)
              (RightOf16 ?x_16)
            )
          )
          (exists (?x_33 )
            (and
              (LeftOf2 ?x_33)
              (RightOf16 ?x_33)
            )
          )
          (exists (?x_16 )
            (and
              (RightOf6 ?x_16)
              (LeftOf6 ?x_16)
            )
          )
          (exists (?x_32 )
            (and
              (LeftOf3 ?x_32)
              (RightOf15 ?x_32)
            )
          )
          (exists (?x_21 )
            (and
              (BelowOf4 ?x_21)
              (AboveOf9 ?x_21)
            )
          )
          (exists (?x_17 )
            (and
              (AboveOf13 ?x_17)
              (BelowOf1 ?x_17)
            )
          )
          (exists (?x_23 )
            (and
              (BelowOf8 ?x_23)
              (AboveOf15 ?x_23)
            )
          )
          (exists (?x_27 )
            (and
              (AboveOf16 ?x_27)
              (BelowOf9 ?x_27)
            )
          )
          (exists (?x_9 )
            (and
              (LeftOf7 ?x_9)
              (RightOf13 ?x_9)
            )
          )
          (exists (?x_9 )
            (and
              (LeftOf4 ?x_9)
              (RightOf9 ?x_9)
            )
          )
          (exists (?x_23 )
            (and
              (AboveOf15 ?x_23)
              (BelowOf1 ?x_23)
            )
          )
          (exists (?x_25 )
            (and
              (AboveOf7 ?x_25)
              (BelowOf5 ?x_25)
            )
          )
          (exists (?x_8 )
            (and
              (RightOf17 ?x_8)
              (LeftOf1 ?x_8)
            )
          )
          (exists (?x_11 )
            (and
              (AboveOf14 ?x_11)
              (BelowOf9 ?x_11)
            )
          )
          (exists (?x_30 )
            (and
              (BelowOf4 ?x_30)
              (AboveOf12 ?x_30)
            )
          )
          (exists (?x_25 )
            (and
              (BelowOf5 ?x_25)
              (AboveOf6 ?x_25)
            )
          )
          (exists (?x_18 )
            (and
              (AboveOf16 ?x_18)
              (BelowOf1 ?x_18)
            )
          )
          (exists (?x_3 )
            (and
              (LeftOf1 ?x_3)
              (RightOf17 ?x_3)
            )
          )
          (exists (?x_1 )
            (and
              (BelowOf2 ?x_1)
              (AboveOf11 ?x_1)
            )
          )
          (exists (?x_14 )
            (and
              (AboveOf11 ?x_14)
              (BelowOf2 ?x_14)
            )
          )
          (exists (?x_20 )
            (and
              (AboveOf7 ?x_20)
              (BelowOf2 ?x_20)
            )
          )
          (exists (?x_28 )
            (and
              (RightOf4 ?x_28)
              (LeftOf2 ?x_28)
            )
          )
          (exists (?x_12 )
            (and
              (LeftOf9 ?x_12)
              (RightOf14 ?x_12)
            )
          )
          (exists (?x_27 )
            (and
              (AboveOf15 ?x_27)
              (BelowOf12 ?x_27)
            )
          )
          (exists (?x_24 )
            (and
              (LeftOf3 ?x_24)
              (RightOf9 ?x_24)
            )
          )
          (exists (?x_17 )
            (and
              (AboveOf9 ?x_17)
              (BelowOf1 ?x_17)
            )
          )
          (exists (?x_19 )
            (and
              (LeftOf5 ?x_19)
              (RightOf17 ?x_19)
            )
          )
          (exists (?x_28 )
            (and
              (LeftOf3 ?x_28)
              (RightOf11 ?x_28)
            )
          )
          (exists (?x_14 )
            (and
              (AboveOf15 ?x_14)
              (BelowOf7 ?x_14)
            )
          )
          (exists (?x_20 )
            (and
              (BelowOf1 ?x_20)
              (AboveOf4 ?x_20)
            )
          )
          (exists (?x_22 )
            (and
              (BelowOf11 ?x_22)
              (AboveOf14 ?x_22)
            )
          )
          (exists (?x_15 )
            (and
              (LeftOf3 ?x_15)
              (RightOf16 ?x_15)
            )
          )
          (exists (?x_16 )
            (and
              (LeftOf2 ?x_16)
              (RightOf7 ?x_16)
            )
          )
          (exists (?x_23 )
            (and
              (AboveOf17 ?x_23)
              (BelowOf9 ?x_23)
            )
          )
          (exists (?x_16 )
            (and
              (RightOf17 ?x_16)
              (LeftOf4 ?x_16)
            )
          )
          (exists (?x_33 )
            (and
              (LeftOf4 ?x_33)
              (RightOf10 ?x_33)
            )
          )
          (exists (?x_20 )
            (and
              (AboveOf8 ?x_20)
              (BelowOf1 ?x_20)
            )
          )
          (exists (?x_10 )
            (and
              (LeftOf4 ?x_10)
              (RightOf10 ?x_10)
            )
          )
          (exists (?x_25 )
            (and
              (BelowOf1 ?x_25)
              (AboveOf14 ?x_25)
            )
          )
          (exists (?x_9 )
            (and
              (LeftOf3 ?x_9)
              (RightOf12 ?x_9)
            )
          )
          (exists (?x_33 )
            (and
              (RightOf11 ?x_33)
              (LeftOf1 ?x_33)
            )
          )
          (exists (?x_30 )
            (and
              (BelowOf1 ?x_30)
              (AboveOf14 ?x_30)
            )
          )
          (exists (?x_28 )
            (and
              (RightOf13 ?x_28)
              (LeftOf3 ?x_28)
            )
          )
          (exists (?x_10 )
            (and
              (RightOf16 ?x_10)
              (LeftOf5 ?x_10)
            )
          )
          (exists (?x_12 )
            (and
              (LeftOf12 ?x_12)
              (RightOf16 ?x_12)
            )
          )
          (exists (?x_6 )
            (and
              (AboveOf10 ?x_6)
              (BelowOf4 ?x_6)
            )
          )
          (exists (?x_13 )
            (and
              (RightOf10 ?x_13)
              (LeftOf7 ?x_13)
            )
          )
          (exists (?x_6 )
            (and
              (BelowOf1 ?x_6)
              (AboveOf11 ?x_6)
            )
          )
          (exists (?x_0 )
            (and
              (RightOf15 ?x_0)
              (LeftOf3 ?x_0)
            )
          )
          (exists (?x_30 )
            (and
              (AboveOf13 ?x_30)
              (BelowOf1 ?x_30)
            )
          )
          (exists (?x_14 )
            (and
              (AboveOf10 ?x_14)
              (BelowOf1 ?x_14)
            )
          )
          (exists (?x_22 )
            (and
              (AboveOf17 ?x_22)
              (BelowOf7 ?x_22)
            )
          )
          (exists (?x_15 )
            (and
              (LeftOf6 ?x_15)
              (RightOf10 ?x_15)
            )
          )
          (exists (?x_6 )
            (and
              (AboveOf11 ?x_6)
              (BelowOf6 ?x_6)
            )
          )
          (exists (?x_18 )
            (and
              (BelowOf11 ?x_18)
              (AboveOf12 ?x_18)
            )
          )
          (exists (?x_22 )
            (and
              (BelowOf11 ?x_22)
              (AboveOf13 ?x_22)
            )
          )
          (exists (?x_15 )
            (and
              (LeftOf8 ?x_15)
              (RightOf12 ?x_15)
            )
          )
          (exists (?x_12 )
            (and
              (LeftOf4 ?x_12)
              (RightOf14 ?x_12)
            )
          )
          (exists (?x_6 )
            (and
              (AboveOf6 ?x_6)
              (BelowOf4 ?x_6)
            )
          )
          (exists (?x_22 )
            (and
              (BelowOf7 ?x_22)
              (AboveOf15 ?x_22)
            )
          )
          (exists (?x_14 )
            (and
              (AboveOf13 ?x_14)
              (BelowOf5 ?x_14)
            )
          )
          (exists (?x_31 )
            (and
              (BelowOf2 ?x_31)
              (AboveOf7 ?x_31)
            )
          )
          (exists (?x_14 )
            (and
              (AboveOf15 ?x_14)
              (BelowOf2 ?x_14)
            )
          )
          (exists (?x_32 )
            (and
              (RightOf15 ?x_32)
              (LeftOf12 ?x_32)
            )
          )
          (exists (?x_28 )
            (and
              (RightOf17 ?x_28)
              (LeftOf2 ?x_28)
            )
          )
          (exists (?x_9 )
            (and
              (LeftOf4 ?x_9)
              (RightOf8 ?x_9)
            )
          )
          (exists (?x_31 )
            (and
              (AboveOf16 ?x_31)
              (BelowOf6 ?x_31)
            )
          )
          (exists (?x_23 )
            (and
              (BelowOf14 ?x_23)
              (AboveOf15 ?x_23)
            )
          )
          (exists (?x_13 )
            (and
              (RightOf13 ?x_13)
              (LeftOf6 ?x_13)
            )
          )
          (exists (?x_27 )
            (and
              (AboveOf15 ?x_27)
              (BelowOf4 ?x_27)
            )
          )
          (exists (?x_1 )
            (and
              (AboveOf10 ?x_1)
              (BelowOf1 ?x_1)
            )
          )
          (exists (?x_22 )
            (and
              (AboveOf11 ?x_22)
              (BelowOf2 ?x_22)
            )
          )
          (exists (?x_1 )
            (and
              (AboveOf10 ?x_1)
              (BelowOf5 ?x_1)
            )
          )
          (exists (?x_10 )
            (and
              (RightOf11 ?x_10)
              (LeftOf5 ?x_10)
            )
          )
          (exists (?x_9 )
            (and
              (LeftOf5 ?x_9)
              (RightOf12 ?x_9)
            )
          )
          (exists (?x_10 )
            (and
              (RightOf10 ?x_10)
              (LeftOf4 ?x_10)
            )
          )
          (exists (?x_12 )
            (and
              (RightOf15 ?x_12)
              (LeftOf13 ?x_12)
            )
          )
          (exists (?x_25 )
            (and
              (BelowOf4 ?x_25)
              (AboveOf13 ?x_25)
            )
          )
          (exists (?x_30 )
            (and
              (AboveOf9 ?x_30)
              (BelowOf1 ?x_30)
            )
          )
          (exists (?x_16 )
            (and
              (RightOf12 ?x_16)
              (LeftOf4 ?x_16)
            )
          )
          (exists (?x_13 )
            (and
              (LeftOf2 ?x_13)
              (RightOf13 ?x_13)
            )
          )
          (exists (?x_8 )
            (and
              (RightOf17 ?x_8)
              (LeftOf7 ?x_8)
            )
          )
          (exists (?x_14 )
            (and
              (BelowOf1 ?x_14)
              (AboveOf11 ?x_14)
            )
          )
          (exists (?x_0 )
            (and
              (RightOf13 ?x_0)
              (LeftOf6 ?x_0)
            )
          )
          (exists (?x_29 )
            (and
              (RightOf15 ?x_29)
              (LeftOf3 ?x_29)
            )
          )
          (exists (?x_13 )
            (and
              (LeftOf9 ?x_13)
              (RightOf17 ?x_13)
            )
          )
          (exists (?x_0 )
            (and
              (LeftOf7 ?x_0)
              (RightOf17 ?x_0)
            )
          )
          (exists (?x_13 )
            (and
              (RightOf10 ?x_13)
              (LeftOf1 ?x_13)
            )
          )
          (exists (?x_22 )
            (and
              (AboveOf14 ?x_22)
              (BelowOf7 ?x_22)
            )
          )
          (exists (?x_6 )
            (and
              (AboveOf8 ?x_6)
              (BelowOf3 ?x_6)
            )
          )
          (exists (?x_33 )
            (and
              (RightOf6 ?x_33)
              (LeftOf2 ?x_33)
            )
          )
          (exists (?x_33 )
            (and
              (LeftOf5 ?x_33)
              (RightOf15 ?x_33)
            )
          )
          (exists (?x_13 )
            (and
              (RightOf10 ?x_13)
              (LeftOf3 ?x_13)
            )
          )
          (exists (?x_19 )
            (and
              (RightOf17 ?x_19)
              (LeftOf6 ?x_19)
            )
          )
          (exists (?x_26 )
            (and
              (LeftOf12 ?x_26)
              (RightOf16 ?x_26)
            )
          )
          (exists (?x_33 )
            (and
              (LeftOf5 ?x_33)
              (RightOf6 ?x_33)
            )
          )
          (exists (?x_13 )
            (and
              (RightOf14 ?x_13)
              (LeftOf3 ?x_13)
            )
          )
          (exists (?x_15 )
            (and
              (RightOf13 ?x_15)
              (LeftOf2 ?x_15)
            )
          )
          (exists (?x_33 )
            (and
              (RightOf7 ?x_33)
              (LeftOf1 ?x_33)
            )
          )
          (exists (?x_29 )
            (and
              (RightOf12 ?x_29)
              (LeftOf3 ?x_29)
            )
          )
          (exists (?x_30 )
            (and
              (BelowOf1 ?x_30)
              (AboveOf8 ?x_30)
            )
          )
          (exists (?x_22 )
            (and
              (AboveOf12 ?x_22)
              (BelowOf7 ?x_22)
            )
          )
          (exists (?x_32 )
            (and
              (LeftOf12 ?x_32)
              (RightOf16 ?x_32)
            )
          )
          (exists (?x_1 )
            (and
              (BelowOf5 ?x_1)
              (AboveOf17 ?x_1)
            )
          )
          (exists (?x_25 )
            (and
              (AboveOf5 ?x_25)
              (BelowOf3 ?x_25)
            )
          )
          (exists (?x_30 )
            (and
              (BelowOf1 ?x_30)
              (AboveOf9 ?x_30)
            )
          )
          (exists (?x_8 )
            (and
              (RightOf17 ?x_8)
              (LeftOf8 ?x_8)
            )
          )
          (exists (?x_31 )
            (and
              (BelowOf7 ?x_31)
              (AboveOf12 ?x_31)
            )
          )
          (exists (?x_33 )
            (and
              (LeftOf4 ?x_33)
              (RightOf13 ?x_33)
            )
          )
          (exists (?x_13 )
            (and
              (RightOf15 ?x_13)
              (LeftOf8 ?x_13)
            )
          )
          (exists (?x_29 )
            (and
              (RightOf14 ?x_29)
              (LeftOf9 ?x_29)
            )
          )
          (exists (?x_15 )
            (and
              (LeftOf9 ?x_15)
              (RightOf11 ?x_15)
            )
          )
          (exists (?x_1 )
            (and
              (BelowOf6 ?x_1)
              (AboveOf12 ?x_1)
            )
          )
          (exists (?x_9 )
            (and
              (RightOf10 ?x_9)
              (LeftOf3 ?x_9)
            )
          )
          (exists (?x_16 )
            (and
              (LeftOf5 ?x_16)
              (RightOf8 ?x_16)
            )
          )
          (exists (?x_32 )
            (and
              (RightOf17 ?x_32)
              (LeftOf9 ?x_32)
            )
          )
          (exists (?x_6 )
            (and
              (AboveOf6 ?x_6)
              (BelowOf3 ?x_6)
            )
          )
          (exists (?x_19 )
            (and
              (RightOf17 ?x_19)
              (LeftOf13 ?x_19)
            )
          )
          (exists (?x_21 )
            (and
              (AboveOf11 ?x_21)
              (BelowOf4 ?x_21)
            )
          )
          (exists (?x_0 )
            (and
              (LeftOf7 ?x_0)
              (RightOf16 ?x_0)
            )
          )
          (exists (?x_29 )
            (and
              (LeftOf7 ?x_29)
              (RightOf15 ?x_29)
            )
          )
          (exists (?x_18 )
            (and
              (BelowOf2 ?x_18)
              (AboveOf17 ?x_18)
            )
          )
          (exists (?x_22 )
            (and
              (BelowOf8 ?x_22)
              (AboveOf12 ?x_22)
            )
          )
          (exists (?x_33 )
            (and
              (LeftOf3 ?x_33)
              (RightOf10 ?x_33)
            )
          )
          (exists (?x_8 )
            (and
              (LeftOf11 ?x_8)
              (RightOf17 ?x_8)
            )
          )
          (exists (?x_4 )
            (and
              (BelowOf17 ?x_4)
              (AboveOf17 ?x_4)
            )
          )
          (exists (?x_11 )
            (and
              (BelowOf1 ?x_11)
              (AboveOf13 ?x_11)
            )
          )
          (exists (?x_10 )
            (and
              (LeftOf7 ?x_10)
              (RightOf14 ?x_10)
            )
          )
          (exists (?x_15 )
            (and
              (RightOf14 ?x_15)
              (LeftOf5 ?x_15)
            )
          )
          (exists (?x_33 )
            (and
              (LeftOf3 ?x_33)
              (RightOf16 ?x_33)
            )
          )
          (exists (?x_22 )
            (and
              (AboveOf14 ?x_22)
              (BelowOf2 ?x_22)
            )
          )
          (exists (?x_14 )
            (and
              (AboveOf9 ?x_14)
              (BelowOf7 ?x_14)
            )
          )
          (exists (?x_14 )
            (and
              (AboveOf15 ?x_14)
              (BelowOf9 ?x_14)
            )
          )
          (exists (?x_26 )
            (and
              (RightOf15 ?x_26)
              (LeftOf2 ?x_26)
            )
          )
          (exists (?x_15 )
            (and
              (RightOf9 ?x_15)
              (LeftOf8 ?x_15)
            )
          )
          (exists (?x_11 )
            (and
              (BelowOf8 ?x_11)
              (AboveOf14 ?x_11)
            )
          )
          (exists (?x_5 )
            (and
              (AboveOf14 ?x_5)
              (BelowOf3 ?x_5)
            )
          )
          (exists (?x_13 )
            (and
              (RightOf10 ?x_13)
              (LeftOf10 ?x_13)
            )
          )
          (exists (?x_23 )
            (and
              (AboveOf14 ?x_23)
              (BelowOf8 ?x_23)
            )
          )
          (exists (?x_29 )
            (and
              (RightOf17 ?x_29)
              (LeftOf2 ?x_29)
            )
          )
          (exists (?x_22 )
            (and
              (BelowOf8 ?x_22)
              (AboveOf15 ?x_22)
            )
          )
          (exists (?x_21 )
            (and
              (BelowOf8 ?x_21)
              (AboveOf9 ?x_21)
            )
          )
          (exists (?x_23 )
            (and
              (BelowOf7 ?x_23)
              (AboveOf15 ?x_23)
            )
          )
          (exists (?x_23 )
            (and
              (BelowOf6 ?x_23)
              (AboveOf17 ?x_23)
            )
          )
          (exists (?x_1 )
            (and
              (BelowOf2 ?x_1)
              (AboveOf17 ?x_1)
            )
          )
          (exists (?x_15 )
            (and
              (LeftOf8 ?x_15)
              (RightOf17 ?x_15)
            )
          )
          (exists (?x_24 )
            (and
              (LeftOf2 ?x_24)
              (RightOf3 ?x_24)
            )
          )
          (exists (?x_14 )
            (and
              (AboveOf14 ?x_14)
              (BelowOf8 ?x_14)
            )
          )
          (exists (?x_18 )
            (and
              (BelowOf8 ?x_18)
              (AboveOf17 ?x_18)
            )
          )
          (exists (?x_13 )
            (and
              (LeftOf4 ?x_13)
              (RightOf12 ?x_13)
            )
          )
          (exists (?x_2 )
            (and
              (RightOf6 ?x_2)
              (LeftOf1 ?x_2)
            )
          )
          (exists (?x_10 )
            (and
              (RightOf10 ?x_10)
              (LeftOf3 ?x_10)
            )
          )
          (exists (?x_25 )
            (and
              (AboveOf11 ?x_25)
              (BelowOf3 ?x_25)
            )
          )
          (exists (?x_15 )
            (and
              (RightOf16 ?x_15)
              (LeftOf7 ?x_15)
            )
          )
          (exists (?x_14 )
            (and
              (BelowOf3 ?x_14)
              (AboveOf15 ?x_14)
            )
          )
          (exists (?x_29 )
            (and
              (LeftOf7 ?x_29)
              (RightOf14 ?x_29)
            )
          )
          (exists (?x_1 )
            (and
              (BelowOf1 ?x_1)
              (AboveOf14 ?x_1)
            )
          )
          (exists (?x_4 )
            (and
              (BelowOf7 ?x_4)
              (AboveOf17 ?x_4)
            )
          )
          (exists (?x_29 )
            (and
              (LeftOf4 ?x_29)
              (RightOf15 ?x_29)
            )
          )
          (exists (?x_28 )
            (and
              (LeftOf2 ?x_28)
              (RightOf10 ?x_28)
            )
          )
          (exists (?x_15 )
            (and
              (RightOf15 ?x_15)
              (LeftOf2 ?x_15)
            )
          )
          (exists (?x_16 )
            (and
              (LeftOf3 ?x_16)
              (RightOf11 ?x_16)
            )
          )
          (exists (?x_23 )
            (and
              (BelowOf11 ?x_23)
              (AboveOf17 ?x_23)
            )
          )
          (exists (?x_18 )
            (and
              (AboveOf16 ?x_18)
              (BelowOf6 ?x_18)
            )
          )
          (exists (?x_14 )
            (and
              (BelowOf1 ?x_14)
              (AboveOf13 ?x_14)
            )
          )
          (exists (?x_6 )
            (and
              (AboveOf13 ?x_6)
              (BelowOf3 ?x_6)
            )
          )
          (exists (?x_22 )
            (and
              (BelowOf1 ?x_22)
              (AboveOf16 ?x_22)
            )
          )
          (exists (?x_3 )
            (and
              (RightOf15 ?x_3)
              (LeftOf2 ?x_3)
            )
          )
          (exists (?x_22 )
            (and
              (BelowOf10 ?x_22)
              (AboveOf13 ?x_22)
            )
          )
          (exists (?x_3 )
            (and
              (RightOf9 ?x_3)
              (LeftOf2 ?x_3)
            )
          )
          (exists (?x_32 )
            (and
              (LeftOf5 ?x_32)
              (RightOf13 ?x_32)
            )
          )
          (exists (?x_18 )
            (and
              (AboveOf14 ?x_18)
              (BelowOf1 ?x_18)
            )
          )
          (exists (?x_31 )
            (and
              (AboveOf9 ?x_31)
              (BelowOf1 ?x_31)
            )
          )
          (exists (?x_15 )
            (and
              (LeftOf6 ?x_15)
              (RightOf17 ?x_15)
            )
          )
          (exists (?x_10 )
            (and
              (RightOf10 ?x_10)
              (LeftOf5 ?x_10)
            )
          )
          (exists (?x_27 )
            (and
              (AboveOf16 ?x_27)
              (BelowOf14 ?x_27)
            )
          )
          (exists (?x_11 )
            (and
              (BelowOf3 ?x_11)
              (AboveOf14 ?x_11)
            )
          )
          (exists (?x_0 )
            (and
              (RightOf13 ?x_0)
              (LeftOf7 ?x_0)
            )
          )
          (exists (?x_13 )
            (and
              (RightOf16 ?x_13)
              (LeftOf2 ?x_13)
            )
          )
          (exists (?x_29 )
            (and
              (RightOf15 ?x_29)
              (LeftOf9 ?x_29)
            )
          )
          (exists (?x_29 )
            (and
              (RightOf16 ?x_29)
              (LeftOf6 ?x_29)
            )
          )
          (exists (?x_25 )
            (and
              (AboveOf10 ?x_25)
              (BelowOf5 ?x_25)
            )
          )
          (exists (?x_24 )
            (and
              (LeftOf2 ?x_24)
              (RightOf5 ?x_24)
            )
          )
          (exists (?x_29 )
            (and
              (LeftOf4 ?x_29)
              (RightOf14 ?x_29)
            )
          )
          (exists (?x_33 )
            (and
              (RightOf9 ?x_33)
              (LeftOf4 ?x_33)
            )
          )
          (exists (?x_20 )
            (and
              (BelowOf1 ?x_20)
              (AboveOf10 ?x_20)
            )
          )
          (exists (?x_24 )
            (and
              (RightOf8 ?x_24)
              (LeftOf1 ?x_24)
            )
          )
          (exists (?x_14 )
            (and
              (BelowOf6 ?x_14)
              (AboveOf12 ?x_14)
            )
          )
          (exists (?x_28 )
            (and
              (RightOf4 ?x_28)
              (LeftOf3 ?x_28)
            )
          )
          (exists (?x_6 )
            (and
              (AboveOf13 ?x_6)
              (BelowOf6 ?x_6)
            )
          )
          (exists (?x_1 )
            (and
              (AboveOf14 ?x_1)
              (BelowOf4 ?x_1)
            )
          )
          (exists (?x_10 )
            (and
              (LeftOf7 ?x_10)
              (RightOf9 ?x_10)
            )
          )
          (exists (?x_16 )
            (and
              (RightOf15 ?x_16)
              (LeftOf4 ?x_16)
            )
          )
          (exists (?x_10 )
            (and
              (LeftOf3 ?x_10)
              (RightOf11 ?x_10)
            )
          )
          (exists (?x_9 )
            (and
              (LeftOf8 ?x_9)
              (RightOf13 ?x_9)
            )
          )
          (exists (?x_15 )
            (and
              (LeftOf4 ?x_15)
              (RightOf15 ?x_15)
            )
          )
          (exists (?x_9 )
            (and
              (LeftOf4 ?x_9)
              (RightOf14 ?x_9)
            )
          )
          (exists (?x_15 )
            (and
              (LeftOf5 ?x_15)
              (RightOf12 ?x_15)
            )
          )
          (exists (?x_13 )
            (and
              (LeftOf1 ?x_13)
              (RightOf17 ?x_13)
            )
          )
          (exists (?x_30 )
            (and
              (AboveOf11 ?x_30)
              (BelowOf3 ?x_30)
            )
          )
          (exists (?x_0 )
            (and
              (RightOf13 ?x_0)
              (LeftOf11 ?x_0)
            )
          )
          (exists (?x_29 )
            (and
              (LeftOf10 ?x_29)
              (RightOf12 ?x_29)
            )
          )
          (exists (?x_15 )
            (and
              (LeftOf5 ?x_15)
              (RightOf10 ?x_15)
            )
          )
          (exists (?x_22 )
            (and
              (AboveOf14 ?x_22)
              (BelowOf8 ?x_22)
            )
          )
          (exists (?x_14 )
            (and
              (AboveOf17 ?x_14)
              (BelowOf8 ?x_14)
            )
          )
          (exists (?x_0 )
            (and
              (RightOf16 ?x_0)
              (LeftOf11 ?x_0)
            )
          )
          (exists (?x_28 )
            (and
              (LeftOf2 ?x_28)
              (RightOf8 ?x_28)
            )
          )
          (exists (?x_22 )
            (and
              (BelowOf6 ?x_22)
              (AboveOf16 ?x_22)
            )
          )
          (exists (?x_15 )
            (and
              (RightOf11 ?x_15)
              (LeftOf8 ?x_15)
            )
          )
          (exists (?x_29 )
            (and
              (RightOf17 ?x_29)
              (LeftOf10 ?x_29)
            )
          )
          (exists (?x_12 )
            (and
              (RightOf16 ?x_12)
              (LeftOf10 ?x_12)
            )
          )
          (exists (?x_9 )
            (and
              (RightOf14 ?x_9)
              (LeftOf1 ?x_9)
            )
          )
          (exists (?x_11 )
            (and
              (AboveOf17 ?x_11)
              (BelowOf12 ?x_11)
            )
          )
          (exists (?x_3 )
            (and
              (RightOf17 ?x_3)
              (LeftOf2 ?x_3)
            )
          )
          (exists (?x_21 )
            (and
              (AboveOf9 ?x_21)
              (BelowOf7 ?x_21)
            )
          )
          (exists (?x_31 )
            (and
              (AboveOf8 ?x_31)
              (BelowOf5 ?x_31)
            )
          )
          (exists (?x_1 )
            (and
              (BelowOf6 ?x_1)
              (AboveOf15 ?x_1)
            )
          )
          (exists (?x_30 )
            (and
              (BelowOf1 ?x_30)
              (AboveOf13 ?x_30)
            )
          )
          (exists (?x_31 )
            (and
              (AboveOf16 ?x_31)
              (BelowOf4 ?x_31)
            )
          )
          (exists (?x_31 )
            (and
              (AboveOf15 ?x_31)
              (BelowOf6 ?x_31)
            )
          )
          (exists (?x_8 )
            (and
              (RightOf16 ?x_8)
              (LeftOf3 ?x_8)
            )
          )
          (exists (?x_18 )
            (and
              (AboveOf13 ?x_18)
              (BelowOf12 ?x_18)
            )
          )
          (exists (?x_0 )
            (and
              (RightOf17 ?x_0)
              (LeftOf3 ?x_0)
            )
          )
          (exists (?x_9 )
            (and
              (RightOf17 ?x_9)
              (LeftOf6 ?x_9)
            )
          )
          (exists (?x_9 )
            (and
              (RightOf16 ?x_9)
              (LeftOf8 ?x_9)
            )
          )
          (exists (?x_31 )
            (and
              (AboveOf16 ?x_31)
              (BelowOf5 ?x_31)
            )
          )
          (exists (?x_14 )
            (and
              (AboveOf16 ?x_14)
              (BelowOf8 ?x_14)
            )
          )
          (exists (?x_23 )
            (and
              (AboveOf14 ?x_23)
              (BelowOf10 ?x_23)
            )
          )
          (exists (?x_22 )
            (and
              (AboveOf15 ?x_22)
              (BelowOf1 ?x_22)
            )
          )
          (exists (?x_31 )
            (and
              (BelowOf7 ?x_31)
              (AboveOf11 ?x_31)
            )
          )
          (exists (?x_31 )
            (and
              (AboveOf13 ?x_31)
              (BelowOf7 ?x_31)
            )
          )
          (exists (?x_13 )
            (and
              (LeftOf1 ?x_13)
              (RightOf16 ?x_13)
            )
          )
          (exists (?x_21 )
            (and
              (AboveOf13 ?x_21)
              (BelowOf1 ?x_21)
            )
          )
          (exists (?x_6 )
            (and
              (AboveOf10 ?x_6)
              (BelowOf3 ?x_6)
            )
          )
          (exists (?x_18 )
            (and
              (AboveOf15 ?x_18)
              (BelowOf4 ?x_18)
            )
          )
          (exists (?x_21 )
            (and
              (AboveOf17 ?x_21)
              (BelowOf5 ?x_21)
            )
          )
          (exists (?x_13 )
            (and
              (LeftOf9 ?x_13)
              (RightOf16 ?x_13)
            )
          )
          (exists (?x_16 )
            (and
              (LeftOf5 ?x_16)
              (RightOf13 ?x_16)
            )
          )
          (exists (?x_0 )
            (and
              (LeftOf13 ?x_0)
              (RightOf17 ?x_0)
            )
          )
          (exists (?x_23 )
            (and
              (AboveOf14 ?x_23)
              (BelowOf1 ?x_23)
            )
          )
          (exists (?x_16 )
            (and
              (RightOf6 ?x_16)
              (LeftOf5 ?x_16)
            )
          )
          (exists (?x_31 )
            (and
              (BelowOf4 ?x_31)
              (AboveOf10 ?x_31)
            )
          )
          (exists (?x_18 )
            (and
              (AboveOf14 ?x_18)
              (BelowOf4 ?x_18)
            )
          )
          (exists (?x_22 )
            (and
              (AboveOf11 ?x_22)
              (BelowOf1 ?x_22)
            )
          )
          (exists (?x_30 )
            (and
              (AboveOf7 ?x_30)
              (BelowOf2 ?x_30)
            )
          )
          (exists (?x_17 )
            (and
              (AboveOf8 ?x_17)
              (BelowOf1 ?x_17)
            )
          )
          (exists (?x_14 )
            (and
              (BelowOf6 ?x_14)
              (AboveOf13 ?x_14)
            )
          )
          (exists (?x_28 )
            (and
              (RightOf6 ?x_28)
              (LeftOf4 ?x_28)
            )
          )
          (exists (?x_11 )
            (and
              (AboveOf17 ?x_11)
              (BelowOf1 ?x_11)
            )
          )
          (exists (?x_16 )
            (and
              (LeftOf2 ?x_16)
              (RightOf14 ?x_16)
            )
          )
          (exists (?x_12 )
            (and
              (LeftOf1 ?x_12)
              (RightOf14 ?x_12)
            )
          )
          (exists (?x_28 )
            (and
              (RightOf15 ?x_28)
              (LeftOf1 ?x_28)
            )
          )
          (exists (?x_28 )
            (and
              (LeftOf3 ?x_28)
              (RightOf9 ?x_28)
            )
          )
          (exists (?x_6 )
            (and
              (BelowOf5 ?x_6)
              (AboveOf17 ?x_6)
            )
          )
          (exists (?x_16 )
            (and
              (RightOf17 ?x_16)
              (LeftOf3 ?x_16)
            )
          )
          (exists (?x_31 )
            (and
              (BelowOf5 ?x_31)
              (AboveOf11 ?x_31)
            )
          )
          (exists (?x_9 )
            (and
              (LeftOf2 ?x_9)
              (RightOf14 ?x_9)
            )
          )
          (exists (?x_16 )
            (and
              (LeftOf3 ?x_16)
              (RightOf14 ?x_16)
            )
          )
          (exists (?x_17 )
            (and
              (AboveOf11 ?x_17)
              (BelowOf1 ?x_17)
            )
          )
          (exists (?x_12 )
            (and
              (LeftOf14 ?x_12)
              (RightOf16 ?x_12)
            )
          )
          (exists (?x_23 )
            (and
              (AboveOf14 ?x_23)
              (BelowOf14 ?x_23)
            )
          )
          (exists (?x_29 )
            (and
              (LeftOf7 ?x_29)
              (RightOf13 ?x_29)
            )
          )
          (exists (?x_9 )
            (and
              (RightOf15 ?x_9)
              (LeftOf5 ?x_9)
            )
          )
          (exists (?x_21 )
            (and
              (AboveOf14 ?x_21)
              (BelowOf7 ?x_21)
            )
          )
          (exists (?x_27 )
            (and
              (BelowOf2 ?x_27)
              (AboveOf15 ?x_27)
            )
          )
          (exists (?x_0 )
            (and
              (LeftOf8 ?x_0)
              (RightOf17 ?x_0)
            )
          )
          (exists (?x_13 )
            (and
              (LeftOf7 ?x_13)
              (RightOf13 ?x_13)
            )
          )
          (exists (?x_25 )
            (and
              (BelowOf4 ?x_25)
              (AboveOf10 ?x_25)
            )
          )
          (exists (?x_15 )
            (and
              (LeftOf4 ?x_15)
              (RightOf16 ?x_15)
            )
          )
          (exists (?x_30 )
            (and
              (BelowOf4 ?x_30)
              (AboveOf11 ?x_30)
            )
          )
          (exists (?x_9 )
            (and
              (LeftOf5 ?x_9)
              (RightOf8 ?x_9)
            )
          )
          (exists (?x_28 )
            (and
              (RightOf14 ?x_28)
              (LeftOf3 ?x_28)
            )
          )
          (exists (?x_32 )
            (and
              (LeftOf6 ?x_32)
              (RightOf16 ?x_32)
            )
          )
          (exists (?x_9 )
            (and
              (RightOf9 ?x_9)
              (LeftOf1 ?x_9)
            )
          )
          (exists (?x_17 )
            (and
              (BelowOf1 ?x_17)
              (AboveOf10 ?x_17)
            )
          )
          (exists (?x_14 )
            (and
              (AboveOf15 ?x_14)
              (BelowOf6 ?x_14)
            )
          )
          (exists (?x_31 )
            (and
              (BelowOf2 ?x_31)
              (AboveOf12 ?x_31)
            )
          )
          (exists (?x_18 )
            (and
              (BelowOf10 ?x_18)
              (AboveOf17 ?x_18)
            )
          )
          (exists (?x_7 )
            (and
              (BelowOf9 ?x_7)
              (AboveOf17 ?x_7)
            )
          )
          (exists (?x_15 )
            (and
              (RightOf13 ?x_15)
              (LeftOf6 ?x_15)
            )
          )
          (exists (?x_9 )
            (and
              (LeftOf7 ?x_9)
              (RightOf17 ?x_9)
            )
          )
          (exists (?x_14 )
            (and
              (AboveOf12 ?x_14)
              (BelowOf2 ?x_14)
            )
          )
          (exists (?x_27 )
            (and
              (AboveOf15 ?x_27)
              (BelowOf14 ?x_27)
            )
          )
          (exists (?x_27 )
            (and
              (BelowOf15 ?x_27)
              (AboveOf16 ?x_27)
            )
          )
          (exists (?x_13 )
            (and
              (LeftOf1 ?x_13)
              (RightOf14 ?x_13)
            )
          )
          (exists (?x_32 )
            (and
              (LeftOf8 ?x_32)
              (RightOf12 ?x_32)
            )
          )
          (exists (?x_16 )
            (and
              (RightOf7 ?x_16)
              (LeftOf1 ?x_16)
            )
          )
          (exists (?x_25 )
            (and
              (BelowOf2 ?x_25)
              (AboveOf17 ?x_25)
            )
          )
          (exists (?x_21 )
            (and
              (AboveOf16 ?x_21)
              (BelowOf7 ?x_21)
            )
          )
          (exists (?x_29 )
            (and
              (RightOf16 ?x_29)
              (LeftOf5 ?x_29)
            )
          )
          (exists (?x_33 )
            (and
              (RightOf8 ?x_33)
              (LeftOf2 ?x_33)
            )
          )
          (exists (?x_6 )
            (and
              (BelowOf2 ?x_6)
              (AboveOf8 ?x_6)
            )
          )
          (exists (?x_18 )
            (and
              (AboveOf12 ?x_18)
              (BelowOf4 ?x_18)
            )
          )
          (exists (?x_0 )
            (and
              (RightOf17 ?x_0)
              (LeftOf5 ?x_0)
            )
          )
          (exists (?x_13 )
            (and
              (LeftOf8 ?x_13)
              (RightOf11 ?x_13)
            )
          )
          (exists (?x_15 )
            (and
              (RightOf14 ?x_15)
              (LeftOf6 ?x_15)
            )
          )
          (exists (?x_20 )
            (and
              (BelowOf2 ?x_20)
              (AboveOf2 ?x_20)
            )
          )
          (exists (?x_26 )
            (and
              (LeftOf15 ?x_26)
              (RightOf17 ?x_26)
            )
          )
          (exists (?x_0 )
            (and
              (LeftOf4 ?x_0)
              (RightOf16 ?x_0)
            )
          )
          (exists (?x_12 )
            (and
              (RightOf15 ?x_12)
              (LeftOf7 ?x_12)
            )
          )
          (exists (?x_14 )
            (and
              (BelowOf6 ?x_14)
              (AboveOf11 ?x_14)
            )
          )
          (exists (?x_13 )
            (and
              (LeftOf4 ?x_13)
              (RightOf15 ?x_13)
            )
          )
          (exists (?x_9 )
            (and
              (RightOf10 ?x_9)
              (LeftOf4 ?x_9)
            )
          )
          (exists (?x_21 )
            (and
              (AboveOf17 ?x_21)
              (BelowOf3 ?x_21)
            )
          )
          (exists (?x_5 )
            (and
              (BelowOf1 ?x_5)
              (AboveOf7 ?x_5)
            )
          )
          (exists (?x_16 )
            (and
              (RightOf6 ?x_16)
              (LeftOf2 ?x_16)
            )
          )
          (exists (?x_32 )
            (and
              (RightOf14 ?x_32)
              (LeftOf8 ?x_32)
            )
          )
        )
        (Error)
      )
    )
  )
)