(define (problem robotProblem )
  (:domain robot)
  (:objects
    robot 
  )
  (:init
    (LeftOf17 robot)
    (RightOf2 robot)
    (BelowOf17 robot)
    (AboveOf0 robot)
  )
  (:goal
    (and
      (not (CheckConsistency))
      (not (Error))
      (Row1 robot)
      (Column2 robot)
    )
  )
)