(define
  (domain robot)
  (:requirements :adl)
  (:predicates
    (AboveOf18 ?x )
    (AboveOf19 ?x )
    (AboveOf16 ?x )
    (AboveOf17 ?x )
    (AboveOf14 ?x )
    (AboveOf15 ?x )
    (AboveOf12 ?x )
    (AboveOf13 ?x )
    (AboveOf10 ?x )
    (AboveOf11 ?x )
    (BelowOf8 ?x )
    (BelowOf9 ?x )
    (AboveOf2 ?x )
    (BelowOf12 ?x )
    (BelowOf13 ?x )
    (BelowOf10 ?x )
    (BelowOf11 ?x )
    (BelowOf16 ?x )
    (BelowOf17 ?x )
    (BelowOf14 ?x )
    (BelowOf15 ?x )
    (BelowOf18 ?x )
    (BelowOf19 ?x )
    (LeftOf3 ?x )
    (LeftOf2 ?x )
    (LeftOf1 ?x )
    (LeftOf7 ?x )
    (LeftOf6 ?x )
    (LeftOf5 ?x )
    (LeftOf4 ?x )
    (LeftOf9 ?x )
    (LeftOf8 ?x )
    (Row18 ?x )
    (Row19 ?x )
    (BelowOf20 ?x )
    (Row12 ?x )
    (Row13 ?x )
    (Row10 ?x )
    (Row11 ?x )
    (Row16 ?x )
    (Row17 ?x )
    (Row14 ?x )
    (Row15 ?x )
    (LeftOf13 ?x )
    (Column19 ?x )
    (Column18 ?x )
    (LeftOf12 ?x )
    (Column11 ?x )
    (Column10 ?x )
    (Column13 ?x )
    (Column12 ?x )
    (Column15 ?x )
    (Column14 ?x )
    (Column17 ?x )
    (Column16 ?x )
    (LeftOf17 ?x )
    (LeftOf16 ?x )
    (LeftOf15 ?x )
    (LeftOf14 ?x )
    (AboveOf0 ?x )
    (AboveOf1 ?x )
    (LeftOf11 ?x )
    (AboveOf3 ?x )
    (AboveOf8 ?x )
    (AboveOf9 ?x )
    (LeftOf19 ?x )
    (LeftOf18 ?x )
    (RightOf9 ?x )
    (RightOf8 ?x )
    (RightOf3 ?x )
    (RightOf2 ?x )
    (RightOf1 ?x )
    (RightOf0 ?x )
    (RightOf7 ?x )
    (RightOf6 ?x )
    (RightOf5 ?x )
    (RightOf4 ?x )
    (Columns ?x )
    (LeftOf20 ?x )
    (BelowOf1 ?x )
    (BelowOf2 ?x )
    (BelowOf3 ?x )
    (Column9 ?x )
    (Column8 ?x )
    (BelowOf6 ?x )
    (BelowOf7 ?x )
    (Column5 ?x )
    (Column4 ?x )
    (Column7 ?x )
    (Column6 ?x )
    (Column1 ?x )
    (Column0 ?x )
    (Column3 ?x )
    (Column2 ?x )
    (Rows ?x )
    (AboveOf4 ?x )
    (AboveOf5 ?x )
    (AboveOf6 ?x )
    (AboveOf7 ?x )
    (RightOf19 ?x )
    (RightOf18 ?x )
    (RightOf17 ?x )
    (RightOf16 ?x )
    (RightOf15 ?x )
    (RightOf14 ?x )
    (RightOf13 ?x )
    (RightOf12 ?x )
    (RightOf11 ?x )
    (RightOf10 ?x )
    (Row0 ?x )
    (Row1 ?x )
    (Row2 ?x )
    (Row3 ?x )
    (Row4 ?x )
    (Row5 ?x )
    (Row6 ?x )
    (Row7 ?x )
    (Row8 ?x )
    (Row9 ?x )
    (LeftOf10 ?x )
    (BelowOf4 ?x )
    (BelowOf5 ?x )
    (CheckConsistency)
    (Error)
  )
  (:action moveLeft
    :parameters ( ?x )
    :precondition ( and
      (not (CheckConsistency))
      (not (Error))
      (or
        (LeftOf3 ?x)
        (RightOf7 ?x)
        (RightOf8 ?x)
        (RightOf15 ?x)
        (LeftOf2 ?x)
        (LeftOf9 ?x)
        (RightOf2 ?x)
        (RightOf10 ?x)
        (LeftOf8 ?x)
        (RightOf12 ?x)
        (LeftOf14 ?x)
        (LeftOf5 ?x)
        (RightOf1 ?x)
        (LeftOf12 ?x)
        (LeftOf19 ?x)
        (Columns ?x)
        (RightOf13 ?x)
        (RightOf14 ?x)
        (LeftOf1 ?x)
        (LeftOf20 ?x)
        (RightOf6 ?x)
        (RightOf9 ?x)
        (LeftOf6 ?x)
        (RightOf11 ?x)
        (RightOf17 ?x)
        (LeftOf11 ?x)
        (RightOf19 ?x)
        (RightOf5 ?x)
        (LeftOf10 ?x)
        (LeftOf15 ?x)
        (LeftOf4 ?x)
        (RightOf18 ?x)
        (LeftOf16 ?x)
        (RightOf16 ?x)
        (RightOf4 ?x)
        (RightOf0 ?x)
        (LeftOf7 ?x)
        (LeftOf13 ?x)
        (LeftOf17 ?x)
        (LeftOf18 ?x)
        (RightOf3 ?x)
      )
    )
    :effect ( and
      (CheckConsistency)
      (when
        (or
          (LeftOf2 ?x)
          (LeftOf1 ?x)
        )
        (and
          (LeftOf1 ?x)
        )
      )
      (when
        (or
          (LeftOf2 ?x)
          (LeftOf3 ?x)
          (LeftOf1 ?x)
        )
        (and
          (LeftOf2 ?x)
        )
      )
      (when
        (or
          (LeftOf2 ?x)
          (LeftOf4 ?x)
          (LeftOf3 ?x)
          (LeftOf1 ?x)
        )
        (and
          (LeftOf3 ?x)
        )
      )
      (when
        (or
          (LeftOf2 ?x)
          (LeftOf4 ?x)
          (LeftOf3 ?x)
          (LeftOf1 ?x)
          (LeftOf5 ?x)
        )
        (and
          (LeftOf4 ?x)
        )
      )
      (when
        (or
          (LeftOf4 ?x)
          (LeftOf3 ?x)
          (LeftOf5 ?x)
          (LeftOf6 ?x)
          (LeftOf1 ?x)
          (LeftOf2 ?x)
        )
        (and
          (LeftOf5 ?x)
        )
      )
      (when
        (or
          (LeftOf4 ?x)
          (LeftOf3 ?x)
          (LeftOf5 ?x)
          (LeftOf6 ?x)
          (LeftOf1 ?x)
          (LeftOf7 ?x)
          (LeftOf2 ?x)
        )
        (and
          (LeftOf6 ?x)
        )
      )
      (when
        (or
          (LeftOf8 ?x)
          (LeftOf4 ?x)
          (LeftOf3 ?x)
          (LeftOf5 ?x)
          (LeftOf6 ?x)
          (LeftOf1 ?x)
          (LeftOf7 ?x)
          (LeftOf2 ?x)
        )
        (and
          (LeftOf7 ?x)
        )
      )
      (when
        (or
          (LeftOf8 ?x)
          (LeftOf4 ?x)
          (LeftOf3 ?x)
          (LeftOf5 ?x)
          (LeftOf6 ?x)
          (LeftOf1 ?x)
          (LeftOf7 ?x)
          (LeftOf2 ?x)
          (LeftOf9 ?x)
        )
        (and
          (LeftOf8 ?x)
        )
      )
      (when
        (or
          (LeftOf8 ?x)
          (LeftOf4 ?x)
          (LeftOf3 ?x)
          (LeftOf5 ?x)
          (LeftOf6 ?x)
          (LeftOf1 ?x)
          (LeftOf7 ?x)
          (LeftOf2 ?x)
          (LeftOf9 ?x)
          (LeftOf10 ?x)
        )
        (and
          (LeftOf9 ?x)
        )
      )
      (when
        (or
          (LeftOf8 ?x)
          (LeftOf4 ?x)
          (LeftOf3 ?x)
          (LeftOf5 ?x)
          (LeftOf6 ?x)
          (LeftOf1 ?x)
          (LeftOf7 ?x)
          (LeftOf2 ?x)
          (LeftOf9 ?x)
          (LeftOf11 ?x)
          (LeftOf10 ?x)
        )
        (and
          (LeftOf10 ?x)
        )
      )
      (when
        (or
          (LeftOf8 ?x)
          (LeftOf4 ?x)
          (LeftOf3 ?x)
          (LeftOf5 ?x)
          (LeftOf6 ?x)
          (LeftOf12 ?x)
          (LeftOf1 ?x)
          (LeftOf7 ?x)
          (LeftOf2 ?x)
          (LeftOf9 ?x)
          (LeftOf11 ?x)
          (LeftOf10 ?x)
        )
        (and
          (LeftOf11 ?x)
        )
      )
      (when
        (or
          (LeftOf8 ?x)
          (LeftOf7 ?x)
          (LeftOf4 ?x)
          (LeftOf3 ?x)
          (LeftOf5 ?x)
          (LeftOf6 ?x)
          (LeftOf12 ?x)
          (LeftOf1 ?x)
          (LeftOf13 ?x)
          (LeftOf2 ?x)
          (LeftOf9 ?x)
          (LeftOf11 ?x)
          (LeftOf10 ?x)
        )
        (and
          (LeftOf12 ?x)
        )
      )
      (when
        (or
          (LeftOf8 ?x)
          (LeftOf7 ?x)
          (LeftOf4 ?x)
          (LeftOf3 ?x)
          (LeftOf14 ?x)
          (LeftOf5 ?x)
          (LeftOf6 ?x)
          (LeftOf12 ?x)
          (LeftOf1 ?x)
          (LeftOf13 ?x)
          (LeftOf2 ?x)
          (LeftOf9 ?x)
          (LeftOf11 ?x)
          (LeftOf10 ?x)
        )
        (and
          (LeftOf13 ?x)
        )
      )
      (when
        (or
          (LeftOf8 ?x)
          (LeftOf7 ?x)
          (LeftOf4 ?x)
          (LeftOf3 ?x)
          (LeftOf14 ?x)
          (LeftOf5 ?x)
          (LeftOf6 ?x)
          (LeftOf12 ?x)
          (LeftOf1 ?x)
          (LeftOf13 ?x)
          (LeftOf2 ?x)
          (LeftOf9 ?x)
          (LeftOf11 ?x)
          (LeftOf10 ?x)
          (LeftOf15 ?x)
        )
        (and
          (LeftOf14 ?x)
        )
      )
      (when
        (or
          (LeftOf8 ?x)
          (LeftOf7 ?x)
          (LeftOf4 ?x)
          (LeftOf3 ?x)
          (LeftOf14 ?x)
          (LeftOf16 ?x)
          (LeftOf5 ?x)
          (LeftOf6 ?x)
          (LeftOf12 ?x)
          (LeftOf1 ?x)
          (LeftOf13 ?x)
          (LeftOf2 ?x)
          (LeftOf9 ?x)
          (LeftOf11 ?x)
          (LeftOf10 ?x)
          (LeftOf15 ?x)
        )
        (and
          (LeftOf15 ?x)
        )
      )
      (when
        (or
          (LeftOf8 ?x)
          (LeftOf7 ?x)
          (LeftOf4 ?x)
          (LeftOf3 ?x)
          (LeftOf14 ?x)
          (LeftOf16 ?x)
          (LeftOf5 ?x)
          (LeftOf6 ?x)
          (LeftOf12 ?x)
          (LeftOf1 ?x)
          (LeftOf13 ?x)
          (LeftOf2 ?x)
          (LeftOf9 ?x)
          (LeftOf11 ?x)
          (LeftOf17 ?x)
          (LeftOf10 ?x)
          (LeftOf15 ?x)
        )
        (and
          (LeftOf16 ?x)
        )
      )
      (when
        (or
          (LeftOf8 ?x)
          (LeftOf7 ?x)
          (LeftOf4 ?x)
          (LeftOf3 ?x)
          (LeftOf14 ?x)
          (LeftOf16 ?x)
          (LeftOf5 ?x)
          (LeftOf6 ?x)
          (LeftOf12 ?x)
          (LeftOf1 ?x)
          (LeftOf13 ?x)
          (LeftOf2 ?x)
          (LeftOf9 ?x)
          (LeftOf11 ?x)
          (LeftOf17 ?x)
          (LeftOf18 ?x)
          (LeftOf10 ?x)
          (LeftOf15 ?x)
        )
        (and
          (LeftOf17 ?x)
        )
      )
      (when
        (or
          (LeftOf8 ?x)
          (LeftOf7 ?x)
          (LeftOf4 ?x)
          (LeftOf3 ?x)
          (LeftOf14 ?x)
          (LeftOf16 ?x)
          (LeftOf5 ?x)
          (LeftOf6 ?x)
          (LeftOf12 ?x)
          (LeftOf19 ?x)
          (LeftOf13 ?x)
          (LeftOf2 ?x)
          (LeftOf9 ?x)
          (LeftOf11 ?x)
          (LeftOf17 ?x)
          (LeftOf18 ?x)
          (LeftOf1 ?x)
          (LeftOf10 ?x)
          (LeftOf15 ?x)
        )
        (and
          (LeftOf18 ?x)
        )
      )
      (when
        (or
          (LeftOf8 ?x)
          (LeftOf7 ?x)
          (LeftOf4 ?x)
          (LeftOf3 ?x)
          (LeftOf14 ?x)
          (LeftOf16 ?x)
          (LeftOf5 ?x)
          (LeftOf6 ?x)
          (LeftOf12 ?x)
          (LeftOf19 ?x)
          (LeftOf13 ?x)
          (LeftOf2 ?x)
          (LeftOf9 ?x)
          (LeftOf11 ?x)
          (LeftOf17 ?x)
          (LeftOf18 ?x)
          (LeftOf1 ?x)
          (LeftOf10 ?x)
          (LeftOf15 ?x)
          (LeftOf20 ?x)
        )
        (and
          (LeftOf19 ?x)
        )
      )
      (when
        (or
          (RightOf6 ?x)
          (RightOf15 ?x)
          (RightOf11 ?x)
          (RightOf7 ?x)
          (RightOf18 ?x)
          (RightOf16 ?x)
          (RightOf1 ?x)
          (RightOf4 ?x)
          (RightOf10 ?x)
          (RightOf12 ?x)
          (RightOf13 ?x)
          (RightOf14 ?x)
          (RightOf17 ?x)
          (RightOf2 ?x)
          (RightOf19 ?x)
          (RightOf9 ?x)
          (RightOf5 ?x)
          (RightOf8 ?x)
          (RightOf3 ?x)
        )
        (and
          (RightOf0 ?x)
          (not (RightOf1 ?x))
        )
      )
      (when
        (or
          (RightOf6 ?x)
          (RightOf15 ?x)
          (RightOf11 ?x)
          (RightOf7 ?x)
          (RightOf18 ?x)
          (RightOf16 ?x)
          (RightOf8 ?x)
          (RightOf4 ?x)
          (RightOf10 ?x)
          (RightOf12 ?x)
          (RightOf13 ?x)
          (RightOf14 ?x)
          (RightOf17 ?x)
          (RightOf2 ?x)
          (RightOf19 ?x)
          (RightOf9 ?x)
          (RightOf5 ?x)
          (RightOf3 ?x)
        )
        (and
          (RightOf1 ?x)
          (not (RightOf2 ?x))
        )
      )
      (when
        (or
          (RightOf6 ?x)
          (RightOf15 ?x)
          (RightOf11 ?x)
          (RightOf18 ?x)
          (RightOf16 ?x)
          (RightOf8 ?x)
          (RightOf4 ?x)
          (RightOf10 ?x)
          (RightOf12 ?x)
          (RightOf13 ?x)
          (RightOf14 ?x)
          (RightOf17 ?x)
          (RightOf19 ?x)
          (RightOf9 ?x)
          (RightOf5 ?x)
          (RightOf3 ?x)
          (RightOf7 ?x)
        )
        (and
          (RightOf2 ?x)
          (not (RightOf3 ?x))
        )
      )
      (when
        (or
          (RightOf6 ?x)
          (RightOf15 ?x)
          (RightOf11 ?x)
          (RightOf18 ?x)
          (RightOf16 ?x)
          (RightOf8 ?x)
          (RightOf4 ?x)
          (RightOf10 ?x)
          (RightOf12 ?x)
          (RightOf13 ?x)
          (RightOf14 ?x)
          (RightOf17 ?x)
          (RightOf19 ?x)
          (RightOf9 ?x)
          (RightOf5 ?x)
          (RightOf7 ?x)
        )
        (and
          (RightOf3 ?x)
          (not (RightOf4 ?x))
        )
      )
      (when
        (or
          (RightOf6 ?x)
          (RightOf15 ?x)
          (RightOf11 ?x)
          (RightOf18 ?x)
          (RightOf16 ?x)
          (RightOf8 ?x)
          (RightOf10 ?x)
          (RightOf12 ?x)
          (RightOf13 ?x)
          (RightOf14 ?x)
          (RightOf17 ?x)
          (RightOf19 ?x)
          (RightOf9 ?x)
          (RightOf5 ?x)
          (RightOf7 ?x)
        )
        (and
          (RightOf4 ?x)
          (not (RightOf5 ?x))
        )
      )
      (when
        (or
          (RightOf6 ?x)
          (RightOf15 ?x)
          (RightOf11 ?x)
          (RightOf18 ?x)
          (RightOf16 ?x)
          (RightOf8 ?x)
          (RightOf10 ?x)
          (RightOf12 ?x)
          (RightOf13 ?x)
          (RightOf14 ?x)
          (RightOf17 ?x)
          (RightOf19 ?x)
          (RightOf9 ?x)
          (RightOf7 ?x)
        )
        (and
          (RightOf5 ?x)
          (not (RightOf6 ?x))
        )
      )
      (when
        (or
          (RightOf11 ?x)
          (RightOf18 ?x)
          (RightOf7 ?x)
          (RightOf16 ?x)
          (RightOf12 ?x)
          (RightOf8 ?x)
          (RightOf10 ?x)
          (RightOf15 ?x)
          (RightOf13 ?x)
          (RightOf14 ?x)
          (RightOf17 ?x)
          (RightOf19 ?x)
          (RightOf9 ?x)
        )
        (and
          (RightOf6 ?x)
          (not (RightOf7 ?x))
        )
      )
      (when
        (or
          (RightOf11 ?x)
          (RightOf18 ?x)
          (RightOf12 ?x)
          (RightOf16 ?x)
          (RightOf8 ?x)
          (RightOf10 ?x)
          (RightOf15 ?x)
          (RightOf13 ?x)
          (RightOf14 ?x)
          (RightOf17 ?x)
          (RightOf19 ?x)
          (RightOf9 ?x)
        )
        (and
          (RightOf7 ?x)
          (not (RightOf8 ?x))
        )
      )
      (when
        (or
          (RightOf11 ?x)
          (RightOf18 ?x)
          (RightOf12 ?x)
          (RightOf16 ?x)
          (RightOf10 ?x)
          (RightOf15 ?x)
          (RightOf13 ?x)
          (RightOf14 ?x)
          (RightOf17 ?x)
          (RightOf9 ?x)
          (RightOf19 ?x)
        )
        (and
          (RightOf8 ?x)
          (not (RightOf9 ?x))
        )
      )
      (when
        (or
          (RightOf11 ?x)
          (RightOf19 ?x)
          (RightOf12 ?x)
          (RightOf16 ?x)
          (RightOf10 ?x)
          (RightOf15 ?x)
          (RightOf13 ?x)
          (RightOf14 ?x)
          (RightOf17 ?x)
          (RightOf18 ?x)
        )
        (and
          (RightOf9 ?x)
          (not (RightOf10 ?x))
        )
      )
      (when
        (or
          (RightOf11 ?x)
          (RightOf19 ?x)
          (RightOf12 ?x)
          (RightOf16 ?x)
          (RightOf15 ?x)
          (RightOf13 ?x)
          (RightOf14 ?x)
          (RightOf17 ?x)
          (RightOf18 ?x)
        )
        (and
          (RightOf10 ?x)
          (not (RightOf11 ?x))
        )
      )
      (when
        (or
          (RightOf15 ?x)
          (RightOf18 ?x)
          (RightOf16 ?x)
          (RightOf12 ?x)
          (RightOf13 ?x)
          (RightOf14 ?x)
          (RightOf17 ?x)
          (RightOf19 ?x)
        )
        (and
          (RightOf11 ?x)
          (not (RightOf12 ?x))
        )
      )
      (when
        (or
          (RightOf18 ?x)
          (RightOf16 ?x)
          (RightOf15 ?x)
          (RightOf13 ?x)
          (RightOf14 ?x)
          (RightOf17 ?x)
          (RightOf19 ?x)
        )
        (and
          (RightOf12 ?x)
          (not (RightOf13 ?x))
        )
      )
      (when
        (or
          (RightOf19 ?x)
          (RightOf16 ?x)
          (RightOf15 ?x)
          (RightOf14 ?x)
          (RightOf17 ?x)
          (RightOf18 ?x)
        )
        (and
          (RightOf13 ?x)
          (not (RightOf14 ?x))
        )
      )
      (when
        (or
          (RightOf15 ?x)
          (RightOf19 ?x)
          (RightOf18 ?x)
          (RightOf17 ?x)
          (RightOf16 ?x)
        )
        (and
          (RightOf14 ?x)
          (not (RightOf15 ?x))
        )
      )
      (when
        (or
          (RightOf19 ?x)
          (RightOf18 ?x)
          (RightOf17 ?x)
          (RightOf16 ?x)
        )
        (and
          (RightOf15 ?x)
          (not (RightOf16 ?x))
        )
      )
      (when
        (or
          (RightOf19 ?x)
          (RightOf18 ?x)
          (RightOf17 ?x)
        )
        (and
          (RightOf16 ?x)
          (not (RightOf17 ?x))
        )
      )
      (when
        (or
          (RightOf19 ?x)
          (RightOf18 ?x)
        )
        (and
          (RightOf17 ?x)
          (not (RightOf18 ?x))
        )
      )
      (when
        (RightOf19 ?x)
        (and
          (RightOf18 ?x)
          (not (RightOf19 ?x))
        )
      )
      (when
        (Column1 ?x)
        (and
          (Column0 ?x)
          (not (Column1 ?x))
        )
      )
      (when
        (Column2 ?x)
        (and
          (Column1 ?x)
          (not (Column2 ?x))
        )
      )
      (when
        (Column3 ?x)
        (and
          (Column2 ?x)
          (not (Column3 ?x))
        )
      )
      (when
        (Column4 ?x)
        (and
          (Column3 ?x)
          (not (Column4 ?x))
        )
      )
      (when
        (Column5 ?x)
        (and
          (Column4 ?x)
          (not (Column5 ?x))
        )
      )
      (when
        (Column6 ?x)
        (and
          (Column5 ?x)
          (not (Column6 ?x))
        )
      )
      (when
        (Column7 ?x)
        (and
          (Column6 ?x)
          (not (Column7 ?x))
        )
      )
      (when
        (Column8 ?x)
        (and
          (Column7 ?x)
          (not (Column8 ?x))
        )
      )
      (when
        (Column9 ?x)
        (and
          (Column8 ?x)
          (not (Column9 ?x))
        )
      )
      (when
        (Column10 ?x)
        (and
          (Column9 ?x)
          (not (Column10 ?x))
        )
      )
      (when
        (Column11 ?x)
        (and
          (Column10 ?x)
          (not (Column11 ?x))
        )
      )
      (when
        (Column12 ?x)
        (and
          (Column11 ?x)
          (not (Column12 ?x))
        )
      )
      (when
        (Column13 ?x)
        (and
          (Column12 ?x)
          (not (Column13 ?x))
        )
      )
      (when
        (Column14 ?x)
        (and
          (Column13 ?x)
          (not (Column14 ?x))
        )
      )
      (when
        (Column15 ?x)
        (and
          (Column14 ?x)
          (not (Column15 ?x))
        )
      )
      (when
        (Column16 ?x)
        (and
          (Column15 ?x)
          (not (Column16 ?x))
        )
      )
      (when
        (Column17 ?x)
        (and
          (Column16 ?x)
          (not (Column17 ?x))
        )
      )
      (when
        (Column18 ?x)
        (and
          (Column17 ?x)
          (not (Column18 ?x))
        )
      )
      (when
        (Column19 ?x)
        (and
          (Column18 ?x)
          (not (Column19 ?x))
        )
      )
      (when
        (or
          (and
            (RightOf4 ?x)
            (LeftOf2 ?x)
          )
          (and
            (RightOf6 ?x)
            (LeftOf2 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf19 ?x)
          )
          (and
            (RightOf16 ?x)
            (LeftOf1 ?x)
          )
          (and
            (RightOf14 ?x)
            (LeftOf1 ?x)
          )
          (and
            (RightOf6 ?x)
            (LeftOf1 ?x)
          )
          (and
            (RightOf2 ?x)
            (LeftOf1 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf2 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf8 ?x)
          )
          (and
            (RightOf15 ?x)
            (LeftOf2 ?x)
          )
          (and
            (LeftOf1 ?x)
            (RightOf13 ?x)
          )
          (and
            (LeftOf1 ?x)
            (RightOf19 ?x)
          )
          (and
            (RightOf17 ?x)
            (LeftOf1 ?x)
          )
          (and
            (RightOf14 ?x)
            (LeftOf2 ?x)
          )
          (and
            (RightOf5 ?x)
            (LeftOf1 ?x)
          )
          (and
            (LeftOf1 ?x)
            (RightOf8 ?x)
          )
          (and
            (RightOf7 ?x)
            (LeftOf1 ?x)
          )
          (and
            (RightOf12 ?x)
            (LeftOf1 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf10 ?x)
          )
          (and
            (RightOf4 ?x)
            (LeftOf1 ?x)
          )
          (and
            (RightOf17 ?x)
            (LeftOf2 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf3 ?x)
          )
          (and
            (LeftOf1 ?x)
            (RightOf11 ?x)
          )
          (and
            (LeftOf1 ?x)
            (RightOf5 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf16 ?x)
          )
          (and
            (RightOf7 ?x)
            (LeftOf2 ?x)
          )
          (and
            (RightOf1 ?x)
            (LeftOf1 ?x)
          )
          (and
            (RightOf9 ?x)
            (LeftOf2 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf5 ?x)
          )
          (and
            (RightOf1 ?x)
            (LeftOf2 ?x)
          )
          (and
            (LeftOf1 ?x)
            (RightOf10 ?x)
          )
          (and
            (RightOf15 ?x)
            (LeftOf1 ?x)
          )
          (and
            (LeftOf1 ?x)
            (RightOf18 ?x)
          )
          (and
            (LeftOf1 ?x)
            (RightOf3 ?x)
          )
          (and
            (RightOf9 ?x)
            (LeftOf1 ?x)
          )
          (and
            (RightOf12 ?x)
            (LeftOf2 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf13 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf18 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf11 ?x)
          )
        )
        (and
          (Column0 ?x)
        )
      )
      (when
        (or
          (and
            (RightOf2 ?x)
            (LeftOf3 ?x)
          )
          (and
            (RightOf4 ?x)
            (LeftOf2 ?x)
          )
          (and
            (RightOf9 ?x)
            (LeftOf3 ?x)
          )
          (and
            (RightOf6 ?x)
            (LeftOf2 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf19 ?x)
          )
          (and
            (RightOf16 ?x)
            (LeftOf1 ?x)
          )
          (and
            (RightOf14 ?x)
            (LeftOf1 ?x)
          )
          (and
            (RightOf6 ?x)
            (LeftOf1 ?x)
          )
          (and
            (RightOf2 ?x)
            (LeftOf1 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf2 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf8 ?x)
          )
          (and
            (RightOf19 ?x)
            (LeftOf3 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf11 ?x)
          )
          (and
            (LeftOf1 ?x)
            (RightOf13 ?x)
          )
          (and
            (LeftOf1 ?x)
            (RightOf19 ?x)
          )
          (and
            (RightOf17 ?x)
            (LeftOf3 ?x)
          )
          (and
            (LeftOf3 ?x)
            (RightOf13 ?x)
          )
          (and
            (RightOf18 ?x)
            (LeftOf3 ?x)
          )
          (and
            (RightOf11 ?x)
            (LeftOf3 ?x)
          )
          (and
            (LeftOf3 ?x)
            (RightOf3 ?x)
          )
          (and
            (LeftOf1 ?x)
            (RightOf8 ?x)
          )
          (and
            (RightOf15 ?x)
            (LeftOf3 ?x)
          )
          (and
            (RightOf5 ?x)
            (LeftOf3 ?x)
          )
          (and
            (LeftOf1 ?x)
            (RightOf18 ?x)
          )
          (and
            (RightOf12 ?x)
            (LeftOf2 ?x)
          )
          (and
            (RightOf7 ?x)
            (LeftOf2 ?x)
          )
          (and
            (RightOf8 ?x)
            (LeftOf3 ?x)
          )
          (and
            (RightOf10 ?x)
            (LeftOf3 ?x)
          )
          (and
            (RightOf12 ?x)
            (LeftOf1 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf10 ?x)
          )
          (and
            (RightOf4 ?x)
            (LeftOf1 ?x)
          )
          (and
            (RightOf14 ?x)
            (LeftOf2 ?x)
          )
          (and
            (RightOf12 ?x)
            (LeftOf3 ?x)
          )
          (and
            (RightOf17 ?x)
            (LeftOf2 ?x)
          )
          (and
            (RightOf5 ?x)
            (LeftOf1 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf3 ?x)
          )
          (and
            (LeftOf1 ?x)
            (RightOf11 ?x)
          )
          (and
            (RightOf7 ?x)
            (LeftOf3 ?x)
          )
          (and
            (LeftOf1 ?x)
            (RightOf5 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf16 ?x)
          )
          (and
            (RightOf7 ?x)
            (LeftOf1 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf5 ?x)
          )
          (and
            (RightOf16 ?x)
            (LeftOf3 ?x)
          )
          (and
            (LeftOf1 ?x)
            (RightOf10 ?x)
          )
          (and
            (RightOf15 ?x)
            (LeftOf1 ?x)
          )
          (and
            (RightOf4 ?x)
            (LeftOf3 ?x)
          )
          (and
            (RightOf17 ?x)
            (LeftOf1 ?x)
          )
          (and
            (RightOf14 ?x)
            (LeftOf3 ?x)
          )
          (and
            (LeftOf1 ?x)
            (RightOf3 ?x)
          )
          (and
            (RightOf9 ?x)
            (LeftOf1 ?x)
          )
          (and
            (RightOf6 ?x)
            (LeftOf3 ?x)
          )
          (and
            (RightOf9 ?x)
            (LeftOf2 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf13 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf18 ?x)
          )
          (and
            (RightOf15 ?x)
            (LeftOf2 ?x)
          )
        )
        (and
          (Column1 ?x)
        )
      )
      (when
        (or
          (and
            (RightOf4 ?x)
            (LeftOf2 ?x)
          )
          (and
            (RightOf9 ?x)
            (LeftOf3 ?x)
          )
          (and
            (RightOf6 ?x)
            (LeftOf2 ?x)
          )
          (and
            (RightOf11 ?x)
            (LeftOf4 ?x)
          )
          (and
            (LeftOf4 ?x)
            (RightOf3 ?x)
          )
          (and
            (RightOf6 ?x)
            (LeftOf1 ?x)
          )
          (and
            (LeftOf4 ?x)
            (RightOf13 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf8 ?x)
          )
          (and
            (RightOf5 ?x)
            (LeftOf4 ?x)
          )
          (and
            (RightOf6 ?x)
            (LeftOf4 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf11 ?x)
          )
          (and
            (LeftOf1 ?x)
            (RightOf13 ?x)
          )
          (and
            (LeftOf1 ?x)
            (RightOf19 ?x)
          )
          (and
            (RightOf9 ?x)
            (LeftOf4 ?x)
          )
          (and
            (RightOf17 ?x)
            (LeftOf3 ?x)
          )
          (and
            (RightOf19 ?x)
            (LeftOf4 ?x)
          )
          (and
            (LeftOf3 ?x)
            (RightOf13 ?x)
          )
          (and
            (RightOf18 ?x)
            (LeftOf3 ?x)
          )
          (and
            (LeftOf4 ?x)
            (RightOf10 ?x)
          )
          (and
            (RightOf12 ?x)
            (LeftOf4 ?x)
          )
          (and
            (RightOf11 ?x)
            (LeftOf3 ?x)
          )
          (and
            (RightOf19 ?x)
            (LeftOf3 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf19 ?x)
          )
          (and
            (RightOf15 ?x)
            (LeftOf3 ?x)
          )
          (and
            (RightOf5 ?x)
            (LeftOf3 ?x)
          )
          (and
            (RightOf16 ?x)
            (LeftOf1 ?x)
          )
          (and
            (LeftOf1 ?x)
            (RightOf18 ?x)
          )
          (and
            (LeftOf4 ?x)
            (RightOf8 ?x)
          )
          (and
            (RightOf12 ?x)
            (LeftOf2 ?x)
          )
          (and
            (RightOf7 ?x)
            (LeftOf2 ?x)
          )
          (and
            (RightOf3 ?x)
            (LeftOf3 ?x)
          )
          (and
            (RightOf10 ?x)
            (LeftOf3 ?x)
          )
          (and
            (RightOf12 ?x)
            (LeftOf1 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf10 ?x)
          )
          (and
            (RightOf14 ?x)
            (LeftOf4 ?x)
          )
          (and
            (RightOf4 ?x)
            (LeftOf1 ?x)
          )
          (and
            (RightOf14 ?x)
            (LeftOf2 ?x)
          )
          (and
            (RightOf12 ?x)
            (LeftOf3 ?x)
          )
          (and
            (RightOf17 ?x)
            (LeftOf2 ?x)
          )
          (and
            (RightOf5 ?x)
            (LeftOf1 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf3 ?x)
          )
          (and
            (LeftOf1 ?x)
            (RightOf11 ?x)
          )
          (and
            (RightOf7 ?x)
            (LeftOf3 ?x)
          )
          (and
            (RightOf15 ?x)
            (LeftOf4 ?x)
          )
          (and
            (LeftOf1 ?x)
            (RightOf5 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf16 ?x)
          )
          (and
            (RightOf7 ?x)
            (LeftOf1 ?x)
          )
          (and
            (RightOf8 ?x)
            (LeftOf3 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf5 ?x)
          )
          (and
            (RightOf7 ?x)
            (LeftOf4 ?x)
          )
          (and
            (RightOf16 ?x)
            (LeftOf3 ?x)
          )
          (and
            (RightOf4 ?x)
            (LeftOf4 ?x)
          )
          (and
            (RightOf17 ?x)
            (LeftOf4 ?x)
          )
          (and
            (LeftOf1 ?x)
            (RightOf8 ?x)
          )
          (and
            (LeftOf1 ?x)
            (RightOf10 ?x)
          )
          (and
            (RightOf15 ?x)
            (LeftOf1 ?x)
          )
          (and
            (RightOf4 ?x)
            (LeftOf3 ?x)
          )
          (and
            (RightOf13 ?x)
            (LeftOf3 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf18 ?x)
          )
          (and
            (RightOf14 ?x)
            (LeftOf1 ?x)
          )
          (and
            (RightOf17 ?x)
            (LeftOf1 ?x)
          )
          (and
            (RightOf14 ?x)
            (LeftOf3 ?x)
          )
          (and
            (LeftOf1 ?x)
            (RightOf3 ?x)
          )
          (and
            (RightOf9 ?x)
            (LeftOf1 ?x)
          )
          (and
            (RightOf6 ?x)
            (LeftOf3 ?x)
          )
          (and
            (RightOf9 ?x)
            (LeftOf2 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf13 ?x)
          )
          (and
            (RightOf16 ?x)
            (LeftOf4 ?x)
          )
          (and
            (LeftOf4 ?x)
            (RightOf18 ?x)
          )
          (and
            (RightOf15 ?x)
            (LeftOf2 ?x)
          )
        )
        (and
          (Column2 ?x)
        )
      )
      (when
        (or
          (and
            (LeftOf5 ?x)
            (RightOf17 ?x)
          )
          (and
            (RightOf4 ?x)
            (LeftOf2 ?x)
          )
          (and
            (RightOf9 ?x)
            (LeftOf3 ?x)
          )
          (and
            (RightOf6 ?x)
            (LeftOf2 ?x)
          )
          (and
            (RightOf11 ?x)
            (LeftOf4 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf5 ?x)
          )
          (and
            (RightOf6 ?x)
            (LeftOf1 ?x)
          )
          (and
            (RightOf5 ?x)
            (LeftOf3 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf14 ?x)
          )
          (and
            (LeftOf4 ?x)
            (RightOf13 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf8 ?x)
          )
          (and
            (RightOf5 ?x)
            (LeftOf4 ?x)
          )
          (and
            (RightOf6 ?x)
            (LeftOf4 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf11 ?x)
          )
          (and
            (LeftOf1 ?x)
            (RightOf13 ?x)
          )
          (and
            (LeftOf1 ?x)
            (RightOf19 ?x)
          )
          (and
            (RightOf9 ?x)
            (LeftOf4 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf18 ?x)
          )
          (and
            (RightOf17 ?x)
            (LeftOf3 ?x)
          )
          (and
            (RightOf19 ?x)
            (LeftOf4 ?x)
          )
          (and
            (RightOf14 ?x)
            (LeftOf2 ?x)
          )
          (and
            (LeftOf3 ?x)
            (RightOf13 ?x)
          )
          (and
            (RightOf18 ?x)
            (LeftOf3 ?x)
          )
          (and
            (LeftOf4 ?x)
            (RightOf10 ?x)
          )
          (and
            (RightOf12 ?x)
            (LeftOf4 ?x)
          )
          (and
            (RightOf11 ?x)
            (LeftOf3 ?x)
          )
          (and
            (RightOf19 ?x)
            (LeftOf3 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf19 ?x)
          )
          (and
            (RightOf12 ?x)
            (LeftOf3 ?x)
          )
          (and
            (RightOf5 ?x)
            (LeftOf1 ?x)
          )
          (and
            (RightOf16 ?x)
            (LeftOf1 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf19 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf8 ?x)
          )
          (and
            (RightOf12 ?x)
            (LeftOf5 ?x)
          )
          (and
            (LeftOf4 ?x)
            (RightOf8 ?x)
          )
          (and
            (LeftOf1 ?x)
            (RightOf8 ?x)
          )
          (and
            (RightOf7 ?x)
            (LeftOf2 ?x)
          )
          (and
            (RightOf10 ?x)
            (LeftOf4 ?x)
          )
          (and
            (RightOf10 ?x)
            (LeftOf3 ?x)
          )
          (and
            (RightOf12 ?x)
            (LeftOf1 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf10 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf10 ?x)
          )
          (and
            (RightOf12 ?x)
            (LeftOf2 ?x)
          )
          (and
            (RightOf6 ?x)
            (LeftOf5 ?x)
          )
          (and
            (RightOf16 ?x)
            (LeftOf4 ?x)
          )
          (and
            (RightOf8 ?x)
            (LeftOf4 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf16 ?x)
          )
          (and
            (RightOf15 ?x)
            (LeftOf3 ?x)
          )
          (and
            (RightOf17 ?x)
            (LeftOf2 ?x)
          )
          (and
            (RightOf14 ?x)
            (LeftOf4 ?x)
          )
          (and
            (LeftOf1 ?x)
            (RightOf11 ?x)
          )
          (and
            (RightOf7 ?x)
            (LeftOf3 ?x)
          )
          (and
            (RightOf15 ?x)
            (LeftOf4 ?x)
          )
          (and
            (LeftOf1 ?x)
            (RightOf5 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf16 ?x)
          )
          (and
            (RightOf7 ?x)
            (LeftOf1 ?x)
          )
          (and
            (RightOf17 ?x)
            (LeftOf1 ?x)
          )
          (and
            (RightOf8 ?x)
            (LeftOf3 ?x)
          )
          (and
            (LeftOf1 ?x)
            (RightOf18 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf5 ?x)
          )
          (and
            (RightOf7 ?x)
            (LeftOf4 ?x)
          )
          (and
            (RightOf15 ?x)
            (LeftOf2 ?x)
          )
          (and
            (RightOf16 ?x)
            (LeftOf3 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf13 ?x)
          )
          (and
            (RightOf4 ?x)
            (LeftOf4 ?x)
          )
          (and
            (RightOf17 ?x)
            (LeftOf4 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf11 ?x)
          )
          (and
            (LeftOf1 ?x)
            (RightOf10 ?x)
          )
          (and
            (RightOf15 ?x)
            (LeftOf1 ?x)
          )
          (and
            (RightOf4 ?x)
            (LeftOf3 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf7 ?x)
          )
          (and
            (RightOf13 ?x)
            (LeftOf3 ?x)
          )
          (and
            (RightOf18 ?x)
            (LeftOf4 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf18 ?x)
          )
          (and
            (RightOf14 ?x)
            (LeftOf1 ?x)
          )
          (and
            (RightOf14 ?x)
            (LeftOf3 ?x)
          )
          (and
            (RightOf9 ?x)
            (LeftOf1 ?x)
          )
          (and
            (RightOf6 ?x)
            (LeftOf3 ?x)
          )
          (and
            (RightOf9 ?x)
            (LeftOf2 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf4 ?x)
          )
          (and
            (RightOf4 ?x)
            (LeftOf1 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf13 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf15 ?x)
          )
          (and
            (LeftOf4 ?x)
            (RightOf18 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf9 ?x)
          )
        )
        (and
          (Column3 ?x)
        )
      )
      (when
        (or
          (and
            (RightOf11 ?x)
            (LeftOf4 ?x)
          )
          (and
            (RightOf19 ?x)
            (LeftOf3 ?x)
          )
          (and
            (RightOf15 ?x)
            (LeftOf2 ?x)
          )
          (and
            (RightOf9 ?x)
            (LeftOf4 ?x)
          )
          (and
            (LeftOf3 ?x)
            (RightOf13 ?x)
          )
          (and
            (RightOf16 ?x)
            (LeftOf1 ?x)
          )
          (and
            (RightOf12 ?x)
            (LeftOf4 ?x)
          )
          (and
            (RightOf11 ?x)
            (LeftOf3 ?x)
          )
          (and
            (LeftOf1 ?x)
            (RightOf8 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf19 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf8 ?x)
          )
          (and
            (LeftOf4 ?x)
            (RightOf8 ?x)
          )
          (and
            (RightOf16 ?x)
            (LeftOf3 ?x)
          )
          (and
            (LeftOf1 ?x)
            (RightOf11 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf8 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf16 ?x)
          )
          (and
            (RightOf8 ?x)
            (LeftOf3 ?x)
          )
          (and
            (RightOf18 ?x)
            (LeftOf4 ?x)
          )
          (and
            (RightOf17 ?x)
            (LeftOf4 ?x)
          )
          (and
            (LeftOf1 ?x)
            (RightOf18 ?x)
          )
          (and
            (RightOf6 ?x)
            (LeftOf3 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf16 ?x)
          )
          (and
            (LeftOf4 ?x)
            (RightOf18 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf11 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf17 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf9 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf11 ?x)
          )
          (and
            (RightOf6 ?x)
            (LeftOf2 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf19 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf5 ?x)
          )
          (and
            (RightOf6 ?x)
            (LeftOf4 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf13 ?x)
          )
          (and
            (LeftOf1 ?x)
            (RightOf13 ?x)
          )
          (and
            (LeftOf1 ?x)
            (RightOf19 ?x)
          )
          (and
            (RightOf19 ?x)
            (LeftOf4 ?x)
          )
          (and
            (LeftOf4 ?x)
            (RightOf10 ?x)
          )
          (and
            (RightOf17 ?x)
            (LeftOf1 ?x)
          )
          (and
            (RightOf5 ?x)
            (LeftOf3 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf10 ?x)
          )
          (and
            (RightOf12 ?x)
            (LeftOf3 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf19 ?x)
          )
          (and
            (LeftOf1 ?x)
            (RightOf5 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf7 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf14 ?x)
          )
          (and
            (RightOf16 ?x)
            (LeftOf4 ?x)
          )
          (and
            (RightOf7 ?x)
            (LeftOf4 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf18 ?x)
          )
          (and
            (RightOf14 ?x)
            (LeftOf3 ?x)
          )
          (and
            (RightOf14 ?x)
            (LeftOf2 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf15 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf9 ?x)
          )
          (and
            (RightOf9 ?x)
            (LeftOf3 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf18 ?x)
          )
          (and
            (RightOf14 ?x)
            (LeftOf1 ?x)
          )
          (and
            (LeftOf4 ?x)
            (RightOf13 ?x)
          )
          (and
            (RightOf5 ?x)
            (LeftOf4 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf6 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf10 ?x)
          )
          (and
            (RightOf15 ?x)
            (LeftOf4 ?x)
          )
          (and
            (RightOf18 ?x)
            (LeftOf3 ?x)
          )
          (and
            (RightOf12 ?x)
            (LeftOf5 ?x)
          )
          (and
            (RightOf7 ?x)
            (LeftOf1 ?x)
          )
          (and
            (RightOf12 ?x)
            (LeftOf1 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf17 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf16 ?x)
          )
          (and
            (RightOf15 ?x)
            (LeftOf3 ?x)
          )
          (and
            (RightOf17 ?x)
            (LeftOf2 ?x)
          )
          (and
            (RightOf14 ?x)
            (LeftOf4 ?x)
          )
          (and
            (RightOf7 ?x)
            (LeftOf3 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf12 ?x)
          )
          (and
            (RightOf7 ?x)
            (LeftOf2 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf5 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf7 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf13 ?x)
          )
          (and
            (LeftOf1 ?x)
            (RightOf10 ?x)
          )
          (and
            (RightOf13 ?x)
            (LeftOf3 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf13 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf14 ?x)
          )
          (and
            (RightOf6 ?x)
            (LeftOf1 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf8 ?x)
          )
          (and
            (RightOf17 ?x)
            (LeftOf3 ?x)
          )
          (and
            (RightOf9 ?x)
            (LeftOf2 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf5 ?x)
          )
          (and
            (RightOf12 ?x)
            (LeftOf2 ?x)
          )
          (and
            (RightOf10 ?x)
            (LeftOf4 ?x)
          )
          (and
            (RightOf10 ?x)
            (LeftOf3 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf10 ?x)
          )
          (and
            (RightOf6 ?x)
            (LeftOf5 ?x)
          )
          (and
            (RightOf8 ?x)
            (LeftOf4 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf11 ?x)
          )
          (and
            (RightOf9 ?x)
            (LeftOf1 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf15 ?x)
          )
          (and
            (RightOf15 ?x)
            (LeftOf1 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf18 ?x)
          )
        )
        (and
          (Column4 ?x)
        )
      )
      (when
        (or
          (and
            (RightOf11 ?x)
            (LeftOf4 ?x)
          )
          (and
            (LeftOf7 ?x)
            (RightOf10 ?x)
          )
          (and
            (RightOf15 ?x)
            (LeftOf2 ?x)
          )
          (and
            (RightOf9 ?x)
            (LeftOf4 ?x)
          )
          (and
            (LeftOf3 ?x)
            (RightOf13 ?x)
          )
          (and
            (RightOf16 ?x)
            (LeftOf1 ?x)
          )
          (and
            (RightOf12 ?x)
            (LeftOf4 ?x)
          )
          (and
            (RightOf11 ?x)
            (LeftOf3 ?x)
          )
          (and
            (LeftOf1 ?x)
            (RightOf8 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf19 ?x)
          )
          (and
            (RightOf14 ?x)
            (LeftOf7 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf8 ?x)
          )
          (and
            (LeftOf7 ?x)
            (RightOf16 ?x)
          )
          (and
            (LeftOf4 ?x)
            (RightOf8 ?x)
          )
          (and
            (RightOf16 ?x)
            (LeftOf3 ?x)
          )
          (and
            (LeftOf1 ?x)
            (RightOf11 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf8 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf16 ?x)
          )
          (and
            (RightOf10 ?x)
            (LeftOf4 ?x)
          )
          (and
            (RightOf18 ?x)
            (LeftOf4 ?x)
          )
          (and
            (RightOf17 ?x)
            (LeftOf4 ?x)
          )
          (and
            (LeftOf1 ?x)
            (RightOf18 ?x)
          )
          (and
            (RightOf6 ?x)
            (LeftOf3 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf16 ?x)
          )
          (and
            (LeftOf4 ?x)
            (RightOf18 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf11 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf17 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf9 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf11 ?x)
          )
          (and
            (RightOf6 ?x)
            (LeftOf2 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf19 ?x)
          )
          (and
            (LeftOf7 ?x)
            (RightOf19 ?x)
          )
          (and
            (RightOf6 ?x)
            (LeftOf4 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf13 ?x)
          )
          (and
            (LeftOf1 ?x)
            (RightOf13 ?x)
          )
          (and
            (LeftOf1 ?x)
            (RightOf19 ?x)
          )
          (and
            (RightOf19 ?x)
            (LeftOf4 ?x)
          )
          (and
            (LeftOf4 ?x)
            (RightOf10 ?x)
          )
          (and
            (RightOf17 ?x)
            (LeftOf1 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf10 ?x)
          )
          (and
            (RightOf12 ?x)
            (LeftOf3 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf19 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf7 ?x)
          )
          (and
            (LeftOf7 ?x)
            (RightOf17 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf14 ?x)
          )
          (and
            (RightOf16 ?x)
            (LeftOf4 ?x)
          )
          (and
            (RightOf7 ?x)
            (LeftOf4 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf18 ?x)
          )
          (and
            (LeftOf7 ?x)
            (RightOf8 ?x)
          )
          (and
            (RightOf14 ?x)
            (LeftOf3 ?x)
          )
          (and
            (LeftOf7 ?x)
            (RightOf11 ?x)
          )
          (and
            (RightOf14 ?x)
            (LeftOf2 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf15 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf9 ?x)
          )
          (and
            (RightOf15 ?x)
            (LeftOf7 ?x)
          )
          (and
            (RightOf9 ?x)
            (LeftOf3 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf18 ?x)
          )
          (and
            (RightOf14 ?x)
            (LeftOf1 ?x)
          )
          (and
            (LeftOf4 ?x)
            (RightOf13 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf10 ?x)
          )
          (and
            (RightOf12 ?x)
            (LeftOf6 ?x)
          )
          (and
            (RightOf6 ?x)
            (LeftOf7 ?x)
          )
          (and
            (RightOf15 ?x)
            (LeftOf4 ?x)
          )
          (and
            (RightOf18 ?x)
            (LeftOf3 ?x)
          )
          (and
            (RightOf12 ?x)
            (LeftOf5 ?x)
          )
          (and
            (RightOf12 ?x)
            (LeftOf7 ?x)
          )
          (and
            (RightOf7 ?x)
            (LeftOf1 ?x)
          )
          (and
            (RightOf12 ?x)
            (LeftOf1 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf17 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf16 ?x)
          )
          (and
            (RightOf15 ?x)
            (LeftOf3 ?x)
          )
          (and
            (RightOf17 ?x)
            (LeftOf2 ?x)
          )
          (and
            (RightOf14 ?x)
            (LeftOf4 ?x)
          )
          (and
            (RightOf7 ?x)
            (LeftOf3 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf12 ?x)
          )
          (and
            (RightOf7 ?x)
            (LeftOf2 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf7 ?x)
          )
          (and
            (LeftOf7 ?x)
            (RightOf18 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf13 ?x)
          )
          (and
            (LeftOf1 ?x)
            (RightOf10 ?x)
          )
          (and
            (RightOf13 ?x)
            (LeftOf3 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf13 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf14 ?x)
          )
          (and
            (RightOf6 ?x)
            (LeftOf1 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf8 ?x)
          )
          (and
            (RightOf17 ?x)
            (LeftOf3 ?x)
          )
          (and
            (RightOf6 ?x)
            (LeftOf6 ?x)
          )
          (and
            (RightOf9 ?x)
            (LeftOf7 ?x)
          )
          (and
            (RightOf9 ?x)
            (LeftOf2 ?x)
          )
          (and
            (RightOf12 ?x)
            (LeftOf2 ?x)
          )
          (and
            (RightOf8 ?x)
            (LeftOf3 ?x)
          )
          (and
            (RightOf10 ?x)
            (LeftOf3 ?x)
          )
          (and
            (RightOf19 ?x)
            (LeftOf3 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf10 ?x)
          )
          (and
            (RightOf6 ?x)
            (LeftOf5 ?x)
          )
          (and
            (RightOf8 ?x)
            (LeftOf4 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf11 ?x)
          )
          (and
            (RightOf9 ?x)
            (LeftOf1 ?x)
          )
          (and
            (LeftOf7 ?x)
            (RightOf13 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf15 ?x)
          )
          (and
            (RightOf7 ?x)
            (LeftOf7 ?x)
          )
          (and
            (RightOf15 ?x)
            (LeftOf1 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf18 ?x)
          )
        )
        (and
          (Column5 ?x)
        )
      )
      (when
        (or
          (and
            (RightOf11 ?x)
            (LeftOf4 ?x)
          )
          (and
            (LeftOf7 ?x)
            (RightOf10 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf11 ?x)
          )
          (and
            (RightOf9 ?x)
            (LeftOf4 ?x)
          )
          (and
            (LeftOf8 ?x)
            (RightOf19 ?x)
          )
          (and
            (LeftOf3 ?x)
            (RightOf13 ?x)
          )
          (and
            (RightOf16 ?x)
            (LeftOf1 ?x)
          )
          (and
            (RightOf12 ?x)
            (LeftOf4 ?x)
          )
          (and
            (RightOf11 ?x)
            (LeftOf3 ?x)
          )
          (and
            (LeftOf1 ?x)
            (RightOf8 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf19 ?x)
          )
          (and
            (RightOf14 ?x)
            (LeftOf7 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf8 ?x)
          )
          (and
            (LeftOf7 ?x)
            (RightOf16 ?x)
          )
          (and
            (LeftOf4 ?x)
            (RightOf8 ?x)
          )
          (and
            (RightOf16 ?x)
            (LeftOf3 ?x)
          )
          (and
            (LeftOf8 ?x)
            (RightOf8 ?x)
          )
          (and
            (LeftOf1 ?x)
            (RightOf11 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf8 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf16 ?x)
          )
          (and
            (RightOf10 ?x)
            (LeftOf4 ?x)
          )
          (and
            (RightOf18 ?x)
            (LeftOf4 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf10 ?x)
          )
          (and
            (RightOf17 ?x)
            (LeftOf4 ?x)
          )
          (and
            (LeftOf1 ?x)
            (RightOf18 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf7 ?x)
          )
          (and
            (RightOf9 ?x)
            (LeftOf2 ?x)
          )
          (and
            (LeftOf4 ?x)
            (RightOf18 ?x)
          )
          (and
            (RightOf15 ?x)
            (LeftOf2 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf17 ?x)
          )
          (and
            (LeftOf8 ?x)
            (RightOf17 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf9 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf11 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf19 ?x)
          )
          (and
            (LeftOf7 ?x)
            (RightOf19 ?x)
          )
          (and
            (LeftOf8 ?x)
            (RightOf11 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf13 ?x)
          )
          (and
            (LeftOf1 ?x)
            (RightOf13 ?x)
          )
          (and
            (LeftOf1 ?x)
            (RightOf19 ?x)
          )
          (and
            (RightOf19 ?x)
            (LeftOf4 ?x)
          )
          (and
            (LeftOf4 ?x)
            (RightOf10 ?x)
          )
          (and
            (RightOf19 ?x)
            (LeftOf3 ?x)
          )
          (and
            (RightOf12 ?x)
            (LeftOf8 ?x)
          )
          (and
            (RightOf12 ?x)
            (LeftOf3 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf19 ?x)
          )
          (and
            (LeftOf7 ?x)
            (RightOf17 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf14 ?x)
          )
          (and
            (LeftOf8 ?x)
            (RightOf14 ?x)
          )
          (and
            (RightOf16 ?x)
            (LeftOf4 ?x)
          )
          (and
            (RightOf7 ?x)
            (LeftOf4 ?x)
          )
          (and
            (LeftOf8 ?x)
            (RightOf18 ?x)
          )
          (and
            (LeftOf8 ?x)
            (RightOf7 ?x)
          )
          (and
            (RightOf9 ?x)
            (LeftOf8 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf18 ?x)
          )
          (and
            (LeftOf7 ?x)
            (RightOf8 ?x)
          )
          (and
            (RightOf14 ?x)
            (LeftOf3 ?x)
          )
          (and
            (LeftOf7 ?x)
            (RightOf11 ?x)
          )
          (and
            (RightOf14 ?x)
            (LeftOf2 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf15 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf9 ?x)
          )
          (and
            (RightOf15 ?x)
            (LeftOf7 ?x)
          )
          (and
            (RightOf9 ?x)
            (LeftOf3 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf18 ?x)
          )
          (and
            (RightOf14 ?x)
            (LeftOf1 ?x)
          )
          (and
            (LeftOf4 ?x)
            (RightOf13 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf10 ?x)
          )
          (and
            (RightOf12 ?x)
            (LeftOf6 ?x)
          )
          (and
            (RightOf15 ?x)
            (LeftOf4 ?x)
          )
          (and
            (RightOf18 ?x)
            (LeftOf3 ?x)
          )
          (and
            (RightOf15 ?x)
            (LeftOf8 ?x)
          )
          (and
            (RightOf12 ?x)
            (LeftOf5 ?x)
          )
          (and
            (RightOf12 ?x)
            (LeftOf7 ?x)
          )
          (and
            (RightOf7 ?x)
            (LeftOf2 ?x)
          )
          (and
            (RightOf12 ?x)
            (LeftOf1 ?x)
          )
          (and
            (LeftOf7 ?x)
            (RightOf7 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf17 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf16 ?x)
          )
          (and
            (LeftOf8 ?x)
            (RightOf16 ?x)
          )
          (and
            (RightOf17 ?x)
            (LeftOf2 ?x)
          )
          (and
            (RightOf14 ?x)
            (LeftOf4 ?x)
          )
          (and
            (RightOf7 ?x)
            (LeftOf3 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf12 ?x)
          )
          (and
            (RightOf7 ?x)
            (LeftOf1 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf7 ?x)
          )
          (and
            (LeftOf7 ?x)
            (RightOf18 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf13 ?x)
          )
          (and
            (LeftOf8 ?x)
            (RightOf13 ?x)
          )
          (and
            (LeftOf1 ?x)
            (RightOf10 ?x)
          )
          (and
            (RightOf13 ?x)
            (LeftOf3 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf13 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf14 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf8 ?x)
          )
          (and
            (RightOf17 ?x)
            (LeftOf3 ?x)
          )
          (and
            (RightOf9 ?x)
            (LeftOf7 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf16 ?x)
          )
          (and
            (RightOf15 ?x)
            (LeftOf3 ?x)
          )
          (and
            (RightOf12 ?x)
            (LeftOf2 ?x)
          )
          (and
            (RightOf8 ?x)
            (LeftOf3 ?x)
          )
          (and
            (RightOf10 ?x)
            (LeftOf3 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf10 ?x)
          )
          (and
            (LeftOf8 ?x)
            (RightOf10 ?x)
          )
          (and
            (RightOf8 ?x)
            (LeftOf4 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf11 ?x)
          )
          (and
            (RightOf9 ?x)
            (LeftOf1 ?x)
          )
          (and
            (LeftOf7 ?x)
            (RightOf13 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf15 ?x)
          )
          (and
            (RightOf17 ?x)
            (LeftOf1 ?x)
          )
          (and
            (RightOf15 ?x)
            (LeftOf1 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf18 ?x)
          )
        )
        (and
          (Column6 ?x)
        )
      )
      (when
        (or
          (and
            (RightOf14 ?x)
            (LeftOf9 ?x)
          )
          (and
            (LeftOf7 ?x)
            (RightOf10 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf11 ?x)
          )
          (and
            (RightOf9 ?x)
            (LeftOf4 ?x)
          )
          (and
            (LeftOf8 ?x)
            (RightOf19 ?x)
          )
          (and
            (LeftOf3 ?x)
            (RightOf13 ?x)
          )
          (and
            (RightOf11 ?x)
            (LeftOf4 ?x)
          )
          (and
            (RightOf12 ?x)
            (LeftOf4 ?x)
          )
          (and
            (RightOf17 ?x)
            (LeftOf9 ?x)
          )
          (and
            (LeftOf1 ?x)
            (RightOf8 ?x)
          )
          (and
            (RightOf16 ?x)
            (LeftOf1 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf19 ?x)
          )
          (and
            (RightOf14 ?x)
            (LeftOf7 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf8 ?x)
          )
          (and
            (LeftOf7 ?x)
            (RightOf16 ?x)
          )
          (and
            (RightOf16 ?x)
            (LeftOf3 ?x)
          )
          (and
            (LeftOf8 ?x)
            (RightOf8 ?x)
          )
          (and
            (LeftOf1 ?x)
            (RightOf11 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf8 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf16 ?x)
          )
          (and
            (RightOf10 ?x)
            (LeftOf4 ?x)
          )
          (and
            (RightOf18 ?x)
            (LeftOf4 ?x)
          )
          (and
            (LeftOf9 ?x)
            (RightOf19 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf10 ?x)
          )
          (and
            (RightOf17 ?x)
            (LeftOf4 ?x)
          )
          (and
            (RightOf16 ?x)
            (LeftOf9 ?x)
          )
          (and
            (LeftOf1 ?x)
            (RightOf18 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf16 ?x)
          )
          (and
            (LeftOf4 ?x)
            (RightOf18 ?x)
          )
          (and
            (RightOf15 ?x)
            (LeftOf2 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf17 ?x)
          )
          (and
            (LeftOf8 ?x)
            (RightOf17 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf9 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf11 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf19 ?x)
          )
          (and
            (LeftOf7 ?x)
            (RightOf19 ?x)
          )
          (and
            (LeftOf8 ?x)
            (RightOf11 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf13 ?x)
          )
          (and
            (LeftOf1 ?x)
            (RightOf13 ?x)
          )
          (and
            (LeftOf1 ?x)
            (RightOf19 ?x)
          )
          (and
            (LeftOf9 ?x)
            (RightOf18 ?x)
          )
          (and
            (RightOf19 ?x)
            (LeftOf4 ?x)
          )
          (and
            (LeftOf4 ?x)
            (RightOf10 ?x)
          )
          (and
            (RightOf19 ?x)
            (LeftOf3 ?x)
          )
          (and
            (RightOf12 ?x)
            (LeftOf8 ?x)
          )
          (and
            (LeftOf9 ?x)
            (RightOf10 ?x)
          )
          (and
            (RightOf12 ?x)
            (LeftOf3 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf19 ?x)
          )
          (and
            (LeftOf7 ?x)
            (RightOf17 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf14 ?x)
          )
          (and
            (LeftOf8 ?x)
            (RightOf14 ?x)
          )
          (and
            (RightOf16 ?x)
            (LeftOf4 ?x)
          )
          (and
            (LeftOf8 ?x)
            (RightOf18 ?x)
          )
          (and
            (RightOf9 ?x)
            (LeftOf8 ?x)
          )
          (and
            (RightOf11 ?x)
            (LeftOf3 ?x)
          )
          (and
            (RightOf15 ?x)
            (LeftOf9 ?x)
          )
          (and
            (RightOf14 ?x)
            (LeftOf3 ?x)
          )
          (and
            (LeftOf7 ?x)
            (RightOf11 ?x)
          )
          (and
            (RightOf14 ?x)
            (LeftOf2 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf15 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf9 ?x)
          )
          (and
            (RightOf15 ?x)
            (LeftOf7 ?x)
          )
          (and
            (RightOf9 ?x)
            (LeftOf3 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf18 ?x)
          )
          (and
            (RightOf14 ?x)
            (LeftOf1 ?x)
          )
          (and
            (LeftOf4 ?x)
            (RightOf13 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf10 ?x)
          )
          (and
            (RightOf12 ?x)
            (LeftOf6 ?x)
          )
          (and
            (RightOf15 ?x)
            (LeftOf4 ?x)
          )
          (and
            (RightOf18 ?x)
            (LeftOf3 ?x)
          )
          (and
            (RightOf15 ?x)
            (LeftOf8 ?x)
          )
          (and
            (LeftOf9 ?x)
            (RightOf11 ?x)
          )
          (and
            (RightOf12 ?x)
            (LeftOf5 ?x)
          )
          (and
            (RightOf12 ?x)
            (LeftOf7 ?x)
          )
          (and
            (RightOf14 ?x)
            (LeftOf8 ?x)
          )
          (and
            (RightOf12 ?x)
            (LeftOf1 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf17 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf16 ?x)
          )
          (and
            (LeftOf8 ?x)
            (RightOf16 ?x)
          )
          (and
            (RightOf17 ?x)
            (LeftOf2 ?x)
          )
          (and
            (RightOf14 ?x)
            (LeftOf4 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf12 ?x)
          )
          (and
            (RightOf9 ?x)
            (LeftOf9 ?x)
          )
          (and
            (LeftOf7 ?x)
            (RightOf18 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf13 ?x)
          )
          (and
            (LeftOf8 ?x)
            (RightOf13 ?x)
          )
          (and
            (LeftOf1 ?x)
            (RightOf10 ?x)
          )
          (and
            (RightOf13 ?x)
            (LeftOf3 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf13 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf14 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf18 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf8 ?x)
          )
          (and
            (LeftOf7 ?x)
            (RightOf8 ?x)
          )
          (and
            (RightOf17 ?x)
            (LeftOf3 ?x)
          )
          (and
            (RightOf9 ?x)
            (LeftOf7 ?x)
          )
          (and
            (RightOf9 ?x)
            (LeftOf2 ?x)
          )
          (and
            (RightOf15 ?x)
            (LeftOf3 ?x)
          )
          (and
            (RightOf12 ?x)
            (LeftOf9 ?x)
          )
          (and
            (RightOf12 ?x)
            (LeftOf2 ?x)
          )
          (and
            (RightOf8 ?x)
            (LeftOf3 ?x)
          )
          (and
            (RightOf10 ?x)
            (LeftOf3 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf10 ?x)
          )
          (and
            (LeftOf8 ?x)
            (RightOf10 ?x)
          )
          (and
            (RightOf8 ?x)
            (LeftOf4 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf11 ?x)
          )
          (and
            (RightOf9 ?x)
            (LeftOf1 ?x)
          )
          (and
            (LeftOf7 ?x)
            (RightOf13 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf15 ?x)
          )
          (and
            (LeftOf9 ?x)
            (RightOf8 ?x)
          )
          (and
            (LeftOf9 ?x)
            (RightOf13 ?x)
          )
          (and
            (RightOf17 ?x)
            (LeftOf1 ?x)
          )
          (and
            (RightOf15 ?x)
            (LeftOf1 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf18 ?x)
          )
        )
        (and
          (Column7 ?x)
        )
      )
      (when
        (or
          (and
            (RightOf14 ?x)
            (LeftOf9 ?x)
          )
          (and
            (LeftOf7 ?x)
            (RightOf10 ?x)
          )
          (and
            (RightOf15 ?x)
            (LeftOf10 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf11 ?x)
          )
          (and
            (RightOf9 ?x)
            (LeftOf4 ?x)
          )
          (and
            (LeftOf8 ?x)
            (RightOf19 ?x)
          )
          (and
            (LeftOf3 ?x)
            (RightOf13 ?x)
          )
          (and
            (RightOf11 ?x)
            (LeftOf4 ?x)
          )
          (and
            (RightOf12 ?x)
            (LeftOf4 ?x)
          )
          (and
            (RightOf17 ?x)
            (LeftOf9 ?x)
          )
          (and
            (RightOf14 ?x)
            (LeftOf2 ?x)
          )
          (and
            (RightOf16 ?x)
            (LeftOf1 ?x)
          )
          (and
            (LeftOf10 ?x)
            (RightOf16 ?x)
          )
          (and
            (RightOf14 ?x)
            (LeftOf7 ?x)
          )
          (and
            (LeftOf7 ?x)
            (RightOf16 ?x)
          )
          (and
            (RightOf16 ?x)
            (LeftOf3 ?x)
          )
          (and
            (LeftOf1 ?x)
            (RightOf11 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf16 ?x)
          )
          (and
            (RightOf18 ?x)
            (LeftOf4 ?x)
          )
          (and
            (LeftOf9 ?x)
            (RightOf19 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf10 ?x)
          )
          (and
            (RightOf17 ?x)
            (LeftOf4 ?x)
          )
          (and
            (RightOf16 ?x)
            (LeftOf9 ?x)
          )
          (and
            (LeftOf10 ?x)
            (RightOf10 ?x)
          )
          (and
            (LeftOf10 ?x)
            (RightOf11 ?x)
          )
          (and
            (LeftOf1 ?x)
            (RightOf18 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf16 ?x)
          )
          (and
            (LeftOf7 ?x)
            (RightOf11 ?x)
          )
          (and
            (LeftOf4 ?x)
            (RightOf18 ?x)
          )
          (and
            (RightOf15 ?x)
            (LeftOf2 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf17 ?x)
          )
          (and
            (LeftOf8 ?x)
            (RightOf17 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf9 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf11 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf19 ?x)
          )
          (and
            (LeftOf7 ?x)
            (RightOf19 ?x)
          )
          (and
            (LeftOf8 ?x)
            (RightOf11 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf13 ?x)
          )
          (and
            (LeftOf1 ?x)
            (RightOf13 ?x)
          )
          (and
            (LeftOf1 ?x)
            (RightOf19 ?x)
          )
          (and
            (LeftOf9 ?x)
            (RightOf18 ?x)
          )
          (and
            (RightOf19 ?x)
            (LeftOf4 ?x)
          )
          (and
            (LeftOf4 ?x)
            (RightOf10 ?x)
          )
          (and
            (RightOf19 ?x)
            (LeftOf3 ?x)
          )
          (and
            (RightOf9 ?x)
            (LeftOf10 ?x)
          )
          (and
            (RightOf12 ?x)
            (LeftOf8 ?x)
          )
          (and
            (LeftOf9 ?x)
            (RightOf10 ?x)
          )
          (and
            (RightOf12 ?x)
            (LeftOf3 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf19 ?x)
          )
          (and
            (LeftOf7 ?x)
            (RightOf17 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf14 ?x)
          )
          (and
            (LeftOf8 ?x)
            (RightOf14 ?x)
          )
          (and
            (RightOf16 ?x)
            (LeftOf4 ?x)
          )
          (and
            (LeftOf8 ?x)
            (RightOf18 ?x)
          )
          (and
            (LeftOf10 ?x)
            (RightOf13 ?x)
          )
          (and
            (RightOf9 ?x)
            (LeftOf8 ?x)
          )
          (and
            (RightOf11 ?x)
            (LeftOf3 ?x)
          )
          (and
            (RightOf15 ?x)
            (LeftOf9 ?x)
          )
          (and
            (RightOf14 ?x)
            (LeftOf3 ?x)
          )
          (and
            (LeftOf10 ?x)
            (RightOf14 ?x)
          )
          (and
            (RightOf9 ?x)
            (LeftOf1 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf15 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf9 ?x)
          )
          (and
            (RightOf15 ?x)
            (LeftOf7 ?x)
          )
          (and
            (RightOf9 ?x)
            (LeftOf3 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf18 ?x)
          )
          (and
            (RightOf14 ?x)
            (LeftOf1 ?x)
          )
          (and
            (LeftOf4 ?x)
            (RightOf13 ?x)
          )
          (and
            (LeftOf10 ?x)
            (RightOf17 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf19 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf10 ?x)
          )
          (and
            (RightOf12 ?x)
            (LeftOf6 ?x)
          )
          (and
            (LeftOf10 ?x)
            (RightOf18 ?x)
          )
          (and
            (RightOf15 ?x)
            (LeftOf4 ?x)
          )
          (and
            (RightOf18 ?x)
            (LeftOf3 ?x)
          )
          (and
            (RightOf15 ?x)
            (LeftOf8 ?x)
          )
          (and
            (LeftOf9 ?x)
            (RightOf11 ?x)
          )
          (and
            (RightOf12 ?x)
            (LeftOf5 ?x)
          )
          (and
            (RightOf12 ?x)
            (LeftOf7 ?x)
          )
          (and
            (RightOf14 ?x)
            (LeftOf8 ?x)
          )
          (and
            (RightOf12 ?x)
            (LeftOf1 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf17 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf16 ?x)
          )
          (and
            (LeftOf8 ?x)
            (RightOf16 ?x)
          )
          (and
            (RightOf17 ?x)
            (LeftOf2 ?x)
          )
          (and
            (RightOf14 ?x)
            (LeftOf4 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf12 ?x)
          )
          (and
            (RightOf9 ?x)
            (LeftOf9 ?x)
          )
          (and
            (LeftOf7 ?x)
            (RightOf18 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf13 ?x)
          )
          (and
            (LeftOf8 ?x)
            (RightOf13 ?x)
          )
          (and
            (LeftOf1 ?x)
            (RightOf10 ?x)
          )
          (and
            (RightOf13 ?x)
            (LeftOf3 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf13 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf14 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf18 ?x)
          )
          (and
            (RightOf17 ?x)
            (LeftOf3 ?x)
          )
          (and
            (RightOf9 ?x)
            (LeftOf7 ?x)
          )
          (and
            (RightOf9 ?x)
            (LeftOf2 ?x)
          )
          (and
            (RightOf15 ?x)
            (LeftOf3 ?x)
          )
          (and
            (RightOf12 ?x)
            (LeftOf9 ?x)
          )
          (and
            (RightOf12 ?x)
            (LeftOf2 ?x)
          )
          (and
            (RightOf10 ?x)
            (LeftOf4 ?x)
          )
          (and
            (RightOf10 ?x)
            (LeftOf3 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf10 ?x)
          )
          (and
            (LeftOf8 ?x)
            (RightOf10 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf11 ?x)
          )
          (and
            (LeftOf7 ?x)
            (RightOf13 ?x)
          )
          (and
            (LeftOf10 ?x)
            (RightOf19 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf15 ?x)
          )
          (and
            (LeftOf9 ?x)
            (RightOf13 ?x)
          )
          (and
            (RightOf12 ?x)
            (LeftOf10 ?x)
          )
          (and
            (RightOf17 ?x)
            (LeftOf1 ?x)
          )
          (and
            (RightOf15 ?x)
            (LeftOf1 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf18 ?x)
          )
        )
        (and
          (Column8 ?x)
        )
      )
      (when
        (or
          (and
            (RightOf14 ?x)
            (LeftOf9 ?x)
          )
          (and
            (LeftOf11 ?x)
            (RightOf10 ?x)
          )
          (and
            (RightOf17 ?x)
            (LeftOf11 ?x)
          )
          (and
            (LeftOf7 ?x)
            (RightOf10 ?x)
          )
          (and
            (RightOf15 ?x)
            (LeftOf10 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf11 ?x)
          )
          (and
            (RightOf19 ?x)
            (LeftOf4 ?x)
          )
          (and
            (LeftOf11 ?x)
            (RightOf19 ?x)
          )
          (and
            (LeftOf8 ?x)
            (RightOf19 ?x)
          )
          (and
            (LeftOf3 ?x)
            (RightOf13 ?x)
          )
          (and
            (RightOf11 ?x)
            (LeftOf4 ?x)
          )
          (and
            (RightOf12 ?x)
            (LeftOf4 ?x)
          )
          (and
            (RightOf17 ?x)
            (LeftOf9 ?x)
          )
          (and
            (RightOf14 ?x)
            (LeftOf2 ?x)
          )
          (and
            (RightOf16 ?x)
            (LeftOf1 ?x)
          )
          (and
            (LeftOf10 ?x)
            (RightOf16 ?x)
          )
          (and
            (LeftOf10 ?x)
            (RightOf18 ?x)
          )
          (and
            (LeftOf7 ?x)
            (RightOf16 ?x)
          )
          (and
            (RightOf16 ?x)
            (LeftOf3 ?x)
          )
          (and
            (LeftOf1 ?x)
            (RightOf11 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf16 ?x)
          )
          (and
            (LeftOf11 ?x)
            (RightOf18 ?x)
          )
          (and
            (RightOf18 ?x)
            (LeftOf4 ?x)
          )
          (and
            (LeftOf9 ?x)
            (RightOf19 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf10 ?x)
          )
          (and
            (RightOf17 ?x)
            (LeftOf4 ?x)
          )
          (and
            (RightOf16 ?x)
            (LeftOf9 ?x)
          )
          (and
            (LeftOf10 ?x)
            (RightOf10 ?x)
          )
          (and
            (LeftOf10 ?x)
            (RightOf11 ?x)
          )
          (and
            (LeftOf1 ?x)
            (RightOf18 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf16 ?x)
          )
          (and
            (LeftOf4 ?x)
            (RightOf18 ?x)
          )
          (and
            (RightOf15 ?x)
            (LeftOf2 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf17 ?x)
          )
          (and
            (LeftOf8 ?x)
            (RightOf17 ?x)
          )
          (and
            (RightOf12 ?x)
            (LeftOf11 ?x)
          )
          (and
            (LeftOf7 ?x)
            (RightOf19 ?x)
          )
          (and
            (LeftOf8 ?x)
            (RightOf11 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf13 ?x)
          )
          (and
            (LeftOf1 ?x)
            (RightOf13 ?x)
          )
          (and
            (LeftOf1 ?x)
            (RightOf19 ?x)
          )
          (and
            (LeftOf9 ?x)
            (RightOf18 ?x)
          )
          (and
            (RightOf17 ?x)
            (LeftOf1 ?x)
          )
          (and
            (RightOf12 ?x)
            (LeftOf8 ?x)
          )
          (and
            (LeftOf9 ?x)
            (RightOf10 ?x)
          )
          (and
            (RightOf12 ?x)
            (LeftOf3 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf19 ?x)
          )
          (and
            (LeftOf7 ?x)
            (RightOf17 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf14 ?x)
          )
          (and
            (LeftOf8 ?x)
            (RightOf14 ?x)
          )
          (and
            (RightOf16 ?x)
            (LeftOf4 ?x)
          )
          (and
            (LeftOf8 ?x)
            (RightOf18 ?x)
          )
          (and
            (LeftOf10 ?x)
            (RightOf13 ?x)
          )
          (and
            (RightOf11 ?x)
            (LeftOf3 ?x)
          )
          (and
            (RightOf15 ?x)
            (LeftOf9 ?x)
          )
          (and
            (RightOf14 ?x)
            (LeftOf3 ?x)
          )
          (and
            (LeftOf7 ?x)
            (RightOf11 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf19 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf15 ?x)
          )
          (and
            (RightOf14 ?x)
            (LeftOf11 ?x)
          )
          (and
            (RightOf15 ?x)
            (LeftOf7 ?x)
          )
          (and
            (RightOf19 ?x)
            (LeftOf3 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf18 ?x)
          )
          (and
            (RightOf14 ?x)
            (LeftOf1 ?x)
          )
          (and
            (LeftOf4 ?x)
            (RightOf13 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf11 ?x)
          )
          (and
            (LeftOf10 ?x)
            (RightOf17 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf19 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf10 ?x)
          )
          (and
            (RightOf12 ?x)
            (LeftOf6 ?x)
          )
          (and
            (RightOf14 ?x)
            (LeftOf7 ?x)
          )
          (and
            (RightOf15 ?x)
            (LeftOf4 ?x)
          )
          (and
            (RightOf18 ?x)
            (LeftOf3 ?x)
          )
          (and
            (RightOf15 ?x)
            (LeftOf8 ?x)
          )
          (and
            (LeftOf11 ?x)
            (RightOf13 ?x)
          )
          (and
            (LeftOf9 ?x)
            (RightOf11 ?x)
          )
          (and
            (RightOf12 ?x)
            (LeftOf5 ?x)
          )
          (and
            (RightOf12 ?x)
            (LeftOf7 ?x)
          )
          (and
            (RightOf14 ?x)
            (LeftOf8 ?x)
          )
          (and
            (RightOf12 ?x)
            (LeftOf1 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf17 ?x)
          )
          (and
            (RightOf14 ?x)
            (LeftOf10 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf16 ?x)
          )
          (and
            (LeftOf8 ?x)
            (RightOf16 ?x)
          )
          (and
            (RightOf17 ?x)
            (LeftOf2 ?x)
          )
          (and
            (RightOf14 ?x)
            (LeftOf4 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf12 ?x)
          )
          (and
            (LeftOf7 ?x)
            (RightOf18 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf13 ?x)
          )
          (and
            (LeftOf8 ?x)
            (RightOf13 ?x)
          )
          (and
            (LeftOf10 ?x)
            (RightOf14 ?x)
          )
          (and
            (LeftOf1 ?x)
            (RightOf10 ?x)
          )
          (and
            (RightOf13 ?x)
            (LeftOf3 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf13 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf14 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf18 ?x)
          )
          (and
            (RightOf17 ?x)
            (LeftOf3 ?x)
          )
          (and
            (RightOf15 ?x)
            (LeftOf3 ?x)
          )
          (and
            (RightOf12 ?x)
            (LeftOf9 ?x)
          )
          (and
            (LeftOf11 ?x)
            (RightOf11 ?x)
          )
          (and
            (RightOf12 ?x)
            (LeftOf2 ?x)
          )
          (and
            (RightOf10 ?x)
            (LeftOf4 ?x)
          )
          (and
            (RightOf10 ?x)
            (LeftOf3 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf10 ?x)
          )
          (and
            (LeftOf8 ?x)
            (RightOf10 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf11 ?x)
          )
          (and
            (LeftOf7 ?x)
            (RightOf13 ?x)
          )
          (and
            (LeftOf10 ?x)
            (RightOf19 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf15 ?x)
          )
          (and
            (RightOf16 ?x)
            (LeftOf11 ?x)
          )
          (and
            (LeftOf9 ?x)
            (RightOf13 ?x)
          )
          (and
            (RightOf12 ?x)
            (LeftOf10 ?x)
          )
          (and
            (RightOf15 ?x)
            (LeftOf11 ?x)
          )
          (and
            (RightOf15 ?x)
            (LeftOf1 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf18 ?x)
          )
        )
        (and
          (Column9 ?x)
        )
      )
      (when
        (or
          (and
            (RightOf14 ?x)
            (LeftOf9 ?x)
          )
          (and
            (RightOf17 ?x)
            (LeftOf11 ?x)
          )
          (and
            (RightOf15 ?x)
            (LeftOf10 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf11 ?x)
          )
          (and
            (LeftOf12 ?x)
            (RightOf13 ?x)
          )
          (and
            (RightOf19 ?x)
            (LeftOf4 ?x)
          )
          (and
            (LeftOf11 ?x)
            (RightOf19 ?x)
          )
          (and
            (LeftOf8 ?x)
            (RightOf19 ?x)
          )
          (and
            (LeftOf3 ?x)
            (RightOf13 ?x)
          )
          (and
            (RightOf11 ?x)
            (LeftOf4 ?x)
          )
          (and
            (RightOf12 ?x)
            (LeftOf4 ?x)
          )
          (and
            (RightOf17 ?x)
            (LeftOf9 ?x)
          )
          (and
            (RightOf14 ?x)
            (LeftOf2 ?x)
          )
          (and
            (RightOf16 ?x)
            (LeftOf1 ?x)
          )
          (and
            (LeftOf10 ?x)
            (RightOf16 ?x)
          )
          (and
            (LeftOf10 ?x)
            (RightOf18 ?x)
          )
          (and
            (LeftOf7 ?x)
            (RightOf16 ?x)
          )
          (and
            (RightOf16 ?x)
            (LeftOf3 ?x)
          )
          (and
            (LeftOf12 ?x)
            (RightOf15 ?x)
          )
          (and
            (LeftOf1 ?x)
            (RightOf11 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf16 ?x)
          )
          (and
            (LeftOf11 ?x)
            (RightOf18 ?x)
          )
          (and
            (LeftOf9 ?x)
            (RightOf19 ?x)
          )
          (and
            (RightOf17 ?x)
            (LeftOf4 ?x)
          )
          (and
            (RightOf16 ?x)
            (LeftOf9 ?x)
          )
          (and
            (LeftOf10 ?x)
            (RightOf11 ?x)
          )
          (and
            (LeftOf1 ?x)
            (RightOf18 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf16 ?x)
          )
          (and
            (LeftOf4 ?x)
            (RightOf18 ?x)
          )
          (and
            (RightOf15 ?x)
            (LeftOf2 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf17 ?x)
          )
          (and
            (LeftOf8 ?x)
            (RightOf17 ?x)
          )
          (and
            (RightOf12 ?x)
            (LeftOf11 ?x)
          )
          (and
            (LeftOf7 ?x)
            (RightOf19 ?x)
          )
          (and
            (LeftOf8 ?x)
            (RightOf11 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf13 ?x)
          )
          (and
            (LeftOf1 ?x)
            (RightOf13 ?x)
          )
          (and
            (LeftOf1 ?x)
            (RightOf19 ?x)
          )
          (and
            (LeftOf9 ?x)
            (RightOf18 ?x)
          )
          (and
            (RightOf17 ?x)
            (LeftOf1 ?x)
          )
          (and
            (RightOf12 ?x)
            (LeftOf8 ?x)
          )
          (and
            (LeftOf12 ?x)
            (RightOf17 ?x)
          )
          (and
            (RightOf12 ?x)
            (LeftOf3 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf19 ?x)
          )
          (and
            (LeftOf7 ?x)
            (RightOf17 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf14 ?x)
          )
          (and
            (LeftOf8 ?x)
            (RightOf14 ?x)
          )
          (and
            (RightOf16 ?x)
            (LeftOf4 ?x)
          )
          (and
            (LeftOf12 ?x)
            (RightOf16 ?x)
          )
          (and
            (LeftOf8 ?x)
            (RightOf18 ?x)
          )
          (and
            (LeftOf10 ?x)
            (RightOf13 ?x)
          )
          (and
            (RightOf11 ?x)
            (LeftOf3 ?x)
          )
          (and
            (LeftOf12 ?x)
            (RightOf12 ?x)
          )
          (and
            (RightOf15 ?x)
            (LeftOf9 ?x)
          )
          (and
            (RightOf14 ?x)
            (LeftOf3 ?x)
          )
          (and
            (LeftOf7 ?x)
            (RightOf11 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf19 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf15 ?x)
          )
          (and
            (RightOf14 ?x)
            (LeftOf11 ?x)
          )
          (and
            (RightOf15 ?x)
            (LeftOf7 ?x)
          )
          (and
            (RightOf19 ?x)
            (LeftOf3 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf18 ?x)
          )
          (and
            (RightOf14 ?x)
            (LeftOf1 ?x)
          )
          (and
            (LeftOf4 ?x)
            (RightOf13 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf11 ?x)
          )
          (and
            (LeftOf10 ?x)
            (RightOf17 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf19 ?x)
          )
          (and
            (RightOf12 ?x)
            (LeftOf6 ?x)
          )
          (and
            (RightOf14 ?x)
            (LeftOf7 ?x)
          )
          (and
            (RightOf15 ?x)
            (LeftOf4 ?x)
          )
          (and
            (RightOf18 ?x)
            (LeftOf3 ?x)
          )
          (and
            (LeftOf12 ?x)
            (RightOf19 ?x)
          )
          (and
            (RightOf15 ?x)
            (LeftOf8 ?x)
          )
          (and
            (LeftOf11 ?x)
            (RightOf13 ?x)
          )
          (and
            (LeftOf9 ?x)
            (RightOf11 ?x)
          )
          (and
            (RightOf12 ?x)
            (LeftOf5 ?x)
          )
          (and
            (RightOf12 ?x)
            (LeftOf7 ?x)
          )
          (and
            (RightOf14 ?x)
            (LeftOf8 ?x)
          )
          (and
            (RightOf12 ?x)
            (LeftOf1 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf17 ?x)
          )
          (and
            (RightOf14 ?x)
            (LeftOf10 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf16 ?x)
          )
          (and
            (LeftOf8 ?x)
            (RightOf16 ?x)
          )
          (and
            (RightOf17 ?x)
            (LeftOf2 ?x)
          )
          (and
            (RightOf14 ?x)
            (LeftOf4 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf12 ?x)
          )
          (and
            (LeftOf7 ?x)
            (RightOf18 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf13 ?x)
          )
          (and
            (LeftOf8 ?x)
            (RightOf13 ?x)
          )
          (and
            (RightOf13 ?x)
            (LeftOf3 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf13 ?x)
          )
          (and
            (LeftOf12 ?x)
            (RightOf18 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf14 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf18 ?x)
          )
          (and
            (RightOf17 ?x)
            (LeftOf3 ?x)
          )
          (and
            (RightOf15 ?x)
            (LeftOf3 ?x)
          )
          (and
            (RightOf12 ?x)
            (LeftOf9 ?x)
          )
          (and
            (LeftOf11 ?x)
            (RightOf11 ?x)
          )
          (and
            (RightOf12 ?x)
            (LeftOf2 ?x)
          )
          (and
            (LeftOf10 ?x)
            (RightOf14 ?x)
          )
          (and
            (RightOf18 ?x)
            (LeftOf4 ?x)
          )
          (and
            (LeftOf12 ?x)
            (RightOf11 ?x)
          )
          (and
            (LeftOf12 ?x)
            (RightOf14 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf11 ?x)
          )
          (and
            (LeftOf7 ?x)
            (RightOf13 ?x)
          )
          (and
            (LeftOf10 ?x)
            (RightOf19 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf15 ?x)
          )
          (and
            (RightOf16 ?x)
            (LeftOf11 ?x)
          )
          (and
            (LeftOf9 ?x)
            (RightOf13 ?x)
          )
          (and
            (RightOf12 ?x)
            (LeftOf10 ?x)
          )
          (and
            (RightOf15 ?x)
            (LeftOf11 ?x)
          )
          (and
            (RightOf15 ?x)
            (LeftOf1 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf18 ?x)
          )
        )
        (and
          (Column10 ?x)
        )
      )
      (when
        (or
          (and
            (RightOf14 ?x)
            (LeftOf9 ?x)
          )
          (and
            (LeftOf13 ?x)
            (RightOf16 ?x)
          )
          (and
            (RightOf17 ?x)
            (LeftOf11 ?x)
          )
          (and
            (RightOf15 ?x)
            (LeftOf10 ?x)
          )
          (and
            (RightOf15 ?x)
            (LeftOf2 ?x)
          )
          (and
            (LeftOf12 ?x)
            (RightOf13 ?x)
          )
          (and
            (RightOf19 ?x)
            (LeftOf4 ?x)
          )
          (and
            (LeftOf11 ?x)
            (RightOf19 ?x)
          )
          (and
            (LeftOf8 ?x)
            (RightOf19 ?x)
          )
          (and
            (LeftOf3 ?x)
            (RightOf13 ?x)
          )
          (and
            (RightOf16 ?x)
            (LeftOf1 ?x)
          )
          (and
            (RightOf12 ?x)
            (LeftOf4 ?x)
          )
          (and
            (RightOf17 ?x)
            (LeftOf9 ?x)
          )
          (and
            (RightOf14 ?x)
            (LeftOf2 ?x)
          )
          (and
            (LeftOf10 ?x)
            (RightOf16 ?x)
          )
          (and
            (RightOf14 ?x)
            (LeftOf7 ?x)
          )
          (and
            (LeftOf7 ?x)
            (RightOf16 ?x)
          )
          (and
            (RightOf12 ?x)
            (LeftOf13 ?x)
          )
          (and
            (RightOf16 ?x)
            (LeftOf3 ?x)
          )
          (and
            (LeftOf12 ?x)
            (RightOf15 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf16 ?x)
          )
          (and
            (LeftOf11 ?x)
            (RightOf18 ?x)
          )
          (and
            (LeftOf9 ?x)
            (RightOf19 ?x)
          )
          (and
            (RightOf17 ?x)
            (LeftOf4 ?x)
          )
          (and
            (RightOf16 ?x)
            (LeftOf9 ?x)
          )
          (and
            (LeftOf1 ?x)
            (RightOf18 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf16 ?x)
          )
          (and
            (LeftOf4 ?x)
            (RightOf18 ?x)
          )
          (and
            (LeftOf13 ?x)
            (RightOf18 ?x)
          )
          (and
            (LeftOf8 ?x)
            (RightOf17 ?x)
          )
          (and
            (RightOf12 ?x)
            (LeftOf11 ?x)
          )
          (and
            (LeftOf7 ?x)
            (RightOf19 ?x)
          )
          (and
            (RightOf12 ?x)
            (LeftOf12 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf13 ?x)
          )
          (and
            (LeftOf1 ?x)
            (RightOf13 ?x)
          )
          (and
            (LeftOf1 ?x)
            (RightOf19 ?x)
          )
          (and
            (LeftOf9 ?x)
            (RightOf18 ?x)
          )
          (and
            (RightOf17 ?x)
            (LeftOf1 ?x)
          )
          (and
            (RightOf12 ?x)
            (LeftOf8 ?x)
          )
          (and
            (LeftOf12 ?x)
            (RightOf17 ?x)
          )
          (and
            (RightOf12 ?x)
            (LeftOf3 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf19 ?x)
          )
          (and
            (LeftOf7 ?x)
            (RightOf17 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf14 ?x)
          )
          (and
            (LeftOf8 ?x)
            (RightOf14 ?x)
          )
          (and
            (RightOf16 ?x)
            (LeftOf4 ?x)
          )
          (and
            (LeftOf12 ?x)
            (RightOf16 ?x)
          )
          (and
            (LeftOf8 ?x)
            (RightOf18 ?x)
          )
          (and
            (LeftOf10 ?x)
            (RightOf13 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf18 ?x)
          )
          (and
            (RightOf15 ?x)
            (LeftOf9 ?x)
          )
          (and
            (RightOf14 ?x)
            (LeftOf3 ?x)
          )
          (and
            (LeftOf10 ?x)
            (RightOf14 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf19 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf15 ?x)
          )
          (and
            (RightOf14 ?x)
            (LeftOf11 ?x)
          )
          (and
            (RightOf15 ?x)
            (LeftOf7 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf17 ?x)
          )
          (and
            (RightOf19 ?x)
            (LeftOf3 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf18 ?x)
          )
          (and
            (RightOf14 ?x)
            (LeftOf1 ?x)
          )
          (and
            (LeftOf4 ?x)
            (RightOf13 ?x)
          )
          (and
            (LeftOf10 ?x)
            (RightOf17 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf19 ?x)
          )
          (and
            (RightOf12 ?x)
            (LeftOf6 ?x)
          )
          (and
            (RightOf14 ?x)
            (LeftOf13 ?x)
          )
          (and
            (RightOf15 ?x)
            (LeftOf4 ?x)
          )
          (and
            (RightOf18 ?x)
            (LeftOf3 ?x)
          )
          (and
            (LeftOf12 ?x)
            (RightOf19 ?x)
          )
          (and
            (LeftOf9 ?x)
            (RightOf13 ?x)
          )
          (and
            (RightOf15 ?x)
            (LeftOf8 ?x)
          )
          (and
            (LeftOf11 ?x)
            (RightOf13 ?x)
          )
          (and
            (RightOf12 ?x)
            (LeftOf5 ?x)
          )
          (and
            (RightOf12 ?x)
            (LeftOf7 ?x)
          )
          (and
            (RightOf14 ?x)
            (LeftOf8 ?x)
          )
          (and
            (RightOf12 ?x)
            (LeftOf1 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf17 ?x)
          )
          (and
            (RightOf14 ?x)
            (LeftOf10 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf16 ?x)
          )
          (and
            (LeftOf8 ?x)
            (RightOf16 ?x)
          )
          (and
            (RightOf17 ?x)
            (LeftOf2 ?x)
          )
          (and
            (RightOf14 ?x)
            (LeftOf4 ?x)
          )
          (and
            (LeftOf7 ?x)
            (RightOf18 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf13 ?x)
          )
          (and
            (LeftOf8 ?x)
            (RightOf13 ?x)
          )
          (and
            (RightOf15 ?x)
            (LeftOf13 ?x)
          )
          (and
            (RightOf13 ?x)
            (LeftOf3 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf13 ?x)
          )
          (and
            (LeftOf13 ?x)
            (RightOf19 ?x)
          )
          (and
            (LeftOf12 ?x)
            (RightOf18 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf14 ?x)
          )
          (and
            (LeftOf13 ?x)
            (RightOf13 ?x)
          )
          (and
            (RightOf17 ?x)
            (LeftOf3 ?x)
          )
          (and
            (LeftOf10 ?x)
            (RightOf18 ?x)
          )
          (and
            (RightOf15 ?x)
            (LeftOf3 ?x)
          )
          (and
            (RightOf12 ?x)
            (LeftOf9 ?x)
          )
          (and
            (RightOf12 ?x)
            (LeftOf2 ?x)
          )
          (and
            (RightOf18 ?x)
            (LeftOf4 ?x)
          )
          (and
            (LeftOf12 ?x)
            (RightOf14 ?x)
          )
          (and
            (LeftOf7 ?x)
            (RightOf13 ?x)
          )
          (and
            (LeftOf10 ?x)
            (RightOf19 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf15 ?x)
          )
          (and
            (RightOf16 ?x)
            (LeftOf11 ?x)
          )
          (and
            (LeftOf13 ?x)
            (RightOf17 ?x)
          )
          (and
            (RightOf12 ?x)
            (LeftOf10 ?x)
          )
          (and
            (RightOf15 ?x)
            (LeftOf11 ?x)
          )
          (and
            (RightOf15 ?x)
            (LeftOf1 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf18 ?x)
          )
        )
        (and
          (Column11 ?x)
        )
      )
      (when
        (or
          (and
            (RightOf14 ?x)
            (LeftOf9 ?x)
          )
          (and
            (LeftOf13 ?x)
            (RightOf16 ?x)
          )
          (and
            (RightOf17 ?x)
            (LeftOf11 ?x)
          )
          (and
            (RightOf15 ?x)
            (LeftOf10 ?x)
          )
          (and
            (RightOf15 ?x)
            (LeftOf2 ?x)
          )
          (and
            (LeftOf12 ?x)
            (RightOf13 ?x)
          )
          (and
            (RightOf19 ?x)
            (LeftOf4 ?x)
          )
          (and
            (LeftOf11 ?x)
            (RightOf19 ?x)
          )
          (and
            (LeftOf8 ?x)
            (RightOf19 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf19 ?x)
          )
          (and
            (RightOf16 ?x)
            (LeftOf1 ?x)
          )
          (and
            (RightOf17 ?x)
            (LeftOf9 ?x)
          )
          (and
            (RightOf14 ?x)
            (LeftOf2 ?x)
          )
          (and
            (LeftOf10 ?x)
            (RightOf16 ?x)
          )
          (and
            (RightOf14 ?x)
            (LeftOf13 ?x)
          )
          (and
            (LeftOf7 ?x)
            (RightOf16 ?x)
          )
          (and
            (RightOf16 ?x)
            (LeftOf3 ?x)
          )
          (and
            (LeftOf12 ?x)
            (RightOf15 ?x)
          )
          (and
            (RightOf19 ?x)
            (LeftOf14 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf16 ?x)
          )
          (and
            (LeftOf11 ?x)
            (RightOf18 ?x)
          )
          (and
            (LeftOf9 ?x)
            (RightOf19 ?x)
          )
          (and
            (RightOf17 ?x)
            (LeftOf4 ?x)
          )
          (and
            (RightOf16 ?x)
            (LeftOf9 ?x)
          )
          (and
            (LeftOf1 ?x)
            (RightOf18 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf16 ?x)
          )
          (and
            (LeftOf4 ?x)
            (RightOf18 ?x)
          )
          (and
            (LeftOf13 ?x)
            (RightOf18 ?x)
          )
          (and
            (LeftOf8 ?x)
            (RightOf17 ?x)
          )
          (and
            (LeftOf7 ?x)
            (RightOf19 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf13 ?x)
          )
          (and
            (LeftOf1 ?x)
            (RightOf13 ?x)
          )
          (and
            (LeftOf1 ?x)
            (RightOf19 ?x)
          )
          (and
            (RightOf15 ?x)
            (LeftOf14 ?x)
          )
          (and
            (LeftOf9 ?x)
            (RightOf18 ?x)
          )
          (and
            (RightOf17 ?x)
            (LeftOf1 ?x)
          )
          (and
            (LeftOf14 ?x)
            (RightOf18 ?x)
          )
          (and
            (RightOf16 ?x)
            (LeftOf14 ?x)
          )
          (and
            (LeftOf12 ?x)
            (RightOf17 ?x)
          )
          (and
            (RightOf14 ?x)
            (LeftOf7 ?x)
          )
          (and
            (LeftOf7 ?x)
            (RightOf17 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf14 ?x)
          )
          (and
            (LeftOf8 ?x)
            (RightOf14 ?x)
          )
          (and
            (RightOf16 ?x)
            (LeftOf4 ?x)
          )
          (and
            (LeftOf12 ?x)
            (RightOf16 ?x)
          )
          (and
            (LeftOf8 ?x)
            (RightOf18 ?x)
          )
          (and
            (LeftOf10 ?x)
            (RightOf13 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf18 ?x)
          )
          (and
            (RightOf15 ?x)
            (LeftOf9 ?x)
          )
          (and
            (RightOf14 ?x)
            (LeftOf3 ?x)
          )
          (and
            (LeftOf10 ?x)
            (RightOf14 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf19 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf15 ?x)
          )
          (and
            (RightOf14 ?x)
            (LeftOf11 ?x)
          )
          (and
            (RightOf15 ?x)
            (LeftOf7 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf17 ?x)
          )
          (and
            (RightOf19 ?x)
            (LeftOf3 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf18 ?x)
          )
          (and
            (RightOf14 ?x)
            (LeftOf1 ?x)
          )
          (and
            (LeftOf4 ?x)
            (RightOf13 ?x)
          )
          (and
            (RightOf17 ?x)
            (LeftOf14 ?x)
          )
          (and
            (LeftOf10 ?x)
            (RightOf17 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf19 ?x)
          )
          (and
            (LeftOf10 ?x)
            (RightOf18 ?x)
          )
          (and
            (RightOf15 ?x)
            (LeftOf4 ?x)
          )
          (and
            (RightOf18 ?x)
            (LeftOf3 ?x)
          )
          (and
            (LeftOf12 ?x)
            (RightOf19 ?x)
          )
          (and
            (RightOf15 ?x)
            (LeftOf8 ?x)
          )
          (and
            (LeftOf11 ?x)
            (RightOf13 ?x)
          )
          (and
            (LeftOf9 ?x)
            (RightOf13 ?x)
          )
          (and
            (RightOf14 ?x)
            (LeftOf8 ?x)
          )
          (and
            (LeftOf14 ?x)
            (RightOf13 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf17 ?x)
          )
          (and
            (RightOf14 ?x)
            (LeftOf10 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf16 ?x)
          )
          (and
            (LeftOf8 ?x)
            (RightOf16 ?x)
          )
          (and
            (RightOf17 ?x)
            (LeftOf2 ?x)
          )
          (and
            (RightOf14 ?x)
            (LeftOf4 ?x)
          )
          (and
            (LeftOf7 ?x)
            (RightOf18 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf13 ?x)
          )
          (and
            (LeftOf8 ?x)
            (RightOf13 ?x)
          )
          (and
            (RightOf15 ?x)
            (LeftOf13 ?x)
          )
          (and
            (RightOf13 ?x)
            (LeftOf3 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf13 ?x)
          )
          (and
            (LeftOf13 ?x)
            (RightOf19 ?x)
          )
          (and
            (LeftOf12 ?x)
            (RightOf18 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf14 ?x)
          )
          (and
            (LeftOf13 ?x)
            (RightOf13 ?x)
          )
          (and
            (RightOf17 ?x)
            (LeftOf3 ?x)
          )
          (and
            (RightOf15 ?x)
            (LeftOf3 ?x)
          )
          (and
            (RightOf18 ?x)
            (LeftOf4 ?x)
          )
          (and
            (LeftOf12 ?x)
            (RightOf14 ?x)
          )
          (and
            (LeftOf7 ?x)
            (RightOf13 ?x)
          )
          (and
            (LeftOf10 ?x)
            (RightOf19 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf15 ?x)
          )
          (and
            (RightOf16 ?x)
            (LeftOf11 ?x)
          )
          (and
            (LeftOf13 ?x)
            (RightOf17 ?x)
          )
          (and
            (RightOf15 ?x)
            (LeftOf11 ?x)
          )
          (and
            (RightOf14 ?x)
            (LeftOf14 ?x)
          )
          (and
            (RightOf15 ?x)
            (LeftOf1 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf18 ?x)
          )
        )
        (and
          (Column12 ?x)
        )
      )
      (when
        (or
          (and
            (LeftOf15 ?x)
            (RightOf14 ?x)
          )
          (and
            (RightOf14 ?x)
            (LeftOf9 ?x)
          )
          (and
            (LeftOf13 ?x)
            (RightOf16 ?x)
          )
          (and
            (RightOf17 ?x)
            (LeftOf11 ?x)
          )
          (and
            (RightOf15 ?x)
            (LeftOf10 ?x)
          )
          (and
            (RightOf15 ?x)
            (LeftOf2 ?x)
          )
          (and
            (RightOf19 ?x)
            (LeftOf4 ?x)
          )
          (and
            (LeftOf11 ?x)
            (RightOf19 ?x)
          )
          (and
            (LeftOf8 ?x)
            (RightOf19 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf19 ?x)
          )
          (and
            (LeftOf15 ?x)
            (RightOf18 ?x)
          )
          (and
            (RightOf17 ?x)
            (LeftOf9 ?x)
          )
          (and
            (RightOf14 ?x)
            (LeftOf2 ?x)
          )
          (and
            (LeftOf10 ?x)
            (RightOf16 ?x)
          )
          (and
            (RightOf14 ?x)
            (LeftOf7 ?x)
          )
          (and
            (LeftOf7 ?x)
            (RightOf16 ?x)
          )
          (and
            (RightOf16 ?x)
            (LeftOf3 ?x)
          )
          (and
            (RightOf19 ?x)
            (LeftOf14 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf16 ?x)
          )
          (and
            (LeftOf11 ?x)
            (RightOf18 ?x)
          )
          (and
            (LeftOf9 ?x)
            (RightOf19 ?x)
          )
          (and
            (RightOf17 ?x)
            (LeftOf4 ?x)
          )
          (and
            (RightOf16 ?x)
            (LeftOf9 ?x)
          )
          (and
            (LeftOf1 ?x)
            (RightOf18 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf16 ?x)
          )
          (and
            (LeftOf15 ?x)
            (RightOf19 ?x)
          )
          (and
            (LeftOf4 ?x)
            (RightOf18 ?x)
          )
          (and
            (LeftOf13 ?x)
            (RightOf18 ?x)
          )
          (and
            (LeftOf8 ?x)
            (RightOf17 ?x)
          )
          (and
            (LeftOf7 ?x)
            (RightOf19 ?x)
          )
          (and
            (LeftOf1 ?x)
            (RightOf19 ?x)
          )
          (and
            (RightOf15 ?x)
            (LeftOf14 ?x)
          )
          (and
            (LeftOf9 ?x)
            (RightOf18 ?x)
          )
          (and
            (RightOf17 ?x)
            (LeftOf1 ?x)
          )
          (and
            (LeftOf14 ?x)
            (RightOf18 ?x)
          )
          (and
            (RightOf16 ?x)
            (LeftOf14 ?x)
          )
          (and
            (LeftOf12 ?x)
            (RightOf17 ?x)
          )
          (and
            (RightOf15 ?x)
            (LeftOf4 ?x)
          )
          (and
            (LeftOf7 ?x)
            (RightOf17 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf14 ?x)
          )
          (and
            (RightOf16 ?x)
            (LeftOf4 ?x)
          )
          (and
            (LeftOf12 ?x)
            (RightOf16 ?x)
          )
          (and
            (LeftOf8 ?x)
            (RightOf18 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf18 ?x)
          )
          (and
            (RightOf15 ?x)
            (LeftOf9 ?x)
          )
          (and
            (RightOf14 ?x)
            (LeftOf3 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf19 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf15 ?x)
          )
          (and
            (RightOf14 ?x)
            (LeftOf11 ?x)
          )
          (and
            (RightOf15 ?x)
            (LeftOf7 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf17 ?x)
          )
          (and
            (RightOf19 ?x)
            (LeftOf3 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf18 ?x)
          )
          (and
            (RightOf14 ?x)
            (LeftOf1 ?x)
          )
          (and
            (RightOf17 ?x)
            (LeftOf14 ?x)
          )
          (and
            (LeftOf10 ?x)
            (RightOf17 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf19 ?x)
          )
          (and
            (RightOf14 ?x)
            (LeftOf13 ?x)
          )
          (and
            (RightOf16 ?x)
            (LeftOf1 ?x)
          )
          (and
            (RightOf18 ?x)
            (LeftOf3 ?x)
          )
          (and
            (LeftOf12 ?x)
            (RightOf19 ?x)
          )
          (and
            (RightOf15 ?x)
            (LeftOf8 ?x)
          )
          (and
            (RightOf14 ?x)
            (LeftOf8 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf17 ?x)
          )
          (and
            (RightOf14 ?x)
            (LeftOf10 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf16 ?x)
          )
          (and
            (RightOf15 ?x)
            (LeftOf3 ?x)
          )
          (and
            (RightOf17 ?x)
            (LeftOf2 ?x)
          )
          (and
            (RightOf14 ?x)
            (LeftOf4 ?x)
          )
          (and
            (LeftOf7 ?x)
            (RightOf18 ?x)
          )
          (and
            (LeftOf12 ?x)
            (RightOf15 ?x)
          )
          (and
            (RightOf15 ?x)
            (LeftOf13 ?x)
          )
          (and
            (RightOf18 ?x)
            (LeftOf14 ?x)
          )
          (and
            (LeftOf13 ?x)
            (RightOf19 ?x)
          )
          (and
            (LeftOf12 ?x)
            (RightOf18 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf14 ?x)
          )
          (and
            (RightOf17 ?x)
            (LeftOf3 ?x)
          )
          (and
            (LeftOf15 ?x)
            (RightOf15 ?x)
          )
          (and
            (LeftOf10 ?x)
            (RightOf18 ?x)
          )
          (and
            (LeftOf8 ?x)
            (RightOf16 ?x)
          )
          (and
            (RightOf18 ?x)
            (LeftOf4 ?x)
          )
          (and
            (LeftOf15 ?x)
            (RightOf17 ?x)
          )
          (and
            (LeftOf12 ?x)
            (RightOf14 ?x)
          )
          (and
            (LeftOf10 ?x)
            (RightOf19 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf15 ?x)
          )
          (and
            (RightOf16 ?x)
            (LeftOf11 ?x)
          )
          (and
            (LeftOf13 ?x)
            (RightOf17 ?x)
          )
          (and
            (LeftOf15 ?x)
            (RightOf16 ?x)
          )
          (and
            (RightOf15 ?x)
            (LeftOf11 ?x)
          )
          (and
            (RightOf14 ?x)
            (LeftOf14 ?x)
          )
          (and
            (RightOf15 ?x)
            (LeftOf1 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf18 ?x)
          )
        )
        (and
          (Column13 ?x)
        )
      )
      (when
        (or
          (and
            (LeftOf13 ?x)
            (RightOf18 ?x)
          )
          (and
            (RightOf15 ?x)
            (LeftOf16 ?x)
          )
          (and
            (RightOf19 ?x)
            (LeftOf3 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf18 ?x)
          )
          (and
            (LeftOf12 ?x)
            (RightOf18 ?x)
          )
          (and
            (LeftOf8 ?x)
            (RightOf17 ?x)
          )
          (and
            (RightOf15 ?x)
            (LeftOf8 ?x)
          )
          (and
            (LeftOf13 ?x)
            (RightOf16 ?x)
          )
          (and
            (RightOf16 ?x)
            (LeftOf9 ?x)
          )
          (and
            (RightOf17 ?x)
            (LeftOf11 ?x)
          )
          (and
            (LeftOf13 ?x)
            (RightOf17 ?x)
          )
          (and
            (RightOf15 ?x)
            (LeftOf10 ?x)
          )
          (and
            (RightOf17 ?x)
            (LeftOf14 ?x)
          )
          (and
            (LeftOf1 ?x)
            (RightOf19 ?x)
          )
          (and
            (RightOf15 ?x)
            (LeftOf14 ?x)
          )
          (and
            (LeftOf10 ?x)
            (RightOf17 ?x)
          )
          (and
            (LeftOf9 ?x)
            (RightOf18 ?x)
          )
          (and
            (LeftOf10 ?x)
            (RightOf16 ?x)
          )
          (and
            (LeftOf16 ?x)
            (RightOf16 ?x)
          )
          (and
            (RightOf19 ?x)
            (LeftOf4 ?x)
          )
          (and
            (LeftOf11 ?x)
            (RightOf19 ?x)
          )
          (and
            (LeftOf8 ?x)
            (RightOf19 ?x)
          )
          (and
            (RightOf15 ?x)
            (LeftOf4 ?x)
          )
          (and
            (RightOf18 ?x)
            (LeftOf3 ?x)
          )
          (and
            (LeftOf15 ?x)
            (RightOf18 ?x)
          )
          (and
            (RightOf17 ?x)
            (LeftOf9 ?x)
          )
          (and
            (RightOf17 ?x)
            (LeftOf1 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf19 ?x)
          )
          (and
            (LeftOf8 ?x)
            (RightOf16 ?x)
          )
          (and
            (LeftOf14 ?x)
            (RightOf18 ?x)
          )
          (and
            (RightOf16 ?x)
            (LeftOf1 ?x)
          )
          (and
            (RightOf16 ?x)
            (LeftOf14 ?x)
          )
          (and
            (LeftOf10 ?x)
            (RightOf18 ?x)
          )
          (and
            (LeftOf7 ?x)
            (RightOf19 ?x)
          )
          (and
            (LeftOf7 ?x)
            (RightOf16 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf16 ?x)
          )
          (and
            (LeftOf9 ?x)
            (RightOf19 ?x)
          )
          (and
            (LeftOf8 ?x)
            (RightOf18 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf19 ?x)
          )
          (and
            (RightOf16 ?x)
            (LeftOf3 ?x)
          )
          (and
            (LeftOf15 ?x)
            (RightOf17 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf17 ?x)
          )
          (and
            (RightOf19 ?x)
            (LeftOf14 ?x)
          )
          (and
            (LeftOf12 ?x)
            (RightOf19 ?x)
          )
          (and
            (LeftOf12 ?x)
            (RightOf17 ?x)
          )
          (and
            (RightOf15 ?x)
            (LeftOf3 ?x)
          )
          (and
            (RightOf17 ?x)
            (LeftOf2 ?x)
          )
          (and
            (RightOf15 ?x)
            (LeftOf1 ?x)
          )
          (and
            (RightOf15 ?x)
            (LeftOf7 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf19 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf16 ?x)
          )
          (and
            (LeftOf13 ?x)
            (RightOf19 ?x)
          )
          (and
            (LeftOf10 ?x)
            (RightOf19 ?x)
          )
          (and
            (LeftOf7 ?x)
            (RightOf17 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf15 ?x)
          )
          (and
            (RightOf16 ?x)
            (LeftOf4 ?x)
          )
          (and
            (RightOf17 ?x)
            (LeftOf3 ?x)
          )
          (and
            (LeftOf11 ?x)
            (RightOf18 ?x)
          )
          (and
            (LeftOf12 ?x)
            (RightOf16 ?x)
          )
          (and
            (RightOf18 ?x)
            (LeftOf4 ?x)
          )
          (and
            (RightOf16 ?x)
            (LeftOf11 ?x)
          )
          (and
            (LeftOf12 ?x)
            (RightOf15 ?x)
          )
          (and
            (RightOf15 ?x)
            (LeftOf13 ?x)
          )
          (and
            (RightOf15 ?x)
            (LeftOf15 ?x)
          )
          (and
            (RightOf17 ?x)
            (LeftOf4 ?x)
          )
          (and
            (RightOf18 ?x)
            (LeftOf14 ?x)
          )
          (and
            (LeftOf16 ?x)
            (RightOf19 ?x)
          )
          (and
            (LeftOf7 ?x)
            (RightOf18 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf18 ?x)
          )
          (and
            (LeftOf16 ?x)
            (RightOf18 ?x)
          )
          (and
            (RightOf15 ?x)
            (LeftOf9 ?x)
          )
          (and
            (RightOf15 ?x)
            (LeftOf11 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf15 ?x)
          )
          (and
            (RightOf17 ?x)
            (LeftOf16 ?x)
          )
          (and
            (LeftOf1 ?x)
            (RightOf18 ?x)
          )
          (and
            (LeftOf15 ?x)
            (RightOf16 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf18 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf16 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf17 ?x)
          )
          (and
            (LeftOf15 ?x)
            (RightOf19 ?x)
          )
          (and
            (LeftOf4 ?x)
            (RightOf18 ?x)
          )
          (and
            (RightOf15 ?x)
            (LeftOf2 ?x)
          )
        )
        (and
          (Column14 ?x)
        )
      )
      (when
        (or
          (and
            (LeftOf13 ?x)
            (RightOf18 ?x)
          )
          (and
            (LeftOf12 ?x)
            (RightOf18 ?x)
          )
          (and
            (RightOf19 ?x)
            (LeftOf3 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf18 ?x)
          )
          (and
            (LeftOf8 ?x)
            (RightOf17 ?x)
          )
          (and
            (LeftOf7 ?x)
            (RightOf19 ?x)
          )
          (and
            (RightOf19 ?x)
            (LeftOf17 ?x)
          )
          (and
            (LeftOf13 ?x)
            (RightOf16 ?x)
          )
          (and
            (RightOf16 ?x)
            (LeftOf9 ?x)
          )
          (and
            (RightOf17 ?x)
            (LeftOf11 ?x)
          )
          (and
            (RightOf17 ?x)
            (LeftOf14 ?x)
          )
          (and
            (LeftOf1 ?x)
            (RightOf19 ?x)
          )
          (and
            (LeftOf10 ?x)
            (RightOf17 ?x)
          )
          (and
            (LeftOf9 ?x)
            (RightOf18 ?x)
          )
          (and
            (LeftOf10 ?x)
            (RightOf16 ?x)
          )
          (and
            (LeftOf16 ?x)
            (RightOf16 ?x)
          )
          (and
            (RightOf19 ?x)
            (LeftOf4 ?x)
          )
          (and
            (LeftOf11 ?x)
            (RightOf19 ?x)
          )
          (and
            (LeftOf8 ?x)
            (RightOf19 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf19 ?x)
          )
          (and
            (RightOf18 ?x)
            (LeftOf3 ?x)
          )
          (and
            (LeftOf15 ?x)
            (RightOf18 ?x)
          )
          (and
            (RightOf17 ?x)
            (LeftOf9 ?x)
          )
          (and
            (RightOf17 ?x)
            (LeftOf1 ?x)
          )
          (and
            (RightOf18 ?x)
            (LeftOf17 ?x)
          )
          (and
            (LeftOf15 ?x)
            (RightOf16 ?x)
          )
          (and
            (LeftOf14 ?x)
            (RightOf18 ?x)
          )
          (and
            (RightOf16 ?x)
            (LeftOf1 ?x)
          )
          (and
            (RightOf16 ?x)
            (LeftOf14 ?x)
          )
          (and
            (LeftOf10 ?x)
            (RightOf18 ?x)
          )
          (and
            (RightOf16 ?x)
            (LeftOf17 ?x)
          )
          (and
            (LeftOf7 ?x)
            (RightOf16 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf16 ?x)
          )
          (and
            (LeftOf9 ?x)
            (RightOf19 ?x)
          )
          (and
            (LeftOf8 ?x)
            (RightOf18 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf19 ?x)
          )
          (and
            (RightOf16 ?x)
            (LeftOf3 ?x)
          )
          (and
            (LeftOf15 ?x)
            (RightOf17 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf17 ?x)
          )
          (and
            (RightOf19 ?x)
            (LeftOf14 ?x)
          )
          (and
            (LeftOf12 ?x)
            (RightOf17 ?x)
          )
          (and
            (LeftOf8 ?x)
            (RightOf16 ?x)
          )
          (and
            (RightOf17 ?x)
            (LeftOf2 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf16 ?x)
          )
          (and
            (LeftOf13 ?x)
            (RightOf19 ?x)
          )
          (and
            (LeftOf10 ?x)
            (RightOf19 ?x)
          )
          (and
            (LeftOf7 ?x)
            (RightOf17 ?x)
          )
          (and
            (RightOf17 ?x)
            (LeftOf17 ?x)
          )
          (and
            (RightOf16 ?x)
            (LeftOf4 ?x)
          )
          (and
            (RightOf17 ?x)
            (LeftOf3 ?x)
          )
          (and
            (LeftOf11 ?x)
            (RightOf18 ?x)
          )
          (and
            (LeftOf12 ?x)
            (RightOf16 ?x)
          )
          (and
            (RightOf18 ?x)
            (LeftOf4 ?x)
          )
          (and
            (RightOf16 ?x)
            (LeftOf11 ?x)
          )
          (and
            (LeftOf12 ?x)
            (RightOf19 ?x)
          )
          (and
            (LeftOf13 ?x)
            (RightOf17 ?x)
          )
          (and
            (RightOf17 ?x)
            (LeftOf4 ?x)
          )
          (and
            (RightOf18 ?x)
            (LeftOf14 ?x)
          )
          (and
            (LeftOf16 ?x)
            (RightOf19 ?x)
          )
          (and
            (LeftOf7 ?x)
            (RightOf18 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf18 ?x)
          )
          (and
            (LeftOf16 ?x)
            (RightOf18 ?x)
          )
          (and
            (LeftOf15 ?x)
            (RightOf19 ?x)
          )
          (and
            (RightOf17 ?x)
            (LeftOf16 ?x)
          )
          (and
            (LeftOf1 ?x)
            (RightOf18 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf19 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf16 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf17 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf18 ?x)
          )
          (and
            (LeftOf4 ?x)
            (RightOf18 ?x)
          )
        )
        (and
          (Column15 ?x)
        )
      )
      (when
        (or
          (and
            (LeftOf13 ?x)
            (RightOf18 ?x)
          )
          (and
            (LeftOf12 ?x)
            (RightOf18 ?x)
          )
          (and
            (RightOf19 ?x)
            (LeftOf3 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf18 ?x)
          )
          (and
            (LeftOf8 ?x)
            (RightOf17 ?x)
          )
          (and
            (LeftOf7 ?x)
            (RightOf19 ?x)
          )
          (and
            (RightOf19 ?x)
            (LeftOf17 ?x)
          )
          (and
            (LeftOf18 ?x)
            (RightOf19 ?x)
          )
          (and
            (LeftOf18 ?x)
            (RightOf17 ?x)
          )
          (and
            (RightOf17 ?x)
            (LeftOf11 ?x)
          )
          (and
            (RightOf17 ?x)
            (LeftOf14 ?x)
          )
          (and
            (LeftOf1 ?x)
            (RightOf19 ?x)
          )
          (and
            (LeftOf10 ?x)
            (RightOf17 ?x)
          )
          (and
            (LeftOf9 ?x)
            (RightOf18 ?x)
          )
          (and
            (RightOf17 ?x)
            (LeftOf3 ?x)
          )
          (and
            (RightOf19 ?x)
            (LeftOf4 ?x)
          )
          (and
            (LeftOf11 ?x)
            (RightOf19 ?x)
          )
          (and
            (LeftOf8 ?x)
            (RightOf19 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf19 ?x)
          )
          (and
            (RightOf18 ?x)
            (LeftOf3 ?x)
          )
          (and
            (LeftOf15 ?x)
            (RightOf18 ?x)
          )
          (and
            (RightOf17 ?x)
            (LeftOf9 ?x)
          )
          (and
            (RightOf17 ?x)
            (LeftOf1 ?x)
          )
          (and
            (RightOf18 ?x)
            (LeftOf17 ?x)
          )
          (and
            (LeftOf14 ?x)
            (RightOf18 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf19 ?x)
          )
          (and
            (LeftOf10 ?x)
            (RightOf18 ?x)
          )
          (and
            (RightOf18 ?x)
            (LeftOf4 ?x)
          )
          (and
            (LeftOf15 ?x)
            (RightOf17 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf17 ?x)
          )
          (and
            (RightOf19 ?x)
            (LeftOf14 ?x)
          )
          (and
            (LeftOf12 ?x)
            (RightOf17 ?x)
          )
          (and
            (RightOf17 ?x)
            (LeftOf2 ?x)
          )
          (and
            (LeftOf13 ?x)
            (RightOf19 ?x)
          )
          (and
            (LeftOf10 ?x)
            (RightOf19 ?x)
          )
          (and
            (LeftOf7 ?x)
            (RightOf17 ?x)
          )
          (and
            (RightOf17 ?x)
            (LeftOf17 ?x)
          )
          (and
            (LeftOf8 ?x)
            (RightOf18 ?x)
          )
          (and
            (LeftOf18 ?x)
            (RightOf18 ?x)
          )
          (and
            (LeftOf11 ?x)
            (RightOf18 ?x)
          )
          (and
            (LeftOf7 ?x)
            (RightOf18 ?x)
          )
          (and
            (LeftOf9 ?x)
            (RightOf19 ?x)
          )
          (and
            (LeftOf12 ?x)
            (RightOf19 ?x)
          )
          (and
            (LeftOf13 ?x)
            (RightOf17 ?x)
          )
          (and
            (RightOf17 ?x)
            (LeftOf4 ?x)
          )
          (and
            (RightOf18 ?x)
            (LeftOf14 ?x)
          )
          (and
            (LeftOf16 ?x)
            (RightOf19 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf18 ?x)
          )
          (and
            (LeftOf16 ?x)
            (RightOf18 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf18 ?x)
          )
          (and
            (RightOf17 ?x)
            (LeftOf16 ?x)
          )
          (and
            (LeftOf1 ?x)
            (RightOf18 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf19 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf17 ?x)
          )
          (and
            (LeftOf15 ?x)
            (RightOf19 ?x)
          )
          (and
            (LeftOf4 ?x)
            (RightOf18 ?x)
          )
        )
        (and
          (Column16 ?x)
        )
      )
      (when
        (or
          (and
            (LeftOf13 ?x)
            (RightOf18 ?x)
          )
          (and
            (LeftOf12 ?x)
            (RightOf18 ?x)
          )
          (and
            (RightOf19 ?x)
            (LeftOf3 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf18 ?x)
          )
          (and
            (LeftOf7 ?x)
            (RightOf19 ?x)
          )
          (and
            (RightOf19 ?x)
            (LeftOf17 ?x)
          )
          (and
            (LeftOf18 ?x)
            (RightOf19 ?x)
          )
          (and
            (LeftOf1 ?x)
            (RightOf19 ?x)
          )
          (and
            (LeftOf9 ?x)
            (RightOf18 ?x)
          )
          (and
            (RightOf19 ?x)
            (LeftOf4 ?x)
          )
          (and
            (LeftOf11 ?x)
            (RightOf19 ?x)
          )
          (and
            (LeftOf8 ?x)
            (RightOf19 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf19 ?x)
          )
          (and
            (RightOf18 ?x)
            (LeftOf3 ?x)
          )
          (and
            (LeftOf12 ?x)
            (RightOf19 ?x)
          )
          (and
            (RightOf18 ?x)
            (LeftOf17 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf19 ?x)
          )
          (and
            (LeftOf10 ?x)
            (RightOf18 ?x)
          )
          (and
            (LeftOf19 ?x)
            (RightOf18 ?x)
          )
          (and
            (RightOf18 ?x)
            (LeftOf4 ?x)
          )
          (and
            (RightOf19 ?x)
            (LeftOf14 ?x)
          )
          (and
            (LeftOf10 ?x)
            (RightOf19 ?x)
          )
          (and
            (LeftOf19 ?x)
            (RightOf19 ?x)
          )
          (and
            (LeftOf8 ?x)
            (RightOf18 ?x)
          )
          (and
            (LeftOf18 ?x)
            (RightOf18 ?x)
          )
          (and
            (LeftOf11 ?x)
            (RightOf18 ?x)
          )
          (and
            (LeftOf7 ?x)
            (RightOf18 ?x)
          )
          (and
            (LeftOf9 ?x)
            (RightOf19 ?x)
          )
          (and
            (LeftOf15 ?x)
            (RightOf18 ?x)
          )
          (and
            (RightOf18 ?x)
            (LeftOf14 ?x)
          )
          (and
            (LeftOf16 ?x)
            (RightOf19 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf18 ?x)
          )
          (and
            (LeftOf16 ?x)
            (RightOf18 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf18 ?x)
          )
          (and
            (LeftOf13 ?x)
            (RightOf19 ?x)
          )
          (and
            (LeftOf1 ?x)
            (RightOf18 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf19 ?x)
          )
          (and
            (LeftOf15 ?x)
            (RightOf19 ?x)
          )
        )
        (and
          (Column17 ?x)
        )
      )
      (when
        (or
          (and
            (LeftOf13 ?x)
            (RightOf19 ?x)
          )
          (and
            (LeftOf8 ?x)
            (RightOf19 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf19 ?x)
          )
          (and
            (RightOf19 ?x)
            (LeftOf3 ?x)
          )
          (and
            (RightOf19 ?x)
            (LeftOf14 ?x)
          )
          (and
            (LeftOf16 ?x)
            (RightOf19 ?x)
          )
          (and
            (LeftOf7 ?x)
            (RightOf19 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf19 ?x)
          )
          (and
            (LeftOf18 ?x)
            (RightOf19 ?x)
          )
          (and
            (LeftOf19 ?x)
            (RightOf19 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf19 ?x)
          )
          (and
            (LeftOf10 ?x)
            (RightOf19 ?x)
          )
          (and
            (LeftOf12 ?x)
            (RightOf19 ?x)
          )
          (and
            (LeftOf1 ?x)
            (RightOf19 ?x)
          )
          (and
            (RightOf19 ?x)
            (LeftOf17 ?x)
          )
          (and
            (RightOf19 ?x)
            (LeftOf4 ?x)
          )
          (and
            (LeftOf11 ?x)
            (RightOf19 ?x)
          )
          (and
            (LeftOf15 ?x)
            (RightOf19 ?x)
          )
          (and
            (LeftOf9 ?x)
            (RightOf19 ?x)
          )
          (and
            (LeftOf20 ?x)
            (RightOf19 ?x)
          )
        )
        (and
          (Column18 ?x)
        )
      )
    )
  )
  (:action moveUp
    :parameters ( ?x )
    :precondition ( and
      (not (CheckConsistency))
      (not (Error))
      (or
        (BelowOf7 ?x)
        (AboveOf9 ?x)
        (BelowOf18 ?x)
        (BelowOf1 ?x)
        (BelowOf17 ?x)
        (AboveOf7 ?x)
        (BelowOf16 ?x)
        (AboveOf1 ?x)
        (AboveOf11 ?x)
        (AboveOf19 ?x)
        (BelowOf14 ?x)
        (BelowOf8 ?x)
        (BelowOf3 ?x)
        (AboveOf14 ?x)
        (AboveOf3 ?x)
        (BelowOf9 ?x)
        (AboveOf16 ?x)
        (BelowOf20 ?x)
        (BelowOf2 ?x)
        (BelowOf5 ?x)
        (BelowOf10 ?x)
        (BelowOf11 ?x)
        (BelowOf19 ?x)
        (AboveOf18 ?x)
        (BelowOf13 ?x)
        (AboveOf17 ?x)
        (BelowOf4 ?x)
        (AboveOf10 ?x)
        (AboveOf0 ?x)
        (AboveOf8 ?x)
        (AboveOf6 ?x)
        (AboveOf5 ?x)
        (AboveOf13 ?x)
        (Rows ?x)
        (AboveOf4 ?x)
        (BelowOf12 ?x)
        (AboveOf15 ?x)
        (AboveOf12 ?x)
        (BelowOf6 ?x)
        (BelowOf15 ?x)
        (AboveOf2 ?x)
      )
    )
    :effect ( and
      (CheckConsistency)
      (when
        (or
          (AboveOf18 ?x)
          (AboveOf1 ?x)
          (AboveOf5 ?x)
          (AboveOf14 ?x)
          (AboveOf13 ?x)
          (AboveOf3 ?x)
          (AboveOf4 ?x)
          (AboveOf16 ?x)
          (AboveOf9 ?x)
          (AboveOf8 ?x)
          (AboveOf15 ?x)
          (AboveOf17 ?x)
          (AboveOf12 ?x)
          (AboveOf7 ?x)
          (AboveOf0 ?x)
          (AboveOf2 ?x)
          (AboveOf10 ?x)
          (AboveOf11 ?x)
          (AboveOf6 ?x)
          (AboveOf19 ?x)
        )
        (and
          (AboveOf1 ?x)
        )
      )
      (when
        (or
          (AboveOf18 ?x)
          (AboveOf1 ?x)
          (AboveOf5 ?x)
          (AboveOf14 ?x)
          (AboveOf13 ?x)
          (AboveOf3 ?x)
          (AboveOf4 ?x)
          (AboveOf16 ?x)
          (AboveOf9 ?x)
          (AboveOf8 ?x)
          (AboveOf15 ?x)
          (AboveOf17 ?x)
          (AboveOf12 ?x)
          (AboveOf7 ?x)
          (AboveOf2 ?x)
          (AboveOf10 ?x)
          (AboveOf11 ?x)
          (AboveOf6 ?x)
          (AboveOf19 ?x)
        )
        (and
          (AboveOf2 ?x)
        )
      )
      (when
        (or
          (AboveOf18 ?x)
          (AboveOf5 ?x)
          (AboveOf14 ?x)
          (AboveOf13 ?x)
          (AboveOf3 ?x)
          (AboveOf4 ?x)
          (AboveOf16 ?x)
          (AboveOf9 ?x)
          (AboveOf8 ?x)
          (AboveOf15 ?x)
          (AboveOf17 ?x)
          (AboveOf12 ?x)
          (AboveOf7 ?x)
          (AboveOf2 ?x)
          (AboveOf10 ?x)
          (AboveOf6 ?x)
          (AboveOf11 ?x)
          (AboveOf19 ?x)
        )
        (and
          (AboveOf3 ?x)
        )
      )
      (when
        (or
          (AboveOf18 ?x)
          (AboveOf5 ?x)
          (AboveOf14 ?x)
          (AboveOf13 ?x)
          (AboveOf3 ?x)
          (AboveOf4 ?x)
          (AboveOf16 ?x)
          (AboveOf9 ?x)
          (AboveOf15 ?x)
          (AboveOf17 ?x)
          (AboveOf12 ?x)
          (AboveOf7 ?x)
          (AboveOf8 ?x)
          (AboveOf10 ?x)
          (AboveOf6 ?x)
          (AboveOf11 ?x)
          (AboveOf19 ?x)
        )
        (and
          (AboveOf4 ?x)
        )
      )
      (when
        (or
          (AboveOf18 ?x)
          (AboveOf5 ?x)
          (AboveOf14 ?x)
          (AboveOf13 ?x)
          (AboveOf4 ?x)
          (AboveOf16 ?x)
          (AboveOf9 ?x)
          (AboveOf15 ?x)
          (AboveOf17 ?x)
          (AboveOf12 ?x)
          (AboveOf7 ?x)
          (AboveOf8 ?x)
          (AboveOf10 ?x)
          (AboveOf6 ?x)
          (AboveOf11 ?x)
          (AboveOf19 ?x)
        )
        (and
          (AboveOf5 ?x)
        )
      )
      (when
        (or
          (AboveOf18 ?x)
          (AboveOf5 ?x)
          (AboveOf14 ?x)
          (AboveOf13 ?x)
          (AboveOf16 ?x)
          (AboveOf9 ?x)
          (AboveOf15 ?x)
          (AboveOf17 ?x)
          (AboveOf12 ?x)
          (AboveOf10 ?x)
          (AboveOf8 ?x)
          (AboveOf7 ?x)
          (AboveOf6 ?x)
          (AboveOf11 ?x)
          (AboveOf19 ?x)
        )
        (and
          (AboveOf6 ?x)
        )
      )
      (when
        (or
          (AboveOf18 ?x)
          (AboveOf14 ?x)
          (AboveOf13 ?x)
          (AboveOf16 ?x)
          (AboveOf9 ?x)
          (AboveOf15 ?x)
          (AboveOf17 ?x)
          (AboveOf12 ?x)
          (AboveOf10 ?x)
          (AboveOf8 ?x)
          (AboveOf7 ?x)
          (AboveOf6 ?x)
          (AboveOf11 ?x)
          (AboveOf19 ?x)
        )
        (and
          (AboveOf7 ?x)
        )
      )
      (when
        (or
          (AboveOf18 ?x)
          (AboveOf14 ?x)
          (AboveOf13 ?x)
          (AboveOf16 ?x)
          (AboveOf9 ?x)
          (AboveOf15 ?x)
          (AboveOf17 ?x)
          (AboveOf12 ?x)
          (AboveOf10 ?x)
          (AboveOf8 ?x)
          (AboveOf7 ?x)
          (AboveOf11 ?x)
          (AboveOf19 ?x)
        )
        (and
          (AboveOf8 ?x)
        )
      )
      (when
        (or
          (AboveOf18 ?x)
          (AboveOf14 ?x)
          (AboveOf13 ?x)
          (AboveOf16 ?x)
          (AboveOf9 ?x)
          (AboveOf15 ?x)
          (AboveOf17 ?x)
          (AboveOf12 ?x)
          (AboveOf10 ?x)
          (AboveOf8 ?x)
          (AboveOf11 ?x)
          (AboveOf19 ?x)
        )
        (and
          (AboveOf9 ?x)
        )
      )
      (when
        (or
          (AboveOf18 ?x)
          (AboveOf14 ?x)
          (AboveOf13 ?x)
          (AboveOf16 ?x)
          (AboveOf9 ?x)
          (AboveOf15 ?x)
          (AboveOf17 ?x)
          (AboveOf12 ?x)
          (AboveOf10 ?x)
          (AboveOf11 ?x)
          (AboveOf19 ?x)
        )
        (and
          (AboveOf10 ?x)
        )
      )
      (when
        (or
          (AboveOf18 ?x)
          (AboveOf14 ?x)
          (AboveOf13 ?x)
          (AboveOf16 ?x)
          (AboveOf15 ?x)
          (AboveOf17 ?x)
          (AboveOf12 ?x)
          (AboveOf10 ?x)
          (AboveOf11 ?x)
          (AboveOf19 ?x)
        )
        (and
          (AboveOf11 ?x)
        )
      )
      (when
        (or
          (AboveOf18 ?x)
          (AboveOf14 ?x)
          (AboveOf13 ?x)
          (AboveOf16 ?x)
          (AboveOf15 ?x)
          (AboveOf17 ?x)
          (AboveOf12 ?x)
          (AboveOf11 ?x)
          (AboveOf19 ?x)
        )
        (and
          (AboveOf12 ?x)
        )
      )
      (when
        (or
          (AboveOf18 ?x)
          (AboveOf14 ?x)
          (AboveOf13 ?x)
          (AboveOf16 ?x)
          (AboveOf15 ?x)
          (AboveOf17 ?x)
          (AboveOf12 ?x)
          (AboveOf19 ?x)
        )
        (and
          (AboveOf13 ?x)
        )
      )
      (when
        (or
          (AboveOf18 ?x)
          (AboveOf14 ?x)
          (AboveOf13 ?x)
          (AboveOf16 ?x)
          (AboveOf15 ?x)
          (AboveOf17 ?x)
          (AboveOf19 ?x)
        )
        (and
          (AboveOf14 ?x)
        )
      )
      (when
        (or
          (AboveOf18 ?x)
          (AboveOf14 ?x)
          (AboveOf16 ?x)
          (AboveOf15 ?x)
          (AboveOf17 ?x)
          (AboveOf19 ?x)
        )
        (and
          (AboveOf15 ?x)
        )
      )
      (when
        (or
          (AboveOf16 ?x)
          (AboveOf15 ?x)
          (AboveOf17 ?x)
          (AboveOf19 ?x)
          (AboveOf18 ?x)
        )
        (and
          (AboveOf16 ?x)
        )
      )
      (when
        (or
          (AboveOf16 ?x)
          (AboveOf17 ?x)
          (AboveOf19 ?x)
          (AboveOf18 ?x)
        )
        (and
          (AboveOf17 ?x)
        )
      )
      (when
        (or
          (AboveOf18 ?x)
          (AboveOf17 ?x)
          (AboveOf19 ?x)
        )
        (and
          (AboveOf18 ?x)
        )
      )
      (when
        (or
          (AboveOf18 ?x)
          (AboveOf19 ?x)
        )
        (and
          (AboveOf19 ?x)
        )
      )
      (when
        (BelowOf1 ?x)
        (and
          (BelowOf2 ?x)
          (not (BelowOf1 ?x))
        )
      )
      (when
        (or
          (BelowOf1 ?x)
          (BelowOf2 ?x)
        )
        (and
          (BelowOf3 ?x)
          (not (BelowOf2 ?x))
        )
      )
      (when
        (or
          (BelowOf3 ?x)
          (BelowOf1 ?x)
          (BelowOf2 ?x)
        )
        (and
          (BelowOf4 ?x)
          (not (BelowOf3 ?x))
        )
      )
      (when
        (or
          (BelowOf4 ?x)
          (BelowOf3 ?x)
          (BelowOf1 ?x)
          (BelowOf2 ?x)
        )
        (and
          (BelowOf5 ?x)
          (not (BelowOf4 ?x))
        )
      )
      (when
        (or
          (BelowOf5 ?x)
          (BelowOf4 ?x)
          (BelowOf3 ?x)
          (BelowOf1 ?x)
          (BelowOf2 ?x)
        )
        (and
          (BelowOf6 ?x)
          (not (BelowOf5 ?x))
        )
      )
      (when
        (or
          (BelowOf3 ?x)
          (BelowOf4 ?x)
          (BelowOf1 ?x)
          (BelowOf2 ?x)
          (BelowOf5 ?x)
          (BelowOf6 ?x)
        )
        (and
          (BelowOf7 ?x)
          (not (BelowOf6 ?x))
        )
      )
      (when
        (or
          (BelowOf3 ?x)
          (BelowOf7 ?x)
          (BelowOf4 ?x)
          (BelowOf1 ?x)
          (BelowOf2 ?x)
          (BelowOf5 ?x)
          (BelowOf6 ?x)
        )
        (and
          (BelowOf8 ?x)
          (not (BelowOf7 ?x))
        )
      )
      (when
        (or
          (BelowOf8 ?x)
          (BelowOf3 ?x)
          (BelowOf7 ?x)
          (BelowOf4 ?x)
          (BelowOf1 ?x)
          (BelowOf2 ?x)
          (BelowOf5 ?x)
          (BelowOf6 ?x)
        )
        (and
          (BelowOf9 ?x)
          (not (BelowOf8 ?x))
        )
      )
      (when
        (or
          (BelowOf8 ?x)
          (BelowOf3 ?x)
          (BelowOf9 ?x)
          (BelowOf7 ?x)
          (BelowOf4 ?x)
          (BelowOf1 ?x)
          (BelowOf2 ?x)
          (BelowOf5 ?x)
          (BelowOf6 ?x)
        )
        (and
          (BelowOf10 ?x)
          (not (BelowOf9 ?x))
        )
      )
      (when
        (or
          (BelowOf8 ?x)
          (BelowOf3 ?x)
          (BelowOf9 ?x)
          (BelowOf7 ?x)
          (BelowOf4 ?x)
          (BelowOf1 ?x)
          (BelowOf2 ?x)
          (BelowOf5 ?x)
          (BelowOf6 ?x)
          (BelowOf10 ?x)
        )
        (and
          (BelowOf11 ?x)
          (not (BelowOf10 ?x))
        )
      )
      (when
        (or
          (BelowOf8 ?x)
          (BelowOf3 ?x)
          (BelowOf9 ?x)
          (BelowOf7 ?x)
          (BelowOf4 ?x)
          (BelowOf1 ?x)
          (BelowOf2 ?x)
          (BelowOf5 ?x)
          (BelowOf6 ?x)
          (BelowOf11 ?x)
          (BelowOf10 ?x)
        )
        (and
          (BelowOf12 ?x)
          (not (BelowOf11 ?x))
        )
      )
      (when
        (or
          (BelowOf8 ?x)
          (BelowOf3 ?x)
          (BelowOf9 ?x)
          (BelowOf7 ?x)
          (BelowOf12 ?x)
          (BelowOf4 ?x)
          (BelowOf1 ?x)
          (BelowOf2 ?x)
          (BelowOf5 ?x)
          (BelowOf6 ?x)
          (BelowOf11 ?x)
          (BelowOf10 ?x)
        )
        (and
          (BelowOf13 ?x)
          (not (BelowOf12 ?x))
        )
      )
      (when
        (or
          (BelowOf8 ?x)
          (BelowOf13 ?x)
          (BelowOf3 ?x)
          (BelowOf9 ?x)
          (BelowOf7 ?x)
          (BelowOf12 ?x)
          (BelowOf4 ?x)
          (BelowOf1 ?x)
          (BelowOf2 ?x)
          (BelowOf5 ?x)
          (BelowOf6 ?x)
          (BelowOf11 ?x)
          (BelowOf10 ?x)
        )
        (and
          (BelowOf14 ?x)
          (not (BelowOf13 ?x))
        )
      )
      (when
        (or
          (BelowOf14 ?x)
          (BelowOf8 ?x)
          (BelowOf13 ?x)
          (BelowOf3 ?x)
          (BelowOf9 ?x)
          (BelowOf7 ?x)
          (BelowOf12 ?x)
          (BelowOf4 ?x)
          (BelowOf1 ?x)
          (BelowOf2 ?x)
          (BelowOf5 ?x)
          (BelowOf6 ?x)
          (BelowOf11 ?x)
          (BelowOf10 ?x)
        )
        (and
          (BelowOf15 ?x)
          (not (BelowOf14 ?x))
        )
      )
      (when
        (or
          (BelowOf14 ?x)
          (BelowOf8 ?x)
          (BelowOf13 ?x)
          (BelowOf3 ?x)
          (BelowOf9 ?x)
          (BelowOf7 ?x)
          (BelowOf12 ?x)
          (BelowOf4 ?x)
          (BelowOf1 ?x)
          (BelowOf2 ?x)
          (BelowOf5 ?x)
          (BelowOf6 ?x)
          (BelowOf15 ?x)
          (BelowOf11 ?x)
          (BelowOf10 ?x)
        )
        (and
          (BelowOf16 ?x)
          (not (BelowOf15 ?x))
        )
      )
      (when
        (or
          (BelowOf14 ?x)
          (BelowOf8 ?x)
          (BelowOf13 ?x)
          (BelowOf3 ?x)
          (BelowOf9 ?x)
          (BelowOf7 ?x)
          (BelowOf12 ?x)
          (BelowOf4 ?x)
          (BelowOf1 ?x)
          (BelowOf2 ?x)
          (BelowOf5 ?x)
          (BelowOf11 ?x)
          (BelowOf6 ?x)
          (BelowOf15 ?x)
          (BelowOf16 ?x)
          (BelowOf10 ?x)
        )
        (and
          (BelowOf17 ?x)
          (not (BelowOf16 ?x))
        )
      )
      (when
        (or
          (BelowOf14 ?x)
          (BelowOf8 ?x)
          (BelowOf13 ?x)
          (BelowOf3 ?x)
          (BelowOf9 ?x)
          (BelowOf7 ?x)
          (BelowOf12 ?x)
          (BelowOf11 ?x)
          (BelowOf4 ?x)
          (BelowOf1 ?x)
          (BelowOf2 ?x)
          (BelowOf5 ?x)
          (BelowOf17 ?x)
          (BelowOf6 ?x)
          (BelowOf15 ?x)
          (BelowOf16 ?x)
          (BelowOf10 ?x)
        )
        (and
          (BelowOf18 ?x)
          (not (BelowOf17 ?x))
        )
      )
      (when
        (or
          (BelowOf14 ?x)
          (BelowOf8 ?x)
          (BelowOf13 ?x)
          (BelowOf3 ?x)
          (BelowOf9 ?x)
          (BelowOf7 ?x)
          (BelowOf12 ?x)
          (BelowOf18 ?x)
          (BelowOf11 ?x)
          (BelowOf4 ?x)
          (BelowOf1 ?x)
          (BelowOf2 ?x)
          (BelowOf5 ?x)
          (BelowOf17 ?x)
          (BelowOf6 ?x)
          (BelowOf15 ?x)
          (BelowOf16 ?x)
          (BelowOf10 ?x)
        )
        (and
          (BelowOf19 ?x)
          (not (BelowOf18 ?x))
        )
      )
      (when
        (or
          (BelowOf14 ?x)
          (BelowOf8 ?x)
          (BelowOf13 ?x)
          (BelowOf10 ?x)
          (BelowOf9 ?x)
          (BelowOf7 ?x)
          (BelowOf12 ?x)
          (BelowOf18 ?x)
          (BelowOf11 ?x)
          (BelowOf4 ?x)
          (BelowOf1 ?x)
          (BelowOf2 ?x)
          (BelowOf5 ?x)
          (BelowOf17 ?x)
          (BelowOf6 ?x)
          (BelowOf15 ?x)
          (BelowOf16 ?x)
          (BelowOf19 ?x)
          (BelowOf3 ?x)
        )
        (and
          (BelowOf20 ?x)
          (not (BelowOf19 ?x))
        )
      )
      (when
        (Row0 ?x)
        (and
          (Row1 ?x)
          (not (Row0 ?x))
        )
      )
      (when
        (Row1 ?x)
        (and
          (Row2 ?x)
          (not (Row1 ?x))
        )
      )
      (when
        (Row2 ?x)
        (and
          (Row3 ?x)
          (not (Row2 ?x))
        )
      )
      (when
        (Row3 ?x)
        (and
          (Row4 ?x)
          (not (Row3 ?x))
        )
      )
      (when
        (Row4 ?x)
        (and
          (Row5 ?x)
          (not (Row4 ?x))
        )
      )
      (when
        (Row5 ?x)
        (and
          (Row6 ?x)
          (not (Row5 ?x))
        )
      )
      (when
        (Row6 ?x)
        (and
          (Row7 ?x)
          (not (Row6 ?x))
        )
      )
      (when
        (Row7 ?x)
        (and
          (Row8 ?x)
          (not (Row7 ?x))
        )
      )
      (when
        (Row8 ?x)
        (and
          (Row9 ?x)
          (not (Row8 ?x))
        )
      )
      (when
        (Row9 ?x)
        (and
          (Row10 ?x)
          (not (Row9 ?x))
        )
      )
      (when
        (Row10 ?x)
        (and
          (Row11 ?x)
          (not (Row10 ?x))
        )
      )
      (when
        (Row11 ?x)
        (and
          (Row12 ?x)
          (not (Row11 ?x))
        )
      )
      (when
        (Row12 ?x)
        (and
          (Row13 ?x)
          (not (Row12 ?x))
        )
      )
      (when
        (Row13 ?x)
        (and
          (Row14 ?x)
          (not (Row13 ?x))
        )
      )
      (when
        (Row14 ?x)
        (and
          (Row15 ?x)
          (not (Row14 ?x))
        )
      )
      (when
        (Row15 ?x)
        (and
          (Row16 ?x)
          (not (Row15 ?x))
        )
      )
      (when
        (Row16 ?x)
        (and
          (Row17 ?x)
          (not (Row16 ?x))
        )
      )
      (when
        (Row17 ?x)
        (and
          (Row18 ?x)
          (not (Row17 ?x))
        )
      )
      (when
        (Row18 ?x)
        (and
          (Row19 ?x)
          (not (Row18 ?x))
        )
      )
      (when
        (or
          (and
            (BelowOf1 ?x)
            (AboveOf17 ?x)
          )
          (and
            (AboveOf9 ?x)
            (BelowOf1 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf3 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf8 ?x)
          )
          (and
            (AboveOf15 ?x)
            (BelowOf1 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf0 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf10 ?x)
          )
          (and
            (AboveOf16 ?x)
            (BelowOf1 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf1 ?x)
          )
          (and
            (AboveOf5 ?x)
            (BelowOf1 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf6 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf11 ?x)
          )
          (and
            (AboveOf7 ?x)
            (BelowOf1 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf1 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf12 ?x)
          )
          (and
            (AboveOf1 ?x)
            (BelowOf1 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf19 ?x)
          )
          (and
            (AboveOf2 ?x)
            (BelowOf1 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf18 ?x)
          )
          (and
            (AboveOf4 ?x)
            (BelowOf1 ?x)
          )
        )
        (and
          (Row1 ?x)
        )
      )
      (when
        (or
          (and
            (BelowOf2 ?x)
            (AboveOf3 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf8 ?x)
          )
          (and
            (AboveOf15 ?x)
            (BelowOf1 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf19 ?x)
          )
          (and
            (AboveOf5 ?x)
            (BelowOf2 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf12 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf12 ?x)
          )
          (and
            (AboveOf4 ?x)
            (BelowOf1 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf8 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf10 ?x)
          )
          (and
            (AboveOf9 ?x)
            (BelowOf1 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf3 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf11 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf10 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf17 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf1 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf6 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf1 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf9 ?x)
          )
          (and
            (AboveOf2 ?x)
            (BelowOf1 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf6 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf2 ?x)
          )
          (and
            (AboveOf17 ?x)
            (BelowOf1 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf4 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf16 ?x)
          )
          (and
            (AboveOf5 ?x)
            (BelowOf1 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf2 ?x)
          )
          (and
            (AboveOf7 ?x)
            (BelowOf1 ?x)
          )
          (and
            (AboveOf16 ?x)
            (BelowOf1 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf2 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf19 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf15 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf7 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf1 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf11 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf18 ?x)
          )
          (and
            (AboveOf1 ?x)
            (BelowOf1 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf18 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf17 ?x)
          )
        )
        (and
          (Row2 ?x)
        )
      )
      (when
        (or
          (and
            (BelowOf2 ?x)
            (AboveOf3 ?x)
          )
          (and
            (AboveOf18 ?x)
            (BelowOf3 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf8 ?x)
          )
          (and
            (AboveOf15 ?x)
            (BelowOf1 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf19 ?x)
          )
          (and
            (AboveOf5 ?x)
            (BelowOf2 ?x)
          )
          (and
            (BelowOf3 ?x)
            (AboveOf11 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf3 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf12 ?x)
          )
          (and
            (BelowOf3 ?x)
            (AboveOf12 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf12 ?x)
          )
          (and
            (AboveOf4 ?x)
            (BelowOf1 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf8 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf10 ?x)
          )
          (and
            (AboveOf9 ?x)
            (BelowOf1 ?x)
          )
          (and
            (BelowOf3 ?x)
            (AboveOf19 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf3 ?x)
          )
          (and
            (AboveOf2 ?x)
            (BelowOf2 ?x)
          )
          (and
            (AboveOf10 ?x)
            (BelowOf3 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf1 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf10 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf17 ?x)
          )
          (and
            (AboveOf4 ?x)
            (BelowOf3 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf6 ?x)
          )
          (and
            (BelowOf3 ?x)
            (AboveOf8 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf1 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf9 ?x)
          )
          (and
            (BelowOf3 ?x)
            (AboveOf3 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf6 ?x)
          )
          (and
            (AboveOf2 ?x)
            (BelowOf3 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf2 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf4 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf16 ?x)
          )
          (and
            (AboveOf15 ?x)
            (BelowOf3 ?x)
          )
          (and
            (AboveOf9 ?x)
            (BelowOf3 ?x)
          )
          (and
            (AboveOf5 ?x)
            (BelowOf1 ?x)
          )
          (and
            (AboveOf16 ?x)
            (BelowOf3 ?x)
          )
          (and
            (AboveOf7 ?x)
            (BelowOf1 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf2 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf19 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf3 ?x)
          )
          (and
            (AboveOf17 ?x)
            (BelowOf3 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf15 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf7 ?x)
          )
          (and
            (AboveOf17 ?x)
            (BelowOf1 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf11 ?x)
          )
          (and
            (AboveOf7 ?x)
            (BelowOf3 ?x)
          )
          (and
            (AboveOf2 ?x)
            (BelowOf1 ?x)
          )
          (and
            (AboveOf6 ?x)
            (BelowOf3 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf11 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf18 ?x)
          )
          (and
            (AboveOf16 ?x)
            (BelowOf1 ?x)
          )
          (and
            (AboveOf5 ?x)
            (BelowOf3 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf18 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf17 ?x)
          )
        )
        (and
          (Row3 ?x)
        )
      )
      (when
        (or
          (and
            (BelowOf2 ?x)
            (AboveOf3 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf8 ?x)
          )
          (and
            (AboveOf18 ?x)
            (BelowOf3 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf8 ?x)
          )
          (and
            (AboveOf15 ?x)
            (BelowOf1 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf19 ?x)
          )
          (and
            (AboveOf5 ?x)
            (BelowOf2 ?x)
          )
          (and
            (BelowOf3 ?x)
            (AboveOf11 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf12 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf2 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf3 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf4 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf11 ?x)
          )
          (and
            (BelowOf3 ?x)
            (AboveOf12 ?x)
          )
          (and
            (AboveOf7 ?x)
            (BelowOf4 ?x)
          )
          (and
            (AboveOf11 ?x)
            (BelowOf3 ?x)
          )
          (and
            (AboveOf4 ?x)
            (BelowOf1 ?x)
          )
          (and
            (AboveOf19 ?x)
            (BelowOf3 ?x)
          )
          (and
            (AboveOf15 ?x)
            (BelowOf4 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf10 ?x)
          )
          (and
            (AboveOf9 ?x)
            (BelowOf1 ?x)
          )
          (and
            (BelowOf3 ?x)
            (AboveOf19 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf3 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf10 ?x)
          )
          (and
            (AboveOf10 ?x)
            (BelowOf3 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf1 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf10 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf12 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf17 ?x)
          )
          (and
            (AboveOf4 ?x)
            (BelowOf3 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf6 ?x)
          )
          (and
            (BelowOf3 ?x)
            (AboveOf8 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf1 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf19 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf9 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf18 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf6 ?x)
          )
          (and
            (AboveOf3 ?x)
            (BelowOf3 ?x)
          )
          (and
            (AboveOf5 ?x)
            (BelowOf4 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf12 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf4 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf16 ?x)
          )
          (and
            (AboveOf15 ?x)
            (BelowOf3 ?x)
          )
          (and
            (AboveOf9 ?x)
            (BelowOf3 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf8 ?x)
          )
          (and
            (AboveOf5 ?x)
            (BelowOf1 ?x)
          )
          (and
            (AboveOf16 ?x)
            (BelowOf3 ?x)
          )
          (and
            (AboveOf7 ?x)
            (BelowOf1 ?x)
          )
          (and
            (AboveOf16 ?x)
            (BelowOf4 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf2 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf19 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf18 ?x)
          )
          (and
            (AboveOf17 ?x)
            (BelowOf3 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf15 ?x)
          )
          (and
            (AboveOf4 ?x)
            (BelowOf4 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf7 ?x)
          )
          (and
            (AboveOf17 ?x)
            (BelowOf1 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf11 ?x)
          )
          (and
            (AboveOf16 ?x)
            (BelowOf1 ?x)
          )
          (and
            (AboveOf7 ?x)
            (BelowOf3 ?x)
          )
          (and
            (AboveOf6 ?x)
            (BelowOf3 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf11 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf6 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf17 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf3 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf4 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf9 ?x)
          )
          (and
            (AboveOf5 ?x)
            (BelowOf3 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf3 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf18 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf17 ?x)
          )
        )
        (and
          (Row4 ?x)
        )
      )
      (when
        (or
          (and
            (BelowOf2 ?x)
            (AboveOf11 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf18 ?x)
          )
          (and
            (AboveOf18 ?x)
            (BelowOf3 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf8 ?x)
          )
          (and
            (AboveOf15 ?x)
            (BelowOf1 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf19 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf6 ?x)
          )
          (and
            (BelowOf3 ?x)
            (AboveOf11 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf11 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf12 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf2 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf3 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf4 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf12 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf11 ?x)
          )
          (and
            (BelowOf3 ?x)
            (AboveOf12 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf18 ?x)
          )
          (and
            (AboveOf11 ?x)
            (BelowOf3 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf9 ?x)
          )
          (and
            (AboveOf4 ?x)
            (BelowOf1 ?x)
          )
          (and
            (AboveOf19 ?x)
            (BelowOf3 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf8 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf10 ?x)
          )
          (and
            (AboveOf9 ?x)
            (BelowOf1 ?x)
          )
          (and
            (BelowOf3 ?x)
            (AboveOf19 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf6 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf10 ?x)
          )
          (and
            (AboveOf10 ?x)
            (BelowOf3 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf1 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf10 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf12 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf7 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf17 ?x)
          )
          (and
            (AboveOf4 ?x)
            (BelowOf3 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf5 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf6 ?x)
          )
          (and
            (BelowOf3 ?x)
            (AboveOf8 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf1 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf19 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf9 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf16 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf6 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf13 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf12 ?x)
          )
          (and
            (AboveOf5 ?x)
            (BelowOf4 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf14 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf4 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf16 ?x)
          )
          (and
            (AboveOf15 ?x)
            (BelowOf3 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf10 ?x)
          )
          (and
            (AboveOf9 ?x)
            (BelowOf3 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf8 ?x)
          )
          (and
            (AboveOf5 ?x)
            (BelowOf1 ?x)
          )
          (and
            (AboveOf16 ?x)
            (BelowOf3 ?x)
          )
          (and
            (AboveOf5 ?x)
            (BelowOf2 ?x)
          )
          (and
            (AboveOf7 ?x)
            (BelowOf1 ?x)
          )
          (and
            (AboveOf16 ?x)
            (BelowOf4 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf2 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf19 ?x)
          )
          (and
            (AboveOf15 ?x)
            (BelowOf4 ?x)
          )
          (and
            (AboveOf17 ?x)
            (BelowOf3 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf15 ?x)
          )
          (and
            (AboveOf4 ?x)
            (BelowOf4 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf7 ?x)
          )
          (and
            (AboveOf17 ?x)
            (BelowOf1 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf11 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf16 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf19 ?x)
          )
          (and
            (AboveOf16 ?x)
            (BelowOf1 ?x)
          )
          (and
            (AboveOf7 ?x)
            (BelowOf3 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf8 ?x)
          )
          (and
            (AboveOf6 ?x)
            (BelowOf3 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf15 ?x)
          )
          (and
            (AboveOf7 ?x)
            (BelowOf4 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf18 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf4 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf17 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf9 ?x)
          )
          (and
            (AboveOf5 ?x)
            (BelowOf3 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf4 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf3 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf17 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf18 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf17 ?x)
          )
        )
        (and
          (Row5 ?x)
        )
      )
      (when
        (or
          (and
            (BelowOf3 ?x)
            (AboveOf12 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf11 ?x)
          )
          (and
            (AboveOf6 ?x)
            (BelowOf3 ?x)
          )
          (and
            (AboveOf5 ?x)
            (BelowOf4 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf14 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf8 ?x)
          )
          (and
            (AboveOf7 ?x)
            (BelowOf6 ?x)
          )
          (and
            (AboveOf5 ?x)
            (BelowOf2 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf19 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf18 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf15 ?x)
          )
          (and
            (AboveOf17 ?x)
            (BelowOf1 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf6 ?x)
          )
          (and
            (BelowOf6 ?x)
            (AboveOf19 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf4 ?x)
          )
          (and
            (AboveOf16 ?x)
            (BelowOf6 ?x)
          )
          (and
            (BelowOf3 ?x)
            (AboveOf11 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf11 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf16 ?x)
          )
          (and
            (AboveOf19 ?x)
            (BelowOf3 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf8 ?x)
          )
          (and
            (AboveOf9 ?x)
            (BelowOf6 ?x)
          )
          (and
            (BelowOf6 ?x)
            (AboveOf8 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf12 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf7 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf6 ?x)
          )
          (and
            (BelowOf3 ?x)
            (AboveOf8 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf19 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf6 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf2 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf12 ?x)
          )
          (and
            (AboveOf7 ?x)
            (BelowOf4 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf9 ?x)
          )
          (and
            (AboveOf17 ?x)
            (BelowOf3 ?x)
          )
          (and
            (AboveOf5 ?x)
            (BelowOf6 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf11 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf16 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf6 ?x)
          )
          (and
            (AboveOf15 ?x)
            (BelowOf6 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf17 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf17 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf8 ?x)
          )
          (and
            (AboveOf15 ?x)
            (BelowOf1 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf6 ?x)
          )
          (and
            (AboveOf10 ?x)
            (BelowOf6 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf3 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf12 ?x)
          )
          (and
            (AboveOf11 ?x)
            (BelowOf3 ?x)
          )
          (and
            (AboveOf15 ?x)
            (BelowOf4 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf10 ?x)
          )
          (and
            (BelowOf3 ?x)
            (AboveOf19 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf6 ?x)
          )
          (and
            (AboveOf17 ?x)
            (BelowOf6 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf10 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf1 ?x)
          )
          (and
            (AboveOf16 ?x)
            (BelowOf4 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf16 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf13 ?x)
          )
          (and
            (AboveOf5 ?x)
            (BelowOf1 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf3 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf5 ?x)
          )
          (and
            (BelowOf6 ?x)
            (AboveOf12 ?x)
          )
          (and
            (AboveOf7 ?x)
            (BelowOf3 ?x)
          )
          (and
            (BelowOf6 ?x)
            (AboveOf11 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf4 ?x)
          )
          (and
            (AboveOf16 ?x)
            (BelowOf1 ?x)
          )
          (and
            (AboveOf18 ?x)
            (BelowOf3 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf1 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf19 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf12 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf9 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf18 ?x)
          )
          (and
            (AboveOf9 ?x)
            (BelowOf1 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf15 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf10 ?x)
          )
          (and
            (AboveOf10 ?x)
            (BelowOf3 ?x)
          )
          (and
            (AboveOf6 ?x)
            (BelowOf6 ?x)
          )
          (and
            (AboveOf15 ?x)
            (BelowOf3 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf9 ?x)
          )
          (and
            (AboveOf18 ?x)
            (BelowOf6 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf17 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf11 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf10 ?x)
          )
          (and
            (AboveOf9 ?x)
            (BelowOf3 ?x)
          )
          (and
            (AboveOf5 ?x)
            (BelowOf5 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf17 ?x)
          )
          (and
            (AboveOf16 ?x)
            (BelowOf3 ?x)
          )
          (and
            (AboveOf7 ?x)
            (BelowOf1 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf2 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf8 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf7 ?x)
          )
          (and
            (AboveOf5 ?x)
            (BelowOf3 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf19 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf18 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf18 ?x)
          )
        )
        (and
          (Row6 ?x)
        )
      )
      (when
        (or
          (and
            (BelowOf3 ?x)
            (AboveOf12 ?x)
          )
          (and
            (BelowOf7 ?x)
            (AboveOf12 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf11 ?x)
          )
          (and
            (AboveOf6 ?x)
            (BelowOf3 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf14 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf8 ?x)
          )
          (and
            (AboveOf7 ?x)
            (BelowOf6 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf19 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf18 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf15 ?x)
          )
          (and
            (BelowOf6 ?x)
            (AboveOf6 ?x)
          )
          (and
            (AboveOf17 ?x)
            (BelowOf1 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf6 ?x)
          )
          (and
            (BelowOf7 ?x)
            (AboveOf9 ?x)
          )
          (and
            (BelowOf6 ?x)
            (AboveOf19 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf4 ?x)
          )
          (and
            (AboveOf16 ?x)
            (BelowOf6 ?x)
          )
          (and
            (BelowOf3 ?x)
            (AboveOf11 ?x)
          )
          (and
            (BelowOf7 ?x)
            (AboveOf11 ?x)
          )
          (and
            (BelowOf7 ?x)
            (AboveOf8 ?x)
          )
          (and
            (BelowOf7 ?x)
            (AboveOf17 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf11 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf16 ?x)
          )
          (and
            (AboveOf19 ?x)
            (BelowOf3 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf8 ?x)
          )
          (and
            (AboveOf9 ?x)
            (BelowOf6 ?x)
          )
          (and
            (BelowOf6 ?x)
            (AboveOf8 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf12 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf7 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf6 ?x)
          )
          (and
            (BelowOf3 ?x)
            (AboveOf8 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf19 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf6 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf2 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf12 ?x)
          )
          (and
            (AboveOf7 ?x)
            (BelowOf4 ?x)
          )
          (and
            (BelowOf7 ?x)
            (AboveOf19 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf9 ?x)
          )
          (and
            (AboveOf17 ?x)
            (BelowOf3 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf11 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf16 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf6 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf7 ?x)
          )
          (and
            (AboveOf15 ?x)
            (BelowOf6 ?x)
          )
          (and
            (BelowOf7 ?x)
            (AboveOf10 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf17 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf17 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf8 ?x)
          )
          (and
            (AboveOf15 ?x)
            (BelowOf1 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf6 ?x)
          )
          (and
            (BelowOf6 ?x)
            (AboveOf18 ?x)
          )
          (and
            (AboveOf10 ?x)
            (BelowOf6 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf3 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf12 ?x)
          )
          (and
            (AboveOf11 ?x)
            (BelowOf3 ?x)
          )
          (and
            (AboveOf15 ?x)
            (BelowOf4 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf10 ?x)
          )
          (and
            (BelowOf3 ?x)
            (AboveOf19 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf6 ?x)
          )
          (and
            (AboveOf17 ?x)
            (BelowOf6 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf10 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf1 ?x)
          )
          (and
            (AboveOf16 ?x)
            (BelowOf4 ?x)
          )
          (and
            (BelowOf6 ?x)
            (AboveOf10 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf16 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf13 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf3 ?x)
          )
          (and
            (BelowOf7 ?x)
            (AboveOf18 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf5 ?x)
          )
          (and
            (BelowOf6 ?x)
            (AboveOf12 ?x)
          )
          (and
            (AboveOf7 ?x)
            (BelowOf3 ?x)
          )
          (and
            (BelowOf7 ?x)
            (AboveOf16 ?x)
          )
          (and
            (BelowOf6 ?x)
            (AboveOf11 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf4 ?x)
          )
          (and
            (AboveOf16 ?x)
            (BelowOf1 ?x)
          )
          (and
            (AboveOf18 ?x)
            (BelowOf3 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf1 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf19 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf12 ?x)
          )
          (and
            (BelowOf7 ?x)
            (AboveOf7 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf9 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf18 ?x)
          )
          (and
            (AboveOf9 ?x)
            (BelowOf1 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf15 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf10 ?x)
          )
          (and
            (AboveOf10 ?x)
            (BelowOf3 ?x)
          )
          (and
            (AboveOf15 ?x)
            (BelowOf3 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf9 ?x)
          )
          (and
            (AboveOf15 ?x)
            (BelowOf7 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf7 ?x)
          )
          (and
            (AboveOf18 ?x)
            (BelowOf6 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf17 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf11 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf10 ?x)
          )
          (and
            (AboveOf9 ?x)
            (BelowOf3 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf17 ?x)
          )
          (and
            (AboveOf16 ?x)
            (BelowOf3 ?x)
          )
          (and
            (AboveOf7 ?x)
            (BelowOf1 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf2 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf8 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf7 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf19 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf18 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf18 ?x)
          )
          (and
            (BelowOf7 ?x)
            (AboveOf6 ?x)
          )
        )
        (and
          (Row7 ?x)
        )
      )
      (when
        (or
          (and
            (BelowOf3 ?x)
            (AboveOf12 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf7 ?x)
          )
          (and
            (BelowOf7 ?x)
            (AboveOf12 ?x)
          )
          (and
            (AboveOf16 ?x)
            (BelowOf8 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf11 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf14 ?x)
          )
          (and
            (BelowOf8 ?x)
            (AboveOf12 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf8 ?x)
          )
          (and
            (AboveOf7 ?x)
            (BelowOf6 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf19 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf18 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf15 ?x)
          )
          (and
            (AboveOf17 ?x)
            (BelowOf1 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf6 ?x)
          )
          (and
            (BelowOf7 ?x)
            (AboveOf9 ?x)
          )
          (and
            (BelowOf6 ?x)
            (AboveOf19 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf4 ?x)
          )
          (and
            (AboveOf15 ?x)
            (BelowOf8 ?x)
          )
          (and
            (AboveOf16 ?x)
            (BelowOf6 ?x)
          )
          (and
            (BelowOf3 ?x)
            (AboveOf11 ?x)
          )
          (and
            (BelowOf7 ?x)
            (AboveOf11 ?x)
          )
          (and
            (BelowOf7 ?x)
            (AboveOf8 ?x)
          )
          (and
            (BelowOf7 ?x)
            (AboveOf17 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf11 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf16 ?x)
          )
          (and
            (AboveOf19 ?x)
            (BelowOf3 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf8 ?x)
          )
          (and
            (AboveOf9 ?x)
            (BelowOf6 ?x)
          )
          (and
            (BelowOf6 ?x)
            (AboveOf8 ?x)
          )
          (and
            (AboveOf19 ?x)
            (BelowOf8 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf12 ?x)
          )
          (and
            (AboveOf7 ?x)
            (BelowOf7 ?x)
          )
          (and
            (BelowOf3 ?x)
            (AboveOf8 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf19 ?x)
          )
          (and
            (BelowOf8 ?x)
            (AboveOf8 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf16 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf2 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf12 ?x)
          )
          (and
            (AboveOf7 ?x)
            (BelowOf4 ?x)
          )
          (and
            (BelowOf7 ?x)
            (AboveOf19 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf9 ?x)
          )
          (and
            (AboveOf17 ?x)
            (BelowOf3 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf11 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf16 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf6 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf7 ?x)
          )
          (and
            (AboveOf15 ?x)
            (BelowOf6 ?x)
          )
          (and
            (AboveOf16 ?x)
            (BelowOf3 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf17 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf17 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf8 ?x)
          )
          (and
            (AboveOf15 ?x)
            (BelowOf1 ?x)
          )
          (and
            (BelowOf6 ?x)
            (AboveOf18 ?x)
          )
          (and
            (AboveOf7 ?x)
            (BelowOf8 ?x)
          )
          (and
            (AboveOf10 ?x)
            (BelowOf6 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf3 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf12 ?x)
          )
          (and
            (AboveOf11 ?x)
            (BelowOf3 ?x)
          )
          (and
            (AboveOf15 ?x)
            (BelowOf4 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf10 ?x)
          )
          (and
            (BelowOf3 ?x)
            (AboveOf19 ?x)
          )
          (and
            (AboveOf17 ?x)
            (BelowOf6 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf10 ?x)
          )
          (and
            (AboveOf9 ?x)
            (BelowOf8 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf1 ?x)
          )
          (and
            (AboveOf16 ?x)
            (BelowOf4 ?x)
          )
          (and
            (BelowOf6 ?x)
            (AboveOf10 ?x)
          )
          (and
            (AboveOf18 ?x)
            (BelowOf8 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf13 ?x)
          )
          (and
            (AboveOf10 ?x)
            (BelowOf8 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf3 ?x)
          )
          (and
            (BelowOf7 ?x)
            (AboveOf18 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf5 ?x)
          )
          (and
            (BelowOf6 ?x)
            (AboveOf12 ?x)
          )
          (and
            (AboveOf7 ?x)
            (BelowOf3 ?x)
          )
          (and
            (BelowOf7 ?x)
            (AboveOf16 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf8 ?x)
          )
          (and
            (BelowOf6 ?x)
            (AboveOf11 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf4 ?x)
          )
          (and
            (AboveOf17 ?x)
            (BelowOf8 ?x)
          )
          (and
            (AboveOf16 ?x)
            (BelowOf1 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf8 ?x)
          )
          (and
            (AboveOf11 ?x)
            (BelowOf8 ?x)
          )
          (and
            (AboveOf18 ?x)
            (BelowOf3 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf1 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf19 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf12 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf9 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf18 ?x)
          )
          (and
            (AboveOf9 ?x)
            (BelowOf1 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf15 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf10 ?x)
          )
          (and
            (AboveOf10 ?x)
            (BelowOf3 ?x)
          )
          (and
            (AboveOf15 ?x)
            (BelowOf3 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf9 ?x)
          )
          (and
            (AboveOf15 ?x)
            (BelowOf7 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf7 ?x)
          )
          (and
            (AboveOf18 ?x)
            (BelowOf6 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf17 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf11 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf10 ?x)
          )
          (and
            (AboveOf9 ?x)
            (BelowOf3 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf17 ?x)
          )
          (and
            (BelowOf7 ?x)
            (AboveOf10 ?x)
          )
          (and
            (AboveOf7 ?x)
            (BelowOf1 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf2 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf8 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf7 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf19 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf18 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf18 ?x)
          )
        )
        (and
          (Row8 ?x)
        )
      )
      (when
        (or
          (and
            (BelowOf9 ?x)
            (AboveOf12 ?x)
          )
          (and
            (BelowOf3 ?x)
            (AboveOf12 ?x)
          )
          (and
            (BelowOf9 ?x)
            (AboveOf8 ?x)
          )
          (and
            (BelowOf9 ?x)
            (AboveOf18 ?x)
          )
          (and
            (BelowOf7 ?x)
            (AboveOf12 ?x)
          )
          (and
            (AboveOf16 ?x)
            (BelowOf8 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf11 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf14 ?x)
          )
          (and
            (BelowOf8 ?x)
            (AboveOf12 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf8 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf19 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf18 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf15 ?x)
          )
          (and
            (AboveOf17 ?x)
            (BelowOf1 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf6 ?x)
          )
          (and
            (BelowOf7 ?x)
            (AboveOf9 ?x)
          )
          (and
            (BelowOf6 ?x)
            (AboveOf19 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf4 ?x)
          )
          (and
            (AboveOf15 ?x)
            (BelowOf8 ?x)
          )
          (and
            (AboveOf15 ?x)
            (BelowOf9 ?x)
          )
          (and
            (BelowOf3 ?x)
            (AboveOf11 ?x)
          )
          (and
            (BelowOf7 ?x)
            (AboveOf11 ?x)
          )
          (and
            (BelowOf7 ?x)
            (AboveOf8 ?x)
          )
          (and
            (BelowOf7 ?x)
            (AboveOf17 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf11 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf16 ?x)
          )
          (and
            (AboveOf19 ?x)
            (BelowOf3 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf8 ?x)
          )
          (and
            (AboveOf9 ?x)
            (BelowOf6 ?x)
          )
          (and
            (BelowOf6 ?x)
            (AboveOf8 ?x)
          )
          (and
            (AboveOf19 ?x)
            (BelowOf8 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf12 ?x)
          )
          (and
            (BelowOf3 ?x)
            (AboveOf8 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf19 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf16 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf2 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf12 ?x)
          )
          (and
            (BelowOf7 ?x)
            (AboveOf19 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf9 ?x)
          )
          (and
            (AboveOf8 ?x)
            (BelowOf8 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf11 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf16 ?x)
          )
          (and
            (AboveOf16 ?x)
            (BelowOf9 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf6 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf7 ?x)
          )
          (and
            (AboveOf15 ?x)
            (BelowOf6 ?x)
          )
          (and
            (AboveOf16 ?x)
            (BelowOf3 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf17 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf17 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf8 ?x)
          )
          (and
            (AboveOf15 ?x)
            (BelowOf1 ?x)
          )
          (and
            (AboveOf9 ?x)
            (BelowOf9 ?x)
          )
          (and
            (BelowOf6 ?x)
            (AboveOf18 ?x)
          )
          (and
            (AboveOf10 ?x)
            (BelowOf6 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf3 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf12 ?x)
          )
          (and
            (AboveOf11 ?x)
            (BelowOf3 ?x)
          )
          (and
            (BelowOf9 ?x)
            (AboveOf19 ?x)
          )
          (and
            (AboveOf15 ?x)
            (BelowOf4 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf10 ?x)
          )
          (and
            (BelowOf3 ?x)
            (AboveOf19 ?x)
          )
          (and
            (AboveOf17 ?x)
            (BelowOf3 ?x)
          )
          (and
            (AboveOf17 ?x)
            (BelowOf6 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf10 ?x)
          )
          (and
            (AboveOf9 ?x)
            (BelowOf8 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf1 ?x)
          )
          (and
            (AboveOf16 ?x)
            (BelowOf4 ?x)
          )
          (and
            (BelowOf6 ?x)
            (AboveOf10 ?x)
          )
          (and
            (AboveOf16 ?x)
            (BelowOf6 ?x)
          )
          (and
            (AboveOf18 ?x)
            (BelowOf8 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf13 ?x)
          )
          (and
            (AboveOf10 ?x)
            (BelowOf8 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf9 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf3 ?x)
          )
          (and
            (BelowOf7 ?x)
            (AboveOf18 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf5 ?x)
          )
          (and
            (BelowOf6 ?x)
            (AboveOf12 ?x)
          )
          (and
            (BelowOf9 ?x)
            (AboveOf10 ?x)
          )
          (and
            (BelowOf7 ?x)
            (AboveOf16 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf8 ?x)
          )
          (and
            (BelowOf6 ?x)
            (AboveOf11 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf4 ?x)
          )
          (and
            (AboveOf17 ?x)
            (BelowOf8 ?x)
          )
          (and
            (AboveOf16 ?x)
            (BelowOf1 ?x)
          )
          (and
            (AboveOf12 ?x)
            (BelowOf8 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf8 ?x)
          )
          (and
            (AboveOf11 ?x)
            (BelowOf8 ?x)
          )
          (and
            (AboveOf18 ?x)
            (BelowOf3 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf1 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf19 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf12 ?x)
          )
          (and
            (BelowOf9 ?x)
            (AboveOf11 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf9 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf18 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf9 ?x)
          )
          (and
            (AboveOf9 ?x)
            (BelowOf1 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf15 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf10 ?x)
          )
          (and
            (AboveOf10 ?x)
            (BelowOf3 ?x)
          )
          (and
            (AboveOf15 ?x)
            (BelowOf3 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf9 ?x)
          )
          (and
            (AboveOf15 ?x)
            (BelowOf7 ?x)
          )
          (and
            (BelowOf9 ?x)
            (AboveOf17 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf7 ?x)
          )
          (and
            (AboveOf18 ?x)
            (BelowOf6 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf17 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf11 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf10 ?x)
          )
          (and
            (AboveOf9 ?x)
            (BelowOf3 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf17 ?x)
          )
          (and
            (BelowOf7 ?x)
            (AboveOf10 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf2 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf8 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf19 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf18 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf18 ?x)
          )
        )
        (and
          (Row9 ?x)
        )
      )
      (when
        (or
          (and
            (BelowOf9 ?x)
            (AboveOf12 ?x)
          )
          (and
            (BelowOf3 ?x)
            (AboveOf12 ?x)
          )
          (and
            (BelowOf9 ?x)
            (AboveOf18 ?x)
          )
          (and
            (BelowOf10 ?x)
            (AboveOf11 ?x)
          )
          (and
            (BelowOf7 ?x)
            (AboveOf12 ?x)
          )
          (and
            (AboveOf16 ?x)
            (BelowOf8 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf11 ?x)
          )
          (and
            (AboveOf15 ?x)
            (BelowOf1 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf14 ?x)
          )
          (and
            (AboveOf17 ?x)
            (BelowOf1 ?x)
          )
          (and
            (BelowOf8 ?x)
            (AboveOf12 ?x)
          )
          (and
            (AboveOf9 ?x)
            (BelowOf3 ?x)
          )
          (and
            (AboveOf17 ?x)
            (BelowOf10 ?x)
          )
          (and
            (AboveOf15 ?x)
            (BelowOf10 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf19 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf18 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf15 ?x)
          )
          (and
            (AboveOf9 ?x)
            (BelowOf10 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf6 ?x)
          )
          (and
            (BelowOf7 ?x)
            (AboveOf9 ?x)
          )
          (and
            (BelowOf6 ?x)
            (AboveOf19 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf4 ?x)
          )
          (and
            (AboveOf15 ?x)
            (BelowOf8 ?x)
          )
          (and
            (AboveOf15 ?x)
            (BelowOf9 ?x)
          )
          (and
            (BelowOf3 ?x)
            (AboveOf11 ?x)
          )
          (and
            (BelowOf7 ?x)
            (AboveOf11 ?x)
          )
          (and
            (BelowOf7 ?x)
            (AboveOf17 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf11 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf16 ?x)
          )
          (and
            (AboveOf19 ?x)
            (BelowOf3 ?x)
          )
          (and
            (AboveOf9 ?x)
            (BelowOf6 ?x)
          )
          (and
            (AboveOf19 ?x)
            (BelowOf8 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf12 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf19 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf16 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf2 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf12 ?x)
          )
          (and
            (BelowOf9 ?x)
            (AboveOf19 ?x)
          )
          (and
            (BelowOf7 ?x)
            (AboveOf19 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf9 ?x)
          )
          (and
            (AboveOf16 ?x)
            (BelowOf10 ?x)
          )
          (and
            (AboveOf17 ?x)
            (BelowOf3 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf11 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf16 ?x)
          )
          (and
            (AboveOf16 ?x)
            (BelowOf9 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf6 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf7 ?x)
          )
          (and
            (AboveOf15 ?x)
            (BelowOf6 ?x)
          )
          (and
            (AboveOf16 ?x)
            (BelowOf3 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf17 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf17 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf10 ?x)
          )
          (and
            (BelowOf6 ?x)
            (AboveOf18 ?x)
          )
          (and
            (AboveOf10 ?x)
            (BelowOf6 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf3 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf12 ?x)
          )
          (and
            (AboveOf11 ?x)
            (BelowOf3 ?x)
          )
          (and
            (BelowOf9 ?x)
            (AboveOf9 ?x)
          )
          (and
            (AboveOf15 ?x)
            (BelowOf4 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf10 ?x)
          )
          (and
            (BelowOf3 ?x)
            (AboveOf19 ?x)
          )
          (and
            (AboveOf17 ?x)
            (BelowOf6 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf10 ?x)
          )
          (and
            (AboveOf9 ?x)
            (BelowOf8 ?x)
          )
          (and
            (BelowOf10 ?x)
            (AboveOf19 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf1 ?x)
          )
          (and
            (AboveOf16 ?x)
            (BelowOf4 ?x)
          )
          (and
            (BelowOf6 ?x)
            (AboveOf10 ?x)
          )
          (and
            (AboveOf16 ?x)
            (BelowOf6 ?x)
          )
          (and
            (AboveOf18 ?x)
            (BelowOf8 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf13 ?x)
          )
          (and
            (AboveOf10 ?x)
            (BelowOf8 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf9 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf10 ?x)
          )
          (and
            (BelowOf7 ?x)
            (AboveOf18 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf5 ?x)
          )
          (and
            (BelowOf6 ?x)
            (AboveOf12 ?x)
          )
          (and
            (BelowOf9 ?x)
            (AboveOf10 ?x)
          )
          (and
            (BelowOf7 ?x)
            (AboveOf16 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf8 ?x)
          )
          (and
            (BelowOf6 ?x)
            (AboveOf11 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf4 ?x)
          )
          (and
            (AboveOf17 ?x)
            (BelowOf8 ?x)
          )
          (and
            (AboveOf16 ?x)
            (BelowOf1 ?x)
          )
          (and
            (AboveOf12 ?x)
            (BelowOf8 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf8 ?x)
          )
          (and
            (AboveOf11 ?x)
            (BelowOf8 ?x)
          )
          (and
            (AboveOf18 ?x)
            (BelowOf3 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf1 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf19 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf12 ?x)
          )
          (and
            (BelowOf9 ?x)
            (AboveOf11 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf9 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf18 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf9 ?x)
          )
          (and
            (AboveOf9 ?x)
            (BelowOf1 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf15 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf10 ?x)
          )
          (and
            (AboveOf10 ?x)
            (BelowOf3 ?x)
          )
          (and
            (BelowOf10 ?x)
            (AboveOf12 ?x)
          )
          (and
            (AboveOf18 ?x)
            (BelowOf10 ?x)
          )
          (and
            (AboveOf15 ?x)
            (BelowOf3 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf9 ?x)
          )
          (and
            (AboveOf10 ?x)
            (BelowOf10 ?x)
          )
          (and
            (AboveOf15 ?x)
            (BelowOf7 ?x)
          )
          (and
            (BelowOf9 ?x)
            (AboveOf17 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf7 ?x)
          )
          (and
            (AboveOf18 ?x)
            (BelowOf6 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf17 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf11 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf10 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf17 ?x)
          )
          (and
            (BelowOf7 ?x)
            (AboveOf10 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf2 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf19 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf18 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf18 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf3 ?x)
          )
        )
        (and
          (Row10 ?x)
        )
      )
      (when
        (or
          (and
            (BelowOf9 ?x)
            (AboveOf12 ?x)
          )
          (and
            (BelowOf11 ?x)
            (AboveOf12 ?x)
          )
          (and
            (BelowOf3 ?x)
            (AboveOf12 ?x)
          )
          (and
            (BelowOf9 ?x)
            (AboveOf18 ?x)
          )
          (and
            (BelowOf10 ?x)
            (AboveOf11 ?x)
          )
          (and
            (BelowOf7 ?x)
            (AboveOf12 ?x)
          )
          (and
            (AboveOf16 ?x)
            (BelowOf8 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf11 ?x)
          )
          (and
            (AboveOf15 ?x)
            (BelowOf1 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf14 ?x)
          )
          (and
            (BelowOf8 ?x)
            (AboveOf12 ?x)
          )
          (and
            (AboveOf17 ?x)
            (BelowOf10 ?x)
          )
          (and
            (AboveOf15 ?x)
            (BelowOf10 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf19 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf18 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf15 ?x)
          )
          (and
            (AboveOf17 ?x)
            (BelowOf1 ?x)
          )
          (and
            (BelowOf11 ?x)
            (AboveOf11 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf6 ?x)
          )
          (and
            (AboveOf10 ?x)
            (BelowOf11 ?x)
          )
          (and
            (BelowOf6 ?x)
            (AboveOf19 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf4 ?x)
          )
          (and
            (AboveOf15 ?x)
            (BelowOf8 ?x)
          )
          (and
            (AboveOf15 ?x)
            (BelowOf9 ?x)
          )
          (and
            (BelowOf3 ?x)
            (AboveOf11 ?x)
          )
          (and
            (BelowOf7 ?x)
            (AboveOf11 ?x)
          )
          (and
            (BelowOf7 ?x)
            (AboveOf17 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf11 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf12 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf16 ?x)
          )
          (and
            (AboveOf19 ?x)
            (BelowOf3 ?x)
          )
          (and
            (AboveOf19 ?x)
            (BelowOf8 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf12 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf19 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf16 ?x)
          )
          (and
            (BelowOf11 ?x)
            (AboveOf19 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf2 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf11 ?x)
          )
          (and
            (BelowOf7 ?x)
            (AboveOf19 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf11 ?x)
          )
          (and
            (AboveOf16 ?x)
            (BelowOf10 ?x)
          )
          (and
            (AboveOf17 ?x)
            (BelowOf3 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf11 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf16 ?x)
          )
          (and
            (AboveOf16 ?x)
            (BelowOf9 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf6 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf7 ?x)
          )
          (and
            (AboveOf15 ?x)
            (BelowOf6 ?x)
          )
          (and
            (AboveOf16 ?x)
            (BelowOf3 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf17 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf17 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf10 ?x)
          )
          (and
            (BelowOf6 ?x)
            (AboveOf18 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf3 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf12 ?x)
          )
          (and
            (AboveOf11 ?x)
            (BelowOf3 ?x)
          )
          (and
            (BelowOf9 ?x)
            (AboveOf19 ?x)
          )
          (and
            (AboveOf15 ?x)
            (BelowOf4 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf10 ?x)
          )
          (and
            (BelowOf3 ?x)
            (AboveOf19 ?x)
          )
          (and
            (AboveOf17 ?x)
            (BelowOf6 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf10 ?x)
          )
          (and
            (AboveOf17 ?x)
            (BelowOf11 ?x)
          )
          (and
            (BelowOf10 ?x)
            (AboveOf19 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf1 ?x)
          )
          (and
            (AboveOf16 ?x)
            (BelowOf4 ?x)
          )
          (and
            (AboveOf15 ?x)
            (BelowOf11 ?x)
          )
          (and
            (BelowOf6 ?x)
            (AboveOf10 ?x)
          )
          (and
            (AboveOf16 ?x)
            (BelowOf6 ?x)
          )
          (and
            (AboveOf18 ?x)
            (BelowOf8 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf13 ?x)
          )
          (and
            (AboveOf10 ?x)
            (BelowOf8 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf9 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf10 ?x)
          )
          (and
            (BelowOf7 ?x)
            (AboveOf18 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf5 ?x)
          )
          (and
            (BelowOf6 ?x)
            (AboveOf12 ?x)
          )
          (and
            (BelowOf9 ?x)
            (AboveOf10 ?x)
          )
          (and
            (BelowOf7 ?x)
            (AboveOf16 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf8 ?x)
          )
          (and
            (BelowOf6 ?x)
            (AboveOf11 ?x)
          )
          (and
            (AboveOf18 ?x)
            (BelowOf11 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf4 ?x)
          )
          (and
            (AboveOf17 ?x)
            (BelowOf8 ?x)
          )
          (and
            (AboveOf16 ?x)
            (BelowOf11 ?x)
          )
          (and
            (AboveOf16 ?x)
            (BelowOf1 ?x)
          )
          (and
            (AboveOf12 ?x)
            (BelowOf8 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf8 ?x)
          )
          (and
            (AboveOf11 ?x)
            (BelowOf8 ?x)
          )
          (and
            (AboveOf18 ?x)
            (BelowOf3 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf1 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf19 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf12 ?x)
          )
          (and
            (BelowOf9 ?x)
            (AboveOf11 ?x)
          )
          (and
            (AboveOf18 ?x)
            (BelowOf10 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf18 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf9 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf15 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf10 ?x)
          )
          (and
            (AboveOf10 ?x)
            (BelowOf3 ?x)
          )
          (and
            (BelowOf10 ?x)
            (AboveOf12 ?x)
          )
          (and
            (AboveOf15 ?x)
            (BelowOf3 ?x)
          )
          (and
            (AboveOf10 ?x)
            (BelowOf10 ?x)
          )
          (and
            (AboveOf15 ?x)
            (BelowOf7 ?x)
          )
          (and
            (BelowOf9 ?x)
            (AboveOf17 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf7 ?x)
          )
          (and
            (AboveOf18 ?x)
            (BelowOf6 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf17 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf11 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf10 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf17 ?x)
          )
          (and
            (BelowOf7 ?x)
            (AboveOf10 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf2 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf19 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf18 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf18 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf3 ?x)
          )
        )
        (and
          (Row11 ?x)
        )
      )
      (when
        (or
          (and
            (AboveOf19 ?x)
            (BelowOf12 ?x)
          )
          (and
            (BelowOf9 ?x)
            (AboveOf12 ?x)
          )
          (and
            (BelowOf11 ?x)
            (AboveOf12 ?x)
          )
          (and
            (BelowOf3 ?x)
            (AboveOf12 ?x)
          )
          (and
            (BelowOf9 ?x)
            (AboveOf18 ?x)
          )
          (and
            (BelowOf10 ?x)
            (AboveOf11 ?x)
          )
          (and
            (BelowOf7 ?x)
            (AboveOf12 ?x)
          )
          (and
            (AboveOf16 ?x)
            (BelowOf8 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf11 ?x)
          )
          (and
            (AboveOf16 ?x)
            (BelowOf12 ?x)
          )
          (and
            (AboveOf15 ?x)
            (BelowOf1 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf14 ?x)
          )
          (and
            (BelowOf8 ?x)
            (AboveOf12 ?x)
          )
          (and
            (AboveOf17 ?x)
            (BelowOf10 ?x)
          )
          (and
            (AboveOf15 ?x)
            (BelowOf10 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf19 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf18 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf15 ?x)
          )
          (and
            (BelowOf12 ?x)
            (AboveOf12 ?x)
          )
          (and
            (AboveOf17 ?x)
            (BelowOf1 ?x)
          )
          (and
            (BelowOf11 ?x)
            (AboveOf11 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf6 ?x)
          )
          (and
            (BelowOf6 ?x)
            (AboveOf19 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf4 ?x)
          )
          (and
            (AboveOf15 ?x)
            (BelowOf8 ?x)
          )
          (and
            (AboveOf15 ?x)
            (BelowOf9 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf12 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf3 ?x)
          )
          (and
            (BelowOf7 ?x)
            (AboveOf11 ?x)
          )
          (and
            (BelowOf7 ?x)
            (AboveOf17 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf11 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf12 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf16 ?x)
          )
          (and
            (AboveOf19 ?x)
            (BelowOf3 ?x)
          )
          (and
            (AboveOf19 ?x)
            (BelowOf8 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf12 ?x)
          )
          (and
            (AboveOf11 ?x)
            (BelowOf12 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf19 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf16 ?x)
          )
          (and
            (BelowOf11 ?x)
            (AboveOf19 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf2 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf11 ?x)
          )
          (and
            (BelowOf7 ?x)
            (AboveOf19 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf11 ?x)
          )
          (and
            (AboveOf16 ?x)
            (BelowOf10 ?x)
          )
          (and
            (AboveOf17 ?x)
            (BelowOf3 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf11 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf16 ?x)
          )
          (and
            (AboveOf16 ?x)
            (BelowOf9 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf6 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf7 ?x)
          )
          (and
            (AboveOf15 ?x)
            (BelowOf6 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf17 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf17 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf10 ?x)
          )
          (and
            (BelowOf6 ?x)
            (AboveOf18 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf3 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf12 ?x)
          )
          (and
            (AboveOf17 ?x)
            (BelowOf12 ?x)
          )
          (and
            (AboveOf11 ?x)
            (BelowOf3 ?x)
          )
          (and
            (BelowOf9 ?x)
            (AboveOf19 ?x)
          )
          (and
            (AboveOf15 ?x)
            (BelowOf4 ?x)
          )
          (and
            (BelowOf3 ?x)
            (AboveOf19 ?x)
          )
          (and
            (AboveOf17 ?x)
            (BelowOf6 ?x)
          )
          (and
            (AboveOf17 ?x)
            (BelowOf11 ?x)
          )
          (and
            (BelowOf10 ?x)
            (AboveOf19 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf1 ?x)
          )
          (and
            (AboveOf16 ?x)
            (BelowOf4 ?x)
          )
          (and
            (AboveOf15 ?x)
            (BelowOf11 ?x)
          )
          (and
            (AboveOf16 ?x)
            (BelowOf6 ?x)
          )
          (and
            (AboveOf18 ?x)
            (BelowOf8 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf13 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf9 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf10 ?x)
          )
          (and
            (BelowOf7 ?x)
            (AboveOf18 ?x)
          )
          (and
            (AboveOf15 ?x)
            (BelowOf12 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf5 ?x)
          )
          (and
            (BelowOf6 ?x)
            (AboveOf12 ?x)
          )
          (and
            (BelowOf7 ?x)
            (AboveOf16 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf8 ?x)
          )
          (and
            (BelowOf6 ?x)
            (AboveOf11 ?x)
          )
          (and
            (AboveOf18 ?x)
            (BelowOf11 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf4 ?x)
          )
          (and
            (AboveOf17 ?x)
            (BelowOf8 ?x)
          )
          (and
            (AboveOf16 ?x)
            (BelowOf11 ?x)
          )
          (and
            (AboveOf16 ?x)
            (BelowOf1 ?x)
          )
          (and
            (AboveOf12 ?x)
            (BelowOf8 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf8 ?x)
          )
          (and
            (AboveOf11 ?x)
            (BelowOf8 ?x)
          )
          (and
            (AboveOf18 ?x)
            (BelowOf3 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf1 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf19 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf12 ?x)
          )
          (and
            (BelowOf9 ?x)
            (AboveOf11 ?x)
          )
          (and
            (AboveOf18 ?x)
            (BelowOf10 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf18 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf9 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf15 ?x)
          )
          (and
            (BelowOf10 ?x)
            (AboveOf12 ?x)
          )
          (and
            (AboveOf15 ?x)
            (BelowOf3 ?x)
          )
          (and
            (AboveOf15 ?x)
            (BelowOf7 ?x)
          )
          (and
            (BelowOf9 ?x)
            (AboveOf17 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf12 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf7 ?x)
          )
          (and
            (AboveOf18 ?x)
            (BelowOf6 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf17 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf11 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf17 ?x)
          )
          (and
            (AboveOf16 ?x)
            (BelowOf3 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf2 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf19 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf18 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf18 ?x)
          )
          (and
            (AboveOf18 ?x)
            (BelowOf12 ?x)
          )
        )
        (and
          (Row12 ?x)
        )
      )
      (when
        (or
          (and
            (AboveOf19 ?x)
            (BelowOf12 ?x)
          )
          (and
            (AboveOf12 ?x)
            (BelowOf12 ?x)
          )
          (and
            (BelowOf9 ?x)
            (AboveOf12 ?x)
          )
          (and
            (BelowOf11 ?x)
            (AboveOf12 ?x)
          )
          (and
            (BelowOf3 ?x)
            (AboveOf12 ?x)
          )
          (and
            (BelowOf9 ?x)
            (AboveOf18 ?x)
          )
          (and
            (BelowOf7 ?x)
            (AboveOf12 ?x)
          )
          (and
            (AboveOf16 ?x)
            (BelowOf8 ?x)
          )
          (and
            (AboveOf16 ?x)
            (BelowOf12 ?x)
          )
          (and
            (BelowOf13 ?x)
            (AboveOf12 ?x)
          )
          (and
            (AboveOf15 ?x)
            (BelowOf1 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf14 ?x)
          )
          (and
            (BelowOf13 ?x)
            (AboveOf15 ?x)
          )
          (and
            (AboveOf17 ?x)
            (BelowOf10 ?x)
          )
          (and
            (AboveOf15 ?x)
            (BelowOf10 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf19 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf18 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf15 ?x)
          )
          (and
            (AboveOf17 ?x)
            (BelowOf1 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf6 ?x)
          )
          (and
            (BelowOf6 ?x)
            (AboveOf19 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf4 ?x)
          )
          (and
            (AboveOf15 ?x)
            (BelowOf8 ?x)
          )
          (and
            (AboveOf15 ?x)
            (BelowOf9 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf12 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf3 ?x)
          )
          (and
            (BelowOf7 ?x)
            (AboveOf17 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf12 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf16 ?x)
          )
          (and
            (AboveOf19 ?x)
            (BelowOf3 ?x)
          )
          (and
            (AboveOf19 ?x)
            (BelowOf8 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf19 ?x)
          )
          (and
            (BelowOf11 ?x)
            (AboveOf19 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf2 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf11 ?x)
          )
          (and
            (BelowOf7 ?x)
            (AboveOf19 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf11 ?x)
          )
          (and
            (AboveOf16 ?x)
            (BelowOf10 ?x)
          )
          (and
            (AboveOf17 ?x)
            (BelowOf3 ?x)
          )
          (and
            (BelowOf13 ?x)
            (AboveOf14 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf16 ?x)
          )
          (and
            (AboveOf16 ?x)
            (BelowOf9 ?x)
          )
          (and
            (BelowOf13 ?x)
            (AboveOf13 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf6 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf7 ?x)
          )
          (and
            (AboveOf15 ?x)
            (BelowOf6 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf17 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf17 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf10 ?x)
          )
          (and
            (BelowOf6 ?x)
            (AboveOf18 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf3 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf12 ?x)
          )
          (and
            (AboveOf17 ?x)
            (BelowOf12 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf12 ?x)
          )
          (and
            (BelowOf9 ?x)
            (AboveOf19 ?x)
          )
          (and
            (AboveOf15 ?x)
            (BelowOf4 ?x)
          )
          (and
            (BelowOf3 ?x)
            (AboveOf19 ?x)
          )
          (and
            (AboveOf17 ?x)
            (BelowOf6 ?x)
          )
          (and
            (AboveOf17 ?x)
            (BelowOf11 ?x)
          )
          (and
            (BelowOf10 ?x)
            (AboveOf19 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf1 ?x)
          )
          (and
            (BelowOf13 ?x)
            (AboveOf17 ?x)
          )
          (and
            (AboveOf16 ?x)
            (BelowOf4 ?x)
          )
          (and
            (AboveOf15 ?x)
            (BelowOf11 ?x)
          )
          (and
            (AboveOf16 ?x)
            (BelowOf6 ?x)
          )
          (and
            (BelowOf13 ?x)
            (AboveOf19 ?x)
          )
          (and
            (AboveOf18 ?x)
            (BelowOf8 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf13 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf9 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf10 ?x)
          )
          (and
            (BelowOf7 ?x)
            (AboveOf18 ?x)
          )
          (and
            (AboveOf15 ?x)
            (BelowOf12 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf5 ?x)
          )
          (and
            (BelowOf6 ?x)
            (AboveOf12 ?x)
          )
          (and
            (BelowOf7 ?x)
            (AboveOf16 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf8 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf4 ?x)
          )
          (and
            (AboveOf17 ?x)
            (BelowOf8 ?x)
          )
          (and
            (AboveOf16 ?x)
            (BelowOf11 ?x)
          )
          (and
            (AboveOf16 ?x)
            (BelowOf1 ?x)
          )
          (and
            (AboveOf12 ?x)
            (BelowOf8 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf8 ?x)
          )
          (and
            (AboveOf18 ?x)
            (BelowOf3 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf1 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf19 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf12 ?x)
          )
          (and
            (AboveOf18 ?x)
            (BelowOf11 ?x)
          )
          (and
            (AboveOf18 ?x)
            (BelowOf10 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf18 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf9 ?x)
          )
          (and
            (BelowOf13 ?x)
            (AboveOf16 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf15 ?x)
          )
          (and
            (BelowOf10 ?x)
            (AboveOf12 ?x)
          )
          (and
            (AboveOf15 ?x)
            (BelowOf3 ?x)
          )
          (and
            (AboveOf15 ?x)
            (BelowOf7 ?x)
          )
          (and
            (BelowOf13 ?x)
            (AboveOf18 ?x)
          )
          (and
            (BelowOf9 ?x)
            (AboveOf17 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf12 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf7 ?x)
          )
          (and
            (AboveOf18 ?x)
            (BelowOf6 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf17 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf16 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf17 ?x)
          )
          (and
            (AboveOf16 ?x)
            (BelowOf3 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf2 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf19 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf18 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf18 ?x)
          )
          (and
            (AboveOf18 ?x)
            (BelowOf12 ?x)
          )
        )
        (and
          (Row13 ?x)
        )
      )
      (when
        (or
          (and
            (AboveOf19 ?x)
            (BelowOf12 ?x)
          )
          (and
            (BelowOf9 ?x)
            (AboveOf18 ?x)
          )
          (and
            (AboveOf16 ?x)
            (BelowOf10 ?x)
          )
          (and
            (AboveOf16 ?x)
            (BelowOf8 ?x)
          )
          (and
            (AboveOf16 ?x)
            (BelowOf12 ?x)
          )
          (and
            (AboveOf15 ?x)
            (BelowOf1 ?x)
          )
          (and
            (BelowOf14 ?x)
            (AboveOf17 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf14 ?x)
          )
          (and
            (BelowOf13 ?x)
            (AboveOf15 ?x)
          )
          (and
            (AboveOf17 ?x)
            (BelowOf10 ?x)
          )
          (and
            (AboveOf15 ?x)
            (BelowOf10 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf19 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf18 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf15 ?x)
          )
          (and
            (AboveOf17 ?x)
            (BelowOf1 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf6 ?x)
          )
          (and
            (BelowOf6 ?x)
            (AboveOf19 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf4 ?x)
          )
          (and
            (AboveOf15 ?x)
            (BelowOf8 ?x)
          )
          (and
            (AboveOf15 ?x)
            (BelowOf9 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf12 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf3 ?x)
          )
          (and
            (BelowOf7 ?x)
            (AboveOf17 ?x)
          )
          (and
            (BelowOf14 ?x)
            (AboveOf18 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf16 ?x)
          )
          (and
            (AboveOf19 ?x)
            (BelowOf3 ?x)
          )
          (and
            (AboveOf19 ?x)
            (BelowOf8 ?x)
          )
          (and
            (BelowOf14 ?x)
            (AboveOf19 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf14 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf19 ?x)
          )
          (and
            (BelowOf11 ?x)
            (AboveOf19 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf2 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf11 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf13 ?x)
          )
          (and
            (BelowOf7 ?x)
            (AboveOf19 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf11 ?x)
          )
          (and
            (AboveOf17 ?x)
            (BelowOf3 ?x)
          )
          (and
            (BelowOf13 ?x)
            (AboveOf14 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf14 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf16 ?x)
          )
          (and
            (AboveOf16 ?x)
            (BelowOf9 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf6 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf7 ?x)
          )
          (and
            (AboveOf15 ?x)
            (BelowOf6 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf17 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf17 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf10 ?x)
          )
          (and
            (BelowOf6 ?x)
            (AboveOf18 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf3 ?x)
          )
          (and
            (AboveOf17 ?x)
            (BelowOf12 ?x)
          )
          (and
            (BelowOf14 ?x)
            (AboveOf16 ?x)
          )
          (and
            (BelowOf9 ?x)
            (AboveOf19 ?x)
          )
          (and
            (AboveOf15 ?x)
            (BelowOf4 ?x)
          )
          (and
            (BelowOf3 ?x)
            (AboveOf19 ?x)
          )
          (and
            (AboveOf17 ?x)
            (BelowOf6 ?x)
          )
          (and
            (AboveOf17 ?x)
            (BelowOf11 ?x)
          )
          (and
            (BelowOf10 ?x)
            (AboveOf19 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf1 ?x)
          )
          (and
            (BelowOf13 ?x)
            (AboveOf17 ?x)
          )
          (and
            (AboveOf16 ?x)
            (BelowOf4 ?x)
          )
          (and
            (AboveOf15 ?x)
            (BelowOf11 ?x)
          )
          (and
            (AboveOf16 ?x)
            (BelowOf6 ?x)
          )
          (and
            (BelowOf13 ?x)
            (AboveOf19 ?x)
          )
          (and
            (AboveOf18 ?x)
            (BelowOf8 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf9 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf10 ?x)
          )
          (and
            (BelowOf7 ?x)
            (AboveOf18 ?x)
          )
          (and
            (AboveOf15 ?x)
            (BelowOf12 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf5 ?x)
          )
          (and
            (BelowOf7 ?x)
            (AboveOf16 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf8 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf4 ?x)
          )
          (and
            (AboveOf17 ?x)
            (BelowOf8 ?x)
          )
          (and
            (AboveOf16 ?x)
            (BelowOf11 ?x)
          )
          (and
            (AboveOf16 ?x)
            (BelowOf1 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf8 ?x)
          )
          (and
            (AboveOf18 ?x)
            (BelowOf3 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf1 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf19 ?x)
          )
          (and
            (AboveOf18 ?x)
            (BelowOf11 ?x)
          )
          (and
            (AboveOf18 ?x)
            (BelowOf10 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf18 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf9 ?x)
          )
          (and
            (BelowOf13 ?x)
            (AboveOf16 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf15 ?x)
          )
          (and
            (AboveOf15 ?x)
            (BelowOf3 ?x)
          )
          (and
            (AboveOf15 ?x)
            (BelowOf7 ?x)
          )
          (and
            (BelowOf13 ?x)
            (AboveOf18 ?x)
          )
          (and
            (BelowOf9 ?x)
            (AboveOf17 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf12 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf7 ?x)
          )
          (and
            (AboveOf18 ?x)
            (BelowOf6 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf17 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf16 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf17 ?x)
          )
          (and
            (AboveOf16 ?x)
            (BelowOf3 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf2 ?x)
          )
          (and
            (BelowOf14 ?x)
            (AboveOf15 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf19 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf18 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf18 ?x)
          )
          (and
            (AboveOf18 ?x)
            (BelowOf12 ?x)
          )
        )
        (and
          (Row14 ?x)
        )
      )
      (when
        (or
          (and
            (AboveOf19 ?x)
            (BelowOf12 ?x)
          )
          (and
            (BelowOf9 ?x)
            (AboveOf18 ?x)
          )
          (and
            (AboveOf16 ?x)
            (BelowOf10 ?x)
          )
          (and
            (AboveOf16 ?x)
            (BelowOf8 ?x)
          )
          (and
            (AboveOf16 ?x)
            (BelowOf12 ?x)
          )
          (and
            (BelowOf14 ?x)
            (AboveOf17 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf14 ?x)
          )
          (and
            (BelowOf13 ?x)
            (AboveOf15 ?x)
          )
          (and
            (AboveOf17 ?x)
            (BelowOf10 ?x)
          )
          (and
            (BelowOf15 ?x)
            (AboveOf16 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf19 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf18 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf15 ?x)
          )
          (and
            (AboveOf17 ?x)
            (BelowOf1 ?x)
          )
          (and
            (AboveOf15 ?x)
            (BelowOf10 ?x)
          )
          (and
            (BelowOf6 ?x)
            (AboveOf19 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf4 ?x)
          )
          (and
            (AboveOf15 ?x)
            (BelowOf8 ?x)
          )
          (and
            (AboveOf15 ?x)
            (BelowOf9 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf12 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf3 ?x)
          )
          (and
            (BelowOf7 ?x)
            (AboveOf17 ?x)
          )
          (and
            (BelowOf15 ?x)
            (AboveOf19 ?x)
          )
          (and
            (BelowOf14 ?x)
            (AboveOf18 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf16 ?x)
          )
          (and
            (AboveOf19 ?x)
            (BelowOf3 ?x)
          )
          (and
            (AboveOf19 ?x)
            (BelowOf8 ?x)
          )
          (and
            (BelowOf14 ?x)
            (AboveOf19 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf19 ?x)
          )
          (and
            (BelowOf11 ?x)
            (AboveOf19 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf2 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf11 ?x)
          )
          (and
            (BelowOf7 ?x)
            (AboveOf19 ?x)
          )
          (and
            (AboveOf17 ?x)
            (BelowOf3 ?x)
          )
          (and
            (BelowOf13 ?x)
            (AboveOf14 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf14 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf16 ?x)
          )
          (and
            (AboveOf16 ?x)
            (BelowOf9 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf6 ?x)
          )
          (and
            (AboveOf15 ?x)
            (BelowOf6 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf17 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf17 ?x)
          )
          (and
            (AboveOf15 ?x)
            (BelowOf1 ?x)
          )
          (and
            (BelowOf15 ?x)
            (AboveOf17 ?x)
          )
          (and
            (BelowOf6 ?x)
            (AboveOf18 ?x)
          )
          (and
            (AboveOf17 ?x)
            (BelowOf12 ?x)
          )
          (and
            (BelowOf14 ?x)
            (AboveOf16 ?x)
          )
          (and
            (BelowOf9 ?x)
            (AboveOf19 ?x)
          )
          (and
            (AboveOf15 ?x)
            (BelowOf4 ?x)
          )
          (and
            (BelowOf3 ?x)
            (AboveOf19 ?x)
          )
          (and
            (AboveOf17 ?x)
            (BelowOf6 ?x)
          )
          (and
            (AboveOf17 ?x)
            (BelowOf11 ?x)
          )
          (and
            (BelowOf10 ?x)
            (AboveOf19 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf1 ?x)
          )
          (and
            (BelowOf13 ?x)
            (AboveOf17 ?x)
          )
          (and
            (AboveOf16 ?x)
            (BelowOf4 ?x)
          )
          (and
            (AboveOf15 ?x)
            (BelowOf11 ?x)
          )
          (and
            (AboveOf16 ?x)
            (BelowOf6 ?x)
          )
          (and
            (BelowOf13 ?x)
            (AboveOf19 ?x)
          )
          (and
            (AboveOf18 ?x)
            (BelowOf8 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf9 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf10 ?x)
          )
          (and
            (BelowOf7 ?x)
            (AboveOf18 ?x)
          )
          (and
            (AboveOf15 ?x)
            (BelowOf15 ?x)
          )
          (and
            (AboveOf15 ?x)
            (BelowOf12 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf15 ?x)
          )
          (and
            (BelowOf7 ?x)
            (AboveOf16 ?x)
          )
          (and
            (AboveOf17 ?x)
            (BelowOf8 ?x)
          )
          (and
            (AboveOf16 ?x)
            (BelowOf11 ?x)
          )
          (and
            (AboveOf16 ?x)
            (BelowOf1 ?x)
          )
          (and
            (BelowOf15 ?x)
            (AboveOf18 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf8 ?x)
          )
          (and
            (AboveOf18 ?x)
            (BelowOf3 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf19 ?x)
          )
          (and
            (AboveOf18 ?x)
            (BelowOf11 ?x)
          )
          (and
            (AboveOf18 ?x)
            (BelowOf10 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf18 ?x)
          )
          (and
            (BelowOf13 ?x)
            (AboveOf16 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf15 ?x)
          )
          (and
            (AboveOf15 ?x)
            (BelowOf3 ?x)
          )
          (and
            (AboveOf15 ?x)
            (BelowOf7 ?x)
          )
          (and
            (BelowOf13 ?x)
            (AboveOf18 ?x)
          )
          (and
            (BelowOf9 ?x)
            (AboveOf17 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf7 ?x)
          )
          (and
            (AboveOf18 ?x)
            (BelowOf6 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf17 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf16 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf17 ?x)
          )
          (and
            (AboveOf16 ?x)
            (BelowOf3 ?x)
          )
          (and
            (BelowOf14 ?x)
            (AboveOf15 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf19 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf18 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf18 ?x)
          )
          (and
            (AboveOf18 ?x)
            (BelowOf12 ?x)
          )
        )
        (and
          (Row15 ?x)
        )
      )
      (when
        (or
          (and
            (BelowOf15 ?x)
            (AboveOf18 ?x)
          )
          (and
            (AboveOf19 ?x)
            (BelowOf8 ?x)
          )
          (and
            (AboveOf19 ?x)
            (BelowOf12 ?x)
          )
          (and
            (AboveOf18 ?x)
            (BelowOf3 ?x)
          )
          (and
            (AboveOf15 ?x)
            (BelowOf8 ?x)
          )
          (and
            (AboveOf15 ?x)
            (BelowOf1 ?x)
          )
          (and
            (AboveOf15 ?x)
            (BelowOf9 ?x)
          )
          (and
            (BelowOf15 ?x)
            (AboveOf17 ?x)
          )
          (and
            (BelowOf16 ?x)
            (AboveOf18 ?x)
          )
          (and
            (BelowOf6 ?x)
            (AboveOf18 ?x)
          )
          (and
            (AboveOf15 ?x)
            (BelowOf6 ?x)
          )
          (and
            (AboveOf18 ?x)
            (BelowOf11 ?x)
          )
          (and
            (BelowOf7 ?x)
            (AboveOf17 ?x)
          )
          (and
            (AboveOf17 ?x)
            (BelowOf12 ?x)
          )
          (and
            (AboveOf15 ?x)
            (BelowOf16 ?x)
          )
          (and
            (BelowOf14 ?x)
            (AboveOf16 ?x)
          )
          (and
            (BelowOf16 ?x)
            (AboveOf19 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf16 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf17 ?x)
          )
          (and
            (AboveOf19 ?x)
            (BelowOf3 ?x)
          )
          (and
            (AboveOf15 ?x)
            (BelowOf4 ?x)
          )
          (and
            (BelowOf3 ?x)
            (AboveOf19 ?x)
          )
          (and
            (AboveOf16 ?x)
            (BelowOf10 ?x)
          )
          (and
            (BelowOf13 ?x)
            (AboveOf16 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf15 ?x)
          )
          (and
            (BelowOf9 ?x)
            (AboveOf18 ?x)
          )
          (and
            (AboveOf16 ?x)
            (BelowOf8 ?x)
          )
          (and
            (AboveOf17 ?x)
            (BelowOf6 ?x)
          )
          (and
            (BelowOf14 ?x)
            (AboveOf19 ?x)
          )
          (and
            (AboveOf17 ?x)
            (BelowOf11 ?x)
          )
          (and
            (BelowOf10 ?x)
            (AboveOf19 ?x)
          )
          (and
            (AboveOf16 ?x)
            (BelowOf12 ?x)
          )
          (and
            (BelowOf9 ?x)
            (AboveOf19 ?x)
          )
          (and
            (AboveOf18 ?x)
            (BelowOf10 ?x)
          )
          (and
            (AboveOf15 ?x)
            (BelowOf3 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf19 ?x)
          )
          (and
            (BelowOf13 ?x)
            (AboveOf17 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf18 ?x)
          )
          (and
            (AboveOf15 ?x)
            (BelowOf11 ?x)
          )
          (and
            (BelowOf16 ?x)
            (AboveOf16 ?x)
          )
          (and
            (BelowOf11 ?x)
            (AboveOf19 ?x)
          )
          (and
            (BelowOf13 ?x)
            (AboveOf18 ?x)
          )
          (and
            (BelowOf9 ?x)
            (AboveOf17 ?x)
          )
          (and
            (BelowOf14 ?x)
            (AboveOf17 ?x)
          )
          (and
            (AboveOf16 ?x)
            (BelowOf6 ?x)
          )
          (and
            (BelowOf13 ?x)
            (AboveOf19 ?x)
          )
          (and
            (BelowOf16 ?x)
            (AboveOf17 ?x)
          )
          (and
            (BelowOf13 ?x)
            (AboveOf15 ?x)
          )
          (and
            (AboveOf18 ?x)
            (BelowOf6 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf17 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf19 ?x)
          )
          (and
            (AboveOf18 ?x)
            (BelowOf8 ?x)
          )
          (and
            (BelowOf7 ?x)
            (AboveOf19 ?x)
          )
          (and
            (BelowOf14 ?x)
            (AboveOf18 ?x)
          )
          (and
            (AboveOf17 ?x)
            (BelowOf10 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf17 ?x)
          )
          (and
            (AboveOf16 ?x)
            (BelowOf3 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf15 ?x)
          )
          (and
            (BelowOf15 ?x)
            (AboveOf16 ?x)
          )
          (and
            (AboveOf16 ?x)
            (BelowOf4 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf19 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf18 ?x)
          )
          (and
            (AboveOf17 ?x)
            (BelowOf3 ?x)
          )
          (and
            (BelowOf7 ?x)
            (AboveOf18 ?x)
          )
          (and
            (AboveOf15 ?x)
            (BelowOf15 ?x)
          )
          (and
            (AboveOf17 ?x)
            (BelowOf1 ?x)
          )
          (and
            (AboveOf15 ?x)
            (BelowOf10 ?x)
          )
          (and
            (BelowOf14 ?x)
            (AboveOf15 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf16 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf19 ?x)
          )
          (and
            (AboveOf16 ?x)
            (BelowOf9 ?x)
          )
          (and
            (AboveOf15 ?x)
            (BelowOf7 ?x)
          )
          (and
            (AboveOf15 ?x)
            (BelowOf12 ?x)
          )
          (and
            (BelowOf7 ?x)
            (AboveOf16 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf18 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf17 ?x)
          )
          (and
            (BelowOf6 ?x)
            (AboveOf19 ?x)
          )
          (and
            (BelowOf15 ?x)
            (AboveOf19 ?x)
          )
          (and
            (AboveOf17 ?x)
            (BelowOf8 ?x)
          )
          (and
            (AboveOf16 ?x)
            (BelowOf11 ?x)
          )
          (and
            (AboveOf16 ?x)
            (BelowOf1 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf16 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf18 ?x)
          )
          (and
            (AboveOf18 ?x)
            (BelowOf12 ?x)
          )
        )
        (and
          (Row16 ?x)
        )
      )
      (when
        (or
          (and
            (BelowOf15 ?x)
            (AboveOf18 ?x)
          )
          (and
            (AboveOf19 ?x)
            (BelowOf8 ?x)
          )
          (and
            (AboveOf19 ?x)
            (BelowOf12 ?x)
          )
          (and
            (AboveOf18 ?x)
            (BelowOf3 ?x)
          )
          (and
            (AboveOf16 ?x)
            (BelowOf6 ?x)
          )
          (and
            (BelowOf15 ?x)
            (AboveOf17 ?x)
          )
          (and
            (BelowOf16 ?x)
            (AboveOf18 ?x)
          )
          (and
            (BelowOf6 ?x)
            (AboveOf18 ?x)
          )
          (and
            (AboveOf18 ?x)
            (BelowOf11 ?x)
          )
          (and
            (BelowOf7 ?x)
            (AboveOf17 ?x)
          )
          (and
            (BelowOf17 ?x)
            (AboveOf19 ?x)
          )
          (and
            (BelowOf9 ?x)
            (AboveOf18 ?x)
          )
          (and
            (BelowOf14 ?x)
            (AboveOf16 ?x)
          )
          (and
            (BelowOf16 ?x)
            (AboveOf19 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf16 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf17 ?x)
          )
          (and
            (BelowOf9 ?x)
            (AboveOf19 ?x)
          )
          (and
            (AboveOf17 ?x)
            (BelowOf12 ?x)
          )
          (and
            (BelowOf3 ?x)
            (AboveOf19 ?x)
          )
          (and
            (BelowOf17 ?x)
            (AboveOf17 ?x)
          )
          (and
            (BelowOf13 ?x)
            (AboveOf16 ?x)
          )
          (and
            (AboveOf16 ?x)
            (BelowOf8 ?x)
          )
          (and
            (AboveOf17 ?x)
            (BelowOf6 ?x)
          )
          (and
            (BelowOf14 ?x)
            (AboveOf19 ?x)
          )
          (and
            (AboveOf17 ?x)
            (BelowOf11 ?x)
          )
          (and
            (BelowOf10 ?x)
            (AboveOf19 ?x)
          )
          (and
            (AboveOf16 ?x)
            (BelowOf12 ?x)
          )
          (and
            (AboveOf18 ?x)
            (BelowOf10 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf19 ?x)
          )
          (and
            (BelowOf13 ?x)
            (AboveOf17 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf18 ?x)
          )
          (and
            (BelowOf16 ?x)
            (AboveOf16 ?x)
          )
          (and
            (BelowOf11 ?x)
            (AboveOf19 ?x)
          )
          (and
            (BelowOf13 ?x)
            (AboveOf18 ?x)
          )
          (and
            (BelowOf9 ?x)
            (AboveOf17 ?x)
          )
          (and
            (BelowOf14 ?x)
            (AboveOf17 ?x)
          )
          (and
            (BelowOf13 ?x)
            (AboveOf19 ?x)
          )
          (and
            (BelowOf16 ?x)
            (AboveOf17 ?x)
          )
          (and
            (AboveOf18 ?x)
            (BelowOf6 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf17 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf19 ?x)
          )
          (and
            (AboveOf18 ?x)
            (BelowOf8 ?x)
          )
          (and
            (BelowOf7 ?x)
            (AboveOf19 ?x)
          )
          (and
            (BelowOf14 ?x)
            (AboveOf18 ?x)
          )
          (and
            (AboveOf17 ?x)
            (BelowOf10 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf17 ?x)
          )
          (and
            (AboveOf16 ?x)
            (BelowOf3 ?x)
          )
          (and
            (BelowOf15 ?x)
            (AboveOf16 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf19 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf18 ?x)
          )
          (and
            (AboveOf17 ?x)
            (BelowOf3 ?x)
          )
          (and
            (BelowOf7 ?x)
            (AboveOf18 ?x)
          )
          (and
            (AboveOf17 ?x)
            (BelowOf1 ?x)
          )
          (and
            (AboveOf16 ?x)
            (BelowOf17 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf17 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf16 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf19 ?x)
          )
          (and
            (AboveOf16 ?x)
            (BelowOf9 ?x)
          )
          (and
            (AboveOf19 ?x)
            (BelowOf3 ?x)
          )
          (and
            (BelowOf17 ?x)
            (AboveOf18 ?x)
          )
          (and
            (BelowOf7 ?x)
            (AboveOf16 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf18 ?x)
          )
          (and
            (AboveOf16 ?x)
            (BelowOf10 ?x)
          )
          (and
            (BelowOf6 ?x)
            (AboveOf19 ?x)
          )
          (and
            (BelowOf15 ?x)
            (AboveOf19 ?x)
          )
          (and
            (AboveOf17 ?x)
            (BelowOf8 ?x)
          )
          (and
            (AboveOf16 ?x)
            (BelowOf11 ?x)
          )
          (and
            (AboveOf16 ?x)
            (BelowOf1 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf16 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf18 ?x)
          )
          (and
            (AboveOf18 ?x)
            (BelowOf12 ?x)
          )
        )
        (and
          (Row17 ?x)
        )
      )
      (when
        (or
          (and
            (BelowOf15 ?x)
            (AboveOf18 ?x)
          )
          (and
            (BelowOf9 ?x)
            (AboveOf17 ?x)
          )
          (and
            (AboveOf19 ?x)
            (BelowOf8 ?x)
          )
          (and
            (AboveOf19 ?x)
            (BelowOf12 ?x)
          )
          (and
            (AboveOf18 ?x)
            (BelowOf3 ?x)
          )
          (and
            (BelowOf15 ?x)
            (AboveOf17 ?x)
          )
          (and
            (BelowOf16 ?x)
            (AboveOf18 ?x)
          )
          (and
            (BelowOf6 ?x)
            (AboveOf18 ?x)
          )
          (and
            (AboveOf18 ?x)
            (BelowOf11 ?x)
          )
          (and
            (BelowOf7 ?x)
            (AboveOf17 ?x)
          )
          (and
            (AboveOf17 ?x)
            (BelowOf12 ?x)
          )
          (and
            (BelowOf9 ?x)
            (AboveOf18 ?x)
          )
          (and
            (BelowOf14 ?x)
            (AboveOf18 ?x)
          )
          (and
            (BelowOf16 ?x)
            (AboveOf19 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf18 ?x)
          )
          (and
            (BelowOf9 ?x)
            (AboveOf19 ?x)
          )
          (and
            (BelowOf17 ?x)
            (AboveOf19 ?x)
          )
          (and
            (BelowOf3 ?x)
            (AboveOf19 ?x)
          )
          (and
            (BelowOf17 ?x)
            (AboveOf17 ?x)
          )
          (and
            (AboveOf17 ?x)
            (BelowOf6 ?x)
          )
          (and
            (BelowOf14 ?x)
            (AboveOf19 ?x)
          )
          (and
            (AboveOf17 ?x)
            (BelowOf11 ?x)
          )
          (and
            (BelowOf10 ?x)
            (AboveOf19 ?x)
          )
          (and
            (BelowOf13 ?x)
            (AboveOf17 ?x)
          )
          (and
            (AboveOf18 ?x)
            (BelowOf10 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf19 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf18 ?x)
          )
          (and
            (BelowOf11 ?x)
            (AboveOf19 ?x)
          )
          (and
            (BelowOf13 ?x)
            (AboveOf18 ?x)
          )
          (and
            (AboveOf18 ?x)
            (BelowOf18 ?x)
          )
          (and
            (BelowOf14 ?x)
            (AboveOf17 ?x)
          )
          (and
            (BelowOf13 ?x)
            (AboveOf19 ?x)
          )
          (and
            (BelowOf16 ?x)
            (AboveOf17 ?x)
          )
          (and
            (AboveOf18 ?x)
            (BelowOf6 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf17 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf19 ?x)
          )
          (and
            (AboveOf18 ?x)
            (BelowOf8 ?x)
          )
          (and
            (AboveOf17 ?x)
            (BelowOf18 ?x)
          )
          (and
            (BelowOf7 ?x)
            (AboveOf19 ?x)
          )
          (and
            (AboveOf17 ?x)
            (BelowOf10 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf17 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf19 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf18 ?x)
          )
          (and
            (AboveOf17 ?x)
            (BelowOf3 ?x)
          )
          (and
            (BelowOf7 ?x)
            (AboveOf18 ?x)
          )
          (and
            (AboveOf17 ?x)
            (BelowOf1 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf19 ?x)
          )
          (and
            (AboveOf19 ?x)
            (BelowOf3 ?x)
          )
          (and
            (BelowOf17 ?x)
            (AboveOf18 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf18 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf17 ?x)
          )
          (and
            (BelowOf6 ?x)
            (AboveOf19 ?x)
          )
          (and
            (BelowOf15 ?x)
            (AboveOf19 ?x)
          )
          (and
            (AboveOf17 ?x)
            (BelowOf8 ?x)
          )
          (and
            (BelowOf18 ?x)
            (AboveOf19 ?x)
          )
          (and
            (AboveOf18 ?x)
            (BelowOf12 ?x)
          )
        )
        (and
          (Row18 ?x)
        )
      )
      (when
        (or
          (and
            (BelowOf15 ?x)
            (AboveOf18 ?x)
          )
          (and
            (AboveOf19 ?x)
            (BelowOf12 ?x)
          )
          (and
            (AboveOf18 ?x)
            (BelowOf3 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf19 ?x)
          )
          (and
            (BelowOf16 ?x)
            (AboveOf18 ?x)
          )
          (and
            (BelowOf6 ?x)
            (AboveOf18 ?x)
          )
          (and
            (AboveOf18 ?x)
            (BelowOf11 ?x)
          )
          (and
            (BelowOf17 ?x)
            (AboveOf19 ?x)
          )
          (and
            (BelowOf9 ?x)
            (AboveOf18 ?x)
          )
          (and
            (BelowOf14 ?x)
            (AboveOf18 ?x)
          )
          (and
            (AboveOf18 ?x)
            (BelowOf10 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf18 ?x)
          )
          (and
            (BelowOf9 ?x)
            (AboveOf19 ?x)
          )
          (and
            (BelowOf3 ?x)
            (AboveOf19 ?x)
          )
          (and
            (BelowOf19 ?x)
            (AboveOf19 ?x)
          )
          (and
            (AboveOf19 ?x)
            (BelowOf8 ?x)
          )
          (and
            (BelowOf14 ?x)
            (AboveOf19 ?x)
          )
          (and
            (BelowOf10 ?x)
            (AboveOf19 ?x)
          )
          (and
            (BelowOf16 ?x)
            (AboveOf19 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf19 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf18 ?x)
          )
          (and
            (BelowOf11 ?x)
            (AboveOf19 ?x)
          )
          (and
            (BelowOf13 ?x)
            (AboveOf18 ?x)
          )
          (and
            (AboveOf18 ?x)
            (BelowOf18 ?x)
          )
          (and
            (BelowOf13 ?x)
            (AboveOf19 ?x)
          )
          (and
            (AboveOf19 ?x)
            (BelowOf3 ?x)
          )
          (and
            (AboveOf18 ?x)
            (BelowOf8 ?x)
          )
          (and
            (AboveOf18 ?x)
            (BelowOf19 ?x)
          )
          (and
            (BelowOf7 ?x)
            (AboveOf19 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf19 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf18 ?x)
          )
          (and
            (BelowOf7 ?x)
            (AboveOf18 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf19 ?x)
          )
          (and
            (BelowOf17 ?x)
            (AboveOf18 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf18 ?x)
          )
          (and
            (BelowOf6 ?x)
            (AboveOf19 ?x)
          )
          (and
            (BelowOf15 ?x)
            (AboveOf19 ?x)
          )
          (and
            (BelowOf18 ?x)
            (AboveOf19 ?x)
          )
          (and
            (AboveOf18 ?x)
            (BelowOf12 ?x)
          )
        )
        (and
          (Row19 ?x)
        )
      )
    )
  )
  (:action moveRight
    :parameters ( ?x )
    :precondition ( and
      (not (CheckConsistency))
      (not (Error))
      (or
        (LeftOf3 ?x)
        (RightOf7 ?x)
        (RightOf8 ?x)
        (RightOf15 ?x)
        (LeftOf2 ?x)
        (LeftOf9 ?x)
        (RightOf2 ?x)
        (RightOf10 ?x)
        (LeftOf8 ?x)
        (RightOf12 ?x)
        (LeftOf14 ?x)
        (LeftOf5 ?x)
        (RightOf1 ?x)
        (LeftOf12 ?x)
        (LeftOf19 ?x)
        (Columns ?x)
        (RightOf13 ?x)
        (RightOf14 ?x)
        (LeftOf1 ?x)
        (LeftOf20 ?x)
        (RightOf6 ?x)
        (RightOf9 ?x)
        (LeftOf6 ?x)
        (RightOf11 ?x)
        (RightOf17 ?x)
        (LeftOf11 ?x)
        (RightOf19 ?x)
        (RightOf5 ?x)
        (LeftOf10 ?x)
        (LeftOf15 ?x)
        (LeftOf4 ?x)
        (RightOf18 ?x)
        (LeftOf16 ?x)
        (RightOf16 ?x)
        (RightOf4 ?x)
        (RightOf0 ?x)
        (LeftOf7 ?x)
        (LeftOf13 ?x)
        (LeftOf17 ?x)
        (LeftOf18 ?x)
        (RightOf3 ?x)
      )
    )
    :effect ( and
      (CheckConsistency)
      (when
        (or
          (RightOf6 ?x)
          (RightOf15 ?x)
          (RightOf11 ?x)
          (RightOf7 ?x)
          (RightOf18 ?x)
          (RightOf16 ?x)
          (RightOf1 ?x)
          (RightOf4 ?x)
          (RightOf0 ?x)
          (RightOf12 ?x)
          (RightOf13 ?x)
          (RightOf14 ?x)
          (RightOf17 ?x)
          (RightOf2 ?x)
          (RightOf19 ?x)
          (RightOf9 ?x)
          (RightOf5 ?x)
          (RightOf10 ?x)
          (RightOf8 ?x)
          (RightOf3 ?x)
        )
        (and
          (RightOf1 ?x)
        )
      )
      (when
        (or
          (RightOf6 ?x)
          (RightOf15 ?x)
          (RightOf11 ?x)
          (RightOf7 ?x)
          (RightOf18 ?x)
          (RightOf16 ?x)
          (RightOf1 ?x)
          (RightOf4 ?x)
          (RightOf10 ?x)
          (RightOf12 ?x)
          (RightOf13 ?x)
          (RightOf14 ?x)
          (RightOf17 ?x)
          (RightOf2 ?x)
          (RightOf19 ?x)
          (RightOf9 ?x)
          (RightOf5 ?x)
          (RightOf8 ?x)
          (RightOf3 ?x)
        )
        (and
          (RightOf2 ?x)
        )
      )
      (when
        (or
          (RightOf6 ?x)
          (RightOf15 ?x)
          (RightOf11 ?x)
          (RightOf7 ?x)
          (RightOf18 ?x)
          (RightOf16 ?x)
          (RightOf8 ?x)
          (RightOf4 ?x)
          (RightOf10 ?x)
          (RightOf12 ?x)
          (RightOf13 ?x)
          (RightOf14 ?x)
          (RightOf17 ?x)
          (RightOf2 ?x)
          (RightOf19 ?x)
          (RightOf9 ?x)
          (RightOf5 ?x)
          (RightOf3 ?x)
        )
        (and
          (RightOf3 ?x)
        )
      )
      (when
        (or
          (RightOf6 ?x)
          (RightOf15 ?x)
          (RightOf11 ?x)
          (RightOf18 ?x)
          (RightOf16 ?x)
          (RightOf8 ?x)
          (RightOf4 ?x)
          (RightOf10 ?x)
          (RightOf12 ?x)
          (RightOf13 ?x)
          (RightOf14 ?x)
          (RightOf17 ?x)
          (RightOf19 ?x)
          (RightOf9 ?x)
          (RightOf5 ?x)
          (RightOf3 ?x)
          (RightOf7 ?x)
        )
        (and
          (RightOf4 ?x)
        )
      )
      (when
        (or
          (RightOf6 ?x)
          (RightOf15 ?x)
          (RightOf11 ?x)
          (RightOf18 ?x)
          (RightOf16 ?x)
          (RightOf8 ?x)
          (RightOf4 ?x)
          (RightOf10 ?x)
          (RightOf12 ?x)
          (RightOf13 ?x)
          (RightOf14 ?x)
          (RightOf17 ?x)
          (RightOf19 ?x)
          (RightOf9 ?x)
          (RightOf5 ?x)
          (RightOf7 ?x)
        )
        (and
          (RightOf5 ?x)
        )
      )
      (when
        (or
          (RightOf6 ?x)
          (RightOf15 ?x)
          (RightOf11 ?x)
          (RightOf18 ?x)
          (RightOf16 ?x)
          (RightOf8 ?x)
          (RightOf10 ?x)
          (RightOf12 ?x)
          (RightOf13 ?x)
          (RightOf14 ?x)
          (RightOf17 ?x)
          (RightOf19 ?x)
          (RightOf9 ?x)
          (RightOf5 ?x)
          (RightOf7 ?x)
        )
        (and
          (RightOf6 ?x)
        )
      )
      (when
        (or
          (RightOf6 ?x)
          (RightOf15 ?x)
          (RightOf11 ?x)
          (RightOf18 ?x)
          (RightOf16 ?x)
          (RightOf8 ?x)
          (RightOf10 ?x)
          (RightOf12 ?x)
          (RightOf13 ?x)
          (RightOf14 ?x)
          (RightOf17 ?x)
          (RightOf19 ?x)
          (RightOf9 ?x)
          (RightOf7 ?x)
        )
        (and
          (RightOf7 ?x)
        )
      )
      (when
        (or
          (RightOf11 ?x)
          (RightOf18 ?x)
          (RightOf7 ?x)
          (RightOf16 ?x)
          (RightOf12 ?x)
          (RightOf8 ?x)
          (RightOf10 ?x)
          (RightOf15 ?x)
          (RightOf13 ?x)
          (RightOf14 ?x)
          (RightOf17 ?x)
          (RightOf19 ?x)
          (RightOf9 ?x)
        )
        (and
          (RightOf8 ?x)
        )
      )
      (when
        (or
          (RightOf11 ?x)
          (RightOf18 ?x)
          (RightOf12 ?x)
          (RightOf16 ?x)
          (RightOf8 ?x)
          (RightOf10 ?x)
          (RightOf15 ?x)
          (RightOf13 ?x)
          (RightOf14 ?x)
          (RightOf17 ?x)
          (RightOf19 ?x)
          (RightOf9 ?x)
        )
        (and
          (RightOf9 ?x)
        )
      )
      (when
        (or
          (RightOf11 ?x)
          (RightOf18 ?x)
          (RightOf12 ?x)
          (RightOf16 ?x)
          (RightOf10 ?x)
          (RightOf15 ?x)
          (RightOf13 ?x)
          (RightOf14 ?x)
          (RightOf17 ?x)
          (RightOf9 ?x)
          (RightOf19 ?x)
        )
        (and
          (RightOf10 ?x)
        )
      )
      (when
        (or
          (RightOf11 ?x)
          (RightOf19 ?x)
          (RightOf12 ?x)
          (RightOf16 ?x)
          (RightOf10 ?x)
          (RightOf15 ?x)
          (RightOf13 ?x)
          (RightOf14 ?x)
          (RightOf17 ?x)
          (RightOf18 ?x)
        )
        (and
          (RightOf11 ?x)
        )
      )
      (when
        (or
          (RightOf11 ?x)
          (RightOf19 ?x)
          (RightOf12 ?x)
          (RightOf16 ?x)
          (RightOf15 ?x)
          (RightOf13 ?x)
          (RightOf14 ?x)
          (RightOf17 ?x)
          (RightOf18 ?x)
        )
        (and
          (RightOf12 ?x)
        )
      )
      (when
        (or
          (RightOf15 ?x)
          (RightOf18 ?x)
          (RightOf16 ?x)
          (RightOf12 ?x)
          (RightOf13 ?x)
          (RightOf14 ?x)
          (RightOf17 ?x)
          (RightOf19 ?x)
        )
        (and
          (RightOf13 ?x)
        )
      )
      (when
        (or
          (RightOf18 ?x)
          (RightOf16 ?x)
          (RightOf15 ?x)
          (RightOf13 ?x)
          (RightOf14 ?x)
          (RightOf17 ?x)
          (RightOf19 ?x)
        )
        (and
          (RightOf14 ?x)
        )
      )
      (when
        (or
          (RightOf19 ?x)
          (RightOf16 ?x)
          (RightOf15 ?x)
          (RightOf14 ?x)
          (RightOf17 ?x)
          (RightOf18 ?x)
        )
        (and
          (RightOf15 ?x)
        )
      )
      (when
        (or
          (RightOf15 ?x)
          (RightOf19 ?x)
          (RightOf18 ?x)
          (RightOf17 ?x)
          (RightOf16 ?x)
        )
        (and
          (RightOf16 ?x)
        )
      )
      (when
        (or
          (RightOf19 ?x)
          (RightOf18 ?x)
          (RightOf17 ?x)
          (RightOf16 ?x)
        )
        (and
          (RightOf17 ?x)
        )
      )
      (when
        (or
          (RightOf19 ?x)
          (RightOf18 ?x)
          (RightOf17 ?x)
        )
        (and
          (RightOf18 ?x)
        )
      )
      (when
        (or
          (RightOf19 ?x)
          (RightOf18 ?x)
        )
        (and
          (RightOf19 ?x)
        )
      )
      (when
        (LeftOf1 ?x)
        (and
          (LeftOf2 ?x)
          (not (LeftOf1 ?x))
        )
      )
      (when
        (or
          (LeftOf2 ?x)
          (LeftOf1 ?x)
        )
        (and
          (LeftOf3 ?x)
          (not (LeftOf2 ?x))
        )
      )
      (when
        (or
          (LeftOf2 ?x)
          (LeftOf3 ?x)
          (LeftOf1 ?x)
        )
        (and
          (LeftOf4 ?x)
          (not (LeftOf3 ?x))
        )
      )
      (when
        (or
          (LeftOf2 ?x)
          (LeftOf4 ?x)
          (LeftOf3 ?x)
          (LeftOf1 ?x)
        )
        (and
          (LeftOf5 ?x)
          (not (LeftOf4 ?x))
        )
      )
      (when
        (or
          (LeftOf2 ?x)
          (LeftOf4 ?x)
          (LeftOf3 ?x)
          (LeftOf1 ?x)
          (LeftOf5 ?x)
        )
        (and
          (LeftOf6 ?x)
          (not (LeftOf5 ?x))
        )
      )
      (when
        (or
          (LeftOf4 ?x)
          (LeftOf3 ?x)
          (LeftOf5 ?x)
          (LeftOf6 ?x)
          (LeftOf1 ?x)
          (LeftOf2 ?x)
        )
        (and
          (LeftOf7 ?x)
          (not (LeftOf6 ?x))
        )
      )
      (when
        (or
          (LeftOf4 ?x)
          (LeftOf3 ?x)
          (LeftOf5 ?x)
          (LeftOf6 ?x)
          (LeftOf1 ?x)
          (LeftOf7 ?x)
          (LeftOf2 ?x)
        )
        (and
          (LeftOf8 ?x)
          (not (LeftOf7 ?x))
        )
      )
      (when
        (or
          (LeftOf8 ?x)
          (LeftOf4 ?x)
          (LeftOf3 ?x)
          (LeftOf5 ?x)
          (LeftOf6 ?x)
          (LeftOf1 ?x)
          (LeftOf7 ?x)
          (LeftOf2 ?x)
        )
        (and
          (LeftOf9 ?x)
          (not (LeftOf8 ?x))
        )
      )
      (when
        (or
          (LeftOf8 ?x)
          (LeftOf4 ?x)
          (LeftOf3 ?x)
          (LeftOf5 ?x)
          (LeftOf6 ?x)
          (LeftOf1 ?x)
          (LeftOf7 ?x)
          (LeftOf2 ?x)
          (LeftOf9 ?x)
        )
        (and
          (LeftOf10 ?x)
          (not (LeftOf9 ?x))
        )
      )
      (when
        (or
          (LeftOf8 ?x)
          (LeftOf4 ?x)
          (LeftOf3 ?x)
          (LeftOf5 ?x)
          (LeftOf6 ?x)
          (LeftOf1 ?x)
          (LeftOf7 ?x)
          (LeftOf2 ?x)
          (LeftOf9 ?x)
          (LeftOf10 ?x)
        )
        (and
          (LeftOf11 ?x)
          (not (LeftOf10 ?x))
        )
      )
      (when
        (or
          (LeftOf8 ?x)
          (LeftOf4 ?x)
          (LeftOf3 ?x)
          (LeftOf5 ?x)
          (LeftOf6 ?x)
          (LeftOf1 ?x)
          (LeftOf7 ?x)
          (LeftOf2 ?x)
          (LeftOf9 ?x)
          (LeftOf11 ?x)
          (LeftOf10 ?x)
        )
        (and
          (LeftOf12 ?x)
          (not (LeftOf11 ?x))
        )
      )
      (when
        (or
          (LeftOf8 ?x)
          (LeftOf4 ?x)
          (LeftOf3 ?x)
          (LeftOf5 ?x)
          (LeftOf6 ?x)
          (LeftOf12 ?x)
          (LeftOf1 ?x)
          (LeftOf7 ?x)
          (LeftOf2 ?x)
          (LeftOf9 ?x)
          (LeftOf11 ?x)
          (LeftOf10 ?x)
        )
        (and
          (LeftOf13 ?x)
          (not (LeftOf12 ?x))
        )
      )
      (when
        (or
          (LeftOf8 ?x)
          (LeftOf7 ?x)
          (LeftOf4 ?x)
          (LeftOf3 ?x)
          (LeftOf5 ?x)
          (LeftOf6 ?x)
          (LeftOf12 ?x)
          (LeftOf1 ?x)
          (LeftOf13 ?x)
          (LeftOf2 ?x)
          (LeftOf9 ?x)
          (LeftOf11 ?x)
          (LeftOf10 ?x)
        )
        (and
          (LeftOf14 ?x)
          (not (LeftOf13 ?x))
        )
      )
      (when
        (or
          (LeftOf8 ?x)
          (LeftOf7 ?x)
          (LeftOf4 ?x)
          (LeftOf3 ?x)
          (LeftOf14 ?x)
          (LeftOf5 ?x)
          (LeftOf6 ?x)
          (LeftOf12 ?x)
          (LeftOf1 ?x)
          (LeftOf13 ?x)
          (LeftOf2 ?x)
          (LeftOf9 ?x)
          (LeftOf11 ?x)
          (LeftOf10 ?x)
        )
        (and
          (LeftOf15 ?x)
          (not (LeftOf14 ?x))
        )
      )
      (when
        (or
          (LeftOf8 ?x)
          (LeftOf7 ?x)
          (LeftOf4 ?x)
          (LeftOf3 ?x)
          (LeftOf14 ?x)
          (LeftOf5 ?x)
          (LeftOf6 ?x)
          (LeftOf12 ?x)
          (LeftOf1 ?x)
          (LeftOf13 ?x)
          (LeftOf2 ?x)
          (LeftOf9 ?x)
          (LeftOf11 ?x)
          (LeftOf10 ?x)
          (LeftOf15 ?x)
        )
        (and
          (LeftOf16 ?x)
          (not (LeftOf15 ?x))
        )
      )
      (when
        (or
          (LeftOf8 ?x)
          (LeftOf7 ?x)
          (LeftOf4 ?x)
          (LeftOf3 ?x)
          (LeftOf14 ?x)
          (LeftOf16 ?x)
          (LeftOf5 ?x)
          (LeftOf6 ?x)
          (LeftOf12 ?x)
          (LeftOf1 ?x)
          (LeftOf13 ?x)
          (LeftOf2 ?x)
          (LeftOf9 ?x)
          (LeftOf11 ?x)
          (LeftOf10 ?x)
          (LeftOf15 ?x)
        )
        (and
          (LeftOf17 ?x)
          (not (LeftOf16 ?x))
        )
      )
      (when
        (or
          (LeftOf8 ?x)
          (LeftOf7 ?x)
          (LeftOf4 ?x)
          (LeftOf3 ?x)
          (LeftOf14 ?x)
          (LeftOf16 ?x)
          (LeftOf5 ?x)
          (LeftOf6 ?x)
          (LeftOf12 ?x)
          (LeftOf1 ?x)
          (LeftOf13 ?x)
          (LeftOf2 ?x)
          (LeftOf9 ?x)
          (LeftOf11 ?x)
          (LeftOf17 ?x)
          (LeftOf10 ?x)
          (LeftOf15 ?x)
        )
        (and
          (LeftOf18 ?x)
          (not (LeftOf17 ?x))
        )
      )
      (when
        (or
          (LeftOf8 ?x)
          (LeftOf7 ?x)
          (LeftOf4 ?x)
          (LeftOf3 ?x)
          (LeftOf14 ?x)
          (LeftOf16 ?x)
          (LeftOf5 ?x)
          (LeftOf6 ?x)
          (LeftOf12 ?x)
          (LeftOf1 ?x)
          (LeftOf13 ?x)
          (LeftOf2 ?x)
          (LeftOf9 ?x)
          (LeftOf11 ?x)
          (LeftOf17 ?x)
          (LeftOf18 ?x)
          (LeftOf10 ?x)
          (LeftOf15 ?x)
        )
        (and
          (LeftOf19 ?x)
          (not (LeftOf18 ?x))
        )
      )
      (when
        (or
          (LeftOf8 ?x)
          (LeftOf7 ?x)
          (LeftOf4 ?x)
          (LeftOf3 ?x)
          (LeftOf14 ?x)
          (LeftOf16 ?x)
          (LeftOf5 ?x)
          (LeftOf6 ?x)
          (LeftOf12 ?x)
          (LeftOf19 ?x)
          (LeftOf13 ?x)
          (LeftOf2 ?x)
          (LeftOf9 ?x)
          (LeftOf11 ?x)
          (LeftOf17 ?x)
          (LeftOf18 ?x)
          (LeftOf1 ?x)
          (LeftOf10 ?x)
          (LeftOf15 ?x)
        )
        (and
          (LeftOf20 ?x)
          (not (LeftOf19 ?x))
        )
      )
      (when
        (Column0 ?x)
        (and
          (Column1 ?x)
          (not (Column0 ?x))
        )
      )
      (when
        (Column1 ?x)
        (and
          (Column2 ?x)
          (not (Column1 ?x))
        )
      )
      (when
        (Column2 ?x)
        (and
          (Column3 ?x)
          (not (Column2 ?x))
        )
      )
      (when
        (Column3 ?x)
        (and
          (Column4 ?x)
          (not (Column3 ?x))
        )
      )
      (when
        (Column4 ?x)
        (and
          (Column5 ?x)
          (not (Column4 ?x))
        )
      )
      (when
        (Column5 ?x)
        (and
          (Column6 ?x)
          (not (Column5 ?x))
        )
      )
      (when
        (Column6 ?x)
        (and
          (Column7 ?x)
          (not (Column6 ?x))
        )
      )
      (when
        (Column7 ?x)
        (and
          (Column8 ?x)
          (not (Column7 ?x))
        )
      )
      (when
        (Column8 ?x)
        (and
          (Column9 ?x)
          (not (Column8 ?x))
        )
      )
      (when
        (Column9 ?x)
        (and
          (Column10 ?x)
          (not (Column9 ?x))
        )
      )
      (when
        (Column10 ?x)
        (and
          (Column11 ?x)
          (not (Column10 ?x))
        )
      )
      (when
        (Column11 ?x)
        (and
          (Column12 ?x)
          (not (Column11 ?x))
        )
      )
      (when
        (Column12 ?x)
        (and
          (Column13 ?x)
          (not (Column12 ?x))
        )
      )
      (when
        (Column13 ?x)
        (and
          (Column14 ?x)
          (not (Column13 ?x))
        )
      )
      (when
        (Column14 ?x)
        (and
          (Column15 ?x)
          (not (Column14 ?x))
        )
      )
      (when
        (Column15 ?x)
        (and
          (Column16 ?x)
          (not (Column15 ?x))
        )
      )
      (when
        (Column16 ?x)
        (and
          (Column17 ?x)
          (not (Column16 ?x))
        )
      )
      (when
        (Column17 ?x)
        (and
          (Column18 ?x)
          (not (Column17 ?x))
        )
      )
      (when
        (Column18 ?x)
        (and
          (Column19 ?x)
          (not (Column18 ?x))
        )
      )
      (when
        (or
          (and
            (RightOf12 ?x)
            (LeftOf1 ?x)
          )
          (and
            (RightOf16 ?x)
            (LeftOf1 ?x)
          )
          (and
            (LeftOf1 ?x)
            (RightOf10 ?x)
          )
          (and
            (RightOf14 ?x)
            (LeftOf1 ?x)
          )
          (and
            (RightOf6 ?x)
            (LeftOf1 ?x)
          )
          (and
            (LeftOf1 ?x)
            (RightOf8 ?x)
          )
          (and
            (LeftOf1 ?x)
            (RightOf11 ?x)
          )
          (and
            (RightOf2 ?x)
            (LeftOf1 ?x)
          )
          (and
            (RightOf5 ?x)
            (LeftOf1 ?x)
          )
          (and
            (LeftOf1 ?x)
            (RightOf0 ?x)
          )
          (and
            (RightOf9 ?x)
            (LeftOf1 ?x)
          )
          (and
            (RightOf17 ?x)
            (LeftOf1 ?x)
          )
          (and
            (LeftOf1 ?x)
            (RightOf18 ?x)
          )
          (and
            (LeftOf1 ?x)
            (RightOf3 ?x)
          )
          (and
            (RightOf1 ?x)
            (LeftOf1 ?x)
          )
          (and
            (LeftOf1 ?x)
            (RightOf13 ?x)
          )
          (and
            (LeftOf1 ?x)
            (RightOf19 ?x)
          )
          (and
            (RightOf7 ?x)
            (LeftOf1 ?x)
          )
          (and
            (RightOf15 ?x)
            (LeftOf1 ?x)
          )
          (and
            (RightOf4 ?x)
            (LeftOf1 ?x)
          )
        )
        (and
          (Column1 ?x)
        )
      )
      (when
        (or
          (and
            (RightOf4 ?x)
            (LeftOf2 ?x)
          )
          (and
            (RightOf6 ?x)
            (LeftOf2 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf19 ?x)
          )
          (and
            (RightOf16 ?x)
            (LeftOf1 ?x)
          )
          (and
            (RightOf14 ?x)
            (LeftOf1 ?x)
          )
          (and
            (RightOf6 ?x)
            (LeftOf1 ?x)
          )
          (and
            (RightOf2 ?x)
            (LeftOf1 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf2 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf8 ?x)
          )
          (and
            (RightOf15 ?x)
            (LeftOf2 ?x)
          )
          (and
            (LeftOf1 ?x)
            (RightOf13 ?x)
          )
          (and
            (LeftOf1 ?x)
            (RightOf19 ?x)
          )
          (and
            (RightOf17 ?x)
            (LeftOf1 ?x)
          )
          (and
            (RightOf14 ?x)
            (LeftOf2 ?x)
          )
          (and
            (RightOf5 ?x)
            (LeftOf1 ?x)
          )
          (and
            (LeftOf1 ?x)
            (RightOf8 ?x)
          )
          (and
            (RightOf7 ?x)
            (LeftOf1 ?x)
          )
          (and
            (RightOf12 ?x)
            (LeftOf1 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf10 ?x)
          )
          (and
            (RightOf4 ?x)
            (LeftOf1 ?x)
          )
          (and
            (RightOf17 ?x)
            (LeftOf2 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf3 ?x)
          )
          (and
            (LeftOf1 ?x)
            (RightOf11 ?x)
          )
          (and
            (LeftOf1 ?x)
            (RightOf5 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf16 ?x)
          )
          (and
            (RightOf7 ?x)
            (LeftOf2 ?x)
          )
          (and
            (RightOf1 ?x)
            (LeftOf1 ?x)
          )
          (and
            (RightOf9 ?x)
            (LeftOf2 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf5 ?x)
          )
          (and
            (RightOf1 ?x)
            (LeftOf2 ?x)
          )
          (and
            (LeftOf1 ?x)
            (RightOf10 ?x)
          )
          (and
            (RightOf15 ?x)
            (LeftOf1 ?x)
          )
          (and
            (LeftOf1 ?x)
            (RightOf18 ?x)
          )
          (and
            (LeftOf1 ?x)
            (RightOf3 ?x)
          )
          (and
            (RightOf9 ?x)
            (LeftOf1 ?x)
          )
          (and
            (RightOf12 ?x)
            (LeftOf2 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf13 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf18 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf11 ?x)
          )
        )
        (and
          (Column2 ?x)
        )
      )
      (when
        (or
          (and
            (RightOf2 ?x)
            (LeftOf3 ?x)
          )
          (and
            (RightOf4 ?x)
            (LeftOf2 ?x)
          )
          (and
            (RightOf9 ?x)
            (LeftOf3 ?x)
          )
          (and
            (RightOf6 ?x)
            (LeftOf2 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf19 ?x)
          )
          (and
            (RightOf16 ?x)
            (LeftOf1 ?x)
          )
          (and
            (RightOf14 ?x)
            (LeftOf1 ?x)
          )
          (and
            (RightOf6 ?x)
            (LeftOf1 ?x)
          )
          (and
            (RightOf2 ?x)
            (LeftOf1 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf2 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf8 ?x)
          )
          (and
            (RightOf19 ?x)
            (LeftOf3 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf11 ?x)
          )
          (and
            (LeftOf1 ?x)
            (RightOf13 ?x)
          )
          (and
            (LeftOf1 ?x)
            (RightOf19 ?x)
          )
          (and
            (RightOf17 ?x)
            (LeftOf3 ?x)
          )
          (and
            (LeftOf3 ?x)
            (RightOf13 ?x)
          )
          (and
            (RightOf18 ?x)
            (LeftOf3 ?x)
          )
          (and
            (RightOf11 ?x)
            (LeftOf3 ?x)
          )
          (and
            (LeftOf3 ?x)
            (RightOf3 ?x)
          )
          (and
            (LeftOf1 ?x)
            (RightOf8 ?x)
          )
          (and
            (RightOf15 ?x)
            (LeftOf3 ?x)
          )
          (and
            (RightOf5 ?x)
            (LeftOf3 ?x)
          )
          (and
            (LeftOf1 ?x)
            (RightOf18 ?x)
          )
          (and
            (RightOf12 ?x)
            (LeftOf2 ?x)
          )
          (and
            (RightOf7 ?x)
            (LeftOf2 ?x)
          )
          (and
            (RightOf8 ?x)
            (LeftOf3 ?x)
          )
          (and
            (RightOf10 ?x)
            (LeftOf3 ?x)
          )
          (and
            (RightOf12 ?x)
            (LeftOf1 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf10 ?x)
          )
          (and
            (RightOf4 ?x)
            (LeftOf1 ?x)
          )
          (and
            (RightOf14 ?x)
            (LeftOf2 ?x)
          )
          (and
            (RightOf12 ?x)
            (LeftOf3 ?x)
          )
          (and
            (RightOf17 ?x)
            (LeftOf2 ?x)
          )
          (and
            (RightOf5 ?x)
            (LeftOf1 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf3 ?x)
          )
          (and
            (LeftOf1 ?x)
            (RightOf11 ?x)
          )
          (and
            (RightOf7 ?x)
            (LeftOf3 ?x)
          )
          (and
            (LeftOf1 ?x)
            (RightOf5 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf16 ?x)
          )
          (and
            (RightOf7 ?x)
            (LeftOf1 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf5 ?x)
          )
          (and
            (RightOf16 ?x)
            (LeftOf3 ?x)
          )
          (and
            (LeftOf1 ?x)
            (RightOf10 ?x)
          )
          (and
            (RightOf15 ?x)
            (LeftOf1 ?x)
          )
          (and
            (RightOf4 ?x)
            (LeftOf3 ?x)
          )
          (and
            (RightOf17 ?x)
            (LeftOf1 ?x)
          )
          (and
            (RightOf14 ?x)
            (LeftOf3 ?x)
          )
          (and
            (LeftOf1 ?x)
            (RightOf3 ?x)
          )
          (and
            (RightOf9 ?x)
            (LeftOf1 ?x)
          )
          (and
            (RightOf6 ?x)
            (LeftOf3 ?x)
          )
          (and
            (RightOf9 ?x)
            (LeftOf2 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf13 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf18 ?x)
          )
          (and
            (RightOf15 ?x)
            (LeftOf2 ?x)
          )
        )
        (and
          (Column3 ?x)
        )
      )
      (when
        (or
          (and
            (RightOf4 ?x)
            (LeftOf2 ?x)
          )
          (and
            (RightOf9 ?x)
            (LeftOf3 ?x)
          )
          (and
            (RightOf6 ?x)
            (LeftOf2 ?x)
          )
          (and
            (RightOf11 ?x)
            (LeftOf4 ?x)
          )
          (and
            (LeftOf4 ?x)
            (RightOf3 ?x)
          )
          (and
            (RightOf6 ?x)
            (LeftOf1 ?x)
          )
          (and
            (LeftOf4 ?x)
            (RightOf13 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf8 ?x)
          )
          (and
            (RightOf5 ?x)
            (LeftOf4 ?x)
          )
          (and
            (RightOf6 ?x)
            (LeftOf4 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf11 ?x)
          )
          (and
            (LeftOf1 ?x)
            (RightOf13 ?x)
          )
          (and
            (LeftOf1 ?x)
            (RightOf19 ?x)
          )
          (and
            (RightOf9 ?x)
            (LeftOf4 ?x)
          )
          (and
            (RightOf17 ?x)
            (LeftOf3 ?x)
          )
          (and
            (RightOf19 ?x)
            (LeftOf4 ?x)
          )
          (and
            (LeftOf3 ?x)
            (RightOf13 ?x)
          )
          (and
            (RightOf18 ?x)
            (LeftOf3 ?x)
          )
          (and
            (LeftOf4 ?x)
            (RightOf10 ?x)
          )
          (and
            (RightOf12 ?x)
            (LeftOf4 ?x)
          )
          (and
            (RightOf11 ?x)
            (LeftOf3 ?x)
          )
          (and
            (RightOf19 ?x)
            (LeftOf3 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf19 ?x)
          )
          (and
            (RightOf15 ?x)
            (LeftOf3 ?x)
          )
          (and
            (RightOf5 ?x)
            (LeftOf3 ?x)
          )
          (and
            (RightOf16 ?x)
            (LeftOf1 ?x)
          )
          (and
            (LeftOf1 ?x)
            (RightOf18 ?x)
          )
          (and
            (LeftOf4 ?x)
            (RightOf8 ?x)
          )
          (and
            (RightOf12 ?x)
            (LeftOf2 ?x)
          )
          (and
            (RightOf7 ?x)
            (LeftOf2 ?x)
          )
          (and
            (RightOf3 ?x)
            (LeftOf3 ?x)
          )
          (and
            (RightOf10 ?x)
            (LeftOf3 ?x)
          )
          (and
            (RightOf12 ?x)
            (LeftOf1 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf10 ?x)
          )
          (and
            (RightOf14 ?x)
            (LeftOf4 ?x)
          )
          (and
            (RightOf4 ?x)
            (LeftOf1 ?x)
          )
          (and
            (RightOf14 ?x)
            (LeftOf2 ?x)
          )
          (and
            (RightOf12 ?x)
            (LeftOf3 ?x)
          )
          (and
            (RightOf17 ?x)
            (LeftOf2 ?x)
          )
          (and
            (RightOf5 ?x)
            (LeftOf1 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf3 ?x)
          )
          (and
            (LeftOf1 ?x)
            (RightOf11 ?x)
          )
          (and
            (RightOf7 ?x)
            (LeftOf3 ?x)
          )
          (and
            (RightOf15 ?x)
            (LeftOf4 ?x)
          )
          (and
            (LeftOf1 ?x)
            (RightOf5 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf16 ?x)
          )
          (and
            (RightOf7 ?x)
            (LeftOf1 ?x)
          )
          (and
            (RightOf8 ?x)
            (LeftOf3 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf5 ?x)
          )
          (and
            (RightOf7 ?x)
            (LeftOf4 ?x)
          )
          (and
            (RightOf16 ?x)
            (LeftOf3 ?x)
          )
          (and
            (RightOf4 ?x)
            (LeftOf4 ?x)
          )
          (and
            (RightOf17 ?x)
            (LeftOf4 ?x)
          )
          (and
            (LeftOf1 ?x)
            (RightOf8 ?x)
          )
          (and
            (LeftOf1 ?x)
            (RightOf10 ?x)
          )
          (and
            (RightOf15 ?x)
            (LeftOf1 ?x)
          )
          (and
            (RightOf4 ?x)
            (LeftOf3 ?x)
          )
          (and
            (RightOf13 ?x)
            (LeftOf3 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf18 ?x)
          )
          (and
            (RightOf14 ?x)
            (LeftOf1 ?x)
          )
          (and
            (RightOf17 ?x)
            (LeftOf1 ?x)
          )
          (and
            (RightOf14 ?x)
            (LeftOf3 ?x)
          )
          (and
            (LeftOf1 ?x)
            (RightOf3 ?x)
          )
          (and
            (RightOf9 ?x)
            (LeftOf1 ?x)
          )
          (and
            (RightOf6 ?x)
            (LeftOf3 ?x)
          )
          (and
            (RightOf9 ?x)
            (LeftOf2 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf13 ?x)
          )
          (and
            (RightOf16 ?x)
            (LeftOf4 ?x)
          )
          (and
            (LeftOf4 ?x)
            (RightOf18 ?x)
          )
          (and
            (RightOf15 ?x)
            (LeftOf2 ?x)
          )
        )
        (and
          (Column4 ?x)
        )
      )
      (when
        (or
          (and
            (LeftOf5 ?x)
            (RightOf17 ?x)
          )
          (and
            (RightOf4 ?x)
            (LeftOf2 ?x)
          )
          (and
            (RightOf9 ?x)
            (LeftOf3 ?x)
          )
          (and
            (RightOf6 ?x)
            (LeftOf2 ?x)
          )
          (and
            (RightOf11 ?x)
            (LeftOf4 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf5 ?x)
          )
          (and
            (RightOf6 ?x)
            (LeftOf1 ?x)
          )
          (and
            (RightOf5 ?x)
            (LeftOf3 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf14 ?x)
          )
          (and
            (LeftOf4 ?x)
            (RightOf13 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf8 ?x)
          )
          (and
            (RightOf5 ?x)
            (LeftOf4 ?x)
          )
          (and
            (RightOf6 ?x)
            (LeftOf4 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf11 ?x)
          )
          (and
            (LeftOf1 ?x)
            (RightOf13 ?x)
          )
          (and
            (LeftOf1 ?x)
            (RightOf19 ?x)
          )
          (and
            (RightOf9 ?x)
            (LeftOf4 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf18 ?x)
          )
          (and
            (RightOf17 ?x)
            (LeftOf3 ?x)
          )
          (and
            (RightOf19 ?x)
            (LeftOf4 ?x)
          )
          (and
            (RightOf14 ?x)
            (LeftOf2 ?x)
          )
          (and
            (LeftOf3 ?x)
            (RightOf13 ?x)
          )
          (and
            (RightOf18 ?x)
            (LeftOf3 ?x)
          )
          (and
            (LeftOf4 ?x)
            (RightOf10 ?x)
          )
          (and
            (RightOf12 ?x)
            (LeftOf4 ?x)
          )
          (and
            (RightOf11 ?x)
            (LeftOf3 ?x)
          )
          (and
            (RightOf19 ?x)
            (LeftOf3 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf19 ?x)
          )
          (and
            (RightOf12 ?x)
            (LeftOf3 ?x)
          )
          (and
            (RightOf5 ?x)
            (LeftOf1 ?x)
          )
          (and
            (RightOf16 ?x)
            (LeftOf1 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf19 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf8 ?x)
          )
          (and
            (RightOf12 ?x)
            (LeftOf5 ?x)
          )
          (and
            (LeftOf4 ?x)
            (RightOf8 ?x)
          )
          (and
            (LeftOf1 ?x)
            (RightOf8 ?x)
          )
          (and
            (RightOf7 ?x)
            (LeftOf2 ?x)
          )
          (and
            (RightOf10 ?x)
            (LeftOf4 ?x)
          )
          (and
            (RightOf10 ?x)
            (LeftOf3 ?x)
          )
          (and
            (RightOf12 ?x)
            (LeftOf1 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf10 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf10 ?x)
          )
          (and
            (RightOf12 ?x)
            (LeftOf2 ?x)
          )
          (and
            (RightOf6 ?x)
            (LeftOf5 ?x)
          )
          (and
            (RightOf16 ?x)
            (LeftOf4 ?x)
          )
          (and
            (RightOf8 ?x)
            (LeftOf4 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf16 ?x)
          )
          (and
            (RightOf15 ?x)
            (LeftOf3 ?x)
          )
          (and
            (RightOf17 ?x)
            (LeftOf2 ?x)
          )
          (and
            (RightOf14 ?x)
            (LeftOf4 ?x)
          )
          (and
            (LeftOf1 ?x)
            (RightOf11 ?x)
          )
          (and
            (RightOf7 ?x)
            (LeftOf3 ?x)
          )
          (and
            (RightOf15 ?x)
            (LeftOf4 ?x)
          )
          (and
            (LeftOf1 ?x)
            (RightOf5 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf16 ?x)
          )
          (and
            (RightOf7 ?x)
            (LeftOf1 ?x)
          )
          (and
            (RightOf17 ?x)
            (LeftOf1 ?x)
          )
          (and
            (RightOf8 ?x)
            (LeftOf3 ?x)
          )
          (and
            (LeftOf1 ?x)
            (RightOf18 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf5 ?x)
          )
          (and
            (RightOf7 ?x)
            (LeftOf4 ?x)
          )
          (and
            (RightOf15 ?x)
            (LeftOf2 ?x)
          )
          (and
            (RightOf16 ?x)
            (LeftOf3 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf13 ?x)
          )
          (and
            (RightOf4 ?x)
            (LeftOf4 ?x)
          )
          (and
            (RightOf17 ?x)
            (LeftOf4 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf11 ?x)
          )
          (and
            (LeftOf1 ?x)
            (RightOf10 ?x)
          )
          (and
            (RightOf15 ?x)
            (LeftOf1 ?x)
          )
          (and
            (RightOf4 ?x)
            (LeftOf3 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf7 ?x)
          )
          (and
            (RightOf13 ?x)
            (LeftOf3 ?x)
          )
          (and
            (RightOf18 ?x)
            (LeftOf4 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf18 ?x)
          )
          (and
            (RightOf14 ?x)
            (LeftOf1 ?x)
          )
          (and
            (RightOf14 ?x)
            (LeftOf3 ?x)
          )
          (and
            (RightOf9 ?x)
            (LeftOf1 ?x)
          )
          (and
            (RightOf6 ?x)
            (LeftOf3 ?x)
          )
          (and
            (RightOf9 ?x)
            (LeftOf2 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf4 ?x)
          )
          (and
            (RightOf4 ?x)
            (LeftOf1 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf13 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf15 ?x)
          )
          (and
            (LeftOf4 ?x)
            (RightOf18 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf9 ?x)
          )
        )
        (and
          (Column5 ?x)
        )
      )
      (when
        (or
          (and
            (RightOf11 ?x)
            (LeftOf4 ?x)
          )
          (and
            (RightOf19 ?x)
            (LeftOf3 ?x)
          )
          (and
            (RightOf15 ?x)
            (LeftOf2 ?x)
          )
          (and
            (RightOf9 ?x)
            (LeftOf4 ?x)
          )
          (and
            (LeftOf3 ?x)
            (RightOf13 ?x)
          )
          (and
            (RightOf16 ?x)
            (LeftOf1 ?x)
          )
          (and
            (RightOf12 ?x)
            (LeftOf4 ?x)
          )
          (and
            (RightOf11 ?x)
            (LeftOf3 ?x)
          )
          (and
            (LeftOf1 ?x)
            (RightOf8 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf19 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf8 ?x)
          )
          (and
            (LeftOf4 ?x)
            (RightOf8 ?x)
          )
          (and
            (RightOf16 ?x)
            (LeftOf3 ?x)
          )
          (and
            (LeftOf1 ?x)
            (RightOf11 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf8 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf16 ?x)
          )
          (and
            (RightOf8 ?x)
            (LeftOf3 ?x)
          )
          (and
            (RightOf18 ?x)
            (LeftOf4 ?x)
          )
          (and
            (RightOf17 ?x)
            (LeftOf4 ?x)
          )
          (and
            (LeftOf1 ?x)
            (RightOf18 ?x)
          )
          (and
            (RightOf6 ?x)
            (LeftOf3 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf16 ?x)
          )
          (and
            (LeftOf4 ?x)
            (RightOf18 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf11 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf17 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf9 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf11 ?x)
          )
          (and
            (RightOf6 ?x)
            (LeftOf2 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf19 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf5 ?x)
          )
          (and
            (RightOf6 ?x)
            (LeftOf4 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf13 ?x)
          )
          (and
            (LeftOf1 ?x)
            (RightOf13 ?x)
          )
          (and
            (LeftOf1 ?x)
            (RightOf19 ?x)
          )
          (and
            (RightOf19 ?x)
            (LeftOf4 ?x)
          )
          (and
            (LeftOf4 ?x)
            (RightOf10 ?x)
          )
          (and
            (RightOf17 ?x)
            (LeftOf1 ?x)
          )
          (and
            (RightOf5 ?x)
            (LeftOf3 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf10 ?x)
          )
          (and
            (RightOf12 ?x)
            (LeftOf3 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf19 ?x)
          )
          (and
            (LeftOf1 ?x)
            (RightOf5 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf7 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf14 ?x)
          )
          (and
            (RightOf16 ?x)
            (LeftOf4 ?x)
          )
          (and
            (RightOf7 ?x)
            (LeftOf4 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf18 ?x)
          )
          (and
            (RightOf14 ?x)
            (LeftOf3 ?x)
          )
          (and
            (RightOf14 ?x)
            (LeftOf2 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf15 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf9 ?x)
          )
          (and
            (RightOf9 ?x)
            (LeftOf3 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf18 ?x)
          )
          (and
            (RightOf14 ?x)
            (LeftOf1 ?x)
          )
          (and
            (LeftOf4 ?x)
            (RightOf13 ?x)
          )
          (and
            (RightOf5 ?x)
            (LeftOf4 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf6 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf10 ?x)
          )
          (and
            (RightOf15 ?x)
            (LeftOf4 ?x)
          )
          (and
            (RightOf18 ?x)
            (LeftOf3 ?x)
          )
          (and
            (RightOf12 ?x)
            (LeftOf5 ?x)
          )
          (and
            (RightOf7 ?x)
            (LeftOf1 ?x)
          )
          (and
            (RightOf12 ?x)
            (LeftOf1 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf17 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf16 ?x)
          )
          (and
            (RightOf15 ?x)
            (LeftOf3 ?x)
          )
          (and
            (RightOf17 ?x)
            (LeftOf2 ?x)
          )
          (and
            (RightOf14 ?x)
            (LeftOf4 ?x)
          )
          (and
            (RightOf7 ?x)
            (LeftOf3 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf12 ?x)
          )
          (and
            (RightOf7 ?x)
            (LeftOf2 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf5 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf7 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf13 ?x)
          )
          (and
            (LeftOf1 ?x)
            (RightOf10 ?x)
          )
          (and
            (RightOf13 ?x)
            (LeftOf3 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf13 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf14 ?x)
          )
          (and
            (RightOf6 ?x)
            (LeftOf1 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf8 ?x)
          )
          (and
            (RightOf17 ?x)
            (LeftOf3 ?x)
          )
          (and
            (RightOf9 ?x)
            (LeftOf2 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf5 ?x)
          )
          (and
            (RightOf12 ?x)
            (LeftOf2 ?x)
          )
          (and
            (RightOf10 ?x)
            (LeftOf4 ?x)
          )
          (and
            (RightOf10 ?x)
            (LeftOf3 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf10 ?x)
          )
          (and
            (RightOf6 ?x)
            (LeftOf5 ?x)
          )
          (and
            (RightOf8 ?x)
            (LeftOf4 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf11 ?x)
          )
          (and
            (RightOf9 ?x)
            (LeftOf1 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf15 ?x)
          )
          (and
            (RightOf15 ?x)
            (LeftOf1 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf18 ?x)
          )
        )
        (and
          (Column6 ?x)
        )
      )
      (when
        (or
          (and
            (RightOf11 ?x)
            (LeftOf4 ?x)
          )
          (and
            (LeftOf7 ?x)
            (RightOf10 ?x)
          )
          (and
            (RightOf15 ?x)
            (LeftOf2 ?x)
          )
          (and
            (RightOf9 ?x)
            (LeftOf4 ?x)
          )
          (and
            (LeftOf3 ?x)
            (RightOf13 ?x)
          )
          (and
            (RightOf16 ?x)
            (LeftOf1 ?x)
          )
          (and
            (RightOf12 ?x)
            (LeftOf4 ?x)
          )
          (and
            (RightOf11 ?x)
            (LeftOf3 ?x)
          )
          (and
            (LeftOf1 ?x)
            (RightOf8 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf19 ?x)
          )
          (and
            (RightOf14 ?x)
            (LeftOf7 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf8 ?x)
          )
          (and
            (LeftOf7 ?x)
            (RightOf16 ?x)
          )
          (and
            (LeftOf4 ?x)
            (RightOf8 ?x)
          )
          (and
            (RightOf16 ?x)
            (LeftOf3 ?x)
          )
          (and
            (LeftOf1 ?x)
            (RightOf11 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf8 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf16 ?x)
          )
          (and
            (RightOf10 ?x)
            (LeftOf4 ?x)
          )
          (and
            (RightOf18 ?x)
            (LeftOf4 ?x)
          )
          (and
            (RightOf17 ?x)
            (LeftOf4 ?x)
          )
          (and
            (LeftOf1 ?x)
            (RightOf18 ?x)
          )
          (and
            (RightOf6 ?x)
            (LeftOf3 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf16 ?x)
          )
          (and
            (LeftOf4 ?x)
            (RightOf18 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf11 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf17 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf9 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf11 ?x)
          )
          (and
            (RightOf6 ?x)
            (LeftOf2 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf19 ?x)
          )
          (and
            (LeftOf7 ?x)
            (RightOf19 ?x)
          )
          (and
            (RightOf6 ?x)
            (LeftOf4 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf13 ?x)
          )
          (and
            (LeftOf1 ?x)
            (RightOf13 ?x)
          )
          (and
            (LeftOf1 ?x)
            (RightOf19 ?x)
          )
          (and
            (RightOf19 ?x)
            (LeftOf4 ?x)
          )
          (and
            (LeftOf4 ?x)
            (RightOf10 ?x)
          )
          (and
            (RightOf17 ?x)
            (LeftOf1 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf10 ?x)
          )
          (and
            (RightOf12 ?x)
            (LeftOf3 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf19 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf7 ?x)
          )
          (and
            (LeftOf7 ?x)
            (RightOf17 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf14 ?x)
          )
          (and
            (RightOf16 ?x)
            (LeftOf4 ?x)
          )
          (and
            (RightOf7 ?x)
            (LeftOf4 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf18 ?x)
          )
          (and
            (LeftOf7 ?x)
            (RightOf8 ?x)
          )
          (and
            (RightOf14 ?x)
            (LeftOf3 ?x)
          )
          (and
            (LeftOf7 ?x)
            (RightOf11 ?x)
          )
          (and
            (RightOf14 ?x)
            (LeftOf2 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf15 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf9 ?x)
          )
          (and
            (RightOf15 ?x)
            (LeftOf7 ?x)
          )
          (and
            (RightOf9 ?x)
            (LeftOf3 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf18 ?x)
          )
          (and
            (RightOf14 ?x)
            (LeftOf1 ?x)
          )
          (and
            (LeftOf4 ?x)
            (RightOf13 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf10 ?x)
          )
          (and
            (RightOf12 ?x)
            (LeftOf6 ?x)
          )
          (and
            (RightOf6 ?x)
            (LeftOf7 ?x)
          )
          (and
            (RightOf15 ?x)
            (LeftOf4 ?x)
          )
          (and
            (RightOf18 ?x)
            (LeftOf3 ?x)
          )
          (and
            (RightOf12 ?x)
            (LeftOf5 ?x)
          )
          (and
            (RightOf12 ?x)
            (LeftOf7 ?x)
          )
          (and
            (RightOf7 ?x)
            (LeftOf1 ?x)
          )
          (and
            (RightOf12 ?x)
            (LeftOf1 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf17 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf16 ?x)
          )
          (and
            (RightOf15 ?x)
            (LeftOf3 ?x)
          )
          (and
            (RightOf17 ?x)
            (LeftOf2 ?x)
          )
          (and
            (RightOf14 ?x)
            (LeftOf4 ?x)
          )
          (and
            (RightOf7 ?x)
            (LeftOf3 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf12 ?x)
          )
          (and
            (RightOf7 ?x)
            (LeftOf2 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf7 ?x)
          )
          (and
            (LeftOf7 ?x)
            (RightOf18 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf13 ?x)
          )
          (and
            (LeftOf1 ?x)
            (RightOf10 ?x)
          )
          (and
            (RightOf13 ?x)
            (LeftOf3 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf13 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf14 ?x)
          )
          (and
            (RightOf6 ?x)
            (LeftOf1 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf8 ?x)
          )
          (and
            (RightOf17 ?x)
            (LeftOf3 ?x)
          )
          (and
            (RightOf6 ?x)
            (LeftOf6 ?x)
          )
          (and
            (RightOf9 ?x)
            (LeftOf7 ?x)
          )
          (and
            (RightOf9 ?x)
            (LeftOf2 ?x)
          )
          (and
            (RightOf12 ?x)
            (LeftOf2 ?x)
          )
          (and
            (RightOf8 ?x)
            (LeftOf3 ?x)
          )
          (and
            (RightOf10 ?x)
            (LeftOf3 ?x)
          )
          (and
            (RightOf19 ?x)
            (LeftOf3 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf10 ?x)
          )
          (and
            (RightOf6 ?x)
            (LeftOf5 ?x)
          )
          (and
            (RightOf8 ?x)
            (LeftOf4 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf11 ?x)
          )
          (and
            (RightOf9 ?x)
            (LeftOf1 ?x)
          )
          (and
            (LeftOf7 ?x)
            (RightOf13 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf15 ?x)
          )
          (and
            (RightOf7 ?x)
            (LeftOf7 ?x)
          )
          (and
            (RightOf15 ?x)
            (LeftOf1 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf18 ?x)
          )
        )
        (and
          (Column7 ?x)
        )
      )
      (when
        (or
          (and
            (RightOf11 ?x)
            (LeftOf4 ?x)
          )
          (and
            (LeftOf7 ?x)
            (RightOf10 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf11 ?x)
          )
          (and
            (RightOf9 ?x)
            (LeftOf4 ?x)
          )
          (and
            (LeftOf8 ?x)
            (RightOf19 ?x)
          )
          (and
            (LeftOf3 ?x)
            (RightOf13 ?x)
          )
          (and
            (RightOf16 ?x)
            (LeftOf1 ?x)
          )
          (and
            (RightOf12 ?x)
            (LeftOf4 ?x)
          )
          (and
            (RightOf11 ?x)
            (LeftOf3 ?x)
          )
          (and
            (LeftOf1 ?x)
            (RightOf8 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf19 ?x)
          )
          (and
            (RightOf14 ?x)
            (LeftOf7 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf8 ?x)
          )
          (and
            (LeftOf7 ?x)
            (RightOf16 ?x)
          )
          (and
            (LeftOf4 ?x)
            (RightOf8 ?x)
          )
          (and
            (RightOf16 ?x)
            (LeftOf3 ?x)
          )
          (and
            (LeftOf8 ?x)
            (RightOf8 ?x)
          )
          (and
            (LeftOf1 ?x)
            (RightOf11 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf8 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf16 ?x)
          )
          (and
            (RightOf10 ?x)
            (LeftOf4 ?x)
          )
          (and
            (RightOf18 ?x)
            (LeftOf4 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf10 ?x)
          )
          (and
            (RightOf17 ?x)
            (LeftOf4 ?x)
          )
          (and
            (LeftOf1 ?x)
            (RightOf18 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf7 ?x)
          )
          (and
            (RightOf9 ?x)
            (LeftOf2 ?x)
          )
          (and
            (LeftOf4 ?x)
            (RightOf18 ?x)
          )
          (and
            (RightOf15 ?x)
            (LeftOf2 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf17 ?x)
          )
          (and
            (LeftOf8 ?x)
            (RightOf17 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf9 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf11 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf19 ?x)
          )
          (and
            (LeftOf7 ?x)
            (RightOf19 ?x)
          )
          (and
            (LeftOf8 ?x)
            (RightOf11 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf13 ?x)
          )
          (and
            (LeftOf1 ?x)
            (RightOf13 ?x)
          )
          (and
            (LeftOf1 ?x)
            (RightOf19 ?x)
          )
          (and
            (RightOf19 ?x)
            (LeftOf4 ?x)
          )
          (and
            (LeftOf4 ?x)
            (RightOf10 ?x)
          )
          (and
            (RightOf19 ?x)
            (LeftOf3 ?x)
          )
          (and
            (RightOf12 ?x)
            (LeftOf8 ?x)
          )
          (and
            (RightOf12 ?x)
            (LeftOf3 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf19 ?x)
          )
          (and
            (LeftOf7 ?x)
            (RightOf17 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf14 ?x)
          )
          (and
            (LeftOf8 ?x)
            (RightOf14 ?x)
          )
          (and
            (RightOf16 ?x)
            (LeftOf4 ?x)
          )
          (and
            (RightOf7 ?x)
            (LeftOf4 ?x)
          )
          (and
            (LeftOf8 ?x)
            (RightOf18 ?x)
          )
          (and
            (LeftOf8 ?x)
            (RightOf7 ?x)
          )
          (and
            (RightOf9 ?x)
            (LeftOf8 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf18 ?x)
          )
          (and
            (LeftOf7 ?x)
            (RightOf8 ?x)
          )
          (and
            (RightOf14 ?x)
            (LeftOf3 ?x)
          )
          (and
            (LeftOf7 ?x)
            (RightOf11 ?x)
          )
          (and
            (RightOf14 ?x)
            (LeftOf2 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf15 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf9 ?x)
          )
          (and
            (RightOf15 ?x)
            (LeftOf7 ?x)
          )
          (and
            (RightOf9 ?x)
            (LeftOf3 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf18 ?x)
          )
          (and
            (RightOf14 ?x)
            (LeftOf1 ?x)
          )
          (and
            (LeftOf4 ?x)
            (RightOf13 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf10 ?x)
          )
          (and
            (RightOf12 ?x)
            (LeftOf6 ?x)
          )
          (and
            (RightOf15 ?x)
            (LeftOf4 ?x)
          )
          (and
            (RightOf18 ?x)
            (LeftOf3 ?x)
          )
          (and
            (RightOf15 ?x)
            (LeftOf8 ?x)
          )
          (and
            (RightOf12 ?x)
            (LeftOf5 ?x)
          )
          (and
            (RightOf12 ?x)
            (LeftOf7 ?x)
          )
          (and
            (RightOf7 ?x)
            (LeftOf2 ?x)
          )
          (and
            (RightOf12 ?x)
            (LeftOf1 ?x)
          )
          (and
            (LeftOf7 ?x)
            (RightOf7 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf17 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf16 ?x)
          )
          (and
            (LeftOf8 ?x)
            (RightOf16 ?x)
          )
          (and
            (RightOf17 ?x)
            (LeftOf2 ?x)
          )
          (and
            (RightOf14 ?x)
            (LeftOf4 ?x)
          )
          (and
            (RightOf7 ?x)
            (LeftOf3 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf12 ?x)
          )
          (and
            (RightOf7 ?x)
            (LeftOf1 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf7 ?x)
          )
          (and
            (LeftOf7 ?x)
            (RightOf18 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf13 ?x)
          )
          (and
            (LeftOf8 ?x)
            (RightOf13 ?x)
          )
          (and
            (LeftOf1 ?x)
            (RightOf10 ?x)
          )
          (and
            (RightOf13 ?x)
            (LeftOf3 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf13 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf14 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf8 ?x)
          )
          (and
            (RightOf17 ?x)
            (LeftOf3 ?x)
          )
          (and
            (RightOf9 ?x)
            (LeftOf7 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf16 ?x)
          )
          (and
            (RightOf15 ?x)
            (LeftOf3 ?x)
          )
          (and
            (RightOf12 ?x)
            (LeftOf2 ?x)
          )
          (and
            (RightOf8 ?x)
            (LeftOf3 ?x)
          )
          (and
            (RightOf10 ?x)
            (LeftOf3 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf10 ?x)
          )
          (and
            (LeftOf8 ?x)
            (RightOf10 ?x)
          )
          (and
            (RightOf8 ?x)
            (LeftOf4 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf11 ?x)
          )
          (and
            (RightOf9 ?x)
            (LeftOf1 ?x)
          )
          (and
            (LeftOf7 ?x)
            (RightOf13 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf15 ?x)
          )
          (and
            (RightOf17 ?x)
            (LeftOf1 ?x)
          )
          (and
            (RightOf15 ?x)
            (LeftOf1 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf18 ?x)
          )
        )
        (and
          (Column8 ?x)
        )
      )
      (when
        (or
          (and
            (RightOf14 ?x)
            (LeftOf9 ?x)
          )
          (and
            (LeftOf7 ?x)
            (RightOf10 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf11 ?x)
          )
          (and
            (RightOf9 ?x)
            (LeftOf4 ?x)
          )
          (and
            (LeftOf8 ?x)
            (RightOf19 ?x)
          )
          (and
            (LeftOf3 ?x)
            (RightOf13 ?x)
          )
          (and
            (RightOf11 ?x)
            (LeftOf4 ?x)
          )
          (and
            (RightOf12 ?x)
            (LeftOf4 ?x)
          )
          (and
            (RightOf17 ?x)
            (LeftOf9 ?x)
          )
          (and
            (LeftOf1 ?x)
            (RightOf8 ?x)
          )
          (and
            (RightOf16 ?x)
            (LeftOf1 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf19 ?x)
          )
          (and
            (RightOf14 ?x)
            (LeftOf7 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf8 ?x)
          )
          (and
            (LeftOf7 ?x)
            (RightOf16 ?x)
          )
          (and
            (RightOf16 ?x)
            (LeftOf3 ?x)
          )
          (and
            (LeftOf8 ?x)
            (RightOf8 ?x)
          )
          (and
            (LeftOf1 ?x)
            (RightOf11 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf8 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf16 ?x)
          )
          (and
            (RightOf10 ?x)
            (LeftOf4 ?x)
          )
          (and
            (RightOf18 ?x)
            (LeftOf4 ?x)
          )
          (and
            (LeftOf9 ?x)
            (RightOf19 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf10 ?x)
          )
          (and
            (RightOf17 ?x)
            (LeftOf4 ?x)
          )
          (and
            (RightOf16 ?x)
            (LeftOf9 ?x)
          )
          (and
            (LeftOf1 ?x)
            (RightOf18 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf16 ?x)
          )
          (and
            (LeftOf4 ?x)
            (RightOf18 ?x)
          )
          (and
            (RightOf15 ?x)
            (LeftOf2 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf17 ?x)
          )
          (and
            (LeftOf8 ?x)
            (RightOf17 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf9 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf11 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf19 ?x)
          )
          (and
            (LeftOf7 ?x)
            (RightOf19 ?x)
          )
          (and
            (LeftOf8 ?x)
            (RightOf11 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf13 ?x)
          )
          (and
            (LeftOf1 ?x)
            (RightOf13 ?x)
          )
          (and
            (LeftOf1 ?x)
            (RightOf19 ?x)
          )
          (and
            (LeftOf9 ?x)
            (RightOf18 ?x)
          )
          (and
            (RightOf19 ?x)
            (LeftOf4 ?x)
          )
          (and
            (LeftOf4 ?x)
            (RightOf10 ?x)
          )
          (and
            (RightOf19 ?x)
            (LeftOf3 ?x)
          )
          (and
            (RightOf12 ?x)
            (LeftOf8 ?x)
          )
          (and
            (LeftOf9 ?x)
            (RightOf10 ?x)
          )
          (and
            (RightOf12 ?x)
            (LeftOf3 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf19 ?x)
          )
          (and
            (LeftOf7 ?x)
            (RightOf17 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf14 ?x)
          )
          (and
            (LeftOf8 ?x)
            (RightOf14 ?x)
          )
          (and
            (RightOf16 ?x)
            (LeftOf4 ?x)
          )
          (and
            (LeftOf8 ?x)
            (RightOf18 ?x)
          )
          (and
            (RightOf9 ?x)
            (LeftOf8 ?x)
          )
          (and
            (RightOf11 ?x)
            (LeftOf3 ?x)
          )
          (and
            (RightOf15 ?x)
            (LeftOf9 ?x)
          )
          (and
            (RightOf14 ?x)
            (LeftOf3 ?x)
          )
          (and
            (LeftOf7 ?x)
            (RightOf11 ?x)
          )
          (and
            (RightOf14 ?x)
            (LeftOf2 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf15 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf9 ?x)
          )
          (and
            (RightOf15 ?x)
            (LeftOf7 ?x)
          )
          (and
            (RightOf9 ?x)
            (LeftOf3 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf18 ?x)
          )
          (and
            (RightOf14 ?x)
            (LeftOf1 ?x)
          )
          (and
            (LeftOf4 ?x)
            (RightOf13 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf10 ?x)
          )
          (and
            (RightOf12 ?x)
            (LeftOf6 ?x)
          )
          (and
            (RightOf15 ?x)
            (LeftOf4 ?x)
          )
          (and
            (RightOf18 ?x)
            (LeftOf3 ?x)
          )
          (and
            (RightOf15 ?x)
            (LeftOf8 ?x)
          )
          (and
            (LeftOf9 ?x)
            (RightOf11 ?x)
          )
          (and
            (RightOf12 ?x)
            (LeftOf5 ?x)
          )
          (and
            (RightOf12 ?x)
            (LeftOf7 ?x)
          )
          (and
            (RightOf14 ?x)
            (LeftOf8 ?x)
          )
          (and
            (RightOf12 ?x)
            (LeftOf1 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf17 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf16 ?x)
          )
          (and
            (LeftOf8 ?x)
            (RightOf16 ?x)
          )
          (and
            (RightOf17 ?x)
            (LeftOf2 ?x)
          )
          (and
            (RightOf14 ?x)
            (LeftOf4 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf12 ?x)
          )
          (and
            (RightOf9 ?x)
            (LeftOf9 ?x)
          )
          (and
            (LeftOf7 ?x)
            (RightOf18 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf13 ?x)
          )
          (and
            (LeftOf8 ?x)
            (RightOf13 ?x)
          )
          (and
            (LeftOf1 ?x)
            (RightOf10 ?x)
          )
          (and
            (RightOf13 ?x)
            (LeftOf3 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf13 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf14 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf18 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf8 ?x)
          )
          (and
            (LeftOf7 ?x)
            (RightOf8 ?x)
          )
          (and
            (RightOf17 ?x)
            (LeftOf3 ?x)
          )
          (and
            (RightOf9 ?x)
            (LeftOf7 ?x)
          )
          (and
            (RightOf9 ?x)
            (LeftOf2 ?x)
          )
          (and
            (RightOf15 ?x)
            (LeftOf3 ?x)
          )
          (and
            (RightOf12 ?x)
            (LeftOf9 ?x)
          )
          (and
            (RightOf12 ?x)
            (LeftOf2 ?x)
          )
          (and
            (RightOf8 ?x)
            (LeftOf3 ?x)
          )
          (and
            (RightOf10 ?x)
            (LeftOf3 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf10 ?x)
          )
          (and
            (LeftOf8 ?x)
            (RightOf10 ?x)
          )
          (and
            (RightOf8 ?x)
            (LeftOf4 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf11 ?x)
          )
          (and
            (RightOf9 ?x)
            (LeftOf1 ?x)
          )
          (and
            (LeftOf7 ?x)
            (RightOf13 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf15 ?x)
          )
          (and
            (LeftOf9 ?x)
            (RightOf8 ?x)
          )
          (and
            (LeftOf9 ?x)
            (RightOf13 ?x)
          )
          (and
            (RightOf17 ?x)
            (LeftOf1 ?x)
          )
          (and
            (RightOf15 ?x)
            (LeftOf1 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf18 ?x)
          )
        )
        (and
          (Column9 ?x)
        )
      )
      (when
        (or
          (and
            (RightOf14 ?x)
            (LeftOf9 ?x)
          )
          (and
            (LeftOf7 ?x)
            (RightOf10 ?x)
          )
          (and
            (RightOf15 ?x)
            (LeftOf10 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf11 ?x)
          )
          (and
            (RightOf9 ?x)
            (LeftOf4 ?x)
          )
          (and
            (LeftOf8 ?x)
            (RightOf19 ?x)
          )
          (and
            (LeftOf3 ?x)
            (RightOf13 ?x)
          )
          (and
            (RightOf11 ?x)
            (LeftOf4 ?x)
          )
          (and
            (RightOf12 ?x)
            (LeftOf4 ?x)
          )
          (and
            (RightOf17 ?x)
            (LeftOf9 ?x)
          )
          (and
            (RightOf14 ?x)
            (LeftOf2 ?x)
          )
          (and
            (RightOf16 ?x)
            (LeftOf1 ?x)
          )
          (and
            (LeftOf10 ?x)
            (RightOf16 ?x)
          )
          (and
            (RightOf14 ?x)
            (LeftOf7 ?x)
          )
          (and
            (LeftOf7 ?x)
            (RightOf16 ?x)
          )
          (and
            (RightOf16 ?x)
            (LeftOf3 ?x)
          )
          (and
            (LeftOf1 ?x)
            (RightOf11 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf16 ?x)
          )
          (and
            (RightOf18 ?x)
            (LeftOf4 ?x)
          )
          (and
            (LeftOf9 ?x)
            (RightOf19 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf10 ?x)
          )
          (and
            (RightOf17 ?x)
            (LeftOf4 ?x)
          )
          (and
            (RightOf16 ?x)
            (LeftOf9 ?x)
          )
          (and
            (LeftOf10 ?x)
            (RightOf10 ?x)
          )
          (and
            (LeftOf10 ?x)
            (RightOf11 ?x)
          )
          (and
            (LeftOf1 ?x)
            (RightOf18 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf16 ?x)
          )
          (and
            (LeftOf7 ?x)
            (RightOf11 ?x)
          )
          (and
            (LeftOf4 ?x)
            (RightOf18 ?x)
          )
          (and
            (RightOf15 ?x)
            (LeftOf2 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf17 ?x)
          )
          (and
            (LeftOf8 ?x)
            (RightOf17 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf9 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf11 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf19 ?x)
          )
          (and
            (LeftOf7 ?x)
            (RightOf19 ?x)
          )
          (and
            (LeftOf8 ?x)
            (RightOf11 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf13 ?x)
          )
          (and
            (LeftOf1 ?x)
            (RightOf13 ?x)
          )
          (and
            (LeftOf1 ?x)
            (RightOf19 ?x)
          )
          (and
            (LeftOf9 ?x)
            (RightOf18 ?x)
          )
          (and
            (RightOf19 ?x)
            (LeftOf4 ?x)
          )
          (and
            (LeftOf4 ?x)
            (RightOf10 ?x)
          )
          (and
            (RightOf19 ?x)
            (LeftOf3 ?x)
          )
          (and
            (RightOf9 ?x)
            (LeftOf10 ?x)
          )
          (and
            (RightOf12 ?x)
            (LeftOf8 ?x)
          )
          (and
            (LeftOf9 ?x)
            (RightOf10 ?x)
          )
          (and
            (RightOf12 ?x)
            (LeftOf3 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf19 ?x)
          )
          (and
            (LeftOf7 ?x)
            (RightOf17 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf14 ?x)
          )
          (and
            (LeftOf8 ?x)
            (RightOf14 ?x)
          )
          (and
            (RightOf16 ?x)
            (LeftOf4 ?x)
          )
          (and
            (LeftOf8 ?x)
            (RightOf18 ?x)
          )
          (and
            (LeftOf10 ?x)
            (RightOf13 ?x)
          )
          (and
            (RightOf9 ?x)
            (LeftOf8 ?x)
          )
          (and
            (RightOf11 ?x)
            (LeftOf3 ?x)
          )
          (and
            (RightOf15 ?x)
            (LeftOf9 ?x)
          )
          (and
            (RightOf14 ?x)
            (LeftOf3 ?x)
          )
          (and
            (LeftOf10 ?x)
            (RightOf14 ?x)
          )
          (and
            (RightOf9 ?x)
            (LeftOf1 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf15 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf9 ?x)
          )
          (and
            (RightOf15 ?x)
            (LeftOf7 ?x)
          )
          (and
            (RightOf9 ?x)
            (LeftOf3 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf18 ?x)
          )
          (and
            (RightOf14 ?x)
            (LeftOf1 ?x)
          )
          (and
            (LeftOf4 ?x)
            (RightOf13 ?x)
          )
          (and
            (LeftOf10 ?x)
            (RightOf17 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf19 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf10 ?x)
          )
          (and
            (RightOf12 ?x)
            (LeftOf6 ?x)
          )
          (and
            (LeftOf10 ?x)
            (RightOf18 ?x)
          )
          (and
            (RightOf15 ?x)
            (LeftOf4 ?x)
          )
          (and
            (RightOf18 ?x)
            (LeftOf3 ?x)
          )
          (and
            (RightOf15 ?x)
            (LeftOf8 ?x)
          )
          (and
            (LeftOf9 ?x)
            (RightOf11 ?x)
          )
          (and
            (RightOf12 ?x)
            (LeftOf5 ?x)
          )
          (and
            (RightOf12 ?x)
            (LeftOf7 ?x)
          )
          (and
            (RightOf14 ?x)
            (LeftOf8 ?x)
          )
          (and
            (RightOf12 ?x)
            (LeftOf1 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf17 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf16 ?x)
          )
          (and
            (LeftOf8 ?x)
            (RightOf16 ?x)
          )
          (and
            (RightOf17 ?x)
            (LeftOf2 ?x)
          )
          (and
            (RightOf14 ?x)
            (LeftOf4 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf12 ?x)
          )
          (and
            (RightOf9 ?x)
            (LeftOf9 ?x)
          )
          (and
            (LeftOf7 ?x)
            (RightOf18 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf13 ?x)
          )
          (and
            (LeftOf8 ?x)
            (RightOf13 ?x)
          )
          (and
            (LeftOf1 ?x)
            (RightOf10 ?x)
          )
          (and
            (RightOf13 ?x)
            (LeftOf3 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf13 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf14 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf18 ?x)
          )
          (and
            (RightOf17 ?x)
            (LeftOf3 ?x)
          )
          (and
            (RightOf9 ?x)
            (LeftOf7 ?x)
          )
          (and
            (RightOf9 ?x)
            (LeftOf2 ?x)
          )
          (and
            (RightOf15 ?x)
            (LeftOf3 ?x)
          )
          (and
            (RightOf12 ?x)
            (LeftOf9 ?x)
          )
          (and
            (RightOf12 ?x)
            (LeftOf2 ?x)
          )
          (and
            (RightOf10 ?x)
            (LeftOf4 ?x)
          )
          (and
            (RightOf10 ?x)
            (LeftOf3 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf10 ?x)
          )
          (and
            (LeftOf8 ?x)
            (RightOf10 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf11 ?x)
          )
          (and
            (LeftOf7 ?x)
            (RightOf13 ?x)
          )
          (and
            (LeftOf10 ?x)
            (RightOf19 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf15 ?x)
          )
          (and
            (LeftOf9 ?x)
            (RightOf13 ?x)
          )
          (and
            (RightOf12 ?x)
            (LeftOf10 ?x)
          )
          (and
            (RightOf17 ?x)
            (LeftOf1 ?x)
          )
          (and
            (RightOf15 ?x)
            (LeftOf1 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf18 ?x)
          )
        )
        (and
          (Column10 ?x)
        )
      )
      (when
        (or
          (and
            (RightOf14 ?x)
            (LeftOf9 ?x)
          )
          (and
            (LeftOf11 ?x)
            (RightOf10 ?x)
          )
          (and
            (RightOf17 ?x)
            (LeftOf11 ?x)
          )
          (and
            (LeftOf7 ?x)
            (RightOf10 ?x)
          )
          (and
            (RightOf15 ?x)
            (LeftOf10 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf11 ?x)
          )
          (and
            (RightOf19 ?x)
            (LeftOf4 ?x)
          )
          (and
            (LeftOf11 ?x)
            (RightOf19 ?x)
          )
          (and
            (LeftOf8 ?x)
            (RightOf19 ?x)
          )
          (and
            (LeftOf3 ?x)
            (RightOf13 ?x)
          )
          (and
            (RightOf11 ?x)
            (LeftOf4 ?x)
          )
          (and
            (RightOf12 ?x)
            (LeftOf4 ?x)
          )
          (and
            (RightOf17 ?x)
            (LeftOf9 ?x)
          )
          (and
            (RightOf14 ?x)
            (LeftOf2 ?x)
          )
          (and
            (RightOf16 ?x)
            (LeftOf1 ?x)
          )
          (and
            (LeftOf10 ?x)
            (RightOf16 ?x)
          )
          (and
            (LeftOf10 ?x)
            (RightOf18 ?x)
          )
          (and
            (LeftOf7 ?x)
            (RightOf16 ?x)
          )
          (and
            (RightOf16 ?x)
            (LeftOf3 ?x)
          )
          (and
            (LeftOf1 ?x)
            (RightOf11 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf16 ?x)
          )
          (and
            (LeftOf11 ?x)
            (RightOf18 ?x)
          )
          (and
            (RightOf18 ?x)
            (LeftOf4 ?x)
          )
          (and
            (LeftOf9 ?x)
            (RightOf19 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf10 ?x)
          )
          (and
            (RightOf17 ?x)
            (LeftOf4 ?x)
          )
          (and
            (RightOf16 ?x)
            (LeftOf9 ?x)
          )
          (and
            (LeftOf10 ?x)
            (RightOf10 ?x)
          )
          (and
            (LeftOf10 ?x)
            (RightOf11 ?x)
          )
          (and
            (LeftOf1 ?x)
            (RightOf18 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf16 ?x)
          )
          (and
            (LeftOf4 ?x)
            (RightOf18 ?x)
          )
          (and
            (RightOf15 ?x)
            (LeftOf2 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf17 ?x)
          )
          (and
            (LeftOf8 ?x)
            (RightOf17 ?x)
          )
          (and
            (RightOf12 ?x)
            (LeftOf11 ?x)
          )
          (and
            (LeftOf7 ?x)
            (RightOf19 ?x)
          )
          (and
            (LeftOf8 ?x)
            (RightOf11 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf13 ?x)
          )
          (and
            (LeftOf1 ?x)
            (RightOf13 ?x)
          )
          (and
            (LeftOf1 ?x)
            (RightOf19 ?x)
          )
          (and
            (LeftOf9 ?x)
            (RightOf18 ?x)
          )
          (and
            (RightOf17 ?x)
            (LeftOf1 ?x)
          )
          (and
            (RightOf12 ?x)
            (LeftOf8 ?x)
          )
          (and
            (LeftOf9 ?x)
            (RightOf10 ?x)
          )
          (and
            (RightOf12 ?x)
            (LeftOf3 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf19 ?x)
          )
          (and
            (LeftOf7 ?x)
            (RightOf17 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf14 ?x)
          )
          (and
            (LeftOf8 ?x)
            (RightOf14 ?x)
          )
          (and
            (RightOf16 ?x)
            (LeftOf4 ?x)
          )
          (and
            (LeftOf8 ?x)
            (RightOf18 ?x)
          )
          (and
            (LeftOf10 ?x)
            (RightOf13 ?x)
          )
          (and
            (RightOf11 ?x)
            (LeftOf3 ?x)
          )
          (and
            (RightOf15 ?x)
            (LeftOf9 ?x)
          )
          (and
            (RightOf14 ?x)
            (LeftOf3 ?x)
          )
          (and
            (LeftOf7 ?x)
            (RightOf11 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf19 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf15 ?x)
          )
          (and
            (RightOf14 ?x)
            (LeftOf11 ?x)
          )
          (and
            (RightOf15 ?x)
            (LeftOf7 ?x)
          )
          (and
            (RightOf19 ?x)
            (LeftOf3 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf18 ?x)
          )
          (and
            (RightOf14 ?x)
            (LeftOf1 ?x)
          )
          (and
            (LeftOf4 ?x)
            (RightOf13 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf11 ?x)
          )
          (and
            (LeftOf10 ?x)
            (RightOf17 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf19 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf10 ?x)
          )
          (and
            (RightOf12 ?x)
            (LeftOf6 ?x)
          )
          (and
            (RightOf14 ?x)
            (LeftOf7 ?x)
          )
          (and
            (RightOf15 ?x)
            (LeftOf4 ?x)
          )
          (and
            (RightOf18 ?x)
            (LeftOf3 ?x)
          )
          (and
            (RightOf15 ?x)
            (LeftOf8 ?x)
          )
          (and
            (LeftOf11 ?x)
            (RightOf13 ?x)
          )
          (and
            (LeftOf9 ?x)
            (RightOf11 ?x)
          )
          (and
            (RightOf12 ?x)
            (LeftOf5 ?x)
          )
          (and
            (RightOf12 ?x)
            (LeftOf7 ?x)
          )
          (and
            (RightOf14 ?x)
            (LeftOf8 ?x)
          )
          (and
            (RightOf12 ?x)
            (LeftOf1 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf17 ?x)
          )
          (and
            (RightOf14 ?x)
            (LeftOf10 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf16 ?x)
          )
          (and
            (LeftOf8 ?x)
            (RightOf16 ?x)
          )
          (and
            (RightOf17 ?x)
            (LeftOf2 ?x)
          )
          (and
            (RightOf14 ?x)
            (LeftOf4 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf12 ?x)
          )
          (and
            (LeftOf7 ?x)
            (RightOf18 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf13 ?x)
          )
          (and
            (LeftOf8 ?x)
            (RightOf13 ?x)
          )
          (and
            (LeftOf10 ?x)
            (RightOf14 ?x)
          )
          (and
            (LeftOf1 ?x)
            (RightOf10 ?x)
          )
          (and
            (RightOf13 ?x)
            (LeftOf3 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf13 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf14 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf18 ?x)
          )
          (and
            (RightOf17 ?x)
            (LeftOf3 ?x)
          )
          (and
            (RightOf15 ?x)
            (LeftOf3 ?x)
          )
          (and
            (RightOf12 ?x)
            (LeftOf9 ?x)
          )
          (and
            (LeftOf11 ?x)
            (RightOf11 ?x)
          )
          (and
            (RightOf12 ?x)
            (LeftOf2 ?x)
          )
          (and
            (RightOf10 ?x)
            (LeftOf4 ?x)
          )
          (and
            (RightOf10 ?x)
            (LeftOf3 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf10 ?x)
          )
          (and
            (LeftOf8 ?x)
            (RightOf10 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf11 ?x)
          )
          (and
            (LeftOf7 ?x)
            (RightOf13 ?x)
          )
          (and
            (LeftOf10 ?x)
            (RightOf19 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf15 ?x)
          )
          (and
            (RightOf16 ?x)
            (LeftOf11 ?x)
          )
          (and
            (LeftOf9 ?x)
            (RightOf13 ?x)
          )
          (and
            (RightOf12 ?x)
            (LeftOf10 ?x)
          )
          (and
            (RightOf15 ?x)
            (LeftOf11 ?x)
          )
          (and
            (RightOf15 ?x)
            (LeftOf1 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf18 ?x)
          )
        )
        (and
          (Column11 ?x)
        )
      )
      (when
        (or
          (and
            (RightOf14 ?x)
            (LeftOf9 ?x)
          )
          (and
            (RightOf17 ?x)
            (LeftOf11 ?x)
          )
          (and
            (RightOf15 ?x)
            (LeftOf10 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf11 ?x)
          )
          (and
            (LeftOf12 ?x)
            (RightOf13 ?x)
          )
          (and
            (RightOf19 ?x)
            (LeftOf4 ?x)
          )
          (and
            (LeftOf11 ?x)
            (RightOf19 ?x)
          )
          (and
            (LeftOf8 ?x)
            (RightOf19 ?x)
          )
          (and
            (LeftOf3 ?x)
            (RightOf13 ?x)
          )
          (and
            (RightOf11 ?x)
            (LeftOf4 ?x)
          )
          (and
            (RightOf12 ?x)
            (LeftOf4 ?x)
          )
          (and
            (RightOf17 ?x)
            (LeftOf9 ?x)
          )
          (and
            (RightOf14 ?x)
            (LeftOf2 ?x)
          )
          (and
            (RightOf16 ?x)
            (LeftOf1 ?x)
          )
          (and
            (LeftOf10 ?x)
            (RightOf16 ?x)
          )
          (and
            (LeftOf10 ?x)
            (RightOf18 ?x)
          )
          (and
            (LeftOf7 ?x)
            (RightOf16 ?x)
          )
          (and
            (RightOf16 ?x)
            (LeftOf3 ?x)
          )
          (and
            (LeftOf12 ?x)
            (RightOf15 ?x)
          )
          (and
            (LeftOf1 ?x)
            (RightOf11 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf16 ?x)
          )
          (and
            (LeftOf11 ?x)
            (RightOf18 ?x)
          )
          (and
            (LeftOf9 ?x)
            (RightOf19 ?x)
          )
          (and
            (RightOf17 ?x)
            (LeftOf4 ?x)
          )
          (and
            (RightOf16 ?x)
            (LeftOf9 ?x)
          )
          (and
            (LeftOf10 ?x)
            (RightOf11 ?x)
          )
          (and
            (LeftOf1 ?x)
            (RightOf18 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf16 ?x)
          )
          (and
            (LeftOf4 ?x)
            (RightOf18 ?x)
          )
          (and
            (RightOf15 ?x)
            (LeftOf2 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf17 ?x)
          )
          (and
            (LeftOf8 ?x)
            (RightOf17 ?x)
          )
          (and
            (RightOf12 ?x)
            (LeftOf11 ?x)
          )
          (and
            (LeftOf7 ?x)
            (RightOf19 ?x)
          )
          (and
            (LeftOf8 ?x)
            (RightOf11 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf13 ?x)
          )
          (and
            (LeftOf1 ?x)
            (RightOf13 ?x)
          )
          (and
            (LeftOf1 ?x)
            (RightOf19 ?x)
          )
          (and
            (LeftOf9 ?x)
            (RightOf18 ?x)
          )
          (and
            (RightOf17 ?x)
            (LeftOf1 ?x)
          )
          (and
            (RightOf12 ?x)
            (LeftOf8 ?x)
          )
          (and
            (LeftOf12 ?x)
            (RightOf17 ?x)
          )
          (and
            (RightOf12 ?x)
            (LeftOf3 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf19 ?x)
          )
          (and
            (LeftOf7 ?x)
            (RightOf17 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf14 ?x)
          )
          (and
            (LeftOf8 ?x)
            (RightOf14 ?x)
          )
          (and
            (RightOf16 ?x)
            (LeftOf4 ?x)
          )
          (and
            (LeftOf12 ?x)
            (RightOf16 ?x)
          )
          (and
            (LeftOf8 ?x)
            (RightOf18 ?x)
          )
          (and
            (LeftOf10 ?x)
            (RightOf13 ?x)
          )
          (and
            (RightOf11 ?x)
            (LeftOf3 ?x)
          )
          (and
            (LeftOf12 ?x)
            (RightOf12 ?x)
          )
          (and
            (RightOf15 ?x)
            (LeftOf9 ?x)
          )
          (and
            (RightOf14 ?x)
            (LeftOf3 ?x)
          )
          (and
            (LeftOf7 ?x)
            (RightOf11 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf19 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf15 ?x)
          )
          (and
            (RightOf14 ?x)
            (LeftOf11 ?x)
          )
          (and
            (RightOf15 ?x)
            (LeftOf7 ?x)
          )
          (and
            (RightOf19 ?x)
            (LeftOf3 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf18 ?x)
          )
          (and
            (RightOf14 ?x)
            (LeftOf1 ?x)
          )
          (and
            (LeftOf4 ?x)
            (RightOf13 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf11 ?x)
          )
          (and
            (LeftOf10 ?x)
            (RightOf17 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf19 ?x)
          )
          (and
            (RightOf12 ?x)
            (LeftOf6 ?x)
          )
          (and
            (RightOf14 ?x)
            (LeftOf7 ?x)
          )
          (and
            (RightOf15 ?x)
            (LeftOf4 ?x)
          )
          (and
            (RightOf18 ?x)
            (LeftOf3 ?x)
          )
          (and
            (LeftOf12 ?x)
            (RightOf19 ?x)
          )
          (and
            (RightOf15 ?x)
            (LeftOf8 ?x)
          )
          (and
            (LeftOf11 ?x)
            (RightOf13 ?x)
          )
          (and
            (LeftOf9 ?x)
            (RightOf11 ?x)
          )
          (and
            (RightOf12 ?x)
            (LeftOf5 ?x)
          )
          (and
            (RightOf12 ?x)
            (LeftOf7 ?x)
          )
          (and
            (RightOf14 ?x)
            (LeftOf8 ?x)
          )
          (and
            (RightOf12 ?x)
            (LeftOf1 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf17 ?x)
          )
          (and
            (RightOf14 ?x)
            (LeftOf10 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf16 ?x)
          )
          (and
            (LeftOf8 ?x)
            (RightOf16 ?x)
          )
          (and
            (RightOf17 ?x)
            (LeftOf2 ?x)
          )
          (and
            (RightOf14 ?x)
            (LeftOf4 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf12 ?x)
          )
          (and
            (LeftOf7 ?x)
            (RightOf18 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf13 ?x)
          )
          (and
            (LeftOf8 ?x)
            (RightOf13 ?x)
          )
          (and
            (RightOf13 ?x)
            (LeftOf3 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf13 ?x)
          )
          (and
            (LeftOf12 ?x)
            (RightOf18 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf14 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf18 ?x)
          )
          (and
            (RightOf17 ?x)
            (LeftOf3 ?x)
          )
          (and
            (RightOf15 ?x)
            (LeftOf3 ?x)
          )
          (and
            (RightOf12 ?x)
            (LeftOf9 ?x)
          )
          (and
            (LeftOf11 ?x)
            (RightOf11 ?x)
          )
          (and
            (RightOf12 ?x)
            (LeftOf2 ?x)
          )
          (and
            (LeftOf10 ?x)
            (RightOf14 ?x)
          )
          (and
            (RightOf18 ?x)
            (LeftOf4 ?x)
          )
          (and
            (LeftOf12 ?x)
            (RightOf11 ?x)
          )
          (and
            (LeftOf12 ?x)
            (RightOf14 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf11 ?x)
          )
          (and
            (LeftOf7 ?x)
            (RightOf13 ?x)
          )
          (and
            (LeftOf10 ?x)
            (RightOf19 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf15 ?x)
          )
          (and
            (RightOf16 ?x)
            (LeftOf11 ?x)
          )
          (and
            (LeftOf9 ?x)
            (RightOf13 ?x)
          )
          (and
            (RightOf12 ?x)
            (LeftOf10 ?x)
          )
          (and
            (RightOf15 ?x)
            (LeftOf11 ?x)
          )
          (and
            (RightOf15 ?x)
            (LeftOf1 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf18 ?x)
          )
        )
        (and
          (Column12 ?x)
        )
      )
      (when
        (or
          (and
            (RightOf14 ?x)
            (LeftOf9 ?x)
          )
          (and
            (LeftOf13 ?x)
            (RightOf16 ?x)
          )
          (and
            (RightOf17 ?x)
            (LeftOf11 ?x)
          )
          (and
            (RightOf15 ?x)
            (LeftOf10 ?x)
          )
          (and
            (RightOf15 ?x)
            (LeftOf2 ?x)
          )
          (and
            (LeftOf12 ?x)
            (RightOf13 ?x)
          )
          (and
            (RightOf19 ?x)
            (LeftOf4 ?x)
          )
          (and
            (LeftOf11 ?x)
            (RightOf19 ?x)
          )
          (and
            (LeftOf8 ?x)
            (RightOf19 ?x)
          )
          (and
            (LeftOf3 ?x)
            (RightOf13 ?x)
          )
          (and
            (RightOf16 ?x)
            (LeftOf1 ?x)
          )
          (and
            (RightOf12 ?x)
            (LeftOf4 ?x)
          )
          (and
            (RightOf17 ?x)
            (LeftOf9 ?x)
          )
          (and
            (RightOf14 ?x)
            (LeftOf2 ?x)
          )
          (and
            (LeftOf10 ?x)
            (RightOf16 ?x)
          )
          (and
            (RightOf14 ?x)
            (LeftOf7 ?x)
          )
          (and
            (LeftOf7 ?x)
            (RightOf16 ?x)
          )
          (and
            (RightOf12 ?x)
            (LeftOf13 ?x)
          )
          (and
            (RightOf16 ?x)
            (LeftOf3 ?x)
          )
          (and
            (LeftOf12 ?x)
            (RightOf15 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf16 ?x)
          )
          (and
            (LeftOf11 ?x)
            (RightOf18 ?x)
          )
          (and
            (LeftOf9 ?x)
            (RightOf19 ?x)
          )
          (and
            (RightOf17 ?x)
            (LeftOf4 ?x)
          )
          (and
            (RightOf16 ?x)
            (LeftOf9 ?x)
          )
          (and
            (LeftOf1 ?x)
            (RightOf18 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf16 ?x)
          )
          (and
            (LeftOf4 ?x)
            (RightOf18 ?x)
          )
          (and
            (LeftOf13 ?x)
            (RightOf18 ?x)
          )
          (and
            (LeftOf8 ?x)
            (RightOf17 ?x)
          )
          (and
            (RightOf12 ?x)
            (LeftOf11 ?x)
          )
          (and
            (LeftOf7 ?x)
            (RightOf19 ?x)
          )
          (and
            (RightOf12 ?x)
            (LeftOf12 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf13 ?x)
          )
          (and
            (LeftOf1 ?x)
            (RightOf13 ?x)
          )
          (and
            (LeftOf1 ?x)
            (RightOf19 ?x)
          )
          (and
            (LeftOf9 ?x)
            (RightOf18 ?x)
          )
          (and
            (RightOf17 ?x)
            (LeftOf1 ?x)
          )
          (and
            (RightOf12 ?x)
            (LeftOf8 ?x)
          )
          (and
            (LeftOf12 ?x)
            (RightOf17 ?x)
          )
          (and
            (RightOf12 ?x)
            (LeftOf3 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf19 ?x)
          )
          (and
            (LeftOf7 ?x)
            (RightOf17 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf14 ?x)
          )
          (and
            (LeftOf8 ?x)
            (RightOf14 ?x)
          )
          (and
            (RightOf16 ?x)
            (LeftOf4 ?x)
          )
          (and
            (LeftOf12 ?x)
            (RightOf16 ?x)
          )
          (and
            (LeftOf8 ?x)
            (RightOf18 ?x)
          )
          (and
            (LeftOf10 ?x)
            (RightOf13 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf18 ?x)
          )
          (and
            (RightOf15 ?x)
            (LeftOf9 ?x)
          )
          (and
            (RightOf14 ?x)
            (LeftOf3 ?x)
          )
          (and
            (LeftOf10 ?x)
            (RightOf14 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf19 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf15 ?x)
          )
          (and
            (RightOf14 ?x)
            (LeftOf11 ?x)
          )
          (and
            (RightOf15 ?x)
            (LeftOf7 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf17 ?x)
          )
          (and
            (RightOf19 ?x)
            (LeftOf3 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf18 ?x)
          )
          (and
            (RightOf14 ?x)
            (LeftOf1 ?x)
          )
          (and
            (LeftOf4 ?x)
            (RightOf13 ?x)
          )
          (and
            (LeftOf10 ?x)
            (RightOf17 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf19 ?x)
          )
          (and
            (RightOf12 ?x)
            (LeftOf6 ?x)
          )
          (and
            (RightOf14 ?x)
            (LeftOf13 ?x)
          )
          (and
            (RightOf15 ?x)
            (LeftOf4 ?x)
          )
          (and
            (RightOf18 ?x)
            (LeftOf3 ?x)
          )
          (and
            (LeftOf12 ?x)
            (RightOf19 ?x)
          )
          (and
            (LeftOf9 ?x)
            (RightOf13 ?x)
          )
          (and
            (RightOf15 ?x)
            (LeftOf8 ?x)
          )
          (and
            (LeftOf11 ?x)
            (RightOf13 ?x)
          )
          (and
            (RightOf12 ?x)
            (LeftOf5 ?x)
          )
          (and
            (RightOf12 ?x)
            (LeftOf7 ?x)
          )
          (and
            (RightOf14 ?x)
            (LeftOf8 ?x)
          )
          (and
            (RightOf12 ?x)
            (LeftOf1 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf17 ?x)
          )
          (and
            (RightOf14 ?x)
            (LeftOf10 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf16 ?x)
          )
          (and
            (LeftOf8 ?x)
            (RightOf16 ?x)
          )
          (and
            (RightOf17 ?x)
            (LeftOf2 ?x)
          )
          (and
            (RightOf14 ?x)
            (LeftOf4 ?x)
          )
          (and
            (LeftOf7 ?x)
            (RightOf18 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf13 ?x)
          )
          (and
            (LeftOf8 ?x)
            (RightOf13 ?x)
          )
          (and
            (RightOf15 ?x)
            (LeftOf13 ?x)
          )
          (and
            (RightOf13 ?x)
            (LeftOf3 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf13 ?x)
          )
          (and
            (LeftOf13 ?x)
            (RightOf19 ?x)
          )
          (and
            (LeftOf12 ?x)
            (RightOf18 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf14 ?x)
          )
          (and
            (LeftOf13 ?x)
            (RightOf13 ?x)
          )
          (and
            (RightOf17 ?x)
            (LeftOf3 ?x)
          )
          (and
            (LeftOf10 ?x)
            (RightOf18 ?x)
          )
          (and
            (RightOf15 ?x)
            (LeftOf3 ?x)
          )
          (and
            (RightOf12 ?x)
            (LeftOf9 ?x)
          )
          (and
            (RightOf12 ?x)
            (LeftOf2 ?x)
          )
          (and
            (RightOf18 ?x)
            (LeftOf4 ?x)
          )
          (and
            (LeftOf12 ?x)
            (RightOf14 ?x)
          )
          (and
            (LeftOf7 ?x)
            (RightOf13 ?x)
          )
          (and
            (LeftOf10 ?x)
            (RightOf19 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf15 ?x)
          )
          (and
            (RightOf16 ?x)
            (LeftOf11 ?x)
          )
          (and
            (LeftOf13 ?x)
            (RightOf17 ?x)
          )
          (and
            (RightOf12 ?x)
            (LeftOf10 ?x)
          )
          (and
            (RightOf15 ?x)
            (LeftOf11 ?x)
          )
          (and
            (RightOf15 ?x)
            (LeftOf1 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf18 ?x)
          )
        )
        (and
          (Column13 ?x)
        )
      )
      (when
        (or
          (and
            (RightOf14 ?x)
            (LeftOf9 ?x)
          )
          (and
            (LeftOf13 ?x)
            (RightOf16 ?x)
          )
          (and
            (RightOf17 ?x)
            (LeftOf11 ?x)
          )
          (and
            (RightOf15 ?x)
            (LeftOf10 ?x)
          )
          (and
            (RightOf15 ?x)
            (LeftOf2 ?x)
          )
          (and
            (LeftOf12 ?x)
            (RightOf13 ?x)
          )
          (and
            (RightOf19 ?x)
            (LeftOf4 ?x)
          )
          (and
            (LeftOf11 ?x)
            (RightOf19 ?x)
          )
          (and
            (LeftOf8 ?x)
            (RightOf19 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf19 ?x)
          )
          (and
            (RightOf16 ?x)
            (LeftOf1 ?x)
          )
          (and
            (RightOf17 ?x)
            (LeftOf9 ?x)
          )
          (and
            (RightOf14 ?x)
            (LeftOf2 ?x)
          )
          (and
            (LeftOf10 ?x)
            (RightOf16 ?x)
          )
          (and
            (RightOf14 ?x)
            (LeftOf13 ?x)
          )
          (and
            (LeftOf7 ?x)
            (RightOf16 ?x)
          )
          (and
            (RightOf16 ?x)
            (LeftOf3 ?x)
          )
          (and
            (LeftOf12 ?x)
            (RightOf15 ?x)
          )
          (and
            (RightOf19 ?x)
            (LeftOf14 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf16 ?x)
          )
          (and
            (LeftOf11 ?x)
            (RightOf18 ?x)
          )
          (and
            (LeftOf9 ?x)
            (RightOf19 ?x)
          )
          (and
            (RightOf17 ?x)
            (LeftOf4 ?x)
          )
          (and
            (RightOf16 ?x)
            (LeftOf9 ?x)
          )
          (and
            (LeftOf1 ?x)
            (RightOf18 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf16 ?x)
          )
          (and
            (LeftOf4 ?x)
            (RightOf18 ?x)
          )
          (and
            (LeftOf13 ?x)
            (RightOf18 ?x)
          )
          (and
            (LeftOf8 ?x)
            (RightOf17 ?x)
          )
          (and
            (LeftOf7 ?x)
            (RightOf19 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf13 ?x)
          )
          (and
            (LeftOf1 ?x)
            (RightOf13 ?x)
          )
          (and
            (LeftOf1 ?x)
            (RightOf19 ?x)
          )
          (and
            (RightOf15 ?x)
            (LeftOf14 ?x)
          )
          (and
            (LeftOf9 ?x)
            (RightOf18 ?x)
          )
          (and
            (RightOf17 ?x)
            (LeftOf1 ?x)
          )
          (and
            (LeftOf14 ?x)
            (RightOf18 ?x)
          )
          (and
            (RightOf16 ?x)
            (LeftOf14 ?x)
          )
          (and
            (LeftOf12 ?x)
            (RightOf17 ?x)
          )
          (and
            (RightOf14 ?x)
            (LeftOf7 ?x)
          )
          (and
            (LeftOf7 ?x)
            (RightOf17 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf14 ?x)
          )
          (and
            (LeftOf8 ?x)
            (RightOf14 ?x)
          )
          (and
            (RightOf16 ?x)
            (LeftOf4 ?x)
          )
          (and
            (LeftOf12 ?x)
            (RightOf16 ?x)
          )
          (and
            (LeftOf8 ?x)
            (RightOf18 ?x)
          )
          (and
            (LeftOf10 ?x)
            (RightOf13 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf18 ?x)
          )
          (and
            (RightOf15 ?x)
            (LeftOf9 ?x)
          )
          (and
            (RightOf14 ?x)
            (LeftOf3 ?x)
          )
          (and
            (LeftOf10 ?x)
            (RightOf14 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf19 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf15 ?x)
          )
          (and
            (RightOf14 ?x)
            (LeftOf11 ?x)
          )
          (and
            (RightOf15 ?x)
            (LeftOf7 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf17 ?x)
          )
          (and
            (RightOf19 ?x)
            (LeftOf3 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf18 ?x)
          )
          (and
            (RightOf14 ?x)
            (LeftOf1 ?x)
          )
          (and
            (LeftOf4 ?x)
            (RightOf13 ?x)
          )
          (and
            (RightOf17 ?x)
            (LeftOf14 ?x)
          )
          (and
            (LeftOf10 ?x)
            (RightOf17 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf19 ?x)
          )
          (and
            (LeftOf10 ?x)
            (RightOf18 ?x)
          )
          (and
            (RightOf15 ?x)
            (LeftOf4 ?x)
          )
          (and
            (RightOf18 ?x)
            (LeftOf3 ?x)
          )
          (and
            (LeftOf12 ?x)
            (RightOf19 ?x)
          )
          (and
            (RightOf15 ?x)
            (LeftOf8 ?x)
          )
          (and
            (LeftOf11 ?x)
            (RightOf13 ?x)
          )
          (and
            (LeftOf9 ?x)
            (RightOf13 ?x)
          )
          (and
            (RightOf14 ?x)
            (LeftOf8 ?x)
          )
          (and
            (LeftOf14 ?x)
            (RightOf13 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf17 ?x)
          )
          (and
            (RightOf14 ?x)
            (LeftOf10 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf16 ?x)
          )
          (and
            (LeftOf8 ?x)
            (RightOf16 ?x)
          )
          (and
            (RightOf17 ?x)
            (LeftOf2 ?x)
          )
          (and
            (RightOf14 ?x)
            (LeftOf4 ?x)
          )
          (and
            (LeftOf7 ?x)
            (RightOf18 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf13 ?x)
          )
          (and
            (LeftOf8 ?x)
            (RightOf13 ?x)
          )
          (and
            (RightOf15 ?x)
            (LeftOf13 ?x)
          )
          (and
            (RightOf13 ?x)
            (LeftOf3 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf13 ?x)
          )
          (and
            (LeftOf13 ?x)
            (RightOf19 ?x)
          )
          (and
            (LeftOf12 ?x)
            (RightOf18 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf14 ?x)
          )
          (and
            (LeftOf13 ?x)
            (RightOf13 ?x)
          )
          (and
            (RightOf17 ?x)
            (LeftOf3 ?x)
          )
          (and
            (RightOf15 ?x)
            (LeftOf3 ?x)
          )
          (and
            (RightOf18 ?x)
            (LeftOf4 ?x)
          )
          (and
            (LeftOf12 ?x)
            (RightOf14 ?x)
          )
          (and
            (LeftOf7 ?x)
            (RightOf13 ?x)
          )
          (and
            (LeftOf10 ?x)
            (RightOf19 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf15 ?x)
          )
          (and
            (RightOf16 ?x)
            (LeftOf11 ?x)
          )
          (and
            (LeftOf13 ?x)
            (RightOf17 ?x)
          )
          (and
            (RightOf15 ?x)
            (LeftOf11 ?x)
          )
          (and
            (RightOf14 ?x)
            (LeftOf14 ?x)
          )
          (and
            (RightOf15 ?x)
            (LeftOf1 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf18 ?x)
          )
        )
        (and
          (Column14 ?x)
        )
      )
      (when
        (or
          (and
            (LeftOf15 ?x)
            (RightOf14 ?x)
          )
          (and
            (RightOf14 ?x)
            (LeftOf9 ?x)
          )
          (and
            (LeftOf13 ?x)
            (RightOf16 ?x)
          )
          (and
            (RightOf17 ?x)
            (LeftOf11 ?x)
          )
          (and
            (RightOf15 ?x)
            (LeftOf10 ?x)
          )
          (and
            (RightOf15 ?x)
            (LeftOf2 ?x)
          )
          (and
            (RightOf19 ?x)
            (LeftOf4 ?x)
          )
          (and
            (LeftOf11 ?x)
            (RightOf19 ?x)
          )
          (and
            (LeftOf8 ?x)
            (RightOf19 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf19 ?x)
          )
          (and
            (LeftOf15 ?x)
            (RightOf18 ?x)
          )
          (and
            (RightOf17 ?x)
            (LeftOf9 ?x)
          )
          (and
            (RightOf14 ?x)
            (LeftOf2 ?x)
          )
          (and
            (LeftOf10 ?x)
            (RightOf16 ?x)
          )
          (and
            (RightOf14 ?x)
            (LeftOf7 ?x)
          )
          (and
            (LeftOf7 ?x)
            (RightOf16 ?x)
          )
          (and
            (RightOf16 ?x)
            (LeftOf3 ?x)
          )
          (and
            (RightOf19 ?x)
            (LeftOf14 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf16 ?x)
          )
          (and
            (LeftOf11 ?x)
            (RightOf18 ?x)
          )
          (and
            (LeftOf9 ?x)
            (RightOf19 ?x)
          )
          (and
            (RightOf17 ?x)
            (LeftOf4 ?x)
          )
          (and
            (RightOf16 ?x)
            (LeftOf9 ?x)
          )
          (and
            (LeftOf1 ?x)
            (RightOf18 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf16 ?x)
          )
          (and
            (LeftOf15 ?x)
            (RightOf19 ?x)
          )
          (and
            (LeftOf4 ?x)
            (RightOf18 ?x)
          )
          (and
            (LeftOf13 ?x)
            (RightOf18 ?x)
          )
          (and
            (LeftOf8 ?x)
            (RightOf17 ?x)
          )
          (and
            (LeftOf7 ?x)
            (RightOf19 ?x)
          )
          (and
            (LeftOf1 ?x)
            (RightOf19 ?x)
          )
          (and
            (RightOf15 ?x)
            (LeftOf14 ?x)
          )
          (and
            (LeftOf9 ?x)
            (RightOf18 ?x)
          )
          (and
            (RightOf17 ?x)
            (LeftOf1 ?x)
          )
          (and
            (LeftOf14 ?x)
            (RightOf18 ?x)
          )
          (and
            (RightOf16 ?x)
            (LeftOf14 ?x)
          )
          (and
            (LeftOf12 ?x)
            (RightOf17 ?x)
          )
          (and
            (RightOf15 ?x)
            (LeftOf4 ?x)
          )
          (and
            (LeftOf7 ?x)
            (RightOf17 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf14 ?x)
          )
          (and
            (RightOf16 ?x)
            (LeftOf4 ?x)
          )
          (and
            (LeftOf12 ?x)
            (RightOf16 ?x)
          )
          (and
            (LeftOf8 ?x)
            (RightOf18 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf18 ?x)
          )
          (and
            (RightOf15 ?x)
            (LeftOf9 ?x)
          )
          (and
            (RightOf14 ?x)
            (LeftOf3 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf19 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf15 ?x)
          )
          (and
            (RightOf14 ?x)
            (LeftOf11 ?x)
          )
          (and
            (RightOf15 ?x)
            (LeftOf7 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf17 ?x)
          )
          (and
            (RightOf19 ?x)
            (LeftOf3 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf18 ?x)
          )
          (and
            (RightOf14 ?x)
            (LeftOf1 ?x)
          )
          (and
            (RightOf17 ?x)
            (LeftOf14 ?x)
          )
          (and
            (LeftOf10 ?x)
            (RightOf17 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf19 ?x)
          )
          (and
            (RightOf14 ?x)
            (LeftOf13 ?x)
          )
          (and
            (RightOf16 ?x)
            (LeftOf1 ?x)
          )
          (and
            (RightOf18 ?x)
            (LeftOf3 ?x)
          )
          (and
            (LeftOf12 ?x)
            (RightOf19 ?x)
          )
          (and
            (RightOf15 ?x)
            (LeftOf8 ?x)
          )
          (and
            (RightOf14 ?x)
            (LeftOf8 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf17 ?x)
          )
          (and
            (RightOf14 ?x)
            (LeftOf10 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf16 ?x)
          )
          (and
            (RightOf15 ?x)
            (LeftOf3 ?x)
          )
          (and
            (RightOf17 ?x)
            (LeftOf2 ?x)
          )
          (and
            (RightOf14 ?x)
            (LeftOf4 ?x)
          )
          (and
            (LeftOf7 ?x)
            (RightOf18 ?x)
          )
          (and
            (LeftOf12 ?x)
            (RightOf15 ?x)
          )
          (and
            (RightOf15 ?x)
            (LeftOf13 ?x)
          )
          (and
            (RightOf18 ?x)
            (LeftOf14 ?x)
          )
          (and
            (LeftOf13 ?x)
            (RightOf19 ?x)
          )
          (and
            (LeftOf12 ?x)
            (RightOf18 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf14 ?x)
          )
          (and
            (RightOf17 ?x)
            (LeftOf3 ?x)
          )
          (and
            (LeftOf15 ?x)
            (RightOf15 ?x)
          )
          (and
            (LeftOf10 ?x)
            (RightOf18 ?x)
          )
          (and
            (LeftOf8 ?x)
            (RightOf16 ?x)
          )
          (and
            (RightOf18 ?x)
            (LeftOf4 ?x)
          )
          (and
            (LeftOf15 ?x)
            (RightOf17 ?x)
          )
          (and
            (LeftOf12 ?x)
            (RightOf14 ?x)
          )
          (and
            (LeftOf10 ?x)
            (RightOf19 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf15 ?x)
          )
          (and
            (RightOf16 ?x)
            (LeftOf11 ?x)
          )
          (and
            (LeftOf13 ?x)
            (RightOf17 ?x)
          )
          (and
            (LeftOf15 ?x)
            (RightOf16 ?x)
          )
          (and
            (RightOf15 ?x)
            (LeftOf11 ?x)
          )
          (and
            (RightOf14 ?x)
            (LeftOf14 ?x)
          )
          (and
            (RightOf15 ?x)
            (LeftOf1 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf18 ?x)
          )
        )
        (and
          (Column15 ?x)
        )
      )
      (when
        (or
          (and
            (LeftOf13 ?x)
            (RightOf18 ?x)
          )
          (and
            (RightOf15 ?x)
            (LeftOf16 ?x)
          )
          (and
            (RightOf19 ?x)
            (LeftOf3 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf18 ?x)
          )
          (and
            (LeftOf12 ?x)
            (RightOf18 ?x)
          )
          (and
            (LeftOf8 ?x)
            (RightOf17 ?x)
          )
          (and
            (RightOf15 ?x)
            (LeftOf8 ?x)
          )
          (and
            (LeftOf13 ?x)
            (RightOf16 ?x)
          )
          (and
            (RightOf16 ?x)
            (LeftOf9 ?x)
          )
          (and
            (RightOf17 ?x)
            (LeftOf11 ?x)
          )
          (and
            (LeftOf13 ?x)
            (RightOf17 ?x)
          )
          (and
            (RightOf15 ?x)
            (LeftOf10 ?x)
          )
          (and
            (RightOf17 ?x)
            (LeftOf14 ?x)
          )
          (and
            (LeftOf1 ?x)
            (RightOf19 ?x)
          )
          (and
            (RightOf15 ?x)
            (LeftOf14 ?x)
          )
          (and
            (LeftOf10 ?x)
            (RightOf17 ?x)
          )
          (and
            (LeftOf9 ?x)
            (RightOf18 ?x)
          )
          (and
            (LeftOf10 ?x)
            (RightOf16 ?x)
          )
          (and
            (LeftOf16 ?x)
            (RightOf16 ?x)
          )
          (and
            (RightOf19 ?x)
            (LeftOf4 ?x)
          )
          (and
            (LeftOf11 ?x)
            (RightOf19 ?x)
          )
          (and
            (LeftOf8 ?x)
            (RightOf19 ?x)
          )
          (and
            (RightOf15 ?x)
            (LeftOf4 ?x)
          )
          (and
            (RightOf18 ?x)
            (LeftOf3 ?x)
          )
          (and
            (LeftOf15 ?x)
            (RightOf18 ?x)
          )
          (and
            (RightOf17 ?x)
            (LeftOf9 ?x)
          )
          (and
            (RightOf17 ?x)
            (LeftOf1 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf19 ?x)
          )
          (and
            (LeftOf8 ?x)
            (RightOf16 ?x)
          )
          (and
            (LeftOf14 ?x)
            (RightOf18 ?x)
          )
          (and
            (RightOf16 ?x)
            (LeftOf1 ?x)
          )
          (and
            (RightOf16 ?x)
            (LeftOf14 ?x)
          )
          (and
            (LeftOf10 ?x)
            (RightOf18 ?x)
          )
          (and
            (LeftOf7 ?x)
            (RightOf19 ?x)
          )
          (and
            (LeftOf7 ?x)
            (RightOf16 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf16 ?x)
          )
          (and
            (LeftOf9 ?x)
            (RightOf19 ?x)
          )
          (and
            (LeftOf8 ?x)
            (RightOf18 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf19 ?x)
          )
          (and
            (RightOf16 ?x)
            (LeftOf3 ?x)
          )
          (and
            (LeftOf15 ?x)
            (RightOf17 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf17 ?x)
          )
          (and
            (RightOf19 ?x)
            (LeftOf14 ?x)
          )
          (and
            (LeftOf12 ?x)
            (RightOf19 ?x)
          )
          (and
            (LeftOf12 ?x)
            (RightOf17 ?x)
          )
          (and
            (RightOf15 ?x)
            (LeftOf3 ?x)
          )
          (and
            (RightOf17 ?x)
            (LeftOf2 ?x)
          )
          (and
            (RightOf15 ?x)
            (LeftOf1 ?x)
          )
          (and
            (RightOf15 ?x)
            (LeftOf7 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf19 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf16 ?x)
          )
          (and
            (LeftOf13 ?x)
            (RightOf19 ?x)
          )
          (and
            (LeftOf10 ?x)
            (RightOf19 ?x)
          )
          (and
            (LeftOf7 ?x)
            (RightOf17 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf15 ?x)
          )
          (and
            (RightOf16 ?x)
            (LeftOf4 ?x)
          )
          (and
            (RightOf17 ?x)
            (LeftOf3 ?x)
          )
          (and
            (LeftOf11 ?x)
            (RightOf18 ?x)
          )
          (and
            (LeftOf12 ?x)
            (RightOf16 ?x)
          )
          (and
            (RightOf18 ?x)
            (LeftOf4 ?x)
          )
          (and
            (RightOf16 ?x)
            (LeftOf11 ?x)
          )
          (and
            (LeftOf12 ?x)
            (RightOf15 ?x)
          )
          (and
            (RightOf15 ?x)
            (LeftOf13 ?x)
          )
          (and
            (RightOf15 ?x)
            (LeftOf15 ?x)
          )
          (and
            (RightOf17 ?x)
            (LeftOf4 ?x)
          )
          (and
            (RightOf18 ?x)
            (LeftOf14 ?x)
          )
          (and
            (LeftOf16 ?x)
            (RightOf19 ?x)
          )
          (and
            (LeftOf7 ?x)
            (RightOf18 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf18 ?x)
          )
          (and
            (LeftOf16 ?x)
            (RightOf18 ?x)
          )
          (and
            (RightOf15 ?x)
            (LeftOf9 ?x)
          )
          (and
            (RightOf15 ?x)
            (LeftOf11 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf15 ?x)
          )
          (and
            (RightOf17 ?x)
            (LeftOf16 ?x)
          )
          (and
            (LeftOf1 ?x)
            (RightOf18 ?x)
          )
          (and
            (LeftOf15 ?x)
            (RightOf16 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf18 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf16 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf17 ?x)
          )
          (and
            (LeftOf15 ?x)
            (RightOf19 ?x)
          )
          (and
            (LeftOf4 ?x)
            (RightOf18 ?x)
          )
          (and
            (RightOf15 ?x)
            (LeftOf2 ?x)
          )
        )
        (and
          (Column16 ?x)
        )
      )
      (when
        (or
          (and
            (LeftOf13 ?x)
            (RightOf18 ?x)
          )
          (and
            (LeftOf12 ?x)
            (RightOf18 ?x)
          )
          (and
            (RightOf19 ?x)
            (LeftOf3 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf18 ?x)
          )
          (and
            (LeftOf8 ?x)
            (RightOf17 ?x)
          )
          (and
            (LeftOf7 ?x)
            (RightOf19 ?x)
          )
          (and
            (RightOf19 ?x)
            (LeftOf17 ?x)
          )
          (and
            (LeftOf13 ?x)
            (RightOf16 ?x)
          )
          (and
            (RightOf16 ?x)
            (LeftOf9 ?x)
          )
          (and
            (RightOf17 ?x)
            (LeftOf11 ?x)
          )
          (and
            (RightOf17 ?x)
            (LeftOf14 ?x)
          )
          (and
            (LeftOf1 ?x)
            (RightOf19 ?x)
          )
          (and
            (LeftOf10 ?x)
            (RightOf17 ?x)
          )
          (and
            (LeftOf9 ?x)
            (RightOf18 ?x)
          )
          (and
            (LeftOf10 ?x)
            (RightOf16 ?x)
          )
          (and
            (LeftOf16 ?x)
            (RightOf16 ?x)
          )
          (and
            (RightOf19 ?x)
            (LeftOf4 ?x)
          )
          (and
            (LeftOf11 ?x)
            (RightOf19 ?x)
          )
          (and
            (LeftOf8 ?x)
            (RightOf19 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf19 ?x)
          )
          (and
            (RightOf18 ?x)
            (LeftOf3 ?x)
          )
          (and
            (LeftOf15 ?x)
            (RightOf18 ?x)
          )
          (and
            (RightOf17 ?x)
            (LeftOf9 ?x)
          )
          (and
            (RightOf17 ?x)
            (LeftOf1 ?x)
          )
          (and
            (RightOf18 ?x)
            (LeftOf17 ?x)
          )
          (and
            (LeftOf15 ?x)
            (RightOf16 ?x)
          )
          (and
            (LeftOf14 ?x)
            (RightOf18 ?x)
          )
          (and
            (RightOf16 ?x)
            (LeftOf1 ?x)
          )
          (and
            (RightOf16 ?x)
            (LeftOf14 ?x)
          )
          (and
            (LeftOf10 ?x)
            (RightOf18 ?x)
          )
          (and
            (RightOf16 ?x)
            (LeftOf17 ?x)
          )
          (and
            (LeftOf7 ?x)
            (RightOf16 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf16 ?x)
          )
          (and
            (LeftOf9 ?x)
            (RightOf19 ?x)
          )
          (and
            (LeftOf8 ?x)
            (RightOf18 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf19 ?x)
          )
          (and
            (RightOf16 ?x)
            (LeftOf3 ?x)
          )
          (and
            (LeftOf15 ?x)
            (RightOf17 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf17 ?x)
          )
          (and
            (RightOf19 ?x)
            (LeftOf14 ?x)
          )
          (and
            (LeftOf12 ?x)
            (RightOf17 ?x)
          )
          (and
            (LeftOf8 ?x)
            (RightOf16 ?x)
          )
          (and
            (RightOf17 ?x)
            (LeftOf2 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf16 ?x)
          )
          (and
            (LeftOf13 ?x)
            (RightOf19 ?x)
          )
          (and
            (LeftOf10 ?x)
            (RightOf19 ?x)
          )
          (and
            (LeftOf7 ?x)
            (RightOf17 ?x)
          )
          (and
            (RightOf17 ?x)
            (LeftOf17 ?x)
          )
          (and
            (RightOf16 ?x)
            (LeftOf4 ?x)
          )
          (and
            (RightOf17 ?x)
            (LeftOf3 ?x)
          )
          (and
            (LeftOf11 ?x)
            (RightOf18 ?x)
          )
          (and
            (LeftOf12 ?x)
            (RightOf16 ?x)
          )
          (and
            (RightOf18 ?x)
            (LeftOf4 ?x)
          )
          (and
            (RightOf16 ?x)
            (LeftOf11 ?x)
          )
          (and
            (LeftOf12 ?x)
            (RightOf19 ?x)
          )
          (and
            (LeftOf13 ?x)
            (RightOf17 ?x)
          )
          (and
            (RightOf17 ?x)
            (LeftOf4 ?x)
          )
          (and
            (RightOf18 ?x)
            (LeftOf14 ?x)
          )
          (and
            (LeftOf16 ?x)
            (RightOf19 ?x)
          )
          (and
            (LeftOf7 ?x)
            (RightOf18 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf18 ?x)
          )
          (and
            (LeftOf16 ?x)
            (RightOf18 ?x)
          )
          (and
            (LeftOf15 ?x)
            (RightOf19 ?x)
          )
          (and
            (RightOf17 ?x)
            (LeftOf16 ?x)
          )
          (and
            (LeftOf1 ?x)
            (RightOf18 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf19 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf16 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf17 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf18 ?x)
          )
          (and
            (LeftOf4 ?x)
            (RightOf18 ?x)
          )
        )
        (and
          (Column17 ?x)
        )
      )
      (when
        (or
          (and
            (LeftOf13 ?x)
            (RightOf18 ?x)
          )
          (and
            (LeftOf12 ?x)
            (RightOf18 ?x)
          )
          (and
            (RightOf19 ?x)
            (LeftOf3 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf18 ?x)
          )
          (and
            (LeftOf8 ?x)
            (RightOf17 ?x)
          )
          (and
            (LeftOf7 ?x)
            (RightOf19 ?x)
          )
          (and
            (RightOf19 ?x)
            (LeftOf17 ?x)
          )
          (and
            (LeftOf18 ?x)
            (RightOf19 ?x)
          )
          (and
            (LeftOf18 ?x)
            (RightOf17 ?x)
          )
          (and
            (RightOf17 ?x)
            (LeftOf11 ?x)
          )
          (and
            (RightOf17 ?x)
            (LeftOf14 ?x)
          )
          (and
            (LeftOf1 ?x)
            (RightOf19 ?x)
          )
          (and
            (LeftOf10 ?x)
            (RightOf17 ?x)
          )
          (and
            (LeftOf9 ?x)
            (RightOf18 ?x)
          )
          (and
            (RightOf17 ?x)
            (LeftOf3 ?x)
          )
          (and
            (RightOf19 ?x)
            (LeftOf4 ?x)
          )
          (and
            (LeftOf11 ?x)
            (RightOf19 ?x)
          )
          (and
            (LeftOf8 ?x)
            (RightOf19 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf19 ?x)
          )
          (and
            (RightOf18 ?x)
            (LeftOf3 ?x)
          )
          (and
            (LeftOf15 ?x)
            (RightOf18 ?x)
          )
          (and
            (RightOf17 ?x)
            (LeftOf9 ?x)
          )
          (and
            (RightOf17 ?x)
            (LeftOf1 ?x)
          )
          (and
            (RightOf18 ?x)
            (LeftOf17 ?x)
          )
          (and
            (LeftOf14 ?x)
            (RightOf18 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf19 ?x)
          )
          (and
            (LeftOf10 ?x)
            (RightOf18 ?x)
          )
          (and
            (RightOf18 ?x)
            (LeftOf4 ?x)
          )
          (and
            (LeftOf15 ?x)
            (RightOf17 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf17 ?x)
          )
          (and
            (RightOf19 ?x)
            (LeftOf14 ?x)
          )
          (and
            (LeftOf12 ?x)
            (RightOf17 ?x)
          )
          (and
            (RightOf17 ?x)
            (LeftOf2 ?x)
          )
          (and
            (LeftOf13 ?x)
            (RightOf19 ?x)
          )
          (and
            (LeftOf10 ?x)
            (RightOf19 ?x)
          )
          (and
            (LeftOf7 ?x)
            (RightOf17 ?x)
          )
          (and
            (RightOf17 ?x)
            (LeftOf17 ?x)
          )
          (and
            (LeftOf8 ?x)
            (RightOf18 ?x)
          )
          (and
            (LeftOf18 ?x)
            (RightOf18 ?x)
          )
          (and
            (LeftOf11 ?x)
            (RightOf18 ?x)
          )
          (and
            (LeftOf7 ?x)
            (RightOf18 ?x)
          )
          (and
            (LeftOf9 ?x)
            (RightOf19 ?x)
          )
          (and
            (LeftOf12 ?x)
            (RightOf19 ?x)
          )
          (and
            (LeftOf13 ?x)
            (RightOf17 ?x)
          )
          (and
            (RightOf17 ?x)
            (LeftOf4 ?x)
          )
          (and
            (RightOf18 ?x)
            (LeftOf14 ?x)
          )
          (and
            (LeftOf16 ?x)
            (RightOf19 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf18 ?x)
          )
          (and
            (LeftOf16 ?x)
            (RightOf18 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf18 ?x)
          )
          (and
            (RightOf17 ?x)
            (LeftOf16 ?x)
          )
          (and
            (LeftOf1 ?x)
            (RightOf18 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf19 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf17 ?x)
          )
          (and
            (LeftOf15 ?x)
            (RightOf19 ?x)
          )
          (and
            (LeftOf4 ?x)
            (RightOf18 ?x)
          )
        )
        (and
          (Column18 ?x)
        )
      )
      (when
        (or
          (and
            (LeftOf13 ?x)
            (RightOf18 ?x)
          )
          (and
            (LeftOf12 ?x)
            (RightOf18 ?x)
          )
          (and
            (RightOf19 ?x)
            (LeftOf3 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf18 ?x)
          )
          (and
            (LeftOf7 ?x)
            (RightOf19 ?x)
          )
          (and
            (RightOf19 ?x)
            (LeftOf17 ?x)
          )
          (and
            (LeftOf18 ?x)
            (RightOf19 ?x)
          )
          (and
            (LeftOf1 ?x)
            (RightOf19 ?x)
          )
          (and
            (LeftOf9 ?x)
            (RightOf18 ?x)
          )
          (and
            (RightOf19 ?x)
            (LeftOf4 ?x)
          )
          (and
            (LeftOf11 ?x)
            (RightOf19 ?x)
          )
          (and
            (LeftOf8 ?x)
            (RightOf19 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf19 ?x)
          )
          (and
            (RightOf18 ?x)
            (LeftOf3 ?x)
          )
          (and
            (LeftOf12 ?x)
            (RightOf19 ?x)
          )
          (and
            (RightOf18 ?x)
            (LeftOf17 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf19 ?x)
          )
          (and
            (LeftOf10 ?x)
            (RightOf18 ?x)
          )
          (and
            (LeftOf19 ?x)
            (RightOf18 ?x)
          )
          (and
            (RightOf18 ?x)
            (LeftOf4 ?x)
          )
          (and
            (RightOf19 ?x)
            (LeftOf14 ?x)
          )
          (and
            (LeftOf10 ?x)
            (RightOf19 ?x)
          )
          (and
            (LeftOf19 ?x)
            (RightOf19 ?x)
          )
          (and
            (LeftOf8 ?x)
            (RightOf18 ?x)
          )
          (and
            (LeftOf18 ?x)
            (RightOf18 ?x)
          )
          (and
            (LeftOf11 ?x)
            (RightOf18 ?x)
          )
          (and
            (LeftOf7 ?x)
            (RightOf18 ?x)
          )
          (and
            (LeftOf9 ?x)
            (RightOf19 ?x)
          )
          (and
            (LeftOf15 ?x)
            (RightOf18 ?x)
          )
          (and
            (RightOf18 ?x)
            (LeftOf14 ?x)
          )
          (and
            (LeftOf16 ?x)
            (RightOf19 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf18 ?x)
          )
          (and
            (LeftOf16 ?x)
            (RightOf18 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf18 ?x)
          )
          (and
            (LeftOf13 ?x)
            (RightOf19 ?x)
          )
          (and
            (LeftOf1 ?x)
            (RightOf18 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf19 ?x)
          )
          (and
            (LeftOf15 ?x)
            (RightOf19 ?x)
          )
        )
        (and
          (Column19 ?x)
        )
      )
    )
  )
  (:action moveDown
    :parameters ( ?x )
    :precondition ( and
      (not (CheckConsistency))
      (not (Error))
      (or
        (BelowOf7 ?x)
        (AboveOf9 ?x)
        (BelowOf18 ?x)
        (BelowOf1 ?x)
        (BelowOf17 ?x)
        (AboveOf7 ?x)
        (BelowOf16 ?x)
        (AboveOf1 ?x)
        (AboveOf11 ?x)
        (AboveOf19 ?x)
        (BelowOf14 ?x)
        (BelowOf8 ?x)
        (BelowOf3 ?x)
        (AboveOf14 ?x)
        (AboveOf3 ?x)
        (BelowOf9 ?x)
        (AboveOf16 ?x)
        (BelowOf20 ?x)
        (BelowOf2 ?x)
        (BelowOf5 ?x)
        (BelowOf10 ?x)
        (BelowOf11 ?x)
        (BelowOf19 ?x)
        (AboveOf18 ?x)
        (BelowOf13 ?x)
        (AboveOf17 ?x)
        (BelowOf4 ?x)
        (AboveOf10 ?x)
        (AboveOf0 ?x)
        (AboveOf8 ?x)
        (AboveOf6 ?x)
        (AboveOf5 ?x)
        (AboveOf13 ?x)
        (Rows ?x)
        (AboveOf4 ?x)
        (BelowOf12 ?x)
        (AboveOf15 ?x)
        (AboveOf12 ?x)
        (BelowOf6 ?x)
        (BelowOf15 ?x)
        (AboveOf2 ?x)
      )
    )
    :effect ( and
      (CheckConsistency)
      (when
        (or
          (BelowOf1 ?x)
          (BelowOf2 ?x)
        )
        (and
          (BelowOf1 ?x)
        )
      )
      (when
        (or
          (BelowOf3 ?x)
          (BelowOf1 ?x)
          (BelowOf2 ?x)
        )
        (and
          (BelowOf2 ?x)
        )
      )
      (when
        (or
          (BelowOf4 ?x)
          (BelowOf3 ?x)
          (BelowOf1 ?x)
          (BelowOf2 ?x)
        )
        (and
          (BelowOf3 ?x)
        )
      )
      (when
        (or
          (BelowOf5 ?x)
          (BelowOf4 ?x)
          (BelowOf3 ?x)
          (BelowOf1 ?x)
          (BelowOf2 ?x)
        )
        (and
          (BelowOf4 ?x)
        )
      )
      (when
        (or
          (BelowOf3 ?x)
          (BelowOf4 ?x)
          (BelowOf1 ?x)
          (BelowOf2 ?x)
          (BelowOf5 ?x)
          (BelowOf6 ?x)
        )
        (and
          (BelowOf5 ?x)
        )
      )
      (when
        (or
          (BelowOf3 ?x)
          (BelowOf7 ?x)
          (BelowOf4 ?x)
          (BelowOf1 ?x)
          (BelowOf2 ?x)
          (BelowOf5 ?x)
          (BelowOf6 ?x)
        )
        (and
          (BelowOf6 ?x)
        )
      )
      (when
        (or
          (BelowOf8 ?x)
          (BelowOf3 ?x)
          (BelowOf7 ?x)
          (BelowOf4 ?x)
          (BelowOf1 ?x)
          (BelowOf2 ?x)
          (BelowOf5 ?x)
          (BelowOf6 ?x)
        )
        (and
          (BelowOf7 ?x)
        )
      )
      (when
        (or
          (BelowOf8 ?x)
          (BelowOf3 ?x)
          (BelowOf9 ?x)
          (BelowOf7 ?x)
          (BelowOf4 ?x)
          (BelowOf1 ?x)
          (BelowOf2 ?x)
          (BelowOf5 ?x)
          (BelowOf6 ?x)
        )
        (and
          (BelowOf8 ?x)
        )
      )
      (when
        (or
          (BelowOf8 ?x)
          (BelowOf3 ?x)
          (BelowOf9 ?x)
          (BelowOf7 ?x)
          (BelowOf4 ?x)
          (BelowOf1 ?x)
          (BelowOf2 ?x)
          (BelowOf5 ?x)
          (BelowOf6 ?x)
          (BelowOf10 ?x)
        )
        (and
          (BelowOf9 ?x)
        )
      )
      (when
        (or
          (BelowOf8 ?x)
          (BelowOf3 ?x)
          (BelowOf9 ?x)
          (BelowOf7 ?x)
          (BelowOf4 ?x)
          (BelowOf1 ?x)
          (BelowOf2 ?x)
          (BelowOf5 ?x)
          (BelowOf6 ?x)
          (BelowOf11 ?x)
          (BelowOf10 ?x)
        )
        (and
          (BelowOf10 ?x)
        )
      )
      (when
        (or
          (BelowOf8 ?x)
          (BelowOf3 ?x)
          (BelowOf9 ?x)
          (BelowOf7 ?x)
          (BelowOf12 ?x)
          (BelowOf4 ?x)
          (BelowOf1 ?x)
          (BelowOf2 ?x)
          (BelowOf5 ?x)
          (BelowOf6 ?x)
          (BelowOf11 ?x)
          (BelowOf10 ?x)
        )
        (and
          (BelowOf11 ?x)
        )
      )
      (when
        (or
          (BelowOf8 ?x)
          (BelowOf13 ?x)
          (BelowOf3 ?x)
          (BelowOf9 ?x)
          (BelowOf7 ?x)
          (BelowOf12 ?x)
          (BelowOf4 ?x)
          (BelowOf1 ?x)
          (BelowOf2 ?x)
          (BelowOf5 ?x)
          (BelowOf6 ?x)
          (BelowOf11 ?x)
          (BelowOf10 ?x)
        )
        (and
          (BelowOf12 ?x)
        )
      )
      (when
        (or
          (BelowOf14 ?x)
          (BelowOf8 ?x)
          (BelowOf13 ?x)
          (BelowOf3 ?x)
          (BelowOf9 ?x)
          (BelowOf7 ?x)
          (BelowOf12 ?x)
          (BelowOf4 ?x)
          (BelowOf1 ?x)
          (BelowOf2 ?x)
          (BelowOf5 ?x)
          (BelowOf6 ?x)
          (BelowOf11 ?x)
          (BelowOf10 ?x)
        )
        (and
          (BelowOf13 ?x)
        )
      )
      (when
        (or
          (BelowOf14 ?x)
          (BelowOf8 ?x)
          (BelowOf13 ?x)
          (BelowOf3 ?x)
          (BelowOf9 ?x)
          (BelowOf7 ?x)
          (BelowOf12 ?x)
          (BelowOf4 ?x)
          (BelowOf1 ?x)
          (BelowOf2 ?x)
          (BelowOf5 ?x)
          (BelowOf6 ?x)
          (BelowOf15 ?x)
          (BelowOf11 ?x)
          (BelowOf10 ?x)
        )
        (and
          (BelowOf14 ?x)
        )
      )
      (when
        (or
          (BelowOf14 ?x)
          (BelowOf8 ?x)
          (BelowOf13 ?x)
          (BelowOf3 ?x)
          (BelowOf9 ?x)
          (BelowOf7 ?x)
          (BelowOf12 ?x)
          (BelowOf4 ?x)
          (BelowOf1 ?x)
          (BelowOf2 ?x)
          (BelowOf5 ?x)
          (BelowOf11 ?x)
          (BelowOf6 ?x)
          (BelowOf15 ?x)
          (BelowOf16 ?x)
          (BelowOf10 ?x)
        )
        (and
          (BelowOf15 ?x)
        )
      )
      (when
        (or
          (BelowOf14 ?x)
          (BelowOf8 ?x)
          (BelowOf13 ?x)
          (BelowOf3 ?x)
          (BelowOf9 ?x)
          (BelowOf7 ?x)
          (BelowOf12 ?x)
          (BelowOf11 ?x)
          (BelowOf4 ?x)
          (BelowOf1 ?x)
          (BelowOf2 ?x)
          (BelowOf5 ?x)
          (BelowOf17 ?x)
          (BelowOf6 ?x)
          (BelowOf15 ?x)
          (BelowOf16 ?x)
          (BelowOf10 ?x)
        )
        (and
          (BelowOf16 ?x)
        )
      )
      (when
        (or
          (BelowOf14 ?x)
          (BelowOf8 ?x)
          (BelowOf13 ?x)
          (BelowOf3 ?x)
          (BelowOf9 ?x)
          (BelowOf7 ?x)
          (BelowOf12 ?x)
          (BelowOf18 ?x)
          (BelowOf11 ?x)
          (BelowOf4 ?x)
          (BelowOf1 ?x)
          (BelowOf2 ?x)
          (BelowOf5 ?x)
          (BelowOf17 ?x)
          (BelowOf6 ?x)
          (BelowOf15 ?x)
          (BelowOf16 ?x)
          (BelowOf10 ?x)
        )
        (and
          (BelowOf17 ?x)
        )
      )
      (when
        (or
          (BelowOf14 ?x)
          (BelowOf8 ?x)
          (BelowOf13 ?x)
          (BelowOf10 ?x)
          (BelowOf9 ?x)
          (BelowOf7 ?x)
          (BelowOf12 ?x)
          (BelowOf18 ?x)
          (BelowOf11 ?x)
          (BelowOf4 ?x)
          (BelowOf1 ?x)
          (BelowOf2 ?x)
          (BelowOf5 ?x)
          (BelowOf17 ?x)
          (BelowOf6 ?x)
          (BelowOf15 ?x)
          (BelowOf16 ?x)
          (BelowOf19 ?x)
          (BelowOf3 ?x)
        )
        (and
          (BelowOf18 ?x)
        )
      )
      (when
        (or
          (BelowOf14 ?x)
          (BelowOf8 ?x)
          (BelowOf13 ?x)
          (BelowOf10 ?x)
          (BelowOf9 ?x)
          (BelowOf7 ?x)
          (BelowOf12 ?x)
          (BelowOf18 ?x)
          (BelowOf4 ?x)
          (BelowOf20 ?x)
          (BelowOf2 ?x)
          (BelowOf5 ?x)
          (BelowOf17 ?x)
          (BelowOf6 ?x)
          (BelowOf15 ?x)
          (BelowOf1 ?x)
          (BelowOf11 ?x)
          (BelowOf19 ?x)
          (BelowOf16 ?x)
          (BelowOf3 ?x)
        )
        (and
          (BelowOf19 ?x)
        )
      )
      (when
        (or
          (AboveOf18 ?x)
          (AboveOf1 ?x)
          (AboveOf5 ?x)
          (AboveOf14 ?x)
          (AboveOf13 ?x)
          (AboveOf3 ?x)
          (AboveOf4 ?x)
          (AboveOf16 ?x)
          (AboveOf9 ?x)
          (AboveOf8 ?x)
          (AboveOf15 ?x)
          (AboveOf17 ?x)
          (AboveOf12 ?x)
          (AboveOf7 ?x)
          (AboveOf2 ?x)
          (AboveOf10 ?x)
          (AboveOf11 ?x)
          (AboveOf6 ?x)
          (AboveOf19 ?x)
        )
        (and
          (AboveOf0 ?x)
          (not (AboveOf1 ?x))
        )
      )
      (when
        (or
          (AboveOf18 ?x)
          (AboveOf5 ?x)
          (AboveOf14 ?x)
          (AboveOf13 ?x)
          (AboveOf3 ?x)
          (AboveOf4 ?x)
          (AboveOf16 ?x)
          (AboveOf9 ?x)
          (AboveOf8 ?x)
          (AboveOf15 ?x)
          (AboveOf17 ?x)
          (AboveOf12 ?x)
          (AboveOf7 ?x)
          (AboveOf2 ?x)
          (AboveOf10 ?x)
          (AboveOf6 ?x)
          (AboveOf11 ?x)
          (AboveOf19 ?x)
        )
        (and
          (AboveOf1 ?x)
          (not (AboveOf2 ?x))
        )
      )
      (when
        (or
          (AboveOf18 ?x)
          (AboveOf5 ?x)
          (AboveOf14 ?x)
          (AboveOf13 ?x)
          (AboveOf3 ?x)
          (AboveOf4 ?x)
          (AboveOf16 ?x)
          (AboveOf9 ?x)
          (AboveOf15 ?x)
          (AboveOf17 ?x)
          (AboveOf12 ?x)
          (AboveOf7 ?x)
          (AboveOf8 ?x)
          (AboveOf10 ?x)
          (AboveOf6 ?x)
          (AboveOf11 ?x)
          (AboveOf19 ?x)
        )
        (and
          (AboveOf2 ?x)
          (not (AboveOf3 ?x))
        )
      )
      (when
        (or
          (AboveOf18 ?x)
          (AboveOf5 ?x)
          (AboveOf14 ?x)
          (AboveOf13 ?x)
          (AboveOf4 ?x)
          (AboveOf16 ?x)
          (AboveOf9 ?x)
          (AboveOf15 ?x)
          (AboveOf17 ?x)
          (AboveOf12 ?x)
          (AboveOf7 ?x)
          (AboveOf8 ?x)
          (AboveOf10 ?x)
          (AboveOf6 ?x)
          (AboveOf11 ?x)
          (AboveOf19 ?x)
        )
        (and
          (AboveOf3 ?x)
          (not (AboveOf4 ?x))
        )
      )
      (when
        (or
          (AboveOf18 ?x)
          (AboveOf5 ?x)
          (AboveOf14 ?x)
          (AboveOf13 ?x)
          (AboveOf16 ?x)
          (AboveOf9 ?x)
          (AboveOf15 ?x)
          (AboveOf17 ?x)
          (AboveOf12 ?x)
          (AboveOf10 ?x)
          (AboveOf8 ?x)
          (AboveOf7 ?x)
          (AboveOf6 ?x)
          (AboveOf11 ?x)
          (AboveOf19 ?x)
        )
        (and
          (AboveOf4 ?x)
          (not (AboveOf5 ?x))
        )
      )
      (when
        (or
          (AboveOf18 ?x)
          (AboveOf14 ?x)
          (AboveOf13 ?x)
          (AboveOf16 ?x)
          (AboveOf9 ?x)
          (AboveOf15 ?x)
          (AboveOf17 ?x)
          (AboveOf12 ?x)
          (AboveOf10 ?x)
          (AboveOf8 ?x)
          (AboveOf7 ?x)
          (AboveOf6 ?x)
          (AboveOf11 ?x)
          (AboveOf19 ?x)
        )
        (and
          (AboveOf5 ?x)
          (not (AboveOf6 ?x))
        )
      )
      (when
        (or
          (AboveOf18 ?x)
          (AboveOf14 ?x)
          (AboveOf13 ?x)
          (AboveOf16 ?x)
          (AboveOf9 ?x)
          (AboveOf15 ?x)
          (AboveOf17 ?x)
          (AboveOf12 ?x)
          (AboveOf10 ?x)
          (AboveOf8 ?x)
          (AboveOf7 ?x)
          (AboveOf11 ?x)
          (AboveOf19 ?x)
        )
        (and
          (AboveOf6 ?x)
          (not (AboveOf7 ?x))
        )
      )
      (when
        (or
          (AboveOf18 ?x)
          (AboveOf14 ?x)
          (AboveOf13 ?x)
          (AboveOf16 ?x)
          (AboveOf9 ?x)
          (AboveOf15 ?x)
          (AboveOf17 ?x)
          (AboveOf12 ?x)
          (AboveOf10 ?x)
          (AboveOf8 ?x)
          (AboveOf11 ?x)
          (AboveOf19 ?x)
        )
        (and
          (AboveOf7 ?x)
          (not (AboveOf8 ?x))
        )
      )
      (when
        (or
          (AboveOf18 ?x)
          (AboveOf14 ?x)
          (AboveOf13 ?x)
          (AboveOf16 ?x)
          (AboveOf9 ?x)
          (AboveOf15 ?x)
          (AboveOf17 ?x)
          (AboveOf12 ?x)
          (AboveOf10 ?x)
          (AboveOf11 ?x)
          (AboveOf19 ?x)
        )
        (and
          (AboveOf8 ?x)
          (not (AboveOf9 ?x))
        )
      )
      (when
        (or
          (AboveOf18 ?x)
          (AboveOf14 ?x)
          (AboveOf13 ?x)
          (AboveOf16 ?x)
          (AboveOf15 ?x)
          (AboveOf17 ?x)
          (AboveOf12 ?x)
          (AboveOf10 ?x)
          (AboveOf11 ?x)
          (AboveOf19 ?x)
        )
        (and
          (AboveOf9 ?x)
          (not (AboveOf10 ?x))
        )
      )
      (when
        (or
          (AboveOf18 ?x)
          (AboveOf14 ?x)
          (AboveOf13 ?x)
          (AboveOf16 ?x)
          (AboveOf15 ?x)
          (AboveOf17 ?x)
          (AboveOf12 ?x)
          (AboveOf11 ?x)
          (AboveOf19 ?x)
        )
        (and
          (AboveOf10 ?x)
          (not (AboveOf11 ?x))
        )
      )
      (when
        (or
          (AboveOf18 ?x)
          (AboveOf14 ?x)
          (AboveOf13 ?x)
          (AboveOf16 ?x)
          (AboveOf15 ?x)
          (AboveOf17 ?x)
          (AboveOf12 ?x)
          (AboveOf19 ?x)
        )
        (and
          (AboveOf11 ?x)
          (not (AboveOf12 ?x))
        )
      )
      (when
        (or
          (AboveOf18 ?x)
          (AboveOf14 ?x)
          (AboveOf13 ?x)
          (AboveOf16 ?x)
          (AboveOf15 ?x)
          (AboveOf17 ?x)
          (AboveOf19 ?x)
        )
        (and
          (AboveOf12 ?x)
          (not (AboveOf13 ?x))
        )
      )
      (when
        (or
          (AboveOf18 ?x)
          (AboveOf14 ?x)
          (AboveOf16 ?x)
          (AboveOf15 ?x)
          (AboveOf17 ?x)
          (AboveOf19 ?x)
        )
        (and
          (AboveOf13 ?x)
          (not (AboveOf14 ?x))
        )
      )
      (when
        (or
          (AboveOf16 ?x)
          (AboveOf15 ?x)
          (AboveOf17 ?x)
          (AboveOf19 ?x)
          (AboveOf18 ?x)
        )
        (and
          (AboveOf14 ?x)
          (not (AboveOf15 ?x))
        )
      )
      (when
        (or
          (AboveOf16 ?x)
          (AboveOf17 ?x)
          (AboveOf19 ?x)
          (AboveOf18 ?x)
        )
        (and
          (AboveOf15 ?x)
          (not (AboveOf16 ?x))
        )
      )
      (when
        (or
          (AboveOf18 ?x)
          (AboveOf17 ?x)
          (AboveOf19 ?x)
        )
        (and
          (AboveOf16 ?x)
          (not (AboveOf17 ?x))
        )
      )
      (when
        (or
          (AboveOf18 ?x)
          (AboveOf19 ?x)
        )
        (and
          (AboveOf17 ?x)
          (not (AboveOf18 ?x))
        )
      )
      (when
        (AboveOf19 ?x)
        (and
          (AboveOf18 ?x)
          (not (AboveOf19 ?x))
        )
      )
      (when
        (Row1 ?x)
        (and
          (Row0 ?x)
          (not (Row1 ?x))
        )
      )
      (when
        (Row2 ?x)
        (and
          (Row1 ?x)
          (not (Row2 ?x))
        )
      )
      (when
        (Row3 ?x)
        (and
          (Row2 ?x)
          (not (Row3 ?x))
        )
      )
      (when
        (Row4 ?x)
        (and
          (Row3 ?x)
          (not (Row4 ?x))
        )
      )
      (when
        (Row5 ?x)
        (and
          (Row4 ?x)
          (not (Row5 ?x))
        )
      )
      (when
        (Row6 ?x)
        (and
          (Row5 ?x)
          (not (Row6 ?x))
        )
      )
      (when
        (Row7 ?x)
        (and
          (Row6 ?x)
          (not (Row7 ?x))
        )
      )
      (when
        (Row8 ?x)
        (and
          (Row7 ?x)
          (not (Row8 ?x))
        )
      )
      (when
        (Row9 ?x)
        (and
          (Row8 ?x)
          (not (Row9 ?x))
        )
      )
      (when
        (Row10 ?x)
        (and
          (Row9 ?x)
          (not (Row10 ?x))
        )
      )
      (when
        (Row11 ?x)
        (and
          (Row10 ?x)
          (not (Row11 ?x))
        )
      )
      (when
        (Row12 ?x)
        (and
          (Row11 ?x)
          (not (Row12 ?x))
        )
      )
      (when
        (Row13 ?x)
        (and
          (Row12 ?x)
          (not (Row13 ?x))
        )
      )
      (when
        (Row14 ?x)
        (and
          (Row13 ?x)
          (not (Row14 ?x))
        )
      )
      (when
        (Row15 ?x)
        (and
          (Row14 ?x)
          (not (Row15 ?x))
        )
      )
      (when
        (Row16 ?x)
        (and
          (Row15 ?x)
          (not (Row16 ?x))
        )
      )
      (when
        (Row17 ?x)
        (and
          (Row16 ?x)
          (not (Row17 ?x))
        )
      )
      (when
        (Row18 ?x)
        (and
          (Row17 ?x)
          (not (Row18 ?x))
        )
      )
      (when
        (Row19 ?x)
        (and
          (Row18 ?x)
          (not (Row19 ?x))
        )
      )
      (when
        (or
          (and
            (BelowOf2 ?x)
            (AboveOf3 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf8 ?x)
          )
          (and
            (AboveOf15 ?x)
            (BelowOf1 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf19 ?x)
          )
          (and
            (AboveOf5 ?x)
            (BelowOf2 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf12 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf12 ?x)
          )
          (and
            (AboveOf4 ?x)
            (BelowOf1 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf8 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf10 ?x)
          )
          (and
            (AboveOf9 ?x)
            (BelowOf1 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf3 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf11 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf10 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf17 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf1 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf6 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf1 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf9 ?x)
          )
          (and
            (AboveOf2 ?x)
            (BelowOf1 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf6 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf2 ?x)
          )
          (and
            (AboveOf17 ?x)
            (BelowOf1 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf4 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf16 ?x)
          )
          (and
            (AboveOf5 ?x)
            (BelowOf1 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf2 ?x)
          )
          (and
            (AboveOf7 ?x)
            (BelowOf1 ?x)
          )
          (and
            (AboveOf16 ?x)
            (BelowOf1 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf2 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf19 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf15 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf7 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf1 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf11 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf18 ?x)
          )
          (and
            (AboveOf1 ?x)
            (BelowOf1 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf18 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf17 ?x)
          )
        )
        (and
          (Row0 ?x)
        )
      )
      (when
        (or
          (and
            (BelowOf2 ?x)
            (AboveOf3 ?x)
          )
          (and
            (AboveOf18 ?x)
            (BelowOf3 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf8 ?x)
          )
          (and
            (AboveOf15 ?x)
            (BelowOf1 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf19 ?x)
          )
          (and
            (AboveOf5 ?x)
            (BelowOf2 ?x)
          )
          (and
            (BelowOf3 ?x)
            (AboveOf11 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf3 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf12 ?x)
          )
          (and
            (BelowOf3 ?x)
            (AboveOf12 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf12 ?x)
          )
          (and
            (AboveOf4 ?x)
            (BelowOf1 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf8 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf10 ?x)
          )
          (and
            (AboveOf9 ?x)
            (BelowOf1 ?x)
          )
          (and
            (BelowOf3 ?x)
            (AboveOf19 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf3 ?x)
          )
          (and
            (AboveOf2 ?x)
            (BelowOf2 ?x)
          )
          (and
            (AboveOf10 ?x)
            (BelowOf3 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf1 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf10 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf17 ?x)
          )
          (and
            (AboveOf4 ?x)
            (BelowOf3 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf6 ?x)
          )
          (and
            (BelowOf3 ?x)
            (AboveOf8 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf1 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf9 ?x)
          )
          (and
            (BelowOf3 ?x)
            (AboveOf3 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf6 ?x)
          )
          (and
            (AboveOf2 ?x)
            (BelowOf3 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf2 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf4 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf16 ?x)
          )
          (and
            (AboveOf15 ?x)
            (BelowOf3 ?x)
          )
          (and
            (AboveOf9 ?x)
            (BelowOf3 ?x)
          )
          (and
            (AboveOf5 ?x)
            (BelowOf1 ?x)
          )
          (and
            (AboveOf16 ?x)
            (BelowOf3 ?x)
          )
          (and
            (AboveOf7 ?x)
            (BelowOf1 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf2 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf19 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf3 ?x)
          )
          (and
            (AboveOf17 ?x)
            (BelowOf3 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf15 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf7 ?x)
          )
          (and
            (AboveOf17 ?x)
            (BelowOf1 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf11 ?x)
          )
          (and
            (AboveOf7 ?x)
            (BelowOf3 ?x)
          )
          (and
            (AboveOf2 ?x)
            (BelowOf1 ?x)
          )
          (and
            (AboveOf6 ?x)
            (BelowOf3 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf11 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf18 ?x)
          )
          (and
            (AboveOf16 ?x)
            (BelowOf1 ?x)
          )
          (and
            (AboveOf5 ?x)
            (BelowOf3 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf18 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf17 ?x)
          )
        )
        (and
          (Row1 ?x)
        )
      )
      (when
        (or
          (and
            (BelowOf2 ?x)
            (AboveOf3 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf8 ?x)
          )
          (and
            (AboveOf18 ?x)
            (BelowOf3 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf8 ?x)
          )
          (and
            (AboveOf15 ?x)
            (BelowOf1 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf19 ?x)
          )
          (and
            (AboveOf5 ?x)
            (BelowOf2 ?x)
          )
          (and
            (BelowOf3 ?x)
            (AboveOf11 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf12 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf2 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf3 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf4 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf11 ?x)
          )
          (and
            (BelowOf3 ?x)
            (AboveOf12 ?x)
          )
          (and
            (AboveOf7 ?x)
            (BelowOf4 ?x)
          )
          (and
            (AboveOf11 ?x)
            (BelowOf3 ?x)
          )
          (and
            (AboveOf4 ?x)
            (BelowOf1 ?x)
          )
          (and
            (AboveOf19 ?x)
            (BelowOf3 ?x)
          )
          (and
            (AboveOf15 ?x)
            (BelowOf4 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf10 ?x)
          )
          (and
            (AboveOf9 ?x)
            (BelowOf1 ?x)
          )
          (and
            (BelowOf3 ?x)
            (AboveOf19 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf3 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf10 ?x)
          )
          (and
            (AboveOf10 ?x)
            (BelowOf3 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf1 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf10 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf12 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf17 ?x)
          )
          (and
            (AboveOf4 ?x)
            (BelowOf3 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf6 ?x)
          )
          (and
            (BelowOf3 ?x)
            (AboveOf8 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf1 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf19 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf9 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf18 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf6 ?x)
          )
          (and
            (AboveOf3 ?x)
            (BelowOf3 ?x)
          )
          (and
            (AboveOf5 ?x)
            (BelowOf4 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf12 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf4 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf16 ?x)
          )
          (and
            (AboveOf15 ?x)
            (BelowOf3 ?x)
          )
          (and
            (AboveOf9 ?x)
            (BelowOf3 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf8 ?x)
          )
          (and
            (AboveOf5 ?x)
            (BelowOf1 ?x)
          )
          (and
            (AboveOf16 ?x)
            (BelowOf3 ?x)
          )
          (and
            (AboveOf7 ?x)
            (BelowOf1 ?x)
          )
          (and
            (AboveOf16 ?x)
            (BelowOf4 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf2 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf19 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf18 ?x)
          )
          (and
            (AboveOf17 ?x)
            (BelowOf3 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf15 ?x)
          )
          (and
            (AboveOf4 ?x)
            (BelowOf4 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf7 ?x)
          )
          (and
            (AboveOf17 ?x)
            (BelowOf1 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf11 ?x)
          )
          (and
            (AboveOf16 ?x)
            (BelowOf1 ?x)
          )
          (and
            (AboveOf7 ?x)
            (BelowOf3 ?x)
          )
          (and
            (AboveOf6 ?x)
            (BelowOf3 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf11 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf6 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf17 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf3 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf4 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf9 ?x)
          )
          (and
            (AboveOf5 ?x)
            (BelowOf3 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf3 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf18 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf17 ?x)
          )
        )
        (and
          (Row2 ?x)
        )
      )
      (when
        (or
          (and
            (BelowOf2 ?x)
            (AboveOf11 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf18 ?x)
          )
          (and
            (AboveOf18 ?x)
            (BelowOf3 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf8 ?x)
          )
          (and
            (AboveOf15 ?x)
            (BelowOf1 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf19 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf6 ?x)
          )
          (and
            (BelowOf3 ?x)
            (AboveOf11 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf11 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf12 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf2 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf3 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf4 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf12 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf11 ?x)
          )
          (and
            (BelowOf3 ?x)
            (AboveOf12 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf18 ?x)
          )
          (and
            (AboveOf11 ?x)
            (BelowOf3 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf9 ?x)
          )
          (and
            (AboveOf4 ?x)
            (BelowOf1 ?x)
          )
          (and
            (AboveOf19 ?x)
            (BelowOf3 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf8 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf10 ?x)
          )
          (and
            (AboveOf9 ?x)
            (BelowOf1 ?x)
          )
          (and
            (BelowOf3 ?x)
            (AboveOf19 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf6 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf10 ?x)
          )
          (and
            (AboveOf10 ?x)
            (BelowOf3 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf1 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf10 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf12 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf7 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf17 ?x)
          )
          (and
            (AboveOf4 ?x)
            (BelowOf3 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf5 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf6 ?x)
          )
          (and
            (BelowOf3 ?x)
            (AboveOf8 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf1 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf19 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf9 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf16 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf6 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf13 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf12 ?x)
          )
          (and
            (AboveOf5 ?x)
            (BelowOf4 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf14 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf4 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf16 ?x)
          )
          (and
            (AboveOf15 ?x)
            (BelowOf3 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf10 ?x)
          )
          (and
            (AboveOf9 ?x)
            (BelowOf3 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf8 ?x)
          )
          (and
            (AboveOf5 ?x)
            (BelowOf1 ?x)
          )
          (and
            (AboveOf16 ?x)
            (BelowOf3 ?x)
          )
          (and
            (AboveOf5 ?x)
            (BelowOf2 ?x)
          )
          (and
            (AboveOf7 ?x)
            (BelowOf1 ?x)
          )
          (and
            (AboveOf16 ?x)
            (BelowOf4 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf2 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf19 ?x)
          )
          (and
            (AboveOf15 ?x)
            (BelowOf4 ?x)
          )
          (and
            (AboveOf17 ?x)
            (BelowOf3 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf15 ?x)
          )
          (and
            (AboveOf4 ?x)
            (BelowOf4 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf7 ?x)
          )
          (and
            (AboveOf17 ?x)
            (BelowOf1 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf11 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf16 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf19 ?x)
          )
          (and
            (AboveOf16 ?x)
            (BelowOf1 ?x)
          )
          (and
            (AboveOf7 ?x)
            (BelowOf3 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf8 ?x)
          )
          (and
            (AboveOf6 ?x)
            (BelowOf3 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf15 ?x)
          )
          (and
            (AboveOf7 ?x)
            (BelowOf4 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf18 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf4 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf17 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf9 ?x)
          )
          (and
            (AboveOf5 ?x)
            (BelowOf3 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf4 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf3 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf17 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf18 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf17 ?x)
          )
        )
        (and
          (Row3 ?x)
        )
      )
      (when
        (or
          (and
            (BelowOf3 ?x)
            (AboveOf12 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf11 ?x)
          )
          (and
            (AboveOf6 ?x)
            (BelowOf3 ?x)
          )
          (and
            (AboveOf5 ?x)
            (BelowOf4 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf14 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf8 ?x)
          )
          (and
            (AboveOf7 ?x)
            (BelowOf6 ?x)
          )
          (and
            (AboveOf5 ?x)
            (BelowOf2 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf19 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf18 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf15 ?x)
          )
          (and
            (AboveOf17 ?x)
            (BelowOf1 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf6 ?x)
          )
          (and
            (BelowOf6 ?x)
            (AboveOf19 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf4 ?x)
          )
          (and
            (AboveOf16 ?x)
            (BelowOf6 ?x)
          )
          (and
            (BelowOf3 ?x)
            (AboveOf11 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf11 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf16 ?x)
          )
          (and
            (AboveOf19 ?x)
            (BelowOf3 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf8 ?x)
          )
          (and
            (AboveOf9 ?x)
            (BelowOf6 ?x)
          )
          (and
            (BelowOf6 ?x)
            (AboveOf8 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf12 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf7 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf6 ?x)
          )
          (and
            (BelowOf3 ?x)
            (AboveOf8 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf19 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf6 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf2 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf12 ?x)
          )
          (and
            (AboveOf7 ?x)
            (BelowOf4 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf9 ?x)
          )
          (and
            (AboveOf17 ?x)
            (BelowOf3 ?x)
          )
          (and
            (AboveOf5 ?x)
            (BelowOf6 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf11 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf16 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf6 ?x)
          )
          (and
            (AboveOf15 ?x)
            (BelowOf6 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf17 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf17 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf8 ?x)
          )
          (and
            (AboveOf15 ?x)
            (BelowOf1 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf6 ?x)
          )
          (and
            (AboveOf10 ?x)
            (BelowOf6 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf3 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf12 ?x)
          )
          (and
            (AboveOf11 ?x)
            (BelowOf3 ?x)
          )
          (and
            (AboveOf15 ?x)
            (BelowOf4 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf10 ?x)
          )
          (and
            (BelowOf3 ?x)
            (AboveOf19 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf6 ?x)
          )
          (and
            (AboveOf17 ?x)
            (BelowOf6 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf10 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf1 ?x)
          )
          (and
            (AboveOf16 ?x)
            (BelowOf4 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf16 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf13 ?x)
          )
          (and
            (AboveOf5 ?x)
            (BelowOf1 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf3 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf5 ?x)
          )
          (and
            (BelowOf6 ?x)
            (AboveOf12 ?x)
          )
          (and
            (AboveOf7 ?x)
            (BelowOf3 ?x)
          )
          (and
            (BelowOf6 ?x)
            (AboveOf11 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf4 ?x)
          )
          (and
            (AboveOf16 ?x)
            (BelowOf1 ?x)
          )
          (and
            (AboveOf18 ?x)
            (BelowOf3 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf1 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf19 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf12 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf9 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf18 ?x)
          )
          (and
            (AboveOf9 ?x)
            (BelowOf1 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf15 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf10 ?x)
          )
          (and
            (AboveOf10 ?x)
            (BelowOf3 ?x)
          )
          (and
            (AboveOf6 ?x)
            (BelowOf6 ?x)
          )
          (and
            (AboveOf15 ?x)
            (BelowOf3 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf9 ?x)
          )
          (and
            (AboveOf18 ?x)
            (BelowOf6 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf17 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf11 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf10 ?x)
          )
          (and
            (AboveOf9 ?x)
            (BelowOf3 ?x)
          )
          (and
            (AboveOf5 ?x)
            (BelowOf5 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf17 ?x)
          )
          (and
            (AboveOf16 ?x)
            (BelowOf3 ?x)
          )
          (and
            (AboveOf7 ?x)
            (BelowOf1 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf2 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf8 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf7 ?x)
          )
          (and
            (AboveOf5 ?x)
            (BelowOf3 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf19 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf18 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf18 ?x)
          )
        )
        (and
          (Row4 ?x)
        )
      )
      (when
        (or
          (and
            (BelowOf3 ?x)
            (AboveOf12 ?x)
          )
          (and
            (BelowOf7 ?x)
            (AboveOf12 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf11 ?x)
          )
          (and
            (AboveOf6 ?x)
            (BelowOf3 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf14 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf8 ?x)
          )
          (and
            (AboveOf7 ?x)
            (BelowOf6 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf19 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf18 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf15 ?x)
          )
          (and
            (BelowOf6 ?x)
            (AboveOf6 ?x)
          )
          (and
            (AboveOf17 ?x)
            (BelowOf1 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf6 ?x)
          )
          (and
            (BelowOf7 ?x)
            (AboveOf9 ?x)
          )
          (and
            (BelowOf6 ?x)
            (AboveOf19 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf4 ?x)
          )
          (and
            (AboveOf16 ?x)
            (BelowOf6 ?x)
          )
          (and
            (BelowOf3 ?x)
            (AboveOf11 ?x)
          )
          (and
            (BelowOf7 ?x)
            (AboveOf11 ?x)
          )
          (and
            (BelowOf7 ?x)
            (AboveOf8 ?x)
          )
          (and
            (BelowOf7 ?x)
            (AboveOf17 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf11 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf16 ?x)
          )
          (and
            (AboveOf19 ?x)
            (BelowOf3 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf8 ?x)
          )
          (and
            (AboveOf9 ?x)
            (BelowOf6 ?x)
          )
          (and
            (BelowOf6 ?x)
            (AboveOf8 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf12 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf7 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf6 ?x)
          )
          (and
            (BelowOf3 ?x)
            (AboveOf8 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf19 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf6 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf2 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf12 ?x)
          )
          (and
            (AboveOf7 ?x)
            (BelowOf4 ?x)
          )
          (and
            (BelowOf7 ?x)
            (AboveOf19 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf9 ?x)
          )
          (and
            (AboveOf17 ?x)
            (BelowOf3 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf11 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf16 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf6 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf7 ?x)
          )
          (and
            (AboveOf15 ?x)
            (BelowOf6 ?x)
          )
          (and
            (BelowOf7 ?x)
            (AboveOf10 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf17 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf17 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf8 ?x)
          )
          (and
            (AboveOf15 ?x)
            (BelowOf1 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf6 ?x)
          )
          (and
            (BelowOf6 ?x)
            (AboveOf18 ?x)
          )
          (and
            (AboveOf10 ?x)
            (BelowOf6 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf3 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf12 ?x)
          )
          (and
            (AboveOf11 ?x)
            (BelowOf3 ?x)
          )
          (and
            (AboveOf15 ?x)
            (BelowOf4 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf10 ?x)
          )
          (and
            (BelowOf3 ?x)
            (AboveOf19 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf6 ?x)
          )
          (and
            (AboveOf17 ?x)
            (BelowOf6 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf10 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf1 ?x)
          )
          (and
            (AboveOf16 ?x)
            (BelowOf4 ?x)
          )
          (and
            (BelowOf6 ?x)
            (AboveOf10 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf16 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf13 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf3 ?x)
          )
          (and
            (BelowOf7 ?x)
            (AboveOf18 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf5 ?x)
          )
          (and
            (BelowOf6 ?x)
            (AboveOf12 ?x)
          )
          (and
            (AboveOf7 ?x)
            (BelowOf3 ?x)
          )
          (and
            (BelowOf7 ?x)
            (AboveOf16 ?x)
          )
          (and
            (BelowOf6 ?x)
            (AboveOf11 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf4 ?x)
          )
          (and
            (AboveOf16 ?x)
            (BelowOf1 ?x)
          )
          (and
            (AboveOf18 ?x)
            (BelowOf3 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf1 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf19 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf12 ?x)
          )
          (and
            (BelowOf7 ?x)
            (AboveOf7 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf9 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf18 ?x)
          )
          (and
            (AboveOf9 ?x)
            (BelowOf1 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf15 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf10 ?x)
          )
          (and
            (AboveOf10 ?x)
            (BelowOf3 ?x)
          )
          (and
            (AboveOf15 ?x)
            (BelowOf3 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf9 ?x)
          )
          (and
            (AboveOf15 ?x)
            (BelowOf7 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf7 ?x)
          )
          (and
            (AboveOf18 ?x)
            (BelowOf6 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf17 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf11 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf10 ?x)
          )
          (and
            (AboveOf9 ?x)
            (BelowOf3 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf17 ?x)
          )
          (and
            (AboveOf16 ?x)
            (BelowOf3 ?x)
          )
          (and
            (AboveOf7 ?x)
            (BelowOf1 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf2 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf8 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf7 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf19 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf18 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf18 ?x)
          )
          (and
            (BelowOf7 ?x)
            (AboveOf6 ?x)
          )
        )
        (and
          (Row5 ?x)
        )
      )
      (when
        (or
          (and
            (BelowOf3 ?x)
            (AboveOf12 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf7 ?x)
          )
          (and
            (BelowOf7 ?x)
            (AboveOf12 ?x)
          )
          (and
            (AboveOf16 ?x)
            (BelowOf8 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf11 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf14 ?x)
          )
          (and
            (BelowOf8 ?x)
            (AboveOf12 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf8 ?x)
          )
          (and
            (AboveOf7 ?x)
            (BelowOf6 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf19 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf18 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf15 ?x)
          )
          (and
            (AboveOf17 ?x)
            (BelowOf1 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf6 ?x)
          )
          (and
            (BelowOf7 ?x)
            (AboveOf9 ?x)
          )
          (and
            (BelowOf6 ?x)
            (AboveOf19 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf4 ?x)
          )
          (and
            (AboveOf15 ?x)
            (BelowOf8 ?x)
          )
          (and
            (AboveOf16 ?x)
            (BelowOf6 ?x)
          )
          (and
            (BelowOf3 ?x)
            (AboveOf11 ?x)
          )
          (and
            (BelowOf7 ?x)
            (AboveOf11 ?x)
          )
          (and
            (BelowOf7 ?x)
            (AboveOf8 ?x)
          )
          (and
            (BelowOf7 ?x)
            (AboveOf17 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf11 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf16 ?x)
          )
          (and
            (AboveOf19 ?x)
            (BelowOf3 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf8 ?x)
          )
          (and
            (AboveOf9 ?x)
            (BelowOf6 ?x)
          )
          (and
            (BelowOf6 ?x)
            (AboveOf8 ?x)
          )
          (and
            (AboveOf19 ?x)
            (BelowOf8 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf12 ?x)
          )
          (and
            (AboveOf7 ?x)
            (BelowOf7 ?x)
          )
          (and
            (BelowOf3 ?x)
            (AboveOf8 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf19 ?x)
          )
          (and
            (BelowOf8 ?x)
            (AboveOf8 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf16 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf2 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf12 ?x)
          )
          (and
            (AboveOf7 ?x)
            (BelowOf4 ?x)
          )
          (and
            (BelowOf7 ?x)
            (AboveOf19 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf9 ?x)
          )
          (and
            (AboveOf17 ?x)
            (BelowOf3 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf11 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf16 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf6 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf7 ?x)
          )
          (and
            (AboveOf15 ?x)
            (BelowOf6 ?x)
          )
          (and
            (AboveOf16 ?x)
            (BelowOf3 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf17 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf17 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf8 ?x)
          )
          (and
            (AboveOf15 ?x)
            (BelowOf1 ?x)
          )
          (and
            (BelowOf6 ?x)
            (AboveOf18 ?x)
          )
          (and
            (AboveOf7 ?x)
            (BelowOf8 ?x)
          )
          (and
            (AboveOf10 ?x)
            (BelowOf6 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf3 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf12 ?x)
          )
          (and
            (AboveOf11 ?x)
            (BelowOf3 ?x)
          )
          (and
            (AboveOf15 ?x)
            (BelowOf4 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf10 ?x)
          )
          (and
            (BelowOf3 ?x)
            (AboveOf19 ?x)
          )
          (and
            (AboveOf17 ?x)
            (BelowOf6 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf10 ?x)
          )
          (and
            (AboveOf9 ?x)
            (BelowOf8 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf1 ?x)
          )
          (and
            (AboveOf16 ?x)
            (BelowOf4 ?x)
          )
          (and
            (BelowOf6 ?x)
            (AboveOf10 ?x)
          )
          (and
            (AboveOf18 ?x)
            (BelowOf8 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf13 ?x)
          )
          (and
            (AboveOf10 ?x)
            (BelowOf8 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf3 ?x)
          )
          (and
            (BelowOf7 ?x)
            (AboveOf18 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf5 ?x)
          )
          (and
            (BelowOf6 ?x)
            (AboveOf12 ?x)
          )
          (and
            (AboveOf7 ?x)
            (BelowOf3 ?x)
          )
          (and
            (BelowOf7 ?x)
            (AboveOf16 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf8 ?x)
          )
          (and
            (BelowOf6 ?x)
            (AboveOf11 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf4 ?x)
          )
          (and
            (AboveOf17 ?x)
            (BelowOf8 ?x)
          )
          (and
            (AboveOf16 ?x)
            (BelowOf1 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf8 ?x)
          )
          (and
            (AboveOf11 ?x)
            (BelowOf8 ?x)
          )
          (and
            (AboveOf18 ?x)
            (BelowOf3 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf1 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf19 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf12 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf9 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf18 ?x)
          )
          (and
            (AboveOf9 ?x)
            (BelowOf1 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf15 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf10 ?x)
          )
          (and
            (AboveOf10 ?x)
            (BelowOf3 ?x)
          )
          (and
            (AboveOf15 ?x)
            (BelowOf3 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf9 ?x)
          )
          (and
            (AboveOf15 ?x)
            (BelowOf7 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf7 ?x)
          )
          (and
            (AboveOf18 ?x)
            (BelowOf6 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf17 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf11 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf10 ?x)
          )
          (and
            (AboveOf9 ?x)
            (BelowOf3 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf17 ?x)
          )
          (and
            (BelowOf7 ?x)
            (AboveOf10 ?x)
          )
          (and
            (AboveOf7 ?x)
            (BelowOf1 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf2 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf8 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf7 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf19 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf18 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf18 ?x)
          )
        )
        (and
          (Row6 ?x)
        )
      )
      (when
        (or
          (and
            (BelowOf9 ?x)
            (AboveOf12 ?x)
          )
          (and
            (BelowOf3 ?x)
            (AboveOf12 ?x)
          )
          (and
            (BelowOf9 ?x)
            (AboveOf8 ?x)
          )
          (and
            (BelowOf9 ?x)
            (AboveOf18 ?x)
          )
          (and
            (BelowOf7 ?x)
            (AboveOf12 ?x)
          )
          (and
            (AboveOf16 ?x)
            (BelowOf8 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf11 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf14 ?x)
          )
          (and
            (BelowOf8 ?x)
            (AboveOf12 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf8 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf19 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf18 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf15 ?x)
          )
          (and
            (AboveOf17 ?x)
            (BelowOf1 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf6 ?x)
          )
          (and
            (BelowOf7 ?x)
            (AboveOf9 ?x)
          )
          (and
            (BelowOf6 ?x)
            (AboveOf19 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf4 ?x)
          )
          (and
            (AboveOf15 ?x)
            (BelowOf8 ?x)
          )
          (and
            (AboveOf15 ?x)
            (BelowOf9 ?x)
          )
          (and
            (BelowOf3 ?x)
            (AboveOf11 ?x)
          )
          (and
            (BelowOf7 ?x)
            (AboveOf11 ?x)
          )
          (and
            (BelowOf7 ?x)
            (AboveOf8 ?x)
          )
          (and
            (BelowOf7 ?x)
            (AboveOf17 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf11 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf16 ?x)
          )
          (and
            (AboveOf19 ?x)
            (BelowOf3 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf8 ?x)
          )
          (and
            (AboveOf9 ?x)
            (BelowOf6 ?x)
          )
          (and
            (BelowOf6 ?x)
            (AboveOf8 ?x)
          )
          (and
            (AboveOf19 ?x)
            (BelowOf8 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf12 ?x)
          )
          (and
            (BelowOf3 ?x)
            (AboveOf8 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf19 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf16 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf2 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf12 ?x)
          )
          (and
            (BelowOf7 ?x)
            (AboveOf19 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf9 ?x)
          )
          (and
            (AboveOf8 ?x)
            (BelowOf8 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf11 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf16 ?x)
          )
          (and
            (AboveOf16 ?x)
            (BelowOf9 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf6 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf7 ?x)
          )
          (and
            (AboveOf15 ?x)
            (BelowOf6 ?x)
          )
          (and
            (AboveOf16 ?x)
            (BelowOf3 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf17 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf17 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf8 ?x)
          )
          (and
            (AboveOf15 ?x)
            (BelowOf1 ?x)
          )
          (and
            (AboveOf9 ?x)
            (BelowOf9 ?x)
          )
          (and
            (BelowOf6 ?x)
            (AboveOf18 ?x)
          )
          (and
            (AboveOf10 ?x)
            (BelowOf6 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf3 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf12 ?x)
          )
          (and
            (AboveOf11 ?x)
            (BelowOf3 ?x)
          )
          (and
            (BelowOf9 ?x)
            (AboveOf19 ?x)
          )
          (and
            (AboveOf15 ?x)
            (BelowOf4 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf10 ?x)
          )
          (and
            (BelowOf3 ?x)
            (AboveOf19 ?x)
          )
          (and
            (AboveOf17 ?x)
            (BelowOf3 ?x)
          )
          (and
            (AboveOf17 ?x)
            (BelowOf6 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf10 ?x)
          )
          (and
            (AboveOf9 ?x)
            (BelowOf8 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf1 ?x)
          )
          (and
            (AboveOf16 ?x)
            (BelowOf4 ?x)
          )
          (and
            (BelowOf6 ?x)
            (AboveOf10 ?x)
          )
          (and
            (AboveOf16 ?x)
            (BelowOf6 ?x)
          )
          (and
            (AboveOf18 ?x)
            (BelowOf8 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf13 ?x)
          )
          (and
            (AboveOf10 ?x)
            (BelowOf8 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf9 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf3 ?x)
          )
          (and
            (BelowOf7 ?x)
            (AboveOf18 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf5 ?x)
          )
          (and
            (BelowOf6 ?x)
            (AboveOf12 ?x)
          )
          (and
            (BelowOf9 ?x)
            (AboveOf10 ?x)
          )
          (and
            (BelowOf7 ?x)
            (AboveOf16 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf8 ?x)
          )
          (and
            (BelowOf6 ?x)
            (AboveOf11 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf4 ?x)
          )
          (and
            (AboveOf17 ?x)
            (BelowOf8 ?x)
          )
          (and
            (AboveOf16 ?x)
            (BelowOf1 ?x)
          )
          (and
            (AboveOf12 ?x)
            (BelowOf8 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf8 ?x)
          )
          (and
            (AboveOf11 ?x)
            (BelowOf8 ?x)
          )
          (and
            (AboveOf18 ?x)
            (BelowOf3 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf1 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf19 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf12 ?x)
          )
          (and
            (BelowOf9 ?x)
            (AboveOf11 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf9 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf18 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf9 ?x)
          )
          (and
            (AboveOf9 ?x)
            (BelowOf1 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf15 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf10 ?x)
          )
          (and
            (AboveOf10 ?x)
            (BelowOf3 ?x)
          )
          (and
            (AboveOf15 ?x)
            (BelowOf3 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf9 ?x)
          )
          (and
            (AboveOf15 ?x)
            (BelowOf7 ?x)
          )
          (and
            (BelowOf9 ?x)
            (AboveOf17 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf7 ?x)
          )
          (and
            (AboveOf18 ?x)
            (BelowOf6 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf17 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf11 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf10 ?x)
          )
          (and
            (AboveOf9 ?x)
            (BelowOf3 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf17 ?x)
          )
          (and
            (BelowOf7 ?x)
            (AboveOf10 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf2 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf8 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf19 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf18 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf18 ?x)
          )
        )
        (and
          (Row7 ?x)
        )
      )
      (when
        (or
          (and
            (BelowOf9 ?x)
            (AboveOf12 ?x)
          )
          (and
            (BelowOf3 ?x)
            (AboveOf12 ?x)
          )
          (and
            (BelowOf9 ?x)
            (AboveOf18 ?x)
          )
          (and
            (BelowOf10 ?x)
            (AboveOf11 ?x)
          )
          (and
            (BelowOf7 ?x)
            (AboveOf12 ?x)
          )
          (and
            (AboveOf16 ?x)
            (BelowOf8 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf11 ?x)
          )
          (and
            (AboveOf15 ?x)
            (BelowOf1 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf14 ?x)
          )
          (and
            (AboveOf17 ?x)
            (BelowOf1 ?x)
          )
          (and
            (BelowOf8 ?x)
            (AboveOf12 ?x)
          )
          (and
            (AboveOf9 ?x)
            (BelowOf3 ?x)
          )
          (and
            (AboveOf17 ?x)
            (BelowOf10 ?x)
          )
          (and
            (AboveOf15 ?x)
            (BelowOf10 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf19 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf18 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf15 ?x)
          )
          (and
            (AboveOf9 ?x)
            (BelowOf10 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf6 ?x)
          )
          (and
            (BelowOf7 ?x)
            (AboveOf9 ?x)
          )
          (and
            (BelowOf6 ?x)
            (AboveOf19 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf4 ?x)
          )
          (and
            (AboveOf15 ?x)
            (BelowOf8 ?x)
          )
          (and
            (AboveOf15 ?x)
            (BelowOf9 ?x)
          )
          (and
            (BelowOf3 ?x)
            (AboveOf11 ?x)
          )
          (and
            (BelowOf7 ?x)
            (AboveOf11 ?x)
          )
          (and
            (BelowOf7 ?x)
            (AboveOf17 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf11 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf16 ?x)
          )
          (and
            (AboveOf19 ?x)
            (BelowOf3 ?x)
          )
          (and
            (AboveOf9 ?x)
            (BelowOf6 ?x)
          )
          (and
            (AboveOf19 ?x)
            (BelowOf8 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf12 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf19 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf16 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf2 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf12 ?x)
          )
          (and
            (BelowOf9 ?x)
            (AboveOf19 ?x)
          )
          (and
            (BelowOf7 ?x)
            (AboveOf19 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf9 ?x)
          )
          (and
            (AboveOf16 ?x)
            (BelowOf10 ?x)
          )
          (and
            (AboveOf17 ?x)
            (BelowOf3 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf11 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf16 ?x)
          )
          (and
            (AboveOf16 ?x)
            (BelowOf9 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf6 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf7 ?x)
          )
          (and
            (AboveOf15 ?x)
            (BelowOf6 ?x)
          )
          (and
            (AboveOf16 ?x)
            (BelowOf3 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf17 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf17 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf10 ?x)
          )
          (and
            (BelowOf6 ?x)
            (AboveOf18 ?x)
          )
          (and
            (AboveOf10 ?x)
            (BelowOf6 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf3 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf12 ?x)
          )
          (and
            (AboveOf11 ?x)
            (BelowOf3 ?x)
          )
          (and
            (BelowOf9 ?x)
            (AboveOf9 ?x)
          )
          (and
            (AboveOf15 ?x)
            (BelowOf4 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf10 ?x)
          )
          (and
            (BelowOf3 ?x)
            (AboveOf19 ?x)
          )
          (and
            (AboveOf17 ?x)
            (BelowOf6 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf10 ?x)
          )
          (and
            (AboveOf9 ?x)
            (BelowOf8 ?x)
          )
          (and
            (BelowOf10 ?x)
            (AboveOf19 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf1 ?x)
          )
          (and
            (AboveOf16 ?x)
            (BelowOf4 ?x)
          )
          (and
            (BelowOf6 ?x)
            (AboveOf10 ?x)
          )
          (and
            (AboveOf16 ?x)
            (BelowOf6 ?x)
          )
          (and
            (AboveOf18 ?x)
            (BelowOf8 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf13 ?x)
          )
          (and
            (AboveOf10 ?x)
            (BelowOf8 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf9 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf10 ?x)
          )
          (and
            (BelowOf7 ?x)
            (AboveOf18 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf5 ?x)
          )
          (and
            (BelowOf6 ?x)
            (AboveOf12 ?x)
          )
          (and
            (BelowOf9 ?x)
            (AboveOf10 ?x)
          )
          (and
            (BelowOf7 ?x)
            (AboveOf16 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf8 ?x)
          )
          (and
            (BelowOf6 ?x)
            (AboveOf11 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf4 ?x)
          )
          (and
            (AboveOf17 ?x)
            (BelowOf8 ?x)
          )
          (and
            (AboveOf16 ?x)
            (BelowOf1 ?x)
          )
          (and
            (AboveOf12 ?x)
            (BelowOf8 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf8 ?x)
          )
          (and
            (AboveOf11 ?x)
            (BelowOf8 ?x)
          )
          (and
            (AboveOf18 ?x)
            (BelowOf3 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf1 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf19 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf12 ?x)
          )
          (and
            (BelowOf9 ?x)
            (AboveOf11 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf9 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf18 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf9 ?x)
          )
          (and
            (AboveOf9 ?x)
            (BelowOf1 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf15 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf10 ?x)
          )
          (and
            (AboveOf10 ?x)
            (BelowOf3 ?x)
          )
          (and
            (BelowOf10 ?x)
            (AboveOf12 ?x)
          )
          (and
            (AboveOf18 ?x)
            (BelowOf10 ?x)
          )
          (and
            (AboveOf15 ?x)
            (BelowOf3 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf9 ?x)
          )
          (and
            (AboveOf10 ?x)
            (BelowOf10 ?x)
          )
          (and
            (AboveOf15 ?x)
            (BelowOf7 ?x)
          )
          (and
            (BelowOf9 ?x)
            (AboveOf17 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf7 ?x)
          )
          (and
            (AboveOf18 ?x)
            (BelowOf6 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf17 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf11 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf10 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf17 ?x)
          )
          (and
            (BelowOf7 ?x)
            (AboveOf10 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf2 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf19 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf18 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf18 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf3 ?x)
          )
        )
        (and
          (Row8 ?x)
        )
      )
      (when
        (or
          (and
            (BelowOf9 ?x)
            (AboveOf12 ?x)
          )
          (and
            (BelowOf11 ?x)
            (AboveOf12 ?x)
          )
          (and
            (BelowOf3 ?x)
            (AboveOf12 ?x)
          )
          (and
            (BelowOf9 ?x)
            (AboveOf18 ?x)
          )
          (and
            (BelowOf10 ?x)
            (AboveOf11 ?x)
          )
          (and
            (BelowOf7 ?x)
            (AboveOf12 ?x)
          )
          (and
            (AboveOf16 ?x)
            (BelowOf8 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf11 ?x)
          )
          (and
            (AboveOf15 ?x)
            (BelowOf1 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf14 ?x)
          )
          (and
            (BelowOf8 ?x)
            (AboveOf12 ?x)
          )
          (and
            (AboveOf17 ?x)
            (BelowOf10 ?x)
          )
          (and
            (AboveOf15 ?x)
            (BelowOf10 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf19 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf18 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf15 ?x)
          )
          (and
            (AboveOf17 ?x)
            (BelowOf1 ?x)
          )
          (and
            (BelowOf11 ?x)
            (AboveOf11 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf6 ?x)
          )
          (and
            (AboveOf10 ?x)
            (BelowOf11 ?x)
          )
          (and
            (BelowOf6 ?x)
            (AboveOf19 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf4 ?x)
          )
          (and
            (AboveOf15 ?x)
            (BelowOf8 ?x)
          )
          (and
            (AboveOf15 ?x)
            (BelowOf9 ?x)
          )
          (and
            (BelowOf3 ?x)
            (AboveOf11 ?x)
          )
          (and
            (BelowOf7 ?x)
            (AboveOf11 ?x)
          )
          (and
            (BelowOf7 ?x)
            (AboveOf17 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf11 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf12 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf16 ?x)
          )
          (and
            (AboveOf19 ?x)
            (BelowOf3 ?x)
          )
          (and
            (AboveOf19 ?x)
            (BelowOf8 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf12 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf19 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf16 ?x)
          )
          (and
            (BelowOf11 ?x)
            (AboveOf19 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf2 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf11 ?x)
          )
          (and
            (BelowOf7 ?x)
            (AboveOf19 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf11 ?x)
          )
          (and
            (AboveOf16 ?x)
            (BelowOf10 ?x)
          )
          (and
            (AboveOf17 ?x)
            (BelowOf3 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf11 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf16 ?x)
          )
          (and
            (AboveOf16 ?x)
            (BelowOf9 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf6 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf7 ?x)
          )
          (and
            (AboveOf15 ?x)
            (BelowOf6 ?x)
          )
          (and
            (AboveOf16 ?x)
            (BelowOf3 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf17 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf17 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf10 ?x)
          )
          (and
            (BelowOf6 ?x)
            (AboveOf18 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf3 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf12 ?x)
          )
          (and
            (AboveOf11 ?x)
            (BelowOf3 ?x)
          )
          (and
            (BelowOf9 ?x)
            (AboveOf19 ?x)
          )
          (and
            (AboveOf15 ?x)
            (BelowOf4 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf10 ?x)
          )
          (and
            (BelowOf3 ?x)
            (AboveOf19 ?x)
          )
          (and
            (AboveOf17 ?x)
            (BelowOf6 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf10 ?x)
          )
          (and
            (AboveOf17 ?x)
            (BelowOf11 ?x)
          )
          (and
            (BelowOf10 ?x)
            (AboveOf19 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf1 ?x)
          )
          (and
            (AboveOf16 ?x)
            (BelowOf4 ?x)
          )
          (and
            (AboveOf15 ?x)
            (BelowOf11 ?x)
          )
          (and
            (BelowOf6 ?x)
            (AboveOf10 ?x)
          )
          (and
            (AboveOf16 ?x)
            (BelowOf6 ?x)
          )
          (and
            (AboveOf18 ?x)
            (BelowOf8 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf13 ?x)
          )
          (and
            (AboveOf10 ?x)
            (BelowOf8 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf9 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf10 ?x)
          )
          (and
            (BelowOf7 ?x)
            (AboveOf18 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf5 ?x)
          )
          (and
            (BelowOf6 ?x)
            (AboveOf12 ?x)
          )
          (and
            (BelowOf9 ?x)
            (AboveOf10 ?x)
          )
          (and
            (BelowOf7 ?x)
            (AboveOf16 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf8 ?x)
          )
          (and
            (BelowOf6 ?x)
            (AboveOf11 ?x)
          )
          (and
            (AboveOf18 ?x)
            (BelowOf11 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf4 ?x)
          )
          (and
            (AboveOf17 ?x)
            (BelowOf8 ?x)
          )
          (and
            (AboveOf16 ?x)
            (BelowOf11 ?x)
          )
          (and
            (AboveOf16 ?x)
            (BelowOf1 ?x)
          )
          (and
            (AboveOf12 ?x)
            (BelowOf8 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf8 ?x)
          )
          (and
            (AboveOf11 ?x)
            (BelowOf8 ?x)
          )
          (and
            (AboveOf18 ?x)
            (BelowOf3 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf1 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf19 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf12 ?x)
          )
          (and
            (BelowOf9 ?x)
            (AboveOf11 ?x)
          )
          (and
            (AboveOf18 ?x)
            (BelowOf10 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf18 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf9 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf15 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf10 ?x)
          )
          (and
            (AboveOf10 ?x)
            (BelowOf3 ?x)
          )
          (and
            (BelowOf10 ?x)
            (AboveOf12 ?x)
          )
          (and
            (AboveOf15 ?x)
            (BelowOf3 ?x)
          )
          (and
            (AboveOf10 ?x)
            (BelowOf10 ?x)
          )
          (and
            (AboveOf15 ?x)
            (BelowOf7 ?x)
          )
          (and
            (BelowOf9 ?x)
            (AboveOf17 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf7 ?x)
          )
          (and
            (AboveOf18 ?x)
            (BelowOf6 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf17 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf11 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf10 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf17 ?x)
          )
          (and
            (BelowOf7 ?x)
            (AboveOf10 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf2 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf19 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf18 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf18 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf3 ?x)
          )
        )
        (and
          (Row9 ?x)
        )
      )
      (when
        (or
          (and
            (AboveOf19 ?x)
            (BelowOf12 ?x)
          )
          (and
            (BelowOf9 ?x)
            (AboveOf12 ?x)
          )
          (and
            (BelowOf11 ?x)
            (AboveOf12 ?x)
          )
          (and
            (BelowOf3 ?x)
            (AboveOf12 ?x)
          )
          (and
            (BelowOf9 ?x)
            (AboveOf18 ?x)
          )
          (and
            (BelowOf10 ?x)
            (AboveOf11 ?x)
          )
          (and
            (BelowOf7 ?x)
            (AboveOf12 ?x)
          )
          (and
            (AboveOf16 ?x)
            (BelowOf8 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf11 ?x)
          )
          (and
            (AboveOf16 ?x)
            (BelowOf12 ?x)
          )
          (and
            (AboveOf15 ?x)
            (BelowOf1 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf14 ?x)
          )
          (and
            (BelowOf8 ?x)
            (AboveOf12 ?x)
          )
          (and
            (AboveOf17 ?x)
            (BelowOf10 ?x)
          )
          (and
            (AboveOf15 ?x)
            (BelowOf10 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf19 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf18 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf15 ?x)
          )
          (and
            (BelowOf12 ?x)
            (AboveOf12 ?x)
          )
          (and
            (AboveOf17 ?x)
            (BelowOf1 ?x)
          )
          (and
            (BelowOf11 ?x)
            (AboveOf11 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf6 ?x)
          )
          (and
            (BelowOf6 ?x)
            (AboveOf19 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf4 ?x)
          )
          (and
            (AboveOf15 ?x)
            (BelowOf8 ?x)
          )
          (and
            (AboveOf15 ?x)
            (BelowOf9 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf12 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf3 ?x)
          )
          (and
            (BelowOf7 ?x)
            (AboveOf11 ?x)
          )
          (and
            (BelowOf7 ?x)
            (AboveOf17 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf11 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf12 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf16 ?x)
          )
          (and
            (AboveOf19 ?x)
            (BelowOf3 ?x)
          )
          (and
            (AboveOf19 ?x)
            (BelowOf8 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf12 ?x)
          )
          (and
            (AboveOf11 ?x)
            (BelowOf12 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf19 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf16 ?x)
          )
          (and
            (BelowOf11 ?x)
            (AboveOf19 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf2 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf11 ?x)
          )
          (and
            (BelowOf7 ?x)
            (AboveOf19 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf11 ?x)
          )
          (and
            (AboveOf16 ?x)
            (BelowOf10 ?x)
          )
          (and
            (AboveOf17 ?x)
            (BelowOf3 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf11 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf16 ?x)
          )
          (and
            (AboveOf16 ?x)
            (BelowOf9 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf6 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf7 ?x)
          )
          (and
            (AboveOf15 ?x)
            (BelowOf6 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf17 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf17 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf10 ?x)
          )
          (and
            (BelowOf6 ?x)
            (AboveOf18 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf3 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf12 ?x)
          )
          (and
            (AboveOf17 ?x)
            (BelowOf12 ?x)
          )
          (and
            (AboveOf11 ?x)
            (BelowOf3 ?x)
          )
          (and
            (BelowOf9 ?x)
            (AboveOf19 ?x)
          )
          (and
            (AboveOf15 ?x)
            (BelowOf4 ?x)
          )
          (and
            (BelowOf3 ?x)
            (AboveOf19 ?x)
          )
          (and
            (AboveOf17 ?x)
            (BelowOf6 ?x)
          )
          (and
            (AboveOf17 ?x)
            (BelowOf11 ?x)
          )
          (and
            (BelowOf10 ?x)
            (AboveOf19 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf1 ?x)
          )
          (and
            (AboveOf16 ?x)
            (BelowOf4 ?x)
          )
          (and
            (AboveOf15 ?x)
            (BelowOf11 ?x)
          )
          (and
            (AboveOf16 ?x)
            (BelowOf6 ?x)
          )
          (and
            (AboveOf18 ?x)
            (BelowOf8 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf13 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf9 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf10 ?x)
          )
          (and
            (BelowOf7 ?x)
            (AboveOf18 ?x)
          )
          (and
            (AboveOf15 ?x)
            (BelowOf12 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf5 ?x)
          )
          (and
            (BelowOf6 ?x)
            (AboveOf12 ?x)
          )
          (and
            (BelowOf7 ?x)
            (AboveOf16 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf8 ?x)
          )
          (and
            (BelowOf6 ?x)
            (AboveOf11 ?x)
          )
          (and
            (AboveOf18 ?x)
            (BelowOf11 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf4 ?x)
          )
          (and
            (AboveOf17 ?x)
            (BelowOf8 ?x)
          )
          (and
            (AboveOf16 ?x)
            (BelowOf11 ?x)
          )
          (and
            (AboveOf16 ?x)
            (BelowOf1 ?x)
          )
          (and
            (AboveOf12 ?x)
            (BelowOf8 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf8 ?x)
          )
          (and
            (AboveOf11 ?x)
            (BelowOf8 ?x)
          )
          (and
            (AboveOf18 ?x)
            (BelowOf3 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf1 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf19 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf12 ?x)
          )
          (and
            (BelowOf9 ?x)
            (AboveOf11 ?x)
          )
          (and
            (AboveOf18 ?x)
            (BelowOf10 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf18 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf9 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf15 ?x)
          )
          (and
            (BelowOf10 ?x)
            (AboveOf12 ?x)
          )
          (and
            (AboveOf15 ?x)
            (BelowOf3 ?x)
          )
          (and
            (AboveOf15 ?x)
            (BelowOf7 ?x)
          )
          (and
            (BelowOf9 ?x)
            (AboveOf17 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf12 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf7 ?x)
          )
          (and
            (AboveOf18 ?x)
            (BelowOf6 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf17 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf11 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf17 ?x)
          )
          (and
            (AboveOf16 ?x)
            (BelowOf3 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf2 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf19 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf18 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf18 ?x)
          )
          (and
            (AboveOf18 ?x)
            (BelowOf12 ?x)
          )
        )
        (and
          (Row10 ?x)
        )
      )
      (when
        (or
          (and
            (AboveOf19 ?x)
            (BelowOf12 ?x)
          )
          (and
            (AboveOf12 ?x)
            (BelowOf12 ?x)
          )
          (and
            (BelowOf9 ?x)
            (AboveOf12 ?x)
          )
          (and
            (BelowOf11 ?x)
            (AboveOf12 ?x)
          )
          (and
            (BelowOf3 ?x)
            (AboveOf12 ?x)
          )
          (and
            (BelowOf9 ?x)
            (AboveOf18 ?x)
          )
          (and
            (BelowOf7 ?x)
            (AboveOf12 ?x)
          )
          (and
            (AboveOf16 ?x)
            (BelowOf8 ?x)
          )
          (and
            (AboveOf16 ?x)
            (BelowOf12 ?x)
          )
          (and
            (BelowOf13 ?x)
            (AboveOf12 ?x)
          )
          (and
            (AboveOf15 ?x)
            (BelowOf1 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf14 ?x)
          )
          (and
            (BelowOf13 ?x)
            (AboveOf15 ?x)
          )
          (and
            (AboveOf17 ?x)
            (BelowOf10 ?x)
          )
          (and
            (AboveOf15 ?x)
            (BelowOf10 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf19 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf18 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf15 ?x)
          )
          (and
            (AboveOf17 ?x)
            (BelowOf1 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf6 ?x)
          )
          (and
            (BelowOf6 ?x)
            (AboveOf19 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf4 ?x)
          )
          (and
            (AboveOf15 ?x)
            (BelowOf8 ?x)
          )
          (and
            (AboveOf15 ?x)
            (BelowOf9 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf12 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf3 ?x)
          )
          (and
            (BelowOf7 ?x)
            (AboveOf17 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf12 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf16 ?x)
          )
          (and
            (AboveOf19 ?x)
            (BelowOf3 ?x)
          )
          (and
            (AboveOf19 ?x)
            (BelowOf8 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf19 ?x)
          )
          (and
            (BelowOf11 ?x)
            (AboveOf19 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf2 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf11 ?x)
          )
          (and
            (BelowOf7 ?x)
            (AboveOf19 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf11 ?x)
          )
          (and
            (AboveOf16 ?x)
            (BelowOf10 ?x)
          )
          (and
            (AboveOf17 ?x)
            (BelowOf3 ?x)
          )
          (and
            (BelowOf13 ?x)
            (AboveOf14 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf16 ?x)
          )
          (and
            (AboveOf16 ?x)
            (BelowOf9 ?x)
          )
          (and
            (BelowOf13 ?x)
            (AboveOf13 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf6 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf7 ?x)
          )
          (and
            (AboveOf15 ?x)
            (BelowOf6 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf17 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf17 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf10 ?x)
          )
          (and
            (BelowOf6 ?x)
            (AboveOf18 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf3 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf12 ?x)
          )
          (and
            (AboveOf17 ?x)
            (BelowOf12 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf12 ?x)
          )
          (and
            (BelowOf9 ?x)
            (AboveOf19 ?x)
          )
          (and
            (AboveOf15 ?x)
            (BelowOf4 ?x)
          )
          (and
            (BelowOf3 ?x)
            (AboveOf19 ?x)
          )
          (and
            (AboveOf17 ?x)
            (BelowOf6 ?x)
          )
          (and
            (AboveOf17 ?x)
            (BelowOf11 ?x)
          )
          (and
            (BelowOf10 ?x)
            (AboveOf19 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf1 ?x)
          )
          (and
            (BelowOf13 ?x)
            (AboveOf17 ?x)
          )
          (and
            (AboveOf16 ?x)
            (BelowOf4 ?x)
          )
          (and
            (AboveOf15 ?x)
            (BelowOf11 ?x)
          )
          (and
            (AboveOf16 ?x)
            (BelowOf6 ?x)
          )
          (and
            (BelowOf13 ?x)
            (AboveOf19 ?x)
          )
          (and
            (AboveOf18 ?x)
            (BelowOf8 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf13 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf9 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf10 ?x)
          )
          (and
            (BelowOf7 ?x)
            (AboveOf18 ?x)
          )
          (and
            (AboveOf15 ?x)
            (BelowOf12 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf5 ?x)
          )
          (and
            (BelowOf6 ?x)
            (AboveOf12 ?x)
          )
          (and
            (BelowOf7 ?x)
            (AboveOf16 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf8 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf4 ?x)
          )
          (and
            (AboveOf17 ?x)
            (BelowOf8 ?x)
          )
          (and
            (AboveOf16 ?x)
            (BelowOf11 ?x)
          )
          (and
            (AboveOf16 ?x)
            (BelowOf1 ?x)
          )
          (and
            (AboveOf12 ?x)
            (BelowOf8 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf8 ?x)
          )
          (and
            (AboveOf18 ?x)
            (BelowOf3 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf1 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf19 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf12 ?x)
          )
          (and
            (AboveOf18 ?x)
            (BelowOf11 ?x)
          )
          (and
            (AboveOf18 ?x)
            (BelowOf10 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf18 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf9 ?x)
          )
          (and
            (BelowOf13 ?x)
            (AboveOf16 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf15 ?x)
          )
          (and
            (BelowOf10 ?x)
            (AboveOf12 ?x)
          )
          (and
            (AboveOf15 ?x)
            (BelowOf3 ?x)
          )
          (and
            (AboveOf15 ?x)
            (BelowOf7 ?x)
          )
          (and
            (BelowOf13 ?x)
            (AboveOf18 ?x)
          )
          (and
            (BelowOf9 ?x)
            (AboveOf17 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf12 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf7 ?x)
          )
          (and
            (AboveOf18 ?x)
            (BelowOf6 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf17 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf16 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf17 ?x)
          )
          (and
            (AboveOf16 ?x)
            (BelowOf3 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf2 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf19 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf18 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf18 ?x)
          )
          (and
            (AboveOf18 ?x)
            (BelowOf12 ?x)
          )
        )
        (and
          (Row11 ?x)
        )
      )
      (when
        (or
          (and
            (AboveOf19 ?x)
            (BelowOf12 ?x)
          )
          (and
            (BelowOf9 ?x)
            (AboveOf18 ?x)
          )
          (and
            (AboveOf16 ?x)
            (BelowOf10 ?x)
          )
          (and
            (AboveOf16 ?x)
            (BelowOf8 ?x)
          )
          (and
            (AboveOf16 ?x)
            (BelowOf12 ?x)
          )
          (and
            (AboveOf15 ?x)
            (BelowOf1 ?x)
          )
          (and
            (BelowOf14 ?x)
            (AboveOf17 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf14 ?x)
          )
          (and
            (BelowOf13 ?x)
            (AboveOf15 ?x)
          )
          (and
            (AboveOf17 ?x)
            (BelowOf10 ?x)
          )
          (and
            (AboveOf15 ?x)
            (BelowOf10 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf19 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf18 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf15 ?x)
          )
          (and
            (AboveOf17 ?x)
            (BelowOf1 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf6 ?x)
          )
          (and
            (BelowOf6 ?x)
            (AboveOf19 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf4 ?x)
          )
          (and
            (AboveOf15 ?x)
            (BelowOf8 ?x)
          )
          (and
            (AboveOf15 ?x)
            (BelowOf9 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf12 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf3 ?x)
          )
          (and
            (BelowOf7 ?x)
            (AboveOf17 ?x)
          )
          (and
            (BelowOf14 ?x)
            (AboveOf18 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf16 ?x)
          )
          (and
            (AboveOf19 ?x)
            (BelowOf3 ?x)
          )
          (and
            (AboveOf19 ?x)
            (BelowOf8 ?x)
          )
          (and
            (BelowOf14 ?x)
            (AboveOf19 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf14 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf19 ?x)
          )
          (and
            (BelowOf11 ?x)
            (AboveOf19 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf2 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf11 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf13 ?x)
          )
          (and
            (BelowOf7 ?x)
            (AboveOf19 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf11 ?x)
          )
          (and
            (AboveOf17 ?x)
            (BelowOf3 ?x)
          )
          (and
            (BelowOf13 ?x)
            (AboveOf14 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf14 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf16 ?x)
          )
          (and
            (AboveOf16 ?x)
            (BelowOf9 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf6 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf7 ?x)
          )
          (and
            (AboveOf15 ?x)
            (BelowOf6 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf17 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf17 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf10 ?x)
          )
          (and
            (BelowOf6 ?x)
            (AboveOf18 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf3 ?x)
          )
          (and
            (AboveOf17 ?x)
            (BelowOf12 ?x)
          )
          (and
            (BelowOf14 ?x)
            (AboveOf16 ?x)
          )
          (and
            (BelowOf9 ?x)
            (AboveOf19 ?x)
          )
          (and
            (AboveOf15 ?x)
            (BelowOf4 ?x)
          )
          (and
            (BelowOf3 ?x)
            (AboveOf19 ?x)
          )
          (and
            (AboveOf17 ?x)
            (BelowOf6 ?x)
          )
          (and
            (AboveOf17 ?x)
            (BelowOf11 ?x)
          )
          (and
            (BelowOf10 ?x)
            (AboveOf19 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf1 ?x)
          )
          (and
            (BelowOf13 ?x)
            (AboveOf17 ?x)
          )
          (and
            (AboveOf16 ?x)
            (BelowOf4 ?x)
          )
          (and
            (AboveOf15 ?x)
            (BelowOf11 ?x)
          )
          (and
            (AboveOf16 ?x)
            (BelowOf6 ?x)
          )
          (and
            (BelowOf13 ?x)
            (AboveOf19 ?x)
          )
          (and
            (AboveOf18 ?x)
            (BelowOf8 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf9 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf10 ?x)
          )
          (and
            (BelowOf7 ?x)
            (AboveOf18 ?x)
          )
          (and
            (AboveOf15 ?x)
            (BelowOf12 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf5 ?x)
          )
          (and
            (BelowOf7 ?x)
            (AboveOf16 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf8 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf4 ?x)
          )
          (and
            (AboveOf17 ?x)
            (BelowOf8 ?x)
          )
          (and
            (AboveOf16 ?x)
            (BelowOf11 ?x)
          )
          (and
            (AboveOf16 ?x)
            (BelowOf1 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf8 ?x)
          )
          (and
            (AboveOf18 ?x)
            (BelowOf3 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf1 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf19 ?x)
          )
          (and
            (AboveOf18 ?x)
            (BelowOf11 ?x)
          )
          (and
            (AboveOf18 ?x)
            (BelowOf10 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf18 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf9 ?x)
          )
          (and
            (BelowOf13 ?x)
            (AboveOf16 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf15 ?x)
          )
          (and
            (AboveOf15 ?x)
            (BelowOf3 ?x)
          )
          (and
            (AboveOf15 ?x)
            (BelowOf7 ?x)
          )
          (and
            (BelowOf13 ?x)
            (AboveOf18 ?x)
          )
          (and
            (BelowOf9 ?x)
            (AboveOf17 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf12 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf7 ?x)
          )
          (and
            (AboveOf18 ?x)
            (BelowOf6 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf17 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf16 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf17 ?x)
          )
          (and
            (AboveOf16 ?x)
            (BelowOf3 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf2 ?x)
          )
          (and
            (BelowOf14 ?x)
            (AboveOf15 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf19 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf18 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf18 ?x)
          )
          (and
            (AboveOf18 ?x)
            (BelowOf12 ?x)
          )
        )
        (and
          (Row12 ?x)
        )
      )
      (when
        (or
          (and
            (AboveOf19 ?x)
            (BelowOf12 ?x)
          )
          (and
            (BelowOf9 ?x)
            (AboveOf18 ?x)
          )
          (and
            (AboveOf16 ?x)
            (BelowOf10 ?x)
          )
          (and
            (AboveOf16 ?x)
            (BelowOf8 ?x)
          )
          (and
            (AboveOf16 ?x)
            (BelowOf12 ?x)
          )
          (and
            (BelowOf14 ?x)
            (AboveOf17 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf14 ?x)
          )
          (and
            (BelowOf13 ?x)
            (AboveOf15 ?x)
          )
          (and
            (AboveOf17 ?x)
            (BelowOf10 ?x)
          )
          (and
            (BelowOf15 ?x)
            (AboveOf16 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf19 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf18 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf15 ?x)
          )
          (and
            (AboveOf17 ?x)
            (BelowOf1 ?x)
          )
          (and
            (AboveOf15 ?x)
            (BelowOf10 ?x)
          )
          (and
            (BelowOf6 ?x)
            (AboveOf19 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf4 ?x)
          )
          (and
            (AboveOf15 ?x)
            (BelowOf8 ?x)
          )
          (and
            (AboveOf15 ?x)
            (BelowOf9 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf12 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf3 ?x)
          )
          (and
            (BelowOf7 ?x)
            (AboveOf17 ?x)
          )
          (and
            (BelowOf15 ?x)
            (AboveOf19 ?x)
          )
          (and
            (BelowOf14 ?x)
            (AboveOf18 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf16 ?x)
          )
          (and
            (AboveOf19 ?x)
            (BelowOf3 ?x)
          )
          (and
            (AboveOf19 ?x)
            (BelowOf8 ?x)
          )
          (and
            (BelowOf14 ?x)
            (AboveOf19 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf19 ?x)
          )
          (and
            (BelowOf11 ?x)
            (AboveOf19 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf2 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf11 ?x)
          )
          (and
            (BelowOf7 ?x)
            (AboveOf19 ?x)
          )
          (and
            (AboveOf17 ?x)
            (BelowOf3 ?x)
          )
          (and
            (BelowOf13 ?x)
            (AboveOf14 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf14 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf16 ?x)
          )
          (and
            (AboveOf16 ?x)
            (BelowOf9 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf6 ?x)
          )
          (and
            (AboveOf15 ?x)
            (BelowOf6 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf17 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf17 ?x)
          )
          (and
            (AboveOf15 ?x)
            (BelowOf1 ?x)
          )
          (and
            (BelowOf15 ?x)
            (AboveOf17 ?x)
          )
          (and
            (BelowOf6 ?x)
            (AboveOf18 ?x)
          )
          (and
            (AboveOf17 ?x)
            (BelowOf12 ?x)
          )
          (and
            (BelowOf14 ?x)
            (AboveOf16 ?x)
          )
          (and
            (BelowOf9 ?x)
            (AboveOf19 ?x)
          )
          (and
            (AboveOf15 ?x)
            (BelowOf4 ?x)
          )
          (and
            (BelowOf3 ?x)
            (AboveOf19 ?x)
          )
          (and
            (AboveOf17 ?x)
            (BelowOf6 ?x)
          )
          (and
            (AboveOf17 ?x)
            (BelowOf11 ?x)
          )
          (and
            (BelowOf10 ?x)
            (AboveOf19 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf1 ?x)
          )
          (and
            (BelowOf13 ?x)
            (AboveOf17 ?x)
          )
          (and
            (AboveOf16 ?x)
            (BelowOf4 ?x)
          )
          (and
            (AboveOf15 ?x)
            (BelowOf11 ?x)
          )
          (and
            (AboveOf16 ?x)
            (BelowOf6 ?x)
          )
          (and
            (BelowOf13 ?x)
            (AboveOf19 ?x)
          )
          (and
            (AboveOf18 ?x)
            (BelowOf8 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf9 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf10 ?x)
          )
          (and
            (BelowOf7 ?x)
            (AboveOf18 ?x)
          )
          (and
            (AboveOf15 ?x)
            (BelowOf15 ?x)
          )
          (and
            (AboveOf15 ?x)
            (BelowOf12 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf15 ?x)
          )
          (and
            (BelowOf7 ?x)
            (AboveOf16 ?x)
          )
          (and
            (AboveOf17 ?x)
            (BelowOf8 ?x)
          )
          (and
            (AboveOf16 ?x)
            (BelowOf11 ?x)
          )
          (and
            (AboveOf16 ?x)
            (BelowOf1 ?x)
          )
          (and
            (BelowOf15 ?x)
            (AboveOf18 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf8 ?x)
          )
          (and
            (AboveOf18 ?x)
            (BelowOf3 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf19 ?x)
          )
          (and
            (AboveOf18 ?x)
            (BelowOf11 ?x)
          )
          (and
            (AboveOf18 ?x)
            (BelowOf10 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf18 ?x)
          )
          (and
            (BelowOf13 ?x)
            (AboveOf16 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf15 ?x)
          )
          (and
            (AboveOf15 ?x)
            (BelowOf3 ?x)
          )
          (and
            (AboveOf15 ?x)
            (BelowOf7 ?x)
          )
          (and
            (BelowOf13 ?x)
            (AboveOf18 ?x)
          )
          (and
            (BelowOf9 ?x)
            (AboveOf17 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf7 ?x)
          )
          (and
            (AboveOf18 ?x)
            (BelowOf6 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf17 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf16 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf17 ?x)
          )
          (and
            (AboveOf16 ?x)
            (BelowOf3 ?x)
          )
          (and
            (BelowOf14 ?x)
            (AboveOf15 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf19 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf18 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf18 ?x)
          )
          (and
            (AboveOf18 ?x)
            (BelowOf12 ?x)
          )
        )
        (and
          (Row13 ?x)
        )
      )
      (when
        (or
          (and
            (BelowOf15 ?x)
            (AboveOf18 ?x)
          )
          (and
            (AboveOf19 ?x)
            (BelowOf8 ?x)
          )
          (and
            (AboveOf19 ?x)
            (BelowOf12 ?x)
          )
          (and
            (AboveOf18 ?x)
            (BelowOf3 ?x)
          )
          (and
            (AboveOf15 ?x)
            (BelowOf8 ?x)
          )
          (and
            (AboveOf15 ?x)
            (BelowOf1 ?x)
          )
          (and
            (AboveOf15 ?x)
            (BelowOf9 ?x)
          )
          (and
            (BelowOf15 ?x)
            (AboveOf17 ?x)
          )
          (and
            (BelowOf16 ?x)
            (AboveOf18 ?x)
          )
          (and
            (BelowOf6 ?x)
            (AboveOf18 ?x)
          )
          (and
            (AboveOf15 ?x)
            (BelowOf6 ?x)
          )
          (and
            (AboveOf18 ?x)
            (BelowOf11 ?x)
          )
          (and
            (BelowOf7 ?x)
            (AboveOf17 ?x)
          )
          (and
            (AboveOf17 ?x)
            (BelowOf12 ?x)
          )
          (and
            (AboveOf15 ?x)
            (BelowOf16 ?x)
          )
          (and
            (BelowOf14 ?x)
            (AboveOf16 ?x)
          )
          (and
            (BelowOf16 ?x)
            (AboveOf19 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf16 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf17 ?x)
          )
          (and
            (AboveOf19 ?x)
            (BelowOf3 ?x)
          )
          (and
            (AboveOf15 ?x)
            (BelowOf4 ?x)
          )
          (and
            (BelowOf3 ?x)
            (AboveOf19 ?x)
          )
          (and
            (AboveOf16 ?x)
            (BelowOf10 ?x)
          )
          (and
            (BelowOf13 ?x)
            (AboveOf16 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf15 ?x)
          )
          (and
            (BelowOf9 ?x)
            (AboveOf18 ?x)
          )
          (and
            (AboveOf16 ?x)
            (BelowOf8 ?x)
          )
          (and
            (AboveOf17 ?x)
            (BelowOf6 ?x)
          )
          (and
            (BelowOf14 ?x)
            (AboveOf19 ?x)
          )
          (and
            (AboveOf17 ?x)
            (BelowOf11 ?x)
          )
          (and
            (BelowOf10 ?x)
            (AboveOf19 ?x)
          )
          (and
            (AboveOf16 ?x)
            (BelowOf12 ?x)
          )
          (and
            (BelowOf9 ?x)
            (AboveOf19 ?x)
          )
          (and
            (AboveOf18 ?x)
            (BelowOf10 ?x)
          )
          (and
            (AboveOf15 ?x)
            (BelowOf3 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf19 ?x)
          )
          (and
            (BelowOf13 ?x)
            (AboveOf17 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf18 ?x)
          )
          (and
            (AboveOf15 ?x)
            (BelowOf11 ?x)
          )
          (and
            (BelowOf16 ?x)
            (AboveOf16 ?x)
          )
          (and
            (BelowOf11 ?x)
            (AboveOf19 ?x)
          )
          (and
            (BelowOf13 ?x)
            (AboveOf18 ?x)
          )
          (and
            (BelowOf9 ?x)
            (AboveOf17 ?x)
          )
          (and
            (BelowOf14 ?x)
            (AboveOf17 ?x)
          )
          (and
            (AboveOf16 ?x)
            (BelowOf6 ?x)
          )
          (and
            (BelowOf13 ?x)
            (AboveOf19 ?x)
          )
          (and
            (BelowOf16 ?x)
            (AboveOf17 ?x)
          )
          (and
            (BelowOf13 ?x)
            (AboveOf15 ?x)
          )
          (and
            (AboveOf18 ?x)
            (BelowOf6 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf17 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf19 ?x)
          )
          (and
            (AboveOf18 ?x)
            (BelowOf8 ?x)
          )
          (and
            (BelowOf7 ?x)
            (AboveOf19 ?x)
          )
          (and
            (BelowOf14 ?x)
            (AboveOf18 ?x)
          )
          (and
            (AboveOf17 ?x)
            (BelowOf10 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf17 ?x)
          )
          (and
            (AboveOf16 ?x)
            (BelowOf3 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf15 ?x)
          )
          (and
            (BelowOf15 ?x)
            (AboveOf16 ?x)
          )
          (and
            (AboveOf16 ?x)
            (BelowOf4 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf19 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf18 ?x)
          )
          (and
            (AboveOf17 ?x)
            (BelowOf3 ?x)
          )
          (and
            (BelowOf7 ?x)
            (AboveOf18 ?x)
          )
          (and
            (AboveOf15 ?x)
            (BelowOf15 ?x)
          )
          (and
            (AboveOf17 ?x)
            (BelowOf1 ?x)
          )
          (and
            (AboveOf15 ?x)
            (BelowOf10 ?x)
          )
          (and
            (BelowOf14 ?x)
            (AboveOf15 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf16 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf19 ?x)
          )
          (and
            (AboveOf16 ?x)
            (BelowOf9 ?x)
          )
          (and
            (AboveOf15 ?x)
            (BelowOf7 ?x)
          )
          (and
            (AboveOf15 ?x)
            (BelowOf12 ?x)
          )
          (and
            (BelowOf7 ?x)
            (AboveOf16 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf18 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf17 ?x)
          )
          (and
            (BelowOf6 ?x)
            (AboveOf19 ?x)
          )
          (and
            (BelowOf15 ?x)
            (AboveOf19 ?x)
          )
          (and
            (AboveOf17 ?x)
            (BelowOf8 ?x)
          )
          (and
            (AboveOf16 ?x)
            (BelowOf11 ?x)
          )
          (and
            (AboveOf16 ?x)
            (BelowOf1 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf16 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf18 ?x)
          )
          (and
            (AboveOf18 ?x)
            (BelowOf12 ?x)
          )
        )
        (and
          (Row14 ?x)
        )
      )
      (when
        (or
          (and
            (BelowOf15 ?x)
            (AboveOf18 ?x)
          )
          (and
            (AboveOf19 ?x)
            (BelowOf8 ?x)
          )
          (and
            (AboveOf19 ?x)
            (BelowOf12 ?x)
          )
          (and
            (AboveOf18 ?x)
            (BelowOf3 ?x)
          )
          (and
            (AboveOf16 ?x)
            (BelowOf6 ?x)
          )
          (and
            (BelowOf15 ?x)
            (AboveOf17 ?x)
          )
          (and
            (BelowOf16 ?x)
            (AboveOf18 ?x)
          )
          (and
            (BelowOf6 ?x)
            (AboveOf18 ?x)
          )
          (and
            (AboveOf18 ?x)
            (BelowOf11 ?x)
          )
          (and
            (BelowOf7 ?x)
            (AboveOf17 ?x)
          )
          (and
            (BelowOf17 ?x)
            (AboveOf19 ?x)
          )
          (and
            (BelowOf9 ?x)
            (AboveOf18 ?x)
          )
          (and
            (BelowOf14 ?x)
            (AboveOf16 ?x)
          )
          (and
            (BelowOf16 ?x)
            (AboveOf19 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf16 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf17 ?x)
          )
          (and
            (BelowOf9 ?x)
            (AboveOf19 ?x)
          )
          (and
            (AboveOf17 ?x)
            (BelowOf12 ?x)
          )
          (and
            (BelowOf3 ?x)
            (AboveOf19 ?x)
          )
          (and
            (BelowOf17 ?x)
            (AboveOf17 ?x)
          )
          (and
            (BelowOf13 ?x)
            (AboveOf16 ?x)
          )
          (and
            (AboveOf16 ?x)
            (BelowOf8 ?x)
          )
          (and
            (AboveOf17 ?x)
            (BelowOf6 ?x)
          )
          (and
            (BelowOf14 ?x)
            (AboveOf19 ?x)
          )
          (and
            (AboveOf17 ?x)
            (BelowOf11 ?x)
          )
          (and
            (BelowOf10 ?x)
            (AboveOf19 ?x)
          )
          (and
            (AboveOf16 ?x)
            (BelowOf12 ?x)
          )
          (and
            (AboveOf18 ?x)
            (BelowOf10 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf19 ?x)
          )
          (and
            (BelowOf13 ?x)
            (AboveOf17 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf18 ?x)
          )
          (and
            (BelowOf16 ?x)
            (AboveOf16 ?x)
          )
          (and
            (BelowOf11 ?x)
            (AboveOf19 ?x)
          )
          (and
            (BelowOf13 ?x)
            (AboveOf18 ?x)
          )
          (and
            (BelowOf9 ?x)
            (AboveOf17 ?x)
          )
          (and
            (BelowOf14 ?x)
            (AboveOf17 ?x)
          )
          (and
            (BelowOf13 ?x)
            (AboveOf19 ?x)
          )
          (and
            (BelowOf16 ?x)
            (AboveOf17 ?x)
          )
          (and
            (AboveOf18 ?x)
            (BelowOf6 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf17 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf19 ?x)
          )
          (and
            (AboveOf18 ?x)
            (BelowOf8 ?x)
          )
          (and
            (BelowOf7 ?x)
            (AboveOf19 ?x)
          )
          (and
            (BelowOf14 ?x)
            (AboveOf18 ?x)
          )
          (and
            (AboveOf17 ?x)
            (BelowOf10 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf17 ?x)
          )
          (and
            (AboveOf16 ?x)
            (BelowOf3 ?x)
          )
          (and
            (BelowOf15 ?x)
            (AboveOf16 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf19 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf18 ?x)
          )
          (and
            (AboveOf17 ?x)
            (BelowOf3 ?x)
          )
          (and
            (BelowOf7 ?x)
            (AboveOf18 ?x)
          )
          (and
            (AboveOf17 ?x)
            (BelowOf1 ?x)
          )
          (and
            (AboveOf16 ?x)
            (BelowOf17 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf17 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf16 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf19 ?x)
          )
          (and
            (AboveOf16 ?x)
            (BelowOf9 ?x)
          )
          (and
            (AboveOf19 ?x)
            (BelowOf3 ?x)
          )
          (and
            (BelowOf17 ?x)
            (AboveOf18 ?x)
          )
          (and
            (BelowOf7 ?x)
            (AboveOf16 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf18 ?x)
          )
          (and
            (AboveOf16 ?x)
            (BelowOf10 ?x)
          )
          (and
            (BelowOf6 ?x)
            (AboveOf19 ?x)
          )
          (and
            (BelowOf15 ?x)
            (AboveOf19 ?x)
          )
          (and
            (AboveOf17 ?x)
            (BelowOf8 ?x)
          )
          (and
            (AboveOf16 ?x)
            (BelowOf11 ?x)
          )
          (and
            (AboveOf16 ?x)
            (BelowOf1 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf16 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf18 ?x)
          )
          (and
            (AboveOf18 ?x)
            (BelowOf12 ?x)
          )
        )
        (and
          (Row15 ?x)
        )
      )
      (when
        (or
          (and
            (BelowOf15 ?x)
            (AboveOf18 ?x)
          )
          (and
            (BelowOf9 ?x)
            (AboveOf17 ?x)
          )
          (and
            (AboveOf19 ?x)
            (BelowOf8 ?x)
          )
          (and
            (AboveOf19 ?x)
            (BelowOf12 ?x)
          )
          (and
            (AboveOf18 ?x)
            (BelowOf3 ?x)
          )
          (and
            (BelowOf15 ?x)
            (AboveOf17 ?x)
          )
          (and
            (BelowOf16 ?x)
            (AboveOf18 ?x)
          )
          (and
            (BelowOf6 ?x)
            (AboveOf18 ?x)
          )
          (and
            (AboveOf18 ?x)
            (BelowOf11 ?x)
          )
          (and
            (BelowOf7 ?x)
            (AboveOf17 ?x)
          )
          (and
            (AboveOf17 ?x)
            (BelowOf12 ?x)
          )
          (and
            (BelowOf9 ?x)
            (AboveOf18 ?x)
          )
          (and
            (BelowOf14 ?x)
            (AboveOf18 ?x)
          )
          (and
            (BelowOf16 ?x)
            (AboveOf19 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf18 ?x)
          )
          (and
            (BelowOf9 ?x)
            (AboveOf19 ?x)
          )
          (and
            (BelowOf17 ?x)
            (AboveOf19 ?x)
          )
          (and
            (BelowOf3 ?x)
            (AboveOf19 ?x)
          )
          (and
            (BelowOf17 ?x)
            (AboveOf17 ?x)
          )
          (and
            (AboveOf17 ?x)
            (BelowOf6 ?x)
          )
          (and
            (BelowOf14 ?x)
            (AboveOf19 ?x)
          )
          (and
            (AboveOf17 ?x)
            (BelowOf11 ?x)
          )
          (and
            (BelowOf10 ?x)
            (AboveOf19 ?x)
          )
          (and
            (BelowOf13 ?x)
            (AboveOf17 ?x)
          )
          (and
            (AboveOf18 ?x)
            (BelowOf10 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf19 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf18 ?x)
          )
          (and
            (BelowOf11 ?x)
            (AboveOf19 ?x)
          )
          (and
            (BelowOf13 ?x)
            (AboveOf18 ?x)
          )
          (and
            (AboveOf18 ?x)
            (BelowOf18 ?x)
          )
          (and
            (BelowOf14 ?x)
            (AboveOf17 ?x)
          )
          (and
            (BelowOf13 ?x)
            (AboveOf19 ?x)
          )
          (and
            (BelowOf16 ?x)
            (AboveOf17 ?x)
          )
          (and
            (AboveOf18 ?x)
            (BelowOf6 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf17 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf19 ?x)
          )
          (and
            (AboveOf18 ?x)
            (BelowOf8 ?x)
          )
          (and
            (AboveOf17 ?x)
            (BelowOf18 ?x)
          )
          (and
            (BelowOf7 ?x)
            (AboveOf19 ?x)
          )
          (and
            (AboveOf17 ?x)
            (BelowOf10 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf17 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf19 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf18 ?x)
          )
          (and
            (AboveOf17 ?x)
            (BelowOf3 ?x)
          )
          (and
            (BelowOf7 ?x)
            (AboveOf18 ?x)
          )
          (and
            (AboveOf17 ?x)
            (BelowOf1 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf19 ?x)
          )
          (and
            (AboveOf19 ?x)
            (BelowOf3 ?x)
          )
          (and
            (BelowOf17 ?x)
            (AboveOf18 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf18 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf17 ?x)
          )
          (and
            (BelowOf6 ?x)
            (AboveOf19 ?x)
          )
          (and
            (BelowOf15 ?x)
            (AboveOf19 ?x)
          )
          (and
            (AboveOf17 ?x)
            (BelowOf8 ?x)
          )
          (and
            (BelowOf18 ?x)
            (AboveOf19 ?x)
          )
          (and
            (AboveOf18 ?x)
            (BelowOf12 ?x)
          )
        )
        (and
          (Row16 ?x)
        )
      )
      (when
        (or
          (and
            (BelowOf15 ?x)
            (AboveOf18 ?x)
          )
          (and
            (AboveOf19 ?x)
            (BelowOf12 ?x)
          )
          (and
            (AboveOf18 ?x)
            (BelowOf3 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf19 ?x)
          )
          (and
            (BelowOf16 ?x)
            (AboveOf18 ?x)
          )
          (and
            (BelowOf6 ?x)
            (AboveOf18 ?x)
          )
          (and
            (AboveOf18 ?x)
            (BelowOf11 ?x)
          )
          (and
            (BelowOf17 ?x)
            (AboveOf19 ?x)
          )
          (and
            (BelowOf9 ?x)
            (AboveOf18 ?x)
          )
          (and
            (BelowOf14 ?x)
            (AboveOf18 ?x)
          )
          (and
            (AboveOf18 ?x)
            (BelowOf10 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf18 ?x)
          )
          (and
            (BelowOf9 ?x)
            (AboveOf19 ?x)
          )
          (and
            (BelowOf3 ?x)
            (AboveOf19 ?x)
          )
          (and
            (BelowOf19 ?x)
            (AboveOf19 ?x)
          )
          (and
            (AboveOf19 ?x)
            (BelowOf8 ?x)
          )
          (and
            (BelowOf14 ?x)
            (AboveOf19 ?x)
          )
          (and
            (BelowOf10 ?x)
            (AboveOf19 ?x)
          )
          (and
            (BelowOf16 ?x)
            (AboveOf19 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf19 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf18 ?x)
          )
          (and
            (BelowOf11 ?x)
            (AboveOf19 ?x)
          )
          (and
            (BelowOf13 ?x)
            (AboveOf18 ?x)
          )
          (and
            (AboveOf18 ?x)
            (BelowOf18 ?x)
          )
          (and
            (BelowOf13 ?x)
            (AboveOf19 ?x)
          )
          (and
            (AboveOf19 ?x)
            (BelowOf3 ?x)
          )
          (and
            (AboveOf18 ?x)
            (BelowOf8 ?x)
          )
          (and
            (AboveOf18 ?x)
            (BelowOf19 ?x)
          )
          (and
            (BelowOf7 ?x)
            (AboveOf19 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf19 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf18 ?x)
          )
          (and
            (BelowOf7 ?x)
            (AboveOf18 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf19 ?x)
          )
          (and
            (BelowOf17 ?x)
            (AboveOf18 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf18 ?x)
          )
          (and
            (BelowOf6 ?x)
            (AboveOf19 ?x)
          )
          (and
            (BelowOf15 ?x)
            (AboveOf19 ?x)
          )
          (and
            (BelowOf18 ?x)
            (AboveOf19 ?x)
          )
          (and
            (AboveOf18 ?x)
            (BelowOf12 ?x)
          )
        )
        (and
          (Row17 ?x)
        )
      )
      (when
        (or
          (and
            (AboveOf19 ?x)
            (BelowOf3 ?x)
          )
          (and
            (BelowOf17 ?x)
            (AboveOf19 ?x)
          )
          (and
            (BelowOf13 ?x)
            (AboveOf19 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf19 ?x)
          )
          (and
            (BelowOf9 ?x)
            (AboveOf19 ?x)
          )
          (and
            (AboveOf19 ?x)
            (BelowOf12 ?x)
          )
          (and
            (AboveOf19 ?x)
            (BelowOf8 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf19 ?x)
          )
          (and
            (BelowOf14 ?x)
            (AboveOf19 ?x)
          )
          (and
            (BelowOf11 ?x)
            (AboveOf19 ?x)
          )
          (and
            (BelowOf10 ?x)
            (AboveOf19 ?x)
          )
          (and
            (BelowOf6 ?x)
            (AboveOf19 ?x)
          )
          (and
            (BelowOf7 ?x)
            (AboveOf19 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf19 ?x)
          )
          (and
            (BelowOf20 ?x)
            (AboveOf19 ?x)
          )
          (and
            (AboveOf19 ?x)
            (BelowOf19 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf19 ?x)
          )
          (and
            (BelowOf18 ?x)
            (AboveOf19 ?x)
          )
          (and
            (BelowOf16 ?x)
            (AboveOf19 ?x)
          )
          (and
            (BelowOf15 ?x)
            (AboveOf19 ?x)
          )
        )
        (and
          (Row18 ?x)
        )
      )
    )
  )
  (:action CheckConsistencyAction
    :parameters ( )
    :precondition ( and
      (CheckConsistency)
      (not (Error))
    )
    :effect ( and
      (not (CheckConsistency))
      (when
        (or
          (exists (?x_2 )
            (and
              (RightOf15 ?x_2)
              (LeftOf3 ?x_2)
            )
          )
          (exists (?x_7 )
            (and
              (AboveOf19 ?x_7)
              (BelowOf2 ?x_7)
            )
          )
          (exists (?x_29 )
            (and
              (BelowOf7 ?x_29)
              (AboveOf19 ?x_29)
            )
          )
          (exists (?x_23 )
            (and
              (RightOf13 ?x_23)
              (LeftOf3 ?x_23)
            )
          )
          (exists (?x_0 )
            (and
              (AboveOf3 ?x_0)
              (BelowOf3 ?x_0)
            )
          )
          (exists (?x_10 )
            (and
              (AboveOf8 ?x_10)
              (BelowOf4 ?x_10)
            )
          )
          (exists (?x_35 )
            (and
              (BelowOf3 ?x_35)
              (AboveOf13 ?x_35)
            )
          )
          (exists (?x_32 )
            (and
              (AboveOf11 ?x_32)
              (BelowOf6 ?x_32)
            )
          )
          (exists (?x_16 )
            (and
              (LeftOf2 ?x_16)
              (RightOf11 ?x_16)
            )
          )
          (exists (?x_18 )
            (and
              (RightOf18 ?x_18)
              (LeftOf8 ?x_18)
            )
          )
          (exists (?x_0 )
            (and
              (BelowOf3 ?x_0)
              (AboveOf4 ?x_0)
            )
          )
          (exists (?x_33 )
            (and
              (AboveOf18 ?x_33)
              (BelowOf4 ?x_33)
            )
          )
          (exists (?x_12 )
            (and
              (RightOf8 ?x_12)
              (LeftOf5 ?x_12)
            )
          )
          (exists (?x_35 )
            (and
              (AboveOf12 ?x_35)
              (BelowOf12 ?x_35)
            )
          )
          (exists (?x_17 )
            (and
              (RightOf3 ?x_17)
              (LeftOf1 ?x_17)
            )
          )
          (exists (?x_19 )
            (and
              (AboveOf19 ?x_19)
              (BelowOf18 ?x_19)
            )
          )
          (exists (?x_21 )
            (and
              (LeftOf5 ?x_21)
              (RightOf11 ?x_21)
            )
          )
          (exists (?x_12 )
            (and
              (LeftOf5 ?x_12)
              (RightOf11 ?x_12)
            )
          )
          (exists (?x_5 )
            (and
              (RightOf19 ?x_5)
              (LeftOf8 ?x_5)
            )
          )
          (exists (?x_10 )
            (and
              (AboveOf16 ?x_10)
              (BelowOf4 ?x_10)
            )
          )
          (exists (?x_26 )
            (and
              (RightOf18 ?x_26)
              (LeftOf7 ?x_26)
            )
          )
          (exists (?x_29 )
            (and
              (BelowOf5 ?x_29)
              (AboveOf8 ?x_29)
            )
          )
          (exists (?x_34 )
            (and
              (BelowOf11 ?x_34)
              (AboveOf19 ?x_34)
            )
          )
          (exists (?x_11 )
            (and
              (AboveOf14 ?x_11)
              (BelowOf4 ?x_11)
            )
          )
          (exists (?x_25 )
            (and
              (LeftOf8 ?x_25)
              (RightOf18 ?x_25)
            )
          )
          (exists (?x_8 )
            (and
              (RightOf6 ?x_8)
              (LeftOf3 ?x_8)
            )
          )
          (exists (?x_29 )
            (and
              (AboveOf17 ?x_29)
              (BelowOf3 ?x_29)
            )
          )
          (exists (?x_6 )
            (and
              (RightOf17 ?x_6)
              (LeftOf14 ?x_6)
            )
          )
          (exists (?x_22 )
            (and
              (BelowOf11 ?x_22)
              (AboveOf19 ?x_22)
            )
          )
          (exists (?x_31 )
            (and
              (LeftOf3 ?x_31)
              (RightOf16 ?x_31)
            )
          )
          (exists (?x_27 )
            (and
              (BelowOf3 ?x_27)
              (AboveOf11 ?x_27)
            )
          )
          (exists (?x_33 )
            (and
              (AboveOf11 ?x_33)
              (BelowOf5 ?x_33)
            )
          )
          (exists (?x_35 )
            (and
              (AboveOf17 ?x_35)
              (BelowOf9 ?x_35)
            )
          )
          (exists (?x_22 )
            (and
              (AboveOf19 ?x_22)
              (BelowOf5 ?x_22)
            )
          )
          (exists (?x_14 )
            (and
              (RightOf19 ?x_14)
              (LeftOf2 ?x_14)
            )
          )
          (exists (?x_18 )
            (and
              (RightOf18 ?x_18)
              (LeftOf1 ?x_18)
            )
          )
          (exists (?x_2 )
            (and
              (LeftOf6 ?x_2)
              (RightOf19 ?x_2)
            )
          )
          (exists (?x_13 )
            (and
              (LeftOf1 ?x_13)
              (RightOf11 ?x_13)
            )
          )
          (exists (?x_7 )
            (and
              (AboveOf16 ?x_7)
              (BelowOf1 ?x_7)
            )
          )
          (exists (?x_14 )
            (and
              (RightOf16 ?x_14)
              (LeftOf2 ?x_14)
            )
          )
          (exists (?x_12 )
            (and
              (RightOf16 ?x_12)
              (LeftOf6 ?x_12)
            )
          )
          (exists (?x_37 )
            (and
              (AboveOf10 ?x_37)
              (BelowOf1 ?x_37)
            )
          )
          (exists (?x_4 )
            (and
              (LeftOf8 ?x_4)
              (RightOf17 ?x_4)
            )
          )
          (exists (?x_28 )
            (and
              (RightOf11 ?x_28)
              (LeftOf1 ?x_28)
            )
          )
          (exists (?x_33 )
            (and
              (AboveOf13 ?x_33)
              (BelowOf8 ?x_33)
            )
          )
          (exists (?x_16 )
            (and
              (LeftOf2 ?x_16)
              (RightOf16 ?x_16)
            )
          )
          (exists (?x_19 )
            (and
              (AboveOf18 ?x_19)
              (BelowOf10 ?x_19)
            )
          )
          (exists (?x_29 )
            (and
              (AboveOf17 ?x_29)
              (BelowOf2 ?x_29)
            )
          )
          (exists (?x_24 )
            (and
              (BelowOf1 ?x_24)
              (AboveOf15 ?x_24)
            )
          )
          (exists (?x_29 )
            (and
              (BelowOf1 ?x_29)
              (AboveOf10 ?x_29)
            )
          )
          (exists (?x_18 )
            (and
              (LeftOf11 ?x_18)
              (RightOf16 ?x_18)
            )
          )
          (exists (?x_3 )
            (and
              (BelowOf12 ?x_3)
              (AboveOf17 ?x_3)
            )
          )
          (exists (?x_10 )
            (and
              (BelowOf2 ?x_10)
              (AboveOf7 ?x_10)
            )
          )
          (exists (?x_4 )
            (and
              (LeftOf2 ?x_4)
              (RightOf19 ?x_4)
            )
          )
          (exists (?x_23 )
            (and
              (RightOf17 ?x_23)
              (LeftOf6 ?x_23)
            )
          )
          (exists (?x_37 )
            (and
              (BelowOf3 ?x_37)
              (AboveOf13 ?x_37)
            )
          )
          (exists (?x_8 )
            (and
              (RightOf18 ?x_8)
              (LeftOf1 ?x_8)
            )
          )
          (exists (?x_34 )
            (and
              (BelowOf3 ?x_34)
              (AboveOf16 ?x_34)
            )
          )
          (exists (?x_26 )
            (and
              (RightOf19 ?x_26)
              (LeftOf9 ?x_26)
            )
          )
          (exists (?x_35 )
            (and
              (BelowOf11 ?x_35)
              (AboveOf18 ?x_35)
            )
          )
          (exists (?x_7 )
            (and
              (AboveOf10 ?x_7)
              (BelowOf5 ?x_7)
            )
          )
          (exists (?x_0 )
            (and
              (AboveOf3 ?x_0)
              (BelowOf2 ?x_0)
            )
          )
          (exists (?x_34 )
            (and
              (AboveOf19 ?x_34)
              (BelowOf2 ?x_34)
            )
          )
          (exists (?x_2 )
            (and
              (LeftOf4 ?x_2)
              (RightOf10 ?x_2)
            )
          )
          (exists (?x_4 )
            (and
              (LeftOf8 ?x_4)
              (RightOf19 ?x_4)
            )
          )
          (exists (?x_12 )
            (and
              (RightOf18 ?x_12)
              (LeftOf6 ?x_12)
            )
          )
          (exists (?x_28 )
            (and
              (LeftOf2 ?x_28)
              (RightOf11 ?x_28)
            )
          )
          (exists (?x_33 )
            (and
              (AboveOf10 ?x_33)
              (BelowOf9 ?x_33)
            )
          )
          (exists (?x_7 )
            (and
              (BelowOf2 ?x_7)
              (AboveOf9 ?x_7)
            )
          )
          (exists (?x_13 )
            (and
              (LeftOf10 ?x_13)
              (RightOf17 ?x_13)
            )
          )
          (exists (?x_33 )
            (and
              (BelowOf1 ?x_33)
              (AboveOf14 ?x_33)
            )
          )
          (exists (?x_31 )
            (and
              (RightOf9 ?x_31)
              (LeftOf1 ?x_31)
            )
          )
          (exists (?x_5 )
            (and
              (LeftOf9 ?x_5)
              (RightOf19 ?x_5)
            )
          )
          (exists (?x_37 )
            (and
              (BelowOf2 ?x_37)
              (AboveOf18 ?x_37)
            )
          )
          (exists (?x_29 )
            (and
              (BelowOf7 ?x_29)
              (AboveOf14 ?x_29)
            )
          )
          (exists (?x_9 )
            (and
              (BelowOf2 ?x_9)
              (AboveOf18 ?x_9)
            )
          )
          (exists (?x_24 )
            (and
              (AboveOf17 ?x_24)
              (BelowOf11 ?x_24)
            )
          )
          (exists (?x_20 )
            (and
              (RightOf16 ?x_20)
              (LeftOf10 ?x_20)
            )
          )
          (exists (?x_5 )
            (and
              (LeftOf11 ?x_5)
              (RightOf12 ?x_5)
            )
          )
          (exists (?x_19 )
            (and
              (AboveOf19 ?x_19)
              (BelowOf4 ?x_19)
            )
          )
          (exists (?x_16 )
            (and
              (RightOf13 ?x_16)
              (LeftOf4 ?x_16)
            )
          )
          (exists (?x_29 )
            (and
              (BelowOf5 ?x_29)
              (AboveOf11 ?x_29)
            )
          )
          (exists (?x_5 )
            (and
              (LeftOf7 ?x_5)
              (RightOf19 ?x_5)
            )
          )
          (exists (?x_23 )
            (and
              (RightOf8 ?x_23)
              (LeftOf7 ?x_23)
            )
          )
          (exists (?x_30 )
            (and
              (LeftOf10 ?x_30)
              (RightOf19 ?x_30)
            )
          )
          (exists (?x_28 )
            (and
              (RightOf5 ?x_28)
              (LeftOf2 ?x_28)
            )
          )
          (exists (?x_37 )
            (and
              (AboveOf8 ?x_37)
              (BelowOf5 ?x_37)
            )
          )
          (exists (?x_20 )
            (and
              (RightOf13 ?x_20)
              (LeftOf10 ?x_20)
            )
          )
          (exists (?x_31 )
            (and
              (LeftOf1 ?x_31)
              (RightOf15 ?x_31)
            )
          )
          (exists (?x_11 )
            (and
              (AboveOf19 ?x_11)
              (BelowOf4 ?x_11)
            )
          )
          (exists (?x_27 )
            (and
              (AboveOf7 ?x_27)
              (BelowOf6 ?x_27)
            )
          )
          (exists (?x_10 )
            (and
              (BelowOf3 ?x_10)
              (AboveOf15 ?x_10)
            )
          )
          (exists (?x_29 )
            (and
              (BelowOf4 ?x_29)
              (AboveOf19 ?x_29)
            )
          )
          (exists (?x_27 )
            (and
              (AboveOf13 ?x_27)
              (BelowOf6 ?x_27)
            )
          )
          (exists (?x_0 )
            (and
              (AboveOf8 ?x_0)
              (BelowOf2 ?x_0)
            )
          )
          (exists (?x_15 )
            (and
              (BelowOf1 ?x_15)
              (AboveOf14 ?x_15)
            )
          )
          (exists (?x_35 )
            (and
              (AboveOf16 ?x_35)
              (BelowOf7 ?x_35)
            )
          )
          (exists (?x_24 )
            (and
              (AboveOf16 ?x_24)
              (BelowOf15 ?x_24)
            )
          )
          (exists (?x_26 )
            (and
              (RightOf19 ?x_26)
              (LeftOf1 ?x_26)
            )
          )
          (exists (?x_2 )
            (and
              (RightOf19 ?x_2)
              (LeftOf3 ?x_2)
            )
          )
          (exists (?x_25 )
            (and
              (RightOf19 ?x_25)
              (LeftOf2 ?x_25)
            )
          )
          (exists (?x_2 )
            (and
              (LeftOf4 ?x_2)
              (RightOf12 ?x_2)
            )
          )
          (exists (?x_10 )
            (and
              (BelowOf3 ?x_10)
              (AboveOf13 ?x_10)
            )
          )
          (exists (?x_32 )
            (and
              (BelowOf1 ?x_32)
              (AboveOf17 ?x_32)
            )
          )
          (exists (?x_15 )
            (and
              (BelowOf1 ?x_15)
              (AboveOf8 ?x_15)
            )
          )
          (exists (?x_33 )
            (and
              (AboveOf9 ?x_33)
              (BelowOf8 ?x_33)
            )
          )
          (exists (?x_21 )
            (and
              (RightOf16 ?x_21)
              (LeftOf1 ?x_21)
            )
          )
          (exists (?x_0 )
            (and
              (AboveOf5 ?x_0)
              (BelowOf1 ?x_0)
            )
          )
          (exists (?x_6 )
            (and
              (LeftOf5 ?x_6)
              (RightOf16 ?x_6)
            )
          )
          (exists (?x_14 )
            (and
              (LeftOf11 ?x_14)
              (RightOf17 ?x_14)
            )
          )
          (exists (?x_31 )
            (and
              (RightOf15 ?x_31)
              (LeftOf4 ?x_31)
            )
          )
          (exists (?x_35 )
            (and
              (BelowOf12 ?x_35)
              (AboveOf17 ?x_35)
            )
          )
          (exists (?x_22 )
            (and
              (AboveOf17 ?x_22)
              (BelowOf2 ?x_22)
            )
          )
          (exists (?x_14 )
            (and
              (RightOf14 ?x_14)
              (LeftOf1 ?x_14)
            )
          )
          (exists (?x_0 )
            (and
              (AboveOf9 ?x_0)
              (BelowOf2 ?x_0)
            )
          )
          (exists (?x_7 )
            (and
              (AboveOf8 ?x_7)
              (BelowOf5 ?x_7)
            )
          )
          (exists (?x_13 )
            (and
              (RightOf14 ?x_13)
              (LeftOf8 ?x_13)
            )
          )
          (exists (?x_8 )
            (and
              (LeftOf1 ?x_8)
              (RightOf8 ?x_8)
            )
          )
          (exists (?x_20 )
            (and
              (RightOf13 ?x_20)
              (LeftOf11 ?x_20)
            )
          )
          (exists (?x_20 )
            (and
              (RightOf16 ?x_20)
              (LeftOf7 ?x_20)
            )
          )
          (exists (?x_21 )
            (and
              (LeftOf2 ?x_21)
              (RightOf17 ?x_21)
            )
          )
          (exists (?x_19 )
            (and
              (AboveOf18 ?x_19)
              (BelowOf4 ?x_19)
            )
          )
          (exists (?x_32 )
            (and
              (AboveOf15 ?x_32)
              (BelowOf9 ?x_32)
            )
          )
          (exists (?x_20 )
            (and
              (RightOf15 ?x_20)
              (LeftOf5 ?x_20)
            )
          )
          (exists (?x_12 )
            (and
              (LeftOf2 ?x_12)
              (RightOf19 ?x_12)
            )
          )
          (exists (?x_27 )
            (and
              (AboveOf6 ?x_27)
              (BelowOf3 ?x_27)
            )
          )
          (exists (?x_6 )
            (and
              (LeftOf15 ?x_6)
              (RightOf19 ?x_6)
            )
          )
          (exists (?x_4 )
            (and
              (RightOf18 ?x_4)
              (LeftOf2 ?x_4)
            )
          )
          (exists (?x_6 )
            (and
              (RightOf19 ?x_6)
              (LeftOf5 ?x_6)
            )
          )
          (exists (?x_21 )
            (and
              (RightOf16 ?x_21)
              (LeftOf2 ?x_21)
            )
          )
          (exists (?x_27 )
            (and
              (AboveOf11 ?x_27)
              (BelowOf5 ?x_27)
            )
          )
          (exists (?x_37 )
            (and
              (AboveOf17 ?x_37)
              (BelowOf2 ?x_37)
            )
          )
          (exists (?x_4 )
            (and
              (RightOf19 ?x_4)
              (LeftOf9 ?x_4)
            )
          )
          (exists (?x_19 )
            (and
              (AboveOf18 ?x_19)
              (BelowOf9 ?x_19)
            )
          )
          (exists (?x_9 )
            (and
              (BelowOf14 ?x_9)
              (AboveOf18 ?x_9)
            )
          )
          (exists (?x_1 )
            (and
              (BelowOf1 ?x_1)
              (AboveOf7 ?x_1)
            )
          )
          (exists (?x_6 )
            (and
              (LeftOf5 ?x_6)
              (RightOf17 ?x_6)
            )
          )
          (exists (?x_30 )
            (and
              (RightOf19 ?x_30)
              (LeftOf19 ?x_30)
            )
          )
          (exists (?x_7 )
            (and
              (BelowOf7 ?x_7)
              (AboveOf10 ?x_7)
            )
          )
          (exists (?x_28 )
            (and
              (RightOf9 ?x_28)
              (LeftOf1 ?x_28)
            )
          )
          (exists (?x_29 )
            (and
              (BelowOf1 ?x_29)
              (AboveOf19 ?x_29)
            )
          )
          (exists (?x_14 )
            (and
              (RightOf17 ?x_14)
              (LeftOf7 ?x_14)
            )
          )
          (exists (?x_37 )
            (and
              (BelowOf5 ?x_37)
              (AboveOf7 ?x_37)
            )
          )
          (exists (?x_36 )
            (and
              (AboveOf19 ?x_36)
              (BelowOf17 ?x_36)
            )
          )
          (exists (?x_11 )
            (and
              (BelowOf3 ?x_11)
              (AboveOf16 ?x_11)
            )
          )
          (exists (?x_23 )
            (and
              (LeftOf7 ?x_23)
              (RightOf19 ?x_23)
            )
          )
          (exists (?x_22 )
            (and
              (AboveOf15 ?x_22)
              (BelowOf7 ?x_22)
            )
          )
          (exists (?x_12 )
            (and
              (LeftOf7 ?x_12)
              (RightOf7 ?x_12)
            )
          )
          (exists (?x_33 )
            (and
              (BelowOf1 ?x_33)
              (AboveOf19 ?x_33)
            )
          )
          (exists (?x_2 )
            (and
              (LeftOf4 ?x_2)
              (RightOf9 ?x_2)
            )
          )
          (exists (?x_12 )
            (and
              (LeftOf7 ?x_12)
              (RightOf9 ?x_12)
            )
          )
          (exists (?x_33 )
            (and
              (BelowOf4 ?x_33)
              (AboveOf14 ?x_33)
            )
          )
          (exists (?x_29 )
            (and
              (BelowOf6 ?x_29)
              (AboveOf11 ?x_29)
            )
          )
          (exists (?x_32 )
            (and
              (AboveOf15 ?x_32)
              (BelowOf5 ?x_32)
            )
          )
          (exists (?x_0 )
            (and
              (BelowOf1 ?x_0)
              (AboveOf13 ?x_0)
            )
          )
          (exists (?x_37 )
            (and
              (BelowOf2 ?x_37)
              (AboveOf13 ?x_37)
            )
          )
          (exists (?x_13 )
            (and
              (LeftOf1 ?x_13)
              (RightOf13 ?x_13)
            )
          )
          (exists (?x_13 )
            (and
              (RightOf10 ?x_13)
              (LeftOf5 ?x_13)
            )
          )
          (exists (?x_3 )
            (and
              (BelowOf15 ?x_3)
              (AboveOf18 ?x_3)
            )
          )
          (exists (?x_13 )
            (and
              (RightOf10 ?x_13)
              (LeftOf6 ?x_13)
            )
          )
          (exists (?x_31 )
            (and
              (LeftOf6 ?x_31)
              (RightOf14 ?x_31)
            )
          )
          (exists (?x_34 )
            (and
              (BelowOf13 ?x_34)
              (AboveOf15 ?x_34)
            )
          )
          (exists (?x_26 )
            (and
              (RightOf18 ?x_26)
              (LeftOf12 ?x_26)
            )
          )
          (exists (?x_6 )
            (and
              (RightOf16 ?x_6)
              (LeftOf14 ?x_6)
            )
          )
          (exists (?x_33 )
            (and
              (AboveOf11 ?x_33)
              (BelowOf3 ?x_33)
            )
          )
          (exists (?x_13 )
            (and
              (LeftOf9 ?x_13)
              (RightOf12 ?x_13)
            )
          )
          (exists (?x_2 )
            (and
              (RightOf6 ?x_2)
              (LeftOf4 ?x_2)
            )
          )
          (exists (?x_32 )
            (and
              (AboveOf15 ?x_32)
              (BelowOf6 ?x_32)
            )
          )
          (exists (?x_24 )
            (and
              (BelowOf11 ?x_24)
              (AboveOf19 ?x_24)
            )
          )
          (exists (?x_33 )
            (and
              (AboveOf16 ?x_33)
              (BelowOf8 ?x_33)
            )
          )
          (exists (?x_11 )
            (and
              (BelowOf5 ?x_11)
              (AboveOf14 ?x_11)
            )
          )
          (exists (?x_10 )
            (and
              (AboveOf14 ?x_10)
              (BelowOf3 ?x_10)
            )
          )
          (exists (?x_5 )
            (and
              (RightOf16 ?x_5)
              (LeftOf2 ?x_5)
            )
          )
          (exists (?x_13 )
            (and
              (RightOf15 ?x_13)
              (LeftOf3 ?x_13)
            )
          )
          (exists (?x_32 )
            (and
              (AboveOf13 ?x_32)
              (BelowOf3 ?x_32)
            )
          )
          (exists (?x_35 )
            (and
              (AboveOf16 ?x_35)
              (BelowOf2 ?x_35)
            )
          )
          (exists (?x_33 )
            (and
              (AboveOf14 ?x_33)
              (BelowOf6 ?x_33)
            )
          )
          (exists (?x_20 )
            (and
              (LeftOf7 ?x_20)
              (RightOf13 ?x_20)
            )
          )
          (exists (?x_34 )
            (and
              (AboveOf16 ?x_34)
              (BelowOf7 ?x_34)
            )
          )
          (exists (?x_24 )
            (and
              (BelowOf10 ?x_24)
              (AboveOf19 ?x_24)
            )
          )
          (exists (?x_35 )
            (and
              (AboveOf12 ?x_35)
              (BelowOf3 ?x_35)
            )
          )
          (exists (?x_0 )
            (and
              (BelowOf3 ?x_0)
              (AboveOf8 ?x_0)
            )
          )
          (exists (?x_23 )
            (and
              (LeftOf1 ?x_23)
              (RightOf18 ?x_23)
            )
          )
          (exists (?x_3 )
            (and
              (BelowOf8 ?x_3)
              (AboveOf19 ?x_3)
            )
          )
          (exists (?x_8 )
            (and
              (RightOf19 ?x_8)
              (LeftOf1 ?x_8)
            )
          )
          (exists (?x_32 )
            (and
              (BelowOf2 ?x_32)
              (AboveOf11 ?x_32)
            )
          )
          (exists (?x_36 )
            (and
              (AboveOf19 ?x_36)
              (BelowOf12 ?x_36)
            )
          )
          (exists (?x_11 )
            (and
              (BelowOf6 ?x_11)
              (AboveOf18 ?x_11)
            )
          )
          (exists (?x_16 )
            (and
              (LeftOf4 ?x_16)
              (RightOf8 ?x_16)
            )
          )
          (exists (?x_24 )
            (and
              (AboveOf16 ?x_24)
              (BelowOf4 ?x_24)
            )
          )
          (exists (?x_20 )
            (and
              (RightOf16 ?x_20)
              (LeftOf6 ?x_20)
            )
          )
          (exists (?x_32 )
            (and
              (AboveOf18 ?x_32)
              (BelowOf6 ?x_32)
            )
          )
          (exists (?x_23 )
            (and
              (LeftOf7 ?x_23)
              (RightOf12 ?x_23)
            )
          )
          (exists (?x_3 )
            (and
              (BelowOf6 ?x_3)
              (AboveOf16 ?x_3)
            )
          )
          (exists (?x_15 )
            (and
              (AboveOf9 ?x_15)
              (BelowOf1 ?x_15)
            )
          )
          (exists (?x_5 )
            (and
              (RightOf13 ?x_5)
              (LeftOf5 ?x_5)
            )
          )
          (exists (?x_27 )
            (and
              (BelowOf4 ?x_27)
              (AboveOf11 ?x_27)
            )
          )
          (exists (?x_21 )
            (and
              (RightOf6 ?x_21)
              (LeftOf1 ?x_21)
            )
          )
          (exists (?x_23 )
            (and
              (RightOf10 ?x_23)
              (LeftOf8 ?x_23)
            )
          )
          (exists (?x_2 )
            (and
              (LeftOf4 ?x_2)
              (RightOf8 ?x_2)
            )
          )
          (exists (?x_20 )
            (and
              (RightOf17 ?x_20)
              (LeftOf10 ?x_20)
            )
          )
          (exists (?x_37 )
            (and
              (AboveOf15 ?x_37)
              (BelowOf4 ?x_37)
            )
          )
          (exists (?x_27 )
            (and
              (BelowOf6 ?x_27)
              (AboveOf12 ?x_27)
            )
          )
          (exists (?x_9 )
            (and
              (AboveOf19 ?x_9)
              (BelowOf8 ?x_9)
            )
          )
          (exists (?x_11 )
            (and
              (BelowOf11 ?x_11)
              (AboveOf16 ?x_11)
            )
          )
          (exists (?x_13 )
            (and
              (LeftOf3 ?x_13)
              (RightOf17 ?x_13)
            )
          )
          (exists (?x_2 )
            (and
              (RightOf15 ?x_2)
              (LeftOf1 ?x_2)
            )
          )
          (exists (?x_35 )
            (and
              (BelowOf11 ?x_35)
              (AboveOf13 ?x_35)
            )
          )
          (exists (?x_13 )
            (and
              (LeftOf9 ?x_13)
              (RightOf13 ?x_13)
            )
          )
          (exists (?x_2 )
            (and
              (RightOf16 ?x_2)
              (LeftOf1 ?x_2)
            )
          )
          (exists (?x_12 )
            (and
              (RightOf19 ?x_12)
              (LeftOf3 ?x_12)
            )
          )
          (exists (?x_23 )
            (and
              (RightOf19 ?x_23)
              (LeftOf3 ?x_23)
            )
          )
          (exists (?x_37 )
            (and
              (BelowOf3 ?x_37)
              (AboveOf11 ?x_37)
            )
          )
          (exists (?x_6 )
            (and
              (RightOf18 ?x_6)
              (LeftOf10 ?x_6)
            )
          )
          (exists (?x_2 )
            (and
              (LeftOf6 ?x_2)
              (RightOf6 ?x_2)
            )
          )
          (exists (?x_4 )
            (and
              (RightOf18 ?x_4)
              (LeftOf15 ?x_4)
            )
          )
          (exists (?x_32 )
            (and
              (BelowOf7 ?x_32)
              (AboveOf12 ?x_32)
            )
          )
          (exists (?x_19 )
            (and
              (AboveOf18 ?x_19)
              (BelowOf12 ?x_19)
            )
          )
          (exists (?x_2 )
            (and
              (RightOf13 ?x_2)
              (LeftOf3 ?x_2)
            )
          )
          (exists (?x_9 )
            (and
              (BelowOf7 ?x_9)
              (AboveOf17 ?x_9)
            )
          )
          (exists (?x_16 )
            (and
              (LeftOf2 ?x_16)
              (RightOf4 ?x_16)
            )
          )
          (exists (?x_27 )
            (and
              (AboveOf7 ?x_27)
              (BelowOf1 ?x_27)
            )
          )
          (exists (?x_34 )
            (and
              (BelowOf12 ?x_34)
              (AboveOf19 ?x_34)
            )
          )
          (exists (?x_13 )
            (and
              (RightOf15 ?x_13)
              (LeftOf9 ?x_13)
            )
          )
          (exists (?x_11 )
            (and
              (AboveOf19 ?x_11)
              (BelowOf6 ?x_11)
            )
          )
          (exists (?x_18 )
            (and
              (RightOf17 ?x_18)
              (LeftOf3 ?x_18)
            )
          )
          (exists (?x_34 )
            (and
              (AboveOf17 ?x_34)
              (BelowOf3 ?x_34)
            )
          )
          (exists (?x_15 )
            (and
              (AboveOf6 ?x_15)
              (BelowOf1 ?x_15)
            )
          )
          (exists (?x_31 )
            (and
              (LeftOf5 ?x_31)
              (RightOf10 ?x_31)
            )
          )
          (exists (?x_4 )
            (and
              (RightOf19 ?x_4)
              (LeftOf13 ?x_4)
            )
          )
          (exists (?x_10 )
            (and
              (AboveOf14 ?x_10)
              (BelowOf4 ?x_10)
            )
          )
          (exists (?x_22 )
            (and
              (BelowOf12 ?x_22)
              (AboveOf16 ?x_22)
            )
          )
          (exists (?x_27 )
            (and
              (BelowOf3 ?x_27)
              (AboveOf8 ?x_27)
            )
          )
          (exists (?x_10 )
            (and
              (BelowOf1 ?x_10)
              (AboveOf7 ?x_10)
            )
          )
          (exists (?x_5 )
            (and
              (LeftOf9 ?x_5)
              (RightOf12 ?x_5)
            )
          )
          (exists (?x_26 )
            (and
              (LeftOf15 ?x_26)
              (RightOf18 ?x_26)
            )
          )
          (exists (?x_10 )
            (and
              (AboveOf6 ?x_10)
              (BelowOf1 ?x_10)
            )
          )
          (exists (?x_33 )
            (and
              (AboveOf19 ?x_33)
              (BelowOf5 ?x_33)
            )
          )
          (exists (?x_26 )
            (and
              (RightOf19 ?x_26)
              (LeftOf11 ?x_26)
            )
          )
          (exists (?x_24 )
            (and
              (AboveOf16 ?x_24)
              (BelowOf5 ?x_24)
            )
          )
          (exists (?x_5 )
            (and
              (LeftOf12 ?x_5)
              (RightOf12 ?x_5)
            )
          )
          (exists (?x_8 )
            (and
              (RightOf6 ?x_8)
              (LeftOf2 ?x_8)
            )
          )
          (exists (?x_32 )
            (and
              (BelowOf7 ?x_32)
              (AboveOf11 ?x_32)
            )
          )
          (exists (?x_33 )
            (and
              (BelowOf2 ?x_33)
              (AboveOf17 ?x_33)
            )
          )
          (exists (?x_32 )
            (and
              (AboveOf13 ?x_32)
              (BelowOf6 ?x_32)
            )
          )
          (exists (?x_3 )
            (and
              (BelowOf16 ?x_3)
              (AboveOf16 ?x_3)
            )
          )
          (exists (?x_15 )
            (and
              (AboveOf10 ?x_15)
              (BelowOf2 ?x_15)
            )
          )
          (exists (?x_13 )
            (and
              (RightOf11 ?x_13)
              (LeftOf3 ?x_13)
            )
          )
          (exists (?x_2 )
            (and
              (RightOf6 ?x_2)
              (LeftOf5 ?x_2)
            )
          )
          (exists (?x_21 )
            (and
              (LeftOf2 ?x_21)
              (RightOf16 ?x_21)
            )
          )
          (exists (?x_31 )
            (and
              (LeftOf4 ?x_31)
              (RightOf18 ?x_31)
            )
          )
          (exists (?x_11 )
            (and
              (AboveOf17 ?x_11)
              (BelowOf3 ?x_11)
            )
          )
          (exists (?x_29 )
            (and
              (BelowOf5 ?x_29)
              (AboveOf19 ?x_29)
            )
          )
          (exists (?x_27 )
            (and
              (AboveOf9 ?x_27)
              (BelowOf5 ?x_27)
            )
          )
          (exists (?x_21 )
            (and
              (LeftOf1 ?x_21)
              (RightOf14 ?x_21)
            )
          )
          (exists (?x_31 )
            (and
              (LeftOf7 ?x_31)
              (RightOf16 ?x_31)
            )
          )
          (exists (?x_12 )
            (and
              (RightOf18 ?x_12)
              (LeftOf3 ?x_12)
            )
          )
          (exists (?x_5 )
            (and
              (RightOf15 ?x_5)
              (LeftOf12 ?x_5)
            )
          )
          (exists (?x_33 )
            (and
              (BelowOf3 ?x_33)
              (AboveOf17 ?x_33)
            )
          )
          (exists (?x_35 )
            (and
              (BelowOf3 ?x_35)
              (AboveOf15 ?x_35)
            )
          )
          (exists (?x_20 )
            (and
              (RightOf12 ?x_20)
              (LeftOf5 ?x_20)
            )
          )
          (exists (?x_20 )
            (and
              (RightOf16 ?x_20)
              (LeftOf3 ?x_20)
            )
          )
          (exists (?x_0 )
            (and
              (BelowOf3 ?x_0)
              (AboveOf15 ?x_0)
            )
          )
          (exists (?x_37 )
            (and
              (AboveOf6 ?x_37)
              (BelowOf3 ?x_37)
            )
          )
          (exists (?x_37 )
            (and
              (AboveOf5 ?x_37)
              (BelowOf4 ?x_37)
            )
          )
          (exists (?x_12 )
            (and
              (RightOf19 ?x_12)
              (LeftOf6 ?x_12)
            )
          )
          (exists (?x_20 )
            (and
              (LeftOf2 ?x_20)
              (RightOf16 ?x_20)
            )
          )
          (exists (?x_23 )
            (and
              (LeftOf4 ?x_23)
              (RightOf19 ?x_23)
            )
          )
          (exists (?x_9 )
            (and
              (AboveOf17 ?x_9)
              (BelowOf10 ?x_9)
            )
          )
          (exists (?x_14 )
            (and
              (RightOf19 ?x_14)
              (LeftOf10 ?x_14)
            )
          )
          (exists (?x_16 )
            (and
              (RightOf14 ?x_16)
              (LeftOf1 ?x_16)
            )
          )
          (exists (?x_14 )
            (and
              (RightOf16 ?x_14)
              (LeftOf3 ?x_14)
            )
          )
          (exists (?x_20 )
            (and
              (LeftOf11 ?x_20)
              (RightOf12 ?x_20)
            )
          )
          (exists (?x_18 )
            (and
              (LeftOf2 ?x_18)
              (RightOf19 ?x_18)
            )
          )
          (exists (?x_14 )
            (and
              (RightOf16 ?x_14)
              (LeftOf6 ?x_14)
            )
          )
          (exists (?x_22 )
            (and
              (AboveOf17 ?x_22)
              (BelowOf10 ?x_22)
            )
          )
          (exists (?x_33 )
            (and
              (AboveOf12 ?x_33)
              (BelowOf5 ?x_33)
            )
          )
          (exists (?x_2 )
            (and
              (RightOf15 ?x_2)
              (LeftOf4 ?x_2)
            )
          )
          (exists (?x_9 )
            (and
              (BelowOf12 ?x_9)
              (AboveOf18 ?x_9)
            )
          )
          (exists (?x_19 )
            (and
              (AboveOf19 ?x_19)
              (BelowOf17 ?x_19)
            )
          )
          (exists (?x_22 )
            (and
              (AboveOf17 ?x_22)
              (BelowOf13 ?x_22)
            )
          )
          (exists (?x_27 )
            (and
              (BelowOf4 ?x_27)
              (AboveOf14 ?x_27)
            )
          )
          (exists (?x_24 )
            (and
              (AboveOf19 ?x_24)
              (BelowOf11 ?x_24)
            )
          )
          (exists (?x_26 )
            (and
              (LeftOf5 ?x_26)
              (RightOf18 ?x_26)
            )
          )
          (exists (?x_35 )
            (and
              (BelowOf11 ?x_35)
              (AboveOf16 ?x_35)
            )
          )
          (exists (?x_22 )
            (and
              (AboveOf14 ?x_22)
              (BelowOf12 ?x_22)
            )
          )
          (exists (?x_27 )
            (and
              (AboveOf16 ?x_27)
              (BelowOf4 ?x_27)
            )
          )
          (exists (?x_8 )
            (and
              (RightOf6 ?x_8)
              (LeftOf1 ?x_8)
            )
          )
          (exists (?x_34 )
            (and
              (AboveOf14 ?x_34)
              (BelowOf7 ?x_34)
            )
          )
          (exists (?x_25 )
            (and
              (RightOf17 ?x_25)
              (LeftOf9 ?x_25)
            )
          )
          (exists (?x_4 )
            (and
              (LeftOf3 ?x_4)
              (RightOf19 ?x_4)
            )
          )
          (exists (?x_29 )
            (and
              (AboveOf15 ?x_29)
              (BelowOf1 ?x_29)
            )
          )
          (exists (?x_29 )
            (and
              (BelowOf8 ?x_29)
              (AboveOf11 ?x_29)
            )
          )
          (exists (?x_19 )
            (and
              (AboveOf19 ?x_19)
              (BelowOf7 ?x_19)
            )
          )
          (exists (?x_13 )
            (and
              (RightOf15 ?x_13)
              (LeftOf6 ?x_13)
            )
          )
          (exists (?x_10 )
            (and
              (BelowOf3 ?x_10)
              (AboveOf11 ?x_10)
            )
          )
          (exists (?x_32 )
            (and
              (AboveOf10 ?x_32)
              (BelowOf5 ?x_32)
            )
          )
          (exists (?x_10 )
            (and
              (AboveOf17 ?x_10)
              (BelowOf4 ?x_10)
            )
          )
          (exists (?x_12 )
            (and
              (RightOf9 ?x_12)
              (LeftOf3 ?x_12)
            )
          )
          (exists (?x_5 )
            (and
              (LeftOf11 ?x_5)
              (RightOf17 ?x_5)
            )
          )
          (exists (?x_6 )
            (and
              (RightOf19 ?x_6)
              (LeftOf10 ?x_6)
            )
          )
          (exists (?x_34 )
            (and
              (AboveOf15 ?x_34)
              (BelowOf7 ?x_34)
            )
          )
          (exists (?x_23 )
            (and
              (RightOf15 ?x_23)
              (LeftOf1 ?x_23)
            )
          )
          (exists (?x_15 )
            (and
              (AboveOf6 ?x_15)
              (BelowOf2 ?x_15)
            )
          )
          (exists (?x_2 )
            (and
              (LeftOf6 ?x_2)
              (RightOf13 ?x_2)
            )
          )
          (exists (?x_2 )
            (and
              (LeftOf4 ?x_2)
              (RightOf11 ?x_2)
            )
          )
          (exists (?x_14 )
            (and
              (LeftOf4 ?x_14)
              (RightOf13 ?x_14)
            )
          )
          (exists (?x_7 )
            (and
              (AboveOf11 ?x_7)
              (BelowOf5 ?x_7)
            )
          )
          (exists (?x_9 )
            (and
              (BelowOf17 ?x_9)
              (AboveOf18 ?x_9)
            )
          )
          (exists (?x_21 )
            (and
              (LeftOf1 ?x_21)
              (RightOf12 ?x_21)
            )
          )
          (exists (?x_0 )
            (and
              (AboveOf8 ?x_0)
              (BelowOf1 ?x_0)
            )
          )
          (exists (?x_2 )
            (and
              (LeftOf3 ?x_2)
              (RightOf17 ?x_2)
            )
          )
          (exists (?x_8 )
            (and
              (RightOf11 ?x_8)
              (LeftOf3 ?x_8)
            )
          )
          (exists (?x_20 )
            (and
              (RightOf18 ?x_20)
              (LeftOf11 ?x_20)
            )
          )
          (exists (?x_29 )
            (and
              (AboveOf17 ?x_29)
              (BelowOf5 ?x_29)
            )
          )
          (exists (?x_15 )
            (and
              (AboveOf15 ?x_15)
              (BelowOf2 ?x_15)
            )
          )
          (exists (?x_11 )
            (and
              (BelowOf7 ?x_11)
              (AboveOf15 ?x_11)
            )
          )
          (exists (?x_35 )
            (and
              (AboveOf17 ?x_35)
              (BelowOf11 ?x_35)
            )
          )
          (exists (?x_32 )
            (and
              (BelowOf2 ?x_32)
              (AboveOf16 ?x_32)
            )
          )
          (exists (?x_33 )
            (and
              (AboveOf18 ?x_33)
              (BelowOf2 ?x_33)
            )
          )
          (exists (?x_25 )
            (and
              (LeftOf8 ?x_25)
              (RightOf16 ?x_25)
            )
          )
          (exists (?x_31 )
            (and
              (RightOf9 ?x_31)
              (LeftOf2 ?x_31)
            )
          )
          (exists (?x_23 )
            (and
              (LeftOf1 ?x_23)
              (RightOf9 ?x_23)
            )
          )
          (exists (?x_18 )
            (and
              (RightOf14 ?x_18)
              (LeftOf9 ?x_18)
            )
          )
          (exists (?x_33 )
            (and
              (AboveOf10 ?x_33)
              (BelowOf8 ?x_33)
            )
          )
          (exists (?x_27 )
            (and
              (BelowOf1 ?x_27)
              (AboveOf13 ?x_27)
            )
          )
          (exists (?x_28 )
            (and
              (LeftOf1 ?x_28)
              (RightOf10 ?x_28)
            )
          )
          (exists (?x_14 )
            (and
              (RightOf14 ?x_14)
              (LeftOf9 ?x_14)
            )
          )
          (exists (?x_34 )
            (and
              (BelowOf4 ?x_34)
              (AboveOf15 ?x_34)
            )
          )
          (exists (?x_26 )
            (and
              (RightOf19 ?x_26)
              (LeftOf13 ?x_26)
            )
          )
          (exists (?x_37 )
            (and
              (BelowOf4 ?x_37)
              (AboveOf16 ?x_37)
            )
          )
          (exists (?x_5 )
            (and
              (RightOf18 ?x_5)
              (LeftOf10 ?x_5)
            )
          )
          (exists (?x_25 )
            (and
              (LeftOf10 ?x_25)
              (RightOf15 ?x_25)
            )
          )
          (exists (?x_4 )
            (and
              (RightOf17 ?x_4)
              (LeftOf5 ?x_4)
            )
          )
          (exists (?x_3 )
            (and
              (BelowOf4 ?x_3)
              (AboveOf16 ?x_3)
            )
          )
          (exists (?x_12 )
            (and
              (LeftOf5 ?x_12)
              (RightOf14 ?x_12)
            )
          )
          (exists (?x_12 )
            (and
              (LeftOf2 ?x_12)
              (RightOf9 ?x_12)
            )
          )
          (exists (?x_9 )
            (and
              (AboveOf19 ?x_9)
              (BelowOf11 ?x_9)
            )
          )
          (exists (?x_35 )
            (and
              (AboveOf17 ?x_35)
              (BelowOf2 ?x_35)
            )
          )
          (exists (?x_8 )
            (and
              (LeftOf1 ?x_8)
              (RightOf11 ?x_8)
            )
          )
          (exists (?x_5 )
            (and
              (RightOf17 ?x_5)
              (LeftOf10 ?x_5)
            )
          )
          (exists (?x_37 )
            (and
              (BelowOf1 ?x_37)
              (AboveOf9 ?x_37)
            )
          )
          (exists (?x_3 )
            (and
              (AboveOf16 ?x_3)
              (BelowOf1 ?x_3)
            )
          )
          (exists (?x_9 )
            (and
              (AboveOf18 ?x_9)
              (BelowOf2 ?x_9)
            )
          )
          (exists (?x_12 )
            (and
              (RightOf18 ?x_12)
              (LeftOf2 ?x_12)
            )
          )
          (exists (?x_13 )
            (and
              (LeftOf4 ?x_13)
              (RightOf17 ?x_13)
            )
          )
          (exists (?x_35 )
            (and
              (AboveOf17 ?x_35)
              (BelowOf5 ?x_35)
            )
          )
          (exists (?x_3 )
            (and
              (BelowOf6 ?x_3)
              (AboveOf17 ?x_3)
            )
          )
          (exists (?x_20 )
            (and
              (RightOf19 ?x_20)
              (LeftOf5 ?x_20)
            )
          )
          (exists (?x_20 )
            (and
              (RightOf13 ?x_20)
              (LeftOf7 ?x_20)
            )
          )
          (exists (?x_22 )
            (and
              (BelowOf8 ?x_22)
              (AboveOf13 ?x_22)
            )
          )
          (exists (?x_31 )
            (and
              (LeftOf5 ?x_31)
              (RightOf9 ?x_31)
            )
          )
          (exists (?x_37 )
            (and
              (BelowOf3 ?x_37)
              (AboveOf10 ?x_37)
            )
          )
          (exists (?x_5 )
            (and
              (RightOf15 ?x_5)
              (LeftOf9 ?x_5)
            )
          )
          (exists (?x_16 )
            (and
              (LeftOf1 ?x_16)
              (RightOf5 ?x_16)
            )
          )
          (exists (?x_34 )
            (and
              (AboveOf17 ?x_34)
              (BelowOf7 ?x_34)
            )
          )
          (exists (?x_25 )
            (and
              (RightOf15 ?x_25)
              (LeftOf14 ?x_25)
            )
          )
          (exists (?x_21 )
            (and
              (LeftOf3 ?x_21)
              (RightOf12 ?x_21)
            )
          )
          (exists (?x_30 )
            (and
              (RightOf19 ?x_30)
              (LeftOf12 ?x_30)
            )
          )
          (exists (?x_4 )
            (and
              (RightOf17 ?x_4)
              (LeftOf1 ?x_4)
            )
          )
          (exists (?x_28 )
            (and
              (LeftOf1 ?x_28)
              (RightOf18 ?x_28)
            )
          )
          (exists (?x_12 )
            (and
              (RightOf8 ?x_12)
              (LeftOf6 ?x_12)
            )
          )
          (exists (?x_18 )
            (and
              (RightOf18 ?x_18)
              (LeftOf5 ?x_18)
            )
          )
          (exists (?x_27 )
            (and
              (BelowOf4 ?x_27)
              (AboveOf19 ?x_27)
            )
          )
          (exists (?x_21 )
            (and
              (LeftOf3 ?x_21)
              (RightOf19 ?x_21)
            )
          )
          (exists (?x_21 )
            (and
              (LeftOf1 ?x_21)
              (RightOf10 ?x_21)
            )
          )
          (exists (?x_34 )
            (and
              (AboveOf17 ?x_34)
              (BelowOf12 ?x_34)
            )
          )
          (exists (?x_12 )
            (and
              (LeftOf4 ?x_12)
              (RightOf19 ?x_12)
            )
          )
          (exists (?x_7 )
            (and
              (BelowOf2 ?x_7)
              (AboveOf13 ?x_7)
            )
          )
          (exists (?x_5 )
            (and
              (RightOf16 ?x_5)
              (LeftOf12 ?x_5)
            )
          )
          (exists (?x_5 )
            (and
              (LeftOf11 ?x_5)
              (RightOf15 ?x_5)
            )
          )
          (exists (?x_10 )
            (and
              (AboveOf11 ?x_10)
              (BelowOf4 ?x_10)
            )
          )
          (exists (?x_31 )
            (and
              (LeftOf6 ?x_31)
              (RightOf12 ?x_31)
            )
          )
          (exists (?x_25 )
            (and
              (RightOf17 ?x_25)
              (LeftOf15 ?x_25)
            )
          )
          (exists (?x_5 )
            (and
              (LeftOf4 ?x_5)
              (RightOf17 ?x_5)
            )
          )
          (exists (?x_14 )
            (and
              (RightOf19 ?x_14)
              (LeftOf12 ?x_14)
            )
          )
          (exists (?x_20 )
            (and
              (LeftOf11 ?x_20)
              (RightOf11 ?x_20)
            )
          )
          (exists (?x_32 )
            (and
              (AboveOf17 ?x_32)
              (BelowOf5 ?x_32)
            )
          )
          (exists (?x_25 )
            (and
              (LeftOf6 ?x_25)
              (RightOf16 ?x_25)
            )
          )
          (exists (?x_30 )
            (and
              (LeftOf3 ?x_30)
              (RightOf19 ?x_30)
            )
          )
          (exists (?x_0 )
            (and
              (AboveOf19 ?x_0)
              (BelowOf2 ?x_0)
            )
          )
          (exists (?x_20 )
            (and
              (RightOf13 ?x_20)
              (LeftOf3 ?x_20)
            )
          )
          (exists (?x_13 )
            (and
              (RightOf19 ?x_13)
              (LeftOf3 ?x_13)
            )
          )
          (exists (?x_33 )
            (and
              (AboveOf18 ?x_33)
              (BelowOf5 ?x_33)
            )
          )
          (exists (?x_32 )
            (and
              (BelowOf7 ?x_32)
              (AboveOf18 ?x_32)
            )
          )
          (exists (?x_18 )
            (and
              (RightOf18 ?x_18)
              (LeftOf10 ?x_18)
            )
          )
          (exists (?x_14 )
            (and
              (LeftOf3 ?x_14)
              (RightOf15 ?x_14)
            )
          )
          (exists (?x_20 )
            (and
              (RightOf13 ?x_20)
              (LeftOf5 ?x_20)
            )
          )
          (exists (?x_7 )
            (and
              (AboveOf18 ?x_7)
              (BelowOf1 ?x_7)
            )
          )
          (exists (?x_6 )
            (and
              (RightOf18 ?x_6)
              (LeftOf9 ?x_6)
            )
          )
          (exists (?x_26 )
            (and
              (LeftOf8 ?x_26)
              (RightOf18 ?x_26)
            )
          )
          (exists (?x_27 )
            (and
              (AboveOf10 ?x_27)
              (BelowOf4 ?x_27)
            )
          )
          (exists (?x_11 )
            (and
              (AboveOf17 ?x_11)
              (BelowOf5 ?x_11)
            )
          )
          (exists (?x_34 )
            (and
              (AboveOf17 ?x_34)
              (BelowOf13 ?x_34)
            )
          )
          (exists (?x_15 )
            (and
              (BelowOf2 ?x_15)
              (AboveOf5 ?x_15)
            )
          )
          (exists (?x_14 )
            (and
              (RightOf18 ?x_14)
              (LeftOf10 ?x_14)
            )
          )
          (exists (?x_18 )
            (and
              (RightOf15 ?x_18)
              (LeftOf1 ?x_18)
            )
          )
          (exists (?x_23 )
            (and
              (RightOf11 ?x_23)
              (LeftOf6 ?x_23)
            )
          )
          (exists (?x_16 )
            (and
              (RightOf7 ?x_16)
              (LeftOf4 ?x_16)
            )
          )
          (exists (?x_20 )
            (and
              (LeftOf3 ?x_20)
              (RightOf17 ?x_20)
            )
          )
          (exists (?x_30 )
            (and
              (RightOf19 ?x_30)
              (LeftOf1 ?x_30)
            )
          )
          (exists (?x_31 )
            (and
              (RightOf14 ?x_31)
              (LeftOf2 ?x_31)
            )
          )
          (exists (?x_23 )
            (and
              (LeftOf1 ?x_23)
              (RightOf17 ?x_23)
            )
          )
          (exists (?x_27 )
            (and
              (AboveOf8 ?x_27)
              (BelowOf5 ?x_27)
            )
          )
          (exists (?x_32 )
            (and
              (BelowOf1 ?x_32)
              (AboveOf11 ?x_32)
            )
          )
          (exists (?x_6 )
            (and
              (LeftOf15 ?x_6)
              (RightOf16 ?x_6)
            )
          )
          (exists (?x_12 )
            (and
              (RightOf15 ?x_12)
              (LeftOf3 ?x_12)
            )
          )
          (exists (?x_36 )
            (and
              (AboveOf19 ?x_36)
              (BelowOf10 ?x_36)
            )
          )
          (exists (?x_6 )
            (and
              (LeftOf16 ?x_6)
              (RightOf19 ?x_6)
            )
          )
          (exists (?x_12 )
            (and
              (RightOf10 ?x_12)
              (LeftOf3 ?x_12)
            )
          )
          (exists (?x_11 )
            (and
              (BelowOf4 ?x_11)
              (AboveOf18 ?x_11)
            )
          )
          (exists (?x_16 )
            (and
              (RightOf8 ?x_16)
              (LeftOf1 ?x_16)
            )
          )
          (exists (?x_35 )
            (and
              (AboveOf13 ?x_35)
              (BelowOf1 ?x_35)
            )
          )
          (exists (?x_2 )
            (and
              (RightOf16 ?x_2)
              (LeftOf3 ?x_2)
            )
          )
          (exists (?x_25 )
            (and
              (RightOf17 ?x_25)
              (LeftOf14 ?x_25)
            )
          )
          (exists (?x_13 )
            (and
              (LeftOf2 ?x_13)
              (RightOf14 ?x_13)
            )
          )
          (exists (?x_31 )
            (and
              (LeftOf8 ?x_31)
              (RightOf15 ?x_31)
            )
          )
          (exists (?x_20 )
            (and
              (LeftOf3 ?x_20)
              (RightOf14 ?x_20)
            )
          )
          (exists (?x_18 )
            (and
              (RightOf14 ?x_18)
              (LeftOf6 ?x_18)
            )
          )
          (exists (?x_25 )
            (and
              (RightOf17 ?x_25)
              (LeftOf3 ?x_25)
            )
          )
          (exists (?x_7 )
            (and
              (BelowOf1 ?x_7)
              (AboveOf9 ?x_7)
            )
          )
          (exists (?x_22 )
            (and
              (BelowOf11 ?x_22)
              (AboveOf17 ?x_22)
            )
          )
          (exists (?x_19 )
            (and
              (AboveOf18 ?x_19)
              (BelowOf6 ?x_19)
            )
          )
          (exists (?x_35 )
            (and
              (BelowOf12 ?x_35)
              (AboveOf13 ?x_35)
            )
          )
          (exists (?x_11 )
            (and
              (AboveOf19 ?x_11)
              (BelowOf3 ?x_11)
            )
          )
          (exists (?x_11 )
            (and
              (AboveOf14 ?x_11)
              (BelowOf7 ?x_11)
            )
          )
          (exists (?x_3 )
            (and
              (BelowOf4 ?x_3)
              (AboveOf18 ?x_3)
            )
          )
          (exists (?x_24 )
            (and
              (AboveOf19 ?x_24)
              (BelowOf14 ?x_24)
            )
          )
          (exists (?x_22 )
            (and
              (AboveOf19 ?x_22)
              (BelowOf11 ?x_22)
            )
          )
          (exists (?x_31 )
            (and
              (RightOf12 ?x_31)
              (LeftOf8 ?x_31)
            )
          )
          (exists (?x_20 )
            (and
              (RightOf13 ?x_20)
              (LeftOf6 ?x_20)
            )
          )
          (exists (?x_12 )
            (and
              (RightOf7 ?x_12)
              (LeftOf3 ?x_12)
            )
          )
          (exists (?x_13 )
            (and
              (RightOf17 ?x_13)
              (LeftOf3 ?x_13)
            )
          )
          (exists (?x_32 )
            (and
              (BelowOf2 ?x_32)
              (AboveOf17 ?x_32)
            )
          )
          (exists (?x_20 )
            (and
              (LeftOf4 ?x_20)
              (RightOf12 ?x_20)
            )
          )
          (exists (?x_24 )
            (and
              (BelowOf1 ?x_24)
              (AboveOf19 ?x_24)
            )
          )
          (exists (?x_5 )
            (and
              (RightOf15 ?x_5)
              (LeftOf3 ?x_5)
            )
          )
          (exists (?x_35 )
            (and
              (AboveOf15 ?x_35)
              (BelowOf7 ?x_35)
            )
          )
          (exists (?x_35 )
            (and
              (AboveOf17 ?x_35)
              (BelowOf4 ?x_35)
            )
          )
          (exists (?x_6 )
            (and
              (LeftOf8 ?x_6)
              (RightOf17 ?x_6)
            )
          )
          (exists (?x_35 )
            (and
              (BelowOf6 ?x_35)
              (AboveOf18 ?x_35)
            )
          )
          (exists (?x_14 )
            (and
              (RightOf15 ?x_14)
              (LeftOf12 ?x_14)
            )
          )
          (exists (?x_31 )
            (and
              (RightOf14 ?x_31)
              (LeftOf1 ?x_31)
            )
          )
          (exists (?x_31 )
            (and
              (RightOf11 ?x_31)
              (LeftOf2 ?x_31)
            )
          )
          (exists (?x_29 )
            (and
              (BelowOf4 ?x_29)
              (AboveOf18 ?x_29)
            )
          )
          (exists (?x_8 )
            (and
              (RightOf9 ?x_8)
              (LeftOf1 ?x_8)
            )
          )
          (exists (?x_33 )
            (and
              (BelowOf7 ?x_33)
              (AboveOf19 ?x_33)
            )
          )
          (exists (?x_9 )
            (and
              (AboveOf17 ?x_9)
              (BelowOf17 ?x_9)
            )
          )
          (exists (?x_21 )
            (and
              (LeftOf1 ?x_21)
              (RightOf15 ?x_21)
            )
          )
          (exists (?x_18 )
            (and
              (RightOf18 ?x_18)
              (LeftOf4 ?x_18)
            )
          )
          (exists (?x_7 )
            (and
              (AboveOf18 ?x_7)
              (BelowOf6 ?x_7)
            )
          )
          (exists (?x_23 )
            (and
              (LeftOf4 ?x_23)
              (RightOf10 ?x_23)
            )
          )
          (exists (?x_30 )
            (and
              (LeftOf5 ?x_30)
              (RightOf19 ?x_30)
            )
          )
          (exists (?x_4 )
            (and
              (RightOf17 ?x_4)
              (LeftOf9 ?x_4)
            )
          )
          (exists (?x_18 )
            (and
              (RightOf14 ?x_18)
              (LeftOf11 ?x_18)
            )
          )
          (exists (?x_4 )
            (and
              (RightOf18 ?x_4)
              (LeftOf9 ?x_4)
            )
          )
          (exists (?x_8 )
            (and
              (LeftOf2 ?x_8)
              (RightOf17 ?x_8)
            )
          )
          (exists (?x_23 )
            (and
              (LeftOf1 ?x_23)
              (RightOf10 ?x_23)
            )
          )
          (exists (?x_3 )
            (and
              (AboveOf19 ?x_3)
              (BelowOf2 ?x_3)
            )
          )
          (exists (?x_22 )
            (and
              (AboveOf16 ?x_22)
              (BelowOf4 ?x_22)
            )
          )
          (exists (?x_3 )
            (and
              (BelowOf12 ?x_3)
              (AboveOf19 ?x_3)
            )
          )
          (exists (?x_22 )
            (and
              (AboveOf19 ?x_22)
              (BelowOf6 ?x_22)
            )
          )
          (exists (?x_23 )
            (and
              (LeftOf7 ?x_23)
              (RightOf16 ?x_23)
            )
          )
          (exists (?x_5 )
            (and
              (LeftOf12 ?x_5)
              (RightOf17 ?x_5)
            )
          )
          (exists (?x_13 )
            (and
              (LeftOf2 ?x_13)
              (RightOf11 ?x_13)
            )
          )
          (exists (?x_8 )
            (and
              (RightOf3 ?x_8)
              (LeftOf1 ?x_8)
            )
          )
          (exists (?x_18 )
            (and
              (LeftOf7 ?x_18)
              (RightOf14 ?x_18)
            )
          )
          (exists (?x_29 )
            (and
              (AboveOf15 ?x_29)
              (BelowOf2 ?x_29)
            )
          )
          (exists (?x_29 )
            (and
              (AboveOf17 ?x_29)
              (BelowOf4 ?x_29)
            )
          )
          (exists (?x_22 )
            (and
              (BelowOf3 ?x_22)
              (AboveOf17 ?x_22)
            )
          )
          (exists (?x_7 )
            (and
              (BelowOf6 ?x_7)
              (AboveOf14 ?x_7)
            )
          )
          (exists (?x_12 )
            (and
              (RightOf10 ?x_12)
              (LeftOf4 ?x_12)
            )
          )
          (exists (?x_8 )
            (and
              (LeftOf3 ?x_8)
              (RightOf8 ?x_8)
            )
          )
          (exists (?x_22 )
            (and
              (AboveOf15 ?x_22)
              (BelowOf10 ?x_22)
            )
          )
          (exists (?x_27 )
            (and
              (AboveOf15 ?x_27)
              (BelowOf6 ?x_27)
            )
          )
          (exists (?x_36 )
            (and
              (AboveOf19 ?x_36)
              (BelowOf5 ?x_36)
            )
          )
          (exists (?x_17 )
            (and
              (RightOf18 ?x_17)
              (LeftOf1 ?x_17)
            )
          )
          (exists (?x_15 )
            (and
              (AboveOf3 ?x_15)
              (BelowOf1 ?x_15)
            )
          )
          (exists (?x_14 )
            (and
              (LeftOf10 ?x_14)
              (RightOf17 ?x_14)
            )
          )
          (exists (?x_7 )
            (and
              (BelowOf2 ?x_7)
              (AboveOf14 ?x_7)
            )
          )
          (exists (?x_23 )
            (and
              (RightOf14 ?x_23)
              (LeftOf3 ?x_23)
            )
          )
          (exists (?x_31 )
            (and
              (RightOf14 ?x_31)
              (LeftOf4 ?x_31)
            )
          )
          (exists (?x_12 )
            (and
              (LeftOf4 ?x_12)
              (RightOf10 ?x_12)
            )
          )
          (exists (?x_22 )
            (and
              (BelowOf5 ?x_22)
              (AboveOf13 ?x_22)
            )
          )
          (exists (?x_3 )
            (and
              (AboveOf17 ?x_3)
              (BelowOf9 ?x_3)
            )
          )
          (exists (?x_15 )
            (and
              (BelowOf2 ?x_15)
              (AboveOf8 ?x_15)
            )
          )
          (exists (?x_23 )
            (and
              (LeftOf4 ?x_23)
              (RightOf13 ?x_23)
            )
          )
          (exists (?x_27 )
            (and
              (BelowOf1 ?x_27)
              (AboveOf6 ?x_27)
            )
          )
          (exists (?x_10 )
            (and
              (BelowOf3 ?x_10)
              (AboveOf16 ?x_10)
            )
          )
          (exists (?x_13 )
            (and
              (RightOf16 ?x_13)
              (LeftOf5 ?x_13)
            )
          )
          (exists (?x_27 )
            (and
              (AboveOf15 ?x_27)
              (BelowOf5 ?x_27)
            )
          )
          (exists (?x_14 )
            (and
              (LeftOf1 ?x_14)
              (RightOf17 ?x_14)
            )
          )
          (exists (?x_26 )
            (and
              (RightOf18 ?x_26)
              (LeftOf17 ?x_26)
            )
          )
          (exists (?x_19 )
            (and
              (BelowOf7 ?x_19)
              (AboveOf19 ?x_19)
            )
          )
          (exists (?x_27 )
            (and
              (BelowOf6 ?x_27)
              (AboveOf8 ?x_27)
            )
          )
          (exists (?x_23 )
            (and
              (RightOf8 ?x_23)
              (LeftOf6 ?x_23)
            )
          )
          (exists (?x_37 )
            (and
              (AboveOf11 ?x_37)
              (BelowOf2 ?x_37)
            )
          )
          (exists (?x_2 )
            (and
              (LeftOf1 ?x_2)
              (RightOf12 ?x_2)
            )
          )
          (exists (?x_19 )
            (and
              (BelowOf7 ?x_19)
              (AboveOf18 ?x_19)
            )
          )
          (exists (?x_32 )
            (and
              (BelowOf10 ?x_32)
              (AboveOf18 ?x_32)
            )
          )
          (exists (?x_32 )
            (and
              (AboveOf10 ?x_32)
              (BelowOf4 ?x_32)
            )
          )
          (exists (?x_20 )
            (and
              (LeftOf8 ?x_20)
              (RightOf19 ?x_20)
            )
          )
          (exists (?x_22 )
            (and
              (BelowOf5 ?x_22)
              (AboveOf15 ?x_22)
            )
          )
          (exists (?x_33 )
            (and
              (AboveOf16 ?x_33)
              (BelowOf9 ?x_33)
            )
          )
          (exists (?x_5 )
            (and
              (LeftOf11 ?x_5)
              (RightOf16 ?x_5)
            )
          )
          (exists (?x_3 )
            (and
              (AboveOf17 ?x_3)
              (BelowOf16 ?x_3)
            )
          )
          (exists (?x_18 )
            (and
              (RightOf18 ?x_18)
              (LeftOf6 ?x_18)
            )
          )
          (exists (?x_3 )
            (and
              (BelowOf11 ?x_3)
              (AboveOf18 ?x_3)
            )
          )
          (exists (?x_0 )
            (and
              (BelowOf3 ?x_0)
              (AboveOf5 ?x_0)
            )
          )
          (exists (?x_36 )
            (and
              (AboveOf19 ?x_36)
              (BelowOf8 ?x_36)
            )
          )
          (exists (?x_34 )
            (and
              (BelowOf5 ?x_34)
              (AboveOf19 ?x_34)
            )
          )
          (exists (?x_7 )
            (and
              (AboveOf7 ?x_7)
              (BelowOf4 ?x_7)
            )
          )
          (exists (?x_21 )
            (and
              (LeftOf1 ?x_21)
              (RightOf11 ?x_21)
            )
          )
          (exists (?x_18 )
            (and
              (RightOf15 ?x_18)
              (LeftOf13 ?x_18)
            )
          )
          (exists (?x_37 )
            (and
              (AboveOf14 ?x_37)
              (BelowOf5 ?x_37)
            )
          )
          (exists (?x_13 )
            (and
              (LeftOf10 ?x_13)
              (RightOf12 ?x_13)
            )
          )
          (exists (?x_29 )
            (and
              (AboveOf19 ?x_29)
              (BelowOf3 ?x_29)
            )
          )
          (exists (?x_35 )
            (and
              (AboveOf14 ?x_35)
              (BelowOf10 ?x_35)
            )
          )
          (exists (?x_17 )
            (and
              (RightOf10 ?x_17)
              (LeftOf1 ?x_17)
            )
          )
          (exists (?x_21 )
            (and
              (RightOf8 ?x_21)
              (LeftOf3 ?x_21)
            )
          )
          (exists (?x_12 )
            (and
              (RightOf16 ?x_12)
              (LeftOf3 ?x_12)
            )
          )
          (exists (?x_3 )
            (and
              (AboveOf17 ?x_3)
              (BelowOf5 ?x_3)
            )
          )
          (exists (?x_32 )
            (and
              (AboveOf10 ?x_32)
              (BelowOf2 ?x_32)
            )
          )
          (exists (?x_32 )
            (and
              (BelowOf6 ?x_32)
              (AboveOf17 ?x_32)
            )
          )
          (exists (?x_19 )
            (and
              (BelowOf8 ?x_19)
              (AboveOf18 ?x_19)
            )
          )
          (exists (?x_12 )
            (and
              (RightOf8 ?x_12)
              (LeftOf7 ?x_12)
            )
          )
          (exists (?x_2 )
            (and
              (LeftOf1 ?x_2)
              (RightOf19 ?x_2)
            )
          )
          (exists (?x_31 )
            (and
              (RightOf17 ?x_31)
              (LeftOf5 ?x_31)
            )
          )
          (exists (?x_5 )
            (and
              (RightOf13 ?x_5)
              (LeftOf3 ?x_5)
            )
          )
          (exists (?x_20 )
            (and
              (LeftOf2 ?x_20)
              (RightOf18 ?x_20)
            )
          )
          (exists (?x_34 )
            (and
              (BelowOf12 ?x_34)
              (AboveOf14 ?x_34)
            )
          )
          (exists (?x_30 )
            (and
              (LeftOf8 ?x_30)
              (RightOf19 ?x_30)
            )
          )
          (exists (?x_6 )
            (and
              (RightOf19 ?x_6)
              (LeftOf11 ?x_6)
            )
          )
          (exists (?x_11 )
            (and
              (BelowOf3 ?x_11)
              (AboveOf13 ?x_11)
            )
          )
          (exists (?x_30 )
            (and
              (LeftOf15 ?x_30)
              (RightOf19 ?x_30)
            )
          )
          (exists (?x_21 )
            (and
              (LeftOf3 ?x_21)
              (RightOf7 ?x_21)
            )
          )
          (exists (?x_3 )
            (and
              (AboveOf18 ?x_3)
              (BelowOf1 ?x_3)
            )
          )
          (exists (?x_21 )
            (and
              (RightOf16 ?x_21)
              (LeftOf3 ?x_21)
            )
          )
          (exists (?x_7 )
            (and
              (AboveOf7 ?x_7)
              (BelowOf5 ?x_7)
            )
          )
          (exists (?x_11 )
            (and
              (AboveOf17 ?x_11)
              (BelowOf8 ?x_11)
            )
          )
          (exists (?x_33 )
            (and
              (AboveOf16 ?x_33)
              (BelowOf5 ?x_33)
            )
          )
          (exists (?x_7 )
            (and
              (AboveOf11 ?x_7)
              (BelowOf4 ?x_7)
            )
          )
          (exists (?x_23 )
            (and
              (RightOf19 ?x_23)
              (LeftOf6 ?x_23)
            )
          )
          (exists (?x_34 )
            (and
              (BelowOf5 ?x_34)
              (AboveOf15 ?x_34)
            )
          )
          (exists (?x_3 )
            (and
              (BelowOf5 ?x_3)
              (AboveOf16 ?x_3)
            )
          )
          (exists (?x_33 )
            (and
              (BelowOf2 ?x_33)
              (AboveOf16 ?x_33)
            )
          )
          (exists (?x_21 )
            (and
              (RightOf10 ?x_21)
              (LeftOf5 ?x_21)
            )
          )
          (exists (?x_29 )
            (and
              (AboveOf16 ?x_29)
              (BelowOf3 ?x_29)
            )
          )
          (exists (?x_29 )
            (and
              (BelowOf1 ?x_29)
              (AboveOf8 ?x_29)
            )
          )
          (exists (?x_29 )
            (and
              (BelowOf2 ?x_29)
              (AboveOf18 ?x_29)
            )
          )
          (exists (?x_5 )
            (and
              (RightOf15 ?x_5)
              (LeftOf8 ?x_5)
            )
          )
          (exists (?x_8 )
            (and
              (RightOf15 ?x_8)
              (LeftOf3 ?x_8)
            )
          )
          (exists (?x_4 )
            (and
              (LeftOf10 ?x_4)
              (RightOf17 ?x_4)
            )
          )
          (exists (?x_28 )
            (and
              (RightOf13 ?x_28)
              (LeftOf1 ?x_28)
            )
          )
          (exists (?x_3 )
            (and
              (BelowOf3 ?x_3)
              (AboveOf19 ?x_3)
            )
          )
          (exists (?x_6 )
            (and
              (LeftOf2 ?x_6)
              (RightOf19 ?x_6)
            )
          )
          (exists (?x_16 )
            (and
              (RightOf16 ?x_16)
              (LeftOf1 ?x_16)
            )
          )
          (exists (?x_2 )
            (and
              (RightOf7 ?x_2)
              (LeftOf5 ?x_2)
            )
          )
          (exists (?x_16 )
            (and
              (LeftOf1 ?x_16)
              (RightOf11 ?x_16)
            )
          )
          (exists (?x_29 )
            (and
              (AboveOf13 ?x_29)
              (BelowOf3 ?x_29)
            )
          )
          (exists (?x_34 )
            (and
              (AboveOf14 ?x_34)
              (BelowOf1 ?x_34)
            )
          )
          (exists (?x_24 )
            (and
              (AboveOf18 ?x_24)
              (BelowOf11 ?x_24)
            )
          )
          (exists (?x_2 )
            (and
              (LeftOf1 ?x_2)
              (RightOf18 ?x_2)
            )
          )
          (exists (?x_31 )
            (and
              (LeftOf2 ?x_31)
              (RightOf10 ?x_31)
            )
          )
          (exists (?x_32 )
            (and
              (BelowOf10 ?x_32)
              (AboveOf14 ?x_32)
            )
          )
          (exists (?x_20 )
            (and
              (LeftOf1 ?x_20)
              (RightOf15 ?x_20)
            )
          )
          (exists (?x_13 )
            (and
              (LeftOf1 ?x_13)
              (RightOf15 ?x_13)
            )
          )
          (exists (?x_12 )
            (and
              (RightOf8 ?x_12)
              (LeftOf2 ?x_12)
            )
          )
          (exists (?x_10 )
            (and
              (BelowOf1 ?x_10)
              (AboveOf15 ?x_10)
            )
          )
          (exists (?x_35 )
            (and
              (AboveOf12 ?x_35)
              (BelowOf1 ?x_35)
            )
          )
          (exists (?x_4 )
            (and
              (RightOf19 ?x_4)
              (LeftOf15 ?x_4)
            )
          )
          (exists (?x_6 )
            (and
              (RightOf18 ?x_6)
              (LeftOf14 ?x_6)
            )
          )
          (exists (?x_2 )
            (and
              (RightOf17 ?x_2)
              (LeftOf3 ?x_2)
            )
          )
          (exists (?x_12 )
            (and
              (RightOf9 ?x_12)
              (LeftOf6 ?x_12)
            )
          )
          (exists (?x_12 )
            (and
              (LeftOf5 ?x_12)
              (RightOf7 ?x_12)
            )
          )
          (exists (?x_18 )
            (and
              (RightOf19 ?x_18)
              (LeftOf13 ?x_18)
            )
          )
          (exists (?x_4 )
            (and
              (RightOf17 ?x_4)
              (LeftOf6 ?x_4)
            )
          )
          (exists (?x_18 )
            (and
              (RightOf15 ?x_18)
              (LeftOf3 ?x_18)
            )
          )
          (exists (?x_33 )
            (and
              (BelowOf1 ?x_33)
              (AboveOf13 ?x_33)
            )
          )
          (exists (?x_20 )
            (and
              (LeftOf9 ?x_20)
              (RightOf11 ?x_20)
            )
          )
          (exists (?x_6 )
            (and
              (LeftOf3 ?x_6)
              (RightOf16 ?x_6)
            )
          )
          (exists (?x_32 )
            (and
              (AboveOf15 ?x_32)
              (BelowOf8 ?x_32)
            )
          )
          (exists (?x_12 )
            (and
              (RightOf13 ?x_12)
              (LeftOf5 ?x_12)
            )
          )
          (exists (?x_13 )
            (and
              (RightOf16 ?x_13)
              (LeftOf2 ?x_13)
            )
          )
          (exists (?x_22 )
            (and
              (AboveOf15 ?x_22)
              (BelowOf4 ?x_22)
            )
          )
          (exists (?x_34 )
            (and
              (BelowOf14 ?x_34)
              (AboveOf15 ?x_34)
            )
          )
          (exists (?x_7 )
            (and
              (AboveOf16 ?x_7)
              (BelowOf3 ?x_7)
            )
          )
          (exists (?x_32 )
            (and
              (BelowOf4 ?x_32)
              (AboveOf11 ?x_32)
            )
          )
          (exists (?x_3 )
            (and
              (BelowOf8 ?x_3)
              (AboveOf18 ?x_3)
            )
          )
          (exists (?x_5 )
            (and
              (LeftOf8 ?x_5)
              (RightOf18 ?x_5)
            )
          )
          (exists (?x_25 )
            (and
              (RightOf16 ?x_25)
              (LeftOf4 ?x_25)
            )
          )
          (exists (?x_7 )
            (and
              (BelowOf2 ?x_7)
              (AboveOf12 ?x_7)
            )
          )
          (exists (?x_33 )
            (and
              (AboveOf15 ?x_33)
              (BelowOf8 ?x_33)
            )
          )
          (exists (?x_14 )
            (and
              (LeftOf9 ?x_14)
              (RightOf15 ?x_14)
            )
          )
          (exists (?x_25 )
            (and
              (RightOf19 ?x_25)
              (LeftOf7 ?x_25)
            )
          )
          (exists (?x_37 )
            (and
              (AboveOf6 ?x_37)
              (BelowOf1 ?x_37)
            )
          )
          (exists (?x_25 )
            (and
              (LeftOf3 ?x_25)
              (RightOf15 ?x_25)
            )
          )
          (exists (?x_18 )
            (and
              (LeftOf8 ?x_18)
              (RightOf14 ?x_18)
            )
          )
          (exists (?x_20 )
            (and
              (LeftOf5 ?x_20)
              (RightOf19 ?x_20)
            )
          )
          (exists (?x_11 )
            (and
              (AboveOf17 ?x_11)
              (BelowOf2 ?x_11)
            )
          )
          (exists (?x_8 )
            (and
              (RightOf17 ?x_8)
              (LeftOf3 ?x_8)
            )
          )
          (exists (?x_24 )
            (and
              (AboveOf15 ?x_24)
              (BelowOf10 ?x_24)
            )
          )
          (exists (?x_20 )
            (and
              (LeftOf6 ?x_20)
              (RightOf14 ?x_20)
            )
          )
          (exists (?x_12 )
            (and
              (RightOf8 ?x_12)
              (LeftOf3 ?x_12)
            )
          )
          (exists (?x_0 )
            (and
              (BelowOf3 ?x_0)
              (AboveOf6 ?x_0)
            )
          )
          (exists (?x_35 )
            (and
              (BelowOf5 ?x_35)
              (AboveOf18 ?x_35)
            )
          )
          (exists (?x_8 )
            (and
              (LeftOf2 ?x_8)
              (RightOf12 ?x_8)
            )
          )
          (exists (?x_3 )
            (and
              (AboveOf17 ?x_3)
              (BelowOf10 ?x_3)
            )
          )
          (exists (?x_4 )
            (and
              (RightOf17 ?x_4)
              (LeftOf7 ?x_4)
            )
          )
          (exists (?x_14 )
            (and
              (LeftOf3 ?x_14)
              (RightOf17 ?x_14)
            )
          )
          (exists (?x_19 )
            (and
              (AboveOf19 ?x_19)
              (BelowOf5 ?x_19)
            )
          )
          (exists (?x_21 )
            (and
              (LeftOf3 ?x_21)
              (RightOf14 ?x_21)
            )
          )
          (exists (?x_0 )
            (and
              (AboveOf6 ?x_0)
              (BelowOf1 ?x_0)
            )
          )
          (exists (?x_24 )
            (and
              (BelowOf7 ?x_24)
              (AboveOf19 ?x_24)
            )
          )
          (exists (?x_13 )
            (and
              (RightOf13 ?x_13)
              (LeftOf10 ?x_13)
            )
          )
          (exists (?x_16 )
            (and
              (RightOf6 ?x_16)
              (LeftOf4 ?x_16)
            )
          )
          (exists (?x_21 )
            (and
              (LeftOf4 ?x_21)
              (RightOf10 ?x_21)
            )
          )
          (exists (?x_4 )
            (and
              (LeftOf17 ?x_4)
              (RightOf19 ?x_4)
            )
          )
          (exists (?x_6 )
            (and
              (RightOf16 ?x_6)
              (LeftOf11 ?x_6)
            )
          )
          (exists (?x_20 )
            (and
              (LeftOf4 ?x_20)
              (RightOf11 ?x_20)
            )
          )
          (exists (?x_17 )
            (and
              (RightOf16 ?x_17)
              (LeftOf1 ?x_17)
            )
          )
          (exists (?x_29 )
            (and
              (AboveOf14 ?x_29)
              (BelowOf1 ?x_29)
            )
          )
          (exists (?x_4 )
            (and
              (RightOf17 ?x_4)
              (LeftOf15 ?x_4)
            )
          )
          (exists (?x_7 )
            (and
              (AboveOf11 ?x_7)
              (BelowOf6 ?x_7)
            )
          )
          (exists (?x_13 )
            (and
              (LeftOf2 ?x_13)
              (RightOf17 ?x_13)
            )
          )
          (exists (?x_16 )
            (and
              (LeftOf3 ?x_16)
              (RightOf19 ?x_16)
            )
          )
          (exists (?x_14 )
            (and
              (RightOf13 ?x_14)
              (LeftOf3 ?x_14)
            )
          )
          (exists (?x_26 )
            (and
              (LeftOf6 ?x_26)
              (RightOf18 ?x_26)
            )
          )
          (exists (?x_12 )
            (and
              (RightOf15 ?x_12)
              (LeftOf5 ?x_12)
            )
          )
          (exists (?x_4 )
            (and
              (LeftOf10 ?x_4)
              (RightOf19 ?x_4)
            )
          )
          (exists (?x_32 )
            (and
              (BelowOf2 ?x_32)
              (AboveOf19 ?x_32)
            )
          )
          (exists (?x_14 )
            (and
              (LeftOf8 ?x_14)
              (RightOf17 ?x_14)
            )
          )
          (exists (?x_35 )
            (and
              (AboveOf12 ?x_35)
              (BelowOf4 ?x_35)
            )
          )
          (exists (?x_22 )
            (and
              (AboveOf17 ?x_22)
              (BelowOf4 ?x_22)
            )
          )
          (exists (?x_25 )
            (and
              (LeftOf13 ?x_25)
              (RightOf18 ?x_25)
            )
          )
          (exists (?x_1 )
            (and
              (BelowOf1 ?x_1)
              (AboveOf4 ?x_1)
            )
          )
          (exists (?x_23 )
            (and
              (LeftOf7 ?x_23)
              (RightOf17 ?x_23)
            )
          )
          (exists (?x_5 )
            (and
              (RightOf14 ?x_5)
              (LeftOf2 ?x_5)
            )
          )
          (exists (?x_5 )
            (and
              (LeftOf12 ?x_5)
              (RightOf14 ?x_5)
            )
          )
          (exists (?x_33 )
            (and
              (AboveOf18 ?x_33)
              (BelowOf9 ?x_33)
            )
          )
          (exists (?x_29 )
            (and
              (BelowOf2 ?x_29)
              (AboveOf10 ?x_29)
            )
          )
          (exists (?x_3 )
            (and
              (BelowOf14 ?x_3)
              (AboveOf18 ?x_3)
            )
          )
          (exists (?x_6 )
            (and
              (LeftOf16 ?x_6)
              (RightOf18 ?x_6)
            )
          )
          (exists (?x_18 )
            (and
              (LeftOf8 ?x_18)
              (RightOf17 ?x_18)
            )
          )
          (exists (?x_19 )
            (and
              (BelowOf18 ?x_19)
              (AboveOf18 ?x_19)
            )
          )
          (exists (?x_5 )
            (and
              (RightOf13 ?x_5)
              (LeftOf8 ?x_5)
            )
          )
          (exists (?x_22 )
            (and
              (AboveOf13 ?x_22)
              (BelowOf9 ?x_22)
            )
          )
          (exists (?x_0 )
            (and
              (AboveOf16 ?x_0)
              (BelowOf1 ?x_0)
            )
          )
          (exists (?x_7 )
            (and
              (BelowOf7 ?x_7)
              (AboveOf13 ?x_7)
            )
          )
          (exists (?x_5 )
            (and
              (LeftOf9 ?x_5)
              (RightOf17 ?x_5)
            )
          )
          (exists (?x_6 )
            (and
              (RightOf19 ?x_6)
              (LeftOf12 ?x_6)
            )
          )
          (exists (?x_20 )
            (and
              (LeftOf11 ?x_20)
              (RightOf19 ?x_20)
            )
          )
          (exists (?x_11 )
            (and
              (BelowOf6 ?x_11)
              (AboveOf13 ?x_11)
            )
          )
          (exists (?x_11 )
            (and
              (AboveOf12 ?x_11)
              (BelowOf7 ?x_11)
            )
          )
          (exists (?x_8 )
            (and
              (LeftOf2 ?x_8)
              (RightOf7 ?x_8)
            )
          )
          (exists (?x_11 )
            (and
              (AboveOf15 ?x_11)
              (BelowOf10 ?x_11)
            )
          )
          (exists (?x_31 )
            (and
              (LeftOf1 ?x_31)
              (RightOf18 ?x_31)
            )
          )
          (exists (?x_22 )
            (and
              (AboveOf17 ?x_22)
              (BelowOf7 ?x_22)
            )
          )
          (exists (?x_15 )
            (and
              (BelowOf1 ?x_15)
              (AboveOf9 ?x_15)
            )
          )
          (exists (?x_33 )
            (and
              (BelowOf9 ?x_33)
              (AboveOf19 ?x_33)
            )
          )
          (exists (?x_24 )
            (and
              (BelowOf2 ?x_24)
              (AboveOf19 ?x_24)
            )
          )
          (exists (?x_24 )
            (and
              (BelowOf4 ?x_24)
              (AboveOf15 ?x_24)
            )
          )
          (exists (?x_16 )
            (and
              (RightOf17 ?x_16)
              (LeftOf1 ?x_16)
            )
          )
          (exists (?x_18 )
            (and
              (LeftOf8 ?x_18)
              (RightOf15 ?x_18)
            )
          )
          (exists (?x_14 )
            (and
              (LeftOf9 ?x_14)
              (RightOf13 ?x_14)
            )
          )
          (exists (?x_11 )
            (and
              (AboveOf12 ?x_11)
              (BelowOf6 ?x_11)
            )
          )
          (exists (?x_23 )
            (and
              (RightOf9 ?x_23)
              (LeftOf6 ?x_23)
            )
          )
          (exists (?x_31 )
            (and
              (RightOf17 ?x_31)
              (LeftOf1 ?x_31)
            )
          )
          (exists (?x_16 )
            (and
              (LeftOf2 ?x_16)
              (RightOf13 ?x_16)
            )
          )
          (exists (?x_22 )
            (and
              (BelowOf3 ?x_22)
              (AboveOf19 ?x_22)
            )
          )
          (exists (?x_1 )
            (and
              (BelowOf1 ?x_1)
              (AboveOf17 ?x_1)
            )
          )
          (exists (?x_2 )
            (and
              (RightOf9 ?x_2)
              (LeftOf5 ?x_2)
            )
          )
          (exists (?x_14 )
            (and
              (LeftOf9 ?x_14)
              (RightOf19 ?x_14)
            )
          )
          (exists (?x_16 )
            (and
              (LeftOf4 ?x_16)
              (RightOf5 ?x_16)
            )
          )
          (exists (?x_31 )
            (and
              (RightOf12 ?x_31)
              (LeftOf6 ?x_31)
            )
          )
          (exists (?x_21 )
            (and
              (LeftOf1 ?x_21)
              (RightOf8 ?x_21)
            )
          )
          (exists (?x_31 )
            (and
              (LeftOf8 ?x_31)
              (RightOf14 ?x_31)
            )
          )
          (exists (?x_34 )
            (and
              (BelowOf9 ?x_34)
              (AboveOf15 ?x_34)
            )
          )
          (exists (?x_16 )
            (and
              (RightOf9 ?x_16)
              (LeftOf3 ?x_16)
            )
          )
          (exists (?x_18 )
            (and
              (RightOf17 ?x_18)
              (LeftOf14 ?x_18)
            )
          )
          (exists (?x_35 )
            (and
              (BelowOf12 ?x_35)
              (AboveOf16 ?x_35)
            )
          )
          (exists (?x_25 )
            (and
              (RightOf15 ?x_25)
              (LeftOf4 ?x_25)
            )
          )
          (exists (?x_22 )
            (and
              (AboveOf17 ?x_22)
              (BelowOf12 ?x_22)
            )
          )
          (exists (?x_5 )
            (and
              (RightOf15 ?x_5)
              (LeftOf7 ?x_5)
            )
          )
          (exists (?x_10 )
            (and
              (BelowOf1 ?x_10)
              (AboveOf10 ?x_10)
            )
          )
          (exists (?x_31 )
            (and
              (LeftOf6 ?x_31)
              (RightOf16 ?x_31)
            )
          )
          (exists (?x_0 )
            (and
              (AboveOf12 ?x_0)
              (BelowOf1 ?x_0)
            )
          )
          (exists (?x_31 )
            (and
              (LeftOf3 ?x_31)
              (RightOf10 ?x_31)
            )
          )
          (exists (?x_16 )
            (and
              (RightOf16 ?x_16)
              (LeftOf4 ?x_16)
            )
          )
          (exists (?x_6 )
            (and
              (LeftOf5 ?x_6)
              (RightOf18 ?x_6)
            )
          )
          (exists (?x_13 )
            (and
              (LeftOf2 ?x_13)
              (RightOf19 ?x_13)
            )
          )
          (exists (?x_31 )
            (and
              (RightOf17 ?x_31)
              (LeftOf7 ?x_31)
            )
          )
          (exists (?x_33 )
            (and
              (BelowOf1 ?x_33)
              (AboveOf12 ?x_33)
            )
          )
          (exists (?x_13 )
            (and
              (RightOf16 ?x_13)
              (LeftOf8 ?x_13)
            )
          )
          (exists (?x_23 )
            (and
              (RightOf9 ?x_23)
              (LeftOf8 ?x_23)
            )
          )
          (exists (?x_23 )
            (and
              (LeftOf5 ?x_23)
              (RightOf14 ?x_23)
            )
          )
          (exists (?x_10 )
            (and
              (AboveOf12 ?x_10)
              (BelowOf3 ?x_10)
            )
          )
          (exists (?x_21 )
            (and
              (LeftOf3 ?x_21)
              (RightOf17 ?x_21)
            )
          )
          (exists (?x_23 )
            (and
              (LeftOf4 ?x_23)
              (RightOf9 ?x_23)
            )
          )
          (exists (?x_33 )
            (and
              (BelowOf5 ?x_33)
              (AboveOf17 ?x_33)
            )
          )
          (exists (?x_14 )
            (and
              (LeftOf13 ?x_14)
              (RightOf17 ?x_14)
            )
          )
          (exists (?x_13 )
            (and
              (RightOf13 ?x_13)
              (LeftOf8 ?x_13)
            )
          )
          (exists (?x_12 )
            (and
              (RightOf12 ?x_12)
              (LeftOf6 ?x_12)
            )
          )
          (exists (?x_35 )
            (and
              (BelowOf4 ?x_35)
              (AboveOf15 ?x_35)
            )
          )
          (exists (?x_20 )
            (and
              (RightOf18 ?x_20)
              (LeftOf1 ?x_20)
            )
          )
          (exists (?x_3 )
            (and
              (BelowOf3 ?x_3)
              (AboveOf18 ?x_3)
            )
          )
          (exists (?x_23 )
            (and
              (RightOf9 ?x_23)
              (LeftOf3 ?x_23)
            )
          )
          (exists (?x_29 )
            (and
              (AboveOf9 ?x_29)
              (BelowOf3 ?x_29)
            )
          )
          (exists (?x_27 )
            (and
              (BelowOf4 ?x_27)
              (AboveOf9 ?x_27)
            )
          )
          (exists (?x_20 )
            (and
              (LeftOf10 ?x_20)
              (RightOf14 ?x_20)
            )
          )
          (exists (?x_28 )
            (and
              (RightOf14 ?x_28)
              (LeftOf1 ?x_28)
            )
          )
          (exists (?x_37 )
            (and
              (AboveOf8 ?x_37)
              (BelowOf1 ?x_37)
            )
          )
          (exists (?x_24 )
            (and
              (BelowOf15 ?x_24)
              (AboveOf18 ?x_24)
            )
          )
          (exists (?x_36 )
            (and
              (AboveOf19 ?x_36)
              (BelowOf1 ?x_36)
            )
          )
          (exists (?x_9 )
            (and
              (BelowOf15 ?x_9)
              (AboveOf18 ?x_9)
            )
          )
          (exists (?x_35 )
            (and
              (AboveOf13 ?x_35)
              (BelowOf7 ?x_35)
            )
          )
          (exists (?x_23 )
            (and
              (LeftOf4 ?x_23)
              (RightOf17 ?x_23)
            )
          )
          (exists (?x_7 )
            (and
              (AboveOf16 ?x_7)
              (BelowOf5 ?x_7)
            )
          )
          (exists (?x_9 )
            (and
              (BelowOf3 ?x_9)
              (AboveOf18 ?x_9)
            )
          )
          (exists (?x_32 )
            (and
              (AboveOf15 ?x_32)
              (BelowOf1 ?x_32)
            )
          )
          (exists (?x_21 )
            (and
              (LeftOf5 ?x_21)
              (RightOf14 ?x_21)
            )
          )
          (exists (?x_22 )
            (and
              (BelowOf11 ?x_22)
              (AboveOf18 ?x_22)
            )
          )
          (exists (?x_18 )
            (and
              (LeftOf2 ?x_18)
              (RightOf17 ?x_18)
            )
          )
          (exists (?x_13 )
            (and
              (RightOf18 ?x_13)
              (LeftOf10 ?x_13)
            )
          )
          (exists (?x_27 )
            (and
              (BelowOf3 ?x_27)
              (AboveOf19 ?x_27)
            )
          )
          (exists (?x_14 )
            (and
              (LeftOf9 ?x_14)
              (RightOf16 ?x_14)
            )
          )
          (exists (?x_31 )
            (and
              (RightOf16 ?x_31)
              (LeftOf9 ?x_31)
            )
          )
          (exists (?x_11 )
            (and
              (BelowOf8 ?x_11)
              (AboveOf11 ?x_11)
            )
          )
          (exists (?x_24 )
            (and
              (BelowOf2 ?x_24)
              (AboveOf18 ?x_24)
            )
          )
          (exists (?x_15 )
            (and
              (AboveOf18 ?x_15)
              (BelowOf1 ?x_15)
            )
          )
          (exists (?x_37 )
            (and
              (AboveOf15 ?x_37)
              (BelowOf1 ?x_37)
            )
          )
          (exists (?x_27 )
            (and
              (AboveOf14 ?x_27)
              (BelowOf3 ?x_27)
            )
          )
          (exists (?x_28 )
            (and
              (RightOf5 ?x_28)
              (LeftOf1 ?x_28)
            )
          )
          (exists (?x_14 )
            (and
              (RightOf19 ?x_14)
              (LeftOf9 ?x_14)
            )
          )
          (exists (?x_23 )
            (and
              (RightOf18 ?x_23)
              (LeftOf8 ?x_23)
            )
          )
          (exists (?x_7 )
            (and
              (BelowOf7 ?x_7)
              (AboveOf9 ?x_7)
            )
          )
          (exists (?x_15 )
            (and
              (BelowOf2 ?x_15)
              (AboveOf11 ?x_15)
            )
          )
          (exists (?x_23 )
            (and
              (RightOf16 ?x_23)
              (LeftOf5 ?x_23)
            )
          )
          (exists (?x_13 )
            (and
              (LeftOf2 ?x_13)
              (RightOf12 ?x_13)
            )
          )
          (exists (?x_7 )
            (and
              (BelowOf3 ?x_7)
              (AboveOf17 ?x_7)
            )
          )
          (exists (?x_15 )
            (and
              (BelowOf1 ?x_15)
              (AboveOf2 ?x_15)
            )
          )
          (exists (?x_25 )
            (and
              (LeftOf6 ?x_25)
              (RightOf18 ?x_25)
            )
          )
          (exists (?x_11 )
            (and
              (AboveOf19 ?x_11)
              (BelowOf7 ?x_11)
            )
          )
          (exists (?x_21 )
            (and
              (RightOf7 ?x_21)
              (LeftOf5 ?x_21)
            )
          )
          (exists (?x_33 )
            (and
              (AboveOf15 ?x_33)
              (BelowOf9 ?x_33)
            )
          )
          (exists (?x_34 )
            (and
              (AboveOf17 ?x_34)
              (BelowOf11 ?x_34)
            )
          )
          (exists (?x_6 )
            (and
              (RightOf19 ?x_6)
              (LeftOf1 ?x_6)
            )
          )
          (exists (?x_13 )
            (and
              (LeftOf4 ?x_13)
              (RightOf16 ?x_13)
            )
          )
          (exists (?x_7 )
            (and
              (AboveOf13 ?x_7)
              (BelowOf3 ?x_7)
            )
          )
          (exists (?x_5 )
            (and
              (LeftOf10 ?x_5)
              (RightOf18 ?x_5)
            )
          )
          (exists (?x_3 )
            (and
              (BelowOf16 ?x_3)
              (AboveOf19 ?x_3)
            )
          )
          (exists (?x_34 )
            (and
              (AboveOf14 ?x_34)
              (BelowOf4 ?x_34)
            )
          )
          (exists (?x_27 )
            (and
              (AboveOf9 ?x_27)
              (BelowOf3 ?x_27)
            )
          )
          (exists (?x_13 )
            (and
              (RightOf18 ?x_13)
              (LeftOf9 ?x_13)
            )
          )
          (exists (?x_19 )
            (and
              (AboveOf19 ?x_19)
              (BelowOf10 ?x_19)
            )
          )
          (exists (?x_10 )
            (and
              (AboveOf8 ?x_10)
              (BelowOf3 ?x_10)
            )
          )
          (exists (?x_19 )
            (and
              (BelowOf2 ?x_19)
              (AboveOf19 ?x_19)
            )
          )
          (exists (?x_34 )
            (and
              (BelowOf1 ?x_34)
              (AboveOf18 ?x_34)
            )
          )
          (exists (?x_6 )
            (and
              (LeftOf15 ?x_6)
              (RightOf18 ?x_6)
            )
          )
          (exists (?x_10 )
            (and
              (BelowOf3 ?x_10)
              (AboveOf9 ?x_10)
            )
          )
          (exists (?x_7 )
            (and
              (BelowOf6 ?x_7)
              (AboveOf17 ?x_7)
            )
          )
          (exists (?x_12 )
            (and
              (RightOf16 ?x_12)
              (LeftOf5 ?x_12)
            )
          )
          (exists (?x_1 )
            (and
              (BelowOf1 ?x_1)
              (AboveOf3 ?x_1)
            )
          )
          (exists (?x_20 )
            (and
              (LeftOf2 ?x_20)
              (RightOf12 ?x_20)
            )
          )
          (exists (?x_0 )
            (and
              (AboveOf11 ?x_0)
              (BelowOf1 ?x_0)
            )
          )
          (exists (?x_27 )
            (and
              (AboveOf7 ?x_27)
              (BelowOf5 ?x_27)
            )
          )
          (exists (?x_23 )
            (and
              (RightOf16 ?x_23)
              (LeftOf2 ?x_23)
            )
          )
          (exists (?x_11 )
            (and
              (AboveOf12 ?x_11)
              (BelowOf4 ?x_11)
            )
          )
          (exists (?x_29 )
            (and
              (BelowOf1 ?x_29)
              (AboveOf13 ?x_29)
            )
          )
          (exists (?x_13 )
            (and
              (RightOf10 ?x_13)
              (LeftOf4 ?x_13)
            )
          )
          (exists (?x_25 )
            (and
              (LeftOf1 ?x_25)
              (RightOf18 ?x_25)
            )
          )
          (exists (?x_18 )
            (and
              (RightOf18 ?x_18)
              (LeftOf12 ?x_18)
            )
          )
          (exists (?x_33 )
            (and
              (AboveOf9 ?x_33)
              (BelowOf6 ?x_33)
            )
          )
          (exists (?x_18 )
            (and
              (RightOf15 ?x_18)
              (LeftOf14 ?x_18)
            )
          )
          (exists (?x_33 )
            (and
              (AboveOf18 ?x_33)
              (BelowOf6 ?x_33)
            )
          )
          (exists (?x_2 )
            (and
              (RightOf12 ?x_2)
              (LeftOf2 ?x_2)
            )
          )
          (exists (?x_20 )
            (and
              (LeftOf1 ?x_20)
              (RightOf19 ?x_20)
            )
          )
          (exists (?x_31 )
            (and
              (RightOf12 ?x_31)
              (LeftOf9 ?x_31)
            )
          )
          (exists (?x_22 )
            (and
              (BelowOf3 ?x_22)
              (AboveOf16 ?x_22)
            )
          )
          (exists (?x_36 )
            (and
              (AboveOf19 ?x_36)
              (BelowOf9 ?x_36)
            )
          )
          (exists (?x_13 )
            (and
              (RightOf18 ?x_13)
              (LeftOf2 ?x_13)
            )
          )
          (exists (?x_34 )
            (and
              (AboveOf17 ?x_34)
              (BelowOf5 ?x_34)
            )
          )
          (exists (?x_19 )
            (and
              (BelowOf5 ?x_19)
              (AboveOf18 ?x_19)
            )
          )
          (exists (?x_26 )
            (and
              (RightOf19 ?x_26)
              (LeftOf3 ?x_26)
            )
          )
          (exists (?x_19 )
            (and
              (AboveOf18 ?x_19)
              (BelowOf17 ?x_19)
            )
          )
          (exists (?x_23 )
            (and
              (RightOf8 ?x_23)
              (LeftOf8 ?x_23)
            )
          )
          (exists (?x_23 )
            (and
              (LeftOf2 ?x_23)
              (RightOf12 ?x_23)
            )
          )
          (exists (?x_21 )
            (and
              (RightOf10 ?x_21)
              (LeftOf4 ?x_21)
            )
          )
          (exists (?x_7 )
            (and
              (BelowOf7 ?x_7)
              (AboveOf18 ?x_7)
            )
          )
          (exists (?x_20 )
            (and
              (RightOf12 ?x_20)
              (LeftOf3 ?x_20)
            )
          )
          (exists (?x_11 )
            (and
              (BelowOf6 ?x_11)
              (AboveOf16 ?x_11)
            )
          )
          (exists (?x_20 )
            (and
              (LeftOf7 ?x_20)
              (RightOf18 ?x_20)
            )
          )
          (exists (?x_6 )
            (and
              (RightOf17 ?x_6)
              (LeftOf6 ?x_6)
            )
          )
          (exists (?x_10 )
            (and
              (BelowOf3 ?x_10)
              (AboveOf4 ?x_10)
            )
          )
          (exists (?x_32 )
            (and
              (BelowOf10 ?x_32)
              (AboveOf11 ?x_32)
            )
          )
          (exists (?x_18 )
            (and
              (RightOf17 ?x_18)
              (LeftOf13 ?x_18)
            )
          )
          (exists (?x_37 )
            (and
              (AboveOf14 ?x_37)
              (BelowOf4 ?x_37)
            )
          )
          (exists (?x_22 )
            (and
              (BelowOf12 ?x_22)
              (AboveOf18 ?x_22)
            )
          )
          (exists (?x_18 )
            (and
              (LeftOf1 ?x_18)
              (RightOf19 ?x_18)
            )
          )
          (exists (?x_33 )
            (and
              (AboveOf10 ?x_33)
              (BelowOf5 ?x_33)
            )
          )
          (exists (?x_34 )
            (and
              (BelowOf14 ?x_34)
              (AboveOf16 ?x_34)
            )
          )
          (exists (?x_19 )
            (and
              (BelowOf11 ?x_19)
              (AboveOf19 ?x_19)
            )
          )
          (exists (?x_2 )
            (and
              (LeftOf6 ?x_2)
              (RightOf9 ?x_2)
            )
          )
          (exists (?x_27 )
            (and
              (BelowOf6 ?x_27)
              (AboveOf14 ?x_27)
            )
          )
          (exists (?x_10 )
            (and
              (BelowOf4 ?x_10)
              (AboveOf9 ?x_10)
            )
          )
          (exists (?x_22 )
            (and
              (BelowOf7 ?x_22)
              (AboveOf16 ?x_22)
            )
          )
          (exists (?x_22 )
            (and
              (AboveOf18 ?x_22)
              (BelowOf1 ?x_22)
            )
          )
          (exists (?x_16 )
            (and
              (LeftOf1 ?x_16)
              (RightOf19 ?x_16)
            )
          )
          (exists (?x_5 )
            (and
              (LeftOf7 ?x_5)
              (RightOf17 ?x_5)
            )
          )
          (exists (?x_7 )
            (and
              (BelowOf1 ?x_7)
              (AboveOf17 ?x_7)
            )
          )
          (exists (?x_23 )
            (and
              (RightOf8 ?x_23)
              (LeftOf3 ?x_23)
            )
          )
          (exists (?x_22 )
            (and
              (AboveOf19 ?x_22)
              (BelowOf10 ?x_22)
            )
          )
          (exists (?x_29 )
            (and
              (BelowOf5 ?x_29)
              (AboveOf14 ?x_29)
            )
          )
          (exists (?x_22 )
            (and
              (BelowOf7 ?x_22)
              (AboveOf13 ?x_22)
            )
          )
          (exists (?x_29 )
            (and
              (BelowOf3 ?x_29)
              (AboveOf10 ?x_29)
            )
          )
          (exists (?x_15 )
            (and
              (BelowOf1 ?x_15)
              (AboveOf5 ?x_15)
            )
          )
          (exists (?x_23 )
            (and
              (LeftOf8 ?x_23)
              (RightOf12 ?x_23)
            )
          )
          (exists (?x_16 )
            (and
              (LeftOf4 ?x_16)
              (RightOf14 ?x_16)
            )
          )
          (exists (?x_5 )
            (and
              (LeftOf6 ?x_5)
              (RightOf18 ?x_5)
            )
          )
          (exists (?x_11 )
            (and
              (AboveOf14 ?x_11)
              (BelowOf6 ?x_11)
            )
          )
          (exists (?x_5 )
            (and
              (RightOf12 ?x_5)
              (LeftOf3 ?x_5)
            )
          )
          (exists (?x_0 )
            (and
              (AboveOf12 ?x_0)
              (BelowOf2 ?x_0)
            )
          )
          (exists (?x_33 )
            (and
              (BelowOf7 ?x_33)
              (AboveOf14 ?x_33)
            )
          )
          (exists (?x_32 )
            (and
              (AboveOf14 ?x_32)
              (BelowOf5 ?x_32)
            )
          )
          (exists (?x_13 )
            (and
              (LeftOf9 ?x_13)
              (RightOf19 ?x_13)
            )
          )
          (exists (?x_2 )
            (and
              (LeftOf4 ?x_2)
              (RightOf14 ?x_2)
            )
          )
          (exists (?x_21 )
            (and
              (RightOf18 ?x_21)
              (LeftOf5 ?x_21)
            )
          )
          (exists (?x_18 )
            (and
              (LeftOf5 ?x_18)
              (RightOf16 ?x_18)
            )
          )
          (exists (?x_31 )
            (and
              (LeftOf5 ?x_31)
              (RightOf19 ?x_31)
            )
          )
          (exists (?x_7 )
            (and
              (AboveOf15 ?x_7)
              (BelowOf6 ?x_7)
            )
          )
          (exists (?x_31 )
            (and
              (RightOf19 ?x_31)
              (LeftOf1 ?x_31)
            )
          )
          (exists (?x_3 )
            (and
              (BelowOf8 ?x_3)
              (AboveOf16 ?x_3)
            )
          )
          (exists (?x_20 )
            (and
              (LeftOf3 ?x_20)
              (RightOf19 ?x_20)
            )
          )
          (exists (?x_27 )
            (and
              (BelowOf3 ?x_27)
              (AboveOf17 ?x_27)
            )
          )
          (exists (?x_6 )
            (and
              (RightOf18 ?x_6)
              (LeftOf12 ?x_6)
            )
          )
          (exists (?x_12 )
            (and
              (RightOf15 ?x_12)
              (LeftOf6 ?x_12)
            )
          )
          (exists (?x_8 )
            (and
              (RightOf5 ?x_8)
              (LeftOf1 ?x_8)
            )
          )
          (exists (?x_5 )
            (and
              (RightOf19 ?x_5)
              (LeftOf10 ?x_5)
            )
          )
          (exists (?x_4 )
            (and
              (RightOf17 ?x_4)
              (LeftOf11 ?x_4)
            )
          )
          (exists (?x_6 )
            (and
              (RightOf17 ?x_6)
              (LeftOf3 ?x_6)
            )
          )
          (exists (?x_18 )
            (and
              (LeftOf2 ?x_18)
              (RightOf16 ?x_18)
            )
          )
          (exists (?x_24 )
            (and
              (AboveOf15 ?x_24)
              (BelowOf14 ?x_24)
            )
          )
          (exists (?x_10 )
            (and
              (AboveOf19 ?x_10)
              (BelowOf1 ?x_10)
            )
          )
          (exists (?x_27 )
            (and
              (AboveOf18 ?x_27)
              (BelowOf1 ?x_27)
            )
          )
          (exists (?x_14 )
            (and
              (RightOf14 ?x_14)
              (LeftOf7 ?x_14)
            )
          )
          (exists (?x_13 )
            (and
              (LeftOf7 ?x_13)
              (RightOf17 ?x_13)
            )
          )
          (exists (?x_8 )
            (and
              (LeftOf2 ?x_8)
              (RightOf19 ?x_8)
            )
          )
          (exists (?x_3 )
            (and
              (BelowOf7 ?x_3)
              (AboveOf18 ?x_3)
            )
          )
          (exists (?x_21 )
            (and
              (RightOf6 ?x_21)
              (LeftOf2 ?x_21)
            )
          )
          (exists (?x_8 )
            (and
              (LeftOf1 ?x_8)
              (RightOf12 ?x_8)
            )
          )
          (exists (?x_23 )
            (and
              (RightOf10 ?x_23)
              (LeftOf4 ?x_23)
            )
          )
          (exists (?x_5 )
            (and
              (RightOf15 ?x_5)
              (LeftOf2 ?x_5)
            )
          )
          (exists (?x_24 )
            (and
              (BelowOf1 ?x_24)
              (AboveOf16 ?x_24)
            )
          )
          (exists (?x_14 )
            (and
              (RightOf19 ?x_14)
              (LeftOf5 ?x_14)
            )
          )
          (exists (?x_5 )
            (and
              (LeftOf1 ?x_5)
              (RightOf18 ?x_5)
            )
          )
          (exists (?x_29 )
            (and
              (BelowOf4 ?x_29)
              (AboveOf8 ?x_29)
            )
          )
          (exists (?x_28 )
            (and
              (RightOf4 ?x_28)
              (LeftOf1 ?x_28)
            )
          )
          (exists (?x_4 )
            (and
              (RightOf18 ?x_4)
              (LeftOf16 ?x_4)
            )
          )
          (exists (?x_35 )
            (and
              (AboveOf19 ?x_35)
              (BelowOf10 ?x_35)
            )
          )
          (exists (?x_19 )
            (and
              (AboveOf19 ?x_19)
              (BelowOf9 ?x_19)
            )
          )
          (exists (?x_22 )
            (and
              (AboveOf14 ?x_22)
              (BelowOf2 ?x_22)
            )
          )
          (exists (?x_33 )
            (and
              (AboveOf13 ?x_33)
              (BelowOf6 ?x_33)
            )
          )
          (exists (?x_35 )
            (and
              (BelowOf10 ?x_35)
              (AboveOf18 ?x_35)
            )
          )
          (exists (?x_32 )
            (and
              (AboveOf14 ?x_32)
              (BelowOf8 ?x_32)
            )
          )
          (exists (?x_7 )
            (and
              (AboveOf8 ?x_7)
              (BelowOf3 ?x_7)
            )
          )
          (exists (?x_32 )
            (and
              (AboveOf19 ?x_32)
              (BelowOf5 ?x_32)
            )
          )
          (exists (?x_14 )
            (and
              (RightOf16 ?x_14)
              (LeftOf12 ?x_14)
            )
          )
          (exists (?x_11 )
            (and
              (BelowOf5 ?x_11)
              (AboveOf17 ?x_11)
            )
          )
          (exists (?x_20 )
            (and
              (RightOf16 ?x_20)
              (LeftOf4 ?x_20)
            )
          )
          (exists (?x_32 )
            (and
              (AboveOf10 ?x_32)
              (BelowOf3 ?x_32)
            )
          )
          (exists (?x_35 )
            (and
              (AboveOf14 ?x_35)
              (BelowOf6 ?x_35)
            )
          )
          (exists (?x_6 )
            (and
              (RightOf16 ?x_6)
              (LeftOf4 ?x_6)
            )
          )
          (exists (?x_33 )
            (and
              (BelowOf4 ?x_33)
              (AboveOf9 ?x_33)
            )
          )
          (exists (?x_31 )
            (and
              (RightOf9 ?x_31)
              (LeftOf7 ?x_31)
            )
          )
          (exists (?x_18 )
            (and
              (LeftOf7 ?x_18)
              (RightOf16 ?x_18)
            )
          )
          (exists (?x_3 )
            (and
              (AboveOf17 ?x_3)
              (BelowOf1 ?x_3)
            )
          )
          (exists (?x_25 )
            (and
              (RightOf19 ?x_25)
              (LeftOf1 ?x_25)
            )
          )
          (exists (?x_25 )
            (and
              (LeftOf13 ?x_25)
              (RightOf19 ?x_25)
            )
          )
          (exists (?x_37 )
            (and
              (AboveOf18 ?x_37)
              (BelowOf1 ?x_37)
            )
          )
          (exists (?x_3 )
            (and
              (BelowOf6 ?x_3)
              (AboveOf18 ?x_3)
            )
          )
          (exists (?x_35 )
            (and
              (AboveOf12 ?x_35)
              (BelowOf5 ?x_35)
            )
          )
          (exists (?x_33 )
            (and
              (BelowOf9 ?x_33)
              (AboveOf12 ?x_33)
            )
          )
          (exists (?x_4 )
            (and
              (LeftOf16 ?x_4)
              (RightOf19 ?x_4)
            )
          )
          (exists (?x_7 )
            (and
              (BelowOf7 ?x_7)
              (AboveOf14 ?x_7)
            )
          )
          (exists (?x_24 )
            (and
              (AboveOf15 ?x_24)
              (BelowOf12 ?x_24)
            )
          )
          (exists (?x_37 )
            (and
              (BelowOf3 ?x_37)
              (AboveOf12 ?x_37)
            )
          )
          (exists (?x_12 )
            (and
              (RightOf14 ?x_12)
              (LeftOf3 ?x_12)
            )
          )
          (exists (?x_6 )
            (and
              (RightOf18 ?x_6)
              (LeftOf3 ?x_6)
            )
          )
          (exists (?x_20 )
            (and
              (RightOf17 ?x_20)
              (LeftOf5 ?x_20)
            )
          )
          (exists (?x_12 )
            (and
              (RightOf13 ?x_12)
              (LeftOf2 ?x_12)
            )
          )
          (exists (?x_35 )
            (and
              (BelowOf8 ?x_35)
              (AboveOf14 ?x_35)
            )
          )
          (exists (?x_31 )
            (and
              (LeftOf3 ?x_31)
              (RightOf9 ?x_31)
            )
          )
          (exists (?x_34 )
            (and
              (BelowOf6 ?x_34)
              (AboveOf14 ?x_34)
            )
          )
          (exists (?x_7 )
            (and
              (BelowOf3 ?x_7)
              (AboveOf15 ?x_7)
            )
          )
          (exists (?x_13 )
            (and
              (LeftOf7 ?x_13)
              (RightOf13 ?x_13)
            )
          )
          (exists (?x_20 )
            (and
              (LeftOf4 ?x_20)
              (RightOf14 ?x_20)
            )
          )
          (exists (?x_11 )
            (and
              (AboveOf19 ?x_11)
              (BelowOf2 ?x_11)
            )
          )
          (exists (?x_10 )
            (and
              (BelowOf4 ?x_10)
              (AboveOf13 ?x_10)
            )
          )
          (exists (?x_25 )
            (and
              (LeftOf5 ?x_25)
              (RightOf19 ?x_25)
            )
          )
          (exists (?x_18 )
            (and
              (LeftOf12 ?x_18)
              (RightOf16 ?x_18)
            )
          )
          (exists (?x_2 )
            (and
              (RightOf10 ?x_2)
              (LeftOf3 ?x_2)
            )
          )
          (exists (?x_7 )
            (and
              (AboveOf9 ?x_7)
              (BelowOf3 ?x_7)
            )
          )
          (exists (?x_13 )
            (and
              (LeftOf4 ?x_13)
              (RightOf18 ?x_13)
            )
          )
          (exists (?x_24 )
            (and
              (AboveOf16 ?x_24)
              (BelowOf2 ?x_24)
            )
          )
          (exists (?x_23 )
            (and
              (RightOf15 ?x_23)
              (LeftOf7 ?x_23)
            )
          )
          (exists (?x_3 )
            (and
              (AboveOf16 ?x_3)
              (BelowOf9 ?x_3)
            )
          )
          (exists (?x_4 )
            (and
              (RightOf19 ?x_4)
              (LeftOf1 ?x_4)
            )
          )
          (exists (?x_9 )
            (and
              (AboveOf19 ?x_9)
              (BelowOf4 ?x_9)
            )
          )
          (exists (?x_11 )
            (and
              (BelowOf3 ?x_11)
              (AboveOf19 ?x_11)
            )
          )
          (exists (?x_20 )
            (and
              (LeftOf4 ?x_20)
              (RightOf18 ?x_20)
            )
          )
          (exists (?x_29 )
            (and
              (BelowOf5 ?x_29)
              (AboveOf10 ?x_29)
            )
          )
          (exists (?x_16 )
            (and
              (LeftOf2 ?x_16)
              (RightOf15 ?x_16)
            )
          )
          (exists (?x_32 )
            (and
              (BelowOf3 ?x_32)
              (AboveOf12 ?x_32)
            )
          )
          (exists (?x_11 )
            (and
              (BelowOf9 ?x_11)
              (AboveOf16 ?x_11)
            )
          )
          (exists (?x_13 )
            (and
              (RightOf18 ?x_13)
              (LeftOf7 ?x_13)
            )
          )
          (exists (?x_29 )
            (and
              (BelowOf7 ?x_29)
              (AboveOf11 ?x_29)
            )
          )
          (exists (?x_19 )
            (and
              (AboveOf19 ?x_19)
              (BelowOf11 ?x_19)
            )
          )
          (exists (?x_32 )
            (and
              (AboveOf18 ?x_32)
              (BelowOf9 ?x_32)
            )
          )
          (exists (?x_8 )
            (and
              (RightOf4 ?x_8)
              (LeftOf3 ?x_8)
            )
          )
          (exists (?x_6 )
            (and
              (RightOf18 ?x_6)
              (LeftOf6 ?x_6)
            )
          )
          (exists (?x_16 )
            (and
              (LeftOf2 ?x_16)
              (RightOf18 ?x_16)
            )
          )
          (exists (?x_33 )
            (and
              (BelowOf9 ?x_33)
              (AboveOf17 ?x_33)
            )
          )
          (exists (?x_18 )
            (and
              (LeftOf14 ?x_18)
              (RightOf16 ?x_18)
            )
          )
          (exists (?x_29 )
            (and
              (AboveOf15 ?x_29)
              (BelowOf7 ?x_29)
            )
          )
          (exists (?x_37 )
            (and
              (AboveOf11 ?x_37)
              (BelowOf5 ?x_37)
            )
          )
          (exists (?x_9 )
            (and
              (BelowOf9 ?x_9)
              (AboveOf18 ?x_9)
            )
          )
          (exists (?x_2 )
            (and
              (LeftOf4 ?x_2)
              (RightOf13 ?x_2)
            )
          )
          (exists (?x_1 )
            (and
              (BelowOf1 ?x_1)
              (AboveOf6 ?x_1)
            )
          )
          (exists (?x_21 )
            (and
              (RightOf10 ?x_21)
              (LeftOf3 ?x_21)
            )
          )
          (exists (?x_33 )
            (and
              (BelowOf6 ?x_33)
              (AboveOf11 ?x_33)
            )
          )
          (exists (?x_10 )
            (and
              (BelowOf3 ?x_10)
              (AboveOf10 ?x_10)
            )
          )
          (exists (?x_13 )
            (and
              (RightOf11 ?x_13)
              (LeftOf6 ?x_13)
            )
          )
          (exists (?x_37 )
            (and
              (BelowOf3 ?x_37)
              (AboveOf8 ?x_37)
            )
          )
          (exists (?x_27 )
            (and
              (AboveOf10 ?x_27)
              (BelowOf5 ?x_27)
            )
          )
          (exists (?x_20 )
            (and
              (RightOf14 ?x_20)
              (LeftOf5 ?x_20)
            )
          )
          (exists (?x_17 )
            (and
              (RightOf9 ?x_17)
              (LeftOf1 ?x_17)
            )
          )
          (exists (?x_24 )
            (and
              (BelowOf12 ?x_24)
              (AboveOf19 ?x_24)
            )
          )
          (exists (?x_2 )
            (and
              (RightOf15 ?x_2)
              (LeftOf2 ?x_2)
            )
          )
          (exists (?x_31 )
            (and
              (RightOf14 ?x_31)
              (LeftOf9 ?x_31)
            )
          )
          (exists (?x_33 )
            (and
              (BelowOf9 ?x_33)
              (AboveOf14 ?x_33)
            )
          )
          (exists (?x_22 )
            (and
              (AboveOf17 ?x_22)
              (BelowOf1 ?x_22)
            )
          )
          (exists (?x_13 )
            (and
              (LeftOf5 ?x_13)
              (RightOf17 ?x_13)
            )
          )
          (exists (?x_6 )
            (and
              (RightOf19 ?x_6)
              (LeftOf8 ?x_6)
            )
          )
          (exists (?x_22 )
            (and
              (AboveOf14 ?x_22)
              (BelowOf7 ?x_22)
            )
          )
          (exists (?x_20 )
            (and
              (RightOf12 ?x_20)
              (LeftOf6 ?x_20)
            )
          )
          (exists (?x_19 )
            (and
              (AboveOf19 ?x_19)
              (BelowOf6 ?x_19)
            )
          )
          (exists (?x_29 )
            (and
              (BelowOf6 ?x_29)
              (AboveOf19 ?x_29)
            )
          )
          (exists (?x_33 )
            (and
              (AboveOf14 ?x_33)
              (BelowOf8 ?x_33)
            )
          )
          (exists (?x_35 )
            (and
              (AboveOf17 ?x_35)
              (BelowOf1 ?x_35)
            )
          )
          (exists (?x_13 )
            (and
              (LeftOf9 ?x_13)
              (RightOf15 ?x_13)
            )
          )
          (exists (?x_14 )
            (and
              (RightOf16 ?x_14)
              (LeftOf13 ?x_14)
            )
          )
          (exists (?x_10 )
            (and
              (AboveOf12 ?x_10)
              (BelowOf1 ?x_10)
            )
          )
          (exists (?x_15 )
            (and
              (AboveOf4 ?x_15)
              (BelowOf1 ?x_15)
            )
          )
          (exists (?x_3 )
            (and
              (AboveOf18 ?x_3)
              (BelowOf9 ?x_3)
            )
          )
          (exists (?x_35 )
            (and
              (AboveOf16 ?x_35)
              (BelowOf9 ?x_35)
            )
          )
          (exists (?x_26 )
            (and
              (LeftOf5 ?x_26)
              (RightOf19 ?x_26)
            )
          )
          (exists (?x_20 )
            (and
              (LeftOf1 ?x_20)
              (RightOf12 ?x_20)
            )
          )
          (exists (?x_15 )
            (and
              (BelowOf2 ?x_15)
              (AboveOf2 ?x_15)
            )
          )
          (exists (?x_29 )
            (and
              (BelowOf1 ?x_29)
              (AboveOf18 ?x_29)
            )
          )
          (exists (?x_35 )
            (and
              (BelowOf5 ?x_35)
              (AboveOf17 ?x_35)
            )
          )
          (exists (?x_27 )
            (and
              (BelowOf1 ?x_27)
              (AboveOf12 ?x_27)
            )
          )
          (exists (?x_14 )
            (and
              (RightOf13 ?x_14)
              (LeftOf13 ?x_14)
            )
          )
          (exists (?x_32 )
            (and
              (BelowOf9 ?x_32)
              (AboveOf14 ?x_32)
            )
          )
          (exists (?x_13 )
            (and
              (RightOf11 ?x_13)
              (LeftOf5 ?x_13)
            )
          )
          (exists (?x_0 )
            (and
              (AboveOf17 ?x_0)
              (BelowOf1 ?x_0)
            )
          )
          (exists (?x_35 )
            (and
              (AboveOf12 ?x_35)
              (BelowOf8 ?x_35)
            )
          )
          (exists (?x_1 )
            (and
              (BelowOf1 ?x_1)
              (AboveOf15 ?x_1)
            )
          )
          (exists (?x_2 )
            (and
              (LeftOf4 ?x_2)
              (RightOf19 ?x_2)
            )
          )
          (exists (?x_29 )
            (and
              (BelowOf5 ?x_29)
              (AboveOf9 ?x_29)
            )
          )
          (exists (?x_17 )
            (and
              (LeftOf1 ?x_17)
              (RightOf5 ?x_17)
            )
          )
          (exists (?x_21 )
            (and
              (LeftOf2 ?x_21)
              (RightOf10 ?x_21)
            )
          )
          (exists (?x_27 )
            (and
              (BelowOf2 ?x_27)
              (AboveOf18 ?x_27)
            )
          )
          (exists (?x_15 )
            (and
              (BelowOf1 ?x_15)
              (AboveOf11 ?x_15)
            )
          )
          (exists (?x_37 )
            (and
              (BelowOf5 ?x_37)
              (AboveOf17 ?x_37)
            )
          )
          (exists (?x_3 )
            (and
              (AboveOf18 ?x_3)
              (BelowOf2 ?x_3)
            )
          )
          (exists (?x_29 )
            (and
              (AboveOf11 ?x_29)
              (BelowOf3 ?x_29)
            )
          )
          (exists (?x_24 )
            (and
              (AboveOf17 ?x_24)
              (BelowOf14 ?x_24)
            )
          )
          (exists (?x_37 )
            (and
              (AboveOf11 ?x_37)
              (BelowOf3 ?x_37)
            )
          )
          (exists (?x_20 )
            (and
              (LeftOf9 ?x_20)
              (RightOf19 ?x_20)
            )
          )
          (exists (?x_5 )
            (and
              (RightOf14 ?x_5)
              (LeftOf8 ?x_5)
            )
          )
          (exists (?x_6 )
            (and
              (RightOf16 ?x_6)
              (LeftOf9 ?x_6)
            )
          )
          (exists (?x_37 )
            (and
              (BelowOf3 ?x_37)
              (AboveOf16 ?x_37)
            )
          )
          (exists (?x_29 )
            (and
              (AboveOf13 ?x_29)
              (BelowOf2 ?x_29)
            )
          )
          (exists (?x_15 )
            (and
              (BelowOf1 ?x_15)
              (AboveOf16 ?x_15)
            )
          )
          (exists (?x_6 )
            (and
              (LeftOf12 ?x_6)
              (RightOf16 ?x_6)
            )
          )
          (exists (?x_29 )
            (and
              (AboveOf17 ?x_29)
              (BelowOf1 ?x_29)
            )
          )
          (exists (?x_32 )
            (and
              (AboveOf19 ?x_32)
              (BelowOf3 ?x_32)
            )
          )
          (exists (?x_5 )
            (and
              (LeftOf2 ?x_5)
              (RightOf18 ?x_5)
            )
          )
          (exists (?x_33 )
            (and
              (AboveOf17 ?x_33)
              (BelowOf5 ?x_33)
            )
          )
          (exists (?x_16 )
            (and
              (RightOf10 ?x_16)
              (LeftOf4 ?x_16)
            )
          )
          (exists (?x_3 )
            (and
              (BelowOf16 ?x_3)
              (AboveOf18 ?x_3)
            )
          )
          (exists (?x_37 )
            (and
              (AboveOf14 ?x_37)
              (BelowOf2 ?x_37)
            )
          )
          (exists (?x_16 )
            (and
              (LeftOf2 ?x_16)
              (RightOf9 ?x_16)
            )
          )
          (exists (?x_13 )
            (and
              (RightOf12 ?x_13)
              (LeftOf6 ?x_13)
            )
          )
          (exists (?x_20 )
            (and
              (RightOf16 ?x_20)
              (LeftOf11 ?x_20)
            )
          )
          (exists (?x_23 )
            (and
              (RightOf17 ?x_23)
              (LeftOf3 ?x_23)
            )
          )
          (exists (?x_33 )
            (and
              (BelowOf3 ?x_33)
              (AboveOf12 ?x_33)
            )
          )
          (exists (?x_22 )
            (and
              (BelowOf6 ?x_22)
              (AboveOf15 ?x_22)
            )
          )
          (exists (?x_35 )
            (and
              (BelowOf4 ?x_35)
              (AboveOf13 ?x_35)
            )
          )
          (exists (?x_4 )
            (and
              (RightOf19 ?x_4)
              (LeftOf5 ?x_4)
            )
          )
          (exists (?x_37 )
            (and
              (AboveOf6 ?x_37)
              (BelowOf5 ?x_37)
            )
          )
          (exists (?x_14 )
            (and
              (LeftOf4 ?x_14)
              (RightOf19 ?x_14)
            )
          )
          (exists (?x_34 )
            (and
              (BelowOf12 ?x_34)
              (AboveOf18 ?x_34)
            )
          )
          (exists (?x_8 )
            (and
              (LeftOf2 ?x_8)
              (RightOf14 ?x_8)
            )
          )
          (exists (?x_5 )
            (and
              (RightOf15 ?x_5)
              (LeftOf10 ?x_5)
            )
          )
          (exists (?x_11 )
            (and
              (BelowOf9 ?x_11)
              (AboveOf13 ?x_11)
            )
          )
          (exists (?x_14 )
            (and
              (RightOf18 ?x_14)
              (LeftOf7 ?x_14)
            )
          )
          (exists (?x_34 )
            (and
              (BelowOf8 ?x_34)
              (AboveOf14 ?x_34)
            )
          )
          (exists (?x_12 )
            (and
              (RightOf15 ?x_12)
              (LeftOf1 ?x_12)
            )
          )
          (exists (?x_21 )
            (and
              (LeftOf2 ?x_21)
              (RightOf6 ?x_21)
            )
          )
          (exists (?x_6 )
            (and
              (RightOf18 ?x_6)
              (LeftOf4 ?x_6)
            )
          )
          (exists (?x_24 )
            (and
              (AboveOf18 ?x_24)
              (BelowOf5 ?x_24)
            )
          )
          (exists (?x_34 )
            (and
              (BelowOf14 ?x_34)
              (AboveOf18 ?x_34)
            )
          )
          (exists (?x_12 )
            (and
              (LeftOf1 ?x_12)
              (RightOf9 ?x_12)
            )
          )
          (exists (?x_12 )
            (and
              (RightOf10 ?x_12)
              (LeftOf2 ?x_12)
            )
          )
          (exists (?x_15 )
            (and
              (AboveOf14 ?x_15)
              (BelowOf2 ?x_15)
            )
          )
          (exists (?x_23 )
            (and
              (LeftOf1 ?x_23)
              (RightOf16 ?x_23)
            )
          )
          (exists (?x_20 )
            (and
              (LeftOf4 ?x_20)
              (RightOf15 ?x_20)
            )
          )
          (exists (?x_23 )
            (and
              (RightOf16 ?x_23)
              (LeftOf8 ?x_23)
            )
          )
          (exists (?x_14 )
            (and
              (RightOf14 ?x_14)
              (LeftOf2 ?x_14)
            )
          )
          (exists (?x_27 )
            (and
              (BelowOf2 ?x_27)
              (AboveOf11 ?x_27)
            )
          )
          (exists (?x_11 )
            (and
              (AboveOf11 ?x_11)
              (BelowOf3 ?x_11)
            )
          )
          (exists (?x_3 )
            (and
              (BelowOf14 ?x_3)
              (AboveOf17 ?x_3)
            )
          )
          (exists (?x_21 )
            (and
              (LeftOf2 ?x_21)
              (RightOf7 ?x_21)
            )
          )
          (exists (?x_10 )
            (and
              (AboveOf4 ?x_10)
              (BelowOf2 ?x_10)
            )
          )
          (exists (?x_14 )
            (and
              (RightOf13 ?x_14)
              (LeftOf5 ?x_14)
            )
          )
          (exists (?x_5 )
            (and
              (LeftOf7 ?x_5)
              (RightOf14 ?x_5)
            )
          )
          (exists (?x_33 )
            (and
              (BelowOf2 ?x_33)
              (AboveOf15 ?x_33)
            )
          )
          (exists (?x_25 )
            (and
              (RightOf17 ?x_25)
              (LeftOf7 ?x_25)
            )
          )
          (exists (?x_5 )
            (and
              (LeftOf9 ?x_5)
              (RightOf16 ?x_5)
            )
          )
          (exists (?x_3 )
            (and
              (BelowOf14 ?x_3)
              (AboveOf19 ?x_3)
            )
          )
          (exists (?x_5 )
            (and
              (RightOf16 ?x_5)
              (LeftOf10 ?x_5)
            )
          )
          (exists (?x_0 )
            (and
              (BelowOf1 ?x_0)
              (AboveOf18 ?x_0)
            )
          )
          (exists (?x_7 )
            (and
              (BelowOf4 ?x_7)
              (AboveOf14 ?x_7)
            )
          )
          (exists (?x_31 )
            (and
              (LeftOf8 ?x_31)
              (RightOf13 ?x_31)
            )
          )
          (exists (?x_18 )
            (and
              (RightOf19 ?x_18)
              (LeftOf9 ?x_18)
            )
          )
          (exists (?x_10 )
            (and
              (AboveOf8 ?x_10)
              (BelowOf2 ?x_10)
            )
          )
          (exists (?x_8 )
            (and
              (RightOf10 ?x_8)
              (LeftOf3 ?x_8)
            )
          )
          (exists (?x_0 )
            (and
              (BelowOf3 ?x_0)
              (AboveOf9 ?x_0)
            )
          )
          (exists (?x_11 )
            (and
              (BelowOf8 ?x_11)
              (AboveOf16 ?x_11)
            )
          )
          (exists (?x_5 )
            (and
              (RightOf14 ?x_5)
              (LeftOf3 ?x_5)
            )
          )
          (exists (?x_4 )
            (and
              (RightOf19 ?x_4)
              (LeftOf12 ?x_4)
            )
          )
          (exists (?x_5 )
            (and
              (RightOf12 ?x_5)
              (LeftOf8 ?x_5)
            )
          )
          (exists (?x_10 )
            (and
              (AboveOf9 ?x_10)
              (BelowOf2 ?x_10)
            )
          )
          (exists (?x_2 )
            (and
              (LeftOf5 ?x_2)
              (RightOf9 ?x_2)
            )
          )
          (exists (?x_33 )
            (and
              (AboveOf15 ?x_33)
              (BelowOf1 ?x_33)
            )
          )
          (exists (?x_37 )
            (and
              (BelowOf2 ?x_37)
              (AboveOf7 ?x_37)
            )
          )
          (exists (?x_0 )
            (and
              (AboveOf18 ?x_0)
              (BelowOf2 ?x_0)
            )
          )
          (exists (?x_25 )
            (and
              (LeftOf7 ?x_25)
              (RightOf16 ?x_25)
            )
          )
          (exists (?x_6 )
            (and
              (RightOf17 ?x_6)
              (LeftOf12 ?x_6)
            )
          )
          (exists (?x_11 )
            (and
              (BelowOf5 ?x_11)
              (AboveOf18 ?x_11)
            )
          )
          (exists (?x_7 )
            (and
              (AboveOf10 ?x_7)
              (BelowOf6 ?x_7)
            )
          )
          (exists (?x_31 )
            (and
              (RightOf9 ?x_31)
              (LeftOf4 ?x_31)
            )
          )
          (exists (?x_24 )
            (and
              (BelowOf9 ?x_24)
              (AboveOf17 ?x_24)
            )
          )
          (exists (?x_23 )
            (and
              (RightOf18 ?x_23)
              (LeftOf2 ?x_23)
            )
          )
          (exists (?x_31 )
            (and
              (RightOf17 ?x_31)
              (LeftOf3 ?x_31)
            )
          )
          (exists (?x_37 )
            (and
              (BelowOf3 ?x_37)
              (AboveOf7 ?x_37)
            )
          )
          (exists (?x_11 )
            (and
              (BelowOf7 ?x_11)
              (AboveOf18 ?x_11)
            )
          )
          (exists (?x_12 )
            (and
              (LeftOf5 ?x_12)
              (RightOf19 ?x_12)
            )
          )
          (exists (?x_5 )
            (and
              (LeftOf12 ?x_5)
              (RightOf13 ?x_5)
            )
          )
          (exists (?x_33 )
            (and
              (AboveOf14 ?x_33)
              (BelowOf3 ?x_33)
            )
          )
          (exists (?x_35 )
            (and
              (AboveOf12 ?x_35)
              (BelowOf9 ?x_35)
            )
          )
          (exists (?x_18 )
            (and
              (RightOf17 ?x_18)
              (LeftOf9 ?x_18)
            )
          )
          (exists (?x_10 )
            (and
              (BelowOf4 ?x_10)
              (AboveOf18 ?x_10)
            )
          )
          (exists (?x_8 )
            (and
              (RightOf16 ?x_8)
              (LeftOf1 ?x_8)
            )
          )
          (exists (?x_37 )
            (and
              (BelowOf4 ?x_37)
              (AboveOf9 ?x_37)
            )
          )
          (exists (?x_23 )
            (and
              (LeftOf6 ?x_23)
              (RightOf12 ?x_23)
            )
          )
          (exists (?x_35 )
            (and
              (AboveOf18 ?x_35)
              (BelowOf2 ?x_35)
            )
          )
          (exists (?x_37 )
            (and
              (BelowOf3 ?x_37)
              (AboveOf5 ?x_37)
            )
          )
          (exists (?x_4 )
            (and
              (RightOf18 ?x_4)
              (LeftOf8 ?x_4)
            )
          )
          (exists (?x_23 )
            (and
              (LeftOf5 ?x_23)
              (RightOf17 ?x_23)
            )
          )
          (exists (?x_14 )
            (and
              (RightOf19 ?x_14)
              (LeftOf7 ?x_14)
            )
          )
          (exists (?x_8 )
            (and
              (RightOf11 ?x_8)
              (LeftOf1 ?x_8)
            )
          )
          (exists (?x_35 )
            (and
              (BelowOf9 ?x_35)
              (AboveOf18 ?x_35)
            )
          )
          (exists (?x_5 )
            (and
              (RightOf17 ?x_5)
              (LeftOf8 ?x_5)
            )
          )
          (exists (?x_0 )
            (and
              (BelowOf3 ?x_0)
              (AboveOf14 ?x_0)
            )
          )
          (exists (?x_18 )
            (and
              (RightOf19 ?x_18)
              (LeftOf1 ?x_18)
            )
          )
          (exists (?x_32 )
            (and
              (BelowOf10 ?x_32)
              (AboveOf16 ?x_32)
            )
          )
          (exists (?x_35 )
            (and
              (BelowOf4 ?x_35)
              (AboveOf18 ?x_35)
            )
          )
          (exists (?x_31 )
            (and
              (LeftOf3 ?x_31)
              (RightOf11 ?x_31)
            )
          )
          (exists (?x_37 )
            (and
              (AboveOf16 ?x_37)
              (BelowOf1 ?x_37)
            )
          )
          (exists (?x_14 )
            (and
              (RightOf19 ?x_14)
              (LeftOf8 ?x_14)
            )
          )
          (exists (?x_12 )
            (and
              (RightOf16 ?x_12)
              (LeftOf2 ?x_12)
            )
          )
          (exists (?x_24 )
            (and
              (AboveOf17 ?x_24)
              (BelowOf8 ?x_24)
            )
          )
          (exists (?x_12 )
            (and
              (LeftOf4 ?x_12)
              (RightOf9 ?x_12)
            )
          )
          (exists (?x_18 )
            (and
              (LeftOf14 ?x_18)
              (RightOf19 ?x_18)
            )
          )
          (exists (?x_24 )
            (and
              (BelowOf10 ?x_24)
              (AboveOf18 ?x_24)
            )
          )
          (exists (?x_32 )
            (and
              (BelowOf10 ?x_32)
              (AboveOf17 ?x_32)
            )
          )
          (exists (?x_28 )
            (and
              (RightOf3 ?x_28)
              (LeftOf2 ?x_28)
            )
          )
          (exists (?x_0 )
            (and
              (AboveOf14 ?x_0)
              (BelowOf2 ?x_0)
            )
          )
          (exists (?x_25 )
            (and
              (LeftOf7 ?x_25)
              (RightOf18 ?x_25)
            )
          )
          (exists (?x_25 )
            (and
              (RightOf15 ?x_25)
              (LeftOf9 ?x_25)
            )
          )
          (exists (?x_3 )
            (and
              (BelowOf4 ?x_3)
              (AboveOf17 ?x_3)
            )
          )
          (exists (?x_18 )
            (and
              (LeftOf7 ?x_18)
              (RightOf17 ?x_18)
            )
          )
          (exists (?x_13 )
            (and
              (RightOf16 ?x_13)
              (LeftOf6 ?x_13)
            )
          )
          (exists (?x_3 )
            (and
              (AboveOf16 ?x_3)
              (BelowOf7 ?x_3)
            )
          )
          (exists (?x_28 )
            (and
              (RightOf19 ?x_28)
              (LeftOf1 ?x_28)
            )
          )
          (exists (?x_7 )
            (and
              (AboveOf19 ?x_7)
              (BelowOf6 ?x_7)
            )
          )
          (exists (?x_34 )
            (and
              (BelowOf3 ?x_34)
              (AboveOf19 ?x_34)
            )
          )
          (exists (?x_18 )
            (and
              (RightOf15 ?x_18)
              (LeftOf5 ?x_18)
            )
          )
          (exists (?x_31 )
            (and
              (LeftOf3 ?x_31)
              (RightOf15 ?x_31)
            )
          )
          (exists (?x_20 )
            (and
              (LeftOf11 ?x_20)
              (RightOf15 ?x_20)
            )
          )
          (exists (?x_32 )
            (and
              (BelowOf2 ?x_32)
              (AboveOf13 ?x_32)
            )
          )
          (exists (?x_31 )
            (and
              (RightOf11 ?x_31)
              (LeftOf7 ?x_31)
            )
          )
          (exists (?x_27 )
            (and
              (BelowOf6 ?x_27)
              (AboveOf11 ?x_27)
            )
          )
          (exists (?x_35 )
            (and
              (AboveOf14 ?x_35)
              (BelowOf2 ?x_35)
            )
          )
          (exists (?x_14 )
            (and
              (RightOf16 ?x_14)
              (LeftOf8 ?x_14)
            )
          )
          (exists (?x_7 )
            (and
              (AboveOf12 ?x_7)
              (BelowOf5 ?x_7)
            )
          )
          (exists (?x_26 )
            (and
              (RightOf19 ?x_26)
              (LeftOf7 ?x_26)
            )
          )
          (exists (?x_24 )
            (and
              (BelowOf13 ?x_24)
              (AboveOf16 ?x_24)
            )
          )
          (exists (?x_29 )
            (and
              (BelowOf1 ?x_29)
              (AboveOf9 ?x_29)
            )
          )
          (exists (?x_33 )
            (and
              (AboveOf18 ?x_33)
              (BelowOf3 ?x_33)
            )
          )
          (exists (?x_23 )
            (and
              (LeftOf5 ?x_23)
              (RightOf19 ?x_23)
            )
          )
          (exists (?x_31 )
            (and
              (RightOf12 ?x_31)
              (LeftOf4 ?x_31)
            )
          )
          (exists (?x_9 )
            (and
              (AboveOf17 ?x_9)
              (BelowOf14 ?x_9)
            )
          )
          (exists (?x_5 )
            (and
              (RightOf16 ?x_5)
              (LeftOf3 ?x_5)
            )
          )
          (exists (?x_11 )
            (and
              (BelowOf8 ?x_11)
              (AboveOf15 ?x_11)
            )
          )
          (exists (?x_25 )
            (and
              (RightOf17 ?x_25)
              (LeftOf2 ?x_25)
            )
          )
          (exists (?x_7 )
            (and
              (AboveOf10 ?x_7)
              (BelowOf1 ?x_7)
            )
          )
          (exists (?x_18 )
            (and
              (LeftOf8 ?x_18)
              (RightOf19 ?x_18)
            )
          )
          (exists (?x_3 )
            (and
              (BelowOf2 ?x_3)
              (AboveOf18 ?x_3)
            )
          )
          (exists (?x_32 )
            (and
              (BelowOf1 ?x_32)
              (AboveOf19 ?x_32)
            )
          )
          (exists (?x_2 )
            (and
              (LeftOf6 ?x_2)
              (RightOf8 ?x_2)
            )
          )
          (exists (?x_12 )
            (and
              (LeftOf2 ?x_12)
              (RightOf14 ?x_12)
            )
          )
          (exists (?x_22 )
            (and
              (BelowOf5 ?x_22)
              (AboveOf17 ?x_22)
            )
          )
          (exists (?x_13 )
            (and
              (RightOf15 ?x_13)
              (LeftOf2 ?x_13)
            )
          )
          (exists (?x_13 )
            (and
              (RightOf13 ?x_13)
              (LeftOf3 ?x_13)
            )
          )
          (exists (?x_16 )
            (and
              (RightOf7 ?x_16)
              (LeftOf3 ?x_16)
            )
          )
          (exists (?x_3 )
            (and
              (BelowOf11 ?x_3)
              (AboveOf19 ?x_3)
            )
          )
          (exists (?x_37 )
            (and
              (BelowOf5 ?x_37)
              (AboveOf13 ?x_37)
            )
          )
          (exists (?x_0 )
            (and
              (AboveOf13 ?x_0)
              (BelowOf2 ?x_0)
            )
          )
          (exists (?x_14 )
            (and
              (LeftOf8 ?x_14)
              (RightOf15 ?x_14)
            )
          )
          (exists (?x_11 )
            (and
              (BelowOf1 ?x_11)
              (AboveOf18 ?x_11)
            )
          )
          (exists (?x_37 )
            (and
              (AboveOf12 ?x_37)
              (BelowOf2 ?x_37)
            )
          )
          (exists (?x_18 )
            (and
              (RightOf18 ?x_18)
              (LeftOf11 ?x_18)
            )
          )
          (exists (?x_21 )
            (and
              (LeftOf2 ?x_21)
              (RightOf19 ?x_21)
            )
          )
          (exists (?x_25 )
            (and
              (LeftOf15 ?x_25)
              (RightOf18 ?x_25)
            )
          )
          (exists (?x_34 )
            (and
              (AboveOf17 ?x_34)
              (BelowOf4 ?x_34)
            )
          )
          (exists (?x_29 )
            (and
              (AboveOf16 ?x_29)
              (BelowOf8 ?x_29)
            )
          )
          (exists (?x_31 )
            (and
              (RightOf19 ?x_31)
              (LeftOf4 ?x_31)
            )
          )
          (exists (?x_16 )
            (and
              (RightOf12 ?x_16)
              (LeftOf3 ?x_16)
            )
          )
          (exists (?x_1 )
            (and
              (BelowOf1 ?x_1)
              (AboveOf16 ?x_1)
            )
          )
          (exists (?x_25 )
            (and
              (RightOf18 ?x_25)
              (LeftOf4 ?x_25)
            )
          )
          (exists (?x_25 )
            (and
              (LeftOf4 ?x_25)
              (RightOf18 ?x_25)
            )
          )
          (exists (?x_17 )
            (and
              (LeftOf1 ?x_17)
              (RightOf14 ?x_17)
            )
          )
          (exists (?x_33 )
            (and
              (AboveOf16 ?x_33)
              (BelowOf1 ?x_33)
            )
          )
          (exists (?x_4 )
            (and
              (RightOf19 ?x_4)
              (LeftOf7 ?x_4)
            )
          )
          (exists (?x_14 )
            (and
              (RightOf14 ?x_14)
              (LeftOf4 ?x_14)
            )
          )
          (exists (?x_12 )
            (and
              (RightOf15 ?x_12)
              (LeftOf4 ?x_12)
            )
          )
          (exists (?x_2 )
            (and
              (RightOf7 ?x_2)
              (LeftOf2 ?x_2)
            )
          )
          (exists (?x_20 )
            (and
              (RightOf18 ?x_20)
              (LeftOf4 ?x_20)
            )
          )
          (exists (?x_13 )
            (and
              (LeftOf9 ?x_13)
              (RightOf14 ?x_13)
            )
          )
          (exists (?x_32 )
            (and
              (BelowOf8 ?x_32)
              (AboveOf12 ?x_32)
            )
          )
          (exists (?x_27 )
            (and
              (BelowOf4 ?x_27)
              (AboveOf12 ?x_27)
            )
          )
          (exists (?x_33 )
            (and
              (AboveOf10 ?x_33)
              (BelowOf4 ?x_33)
            )
          )
          (exists (?x_22 )
            (and
              (BelowOf6 ?x_22)
              (AboveOf17 ?x_22)
            )
          )
          (exists (?x_11 )
            (and
              (BelowOf8 ?x_11)
              (AboveOf19 ?x_11)
            )
          )
          (exists (?x_2 )
            (and
              (RightOf6 ?x_2)
              (LeftOf2 ?x_2)
            )
          )
          (exists (?x_34 )
            (and
              (BelowOf12 ?x_34)
              (AboveOf15 ?x_34)
            )
          )
          (exists (?x_11 )
            (and
              (BelowOf3 ?x_11)
              (AboveOf15 ?x_11)
            )
          )
          (exists (?x_13 )
            (and
              (LeftOf7 ?x_13)
              (RightOf11 ?x_13)
            )
          )
          (exists (?x_35 )
            (and
              (AboveOf12 ?x_35)
              (BelowOf11 ?x_35)
            )
          )
          (exists (?x_13 )
            (and
              (LeftOf9 ?x_13)
              (RightOf11 ?x_13)
            )
          )
          (exists (?x_2 )
            (and
              (RightOf18 ?x_2)
              (LeftOf3 ?x_2)
            )
          )
          (exists (?x_14 )
            (and
              (RightOf13 ?x_14)
              (LeftOf10 ?x_14)
            )
          )
          (exists (?x_5 )
            (and
              (LeftOf9 ?x_5)
              (RightOf14 ?x_5)
            )
          )
          (exists (?x_20 )
            (and
              (RightOf16 ?x_20)
              (LeftOf5 ?x_20)
            )
          )
          (exists (?x_6 )
            (and
              (RightOf19 ?x_6)
              (LeftOf3 ?x_6)
            )
          )
          (exists (?x_32 )
            (and
              (AboveOf16 ?x_32)
              (BelowOf5 ?x_32)
            )
          )
          (exists (?x_15 )
            (and
              (BelowOf1 ?x_15)
              (AboveOf13 ?x_15)
            )
          )
          (exists (?x_32 )
            (and
              (AboveOf16 ?x_32)
              (BelowOf6 ?x_32)
            )
          )
          (exists (?x_23 )
            (and
              (RightOf10 ?x_23)
              (LeftOf2 ?x_23)
            )
          )
          (exists (?x_33 )
            (and
              (AboveOf10 ?x_33)
              (BelowOf1 ?x_33)
            )
          )
          (exists (?x_28 )
            (and
              (RightOf6 ?x_28)
              (LeftOf2 ?x_28)
            )
          )
          (exists (?x_32 )
            (and
              (AboveOf15 ?x_32)
              (BelowOf4 ?x_32)
            )
          )
          (exists (?x_31 )
            (and
              (RightOf13 ?x_31)
              (LeftOf9 ?x_31)
            )
          )
          (exists (?x_27 )
            (and
              (AboveOf18 ?x_27)
              (BelowOf3 ?x_27)
            )
          )
          (exists (?x_12 )
            (and
              (LeftOf1 ?x_12)
              (RightOf16 ?x_12)
            )
          )
          (exists (?x_34 )
            (and
              (BelowOf8 ?x_34)
              (AboveOf18 ?x_34)
            )
          )
          (exists (?x_18 )
            (and
              (RightOf17 ?x_18)
              (LeftOf4 ?x_18)
            )
          )
          (exists (?x_20 )
            (and
              (LeftOf2 ?x_20)
              (RightOf14 ?x_20)
            )
          )
          (exists (?x_31 )
            (and
              (LeftOf3 ?x_31)
              (RightOf19 ?x_31)
            )
          )
          (exists (?x_25 )
            (and
              (RightOf15 ?x_25)
              (LeftOf11 ?x_25)
            )
          )
          (exists (?x_16 )
            (and
              (RightOf4 ?x_16)
              (LeftOf4 ?x_16)
            )
          )
          (exists (?x_7 )
            (and
              (AboveOf10 ?x_7)
              (BelowOf4 ?x_7)
            )
          )
          (exists (?x_13 )
            (and
              (LeftOf10 ?x_13)
              (RightOf19 ?x_13)
            )
          )
          (exists (?x_24 )
            (and
              (BelowOf13 ?x_24)
              (AboveOf18 ?x_24)
            )
          )
          (exists (?x_4 )
            (and
              (RightOf19 ?x_4)
              (LeftOf4 ?x_4)
            )
          )
          (exists (?x_13 )
            (and
              (RightOf15 ?x_13)
              (LeftOf5 ?x_13)
            )
          )
          (exists (?x_2 )
            (and
              (LeftOf2 ?x_2)
              (RightOf10 ?x_2)
            )
          )
          (exists (?x_32 )
            (and
              (AboveOf12 ?x_32)
              (BelowOf8 ?x_32)
            )
          )
          (exists (?x_35 )
            (and
              (BelowOf5 ?x_35)
              (AboveOf15 ?x_35)
            )
          )
          (exists (?x_25 )
            (and
              (LeftOf6 ?x_25)
              (RightOf19 ?x_25)
            )
          )
          (exists (?x_5 )
            (and
              (RightOf19 ?x_5)
              (LeftOf3 ?x_5)
            )
          )
          (exists (?x_30 )
            (and
              (RightOf19 ?x_30)
              (LeftOf17 ?x_30)
            )
          )
          (exists (?x_8 )
            (and
              (LeftOf2 ?x_8)
              (RightOf4 ?x_8)
            )
          )
          (exists (?x_20 )
            (and
              (RightOf18 ?x_20)
              (LeftOf3 ?x_20)
            )
          )
          (exists (?x_23 )
            (and
              (LeftOf1 ?x_23)
              (RightOf15 ?x_23)
            )
          )
          (exists (?x_29 )
            (and
              (AboveOf9 ?x_29)
              (BelowOf2 ?x_29)
            )
          )
          (exists (?x_34 )
            (and
              (BelowOf11 ?x_34)
              (AboveOf15 ?x_34)
            )
          )
          (exists (?x_29 )
            (and
              (AboveOf18 ?x_29)
              (BelowOf2 ?x_29)
            )
          )
          (exists (?x_27 )
            (and
              (BelowOf2 ?x_27)
              (AboveOf16 ?x_27)
            )
          )
          (exists (?x_31 )
            (and
              (LeftOf3 ?x_31)
              (RightOf14 ?x_31)
            )
          )
          (exists (?x_3 )
            (and
              (AboveOf16 ?x_3)
              (BelowOf10 ?x_3)
            )
          )
          (exists (?x_25 )
            (and
              (LeftOf10 ?x_25)
              (RightOf18 ?x_25)
            )
          )
          (exists (?x_7 )
            (and
              (AboveOf7 ?x_7)
              (BelowOf1 ?x_7)
            )
          )
          (exists (?x_32 )
            (and
              (BelowOf10 ?x_32)
              (AboveOf12 ?x_32)
            )
          )
          (exists (?x_15 )
            (and
              (AboveOf17 ?x_15)
              (BelowOf2 ?x_15)
            )
          )
          (exists (?x_32 )
            (and
              (AboveOf10 ?x_32)
              (BelowOf9 ?x_32)
            )
          )
          (exists (?x_18 )
            (and
              (LeftOf5 ?x_18)
              (RightOf14 ?x_18)
            )
          )
          (exists (?x_31 )
            (and
              (LeftOf6 ?x_31)
              (RightOf15 ?x_31)
            )
          )
          (exists (?x_7 )
            (and
              (AboveOf16 ?x_7)
              (BelowOf7 ?x_7)
            )
          )
          (exists (?x_15 )
            (and
              (BelowOf1 ?x_15)
              (AboveOf17 ?x_15)
            )
          )
          (exists (?x_31 )
            (and
              (LeftOf8 ?x_31)
              (RightOf18 ?x_31)
            )
          )
          (exists (?x_18 )
            (and
              (LeftOf2 ?x_18)
              (RightOf14 ?x_18)
            )
          )
          (exists (?x_20 )
            (and
              (RightOf15 ?x_20)
              (LeftOf8 ?x_20)
            )
          )
          (exists (?x_20 )
            (and
              (RightOf18 ?x_20)
              (LeftOf5 ?x_20)
            )
          )
          (exists (?x_23 )
            (and
              (LeftOf4 ?x_23)
              (RightOf11 ?x_23)
            )
          )
          (exists (?x_10 )
            (and
              (BelowOf3 ?x_10)
              (AboveOf18 ?x_10)
            )
          )
          (exists (?x_7 )
            (and
              (AboveOf7 ?x_7)
              (BelowOf3 ?x_7)
            )
          )
          (exists (?x_37 )
            (and
              (BelowOf2 ?x_37)
              (AboveOf16 ?x_37)
            )
          )
          (exists (?x_2 )
            (and
              (LeftOf6 ?x_2)
              (RightOf17 ?x_2)
            )
          )
          (exists (?x_16 )
            (and
              (RightOf13 ?x_16)
              (LeftOf3 ?x_16)
            )
          )
          (exists (?x_20 )
            (and
              (RightOf18 ?x_20)
              (LeftOf9 ?x_20)
            )
          )
          (exists (?x_5 )
            (and
              (RightOf15 ?x_5)
              (LeftOf6 ?x_5)
            )
          )
          (exists (?x_17 )
            (and
              (RightOf7 ?x_17)
              (LeftOf1 ?x_17)
            )
          )
          (exists (?x_5 )
            (and
              (LeftOf11 ?x_5)
              (RightOf14 ?x_5)
            )
          )
          (exists (?x_23 )
            (and
              (RightOf13 ?x_23)
              (LeftOf8 ?x_23)
            )
          )
          (exists (?x_7 )
            (and
              (BelowOf1 ?x_7)
              (AboveOf12 ?x_7)
            )
          )
          (exists (?x_35 )
            (and
              (BelowOf1 ?x_35)
              (AboveOf18 ?x_35)
            )
          )
          (exists (?x_10 )
            (and
              (AboveOf19 ?x_10)
              (BelowOf4 ?x_10)
            )
          )
          (exists (?x_16 )
            (and
              (RightOf6 ?x_16)
              (LeftOf3 ?x_16)
            )
          )
          (exists (?x_7 )
            (and
              (AboveOf8 ?x_7)
              (BelowOf4 ?x_7)
            )
          )
          (exists (?x_5 )
            (and
              (LeftOf6 ?x_5)
              (RightOf19 ?x_5)
            )
          )
          (exists (?x_2 )
            (and
              (LeftOf6 ?x_2)
              (RightOf15 ?x_2)
            )
          )
          (exists (?x_37 )
            (and
              (AboveOf10 ?x_37)
              (BelowOf4 ?x_37)
            )
          )
          (exists (?x_22 )
            (and
              (BelowOf2 ?x_22)
              (AboveOf13 ?x_22)
            )
          )
          (exists (?x_15 )
            (and
              (BelowOf2 ?x_15)
              (AboveOf19 ?x_15)
            )
          )
          (exists (?x_33 )
            (and
              (AboveOf18 ?x_33)
              (BelowOf1 ?x_33)
            )
          )
          (exists (?x_9 )
            (and
              (AboveOf19 ?x_9)
              (BelowOf3 ?x_9)
            )
          )
          (exists (?x_27 )
            (and
              (AboveOf10 ?x_27)
              (BelowOf3 ?x_27)
            )
          )
          (exists (?x_5 )
            (and
              (LeftOf6 ?x_5)
              (RightOf12 ?x_5)
            )
          )
          (exists (?x_37 )
            (and
              (BelowOf4 ?x_37)
              (AboveOf13 ?x_37)
            )
          )
          (exists (?x_25 )
            (and
              (LeftOf2 ?x_25)
              (RightOf15 ?x_25)
            )
          )
          (exists (?x_12 )
            (and
              (LeftOf4 ?x_12)
              (RightOf17 ?x_12)
            )
          )
          (exists (?x_11 )
            (and
              (AboveOf15 ?x_11)
              (BelowOf2 ?x_11)
            )
          )
          (exists (?x_36 )
            (and
              (AboveOf19 ?x_36)
              (BelowOf6 ?x_36)
            )
          )
          (exists (?x_29 )
            (and
              (BelowOf4 ?x_29)
              (AboveOf11 ?x_29)
            )
          )
          (exists (?x_14 )
            (and
              (LeftOf9 ?x_14)
              (RightOf17 ?x_14)
            )
          )
          (exists (?x_3 )
            (and
              (BelowOf6 ?x_3)
              (AboveOf19 ?x_3)
            )
          )
          (exists (?x_11 )
            (and
              (BelowOf11 ?x_11)
              (AboveOf18 ?x_11)
            )
          )
          (exists (?x_27 )
            (and
              (BelowOf2 ?x_27)
              (AboveOf13 ?x_27)
            )
          )
          (exists (?x_23 )
            (and
              (LeftOf4 ?x_23)
              (RightOf12 ?x_23)
            )
          )
          (exists (?x_33 )
            (and
              (BelowOf2 ?x_33)
              (AboveOf10 ?x_33)
            )
          )
          (exists (?x_12 )
            (and
              (RightOf10 ?x_12)
              (LeftOf7 ?x_12)
            )
          )
          (exists (?x_27 )
            (and
              (AboveOf12 ?x_27)
              (BelowOf5 ?x_27)
            )
          )
          (exists (?x_29 )
            (and
              (AboveOf15 ?x_29)
              (BelowOf6 ?x_29)
            )
          )
          (exists (?x_34 )
            (and
              (BelowOf5 ?x_34)
              (AboveOf17 ?x_34)
            )
          )
          (exists (?x_7 )
            (and
              (AboveOf12 ?x_7)
              (BelowOf3 ?x_7)
            )
          )
          (exists (?x_32 )
            (and
              (BelowOf5 ?x_32)
              (AboveOf12 ?x_32)
            )
          )
          (exists (?x_19 )
            (and
              (AboveOf18 ?x_19)
              (BelowOf3 ?x_19)
            )
          )
          (exists (?x_30 )
            (and
              (LeftOf16 ?x_30)
              (RightOf19 ?x_30)
            )
          )
          (exists (?x_34 )
            (and
              (AboveOf19 ?x_34)
              (BelowOf6 ?x_34)
            )
          )
          (exists (?x_33 )
            (and
              (AboveOf11 ?x_33)
              (BelowOf8 ?x_33)
            )
          )
          (exists (?x_33 )
            (and
              (BelowOf2 ?x_33)
              (AboveOf14 ?x_33)
            )
          )
          (exists (?x_20 )
            (and
              (LeftOf10 ?x_20)
              (RightOf11 ?x_20)
            )
          )
          (exists (?x_14 )
            (and
              (LeftOf13 ?x_14)
              (RightOf15 ?x_14)
            )
          )
          (exists (?x_35 )
            (and
              (BelowOf9 ?x_35)
              (AboveOf15 ?x_35)
            )
          )
          (exists (?x_35 )
            (and
              (BelowOf3 ?x_35)
              (AboveOf16 ?x_35)
            )
          )
          (exists (?x_7 )
            (and
              (AboveOf11 ?x_7)
              (BelowOf1 ?x_7)
            )
          )
          (exists (?x_21 )
            (and
              (RightOf5 ?x_21)
              (LeftOf3 ?x_21)
            )
          )
          (exists (?x_25 )
            (and
              (LeftOf9 ?x_25)
              (RightOf18 ?x_25)
            )
          )
          (exists (?x_5 )
            (and
              (RightOf13 ?x_5)
              (LeftOf4 ?x_5)
            )
          )
          (exists (?x_9 )
            (and
              (BelowOf13 ?x_9)
              (AboveOf18 ?x_9)
            )
          )
          (exists (?x_24 )
            (and
              (AboveOf15 ?x_24)
              (BelowOf8 ?x_24)
            )
          )
          (exists (?x_5 )
            (and
              (RightOf16 ?x_5)
              (LeftOf6 ?x_5)
            )
          )
          (exists (?x_14 )
            (and
              (RightOf19 ?x_14)
              (LeftOf1 ?x_14)
            )
          )
          (exists (?x_37 )
            (and
              (AboveOf17 ?x_37)
              (BelowOf1 ?x_37)
            )
          )
          (exists (?x_35 )
            (and
              (BelowOf5 ?x_35)
              (AboveOf14 ?x_35)
            )
          )
          (exists (?x_21 )
            (and
              (RightOf12 ?x_21)
              (LeftOf5 ?x_21)
            )
          )
          (exists (?x_18 )
            (and
              (LeftOf4 ?x_18)
              (RightOf16 ?x_18)
            )
          )
          (exists (?x_14 )
            (and
              (LeftOf1 ?x_14)
              (RightOf19 ?x_14)
            )
          )
          (exists (?x_9 )
            (and
              (BelowOf11 ?x_9)
              (AboveOf18 ?x_9)
            )
          )
          (exists (?x_15 )
            (and
              (AboveOf9 ?x_15)
              (BelowOf2 ?x_15)
            )
          )
          (exists (?x_26 )
            (and
              (RightOf18 ?x_26)
              (LeftOf11 ?x_26)
            )
          )
          (exists (?x_13 )
            (and
              (RightOf10 ?x_13)
              (LeftOf9 ?x_13)
            )
          )
          (exists (?x_25 )
            (and
              (LeftOf12 ?x_25)
              (RightOf15 ?x_25)
            )
          )
          (exists (?x_18 )
            (and
              (RightOf18 ?x_18)
              (LeftOf14 ?x_18)
            )
          )
          (exists (?x_18 )
            (and
              (RightOf18 ?x_18)
              (LeftOf3 ?x_18)
            )
          )
          (exists (?x_23 )
            (and
              (RightOf14 ?x_23)
              (LeftOf6 ?x_23)
            )
          )
          (exists (?x_5 )
            (and
              (RightOf13 ?x_5)
              (LeftOf7 ?x_5)
            )
          )
          (exists (?x_6 )
            (and
              (RightOf17 ?x_6)
              (LeftOf4 ?x_6)
            )
          )
          (exists (?x_0 )
            (and
              (BelowOf3 ?x_0)
              (AboveOf17 ?x_0)
            )
          )
          (exists (?x_6 )
            (and
              (LeftOf16 ?x_6)
              (RightOf17 ?x_6)
            )
          )
          (exists (?x_4 )
            (and
              (RightOf18 ?x_4)
              (LeftOf6 ?x_4)
            )
          )
          (exists (?x_2 )
            (and
              (LeftOf5 ?x_2)
              (RightOf17 ?x_2)
            )
          )
          (exists (?x_33 )
            (and
              (BelowOf4 ?x_33)
              (AboveOf19 ?x_33)
            )
          )
          (exists (?x_22 )
            (and
              (BelowOf8 ?x_22)
              (AboveOf16 ?x_22)
            )
          )
          (exists (?x_13 )
            (and
              (RightOf18 ?x_13)
              (LeftOf1 ?x_13)
            )
          )
          (exists (?x_34 )
            (and
              (AboveOf14 ?x_34)
              (BelowOf2 ?x_34)
            )
          )
          (exists (?x_35 )
            (and
              (AboveOf12 ?x_35)
              (BelowOf7 ?x_35)
            )
          )
          (exists (?x_23 )
            (and
              (LeftOf5 ?x_23)
              (RightOf12 ?x_23)
            )
          )
          (exists (?x_29 )
            (and
              (AboveOf8 ?x_29)
              (BelowOf8 ?x_29)
            )
          )
          (exists (?x_23 )
            (and
              (LeftOf7 ?x_23)
              (RightOf13 ?x_23)
            )
          )
          (exists (?x_28 )
            (and
              (RightOf3 ?x_28)
              (LeftOf1 ?x_28)
            )
          )
          (exists (?x_7 )
            (and
              (AboveOf8 ?x_7)
              (BelowOf6 ?x_7)
            )
          )
          (exists (?x_15 )
            (and
              (AboveOf3 ?x_15)
              (BelowOf2 ?x_15)
            )
          )
          (exists (?x_33 )
            (and
              (AboveOf10 ?x_33)
              (BelowOf2 ?x_33)
            )
          )
          (exists (?x_8 )
            (and
              (LeftOf2 ?x_8)
              (RightOf13 ?x_8)
            )
          )
          (exists (?x_0 )
            (and
              (AboveOf16 ?x_0)
              (BelowOf2 ?x_0)
            )
          )
          (exists (?x_31 )
            (and
              (LeftOf9 ?x_31)
              (RightOf18 ?x_31)
            )
          )
          (exists (?x_11 )
            (and
              (AboveOf16 ?x_11)
              (BelowOf2 ?x_11)
            )
          )
          (exists (?x_11 )
            (and
              (AboveOf17 ?x_11)
              (BelowOf10 ?x_11)
            )
          )
          (exists (?x_24 )
            (and
              (BelowOf15 ?x_24)
              (AboveOf17 ?x_24)
            )
          )
          (exists (?x_16 )
            (and
              (LeftOf3 ?x_16)
              (RightOf8 ?x_16)
            )
          )
          (exists (?x_3 )
            (and
              (AboveOf19 ?x_3)
              (BelowOf10 ?x_3)
            )
          )
          (exists (?x_22 )
            (and
              (AboveOf17 ?x_22)
              (BelowOf9 ?x_22)
            )
          )
          (exists (?x_7 )
            (and
              (AboveOf8 ?x_7)
              (BelowOf1 ?x_7)
            )
          )
          (exists (?x_11 )
            (and
              (BelowOf6 ?x_11)
              (AboveOf15 ?x_11)
            )
          )
          (exists (?x_32 )
            (and
              (BelowOf9 ?x_32)
              (AboveOf13 ?x_32)
            )
          )
          (exists (?x_7 )
            (and
              (BelowOf7 ?x_7)
              (AboveOf17 ?x_7)
            )
          )
          (exists (?x_24 )
            (and
              (BelowOf4 ?x_24)
              (AboveOf19 ?x_24)
            )
          )
          (exists (?x_27 )
            (and
              (AboveOf10 ?x_27)
              (BelowOf2 ?x_27)
            )
          )
          (exists (?x_10 )
            (and
              (AboveOf17 ?x_10)
              (BelowOf1 ?x_10)
            )
          )
          (exists (?x_34 )
            (and
              (AboveOf14 ?x_34)
              (BelowOf9 ?x_34)
            )
          )
          (exists (?x_14 )
            (and
              (RightOf13 ?x_14)
              (LeftOf12 ?x_14)
            )
          )
          (exists (?x_19 )
            (and
              (BelowOf2 ?x_19)
              (AboveOf18 ?x_19)
            )
          )
          (exists (?x_16 )
            (and
              (LeftOf4 ?x_16)
              (RightOf11 ?x_16)
            )
          )
          (exists (?x_13 )
            (and
              (LeftOf7 ?x_13)
              (RightOf12 ?x_13)
            )
          )
          (exists (?x_29 )
            (and
              (AboveOf14 ?x_29)
              (BelowOf3 ?x_29)
            )
          )
          (exists (?x_0 )
            (and
              (AboveOf4 ?x_0)
              (BelowOf1 ?x_0)
            )
          )
          (exists (?x_37 )
            (and
              (AboveOf12 ?x_37)
              (BelowOf5 ?x_37)
            )
          )
          (exists (?x_29 )
            (and
              (AboveOf17 ?x_29)
              (BelowOf7 ?x_29)
            )
          )
          (exists (?x_5 )
            (and
              (LeftOf9 ?x_5)
              (RightOf15 ?x_5)
            )
          )
          (exists (?x_5 )
            (and
              (LeftOf1 ?x_5)
              (RightOf19 ?x_5)
            )
          )
          (exists (?x_12 )
            (and
              (RightOf11 ?x_12)
              (LeftOf6 ?x_12)
            )
          )
          (exists (?x_33 )
            (and
              (AboveOf14 ?x_33)
              (BelowOf5 ?x_33)
            )
          )
          (exists (?x_1 )
            (and
              (BelowOf1 ?x_1)
              (AboveOf12 ?x_1)
            )
          )
          (exists (?x_19 )
            (and
              (BelowOf16 ?x_19)
              (AboveOf18 ?x_19)
            )
          )
          (exists (?x_5 )
            (and
              (RightOf15 ?x_5)
              (LeftOf4 ?x_5)
            )
          )
          (exists (?x_0 )
            (and
              (AboveOf5 ?x_0)
              (BelowOf2 ?x_0)
            )
          )
          (exists (?x_3 )
            (and
              (AboveOf16 ?x_3)
              (BelowOf2 ?x_3)
            )
          )
          (exists (?x_9 )
            (and
              (BelowOf4 ?x_9)
              (AboveOf18 ?x_9)
            )
          )
          (exists (?x_18 )
            (and
              (RightOf14 ?x_18)
              (LeftOf4 ?x_18)
            )
          )
          (exists (?x_8 )
            (and
              (LeftOf2 ?x_8)
              (RightOf9 ?x_8)
            )
          )
          (exists (?x_12 )
            (and
              (LeftOf7 ?x_12)
              (RightOf19 ?x_12)
            )
          )
          (exists (?x_29 )
            (and
              (BelowOf7 ?x_29)
              (AboveOf15 ?x_29)
            )
          )
          (exists (?x_4 )
            (and
              (RightOf19 ?x_4)
              (LeftOf11 ?x_4)
            )
          )
          (exists (?x_11 )
            (and
              (AboveOf17 ?x_11)
              (BelowOf9 ?x_11)
            )
          )
          (exists (?x_10 )
            (and
              (AboveOf17 ?x_10)
              (BelowOf2 ?x_10)
            )
          )
          (exists (?x_35 )
            (and
              (AboveOf14 ?x_35)
              (BelowOf4 ?x_35)
            )
          )
          (exists (?x_24 )
            (and
              (BelowOf4 ?x_24)
              (AboveOf17 ?x_24)
            )
          )
          (exists (?x_12 )
            (and
              (LeftOf1 ?x_12)
              (RightOf17 ?x_12)
            )
          )
          (exists (?x_9 )
            (and
              (AboveOf19 ?x_9)
              (BelowOf13 ?x_9)
            )
          )
          (exists (?x_24 )
            (and
              (AboveOf15 ?x_24)
              (BelowOf7 ?x_24)
            )
          )
          (exists (?x_12 )
            (and
              (RightOf18 ?x_12)
              (LeftOf5 ?x_12)
            )
          )
          (exists (?x_31 )
            (and
              (RightOf19 ?x_31)
              (LeftOf7 ?x_31)
            )
          )
          (exists (?x_23 )
            (and
              (LeftOf8 ?x_23)
              (RightOf17 ?x_23)
            )
          )
          (exists (?x_26 )
            (and
              (LeftOf15 ?x_26)
              (RightOf19 ?x_26)
            )
          )
          (exists (?x_35 )
            (and
              (BelowOf5 ?x_35)
              (AboveOf16 ?x_35)
            )
          )
          (exists (?x_4 )
            (and
              (RightOf18 ?x_4)
              (LeftOf11 ?x_4)
            )
          )
          (exists (?x_27 )
            (and
              (BelowOf4 ?x_27)
              (AboveOf8 ?x_27)
            )
          )
          (exists (?x_16 )
            (and
              (RightOf15 ?x_16)
              (LeftOf3 ?x_16)
            )
          )
          (exists (?x_11 )
            (and
              (AboveOf12 ?x_11)
              (BelowOf9 ?x_11)
            )
          )
          (exists (?x_22 )
            (and
              (AboveOf18 ?x_22)
              (BelowOf10 ?x_22)
            )
          )
          (exists (?x_22 )
            (and
              (BelowOf12 ?x_22)
              (AboveOf15 ?x_22)
            )
          )
          (exists (?x_10 )
            (and
              (AboveOf11 ?x_10)
              (BelowOf2 ?x_10)
            )
          )
          (exists (?x_32 )
            (and
              (BelowOf9 ?x_32)
              (AboveOf17 ?x_32)
            )
          )
          (exists (?x_19 )
            (and
              (BelowOf11 ?x_19)
              (AboveOf18 ?x_19)
            )
          )
          (exists (?x_12 )
            (and
              (LeftOf4 ?x_12)
              (RightOf16 ?x_12)
            )
          )
          (exists (?x_3 )
            (and
              (AboveOf17 ?x_3)
              (BelowOf11 ?x_3)
            )
          )
          (exists (?x_37 )
            (and
              (AboveOf19 ?x_37)
              (BelowOf2 ?x_37)
            )
          )
          (exists (?x_20 )
            (and
              (LeftOf11 ?x_20)
              (RightOf14 ?x_20)
            )
          )
          (exists (?x_31 )
            (and
              (LeftOf5 ?x_31)
              (RightOf16 ?x_31)
            )
          )
          (exists (?x_31 )
            (and
              (RightOf13 ?x_31)
              (LeftOf4 ?x_31)
            )
          )
          (exists (?x_28 )
            (and
              (LeftOf2 ?x_28)
              (RightOf18 ?x_28)
            )
          )
          (exists (?x_0 )
            (and
              (AboveOf7 ?x_0)
              (BelowOf2 ?x_0)
            )
          )
          (exists (?x_33 )
            (and
              (BelowOf7 ?x_33)
              (AboveOf17 ?x_33)
            )
          )
          (exists (?x_21 )
            (and
              (RightOf17 ?x_21)
              (LeftOf3 ?x_21)
            )
          )
          (exists (?x_23 )
            (and
              (RightOf15 ?x_23)
              (LeftOf2 ?x_23)
            )
          )
          (exists (?x_13 )
            (and
              (RightOf19 ?x_13)
              (LeftOf5 ?x_13)
            )
          )
          (exists (?x_21 )
            (and
              (LeftOf4 ?x_21)
              (RightOf17 ?x_21)
            )
          )
          (exists (?x_15 )
            (and
              (AboveOf10 ?x_15)
              (BelowOf1 ?x_15)
            )
          )
          (exists (?x_36 )
            (and
              (AboveOf19 ?x_36)
              (BelowOf15 ?x_36)
            )
          )
          (exists (?x_21 )
            (and
              (RightOf5 ?x_21)
              (LeftOf4 ?x_21)
            )
          )
          (exists (?x_9 )
            (and
              (AboveOf17 ?x_9)
              (BelowOf4 ?x_9)
            )
          )
          (exists (?x_30 )
            (and
              (RightOf19 ?x_30)
              (LeftOf11 ?x_30)
            )
          )
          (exists (?x_24 )
            (and
              (BelowOf1 ?x_24)
              (AboveOf17 ?x_24)
            )
          )
          (exists (?x_21 )
            (and
              (LeftOf2 ?x_21)
              (RightOf8 ?x_21)
            )
          )
          (exists (?x_20 )
            (and
              (RightOf14 ?x_20)
              (LeftOf10 ?x_20)
            )
          )
          (exists (?x_33 )
            (and
              (BelowOf2 ?x_33)
              (AboveOf9 ?x_33)
            )
          )
          (exists (?x_35 )
            (and
              (BelowOf1 ?x_35)
              (AboveOf15 ?x_35)
            )
          )
          (exists (?x_34 )
            (and
              (AboveOf14 ?x_34)
              (BelowOf13 ?x_34)
            )
          )
          (exists (?x_37 )
            (and
              (AboveOf11 ?x_37)
              (BelowOf1 ?x_37)
            )
          )
          (exists (?x_37 )
            (and
              (AboveOf6 ?x_37)
              (BelowOf4 ?x_37)
            )
          )
          (exists (?x_12 )
            (and
              (RightOf10 ?x_12)
              (LeftOf5 ?x_12)
            )
          )
          (exists (?x_35 )
            (and
              (AboveOf17 ?x_35)
              (BelowOf3 ?x_35)
            )
          )
          (exists (?x_7 )
            (and
              (AboveOf7 ?x_7)
              (BelowOf7 ?x_7)
            )
          )
          (exists (?x_37 )
            (and
              (AboveOf7 ?x_37)
              (BelowOf1 ?x_37)
            )
          )
          (exists (?x_0 )
            (and
              (BelowOf3 ?x_0)
              (AboveOf18 ?x_0)
            )
          )
          (exists (?x_26 )
            (and
              (LeftOf18 ?x_26)
              (RightOf18 ?x_26)
            )
          )
          (exists (?x_31 )
            (and
              (LeftOf4 ?x_31)
              (RightOf10 ?x_31)
            )
          )
          (exists (?x_32 )
            (and
              (BelowOf1 ?x_32)
              (AboveOf13 ?x_32)
            )
          )
          (exists (?x_3 )
            (and
              (BelowOf12 ?x_3)
              (AboveOf18 ?x_3)
            )
          )
          (exists (?x_11 )
            (and
              (BelowOf1 ?x_11)
              (AboveOf16 ?x_11)
            )
          )
          (exists (?x_32 )
            (and
              (AboveOf17 ?x_32)
              (BelowOf8 ?x_32)
            )
          )
          (exists (?x_12 )
            (and
              (LeftOf1 ?x_12)
              (RightOf12 ?x_12)
            )
          )
          (exists (?x_37 )
            (and
              (AboveOf7 ?x_37)
              (BelowOf4 ?x_37)
            )
          )
          (exists (?x_35 )
            (and
              (BelowOf2 ?x_35)
              (AboveOf15 ?x_35)
            )
          )
          (exists (?x_12 )
            (and
              (LeftOf4 ?x_12)
              (RightOf18 ?x_12)
            )
          )
          (exists (?x_25 )
            (and
              (LeftOf14 ?x_25)
              (RightOf18 ?x_25)
            )
          )
          (exists (?x_2 )
            (and
              (LeftOf5 ?x_2)
              (RightOf18 ?x_2)
            )
          )
          (exists (?x_7 )
            (and
              (AboveOf18 ?x_7)
              (BelowOf5 ?x_7)
            )
          )
          (exists (?x_16 )
            (and
              (RightOf18 ?x_16)
              (LeftOf4 ?x_16)
            )
          )
          (exists (?x_8 )
            (and
              (RightOf16 ?x_8)
              (LeftOf2 ?x_8)
            )
          )
          (exists (?x_16 )
            (and
              (RightOf15 ?x_16)
              (LeftOf1 ?x_16)
            )
          )
          (exists (?x_20 )
            (and
              (LeftOf2 ?x_20)
              (RightOf11 ?x_20)
            )
          )
          (exists (?x_21 )
            (and
              (LeftOf1 ?x_21)
              (RightOf7 ?x_21)
            )
          )
          (exists (?x_35 )
            (and
              (BelowOf8 ?x_35)
              (AboveOf19 ?x_35)
            )
          )
          (exists (?x_21 )
            (and
              (RightOf13 ?x_21)
              (LeftOf1 ?x_21)
            )
          )
          (exists (?x_13 )
            (and
              (LeftOf1 ?x_13)
              (RightOf19 ?x_13)
            )
          )
          (exists (?x_25 )
            (and
              (LeftOf3 ?x_25)
              (RightOf17 ?x_25)
            )
          )
          (exists (?x_27 )
            (and
              (BelowOf1 ?x_27)
              (AboveOf14 ?x_27)
            )
          )
          (exists (?x_16 )
            (and
              (LeftOf2 ?x_16)
              (RightOf8 ?x_16)
            )
          )
          (exists (?x_25 )
            (and
              (RightOf17 ?x_25)
              (LeftOf11 ?x_25)
            )
          )
          (exists (?x_4 )
            (and
              (RightOf18 ?x_4)
              (LeftOf17 ?x_4)
            )
          )
          (exists (?x_13 )
            (and
              (LeftOf8 ?x_13)
              (RightOf12 ?x_13)
            )
          )
          (exists (?x_18 )
            (and
              (RightOf14 ?x_18)
              (LeftOf3 ?x_18)
            )
          )
          (exists (?x_11 )
            (and
              (AboveOf14 ?x_11)
              (BelowOf2 ?x_11)
            )
          )
          (exists (?x_33 )
            (and
              (BelowOf1 ?x_33)
              (AboveOf11 ?x_33)
            )
          )
          (exists (?x_27 )
            (and
              (BelowOf2 ?x_27)
              (AboveOf8 ?x_27)
            )
          )
          (exists (?x_29 )
            (and
              (AboveOf17 ?x_29)
              (BelowOf8 ?x_29)
            )
          )
          (exists (?x_11 )
            (and
              (AboveOf13 ?x_11)
              (BelowOf4 ?x_11)
            )
          )
          (exists (?x_27 )
            (and
              (AboveOf16 ?x_27)
              (BelowOf5 ?x_27)
            )
          )
          (exists (?x_26 )
            (and
              (RightOf19 ?x_26)
              (LeftOf14 ?x_26)
            )
          )
          (exists (?x_27 )
            (and
              (BelowOf4 ?x_27)
              (AboveOf6 ?x_27)
            )
          )
          (exists (?x_32 )
            (and
              (AboveOf11 ?x_32)
              (BelowOf5 ?x_32)
            )
          )
          (exists (?x_13 )
            (and
              (LeftOf7 ?x_13)
              (RightOf14 ?x_13)
            )
          )
          (exists (?x_5 )
            (and
              (RightOf17 ?x_5)
              (LeftOf3 ?x_5)
            )
          )
          (exists (?x_26 )
            (and
              (LeftOf16 ?x_26)
              (RightOf18 ?x_26)
            )
          )
          (exists (?x_6 )
            (and
              (RightOf17 ?x_6)
              (LeftOf1 ?x_6)
            )
          )
          (exists (?x_27 )
            (and
              (AboveOf6 ?x_27)
              (BelowOf5 ?x_27)
            )
          )
          (exists (?x_23 )
            (and
              (RightOf10 ?x_23)
              (LeftOf7 ?x_23)
            )
          )
          (exists (?x_15 )
            (and
              (AboveOf12 ?x_15)
              (BelowOf1 ?x_15)
            )
          )
          (exists (?x_22 )
            (and
              (BelowOf3 ?x_22)
              (AboveOf13 ?x_22)
            )
          )
          (exists (?x_33 )
            (and
              (AboveOf9 ?x_33)
              (BelowOf9 ?x_33)
            )
          )
          (exists (?x_10 )
            (and
              (BelowOf4 ?x_10)
              (AboveOf7 ?x_10)
            )
          )
          (exists (?x_2 )
            (and
              (LeftOf6 ?x_2)
              (RightOf14 ?x_2)
            )
          )
          (exists (?x_34 )
            (and
              (BelowOf1 ?x_34)
              (AboveOf16 ?x_34)
            )
          )
          (exists (?x_9 )
            (and
              (BelowOf16 ?x_9)
              (AboveOf18 ?x_9)
            )
          )
          (exists (?x_11 )
            (and
              (AboveOf17 ?x_11)
              (BelowOf4 ?x_11)
            )
          )
          (exists (?x_20 )
            (and
              (LeftOf4 ?x_20)
              (RightOf19 ?x_20)
            )
          )
          (exists (?x_33 )
            (and
              (BelowOf2 ?x_33)
              (AboveOf11 ?x_33)
            )
          )
          (exists (?x_22 )
            (and
              (AboveOf18 ?x_22)
              (BelowOf13 ?x_22)
            )
          )
          (exists (?x_19 )
            (and
              (BelowOf14 ?x_19)
              (AboveOf18 ?x_19)
            )
          )
          (exists (?x_2 )
            (and
              (LeftOf1 ?x_2)
              (RightOf13 ?x_2)
            )
          )
          (exists (?x_21 )
            (and
              (LeftOf5 ?x_21)
              (RightOf19 ?x_21)
            )
          )
          (exists (?x_7 )
            (and
              (AboveOf16 ?x_7)
              (BelowOf4 ?x_7)
            )
          )
          (exists (?x_26 )
            (and
              (LeftOf18 ?x_26)
              (RightOf19 ?x_26)
            )
          )
          (exists (?x_2 )
            (and
              (RightOf13 ?x_2)
              (LeftOf2 ?x_2)
            )
          )
          (exists (?x_35 )
            (and
              (AboveOf17 ?x_35)
              (BelowOf6 ?x_35)
            )
          )
          (exists (?x_22 )
            (and
              (BelowOf13 ?x_22)
              (AboveOf16 ?x_22)
            )
          )
          (exists (?x_7 )
            (and
              (AboveOf9 ?x_7)
              (BelowOf1 ?x_7)
            )
          )
          (exists (?x_37 )
            (and
              (AboveOf5 ?x_37)
              (BelowOf5 ?x_37)
            )
          )
          (exists (?x_1 )
            (and
              (AboveOf2 ?x_1)
              (BelowOf1 ?x_1)
            )
          )
          (exists (?x_6 )
            (and
              (RightOf18 ?x_6)
              (LeftOf13 ?x_6)
            )
          )
          (exists (?x_9 )
            (and
              (AboveOf19 ?x_9)
              (BelowOf15 ?x_9)
            )
          )
          (exists (?x_18 )
            (and
              (LeftOf11 ?x_18)
              (RightOf19 ?x_18)
            )
          )
          (exists (?x_2 )
            (and
              (LeftOf6 ?x_2)
              (RightOf12 ?x_2)
            )
          )
          (exists (?x_10 )
            (and
              (AboveOf8 ?x_10)
              (BelowOf1 ?x_10)
            )
          )
          (exists (?x_14 )
            (and
              (LeftOf11 ?x_14)
              (RightOf16 ?x_14)
            )
          )
          (exists (?x_34 )
            (and
              (AboveOf19 ?x_34)
              (BelowOf9 ?x_34)
            )
          )
          (exists (?x_28 )
            (and
              (LeftOf1 ?x_28)
              (RightOf8 ?x_28)
            )
          )
          (exists (?x_33 )
            (and
              (AboveOf18 ?x_33)
              (BelowOf7 ?x_33)
            )
          )
          (exists (?x_34 )
            (and
              (BelowOf12 ?x_34)
              (AboveOf16 ?x_34)
            )
          )
          (exists (?x_5 )
            (and
              (LeftOf6 ?x_5)
              (RightOf14 ?x_5)
            )
          )
          (exists (?x_32 )
            (and
              (BelowOf10 ?x_32)
              (AboveOf13 ?x_32)
            )
          )
          (exists (?x_7 )
            (and
              (AboveOf16 ?x_7)
              (BelowOf6 ?x_7)
            )
          )
          (exists (?x_1 )
            (and
              (BelowOf1 ?x_1)
              (AboveOf14 ?x_1)
            )
          )
          (exists (?x_23 )
            (and
              (LeftOf1 ?x_23)
              (RightOf8 ?x_23)
            )
          )
          (exists (?x_13 )
            (and
              (RightOf18 ?x_13)
              (LeftOf8 ?x_13)
            )
          )
          (exists (?x_7 )
            (and
              (BelowOf7 ?x_7)
              (AboveOf12 ?x_7)
            )
          )
          (exists (?x_5 )
            (and
              (RightOf16 ?x_5)
              (LeftOf5 ?x_5)
            )
          )
          (exists (?x_9 )
            (and
              (AboveOf17 ?x_9)
              (BelowOf3 ?x_9)
            )
          )
          (exists (?x_12 )
            (and
              (LeftOf3 ?x_12)
              (RightOf17 ?x_12)
            )
          )
          (exists (?x_7 )
            (and
              (AboveOf12 ?x_7)
              (BelowOf6 ?x_7)
            )
          )
          (exists (?x_14 )
            (and
              (LeftOf4 ?x_14)
              (RightOf15 ?x_14)
            )
          )
          (exists (?x_36 )
            (and
              (AboveOf19 ?x_36)
              (BelowOf2 ?x_36)
            )
          )
          (exists (?x_32 )
            (and
              (AboveOf10 ?x_32)
              (BelowOf10 ?x_32)
            )
          )
          (exists (?x_29 )
            (and
              (AboveOf13 ?x_29)
              (BelowOf1 ?x_29)
            )
          )
          (exists (?x_11 )
            (and
              (BelowOf11 ?x_11)
              (AboveOf14 ?x_11)
            )
          )
          (exists (?x_0 )
            (and
              (BelowOf3 ?x_0)
              (AboveOf7 ?x_0)
            )
          )
          (exists (?x_31 )
            (and
              (LeftOf7 ?x_31)
              (RightOf18 ?x_31)
            )
          )
          (exists (?x_22 )
            (and
              (AboveOf14 ?x_22)
              (BelowOf5 ?x_22)
            )
          )
          (exists (?x_2 )
            (and
              (RightOf7 ?x_2)
              (LeftOf3 ?x_2)
            )
          )
          (exists (?x_36 )
            (and
              (AboveOf19 ?x_36)
              (BelowOf18 ?x_36)
            )
          )
          (exists (?x_7 )
            (and
              (AboveOf14 ?x_7)
              (BelowOf5 ?x_7)
            )
          )
          (exists (?x_24 )
            (and
              (AboveOf15 ?x_24)
              (BelowOf15 ?x_24)
            )
          )
          (exists (?x_13 )
            (and
              (RightOf10 ?x_13)
              (LeftOf2 ?x_13)
            )
          )
          (exists (?x_12 )
            (and
              (LeftOf1 ?x_12)
              (RightOf18 ?x_12)
            )
          )
          (exists (?x_22 )
            (and
              (AboveOf18 ?x_22)
              (BelowOf2 ?x_22)
            )
          )
          (exists (?x_24 )
            (and
              (AboveOf18 ?x_24)
              (BelowOf12 ?x_24)
            )
          )
          (exists (?x_11 )
            (and
              (BelowOf5 ?x_11)
              (AboveOf15 ?x_11)
            )
          )
          (exists (?x_33 )
            (and
              (BelowOf2 ?x_33)
              (AboveOf13 ?x_33)
            )
          )
          (exists (?x_34 )
            (and
              (AboveOf17 ?x_34)
              (BelowOf9 ?x_34)
            )
          )
          (exists (?x_32 )
            (and
              (AboveOf18 ?x_32)
              (BelowOf4 ?x_32)
            )
          )
          (exists (?x_37 )
            (and
              (BelowOf3 ?x_37)
              (AboveOf19 ?x_37)
            )
          )
          (exists (?x_18 )
            (and
              (LeftOf10 ?x_18)
              (RightOf14 ?x_18)
            )
          )
          (exists (?x_2 )
            (and
              (LeftOf6 ?x_2)
              (RightOf10 ?x_2)
            )
          )
          (exists (?x_4 )
            (and
              (RightOf17 ?x_4)
              (LeftOf12 ?x_4)
            )
          )
          (exists (?x_3 )
            (and
              (BelowOf15 ?x_3)
              (AboveOf17 ?x_3)
            )
          )
          (exists (?x_24 )
            (and
              (BelowOf4 ?x_24)
              (AboveOf18 ?x_24)
            )
          )
          (exists (?x_21 )
            (and
              (RightOf17 ?x_21)
              (LeftOf5 ?x_21)
            )
          )
          (exists (?x_25 )
            (and
              (RightOf17 ?x_25)
              (LeftOf13 ?x_25)
            )
          )
          (exists (?x_3 )
            (and
              (BelowOf12 ?x_3)
              (AboveOf16 ?x_3)
            )
          )
          (exists (?x_2 )
            (and
              (LeftOf1 ?x_2)
              (RightOf15 ?x_2)
            )
          )
          (exists (?x_37 )
            (and
              (AboveOf19 ?x_37)
              (BelowOf4 ?x_37)
            )
          )
          (exists (?x_14 )
            (and
              (LeftOf12 ?x_14)
              (RightOf17 ?x_14)
            )
          )
          (exists (?x_18 )
            (and
              (RightOf16 ?x_18)
              (LeftOf3 ?x_18)
            )
          )
          (exists (?x_29 )
            (and
              (BelowOf3 ?x_29)
              (AboveOf18 ?x_29)
            )
          )
          (exists (?x_10 )
            (and
              (AboveOf9 ?x_10)
              (BelowOf1 ?x_10)
            )
          )
          (exists (?x_29 )
            (and
              (AboveOf10 ?x_29)
              (BelowOf2 ?x_29)
            )
          )
          (exists (?x_25 )
            (and
              (RightOf19 ?x_25)
              (LeftOf14 ?x_25)
            )
          )
          (exists (?x_13 )
            (and
              (RightOf15 ?x_13)
              (LeftOf8 ?x_13)
            )
          )
          (exists (?x_26 )
            (and
              (RightOf18 ?x_26)
              (LeftOf9 ?x_26)
            )
          )
          (exists (?x_33 )
            (and
              (AboveOf10 ?x_33)
              (BelowOf6 ?x_33)
            )
          )
          (exists (?x_8 )
            (and
              (LeftOf2 ?x_8)
              (RightOf8 ?x_8)
            )
          )
          (exists (?x_9 )
            (and
              (AboveOf19 ?x_9)
              (BelowOf12 ?x_9)
            )
          )
          (exists (?x_12 )
            (and
              (LeftOf4 ?x_12)
              (RightOf12 ?x_12)
            )
          )
          (exists (?x_37 )
            (and
              (BelowOf1 ?x_37)
              (AboveOf13 ?x_37)
            )
          )
          (exists (?x_29 )
            (and
              (BelowOf7 ?x_29)
              (AboveOf18 ?x_29)
            )
          )
          (exists (?x_24 )
            (and
              (BelowOf10 ?x_24)
              (AboveOf17 ?x_24)
            )
          )
          (exists (?x_24 )
            (and
              (AboveOf17 ?x_24)
              (BelowOf12 ?x_24)
            )
          )
          (exists (?x_22 )
            (and
              (AboveOf15 ?x_22)
              (BelowOf9 ?x_22)
            )
          )
          (exists (?x_16 )
            (and
              (LeftOf3 ?x_16)
              (RightOf11 ?x_16)
            )
          )
          (exists (?x_32 )
            (and
              (AboveOf16 ?x_32)
              (BelowOf3 ?x_32)
            )
          )
          (exists (?x_7 )
            (and
              (BelowOf7 ?x_7)
              (AboveOf11 ?x_7)
            )
          )
          (exists (?x_21 )
            (and
              (RightOf15 ?x_21)
              (LeftOf5 ?x_21)
            )
          )
          (exists (?x_12 )
            (and
              (LeftOf7 ?x_12)
              (RightOf12 ?x_12)
            )
          )
          (exists (?x_20 )
            (and
              (RightOf12 ?x_20)
              (LeftOf8 ?x_20)
            )
          )
          (exists (?x_19 )
            (and
              (BelowOf14 ?x_19)
              (AboveOf19 ?x_19)
            )
          )
          (exists (?x_3 )
            (and
              (BelowOf5 ?x_3)
              (AboveOf18 ?x_3)
            )
          )
          (exists (?x_16 )
            (and
              (LeftOf3 ?x_16)
              (RightOf5 ?x_16)
            )
          )
          (exists (?x_13 )
            (and
              (RightOf15 ?x_13)
              (LeftOf1 ?x_13)
            )
          )
          (exists (?x_35 )
            (and
              (BelowOf11 ?x_35)
              (AboveOf19 ?x_35)
            )
          )
          (exists (?x_8 )
            (and
              (LeftOf2 ?x_8)
              (RightOf11 ?x_8)
            )
          )
          (exists (?x_33 )
            (and
              (BelowOf7 ?x_33)
              (AboveOf9 ?x_33)
            )
          )
          (exists (?x_2 )
            (and
              (LeftOf1 ?x_2)
              (RightOf8 ?x_2)
            )
          )
          (exists (?x_21 )
            (and
              (LeftOf1 ?x_21)
              (RightOf18 ?x_21)
            )
          )
          (exists (?x_33 )
            (and
              (AboveOf19 ?x_33)
              (BelowOf3 ?x_33)
            )
          )
          (exists (?x_26 )
            (and
              (LeftOf13 ?x_26)
              (RightOf18 ?x_26)
            )
          )
          (exists (?x_16 )
            (and
              (LeftOf2 ?x_16)
              (RightOf6 ?x_16)
            )
          )
          (exists (?x_27 )
            (and
              (AboveOf19 ?x_27)
              (BelowOf3 ?x_27)
            )
          )
          (exists (?x_34 )
            (and
              (AboveOf18 ?x_34)
              (BelowOf2 ?x_34)
            )
          )
          (exists (?x_5 )
            (and
              (LeftOf9 ?x_5)
              (RightOf13 ?x_5)
            )
          )
          (exists (?x_33 )
            (and
              (AboveOf13 ?x_33)
              (BelowOf3 ?x_33)
            )
          )
          (exists (?x_27 )
            (and
              (AboveOf18 ?x_27)
              (BelowOf2 ?x_27)
            )
          )
          (exists (?x_13 )
            (and
              (LeftOf10 ?x_13)
              (RightOf11 ?x_13)
            )
          )
          (exists (?x_16 )
            (and
              (LeftOf2 ?x_16)
              (RightOf17 ?x_16)
            )
          )
          (exists (?x_28 )
            (and
              (RightOf6 ?x_28)
              (LeftOf1 ?x_28)
            )
          )
          (exists (?x_6 )
            (and
              (RightOf18 ?x_6)
              (LeftOf8 ?x_6)
            )
          )
          (exists (?x_34 )
            (and
              (BelowOf9 ?x_34)
              (AboveOf18 ?x_34)
            )
          )
          (exists (?x_22 )
            (and
              (BelowOf1 ?x_22)
              (AboveOf16 ?x_22)
            )
          )
          (exists (?x_33 )
            (and
              (BelowOf6 ?x_33)
              (AboveOf17 ?x_33)
            )
          )
          (exists (?x_11 )
            (and
              (BelowOf9 ?x_11)
              (AboveOf18 ?x_11)
            )
          )
          (exists (?x_3 )
            (and
              (AboveOf19 ?x_3)
              (BelowOf1 ?x_3)
            )
          )
          (exists (?x_1 )
            (and
              (BelowOf1 ?x_1)
              (AboveOf13 ?x_1)
            )
          )
          (exists (?x_0 )
            (and
              (BelowOf2 ?x_0)
              (AboveOf10 ?x_0)
            )
          )
          (exists (?x_34 )
            (and
              (BelowOf11 ?x_34)
              (AboveOf18 ?x_34)
            )
          )
          (exists (?x_12 )
            (and
              (LeftOf2 ?x_12)
              (RightOf17 ?x_12)
            )
          )
          (exists (?x_24 )
            (and
              (BelowOf2 ?x_24)
              (AboveOf17 ?x_24)
            )
          )
          (exists (?x_24 )
            (and
              (BelowOf5 ?x_24)
              (AboveOf19 ?x_24)
            )
          )
          (exists (?x_22 )
            (and
              (BelowOf2 ?x_22)
              (AboveOf16 ?x_22)
            )
          )
          (exists (?x_20 )
            (and
              (LeftOf10 ?x_20)
              (RightOf19 ?x_20)
            )
          )
          (exists (?x_24 )
            (and
              (AboveOf16 ?x_24)
              (BelowOf7 ?x_24)
            )
          )
          (exists (?x_22 )
            (and
              (BelowOf4 ?x_22)
              (AboveOf16 ?x_22)
            )
          )
          (exists (?x_34 )
            (and
              (AboveOf17 ?x_34)
              (BelowOf14 ?x_34)
            )
          )
          (exists (?x_23 )
            (and
              (LeftOf7 ?x_23)
              (RightOf9 ?x_23)
            )
          )
          (exists (?x_25 )
            (and
              (RightOf17 ?x_25)
              (LeftOf6 ?x_25)
            )
          )
          (exists (?x_20 )
            (and
              (RightOf13 ?x_20)
              (LeftOf9 ?x_20)
            )
          )
          (exists (?x_25 )
            (and
              (LeftOf2 ?x_25)
              (RightOf18 ?x_25)
            )
          )
          (exists (?x_7 )
            (and
              (BelowOf2 ?x_7)
              (AboveOf10 ?x_7)
            )
          )
          (exists (?x_24 )
            (and
              (AboveOf16 ?x_24)
              (BelowOf6 ?x_24)
            )
          )
          (exists (?x_23 )
            (and
              (RightOf15 ?x_23)
              (LeftOf8 ?x_23)
            )
          )
          (exists (?x_7 )
            (and
              (BelowOf1 ?x_7)
              (AboveOf14 ?x_7)
            )
          )
          (exists (?x_26 )
            (and
              (RightOf19 ?x_26)
              (LeftOf6 ?x_26)
            )
          )
          (exists (?x_36 )
            (and
              (AboveOf19 ?x_36)
              (BelowOf3 ?x_36)
            )
          )
          (exists (?x_8 )
            (and
              (LeftOf3 ?x_8)
              (RightOf14 ?x_8)
            )
          )
          (exists (?x_13 )
            (and
              (LeftOf10 ?x_13)
              (RightOf14 ?x_13)
            )
          )
          (exists (?x_10 )
            (and
              (AboveOf14 ?x_10)
              (BelowOf1 ?x_10)
            )
          )
          (exists (?x_7 )
            (and
              (AboveOf8 ?x_7)
              (BelowOf7 ?x_7)
            )
          )
          (exists (?x_16 )
            (and
              (RightOf4 ?x_16)
              (LeftOf1 ?x_16)
            )
          )
          (exists (?x_18 )
            (and
              (LeftOf9 ?x_18)
              (RightOf16 ?x_18)
            )
          )
          (exists (?x_33 )
            (and
              (BelowOf9 ?x_33)
              (AboveOf11 ?x_33)
            )
          )
          (exists (?x_35 )
            (and
              (AboveOf19 ?x_35)
              (BelowOf2 ?x_35)
            )
          )
          (exists (?x_14 )
            (and
              (LeftOf1 ?x_14)
              (RightOf13 ?x_14)
            )
          )
          (exists (?x_32 )
            (and
              (AboveOf18 ?x_32)
              (BelowOf3 ?x_32)
            )
          )
          (exists (?x_18 )
            (and
              (LeftOf8 ?x_18)
              (RightOf16 ?x_18)
            )
          )
          (exists (?x_34 )
            (and
              (BelowOf7 ?x_34)
              (AboveOf18 ?x_34)
            )
          )
          (exists (?x_21 )
            (and
              (LeftOf2 ?x_21)
              (RightOf14 ?x_21)
            )
          )
          (exists (?x_13 )
            (and
              (RightOf14 ?x_13)
              (LeftOf6 ?x_13)
            )
          )
          (exists (?x_24 )
            (and
              (AboveOf18 ?x_24)
              (BelowOf2 ?x_24)
            )
          )
          (exists (?x_4 )
            (and
              (RightOf18 ?x_4)
              (LeftOf7 ?x_4)
            )
          )
          (exists (?x_4 )
            (and
              (LeftOf8 ?x_4)
              (RightOf18 ?x_4)
            )
          )
          (exists (?x_18 )
            (and
              (LeftOf9 ?x_18)
              (RightOf19 ?x_18)
            )
          )
          (exists (?x_32 )
            (and
              (BelowOf3 ?x_32)
              (AboveOf19 ?x_32)
            )
          )
          (exists (?x_10 )
            (and
              (AboveOf13 ?x_10)
              (BelowOf2 ?x_10)
            )
          )
          (exists (?x_13 )
            (and
              (LeftOf1 ?x_13)
              (RightOf12 ?x_13)
            )
          )
          (exists (?x_37 )
            (and
              (BelowOf5 ?x_37)
              (AboveOf15 ?x_37)
            )
          )
          (exists (?x_23 )
            (and
              (LeftOf2 ?x_23)
              (RightOf19 ?x_23)
            )
          )
          (exists (?x_14 )
            (and
              (RightOf15 ?x_14)
              (LeftOf7 ?x_14)
            )
          )
          (exists (?x_16 )
            (and
              (RightOf10 ?x_16)
              (LeftOf1 ?x_16)
            )
          )
          (exists (?x_12 )
            (and
              (RightOf17 ?x_12)
              (LeftOf3 ?x_12)
            )
          )
          (exists (?x_5 )
            (and
              (RightOf15 ?x_5)
              (LeftOf1 ?x_5)
            )
          )
          (exists (?x_32 )
            (and
              (BelowOf7 ?x_32)
              (AboveOf10 ?x_32)
            )
          )
          (exists (?x_17 )
            (and
              (RightOf15 ?x_17)
              (LeftOf1 ?x_17)
            )
          )
          (exists (?x_2 )
            (and
              (LeftOf2 ?x_2)
              (RightOf19 ?x_2)
            )
          )
          (exists (?x_7 )
            (and
              (AboveOf9 ?x_7)
              (BelowOf5 ?x_7)
            )
          )
          (exists (?x_37 )
            (and
              (AboveOf12 ?x_37)
              (BelowOf4 ?x_37)
            )
          )
          (exists (?x_3 )
            (and
              (AboveOf17 ?x_3)
              (BelowOf8 ?x_3)
            )
          )
          (exists (?x_1 )
            (and
              (BelowOf1 ?x_1)
              (AboveOf5 ?x_1)
            )
          )
          (exists (?x_23 )
            (and
              (LeftOf2 ?x_23)
              (RightOf17 ?x_23)
            )
          )
          (exists (?x_21 )
            (and
              (RightOf5 ?x_21)
              (LeftOf5 ?x_21)
            )
          )
          (exists (?x_10 )
            (and
              (AboveOf11 ?x_10)
              (BelowOf1 ?x_10)
            )
          )
          (exists (?x_11 )
            (and
              (BelowOf5 ?x_11)
              (AboveOf19 ?x_11)
            )
          )
          (exists (?x_20 )
            (and
              (RightOf12 ?x_20)
              (LeftOf10 ?x_20)
            )
          )
          (exists (?x_20 )
            (and
              (LeftOf2 ?x_20)
              (RightOf17 ?x_20)
            )
          )
          (exists (?x_9 )
            (and
              (AboveOf19 ?x_9)
              (BelowOf6 ?x_9)
            )
          )
          (exists (?x_5 )
            (and
              (LeftOf4 ?x_5)
              (RightOf14 ?x_5)
            )
          )
          (exists (?x_6 )
            (and
              (LeftOf5 ?x_6)
              (RightOf19 ?x_6)
            )
          )
          (exists (?x_22 )
            (and
              (BelowOf5 ?x_22)
              (AboveOf16 ?x_22)
            )
          )
          (exists (?x_14 )
            (and
              (LeftOf1 ?x_14)
              (RightOf18 ?x_14)
            )
          )
          (exists (?x_3 )
            (and
              (AboveOf17 ?x_3)
              (BelowOf13 ?x_3)
            )
          )
          (exists (?x_32 )
            (and
              (BelowOf2 ?x_32)
              (AboveOf18 ?x_32)
            )
          )
          (exists (?x_35 )
            (and
              (AboveOf13 ?x_35)
              (BelowOf9 ?x_35)
            )
          )
          (exists (?x_25 )
            (and
              (RightOf15 ?x_25)
              (LeftOf1 ?x_25)
            )
          )
          (exists (?x_18 )
            (and
              (RightOf17 ?x_18)
              (LeftOf11 ?x_18)
            )
          )
          (exists (?x_6 )
            (and
              (RightOf16 ?x_6)
              (LeftOf13 ?x_6)
            )
          )
          (exists (?x_29 )
            (and
              (BelowOf6 ?x_29)
              (AboveOf12 ?x_29)
            )
          )
          (exists (?x_12 )
            (and
              (LeftOf2 ?x_12)
              (RightOf11 ?x_12)
            )
          )
          (exists (?x_11 )
            (and
              (AboveOf12 ?x_11)
              (BelowOf10 ?x_11)
            )
          )
          (exists (?x_8 )
            (and
              (LeftOf2 ?x_8)
              (RightOf18 ?x_8)
            )
          )
          (exists (?x_23 )
            (and
              (RightOf15 ?x_23)
              (LeftOf4 ?x_23)
            )
          )
          (exists (?x_23 )
            (and
              (RightOf8 ?x_23)
              (LeftOf5 ?x_23)
            )
          )
          (exists (?x_24 )
            (and
              (BelowOf13 ?x_24)
              (AboveOf17 ?x_24)
            )
          )
          (exists (?x_14 )
            (and
              (LeftOf1 ?x_14)
              (RightOf16 ?x_14)
            )
          )
          (exists (?x_27 )
            (and
              (BelowOf2 ?x_27)
              (AboveOf10 ?x_27)
            )
          )
          (exists (?x_35 )
            (and
              (AboveOf16 ?x_35)
              (BelowOf10 ?x_35)
            )
          )
          (exists (?x_25 )
            (and
              (RightOf19 ?x_25)
              (LeftOf12 ?x_25)
            )
          )
          (exists (?x_9 )
            (and
              (BelowOf11 ?x_9)
              (AboveOf19 ?x_9)
            )
          )
          (exists (?x_32 )
            (and
              (BelowOf3 ?x_32)
              (AboveOf17 ?x_32)
            )
          )
          (exists (?x_0 )
            (and
              (AboveOf11 ?x_0)
              (BelowOf2 ?x_0)
            )
          )
          (exists (?x_34 )
            (and
              (BelowOf7 ?x_34)
              (AboveOf15 ?x_34)
            )
          )
          (exists (?x_14 )
            (and
              (RightOf13 ?x_14)
              (LeftOf8 ?x_14)
            )
          )
          (exists (?x_34 )
            (and
              (AboveOf19 ?x_34)
              (BelowOf1 ?x_34)
            )
          )
          (exists (?x_17 )
            (and
              (LeftOf1 ?x_17)
              (RightOf11 ?x_17)
            )
          )
          (exists (?x_14 )
            (and
              (RightOf18 ?x_14)
              (LeftOf8 ?x_14)
            )
          )
          (exists (?x_32 )
            (and
              (BelowOf1 ?x_32)
              (AboveOf16 ?x_32)
            )
          )
          (exists (?x_20 )
            (and
              (LeftOf7 ?x_20)
              (RightOf12 ?x_20)
            )
          )
          (exists (?x_23 )
            (and
              (RightOf18 ?x_23)
              (LeftOf5 ?x_23)
            )
          )
          (exists (?x_21 )
            (and
              (RightOf8 ?x_21)
              (LeftOf5 ?x_21)
            )
          )
          (exists (?x_20 )
            (and
              (LeftOf1 ?x_20)
              (RightOf11 ?x_20)
            )
          )
          (exists (?x_13 )
            (and
              (LeftOf2 ?x_13)
              (RightOf13 ?x_13)
            )
          )
          (exists (?x_34 )
            (and
              (BelowOf3 ?x_34)
              (AboveOf14 ?x_34)
            )
          )
          (exists (?x_4 )
            (and
              (RightOf17 ?x_4)
              (LeftOf3 ?x_4)
            )
          )
          (exists (?x_7 )
            (and
              (AboveOf7 ?x_7)
              (BelowOf6 ?x_7)
            )
          )
          (exists (?x_23 )
            (and
              (RightOf18 ?x_23)
              (LeftOf6 ?x_23)
            )
          )
          (exists (?x_36 )
            (and
              (AboveOf19 ?x_36)
              (BelowOf14 ?x_36)
            )
          )
          (exists (?x_29 )
            (and
              (BelowOf4 ?x_29)
              (AboveOf14 ?x_29)
            )
          )
          (exists (?x_35 )
            (and
              (AboveOf17 ?x_35)
              (BelowOf10 ?x_35)
            )
          )
          (exists (?x_26 )
            (and
              (LeftOf3 ?x_26)
              (RightOf18 ?x_26)
            )
          )
          (exists (?x_35 )
            (and
              (BelowOf6 ?x_35)
              (AboveOf15 ?x_35)
            )
          )
          (exists (?x_3 )
            (and
              (AboveOf16 ?x_3)
              (BelowOf11 ?x_3)
            )
          )
          (exists (?x_2 )
            (and
              (LeftOf1 ?x_2)
              (RightOf11 ?x_2)
            )
          )
          (exists (?x_23 )
            (and
              (RightOf15 ?x_23)
              (LeftOf6 ?x_23)
            )
          )
          (exists (?x_11 )
            (and
              (AboveOf18 ?x_11)
              (BelowOf2 ?x_11)
            )
          )
          (exists (?x_10 )
            (and
              (AboveOf5 ?x_10)
              (BelowOf3 ?x_10)
            )
          )
          (exists (?x_8 )
            (and
              (RightOf9 ?x_8)
              (LeftOf3 ?x_8)
            )
          )
          (exists (?x_11 )
            (and
              (BelowOf2 ?x_11)
              (AboveOf18 ?x_11)
            )
          )
          (exists (?x_9 )
            (and
              (BelowOf1 ?x_9)
              (AboveOf18 ?x_9)
            )
          )
          (exists (?x_13 )
            (and
              (RightOf18 ?x_13)
              (LeftOf5 ?x_13)
            )
          )
          (exists (?x_34 )
            (and
              (AboveOf17 ?x_34)
              (BelowOf1 ?x_34)
            )
          )
          (exists (?x_33 )
            (and
              (AboveOf15 ?x_33)
              (BelowOf5 ?x_33)
            )
          )
          (exists (?x_11 )
            (and
              (BelowOf11 ?x_11)
              (AboveOf15 ?x_11)
            )
          )
          (exists (?x_27 )
            (and
              (AboveOf7 ?x_27)
              (BelowOf3 ?x_27)
            )
          )
          (exists (?x_22 )
            (and
              (BelowOf6 ?x_22)
              (AboveOf18 ?x_22)
            )
          )
          (exists (?x_14 )
            (and
              (RightOf16 ?x_14)
              (LeftOf7 ?x_14)
            )
          )
          (exists (?x_9 )
            (and
              (BelowOf10 ?x_9)
              (AboveOf18 ?x_9)
            )
          )
          (exists (?x_12 )
            (and
              (LeftOf3 ?x_12)
              (RightOf12 ?x_12)
            )
          )
          (exists (?x_4 )
            (and
              (RightOf18 ?x_4)
              (LeftOf14 ?x_4)
            )
          )
          (exists (?x_22 )
            (and
              (AboveOf19 ?x_22)
              (BelowOf2 ?x_22)
            )
          )
          (exists (?x_0 )
            (and
              (BelowOf1 ?x_0)
              (AboveOf9 ?x_0)
            )
          )
          (exists (?x_21 )
            (and
              (RightOf15 ?x_21)
              (LeftOf4 ?x_21)
            )
          )
          (exists (?x_25 )
            (and
              (LeftOf12 ?x_25)
              (RightOf16 ?x_25)
            )
          )
          (exists (?x_32 )
            (and
              (BelowOf7 ?x_32)
              (AboveOf14 ?x_32)
            )
          )
          (exists (?x_0 )
            (and
              (AboveOf15 ?x_0)
              (BelowOf2 ?x_0)
            )
          )
          (exists (?x_11 )
            (and
              (BelowOf5 ?x_11)
              (AboveOf13 ?x_11)
            )
          )
          (exists (?x_35 )
            (and
              (AboveOf14 ?x_35)
              (BelowOf9 ?x_35)
            )
          )
          (exists (?x_31 )
            (and
              (LeftOf2 ?x_31)
              (RightOf18 ?x_31)
            )
          )
          (exists (?x_8 )
            (and
              (RightOf3 ?x_8)
              (LeftOf3 ?x_8)
            )
          )
          (exists (?x_17 )
            (and
              (RightOf2 ?x_17)
              (LeftOf1 ?x_17)
            )
          )
          (exists (?x_18 )
            (and
              (RightOf17 ?x_18)
              (LeftOf6 ?x_18)
            )
          )
          (exists (?x_30 )
            (and
              (RightOf19 ?x_30)
              (LeftOf9 ?x_30)
            )
          )
          (exists (?x_35 )
            (and
              (BelowOf5 ?x_35)
              (AboveOf13 ?x_35)
            )
          )
          (exists (?x_34 )
            (and
              (BelowOf3 ?x_34)
              (AboveOf18 ?x_34)
            )
          )
          (exists (?x_7 )
            (and
              (AboveOf18 ?x_7)
              (BelowOf3 ?x_7)
            )
          )
          (exists (?x_32 )
            (and
              (BelowOf7 ?x_32)
              (AboveOf19 ?x_32)
            )
          )
          (exists (?x_34 )
            (and
              (AboveOf16 ?x_34)
              (BelowOf10 ?x_34)
            )
          )
          (exists (?x_11 )
            (and
              (AboveOf17 ?x_11)
              (BelowOf7 ?x_11)
            )
          )
          (exists (?x_29 )
            (and
              (AboveOf12 ?x_29)
              (BelowOf3 ?x_29)
            )
          )
          (exists (?x_33 )
            (and
              (BelowOf4 ?x_33)
              (AboveOf12 ?x_33)
            )
          )
          (exists (?x_26 )
            (and
              (LeftOf8 ?x_26)
              (RightOf19 ?x_26)
            )
          )
          (exists (?x_12 )
            (and
              (RightOf8 ?x_12)
              (LeftOf4 ?x_12)
            )
          )
          (exists (?x_1 )
            (and
              (BelowOf1 ?x_1)
              (AboveOf9 ?x_1)
            )
          )
          (exists (?x_24 )
            (and
              (AboveOf16 ?x_24)
              (BelowOf11 ?x_24)
            )
          )
          (exists (?x_2 )
            (and
              (RightOf14 ?x_2)
              (LeftOf2 ?x_2)
            )
          )
          (exists (?x_26 )
            (and
              (LeftOf10 ?x_26)
              (RightOf19 ?x_26)
            )
          )
          (exists (?x_7 )
            (and
              (BelowOf4 ?x_7)
              (AboveOf15 ?x_7)
            )
          )
          (exists (?x_19 )
            (and
              (AboveOf18 ?x_19)
              (BelowOf15 ?x_19)
            )
          )
          (exists (?x_31 )
            (and
              (LeftOf3 ?x_31)
              (RightOf13 ?x_31)
            )
          )
          (exists (?x_21 )
            (and
              (RightOf6 ?x_21)
              (LeftOf3 ?x_21)
            )
          )
          (exists (?x_27 )
            (and
              (AboveOf16 ?x_27)
              (BelowOf3 ?x_27)
            )
          )
          (exists (?x_35 )
            (and
              (BelowOf2 ?x_35)
              (AboveOf18 ?x_35)
            )
          )
          (exists (?x_29 )
            (and
              (AboveOf16 ?x_29)
              (BelowOf6 ?x_29)
            )
          )
          (exists (?x_19 )
            (and
              (AboveOf18 ?x_19)
              (BelowOf1 ?x_19)
            )
          )
          (exists (?x_5 )
            (and
              (LeftOf11 ?x_5)
              (RightOf13 ?x_5)
            )
          )
          (exists (?x_18 )
            (and
              (LeftOf7 ?x_18)
              (RightOf19 ?x_18)
            )
          )
          (exists (?x_22 )
            (and
              (AboveOf13 ?x_22)
              (BelowOf4 ?x_22)
            )
          )
          (exists (?x_5 )
            (and
              (LeftOf4 ?x_5)
              (RightOf12 ?x_5)
            )
          )
          (exists (?x_6 )
            (and
              (LeftOf16 ?x_6)
              (RightOf16 ?x_6)
            )
          )
          (exists (?x_11 )
            (and
              (BelowOf9 ?x_11)
              (AboveOf15 ?x_11)
            )
          )
          (exists (?x_11 )
            (and
              (BelowOf5 ?x_11)
              (AboveOf16 ?x_11)
            )
          )
          (exists (?x_22 )
            (and
              (AboveOf15 ?x_22)
              (BelowOf13 ?x_22)
            )
          )
          (exists (?x_21 )
            (and
              (LeftOf2 ?x_21)
              (RightOf11 ?x_21)
            )
          )
          (exists (?x_18 )
            (and
              (RightOf19 ?x_18)
              (LeftOf11 ?x_18)
            )
          )
          (exists (?x_31 )
            (and
              (LeftOf5 ?x_31)
              (RightOf18 ?x_31)
            )
          )
          (exists (?x_24 )
            (and
              (AboveOf15 ?x_24)
              (BelowOf2 ?x_24)
            )
          )
          (exists (?x_29 )
            (and
              (BelowOf6 ?x_29)
              (AboveOf13 ?x_29)
            )
          )
          (exists (?x_22 )
            (and
              (AboveOf15 ?x_22)
              (BelowOf2 ?x_22)
            )
          )
          (exists (?x_11 )
            (and
              (AboveOf12 ?x_11)
              (BelowOf1 ?x_11)
            )
          )
          (exists (?x_13 )
            (and
              (RightOf16 ?x_13)
              (LeftOf7 ?x_13)
            )
          )
          (exists (?x_23 )
            (and
              (LeftOf2 ?x_23)
              (RightOf14 ?x_23)
            )
          )
          (exists (?x_10 )
            (and
              (AboveOf16 ?x_10)
              (BelowOf2 ?x_10)
            )
          )
          (exists (?x_13 )
            (and
              (RightOf14 ?x_13)
              (LeftOf5 ?x_13)
            )
          )
          (exists (?x_14 )
            (and
              (RightOf14 ?x_14)
              (LeftOf10 ?x_14)
            )
          )
          (exists (?x_6 )
            (and
              (RightOf18 ?x_6)
              (LeftOf11 ?x_6)
            )
          )
          (exists (?x_0 )
            (and
              (AboveOf17 ?x_0)
              (BelowOf2 ?x_0)
            )
          )
          (exists (?x_10 )
            (and
              (AboveOf18 ?x_10)
              (BelowOf2 ?x_10)
            )
          )
          (exists (?x_36 )
            (and
              (AboveOf19 ?x_36)
              (BelowOf7 ?x_36)
            )
          )
          (exists (?x_36 )
            (and
              (AboveOf19 ?x_36)
              (BelowOf4 ?x_36)
            )
          )
          (exists (?x_7 )
            (and
              (AboveOf9 ?x_7)
              (BelowOf6 ?x_7)
            )
          )
          (exists (?x_22 )
            (and
              (AboveOf19 ?x_22)
              (BelowOf9 ?x_22)
            )
          )
          (exists (?x_9 )
            (and
              (BelowOf11 ?x_9)
              (AboveOf17 ?x_9)
            )
          )
          (exists (?x_26 )
            (and
              (RightOf19 ?x_26)
              (LeftOf16 ?x_26)
            )
          )
          (exists (?x_35 )
            (and
              (BelowOf6 ?x_35)
              (AboveOf13 ?x_35)
            )
          )
          (exists (?x_12 )
            (and
              (LeftOf6 ?x_12)
              (RightOf12 ?x_12)
            )
          )
          (exists (?x_37 )
            (and
              (AboveOf18 ?x_37)
              (BelowOf4 ?x_37)
            )
          )
          (exists (?x_28 )
            (and
              (RightOf7 ?x_28)
              (LeftOf2 ?x_28)
            )
          )
          (exists (?x_12 )
            (and
              (LeftOf7 ?x_12)
              (RightOf17 ?x_12)
            )
          )
          (exists (?x_29 )
            (and
              (BelowOf4 ?x_29)
              (AboveOf10 ?x_29)
            )
          )
          (exists (?x_5 )
            (and
              (LeftOf1 ?x_5)
              (RightOf15 ?x_5)
            )
          )
          (exists (?x_25 )
            (and
              (LeftOf3 ?x_25)
              (RightOf19 ?x_25)
            )
          )
          (exists (?x_13 )
            (and
              (RightOf15 ?x_13)
              (LeftOf7 ?x_13)
            )
          )
          (exists (?x_14 )
            (and
              (RightOf19 ?x_14)
              (LeftOf11 ?x_14)
            )
          )
          (exists (?x_11 )
            (and
              (BelowOf4 ?x_11)
              (AboveOf16 ?x_11)
            )
          )
          (exists (?x_16 )
            (and
              (LeftOf2 ?x_16)
              (RightOf10 ?x_16)
            )
          )
          (exists (?x_5 )
            (and
              (LeftOf4 ?x_5)
              (RightOf18 ?x_5)
            )
          )
          (exists (?x_23 )
            (and
              (LeftOf3 ?x_23)
              (RightOf12 ?x_23)
            )
          )
          (exists (?x_35 )
            (and
              (BelowOf8 ?x_35)
              (AboveOf17 ?x_35)
            )
          )
          (exists (?x_31 )
            (and
              (LeftOf3 ?x_31)
              (RightOf12 ?x_31)
            )
          )
          (exists (?x_35 )
            (and
              (AboveOf18 ?x_35)
              (BelowOf10 ?x_35)
            )
          )
          (exists (?x_20 )
            (and
              (LeftOf6 ?x_20)
              (RightOf11 ?x_20)
            )
          )
          (exists (?x_27 )
            (and
              (BelowOf1 ?x_27)
              (AboveOf19 ?x_27)
            )
          )
          (exists (?x_12 )
            (and
              (LeftOf1 ?x_12)
              (RightOf15 ?x_12)
            )
          )
          (exists (?x_0 )
            (and
              (BelowOf3 ?x_0)
              (AboveOf13 ?x_0)
            )
          )
          (exists (?x_31 )
            (and
              (LeftOf8 ?x_31)
              (RightOf16 ?x_31)
            )
          )
          (exists (?x_12 )
            (and
              (RightOf18 ?x_12)
              (LeftOf7 ?x_12)
            )
          )
          (exists (?x_2 )
            (and
              (RightOf6 ?x_2)
              (LeftOf3 ?x_2)
            )
          )
          (exists (?x_34 )
            (and
              (AboveOf17 ?x_34)
              (BelowOf8 ?x_34)
            )
          )
          (exists (?x_12 )
            (and
              (LeftOf2 ?x_12)
              (RightOf12 ?x_12)
            )
          )
          (exists (?x_37 )
            (and
              (AboveOf14 ?x_37)
              (BelowOf1 ?x_37)
            )
          )
          (exists (?x_31 )
            (and
              (RightOf17 ?x_31)
              (LeftOf4 ?x_31)
            )
          )
          (exists (?x_5 )
            (and
              (RightOf12 ?x_5)
              (LeftOf10 ?x_5)
            )
          )
          (exists (?x_35 )
            (and
              (BelowOf10 ?x_35)
              (AboveOf15 ?x_35)
            )
          )
          (exists (?x_16 )
            (and
              (RightOf9 ?x_16)
              (LeftOf4 ?x_16)
            )
          )
          (exists (?x_23 )
            (and
              (LeftOf5 ?x_23)
              (RightOf9 ?x_23)
            )
          )
          (exists (?x_20 )
            (and
              (RightOf13 ?x_20)
              (LeftOf8 ?x_20)
            )
          )
          (exists (?x_27 )
            (and
              (AboveOf18 ?x_27)
              (BelowOf4 ?x_27)
            )
          )
          (exists (?x_9 )
            (and
              (AboveOf17 ?x_9)
              (BelowOf6 ?x_9)
            )
          )
          (exists (?x_27 )
            (and
              (BelowOf4 ?x_27)
              (AboveOf13 ?x_27)
            )
          )
          (exists (?x_3 )
            (and
              (BelowOf5 ?x_3)
              (AboveOf19 ?x_3)
            )
          )
          (exists (?x_7 )
            (and
              (AboveOf15 ?x_7)
              (BelowOf5 ?x_7)
            )
          )
          (exists (?x_26 )
            (and
              (RightOf18 ?x_26)
              (LeftOf2 ?x_26)
            )
          )
          (exists (?x_24 )
            (and
              (BelowOf5 ?x_24)
              (AboveOf17 ?x_24)
            )
          )
          (exists (?x_5 )
            (and
              (RightOf14 ?x_5)
              (LeftOf10 ?x_5)
            )
          )
          (exists (?x_27 )
            (and
              (AboveOf13 ?x_27)
              (BelowOf5 ?x_27)
            )
          )
          (exists (?x_15 )
            (and
              (BelowOf1 ?x_15)
              (AboveOf19 ?x_15)
            )
          )
          (exists (?x_13 )
            (and
              (LeftOf1 ?x_13)
              (RightOf16 ?x_13)
            )
          )
          (exists (?x_18 )
            (and
              (RightOf15 ?x_18)
              (LeftOf12 ?x_18)
            )
          )
          (exists (?x_11 )
            (and
              (AboveOf11 ?x_11)
              (BelowOf9 ?x_11)
            )
          )
          (exists (?x_11 )
            (and
              (BelowOf10 ?x_11)
              (AboveOf18 ?x_11)
            )
          )
          (exists (?x_12 )
            (and
              (RightOf14 ?x_12)
              (LeftOf6 ?x_12)
            )
          )
          (exists (?x_34 )
            (and
              (AboveOf17 ?x_34)
              (BelowOf2 ?x_34)
            )
          )
          (exists (?x_31 )
            (and
              (RightOf13 ?x_31)
              (LeftOf1 ?x_31)
            )
          )
          (exists (?x_7 )
            (and
              (BelowOf5 ?x_7)
              (AboveOf17 ?x_7)
            )
          )
          (exists (?x_3 )
            (and
              (BelowOf3 ?x_3)
              (AboveOf16 ?x_3)
            )
          )
          (exists (?x_1 )
            (and
              (BelowOf1 ?x_1)
              (AboveOf11 ?x_1)
            )
          )
          (exists (?x_11 )
            (and
              (AboveOf19 ?x_11)
              (BelowOf1 ?x_11)
            )
          )
          (exists (?x_8 )
            (and
              (RightOf15 ?x_8)
              (LeftOf2 ?x_8)
            )
          )
          (exists (?x_14 )
            (and
              (LeftOf6 ?x_14)
              (RightOf17 ?x_14)
            )
          )
          (exists (?x_4 )
            (and
              (RightOf17 ?x_4)
              (LeftOf4 ?x_4)
            )
          )
          (exists (?x_8 )
            (and
              (RightOf19 ?x_8)
              (LeftOf3 ?x_8)
            )
          )
          (exists (?x_32 )
            (and
              (BelowOf4 ?x_32)
              (AboveOf17 ?x_32)
            )
          )
          (exists (?x_32 )
            (and
              (BelowOf9 ?x_32)
              (AboveOf16 ?x_32)
            )
          )
          (exists (?x_7 )
            (and
              (AboveOf18 ?x_7)
              (BelowOf2 ?x_7)
            )
          )
          (exists (?x_29 )
            (and
              (BelowOf8 ?x_29)
              (AboveOf18 ?x_29)
            )
          )
          (exists (?x_19 )
            (and
              (AboveOf19 ?x_19)
              (BelowOf13 ?x_19)
            )
          )
          (exists (?x_33 )
            (and
              (AboveOf12 ?x_33)
              (BelowOf8 ?x_33)
            )
          )
          (exists (?x_7 )
            (and
              (BelowOf3 ?x_7)
              (AboveOf14 ?x_7)
            )
          )
          (exists (?x_2 )
            (and
              (LeftOf1 ?x_2)
              (RightOf10 ?x_2)
            )
          )
          (exists (?x_33 )
            (and
              (AboveOf10 ?x_33)
              (BelowOf3 ?x_33)
            )
          )
          (exists (?x_30 )
            (and
              (LeftOf6 ?x_30)
              (RightOf19 ?x_30)
            )
          )
          (exists (?x_7 )
            (and
              (BelowOf7 ?x_7)
              (AboveOf15 ?x_7)
            )
          )
          (exists (?x_35 )
            (and
              (AboveOf18 ?x_35)
              (BelowOf7 ?x_35)
            )
          )
          (exists (?x_31 )
            (and
              (RightOf12 ?x_31)
              (LeftOf7 ?x_31)
            )
          )
          (exists (?x_24 )
            (and
              (AboveOf18 ?x_24)
              (BelowOf10 ?x_24)
            )
          )
          (exists (?x_7 )
            (and
              (AboveOf13 ?x_7)
              (BelowOf4 ?x_7)
            )
          )
          (exists (?x_24 )
            (and
              (AboveOf19 ?x_24)
              (BelowOf3 ?x_24)
            )
          )
          (exists (?x_7 )
            (and
              (AboveOf7 ?x_7)
              (BelowOf2 ?x_7)
            )
          )
          (exists (?x_22 )
            (and
              (AboveOf14 ?x_22)
              (BelowOf4 ?x_22)
            )
          )
          (exists (?x_20 )
            (and
              (LeftOf9 ?x_20)
              (RightOf14 ?x_20)
            )
          )
          (exists (?x_24 )
            (and
              (BelowOf1 ?x_24)
              (AboveOf18 ?x_24)
            )
          )
          (exists (?x_0 )
            (and
              (AboveOf19 ?x_0)
              (BelowOf1 ?x_0)
            )
          )
          (exists (?x_29 )
            (and
              (BelowOf5 ?x_29)
              (AboveOf17 ?x_29)
            )
          )
          (exists (?x_20 )
            (and
              (RightOf18 ?x_20)
              (LeftOf8 ?x_20)
            )
          )
          (exists (?x_35 )
            (and
              (AboveOf17 ?x_35)
              (BelowOf7 ?x_35)
            )
          )
          (exists (?x_37 )
            (and
              (AboveOf9 ?x_37)
              (BelowOf1 ?x_37)
            )
          )
          (exists (?x_23 )
            (and
              (LeftOf7 ?x_23)
              (RightOf14 ?x_23)
            )
          )
          (exists (?x_29 )
            (and
              (BelowOf4 ?x_29)
              (AboveOf9 ?x_29)
            )
          )
          (exists (?x_18 )
            (and
              (RightOf14 ?x_18)
              (LeftOf1 ?x_18)
            )
          )
          (exists (?x_37 )
            (and
              (AboveOf18 ?x_37)
              (BelowOf2 ?x_37)
            )
          )
          (exists (?x_14 )
            (and
              (LeftOf13 ?x_14)
              (RightOf18 ?x_14)
            )
          )
          (exists (?x_8 )
            (and
              (LeftOf3 ?x_8)
              (RightOf7 ?x_8)
            )
          )
          (exists (?x_25 )
            (and
              (RightOf16 ?x_25)
              (LeftOf9 ?x_25)
            )
          )
          (exists (?x_22 )
            (and
              (BelowOf5 ?x_22)
              (AboveOf18 ?x_22)
            )
          )
          (exists (?x_34 )
            (and
              (BelowOf11 ?x_34)
              (AboveOf14 ?x_34)
            )
          )
          (exists (?x_2 )
            (and
              (LeftOf4 ?x_2)
              (RightOf17 ?x_2)
            )
          )
          (exists (?x_31 )
            (and
              (RightOf19 ?x_31)
              (LeftOf8 ?x_31)
            )
          )
          (exists (?x_21 )
            (and
              (LeftOf4 ?x_21)
              (RightOf19 ?x_21)
            )
          )
          (exists (?x_2 )
            (and
              (LeftOf6 ?x_2)
              (RightOf11 ?x_2)
            )
          )
          (exists (?x_20 )
            (and
              (LeftOf4 ?x_20)
              (RightOf17 ?x_20)
            )
          )
          (exists (?x_20 )
            (and
              (RightOf15 ?x_20)
              (LeftOf6 ?x_20)
            )
          )
          (exists (?x_20 )
            (and
              (LeftOf7 ?x_20)
              (RightOf14 ?x_20)
            )
          )
          (exists (?x_21 )
            (and
              (LeftOf4 ?x_21)
              (RightOf12 ?x_21)
            )
          )
          (exists (?x_8 )
            (and
              (LeftOf1 ?x_8)
              (RightOf19 ?x_8)
            )
          )
          (exists (?x_27 )
            (and
              (AboveOf7 ?x_27)
              (BelowOf4 ?x_27)
            )
          )
          (exists (?x_27 )
            (and
              (AboveOf17 ?x_27)
              (BelowOf5 ?x_27)
            )
          )
          (exists (?x_6 )
            (and
              (RightOf18 ?x_6)
              (LeftOf1 ?x_6)
            )
          )
          (exists (?x_34 )
            (and
              (AboveOf19 ?x_34)
              (BelowOf13 ?x_34)
            )
          )
          (exists (?x_7 )
            (and
              (BelowOf7 ?x_7)
              (AboveOf19 ?x_7)
            )
          )
          (exists (?x_14 )
            (and
              (LeftOf11 ?x_14)
              (RightOf18 ?x_14)
            )
          )
          (exists (?x_22 )
            (and
              (AboveOf17 ?x_22)
              (BelowOf8 ?x_22)
            )
          )
          (exists (?x_19 )
            (and
              (AboveOf19 ?x_19)
              (BelowOf3 ?x_19)
            )
          )
          (exists (?x_7 )
            (and
              (AboveOf19 ?x_7)
              (BelowOf5 ?x_7)
            )
          )
          (exists (?x_7 )
            (and
              (AboveOf18 ?x_7)
              (BelowOf4 ?x_7)
            )
          )
          (exists (?x_7 )
            (and
              (BelowOf1 ?x_7)
              (AboveOf15 ?x_7)
            )
          )
          (exists (?x_30 )
            (and
              (RightOf19 ?x_30)
              (LeftOf4 ?x_30)
            )
          )
          (exists (?x_18 )
            (and
              (RightOf18 ?x_18)
              (LeftOf13 ?x_18)
            )
          )
          (exists (?x_11 )
            (and
              (AboveOf17 ?x_11)
              (BelowOf6 ?x_11)
            )
          )
          (exists (?x_27 )
            (and
              (AboveOf9 ?x_27)
              (BelowOf1 ?x_27)
            )
          )
          (exists (?x_10 )
            (and
              (BelowOf4 ?x_10)
              (AboveOf4 ?x_10)
            )
          )
          (exists (?x_13 )
            (and
              (RightOf12 ?x_13)
              (LeftOf3 ?x_13)
            )
          )
          (exists (?x_12 )
            (and
              (LeftOf1 ?x_12)
              (RightOf8 ?x_12)
            )
          )
          (exists (?x_9 )
            (and
              (AboveOf19 ?x_9)
              (BelowOf14 ?x_9)
            )
          )
          (exists (?x_5 )
            (and
              (LeftOf7 ?x_5)
              (RightOf18 ?x_5)
            )
          )
          (exists (?x_34 )
            (and
              (BelowOf2 ?x_34)
              (AboveOf18 ?x_34)
            )
          )
          (exists (?x_35 )
            (and
              (BelowOf6 ?x_35)
              (AboveOf16 ?x_35)
            )
          )
          (exists (?x_14 )
            (and
              (RightOf15 ?x_14)
              (LeftOf5 ?x_14)
            )
          )
          (exists (?x_2 )
            (and
              (LeftOf6 ?x_2)
              (RightOf18 ?x_2)
            )
          )
          (exists (?x_24 )
            (and
              (AboveOf16 ?x_24)
              (BelowOf3 ?x_24)
            )
          )
          (exists (?x_25 )
            (and
              (RightOf19 ?x_25)
              (LeftOf11 ?x_25)
            )
          )
          (exists (?x_8 )
            (and
              (RightOf12 ?x_8)
              (LeftOf3 ?x_8)
            )
          )
          (exists (?x_16 )
            (and
              (RightOf19 ?x_16)
              (LeftOf1 ?x_16)
            )
          )
          (exists (?x_31 )
            (and
              (LeftOf5 ?x_31)
              (RightOf13 ?x_31)
            )
          )
          (exists (?x_27 )
            (and
              (BelowOf2 ?x_27)
              (AboveOf17 ?x_27)
            )
          )
          (exists (?x_9 )
            (and
              (AboveOf17 ?x_9)
              (BelowOf12 ?x_9)
            )
          )
          (exists (?x_11 )
            (and
              (AboveOf12 ?x_11)
              (BelowOf11 ?x_11)
            )
          )
          (exists (?x_11 )
            (and
              (AboveOf11 ?x_11)
              (BelowOf7 ?x_11)
            )
          )
          (exists (?x_33 )
            (and
              (AboveOf16 ?x_33)
              (BelowOf3 ?x_33)
            )
          )
          (exists (?x_24 )
            (and
              (BelowOf9 ?x_24)
              (AboveOf19 ?x_24)
            )
          )
          (exists (?x_24 )
            (and
              (BelowOf9 ?x_24)
              (AboveOf18 ?x_24)
            )
          )
          (exists (?x_23 )
            (and
              (RightOf18 ?x_23)
              (LeftOf7 ?x_23)
            )
          )
          (exists (?x_20 )
            (and
              (LeftOf9 ?x_20)
              (RightOf17 ?x_20)
            )
          )
          (exists (?x_14 )
            (and
              (RightOf14 ?x_14)
              (LeftOf12 ?x_14)
            )
          )
          (exists (?x_9 )
            (and
              (AboveOf18 ?x_9)
              (BelowOf10 ?x_9)
            )
          )
          (exists (?x_3 )
            (and
              (AboveOf18 ?x_3)
              (BelowOf13 ?x_3)
            )
          )
          (exists (?x_22 )
            (and
              (AboveOf14 ?x_22)
              (BelowOf9 ?x_22)
            )
          )
          (exists (?x_32 )
            (and
              (AboveOf15 ?x_32)
              (BelowOf7 ?x_32)
            )
          )
          (exists (?x_36 )
            (and
              (AboveOf19 ?x_36)
              (BelowOf19 ?x_36)
            )
          )
          (exists (?x_33 )
            (and
              (BelowOf7 ?x_33)
              (AboveOf16 ?x_33)
            )
          )
          (exists (?x_18 )
            (and
              (RightOf18 ?x_18)
              (LeftOf2 ?x_18)
            )
          )
          (exists (?x_11 )
            (and
              (BelowOf8 ?x_11)
              (AboveOf18 ?x_11)
            )
          )
          (exists (?x_22 )
            (and
              (BelowOf7 ?x_22)
              (AboveOf18 ?x_22)
            )
          )
          (exists (?x_31 )
            (and
              (LeftOf2 ?x_31)
              (RightOf15 ?x_31)
            )
          )
          (exists (?x_32 )
            (and
              (BelowOf2 ?x_32)
              (AboveOf12 ?x_32)
            )
          )
          (exists (?x_34 )
            (and
              (AboveOf19 ?x_34)
              (BelowOf7 ?x_34)
            )
          )
          (exists (?x_28 )
            (and
              (RightOf16 ?x_28)
              (LeftOf2 ?x_28)
            )
          )
          (exists (?x_0 )
            (and
              (BelowOf3 ?x_0)
              (AboveOf11 ?x_0)
            )
          )
          (exists (?x_35 )
            (and
              (AboveOf13 ?x_35)
              (BelowOf2 ?x_35)
            )
          )
          (exists (?x_25 )
            (and
              (LeftOf10 ?x_25)
              (RightOf16 ?x_25)
            )
          )
          (exists (?x_13 )
            (and
              (RightOf18 ?x_13)
              (LeftOf3 ?x_13)
            )
          )
          (exists (?x_13 )
            (and
              (LeftOf6 ?x_13)
              (RightOf17 ?x_13)
            )
          )
          (exists (?x_37 )
            (and
              (BelowOf5 ?x_37)
              (AboveOf18 ?x_37)
            )
          )
          (exists (?x_28 )
            (and
              (RightOf13 ?x_28)
              (LeftOf2 ?x_28)
            )
          )
          (exists (?x_9 )
            (and
              (AboveOf17 ?x_9)
              (BelowOf2 ?x_9)
            )
          )
          (exists (?x_6 )
            (and
              (RightOf19 ?x_6)
              (LeftOf6 ?x_6)
            )
          )
          (exists (?x_14 )
            (and
              (RightOf16 ?x_14)
              (LeftOf5 ?x_14)
            )
          )
          (exists (?x_20 )
            (and
              (LeftOf8 ?x_20)
              (RightOf14 ?x_20)
            )
          )
          (exists (?x_28 )
            (and
              (RightOf12 ?x_28)
              (LeftOf1 ?x_28)
            )
          )
          (exists (?x_18 )
            (and
              (LeftOf12 ?x_18)
              (RightOf19 ?x_18)
            )
          )
          (exists (?x_35 )
            (and
              (AboveOf14 ?x_35)
              (BelowOf1 ?x_35)
            )
          )
          (exists (?x_13 )
            (and
              (LeftOf4 ?x_13)
              (RightOf13 ?x_13)
            )
          )
          (exists (?x_20 )
            (and
              (RightOf16 ?x_20)
              (LeftOf1 ?x_20)
            )
          )
          (exists (?x_3 )
            (and
              (BelowOf7 ?x_3)
              (AboveOf19 ?x_3)
            )
          )
          (exists (?x_21 )
            (and
              (LeftOf1 ?x_21)
              (RightOf9 ?x_21)
            )
          )
          (exists (?x_11 )
            (and
              (AboveOf14 ?x_11)
              (BelowOf10 ?x_11)
            )
          )
          (exists (?x_29 )
            (and
              (BelowOf7 ?x_29)
              (AboveOf8 ?x_29)
            )
          )
          (exists (?x_32 )
            (and
              (AboveOf18 ?x_32)
              (BelowOf1 ?x_32)
            )
          )
          (exists (?x_25 )
            (and
              (LeftOf5 ?x_25)
              (RightOf15 ?x_25)
            )
          )
          (exists (?x_20 )
            (and
              (LeftOf7 ?x_20)
              (RightOf15 ?x_20)
            )
          )
          (exists (?x_9 )
            (and
              (BelowOf16 ?x_9)
              (AboveOf17 ?x_9)
            )
          )
          (exists (?x_29 )
            (and
              (AboveOf11 ?x_29)
              (BelowOf2 ?x_29)
            )
          )
          (exists (?x_10 )
            (and
              (AboveOf16 ?x_10)
              (BelowOf1 ?x_10)
            )
          )
          (exists (?x_7 )
            (and
              (AboveOf8 ?x_7)
              (BelowOf2 ?x_7)
            )
          )
          (exists (?x_29 )
            (and
              (BelowOf7 ?x_29)
              (AboveOf10 ?x_29)
            )
          )
          (exists (?x_27 )
            (and
              (AboveOf9 ?x_27)
              (BelowOf6 ?x_27)
            )
          )
          (exists (?x_35 )
            (and
              (AboveOf14 ?x_35)
              (BelowOf3 ?x_35)
            )
          )
          (exists (?x_18 )
            (and
              (RightOf14 ?x_18)
              (LeftOf14 ?x_18)
            )
          )
          (exists (?x_10 )
            (and
              (AboveOf14 ?x_10)
              (BelowOf2 ?x_10)
            )
          )
          (exists (?x_37 )
            (and
              (BelowOf3 ?x_37)
              (AboveOf15 ?x_37)
            )
          )
          (exists (?x_32 )
            (and
              (BelowOf7 ?x_32)
              (AboveOf17 ?x_32)
            )
          )
          (exists (?x_15 )
            (and
              (AboveOf4 ?x_15)
              (BelowOf2 ?x_15)
            )
          )
          (exists (?x_35 )
            (and
              (AboveOf19 ?x_35)
              (BelowOf4 ?x_35)
            )
          )
          (exists (?x_11 )
            (and
              (AboveOf11 ?x_11)
              (BelowOf11 ?x_11)
            )
          )
          (exists (?x_27 )
            (and
              (AboveOf15 ?x_27)
              (BelowOf3 ?x_27)
            )
          )
          (exists (?x_11 )
            (and
              (BelowOf5 ?x_11)
              (AboveOf12 ?x_11)
            )
          )
          (exists (?x_24 )
            (and
              (AboveOf18 ?x_24)
              (BelowOf3 ?x_24)
            )
          )
          (exists (?x_18 )
            (and
              (RightOf18 ?x_18)
              (LeftOf7 ?x_18)
            )
          )
          (exists (?x_20 )
            (and
              (RightOf18 ?x_20)
              (LeftOf6 ?x_20)
            )
          )
          (exists (?x_29 )
            (and
              (AboveOf16 ?x_29)
              (BelowOf1 ?x_29)
            )
          )
          (exists (?x_33 )
            (and
              (BelowOf3 ?x_33)
              (AboveOf19 ?x_33)
            )
          )
          (exists (?x_21 )
            (and
              (LeftOf1 ?x_21)
              (RightOf5 ?x_21)
            )
          )
          (exists (?x_4 )
            (and
              (RightOf18 ?x_4)
              (LeftOf3 ?x_4)
            )
          )
          (exists (?x_4 )
            (and
              (RightOf17 ?x_4)
              (LeftOf2 ?x_4)
            )
          )
          (exists (?x_11 )
            (and
              (AboveOf16 ?x_11)
              (BelowOf7 ?x_11)
            )
          )
          (exists (?x_5 )
            (and
              (RightOf13 ?x_5)
              (LeftOf10 ?x_5)
            )
          )
          (exists (?x_11 )
            (and
              (AboveOf13 ?x_11)
              (BelowOf2 ?x_11)
            )
          )
          (exists (?x_18 )
            (and
              (RightOf14 ?x_18)
              (LeftOf13 ?x_18)
            )
          )
          (exists (?x_10 )
            (and
              (AboveOf5 ?x_10)
              (BelowOf1 ?x_10)
            )
          )
          (exists (?x_34 )
            (and
              (AboveOf19 ?x_34)
              (BelowOf14 ?x_34)
            )
          )
          (exists (?x_19 )
            (and
              (AboveOf19 ?x_19)
              (BelowOf1 ?x_19)
            )
          )
          (exists (?x_13 )
            (and
              (RightOf15 ?x_13)
              (LeftOf10 ?x_13)
            )
          )
          (exists (?x_34 )
            (and
              (BelowOf5 ?x_34)
              (AboveOf14 ?x_34)
            )
          )
          (exists (?x_31 )
            (and
              (RightOf11 ?x_31)
              (LeftOf8 ?x_31)
            )
          )
          (exists (?x_29 )
            (and
              (AboveOf15 ?x_29)
              (BelowOf3 ?x_29)
            )
          )
          (exists (?x_29 )
            (and
              (AboveOf16 ?x_29)
              (BelowOf7 ?x_29)
            )
          )
          (exists (?x_28 )
            (and
              (LeftOf2 ?x_28)
              (RightOf19 ?x_28)
            )
          )
          (exists (?x_30 )
            (and
              (RightOf19 ?x_30)
              (LeftOf7 ?x_30)
            )
          )
          (exists (?x_17 )
            (and
              (RightOf4 ?x_17)
              (LeftOf1 ?x_17)
            )
          )
          (exists (?x_33 )
            (and
              (BelowOf4 ?x_33)
              (AboveOf17 ?x_33)
            )
          )
          (exists (?x_21 )
            (and
              (LeftOf2 ?x_21)
              (RightOf18 ?x_21)
            )
          )
          (exists (?x_25 )
            (and
              (LeftOf13 ?x_25)
              (RightOf17 ?x_25)
            )
          )
          (exists (?x_16 )
            (and
              (RightOf10 ?x_16)
              (LeftOf3 ?x_16)
            )
          )
          (exists (?x_6 )
            (and
              (RightOf17 ?x_6)
              (LeftOf11 ?x_6)
            )
          )
          (exists (?x_13 )
            (and
              (RightOf10 ?x_13)
              (LeftOf10 ?x_13)
            )
          )
          (exists (?x_33 )
            (and
              (BelowOf9 ?x_33)
              (AboveOf13 ?x_33)
            )
          )
          (exists (?x_33 )
            (and
              (AboveOf16 ?x_33)
              (BelowOf6 ?x_33)
            )
          )
          (exists (?x_12 )
            (and
              (LeftOf1 ?x_12)
              (RightOf10 ?x_12)
            )
          )
          (exists (?x_12 )
            (and
              (RightOf10 ?x_12)
              (LeftOf6 ?x_12)
            )
          )
          (exists (?x_12 )
            (and
              (LeftOf1 ?x_12)
              (RightOf11 ?x_12)
            )
          )
          (exists (?x_12 )
            (and
              (RightOf15 ?x_12)
              (LeftOf2 ?x_12)
            )
          )
          (exists (?x_7 )
            (and
              (BelowOf1 ?x_7)
              (AboveOf13 ?x_7)
            )
          )
          (exists (?x_18 )
            (and
              (RightOf16 ?x_18)
              (LeftOf6 ?x_18)
            )
          )
          (exists (?x_27 )
            (and
              (AboveOf16 ?x_27)
              (BelowOf6 ?x_27)
            )
          )
          (exists (?x_31 )
            (and
              (RightOf17 ?x_31)
              (LeftOf9 ?x_31)
            )
          )
          (exists (?x_31 )
            (and
              (RightOf15 ?x_31)
              (LeftOf1 ?x_31)
            )
          )
          (exists (?x_13 )
            (and
              (LeftOf8 ?x_13)
              (RightOf17 ?x_13)
            )
          )
          (exists (?x_32 )
            (and
              (BelowOf3 ?x_32)
              (AboveOf14 ?x_32)
            )
          )
          (exists (?x_24 )
            (and
              (BelowOf13 ?x_24)
              (AboveOf19 ?x_24)
            )
          )
          (exists (?x_22 )
            (and
              (AboveOf13 ?x_22)
              (BelowOf1 ?x_22)
            )
          )
          (exists (?x_34 )
            (and
              (BelowOf8 ?x_34)
              (AboveOf15 ?x_34)
            )
          )
          (exists (?x_33 )
            (and
              (BelowOf1 ?x_33)
              (AboveOf17 ?x_33)
            )
          )
          (exists (?x_33 )
            (and
              (BelowOf7 ?x_33)
              (AboveOf11 ?x_33)
            )
          )
          (exists (?x_27 )
            (and
              (BelowOf2 ?x_27)
              (AboveOf14 ?x_27)
            )
          )
          (exists (?x_37 )
            (and
              (AboveOf5 ?x_37)
              (BelowOf1 ?x_37)
            )
          )
          (exists (?x_31 )
            (and
              (RightOf11 ?x_31)
              (LeftOf4 ?x_31)
            )
          )
          (exists (?x_12 )
            (and
              (LeftOf7 ?x_12)
              (RightOf11 ?x_12)
            )
          )
          (exists (?x_18 )
            (and
              (LeftOf10 ?x_18)
              (RightOf16 ?x_18)
            )
          )
          (exists (?x_35 )
            (and
              (BelowOf12 ?x_35)
              (AboveOf19 ?x_35)
            )
          )
          (exists (?x_21 )
            (and
              (LeftOf4 ?x_21)
              (RightOf9 ?x_21)
            )
          )
          (exists (?x_7 )
            (and
              (AboveOf10 ?x_7)
              (BelowOf3 ?x_7)
            )
          )
          (exists (?x_28 )
            (and
              (RightOf15 ?x_28)
              (LeftOf2 ?x_28)
            )
          )
          (exists (?x_28 )
            (and
              (RightOf7 ?x_28)
              (LeftOf1 ?x_28)
            )
          )
          (exists (?x_13 )
            (and
              (LeftOf4 ?x_13)
              (RightOf19 ?x_13)
            )
          )
          (exists (?x_21 )
            (and
              (RightOf16 ?x_21)
              (LeftOf5 ?x_21)
            )
          )
          (exists (?x_21 )
            (and
              (LeftOf4 ?x_21)
              (RightOf18 ?x_21)
            )
          )
          (exists (?x_35 )
            (and
              (BelowOf3 ?x_35)
              (AboveOf18 ?x_35)
            )
          )
          (exists (?x_35 )
            (and
              (AboveOf13 ?x_35)
              (BelowOf10 ?x_35)
            )
          )
          (exists (?x_10 )
            (and
              (AboveOf4 ?x_10)
              (BelowOf1 ?x_10)
            )
          )
          (exists (?x_31 )
            (and
              (LeftOf6 ?x_31)
              (RightOf11 ?x_31)
            )
          )
          (exists (?x_18 )
            (and
              (LeftOf5 ?x_18)
              (RightOf17 ?x_18)
            )
          )
          (exists (?x_14 )
            (and
              (RightOf14 ?x_14)
              (LeftOf3 ?x_14)
            )
          )
          (exists (?x_24 )
            (and
              (BelowOf7 ?x_24)
              (AboveOf18 ?x_24)
            )
          )
          (exists (?x_5 )
            (and
              (LeftOf2 ?x_5)
              (RightOf17 ?x_5)
            )
          )
          (exists (?x_24 )
            (and
              (BelowOf4 ?x_24)
              (AboveOf16 ?x_24)
            )
          )
          (exists (?x_29 )
            (and
              (BelowOf6 ?x_29)
              (AboveOf8 ?x_29)
            )
          )
          (exists (?x_14 )
            (and
              (RightOf14 ?x_14)
              (LeftOf5 ?x_14)
            )
          )
          (exists (?x_11 )
            (and
              (AboveOf16 ?x_11)
              (BelowOf10 ?x_11)
            )
          )
          (exists (?x_29 )
            (and
              (BelowOf6 ?x_29)
              (AboveOf14 ?x_29)
            )
          )
          (exists (?x_35 )
            (and
              (AboveOf19 ?x_35)
              (BelowOf1 ?x_35)
            )
          )
          (exists (?x_37 )
            (and
              (AboveOf5 ?x_37)
              (BelowOf2 ?x_37)
            )
          )
          (exists (?x_14 )
            (and
              (RightOf16 ?x_14)
              (LeftOf10 ?x_14)
            )
          )
          (exists (?x_23 )
            (and
              (RightOf19 ?x_23)
              (LeftOf8 ?x_23)
            )
          )
          (exists (?x_34 )
            (and
              (BelowOf5 ?x_34)
              (AboveOf16 ?x_34)
            )
          )
          (exists (?x_5 )
            (and
              (RightOf19 ?x_5)
              (LeftOf5 ?x_5)
            )
          )
          (exists (?x_6 )
            (and
              (RightOf19 ?x_6)
              (LeftOf9 ?x_6)
            )
          )
          (exists (?x_25 )
            (and
              (RightOf19 ?x_25)
              (LeftOf10 ?x_25)
            )
          )
          (exists (?x_22 )
            (and
              (BelowOf2 ?x_22)
              (AboveOf18 ?x_22)
            )
          )
          (exists (?x_14 )
            (and
              (RightOf14 ?x_14)
              (LeftOf6 ?x_14)
            )
          )
          (exists (?x_1 )
            (and
              (AboveOf18 ?x_1)
              (BelowOf1 ?x_1)
            )
          )
          (exists (?x_2 )
            (and
              (RightOf8 ?x_2)
              (LeftOf4 ?x_2)
            )
          )
          (exists (?x_2 )
            (and
              (RightOf16 ?x_2)
              (LeftOf5 ?x_2)
            )
          )
          (exists (?x_20 )
            (and
              (RightOf15 ?x_20)
              (LeftOf10 ?x_20)
            )
          )
          (exists (?x_15 )
            (and
              (BelowOf2 ?x_15)
              (AboveOf16 ?x_15)
            )
          )
          (exists (?x_22 )
            (and
              (BelowOf3 ?x_22)
              (AboveOf18 ?x_22)
            )
          )
          (exists (?x_35 )
            (and
              (AboveOf19 ?x_35)
              (BelowOf9 ?x_35)
            )
          )
          (exists (?x_12 )
            (and
              (LeftOf7 ?x_12)
              (RightOf16 ?x_12)
            )
          )
          (exists (?x_9 )
            (and
              (BelowOf6 ?x_9)
              (AboveOf18 ?x_9)
            )
          )
          (exists (?x_24 )
            (and
              (AboveOf19 ?x_24)
              (BelowOf6 ?x_24)
            )
          )
          (exists (?x_6 )
            (and
              (LeftOf2 ?x_6)
              (RightOf16 ?x_6)
            )
          )
          (exists (?x_9 )
            (and
              (AboveOf19 ?x_9)
              (BelowOf1 ?x_9)
            )
          )
          (exists (?x_10 )
            (and
              (AboveOf19 ?x_10)
              (BelowOf2 ?x_10)
            )
          )
          (exists (?x_9 )
            (and
              (AboveOf19 ?x_9)
              (BelowOf5 ?x_9)
            )
          )
          (exists (?x_23 )
            (and
              (LeftOf4 ?x_23)
              (RightOf14 ?x_23)
            )
          )
          (exists (?x_4 )
            (and
              (RightOf18 ?x_4)
              (LeftOf10 ?x_4)
            )
          )
          (exists (?x_24 )
            (and
              (AboveOf18 ?x_24)
              (BelowOf14 ?x_24)
            )
          )
          (exists (?x_22 )
            (and
              (AboveOf18 ?x_22)
              (BelowOf4 ?x_22)
            )
          )
          (exists (?x_35 )
            (and
              (BelowOf8 ?x_35)
              (AboveOf13 ?x_35)
            )
          )
          (exists (?x_21 )
            (and
              (LeftOf4 ?x_21)
              (RightOf14 ?x_21)
            )
          )
          (exists (?x_13 )
            (and
              (LeftOf8 ?x_13)
              (RightOf19 ?x_13)
            )
          )
          (exists (?x_16 )
            (and
              (RightOf11 ?x_16)
              (LeftOf1 ?x_16)
            )
          )
          (exists (?x_37 )
            (and
              (BelowOf3 ?x_37)
              (AboveOf9 ?x_37)
            )
          )
          (exists (?x_34 )
            (and
              (AboveOf15 ?x_34)
              (BelowOf10 ?x_34)
            )
          )
          (exists (?x_24 )
            (and
              (AboveOf15 ?x_24)
              (BelowOf5 ?x_24)
            )
          )
          (exists (?x_32 )
            (and
              (AboveOf15 ?x_32)
              (BelowOf3 ?x_32)
            )
          )
          (exists (?x_14 )
            (and
              (RightOf19 ?x_14)
              (LeftOf3 ?x_14)
            )
          )
          (exists (?x_22 )
            (and
              (BelowOf10 ?x_22)
              (AboveOf16 ?x_22)
            )
          )
          (exists (?x_20 )
            (and
              (LeftOf9 ?x_20)
              (RightOf15 ?x_20)
            )
          )
          (exists (?x_10 )
            (and
              (BelowOf2 ?x_10)
              (AboveOf18 ?x_10)
            )
          )
          (exists (?x_12 )
            (and
              (LeftOf4 ?x_12)
              (RightOf13 ?x_12)
            )
          )
          (exists (?x_31 )
            (and
              (LeftOf9 ?x_31)
              (RightOf10 ?x_31)
            )
          )
          (exists (?x_13 )
            (and
              (RightOf18 ?x_13)
              (LeftOf6 ?x_13)
            )
          )
          (exists (?x_21 )
            (and
              (LeftOf2 ?x_21)
              (RightOf15 ?x_21)
            )
          )
          (exists (?x_26 )
            (and
              (RightOf19 ?x_26)
              (LeftOf15 ?x_26)
            )
          )
          (exists (?x_3 )
            (and
              (AboveOf17 ?x_3)
              (BelowOf2 ?x_3)
            )
          )
          (exists (?x_31 )
            (and
              (RightOf17 ?x_31)
              (LeftOf8 ?x_31)
            )
          )
          (exists (?x_35 )
            (and
              (AboveOf14 ?x_35)
              (BelowOf11 ?x_35)
            )
          )
          (exists (?x_37 )
            (and
              (BelowOf3 ?x_37)
              (AboveOf17 ?x_37)
            )
          )
          (exists (?x_32 )
            (and
              (AboveOf18 ?x_32)
              (BelowOf5 ?x_32)
            )
          )
          (exists (?x_22 )
            (and
              (BelowOf6 ?x_22)
              (AboveOf14 ?x_22)
            )
          )
          (exists (?x_20 )
            (and
              (LeftOf6 ?x_20)
              (RightOf19 ?x_20)
            )
          )
          (exists (?x_35 )
            (and
              (BelowOf11 ?x_35)
              (AboveOf15 ?x_35)
            )
          )
          (exists (?x_6 )
            (and
              (RightOf16 ?x_6)
              (LeftOf7 ?x_6)
            )
          )
          (exists (?x_13 )
            (and
              (RightOf16 ?x_13)
              (LeftOf10 ?x_13)
            )
          )
          (exists (?x_31 )
            (and
              (LeftOf6 ?x_31)
              (RightOf13 ?x_31)
            )
          )
          (exists (?x_37 )
            (and
              (AboveOf8 ?x_37)
              (BelowOf2 ?x_37)
            )
          )
          (exists (?x_29 )
            (and
              (BelowOf7 ?x_29)
              (AboveOf12 ?x_29)
            )
          )
          (exists (?x_2 )
            (and
              (LeftOf1 ?x_2)
              (RightOf14 ?x_2)
            )
          )
          (exists (?x_2 )
            (and
              (LeftOf2 ?x_2)
              (RightOf18 ?x_2)
            )
          )
          (exists (?x_29 )
            (and
              (BelowOf6 ?x_29)
              (AboveOf9 ?x_29)
            )
          )
          (exists (?x_27 )
            (and
              (AboveOf19 ?x_27)
              (BelowOf5 ?x_27)
            )
          )
          (exists (?x_35 )
            (and
              (BelowOf7 ?x_35)
              (AboveOf15 ?x_35)
            )
          )
          (exists (?x_19 )
            (and
              (AboveOf19 ?x_19)
              (BelowOf15 ?x_19)
            )
          )
          (exists (?x_13 )
            (and
              (RightOf10 ?x_13)
              (LeftOf8 ?x_13)
            )
          )
          (exists (?x_14 )
            (and
              (LeftOf6 ?x_14)
              (RightOf13 ?x_14)
            )
          )
          (exists (?x_13 )
            (and
              (RightOf19 ?x_13)
              (LeftOf6 ?x_13)
            )
          )
          (exists (?x_12 )
            (and
              (LeftOf4 ?x_12)
              (RightOf7 ?x_12)
            )
          )
          (exists (?x_33 )
            (and
              (BelowOf7 ?x_33)
              (AboveOf12 ?x_33)
            )
          )
          (exists (?x_15 )
            (and
              (AboveOf18 ?x_15)
              (BelowOf2 ?x_15)
            )
          )
          (exists (?x_33 )
            (and
              (AboveOf9 ?x_33)
              (BelowOf3 ?x_33)
            )
          )
          (exists (?x_18 )
            (and
              (RightOf15 ?x_18)
              (LeftOf9 ?x_18)
            )
          )
          (exists (?x_12 )
            (and
              (RightOf11 ?x_12)
              (LeftOf3 ?x_12)
            )
          )
          (exists (?x_3 )
            (and
              (AboveOf17 ?x_3)
              (BelowOf7 ?x_3)
            )
          )
          (exists (?x_31 )
            (and
              (LeftOf6 ?x_31)
              (RightOf10 ?x_31)
            )
          )
          (exists (?x_2 )
            (and
              (RightOf12 ?x_2)
              (LeftOf5 ?x_2)
            )
          )
          (exists (?x_20 )
            (and
              (LeftOf8 ?x_20)
              (RightOf11 ?x_20)
            )
          )
          (exists (?x_26 )
            (and
              (RightOf18 ?x_26)
              (LeftOf4 ?x_26)
            )
          )
          (exists (?x_2 )
            (and
              (LeftOf2 ?x_2)
              (RightOf17 ?x_2)
            )
          )
          (exists (?x_5 )
            (and
              (LeftOf5 ?x_5)
              (RightOf12 ?x_5)
            )
          )
          (exists (?x_11 )
            (and
              (BelowOf8 ?x_11)
              (AboveOf13 ?x_11)
            )
          )
          (exists (?x_18 )
            (and
              (LeftOf1 ?x_18)
              (RightOf16 ?x_18)
            )
          )
          (exists (?x_32 )
            (and
              (BelowOf6 ?x_32)
              (AboveOf12 ?x_32)
            )
          )
          (exists (?x_20 )
            (and
              (LeftOf9 ?x_20)
              (RightOf12 ?x_20)
            )
          )
          (exists (?x_32 )
            (and
              (BelowOf7 ?x_32)
              (AboveOf16 ?x_32)
            )
          )
          (exists (?x_2 )
            (and
              (LeftOf1 ?x_2)
              (RightOf9 ?x_2)
            )
          )
          (exists (?x_11 )
            (and
              (AboveOf17 ?x_11)
              (BelowOf11 ?x_11)
            )
          )
          (exists (?x_32 )
            (and
              (BelowOf1 ?x_32)
              (AboveOf12 ?x_32)
            )
          )
          (exists (?x_9 )
            (and
              (BelowOf5 ?x_9)
              (AboveOf18 ?x_9)
            )
          )
          (exists (?x_24 )
            (and
              (AboveOf15 ?x_24)
              (BelowOf6 ?x_24)
            )
          )
          (exists (?x_12 )
            (and
              (RightOf17 ?x_12)
              (LeftOf6 ?x_12)
            )
          )
          (exists (?x_32 )
            (and
              (BelowOf4 ?x_32)
              (AboveOf19 ?x_32)
            )
          )
          (exists (?x_14 )
            (and
              (LeftOf6 ?x_14)
              (RightOf16 ?x_14)
            )
          )
          (exists (?x_22 )
            (and
              (BelowOf6 ?x_22)
              (AboveOf13 ?x_22)
            )
          )
          (exists (?x_17 )
            (and
              (RightOf13 ?x_17)
              (LeftOf1 ?x_17)
            )
          )
          (exists (?x_18 )
            (and
              (RightOf19 ?x_18)
              (LeftOf6 ?x_18)
            )
          )
          (exists (?x_34 )
            (and
              (AboveOf19 ?x_34)
              (BelowOf10 ?x_34)
            )
          )
          (exists (?x_19 )
            (and
              (BelowOf13 ?x_19)
              (AboveOf18 ?x_19)
            )
          )
          (exists (?x_25 )
            (and
              (RightOf17 ?x_25)
              (LeftOf8 ?x_25)
            )
          )
          (exists (?x_33 )
            (and
              (BelowOf6 ?x_33)
              (AboveOf19 ?x_33)
            )
          )
          (exists (?x_23 )
            (and
              (RightOf10 ?x_23)
              (LeftOf6 ?x_23)
            )
          )
          (exists (?x_22 )
            (and
              (AboveOf18 ?x_22)
              (BelowOf9 ?x_22)
            )
          )
          (exists (?x_28 )
            (and
              (RightOf16 ?x_28)
              (LeftOf1 ?x_28)
            )
          )
          (exists (?x_23 )
            (and
              (RightOf10 ?x_23)
              (LeftOf3 ?x_23)
            )
          )
          (exists (?x_10 )
            (and
              (AboveOf13 ?x_10)
              (BelowOf1 ?x_10)
            )
          )
          (exists (?x_34 )
            (and
              (AboveOf14 ?x_34)
              (BelowOf14 ?x_34)
            )
          )
          (exists (?x_31 )
            (and
              (LeftOf6 ?x_31)
              (RightOf19 ?x_31)
            )
          )
          (exists (?x_28 )
            (and
              (RightOf14 ?x_28)
              (LeftOf2 ?x_28)
            )
          )
          (exists (?x_13 )
            (and
              (LeftOf6 ?x_13)
              (RightOf12 ?x_13)
            )
          )
          (exists (?x_23 )
            (and
              (RightOf12 ?x_23)
              (LeftOf6 ?x_23)
            )
          )
          (exists (?x_3 )
            (and
              (BelowOf10 ?x_3)
              (AboveOf18 ?x_3)
            )
          )
          (exists (?x_13 )
            (and
              (LeftOf4 ?x_13)
              (RightOf14 ?x_13)
            )
          )
          (exists (?x_9 )
            (and
              (AboveOf17 ?x_9)
              (BelowOf13 ?x_9)
            )
          )
          (exists (?x_31 )
            (and
              (RightOf11 ?x_31)
              (LeftOf9 ?x_31)
            )
          )
          (exists (?x_5 )
            (and
              (LeftOf7 ?x_5)
              (RightOf13 ?x_5)
            )
          )
          (exists (?x_13 )
            (and
              (RightOf16 ?x_13)
              (LeftOf3 ?x_13)
            )
          )
          (exists (?x_26 )
            (and
              (RightOf19 ?x_26)
              (LeftOf4 ?x_26)
            )
          )
          (exists (?x_37 )
            (and
              (AboveOf16 ?x_37)
              (BelowOf4 ?x_37)
            )
          )
          (exists (?x_21 )
            (and
              (LeftOf2 ?x_21)
              (RightOf9 ?x_21)
            )
          )
          (exists (?x_32 )
            (and
              (AboveOf11 ?x_32)
              (BelowOf3 ?x_32)
            )
          )
          (exists (?x_24 )
            (and
              (AboveOf18 ?x_24)
              (BelowOf8 ?x_24)
            )
          )
          (exists (?x_27 )
            (and
              (BelowOf5 ?x_27)
              (AboveOf17 ?x_27)
            )
          )
          (exists (?x_18 )
            (and
              (LeftOf6 ?x_18)
              (RightOf16 ?x_18)
            )
          )
          (exists (?x_32 )
            (and
              (BelowOf9 ?x_32)
              (AboveOf11 ?x_32)
            )
          )
          (exists (?x_25 )
            (and
              (RightOf17 ?x_25)
              (LeftOf12 ?x_25)
            )
          )
          (exists (?x_22 )
            (and
              (AboveOf14 ?x_22)
              (BelowOf13 ?x_22)
            )
          )
          (exists (?x_20 )
            (and
              (RightOf17 ?x_20)
              (LeftOf8 ?x_20)
            )
          )
          (exists (?x_21 )
            (and
              (RightOf16 ?x_21)
              (LeftOf4 ?x_21)
            )
          )
          (exists (?x_23 )
            (and
              (LeftOf1 ?x_23)
              (RightOf14 ?x_23)
            )
          )
          (exists (?x_29 )
            (and
              (BelowOf1 ?x_29)
              (AboveOf12 ?x_29)
            )
          )
          (exists (?x_6 )
            (and
              (LeftOf2 ?x_6)
              (RightOf17 ?x_6)
            )
          )
          (exists (?x_12 )
            (and
              (LeftOf1 ?x_12)
              (RightOf7 ?x_12)
            )
          )
          (exists (?x_14 )
            (and
              (LeftOf6 ?x_14)
              (RightOf18 ?x_14)
            )
          )
          (exists (?x_32 )
            (and
              (AboveOf10 ?x_32)
              (BelowOf6 ?x_32)
            )
          )
          (exists (?x_13 )
            (and
              (RightOf13 ?x_13)
              (LeftOf5 ?x_13)
            )
          )
          (exists (?x_20 )
            (and
              (RightOf15 ?x_20)
              (LeftOf9 ?x_20)
            )
          )
          (exists (?x_25 )
            (and
              (LeftOf8 ?x_25)
              (RightOf15 ?x_25)
            )
          )
          (exists (?x_23 )
            (and
              (RightOf16 ?x_23)
              (LeftOf3 ?x_23)
            )
          )
          (exists (?x_34 )
            (and
              (AboveOf18 ?x_34)
              (BelowOf10 ?x_34)
            )
          )
          (exists (?x_16 )
            (and
              (RightOf4 ?x_16)
              (LeftOf3 ?x_16)
            )
          )
          (exists (?x_27 )
            (and
              (AboveOf18 ?x_27)
              (BelowOf6 ?x_27)
            )
          )
          (exists (?x_34 )
            (and
              (BelowOf4 ?x_34)
              (AboveOf18 ?x_34)
            )
          )
          (exists (?x_32 )
            (and
              (AboveOf18 ?x_32)
              (BelowOf2 ?x_32)
            )
          )
          (exists (?x_27 )
            (and
              (BelowOf1 ?x_27)
              (AboveOf17 ?x_27)
            )
          )
          (exists (?x_2 )
            (and
              (LeftOf6 ?x_2)
              (RightOf7 ?x_2)
            )
          )
          (exists (?x_32 )
            (and
              (BelowOf4 ?x_32)
              (AboveOf12 ?x_32)
            )
          )
          (exists (?x_33 )
            (and
              (BelowOf8 ?x_33)
              (AboveOf17 ?x_33)
            )
          )
          (exists (?x_29 )
            (and
              (AboveOf9 ?x_29)
              (BelowOf8 ?x_29)
            )
          )
          (exists (?x_22 )
            (and
              (BelowOf11 ?x_22)
              (AboveOf16 ?x_22)
            )
          )
          (exists (?x_11 )
            (and
              (AboveOf15 ?x_11)
              (BelowOf7 ?x_11)
            )
          )
          (exists (?x_33 )
            (and
              (AboveOf19 ?x_33)
              (BelowOf8 ?x_33)
            )
          )
          (exists (?x_31 )
            (and
              (RightOf12 ?x_31)
              (LeftOf2 ?x_31)
            )
          )
          (exists (?x_32 )
            (and
              (AboveOf15 ?x_32)
              (BelowOf2 ?x_32)
            )
          )
          (exists (?x_33 )
            (and
              (AboveOf9 ?x_33)
              (BelowOf1 ?x_33)
            )
          )
          (exists (?x_5 )
            (and
              (LeftOf1 ?x_5)
              (RightOf16 ?x_5)
            )
          )
          (exists (?x_17 )
            (and
              (RightOf12 ?x_17)
              (LeftOf1 ?x_17)
            )
          )
          (exists (?x_31 )
            (and
              (LeftOf1 ?x_31)
              (RightOf10 ?x_31)
            )
          )
          (exists (?x_29 )
            (and
              (BelowOf7 ?x_29)
              (AboveOf13 ?x_29)
            )
          )
          (exists (?x_6 )
            (and
              (RightOf17 ?x_6)
              (LeftOf9 ?x_6)
            )
          )
          (exists (?x_22 )
            (and
              (BelowOf8 ?x_22)
              (AboveOf15 ?x_22)
            )
          )
          (exists (?x_15 )
            (and
              (AboveOf12 ?x_15)
              (BelowOf2 ?x_15)
            )
          )
          (exists (?x_32 )
            (and
              (BelowOf4 ?x_32)
              (AboveOf14 ?x_32)
            )
          )
          (exists (?x_34 )
            (and
              (AboveOf16 ?x_34)
              (BelowOf2 ?x_34)
            )
          )
          (exists (?x_37 )
            (and
              (AboveOf11 ?x_37)
              (BelowOf4 ?x_37)
            )
          )
          (exists (?x_35 )
            (and
              (AboveOf12 ?x_35)
              (BelowOf6 ?x_35)
            )
          )
          (exists (?x_14 )
            (and
              (LeftOf4 ?x_14)
              (RightOf14 ?x_14)
            )
          )
          (exists (?x_27 )
            (and
              (AboveOf11 ?x_27)
              (BelowOf3 ?x_27)
            )
          )
          (exists (?x_35 )
            (and
              (BelowOf8 ?x_35)
              (AboveOf16 ?x_35)
            )
          )
          (exists (?x_25 )
            (and
              (RightOf17 ?x_25)
              (LeftOf1 ?x_25)
            )
          )
          (exists (?x_14 )
            (and
              (LeftOf3 ?x_14)
              (RightOf18 ?x_14)
            )
          )
          (exists (?x_11 )
            (and
              (BelowOf1 ?x_11)
              (AboveOf15 ?x_11)
            )
          )
          (exists (?x_11 )
            (and
              (BelowOf11 ?x_11)
              (AboveOf19 ?x_11)
            )
          )
          (exists (?x_21 )
            (and
              (RightOf6 ?x_21)
              (LeftOf5 ?x_21)
            )
          )
          (exists (?x_0 )
            (and
              (AboveOf3 ?x_0)
              (BelowOf1 ?x_0)
            )
          )
          (exists (?x_21 )
            (and
              (LeftOf4 ?x_21)
              (RightOf11 ?x_21)
            )
          )
          (exists (?x_2 )
            (and
              (RightOf13 ?x_2)
              (LeftOf5 ?x_2)
            )
          )
          (exists (?x_11 )
            (and
              (AboveOf13 ?x_11)
              (BelowOf10 ?x_11)
            )
          )
          (exists (?x_2 )
            (and
              (RightOf16 ?x_2)
              (LeftOf4 ?x_2)
            )
          )
          (exists (?x_2 )
            (and
              (RightOf9 ?x_2)
              (LeftOf3 ?x_2)
            )
          )
          (exists (?x_12 )
            (and
              (RightOf13 ?x_12)
              (LeftOf6 ?x_12)
            )
          )
          (exists (?x_27 )
            (and
              (BelowOf1 ?x_27)
              (AboveOf11 ?x_27)
            )
          )
          (exists (?x_12 )
            (and
              (LeftOf4 ?x_12)
              (RightOf11 ?x_12)
            )
          )
          (exists (?x_31 )
            (and
              (LeftOf2 ?x_31)
              (RightOf16 ?x_31)
            )
          )
          (exists (?x_11 )
            (and
              (AboveOf11 ?x_11)
              (BelowOf6 ?x_11)
            )
          )
          (exists (?x_21 )
            (and
              (RightOf7 ?x_21)
              (LeftOf3 ?x_21)
            )
          )
          (exists (?x_5 )
            (and
              (RightOf19 ?x_5)
              (LeftOf2 ?x_5)
            )
          )
          (exists (?x_25 )
            (and
              (LeftOf13 ?x_25)
              (RightOf15 ?x_25)
            )
          )
          (exists (?x_32 )
            (and
              (BelowOf4 ?x_32)
              (AboveOf16 ?x_32)
            )
          )
          (exists (?x_19 )
            (and
              (AboveOf19 ?x_19)
              (BelowOf16 ?x_19)
            )
          )
          (exists (?x_5 )
            (and
              (LeftOf6 ?x_5)
              (RightOf13 ?x_5)
            )
          )
          (exists (?x_2 )
            (and
              (RightOf14 ?x_2)
              (LeftOf5 ?x_2)
            )
          )
          (exists (?x_3 )
            (and
              (AboveOf16 ?x_3)
              (BelowOf13 ?x_3)
            )
          )
          (exists (?x_13 )
            (and
              (LeftOf5 ?x_13)
              (RightOf12 ?x_13)
            )
          )
          (exists (?x_11 )
            (and
              (AboveOf11 ?x_11)
              (BelowOf4 ?x_11)
            )
          )
          (exists (?x_3 )
            (and
              (AboveOf19 ?x_3)
              (BelowOf9 ?x_3)
            )
          )
          (exists (?x_33 )
            (and
              (AboveOf10 ?x_33)
              (BelowOf7 ?x_33)
            )
          )
          (exists (?x_29 )
            (and
              (BelowOf6 ?x_29)
              (AboveOf10 ?x_29)
            )
          )
          (exists (?x_32 )
            (and
              (AboveOf10 ?x_32)
              (BelowOf8 ?x_32)
            )
          )
          (exists (?x_14 )
            (and
              (RightOf19 ?x_14)
              (LeftOf13 ?x_14)
            )
          )
          (exists (?x_5 )
            (and
              (LeftOf12 ?x_5)
              (RightOf18 ?x_5)
            )
          )
          (exists (?x_20 )
            (and
              (RightOf13 ?x_20)
              (LeftOf1 ?x_20)
            )
          )
          (exists (?x_18 )
            (and
              (RightOf15 ?x_18)
              (LeftOf4 ?x_18)
            )
          )
          (exists (?x_16 )
            (and
              (RightOf9 ?x_16)
              (LeftOf1 ?x_16)
            )
          )
          (exists (?x_22 )
            (and
              (AboveOf14 ?x_22)
              (BelowOf1 ?x_22)
            )
          )
          (exists (?x_28 )
            (and
              (LeftOf2 ?x_28)
              (RightOf12 ?x_28)
            )
          )
          (exists (?x_16 )
            (and
              (RightOf6 ?x_16)
              (LeftOf1 ?x_16)
            )
          )
          (exists (?x_35 )
            (and
              (AboveOf14 ?x_35)
              (BelowOf7 ?x_35)
            )
          )
          (exists (?x_29 )
            (and
              (AboveOf14 ?x_29)
              (BelowOf2 ?x_29)
            )
          )
          (exists (?x_2 )
            (and
              (LeftOf1 ?x_2)
              (RightOf7 ?x_2)
            )
          )
          (exists (?x_22 )
            (and
              (BelowOf3 ?x_22)
              (AboveOf14 ?x_22)
            )
          )
          (exists (?x_4 )
            (and
              (RightOf18 ?x_4)
              (LeftOf1 ?x_4)
            )
          )
          (exists (?x_35 )
            (and
              (BelowOf12 ?x_35)
              (AboveOf14 ?x_35)
            )
          )
          (exists (?x_22 )
            (and
              (AboveOf14 ?x_22)
              (BelowOf6 ?x_22)
            )
          )
          (exists (?x_11 )
            (and
              (AboveOf12 ?x_11)
              (BelowOf3 ?x_11)
            )
          )
          (exists (?x_10 )
            (and
              (BelowOf2 ?x_10)
              (AboveOf10 ?x_10)
            )
          )
          (exists (?x_37 )
            (and
              (AboveOf10 ?x_37)
              (BelowOf2 ?x_37)
            )
          )
          (exists (?x_2 )
            (and
              (RightOf14 ?x_2)
              (LeftOf3 ?x_2)
            )
          )
          (exists (?x_7 )
            (and
              (AboveOf16 ?x_7)
              (BelowOf2 ?x_7)
            )
          )
          (exists (?x_18 )
            (and
              (LeftOf10 ?x_18)
              (RightOf19 ?x_18)
            )
          )
          (exists (?x_29 )
            (and
              (AboveOf14 ?x_29)
              (BelowOf8 ?x_29)
            )
          )
          (exists (?x_16 )
            (and
              (RightOf5 ?x_16)
              (LeftOf1 ?x_16)
            )
          )
          (exists (?x_29 )
            (and
              (BelowOf8 ?x_29)
              (AboveOf12 ?x_29)
            )
          )
          (exists (?x_37 )
            (and
              (AboveOf8 ?x_37)
              (BelowOf4 ?x_37)
            )
          )
          (exists (?x_12 )
            (and
              (RightOf18 ?x_12)
              (LeftOf4 ?x_12)
            )
          )
          (exists (?x_35 )
            (and
              (AboveOf19 ?x_35)
              (BelowOf7 ?x_35)
            )
          )
          (exists (?x_32 )
            (and
              (BelowOf7 ?x_32)
              (AboveOf15 ?x_32)
            )
          )
          (exists (?x_1 )
            (and
              (BelowOf1 ?x_1)
              (AboveOf8 ?x_1)
            )
          )
          (exists (?x_37 )
            (and
              (BelowOf2 ?x_37)
              (AboveOf15 ?x_37)
            )
          )
          (exists (?x_25 )
            (and
              (RightOf16 ?x_25)
              (LeftOf1 ?x_25)
            )
          )
          (exists (?x_31 )
            (and
              (RightOf12 ?x_31)
              (LeftOf1 ?x_31)
            )
          )
          (exists (?x_12 )
            (and
              (RightOf7 ?x_12)
              (LeftOf6 ?x_12)
            )
          )
          (exists (?x_33 )
            (and
              (BelowOf7 ?x_33)
              (AboveOf15 ?x_33)
            )
          )
          (exists (?x_22 )
            (and
              (AboveOf19 ?x_22)
              (BelowOf8 ?x_22)
            )
          )
          (exists (?x_25 )
            (and
              (RightOf17 ?x_25)
              (LeftOf4 ?x_25)
            )
          )
          (exists (?x_35 )
            (and
              (BelowOf6 ?x_35)
              (AboveOf19 ?x_35)
            )
          )
          (exists (?x_4 )
            (and
              (RightOf18 ?x_4)
              (LeftOf4 ?x_4)
            )
          )
          (exists (?x_5 )
            (and
              (LeftOf11 ?x_5)
              (RightOf18 ?x_5)
            )
          )
          (exists (?x_16 )
            (and
              (RightOf12 ?x_16)
              (LeftOf2 ?x_16)
            )
          )
          (exists (?x_6 )
            (and
              (RightOf16 ?x_6)
              (LeftOf1 ?x_6)
            )
          )
          (exists (?x_4 )
            (and
              (RightOf17 ?x_4)
              (LeftOf16 ?x_4)
            )
          )
          (exists (?x_10 )
            (and
              (AboveOf12 ?x_10)
              (BelowOf4 ?x_10)
            )
          )
          (exists (?x_20 )
            (and
              (RightOf15 ?x_20)
              (LeftOf3 ?x_20)
            )
          )
          (exists (?x_4 )
            (and
              (LeftOf10 ?x_4)
              (RightOf18 ?x_4)
            )
          )
          (exists (?x_7 )
            (and
              (BelowOf4 ?x_7)
              (AboveOf17 ?x_7)
            )
          )
          (exists (?x_12 )
            (and
              (LeftOf7 ?x_12)
              (RightOf13 ?x_12)
            )
          )
          (exists (?x_29 )
            (and
              (BelowOf6 ?x_29)
              (AboveOf18 ?x_29)
            )
          )
          (exists (?x_0 )
            (and
              (BelowOf3 ?x_0)
              (AboveOf19 ?x_0)
            )
          )
          (exists (?x_20 )
            (and
              (LeftOf1 ?x_20)
              (RightOf17 ?x_20)
            )
          )
          (exists (?x_14 )
            (and
              (LeftOf1 ?x_14)
              (RightOf15 ?x_14)
            )
          )
          (exists (?x_35 )
            (and
              (BelowOf12 ?x_35)
              (AboveOf15 ?x_35)
            )
          )
          (exists (?x_8 )
            (and
              (LeftOf2 ?x_8)
              (RightOf10 ?x_8)
            )
          )
          (exists (?x_28 )
            (and
              (RightOf2 ?x_28)
              (LeftOf1 ?x_28)
            )
          )
          (exists (?x_31 )
            (and
              (LeftOf5 ?x_31)
              (RightOf15 ?x_31)
            )
          )
          (exists (?x_34 )
            (and
              (AboveOf17 ?x_34)
              (BelowOf6 ?x_34)
            )
          )
          (exists (?x_25 )
            (and
              (RightOf18 ?x_25)
              (LeftOf14 ?x_25)
            )
          )
          (exists (?x_14 )
            (and
              (RightOf18 ?x_14)
              (LeftOf2 ?x_14)
            )
          )
          (exists (?x_9 )
            (and
              (AboveOf19 ?x_9)
              (BelowOf16 ?x_9)
            )
          )
          (exists (?x_10 )
            (and
              (BelowOf3 ?x_10)
              (AboveOf7 ?x_10)
            )
          )
          (exists (?x_33 )
            (and
              (AboveOf15 ?x_33)
              (BelowOf6 ?x_33)
            )
          )
          (exists (?x_11 )
            (and
              (BelowOf11 ?x_11)
              (AboveOf13 ?x_11)
            )
          )
          (exists (?x_32 )
            (and
              (AboveOf19 ?x_32)
              (BelowOf8 ?x_32)
            )
          )
          (exists (?x_29 )
            (and
              (AboveOf12 ?x_29)
              (BelowOf2 ?x_29)
            )
          )
          (exists (?x_5 )
            (and
              (LeftOf7 ?x_5)
              (RightOf12 ?x_5)
            )
          )
          (exists (?x_10 )
            (and
              (AboveOf12 ?x_10)
              (BelowOf2 ?x_10)
            )
          )
          (exists (?x_11 )
            (and
              (AboveOf12 ?x_11)
              (BelowOf2 ?x_11)
            )
          )
          (exists (?x_2 )
            (and
              (RightOf12 ?x_2)
              (LeftOf3 ?x_2)
            )
          )
          (exists (?x_20 )
            (and
              (LeftOf11 ?x_20)
              (RightOf17 ?x_20)
            )
          )
          (exists (?x_25 )
            (and
              (LeftOf13 ?x_25)
              (RightOf16 ?x_25)
            )
          )
          (exists (?x_25 )
            (and
              (RightOf19 ?x_25)
              (LeftOf8 ?x_25)
            )
          )
          (exists (?x_29 )
            (and
              (AboveOf8 ?x_29)
              (BelowOf2 ?x_29)
            )
          )
          (exists (?x_25 )
            (and
              (LeftOf5 ?x_25)
              (RightOf16 ?x_25)
            )
          )
          (exists (?x_5 )
            (and
              (RightOf13 ?x_5)
              (LeftOf2 ?x_5)
            )
          )
          (exists (?x_8 )
            (and
              (RightOf5 ?x_8)
              (LeftOf2 ?x_8)
            )
          )
          (exists (?x_23 )
            (and
              (LeftOf1 ?x_23)
              (RightOf13 ?x_23)
            )
          )
          (exists (?x_22 )
            (and
              (AboveOf14 ?x_22)
              (BelowOf10 ?x_22)
            )
          )
          (exists (?x_20 )
            (and
              (LeftOf7 ?x_20)
              (RightOf11 ?x_20)
            )
          )
          (exists (?x_23 )
            (and
              (LeftOf3 ?x_23)
              (RightOf17 ?x_23)
            )
          )
          (exists (?x_16 )
            (and
              (RightOf12 ?x_16)
              (LeftOf1 ?x_16)
            )
          )
          (exists (?x_20 )
            (and
              (RightOf16 ?x_20)
              (LeftOf9 ?x_20)
            )
          )
          (exists (?x_7 )
            (and
              (AboveOf11 ?x_7)
              (BelowOf2 ?x_7)
            )
          )
          (exists (?x_24 )
            (and
              (AboveOf17 ?x_24)
              (BelowOf3 ?x_24)
            )
          )
          (exists (?x_0 )
            (and
              (BelowOf1 ?x_0)
              (AboveOf10 ?x_0)
            )
          )
          (exists (?x_32 )
            (and
              (BelowOf2 ?x_32)
              (AboveOf14 ?x_32)
            )
          )
          (exists (?x_31 )
            (and
              (RightOf9 ?x_31)
              (LeftOf9 ?x_31)
            )
          )
          (exists (?x_33 )
            (and
              (AboveOf16 ?x_33)
              (BelowOf4 ?x_33)
            )
          )
          (exists (?x_31 )
            (and
              (RightOf16 ?x_31)
              (LeftOf4 ?x_31)
            )
          )
          (exists (?x_26 )
            (and
              (RightOf19 ?x_26)
              (LeftOf5 ?x_26)
            )
          )
          (exists (?x_37 )
            (and
              (BelowOf5 ?x_37)
              (AboveOf9 ?x_37)
            )
          )
          (exists (?x_26 )
            (and
              (RightOf18 ?x_26)
              (LeftOf14 ?x_26)
            )
          )
          (exists (?x_22 )
            (and
              (BelowOf11 ?x_22)
              (AboveOf15 ?x_22)
            )
          )
          (exists (?x_7 )
            (and
              (BelowOf2 ?x_7)
              (AboveOf15 ?x_7)
            )
          )
          (exists (?x_6 )
            (and
              (LeftOf10 ?x_6)
              (RightOf17 ?x_6)
            )
          )
          (exists (?x_28 )
            (and
              (RightOf15 ?x_28)
              (LeftOf1 ?x_28)
            )
          )
          (exists (?x_5 )
            (and
              (LeftOf3 ?x_5)
              (RightOf18 ?x_5)
            )
          )
          (exists (?x_11 )
            (and
              (AboveOf11 ?x_11)
              (BelowOf1 ?x_11)
            )
          )
          (exists (?x_23 )
            (and
              (LeftOf1 ?x_23)
              (RightOf11 ?x_23)
            )
          )
          (exists (?x_34 )
            (and
              (BelowOf13 ?x_34)
              (AboveOf16 ?x_34)
            )
          )
          (exists (?x_22 )
            (and
              (BelowOf3 ?x_22)
              (AboveOf15 ?x_22)
            )
          )
          (exists (?x_5 )
            (and
              (LeftOf1 ?x_5)
              (RightOf14 ?x_5)
            )
          )
          (exists (?x_12 )
            (and
              (LeftOf7 ?x_12)
              (RightOf14 ?x_12)
            )
          )
          (exists (?x_22 )
            (and
              (AboveOf19 ?x_22)
              (BelowOf4 ?x_22)
            )
          )
          (exists (?x_12 )
            (and
              (LeftOf5 ?x_12)
              (RightOf9 ?x_12)
            )
          )
          (exists (?x_18 )
            (and
              (LeftOf10 ?x_18)
              (RightOf18 ?x_18)
            )
          )
          (exists (?x_14 )
            (and
              (LeftOf4 ?x_14)
              (RightOf16 ?x_14)
            )
          )
          (exists (?x_21 )
            (and
              (LeftOf2 ?x_21)
              (RightOf12 ?x_21)
            )
          )
          (exists (?x_2 )
            (and
              (RightOf16 ?x_2)
              (LeftOf2 ?x_2)
            )
          )
          (exists (?x_33 )
            (and
              (BelowOf3 ?x_33)
              (AboveOf11 ?x_33)
            )
          )
          (exists (?x_3 )
            (and
              (BelowOf4 ?x_3)
              (AboveOf19 ?x_3)
            )
          )
          (exists (?x_34 )
            (and
              (AboveOf14 ?x_34)
              (BelowOf10 ?x_34)
            )
          )
          (exists (?x_31 )
            (and
              (LeftOf6 ?x_31)
              (RightOf18 ?x_31)
            )
          )
          (exists (?x_15 )
            (and
              (BelowOf2 ?x_15)
              (AboveOf7 ?x_15)
            )
          )
          (exists (?x_29 )
            (and
              (BelowOf4 ?x_29)
              (AboveOf12 ?x_29)
            )
          )
          (exists (?x_31 )
            (and
              (LeftOf5 ?x_31)
              (RightOf11 ?x_31)
            )
          )
          (exists (?x_13 )
            (and
              (LeftOf4 ?x_13)
              (RightOf11 ?x_13)
            )
          )
          (exists (?x_6 )
            (and
              (LeftOf6 ?x_6)
              (RightOf16 ?x_6)
            )
          )
          (exists (?x_34 )
            (and
              (AboveOf17 ?x_34)
              (BelowOf10 ?x_34)
            )
          )
          (exists (?x_2 )
            (and
              (LeftOf5 ?x_2)
              (RightOf10 ?x_2)
            )
          )
          (exists (?x_14 )
            (and
              (RightOf18 ?x_14)
              (LeftOf12 ?x_14)
            )
          )
          (exists (?x_25 )
            (and
              (LeftOf15 ?x_25)
              (RightOf16 ?x_25)
            )
          )
          (exists (?x_29 )
            (and
              (AboveOf8 ?x_29)
              (BelowOf3 ?x_29)
            )
          )
          (exists (?x_24 )
            (and
              (AboveOf17 ?x_24)
              (BelowOf6 ?x_24)
            )
          )
          (exists (?x_27 )
            (and
              (AboveOf15 ?x_27)
              (BelowOf1 ?x_27)
            )
          )
          (exists (?x_25 )
            (and
              (LeftOf7 ?x_25)
              (RightOf15 ?x_25)
            )
          )
          (exists (?x_25 )
            (and
              (RightOf17 ?x_25)
              (LeftOf10 ?x_25)
            )
          )
          (exists (?x_18 )
            (and
              (RightOf15 ?x_18)
              (LeftOf7 ?x_18)
            )
          )
          (exists (?x_36 )
            (and
              (AboveOf19 ?x_36)
              (BelowOf13 ?x_36)
            )
          )
          (exists (?x_18 )
            (and
              (RightOf15 ?x_18)
              (LeftOf6 ?x_18)
            )
          )
          (exists (?x_32 )
            (and
              (BelowOf6 ?x_32)
              (AboveOf14 ?x_32)
            )
          )
          (exists (?x_37 )
            (and
              (BelowOf2 ?x_37)
              (AboveOf9 ?x_37)
            )
          )
          (exists (?x_14 )
            (and
              (LeftOf2 ?x_14)
              (RightOf17 ?x_14)
            )
          )
          (exists (?x_13 )
            (and
              (LeftOf8 ?x_13)
              (RightOf14 ?x_13)
            )
          )
          (exists (?x_10 )
            (and
              (AboveOf10 ?x_10)
              (BelowOf2 ?x_10)
            )
          )
          (exists (?x_27 )
            (and
              (BelowOf2 ?x_27)
              (AboveOf12 ?x_27)
            )
          )
          (exists (?x_24 )
            (and
              (AboveOf15 ?x_24)
              (BelowOf3 ?x_24)
            )
          )
          (exists (?x_12 )
            (and
              (LeftOf2 ?x_12)
              (RightOf7 ?x_12)
            )
          )
          (exists (?x_6 )
            (and
              (LeftOf10 ?x_6)
              (RightOf16 ?x_6)
            )
          )
          (exists (?x_31 )
            (and
              (LeftOf9 ?x_31)
              (RightOf15 ?x_31)
            )
          )
          (exists (?x_5 )
            (and
              (RightOf16 ?x_5)
              (LeftOf4 ?x_5)
            )
          )
          (exists (?x_4 )
            (and
              (RightOf18 ?x_4)
              (LeftOf5 ?x_4)
            )
          )
          (exists (?x_25 )
            (and
              (RightOf16 ?x_25)
              (LeftOf11 ?x_25)
            )
          )
          (exists (?x_33 )
            (and
              (BelowOf6 ?x_33)
              (AboveOf12 ?x_33)
            )
          )
          (exists (?x_36 )
            (and
              (AboveOf19 ?x_36)
              (BelowOf11 ?x_36)
            )
          )
          (exists (?x_25 )
            (and
              (LeftOf3 ?x_25)
              (RightOf18 ?x_25)
            )
          )
          (exists (?x_31 )
            (and
              (LeftOf8 ?x_31)
              (RightOf10 ?x_31)
            )
          )
          (exists (?x_22 )
            (and
              (AboveOf19 ?x_22)
              (BelowOf3 ?x_22)
            )
          )
          (exists (?x_32 )
            (and
              (BelowOf10 ?x_32)
              (AboveOf19 ?x_32)
            )
          )
          (exists (?x_29 )
            (and
              (BelowOf1 ?x_29)
              (AboveOf11 ?x_29)
            )
          )
          (exists (?x_32 )
            (and
              (BelowOf7 ?x_32)
              (AboveOf13 ?x_32)
            )
          )
          (exists (?x_31 )
            (and
              (RightOf11 ?x_31)
              (LeftOf1 ?x_31)
            )
          )
          (exists (?x_8 )
            (and
              (RightOf17 ?x_8)
              (LeftOf1 ?x_8)
            )
          )
          (exists (?x_11 )
            (and
              (AboveOf14 ?x_11)
              (BelowOf9 ?x_11)
            )
          )
          (exists (?x_22 )
            (and
              (AboveOf19 ?x_22)
              (BelowOf7 ?x_22)
            )
          )
          (exists (?x_6 )
            (and
              (RightOf19 ?x_6)
              (LeftOf13 ?x_6)
            )
          )
          (exists (?x_17 )
            (and
              (RightOf6 ?x_17)
              (LeftOf1 ?x_17)
            )
          )
          (exists (?x_8 )
            (and
              (RightOf10 ?x_8)
              (LeftOf1 ?x_8)
            )
          )
          (exists (?x_14 )
            (and
              (RightOf14 ?x_14)
              (LeftOf11 ?x_14)
            )
          )
          (exists (?x_29 )
            (and
              (BelowOf5 ?x_29)
              (AboveOf18 ?x_29)
            )
          )
          (exists (?x_1 )
            (and
              (BelowOf1 ?x_1)
              (AboveOf19 ?x_1)
            )
          )
          (exists (?x_37 )
            (and
              (AboveOf6 ?x_37)
              (BelowOf2 ?x_37)
            )
          )
          (exists (?x_29 )
            (and
              (BelowOf4 ?x_29)
              (AboveOf15 ?x_29)
            )
          )
          (exists (?x_28 )
            (and
              (RightOf4 ?x_28)
              (LeftOf2 ?x_28)
            )
          )
          (exists (?x_31 )
            (and
              (RightOf13 ?x_31)
              (LeftOf2 ?x_31)
            )
          )
          (exists (?x_34 )
            (and
              (AboveOf19 ?x_34)
              (BelowOf3 ?x_34)
            )
          )
          (exists (?x_34 )
            (and
              (AboveOf19 ?x_34)
              (BelowOf4 ?x_34)
            )
          )
          (exists (?x_7 )
            (and
              (AboveOf13 ?x_7)
              (BelowOf6 ?x_7)
            )
          )
          (exists (?x_29 )
            (and
              (AboveOf13 ?x_29)
              (BelowOf8 ?x_29)
            )
          )
          (exists (?x_33 )
            (and
              (BelowOf8 ?x_33)
              (AboveOf12 ?x_33)
            )
          )
          (exists (?x_31 )
            (and
              (RightOf18 ?x_31)
              (LeftOf4 ?x_31)
            )
          )
          (exists (?x_8 )
            (and
              (LeftOf2 ?x_8)
              (RightOf3 ?x_8)
            )
          )
          (exists (?x_23 )
            (and
              (RightOf13 ?x_23)
              (LeftOf7 ?x_23)
            )
          )
          (exists (?x_6 )
            (and
              (LeftOf8 ?x_6)
              (RightOf16 ?x_6)
            )
          )
          (exists (?x_7 )
            (and
              (AboveOf9 ?x_7)
              (BelowOf4 ?x_7)
            )
          )
          (exists (?x_31 )
            (and
              (LeftOf3 ?x_31)
              (RightOf18 ?x_31)
            )
          )
          (exists (?x_20 )
            (and
              (RightOf13 ?x_20)
              (LeftOf4 ?x_20)
            )
          )
          (exists (?x_4 )
            (and
              (RightOf19 ?x_4)
              (LeftOf6 ?x_4)
            )
          )
          (exists (?x_22 )
            (and
              (BelowOf11 ?x_22)
              (AboveOf14 ?x_22)
            )
          )
          (exists (?x_37 )
            (and
              (AboveOf19 ?x_37)
              (BelowOf5 ?x_37)
            )
          )
          (exists (?x_16 )
            (and
              (LeftOf4 ?x_16)
              (RightOf19 ?x_16)
            )
          )
          (exists (?x_16 )
            (and
              (LeftOf2 ?x_16)
              (RightOf7 ?x_16)
            )
          )
          (exists (?x_6 )
            (and
              (RightOf18 ?x_6)
              (LeftOf7 ?x_6)
            )
          )
          (exists (?x_16 )
            (and
              (RightOf17 ?x_16)
              (LeftOf4 ?x_16)
            )
          )
          (exists (?x_18 )
            (and
              (RightOf17 ?x_18)
              (LeftOf1 ?x_18)
            )
          )
          (exists (?x_2 )
            (and
              (RightOf8 ?x_2)
              (LeftOf5 ?x_2)
            )
          )
          (exists (?x_20 )
            (and
              (RightOf18 ?x_20)
              (LeftOf10 ?x_20)
            )
          )
          (exists (?x_8 )
            (and
              (LeftOf1 ?x_8)
              (RightOf7 ?x_8)
            )
          )
          (exists (?x_20 )
            (and
              (RightOf17 ?x_20)
              (LeftOf3 ?x_20)
            )
          )
          (exists (?x_24 )
            (and
              (BelowOf13 ?x_24)
              (AboveOf15 ?x_24)
            )
          )
          (exists (?x_34 )
            (and
              (BelowOf6 ?x_34)
              (AboveOf16 ?x_34)
            )
          )
          (exists (?x_8 )
            (and
              (LeftOf1 ?x_8)
              (RightOf14 ?x_8)
            )
          )
          (exists (?x_24 )
            (and
              (AboveOf16 ?x_24)
              (BelowOf14 ?x_24)
            )
          )
          (exists (?x_14 )
            (and
              (LeftOf7 ?x_14)
              (RightOf17 ?x_14)
            )
          )
          (exists (?x_7 )
            (and
              (AboveOf13 ?x_7)
              (BelowOf5 ?x_7)
            )
          )
          (exists (?x_14 )
            (and
              (LeftOf9 ?x_14)
              (RightOf18 ?x_14)
            )
          )
          (exists (?x_37 )
            (and
              (AboveOf17 ?x_37)
              (BelowOf4 ?x_37)
            )
          )
          (exists (?x_5 )
            (and
              (LeftOf5 ?x_5)
              (RightOf17 ?x_5)
            )
          )
          (exists (?x_18 )
            (and
              (RightOf19 ?x_18)
              (LeftOf3 ?x_18)
            )
          )
          (exists (?x_14 )
            (and
              (LeftOf11 ?x_14)
              (RightOf15 ?x_14)
            )
          )
          (exists (?x_31 )
            (and
              (LeftOf5 ?x_31)
              (RightOf14 ?x_31)
            )
          )
          (exists (?x_20 )
            (and
              (LeftOf7 ?x_20)
              (RightOf17 ?x_20)
            )
          )
          (exists (?x_0 )
            (and
              (AboveOf6 ?x_0)
              (BelowOf2 ?x_0)
            )
          )
          (exists (?x_11 )
            (and
              (AboveOf18 ?x_11)
              (BelowOf10 ?x_11)
            )
          )
          (exists (?x_18 )
            (and
              (LeftOf12 ?x_18)
              (RightOf14 ?x_18)
            )
          )
          (exists (?x_2 )
            (and
              (LeftOf2 ?x_2)
              (RightOf12 ?x_2)
            )
          )
          (exists (?x_23 )
            (and
              (RightOf10 ?x_23)
              (LeftOf5 ?x_23)
            )
          )
          (exists (?x_13 )
            (and
              (RightOf10 ?x_13)
              (LeftOf7 ?x_13)
            )
          )
          (exists (?x_33 )
            (and
              (BelowOf4 ?x_33)
              (AboveOf13 ?x_33)
            )
          )
          (exists (?x_27 )
            (and
              (BelowOf2 ?x_27)
              (AboveOf6 ?x_27)
            )
          )
          (exists (?x_33 )
            (and
              (AboveOf18 ?x_33)
              (BelowOf8 ?x_33)
            )
          )
          (exists (?x_31 )
            (and
              (RightOf19 ?x_31)
              (LeftOf9 ?x_31)
            )
          )
          (exists (?x_34 )
            (and
              (BelowOf5 ?x_34)
              (AboveOf18 ?x_34)
            )
          )
          (exists (?x_23 )
            (and
              (RightOf13 ?x_23)
              (LeftOf2 ?x_23)
            )
          )
          (exists (?x_14 )
            (and
              (RightOf13 ?x_14)
              (LeftOf7 ?x_14)
            )
          )
          (exists (?x_9 )
            (and
              (AboveOf17 ?x_9)
              (BelowOf5 ?x_9)
            )
          )
          (exists (?x_32 )
            (and
              (BelowOf9 ?x_32)
              (AboveOf19 ?x_32)
            )
          )
          (exists (?x_25 )
            (and
              (LeftOf6 ?x_25)
              (RightOf15 ?x_25)
            )
          )
          (exists (?x_27 )
            (and
              (BelowOf2 ?x_27)
              (AboveOf7 ?x_27)
            )
          )
          (exists (?x_32 )
            (and
              (AboveOf10 ?x_32)
              (BelowOf1 ?x_32)
            )
          )
          (exists (?x_22 )
            (and
              (BelowOf11 ?x_22)
              (AboveOf13 ?x_22)
            )
          )
          (exists (?x_33 )
            (and
              (BelowOf6 ?x_33)
              (AboveOf14 ?x_33)
            )
          )
          (exists (?x_3 )
            (and
              (AboveOf19 ?x_3)
              (BelowOf13 ?x_3)
            )
          )
          (exists (?x_26 )
            (and
              (RightOf19 ?x_26)
              (LeftOf17 ?x_26)
            )
          )
          (exists (?x_4 )
            (and
              (RightOf19 ?x_4)
              (LeftOf14 ?x_4)
            )
          )
          (exists (?x_16 )
            (and
              (RightOf18 ?x_16)
              (LeftOf3 ?x_16)
            )
          )
          (exists (?x_9 )
            (and
              (BelowOf7 ?x_9)
              (AboveOf19 ?x_9)
            )
          )
          (exists (?x_19 )
            (and
              (AboveOf19 ?x_19)
              (BelowOf8 ?x_19)
            )
          )
          (exists (?x_12 )
            (and
              (LeftOf4 ?x_12)
              (RightOf14 ?x_12)
            )
          )
          (exists (?x_31 )
            (and
              (RightOf13 ?x_31)
              (LeftOf7 ?x_31)
            )
          )
          (exists (?x_22 )
            (and
              (BelowOf7 ?x_22)
              (AboveOf15 ?x_22)
            )
          )
          (exists (?x_5 )
            (and
              (RightOf15 ?x_5)
              (LeftOf5 ?x_5)
            )
          )
          (exists (?x_31 )
            (and
              (RightOf9 ?x_31)
              (LeftOf8 ?x_31)
            )
          )
          (exists (?x_9 )
            (and
              (BelowOf8 ?x_9)
              (AboveOf17 ?x_9)
            )
          )
          (exists (?x_18 )
            (and
              (LeftOf5 ?x_18)
              (RightOf19 ?x_18)
            )
          )
          (exists (?x_22 )
            (and
              (BelowOf12 ?x_22)
              (AboveOf13 ?x_22)
            )
          )
          (exists (?x_5 )
            (and
              (LeftOf12 ?x_5)
              (RightOf19 ?x_5)
            )
          )
          (exists (?x_28 )
            (and
              (RightOf17 ?x_28)
              (LeftOf2 ?x_28)
            )
          )
          (exists (?x_11 )
            (and
              (BelowOf1 ?x_11)
              (AboveOf13 ?x_11)
            )
          )
          (exists (?x_23 )
            (and
              (RightOf11 ?x_23)
              (LeftOf3 ?x_23)
            )
          )
          (exists (?x_16 )
            (and
              (RightOf16 ?x_16)
              (LeftOf3 ?x_16)
            )
          )
          (exists (?x_17 )
            (and
              (RightOf17 ?x_17)
              (LeftOf1 ?x_17)
            )
          )
          (exists (?x_31 )
            (and
              (LeftOf7 ?x_31)
              (RightOf10 ?x_31)
            )
          )
          (exists (?x_6 )
            (and
              (LeftOf2 ?x_6)
              (RightOf18 ?x_6)
            )
          )
          (exists (?x_25 )
            (and
              (LeftOf11 ?x_25)
              (RightOf18 ?x_25)
            )
          )
          (exists (?x_13 )
            (and
              (RightOf13 ?x_13)
              (LeftOf6 ?x_13)
            )
          )
          (exists (?x_25 )
            (and
              (RightOf16 ?x_25)
              (LeftOf14 ?x_25)
            )
          )
          (exists (?x_18 )
            (and
              (RightOf18 ?x_18)
              (LeftOf9 ?x_18)
            )
          )
          (exists (?x_27 )
            (and
              (AboveOf15 ?x_27)
              (BelowOf4 ?x_27)
            )
          )
          (exists (?x_1 )
            (and
              (AboveOf10 ?x_1)
              (BelowOf1 ?x_1)
            )
          )
          (exists (?x_6 )
            (and
              (RightOf17 ?x_6)
              (LeftOf7 ?x_6)
            )
          )
          (exists (?x_32 )
            (and
              (AboveOf18 ?x_32)
              (BelowOf8 ?x_32)
            )
          )
          (exists (?x_32 )
            (and
              (AboveOf19 ?x_32)
              (BelowOf6 ?x_32)
            )
          )
          (exists (?x_10 )
            (and
              (BelowOf1 ?x_10)
              (AboveOf18 ?x_10)
            )
          )
          (exists (?x_24 )
            (and
              (BelowOf9 ?x_24)
              (AboveOf15 ?x_24)
            )
          )
          (exists (?x_14 )
            (and
              (LeftOf4 ?x_14)
              (RightOf18 ?x_14)
            )
          )
          (exists (?x_26 )
            (and
              (RightOf19 ?x_26)
              (LeftOf2 ?x_26)
            )
          )
          (exists (?x_7 )
            (and
              (AboveOf10 ?x_7)
              (BelowOf2 ?x_7)
            )
          )
          (exists (?x_21 )
            (and
              (LeftOf1 ?x_21)
              (RightOf17 ?x_21)
            )
          )
          (exists (?x_21 )
            (and
              (LeftOf4 ?x_21)
              (RightOf8 ?x_21)
            )
          )
          (exists (?x_18 )
            (and
              (LeftOf10 ?x_18)
              (RightOf15 ?x_18)
            )
          )
          (exists (?x_13 )
            (and
              (LeftOf7 ?x_13)
              (RightOf19 ?x_13)
            )
          )
          (exists (?x_16 )
            (and
              (RightOf12 ?x_16)
              (LeftOf4 ?x_16)
            )
          )
          (exists (?x_16 )
            (and
              (LeftOf2 ?x_16)
              (RightOf5 ?x_16)
            )
          )
          (exists (?x_11 )
            (and
              (AboveOf19 ?x_11)
              (BelowOf10 ?x_11)
            )
          )
          (exists (?x_7 )
            (and
              (AboveOf19 ?x_7)
              (BelowOf3 ?x_7)
            )
          )
          (exists (?x_9 )
            (and
              (BelowOf7 ?x_9)
              (AboveOf18 ?x_9)
            )
          )
          (exists (?x_12 )
            (and
              (LeftOf5 ?x_12)
              (RightOf12 ?x_12)
            )
          )
          (exists (?x_27 )
            (and
              (BelowOf1 ?x_27)
              (AboveOf8 ?x_27)
            )
          )
          (exists (?x_2 )
            (and
              (LeftOf2 ?x_2)
              (RightOf11 ?x_2)
            )
          )
          (exists (?x_18 )
            (and
              (LeftOf8 ?x_18)
              (RightOf18 ?x_18)
            )
          )
          (exists (?x_10 )
            (and
              (AboveOf6 ?x_10)
              (BelowOf4 ?x_10)
            )
          )
          (exists (?x_13 )
            (and
              (LeftOf9 ?x_13)
              (RightOf17 ?x_13)
            )
          )
          (exists (?x_29 )
            (and
              (BelowOf8 ?x_29)
              (AboveOf19 ?x_29)
            )
          )
          (exists (?x_13 )
            (and
              (RightOf10 ?x_13)
              (LeftOf1 ?x_13)
            )
          )
          (exists (?x_22 )
            (and
              (BelowOf9 ?x_22)
              (AboveOf16 ?x_22)
            )
          )
          (exists (?x_14 )
            (and
              (LeftOf4 ?x_14)
              (RightOf17 ?x_14)
            )
          )
          (exists (?x_15 )
            (and
              (AboveOf15 ?x_15)
              (BelowOf1 ?x_15)
            )
          )
          (exists (?x_21 )
            (and
              (LeftOf3 ?x_21)
              (RightOf9 ?x_21)
            )
          )
          (exists (?x_22 )
            (and
              (BelowOf13 ?x_22)
              (AboveOf13 ?x_22)
            )
          )
          (exists (?x_27 )
            (and
              (AboveOf14 ?x_27)
              (BelowOf5 ?x_27)
            )
          )
          (exists (?x_34 )
            (and
              (BelowOf10 ?x_34)
              (AboveOf18 ?x_34)
            )
          )
          (exists (?x_15 )
            (and
              (BelowOf2 ?x_15)
              (AboveOf13 ?x_15)
            )
          )
          (exists (?x_13 )
            (and
              (RightOf10 ?x_13)
              (LeftOf3 ?x_13)
            )
          )
          (exists (?x_18 )
            (and
              (LeftOf4 ?x_18)
              (RightOf19 ?x_18)
            )
          )
          (exists (?x_11 )
            (and
              (BelowOf3 ?x_11)
              (AboveOf18 ?x_11)
            )
          )
          (exists (?x_24 )
            (and
              (BelowOf7 ?x_24)
              (AboveOf17 ?x_24)
            )
          )
          (exists (?x_15 )
            (and
              (BelowOf1 ?x_15)
              (AboveOf7 ?x_15)
            )
          )
          (exists (?x_5 )
            (and
              (LeftOf1 ?x_5)
              (RightOf17 ?x_5)
            )
          )
          (exists (?x_4 )
            (and
              (LeftOf17 ?x_4)
              (RightOf17 ?x_4)
            )
          )
          (exists (?x_25 )
            (and
              (RightOf19 ?x_25)
              (LeftOf9 ?x_25)
            )
          )
          (exists (?x_2 )
            (and
              (LeftOf5 ?x_2)
              (RightOf19 ?x_2)
            )
          )
          (exists (?x_27 )
            (and
              (BelowOf4 ?x_27)
              (AboveOf17 ?x_27)
            )
          )
          (exists (?x_16 )
            (and
              (LeftOf2 ?x_16)
              (RightOf19 ?x_16)
            )
          )
          (exists (?x_13 )
            (and
              (RightOf14 ?x_13)
              (LeftOf3 ?x_13)
            )
          )
          (exists (?x_25 )
            (and
              (RightOf19 ?x_25)
              (LeftOf4 ?x_25)
            )
          )
          (exists (?x_23 )
            (and
              (LeftOf5 ?x_23)
              (RightOf11 ?x_23)
            )
          )
          (exists (?x_33 )
            (and
              (AboveOf9 ?x_33)
              (BelowOf5 ?x_33)
            )
          )
          (exists (?x_3 )
            (and
              (BelowOf3 ?x_3)
              (AboveOf17 ?x_3)
            )
          )
          (exists (?x_21 )
            (and
              (RightOf18 ?x_21)
              (LeftOf3 ?x_21)
            )
          )
          (exists (?x_2 )
            (and
              (RightOf14 ?x_2)
              (LeftOf4 ?x_2)
            )
          )
          (exists (?x_0 )
            (and
              (BelowOf2 ?x_0)
              (AboveOf18 ?x_0)
            )
          )
          (exists (?x_6 )
            (and
              (RightOf19 ?x_6)
              (LeftOf4 ?x_6)
            )
          )
          (exists (?x_23 )
            (and
              (RightOf18 ?x_23)
              (LeftOf4 ?x_23)
            )
          )
          (exists (?x_31 )
            (and
              (RightOf19 ?x_31)
              (LeftOf2 ?x_31)
            )
          )
          (exists (?x_20 )
            (and
              (RightOf14 ?x_20)
              (LeftOf8 ?x_20)
            )
          )
          (exists (?x_14 )
            (and
              (LeftOf5 ?x_14)
              (RightOf15 ?x_14)
            )
          )
          (exists (?x_32 )
            (and
              (AboveOf16 ?x_32)
              (BelowOf8 ?x_32)
            )
          )
          (exists (?x_9 )
            (and
              (AboveOf19 ?x_9)
              (BelowOf17 ?x_9)
            )
          )
          (exists (?x_35 )
            (and
              (BelowOf3 ?x_35)
              (AboveOf19 ?x_35)
            )
          )
          (exists (?x_29 )
            (and
              (BelowOf5 ?x_29)
              (AboveOf12 ?x_29)
            )
          )
          (exists (?x_34 )
            (and
              (BelowOf6 ?x_34)
              (AboveOf15 ?x_34)
            )
          )
          (exists (?x_32 )
            (and
              (AboveOf15 ?x_32)
              (BelowOf10 ?x_32)
            )
          )
          (exists (?x_23 )
            (and
              (RightOf16 ?x_23)
              (LeftOf6 ?x_23)
            )
          )
          (exists (?x_28 )
            (and
              (LeftOf2 ?x_28)
              (RightOf2 ?x_28)
            )
          )
          (exists (?x_25 )
            (and
              (LeftOf3 ?x_25)
              (RightOf16 ?x_25)
            )
          )
          (exists (?x_27 )
            (and
              (AboveOf10 ?x_27)
              (BelowOf1 ?x_27)
            )
          )
          (exists (?x_12 )
            (and
              (LeftOf1 ?x_12)
              (RightOf19 ?x_12)
            )
          )
          (exists (?x_24 )
            (and
              (AboveOf16 ?x_24)
              (BelowOf10 ?x_24)
            )
          )
          (exists (?x_34 )
            (and
              (BelowOf1 ?x_34)
              (AboveOf15 ?x_34)
            )
          )
          (exists (?x_5 )
            (and
              (LeftOf11 ?x_5)
              (RightOf19 ?x_5)
            )
          )
          (exists (?x_34 )
            (and
              (BelowOf8 ?x_34)
              (AboveOf19 ?x_34)
            )
          )
          (exists (?x_20 )
            (and
              (LeftOf3 ?x_20)
              (RightOf11 ?x_20)
            )
          )
          (exists (?x_21 )
            (and
              (RightOf13 ?x_21)
              (LeftOf2 ?x_21)
            )
          )
          (exists (?x_20 )
            (and
              (LeftOf2 ?x_20)
              (RightOf15 ?x_20)
            )
          )
          (exists (?x_12 )
            (and
              (LeftOf1 ?x_12)
              (RightOf13 ?x_12)
            )
          )
          (exists (?x_7 )
            (and
              (AboveOf19 ?x_7)
              (BelowOf1 ?x_7)
            )
          )
          (exists (?x_11 )
            (and
              (BelowOf4 ?x_11)
              (AboveOf15 ?x_11)
            )
          )
          (exists (?x_27 )
            (and
              (BelowOf2 ?x_27)
              (AboveOf9 ?x_27)
            )
          )
          (exists (?x_9 )
            (and
              (AboveOf19 ?x_9)
              (BelowOf9 ?x_9)
            )
          )
          (exists (?x_14 )
            (and
              (LeftOf11 ?x_14)
              (RightOf13 ?x_14)
            )
          )
          (exists (?x_31 )
            (and
              (RightOf10 ?x_31)
              (LeftOf4 ?x_31)
            )
          )
          (exists (?x_7 )
            (and
              (BelowOf2 ?x_7)
              (AboveOf17 ?x_7)
            )
          )
          (exists (?x_6 )
            (and
              (RightOf19 ?x_6)
              (LeftOf7 ?x_6)
            )
          )
          (exists (?x_1 )
            (and
              (AboveOf1 ?x_1)
              (BelowOf1 ?x_1)
            )
          )
          (exists (?x_9 )
            (and
              (AboveOf19 ?x_9)
              (BelowOf2 ?x_9)
            )
          )
          (exists (?x_23 )
            (and
              (RightOf8 ?x_23)
              (LeftOf4 ?x_23)
            )
          )
          (exists (?x_20 )
            (and
              (LeftOf2 ?x_20)
              (RightOf19 ?x_20)
            )
          )
          (exists (?x_5 )
            (and
              (LeftOf9 ?x_5)
              (RightOf18 ?x_5)
            )
          )
          (exists (?x_29 )
            (and
              (AboveOf19 ?x_29)
              (BelowOf2 ?x_29)
            )
          )
          (exists (?x_14 )
            (and
              (RightOf13 ?x_14)
              (LeftOf2 ?x_14)
            )
          )
          (exists (?x_24 )
            (and
              (BelowOf15 ?x_24)
              (AboveOf19 ?x_24)
            )
          )
          (exists (?x_37 )
            (and
              (AboveOf12 ?x_37)
              (BelowOf1 ?x_37)
            )
          )
          (exists (?x_35 )
            (and
              (AboveOf16 ?x_35)
              (BelowOf4 ?x_35)
            )
          )
          (exists (?x_5 )
            (and
              (RightOf15 ?x_5)
              (LeftOf11 ?x_5)
            )
          )
          (exists (?x_22 )
            (and
              (AboveOf19 ?x_22)
              (BelowOf12 ?x_22)
            )
          )
          (exists (?x_7 )
            (and
              (BelowOf2 ?x_7)
              (AboveOf18 ?x_7)
            )
          )
          (exists (?x_0 )
            (and
              (AboveOf7 ?x_0)
              (BelowOf1 ?x_0)
            )
          )
          (exists (?x_34 )
            (and
              (BelowOf11 ?x_34)
              (AboveOf16 ?x_34)
            )
          )
          (exists (?x_27 )
            (and
              (BelowOf3 ?x_27)
              (AboveOf12 ?x_27)
            )
          )
          (exists (?x_35 )
            (and
              (BelowOf5 ?x_35)
              (AboveOf19 ?x_35)
            )
          )
          (exists (?x_23 )
            (and
              (LeftOf2 ?x_23)
              (RightOf11 ?x_23)
            )
          )
          (exists (?x_13 )
            (and
              (RightOf18 ?x_13)
              (LeftOf4 ?x_13)
            )
          )
          (exists (?x_22 )
            (and
              (BelowOf10 ?x_22)
              (AboveOf18 ?x_22)
            )
          )
          (exists (?x_7 )
            (and
              (AboveOf19 ?x_7)
              (BelowOf4 ?x_7)
            )
          )
          (exists (?x_29 )
            (and
              (BelowOf4 ?x_29)
              (AboveOf16 ?x_29)
            )
          )
          (exists (?x_27 )
            (and
              (AboveOf10 ?x_27)
              (BelowOf6 ?x_27)
            )
          )
          (exists (?x_11 )
            (and
              (AboveOf11 ?x_11)
              (BelowOf10 ?x_11)
            )
          )
          (exists (?x_28 )
            (and
              (RightOf17 ?x_28)
              (LeftOf1 ?x_28)
            )
          )
          (exists (?x_14 )
            (and
              (RightOf18 ?x_14)
              (LeftOf5 ?x_14)
            )
          )
          (exists (?x_11 )
            (and
              (BelowOf8 ?x_11)
              (AboveOf14 ?x_11)
            )
          )
          (exists (?x_24 )
            (and
              (BelowOf3 ?x_24)
              (AboveOf19 ?x_24)
            )
          )
          (exists (?x_33 )
            (and
              (AboveOf15 ?x_33)
              (BelowOf4 ?x_33)
            )
          )
          (exists (?x_14 )
            (and
              (RightOf19 ?x_14)
              (LeftOf6 ?x_14)
            )
          )
          (exists (?x_35 )
            (and
              (BelowOf12 ?x_35)
              (AboveOf18 ?x_35)
            )
          )
          (exists (?x_30 )
            (and
              (RightOf19 ?x_30)
              (LeftOf14 ?x_30)
            )
          )
          (exists (?x_20 )
            (and
              (RightOf16 ?x_20)
              (LeftOf8 ?x_20)
            )
          )
          (exists (?x_0 )
            (and
              (BelowOf3 ?x_0)
              (AboveOf10 ?x_0)
            )
          )
          (exists (?x_7 )
            (and
              (AboveOf14 ?x_7)
              (BelowOf6 ?x_7)
            )
          )
          (exists (?x_23 )
            (and
              (LeftOf1 ?x_23)
              (RightOf19 ?x_23)
            )
          )
          (exists (?x_19 )
            (and
              (AboveOf19 ?x_19)
              (BelowOf12 ?x_19)
            )
          )
          (exists (?x_12 )
            (and
              (LeftOf5 ?x_12)
              (RightOf17 ?x_12)
            )
          )
          (exists (?x_32 )
            (and
              (BelowOf5 ?x_32)
              (AboveOf17 ?x_32)
            )
          )
          (exists (?x_29 )
            (and
              (BelowOf5 ?x_29)
              (AboveOf15 ?x_29)
            )
          )
          (exists (?x_14 )
            (and
              (RightOf15 ?x_14)
              (LeftOf2 ?x_14)
            )
          )
          (exists (?x_25 )
            (and
              (LeftOf15 ?x_25)
              (RightOf15 ?x_25)
            )
          )
          (exists (?x_13 )
            (and
              (LeftOf4 ?x_13)
              (RightOf12 ?x_13)
            )
          )
          (exists (?x_10 )
            (and
              (AboveOf17 ?x_10)
              (BelowOf3 ?x_10)
            )
          )
          (exists (?x_2 )
            (and
              (RightOf6 ?x_2)
              (LeftOf1 ?x_2)
            )
          )
          (exists (?x_23 )
            (and
              (LeftOf4 ?x_23)
              (RightOf16 ?x_23)
            )
          )
          (exists (?x_34 )
            (and
              (BelowOf6 ?x_34)
              (AboveOf18 ?x_34)
            )
          )
          (exists (?x_11 )
            (and
              (AboveOf12 ?x_11)
              (BelowOf8 ?x_11)
            )
          )
          (exists (?x_21 )
            (and
              (LeftOf2 ?x_21)
              (RightOf5 ?x_21)
            )
          )
          (exists (?x_3 )
            (and
              (BelowOf15 ?x_3)
              (AboveOf16 ?x_3)
            )
          )
          (exists (?x_26 )
            (and
              (RightOf18 ?x_26)
              (LeftOf1 ?x_26)
            )
          )
          (exists (?x_28 )
            (and
              (LeftOf2 ?x_28)
              (RightOf10 ?x_28)
            )
          )
          (exists (?x_3 )
            (and
              (BelowOf14 ?x_3)
              (AboveOf16 ?x_3)
            )
          )
          (exists (?x_31 )
            (and
              (LeftOf6 ?x_31)
              (RightOf9 ?x_31)
            )
          )
          (exists (?x_8 )
            (and
              (RightOf18 ?x_8)
              (LeftOf3 ?x_8)
            )
          )
          (exists (?x_20 )
            (and
              (RightOf11 ?x_20)
              (LeftOf5 ?x_20)
            )
          )
          (exists (?x_24 )
            (and
              (AboveOf16 ?x_24)
              (BelowOf12 ?x_24)
            )
          )
          (exists (?x_37 )
            (and
              (BelowOf5 ?x_37)
              (AboveOf16 ?x_37)
            )
          )
          (exists (?x_25 )
            (and
              (LeftOf5 ?x_25)
              (RightOf18 ?x_25)
            )
          )
          (exists (?x_0 )
            (and
              (BelowOf3 ?x_0)
              (AboveOf16 ?x_0)
            )
          )
          (exists (?x_22 )
            (and
              (BelowOf10 ?x_22)
              (AboveOf13 ?x_22)
            )
          )
          (exists (?x_2 )
            (and
              (LeftOf6 ?x_2)
              (RightOf16 ?x_2)
            )
          )
          (exists (?x_21 )
            (and
              (LeftOf4 ?x_21)
              (RightOf7 ?x_21)
            )
          )
          (exists (?x_9 )
            (and
              (AboveOf17 ?x_9)
              (BelowOf15 ?x_9)
            )
          )
          (exists (?x_10 )
            (and
              (AboveOf5 ?x_10)
              (BelowOf2 ?x_10)
            )
          )
          (exists (?x_2 )
            (and
              (RightOf8 ?x_2)
              (LeftOf2 ?x_2)
            )
          )
          (exists (?x_16 )
            (and
              (RightOf18 ?x_16)
              (LeftOf1 ?x_16)
            )
          )
          (exists (?x_9 )
            (and
              (AboveOf19 ?x_9)
              (BelowOf10 ?x_9)
            )
          )
          (exists (?x_24 )
            (and
              (AboveOf15 ?x_24)
              (BelowOf11 ?x_24)
            )
          )
          (exists (?x_11 )
            (and
              (BelowOf3 ?x_11)
              (AboveOf14 ?x_11)
            )
          )
          (exists (?x_29 )
            (and
              (BelowOf4 ?x_29)
              (AboveOf13 ?x_29)
            )
          )
          (exists (?x_18 )
            (and
              (LeftOf12 ?x_18)
              (RightOf17 ?x_18)
            )
          )
          (exists (?x_7 )
            (and
              (BelowOf4 ?x_7)
              (AboveOf12 ?x_7)
            )
          )
          (exists (?x_33 )
            (and
              (AboveOf15 ?x_33)
              (BelowOf7 ?x_33)
            )
          )
          (exists (?x_22 )
            (and
              (BelowOf8 ?x_22)
              (AboveOf18 ?x_22)
            )
          )
          (exists (?x_6 )
            (and
              (LeftOf15 ?x_6)
              (RightOf17 ?x_6)
            )
          )
          (exists (?x_3 )
            (and
              (AboveOf18 ?x_3)
              (BelowOf10 ?x_3)
            )
          )
          (exists (?x_23 )
            (and
              (RightOf15 ?x_23)
              (LeftOf3 ?x_23)
            )
          )
          (exists (?x_18 )
            (and
              (LeftOf10 ?x_18)
              (RightOf17 ?x_18)
            )
          )
          (exists (?x_14 )
            (and
              (LeftOf10 ?x_14)
              (RightOf15 ?x_14)
            )
          )
          (exists (?x_9 )
            (and
              (BelowOf8 ?x_9)
              (AboveOf18 ?x_9)
            )
          )
          (exists (?x_11 )
            (and
              (AboveOf11 ?x_11)
              (BelowOf2 ?x_11)
            )
          )
          (exists (?x_34 )
            (and
              (BelowOf4 ?x_34)
              (AboveOf16 ?x_34)
            )
          )
          (exists (?x_20 )
            (and
              (RightOf16 ?x_20)
              (LeftOf2 ?x_20)
            )
          )
          (exists (?x_37 )
            (and
              (AboveOf19 ?x_37)
              (BelowOf1 ?x_37)
            )
          )
          (exists (?x_18 )
            (and
              (RightOf15 ?x_18)
              (LeftOf2 ?x_18)
            )
          )
          (exists (?x_2 )
            (and
              (LeftOf2 ?x_2)
              (RightOf9 ?x_2)
            )
          )
          (exists (?x_27 )
            (and
              (BelowOf2 ?x_27)
              (AboveOf19 ?x_27)
            )
          )
          (exists (?x_16 )
            (and
              (RightOf15 ?x_16)
              (LeftOf4 ?x_16)
            )
          )
          (exists (?x_35 )
            (and
              (AboveOf12 ?x_35)
              (BelowOf2 ?x_35)
            )
          )
          (exists (?x_32 )
            (and
              (BelowOf9 ?x_32)
              (AboveOf12 ?x_32)
            )
          )
          (exists (?x_29 )
            (and
              (AboveOf9 ?x_29)
              (BelowOf1 ?x_29)
            )
          )
          (exists (?x_14 )
            (and
              (LeftOf11 ?x_14)
              (RightOf19 ?x_14)
            )
          )
          (exists (?x_23 )
            (and
              (LeftOf1 ?x_23)
              (RightOf12 ?x_23)
            )
          )
          (exists (?x_4 )
            (and
              (RightOf18 ?x_4)
              (LeftOf13 ?x_4)
            )
          )
          (exists (?x_13 )
            (and
              (LeftOf1 ?x_13)
              (RightOf17 ?x_13)
            )
          )
          (exists (?x_34 )
            (and
              (BelowOf8 ?x_34)
              (AboveOf16 ?x_34)
            )
          )
          (exists (?x_2 )
            (and
              (LeftOf4 ?x_2)
              (RightOf7 ?x_2)
            )
          )
          (exists (?x_26 )
            (and
              (RightOf19 ?x_26)
              (LeftOf12 ?x_26)
            )
          )
          (exists (?x_33 )
            (and
              (BelowOf4 ?x_33)
              (AboveOf11 ?x_33)
            )
          )
          (exists (?x_32 )
            (and
              (AboveOf13 ?x_32)
              (BelowOf5 ?x_32)
            )
          )
          (exists (?x_21 )
            (and
              (RightOf13 ?x_21)
              (LeftOf5 ?x_21)
            )
          )
          (exists (?x_16 )
            (and
              (RightOf13 ?x_16)
              (LeftOf1 ?x_16)
            )
          )
          (exists (?x_10 )
            (and
              (BelowOf3 ?x_10)
              (AboveOf19 ?x_10)
            )
          )
          (exists (?x_27 )
            (and
              (AboveOf13 ?x_27)
              (BelowOf3 ?x_27)
            )
          )
          (exists (?x_4 )
            (and
              (RightOf18 ?x_4)
              (LeftOf12 ?x_4)
            )
          )
          (exists (?x_22 )
            (and
              (AboveOf14 ?x_22)
              (BelowOf8 ?x_22)
            )
          )
          (exists (?x_4 )
            (and
              (RightOf17 ?x_4)
              (LeftOf13 ?x_4)
            )
          )
          (exists (?x_32 )
            (and
              (AboveOf13 ?x_32)
              (BelowOf4 ?x_32)
            )
          )
          (exists (?x_23 )
            (and
              (RightOf15 ?x_23)
              (LeftOf5 ?x_23)
            )
          )
          (exists (?x_31 )
            (and
              (LeftOf7 ?x_31)
              (RightOf13 ?x_31)
            )
          )
          (exists (?x_18 )
            (and
              (RightOf16 ?x_18)
              (LeftOf13 ?x_18)
            )
          )
          (exists (?x_17 )
            (and
              (LeftOf1 ?x_17)
              (RightOf8 ?x_17)
            )
          )
          (exists (?x_37 )
            (and
              (AboveOf13 ?x_37)
              (BelowOf1 ?x_37)
            )
          )
          (exists (?x_28 )
            (and
              (LeftOf2 ?x_28)
              (RightOf8 ?x_28)
            )
          )
          (exists (?x_32 )
            (and
              (BelowOf1 ?x_32)
              (AboveOf14 ?x_32)
            )
          )
          (exists (?x_31 )
            (and
              (RightOf16 ?x_31)
              (LeftOf1 ?x_31)
            )
          )
          (exists (?x_18 )
            (and
              (LeftOf5 ?x_18)
              (RightOf15 ?x_18)
            )
          )
          (exists (?x_21 )
            (and
              (LeftOf3 ?x_21)
              (RightOf11 ?x_21)
            )
          )
          (exists (?x_31 )
            (and
              (RightOf17 ?x_31)
              (LeftOf6 ?x_31)
            )
          )
          (exists (?x_22 )
            (and
              (BelowOf6 ?x_22)
              (AboveOf16 ?x_22)
            )
          )
          (exists (?x_30 )
            (and
              (RightOf19 ?x_30)
              (LeftOf2 ?x_30)
            )
          )
          (exists (?x_16 )
            (and
              (LeftOf2 ?x_16)
              (RightOf12 ?x_16)
            )
          )
          (exists (?x_23 )
            (and
              (RightOf13 ?x_23)
              (LeftOf5 ?x_23)
            )
          )
          (exists (?x_14 )
            (and
              (LeftOf5 ?x_14)
              (RightOf17 ?x_14)
            )
          )
          (exists (?x_27 )
            (and
              (AboveOf18 ?x_27)
              (BelowOf5 ?x_27)
            )
          )
          (exists (?x_0 )
            (and
              (AboveOf14 ?x_0)
              (BelowOf1 ?x_0)
            )
          )
          (exists (?x_23 )
            (and
              (RightOf13 ?x_23)
              (LeftOf6 ?x_23)
            )
          )
          (exists (?x_6 )
            (and
              (LeftOf3 ?x_6)
              (RightOf17 ?x_6)
            )
          )
          (exists (?x_23 )
            (and
              (RightOf14 ?x_23)
              (LeftOf8 ?x_23)
            )
          )
          (exists (?x_14 )
            (and
              (RightOf14 ?x_14)
              (LeftOf13 ?x_14)
            )
          )
          (exists (?x_8 )
            (and
              (RightOf16 ?x_8)
              (LeftOf3 ?x_8)
            )
          )
          (exists (?x_2 )
            (and
              (RightOf15 ?x_2)
              (LeftOf5 ?x_2)
            )
          )
          (exists (?x_31 )
            (and
              (RightOf17 ?x_31)
              (LeftOf2 ?x_31)
            )
          )
          (exists (?x_37 )
            (and
              (BelowOf5 ?x_37)
              (AboveOf10 ?x_37)
            )
          )
          (exists (?x_6 )
            (and
              (RightOf17 ?x_6)
              (LeftOf13 ?x_6)
            )
          )
          (exists (?x_30 )
            (and
              (LeftOf18 ?x_30)
              (RightOf19 ?x_30)
            )
          )
          (exists (?x_5 )
            (and
              (LeftOf1 ?x_5)
              (RightOf12 ?x_5)
            )
          )
          (exists (?x_5 )
            (and
              (RightOf12 ?x_5)
              (LeftOf2 ?x_5)
            )
          )
          (exists (?x_36 )
            (and
              (AboveOf19 ?x_36)
              (BelowOf16 ?x_36)
            )
          )
          (exists (?x_17 )
            (and
              (RightOf1 ?x_17)
              (LeftOf1 ?x_17)
            )
          )
          (exists (?x_2 )
            (and
              (LeftOf4 ?x_2)
              (RightOf18 ?x_2)
            )
          )
          (exists (?x_20 )
            (and
              (LeftOf1 ?x_20)
              (RightOf14 ?x_20)
            )
          )
          (exists (?x_33 )
            (and
              (BelowOf2 ?x_33)
              (AboveOf19 ?x_33)
            )
          )
          (exists (?x_5 )
            (and
              (LeftOf4 ?x_5)
              (RightOf19 ?x_5)
            )
          )
          (exists (?x_0 )
            (and
              (BelowOf3 ?x_0)
              (AboveOf12 ?x_0)
            )
          )
          (exists (?x_12 )
            (and
              (RightOf13 ?x_12)
              (LeftOf3 ?x_12)
            )
          )
          (exists (?x_22 )
            (and
              (AboveOf19 ?x_22)
              (BelowOf13 ?x_22)
            )
          )
          (exists (?x_22 )
            (and
              (AboveOf15 ?x_22)
              (BelowOf1 ?x_22)
            )
          )
          (exists (?x_2 )
            (and
              (RightOf11 ?x_2)
              (LeftOf3 ?x_2)
            )
          )
          (exists (?x_20 )
            (and
              (LeftOf6 ?x_20)
              (RightOf17 ?x_20)
            )
          )
          (exists (?x_2 )
            (and
              (LeftOf5 ?x_2)
              (RightOf11 ?x_2)
            )
          )
          (exists (?x_31 )
            (and
              (RightOf14 ?x_31)
              (LeftOf7 ?x_31)
            )
          )
          (exists (?x_9 )
            (and
              (AboveOf17 ?x_9)
              (BelowOf1 ?x_9)
            )
          )
          (exists (?x_0 )
            (and
              (AboveOf15 ?x_0)
              (BelowOf1 ?x_0)
            )
          )
          (exists (?x_37 )
            (and
              (BelowOf3 ?x_37)
              (AboveOf18 ?x_37)
            )
          )
          (exists (?x_10 )
            (and
              (AboveOf6 ?x_10)
              (BelowOf3 ?x_10)
            )
          )
          (exists (?x_8 )
            (and
              (LeftOf3 ?x_8)
              (RightOf13 ?x_8)
            )
          )
          (exists (?x_5 )
            (and
              (RightOf18 ?x_5)
              (LeftOf8 ?x_5)
            )
          )
          (exists (?x_8 )
            (and
              (RightOf15 ?x_8)
              (LeftOf1 ?x_8)
            )
          )
          (exists (?x_34 )
            (and
              (BelowOf9 ?x_34)
              (AboveOf16 ?x_34)
            )
          )
          (exists (?x_33 )
            (and
              (AboveOf15 ?x_33)
              (BelowOf3 ?x_33)
            )
          )
          (exists (?x_25 )
            (and
              (RightOf17 ?x_25)
              (LeftOf5 ?x_25)
            )
          )
          (exists (?x_5 )
            (and
              (LeftOf5 ?x_5)
              (RightOf18 ?x_5)
            )
          )
          (exists (?x_13 )
            (and
              (LeftOf9 ?x_13)
              (RightOf16 ?x_13)
            )
          )
          (exists (?x_9 )
            (and
              (AboveOf19 ?x_9)
              (BelowOf7 ?x_9)
            )
          )
          (exists (?x_33 )
            (and
              (AboveOf13 ?x_33)
              (BelowOf5 ?x_33)
            )
          )
          (exists (?x_29 )
            (and
              (AboveOf17 ?x_29)
              (BelowOf6 ?x_29)
            )
          )
          (exists (?x_12 )
            (and
              (LeftOf4 ?x_12)
              (RightOf8 ?x_12)
            )
          )
          (exists (?x_35 )
            (and
              (BelowOf8 ?x_35)
              (AboveOf18 ?x_35)
            )
          )
          (exists (?x_23 )
            (and
              (RightOf11 ?x_23)
              (LeftOf8 ?x_23)
            )
          )
          (exists (?x_7 )
            (and
              (AboveOf17 ?x_7)
              (BelowOf5 ?x_7)
            )
          )
          (exists (?x_35 )
            (and
              (BelowOf6 ?x_35)
              (AboveOf14 ?x_35)
            )
          )
          (exists (?x_8 )
            (and
              (RightOf4 ?x_8)
              (LeftOf1 ?x_8)
            )
          )
          (exists (?x_17 )
            (and
              (LeftOf1 ?x_17)
              (RightOf19 ?x_17)
            )
          )
          (exists (?x_5 )
            (and
              (RightOf16 ?x_5)
              (LeftOf7 ?x_5)
            )
          )
          (exists (?x_26 )
            (and
              (LeftOf10 ?x_26)
              (RightOf18 ?x_26)
            )
          )
          (exists (?x_11 )
            (and
              (AboveOf13 ?x_11)
              (BelowOf7 ?x_11)
            )
          )
          (exists (?x_21 )
            (and
              (LeftOf1 ?x_21)
              (RightOf19 ?x_21)
            )
          )
          (exists (?x_11 )
            (and
              (AboveOf17 ?x_11)
              (BelowOf1 ?x_11)
            )
          )
          (exists (?x_16 )
            (and
              (LeftOf2 ?x_16)
              (RightOf14 ?x_16)
            )
          )
          (exists (?x_24 )
            (and
              (BelowOf8 ?x_24)
              (AboveOf19 ?x_24)
            )
          )
          (exists (?x_12 )
            (and
              (LeftOf1 ?x_12)
              (RightOf14 ?x_12)
            )
          )
          (exists (?x_27 )
            (and
              (BelowOf6 ?x_27)
              (AboveOf6 ?x_27)
            )
          )
          (exists (?x_21 )
            (and
              (RightOf6 ?x_21)
              (LeftOf4 ?x_21)
            )
          )
          (exists (?x_10 )
            (and
              (AboveOf5 ?x_10)
              (BelowOf4 ?x_10)
            )
          )
          (exists (?x_29 )
            (and
              (BelowOf8 ?x_29)
              (AboveOf10 ?x_29)
            )
          )
          (exists (?x_5 )
            (and
              (RightOf14 ?x_5)
              (LeftOf5 ?x_5)
            )
          )
          (exists (?x_16 )
            (and
              (RightOf17 ?x_16)
              (LeftOf3 ?x_16)
            )
          )
          (exists (?x_9 )
            (and
              (AboveOf17 ?x_9)
              (BelowOf9 ?x_9)
            )
          )
          (exists (?x_37 )
            (and
              (AboveOf14 ?x_37)
              (BelowOf3 ?x_37)
            )
          )
          (exists (?x_16 )
            (and
              (LeftOf3 ?x_16)
              (RightOf14 ?x_16)
            )
          )
          (exists (?x_11 )
            (and
              (AboveOf19 ?x_11)
              (BelowOf9 ?x_11)
            )
          )
          (exists (?x_10 )
            (and
              (BelowOf4 ?x_10)
              (AboveOf10 ?x_10)
            )
          )
          (exists (?x_32 )
            (and
              (AboveOf11 ?x_32)
              (BelowOf8 ?x_32)
            )
          )
          (exists (?x_4 )
            (and
              (RightOf17 ?x_4)
              (LeftOf14 ?x_4)
            )
          )
          (exists (?x_22 )
            (and
              (AboveOf19 ?x_22)
              (BelowOf1 ?x_22)
            )
          )
          (exists (?x_23 )
            (and
              (RightOf8 ?x_23)
              (LeftOf2 ?x_23)
            )
          )
          (exists (?x_21 )
            (and
              (LeftOf5 ?x_21)
              (RightOf9 ?x_21)
            )
          )
          (exists (?x_21 )
            (and
              (RightOf15 ?x_21)
              (LeftOf3 ?x_21)
            )
          )
          (exists (?x_29 )
            (and
              (AboveOf16 ?x_29)
              (BelowOf2 ?x_29)
            )
          )
          (exists (?x_24 )
            (and
              (AboveOf16 ?x_24)
              (BelowOf8 ?x_24)
            )
          )
          (exists (?x_23 )
            (and
              (LeftOf7 ?x_23)
              (RightOf11 ?x_23)
            )
          )
          (exists (?x_21 )
            (and
              (RightOf13 ?x_21)
              (LeftOf3 ?x_21)
            )
          )
          (exists (?x_20 )
            (and
              (LeftOf2 ?x_20)
              (RightOf13 ?x_20)
            )
          )
          (exists (?x_24 )
            (and
              (AboveOf18 ?x_24)
              (BelowOf6 ?x_24)
            )
          )
          (exists (?x_14 )
            (and
              (LeftOf6 ?x_14)
              (RightOf15 ?x_14)
            )
          )
          (exists (?x_31 )
            (and
              (LeftOf7 ?x_31)
              (RightOf15 ?x_31)
            )
          )
          (exists (?x_11 )
            (and
              (BelowOf5 ?x_11)
              (AboveOf11 ?x_11)
            )
          )
          (exists (?x_33 )
            (and
              (BelowOf2 ?x_33)
              (AboveOf12 ?x_33)
            )
          )
          (exists (?x_10 )
            (and
              (AboveOf6 ?x_10)
              (BelowOf2 ?x_10)
            )
          )
          (exists (?x_35 )
            (and
              (AboveOf12 ?x_35)
              (BelowOf10 ?x_35)
            )
          )
          (exists (?x_29 )
            (and
              (BelowOf5 ?x_29)
              (AboveOf16 ?x_29)
            )
          )
          (exists (?x_27 )
            (and
              (BelowOf2 ?x_27)
              (AboveOf15 ?x_27)
            )
          )
          (exists (?x_32 )
            (and
              (BelowOf3 ?x_32)
              (AboveOf11 ?x_32)
            )
          )
          (exists (?x_5 )
            (and
              (RightOf16 ?x_5)
              (LeftOf8 ?x_5)
            )
          )
          (exists (?x_5 )
            (and
              (LeftOf3 ?x_5)
              (RightOf17 ?x_5)
            )
          )
          (exists (?x_30 )
            (and
              (LeftOf13 ?x_30)
              (RightOf19 ?x_30)
            )
          )
          (exists (?x_34 )
            (and
              (BelowOf3 ?x_34)
              (AboveOf15 ?x_34)
            )
          )
          (exists (?x_31 )
            (and
              (RightOf12 ?x_31)
              (LeftOf5 ?x_31)
            )
          )
          (exists (?x_18 )
            (and
              (RightOf15 ?x_18)
              (LeftOf11 ?x_18)
            )
          )
          (exists (?x_6 )
            (and
              (LeftOf10 ?x_6)
              (RightOf18 ?x_6)
            )
          )
          (exists (?x_27 )
            (and
              (BelowOf6 ?x_27)
              (AboveOf19 ?x_27)
            )
          )
          (exists (?x_11 )
            (and
              (BelowOf8 ?x_11)
              (AboveOf12 ?x_11)
            )
          )
          (exists (?x_14 )
            (and
              (RightOf14 ?x_14)
              (LeftOf8 ?x_14)
            )
          )
          (exists (?x_33 )
            (and
              (BelowOf7 ?x_33)
              (AboveOf13 ?x_33)
            )
          )
          (exists (?x_5 )
            (and
              (LeftOf1 ?x_5)
              (RightOf13 ?x_5)
            )
          )
          (exists (?x_5 )
            (and
              (LeftOf6 ?x_5)
              (RightOf17 ?x_5)
            )
          )
          (exists (?x_37 )
            (and
              (AboveOf19 ?x_37)
              (BelowOf3 ?x_37)
            )
          )
          (exists (?x_20 )
            (and
              (LeftOf7 ?x_20)
              (RightOf19 ?x_20)
            )
          )
          (exists (?x_13 )
            (and
              (LeftOf4 ?x_13)
              (RightOf15 ?x_13)
            )
          )
          (exists (?x_2 )
            (and
              (LeftOf1 ?x_2)
              (RightOf17 ?x_2)
            )
          )
          (exists (?x_32 )
            (and
              (AboveOf13 ?x_32)
              (BelowOf8 ?x_32)
            )
          )
          (exists (?x_10 )
            (and
              (BelowOf4 ?x_10)
              (AboveOf15 ?x_10)
            )
          )
          (exists (?x_2 )
            (and
              (RightOf8 ?x_2)
              (LeftOf3 ?x_2)
            )
          )
          (exists (?x_23 )
            (and
              (LeftOf2 ?x_23)
              (RightOf9 ?x_23)
            )
          )
          (exists (?x_25 )
            (and
              (LeftOf15 ?x_25)
              (RightOf19 ?x_25)
            )
          )
          (exists (?x_27 )
            (and
              (AboveOf16 ?x_27)
              (BelowOf1 ?x_27)
            )
          )
          (exists (?x_13 )
            (and
              (LeftOf1 ?x_13)
              (RightOf14 ?x_13)
            )
          )
          (exists (?x_34 )
            (and
              (BelowOf13 ?x_34)
              (AboveOf18 ?x_34)
            )
          )
          (exists (?x_16 )
            (and
              (RightOf7 ?x_16)
              (LeftOf1 ?x_16)
            )
          )
          (exists (?x_10 )
            (and
              (BelowOf2 ?x_10)
              (AboveOf15 ?x_10)
            )
          )
          (exists (?x_7 )
            (and
              (AboveOf11 ?x_7)
              (BelowOf3 ?x_7)
            )
          )
          (exists (?x_0 )
            (and
              (AboveOf4 ?x_0)
              (BelowOf2 ?x_0)
            )
          )
          (exists (?x_28 )
            (and
              (LeftOf2 ?x_28)
              (RightOf9 ?x_28)
            )
          )
          (exists (?x_25 )
            (and
              (LeftOf2 ?x_25)
              (RightOf16 ?x_25)
            )
          )
          (exists (?x_21 )
            (and
              (RightOf13 ?x_21)
              (LeftOf4 ?x_21)
            )
          )
          (exists (?x_11 )
            (and
              (AboveOf14 ?x_11)
              (BelowOf1 ?x_11)
            )
          )
          (exists (?x_35 )
            (and
              (AboveOf16 ?x_35)
              (BelowOf1 ?x_35)
            )
          )
          (exists (?x_35 )
            (and
              (BelowOf8 ?x_35)
              (AboveOf15 ?x_35)
            )
          )
          (exists (?x_25 )
            (and
              (LeftOf12 ?x_25)
              (RightOf18 ?x_25)
            )
          )
          (exists (?x_3 )
            (and
              (BelowOf15 ?x_3)
              (AboveOf19 ?x_3)
            )
          )
          (exists (?x_6 )
            (and
              (RightOf19 ?x_6)
              (LeftOf14 ?x_6)
            )
          )
          (exists (?x_13 )
            (and
              (LeftOf8 ?x_13)
              (RightOf11 ?x_13)
            )
          )
          (exists (?x_0 )
            (and
              (AboveOf10 ?x_0)
              (BelowOf2 ?x_0)
            )
          )
          (exists (?x_33 )
            (and
              (BelowOf2 ?x_33)
              (AboveOf18 ?x_33)
            )
          )
          (exists (?x_8 )
            (and
              (LeftOf1 ?x_8)
              (RightOf13 ?x_8)
            )
          )
          (exists (?x_32 )
            (and
              (AboveOf14 ?x_32)
              (BelowOf6 ?x_32)
            )
          )
          (exists (?x_23 )
            (and
              (LeftOf4 ?x_23)
              (RightOf18 ?x_23)
            )
          )
          (exists (?x_8 )
            (and
              (RightOf5 ?x_8)
              (LeftOf3 ?x_8)
            )
          )
          (exists (?x_12 )
            (and
              (RightOf15 ?x_12)
              (LeftOf7 ?x_12)
            )
          )
          (exists (?x_29 )
            (and
              (BelowOf5 ?x_29)
              (AboveOf13 ?x_29)
            )
          )
          (exists (?x_29 )
            (and
              (BelowOf7 ?x_29)
              (AboveOf9 ?x_29)
            )
          )
          (exists (?x_6 )
            (and
              (LeftOf8 ?x_6)
              (RightOf18 ?x_6)
            )
          )
          (exists (?x_31 )
            (and
              (LeftOf3 ?x_31)
              (RightOf17 ?x_31)
            )
          )
          (exists (?x_23 )
            (and
              (RightOf18 ?x_23)
              (LeftOf3 ?x_23)
            )
          )
          (exists (?x_34 )
            (and
              (AboveOf15 ?x_34)
              (BelowOf2 ?x_34)
            )
          )
          (exists (?x_24 )
            (and
              (BelowOf9 ?x_24)
              (AboveOf16 ?x_24)
            )
          )
          (exists (?x_29 )
            (and
              (AboveOf15 ?x_29)
              (BelowOf8 ?x_29)
            )
          )
          (exists (?x_16 )
            (and
              (RightOf6 ?x_16)
              (LeftOf2 ?x_16)
            )
          )
          (exists (?x_27 )
            (and
              (BelowOf6 ?x_27)
              (AboveOf17 ?x_27)
            )
          )
        )
        (Error)
      )
    )
  )
)