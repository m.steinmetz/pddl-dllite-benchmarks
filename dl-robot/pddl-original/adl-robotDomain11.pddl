(define
  (domain robot)
  (:requirements :adl)
  (:predicates
    (LeftOf3 ?x )
    (LeftOf2 ?x )
    (RightOf9 ?x )
    (Rows ?x )
    (LeftOf7 ?x )
    (LeftOf6 ?x )
    (LeftOf1 ?x )
    (LeftOf4 ?x )
    (RightOf3 ?x )
    (RightOf2 ?x )
    (RightOf1 ?x )
    (RightOf0 ?x )
    (RightOf7 ?x )
    (RightOf6 ?x )
    (RightOf5 ?x )
    (RightOf4 ?x )
    (BelowOf8 ?x )
    (RightOf8 ?x )
    (BelowOf9 ?x )
    (Columns ?x )
    (LeftOf5 ?x )
    (BelowOf10 ?x )
    (BelowOf11 ?x )
    (Row10 ?x )
    (LeftOf9 ?x )
    (Row3 ?x )
    (RightOf10 ?x )
    (Row0 ?x )
    (Row1 ?x )
    (Row2 ?x )
    (LeftOf8 ?x )
    (Row4 ?x )
    (Row5 ?x )
    (Row6 ?x )
    (Row7 ?x )
    (Row8 ?x )
    (Row9 ?x )
    (AboveOf3 ?x )
    (BelowOf1 ?x )
    (AboveOf10 ?x )
    (BelowOf3 ?x )
    (Column9 ?x )
    (Column8 ?x )
    (BelowOf6 ?x )
    (BelowOf7 ?x )
    (Column5 ?x )
    (Column4 ?x )
    (Column7 ?x )
    (Column6 ?x )
    (Column1 ?x )
    (Column0 ?x )
    (Column3 ?x )
    (Column2 ?x )
    (AboveOf2 ?x )
    (Column10 ?x )
    (BelowOf2 ?x )
    (AboveOf4 ?x )
    (AboveOf5 ?x )
    (AboveOf6 ?x )
    (AboveOf7 ?x )
    (AboveOf0 ?x )
    (AboveOf1 ?x )
    (LeftOf11 ?x )
    (LeftOf10 ?x )
    (BelowOf4 ?x )
    (AboveOf8 ?x )
    (AboveOf9 ?x )
    (BelowOf5 ?x )
    (CheckConsistency)
    (Error)
  )
  (:action moveDown
    :parameters ( ?x )
    :precondition ( and
      (not (CheckConsistency))
      (not (Error))
      (or
        (BelowOf7 ?x)
        (AboveOf9 ?x)
        (BelowOf1 ?x)
        (AboveOf7 ?x)
        (AboveOf1 ?x)
        (BelowOf8 ?x)
        (BelowOf3 ?x)
        (AboveOf3 ?x)
        (BelowOf9 ?x)
        (BelowOf2 ?x)
        (BelowOf5 ?x)
        (BelowOf11 ?x)
        (BelowOf10 ?x)
        (BelowOf4 ?x)
        (AboveOf10 ?x)
        (AboveOf0 ?x)
        (AboveOf8 ?x)
        (AboveOf6 ?x)
        (AboveOf5 ?x)
        (Rows ?x)
        (AboveOf4 ?x)
        (BelowOf6 ?x)
        (AboveOf2 ?x)
      )
    )
    :effect ( and
      (CheckConsistency)
      (when
        (or
          (BelowOf1 ?x)
          (BelowOf2 ?x)
        )
        (and
          (BelowOf1 ?x)
        )
      )
      (when
        (or
          (BelowOf3 ?x)
          (BelowOf1 ?x)
          (BelowOf2 ?x)
        )
        (and
          (BelowOf2 ?x)
        )
      )
      (when
        (or
          (BelowOf4 ?x)
          (BelowOf3 ?x)
          (BelowOf1 ?x)
          (BelowOf2 ?x)
        )
        (and
          (BelowOf3 ?x)
        )
      )
      (when
        (or
          (BelowOf5 ?x)
          (BelowOf4 ?x)
          (BelowOf3 ?x)
          (BelowOf1 ?x)
          (BelowOf2 ?x)
        )
        (and
          (BelowOf4 ?x)
        )
      )
      (when
        (or
          (BelowOf3 ?x)
          (BelowOf4 ?x)
          (BelowOf1 ?x)
          (BelowOf2 ?x)
          (BelowOf5 ?x)
          (BelowOf6 ?x)
        )
        (and
          (BelowOf5 ?x)
        )
      )
      (when
        (or
          (BelowOf3 ?x)
          (BelowOf7 ?x)
          (BelowOf4 ?x)
          (BelowOf1 ?x)
          (BelowOf2 ?x)
          (BelowOf5 ?x)
          (BelowOf6 ?x)
        )
        (and
          (BelowOf6 ?x)
        )
      )
      (when
        (or
          (BelowOf8 ?x)
          (BelowOf3 ?x)
          (BelowOf7 ?x)
          (BelowOf4 ?x)
          (BelowOf1 ?x)
          (BelowOf2 ?x)
          (BelowOf5 ?x)
          (BelowOf6 ?x)
        )
        (and
          (BelowOf7 ?x)
        )
      )
      (when
        (or
          (BelowOf8 ?x)
          (BelowOf3 ?x)
          (BelowOf9 ?x)
          (BelowOf7 ?x)
          (BelowOf4 ?x)
          (BelowOf1 ?x)
          (BelowOf2 ?x)
          (BelowOf5 ?x)
          (BelowOf6 ?x)
        )
        (and
          (BelowOf8 ?x)
        )
      )
      (when
        (or
          (BelowOf8 ?x)
          (BelowOf3 ?x)
          (BelowOf9 ?x)
          (BelowOf7 ?x)
          (BelowOf4 ?x)
          (BelowOf1 ?x)
          (BelowOf2 ?x)
          (BelowOf5 ?x)
          (BelowOf6 ?x)
          (BelowOf10 ?x)
        )
        (and
          (BelowOf9 ?x)
        )
      )
      (when
        (or
          (BelowOf8 ?x)
          (BelowOf3 ?x)
          (BelowOf9 ?x)
          (BelowOf7 ?x)
          (BelowOf4 ?x)
          (BelowOf1 ?x)
          (BelowOf2 ?x)
          (BelowOf5 ?x)
          (BelowOf6 ?x)
          (BelowOf11 ?x)
          (BelowOf10 ?x)
        )
        (and
          (BelowOf10 ?x)
        )
      )
      (when
        (or
          (AboveOf5 ?x)
          (AboveOf3 ?x)
          (AboveOf4 ?x)
          (AboveOf9 ?x)
          (AboveOf8 ?x)
          (AboveOf7 ?x)
          (AboveOf2 ?x)
          (AboveOf10 ?x)
          (AboveOf1 ?x)
          (AboveOf6 ?x)
        )
        (and
          (AboveOf0 ?x)
          (not (AboveOf1 ?x))
        )
      )
      (when
        (or
          (AboveOf5 ?x)
          (AboveOf3 ?x)
          (AboveOf4 ?x)
          (AboveOf9 ?x)
          (AboveOf8 ?x)
          (AboveOf7 ?x)
          (AboveOf2 ?x)
          (AboveOf10 ?x)
          (AboveOf6 ?x)
        )
        (and
          (AboveOf1 ?x)
          (not (AboveOf2 ?x))
        )
      )
      (when
        (or
          (AboveOf5 ?x)
          (AboveOf3 ?x)
          (AboveOf4 ?x)
          (AboveOf9 ?x)
          (AboveOf7 ?x)
          (AboveOf8 ?x)
          (AboveOf10 ?x)
          (AboveOf6 ?x)
        )
        (and
          (AboveOf2 ?x)
          (not (AboveOf3 ?x))
        )
      )
      (when
        (or
          (AboveOf5 ?x)
          (AboveOf4 ?x)
          (AboveOf9 ?x)
          (AboveOf7 ?x)
          (AboveOf8 ?x)
          (AboveOf10 ?x)
          (AboveOf6 ?x)
        )
        (and
          (AboveOf3 ?x)
          (not (AboveOf4 ?x))
        )
      )
      (when
        (or
          (AboveOf5 ?x)
          (AboveOf9 ?x)
          (AboveOf10 ?x)
          (AboveOf8 ?x)
          (AboveOf7 ?x)
          (AboveOf6 ?x)
        )
        (and
          (AboveOf4 ?x)
          (not (AboveOf5 ?x))
        )
      )
      (when
        (or
          (AboveOf10 ?x)
          (AboveOf6 ?x)
          (AboveOf7 ?x)
          (AboveOf9 ?x)
          (AboveOf8 ?x)
        )
        (and
          (AboveOf5 ?x)
          (not (AboveOf6 ?x))
        )
      )
      (when
        (or
          (AboveOf10 ?x)
          (AboveOf9 ?x)
          (AboveOf7 ?x)
          (AboveOf8 ?x)
        )
        (and
          (AboveOf6 ?x)
          (not (AboveOf7 ?x))
        )
      )
      (when
        (or
          (AboveOf9 ?x)
          (AboveOf10 ?x)
          (AboveOf8 ?x)
        )
        (and
          (AboveOf7 ?x)
          (not (AboveOf8 ?x))
        )
      )
      (when
        (or
          (AboveOf9 ?x)
          (AboveOf10 ?x)
        )
        (and
          (AboveOf8 ?x)
          (not (AboveOf9 ?x))
        )
      )
      (when
        (AboveOf10 ?x)
        (and
          (AboveOf9 ?x)
          (not (AboveOf10 ?x))
        )
      )
      (when
        (Row1 ?x)
        (and
          (Row0 ?x)
          (not (Row1 ?x))
        )
      )
      (when
        (Row2 ?x)
        (and
          (Row1 ?x)
          (not (Row2 ?x))
        )
      )
      (when
        (Row3 ?x)
        (and
          (Row2 ?x)
          (not (Row3 ?x))
        )
      )
      (when
        (Row4 ?x)
        (and
          (Row3 ?x)
          (not (Row4 ?x))
        )
      )
      (when
        (Row5 ?x)
        (and
          (Row4 ?x)
          (not (Row5 ?x))
        )
      )
      (when
        (Row6 ?x)
        (and
          (Row5 ?x)
          (not (Row6 ?x))
        )
      )
      (when
        (Row7 ?x)
        (and
          (Row6 ?x)
          (not (Row7 ?x))
        )
      )
      (when
        (Row8 ?x)
        (and
          (Row7 ?x)
          (not (Row8 ?x))
        )
      )
      (when
        (Row9 ?x)
        (and
          (Row8 ?x)
          (not (Row9 ?x))
        )
      )
      (when
        (Row10 ?x)
        (and
          (Row9 ?x)
          (not (Row10 ?x))
        )
      )
      (when
        (or
          (and
            (BelowOf2 ?x)
            (AboveOf1 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf8 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf3 ?x)
          )
          (and
            (AboveOf9 ?x)
            (BelowOf1 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf7 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf3 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf8 ?x)
          )
          (and
            (AboveOf1 ?x)
            (BelowOf1 ?x)
          )
          (and
            (AboveOf5 ?x)
            (BelowOf2 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf4 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf10 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf6 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf10 ?x)
          )
          (and
            (AboveOf7 ?x)
            (BelowOf1 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf9 ?x)
          )
          (and
            (AboveOf5 ?x)
            (BelowOf1 ?x)
          )
          (and
            (AboveOf4 ?x)
            (BelowOf1 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf2 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf6 ?x)
          )
          (and
            (AboveOf2 ?x)
            (BelowOf1 ?x)
          )
        )
        (and
          (Row0 ?x)
        )
      )
      (when
        (or
          (and
            (BelowOf2 ?x)
            (AboveOf3 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf8 ?x)
          )
          (and
            (AboveOf5 ?x)
            (BelowOf2 ?x)
          )
          (and
            (AboveOf4 ?x)
            (BelowOf1 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf8 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf10 ?x)
          )
          (and
            (AboveOf9 ?x)
            (BelowOf1 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf3 ?x)
          )
          (and
            (AboveOf2 ?x)
            (BelowOf2 ?x)
          )
          (and
            (AboveOf10 ?x)
            (BelowOf3 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf10 ?x)
          )
          (and
            (AboveOf4 ?x)
            (BelowOf3 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf6 ?x)
          )
          (and
            (BelowOf3 ?x)
            (AboveOf8 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf9 ?x)
          )
          (and
            (BelowOf3 ?x)
            (AboveOf3 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf6 ?x)
          )
          (and
            (AboveOf2 ?x)
            (BelowOf3 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf4 ?x)
          )
          (and
            (AboveOf9 ?x)
            (BelowOf3 ?x)
          )
          (and
            (AboveOf5 ?x)
            (BelowOf1 ?x)
          )
          (and
            (AboveOf7 ?x)
            (BelowOf1 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf7 ?x)
          )
          (and
            (AboveOf7 ?x)
            (BelowOf3 ?x)
          )
          (and
            (AboveOf2 ?x)
            (BelowOf1 ?x)
          )
          (and
            (AboveOf6 ?x)
            (BelowOf3 ?x)
          )
          (and
            (AboveOf5 ?x)
            (BelowOf3 ?x)
          )
        )
        (and
          (Row1 ?x)
        )
      )
      (when
        (or
          (and
            (BelowOf2 ?x)
            (AboveOf3 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf8 ?x)
          )
          (and
            (AboveOf5 ?x)
            (BelowOf2 ?x)
          )
          (and
            (AboveOf4 ?x)
            (BelowOf1 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf8 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf10 ?x)
          )
          (and
            (AboveOf9 ?x)
            (BelowOf1 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf3 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf10 ?x)
          )
          (and
            (AboveOf10 ?x)
            (BelowOf3 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf10 ?x)
          )
          (and
            (AboveOf4 ?x)
            (BelowOf3 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf6 ?x)
          )
          (and
            (BelowOf3 ?x)
            (AboveOf8 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf9 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf6 ?x)
          )
          (and
            (AboveOf3 ?x)
            (BelowOf3 ?x)
          )
          (and
            (AboveOf5 ?x)
            (BelowOf4 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf4 ?x)
          )
          (and
            (AboveOf7 ?x)
            (BelowOf4 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf8 ?x)
          )
          (and
            (AboveOf4 ?x)
            (BelowOf4 ?x)
          )
          (and
            (AboveOf5 ?x)
            (BelowOf1 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf9 ?x)
          )
          (and
            (AboveOf7 ?x)
            (BelowOf1 ?x)
          )
          (and
            (AboveOf9 ?x)
            (BelowOf3 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf7 ?x)
          )
          (and
            (AboveOf7 ?x)
            (BelowOf3 ?x)
          )
          (and
            (AboveOf6 ?x)
            (BelowOf3 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf6 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf3 ?x)
          )
          (and
            (AboveOf5 ?x)
            (BelowOf3 ?x)
          )
        )
        (and
          (Row2 ?x)
        )
      )
      (when
        (or
          (and
            (BelowOf1 ?x)
            (AboveOf8 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf6 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf9 ?x)
          )
          (and
            (AboveOf4 ?x)
            (BelowOf1 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf8 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf10 ?x)
          )
          (and
            (AboveOf9 ?x)
            (BelowOf1 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf6 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf10 ?x)
          )
          (and
            (AboveOf10 ?x)
            (BelowOf3 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf10 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf7 ?x)
          )
          (and
            (AboveOf4 ?x)
            (BelowOf3 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf5 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf6 ?x)
          )
          (and
            (BelowOf3 ?x)
            (AboveOf8 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf9 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf6 ?x)
          )
          (and
            (AboveOf5 ?x)
            (BelowOf4 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf4 ?x)
          )
          (and
            (AboveOf7 ?x)
            (BelowOf4 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf10 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf8 ?x)
          )
          (and
            (AboveOf4 ?x)
            (BelowOf4 ?x)
          )
          (and
            (AboveOf5 ?x)
            (BelowOf1 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf9 ?x)
          )
          (and
            (AboveOf5 ?x)
            (BelowOf2 ?x)
          )
          (and
            (AboveOf7 ?x)
            (BelowOf1 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf8 ?x)
          )
          (and
            (AboveOf9 ?x)
            (BelowOf3 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf7 ?x)
          )
          (and
            (AboveOf7 ?x)
            (BelowOf3 ?x)
          )
          (and
            (AboveOf6 ?x)
            (BelowOf3 ?x)
          )
          (and
            (AboveOf5 ?x)
            (BelowOf3 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf4 ?x)
          )
        )
        (and
          (Row3 ?x)
        )
      )
      (when
        (or
          (and
            (BelowOf1 ?x)
            (AboveOf8 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf6 ?x)
          )
          (and
            (AboveOf10 ?x)
            (BelowOf6 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf9 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf8 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf10 ?x)
          )
          (and
            (AboveOf9 ?x)
            (BelowOf6 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf6 ?x)
          )
          (and
            (BelowOf6 ?x)
            (AboveOf8 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf10 ?x)
          )
          (and
            (AboveOf10 ?x)
            (BelowOf3 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf10 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf7 ?x)
          )
          (and
            (AboveOf6 ?x)
            (BelowOf6 ?x)
          )
          (and
            (BelowOf3 ?x)
            (AboveOf8 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf6 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf6 ?x)
          )
          (and
            (AboveOf5 ?x)
            (BelowOf4 ?x)
          )
          (and
            (AboveOf7 ?x)
            (BelowOf4 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf10 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf8 ?x)
          )
          (and
            (AboveOf9 ?x)
            (BelowOf3 ?x)
          )
          (and
            (AboveOf5 ?x)
            (BelowOf5 ?x)
          )
          (and
            (AboveOf7 ?x)
            (BelowOf6 ?x)
          )
          (and
            (AboveOf5 ?x)
            (BelowOf1 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf9 ?x)
          )
          (and
            (AboveOf5 ?x)
            (BelowOf2 ?x)
          )
          (and
            (AboveOf7 ?x)
            (BelowOf1 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf9 ?x)
          )
          (and
            (AboveOf9 ?x)
            (BelowOf1 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf8 ?x)
          )
          (and
            (AboveOf5 ?x)
            (BelowOf6 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf7 ?x)
          )
          (and
            (AboveOf7 ?x)
            (BelowOf3 ?x)
          )
          (and
            (AboveOf6 ?x)
            (BelowOf3 ?x)
          )
          (and
            (AboveOf5 ?x)
            (BelowOf3 ?x)
          )
        )
        (and
          (Row4 ?x)
        )
      )
      (when
        (or
          (and
            (BelowOf1 ?x)
            (AboveOf8 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf6 ?x)
          )
          (and
            (AboveOf10 ?x)
            (BelowOf6 ?x)
          )
          (and
            (BelowOf7 ?x)
            (AboveOf8 ?x)
          )
          (and
            (BelowOf7 ?x)
            (AboveOf7 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf9 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf8 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf10 ?x)
          )
          (and
            (AboveOf9 ?x)
            (BelowOf6 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf6 ?x)
          )
          (and
            (BelowOf6 ?x)
            (AboveOf8 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf10 ?x)
          )
          (and
            (AboveOf10 ?x)
            (BelowOf3 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf10 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf7 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf6 ?x)
          )
          (and
            (BelowOf3 ?x)
            (AboveOf8 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf9 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf6 ?x)
          )
          (and
            (BelowOf6 ?x)
            (AboveOf10 ?x)
          )
          (and
            (AboveOf7 ?x)
            (BelowOf4 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf10 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf8 ?x)
          )
          (and
            (AboveOf9 ?x)
            (BelowOf3 ?x)
          )
          (and
            (AboveOf7 ?x)
            (BelowOf6 ?x)
          )
          (and
            (BelowOf7 ?x)
            (AboveOf10 ?x)
          )
          (and
            (AboveOf7 ?x)
            (BelowOf1 ?x)
          )
          (and
            (AboveOf9 ?x)
            (BelowOf1 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf8 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf7 ?x)
          )
          (and
            (BelowOf6 ?x)
            (AboveOf6 ?x)
          )
          (and
            (BelowOf7 ?x)
            (AboveOf9 ?x)
          )
          (and
            (AboveOf7 ?x)
            (BelowOf3 ?x)
          )
          (and
            (AboveOf6 ?x)
            (BelowOf3 ?x)
          )
          (and
            (BelowOf7 ?x)
            (AboveOf6 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf9 ?x)
          )
        )
        (and
          (Row5 ?x)
        )
      )
      (when
        (or
          (and
            (BelowOf1 ?x)
            (AboveOf8 ?x)
          )
          (and
            (AboveOf7 ?x)
            (BelowOf8 ?x)
          )
          (and
            (AboveOf10 ?x)
            (BelowOf6 ?x)
          )
          (and
            (BelowOf7 ?x)
            (AboveOf8 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf9 ?x)
          )
          (and
            (AboveOf7 ?x)
            (BelowOf7 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf8 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf10 ?x)
          )
          (and
            (AboveOf9 ?x)
            (BelowOf6 ?x)
          )
          (and
            (BelowOf6 ?x)
            (AboveOf8 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf10 ?x)
          )
          (and
            (AboveOf10 ?x)
            (BelowOf3 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf10 ?x)
          )
          (and
            (AboveOf9 ?x)
            (BelowOf8 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf7 ?x)
          )
          (and
            (BelowOf3 ?x)
            (AboveOf8 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf9 ?x)
          )
          (and
            (BelowOf8 ?x)
            (AboveOf8 ?x)
          )
          (and
            (BelowOf6 ?x)
            (AboveOf10 ?x)
          )
          (and
            (AboveOf7 ?x)
            (BelowOf4 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf10 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf8 ?x)
          )
          (and
            (AboveOf9 ?x)
            (BelowOf3 ?x)
          )
          (and
            (AboveOf10 ?x)
            (BelowOf8 ?x)
          )
          (and
            (AboveOf7 ?x)
            (BelowOf6 ?x)
          )
          (and
            (BelowOf7 ?x)
            (AboveOf10 ?x)
          )
          (and
            (AboveOf7 ?x)
            (BelowOf1 ?x)
          )
          (and
            (AboveOf9 ?x)
            (BelowOf1 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf8 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf7 ?x)
          )
          (and
            (BelowOf7 ?x)
            (AboveOf9 ?x)
          )
          (and
            (AboveOf7 ?x)
            (BelowOf3 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf9 ?x)
          )
        )
        (and
          (Row6 ?x)
        )
      )
      (when
        (or
          (and
            (BelowOf1 ?x)
            (AboveOf8 ?x)
          )
          (and
            (AboveOf9 ?x)
            (BelowOf9 ?x)
          )
          (and
            (AboveOf10 ?x)
            (BelowOf6 ?x)
          )
          (and
            (BelowOf7 ?x)
            (AboveOf8 ?x)
          )
          (and
            (BelowOf9 ?x)
            (AboveOf8 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf9 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf8 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf10 ?x)
          )
          (and
            (AboveOf9 ?x)
            (BelowOf6 ?x)
          )
          (and
            (BelowOf6 ?x)
            (AboveOf8 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf10 ?x)
          )
          (and
            (AboveOf10 ?x)
            (BelowOf3 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf10 ?x)
          )
          (and
            (AboveOf9 ?x)
            (BelowOf8 ?x)
          )
          (and
            (BelowOf3 ?x)
            (AboveOf8 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf9 ?x)
          )
          (and
            (BelowOf6 ?x)
            (AboveOf10 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf10 ?x)
          )
          (and
            (AboveOf9 ?x)
            (BelowOf3 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf8 ?x)
          )
          (and
            (AboveOf10 ?x)
            (BelowOf8 ?x)
          )
          (and
            (BelowOf7 ?x)
            (AboveOf10 ?x)
          )
          (and
            (AboveOf9 ?x)
            (BelowOf1 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf8 ?x)
          )
          (and
            (AboveOf8 ?x)
            (BelowOf8 ?x)
          )
          (and
            (BelowOf7 ?x)
            (AboveOf9 ?x)
          )
          (and
            (BelowOf9 ?x)
            (AboveOf10 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf9 ?x)
          )
        )
        (and
          (Row7 ?x)
        )
      )
      (when
        (or
          (and
            (AboveOf9 ?x)
            (BelowOf10 ?x)
          )
          (and
            (AboveOf10 ?x)
            (BelowOf3 ?x)
          )
          (and
            (AboveOf9 ?x)
            (BelowOf6 ?x)
          )
          (and
            (BelowOf7 ?x)
            (AboveOf9 ?x)
          )
          (and
            (BelowOf9 ?x)
            (AboveOf9 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf10 ?x)
          )
          (and
            (BelowOf9 ?x)
            (AboveOf10 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf10 ?x)
          )
          (and
            (AboveOf9 ?x)
            (BelowOf3 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf10 ?x)
          )
          (and
            (AboveOf9 ?x)
            (BelowOf8 ?x)
          )
          (and
            (AboveOf9 ?x)
            (BelowOf1 ?x)
          )
          (and
            (AboveOf10 ?x)
            (BelowOf8 ?x)
          )
          (and
            (AboveOf10 ?x)
            (BelowOf6 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf10 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf9 ?x)
          )
          (and
            (BelowOf6 ?x)
            (AboveOf10 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf9 ?x)
          )
          (and
            (BelowOf7 ?x)
            (AboveOf10 ?x)
          )
          (and
            (AboveOf10 ?x)
            (BelowOf10 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf9 ?x)
          )
        )
        (and
          (Row8 ?x)
        )
      )
      (when
        (or
          (and
            (AboveOf10 ?x)
            (BelowOf3 ?x)
          )
          (and
            (AboveOf10 ?x)
            (BelowOf11 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf10 ?x)
          )
          (and
            (BelowOf9 ?x)
            (AboveOf10 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf10 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf10 ?x)
          )
          (and
            (AboveOf10 ?x)
            (BelowOf8 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf10 ?x)
          )
          (and
            (BelowOf7 ?x)
            (AboveOf10 ?x)
          )
          (and
            (AboveOf10 ?x)
            (BelowOf10 ?x)
          )
          (and
            (BelowOf6 ?x)
            (AboveOf10 ?x)
          )
        )
        (and
          (Row9 ?x)
        )
      )
    )
  )
  (:action moveLeft
    :parameters ( ?x )
    :precondition ( and
      (not (CheckConsistency))
      (not (Error))
      (or
        (LeftOf3 ?x)
        (RightOf7 ?x)
        (RightOf8 ?x)
        (LeftOf2 ?x)
        (LeftOf9 ?x)
        (RightOf2 ?x)
        (LeftOf1 ?x)
        (LeftOf8 ?x)
        (LeftOf5 ?x)
        (RightOf1 ?x)
        (Columns ?x)
        (RightOf6 ?x)
        (RightOf9 ?x)
        (LeftOf6 ?x)
        (RightOf0 ?x)
        (LeftOf11 ?x)
        (RightOf5 ?x)
        (LeftOf10 ?x)
        (LeftOf4 ?x)
        (RightOf4 ?x)
        (RightOf10 ?x)
        (LeftOf7 ?x)
        (RightOf3 ?x)
      )
    )
    :effect ( and
      (CheckConsistency)
      (when
        (or
          (LeftOf2 ?x)
          (LeftOf1 ?x)
        )
        (and
          (LeftOf1 ?x)
        )
      )
      (when
        (or
          (LeftOf2 ?x)
          (LeftOf3 ?x)
          (LeftOf1 ?x)
        )
        (and
          (LeftOf2 ?x)
        )
      )
      (when
        (or
          (LeftOf2 ?x)
          (LeftOf4 ?x)
          (LeftOf3 ?x)
          (LeftOf1 ?x)
        )
        (and
          (LeftOf3 ?x)
        )
      )
      (when
        (or
          (LeftOf2 ?x)
          (LeftOf4 ?x)
          (LeftOf3 ?x)
          (LeftOf1 ?x)
          (LeftOf5 ?x)
        )
        (and
          (LeftOf4 ?x)
        )
      )
      (when
        (or
          (LeftOf4 ?x)
          (LeftOf3 ?x)
          (LeftOf5 ?x)
          (LeftOf6 ?x)
          (LeftOf1 ?x)
          (LeftOf2 ?x)
        )
        (and
          (LeftOf5 ?x)
        )
      )
      (when
        (or
          (LeftOf4 ?x)
          (LeftOf3 ?x)
          (LeftOf5 ?x)
          (LeftOf6 ?x)
          (LeftOf1 ?x)
          (LeftOf7 ?x)
          (LeftOf2 ?x)
        )
        (and
          (LeftOf6 ?x)
        )
      )
      (when
        (or
          (LeftOf8 ?x)
          (LeftOf4 ?x)
          (LeftOf3 ?x)
          (LeftOf5 ?x)
          (LeftOf6 ?x)
          (LeftOf1 ?x)
          (LeftOf7 ?x)
          (LeftOf2 ?x)
        )
        (and
          (LeftOf7 ?x)
        )
      )
      (when
        (or
          (LeftOf8 ?x)
          (LeftOf4 ?x)
          (LeftOf3 ?x)
          (LeftOf5 ?x)
          (LeftOf6 ?x)
          (LeftOf1 ?x)
          (LeftOf7 ?x)
          (LeftOf2 ?x)
          (LeftOf9 ?x)
        )
        (and
          (LeftOf8 ?x)
        )
      )
      (when
        (or
          (LeftOf8 ?x)
          (LeftOf4 ?x)
          (LeftOf3 ?x)
          (LeftOf5 ?x)
          (LeftOf6 ?x)
          (LeftOf1 ?x)
          (LeftOf7 ?x)
          (LeftOf2 ?x)
          (LeftOf9 ?x)
          (LeftOf10 ?x)
        )
        (and
          (LeftOf9 ?x)
        )
      )
      (when
        (or
          (LeftOf8 ?x)
          (LeftOf4 ?x)
          (LeftOf3 ?x)
          (LeftOf5 ?x)
          (LeftOf6 ?x)
          (LeftOf1 ?x)
          (LeftOf7 ?x)
          (LeftOf2 ?x)
          (LeftOf9 ?x)
          (LeftOf11 ?x)
          (LeftOf10 ?x)
        )
        (and
          (LeftOf10 ?x)
        )
      )
      (when
        (or
          (RightOf6 ?x)
          (RightOf9 ?x)
          (RightOf7 ?x)
          (RightOf1 ?x)
          (RightOf4 ?x)
          (RightOf10 ?x)
          (RightOf2 ?x)
          (RightOf5 ?x)
          (RightOf8 ?x)
          (RightOf3 ?x)
        )
        (and
          (RightOf0 ?x)
          (not (RightOf1 ?x))
        )
      )
      (when
        (or
          (RightOf6 ?x)
          (RightOf9 ?x)
          (RightOf7 ?x)
          (RightOf8 ?x)
          (RightOf4 ?x)
          (RightOf10 ?x)
          (RightOf2 ?x)
          (RightOf5 ?x)
          (RightOf3 ?x)
        )
        (and
          (RightOf1 ?x)
          (not (RightOf2 ?x))
        )
      )
      (when
        (or
          (RightOf6 ?x)
          (RightOf9 ?x)
          (RightOf7 ?x)
          (RightOf8 ?x)
          (RightOf4 ?x)
          (RightOf10 ?x)
          (RightOf5 ?x)
          (RightOf3 ?x)
        )
        (and
          (RightOf2 ?x)
          (not (RightOf3 ?x))
        )
      )
      (when
        (or
          (RightOf6 ?x)
          (RightOf9 ?x)
          (RightOf7 ?x)
          (RightOf8 ?x)
          (RightOf4 ?x)
          (RightOf10 ?x)
          (RightOf5 ?x)
        )
        (and
          (RightOf3 ?x)
          (not (RightOf4 ?x))
        )
      )
      (when
        (or
          (RightOf6 ?x)
          (RightOf9 ?x)
          (RightOf7 ?x)
          (RightOf8 ?x)
          (RightOf10 ?x)
          (RightOf5 ?x)
        )
        (and
          (RightOf4 ?x)
          (not (RightOf5 ?x))
        )
      )
      (when
        (or
          (RightOf6 ?x)
          (RightOf8 ?x)
          (RightOf9 ?x)
          (RightOf7 ?x)
          (RightOf10 ?x)
        )
        (and
          (RightOf5 ?x)
          (not (RightOf6 ?x))
        )
      )
      (when
        (or
          (RightOf8 ?x)
          (RightOf9 ?x)
          (RightOf7 ?x)
          (RightOf10 ?x)
        )
        (and
          (RightOf6 ?x)
          (not (RightOf7 ?x))
        )
      )
      (when
        (or
          (RightOf8 ?x)
          (RightOf9 ?x)
          (RightOf10 ?x)
        )
        (and
          (RightOf7 ?x)
          (not (RightOf8 ?x))
        )
      )
      (when
        (or
          (RightOf9 ?x)
          (RightOf10 ?x)
        )
        (and
          (RightOf8 ?x)
          (not (RightOf9 ?x))
        )
      )
      (when
        (RightOf10 ?x)
        (and
          (RightOf9 ?x)
          (not (RightOf10 ?x))
        )
      )
      (when
        (Column1 ?x)
        (and
          (Column0 ?x)
          (not (Column1 ?x))
        )
      )
      (when
        (Column2 ?x)
        (and
          (Column1 ?x)
          (not (Column2 ?x))
        )
      )
      (when
        (Column3 ?x)
        (and
          (Column2 ?x)
          (not (Column3 ?x))
        )
      )
      (when
        (Column4 ?x)
        (and
          (Column3 ?x)
          (not (Column4 ?x))
        )
      )
      (when
        (Column5 ?x)
        (and
          (Column4 ?x)
          (not (Column5 ?x))
        )
      )
      (when
        (Column6 ?x)
        (and
          (Column5 ?x)
          (not (Column6 ?x))
        )
      )
      (when
        (Column7 ?x)
        (and
          (Column6 ?x)
          (not (Column7 ?x))
        )
      )
      (when
        (Column8 ?x)
        (and
          (Column7 ?x)
          (not (Column8 ?x))
        )
      )
      (when
        (Column9 ?x)
        (and
          (Column8 ?x)
          (not (Column9 ?x))
        )
      )
      (when
        (Column10 ?x)
        (and
          (Column9 ?x)
          (not (Column10 ?x))
        )
      )
      (when
        (or
          (and
            (RightOf4 ?x)
            (LeftOf2 ?x)
          )
          (and
            (RightOf6 ?x)
            (LeftOf2 ?x)
          )
          (and
            (LeftOf1 ?x)
            (RightOf10 ?x)
          )
          (and
            (RightOf6 ?x)
            (LeftOf1 ?x)
          )
          (and
            (LeftOf1 ?x)
            (RightOf8 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf3 ?x)
          )
          (and
            (RightOf2 ?x)
            (LeftOf1 ?x)
          )
          (and
            (RightOf5 ?x)
            (LeftOf1 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf2 ?x)
          )
          (and
            (LeftOf1 ?x)
            (RightOf5 ?x)
          )
          (and
            (RightOf7 ?x)
            (LeftOf1 ?x)
          )
          (and
            (LeftOf1 ?x)
            (RightOf3 ?x)
          )
          (and
            (RightOf1 ?x)
            (LeftOf1 ?x)
          )
          (and
            (RightOf9 ?x)
            (LeftOf1 ?x)
          )
          (and
            (RightOf7 ?x)
            (LeftOf2 ?x)
          )
          (and
            (RightOf9 ?x)
            (LeftOf2 ?x)
          )
          (and
            (RightOf1 ?x)
            (LeftOf2 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf10 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf8 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf5 ?x)
          )
          (and
            (RightOf4 ?x)
            (LeftOf1 ?x)
          )
        )
        (and
          (Column0 ?x)
        )
      )
      (when
        (or
          (and
            (RightOf2 ?x)
            (LeftOf3 ?x)
          )
          (and
            (RightOf4 ?x)
            (LeftOf2 ?x)
          )
          (and
            (RightOf9 ?x)
            (LeftOf3 ?x)
          )
          (and
            (RightOf6 ?x)
            (LeftOf2 ?x)
          )
          (and
            (RightOf6 ?x)
            (LeftOf1 ?x)
          )
          (and
            (RightOf2 ?x)
            (LeftOf1 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf2 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf8 ?x)
          )
          (and
            (LeftOf3 ?x)
            (RightOf3 ?x)
          )
          (and
            (LeftOf1 ?x)
            (RightOf8 ?x)
          )
          (and
            (RightOf5 ?x)
            (LeftOf3 ?x)
          )
          (and
            (RightOf7 ?x)
            (LeftOf2 ?x)
          )
          (and
            (RightOf8 ?x)
            (LeftOf3 ?x)
          )
          (and
            (RightOf10 ?x)
            (LeftOf3 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf10 ?x)
          )
          (and
            (RightOf4 ?x)
            (LeftOf1 ?x)
          )
          (and
            (RightOf5 ?x)
            (LeftOf1 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf3 ?x)
          )
          (and
            (RightOf7 ?x)
            (LeftOf3 ?x)
          )
          (and
            (LeftOf1 ?x)
            (RightOf5 ?x)
          )
          (and
            (RightOf7 ?x)
            (LeftOf1 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf5 ?x)
          )
          (and
            (LeftOf1 ?x)
            (RightOf10 ?x)
          )
          (and
            (RightOf4 ?x)
            (LeftOf3 ?x)
          )
          (and
            (LeftOf1 ?x)
            (RightOf3 ?x)
          )
          (and
            (RightOf9 ?x)
            (LeftOf1 ?x)
          )
          (and
            (RightOf6 ?x)
            (LeftOf3 ?x)
          )
          (and
            (RightOf9 ?x)
            (LeftOf2 ?x)
          )
        )
        (and
          (Column1 ?x)
        )
      )
      (when
        (or
          (and
            (RightOf4 ?x)
            (LeftOf2 ?x)
          )
          (and
            (RightOf9 ?x)
            (LeftOf3 ?x)
          )
          (and
            (RightOf6 ?x)
            (LeftOf2 ?x)
          )
          (and
            (LeftOf4 ?x)
            (RightOf3 ?x)
          )
          (and
            (RightOf6 ?x)
            (LeftOf1 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf8 ?x)
          )
          (and
            (RightOf5 ?x)
            (LeftOf4 ?x)
          )
          (and
            (RightOf6 ?x)
            (LeftOf4 ?x)
          )
          (and
            (RightOf9 ?x)
            (LeftOf4 ?x)
          )
          (and
            (LeftOf4 ?x)
            (RightOf10 ?x)
          )
          (and
            (LeftOf1 ?x)
            (RightOf8 ?x)
          )
          (and
            (RightOf5 ?x)
            (LeftOf3 ?x)
          )
          (and
            (LeftOf4 ?x)
            (RightOf8 ?x)
          )
          (and
            (RightOf7 ?x)
            (LeftOf2 ?x)
          )
          (and
            (RightOf3 ?x)
            (LeftOf3 ?x)
          )
          (and
            (RightOf10 ?x)
            (LeftOf3 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf10 ?x)
          )
          (and
            (RightOf4 ?x)
            (LeftOf1 ?x)
          )
          (and
            (RightOf5 ?x)
            (LeftOf1 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf3 ?x)
          )
          (and
            (RightOf7 ?x)
            (LeftOf3 ?x)
          )
          (and
            (LeftOf1 ?x)
            (RightOf5 ?x)
          )
          (and
            (RightOf7 ?x)
            (LeftOf1 ?x)
          )
          (and
            (RightOf8 ?x)
            (LeftOf3 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf5 ?x)
          )
          (and
            (RightOf7 ?x)
            (LeftOf4 ?x)
          )
          (and
            (RightOf4 ?x)
            (LeftOf4 ?x)
          )
          (and
            (LeftOf1 ?x)
            (RightOf10 ?x)
          )
          (and
            (RightOf4 ?x)
            (LeftOf3 ?x)
          )
          (and
            (LeftOf1 ?x)
            (RightOf3 ?x)
          )
          (and
            (RightOf9 ?x)
            (LeftOf1 ?x)
          )
          (and
            (RightOf6 ?x)
            (LeftOf3 ?x)
          )
          (and
            (RightOf9 ?x)
            (LeftOf2 ?x)
          )
        )
        (and
          (Column2 ?x)
        )
      )
      (when
        (or
          (and
            (RightOf4 ?x)
            (LeftOf2 ?x)
          )
          (and
            (RightOf9 ?x)
            (LeftOf3 ?x)
          )
          (and
            (RightOf6 ?x)
            (LeftOf2 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf5 ?x)
          )
          (and
            (RightOf6 ?x)
            (LeftOf1 ?x)
          )
          (and
            (RightOf5 ?x)
            (LeftOf3 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf8 ?x)
          )
          (and
            (RightOf5 ?x)
            (LeftOf4 ?x)
          )
          (and
            (RightOf6 ?x)
            (LeftOf4 ?x)
          )
          (and
            (RightOf9 ?x)
            (LeftOf4 ?x)
          )
          (and
            (LeftOf4 ?x)
            (RightOf10 ?x)
          )
          (and
            (LeftOf1 ?x)
            (RightOf8 ?x)
          )
          (and
            (RightOf5 ?x)
            (LeftOf1 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf8 ?x)
          )
          (and
            (LeftOf4 ?x)
            (RightOf8 ?x)
          )
          (and
            (RightOf7 ?x)
            (LeftOf2 ?x)
          )
          (and
            (RightOf8 ?x)
            (LeftOf3 ?x)
          )
          (and
            (RightOf10 ?x)
            (LeftOf3 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf10 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf10 ?x)
          )
          (and
            (RightOf4 ?x)
            (LeftOf1 ?x)
          )
          (and
            (RightOf6 ?x)
            (LeftOf5 ?x)
          )
          (and
            (RightOf8 ?x)
            (LeftOf4 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf4 ?x)
          )
          (and
            (RightOf7 ?x)
            (LeftOf3 ?x)
          )
          (and
            (LeftOf1 ?x)
            (RightOf5 ?x)
          )
          (and
            (RightOf7 ?x)
            (LeftOf1 ?x)
          )
          (and
            (RightOf10 ?x)
            (LeftOf4 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf5 ?x)
          )
          (and
            (RightOf7 ?x)
            (LeftOf4 ?x)
          )
          (and
            (RightOf4 ?x)
            (LeftOf4 ?x)
          )
          (and
            (LeftOf1 ?x)
            (RightOf10 ?x)
          )
          (and
            (RightOf4 ?x)
            (LeftOf3 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf7 ?x)
          )
          (and
            (RightOf9 ?x)
            (LeftOf1 ?x)
          )
          (and
            (RightOf6 ?x)
            (LeftOf3 ?x)
          )
          (and
            (RightOf9 ?x)
            (LeftOf2 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf9 ?x)
          )
        )
        (and
          (Column3 ?x)
        )
      )
      (when
        (or
          (and
            (LeftOf6 ?x)
            (RightOf9 ?x)
          )
          (and
            (RightOf9 ?x)
            (LeftOf3 ?x)
          )
          (and
            (RightOf6 ?x)
            (LeftOf2 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf5 ?x)
          )
          (and
            (RightOf6 ?x)
            (LeftOf1 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf8 ?x)
          )
          (and
            (RightOf5 ?x)
            (LeftOf4 ?x)
          )
          (and
            (RightOf6 ?x)
            (LeftOf4 ?x)
          )
          (and
            (RightOf9 ?x)
            (LeftOf4 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf6 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf10 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf7 ?x)
          )
          (and
            (LeftOf4 ?x)
            (RightOf10 ?x)
          )
          (and
            (LeftOf1 ?x)
            (RightOf8 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf5 ?x)
          )
          (and
            (RightOf5 ?x)
            (LeftOf3 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf8 ?x)
          )
          (and
            (LeftOf4 ?x)
            (RightOf8 ?x)
          )
          (and
            (RightOf7 ?x)
            (LeftOf2 ?x)
          )
          (and
            (RightOf8 ?x)
            (LeftOf3 ?x)
          )
          (and
            (RightOf10 ?x)
            (LeftOf3 ?x)
          )
          (and
            (RightOf6 ?x)
            (LeftOf3 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf10 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf10 ?x)
          )
          (and
            (RightOf6 ?x)
            (LeftOf5 ?x)
          )
          (and
            (RightOf8 ?x)
            (LeftOf4 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf8 ?x)
          )
          (and
            (RightOf7 ?x)
            (LeftOf3 ?x)
          )
          (and
            (LeftOf1 ?x)
            (RightOf5 ?x)
          )
          (and
            (RightOf7 ?x)
            (LeftOf1 ?x)
          )
          (and
            (RightOf10 ?x)
            (LeftOf4 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf5 ?x)
          )
          (and
            (RightOf7 ?x)
            (LeftOf4 ?x)
          )
          (and
            (LeftOf1 ?x)
            (RightOf10 ?x)
          )
          (and
            (RightOf9 ?x)
            (LeftOf1 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf7 ?x)
          )
          (and
            (RightOf9 ?x)
            (LeftOf2 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf9 ?x)
          )
        )
        (and
          (Column4 ?x)
        )
      )
      (when
        (or
          (and
            (LeftOf6 ?x)
            (RightOf9 ?x)
          )
          (and
            (RightOf9 ?x)
            (LeftOf3 ?x)
          )
          (and
            (RightOf6 ?x)
            (LeftOf2 ?x)
          )
          (and
            (RightOf6 ?x)
            (LeftOf1 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf8 ?x)
          )
          (and
            (LeftOf7 ?x)
            (RightOf10 ?x)
          )
          (and
            (RightOf9 ?x)
            (LeftOf4 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf10 ?x)
          )
          (and
            (RightOf6 ?x)
            (LeftOf7 ?x)
          )
          (and
            (RightOf9 ?x)
            (LeftOf7 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf7 ?x)
          )
          (and
            (LeftOf4 ?x)
            (RightOf10 ?x)
          )
          (and
            (LeftOf1 ?x)
            (RightOf8 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf8 ?x)
          )
          (and
            (LeftOf4 ?x)
            (RightOf8 ?x)
          )
          (and
            (RightOf7 ?x)
            (LeftOf2 ?x)
          )
          (and
            (RightOf8 ?x)
            (LeftOf3 ?x)
          )
          (and
            (RightOf10 ?x)
            (LeftOf3 ?x)
          )
          (and
            (RightOf6 ?x)
            (LeftOf6 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf10 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf10 ?x)
          )
          (and
            (RightOf6 ?x)
            (LeftOf5 ?x)
          )
          (and
            (RightOf8 ?x)
            (LeftOf4 ?x)
          )
          (and
            (RightOf7 ?x)
            (LeftOf3 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf8 ?x)
          )
          (and
            (RightOf7 ?x)
            (LeftOf1 ?x)
          )
          (and
            (RightOf10 ?x)
            (LeftOf4 ?x)
          )
          (and
            (RightOf7 ?x)
            (LeftOf4 ?x)
          )
          (and
            (RightOf6 ?x)
            (LeftOf3 ?x)
          )
          (and
            (LeftOf1 ?x)
            (RightOf10 ?x)
          )
          (and
            (LeftOf7 ?x)
            (RightOf8 ?x)
          )
          (and
            (RightOf7 ?x)
            (LeftOf7 ?x)
          )
          (and
            (RightOf6 ?x)
            (LeftOf4 ?x)
          )
          (and
            (RightOf9 ?x)
            (LeftOf1 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf7 ?x)
          )
          (and
            (RightOf9 ?x)
            (LeftOf2 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf9 ?x)
          )
        )
        (and
          (Column5 ?x)
        )
      )
      (when
        (or
          (and
            (LeftOf6 ?x)
            (RightOf9 ?x)
          )
          (and
            (RightOf9 ?x)
            (LeftOf3 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf8 ?x)
          )
          (and
            (LeftOf7 ?x)
            (RightOf10 ?x)
          )
          (and
            (RightOf9 ?x)
            (LeftOf4 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf10 ?x)
          )
          (and
            (RightOf9 ?x)
            (LeftOf7 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf7 ?x)
          )
          (and
            (LeftOf4 ?x)
            (RightOf10 ?x)
          )
          (and
            (RightOf9 ?x)
            (LeftOf2 ?x)
          )
          (and
            (LeftOf1 ?x)
            (RightOf8 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf8 ?x)
          )
          (and
            (LeftOf4 ?x)
            (RightOf8 ?x)
          )
          (and
            (RightOf7 ?x)
            (LeftOf2 ?x)
          )
          (and
            (RightOf8 ?x)
            (LeftOf3 ?x)
          )
          (and
            (RightOf10 ?x)
            (LeftOf3 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf10 ?x)
          )
          (and
            (LeftOf7 ?x)
            (RightOf7 ?x)
          )
          (and
            (LeftOf8 ?x)
            (RightOf8 ?x)
          )
          (and
            (LeftOf8 ?x)
            (RightOf10 ?x)
          )
          (and
            (RightOf8 ?x)
            (LeftOf4 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf8 ?x)
          )
          (and
            (RightOf7 ?x)
            (LeftOf3 ?x)
          )
          (and
            (RightOf7 ?x)
            (LeftOf1 ?x)
          )
          (and
            (RightOf10 ?x)
            (LeftOf4 ?x)
          )
          (and
            (RightOf7 ?x)
            (LeftOf4 ?x)
          )
          (and
            (LeftOf1 ?x)
            (RightOf10 ?x)
          )
          (and
            (RightOf9 ?x)
            (LeftOf8 ?x)
          )
          (and
            (LeftOf7 ?x)
            (RightOf8 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf10 ?x)
          )
          (and
            (RightOf9 ?x)
            (LeftOf1 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf7 ?x)
          )
          (and
            (LeftOf8 ?x)
            (RightOf7 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf9 ?x)
          )
        )
        (and
          (Column6 ?x)
        )
      )
      (when
        (or
          (and
            (LeftOf6 ?x)
            (RightOf9 ?x)
          )
          (and
            (RightOf9 ?x)
            (LeftOf3 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf8 ?x)
          )
          (and
            (LeftOf7 ?x)
            (RightOf10 ?x)
          )
          (and
            (RightOf9 ?x)
            (LeftOf4 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf10 ?x)
          )
          (and
            (RightOf9 ?x)
            (LeftOf7 ?x)
          )
          (and
            (LeftOf4 ?x)
            (RightOf10 ?x)
          )
          (and
            (LeftOf1 ?x)
            (RightOf8 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf8 ?x)
          )
          (and
            (RightOf10 ?x)
            (LeftOf4 ?x)
          )
          (and
            (RightOf10 ?x)
            (LeftOf3 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf10 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf10 ?x)
          )
          (and
            (LeftOf8 ?x)
            (RightOf10 ?x)
          )
          (and
            (LeftOf9 ?x)
            (RightOf10 ?x)
          )
          (and
            (RightOf8 ?x)
            (LeftOf4 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf8 ?x)
          )
          (and
            (RightOf9 ?x)
            (LeftOf9 ?x)
          )
          (and
            (RightOf8 ?x)
            (LeftOf3 ?x)
          )
          (and
            (LeftOf9 ?x)
            (RightOf8 ?x)
          )
          (and
            (LeftOf1 ?x)
            (RightOf10 ?x)
          )
          (and
            (RightOf9 ?x)
            (LeftOf8 ?x)
          )
          (and
            (LeftOf7 ?x)
            (RightOf8 ?x)
          )
          (and
            (LeftOf8 ?x)
            (RightOf8 ?x)
          )
          (and
            (RightOf9 ?x)
            (LeftOf1 ?x)
          )
          (and
            (RightOf9 ?x)
            (LeftOf2 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf9 ?x)
          )
        )
        (and
          (Column7 ?x)
        )
      )
      (when
        (or
          (and
            (RightOf9 ?x)
            (LeftOf7 ?x)
          )
          (and
            (LeftOf9 ?x)
            (RightOf10 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf9 ?x)
          )
          (and
            (RightOf9 ?x)
            (LeftOf3 ?x)
          )
          (and
            (RightOf9 ?x)
            (LeftOf4 ?x)
          )
          (and
            (LeftOf4 ?x)
            (RightOf10 ?x)
          )
          (and
            (LeftOf1 ?x)
            (RightOf10 ?x)
          )
          (and
            (RightOf9 ?x)
            (LeftOf2 ?x)
          )
          (and
            (RightOf9 ?x)
            (LeftOf8 ?x)
          )
          (and
            (LeftOf10 ?x)
            (RightOf10 ?x)
          )
          (and
            (LeftOf7 ?x)
            (RightOf10 ?x)
          )
          (and
            (RightOf9 ?x)
            (LeftOf10 ?x)
          )
          (and
            (RightOf9 ?x)
            (LeftOf9 ?x)
          )
          (and
            (RightOf9 ?x)
            (LeftOf1 ?x)
          )
          (and
            (RightOf10 ?x)
            (LeftOf4 ?x)
          )
          (and
            (RightOf10 ?x)
            (LeftOf3 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf9 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf10 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf10 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf10 ?x)
          )
          (and
            (LeftOf8 ?x)
            (RightOf10 ?x)
          )
        )
        (and
          (Column8 ?x)
        )
      )
      (when
        (or
          (and
            (LeftOf9 ?x)
            (RightOf10 ?x)
          )
          (and
            (LeftOf1 ?x)
            (RightOf10 ?x)
          )
          (and
            (LeftOf11 ?x)
            (RightOf10 ?x)
          )
          (and
            (LeftOf10 ?x)
            (RightOf10 ?x)
          )
          (and
            (LeftOf7 ?x)
            (RightOf10 ?x)
          )
          (and
            (RightOf10 ?x)
            (LeftOf4 ?x)
          )
          (and
            (RightOf10 ?x)
            (LeftOf3 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf10 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf10 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf10 ?x)
          )
          (and
            (LeftOf8 ?x)
            (RightOf10 ?x)
          )
        )
        (and
          (Column9 ?x)
        )
      )
    )
  )
  (:action moveUp
    :parameters ( ?x )
    :precondition ( and
      (not (CheckConsistency))
      (not (Error))
      (or
        (BelowOf7 ?x)
        (AboveOf9 ?x)
        (BelowOf1 ?x)
        (AboveOf7 ?x)
        (AboveOf1 ?x)
        (BelowOf8 ?x)
        (BelowOf3 ?x)
        (AboveOf3 ?x)
        (BelowOf9 ?x)
        (BelowOf2 ?x)
        (BelowOf5 ?x)
        (BelowOf11 ?x)
        (BelowOf10 ?x)
        (BelowOf4 ?x)
        (AboveOf10 ?x)
        (AboveOf0 ?x)
        (AboveOf8 ?x)
        (AboveOf6 ?x)
        (AboveOf5 ?x)
        (Rows ?x)
        (AboveOf4 ?x)
        (BelowOf6 ?x)
        (AboveOf2 ?x)
      )
    )
    :effect ( and
      (CheckConsistency)
      (when
        (or
          (AboveOf5 ?x)
          (AboveOf3 ?x)
          (AboveOf4 ?x)
          (AboveOf9 ?x)
          (AboveOf8 ?x)
          (AboveOf7 ?x)
          (AboveOf0 ?x)
          (AboveOf2 ?x)
          (AboveOf10 ?x)
          (AboveOf1 ?x)
          (AboveOf6 ?x)
        )
        (and
          (AboveOf1 ?x)
        )
      )
      (when
        (or
          (AboveOf5 ?x)
          (AboveOf3 ?x)
          (AboveOf4 ?x)
          (AboveOf9 ?x)
          (AboveOf8 ?x)
          (AboveOf7 ?x)
          (AboveOf2 ?x)
          (AboveOf10 ?x)
          (AboveOf1 ?x)
          (AboveOf6 ?x)
        )
        (and
          (AboveOf2 ?x)
        )
      )
      (when
        (or
          (AboveOf5 ?x)
          (AboveOf3 ?x)
          (AboveOf4 ?x)
          (AboveOf9 ?x)
          (AboveOf8 ?x)
          (AboveOf7 ?x)
          (AboveOf2 ?x)
          (AboveOf10 ?x)
          (AboveOf6 ?x)
        )
        (and
          (AboveOf3 ?x)
        )
      )
      (when
        (or
          (AboveOf5 ?x)
          (AboveOf3 ?x)
          (AboveOf4 ?x)
          (AboveOf9 ?x)
          (AboveOf7 ?x)
          (AboveOf8 ?x)
          (AboveOf10 ?x)
          (AboveOf6 ?x)
        )
        (and
          (AboveOf4 ?x)
        )
      )
      (when
        (or
          (AboveOf5 ?x)
          (AboveOf4 ?x)
          (AboveOf9 ?x)
          (AboveOf7 ?x)
          (AboveOf8 ?x)
          (AboveOf10 ?x)
          (AboveOf6 ?x)
        )
        (and
          (AboveOf5 ?x)
        )
      )
      (when
        (or
          (AboveOf5 ?x)
          (AboveOf9 ?x)
          (AboveOf10 ?x)
          (AboveOf8 ?x)
          (AboveOf7 ?x)
          (AboveOf6 ?x)
        )
        (and
          (AboveOf6 ?x)
        )
      )
      (when
        (or
          (AboveOf10 ?x)
          (AboveOf6 ?x)
          (AboveOf7 ?x)
          (AboveOf9 ?x)
          (AboveOf8 ?x)
        )
        (and
          (AboveOf7 ?x)
        )
      )
      (when
        (or
          (AboveOf10 ?x)
          (AboveOf9 ?x)
          (AboveOf7 ?x)
          (AboveOf8 ?x)
        )
        (and
          (AboveOf8 ?x)
        )
      )
      (when
        (or
          (AboveOf9 ?x)
          (AboveOf10 ?x)
          (AboveOf8 ?x)
        )
        (and
          (AboveOf9 ?x)
        )
      )
      (when
        (or
          (AboveOf9 ?x)
          (AboveOf10 ?x)
        )
        (and
          (AboveOf10 ?x)
        )
      )
      (when
        (BelowOf1 ?x)
        (and
          (BelowOf2 ?x)
          (not (BelowOf1 ?x))
        )
      )
      (when
        (or
          (BelowOf1 ?x)
          (BelowOf2 ?x)
        )
        (and
          (BelowOf3 ?x)
          (not (BelowOf2 ?x))
        )
      )
      (when
        (or
          (BelowOf3 ?x)
          (BelowOf1 ?x)
          (BelowOf2 ?x)
        )
        (and
          (BelowOf4 ?x)
          (not (BelowOf3 ?x))
        )
      )
      (when
        (or
          (BelowOf4 ?x)
          (BelowOf3 ?x)
          (BelowOf1 ?x)
          (BelowOf2 ?x)
        )
        (and
          (BelowOf5 ?x)
          (not (BelowOf4 ?x))
        )
      )
      (when
        (or
          (BelowOf5 ?x)
          (BelowOf4 ?x)
          (BelowOf3 ?x)
          (BelowOf1 ?x)
          (BelowOf2 ?x)
        )
        (and
          (BelowOf6 ?x)
          (not (BelowOf5 ?x))
        )
      )
      (when
        (or
          (BelowOf3 ?x)
          (BelowOf4 ?x)
          (BelowOf1 ?x)
          (BelowOf2 ?x)
          (BelowOf5 ?x)
          (BelowOf6 ?x)
        )
        (and
          (BelowOf7 ?x)
          (not (BelowOf6 ?x))
        )
      )
      (when
        (or
          (BelowOf3 ?x)
          (BelowOf7 ?x)
          (BelowOf4 ?x)
          (BelowOf1 ?x)
          (BelowOf2 ?x)
          (BelowOf5 ?x)
          (BelowOf6 ?x)
        )
        (and
          (BelowOf8 ?x)
          (not (BelowOf7 ?x))
        )
      )
      (when
        (or
          (BelowOf8 ?x)
          (BelowOf3 ?x)
          (BelowOf7 ?x)
          (BelowOf4 ?x)
          (BelowOf1 ?x)
          (BelowOf2 ?x)
          (BelowOf5 ?x)
          (BelowOf6 ?x)
        )
        (and
          (BelowOf9 ?x)
          (not (BelowOf8 ?x))
        )
      )
      (when
        (or
          (BelowOf8 ?x)
          (BelowOf3 ?x)
          (BelowOf9 ?x)
          (BelowOf7 ?x)
          (BelowOf4 ?x)
          (BelowOf1 ?x)
          (BelowOf2 ?x)
          (BelowOf5 ?x)
          (BelowOf6 ?x)
        )
        (and
          (BelowOf10 ?x)
          (not (BelowOf9 ?x))
        )
      )
      (when
        (or
          (BelowOf8 ?x)
          (BelowOf3 ?x)
          (BelowOf9 ?x)
          (BelowOf7 ?x)
          (BelowOf4 ?x)
          (BelowOf1 ?x)
          (BelowOf2 ?x)
          (BelowOf5 ?x)
          (BelowOf6 ?x)
          (BelowOf10 ?x)
        )
        (and
          (BelowOf11 ?x)
          (not (BelowOf10 ?x))
        )
      )
      (when
        (Row0 ?x)
        (and
          (Row1 ?x)
          (not (Row0 ?x))
        )
      )
      (when
        (Row1 ?x)
        (and
          (Row2 ?x)
          (not (Row1 ?x))
        )
      )
      (when
        (Row2 ?x)
        (and
          (Row3 ?x)
          (not (Row2 ?x))
        )
      )
      (when
        (Row3 ?x)
        (and
          (Row4 ?x)
          (not (Row3 ?x))
        )
      )
      (when
        (Row4 ?x)
        (and
          (Row5 ?x)
          (not (Row4 ?x))
        )
      )
      (when
        (Row5 ?x)
        (and
          (Row6 ?x)
          (not (Row5 ?x))
        )
      )
      (when
        (Row6 ?x)
        (and
          (Row7 ?x)
          (not (Row6 ?x))
        )
      )
      (when
        (Row7 ?x)
        (and
          (Row8 ?x)
          (not (Row7 ?x))
        )
      )
      (when
        (Row8 ?x)
        (and
          (Row9 ?x)
          (not (Row8 ?x))
        )
      )
      (when
        (Row9 ?x)
        (and
          (Row10 ?x)
          (not (Row9 ?x))
        )
      )
      (when
        (or
          (and
            (AboveOf9 ?x)
            (BelowOf1 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf3 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf8 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf0 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf10 ?x)
          )
          (and
            (AboveOf5 ?x)
            (BelowOf1 ?x)
          )
          (and
            (AboveOf1 ?x)
            (BelowOf1 ?x)
          )
          (and
            (AboveOf7 ?x)
            (BelowOf1 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf6 ?x)
          )
          (and
            (AboveOf2 ?x)
            (BelowOf1 ?x)
          )
          (and
            (AboveOf4 ?x)
            (BelowOf1 ?x)
          )
        )
        (and
          (Row1 ?x)
        )
      )
      (when
        (or
          (and
            (BelowOf2 ?x)
            (AboveOf1 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf8 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf3 ?x)
          )
          (and
            (AboveOf9 ?x)
            (BelowOf1 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf7 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf3 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf8 ?x)
          )
          (and
            (AboveOf1 ?x)
            (BelowOf1 ?x)
          )
          (and
            (AboveOf5 ?x)
            (BelowOf2 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf4 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf10 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf6 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf10 ?x)
          )
          (and
            (AboveOf7 ?x)
            (BelowOf1 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf9 ?x)
          )
          (and
            (AboveOf5 ?x)
            (BelowOf1 ?x)
          )
          (and
            (AboveOf4 ?x)
            (BelowOf1 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf2 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf6 ?x)
          )
          (and
            (AboveOf2 ?x)
            (BelowOf1 ?x)
          )
        )
        (and
          (Row2 ?x)
        )
      )
      (when
        (or
          (and
            (BelowOf2 ?x)
            (AboveOf3 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf8 ?x)
          )
          (and
            (AboveOf5 ?x)
            (BelowOf2 ?x)
          )
          (and
            (AboveOf4 ?x)
            (BelowOf1 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf8 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf10 ?x)
          )
          (and
            (AboveOf9 ?x)
            (BelowOf1 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf3 ?x)
          )
          (and
            (AboveOf2 ?x)
            (BelowOf2 ?x)
          )
          (and
            (AboveOf10 ?x)
            (BelowOf3 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf10 ?x)
          )
          (and
            (AboveOf4 ?x)
            (BelowOf3 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf6 ?x)
          )
          (and
            (BelowOf3 ?x)
            (AboveOf8 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf9 ?x)
          )
          (and
            (BelowOf3 ?x)
            (AboveOf3 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf6 ?x)
          )
          (and
            (AboveOf2 ?x)
            (BelowOf3 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf4 ?x)
          )
          (and
            (AboveOf9 ?x)
            (BelowOf3 ?x)
          )
          (and
            (AboveOf5 ?x)
            (BelowOf1 ?x)
          )
          (and
            (AboveOf7 ?x)
            (BelowOf1 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf7 ?x)
          )
          (and
            (AboveOf7 ?x)
            (BelowOf3 ?x)
          )
          (and
            (AboveOf2 ?x)
            (BelowOf1 ?x)
          )
          (and
            (AboveOf6 ?x)
            (BelowOf3 ?x)
          )
          (and
            (AboveOf5 ?x)
            (BelowOf3 ?x)
          )
        )
        (and
          (Row3 ?x)
        )
      )
      (when
        (or
          (and
            (BelowOf2 ?x)
            (AboveOf3 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf8 ?x)
          )
          (and
            (AboveOf5 ?x)
            (BelowOf2 ?x)
          )
          (and
            (AboveOf4 ?x)
            (BelowOf1 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf8 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf10 ?x)
          )
          (and
            (AboveOf9 ?x)
            (BelowOf1 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf3 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf10 ?x)
          )
          (and
            (AboveOf10 ?x)
            (BelowOf3 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf10 ?x)
          )
          (and
            (AboveOf4 ?x)
            (BelowOf3 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf6 ?x)
          )
          (and
            (BelowOf3 ?x)
            (AboveOf8 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf9 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf6 ?x)
          )
          (and
            (AboveOf3 ?x)
            (BelowOf3 ?x)
          )
          (and
            (AboveOf5 ?x)
            (BelowOf4 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf4 ?x)
          )
          (and
            (AboveOf7 ?x)
            (BelowOf4 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf8 ?x)
          )
          (and
            (AboveOf4 ?x)
            (BelowOf4 ?x)
          )
          (and
            (AboveOf5 ?x)
            (BelowOf1 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf9 ?x)
          )
          (and
            (AboveOf7 ?x)
            (BelowOf1 ?x)
          )
          (and
            (AboveOf9 ?x)
            (BelowOf3 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf7 ?x)
          )
          (and
            (AboveOf7 ?x)
            (BelowOf3 ?x)
          )
          (and
            (AboveOf6 ?x)
            (BelowOf3 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf6 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf3 ?x)
          )
          (and
            (AboveOf5 ?x)
            (BelowOf3 ?x)
          )
        )
        (and
          (Row4 ?x)
        )
      )
      (when
        (or
          (and
            (BelowOf1 ?x)
            (AboveOf8 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf6 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf9 ?x)
          )
          (and
            (AboveOf4 ?x)
            (BelowOf1 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf8 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf10 ?x)
          )
          (and
            (AboveOf9 ?x)
            (BelowOf1 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf6 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf10 ?x)
          )
          (and
            (AboveOf10 ?x)
            (BelowOf3 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf10 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf7 ?x)
          )
          (and
            (AboveOf4 ?x)
            (BelowOf3 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf5 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf6 ?x)
          )
          (and
            (BelowOf3 ?x)
            (AboveOf8 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf9 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf6 ?x)
          )
          (and
            (AboveOf5 ?x)
            (BelowOf4 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf4 ?x)
          )
          (and
            (AboveOf7 ?x)
            (BelowOf4 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf10 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf8 ?x)
          )
          (and
            (AboveOf4 ?x)
            (BelowOf4 ?x)
          )
          (and
            (AboveOf5 ?x)
            (BelowOf1 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf9 ?x)
          )
          (and
            (AboveOf5 ?x)
            (BelowOf2 ?x)
          )
          (and
            (AboveOf7 ?x)
            (BelowOf1 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf8 ?x)
          )
          (and
            (AboveOf9 ?x)
            (BelowOf3 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf7 ?x)
          )
          (and
            (AboveOf7 ?x)
            (BelowOf3 ?x)
          )
          (and
            (AboveOf6 ?x)
            (BelowOf3 ?x)
          )
          (and
            (AboveOf5 ?x)
            (BelowOf3 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf4 ?x)
          )
        )
        (and
          (Row5 ?x)
        )
      )
      (when
        (or
          (and
            (BelowOf1 ?x)
            (AboveOf8 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf6 ?x)
          )
          (and
            (AboveOf10 ?x)
            (BelowOf6 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf9 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf8 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf10 ?x)
          )
          (and
            (AboveOf9 ?x)
            (BelowOf6 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf6 ?x)
          )
          (and
            (BelowOf6 ?x)
            (AboveOf8 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf10 ?x)
          )
          (and
            (AboveOf10 ?x)
            (BelowOf3 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf10 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf7 ?x)
          )
          (and
            (AboveOf6 ?x)
            (BelowOf6 ?x)
          )
          (and
            (BelowOf3 ?x)
            (AboveOf8 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf6 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf6 ?x)
          )
          (and
            (AboveOf5 ?x)
            (BelowOf4 ?x)
          )
          (and
            (AboveOf7 ?x)
            (BelowOf4 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf10 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf8 ?x)
          )
          (and
            (AboveOf9 ?x)
            (BelowOf3 ?x)
          )
          (and
            (AboveOf5 ?x)
            (BelowOf5 ?x)
          )
          (and
            (AboveOf7 ?x)
            (BelowOf6 ?x)
          )
          (and
            (AboveOf5 ?x)
            (BelowOf1 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf9 ?x)
          )
          (and
            (AboveOf5 ?x)
            (BelowOf2 ?x)
          )
          (and
            (AboveOf7 ?x)
            (BelowOf1 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf9 ?x)
          )
          (and
            (AboveOf9 ?x)
            (BelowOf1 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf8 ?x)
          )
          (and
            (AboveOf5 ?x)
            (BelowOf6 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf7 ?x)
          )
          (and
            (AboveOf7 ?x)
            (BelowOf3 ?x)
          )
          (and
            (AboveOf6 ?x)
            (BelowOf3 ?x)
          )
          (and
            (AboveOf5 ?x)
            (BelowOf3 ?x)
          )
        )
        (and
          (Row6 ?x)
        )
      )
      (when
        (or
          (and
            (BelowOf1 ?x)
            (AboveOf8 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf6 ?x)
          )
          (and
            (AboveOf10 ?x)
            (BelowOf6 ?x)
          )
          (and
            (BelowOf7 ?x)
            (AboveOf8 ?x)
          )
          (and
            (BelowOf7 ?x)
            (AboveOf7 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf9 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf8 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf10 ?x)
          )
          (and
            (AboveOf9 ?x)
            (BelowOf6 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf6 ?x)
          )
          (and
            (BelowOf6 ?x)
            (AboveOf8 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf10 ?x)
          )
          (and
            (AboveOf10 ?x)
            (BelowOf3 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf10 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf7 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf6 ?x)
          )
          (and
            (BelowOf3 ?x)
            (AboveOf8 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf9 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf6 ?x)
          )
          (and
            (BelowOf6 ?x)
            (AboveOf10 ?x)
          )
          (and
            (AboveOf7 ?x)
            (BelowOf4 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf10 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf8 ?x)
          )
          (and
            (AboveOf9 ?x)
            (BelowOf3 ?x)
          )
          (and
            (AboveOf7 ?x)
            (BelowOf6 ?x)
          )
          (and
            (BelowOf7 ?x)
            (AboveOf10 ?x)
          )
          (and
            (AboveOf7 ?x)
            (BelowOf1 ?x)
          )
          (and
            (AboveOf9 ?x)
            (BelowOf1 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf8 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf7 ?x)
          )
          (and
            (BelowOf6 ?x)
            (AboveOf6 ?x)
          )
          (and
            (BelowOf7 ?x)
            (AboveOf9 ?x)
          )
          (and
            (AboveOf7 ?x)
            (BelowOf3 ?x)
          )
          (and
            (AboveOf6 ?x)
            (BelowOf3 ?x)
          )
          (and
            (BelowOf7 ?x)
            (AboveOf6 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf9 ?x)
          )
        )
        (and
          (Row7 ?x)
        )
      )
      (when
        (or
          (and
            (BelowOf1 ?x)
            (AboveOf8 ?x)
          )
          (and
            (AboveOf7 ?x)
            (BelowOf8 ?x)
          )
          (and
            (AboveOf10 ?x)
            (BelowOf6 ?x)
          )
          (and
            (BelowOf7 ?x)
            (AboveOf8 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf9 ?x)
          )
          (and
            (AboveOf7 ?x)
            (BelowOf7 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf8 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf10 ?x)
          )
          (and
            (AboveOf9 ?x)
            (BelowOf6 ?x)
          )
          (and
            (BelowOf6 ?x)
            (AboveOf8 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf10 ?x)
          )
          (and
            (AboveOf10 ?x)
            (BelowOf3 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf10 ?x)
          )
          (and
            (AboveOf9 ?x)
            (BelowOf8 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf7 ?x)
          )
          (and
            (BelowOf3 ?x)
            (AboveOf8 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf9 ?x)
          )
          (and
            (BelowOf8 ?x)
            (AboveOf8 ?x)
          )
          (and
            (BelowOf6 ?x)
            (AboveOf10 ?x)
          )
          (and
            (AboveOf7 ?x)
            (BelowOf4 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf10 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf8 ?x)
          )
          (and
            (AboveOf9 ?x)
            (BelowOf3 ?x)
          )
          (and
            (AboveOf10 ?x)
            (BelowOf8 ?x)
          )
          (and
            (AboveOf7 ?x)
            (BelowOf6 ?x)
          )
          (and
            (BelowOf7 ?x)
            (AboveOf10 ?x)
          )
          (and
            (AboveOf7 ?x)
            (BelowOf1 ?x)
          )
          (and
            (AboveOf9 ?x)
            (BelowOf1 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf8 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf7 ?x)
          )
          (and
            (BelowOf7 ?x)
            (AboveOf9 ?x)
          )
          (and
            (AboveOf7 ?x)
            (BelowOf3 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf9 ?x)
          )
        )
        (and
          (Row8 ?x)
        )
      )
      (when
        (or
          (and
            (BelowOf1 ?x)
            (AboveOf8 ?x)
          )
          (and
            (AboveOf9 ?x)
            (BelowOf9 ?x)
          )
          (and
            (AboveOf10 ?x)
            (BelowOf6 ?x)
          )
          (and
            (BelowOf7 ?x)
            (AboveOf8 ?x)
          )
          (and
            (BelowOf9 ?x)
            (AboveOf8 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf9 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf8 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf10 ?x)
          )
          (and
            (AboveOf9 ?x)
            (BelowOf6 ?x)
          )
          (and
            (BelowOf6 ?x)
            (AboveOf8 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf10 ?x)
          )
          (and
            (AboveOf10 ?x)
            (BelowOf3 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf10 ?x)
          )
          (and
            (AboveOf9 ?x)
            (BelowOf8 ?x)
          )
          (and
            (BelowOf3 ?x)
            (AboveOf8 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf9 ?x)
          )
          (and
            (BelowOf6 ?x)
            (AboveOf10 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf10 ?x)
          )
          (and
            (AboveOf9 ?x)
            (BelowOf3 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf8 ?x)
          )
          (and
            (AboveOf10 ?x)
            (BelowOf8 ?x)
          )
          (and
            (BelowOf7 ?x)
            (AboveOf10 ?x)
          )
          (and
            (AboveOf9 ?x)
            (BelowOf1 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf8 ?x)
          )
          (and
            (AboveOf8 ?x)
            (BelowOf8 ?x)
          )
          (and
            (BelowOf7 ?x)
            (AboveOf9 ?x)
          )
          (and
            (BelowOf9 ?x)
            (AboveOf10 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf9 ?x)
          )
        )
        (and
          (Row9 ?x)
        )
      )
      (when
        (or
          (and
            (AboveOf9 ?x)
            (BelowOf10 ?x)
          )
          (and
            (AboveOf10 ?x)
            (BelowOf3 ?x)
          )
          (and
            (AboveOf9 ?x)
            (BelowOf6 ?x)
          )
          (and
            (BelowOf7 ?x)
            (AboveOf9 ?x)
          )
          (and
            (BelowOf9 ?x)
            (AboveOf9 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf10 ?x)
          )
          (and
            (BelowOf9 ?x)
            (AboveOf10 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf10 ?x)
          )
          (and
            (AboveOf9 ?x)
            (BelowOf3 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf10 ?x)
          )
          (and
            (AboveOf9 ?x)
            (BelowOf8 ?x)
          )
          (and
            (AboveOf9 ?x)
            (BelowOf1 ?x)
          )
          (and
            (AboveOf10 ?x)
            (BelowOf8 ?x)
          )
          (and
            (AboveOf10 ?x)
            (BelowOf6 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf10 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf9 ?x)
          )
          (and
            (BelowOf6 ?x)
            (AboveOf10 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf9 ?x)
          )
          (and
            (BelowOf7 ?x)
            (AboveOf10 ?x)
          )
          (and
            (AboveOf10 ?x)
            (BelowOf10 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf9 ?x)
          )
        )
        (and
          (Row10 ?x)
        )
      )
    )
  )
  (:action moveRight
    :parameters ( ?x )
    :precondition ( and
      (not (CheckConsistency))
      (not (Error))
      (or
        (LeftOf3 ?x)
        (RightOf7 ?x)
        (RightOf8 ?x)
        (LeftOf2 ?x)
        (LeftOf9 ?x)
        (RightOf2 ?x)
        (LeftOf1 ?x)
        (LeftOf8 ?x)
        (LeftOf5 ?x)
        (RightOf1 ?x)
        (Columns ?x)
        (RightOf6 ?x)
        (RightOf9 ?x)
        (LeftOf6 ?x)
        (RightOf0 ?x)
        (LeftOf11 ?x)
        (RightOf5 ?x)
        (LeftOf10 ?x)
        (LeftOf4 ?x)
        (RightOf4 ?x)
        (RightOf10 ?x)
        (LeftOf7 ?x)
        (RightOf3 ?x)
      )
    )
    :effect ( and
      (CheckConsistency)
      (when
        (or
          (RightOf6 ?x)
          (RightOf9 ?x)
          (RightOf7 ?x)
          (RightOf1 ?x)
          (RightOf4 ?x)
          (RightOf0 ?x)
          (RightOf2 ?x)
          (RightOf5 ?x)
          (RightOf10 ?x)
          (RightOf8 ?x)
          (RightOf3 ?x)
        )
        (and
          (RightOf1 ?x)
        )
      )
      (when
        (or
          (RightOf6 ?x)
          (RightOf9 ?x)
          (RightOf7 ?x)
          (RightOf1 ?x)
          (RightOf4 ?x)
          (RightOf10 ?x)
          (RightOf2 ?x)
          (RightOf5 ?x)
          (RightOf8 ?x)
          (RightOf3 ?x)
        )
        (and
          (RightOf2 ?x)
        )
      )
      (when
        (or
          (RightOf6 ?x)
          (RightOf9 ?x)
          (RightOf7 ?x)
          (RightOf8 ?x)
          (RightOf4 ?x)
          (RightOf10 ?x)
          (RightOf2 ?x)
          (RightOf5 ?x)
          (RightOf3 ?x)
        )
        (and
          (RightOf3 ?x)
        )
      )
      (when
        (or
          (RightOf6 ?x)
          (RightOf9 ?x)
          (RightOf7 ?x)
          (RightOf8 ?x)
          (RightOf4 ?x)
          (RightOf10 ?x)
          (RightOf5 ?x)
          (RightOf3 ?x)
        )
        (and
          (RightOf4 ?x)
        )
      )
      (when
        (or
          (RightOf6 ?x)
          (RightOf9 ?x)
          (RightOf7 ?x)
          (RightOf8 ?x)
          (RightOf4 ?x)
          (RightOf10 ?x)
          (RightOf5 ?x)
        )
        (and
          (RightOf5 ?x)
        )
      )
      (when
        (or
          (RightOf6 ?x)
          (RightOf9 ?x)
          (RightOf7 ?x)
          (RightOf8 ?x)
          (RightOf10 ?x)
          (RightOf5 ?x)
        )
        (and
          (RightOf6 ?x)
        )
      )
      (when
        (or
          (RightOf6 ?x)
          (RightOf8 ?x)
          (RightOf9 ?x)
          (RightOf7 ?x)
          (RightOf10 ?x)
        )
        (and
          (RightOf7 ?x)
        )
      )
      (when
        (or
          (RightOf8 ?x)
          (RightOf9 ?x)
          (RightOf7 ?x)
          (RightOf10 ?x)
        )
        (and
          (RightOf8 ?x)
        )
      )
      (when
        (or
          (RightOf8 ?x)
          (RightOf9 ?x)
          (RightOf10 ?x)
        )
        (and
          (RightOf9 ?x)
        )
      )
      (when
        (or
          (RightOf9 ?x)
          (RightOf10 ?x)
        )
        (and
          (RightOf10 ?x)
        )
      )
      (when
        (LeftOf1 ?x)
        (and
          (LeftOf2 ?x)
          (not (LeftOf1 ?x))
        )
      )
      (when
        (or
          (LeftOf2 ?x)
          (LeftOf1 ?x)
        )
        (and
          (LeftOf3 ?x)
          (not (LeftOf2 ?x))
        )
      )
      (when
        (or
          (LeftOf2 ?x)
          (LeftOf3 ?x)
          (LeftOf1 ?x)
        )
        (and
          (LeftOf4 ?x)
          (not (LeftOf3 ?x))
        )
      )
      (when
        (or
          (LeftOf2 ?x)
          (LeftOf4 ?x)
          (LeftOf3 ?x)
          (LeftOf1 ?x)
        )
        (and
          (LeftOf5 ?x)
          (not (LeftOf4 ?x))
        )
      )
      (when
        (or
          (LeftOf2 ?x)
          (LeftOf4 ?x)
          (LeftOf3 ?x)
          (LeftOf1 ?x)
          (LeftOf5 ?x)
        )
        (and
          (LeftOf6 ?x)
          (not (LeftOf5 ?x))
        )
      )
      (when
        (or
          (LeftOf4 ?x)
          (LeftOf3 ?x)
          (LeftOf5 ?x)
          (LeftOf6 ?x)
          (LeftOf1 ?x)
          (LeftOf2 ?x)
        )
        (and
          (LeftOf7 ?x)
          (not (LeftOf6 ?x))
        )
      )
      (when
        (or
          (LeftOf4 ?x)
          (LeftOf3 ?x)
          (LeftOf5 ?x)
          (LeftOf6 ?x)
          (LeftOf1 ?x)
          (LeftOf7 ?x)
          (LeftOf2 ?x)
        )
        (and
          (LeftOf8 ?x)
          (not (LeftOf7 ?x))
        )
      )
      (when
        (or
          (LeftOf8 ?x)
          (LeftOf4 ?x)
          (LeftOf3 ?x)
          (LeftOf5 ?x)
          (LeftOf6 ?x)
          (LeftOf1 ?x)
          (LeftOf7 ?x)
          (LeftOf2 ?x)
        )
        (and
          (LeftOf9 ?x)
          (not (LeftOf8 ?x))
        )
      )
      (when
        (or
          (LeftOf8 ?x)
          (LeftOf4 ?x)
          (LeftOf3 ?x)
          (LeftOf5 ?x)
          (LeftOf6 ?x)
          (LeftOf1 ?x)
          (LeftOf7 ?x)
          (LeftOf2 ?x)
          (LeftOf9 ?x)
        )
        (and
          (LeftOf10 ?x)
          (not (LeftOf9 ?x))
        )
      )
      (when
        (or
          (LeftOf8 ?x)
          (LeftOf4 ?x)
          (LeftOf3 ?x)
          (LeftOf5 ?x)
          (LeftOf6 ?x)
          (LeftOf1 ?x)
          (LeftOf7 ?x)
          (LeftOf2 ?x)
          (LeftOf9 ?x)
          (LeftOf10 ?x)
        )
        (and
          (LeftOf11 ?x)
          (not (LeftOf10 ?x))
        )
      )
      (when
        (Column0 ?x)
        (and
          (Column1 ?x)
          (not (Column0 ?x))
        )
      )
      (when
        (Column1 ?x)
        (and
          (Column2 ?x)
          (not (Column1 ?x))
        )
      )
      (when
        (Column2 ?x)
        (and
          (Column3 ?x)
          (not (Column2 ?x))
        )
      )
      (when
        (Column3 ?x)
        (and
          (Column4 ?x)
          (not (Column3 ?x))
        )
      )
      (when
        (Column4 ?x)
        (and
          (Column5 ?x)
          (not (Column4 ?x))
        )
      )
      (when
        (Column5 ?x)
        (and
          (Column6 ?x)
          (not (Column5 ?x))
        )
      )
      (when
        (Column6 ?x)
        (and
          (Column7 ?x)
          (not (Column6 ?x))
        )
      )
      (when
        (Column7 ?x)
        (and
          (Column8 ?x)
          (not (Column7 ?x))
        )
      )
      (when
        (Column8 ?x)
        (and
          (Column9 ?x)
          (not (Column8 ?x))
        )
      )
      (when
        (Column9 ?x)
        (and
          (Column10 ?x)
          (not (Column9 ?x))
        )
      )
      (when
        (or
          (and
            (LeftOf1 ?x)
            (RightOf10 ?x)
          )
          (and
            (RightOf6 ?x)
            (LeftOf1 ?x)
          )
          (and
            (LeftOf1 ?x)
            (RightOf8 ?x)
          )
          (and
            (RightOf2 ?x)
            (LeftOf1 ?x)
          )
          (and
            (RightOf5 ?x)
            (LeftOf1 ?x)
          )
          (and
            (LeftOf1 ?x)
            (RightOf0 ?x)
          )
          (and
            (LeftOf1 ?x)
            (RightOf3 ?x)
          )
          (and
            (RightOf1 ?x)
            (LeftOf1 ?x)
          )
          (and
            (RightOf9 ?x)
            (LeftOf1 ?x)
          )
          (and
            (RightOf7 ?x)
            (LeftOf1 ?x)
          )
          (and
            (RightOf4 ?x)
            (LeftOf1 ?x)
          )
        )
        (and
          (Column1 ?x)
        )
      )
      (when
        (or
          (and
            (RightOf4 ?x)
            (LeftOf2 ?x)
          )
          (and
            (RightOf6 ?x)
            (LeftOf2 ?x)
          )
          (and
            (LeftOf1 ?x)
            (RightOf10 ?x)
          )
          (and
            (RightOf6 ?x)
            (LeftOf1 ?x)
          )
          (and
            (LeftOf1 ?x)
            (RightOf8 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf3 ?x)
          )
          (and
            (RightOf2 ?x)
            (LeftOf1 ?x)
          )
          (and
            (RightOf5 ?x)
            (LeftOf1 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf2 ?x)
          )
          (and
            (LeftOf1 ?x)
            (RightOf5 ?x)
          )
          (and
            (RightOf7 ?x)
            (LeftOf1 ?x)
          )
          (and
            (LeftOf1 ?x)
            (RightOf3 ?x)
          )
          (and
            (RightOf1 ?x)
            (LeftOf1 ?x)
          )
          (and
            (RightOf9 ?x)
            (LeftOf1 ?x)
          )
          (and
            (RightOf7 ?x)
            (LeftOf2 ?x)
          )
          (and
            (RightOf9 ?x)
            (LeftOf2 ?x)
          )
          (and
            (RightOf1 ?x)
            (LeftOf2 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf10 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf8 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf5 ?x)
          )
          (and
            (RightOf4 ?x)
            (LeftOf1 ?x)
          )
        )
        (and
          (Column2 ?x)
        )
      )
      (when
        (or
          (and
            (RightOf2 ?x)
            (LeftOf3 ?x)
          )
          (and
            (RightOf4 ?x)
            (LeftOf2 ?x)
          )
          (and
            (RightOf9 ?x)
            (LeftOf3 ?x)
          )
          (and
            (RightOf6 ?x)
            (LeftOf2 ?x)
          )
          (and
            (RightOf6 ?x)
            (LeftOf1 ?x)
          )
          (and
            (RightOf2 ?x)
            (LeftOf1 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf2 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf8 ?x)
          )
          (and
            (LeftOf3 ?x)
            (RightOf3 ?x)
          )
          (and
            (LeftOf1 ?x)
            (RightOf8 ?x)
          )
          (and
            (RightOf5 ?x)
            (LeftOf3 ?x)
          )
          (and
            (RightOf7 ?x)
            (LeftOf2 ?x)
          )
          (and
            (RightOf8 ?x)
            (LeftOf3 ?x)
          )
          (and
            (RightOf10 ?x)
            (LeftOf3 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf10 ?x)
          )
          (and
            (RightOf4 ?x)
            (LeftOf1 ?x)
          )
          (and
            (RightOf5 ?x)
            (LeftOf1 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf3 ?x)
          )
          (and
            (RightOf7 ?x)
            (LeftOf3 ?x)
          )
          (and
            (LeftOf1 ?x)
            (RightOf5 ?x)
          )
          (and
            (RightOf7 ?x)
            (LeftOf1 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf5 ?x)
          )
          (and
            (LeftOf1 ?x)
            (RightOf10 ?x)
          )
          (and
            (RightOf4 ?x)
            (LeftOf3 ?x)
          )
          (and
            (LeftOf1 ?x)
            (RightOf3 ?x)
          )
          (and
            (RightOf9 ?x)
            (LeftOf1 ?x)
          )
          (and
            (RightOf6 ?x)
            (LeftOf3 ?x)
          )
          (and
            (RightOf9 ?x)
            (LeftOf2 ?x)
          )
        )
        (and
          (Column3 ?x)
        )
      )
      (when
        (or
          (and
            (RightOf4 ?x)
            (LeftOf2 ?x)
          )
          (and
            (RightOf9 ?x)
            (LeftOf3 ?x)
          )
          (and
            (RightOf6 ?x)
            (LeftOf2 ?x)
          )
          (and
            (LeftOf4 ?x)
            (RightOf3 ?x)
          )
          (and
            (RightOf6 ?x)
            (LeftOf1 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf8 ?x)
          )
          (and
            (RightOf5 ?x)
            (LeftOf4 ?x)
          )
          (and
            (RightOf6 ?x)
            (LeftOf4 ?x)
          )
          (and
            (RightOf9 ?x)
            (LeftOf4 ?x)
          )
          (and
            (LeftOf4 ?x)
            (RightOf10 ?x)
          )
          (and
            (LeftOf1 ?x)
            (RightOf8 ?x)
          )
          (and
            (RightOf5 ?x)
            (LeftOf3 ?x)
          )
          (and
            (LeftOf4 ?x)
            (RightOf8 ?x)
          )
          (and
            (RightOf7 ?x)
            (LeftOf2 ?x)
          )
          (and
            (RightOf3 ?x)
            (LeftOf3 ?x)
          )
          (and
            (RightOf10 ?x)
            (LeftOf3 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf10 ?x)
          )
          (and
            (RightOf4 ?x)
            (LeftOf1 ?x)
          )
          (and
            (RightOf5 ?x)
            (LeftOf1 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf3 ?x)
          )
          (and
            (RightOf7 ?x)
            (LeftOf3 ?x)
          )
          (and
            (LeftOf1 ?x)
            (RightOf5 ?x)
          )
          (and
            (RightOf7 ?x)
            (LeftOf1 ?x)
          )
          (and
            (RightOf8 ?x)
            (LeftOf3 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf5 ?x)
          )
          (and
            (RightOf7 ?x)
            (LeftOf4 ?x)
          )
          (and
            (RightOf4 ?x)
            (LeftOf4 ?x)
          )
          (and
            (LeftOf1 ?x)
            (RightOf10 ?x)
          )
          (and
            (RightOf4 ?x)
            (LeftOf3 ?x)
          )
          (and
            (LeftOf1 ?x)
            (RightOf3 ?x)
          )
          (and
            (RightOf9 ?x)
            (LeftOf1 ?x)
          )
          (and
            (RightOf6 ?x)
            (LeftOf3 ?x)
          )
          (and
            (RightOf9 ?x)
            (LeftOf2 ?x)
          )
        )
        (and
          (Column4 ?x)
        )
      )
      (when
        (or
          (and
            (RightOf4 ?x)
            (LeftOf2 ?x)
          )
          (and
            (RightOf9 ?x)
            (LeftOf3 ?x)
          )
          (and
            (RightOf6 ?x)
            (LeftOf2 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf5 ?x)
          )
          (and
            (RightOf6 ?x)
            (LeftOf1 ?x)
          )
          (and
            (RightOf5 ?x)
            (LeftOf3 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf8 ?x)
          )
          (and
            (RightOf5 ?x)
            (LeftOf4 ?x)
          )
          (and
            (RightOf6 ?x)
            (LeftOf4 ?x)
          )
          (and
            (RightOf9 ?x)
            (LeftOf4 ?x)
          )
          (and
            (LeftOf4 ?x)
            (RightOf10 ?x)
          )
          (and
            (LeftOf1 ?x)
            (RightOf8 ?x)
          )
          (and
            (RightOf5 ?x)
            (LeftOf1 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf8 ?x)
          )
          (and
            (LeftOf4 ?x)
            (RightOf8 ?x)
          )
          (and
            (RightOf7 ?x)
            (LeftOf2 ?x)
          )
          (and
            (RightOf8 ?x)
            (LeftOf3 ?x)
          )
          (and
            (RightOf10 ?x)
            (LeftOf3 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf10 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf10 ?x)
          )
          (and
            (RightOf4 ?x)
            (LeftOf1 ?x)
          )
          (and
            (RightOf6 ?x)
            (LeftOf5 ?x)
          )
          (and
            (RightOf8 ?x)
            (LeftOf4 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf4 ?x)
          )
          (and
            (RightOf7 ?x)
            (LeftOf3 ?x)
          )
          (and
            (LeftOf1 ?x)
            (RightOf5 ?x)
          )
          (and
            (RightOf7 ?x)
            (LeftOf1 ?x)
          )
          (and
            (RightOf10 ?x)
            (LeftOf4 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf5 ?x)
          )
          (and
            (RightOf7 ?x)
            (LeftOf4 ?x)
          )
          (and
            (RightOf4 ?x)
            (LeftOf4 ?x)
          )
          (and
            (LeftOf1 ?x)
            (RightOf10 ?x)
          )
          (and
            (RightOf4 ?x)
            (LeftOf3 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf7 ?x)
          )
          (and
            (RightOf9 ?x)
            (LeftOf1 ?x)
          )
          (and
            (RightOf6 ?x)
            (LeftOf3 ?x)
          )
          (and
            (RightOf9 ?x)
            (LeftOf2 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf9 ?x)
          )
        )
        (and
          (Column5 ?x)
        )
      )
      (when
        (or
          (and
            (LeftOf6 ?x)
            (RightOf9 ?x)
          )
          (and
            (RightOf9 ?x)
            (LeftOf3 ?x)
          )
          (and
            (RightOf6 ?x)
            (LeftOf2 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf5 ?x)
          )
          (and
            (RightOf6 ?x)
            (LeftOf1 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf8 ?x)
          )
          (and
            (RightOf5 ?x)
            (LeftOf4 ?x)
          )
          (and
            (RightOf6 ?x)
            (LeftOf4 ?x)
          )
          (and
            (RightOf9 ?x)
            (LeftOf4 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf6 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf10 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf7 ?x)
          )
          (and
            (LeftOf4 ?x)
            (RightOf10 ?x)
          )
          (and
            (LeftOf1 ?x)
            (RightOf8 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf5 ?x)
          )
          (and
            (RightOf5 ?x)
            (LeftOf3 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf8 ?x)
          )
          (and
            (LeftOf4 ?x)
            (RightOf8 ?x)
          )
          (and
            (RightOf7 ?x)
            (LeftOf2 ?x)
          )
          (and
            (RightOf8 ?x)
            (LeftOf3 ?x)
          )
          (and
            (RightOf10 ?x)
            (LeftOf3 ?x)
          )
          (and
            (RightOf6 ?x)
            (LeftOf3 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf10 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf10 ?x)
          )
          (and
            (RightOf6 ?x)
            (LeftOf5 ?x)
          )
          (and
            (RightOf8 ?x)
            (LeftOf4 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf8 ?x)
          )
          (and
            (RightOf7 ?x)
            (LeftOf3 ?x)
          )
          (and
            (LeftOf1 ?x)
            (RightOf5 ?x)
          )
          (and
            (RightOf7 ?x)
            (LeftOf1 ?x)
          )
          (and
            (RightOf10 ?x)
            (LeftOf4 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf5 ?x)
          )
          (and
            (RightOf7 ?x)
            (LeftOf4 ?x)
          )
          (and
            (LeftOf1 ?x)
            (RightOf10 ?x)
          )
          (and
            (RightOf9 ?x)
            (LeftOf1 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf7 ?x)
          )
          (and
            (RightOf9 ?x)
            (LeftOf2 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf9 ?x)
          )
        )
        (and
          (Column6 ?x)
        )
      )
      (when
        (or
          (and
            (LeftOf6 ?x)
            (RightOf9 ?x)
          )
          (and
            (RightOf9 ?x)
            (LeftOf3 ?x)
          )
          (and
            (RightOf6 ?x)
            (LeftOf2 ?x)
          )
          (and
            (RightOf6 ?x)
            (LeftOf1 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf8 ?x)
          )
          (and
            (LeftOf7 ?x)
            (RightOf10 ?x)
          )
          (and
            (RightOf9 ?x)
            (LeftOf4 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf10 ?x)
          )
          (and
            (RightOf6 ?x)
            (LeftOf7 ?x)
          )
          (and
            (RightOf9 ?x)
            (LeftOf7 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf7 ?x)
          )
          (and
            (LeftOf4 ?x)
            (RightOf10 ?x)
          )
          (and
            (LeftOf1 ?x)
            (RightOf8 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf8 ?x)
          )
          (and
            (LeftOf4 ?x)
            (RightOf8 ?x)
          )
          (and
            (RightOf7 ?x)
            (LeftOf2 ?x)
          )
          (and
            (RightOf8 ?x)
            (LeftOf3 ?x)
          )
          (and
            (RightOf10 ?x)
            (LeftOf3 ?x)
          )
          (and
            (RightOf6 ?x)
            (LeftOf6 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf10 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf10 ?x)
          )
          (and
            (RightOf6 ?x)
            (LeftOf5 ?x)
          )
          (and
            (RightOf8 ?x)
            (LeftOf4 ?x)
          )
          (and
            (RightOf7 ?x)
            (LeftOf3 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf8 ?x)
          )
          (and
            (RightOf7 ?x)
            (LeftOf1 ?x)
          )
          (and
            (RightOf10 ?x)
            (LeftOf4 ?x)
          )
          (and
            (RightOf7 ?x)
            (LeftOf4 ?x)
          )
          (and
            (RightOf6 ?x)
            (LeftOf3 ?x)
          )
          (and
            (LeftOf1 ?x)
            (RightOf10 ?x)
          )
          (and
            (LeftOf7 ?x)
            (RightOf8 ?x)
          )
          (and
            (RightOf7 ?x)
            (LeftOf7 ?x)
          )
          (and
            (RightOf6 ?x)
            (LeftOf4 ?x)
          )
          (and
            (RightOf9 ?x)
            (LeftOf1 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf7 ?x)
          )
          (and
            (RightOf9 ?x)
            (LeftOf2 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf9 ?x)
          )
        )
        (and
          (Column7 ?x)
        )
      )
      (when
        (or
          (and
            (LeftOf6 ?x)
            (RightOf9 ?x)
          )
          (and
            (RightOf9 ?x)
            (LeftOf3 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf8 ?x)
          )
          (and
            (LeftOf7 ?x)
            (RightOf10 ?x)
          )
          (and
            (RightOf9 ?x)
            (LeftOf4 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf10 ?x)
          )
          (and
            (RightOf9 ?x)
            (LeftOf7 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf7 ?x)
          )
          (and
            (LeftOf4 ?x)
            (RightOf10 ?x)
          )
          (and
            (RightOf9 ?x)
            (LeftOf2 ?x)
          )
          (and
            (LeftOf1 ?x)
            (RightOf8 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf8 ?x)
          )
          (and
            (LeftOf4 ?x)
            (RightOf8 ?x)
          )
          (and
            (RightOf7 ?x)
            (LeftOf2 ?x)
          )
          (and
            (RightOf8 ?x)
            (LeftOf3 ?x)
          )
          (and
            (RightOf10 ?x)
            (LeftOf3 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf10 ?x)
          )
          (and
            (LeftOf7 ?x)
            (RightOf7 ?x)
          )
          (and
            (LeftOf8 ?x)
            (RightOf8 ?x)
          )
          (and
            (LeftOf8 ?x)
            (RightOf10 ?x)
          )
          (and
            (RightOf8 ?x)
            (LeftOf4 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf8 ?x)
          )
          (and
            (RightOf7 ?x)
            (LeftOf3 ?x)
          )
          (and
            (RightOf7 ?x)
            (LeftOf1 ?x)
          )
          (and
            (RightOf10 ?x)
            (LeftOf4 ?x)
          )
          (and
            (RightOf7 ?x)
            (LeftOf4 ?x)
          )
          (and
            (LeftOf1 ?x)
            (RightOf10 ?x)
          )
          (and
            (RightOf9 ?x)
            (LeftOf8 ?x)
          )
          (and
            (LeftOf7 ?x)
            (RightOf8 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf10 ?x)
          )
          (and
            (RightOf9 ?x)
            (LeftOf1 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf7 ?x)
          )
          (and
            (LeftOf8 ?x)
            (RightOf7 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf9 ?x)
          )
        )
        (and
          (Column8 ?x)
        )
      )
      (when
        (or
          (and
            (LeftOf6 ?x)
            (RightOf9 ?x)
          )
          (and
            (RightOf9 ?x)
            (LeftOf3 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf8 ?x)
          )
          (and
            (LeftOf7 ?x)
            (RightOf10 ?x)
          )
          (and
            (RightOf9 ?x)
            (LeftOf4 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf10 ?x)
          )
          (and
            (RightOf9 ?x)
            (LeftOf7 ?x)
          )
          (and
            (LeftOf4 ?x)
            (RightOf10 ?x)
          )
          (and
            (LeftOf1 ?x)
            (RightOf8 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf8 ?x)
          )
          (and
            (RightOf10 ?x)
            (LeftOf4 ?x)
          )
          (and
            (RightOf10 ?x)
            (LeftOf3 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf10 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf10 ?x)
          )
          (and
            (LeftOf8 ?x)
            (RightOf10 ?x)
          )
          (and
            (LeftOf9 ?x)
            (RightOf10 ?x)
          )
          (and
            (RightOf8 ?x)
            (LeftOf4 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf8 ?x)
          )
          (and
            (RightOf9 ?x)
            (LeftOf9 ?x)
          )
          (and
            (RightOf8 ?x)
            (LeftOf3 ?x)
          )
          (and
            (LeftOf9 ?x)
            (RightOf8 ?x)
          )
          (and
            (LeftOf1 ?x)
            (RightOf10 ?x)
          )
          (and
            (RightOf9 ?x)
            (LeftOf8 ?x)
          )
          (and
            (LeftOf7 ?x)
            (RightOf8 ?x)
          )
          (and
            (LeftOf8 ?x)
            (RightOf8 ?x)
          )
          (and
            (RightOf9 ?x)
            (LeftOf1 ?x)
          )
          (and
            (RightOf9 ?x)
            (LeftOf2 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf9 ?x)
          )
        )
        (and
          (Column9 ?x)
        )
      )
      (when
        (or
          (and
            (RightOf9 ?x)
            (LeftOf7 ?x)
          )
          (and
            (LeftOf9 ?x)
            (RightOf10 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf9 ?x)
          )
          (and
            (RightOf9 ?x)
            (LeftOf3 ?x)
          )
          (and
            (RightOf9 ?x)
            (LeftOf4 ?x)
          )
          (and
            (LeftOf4 ?x)
            (RightOf10 ?x)
          )
          (and
            (LeftOf1 ?x)
            (RightOf10 ?x)
          )
          (and
            (RightOf9 ?x)
            (LeftOf2 ?x)
          )
          (and
            (RightOf9 ?x)
            (LeftOf8 ?x)
          )
          (and
            (LeftOf10 ?x)
            (RightOf10 ?x)
          )
          (and
            (LeftOf7 ?x)
            (RightOf10 ?x)
          )
          (and
            (RightOf9 ?x)
            (LeftOf10 ?x)
          )
          (and
            (RightOf9 ?x)
            (LeftOf9 ?x)
          )
          (and
            (RightOf9 ?x)
            (LeftOf1 ?x)
          )
          (and
            (RightOf10 ?x)
            (LeftOf4 ?x)
          )
          (and
            (RightOf10 ?x)
            (LeftOf3 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf9 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf10 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf10 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf10 ?x)
          )
          (and
            (LeftOf8 ?x)
            (RightOf10 ?x)
          )
        )
        (and
          (Column10 ?x)
        )
      )
    )
  )
  (:action CheckConsistencyAction
    :parameters ( )
    :precondition ( and
      (CheckConsistency)
      (not (Error))
    )
    :effect ( and
      (not (CheckConsistency))
      (when
        (or
          (exists (?x_19 )
            (and
              (AboveOf10 ?x_19)
              (BelowOf4 ?x_19)
            )
          )
          (exists (?x_2 )
            (and
              (RightOf9 ?x_2)
              (LeftOf5 ?x_2)
            )
          )
          (exists (?x_19 )
            (and
              (BelowOf4 ?x_19)
              (AboveOf9 ?x_19)
            )
          )
          (exists (?x_0 )
            (and
              (BelowOf3 ?x_0)
              (AboveOf4 ?x_0)
            )
          )
          (exists (?x_14 )
            (and
              (AboveOf4 ?x_14)
              (BelowOf2 ?x_14)
            )
          )
          (exists (?x_8 )
            (and
              (AboveOf5 ?x_8)
              (BelowOf5 ?x_8)
            )
          )
          (exists (?x_16 )
            (and
              (RightOf9 ?x_16)
              (LeftOf3 ?x_16)
            )
          )
          (exists (?x_5 )
            (and
              (RightOf7 ?x_5)
              (LeftOf5 ?x_5)
            )
          )
          (exists (?x_10 )
            (and
              (BelowOf3 ?x_10)
              (AboveOf3 ?x_10)
            )
          )
          (exists (?x_8 )
            (and
              (BelowOf5 ?x_8)
              (AboveOf10 ?x_8)
            )
          )
          (exists (?x_15 )
            (and
              (RightOf2 ?x_15)
              (LeftOf2 ?x_15)
            )
          )
          (exists (?x_15 )
            (and
              (LeftOf1 ?x_15)
              (RightOf7 ?x_15)
            )
          )
          (exists (?x_18 )
            (and
              (RightOf6 ?x_18)
              (LeftOf1 ?x_18)
            )
          )
          (exists (?x_13 )
            (and
              (RightOf10 ?x_13)
              (LeftOf8 ?x_13)
            )
          )
          (exists (?x_13 )
            (and
              (LeftOf4 ?x_13)
              (RightOf9 ?x_13)
            )
          )
          (exists (?x_18 )
            (and
              (RightOf8 ?x_18)
              (LeftOf1 ?x_18)
            )
          )
          (exists (?x_18 )
            (and
              (LeftOf2 ?x_18)
              (RightOf7 ?x_18)
            )
          )
          (exists (?x_11 )
            (and
              (BelowOf7 ?x_11)
              (AboveOf10 ?x_11)
            )
          )
          (exists (?x_18 )
            (and
              (RightOf10 ?x_18)
              (LeftOf1 ?x_18)
            )
          )
          (exists (?x_0 )
            (and
              (AboveOf7 ?x_0)
              (BelowOf2 ?x_0)
            )
          )
          (exists (?x_4 )
            (and
              (RightOf8 ?x_4)
              (LeftOf4 ?x_4)
            )
          )
          (exists (?x_17 )
            (and
              (RightOf10 ?x_17)
              (LeftOf4 ?x_17)
            )
          )
          (exists (?x_19 )
            (and
              (AboveOf8 ?x_19)
              (BelowOf6 ?x_19)
            )
          )
          (exists (?x_10 )
            (and
              (BelowOf2 ?x_10)
              (AboveOf7 ?x_10)
            )
          )
          (exists (?x_2 )
            (and
              (LeftOf1 ?x_2)
              (RightOf9 ?x_2)
            )
          )
          (exists (?x_1 )
            (and
              (AboveOf10 ?x_1)
              (BelowOf10 ?x_1)
            )
          )
          (exists (?x_18 )
            (and
              (RightOf9 ?x_18)
              (LeftOf1 ?x_18)
            )
          )
          (exists (?x_18 )
            (and
              (RightOf8 ?x_18)
              (LeftOf4 ?x_18)
            )
          )
          (exists (?x_9 )
            (and
              (AboveOf8 ?x_9)
              (BelowOf6 ?x_9)
            )
          )
          (exists (?x_17 )
            (and
              (LeftOf8 ?x_17)
              (RightOf10 ?x_17)
            )
          )
          (exists (?x_8 )
            (and
              (AboveOf5 ?x_8)
              (BelowOf1 ?x_8)
            )
          )
          (exists (?x_15 )
            (and
              (RightOf7 ?x_15)
              (LeftOf2 ?x_15)
            )
          )
          (exists (?x_12 )
            (and
              (BelowOf3 ?x_12)
              (AboveOf8 ?x_12)
            )
          )
          (exists (?x_3 )
            (and
              (LeftOf1 ?x_3)
              (RightOf5 ?x_3)
            )
          )
          (exists (?x_11 )
            (and
              (BelowOf1 ?x_11)
              (AboveOf10 ?x_11)
            )
          )
          (exists (?x_19 )
            (and
              (AboveOf10 ?x_19)
              (BelowOf1 ?x_19)
            )
          )
          (exists (?x_11 )
            (and
              (BelowOf3 ?x_11)
              (AboveOf10 ?x_11)
            )
          )
          (exists (?x_3 )
            (and
              (LeftOf1 ?x_3)
              (RightOf10 ?x_3)
            )
          )
          (exists (?x_14 )
            (and
              (BelowOf1 ?x_14)
              (AboveOf8 ?x_14)
            )
          )
          (exists (?x_13 )
            (and
              (RightOf9 ?x_13)
              (LeftOf5 ?x_13)
            )
          )
          (exists (?x_18 )
            (and
              (LeftOf2 ?x_18)
              (RightOf5 ?x_18)
            )
          )
          (exists (?x_9 )
            (and
              (BelowOf7 ?x_9)
              (AboveOf8 ?x_9)
            )
          )
          (exists (?x_0 )
            (and
              (BelowOf4 ?x_0)
              (AboveOf7 ?x_0)
            )
          )
          (exists (?x_18 )
            (and
              (RightOf10 ?x_18)
              (LeftOf2 ?x_18)
            )
          )
          (exists (?x_18 )
            (and
              (RightOf8 ?x_18)
              (LeftOf3 ?x_18)
            )
          )
          (exists (?x_16 )
            (and
              (LeftOf2 ?x_16)
              (RightOf8 ?x_16)
            )
          )
          (exists (?x_19 )
            (and
              (BelowOf2 ?x_19)
              (AboveOf7 ?x_19)
            )
          )
          (exists (?x_0 )
            (and
              (BelowOf4 ?x_0)
              (AboveOf5 ?x_0)
            )
          )
          (exists (?x_0 )
            (and
              (AboveOf8 ?x_0)
              (BelowOf2 ?x_0)
            )
          )
          (exists (?x_18 )
            (and
              (RightOf4 ?x_18)
              (LeftOf3 ?x_18)
            )
          )
          (exists (?x_12 )
            (and
              (AboveOf10 ?x_12)
              (BelowOf4 ?x_12)
            )
          )
          (exists (?x_9 )
            (and
              (AboveOf8 ?x_9)
              (BelowOf5 ?x_9)
            )
          )
          (exists (?x_9 )
            (and
              (BelowOf1 ?x_9)
              (AboveOf10 ?x_9)
            )
          )
          (exists (?x_1 )
            (and
              (AboveOf10 ?x_1)
              (BelowOf8 ?x_1)
            )
          )
          (exists (?x_16 )
            (and
              (RightOf4 ?x_16)
              (LeftOf3 ?x_16)
            )
          )
          (exists (?x_10 )
            (and
              (AboveOf8 ?x_10)
              (BelowOf3 ?x_10)
            )
          )
          (exists (?x_2 )
            (and
              (RightOf7 ?x_2)
              (LeftOf5 ?x_2)
            )
          )
          (exists (?x_4 )
            (and
              (RightOf10 ?x_4)
              (LeftOf4 ?x_4)
            )
          )
          (exists (?x_10 )
            (and
              (BelowOf3 ?x_10)
              (AboveOf9 ?x_10)
            )
          )
          (exists (?x_0 )
            (and
              (AboveOf5 ?x_0)
              (BelowOf1 ?x_0)
            )
          )
          (exists (?x_18 )
            (and
              (RightOf5 ?x_18)
              (LeftOf3 ?x_18)
            )
          )
          (exists (?x_10 )
            (and
              (AboveOf6 ?x_10)
              (BelowOf3 ?x_10)
            )
          )
          (exists (?x_19 )
            (and
              (AboveOf7 ?x_19)
              (BelowOf1 ?x_19)
            )
          )
          (exists (?x_8 )
            (and
              (AboveOf10 ?x_8)
              (BelowOf1 ?x_8)
            )
          )
          (exists (?x_4 )
            (and
              (RightOf9 ?x_4)
              (LeftOf1 ?x_4)
            )
          )
          (exists (?x_5 )
            (and
              (RightOf6 ?x_5)
              (LeftOf5 ?x_5)
            )
          )
          (exists (?x_18 )
            (and
              (RightOf6 ?x_18)
              (LeftOf3 ?x_18)
            )
          )
          (exists (?x_0 )
            (and
              (AboveOf9 ?x_0)
              (BelowOf2 ?x_0)
            )
          )
          (exists (?x_6 )
            (and
              (RightOf10 ?x_6)
              (LeftOf9 ?x_6)
            )
          )
          (exists (?x_12 )
            (and
              (BelowOf2 ?x_12)
              (AboveOf7 ?x_12)
            )
          )
          (exists (?x_12 )
            (and
              (AboveOf7 ?x_12)
              (BelowOf7 ?x_12)
            )
          )
          (exists (?x_18 )
            (and
              (LeftOf2 ?x_18)
              (RightOf6 ?x_18)
            )
          )
          (exists (?x_12 )
            (and
              (BelowOf1 ?x_12)
              (AboveOf9 ?x_12)
            )
          )
          (exists (?x_15 )
            (and
              (RightOf3 ?x_15)
              (LeftOf2 ?x_15)
            )
          )
          (exists (?x_3 )
            (and
              (RightOf8 ?x_3)
              (LeftOf1 ?x_3)
            )
          )
          (exists (?x_5 )
            (and
              (LeftOf3 ?x_5)
              (RightOf10 ?x_5)
            )
          )
          (exists (?x_17 )
            (and
              (LeftOf6 ?x_17)
              (RightOf8 ?x_17)
            )
          )
          (exists (?x_10 )
            (and
              (BelowOf3 ?x_10)
              (AboveOf4 ?x_10)
            )
          )
          (exists (?x_2 )
            (and
              (RightOf6 ?x_2)
              (LeftOf5 ?x_2)
            )
          )
          (exists (?x_10 )
            (and
              (AboveOf8 ?x_10)
              (BelowOf1 ?x_10)
            )
          )
          (exists (?x_6 )
            (and
              (RightOf10 ?x_6)
              (LeftOf8 ?x_6)
            )
          )
          (exists (?x_14 )
            (and
              (AboveOf10 ?x_14)
              (BelowOf2 ?x_14)
            )
          )
          (exists (?x_2 )
            (and
              (RightOf9 ?x_2)
              (LeftOf3 ?x_2)
            )
          )
          (exists (?x_2 )
            (and
              (LeftOf4 ?x_2)
              (RightOf9 ?x_2)
            )
          )
          (exists (?x_11 )
            (and
              (BelowOf6 ?x_11)
              (AboveOf10 ?x_11)
            )
          )
          (exists (?x_7 )
            (and
              (BelowOf1 ?x_7)
              (AboveOf6 ?x_7)
            )
          )
          (exists (?x_16 )
            (and
              (RightOf10 ?x_16)
              (LeftOf1 ?x_16)
            )
          )
          (exists (?x_11 )
            (and
              (BelowOf1 ?x_11)
              (AboveOf9 ?x_11)
            )
          )
          (exists (?x_6 )
            (and
              (RightOf10 ?x_6)
              (LeftOf4 ?x_6)
            )
          )
          (exists (?x_0 )
            (and
              (BelowOf3 ?x_0)
              (AboveOf7 ?x_0)
            )
          )
          (exists (?x_6 )
            (and
              (RightOf10 ?x_6)
              (LeftOf7 ?x_6)
            )
          )
          (exists (?x_19 )
            (and
              (AboveOf6 ?x_19)
              (BelowOf1 ?x_19)
            )
          )
          (exists (?x_13 )
            (and
              (RightOf10 ?x_13)
              (LeftOf5 ?x_13)
            )
          )
          (exists (?x_2 )
            (and
              (RightOf7 ?x_2)
              (LeftOf3 ?x_2)
            )
          )
          (exists (?x_15 )
            (and
              (RightOf9 ?x_15)
              (LeftOf2 ?x_15)
            )
          )
          (exists (?x_18 )
            (and
              (LeftOf4 ?x_18)
              (RightOf5 ?x_18)
            )
          )
          (exists (?x_16 )
            (and
              (RightOf6 ?x_16)
              (LeftOf1 ?x_16)
            )
          )
          (exists (?x_5 )
            (and
              (LeftOf5 ?x_5)
              (RightOf9 ?x_5)
            )
          )
          (exists (?x_13 )
            (and
              (RightOf10 ?x_13)
              (LeftOf2 ?x_13)
            )
          )
          (exists (?x_8 )
            (and
              (AboveOf5 ?x_8)
              (BelowOf4 ?x_8)
            )
          )
          (exists (?x_5 )
            (and
              (LeftOf6 ?x_5)
              (RightOf7 ?x_5)
            )
          )
          (exists (?x_10 )
            (and
              (BelowOf2 ?x_10)
              (AboveOf10 ?x_10)
            )
          )
          (exists (?x_5 )
            (and
              (RightOf6 ?x_5)
              (LeftOf2 ?x_5)
            )
          )
          (exists (?x_6 )
            (and
              (LeftOf2 ?x_6)
              (RightOf10 ?x_6)
            )
          )
          (exists (?x_12 )
            (and
              (AboveOf8 ?x_12)
              (BelowOf7 ?x_12)
            )
          )
          (exists (?x_6 )
            (and
              (LeftOf5 ?x_6)
              (RightOf10 ?x_6)
            )
          )
          (exists (?x_2 )
            (and
              (RightOf6 ?x_2)
              (LeftOf4 ?x_2)
            )
          )
          (exists (?x_12 )
            (and
              (BelowOf5 ?x_12)
              (AboveOf10 ?x_12)
            )
          )
          (exists (?x_8 )
            (and
              (AboveOf6 ?x_8)
              (BelowOf4 ?x_8)
            )
          )
          (exists (?x_9 )
            (and
              (BelowOf3 ?x_9)
              (AboveOf9 ?x_9)
            )
          )
          (exists (?x_17 )
            (and
              (RightOf9 ?x_17)
              (LeftOf6 ?x_17)
            )
          )
          (exists (?x_16 )
            (and
              (RightOf5 ?x_16)
              (LeftOf1 ?x_16)
            )
          )
          (exists (?x_6 )
            (and
              (LeftOf10 ?x_6)
              (RightOf10 ?x_6)
            )
          )
          (exists (?x_17 )
            (and
              (RightOf10 ?x_17)
              (LeftOf3 ?x_17)
            )
          )
          (exists (?x_5 )
            (and
              (RightOf9 ?x_5)
              (LeftOf5 ?x_5)
            )
          )
          (exists (?x_10 )
            (and
              (AboveOf9 ?x_10)
              (BelowOf1 ?x_10)
            )
          )
          (exists (?x_17 )
            (and
              (LeftOf2 ?x_17)
              (RightOf9 ?x_17)
            )
          )
          (exists (?x_0 )
            (and
              (BelowOf3 ?x_0)
              (AboveOf8 ?x_0)
            )
          )
          (exists (?x_15 )
            (and
              (RightOf10 ?x_15)
              (LeftOf2 ?x_15)
            )
          )
          (exists (?x_5 )
            (and
              (LeftOf4 ?x_5)
              (RightOf9 ?x_5)
            )
          )
          (exists (?x_12 )
            (and
              (BelowOf5 ?x_12)
              (AboveOf7 ?x_12)
            )
          )
          (exists (?x_1 )
            (and
              (AboveOf10 ?x_1)
              (BelowOf6 ?x_1)
            )
          )
          (exists (?x_12 )
            (and
              (AboveOf7 ?x_12)
              (BelowOf4 ?x_12)
            )
          )
          (exists (?x_14 )
            (and
              (BelowOf1 ?x_14)
              (AboveOf2 ?x_14)
            )
          )
          (exists (?x_11 )
            (and
              (AboveOf9 ?x_11)
              (BelowOf4 ?x_11)
            )
          )
          (exists (?x_19 )
            (and
              (BelowOf2 ?x_19)
              (AboveOf10 ?x_19)
            )
          )
          (exists (?x_17 )
            (and
              (RightOf8 ?x_17)
              (LeftOf4 ?x_17)
            )
          )
          (exists (?x_16 )
            (and
              (LeftOf3 ?x_16)
              (RightOf5 ?x_16)
            )
          )
          (exists (?x_2 )
            (and
              (LeftOf4 ?x_2)
              (RightOf8 ?x_2)
            )
          )
          (exists (?x_7 )
            (and
              (BelowOf1 ?x_7)
              (AboveOf5 ?x_7)
            )
          )
          (exists (?x_18 )
            (and
              (LeftOf4 ?x_18)
              (RightOf7 ?x_18)
            )
          )
          (exists (?x_2 )
            (and
              (LeftOf1 ?x_2)
              (RightOf8 ?x_2)
            )
          )
          (exists (?x_8 )
            (and
              (BelowOf5 ?x_8)
              (AboveOf7 ?x_8)
            )
          )
          (exists (?x_0 )
            (and
              (AboveOf4 ?x_0)
              (BelowOf4 ?x_0)
            )
          )
          (exists (?x_16 )
            (and
              (LeftOf2 ?x_16)
              (RightOf6 ?x_16)
            )
          )
          (exists (?x_4 )
            (and
              (RightOf7 ?x_4)
              (LeftOf6 ?x_4)
            )
          )
          (exists (?x_8 )
            (and
              (AboveOf9 ?x_8)
              (BelowOf4 ?x_8)
            )
          )
          (exists (?x_19 )
            (and
              (BelowOf5 ?x_19)
              (AboveOf6 ?x_19)
            )
          )
          (exists (?x_0 )
            (and
              (BelowOf1 ?x_0)
              (AboveOf10 ?x_0)
            )
          )
          (exists (?x_16 )
            (and
              (LeftOf2 ?x_16)
              (RightOf4 ?x_16)
            )
          )
          (exists (?x_2 )
            (and
              (RightOf10 ?x_2)
              (LeftOf3 ?x_2)
            )
          )
          (exists (?x_0 )
            (and
              (BelowOf2 ?x_0)
              (AboveOf10 ?x_0)
            )
          )
          (exists (?x_18 )
            (and
              (RightOf6 ?x_18)
              (LeftOf4 ?x_18)
            )
          )
          (exists (?x_7 )
            (and
              (BelowOf1 ?x_7)
              (AboveOf4 ?x_7)
            )
          )
          (exists (?x_4 )
            (and
              (RightOf10 ?x_4)
              (LeftOf6 ?x_4)
            )
          )
          (exists (?x_19 )
            (and
              (AboveOf8 ?x_19)
              (BelowOf4 ?x_19)
            )
          )
          (exists (?x_10 )
            (and
              (AboveOf6 ?x_10)
              (BelowOf1 ?x_10)
            )
          )
          (exists (?x_10 )
            (and
              (BelowOf1 ?x_10)
              (AboveOf10 ?x_10)
            )
          )
          (exists (?x_18 )
            (and
              (RightOf9 ?x_18)
              (LeftOf4 ?x_18)
            )
          )
          (exists (?x_12 )
            (and
              (BelowOf6 ?x_12)
              (AboveOf8 ?x_12)
            )
          )
          (exists (?x_12 )
            (and
              (AboveOf7 ?x_12)
              (BelowOf1 ?x_12)
            )
          )
          (exists (?x_16 )
            (and
              (RightOf9 ?x_16)
              (LeftOf1 ?x_16)
            )
          )
          (exists (?x_18 )
            (and
              (RightOf9 ?x_18)
              (LeftOf3 ?x_18)
            )
          )
          (exists (?x_19 )
            (and
              (AboveOf7 ?x_19)
              (BelowOf5 ?x_19)
            )
          )
          (exists (?x_15 )
            (and
              (LeftOf1 ?x_15)
              (RightOf6 ?x_15)
            )
          )
          (exists (?x_16 )
            (and
              (RightOf4 ?x_16)
              (LeftOf1 ?x_16)
            )
          )
          (exists (?x_3 )
            (and
              (LeftOf1 ?x_3)
              (RightOf4 ?x_3)
            )
          )
          (exists (?x_2 )
            (and
              (LeftOf5 ?x_2)
              (RightOf10 ?x_2)
            )
          )
          (exists (?x_19 )
            (and
              (AboveOf8 ?x_19)
              (BelowOf3 ?x_19)
            )
          )
          (exists (?x_13 )
            (and
              (LeftOf4 ?x_13)
              (RightOf10 ?x_13)
            )
          )
          (exists (?x_10 )
            (and
              (BelowOf3 ?x_10)
              (AboveOf10 ?x_10)
            )
          )
          (exists (?x_4 )
            (and
              (LeftOf2 ?x_4)
              (RightOf7 ?x_4)
            )
          )
          (exists (?x_6 )
            (and
              (RightOf10 ?x_6)
              (LeftOf6 ?x_6)
            )
          )
          (exists (?x_17 )
            (and
              (RightOf9 ?x_17)
              (LeftOf1 ?x_17)
            )
          )
          (exists (?x_9 )
            (and
              (AboveOf8 ?x_9)
              (BelowOf1 ?x_9)
            )
          )
          (exists (?x_10 )
            (and
              (BelowOf1 ?x_10)
              (AboveOf7 ?x_10)
            )
          )
          (exists (?x_11 )
            (and
              (AboveOf10 ?x_11)
              (BelowOf2 ?x_11)
            )
          )
          (exists (?x_10 )
            (and
              (AboveOf10 ?x_10)
              (BelowOf2 ?x_10)
            )
          )
          (exists (?x_16 )
            (and
              (LeftOf2 ?x_16)
              (RightOf7 ?x_16)
            )
          )
          (exists (?x_1 )
            (and
              (AboveOf10 ?x_1)
              (BelowOf3 ?x_1)
            )
          )
          (exists (?x_2 )
            (and
              (LeftOf4 ?x_2)
              (RightOf10 ?x_2)
            )
          )
          (exists (?x_5 )
            (and
              (RightOf9 ?x_5)
              (LeftOf2 ?x_5)
            )
          )
          (exists (?x_8 )
            (and
              (BelowOf2 ?x_8)
              (AboveOf10 ?x_8)
            )
          )
          (exists (?x_15 )
            (and
              (RightOf5 ?x_15)
              (LeftOf1 ?x_15)
            )
          )
          (exists (?x_19 )
            (and
              (AboveOf7 ?x_19)
              (BelowOf3 ?x_19)
            )
          )
          (exists (?x_18 )
            (and
              (RightOf4 ?x_18)
              (LeftOf4 ?x_18)
            )
          )
          (exists (?x_12 )
            (and
              (BelowOf4 ?x_12)
              (AboveOf9 ?x_12)
            )
          )
          (exists (?x_4 )
            (and
              (RightOf9 ?x_4)
              (LeftOf4 ?x_4)
            )
          )
          (exists (?x_18 )
            (and
              (RightOf5 ?x_18)
              (LeftOf1 ?x_18)
            )
          )
          (exists (?x_0 )
            (and
              (BelowOf4 ?x_0)
              (AboveOf9 ?x_0)
            )
          )
          (exists (?x_9 )
            (and
              (BelowOf3 ?x_9)
              (AboveOf10 ?x_9)
            )
          )
          (exists (?x_5 )
            (and
              (LeftOf1 ?x_5)
              (RightOf10 ?x_5)
            )
          )
          (exists (?x_12 )
            (and
              (BelowOf3 ?x_12)
              (AboveOf10 ?x_12)
            )
          )
          (exists (?x_4 )
            (and
              (LeftOf5 ?x_4)
              (RightOf8 ?x_4)
            )
          )
          (exists (?x_15 )
            (and
              (LeftOf1 ?x_15)
              (RightOf8 ?x_15)
            )
          )
          (exists (?x_15 )
            (and
              (LeftOf1 ?x_15)
              (RightOf2 ?x_15)
            )
          )
          (exists (?x_8 )
            (and
              (AboveOf10 ?x_8)
              (BelowOf2 ?x_8)
            )
          )
          (exists (?x_5 )
            (and
              (RightOf7 ?x_5)
              (LeftOf3 ?x_5)
            )
          )
          (exists (?x_4 )
            (and
              (LeftOf2 ?x_4)
              (RightOf8 ?x_4)
            )
          )
          (exists (?x_18 )
            (and
              (RightOf10 ?x_18)
              (LeftOf3 ?x_18)
            )
          )
          (exists (?x_12 )
            (and
              (AboveOf8 ?x_12)
              (BelowOf2 ?x_12)
            )
          )
          (exists (?x_13 )
            (and
              (LeftOf2 ?x_13)
              (RightOf9 ?x_13)
            )
          )
          (exists (?x_15 )
            (and
              (RightOf8 ?x_15)
              (LeftOf2 ?x_15)
            )
          )
          (exists (?x_8 )
            (and
              (AboveOf6 ?x_8)
              (BelowOf1 ?x_8)
            )
          )
          (exists (?x_12 )
            (and
              (BelowOf2 ?x_12)
              (AboveOf9 ?x_12)
            )
          )
          (exists (?x_12 )
            (and
              (AboveOf10 ?x_12)
              (BelowOf2 ?x_12)
            )
          )
          (exists (?x_2 )
            (and
              (RightOf5 ?x_2)
              (LeftOf1 ?x_2)
            )
          )
          (exists (?x_4 )
            (and
              (LeftOf7 ?x_4)
              (RightOf7 ?x_4)
            )
          )
          (exists (?x_4 )
            (and
              (LeftOf3 ?x_4)
              (RightOf7 ?x_4)
            )
          )
          (exists (?x_5 )
            (and
              (RightOf8 ?x_5)
              (LeftOf2 ?x_5)
            )
          )
          (exists (?x_4 )
            (and
              (RightOf8 ?x_4)
              (LeftOf7 ?x_4)
            )
          )
          (exists (?x_14 )
            (and
              (AboveOf3 ?x_14)
              (BelowOf2 ?x_14)
            )
          )
          (exists (?x_0 )
            (and
              (AboveOf8 ?x_0)
              (BelowOf1 ?x_0)
            )
          )
          (exists (?x_14 )
            (and
              (BelowOf1 ?x_14)
              (AboveOf9 ?x_14)
            )
          )
          (exists (?x_7 )
            (and
              (BelowOf1 ?x_7)
              (AboveOf3 ?x_7)
            )
          )
          (exists (?x_2 )
            (and
              (RightOf8 ?x_2)
              (LeftOf5 ?x_2)
            )
          )
          (exists (?x_16 )
            (and
              (LeftOf2 ?x_16)
              (RightOf9 ?x_16)
            )
          )
          (exists (?x_5 )
            (and
              (LeftOf6 ?x_5)
              (RightOf6 ?x_5)
            )
          )
          (exists (?x_11 )
            (and
              (BelowOf5 ?x_11)
              (AboveOf10 ?x_11)
            )
          )
          (exists (?x_13 )
            (and
              (RightOf9 ?x_13)
              (LeftOf6 ?x_13)
            )
          )
          (exists (?x_8 )
            (and
              (AboveOf9 ?x_8)
              (BelowOf1 ?x_8)
            )
          )
          (exists (?x_4 )
            (and
              (RightOf9 ?x_4)
              (LeftOf7 ?x_4)
            )
          )
          (exists (?x_13 )
            (and
              (RightOf10 ?x_13)
              (LeftOf7 ?x_13)
            )
          )
          (exists (?x_14 )
            (and
              (BelowOf1 ?x_14)
              (AboveOf4 ?x_14)
            )
          )
          (exists (?x_17 )
            (and
              (RightOf9 ?x_17)
              (LeftOf3 ?x_17)
            )
          )
          (exists (?x_14 )
            (and
              (AboveOf10 ?x_14)
              (BelowOf1 ?x_14)
            )
          )
          (exists (?x_18 )
            (and
              (RightOf9 ?x_18)
              (LeftOf2 ?x_18)
            )
          )
          (exists (?x_4 )
            (and
              (RightOf10 ?x_4)
              (LeftOf3 ?x_4)
            )
          )
          (exists (?x_0 )
            (and
              (BelowOf1 ?x_0)
              (AboveOf9 ?x_0)
            )
          )
          (exists (?x_8 )
            (and
              (BelowOf5 ?x_8)
              (AboveOf9 ?x_8)
            )
          )
          (exists (?x_5 )
            (and
              (RightOf9 ?x_5)
              (LeftOf3 ?x_5)
            )
          )
          (exists (?x_3 )
            (and
              (LeftOf1 ?x_3)
              (RightOf3 ?x_3)
            )
          )
          (exists (?x_9 )
            (and
              (BelowOf6 ?x_9)
              (AboveOf10 ?x_9)
            )
          )
          (exists (?x_5 )
            (and
              (RightOf6 ?x_5)
              (LeftOf4 ?x_5)
            )
          )
          (exists (?x_3 )
            (and
              (LeftOf1 ?x_3)
              (RightOf6 ?x_3)
            )
          )
          (exists (?x_5 )
            (and
              (LeftOf6 ?x_5)
              (RightOf9 ?x_5)
            )
          )
          (exists (?x_17 )
            (and
              (LeftOf7 ?x_17)
              (RightOf10 ?x_17)
            )
          )
          (exists (?x_10 )
            (and
              (AboveOf4 ?x_10)
              (BelowOf2 ?x_10)
            )
          )
          (exists (?x_19 )
            (and
              (AboveOf8 ?x_19)
              (BelowOf1 ?x_19)
            )
          )
          (exists (?x_9 )
            (and
              (BelowOf4 ?x_9)
              (AboveOf10 ?x_9)
            )
          )
          (exists (?x_1 )
            (and
              (AboveOf10 ?x_1)
              (BelowOf1 ?x_1)
            )
          )
          (exists (?x_18 )
            (and
              (LeftOf1 ?x_18)
              (RightOf7 ?x_18)
            )
          )
          (exists (?x_9 )
            (and
              (AboveOf8 ?x_9)
              (BelowOf4 ?x_9)
            )
          )
          (exists (?x_1 )
            (and
              (AboveOf10 ?x_1)
              (BelowOf5 ?x_1)
            )
          )
          (exists (?x_10 )
            (and
              (AboveOf8 ?x_10)
              (BelowOf2 ?x_10)
            )
          )
          (exists (?x_16 )
            (and
              (LeftOf1 ?x_16)
              (RightOf5 ?x_16)
            )
          )
          (exists (?x_0 )
            (and
              (BelowOf3 ?x_0)
              (AboveOf9 ?x_0)
            )
          )
          (exists (?x_6 )
            (and
              (RightOf10 ?x_6)
              (LeftOf3 ?x_6)
            )
          )
          (exists (?x_2 )
            (and
              (LeftOf1 ?x_2)
              (RightOf7 ?x_2)
            )
          )
          (exists (?x_10 )
            (and
              (AboveOf3 ?x_10)
              (BelowOf2 ?x_10)
            )
          )
          (exists (?x_10 )
            (and
              (AboveOf9 ?x_10)
              (BelowOf2 ?x_10)
            )
          )
          (exists (?x_9 )
            (and
              (AboveOf8 ?x_9)
              (BelowOf8 ?x_9)
            )
          )
          (exists (?x_5 )
            (and
              (RightOf7 ?x_5)
              (LeftOf2 ?x_5)
            )
          )
          (exists (?x_16 )
            (and
              (LeftOf2 ?x_16)
              (RightOf5 ?x_16)
            )
          )
          (exists (?x_9 )
            (and
              (BelowOf1 ?x_9)
              (AboveOf9 ?x_9)
            )
          )
          (exists (?x_8 )
            (and
              (AboveOf7 ?x_8)
              (BelowOf4 ?x_8)
            )
          )
          (exists (?x_2 )
            (and
              (RightOf5 ?x_2)
              (LeftOf5 ?x_2)
            )
          )
          (exists (?x_19 )
            (and
              (AboveOf9 ?x_19)
              (BelowOf1 ?x_19)
            )
          )
          (exists (?x_12 )
            (and
              (BelowOf5 ?x_12)
              (AboveOf9 ?x_12)
            )
          )
          (exists (?x_19 )
            (and
              (BelowOf2 ?x_19)
              (AboveOf8 ?x_19)
            )
          )
          (exists (?x_4 )
            (and
              (LeftOf4 ?x_4)
              (RightOf7 ?x_4)
            )
          )
          (exists (?x_5 )
            (and
              (RightOf6 ?x_5)
              (LeftOf3 ?x_5)
            )
          )
          (exists (?x_8 )
            (and
              (AboveOf6 ?x_8)
              (BelowOf5 ?x_8)
            )
          )
          (exists (?x_11 )
            (and
              (BelowOf2 ?x_11)
              (AboveOf10 ?x_11)
            )
          )
          (exists (?x_13 )
            (and
              (RightOf10 ?x_13)
              (LeftOf1 ?x_13)
            )
          )
          (exists (?x_4 )
            (and
              (RightOf10 ?x_4)
              (LeftOf7 ?x_4)
            )
          )
          (exists (?x_13 )
            (and
              (LeftOf8 ?x_13)
              (RightOf9 ?x_13)
            )
          )
          (exists (?x_18 )
            (and
              (RightOf6 ?x_18)
              (LeftOf2 ?x_18)
            )
          )
          (exists (?x_9 )
            (and
              (AboveOf8 ?x_9)
              (BelowOf3 ?x_9)
            )
          )
          (exists (?x_0 )
            (and
              (AboveOf6 ?x_0)
              (BelowOf2 ?x_0)
            )
          )
          (exists (?x_13 )
            (and
              (RightOf10 ?x_13)
              (LeftOf3 ?x_13)
            )
          )
          (exists (?x_12 )
            (and
              (BelowOf6 ?x_12)
              (AboveOf10 ?x_12)
            )
          )
          (exists (?x_8 )
            (and
              (BelowOf1 ?x_8)
              (AboveOf9 ?x_8)
            )
          )
          (exists (?x_14 )
            (and
              (AboveOf7 ?x_14)
              (BelowOf2 ?x_14)
            )
          )
          (exists (?x_19 )
            (and
              (AboveOf6 ?x_19)
              (BelowOf4 ?x_19)
            )
          )
          (exists (?x_17 )
            (and
              (RightOf9 ?x_17)
              (LeftOf5 ?x_17)
            )
          )
          (exists (?x_17 )
            (and
              (LeftOf3 ?x_17)
              (RightOf8 ?x_17)
            )
          )
          (exists (?x_6 )
            (and
              (RightOf10 ?x_6)
              (LeftOf1 ?x_6)
            )
          )
          (exists (?x_14 )
            (and
              (AboveOf6 ?x_14)
              (BelowOf2 ?x_14)
            )
          )
          (exists (?x_16 )
            (and
              (LeftOf2 ?x_16)
              (RightOf10 ?x_16)
            )
          )
          (exists (?x_16 )
            (and
              (LeftOf2 ?x_16)
              (RightOf3 ?x_16)
            )
          )
          (exists (?x_17 )
            (and
              (RightOf9 ?x_17)
              (LeftOf4 ?x_17)
            )
          )
          (exists (?x_14 )
            (and
              (BelowOf1 ?x_14)
              (AboveOf7 ?x_14)
            )
          )
          (exists (?x_18 )
            (and
              (RightOf4 ?x_18)
              (LeftOf1 ?x_18)
            )
          )
          (exists (?x_18 )
            (and
              (LeftOf2 ?x_18)
              (RightOf8 ?x_18)
            )
          )
          (exists (?x_2 )
            (and
              (RightOf6 ?x_2)
              (LeftOf3 ?x_2)
            )
          )
          (exists (?x_5 )
            (and
              (LeftOf6 ?x_5)
              (RightOf10 ?x_5)
            )
          )
          (exists (?x_7 )
            (and
              (AboveOf2 ?x_7)
              (BelowOf1 ?x_7)
            )
          )
          (exists (?x_9 )
            (and
              (BelowOf7 ?x_9)
              (AboveOf10 ?x_9)
            )
          )
          (exists (?x_2 )
            (and
              (LeftOf2 ?x_2)
              (RightOf9 ?x_2)
            )
          )
          (exists (?x_16 )
            (and
              (RightOf8 ?x_16)
              (LeftOf1 ?x_16)
            )
          )
          (exists (?x_9 )
            (and
              (AboveOf9 ?x_9)
              (BelowOf2 ?x_9)
            )
          )
          (exists (?x_12 )
            (and
              (BelowOf7 ?x_12)
              (AboveOf9 ?x_12)
            )
          )
          (exists (?x_5 )
            (and
              (LeftOf2 ?x_5)
              (RightOf10 ?x_5)
            )
          )
          (exists (?x_2 )
            (and
              (RightOf5 ?x_2)
              (LeftOf4 ?x_2)
            )
          )
          (exists (?x_17 )
            (and
              (LeftOf5 ?x_17)
              (RightOf8 ?x_17)
            )
          )
          (exists (?x_4 )
            (and
              (RightOf8 ?x_4)
              (LeftOf1 ?x_4)
            )
          )
          (exists (?x_11 )
            (and
              (BelowOf3 ?x_11)
              (AboveOf9 ?x_11)
            )
          )
          (exists (?x_19 )
            (and
              (AboveOf7 ?x_19)
              (BelowOf4 ?x_19)
            )
          )
          (exists (?x_13 )
            (and
              (RightOf9 ?x_13)
              (LeftOf3 ?x_13)
            )
          )
          (exists (?x_8 )
            (and
              (AboveOf6 ?x_8)
              (BelowOf2 ?x_8)
            )
          )
          (exists (?x_7 )
            (and
              (AboveOf10 ?x_7)
              (BelowOf1 ?x_7)
            )
          )
          (exists (?x_5 )
            (and
              (LeftOf4 ?x_5)
              (RightOf8 ?x_5)
            )
          )
          (exists (?x_3 )
            (and
              (LeftOf1 ?x_3)
              (RightOf2 ?x_3)
            )
          )
          (exists (?x_1 )
            (and
              (AboveOf10 ?x_1)
              (BelowOf2 ?x_1)
            )
          )
          (exists (?x_2 )
            (and
              (LeftOf1 ?x_2)
              (RightOf10 ?x_2)
            )
          )
          (exists (?x_15 )
            (and
              (LeftOf1 ?x_15)
              (RightOf10 ?x_15)
            )
          )
          (exists (?x_4 )
            (and
              (RightOf8 ?x_4)
              (LeftOf6 ?x_4)
            )
          )
          (exists (?x_18 )
            (and
              (RightOf4 ?x_18)
              (LeftOf2 ?x_18)
            )
          )
          (exists (?x_19 )
            (and
              (BelowOf6 ?x_19)
              (AboveOf6 ?x_19)
            )
          )
          (exists (?x_4 )
            (and
              (LeftOf3 ?x_4)
              (RightOf9 ?x_4)
            )
          )
          (exists (?x_16 )
            (and
              (RightOf7 ?x_16)
              (LeftOf3 ?x_16)
            )
          )
          (exists (?x_18 )
            (and
              (RightOf7 ?x_18)
              (LeftOf3 ?x_18)
            )
          )
          (exists (?x_15 )
            (and
              (LeftOf1 ?x_15)
              (RightOf4 ?x_15)
            )
          )
          (exists (?x_9 )
            (and
              (AboveOf10 ?x_9)
              (BelowOf2 ?x_9)
            )
          )
          (exists (?x_1 )
            (and
              (AboveOf10 ?x_1)
              (BelowOf9 ?x_1)
            )
          )
          (exists (?x_16 )
            (and
              (LeftOf3 ?x_16)
              (RightOf8 ?x_16)
            )
          )
          (exists (?x_0 )
            (and
              (BelowOf3 ?x_0)
              (AboveOf10 ?x_0)
            )
          )
          (exists (?x_12 )
            (and
              (BelowOf6 ?x_12)
              (AboveOf7 ?x_12)
            )
          )
          (exists (?x_17 )
            (and
              (LeftOf2 ?x_17)
              (RightOf10 ?x_17)
            )
          )
          (exists (?x_8 )
            (and
              (AboveOf5 ?x_8)
              (BelowOf2 ?x_8)
            )
          )
          (exists (?x_16 )
            (and
              (RightOf3 ?x_16)
              (LeftOf1 ?x_16)
            )
          )
          (exists (?x_2 )
            (and
              (RightOf6 ?x_2)
              (LeftOf1 ?x_2)
            )
          )
          (exists (?x_12 )
            (and
              (AboveOf8 ?x_12)
              (BelowOf5 ?x_12)
            )
          )
          (exists (?x_8 )
            (and
              (AboveOf7 ?x_8)
              (BelowOf3 ?x_8)
            )
          )
          (exists (?x_19 )
            (and
              (BelowOf5 ?x_19)
              (AboveOf10 ?x_19)
            )
          )
          (exists (?x_8 )
            (and
              (AboveOf8 ?x_8)
              (BelowOf3 ?x_8)
            )
          )
          (exists (?x_13 )
            (and
              (RightOf10 ?x_13)
              (LeftOf6 ?x_13)
            )
          )
          (exists (?x_18 )
            (and
              (LeftOf3 ?x_18)
              (RightOf7 ?x_18)
            )
          )
          (exists (?x_2 )
            (and
              (RightOf7 ?x_2)
              (LeftOf2 ?x_2)
            )
          )
          (exists (?x_15 )
            (and
              (LeftOf1 ?x_15)
              (RightOf5 ?x_15)
            )
          )
          (exists (?x_2 )
            (and
              (RightOf5 ?x_2)
              (LeftOf2 ?x_2)
            )
          )
          (exists (?x_8 )
            (and
              (AboveOf5 ?x_8)
              (BelowOf3 ?x_8)
            )
          )
          (exists (?x_2 )
            (and
              (RightOf6 ?x_2)
              (LeftOf2 ?x_2)
            )
          )
          (exists (?x_1 )
            (and
              (AboveOf10 ?x_1)
              (BelowOf4 ?x_1)
            )
          )
          (exists (?x_4 )
            (and
              (RightOf9 ?x_4)
              (LeftOf6 ?x_4)
            )
          )
          (exists (?x_3 )
            (and
              (LeftOf1 ?x_3)
              (RightOf9 ?x_3)
            )
          )
          (exists (?x_11 )
            (and
              (BelowOf5 ?x_11)
              (AboveOf9 ?x_11)
            )
          )
          (exists (?x_14 )
            (and
              (BelowOf2 ?x_14)
              (AboveOf5 ?x_14)
            )
          )
          (exists (?x_10 )
            (and
              (AboveOf5 ?x_10)
              (BelowOf2 ?x_10)
            )
          )
          (exists (?x_9 )
            (and
              (BelowOf6 ?x_9)
              (AboveOf9 ?x_9)
            )
          )
          (exists (?x_9 )
            (and
              (BelowOf4 ?x_9)
              (AboveOf9 ?x_9)
            )
          )
          (exists (?x_8 )
            (and
              (AboveOf9 ?x_8)
              (BelowOf3 ?x_8)
            )
          )
          (exists (?x_16 )
            (and
              (RightOf3 ?x_16)
              (LeftOf3 ?x_16)
            )
          )
          (exists (?x_3 )
            (and
              (RightOf7 ?x_3)
              (LeftOf1 ?x_3)
            )
          )
          (exists (?x_8 )
            (and
              (BelowOf5 ?x_8)
              (AboveOf8 ?x_8)
            )
          )
          (exists (?x_18 )
            (and
              (LeftOf1 ?x_18)
              (RightOf5 ?x_18)
            )
          )
          (exists (?x_7 )
            (and
              (AboveOf1 ?x_7)
              (BelowOf1 ?x_7)
            )
          )
          (exists (?x_17 )
            (and
              (LeftOf2 ?x_17)
              (RightOf8 ?x_17)
            )
          )
          (exists (?x_19 )
            (and
              (AboveOf8 ?x_19)
              (BelowOf5 ?x_19)
            )
          )
          (exists (?x_4 )
            (and
              (RightOf7 ?x_4)
              (LeftOf1 ?x_4)
            )
          )
          (exists (?x_9 )
            (and
              (AboveOf8 ?x_9)
              (BelowOf2 ?x_9)
            )
          )
          (exists (?x_0 )
            (and
              (AboveOf4 ?x_0)
              (BelowOf2 ?x_0)
            )
          )
          (exists (?x_14 )
            (and
              (BelowOf1 ?x_14)
              (AboveOf5 ?x_14)
            )
          )
          (exists (?x_2 )
            (and
              (RightOf8 ?x_2)
              (LeftOf2 ?x_2)
            )
          )
          (exists (?x_5 )
            (and
              (RightOf7 ?x_5)
              (LeftOf4 ?x_5)
            )
          )
          (exists (?x_14 )
            (and
              (AboveOf6 ?x_14)
              (BelowOf1 ?x_14)
            )
          )
          (exists (?x_0 )
            (and
              (BelowOf3 ?x_0)
              (AboveOf5 ?x_0)
            )
          )
          (exists (?x_0 )
            (and
              (BelowOf4 ?x_0)
              (AboveOf10 ?x_0)
            )
          )
          (exists (?x_9 )
            (and
              (BelowOf8 ?x_9)
              (AboveOf9 ?x_9)
            )
          )
          (exists (?x_14 )
            (and
              (AboveOf3 ?x_14)
              (BelowOf1 ?x_14)
            )
          )
          (exists (?x_19 )
            (and
              (BelowOf2 ?x_19)
              (AboveOf9 ?x_19)
            )
          )
          (exists (?x_15 )
            (and
              (LeftOf1 ?x_15)
              (RightOf3 ?x_15)
            )
          )
          (exists (?x_5 )
            (and
              (RightOf8 ?x_5)
              (LeftOf3 ?x_5)
            )
          )
          (exists (?x_2 )
            (and
              (LeftOf4 ?x_2)
              (RightOf7 ?x_2)
            )
          )
          (exists (?x_8 )
            (and
              (AboveOf8 ?x_8)
              (BelowOf4 ?x_8)
            )
          )
          (exists (?x_17 )
            (and
              (LeftOf5 ?x_17)
              (RightOf10 ?x_17)
            )
          )
          (exists (?x_17 )
            (and
              (RightOf10 ?x_17)
              (LeftOf1 ?x_17)
            )
          )
          (exists (?x_11 )
            (and
              (BelowOf4 ?x_11)
              (AboveOf10 ?x_11)
            )
          )
          (exists (?x_7 )
            (and
              (AboveOf7 ?x_7)
              (BelowOf1 ?x_7)
            )
          )
          (exists (?x_17 )
            (and
              (LeftOf1 ?x_17)
              (RightOf8 ?x_17)
            )
          )
          (exists (?x_12 )
            (and
              (AboveOf8 ?x_12)
              (BelowOf4 ?x_12)
            )
          )
          (exists (?x_11 )
            (and
              (BelowOf9 ?x_11)
              (AboveOf9 ?x_11)
            )
          )
          (exists (?x_9 )
            (and
              (BelowOf7 ?x_9)
              (AboveOf9 ?x_9)
            )
          )
          (exists (?x_19 )
            (and
              (BelowOf6 ?x_19)
              (AboveOf9 ?x_19)
            )
          )
          (exists (?x_19 )
            (and
              (BelowOf1 ?x_19)
              (AboveOf9 ?x_19)
            )
          )
          (exists (?x_9 )
            (and
              (BelowOf5 ?x_9)
              (AboveOf10 ?x_9)
            )
          )
          (exists (?x_0 )
            (and
              (BelowOf4 ?x_0)
              (AboveOf6 ?x_0)
            )
          )
          (exists (?x_17 )
            (and
              (LeftOf5 ?x_17)
              (RightOf9 ?x_17)
            )
          )
          (exists (?x_19 )
            (and
              (BelowOf2 ?x_19)
              (AboveOf6 ?x_19)
            )
          )
          (exists (?x_8 )
            (and
              (AboveOf7 ?x_8)
              (BelowOf2 ?x_8)
            )
          )
          (exists (?x_4 )
            (and
              (RightOf10 ?x_4)
              (LeftOf5 ?x_4)
            )
          )
          (exists (?x_5 )
            (and
              (LeftOf1 ?x_5)
              (RightOf9 ?x_5)
            )
          )
          (exists (?x_15 )
            (and
              (LeftOf2 ?x_15)
              (RightOf6 ?x_15)
            )
          )
          (exists (?x_11 )
            (and
              (BelowOf8 ?x_11)
              (AboveOf9 ?x_11)
            )
          )
          (exists (?x_12 )
            (and
              (AboveOf9 ?x_12)
              (BelowOf1 ?x_12)
            )
          )
          (exists (?x_8 )
            (and
              (BelowOf2 ?x_8)
              (AboveOf9 ?x_8)
            )
          )
          (exists (?x_16 )
            (and
              (RightOf6 ?x_16)
              (LeftOf3 ?x_16)
            )
          )
          (exists (?x_12 )
            (and
              (BelowOf3 ?x_12)
              (AboveOf7 ?x_12)
            )
          )
          (exists (?x_1 )
            (and
              (AboveOf10 ?x_1)
              (BelowOf7 ?x_1)
            )
          )
          (exists (?x_18 )
            (and
              (RightOf10 ?x_18)
              (LeftOf4 ?x_18)
            )
          )
          (exists (?x_11 )
            (and
              (BelowOf6 ?x_11)
              (AboveOf9 ?x_11)
            )
          )
          (exists (?x_3 )
            (and
              (LeftOf1 ?x_3)
              (RightOf1 ?x_3)
            )
          )
          (exists (?x_9 )
            (and
              (BelowOf8 ?x_9)
              (AboveOf10 ?x_9)
            )
          )
          (exists (?x_8 )
            (and
              (AboveOf8 ?x_8)
              (BelowOf1 ?x_8)
            )
          )
          (exists (?x_8 )
            (and
              (AboveOf10 ?x_8)
              (BelowOf3 ?x_8)
            )
          )
          (exists (?x_9 )
            (and
              (BelowOf2 ?x_9)
              (AboveOf10 ?x_9)
            )
          )
          (exists (?x_5 )
            (and
              (LeftOf5 ?x_5)
              (RightOf10 ?x_5)
            )
          )
          (exists (?x_4 )
            (and
              (LeftOf5 ?x_4)
              (RightOf7 ?x_4)
            )
          )
          (exists (?x_8 )
            (and
              (AboveOf7 ?x_8)
              (BelowOf1 ?x_8)
            )
          )
          (exists (?x_17 )
            (and
              (LeftOf8 ?x_17)
              (RightOf8 ?x_17)
            )
          )
          (exists (?x_10 )
            (and
              (AboveOf5 ?x_10)
              (BelowOf1 ?x_10)
            )
          )
          (exists (?x_12 )
            (and
              (AboveOf10 ?x_12)
              (BelowOf7 ?x_12)
            )
          )
          (exists (?x_17 )
            (and
              (RightOf9 ?x_17)
              (LeftOf8 ?x_17)
            )
          )
          (exists (?x_13 )
            (and
              (LeftOf1 ?x_13)
              (RightOf9 ?x_13)
            )
          )
          (exists (?x_15 )
            (and
              (RightOf5 ?x_15)
              (LeftOf2 ?x_15)
            )
          )
          (exists (?x_10 )
            (and
              (AboveOf5 ?x_10)
              (BelowOf3 ?x_10)
            )
          )
          (exists (?x_10 )
            (and
              (BelowOf3 ?x_10)
              (AboveOf7 ?x_10)
            )
          )
          (exists (?x_16 )
            (and
              (RightOf10 ?x_16)
              (LeftOf3 ?x_16)
            )
          )
          (exists (?x_19 )
            (and
              (AboveOf10 ?x_19)
              (BelowOf6 ?x_19)
            )
          )
          (exists (?x_11 )
            (and
              (AboveOf9 ?x_11)
              (BelowOf7 ?x_11)
            )
          )
          (exists (?x_2 )
            (and
              (RightOf5 ?x_2)
              (LeftOf3 ?x_2)
            )
          )
          (exists (?x_12 )
            (and
              (BelowOf3 ?x_12)
              (AboveOf9 ?x_12)
            )
          )
          (exists (?x_11 )
            (and
              (BelowOf8 ?x_11)
              (AboveOf10 ?x_11)
            )
          )
          (exists (?x_14 )
            (and
              (AboveOf9 ?x_14)
              (BelowOf2 ?x_14)
            )
          )
          (exists (?x_17 )
            (and
              (RightOf10 ?x_17)
              (LeftOf6 ?x_17)
            )
          )
          (exists (?x_0 )
            (and
              (BelowOf3 ?x_0)
              (AboveOf6 ?x_0)
            )
          )
          (exists (?x_13 )
            (and
              (RightOf10 ?x_13)
              (LeftOf9 ?x_13)
            )
          )
          (exists (?x_19 )
            (and
              (BelowOf5 ?x_19)
              (AboveOf9 ?x_19)
            )
          )
          (exists (?x_12 )
            (and
              (AboveOf10 ?x_12)
              (BelowOf1 ?x_12)
            )
          )
          (exists (?x_13 )
            (and
              (RightOf10 ?x_13)
              (LeftOf4 ?x_13)
            )
          )
          (exists (?x_5 )
            (and
              (LeftOf4 ?x_5)
              (RightOf10 ?x_5)
            )
          )
          (exists (?x_0 )
            (and
              (AboveOf6 ?x_0)
              (BelowOf1 ?x_0)
            )
          )
          (exists (?x_19 )
            (and
              (AboveOf10 ?x_19)
              (BelowOf3 ?x_19)
            )
          )
          (exists (?x_10 )
            (and
              (AboveOf6 ?x_10)
              (BelowOf2 ?x_10)
            )
          )
          (exists (?x_4 )
            (and
              (RightOf10 ?x_4)
              (LeftOf2 ?x_4)
            )
          )
          (exists (?x_10 )
            (and
              (AboveOf3 ?x_10)
              (BelowOf1 ?x_10)
            )
          )
          (exists (?x_8 )
            (and
              (AboveOf10 ?x_8)
              (BelowOf4 ?x_8)
            )
          )
          (exists (?x_11 )
            (and
              (AboveOf9 ?x_11)
              (BelowOf2 ?x_11)
            )
          )
          (exists (?x_14 )
            (and
              (AboveOf9 ?x_14)
              (BelowOf1 ?x_14)
            )
          )
          (exists (?x_14 )
            (and
              (AboveOf2 ?x_14)
              (BelowOf2 ?x_14)
            )
          )
          (exists (?x_8 )
            (and
              (AboveOf8 ?x_8)
              (BelowOf2 ?x_8)
            )
          )
          (exists (?x_10 )
            (and
              (AboveOf4 ?x_10)
              (BelowOf1 ?x_10)
            )
          )
          (exists (?x_19 )
            (and
              (AboveOf6 ?x_19)
              (BelowOf3 ?x_19)
            )
          )
          (exists (?x_17 )
            (and
              (LeftOf7 ?x_17)
              (RightOf8 ?x_17)
            )
          )
          (exists (?x_4 )
            (and
              (LeftOf3 ?x_4)
              (RightOf8 ?x_4)
            )
          )
          (exists (?x_19 )
            (and
              (AboveOf7 ?x_19)
              (BelowOf6 ?x_19)
            )
          )
          (exists (?x_5 )
            (and
              (LeftOf6 ?x_5)
              (RightOf8 ?x_5)
            )
          )
          (exists (?x_4 )
            (and
              (RightOf9 ?x_4)
              (LeftOf5 ?x_4)
            )
          )
          (exists (?x_15 )
            (and
              (LeftOf1 ?x_15)
              (RightOf9 ?x_15)
            )
          )
          (exists (?x_2 )
            (and
              (RightOf8 ?x_2)
              (LeftOf3 ?x_2)
            )
          )
          (exists (?x_7 )
            (and
              (AboveOf8 ?x_7)
              (BelowOf1 ?x_7)
            )
          )
          (exists (?x_5 )
            (and
              (LeftOf1 ?x_5)
              (RightOf6 ?x_5)
            )
          )
          (exists (?x_2 )
            (and
              (LeftOf2 ?x_2)
              (RightOf10 ?x_2)
            )
          )
          (exists (?x_16 )
            (and
              (RightOf7 ?x_16)
              (LeftOf1 ?x_16)
            )
          )
          (exists (?x_0 )
            (and
              (AboveOf7 ?x_0)
              (BelowOf1 ?x_0)
            )
          )
          (exists (?x_0 )
            (and
              (BelowOf4 ?x_0)
              (AboveOf8 ?x_0)
            )
          )
          (exists (?x_17 )
            (and
              (LeftOf7 ?x_17)
              (RightOf9 ?x_17)
            )
          )
          (exists (?x_11 )
            (and
              (BelowOf9 ?x_11)
              (AboveOf10 ?x_11)
            )
          )
          (exists (?x_4 )
            (and
              (LeftOf2 ?x_4)
              (RightOf9 ?x_4)
            )
          )
          (exists (?x_0 )
            (and
              (AboveOf4 ?x_0)
              (BelowOf1 ?x_0)
            )
          )
          (exists (?x_13 )
            (and
              (LeftOf9 ?x_13)
              (RightOf9 ?x_13)
            )
          )
          (exists (?x_2 )
            (and
              (RightOf8 ?x_2)
              (LeftOf4 ?x_2)
            )
          )
          (exists (?x_5 )
            (and
              (LeftOf1 ?x_5)
              (RightOf7 ?x_5)
            )
          )
          (exists (?x_5 )
            (and
              (LeftOf1 ?x_5)
              (RightOf8 ?x_5)
            )
          )
          (exists (?x_19 )
            (and
              (BelowOf3 ?x_19)
              (AboveOf9 ?x_19)
            )
          )
          (exists (?x_12 )
            (and
              (BelowOf6 ?x_12)
              (AboveOf9 ?x_12)
            )
          )
          (exists (?x_14 )
            (and
              (BelowOf2 ?x_14)
              (AboveOf8 ?x_14)
            )
          )
          (exists (?x_0 )
            (and
              (AboveOf5 ?x_0)
              (BelowOf2 ?x_0)
            )
          )
          (exists (?x_12 )
            (and
              (AboveOf8 ?x_12)
              (BelowOf1 ?x_12)
            )
          )
          (exists (?x_0 )
            (and
              (AboveOf10 ?x_0)
              (BelowOf2 ?x_0)
            )
          )
          (exists (?x_13 )
            (and
              (LeftOf7 ?x_13)
              (RightOf9 ?x_13)
            )
          )
          (exists (?x_8 )
            (and
              (AboveOf6 ?x_8)
              (BelowOf3 ?x_8)
            )
          )
          (exists (?x_5 )
            (and
              (RightOf8 ?x_5)
              (LeftOf5 ?x_5)
            )
          )
          (exists (?x_4 )
            (and
              (RightOf10 ?x_4)
              (LeftOf1 ?x_4)
            )
          )
          (exists (?x_7 )
            (and
              (AboveOf9 ?x_7)
              (BelowOf1 ?x_7)
            )
          )
          (exists (?x_9 )
            (and
              (BelowOf5 ?x_9)
              (AboveOf9 ?x_9)
            )
          )
          (exists (?x_15 )
            (and
              (RightOf4 ?x_15)
              (LeftOf2 ?x_15)
            )
          )
          (exists (?x_16 )
            (and
              (RightOf6 ?x_16)
              (LeftOf2 ?x_16)
            )
          )
        )
        (Error)
      )
    )
  )
)