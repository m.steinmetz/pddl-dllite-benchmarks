(define (problem robotProblem )
  (:domain robot)
  (:objects
    robot 
  )
  (:init
    (BelowOf19 robot)
    (AboveOf0 robot)
    (RightOf2 robot)
    (LeftOf19 robot)
  )
  (:goal
    (and
      (not (CheckConsistency))
      (not (Error))
      (Row1 robot)
      (Column2 robot)
    )
  )
)