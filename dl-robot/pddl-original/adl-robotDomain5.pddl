(define
  (domain robot)
  (:requirements :adl)
  (:predicates
    (LeftOf3 ?x )
    (LeftOf2 ?x )
    (LeftOf1 ?x )
    (Rows ?x )
    (LeftOf5 ?x )
    (LeftOf4 ?x )
    (RightOf3 ?x )
    (RightOf2 ?x )
    (RightOf1 ?x )
    (RightOf0 ?x )
    (RightOf4 ?x )
    (Columns ?x )
    (Row0 ?x )
    (Row1 ?x )
    (Row2 ?x )
    (Row3 ?x )
    (Row4 ?x )
    (BelowOf1 ?x )
    (BelowOf2 ?x )
    (BelowOf3 ?x )
    (BelowOf4 ?x )
    (BelowOf5 ?x )
    (Column4 ?x )
    (Column1 ?x )
    (Column0 ?x )
    (Column3 ?x )
    (Column2 ?x )
    (AboveOf4 ?x )
    (AboveOf0 ?x )
    (AboveOf1 ?x )
    (AboveOf2 ?x )
    (AboveOf3 ?x )
    (CheckConsistency)
    (Error)
  )
  (:action moveUp
    :parameters ( ?x )
    :precondition ( and
      (not (CheckConsistency))
      (not (Error))
      (or
        (BelowOf3 ?x)
        (AboveOf3 ?x)
        (Rows ?x)
        (AboveOf4 ?x)
        (BelowOf1 ?x)
        (BelowOf2 ?x)
        (BelowOf5 ?x)
        (BelowOf4 ?x)
        (AboveOf0 ?x)
        (AboveOf2 ?x)
        (AboveOf1 ?x)
      )
    )
    :effect ( and
      (CheckConsistency)
      (when
        (or
          (AboveOf1 ?x)
          (AboveOf4 ?x)
          (AboveOf3 ?x)
          (AboveOf0 ?x)
          (AboveOf2 ?x)
        )
        (and
          (AboveOf1 ?x)
        )
      )
      (when
        (or
          (AboveOf1 ?x)
          (AboveOf4 ?x)
          (AboveOf3 ?x)
          (AboveOf2 ?x)
        )
        (and
          (AboveOf2 ?x)
        )
      )
      (when
        (or
          (AboveOf4 ?x)
          (AboveOf3 ?x)
          (AboveOf2 ?x)
        )
        (and
          (AboveOf3 ?x)
        )
      )
      (when
        (or
          (AboveOf3 ?x)
          (AboveOf4 ?x)
        )
        (and
          (AboveOf4 ?x)
        )
      )
      (when
        (BelowOf1 ?x)
        (and
          (BelowOf2 ?x)
          (not (BelowOf1 ?x))
        )
      )
      (when
        (or
          (BelowOf1 ?x)
          (BelowOf2 ?x)
        )
        (and
          (BelowOf3 ?x)
          (not (BelowOf2 ?x))
        )
      )
      (when
        (or
          (BelowOf3 ?x)
          (BelowOf1 ?x)
          (BelowOf2 ?x)
        )
        (and
          (BelowOf4 ?x)
          (not (BelowOf3 ?x))
        )
      )
      (when
        (or
          (BelowOf4 ?x)
          (BelowOf3 ?x)
          (BelowOf1 ?x)
          (BelowOf2 ?x)
        )
        (and
          (BelowOf5 ?x)
          (not (BelowOf4 ?x))
        )
      )
      (when
        (Row0 ?x)
        (and
          (Row1 ?x)
          (not (Row0 ?x))
        )
      )
      (when
        (Row1 ?x)
        (and
          (Row2 ?x)
          (not (Row1 ?x))
        )
      )
      (when
        (Row2 ?x)
        (and
          (Row3 ?x)
          (not (Row2 ?x))
        )
      )
      (when
        (Row3 ?x)
        (and
          (Row4 ?x)
          (not (Row3 ?x))
        )
      )
      (when
        (or
          (and
            (BelowOf1 ?x)
            (AboveOf3 ?x)
          )
          (and
            (AboveOf1 ?x)
            (BelowOf1 ?x)
          )
          (and
            (AboveOf4 ?x)
            (BelowOf1 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf0 ?x)
          )
          (and
            (AboveOf2 ?x)
            (BelowOf1 ?x)
          )
        )
        (and
          (Row1 ?x)
        )
      )
      (when
        (or
          (and
            (BelowOf2 ?x)
            (AboveOf1 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf3 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf3 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf4 ?x)
          )
          (and
            (AboveOf1 ?x)
            (BelowOf1 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf2 ?x)
          )
          (and
            (AboveOf4 ?x)
            (BelowOf1 ?x)
          )
          (and
            (AboveOf2 ?x)
            (BelowOf1 ?x)
          )
        )
        (and
          (Row2 ?x)
        )
      )
      (when
        (or
          (and
            (AboveOf4 ?x)
            (BelowOf1 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf3 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf4 ?x)
          )
          (and
            (AboveOf2 ?x)
            (BelowOf2 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf3 ?x)
          )
          (and
            (AboveOf4 ?x)
            (BelowOf3 ?x)
          )
          (and
            (BelowOf3 ?x)
            (AboveOf3 ?x)
          )
          (and
            (AboveOf2 ?x)
            (BelowOf3 ?x)
          )
          (and
            (AboveOf2 ?x)
            (BelowOf1 ?x)
          )
        )
        (and
          (Row3 ?x)
        )
      )
      (when
        (or
          (and
            (AboveOf3 ?x)
            (BelowOf3 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf3 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf4 ?x)
          )
          (and
            (AboveOf4 ?x)
            (BelowOf4 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf3 ?x)
          )
          (and
            (AboveOf4 ?x)
            (BelowOf3 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf3 ?x)
          )
          (and
            (AboveOf4 ?x)
            (BelowOf1 ?x)
          )
        )
        (and
          (Row4 ?x)
        )
      )
    )
  )
  (:action moveLeft
    :parameters ( ?x )
    :precondition ( and
      (not (CheckConsistency))
      (not (Error))
      (or
        (LeftOf4 ?x)
        (LeftOf3 ?x)
        (LeftOf5 ?x)
        (RightOf1 ?x)
        (RightOf4 ?x)
        (RightOf0 ?x)
        (LeftOf2 ?x)
        (Columns ?x)
        (RightOf2 ?x)
        (LeftOf1 ?x)
        (RightOf3 ?x)
      )
    )
    :effect ( and
      (CheckConsistency)
      (when
        (or
          (LeftOf2 ?x)
          (LeftOf1 ?x)
        )
        (and
          (LeftOf1 ?x)
        )
      )
      (when
        (or
          (LeftOf2 ?x)
          (LeftOf3 ?x)
          (LeftOf1 ?x)
        )
        (and
          (LeftOf2 ?x)
        )
      )
      (when
        (or
          (LeftOf2 ?x)
          (LeftOf4 ?x)
          (LeftOf3 ?x)
          (LeftOf1 ?x)
        )
        (and
          (LeftOf3 ?x)
        )
      )
      (when
        (or
          (LeftOf2 ?x)
          (LeftOf4 ?x)
          (LeftOf3 ?x)
          (LeftOf1 ?x)
          (LeftOf5 ?x)
        )
        (and
          (LeftOf4 ?x)
        )
      )
      (when
        (or
          (RightOf2 ?x)
          (RightOf1 ?x)
          (RightOf4 ?x)
          (RightOf3 ?x)
        )
        (and
          (RightOf0 ?x)
          (not (RightOf1 ?x))
        )
      )
      (when
        (or
          (RightOf2 ?x)
          (RightOf4 ?x)
          (RightOf3 ?x)
        )
        (and
          (RightOf1 ?x)
          (not (RightOf2 ?x))
        )
      )
      (when
        (or
          (RightOf4 ?x)
          (RightOf3 ?x)
        )
        (and
          (RightOf2 ?x)
          (not (RightOf3 ?x))
        )
      )
      (when
        (RightOf4 ?x)
        (and
          (RightOf3 ?x)
          (not (RightOf4 ?x))
        )
      )
      (when
        (Column1 ?x)
        (and
          (Column0 ?x)
          (not (Column1 ?x))
        )
      )
      (when
        (Column2 ?x)
        (and
          (Column1 ?x)
          (not (Column2 ?x))
        )
      )
      (when
        (Column3 ?x)
        (and
          (Column2 ?x)
          (not (Column3 ?x))
        )
      )
      (when
        (Column4 ?x)
        (and
          (Column3 ?x)
          (not (Column4 ?x))
        )
      )
      (when
        (or
          (and
            (RightOf4 ?x)
            (LeftOf2 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf3 ?x)
          )
          (and
            (RightOf2 ?x)
            (LeftOf1 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf2 ?x)
          )
          (and
            (LeftOf1 ?x)
            (RightOf3 ?x)
          )
          (and
            (RightOf1 ?x)
            (LeftOf1 ?x)
          )
          (and
            (RightOf1 ?x)
            (LeftOf2 ?x)
          )
          (and
            (RightOf4 ?x)
            (LeftOf1 ?x)
          )
        )
        (and
          (Column0 ?x)
        )
      )
      (when
        (or
          (and
            (RightOf2 ?x)
            (LeftOf3 ?x)
          )
          (and
            (RightOf4 ?x)
            (LeftOf2 ?x)
          )
          (and
            (LeftOf3 ?x)
            (RightOf3 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf3 ?x)
          )
          (and
            (RightOf4 ?x)
            (LeftOf3 ?x)
          )
          (and
            (RightOf2 ?x)
            (LeftOf1 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf2 ?x)
          )
          (and
            (LeftOf1 ?x)
            (RightOf3 ?x)
          )
          (and
            (RightOf4 ?x)
            (LeftOf1 ?x)
          )
        )
        (and
          (Column1 ?x)
        )
      )
      (when
        (or
          (and
            (RightOf4 ?x)
            (LeftOf2 ?x)
          )
          (and
            (RightOf4 ?x)
            (LeftOf4 ?x)
          )
          (and
            (LeftOf4 ?x)
            (RightOf3 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf3 ?x)
          )
          (and
            (RightOf4 ?x)
            (LeftOf3 ?x)
          )
          (and
            (RightOf3 ?x)
            (LeftOf3 ?x)
          )
          (and
            (LeftOf1 ?x)
            (RightOf3 ?x)
          )
          (and
            (RightOf4 ?x)
            (LeftOf1 ?x)
          )
        )
        (and
          (Column2 ?x)
        )
      )
      (when
        (or
          (and
            (RightOf4 ?x)
            (LeftOf1 ?x)
          )
          (and
            (RightOf4 ?x)
            (LeftOf2 ?x)
          )
          (and
            (RightOf4 ?x)
            (LeftOf4 ?x)
          )
          (and
            (RightOf4 ?x)
            (LeftOf3 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf4 ?x)
          )
        )
        (and
          (Column3 ?x)
        )
      )
    )
  )
  (:action moveDown
    :parameters ( ?x )
    :precondition ( and
      (not (CheckConsistency))
      (not (Error))
      (or
        (BelowOf3 ?x)
        (AboveOf3 ?x)
        (Rows ?x)
        (AboveOf4 ?x)
        (BelowOf1 ?x)
        (BelowOf2 ?x)
        (BelowOf5 ?x)
        (BelowOf4 ?x)
        (AboveOf0 ?x)
        (AboveOf2 ?x)
        (AboveOf1 ?x)
      )
    )
    :effect ( and
      (CheckConsistency)
      (when
        (or
          (BelowOf1 ?x)
          (BelowOf2 ?x)
        )
        (and
          (BelowOf1 ?x)
        )
      )
      (when
        (or
          (BelowOf3 ?x)
          (BelowOf1 ?x)
          (BelowOf2 ?x)
        )
        (and
          (BelowOf2 ?x)
        )
      )
      (when
        (or
          (BelowOf4 ?x)
          (BelowOf3 ?x)
          (BelowOf1 ?x)
          (BelowOf2 ?x)
        )
        (and
          (BelowOf3 ?x)
        )
      )
      (when
        (or
          (BelowOf5 ?x)
          (BelowOf4 ?x)
          (BelowOf3 ?x)
          (BelowOf1 ?x)
          (BelowOf2 ?x)
        )
        (and
          (BelowOf4 ?x)
        )
      )
      (when
        (or
          (AboveOf1 ?x)
          (AboveOf4 ?x)
          (AboveOf3 ?x)
          (AboveOf2 ?x)
        )
        (and
          (AboveOf0 ?x)
          (not (AboveOf1 ?x))
        )
      )
      (when
        (or
          (AboveOf4 ?x)
          (AboveOf3 ?x)
          (AboveOf2 ?x)
        )
        (and
          (AboveOf1 ?x)
          (not (AboveOf2 ?x))
        )
      )
      (when
        (or
          (AboveOf3 ?x)
          (AboveOf4 ?x)
        )
        (and
          (AboveOf2 ?x)
          (not (AboveOf3 ?x))
        )
      )
      (when
        (AboveOf4 ?x)
        (and
          (AboveOf3 ?x)
          (not (AboveOf4 ?x))
        )
      )
      (when
        (Row1 ?x)
        (and
          (Row0 ?x)
          (not (Row1 ?x))
        )
      )
      (when
        (Row2 ?x)
        (and
          (Row1 ?x)
          (not (Row2 ?x))
        )
      )
      (when
        (Row3 ?x)
        (and
          (Row2 ?x)
          (not (Row3 ?x))
        )
      )
      (when
        (Row4 ?x)
        (and
          (Row3 ?x)
          (not (Row4 ?x))
        )
      )
      (when
        (or
          (and
            (BelowOf2 ?x)
            (AboveOf1 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf3 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf3 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf4 ?x)
          )
          (and
            (AboveOf1 ?x)
            (BelowOf1 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf2 ?x)
          )
          (and
            (AboveOf4 ?x)
            (BelowOf1 ?x)
          )
          (and
            (AboveOf2 ?x)
            (BelowOf1 ?x)
          )
        )
        (and
          (Row0 ?x)
        )
      )
      (when
        (or
          (and
            (AboveOf4 ?x)
            (BelowOf1 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf3 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf4 ?x)
          )
          (and
            (AboveOf2 ?x)
            (BelowOf2 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf3 ?x)
          )
          (and
            (AboveOf4 ?x)
            (BelowOf3 ?x)
          )
          (and
            (BelowOf3 ?x)
            (AboveOf3 ?x)
          )
          (and
            (AboveOf2 ?x)
            (BelowOf3 ?x)
          )
          (and
            (AboveOf2 ?x)
            (BelowOf1 ?x)
          )
        )
        (and
          (Row1 ?x)
        )
      )
      (when
        (or
          (and
            (AboveOf3 ?x)
            (BelowOf3 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf3 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf4 ?x)
          )
          (and
            (AboveOf4 ?x)
            (BelowOf4 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf3 ?x)
          )
          (and
            (AboveOf4 ?x)
            (BelowOf3 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf3 ?x)
          )
          (and
            (AboveOf4 ?x)
            (BelowOf1 ?x)
          )
        )
        (and
          (Row2 ?x)
        )
      )
      (when
        (or
          (and
            (BelowOf2 ?x)
            (AboveOf4 ?x)
          )
          (and
            (AboveOf4 ?x)
            (BelowOf3 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf4 ?x)
          )
          (and
            (AboveOf4 ?x)
            (BelowOf1 ?x)
          )
          (and
            (AboveOf4 ?x)
            (BelowOf4 ?x)
          )
        )
        (and
          (Row3 ?x)
        )
      )
    )
  )
  (:action moveRight
    :parameters ( ?x )
    :precondition ( and
      (not (CheckConsistency))
      (not (Error))
      (or
        (LeftOf4 ?x)
        (LeftOf3 ?x)
        (LeftOf5 ?x)
        (RightOf1 ?x)
        (RightOf4 ?x)
        (RightOf0 ?x)
        (LeftOf2 ?x)
        (Columns ?x)
        (RightOf2 ?x)
        (LeftOf1 ?x)
        (RightOf3 ?x)
      )
    )
    :effect ( and
      (CheckConsistency)
      (when
        (or
          (RightOf2 ?x)
          (RightOf1 ?x)
          (RightOf4 ?x)
          (RightOf3 ?x)
          (RightOf0 ?x)
        )
        (and
          (RightOf1 ?x)
        )
      )
      (when
        (or
          (RightOf2 ?x)
          (RightOf1 ?x)
          (RightOf4 ?x)
          (RightOf3 ?x)
        )
        (and
          (RightOf2 ?x)
        )
      )
      (when
        (or
          (RightOf2 ?x)
          (RightOf4 ?x)
          (RightOf3 ?x)
        )
        (and
          (RightOf3 ?x)
        )
      )
      (when
        (or
          (RightOf4 ?x)
          (RightOf3 ?x)
        )
        (and
          (RightOf4 ?x)
        )
      )
      (when
        (LeftOf1 ?x)
        (and
          (LeftOf2 ?x)
          (not (LeftOf1 ?x))
        )
      )
      (when
        (or
          (LeftOf2 ?x)
          (LeftOf1 ?x)
        )
        (and
          (LeftOf3 ?x)
          (not (LeftOf2 ?x))
        )
      )
      (when
        (or
          (LeftOf2 ?x)
          (LeftOf3 ?x)
          (LeftOf1 ?x)
        )
        (and
          (LeftOf4 ?x)
          (not (LeftOf3 ?x))
        )
      )
      (when
        (or
          (LeftOf2 ?x)
          (LeftOf4 ?x)
          (LeftOf3 ?x)
          (LeftOf1 ?x)
        )
        (and
          (LeftOf5 ?x)
          (not (LeftOf4 ?x))
        )
      )
      (when
        (Column0 ?x)
        (and
          (Column1 ?x)
          (not (Column0 ?x))
        )
      )
      (when
        (Column1 ?x)
        (and
          (Column2 ?x)
          (not (Column1 ?x))
        )
      )
      (when
        (Column2 ?x)
        (and
          (Column3 ?x)
          (not (Column2 ?x))
        )
      )
      (when
        (Column3 ?x)
        (and
          (Column4 ?x)
          (not (Column3 ?x))
        )
      )
      (when
        (or
          (and
            (LeftOf1 ?x)
            (RightOf3 ?x)
          )
          (and
            (RightOf2 ?x)
            (LeftOf1 ?x)
          )
          (and
            (RightOf1 ?x)
            (LeftOf1 ?x)
          )
          (and
            (LeftOf1 ?x)
            (RightOf0 ?x)
          )
          (and
            (RightOf4 ?x)
            (LeftOf1 ?x)
          )
        )
        (and
          (Column1 ?x)
        )
      )
      (when
        (or
          (and
            (RightOf4 ?x)
            (LeftOf2 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf3 ?x)
          )
          (and
            (RightOf2 ?x)
            (LeftOf1 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf2 ?x)
          )
          (and
            (LeftOf1 ?x)
            (RightOf3 ?x)
          )
          (and
            (RightOf1 ?x)
            (LeftOf1 ?x)
          )
          (and
            (RightOf1 ?x)
            (LeftOf2 ?x)
          )
          (and
            (RightOf4 ?x)
            (LeftOf1 ?x)
          )
        )
        (and
          (Column2 ?x)
        )
      )
      (when
        (or
          (and
            (RightOf2 ?x)
            (LeftOf3 ?x)
          )
          (and
            (RightOf4 ?x)
            (LeftOf2 ?x)
          )
          (and
            (LeftOf3 ?x)
            (RightOf3 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf3 ?x)
          )
          (and
            (RightOf4 ?x)
            (LeftOf3 ?x)
          )
          (and
            (RightOf2 ?x)
            (LeftOf1 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf2 ?x)
          )
          (and
            (LeftOf1 ?x)
            (RightOf3 ?x)
          )
          (and
            (RightOf4 ?x)
            (LeftOf1 ?x)
          )
        )
        (and
          (Column3 ?x)
        )
      )
      (when
        (or
          (and
            (RightOf4 ?x)
            (LeftOf2 ?x)
          )
          (and
            (RightOf4 ?x)
            (LeftOf4 ?x)
          )
          (and
            (LeftOf4 ?x)
            (RightOf3 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf3 ?x)
          )
          (and
            (RightOf4 ?x)
            (LeftOf3 ?x)
          )
          (and
            (RightOf3 ?x)
            (LeftOf3 ?x)
          )
          (and
            (LeftOf1 ?x)
            (RightOf3 ?x)
          )
          (and
            (RightOf4 ?x)
            (LeftOf1 ?x)
          )
        )
        (and
          (Column4 ?x)
        )
      )
    )
  )
  (:action CheckConsistencyAction
    :parameters ( )
    :precondition ( and
      (CheckConsistency)
      (not (Error))
    )
    :effect ( and
      (not (CheckConsistency))
      (when
        (or
          (exists (?x_0 )
            (and
              (RightOf4 ?x_0)
              (LeftOf2 ?x_0)
            )
          )
          (exists (?x_2 )
            (and
              (BelowOf4 ?x_2)
              (AboveOf4 ?x_2)
            )
          )
          (exists (?x_6 )
            (and
              (BelowOf1 ?x_6)
              (AboveOf2 ?x_6)
            )
          )
          (exists (?x_0 )
            (and
              (LeftOf1 ?x_0)
              (RightOf3 ?x_0)
            )
          )
          (exists (?x_4 )
            (and
              (RightOf3 ?x_4)
              (LeftOf1 ?x_4)
            )
          )
          (exists (?x_2 )
            (and
              (AboveOf4 ?x_2)
              (BelowOf2 ?x_2)
            )
          )
          (exists (?x_7 )
            (and
              (RightOf4 ?x_7)
              (LeftOf1 ?x_7)
            )
          )
          (exists (?x_0 )
            (and
              (LeftOf1 ?x_0)
              (RightOf4 ?x_0)
            )
          )
          (exists (?x_1 )
            (and
              (BelowOf1 ?x_1)
              (AboveOf4 ?x_1)
            )
          )
          (exists (?x_7 )
            (and
              (RightOf4 ?x_7)
              (LeftOf2 ?x_7)
            )
          )
          (exists (?x_3 )
            (and
              (AboveOf4 ?x_3)
              (BelowOf3 ?x_3)
            )
          )
          (exists (?x_1 )
            (and
              (BelowOf1 ?x_1)
              (AboveOf3 ?x_1)
            )
          )
          (exists (?x_4 )
            (and
              (RightOf4 ?x_4)
              (LeftOf2 ?x_4)
            )
          )
          (exists (?x_0 )
            (and
              (RightOf4 ?x_0)
              (LeftOf3 ?x_0)
            )
          )
          (exists (?x_5 )
            (and
              (LeftOf1 ?x_5)
              (RightOf4 ?x_5)
            )
          )
          (exists (?x_3 )
            (and
              (AboveOf3 ?x_3)
              (BelowOf1 ?x_3)
            )
          )
          (exists (?x_6 )
            (and
              (AboveOf1 ?x_6)
              (BelowOf1 ?x_6)
            )
          )
          (exists (?x_3 )
            (and
              (AboveOf4 ?x_3)
              (BelowOf2 ?x_3)
            )
          )
          (exists (?x_3 )
            (and
              (AboveOf3 ?x_3)
              (BelowOf2 ?x_3)
            )
          )
          (exists (?x_7 )
            (and
              (RightOf4 ?x_7)
              (LeftOf4 ?x_7)
            )
          )
          (exists (?x_5 )
            (and
              (LeftOf1 ?x_5)
              (RightOf3 ?x_5)
            )
          )
          (exists (?x_2 )
            (and
              (AboveOf4 ?x_2)
              (BelowOf1 ?x_2)
            )
          )
          (exists (?x_4 )
            (and
              (RightOf3 ?x_4)
              (LeftOf2 ?x_4)
            )
          )
          (exists (?x_1 )
            (and
              (AboveOf2 ?x_1)
              (BelowOf2 ?x_1)
            )
          )
          (exists (?x_4 )
            (and
              (RightOf2 ?x_4)
              (LeftOf1 ?x_4)
            )
          )
          (exists (?x_7 )
            (and
              (LeftOf3 ?x_7)
              (RightOf4 ?x_7)
            )
          )
          (exists (?x_0 )
            (and
              (RightOf3 ?x_0)
              (LeftOf2 ?x_0)
            )
          )
          (exists (?x_5 )
            (and
              (LeftOf1 ?x_5)
              (RightOf2 ?x_5)
            )
          )
          (exists (?x_2 )
            (and
              (BelowOf3 ?x_2)
              (AboveOf4 ?x_2)
            )
          )
          (exists (?x_1 )
            (and
              (BelowOf2 ?x_1)
              (AboveOf3 ?x_1)
            )
          )
          (exists (?x_6 )
            (and
              (BelowOf1 ?x_6)
              (AboveOf3 ?x_6)
            )
          )
          (exists (?x_4 )
            (and
              (LeftOf2 ?x_4)
              (RightOf2 ?x_4)
            )
          )
          (exists (?x_1 )
            (and
              (BelowOf2 ?x_1)
              (AboveOf4 ?x_1)
            )
          )
          (exists (?x_0 )
            (and
              (LeftOf3 ?x_0)
              (RightOf3 ?x_0)
            )
          )
          (exists (?x_3 )
            (and
              (AboveOf4 ?x_3)
              (BelowOf1 ?x_3)
            )
          )
          (exists (?x_1 )
            (and
              (AboveOf2 ?x_1)
              (BelowOf1 ?x_1)
            )
          )
          (exists (?x_5 )
            (and
              (LeftOf1 ?x_5)
              (RightOf1 ?x_5)
            )
          )
          (exists (?x_6 )
            (and
              (BelowOf1 ?x_6)
              (AboveOf4 ?x_6)
            )
          )
          (exists (?x_3 )
            (and
              (AboveOf3 ?x_3)
              (BelowOf3 ?x_3)
            )
          )
          (exists (?x_4 )
            (and
              (RightOf4 ?x_4)
              (LeftOf1 ?x_4)
            )
          )
        )
        (Error)
      )
    )
  )
)