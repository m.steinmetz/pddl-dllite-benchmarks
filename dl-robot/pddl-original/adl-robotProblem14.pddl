(define (problem robotProblem )
  (:domain robot)
  (:objects
    robot 
  )
  (:init
    (AboveOf0 robot)
    (RightOf2 robot)
    (LeftOf13 robot)
    (BelowOf13 robot)
  )
  (:goal
    (and
      (not (CheckConsistency))
      (not (Error))
      (Row1 robot)
      (Column2 robot)
    )
  )
)