(define
  (domain robot)
  (:requirements :adl)
  (:predicates
    (AboveOf14 ?x )
    (AboveOf12 ?x )
    (AboveOf13 ?x )
    (AboveOf10 ?x )
    (AboveOf11 ?x )
    (Column5 ?x )
    (Column4 ?x )
    (BelowOf12 ?x )
    (BelowOf13 ?x )
    (BelowOf10 ?x )
    (BelowOf11 ?x )
    (BelowOf14 ?x )
    (BelowOf15 ?x )
    (LeftOf3 ?x )
    (LeftOf2 ?x )
    (LeftOf1 ?x )
    (LeftOf7 ?x )
    (LeftOf6 ?x )
    (LeftOf5 ?x )
    (LeftOf4 ?x )
    (LeftOf9 ?x )
    (LeftOf8 ?x )
    (Row12 ?x )
    (Row13 ?x )
    (Row10 ?x )
    (Row11 ?x )
    (Row14 ?x )
    (AboveOf2 ?x )
    (LeftOf12 ?x )
    (Column11 ?x )
    (Column10 ?x )
    (Column13 ?x )
    (Column12 ?x )
    (Column14 ?x )
    (AboveOf4 ?x )
    (AboveOf5 ?x )
    (AboveOf6 ?x )
    (AboveOf7 ?x )
    (AboveOf0 ?x )
    (AboveOf1 ?x )
    (LeftOf11 ?x )
    (AboveOf3 ?x )
    (AboveOf8 ?x )
    (AboveOf9 ?x )
    (RightOf9 ?x )
    (RightOf8 ?x )
    (RightOf3 ?x )
    (RightOf2 ?x )
    (RightOf1 ?x )
    (RightOf0 ?x )
    (RightOf7 ?x )
    (RightOf6 ?x )
    (RightOf5 ?x )
    (RightOf4 ?x )
    (Columns ?x )
    (BelowOf1 ?x )
    (BelowOf2 ?x )
    (BelowOf3 ?x )
    (Column9 ?x )
    (BelowOf5 ?x )
    (BelowOf6 ?x )
    (BelowOf7 ?x )
    (BelowOf8 ?x )
    (BelowOf9 ?x )
    (Column7 ?x )
    (Column6 ?x )
    (Column1 ?x )
    (Column0 ?x )
    (Column3 ?x )
    (Column2 ?x )
    (Rows ?x )
    (LeftOf15 ?x )
    (LeftOf14 ?x )
    (LeftOf13 ?x )
    (RightOf14 ?x )
    (RightOf13 ?x )
    (RightOf12 ?x )
    (RightOf11 ?x )
    (RightOf10 ?x )
    (Row0 ?x )
    (Row1 ?x )
    (Row2 ?x )
    (Row3 ?x )
    (Row4 ?x )
    (Row5 ?x )
    (Row6 ?x )
    (Row7 ?x )
    (Row8 ?x )
    (Row9 ?x )
    (LeftOf10 ?x )
    (BelowOf4 ?x )
    (Column8 ?x )
    (CheckConsistency)
    (Error)
  )
  (:action moveDown
    :parameters ( ?x )
    :precondition ( and
      (not (CheckConsistency))
      (not (Error))
      (or
        (BelowOf7 ?x)
        (AboveOf9 ?x)
        (BelowOf1 ?x)
        (AboveOf7 ?x)
        (AboveOf11 ?x)
        (AboveOf1 ?x)
        (BelowOf14 ?x)
        (BelowOf8 ?x)
        (BelowOf3 ?x)
        (AboveOf14 ?x)
        (AboveOf3 ?x)
        (BelowOf9 ?x)
        (BelowOf2 ?x)
        (BelowOf5 ?x)
        (BelowOf11 ?x)
        (BelowOf10 ?x)
        (BelowOf13 ?x)
        (BelowOf4 ?x)
        (AboveOf10 ?x)
        (AboveOf0 ?x)
        (AboveOf8 ?x)
        (AboveOf6 ?x)
        (AboveOf5 ?x)
        (AboveOf13 ?x)
        (Rows ?x)
        (AboveOf4 ?x)
        (BelowOf12 ?x)
        (AboveOf12 ?x)
        (BelowOf6 ?x)
        (BelowOf15 ?x)
        (AboveOf2 ?x)
      )
    )
    :effect ( and
      (CheckConsistency)
      (when
        (or
          (BelowOf1 ?x)
          (BelowOf2 ?x)
        )
        (and
          (BelowOf1 ?x)
        )
      )
      (when
        (or
          (BelowOf3 ?x)
          (BelowOf1 ?x)
          (BelowOf2 ?x)
        )
        (and
          (BelowOf2 ?x)
        )
      )
      (when
        (or
          (BelowOf4 ?x)
          (BelowOf3 ?x)
          (BelowOf1 ?x)
          (BelowOf2 ?x)
        )
        (and
          (BelowOf3 ?x)
        )
      )
      (when
        (or
          (BelowOf5 ?x)
          (BelowOf4 ?x)
          (BelowOf3 ?x)
          (BelowOf1 ?x)
          (BelowOf2 ?x)
        )
        (and
          (BelowOf4 ?x)
        )
      )
      (when
        (or
          (BelowOf3 ?x)
          (BelowOf4 ?x)
          (BelowOf1 ?x)
          (BelowOf2 ?x)
          (BelowOf5 ?x)
          (BelowOf6 ?x)
        )
        (and
          (BelowOf5 ?x)
        )
      )
      (when
        (or
          (BelowOf3 ?x)
          (BelowOf7 ?x)
          (BelowOf4 ?x)
          (BelowOf1 ?x)
          (BelowOf2 ?x)
          (BelowOf5 ?x)
          (BelowOf6 ?x)
        )
        (and
          (BelowOf6 ?x)
        )
      )
      (when
        (or
          (BelowOf8 ?x)
          (BelowOf3 ?x)
          (BelowOf7 ?x)
          (BelowOf4 ?x)
          (BelowOf1 ?x)
          (BelowOf2 ?x)
          (BelowOf5 ?x)
          (BelowOf6 ?x)
        )
        (and
          (BelowOf7 ?x)
        )
      )
      (when
        (or
          (BelowOf8 ?x)
          (BelowOf3 ?x)
          (BelowOf9 ?x)
          (BelowOf7 ?x)
          (BelowOf4 ?x)
          (BelowOf1 ?x)
          (BelowOf2 ?x)
          (BelowOf5 ?x)
          (BelowOf6 ?x)
        )
        (and
          (BelowOf8 ?x)
        )
      )
      (when
        (or
          (BelowOf8 ?x)
          (BelowOf3 ?x)
          (BelowOf9 ?x)
          (BelowOf7 ?x)
          (BelowOf4 ?x)
          (BelowOf1 ?x)
          (BelowOf2 ?x)
          (BelowOf5 ?x)
          (BelowOf6 ?x)
          (BelowOf10 ?x)
        )
        (and
          (BelowOf9 ?x)
        )
      )
      (when
        (or
          (BelowOf8 ?x)
          (BelowOf3 ?x)
          (BelowOf9 ?x)
          (BelowOf7 ?x)
          (BelowOf4 ?x)
          (BelowOf1 ?x)
          (BelowOf2 ?x)
          (BelowOf5 ?x)
          (BelowOf6 ?x)
          (BelowOf11 ?x)
          (BelowOf10 ?x)
        )
        (and
          (BelowOf10 ?x)
        )
      )
      (when
        (or
          (BelowOf8 ?x)
          (BelowOf3 ?x)
          (BelowOf9 ?x)
          (BelowOf7 ?x)
          (BelowOf12 ?x)
          (BelowOf4 ?x)
          (BelowOf1 ?x)
          (BelowOf2 ?x)
          (BelowOf5 ?x)
          (BelowOf6 ?x)
          (BelowOf11 ?x)
          (BelowOf10 ?x)
        )
        (and
          (BelowOf11 ?x)
        )
      )
      (when
        (or
          (BelowOf8 ?x)
          (BelowOf13 ?x)
          (BelowOf3 ?x)
          (BelowOf9 ?x)
          (BelowOf7 ?x)
          (BelowOf12 ?x)
          (BelowOf4 ?x)
          (BelowOf1 ?x)
          (BelowOf2 ?x)
          (BelowOf5 ?x)
          (BelowOf6 ?x)
          (BelowOf11 ?x)
          (BelowOf10 ?x)
        )
        (and
          (BelowOf12 ?x)
        )
      )
      (when
        (or
          (BelowOf14 ?x)
          (BelowOf8 ?x)
          (BelowOf13 ?x)
          (BelowOf3 ?x)
          (BelowOf9 ?x)
          (BelowOf7 ?x)
          (BelowOf12 ?x)
          (BelowOf4 ?x)
          (BelowOf1 ?x)
          (BelowOf2 ?x)
          (BelowOf5 ?x)
          (BelowOf6 ?x)
          (BelowOf11 ?x)
          (BelowOf10 ?x)
        )
        (and
          (BelowOf13 ?x)
        )
      )
      (when
        (or
          (BelowOf14 ?x)
          (BelowOf8 ?x)
          (BelowOf13 ?x)
          (BelowOf3 ?x)
          (BelowOf9 ?x)
          (BelowOf7 ?x)
          (BelowOf12 ?x)
          (BelowOf4 ?x)
          (BelowOf1 ?x)
          (BelowOf2 ?x)
          (BelowOf5 ?x)
          (BelowOf6 ?x)
          (BelowOf15 ?x)
          (BelowOf11 ?x)
          (BelowOf10 ?x)
        )
        (and
          (BelowOf14 ?x)
        )
      )
      (when
        (or
          (AboveOf1 ?x)
          (AboveOf5 ?x)
          (AboveOf14 ?x)
          (AboveOf13 ?x)
          (AboveOf3 ?x)
          (AboveOf4 ?x)
          (AboveOf9 ?x)
          (AboveOf8 ?x)
          (AboveOf12 ?x)
          (AboveOf7 ?x)
          (AboveOf2 ?x)
          (AboveOf10 ?x)
          (AboveOf11 ?x)
          (AboveOf6 ?x)
        )
        (and
          (AboveOf0 ?x)
          (not (AboveOf1 ?x))
        )
      )
      (when
        (or
          (AboveOf5 ?x)
          (AboveOf14 ?x)
          (AboveOf13 ?x)
          (AboveOf3 ?x)
          (AboveOf4 ?x)
          (AboveOf9 ?x)
          (AboveOf8 ?x)
          (AboveOf12 ?x)
          (AboveOf7 ?x)
          (AboveOf2 ?x)
          (AboveOf10 ?x)
          (AboveOf6 ?x)
          (AboveOf11 ?x)
        )
        (and
          (AboveOf1 ?x)
          (not (AboveOf2 ?x))
        )
      )
      (when
        (or
          (AboveOf5 ?x)
          (AboveOf14 ?x)
          (AboveOf13 ?x)
          (AboveOf3 ?x)
          (AboveOf4 ?x)
          (AboveOf9 ?x)
          (AboveOf12 ?x)
          (AboveOf7 ?x)
          (AboveOf8 ?x)
          (AboveOf10 ?x)
          (AboveOf6 ?x)
          (AboveOf11 ?x)
        )
        (and
          (AboveOf2 ?x)
          (not (AboveOf3 ?x))
        )
      )
      (when
        (or
          (AboveOf5 ?x)
          (AboveOf14 ?x)
          (AboveOf13 ?x)
          (AboveOf4 ?x)
          (AboveOf9 ?x)
          (AboveOf12 ?x)
          (AboveOf7 ?x)
          (AboveOf8 ?x)
          (AboveOf10 ?x)
          (AboveOf6 ?x)
          (AboveOf11 ?x)
        )
        (and
          (AboveOf3 ?x)
          (not (AboveOf4 ?x))
        )
      )
      (when
        (or
          (AboveOf5 ?x)
          (AboveOf14 ?x)
          (AboveOf13 ?x)
          (AboveOf9 ?x)
          (AboveOf12 ?x)
          (AboveOf10 ?x)
          (AboveOf8 ?x)
          (AboveOf7 ?x)
          (AboveOf6 ?x)
          (AboveOf11 ?x)
        )
        (and
          (AboveOf4 ?x)
          (not (AboveOf5 ?x))
        )
      )
      (when
        (or
          (AboveOf14 ?x)
          (AboveOf13 ?x)
          (AboveOf9 ?x)
          (AboveOf12 ?x)
          (AboveOf10 ?x)
          (AboveOf8 ?x)
          (AboveOf7 ?x)
          (AboveOf6 ?x)
          (AboveOf11 ?x)
        )
        (and
          (AboveOf5 ?x)
          (not (AboveOf6 ?x))
        )
      )
      (when
        (or
          (AboveOf14 ?x)
          (AboveOf13 ?x)
          (AboveOf9 ?x)
          (AboveOf12 ?x)
          (AboveOf10 ?x)
          (AboveOf8 ?x)
          (AboveOf7 ?x)
          (AboveOf11 ?x)
        )
        (and
          (AboveOf6 ?x)
          (not (AboveOf7 ?x))
        )
      )
      (when
        (or
          (AboveOf14 ?x)
          (AboveOf13 ?x)
          (AboveOf9 ?x)
          (AboveOf12 ?x)
          (AboveOf10 ?x)
          (AboveOf8 ?x)
          (AboveOf11 ?x)
        )
        (and
          (AboveOf7 ?x)
          (not (AboveOf8 ?x))
        )
      )
      (when
        (or
          (AboveOf14 ?x)
          (AboveOf13 ?x)
          (AboveOf9 ?x)
          (AboveOf12 ?x)
          (AboveOf10 ?x)
          (AboveOf11 ?x)
        )
        (and
          (AboveOf8 ?x)
          (not (AboveOf9 ?x))
        )
      )
      (when
        (or
          (AboveOf14 ?x)
          (AboveOf11 ?x)
          (AboveOf10 ?x)
          (AboveOf13 ?x)
          (AboveOf12 ?x)
        )
        (and
          (AboveOf9 ?x)
          (not (AboveOf10 ?x))
        )
      )
      (when
        (or
          (AboveOf11 ?x)
          (AboveOf14 ?x)
          (AboveOf13 ?x)
          (AboveOf12 ?x)
        )
        (and
          (AboveOf10 ?x)
          (not (AboveOf11 ?x))
        )
      )
      (when
        (or
          (AboveOf14 ?x)
          (AboveOf13 ?x)
          (AboveOf12 ?x)
        )
        (and
          (AboveOf11 ?x)
          (not (AboveOf12 ?x))
        )
      )
      (when
        (or
          (AboveOf14 ?x)
          (AboveOf13 ?x)
        )
        (and
          (AboveOf12 ?x)
          (not (AboveOf13 ?x))
        )
      )
      (when
        (AboveOf14 ?x)
        (and
          (AboveOf13 ?x)
          (not (AboveOf14 ?x))
        )
      )
      (when
        (Row1 ?x)
        (and
          (Row0 ?x)
          (not (Row1 ?x))
        )
      )
      (when
        (Row2 ?x)
        (and
          (Row1 ?x)
          (not (Row2 ?x))
        )
      )
      (when
        (Row3 ?x)
        (and
          (Row2 ?x)
          (not (Row3 ?x))
        )
      )
      (when
        (Row4 ?x)
        (and
          (Row3 ?x)
          (not (Row4 ?x))
        )
      )
      (when
        (Row5 ?x)
        (and
          (Row4 ?x)
          (not (Row5 ?x))
        )
      )
      (when
        (Row6 ?x)
        (and
          (Row5 ?x)
          (not (Row6 ?x))
        )
      )
      (when
        (Row7 ?x)
        (and
          (Row6 ?x)
          (not (Row7 ?x))
        )
      )
      (when
        (Row8 ?x)
        (and
          (Row7 ?x)
          (not (Row8 ?x))
        )
      )
      (when
        (Row9 ?x)
        (and
          (Row8 ?x)
          (not (Row9 ?x))
        )
      )
      (when
        (Row10 ?x)
        (and
          (Row9 ?x)
          (not (Row10 ?x))
        )
      )
      (when
        (Row11 ?x)
        (and
          (Row10 ?x)
          (not (Row11 ?x))
        )
      )
      (when
        (Row12 ?x)
        (and
          (Row11 ?x)
          (not (Row12 ?x))
        )
      )
      (when
        (Row13 ?x)
        (and
          (Row12 ?x)
          (not (Row13 ?x))
        )
      )
      (when
        (Row14 ?x)
        (and
          (Row13 ?x)
          (not (Row14 ?x))
        )
      )
      (when
        (or
          (and
            (BelowOf2 ?x)
            (AboveOf3 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf8 ?x)
          )
          (and
            (AboveOf5 ?x)
            (BelowOf2 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf12 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf12 ?x)
          )
          (and
            (AboveOf4 ?x)
            (BelowOf1 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf8 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf10 ?x)
          )
          (and
            (AboveOf9 ?x)
            (BelowOf1 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf3 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf11 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf10 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf1 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf6 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf1 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf9 ?x)
          )
          (and
            (AboveOf2 ?x)
            (BelowOf1 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf6 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf2 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf4 ?x)
          )
          (and
            (AboveOf5 ?x)
            (BelowOf1 ?x)
          )
          (and
            (AboveOf1 ?x)
            (BelowOf1 ?x)
          )
          (and
            (AboveOf7 ?x)
            (BelowOf1 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf2 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf7 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf1 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf11 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf2 ?x)
          )
        )
        (and
          (Row0 ?x)
        )
      )
      (when
        (or
          (and
            (BelowOf2 ?x)
            (AboveOf3 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf8 ?x)
          )
          (and
            (AboveOf5 ?x)
            (BelowOf2 ?x)
          )
          (and
            (BelowOf3 ?x)
            (AboveOf11 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf3 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf12 ?x)
          )
          (and
            (BelowOf3 ?x)
            (AboveOf12 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf12 ?x)
          )
          (and
            (AboveOf4 ?x)
            (BelowOf1 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf8 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf10 ?x)
          )
          (and
            (AboveOf9 ?x)
            (BelowOf1 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf3 ?x)
          )
          (and
            (AboveOf2 ?x)
            (BelowOf2 ?x)
          )
          (and
            (AboveOf10 ?x)
            (BelowOf3 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf1 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf10 ?x)
          )
          (and
            (AboveOf4 ?x)
            (BelowOf3 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf6 ?x)
          )
          (and
            (BelowOf3 ?x)
            (AboveOf8 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf1 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf9 ?x)
          )
          (and
            (BelowOf3 ?x)
            (AboveOf3 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf6 ?x)
          )
          (and
            (AboveOf2 ?x)
            (BelowOf3 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf2 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf4 ?x)
          )
          (and
            (AboveOf9 ?x)
            (BelowOf3 ?x)
          )
          (and
            (AboveOf5 ?x)
            (BelowOf1 ?x)
          )
          (and
            (AboveOf7 ?x)
            (BelowOf1 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf2 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf3 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf7 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf11 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf11 ?x)
          )
          (and
            (AboveOf7 ?x)
            (BelowOf3 ?x)
          )
          (and
            (AboveOf2 ?x)
            (BelowOf1 ?x)
          )
          (and
            (AboveOf6 ?x)
            (BelowOf3 ?x)
          )
          (and
            (AboveOf5 ?x)
            (BelowOf3 ?x)
          )
        )
        (and
          (Row1 ?x)
        )
      )
      (when
        (or
          (and
            (BelowOf2 ?x)
            (AboveOf3 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf8 ?x)
          )
          (and
            (AboveOf5 ?x)
            (BelowOf2 ?x)
          )
          (and
            (BelowOf3 ?x)
            (AboveOf11 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf12 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf2 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf3 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf11 ?x)
          )
          (and
            (BelowOf3 ?x)
            (AboveOf12 ?x)
          )
          (and
            (AboveOf11 ?x)
            (BelowOf3 ?x)
          )
          (and
            (AboveOf4 ?x)
            (BelowOf1 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf8 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf10 ?x)
          )
          (and
            (AboveOf9 ?x)
            (BelowOf1 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf3 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf10 ?x)
          )
          (and
            (AboveOf10 ?x)
            (BelowOf3 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf1 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf10 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf12 ?x)
          )
          (and
            (AboveOf4 ?x)
            (BelowOf3 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf6 ?x)
          )
          (and
            (BelowOf3 ?x)
            (AboveOf8 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf1 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf9 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf6 ?x)
          )
          (and
            (AboveOf3 ?x)
            (BelowOf3 ?x)
          )
          (and
            (AboveOf5 ?x)
            (BelowOf4 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf12 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf4 ?x)
          )
          (and
            (AboveOf7 ?x)
            (BelowOf4 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf8 ?x)
          )
          (and
            (AboveOf4 ?x)
            (BelowOf4 ?x)
          )
          (and
            (AboveOf5 ?x)
            (BelowOf1 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf9 ?x)
          )
          (and
            (AboveOf7 ?x)
            (BelowOf1 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf2 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf3 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf4 ?x)
          )
          (and
            (AboveOf9 ?x)
            (BelowOf3 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf7 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf11 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf11 ?x)
          )
          (and
            (AboveOf7 ?x)
            (BelowOf3 ?x)
          )
          (and
            (AboveOf6 ?x)
            (BelowOf3 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf6 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf3 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf4 ?x)
          )
          (and
            (AboveOf5 ?x)
            (BelowOf3 ?x)
          )
        )
        (and
          (Row2 ?x)
        )
      )
      (when
        (or
          (and
            (BelowOf5 ?x)
            (AboveOf14 ?x)
          )
          (and
            (AboveOf10 ?x)
            (BelowOf3 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf8 ?x)
          )
          (and
            (AboveOf5 ?x)
            (BelowOf2 ?x)
          )
          (and
            (BelowOf3 ?x)
            (AboveOf11 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf12 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf2 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf3 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf12 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf11 ?x)
          )
          (and
            (BelowOf3 ?x)
            (AboveOf12 ?x)
          )
          (and
            (AboveOf11 ?x)
            (BelowOf3 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf9 ?x)
          )
          (and
            (AboveOf4 ?x)
            (BelowOf1 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf8 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf10 ?x)
          )
          (and
            (AboveOf9 ?x)
            (BelowOf1 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf6 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf10 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf11 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf1 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf10 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf12 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf7 ?x)
          )
          (and
            (AboveOf4 ?x)
            (BelowOf3 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf5 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf6 ?x)
          )
          (and
            (BelowOf3 ?x)
            (AboveOf8 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf1 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf9 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf6 ?x)
          )
          (and
            (AboveOf5 ?x)
            (BelowOf4 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf12 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf4 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf11 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf10 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf13 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf8 ?x)
          )
          (and
            (AboveOf5 ?x)
            (BelowOf1 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf9 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf6 ?x)
          )
          (and
            (AboveOf7 ?x)
            (BelowOf1 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf2 ?x)
          )
          (and
            (AboveOf4 ?x)
            (BelowOf4 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf4 ?x)
          )
          (and
            (AboveOf9 ?x)
            (BelowOf3 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf7 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf11 ?x)
          )
          (and
            (AboveOf7 ?x)
            (BelowOf3 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf8 ?x)
          )
          (and
            (AboveOf6 ?x)
            (BelowOf3 ?x)
          )
          (and
            (AboveOf7 ?x)
            (BelowOf4 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf4 ?x)
          )
          (and
            (AboveOf5 ?x)
            (BelowOf3 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf4 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf3 ?x)
          )
        )
        (and
          (Row3 ?x)
        )
      )
      (when
        (or
          (and
            (BelowOf5 ?x)
            (AboveOf14 ?x)
          )
          (and
            (AboveOf10 ?x)
            (BelowOf3 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf8 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf6 ?x)
          )
          (and
            (BelowOf3 ?x)
            (AboveOf11 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf11 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf12 ?x)
          )
          (and
            (AboveOf10 ?x)
            (BelowOf6 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf2 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf3 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf4 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf11 ?x)
          )
          (and
            (BelowOf3 ?x)
            (AboveOf12 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf12 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf9 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf8 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf10 ?x)
          )
          (and
            (AboveOf9 ?x)
            (BelowOf6 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf6 ?x)
          )
          (and
            (BelowOf6 ?x)
            (AboveOf8 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf10 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf11 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf10 ?x)
          )
          (and
            (AboveOf11 ?x)
            (BelowOf3 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf7 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf1 ?x)
          )
          (and
            (AboveOf6 ?x)
            (BelowOf6 ?x)
          )
          (and
            (BelowOf3 ?x)
            (AboveOf8 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf1 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf6 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf6 ?x)
          )
          (and
            (AboveOf5 ?x)
            (BelowOf4 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf12 ?x)
          )
          (and
            (AboveOf7 ?x)
            (BelowOf4 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf10 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf13 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf8 ?x)
          )
          (and
            (AboveOf5 ?x)
            (BelowOf5 ?x)
          )
          (and
            (AboveOf7 ?x)
            (BelowOf6 ?x)
          )
          (and
            (AboveOf5 ?x)
            (BelowOf1 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf9 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf12 ?x)
          )
          (and
            (AboveOf7 ?x)
            (BelowOf1 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf9 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf2 ?x)
          )
          (and
            (AboveOf9 ?x)
            (BelowOf1 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf8 ?x)
          )
          (and
            (AboveOf5 ?x)
            (BelowOf6 ?x)
          )
          (and
            (AboveOf9 ?x)
            (BelowOf3 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf7 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf6 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf11 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf5 ?x)
          )
          (and
            (BelowOf6 ?x)
            (AboveOf12 ?x)
          )
          (and
            (AboveOf7 ?x)
            (BelowOf3 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf6 ?x)
          )
          (and
            (AboveOf6 ?x)
            (BelowOf3 ?x)
          )
          (and
            (BelowOf6 ?x)
            (AboveOf11 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf4 ?x)
          )
          (and
            (AboveOf5 ?x)
            (BelowOf2 ?x)
          )
          (and
            (AboveOf5 ?x)
            (BelowOf3 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf3 ?x)
          )
        )
        (and
          (Row4 ?x)
        )
      )
      (when
        (or
          (and
            (BelowOf5 ?x)
            (AboveOf14 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf11 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf8 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf6 ?x)
          )
          (and
            (BelowOf3 ?x)
            (AboveOf11 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf11 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf11 ?x)
          )
          (and
            (BelowOf7 ?x)
            (AboveOf11 ?x)
          )
          (and
            (BelowOf7 ?x)
            (AboveOf8 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf3 ?x)
          )
          (and
            (BelowOf7 ?x)
            (AboveOf7 ?x)
          )
          (and
            (BelowOf3 ?x)
            (AboveOf12 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf7 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf7 ?x)
          )
          (and
            (AboveOf11 ?x)
            (BelowOf3 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf9 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf8 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf10 ?x)
          )
          (and
            (AboveOf9 ?x)
            (BelowOf6 ?x)
          )
          (and
            (BelowOf7 ?x)
            (AboveOf12 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf6 ?x)
          )
          (and
            (BelowOf6 ?x)
            (AboveOf8 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf10 ?x)
          )
          (and
            (AboveOf10 ?x)
            (BelowOf3 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf10 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf12 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf7 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf1 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf6 ?x)
          )
          (and
            (BelowOf3 ?x)
            (AboveOf8 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf1 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf9 ?x)
          )
          (and
            (AboveOf6 ?x)
            (BelowOf3 ?x)
          )
          (and
            (BelowOf6 ?x)
            (AboveOf10 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf2 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf12 ?x)
          )
          (and
            (AboveOf7 ?x)
            (BelowOf4 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf10 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf13 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf8 ?x)
          )
          (and
            (AboveOf7 ?x)
            (BelowOf6 ?x)
          )
          (and
            (BelowOf7 ?x)
            (AboveOf10 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf12 ?x)
          )
          (and
            (AboveOf7 ?x)
            (BelowOf1 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf2 ?x)
          )
          (and
            (AboveOf9 ?x)
            (BelowOf1 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf8 ?x)
          )
          (and
            (AboveOf9 ?x)
            (BelowOf3 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf7 ?x)
          )
          (and
            (BelowOf6 ?x)
            (AboveOf6 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf6 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf11 ?x)
          )
          (and
            (BelowOf7 ?x)
            (AboveOf9 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf5 ?x)
          )
          (and
            (BelowOf6 ?x)
            (AboveOf12 ?x)
          )
          (and
            (AboveOf7 ?x)
            (BelowOf3 ?x)
          )
          (and
            (AboveOf10 ?x)
            (BelowOf6 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf6 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf6 ?x)
          )
          (and
            (BelowOf6 ?x)
            (AboveOf11 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf4 ?x)
          )
          (and
            (BelowOf7 ?x)
            (AboveOf6 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf9 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf4 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf3 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf12 ?x)
          )
        )
        (and
          (Row5 ?x)
        )
      )
      (when
        (or
          (and
            (BelowOf5 ?x)
            (AboveOf14 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf8 ?x)
          )
          (and
            (AboveOf11 ?x)
            (BelowOf8 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf8 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf12 ?x)
          )
          (and
            (BelowOf3 ?x)
            (AboveOf11 ?x)
          )
          (and
            (AboveOf7 ?x)
            (BelowOf8 ?x)
          )
          (and
            (BelowOf7 ?x)
            (AboveOf11 ?x)
          )
          (and
            (BelowOf7 ?x)
            (AboveOf8 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf3 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf6 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf11 ?x)
          )
          (and
            (BelowOf3 ?x)
            (AboveOf12 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf7 ?x)
          )
          (and
            (AboveOf11 ?x)
            (BelowOf3 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf9 ?x)
          )
          (and
            (AboveOf7 ?x)
            (BelowOf7 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf8 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf10 ?x)
          )
          (and
            (AboveOf9 ?x)
            (BelowOf6 ?x)
          )
          (and
            (BelowOf7 ?x)
            (AboveOf12 ?x)
          )
          (and
            (BelowOf6 ?x)
            (AboveOf8 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf10 ?x)
          )
          (and
            (AboveOf10 ?x)
            (BelowOf3 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf10 ?x)
          )
          (and
            (AboveOf9 ?x)
            (BelowOf8 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf7 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf1 ?x)
          )
          (and
            (BelowOf3 ?x)
            (AboveOf8 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf1 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf9 ?x)
          )
          (and
            (BelowOf8 ?x)
            (AboveOf8 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf11 ?x)
          )
          (and
            (BelowOf6 ?x)
            (AboveOf10 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf2 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf12 ?x)
          )
          (and
            (AboveOf7 ?x)
            (BelowOf4 ?x)
          )
          (and
            (BelowOf8 ?x)
            (AboveOf12 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf10 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf13 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf8 ?x)
          )
          (and
            (AboveOf10 ?x)
            (BelowOf8 ?x)
          )
          (and
            (AboveOf7 ?x)
            (BelowOf6 ?x)
          )
          (and
            (BelowOf7 ?x)
            (AboveOf10 ?x)
          )
          (and
            (AboveOf7 ?x)
            (BelowOf1 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf2 ?x)
          )
          (and
            (AboveOf9 ?x)
            (BelowOf1 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf8 ?x)
          )
          (and
            (AboveOf9 ?x)
            (BelowOf3 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf7 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf11 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf6 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf11 ?x)
          )
          (and
            (BelowOf7 ?x)
            (AboveOf9 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf5 ?x)
          )
          (and
            (BelowOf6 ?x)
            (AboveOf12 ?x)
          )
          (and
            (AboveOf7 ?x)
            (BelowOf3 ?x)
          )
          (and
            (AboveOf10 ?x)
            (BelowOf6 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf8 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf7 ?x)
          )
          (and
            (BelowOf6 ?x)
            (AboveOf11 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf4 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf12 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf9 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf4 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf3 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf12 ?x)
          )
        )
        (and
          (Row6 ?x)
        )
      )
      (when
        (or
          (and
            (BelowOf5 ?x)
            (AboveOf14 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf8 ?x)
          )
          (and
            (AboveOf11 ?x)
            (BelowOf8 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf8 ?x)
          )
          (and
            (AboveOf9 ?x)
            (BelowOf9 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf12 ?x)
          )
          (and
            (BelowOf3 ?x)
            (AboveOf11 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf12 ?x)
          )
          (and
            (AboveOf10 ?x)
            (BelowOf6 ?x)
          )
          (and
            (BelowOf9 ?x)
            (AboveOf11 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf3 ?x)
          )
          (and
            (BelowOf9 ?x)
            (AboveOf12 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf11 ?x)
          )
          (and
            (BelowOf3 ?x)
            (AboveOf12 ?x)
          )
          (and
            (BelowOf9 ?x)
            (AboveOf8 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf7 ?x)
          )
          (and
            (AboveOf11 ?x)
            (BelowOf3 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf9 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf9 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf8 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf10 ?x)
          )
          (and
            (AboveOf9 ?x)
            (BelowOf6 ?x)
          )
          (and
            (BelowOf7 ?x)
            (AboveOf12 ?x)
          )
          (and
            (BelowOf6 ?x)
            (AboveOf8 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf10 ?x)
          )
          (and
            (AboveOf10 ?x)
            (BelowOf3 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf10 ?x)
          )
          (and
            (AboveOf9 ?x)
            (BelowOf8 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf1 ?x)
          )
          (and
            (BelowOf3 ?x)
            (AboveOf8 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf1 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf9 ?x)
          )
          (and
            (BelowOf6 ?x)
            (AboveOf10 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf2 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf12 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf11 ?x)
          )
          (and
            (BelowOf8 ?x)
            (AboveOf12 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf10 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf13 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf8 ?x)
          )
          (and
            (AboveOf10 ?x)
            (BelowOf8 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf9 ?x)
          )
          (and
            (BelowOf7 ?x)
            (AboveOf10 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf2 ?x)
          )
          (and
            (AboveOf9 ?x)
            (BelowOf1 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf8 ?x)
          )
          (and
            (AboveOf8 ?x)
            (BelowOf8 ?x)
          )
          (and
            (AboveOf9 ?x)
            (BelowOf3 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf11 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf6 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf11 ?x)
          )
          (and
            (BelowOf7 ?x)
            (AboveOf9 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf5 ?x)
          )
          (and
            (BelowOf6 ?x)
            (AboveOf12 ?x)
          )
          (and
            (BelowOf9 ?x)
            (AboveOf10 ?x)
          )
          (and
            (BelowOf7 ?x)
            (AboveOf11 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf8 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf7 ?x)
          )
          (and
            (BelowOf6 ?x)
            (AboveOf11 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf6 ?x)
          )
          (and
            (BelowOf7 ?x)
            (AboveOf8 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf4 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf12 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf9 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf4 ?x)
          )
          (and
            (AboveOf12 ?x)
            (BelowOf8 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf3 ?x)
          )
        )
        (and
          (Row7 ?x)
        )
      )
      (when
        (or
          (and
            (BelowOf5 ?x)
            (AboveOf14 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf8 ?x)
          )
          (and
            (AboveOf11 ?x)
            (BelowOf8 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf1 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf10 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf12 ?x)
          )
          (and
            (BelowOf3 ?x)
            (AboveOf11 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf12 ?x)
          )
          (and
            (BelowOf7 ?x)
            (AboveOf11 ?x)
          )
          (and
            (BelowOf9 ?x)
            (AboveOf11 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf3 ?x)
          )
          (and
            (BelowOf9 ?x)
            (AboveOf12 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf11 ?x)
          )
          (and
            (BelowOf3 ?x)
            (AboveOf12 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf7 ?x)
          )
          (and
            (AboveOf11 ?x)
            (BelowOf3 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf9 ?x)
          )
          (and
            (BelowOf10 ?x)
            (AboveOf11 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf9 ?x)
          )
          (and
            (BelowOf9 ?x)
            (AboveOf9 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf10 ?x)
          )
          (and
            (AboveOf9 ?x)
            (BelowOf6 ?x)
          )
          (and
            (BelowOf7 ?x)
            (AboveOf12 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf10 ?x)
          )
          (and
            (AboveOf10 ?x)
            (BelowOf3 ?x)
          )
          (and
            (BelowOf10 ?x)
            (AboveOf12 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf10 ?x)
          )
          (and
            (AboveOf9 ?x)
            (BelowOf8 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf1 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf9 ?x)
          )
          (and
            (AboveOf10 ?x)
            (BelowOf10 ?x)
          )
          (and
            (BelowOf6 ?x)
            (AboveOf10 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf2 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf12 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf11 ?x)
          )
          (and
            (BelowOf8 ?x)
            (AboveOf12 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf10 ?x)
          )
          (and
            (AboveOf9 ?x)
            (BelowOf3 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf13 ?x)
          )
          (and
            (AboveOf10 ?x)
            (BelowOf8 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf9 ?x)
          )
          (and
            (BelowOf7 ?x)
            (AboveOf10 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf11 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf2 ?x)
          )
          (and
            (AboveOf9 ?x)
            (BelowOf1 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf10 ?x)
          )
          (and
            (AboveOf9 ?x)
            (BelowOf10 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf6 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf11 ?x)
          )
          (and
            (BelowOf7 ?x)
            (AboveOf9 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf5 ?x)
          )
          (and
            (BelowOf6 ?x)
            (AboveOf12 ?x)
          )
          (and
            (BelowOf9 ?x)
            (AboveOf10 ?x)
          )
          (and
            (AboveOf10 ?x)
            (BelowOf6 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf8 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf7 ?x)
          )
          (and
            (BelowOf6 ?x)
            (AboveOf11 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf6 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf4 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf12 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf9 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf4 ?x)
          )
          (and
            (AboveOf12 ?x)
            (BelowOf8 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf3 ?x)
          )
        )
        (and
          (Row8 ?x)
        )
      )
      (when
        (or
          (and
            (BelowOf5 ?x)
            (AboveOf14 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf8 ?x)
          )
          (and
            (AboveOf11 ?x)
            (BelowOf8 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf1 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf10 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf12 ?x)
          )
          (and
            (BelowOf3 ?x)
            (AboveOf11 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf12 ?x)
          )
          (and
            (BelowOf7 ?x)
            (AboveOf11 ?x)
          )
          (and
            (BelowOf9 ?x)
            (AboveOf11 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf3 ?x)
          )
          (and
            (BelowOf9 ?x)
            (AboveOf12 ?x)
          )
          (and
            (BelowOf11 ?x)
            (AboveOf12 ?x)
          )
          (and
            (BelowOf3 ?x)
            (AboveOf12 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf11 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf11 ?x)
          )
          (and
            (AboveOf11 ?x)
            (BelowOf3 ?x)
          )
          (and
            (BelowOf10 ?x)
            (AboveOf11 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf9 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf10 ?x)
          )
          (and
            (BelowOf7 ?x)
            (AboveOf12 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf10 ?x)
          )
          (and
            (AboveOf10 ?x)
            (BelowOf3 ?x)
          )
          (and
            (BelowOf10 ?x)
            (AboveOf12 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf10 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf12 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf1 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf12 ?x)
          )
          (and
            (AboveOf10 ?x)
            (BelowOf10 ?x)
          )
          (and
            (BelowOf6 ?x)
            (AboveOf10 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf2 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf7 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf11 ?x)
          )
          (and
            (BelowOf8 ?x)
            (AboveOf12 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf10 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf13 ?x)
          )
          (and
            (BelowOf6 ?x)
            (AboveOf12 ?x)
          )
          (and
            (AboveOf10 ?x)
            (BelowOf8 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf9 ?x)
          )
          (and
            (BelowOf7 ?x)
            (AboveOf10 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf11 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf2 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf10 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf11 ?x)
          )
          (and
            (BelowOf11 ?x)
            (AboveOf11 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf6 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf11 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf5 ?x)
          )
          (and
            (AboveOf10 ?x)
            (BelowOf11 ?x)
          )
          (and
            (BelowOf9 ?x)
            (AboveOf10 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf8 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf7 ?x)
          )
          (and
            (BelowOf6 ?x)
            (AboveOf11 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf6 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf4 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf4 ?x)
          )
          (and
            (AboveOf12 ?x)
            (BelowOf8 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf3 ?x)
          )
        )
        (and
          (Row9 ?x)
        )
      )
      (when
        (or
          (and
            (BelowOf5 ?x)
            (AboveOf14 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf8 ?x)
          )
          (and
            (AboveOf11 ?x)
            (BelowOf8 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf1 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf10 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf12 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf3 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf12 ?x)
          )
          (and
            (BelowOf7 ?x)
            (AboveOf11 ?x)
          )
          (and
            (BelowOf9 ?x)
            (AboveOf11 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf3 ?x)
          )
          (and
            (BelowOf9 ?x)
            (AboveOf12 ?x)
          )
          (and
            (BelowOf11 ?x)
            (AboveOf12 ?x)
          )
          (and
            (BelowOf3 ?x)
            (AboveOf12 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf11 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf11 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf7 ?x)
          )
          (and
            (AboveOf11 ?x)
            (BelowOf3 ?x)
          )
          (and
            (BelowOf10 ?x)
            (AboveOf11 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf9 ?x)
          )
          (and
            (BelowOf7 ?x)
            (AboveOf12 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf11 ?x)
          )
          (and
            (BelowOf10 ?x)
            (AboveOf12 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf12 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf1 ?x)
          )
          (and
            (AboveOf11 ?x)
            (BelowOf12 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf12 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf12 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf11 ?x)
          )
          (and
            (BelowOf8 ?x)
            (AboveOf12 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf13 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf9 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf12 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf2 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf11 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf2 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf10 ?x)
          )
          (and
            (BelowOf12 ?x)
            (AboveOf12 ?x)
          )
          (and
            (BelowOf11 ?x)
            (AboveOf11 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf6 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf11 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf5 ?x)
          )
          (and
            (BelowOf6 ?x)
            (AboveOf12 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf8 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf7 ?x)
          )
          (and
            (BelowOf6 ?x)
            (AboveOf11 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf6 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf4 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf4 ?x)
          )
          (and
            (AboveOf12 ?x)
            (BelowOf8 ?x)
          )
        )
        (and
          (Row10 ?x)
        )
      )
      (when
        (or
          (and
            (BelowOf5 ?x)
            (AboveOf14 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf8 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf1 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf10 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf12 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf3 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf2 ?x)
          )
          (and
            (AboveOf12 ?x)
            (BelowOf12 ?x)
          )
          (and
            (BelowOf9 ?x)
            (AboveOf12 ?x)
          )
          (and
            (BelowOf11 ?x)
            (AboveOf12 ?x)
          )
          (and
            (BelowOf3 ?x)
            (AboveOf12 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf12 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf11 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf7 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf12 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf9 ?x)
          )
          (and
            (BelowOf7 ?x)
            (AboveOf12 ?x)
          )
          (and
            (BelowOf10 ?x)
            (AboveOf12 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf1 ?x)
          )
          (and
            (BelowOf13 ?x)
            (AboveOf12 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf12 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf12 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf13 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf9 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf12 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf11 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf2 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf10 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf4 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf6 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf5 ?x)
          )
          (and
            (BelowOf6 ?x)
            (AboveOf12 ?x)
          )
          (and
            (BelowOf13 ?x)
            (AboveOf13 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf8 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf7 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf6 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf4 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf3 ?x)
          )
          (and
            (BelowOf13 ?x)
            (AboveOf14 ?x)
          )
          (and
            (AboveOf12 ?x)
            (BelowOf8 ?x)
          )
        )
        (and
          (Row11 ?x)
        )
      )
      (when
        (or
          (and
            (BelowOf5 ?x)
            (AboveOf14 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf8 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf1 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf10 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf12 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf3 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf2 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf3 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf8 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf7 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf9 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf14 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf1 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf12 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf11 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf13 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf9 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf11 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf2 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf10 ?x)
          )
          (and
            (BelowOf13 ?x)
            (AboveOf14 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf6 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf14 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf5 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf6 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf7 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf4 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf4 ?x)
          )
        )
        (and
          (Row12 ?x)
        )
      )
      (when
        (or
          (and
            (AboveOf14 ?x)
            (BelowOf8 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf2 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf14 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf11 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf15 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf12 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf3 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf1 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf9 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf6 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf14 ?x)
          )
          (and
            (BelowOf13 ?x)
            (AboveOf14 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf10 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf7 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf4 ?x)
          )
        )
        (and
          (Row13 ?x)
        )
      )
    )
  )
  (:action moveRight
    :parameters ( ?x )
    :precondition ( and
      (not (CheckConsistency))
      (not (Error))
      (or
        (LeftOf3 ?x)
        (RightOf7 ?x)
        (RightOf8 ?x)
        (LeftOf2 ?x)
        (LeftOf9 ?x)
        (RightOf2 ?x)
        (LeftOf1 ?x)
        (LeftOf8 ?x)
        (LeftOf13 ?x)
        (LeftOf14 ?x)
        (LeftOf5 ?x)
        (RightOf12 ?x)
        (RightOf1 ?x)
        (LeftOf12 ?x)
        (Columns ?x)
        (RightOf13 ?x)
        (RightOf14 ?x)
        (RightOf6 ?x)
        (RightOf9 ?x)
        (LeftOf6 ?x)
        (RightOf0 ?x)
        (RightOf11 ?x)
        (LeftOf11 ?x)
        (RightOf5 ?x)
        (LeftOf10 ?x)
        (LeftOf15 ?x)
        (LeftOf4 ?x)
        (RightOf4 ?x)
        (RightOf10 ?x)
        (LeftOf7 ?x)
        (RightOf3 ?x)
      )
    )
    :effect ( and
      (CheckConsistency)
      (when
        (or
          (RightOf6 ?x)
          (RightOf9 ?x)
          (RightOf7 ?x)
          (RightOf12 ?x)
          (RightOf1 ?x)
          (RightOf4 ?x)
          (RightOf0 ?x)
          (RightOf11 ?x)
          (RightOf13 ?x)
          (RightOf14 ?x)
          (RightOf2 ?x)
          (RightOf5 ?x)
          (RightOf10 ?x)
          (RightOf8 ?x)
          (RightOf3 ?x)
        )
        (and
          (RightOf1 ?x)
        )
      )
      (when
        (or
          (RightOf6 ?x)
          (RightOf9 ?x)
          (RightOf7 ?x)
          (RightOf12 ?x)
          (RightOf1 ?x)
          (RightOf4 ?x)
          (RightOf10 ?x)
          (RightOf11 ?x)
          (RightOf13 ?x)
          (RightOf14 ?x)
          (RightOf2 ?x)
          (RightOf5 ?x)
          (RightOf8 ?x)
          (RightOf3 ?x)
        )
        (and
          (RightOf2 ?x)
        )
      )
      (when
        (or
          (RightOf6 ?x)
          (RightOf9 ?x)
          (RightOf7 ?x)
          (RightOf12 ?x)
          (RightOf8 ?x)
          (RightOf4 ?x)
          (RightOf10 ?x)
          (RightOf11 ?x)
          (RightOf13 ?x)
          (RightOf14 ?x)
          (RightOf2 ?x)
          (RightOf5 ?x)
          (RightOf3 ?x)
        )
        (and
          (RightOf3 ?x)
        )
      )
      (when
        (or
          (RightOf6 ?x)
          (RightOf9 ?x)
          (RightOf7 ?x)
          (RightOf12 ?x)
          (RightOf8 ?x)
          (RightOf4 ?x)
          (RightOf10 ?x)
          (RightOf11 ?x)
          (RightOf13 ?x)
          (RightOf14 ?x)
          (RightOf5 ?x)
          (RightOf3 ?x)
        )
        (and
          (RightOf4 ?x)
        )
      )
      (when
        (or
          (RightOf6 ?x)
          (RightOf9 ?x)
          (RightOf7 ?x)
          (RightOf12 ?x)
          (RightOf8 ?x)
          (RightOf4 ?x)
          (RightOf10 ?x)
          (RightOf11 ?x)
          (RightOf13 ?x)
          (RightOf14 ?x)
          (RightOf5 ?x)
        )
        (and
          (RightOf5 ?x)
        )
      )
      (when
        (or
          (RightOf6 ?x)
          (RightOf9 ?x)
          (RightOf7 ?x)
          (RightOf12 ?x)
          (RightOf8 ?x)
          (RightOf10 ?x)
          (RightOf11 ?x)
          (RightOf13 ?x)
          (RightOf14 ?x)
          (RightOf5 ?x)
        )
        (and
          (RightOf6 ?x)
        )
      )
      (when
        (or
          (RightOf6 ?x)
          (RightOf9 ?x)
          (RightOf7 ?x)
          (RightOf12 ?x)
          (RightOf8 ?x)
          (RightOf10 ?x)
          (RightOf11 ?x)
          (RightOf13 ?x)
          (RightOf14 ?x)
        )
        (and
          (RightOf7 ?x)
        )
      )
      (when
        (or
          (RightOf12 ?x)
          (RightOf9 ?x)
          (RightOf7 ?x)
          (RightOf8 ?x)
          (RightOf10 ?x)
          (RightOf11 ?x)
          (RightOf13 ?x)
          (RightOf14 ?x)
        )
        (and
          (RightOf8 ?x)
        )
      )
      (when
        (or
          (RightOf12 ?x)
          (RightOf9 ?x)
          (RightOf8 ?x)
          (RightOf10 ?x)
          (RightOf11 ?x)
          (RightOf13 ?x)
          (RightOf14 ?x)
        )
        (and
          (RightOf9 ?x)
        )
      )
      (when
        (or
          (RightOf12 ?x)
          (RightOf9 ?x)
          (RightOf10 ?x)
          (RightOf11 ?x)
          (RightOf13 ?x)
          (RightOf14 ?x)
        )
        (and
          (RightOf10 ?x)
        )
      )
      (when
        (or
          (RightOf11 ?x)
          (RightOf12 ?x)
          (RightOf13 ?x)
          (RightOf14 ?x)
          (RightOf10 ?x)
        )
        (and
          (RightOf11 ?x)
        )
      )
      (when
        (or
          (RightOf11 ?x)
          (RightOf12 ?x)
          (RightOf13 ?x)
          (RightOf14 ?x)
        )
        (and
          (RightOf12 ?x)
        )
      )
      (when
        (or
          (RightOf12 ?x)
          (RightOf13 ?x)
          (RightOf14 ?x)
        )
        (and
          (RightOf13 ?x)
        )
      )
      (when
        (or
          (RightOf13 ?x)
          (RightOf14 ?x)
        )
        (and
          (RightOf14 ?x)
        )
      )
      (when
        (LeftOf1 ?x)
        (and
          (LeftOf2 ?x)
          (not (LeftOf1 ?x))
        )
      )
      (when
        (or
          (LeftOf2 ?x)
          (LeftOf1 ?x)
        )
        (and
          (LeftOf3 ?x)
          (not (LeftOf2 ?x))
        )
      )
      (when
        (or
          (LeftOf2 ?x)
          (LeftOf3 ?x)
          (LeftOf1 ?x)
        )
        (and
          (LeftOf4 ?x)
          (not (LeftOf3 ?x))
        )
      )
      (when
        (or
          (LeftOf2 ?x)
          (LeftOf4 ?x)
          (LeftOf3 ?x)
          (LeftOf1 ?x)
        )
        (and
          (LeftOf5 ?x)
          (not (LeftOf4 ?x))
        )
      )
      (when
        (or
          (LeftOf2 ?x)
          (LeftOf4 ?x)
          (LeftOf3 ?x)
          (LeftOf1 ?x)
          (LeftOf5 ?x)
        )
        (and
          (LeftOf6 ?x)
          (not (LeftOf5 ?x))
        )
      )
      (when
        (or
          (LeftOf4 ?x)
          (LeftOf3 ?x)
          (LeftOf5 ?x)
          (LeftOf6 ?x)
          (LeftOf1 ?x)
          (LeftOf2 ?x)
        )
        (and
          (LeftOf7 ?x)
          (not (LeftOf6 ?x))
        )
      )
      (when
        (or
          (LeftOf4 ?x)
          (LeftOf3 ?x)
          (LeftOf5 ?x)
          (LeftOf6 ?x)
          (LeftOf1 ?x)
          (LeftOf7 ?x)
          (LeftOf2 ?x)
        )
        (and
          (LeftOf8 ?x)
          (not (LeftOf7 ?x))
        )
      )
      (when
        (or
          (LeftOf8 ?x)
          (LeftOf4 ?x)
          (LeftOf3 ?x)
          (LeftOf5 ?x)
          (LeftOf6 ?x)
          (LeftOf1 ?x)
          (LeftOf7 ?x)
          (LeftOf2 ?x)
        )
        (and
          (LeftOf9 ?x)
          (not (LeftOf8 ?x))
        )
      )
      (when
        (or
          (LeftOf8 ?x)
          (LeftOf4 ?x)
          (LeftOf3 ?x)
          (LeftOf5 ?x)
          (LeftOf6 ?x)
          (LeftOf1 ?x)
          (LeftOf7 ?x)
          (LeftOf2 ?x)
          (LeftOf9 ?x)
        )
        (and
          (LeftOf10 ?x)
          (not (LeftOf9 ?x))
        )
      )
      (when
        (or
          (LeftOf8 ?x)
          (LeftOf4 ?x)
          (LeftOf3 ?x)
          (LeftOf5 ?x)
          (LeftOf6 ?x)
          (LeftOf1 ?x)
          (LeftOf7 ?x)
          (LeftOf2 ?x)
          (LeftOf9 ?x)
          (LeftOf10 ?x)
        )
        (and
          (LeftOf11 ?x)
          (not (LeftOf10 ?x))
        )
      )
      (when
        (or
          (LeftOf8 ?x)
          (LeftOf4 ?x)
          (LeftOf3 ?x)
          (LeftOf5 ?x)
          (LeftOf6 ?x)
          (LeftOf1 ?x)
          (LeftOf7 ?x)
          (LeftOf2 ?x)
          (LeftOf9 ?x)
          (LeftOf11 ?x)
          (LeftOf10 ?x)
        )
        (and
          (LeftOf12 ?x)
          (not (LeftOf11 ?x))
        )
      )
      (when
        (or
          (LeftOf8 ?x)
          (LeftOf4 ?x)
          (LeftOf3 ?x)
          (LeftOf5 ?x)
          (LeftOf6 ?x)
          (LeftOf12 ?x)
          (LeftOf1 ?x)
          (LeftOf7 ?x)
          (LeftOf2 ?x)
          (LeftOf9 ?x)
          (LeftOf11 ?x)
          (LeftOf10 ?x)
        )
        (and
          (LeftOf13 ?x)
          (not (LeftOf12 ?x))
        )
      )
      (when
        (or
          (LeftOf8 ?x)
          (LeftOf7 ?x)
          (LeftOf4 ?x)
          (LeftOf3 ?x)
          (LeftOf5 ?x)
          (LeftOf6 ?x)
          (LeftOf12 ?x)
          (LeftOf1 ?x)
          (LeftOf13 ?x)
          (LeftOf2 ?x)
          (LeftOf9 ?x)
          (LeftOf11 ?x)
          (LeftOf10 ?x)
        )
        (and
          (LeftOf14 ?x)
          (not (LeftOf13 ?x))
        )
      )
      (when
        (or
          (LeftOf8 ?x)
          (LeftOf7 ?x)
          (LeftOf4 ?x)
          (LeftOf3 ?x)
          (LeftOf14 ?x)
          (LeftOf5 ?x)
          (LeftOf6 ?x)
          (LeftOf12 ?x)
          (LeftOf1 ?x)
          (LeftOf13 ?x)
          (LeftOf2 ?x)
          (LeftOf9 ?x)
          (LeftOf11 ?x)
          (LeftOf10 ?x)
        )
        (and
          (LeftOf15 ?x)
          (not (LeftOf14 ?x))
        )
      )
      (when
        (Column0 ?x)
        (and
          (Column1 ?x)
          (not (Column0 ?x))
        )
      )
      (when
        (Column1 ?x)
        (and
          (Column2 ?x)
          (not (Column1 ?x))
        )
      )
      (when
        (Column2 ?x)
        (and
          (Column3 ?x)
          (not (Column2 ?x))
        )
      )
      (when
        (Column3 ?x)
        (and
          (Column4 ?x)
          (not (Column3 ?x))
        )
      )
      (when
        (Column4 ?x)
        (and
          (Column5 ?x)
          (not (Column4 ?x))
        )
      )
      (when
        (Column5 ?x)
        (and
          (Column6 ?x)
          (not (Column5 ?x))
        )
      )
      (when
        (Column6 ?x)
        (and
          (Column7 ?x)
          (not (Column6 ?x))
        )
      )
      (when
        (Column7 ?x)
        (and
          (Column8 ?x)
          (not (Column7 ?x))
        )
      )
      (when
        (Column8 ?x)
        (and
          (Column9 ?x)
          (not (Column8 ?x))
        )
      )
      (when
        (Column9 ?x)
        (and
          (Column10 ?x)
          (not (Column9 ?x))
        )
      )
      (when
        (Column10 ?x)
        (and
          (Column11 ?x)
          (not (Column10 ?x))
        )
      )
      (when
        (Column11 ?x)
        (and
          (Column12 ?x)
          (not (Column11 ?x))
        )
      )
      (when
        (Column12 ?x)
        (and
          (Column13 ?x)
          (not (Column12 ?x))
        )
      )
      (when
        (Column13 ?x)
        (and
          (Column14 ?x)
          (not (Column13 ?x))
        )
      )
      (when
        (or
          (and
            (LeftOf1 ?x)
            (RightOf10 ?x)
          )
          (and
            (RightOf14 ?x)
            (LeftOf1 ?x)
          )
          (and
            (RightOf6 ?x)
            (LeftOf1 ?x)
          )
          (and
            (LeftOf1 ?x)
            (RightOf8 ?x)
          )
          (and
            (LeftOf1 ?x)
            (RightOf11 ?x)
          )
          (and
            (RightOf2 ?x)
            (LeftOf1 ?x)
          )
          (and
            (RightOf5 ?x)
            (LeftOf1 ?x)
          )
          (and
            (LeftOf1 ?x)
            (RightOf0 ?x)
          )
          (and
            (LeftOf1 ?x)
            (RightOf3 ?x)
          )
          (and
            (RightOf1 ?x)
            (LeftOf1 ?x)
          )
          (and
            (LeftOf1 ?x)
            (RightOf13 ?x)
          )
          (and
            (RightOf9 ?x)
            (LeftOf1 ?x)
          )
          (and
            (RightOf7 ?x)
            (LeftOf1 ?x)
          )
          (and
            (RightOf12 ?x)
            (LeftOf1 ?x)
          )
          (and
            (RightOf4 ?x)
            (LeftOf1 ?x)
          )
        )
        (and
          (Column1 ?x)
        )
      )
      (when
        (or
          (and
            (RightOf4 ?x)
            (LeftOf2 ?x)
          )
          (and
            (RightOf6 ?x)
            (LeftOf2 ?x)
          )
          (and
            (RightOf14 ?x)
            (LeftOf1 ?x)
          )
          (and
            (RightOf6 ?x)
            (LeftOf1 ?x)
          )
          (and
            (RightOf2 ?x)
            (LeftOf1 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf2 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf8 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf11 ?x)
          )
          (and
            (LeftOf1 ?x)
            (RightOf13 ?x)
          )
          (and
            (LeftOf1 ?x)
            (RightOf8 ?x)
          )
          (and
            (RightOf5 ?x)
            (LeftOf1 ?x)
          )
          (and
            (RightOf12 ?x)
            (LeftOf2 ?x)
          )
          (and
            (RightOf7 ?x)
            (LeftOf1 ?x)
          )
          (and
            (RightOf12 ?x)
            (LeftOf1 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf10 ?x)
          )
          (and
            (RightOf4 ?x)
            (LeftOf1 ?x)
          )
          (and
            (RightOf14 ?x)
            (LeftOf2 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf3 ?x)
          )
          (and
            (LeftOf1 ?x)
            (RightOf11 ?x)
          )
          (and
            (LeftOf1 ?x)
            (RightOf5 ?x)
          )
          (and
            (RightOf7 ?x)
            (LeftOf2 ?x)
          )
          (and
            (RightOf1 ?x)
            (LeftOf1 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf5 ?x)
          )
          (and
            (RightOf1 ?x)
            (LeftOf2 ?x)
          )
          (and
            (LeftOf1 ?x)
            (RightOf10 ?x)
          )
          (and
            (LeftOf1 ?x)
            (RightOf3 ?x)
          )
          (and
            (RightOf9 ?x)
            (LeftOf1 ?x)
          )
          (and
            (RightOf9 ?x)
            (LeftOf2 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf13 ?x)
          )
        )
        (and
          (Column2 ?x)
        )
      )
      (when
        (or
          (and
            (RightOf2 ?x)
            (LeftOf3 ?x)
          )
          (and
            (RightOf4 ?x)
            (LeftOf2 ?x)
          )
          (and
            (RightOf9 ?x)
            (LeftOf3 ?x)
          )
          (and
            (RightOf6 ?x)
            (LeftOf2 ?x)
          )
          (and
            (RightOf14 ?x)
            (LeftOf1 ?x)
          )
          (and
            (RightOf6 ?x)
            (LeftOf1 ?x)
          )
          (and
            (RightOf2 ?x)
            (LeftOf1 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf2 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf8 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf11 ?x)
          )
          (and
            (LeftOf1 ?x)
            (RightOf13 ?x)
          )
          (and
            (LeftOf3 ?x)
            (RightOf13 ?x)
          )
          (and
            (RightOf11 ?x)
            (LeftOf3 ?x)
          )
          (and
            (LeftOf3 ?x)
            (RightOf3 ?x)
          )
          (and
            (LeftOf1 ?x)
            (RightOf8 ?x)
          )
          (and
            (RightOf5 ?x)
            (LeftOf3 ?x)
          )
          (and
            (RightOf12 ?x)
            (LeftOf2 ?x)
          )
          (and
            (RightOf7 ?x)
            (LeftOf2 ?x)
          )
          (and
            (RightOf8 ?x)
            (LeftOf3 ?x)
          )
          (and
            (RightOf10 ?x)
            (LeftOf3 ?x)
          )
          (and
            (RightOf12 ?x)
            (LeftOf1 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf10 ?x)
          )
          (and
            (RightOf4 ?x)
            (LeftOf1 ?x)
          )
          (and
            (RightOf14 ?x)
            (LeftOf2 ?x)
          )
          (and
            (RightOf12 ?x)
            (LeftOf3 ?x)
          )
          (and
            (RightOf5 ?x)
            (LeftOf1 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf3 ?x)
          )
          (and
            (LeftOf1 ?x)
            (RightOf11 ?x)
          )
          (and
            (RightOf7 ?x)
            (LeftOf3 ?x)
          )
          (and
            (LeftOf1 ?x)
            (RightOf5 ?x)
          )
          (and
            (RightOf7 ?x)
            (LeftOf1 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf5 ?x)
          )
          (and
            (LeftOf1 ?x)
            (RightOf10 ?x)
          )
          (and
            (RightOf4 ?x)
            (LeftOf3 ?x)
          )
          (and
            (RightOf14 ?x)
            (LeftOf3 ?x)
          )
          (and
            (LeftOf1 ?x)
            (RightOf3 ?x)
          )
          (and
            (RightOf9 ?x)
            (LeftOf1 ?x)
          )
          (and
            (RightOf6 ?x)
            (LeftOf3 ?x)
          )
          (and
            (RightOf9 ?x)
            (LeftOf2 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf13 ?x)
          )
        )
        (and
          (Column3 ?x)
        )
      )
      (when
        (or
          (and
            (RightOf4 ?x)
            (LeftOf2 ?x)
          )
          (and
            (RightOf9 ?x)
            (LeftOf3 ?x)
          )
          (and
            (RightOf6 ?x)
            (LeftOf2 ?x)
          )
          (and
            (RightOf11 ?x)
            (LeftOf4 ?x)
          )
          (and
            (LeftOf4 ?x)
            (RightOf3 ?x)
          )
          (and
            (RightOf6 ?x)
            (LeftOf1 ?x)
          )
          (and
            (LeftOf4 ?x)
            (RightOf13 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf8 ?x)
          )
          (and
            (RightOf5 ?x)
            (LeftOf4 ?x)
          )
          (and
            (RightOf6 ?x)
            (LeftOf4 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf11 ?x)
          )
          (and
            (LeftOf1 ?x)
            (RightOf13 ?x)
          )
          (and
            (RightOf9 ?x)
            (LeftOf4 ?x)
          )
          (and
            (LeftOf3 ?x)
            (RightOf13 ?x)
          )
          (and
            (LeftOf4 ?x)
            (RightOf10 ?x)
          )
          (and
            (RightOf12 ?x)
            (LeftOf4 ?x)
          )
          (and
            (RightOf11 ?x)
            (LeftOf3 ?x)
          )
          (and
            (LeftOf1 ?x)
            (RightOf8 ?x)
          )
          (and
            (RightOf14 ?x)
            (LeftOf1 ?x)
          )
          (and
            (RightOf5 ?x)
            (LeftOf3 ?x)
          )
          (and
            (LeftOf4 ?x)
            (RightOf8 ?x)
          )
          (and
            (RightOf12 ?x)
            (LeftOf2 ?x)
          )
          (and
            (RightOf7 ?x)
            (LeftOf2 ?x)
          )
          (and
            (RightOf3 ?x)
            (LeftOf3 ?x)
          )
          (and
            (RightOf10 ?x)
            (LeftOf3 ?x)
          )
          (and
            (RightOf12 ?x)
            (LeftOf1 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf10 ?x)
          )
          (and
            (RightOf14 ?x)
            (LeftOf4 ?x)
          )
          (and
            (RightOf4 ?x)
            (LeftOf1 ?x)
          )
          (and
            (RightOf14 ?x)
            (LeftOf2 ?x)
          )
          (and
            (RightOf12 ?x)
            (LeftOf3 ?x)
          )
          (and
            (RightOf5 ?x)
            (LeftOf1 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf3 ?x)
          )
          (and
            (LeftOf1 ?x)
            (RightOf11 ?x)
          )
          (and
            (RightOf7 ?x)
            (LeftOf3 ?x)
          )
          (and
            (LeftOf1 ?x)
            (RightOf5 ?x)
          )
          (and
            (RightOf7 ?x)
            (LeftOf1 ?x)
          )
          (and
            (RightOf8 ?x)
            (LeftOf3 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf5 ?x)
          )
          (and
            (RightOf7 ?x)
            (LeftOf4 ?x)
          )
          (and
            (RightOf4 ?x)
            (LeftOf4 ?x)
          )
          (and
            (LeftOf1 ?x)
            (RightOf10 ?x)
          )
          (and
            (RightOf4 ?x)
            (LeftOf3 ?x)
          )
          (and
            (RightOf13 ?x)
            (LeftOf3 ?x)
          )
          (and
            (RightOf14 ?x)
            (LeftOf3 ?x)
          )
          (and
            (LeftOf1 ?x)
            (RightOf3 ?x)
          )
          (and
            (RightOf9 ?x)
            (LeftOf1 ?x)
          )
          (and
            (RightOf6 ?x)
            (LeftOf3 ?x)
          )
          (and
            (RightOf9 ?x)
            (LeftOf2 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf13 ?x)
          )
        )
        (and
          (Column4 ?x)
        )
      )
      (when
        (or
          (and
            (RightOf4 ?x)
            (LeftOf2 ?x)
          )
          (and
            (RightOf9 ?x)
            (LeftOf3 ?x)
          )
          (and
            (RightOf6 ?x)
            (LeftOf2 ?x)
          )
          (and
            (RightOf11 ?x)
            (LeftOf4 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf5 ?x)
          )
          (and
            (RightOf6 ?x)
            (LeftOf1 ?x)
          )
          (and
            (RightOf5 ?x)
            (LeftOf3 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf14 ?x)
          )
          (and
            (LeftOf4 ?x)
            (RightOf13 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf8 ?x)
          )
          (and
            (RightOf5 ?x)
            (LeftOf4 ?x)
          )
          (and
            (RightOf6 ?x)
            (LeftOf4 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf11 ?x)
          )
          (and
            (LeftOf1 ?x)
            (RightOf13 ?x)
          )
          (and
            (RightOf9 ?x)
            (LeftOf4 ?x)
          )
          (and
            (LeftOf3 ?x)
            (RightOf13 ?x)
          )
          (and
            (LeftOf4 ?x)
            (RightOf10 ?x)
          )
          (and
            (RightOf12 ?x)
            (LeftOf4 ?x)
          )
          (and
            (RightOf11 ?x)
            (LeftOf3 ?x)
          )
          (and
            (RightOf14 ?x)
            (LeftOf2 ?x)
          )
          (and
            (RightOf12 ?x)
            (LeftOf3 ?x)
          )
          (and
            (RightOf5 ?x)
            (LeftOf1 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf8 ?x)
          )
          (and
            (RightOf12 ?x)
            (LeftOf5 ?x)
          )
          (and
            (LeftOf4 ?x)
            (RightOf8 ?x)
          )
          (and
            (RightOf12 ?x)
            (LeftOf2 ?x)
          )
          (and
            (RightOf7 ?x)
            (LeftOf2 ?x)
          )
          (and
            (RightOf10 ?x)
            (LeftOf4 ?x)
          )
          (and
            (RightOf10 ?x)
            (LeftOf3 ?x)
          )
          (and
            (RightOf12 ?x)
            (LeftOf1 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf10 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf10 ?x)
          )
          (and
            (RightOf4 ?x)
            (LeftOf1 ?x)
          )
          (and
            (RightOf6 ?x)
            (LeftOf5 ?x)
          )
          (and
            (RightOf8 ?x)
            (LeftOf4 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf4 ?x)
          )
          (and
            (RightOf14 ?x)
            (LeftOf4 ?x)
          )
          (and
            (LeftOf1 ?x)
            (RightOf11 ?x)
          )
          (and
            (RightOf7 ?x)
            (LeftOf3 ?x)
          )
          (and
            (LeftOf1 ?x)
            (RightOf5 ?x)
          )
          (and
            (RightOf7 ?x)
            (LeftOf1 ?x)
          )
          (and
            (RightOf8 ?x)
            (LeftOf3 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf5 ?x)
          )
          (and
            (RightOf7 ?x)
            (LeftOf4 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf13 ?x)
          )
          (and
            (RightOf4 ?x)
            (LeftOf4 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf11 ?x)
          )
          (and
            (LeftOf1 ?x)
            (RightOf8 ?x)
          )
          (and
            (LeftOf1 ?x)
            (RightOf10 ?x)
          )
          (and
            (RightOf4 ?x)
            (LeftOf3 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf7 ?x)
          )
          (and
            (RightOf13 ?x)
            (LeftOf3 ?x)
          )
          (and
            (RightOf14 ?x)
            (LeftOf1 ?x)
          )
          (and
            (RightOf14 ?x)
            (LeftOf3 ?x)
          )
          (and
            (RightOf9 ?x)
            (LeftOf1 ?x)
          )
          (and
            (RightOf6 ?x)
            (LeftOf3 ?x)
          )
          (and
            (RightOf9 ?x)
            (LeftOf2 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf13 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf9 ?x)
          )
        )
        (and
          (Column5 ?x)
        )
      )
      (when
        (or
          (and
            (LeftOf6 ?x)
            (RightOf9 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf11 ?x)
          )
          (and
            (RightOf6 ?x)
            (LeftOf2 ?x)
          )
          (and
            (RightOf11 ?x)
            (LeftOf4 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf5 ?x)
          )
          (and
            (RightOf6 ?x)
            (LeftOf1 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf14 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf14 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf8 ?x)
          )
          (and
            (RightOf5 ?x)
            (LeftOf4 ?x)
          )
          (and
            (RightOf6 ?x)
            (LeftOf4 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf13 ?x)
          )
          (and
            (LeftOf1 ?x)
            (RightOf13 ?x)
          )
          (and
            (RightOf9 ?x)
            (LeftOf4 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf6 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf10 ?x)
          )
          (and
            (LeftOf3 ?x)
            (RightOf13 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf7 ?x)
          )
          (and
            (LeftOf4 ?x)
            (RightOf10 ?x)
          )
          (and
            (RightOf12 ?x)
            (LeftOf4 ?x)
          )
          (and
            (RightOf11 ?x)
            (LeftOf3 ?x)
          )
          (and
            (LeftOf1 ?x)
            (RightOf8 ?x)
          )
          (and
            (RightOf14 ?x)
            (LeftOf1 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf5 ?x)
          )
          (and
            (RightOf5 ?x)
            (LeftOf3 ?x)
          )
          (and
            (RightOf14 ?x)
            (LeftOf2 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf8 ?x)
          )
          (and
            (RightOf12 ?x)
            (LeftOf5 ?x)
          )
          (and
            (LeftOf4 ?x)
            (RightOf8 ?x)
          )
          (and
            (RightOf12 ?x)
            (LeftOf2 ?x)
          )
          (and
            (RightOf7 ?x)
            (LeftOf1 ?x)
          )
          (and
            (RightOf10 ?x)
            (LeftOf4 ?x)
          )
          (and
            (RightOf10 ?x)
            (LeftOf3 ?x)
          )
          (and
            (RightOf12 ?x)
            (LeftOf1 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf10 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf10 ?x)
          )
          (and
            (RightOf6 ?x)
            (LeftOf5 ?x)
          )
          (and
            (RightOf8 ?x)
            (LeftOf4 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf11 ?x)
          )
          (and
            (RightOf12 ?x)
            (LeftOf3 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf8 ?x)
          )
          (and
            (RightOf14 ?x)
            (LeftOf4 ?x)
          )
          (and
            (LeftOf1 ?x)
            (RightOf11 ?x)
          )
          (and
            (RightOf7 ?x)
            (LeftOf3 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf12 ?x)
          )
          (and
            (LeftOf4 ?x)
            (RightOf13 ?x)
          )
          (and
            (RightOf7 ?x)
            (LeftOf2 ?x)
          )
          (and
            (LeftOf1 ?x)
            (RightOf5 ?x)
          )
          (and
            (RightOf8 ?x)
            (LeftOf3 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf5 ?x)
          )
          (and
            (RightOf7 ?x)
            (LeftOf4 ?x)
          )
          (and
            (RightOf6 ?x)
            (LeftOf3 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf13 ?x)
          )
          (and
            (LeftOf1 ?x)
            (RightOf10 ?x)
          )
          (and
            (RightOf9 ?x)
            (LeftOf3 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf11 ?x)
          )
          (and
            (RightOf13 ?x)
            (LeftOf3 ?x)
          )
          (and
            (RightOf14 ?x)
            (LeftOf3 ?x)
          )
          (and
            (RightOf9 ?x)
            (LeftOf1 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf7 ?x)
          )
          (and
            (RightOf9 ?x)
            (LeftOf2 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf13 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf9 ?x)
          )
        )
        (and
          (Column6 ?x)
        )
      )
      (when
        (or
          (and
            (LeftOf6 ?x)
            (RightOf9 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf11 ?x)
          )
          (and
            (RightOf6 ?x)
            (LeftOf2 ?x)
          )
          (and
            (RightOf11 ?x)
            (LeftOf4 ?x)
          )
          (and
            (RightOf14 ?x)
            (LeftOf1 ?x)
          )
          (and
            (RightOf6 ?x)
            (LeftOf1 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf14 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf14 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf8 ?x)
          )
          (and
            (LeftOf7 ?x)
            (RightOf10 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf13 ?x)
          )
          (and
            (LeftOf1 ?x)
            (RightOf13 ?x)
          )
          (and
            (RightOf9 ?x)
            (LeftOf4 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf10 ?x)
          )
          (and
            (RightOf12 ?x)
            (LeftOf6 ?x)
          )
          (and
            (RightOf6 ?x)
            (LeftOf7 ?x)
          )
          (and
            (RightOf9 ?x)
            (LeftOf7 ?x)
          )
          (and
            (LeftOf3 ?x)
            (RightOf13 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf7 ?x)
          )
          (and
            (LeftOf4 ?x)
            (RightOf10 ?x)
          )
          (and
            (RightOf12 ?x)
            (LeftOf4 ?x)
          )
          (and
            (RightOf11 ?x)
            (LeftOf3 ?x)
          )
          (and
            (LeftOf1 ?x)
            (RightOf8 ?x)
          )
          (and
            (LeftOf4 ?x)
            (RightOf8 ?x)
          )
          (and
            (RightOf14 ?x)
            (LeftOf2 ?x)
          )
          (and
            (RightOf14 ?x)
            (LeftOf7 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf8 ?x)
          )
          (and
            (RightOf12 ?x)
            (LeftOf5 ?x)
          )
          (and
            (RightOf12 ?x)
            (LeftOf7 ?x)
          )
          (and
            (RightOf12 ?x)
            (LeftOf2 ?x)
          )
          (and
            (RightOf7 ?x)
            (LeftOf1 ?x)
          )
          (and
            (RightOf8 ?x)
            (LeftOf3 ?x)
          )
          (and
            (RightOf10 ?x)
            (LeftOf3 ?x)
          )
          (and
            (RightOf6 ?x)
            (LeftOf6 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf10 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf10 ?x)
          )
          (and
            (RightOf6 ?x)
            (LeftOf5 ?x)
          )
          (and
            (RightOf8 ?x)
            (LeftOf4 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf11 ?x)
          )
          (and
            (RightOf12 ?x)
            (LeftOf3 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf8 ?x)
          )
          (and
            (RightOf14 ?x)
            (LeftOf4 ?x)
          )
          (and
            (LeftOf1 ?x)
            (RightOf11 ?x)
          )
          (and
            (RightOf7 ?x)
            (LeftOf3 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf12 ?x)
          )
          (and
            (LeftOf4 ?x)
            (RightOf13 ?x)
          )
          (and
            (RightOf14 ?x)
            (LeftOf3 ?x)
          )
          (and
            (RightOf7 ?x)
            (LeftOf2 ?x)
          )
          (and
            (LeftOf7 ?x)
            (RightOf13 ?x)
          )
          (and
            (RightOf10 ?x)
            (LeftOf4 ?x)
          )
          (and
            (RightOf7 ?x)
            (LeftOf4 ?x)
          )
          (and
            (RightOf6 ?x)
            (LeftOf3 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf13 ?x)
          )
          (and
            (RightOf12 ?x)
            (LeftOf1 ?x)
          )
          (and
            (LeftOf1 ?x)
            (RightOf10 ?x)
          )
          (and
            (RightOf9 ?x)
            (LeftOf3 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf11 ?x)
          )
          (and
            (RightOf13 ?x)
            (LeftOf3 ?x)
          )
          (and
            (RightOf7 ?x)
            (LeftOf7 ?x)
          )
          (and
            (LeftOf7 ?x)
            (RightOf8 ?x)
          )
          (and
            (RightOf6 ?x)
            (LeftOf4 ?x)
          )
          (and
            (LeftOf7 ?x)
            (RightOf11 ?x)
          )
          (and
            (RightOf9 ?x)
            (LeftOf1 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf7 ?x)
          )
          (and
            (RightOf9 ?x)
            (LeftOf2 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf13 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf9 ?x)
          )
        )
        (and
          (Column7 ?x)
        )
      )
      (when
        (or
          (and
            (LeftOf6 ?x)
            (RightOf9 ?x)
          )
          (and
            (RightOf9 ?x)
            (LeftOf3 ?x)
          )
          (and
            (RightOf11 ?x)
            (LeftOf4 ?x)
          )
          (and
            (RightOf14 ?x)
            (LeftOf1 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf14 ?x)
          )
          (and
            (LeftOf8 ?x)
            (RightOf11 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf14 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf8 ?x)
          )
          (and
            (LeftOf7 ?x)
            (RightOf10 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf13 ?x)
          )
          (and
            (LeftOf1 ?x)
            (RightOf13 ?x)
          )
          (and
            (RightOf9 ?x)
            (LeftOf4 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf10 ?x)
          )
          (and
            (RightOf12 ?x)
            (LeftOf6 ?x)
          )
          (and
            (RightOf9 ?x)
            (LeftOf7 ?x)
          )
          (and
            (LeftOf3 ?x)
            (RightOf13 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf7 ?x)
          )
          (and
            (LeftOf4 ?x)
            (RightOf10 ?x)
          )
          (and
            (RightOf12 ?x)
            (LeftOf4 ?x)
          )
          (and
            (RightOf11 ?x)
            (LeftOf3 ?x)
          )
          (and
            (LeftOf1 ?x)
            (RightOf8 ?x)
          )
          (and
            (LeftOf4 ?x)
            (RightOf8 ?x)
          )
          (and
            (RightOf14 ?x)
            (LeftOf2 ?x)
          )
          (and
            (RightOf14 ?x)
            (LeftOf7 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf8 ?x)
          )
          (and
            (RightOf12 ?x)
            (LeftOf5 ?x)
          )
          (and
            (RightOf12 ?x)
            (LeftOf7 ?x)
          )
          (and
            (RightOf12 ?x)
            (LeftOf2 ?x)
          )
          (and
            (RightOf7 ?x)
            (LeftOf2 ?x)
          )
          (and
            (RightOf8 ?x)
            (LeftOf3 ?x)
          )
          (and
            (RightOf10 ?x)
            (LeftOf3 ?x)
          )
          (and
            (RightOf12 ?x)
            (LeftOf1 ?x)
          )
          (and
            (RightOf12 ?x)
            (LeftOf8 ?x)
          )
          (and
            (LeftOf7 ?x)
            (RightOf7 ?x)
          )
          (and
            (LeftOf8 ?x)
            (RightOf8 ?x)
          )
          (and
            (LeftOf8 ?x)
            (RightOf10 ?x)
          )
          (and
            (RightOf8 ?x)
            (LeftOf4 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf11 ?x)
          )
          (and
            (RightOf12 ?x)
            (LeftOf3 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf8 ?x)
          )
          (and
            (RightOf14 ?x)
            (LeftOf4 ?x)
          )
          (and
            (LeftOf1 ?x)
            (RightOf11 ?x)
          )
          (and
            (RightOf7 ?x)
            (LeftOf3 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf12 ?x)
          )
          (and
            (LeftOf4 ?x)
            (RightOf13 ?x)
          )
          (and
            (RightOf7 ?x)
            (LeftOf1 ?x)
          )
          (and
            (LeftOf7 ?x)
            (RightOf13 ?x)
          )
          (and
            (RightOf10 ?x)
            (LeftOf4 ?x)
          )
          (and
            (LeftOf8 ?x)
            (RightOf14 ?x)
          )
          (and
            (RightOf7 ?x)
            (LeftOf4 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf13 ?x)
          )
          (and
            (LeftOf8 ?x)
            (RightOf13 ?x)
          )
          (and
            (RightOf9 ?x)
            (LeftOf2 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf11 ?x)
          )
          (and
            (LeftOf1 ?x)
            (RightOf10 ?x)
          )
          (and
            (RightOf9 ?x)
            (LeftOf8 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf10 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf11 ?x)
          )
          (and
            (RightOf13 ?x)
            (LeftOf3 ?x)
          )
          (and
            (LeftOf7 ?x)
            (RightOf8 ?x)
          )
          (and
            (RightOf14 ?x)
            (LeftOf3 ?x)
          )
          (and
            (LeftOf7 ?x)
            (RightOf11 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf10 ?x)
          )
          (and
            (RightOf9 ?x)
            (LeftOf1 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf7 ?x)
          )
          (and
            (LeftOf8 ?x)
            (RightOf7 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf13 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf9 ?x)
          )
        )
        (and
          (Column8 ?x)
        )
      )
      (when
        (or
          (and
            (LeftOf6 ?x)
            (RightOf9 ?x)
          )
          (and
            (RightOf9 ?x)
            (LeftOf3 ?x)
          )
          (and
            (RightOf14 ?x)
            (LeftOf9 ?x)
          )
          (and
            (RightOf14 ?x)
            (LeftOf1 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf14 ?x)
          )
          (and
            (LeftOf8 ?x)
            (RightOf11 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf14 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf8 ?x)
          )
          (and
            (LeftOf7 ?x)
            (RightOf10 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf13 ?x)
          )
          (and
            (LeftOf1 ?x)
            (RightOf13 ?x)
          )
          (and
            (RightOf9 ?x)
            (LeftOf4 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf10 ?x)
          )
          (and
            (RightOf12 ?x)
            (LeftOf6 ?x)
          )
          (and
            (RightOf9 ?x)
            (LeftOf7 ?x)
          )
          (and
            (LeftOf3 ?x)
            (RightOf13 ?x)
          )
          (and
            (LeftOf4 ?x)
            (RightOf10 ?x)
          )
          (and
            (RightOf12 ?x)
            (LeftOf4 ?x)
          )
          (and
            (RightOf11 ?x)
            (LeftOf3 ?x)
          )
          (and
            (LeftOf1 ?x)
            (RightOf8 ?x)
          )
          (and
            (LeftOf9 ?x)
            (RightOf11 ?x)
          )
          (and
            (RightOf14 ?x)
            (LeftOf2 ?x)
          )
          (and
            (RightOf14 ?x)
            (LeftOf7 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf8 ?x)
          )
          (and
            (RightOf12 ?x)
            (LeftOf9 ?x)
          )
          (and
            (RightOf12 ?x)
            (LeftOf5 ?x)
          )
          (and
            (RightOf12 ?x)
            (LeftOf7 ?x)
          )
          (and
            (RightOf12 ?x)
            (LeftOf2 ?x)
          )
          (and
            (RightOf14 ?x)
            (LeftOf8 ?x)
          )
          (and
            (RightOf10 ?x)
            (LeftOf4 ?x)
          )
          (and
            (RightOf10 ?x)
            (LeftOf3 ?x)
          )
          (and
            (RightOf12 ?x)
            (LeftOf1 ?x)
          )
          (and
            (RightOf12 ?x)
            (LeftOf8 ?x)
          )
          (and
            (LeftOf8 ?x)
            (RightOf8 ?x)
          )
          (and
            (LeftOf8 ?x)
            (RightOf10 ?x)
          )
          (and
            (LeftOf9 ?x)
            (RightOf10 ?x)
          )
          (and
            (RightOf8 ?x)
            (LeftOf4 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf11 ?x)
          )
          (and
            (RightOf12 ?x)
            (LeftOf3 ?x)
          )
          (and
            (RightOf14 ?x)
            (LeftOf4 ?x)
          )
          (and
            (LeftOf1 ?x)
            (RightOf11 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf8 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf12 ?x)
          )
          (and
            (LeftOf4 ?x)
            (RightOf13 ?x)
          )
          (and
            (LeftOf7 ?x)
            (RightOf13 ?x)
          )
          (and
            (RightOf9 ?x)
            (LeftOf9 ?x)
          )
          (and
            (RightOf8 ?x)
            (LeftOf3 ?x)
          )
          (and
            (LeftOf8 ?x)
            (RightOf14 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf13 ?x)
          )
          (and
            (LeftOf8 ?x)
            (RightOf13 ?x)
          )
          (and
            (LeftOf9 ?x)
            (RightOf8 ?x)
          )
          (and
            (LeftOf9 ?x)
            (RightOf13 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf11 ?x)
          )
          (and
            (LeftOf1 ?x)
            (RightOf10 ?x)
          )
          (and
            (RightOf9 ?x)
            (LeftOf8 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf10 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf11 ?x)
          )
          (and
            (RightOf13 ?x)
            (LeftOf3 ?x)
          )
          (and
            (LeftOf7 ?x)
            (RightOf8 ?x)
          )
          (and
            (RightOf14 ?x)
            (LeftOf3 ?x)
          )
          (and
            (LeftOf7 ?x)
            (RightOf11 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf10 ?x)
          )
          (and
            (RightOf9 ?x)
            (LeftOf1 ?x)
          )
          (and
            (RightOf9 ?x)
            (LeftOf2 ?x)
          )
          (and
            (RightOf11 ?x)
            (LeftOf4 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf13 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf9 ?x)
          )
        )
        (and
          (Column9 ?x)
        )
      )
      (when
        (or
          (and
            (LeftOf6 ?x)
            (RightOf9 ?x)
          )
          (and
            (RightOf9 ?x)
            (LeftOf3 ?x)
          )
          (and
            (RightOf14 ?x)
            (LeftOf9 ?x)
          )
          (and
            (RightOf14 ?x)
            (LeftOf1 ?x)
          )
          (and
            (LeftOf8 ?x)
            (RightOf11 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf14 ?x)
          )
          (and
            (LeftOf7 ?x)
            (RightOf10 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf13 ?x)
          )
          (and
            (LeftOf1 ?x)
            (RightOf13 ?x)
          )
          (and
            (LeftOf7 ?x)
            (RightOf11 ?x)
          )
          (and
            (RightOf9 ?x)
            (LeftOf4 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf10 ?x)
          )
          (and
            (RightOf12 ?x)
            (LeftOf6 ?x)
          )
          (and
            (RightOf9 ?x)
            (LeftOf7 ?x)
          )
          (and
            (LeftOf3 ?x)
            (RightOf13 ?x)
          )
          (and
            (LeftOf4 ?x)
            (RightOf10 ?x)
          )
          (and
            (RightOf12 ?x)
            (LeftOf4 ?x)
          )
          (and
            (RightOf11 ?x)
            (LeftOf3 ?x)
          )
          (and
            (RightOf14 ?x)
            (LeftOf2 ?x)
          )
          (and
            (RightOf12 ?x)
            (LeftOf9 ?x)
          )
          (and
            (RightOf14 ?x)
            (LeftOf7 ?x)
          )
          (and
            (LeftOf9 ?x)
            (RightOf11 ?x)
          )
          (and
            (RightOf12 ?x)
            (LeftOf5 ?x)
          )
          (and
            (RightOf12 ?x)
            (LeftOf7 ?x)
          )
          (and
            (RightOf12 ?x)
            (LeftOf2 ?x)
          )
          (and
            (RightOf14 ?x)
            (LeftOf8 ?x)
          )
          (and
            (RightOf10 ?x)
            (LeftOf4 ?x)
          )
          (and
            (RightOf10 ?x)
            (LeftOf3 ?x)
          )
          (and
            (RightOf12 ?x)
            (LeftOf1 ?x)
          )
          (and
            (RightOf12 ?x)
            (LeftOf8 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf10 ?x)
          )
          (and
            (LeftOf8 ?x)
            (RightOf10 ?x)
          )
          (and
            (LeftOf9 ?x)
            (RightOf10 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf11 ?x)
          )
          (and
            (RightOf12 ?x)
            (LeftOf3 ?x)
          )
          (and
            (RightOf14 ?x)
            (LeftOf4 ?x)
          )
          (and
            (LeftOf1 ?x)
            (RightOf11 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf12 ?x)
          )
          (and
            (LeftOf4 ?x)
            (RightOf13 ?x)
          )
          (and
            (RightOf14 ?x)
            (LeftOf3 ?x)
          )
          (and
            (RightOf9 ?x)
            (LeftOf10 ?x)
          )
          (and
            (RightOf9 ?x)
            (LeftOf9 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf14 ?x)
          )
          (and
            (LeftOf8 ?x)
            (RightOf14 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf13 ?x)
          )
          (and
            (LeftOf8 ?x)
            (RightOf13 ?x)
          )
          (and
            (LeftOf9 ?x)
            (RightOf13 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf11 ?x)
          )
          (and
            (LeftOf1 ?x)
            (RightOf10 ?x)
          )
          (and
            (LeftOf10 ?x)
            (RightOf13 ?x)
          )
          (and
            (RightOf9 ?x)
            (LeftOf8 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf10 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf11 ?x)
          )
          (and
            (RightOf13 ?x)
            (LeftOf3 ?x)
          )
          (and
            (LeftOf10 ?x)
            (RightOf10 ?x)
          )
          (and
            (LeftOf10 ?x)
            (RightOf11 ?x)
          )
          (and
            (RightOf12 ?x)
            (LeftOf10 ?x)
          )
          (and
            (LeftOf7 ?x)
            (RightOf13 ?x)
          )
          (and
            (LeftOf10 ?x)
            (RightOf14 ?x)
          )
          (and
            (RightOf9 ?x)
            (LeftOf1 ?x)
          )
          (and
            (RightOf9 ?x)
            (LeftOf2 ?x)
          )
          (and
            (RightOf11 ?x)
            (LeftOf4 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf13 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf9 ?x)
          )
        )
        (and
          (Column10 ?x)
        )
      )
      (when
        (or
          (and
            (LeftOf6 ?x)
            (RightOf14 ?x)
          )
          (and
            (RightOf12 ?x)
            (LeftOf11 ?x)
          )
          (and
            (RightOf14 ?x)
            (LeftOf9 ?x)
          )
          (and
            (RightOf14 ?x)
            (LeftOf1 ?x)
          )
          (and
            (LeftOf8 ?x)
            (RightOf11 ?x)
          )
          (and
            (LeftOf4 ?x)
            (RightOf13 ?x)
          )
          (and
            (LeftOf11 ?x)
            (RightOf10 ?x)
          )
          (and
            (LeftOf7 ?x)
            (RightOf10 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf13 ?x)
          )
          (and
            (LeftOf1 ?x)
            (RightOf13 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf10 ?x)
          )
          (and
            (RightOf12 ?x)
            (LeftOf6 ?x)
          )
          (and
            (LeftOf3 ?x)
            (RightOf13 ?x)
          )
          (and
            (RightOf11 ?x)
            (LeftOf4 ?x)
          )
          (and
            (RightOf12 ?x)
            (LeftOf4 ?x)
          )
          (and
            (RightOf11 ?x)
            (LeftOf3 ?x)
          )
          (and
            (RightOf14 ?x)
            (LeftOf2 ?x)
          )
          (and
            (LeftOf9 ?x)
            (RightOf11 ?x)
          )
          (and
            (LeftOf11 ?x)
            (RightOf13 ?x)
          )
          (and
            (RightOf14 ?x)
            (LeftOf7 ?x)
          )
          (and
            (RightOf12 ?x)
            (LeftOf9 ?x)
          )
          (and
            (RightOf12 ?x)
            (LeftOf5 ?x)
          )
          (and
            (RightOf12 ?x)
            (LeftOf7 ?x)
          )
          (and
            (RightOf12 ?x)
            (LeftOf2 ?x)
          )
          (and
            (RightOf14 ?x)
            (LeftOf8 ?x)
          )
          (and
            (RightOf10 ?x)
            (LeftOf4 ?x)
          )
          (and
            (RightOf10 ?x)
            (LeftOf3 ?x)
          )
          (and
            (RightOf12 ?x)
            (LeftOf1 ?x)
          )
          (and
            (RightOf12 ?x)
            (LeftOf8 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf10 ?x)
          )
          (and
            (LeftOf8 ?x)
            (RightOf10 ?x)
          )
          (and
            (LeftOf9 ?x)
            (RightOf10 ?x)
          )
          (and
            (RightOf14 ?x)
            (LeftOf10 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf11 ?x)
          )
          (and
            (RightOf12 ?x)
            (LeftOf3 ?x)
          )
          (and
            (RightOf14 ?x)
            (LeftOf4 ?x)
          )
          (and
            (LeftOf1 ?x)
            (RightOf11 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf12 ?x)
          )
          (and
            (RightOf14 ?x)
            (LeftOf3 ?x)
          )
          (and
            (LeftOf7 ?x)
            (RightOf13 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf14 ?x)
          )
          (and
            (LeftOf8 ?x)
            (RightOf14 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf13 ?x)
          )
          (and
            (LeftOf8 ?x)
            (RightOf13 ?x)
          )
          (and
            (LeftOf9 ?x)
            (RightOf13 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf11 ?x)
          )
          (and
            (LeftOf1 ?x)
            (RightOf10 ?x)
          )
          (and
            (LeftOf10 ?x)
            (RightOf13 ?x)
          )
          (and
            (RightOf12 ?x)
            (LeftOf10 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf10 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf11 ?x)
          )
          (and
            (RightOf13 ?x)
            (LeftOf3 ?x)
          )
          (and
            (LeftOf10 ?x)
            (RightOf10 ?x)
          )
          (and
            (LeftOf10 ?x)
            (RightOf11 ?x)
          )
          (and
            (LeftOf11 ?x)
            (RightOf11 ?x)
          )
          (and
            (LeftOf10 ?x)
            (RightOf14 ?x)
          )
          (and
            (LeftOf7 ?x)
            (RightOf11 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf13 ?x)
          )
          (and
            (RightOf14 ?x)
            (LeftOf11 ?x)
          )
        )
        (and
          (Column11 ?x)
        )
      )
      (when
        (or
          (and
            (LeftOf6 ?x)
            (RightOf14 ?x)
          )
          (and
            (RightOf12 ?x)
            (LeftOf11 ?x)
          )
          (and
            (RightOf14 ?x)
            (LeftOf9 ?x)
          )
          (and
            (RightOf14 ?x)
            (LeftOf1 ?x)
          )
          (and
            (LeftOf8 ?x)
            (RightOf11 ?x)
          )
          (and
            (LeftOf4 ?x)
            (RightOf13 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf11 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf13 ?x)
          )
          (and
            (LeftOf1 ?x)
            (RightOf13 ?x)
          )
          (and
            (LeftOf12 ?x)
            (RightOf13 ?x)
          )
          (and
            (RightOf12 ?x)
            (LeftOf6 ?x)
          )
          (and
            (LeftOf3 ?x)
            (RightOf13 ?x)
          )
          (and
            (RightOf11 ?x)
            (LeftOf4 ?x)
          )
          (and
            (RightOf12 ?x)
            (LeftOf4 ?x)
          )
          (and
            (RightOf11 ?x)
            (LeftOf3 ?x)
          )
          (and
            (RightOf14 ?x)
            (LeftOf2 ?x)
          )
          (and
            (LeftOf9 ?x)
            (RightOf11 ?x)
          )
          (and
            (LeftOf11 ?x)
            (RightOf13 ?x)
          )
          (and
            (RightOf14 ?x)
            (LeftOf7 ?x)
          )
          (and
            (RightOf12 ?x)
            (LeftOf9 ?x)
          )
          (and
            (RightOf12 ?x)
            (LeftOf5 ?x)
          )
          (and
            (RightOf12 ?x)
            (LeftOf7 ?x)
          )
          (and
            (RightOf12 ?x)
            (LeftOf2 ?x)
          )
          (and
            (RightOf14 ?x)
            (LeftOf8 ?x)
          )
          (and
            (LeftOf7 ?x)
            (RightOf11 ?x)
          )
          (and
            (RightOf12 ?x)
            (LeftOf1 ?x)
          )
          (and
            (RightOf12 ?x)
            (LeftOf8 ?x)
          )
          (and
            (LeftOf12 ?x)
            (RightOf11 ?x)
          )
          (and
            (RightOf14 ?x)
            (LeftOf10 ?x)
          )
          (and
            (LeftOf12 ?x)
            (RightOf14 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf11 ?x)
          )
          (and
            (RightOf12 ?x)
            (LeftOf3 ?x)
          )
          (and
            (RightOf14 ?x)
            (LeftOf4 ?x)
          )
          (and
            (LeftOf1 ?x)
            (RightOf11 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf12 ?x)
          )
          (and
            (RightOf14 ?x)
            (LeftOf3 ?x)
          )
          (and
            (LeftOf7 ?x)
            (RightOf13 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf14 ?x)
          )
          (and
            (LeftOf8 ?x)
            (RightOf14 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf13 ?x)
          )
          (and
            (LeftOf8 ?x)
            (RightOf13 ?x)
          )
          (and
            (LeftOf9 ?x)
            (RightOf13 ?x)
          )
          (and
            (LeftOf10 ?x)
            (RightOf13 ?x)
          )
          (and
            (RightOf12 ?x)
            (LeftOf10 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf11 ?x)
          )
          (and
            (LeftOf12 ?x)
            (RightOf12 ?x)
          )
          (and
            (RightOf13 ?x)
            (LeftOf3 ?x)
          )
          (and
            (LeftOf10 ?x)
            (RightOf11 ?x)
          )
          (and
            (LeftOf11 ?x)
            (RightOf11 ?x)
          )
          (and
            (LeftOf10 ?x)
            (RightOf14 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf13 ?x)
          )
          (and
            (RightOf14 ?x)
            (LeftOf11 ?x)
          )
        )
        (and
          (Column12 ?x)
        )
      )
      (when
        (or
          (and
            (LeftOf6 ?x)
            (RightOf14 ?x)
          )
          (and
            (RightOf12 ?x)
            (LeftOf11 ?x)
          )
          (and
            (RightOf14 ?x)
            (LeftOf9 ?x)
          )
          (and
            (RightOf14 ?x)
            (LeftOf1 ?x)
          )
          (and
            (RightOf12 ?x)
            (LeftOf12 ?x)
          )
          (and
            (LeftOf4 ?x)
            (RightOf13 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf13 ?x)
          )
          (and
            (LeftOf1 ?x)
            (RightOf13 ?x)
          )
          (and
            (LeftOf13 ?x)
            (RightOf13 ?x)
          )
          (and
            (LeftOf12 ?x)
            (RightOf13 ?x)
          )
          (and
            (RightOf12 ?x)
            (LeftOf6 ?x)
          )
          (and
            (RightOf14 ?x)
            (LeftOf7 ?x)
          )
          (and
            (LeftOf3 ?x)
            (RightOf13 ?x)
          )
          (and
            (RightOf12 ?x)
            (LeftOf4 ?x)
          )
          (and
            (RightOf14 ?x)
            (LeftOf2 ?x)
          )
          (and
            (LeftOf11 ?x)
            (RightOf13 ?x)
          )
          (and
            (RightOf14 ?x)
            (LeftOf13 ?x)
          )
          (and
            (RightOf12 ?x)
            (LeftOf9 ?x)
          )
          (and
            (RightOf12 ?x)
            (LeftOf5 ?x)
          )
          (and
            (RightOf12 ?x)
            (LeftOf7 ?x)
          )
          (and
            (RightOf12 ?x)
            (LeftOf2 ?x)
          )
          (and
            (RightOf14 ?x)
            (LeftOf8 ?x)
          )
          (and
            (RightOf12 ?x)
            (LeftOf13 ?x)
          )
          (and
            (RightOf12 ?x)
            (LeftOf1 ?x)
          )
          (and
            (RightOf12 ?x)
            (LeftOf8 ?x)
          )
          (and
            (RightOf14 ?x)
            (LeftOf10 ?x)
          )
          (and
            (LeftOf12 ?x)
            (RightOf14 ?x)
          )
          (and
            (RightOf12 ?x)
            (LeftOf3 ?x)
          )
          (and
            (RightOf14 ?x)
            (LeftOf4 ?x)
          )
          (and
            (LeftOf7 ?x)
            (RightOf13 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf14 ?x)
          )
          (and
            (LeftOf8 ?x)
            (RightOf14 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf13 ?x)
          )
          (and
            (LeftOf8 ?x)
            (RightOf13 ?x)
          )
          (and
            (LeftOf9 ?x)
            (RightOf13 ?x)
          )
          (and
            (LeftOf10 ?x)
            (RightOf13 ?x)
          )
          (and
            (RightOf12 ?x)
            (LeftOf10 ?x)
          )
          (and
            (RightOf13 ?x)
            (LeftOf3 ?x)
          )
          (and
            (RightOf14 ?x)
            (LeftOf3 ?x)
          )
          (and
            (LeftOf10 ?x)
            (RightOf14 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf13 ?x)
          )
          (and
            (RightOf14 ?x)
            (LeftOf11 ?x)
          )
        )
        (and
          (Column13 ?x)
        )
      )
      (when
        (or
          (and
            (LeftOf6 ?x)
            (RightOf14 ?x)
          )
          (and
            (RightOf14 ?x)
            (LeftOf9 ?x)
          )
          (and
            (RightOf14 ?x)
            (LeftOf1 ?x)
          )
          (and
            (LeftOf4 ?x)
            (RightOf13 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf13 ?x)
          )
          (and
            (LeftOf1 ?x)
            (RightOf13 ?x)
          )
          (and
            (LeftOf13 ?x)
            (RightOf13 ?x)
          )
          (and
            (LeftOf12 ?x)
            (RightOf13 ?x)
          )
          (and
            (RightOf14 ?x)
            (LeftOf7 ?x)
          )
          (and
            (RightOf14 ?x)
            (LeftOf2 ?x)
          )
          (and
            (LeftOf11 ?x)
            (RightOf13 ?x)
          )
          (and
            (RightOf14 ?x)
            (LeftOf13 ?x)
          )
          (and
            (RightOf14 ?x)
            (LeftOf8 ?x)
          )
          (and
            (LeftOf10 ?x)
            (RightOf14 ?x)
          )
          (and
            (LeftOf14 ?x)
            (RightOf13 ?x)
          )
          (and
            (RightOf14 ?x)
            (LeftOf10 ?x)
          )
          (and
            (LeftOf12 ?x)
            (RightOf14 ?x)
          )
          (and
            (RightOf14 ?x)
            (LeftOf4 ?x)
          )
          (and
            (LeftOf7 ?x)
            (RightOf13 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf14 ?x)
          )
          (and
            (LeftOf8 ?x)
            (RightOf14 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf13 ?x)
          )
          (and
            (LeftOf8 ?x)
            (RightOf13 ?x)
          )
          (and
            (LeftOf9 ?x)
            (RightOf13 ?x)
          )
          (and
            (LeftOf10 ?x)
            (RightOf13 ?x)
          )
          (and
            (RightOf13 ?x)
            (LeftOf3 ?x)
          )
          (and
            (RightOf14 ?x)
            (LeftOf3 ?x)
          )
          (and
            (RightOf14 ?x)
            (LeftOf14 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf13 ?x)
          )
          (and
            (RightOf14 ?x)
            (LeftOf11 ?x)
          )
        )
        (and
          (Column14 ?x)
        )
      )
    )
  )
  (:action moveLeft
    :parameters ( ?x )
    :precondition ( and
      (not (CheckConsistency))
      (not (Error))
      (or
        (LeftOf3 ?x)
        (RightOf7 ?x)
        (RightOf8 ?x)
        (LeftOf2 ?x)
        (LeftOf9 ?x)
        (RightOf2 ?x)
        (LeftOf1 ?x)
        (LeftOf8 ?x)
        (LeftOf13 ?x)
        (LeftOf14 ?x)
        (LeftOf5 ?x)
        (RightOf12 ?x)
        (RightOf1 ?x)
        (LeftOf12 ?x)
        (Columns ?x)
        (RightOf13 ?x)
        (RightOf14 ?x)
        (RightOf6 ?x)
        (RightOf9 ?x)
        (LeftOf6 ?x)
        (RightOf0 ?x)
        (RightOf11 ?x)
        (LeftOf11 ?x)
        (RightOf5 ?x)
        (LeftOf10 ?x)
        (LeftOf15 ?x)
        (LeftOf4 ?x)
        (RightOf4 ?x)
        (RightOf10 ?x)
        (LeftOf7 ?x)
        (RightOf3 ?x)
      )
    )
    :effect ( and
      (CheckConsistency)
      (when
        (or
          (LeftOf2 ?x)
          (LeftOf1 ?x)
        )
        (and
          (LeftOf1 ?x)
        )
      )
      (when
        (or
          (LeftOf2 ?x)
          (LeftOf3 ?x)
          (LeftOf1 ?x)
        )
        (and
          (LeftOf2 ?x)
        )
      )
      (when
        (or
          (LeftOf2 ?x)
          (LeftOf4 ?x)
          (LeftOf3 ?x)
          (LeftOf1 ?x)
        )
        (and
          (LeftOf3 ?x)
        )
      )
      (when
        (or
          (LeftOf2 ?x)
          (LeftOf4 ?x)
          (LeftOf3 ?x)
          (LeftOf1 ?x)
          (LeftOf5 ?x)
        )
        (and
          (LeftOf4 ?x)
        )
      )
      (when
        (or
          (LeftOf4 ?x)
          (LeftOf3 ?x)
          (LeftOf5 ?x)
          (LeftOf6 ?x)
          (LeftOf1 ?x)
          (LeftOf2 ?x)
        )
        (and
          (LeftOf5 ?x)
        )
      )
      (when
        (or
          (LeftOf4 ?x)
          (LeftOf3 ?x)
          (LeftOf5 ?x)
          (LeftOf6 ?x)
          (LeftOf1 ?x)
          (LeftOf7 ?x)
          (LeftOf2 ?x)
        )
        (and
          (LeftOf6 ?x)
        )
      )
      (when
        (or
          (LeftOf8 ?x)
          (LeftOf4 ?x)
          (LeftOf3 ?x)
          (LeftOf5 ?x)
          (LeftOf6 ?x)
          (LeftOf1 ?x)
          (LeftOf7 ?x)
          (LeftOf2 ?x)
        )
        (and
          (LeftOf7 ?x)
        )
      )
      (when
        (or
          (LeftOf8 ?x)
          (LeftOf4 ?x)
          (LeftOf3 ?x)
          (LeftOf5 ?x)
          (LeftOf6 ?x)
          (LeftOf1 ?x)
          (LeftOf7 ?x)
          (LeftOf2 ?x)
          (LeftOf9 ?x)
        )
        (and
          (LeftOf8 ?x)
        )
      )
      (when
        (or
          (LeftOf8 ?x)
          (LeftOf4 ?x)
          (LeftOf3 ?x)
          (LeftOf5 ?x)
          (LeftOf6 ?x)
          (LeftOf1 ?x)
          (LeftOf7 ?x)
          (LeftOf2 ?x)
          (LeftOf9 ?x)
          (LeftOf10 ?x)
        )
        (and
          (LeftOf9 ?x)
        )
      )
      (when
        (or
          (LeftOf8 ?x)
          (LeftOf4 ?x)
          (LeftOf3 ?x)
          (LeftOf5 ?x)
          (LeftOf6 ?x)
          (LeftOf1 ?x)
          (LeftOf7 ?x)
          (LeftOf2 ?x)
          (LeftOf9 ?x)
          (LeftOf11 ?x)
          (LeftOf10 ?x)
        )
        (and
          (LeftOf10 ?x)
        )
      )
      (when
        (or
          (LeftOf8 ?x)
          (LeftOf4 ?x)
          (LeftOf3 ?x)
          (LeftOf5 ?x)
          (LeftOf6 ?x)
          (LeftOf12 ?x)
          (LeftOf1 ?x)
          (LeftOf7 ?x)
          (LeftOf2 ?x)
          (LeftOf9 ?x)
          (LeftOf11 ?x)
          (LeftOf10 ?x)
        )
        (and
          (LeftOf11 ?x)
        )
      )
      (when
        (or
          (LeftOf8 ?x)
          (LeftOf7 ?x)
          (LeftOf4 ?x)
          (LeftOf3 ?x)
          (LeftOf5 ?x)
          (LeftOf6 ?x)
          (LeftOf12 ?x)
          (LeftOf1 ?x)
          (LeftOf13 ?x)
          (LeftOf2 ?x)
          (LeftOf9 ?x)
          (LeftOf11 ?x)
          (LeftOf10 ?x)
        )
        (and
          (LeftOf12 ?x)
        )
      )
      (when
        (or
          (LeftOf8 ?x)
          (LeftOf7 ?x)
          (LeftOf4 ?x)
          (LeftOf3 ?x)
          (LeftOf14 ?x)
          (LeftOf5 ?x)
          (LeftOf6 ?x)
          (LeftOf12 ?x)
          (LeftOf1 ?x)
          (LeftOf13 ?x)
          (LeftOf2 ?x)
          (LeftOf9 ?x)
          (LeftOf11 ?x)
          (LeftOf10 ?x)
        )
        (and
          (LeftOf13 ?x)
        )
      )
      (when
        (or
          (LeftOf8 ?x)
          (LeftOf7 ?x)
          (LeftOf4 ?x)
          (LeftOf3 ?x)
          (LeftOf14 ?x)
          (LeftOf5 ?x)
          (LeftOf6 ?x)
          (LeftOf12 ?x)
          (LeftOf1 ?x)
          (LeftOf13 ?x)
          (LeftOf2 ?x)
          (LeftOf9 ?x)
          (LeftOf11 ?x)
          (LeftOf10 ?x)
          (LeftOf15 ?x)
        )
        (and
          (LeftOf14 ?x)
        )
      )
      (when
        (or
          (RightOf6 ?x)
          (RightOf9 ?x)
          (RightOf7 ?x)
          (RightOf12 ?x)
          (RightOf1 ?x)
          (RightOf4 ?x)
          (RightOf10 ?x)
          (RightOf11 ?x)
          (RightOf13 ?x)
          (RightOf14 ?x)
          (RightOf2 ?x)
          (RightOf5 ?x)
          (RightOf8 ?x)
          (RightOf3 ?x)
        )
        (and
          (RightOf0 ?x)
          (not (RightOf1 ?x))
        )
      )
      (when
        (or
          (RightOf6 ?x)
          (RightOf9 ?x)
          (RightOf7 ?x)
          (RightOf12 ?x)
          (RightOf8 ?x)
          (RightOf4 ?x)
          (RightOf10 ?x)
          (RightOf11 ?x)
          (RightOf13 ?x)
          (RightOf14 ?x)
          (RightOf2 ?x)
          (RightOf5 ?x)
          (RightOf3 ?x)
        )
        (and
          (RightOf1 ?x)
          (not (RightOf2 ?x))
        )
      )
      (when
        (or
          (RightOf6 ?x)
          (RightOf9 ?x)
          (RightOf7 ?x)
          (RightOf12 ?x)
          (RightOf8 ?x)
          (RightOf4 ?x)
          (RightOf10 ?x)
          (RightOf11 ?x)
          (RightOf13 ?x)
          (RightOf14 ?x)
          (RightOf5 ?x)
          (RightOf3 ?x)
        )
        (and
          (RightOf2 ?x)
          (not (RightOf3 ?x))
        )
      )
      (when
        (or
          (RightOf6 ?x)
          (RightOf9 ?x)
          (RightOf7 ?x)
          (RightOf12 ?x)
          (RightOf8 ?x)
          (RightOf4 ?x)
          (RightOf10 ?x)
          (RightOf11 ?x)
          (RightOf13 ?x)
          (RightOf14 ?x)
          (RightOf5 ?x)
        )
        (and
          (RightOf3 ?x)
          (not (RightOf4 ?x))
        )
      )
      (when
        (or
          (RightOf6 ?x)
          (RightOf9 ?x)
          (RightOf7 ?x)
          (RightOf12 ?x)
          (RightOf8 ?x)
          (RightOf10 ?x)
          (RightOf11 ?x)
          (RightOf13 ?x)
          (RightOf14 ?x)
          (RightOf5 ?x)
        )
        (and
          (RightOf4 ?x)
          (not (RightOf5 ?x))
        )
      )
      (when
        (or
          (RightOf6 ?x)
          (RightOf9 ?x)
          (RightOf7 ?x)
          (RightOf12 ?x)
          (RightOf8 ?x)
          (RightOf10 ?x)
          (RightOf11 ?x)
          (RightOf13 ?x)
          (RightOf14 ?x)
        )
        (and
          (RightOf5 ?x)
          (not (RightOf6 ?x))
        )
      )
      (when
        (or
          (RightOf12 ?x)
          (RightOf9 ?x)
          (RightOf7 ?x)
          (RightOf8 ?x)
          (RightOf10 ?x)
          (RightOf11 ?x)
          (RightOf13 ?x)
          (RightOf14 ?x)
        )
        (and
          (RightOf6 ?x)
          (not (RightOf7 ?x))
        )
      )
      (when
        (or
          (RightOf12 ?x)
          (RightOf9 ?x)
          (RightOf8 ?x)
          (RightOf10 ?x)
          (RightOf11 ?x)
          (RightOf13 ?x)
          (RightOf14 ?x)
        )
        (and
          (RightOf7 ?x)
          (not (RightOf8 ?x))
        )
      )
      (when
        (or
          (RightOf12 ?x)
          (RightOf9 ?x)
          (RightOf10 ?x)
          (RightOf11 ?x)
          (RightOf13 ?x)
          (RightOf14 ?x)
        )
        (and
          (RightOf8 ?x)
          (not (RightOf9 ?x))
        )
      )
      (when
        (or
          (RightOf11 ?x)
          (RightOf12 ?x)
          (RightOf13 ?x)
          (RightOf14 ?x)
          (RightOf10 ?x)
        )
        (and
          (RightOf9 ?x)
          (not (RightOf10 ?x))
        )
      )
      (when
        (or
          (RightOf11 ?x)
          (RightOf12 ?x)
          (RightOf13 ?x)
          (RightOf14 ?x)
        )
        (and
          (RightOf10 ?x)
          (not (RightOf11 ?x))
        )
      )
      (when
        (or
          (RightOf12 ?x)
          (RightOf13 ?x)
          (RightOf14 ?x)
        )
        (and
          (RightOf11 ?x)
          (not (RightOf12 ?x))
        )
      )
      (when
        (or
          (RightOf13 ?x)
          (RightOf14 ?x)
        )
        (and
          (RightOf12 ?x)
          (not (RightOf13 ?x))
        )
      )
      (when
        (RightOf14 ?x)
        (and
          (RightOf13 ?x)
          (not (RightOf14 ?x))
        )
      )
      (when
        (Column1 ?x)
        (and
          (Column0 ?x)
          (not (Column1 ?x))
        )
      )
      (when
        (Column2 ?x)
        (and
          (Column1 ?x)
          (not (Column2 ?x))
        )
      )
      (when
        (Column3 ?x)
        (and
          (Column2 ?x)
          (not (Column3 ?x))
        )
      )
      (when
        (Column4 ?x)
        (and
          (Column3 ?x)
          (not (Column4 ?x))
        )
      )
      (when
        (Column5 ?x)
        (and
          (Column4 ?x)
          (not (Column5 ?x))
        )
      )
      (when
        (Column6 ?x)
        (and
          (Column5 ?x)
          (not (Column6 ?x))
        )
      )
      (when
        (Column7 ?x)
        (and
          (Column6 ?x)
          (not (Column7 ?x))
        )
      )
      (when
        (Column8 ?x)
        (and
          (Column7 ?x)
          (not (Column8 ?x))
        )
      )
      (when
        (Column9 ?x)
        (and
          (Column8 ?x)
          (not (Column9 ?x))
        )
      )
      (when
        (Column10 ?x)
        (and
          (Column9 ?x)
          (not (Column10 ?x))
        )
      )
      (when
        (Column11 ?x)
        (and
          (Column10 ?x)
          (not (Column11 ?x))
        )
      )
      (when
        (Column12 ?x)
        (and
          (Column11 ?x)
          (not (Column12 ?x))
        )
      )
      (when
        (Column13 ?x)
        (and
          (Column12 ?x)
          (not (Column13 ?x))
        )
      )
      (when
        (Column14 ?x)
        (and
          (Column13 ?x)
          (not (Column14 ?x))
        )
      )
      (when
        (or
          (and
            (RightOf4 ?x)
            (LeftOf2 ?x)
          )
          (and
            (RightOf6 ?x)
            (LeftOf2 ?x)
          )
          (and
            (RightOf14 ?x)
            (LeftOf1 ?x)
          )
          (and
            (RightOf6 ?x)
            (LeftOf1 ?x)
          )
          (and
            (RightOf2 ?x)
            (LeftOf1 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf2 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf8 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf11 ?x)
          )
          (and
            (LeftOf1 ?x)
            (RightOf13 ?x)
          )
          (and
            (LeftOf1 ?x)
            (RightOf8 ?x)
          )
          (and
            (RightOf5 ?x)
            (LeftOf1 ?x)
          )
          (and
            (RightOf12 ?x)
            (LeftOf2 ?x)
          )
          (and
            (RightOf7 ?x)
            (LeftOf1 ?x)
          )
          (and
            (RightOf12 ?x)
            (LeftOf1 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf10 ?x)
          )
          (and
            (RightOf4 ?x)
            (LeftOf1 ?x)
          )
          (and
            (RightOf14 ?x)
            (LeftOf2 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf3 ?x)
          )
          (and
            (LeftOf1 ?x)
            (RightOf11 ?x)
          )
          (and
            (LeftOf1 ?x)
            (RightOf5 ?x)
          )
          (and
            (RightOf7 ?x)
            (LeftOf2 ?x)
          )
          (and
            (RightOf1 ?x)
            (LeftOf1 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf5 ?x)
          )
          (and
            (RightOf1 ?x)
            (LeftOf2 ?x)
          )
          (and
            (LeftOf1 ?x)
            (RightOf10 ?x)
          )
          (and
            (LeftOf1 ?x)
            (RightOf3 ?x)
          )
          (and
            (RightOf9 ?x)
            (LeftOf1 ?x)
          )
          (and
            (RightOf9 ?x)
            (LeftOf2 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf13 ?x)
          )
        )
        (and
          (Column0 ?x)
        )
      )
      (when
        (or
          (and
            (RightOf2 ?x)
            (LeftOf3 ?x)
          )
          (and
            (RightOf4 ?x)
            (LeftOf2 ?x)
          )
          (and
            (RightOf9 ?x)
            (LeftOf3 ?x)
          )
          (and
            (RightOf6 ?x)
            (LeftOf2 ?x)
          )
          (and
            (RightOf14 ?x)
            (LeftOf1 ?x)
          )
          (and
            (RightOf6 ?x)
            (LeftOf1 ?x)
          )
          (and
            (RightOf2 ?x)
            (LeftOf1 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf2 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf8 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf11 ?x)
          )
          (and
            (LeftOf1 ?x)
            (RightOf13 ?x)
          )
          (and
            (LeftOf3 ?x)
            (RightOf13 ?x)
          )
          (and
            (RightOf11 ?x)
            (LeftOf3 ?x)
          )
          (and
            (LeftOf3 ?x)
            (RightOf3 ?x)
          )
          (and
            (LeftOf1 ?x)
            (RightOf8 ?x)
          )
          (and
            (RightOf5 ?x)
            (LeftOf3 ?x)
          )
          (and
            (RightOf12 ?x)
            (LeftOf2 ?x)
          )
          (and
            (RightOf7 ?x)
            (LeftOf2 ?x)
          )
          (and
            (RightOf8 ?x)
            (LeftOf3 ?x)
          )
          (and
            (RightOf10 ?x)
            (LeftOf3 ?x)
          )
          (and
            (RightOf12 ?x)
            (LeftOf1 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf10 ?x)
          )
          (and
            (RightOf4 ?x)
            (LeftOf1 ?x)
          )
          (and
            (RightOf14 ?x)
            (LeftOf2 ?x)
          )
          (and
            (RightOf12 ?x)
            (LeftOf3 ?x)
          )
          (and
            (RightOf5 ?x)
            (LeftOf1 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf3 ?x)
          )
          (and
            (LeftOf1 ?x)
            (RightOf11 ?x)
          )
          (and
            (RightOf7 ?x)
            (LeftOf3 ?x)
          )
          (and
            (LeftOf1 ?x)
            (RightOf5 ?x)
          )
          (and
            (RightOf7 ?x)
            (LeftOf1 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf5 ?x)
          )
          (and
            (LeftOf1 ?x)
            (RightOf10 ?x)
          )
          (and
            (RightOf4 ?x)
            (LeftOf3 ?x)
          )
          (and
            (RightOf14 ?x)
            (LeftOf3 ?x)
          )
          (and
            (LeftOf1 ?x)
            (RightOf3 ?x)
          )
          (and
            (RightOf9 ?x)
            (LeftOf1 ?x)
          )
          (and
            (RightOf6 ?x)
            (LeftOf3 ?x)
          )
          (and
            (RightOf9 ?x)
            (LeftOf2 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf13 ?x)
          )
        )
        (and
          (Column1 ?x)
        )
      )
      (when
        (or
          (and
            (RightOf4 ?x)
            (LeftOf2 ?x)
          )
          (and
            (RightOf9 ?x)
            (LeftOf3 ?x)
          )
          (and
            (RightOf6 ?x)
            (LeftOf2 ?x)
          )
          (and
            (RightOf11 ?x)
            (LeftOf4 ?x)
          )
          (and
            (LeftOf4 ?x)
            (RightOf3 ?x)
          )
          (and
            (RightOf6 ?x)
            (LeftOf1 ?x)
          )
          (and
            (LeftOf4 ?x)
            (RightOf13 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf8 ?x)
          )
          (and
            (RightOf5 ?x)
            (LeftOf4 ?x)
          )
          (and
            (RightOf6 ?x)
            (LeftOf4 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf11 ?x)
          )
          (and
            (LeftOf1 ?x)
            (RightOf13 ?x)
          )
          (and
            (RightOf9 ?x)
            (LeftOf4 ?x)
          )
          (and
            (LeftOf3 ?x)
            (RightOf13 ?x)
          )
          (and
            (LeftOf4 ?x)
            (RightOf10 ?x)
          )
          (and
            (RightOf12 ?x)
            (LeftOf4 ?x)
          )
          (and
            (RightOf11 ?x)
            (LeftOf3 ?x)
          )
          (and
            (LeftOf1 ?x)
            (RightOf8 ?x)
          )
          (and
            (RightOf14 ?x)
            (LeftOf1 ?x)
          )
          (and
            (RightOf5 ?x)
            (LeftOf3 ?x)
          )
          (and
            (LeftOf4 ?x)
            (RightOf8 ?x)
          )
          (and
            (RightOf12 ?x)
            (LeftOf2 ?x)
          )
          (and
            (RightOf7 ?x)
            (LeftOf2 ?x)
          )
          (and
            (RightOf3 ?x)
            (LeftOf3 ?x)
          )
          (and
            (RightOf10 ?x)
            (LeftOf3 ?x)
          )
          (and
            (RightOf12 ?x)
            (LeftOf1 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf10 ?x)
          )
          (and
            (RightOf14 ?x)
            (LeftOf4 ?x)
          )
          (and
            (RightOf4 ?x)
            (LeftOf1 ?x)
          )
          (and
            (RightOf14 ?x)
            (LeftOf2 ?x)
          )
          (and
            (RightOf12 ?x)
            (LeftOf3 ?x)
          )
          (and
            (RightOf5 ?x)
            (LeftOf1 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf3 ?x)
          )
          (and
            (LeftOf1 ?x)
            (RightOf11 ?x)
          )
          (and
            (RightOf7 ?x)
            (LeftOf3 ?x)
          )
          (and
            (LeftOf1 ?x)
            (RightOf5 ?x)
          )
          (and
            (RightOf7 ?x)
            (LeftOf1 ?x)
          )
          (and
            (RightOf8 ?x)
            (LeftOf3 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf5 ?x)
          )
          (and
            (RightOf7 ?x)
            (LeftOf4 ?x)
          )
          (and
            (RightOf4 ?x)
            (LeftOf4 ?x)
          )
          (and
            (LeftOf1 ?x)
            (RightOf10 ?x)
          )
          (and
            (RightOf4 ?x)
            (LeftOf3 ?x)
          )
          (and
            (RightOf13 ?x)
            (LeftOf3 ?x)
          )
          (and
            (RightOf14 ?x)
            (LeftOf3 ?x)
          )
          (and
            (LeftOf1 ?x)
            (RightOf3 ?x)
          )
          (and
            (RightOf9 ?x)
            (LeftOf1 ?x)
          )
          (and
            (RightOf6 ?x)
            (LeftOf3 ?x)
          )
          (and
            (RightOf9 ?x)
            (LeftOf2 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf13 ?x)
          )
        )
        (and
          (Column2 ?x)
        )
      )
      (when
        (or
          (and
            (RightOf4 ?x)
            (LeftOf2 ?x)
          )
          (and
            (RightOf9 ?x)
            (LeftOf3 ?x)
          )
          (and
            (RightOf6 ?x)
            (LeftOf2 ?x)
          )
          (and
            (RightOf11 ?x)
            (LeftOf4 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf5 ?x)
          )
          (and
            (RightOf6 ?x)
            (LeftOf1 ?x)
          )
          (and
            (RightOf5 ?x)
            (LeftOf3 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf14 ?x)
          )
          (and
            (LeftOf4 ?x)
            (RightOf13 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf8 ?x)
          )
          (and
            (RightOf5 ?x)
            (LeftOf4 ?x)
          )
          (and
            (RightOf6 ?x)
            (LeftOf4 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf11 ?x)
          )
          (and
            (LeftOf1 ?x)
            (RightOf13 ?x)
          )
          (and
            (RightOf9 ?x)
            (LeftOf4 ?x)
          )
          (and
            (LeftOf3 ?x)
            (RightOf13 ?x)
          )
          (and
            (LeftOf4 ?x)
            (RightOf10 ?x)
          )
          (and
            (RightOf12 ?x)
            (LeftOf4 ?x)
          )
          (and
            (RightOf11 ?x)
            (LeftOf3 ?x)
          )
          (and
            (RightOf14 ?x)
            (LeftOf2 ?x)
          )
          (and
            (RightOf12 ?x)
            (LeftOf3 ?x)
          )
          (and
            (RightOf5 ?x)
            (LeftOf1 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf8 ?x)
          )
          (and
            (RightOf12 ?x)
            (LeftOf5 ?x)
          )
          (and
            (LeftOf4 ?x)
            (RightOf8 ?x)
          )
          (and
            (RightOf12 ?x)
            (LeftOf2 ?x)
          )
          (and
            (RightOf7 ?x)
            (LeftOf2 ?x)
          )
          (and
            (RightOf10 ?x)
            (LeftOf4 ?x)
          )
          (and
            (RightOf10 ?x)
            (LeftOf3 ?x)
          )
          (and
            (RightOf12 ?x)
            (LeftOf1 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf10 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf10 ?x)
          )
          (and
            (RightOf4 ?x)
            (LeftOf1 ?x)
          )
          (and
            (RightOf6 ?x)
            (LeftOf5 ?x)
          )
          (and
            (RightOf8 ?x)
            (LeftOf4 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf4 ?x)
          )
          (and
            (RightOf14 ?x)
            (LeftOf4 ?x)
          )
          (and
            (LeftOf1 ?x)
            (RightOf11 ?x)
          )
          (and
            (RightOf7 ?x)
            (LeftOf3 ?x)
          )
          (and
            (LeftOf1 ?x)
            (RightOf5 ?x)
          )
          (and
            (RightOf7 ?x)
            (LeftOf1 ?x)
          )
          (and
            (RightOf8 ?x)
            (LeftOf3 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf5 ?x)
          )
          (and
            (RightOf7 ?x)
            (LeftOf4 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf13 ?x)
          )
          (and
            (RightOf4 ?x)
            (LeftOf4 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf11 ?x)
          )
          (and
            (LeftOf1 ?x)
            (RightOf8 ?x)
          )
          (and
            (LeftOf1 ?x)
            (RightOf10 ?x)
          )
          (and
            (RightOf4 ?x)
            (LeftOf3 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf7 ?x)
          )
          (and
            (RightOf13 ?x)
            (LeftOf3 ?x)
          )
          (and
            (RightOf14 ?x)
            (LeftOf1 ?x)
          )
          (and
            (RightOf14 ?x)
            (LeftOf3 ?x)
          )
          (and
            (RightOf9 ?x)
            (LeftOf1 ?x)
          )
          (and
            (RightOf6 ?x)
            (LeftOf3 ?x)
          )
          (and
            (RightOf9 ?x)
            (LeftOf2 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf13 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf9 ?x)
          )
        )
        (and
          (Column3 ?x)
        )
      )
      (when
        (or
          (and
            (LeftOf6 ?x)
            (RightOf9 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf11 ?x)
          )
          (and
            (RightOf6 ?x)
            (LeftOf2 ?x)
          )
          (and
            (RightOf11 ?x)
            (LeftOf4 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf5 ?x)
          )
          (and
            (RightOf6 ?x)
            (LeftOf1 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf14 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf14 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf8 ?x)
          )
          (and
            (RightOf5 ?x)
            (LeftOf4 ?x)
          )
          (and
            (RightOf6 ?x)
            (LeftOf4 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf13 ?x)
          )
          (and
            (LeftOf1 ?x)
            (RightOf13 ?x)
          )
          (and
            (RightOf9 ?x)
            (LeftOf4 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf6 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf10 ?x)
          )
          (and
            (LeftOf3 ?x)
            (RightOf13 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf7 ?x)
          )
          (and
            (LeftOf4 ?x)
            (RightOf10 ?x)
          )
          (and
            (RightOf12 ?x)
            (LeftOf4 ?x)
          )
          (and
            (RightOf11 ?x)
            (LeftOf3 ?x)
          )
          (and
            (LeftOf1 ?x)
            (RightOf8 ?x)
          )
          (and
            (RightOf14 ?x)
            (LeftOf1 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf5 ?x)
          )
          (and
            (RightOf5 ?x)
            (LeftOf3 ?x)
          )
          (and
            (RightOf14 ?x)
            (LeftOf2 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf8 ?x)
          )
          (and
            (RightOf12 ?x)
            (LeftOf5 ?x)
          )
          (and
            (LeftOf4 ?x)
            (RightOf8 ?x)
          )
          (and
            (RightOf12 ?x)
            (LeftOf2 ?x)
          )
          (and
            (RightOf7 ?x)
            (LeftOf1 ?x)
          )
          (and
            (RightOf10 ?x)
            (LeftOf4 ?x)
          )
          (and
            (RightOf10 ?x)
            (LeftOf3 ?x)
          )
          (and
            (RightOf12 ?x)
            (LeftOf1 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf10 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf10 ?x)
          )
          (and
            (RightOf6 ?x)
            (LeftOf5 ?x)
          )
          (and
            (RightOf8 ?x)
            (LeftOf4 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf11 ?x)
          )
          (and
            (RightOf12 ?x)
            (LeftOf3 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf8 ?x)
          )
          (and
            (RightOf14 ?x)
            (LeftOf4 ?x)
          )
          (and
            (LeftOf1 ?x)
            (RightOf11 ?x)
          )
          (and
            (RightOf7 ?x)
            (LeftOf3 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf12 ?x)
          )
          (and
            (LeftOf4 ?x)
            (RightOf13 ?x)
          )
          (and
            (RightOf7 ?x)
            (LeftOf2 ?x)
          )
          (and
            (LeftOf1 ?x)
            (RightOf5 ?x)
          )
          (and
            (RightOf8 ?x)
            (LeftOf3 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf5 ?x)
          )
          (and
            (RightOf7 ?x)
            (LeftOf4 ?x)
          )
          (and
            (RightOf6 ?x)
            (LeftOf3 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf13 ?x)
          )
          (and
            (LeftOf1 ?x)
            (RightOf10 ?x)
          )
          (and
            (RightOf9 ?x)
            (LeftOf3 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf11 ?x)
          )
          (and
            (RightOf13 ?x)
            (LeftOf3 ?x)
          )
          (and
            (RightOf14 ?x)
            (LeftOf3 ?x)
          )
          (and
            (RightOf9 ?x)
            (LeftOf1 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf7 ?x)
          )
          (and
            (RightOf9 ?x)
            (LeftOf2 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf13 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf9 ?x)
          )
        )
        (and
          (Column4 ?x)
        )
      )
      (when
        (or
          (and
            (LeftOf6 ?x)
            (RightOf9 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf11 ?x)
          )
          (and
            (RightOf6 ?x)
            (LeftOf2 ?x)
          )
          (and
            (RightOf11 ?x)
            (LeftOf4 ?x)
          )
          (and
            (RightOf14 ?x)
            (LeftOf1 ?x)
          )
          (and
            (RightOf6 ?x)
            (LeftOf1 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf14 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf14 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf8 ?x)
          )
          (and
            (LeftOf7 ?x)
            (RightOf10 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf13 ?x)
          )
          (and
            (LeftOf1 ?x)
            (RightOf13 ?x)
          )
          (and
            (RightOf9 ?x)
            (LeftOf4 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf10 ?x)
          )
          (and
            (RightOf12 ?x)
            (LeftOf6 ?x)
          )
          (and
            (RightOf6 ?x)
            (LeftOf7 ?x)
          )
          (and
            (RightOf9 ?x)
            (LeftOf7 ?x)
          )
          (and
            (LeftOf3 ?x)
            (RightOf13 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf7 ?x)
          )
          (and
            (LeftOf4 ?x)
            (RightOf10 ?x)
          )
          (and
            (RightOf12 ?x)
            (LeftOf4 ?x)
          )
          (and
            (RightOf11 ?x)
            (LeftOf3 ?x)
          )
          (and
            (LeftOf1 ?x)
            (RightOf8 ?x)
          )
          (and
            (LeftOf4 ?x)
            (RightOf8 ?x)
          )
          (and
            (RightOf14 ?x)
            (LeftOf2 ?x)
          )
          (and
            (RightOf14 ?x)
            (LeftOf7 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf8 ?x)
          )
          (and
            (RightOf12 ?x)
            (LeftOf5 ?x)
          )
          (and
            (RightOf12 ?x)
            (LeftOf7 ?x)
          )
          (and
            (RightOf12 ?x)
            (LeftOf2 ?x)
          )
          (and
            (RightOf7 ?x)
            (LeftOf1 ?x)
          )
          (and
            (RightOf8 ?x)
            (LeftOf3 ?x)
          )
          (and
            (RightOf10 ?x)
            (LeftOf3 ?x)
          )
          (and
            (RightOf6 ?x)
            (LeftOf6 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf10 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf10 ?x)
          )
          (and
            (RightOf6 ?x)
            (LeftOf5 ?x)
          )
          (and
            (RightOf8 ?x)
            (LeftOf4 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf11 ?x)
          )
          (and
            (RightOf12 ?x)
            (LeftOf3 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf8 ?x)
          )
          (and
            (RightOf14 ?x)
            (LeftOf4 ?x)
          )
          (and
            (LeftOf1 ?x)
            (RightOf11 ?x)
          )
          (and
            (RightOf7 ?x)
            (LeftOf3 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf12 ?x)
          )
          (and
            (LeftOf4 ?x)
            (RightOf13 ?x)
          )
          (and
            (RightOf14 ?x)
            (LeftOf3 ?x)
          )
          (and
            (RightOf7 ?x)
            (LeftOf2 ?x)
          )
          (and
            (LeftOf7 ?x)
            (RightOf13 ?x)
          )
          (and
            (RightOf10 ?x)
            (LeftOf4 ?x)
          )
          (and
            (RightOf7 ?x)
            (LeftOf4 ?x)
          )
          (and
            (RightOf6 ?x)
            (LeftOf3 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf13 ?x)
          )
          (and
            (RightOf12 ?x)
            (LeftOf1 ?x)
          )
          (and
            (LeftOf1 ?x)
            (RightOf10 ?x)
          )
          (and
            (RightOf9 ?x)
            (LeftOf3 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf11 ?x)
          )
          (and
            (RightOf13 ?x)
            (LeftOf3 ?x)
          )
          (and
            (RightOf7 ?x)
            (LeftOf7 ?x)
          )
          (and
            (LeftOf7 ?x)
            (RightOf8 ?x)
          )
          (and
            (RightOf6 ?x)
            (LeftOf4 ?x)
          )
          (and
            (LeftOf7 ?x)
            (RightOf11 ?x)
          )
          (and
            (RightOf9 ?x)
            (LeftOf1 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf7 ?x)
          )
          (and
            (RightOf9 ?x)
            (LeftOf2 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf13 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf9 ?x)
          )
        )
        (and
          (Column5 ?x)
        )
      )
      (when
        (or
          (and
            (LeftOf6 ?x)
            (RightOf9 ?x)
          )
          (and
            (RightOf9 ?x)
            (LeftOf3 ?x)
          )
          (and
            (RightOf11 ?x)
            (LeftOf4 ?x)
          )
          (and
            (RightOf14 ?x)
            (LeftOf1 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf14 ?x)
          )
          (and
            (LeftOf8 ?x)
            (RightOf11 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf14 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf8 ?x)
          )
          (and
            (LeftOf7 ?x)
            (RightOf10 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf13 ?x)
          )
          (and
            (LeftOf1 ?x)
            (RightOf13 ?x)
          )
          (and
            (RightOf9 ?x)
            (LeftOf4 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf10 ?x)
          )
          (and
            (RightOf12 ?x)
            (LeftOf6 ?x)
          )
          (and
            (RightOf9 ?x)
            (LeftOf7 ?x)
          )
          (and
            (LeftOf3 ?x)
            (RightOf13 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf7 ?x)
          )
          (and
            (LeftOf4 ?x)
            (RightOf10 ?x)
          )
          (and
            (RightOf12 ?x)
            (LeftOf4 ?x)
          )
          (and
            (RightOf11 ?x)
            (LeftOf3 ?x)
          )
          (and
            (LeftOf1 ?x)
            (RightOf8 ?x)
          )
          (and
            (LeftOf4 ?x)
            (RightOf8 ?x)
          )
          (and
            (RightOf14 ?x)
            (LeftOf2 ?x)
          )
          (and
            (RightOf14 ?x)
            (LeftOf7 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf8 ?x)
          )
          (and
            (RightOf12 ?x)
            (LeftOf5 ?x)
          )
          (and
            (RightOf12 ?x)
            (LeftOf7 ?x)
          )
          (and
            (RightOf12 ?x)
            (LeftOf2 ?x)
          )
          (and
            (RightOf7 ?x)
            (LeftOf2 ?x)
          )
          (and
            (RightOf8 ?x)
            (LeftOf3 ?x)
          )
          (and
            (RightOf10 ?x)
            (LeftOf3 ?x)
          )
          (and
            (RightOf12 ?x)
            (LeftOf1 ?x)
          )
          (and
            (RightOf12 ?x)
            (LeftOf8 ?x)
          )
          (and
            (LeftOf7 ?x)
            (RightOf7 ?x)
          )
          (and
            (LeftOf8 ?x)
            (RightOf8 ?x)
          )
          (and
            (LeftOf8 ?x)
            (RightOf10 ?x)
          )
          (and
            (RightOf8 ?x)
            (LeftOf4 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf11 ?x)
          )
          (and
            (RightOf12 ?x)
            (LeftOf3 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf8 ?x)
          )
          (and
            (RightOf14 ?x)
            (LeftOf4 ?x)
          )
          (and
            (LeftOf1 ?x)
            (RightOf11 ?x)
          )
          (and
            (RightOf7 ?x)
            (LeftOf3 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf12 ?x)
          )
          (and
            (LeftOf4 ?x)
            (RightOf13 ?x)
          )
          (and
            (RightOf7 ?x)
            (LeftOf1 ?x)
          )
          (and
            (LeftOf7 ?x)
            (RightOf13 ?x)
          )
          (and
            (RightOf10 ?x)
            (LeftOf4 ?x)
          )
          (and
            (LeftOf8 ?x)
            (RightOf14 ?x)
          )
          (and
            (RightOf7 ?x)
            (LeftOf4 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf13 ?x)
          )
          (and
            (LeftOf8 ?x)
            (RightOf13 ?x)
          )
          (and
            (RightOf9 ?x)
            (LeftOf2 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf11 ?x)
          )
          (and
            (LeftOf1 ?x)
            (RightOf10 ?x)
          )
          (and
            (RightOf9 ?x)
            (LeftOf8 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf10 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf11 ?x)
          )
          (and
            (RightOf13 ?x)
            (LeftOf3 ?x)
          )
          (and
            (LeftOf7 ?x)
            (RightOf8 ?x)
          )
          (and
            (RightOf14 ?x)
            (LeftOf3 ?x)
          )
          (and
            (LeftOf7 ?x)
            (RightOf11 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf10 ?x)
          )
          (and
            (RightOf9 ?x)
            (LeftOf1 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf7 ?x)
          )
          (and
            (LeftOf8 ?x)
            (RightOf7 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf13 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf9 ?x)
          )
        )
        (and
          (Column6 ?x)
        )
      )
      (when
        (or
          (and
            (LeftOf6 ?x)
            (RightOf9 ?x)
          )
          (and
            (RightOf9 ?x)
            (LeftOf3 ?x)
          )
          (and
            (RightOf14 ?x)
            (LeftOf9 ?x)
          )
          (and
            (RightOf14 ?x)
            (LeftOf1 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf14 ?x)
          )
          (and
            (LeftOf8 ?x)
            (RightOf11 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf14 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf8 ?x)
          )
          (and
            (LeftOf7 ?x)
            (RightOf10 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf13 ?x)
          )
          (and
            (LeftOf1 ?x)
            (RightOf13 ?x)
          )
          (and
            (RightOf9 ?x)
            (LeftOf4 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf10 ?x)
          )
          (and
            (RightOf12 ?x)
            (LeftOf6 ?x)
          )
          (and
            (RightOf9 ?x)
            (LeftOf7 ?x)
          )
          (and
            (LeftOf3 ?x)
            (RightOf13 ?x)
          )
          (and
            (LeftOf4 ?x)
            (RightOf10 ?x)
          )
          (and
            (RightOf12 ?x)
            (LeftOf4 ?x)
          )
          (and
            (RightOf11 ?x)
            (LeftOf3 ?x)
          )
          (and
            (LeftOf1 ?x)
            (RightOf8 ?x)
          )
          (and
            (LeftOf9 ?x)
            (RightOf11 ?x)
          )
          (and
            (RightOf14 ?x)
            (LeftOf2 ?x)
          )
          (and
            (RightOf14 ?x)
            (LeftOf7 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf8 ?x)
          )
          (and
            (RightOf12 ?x)
            (LeftOf9 ?x)
          )
          (and
            (RightOf12 ?x)
            (LeftOf5 ?x)
          )
          (and
            (RightOf12 ?x)
            (LeftOf7 ?x)
          )
          (and
            (RightOf12 ?x)
            (LeftOf2 ?x)
          )
          (and
            (RightOf14 ?x)
            (LeftOf8 ?x)
          )
          (and
            (RightOf10 ?x)
            (LeftOf4 ?x)
          )
          (and
            (RightOf10 ?x)
            (LeftOf3 ?x)
          )
          (and
            (RightOf12 ?x)
            (LeftOf1 ?x)
          )
          (and
            (RightOf12 ?x)
            (LeftOf8 ?x)
          )
          (and
            (LeftOf8 ?x)
            (RightOf8 ?x)
          )
          (and
            (LeftOf8 ?x)
            (RightOf10 ?x)
          )
          (and
            (LeftOf9 ?x)
            (RightOf10 ?x)
          )
          (and
            (RightOf8 ?x)
            (LeftOf4 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf11 ?x)
          )
          (and
            (RightOf12 ?x)
            (LeftOf3 ?x)
          )
          (and
            (RightOf14 ?x)
            (LeftOf4 ?x)
          )
          (and
            (LeftOf1 ?x)
            (RightOf11 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf8 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf12 ?x)
          )
          (and
            (LeftOf4 ?x)
            (RightOf13 ?x)
          )
          (and
            (LeftOf7 ?x)
            (RightOf13 ?x)
          )
          (and
            (RightOf9 ?x)
            (LeftOf9 ?x)
          )
          (and
            (RightOf8 ?x)
            (LeftOf3 ?x)
          )
          (and
            (LeftOf8 ?x)
            (RightOf14 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf13 ?x)
          )
          (and
            (LeftOf8 ?x)
            (RightOf13 ?x)
          )
          (and
            (LeftOf9 ?x)
            (RightOf8 ?x)
          )
          (and
            (LeftOf9 ?x)
            (RightOf13 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf11 ?x)
          )
          (and
            (LeftOf1 ?x)
            (RightOf10 ?x)
          )
          (and
            (RightOf9 ?x)
            (LeftOf8 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf10 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf11 ?x)
          )
          (and
            (RightOf13 ?x)
            (LeftOf3 ?x)
          )
          (and
            (LeftOf7 ?x)
            (RightOf8 ?x)
          )
          (and
            (RightOf14 ?x)
            (LeftOf3 ?x)
          )
          (and
            (LeftOf7 ?x)
            (RightOf11 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf10 ?x)
          )
          (and
            (RightOf9 ?x)
            (LeftOf1 ?x)
          )
          (and
            (RightOf9 ?x)
            (LeftOf2 ?x)
          )
          (and
            (RightOf11 ?x)
            (LeftOf4 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf13 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf9 ?x)
          )
        )
        (and
          (Column7 ?x)
        )
      )
      (when
        (or
          (and
            (LeftOf6 ?x)
            (RightOf9 ?x)
          )
          (and
            (RightOf9 ?x)
            (LeftOf3 ?x)
          )
          (and
            (RightOf14 ?x)
            (LeftOf9 ?x)
          )
          (and
            (RightOf14 ?x)
            (LeftOf1 ?x)
          )
          (and
            (LeftOf8 ?x)
            (RightOf11 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf14 ?x)
          )
          (and
            (LeftOf7 ?x)
            (RightOf10 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf13 ?x)
          )
          (and
            (LeftOf1 ?x)
            (RightOf13 ?x)
          )
          (and
            (LeftOf7 ?x)
            (RightOf11 ?x)
          )
          (and
            (RightOf9 ?x)
            (LeftOf4 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf10 ?x)
          )
          (and
            (RightOf12 ?x)
            (LeftOf6 ?x)
          )
          (and
            (RightOf9 ?x)
            (LeftOf7 ?x)
          )
          (and
            (LeftOf3 ?x)
            (RightOf13 ?x)
          )
          (and
            (LeftOf4 ?x)
            (RightOf10 ?x)
          )
          (and
            (RightOf12 ?x)
            (LeftOf4 ?x)
          )
          (and
            (RightOf11 ?x)
            (LeftOf3 ?x)
          )
          (and
            (RightOf14 ?x)
            (LeftOf2 ?x)
          )
          (and
            (RightOf12 ?x)
            (LeftOf9 ?x)
          )
          (and
            (RightOf14 ?x)
            (LeftOf7 ?x)
          )
          (and
            (LeftOf9 ?x)
            (RightOf11 ?x)
          )
          (and
            (RightOf12 ?x)
            (LeftOf5 ?x)
          )
          (and
            (RightOf12 ?x)
            (LeftOf7 ?x)
          )
          (and
            (RightOf12 ?x)
            (LeftOf2 ?x)
          )
          (and
            (RightOf14 ?x)
            (LeftOf8 ?x)
          )
          (and
            (RightOf10 ?x)
            (LeftOf4 ?x)
          )
          (and
            (RightOf10 ?x)
            (LeftOf3 ?x)
          )
          (and
            (RightOf12 ?x)
            (LeftOf1 ?x)
          )
          (and
            (RightOf12 ?x)
            (LeftOf8 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf10 ?x)
          )
          (and
            (LeftOf8 ?x)
            (RightOf10 ?x)
          )
          (and
            (LeftOf9 ?x)
            (RightOf10 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf11 ?x)
          )
          (and
            (RightOf12 ?x)
            (LeftOf3 ?x)
          )
          (and
            (RightOf14 ?x)
            (LeftOf4 ?x)
          )
          (and
            (LeftOf1 ?x)
            (RightOf11 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf12 ?x)
          )
          (and
            (LeftOf4 ?x)
            (RightOf13 ?x)
          )
          (and
            (RightOf14 ?x)
            (LeftOf3 ?x)
          )
          (and
            (RightOf9 ?x)
            (LeftOf10 ?x)
          )
          (and
            (RightOf9 ?x)
            (LeftOf9 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf14 ?x)
          )
          (and
            (LeftOf8 ?x)
            (RightOf14 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf13 ?x)
          )
          (and
            (LeftOf8 ?x)
            (RightOf13 ?x)
          )
          (and
            (LeftOf9 ?x)
            (RightOf13 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf11 ?x)
          )
          (and
            (LeftOf1 ?x)
            (RightOf10 ?x)
          )
          (and
            (LeftOf10 ?x)
            (RightOf13 ?x)
          )
          (and
            (RightOf9 ?x)
            (LeftOf8 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf10 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf11 ?x)
          )
          (and
            (RightOf13 ?x)
            (LeftOf3 ?x)
          )
          (and
            (LeftOf10 ?x)
            (RightOf10 ?x)
          )
          (and
            (LeftOf10 ?x)
            (RightOf11 ?x)
          )
          (and
            (RightOf12 ?x)
            (LeftOf10 ?x)
          )
          (and
            (LeftOf7 ?x)
            (RightOf13 ?x)
          )
          (and
            (LeftOf10 ?x)
            (RightOf14 ?x)
          )
          (and
            (RightOf9 ?x)
            (LeftOf1 ?x)
          )
          (and
            (RightOf9 ?x)
            (LeftOf2 ?x)
          )
          (and
            (RightOf11 ?x)
            (LeftOf4 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf13 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf9 ?x)
          )
        )
        (and
          (Column8 ?x)
        )
      )
      (when
        (or
          (and
            (LeftOf6 ?x)
            (RightOf14 ?x)
          )
          (and
            (RightOf12 ?x)
            (LeftOf11 ?x)
          )
          (and
            (RightOf14 ?x)
            (LeftOf9 ?x)
          )
          (and
            (RightOf14 ?x)
            (LeftOf1 ?x)
          )
          (and
            (LeftOf8 ?x)
            (RightOf11 ?x)
          )
          (and
            (LeftOf4 ?x)
            (RightOf13 ?x)
          )
          (and
            (LeftOf11 ?x)
            (RightOf10 ?x)
          )
          (and
            (LeftOf7 ?x)
            (RightOf10 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf13 ?x)
          )
          (and
            (LeftOf1 ?x)
            (RightOf13 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf10 ?x)
          )
          (and
            (RightOf12 ?x)
            (LeftOf6 ?x)
          )
          (and
            (LeftOf3 ?x)
            (RightOf13 ?x)
          )
          (and
            (RightOf11 ?x)
            (LeftOf4 ?x)
          )
          (and
            (RightOf12 ?x)
            (LeftOf4 ?x)
          )
          (and
            (RightOf11 ?x)
            (LeftOf3 ?x)
          )
          (and
            (RightOf14 ?x)
            (LeftOf2 ?x)
          )
          (and
            (LeftOf9 ?x)
            (RightOf11 ?x)
          )
          (and
            (LeftOf11 ?x)
            (RightOf13 ?x)
          )
          (and
            (RightOf14 ?x)
            (LeftOf7 ?x)
          )
          (and
            (RightOf12 ?x)
            (LeftOf9 ?x)
          )
          (and
            (RightOf12 ?x)
            (LeftOf5 ?x)
          )
          (and
            (RightOf12 ?x)
            (LeftOf7 ?x)
          )
          (and
            (RightOf12 ?x)
            (LeftOf2 ?x)
          )
          (and
            (RightOf14 ?x)
            (LeftOf8 ?x)
          )
          (and
            (RightOf10 ?x)
            (LeftOf4 ?x)
          )
          (and
            (RightOf10 ?x)
            (LeftOf3 ?x)
          )
          (and
            (RightOf12 ?x)
            (LeftOf1 ?x)
          )
          (and
            (RightOf12 ?x)
            (LeftOf8 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf10 ?x)
          )
          (and
            (LeftOf8 ?x)
            (RightOf10 ?x)
          )
          (and
            (LeftOf9 ?x)
            (RightOf10 ?x)
          )
          (and
            (RightOf14 ?x)
            (LeftOf10 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf11 ?x)
          )
          (and
            (RightOf12 ?x)
            (LeftOf3 ?x)
          )
          (and
            (RightOf14 ?x)
            (LeftOf4 ?x)
          )
          (and
            (LeftOf1 ?x)
            (RightOf11 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf12 ?x)
          )
          (and
            (RightOf14 ?x)
            (LeftOf3 ?x)
          )
          (and
            (LeftOf7 ?x)
            (RightOf13 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf14 ?x)
          )
          (and
            (LeftOf8 ?x)
            (RightOf14 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf13 ?x)
          )
          (and
            (LeftOf8 ?x)
            (RightOf13 ?x)
          )
          (and
            (LeftOf9 ?x)
            (RightOf13 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf11 ?x)
          )
          (and
            (LeftOf1 ?x)
            (RightOf10 ?x)
          )
          (and
            (LeftOf10 ?x)
            (RightOf13 ?x)
          )
          (and
            (RightOf12 ?x)
            (LeftOf10 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf10 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf11 ?x)
          )
          (and
            (RightOf13 ?x)
            (LeftOf3 ?x)
          )
          (and
            (LeftOf10 ?x)
            (RightOf10 ?x)
          )
          (and
            (LeftOf10 ?x)
            (RightOf11 ?x)
          )
          (and
            (LeftOf11 ?x)
            (RightOf11 ?x)
          )
          (and
            (LeftOf10 ?x)
            (RightOf14 ?x)
          )
          (and
            (LeftOf7 ?x)
            (RightOf11 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf13 ?x)
          )
          (and
            (RightOf14 ?x)
            (LeftOf11 ?x)
          )
        )
        (and
          (Column9 ?x)
        )
      )
      (when
        (or
          (and
            (LeftOf6 ?x)
            (RightOf14 ?x)
          )
          (and
            (RightOf12 ?x)
            (LeftOf11 ?x)
          )
          (and
            (RightOf14 ?x)
            (LeftOf9 ?x)
          )
          (and
            (RightOf14 ?x)
            (LeftOf1 ?x)
          )
          (and
            (LeftOf8 ?x)
            (RightOf11 ?x)
          )
          (and
            (LeftOf4 ?x)
            (RightOf13 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf11 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf13 ?x)
          )
          (and
            (LeftOf1 ?x)
            (RightOf13 ?x)
          )
          (and
            (LeftOf12 ?x)
            (RightOf13 ?x)
          )
          (and
            (RightOf12 ?x)
            (LeftOf6 ?x)
          )
          (and
            (LeftOf3 ?x)
            (RightOf13 ?x)
          )
          (and
            (RightOf11 ?x)
            (LeftOf4 ?x)
          )
          (and
            (RightOf12 ?x)
            (LeftOf4 ?x)
          )
          (and
            (RightOf11 ?x)
            (LeftOf3 ?x)
          )
          (and
            (RightOf14 ?x)
            (LeftOf2 ?x)
          )
          (and
            (LeftOf9 ?x)
            (RightOf11 ?x)
          )
          (and
            (LeftOf11 ?x)
            (RightOf13 ?x)
          )
          (and
            (RightOf14 ?x)
            (LeftOf7 ?x)
          )
          (and
            (RightOf12 ?x)
            (LeftOf9 ?x)
          )
          (and
            (RightOf12 ?x)
            (LeftOf5 ?x)
          )
          (and
            (RightOf12 ?x)
            (LeftOf7 ?x)
          )
          (and
            (RightOf12 ?x)
            (LeftOf2 ?x)
          )
          (and
            (RightOf14 ?x)
            (LeftOf8 ?x)
          )
          (and
            (LeftOf7 ?x)
            (RightOf11 ?x)
          )
          (and
            (RightOf12 ?x)
            (LeftOf1 ?x)
          )
          (and
            (RightOf12 ?x)
            (LeftOf8 ?x)
          )
          (and
            (LeftOf12 ?x)
            (RightOf11 ?x)
          )
          (and
            (RightOf14 ?x)
            (LeftOf10 ?x)
          )
          (and
            (LeftOf12 ?x)
            (RightOf14 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf11 ?x)
          )
          (and
            (RightOf12 ?x)
            (LeftOf3 ?x)
          )
          (and
            (RightOf14 ?x)
            (LeftOf4 ?x)
          )
          (and
            (LeftOf1 ?x)
            (RightOf11 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf12 ?x)
          )
          (and
            (RightOf14 ?x)
            (LeftOf3 ?x)
          )
          (and
            (LeftOf7 ?x)
            (RightOf13 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf14 ?x)
          )
          (and
            (LeftOf8 ?x)
            (RightOf14 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf13 ?x)
          )
          (and
            (LeftOf8 ?x)
            (RightOf13 ?x)
          )
          (and
            (LeftOf9 ?x)
            (RightOf13 ?x)
          )
          (and
            (LeftOf10 ?x)
            (RightOf13 ?x)
          )
          (and
            (RightOf12 ?x)
            (LeftOf10 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf11 ?x)
          )
          (and
            (LeftOf12 ?x)
            (RightOf12 ?x)
          )
          (and
            (RightOf13 ?x)
            (LeftOf3 ?x)
          )
          (and
            (LeftOf10 ?x)
            (RightOf11 ?x)
          )
          (and
            (LeftOf11 ?x)
            (RightOf11 ?x)
          )
          (and
            (LeftOf10 ?x)
            (RightOf14 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf13 ?x)
          )
          (and
            (RightOf14 ?x)
            (LeftOf11 ?x)
          )
        )
        (and
          (Column10 ?x)
        )
      )
      (when
        (or
          (and
            (LeftOf6 ?x)
            (RightOf14 ?x)
          )
          (and
            (RightOf12 ?x)
            (LeftOf11 ?x)
          )
          (and
            (RightOf14 ?x)
            (LeftOf9 ?x)
          )
          (and
            (RightOf14 ?x)
            (LeftOf1 ?x)
          )
          (and
            (RightOf12 ?x)
            (LeftOf12 ?x)
          )
          (and
            (LeftOf4 ?x)
            (RightOf13 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf13 ?x)
          )
          (and
            (LeftOf1 ?x)
            (RightOf13 ?x)
          )
          (and
            (LeftOf13 ?x)
            (RightOf13 ?x)
          )
          (and
            (LeftOf12 ?x)
            (RightOf13 ?x)
          )
          (and
            (RightOf12 ?x)
            (LeftOf6 ?x)
          )
          (and
            (RightOf14 ?x)
            (LeftOf7 ?x)
          )
          (and
            (LeftOf3 ?x)
            (RightOf13 ?x)
          )
          (and
            (RightOf12 ?x)
            (LeftOf4 ?x)
          )
          (and
            (RightOf14 ?x)
            (LeftOf2 ?x)
          )
          (and
            (LeftOf11 ?x)
            (RightOf13 ?x)
          )
          (and
            (RightOf14 ?x)
            (LeftOf13 ?x)
          )
          (and
            (RightOf12 ?x)
            (LeftOf9 ?x)
          )
          (and
            (RightOf12 ?x)
            (LeftOf5 ?x)
          )
          (and
            (RightOf12 ?x)
            (LeftOf7 ?x)
          )
          (and
            (RightOf12 ?x)
            (LeftOf2 ?x)
          )
          (and
            (RightOf14 ?x)
            (LeftOf8 ?x)
          )
          (and
            (RightOf12 ?x)
            (LeftOf13 ?x)
          )
          (and
            (RightOf12 ?x)
            (LeftOf1 ?x)
          )
          (and
            (RightOf12 ?x)
            (LeftOf8 ?x)
          )
          (and
            (RightOf14 ?x)
            (LeftOf10 ?x)
          )
          (and
            (LeftOf12 ?x)
            (RightOf14 ?x)
          )
          (and
            (RightOf12 ?x)
            (LeftOf3 ?x)
          )
          (and
            (RightOf14 ?x)
            (LeftOf4 ?x)
          )
          (and
            (LeftOf7 ?x)
            (RightOf13 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf14 ?x)
          )
          (and
            (LeftOf8 ?x)
            (RightOf14 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf13 ?x)
          )
          (and
            (LeftOf8 ?x)
            (RightOf13 ?x)
          )
          (and
            (LeftOf9 ?x)
            (RightOf13 ?x)
          )
          (and
            (LeftOf10 ?x)
            (RightOf13 ?x)
          )
          (and
            (RightOf12 ?x)
            (LeftOf10 ?x)
          )
          (and
            (RightOf13 ?x)
            (LeftOf3 ?x)
          )
          (and
            (RightOf14 ?x)
            (LeftOf3 ?x)
          )
          (and
            (LeftOf10 ?x)
            (RightOf14 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf13 ?x)
          )
          (and
            (RightOf14 ?x)
            (LeftOf11 ?x)
          )
        )
        (and
          (Column11 ?x)
        )
      )
      (when
        (or
          (and
            (LeftOf6 ?x)
            (RightOf14 ?x)
          )
          (and
            (RightOf14 ?x)
            (LeftOf9 ?x)
          )
          (and
            (RightOf14 ?x)
            (LeftOf1 ?x)
          )
          (and
            (LeftOf4 ?x)
            (RightOf13 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf13 ?x)
          )
          (and
            (LeftOf1 ?x)
            (RightOf13 ?x)
          )
          (and
            (LeftOf13 ?x)
            (RightOf13 ?x)
          )
          (and
            (LeftOf12 ?x)
            (RightOf13 ?x)
          )
          (and
            (RightOf14 ?x)
            (LeftOf7 ?x)
          )
          (and
            (RightOf14 ?x)
            (LeftOf2 ?x)
          )
          (and
            (LeftOf11 ?x)
            (RightOf13 ?x)
          )
          (and
            (RightOf14 ?x)
            (LeftOf13 ?x)
          )
          (and
            (RightOf14 ?x)
            (LeftOf8 ?x)
          )
          (and
            (LeftOf10 ?x)
            (RightOf14 ?x)
          )
          (and
            (LeftOf14 ?x)
            (RightOf13 ?x)
          )
          (and
            (RightOf14 ?x)
            (LeftOf10 ?x)
          )
          (and
            (LeftOf12 ?x)
            (RightOf14 ?x)
          )
          (and
            (RightOf14 ?x)
            (LeftOf4 ?x)
          )
          (and
            (LeftOf7 ?x)
            (RightOf13 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf14 ?x)
          )
          (and
            (LeftOf8 ?x)
            (RightOf14 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf13 ?x)
          )
          (and
            (LeftOf8 ?x)
            (RightOf13 ?x)
          )
          (and
            (LeftOf9 ?x)
            (RightOf13 ?x)
          )
          (and
            (LeftOf10 ?x)
            (RightOf13 ?x)
          )
          (and
            (RightOf13 ?x)
            (LeftOf3 ?x)
          )
          (and
            (RightOf14 ?x)
            (LeftOf3 ?x)
          )
          (and
            (RightOf14 ?x)
            (LeftOf14 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf13 ?x)
          )
          (and
            (RightOf14 ?x)
            (LeftOf11 ?x)
          )
        )
        (and
          (Column12 ?x)
        )
      )
      (when
        (or
          (and
            (RightOf14 ?x)
            (LeftOf7 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf14 ?x)
          )
          (and
            (RightOf14 ?x)
            (LeftOf10 ?x)
          )
          (and
            (LeftOf15 ?x)
            (RightOf14 ?x)
          )
          (and
            (RightOf14 ?x)
            (LeftOf9 ?x)
          )
          (and
            (RightOf14 ?x)
            (LeftOf1 ?x)
          )
          (and
            (RightOf14 ?x)
            (LeftOf2 ?x)
          )
          (and
            (RightOf14 ?x)
            (LeftOf4 ?x)
          )
          (and
            (RightOf14 ?x)
            (LeftOf13 ?x)
          )
          (and
            (RightOf14 ?x)
            (LeftOf3 ?x)
          )
          (and
            (RightOf14 ?x)
            (LeftOf14 ?x)
          )
          (and
            (LeftOf12 ?x)
            (RightOf14 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf14 ?x)
          )
          (and
            (RightOf14 ?x)
            (LeftOf8 ?x)
          )
          (and
            (RightOf14 ?x)
            (LeftOf11 ?x)
          )
        )
        (and
          (Column13 ?x)
        )
      )
    )
  )
  (:action moveUp
    :parameters ( ?x )
    :precondition ( and
      (not (CheckConsistency))
      (not (Error))
      (or
        (BelowOf7 ?x)
        (AboveOf9 ?x)
        (BelowOf1 ?x)
        (AboveOf7 ?x)
        (AboveOf11 ?x)
        (AboveOf1 ?x)
        (BelowOf14 ?x)
        (BelowOf8 ?x)
        (BelowOf3 ?x)
        (AboveOf14 ?x)
        (AboveOf3 ?x)
        (BelowOf9 ?x)
        (BelowOf2 ?x)
        (BelowOf5 ?x)
        (BelowOf11 ?x)
        (BelowOf10 ?x)
        (BelowOf13 ?x)
        (BelowOf4 ?x)
        (AboveOf10 ?x)
        (AboveOf0 ?x)
        (AboveOf8 ?x)
        (AboveOf6 ?x)
        (AboveOf5 ?x)
        (AboveOf13 ?x)
        (Rows ?x)
        (AboveOf4 ?x)
        (BelowOf12 ?x)
        (AboveOf12 ?x)
        (BelowOf6 ?x)
        (BelowOf15 ?x)
        (AboveOf2 ?x)
      )
    )
    :effect ( and
      (CheckConsistency)
      (when
        (or
          (AboveOf1 ?x)
          (AboveOf5 ?x)
          (AboveOf14 ?x)
          (AboveOf13 ?x)
          (AboveOf3 ?x)
          (AboveOf4 ?x)
          (AboveOf9 ?x)
          (AboveOf8 ?x)
          (AboveOf12 ?x)
          (AboveOf7 ?x)
          (AboveOf0 ?x)
          (AboveOf2 ?x)
          (AboveOf10 ?x)
          (AboveOf11 ?x)
          (AboveOf6 ?x)
        )
        (and
          (AboveOf1 ?x)
        )
      )
      (when
        (or
          (AboveOf1 ?x)
          (AboveOf5 ?x)
          (AboveOf14 ?x)
          (AboveOf13 ?x)
          (AboveOf3 ?x)
          (AboveOf4 ?x)
          (AboveOf9 ?x)
          (AboveOf8 ?x)
          (AboveOf12 ?x)
          (AboveOf7 ?x)
          (AboveOf2 ?x)
          (AboveOf10 ?x)
          (AboveOf11 ?x)
          (AboveOf6 ?x)
        )
        (and
          (AboveOf2 ?x)
        )
      )
      (when
        (or
          (AboveOf5 ?x)
          (AboveOf14 ?x)
          (AboveOf13 ?x)
          (AboveOf3 ?x)
          (AboveOf4 ?x)
          (AboveOf9 ?x)
          (AboveOf8 ?x)
          (AboveOf12 ?x)
          (AboveOf7 ?x)
          (AboveOf2 ?x)
          (AboveOf10 ?x)
          (AboveOf6 ?x)
          (AboveOf11 ?x)
        )
        (and
          (AboveOf3 ?x)
        )
      )
      (when
        (or
          (AboveOf5 ?x)
          (AboveOf14 ?x)
          (AboveOf13 ?x)
          (AboveOf3 ?x)
          (AboveOf4 ?x)
          (AboveOf9 ?x)
          (AboveOf12 ?x)
          (AboveOf7 ?x)
          (AboveOf8 ?x)
          (AboveOf10 ?x)
          (AboveOf6 ?x)
          (AboveOf11 ?x)
        )
        (and
          (AboveOf4 ?x)
        )
      )
      (when
        (or
          (AboveOf5 ?x)
          (AboveOf14 ?x)
          (AboveOf13 ?x)
          (AboveOf4 ?x)
          (AboveOf9 ?x)
          (AboveOf12 ?x)
          (AboveOf7 ?x)
          (AboveOf8 ?x)
          (AboveOf10 ?x)
          (AboveOf6 ?x)
          (AboveOf11 ?x)
        )
        (and
          (AboveOf5 ?x)
        )
      )
      (when
        (or
          (AboveOf5 ?x)
          (AboveOf14 ?x)
          (AboveOf13 ?x)
          (AboveOf9 ?x)
          (AboveOf12 ?x)
          (AboveOf10 ?x)
          (AboveOf8 ?x)
          (AboveOf7 ?x)
          (AboveOf6 ?x)
          (AboveOf11 ?x)
        )
        (and
          (AboveOf6 ?x)
        )
      )
      (when
        (or
          (AboveOf14 ?x)
          (AboveOf13 ?x)
          (AboveOf9 ?x)
          (AboveOf12 ?x)
          (AboveOf10 ?x)
          (AboveOf8 ?x)
          (AboveOf7 ?x)
          (AboveOf6 ?x)
          (AboveOf11 ?x)
        )
        (and
          (AboveOf7 ?x)
        )
      )
      (when
        (or
          (AboveOf14 ?x)
          (AboveOf13 ?x)
          (AboveOf9 ?x)
          (AboveOf12 ?x)
          (AboveOf10 ?x)
          (AboveOf8 ?x)
          (AboveOf7 ?x)
          (AboveOf11 ?x)
        )
        (and
          (AboveOf8 ?x)
        )
      )
      (when
        (or
          (AboveOf14 ?x)
          (AboveOf13 ?x)
          (AboveOf9 ?x)
          (AboveOf12 ?x)
          (AboveOf10 ?x)
          (AboveOf8 ?x)
          (AboveOf11 ?x)
        )
        (and
          (AboveOf9 ?x)
        )
      )
      (when
        (or
          (AboveOf14 ?x)
          (AboveOf13 ?x)
          (AboveOf9 ?x)
          (AboveOf12 ?x)
          (AboveOf10 ?x)
          (AboveOf11 ?x)
        )
        (and
          (AboveOf10 ?x)
        )
      )
      (when
        (or
          (AboveOf14 ?x)
          (AboveOf11 ?x)
          (AboveOf10 ?x)
          (AboveOf13 ?x)
          (AboveOf12 ?x)
        )
        (and
          (AboveOf11 ?x)
        )
      )
      (when
        (or
          (AboveOf11 ?x)
          (AboveOf14 ?x)
          (AboveOf13 ?x)
          (AboveOf12 ?x)
        )
        (and
          (AboveOf12 ?x)
        )
      )
      (when
        (or
          (AboveOf14 ?x)
          (AboveOf13 ?x)
          (AboveOf12 ?x)
        )
        (and
          (AboveOf13 ?x)
        )
      )
      (when
        (or
          (AboveOf14 ?x)
          (AboveOf13 ?x)
        )
        (and
          (AboveOf14 ?x)
        )
      )
      (when
        (BelowOf1 ?x)
        (and
          (BelowOf2 ?x)
          (not (BelowOf1 ?x))
        )
      )
      (when
        (or
          (BelowOf1 ?x)
          (BelowOf2 ?x)
        )
        (and
          (BelowOf3 ?x)
          (not (BelowOf2 ?x))
        )
      )
      (when
        (or
          (BelowOf3 ?x)
          (BelowOf1 ?x)
          (BelowOf2 ?x)
        )
        (and
          (BelowOf4 ?x)
          (not (BelowOf3 ?x))
        )
      )
      (when
        (or
          (BelowOf4 ?x)
          (BelowOf3 ?x)
          (BelowOf1 ?x)
          (BelowOf2 ?x)
        )
        (and
          (BelowOf5 ?x)
          (not (BelowOf4 ?x))
        )
      )
      (when
        (or
          (BelowOf5 ?x)
          (BelowOf4 ?x)
          (BelowOf3 ?x)
          (BelowOf1 ?x)
          (BelowOf2 ?x)
        )
        (and
          (BelowOf6 ?x)
          (not (BelowOf5 ?x))
        )
      )
      (when
        (or
          (BelowOf3 ?x)
          (BelowOf4 ?x)
          (BelowOf1 ?x)
          (BelowOf2 ?x)
          (BelowOf5 ?x)
          (BelowOf6 ?x)
        )
        (and
          (BelowOf7 ?x)
          (not (BelowOf6 ?x))
        )
      )
      (when
        (or
          (BelowOf3 ?x)
          (BelowOf7 ?x)
          (BelowOf4 ?x)
          (BelowOf1 ?x)
          (BelowOf2 ?x)
          (BelowOf5 ?x)
          (BelowOf6 ?x)
        )
        (and
          (BelowOf8 ?x)
          (not (BelowOf7 ?x))
        )
      )
      (when
        (or
          (BelowOf8 ?x)
          (BelowOf3 ?x)
          (BelowOf7 ?x)
          (BelowOf4 ?x)
          (BelowOf1 ?x)
          (BelowOf2 ?x)
          (BelowOf5 ?x)
          (BelowOf6 ?x)
        )
        (and
          (BelowOf9 ?x)
          (not (BelowOf8 ?x))
        )
      )
      (when
        (or
          (BelowOf8 ?x)
          (BelowOf3 ?x)
          (BelowOf9 ?x)
          (BelowOf7 ?x)
          (BelowOf4 ?x)
          (BelowOf1 ?x)
          (BelowOf2 ?x)
          (BelowOf5 ?x)
          (BelowOf6 ?x)
        )
        (and
          (BelowOf10 ?x)
          (not (BelowOf9 ?x))
        )
      )
      (when
        (or
          (BelowOf8 ?x)
          (BelowOf3 ?x)
          (BelowOf9 ?x)
          (BelowOf7 ?x)
          (BelowOf4 ?x)
          (BelowOf1 ?x)
          (BelowOf2 ?x)
          (BelowOf5 ?x)
          (BelowOf6 ?x)
          (BelowOf10 ?x)
        )
        (and
          (BelowOf11 ?x)
          (not (BelowOf10 ?x))
        )
      )
      (when
        (or
          (BelowOf8 ?x)
          (BelowOf3 ?x)
          (BelowOf9 ?x)
          (BelowOf7 ?x)
          (BelowOf4 ?x)
          (BelowOf1 ?x)
          (BelowOf2 ?x)
          (BelowOf5 ?x)
          (BelowOf6 ?x)
          (BelowOf11 ?x)
          (BelowOf10 ?x)
        )
        (and
          (BelowOf12 ?x)
          (not (BelowOf11 ?x))
        )
      )
      (when
        (or
          (BelowOf8 ?x)
          (BelowOf3 ?x)
          (BelowOf9 ?x)
          (BelowOf7 ?x)
          (BelowOf12 ?x)
          (BelowOf4 ?x)
          (BelowOf1 ?x)
          (BelowOf2 ?x)
          (BelowOf5 ?x)
          (BelowOf6 ?x)
          (BelowOf11 ?x)
          (BelowOf10 ?x)
        )
        (and
          (BelowOf13 ?x)
          (not (BelowOf12 ?x))
        )
      )
      (when
        (or
          (BelowOf8 ?x)
          (BelowOf13 ?x)
          (BelowOf3 ?x)
          (BelowOf9 ?x)
          (BelowOf7 ?x)
          (BelowOf12 ?x)
          (BelowOf4 ?x)
          (BelowOf1 ?x)
          (BelowOf2 ?x)
          (BelowOf5 ?x)
          (BelowOf6 ?x)
          (BelowOf11 ?x)
          (BelowOf10 ?x)
        )
        (and
          (BelowOf14 ?x)
          (not (BelowOf13 ?x))
        )
      )
      (when
        (or
          (BelowOf14 ?x)
          (BelowOf8 ?x)
          (BelowOf13 ?x)
          (BelowOf3 ?x)
          (BelowOf9 ?x)
          (BelowOf7 ?x)
          (BelowOf12 ?x)
          (BelowOf4 ?x)
          (BelowOf1 ?x)
          (BelowOf2 ?x)
          (BelowOf5 ?x)
          (BelowOf6 ?x)
          (BelowOf11 ?x)
          (BelowOf10 ?x)
        )
        (and
          (BelowOf15 ?x)
          (not (BelowOf14 ?x))
        )
      )
      (when
        (Row0 ?x)
        (and
          (Row1 ?x)
          (not (Row0 ?x))
        )
      )
      (when
        (Row1 ?x)
        (and
          (Row2 ?x)
          (not (Row1 ?x))
        )
      )
      (when
        (Row2 ?x)
        (and
          (Row3 ?x)
          (not (Row2 ?x))
        )
      )
      (when
        (Row3 ?x)
        (and
          (Row4 ?x)
          (not (Row3 ?x))
        )
      )
      (when
        (Row4 ?x)
        (and
          (Row5 ?x)
          (not (Row4 ?x))
        )
      )
      (when
        (Row5 ?x)
        (and
          (Row6 ?x)
          (not (Row5 ?x))
        )
      )
      (when
        (Row6 ?x)
        (and
          (Row7 ?x)
          (not (Row6 ?x))
        )
      )
      (when
        (Row7 ?x)
        (and
          (Row8 ?x)
          (not (Row7 ?x))
        )
      )
      (when
        (Row8 ?x)
        (and
          (Row9 ?x)
          (not (Row8 ?x))
        )
      )
      (when
        (Row9 ?x)
        (and
          (Row10 ?x)
          (not (Row9 ?x))
        )
      )
      (when
        (Row10 ?x)
        (and
          (Row11 ?x)
          (not (Row10 ?x))
        )
      )
      (when
        (Row11 ?x)
        (and
          (Row12 ?x)
          (not (Row11 ?x))
        )
      )
      (when
        (Row12 ?x)
        (and
          (Row13 ?x)
          (not (Row12 ?x))
        )
      )
      (when
        (Row13 ?x)
        (and
          (Row14 ?x)
          (not (Row13 ?x))
        )
      )
      (when
        (or
          (and
            (AboveOf9 ?x)
            (BelowOf1 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf3 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf8 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf0 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf10 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf1 ?x)
          )
          (and
            (AboveOf5 ?x)
            (BelowOf1 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf6 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf11 ?x)
          )
          (and
            (AboveOf7 ?x)
            (BelowOf1 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf1 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf12 ?x)
          )
          (and
            (AboveOf1 ?x)
            (BelowOf1 ?x)
          )
          (and
            (AboveOf2 ?x)
            (BelowOf1 ?x)
          )
          (and
            (AboveOf4 ?x)
            (BelowOf1 ?x)
          )
        )
        (and
          (Row1 ?x)
        )
      )
      (when
        (or
          (and
            (BelowOf2 ?x)
            (AboveOf3 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf8 ?x)
          )
          (and
            (AboveOf5 ?x)
            (BelowOf2 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf12 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf12 ?x)
          )
          (and
            (AboveOf4 ?x)
            (BelowOf1 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf8 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf10 ?x)
          )
          (and
            (AboveOf9 ?x)
            (BelowOf1 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf3 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf11 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf10 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf1 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf6 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf1 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf9 ?x)
          )
          (and
            (AboveOf2 ?x)
            (BelowOf1 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf6 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf2 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf4 ?x)
          )
          (and
            (AboveOf5 ?x)
            (BelowOf1 ?x)
          )
          (and
            (AboveOf1 ?x)
            (BelowOf1 ?x)
          )
          (and
            (AboveOf7 ?x)
            (BelowOf1 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf2 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf7 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf1 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf11 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf2 ?x)
          )
        )
        (and
          (Row2 ?x)
        )
      )
      (when
        (or
          (and
            (BelowOf2 ?x)
            (AboveOf3 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf8 ?x)
          )
          (and
            (AboveOf5 ?x)
            (BelowOf2 ?x)
          )
          (and
            (BelowOf3 ?x)
            (AboveOf11 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf3 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf12 ?x)
          )
          (and
            (BelowOf3 ?x)
            (AboveOf12 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf12 ?x)
          )
          (and
            (AboveOf4 ?x)
            (BelowOf1 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf8 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf10 ?x)
          )
          (and
            (AboveOf9 ?x)
            (BelowOf1 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf3 ?x)
          )
          (and
            (AboveOf2 ?x)
            (BelowOf2 ?x)
          )
          (and
            (AboveOf10 ?x)
            (BelowOf3 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf1 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf10 ?x)
          )
          (and
            (AboveOf4 ?x)
            (BelowOf3 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf6 ?x)
          )
          (and
            (BelowOf3 ?x)
            (AboveOf8 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf1 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf9 ?x)
          )
          (and
            (BelowOf3 ?x)
            (AboveOf3 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf6 ?x)
          )
          (and
            (AboveOf2 ?x)
            (BelowOf3 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf2 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf4 ?x)
          )
          (and
            (AboveOf9 ?x)
            (BelowOf3 ?x)
          )
          (and
            (AboveOf5 ?x)
            (BelowOf1 ?x)
          )
          (and
            (AboveOf7 ?x)
            (BelowOf1 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf2 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf3 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf7 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf11 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf11 ?x)
          )
          (and
            (AboveOf7 ?x)
            (BelowOf3 ?x)
          )
          (and
            (AboveOf2 ?x)
            (BelowOf1 ?x)
          )
          (and
            (AboveOf6 ?x)
            (BelowOf3 ?x)
          )
          (and
            (AboveOf5 ?x)
            (BelowOf3 ?x)
          )
        )
        (and
          (Row3 ?x)
        )
      )
      (when
        (or
          (and
            (BelowOf2 ?x)
            (AboveOf3 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf8 ?x)
          )
          (and
            (AboveOf5 ?x)
            (BelowOf2 ?x)
          )
          (and
            (BelowOf3 ?x)
            (AboveOf11 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf12 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf2 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf3 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf11 ?x)
          )
          (and
            (BelowOf3 ?x)
            (AboveOf12 ?x)
          )
          (and
            (AboveOf11 ?x)
            (BelowOf3 ?x)
          )
          (and
            (AboveOf4 ?x)
            (BelowOf1 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf8 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf10 ?x)
          )
          (and
            (AboveOf9 ?x)
            (BelowOf1 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf3 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf10 ?x)
          )
          (and
            (AboveOf10 ?x)
            (BelowOf3 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf1 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf10 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf12 ?x)
          )
          (and
            (AboveOf4 ?x)
            (BelowOf3 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf6 ?x)
          )
          (and
            (BelowOf3 ?x)
            (AboveOf8 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf1 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf9 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf6 ?x)
          )
          (and
            (AboveOf3 ?x)
            (BelowOf3 ?x)
          )
          (and
            (AboveOf5 ?x)
            (BelowOf4 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf12 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf4 ?x)
          )
          (and
            (AboveOf7 ?x)
            (BelowOf4 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf8 ?x)
          )
          (and
            (AboveOf4 ?x)
            (BelowOf4 ?x)
          )
          (and
            (AboveOf5 ?x)
            (BelowOf1 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf9 ?x)
          )
          (and
            (AboveOf7 ?x)
            (BelowOf1 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf2 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf3 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf4 ?x)
          )
          (and
            (AboveOf9 ?x)
            (BelowOf3 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf7 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf11 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf11 ?x)
          )
          (and
            (AboveOf7 ?x)
            (BelowOf3 ?x)
          )
          (and
            (AboveOf6 ?x)
            (BelowOf3 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf6 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf3 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf4 ?x)
          )
          (and
            (AboveOf5 ?x)
            (BelowOf3 ?x)
          )
        )
        (and
          (Row4 ?x)
        )
      )
      (when
        (or
          (and
            (BelowOf5 ?x)
            (AboveOf14 ?x)
          )
          (and
            (AboveOf10 ?x)
            (BelowOf3 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf8 ?x)
          )
          (and
            (AboveOf5 ?x)
            (BelowOf2 ?x)
          )
          (and
            (BelowOf3 ?x)
            (AboveOf11 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf12 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf2 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf3 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf12 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf11 ?x)
          )
          (and
            (BelowOf3 ?x)
            (AboveOf12 ?x)
          )
          (and
            (AboveOf11 ?x)
            (BelowOf3 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf9 ?x)
          )
          (and
            (AboveOf4 ?x)
            (BelowOf1 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf8 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf10 ?x)
          )
          (and
            (AboveOf9 ?x)
            (BelowOf1 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf6 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf10 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf11 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf1 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf10 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf12 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf7 ?x)
          )
          (and
            (AboveOf4 ?x)
            (BelowOf3 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf5 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf6 ?x)
          )
          (and
            (BelowOf3 ?x)
            (AboveOf8 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf1 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf9 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf6 ?x)
          )
          (and
            (AboveOf5 ?x)
            (BelowOf4 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf12 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf4 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf11 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf10 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf13 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf8 ?x)
          )
          (and
            (AboveOf5 ?x)
            (BelowOf1 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf9 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf6 ?x)
          )
          (and
            (AboveOf7 ?x)
            (BelowOf1 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf2 ?x)
          )
          (and
            (AboveOf4 ?x)
            (BelowOf4 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf4 ?x)
          )
          (and
            (AboveOf9 ?x)
            (BelowOf3 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf7 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf11 ?x)
          )
          (and
            (AboveOf7 ?x)
            (BelowOf3 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf8 ?x)
          )
          (and
            (AboveOf6 ?x)
            (BelowOf3 ?x)
          )
          (and
            (AboveOf7 ?x)
            (BelowOf4 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf4 ?x)
          )
          (and
            (AboveOf5 ?x)
            (BelowOf3 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf4 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf3 ?x)
          )
        )
        (and
          (Row5 ?x)
        )
      )
      (when
        (or
          (and
            (BelowOf5 ?x)
            (AboveOf14 ?x)
          )
          (and
            (AboveOf10 ?x)
            (BelowOf3 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf8 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf6 ?x)
          )
          (and
            (BelowOf3 ?x)
            (AboveOf11 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf11 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf12 ?x)
          )
          (and
            (AboveOf10 ?x)
            (BelowOf6 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf2 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf3 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf4 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf11 ?x)
          )
          (and
            (BelowOf3 ?x)
            (AboveOf12 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf12 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf9 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf8 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf10 ?x)
          )
          (and
            (AboveOf9 ?x)
            (BelowOf6 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf6 ?x)
          )
          (and
            (BelowOf6 ?x)
            (AboveOf8 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf10 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf11 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf10 ?x)
          )
          (and
            (AboveOf11 ?x)
            (BelowOf3 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf7 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf1 ?x)
          )
          (and
            (AboveOf6 ?x)
            (BelowOf6 ?x)
          )
          (and
            (BelowOf3 ?x)
            (AboveOf8 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf1 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf6 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf6 ?x)
          )
          (and
            (AboveOf5 ?x)
            (BelowOf4 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf12 ?x)
          )
          (and
            (AboveOf7 ?x)
            (BelowOf4 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf10 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf13 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf8 ?x)
          )
          (and
            (AboveOf5 ?x)
            (BelowOf5 ?x)
          )
          (and
            (AboveOf7 ?x)
            (BelowOf6 ?x)
          )
          (and
            (AboveOf5 ?x)
            (BelowOf1 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf9 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf12 ?x)
          )
          (and
            (AboveOf7 ?x)
            (BelowOf1 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf9 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf2 ?x)
          )
          (and
            (AboveOf9 ?x)
            (BelowOf1 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf8 ?x)
          )
          (and
            (AboveOf5 ?x)
            (BelowOf6 ?x)
          )
          (and
            (AboveOf9 ?x)
            (BelowOf3 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf7 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf6 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf11 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf5 ?x)
          )
          (and
            (BelowOf6 ?x)
            (AboveOf12 ?x)
          )
          (and
            (AboveOf7 ?x)
            (BelowOf3 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf6 ?x)
          )
          (and
            (AboveOf6 ?x)
            (BelowOf3 ?x)
          )
          (and
            (BelowOf6 ?x)
            (AboveOf11 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf4 ?x)
          )
          (and
            (AboveOf5 ?x)
            (BelowOf2 ?x)
          )
          (and
            (AboveOf5 ?x)
            (BelowOf3 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf3 ?x)
          )
        )
        (and
          (Row6 ?x)
        )
      )
      (when
        (or
          (and
            (BelowOf5 ?x)
            (AboveOf14 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf11 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf8 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf6 ?x)
          )
          (and
            (BelowOf3 ?x)
            (AboveOf11 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf11 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf11 ?x)
          )
          (and
            (BelowOf7 ?x)
            (AboveOf11 ?x)
          )
          (and
            (BelowOf7 ?x)
            (AboveOf8 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf3 ?x)
          )
          (and
            (BelowOf7 ?x)
            (AboveOf7 ?x)
          )
          (and
            (BelowOf3 ?x)
            (AboveOf12 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf7 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf7 ?x)
          )
          (and
            (AboveOf11 ?x)
            (BelowOf3 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf9 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf8 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf10 ?x)
          )
          (and
            (AboveOf9 ?x)
            (BelowOf6 ?x)
          )
          (and
            (BelowOf7 ?x)
            (AboveOf12 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf6 ?x)
          )
          (and
            (BelowOf6 ?x)
            (AboveOf8 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf10 ?x)
          )
          (and
            (AboveOf10 ?x)
            (BelowOf3 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf10 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf12 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf7 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf1 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf6 ?x)
          )
          (and
            (BelowOf3 ?x)
            (AboveOf8 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf1 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf9 ?x)
          )
          (and
            (AboveOf6 ?x)
            (BelowOf3 ?x)
          )
          (and
            (BelowOf6 ?x)
            (AboveOf10 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf2 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf12 ?x)
          )
          (and
            (AboveOf7 ?x)
            (BelowOf4 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf10 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf13 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf8 ?x)
          )
          (and
            (AboveOf7 ?x)
            (BelowOf6 ?x)
          )
          (and
            (BelowOf7 ?x)
            (AboveOf10 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf12 ?x)
          )
          (and
            (AboveOf7 ?x)
            (BelowOf1 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf2 ?x)
          )
          (and
            (AboveOf9 ?x)
            (BelowOf1 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf8 ?x)
          )
          (and
            (AboveOf9 ?x)
            (BelowOf3 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf7 ?x)
          )
          (and
            (BelowOf6 ?x)
            (AboveOf6 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf6 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf11 ?x)
          )
          (and
            (BelowOf7 ?x)
            (AboveOf9 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf5 ?x)
          )
          (and
            (BelowOf6 ?x)
            (AboveOf12 ?x)
          )
          (and
            (AboveOf7 ?x)
            (BelowOf3 ?x)
          )
          (and
            (AboveOf10 ?x)
            (BelowOf6 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf6 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf6 ?x)
          )
          (and
            (BelowOf6 ?x)
            (AboveOf11 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf4 ?x)
          )
          (and
            (BelowOf7 ?x)
            (AboveOf6 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf9 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf4 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf3 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf12 ?x)
          )
        )
        (and
          (Row7 ?x)
        )
      )
      (when
        (or
          (and
            (BelowOf5 ?x)
            (AboveOf14 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf8 ?x)
          )
          (and
            (AboveOf11 ?x)
            (BelowOf8 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf8 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf12 ?x)
          )
          (and
            (BelowOf3 ?x)
            (AboveOf11 ?x)
          )
          (and
            (AboveOf7 ?x)
            (BelowOf8 ?x)
          )
          (and
            (BelowOf7 ?x)
            (AboveOf11 ?x)
          )
          (and
            (BelowOf7 ?x)
            (AboveOf8 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf3 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf6 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf11 ?x)
          )
          (and
            (BelowOf3 ?x)
            (AboveOf12 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf7 ?x)
          )
          (and
            (AboveOf11 ?x)
            (BelowOf3 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf9 ?x)
          )
          (and
            (AboveOf7 ?x)
            (BelowOf7 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf8 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf10 ?x)
          )
          (and
            (AboveOf9 ?x)
            (BelowOf6 ?x)
          )
          (and
            (BelowOf7 ?x)
            (AboveOf12 ?x)
          )
          (and
            (BelowOf6 ?x)
            (AboveOf8 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf10 ?x)
          )
          (and
            (AboveOf10 ?x)
            (BelowOf3 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf10 ?x)
          )
          (and
            (AboveOf9 ?x)
            (BelowOf8 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf7 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf1 ?x)
          )
          (and
            (BelowOf3 ?x)
            (AboveOf8 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf1 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf9 ?x)
          )
          (and
            (BelowOf8 ?x)
            (AboveOf8 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf11 ?x)
          )
          (and
            (BelowOf6 ?x)
            (AboveOf10 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf2 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf12 ?x)
          )
          (and
            (AboveOf7 ?x)
            (BelowOf4 ?x)
          )
          (and
            (BelowOf8 ?x)
            (AboveOf12 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf10 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf13 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf8 ?x)
          )
          (and
            (AboveOf10 ?x)
            (BelowOf8 ?x)
          )
          (and
            (AboveOf7 ?x)
            (BelowOf6 ?x)
          )
          (and
            (BelowOf7 ?x)
            (AboveOf10 ?x)
          )
          (and
            (AboveOf7 ?x)
            (BelowOf1 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf2 ?x)
          )
          (and
            (AboveOf9 ?x)
            (BelowOf1 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf8 ?x)
          )
          (and
            (AboveOf9 ?x)
            (BelowOf3 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf7 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf11 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf6 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf11 ?x)
          )
          (and
            (BelowOf7 ?x)
            (AboveOf9 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf5 ?x)
          )
          (and
            (BelowOf6 ?x)
            (AboveOf12 ?x)
          )
          (and
            (AboveOf7 ?x)
            (BelowOf3 ?x)
          )
          (and
            (AboveOf10 ?x)
            (BelowOf6 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf8 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf7 ?x)
          )
          (and
            (BelowOf6 ?x)
            (AboveOf11 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf4 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf12 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf9 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf4 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf3 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf12 ?x)
          )
        )
        (and
          (Row8 ?x)
        )
      )
      (when
        (or
          (and
            (BelowOf5 ?x)
            (AboveOf14 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf8 ?x)
          )
          (and
            (AboveOf11 ?x)
            (BelowOf8 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf8 ?x)
          )
          (and
            (AboveOf9 ?x)
            (BelowOf9 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf12 ?x)
          )
          (and
            (BelowOf3 ?x)
            (AboveOf11 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf12 ?x)
          )
          (and
            (AboveOf10 ?x)
            (BelowOf6 ?x)
          )
          (and
            (BelowOf9 ?x)
            (AboveOf11 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf3 ?x)
          )
          (and
            (BelowOf9 ?x)
            (AboveOf12 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf11 ?x)
          )
          (and
            (BelowOf3 ?x)
            (AboveOf12 ?x)
          )
          (and
            (BelowOf9 ?x)
            (AboveOf8 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf7 ?x)
          )
          (and
            (AboveOf11 ?x)
            (BelowOf3 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf9 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf9 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf8 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf10 ?x)
          )
          (and
            (AboveOf9 ?x)
            (BelowOf6 ?x)
          )
          (and
            (BelowOf7 ?x)
            (AboveOf12 ?x)
          )
          (and
            (BelowOf6 ?x)
            (AboveOf8 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf10 ?x)
          )
          (and
            (AboveOf10 ?x)
            (BelowOf3 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf10 ?x)
          )
          (and
            (AboveOf9 ?x)
            (BelowOf8 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf1 ?x)
          )
          (and
            (BelowOf3 ?x)
            (AboveOf8 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf1 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf9 ?x)
          )
          (and
            (BelowOf6 ?x)
            (AboveOf10 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf2 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf12 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf11 ?x)
          )
          (and
            (BelowOf8 ?x)
            (AboveOf12 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf10 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf13 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf8 ?x)
          )
          (and
            (AboveOf10 ?x)
            (BelowOf8 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf9 ?x)
          )
          (and
            (BelowOf7 ?x)
            (AboveOf10 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf2 ?x)
          )
          (and
            (AboveOf9 ?x)
            (BelowOf1 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf8 ?x)
          )
          (and
            (AboveOf8 ?x)
            (BelowOf8 ?x)
          )
          (and
            (AboveOf9 ?x)
            (BelowOf3 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf11 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf6 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf11 ?x)
          )
          (and
            (BelowOf7 ?x)
            (AboveOf9 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf5 ?x)
          )
          (and
            (BelowOf6 ?x)
            (AboveOf12 ?x)
          )
          (and
            (BelowOf9 ?x)
            (AboveOf10 ?x)
          )
          (and
            (BelowOf7 ?x)
            (AboveOf11 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf8 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf7 ?x)
          )
          (and
            (BelowOf6 ?x)
            (AboveOf11 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf6 ?x)
          )
          (and
            (BelowOf7 ?x)
            (AboveOf8 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf4 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf12 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf9 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf4 ?x)
          )
          (and
            (AboveOf12 ?x)
            (BelowOf8 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf3 ?x)
          )
        )
        (and
          (Row9 ?x)
        )
      )
      (when
        (or
          (and
            (BelowOf5 ?x)
            (AboveOf14 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf8 ?x)
          )
          (and
            (AboveOf11 ?x)
            (BelowOf8 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf1 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf10 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf12 ?x)
          )
          (and
            (BelowOf3 ?x)
            (AboveOf11 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf12 ?x)
          )
          (and
            (BelowOf7 ?x)
            (AboveOf11 ?x)
          )
          (and
            (BelowOf9 ?x)
            (AboveOf11 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf3 ?x)
          )
          (and
            (BelowOf9 ?x)
            (AboveOf12 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf11 ?x)
          )
          (and
            (BelowOf3 ?x)
            (AboveOf12 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf7 ?x)
          )
          (and
            (AboveOf11 ?x)
            (BelowOf3 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf9 ?x)
          )
          (and
            (BelowOf10 ?x)
            (AboveOf11 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf9 ?x)
          )
          (and
            (BelowOf9 ?x)
            (AboveOf9 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf10 ?x)
          )
          (and
            (AboveOf9 ?x)
            (BelowOf6 ?x)
          )
          (and
            (BelowOf7 ?x)
            (AboveOf12 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf10 ?x)
          )
          (and
            (AboveOf10 ?x)
            (BelowOf3 ?x)
          )
          (and
            (BelowOf10 ?x)
            (AboveOf12 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf10 ?x)
          )
          (and
            (AboveOf9 ?x)
            (BelowOf8 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf1 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf9 ?x)
          )
          (and
            (AboveOf10 ?x)
            (BelowOf10 ?x)
          )
          (and
            (BelowOf6 ?x)
            (AboveOf10 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf2 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf12 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf11 ?x)
          )
          (and
            (BelowOf8 ?x)
            (AboveOf12 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf10 ?x)
          )
          (and
            (AboveOf9 ?x)
            (BelowOf3 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf13 ?x)
          )
          (and
            (AboveOf10 ?x)
            (BelowOf8 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf9 ?x)
          )
          (and
            (BelowOf7 ?x)
            (AboveOf10 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf11 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf2 ?x)
          )
          (and
            (AboveOf9 ?x)
            (BelowOf1 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf10 ?x)
          )
          (and
            (AboveOf9 ?x)
            (BelowOf10 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf6 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf11 ?x)
          )
          (and
            (BelowOf7 ?x)
            (AboveOf9 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf5 ?x)
          )
          (and
            (BelowOf6 ?x)
            (AboveOf12 ?x)
          )
          (and
            (BelowOf9 ?x)
            (AboveOf10 ?x)
          )
          (and
            (AboveOf10 ?x)
            (BelowOf6 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf8 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf7 ?x)
          )
          (and
            (BelowOf6 ?x)
            (AboveOf11 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf6 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf4 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf12 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf9 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf4 ?x)
          )
          (and
            (AboveOf12 ?x)
            (BelowOf8 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf3 ?x)
          )
        )
        (and
          (Row10 ?x)
        )
      )
      (when
        (or
          (and
            (BelowOf5 ?x)
            (AboveOf14 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf8 ?x)
          )
          (and
            (AboveOf11 ?x)
            (BelowOf8 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf1 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf10 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf12 ?x)
          )
          (and
            (BelowOf3 ?x)
            (AboveOf11 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf12 ?x)
          )
          (and
            (BelowOf7 ?x)
            (AboveOf11 ?x)
          )
          (and
            (BelowOf9 ?x)
            (AboveOf11 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf3 ?x)
          )
          (and
            (BelowOf9 ?x)
            (AboveOf12 ?x)
          )
          (and
            (BelowOf11 ?x)
            (AboveOf12 ?x)
          )
          (and
            (BelowOf3 ?x)
            (AboveOf12 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf11 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf11 ?x)
          )
          (and
            (AboveOf11 ?x)
            (BelowOf3 ?x)
          )
          (and
            (BelowOf10 ?x)
            (AboveOf11 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf9 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf10 ?x)
          )
          (and
            (BelowOf7 ?x)
            (AboveOf12 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf10 ?x)
          )
          (and
            (AboveOf10 ?x)
            (BelowOf3 ?x)
          )
          (and
            (BelowOf10 ?x)
            (AboveOf12 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf10 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf12 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf1 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf12 ?x)
          )
          (and
            (AboveOf10 ?x)
            (BelowOf10 ?x)
          )
          (and
            (BelowOf6 ?x)
            (AboveOf10 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf2 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf7 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf11 ?x)
          )
          (and
            (BelowOf8 ?x)
            (AboveOf12 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf10 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf13 ?x)
          )
          (and
            (BelowOf6 ?x)
            (AboveOf12 ?x)
          )
          (and
            (AboveOf10 ?x)
            (BelowOf8 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf9 ?x)
          )
          (and
            (BelowOf7 ?x)
            (AboveOf10 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf11 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf2 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf10 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf11 ?x)
          )
          (and
            (BelowOf11 ?x)
            (AboveOf11 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf6 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf11 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf5 ?x)
          )
          (and
            (AboveOf10 ?x)
            (BelowOf11 ?x)
          )
          (and
            (BelowOf9 ?x)
            (AboveOf10 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf8 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf7 ?x)
          )
          (and
            (BelowOf6 ?x)
            (AboveOf11 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf6 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf4 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf4 ?x)
          )
          (and
            (AboveOf12 ?x)
            (BelowOf8 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf3 ?x)
          )
        )
        (and
          (Row11 ?x)
        )
      )
      (when
        (or
          (and
            (BelowOf5 ?x)
            (AboveOf14 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf8 ?x)
          )
          (and
            (AboveOf11 ?x)
            (BelowOf8 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf1 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf10 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf12 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf3 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf12 ?x)
          )
          (and
            (BelowOf7 ?x)
            (AboveOf11 ?x)
          )
          (and
            (BelowOf9 ?x)
            (AboveOf11 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf3 ?x)
          )
          (and
            (BelowOf9 ?x)
            (AboveOf12 ?x)
          )
          (and
            (BelowOf11 ?x)
            (AboveOf12 ?x)
          )
          (and
            (BelowOf3 ?x)
            (AboveOf12 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf11 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf11 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf7 ?x)
          )
          (and
            (AboveOf11 ?x)
            (BelowOf3 ?x)
          )
          (and
            (BelowOf10 ?x)
            (AboveOf11 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf9 ?x)
          )
          (and
            (BelowOf7 ?x)
            (AboveOf12 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf11 ?x)
          )
          (and
            (BelowOf10 ?x)
            (AboveOf12 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf12 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf1 ?x)
          )
          (and
            (AboveOf11 ?x)
            (BelowOf12 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf12 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf12 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf11 ?x)
          )
          (and
            (BelowOf8 ?x)
            (AboveOf12 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf13 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf9 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf12 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf2 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf11 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf2 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf10 ?x)
          )
          (and
            (BelowOf12 ?x)
            (AboveOf12 ?x)
          )
          (and
            (BelowOf11 ?x)
            (AboveOf11 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf6 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf11 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf5 ?x)
          )
          (and
            (BelowOf6 ?x)
            (AboveOf12 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf8 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf7 ?x)
          )
          (and
            (BelowOf6 ?x)
            (AboveOf11 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf6 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf4 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf4 ?x)
          )
          (and
            (AboveOf12 ?x)
            (BelowOf8 ?x)
          )
        )
        (and
          (Row12 ?x)
        )
      )
      (when
        (or
          (and
            (BelowOf5 ?x)
            (AboveOf14 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf8 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf1 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf10 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf12 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf3 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf2 ?x)
          )
          (and
            (AboveOf12 ?x)
            (BelowOf12 ?x)
          )
          (and
            (BelowOf9 ?x)
            (AboveOf12 ?x)
          )
          (and
            (BelowOf11 ?x)
            (AboveOf12 ?x)
          )
          (and
            (BelowOf3 ?x)
            (AboveOf12 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf12 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf11 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf7 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf12 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf9 ?x)
          )
          (and
            (BelowOf7 ?x)
            (AboveOf12 ?x)
          )
          (and
            (BelowOf10 ?x)
            (AboveOf12 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf1 ?x)
          )
          (and
            (BelowOf13 ?x)
            (AboveOf12 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf12 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf12 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf13 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf9 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf12 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf11 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf2 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf10 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf4 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf6 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf5 ?x)
          )
          (and
            (BelowOf6 ?x)
            (AboveOf12 ?x)
          )
          (and
            (BelowOf13 ?x)
            (AboveOf13 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf8 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf7 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf6 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf4 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf3 ?x)
          )
          (and
            (BelowOf13 ?x)
            (AboveOf14 ?x)
          )
          (and
            (AboveOf12 ?x)
            (BelowOf8 ?x)
          )
        )
        (and
          (Row13 ?x)
        )
      )
      (when
        (or
          (and
            (BelowOf5 ?x)
            (AboveOf14 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf8 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf1 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf10 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf12 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf3 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf2 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf3 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf8 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf7 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf9 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf14 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf1 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf12 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf11 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf13 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf9 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf11 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf2 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf10 ?x)
          )
          (and
            (BelowOf13 ?x)
            (AboveOf14 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf6 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf14 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf5 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf6 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf7 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf4 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf4 ?x)
          )
        )
        (and
          (Row14 ?x)
        )
      )
    )
  )
  (:action CheckConsistencyAction
    :parameters ( )
    :precondition ( and
      (CheckConsistency)
      (not (Error))
    )
    :effect ( and
      (not (CheckConsistency))
      (when
        (or
          (exists (?x_24 )
            (and
              (RightOf10 ?x_24)
              (LeftOf1 ?x_24)
            )
          )
          (exists (?x_4 )
            (and
              (AboveOf12 ?x_4)
              (BelowOf6 ?x_4)
            )
          )
          (exists (?x_16 )
            (and
              (BelowOf4 ?x_16)
              (AboveOf12 ?x_16)
            )
          )
          (exists (?x_4 )
            (and
              (AboveOf13 ?x_4)
              (BelowOf6 ?x_4)
            )
          )
          (exists (?x_6 )
            (and
              (LeftOf12 ?x_6)
              (RightOf14 ?x_6)
            )
          )
          (exists (?x_26 )
            (and
              (LeftOf2 ?x_26)
              (RightOf13 ?x_26)
            )
          )
          (exists (?x_24 )
            (and
              (LeftOf6 ?x_24)
              (RightOf10 ?x_24)
            )
          )
          (exists (?x_14 )
            (and
              (LeftOf1 ?x_14)
              (RightOf11 ?x_14)
            )
          )
          (exists (?x_0 )
            (and
              (RightOf7 ?x_0)
              (LeftOf1 ?x_0)
            )
          )
          (exists (?x_24 )
            (and
              (RightOf14 ?x_24)
              (LeftOf4 ?x_24)
            )
          )
          (exists (?x_4 )
            (and
              (BelowOf3 ?x_4)
              (AboveOf14 ?x_4)
            )
          )
          (exists (?x_18 )
            (and
              (AboveOf9 ?x_18)
              (BelowOf4 ?x_18)
            )
          )
          (exists (?x_10 )
            (and
              (LeftOf3 ?x_10)
              (RightOf14 ?x_10)
            )
          )
          (exists (?x_17 )
            (and
              (BelowOf1 ?x_17)
              (AboveOf9 ?x_17)
            )
          )
          (exists (?x_27 )
            (and
              (LeftOf3 ?x_27)
              (RightOf3 ?x_27)
            )
          )
          (exists (?x_10 )
            (and
              (RightOf13 ?x_10)
              (LeftOf1 ?x_10)
            )
          )
          (exists (?x_6 )
            (and
              (LeftOf12 ?x_6)
              (RightOf13 ?x_6)
            )
          )
          (exists (?x_22 )
            (and
              (RightOf10 ?x_22)
              (LeftOf1 ?x_22)
            )
          )
          (exists (?x_25 )
            (and
              (RightOf12 ?x_25)
              (LeftOf12 ?x_25)
            )
          )
          (exists (?x_4 )
            (and
              (AboveOf13 ?x_4)
              (BelowOf5 ?x_4)
            )
          )
          (exists (?x_10 )
            (and
              (RightOf10 ?x_10)
              (LeftOf6 ?x_10)
            )
          )
          (exists (?x_23 )
            (and
              (BelowOf9 ?x_23)
              (AboveOf13 ?x_23)
            )
          )
          (exists (?x_20 )
            (and
              (BelowOf1 ?x_20)
              (AboveOf2 ?x_20)
            )
          )
          (exists (?x_19 )
            (and
              (LeftOf1 ?x_19)
              (RightOf11 ?x_19)
            )
          )
          (exists (?x_13 )
            (and
              (LeftOf5 ?x_13)
              (RightOf7 ?x_13)
            )
          )
          (exists (?x_11 )
            (and
              (AboveOf6 ?x_11)
              (BelowOf1 ?x_11)
            )
          )
          (exists (?x_21 )
            (and
              (AboveOf12 ?x_21)
              (BelowOf4 ?x_21)
            )
          )
          (exists (?x_11 )
            (and
              (AboveOf14 ?x_11)
              (BelowOf4 ?x_11)
            )
          )
          (exists (?x_4 )
            (and
              (AboveOf11 ?x_4)
              (BelowOf4 ?x_4)
            )
          )
          (exists (?x_9 )
            (and
              (AboveOf13 ?x_9)
              (BelowOf2 ?x_9)
            )
          )
          (exists (?x_2 )
            (and
              (AboveOf10 ?x_2)
              (BelowOf7 ?x_2)
            )
          )
          (exists (?x_26 )
            (and
              (LeftOf8 ?x_26)
              (RightOf10 ?x_26)
            )
          )
          (exists (?x_5 )
            (and
              (AboveOf13 ?x_5)
              (BelowOf1 ?x_5)
            )
          )
          (exists (?x_15 )
            (and
              (LeftOf1 ?x_15)
              (RightOf7 ?x_15)
            )
          )
          (exists (?x_23 )
            (and
              (AboveOf13 ?x_23)
              (BelowOf1 ?x_23)
            )
          )
          (exists (?x_24 )
            (and
              (RightOf12 ?x_24)
              (LeftOf4 ?x_24)
            )
          )
          (exists (?x_16 )
            (and
              (BelowOf1 ?x_16)
              (AboveOf9 ?x_16)
            )
          )
          (exists (?x_7 )
            (and
              (BelowOf3 ?x_7)
              (AboveOf6 ?x_7)
            )
          )
          (exists (?x_3 )
            (and
              (LeftOf7 ?x_3)
              (RightOf11 ?x_3)
            )
          )
          (exists (?x_4 )
            (and
              (BelowOf8 ?x_4)
              (AboveOf14 ?x_4)
            )
          )
          (exists (?x_13 )
            (and
              (RightOf8 ?x_13)
              (LeftOf3 ?x_13)
            )
          )
          (exists (?x_27 )
            (and
              (RightOf10 ?x_27)
              (LeftOf2 ?x_27)
            )
          )
          (exists (?x_18 )
            (and
              (BelowOf7 ?x_18)
              (AboveOf14 ?x_18)
            )
          )
          (exists (?x_9 )
            (and
              (BelowOf6 ?x_9)
              (AboveOf13 ?x_9)
            )
          )
          (exists (?x_26 )
            (and
              (LeftOf6 ?x_26)
              (RightOf13 ?x_26)
            )
          )
          (exists (?x_24 )
            (and
              (RightOf11 ?x_24)
              (LeftOf4 ?x_24)
            )
          )
          (exists (?x_24 )
            (and
              (RightOf13 ?x_24)
              (LeftOf1 ?x_24)
            )
          )
          (exists (?x_2 )
            (and
              (BelowOf5 ?x_2)
              (AboveOf14 ?x_2)
            )
          )
          (exists (?x_16 )
            (and
              (AboveOf8 ?x_16)
              (BelowOf6 ?x_16)
            )
          )
          (exists (?x_17 )
            (and
              (AboveOf4 ?x_17)
              (BelowOf1 ?x_17)
            )
          )
          (exists (?x_16 )
            (and
              (BelowOf5 ?x_16)
              (AboveOf10 ?x_16)
            )
          )
          (exists (?x_22 )
            (and
              (LeftOf4 ?x_22)
              (RightOf8 ?x_22)
            )
          )
          (exists (?x_1 )
            (and
              (BelowOf2 ?x_1)
              (AboveOf3 ?x_1)
            )
          )
          (exists (?x_5 )
            (and
              (AboveOf12 ?x_5)
              (BelowOf2 ?x_5)
            )
          )
          (exists (?x_18 )
            (and
              (BelowOf6 ?x_18)
              (AboveOf9 ?x_18)
            )
          )
          (exists (?x_20 )
            (and
              (BelowOf1 ?x_20)
              (AboveOf1 ?x_20)
            )
          )
          (exists (?x_24 )
            (and
              (RightOf12 ?x_24)
              (LeftOf1 ?x_24)
            )
          )
          (exists (?x_13 )
            (and
              (LeftOf1 ?x_13)
              (RightOf11 ?x_13)
            )
          )
          (exists (?x_1 )
            (and
              (BelowOf2 ?x_1)
              (AboveOf4 ?x_1)
            )
          )
          (exists (?x_2 )
            (and
              (BelowOf3 ?x_2)
              (AboveOf10 ?x_2)
            )
          )
          (exists (?x_1 )
            (and
              (BelowOf2 ?x_1)
              (AboveOf13 ?x_1)
            )
          )
          (exists (?x_15 )
            (and
              (LeftOf2 ?x_15)
              (RightOf12 ?x_15)
            )
          )
          (exists (?x_5 )
            (and
              (BelowOf7 ?x_5)
              (AboveOf11 ?x_5)
            )
          )
          (exists (?x_21 )
            (and
              (BelowOf3 ?x_21)
              (AboveOf13 ?x_21)
            )
          )
          (exists (?x_18 )
            (and
              (BelowOf3 ?x_18)
              (AboveOf9 ?x_18)
            )
          )
          (exists (?x_10 )
            (and
              (LeftOf1 ?x_10)
              (RightOf10 ?x_10)
            )
          )
          (exists (?x_1 )
            (and
              (BelowOf2 ?x_1)
              (AboveOf6 ?x_1)
            )
          )
          (exists (?x_4 )
            (and
              (AboveOf11 ?x_4)
              (BelowOf1 ?x_4)
            )
          )
          (exists (?x_3 )
            (and
              (RightOf14 ?x_3)
              (LeftOf3 ?x_3)
            )
          )
          (exists (?x_18 )
            (and
              (AboveOf9 ?x_18)
              (BelowOf1 ?x_18)
            )
          )
          (exists (?x_5 )
            (and
              (AboveOf14 ?x_5)
              (BelowOf3 ?x_5)
            )
          )
          (exists (?x_22 )
            (and
              (LeftOf1 ?x_22)
              (RightOf6 ?x_22)
            )
          )
          (exists (?x_9 )
            (and
              (BelowOf9 ?x_9)
              (AboveOf13 ?x_9)
            )
          )
          (exists (?x_14 )
            (and
              (LeftOf3 ?x_14)
              (RightOf12 ?x_14)
            )
          )
          (exists (?x_26 )
            (and
              (RightOf14 ?x_26)
              (LeftOf4 ?x_26)
            )
          )
          (exists (?x_3 )
            (and
              (LeftOf2 ?x_3)
              (RightOf12 ?x_3)
            )
          )
          (exists (?x_13 )
            (and
              (LeftOf1 ?x_13)
              (RightOf12 ?x_13)
            )
          )
          (exists (?x_5 )
            (and
              (BelowOf2 ?x_5)
              (AboveOf11 ?x_5)
            )
          )
          (exists (?x_22 )
            (and
              (LeftOf2 ?x_22)
              (RightOf11 ?x_22)
            )
          )
          (exists (?x_18 )
            (and
              (AboveOf8 ?x_18)
              (BelowOf4 ?x_18)
            )
          )
          (exists (?x_8 )
            (and
              (LeftOf10 ?x_8)
              (RightOf14 ?x_8)
            )
          )
          (exists (?x_24 )
            (and
              (RightOf11 ?x_24)
              (LeftOf1 ?x_24)
            )
          )
          (exists (?x_3 )
            (and
              (RightOf13 ?x_3)
              (LeftOf9 ?x_3)
            )
          )
          (exists (?x_18 )
            (and
              (BelowOf5 ?x_18)
              (AboveOf8 ?x_18)
            )
          )
          (exists (?x_18 )
            (and
              (BelowOf8 ?x_18)
              (AboveOf12 ?x_18)
            )
          )
          (exists (?x_17 )
            (and
              (BelowOf2 ?x_17)
              (AboveOf6 ?x_17)
            )
          )
          (exists (?x_0 )
            (and
              (LeftOf1 ?x_0)
              (RightOf12 ?x_0)
            )
          )
          (exists (?x_10 )
            (and
              (LeftOf6 ?x_10)
              (RightOf12 ?x_10)
            )
          )
          (exists (?x_2 )
            (and
              (BelowOf5 ?x_2)
              (AboveOf12 ?x_2)
            )
          )
          (exists (?x_13 )
            (and
              (LeftOf7 ?x_13)
              (RightOf7 ?x_13)
            )
          )
          (exists (?x_2 )
            (and
              (AboveOf9 ?x_2)
              (BelowOf8 ?x_2)
            )
          )
          (exists (?x_22 )
            (and
              (RightOf11 ?x_22)
              (LeftOf6 ?x_22)
            )
          )
          (exists (?x_21 )
            (and
              (BelowOf3 ?x_21)
              (AboveOf11 ?x_21)
            )
          )
          (exists (?x_11 )
            (and
              (BelowOf5 ?x_11)
              (AboveOf8 ?x_11)
            )
          )
          (exists (?x_21 )
            (and
              (AboveOf11 ?x_21)
              (BelowOf10 ?x_21)
            )
          )
          (exists (?x_19 )
            (and
              (RightOf1 ?x_19)
              (LeftOf1 ?x_19)
            )
          )
          (exists (?x_2 )
            (and
              (AboveOf13 ?x_2)
              (BelowOf2 ?x_2)
            )
          )
          (exists (?x_10 )
            (and
              (LeftOf6 ?x_10)
              (RightOf11 ?x_10)
            )
          )
          (exists (?x_18 )
            (and
              (AboveOf14 ?x_18)
              (BelowOf8 ?x_18)
            )
          )
          (exists (?x_15 )
            (and
              (RightOf7 ?x_15)
              (LeftOf2 ?x_15)
            )
          )
          (exists (?x_2 )
            (and
              (AboveOf9 ?x_2)
              (BelowOf7 ?x_2)
            )
          )
          (exists (?x_10 )
            (and
              (LeftOf4 ?x_10)
              (RightOf11 ?x_10)
            )
          )
          (exists (?x_21 )
            (and
              (AboveOf11 ?x_21)
              (BelowOf2 ?x_21)
            )
          )
          (exists (?x_23 )
            (and
              (AboveOf14 ?x_23)
              (BelowOf2 ?x_23)
            )
          )
          (exists (?x_11 )
            (and
              (BelowOf1 ?x_11)
              (AboveOf10 ?x_11)
            )
          )
          (exists (?x_6 )
            (and
              (LeftOf2 ?x_6)
              (RightOf14 ?x_6)
            )
          )
          (exists (?x_11 )
            (and
              (BelowOf3 ?x_11)
              (AboveOf10 ?x_11)
            )
          )
          (exists (?x_10 )
            (and
              (RightOf13 ?x_10)
              (LeftOf10 ?x_10)
            )
          )
          (exists (?x_13 )
            (and
              (LeftOf6 ?x_13)
              (RightOf12 ?x_13)
            )
          )
          (exists (?x_26 )
            (and
              (RightOf14 ?x_26)
              (LeftOf7 ?x_26)
            )
          )
          (exists (?x_3 )
            (and
              (LeftOf9 ?x_3)
              (RightOf12 ?x_3)
            )
          )
          (exists (?x_21 )
            (and
              (BelowOf6 ?x_21)
              (AboveOf14 ?x_21)
            )
          )
          (exists (?x_27 )
            (and
              (RightOf12 ?x_27)
              (LeftOf2 ?x_27)
            )
          )
          (exists (?x_25 )
            (and
              (LeftOf6 ?x_25)
              (RightOf13 ?x_25)
            )
          )
          (exists (?x_21 )
            (and
              (AboveOf12 ?x_21)
              (BelowOf8 ?x_21)
            )
          )
          (exists (?x_27 )
            (and
              (LeftOf1 ?x_27)
              (RightOf10 ?x_27)
            )
          )
          (exists (?x_13 )
            (and
              (LeftOf7 ?x_13)
              (RightOf11 ?x_13)
            )
          )
          (exists (?x_25 )
            (and
              (RightOf14 ?x_25)
              (LeftOf9 ?x_25)
            )
          )
          (exists (?x_26 )
            (and
              (LeftOf1 ?x_26)
              (RightOf13 ?x_26)
            )
          )
          (exists (?x_9 )
            (and
              (BelowOf5 ?x_9)
              (AboveOf14 ?x_9)
            )
          )
          (exists (?x_25 )
            (and
              (RightOf14 ?x_25)
              (LeftOf1 ?x_25)
            )
          )
          (exists (?x_24 )
            (and
              (LeftOf7 ?x_24)
              (RightOf13 ?x_24)
            )
          )
          (exists (?x_21 )
            (and
              (AboveOf14 ?x_21)
              (BelowOf1 ?x_21)
            )
          )
          (exists (?x_13 )
            (and
              (LeftOf2 ?x_13)
              (RightOf12 ?x_13)
            )
          )
          (exists (?x_5 )
            (and
              (AboveOf12 ?x_5)
              (BelowOf7 ?x_5)
            )
          )
          (exists (?x_24 )
            (and
              (RightOf14 ?x_24)
              (LeftOf2 ?x_24)
            )
          )
          (exists (?x_22 )
            (and
              (LeftOf6 ?x_22)
              (RightOf12 ?x_22)
            )
          )
          (exists (?x_21 )
            (and
              (AboveOf14 ?x_21)
              (BelowOf4 ?x_21)
            )
          )
          (exists (?x_2 )
            (and
              (BelowOf4 ?x_2)
              (AboveOf12 ?x_2)
            )
          )
          (exists (?x_10 )
            (and
              (LeftOf1 ?x_10)
              (RightOf11 ?x_10)
            )
          )
          (exists (?x_15 )
            (and
              (LeftOf4 ?x_15)
              (RightOf6 ?x_15)
            )
          )
          (exists (?x_9 )
            (and
              (BelowOf13 ?x_9)
              (AboveOf13 ?x_9)
            )
          )
          (exists (?x_11 )
            (and
              (AboveOf14 ?x_11)
              (BelowOf2 ?x_11)
            )
          )
          (exists (?x_0 )
            (and
              (RightOf13 ?x_0)
              (LeftOf2 ?x_0)
            )
          )
          (exists (?x_18 )
            (and
              (AboveOf14 ?x_18)
              (BelowOf5 ?x_18)
            )
          )
          (exists (?x_10 )
            (and
              (LeftOf7 ?x_10)
              (RightOf10 ?x_10)
            )
          )
          (exists (?x_18 )
            (and
              (BelowOf2 ?x_18)
              (AboveOf13 ?x_18)
            )
          )
          (exists (?x_21 )
            (and
              (BelowOf4 ?x_21)
              (AboveOf13 ?x_21)
            )
          )
          (exists (?x_5 )
            (and
              (AboveOf12 ?x_5)
              (BelowOf1 ?x_5)
            )
          )
          (exists (?x_26 )
            (and
              (RightOf12 ?x_26)
              (LeftOf6 ?x_26)
            )
          )
          (exists (?x_11 )
            (and
              (AboveOf13 ?x_11)
              (BelowOf4 ?x_11)
            )
          )
          (exists (?x_17 )
            (and
              (AboveOf4 ?x_17)
              (BelowOf3 ?x_17)
            )
          )
          (exists (?x_13 )
            (and
              (LeftOf7 ?x_13)
              (RightOf14 ?x_13)
            )
          )
          (exists (?x_5 )
            (and
              (AboveOf8 ?x_5)
              (BelowOf7 ?x_5)
            )
          )
          (exists (?x_13 )
            (and
              (RightOf13 ?x_13)
              (LeftOf5 ?x_13)
            )
          )
          (exists (?x_9 )
            (and
              (BelowOf11 ?x_9)
              (AboveOf14 ?x_9)
            )
          )
          (exists (?x_12 )
            (and
              (AboveOf14 ?x_12)
              (BelowOf13 ?x_12)
            )
          )
          (exists (?x_7 )
            (and
              (BelowOf2 ?x_7)
              (AboveOf6 ?x_7)
            )
          )
          (exists (?x_4 )
            (and
              (AboveOf10 ?x_4)
              (BelowOf6 ?x_4)
            )
          )
          (exists (?x_26 )
            (and
              (RightOf12 ?x_26)
              (LeftOf4 ?x_26)
            )
          )
          (exists (?x_7 )
            (and
              (AboveOf11 ?x_7)
              (BelowOf4 ?x_7)
            )
          )
          (exists (?x_17 )
            (and
              (BelowOf2 ?x_17)
              (AboveOf4 ?x_17)
            )
          )
          (exists (?x_27 )
            (and
              (RightOf7 ?x_27)
              (LeftOf1 ?x_27)
            )
          )
          (exists (?x_21 )
            (and
              (AboveOf13 ?x_21)
              (BelowOf2 ?x_21)
            )
          )
          (exists (?x_1 )
            (and
              (BelowOf1 ?x_1)
              (AboveOf9 ?x_1)
            )
          )
          (exists (?x_10 )
            (and
              (RightOf13 ?x_10)
              (LeftOf9 ?x_10)
            )
          )
          (exists (?x_24 )
            (and
              (RightOf9 ?x_24)
              (LeftOf7 ?x_24)
            )
          )
          (exists (?x_0 )
            (and
              (LeftOf1 ?x_0)
              (RightOf9 ?x_0)
            )
          )
          (exists (?x_4 )
            (and
              (BelowOf7 ?x_4)
              (AboveOf11 ?x_4)
            )
          )
          (exists (?x_5 )
            (and
              (BelowOf3 ?x_5)
              (AboveOf11 ?x_5)
            )
          )
          (exists (?x_18 )
            (and
              (AboveOf13 ?x_18)
              (BelowOf5 ?x_18)
            )
          )
          (exists (?x_1 )
            (and
              (BelowOf1 ?x_1)
              (AboveOf3 ?x_1)
            )
          )
          (exists (?x_13 )
            (and
              (LeftOf1 ?x_13)
              (RightOf9 ?x_13)
            )
          )
          (exists (?x_23 )
            (and
              (BelowOf7 ?x_23)
              (AboveOf13 ?x_23)
            )
          )
          (exists (?x_23 )
            (and
              (BelowOf6 ?x_23)
              (AboveOf14 ?x_23)
            )
          )
          (exists (?x_18 )
            (and
              (BelowOf7 ?x_18)
              (AboveOf8 ?x_18)
            )
          )
          (exists (?x_7 )
            (and
              (BelowOf1 ?x_7)
              (AboveOf13 ?x_7)
            )
          )
          (exists (?x_20 )
            (and
              (BelowOf1 ?x_20)
              (AboveOf9 ?x_20)
            )
          )
          (exists (?x_13 )
            (and
              (RightOf10 ?x_13)
              (LeftOf4 ?x_13)
            )
          )
          (exists (?x_14 )
            (and
              (LeftOf1 ?x_14)
              (RightOf6 ?x_14)
            )
          )
          (exists (?x_25 )
            (and
              (RightOf12 ?x_25)
              (LeftOf11 ?x_25)
            )
          )
          (exists (?x_7 )
            (and
              (BelowOf2 ?x_7)
              (AboveOf9 ?x_7)
            )
          )
          (exists (?x_21 )
            (and
              (BelowOf5 ?x_21)
              (AboveOf13 ?x_21)
            )
          )
          (exists (?x_5 )
            (and
              (BelowOf3 ?x_5)
              (AboveOf10 ?x_5)
            )
          )
          (exists (?x_14 )
            (and
              (RightOf9 ?x_14)
              (LeftOf2 ?x_14)
            )
          )
          (exists (?x_5 )
            (and
              (BelowOf5 ?x_5)
              (AboveOf11 ?x_5)
            )
          )
          (exists (?x_5 )
            (and
              (BelowOf1 ?x_5)
              (AboveOf8 ?x_5)
            )
          )
          (exists (?x_0 )
            (and
              (LeftOf2 ?x_0)
              (RightOf9 ?x_0)
            )
          )
          (exists (?x_1 )
            (and
              (AboveOf2 ?x_1)
              (BelowOf2 ?x_1)
            )
          )
          (exists (?x_22 )
            (and
              (LeftOf2 ?x_22)
              (RightOf12 ?x_22)
            )
          )
          (exists (?x_16 )
            (and
              (BelowOf5 ?x_16)
              (AboveOf13 ?x_16)
            )
          )
          (exists (?x_1 )
            (and
              (BelowOf2 ?x_1)
              (AboveOf9 ?x_1)
            )
          )
          (exists (?x_16 )
            (and
              (AboveOf8 ?x_16)
              (BelowOf4 ?x_16)
            )
          )
          (exists (?x_13 )
            (and
              (LeftOf4 ?x_13)
              (RightOf12 ?x_13)
            )
          )
          (exists (?x_3 )
            (and
              (LeftOf7 ?x_3)
              (RightOf13 ?x_3)
            )
          )
          (exists (?x_6 )
            (and
              (LeftOf4 ?x_6)
              (RightOf13 ?x_6)
            )
          )
          (exists (?x_6 )
            (and
              (LeftOf10 ?x_6)
              (RightOf14 ?x_6)
            )
          )
          (exists (?x_23 )
            (and
              (AboveOf14 ?x_23)
              (BelowOf12 ?x_23)
            )
          )
          (exists (?x_13 )
            (and
              (RightOf8 ?x_13)
              (LeftOf1 ?x_13)
            )
          )
          (exists (?x_7 )
            (and
              (AboveOf9 ?x_7)
              (BelowOf1 ?x_7)
            )
          )
          (exists (?x_3 )
            (and
              (RightOf14 ?x_3)
              (LeftOf4 ?x_3)
            )
          )
          (exists (?x_14 )
            (and
              (LeftOf4 ?x_14)
              (RightOf14 ?x_14)
            )
          )
          (exists (?x_14 )
            (and
              (RightOf14 ?x_14)
              (LeftOf5 ?x_14)
            )
          )
          (exists (?x_6 )
            (and
              (LeftOf5 ?x_6)
              (RightOf13 ?x_6)
            )
          )
          (exists (?x_24 )
            (and
              (RightOf9 ?x_24)
              (LeftOf1 ?x_24)
            )
          )
          (exists (?x_3 )
            (and
              (LeftOf3 ?x_3)
              (RightOf12 ?x_3)
            )
          )
          (exists (?x_15 )
            (and
              (LeftOf4 ?x_15)
              (RightOf8 ?x_15)
            )
          )
          (exists (?x_5 )
            (and
              (BelowOf6 ?x_5)
              (AboveOf12 ?x_5)
            )
          )
          (exists (?x_18 )
            (and
              (BelowOf8 ?x_18)
              (AboveOf9 ?x_18)
            )
          )
          (exists (?x_1 )
            (and
              (AboveOf2 ?x_1)
              (BelowOf1 ?x_1)
            )
          )
          (exists (?x_18 )
            (and
              (BelowOf4 ?x_18)
              (AboveOf12 ?x_18)
            )
          )
          (exists (?x_21 )
            (and
              (BelowOf8 ?x_21)
              (AboveOf13 ?x_21)
            )
          )
          (exists (?x_26 )
            (and
              (RightOf13 ?x_26)
              (LeftOf7 ?x_26)
            )
          )
          (exists (?x_12 )
            (and
              (AboveOf14 ?x_12)
              (BelowOf9 ?x_12)
            )
          )
          (exists (?x_11 )
            (and
              (BelowOf3 ?x_11)
              (AboveOf7 ?x_11)
            )
          )
          (exists (?x_14 )
            (and
              (LeftOf4 ?x_14)
              (RightOf12 ?x_14)
            )
          )
          (exists (?x_10 )
            (and
              (LeftOf2 ?x_10)
              (RightOf11 ?x_10)
            )
          )
          (exists (?x_5 )
            (and
              (AboveOf12 ?x_5)
              (BelowOf3 ?x_5)
            )
          )
          (exists (?x_21 )
            (and
              (BelowOf6 ?x_21)
              (AboveOf13 ?x_21)
            )
          )
          (exists (?x_22 )
            (and
              (LeftOf1 ?x_22)
              (RightOf12 ?x_22)
            )
          )
          (exists (?x_0 )
            (and
              (LeftOf2 ?x_0)
              (RightOf11 ?x_0)
            )
          )
          (exists (?x_10 )
            (and
              (RightOf13 ?x_10)
              (LeftOf4 ?x_10)
            )
          )
          (exists (?x_8 )
            (and
              (LeftOf3 ?x_8)
              (RightOf14 ?x_8)
            )
          )
          (exists (?x_25 )
            (and
              (LeftOf3 ?x_25)
              (RightOf14 ?x_25)
            )
          )
          (exists (?x_23 )
            (and
              (AboveOf14 ?x_23)
              (BelowOf5 ?x_23)
            )
          )
          (exists (?x_18 )
            (and
              (AboveOf13 ?x_18)
              (BelowOf8 ?x_18)
            )
          )
          (exists (?x_23 )
            (and
              (AboveOf13 ?x_23)
              (BelowOf9 ?x_23)
            )
          )
          (exists (?x_22 )
            (and
              (RightOf10 ?x_22)
              (LeftOf6 ?x_22)
            )
          )
          (exists (?x_4 )
            (and
              (AboveOf11 ?x_4)
              (BelowOf2 ?x_4)
            )
          )
          (exists (?x_1 )
            (and
              (BelowOf2 ?x_1)
              (AboveOf12 ?x_1)
            )
          )
          (exists (?x_9 )
            (and
              (BelowOf3 ?x_9)
              (AboveOf13 ?x_9)
            )
          )
          (exists (?x_11 )
            (and
              (AboveOf8 ?x_11)
              (BelowOf1 ?x_11)
            )
          )
          (exists (?x_15 )
            (and
              (LeftOf1 ?x_15)
              (RightOf13 ?x_15)
            )
          )
          (exists (?x_19 )
            (and
              (RightOf10 ?x_19)
              (LeftOf1 ?x_19)
            )
          )
          (exists (?x_21 )
            (and
              (AboveOf14 ?x_21)
              (BelowOf9 ?x_21)
            )
          )
          (exists (?x_15 )
            (and
              (LeftOf3 ?x_15)
              (RightOf9 ?x_15)
            )
          )
          (exists (?x_14 )
            (and
              (LeftOf3 ?x_14)
              (RightOf9 ?x_14)
            )
          )
          (exists (?x_4 )
            (and
              (BelowOf10 ?x_4)
              (AboveOf10 ?x_4)
            )
          )
          (exists (?x_2 )
            (and
              (AboveOf14 ?x_2)
              (BelowOf8 ?x_2)
            )
          )
          (exists (?x_21 )
            (and
              (BelowOf5 ?x_21)
              (AboveOf14 ?x_21)
            )
          )
          (exists (?x_26 )
            (and
              (RightOf12 ?x_26)
              (LeftOf1 ?x_26)
            )
          )
          (exists (?x_25 )
            (and
              (RightOf13 ?x_25)
              (LeftOf12 ?x_25)
            )
          )
          (exists (?x_5 )
            (and
              (BelowOf4 ?x_5)
              (AboveOf10 ?x_5)
            )
          )
          (exists (?x_7 )
            (and
              (BelowOf1 ?x_7)
              (AboveOf6 ?x_7)
            )
          )
          (exists (?x_8 )
            (and
              (LeftOf7 ?x_8)
              (RightOf14 ?x_8)
            )
          )
          (exists (?x_16 )
            (and
              (AboveOf11 ?x_16)
              (BelowOf1 ?x_16)
            )
          )
          (exists (?x_3 )
            (and
              (RightOf13 ?x_3)
              (LeftOf10 ?x_3)
            )
          )
          (exists (?x_18 )
            (and
              (AboveOf8 ?x_18)
              (BelowOf1 ?x_18)
            )
          )
          (exists (?x_3 )
            (and
              (LeftOf8 ?x_3)
              (RightOf12 ?x_3)
            )
          )
          (exists (?x_21 )
            (and
              (BelowOf6 ?x_21)
              (AboveOf11 ?x_21)
            )
          )
          (exists (?x_24 )
            (and
              (LeftOf8 ?x_24)
              (RightOf13 ?x_24)
            )
          )
          (exists (?x_17 )
            (and
              (BelowOf1 ?x_17)
              (AboveOf6 ?x_17)
            )
          )
          (exists (?x_0 )
            (and
              (RightOf14 ?x_0)
              (LeftOf2 ?x_0)
            )
          )
          (exists (?x_11 )
            (and
              (AboveOf11 ?x_11)
              (BelowOf4 ?x_11)
            )
          )
          (exists (?x_27 )
            (and
              (RightOf9 ?x_27)
              (LeftOf1 ?x_27)
            )
          )
          (exists (?x_18 )
            (and
              (AboveOf8 ?x_18)
              (BelowOf3 ?x_18)
            )
          )
          (exists (?x_26 )
            (and
              (RightOf9 ?x_26)
              (LeftOf5 ?x_26)
            )
          )
          (exists (?x_24 )
            (and
              (RightOf13 ?x_24)
              (LeftOf7 ?x_24)
            )
          )
          (exists (?x_8 )
            (and
              (LeftOf5 ?x_8)
              (RightOf14 ?x_8)
            )
          )
          (exists (?x_13 )
            (and
              (RightOf10 ?x_13)
              (LeftOf5 ?x_13)
            )
          )
          (exists (?x_25 )
            (and
              (RightOf12 ?x_25)
              (LeftOf6 ?x_25)
            )
          )
          (exists (?x_24 )
            (and
              (RightOf8 ?x_24)
              (LeftOf4 ?x_24)
            )
          )
          (exists (?x_18 )
            (and
              (BelowOf5 ?x_18)
              (AboveOf9 ?x_18)
            )
          )
          (exists (?x_17 )
            (and
              (AboveOf8 ?x_17)
              (BelowOf2 ?x_17)
            )
          )
          (exists (?x_23 )
            (and
              (BelowOf3 ?x_23)
              (AboveOf12 ?x_23)
            )
          )
          (exists (?x_0 )
            (and
              (RightOf3 ?x_0)
              (LeftOf2 ?x_0)
            )
          )
          (exists (?x_21 )
            (and
              (AboveOf14 ?x_21)
              (BelowOf7 ?x_21)
            )
          )
          (exists (?x_0 )
            (and
              (RightOf7 ?x_0)
              (LeftOf2 ?x_0)
            )
          )
          (exists (?x_7 )
            (and
              (BelowOf4 ?x_7)
              (AboveOf5 ?x_7)
            )
          )
          (exists (?x_13 )
            (and
              (RightOf10 ?x_13)
              (LeftOf2 ?x_13)
            )
          )
          (exists (?x_9 )
            (and
              (AboveOf14 ?x_9)
              (BelowOf1 ?x_9)
            )
          )
          (exists (?x_26 )
            (and
              (RightOf10 ?x_26)
              (LeftOf9 ?x_26)
            )
          )
          (exists (?x_4 )
            (and
              (AboveOf13 ?x_4)
              (BelowOf2 ?x_4)
            )
          )
          (exists (?x_26 )
            (and
              (RightOf11 ?x_26)
              (LeftOf7 ?x_26)
            )
          )
          (exists (?x_12 )
            (and
              (AboveOf14 ?x_12)
              (BelowOf12 ?x_12)
            )
          )
          (exists (?x_14 )
            (and
              (RightOf5 ?x_14)
              (LeftOf1 ?x_14)
            )
          )
          (exists (?x_4 )
            (and
              (BelowOf5 ?x_4)
              (AboveOf14 ?x_4)
            )
          )
          (exists (?x_22 )
            (and
              (RightOf6 ?x_22)
              (LeftOf6 ?x_22)
            )
          )
          (exists (?x_5 )
            (and
              (BelowOf4 ?x_5)
              (AboveOf7 ?x_5)
            )
          )
          (exists (?x_6 )
            (and
              (LeftOf1 ?x_6)
              (RightOf13 ?x_6)
            )
          )
          (exists (?x_22 )
            (and
              (LeftOf4 ?x_22)
              (RightOf12 ?x_22)
            )
          )
          (exists (?x_5 )
            (and
              (AboveOf14 ?x_5)
              (BelowOf6 ?x_5)
            )
          )
          (exists (?x_15 )
            (and
              (RightOf5 ?x_15)
              (LeftOf3 ?x_15)
            )
          )
          (exists (?x_15 )
            (and
              (RightOf5 ?x_15)
              (LeftOf2 ?x_15)
            )
          )
          (exists (?x_10 )
            (and
              (LeftOf9 ?x_10)
              (RightOf11 ?x_10)
            )
          )
          (exists (?x_15 )
            (and
              (RightOf8 ?x_15)
              (LeftOf3 ?x_15)
            )
          )
          (exists (?x_11 )
            (and
              (AboveOf12 ?x_11)
              (BelowOf3 ?x_11)
            )
          )
          (exists (?x_10 )
            (and
              (LeftOf7 ?x_10)
              (RightOf13 ?x_10)
            )
          )
          (exists (?x_9 )
            (and
              (AboveOf14 ?x_9)
              (BelowOf4 ?x_9)
            )
          )
          (exists (?x_5 )
            (and
              (BelowOf5 ?x_5)
              (AboveOf8 ?x_5)
            )
          )
          (exists (?x_26 )
            (and
              (RightOf14 ?x_26)
              (LeftOf8 ?x_26)
            )
          )
          (exists (?x_19 )
            (and
              (RightOf6 ?x_19)
              (LeftOf1 ?x_19)
            )
          )
          (exists (?x_18 )
            (and
              (BelowOf3 ?x_18)
              (AboveOf10 ?x_18)
            )
          )
          (exists (?x_16 )
            (and
              (AboveOf11 ?x_16)
              (BelowOf2 ?x_16)
            )
          )
          (exists (?x_0 )
            (and
              (RightOf14 ?x_0)
              (LeftOf1 ?x_0)
            )
          )
          (exists (?x_24 )
            (and
              (LeftOf8 ?x_24)
              (RightOf14 ?x_24)
            )
          )
          (exists (?x_11 )
            (and
              (BelowOf5 ?x_11)
              (AboveOf14 ?x_11)
            )
          )
          (exists (?x_26 )
            (and
              (RightOf10 ?x_26)
              (LeftOf7 ?x_26)
            )
          )
          (exists (?x_11 )
            (and
              (AboveOf8 ?x_11)
              (BelowOf3 ?x_11)
            )
          )
          (exists (?x_6 )
            (and
              (LeftOf5 ?x_6)
              (RightOf14 ?x_6)
            )
          )
          (exists (?x_24 )
            (and
              (RightOf12 ?x_24)
              (LeftOf6 ?x_24)
            )
          )
          (exists (?x_10 )
            (and
              (RightOf13 ?x_10)
              (LeftOf6 ?x_10)
            )
          )
          (exists (?x_1 )
            (and
              (BelowOf1 ?x_1)
              (AboveOf8 ?x_1)
            )
          )
          (exists (?x_17 )
            (and
              (AboveOf12 ?x_17)
              (BelowOf3 ?x_17)
            )
          )
          (exists (?x_16 )
            (and
              (BelowOf6 ?x_16)
              (AboveOf6 ?x_16)
            )
          )
          (exists (?x_13 )
            (and
              (LeftOf2 ?x_13)
              (RightOf11 ?x_13)
            )
          )
          (exists (?x_21 )
            (and
              (AboveOf14 ?x_21)
              (BelowOf11 ?x_21)
            )
          )
          (exists (?x_15 )
            (and
              (LeftOf4 ?x_15)
              (RightOf7 ?x_15)
            )
          )
          (exists (?x_15 )
            (and
              (LeftOf4 ?x_15)
              (RightOf9 ?x_15)
            )
          )
          (exists (?x_20 )
            (and
              (BelowOf1 ?x_20)
              (AboveOf13 ?x_20)
            )
          )
          (exists (?x_3 )
            (and
              (LeftOf5 ?x_3)
              (RightOf12 ?x_3)
            )
          )
          (exists (?x_4 )
            (and
              (AboveOf11 ?x_4)
              (BelowOf5 ?x_4)
            )
          )
          (exists (?x_14 )
            (and
              (RightOf8 ?x_14)
              (LeftOf4 ?x_14)
            )
          )
          (exists (?x_7 )
            (and
              (AboveOf11 ?x_7)
              (BelowOf1 ?x_7)
            )
          )
          (exists (?x_14 )
            (and
              (RightOf10 ?x_14)
              (LeftOf5 ?x_14)
            )
          )
          (exists (?x_26 )
            (and
              (LeftOf8 ?x_26)
              (RightOf14 ?x_26)
            )
          )
          (exists (?x_22 )
            (and
              (LeftOf3 ?x_22)
              (RightOf7 ?x_22)
            )
          )
          (exists (?x_5 )
            (and
              (AboveOf10 ?x_5)
              (BelowOf7 ?x_5)
            )
          )
          (exists (?x_23 )
            (and
              (BelowOf5 ?x_23)
              (AboveOf13 ?x_23)
            )
          )
          (exists (?x_0 )
            (and
              (RightOf12 ?x_0)
              (LeftOf2 ?x_0)
            )
          )
          (exists (?x_17 )
            (and
              (BelowOf2 ?x_17)
              (AboveOf3 ?x_17)
            )
          )
          (exists (?x_15 )
            (and
              (LeftOf3 ?x_15)
              (RightOf10 ?x_15)
            )
          )
          (exists (?x_22 )
            (and
              (RightOf8 ?x_22)
              (LeftOf1 ?x_22)
            )
          )
          (exists (?x_16 )
            (and
              (BelowOf5 ?x_16)
              (AboveOf6 ?x_16)
            )
          )
          (exists (?x_14 )
            (and
              (RightOf13 ?x_14)
              (LeftOf2 ?x_14)
            )
          )
          (exists (?x_18 )
            (and
              (BelowOf7 ?x_18)
              (AboveOf9 ?x_18)
            )
          )
          (exists (?x_5 )
            (and
              (BelowOf2 ?x_5)
              (AboveOf8 ?x_5)
            )
          )
          (exists (?x_4 )
            (and
              (BelowOf9 ?x_4)
              (AboveOf13 ?x_4)
            )
          )
          (exists (?x_6 )
            (and
              (RightOf13 ?x_6)
              (LeftOf7 ?x_6)
            )
          )
          (exists (?x_4 )
            (and
              (AboveOf11 ?x_4)
              (BelowOf8 ?x_4)
            )
          )
          (exists (?x_5 )
            (and
              (BelowOf5 ?x_5)
              (AboveOf12 ?x_5)
            )
          )
          (exists (?x_15 )
            (and
              (RightOf11 ?x_15)
              (LeftOf3 ?x_15)
            )
          )
          (exists (?x_19 )
            (and
              (LeftOf1 ?x_19)
              (RightOf13 ?x_19)
            )
          )
          (exists (?x_14 )
            (and
              (LeftOf5 ?x_14)
              (RightOf6 ?x_14)
            )
          )
          (exists (?x_5 )
            (and
              (BelowOf4 ?x_5)
              (AboveOf8 ?x_5)
            )
          )
          (exists (?x_7 )
            (and
              (AboveOf8 ?x_7)
              (BelowOf3 ?x_7)
            )
          )
          (exists (?x_17 )
            (and
              (AboveOf14 ?x_17)
              (BelowOf1 ?x_17)
            )
          )
          (exists (?x_13 )
            (and
              (RightOf8 ?x_13)
              (LeftOf5 ?x_13)
            )
          )
          (exists (?x_21 )
            (and
              (AboveOf13 ?x_21)
              (BelowOf10 ?x_21)
            )
          )
          (exists (?x_26 )
            (and
              (LeftOf5 ?x_26)
              (RightOf12 ?x_26)
            )
          )
          (exists (?x_18 )
            (and
              (BelowOf8 ?x_18)
              (AboveOf8 ?x_18)
            )
          )
          (exists (?x_2 )
            (and
              (BelowOf3 ?x_2)
              (AboveOf12 ?x_2)
            )
          )
          (exists (?x_11 )
            (and
              (AboveOf9 ?x_11)
              (BelowOf4 ?x_11)
            )
          )
          (exists (?x_3 )
            (and
              (LeftOf7 ?x_3)
              (RightOf12 ?x_3)
            )
          )
          (exists (?x_16 )
            (and
              (AboveOf11 ?x_16)
              (BelowOf3 ?x_16)
            )
          )
          (exists (?x_11 )
            (and
              (BelowOf1 ?x_11)
              (AboveOf9 ?x_11)
            )
          )
          (exists (?x_2 )
            (and
              (BelowOf4 ?x_2)
              (AboveOf11 ?x_2)
            )
          )
          (exists (?x_27 )
            (and
              (LeftOf1 ?x_27)
              (RightOf8 ?x_27)
            )
          )
          (exists (?x_5 )
            (and
              (BelowOf3 ?x_5)
              (AboveOf9 ?x_5)
            )
          )
          (exists (?x_21 )
            (and
              (AboveOf11 ?x_21)
              (BelowOf1 ?x_21)
            )
          )
          (exists (?x_5 )
            (and
              (BelowOf6 ?x_5)
              (AboveOf10 ?x_5)
            )
          )
          (exists (?x_3 )
            (and
              (RightOf13 ?x_3)
              (LeftOf3 ?x_3)
            )
          )
          (exists (?x_15 )
            (and
              (LeftOf1 ?x_15)
              (RightOf11 ?x_15)
            )
          )
          (exists (?x_10 )
            (and
              (LeftOf5 ?x_10)
              (RightOf11 ?x_10)
            )
          )
          (exists (?x_16 )
            (and
              (AboveOf9 ?x_16)
              (BelowOf3 ?x_16)
            )
          )
          (exists (?x_5 )
            (and
              (BelowOf2 ?x_5)
              (AboveOf9 ?x_5)
            )
          )
          (exists (?x_7 )
            (and
              (BelowOf1 ?x_7)
              (AboveOf5 ?x_7)
            )
          )
          (exists (?x_22 )
            (and
              (LeftOf1 ?x_22)
              (RightOf14 ?x_22)
            )
          )
          (exists (?x_26 )
            (and
              (RightOf9 ?x_26)
              (LeftOf4 ?x_26)
            )
          )
          (exists (?x_23 )
            (and
              (BelowOf11 ?x_23)
              (AboveOf13 ?x_23)
            )
          )
          (exists (?x_13 )
            (and
              (LeftOf5 ?x_13)
              (RightOf12 ?x_13)
            )
          )
          (exists (?x_5 )
            (and
              (BelowOf3 ?x_5)
              (AboveOf7 ?x_5)
            )
          )
          (exists (?x_27 )
            (and
              (LeftOf3 ?x_27)
              (RightOf13 ?x_27)
            )
          )
          (exists (?x_14 )
            (and
              (LeftOf4 ?x_14)
              (RightOf8 ?x_14)
            )
          )
          (exists (?x_11 )
            (and
              (AboveOf5 ?x_11)
              (BelowOf4 ?x_11)
            )
          )
          (exists (?x_4 )
            (and
              (BelowOf7 ?x_4)
              (AboveOf14 ?x_4)
            )
          )
          (exists (?x_26 )
            (and
              (RightOf11 ?x_26)
              (LeftOf4 ?x_26)
            )
          )
          (exists (?x_14 )
            (and
              (RightOf5 ?x_14)
              (LeftOf5 ?x_14)
            )
          )
          (exists (?x_2 )
            (and
              (BelowOf4 ?x_2)
              (AboveOf13 ?x_2)
            )
          )
          (exists (?x_24 )
            (and
              (RightOf11 ?x_24)
              (LeftOf2 ?x_24)
            )
          )
          (exists (?x_21 )
            (and
              (AboveOf11 ?x_21)
              (BelowOf7 ?x_21)
            )
          )
          (exists (?x_16 )
            (and
              (AboveOf8 ?x_16)
              (BelowOf1 ?x_16)
            )
          )
          (exists (?x_13 )
            (and
              (RightOf8 ?x_13)
              (LeftOf7 ?x_13)
            )
          )
          (exists (?x_4 )
            (and
              (AboveOf11 ?x_4)
              (BelowOf10 ?x_4)
            )
          )
          (exists (?x_10 )
            (and
              (RightOf13 ?x_10)
              (LeftOf2 ?x_10)
            )
          )
          (exists (?x_25 )
            (and
              (LeftOf8 ?x_25)
              (RightOf14 ?x_25)
            )
          )
          (exists (?x_24 )
            (and
              (LeftOf3 ?x_24)
              (RightOf10 ?x_24)
            )
          )
          (exists (?x_4 )
            (and
              (BelowOf7 ?x_4)
              (AboveOf12 ?x_4)
            )
          )
          (exists (?x_7 )
            (and
              (BelowOf2 ?x_7)
              (AboveOf4 ?x_7)
            )
          )
          (exists (?x_4 )
            (and
              (BelowOf2 ?x_4)
              (AboveOf14 ?x_4)
            )
          )
          (exists (?x_13 )
            (and
              (LeftOf7 ?x_13)
              (RightOf13 ?x_13)
            )
          )
          (exists (?x_11 )
            (and
              (AboveOf9 ?x_11)
              (BelowOf2 ?x_11)
            )
          )
          (exists (?x_7 )
            (and
              (AboveOf11 ?x_7)
              (BelowOf2 ?x_7)
            )
          )
          (exists (?x_5 )
            (and
              (BelowOf6 ?x_5)
              (AboveOf11 ?x_5)
            )
          )
          (exists (?x_19 )
            (and
              (LeftOf1 ?x_19)
              (RightOf7 ?x_19)
            )
          )
          (exists (?x_16 )
            (and
              (AboveOf13 ?x_16)
              (BelowOf4 ?x_16)
            )
          )
          (exists (?x_24 )
            (and
              (LeftOf2 ?x_24)
              (RightOf10 ?x_24)
            )
          )
          (exists (?x_3 )
            (and
              (RightOf14 ?x_3)
              (LeftOf11 ?x_3)
            )
          )
          (exists (?x_16 )
            (and
              (AboveOf9 ?x_16)
              (BelowOf4 ?x_16)
            )
          )
          (exists (?x_15 )
            (and
              (RightOf14 ?x_15)
              (LeftOf2 ?x_15)
            )
          )
          (exists (?x_23 )
            (and
              (BelowOf3 ?x_23)
              (AboveOf13 ?x_23)
            )
          )
          (exists (?x_16 )
            (and
              (BelowOf5 ?x_16)
              (AboveOf11 ?x_16)
            )
          )
          (exists (?x_7 )
            (and
              (AboveOf9 ?x_7)
              (BelowOf3 ?x_7)
            )
          )
          (exists (?x_3 )
            (and
              (LeftOf7 ?x_3)
              (RightOf14 ?x_3)
            )
          )
          (exists (?x_1 )
            (and
              (BelowOf1 ?x_1)
              (AboveOf13 ?x_1)
            )
          )
          (exists (?x_18 )
            (and
              (AboveOf11 ?x_18)
              (BelowOf6 ?x_18)
            )
          )
          (exists (?x_13 )
            (and
              (LeftOf1 ?x_13)
              (RightOf13 ?x_13)
            )
          )
          (exists (?x_26 )
            (and
              (RightOf12 ?x_26)
              (LeftOf7 ?x_26)
            )
          )
          (exists (?x_17 )
            (and
              (AboveOf5 ?x_17)
              (BelowOf2 ?x_17)
            )
          )
          (exists (?x_17 )
            (and
              (BelowOf2 ?x_17)
              (AboveOf11 ?x_17)
            )
          )
          (exists (?x_7 )
            (and
              (BelowOf1 ?x_7)
              (AboveOf4 ?x_7)
            )
          )
          (exists (?x_22 )
            (and
              (LeftOf5 ?x_22)
              (RightOf7 ?x_22)
            )
          )
          (exists (?x_20 )
            (and
              (AboveOf12 ?x_20)
              (BelowOf1 ?x_20)
            )
          )
          (exists (?x_11 )
            (and
              (AboveOf6 ?x_11)
              (BelowOf2 ?x_11)
            )
          )
          (exists (?x_13 )
            (and
              (RightOf8 ?x_13)
              (LeftOf4 ?x_13)
            )
          )
          (exists (?x_27 )
            (and
              (RightOf6 ?x_27)
              (LeftOf2 ?x_27)
            )
          )
          (exists (?x_0 )
            (and
              (RightOf8 ?x_0)
              (LeftOf1 ?x_0)
            )
          )
          (exists (?x_18 )
            (and
              (BelowOf5 ?x_18)
              (AboveOf13 ?x_18)
            )
          )
          (exists (?x_12 )
            (and
              (AboveOf14 ?x_12)
              (BelowOf5 ?x_12)
            )
          )
          (exists (?x_2 )
            (and
              (BelowOf5 ?x_2)
              (AboveOf11 ?x_2)
            )
          )
          (exists (?x_10 )
            (and
              (RightOf10 ?x_10)
              (LeftOf8 ?x_10)
            )
          )
          (exists (?x_0 )
            (and
              (LeftOf1 ?x_0)
              (RightOf3 ?x_0)
            )
          )
          (exists (?x_5 )
            (and
              (BelowOf1 ?x_5)
              (AboveOf11 ?x_5)
            )
          )
          (exists (?x_11 )
            (and
              (AboveOf11 ?x_11)
              (BelowOf1 ?x_11)
            )
          )
          (exists (?x_26 )
            (and
              (LeftOf5 ?x_26)
              (RightOf10 ?x_26)
            )
          )
          (exists (?x_24 )
            (and
              (RightOf12 ?x_24)
              (LeftOf3 ?x_24)
            )
          )
          (exists (?x_8 )
            (and
              (LeftOf12 ?x_8)
              (RightOf14 ?x_8)
            )
          )
          (exists (?x_24 )
            (and
              (LeftOf3 ?x_24)
              (RightOf13 ?x_24)
            )
          )
          (exists (?x_9 )
            (and
              (AboveOf14 ?x_9)
              (BelowOf2 ?x_9)
            )
          )
          (exists (?x_17 )
            (and
              (AboveOf14 ?x_17)
              (BelowOf3 ?x_17)
            )
          )
          (exists (?x_18 )
            (and
              (BelowOf2 ?x_18)
              (AboveOf8 ?x_18)
            )
          )
          (exists (?x_15 )
            (and
              (RightOf9 ?x_15)
              (LeftOf2 ?x_15)
            )
          )
          (exists (?x_15 )
            (and
              (LeftOf1 ?x_15)
              (RightOf9 ?x_15)
            )
          )
          (exists (?x_26 )
            (and
              (RightOf14 ?x_26)
              (LeftOf2 ?x_26)
            )
          )
          (exists (?x_5 )
            (and
              (BelowOf6 ?x_5)
              (AboveOf7 ?x_5)
            )
          )
          (exists (?x_24 )
            (and
              (RightOf9 ?x_24)
              (LeftOf2 ?x_24)
            )
          )
          (exists (?x_26 )
            (and
              (RightOf11 ?x_26)
              (LeftOf2 ?x_26)
            )
          )
          (exists (?x_5 )
            (and
              (AboveOf14 ?x_5)
              (BelowOf1 ?x_5)
            )
          )
          (exists (?x_7 )
            (and
              (BelowOf2 ?x_7)
              (AboveOf10 ?x_7)
            )
          )
          (exists (?x_24 )
            (and
              (LeftOf4 ?x_24)
              (RightOf10 ?x_24)
            )
          )
          (exists (?x_13 )
            (and
              (RightOf11 ?x_13)
              (LeftOf3 ?x_13)
            )
          )
          (exists (?x_7 )
            (and
              (BelowOf1 ?x_7)
              (AboveOf14 ?x_7)
            )
          )
          (exists (?x_3 )
            (and
              (LeftOf4 ?x_3)
              (RightOf12 ?x_3)
            )
          )
          (exists (?x_26 )
            (and
              (RightOf11 ?x_26)
              (LeftOf1 ?x_26)
            )
          )
          (exists (?x_17 )
            (and
              (BelowOf2 ?x_17)
              (AboveOf10 ?x_17)
            )
          )
          (exists (?x_22 )
            (and
              (RightOf9 ?x_22)
              (LeftOf6 ?x_22)
            )
          )
          (exists (?x_22 )
            (and
              (RightOf6 ?x_22)
              (LeftOf3 ?x_22)
            )
          )
          (exists (?x_15 )
            (and
              (LeftOf1 ?x_15)
              (RightOf6 ?x_15)
            )
          )
          (exists (?x_24 )
            (and
              (LeftOf6 ?x_24)
              (RightOf9 ?x_24)
            )
          )
          (exists (?x_16 )
            (and
              (AboveOf8 ?x_16)
              (BelowOf2 ?x_16)
            )
          )
          (exists (?x_9 )
            (and
              (AboveOf14 ?x_9)
              (BelowOf10 ?x_9)
            )
          )
          (exists (?x_21 )
            (and
              (AboveOf14 ?x_21)
              (BelowOf6 ?x_21)
            )
          )
          (exists (?x_13 )
            (and
              (LeftOf4 ?x_13)
              (RightOf11 ?x_13)
            )
          )
          (exists (?x_25 )
            (and
              (LeftOf6 ?x_25)
              (RightOf14 ?x_25)
            )
          )
          (exists (?x_22 )
            (and
              (RightOf6 ?x_22)
              (LeftOf5 ?x_22)
            )
          )
          (exists (?x_8 )
            (and
              (RightOf14 ?x_8)
              (LeftOf8 ?x_8)
            )
          )
          (exists (?x_15 )
            (and
              (LeftOf4 ?x_15)
              (RightOf14 ?x_15)
            )
          )
          (exists (?x_10 )
            (and
              (LeftOf9 ?x_10)
              (RightOf10 ?x_10)
            )
          )
          (exists (?x_11 )
            (and
              (AboveOf7 ?x_11)
              (BelowOf2 ?x_11)
            )
          )
          (exists (?x_22 )
            (and
              (LeftOf2 ?x_22)
              (RightOf7 ?x_22)
            )
          )
          (exists (?x_1 )
            (and
              (BelowOf1 ?x_1)
              (AboveOf6 ?x_1)
            )
          )
          (exists (?x_14 )
            (and
              (LeftOf1 ?x_14)
              (RightOf13 ?x_14)
            )
          )
          (exists (?x_13 )
            (and
              (LeftOf4 ?x_13)
              (RightOf10 ?x_13)
            )
          )
          (exists (?x_13 )
            (and
              (RightOf11 ?x_13)
              (LeftOf6 ?x_13)
            )
          )
          (exists (?x_2 )
            (and
              (BelowOf9 ?x_2)
              (AboveOf13 ?x_2)
            )
          )
          (exists (?x_4 )
            (and
              (AboveOf14 ?x_4)
              (BelowOf6 ?x_4)
            )
          )
          (exists (?x_22 )
            (and
              (RightOf10 ?x_22)
              (LeftOf4 ?x_22)
            )
          )
          (exists (?x_13 )
            (and
              (RightOf14 ?x_13)
              (LeftOf6 ?x_13)
            )
          )
          (exists (?x_10 )
            (and
              (LeftOf4 ?x_10)
              (RightOf14 ?x_10)
            )
          )
          (exists (?x_24 )
            (and
              (RightOf12 ?x_24)
              (LeftOf2 ?x_24)
            )
          )
          (exists (?x_25 )
            (and
              (RightOf12 ?x_25)
              (LeftOf10 ?x_25)
            )
          )
          (exists (?x_13 )
            (and
              (LeftOf4 ?x_13)
              (RightOf13 ?x_13)
            )
          )
          (exists (?x_0 )
            (and
              (LeftOf1 ?x_0)
              (RightOf4 ?x_0)
            )
          )
          (exists (?x_18 )
            (and
              (BelowOf8 ?x_18)
              (AboveOf10 ?x_18)
            )
          )
          (exists (?x_11 )
            (and
              (AboveOf10 ?x_11)
              (BelowOf2 ?x_11)
            )
          )
          (exists (?x_11 )
            (and
              (AboveOf8 ?x_11)
              (BelowOf4 ?x_11)
            )
          )
          (exists (?x_18 )
            (and
              (AboveOf10 ?x_18)
              (BelowOf1 ?x_18)
            )
          )
          (exists (?x_18 )
            (and
              (AboveOf11 ?x_18)
              (BelowOf8 ?x_18)
            )
          )
          (exists (?x_5 )
            (and
              (AboveOf9 ?x_5)
              (BelowOf7 ?x_5)
            )
          )
          (exists (?x_15 )
            (and
              (RightOf5 ?x_15)
              (LeftOf4 ?x_15)
            )
          )
          (exists (?x_14 )
            (and
              (LeftOf1 ?x_14)
              (RightOf12 ?x_14)
            )
          )
          (exists (?x_25 )
            (and
              (RightOf12 ?x_25)
              (LeftOf1 ?x_25)
            )
          )
          (exists (?x_16 )
            (and
              (AboveOf14 ?x_16)
              (BelowOf4 ?x_16)
            )
          )
          (exists (?x_18 )
            (and
              (AboveOf13 ?x_18)
              (BelowOf4 ?x_18)
            )
          )
          (exists (?x_3 )
            (and
              (LeftOf1 ?x_3)
              (RightOf11 ?x_3)
            )
          )
          (exists (?x_21 )
            (and
              (AboveOf12 ?x_21)
              (BelowOf3 ?x_21)
            )
          )
          (exists (?x_6 )
            (and
              (RightOf13 ?x_6)
              (LeftOf13 ?x_6)
            )
          )
          (exists (?x_26 )
            (and
              (LeftOf6 ?x_26)
              (RightOf10 ?x_26)
            )
          )
          (exists (?x_26 )
            (and
              (RightOf9 ?x_26)
              (LeftOf2 ?x_26)
            )
          )
          (exists (?x_3 )
            (and
              (RightOf14 ?x_3)
              (LeftOf10 ?x_3)
            )
          )
          (exists (?x_16 )
            (and
              (BelowOf5 ?x_16)
              (AboveOf8 ?x_16)
            )
          )
          (exists (?x_12 )
            (and
              (AboveOf14 ?x_12)
              (BelowOf1 ?x_12)
            )
          )
          (exists (?x_2 )
            (and
              (AboveOf12 ?x_2)
              (BelowOf2 ?x_2)
            )
          )
          (exists (?x_5 )
            (and
              (BelowOf1 ?x_5)
              (AboveOf13 ?x_5)
            )
          )
          (exists (?x_4 )
            (and
              (AboveOf11 ?x_4)
              (BelowOf9 ?x_4)
            )
          )
          (exists (?x_15 )
            (and
              (LeftOf4 ?x_15)
              (RightOf12 ?x_15)
            )
          )
          (exists (?x_23 )
            (and
              (AboveOf12 ?x_23)
              (BelowOf4 ?x_23)
            )
          )
          (exists (?x_5 )
            (and
              (AboveOf10 ?x_5)
              (BelowOf1 ?x_5)
            )
          )
          (exists (?x_15 )
            (and
              (RightOf5 ?x_15)
              (LeftOf1 ?x_15)
            )
          )
          (exists (?x_17 )
            (and
              (AboveOf13 ?x_17)
              (BelowOf1 ?x_17)
            )
          )
          (exists (?x_4 )
            (and
              (BelowOf1 ?x_4)
              (AboveOf13 ?x_4)
            )
          )
          (exists (?x_22 )
            (and
              (RightOf7 ?x_22)
              (LeftOf3 ?x_22)
            )
          )
          (exists (?x_1 )
            (and
              (BelowOf1 ?x_1)
              (AboveOf5 ?x_1)
            )
          )
          (exists (?x_21 )
            (and
              (AboveOf14 ?x_21)
              (BelowOf2 ?x_21)
            )
          )
          (exists (?x_22 )
            (and
              (RightOf14 ?x_22)
              (LeftOf6 ?x_22)
            )
          )
          (exists (?x_15 )
            (and
              (LeftOf1 ?x_15)
              (RightOf8 ?x_15)
            )
          )
          (exists (?x_2 )
            (and
              (BelowOf6 ?x_2)
              (AboveOf12 ?x_2)
            )
          )
          (exists (?x_18 )
            (and
              (AboveOf11 ?x_18)
              (BelowOf4 ?x_18)
            )
          )
          (exists (?x_24 )
            (and
              (RightOf12 ?x_24)
              (LeftOf5 ?x_24)
            )
          )
          (exists (?x_4 )
            (and
              (BelowOf1 ?x_4)
              (AboveOf10 ?x_4)
            )
          )
          (exists (?x_16 )
            (and
              (BelowOf2 ?x_16)
              (AboveOf9 ?x_16)
            )
          )
          (exists (?x_18 )
            (and
              (BelowOf7 ?x_18)
              (AboveOf12 ?x_18)
            )
          )
          (exists (?x_14 )
            (and
              (RightOf11 ?x_14)
              (LeftOf1 ?x_14)
            )
          )
          (exists (?x_14 )
            (and
              (RightOf11 ?x_14)
              (LeftOf5 ?x_14)
            )
          )
          (exists (?x_7 )
            (and
              (BelowOf2 ?x_7)
              (AboveOf13 ?x_7)
            )
          )
          (exists (?x_13 )
            (and
              (RightOf11 ?x_13)
              (LeftOf5 ?x_13)
            )
          )
          (exists (?x_20 )
            (and
              (AboveOf14 ?x_20)
              (BelowOf1 ?x_20)
            )
          )
          (exists (?x_27 )
            (and
              (RightOf8 ?x_27)
              (LeftOf2 ?x_27)
            )
          )
          (exists (?x_4 )
            (and
              (BelowOf2 ?x_4)
              (AboveOf10 ?x_4)
            )
          )
          (exists (?x_21 )
            (and
              (AboveOf12 ?x_21)
              (BelowOf7 ?x_21)
            )
          )
          (exists (?x_14 )
            (and
              (LeftOf4 ?x_14)
              (RightOf9 ?x_14)
            )
          )
          (exists (?x_24 )
            (and
              (LeftOf5 ?x_24)
              (RightOf13 ?x_24)
            )
          )
          (exists (?x_18 )
            (and
              (BelowOf7 ?x_18)
              (AboveOf13 ?x_18)
            )
          )
          (exists (?x_2 )
            (and
              (BelowOf1 ?x_2)
              (AboveOf13 ?x_2)
            )
          )
          (exists (?x_9 )
            (and
              (AboveOf14 ?x_9)
              (BelowOf9 ?x_9)
            )
          )
          (exists (?x_1 )
            (and
              (BelowOf2 ?x_1)
              (AboveOf11 ?x_1)
            )
          )
          (exists (?x_2 )
            (and
              (AboveOf14 ?x_2)
              (BelowOf1 ?x_2)
            )
          )
          (exists (?x_25 )
            (and
              (LeftOf10 ?x_25)
              (RightOf14 ?x_25)
            )
          )
          (exists (?x_27 )
            (and
              (RightOf12 ?x_27)
              (LeftOf1 ?x_27)
            )
          )
          (exists (?x_22 )
            (and
              (LeftOf4 ?x_22)
              (RightOf10 ?x_22)
            )
          )
          (exists (?x_24 )
            (and
              (LeftOf3 ?x_24)
              (RightOf9 ?x_24)
            )
          )
          (exists (?x_4 )
            (and
              (AboveOf10 ?x_4)
              (BelowOf3 ?x_4)
            )
          )
          (exists (?x_17 )
            (and
              (AboveOf9 ?x_17)
              (BelowOf3 ?x_17)
            )
          )
          (exists (?x_0 )
            (and
              (LeftOf1 ?x_0)
              (RightOf10 ?x_0)
            )
          )
          (exists (?x_22 )
            (and
              (LeftOf4 ?x_22)
              (RightOf9 ?x_22)
            )
          )
          (exists (?x_9 )
            (and
              (BelowOf7 ?x_9)
              (AboveOf14 ?x_9)
            )
          )
          (exists (?x_17 )
            (and
              (AboveOf10 ?x_17)
              (BelowOf3 ?x_17)
            )
          )
          (exists (?x_4 )
            (and
              (BelowOf1 ?x_4)
              (AboveOf12 ?x_4)
            )
          )
          (exists (?x_24 )
            (and
              (RightOf11 ?x_24)
              (LeftOf7 ?x_24)
            )
          )
          (exists (?x_3 )
            (and
              (RightOf13 ?x_3)
              (LeftOf2 ?x_3)
            )
          )
          (exists (?x_2 )
            (and
              (AboveOf13 ?x_2)
              (BelowOf7 ?x_2)
            )
          )
          (exists (?x_14 )
            (and
              (RightOf7 ?x_14)
              (LeftOf3 ?x_14)
            )
          )
          (exists (?x_2 )
            (and
              (BelowOf4 ?x_2)
              (AboveOf10 ?x_2)
            )
          )
          (exists (?x_7 )
            (and
              (AboveOf9 ?x_7)
              (BelowOf4 ?x_7)
            )
          )
          (exists (?x_17 )
            (and
              (AboveOf5 ?x_17)
              (BelowOf1 ?x_17)
            )
          )
          (exists (?x_14 )
            (and
              (LeftOf4 ?x_14)
              (RightOf13 ?x_14)
            )
          )
          (exists (?x_5 )
            (and
              (BelowOf4 ?x_5)
              (AboveOf12 ?x_5)
            )
          )
          (exists (?x_13 )
            (and
              (LeftOf2 ?x_13)
              (RightOf7 ?x_13)
            )
          )
          (exists (?x_26 )
            (and
              (RightOf9 ?x_26)
              (LeftOf3 ?x_26)
            )
          )
          (exists (?x_21 )
            (and
              (BelowOf8 ?x_21)
              (AboveOf11 ?x_21)
            )
          )
          (exists (?x_14 )
            (and
              (RightOf12 ?x_14)
              (LeftOf2 ?x_14)
            )
          )
          (exists (?x_26 )
            (and
              (LeftOf8 ?x_26)
              (RightOf13 ?x_26)
            )
          )
          (exists (?x_16 )
            (and
              (AboveOf9 ?x_16)
              (BelowOf1 ?x_16)
            )
          )
          (exists (?x_24 )
            (and
              (LeftOf2 ?x_24)
              (RightOf13 ?x_24)
            )
          )
          (exists (?x_4 )
            (and
              (AboveOf13 ?x_4)
              (BelowOf4 ?x_4)
            )
          )
          (exists (?x_3 )
            (and
              (LeftOf11 ?x_3)
              (RightOf12 ?x_3)
            )
          )
          (exists (?x_10 )
            (and
              (RightOf12 ?x_10)
              (LeftOf5 ?x_10)
            )
          )
          (exists (?x_5 )
            (and
              (BelowOf5 ?x_5)
              (AboveOf13 ?x_5)
            )
          )
          (exists (?x_3 )
            (and
              (RightOf12 ?x_3)
              (LeftOf2 ?x_3)
            )
          )
          (exists (?x_18 )
            (and
              (AboveOf14 ?x_18)
              (BelowOf3 ?x_18)
            )
          )
          (exists (?x_16 )
            (and
              (AboveOf13 ?x_16)
              (BelowOf6 ?x_16)
            )
          )
          (exists (?x_13 )
            (and
              (RightOf12 ?x_13)
              (LeftOf6 ?x_13)
            )
          )
          (exists (?x_16 )
            (and
              (AboveOf12 ?x_16)
              (BelowOf3 ?x_16)
            )
          )
          (exists (?x_19 )
            (and
              (LeftOf1 ?x_19)
              (RightOf5 ?x_19)
            )
          )
          (exists (?x_8 )
            (and
              (LeftOf1 ?x_8)
              (RightOf14 ?x_8)
            )
          )
          (exists (?x_23 )
            (and
              (AboveOf12 ?x_23)
              (BelowOf9 ?x_23)
            )
          )
          (exists (?x_17 )
            (and
              (AboveOf5 ?x_17)
              (BelowOf3 ?x_17)
            )
          )
          (exists (?x_22 )
            (and
              (RightOf8 ?x_22)
              (LeftOf3 ?x_22)
            )
          )
          (exists (?x_8 )
            (and
              (RightOf14 ?x_8)
              (LeftOf14 ?x_8)
            )
          )
          (exists (?x_19 )
            (and
              (LeftOf1 ?x_19)
              (RightOf14 ?x_19)
            )
          )
          (exists (?x_12 )
            (and
              (AboveOf14 ?x_12)
              (BelowOf7 ?x_12)
            )
          )
          (exists (?x_11 )
            (and
              (BelowOf5 ?x_11)
              (AboveOf10 ?x_11)
            )
          )
          (exists (?x_6 )
            (and
              (LeftOf3 ?x_6)
              (RightOf13 ?x_6)
            )
          )
          (exists (?x_26 )
            (and
              (LeftOf8 ?x_26)
              (RightOf12 ?x_26)
            )
          )
          (exists (?x_16 )
            (and
              (AboveOf14 ?x_16)
              (BelowOf1 ?x_16)
            )
          )
          (exists (?x_27 )
            (and
              (RightOf7 ?x_27)
              (LeftOf2 ?x_27)
            )
          )
          (exists (?x_7 )
            (and
              (AboveOf7 ?x_7)
              (BelowOf2 ?x_7)
            )
          )
          (exists (?x_4 )
            (and
              (BelowOf1 ?x_4)
              (AboveOf14 ?x_4)
            )
          )
          (exists (?x_18 )
            (and
              (BelowOf2 ?x_18)
              (AboveOf14 ?x_18)
            )
          )
          (exists (?x_8 )
            (and
              (LeftOf2 ?x_8)
              (RightOf14 ?x_8)
            )
          )
          (exists (?x_3 )
            (and
              (RightOf13 ?x_3)
              (LeftOf8 ?x_3)
            )
          )
          (exists (?x_5 )
            (and
              (AboveOf14 ?x_5)
              (BelowOf7 ?x_5)
            )
          )
          (exists (?x_0 )
            (and
              (LeftOf1 ?x_0)
              (RightOf5 ?x_0)
            )
          )
          (exists (?x_24 )
            (and
              (LeftOf6 ?x_24)
              (RightOf14 ?x_24)
            )
          )
          (exists (?x_16 )
            (and
              (AboveOf11 ?x_16)
              (BelowOf4 ?x_16)
            )
          )
          (exists (?x_26 )
            (and
              (LeftOf5 ?x_26)
              (RightOf11 ?x_26)
            )
          )
          (exists (?x_13 )
            (and
              (RightOf10 ?x_13)
              (LeftOf7 ?x_13)
            )
          )
          (exists (?x_10 )
            (and
              (LeftOf1 ?x_10)
              (RightOf14 ?x_10)
            )
          )
          (exists (?x_5 )
            (and
              (BelowOf6 ?x_5)
              (AboveOf9 ?x_5)
            )
          )
          (exists (?x_21 )
            (and
              (BelowOf11 ?x_21)
              (AboveOf13 ?x_21)
            )
          )
          (exists (?x_1 )
            (and
              (BelowOf2 ?x_1)
              (AboveOf10 ?x_1)
            )
          )
          (exists (?x_27 )
            (and
              (LeftOf3 ?x_27)
              (RightOf10 ?x_27)
            )
          )
          (exists (?x_1 )
            (and
              (BelowOf2 ?x_1)
              (AboveOf5 ?x_1)
            )
          )
          (exists (?x_18 )
            (and
              (AboveOf14 ?x_18)
              (BelowOf6 ?x_18)
            )
          )
          (exists (?x_14 )
            (and
              (RightOf7 ?x_14)
              (LeftOf5 ?x_14)
            )
          )
          (exists (?x_7 )
            (and
              (BelowOf4 ?x_7)
              (AboveOf12 ?x_7)
            )
          )
          (exists (?x_27 )
            (and
              (RightOf4 ?x_27)
              (LeftOf2 ?x_27)
            )
          )
          (exists (?x_11 )
            (and
              (BelowOf5 ?x_11)
              (AboveOf7 ?x_11)
            )
          )
          (exists (?x_17 )
            (and
              (AboveOf7 ?x_17)
              (BelowOf3 ?x_17)
            )
          )
          (exists (?x_4 )
            (and
              (BelowOf9 ?x_4)
              (AboveOf14 ?x_4)
            )
          )
          (exists (?x_15 )
            (and
              (RightOf14 ?x_15)
              (LeftOf3 ?x_15)
            )
          )
          (exists (?x_8 )
            (and
              (LeftOf9 ?x_8)
              (RightOf14 ?x_8)
            )
          )
          (exists (?x_22 )
            (and
              (RightOf12 ?x_22)
              (LeftOf3 ?x_22)
            )
          )
          (exists (?x_5 )
            (and
              (BelowOf5 ?x_5)
              (AboveOf10 ?x_5)
            )
          )
          (exists (?x_26 )
            (and
              (LeftOf8 ?x_26)
              (RightOf11 ?x_26)
            )
          )
          (exists (?x_14 )
            (and
              (LeftOf1 ?x_14)
              (RightOf7 ?x_14)
            )
          )
          (exists (?x_26 )
            (and
              (LeftOf4 ?x_26)
              (RightOf10 ?x_26)
            )
          )
          (exists (?x_11 )
            (and
              (BelowOf5 ?x_11)
              (AboveOf13 ?x_11)
            )
          )
          (exists (?x_27 )
            (and
              (LeftOf3 ?x_27)
              (RightOf12 ?x_27)
            )
          )
          (exists (?x_26 )
            (and
              (RightOf14 ?x_26)
              (LeftOf6 ?x_26)
            )
          )
          (exists (?x_18 )
            (and
              (AboveOf11 ?x_18)
              (BelowOf5 ?x_18)
            )
          )
          (exists (?x_3 )
            (and
              (RightOf13 ?x_3)
              (LeftOf7 ?x_3)
            )
          )
          (exists (?x_3 )
            (and
              (LeftOf6 ?x_3)
              (RightOf12 ?x_3)
            )
          )
          (exists (?x_27 )
            (and
              (LeftOf3 ?x_27)
              (RightOf8 ?x_27)
            )
          )
          (exists (?x_6 )
            (and
              (LeftOf2 ?x_6)
              (RightOf13 ?x_6)
            )
          )
          (exists (?x_10 )
            (and
              (RightOf13 ?x_10)
              (LeftOf5 ?x_10)
            )
          )
          (exists (?x_23 )
            (and
              (BelowOf2 ?x_23)
              (AboveOf13 ?x_23)
            )
          )
          (exists (?x_18 )
            (and
              (AboveOf11 ?x_18)
              (BelowOf1 ?x_18)
            )
          )
          (exists (?x_14 )
            (and
              (RightOf14 ?x_14)
              (LeftOf2 ?x_14)
            )
          )
          (exists (?x_14 )
            (and
              (LeftOf2 ?x_14)
              (RightOf12 ?x_14)
            )
          )
          (exists (?x_4 )
            (and
              (BelowOf10 ?x_4)
              (AboveOf12 ?x_4)
            )
          )
          (exists (?x_11 )
            (and
              (AboveOf11 ?x_11)
              (BelowOf3 ?x_11)
            )
          )
          (exists (?x_11 )
            (and
              (BelowOf1 ?x_11)
              (AboveOf13 ?x_11)
            )
          )
          (exists (?x_25 )
            (and
              (LeftOf3 ?x_25)
              (RightOf13 ?x_25)
            )
          )
          (exists (?x_17 )
            (and
              (AboveOf8 ?x_17)
              (BelowOf3 ?x_17)
            )
          )
          (exists (?x_10 )
            (and
              (LeftOf5 ?x_10)
              (RightOf14 ?x_10)
            )
          )
          (exists (?x_25 )
            (and
              (LeftOf7 ?x_25)
              (RightOf13 ?x_25)
            )
          )
          (exists (?x_14 )
            (and
              (RightOf13 ?x_14)
              (LeftOf5 ?x_14)
            )
          )
          (exists (?x_9 )
            (and
              (BelowOf1 ?x_9)
              (AboveOf13 ?x_9)
            )
          )
          (exists (?x_22 )
            (and
              (LeftOf5 ?x_22)
              (RightOf12 ?x_22)
            )
          )
          (exists (?x_13 )
            (and
              (RightOf13 ?x_13)
              (LeftOf6 ?x_13)
            )
          )
          (exists (?x_5 )
            (and
              (AboveOf13 ?x_5)
              (BelowOf7 ?x_5)
            )
          )
          (exists (?x_23 )
            (and
              (AboveOf14 ?x_23)
              (BelowOf4 ?x_23)
            )
          )
          (exists (?x_1 )
            (and
              (AboveOf10 ?x_1)
              (BelowOf1 ?x_1)
            )
          )
          (exists (?x_26 )
            (and
              (RightOf11 ?x_26)
              (LeftOf3 ?x_26)
            )
          )
          (exists (?x_13 )
            (and
              (LeftOf1 ?x_13)
              (RightOf7 ?x_13)
            )
          )
          (exists (?x_7 )
            (and
              (BelowOf4 ?x_7)
              (AboveOf14 ?x_7)
            )
          )
          (exists (?x_14 )
            (and
              (LeftOf1 ?x_14)
              (RightOf9 ?x_14)
            )
          )
          (exists (?x_3 )
            (and
              (LeftOf10 ?x_3)
              (RightOf11 ?x_3)
            )
          )
          (exists (?x_22 )
            (and
              (LeftOf4 ?x_22)
              (RightOf11 ?x_22)
            )
          )
          (exists (?x_17 )
            (and
              (BelowOf2 ?x_17)
              (AboveOf12 ?x_17)
            )
          )
          (exists (?x_25 )
            (and
              (RightOf12 ?x_25)
              (LeftOf7 ?x_25)
            )
          )
          (exists (?x_10 )
            (and
              (RightOf11 ?x_10)
              (LeftOf5 ?x_10)
            )
          )
          (exists (?x_26 )
            (and
              (RightOf14 ?x_26)
              (LeftOf3 ?x_26)
            )
          )
          (exists (?x_25 )
            (and
              (RightOf12 ?x_25)
              (LeftOf5 ?x_25)
            )
          )
          (exists (?x_10 )
            (and
              (RightOf10 ?x_10)
              (LeftOf4 ?x_10)
            )
          )
          (exists (?x_12 )
            (and
              (AboveOf14 ?x_12)
              (BelowOf10 ?x_12)
            )
          )
          (exists (?x_25 )
            (and
              (RightOf12 ?x_25)
              (LeftOf8 ?x_25)
            )
          )
          (exists (?x_7 )
            (and
              (BelowOf2 ?x_7)
              (AboveOf5 ?x_7)
            )
          )
          (exists (?x_24 )
            (and
              (RightOf12 ?x_24)
              (LeftOf8 ?x_24)
            )
          )
          (exists (?x_11 )
            (and
              (AboveOf5 ?x_11)
              (BelowOf2 ?x_11)
            )
          )
          (exists (?x_7 )
            (and
              (AboveOf10 ?x_7)
              (BelowOf2 ?x_7)
            )
          )
          (exists (?x_2 )
            (and
              (AboveOf12 ?x_2)
              (BelowOf9 ?x_2)
            )
          )
          (exists (?x_4 )
            (and
              (AboveOf10 ?x_4)
              (BelowOf5 ?x_4)
            )
          )
          (exists (?x_24 )
            (and
              (RightOf14 ?x_24)
              (LeftOf1 ?x_24)
            )
          )
          (exists (?x_16 )
            (and
              (AboveOf14 ?x_16)
              (BelowOf6 ?x_16)
            )
          )
          (exists (?x_7 )
            (and
              (BelowOf3 ?x_7)
              (AboveOf5 ?x_7)
            )
          )
          (exists (?x_24 )
            (and
              (RightOf9 ?x_24)
              (LeftOf4 ?x_24)
            )
          )
          (exists (?x_9 )
            (and
              (BelowOf5 ?x_9)
              (AboveOf13 ?x_9)
            )
          )
          (exists (?x_11 )
            (and
              (BelowOf3 ?x_11)
              (AboveOf6 ?x_11)
            )
          )
          (exists (?x_3 )
            (and
              (LeftOf11 ?x_3)
              (RightOf11 ?x_3)
            )
          )
          (exists (?x_4 )
            (and
              (AboveOf13 ?x_4)
              (BelowOf9 ?x_4)
            )
          )
          (exists (?x_17 )
            (and
              (AboveOf13 ?x_17)
              (BelowOf2 ?x_17)
            )
          )
          (exists (?x_25 )
            (and
              (RightOf14 ?x_25)
              (LeftOf7 ?x_25)
            )
          )
          (exists (?x_27 )
            (and
              (LeftOf1 ?x_27)
              (RightOf13 ?x_27)
            )
          )
          (exists (?x_25 )
            (and
              (RightOf14 ?x_25)
              (LeftOf12 ?x_25)
            )
          )
          (exists (?x_2 )
            (and
              (AboveOf14 ?x_2)
              (BelowOf9 ?x_2)
            )
          )
          (exists (?x_14 )
            (and
              (LeftOf3 ?x_14)
              (RightOf10 ?x_14)
            )
          )
          (exists (?x_17 )
            (and
              (AboveOf7 ?x_17)
              (BelowOf1 ?x_17)
            )
          )
          (exists (?x_11 )
            (and
              (AboveOf12 ?x_11)
              (BelowOf2 ?x_11)
            )
          )
          (exists (?x_2 )
            (and
              (BelowOf9 ?x_2)
              (AboveOf11 ?x_2)
            )
          )
          (exists (?x_21 )
            (and
              (AboveOf12 ?x_21)
              (BelowOf2 ?x_21)
            )
          )
          (exists (?x_11 )
            (and
              (BelowOf2 ?x_11)
              (AboveOf10 ?x_11)
            )
          )
          (exists (?x_22 )
            (and
              (LeftOf2 ?x_22)
              (RightOf14 ?x_22)
            )
          )
          (exists (?x_4 )
            (and
              (AboveOf11 ?x_4)
              (BelowOf3 ?x_4)
            )
          )
          (exists (?x_23 )
            (and
              (AboveOf12 ?x_23)
              (BelowOf12 ?x_23)
            )
          )
          (exists (?x_18 )
            (and
              (BelowOf2 ?x_18)
              (AboveOf9 ?x_18)
            )
          )
          (exists (?x_11 )
            (and
              (AboveOf12 ?x_11)
              (BelowOf1 ?x_11)
            )
          )
          (exists (?x_13 )
            (and
              (RightOf10 ?x_13)
              (LeftOf1 ?x_13)
            )
          )
          (exists (?x_18 )
            (and
              (AboveOf13 ?x_18)
              (BelowOf1 ?x_18)
            )
          )
          (exists (?x_16 )
            (and
              (BelowOf5 ?x_16)
              (AboveOf7 ?x_16)
            )
          )
          (exists (?x_11 )
            (and
              (AboveOf11 ?x_11)
              (BelowOf2 ?x_11)
            )
          )
          (exists (?x_25 )
            (and
              (RightOf13 ?x_25)
              (LeftOf9 ?x_25)
            )
          )
          (exists (?x_15 )
            (and
              (RightOf11 ?x_15)
              (LeftOf4 ?x_15)
            )
          )
          (exists (?x_27 )
            (and
              (RightOf11 ?x_27)
              (LeftOf2 ?x_27)
            )
          )
          (exists (?x_3 )
            (and
              (LeftOf9 ?x_3)
              (RightOf11 ?x_3)
            )
          )
          (exists (?x_22 )
            (and
              (LeftOf4 ?x_22)
              (RightOf13 ?x_22)
            )
          )
          (exists (?x_12 )
            (and
              (AboveOf14 ?x_12)
              (BelowOf3 ?x_12)
            )
          )
          (exists (?x_21 )
            (and
              (BelowOf8 ?x_21)
              (AboveOf12 ?x_21)
            )
          )
          (exists (?x_24 )
            (and
              (LeftOf6 ?x_24)
              (RightOf12 ?x_24)
            )
          )
          (exists (?x_12 )
            (and
              (AboveOf14 ?x_12)
              (BelowOf11 ?x_12)
            )
          )
          (exists (?x_27 )
            (and
              (LeftOf2 ?x_27)
              (RightOf6 ?x_27)
            )
          )
          (exists (?x_22 )
            (and
              (LeftOf1 ?x_22)
              (RightOf11 ?x_22)
            )
          )
          (exists (?x_16 )
            (and
              (AboveOf14 ?x_16)
              (BelowOf3 ?x_16)
            )
          )
          (exists (?x_23 )
            (and
              (BelowOf6 ?x_23)
              (AboveOf12 ?x_23)
            )
          )
          (exists (?x_22 )
            (and
              (RightOf6 ?x_22)
              (LeftOf2 ?x_22)
            )
          )
          (exists (?x_26 )
            (and
              (RightOf12 ?x_26)
              (LeftOf3 ?x_26)
            )
          )
          (exists (?x_4 )
            (and
              (AboveOf12 ?x_4)
              (BelowOf4 ?x_4)
            )
          )
          (exists (?x_5 )
            (and
              (BelowOf3 ?x_5)
              (AboveOf8 ?x_5)
            )
          )
          (exists (?x_13 )
            (and
              (RightOf10 ?x_13)
              (LeftOf3 ?x_13)
            )
          )
          (exists (?x_5 )
            (and
              (AboveOf14 ?x_5)
              (BelowOf2 ?x_5)
            )
          )
          (exists (?x_27 )
            (and
              (RightOf13 ?x_27)
              (LeftOf2 ?x_27)
            )
          )
          (exists (?x_22 )
            (and
              (RightOf9 ?x_22)
              (LeftOf5 ?x_22)
            )
          )
          (exists (?x_14 )
            (and
              (LeftOf5 ?x_14)
              (RightOf12 ?x_14)
            )
          )
          (exists (?x_18 )
            (and
              (BelowOf5 ?x_18)
              (AboveOf12 ?x_18)
            )
          )
          (exists (?x_3 )
            (and
              (RightOf13 ?x_3)
              (LeftOf6 ?x_3)
            )
          )
          (exists (?x_11 )
            (and
              (BelowOf1 ?x_11)
              (AboveOf7 ?x_11)
            )
          )
          (exists (?x_18 )
            (and
              (AboveOf11 ?x_18)
              (BelowOf3 ?x_18)
            )
          )
          (exists (?x_24 )
            (and
              (LeftOf6 ?x_24)
              (RightOf8 ?x_24)
            )
          )
          (exists (?x_25 )
            (and
              (LeftOf3 ?x_25)
              (RightOf12 ?x_25)
            )
          )
          (exists (?x_11 )
            (and
              (AboveOf8 ?x_11)
              (BelowOf2 ?x_11)
            )
          )
          (exists (?x_6 )
            (and
              (LeftOf6 ?x_6)
              (RightOf13 ?x_6)
            )
          )
          (exists (?x_14 )
            (and
              (LeftOf3 ?x_14)
              (RightOf6 ?x_14)
            )
          )
          (exists (?x_22 )
            (and
              (RightOf14 ?x_22)
              (LeftOf5 ?x_22)
            )
          )
          (exists (?x_10 )
            (and
              (LeftOf9 ?x_10)
              (RightOf12 ?x_10)
            )
          )
          (exists (?x_18 )
            (and
              (BelowOf2 ?x_18)
              (AboveOf10 ?x_18)
            )
          )
          (exists (?x_2 )
            (and
              (BelowOf3 ?x_2)
              (AboveOf11 ?x_2)
            )
          )
          (exists (?x_13 )
            (and
              (RightOf14 ?x_13)
              (LeftOf3 ?x_13)
            )
          )
          (exists (?x_23 )
            (and
              (BelowOf11 ?x_23)
              (AboveOf12 ?x_23)
            )
          )
          (exists (?x_15 )
            (and
              (RightOf13 ?x_15)
              (LeftOf2 ?x_15)
            )
          )
          (exists (?x_3 )
            (and
              (RightOf14 ?x_3)
              (LeftOf2 ?x_3)
            )
          )
          (exists (?x_16 )
            (and
              (BelowOf6 ?x_16)
              (AboveOf10 ?x_16)
            )
          )
          (exists (?x_5 )
            (and
              (BelowOf4 ?x_5)
              (AboveOf13 ?x_5)
            )
          )
          (exists (?x_24 )
            (and
              (RightOf13 ?x_24)
              (LeftOf4 ?x_24)
            )
          )
          (exists (?x_8 )
            (and
              (LeftOf11 ?x_8)
              (RightOf14 ?x_8)
            )
          )
          (exists (?x_8 )
            (and
              (LeftOf4 ?x_8)
              (RightOf14 ?x_8)
            )
          )
          (exists (?x_2 )
            (and
              (AboveOf12 ?x_2)
              (BelowOf1 ?x_2)
            )
          )
          (exists (?x_23 )
            (and
              (BelowOf10 ?x_23)
              (AboveOf13 ?x_23)
            )
          )
          (exists (?x_5 )
            (and
              (BelowOf5 ?x_5)
              (AboveOf9 ?x_5)
            )
          )
          (exists (?x_21 )
            (and
              (AboveOf11 ?x_21)
              (BelowOf9 ?x_21)
            )
          )
          (exists (?x_2 )
            (and
              (BelowOf9 ?x_2)
              (AboveOf10 ?x_2)
            )
          )
          (exists (?x_10 )
            (and
              (LeftOf2 ?x_10)
              (RightOf14 ?x_10)
            )
          )
          (exists (?x_2 )
            (and
              (BelowOf3 ?x_2)
              (AboveOf13 ?x_2)
            )
          )
          (exists (?x_6 )
            (and
              (LeftOf11 ?x_6)
              (RightOf13 ?x_6)
            )
          )
          (exists (?x_2 )
            (and
              (BelowOf6 ?x_2)
              (AboveOf10 ?x_2)
            )
          )
          (exists (?x_16 )
            (and
              (AboveOf8 ?x_16)
              (BelowOf3 ?x_16)
            )
          )
          (exists (?x_22 )
            (and
              (RightOf10 ?x_22)
              (LeftOf2 ?x_22)
            )
          )
          (exists (?x_20 )
            (and
              (AboveOf5 ?x_20)
              (BelowOf1 ?x_20)
            )
          )
          (exists (?x_20 )
            (and
              (AboveOf3 ?x_20)
              (BelowOf1 ?x_20)
            )
          )
          (exists (?x_10 )
            (and
              (LeftOf2 ?x_10)
              (RightOf12 ?x_10)
            )
          )
          (exists (?x_21 )
            (and
              (AboveOf13 ?x_21)
              (BelowOf7 ?x_21)
            )
          )
          (exists (?x_24 )
            (and
              (LeftOf5 ?x_24)
              (RightOf11 ?x_24)
            )
          )
          (exists (?x_19 )
            (and
              (LeftOf1 ?x_19)
              (RightOf8 ?x_19)
            )
          )
          (exists (?x_10 )
            (and
              (RightOf11 ?x_10)
              (LeftOf10 ?x_10)
            )
          )
          (exists (?x_26 )
            (and
              (RightOf14 ?x_26)
              (LeftOf9 ?x_26)
            )
          )
          (exists (?x_17 )
            (and
              (BelowOf2 ?x_17)
              (AboveOf9 ?x_17)
            )
          )
          (exists (?x_14 )
            (and
              (RightOf13 ?x_14)
              (LeftOf3 ?x_14)
            )
          )
          (exists (?x_14 )
            (and
              (RightOf5 ?x_14)
              (LeftOf3 ?x_14)
            )
          )
          (exists (?x_13 )
            (and
              (RightOf9 ?x_13)
              (LeftOf5 ?x_13)
            )
          )
          (exists (?x_3 )
            (and
              (LeftOf4 ?x_3)
              (RightOf11 ?x_3)
            )
          )
          (exists (?x_12 )
            (and
              (AboveOf14 ?x_12)
              (BelowOf8 ?x_12)
            )
          )
          (exists (?x_7 )
            (and
              (BelowOf4 ?x_7)
              (AboveOf4 ?x_7)
            )
          )
          (exists (?x_2 )
            (and
              (BelowOf1 ?x_2)
              (AboveOf10 ?x_2)
            )
          )
          (exists (?x_0 )
            (and
              (RightOf8 ?x_0)
              (LeftOf2 ?x_0)
            )
          )
          (exists (?x_26 )
            (and
              (RightOf9 ?x_26)
              (LeftOf7 ?x_26)
            )
          )
          (exists (?x_4 )
            (and
              (AboveOf13 ?x_4)
              (BelowOf1 ?x_4)
            )
          )
          (exists (?x_0 )
            (and
              (RightOf6 ?x_0)
              (LeftOf1 ?x_0)
            )
          )
          (exists (?x_2 )
            (and
              (BelowOf1 ?x_2)
              (AboveOf9 ?x_2)
            )
          )
          (exists (?x_18 )
            (and
              (AboveOf13 ?x_18)
              (BelowOf6 ?x_18)
            )
          )
          (exists (?x_19 )
            (and
              (LeftOf1 ?x_19)
              (RightOf2 ?x_19)
            )
          )
          (exists (?x_14 )
            (and
              (RightOf8 ?x_14)
              (LeftOf5 ?x_14)
            )
          )
          (exists (?x_13 )
            (and
              (LeftOf4 ?x_13)
              (RightOf14 ?x_13)
            )
          )
          (exists (?x_11 )
            (and
              (AboveOf5 ?x_11)
              (BelowOf1 ?x_11)
            )
          )
          (exists (?x_17 )
            (and
              (BelowOf3 ?x_17)
              (AboveOf3 ?x_17)
            )
          )
          (exists (?x_16 )
            (and
              (AboveOf12 ?x_16)
              (BelowOf6 ?x_16)
            )
          )
          (exists (?x_18 )
            (and
              (BelowOf1 ?x_18)
              (AboveOf9 ?x_18)
            )
          )
          (exists (?x_13 )
            (and
              (LeftOf4 ?x_13)
              (RightOf7 ?x_13)
            )
          )
          (exists (?x_13 )
            (and
              (LeftOf2 ?x_13)
              (RightOf14 ?x_13)
            )
          )
          (exists (?x_21 )
            (and
              (AboveOf11 ?x_21)
              (BelowOf4 ?x_21)
            )
          )
          (exists (?x_1 )
            (and
              (BelowOf2 ?x_1)
              (AboveOf14 ?x_1)
            )
          )
          (exists (?x_24 )
            (and
              (LeftOf5 ?x_24)
              (RightOf10 ?x_24)
            )
          )
          (exists (?x_4 )
            (and
              (AboveOf11 ?x_4)
              (BelowOf7 ?x_4)
            )
          )
          (exists (?x_27 )
            (and
              (RightOf4 ?x_27)
              (LeftOf1 ?x_27)
            )
          )
          (exists (?x_18 )
            (and
              (AboveOf11 ?x_18)
              (BelowOf7 ?x_18)
            )
          )
          (exists (?x_26 )
            (and
              (RightOf11 ?x_26)
              (LeftOf6 ?x_26)
            )
          )
          (exists (?x_25 )
            (and
              (RightOf14 ?x_25)
              (LeftOf4 ?x_25)
            )
          )
          (exists (?x_4 )
            (and
              (BelowOf9 ?x_4)
              (AboveOf10 ?x_4)
            )
          )
          (exists (?x_13 )
            (and
              (LeftOf2 ?x_13)
              (RightOf9 ?x_13)
            )
          )
          (exists (?x_4 )
            (and
              (AboveOf12 ?x_4)
              (BelowOf5 ?x_4)
            )
          )
          (exists (?x_9 )
            (and
              (BelowOf6 ?x_9)
              (AboveOf14 ?x_9)
            )
          )
          (exists (?x_7 )
            (and
              (BelowOf1 ?x_7)
              (AboveOf9 ?x_7)
            )
          )
          (exists (?x_10 )
            (and
              (LeftOf1 ?x_10)
              (RightOf12 ?x_10)
            )
          )
          (exists (?x_20 )
            (and
              (BelowOf1 ?x_20)
              (AboveOf7 ?x_20)
            )
          )
          (exists (?x_25 )
            (and
              (RightOf12 ?x_25)
              (LeftOf4 ?x_25)
            )
          )
          (exists (?x_4 )
            (and
              (BelowOf7 ?x_4)
              (AboveOf10 ?x_4)
            )
          )
          (exists (?x_6 )
            (and
              (LeftOf3 ?x_6)
              (RightOf14 ?x_6)
            )
          )
          (exists (?x_11 )
            (and
              (BelowOf3 ?x_11)
              (AboveOf11 ?x_11)
            )
          )
          (exists (?x_1 )
            (and
              (BelowOf1 ?x_1)
              (AboveOf11 ?x_1)
            )
          )
          (exists (?x_14 )
            (and
              (RightOf9 ?x_14)
              (LeftOf5 ?x_14)
            )
          )
          (exists (?x_10 )
            (and
              (LeftOf7 ?x_10)
              (RightOf14 ?x_10)
            )
          )
          (exists (?x_13 )
            (and
              (RightOf9 ?x_13)
              (LeftOf3 ?x_13)
            )
          )
          (exists (?x_16 )
            (and
              (BelowOf4 ?x_16)
              (AboveOf10 ?x_16)
            )
          )
          (exists (?x_23 )
            (and
              (AboveOf12 ?x_23)
              (BelowOf7 ?x_23)
            )
          )
          (exists (?x_18 )
            (and
              (AboveOf8 ?x_18)
              (BelowOf6 ?x_18)
            )
          )
          (exists (?x_15 )
            (and
              (RightOf10 ?x_15)
              (LeftOf2 ?x_15)
            )
          )
          (exists (?x_17 )
            (and
              (AboveOf6 ?x_17)
              (BelowOf3 ?x_17)
            )
          )
          (exists (?x_4 )
            (and
              (BelowOf2 ?x_4)
              (AboveOf12 ?x_4)
            )
          )
          (exists (?x_2 )
            (and
              (AboveOf14 ?x_2)
              (BelowOf2 ?x_2)
            )
          )
          (exists (?x_26 )
            (and
              (RightOf14 ?x_26)
              (LeftOf1 ?x_26)
            )
          )
          (exists (?x_9 )
            (and
              (AboveOf13 ?x_9)
              (BelowOf10 ?x_9)
            )
          )
          (exists (?x_0 )
            (and
              (RightOf13 ?x_0)
              (LeftOf1 ?x_0)
            )
          )
          (exists (?x_11 )
            (and
              (AboveOf14 ?x_11)
              (BelowOf1 ?x_11)
            )
          )
          (exists (?x_14 )
            (and
              (RightOf6 ?x_14)
              (LeftOf2 ?x_14)
            )
          )
          (exists (?x_9 )
            (and
              (AboveOf14 ?x_9)
              (BelowOf12 ?x_9)
            )
          )
          (exists (?x_9 )
            (and
              (AboveOf14 ?x_9)
              (BelowOf13 ?x_9)
            )
          )
          (exists (?x_26 )
            (and
              (RightOf14 ?x_26)
              (LeftOf5 ?x_26)
            )
          )
          (exists (?x_6 )
            (and
              (LeftOf10 ?x_6)
              (RightOf13 ?x_6)
            )
          )
          (exists (?x_15 )
            (and
              (RightOf8 ?x_15)
              (LeftOf2 ?x_15)
            )
          )
          (exists (?x_15 )
            (and
              (LeftOf1 ?x_15)
              (RightOf10 ?x_15)
            )
          )
          (exists (?x_6 )
            (and
              (LeftOf1 ?x_6)
              (RightOf14 ?x_6)
            )
          )
          (exists (?x_16 )
            (and
              (AboveOf13 ?x_16)
              (BelowOf1 ?x_16)
            )
          )
          (exists (?x_22 )
            (and
              (LeftOf1 ?x_22)
              (RightOf13 ?x_22)
            )
          )
          (exists (?x_18 )
            (and
              (BelowOf7 ?x_18)
              (AboveOf11 ?x_18)
            )
          )
          (exists (?x_13 )
            (and
              (RightOf13 ?x_13)
              (LeftOf3 ?x_13)
            )
          )
          (exists (?x_24 )
            (and
              (LeftOf5 ?x_24)
              (RightOf8 ?x_24)
            )
          )
          (exists (?x_14 )
            (and
              (RightOf14 ?x_14)
              (LeftOf1 ?x_14)
            )
          )
          (exists (?x_24 )
            (and
              (LeftOf6 ?x_24)
              (RightOf11 ?x_24)
            )
          )
          (exists (?x_7 )
            (and
              (AboveOf13 ?x_7)
              (BelowOf4 ?x_7)
            )
          )
          (exists (?x_10 )
            (and
              (RightOf11 ?x_10)
              (LeftOf8 ?x_10)
            )
          )
          (exists (?x_17 )
            (and
              (AboveOf9 ?x_17)
              (BelowOf1 ?x_17)
            )
          )
          (exists (?x_16 )
            (and
              (AboveOf14 ?x_16)
              (BelowOf2 ?x_16)
            )
          )
          (exists (?x_21 )
            (and
              (AboveOf12 ?x_21)
              (BelowOf9 ?x_21)
            )
          )
          (exists (?x_5 )
            (and
              (BelowOf1 ?x_5)
              (AboveOf9 ?x_5)
            )
          )
          (exists (?x_3 )
            (and
              (LeftOf3 ?x_3)
              (RightOf11 ?x_3)
            )
          )
          (exists (?x_15 )
            (and
              (LeftOf3 ?x_15)
              (RightOf12 ?x_15)
            )
          )
          (exists (?x_22 )
            (and
              (RightOf13 ?x_22)
              (LeftOf3 ?x_22)
            )
          )
          (exists (?x_17 )
            (and
              (AboveOf12 ?x_17)
              (BelowOf1 ?x_17)
            )
          )
          (exists (?x_22 )
            (and
              (RightOf8 ?x_22)
              (LeftOf2 ?x_22)
            )
          )
          (exists (?x_7 )
            (and
              (AboveOf7 ?x_7)
              (BelowOf4 ?x_7)
            )
          )
          (exists (?x_2 )
            (and
              (BelowOf5 ?x_2)
              (AboveOf9 ?x_2)
            )
          )
          (exists (?x_16 )
            (and
              (BelowOf2 ?x_16)
              (AboveOf6 ?x_16)
            )
          )
          (exists (?x_27 )
            (and
              (LeftOf1 ?x_27)
              (RightOf3 ?x_27)
            )
          )
          (exists (?x_16 )
            (and
              (AboveOf7 ?x_16)
              (BelowOf3 ?x_16)
            )
          )
          (exists (?x_22 )
            (and
              (RightOf8 ?x_22)
              (LeftOf5 ?x_22)
            )
          )
          (exists (?x_23 )
            (and
              (AboveOf14 ?x_23)
              (BelowOf9 ?x_23)
            )
          )
          (exists (?x_25 )
            (and
              (RightOf14 ?x_25)
              (LeftOf2 ?x_25)
            )
          )
          (exists (?x_10 )
            (and
              (RightOf12 ?x_10)
              (LeftOf8 ?x_10)
            )
          )
          (exists (?x_23 )
            (and
              (AboveOf12 ?x_23)
              (BelowOf8 ?x_23)
            )
          )
          (exists (?x_5 )
            (and
              (BelowOf4 ?x_5)
              (AboveOf9 ?x_5)
            )
          )
          (exists (?x_0 )
            (and
              (RightOf2 ?x_0)
              (LeftOf2 ?x_0)
            )
          )
          (exists (?x_11 )
            (and
              (BelowOf5 ?x_11)
              (AboveOf6 ?x_11)
            )
          )
          (exists (?x_24 )
            (and
              (RightOf14 ?x_24)
              (LeftOf5 ?x_24)
            )
          )
          (exists (?x_10 )
            (and
              (RightOf10 ?x_10)
              (LeftOf3 ?x_10)
            )
          )
          (exists (?x_16 )
            (and
              (BelowOf4 ?x_16)
              (AboveOf6 ?x_16)
            )
          )
          (exists (?x_1 )
            (and
              (BelowOf1 ?x_1)
              (AboveOf7 ?x_1)
            )
          )
          (exists (?x_14 )
            (and
              (LeftOf4 ?x_14)
              (RightOf11 ?x_14)
            )
          )
          (exists (?x_25 )
            (and
              (RightOf13 ?x_25)
              (LeftOf7 ?x_25)
            )
          )
          (exists (?x_24 )
            (and
              (LeftOf6 ?x_24)
              (RightOf13 ?x_24)
            )
          )
          (exists (?x_10 )
            (and
              (LeftOf2 ?x_10)
              (RightOf10 ?x_10)
            )
          )
          (exists (?x_27 )
            (and
              (LeftOf1 ?x_27)
              (RightOf6 ?x_27)
            )
          )
          (exists (?x_17 )
            (and
              (AboveOf13 ?x_17)
              (BelowOf3 ?x_17)
            )
          )
          (exists (?x_23 )
            (and
              (BelowOf8 ?x_23)
              (AboveOf13 ?x_23)
            )
          )
          (exists (?x_3 )
            (and
              (RightOf11 ?x_3)
              (LeftOf2 ?x_3)
            )
          )
          (exists (?x_20 )
            (and
              (AboveOf6 ?x_20)
              (BelowOf1 ?x_20)
            )
          )
          (exists (?x_4 )
            (and
              (BelowOf8 ?x_4)
              (AboveOf13 ?x_4)
            )
          )
          (exists (?x_14 )
            (and
              (RightOf14 ?x_14)
              (LeftOf4 ?x_14)
            )
          )
          (exists (?x_22 )
            (and
              (RightOf8 ?x_22)
              (LeftOf4 ?x_22)
            )
          )
          (exists (?x_10 )
            (and
              (LeftOf8 ?x_10)
              (RightOf14 ?x_10)
            )
          )
          (exists (?x_15 )
            (and
              (LeftOf1 ?x_15)
              (RightOf5 ?x_15)
            )
          )
          (exists (?x_18 )
            (and
              (BelowOf3 ?x_18)
              (AboveOf12 ?x_18)
            )
          )
          (exists (?x_22 )
            (and
              (RightOf10 ?x_22)
              (LeftOf5 ?x_22)
            )
          )
          (exists (?x_7 )
            (and
              (AboveOf4 ?x_7)
              (BelowOf3 ?x_7)
            )
          )
          (exists (?x_26 )
            (and
              (RightOf11 ?x_26)
              (LeftOf5 ?x_26)
            )
          )
          (exists (?x_15 )
            (and
              (LeftOf3 ?x_15)
              (RightOf13 ?x_15)
            )
          )
          (exists (?x_10 )
            (and
              (RightOf12 ?x_10)
              (LeftOf10 ?x_10)
            )
          )
          (exists (?x_7 )
            (and
              (BelowOf2 ?x_7)
              (AboveOf12 ?x_7)
            )
          )
          (exists (?x_0 )
            (and
              (RightOf6 ?x_0)
              (LeftOf2 ?x_0)
            )
          )
          (exists (?x_22 )
            (and
              (RightOf11 ?x_22)
              (LeftOf5 ?x_22)
            )
          )
          (exists (?x_17 )
            (and
              (BelowOf1 ?x_17)
              (AboveOf3 ?x_17)
            )
          )
          (exists (?x_11 )
            (and
              (AboveOf5 ?x_11)
              (BelowOf5 ?x_11)
            )
          )
          (exists (?x_2 )
            (and
              (AboveOf14 ?x_2)
              (BelowOf6 ?x_2)
            )
          )
          (exists (?x_11 )
            (and
              (BelowOf5 ?x_11)
              (AboveOf9 ?x_11)
            )
          )
          (exists (?x_25 )
            (and
              (LeftOf5 ?x_25)
              (RightOf13 ?x_25)
            )
          )
          (exists (?x_16 )
            (and
              (AboveOf13 ?x_16)
              (BelowOf2 ?x_16)
            )
          )
          (exists (?x_23 )
            (and
              (AboveOf14 ?x_23)
              (BelowOf3 ?x_23)
            )
          )
          (exists (?x_2 )
            (and
              (BelowOf6 ?x_2)
              (AboveOf11 ?x_2)
            )
          )
          (exists (?x_15 )
            (and
              (LeftOf3 ?x_15)
              (RightOf4 ?x_15)
            )
          )
          (exists (?x_8 )
            (and
              (LeftOf6 ?x_8)
              (RightOf14 ?x_8)
            )
          )
          (exists (?x_20 )
            (and
              (BelowOf1 ?x_20)
              (AboveOf4 ?x_20)
            )
          )
          (exists (?x_2 )
            (and
              (BelowOf6 ?x_2)
              (AboveOf14 ?x_2)
            )
          )
          (exists (?x_0 )
            (and
              (RightOf5 ?x_0)
              (LeftOf2 ?x_0)
            )
          )
          (exists (?x_2 )
            (and
              (BelowOf5 ?x_2)
              (AboveOf10 ?x_2)
            )
          )
          (exists (?x_10 )
            (and
              (RightOf10 ?x_10)
              (LeftOf5 ?x_10)
            )
          )
          (exists (?x_19 )
            (and
              (RightOf3 ?x_19)
              (LeftOf1 ?x_19)
            )
          )
          (exists (?x_25 )
            (and
              (RightOf13 ?x_25)
              (LeftOf4 ?x_25)
            )
          )
          (exists (?x_7 )
            (and
              (BelowOf4 ?x_7)
              (AboveOf6 ?x_7)
            )
          )
          (exists (?x_18 )
            (and
              (AboveOf14 ?x_18)
              (BelowOf2 ?x_18)
            )
          )
          (exists (?x_11 )
            (and
              (BelowOf3 ?x_11)
              (AboveOf14 ?x_11)
            )
          )
          (exists (?x_2 )
            (and
              (BelowOf2 ?x_2)
              (AboveOf10 ?x_2)
            )
          )
          (exists (?x_11 )
            (and
              (AboveOf6 ?x_11)
              (BelowOf4 ?x_11)
            )
          )
          (exists (?x_2 )
            (and
              (AboveOf12 ?x_2)
              (BelowOf8 ?x_2)
            )
          )
          (exists (?x_4 )
            (and
              (AboveOf10 ?x_4)
              (BelowOf4 ?x_4)
            )
          )
          (exists (?x_14 )
            (and
              (LeftOf4 ?x_14)
              (RightOf7 ?x_14)
            )
          )
          (exists (?x_2 )
            (and
              (AboveOf14 ?x_2)
              (BelowOf3 ?x_2)
            )
          )
          (exists (?x_26 )
            (and
              (RightOf9 ?x_26)
              (LeftOf1 ?x_26)
            )
          )
          (exists (?x_16 )
            (and
              (BelowOf1 ?x_16)
              (AboveOf6 ?x_16)
            )
          )
          (exists (?x_2 )
            (and
              (AboveOf11 ?x_2)
              (BelowOf8 ?x_2)
            )
          )
          (exists (?x_5 )
            (and
              (BelowOf6 ?x_5)
              (AboveOf13 ?x_5)
            )
          )
          (exists (?x_23 )
            (and
              (BelowOf1 ?x_23)
              (AboveOf13 ?x_23)
            )
          )
          (exists (?x_6 )
            (and
              (RightOf14 ?x_6)
              (LeftOf7 ?x_6)
            )
          )
          (exists (?x_2 )
            (and
              (BelowOf1 ?x_2)
              (AboveOf11 ?x_2)
            )
          )
          (exists (?x_24 )
            (and
              (LeftOf3 ?x_24)
              (RightOf8 ?x_24)
            )
          )
          (exists (?x_20 )
            (and
              (BelowOf1 ?x_20)
              (AboveOf10 ?x_20)
            )
          )
          (exists (?x_24 )
            (and
              (RightOf8 ?x_24)
              (LeftOf1 ?x_24)
            )
          )
          (exists (?x_2 )
            (and
              (AboveOf10 ?x_2)
              (BelowOf8 ?x_2)
            )
          )
          (exists (?x_13 )
            (and
              (LeftOf2 ?x_13)
              (RightOf13 ?x_13)
            )
          )
          (exists (?x_13 )
            (and
              (RightOf14 ?x_13)
              (LeftOf5 ?x_13)
            )
          )
          (exists (?x_23 )
            (and
              (AboveOf14 ?x_23)
              (BelowOf6 ?x_23)
            )
          )
          (exists (?x_9 )
            (and
              (AboveOf14 ?x_9)
              (BelowOf3 ?x_9)
            )
          )
          (exists (?x_24 )
            (and
              (LeftOf7 ?x_24)
              (RightOf10 ?x_24)
            )
          )
          (exists (?x_27 )
            (and
              (LeftOf1 ?x_27)
              (RightOf5 ?x_27)
            )
          )
          (exists (?x_7 )
            (and
              (AboveOf10 ?x_7)
              (BelowOf4 ?x_7)
            )
          )
          (exists (?x_6 )
            (and
              (LeftOf9 ?x_6)
              (RightOf14 ?x_6)
            )
          )
          (exists (?x_1 )
            (and
              (BelowOf1 ?x_1)
              (AboveOf14 ?x_1)
            )
          )
          (exists (?x_18 )
            (and
              (BelowOf7 ?x_18)
              (AboveOf10 ?x_18)
            )
          )
          (exists (?x_11 )
            (and
              (AboveOf5 ?x_11)
              (BelowOf3 ?x_11)
            )
          )
          (exists (?x_0 )
            (and
              (LeftOf1 ?x_0)
              (RightOf2 ?x_0)
            )
          )
          (exists (?x_2 )
            (and
              (AboveOf10 ?x_2)
              (BelowOf2 ?x_2)
            )
          )
          (exists (?x_23 )
            (and
              (AboveOf14 ?x_23)
              (BelowOf11 ?x_23)
            )
          )
          (exists (?x_14 )
            (and
              (LeftOf2 ?x_14)
              (RightOf6 ?x_14)
            )
          )
          (exists (?x_27 )
            (and
              (RightOf4 ?x_27)
              (LeftOf3 ?x_27)
            )
          )
          (exists (?x_11 )
            (and
              (BelowOf5 ?x_11)
              (AboveOf12 ?x_11)
            )
          )
          (exists (?x_5 )
            (and
              (BelowOf3 ?x_5)
              (AboveOf13 ?x_5)
            )
          )
          (exists (?x_16 )
            (and
              (BelowOf5 ?x_16)
              (AboveOf14 ?x_16)
            )
          )
          (exists (?x_10 )
            (and
              (LeftOf9 ?x_10)
              (RightOf14 ?x_10)
            )
          )
          (exists (?x_2 )
            (and
              (BelowOf9 ?x_2)
              (AboveOf9 ?x_2)
            )
          )
          (exists (?x_22 )
            (and
              (RightOf9 ?x_22)
              (LeftOf3 ?x_22)
            )
          )
          (exists (?x_26 )
            (and
              (RightOf12 ?x_26)
              (LeftOf9 ?x_26)
            )
          )
          (exists (?x_26 )
            (and
              (LeftOf8 ?x_26)
              (RightOf9 ?x_26)
            )
          )
          (exists (?x_22 )
            (and
              (LeftOf4 ?x_22)
              (RightOf14 ?x_22)
            )
          )
          (exists (?x_16 )
            (and
              (BelowOf3 ?x_16)
              (AboveOf6 ?x_16)
            )
          )
          (exists (?x_12 )
            (and
              (AboveOf14 ?x_12)
              (BelowOf4 ?x_12)
            )
          )
          (exists (?x_16 )
            (and
              (BelowOf2 ?x_16)
              (AboveOf12 ?x_16)
            )
          )
          (exists (?x_5 )
            (and
              (BelowOf7 ?x_5)
              (AboveOf7 ?x_5)
            )
          )
          (exists (?x_11 )
            (and
              (BelowOf4 ?x_11)
              (AboveOf10 ?x_11)
            )
          )
          (exists (?x_10 )
            (and
              (LeftOf6 ?x_10)
              (RightOf14 ?x_10)
            )
          )
          (exists (?x_2 )
            (and
              (BelowOf7 ?x_2)
              (AboveOf11 ?x_2)
            )
          )
          (exists (?x_26 )
            (and
              (RightOf10 ?x_26)
              (LeftOf4 ?x_26)
            )
          )
          (exists (?x_2 )
            (and
              (AboveOf14 ?x_2)
              (BelowOf4 ?x_2)
            )
          )
          (exists (?x_21 )
            (and
              (AboveOf12 ?x_21)
              (BelowOf1 ?x_21)
            )
          )
          (exists (?x_16 )
            (and
              (BelowOf1 ?x_16)
              (AboveOf10 ?x_16)
            )
          )
          (exists (?x_15 )
            (and
              (RightOf12 ?x_15)
              (LeftOf2 ?x_15)
            )
          )
          (exists (?x_22 )
            (and
              (LeftOf6 ?x_22)
              (RightOf7 ?x_22)
            )
          )
          (exists (?x_18 )
            (and
              (BelowOf6 ?x_18)
              (AboveOf10 ?x_18)
            )
          )
          (exists (?x_14 )
            (and
              (RightOf5 ?x_14)
              (LeftOf2 ?x_14)
            )
          )
          (exists (?x_3 )
            (and
              (LeftOf10 ?x_3)
              (RightOf12 ?x_3)
            )
          )
          (exists (?x_4 )
            (and
              (BelowOf8 ?x_4)
              (AboveOf10 ?x_4)
            )
          )
          (exists (?x_15 )
            (and
              (LeftOf4 ?x_15)
              (RightOf4 ?x_15)
            )
          )
          (exists (?x_7 )
            (and
              (AboveOf7 ?x_7)
              (BelowOf1 ?x_7)
            )
          )
          (exists (?x_6 )
            (and
              (LeftOf6 ?x_6)
              (RightOf14 ?x_6)
            )
          )
          (exists (?x_3 )
            (and
              (RightOf14 ?x_3)
              (LeftOf5 ?x_3)
            )
          )
          (exists (?x_27 )
            (and
              (RightOf14 ?x_27)
              (LeftOf3 ?x_27)
            )
          )
          (exists (?x_9 )
            (and
              (BelowOf4 ?x_9)
              (AboveOf13 ?x_9)
            )
          )
          (exists (?x_23 )
            (and
              (AboveOf12 ?x_23)
              (BelowOf1 ?x_23)
            )
          )
          (exists (?x_9 )
            (and
              (BelowOf7 ?x_9)
              (AboveOf13 ?x_9)
            )
          )
          (exists (?x_16 )
            (and
              (AboveOf7 ?x_16)
              (BelowOf6 ?x_16)
            )
          )
          (exists (?x_2 )
            (and
              (BelowOf6 ?x_2)
              (AboveOf13 ?x_2)
            )
          )
          (exists (?x_11 )
            (and
              (BelowOf3 ?x_11)
              (AboveOf13 ?x_11)
            )
          )
          (exists (?x_23 )
            (and
              (AboveOf12 ?x_23)
              (BelowOf2 ?x_23)
            )
          )
          (exists (?x_13 )
            (and
              (RightOf8 ?x_13)
              (LeftOf6 ?x_13)
            )
          )
          (exists (?x_2 )
            (and
              (BelowOf2 ?x_2)
              (AboveOf11 ?x_2)
            )
          )
          (exists (?x_21 )
            (and
              (AboveOf12 ?x_21)
              (BelowOf10 ?x_21)
            )
          )
          (exists (?x_16 )
            (and
              (AboveOf7 ?x_16)
              (BelowOf1 ?x_16)
            )
          )
          (exists (?x_24 )
            (and
              (RightOf10 ?x_24)
              (LeftOf4 ?x_24)
            )
          )
          (exists (?x_4 )
            (and
              (BelowOf9 ?x_4)
              (AboveOf12 ?x_4)
            )
          )
          (exists (?x_20 )
            (and
              (AboveOf8 ?x_20)
              (BelowOf1 ?x_20)
            )
          )
          (exists (?x_5 )
            (and
              (AboveOf14 ?x_5)
              (BelowOf4 ?x_5)
            )
          )
          (exists (?x_10 )
            (and
              (LeftOf10 ?x_10)
              (RightOf14 ?x_10)
            )
          )
          (exists (?x_25 )
            (and
              (LeftOf10 ?x_25)
              (RightOf13 ?x_25)
            )
          )
          (exists (?x_13 )
            (and
              (RightOf10 ?x_13)
              (LeftOf6 ?x_13)
            )
          )
          (exists (?x_17 )
            (and
              (AboveOf11 ?x_17)
              (BelowOf3 ?x_17)
            )
          )
          (exists (?x_0 )
            (and
              (RightOf4 ?x_0)
              (LeftOf2 ?x_0)
            )
          )
          (exists (?x_21 )
            (and
              (AboveOf12 ?x_21)
              (BelowOf11 ?x_21)
            )
          )
          (exists (?x_26 )
            (and
              (RightOf11 ?x_26)
              (LeftOf9 ?x_26)
            )
          )
          (exists (?x_7 )
            (and
              (AboveOf7 ?x_7)
              (BelowOf3 ?x_7)
            )
          )
          (exists (?x_11 )
            (and
              (BelowOf3 ?x_11)
              (AboveOf9 ?x_11)
            )
          )
          (exists (?x_21 )
            (and
              (BelowOf8 ?x_21)
              (AboveOf14 ?x_21)
            )
          )
          (exists (?x_23 )
            (and
              (BelowOf4 ?x_23)
              (AboveOf13 ?x_23)
            )
          )
          (exists (?x_10 )
            (and
              (LeftOf7 ?x_10)
              (RightOf11 ?x_10)
            )
          )
          (exists (?x_18 )
            (and
              (AboveOf14 ?x_18)
              (BelowOf1 ?x_18)
            )
          )
          (exists (?x_16 )
            (and
              (BelowOf5 ?x_16)
              (AboveOf9 ?x_16)
            )
          )
          (exists (?x_21 )
            (and
              (BelowOf5 ?x_21)
              (AboveOf11 ?x_21)
            )
          )
          (exists (?x_1 )
            (and
              (BelowOf2 ?x_1)
              (AboveOf8 ?x_1)
            )
          )
          (exists (?x_26 )
            (and
              (RightOf9 ?x_26)
              (LeftOf9 ?x_26)
            )
          )
          (exists (?x_8 )
            (and
              (LeftOf13 ?x_8)
              (RightOf14 ?x_8)
            )
          )
          (exists (?x_15 )
            (and
              (LeftOf1 ?x_15)
              (RightOf4 ?x_15)
            )
          )
          (exists (?x_2 )
            (and
              (BelowOf3 ?x_2)
              (AboveOf9 ?x_2)
            )
          )
          (exists (?x_23 )
            (and
              (AboveOf12 ?x_23)
              (BelowOf5 ?x_23)
            )
          )
          (exists (?x_15 )
            (and
              (LeftOf2 ?x_15)
              (RightOf6 ?x_15)
            )
          )
          (exists (?x_7 )
            (and
              (BelowOf1 ?x_7)
              (AboveOf12 ?x_7)
            )
          )
          (exists (?x_2 )
            (and
              (BelowOf6 ?x_2)
              (AboveOf9 ?x_2)
            )
          )
          (exists (?x_23 )
            (and
              (AboveOf12 ?x_23)
              (BelowOf10 ?x_23)
            )
          )
          (exists (?x_14 )
            (and
              (RightOf8 ?x_14)
              (LeftOf1 ?x_14)
            )
          )
          (exists (?x_2 )
            (and
              (AboveOf13 ?x_2)
              (BelowOf8 ?x_2)
            )
          )
          (exists (?x_16 )
            (and
              (AboveOf12 ?x_16)
              (BelowOf4 ?x_16)
            )
          )
          (exists (?x_18 )
            (and
              (AboveOf12 ?x_18)
              (BelowOf4 ?x_18)
            )
          )
          (exists (?x_26 )
            (and
              (LeftOf3 ?x_26)
              (RightOf10 ?x_26)
            )
          )
          (exists (?x_21 )
            (and
              (AboveOf11 ?x_21)
              (BelowOf11 ?x_21)
            )
          )
          (exists (?x_13 )
            (and
              (LeftOf4 ?x_13)
              (RightOf9 ?x_13)
            )
          )
          (exists (?x_22 )
            (and
              (RightOf11 ?x_22)
              (LeftOf3 ?x_22)
            )
          )
          (exists (?x_9 )
            (and
              (BelowOf11 ?x_9)
              (AboveOf13 ?x_9)
            )
          )
          (exists (?x_7 )
            (and
              (AboveOf8 ?x_7)
              (BelowOf4 ?x_7)
            )
          )
          (exists (?x_27 )
            (and
              (LeftOf3 ?x_27)
              (RightOf6 ?x_27)
            )
          )
          (exists (?x_22 )
            (and
              (RightOf10 ?x_22)
              (LeftOf3 ?x_22)
            )
          )
          (exists (?x_23 )
            (and
              (AboveOf14 ?x_23)
              (BelowOf10 ?x_23)
            )
          )
          (exists (?x_27 )
            (and
              (RightOf14 ?x_27)
              (LeftOf2 ?x_27)
            )
          )
          (exists (?x_26 )
            (and
              (LeftOf7 ?x_26)
              (RightOf13 ?x_26)
            )
          )
          (exists (?x_25 )
            (and
              (RightOf12 ?x_25)
              (LeftOf2 ?x_25)
            )
          )
          (exists (?x_14 )
            (and
              (LeftOf4 ?x_14)
              (RightOf6 ?x_14)
            )
          )
          (exists (?x_5 )
            (and
              (BelowOf6 ?x_5)
              (AboveOf8 ?x_5)
            )
          )
          (exists (?x_16 )
            (and
              (AboveOf7 ?x_16)
              (BelowOf4 ?x_16)
            )
          )
          (exists (?x_22 )
            (and
              (LeftOf2 ?x_22)
              (RightOf13 ?x_22)
            )
          )
          (exists (?x_10 )
            (and
              (LeftOf3 ?x_10)
              (RightOf12 ?x_10)
            )
          )
          (exists (?x_18 )
            (and
              (BelowOf1 ?x_18)
              (AboveOf12 ?x_18)
            )
          )
          (exists (?x_14 )
            (and
              (RightOf10 ?x_14)
              (LeftOf2 ?x_14)
            )
          )
          (exists (?x_21 )
            (and
              (AboveOf13 ?x_21)
              (BelowOf1 ?x_21)
            )
          )
          (exists (?x_25 )
            (and
              (LeftOf8 ?x_25)
              (RightOf13 ?x_25)
            )
          )
          (exists (?x_24 )
            (and
              (LeftOf3 ?x_24)
              (RightOf11 ?x_24)
            )
          )
          (exists (?x_24 )
            (and
              (RightOf14 ?x_24)
              (LeftOf7 ?x_24)
            )
          )
          (exists (?x_3 )
            (and
              (RightOf11 ?x_3)
              (LeftOf5 ?x_3)
            )
          )
          (exists (?x_16 )
            (and
              (BelowOf1 ?x_16)
              (AboveOf12 ?x_16)
            )
          )
          (exists (?x_0 )
            (and
              (RightOf10 ?x_0)
              (LeftOf2 ?x_0)
            )
          )
          (exists (?x_27 )
            (and
              (RightOf14 ?x_27)
              (LeftOf1 ?x_27)
            )
          )
          (exists (?x_24 )
            (and
              (LeftOf7 ?x_24)
              (RightOf8 ?x_24)
            )
          )
          (exists (?x_24 )
            (and
              (LeftOf8 ?x_24)
              (RightOf9 ?x_24)
            )
          )
          (exists (?x_7 )
            (and
              (AboveOf13 ?x_7)
              (BelowOf3 ?x_7)
            )
          )
          (exists (?x_3 )
            (and
              (RightOf13 ?x_3)
              (LeftOf11 ?x_3)
            )
          )
          (exists (?x_2 )
            (and
              (BelowOf4 ?x_2)
              (AboveOf9 ?x_2)
            )
          )
          (exists (?x_4 )
            (and
              (AboveOf13 ?x_4)
              (BelowOf3 ?x_4)
            )
          )
          (exists (?x_15 )
            (and
              (RightOf11 ?x_15)
              (LeftOf2 ?x_15)
            )
          )
          (exists (?x_10 )
            (and
              (RightOf13 ?x_10)
              (LeftOf8 ?x_10)
            )
          )
          (exists (?x_16 )
            (and
              (BelowOf2 ?x_16)
              (AboveOf10 ?x_16)
            )
          )
          (exists (?x_15 )
            (and
              (LeftOf1 ?x_15)
              (RightOf14 ?x_15)
            )
          )
          (exists (?x_16 )
            (and
              (AboveOf7 ?x_16)
              (BelowOf2 ?x_16)
            )
          )
          (exists (?x_26 )
            (and
              (LeftOf5 ?x_26)
              (RightOf13 ?x_26)
            )
          )
          (exists (?x_11 )
            (and
              (AboveOf13 ?x_11)
              (BelowOf2 ?x_11)
            )
          )
          (exists (?x_3 )
            (and
              (RightOf14 ?x_3)
              (LeftOf6 ?x_3)
            )
          )
          (exists (?x_21 )
            (and
              (AboveOf13 ?x_21)
              (BelowOf9 ?x_21)
            )
          )
          (exists (?x_23 )
            (and
              (AboveOf14 ?x_23)
              (BelowOf7 ?x_23)
            )
          )
          (exists (?x_6 )
            (and
              (LeftOf8 ?x_6)
              (RightOf14 ?x_6)
            )
          )
          (exists (?x_21 )
            (and
              (AboveOf12 ?x_21)
              (BelowOf5 ?x_21)
            )
          )
          (exists (?x_19 )
            (and
              (RightOf4 ?x_19)
              (LeftOf1 ?x_19)
            )
          )
          (exists (?x_7 )
            (and
              (AboveOf12 ?x_7)
              (BelowOf3 ?x_7)
            )
          )
          (exists (?x_18 )
            (and
              (AboveOf11 ?x_18)
              (BelowOf2 ?x_18)
            )
          )
          (exists (?x_12 )
            (and
              (AboveOf14 ?x_12)
              (BelowOf6 ?x_12)
            )
          )
          (exists (?x_23 )
            (and
              (AboveOf14 ?x_23)
              (BelowOf1 ?x_23)
            )
          )
          (exists (?x_21 )
            (and
              (AboveOf14 ?x_21)
              (BelowOf3 ?x_21)
            )
          )
          (exists (?x_10 )
            (and
              (RightOf10 ?x_10)
              (LeftOf10 ?x_10)
            )
          )
          (exists (?x_14 )
            (and
              (RightOf8 ?x_14)
              (LeftOf3 ?x_14)
            )
          )
          (exists (?x_13 )
            (and
              (RightOf9 ?x_13)
              (LeftOf6 ?x_13)
            )
          )
          (exists (?x_18 )
            (and
              (AboveOf14 ?x_18)
              (BelowOf4 ?x_18)
            )
          )
          (exists (?x_15 )
            (and
              (LeftOf4 ?x_15)
              (RightOf10 ?x_15)
            )
          )
          (exists (?x_18 )
            (and
              (BelowOf5 ?x_18)
              (AboveOf10 ?x_18)
            )
          )
          (exists (?x_15 )
            (and
              (LeftOf1 ?x_15)
              (RightOf12 ?x_15)
            )
          )
          (exists (?x_2 )
            (and
              (AboveOf14 ?x_2)
              (BelowOf7 ?x_2)
            )
          )
          (exists (?x_26 )
            (and
              (LeftOf3 ?x_26)
              (RightOf13 ?x_26)
            )
          )
          (exists (?x_16 )
            (and
              (AboveOf11 ?x_16)
              (BelowOf6 ?x_16)
            )
          )
          (exists (?x_20 )
            (and
              (AboveOf11 ?x_20)
              (BelowOf1 ?x_20)
            )
          )
          (exists (?x_13 )
            (and
              (RightOf8 ?x_13)
              (LeftOf2 ?x_13)
            )
          )
          (exists (?x_12 )
            (and
              (AboveOf14 ?x_12)
              (BelowOf2 ?x_12)
            )
          )
          (exists (?x_17 )
            (and
              (AboveOf8 ?x_17)
              (BelowOf1 ?x_17)
            )
          )
          (exists (?x_27 )
            (and
              (RightOf3 ?x_27)
              (LeftOf2 ?x_27)
            )
          )
          (exists (?x_14 )
            (and
              (RightOf11 ?x_14)
              (LeftOf2 ?x_14)
            )
          )
          (exists (?x_12 )
            (and
              (AboveOf14 ?x_12)
              (BelowOf14 ?x_12)
            )
          )
          (exists (?x_13 )
            (and
              (RightOf7 ?x_13)
              (LeftOf3 ?x_13)
            )
          )
          (exists (?x_10 )
            (and
              (RightOf14 ?x_10)
              (LeftOf8 ?x_10)
            )
          )
          (exists (?x_11 )
            (and
              (BelowOf4 ?x_11)
              (AboveOf7 ?x_11)
            )
          )
          (exists (?x_24 )
            (and
              (RightOf9 ?x_24)
              (LeftOf5 ?x_24)
            )
          )
          (exists (?x_15 )
            (and
              (LeftOf3 ?x_15)
              (RightOf6 ?x_15)
            )
          )
          (exists (?x_5 )
            (and
              (BelowOf6 ?x_5)
              (AboveOf14 ?x_5)
            )
          )
          (exists (?x_14 )
            (and
              (LeftOf1 ?x_14)
              (RightOf10 ?x_14)
            )
          )
          (exists (?x_22 )
            (and
              (RightOf13 ?x_22)
              (LeftOf5 ?x_22)
            )
          )
          (exists (?x_4 )
            (and
              (BelowOf4 ?x_4)
              (AboveOf14 ?x_4)
            )
          )
          (exists (?x_14 )
            (and
              (RightOf7 ?x_14)
              (LeftOf2 ?x_14)
            )
          )
          (exists (?x_15 )
            (and
              (RightOf6 ?x_15)
              (LeftOf2 ?x_15)
            )
          )
          (exists (?x_6 )
            (and
              (RightOf14 ?x_6)
              (LeftOf13 ?x_6)
            )
          )
          (exists (?x_0 )
            (and
              (LeftOf1 ?x_0)
              (RightOf11 ?x_0)
            )
          )
          (exists (?x_3 )
            (and
              (RightOf14 ?x_3)
              (LeftOf1 ?x_3)
            )
          )
          (exists (?x_17 )
            (and
              (AboveOf11 ?x_17)
              (BelowOf1 ?x_17)
            )
          )
          (exists (?x_24 )
            (and
              (LeftOf3 ?x_24)
              (RightOf14 ?x_24)
            )
          )
          (exists (?x_14 )
            (and
              (LeftOf4 ?x_14)
              (RightOf5 ?x_14)
            )
          )
          (exists (?x_3 )
            (and
              (LeftOf1 ?x_3)
              (RightOf12 ?x_3)
            )
          )
          (exists (?x_11 )
            (and
              (AboveOf12 ?x_11)
              (BelowOf4 ?x_11)
            )
          )
          (exists (?x_15 )
            (and
              (RightOf11 ?x_15)
              (LeftOf1 ?x_15)
            )
          )
          (exists (?x_14 )
            (and
              (LeftOf4 ?x_14)
              (RightOf10 ?x_14)
            )
          )
          (exists (?x_24 )
            (and
              (LeftOf8 ?x_24)
              (RightOf8 ?x_24)
            )
          )
          (exists (?x_22 )
            (and
              (LeftOf2 ?x_22)
              (RightOf9 ?x_22)
            )
          )
          (exists (?x_5 )
            (and
              (BelowOf5 ?x_5)
              (AboveOf7 ?x_5)
            )
          )
          (exists (?x_24 )
            (and
              (LeftOf2 ?x_24)
              (RightOf8 ?x_24)
            )
          )
          (exists (?x_4 )
            (and
              (BelowOf10 ?x_4)
              (AboveOf14 ?x_4)
            )
          )
          (exists (?x_9 )
            (and
              (BelowOf8 ?x_9)
              (AboveOf13 ?x_9)
            )
          )
          (exists (?x_5 )
            (and
              (AboveOf14 ?x_5)
              (BelowOf5 ?x_5)
            )
          )
          (exists (?x_26 )
            (and
              (LeftOf4 ?x_26)
              (RightOf13 ?x_26)
            )
          )
          (exists (?x_3 )
            (and
              (LeftOf8 ?x_3)
              (RightOf11 ?x_3)
            )
          )
          (exists (?x_7 )
            (and
              (BelowOf2 ?x_7)
              (AboveOf14 ?x_7)
            )
          )
          (exists (?x_6 )
            (and
              (RightOf14 ?x_6)
              (LeftOf4 ?x_6)
            )
          )
          (exists (?x_11 )
            (and
              (BelowOf5 ?x_11)
              (AboveOf11 ?x_11)
            )
          )
          (exists (?x_3 )
            (and
              (RightOf13 ?x_3)
              (LeftOf4 ?x_3)
            )
          )
          (exists (?x_9 )
            (and
              (BelowOf12 ?x_9)
              (AboveOf13 ?x_9)
            )
          )
          (exists (?x_27 )
            (and
              (LeftOf3 ?x_27)
              (RightOf5 ?x_27)
            )
          )
          (exists (?x_23 )
            (and
              (BelowOf12 ?x_23)
              (AboveOf13 ?x_23)
            )
          )
          (exists (?x_25 )
            (and
              (RightOf13 ?x_25)
              (LeftOf11 ?x_25)
            )
          )
          (exists (?x_4 )
            (and
              (BelowOf7 ?x_4)
              (AboveOf13 ?x_4)
            )
          )
          (exists (?x_18 )
            (and
              (BelowOf2 ?x_18)
              (AboveOf12 ?x_18)
            )
          )
          (exists (?x_18 )
            (and
              (BelowOf6 ?x_18)
              (AboveOf12 ?x_18)
            )
          )
          (exists (?x_13 )
            (and
              (LeftOf6 ?x_13)
              (RightOf7 ?x_13)
            )
          )
          (exists (?x_27 )
            (and
              (RightOf11 ?x_27)
              (LeftOf3 ?x_27)
            )
          )
          (exists (?x_7 )
            (and
              (AboveOf10 ?x_7)
              (BelowOf3 ?x_7)
            )
          )
          (exists (?x_22 )
            (and
              (LeftOf1 ?x_22)
              (RightOf7 ?x_22)
            )
          )
          (exists (?x_3 )
            (and
              (RightOf13 ?x_3)
              (LeftOf1 ?x_3)
            )
          )
          (exists (?x_2 )
            (and
              (AboveOf12 ?x_2)
              (BelowOf7 ?x_2)
            )
          )
          (exists (?x_24 )
            (and
              (LeftOf8 ?x_24)
              (RightOf11 ?x_24)
            )
          )
          (exists (?x_21 )
            (and
              (AboveOf12 ?x_21)
              (BelowOf6 ?x_21)
            )
          )
          (exists (?x_26 )
            (and
              (RightOf10 ?x_26)
              (LeftOf2 ?x_26)
            )
          )
          (exists (?x_4 )
            (and
              (BelowOf6 ?x_4)
              (AboveOf14 ?x_4)
            )
          )
          (exists (?x_25 )
            (and
              (LeftOf5 ?x_25)
              (RightOf14 ?x_25)
            )
          )
          (exists (?x_25 )
            (and
              (RightOf14 ?x_25)
              (LeftOf11 ?x_25)
            )
          )
          (exists (?x_26 )
            (and
              (RightOf10 ?x_26)
              (LeftOf1 ?x_26)
            )
          )
          (exists (?x_17 )
            (and
              (BelowOf1 ?x_17)
              (AboveOf10 ?x_17)
            )
          )
          (exists (?x_22 )
            (and
              (RightOf14 ?x_22)
              (LeftOf3 ?x_22)
            )
          )
          (exists (?x_16 )
            (and
              (BelowOf3 ?x_16)
              (AboveOf10 ?x_16)
            )
          )
          (exists (?x_22 )
            (and
              (LeftOf4 ?x_22)
              (RightOf6 ?x_22)
            )
          )
          (exists (?x_10 )
            (and
              (LeftOf4 ?x_10)
              (RightOf12 ?x_10)
            )
          )
          (exists (?x_27 )
            (and
              (RightOf9 ?x_27)
              (LeftOf2 ?x_27)
            )
          )
          (exists (?x_6 )
            (and
              (LeftOf9 ?x_6)
              (RightOf13 ?x_6)
            )
          )
          (exists (?x_15 )
            (and
              (LeftOf3 ?x_15)
              (RightOf7 ?x_15)
            )
          )
          (exists (?x_2 )
            (and
              (AboveOf9 ?x_2)
              (BelowOf2 ?x_2)
            )
          )
          (exists (?x_22 )
            (and
              (LeftOf1 ?x_22)
              (RightOf9 ?x_22)
            )
          )
          (exists (?x_23 )
            (and
              (BelowOf6 ?x_23)
              (AboveOf13 ?x_23)
            )
          )
          (exists (?x_23 )
            (and
              (AboveOf14 ?x_23)
              (BelowOf8 ?x_23)
            )
          )
          (exists (?x_7 )
            (and
              (AboveOf10 ?x_7)
              (BelowOf1 ?x_7)
            )
          )
          (exists (?x_14 )
            (and
              (RightOf14 ?x_14)
              (LeftOf3 ?x_14)
            )
          )
          (exists (?x_19 )
            (and
              (RightOf9 ?x_19)
              (LeftOf1 ?x_19)
            )
          )
          (exists (?x_7 )
            (and
              (AboveOf8 ?x_7)
              (BelowOf1 ?x_7)
            )
          )
          (exists (?x_25 )
            (and
              (RightOf13 ?x_25)
              (LeftOf1 ?x_25)
            )
          )
          (exists (?x_21 )
            (and
              (AboveOf14 ?x_21)
              (BelowOf10 ?x_21)
            )
          )
          (exists (?x_18 )
            (and
              (AboveOf13 ?x_18)
              (BelowOf3 ?x_18)
            )
          )
          (exists (?x_3 )
            (and
              (RightOf14 ?x_3)
              (LeftOf8 ?x_3)
            )
          )
          (exists (?x_14 )
            (and
              (RightOf8 ?x_14)
              (LeftOf2 ?x_14)
            )
          )
          (exists (?x_1 )
            (and
              (BelowOf1 ?x_1)
              (AboveOf4 ?x_1)
            )
          )
          (exists (?x_17 )
            (and
              (AboveOf14 ?x_17)
              (BelowOf2 ?x_17)
            )
          )
          (exists (?x_13 )
            (and
              (LeftOf1 ?x_13)
              (RightOf14 ?x_13)
            )
          )
          (exists (?x_5 )
            (and
              (BelowOf1 ?x_5)
              (AboveOf7 ?x_5)
            )
          )
          (exists (?x_3 )
            (and
              (RightOf14 ?x_3)
              (LeftOf9 ?x_3)
            )
          )
          (exists (?x_25 )
            (and
              (RightOf14 ?x_25)
              (LeftOf10 ?x_25)
            )
          )
          (exists (?x_22 )
            (and
              (RightOf8 ?x_22)
              (LeftOf6 ?x_22)
            )
          )
          (exists (?x_2 )
            (and
              (BelowOf5 ?x_2)
              (AboveOf13 ?x_2)
            )
          )
          (exists (?x_1 )
            (and
              (BelowOf2 ?x_1)
              (AboveOf7 ?x_1)
            )
          )
          (exists (?x_16 )
            (and
              (AboveOf13 ?x_16)
              (BelowOf5 ?x_16)
            )
          )
          (exists (?x_7 )
            (and
              (AboveOf11 ?x_7)
              (BelowOf3 ?x_7)
            )
          )
          (exists (?x_25 )
            (and
              (RightOf13 ?x_25)
              (LeftOf2 ?x_25)
            )
          )
          (exists (?x_24 )
            (and
              (LeftOf8 ?x_24)
              (RightOf10 ?x_24)
            )
          )
          (exists (?x_13 )
            (and
              (LeftOf7 ?x_13)
              (RightOf12 ?x_13)
            )
          )
          (exists (?x_0 )
            (and
              (RightOf5 ?x_0)
              (LeftOf1 ?x_0)
            )
          )
          (exists (?x_27 )
            (and
              (RightOf9 ?x_27)
              (LeftOf3 ?x_27)
            )
          )
          (exists (?x_26 )
            (and
              (RightOf9 ?x_26)
              (LeftOf6 ?x_26)
            )
          )
          (exists (?x_26 )
            (and
              (LeftOf9 ?x_26)
              (RightOf13 ?x_26)
            )
          )
          (exists (?x_10 )
            (and
              (RightOf13 ?x_10)
              (LeftOf3 ?x_10)
            )
          )
          (exists (?x_22 )
            (and
              (LeftOf4 ?x_22)
              (RightOf7 ?x_22)
            )
          )
          (exists (?x_14 )
            (and
              (RightOf11 ?x_14)
              (LeftOf3 ?x_14)
            )
          )
          (exists (?x_4 )
            (and
              (AboveOf11 ?x_4)
              (BelowOf6 ?x_4)
            )
          )
          (exists (?x_13 )
            (and
              (RightOf12 ?x_13)
              (LeftOf3 ?x_13)
            )
          )
          (exists (?x_5 )
            (and
              (BelowOf2 ?x_5)
              (AboveOf13 ?x_5)
            )
          )
          (exists (?x_5 )
            (and
              (BelowOf4 ?x_5)
              (AboveOf11 ?x_5)
            )
          )
          (exists (?x_19 )
            (and
              (RightOf12 ?x_19)
              (LeftOf1 ?x_19)
            )
          )
          (exists (?x_26 )
            (and
              (RightOf12 ?x_26)
              (LeftOf2 ?x_26)
            )
          )
          (exists (?x_1 )
            (and
              (BelowOf1 ?x_1)
              (AboveOf12 ?x_1)
            )
          )
          (exists (?x_5 )
            (and
              (AboveOf10 ?x_5)
              (BelowOf2 ?x_5)
            )
          )
          (exists (?x_16 )
            (and
              (BelowOf5 ?x_16)
              (AboveOf12 ?x_16)
            )
          )
          (exists (?x_27 )
            (and
              (RightOf11 ?x_27)
              (LeftOf1 ?x_27)
            )
          )
          (exists (?x_3 )
            (and
              (RightOf13 ?x_3)
              (LeftOf5 ?x_3)
            )
          )
          (exists (?x_6 )
            (and
              (LeftOf8 ?x_6)
              (RightOf13 ?x_6)
            )
          )
          (exists (?x_10 )
            (and
              (LeftOf7 ?x_10)
              (RightOf12 ?x_10)
            )
          )
          (exists (?x_4 )
            (and
              (BelowOf8 ?x_4)
              (AboveOf12 ?x_4)
            )
          )
          (exists (?x_13 )
            (and
              (LeftOf7 ?x_13)
              (RightOf9 ?x_13)
            )
          )
          (exists (?x_4 )
            (and
              (BelowOf3 ?x_4)
              (AboveOf12 ?x_4)
            )
          )
          (exists (?x_27 )
            (and
              (LeftOf3 ?x_27)
              (RightOf7 ?x_27)
            )
          )
          (exists (?x_3 )
            (and
              (LeftOf6 ?x_3)
              (RightOf11 ?x_3)
            )
          )
          (exists (?x_10 )
            (and
              (RightOf13 ?x_10)
              (LeftOf7 ?x_10)
            )
          )
          (exists (?x_7 )
            (and
              (BelowOf3 ?x_7)
              (AboveOf14 ?x_7)
            )
          )
          (exists (?x_13 )
            (and
              (LeftOf4 ?x_13)
              (RightOf8 ?x_13)
            )
          )
          (exists (?x_15 )
            (and
              (LeftOf4 ?x_15)
              (RightOf13 ?x_15)
            )
          )
          (exists (?x_22 )
            (and
              (RightOf13 ?x_22)
              (LeftOf6 ?x_22)
            )
          )
          (exists (?x_9 )
            (and
              (BelowOf8 ?x_9)
              (AboveOf14 ?x_9)
            )
          )
          (exists (?x_10 )
            (and
              (LeftOf3 ?x_10)
              (RightOf11 ?x_10)
            )
          )
          (exists (?x_4 )
            (and
              (AboveOf13 ?x_4)
              (BelowOf10 ?x_4)
            )
          )
          (exists (?x_27 )
            (and
              (RightOf5 ?x_27)
              (LeftOf2 ?x_27)
            )
          )
          (exists (?x_18 )
            (and
              (AboveOf10 ?x_18)
              (BelowOf4 ?x_18)
            )
          )
          (exists (?x_5 )
            (and
              (BelowOf2 ?x_5)
              (AboveOf7 ?x_5)
            )
          )
          (exists (?x_15 )
            (and
              (RightOf4 ?x_15)
              (LeftOf2 ?x_15)
            )
          )
          (exists (?x_16 )
            (and
              (AboveOf13 ?x_16)
              (BelowOf3 ?x_16)
            )
          )
          (exists (?x_24 )
            (and
              (RightOf12 ?x_24)
              (LeftOf7 ?x_24)
            )
          )
          (exists (?x_17 )
            (and
              (BelowOf2 ?x_17)
              (AboveOf7 ?x_17)
            )
          )
          (exists (?x_16 )
            (and
              (AboveOf9 ?x_16)
              (BelowOf6 ?x_16)
            )
          )
          (exists (?x_6 )
            (and
              (LeftOf11 ?x_6)
              (RightOf14 ?x_6)
            )
          )
          (exists (?x_7 )
            (and
              (AboveOf8 ?x_7)
              (BelowOf2 ?x_7)
            )
          )
          (exists (?x_25 )
            (and
              (RightOf12 ?x_25)
              (LeftOf9 ?x_25)
            )
          )
        )
        (Error)
      )
    )
  )
)