(define (problem robotProblem )
  (:domain robot)
  (:objects
    robot 
  )
  (:init
    (LeftOf18 robot)
    (AboveOf0 robot)
    (RightOf2 robot)
    (BelowOf18 robot)
  )
  (:goal
    (and
      (not (CheckConsistency))
      (not (Error))
      (Row1 robot)
      (Column2 robot)
    )
  )
)