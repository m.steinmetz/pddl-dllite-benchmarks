(define (problem robotProblem )
  (:domain robot)
  (:objects
    robot 
  )
  (:init
    (LeftOf3 robot)
    (RightOf2 robot)
    (BelowOf3 robot)
    (AboveOf0 robot)
  )
  (:goal
    (and
      (not (CheckConsistency))
      (not (Error))
      (Row1 robot)
      (Column2 robot)
    )
  )
)