(define
  (domain robot)
  (:requirements :adl)
  (:predicates
    (AboveOf18 ?x )
    (AboveOf16 ?x )
    (AboveOf17 ?x )
    (AboveOf14 ?x )
    (AboveOf15 ?x )
    (AboveOf12 ?x )
    (AboveOf13 ?x )
    (AboveOf10 ?x )
    (AboveOf11 ?x )
    (BelowOf8 ?x )
    (BelowOf9 ?x )
    (AboveOf2 ?x )
    (BelowOf12 ?x )
    (BelowOf13 ?x )
    (BelowOf10 ?x )
    (BelowOf11 ?x )
    (BelowOf16 ?x )
    (BelowOf17 ?x )
    (BelowOf14 ?x )
    (BelowOf15 ?x )
    (BelowOf18 ?x )
    (BelowOf19 ?x )
    (LeftOf3 ?x )
    (LeftOf2 ?x )
    (LeftOf1 ?x )
    (LeftOf7 ?x )
    (LeftOf6 ?x )
    (LeftOf5 ?x )
    (LeftOf4 ?x )
    (LeftOf9 ?x )
    (LeftOf8 ?x )
    (Row18 ?x )
    (Row12 ?x )
    (Row13 ?x )
    (Row10 ?x )
    (Row11 ?x )
    (Row16 ?x )
    (Row17 ?x )
    (Row14 ?x )
    (Row15 ?x )
    (LeftOf13 ?x )
    (Column18 ?x )
    (LeftOf12 ?x )
    (Column11 ?x )
    (Column10 ?x )
    (Column13 ?x )
    (Column12 ?x )
    (Column15 ?x )
    (Column14 ?x )
    (Column17 ?x )
    (Column16 ?x )
    (AboveOf4 ?x )
    (LeftOf16 ?x )
    (AboveOf6 ?x )
    (AboveOf7 ?x )
    (AboveOf0 ?x )
    (AboveOf1 ?x )
    (LeftOf11 ?x )
    (AboveOf3 ?x )
    (AboveOf8 ?x )
    (AboveOf9 ?x )
    (LeftOf19 ?x )
    (LeftOf18 ?x )
    (RightOf9 ?x )
    (RightOf8 ?x )
    (RightOf3 ?x )
    (RightOf2 ?x )
    (RightOf1 ?x )
    (RightOf0 ?x )
    (RightOf7 ?x )
    (RightOf6 ?x )
    (RightOf5 ?x )
    (RightOf4 ?x )
    (Columns ?x )
    (BelowOf1 ?x )
    (BelowOf2 ?x )
    (BelowOf3 ?x )
    (Column9 ?x )
    (Column8 ?x )
    (BelowOf6 ?x )
    (BelowOf7 ?x )
    (Column5 ?x )
    (Column4 ?x )
    (Column7 ?x )
    (Column6 ?x )
    (Column1 ?x )
    (Column0 ?x )
    (Column3 ?x )
    (Column2 ?x )
    (Rows ?x )
    (LeftOf17 ?x )
    (AboveOf5 ?x )
    (LeftOf15 ?x )
    (LeftOf14 ?x )
    (RightOf18 ?x )
    (RightOf17 ?x )
    (RightOf16 ?x )
    (RightOf15 ?x )
    (RightOf14 ?x )
    (RightOf13 ?x )
    (RightOf12 ?x )
    (RightOf11 ?x )
    (RightOf10 ?x )
    (Row0 ?x )
    (Row1 ?x )
    (Row2 ?x )
    (Row3 ?x )
    (Row4 ?x )
    (Row5 ?x )
    (Row6 ?x )
    (Row7 ?x )
    (Row8 ?x )
    (Row9 ?x )
    (LeftOf10 ?x )
    (BelowOf4 ?x )
    (BelowOf5 ?x )
    (CheckConsistency)
    (Error)
  )
  (:action moveLeft
    :parameters ( ?x )
    :precondition ( and
      (not (CheckConsistency))
      (not (Error))
      (or
        (LeftOf3 ?x)
        (RightOf7 ?x)
        (RightOf8 ?x)
        (RightOf15 ?x)
        (LeftOf2 ?x)
        (LeftOf9 ?x)
        (RightOf2 ?x)
        (RightOf0 ?x)
        (LeftOf8 ?x)
        (RightOf12 ?x)
        (LeftOf14 ?x)
        (LeftOf5 ?x)
        (RightOf1 ?x)
        (LeftOf12 ?x)
        (LeftOf19 ?x)
        (Columns ?x)
        (RightOf13 ?x)
        (RightOf14 ?x)
        (LeftOf1 ?x)
        (RightOf6 ?x)
        (RightOf9 ?x)
        (LeftOf6 ?x)
        (RightOf11 ?x)
        (RightOf17 ?x)
        (LeftOf11 ?x)
        (RightOf5 ?x)
        (LeftOf10 ?x)
        (LeftOf15 ?x)
        (LeftOf4 ?x)
        (RightOf18 ?x)
        (LeftOf16 ?x)
        (RightOf16 ?x)
        (RightOf4 ?x)
        (RightOf10 ?x)
        (LeftOf7 ?x)
        (LeftOf13 ?x)
        (LeftOf17 ?x)
        (LeftOf18 ?x)
        (RightOf3 ?x)
      )
    )
    :effect ( and
      (CheckConsistency)
      (when
        (or
          (LeftOf2 ?x)
          (LeftOf1 ?x)
        )
        (and
          (LeftOf1 ?x)
        )
      )
      (when
        (or
          (LeftOf2 ?x)
          (LeftOf3 ?x)
          (LeftOf1 ?x)
        )
        (and
          (LeftOf2 ?x)
        )
      )
      (when
        (or
          (LeftOf2 ?x)
          (LeftOf4 ?x)
          (LeftOf3 ?x)
          (LeftOf1 ?x)
        )
        (and
          (LeftOf3 ?x)
        )
      )
      (when
        (or
          (LeftOf2 ?x)
          (LeftOf4 ?x)
          (LeftOf3 ?x)
          (LeftOf1 ?x)
          (LeftOf5 ?x)
        )
        (and
          (LeftOf4 ?x)
        )
      )
      (when
        (or
          (LeftOf4 ?x)
          (LeftOf3 ?x)
          (LeftOf5 ?x)
          (LeftOf6 ?x)
          (LeftOf1 ?x)
          (LeftOf2 ?x)
        )
        (and
          (LeftOf5 ?x)
        )
      )
      (when
        (or
          (LeftOf4 ?x)
          (LeftOf3 ?x)
          (LeftOf5 ?x)
          (LeftOf6 ?x)
          (LeftOf1 ?x)
          (LeftOf7 ?x)
          (LeftOf2 ?x)
        )
        (and
          (LeftOf6 ?x)
        )
      )
      (when
        (or
          (LeftOf8 ?x)
          (LeftOf4 ?x)
          (LeftOf3 ?x)
          (LeftOf5 ?x)
          (LeftOf6 ?x)
          (LeftOf1 ?x)
          (LeftOf7 ?x)
          (LeftOf2 ?x)
        )
        (and
          (LeftOf7 ?x)
        )
      )
      (when
        (or
          (LeftOf8 ?x)
          (LeftOf4 ?x)
          (LeftOf3 ?x)
          (LeftOf5 ?x)
          (LeftOf6 ?x)
          (LeftOf1 ?x)
          (LeftOf7 ?x)
          (LeftOf2 ?x)
          (LeftOf9 ?x)
        )
        (and
          (LeftOf8 ?x)
        )
      )
      (when
        (or
          (LeftOf8 ?x)
          (LeftOf4 ?x)
          (LeftOf3 ?x)
          (LeftOf5 ?x)
          (LeftOf6 ?x)
          (LeftOf1 ?x)
          (LeftOf7 ?x)
          (LeftOf2 ?x)
          (LeftOf9 ?x)
          (LeftOf10 ?x)
        )
        (and
          (LeftOf9 ?x)
        )
      )
      (when
        (or
          (LeftOf8 ?x)
          (LeftOf4 ?x)
          (LeftOf3 ?x)
          (LeftOf5 ?x)
          (LeftOf6 ?x)
          (LeftOf1 ?x)
          (LeftOf7 ?x)
          (LeftOf2 ?x)
          (LeftOf9 ?x)
          (LeftOf11 ?x)
          (LeftOf10 ?x)
        )
        (and
          (LeftOf10 ?x)
        )
      )
      (when
        (or
          (LeftOf8 ?x)
          (LeftOf4 ?x)
          (LeftOf3 ?x)
          (LeftOf5 ?x)
          (LeftOf6 ?x)
          (LeftOf12 ?x)
          (LeftOf1 ?x)
          (LeftOf7 ?x)
          (LeftOf2 ?x)
          (LeftOf9 ?x)
          (LeftOf11 ?x)
          (LeftOf10 ?x)
        )
        (and
          (LeftOf11 ?x)
        )
      )
      (when
        (or
          (LeftOf8 ?x)
          (LeftOf7 ?x)
          (LeftOf4 ?x)
          (LeftOf3 ?x)
          (LeftOf5 ?x)
          (LeftOf6 ?x)
          (LeftOf12 ?x)
          (LeftOf1 ?x)
          (LeftOf13 ?x)
          (LeftOf2 ?x)
          (LeftOf9 ?x)
          (LeftOf11 ?x)
          (LeftOf10 ?x)
        )
        (and
          (LeftOf12 ?x)
        )
      )
      (when
        (or
          (LeftOf8 ?x)
          (LeftOf7 ?x)
          (LeftOf4 ?x)
          (LeftOf3 ?x)
          (LeftOf14 ?x)
          (LeftOf5 ?x)
          (LeftOf6 ?x)
          (LeftOf12 ?x)
          (LeftOf1 ?x)
          (LeftOf13 ?x)
          (LeftOf2 ?x)
          (LeftOf9 ?x)
          (LeftOf11 ?x)
          (LeftOf10 ?x)
        )
        (and
          (LeftOf13 ?x)
        )
      )
      (when
        (or
          (LeftOf8 ?x)
          (LeftOf7 ?x)
          (LeftOf4 ?x)
          (LeftOf3 ?x)
          (LeftOf14 ?x)
          (LeftOf5 ?x)
          (LeftOf6 ?x)
          (LeftOf12 ?x)
          (LeftOf1 ?x)
          (LeftOf13 ?x)
          (LeftOf2 ?x)
          (LeftOf9 ?x)
          (LeftOf11 ?x)
          (LeftOf10 ?x)
          (LeftOf15 ?x)
        )
        (and
          (LeftOf14 ?x)
        )
      )
      (when
        (or
          (LeftOf8 ?x)
          (LeftOf7 ?x)
          (LeftOf4 ?x)
          (LeftOf3 ?x)
          (LeftOf14 ?x)
          (LeftOf16 ?x)
          (LeftOf5 ?x)
          (LeftOf6 ?x)
          (LeftOf12 ?x)
          (LeftOf1 ?x)
          (LeftOf13 ?x)
          (LeftOf2 ?x)
          (LeftOf9 ?x)
          (LeftOf11 ?x)
          (LeftOf10 ?x)
          (LeftOf15 ?x)
        )
        (and
          (LeftOf15 ?x)
        )
      )
      (when
        (or
          (LeftOf8 ?x)
          (LeftOf7 ?x)
          (LeftOf4 ?x)
          (LeftOf3 ?x)
          (LeftOf14 ?x)
          (LeftOf16 ?x)
          (LeftOf5 ?x)
          (LeftOf6 ?x)
          (LeftOf12 ?x)
          (LeftOf1 ?x)
          (LeftOf13 ?x)
          (LeftOf2 ?x)
          (LeftOf9 ?x)
          (LeftOf11 ?x)
          (LeftOf17 ?x)
          (LeftOf10 ?x)
          (LeftOf15 ?x)
        )
        (and
          (LeftOf16 ?x)
        )
      )
      (when
        (or
          (LeftOf8 ?x)
          (LeftOf7 ?x)
          (LeftOf4 ?x)
          (LeftOf3 ?x)
          (LeftOf14 ?x)
          (LeftOf16 ?x)
          (LeftOf5 ?x)
          (LeftOf6 ?x)
          (LeftOf12 ?x)
          (LeftOf1 ?x)
          (LeftOf13 ?x)
          (LeftOf2 ?x)
          (LeftOf9 ?x)
          (LeftOf11 ?x)
          (LeftOf17 ?x)
          (LeftOf18 ?x)
          (LeftOf10 ?x)
          (LeftOf15 ?x)
        )
        (and
          (LeftOf17 ?x)
        )
      )
      (when
        (or
          (LeftOf8 ?x)
          (LeftOf7 ?x)
          (LeftOf4 ?x)
          (LeftOf3 ?x)
          (LeftOf14 ?x)
          (LeftOf16 ?x)
          (LeftOf5 ?x)
          (LeftOf6 ?x)
          (LeftOf12 ?x)
          (LeftOf19 ?x)
          (LeftOf13 ?x)
          (LeftOf2 ?x)
          (LeftOf9 ?x)
          (LeftOf11 ?x)
          (LeftOf17 ?x)
          (LeftOf18 ?x)
          (LeftOf1 ?x)
          (LeftOf10 ?x)
          (LeftOf15 ?x)
        )
        (and
          (LeftOf18 ?x)
        )
      )
      (when
        (or
          (RightOf6 ?x)
          (RightOf15 ?x)
          (RightOf11 ?x)
          (RightOf7 ?x)
          (RightOf18 ?x)
          (RightOf16 ?x)
          (RightOf1 ?x)
          (RightOf4 ?x)
          (RightOf10 ?x)
          (RightOf12 ?x)
          (RightOf13 ?x)
          (RightOf14 ?x)
          (RightOf17 ?x)
          (RightOf2 ?x)
          (RightOf9 ?x)
          (RightOf5 ?x)
          (RightOf8 ?x)
          (RightOf3 ?x)
        )
        (and
          (RightOf0 ?x)
          (not (RightOf1 ?x))
        )
      )
      (when
        (or
          (RightOf6 ?x)
          (RightOf15 ?x)
          (RightOf11 ?x)
          (RightOf7 ?x)
          (RightOf18 ?x)
          (RightOf16 ?x)
          (RightOf8 ?x)
          (RightOf4 ?x)
          (RightOf10 ?x)
          (RightOf12 ?x)
          (RightOf13 ?x)
          (RightOf14 ?x)
          (RightOf17 ?x)
          (RightOf2 ?x)
          (RightOf9 ?x)
          (RightOf5 ?x)
          (RightOf3 ?x)
        )
        (and
          (RightOf1 ?x)
          (not (RightOf2 ?x))
        )
      )
      (when
        (or
          (RightOf6 ?x)
          (RightOf15 ?x)
          (RightOf11 ?x)
          (RightOf18 ?x)
          (RightOf16 ?x)
          (RightOf8 ?x)
          (RightOf4 ?x)
          (RightOf10 ?x)
          (RightOf12 ?x)
          (RightOf13 ?x)
          (RightOf14 ?x)
          (RightOf17 ?x)
          (RightOf9 ?x)
          (RightOf5 ?x)
          (RightOf3 ?x)
          (RightOf7 ?x)
        )
        (and
          (RightOf2 ?x)
          (not (RightOf3 ?x))
        )
      )
      (when
        (or
          (RightOf6 ?x)
          (RightOf15 ?x)
          (RightOf11 ?x)
          (RightOf18 ?x)
          (RightOf16 ?x)
          (RightOf8 ?x)
          (RightOf4 ?x)
          (RightOf10 ?x)
          (RightOf12 ?x)
          (RightOf13 ?x)
          (RightOf14 ?x)
          (RightOf17 ?x)
          (RightOf9 ?x)
          (RightOf5 ?x)
          (RightOf7 ?x)
        )
        (and
          (RightOf3 ?x)
          (not (RightOf4 ?x))
        )
      )
      (when
        (or
          (RightOf6 ?x)
          (RightOf15 ?x)
          (RightOf11 ?x)
          (RightOf18 ?x)
          (RightOf16 ?x)
          (RightOf8 ?x)
          (RightOf10 ?x)
          (RightOf12 ?x)
          (RightOf13 ?x)
          (RightOf14 ?x)
          (RightOf17 ?x)
          (RightOf9 ?x)
          (RightOf5 ?x)
          (RightOf7 ?x)
        )
        (and
          (RightOf4 ?x)
          (not (RightOf5 ?x))
        )
      )
      (when
        (or
          (RightOf6 ?x)
          (RightOf15 ?x)
          (RightOf11 ?x)
          (RightOf18 ?x)
          (RightOf16 ?x)
          (RightOf8 ?x)
          (RightOf10 ?x)
          (RightOf12 ?x)
          (RightOf13 ?x)
          (RightOf14 ?x)
          (RightOf17 ?x)
          (RightOf9 ?x)
          (RightOf7 ?x)
        )
        (and
          (RightOf5 ?x)
          (not (RightOf6 ?x))
        )
      )
      (when
        (or
          (RightOf11 ?x)
          (RightOf9 ?x)
          (RightOf7 ?x)
          (RightOf16 ?x)
          (RightOf12 ?x)
          (RightOf8 ?x)
          (RightOf10 ?x)
          (RightOf15 ?x)
          (RightOf13 ?x)
          (RightOf14 ?x)
          (RightOf17 ?x)
          (RightOf18 ?x)
        )
        (and
          (RightOf6 ?x)
          (not (RightOf7 ?x))
        )
      )
      (when
        (or
          (RightOf11 ?x)
          (RightOf9 ?x)
          (RightOf12 ?x)
          (RightOf16 ?x)
          (RightOf8 ?x)
          (RightOf10 ?x)
          (RightOf15 ?x)
          (RightOf13 ?x)
          (RightOf14 ?x)
          (RightOf17 ?x)
          (RightOf18 ?x)
        )
        (and
          (RightOf7 ?x)
          (not (RightOf8 ?x))
        )
      )
      (when
        (or
          (RightOf11 ?x)
          (RightOf18 ?x)
          (RightOf12 ?x)
          (RightOf16 ?x)
          (RightOf10 ?x)
          (RightOf15 ?x)
          (RightOf13 ?x)
          (RightOf14 ?x)
          (RightOf17 ?x)
          (RightOf9 ?x)
        )
        (and
          (RightOf8 ?x)
          (not (RightOf9 ?x))
        )
      )
      (when
        (or
          (RightOf11 ?x)
          (RightOf18 ?x)
          (RightOf12 ?x)
          (RightOf16 ?x)
          (RightOf10 ?x)
          (RightOf15 ?x)
          (RightOf13 ?x)
          (RightOf14 ?x)
          (RightOf17 ?x)
        )
        (and
          (RightOf9 ?x)
          (not (RightOf10 ?x))
        )
      )
      (when
        (or
          (RightOf11 ?x)
          (RightOf18 ?x)
          (RightOf12 ?x)
          (RightOf16 ?x)
          (RightOf15 ?x)
          (RightOf13 ?x)
          (RightOf14 ?x)
          (RightOf17 ?x)
        )
        (and
          (RightOf10 ?x)
          (not (RightOf11 ?x))
        )
      )
      (when
        (or
          (RightOf15 ?x)
          (RightOf18 ?x)
          (RightOf16 ?x)
          (RightOf12 ?x)
          (RightOf13 ?x)
          (RightOf14 ?x)
          (RightOf17 ?x)
        )
        (and
          (RightOf11 ?x)
          (not (RightOf12 ?x))
        )
      )
      (when
        (or
          (RightOf18 ?x)
          (RightOf16 ?x)
          (RightOf15 ?x)
          (RightOf13 ?x)
          (RightOf14 ?x)
          (RightOf17 ?x)
        )
        (and
          (RightOf12 ?x)
          (not (RightOf13 ?x))
        )
      )
      (when
        (or
          (RightOf15 ?x)
          (RightOf18 ?x)
          (RightOf14 ?x)
          (RightOf17 ?x)
          (RightOf16 ?x)
        )
        (and
          (RightOf13 ?x)
          (not (RightOf14 ?x))
        )
      )
      (when
        (or
          (RightOf15 ?x)
          (RightOf18 ?x)
          (RightOf17 ?x)
          (RightOf16 ?x)
        )
        (and
          (RightOf14 ?x)
          (not (RightOf15 ?x))
        )
      )
      (when
        (or
          (RightOf18 ?x)
          (RightOf17 ?x)
          (RightOf16 ?x)
        )
        (and
          (RightOf15 ?x)
          (not (RightOf16 ?x))
        )
      )
      (when
        (or
          (RightOf18 ?x)
          (RightOf17 ?x)
        )
        (and
          (RightOf16 ?x)
          (not (RightOf17 ?x))
        )
      )
      (when
        (RightOf18 ?x)
        (and
          (RightOf17 ?x)
          (not (RightOf18 ?x))
        )
      )
      (when
        (Column1 ?x)
        (and
          (Column0 ?x)
          (not (Column1 ?x))
        )
      )
      (when
        (Column2 ?x)
        (and
          (Column1 ?x)
          (not (Column2 ?x))
        )
      )
      (when
        (Column3 ?x)
        (and
          (Column2 ?x)
          (not (Column3 ?x))
        )
      )
      (when
        (Column4 ?x)
        (and
          (Column3 ?x)
          (not (Column4 ?x))
        )
      )
      (when
        (Column5 ?x)
        (and
          (Column4 ?x)
          (not (Column5 ?x))
        )
      )
      (when
        (Column6 ?x)
        (and
          (Column5 ?x)
          (not (Column6 ?x))
        )
      )
      (when
        (Column7 ?x)
        (and
          (Column6 ?x)
          (not (Column7 ?x))
        )
      )
      (when
        (Column8 ?x)
        (and
          (Column7 ?x)
          (not (Column8 ?x))
        )
      )
      (when
        (Column9 ?x)
        (and
          (Column8 ?x)
          (not (Column9 ?x))
        )
      )
      (when
        (Column10 ?x)
        (and
          (Column9 ?x)
          (not (Column10 ?x))
        )
      )
      (when
        (Column11 ?x)
        (and
          (Column10 ?x)
          (not (Column11 ?x))
        )
      )
      (when
        (Column12 ?x)
        (and
          (Column11 ?x)
          (not (Column12 ?x))
        )
      )
      (when
        (Column13 ?x)
        (and
          (Column12 ?x)
          (not (Column13 ?x))
        )
      )
      (when
        (Column14 ?x)
        (and
          (Column13 ?x)
          (not (Column14 ?x))
        )
      )
      (when
        (Column15 ?x)
        (and
          (Column14 ?x)
          (not (Column15 ?x))
        )
      )
      (when
        (Column16 ?x)
        (and
          (Column15 ?x)
          (not (Column16 ?x))
        )
      )
      (when
        (Column17 ?x)
        (and
          (Column16 ?x)
          (not (Column17 ?x))
        )
      )
      (when
        (Column18 ?x)
        (and
          (Column17 ?x)
          (not (Column18 ?x))
        )
      )
      (when
        (or
          (and
            (RightOf4 ?x)
            (LeftOf2 ?x)
          )
          (and
            (RightOf6 ?x)
            (LeftOf2 ?x)
          )
          (and
            (RightOf16 ?x)
            (LeftOf1 ?x)
          )
          (and
            (RightOf14 ?x)
            (LeftOf1 ?x)
          )
          (and
            (RightOf6 ?x)
            (LeftOf1 ?x)
          )
          (and
            (RightOf2 ?x)
            (LeftOf1 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf2 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf8 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf11 ?x)
          )
          (and
            (LeftOf1 ?x)
            (RightOf13 ?x)
          )
          (and
            (RightOf17 ?x)
            (LeftOf1 ?x)
          )
          (and
            (LeftOf1 ?x)
            (RightOf8 ?x)
          )
          (and
            (RightOf5 ?x)
            (LeftOf1 ?x)
          )
          (and
            (RightOf12 ?x)
            (LeftOf2 ?x)
          )
          (and
            (RightOf7 ?x)
            (LeftOf1 ?x)
          )
          (and
            (RightOf12 ?x)
            (LeftOf1 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf10 ?x)
          )
          (and
            (RightOf4 ?x)
            (LeftOf1 ?x)
          )
          (and
            (RightOf14 ?x)
            (LeftOf2 ?x)
          )
          (and
            (RightOf17 ?x)
            (LeftOf2 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf3 ?x)
          )
          (and
            (LeftOf1 ?x)
            (RightOf11 ?x)
          )
          (and
            (LeftOf1 ?x)
            (RightOf5 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf16 ?x)
          )
          (and
            (RightOf7 ?x)
            (LeftOf2 ?x)
          )
          (and
            (RightOf1 ?x)
            (LeftOf1 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf5 ?x)
          )
          (and
            (RightOf1 ?x)
            (LeftOf2 ?x)
          )
          (and
            (LeftOf1 ?x)
            (RightOf10 ?x)
          )
          (and
            (RightOf15 ?x)
            (LeftOf1 ?x)
          )
          (and
            (LeftOf1 ?x)
            (RightOf18 ?x)
          )
          (and
            (LeftOf1 ?x)
            (RightOf3 ?x)
          )
          (and
            (RightOf9 ?x)
            (LeftOf1 ?x)
          )
          (and
            (RightOf9 ?x)
            (LeftOf2 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf13 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf18 ?x)
          )
          (and
            (RightOf15 ?x)
            (LeftOf2 ?x)
          )
        )
        (and
          (Column0 ?x)
        )
      )
      (when
        (or
          (and
            (RightOf2 ?x)
            (LeftOf3 ?x)
          )
          (and
            (RightOf4 ?x)
            (LeftOf2 ?x)
          )
          (and
            (RightOf9 ?x)
            (LeftOf3 ?x)
          )
          (and
            (RightOf6 ?x)
            (LeftOf2 ?x)
          )
          (and
            (RightOf16 ?x)
            (LeftOf1 ?x)
          )
          (and
            (RightOf14 ?x)
            (LeftOf1 ?x)
          )
          (and
            (RightOf6 ?x)
            (LeftOf1 ?x)
          )
          (and
            (RightOf2 ?x)
            (LeftOf1 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf2 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf8 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf11 ?x)
          )
          (and
            (LeftOf1 ?x)
            (RightOf13 ?x)
          )
          (and
            (RightOf17 ?x)
            (LeftOf3 ?x)
          )
          (and
            (LeftOf3 ?x)
            (RightOf13 ?x)
          )
          (and
            (RightOf18 ?x)
            (LeftOf3 ?x)
          )
          (and
            (RightOf11 ?x)
            (LeftOf3 ?x)
          )
          (and
            (LeftOf3 ?x)
            (RightOf3 ?x)
          )
          (and
            (LeftOf1 ?x)
            (RightOf8 ?x)
          )
          (and
            (RightOf15 ?x)
            (LeftOf3 ?x)
          )
          (and
            (RightOf5 ?x)
            (LeftOf3 ?x)
          )
          (and
            (LeftOf1 ?x)
            (RightOf18 ?x)
          )
          (and
            (RightOf12 ?x)
            (LeftOf2 ?x)
          )
          (and
            (RightOf7 ?x)
            (LeftOf2 ?x)
          )
          (and
            (RightOf8 ?x)
            (LeftOf3 ?x)
          )
          (and
            (RightOf10 ?x)
            (LeftOf3 ?x)
          )
          (and
            (RightOf12 ?x)
            (LeftOf1 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf10 ?x)
          )
          (and
            (RightOf4 ?x)
            (LeftOf1 ?x)
          )
          (and
            (RightOf14 ?x)
            (LeftOf2 ?x)
          )
          (and
            (RightOf12 ?x)
            (LeftOf3 ?x)
          )
          (and
            (RightOf17 ?x)
            (LeftOf2 ?x)
          )
          (and
            (RightOf5 ?x)
            (LeftOf1 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf3 ?x)
          )
          (and
            (LeftOf1 ?x)
            (RightOf11 ?x)
          )
          (and
            (RightOf7 ?x)
            (LeftOf3 ?x)
          )
          (and
            (LeftOf1 ?x)
            (RightOf5 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf16 ?x)
          )
          (and
            (RightOf7 ?x)
            (LeftOf1 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf5 ?x)
          )
          (and
            (RightOf16 ?x)
            (LeftOf3 ?x)
          )
          (and
            (LeftOf1 ?x)
            (RightOf10 ?x)
          )
          (and
            (RightOf15 ?x)
            (LeftOf1 ?x)
          )
          (and
            (RightOf4 ?x)
            (LeftOf3 ?x)
          )
          (and
            (RightOf17 ?x)
            (LeftOf1 ?x)
          )
          (and
            (RightOf14 ?x)
            (LeftOf3 ?x)
          )
          (and
            (LeftOf1 ?x)
            (RightOf3 ?x)
          )
          (and
            (RightOf9 ?x)
            (LeftOf1 ?x)
          )
          (and
            (RightOf6 ?x)
            (LeftOf3 ?x)
          )
          (and
            (RightOf9 ?x)
            (LeftOf2 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf13 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf18 ?x)
          )
          (and
            (RightOf15 ?x)
            (LeftOf2 ?x)
          )
        )
        (and
          (Column1 ?x)
        )
      )
      (when
        (or
          (and
            (RightOf4 ?x)
            (LeftOf2 ?x)
          )
          (and
            (RightOf9 ?x)
            (LeftOf3 ?x)
          )
          (and
            (RightOf6 ?x)
            (LeftOf2 ?x)
          )
          (and
            (RightOf11 ?x)
            (LeftOf4 ?x)
          )
          (and
            (LeftOf4 ?x)
            (RightOf3 ?x)
          )
          (and
            (RightOf6 ?x)
            (LeftOf1 ?x)
          )
          (and
            (LeftOf4 ?x)
            (RightOf13 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf8 ?x)
          )
          (and
            (RightOf5 ?x)
            (LeftOf4 ?x)
          )
          (and
            (RightOf6 ?x)
            (LeftOf4 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf11 ?x)
          )
          (and
            (LeftOf1 ?x)
            (RightOf13 ?x)
          )
          (and
            (RightOf16 ?x)
            (LeftOf4 ?x)
          )
          (and
            (RightOf9 ?x)
            (LeftOf4 ?x)
          )
          (and
            (RightOf17 ?x)
            (LeftOf3 ?x)
          )
          (and
            (LeftOf3 ?x)
            (RightOf13 ?x)
          )
          (and
            (RightOf18 ?x)
            (LeftOf3 ?x)
          )
          (and
            (LeftOf4 ?x)
            (RightOf10 ?x)
          )
          (and
            (RightOf12 ?x)
            (LeftOf4 ?x)
          )
          (and
            (RightOf11 ?x)
            (LeftOf3 ?x)
          )
          (and
            (RightOf17 ?x)
            (LeftOf1 ?x)
          )
          (and
            (LeftOf1 ?x)
            (RightOf8 ?x)
          )
          (and
            (RightOf15 ?x)
            (LeftOf3 ?x)
          )
          (and
            (RightOf5 ?x)
            (LeftOf3 ?x)
          )
          (and
            (RightOf16 ?x)
            (LeftOf1 ?x)
          )
          (and
            (LeftOf1 ?x)
            (RightOf18 ?x)
          )
          (and
            (LeftOf4 ?x)
            (RightOf8 ?x)
          )
          (and
            (RightOf12 ?x)
            (LeftOf2 ?x)
          )
          (and
            (RightOf7 ?x)
            (LeftOf2 ?x)
          )
          (and
            (RightOf3 ?x)
            (LeftOf3 ?x)
          )
          (and
            (RightOf10 ?x)
            (LeftOf3 ?x)
          )
          (and
            (RightOf12 ?x)
            (LeftOf1 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf10 ?x)
          )
          (and
            (RightOf14 ?x)
            (LeftOf4 ?x)
          )
          (and
            (RightOf4 ?x)
            (LeftOf1 ?x)
          )
          (and
            (RightOf14 ?x)
            (LeftOf2 ?x)
          )
          (and
            (RightOf12 ?x)
            (LeftOf3 ?x)
          )
          (and
            (RightOf17 ?x)
            (LeftOf2 ?x)
          )
          (and
            (RightOf5 ?x)
            (LeftOf1 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf3 ?x)
          )
          (and
            (LeftOf1 ?x)
            (RightOf11 ?x)
          )
          (and
            (RightOf7 ?x)
            (LeftOf3 ?x)
          )
          (and
            (RightOf15 ?x)
            (LeftOf4 ?x)
          )
          (and
            (LeftOf1 ?x)
            (RightOf5 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf16 ?x)
          )
          (and
            (RightOf7 ?x)
            (LeftOf1 ?x)
          )
          (and
            (RightOf8 ?x)
            (LeftOf3 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf5 ?x)
          )
          (and
            (RightOf7 ?x)
            (LeftOf4 ?x)
          )
          (and
            (RightOf16 ?x)
            (LeftOf3 ?x)
          )
          (and
            (RightOf4 ?x)
            (LeftOf4 ?x)
          )
          (and
            (RightOf17 ?x)
            (LeftOf4 ?x)
          )
          (and
            (LeftOf1 ?x)
            (RightOf10 ?x)
          )
          (and
            (RightOf15 ?x)
            (LeftOf1 ?x)
          )
          (and
            (RightOf4 ?x)
            (LeftOf3 ?x)
          )
          (and
            (RightOf13 ?x)
            (LeftOf3 ?x)
          )
          (and
            (RightOf14 ?x)
            (LeftOf1 ?x)
          )
          (and
            (RightOf14 ?x)
            (LeftOf3 ?x)
          )
          (and
            (LeftOf1 ?x)
            (RightOf3 ?x)
          )
          (and
            (RightOf9 ?x)
            (LeftOf1 ?x)
          )
          (and
            (RightOf6 ?x)
            (LeftOf3 ?x)
          )
          (and
            (RightOf9 ?x)
            (LeftOf2 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf13 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf18 ?x)
          )
          (and
            (LeftOf4 ?x)
            (RightOf18 ?x)
          )
          (and
            (RightOf15 ?x)
            (LeftOf2 ?x)
          )
        )
        (and
          (Column2 ?x)
        )
      )
      (when
        (or
          (and
            (LeftOf5 ?x)
            (RightOf17 ?x)
          )
          (and
            (RightOf4 ?x)
            (LeftOf2 ?x)
          )
          (and
            (RightOf9 ?x)
            (LeftOf3 ?x)
          )
          (and
            (RightOf6 ?x)
            (LeftOf2 ?x)
          )
          (and
            (RightOf11 ?x)
            (LeftOf4 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf5 ?x)
          )
          (and
            (RightOf6 ?x)
            (LeftOf1 ?x)
          )
          (and
            (RightOf5 ?x)
            (LeftOf3 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf14 ?x)
          )
          (and
            (LeftOf4 ?x)
            (RightOf13 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf8 ?x)
          )
          (and
            (RightOf5 ?x)
            (LeftOf4 ?x)
          )
          (and
            (RightOf6 ?x)
            (LeftOf4 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf11 ?x)
          )
          (and
            (LeftOf1 ?x)
            (RightOf13 ?x)
          )
          (and
            (RightOf16 ?x)
            (LeftOf4 ?x)
          )
          (and
            (RightOf9 ?x)
            (LeftOf4 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf18 ?x)
          )
          (and
            (RightOf17 ?x)
            (LeftOf3 ?x)
          )
          (and
            (LeftOf3 ?x)
            (RightOf13 ?x)
          )
          (and
            (RightOf18 ?x)
            (LeftOf3 ?x)
          )
          (and
            (LeftOf4 ?x)
            (RightOf10 ?x)
          )
          (and
            (RightOf12 ?x)
            (LeftOf4 ?x)
          )
          (and
            (RightOf11 ?x)
            (LeftOf3 ?x)
          )
          (and
            (RightOf17 ?x)
            (LeftOf1 ?x)
          )
          (and
            (RightOf14 ?x)
            (LeftOf2 ?x)
          )
          (and
            (RightOf12 ?x)
            (LeftOf3 ?x)
          )
          (and
            (RightOf5 ?x)
            (LeftOf1 ?x)
          )
          (and
            (RightOf16 ?x)
            (LeftOf1 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf8 ?x)
          )
          (and
            (RightOf12 ?x)
            (LeftOf5 ?x)
          )
          (and
            (LeftOf4 ?x)
            (RightOf8 ?x)
          )
          (and
            (RightOf12 ?x)
            (LeftOf2 ?x)
          )
          (and
            (RightOf7 ?x)
            (LeftOf2 ?x)
          )
          (and
            (RightOf10 ?x)
            (LeftOf4 ?x)
          )
          (and
            (RightOf10 ?x)
            (LeftOf3 ?x)
          )
          (and
            (RightOf12 ?x)
            (LeftOf1 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf10 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf10 ?x)
          )
          (and
            (RightOf4 ?x)
            (LeftOf1 ?x)
          )
          (and
            (RightOf6 ?x)
            (LeftOf5 ?x)
          )
          (and
            (RightOf8 ?x)
            (LeftOf4 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf16 ?x)
          )
          (and
            (RightOf15 ?x)
            (LeftOf3 ?x)
          )
          (and
            (RightOf17 ?x)
            (LeftOf2 ?x)
          )
          (and
            (RightOf14 ?x)
            (LeftOf4 ?x)
          )
          (and
            (LeftOf1 ?x)
            (RightOf11 ?x)
          )
          (and
            (RightOf7 ?x)
            (LeftOf3 ?x)
          )
          (and
            (RightOf15 ?x)
            (LeftOf4 ?x)
          )
          (and
            (LeftOf1 ?x)
            (RightOf5 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf16 ?x)
          )
          (and
            (RightOf7 ?x)
            (LeftOf1 ?x)
          )
          (and
            (RightOf8 ?x)
            (LeftOf3 ?x)
          )
          (and
            (LeftOf1 ?x)
            (RightOf18 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf5 ?x)
          )
          (and
            (RightOf7 ?x)
            (LeftOf4 ?x)
          )
          (and
            (RightOf15 ?x)
            (LeftOf2 ?x)
          )
          (and
            (RightOf16 ?x)
            (LeftOf3 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf13 ?x)
          )
          (and
            (RightOf4 ?x)
            (LeftOf4 ?x)
          )
          (and
            (RightOf17 ?x)
            (LeftOf4 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf11 ?x)
          )
          (and
            (LeftOf1 ?x)
            (RightOf8 ?x)
          )
          (and
            (LeftOf1 ?x)
            (RightOf10 ?x)
          )
          (and
            (RightOf15 ?x)
            (LeftOf1 ?x)
          )
          (and
            (RightOf4 ?x)
            (LeftOf3 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf7 ?x)
          )
          (and
            (RightOf13 ?x)
            (LeftOf3 ?x)
          )
          (and
            (RightOf18 ?x)
            (LeftOf4 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf18 ?x)
          )
          (and
            (RightOf14 ?x)
            (LeftOf1 ?x)
          )
          (and
            (RightOf14 ?x)
            (LeftOf3 ?x)
          )
          (and
            (RightOf9 ?x)
            (LeftOf1 ?x)
          )
          (and
            (RightOf6 ?x)
            (LeftOf3 ?x)
          )
          (and
            (RightOf9 ?x)
            (LeftOf2 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf4 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf13 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf15 ?x)
          )
          (and
            (LeftOf4 ?x)
            (RightOf18 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf9 ?x)
          )
        )
        (and
          (Column3 ?x)
        )
      )
      (when
        (or
          (and
            (RightOf11 ?x)
            (LeftOf4 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf11 ?x)
          )
          (and
            (RightOf9 ?x)
            (LeftOf4 ?x)
          )
          (and
            (LeftOf3 ?x)
            (RightOf13 ?x)
          )
          (and
            (RightOf16 ?x)
            (LeftOf1 ?x)
          )
          (and
            (RightOf12 ?x)
            (LeftOf4 ?x)
          )
          (and
            (RightOf11 ?x)
            (LeftOf3 ?x)
          )
          (and
            (LeftOf1 ?x)
            (RightOf8 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf8 ?x)
          )
          (and
            (LeftOf4 ?x)
            (RightOf8 ?x)
          )
          (and
            (RightOf16 ?x)
            (LeftOf3 ?x)
          )
          (and
            (LeftOf1 ?x)
            (RightOf11 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf8 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf16 ?x)
          )
          (and
            (RightOf8 ?x)
            (LeftOf3 ?x)
          )
          (and
            (RightOf18 ?x)
            (LeftOf4 ?x)
          )
          (and
            (RightOf17 ?x)
            (LeftOf4 ?x)
          )
          (and
            (LeftOf1 ?x)
            (RightOf18 ?x)
          )
          (and
            (RightOf6 ?x)
            (LeftOf3 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf16 ?x)
          )
          (and
            (LeftOf4 ?x)
            (RightOf18 ?x)
          )
          (and
            (RightOf15 ?x)
            (LeftOf2 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf17 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf9 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf11 ?x)
          )
          (and
            (RightOf6 ?x)
            (LeftOf2 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf5 ?x)
          )
          (and
            (RightOf6 ?x)
            (LeftOf4 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf13 ?x)
          )
          (and
            (LeftOf1 ?x)
            (RightOf13 ?x)
          )
          (and
            (LeftOf4 ?x)
            (RightOf10 ?x)
          )
          (and
            (RightOf17 ?x)
            (LeftOf1 ?x)
          )
          (and
            (RightOf5 ?x)
            (LeftOf3 ?x)
          )
          (and
            (RightOf14 ?x)
            (LeftOf2 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf10 ?x)
          )
          (and
            (RightOf12 ?x)
            (LeftOf3 ?x)
          )
          (and
            (LeftOf1 ?x)
            (RightOf5 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf7 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf14 ?x)
          )
          (and
            (RightOf16 ?x)
            (LeftOf4 ?x)
          )
          (and
            (RightOf7 ?x)
            (LeftOf4 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf18 ?x)
          )
          (and
            (RightOf14 ?x)
            (LeftOf3 ?x)
          )
          (and
            (RightOf9 ?x)
            (LeftOf1 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf15 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf9 ?x)
          )
          (and
            (RightOf9 ?x)
            (LeftOf3 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf18 ?x)
          )
          (and
            (RightOf14 ?x)
            (LeftOf1 ?x)
          )
          (and
            (LeftOf4 ?x)
            (RightOf13 ?x)
          )
          (and
            (RightOf5 ?x)
            (LeftOf4 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf6 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf10 ?x)
          )
          (and
            (RightOf15 ?x)
            (LeftOf4 ?x)
          )
          (and
            (RightOf18 ?x)
            (LeftOf3 ?x)
          )
          (and
            (RightOf12 ?x)
            (LeftOf5 ?x)
          )
          (and
            (RightOf7 ?x)
            (LeftOf1 ?x)
          )
          (and
            (RightOf12 ?x)
            (LeftOf1 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf17 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf16 ?x)
          )
          (and
            (RightOf15 ?x)
            (LeftOf3 ?x)
          )
          (and
            (RightOf17 ?x)
            (LeftOf2 ?x)
          )
          (and
            (RightOf14 ?x)
            (LeftOf4 ?x)
          )
          (and
            (RightOf7 ?x)
            (LeftOf3 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf12 ?x)
          )
          (and
            (RightOf7 ?x)
            (LeftOf2 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf5 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf7 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf13 ?x)
          )
          (and
            (LeftOf1 ?x)
            (RightOf10 ?x)
          )
          (and
            (RightOf13 ?x)
            (LeftOf3 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf13 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf14 ?x)
          )
          (and
            (RightOf6 ?x)
            (LeftOf1 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf8 ?x)
          )
          (and
            (RightOf17 ?x)
            (LeftOf3 ?x)
          )
          (and
            (RightOf9 ?x)
            (LeftOf2 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf5 ?x)
          )
          (and
            (RightOf12 ?x)
            (LeftOf2 ?x)
          )
          (and
            (RightOf10 ?x)
            (LeftOf4 ?x)
          )
          (and
            (RightOf10 ?x)
            (LeftOf3 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf10 ?x)
          )
          (and
            (RightOf6 ?x)
            (LeftOf5 ?x)
          )
          (and
            (RightOf8 ?x)
            (LeftOf4 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf11 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf15 ?x)
          )
          (and
            (RightOf15 ?x)
            (LeftOf1 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf18 ?x)
          )
        )
        (and
          (Column4 ?x)
        )
      )
      (when
        (or
          (and
            (RightOf11 ?x)
            (LeftOf4 ?x)
          )
          (and
            (LeftOf7 ?x)
            (RightOf10 ?x)
          )
          (and
            (RightOf15 ?x)
            (LeftOf2 ?x)
          )
          (and
            (RightOf9 ?x)
            (LeftOf4 ?x)
          )
          (and
            (LeftOf3 ?x)
            (RightOf13 ?x)
          )
          (and
            (RightOf16 ?x)
            (LeftOf1 ?x)
          )
          (and
            (RightOf12 ?x)
            (LeftOf4 ?x)
          )
          (and
            (RightOf11 ?x)
            (LeftOf3 ?x)
          )
          (and
            (LeftOf1 ?x)
            (RightOf8 ?x)
          )
          (and
            (RightOf14 ?x)
            (LeftOf7 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf8 ?x)
          )
          (and
            (LeftOf7 ?x)
            (RightOf16 ?x)
          )
          (and
            (LeftOf4 ?x)
            (RightOf8 ?x)
          )
          (and
            (RightOf16 ?x)
            (LeftOf3 ?x)
          )
          (and
            (LeftOf1 ?x)
            (RightOf11 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf8 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf16 ?x)
          )
          (and
            (RightOf8 ?x)
            (LeftOf3 ?x)
          )
          (and
            (RightOf18 ?x)
            (LeftOf4 ?x)
          )
          (and
            (RightOf17 ?x)
            (LeftOf4 ?x)
          )
          (and
            (LeftOf1 ?x)
            (RightOf18 ?x)
          )
          (and
            (RightOf6 ?x)
            (LeftOf3 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf16 ?x)
          )
          (and
            (LeftOf4 ?x)
            (RightOf18 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf11 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf17 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf9 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf11 ?x)
          )
          (and
            (RightOf6 ?x)
            (LeftOf2 ?x)
          )
          (and
            (RightOf6 ?x)
            (LeftOf4 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf13 ?x)
          )
          (and
            (LeftOf1 ?x)
            (RightOf13 ?x)
          )
          (and
            (LeftOf4 ?x)
            (RightOf10 ?x)
          )
          (and
            (RightOf17 ?x)
            (LeftOf1 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf10 ?x)
          )
          (and
            (RightOf12 ?x)
            (LeftOf3 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf7 ?x)
          )
          (and
            (LeftOf7 ?x)
            (RightOf17 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf14 ?x)
          )
          (and
            (RightOf16 ?x)
            (LeftOf4 ?x)
          )
          (and
            (RightOf7 ?x)
            (LeftOf4 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf18 ?x)
          )
          (and
            (LeftOf7 ?x)
            (RightOf8 ?x)
          )
          (and
            (RightOf14 ?x)
            (LeftOf3 ?x)
          )
          (and
            (LeftOf7 ?x)
            (RightOf11 ?x)
          )
          (and
            (RightOf14 ?x)
            (LeftOf2 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf15 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf9 ?x)
          )
          (and
            (RightOf15 ?x)
            (LeftOf7 ?x)
          )
          (and
            (RightOf9 ?x)
            (LeftOf3 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf18 ?x)
          )
          (and
            (RightOf14 ?x)
            (LeftOf1 ?x)
          )
          (and
            (LeftOf4 ?x)
            (RightOf13 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf10 ?x)
          )
          (and
            (RightOf12 ?x)
            (LeftOf6 ?x)
          )
          (and
            (RightOf6 ?x)
            (LeftOf7 ?x)
          )
          (and
            (RightOf15 ?x)
            (LeftOf4 ?x)
          )
          (and
            (RightOf18 ?x)
            (LeftOf3 ?x)
          )
          (and
            (RightOf12 ?x)
            (LeftOf5 ?x)
          )
          (and
            (RightOf12 ?x)
            (LeftOf7 ?x)
          )
          (and
            (RightOf7 ?x)
            (LeftOf1 ?x)
          )
          (and
            (RightOf12 ?x)
            (LeftOf1 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf17 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf16 ?x)
          )
          (and
            (RightOf15 ?x)
            (LeftOf3 ?x)
          )
          (and
            (RightOf17 ?x)
            (LeftOf2 ?x)
          )
          (and
            (RightOf14 ?x)
            (LeftOf4 ?x)
          )
          (and
            (RightOf7 ?x)
            (LeftOf3 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf12 ?x)
          )
          (and
            (RightOf7 ?x)
            (LeftOf2 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf7 ?x)
          )
          (and
            (LeftOf7 ?x)
            (RightOf18 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf13 ?x)
          )
          (and
            (LeftOf1 ?x)
            (RightOf10 ?x)
          )
          (and
            (RightOf13 ?x)
            (LeftOf3 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf13 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf14 ?x)
          )
          (and
            (RightOf6 ?x)
            (LeftOf1 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf8 ?x)
          )
          (and
            (RightOf17 ?x)
            (LeftOf3 ?x)
          )
          (and
            (RightOf6 ?x)
            (LeftOf6 ?x)
          )
          (and
            (RightOf9 ?x)
            (LeftOf7 ?x)
          )
          (and
            (RightOf9 ?x)
            (LeftOf2 ?x)
          )
          (and
            (RightOf12 ?x)
            (LeftOf2 ?x)
          )
          (and
            (RightOf10 ?x)
            (LeftOf4 ?x)
          )
          (and
            (RightOf10 ?x)
            (LeftOf3 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf10 ?x)
          )
          (and
            (RightOf6 ?x)
            (LeftOf5 ?x)
          )
          (and
            (RightOf8 ?x)
            (LeftOf4 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf11 ?x)
          )
          (and
            (RightOf9 ?x)
            (LeftOf1 ?x)
          )
          (and
            (LeftOf7 ?x)
            (RightOf13 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf15 ?x)
          )
          (and
            (RightOf7 ?x)
            (LeftOf7 ?x)
          )
          (and
            (RightOf15 ?x)
            (LeftOf1 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf18 ?x)
          )
        )
        (and
          (Column5 ?x)
        )
      )
      (when
        (or
          (and
            (RightOf11 ?x)
            (LeftOf4 ?x)
          )
          (and
            (LeftOf7 ?x)
            (RightOf10 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf11 ?x)
          )
          (and
            (RightOf9 ?x)
            (LeftOf4 ?x)
          )
          (and
            (LeftOf3 ?x)
            (RightOf13 ?x)
          )
          (and
            (RightOf16 ?x)
            (LeftOf1 ?x)
          )
          (and
            (RightOf12 ?x)
            (LeftOf4 ?x)
          )
          (and
            (RightOf11 ?x)
            (LeftOf3 ?x)
          )
          (and
            (LeftOf1 ?x)
            (RightOf8 ?x)
          )
          (and
            (RightOf14 ?x)
            (LeftOf7 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf8 ?x)
          )
          (and
            (LeftOf7 ?x)
            (RightOf16 ?x)
          )
          (and
            (LeftOf4 ?x)
            (RightOf8 ?x)
          )
          (and
            (RightOf16 ?x)
            (LeftOf3 ?x)
          )
          (and
            (LeftOf8 ?x)
            (RightOf8 ?x)
          )
          (and
            (LeftOf1 ?x)
            (RightOf11 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf8 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf16 ?x)
          )
          (and
            (RightOf10 ?x)
            (LeftOf4 ?x)
          )
          (and
            (RightOf18 ?x)
            (LeftOf4 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf10 ?x)
          )
          (and
            (RightOf17 ?x)
            (LeftOf4 ?x)
          )
          (and
            (LeftOf1 ?x)
            (RightOf18 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf7 ?x)
          )
          (and
            (RightOf9 ?x)
            (LeftOf2 ?x)
          )
          (and
            (LeftOf4 ?x)
            (RightOf18 ?x)
          )
          (and
            (RightOf15 ?x)
            (LeftOf2 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf17 ?x)
          )
          (and
            (LeftOf8 ?x)
            (RightOf17 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf9 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf11 ?x)
          )
          (and
            (LeftOf8 ?x)
            (RightOf11 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf13 ?x)
          )
          (and
            (LeftOf1 ?x)
            (RightOf13 ?x)
          )
          (and
            (LeftOf4 ?x)
            (RightOf10 ?x)
          )
          (and
            (RightOf17 ?x)
            (LeftOf1 ?x)
          )
          (and
            (RightOf12 ?x)
            (LeftOf8 ?x)
          )
          (and
            (RightOf12 ?x)
            (LeftOf3 ?x)
          )
          (and
            (LeftOf7 ?x)
            (RightOf17 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf14 ?x)
          )
          (and
            (LeftOf8 ?x)
            (RightOf14 ?x)
          )
          (and
            (RightOf16 ?x)
            (LeftOf4 ?x)
          )
          (and
            (RightOf7 ?x)
            (LeftOf4 ?x)
          )
          (and
            (LeftOf8 ?x)
            (RightOf18 ?x)
          )
          (and
            (LeftOf8 ?x)
            (RightOf7 ?x)
          )
          (and
            (RightOf9 ?x)
            (LeftOf8 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf18 ?x)
          )
          (and
            (LeftOf7 ?x)
            (RightOf8 ?x)
          )
          (and
            (RightOf14 ?x)
            (LeftOf3 ?x)
          )
          (and
            (LeftOf7 ?x)
            (RightOf11 ?x)
          )
          (and
            (RightOf14 ?x)
            (LeftOf2 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf15 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf9 ?x)
          )
          (and
            (RightOf15 ?x)
            (LeftOf7 ?x)
          )
          (and
            (RightOf9 ?x)
            (LeftOf3 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf18 ?x)
          )
          (and
            (RightOf14 ?x)
            (LeftOf1 ?x)
          )
          (and
            (LeftOf4 ?x)
            (RightOf13 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf10 ?x)
          )
          (and
            (RightOf12 ?x)
            (LeftOf6 ?x)
          )
          (and
            (RightOf15 ?x)
            (LeftOf4 ?x)
          )
          (and
            (RightOf18 ?x)
            (LeftOf3 ?x)
          )
          (and
            (RightOf15 ?x)
            (LeftOf8 ?x)
          )
          (and
            (RightOf12 ?x)
            (LeftOf5 ?x)
          )
          (and
            (RightOf12 ?x)
            (LeftOf7 ?x)
          )
          (and
            (RightOf7 ?x)
            (LeftOf2 ?x)
          )
          (and
            (RightOf12 ?x)
            (LeftOf1 ?x)
          )
          (and
            (LeftOf7 ?x)
            (RightOf7 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf17 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf16 ?x)
          )
          (and
            (RightOf15 ?x)
            (LeftOf3 ?x)
          )
          (and
            (RightOf17 ?x)
            (LeftOf2 ?x)
          )
          (and
            (RightOf14 ?x)
            (LeftOf4 ?x)
          )
          (and
            (RightOf7 ?x)
            (LeftOf3 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf12 ?x)
          )
          (and
            (RightOf7 ?x)
            (LeftOf1 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf7 ?x)
          )
          (and
            (LeftOf7 ?x)
            (RightOf18 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf13 ?x)
          )
          (and
            (LeftOf8 ?x)
            (RightOf13 ?x)
          )
          (and
            (LeftOf1 ?x)
            (RightOf10 ?x)
          )
          (and
            (RightOf13 ?x)
            (LeftOf3 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf13 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf14 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf8 ?x)
          )
          (and
            (RightOf17 ?x)
            (LeftOf3 ?x)
          )
          (and
            (RightOf9 ?x)
            (LeftOf7 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf16 ?x)
          )
          (and
            (LeftOf8 ?x)
            (RightOf16 ?x)
          )
          (and
            (RightOf12 ?x)
            (LeftOf2 ?x)
          )
          (and
            (RightOf8 ?x)
            (LeftOf3 ?x)
          )
          (and
            (RightOf10 ?x)
            (LeftOf3 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf10 ?x)
          )
          (and
            (LeftOf8 ?x)
            (RightOf10 ?x)
          )
          (and
            (RightOf8 ?x)
            (LeftOf4 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf11 ?x)
          )
          (and
            (RightOf9 ?x)
            (LeftOf1 ?x)
          )
          (and
            (LeftOf7 ?x)
            (RightOf13 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf15 ?x)
          )
          (and
            (RightOf15 ?x)
            (LeftOf1 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf18 ?x)
          )
        )
        (and
          (Column6 ?x)
        )
      )
      (when
        (or
          (and
            (RightOf14 ?x)
            (LeftOf9 ?x)
          )
          (and
            (LeftOf7 ?x)
            (RightOf10 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf11 ?x)
          )
          (and
            (RightOf9 ?x)
            (LeftOf4 ?x)
          )
          (and
            (LeftOf3 ?x)
            (RightOf13 ?x)
          )
          (and
            (RightOf11 ?x)
            (LeftOf4 ?x)
          )
          (and
            (RightOf12 ?x)
            (LeftOf4 ?x)
          )
          (and
            (RightOf17 ?x)
            (LeftOf9 ?x)
          )
          (and
            (LeftOf1 ?x)
            (RightOf8 ?x)
          )
          (and
            (RightOf14 ?x)
            (LeftOf7 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf8 ?x)
          )
          (and
            (LeftOf7 ?x)
            (RightOf16 ?x)
          )
          (and
            (RightOf16 ?x)
            (LeftOf3 ?x)
          )
          (and
            (LeftOf8 ?x)
            (RightOf8 ?x)
          )
          (and
            (LeftOf1 ?x)
            (RightOf11 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf8 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf16 ?x)
          )
          (and
            (RightOf10 ?x)
            (LeftOf4 ?x)
          )
          (and
            (RightOf18 ?x)
            (LeftOf4 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf10 ?x)
          )
          (and
            (RightOf17 ?x)
            (LeftOf4 ?x)
          )
          (and
            (RightOf16 ?x)
            (LeftOf9 ?x)
          )
          (and
            (LeftOf1 ?x)
            (RightOf18 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf16 ?x)
          )
          (and
            (LeftOf4 ?x)
            (RightOf18 ?x)
          )
          (and
            (RightOf15 ?x)
            (LeftOf2 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf17 ?x)
          )
          (and
            (LeftOf8 ?x)
            (RightOf17 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf9 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf11 ?x)
          )
          (and
            (LeftOf8 ?x)
            (RightOf11 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf13 ?x)
          )
          (and
            (LeftOf1 ?x)
            (RightOf13 ?x)
          )
          (and
            (LeftOf9 ?x)
            (RightOf18 ?x)
          )
          (and
            (LeftOf4 ?x)
            (RightOf10 ?x)
          )
          (and
            (RightOf17 ?x)
            (LeftOf1 ?x)
          )
          (and
            (RightOf14 ?x)
            (LeftOf2 ?x)
          )
          (and
            (RightOf12 ?x)
            (LeftOf8 ?x)
          )
          (and
            (LeftOf9 ?x)
            (RightOf10 ?x)
          )
          (and
            (RightOf12 ?x)
            (LeftOf3 ?x)
          )
          (and
            (RightOf15 ?x)
            (LeftOf4 ?x)
          )
          (and
            (LeftOf7 ?x)
            (RightOf17 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf14 ?x)
          )
          (and
            (LeftOf8 ?x)
            (RightOf14 ?x)
          )
          (and
            (RightOf16 ?x)
            (LeftOf4 ?x)
          )
          (and
            (LeftOf8 ?x)
            (RightOf18 ?x)
          )
          (and
            (RightOf9 ?x)
            (LeftOf8 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf18 ?x)
          )
          (and
            (RightOf15 ?x)
            (LeftOf9 ?x)
          )
          (and
            (RightOf14 ?x)
            (LeftOf3 ?x)
          )
          (and
            (LeftOf7 ?x)
            (RightOf11 ?x)
          )
          (and
            (RightOf9 ?x)
            (LeftOf1 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf15 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf9 ?x)
          )
          (and
            (RightOf15 ?x)
            (LeftOf7 ?x)
          )
          (and
            (RightOf9 ?x)
            (LeftOf3 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf18 ?x)
          )
          (and
            (RightOf14 ?x)
            (LeftOf1 ?x)
          )
          (and
            (LeftOf4 ?x)
            (RightOf13 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf10 ?x)
          )
          (and
            (RightOf12 ?x)
            (LeftOf6 ?x)
          )
          (and
            (RightOf16 ?x)
            (LeftOf1 ?x)
          )
          (and
            (RightOf18 ?x)
            (LeftOf3 ?x)
          )
          (and
            (RightOf15 ?x)
            (LeftOf8 ?x)
          )
          (and
            (LeftOf9 ?x)
            (RightOf11 ?x)
          )
          (and
            (RightOf12 ?x)
            (LeftOf5 ?x)
          )
          (and
            (RightOf12 ?x)
            (LeftOf7 ?x)
          )
          (and
            (RightOf14 ?x)
            (LeftOf8 ?x)
          )
          (and
            (RightOf12 ?x)
            (LeftOf1 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf17 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf16 ?x)
          )
          (and
            (LeftOf8 ?x)
            (RightOf16 ?x)
          )
          (and
            (RightOf17 ?x)
            (LeftOf2 ?x)
          )
          (and
            (RightOf11 ?x)
            (LeftOf3 ?x)
          )
          (and
            (RightOf14 ?x)
            (LeftOf4 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf12 ?x)
          )
          (and
            (RightOf9 ?x)
            (LeftOf9 ?x)
          )
          (and
            (LeftOf7 ?x)
            (RightOf18 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf13 ?x)
          )
          (and
            (LeftOf8 ?x)
            (RightOf13 ?x)
          )
          (and
            (LeftOf1 ?x)
            (RightOf10 ?x)
          )
          (and
            (RightOf13 ?x)
            (LeftOf3 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf13 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf14 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf8 ?x)
          )
          (and
            (LeftOf7 ?x)
            (RightOf8 ?x)
          )
          (and
            (RightOf17 ?x)
            (LeftOf3 ?x)
          )
          (and
            (RightOf9 ?x)
            (LeftOf7 ?x)
          )
          (and
            (RightOf9 ?x)
            (LeftOf2 ?x)
          )
          (and
            (RightOf15 ?x)
            (LeftOf3 ?x)
          )
          (and
            (RightOf12 ?x)
            (LeftOf9 ?x)
          )
          (and
            (RightOf12 ?x)
            (LeftOf2 ?x)
          )
          (and
            (RightOf8 ?x)
            (LeftOf3 ?x)
          )
          (and
            (RightOf10 ?x)
            (LeftOf3 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf10 ?x)
          )
          (and
            (LeftOf8 ?x)
            (RightOf10 ?x)
          )
          (and
            (RightOf8 ?x)
            (LeftOf4 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf11 ?x)
          )
          (and
            (LeftOf7 ?x)
            (RightOf13 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf15 ?x)
          )
          (and
            (LeftOf9 ?x)
            (RightOf8 ?x)
          )
          (and
            (LeftOf9 ?x)
            (RightOf13 ?x)
          )
          (and
            (RightOf15 ?x)
            (LeftOf1 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf18 ?x)
          )
        )
        (and
          (Column7 ?x)
        )
      )
      (when
        (or
          (and
            (RightOf14 ?x)
            (LeftOf9 ?x)
          )
          (and
            (LeftOf7 ?x)
            (RightOf10 ?x)
          )
          (and
            (RightOf15 ?x)
            (LeftOf10 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf11 ?x)
          )
          (and
            (RightOf9 ?x)
            (LeftOf4 ?x)
          )
          (and
            (LeftOf3 ?x)
            (RightOf13 ?x)
          )
          (and
            (RightOf11 ?x)
            (LeftOf4 ?x)
          )
          (and
            (RightOf12 ?x)
            (LeftOf4 ?x)
          )
          (and
            (RightOf17 ?x)
            (LeftOf9 ?x)
          )
          (and
            (RightOf14 ?x)
            (LeftOf2 ?x)
          )
          (and
            (LeftOf10 ?x)
            (RightOf16 ?x)
          )
          (and
            (RightOf14 ?x)
            (LeftOf7 ?x)
          )
          (and
            (LeftOf7 ?x)
            (RightOf16 ?x)
          )
          (and
            (RightOf16 ?x)
            (LeftOf3 ?x)
          )
          (and
            (LeftOf1 ?x)
            (RightOf11 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf16 ?x)
          )
          (and
            (RightOf18 ?x)
            (LeftOf4 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf10 ?x)
          )
          (and
            (RightOf17 ?x)
            (LeftOf4 ?x)
          )
          (and
            (RightOf16 ?x)
            (LeftOf9 ?x)
          )
          (and
            (LeftOf10 ?x)
            (RightOf10 ?x)
          )
          (and
            (LeftOf10 ?x)
            (RightOf11 ?x)
          )
          (and
            (LeftOf1 ?x)
            (RightOf18 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf16 ?x)
          )
          (and
            (LeftOf7 ?x)
            (RightOf11 ?x)
          )
          (and
            (LeftOf4 ?x)
            (RightOf18 ?x)
          )
          (and
            (RightOf15 ?x)
            (LeftOf2 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf17 ?x)
          )
          (and
            (LeftOf8 ?x)
            (RightOf17 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf9 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf11 ?x)
          )
          (and
            (LeftOf8 ?x)
            (RightOf11 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf13 ?x)
          )
          (and
            (LeftOf1 ?x)
            (RightOf13 ?x)
          )
          (and
            (LeftOf9 ?x)
            (RightOf18 ?x)
          )
          (and
            (LeftOf4 ?x)
            (RightOf10 ?x)
          )
          (and
            (RightOf17 ?x)
            (LeftOf1 ?x)
          )
          (and
            (RightOf9 ?x)
            (LeftOf10 ?x)
          )
          (and
            (RightOf12 ?x)
            (LeftOf8 ?x)
          )
          (and
            (LeftOf9 ?x)
            (RightOf10 ?x)
          )
          (and
            (RightOf12 ?x)
            (LeftOf3 ?x)
          )
          (and
            (RightOf15 ?x)
            (LeftOf4 ?x)
          )
          (and
            (LeftOf7 ?x)
            (RightOf17 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf14 ?x)
          )
          (and
            (LeftOf8 ?x)
            (RightOf14 ?x)
          )
          (and
            (RightOf16 ?x)
            (LeftOf4 ?x)
          )
          (and
            (LeftOf8 ?x)
            (RightOf18 ?x)
          )
          (and
            (LeftOf10 ?x)
            (RightOf13 ?x)
          )
          (and
            (RightOf9 ?x)
            (LeftOf8 ?x)
          )
          (and
            (RightOf11 ?x)
            (LeftOf3 ?x)
          )
          (and
            (RightOf15 ?x)
            (LeftOf9 ?x)
          )
          (and
            (RightOf14 ?x)
            (LeftOf3 ?x)
          )
          (and
            (LeftOf10 ?x)
            (RightOf14 ?x)
          )
          (and
            (RightOf9 ?x)
            (LeftOf1 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf15 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf9 ?x)
          )
          (and
            (RightOf15 ?x)
            (LeftOf7 ?x)
          )
          (and
            (RightOf9 ?x)
            (LeftOf3 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf18 ?x)
          )
          (and
            (RightOf14 ?x)
            (LeftOf1 ?x)
          )
          (and
            (LeftOf4 ?x)
            (RightOf13 ?x)
          )
          (and
            (LeftOf10 ?x)
            (RightOf17 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf10 ?x)
          )
          (and
            (RightOf12 ?x)
            (LeftOf6 ?x)
          )
          (and
            (LeftOf10 ?x)
            (RightOf18 ?x)
          )
          (and
            (RightOf16 ?x)
            (LeftOf1 ?x)
          )
          (and
            (RightOf18 ?x)
            (LeftOf3 ?x)
          )
          (and
            (RightOf15 ?x)
            (LeftOf8 ?x)
          )
          (and
            (LeftOf9 ?x)
            (RightOf11 ?x)
          )
          (and
            (RightOf12 ?x)
            (LeftOf5 ?x)
          )
          (and
            (RightOf12 ?x)
            (LeftOf7 ?x)
          )
          (and
            (RightOf14 ?x)
            (LeftOf8 ?x)
          )
          (and
            (RightOf12 ?x)
            (LeftOf1 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf17 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf16 ?x)
          )
          (and
            (LeftOf8 ?x)
            (RightOf16 ?x)
          )
          (and
            (RightOf17 ?x)
            (LeftOf2 ?x)
          )
          (and
            (RightOf14 ?x)
            (LeftOf4 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf12 ?x)
          )
          (and
            (RightOf9 ?x)
            (LeftOf9 ?x)
          )
          (and
            (LeftOf7 ?x)
            (RightOf18 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf13 ?x)
          )
          (and
            (LeftOf8 ?x)
            (RightOf13 ?x)
          )
          (and
            (LeftOf1 ?x)
            (RightOf10 ?x)
          )
          (and
            (RightOf13 ?x)
            (LeftOf3 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf13 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf14 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf18 ?x)
          )
          (and
            (RightOf17 ?x)
            (LeftOf3 ?x)
          )
          (and
            (RightOf9 ?x)
            (LeftOf7 ?x)
          )
          (and
            (RightOf9 ?x)
            (LeftOf2 ?x)
          )
          (and
            (RightOf15 ?x)
            (LeftOf3 ?x)
          )
          (and
            (RightOf12 ?x)
            (LeftOf9 ?x)
          )
          (and
            (RightOf12 ?x)
            (LeftOf2 ?x)
          )
          (and
            (RightOf10 ?x)
            (LeftOf4 ?x)
          )
          (and
            (RightOf10 ?x)
            (LeftOf3 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf10 ?x)
          )
          (and
            (LeftOf8 ?x)
            (RightOf10 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf11 ?x)
          )
          (and
            (LeftOf7 ?x)
            (RightOf13 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf15 ?x)
          )
          (and
            (LeftOf9 ?x)
            (RightOf13 ?x)
          )
          (and
            (RightOf12 ?x)
            (LeftOf10 ?x)
          )
          (and
            (RightOf15 ?x)
            (LeftOf1 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf18 ?x)
          )
        )
        (and
          (Column8 ?x)
        )
      )
      (when
        (or
          (and
            (RightOf14 ?x)
            (LeftOf9 ?x)
          )
          (and
            (LeftOf11 ?x)
            (RightOf10 ?x)
          )
          (and
            (RightOf17 ?x)
            (LeftOf11 ?x)
          )
          (and
            (LeftOf7 ?x)
            (RightOf10 ?x)
          )
          (and
            (RightOf15 ?x)
            (LeftOf10 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf11 ?x)
          )
          (and
            (LeftOf3 ?x)
            (RightOf13 ?x)
          )
          (and
            (RightOf11 ?x)
            (LeftOf4 ?x)
          )
          (and
            (RightOf12 ?x)
            (LeftOf4 ?x)
          )
          (and
            (RightOf17 ?x)
            (LeftOf9 ?x)
          )
          (and
            (RightOf14 ?x)
            (LeftOf2 ?x)
          )
          (and
            (LeftOf10 ?x)
            (RightOf16 ?x)
          )
          (and
            (LeftOf10 ?x)
            (RightOf18 ?x)
          )
          (and
            (LeftOf7 ?x)
            (RightOf16 ?x)
          )
          (and
            (RightOf16 ?x)
            (LeftOf3 ?x)
          )
          (and
            (LeftOf1 ?x)
            (RightOf11 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf16 ?x)
          )
          (and
            (LeftOf11 ?x)
            (RightOf18 ?x)
          )
          (and
            (RightOf18 ?x)
            (LeftOf4 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf10 ?x)
          )
          (and
            (RightOf17 ?x)
            (LeftOf4 ?x)
          )
          (and
            (RightOf16 ?x)
            (LeftOf9 ?x)
          )
          (and
            (LeftOf10 ?x)
            (RightOf10 ?x)
          )
          (and
            (LeftOf10 ?x)
            (RightOf11 ?x)
          )
          (and
            (LeftOf1 ?x)
            (RightOf18 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf16 ?x)
          )
          (and
            (LeftOf4 ?x)
            (RightOf18 ?x)
          )
          (and
            (RightOf15 ?x)
            (LeftOf2 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf17 ?x)
          )
          (and
            (LeftOf8 ?x)
            (RightOf17 ?x)
          )
          (and
            (RightOf12 ?x)
            (LeftOf11 ?x)
          )
          (and
            (LeftOf8 ?x)
            (RightOf11 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf13 ?x)
          )
          (and
            (LeftOf1 ?x)
            (RightOf13 ?x)
          )
          (and
            (LeftOf9 ?x)
            (RightOf18 ?x)
          )
          (and
            (LeftOf7 ?x)
            (RightOf11 ?x)
          )
          (and
            (RightOf17 ?x)
            (LeftOf1 ?x)
          )
          (and
            (RightOf12 ?x)
            (LeftOf8 ?x)
          )
          (and
            (LeftOf9 ?x)
            (RightOf10 ?x)
          )
          (and
            (RightOf12 ?x)
            (LeftOf3 ?x)
          )
          (and
            (RightOf15 ?x)
            (LeftOf4 ?x)
          )
          (and
            (LeftOf7 ?x)
            (RightOf17 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf14 ?x)
          )
          (and
            (LeftOf8 ?x)
            (RightOf14 ?x)
          )
          (and
            (RightOf16 ?x)
            (LeftOf4 ?x)
          )
          (and
            (LeftOf8 ?x)
            (RightOf18 ?x)
          )
          (and
            (LeftOf10 ?x)
            (RightOf13 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf18 ?x)
          )
          (and
            (RightOf15 ?x)
            (LeftOf9 ?x)
          )
          (and
            (RightOf14 ?x)
            (LeftOf3 ?x)
          )
          (and
            (LeftOf10 ?x)
            (RightOf14 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf15 ?x)
          )
          (and
            (RightOf14 ?x)
            (LeftOf11 ?x)
          )
          (and
            (RightOf15 ?x)
            (LeftOf7 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf18 ?x)
          )
          (and
            (RightOf14 ?x)
            (LeftOf1 ?x)
          )
          (and
            (LeftOf4 ?x)
            (RightOf13 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf11 ?x)
          )
          (and
            (LeftOf10 ?x)
            (RightOf17 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf10 ?x)
          )
          (and
            (RightOf12 ?x)
            (LeftOf6 ?x)
          )
          (and
            (RightOf14 ?x)
            (LeftOf7 ?x)
          )
          (and
            (RightOf16 ?x)
            (LeftOf1 ?x)
          )
          (and
            (RightOf18 ?x)
            (LeftOf3 ?x)
          )
          (and
            (RightOf15 ?x)
            (LeftOf8 ?x)
          )
          (and
            (LeftOf11 ?x)
            (RightOf13 ?x)
          )
          (and
            (LeftOf9 ?x)
            (RightOf11 ?x)
          )
          (and
            (RightOf12 ?x)
            (LeftOf5 ?x)
          )
          (and
            (RightOf12 ?x)
            (LeftOf7 ?x)
          )
          (and
            (RightOf14 ?x)
            (LeftOf8 ?x)
          )
          (and
            (RightOf12 ?x)
            (LeftOf1 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf17 ?x)
          )
          (and
            (RightOf14 ?x)
            (LeftOf10 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf16 ?x)
          )
          (and
            (LeftOf8 ?x)
            (RightOf16 ?x)
          )
          (and
            (RightOf17 ?x)
            (LeftOf2 ?x)
          )
          (and
            (RightOf11 ?x)
            (LeftOf3 ?x)
          )
          (and
            (RightOf14 ?x)
            (LeftOf4 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf12 ?x)
          )
          (and
            (LeftOf7 ?x)
            (RightOf18 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf13 ?x)
          )
          (and
            (LeftOf8 ?x)
            (RightOf13 ?x)
          )
          (and
            (LeftOf1 ?x)
            (RightOf10 ?x)
          )
          (and
            (RightOf13 ?x)
            (LeftOf3 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf13 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf14 ?x)
          )
          (and
            (RightOf17 ?x)
            (LeftOf3 ?x)
          )
          (and
            (RightOf15 ?x)
            (LeftOf3 ?x)
          )
          (and
            (RightOf12 ?x)
            (LeftOf9 ?x)
          )
          (and
            (LeftOf11 ?x)
            (RightOf11 ?x)
          )
          (and
            (RightOf12 ?x)
            (LeftOf2 ?x)
          )
          (and
            (RightOf10 ?x)
            (LeftOf4 ?x)
          )
          (and
            (RightOf10 ?x)
            (LeftOf3 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf10 ?x)
          )
          (and
            (LeftOf8 ?x)
            (RightOf10 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf11 ?x)
          )
          (and
            (LeftOf7 ?x)
            (RightOf13 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf15 ?x)
          )
          (and
            (RightOf16 ?x)
            (LeftOf11 ?x)
          )
          (and
            (LeftOf9 ?x)
            (RightOf13 ?x)
          )
          (and
            (RightOf12 ?x)
            (LeftOf10 ?x)
          )
          (and
            (RightOf15 ?x)
            (LeftOf11 ?x)
          )
          (and
            (RightOf15 ?x)
            (LeftOf1 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf18 ?x)
          )
        )
        (and
          (Column9 ?x)
        )
      )
      (when
        (or
          (and
            (RightOf14 ?x)
            (LeftOf9 ?x)
          )
          (and
            (RightOf17 ?x)
            (LeftOf11 ?x)
          )
          (and
            (RightOf15 ?x)
            (LeftOf10 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf11 ?x)
          )
          (and
            (LeftOf12 ?x)
            (RightOf13 ?x)
          )
          (and
            (LeftOf3 ?x)
            (RightOf13 ?x)
          )
          (and
            (RightOf11 ?x)
            (LeftOf4 ?x)
          )
          (and
            (RightOf12 ?x)
            (LeftOf4 ?x)
          )
          (and
            (RightOf17 ?x)
            (LeftOf9 ?x)
          )
          (and
            (RightOf14 ?x)
            (LeftOf2 ?x)
          )
          (and
            (LeftOf10 ?x)
            (RightOf16 ?x)
          )
          (and
            (LeftOf10 ?x)
            (RightOf18 ?x)
          )
          (and
            (LeftOf7 ?x)
            (RightOf16 ?x)
          )
          (and
            (RightOf16 ?x)
            (LeftOf3 ?x)
          )
          (and
            (LeftOf12 ?x)
            (RightOf15 ?x)
          )
          (and
            (LeftOf1 ?x)
            (RightOf11 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf16 ?x)
          )
          (and
            (LeftOf11 ?x)
            (RightOf18 ?x)
          )
          (and
            (RightOf17 ?x)
            (LeftOf4 ?x)
          )
          (and
            (RightOf16 ?x)
            (LeftOf9 ?x)
          )
          (and
            (LeftOf10 ?x)
            (RightOf11 ?x)
          )
          (and
            (LeftOf1 ?x)
            (RightOf18 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf16 ?x)
          )
          (and
            (LeftOf4 ?x)
            (RightOf18 ?x)
          )
          (and
            (RightOf15 ?x)
            (LeftOf2 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf17 ?x)
          )
          (and
            (LeftOf8 ?x)
            (RightOf17 ?x)
          )
          (and
            (RightOf12 ?x)
            (LeftOf11 ?x)
          )
          (and
            (LeftOf8 ?x)
            (RightOf11 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf13 ?x)
          )
          (and
            (LeftOf1 ?x)
            (RightOf13 ?x)
          )
          (and
            (LeftOf9 ?x)
            (RightOf18 ?x)
          )
          (and
            (RightOf17 ?x)
            (LeftOf1 ?x)
          )
          (and
            (RightOf12 ?x)
            (LeftOf8 ?x)
          )
          (and
            (LeftOf12 ?x)
            (RightOf17 ?x)
          )
          (and
            (RightOf12 ?x)
            (LeftOf3 ?x)
          )
          (and
            (RightOf15 ?x)
            (LeftOf4 ?x)
          )
          (and
            (LeftOf7 ?x)
            (RightOf17 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf14 ?x)
          )
          (and
            (LeftOf8 ?x)
            (RightOf14 ?x)
          )
          (and
            (RightOf16 ?x)
            (LeftOf4 ?x)
          )
          (and
            (LeftOf12 ?x)
            (RightOf16 ?x)
          )
          (and
            (LeftOf8 ?x)
            (RightOf18 ?x)
          )
          (and
            (LeftOf10 ?x)
            (RightOf13 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf18 ?x)
          )
          (and
            (LeftOf12 ?x)
            (RightOf12 ?x)
          )
          (and
            (RightOf15 ?x)
            (LeftOf9 ?x)
          )
          (and
            (RightOf14 ?x)
            (LeftOf3 ?x)
          )
          (and
            (LeftOf7 ?x)
            (RightOf11 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf15 ?x)
          )
          (and
            (RightOf14 ?x)
            (LeftOf11 ?x)
          )
          (and
            (RightOf15 ?x)
            (LeftOf7 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf18 ?x)
          )
          (and
            (RightOf14 ?x)
            (LeftOf1 ?x)
          )
          (and
            (LeftOf4 ?x)
            (RightOf13 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf11 ?x)
          )
          (and
            (LeftOf10 ?x)
            (RightOf17 ?x)
          )
          (and
            (RightOf12 ?x)
            (LeftOf6 ?x)
          )
          (and
            (RightOf14 ?x)
            (LeftOf7 ?x)
          )
          (and
            (RightOf16 ?x)
            (LeftOf1 ?x)
          )
          (and
            (RightOf18 ?x)
            (LeftOf3 ?x)
          )
          (and
            (RightOf15 ?x)
            (LeftOf8 ?x)
          )
          (and
            (LeftOf11 ?x)
            (RightOf13 ?x)
          )
          (and
            (LeftOf9 ?x)
            (RightOf11 ?x)
          )
          (and
            (RightOf12 ?x)
            (LeftOf5 ?x)
          )
          (and
            (RightOf12 ?x)
            (LeftOf7 ?x)
          )
          (and
            (RightOf14 ?x)
            (LeftOf8 ?x)
          )
          (and
            (RightOf12 ?x)
            (LeftOf1 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf17 ?x)
          )
          (and
            (RightOf14 ?x)
            (LeftOf10 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf16 ?x)
          )
          (and
            (RightOf15 ?x)
            (LeftOf3 ?x)
          )
          (and
            (RightOf17 ?x)
            (LeftOf2 ?x)
          )
          (and
            (RightOf11 ?x)
            (LeftOf3 ?x)
          )
          (and
            (RightOf14 ?x)
            (LeftOf4 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf12 ?x)
          )
          (and
            (LeftOf7 ?x)
            (RightOf18 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf13 ?x)
          )
          (and
            (LeftOf8 ?x)
            (RightOf13 ?x)
          )
          (and
            (RightOf13 ?x)
            (LeftOf3 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf13 ?x)
          )
          (and
            (LeftOf12 ?x)
            (RightOf18 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf14 ?x)
          )
          (and
            (RightOf17 ?x)
            (LeftOf3 ?x)
          )
          (and
            (LeftOf8 ?x)
            (RightOf16 ?x)
          )
          (and
            (RightOf12 ?x)
            (LeftOf9 ?x)
          )
          (and
            (LeftOf11 ?x)
            (RightOf11 ?x)
          )
          (and
            (RightOf12 ?x)
            (LeftOf2 ?x)
          )
          (and
            (LeftOf10 ?x)
            (RightOf14 ?x)
          )
          (and
            (RightOf18 ?x)
            (LeftOf4 ?x)
          )
          (and
            (LeftOf12 ?x)
            (RightOf11 ?x)
          )
          (and
            (LeftOf12 ?x)
            (RightOf14 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf11 ?x)
          )
          (and
            (LeftOf7 ?x)
            (RightOf13 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf15 ?x)
          )
          (and
            (RightOf16 ?x)
            (LeftOf11 ?x)
          )
          (and
            (LeftOf9 ?x)
            (RightOf13 ?x)
          )
          (and
            (RightOf12 ?x)
            (LeftOf10 ?x)
          )
          (and
            (RightOf15 ?x)
            (LeftOf11 ?x)
          )
          (and
            (RightOf15 ?x)
            (LeftOf1 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf18 ?x)
          )
        )
        (and
          (Column10 ?x)
        )
      )
      (when
        (or
          (and
            (RightOf14 ?x)
            (LeftOf9 ?x)
          )
          (and
            (LeftOf13 ?x)
            (RightOf16 ?x)
          )
          (and
            (RightOf17 ?x)
            (LeftOf11 ?x)
          )
          (and
            (RightOf15 ?x)
            (LeftOf10 ?x)
          )
          (and
            (RightOf15 ?x)
            (LeftOf2 ?x)
          )
          (and
            (LeftOf12 ?x)
            (RightOf13 ?x)
          )
          (and
            (LeftOf3 ?x)
            (RightOf13 ?x)
          )
          (and
            (RightOf16 ?x)
            (LeftOf1 ?x)
          )
          (and
            (RightOf12 ?x)
            (LeftOf4 ?x)
          )
          (and
            (RightOf17 ?x)
            (LeftOf9 ?x)
          )
          (and
            (RightOf14 ?x)
            (LeftOf2 ?x)
          )
          (and
            (LeftOf10 ?x)
            (RightOf16 ?x)
          )
          (and
            (RightOf14 ?x)
            (LeftOf7 ?x)
          )
          (and
            (LeftOf7 ?x)
            (RightOf16 ?x)
          )
          (and
            (RightOf12 ?x)
            (LeftOf13 ?x)
          )
          (and
            (RightOf16 ?x)
            (LeftOf3 ?x)
          )
          (and
            (LeftOf12 ?x)
            (RightOf15 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf16 ?x)
          )
          (and
            (LeftOf11 ?x)
            (RightOf18 ?x)
          )
          (and
            (RightOf17 ?x)
            (LeftOf4 ?x)
          )
          (and
            (RightOf16 ?x)
            (LeftOf9 ?x)
          )
          (and
            (LeftOf1 ?x)
            (RightOf18 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf16 ?x)
          )
          (and
            (LeftOf4 ?x)
            (RightOf18 ?x)
          )
          (and
            (LeftOf13 ?x)
            (RightOf18 ?x)
          )
          (and
            (LeftOf8 ?x)
            (RightOf17 ?x)
          )
          (and
            (RightOf12 ?x)
            (LeftOf11 ?x)
          )
          (and
            (RightOf12 ?x)
            (LeftOf12 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf13 ?x)
          )
          (and
            (LeftOf1 ?x)
            (RightOf13 ?x)
          )
          (and
            (LeftOf9 ?x)
            (RightOf18 ?x)
          )
          (and
            (RightOf17 ?x)
            (LeftOf1 ?x)
          )
          (and
            (RightOf12 ?x)
            (LeftOf8 ?x)
          )
          (and
            (LeftOf12 ?x)
            (RightOf17 ?x)
          )
          (and
            (RightOf12 ?x)
            (LeftOf3 ?x)
          )
          (and
            (LeftOf7 ?x)
            (RightOf17 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf14 ?x)
          )
          (and
            (LeftOf8 ?x)
            (RightOf14 ?x)
          )
          (and
            (RightOf16 ?x)
            (LeftOf4 ?x)
          )
          (and
            (LeftOf12 ?x)
            (RightOf16 ?x)
          )
          (and
            (LeftOf8 ?x)
            (RightOf18 ?x)
          )
          (and
            (LeftOf10 ?x)
            (RightOf13 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf18 ?x)
          )
          (and
            (RightOf15 ?x)
            (LeftOf9 ?x)
          )
          (and
            (RightOf14 ?x)
            (LeftOf3 ?x)
          )
          (and
            (LeftOf10 ?x)
            (RightOf14 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf15 ?x)
          )
          (and
            (RightOf14 ?x)
            (LeftOf11 ?x)
          )
          (and
            (RightOf15 ?x)
            (LeftOf7 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf17 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf18 ?x)
          )
          (and
            (RightOf14 ?x)
            (LeftOf1 ?x)
          )
          (and
            (LeftOf4 ?x)
            (RightOf13 ?x)
          )
          (and
            (LeftOf10 ?x)
            (RightOf17 ?x)
          )
          (and
            (RightOf12 ?x)
            (LeftOf6 ?x)
          )
          (and
            (RightOf14 ?x)
            (LeftOf13 ?x)
          )
          (and
            (RightOf15 ?x)
            (LeftOf4 ?x)
          )
          (and
            (RightOf18 ?x)
            (LeftOf3 ?x)
          )
          (and
            (LeftOf9 ?x)
            (RightOf13 ?x)
          )
          (and
            (RightOf15 ?x)
            (LeftOf8 ?x)
          )
          (and
            (LeftOf11 ?x)
            (RightOf13 ?x)
          )
          (and
            (RightOf12 ?x)
            (LeftOf5 ?x)
          )
          (and
            (RightOf12 ?x)
            (LeftOf7 ?x)
          )
          (and
            (RightOf14 ?x)
            (LeftOf8 ?x)
          )
          (and
            (RightOf12 ?x)
            (LeftOf1 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf17 ?x)
          )
          (and
            (RightOf14 ?x)
            (LeftOf10 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf16 ?x)
          )
          (and
            (LeftOf8 ?x)
            (RightOf16 ?x)
          )
          (and
            (RightOf17 ?x)
            (LeftOf2 ?x)
          )
          (and
            (RightOf14 ?x)
            (LeftOf4 ?x)
          )
          (and
            (LeftOf7 ?x)
            (RightOf18 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf13 ?x)
          )
          (and
            (LeftOf8 ?x)
            (RightOf13 ?x)
          )
          (and
            (RightOf15 ?x)
            (LeftOf13 ?x)
          )
          (and
            (RightOf13 ?x)
            (LeftOf3 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf13 ?x)
          )
          (and
            (LeftOf12 ?x)
            (RightOf18 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf14 ?x)
          )
          (and
            (LeftOf13 ?x)
            (RightOf13 ?x)
          )
          (and
            (RightOf17 ?x)
            (LeftOf3 ?x)
          )
          (and
            (LeftOf10 ?x)
            (RightOf18 ?x)
          )
          (and
            (RightOf15 ?x)
            (LeftOf3 ?x)
          )
          (and
            (RightOf12 ?x)
            (LeftOf9 ?x)
          )
          (and
            (RightOf12 ?x)
            (LeftOf2 ?x)
          )
          (and
            (RightOf18 ?x)
            (LeftOf4 ?x)
          )
          (and
            (LeftOf12 ?x)
            (RightOf14 ?x)
          )
          (and
            (LeftOf7 ?x)
            (RightOf13 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf15 ?x)
          )
          (and
            (RightOf16 ?x)
            (LeftOf11 ?x)
          )
          (and
            (LeftOf13 ?x)
            (RightOf17 ?x)
          )
          (and
            (RightOf12 ?x)
            (LeftOf10 ?x)
          )
          (and
            (RightOf15 ?x)
            (LeftOf11 ?x)
          )
          (and
            (RightOf15 ?x)
            (LeftOf1 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf18 ?x)
          )
        )
        (and
          (Column11 ?x)
        )
      )
      (when
        (or
          (and
            (RightOf14 ?x)
            (LeftOf9 ?x)
          )
          (and
            (LeftOf13 ?x)
            (RightOf16 ?x)
          )
          (and
            (RightOf17 ?x)
            (LeftOf11 ?x)
          )
          (and
            (RightOf15 ?x)
            (LeftOf10 ?x)
          )
          (and
            (RightOf15 ?x)
            (LeftOf2 ?x)
          )
          (and
            (LeftOf12 ?x)
            (RightOf13 ?x)
          )
          (and
            (RightOf16 ?x)
            (LeftOf1 ?x)
          )
          (and
            (RightOf17 ?x)
            (LeftOf9 ?x)
          )
          (and
            (RightOf14 ?x)
            (LeftOf2 ?x)
          )
          (and
            (LeftOf10 ?x)
            (RightOf16 ?x)
          )
          (and
            (RightOf14 ?x)
            (LeftOf7 ?x)
          )
          (and
            (LeftOf7 ?x)
            (RightOf16 ?x)
          )
          (and
            (RightOf16 ?x)
            (LeftOf3 ?x)
          )
          (and
            (LeftOf12 ?x)
            (RightOf15 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf16 ?x)
          )
          (and
            (RightOf14 ?x)
            (LeftOf13 ?x)
          )
          (and
            (LeftOf11 ?x)
            (RightOf18 ?x)
          )
          (and
            (RightOf17 ?x)
            (LeftOf4 ?x)
          )
          (and
            (RightOf16 ?x)
            (LeftOf9 ?x)
          )
          (and
            (LeftOf1 ?x)
            (RightOf18 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf16 ?x)
          )
          (and
            (LeftOf4 ?x)
            (RightOf18 ?x)
          )
          (and
            (LeftOf13 ?x)
            (RightOf18 ?x)
          )
          (and
            (LeftOf8 ?x)
            (RightOf17 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf13 ?x)
          )
          (and
            (LeftOf1 ?x)
            (RightOf13 ?x)
          )
          (and
            (RightOf15 ?x)
            (LeftOf14 ?x)
          )
          (and
            (LeftOf9 ?x)
            (RightOf18 ?x)
          )
          (and
            (RightOf17 ?x)
            (LeftOf1 ?x)
          )
          (and
            (LeftOf14 ?x)
            (RightOf18 ?x)
          )
          (and
            (RightOf16 ?x)
            (LeftOf14 ?x)
          )
          (and
            (LeftOf12 ?x)
            (RightOf17 ?x)
          )
          (and
            (LeftOf7 ?x)
            (RightOf17 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf14 ?x)
          )
          (and
            (LeftOf8 ?x)
            (RightOf14 ?x)
          )
          (and
            (RightOf16 ?x)
            (LeftOf4 ?x)
          )
          (and
            (LeftOf12 ?x)
            (RightOf16 ?x)
          )
          (and
            (LeftOf8 ?x)
            (RightOf18 ?x)
          )
          (and
            (LeftOf10 ?x)
            (RightOf13 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf18 ?x)
          )
          (and
            (RightOf15 ?x)
            (LeftOf9 ?x)
          )
          (and
            (RightOf14 ?x)
            (LeftOf3 ?x)
          )
          (and
            (LeftOf10 ?x)
            (RightOf14 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf15 ?x)
          )
          (and
            (RightOf14 ?x)
            (LeftOf11 ?x)
          )
          (and
            (RightOf15 ?x)
            (LeftOf7 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf17 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf18 ?x)
          )
          (and
            (RightOf14 ?x)
            (LeftOf1 ?x)
          )
          (and
            (LeftOf4 ?x)
            (RightOf13 ?x)
          )
          (and
            (RightOf17 ?x)
            (LeftOf14 ?x)
          )
          (and
            (LeftOf10 ?x)
            (RightOf17 ?x)
          )
          (and
            (LeftOf10 ?x)
            (RightOf18 ?x)
          )
          (and
            (RightOf15 ?x)
            (LeftOf4 ?x)
          )
          (and
            (RightOf18 ?x)
            (LeftOf3 ?x)
          )
          (and
            (RightOf15 ?x)
            (LeftOf8 ?x)
          )
          (and
            (LeftOf11 ?x)
            (RightOf13 ?x)
          )
          (and
            (LeftOf9 ?x)
            (RightOf13 ?x)
          )
          (and
            (RightOf14 ?x)
            (LeftOf8 ?x)
          )
          (and
            (LeftOf14 ?x)
            (RightOf13 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf17 ?x)
          )
          (and
            (RightOf14 ?x)
            (LeftOf10 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf16 ?x)
          )
          (and
            (RightOf15 ?x)
            (LeftOf3 ?x)
          )
          (and
            (RightOf17 ?x)
            (LeftOf2 ?x)
          )
          (and
            (RightOf14 ?x)
            (LeftOf4 ?x)
          )
          (and
            (LeftOf7 ?x)
            (RightOf18 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf13 ?x)
          )
          (and
            (LeftOf8 ?x)
            (RightOf13 ?x)
          )
          (and
            (RightOf15 ?x)
            (LeftOf13 ?x)
          )
          (and
            (RightOf13 ?x)
            (LeftOf3 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf13 ?x)
          )
          (and
            (LeftOf12 ?x)
            (RightOf18 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf14 ?x)
          )
          (and
            (LeftOf13 ?x)
            (RightOf13 ?x)
          )
          (and
            (RightOf17 ?x)
            (LeftOf3 ?x)
          )
          (and
            (LeftOf8 ?x)
            (RightOf16 ?x)
          )
          (and
            (RightOf18 ?x)
            (LeftOf4 ?x)
          )
          (and
            (LeftOf12 ?x)
            (RightOf14 ?x)
          )
          (and
            (LeftOf7 ?x)
            (RightOf13 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf15 ?x)
          )
          (and
            (RightOf16 ?x)
            (LeftOf11 ?x)
          )
          (and
            (LeftOf13 ?x)
            (RightOf17 ?x)
          )
          (and
            (RightOf15 ?x)
            (LeftOf11 ?x)
          )
          (and
            (RightOf14 ?x)
            (LeftOf14 ?x)
          )
          (and
            (RightOf15 ?x)
            (LeftOf1 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf18 ?x)
          )
        )
        (and
          (Column12 ?x)
        )
      )
      (when
        (or
          (and
            (LeftOf13 ?x)
            (RightOf18 ?x)
          )
          (and
            (LeftOf12 ?x)
            (RightOf18 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf14 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf18 ?x)
          )
          (and
            (LeftOf15 ?x)
            (RightOf14 ?x)
          )
          (and
            (RightOf14 ?x)
            (LeftOf9 ?x)
          )
          (and
            (LeftOf8 ?x)
            (RightOf17 ?x)
          )
          (and
            (LeftOf7 ?x)
            (RightOf18 ?x)
          )
          (and
            (RightOf15 ?x)
            (LeftOf8 ?x)
          )
          (and
            (LeftOf13 ?x)
            (RightOf16 ?x)
          )
          (and
            (RightOf17 ?x)
            (LeftOf11 ?x)
          )
          (and
            (RightOf15 ?x)
            (LeftOf10 ?x)
          )
          (and
            (RightOf17 ?x)
            (LeftOf14 ?x)
          )
          (and
            (RightOf15 ?x)
            (LeftOf14 ?x)
          )
          (and
            (LeftOf10 ?x)
            (RightOf17 ?x)
          )
          (and
            (LeftOf9 ?x)
            (RightOf18 ?x)
          )
          (and
            (LeftOf10 ?x)
            (RightOf16 ?x)
          )
          (and
            (RightOf17 ?x)
            (LeftOf3 ?x)
          )
          (and
            (RightOf14 ?x)
            (LeftOf3 ?x)
          )
          (and
            (LeftOf15 ?x)
            (RightOf15 ?x)
          )
          (and
            (RightOf14 ?x)
            (LeftOf7 ?x)
          )
          (and
            (RightOf15 ?x)
            (LeftOf4 ?x)
          )
          (and
            (RightOf18 ?x)
            (LeftOf3 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf18 ?x)
          )
          (and
            (RightOf17 ?x)
            (LeftOf9 ?x)
          )
          (and
            (RightOf17 ?x)
            (LeftOf1 ?x)
          )
          (and
            (RightOf14 ?x)
            (LeftOf2 ?x)
          )
          (and
            (RightOf15 ?x)
            (LeftOf3 ?x)
          )
          (and
            (LeftOf14 ?x)
            (RightOf18 ?x)
          )
          (and
            (RightOf16 ?x)
            (LeftOf1 ?x)
          )
          (and
            (RightOf16 ?x)
            (LeftOf14 ?x)
          )
          (and
            (LeftOf10 ?x)
            (RightOf18 ?x)
          )
          (and
            (LeftOf7 ?x)
            (RightOf16 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf16 ?x)
          )
          (and
            (LeftOf15 ?x)
            (RightOf16 ?x)
          )
          (and
            (RightOf14 ?x)
            (LeftOf8 ?x)
          )
          (and
            (RightOf16 ?x)
            (LeftOf3 ?x)
          )
          (and
            (LeftOf12 ?x)
            (RightOf15 ?x)
          )
          (and
            (LeftOf15 ?x)
            (RightOf17 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf17 ?x)
          )
          (and
            (RightOf14 ?x)
            (LeftOf10 ?x)
          )
          (and
            (LeftOf12 ?x)
            (RightOf14 ?x)
          )
          (and
            (LeftOf12 ?x)
            (RightOf17 ?x)
          )
          (and
            (LeftOf8 ?x)
            (RightOf16 ?x)
          )
          (and
            (RightOf17 ?x)
            (LeftOf2 ?x)
          )
          (and
            (RightOf14 ?x)
            (LeftOf4 ?x)
          )
          (and
            (RightOf15 ?x)
            (LeftOf7 ?x)
          )
          (and
            (RightOf14 ?x)
            (LeftOf13 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf16 ?x)
          )
          (and
            (LeftOf7 ?x)
            (RightOf17 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf15 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf14 ?x)
          )
          (and
            (RightOf16 ?x)
            (LeftOf4 ?x)
          )
          (and
            (LeftOf11 ?x)
            (RightOf18 ?x)
          )
          (and
            (LeftOf12 ?x)
            (RightOf16 ?x)
          )
          (and
            (RightOf18 ?x)
            (LeftOf4 ?x)
          )
          (and
            (RightOf16 ?x)
            (LeftOf11 ?x)
          )
          (and
            (LeftOf15 ?x)
            (RightOf18 ?x)
          )
          (and
            (RightOf15 ?x)
            (LeftOf13 ?x)
          )
          (and
            (LeftOf13 ?x)
            (RightOf17 ?x)
          )
          (and
            (RightOf15 ?x)
            (LeftOf2 ?x)
          )
          (and
            (RightOf18 ?x)
            (LeftOf14 ?x)
          )
          (and
            (LeftOf8 ?x)
            (RightOf18 ?x)
          )
          (and
            (RightOf17 ?x)
            (LeftOf4 ?x)
          )
          (and
            (RightOf16 ?x)
            (LeftOf9 ?x)
          )
          (and
            (RightOf15 ?x)
            (LeftOf9 ?x)
          )
          (and
            (RightOf15 ?x)
            (LeftOf11 ?x)
          )
          (and
            (RightOf14 ?x)
            (LeftOf1 ?x)
          )
          (and
            (LeftOf1 ?x)
            (RightOf18 ?x)
          )
          (and
            (RightOf14 ?x)
            (LeftOf14 ?x)
          )
          (and
            (RightOf15 ?x)
            (LeftOf1 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf16 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf17 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf18 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf15 ?x)
          )
          (and
            (LeftOf4 ?x)
            (RightOf18 ?x)
          )
          (and
            (RightOf14 ?x)
            (LeftOf11 ?x)
          )
        )
        (and
          (Column13 ?x)
        )
      )
      (when
        (or
          (and
            (LeftOf13 ?x)
            (RightOf18 ?x)
          )
          (and
            (RightOf15 ?x)
            (LeftOf16 ?x)
          )
          (and
            (RightOf17 ?x)
            (LeftOf4 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf18 ?x)
          )
          (and
            (LeftOf8 ?x)
            (RightOf17 ?x)
          )
          (and
            (LeftOf7 ?x)
            (RightOf18 ?x)
          )
          (and
            (RightOf15 ?x)
            (LeftOf8 ?x)
          )
          (and
            (LeftOf13 ?x)
            (RightOf16 ?x)
          )
          (and
            (RightOf16 ?x)
            (LeftOf9 ?x)
          )
          (and
            (RightOf17 ?x)
            (LeftOf11 ?x)
          )
          (and
            (LeftOf13 ?x)
            (RightOf17 ?x)
          )
          (and
            (RightOf15 ?x)
            (LeftOf10 ?x)
          )
          (and
            (RightOf17 ?x)
            (LeftOf14 ?x)
          )
          (and
            (RightOf15 ?x)
            (LeftOf14 ?x)
          )
          (and
            (LeftOf10 ?x)
            (RightOf17 ?x)
          )
          (and
            (LeftOf9 ?x)
            (RightOf18 ?x)
          )
          (and
            (LeftOf10 ?x)
            (RightOf16 ?x)
          )
          (and
            (LeftOf16 ?x)
            (RightOf16 ?x)
          )
          (and
            (RightOf15 ?x)
            (LeftOf4 ?x)
          )
          (and
            (RightOf18 ?x)
            (LeftOf3 ?x)
          )
          (and
            (LeftOf15 ?x)
            (RightOf18 ?x)
          )
          (and
            (RightOf17 ?x)
            (LeftOf9 ?x)
          )
          (and
            (RightOf17 ?x)
            (LeftOf1 ?x)
          )
          (and
            (RightOf17 ?x)
            (LeftOf3 ?x)
          )
          (and
            (LeftOf8 ?x)
            (RightOf16 ?x)
          )
          (and
            (LeftOf14 ?x)
            (RightOf18 ?x)
          )
          (and
            (RightOf16 ?x)
            (LeftOf1 ?x)
          )
          (and
            (RightOf16 ?x)
            (LeftOf14 ?x)
          )
          (and
            (LeftOf10 ?x)
            (RightOf18 ?x)
          )
          (and
            (LeftOf7 ?x)
            (RightOf16 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf16 ?x)
          )
          (and
            (LeftOf15 ?x)
            (RightOf16 ?x)
          )
          (and
            (RightOf16 ?x)
            (LeftOf3 ?x)
          )
          (and
            (LeftOf12 ?x)
            (RightOf18 ?x)
          )
          (and
            (LeftOf15 ?x)
            (RightOf17 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf17 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf18 ?x)
          )
          (and
            (LeftOf12 ?x)
            (RightOf17 ?x)
          )
          (and
            (RightOf15 ?x)
            (LeftOf3 ?x)
          )
          (and
            (RightOf17 ?x)
            (LeftOf2 ?x)
          )
          (and
            (RightOf15 ?x)
            (LeftOf7 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf16 ?x)
          )
          (and
            (LeftOf7 ?x)
            (RightOf17 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf15 ?x)
          )
          (and
            (RightOf16 ?x)
            (LeftOf4 ?x)
          )
          (and
            (LeftOf11 ?x)
            (RightOf18 ?x)
          )
          (and
            (LeftOf12 ?x)
            (RightOf16 ?x)
          )
          (and
            (RightOf18 ?x)
            (LeftOf4 ?x)
          )
          (and
            (RightOf16 ?x)
            (LeftOf11 ?x)
          )
          (and
            (LeftOf12 ?x)
            (RightOf15 ?x)
          )
          (and
            (RightOf15 ?x)
            (LeftOf13 ?x)
          )
          (and
            (RightOf15 ?x)
            (LeftOf15 ?x)
          )
          (and
            (RightOf18 ?x)
            (LeftOf14 ?x)
          )
          (and
            (LeftOf8 ?x)
            (RightOf18 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf18 ?x)
          )
          (and
            (LeftOf16 ?x)
            (RightOf18 ?x)
          )
          (and
            (RightOf15 ?x)
            (LeftOf9 ?x)
          )
          (and
            (RightOf15 ?x)
            (LeftOf11 ?x)
          )
          (and
            (RightOf17 ?x)
            (LeftOf16 ?x)
          )
          (and
            (LeftOf1 ?x)
            (RightOf18 ?x)
          )
          (and
            (RightOf15 ?x)
            (LeftOf1 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf16 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf17 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf15 ?x)
          )
          (and
            (LeftOf4 ?x)
            (RightOf18 ?x)
          )
          (and
            (RightOf15 ?x)
            (LeftOf2 ?x)
          )
        )
        (and
          (Column14 ?x)
        )
      )
      (when
        (or
          (and
            (LeftOf13 ?x)
            (RightOf18 ?x)
          )
          (and
            (LeftOf12 ?x)
            (RightOf18 ?x)
          )
          (and
            (RightOf17 ?x)
            (LeftOf4 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf18 ?x)
          )
          (and
            (LeftOf8 ?x)
            (RightOf17 ?x)
          )
          (and
            (LeftOf7 ?x)
            (RightOf18 ?x)
          )
          (and
            (LeftOf13 ?x)
            (RightOf16 ?x)
          )
          (and
            (RightOf16 ?x)
            (LeftOf9 ?x)
          )
          (and
            (RightOf17 ?x)
            (LeftOf11 ?x)
          )
          (and
            (RightOf17 ?x)
            (LeftOf14 ?x)
          )
          (and
            (LeftOf10 ?x)
            (RightOf17 ?x)
          )
          (and
            (LeftOf9 ?x)
            (RightOf18 ?x)
          )
          (and
            (RightOf16 ?x)
            (LeftOf14 ?x)
          )
          (and
            (LeftOf16 ?x)
            (RightOf16 ?x)
          )
          (and
            (RightOf18 ?x)
            (LeftOf3 ?x)
          )
          (and
            (RightOf16 ?x)
            (LeftOf1 ?x)
          )
          (and
            (RightOf17 ?x)
            (LeftOf9 ?x)
          )
          (and
            (RightOf17 ?x)
            (LeftOf1 ?x)
          )
          (and
            (RightOf18 ?x)
            (LeftOf17 ?x)
          )
          (and
            (LeftOf14 ?x)
            (RightOf18 ?x)
          )
          (and
            (LeftOf10 ?x)
            (RightOf16 ?x)
          )
          (and
            (LeftOf10 ?x)
            (RightOf18 ?x)
          )
          (and
            (RightOf16 ?x)
            (LeftOf17 ?x)
          )
          (and
            (LeftOf7 ?x)
            (RightOf16 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf16 ?x)
          )
          (and
            (LeftOf15 ?x)
            (RightOf16 ?x)
          )
          (and
            (RightOf16 ?x)
            (LeftOf3 ?x)
          )
          (and
            (LeftOf15 ?x)
            (RightOf17 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf17 ?x)
          )
          (and
            (LeftOf12 ?x)
            (RightOf17 ?x)
          )
          (and
            (LeftOf8 ?x)
            (RightOf16 ?x)
          )
          (and
            (RightOf17 ?x)
            (LeftOf2 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf16 ?x)
          )
          (and
            (LeftOf7 ?x)
            (RightOf17 ?x)
          )
          (and
            (RightOf17 ?x)
            (LeftOf17 ?x)
          )
          (and
            (RightOf16 ?x)
            (LeftOf4 ?x)
          )
          (and
            (RightOf17 ?x)
            (LeftOf3 ?x)
          )
          (and
            (LeftOf11 ?x)
            (RightOf18 ?x)
          )
          (and
            (LeftOf12 ?x)
            (RightOf16 ?x)
          )
          (and
            (RightOf18 ?x)
            (LeftOf4 ?x)
          )
          (and
            (RightOf16 ?x)
            (LeftOf11 ?x)
          )
          (and
            (LeftOf15 ?x)
            (RightOf18 ?x)
          )
          (and
            (LeftOf13 ?x)
            (RightOf17 ?x)
          )
          (and
            (RightOf18 ?x)
            (LeftOf14 ?x)
          )
          (and
            (LeftOf8 ?x)
            (RightOf18 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf18 ?x)
          )
          (and
            (LeftOf16 ?x)
            (RightOf18 ?x)
          )
          (and
            (RightOf17 ?x)
            (LeftOf16 ?x)
          )
          (and
            (LeftOf1 ?x)
            (RightOf18 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf16 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf17 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf18 ?x)
          )
          (and
            (LeftOf4 ?x)
            (RightOf18 ?x)
          )
        )
        (and
          (Column15 ?x)
        )
      )
      (when
        (or
          (and
            (LeftOf13 ?x)
            (RightOf18 ?x)
          )
          (and
            (LeftOf12 ?x)
            (RightOf18 ?x)
          )
          (and
            (RightOf17 ?x)
            (LeftOf4 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf18 ?x)
          )
          (and
            (LeftOf8 ?x)
            (RightOf17 ?x)
          )
          (and
            (LeftOf18 ?x)
            (RightOf17 ?x)
          )
          (and
            (RightOf17 ?x)
            (LeftOf11 ?x)
          )
          (and
            (RightOf17 ?x)
            (LeftOf14 ?x)
          )
          (and
            (LeftOf10 ?x)
            (RightOf17 ?x)
          )
          (and
            (LeftOf9 ?x)
            (RightOf18 ?x)
          )
          (and
            (RightOf17 ?x)
            (LeftOf3 ?x)
          )
          (and
            (RightOf18 ?x)
            (LeftOf3 ?x)
          )
          (and
            (LeftOf15 ?x)
            (RightOf18 ?x)
          )
          (and
            (RightOf17 ?x)
            (LeftOf9 ?x)
          )
          (and
            (RightOf17 ?x)
            (LeftOf1 ?x)
          )
          (and
            (RightOf18 ?x)
            (LeftOf17 ?x)
          )
          (and
            (LeftOf14 ?x)
            (RightOf18 ?x)
          )
          (and
            (LeftOf10 ?x)
            (RightOf18 ?x)
          )
          (and
            (RightOf18 ?x)
            (LeftOf4 ?x)
          )
          (and
            (LeftOf15 ?x)
            (RightOf17 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf17 ?x)
          )
          (and
            (LeftOf12 ?x)
            (RightOf17 ?x)
          )
          (and
            (RightOf17 ?x)
            (LeftOf2 ?x)
          )
          (and
            (LeftOf7 ?x)
            (RightOf17 ?x)
          )
          (and
            (RightOf17 ?x)
            (LeftOf17 ?x)
          )
          (and
            (LeftOf8 ?x)
            (RightOf18 ?x)
          )
          (and
            (LeftOf18 ?x)
            (RightOf18 ?x)
          )
          (and
            (LeftOf11 ?x)
            (RightOf18 ?x)
          )
          (and
            (LeftOf7 ?x)
            (RightOf18 ?x)
          )
          (and
            (LeftOf13 ?x)
            (RightOf17 ?x)
          )
          (and
            (RightOf18 ?x)
            (LeftOf14 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf18 ?x)
          )
          (and
            (LeftOf16 ?x)
            (RightOf18 ?x)
          )
          (and
            (RightOf17 ?x)
            (LeftOf16 ?x)
          )
          (and
            (LeftOf1 ?x)
            (RightOf18 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf17 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf18 ?x)
          )
          (and
            (LeftOf4 ?x)
            (RightOf18 ?x)
          )
        )
        (and
          (Column16 ?x)
        )
      )
      (when
        (or
          (and
            (LeftOf13 ?x)
            (RightOf18 ?x)
          )
          (and
            (LeftOf12 ?x)
            (RightOf18 ?x)
          )
          (and
            (RightOf18 ?x)
            (LeftOf3 ?x)
          )
          (and
            (RightOf18 ?x)
            (LeftOf14 ?x)
          )
          (and
            (LeftOf15 ?x)
            (RightOf18 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf18 ?x)
          )
          (and
            (RightOf18 ?x)
            (LeftOf17 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf18 ?x)
          )
          (and
            (LeftOf16 ?x)
            (RightOf18 ?x)
          )
          (and
            (RightOf18 ?x)
            (LeftOf4 ?x)
          )
          (and
            (LeftOf10 ?x)
            (RightOf18 ?x)
          )
          (and
            (LeftOf19 ?x)
            (RightOf18 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf18 ?x)
          )
          (and
            (LeftOf1 ?x)
            (RightOf18 ?x)
          )
          (and
            (LeftOf8 ?x)
            (RightOf18 ?x)
          )
          (and
            (LeftOf18 ?x)
            (RightOf18 ?x)
          )
          (and
            (LeftOf11 ?x)
            (RightOf18 ?x)
          )
          (and
            (LeftOf7 ?x)
            (RightOf18 ?x)
          )
          (and
            (LeftOf9 ?x)
            (RightOf18 ?x)
          )
        )
        (and
          (Column17 ?x)
        )
      )
    )
  )
  (:action moveRight
    :parameters ( ?x )
    :precondition ( and
      (not (CheckConsistency))
      (not (Error))
      (or
        (LeftOf3 ?x)
        (RightOf7 ?x)
        (RightOf8 ?x)
        (RightOf15 ?x)
        (LeftOf2 ?x)
        (LeftOf9 ?x)
        (RightOf2 ?x)
        (RightOf0 ?x)
        (LeftOf8 ?x)
        (RightOf12 ?x)
        (LeftOf14 ?x)
        (LeftOf5 ?x)
        (RightOf1 ?x)
        (LeftOf12 ?x)
        (LeftOf19 ?x)
        (Columns ?x)
        (RightOf13 ?x)
        (RightOf14 ?x)
        (LeftOf1 ?x)
        (RightOf6 ?x)
        (RightOf9 ?x)
        (LeftOf6 ?x)
        (RightOf11 ?x)
        (RightOf17 ?x)
        (LeftOf11 ?x)
        (RightOf5 ?x)
        (LeftOf10 ?x)
        (LeftOf15 ?x)
        (LeftOf4 ?x)
        (RightOf18 ?x)
        (LeftOf16 ?x)
        (RightOf16 ?x)
        (RightOf4 ?x)
        (RightOf10 ?x)
        (LeftOf7 ?x)
        (LeftOf13 ?x)
        (LeftOf17 ?x)
        (LeftOf18 ?x)
        (RightOf3 ?x)
      )
    )
    :effect ( and
      (CheckConsistency)
      (when
        (or
          (RightOf6 ?x)
          (RightOf15 ?x)
          (RightOf11 ?x)
          (RightOf7 ?x)
          (RightOf18 ?x)
          (RightOf16 ?x)
          (RightOf1 ?x)
          (RightOf4 ?x)
          (RightOf0 ?x)
          (RightOf12 ?x)
          (RightOf13 ?x)
          (RightOf14 ?x)
          (RightOf17 ?x)
          (RightOf2 ?x)
          (RightOf9 ?x)
          (RightOf5 ?x)
          (RightOf10 ?x)
          (RightOf8 ?x)
          (RightOf3 ?x)
        )
        (and
          (RightOf1 ?x)
        )
      )
      (when
        (or
          (RightOf6 ?x)
          (RightOf15 ?x)
          (RightOf11 ?x)
          (RightOf7 ?x)
          (RightOf18 ?x)
          (RightOf16 ?x)
          (RightOf1 ?x)
          (RightOf4 ?x)
          (RightOf10 ?x)
          (RightOf12 ?x)
          (RightOf13 ?x)
          (RightOf14 ?x)
          (RightOf17 ?x)
          (RightOf2 ?x)
          (RightOf9 ?x)
          (RightOf5 ?x)
          (RightOf8 ?x)
          (RightOf3 ?x)
        )
        (and
          (RightOf2 ?x)
        )
      )
      (when
        (or
          (RightOf6 ?x)
          (RightOf15 ?x)
          (RightOf11 ?x)
          (RightOf7 ?x)
          (RightOf18 ?x)
          (RightOf16 ?x)
          (RightOf8 ?x)
          (RightOf4 ?x)
          (RightOf10 ?x)
          (RightOf12 ?x)
          (RightOf13 ?x)
          (RightOf14 ?x)
          (RightOf17 ?x)
          (RightOf2 ?x)
          (RightOf9 ?x)
          (RightOf5 ?x)
          (RightOf3 ?x)
        )
        (and
          (RightOf3 ?x)
        )
      )
      (when
        (or
          (RightOf6 ?x)
          (RightOf15 ?x)
          (RightOf11 ?x)
          (RightOf18 ?x)
          (RightOf16 ?x)
          (RightOf8 ?x)
          (RightOf4 ?x)
          (RightOf10 ?x)
          (RightOf12 ?x)
          (RightOf13 ?x)
          (RightOf14 ?x)
          (RightOf17 ?x)
          (RightOf9 ?x)
          (RightOf5 ?x)
          (RightOf3 ?x)
          (RightOf7 ?x)
        )
        (and
          (RightOf4 ?x)
        )
      )
      (when
        (or
          (RightOf6 ?x)
          (RightOf15 ?x)
          (RightOf11 ?x)
          (RightOf18 ?x)
          (RightOf16 ?x)
          (RightOf8 ?x)
          (RightOf4 ?x)
          (RightOf10 ?x)
          (RightOf12 ?x)
          (RightOf13 ?x)
          (RightOf14 ?x)
          (RightOf17 ?x)
          (RightOf9 ?x)
          (RightOf5 ?x)
          (RightOf7 ?x)
        )
        (and
          (RightOf5 ?x)
        )
      )
      (when
        (or
          (RightOf6 ?x)
          (RightOf15 ?x)
          (RightOf11 ?x)
          (RightOf18 ?x)
          (RightOf16 ?x)
          (RightOf8 ?x)
          (RightOf10 ?x)
          (RightOf12 ?x)
          (RightOf13 ?x)
          (RightOf14 ?x)
          (RightOf17 ?x)
          (RightOf9 ?x)
          (RightOf5 ?x)
          (RightOf7 ?x)
        )
        (and
          (RightOf6 ?x)
        )
      )
      (when
        (or
          (RightOf6 ?x)
          (RightOf15 ?x)
          (RightOf11 ?x)
          (RightOf18 ?x)
          (RightOf16 ?x)
          (RightOf8 ?x)
          (RightOf10 ?x)
          (RightOf12 ?x)
          (RightOf13 ?x)
          (RightOf14 ?x)
          (RightOf17 ?x)
          (RightOf9 ?x)
          (RightOf7 ?x)
        )
        (and
          (RightOf7 ?x)
        )
      )
      (when
        (or
          (RightOf11 ?x)
          (RightOf9 ?x)
          (RightOf7 ?x)
          (RightOf16 ?x)
          (RightOf12 ?x)
          (RightOf8 ?x)
          (RightOf10 ?x)
          (RightOf15 ?x)
          (RightOf13 ?x)
          (RightOf14 ?x)
          (RightOf17 ?x)
          (RightOf18 ?x)
        )
        (and
          (RightOf8 ?x)
        )
      )
      (when
        (or
          (RightOf11 ?x)
          (RightOf9 ?x)
          (RightOf12 ?x)
          (RightOf16 ?x)
          (RightOf8 ?x)
          (RightOf10 ?x)
          (RightOf15 ?x)
          (RightOf13 ?x)
          (RightOf14 ?x)
          (RightOf17 ?x)
          (RightOf18 ?x)
        )
        (and
          (RightOf9 ?x)
        )
      )
      (when
        (or
          (RightOf11 ?x)
          (RightOf18 ?x)
          (RightOf12 ?x)
          (RightOf16 ?x)
          (RightOf10 ?x)
          (RightOf15 ?x)
          (RightOf13 ?x)
          (RightOf14 ?x)
          (RightOf17 ?x)
          (RightOf9 ?x)
        )
        (and
          (RightOf10 ?x)
        )
      )
      (when
        (or
          (RightOf11 ?x)
          (RightOf18 ?x)
          (RightOf12 ?x)
          (RightOf16 ?x)
          (RightOf10 ?x)
          (RightOf15 ?x)
          (RightOf13 ?x)
          (RightOf14 ?x)
          (RightOf17 ?x)
        )
        (and
          (RightOf11 ?x)
        )
      )
      (when
        (or
          (RightOf11 ?x)
          (RightOf18 ?x)
          (RightOf12 ?x)
          (RightOf16 ?x)
          (RightOf15 ?x)
          (RightOf13 ?x)
          (RightOf14 ?x)
          (RightOf17 ?x)
        )
        (and
          (RightOf12 ?x)
        )
      )
      (when
        (or
          (RightOf15 ?x)
          (RightOf18 ?x)
          (RightOf16 ?x)
          (RightOf12 ?x)
          (RightOf13 ?x)
          (RightOf14 ?x)
          (RightOf17 ?x)
        )
        (and
          (RightOf13 ?x)
        )
      )
      (when
        (or
          (RightOf18 ?x)
          (RightOf16 ?x)
          (RightOf15 ?x)
          (RightOf13 ?x)
          (RightOf14 ?x)
          (RightOf17 ?x)
        )
        (and
          (RightOf14 ?x)
        )
      )
      (when
        (or
          (RightOf15 ?x)
          (RightOf18 ?x)
          (RightOf14 ?x)
          (RightOf17 ?x)
          (RightOf16 ?x)
        )
        (and
          (RightOf15 ?x)
        )
      )
      (when
        (or
          (RightOf15 ?x)
          (RightOf18 ?x)
          (RightOf17 ?x)
          (RightOf16 ?x)
        )
        (and
          (RightOf16 ?x)
        )
      )
      (when
        (or
          (RightOf18 ?x)
          (RightOf17 ?x)
          (RightOf16 ?x)
        )
        (and
          (RightOf17 ?x)
        )
      )
      (when
        (or
          (RightOf18 ?x)
          (RightOf17 ?x)
        )
        (and
          (RightOf18 ?x)
        )
      )
      (when
        (LeftOf1 ?x)
        (and
          (LeftOf2 ?x)
          (not (LeftOf1 ?x))
        )
      )
      (when
        (or
          (LeftOf2 ?x)
          (LeftOf1 ?x)
        )
        (and
          (LeftOf3 ?x)
          (not (LeftOf2 ?x))
        )
      )
      (when
        (or
          (LeftOf2 ?x)
          (LeftOf3 ?x)
          (LeftOf1 ?x)
        )
        (and
          (LeftOf4 ?x)
          (not (LeftOf3 ?x))
        )
      )
      (when
        (or
          (LeftOf2 ?x)
          (LeftOf4 ?x)
          (LeftOf3 ?x)
          (LeftOf1 ?x)
        )
        (and
          (LeftOf5 ?x)
          (not (LeftOf4 ?x))
        )
      )
      (when
        (or
          (LeftOf2 ?x)
          (LeftOf4 ?x)
          (LeftOf3 ?x)
          (LeftOf1 ?x)
          (LeftOf5 ?x)
        )
        (and
          (LeftOf6 ?x)
          (not (LeftOf5 ?x))
        )
      )
      (when
        (or
          (LeftOf4 ?x)
          (LeftOf3 ?x)
          (LeftOf5 ?x)
          (LeftOf6 ?x)
          (LeftOf1 ?x)
          (LeftOf2 ?x)
        )
        (and
          (LeftOf7 ?x)
          (not (LeftOf6 ?x))
        )
      )
      (when
        (or
          (LeftOf4 ?x)
          (LeftOf3 ?x)
          (LeftOf5 ?x)
          (LeftOf6 ?x)
          (LeftOf1 ?x)
          (LeftOf7 ?x)
          (LeftOf2 ?x)
        )
        (and
          (LeftOf8 ?x)
          (not (LeftOf7 ?x))
        )
      )
      (when
        (or
          (LeftOf8 ?x)
          (LeftOf4 ?x)
          (LeftOf3 ?x)
          (LeftOf5 ?x)
          (LeftOf6 ?x)
          (LeftOf1 ?x)
          (LeftOf7 ?x)
          (LeftOf2 ?x)
        )
        (and
          (LeftOf9 ?x)
          (not (LeftOf8 ?x))
        )
      )
      (when
        (or
          (LeftOf8 ?x)
          (LeftOf4 ?x)
          (LeftOf3 ?x)
          (LeftOf5 ?x)
          (LeftOf6 ?x)
          (LeftOf1 ?x)
          (LeftOf7 ?x)
          (LeftOf2 ?x)
          (LeftOf9 ?x)
        )
        (and
          (LeftOf10 ?x)
          (not (LeftOf9 ?x))
        )
      )
      (when
        (or
          (LeftOf8 ?x)
          (LeftOf4 ?x)
          (LeftOf3 ?x)
          (LeftOf5 ?x)
          (LeftOf6 ?x)
          (LeftOf1 ?x)
          (LeftOf7 ?x)
          (LeftOf2 ?x)
          (LeftOf9 ?x)
          (LeftOf10 ?x)
        )
        (and
          (LeftOf11 ?x)
          (not (LeftOf10 ?x))
        )
      )
      (when
        (or
          (LeftOf8 ?x)
          (LeftOf4 ?x)
          (LeftOf3 ?x)
          (LeftOf5 ?x)
          (LeftOf6 ?x)
          (LeftOf1 ?x)
          (LeftOf7 ?x)
          (LeftOf2 ?x)
          (LeftOf9 ?x)
          (LeftOf11 ?x)
          (LeftOf10 ?x)
        )
        (and
          (LeftOf12 ?x)
          (not (LeftOf11 ?x))
        )
      )
      (when
        (or
          (LeftOf8 ?x)
          (LeftOf4 ?x)
          (LeftOf3 ?x)
          (LeftOf5 ?x)
          (LeftOf6 ?x)
          (LeftOf12 ?x)
          (LeftOf1 ?x)
          (LeftOf7 ?x)
          (LeftOf2 ?x)
          (LeftOf9 ?x)
          (LeftOf11 ?x)
          (LeftOf10 ?x)
        )
        (and
          (LeftOf13 ?x)
          (not (LeftOf12 ?x))
        )
      )
      (when
        (or
          (LeftOf8 ?x)
          (LeftOf7 ?x)
          (LeftOf4 ?x)
          (LeftOf3 ?x)
          (LeftOf5 ?x)
          (LeftOf6 ?x)
          (LeftOf12 ?x)
          (LeftOf1 ?x)
          (LeftOf13 ?x)
          (LeftOf2 ?x)
          (LeftOf9 ?x)
          (LeftOf11 ?x)
          (LeftOf10 ?x)
        )
        (and
          (LeftOf14 ?x)
          (not (LeftOf13 ?x))
        )
      )
      (when
        (or
          (LeftOf8 ?x)
          (LeftOf7 ?x)
          (LeftOf4 ?x)
          (LeftOf3 ?x)
          (LeftOf14 ?x)
          (LeftOf5 ?x)
          (LeftOf6 ?x)
          (LeftOf12 ?x)
          (LeftOf1 ?x)
          (LeftOf13 ?x)
          (LeftOf2 ?x)
          (LeftOf9 ?x)
          (LeftOf11 ?x)
          (LeftOf10 ?x)
        )
        (and
          (LeftOf15 ?x)
          (not (LeftOf14 ?x))
        )
      )
      (when
        (or
          (LeftOf8 ?x)
          (LeftOf7 ?x)
          (LeftOf4 ?x)
          (LeftOf3 ?x)
          (LeftOf14 ?x)
          (LeftOf5 ?x)
          (LeftOf6 ?x)
          (LeftOf12 ?x)
          (LeftOf1 ?x)
          (LeftOf13 ?x)
          (LeftOf2 ?x)
          (LeftOf9 ?x)
          (LeftOf11 ?x)
          (LeftOf10 ?x)
          (LeftOf15 ?x)
        )
        (and
          (LeftOf16 ?x)
          (not (LeftOf15 ?x))
        )
      )
      (when
        (or
          (LeftOf8 ?x)
          (LeftOf7 ?x)
          (LeftOf4 ?x)
          (LeftOf3 ?x)
          (LeftOf14 ?x)
          (LeftOf16 ?x)
          (LeftOf5 ?x)
          (LeftOf6 ?x)
          (LeftOf12 ?x)
          (LeftOf1 ?x)
          (LeftOf13 ?x)
          (LeftOf2 ?x)
          (LeftOf9 ?x)
          (LeftOf11 ?x)
          (LeftOf10 ?x)
          (LeftOf15 ?x)
        )
        (and
          (LeftOf17 ?x)
          (not (LeftOf16 ?x))
        )
      )
      (when
        (or
          (LeftOf8 ?x)
          (LeftOf7 ?x)
          (LeftOf4 ?x)
          (LeftOf3 ?x)
          (LeftOf14 ?x)
          (LeftOf16 ?x)
          (LeftOf5 ?x)
          (LeftOf6 ?x)
          (LeftOf12 ?x)
          (LeftOf1 ?x)
          (LeftOf13 ?x)
          (LeftOf2 ?x)
          (LeftOf9 ?x)
          (LeftOf11 ?x)
          (LeftOf17 ?x)
          (LeftOf10 ?x)
          (LeftOf15 ?x)
        )
        (and
          (LeftOf18 ?x)
          (not (LeftOf17 ?x))
        )
      )
      (when
        (or
          (LeftOf8 ?x)
          (LeftOf7 ?x)
          (LeftOf4 ?x)
          (LeftOf3 ?x)
          (LeftOf14 ?x)
          (LeftOf16 ?x)
          (LeftOf5 ?x)
          (LeftOf6 ?x)
          (LeftOf12 ?x)
          (LeftOf1 ?x)
          (LeftOf13 ?x)
          (LeftOf2 ?x)
          (LeftOf9 ?x)
          (LeftOf11 ?x)
          (LeftOf17 ?x)
          (LeftOf18 ?x)
          (LeftOf10 ?x)
          (LeftOf15 ?x)
        )
        (and
          (LeftOf19 ?x)
          (not (LeftOf18 ?x))
        )
      )
      (when
        (Column0 ?x)
        (and
          (Column1 ?x)
          (not (Column0 ?x))
        )
      )
      (when
        (Column1 ?x)
        (and
          (Column2 ?x)
          (not (Column1 ?x))
        )
      )
      (when
        (Column2 ?x)
        (and
          (Column3 ?x)
          (not (Column2 ?x))
        )
      )
      (when
        (Column3 ?x)
        (and
          (Column4 ?x)
          (not (Column3 ?x))
        )
      )
      (when
        (Column4 ?x)
        (and
          (Column5 ?x)
          (not (Column4 ?x))
        )
      )
      (when
        (Column5 ?x)
        (and
          (Column6 ?x)
          (not (Column5 ?x))
        )
      )
      (when
        (Column6 ?x)
        (and
          (Column7 ?x)
          (not (Column6 ?x))
        )
      )
      (when
        (Column7 ?x)
        (and
          (Column8 ?x)
          (not (Column7 ?x))
        )
      )
      (when
        (Column8 ?x)
        (and
          (Column9 ?x)
          (not (Column8 ?x))
        )
      )
      (when
        (Column9 ?x)
        (and
          (Column10 ?x)
          (not (Column9 ?x))
        )
      )
      (when
        (Column10 ?x)
        (and
          (Column11 ?x)
          (not (Column10 ?x))
        )
      )
      (when
        (Column11 ?x)
        (and
          (Column12 ?x)
          (not (Column11 ?x))
        )
      )
      (when
        (Column12 ?x)
        (and
          (Column13 ?x)
          (not (Column12 ?x))
        )
      )
      (when
        (Column13 ?x)
        (and
          (Column14 ?x)
          (not (Column13 ?x))
        )
      )
      (when
        (Column14 ?x)
        (and
          (Column15 ?x)
          (not (Column14 ?x))
        )
      )
      (when
        (Column15 ?x)
        (and
          (Column16 ?x)
          (not (Column15 ?x))
        )
      )
      (when
        (Column16 ?x)
        (and
          (Column17 ?x)
          (not (Column16 ?x))
        )
      )
      (when
        (Column17 ?x)
        (and
          (Column18 ?x)
          (not (Column17 ?x))
        )
      )
      (when
        (or
          (and
            (RightOf12 ?x)
            (LeftOf1 ?x)
          )
          (and
            (RightOf16 ?x)
            (LeftOf1 ?x)
          )
          (and
            (LeftOf1 ?x)
            (RightOf10 ?x)
          )
          (and
            (RightOf14 ?x)
            (LeftOf1 ?x)
          )
          (and
            (RightOf6 ?x)
            (LeftOf1 ?x)
          )
          (and
            (LeftOf1 ?x)
            (RightOf8 ?x)
          )
          (and
            (LeftOf1 ?x)
            (RightOf11 ?x)
          )
          (and
            (RightOf2 ?x)
            (LeftOf1 ?x)
          )
          (and
            (RightOf5 ?x)
            (LeftOf1 ?x)
          )
          (and
            (LeftOf1 ?x)
            (RightOf0 ?x)
          )
          (and
            (RightOf17 ?x)
            (LeftOf1 ?x)
          )
          (and
            (LeftOf1 ?x)
            (RightOf18 ?x)
          )
          (and
            (LeftOf1 ?x)
            (RightOf3 ?x)
          )
          (and
            (RightOf1 ?x)
            (LeftOf1 ?x)
          )
          (and
            (LeftOf1 ?x)
            (RightOf13 ?x)
          )
          (and
            (RightOf9 ?x)
            (LeftOf1 ?x)
          )
          (and
            (RightOf7 ?x)
            (LeftOf1 ?x)
          )
          (and
            (RightOf15 ?x)
            (LeftOf1 ?x)
          )
          (and
            (RightOf4 ?x)
            (LeftOf1 ?x)
          )
        )
        (and
          (Column1 ?x)
        )
      )
      (when
        (or
          (and
            (RightOf4 ?x)
            (LeftOf2 ?x)
          )
          (and
            (RightOf6 ?x)
            (LeftOf2 ?x)
          )
          (and
            (RightOf16 ?x)
            (LeftOf1 ?x)
          )
          (and
            (RightOf14 ?x)
            (LeftOf1 ?x)
          )
          (and
            (RightOf6 ?x)
            (LeftOf1 ?x)
          )
          (and
            (RightOf2 ?x)
            (LeftOf1 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf2 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf8 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf11 ?x)
          )
          (and
            (LeftOf1 ?x)
            (RightOf13 ?x)
          )
          (and
            (RightOf17 ?x)
            (LeftOf1 ?x)
          )
          (and
            (LeftOf1 ?x)
            (RightOf8 ?x)
          )
          (and
            (RightOf5 ?x)
            (LeftOf1 ?x)
          )
          (and
            (RightOf12 ?x)
            (LeftOf2 ?x)
          )
          (and
            (RightOf7 ?x)
            (LeftOf1 ?x)
          )
          (and
            (RightOf12 ?x)
            (LeftOf1 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf10 ?x)
          )
          (and
            (RightOf4 ?x)
            (LeftOf1 ?x)
          )
          (and
            (RightOf14 ?x)
            (LeftOf2 ?x)
          )
          (and
            (RightOf17 ?x)
            (LeftOf2 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf3 ?x)
          )
          (and
            (LeftOf1 ?x)
            (RightOf11 ?x)
          )
          (and
            (LeftOf1 ?x)
            (RightOf5 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf16 ?x)
          )
          (and
            (RightOf7 ?x)
            (LeftOf2 ?x)
          )
          (and
            (RightOf1 ?x)
            (LeftOf1 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf5 ?x)
          )
          (and
            (RightOf1 ?x)
            (LeftOf2 ?x)
          )
          (and
            (LeftOf1 ?x)
            (RightOf10 ?x)
          )
          (and
            (RightOf15 ?x)
            (LeftOf1 ?x)
          )
          (and
            (LeftOf1 ?x)
            (RightOf18 ?x)
          )
          (and
            (LeftOf1 ?x)
            (RightOf3 ?x)
          )
          (and
            (RightOf9 ?x)
            (LeftOf1 ?x)
          )
          (and
            (RightOf9 ?x)
            (LeftOf2 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf13 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf18 ?x)
          )
          (and
            (RightOf15 ?x)
            (LeftOf2 ?x)
          )
        )
        (and
          (Column2 ?x)
        )
      )
      (when
        (or
          (and
            (RightOf2 ?x)
            (LeftOf3 ?x)
          )
          (and
            (RightOf4 ?x)
            (LeftOf2 ?x)
          )
          (and
            (RightOf9 ?x)
            (LeftOf3 ?x)
          )
          (and
            (RightOf6 ?x)
            (LeftOf2 ?x)
          )
          (and
            (RightOf16 ?x)
            (LeftOf1 ?x)
          )
          (and
            (RightOf14 ?x)
            (LeftOf1 ?x)
          )
          (and
            (RightOf6 ?x)
            (LeftOf1 ?x)
          )
          (and
            (RightOf2 ?x)
            (LeftOf1 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf2 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf8 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf11 ?x)
          )
          (and
            (LeftOf1 ?x)
            (RightOf13 ?x)
          )
          (and
            (RightOf17 ?x)
            (LeftOf3 ?x)
          )
          (and
            (LeftOf3 ?x)
            (RightOf13 ?x)
          )
          (and
            (RightOf18 ?x)
            (LeftOf3 ?x)
          )
          (and
            (RightOf11 ?x)
            (LeftOf3 ?x)
          )
          (and
            (LeftOf3 ?x)
            (RightOf3 ?x)
          )
          (and
            (LeftOf1 ?x)
            (RightOf8 ?x)
          )
          (and
            (RightOf15 ?x)
            (LeftOf3 ?x)
          )
          (and
            (RightOf5 ?x)
            (LeftOf3 ?x)
          )
          (and
            (LeftOf1 ?x)
            (RightOf18 ?x)
          )
          (and
            (RightOf12 ?x)
            (LeftOf2 ?x)
          )
          (and
            (RightOf7 ?x)
            (LeftOf2 ?x)
          )
          (and
            (RightOf8 ?x)
            (LeftOf3 ?x)
          )
          (and
            (RightOf10 ?x)
            (LeftOf3 ?x)
          )
          (and
            (RightOf12 ?x)
            (LeftOf1 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf10 ?x)
          )
          (and
            (RightOf4 ?x)
            (LeftOf1 ?x)
          )
          (and
            (RightOf14 ?x)
            (LeftOf2 ?x)
          )
          (and
            (RightOf12 ?x)
            (LeftOf3 ?x)
          )
          (and
            (RightOf17 ?x)
            (LeftOf2 ?x)
          )
          (and
            (RightOf5 ?x)
            (LeftOf1 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf3 ?x)
          )
          (and
            (LeftOf1 ?x)
            (RightOf11 ?x)
          )
          (and
            (RightOf7 ?x)
            (LeftOf3 ?x)
          )
          (and
            (LeftOf1 ?x)
            (RightOf5 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf16 ?x)
          )
          (and
            (RightOf7 ?x)
            (LeftOf1 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf5 ?x)
          )
          (and
            (RightOf16 ?x)
            (LeftOf3 ?x)
          )
          (and
            (LeftOf1 ?x)
            (RightOf10 ?x)
          )
          (and
            (RightOf15 ?x)
            (LeftOf1 ?x)
          )
          (and
            (RightOf4 ?x)
            (LeftOf3 ?x)
          )
          (and
            (RightOf17 ?x)
            (LeftOf1 ?x)
          )
          (and
            (RightOf14 ?x)
            (LeftOf3 ?x)
          )
          (and
            (LeftOf1 ?x)
            (RightOf3 ?x)
          )
          (and
            (RightOf9 ?x)
            (LeftOf1 ?x)
          )
          (and
            (RightOf6 ?x)
            (LeftOf3 ?x)
          )
          (and
            (RightOf9 ?x)
            (LeftOf2 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf13 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf18 ?x)
          )
          (and
            (RightOf15 ?x)
            (LeftOf2 ?x)
          )
        )
        (and
          (Column3 ?x)
        )
      )
      (when
        (or
          (and
            (RightOf4 ?x)
            (LeftOf2 ?x)
          )
          (and
            (RightOf9 ?x)
            (LeftOf3 ?x)
          )
          (and
            (RightOf6 ?x)
            (LeftOf2 ?x)
          )
          (and
            (RightOf11 ?x)
            (LeftOf4 ?x)
          )
          (and
            (LeftOf4 ?x)
            (RightOf3 ?x)
          )
          (and
            (RightOf6 ?x)
            (LeftOf1 ?x)
          )
          (and
            (LeftOf4 ?x)
            (RightOf13 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf8 ?x)
          )
          (and
            (RightOf5 ?x)
            (LeftOf4 ?x)
          )
          (and
            (RightOf6 ?x)
            (LeftOf4 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf11 ?x)
          )
          (and
            (LeftOf1 ?x)
            (RightOf13 ?x)
          )
          (and
            (RightOf16 ?x)
            (LeftOf4 ?x)
          )
          (and
            (RightOf9 ?x)
            (LeftOf4 ?x)
          )
          (and
            (RightOf17 ?x)
            (LeftOf3 ?x)
          )
          (and
            (LeftOf3 ?x)
            (RightOf13 ?x)
          )
          (and
            (RightOf18 ?x)
            (LeftOf3 ?x)
          )
          (and
            (LeftOf4 ?x)
            (RightOf10 ?x)
          )
          (and
            (RightOf12 ?x)
            (LeftOf4 ?x)
          )
          (and
            (RightOf11 ?x)
            (LeftOf3 ?x)
          )
          (and
            (RightOf17 ?x)
            (LeftOf1 ?x)
          )
          (and
            (LeftOf1 ?x)
            (RightOf8 ?x)
          )
          (and
            (RightOf15 ?x)
            (LeftOf3 ?x)
          )
          (and
            (RightOf5 ?x)
            (LeftOf3 ?x)
          )
          (and
            (RightOf16 ?x)
            (LeftOf1 ?x)
          )
          (and
            (LeftOf1 ?x)
            (RightOf18 ?x)
          )
          (and
            (LeftOf4 ?x)
            (RightOf8 ?x)
          )
          (and
            (RightOf12 ?x)
            (LeftOf2 ?x)
          )
          (and
            (RightOf7 ?x)
            (LeftOf2 ?x)
          )
          (and
            (RightOf3 ?x)
            (LeftOf3 ?x)
          )
          (and
            (RightOf10 ?x)
            (LeftOf3 ?x)
          )
          (and
            (RightOf12 ?x)
            (LeftOf1 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf10 ?x)
          )
          (and
            (RightOf14 ?x)
            (LeftOf4 ?x)
          )
          (and
            (RightOf4 ?x)
            (LeftOf1 ?x)
          )
          (and
            (RightOf14 ?x)
            (LeftOf2 ?x)
          )
          (and
            (RightOf12 ?x)
            (LeftOf3 ?x)
          )
          (and
            (RightOf17 ?x)
            (LeftOf2 ?x)
          )
          (and
            (RightOf5 ?x)
            (LeftOf1 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf3 ?x)
          )
          (and
            (LeftOf1 ?x)
            (RightOf11 ?x)
          )
          (and
            (RightOf7 ?x)
            (LeftOf3 ?x)
          )
          (and
            (RightOf15 ?x)
            (LeftOf4 ?x)
          )
          (and
            (LeftOf1 ?x)
            (RightOf5 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf16 ?x)
          )
          (and
            (RightOf7 ?x)
            (LeftOf1 ?x)
          )
          (and
            (RightOf8 ?x)
            (LeftOf3 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf5 ?x)
          )
          (and
            (RightOf7 ?x)
            (LeftOf4 ?x)
          )
          (and
            (RightOf16 ?x)
            (LeftOf3 ?x)
          )
          (and
            (RightOf4 ?x)
            (LeftOf4 ?x)
          )
          (and
            (RightOf17 ?x)
            (LeftOf4 ?x)
          )
          (and
            (LeftOf1 ?x)
            (RightOf10 ?x)
          )
          (and
            (RightOf15 ?x)
            (LeftOf1 ?x)
          )
          (and
            (RightOf4 ?x)
            (LeftOf3 ?x)
          )
          (and
            (RightOf13 ?x)
            (LeftOf3 ?x)
          )
          (and
            (RightOf14 ?x)
            (LeftOf1 ?x)
          )
          (and
            (RightOf14 ?x)
            (LeftOf3 ?x)
          )
          (and
            (LeftOf1 ?x)
            (RightOf3 ?x)
          )
          (and
            (RightOf9 ?x)
            (LeftOf1 ?x)
          )
          (and
            (RightOf6 ?x)
            (LeftOf3 ?x)
          )
          (and
            (RightOf9 ?x)
            (LeftOf2 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf13 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf18 ?x)
          )
          (and
            (LeftOf4 ?x)
            (RightOf18 ?x)
          )
          (and
            (RightOf15 ?x)
            (LeftOf2 ?x)
          )
        )
        (and
          (Column4 ?x)
        )
      )
      (when
        (or
          (and
            (LeftOf5 ?x)
            (RightOf17 ?x)
          )
          (and
            (RightOf4 ?x)
            (LeftOf2 ?x)
          )
          (and
            (RightOf9 ?x)
            (LeftOf3 ?x)
          )
          (and
            (RightOf6 ?x)
            (LeftOf2 ?x)
          )
          (and
            (RightOf11 ?x)
            (LeftOf4 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf5 ?x)
          )
          (and
            (RightOf6 ?x)
            (LeftOf1 ?x)
          )
          (and
            (RightOf5 ?x)
            (LeftOf3 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf14 ?x)
          )
          (and
            (LeftOf4 ?x)
            (RightOf13 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf8 ?x)
          )
          (and
            (RightOf5 ?x)
            (LeftOf4 ?x)
          )
          (and
            (RightOf6 ?x)
            (LeftOf4 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf11 ?x)
          )
          (and
            (LeftOf1 ?x)
            (RightOf13 ?x)
          )
          (and
            (RightOf16 ?x)
            (LeftOf4 ?x)
          )
          (and
            (RightOf9 ?x)
            (LeftOf4 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf18 ?x)
          )
          (and
            (RightOf17 ?x)
            (LeftOf3 ?x)
          )
          (and
            (LeftOf3 ?x)
            (RightOf13 ?x)
          )
          (and
            (RightOf18 ?x)
            (LeftOf3 ?x)
          )
          (and
            (LeftOf4 ?x)
            (RightOf10 ?x)
          )
          (and
            (RightOf12 ?x)
            (LeftOf4 ?x)
          )
          (and
            (RightOf11 ?x)
            (LeftOf3 ?x)
          )
          (and
            (RightOf17 ?x)
            (LeftOf1 ?x)
          )
          (and
            (RightOf14 ?x)
            (LeftOf2 ?x)
          )
          (and
            (RightOf12 ?x)
            (LeftOf3 ?x)
          )
          (and
            (RightOf5 ?x)
            (LeftOf1 ?x)
          )
          (and
            (RightOf16 ?x)
            (LeftOf1 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf8 ?x)
          )
          (and
            (RightOf12 ?x)
            (LeftOf5 ?x)
          )
          (and
            (LeftOf4 ?x)
            (RightOf8 ?x)
          )
          (and
            (RightOf12 ?x)
            (LeftOf2 ?x)
          )
          (and
            (RightOf7 ?x)
            (LeftOf2 ?x)
          )
          (and
            (RightOf10 ?x)
            (LeftOf4 ?x)
          )
          (and
            (RightOf10 ?x)
            (LeftOf3 ?x)
          )
          (and
            (RightOf12 ?x)
            (LeftOf1 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf10 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf10 ?x)
          )
          (and
            (RightOf4 ?x)
            (LeftOf1 ?x)
          )
          (and
            (RightOf6 ?x)
            (LeftOf5 ?x)
          )
          (and
            (RightOf8 ?x)
            (LeftOf4 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf16 ?x)
          )
          (and
            (RightOf15 ?x)
            (LeftOf3 ?x)
          )
          (and
            (RightOf17 ?x)
            (LeftOf2 ?x)
          )
          (and
            (RightOf14 ?x)
            (LeftOf4 ?x)
          )
          (and
            (LeftOf1 ?x)
            (RightOf11 ?x)
          )
          (and
            (RightOf7 ?x)
            (LeftOf3 ?x)
          )
          (and
            (RightOf15 ?x)
            (LeftOf4 ?x)
          )
          (and
            (LeftOf1 ?x)
            (RightOf5 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf16 ?x)
          )
          (and
            (RightOf7 ?x)
            (LeftOf1 ?x)
          )
          (and
            (RightOf8 ?x)
            (LeftOf3 ?x)
          )
          (and
            (LeftOf1 ?x)
            (RightOf18 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf5 ?x)
          )
          (and
            (RightOf7 ?x)
            (LeftOf4 ?x)
          )
          (and
            (RightOf15 ?x)
            (LeftOf2 ?x)
          )
          (and
            (RightOf16 ?x)
            (LeftOf3 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf13 ?x)
          )
          (and
            (RightOf4 ?x)
            (LeftOf4 ?x)
          )
          (and
            (RightOf17 ?x)
            (LeftOf4 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf11 ?x)
          )
          (and
            (LeftOf1 ?x)
            (RightOf8 ?x)
          )
          (and
            (LeftOf1 ?x)
            (RightOf10 ?x)
          )
          (and
            (RightOf15 ?x)
            (LeftOf1 ?x)
          )
          (and
            (RightOf4 ?x)
            (LeftOf3 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf7 ?x)
          )
          (and
            (RightOf13 ?x)
            (LeftOf3 ?x)
          )
          (and
            (RightOf18 ?x)
            (LeftOf4 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf18 ?x)
          )
          (and
            (RightOf14 ?x)
            (LeftOf1 ?x)
          )
          (and
            (RightOf14 ?x)
            (LeftOf3 ?x)
          )
          (and
            (RightOf9 ?x)
            (LeftOf1 ?x)
          )
          (and
            (RightOf6 ?x)
            (LeftOf3 ?x)
          )
          (and
            (RightOf9 ?x)
            (LeftOf2 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf4 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf13 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf15 ?x)
          )
          (and
            (LeftOf4 ?x)
            (RightOf18 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf9 ?x)
          )
        )
        (and
          (Column5 ?x)
        )
      )
      (when
        (or
          (and
            (RightOf11 ?x)
            (LeftOf4 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf11 ?x)
          )
          (and
            (RightOf9 ?x)
            (LeftOf4 ?x)
          )
          (and
            (LeftOf3 ?x)
            (RightOf13 ?x)
          )
          (and
            (RightOf16 ?x)
            (LeftOf1 ?x)
          )
          (and
            (RightOf12 ?x)
            (LeftOf4 ?x)
          )
          (and
            (RightOf11 ?x)
            (LeftOf3 ?x)
          )
          (and
            (LeftOf1 ?x)
            (RightOf8 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf8 ?x)
          )
          (and
            (LeftOf4 ?x)
            (RightOf8 ?x)
          )
          (and
            (RightOf16 ?x)
            (LeftOf3 ?x)
          )
          (and
            (LeftOf1 ?x)
            (RightOf11 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf8 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf16 ?x)
          )
          (and
            (RightOf8 ?x)
            (LeftOf3 ?x)
          )
          (and
            (RightOf18 ?x)
            (LeftOf4 ?x)
          )
          (and
            (RightOf17 ?x)
            (LeftOf4 ?x)
          )
          (and
            (LeftOf1 ?x)
            (RightOf18 ?x)
          )
          (and
            (RightOf6 ?x)
            (LeftOf3 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf16 ?x)
          )
          (and
            (LeftOf4 ?x)
            (RightOf18 ?x)
          )
          (and
            (RightOf15 ?x)
            (LeftOf2 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf17 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf9 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf11 ?x)
          )
          (and
            (RightOf6 ?x)
            (LeftOf2 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf5 ?x)
          )
          (and
            (RightOf6 ?x)
            (LeftOf4 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf13 ?x)
          )
          (and
            (LeftOf1 ?x)
            (RightOf13 ?x)
          )
          (and
            (LeftOf4 ?x)
            (RightOf10 ?x)
          )
          (and
            (RightOf17 ?x)
            (LeftOf1 ?x)
          )
          (and
            (RightOf5 ?x)
            (LeftOf3 ?x)
          )
          (and
            (RightOf14 ?x)
            (LeftOf2 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf10 ?x)
          )
          (and
            (RightOf12 ?x)
            (LeftOf3 ?x)
          )
          (and
            (LeftOf1 ?x)
            (RightOf5 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf7 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf14 ?x)
          )
          (and
            (RightOf16 ?x)
            (LeftOf4 ?x)
          )
          (and
            (RightOf7 ?x)
            (LeftOf4 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf18 ?x)
          )
          (and
            (RightOf14 ?x)
            (LeftOf3 ?x)
          )
          (and
            (RightOf9 ?x)
            (LeftOf1 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf15 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf9 ?x)
          )
          (and
            (RightOf9 ?x)
            (LeftOf3 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf18 ?x)
          )
          (and
            (RightOf14 ?x)
            (LeftOf1 ?x)
          )
          (and
            (LeftOf4 ?x)
            (RightOf13 ?x)
          )
          (and
            (RightOf5 ?x)
            (LeftOf4 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf6 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf10 ?x)
          )
          (and
            (RightOf15 ?x)
            (LeftOf4 ?x)
          )
          (and
            (RightOf18 ?x)
            (LeftOf3 ?x)
          )
          (and
            (RightOf12 ?x)
            (LeftOf5 ?x)
          )
          (and
            (RightOf7 ?x)
            (LeftOf1 ?x)
          )
          (and
            (RightOf12 ?x)
            (LeftOf1 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf17 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf16 ?x)
          )
          (and
            (RightOf15 ?x)
            (LeftOf3 ?x)
          )
          (and
            (RightOf17 ?x)
            (LeftOf2 ?x)
          )
          (and
            (RightOf14 ?x)
            (LeftOf4 ?x)
          )
          (and
            (RightOf7 ?x)
            (LeftOf3 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf12 ?x)
          )
          (and
            (RightOf7 ?x)
            (LeftOf2 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf5 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf7 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf13 ?x)
          )
          (and
            (LeftOf1 ?x)
            (RightOf10 ?x)
          )
          (and
            (RightOf13 ?x)
            (LeftOf3 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf13 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf14 ?x)
          )
          (and
            (RightOf6 ?x)
            (LeftOf1 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf8 ?x)
          )
          (and
            (RightOf17 ?x)
            (LeftOf3 ?x)
          )
          (and
            (RightOf9 ?x)
            (LeftOf2 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf5 ?x)
          )
          (and
            (RightOf12 ?x)
            (LeftOf2 ?x)
          )
          (and
            (RightOf10 ?x)
            (LeftOf4 ?x)
          )
          (and
            (RightOf10 ?x)
            (LeftOf3 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf10 ?x)
          )
          (and
            (RightOf6 ?x)
            (LeftOf5 ?x)
          )
          (and
            (RightOf8 ?x)
            (LeftOf4 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf11 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf15 ?x)
          )
          (and
            (RightOf15 ?x)
            (LeftOf1 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf18 ?x)
          )
        )
        (and
          (Column6 ?x)
        )
      )
      (when
        (or
          (and
            (RightOf11 ?x)
            (LeftOf4 ?x)
          )
          (and
            (LeftOf7 ?x)
            (RightOf10 ?x)
          )
          (and
            (RightOf15 ?x)
            (LeftOf2 ?x)
          )
          (and
            (RightOf9 ?x)
            (LeftOf4 ?x)
          )
          (and
            (LeftOf3 ?x)
            (RightOf13 ?x)
          )
          (and
            (RightOf16 ?x)
            (LeftOf1 ?x)
          )
          (and
            (RightOf12 ?x)
            (LeftOf4 ?x)
          )
          (and
            (RightOf11 ?x)
            (LeftOf3 ?x)
          )
          (and
            (LeftOf1 ?x)
            (RightOf8 ?x)
          )
          (and
            (RightOf14 ?x)
            (LeftOf7 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf8 ?x)
          )
          (and
            (LeftOf7 ?x)
            (RightOf16 ?x)
          )
          (and
            (LeftOf4 ?x)
            (RightOf8 ?x)
          )
          (and
            (RightOf16 ?x)
            (LeftOf3 ?x)
          )
          (and
            (LeftOf1 ?x)
            (RightOf11 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf8 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf16 ?x)
          )
          (and
            (RightOf8 ?x)
            (LeftOf3 ?x)
          )
          (and
            (RightOf18 ?x)
            (LeftOf4 ?x)
          )
          (and
            (RightOf17 ?x)
            (LeftOf4 ?x)
          )
          (and
            (LeftOf1 ?x)
            (RightOf18 ?x)
          )
          (and
            (RightOf6 ?x)
            (LeftOf3 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf16 ?x)
          )
          (and
            (LeftOf4 ?x)
            (RightOf18 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf11 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf17 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf9 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf11 ?x)
          )
          (and
            (RightOf6 ?x)
            (LeftOf2 ?x)
          )
          (and
            (RightOf6 ?x)
            (LeftOf4 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf13 ?x)
          )
          (and
            (LeftOf1 ?x)
            (RightOf13 ?x)
          )
          (and
            (LeftOf4 ?x)
            (RightOf10 ?x)
          )
          (and
            (RightOf17 ?x)
            (LeftOf1 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf10 ?x)
          )
          (and
            (RightOf12 ?x)
            (LeftOf3 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf7 ?x)
          )
          (and
            (LeftOf7 ?x)
            (RightOf17 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf14 ?x)
          )
          (and
            (RightOf16 ?x)
            (LeftOf4 ?x)
          )
          (and
            (RightOf7 ?x)
            (LeftOf4 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf18 ?x)
          )
          (and
            (LeftOf7 ?x)
            (RightOf8 ?x)
          )
          (and
            (RightOf14 ?x)
            (LeftOf3 ?x)
          )
          (and
            (LeftOf7 ?x)
            (RightOf11 ?x)
          )
          (and
            (RightOf14 ?x)
            (LeftOf2 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf15 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf9 ?x)
          )
          (and
            (RightOf15 ?x)
            (LeftOf7 ?x)
          )
          (and
            (RightOf9 ?x)
            (LeftOf3 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf18 ?x)
          )
          (and
            (RightOf14 ?x)
            (LeftOf1 ?x)
          )
          (and
            (LeftOf4 ?x)
            (RightOf13 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf10 ?x)
          )
          (and
            (RightOf12 ?x)
            (LeftOf6 ?x)
          )
          (and
            (RightOf6 ?x)
            (LeftOf7 ?x)
          )
          (and
            (RightOf15 ?x)
            (LeftOf4 ?x)
          )
          (and
            (RightOf18 ?x)
            (LeftOf3 ?x)
          )
          (and
            (RightOf12 ?x)
            (LeftOf5 ?x)
          )
          (and
            (RightOf12 ?x)
            (LeftOf7 ?x)
          )
          (and
            (RightOf7 ?x)
            (LeftOf1 ?x)
          )
          (and
            (RightOf12 ?x)
            (LeftOf1 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf17 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf16 ?x)
          )
          (and
            (RightOf15 ?x)
            (LeftOf3 ?x)
          )
          (and
            (RightOf17 ?x)
            (LeftOf2 ?x)
          )
          (and
            (RightOf14 ?x)
            (LeftOf4 ?x)
          )
          (and
            (RightOf7 ?x)
            (LeftOf3 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf12 ?x)
          )
          (and
            (RightOf7 ?x)
            (LeftOf2 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf7 ?x)
          )
          (and
            (LeftOf7 ?x)
            (RightOf18 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf13 ?x)
          )
          (and
            (LeftOf1 ?x)
            (RightOf10 ?x)
          )
          (and
            (RightOf13 ?x)
            (LeftOf3 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf13 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf14 ?x)
          )
          (and
            (RightOf6 ?x)
            (LeftOf1 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf8 ?x)
          )
          (and
            (RightOf17 ?x)
            (LeftOf3 ?x)
          )
          (and
            (RightOf6 ?x)
            (LeftOf6 ?x)
          )
          (and
            (RightOf9 ?x)
            (LeftOf7 ?x)
          )
          (and
            (RightOf9 ?x)
            (LeftOf2 ?x)
          )
          (and
            (RightOf12 ?x)
            (LeftOf2 ?x)
          )
          (and
            (RightOf10 ?x)
            (LeftOf4 ?x)
          )
          (and
            (RightOf10 ?x)
            (LeftOf3 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf10 ?x)
          )
          (and
            (RightOf6 ?x)
            (LeftOf5 ?x)
          )
          (and
            (RightOf8 ?x)
            (LeftOf4 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf11 ?x)
          )
          (and
            (RightOf9 ?x)
            (LeftOf1 ?x)
          )
          (and
            (LeftOf7 ?x)
            (RightOf13 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf15 ?x)
          )
          (and
            (RightOf7 ?x)
            (LeftOf7 ?x)
          )
          (and
            (RightOf15 ?x)
            (LeftOf1 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf18 ?x)
          )
        )
        (and
          (Column7 ?x)
        )
      )
      (when
        (or
          (and
            (RightOf11 ?x)
            (LeftOf4 ?x)
          )
          (and
            (LeftOf7 ?x)
            (RightOf10 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf11 ?x)
          )
          (and
            (RightOf9 ?x)
            (LeftOf4 ?x)
          )
          (and
            (LeftOf3 ?x)
            (RightOf13 ?x)
          )
          (and
            (RightOf16 ?x)
            (LeftOf1 ?x)
          )
          (and
            (RightOf12 ?x)
            (LeftOf4 ?x)
          )
          (and
            (RightOf11 ?x)
            (LeftOf3 ?x)
          )
          (and
            (LeftOf1 ?x)
            (RightOf8 ?x)
          )
          (and
            (RightOf14 ?x)
            (LeftOf7 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf8 ?x)
          )
          (and
            (LeftOf7 ?x)
            (RightOf16 ?x)
          )
          (and
            (LeftOf4 ?x)
            (RightOf8 ?x)
          )
          (and
            (RightOf16 ?x)
            (LeftOf3 ?x)
          )
          (and
            (LeftOf8 ?x)
            (RightOf8 ?x)
          )
          (and
            (LeftOf1 ?x)
            (RightOf11 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf8 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf16 ?x)
          )
          (and
            (RightOf10 ?x)
            (LeftOf4 ?x)
          )
          (and
            (RightOf18 ?x)
            (LeftOf4 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf10 ?x)
          )
          (and
            (RightOf17 ?x)
            (LeftOf4 ?x)
          )
          (and
            (LeftOf1 ?x)
            (RightOf18 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf7 ?x)
          )
          (and
            (RightOf9 ?x)
            (LeftOf2 ?x)
          )
          (and
            (LeftOf4 ?x)
            (RightOf18 ?x)
          )
          (and
            (RightOf15 ?x)
            (LeftOf2 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf17 ?x)
          )
          (and
            (LeftOf8 ?x)
            (RightOf17 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf9 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf11 ?x)
          )
          (and
            (LeftOf8 ?x)
            (RightOf11 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf13 ?x)
          )
          (and
            (LeftOf1 ?x)
            (RightOf13 ?x)
          )
          (and
            (LeftOf4 ?x)
            (RightOf10 ?x)
          )
          (and
            (RightOf17 ?x)
            (LeftOf1 ?x)
          )
          (and
            (RightOf12 ?x)
            (LeftOf8 ?x)
          )
          (and
            (RightOf12 ?x)
            (LeftOf3 ?x)
          )
          (and
            (LeftOf7 ?x)
            (RightOf17 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf14 ?x)
          )
          (and
            (LeftOf8 ?x)
            (RightOf14 ?x)
          )
          (and
            (RightOf16 ?x)
            (LeftOf4 ?x)
          )
          (and
            (RightOf7 ?x)
            (LeftOf4 ?x)
          )
          (and
            (LeftOf8 ?x)
            (RightOf18 ?x)
          )
          (and
            (LeftOf8 ?x)
            (RightOf7 ?x)
          )
          (and
            (RightOf9 ?x)
            (LeftOf8 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf18 ?x)
          )
          (and
            (LeftOf7 ?x)
            (RightOf8 ?x)
          )
          (and
            (RightOf14 ?x)
            (LeftOf3 ?x)
          )
          (and
            (LeftOf7 ?x)
            (RightOf11 ?x)
          )
          (and
            (RightOf14 ?x)
            (LeftOf2 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf15 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf9 ?x)
          )
          (and
            (RightOf15 ?x)
            (LeftOf7 ?x)
          )
          (and
            (RightOf9 ?x)
            (LeftOf3 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf18 ?x)
          )
          (and
            (RightOf14 ?x)
            (LeftOf1 ?x)
          )
          (and
            (LeftOf4 ?x)
            (RightOf13 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf10 ?x)
          )
          (and
            (RightOf12 ?x)
            (LeftOf6 ?x)
          )
          (and
            (RightOf15 ?x)
            (LeftOf4 ?x)
          )
          (and
            (RightOf18 ?x)
            (LeftOf3 ?x)
          )
          (and
            (RightOf15 ?x)
            (LeftOf8 ?x)
          )
          (and
            (RightOf12 ?x)
            (LeftOf5 ?x)
          )
          (and
            (RightOf12 ?x)
            (LeftOf7 ?x)
          )
          (and
            (RightOf7 ?x)
            (LeftOf2 ?x)
          )
          (and
            (RightOf12 ?x)
            (LeftOf1 ?x)
          )
          (and
            (LeftOf7 ?x)
            (RightOf7 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf17 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf16 ?x)
          )
          (and
            (RightOf15 ?x)
            (LeftOf3 ?x)
          )
          (and
            (RightOf17 ?x)
            (LeftOf2 ?x)
          )
          (and
            (RightOf14 ?x)
            (LeftOf4 ?x)
          )
          (and
            (RightOf7 ?x)
            (LeftOf3 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf12 ?x)
          )
          (and
            (RightOf7 ?x)
            (LeftOf1 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf7 ?x)
          )
          (and
            (LeftOf7 ?x)
            (RightOf18 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf13 ?x)
          )
          (and
            (LeftOf8 ?x)
            (RightOf13 ?x)
          )
          (and
            (LeftOf1 ?x)
            (RightOf10 ?x)
          )
          (and
            (RightOf13 ?x)
            (LeftOf3 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf13 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf14 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf8 ?x)
          )
          (and
            (RightOf17 ?x)
            (LeftOf3 ?x)
          )
          (and
            (RightOf9 ?x)
            (LeftOf7 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf16 ?x)
          )
          (and
            (LeftOf8 ?x)
            (RightOf16 ?x)
          )
          (and
            (RightOf12 ?x)
            (LeftOf2 ?x)
          )
          (and
            (RightOf8 ?x)
            (LeftOf3 ?x)
          )
          (and
            (RightOf10 ?x)
            (LeftOf3 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf10 ?x)
          )
          (and
            (LeftOf8 ?x)
            (RightOf10 ?x)
          )
          (and
            (RightOf8 ?x)
            (LeftOf4 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf11 ?x)
          )
          (and
            (RightOf9 ?x)
            (LeftOf1 ?x)
          )
          (and
            (LeftOf7 ?x)
            (RightOf13 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf15 ?x)
          )
          (and
            (RightOf15 ?x)
            (LeftOf1 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf18 ?x)
          )
        )
        (and
          (Column8 ?x)
        )
      )
      (when
        (or
          (and
            (RightOf14 ?x)
            (LeftOf9 ?x)
          )
          (and
            (LeftOf7 ?x)
            (RightOf10 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf11 ?x)
          )
          (and
            (RightOf9 ?x)
            (LeftOf4 ?x)
          )
          (and
            (LeftOf3 ?x)
            (RightOf13 ?x)
          )
          (and
            (RightOf11 ?x)
            (LeftOf4 ?x)
          )
          (and
            (RightOf12 ?x)
            (LeftOf4 ?x)
          )
          (and
            (RightOf17 ?x)
            (LeftOf9 ?x)
          )
          (and
            (LeftOf1 ?x)
            (RightOf8 ?x)
          )
          (and
            (RightOf14 ?x)
            (LeftOf7 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf8 ?x)
          )
          (and
            (LeftOf7 ?x)
            (RightOf16 ?x)
          )
          (and
            (RightOf16 ?x)
            (LeftOf3 ?x)
          )
          (and
            (LeftOf8 ?x)
            (RightOf8 ?x)
          )
          (and
            (LeftOf1 ?x)
            (RightOf11 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf8 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf16 ?x)
          )
          (and
            (RightOf10 ?x)
            (LeftOf4 ?x)
          )
          (and
            (RightOf18 ?x)
            (LeftOf4 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf10 ?x)
          )
          (and
            (RightOf17 ?x)
            (LeftOf4 ?x)
          )
          (and
            (RightOf16 ?x)
            (LeftOf9 ?x)
          )
          (and
            (LeftOf1 ?x)
            (RightOf18 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf16 ?x)
          )
          (and
            (LeftOf4 ?x)
            (RightOf18 ?x)
          )
          (and
            (RightOf15 ?x)
            (LeftOf2 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf17 ?x)
          )
          (and
            (LeftOf8 ?x)
            (RightOf17 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf9 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf11 ?x)
          )
          (and
            (LeftOf8 ?x)
            (RightOf11 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf13 ?x)
          )
          (and
            (LeftOf1 ?x)
            (RightOf13 ?x)
          )
          (and
            (LeftOf9 ?x)
            (RightOf18 ?x)
          )
          (and
            (LeftOf4 ?x)
            (RightOf10 ?x)
          )
          (and
            (RightOf17 ?x)
            (LeftOf1 ?x)
          )
          (and
            (RightOf14 ?x)
            (LeftOf2 ?x)
          )
          (and
            (RightOf12 ?x)
            (LeftOf8 ?x)
          )
          (and
            (LeftOf9 ?x)
            (RightOf10 ?x)
          )
          (and
            (RightOf12 ?x)
            (LeftOf3 ?x)
          )
          (and
            (RightOf15 ?x)
            (LeftOf4 ?x)
          )
          (and
            (LeftOf7 ?x)
            (RightOf17 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf14 ?x)
          )
          (and
            (LeftOf8 ?x)
            (RightOf14 ?x)
          )
          (and
            (RightOf16 ?x)
            (LeftOf4 ?x)
          )
          (and
            (LeftOf8 ?x)
            (RightOf18 ?x)
          )
          (and
            (RightOf9 ?x)
            (LeftOf8 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf18 ?x)
          )
          (and
            (RightOf15 ?x)
            (LeftOf9 ?x)
          )
          (and
            (RightOf14 ?x)
            (LeftOf3 ?x)
          )
          (and
            (LeftOf7 ?x)
            (RightOf11 ?x)
          )
          (and
            (RightOf9 ?x)
            (LeftOf1 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf15 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf9 ?x)
          )
          (and
            (RightOf15 ?x)
            (LeftOf7 ?x)
          )
          (and
            (RightOf9 ?x)
            (LeftOf3 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf18 ?x)
          )
          (and
            (RightOf14 ?x)
            (LeftOf1 ?x)
          )
          (and
            (LeftOf4 ?x)
            (RightOf13 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf10 ?x)
          )
          (and
            (RightOf12 ?x)
            (LeftOf6 ?x)
          )
          (and
            (RightOf16 ?x)
            (LeftOf1 ?x)
          )
          (and
            (RightOf18 ?x)
            (LeftOf3 ?x)
          )
          (and
            (RightOf15 ?x)
            (LeftOf8 ?x)
          )
          (and
            (LeftOf9 ?x)
            (RightOf11 ?x)
          )
          (and
            (RightOf12 ?x)
            (LeftOf5 ?x)
          )
          (and
            (RightOf12 ?x)
            (LeftOf7 ?x)
          )
          (and
            (RightOf14 ?x)
            (LeftOf8 ?x)
          )
          (and
            (RightOf12 ?x)
            (LeftOf1 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf17 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf16 ?x)
          )
          (and
            (LeftOf8 ?x)
            (RightOf16 ?x)
          )
          (and
            (RightOf17 ?x)
            (LeftOf2 ?x)
          )
          (and
            (RightOf11 ?x)
            (LeftOf3 ?x)
          )
          (and
            (RightOf14 ?x)
            (LeftOf4 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf12 ?x)
          )
          (and
            (RightOf9 ?x)
            (LeftOf9 ?x)
          )
          (and
            (LeftOf7 ?x)
            (RightOf18 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf13 ?x)
          )
          (and
            (LeftOf8 ?x)
            (RightOf13 ?x)
          )
          (and
            (LeftOf1 ?x)
            (RightOf10 ?x)
          )
          (and
            (RightOf13 ?x)
            (LeftOf3 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf13 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf14 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf8 ?x)
          )
          (and
            (LeftOf7 ?x)
            (RightOf8 ?x)
          )
          (and
            (RightOf17 ?x)
            (LeftOf3 ?x)
          )
          (and
            (RightOf9 ?x)
            (LeftOf7 ?x)
          )
          (and
            (RightOf9 ?x)
            (LeftOf2 ?x)
          )
          (and
            (RightOf15 ?x)
            (LeftOf3 ?x)
          )
          (and
            (RightOf12 ?x)
            (LeftOf9 ?x)
          )
          (and
            (RightOf12 ?x)
            (LeftOf2 ?x)
          )
          (and
            (RightOf8 ?x)
            (LeftOf3 ?x)
          )
          (and
            (RightOf10 ?x)
            (LeftOf3 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf10 ?x)
          )
          (and
            (LeftOf8 ?x)
            (RightOf10 ?x)
          )
          (and
            (RightOf8 ?x)
            (LeftOf4 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf11 ?x)
          )
          (and
            (LeftOf7 ?x)
            (RightOf13 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf15 ?x)
          )
          (and
            (LeftOf9 ?x)
            (RightOf8 ?x)
          )
          (and
            (LeftOf9 ?x)
            (RightOf13 ?x)
          )
          (and
            (RightOf15 ?x)
            (LeftOf1 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf18 ?x)
          )
        )
        (and
          (Column9 ?x)
        )
      )
      (when
        (or
          (and
            (RightOf14 ?x)
            (LeftOf9 ?x)
          )
          (and
            (LeftOf7 ?x)
            (RightOf10 ?x)
          )
          (and
            (RightOf15 ?x)
            (LeftOf10 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf11 ?x)
          )
          (and
            (RightOf9 ?x)
            (LeftOf4 ?x)
          )
          (and
            (LeftOf3 ?x)
            (RightOf13 ?x)
          )
          (and
            (RightOf11 ?x)
            (LeftOf4 ?x)
          )
          (and
            (RightOf12 ?x)
            (LeftOf4 ?x)
          )
          (and
            (RightOf17 ?x)
            (LeftOf9 ?x)
          )
          (and
            (RightOf14 ?x)
            (LeftOf2 ?x)
          )
          (and
            (LeftOf10 ?x)
            (RightOf16 ?x)
          )
          (and
            (RightOf14 ?x)
            (LeftOf7 ?x)
          )
          (and
            (LeftOf7 ?x)
            (RightOf16 ?x)
          )
          (and
            (RightOf16 ?x)
            (LeftOf3 ?x)
          )
          (and
            (LeftOf1 ?x)
            (RightOf11 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf16 ?x)
          )
          (and
            (RightOf18 ?x)
            (LeftOf4 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf10 ?x)
          )
          (and
            (RightOf17 ?x)
            (LeftOf4 ?x)
          )
          (and
            (RightOf16 ?x)
            (LeftOf9 ?x)
          )
          (and
            (LeftOf10 ?x)
            (RightOf10 ?x)
          )
          (and
            (LeftOf10 ?x)
            (RightOf11 ?x)
          )
          (and
            (LeftOf1 ?x)
            (RightOf18 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf16 ?x)
          )
          (and
            (LeftOf7 ?x)
            (RightOf11 ?x)
          )
          (and
            (LeftOf4 ?x)
            (RightOf18 ?x)
          )
          (and
            (RightOf15 ?x)
            (LeftOf2 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf17 ?x)
          )
          (and
            (LeftOf8 ?x)
            (RightOf17 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf9 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf11 ?x)
          )
          (and
            (LeftOf8 ?x)
            (RightOf11 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf13 ?x)
          )
          (and
            (LeftOf1 ?x)
            (RightOf13 ?x)
          )
          (and
            (LeftOf9 ?x)
            (RightOf18 ?x)
          )
          (and
            (LeftOf4 ?x)
            (RightOf10 ?x)
          )
          (and
            (RightOf17 ?x)
            (LeftOf1 ?x)
          )
          (and
            (RightOf9 ?x)
            (LeftOf10 ?x)
          )
          (and
            (RightOf12 ?x)
            (LeftOf8 ?x)
          )
          (and
            (LeftOf9 ?x)
            (RightOf10 ?x)
          )
          (and
            (RightOf12 ?x)
            (LeftOf3 ?x)
          )
          (and
            (RightOf15 ?x)
            (LeftOf4 ?x)
          )
          (and
            (LeftOf7 ?x)
            (RightOf17 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf14 ?x)
          )
          (and
            (LeftOf8 ?x)
            (RightOf14 ?x)
          )
          (and
            (RightOf16 ?x)
            (LeftOf4 ?x)
          )
          (and
            (LeftOf8 ?x)
            (RightOf18 ?x)
          )
          (and
            (LeftOf10 ?x)
            (RightOf13 ?x)
          )
          (and
            (RightOf9 ?x)
            (LeftOf8 ?x)
          )
          (and
            (RightOf11 ?x)
            (LeftOf3 ?x)
          )
          (and
            (RightOf15 ?x)
            (LeftOf9 ?x)
          )
          (and
            (RightOf14 ?x)
            (LeftOf3 ?x)
          )
          (and
            (LeftOf10 ?x)
            (RightOf14 ?x)
          )
          (and
            (RightOf9 ?x)
            (LeftOf1 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf15 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf9 ?x)
          )
          (and
            (RightOf15 ?x)
            (LeftOf7 ?x)
          )
          (and
            (RightOf9 ?x)
            (LeftOf3 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf18 ?x)
          )
          (and
            (RightOf14 ?x)
            (LeftOf1 ?x)
          )
          (and
            (LeftOf4 ?x)
            (RightOf13 ?x)
          )
          (and
            (LeftOf10 ?x)
            (RightOf17 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf10 ?x)
          )
          (and
            (RightOf12 ?x)
            (LeftOf6 ?x)
          )
          (and
            (LeftOf10 ?x)
            (RightOf18 ?x)
          )
          (and
            (RightOf16 ?x)
            (LeftOf1 ?x)
          )
          (and
            (RightOf18 ?x)
            (LeftOf3 ?x)
          )
          (and
            (RightOf15 ?x)
            (LeftOf8 ?x)
          )
          (and
            (LeftOf9 ?x)
            (RightOf11 ?x)
          )
          (and
            (RightOf12 ?x)
            (LeftOf5 ?x)
          )
          (and
            (RightOf12 ?x)
            (LeftOf7 ?x)
          )
          (and
            (RightOf14 ?x)
            (LeftOf8 ?x)
          )
          (and
            (RightOf12 ?x)
            (LeftOf1 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf17 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf16 ?x)
          )
          (and
            (LeftOf8 ?x)
            (RightOf16 ?x)
          )
          (and
            (RightOf17 ?x)
            (LeftOf2 ?x)
          )
          (and
            (RightOf14 ?x)
            (LeftOf4 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf12 ?x)
          )
          (and
            (RightOf9 ?x)
            (LeftOf9 ?x)
          )
          (and
            (LeftOf7 ?x)
            (RightOf18 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf13 ?x)
          )
          (and
            (LeftOf8 ?x)
            (RightOf13 ?x)
          )
          (and
            (LeftOf1 ?x)
            (RightOf10 ?x)
          )
          (and
            (RightOf13 ?x)
            (LeftOf3 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf13 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf14 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf18 ?x)
          )
          (and
            (RightOf17 ?x)
            (LeftOf3 ?x)
          )
          (and
            (RightOf9 ?x)
            (LeftOf7 ?x)
          )
          (and
            (RightOf9 ?x)
            (LeftOf2 ?x)
          )
          (and
            (RightOf15 ?x)
            (LeftOf3 ?x)
          )
          (and
            (RightOf12 ?x)
            (LeftOf9 ?x)
          )
          (and
            (RightOf12 ?x)
            (LeftOf2 ?x)
          )
          (and
            (RightOf10 ?x)
            (LeftOf4 ?x)
          )
          (and
            (RightOf10 ?x)
            (LeftOf3 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf10 ?x)
          )
          (and
            (LeftOf8 ?x)
            (RightOf10 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf11 ?x)
          )
          (and
            (LeftOf7 ?x)
            (RightOf13 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf15 ?x)
          )
          (and
            (LeftOf9 ?x)
            (RightOf13 ?x)
          )
          (and
            (RightOf12 ?x)
            (LeftOf10 ?x)
          )
          (and
            (RightOf15 ?x)
            (LeftOf1 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf18 ?x)
          )
        )
        (and
          (Column10 ?x)
        )
      )
      (when
        (or
          (and
            (RightOf14 ?x)
            (LeftOf9 ?x)
          )
          (and
            (LeftOf11 ?x)
            (RightOf10 ?x)
          )
          (and
            (RightOf17 ?x)
            (LeftOf11 ?x)
          )
          (and
            (LeftOf7 ?x)
            (RightOf10 ?x)
          )
          (and
            (RightOf15 ?x)
            (LeftOf10 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf11 ?x)
          )
          (and
            (LeftOf3 ?x)
            (RightOf13 ?x)
          )
          (and
            (RightOf11 ?x)
            (LeftOf4 ?x)
          )
          (and
            (RightOf12 ?x)
            (LeftOf4 ?x)
          )
          (and
            (RightOf17 ?x)
            (LeftOf9 ?x)
          )
          (and
            (RightOf14 ?x)
            (LeftOf2 ?x)
          )
          (and
            (LeftOf10 ?x)
            (RightOf16 ?x)
          )
          (and
            (LeftOf10 ?x)
            (RightOf18 ?x)
          )
          (and
            (LeftOf7 ?x)
            (RightOf16 ?x)
          )
          (and
            (RightOf16 ?x)
            (LeftOf3 ?x)
          )
          (and
            (LeftOf1 ?x)
            (RightOf11 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf16 ?x)
          )
          (and
            (LeftOf11 ?x)
            (RightOf18 ?x)
          )
          (and
            (RightOf18 ?x)
            (LeftOf4 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf10 ?x)
          )
          (and
            (RightOf17 ?x)
            (LeftOf4 ?x)
          )
          (and
            (RightOf16 ?x)
            (LeftOf9 ?x)
          )
          (and
            (LeftOf10 ?x)
            (RightOf10 ?x)
          )
          (and
            (LeftOf10 ?x)
            (RightOf11 ?x)
          )
          (and
            (LeftOf1 ?x)
            (RightOf18 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf16 ?x)
          )
          (and
            (LeftOf4 ?x)
            (RightOf18 ?x)
          )
          (and
            (RightOf15 ?x)
            (LeftOf2 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf17 ?x)
          )
          (and
            (LeftOf8 ?x)
            (RightOf17 ?x)
          )
          (and
            (RightOf12 ?x)
            (LeftOf11 ?x)
          )
          (and
            (LeftOf8 ?x)
            (RightOf11 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf13 ?x)
          )
          (and
            (LeftOf1 ?x)
            (RightOf13 ?x)
          )
          (and
            (LeftOf9 ?x)
            (RightOf18 ?x)
          )
          (and
            (LeftOf7 ?x)
            (RightOf11 ?x)
          )
          (and
            (RightOf17 ?x)
            (LeftOf1 ?x)
          )
          (and
            (RightOf12 ?x)
            (LeftOf8 ?x)
          )
          (and
            (LeftOf9 ?x)
            (RightOf10 ?x)
          )
          (and
            (RightOf12 ?x)
            (LeftOf3 ?x)
          )
          (and
            (RightOf15 ?x)
            (LeftOf4 ?x)
          )
          (and
            (LeftOf7 ?x)
            (RightOf17 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf14 ?x)
          )
          (and
            (LeftOf8 ?x)
            (RightOf14 ?x)
          )
          (and
            (RightOf16 ?x)
            (LeftOf4 ?x)
          )
          (and
            (LeftOf8 ?x)
            (RightOf18 ?x)
          )
          (and
            (LeftOf10 ?x)
            (RightOf13 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf18 ?x)
          )
          (and
            (RightOf15 ?x)
            (LeftOf9 ?x)
          )
          (and
            (RightOf14 ?x)
            (LeftOf3 ?x)
          )
          (and
            (LeftOf10 ?x)
            (RightOf14 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf15 ?x)
          )
          (and
            (RightOf14 ?x)
            (LeftOf11 ?x)
          )
          (and
            (RightOf15 ?x)
            (LeftOf7 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf18 ?x)
          )
          (and
            (RightOf14 ?x)
            (LeftOf1 ?x)
          )
          (and
            (LeftOf4 ?x)
            (RightOf13 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf11 ?x)
          )
          (and
            (LeftOf10 ?x)
            (RightOf17 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf10 ?x)
          )
          (and
            (RightOf12 ?x)
            (LeftOf6 ?x)
          )
          (and
            (RightOf14 ?x)
            (LeftOf7 ?x)
          )
          (and
            (RightOf16 ?x)
            (LeftOf1 ?x)
          )
          (and
            (RightOf18 ?x)
            (LeftOf3 ?x)
          )
          (and
            (RightOf15 ?x)
            (LeftOf8 ?x)
          )
          (and
            (LeftOf11 ?x)
            (RightOf13 ?x)
          )
          (and
            (LeftOf9 ?x)
            (RightOf11 ?x)
          )
          (and
            (RightOf12 ?x)
            (LeftOf5 ?x)
          )
          (and
            (RightOf12 ?x)
            (LeftOf7 ?x)
          )
          (and
            (RightOf14 ?x)
            (LeftOf8 ?x)
          )
          (and
            (RightOf12 ?x)
            (LeftOf1 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf17 ?x)
          )
          (and
            (RightOf14 ?x)
            (LeftOf10 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf16 ?x)
          )
          (and
            (LeftOf8 ?x)
            (RightOf16 ?x)
          )
          (and
            (RightOf17 ?x)
            (LeftOf2 ?x)
          )
          (and
            (RightOf11 ?x)
            (LeftOf3 ?x)
          )
          (and
            (RightOf14 ?x)
            (LeftOf4 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf12 ?x)
          )
          (and
            (LeftOf7 ?x)
            (RightOf18 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf13 ?x)
          )
          (and
            (LeftOf8 ?x)
            (RightOf13 ?x)
          )
          (and
            (LeftOf1 ?x)
            (RightOf10 ?x)
          )
          (and
            (RightOf13 ?x)
            (LeftOf3 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf13 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf14 ?x)
          )
          (and
            (RightOf17 ?x)
            (LeftOf3 ?x)
          )
          (and
            (RightOf15 ?x)
            (LeftOf3 ?x)
          )
          (and
            (RightOf12 ?x)
            (LeftOf9 ?x)
          )
          (and
            (LeftOf11 ?x)
            (RightOf11 ?x)
          )
          (and
            (RightOf12 ?x)
            (LeftOf2 ?x)
          )
          (and
            (RightOf10 ?x)
            (LeftOf4 ?x)
          )
          (and
            (RightOf10 ?x)
            (LeftOf3 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf10 ?x)
          )
          (and
            (LeftOf8 ?x)
            (RightOf10 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf11 ?x)
          )
          (and
            (LeftOf7 ?x)
            (RightOf13 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf15 ?x)
          )
          (and
            (RightOf16 ?x)
            (LeftOf11 ?x)
          )
          (and
            (LeftOf9 ?x)
            (RightOf13 ?x)
          )
          (and
            (RightOf12 ?x)
            (LeftOf10 ?x)
          )
          (and
            (RightOf15 ?x)
            (LeftOf11 ?x)
          )
          (and
            (RightOf15 ?x)
            (LeftOf1 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf18 ?x)
          )
        )
        (and
          (Column11 ?x)
        )
      )
      (when
        (or
          (and
            (RightOf14 ?x)
            (LeftOf9 ?x)
          )
          (and
            (RightOf17 ?x)
            (LeftOf11 ?x)
          )
          (and
            (RightOf15 ?x)
            (LeftOf10 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf11 ?x)
          )
          (and
            (LeftOf12 ?x)
            (RightOf13 ?x)
          )
          (and
            (LeftOf3 ?x)
            (RightOf13 ?x)
          )
          (and
            (RightOf11 ?x)
            (LeftOf4 ?x)
          )
          (and
            (RightOf12 ?x)
            (LeftOf4 ?x)
          )
          (and
            (RightOf17 ?x)
            (LeftOf9 ?x)
          )
          (and
            (RightOf14 ?x)
            (LeftOf2 ?x)
          )
          (and
            (LeftOf10 ?x)
            (RightOf16 ?x)
          )
          (and
            (LeftOf10 ?x)
            (RightOf18 ?x)
          )
          (and
            (LeftOf7 ?x)
            (RightOf16 ?x)
          )
          (and
            (RightOf16 ?x)
            (LeftOf3 ?x)
          )
          (and
            (LeftOf12 ?x)
            (RightOf15 ?x)
          )
          (and
            (LeftOf1 ?x)
            (RightOf11 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf16 ?x)
          )
          (and
            (LeftOf11 ?x)
            (RightOf18 ?x)
          )
          (and
            (RightOf17 ?x)
            (LeftOf4 ?x)
          )
          (and
            (RightOf16 ?x)
            (LeftOf9 ?x)
          )
          (and
            (LeftOf10 ?x)
            (RightOf11 ?x)
          )
          (and
            (LeftOf1 ?x)
            (RightOf18 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf16 ?x)
          )
          (and
            (LeftOf4 ?x)
            (RightOf18 ?x)
          )
          (and
            (RightOf15 ?x)
            (LeftOf2 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf17 ?x)
          )
          (and
            (LeftOf8 ?x)
            (RightOf17 ?x)
          )
          (and
            (RightOf12 ?x)
            (LeftOf11 ?x)
          )
          (and
            (LeftOf8 ?x)
            (RightOf11 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf13 ?x)
          )
          (and
            (LeftOf1 ?x)
            (RightOf13 ?x)
          )
          (and
            (LeftOf9 ?x)
            (RightOf18 ?x)
          )
          (and
            (RightOf17 ?x)
            (LeftOf1 ?x)
          )
          (and
            (RightOf12 ?x)
            (LeftOf8 ?x)
          )
          (and
            (LeftOf12 ?x)
            (RightOf17 ?x)
          )
          (and
            (RightOf12 ?x)
            (LeftOf3 ?x)
          )
          (and
            (RightOf15 ?x)
            (LeftOf4 ?x)
          )
          (and
            (LeftOf7 ?x)
            (RightOf17 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf14 ?x)
          )
          (and
            (LeftOf8 ?x)
            (RightOf14 ?x)
          )
          (and
            (RightOf16 ?x)
            (LeftOf4 ?x)
          )
          (and
            (LeftOf12 ?x)
            (RightOf16 ?x)
          )
          (and
            (LeftOf8 ?x)
            (RightOf18 ?x)
          )
          (and
            (LeftOf10 ?x)
            (RightOf13 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf18 ?x)
          )
          (and
            (LeftOf12 ?x)
            (RightOf12 ?x)
          )
          (and
            (RightOf15 ?x)
            (LeftOf9 ?x)
          )
          (and
            (RightOf14 ?x)
            (LeftOf3 ?x)
          )
          (and
            (LeftOf7 ?x)
            (RightOf11 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf15 ?x)
          )
          (and
            (RightOf14 ?x)
            (LeftOf11 ?x)
          )
          (and
            (RightOf15 ?x)
            (LeftOf7 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf18 ?x)
          )
          (and
            (RightOf14 ?x)
            (LeftOf1 ?x)
          )
          (and
            (LeftOf4 ?x)
            (RightOf13 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf11 ?x)
          )
          (and
            (LeftOf10 ?x)
            (RightOf17 ?x)
          )
          (and
            (RightOf12 ?x)
            (LeftOf6 ?x)
          )
          (and
            (RightOf14 ?x)
            (LeftOf7 ?x)
          )
          (and
            (RightOf16 ?x)
            (LeftOf1 ?x)
          )
          (and
            (RightOf18 ?x)
            (LeftOf3 ?x)
          )
          (and
            (RightOf15 ?x)
            (LeftOf8 ?x)
          )
          (and
            (LeftOf11 ?x)
            (RightOf13 ?x)
          )
          (and
            (LeftOf9 ?x)
            (RightOf11 ?x)
          )
          (and
            (RightOf12 ?x)
            (LeftOf5 ?x)
          )
          (and
            (RightOf12 ?x)
            (LeftOf7 ?x)
          )
          (and
            (RightOf14 ?x)
            (LeftOf8 ?x)
          )
          (and
            (RightOf12 ?x)
            (LeftOf1 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf17 ?x)
          )
          (and
            (RightOf14 ?x)
            (LeftOf10 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf16 ?x)
          )
          (and
            (RightOf15 ?x)
            (LeftOf3 ?x)
          )
          (and
            (RightOf17 ?x)
            (LeftOf2 ?x)
          )
          (and
            (RightOf11 ?x)
            (LeftOf3 ?x)
          )
          (and
            (RightOf14 ?x)
            (LeftOf4 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf12 ?x)
          )
          (and
            (LeftOf7 ?x)
            (RightOf18 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf13 ?x)
          )
          (and
            (LeftOf8 ?x)
            (RightOf13 ?x)
          )
          (and
            (RightOf13 ?x)
            (LeftOf3 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf13 ?x)
          )
          (and
            (LeftOf12 ?x)
            (RightOf18 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf14 ?x)
          )
          (and
            (RightOf17 ?x)
            (LeftOf3 ?x)
          )
          (and
            (LeftOf8 ?x)
            (RightOf16 ?x)
          )
          (and
            (RightOf12 ?x)
            (LeftOf9 ?x)
          )
          (and
            (LeftOf11 ?x)
            (RightOf11 ?x)
          )
          (and
            (RightOf12 ?x)
            (LeftOf2 ?x)
          )
          (and
            (LeftOf10 ?x)
            (RightOf14 ?x)
          )
          (and
            (RightOf18 ?x)
            (LeftOf4 ?x)
          )
          (and
            (LeftOf12 ?x)
            (RightOf11 ?x)
          )
          (and
            (LeftOf12 ?x)
            (RightOf14 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf11 ?x)
          )
          (and
            (LeftOf7 ?x)
            (RightOf13 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf15 ?x)
          )
          (and
            (RightOf16 ?x)
            (LeftOf11 ?x)
          )
          (and
            (LeftOf9 ?x)
            (RightOf13 ?x)
          )
          (and
            (RightOf12 ?x)
            (LeftOf10 ?x)
          )
          (and
            (RightOf15 ?x)
            (LeftOf11 ?x)
          )
          (and
            (RightOf15 ?x)
            (LeftOf1 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf18 ?x)
          )
        )
        (and
          (Column12 ?x)
        )
      )
      (when
        (or
          (and
            (RightOf14 ?x)
            (LeftOf9 ?x)
          )
          (and
            (LeftOf13 ?x)
            (RightOf16 ?x)
          )
          (and
            (RightOf17 ?x)
            (LeftOf11 ?x)
          )
          (and
            (RightOf15 ?x)
            (LeftOf10 ?x)
          )
          (and
            (RightOf15 ?x)
            (LeftOf2 ?x)
          )
          (and
            (LeftOf12 ?x)
            (RightOf13 ?x)
          )
          (and
            (LeftOf3 ?x)
            (RightOf13 ?x)
          )
          (and
            (RightOf16 ?x)
            (LeftOf1 ?x)
          )
          (and
            (RightOf12 ?x)
            (LeftOf4 ?x)
          )
          (and
            (RightOf17 ?x)
            (LeftOf9 ?x)
          )
          (and
            (RightOf14 ?x)
            (LeftOf2 ?x)
          )
          (and
            (LeftOf10 ?x)
            (RightOf16 ?x)
          )
          (and
            (RightOf14 ?x)
            (LeftOf7 ?x)
          )
          (and
            (LeftOf7 ?x)
            (RightOf16 ?x)
          )
          (and
            (RightOf12 ?x)
            (LeftOf13 ?x)
          )
          (and
            (RightOf16 ?x)
            (LeftOf3 ?x)
          )
          (and
            (LeftOf12 ?x)
            (RightOf15 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf16 ?x)
          )
          (and
            (LeftOf11 ?x)
            (RightOf18 ?x)
          )
          (and
            (RightOf17 ?x)
            (LeftOf4 ?x)
          )
          (and
            (RightOf16 ?x)
            (LeftOf9 ?x)
          )
          (and
            (LeftOf1 ?x)
            (RightOf18 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf16 ?x)
          )
          (and
            (LeftOf4 ?x)
            (RightOf18 ?x)
          )
          (and
            (LeftOf13 ?x)
            (RightOf18 ?x)
          )
          (and
            (LeftOf8 ?x)
            (RightOf17 ?x)
          )
          (and
            (RightOf12 ?x)
            (LeftOf11 ?x)
          )
          (and
            (RightOf12 ?x)
            (LeftOf12 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf13 ?x)
          )
          (and
            (LeftOf1 ?x)
            (RightOf13 ?x)
          )
          (and
            (LeftOf9 ?x)
            (RightOf18 ?x)
          )
          (and
            (RightOf17 ?x)
            (LeftOf1 ?x)
          )
          (and
            (RightOf12 ?x)
            (LeftOf8 ?x)
          )
          (and
            (LeftOf12 ?x)
            (RightOf17 ?x)
          )
          (and
            (RightOf12 ?x)
            (LeftOf3 ?x)
          )
          (and
            (LeftOf7 ?x)
            (RightOf17 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf14 ?x)
          )
          (and
            (LeftOf8 ?x)
            (RightOf14 ?x)
          )
          (and
            (RightOf16 ?x)
            (LeftOf4 ?x)
          )
          (and
            (LeftOf12 ?x)
            (RightOf16 ?x)
          )
          (and
            (LeftOf8 ?x)
            (RightOf18 ?x)
          )
          (and
            (LeftOf10 ?x)
            (RightOf13 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf18 ?x)
          )
          (and
            (RightOf15 ?x)
            (LeftOf9 ?x)
          )
          (and
            (RightOf14 ?x)
            (LeftOf3 ?x)
          )
          (and
            (LeftOf10 ?x)
            (RightOf14 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf15 ?x)
          )
          (and
            (RightOf14 ?x)
            (LeftOf11 ?x)
          )
          (and
            (RightOf15 ?x)
            (LeftOf7 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf17 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf18 ?x)
          )
          (and
            (RightOf14 ?x)
            (LeftOf1 ?x)
          )
          (and
            (LeftOf4 ?x)
            (RightOf13 ?x)
          )
          (and
            (LeftOf10 ?x)
            (RightOf17 ?x)
          )
          (and
            (RightOf12 ?x)
            (LeftOf6 ?x)
          )
          (and
            (RightOf14 ?x)
            (LeftOf13 ?x)
          )
          (and
            (RightOf15 ?x)
            (LeftOf4 ?x)
          )
          (and
            (RightOf18 ?x)
            (LeftOf3 ?x)
          )
          (and
            (LeftOf9 ?x)
            (RightOf13 ?x)
          )
          (and
            (RightOf15 ?x)
            (LeftOf8 ?x)
          )
          (and
            (LeftOf11 ?x)
            (RightOf13 ?x)
          )
          (and
            (RightOf12 ?x)
            (LeftOf5 ?x)
          )
          (and
            (RightOf12 ?x)
            (LeftOf7 ?x)
          )
          (and
            (RightOf14 ?x)
            (LeftOf8 ?x)
          )
          (and
            (RightOf12 ?x)
            (LeftOf1 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf17 ?x)
          )
          (and
            (RightOf14 ?x)
            (LeftOf10 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf16 ?x)
          )
          (and
            (LeftOf8 ?x)
            (RightOf16 ?x)
          )
          (and
            (RightOf17 ?x)
            (LeftOf2 ?x)
          )
          (and
            (RightOf14 ?x)
            (LeftOf4 ?x)
          )
          (and
            (LeftOf7 ?x)
            (RightOf18 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf13 ?x)
          )
          (and
            (LeftOf8 ?x)
            (RightOf13 ?x)
          )
          (and
            (RightOf15 ?x)
            (LeftOf13 ?x)
          )
          (and
            (RightOf13 ?x)
            (LeftOf3 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf13 ?x)
          )
          (and
            (LeftOf12 ?x)
            (RightOf18 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf14 ?x)
          )
          (and
            (LeftOf13 ?x)
            (RightOf13 ?x)
          )
          (and
            (RightOf17 ?x)
            (LeftOf3 ?x)
          )
          (and
            (LeftOf10 ?x)
            (RightOf18 ?x)
          )
          (and
            (RightOf15 ?x)
            (LeftOf3 ?x)
          )
          (and
            (RightOf12 ?x)
            (LeftOf9 ?x)
          )
          (and
            (RightOf12 ?x)
            (LeftOf2 ?x)
          )
          (and
            (RightOf18 ?x)
            (LeftOf4 ?x)
          )
          (and
            (LeftOf12 ?x)
            (RightOf14 ?x)
          )
          (and
            (LeftOf7 ?x)
            (RightOf13 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf15 ?x)
          )
          (and
            (RightOf16 ?x)
            (LeftOf11 ?x)
          )
          (and
            (LeftOf13 ?x)
            (RightOf17 ?x)
          )
          (and
            (RightOf12 ?x)
            (LeftOf10 ?x)
          )
          (and
            (RightOf15 ?x)
            (LeftOf11 ?x)
          )
          (and
            (RightOf15 ?x)
            (LeftOf1 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf18 ?x)
          )
        )
        (and
          (Column13 ?x)
        )
      )
      (when
        (or
          (and
            (RightOf14 ?x)
            (LeftOf9 ?x)
          )
          (and
            (LeftOf13 ?x)
            (RightOf16 ?x)
          )
          (and
            (RightOf17 ?x)
            (LeftOf11 ?x)
          )
          (and
            (RightOf15 ?x)
            (LeftOf10 ?x)
          )
          (and
            (RightOf15 ?x)
            (LeftOf2 ?x)
          )
          (and
            (LeftOf12 ?x)
            (RightOf13 ?x)
          )
          (and
            (RightOf16 ?x)
            (LeftOf1 ?x)
          )
          (and
            (RightOf17 ?x)
            (LeftOf9 ?x)
          )
          (and
            (RightOf14 ?x)
            (LeftOf2 ?x)
          )
          (and
            (LeftOf10 ?x)
            (RightOf16 ?x)
          )
          (and
            (RightOf14 ?x)
            (LeftOf7 ?x)
          )
          (and
            (LeftOf7 ?x)
            (RightOf16 ?x)
          )
          (and
            (RightOf16 ?x)
            (LeftOf3 ?x)
          )
          (and
            (LeftOf12 ?x)
            (RightOf15 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf16 ?x)
          )
          (and
            (RightOf14 ?x)
            (LeftOf13 ?x)
          )
          (and
            (LeftOf11 ?x)
            (RightOf18 ?x)
          )
          (and
            (RightOf17 ?x)
            (LeftOf4 ?x)
          )
          (and
            (RightOf16 ?x)
            (LeftOf9 ?x)
          )
          (and
            (LeftOf1 ?x)
            (RightOf18 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf16 ?x)
          )
          (and
            (LeftOf4 ?x)
            (RightOf18 ?x)
          )
          (and
            (LeftOf13 ?x)
            (RightOf18 ?x)
          )
          (and
            (LeftOf8 ?x)
            (RightOf17 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf13 ?x)
          )
          (and
            (LeftOf1 ?x)
            (RightOf13 ?x)
          )
          (and
            (RightOf15 ?x)
            (LeftOf14 ?x)
          )
          (and
            (LeftOf9 ?x)
            (RightOf18 ?x)
          )
          (and
            (RightOf17 ?x)
            (LeftOf1 ?x)
          )
          (and
            (LeftOf14 ?x)
            (RightOf18 ?x)
          )
          (and
            (RightOf16 ?x)
            (LeftOf14 ?x)
          )
          (and
            (LeftOf12 ?x)
            (RightOf17 ?x)
          )
          (and
            (LeftOf7 ?x)
            (RightOf17 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf14 ?x)
          )
          (and
            (LeftOf8 ?x)
            (RightOf14 ?x)
          )
          (and
            (RightOf16 ?x)
            (LeftOf4 ?x)
          )
          (and
            (LeftOf12 ?x)
            (RightOf16 ?x)
          )
          (and
            (LeftOf8 ?x)
            (RightOf18 ?x)
          )
          (and
            (LeftOf10 ?x)
            (RightOf13 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf18 ?x)
          )
          (and
            (RightOf15 ?x)
            (LeftOf9 ?x)
          )
          (and
            (RightOf14 ?x)
            (LeftOf3 ?x)
          )
          (and
            (LeftOf10 ?x)
            (RightOf14 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf15 ?x)
          )
          (and
            (RightOf14 ?x)
            (LeftOf11 ?x)
          )
          (and
            (RightOf15 ?x)
            (LeftOf7 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf17 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf18 ?x)
          )
          (and
            (RightOf14 ?x)
            (LeftOf1 ?x)
          )
          (and
            (LeftOf4 ?x)
            (RightOf13 ?x)
          )
          (and
            (RightOf17 ?x)
            (LeftOf14 ?x)
          )
          (and
            (LeftOf10 ?x)
            (RightOf17 ?x)
          )
          (and
            (LeftOf10 ?x)
            (RightOf18 ?x)
          )
          (and
            (RightOf15 ?x)
            (LeftOf4 ?x)
          )
          (and
            (RightOf18 ?x)
            (LeftOf3 ?x)
          )
          (and
            (RightOf15 ?x)
            (LeftOf8 ?x)
          )
          (and
            (LeftOf11 ?x)
            (RightOf13 ?x)
          )
          (and
            (LeftOf9 ?x)
            (RightOf13 ?x)
          )
          (and
            (RightOf14 ?x)
            (LeftOf8 ?x)
          )
          (and
            (LeftOf14 ?x)
            (RightOf13 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf17 ?x)
          )
          (and
            (RightOf14 ?x)
            (LeftOf10 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf16 ?x)
          )
          (and
            (RightOf15 ?x)
            (LeftOf3 ?x)
          )
          (and
            (RightOf17 ?x)
            (LeftOf2 ?x)
          )
          (and
            (RightOf14 ?x)
            (LeftOf4 ?x)
          )
          (and
            (LeftOf7 ?x)
            (RightOf18 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf13 ?x)
          )
          (and
            (LeftOf8 ?x)
            (RightOf13 ?x)
          )
          (and
            (RightOf15 ?x)
            (LeftOf13 ?x)
          )
          (and
            (RightOf13 ?x)
            (LeftOf3 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf13 ?x)
          )
          (and
            (LeftOf12 ?x)
            (RightOf18 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf14 ?x)
          )
          (and
            (LeftOf13 ?x)
            (RightOf13 ?x)
          )
          (and
            (RightOf17 ?x)
            (LeftOf3 ?x)
          )
          (and
            (LeftOf8 ?x)
            (RightOf16 ?x)
          )
          (and
            (RightOf18 ?x)
            (LeftOf4 ?x)
          )
          (and
            (LeftOf12 ?x)
            (RightOf14 ?x)
          )
          (and
            (LeftOf7 ?x)
            (RightOf13 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf15 ?x)
          )
          (and
            (RightOf16 ?x)
            (LeftOf11 ?x)
          )
          (and
            (LeftOf13 ?x)
            (RightOf17 ?x)
          )
          (and
            (RightOf15 ?x)
            (LeftOf11 ?x)
          )
          (and
            (RightOf14 ?x)
            (LeftOf14 ?x)
          )
          (and
            (RightOf15 ?x)
            (LeftOf1 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf18 ?x)
          )
        )
        (and
          (Column14 ?x)
        )
      )
      (when
        (or
          (and
            (LeftOf13 ?x)
            (RightOf18 ?x)
          )
          (and
            (LeftOf12 ?x)
            (RightOf18 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf14 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf18 ?x)
          )
          (and
            (LeftOf15 ?x)
            (RightOf14 ?x)
          )
          (and
            (RightOf14 ?x)
            (LeftOf9 ?x)
          )
          (and
            (LeftOf8 ?x)
            (RightOf17 ?x)
          )
          (and
            (LeftOf7 ?x)
            (RightOf18 ?x)
          )
          (and
            (RightOf15 ?x)
            (LeftOf8 ?x)
          )
          (and
            (LeftOf13 ?x)
            (RightOf16 ?x)
          )
          (and
            (RightOf17 ?x)
            (LeftOf11 ?x)
          )
          (and
            (RightOf15 ?x)
            (LeftOf10 ?x)
          )
          (and
            (RightOf17 ?x)
            (LeftOf14 ?x)
          )
          (and
            (RightOf15 ?x)
            (LeftOf14 ?x)
          )
          (and
            (LeftOf10 ?x)
            (RightOf17 ?x)
          )
          (and
            (LeftOf9 ?x)
            (RightOf18 ?x)
          )
          (and
            (LeftOf10 ?x)
            (RightOf16 ?x)
          )
          (and
            (RightOf17 ?x)
            (LeftOf3 ?x)
          )
          (and
            (RightOf14 ?x)
            (LeftOf3 ?x)
          )
          (and
            (LeftOf15 ?x)
            (RightOf15 ?x)
          )
          (and
            (RightOf14 ?x)
            (LeftOf7 ?x)
          )
          (and
            (RightOf15 ?x)
            (LeftOf4 ?x)
          )
          (and
            (RightOf18 ?x)
            (LeftOf3 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf18 ?x)
          )
          (and
            (RightOf17 ?x)
            (LeftOf9 ?x)
          )
          (and
            (RightOf17 ?x)
            (LeftOf1 ?x)
          )
          (and
            (RightOf14 ?x)
            (LeftOf2 ?x)
          )
          (and
            (RightOf15 ?x)
            (LeftOf3 ?x)
          )
          (and
            (LeftOf14 ?x)
            (RightOf18 ?x)
          )
          (and
            (RightOf16 ?x)
            (LeftOf1 ?x)
          )
          (and
            (RightOf16 ?x)
            (LeftOf14 ?x)
          )
          (and
            (LeftOf10 ?x)
            (RightOf18 ?x)
          )
          (and
            (LeftOf7 ?x)
            (RightOf16 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf16 ?x)
          )
          (and
            (LeftOf15 ?x)
            (RightOf16 ?x)
          )
          (and
            (RightOf14 ?x)
            (LeftOf8 ?x)
          )
          (and
            (RightOf16 ?x)
            (LeftOf3 ?x)
          )
          (and
            (LeftOf12 ?x)
            (RightOf15 ?x)
          )
          (and
            (LeftOf15 ?x)
            (RightOf17 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf17 ?x)
          )
          (and
            (RightOf14 ?x)
            (LeftOf10 ?x)
          )
          (and
            (LeftOf12 ?x)
            (RightOf14 ?x)
          )
          (and
            (LeftOf12 ?x)
            (RightOf17 ?x)
          )
          (and
            (LeftOf8 ?x)
            (RightOf16 ?x)
          )
          (and
            (RightOf17 ?x)
            (LeftOf2 ?x)
          )
          (and
            (RightOf14 ?x)
            (LeftOf4 ?x)
          )
          (and
            (RightOf15 ?x)
            (LeftOf7 ?x)
          )
          (and
            (RightOf14 ?x)
            (LeftOf13 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf16 ?x)
          )
          (and
            (LeftOf7 ?x)
            (RightOf17 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf15 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf14 ?x)
          )
          (and
            (RightOf16 ?x)
            (LeftOf4 ?x)
          )
          (and
            (LeftOf11 ?x)
            (RightOf18 ?x)
          )
          (and
            (LeftOf12 ?x)
            (RightOf16 ?x)
          )
          (and
            (RightOf18 ?x)
            (LeftOf4 ?x)
          )
          (and
            (RightOf16 ?x)
            (LeftOf11 ?x)
          )
          (and
            (LeftOf15 ?x)
            (RightOf18 ?x)
          )
          (and
            (RightOf15 ?x)
            (LeftOf13 ?x)
          )
          (and
            (LeftOf13 ?x)
            (RightOf17 ?x)
          )
          (and
            (RightOf15 ?x)
            (LeftOf2 ?x)
          )
          (and
            (RightOf18 ?x)
            (LeftOf14 ?x)
          )
          (and
            (LeftOf8 ?x)
            (RightOf18 ?x)
          )
          (and
            (RightOf17 ?x)
            (LeftOf4 ?x)
          )
          (and
            (RightOf16 ?x)
            (LeftOf9 ?x)
          )
          (and
            (RightOf15 ?x)
            (LeftOf9 ?x)
          )
          (and
            (RightOf15 ?x)
            (LeftOf11 ?x)
          )
          (and
            (RightOf14 ?x)
            (LeftOf1 ?x)
          )
          (and
            (LeftOf1 ?x)
            (RightOf18 ?x)
          )
          (and
            (RightOf14 ?x)
            (LeftOf14 ?x)
          )
          (and
            (RightOf15 ?x)
            (LeftOf1 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf16 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf17 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf18 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf15 ?x)
          )
          (and
            (LeftOf4 ?x)
            (RightOf18 ?x)
          )
          (and
            (RightOf14 ?x)
            (LeftOf11 ?x)
          )
        )
        (and
          (Column15 ?x)
        )
      )
      (when
        (or
          (and
            (LeftOf13 ?x)
            (RightOf18 ?x)
          )
          (and
            (RightOf15 ?x)
            (LeftOf16 ?x)
          )
          (and
            (RightOf17 ?x)
            (LeftOf4 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf18 ?x)
          )
          (and
            (LeftOf8 ?x)
            (RightOf17 ?x)
          )
          (and
            (LeftOf7 ?x)
            (RightOf18 ?x)
          )
          (and
            (RightOf15 ?x)
            (LeftOf8 ?x)
          )
          (and
            (LeftOf13 ?x)
            (RightOf16 ?x)
          )
          (and
            (RightOf16 ?x)
            (LeftOf9 ?x)
          )
          (and
            (RightOf17 ?x)
            (LeftOf11 ?x)
          )
          (and
            (LeftOf13 ?x)
            (RightOf17 ?x)
          )
          (and
            (RightOf15 ?x)
            (LeftOf10 ?x)
          )
          (and
            (RightOf17 ?x)
            (LeftOf14 ?x)
          )
          (and
            (RightOf15 ?x)
            (LeftOf14 ?x)
          )
          (and
            (LeftOf10 ?x)
            (RightOf17 ?x)
          )
          (and
            (LeftOf9 ?x)
            (RightOf18 ?x)
          )
          (and
            (LeftOf10 ?x)
            (RightOf16 ?x)
          )
          (and
            (LeftOf16 ?x)
            (RightOf16 ?x)
          )
          (and
            (RightOf15 ?x)
            (LeftOf4 ?x)
          )
          (and
            (RightOf18 ?x)
            (LeftOf3 ?x)
          )
          (and
            (LeftOf15 ?x)
            (RightOf18 ?x)
          )
          (and
            (RightOf17 ?x)
            (LeftOf9 ?x)
          )
          (and
            (RightOf17 ?x)
            (LeftOf1 ?x)
          )
          (and
            (RightOf17 ?x)
            (LeftOf3 ?x)
          )
          (and
            (LeftOf8 ?x)
            (RightOf16 ?x)
          )
          (and
            (LeftOf14 ?x)
            (RightOf18 ?x)
          )
          (and
            (RightOf16 ?x)
            (LeftOf1 ?x)
          )
          (and
            (RightOf16 ?x)
            (LeftOf14 ?x)
          )
          (and
            (LeftOf10 ?x)
            (RightOf18 ?x)
          )
          (and
            (LeftOf7 ?x)
            (RightOf16 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf16 ?x)
          )
          (and
            (LeftOf15 ?x)
            (RightOf16 ?x)
          )
          (and
            (RightOf16 ?x)
            (LeftOf3 ?x)
          )
          (and
            (LeftOf12 ?x)
            (RightOf18 ?x)
          )
          (and
            (LeftOf15 ?x)
            (RightOf17 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf17 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf18 ?x)
          )
          (and
            (LeftOf12 ?x)
            (RightOf17 ?x)
          )
          (and
            (RightOf15 ?x)
            (LeftOf3 ?x)
          )
          (and
            (RightOf17 ?x)
            (LeftOf2 ?x)
          )
          (and
            (RightOf15 ?x)
            (LeftOf7 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf16 ?x)
          )
          (and
            (LeftOf7 ?x)
            (RightOf17 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf15 ?x)
          )
          (and
            (RightOf16 ?x)
            (LeftOf4 ?x)
          )
          (and
            (LeftOf11 ?x)
            (RightOf18 ?x)
          )
          (and
            (LeftOf12 ?x)
            (RightOf16 ?x)
          )
          (and
            (RightOf18 ?x)
            (LeftOf4 ?x)
          )
          (and
            (RightOf16 ?x)
            (LeftOf11 ?x)
          )
          (and
            (LeftOf12 ?x)
            (RightOf15 ?x)
          )
          (and
            (RightOf15 ?x)
            (LeftOf13 ?x)
          )
          (and
            (RightOf15 ?x)
            (LeftOf15 ?x)
          )
          (and
            (RightOf18 ?x)
            (LeftOf14 ?x)
          )
          (and
            (LeftOf8 ?x)
            (RightOf18 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf18 ?x)
          )
          (and
            (LeftOf16 ?x)
            (RightOf18 ?x)
          )
          (and
            (RightOf15 ?x)
            (LeftOf9 ?x)
          )
          (and
            (RightOf15 ?x)
            (LeftOf11 ?x)
          )
          (and
            (RightOf17 ?x)
            (LeftOf16 ?x)
          )
          (and
            (LeftOf1 ?x)
            (RightOf18 ?x)
          )
          (and
            (RightOf15 ?x)
            (LeftOf1 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf16 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf17 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf15 ?x)
          )
          (and
            (LeftOf4 ?x)
            (RightOf18 ?x)
          )
          (and
            (RightOf15 ?x)
            (LeftOf2 ?x)
          )
        )
        (and
          (Column16 ?x)
        )
      )
      (when
        (or
          (and
            (LeftOf13 ?x)
            (RightOf18 ?x)
          )
          (and
            (LeftOf12 ?x)
            (RightOf18 ?x)
          )
          (and
            (RightOf17 ?x)
            (LeftOf4 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf18 ?x)
          )
          (and
            (LeftOf8 ?x)
            (RightOf17 ?x)
          )
          (and
            (LeftOf7 ?x)
            (RightOf18 ?x)
          )
          (and
            (LeftOf13 ?x)
            (RightOf16 ?x)
          )
          (and
            (RightOf16 ?x)
            (LeftOf9 ?x)
          )
          (and
            (RightOf17 ?x)
            (LeftOf11 ?x)
          )
          (and
            (RightOf17 ?x)
            (LeftOf14 ?x)
          )
          (and
            (LeftOf10 ?x)
            (RightOf17 ?x)
          )
          (and
            (LeftOf9 ?x)
            (RightOf18 ?x)
          )
          (and
            (RightOf16 ?x)
            (LeftOf14 ?x)
          )
          (and
            (LeftOf16 ?x)
            (RightOf16 ?x)
          )
          (and
            (RightOf18 ?x)
            (LeftOf3 ?x)
          )
          (and
            (RightOf16 ?x)
            (LeftOf1 ?x)
          )
          (and
            (RightOf17 ?x)
            (LeftOf9 ?x)
          )
          (and
            (RightOf17 ?x)
            (LeftOf1 ?x)
          )
          (and
            (RightOf18 ?x)
            (LeftOf17 ?x)
          )
          (and
            (LeftOf14 ?x)
            (RightOf18 ?x)
          )
          (and
            (LeftOf10 ?x)
            (RightOf16 ?x)
          )
          (and
            (LeftOf10 ?x)
            (RightOf18 ?x)
          )
          (and
            (RightOf16 ?x)
            (LeftOf17 ?x)
          )
          (and
            (LeftOf7 ?x)
            (RightOf16 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf16 ?x)
          )
          (and
            (LeftOf15 ?x)
            (RightOf16 ?x)
          )
          (and
            (RightOf16 ?x)
            (LeftOf3 ?x)
          )
          (and
            (LeftOf15 ?x)
            (RightOf17 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf17 ?x)
          )
          (and
            (LeftOf12 ?x)
            (RightOf17 ?x)
          )
          (and
            (LeftOf8 ?x)
            (RightOf16 ?x)
          )
          (and
            (RightOf17 ?x)
            (LeftOf2 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf16 ?x)
          )
          (and
            (LeftOf7 ?x)
            (RightOf17 ?x)
          )
          (and
            (RightOf17 ?x)
            (LeftOf17 ?x)
          )
          (and
            (RightOf16 ?x)
            (LeftOf4 ?x)
          )
          (and
            (RightOf17 ?x)
            (LeftOf3 ?x)
          )
          (and
            (LeftOf11 ?x)
            (RightOf18 ?x)
          )
          (and
            (LeftOf12 ?x)
            (RightOf16 ?x)
          )
          (and
            (RightOf18 ?x)
            (LeftOf4 ?x)
          )
          (and
            (RightOf16 ?x)
            (LeftOf11 ?x)
          )
          (and
            (LeftOf15 ?x)
            (RightOf18 ?x)
          )
          (and
            (LeftOf13 ?x)
            (RightOf17 ?x)
          )
          (and
            (RightOf18 ?x)
            (LeftOf14 ?x)
          )
          (and
            (LeftOf8 ?x)
            (RightOf18 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf18 ?x)
          )
          (and
            (LeftOf16 ?x)
            (RightOf18 ?x)
          )
          (and
            (RightOf17 ?x)
            (LeftOf16 ?x)
          )
          (and
            (LeftOf1 ?x)
            (RightOf18 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf16 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf17 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf18 ?x)
          )
          (and
            (LeftOf4 ?x)
            (RightOf18 ?x)
          )
        )
        (and
          (Column17 ?x)
        )
      )
      (when
        (or
          (and
            (LeftOf13 ?x)
            (RightOf18 ?x)
          )
          (and
            (LeftOf12 ?x)
            (RightOf18 ?x)
          )
          (and
            (RightOf17 ?x)
            (LeftOf4 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf18 ?x)
          )
          (and
            (LeftOf8 ?x)
            (RightOf17 ?x)
          )
          (and
            (LeftOf18 ?x)
            (RightOf17 ?x)
          )
          (and
            (RightOf17 ?x)
            (LeftOf11 ?x)
          )
          (and
            (RightOf17 ?x)
            (LeftOf14 ?x)
          )
          (and
            (LeftOf10 ?x)
            (RightOf17 ?x)
          )
          (and
            (LeftOf9 ?x)
            (RightOf18 ?x)
          )
          (and
            (RightOf17 ?x)
            (LeftOf3 ?x)
          )
          (and
            (RightOf18 ?x)
            (LeftOf3 ?x)
          )
          (and
            (LeftOf15 ?x)
            (RightOf18 ?x)
          )
          (and
            (RightOf17 ?x)
            (LeftOf9 ?x)
          )
          (and
            (RightOf17 ?x)
            (LeftOf1 ?x)
          )
          (and
            (RightOf18 ?x)
            (LeftOf17 ?x)
          )
          (and
            (LeftOf14 ?x)
            (RightOf18 ?x)
          )
          (and
            (LeftOf10 ?x)
            (RightOf18 ?x)
          )
          (and
            (RightOf18 ?x)
            (LeftOf4 ?x)
          )
          (and
            (LeftOf15 ?x)
            (RightOf17 ?x)
          )
          (and
            (LeftOf6 ?x)
            (RightOf17 ?x)
          )
          (and
            (LeftOf12 ?x)
            (RightOf17 ?x)
          )
          (and
            (RightOf17 ?x)
            (LeftOf2 ?x)
          )
          (and
            (LeftOf7 ?x)
            (RightOf17 ?x)
          )
          (and
            (RightOf17 ?x)
            (LeftOf17 ?x)
          )
          (and
            (LeftOf8 ?x)
            (RightOf18 ?x)
          )
          (and
            (LeftOf18 ?x)
            (RightOf18 ?x)
          )
          (and
            (LeftOf11 ?x)
            (RightOf18 ?x)
          )
          (and
            (LeftOf7 ?x)
            (RightOf18 ?x)
          )
          (and
            (LeftOf13 ?x)
            (RightOf17 ?x)
          )
          (and
            (RightOf18 ?x)
            (LeftOf14 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf18 ?x)
          )
          (and
            (LeftOf16 ?x)
            (RightOf18 ?x)
          )
          (and
            (RightOf17 ?x)
            (LeftOf16 ?x)
          )
          (and
            (LeftOf1 ?x)
            (RightOf18 ?x)
          )
          (and
            (LeftOf5 ?x)
            (RightOf17 ?x)
          )
          (and
            (LeftOf2 ?x)
            (RightOf18 ?x)
          )
          (and
            (LeftOf4 ?x)
            (RightOf18 ?x)
          )
        )
        (and
          (Column18 ?x)
        )
      )
    )
  )
  (:action moveDown
    :parameters ( ?x )
    :precondition ( and
      (not (CheckConsistency))
      (not (Error))
      (or
        (BelowOf7 ?x)
        (AboveOf9 ?x)
        (BelowOf18 ?x)
        (BelowOf19 ?x)
        (BelowOf1 ?x)
        (BelowOf17 ?x)
        (AboveOf7 ?x)
        (BelowOf16 ?x)
        (AboveOf1 ?x)
        (AboveOf11 ?x)
        (BelowOf14 ?x)
        (BelowOf8 ?x)
        (BelowOf3 ?x)
        (AboveOf14 ?x)
        (AboveOf3 ?x)
        (BelowOf9 ?x)
        (AboveOf16 ?x)
        (BelowOf2 ?x)
        (BelowOf5 ?x)
        (BelowOf11 ?x)
        (BelowOf10 ?x)
        (AboveOf18 ?x)
        (BelowOf13 ?x)
        (AboveOf17 ?x)
        (BelowOf4 ?x)
        (AboveOf10 ?x)
        (AboveOf0 ?x)
        (AboveOf8 ?x)
        (AboveOf6 ?x)
        (AboveOf5 ?x)
        (AboveOf13 ?x)
        (Rows ?x)
        (AboveOf4 ?x)
        (BelowOf12 ?x)
        (AboveOf15 ?x)
        (AboveOf12 ?x)
        (BelowOf6 ?x)
        (BelowOf15 ?x)
        (AboveOf2 ?x)
      )
    )
    :effect ( and
      (CheckConsistency)
      (when
        (or
          (BelowOf1 ?x)
          (BelowOf2 ?x)
        )
        (and
          (BelowOf1 ?x)
        )
      )
      (when
        (or
          (BelowOf3 ?x)
          (BelowOf1 ?x)
          (BelowOf2 ?x)
        )
        (and
          (BelowOf2 ?x)
        )
      )
      (when
        (or
          (BelowOf4 ?x)
          (BelowOf3 ?x)
          (BelowOf1 ?x)
          (BelowOf2 ?x)
        )
        (and
          (BelowOf3 ?x)
        )
      )
      (when
        (or
          (BelowOf5 ?x)
          (BelowOf4 ?x)
          (BelowOf3 ?x)
          (BelowOf1 ?x)
          (BelowOf2 ?x)
        )
        (and
          (BelowOf4 ?x)
        )
      )
      (when
        (or
          (BelowOf3 ?x)
          (BelowOf4 ?x)
          (BelowOf1 ?x)
          (BelowOf2 ?x)
          (BelowOf5 ?x)
          (BelowOf6 ?x)
        )
        (and
          (BelowOf5 ?x)
        )
      )
      (when
        (or
          (BelowOf3 ?x)
          (BelowOf7 ?x)
          (BelowOf4 ?x)
          (BelowOf1 ?x)
          (BelowOf2 ?x)
          (BelowOf5 ?x)
          (BelowOf6 ?x)
        )
        (and
          (BelowOf6 ?x)
        )
      )
      (when
        (or
          (BelowOf8 ?x)
          (BelowOf3 ?x)
          (BelowOf7 ?x)
          (BelowOf4 ?x)
          (BelowOf1 ?x)
          (BelowOf2 ?x)
          (BelowOf5 ?x)
          (BelowOf6 ?x)
        )
        (and
          (BelowOf7 ?x)
        )
      )
      (when
        (or
          (BelowOf8 ?x)
          (BelowOf3 ?x)
          (BelowOf9 ?x)
          (BelowOf7 ?x)
          (BelowOf4 ?x)
          (BelowOf1 ?x)
          (BelowOf2 ?x)
          (BelowOf5 ?x)
          (BelowOf6 ?x)
        )
        (and
          (BelowOf8 ?x)
        )
      )
      (when
        (or
          (BelowOf8 ?x)
          (BelowOf3 ?x)
          (BelowOf9 ?x)
          (BelowOf7 ?x)
          (BelowOf4 ?x)
          (BelowOf1 ?x)
          (BelowOf2 ?x)
          (BelowOf5 ?x)
          (BelowOf6 ?x)
          (BelowOf10 ?x)
        )
        (and
          (BelowOf9 ?x)
        )
      )
      (when
        (or
          (BelowOf8 ?x)
          (BelowOf3 ?x)
          (BelowOf9 ?x)
          (BelowOf7 ?x)
          (BelowOf4 ?x)
          (BelowOf1 ?x)
          (BelowOf2 ?x)
          (BelowOf5 ?x)
          (BelowOf6 ?x)
          (BelowOf11 ?x)
          (BelowOf10 ?x)
        )
        (and
          (BelowOf10 ?x)
        )
      )
      (when
        (or
          (BelowOf8 ?x)
          (BelowOf3 ?x)
          (BelowOf9 ?x)
          (BelowOf7 ?x)
          (BelowOf12 ?x)
          (BelowOf4 ?x)
          (BelowOf1 ?x)
          (BelowOf2 ?x)
          (BelowOf5 ?x)
          (BelowOf6 ?x)
          (BelowOf11 ?x)
          (BelowOf10 ?x)
        )
        (and
          (BelowOf11 ?x)
        )
      )
      (when
        (or
          (BelowOf8 ?x)
          (BelowOf13 ?x)
          (BelowOf3 ?x)
          (BelowOf9 ?x)
          (BelowOf7 ?x)
          (BelowOf12 ?x)
          (BelowOf4 ?x)
          (BelowOf1 ?x)
          (BelowOf2 ?x)
          (BelowOf5 ?x)
          (BelowOf6 ?x)
          (BelowOf11 ?x)
          (BelowOf10 ?x)
        )
        (and
          (BelowOf12 ?x)
        )
      )
      (when
        (or
          (BelowOf14 ?x)
          (BelowOf8 ?x)
          (BelowOf13 ?x)
          (BelowOf3 ?x)
          (BelowOf9 ?x)
          (BelowOf7 ?x)
          (BelowOf12 ?x)
          (BelowOf4 ?x)
          (BelowOf1 ?x)
          (BelowOf2 ?x)
          (BelowOf5 ?x)
          (BelowOf6 ?x)
          (BelowOf11 ?x)
          (BelowOf10 ?x)
        )
        (and
          (BelowOf13 ?x)
        )
      )
      (when
        (or
          (BelowOf14 ?x)
          (BelowOf8 ?x)
          (BelowOf13 ?x)
          (BelowOf3 ?x)
          (BelowOf9 ?x)
          (BelowOf7 ?x)
          (BelowOf12 ?x)
          (BelowOf4 ?x)
          (BelowOf1 ?x)
          (BelowOf2 ?x)
          (BelowOf5 ?x)
          (BelowOf6 ?x)
          (BelowOf15 ?x)
          (BelowOf11 ?x)
          (BelowOf10 ?x)
        )
        (and
          (BelowOf14 ?x)
        )
      )
      (when
        (or
          (BelowOf14 ?x)
          (BelowOf8 ?x)
          (BelowOf13 ?x)
          (BelowOf3 ?x)
          (BelowOf9 ?x)
          (BelowOf7 ?x)
          (BelowOf12 ?x)
          (BelowOf4 ?x)
          (BelowOf1 ?x)
          (BelowOf2 ?x)
          (BelowOf5 ?x)
          (BelowOf11 ?x)
          (BelowOf6 ?x)
          (BelowOf15 ?x)
          (BelowOf16 ?x)
          (BelowOf10 ?x)
        )
        (and
          (BelowOf15 ?x)
        )
      )
      (when
        (or
          (BelowOf14 ?x)
          (BelowOf8 ?x)
          (BelowOf13 ?x)
          (BelowOf3 ?x)
          (BelowOf9 ?x)
          (BelowOf7 ?x)
          (BelowOf12 ?x)
          (BelowOf11 ?x)
          (BelowOf4 ?x)
          (BelowOf1 ?x)
          (BelowOf2 ?x)
          (BelowOf5 ?x)
          (BelowOf17 ?x)
          (BelowOf6 ?x)
          (BelowOf15 ?x)
          (BelowOf16 ?x)
          (BelowOf10 ?x)
        )
        (and
          (BelowOf16 ?x)
        )
      )
      (when
        (or
          (BelowOf14 ?x)
          (BelowOf8 ?x)
          (BelowOf13 ?x)
          (BelowOf3 ?x)
          (BelowOf9 ?x)
          (BelowOf7 ?x)
          (BelowOf12 ?x)
          (BelowOf18 ?x)
          (BelowOf11 ?x)
          (BelowOf4 ?x)
          (BelowOf1 ?x)
          (BelowOf2 ?x)
          (BelowOf5 ?x)
          (BelowOf17 ?x)
          (BelowOf6 ?x)
          (BelowOf15 ?x)
          (BelowOf16 ?x)
          (BelowOf10 ?x)
        )
        (and
          (BelowOf17 ?x)
        )
      )
      (when
        (or
          (BelowOf14 ?x)
          (BelowOf8 ?x)
          (BelowOf13 ?x)
          (BelowOf10 ?x)
          (BelowOf9 ?x)
          (BelowOf7 ?x)
          (BelowOf12 ?x)
          (BelowOf18 ?x)
          (BelowOf11 ?x)
          (BelowOf4 ?x)
          (BelowOf1 ?x)
          (BelowOf2 ?x)
          (BelowOf5 ?x)
          (BelowOf17 ?x)
          (BelowOf6 ?x)
          (BelowOf15 ?x)
          (BelowOf16 ?x)
          (BelowOf19 ?x)
          (BelowOf3 ?x)
        )
        (and
          (BelowOf18 ?x)
        )
      )
      (when
        (or
          (AboveOf18 ?x)
          (AboveOf1 ?x)
          (AboveOf5 ?x)
          (AboveOf14 ?x)
          (AboveOf13 ?x)
          (AboveOf3 ?x)
          (AboveOf4 ?x)
          (AboveOf16 ?x)
          (AboveOf9 ?x)
          (AboveOf8 ?x)
          (AboveOf15 ?x)
          (AboveOf17 ?x)
          (AboveOf12 ?x)
          (AboveOf7 ?x)
          (AboveOf2 ?x)
          (AboveOf10 ?x)
          (AboveOf11 ?x)
          (AboveOf6 ?x)
        )
        (and
          (AboveOf0 ?x)
          (not (AboveOf1 ?x))
        )
      )
      (when
        (or
          (AboveOf18 ?x)
          (AboveOf5 ?x)
          (AboveOf14 ?x)
          (AboveOf13 ?x)
          (AboveOf3 ?x)
          (AboveOf4 ?x)
          (AboveOf16 ?x)
          (AboveOf9 ?x)
          (AboveOf8 ?x)
          (AboveOf15 ?x)
          (AboveOf17 ?x)
          (AboveOf12 ?x)
          (AboveOf7 ?x)
          (AboveOf2 ?x)
          (AboveOf10 ?x)
          (AboveOf6 ?x)
          (AboveOf11 ?x)
        )
        (and
          (AboveOf1 ?x)
          (not (AboveOf2 ?x))
        )
      )
      (when
        (or
          (AboveOf18 ?x)
          (AboveOf5 ?x)
          (AboveOf14 ?x)
          (AboveOf13 ?x)
          (AboveOf3 ?x)
          (AboveOf4 ?x)
          (AboveOf16 ?x)
          (AboveOf9 ?x)
          (AboveOf15 ?x)
          (AboveOf17 ?x)
          (AboveOf12 ?x)
          (AboveOf7 ?x)
          (AboveOf8 ?x)
          (AboveOf10 ?x)
          (AboveOf6 ?x)
          (AboveOf11 ?x)
        )
        (and
          (AboveOf2 ?x)
          (not (AboveOf3 ?x))
        )
      )
      (when
        (or
          (AboveOf18 ?x)
          (AboveOf5 ?x)
          (AboveOf14 ?x)
          (AboveOf13 ?x)
          (AboveOf4 ?x)
          (AboveOf16 ?x)
          (AboveOf9 ?x)
          (AboveOf15 ?x)
          (AboveOf17 ?x)
          (AboveOf12 ?x)
          (AboveOf7 ?x)
          (AboveOf8 ?x)
          (AboveOf10 ?x)
          (AboveOf6 ?x)
          (AboveOf11 ?x)
        )
        (and
          (AboveOf3 ?x)
          (not (AboveOf4 ?x))
        )
      )
      (when
        (or
          (AboveOf18 ?x)
          (AboveOf5 ?x)
          (AboveOf14 ?x)
          (AboveOf13 ?x)
          (AboveOf16 ?x)
          (AboveOf9 ?x)
          (AboveOf15 ?x)
          (AboveOf17 ?x)
          (AboveOf12 ?x)
          (AboveOf10 ?x)
          (AboveOf8 ?x)
          (AboveOf7 ?x)
          (AboveOf6 ?x)
          (AboveOf11 ?x)
        )
        (and
          (AboveOf4 ?x)
          (not (AboveOf5 ?x))
        )
      )
      (when
        (or
          (AboveOf18 ?x)
          (AboveOf14 ?x)
          (AboveOf13 ?x)
          (AboveOf16 ?x)
          (AboveOf9 ?x)
          (AboveOf15 ?x)
          (AboveOf17 ?x)
          (AboveOf12 ?x)
          (AboveOf10 ?x)
          (AboveOf8 ?x)
          (AboveOf7 ?x)
          (AboveOf6 ?x)
          (AboveOf11 ?x)
        )
        (and
          (AboveOf5 ?x)
          (not (AboveOf6 ?x))
        )
      )
      (when
        (or
          (AboveOf18 ?x)
          (AboveOf14 ?x)
          (AboveOf13 ?x)
          (AboveOf16 ?x)
          (AboveOf9 ?x)
          (AboveOf15 ?x)
          (AboveOf17 ?x)
          (AboveOf12 ?x)
          (AboveOf10 ?x)
          (AboveOf8 ?x)
          (AboveOf7 ?x)
          (AboveOf11 ?x)
        )
        (and
          (AboveOf6 ?x)
          (not (AboveOf7 ?x))
        )
      )
      (when
        (or
          (AboveOf18 ?x)
          (AboveOf14 ?x)
          (AboveOf13 ?x)
          (AboveOf16 ?x)
          (AboveOf9 ?x)
          (AboveOf15 ?x)
          (AboveOf17 ?x)
          (AboveOf12 ?x)
          (AboveOf10 ?x)
          (AboveOf8 ?x)
          (AboveOf11 ?x)
        )
        (and
          (AboveOf7 ?x)
          (not (AboveOf8 ?x))
        )
      )
      (when
        (or
          (AboveOf18 ?x)
          (AboveOf14 ?x)
          (AboveOf13 ?x)
          (AboveOf16 ?x)
          (AboveOf9 ?x)
          (AboveOf15 ?x)
          (AboveOf17 ?x)
          (AboveOf12 ?x)
          (AboveOf10 ?x)
          (AboveOf11 ?x)
        )
        (and
          (AboveOf8 ?x)
          (not (AboveOf9 ?x))
        )
      )
      (when
        (or
          (AboveOf18 ?x)
          (AboveOf14 ?x)
          (AboveOf13 ?x)
          (AboveOf16 ?x)
          (AboveOf15 ?x)
          (AboveOf17 ?x)
          (AboveOf12 ?x)
          (AboveOf10 ?x)
          (AboveOf11 ?x)
        )
        (and
          (AboveOf9 ?x)
          (not (AboveOf10 ?x))
        )
      )
      (when
        (or
          (AboveOf18 ?x)
          (AboveOf14 ?x)
          (AboveOf13 ?x)
          (AboveOf16 ?x)
          (AboveOf15 ?x)
          (AboveOf17 ?x)
          (AboveOf12 ?x)
          (AboveOf11 ?x)
        )
        (and
          (AboveOf10 ?x)
          (not (AboveOf11 ?x))
        )
      )
      (when
        (or
          (AboveOf18 ?x)
          (AboveOf14 ?x)
          (AboveOf13 ?x)
          (AboveOf16 ?x)
          (AboveOf15 ?x)
          (AboveOf17 ?x)
          (AboveOf12 ?x)
        )
        (and
          (AboveOf11 ?x)
          (not (AboveOf12 ?x))
        )
      )
      (when
        (or
          (AboveOf18 ?x)
          (AboveOf14 ?x)
          (AboveOf13 ?x)
          (AboveOf16 ?x)
          (AboveOf15 ?x)
          (AboveOf17 ?x)
        )
        (and
          (AboveOf12 ?x)
          (not (AboveOf13 ?x))
        )
      )
      (when
        (or
          (AboveOf18 ?x)
          (AboveOf15 ?x)
          (AboveOf17 ?x)
          (AboveOf14 ?x)
          (AboveOf16 ?x)
        )
        (and
          (AboveOf13 ?x)
          (not (AboveOf14 ?x))
        )
      )
      (when
        (or
          (AboveOf16 ?x)
          (AboveOf15 ?x)
          (AboveOf17 ?x)
          (AboveOf18 ?x)
        )
        (and
          (AboveOf14 ?x)
          (not (AboveOf15 ?x))
        )
      )
      (when
        (or
          (AboveOf16 ?x)
          (AboveOf17 ?x)
          (AboveOf18 ?x)
        )
        (and
          (AboveOf15 ?x)
          (not (AboveOf16 ?x))
        )
      )
      (when
        (or
          (AboveOf18 ?x)
          (AboveOf17 ?x)
        )
        (and
          (AboveOf16 ?x)
          (not (AboveOf17 ?x))
        )
      )
      (when
        (AboveOf18 ?x)
        (and
          (AboveOf17 ?x)
          (not (AboveOf18 ?x))
        )
      )
      (when
        (Row1 ?x)
        (and
          (Row0 ?x)
          (not (Row1 ?x))
        )
      )
      (when
        (Row2 ?x)
        (and
          (Row1 ?x)
          (not (Row2 ?x))
        )
      )
      (when
        (Row3 ?x)
        (and
          (Row2 ?x)
          (not (Row3 ?x))
        )
      )
      (when
        (Row4 ?x)
        (and
          (Row3 ?x)
          (not (Row4 ?x))
        )
      )
      (when
        (Row5 ?x)
        (and
          (Row4 ?x)
          (not (Row5 ?x))
        )
      )
      (when
        (Row6 ?x)
        (and
          (Row5 ?x)
          (not (Row6 ?x))
        )
      )
      (when
        (Row7 ?x)
        (and
          (Row6 ?x)
          (not (Row7 ?x))
        )
      )
      (when
        (Row8 ?x)
        (and
          (Row7 ?x)
          (not (Row8 ?x))
        )
      )
      (when
        (Row9 ?x)
        (and
          (Row8 ?x)
          (not (Row9 ?x))
        )
      )
      (when
        (Row10 ?x)
        (and
          (Row9 ?x)
          (not (Row10 ?x))
        )
      )
      (when
        (Row11 ?x)
        (and
          (Row10 ?x)
          (not (Row11 ?x))
        )
      )
      (when
        (Row12 ?x)
        (and
          (Row11 ?x)
          (not (Row12 ?x))
        )
      )
      (when
        (Row13 ?x)
        (and
          (Row12 ?x)
          (not (Row13 ?x))
        )
      )
      (when
        (Row14 ?x)
        (and
          (Row13 ?x)
          (not (Row14 ?x))
        )
      )
      (when
        (Row15 ?x)
        (and
          (Row14 ?x)
          (not (Row15 ?x))
        )
      )
      (when
        (Row16 ?x)
        (and
          (Row15 ?x)
          (not (Row16 ?x))
        )
      )
      (when
        (Row17 ?x)
        (and
          (Row16 ?x)
          (not (Row17 ?x))
        )
      )
      (when
        (Row18 ?x)
        (and
          (Row17 ?x)
          (not (Row18 ?x))
        )
      )
      (when
        (or
          (and
            (BelowOf2 ?x)
            (AboveOf3 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf8 ?x)
          )
          (and
            (AboveOf15 ?x)
            (BelowOf1 ?x)
          )
          (and
            (AboveOf5 ?x)
            (BelowOf2 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf12 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf12 ?x)
          )
          (and
            (AboveOf4 ?x)
            (BelowOf1 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf8 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf10 ?x)
          )
          (and
            (AboveOf9 ?x)
            (BelowOf1 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf3 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf11 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf10 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf17 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf1 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf6 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf1 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf9 ?x)
          )
          (and
            (AboveOf2 ?x)
            (BelowOf1 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf6 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf2 ?x)
          )
          (and
            (AboveOf17 ?x)
            (BelowOf1 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf4 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf16 ?x)
          )
          (and
            (AboveOf5 ?x)
            (BelowOf1 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf2 ?x)
          )
          (and
            (AboveOf7 ?x)
            (BelowOf1 ?x)
          )
          (and
            (AboveOf16 ?x)
            (BelowOf1 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf2 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf15 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf7 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf1 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf11 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf18 ?x)
          )
          (and
            (AboveOf1 ?x)
            (BelowOf1 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf18 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf17 ?x)
          )
        )
        (and
          (Row0 ?x)
        )
      )
      (when
        (or
          (and
            (BelowOf2 ?x)
            (AboveOf3 ?x)
          )
          (and
            (AboveOf18 ?x)
            (BelowOf3 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf8 ?x)
          )
          (and
            (AboveOf15 ?x)
            (BelowOf1 ?x)
          )
          (and
            (AboveOf5 ?x)
            (BelowOf2 ?x)
          )
          (and
            (BelowOf3 ?x)
            (AboveOf11 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf3 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf12 ?x)
          )
          (and
            (BelowOf3 ?x)
            (AboveOf12 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf12 ?x)
          )
          (and
            (AboveOf4 ?x)
            (BelowOf1 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf8 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf10 ?x)
          )
          (and
            (AboveOf9 ?x)
            (BelowOf1 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf3 ?x)
          )
          (and
            (AboveOf2 ?x)
            (BelowOf2 ?x)
          )
          (and
            (AboveOf10 ?x)
            (BelowOf3 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf1 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf10 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf17 ?x)
          )
          (and
            (AboveOf4 ?x)
            (BelowOf3 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf6 ?x)
          )
          (and
            (BelowOf3 ?x)
            (AboveOf8 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf1 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf9 ?x)
          )
          (and
            (BelowOf3 ?x)
            (AboveOf3 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf6 ?x)
          )
          (and
            (AboveOf2 ?x)
            (BelowOf3 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf2 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf4 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf16 ?x)
          )
          (and
            (AboveOf15 ?x)
            (BelowOf3 ?x)
          )
          (and
            (AboveOf9 ?x)
            (BelowOf3 ?x)
          )
          (and
            (AboveOf5 ?x)
            (BelowOf1 ?x)
          )
          (and
            (AboveOf16 ?x)
            (BelowOf3 ?x)
          )
          (and
            (AboveOf7 ?x)
            (BelowOf1 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf2 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf3 ?x)
          )
          (and
            (AboveOf17 ?x)
            (BelowOf3 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf15 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf7 ?x)
          )
          (and
            (AboveOf17 ?x)
            (BelowOf1 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf11 ?x)
          )
          (and
            (AboveOf7 ?x)
            (BelowOf3 ?x)
          )
          (and
            (AboveOf2 ?x)
            (BelowOf1 ?x)
          )
          (and
            (AboveOf6 ?x)
            (BelowOf3 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf11 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf18 ?x)
          )
          (and
            (AboveOf16 ?x)
            (BelowOf1 ?x)
          )
          (and
            (AboveOf5 ?x)
            (BelowOf3 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf18 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf17 ?x)
          )
        )
        (and
          (Row1 ?x)
        )
      )
      (when
        (or
          (and
            (BelowOf2 ?x)
            (AboveOf3 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf8 ?x)
          )
          (and
            (AboveOf18 ?x)
            (BelowOf3 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf8 ?x)
          )
          (and
            (AboveOf15 ?x)
            (BelowOf1 ?x)
          )
          (and
            (AboveOf5 ?x)
            (BelowOf2 ?x)
          )
          (and
            (BelowOf3 ?x)
            (AboveOf11 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf12 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf2 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf3 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf4 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf11 ?x)
          )
          (and
            (BelowOf3 ?x)
            (AboveOf12 ?x)
          )
          (and
            (AboveOf7 ?x)
            (BelowOf4 ?x)
          )
          (and
            (AboveOf11 ?x)
            (BelowOf3 ?x)
          )
          (and
            (AboveOf4 ?x)
            (BelowOf1 ?x)
          )
          (and
            (AboveOf15 ?x)
            (BelowOf4 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf10 ?x)
          )
          (and
            (AboveOf9 ?x)
            (BelowOf1 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf3 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf10 ?x)
          )
          (and
            (AboveOf10 ?x)
            (BelowOf3 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf1 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf10 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf12 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf17 ?x)
          )
          (and
            (AboveOf4 ?x)
            (BelowOf3 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf6 ?x)
          )
          (and
            (BelowOf3 ?x)
            (AboveOf8 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf1 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf9 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf18 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf6 ?x)
          )
          (and
            (AboveOf3 ?x)
            (BelowOf3 ?x)
          )
          (and
            (AboveOf5 ?x)
            (BelowOf4 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf12 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf4 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf16 ?x)
          )
          (and
            (AboveOf15 ?x)
            (BelowOf3 ?x)
          )
          (and
            (AboveOf9 ?x)
            (BelowOf3 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf8 ?x)
          )
          (and
            (AboveOf5 ?x)
            (BelowOf1 ?x)
          )
          (and
            (AboveOf16 ?x)
            (BelowOf3 ?x)
          )
          (and
            (AboveOf7 ?x)
            (BelowOf1 ?x)
          )
          (and
            (AboveOf16 ?x)
            (BelowOf4 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf2 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf18 ?x)
          )
          (and
            (AboveOf17 ?x)
            (BelowOf3 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf15 ?x)
          )
          (and
            (AboveOf4 ?x)
            (BelowOf4 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf7 ?x)
          )
          (and
            (AboveOf17 ?x)
            (BelowOf1 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf11 ?x)
          )
          (and
            (AboveOf16 ?x)
            (BelowOf1 ?x)
          )
          (and
            (AboveOf7 ?x)
            (BelowOf3 ?x)
          )
          (and
            (AboveOf6 ?x)
            (BelowOf3 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf11 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf6 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf17 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf3 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf4 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf9 ?x)
          )
          (and
            (AboveOf5 ?x)
            (BelowOf3 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf3 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf18 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf17 ?x)
          )
        )
        (and
          (Row2 ?x)
        )
      )
      (when
        (or
          (and
            (BelowOf5 ?x)
            (AboveOf14 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf11 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf18 ?x)
          )
          (and
            (AboveOf18 ?x)
            (BelowOf3 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf8 ?x)
          )
          (and
            (AboveOf15 ?x)
            (BelowOf1 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf6 ?x)
          )
          (and
            (BelowOf3 ?x)
            (AboveOf11 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf11 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf12 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf2 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf3 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf4 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf12 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf11 ?x)
          )
          (and
            (BelowOf3 ?x)
            (AboveOf12 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf18 ?x)
          )
          (and
            (AboveOf11 ?x)
            (BelowOf3 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf9 ?x)
          )
          (and
            (AboveOf4 ?x)
            (BelowOf1 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf8 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf10 ?x)
          )
          (and
            (AboveOf9 ?x)
            (BelowOf1 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf6 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf10 ?x)
          )
          (and
            (AboveOf10 ?x)
            (BelowOf3 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf1 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf10 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf12 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf7 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf17 ?x)
          )
          (and
            (AboveOf4 ?x)
            (BelowOf3 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf5 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf6 ?x)
          )
          (and
            (BelowOf3 ?x)
            (AboveOf8 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf1 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf9 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf16 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf6 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf13 ?x)
          )
          (and
            (AboveOf5 ?x)
            (BelowOf4 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf12 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf4 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf16 ?x)
          )
          (and
            (AboveOf15 ?x)
            (BelowOf3 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf10 ?x)
          )
          (and
            (AboveOf9 ?x)
            (BelowOf3 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf8 ?x)
          )
          (and
            (AboveOf5 ?x)
            (BelowOf1 ?x)
          )
          (and
            (AboveOf16 ?x)
            (BelowOf3 ?x)
          )
          (and
            (AboveOf5 ?x)
            (BelowOf2 ?x)
          )
          (and
            (AboveOf7 ?x)
            (BelowOf1 ?x)
          )
          (and
            (AboveOf16 ?x)
            (BelowOf4 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf2 ?x)
          )
          (and
            (AboveOf15 ?x)
            (BelowOf4 ?x)
          )
          (and
            (AboveOf17 ?x)
            (BelowOf3 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf15 ?x)
          )
          (and
            (AboveOf4 ?x)
            (BelowOf4 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf7 ?x)
          )
          (and
            (AboveOf17 ?x)
            (BelowOf1 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf11 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf16 ?x)
          )
          (and
            (AboveOf16 ?x)
            (BelowOf1 ?x)
          )
          (and
            (AboveOf7 ?x)
            (BelowOf3 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf8 ?x)
          )
          (and
            (AboveOf6 ?x)
            (BelowOf3 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf15 ?x)
          )
          (and
            (AboveOf7 ?x)
            (BelowOf4 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf18 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf4 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf17 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf9 ?x)
          )
          (and
            (AboveOf5 ?x)
            (BelowOf3 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf4 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf3 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf17 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf18 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf17 ?x)
          )
        )
        (and
          (Row3 ?x)
        )
      )
      (when
        (or
          (and
            (BelowOf3 ?x)
            (AboveOf12 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf11 ?x)
          )
          (and
            (AboveOf6 ?x)
            (BelowOf3 ?x)
          )
          (and
            (AboveOf5 ?x)
            (BelowOf4 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf14 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf8 ?x)
          )
          (and
            (AboveOf7 ?x)
            (BelowOf6 ?x)
          )
          (and
            (AboveOf5 ?x)
            (BelowOf2 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf18 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf15 ?x)
          )
          (and
            (AboveOf17 ?x)
            (BelowOf1 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf6 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf4 ?x)
          )
          (and
            (AboveOf16 ?x)
            (BelowOf6 ?x)
          )
          (and
            (BelowOf3 ?x)
            (AboveOf11 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf11 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf16 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf8 ?x)
          )
          (and
            (AboveOf9 ?x)
            (BelowOf6 ?x)
          )
          (and
            (BelowOf6 ?x)
            (AboveOf8 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf12 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf7 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf6 ?x)
          )
          (and
            (BelowOf3 ?x)
            (AboveOf8 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf6 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf2 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf12 ?x)
          )
          (and
            (AboveOf7 ?x)
            (BelowOf4 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf9 ?x)
          )
          (and
            (AboveOf17 ?x)
            (BelowOf3 ?x)
          )
          (and
            (AboveOf5 ?x)
            (BelowOf6 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf11 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf16 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf6 ?x)
          )
          (and
            (AboveOf15 ?x)
            (BelowOf6 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf17 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf17 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf8 ?x)
          )
          (and
            (AboveOf15 ?x)
            (BelowOf1 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf6 ?x)
          )
          (and
            (AboveOf10 ?x)
            (BelowOf6 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf3 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf12 ?x)
          )
          (and
            (AboveOf11 ?x)
            (BelowOf3 ?x)
          )
          (and
            (AboveOf15 ?x)
            (BelowOf4 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf10 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf6 ?x)
          )
          (and
            (AboveOf17 ?x)
            (BelowOf6 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf10 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf1 ?x)
          )
          (and
            (AboveOf16 ?x)
            (BelowOf4 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf16 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf13 ?x)
          )
          (and
            (AboveOf5 ?x)
            (BelowOf1 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf3 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf5 ?x)
          )
          (and
            (BelowOf6 ?x)
            (AboveOf12 ?x)
          )
          (and
            (AboveOf7 ?x)
            (BelowOf3 ?x)
          )
          (and
            (BelowOf6 ?x)
            (AboveOf11 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf4 ?x)
          )
          (and
            (AboveOf16 ?x)
            (BelowOf1 ?x)
          )
          (and
            (AboveOf18 ?x)
            (BelowOf3 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf1 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf12 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf9 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf18 ?x)
          )
          (and
            (AboveOf9 ?x)
            (BelowOf1 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf15 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf10 ?x)
          )
          (and
            (AboveOf10 ?x)
            (BelowOf3 ?x)
          )
          (and
            (AboveOf6 ?x)
            (BelowOf6 ?x)
          )
          (and
            (AboveOf15 ?x)
            (BelowOf3 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf9 ?x)
          )
          (and
            (AboveOf18 ?x)
            (BelowOf6 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf17 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf11 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf10 ?x)
          )
          (and
            (AboveOf9 ?x)
            (BelowOf3 ?x)
          )
          (and
            (AboveOf5 ?x)
            (BelowOf5 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf17 ?x)
          )
          (and
            (AboveOf16 ?x)
            (BelowOf3 ?x)
          )
          (and
            (AboveOf7 ?x)
            (BelowOf1 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf2 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf8 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf7 ?x)
          )
          (and
            (AboveOf5 ?x)
            (BelowOf3 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf18 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf18 ?x)
          )
        )
        (and
          (Row4 ?x)
        )
      )
      (when
        (or
          (and
            (BelowOf3 ?x)
            (AboveOf12 ?x)
          )
          (and
            (BelowOf7 ?x)
            (AboveOf12 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf11 ?x)
          )
          (and
            (AboveOf6 ?x)
            (BelowOf3 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf14 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf8 ?x)
          )
          (and
            (AboveOf7 ?x)
            (BelowOf6 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf18 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf15 ?x)
          )
          (and
            (BelowOf6 ?x)
            (AboveOf6 ?x)
          )
          (and
            (AboveOf17 ?x)
            (BelowOf1 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf6 ?x)
          )
          (and
            (BelowOf7 ?x)
            (AboveOf9 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf4 ?x)
          )
          (and
            (AboveOf16 ?x)
            (BelowOf6 ?x)
          )
          (and
            (BelowOf3 ?x)
            (AboveOf11 ?x)
          )
          (and
            (BelowOf7 ?x)
            (AboveOf11 ?x)
          )
          (and
            (BelowOf7 ?x)
            (AboveOf8 ?x)
          )
          (and
            (BelowOf7 ?x)
            (AboveOf17 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf11 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf16 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf8 ?x)
          )
          (and
            (AboveOf9 ?x)
            (BelowOf6 ?x)
          )
          (and
            (BelowOf6 ?x)
            (AboveOf8 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf12 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf7 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf6 ?x)
          )
          (and
            (BelowOf3 ?x)
            (AboveOf8 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf6 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf2 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf12 ?x)
          )
          (and
            (AboveOf7 ?x)
            (BelowOf4 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf9 ?x)
          )
          (and
            (AboveOf17 ?x)
            (BelowOf3 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf11 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf16 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf6 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf7 ?x)
          )
          (and
            (AboveOf15 ?x)
            (BelowOf6 ?x)
          )
          (and
            (BelowOf7 ?x)
            (AboveOf10 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf17 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf17 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf8 ?x)
          )
          (and
            (AboveOf15 ?x)
            (BelowOf1 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf6 ?x)
          )
          (and
            (BelowOf6 ?x)
            (AboveOf18 ?x)
          )
          (and
            (AboveOf10 ?x)
            (BelowOf6 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf3 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf12 ?x)
          )
          (and
            (AboveOf11 ?x)
            (BelowOf3 ?x)
          )
          (and
            (AboveOf15 ?x)
            (BelowOf4 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf10 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf6 ?x)
          )
          (and
            (AboveOf17 ?x)
            (BelowOf6 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf10 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf1 ?x)
          )
          (and
            (AboveOf16 ?x)
            (BelowOf4 ?x)
          )
          (and
            (BelowOf6 ?x)
            (AboveOf10 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf16 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf13 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf3 ?x)
          )
          (and
            (BelowOf7 ?x)
            (AboveOf18 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf5 ?x)
          )
          (and
            (BelowOf6 ?x)
            (AboveOf12 ?x)
          )
          (and
            (AboveOf7 ?x)
            (BelowOf3 ?x)
          )
          (and
            (BelowOf7 ?x)
            (AboveOf16 ?x)
          )
          (and
            (BelowOf6 ?x)
            (AboveOf11 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf4 ?x)
          )
          (and
            (AboveOf16 ?x)
            (BelowOf1 ?x)
          )
          (and
            (AboveOf18 ?x)
            (BelowOf3 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf1 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf12 ?x)
          )
          (and
            (BelowOf7 ?x)
            (AboveOf7 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf9 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf18 ?x)
          )
          (and
            (AboveOf9 ?x)
            (BelowOf1 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf15 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf10 ?x)
          )
          (and
            (AboveOf10 ?x)
            (BelowOf3 ?x)
          )
          (and
            (AboveOf15 ?x)
            (BelowOf3 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf9 ?x)
          )
          (and
            (AboveOf15 ?x)
            (BelowOf7 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf7 ?x)
          )
          (and
            (AboveOf18 ?x)
            (BelowOf6 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf17 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf11 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf10 ?x)
          )
          (and
            (AboveOf9 ?x)
            (BelowOf3 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf17 ?x)
          )
          (and
            (AboveOf16 ?x)
            (BelowOf3 ?x)
          )
          (and
            (AboveOf7 ?x)
            (BelowOf1 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf2 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf8 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf7 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf18 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf18 ?x)
          )
          (and
            (BelowOf7 ?x)
            (AboveOf6 ?x)
          )
        )
        (and
          (Row5 ?x)
        )
      )
      (when
        (or
          (and
            (BelowOf3 ?x)
            (AboveOf12 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf7 ?x)
          )
          (and
            (BelowOf7 ?x)
            (AboveOf12 ?x)
          )
          (and
            (AboveOf16 ?x)
            (BelowOf8 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf11 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf14 ?x)
          )
          (and
            (BelowOf8 ?x)
            (AboveOf12 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf8 ?x)
          )
          (and
            (AboveOf7 ?x)
            (BelowOf6 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf18 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf15 ?x)
          )
          (and
            (AboveOf17 ?x)
            (BelowOf1 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf6 ?x)
          )
          (and
            (BelowOf7 ?x)
            (AboveOf9 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf4 ?x)
          )
          (and
            (AboveOf15 ?x)
            (BelowOf8 ?x)
          )
          (and
            (AboveOf16 ?x)
            (BelowOf6 ?x)
          )
          (and
            (BelowOf3 ?x)
            (AboveOf11 ?x)
          )
          (and
            (BelowOf7 ?x)
            (AboveOf11 ?x)
          )
          (and
            (BelowOf7 ?x)
            (AboveOf8 ?x)
          )
          (and
            (BelowOf7 ?x)
            (AboveOf17 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf11 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf16 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf8 ?x)
          )
          (and
            (AboveOf9 ?x)
            (BelowOf6 ?x)
          )
          (and
            (BelowOf6 ?x)
            (AboveOf8 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf12 ?x)
          )
          (and
            (AboveOf7 ?x)
            (BelowOf7 ?x)
          )
          (and
            (BelowOf3 ?x)
            (AboveOf8 ?x)
          )
          (and
            (BelowOf8 ?x)
            (AboveOf8 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf16 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf2 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf12 ?x)
          )
          (and
            (AboveOf7 ?x)
            (BelowOf4 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf9 ?x)
          )
          (and
            (AboveOf17 ?x)
            (BelowOf3 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf11 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf16 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf6 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf7 ?x)
          )
          (and
            (AboveOf15 ?x)
            (BelowOf6 ?x)
          )
          (and
            (AboveOf16 ?x)
            (BelowOf3 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf17 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf17 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf8 ?x)
          )
          (and
            (AboveOf15 ?x)
            (BelowOf1 ?x)
          )
          (and
            (BelowOf6 ?x)
            (AboveOf18 ?x)
          )
          (and
            (AboveOf7 ?x)
            (BelowOf8 ?x)
          )
          (and
            (AboveOf10 ?x)
            (BelowOf6 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf3 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf12 ?x)
          )
          (and
            (AboveOf11 ?x)
            (BelowOf3 ?x)
          )
          (and
            (AboveOf15 ?x)
            (BelowOf4 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf10 ?x)
          )
          (and
            (AboveOf17 ?x)
            (BelowOf6 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf10 ?x)
          )
          (and
            (AboveOf9 ?x)
            (BelowOf8 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf1 ?x)
          )
          (and
            (AboveOf16 ?x)
            (BelowOf4 ?x)
          )
          (and
            (BelowOf6 ?x)
            (AboveOf10 ?x)
          )
          (and
            (AboveOf18 ?x)
            (BelowOf8 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf13 ?x)
          )
          (and
            (AboveOf10 ?x)
            (BelowOf8 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf3 ?x)
          )
          (and
            (BelowOf7 ?x)
            (AboveOf18 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf5 ?x)
          )
          (and
            (BelowOf6 ?x)
            (AboveOf12 ?x)
          )
          (and
            (AboveOf7 ?x)
            (BelowOf3 ?x)
          )
          (and
            (BelowOf7 ?x)
            (AboveOf16 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf8 ?x)
          )
          (and
            (BelowOf6 ?x)
            (AboveOf11 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf4 ?x)
          )
          (and
            (AboveOf17 ?x)
            (BelowOf8 ?x)
          )
          (and
            (AboveOf16 ?x)
            (BelowOf1 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf8 ?x)
          )
          (and
            (AboveOf11 ?x)
            (BelowOf8 ?x)
          )
          (and
            (AboveOf18 ?x)
            (BelowOf3 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf1 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf12 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf9 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf18 ?x)
          )
          (and
            (AboveOf9 ?x)
            (BelowOf1 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf15 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf10 ?x)
          )
          (and
            (AboveOf10 ?x)
            (BelowOf3 ?x)
          )
          (and
            (AboveOf15 ?x)
            (BelowOf3 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf9 ?x)
          )
          (and
            (AboveOf15 ?x)
            (BelowOf7 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf7 ?x)
          )
          (and
            (AboveOf18 ?x)
            (BelowOf6 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf17 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf11 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf10 ?x)
          )
          (and
            (AboveOf9 ?x)
            (BelowOf3 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf17 ?x)
          )
          (and
            (BelowOf7 ?x)
            (AboveOf10 ?x)
          )
          (and
            (AboveOf7 ?x)
            (BelowOf1 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf2 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf8 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf7 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf18 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf18 ?x)
          )
        )
        (and
          (Row6 ?x)
        )
      )
      (when
        (or
          (and
            (BelowOf9 ?x)
            (AboveOf12 ?x)
          )
          (and
            (BelowOf3 ?x)
            (AboveOf12 ?x)
          )
          (and
            (BelowOf9 ?x)
            (AboveOf8 ?x)
          )
          (and
            (BelowOf9 ?x)
            (AboveOf18 ?x)
          )
          (and
            (BelowOf7 ?x)
            (AboveOf12 ?x)
          )
          (and
            (AboveOf16 ?x)
            (BelowOf8 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf11 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf14 ?x)
          )
          (and
            (BelowOf8 ?x)
            (AboveOf12 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf8 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf18 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf15 ?x)
          )
          (and
            (AboveOf17 ?x)
            (BelowOf1 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf6 ?x)
          )
          (and
            (BelowOf7 ?x)
            (AboveOf9 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf4 ?x)
          )
          (and
            (AboveOf15 ?x)
            (BelowOf8 ?x)
          )
          (and
            (AboveOf15 ?x)
            (BelowOf9 ?x)
          )
          (and
            (BelowOf3 ?x)
            (AboveOf11 ?x)
          )
          (and
            (BelowOf7 ?x)
            (AboveOf11 ?x)
          )
          (and
            (BelowOf7 ?x)
            (AboveOf8 ?x)
          )
          (and
            (BelowOf7 ?x)
            (AboveOf17 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf11 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf16 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf8 ?x)
          )
          (and
            (AboveOf9 ?x)
            (BelowOf6 ?x)
          )
          (and
            (BelowOf6 ?x)
            (AboveOf8 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf12 ?x)
          )
          (and
            (BelowOf3 ?x)
            (AboveOf8 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf16 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf2 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf12 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf9 ?x)
          )
          (and
            (AboveOf8 ?x)
            (BelowOf8 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf11 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf16 ?x)
          )
          (and
            (AboveOf16 ?x)
            (BelowOf9 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf6 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf7 ?x)
          )
          (and
            (AboveOf15 ?x)
            (BelowOf6 ?x)
          )
          (and
            (AboveOf16 ?x)
            (BelowOf3 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf17 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf17 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf8 ?x)
          )
          (and
            (AboveOf15 ?x)
            (BelowOf1 ?x)
          )
          (and
            (AboveOf9 ?x)
            (BelowOf9 ?x)
          )
          (and
            (BelowOf6 ?x)
            (AboveOf18 ?x)
          )
          (and
            (AboveOf10 ?x)
            (BelowOf6 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf3 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf12 ?x)
          )
          (and
            (AboveOf11 ?x)
            (BelowOf3 ?x)
          )
          (and
            (AboveOf15 ?x)
            (BelowOf4 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf10 ?x)
          )
          (and
            (AboveOf17 ?x)
            (BelowOf3 ?x)
          )
          (and
            (AboveOf17 ?x)
            (BelowOf6 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf10 ?x)
          )
          (and
            (AboveOf9 ?x)
            (BelowOf8 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf1 ?x)
          )
          (and
            (AboveOf16 ?x)
            (BelowOf4 ?x)
          )
          (and
            (BelowOf6 ?x)
            (AboveOf10 ?x)
          )
          (and
            (AboveOf16 ?x)
            (BelowOf6 ?x)
          )
          (and
            (AboveOf18 ?x)
            (BelowOf8 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf13 ?x)
          )
          (and
            (AboveOf10 ?x)
            (BelowOf8 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf9 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf3 ?x)
          )
          (and
            (BelowOf7 ?x)
            (AboveOf18 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf5 ?x)
          )
          (and
            (BelowOf6 ?x)
            (AboveOf12 ?x)
          )
          (and
            (BelowOf9 ?x)
            (AboveOf10 ?x)
          )
          (and
            (BelowOf7 ?x)
            (AboveOf16 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf8 ?x)
          )
          (and
            (BelowOf6 ?x)
            (AboveOf11 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf4 ?x)
          )
          (and
            (AboveOf17 ?x)
            (BelowOf8 ?x)
          )
          (and
            (AboveOf16 ?x)
            (BelowOf1 ?x)
          )
          (and
            (AboveOf12 ?x)
            (BelowOf8 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf8 ?x)
          )
          (and
            (AboveOf11 ?x)
            (BelowOf8 ?x)
          )
          (and
            (AboveOf18 ?x)
            (BelowOf3 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf1 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf12 ?x)
          )
          (and
            (BelowOf9 ?x)
            (AboveOf11 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf9 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf18 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf9 ?x)
          )
          (and
            (AboveOf9 ?x)
            (BelowOf1 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf15 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf10 ?x)
          )
          (and
            (AboveOf10 ?x)
            (BelowOf3 ?x)
          )
          (and
            (AboveOf15 ?x)
            (BelowOf3 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf9 ?x)
          )
          (and
            (AboveOf15 ?x)
            (BelowOf7 ?x)
          )
          (and
            (BelowOf9 ?x)
            (AboveOf17 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf7 ?x)
          )
          (and
            (AboveOf18 ?x)
            (BelowOf6 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf17 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf11 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf10 ?x)
          )
          (and
            (AboveOf9 ?x)
            (BelowOf3 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf17 ?x)
          )
          (and
            (BelowOf7 ?x)
            (AboveOf10 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf2 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf8 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf18 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf18 ?x)
          )
        )
        (and
          (Row7 ?x)
        )
      )
      (when
        (or
          (and
            (BelowOf9 ?x)
            (AboveOf12 ?x)
          )
          (and
            (BelowOf3 ?x)
            (AboveOf12 ?x)
          )
          (and
            (BelowOf9 ?x)
            (AboveOf18 ?x)
          )
          (and
            (BelowOf10 ?x)
            (AboveOf11 ?x)
          )
          (and
            (BelowOf7 ?x)
            (AboveOf12 ?x)
          )
          (and
            (AboveOf16 ?x)
            (BelowOf8 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf11 ?x)
          )
          (and
            (AboveOf15 ?x)
            (BelowOf1 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf14 ?x)
          )
          (and
            (AboveOf17 ?x)
            (BelowOf1 ?x)
          )
          (and
            (BelowOf8 ?x)
            (AboveOf12 ?x)
          )
          (and
            (AboveOf9 ?x)
            (BelowOf3 ?x)
          )
          (and
            (AboveOf17 ?x)
            (BelowOf10 ?x)
          )
          (and
            (AboveOf15 ?x)
            (BelowOf10 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf18 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf15 ?x)
          )
          (and
            (AboveOf9 ?x)
            (BelowOf10 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf6 ?x)
          )
          (and
            (BelowOf7 ?x)
            (AboveOf9 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf4 ?x)
          )
          (and
            (AboveOf15 ?x)
            (BelowOf8 ?x)
          )
          (and
            (AboveOf15 ?x)
            (BelowOf9 ?x)
          )
          (and
            (BelowOf3 ?x)
            (AboveOf11 ?x)
          )
          (and
            (BelowOf7 ?x)
            (AboveOf11 ?x)
          )
          (and
            (BelowOf7 ?x)
            (AboveOf17 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf11 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf16 ?x)
          )
          (and
            (AboveOf9 ?x)
            (BelowOf6 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf12 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf16 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf2 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf12 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf9 ?x)
          )
          (and
            (AboveOf16 ?x)
            (BelowOf10 ?x)
          )
          (and
            (AboveOf17 ?x)
            (BelowOf3 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf11 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf16 ?x)
          )
          (and
            (AboveOf16 ?x)
            (BelowOf9 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf6 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf7 ?x)
          )
          (and
            (AboveOf15 ?x)
            (BelowOf6 ?x)
          )
          (and
            (AboveOf16 ?x)
            (BelowOf3 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf17 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf17 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf10 ?x)
          )
          (and
            (BelowOf6 ?x)
            (AboveOf18 ?x)
          )
          (and
            (AboveOf10 ?x)
            (BelowOf6 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf3 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf12 ?x)
          )
          (and
            (AboveOf11 ?x)
            (BelowOf3 ?x)
          )
          (and
            (BelowOf9 ?x)
            (AboveOf9 ?x)
          )
          (and
            (AboveOf15 ?x)
            (BelowOf4 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf10 ?x)
          )
          (and
            (AboveOf17 ?x)
            (BelowOf6 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf10 ?x)
          )
          (and
            (AboveOf9 ?x)
            (BelowOf8 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf1 ?x)
          )
          (and
            (AboveOf16 ?x)
            (BelowOf4 ?x)
          )
          (and
            (BelowOf6 ?x)
            (AboveOf10 ?x)
          )
          (and
            (AboveOf16 ?x)
            (BelowOf6 ?x)
          )
          (and
            (AboveOf18 ?x)
            (BelowOf8 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf13 ?x)
          )
          (and
            (AboveOf10 ?x)
            (BelowOf8 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf9 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf10 ?x)
          )
          (and
            (BelowOf7 ?x)
            (AboveOf18 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf5 ?x)
          )
          (and
            (BelowOf6 ?x)
            (AboveOf12 ?x)
          )
          (and
            (BelowOf9 ?x)
            (AboveOf10 ?x)
          )
          (and
            (BelowOf7 ?x)
            (AboveOf16 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf8 ?x)
          )
          (and
            (BelowOf6 ?x)
            (AboveOf11 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf4 ?x)
          )
          (and
            (AboveOf17 ?x)
            (BelowOf8 ?x)
          )
          (and
            (AboveOf16 ?x)
            (BelowOf1 ?x)
          )
          (and
            (AboveOf12 ?x)
            (BelowOf8 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf8 ?x)
          )
          (and
            (AboveOf11 ?x)
            (BelowOf8 ?x)
          )
          (and
            (AboveOf18 ?x)
            (BelowOf3 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf1 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf12 ?x)
          )
          (and
            (BelowOf9 ?x)
            (AboveOf11 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf9 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf18 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf9 ?x)
          )
          (and
            (AboveOf9 ?x)
            (BelowOf1 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf15 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf10 ?x)
          )
          (and
            (AboveOf10 ?x)
            (BelowOf3 ?x)
          )
          (and
            (BelowOf10 ?x)
            (AboveOf12 ?x)
          )
          (and
            (AboveOf18 ?x)
            (BelowOf10 ?x)
          )
          (and
            (AboveOf15 ?x)
            (BelowOf3 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf9 ?x)
          )
          (and
            (AboveOf10 ?x)
            (BelowOf10 ?x)
          )
          (and
            (AboveOf15 ?x)
            (BelowOf7 ?x)
          )
          (and
            (BelowOf9 ?x)
            (AboveOf17 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf7 ?x)
          )
          (and
            (AboveOf18 ?x)
            (BelowOf6 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf17 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf11 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf10 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf17 ?x)
          )
          (and
            (BelowOf7 ?x)
            (AboveOf10 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf2 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf18 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf18 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf3 ?x)
          )
        )
        (and
          (Row8 ?x)
        )
      )
      (when
        (or
          (and
            (BelowOf9 ?x)
            (AboveOf12 ?x)
          )
          (and
            (BelowOf11 ?x)
            (AboveOf12 ?x)
          )
          (and
            (BelowOf3 ?x)
            (AboveOf12 ?x)
          )
          (and
            (BelowOf9 ?x)
            (AboveOf18 ?x)
          )
          (and
            (BelowOf10 ?x)
            (AboveOf11 ?x)
          )
          (and
            (BelowOf7 ?x)
            (AboveOf12 ?x)
          )
          (and
            (AboveOf16 ?x)
            (BelowOf8 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf11 ?x)
          )
          (and
            (AboveOf15 ?x)
            (BelowOf1 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf14 ?x)
          )
          (and
            (BelowOf8 ?x)
            (AboveOf12 ?x)
          )
          (and
            (AboveOf17 ?x)
            (BelowOf10 ?x)
          )
          (and
            (AboveOf15 ?x)
            (BelowOf10 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf18 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf15 ?x)
          )
          (and
            (AboveOf17 ?x)
            (BelowOf1 ?x)
          )
          (and
            (BelowOf11 ?x)
            (AboveOf11 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf6 ?x)
          )
          (and
            (AboveOf10 ?x)
            (BelowOf11 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf4 ?x)
          )
          (and
            (AboveOf15 ?x)
            (BelowOf8 ?x)
          )
          (and
            (AboveOf15 ?x)
            (BelowOf9 ?x)
          )
          (and
            (BelowOf3 ?x)
            (AboveOf11 ?x)
          )
          (and
            (BelowOf7 ?x)
            (AboveOf11 ?x)
          )
          (and
            (BelowOf7 ?x)
            (AboveOf17 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf11 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf12 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf16 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf12 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf16 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf2 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf11 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf11 ?x)
          )
          (and
            (AboveOf16 ?x)
            (BelowOf10 ?x)
          )
          (and
            (AboveOf17 ?x)
            (BelowOf3 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf11 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf16 ?x)
          )
          (and
            (AboveOf16 ?x)
            (BelowOf9 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf6 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf7 ?x)
          )
          (and
            (AboveOf15 ?x)
            (BelowOf6 ?x)
          )
          (and
            (AboveOf16 ?x)
            (BelowOf3 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf17 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf17 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf10 ?x)
          )
          (and
            (BelowOf6 ?x)
            (AboveOf18 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf3 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf12 ?x)
          )
          (and
            (AboveOf11 ?x)
            (BelowOf3 ?x)
          )
          (and
            (AboveOf15 ?x)
            (BelowOf4 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf10 ?x)
          )
          (and
            (AboveOf17 ?x)
            (BelowOf6 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf10 ?x)
          )
          (and
            (AboveOf17 ?x)
            (BelowOf11 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf1 ?x)
          )
          (and
            (AboveOf16 ?x)
            (BelowOf4 ?x)
          )
          (and
            (AboveOf15 ?x)
            (BelowOf11 ?x)
          )
          (and
            (BelowOf6 ?x)
            (AboveOf10 ?x)
          )
          (and
            (AboveOf16 ?x)
            (BelowOf6 ?x)
          )
          (and
            (AboveOf18 ?x)
            (BelowOf8 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf13 ?x)
          )
          (and
            (AboveOf10 ?x)
            (BelowOf8 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf9 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf10 ?x)
          )
          (and
            (BelowOf7 ?x)
            (AboveOf18 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf5 ?x)
          )
          (and
            (BelowOf6 ?x)
            (AboveOf12 ?x)
          )
          (and
            (BelowOf9 ?x)
            (AboveOf10 ?x)
          )
          (and
            (BelowOf7 ?x)
            (AboveOf16 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf8 ?x)
          )
          (and
            (BelowOf6 ?x)
            (AboveOf11 ?x)
          )
          (and
            (AboveOf18 ?x)
            (BelowOf11 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf4 ?x)
          )
          (and
            (AboveOf17 ?x)
            (BelowOf8 ?x)
          )
          (and
            (AboveOf16 ?x)
            (BelowOf11 ?x)
          )
          (and
            (AboveOf16 ?x)
            (BelowOf1 ?x)
          )
          (and
            (AboveOf12 ?x)
            (BelowOf8 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf8 ?x)
          )
          (and
            (AboveOf11 ?x)
            (BelowOf8 ?x)
          )
          (and
            (AboveOf18 ?x)
            (BelowOf3 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf1 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf12 ?x)
          )
          (and
            (BelowOf9 ?x)
            (AboveOf11 ?x)
          )
          (and
            (AboveOf18 ?x)
            (BelowOf10 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf18 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf9 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf15 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf10 ?x)
          )
          (and
            (AboveOf10 ?x)
            (BelowOf3 ?x)
          )
          (and
            (BelowOf10 ?x)
            (AboveOf12 ?x)
          )
          (and
            (AboveOf15 ?x)
            (BelowOf3 ?x)
          )
          (and
            (AboveOf10 ?x)
            (BelowOf10 ?x)
          )
          (and
            (AboveOf15 ?x)
            (BelowOf7 ?x)
          )
          (and
            (BelowOf9 ?x)
            (AboveOf17 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf7 ?x)
          )
          (and
            (AboveOf18 ?x)
            (BelowOf6 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf17 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf11 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf10 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf17 ?x)
          )
          (and
            (BelowOf7 ?x)
            (AboveOf10 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf2 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf18 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf18 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf3 ?x)
          )
        )
        (and
          (Row9 ?x)
        )
      )
      (when
        (or
          (and
            (BelowOf9 ?x)
            (AboveOf12 ?x)
          )
          (and
            (BelowOf11 ?x)
            (AboveOf12 ?x)
          )
          (and
            (BelowOf3 ?x)
            (AboveOf12 ?x)
          )
          (and
            (BelowOf9 ?x)
            (AboveOf18 ?x)
          )
          (and
            (BelowOf10 ?x)
            (AboveOf11 ?x)
          )
          (and
            (BelowOf7 ?x)
            (AboveOf12 ?x)
          )
          (and
            (AboveOf16 ?x)
            (BelowOf8 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf11 ?x)
          )
          (and
            (AboveOf16 ?x)
            (BelowOf12 ?x)
          )
          (and
            (AboveOf15 ?x)
            (BelowOf1 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf14 ?x)
          )
          (and
            (BelowOf8 ?x)
            (AboveOf12 ?x)
          )
          (and
            (AboveOf17 ?x)
            (BelowOf10 ?x)
          )
          (and
            (AboveOf15 ?x)
            (BelowOf10 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf18 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf15 ?x)
          )
          (and
            (BelowOf12 ?x)
            (AboveOf12 ?x)
          )
          (and
            (AboveOf17 ?x)
            (BelowOf1 ?x)
          )
          (and
            (BelowOf11 ?x)
            (AboveOf11 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf6 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf4 ?x)
          )
          (and
            (AboveOf15 ?x)
            (BelowOf8 ?x)
          )
          (and
            (AboveOf15 ?x)
            (BelowOf9 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf12 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf3 ?x)
          )
          (and
            (BelowOf7 ?x)
            (AboveOf11 ?x)
          )
          (and
            (BelowOf7 ?x)
            (AboveOf17 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf11 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf12 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf16 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf12 ?x)
          )
          (and
            (AboveOf11 ?x)
            (BelowOf12 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf16 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf2 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf11 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf11 ?x)
          )
          (and
            (AboveOf16 ?x)
            (BelowOf10 ?x)
          )
          (and
            (AboveOf17 ?x)
            (BelowOf3 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf11 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf16 ?x)
          )
          (and
            (AboveOf16 ?x)
            (BelowOf9 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf6 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf7 ?x)
          )
          (and
            (AboveOf15 ?x)
            (BelowOf6 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf17 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf17 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf10 ?x)
          )
          (and
            (BelowOf6 ?x)
            (AboveOf18 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf3 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf12 ?x)
          )
          (and
            (AboveOf17 ?x)
            (BelowOf12 ?x)
          )
          (and
            (AboveOf11 ?x)
            (BelowOf3 ?x)
          )
          (and
            (AboveOf15 ?x)
            (BelowOf4 ?x)
          )
          (and
            (AboveOf17 ?x)
            (BelowOf6 ?x)
          )
          (and
            (AboveOf17 ?x)
            (BelowOf11 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf1 ?x)
          )
          (and
            (AboveOf16 ?x)
            (BelowOf4 ?x)
          )
          (and
            (AboveOf15 ?x)
            (BelowOf11 ?x)
          )
          (and
            (AboveOf16 ?x)
            (BelowOf6 ?x)
          )
          (and
            (AboveOf18 ?x)
            (BelowOf8 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf13 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf9 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf10 ?x)
          )
          (and
            (BelowOf7 ?x)
            (AboveOf18 ?x)
          )
          (and
            (AboveOf15 ?x)
            (BelowOf12 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf5 ?x)
          )
          (and
            (BelowOf6 ?x)
            (AboveOf12 ?x)
          )
          (and
            (BelowOf7 ?x)
            (AboveOf16 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf8 ?x)
          )
          (and
            (BelowOf6 ?x)
            (AboveOf11 ?x)
          )
          (and
            (AboveOf18 ?x)
            (BelowOf11 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf4 ?x)
          )
          (and
            (AboveOf17 ?x)
            (BelowOf8 ?x)
          )
          (and
            (AboveOf16 ?x)
            (BelowOf11 ?x)
          )
          (and
            (AboveOf16 ?x)
            (BelowOf1 ?x)
          )
          (and
            (AboveOf12 ?x)
            (BelowOf8 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf8 ?x)
          )
          (and
            (AboveOf11 ?x)
            (BelowOf8 ?x)
          )
          (and
            (AboveOf18 ?x)
            (BelowOf3 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf1 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf12 ?x)
          )
          (and
            (BelowOf9 ?x)
            (AboveOf11 ?x)
          )
          (and
            (AboveOf18 ?x)
            (BelowOf10 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf18 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf9 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf15 ?x)
          )
          (and
            (BelowOf10 ?x)
            (AboveOf12 ?x)
          )
          (and
            (AboveOf15 ?x)
            (BelowOf3 ?x)
          )
          (and
            (AboveOf15 ?x)
            (BelowOf7 ?x)
          )
          (and
            (BelowOf9 ?x)
            (AboveOf17 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf12 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf7 ?x)
          )
          (and
            (AboveOf18 ?x)
            (BelowOf6 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf17 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf11 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf17 ?x)
          )
          (and
            (AboveOf16 ?x)
            (BelowOf3 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf2 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf18 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf18 ?x)
          )
          (and
            (AboveOf18 ?x)
            (BelowOf12 ?x)
          )
        )
        (and
          (Row10 ?x)
        )
      )
      (when
        (or
          (and
            (AboveOf12 ?x)
            (BelowOf12 ?x)
          )
          (and
            (BelowOf9 ?x)
            (AboveOf12 ?x)
          )
          (and
            (BelowOf11 ?x)
            (AboveOf12 ?x)
          )
          (and
            (BelowOf3 ?x)
            (AboveOf12 ?x)
          )
          (and
            (BelowOf9 ?x)
            (AboveOf18 ?x)
          )
          (and
            (BelowOf7 ?x)
            (AboveOf12 ?x)
          )
          (and
            (AboveOf16 ?x)
            (BelowOf8 ?x)
          )
          (and
            (AboveOf16 ?x)
            (BelowOf12 ?x)
          )
          (and
            (BelowOf13 ?x)
            (AboveOf12 ?x)
          )
          (and
            (AboveOf15 ?x)
            (BelowOf1 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf14 ?x)
          )
          (and
            (BelowOf13 ?x)
            (AboveOf15 ?x)
          )
          (and
            (AboveOf17 ?x)
            (BelowOf10 ?x)
          )
          (and
            (AboveOf15 ?x)
            (BelowOf10 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf18 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf15 ?x)
          )
          (and
            (AboveOf17 ?x)
            (BelowOf1 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf6 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf4 ?x)
          )
          (and
            (AboveOf15 ?x)
            (BelowOf8 ?x)
          )
          (and
            (AboveOf15 ?x)
            (BelowOf9 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf12 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf3 ?x)
          )
          (and
            (BelowOf7 ?x)
            (AboveOf17 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf12 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf16 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf2 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf11 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf11 ?x)
          )
          (and
            (AboveOf16 ?x)
            (BelowOf10 ?x)
          )
          (and
            (AboveOf17 ?x)
            (BelowOf3 ?x)
          )
          (and
            (BelowOf13 ?x)
            (AboveOf14 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf16 ?x)
          )
          (and
            (AboveOf16 ?x)
            (BelowOf9 ?x)
          )
          (and
            (BelowOf13 ?x)
            (AboveOf13 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf6 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf7 ?x)
          )
          (and
            (AboveOf15 ?x)
            (BelowOf6 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf17 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf17 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf10 ?x)
          )
          (and
            (BelowOf6 ?x)
            (AboveOf18 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf3 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf12 ?x)
          )
          (and
            (AboveOf17 ?x)
            (BelowOf12 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf12 ?x)
          )
          (and
            (AboveOf15 ?x)
            (BelowOf4 ?x)
          )
          (and
            (AboveOf17 ?x)
            (BelowOf6 ?x)
          )
          (and
            (AboveOf17 ?x)
            (BelowOf11 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf1 ?x)
          )
          (and
            (BelowOf13 ?x)
            (AboveOf17 ?x)
          )
          (and
            (AboveOf16 ?x)
            (BelowOf4 ?x)
          )
          (and
            (AboveOf15 ?x)
            (BelowOf11 ?x)
          )
          (and
            (AboveOf16 ?x)
            (BelowOf6 ?x)
          )
          (and
            (AboveOf18 ?x)
            (BelowOf8 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf13 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf9 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf10 ?x)
          )
          (and
            (BelowOf7 ?x)
            (AboveOf18 ?x)
          )
          (and
            (AboveOf15 ?x)
            (BelowOf12 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf5 ?x)
          )
          (and
            (BelowOf6 ?x)
            (AboveOf12 ?x)
          )
          (and
            (BelowOf7 ?x)
            (AboveOf16 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf8 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf4 ?x)
          )
          (and
            (AboveOf17 ?x)
            (BelowOf8 ?x)
          )
          (and
            (AboveOf16 ?x)
            (BelowOf11 ?x)
          )
          (and
            (AboveOf16 ?x)
            (BelowOf1 ?x)
          )
          (and
            (AboveOf12 ?x)
            (BelowOf8 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf8 ?x)
          )
          (and
            (AboveOf18 ?x)
            (BelowOf3 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf1 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf12 ?x)
          )
          (and
            (AboveOf18 ?x)
            (BelowOf11 ?x)
          )
          (and
            (AboveOf18 ?x)
            (BelowOf10 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf18 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf9 ?x)
          )
          (and
            (BelowOf13 ?x)
            (AboveOf16 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf15 ?x)
          )
          (and
            (BelowOf10 ?x)
            (AboveOf12 ?x)
          )
          (and
            (AboveOf15 ?x)
            (BelowOf3 ?x)
          )
          (and
            (AboveOf15 ?x)
            (BelowOf7 ?x)
          )
          (and
            (BelowOf13 ?x)
            (AboveOf18 ?x)
          )
          (and
            (BelowOf9 ?x)
            (AboveOf17 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf12 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf7 ?x)
          )
          (and
            (AboveOf18 ?x)
            (BelowOf6 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf17 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf16 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf17 ?x)
          )
          (and
            (AboveOf16 ?x)
            (BelowOf3 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf2 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf18 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf18 ?x)
          )
          (and
            (AboveOf18 ?x)
            (BelowOf12 ?x)
          )
        )
        (and
          (Row11 ?x)
        )
      )
      (when
        (or
          (and
            (BelowOf9 ?x)
            (AboveOf18 ?x)
          )
          (and
            (AboveOf16 ?x)
            (BelowOf10 ?x)
          )
          (and
            (AboveOf16 ?x)
            (BelowOf8 ?x)
          )
          (and
            (AboveOf16 ?x)
            (BelowOf12 ?x)
          )
          (and
            (AboveOf15 ?x)
            (BelowOf1 ?x)
          )
          (and
            (BelowOf14 ?x)
            (AboveOf17 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf14 ?x)
          )
          (and
            (BelowOf13 ?x)
            (AboveOf15 ?x)
          )
          (and
            (AboveOf17 ?x)
            (BelowOf10 ?x)
          )
          (and
            (AboveOf15 ?x)
            (BelowOf10 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf18 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf15 ?x)
          )
          (and
            (AboveOf17 ?x)
            (BelowOf1 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf6 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf4 ?x)
          )
          (and
            (AboveOf15 ?x)
            (BelowOf8 ?x)
          )
          (and
            (AboveOf15 ?x)
            (BelowOf9 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf12 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf3 ?x)
          )
          (and
            (BelowOf7 ?x)
            (AboveOf17 ?x)
          )
          (and
            (BelowOf14 ?x)
            (AboveOf18 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf16 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf14 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf2 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf11 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf13 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf11 ?x)
          )
          (and
            (AboveOf17 ?x)
            (BelowOf3 ?x)
          )
          (and
            (BelowOf13 ?x)
            (AboveOf14 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf14 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf16 ?x)
          )
          (and
            (AboveOf16 ?x)
            (BelowOf9 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf6 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf7 ?x)
          )
          (and
            (AboveOf15 ?x)
            (BelowOf6 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf17 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf17 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf10 ?x)
          )
          (and
            (BelowOf6 ?x)
            (AboveOf18 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf3 ?x)
          )
          (and
            (AboveOf17 ?x)
            (BelowOf12 ?x)
          )
          (and
            (BelowOf14 ?x)
            (AboveOf16 ?x)
          )
          (and
            (AboveOf15 ?x)
            (BelowOf4 ?x)
          )
          (and
            (AboveOf17 ?x)
            (BelowOf6 ?x)
          )
          (and
            (AboveOf17 ?x)
            (BelowOf11 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf1 ?x)
          )
          (and
            (BelowOf13 ?x)
            (AboveOf17 ?x)
          )
          (and
            (AboveOf16 ?x)
            (BelowOf4 ?x)
          )
          (and
            (AboveOf15 ?x)
            (BelowOf11 ?x)
          )
          (and
            (AboveOf16 ?x)
            (BelowOf6 ?x)
          )
          (and
            (AboveOf18 ?x)
            (BelowOf8 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf9 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf10 ?x)
          )
          (and
            (BelowOf7 ?x)
            (AboveOf18 ?x)
          )
          (and
            (AboveOf15 ?x)
            (BelowOf12 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf5 ?x)
          )
          (and
            (BelowOf7 ?x)
            (AboveOf16 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf8 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf4 ?x)
          )
          (and
            (AboveOf17 ?x)
            (BelowOf8 ?x)
          )
          (and
            (AboveOf16 ?x)
            (BelowOf11 ?x)
          )
          (and
            (AboveOf16 ?x)
            (BelowOf1 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf8 ?x)
          )
          (and
            (AboveOf18 ?x)
            (BelowOf3 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf1 ?x)
          )
          (and
            (AboveOf18 ?x)
            (BelowOf11 ?x)
          )
          (and
            (AboveOf18 ?x)
            (BelowOf10 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf18 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf9 ?x)
          )
          (and
            (BelowOf13 ?x)
            (AboveOf16 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf15 ?x)
          )
          (and
            (AboveOf15 ?x)
            (BelowOf3 ?x)
          )
          (and
            (AboveOf15 ?x)
            (BelowOf7 ?x)
          )
          (and
            (BelowOf13 ?x)
            (AboveOf18 ?x)
          )
          (and
            (BelowOf9 ?x)
            (AboveOf17 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf12 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf7 ?x)
          )
          (and
            (AboveOf18 ?x)
            (BelowOf6 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf17 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf16 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf17 ?x)
          )
          (and
            (AboveOf16 ?x)
            (BelowOf3 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf2 ?x)
          )
          (and
            (BelowOf14 ?x)
            (AboveOf15 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf18 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf18 ?x)
          )
          (and
            (AboveOf18 ?x)
            (BelowOf12 ?x)
          )
        )
        (and
          (Row12 ?x)
        )
      )
      (when
        (or
          (and
            (BelowOf15 ?x)
            (AboveOf18 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf8 ?x)
          )
          (and
            (BelowOf15 ?x)
            (AboveOf16 ?x)
          )
          (and
            (AboveOf18 ?x)
            (BelowOf3 ?x)
          )
          (and
            (AboveOf15 ?x)
            (BelowOf8 ?x)
          )
          (and
            (AboveOf15 ?x)
            (BelowOf1 ?x)
          )
          (and
            (AboveOf15 ?x)
            (BelowOf9 ?x)
          )
          (and
            (BelowOf15 ?x)
            (AboveOf17 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf12 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf3 ?x)
          )
          (and
            (AboveOf18 ?x)
            (BelowOf11 ?x)
          )
          (and
            (BelowOf7 ?x)
            (AboveOf17 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf7 ?x)
          )
          (and
            (AboveOf17 ?x)
            (BelowOf12 ?x)
          )
          (and
            (BelowOf9 ?x)
            (AboveOf18 ?x)
          )
          (and
            (BelowOf14 ?x)
            (AboveOf16 ?x)
          )
          (and
            (AboveOf18 ?x)
            (BelowOf10 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf16 ?x)
          )
          (and
            (AboveOf15 ?x)
            (BelowOf4 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf18 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf11 ?x)
          )
          (and
            (AboveOf16 ?x)
            (BelowOf10 ?x)
          )
          (and
            (BelowOf13 ?x)
            (AboveOf16 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf15 ?x)
          )
          (and
            (AboveOf16 ?x)
            (BelowOf8 ?x)
          )
          (and
            (AboveOf17 ?x)
            (BelowOf6 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf4 ?x)
          )
          (and
            (BelowOf14 ?x)
            (AboveOf18 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf1 ?x)
          )
          (and
            (AboveOf16 ?x)
            (BelowOf12 ?x)
          )
          (and
            (AboveOf15 ?x)
            (BelowOf3 ?x)
          )
          (and
            (BelowOf13 ?x)
            (AboveOf17 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf18 ?x)
          )
          (and
            (AboveOf15 ?x)
            (BelowOf11 ?x)
          )
          (and
            (AboveOf18 ?x)
            (BelowOf8 ?x)
          )
          (and
            (BelowOf13 ?x)
            (AboveOf18 ?x)
          )
          (and
            (BelowOf9 ?x)
            (AboveOf17 ?x)
          )
          (and
            (BelowOf14 ?x)
            (AboveOf17 ?x)
          )
          (and
            (AboveOf16 ?x)
            (BelowOf6 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf14 ?x)
          )
          (and
            (AboveOf15 ?x)
            (BelowOf6 ?x)
          )
          (and
            (BelowOf13 ?x)
            (AboveOf15 ?x)
          )
          (and
            (AboveOf18 ?x)
            (BelowOf6 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf17 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf16 ?x)
          )
          (and
            (AboveOf17 ?x)
            (BelowOf10 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf9 ?x)
          )
          (and
            (AboveOf16 ?x)
            (BelowOf3 ?x)
          )
          (and
            (BelowOf13 ?x)
            (AboveOf14 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf2 ?x)
          )
          (and
            (AboveOf16 ?x)
            (BelowOf4 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf14 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf10 ?x)
          )
          (and
            (AboveOf17 ?x)
            (BelowOf3 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf15 ?x)
          )
          (and
            (AboveOf15 ?x)
            (BelowOf15 ?x)
          )
          (and
            (AboveOf17 ?x)
            (BelowOf1 ?x)
          )
          (and
            (AboveOf15 ?x)
            (BelowOf10 ?x)
          )
          (and
            (BelowOf14 ?x)
            (AboveOf15 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf16 ?x)
          )
          (and
            (BelowOf7 ?x)
            (AboveOf18 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf15 ?x)
          )
          (and
            (AboveOf16 ?x)
            (BelowOf9 ?x)
          )
          (and
            (AboveOf15 ?x)
            (BelowOf7 ?x)
          )
          (and
            (AboveOf15 ?x)
            (BelowOf12 ?x)
          )
          (and
            (BelowOf7 ?x)
            (AboveOf16 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf6 ?x)
          )
          (and
            (BelowOf6 ?x)
            (AboveOf18 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf17 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf18 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf18 ?x)
          )
          (and
            (AboveOf17 ?x)
            (BelowOf8 ?x)
          )
          (and
            (AboveOf16 ?x)
            (BelowOf11 ?x)
          )
          (and
            (AboveOf16 ?x)
            (BelowOf1 ?x)
          )
          (and
            (AboveOf17 ?x)
            (BelowOf11 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf17 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf17 ?x)
          )
          (and
            (AboveOf18 ?x)
            (BelowOf12 ?x)
          )
        )
        (and
          (Row13 ?x)
        )
      )
      (when
        (or
          (and
            (BelowOf15 ?x)
            (AboveOf18 ?x)
          )
          (and
            (AboveOf18 ?x)
            (BelowOf3 ?x)
          )
          (and
            (AboveOf15 ?x)
            (BelowOf8 ?x)
          )
          (and
            (AboveOf15 ?x)
            (BelowOf1 ?x)
          )
          (and
            (AboveOf15 ?x)
            (BelowOf9 ?x)
          )
          (and
            (BelowOf15 ?x)
            (AboveOf17 ?x)
          )
          (and
            (BelowOf16 ?x)
            (AboveOf18 ?x)
          )
          (and
            (BelowOf6 ?x)
            (AboveOf18 ?x)
          )
          (and
            (AboveOf15 ?x)
            (BelowOf6 ?x)
          )
          (and
            (AboveOf18 ?x)
            (BelowOf11 ?x)
          )
          (and
            (BelowOf7 ?x)
            (AboveOf17 ?x)
          )
          (and
            (AboveOf17 ?x)
            (BelowOf12 ?x)
          )
          (and
            (BelowOf9 ?x)
            (AboveOf18 ?x)
          )
          (and
            (BelowOf14 ?x)
            (AboveOf16 ?x)
          )
          (and
            (AboveOf18 ?x)
            (BelowOf10 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf16 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf17 ?x)
          )
          (and
            (AboveOf15 ?x)
            (BelowOf4 ?x)
          )
          (and
            (AboveOf16 ?x)
            (BelowOf10 ?x)
          )
          (and
            (BelowOf13 ?x)
            (AboveOf16 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf15 ?x)
          )
          (and
            (AboveOf15 ?x)
            (BelowOf16 ?x)
          )
          (and
            (AboveOf16 ?x)
            (BelowOf8 ?x)
          )
          (and
            (AboveOf17 ?x)
            (BelowOf6 ?x)
          )
          (and
            (AboveOf17 ?x)
            (BelowOf11 ?x)
          )
          (and
            (AboveOf16 ?x)
            (BelowOf12 ?x)
          )
          (and
            (AboveOf15 ?x)
            (BelowOf3 ?x)
          )
          (and
            (BelowOf13 ?x)
            (AboveOf17 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf18 ?x)
          )
          (and
            (AboveOf15 ?x)
            (BelowOf11 ?x)
          )
          (and
            (BelowOf16 ?x)
            (AboveOf16 ?x)
          )
          (and
            (BelowOf13 ?x)
            (AboveOf18 ?x)
          )
          (and
            (BelowOf9 ?x)
            (AboveOf17 ?x)
          )
          (and
            (BelowOf14 ?x)
            (AboveOf17 ?x)
          )
          (and
            (AboveOf16 ?x)
            (BelowOf6 ?x)
          )
          (and
            (BelowOf16 ?x)
            (AboveOf17 ?x)
          )
          (and
            (BelowOf13 ?x)
            (AboveOf15 ?x)
          )
          (and
            (AboveOf18 ?x)
            (BelowOf6 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf17 ?x)
          )
          (and
            (AboveOf18 ?x)
            (BelowOf8 ?x)
          )
          (and
            (BelowOf14 ?x)
            (AboveOf18 ?x)
          )
          (and
            (AboveOf17 ?x)
            (BelowOf10 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf17 ?x)
          )
          (and
            (AboveOf16 ?x)
            (BelowOf3 ?x)
          )
          (and
            (BelowOf15 ?x)
            (AboveOf16 ?x)
          )
          (and
            (AboveOf16 ?x)
            (BelowOf4 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf18 ?x)
          )
          (and
            (AboveOf17 ?x)
            (BelowOf3 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf15 ?x)
          )
          (and
            (AboveOf15 ?x)
            (BelowOf15 ?x)
          )
          (and
            (AboveOf17 ?x)
            (BelowOf1 ?x)
          )
          (and
            (AboveOf15 ?x)
            (BelowOf10 ?x)
          )
          (and
            (BelowOf14 ?x)
            (AboveOf15 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf16 ?x)
          )
          (and
            (BelowOf7 ?x)
            (AboveOf18 ?x)
          )
          (and
            (AboveOf16 ?x)
            (BelowOf9 ?x)
          )
          (and
            (AboveOf15 ?x)
            (BelowOf7 ?x)
          )
          (and
            (AboveOf15 ?x)
            (BelowOf12 ?x)
          )
          (and
            (BelowOf7 ?x)
            (AboveOf16 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf18 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf18 ?x)
          )
          (and
            (AboveOf17 ?x)
            (BelowOf8 ?x)
          )
          (and
            (AboveOf16 ?x)
            (BelowOf11 ?x)
          )
          (and
            (AboveOf16 ?x)
            (BelowOf1 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf16 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf17 ?x)
          )
          (and
            (AboveOf18 ?x)
            (BelowOf12 ?x)
          )
        )
        (and
          (Row14 ?x)
        )
      )
      (when
        (or
          (and
            (BelowOf15 ?x)
            (AboveOf18 ?x)
          )
          (and
            (AboveOf18 ?x)
            (BelowOf3 ?x)
          )
          (and
            (AboveOf16 ?x)
            (BelowOf6 ?x)
          )
          (and
            (BelowOf15 ?x)
            (AboveOf17 ?x)
          )
          (and
            (BelowOf16 ?x)
            (AboveOf18 ?x)
          )
          (and
            (BelowOf6 ?x)
            (AboveOf18 ?x)
          )
          (and
            (AboveOf18 ?x)
            (BelowOf11 ?x)
          )
          (and
            (BelowOf7 ?x)
            (AboveOf17 ?x)
          )
          (and
            (AboveOf17 ?x)
            (BelowOf12 ?x)
          )
          (and
            (BelowOf9 ?x)
            (AboveOf18 ?x)
          )
          (and
            (BelowOf14 ?x)
            (AboveOf16 ?x)
          )
          (and
            (AboveOf18 ?x)
            (BelowOf10 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf16 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf17 ?x)
          )
          (and
            (BelowOf17 ?x)
            (AboveOf17 ?x)
          )
          (and
            (BelowOf13 ?x)
            (AboveOf16 ?x)
          )
          (and
            (AboveOf16 ?x)
            (BelowOf8 ?x)
          )
          (and
            (AboveOf17 ?x)
            (BelowOf6 ?x)
          )
          (and
            (AboveOf17 ?x)
            (BelowOf11 ?x)
          )
          (and
            (AboveOf16 ?x)
            (BelowOf12 ?x)
          )
          (and
            (BelowOf13 ?x)
            (AboveOf17 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf18 ?x)
          )
          (and
            (BelowOf16 ?x)
            (AboveOf16 ?x)
          )
          (and
            (BelowOf13 ?x)
            (AboveOf18 ?x)
          )
          (and
            (BelowOf9 ?x)
            (AboveOf17 ?x)
          )
          (and
            (BelowOf14 ?x)
            (AboveOf17 ?x)
          )
          (and
            (BelowOf16 ?x)
            (AboveOf17 ?x)
          )
          (and
            (AboveOf18 ?x)
            (BelowOf6 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf17 ?x)
          )
          (and
            (AboveOf18 ?x)
            (BelowOf8 ?x)
          )
          (and
            (BelowOf14 ?x)
            (AboveOf18 ?x)
          )
          (and
            (AboveOf17 ?x)
            (BelowOf10 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf17 ?x)
          )
          (and
            (AboveOf16 ?x)
            (BelowOf3 ?x)
          )
          (and
            (BelowOf15 ?x)
            (AboveOf16 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf18 ?x)
          )
          (and
            (AboveOf17 ?x)
            (BelowOf3 ?x)
          )
          (and
            (BelowOf7 ?x)
            (AboveOf18 ?x)
          )
          (and
            (AboveOf17 ?x)
            (BelowOf1 ?x)
          )
          (and
            (AboveOf16 ?x)
            (BelowOf17 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf16 ?x)
          )
          (and
            (AboveOf16 ?x)
            (BelowOf9 ?x)
          )
          (and
            (BelowOf17 ?x)
            (AboveOf18 ?x)
          )
          (and
            (BelowOf7 ?x)
            (AboveOf16 ?x)
          )
          (and
            (AboveOf16 ?x)
            (BelowOf10 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf18 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf18 ?x)
          )
          (and
            (AboveOf17 ?x)
            (BelowOf8 ?x)
          )
          (and
            (AboveOf16 ?x)
            (BelowOf11 ?x)
          )
          (and
            (AboveOf16 ?x)
            (BelowOf1 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf16 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf17 ?x)
          )
          (and
            (AboveOf18 ?x)
            (BelowOf12 ?x)
          )
        )
        (and
          (Row15 ?x)
        )
      )
      (when
        (or
          (and
            (BelowOf15 ?x)
            (AboveOf18 ?x)
          )
          (and
            (BelowOf9 ?x)
            (AboveOf17 ?x)
          )
          (and
            (AboveOf18 ?x)
            (BelowOf3 ?x)
          )
          (and
            (BelowOf15 ?x)
            (AboveOf17 ?x)
          )
          (and
            (BelowOf16 ?x)
            (AboveOf18 ?x)
          )
          (and
            (BelowOf6 ?x)
            (AboveOf18 ?x)
          )
          (and
            (AboveOf18 ?x)
            (BelowOf11 ?x)
          )
          (and
            (BelowOf7 ?x)
            (AboveOf17 ?x)
          )
          (and
            (AboveOf17 ?x)
            (BelowOf12 ?x)
          )
          (and
            (BelowOf9 ?x)
            (AboveOf18 ?x)
          )
          (and
            (BelowOf14 ?x)
            (AboveOf18 ?x)
          )
          (and
            (AboveOf18 ?x)
            (BelowOf10 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf18 ?x)
          )
          (and
            (BelowOf17 ?x)
            (AboveOf17 ?x)
          )
          (and
            (AboveOf17 ?x)
            (BelowOf6 ?x)
          )
          (and
            (AboveOf17 ?x)
            (BelowOf11 ?x)
          )
          (and
            (BelowOf13 ?x)
            (AboveOf17 ?x)
          )
          (and
            (BelowOf13 ?x)
            (AboveOf18 ?x)
          )
          (and
            (AboveOf18 ?x)
            (BelowOf18 ?x)
          )
          (and
            (BelowOf14 ?x)
            (AboveOf17 ?x)
          )
          (and
            (BelowOf16 ?x)
            (AboveOf17 ?x)
          )
          (and
            (AboveOf18 ?x)
            (BelowOf6 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf17 ?x)
          )
          (and
            (AboveOf18 ?x)
            (BelowOf8 ?x)
          )
          (and
            (AboveOf17 ?x)
            (BelowOf18 ?x)
          )
          (and
            (AboveOf17 ?x)
            (BelowOf10 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf17 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf18 ?x)
          )
          (and
            (AboveOf17 ?x)
            (BelowOf3 ?x)
          )
          (and
            (BelowOf7 ?x)
            (AboveOf18 ?x)
          )
          (and
            (AboveOf17 ?x)
            (BelowOf1 ?x)
          )
          (and
            (BelowOf17 ?x)
            (AboveOf18 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf18 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf18 ?x)
          )
          (and
            (AboveOf17 ?x)
            (BelowOf8 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf17 ?x)
          )
          (and
            (AboveOf18 ?x)
            (BelowOf12 ?x)
          )
        )
        (and
          (Row16 ?x)
        )
      )
      (when
        (or
          (and
            (AboveOf18 ?x)
            (BelowOf18 ?x)
          )
          (and
            (BelowOf15 ?x)
            (AboveOf18 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf18 ?x)
          )
          (and
            (AboveOf18 ?x)
            (BelowOf3 ?x)
          )
          (and
            (AboveOf18 ?x)
            (BelowOf8 ?x)
          )
          (and
            (BelowOf16 ?x)
            (AboveOf18 ?x)
          )
          (and
            (AboveOf18 ?x)
            (BelowOf19 ?x)
          )
          (and
            (BelowOf7 ?x)
            (AboveOf18 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf18 ?x)
          )
          (and
            (AboveOf18 ?x)
            (BelowOf11 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf18 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf18 ?x)
          )
          (and
            (AboveOf18 ?x)
            (BelowOf12 ?x)
          )
          (and
            (BelowOf17 ?x)
            (AboveOf18 ?x)
          )
          (and
            (BelowOf6 ?x)
            (AboveOf18 ?x)
          )
          (and
            (BelowOf9 ?x)
            (AboveOf18 ?x)
          )
          (and
            (BelowOf14 ?x)
            (AboveOf18 ?x)
          )
          (and
            (AboveOf18 ?x)
            (BelowOf10 ?x)
          )
          (and
            (BelowOf13 ?x)
            (AboveOf18 ?x)
          )
        )
        (and
          (Row17 ?x)
        )
      )
    )
  )
  (:action moveUp
    :parameters ( ?x )
    :precondition ( and
      (not (CheckConsistency))
      (not (Error))
      (or
        (BelowOf7 ?x)
        (AboveOf9 ?x)
        (BelowOf18 ?x)
        (BelowOf19 ?x)
        (BelowOf1 ?x)
        (BelowOf17 ?x)
        (AboveOf7 ?x)
        (BelowOf16 ?x)
        (AboveOf1 ?x)
        (AboveOf11 ?x)
        (BelowOf14 ?x)
        (BelowOf8 ?x)
        (BelowOf3 ?x)
        (AboveOf14 ?x)
        (AboveOf3 ?x)
        (BelowOf9 ?x)
        (AboveOf16 ?x)
        (BelowOf2 ?x)
        (BelowOf5 ?x)
        (BelowOf11 ?x)
        (BelowOf10 ?x)
        (AboveOf18 ?x)
        (BelowOf13 ?x)
        (AboveOf17 ?x)
        (BelowOf4 ?x)
        (AboveOf10 ?x)
        (AboveOf0 ?x)
        (AboveOf8 ?x)
        (AboveOf6 ?x)
        (AboveOf5 ?x)
        (AboveOf13 ?x)
        (Rows ?x)
        (AboveOf4 ?x)
        (BelowOf12 ?x)
        (AboveOf15 ?x)
        (AboveOf12 ?x)
        (BelowOf6 ?x)
        (BelowOf15 ?x)
        (AboveOf2 ?x)
      )
    )
    :effect ( and
      (CheckConsistency)
      (when
        (or
          (AboveOf18 ?x)
          (AboveOf1 ?x)
          (AboveOf5 ?x)
          (AboveOf14 ?x)
          (AboveOf13 ?x)
          (AboveOf3 ?x)
          (AboveOf4 ?x)
          (AboveOf16 ?x)
          (AboveOf9 ?x)
          (AboveOf8 ?x)
          (AboveOf15 ?x)
          (AboveOf17 ?x)
          (AboveOf12 ?x)
          (AboveOf7 ?x)
          (AboveOf0 ?x)
          (AboveOf2 ?x)
          (AboveOf10 ?x)
          (AboveOf11 ?x)
          (AboveOf6 ?x)
        )
        (and
          (AboveOf1 ?x)
        )
      )
      (when
        (or
          (AboveOf18 ?x)
          (AboveOf1 ?x)
          (AboveOf5 ?x)
          (AboveOf14 ?x)
          (AboveOf13 ?x)
          (AboveOf3 ?x)
          (AboveOf4 ?x)
          (AboveOf16 ?x)
          (AboveOf9 ?x)
          (AboveOf8 ?x)
          (AboveOf15 ?x)
          (AboveOf17 ?x)
          (AboveOf12 ?x)
          (AboveOf7 ?x)
          (AboveOf2 ?x)
          (AboveOf10 ?x)
          (AboveOf11 ?x)
          (AboveOf6 ?x)
        )
        (and
          (AboveOf2 ?x)
        )
      )
      (when
        (or
          (AboveOf18 ?x)
          (AboveOf5 ?x)
          (AboveOf14 ?x)
          (AboveOf13 ?x)
          (AboveOf3 ?x)
          (AboveOf4 ?x)
          (AboveOf16 ?x)
          (AboveOf9 ?x)
          (AboveOf8 ?x)
          (AboveOf15 ?x)
          (AboveOf17 ?x)
          (AboveOf12 ?x)
          (AboveOf7 ?x)
          (AboveOf2 ?x)
          (AboveOf10 ?x)
          (AboveOf6 ?x)
          (AboveOf11 ?x)
        )
        (and
          (AboveOf3 ?x)
        )
      )
      (when
        (or
          (AboveOf18 ?x)
          (AboveOf5 ?x)
          (AboveOf14 ?x)
          (AboveOf13 ?x)
          (AboveOf3 ?x)
          (AboveOf4 ?x)
          (AboveOf16 ?x)
          (AboveOf9 ?x)
          (AboveOf15 ?x)
          (AboveOf17 ?x)
          (AboveOf12 ?x)
          (AboveOf7 ?x)
          (AboveOf8 ?x)
          (AboveOf10 ?x)
          (AboveOf6 ?x)
          (AboveOf11 ?x)
        )
        (and
          (AboveOf4 ?x)
        )
      )
      (when
        (or
          (AboveOf18 ?x)
          (AboveOf5 ?x)
          (AboveOf14 ?x)
          (AboveOf13 ?x)
          (AboveOf4 ?x)
          (AboveOf16 ?x)
          (AboveOf9 ?x)
          (AboveOf15 ?x)
          (AboveOf17 ?x)
          (AboveOf12 ?x)
          (AboveOf7 ?x)
          (AboveOf8 ?x)
          (AboveOf10 ?x)
          (AboveOf6 ?x)
          (AboveOf11 ?x)
        )
        (and
          (AboveOf5 ?x)
        )
      )
      (when
        (or
          (AboveOf18 ?x)
          (AboveOf5 ?x)
          (AboveOf14 ?x)
          (AboveOf13 ?x)
          (AboveOf16 ?x)
          (AboveOf9 ?x)
          (AboveOf15 ?x)
          (AboveOf17 ?x)
          (AboveOf12 ?x)
          (AboveOf10 ?x)
          (AboveOf8 ?x)
          (AboveOf7 ?x)
          (AboveOf6 ?x)
          (AboveOf11 ?x)
        )
        (and
          (AboveOf6 ?x)
        )
      )
      (when
        (or
          (AboveOf18 ?x)
          (AboveOf14 ?x)
          (AboveOf13 ?x)
          (AboveOf16 ?x)
          (AboveOf9 ?x)
          (AboveOf15 ?x)
          (AboveOf17 ?x)
          (AboveOf12 ?x)
          (AboveOf10 ?x)
          (AboveOf8 ?x)
          (AboveOf7 ?x)
          (AboveOf6 ?x)
          (AboveOf11 ?x)
        )
        (and
          (AboveOf7 ?x)
        )
      )
      (when
        (or
          (AboveOf18 ?x)
          (AboveOf14 ?x)
          (AboveOf13 ?x)
          (AboveOf16 ?x)
          (AboveOf9 ?x)
          (AboveOf15 ?x)
          (AboveOf17 ?x)
          (AboveOf12 ?x)
          (AboveOf10 ?x)
          (AboveOf8 ?x)
          (AboveOf7 ?x)
          (AboveOf11 ?x)
        )
        (and
          (AboveOf8 ?x)
        )
      )
      (when
        (or
          (AboveOf18 ?x)
          (AboveOf14 ?x)
          (AboveOf13 ?x)
          (AboveOf16 ?x)
          (AboveOf9 ?x)
          (AboveOf15 ?x)
          (AboveOf17 ?x)
          (AboveOf12 ?x)
          (AboveOf10 ?x)
          (AboveOf8 ?x)
          (AboveOf11 ?x)
        )
        (and
          (AboveOf9 ?x)
        )
      )
      (when
        (or
          (AboveOf18 ?x)
          (AboveOf14 ?x)
          (AboveOf13 ?x)
          (AboveOf16 ?x)
          (AboveOf9 ?x)
          (AboveOf15 ?x)
          (AboveOf17 ?x)
          (AboveOf12 ?x)
          (AboveOf10 ?x)
          (AboveOf11 ?x)
        )
        (and
          (AboveOf10 ?x)
        )
      )
      (when
        (or
          (AboveOf18 ?x)
          (AboveOf14 ?x)
          (AboveOf13 ?x)
          (AboveOf16 ?x)
          (AboveOf15 ?x)
          (AboveOf17 ?x)
          (AboveOf12 ?x)
          (AboveOf10 ?x)
          (AboveOf11 ?x)
        )
        (and
          (AboveOf11 ?x)
        )
      )
      (when
        (or
          (AboveOf18 ?x)
          (AboveOf14 ?x)
          (AboveOf13 ?x)
          (AboveOf16 ?x)
          (AboveOf15 ?x)
          (AboveOf17 ?x)
          (AboveOf12 ?x)
          (AboveOf11 ?x)
        )
        (and
          (AboveOf12 ?x)
        )
      )
      (when
        (or
          (AboveOf18 ?x)
          (AboveOf14 ?x)
          (AboveOf13 ?x)
          (AboveOf16 ?x)
          (AboveOf15 ?x)
          (AboveOf17 ?x)
          (AboveOf12 ?x)
        )
        (and
          (AboveOf13 ?x)
        )
      )
      (when
        (or
          (AboveOf18 ?x)
          (AboveOf14 ?x)
          (AboveOf13 ?x)
          (AboveOf16 ?x)
          (AboveOf15 ?x)
          (AboveOf17 ?x)
        )
        (and
          (AboveOf14 ?x)
        )
      )
      (when
        (or
          (AboveOf18 ?x)
          (AboveOf15 ?x)
          (AboveOf17 ?x)
          (AboveOf14 ?x)
          (AboveOf16 ?x)
        )
        (and
          (AboveOf15 ?x)
        )
      )
      (when
        (or
          (AboveOf16 ?x)
          (AboveOf15 ?x)
          (AboveOf17 ?x)
          (AboveOf18 ?x)
        )
        (and
          (AboveOf16 ?x)
        )
      )
      (when
        (or
          (AboveOf16 ?x)
          (AboveOf17 ?x)
          (AboveOf18 ?x)
        )
        (and
          (AboveOf17 ?x)
        )
      )
      (when
        (or
          (AboveOf18 ?x)
          (AboveOf17 ?x)
        )
        (and
          (AboveOf18 ?x)
        )
      )
      (when
        (BelowOf1 ?x)
        (and
          (BelowOf2 ?x)
          (not (BelowOf1 ?x))
        )
      )
      (when
        (or
          (BelowOf1 ?x)
          (BelowOf2 ?x)
        )
        (and
          (BelowOf3 ?x)
          (not (BelowOf2 ?x))
        )
      )
      (when
        (or
          (BelowOf3 ?x)
          (BelowOf1 ?x)
          (BelowOf2 ?x)
        )
        (and
          (BelowOf4 ?x)
          (not (BelowOf3 ?x))
        )
      )
      (when
        (or
          (BelowOf4 ?x)
          (BelowOf3 ?x)
          (BelowOf1 ?x)
          (BelowOf2 ?x)
        )
        (and
          (BelowOf5 ?x)
          (not (BelowOf4 ?x))
        )
      )
      (when
        (or
          (BelowOf5 ?x)
          (BelowOf4 ?x)
          (BelowOf3 ?x)
          (BelowOf1 ?x)
          (BelowOf2 ?x)
        )
        (and
          (BelowOf6 ?x)
          (not (BelowOf5 ?x))
        )
      )
      (when
        (or
          (BelowOf3 ?x)
          (BelowOf4 ?x)
          (BelowOf1 ?x)
          (BelowOf2 ?x)
          (BelowOf5 ?x)
          (BelowOf6 ?x)
        )
        (and
          (BelowOf7 ?x)
          (not (BelowOf6 ?x))
        )
      )
      (when
        (or
          (BelowOf3 ?x)
          (BelowOf7 ?x)
          (BelowOf4 ?x)
          (BelowOf1 ?x)
          (BelowOf2 ?x)
          (BelowOf5 ?x)
          (BelowOf6 ?x)
        )
        (and
          (BelowOf8 ?x)
          (not (BelowOf7 ?x))
        )
      )
      (when
        (or
          (BelowOf8 ?x)
          (BelowOf3 ?x)
          (BelowOf7 ?x)
          (BelowOf4 ?x)
          (BelowOf1 ?x)
          (BelowOf2 ?x)
          (BelowOf5 ?x)
          (BelowOf6 ?x)
        )
        (and
          (BelowOf9 ?x)
          (not (BelowOf8 ?x))
        )
      )
      (when
        (or
          (BelowOf8 ?x)
          (BelowOf3 ?x)
          (BelowOf9 ?x)
          (BelowOf7 ?x)
          (BelowOf4 ?x)
          (BelowOf1 ?x)
          (BelowOf2 ?x)
          (BelowOf5 ?x)
          (BelowOf6 ?x)
        )
        (and
          (BelowOf10 ?x)
          (not (BelowOf9 ?x))
        )
      )
      (when
        (or
          (BelowOf8 ?x)
          (BelowOf3 ?x)
          (BelowOf9 ?x)
          (BelowOf7 ?x)
          (BelowOf4 ?x)
          (BelowOf1 ?x)
          (BelowOf2 ?x)
          (BelowOf5 ?x)
          (BelowOf6 ?x)
          (BelowOf10 ?x)
        )
        (and
          (BelowOf11 ?x)
          (not (BelowOf10 ?x))
        )
      )
      (when
        (or
          (BelowOf8 ?x)
          (BelowOf3 ?x)
          (BelowOf9 ?x)
          (BelowOf7 ?x)
          (BelowOf4 ?x)
          (BelowOf1 ?x)
          (BelowOf2 ?x)
          (BelowOf5 ?x)
          (BelowOf6 ?x)
          (BelowOf11 ?x)
          (BelowOf10 ?x)
        )
        (and
          (BelowOf12 ?x)
          (not (BelowOf11 ?x))
        )
      )
      (when
        (or
          (BelowOf8 ?x)
          (BelowOf3 ?x)
          (BelowOf9 ?x)
          (BelowOf7 ?x)
          (BelowOf12 ?x)
          (BelowOf4 ?x)
          (BelowOf1 ?x)
          (BelowOf2 ?x)
          (BelowOf5 ?x)
          (BelowOf6 ?x)
          (BelowOf11 ?x)
          (BelowOf10 ?x)
        )
        (and
          (BelowOf13 ?x)
          (not (BelowOf12 ?x))
        )
      )
      (when
        (or
          (BelowOf8 ?x)
          (BelowOf13 ?x)
          (BelowOf3 ?x)
          (BelowOf9 ?x)
          (BelowOf7 ?x)
          (BelowOf12 ?x)
          (BelowOf4 ?x)
          (BelowOf1 ?x)
          (BelowOf2 ?x)
          (BelowOf5 ?x)
          (BelowOf6 ?x)
          (BelowOf11 ?x)
          (BelowOf10 ?x)
        )
        (and
          (BelowOf14 ?x)
          (not (BelowOf13 ?x))
        )
      )
      (when
        (or
          (BelowOf14 ?x)
          (BelowOf8 ?x)
          (BelowOf13 ?x)
          (BelowOf3 ?x)
          (BelowOf9 ?x)
          (BelowOf7 ?x)
          (BelowOf12 ?x)
          (BelowOf4 ?x)
          (BelowOf1 ?x)
          (BelowOf2 ?x)
          (BelowOf5 ?x)
          (BelowOf6 ?x)
          (BelowOf11 ?x)
          (BelowOf10 ?x)
        )
        (and
          (BelowOf15 ?x)
          (not (BelowOf14 ?x))
        )
      )
      (when
        (or
          (BelowOf14 ?x)
          (BelowOf8 ?x)
          (BelowOf13 ?x)
          (BelowOf3 ?x)
          (BelowOf9 ?x)
          (BelowOf7 ?x)
          (BelowOf12 ?x)
          (BelowOf4 ?x)
          (BelowOf1 ?x)
          (BelowOf2 ?x)
          (BelowOf5 ?x)
          (BelowOf6 ?x)
          (BelowOf15 ?x)
          (BelowOf11 ?x)
          (BelowOf10 ?x)
        )
        (and
          (BelowOf16 ?x)
          (not (BelowOf15 ?x))
        )
      )
      (when
        (or
          (BelowOf14 ?x)
          (BelowOf8 ?x)
          (BelowOf13 ?x)
          (BelowOf3 ?x)
          (BelowOf9 ?x)
          (BelowOf7 ?x)
          (BelowOf12 ?x)
          (BelowOf4 ?x)
          (BelowOf1 ?x)
          (BelowOf2 ?x)
          (BelowOf5 ?x)
          (BelowOf11 ?x)
          (BelowOf6 ?x)
          (BelowOf15 ?x)
          (BelowOf16 ?x)
          (BelowOf10 ?x)
        )
        (and
          (BelowOf17 ?x)
          (not (BelowOf16 ?x))
        )
      )
      (when
        (or
          (BelowOf14 ?x)
          (BelowOf8 ?x)
          (BelowOf13 ?x)
          (BelowOf3 ?x)
          (BelowOf9 ?x)
          (BelowOf7 ?x)
          (BelowOf12 ?x)
          (BelowOf11 ?x)
          (BelowOf4 ?x)
          (BelowOf1 ?x)
          (BelowOf2 ?x)
          (BelowOf5 ?x)
          (BelowOf17 ?x)
          (BelowOf6 ?x)
          (BelowOf15 ?x)
          (BelowOf16 ?x)
          (BelowOf10 ?x)
        )
        (and
          (BelowOf18 ?x)
          (not (BelowOf17 ?x))
        )
      )
      (when
        (or
          (BelowOf14 ?x)
          (BelowOf8 ?x)
          (BelowOf13 ?x)
          (BelowOf3 ?x)
          (BelowOf9 ?x)
          (BelowOf7 ?x)
          (BelowOf12 ?x)
          (BelowOf18 ?x)
          (BelowOf11 ?x)
          (BelowOf4 ?x)
          (BelowOf1 ?x)
          (BelowOf2 ?x)
          (BelowOf5 ?x)
          (BelowOf17 ?x)
          (BelowOf6 ?x)
          (BelowOf15 ?x)
          (BelowOf16 ?x)
          (BelowOf10 ?x)
        )
        (and
          (BelowOf19 ?x)
          (not (BelowOf18 ?x))
        )
      )
      (when
        (Row0 ?x)
        (and
          (Row1 ?x)
          (not (Row0 ?x))
        )
      )
      (when
        (Row1 ?x)
        (and
          (Row2 ?x)
          (not (Row1 ?x))
        )
      )
      (when
        (Row2 ?x)
        (and
          (Row3 ?x)
          (not (Row2 ?x))
        )
      )
      (when
        (Row3 ?x)
        (and
          (Row4 ?x)
          (not (Row3 ?x))
        )
      )
      (when
        (Row4 ?x)
        (and
          (Row5 ?x)
          (not (Row4 ?x))
        )
      )
      (when
        (Row5 ?x)
        (and
          (Row6 ?x)
          (not (Row5 ?x))
        )
      )
      (when
        (Row6 ?x)
        (and
          (Row7 ?x)
          (not (Row6 ?x))
        )
      )
      (when
        (Row7 ?x)
        (and
          (Row8 ?x)
          (not (Row7 ?x))
        )
      )
      (when
        (Row8 ?x)
        (and
          (Row9 ?x)
          (not (Row8 ?x))
        )
      )
      (when
        (Row9 ?x)
        (and
          (Row10 ?x)
          (not (Row9 ?x))
        )
      )
      (when
        (Row10 ?x)
        (and
          (Row11 ?x)
          (not (Row10 ?x))
        )
      )
      (when
        (Row11 ?x)
        (and
          (Row12 ?x)
          (not (Row11 ?x))
        )
      )
      (when
        (Row12 ?x)
        (and
          (Row13 ?x)
          (not (Row12 ?x))
        )
      )
      (when
        (Row13 ?x)
        (and
          (Row14 ?x)
          (not (Row13 ?x))
        )
      )
      (when
        (Row14 ?x)
        (and
          (Row15 ?x)
          (not (Row14 ?x))
        )
      )
      (when
        (Row15 ?x)
        (and
          (Row16 ?x)
          (not (Row15 ?x))
        )
      )
      (when
        (Row16 ?x)
        (and
          (Row17 ?x)
          (not (Row16 ?x))
        )
      )
      (when
        (Row17 ?x)
        (and
          (Row18 ?x)
          (not (Row17 ?x))
        )
      )
      (when
        (or
          (and
            (BelowOf1 ?x)
            (AboveOf17 ?x)
          )
          (and
            (AboveOf9 ?x)
            (BelowOf1 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf3 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf8 ?x)
          )
          (and
            (AboveOf15 ?x)
            (BelowOf1 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf0 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf10 ?x)
          )
          (and
            (AboveOf16 ?x)
            (BelowOf1 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf1 ?x)
          )
          (and
            (AboveOf5 ?x)
            (BelowOf1 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf6 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf11 ?x)
          )
          (and
            (AboveOf7 ?x)
            (BelowOf1 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf1 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf12 ?x)
          )
          (and
            (AboveOf1 ?x)
            (BelowOf1 ?x)
          )
          (and
            (AboveOf2 ?x)
            (BelowOf1 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf18 ?x)
          )
          (and
            (AboveOf4 ?x)
            (BelowOf1 ?x)
          )
        )
        (and
          (Row1 ?x)
        )
      )
      (when
        (or
          (and
            (BelowOf2 ?x)
            (AboveOf3 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf8 ?x)
          )
          (and
            (AboveOf15 ?x)
            (BelowOf1 ?x)
          )
          (and
            (AboveOf5 ?x)
            (BelowOf2 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf12 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf12 ?x)
          )
          (and
            (AboveOf4 ?x)
            (BelowOf1 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf8 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf10 ?x)
          )
          (and
            (AboveOf9 ?x)
            (BelowOf1 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf3 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf11 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf10 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf17 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf1 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf6 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf1 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf9 ?x)
          )
          (and
            (AboveOf2 ?x)
            (BelowOf1 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf6 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf2 ?x)
          )
          (and
            (AboveOf17 ?x)
            (BelowOf1 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf4 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf16 ?x)
          )
          (and
            (AboveOf5 ?x)
            (BelowOf1 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf2 ?x)
          )
          (and
            (AboveOf7 ?x)
            (BelowOf1 ?x)
          )
          (and
            (AboveOf16 ?x)
            (BelowOf1 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf2 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf15 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf7 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf1 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf11 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf18 ?x)
          )
          (and
            (AboveOf1 ?x)
            (BelowOf1 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf18 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf17 ?x)
          )
        )
        (and
          (Row2 ?x)
        )
      )
      (when
        (or
          (and
            (BelowOf2 ?x)
            (AboveOf3 ?x)
          )
          (and
            (AboveOf18 ?x)
            (BelowOf3 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf8 ?x)
          )
          (and
            (AboveOf15 ?x)
            (BelowOf1 ?x)
          )
          (and
            (AboveOf5 ?x)
            (BelowOf2 ?x)
          )
          (and
            (BelowOf3 ?x)
            (AboveOf11 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf3 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf12 ?x)
          )
          (and
            (BelowOf3 ?x)
            (AboveOf12 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf12 ?x)
          )
          (and
            (AboveOf4 ?x)
            (BelowOf1 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf8 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf10 ?x)
          )
          (and
            (AboveOf9 ?x)
            (BelowOf1 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf3 ?x)
          )
          (and
            (AboveOf2 ?x)
            (BelowOf2 ?x)
          )
          (and
            (AboveOf10 ?x)
            (BelowOf3 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf1 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf10 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf17 ?x)
          )
          (and
            (AboveOf4 ?x)
            (BelowOf3 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf6 ?x)
          )
          (and
            (BelowOf3 ?x)
            (AboveOf8 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf1 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf9 ?x)
          )
          (and
            (BelowOf3 ?x)
            (AboveOf3 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf6 ?x)
          )
          (and
            (AboveOf2 ?x)
            (BelowOf3 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf2 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf4 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf16 ?x)
          )
          (and
            (AboveOf15 ?x)
            (BelowOf3 ?x)
          )
          (and
            (AboveOf9 ?x)
            (BelowOf3 ?x)
          )
          (and
            (AboveOf5 ?x)
            (BelowOf1 ?x)
          )
          (and
            (AboveOf16 ?x)
            (BelowOf3 ?x)
          )
          (and
            (AboveOf7 ?x)
            (BelowOf1 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf2 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf3 ?x)
          )
          (and
            (AboveOf17 ?x)
            (BelowOf3 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf15 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf7 ?x)
          )
          (and
            (AboveOf17 ?x)
            (BelowOf1 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf11 ?x)
          )
          (and
            (AboveOf7 ?x)
            (BelowOf3 ?x)
          )
          (and
            (AboveOf2 ?x)
            (BelowOf1 ?x)
          )
          (and
            (AboveOf6 ?x)
            (BelowOf3 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf11 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf18 ?x)
          )
          (and
            (AboveOf16 ?x)
            (BelowOf1 ?x)
          )
          (and
            (AboveOf5 ?x)
            (BelowOf3 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf18 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf17 ?x)
          )
        )
        (and
          (Row3 ?x)
        )
      )
      (when
        (or
          (and
            (BelowOf2 ?x)
            (AboveOf3 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf8 ?x)
          )
          (and
            (AboveOf18 ?x)
            (BelowOf3 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf8 ?x)
          )
          (and
            (AboveOf15 ?x)
            (BelowOf1 ?x)
          )
          (and
            (AboveOf5 ?x)
            (BelowOf2 ?x)
          )
          (and
            (BelowOf3 ?x)
            (AboveOf11 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf12 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf2 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf3 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf4 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf11 ?x)
          )
          (and
            (BelowOf3 ?x)
            (AboveOf12 ?x)
          )
          (and
            (AboveOf7 ?x)
            (BelowOf4 ?x)
          )
          (and
            (AboveOf11 ?x)
            (BelowOf3 ?x)
          )
          (and
            (AboveOf4 ?x)
            (BelowOf1 ?x)
          )
          (and
            (AboveOf15 ?x)
            (BelowOf4 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf10 ?x)
          )
          (and
            (AboveOf9 ?x)
            (BelowOf1 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf3 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf10 ?x)
          )
          (and
            (AboveOf10 ?x)
            (BelowOf3 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf1 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf10 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf12 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf17 ?x)
          )
          (and
            (AboveOf4 ?x)
            (BelowOf3 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf6 ?x)
          )
          (and
            (BelowOf3 ?x)
            (AboveOf8 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf1 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf9 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf18 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf6 ?x)
          )
          (and
            (AboveOf3 ?x)
            (BelowOf3 ?x)
          )
          (and
            (AboveOf5 ?x)
            (BelowOf4 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf12 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf4 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf16 ?x)
          )
          (and
            (AboveOf15 ?x)
            (BelowOf3 ?x)
          )
          (and
            (AboveOf9 ?x)
            (BelowOf3 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf8 ?x)
          )
          (and
            (AboveOf5 ?x)
            (BelowOf1 ?x)
          )
          (and
            (AboveOf16 ?x)
            (BelowOf3 ?x)
          )
          (and
            (AboveOf7 ?x)
            (BelowOf1 ?x)
          )
          (and
            (AboveOf16 ?x)
            (BelowOf4 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf2 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf18 ?x)
          )
          (and
            (AboveOf17 ?x)
            (BelowOf3 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf15 ?x)
          )
          (and
            (AboveOf4 ?x)
            (BelowOf4 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf7 ?x)
          )
          (and
            (AboveOf17 ?x)
            (BelowOf1 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf11 ?x)
          )
          (and
            (AboveOf16 ?x)
            (BelowOf1 ?x)
          )
          (and
            (AboveOf7 ?x)
            (BelowOf3 ?x)
          )
          (and
            (AboveOf6 ?x)
            (BelowOf3 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf11 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf6 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf17 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf3 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf4 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf9 ?x)
          )
          (and
            (AboveOf5 ?x)
            (BelowOf3 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf3 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf18 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf17 ?x)
          )
        )
        (and
          (Row4 ?x)
        )
      )
      (when
        (or
          (and
            (BelowOf5 ?x)
            (AboveOf14 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf11 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf18 ?x)
          )
          (and
            (AboveOf18 ?x)
            (BelowOf3 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf8 ?x)
          )
          (and
            (AboveOf15 ?x)
            (BelowOf1 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf6 ?x)
          )
          (and
            (BelowOf3 ?x)
            (AboveOf11 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf11 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf12 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf2 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf3 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf4 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf12 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf11 ?x)
          )
          (and
            (BelowOf3 ?x)
            (AboveOf12 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf18 ?x)
          )
          (and
            (AboveOf11 ?x)
            (BelowOf3 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf9 ?x)
          )
          (and
            (AboveOf4 ?x)
            (BelowOf1 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf8 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf10 ?x)
          )
          (and
            (AboveOf9 ?x)
            (BelowOf1 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf6 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf10 ?x)
          )
          (and
            (AboveOf10 ?x)
            (BelowOf3 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf1 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf10 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf12 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf7 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf17 ?x)
          )
          (and
            (AboveOf4 ?x)
            (BelowOf3 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf5 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf6 ?x)
          )
          (and
            (BelowOf3 ?x)
            (AboveOf8 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf1 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf9 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf16 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf6 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf13 ?x)
          )
          (and
            (AboveOf5 ?x)
            (BelowOf4 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf12 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf4 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf16 ?x)
          )
          (and
            (AboveOf15 ?x)
            (BelowOf3 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf10 ?x)
          )
          (and
            (AboveOf9 ?x)
            (BelowOf3 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf8 ?x)
          )
          (and
            (AboveOf5 ?x)
            (BelowOf1 ?x)
          )
          (and
            (AboveOf16 ?x)
            (BelowOf3 ?x)
          )
          (and
            (AboveOf5 ?x)
            (BelowOf2 ?x)
          )
          (and
            (AboveOf7 ?x)
            (BelowOf1 ?x)
          )
          (and
            (AboveOf16 ?x)
            (BelowOf4 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf2 ?x)
          )
          (and
            (AboveOf15 ?x)
            (BelowOf4 ?x)
          )
          (and
            (AboveOf17 ?x)
            (BelowOf3 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf15 ?x)
          )
          (and
            (AboveOf4 ?x)
            (BelowOf4 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf7 ?x)
          )
          (and
            (AboveOf17 ?x)
            (BelowOf1 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf11 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf16 ?x)
          )
          (and
            (AboveOf16 ?x)
            (BelowOf1 ?x)
          )
          (and
            (AboveOf7 ?x)
            (BelowOf3 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf8 ?x)
          )
          (and
            (AboveOf6 ?x)
            (BelowOf3 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf15 ?x)
          )
          (and
            (AboveOf7 ?x)
            (BelowOf4 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf18 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf4 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf17 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf9 ?x)
          )
          (and
            (AboveOf5 ?x)
            (BelowOf3 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf4 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf3 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf17 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf18 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf17 ?x)
          )
        )
        (and
          (Row5 ?x)
        )
      )
      (when
        (or
          (and
            (BelowOf3 ?x)
            (AboveOf12 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf11 ?x)
          )
          (and
            (AboveOf6 ?x)
            (BelowOf3 ?x)
          )
          (and
            (AboveOf5 ?x)
            (BelowOf4 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf14 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf8 ?x)
          )
          (and
            (AboveOf7 ?x)
            (BelowOf6 ?x)
          )
          (and
            (AboveOf5 ?x)
            (BelowOf2 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf18 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf15 ?x)
          )
          (and
            (AboveOf17 ?x)
            (BelowOf1 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf6 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf4 ?x)
          )
          (and
            (AboveOf16 ?x)
            (BelowOf6 ?x)
          )
          (and
            (BelowOf3 ?x)
            (AboveOf11 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf11 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf16 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf8 ?x)
          )
          (and
            (AboveOf9 ?x)
            (BelowOf6 ?x)
          )
          (and
            (BelowOf6 ?x)
            (AboveOf8 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf12 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf7 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf6 ?x)
          )
          (and
            (BelowOf3 ?x)
            (AboveOf8 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf6 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf2 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf12 ?x)
          )
          (and
            (AboveOf7 ?x)
            (BelowOf4 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf9 ?x)
          )
          (and
            (AboveOf17 ?x)
            (BelowOf3 ?x)
          )
          (and
            (AboveOf5 ?x)
            (BelowOf6 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf11 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf16 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf6 ?x)
          )
          (and
            (AboveOf15 ?x)
            (BelowOf6 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf17 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf17 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf8 ?x)
          )
          (and
            (AboveOf15 ?x)
            (BelowOf1 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf6 ?x)
          )
          (and
            (AboveOf10 ?x)
            (BelowOf6 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf3 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf12 ?x)
          )
          (and
            (AboveOf11 ?x)
            (BelowOf3 ?x)
          )
          (and
            (AboveOf15 ?x)
            (BelowOf4 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf10 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf6 ?x)
          )
          (and
            (AboveOf17 ?x)
            (BelowOf6 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf10 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf1 ?x)
          )
          (and
            (AboveOf16 ?x)
            (BelowOf4 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf16 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf13 ?x)
          )
          (and
            (AboveOf5 ?x)
            (BelowOf1 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf3 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf5 ?x)
          )
          (and
            (BelowOf6 ?x)
            (AboveOf12 ?x)
          )
          (and
            (AboveOf7 ?x)
            (BelowOf3 ?x)
          )
          (and
            (BelowOf6 ?x)
            (AboveOf11 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf4 ?x)
          )
          (and
            (AboveOf16 ?x)
            (BelowOf1 ?x)
          )
          (and
            (AboveOf18 ?x)
            (BelowOf3 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf1 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf12 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf9 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf18 ?x)
          )
          (and
            (AboveOf9 ?x)
            (BelowOf1 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf15 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf10 ?x)
          )
          (and
            (AboveOf10 ?x)
            (BelowOf3 ?x)
          )
          (and
            (AboveOf6 ?x)
            (BelowOf6 ?x)
          )
          (and
            (AboveOf15 ?x)
            (BelowOf3 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf9 ?x)
          )
          (and
            (AboveOf18 ?x)
            (BelowOf6 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf17 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf11 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf10 ?x)
          )
          (and
            (AboveOf9 ?x)
            (BelowOf3 ?x)
          )
          (and
            (AboveOf5 ?x)
            (BelowOf5 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf17 ?x)
          )
          (and
            (AboveOf16 ?x)
            (BelowOf3 ?x)
          )
          (and
            (AboveOf7 ?x)
            (BelowOf1 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf2 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf8 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf7 ?x)
          )
          (and
            (AboveOf5 ?x)
            (BelowOf3 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf18 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf18 ?x)
          )
        )
        (and
          (Row6 ?x)
        )
      )
      (when
        (or
          (and
            (BelowOf3 ?x)
            (AboveOf12 ?x)
          )
          (and
            (BelowOf7 ?x)
            (AboveOf12 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf11 ?x)
          )
          (and
            (AboveOf6 ?x)
            (BelowOf3 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf14 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf8 ?x)
          )
          (and
            (AboveOf7 ?x)
            (BelowOf6 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf18 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf15 ?x)
          )
          (and
            (BelowOf6 ?x)
            (AboveOf6 ?x)
          )
          (and
            (AboveOf17 ?x)
            (BelowOf1 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf6 ?x)
          )
          (and
            (BelowOf7 ?x)
            (AboveOf9 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf4 ?x)
          )
          (and
            (AboveOf16 ?x)
            (BelowOf6 ?x)
          )
          (and
            (BelowOf3 ?x)
            (AboveOf11 ?x)
          )
          (and
            (BelowOf7 ?x)
            (AboveOf11 ?x)
          )
          (and
            (BelowOf7 ?x)
            (AboveOf8 ?x)
          )
          (and
            (BelowOf7 ?x)
            (AboveOf17 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf11 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf16 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf8 ?x)
          )
          (and
            (AboveOf9 ?x)
            (BelowOf6 ?x)
          )
          (and
            (BelowOf6 ?x)
            (AboveOf8 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf12 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf7 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf6 ?x)
          )
          (and
            (BelowOf3 ?x)
            (AboveOf8 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf6 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf2 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf12 ?x)
          )
          (and
            (AboveOf7 ?x)
            (BelowOf4 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf9 ?x)
          )
          (and
            (AboveOf17 ?x)
            (BelowOf3 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf11 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf16 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf6 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf7 ?x)
          )
          (and
            (AboveOf15 ?x)
            (BelowOf6 ?x)
          )
          (and
            (BelowOf7 ?x)
            (AboveOf10 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf17 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf17 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf8 ?x)
          )
          (and
            (AboveOf15 ?x)
            (BelowOf1 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf6 ?x)
          )
          (and
            (BelowOf6 ?x)
            (AboveOf18 ?x)
          )
          (and
            (AboveOf10 ?x)
            (BelowOf6 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf3 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf12 ?x)
          )
          (and
            (AboveOf11 ?x)
            (BelowOf3 ?x)
          )
          (and
            (AboveOf15 ?x)
            (BelowOf4 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf10 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf6 ?x)
          )
          (and
            (AboveOf17 ?x)
            (BelowOf6 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf10 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf1 ?x)
          )
          (and
            (AboveOf16 ?x)
            (BelowOf4 ?x)
          )
          (and
            (BelowOf6 ?x)
            (AboveOf10 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf16 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf13 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf3 ?x)
          )
          (and
            (BelowOf7 ?x)
            (AboveOf18 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf5 ?x)
          )
          (and
            (BelowOf6 ?x)
            (AboveOf12 ?x)
          )
          (and
            (AboveOf7 ?x)
            (BelowOf3 ?x)
          )
          (and
            (BelowOf7 ?x)
            (AboveOf16 ?x)
          )
          (and
            (BelowOf6 ?x)
            (AboveOf11 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf4 ?x)
          )
          (and
            (AboveOf16 ?x)
            (BelowOf1 ?x)
          )
          (and
            (AboveOf18 ?x)
            (BelowOf3 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf1 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf12 ?x)
          )
          (and
            (BelowOf7 ?x)
            (AboveOf7 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf9 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf18 ?x)
          )
          (and
            (AboveOf9 ?x)
            (BelowOf1 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf15 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf10 ?x)
          )
          (and
            (AboveOf10 ?x)
            (BelowOf3 ?x)
          )
          (and
            (AboveOf15 ?x)
            (BelowOf3 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf9 ?x)
          )
          (and
            (AboveOf15 ?x)
            (BelowOf7 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf7 ?x)
          )
          (and
            (AboveOf18 ?x)
            (BelowOf6 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf17 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf11 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf10 ?x)
          )
          (and
            (AboveOf9 ?x)
            (BelowOf3 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf17 ?x)
          )
          (and
            (AboveOf16 ?x)
            (BelowOf3 ?x)
          )
          (and
            (AboveOf7 ?x)
            (BelowOf1 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf2 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf8 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf7 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf18 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf18 ?x)
          )
          (and
            (BelowOf7 ?x)
            (AboveOf6 ?x)
          )
        )
        (and
          (Row7 ?x)
        )
      )
      (when
        (or
          (and
            (BelowOf3 ?x)
            (AboveOf12 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf7 ?x)
          )
          (and
            (BelowOf7 ?x)
            (AboveOf12 ?x)
          )
          (and
            (AboveOf16 ?x)
            (BelowOf8 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf11 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf14 ?x)
          )
          (and
            (BelowOf8 ?x)
            (AboveOf12 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf8 ?x)
          )
          (and
            (AboveOf7 ?x)
            (BelowOf6 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf18 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf15 ?x)
          )
          (and
            (AboveOf17 ?x)
            (BelowOf1 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf6 ?x)
          )
          (and
            (BelowOf7 ?x)
            (AboveOf9 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf4 ?x)
          )
          (and
            (AboveOf15 ?x)
            (BelowOf8 ?x)
          )
          (and
            (AboveOf16 ?x)
            (BelowOf6 ?x)
          )
          (and
            (BelowOf3 ?x)
            (AboveOf11 ?x)
          )
          (and
            (BelowOf7 ?x)
            (AboveOf11 ?x)
          )
          (and
            (BelowOf7 ?x)
            (AboveOf8 ?x)
          )
          (and
            (BelowOf7 ?x)
            (AboveOf17 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf11 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf16 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf8 ?x)
          )
          (and
            (AboveOf9 ?x)
            (BelowOf6 ?x)
          )
          (and
            (BelowOf6 ?x)
            (AboveOf8 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf12 ?x)
          )
          (and
            (AboveOf7 ?x)
            (BelowOf7 ?x)
          )
          (and
            (BelowOf3 ?x)
            (AboveOf8 ?x)
          )
          (and
            (BelowOf8 ?x)
            (AboveOf8 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf16 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf2 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf12 ?x)
          )
          (and
            (AboveOf7 ?x)
            (BelowOf4 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf9 ?x)
          )
          (and
            (AboveOf17 ?x)
            (BelowOf3 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf11 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf16 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf6 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf7 ?x)
          )
          (and
            (AboveOf15 ?x)
            (BelowOf6 ?x)
          )
          (and
            (AboveOf16 ?x)
            (BelowOf3 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf17 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf17 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf8 ?x)
          )
          (and
            (AboveOf15 ?x)
            (BelowOf1 ?x)
          )
          (and
            (BelowOf6 ?x)
            (AboveOf18 ?x)
          )
          (and
            (AboveOf7 ?x)
            (BelowOf8 ?x)
          )
          (and
            (AboveOf10 ?x)
            (BelowOf6 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf3 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf12 ?x)
          )
          (and
            (AboveOf11 ?x)
            (BelowOf3 ?x)
          )
          (and
            (AboveOf15 ?x)
            (BelowOf4 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf10 ?x)
          )
          (and
            (AboveOf17 ?x)
            (BelowOf6 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf10 ?x)
          )
          (and
            (AboveOf9 ?x)
            (BelowOf8 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf1 ?x)
          )
          (and
            (AboveOf16 ?x)
            (BelowOf4 ?x)
          )
          (and
            (BelowOf6 ?x)
            (AboveOf10 ?x)
          )
          (and
            (AboveOf18 ?x)
            (BelowOf8 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf13 ?x)
          )
          (and
            (AboveOf10 ?x)
            (BelowOf8 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf3 ?x)
          )
          (and
            (BelowOf7 ?x)
            (AboveOf18 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf5 ?x)
          )
          (and
            (BelowOf6 ?x)
            (AboveOf12 ?x)
          )
          (and
            (AboveOf7 ?x)
            (BelowOf3 ?x)
          )
          (and
            (BelowOf7 ?x)
            (AboveOf16 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf8 ?x)
          )
          (and
            (BelowOf6 ?x)
            (AboveOf11 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf4 ?x)
          )
          (and
            (AboveOf17 ?x)
            (BelowOf8 ?x)
          )
          (and
            (AboveOf16 ?x)
            (BelowOf1 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf8 ?x)
          )
          (and
            (AboveOf11 ?x)
            (BelowOf8 ?x)
          )
          (and
            (AboveOf18 ?x)
            (BelowOf3 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf1 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf12 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf9 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf18 ?x)
          )
          (and
            (AboveOf9 ?x)
            (BelowOf1 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf15 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf10 ?x)
          )
          (and
            (AboveOf10 ?x)
            (BelowOf3 ?x)
          )
          (and
            (AboveOf15 ?x)
            (BelowOf3 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf9 ?x)
          )
          (and
            (AboveOf15 ?x)
            (BelowOf7 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf7 ?x)
          )
          (and
            (AboveOf18 ?x)
            (BelowOf6 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf17 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf11 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf10 ?x)
          )
          (and
            (AboveOf9 ?x)
            (BelowOf3 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf17 ?x)
          )
          (and
            (BelowOf7 ?x)
            (AboveOf10 ?x)
          )
          (and
            (AboveOf7 ?x)
            (BelowOf1 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf2 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf8 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf7 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf18 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf18 ?x)
          )
        )
        (and
          (Row8 ?x)
        )
      )
      (when
        (or
          (and
            (BelowOf9 ?x)
            (AboveOf12 ?x)
          )
          (and
            (BelowOf3 ?x)
            (AboveOf12 ?x)
          )
          (and
            (BelowOf9 ?x)
            (AboveOf8 ?x)
          )
          (and
            (BelowOf9 ?x)
            (AboveOf18 ?x)
          )
          (and
            (BelowOf7 ?x)
            (AboveOf12 ?x)
          )
          (and
            (AboveOf16 ?x)
            (BelowOf8 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf11 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf14 ?x)
          )
          (and
            (BelowOf8 ?x)
            (AboveOf12 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf8 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf18 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf15 ?x)
          )
          (and
            (AboveOf17 ?x)
            (BelowOf1 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf6 ?x)
          )
          (and
            (BelowOf7 ?x)
            (AboveOf9 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf4 ?x)
          )
          (and
            (AboveOf15 ?x)
            (BelowOf8 ?x)
          )
          (and
            (AboveOf15 ?x)
            (BelowOf9 ?x)
          )
          (and
            (BelowOf3 ?x)
            (AboveOf11 ?x)
          )
          (and
            (BelowOf7 ?x)
            (AboveOf11 ?x)
          )
          (and
            (BelowOf7 ?x)
            (AboveOf8 ?x)
          )
          (and
            (BelowOf7 ?x)
            (AboveOf17 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf11 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf16 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf8 ?x)
          )
          (and
            (AboveOf9 ?x)
            (BelowOf6 ?x)
          )
          (and
            (BelowOf6 ?x)
            (AboveOf8 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf12 ?x)
          )
          (and
            (BelowOf3 ?x)
            (AboveOf8 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf16 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf2 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf12 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf9 ?x)
          )
          (and
            (AboveOf8 ?x)
            (BelowOf8 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf11 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf16 ?x)
          )
          (and
            (AboveOf16 ?x)
            (BelowOf9 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf6 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf7 ?x)
          )
          (and
            (AboveOf15 ?x)
            (BelowOf6 ?x)
          )
          (and
            (AboveOf16 ?x)
            (BelowOf3 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf17 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf17 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf8 ?x)
          )
          (and
            (AboveOf15 ?x)
            (BelowOf1 ?x)
          )
          (and
            (AboveOf9 ?x)
            (BelowOf9 ?x)
          )
          (and
            (BelowOf6 ?x)
            (AboveOf18 ?x)
          )
          (and
            (AboveOf10 ?x)
            (BelowOf6 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf3 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf12 ?x)
          )
          (and
            (AboveOf11 ?x)
            (BelowOf3 ?x)
          )
          (and
            (AboveOf15 ?x)
            (BelowOf4 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf10 ?x)
          )
          (and
            (AboveOf17 ?x)
            (BelowOf3 ?x)
          )
          (and
            (AboveOf17 ?x)
            (BelowOf6 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf10 ?x)
          )
          (and
            (AboveOf9 ?x)
            (BelowOf8 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf1 ?x)
          )
          (and
            (AboveOf16 ?x)
            (BelowOf4 ?x)
          )
          (and
            (BelowOf6 ?x)
            (AboveOf10 ?x)
          )
          (and
            (AboveOf16 ?x)
            (BelowOf6 ?x)
          )
          (and
            (AboveOf18 ?x)
            (BelowOf8 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf13 ?x)
          )
          (and
            (AboveOf10 ?x)
            (BelowOf8 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf9 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf3 ?x)
          )
          (and
            (BelowOf7 ?x)
            (AboveOf18 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf5 ?x)
          )
          (and
            (BelowOf6 ?x)
            (AboveOf12 ?x)
          )
          (and
            (BelowOf9 ?x)
            (AboveOf10 ?x)
          )
          (and
            (BelowOf7 ?x)
            (AboveOf16 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf8 ?x)
          )
          (and
            (BelowOf6 ?x)
            (AboveOf11 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf4 ?x)
          )
          (and
            (AboveOf17 ?x)
            (BelowOf8 ?x)
          )
          (and
            (AboveOf16 ?x)
            (BelowOf1 ?x)
          )
          (and
            (AboveOf12 ?x)
            (BelowOf8 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf8 ?x)
          )
          (and
            (AboveOf11 ?x)
            (BelowOf8 ?x)
          )
          (and
            (AboveOf18 ?x)
            (BelowOf3 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf1 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf12 ?x)
          )
          (and
            (BelowOf9 ?x)
            (AboveOf11 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf9 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf18 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf9 ?x)
          )
          (and
            (AboveOf9 ?x)
            (BelowOf1 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf15 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf10 ?x)
          )
          (and
            (AboveOf10 ?x)
            (BelowOf3 ?x)
          )
          (and
            (AboveOf15 ?x)
            (BelowOf3 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf9 ?x)
          )
          (and
            (AboveOf15 ?x)
            (BelowOf7 ?x)
          )
          (and
            (BelowOf9 ?x)
            (AboveOf17 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf7 ?x)
          )
          (and
            (AboveOf18 ?x)
            (BelowOf6 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf17 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf11 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf10 ?x)
          )
          (and
            (AboveOf9 ?x)
            (BelowOf3 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf17 ?x)
          )
          (and
            (BelowOf7 ?x)
            (AboveOf10 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf2 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf8 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf18 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf18 ?x)
          )
        )
        (and
          (Row9 ?x)
        )
      )
      (when
        (or
          (and
            (BelowOf9 ?x)
            (AboveOf12 ?x)
          )
          (and
            (BelowOf3 ?x)
            (AboveOf12 ?x)
          )
          (and
            (BelowOf9 ?x)
            (AboveOf18 ?x)
          )
          (and
            (BelowOf10 ?x)
            (AboveOf11 ?x)
          )
          (and
            (BelowOf7 ?x)
            (AboveOf12 ?x)
          )
          (and
            (AboveOf16 ?x)
            (BelowOf8 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf11 ?x)
          )
          (and
            (AboveOf15 ?x)
            (BelowOf1 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf14 ?x)
          )
          (and
            (AboveOf17 ?x)
            (BelowOf1 ?x)
          )
          (and
            (BelowOf8 ?x)
            (AboveOf12 ?x)
          )
          (and
            (AboveOf9 ?x)
            (BelowOf3 ?x)
          )
          (and
            (AboveOf17 ?x)
            (BelowOf10 ?x)
          )
          (and
            (AboveOf15 ?x)
            (BelowOf10 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf18 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf15 ?x)
          )
          (and
            (AboveOf9 ?x)
            (BelowOf10 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf6 ?x)
          )
          (and
            (BelowOf7 ?x)
            (AboveOf9 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf4 ?x)
          )
          (and
            (AboveOf15 ?x)
            (BelowOf8 ?x)
          )
          (and
            (AboveOf15 ?x)
            (BelowOf9 ?x)
          )
          (and
            (BelowOf3 ?x)
            (AboveOf11 ?x)
          )
          (and
            (BelowOf7 ?x)
            (AboveOf11 ?x)
          )
          (and
            (BelowOf7 ?x)
            (AboveOf17 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf11 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf16 ?x)
          )
          (and
            (AboveOf9 ?x)
            (BelowOf6 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf12 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf16 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf2 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf12 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf9 ?x)
          )
          (and
            (AboveOf16 ?x)
            (BelowOf10 ?x)
          )
          (and
            (AboveOf17 ?x)
            (BelowOf3 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf11 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf16 ?x)
          )
          (and
            (AboveOf16 ?x)
            (BelowOf9 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf6 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf7 ?x)
          )
          (and
            (AboveOf15 ?x)
            (BelowOf6 ?x)
          )
          (and
            (AboveOf16 ?x)
            (BelowOf3 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf17 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf17 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf10 ?x)
          )
          (and
            (BelowOf6 ?x)
            (AboveOf18 ?x)
          )
          (and
            (AboveOf10 ?x)
            (BelowOf6 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf3 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf12 ?x)
          )
          (and
            (AboveOf11 ?x)
            (BelowOf3 ?x)
          )
          (and
            (BelowOf9 ?x)
            (AboveOf9 ?x)
          )
          (and
            (AboveOf15 ?x)
            (BelowOf4 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf10 ?x)
          )
          (and
            (AboveOf17 ?x)
            (BelowOf6 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf10 ?x)
          )
          (and
            (AboveOf9 ?x)
            (BelowOf8 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf1 ?x)
          )
          (and
            (AboveOf16 ?x)
            (BelowOf4 ?x)
          )
          (and
            (BelowOf6 ?x)
            (AboveOf10 ?x)
          )
          (and
            (AboveOf16 ?x)
            (BelowOf6 ?x)
          )
          (and
            (AboveOf18 ?x)
            (BelowOf8 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf13 ?x)
          )
          (and
            (AboveOf10 ?x)
            (BelowOf8 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf9 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf10 ?x)
          )
          (and
            (BelowOf7 ?x)
            (AboveOf18 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf5 ?x)
          )
          (and
            (BelowOf6 ?x)
            (AboveOf12 ?x)
          )
          (and
            (BelowOf9 ?x)
            (AboveOf10 ?x)
          )
          (and
            (BelowOf7 ?x)
            (AboveOf16 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf8 ?x)
          )
          (and
            (BelowOf6 ?x)
            (AboveOf11 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf4 ?x)
          )
          (and
            (AboveOf17 ?x)
            (BelowOf8 ?x)
          )
          (and
            (AboveOf16 ?x)
            (BelowOf1 ?x)
          )
          (and
            (AboveOf12 ?x)
            (BelowOf8 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf8 ?x)
          )
          (and
            (AboveOf11 ?x)
            (BelowOf8 ?x)
          )
          (and
            (AboveOf18 ?x)
            (BelowOf3 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf1 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf12 ?x)
          )
          (and
            (BelowOf9 ?x)
            (AboveOf11 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf9 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf18 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf9 ?x)
          )
          (and
            (AboveOf9 ?x)
            (BelowOf1 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf15 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf10 ?x)
          )
          (and
            (AboveOf10 ?x)
            (BelowOf3 ?x)
          )
          (and
            (BelowOf10 ?x)
            (AboveOf12 ?x)
          )
          (and
            (AboveOf18 ?x)
            (BelowOf10 ?x)
          )
          (and
            (AboveOf15 ?x)
            (BelowOf3 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf9 ?x)
          )
          (and
            (AboveOf10 ?x)
            (BelowOf10 ?x)
          )
          (and
            (AboveOf15 ?x)
            (BelowOf7 ?x)
          )
          (and
            (BelowOf9 ?x)
            (AboveOf17 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf7 ?x)
          )
          (and
            (AboveOf18 ?x)
            (BelowOf6 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf17 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf11 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf10 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf17 ?x)
          )
          (and
            (BelowOf7 ?x)
            (AboveOf10 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf2 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf18 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf18 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf3 ?x)
          )
        )
        (and
          (Row10 ?x)
        )
      )
      (when
        (or
          (and
            (BelowOf9 ?x)
            (AboveOf12 ?x)
          )
          (and
            (BelowOf11 ?x)
            (AboveOf12 ?x)
          )
          (and
            (BelowOf3 ?x)
            (AboveOf12 ?x)
          )
          (and
            (BelowOf9 ?x)
            (AboveOf18 ?x)
          )
          (and
            (BelowOf10 ?x)
            (AboveOf11 ?x)
          )
          (and
            (BelowOf7 ?x)
            (AboveOf12 ?x)
          )
          (and
            (AboveOf16 ?x)
            (BelowOf8 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf11 ?x)
          )
          (and
            (AboveOf15 ?x)
            (BelowOf1 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf14 ?x)
          )
          (and
            (BelowOf8 ?x)
            (AboveOf12 ?x)
          )
          (and
            (AboveOf17 ?x)
            (BelowOf10 ?x)
          )
          (and
            (AboveOf15 ?x)
            (BelowOf10 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf18 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf15 ?x)
          )
          (and
            (AboveOf17 ?x)
            (BelowOf1 ?x)
          )
          (and
            (BelowOf11 ?x)
            (AboveOf11 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf6 ?x)
          )
          (and
            (AboveOf10 ?x)
            (BelowOf11 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf4 ?x)
          )
          (and
            (AboveOf15 ?x)
            (BelowOf8 ?x)
          )
          (and
            (AboveOf15 ?x)
            (BelowOf9 ?x)
          )
          (and
            (BelowOf3 ?x)
            (AboveOf11 ?x)
          )
          (and
            (BelowOf7 ?x)
            (AboveOf11 ?x)
          )
          (and
            (BelowOf7 ?x)
            (AboveOf17 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf11 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf12 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf16 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf12 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf16 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf2 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf11 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf11 ?x)
          )
          (and
            (AboveOf16 ?x)
            (BelowOf10 ?x)
          )
          (and
            (AboveOf17 ?x)
            (BelowOf3 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf11 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf16 ?x)
          )
          (and
            (AboveOf16 ?x)
            (BelowOf9 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf6 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf7 ?x)
          )
          (and
            (AboveOf15 ?x)
            (BelowOf6 ?x)
          )
          (and
            (AboveOf16 ?x)
            (BelowOf3 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf17 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf17 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf10 ?x)
          )
          (and
            (BelowOf6 ?x)
            (AboveOf18 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf3 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf12 ?x)
          )
          (and
            (AboveOf11 ?x)
            (BelowOf3 ?x)
          )
          (and
            (AboveOf15 ?x)
            (BelowOf4 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf10 ?x)
          )
          (and
            (AboveOf17 ?x)
            (BelowOf6 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf10 ?x)
          )
          (and
            (AboveOf17 ?x)
            (BelowOf11 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf1 ?x)
          )
          (and
            (AboveOf16 ?x)
            (BelowOf4 ?x)
          )
          (and
            (AboveOf15 ?x)
            (BelowOf11 ?x)
          )
          (and
            (BelowOf6 ?x)
            (AboveOf10 ?x)
          )
          (and
            (AboveOf16 ?x)
            (BelowOf6 ?x)
          )
          (and
            (AboveOf18 ?x)
            (BelowOf8 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf13 ?x)
          )
          (and
            (AboveOf10 ?x)
            (BelowOf8 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf9 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf10 ?x)
          )
          (and
            (BelowOf7 ?x)
            (AboveOf18 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf5 ?x)
          )
          (and
            (BelowOf6 ?x)
            (AboveOf12 ?x)
          )
          (and
            (BelowOf9 ?x)
            (AboveOf10 ?x)
          )
          (and
            (BelowOf7 ?x)
            (AboveOf16 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf8 ?x)
          )
          (and
            (BelowOf6 ?x)
            (AboveOf11 ?x)
          )
          (and
            (AboveOf18 ?x)
            (BelowOf11 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf4 ?x)
          )
          (and
            (AboveOf17 ?x)
            (BelowOf8 ?x)
          )
          (and
            (AboveOf16 ?x)
            (BelowOf11 ?x)
          )
          (and
            (AboveOf16 ?x)
            (BelowOf1 ?x)
          )
          (and
            (AboveOf12 ?x)
            (BelowOf8 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf8 ?x)
          )
          (and
            (AboveOf11 ?x)
            (BelowOf8 ?x)
          )
          (and
            (AboveOf18 ?x)
            (BelowOf3 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf1 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf12 ?x)
          )
          (and
            (BelowOf9 ?x)
            (AboveOf11 ?x)
          )
          (and
            (AboveOf18 ?x)
            (BelowOf10 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf18 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf9 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf15 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf10 ?x)
          )
          (and
            (AboveOf10 ?x)
            (BelowOf3 ?x)
          )
          (and
            (BelowOf10 ?x)
            (AboveOf12 ?x)
          )
          (and
            (AboveOf15 ?x)
            (BelowOf3 ?x)
          )
          (and
            (AboveOf10 ?x)
            (BelowOf10 ?x)
          )
          (and
            (AboveOf15 ?x)
            (BelowOf7 ?x)
          )
          (and
            (BelowOf9 ?x)
            (AboveOf17 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf7 ?x)
          )
          (and
            (AboveOf18 ?x)
            (BelowOf6 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf17 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf11 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf10 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf17 ?x)
          )
          (and
            (BelowOf7 ?x)
            (AboveOf10 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf2 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf18 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf18 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf3 ?x)
          )
        )
        (and
          (Row11 ?x)
        )
      )
      (when
        (or
          (and
            (BelowOf9 ?x)
            (AboveOf12 ?x)
          )
          (and
            (BelowOf11 ?x)
            (AboveOf12 ?x)
          )
          (and
            (BelowOf3 ?x)
            (AboveOf12 ?x)
          )
          (and
            (BelowOf9 ?x)
            (AboveOf18 ?x)
          )
          (and
            (BelowOf10 ?x)
            (AboveOf11 ?x)
          )
          (and
            (BelowOf7 ?x)
            (AboveOf12 ?x)
          )
          (and
            (AboveOf16 ?x)
            (BelowOf8 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf11 ?x)
          )
          (and
            (AboveOf16 ?x)
            (BelowOf12 ?x)
          )
          (and
            (AboveOf15 ?x)
            (BelowOf1 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf14 ?x)
          )
          (and
            (BelowOf8 ?x)
            (AboveOf12 ?x)
          )
          (and
            (AboveOf17 ?x)
            (BelowOf10 ?x)
          )
          (and
            (AboveOf15 ?x)
            (BelowOf10 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf18 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf15 ?x)
          )
          (and
            (BelowOf12 ?x)
            (AboveOf12 ?x)
          )
          (and
            (AboveOf17 ?x)
            (BelowOf1 ?x)
          )
          (and
            (BelowOf11 ?x)
            (AboveOf11 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf6 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf4 ?x)
          )
          (and
            (AboveOf15 ?x)
            (BelowOf8 ?x)
          )
          (and
            (AboveOf15 ?x)
            (BelowOf9 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf12 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf3 ?x)
          )
          (and
            (BelowOf7 ?x)
            (AboveOf11 ?x)
          )
          (and
            (BelowOf7 ?x)
            (AboveOf17 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf11 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf12 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf16 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf12 ?x)
          )
          (and
            (AboveOf11 ?x)
            (BelowOf12 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf16 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf2 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf11 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf11 ?x)
          )
          (and
            (AboveOf16 ?x)
            (BelowOf10 ?x)
          )
          (and
            (AboveOf17 ?x)
            (BelowOf3 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf11 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf16 ?x)
          )
          (and
            (AboveOf16 ?x)
            (BelowOf9 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf6 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf7 ?x)
          )
          (and
            (AboveOf15 ?x)
            (BelowOf6 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf17 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf17 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf10 ?x)
          )
          (and
            (BelowOf6 ?x)
            (AboveOf18 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf3 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf12 ?x)
          )
          (and
            (AboveOf17 ?x)
            (BelowOf12 ?x)
          )
          (and
            (AboveOf11 ?x)
            (BelowOf3 ?x)
          )
          (and
            (AboveOf15 ?x)
            (BelowOf4 ?x)
          )
          (and
            (AboveOf17 ?x)
            (BelowOf6 ?x)
          )
          (and
            (AboveOf17 ?x)
            (BelowOf11 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf1 ?x)
          )
          (and
            (AboveOf16 ?x)
            (BelowOf4 ?x)
          )
          (and
            (AboveOf15 ?x)
            (BelowOf11 ?x)
          )
          (and
            (AboveOf16 ?x)
            (BelowOf6 ?x)
          )
          (and
            (AboveOf18 ?x)
            (BelowOf8 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf13 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf9 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf10 ?x)
          )
          (and
            (BelowOf7 ?x)
            (AboveOf18 ?x)
          )
          (and
            (AboveOf15 ?x)
            (BelowOf12 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf5 ?x)
          )
          (and
            (BelowOf6 ?x)
            (AboveOf12 ?x)
          )
          (and
            (BelowOf7 ?x)
            (AboveOf16 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf8 ?x)
          )
          (and
            (BelowOf6 ?x)
            (AboveOf11 ?x)
          )
          (and
            (AboveOf18 ?x)
            (BelowOf11 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf4 ?x)
          )
          (and
            (AboveOf17 ?x)
            (BelowOf8 ?x)
          )
          (and
            (AboveOf16 ?x)
            (BelowOf11 ?x)
          )
          (and
            (AboveOf16 ?x)
            (BelowOf1 ?x)
          )
          (and
            (AboveOf12 ?x)
            (BelowOf8 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf8 ?x)
          )
          (and
            (AboveOf11 ?x)
            (BelowOf8 ?x)
          )
          (and
            (AboveOf18 ?x)
            (BelowOf3 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf1 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf12 ?x)
          )
          (and
            (BelowOf9 ?x)
            (AboveOf11 ?x)
          )
          (and
            (AboveOf18 ?x)
            (BelowOf10 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf18 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf9 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf15 ?x)
          )
          (and
            (BelowOf10 ?x)
            (AboveOf12 ?x)
          )
          (and
            (AboveOf15 ?x)
            (BelowOf3 ?x)
          )
          (and
            (AboveOf15 ?x)
            (BelowOf7 ?x)
          )
          (and
            (BelowOf9 ?x)
            (AboveOf17 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf12 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf7 ?x)
          )
          (and
            (AboveOf18 ?x)
            (BelowOf6 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf17 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf11 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf17 ?x)
          )
          (and
            (AboveOf16 ?x)
            (BelowOf3 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf2 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf18 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf18 ?x)
          )
          (and
            (AboveOf18 ?x)
            (BelowOf12 ?x)
          )
        )
        (and
          (Row12 ?x)
        )
      )
      (when
        (or
          (and
            (AboveOf12 ?x)
            (BelowOf12 ?x)
          )
          (and
            (BelowOf9 ?x)
            (AboveOf12 ?x)
          )
          (and
            (BelowOf11 ?x)
            (AboveOf12 ?x)
          )
          (and
            (BelowOf3 ?x)
            (AboveOf12 ?x)
          )
          (and
            (BelowOf9 ?x)
            (AboveOf18 ?x)
          )
          (and
            (BelowOf7 ?x)
            (AboveOf12 ?x)
          )
          (and
            (AboveOf16 ?x)
            (BelowOf8 ?x)
          )
          (and
            (AboveOf16 ?x)
            (BelowOf12 ?x)
          )
          (and
            (BelowOf13 ?x)
            (AboveOf12 ?x)
          )
          (and
            (AboveOf15 ?x)
            (BelowOf1 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf14 ?x)
          )
          (and
            (BelowOf13 ?x)
            (AboveOf15 ?x)
          )
          (and
            (AboveOf17 ?x)
            (BelowOf10 ?x)
          )
          (and
            (AboveOf15 ?x)
            (BelowOf10 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf18 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf15 ?x)
          )
          (and
            (AboveOf17 ?x)
            (BelowOf1 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf6 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf4 ?x)
          )
          (and
            (AboveOf15 ?x)
            (BelowOf8 ?x)
          )
          (and
            (AboveOf15 ?x)
            (BelowOf9 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf12 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf3 ?x)
          )
          (and
            (BelowOf7 ?x)
            (AboveOf17 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf12 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf16 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf2 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf11 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf11 ?x)
          )
          (and
            (AboveOf16 ?x)
            (BelowOf10 ?x)
          )
          (and
            (AboveOf17 ?x)
            (BelowOf3 ?x)
          )
          (and
            (BelowOf13 ?x)
            (AboveOf14 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf16 ?x)
          )
          (and
            (AboveOf16 ?x)
            (BelowOf9 ?x)
          )
          (and
            (BelowOf13 ?x)
            (AboveOf13 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf6 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf7 ?x)
          )
          (and
            (AboveOf15 ?x)
            (BelowOf6 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf17 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf17 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf10 ?x)
          )
          (and
            (BelowOf6 ?x)
            (AboveOf18 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf3 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf12 ?x)
          )
          (and
            (AboveOf17 ?x)
            (BelowOf12 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf12 ?x)
          )
          (and
            (AboveOf15 ?x)
            (BelowOf4 ?x)
          )
          (and
            (AboveOf17 ?x)
            (BelowOf6 ?x)
          )
          (and
            (AboveOf17 ?x)
            (BelowOf11 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf1 ?x)
          )
          (and
            (BelowOf13 ?x)
            (AboveOf17 ?x)
          )
          (and
            (AboveOf16 ?x)
            (BelowOf4 ?x)
          )
          (and
            (AboveOf15 ?x)
            (BelowOf11 ?x)
          )
          (and
            (AboveOf16 ?x)
            (BelowOf6 ?x)
          )
          (and
            (AboveOf18 ?x)
            (BelowOf8 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf13 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf9 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf10 ?x)
          )
          (and
            (BelowOf7 ?x)
            (AboveOf18 ?x)
          )
          (and
            (AboveOf15 ?x)
            (BelowOf12 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf5 ?x)
          )
          (and
            (BelowOf6 ?x)
            (AboveOf12 ?x)
          )
          (and
            (BelowOf7 ?x)
            (AboveOf16 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf8 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf4 ?x)
          )
          (and
            (AboveOf17 ?x)
            (BelowOf8 ?x)
          )
          (and
            (AboveOf16 ?x)
            (BelowOf11 ?x)
          )
          (and
            (AboveOf16 ?x)
            (BelowOf1 ?x)
          )
          (and
            (AboveOf12 ?x)
            (BelowOf8 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf8 ?x)
          )
          (and
            (AboveOf18 ?x)
            (BelowOf3 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf1 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf12 ?x)
          )
          (and
            (AboveOf18 ?x)
            (BelowOf11 ?x)
          )
          (and
            (AboveOf18 ?x)
            (BelowOf10 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf18 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf9 ?x)
          )
          (and
            (BelowOf13 ?x)
            (AboveOf16 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf15 ?x)
          )
          (and
            (BelowOf10 ?x)
            (AboveOf12 ?x)
          )
          (and
            (AboveOf15 ?x)
            (BelowOf3 ?x)
          )
          (and
            (AboveOf15 ?x)
            (BelowOf7 ?x)
          )
          (and
            (BelowOf13 ?x)
            (AboveOf18 ?x)
          )
          (and
            (BelowOf9 ?x)
            (AboveOf17 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf12 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf7 ?x)
          )
          (and
            (AboveOf18 ?x)
            (BelowOf6 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf17 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf16 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf17 ?x)
          )
          (and
            (AboveOf16 ?x)
            (BelowOf3 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf2 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf18 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf18 ?x)
          )
          (and
            (AboveOf18 ?x)
            (BelowOf12 ?x)
          )
        )
        (and
          (Row13 ?x)
        )
      )
      (when
        (or
          (and
            (BelowOf9 ?x)
            (AboveOf18 ?x)
          )
          (and
            (AboveOf16 ?x)
            (BelowOf10 ?x)
          )
          (and
            (AboveOf16 ?x)
            (BelowOf8 ?x)
          )
          (and
            (AboveOf16 ?x)
            (BelowOf12 ?x)
          )
          (and
            (AboveOf15 ?x)
            (BelowOf1 ?x)
          )
          (and
            (BelowOf14 ?x)
            (AboveOf17 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf14 ?x)
          )
          (and
            (BelowOf13 ?x)
            (AboveOf15 ?x)
          )
          (and
            (AboveOf17 ?x)
            (BelowOf10 ?x)
          )
          (and
            (AboveOf15 ?x)
            (BelowOf10 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf18 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf15 ?x)
          )
          (and
            (AboveOf17 ?x)
            (BelowOf1 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf6 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf4 ?x)
          )
          (and
            (AboveOf15 ?x)
            (BelowOf8 ?x)
          )
          (and
            (AboveOf15 ?x)
            (BelowOf9 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf12 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf3 ?x)
          )
          (and
            (BelowOf7 ?x)
            (AboveOf17 ?x)
          )
          (and
            (BelowOf14 ?x)
            (AboveOf18 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf16 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf14 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf2 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf11 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf13 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf11 ?x)
          )
          (and
            (AboveOf17 ?x)
            (BelowOf3 ?x)
          )
          (and
            (BelowOf13 ?x)
            (AboveOf14 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf14 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf16 ?x)
          )
          (and
            (AboveOf16 ?x)
            (BelowOf9 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf6 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf7 ?x)
          )
          (and
            (AboveOf15 ?x)
            (BelowOf6 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf17 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf17 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf10 ?x)
          )
          (and
            (BelowOf6 ?x)
            (AboveOf18 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf3 ?x)
          )
          (and
            (AboveOf17 ?x)
            (BelowOf12 ?x)
          )
          (and
            (BelowOf14 ?x)
            (AboveOf16 ?x)
          )
          (and
            (AboveOf15 ?x)
            (BelowOf4 ?x)
          )
          (and
            (AboveOf17 ?x)
            (BelowOf6 ?x)
          )
          (and
            (AboveOf17 ?x)
            (BelowOf11 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf1 ?x)
          )
          (and
            (BelowOf13 ?x)
            (AboveOf17 ?x)
          )
          (and
            (AboveOf16 ?x)
            (BelowOf4 ?x)
          )
          (and
            (AboveOf15 ?x)
            (BelowOf11 ?x)
          )
          (and
            (AboveOf16 ?x)
            (BelowOf6 ?x)
          )
          (and
            (AboveOf18 ?x)
            (BelowOf8 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf9 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf10 ?x)
          )
          (and
            (BelowOf7 ?x)
            (AboveOf18 ?x)
          )
          (and
            (AboveOf15 ?x)
            (BelowOf12 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf5 ?x)
          )
          (and
            (BelowOf7 ?x)
            (AboveOf16 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf8 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf4 ?x)
          )
          (and
            (AboveOf17 ?x)
            (BelowOf8 ?x)
          )
          (and
            (AboveOf16 ?x)
            (BelowOf11 ?x)
          )
          (and
            (AboveOf16 ?x)
            (BelowOf1 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf8 ?x)
          )
          (and
            (AboveOf18 ?x)
            (BelowOf3 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf1 ?x)
          )
          (and
            (AboveOf18 ?x)
            (BelowOf11 ?x)
          )
          (and
            (AboveOf18 ?x)
            (BelowOf10 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf18 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf9 ?x)
          )
          (and
            (BelowOf13 ?x)
            (AboveOf16 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf15 ?x)
          )
          (and
            (AboveOf15 ?x)
            (BelowOf3 ?x)
          )
          (and
            (AboveOf15 ?x)
            (BelowOf7 ?x)
          )
          (and
            (BelowOf13 ?x)
            (AboveOf18 ?x)
          )
          (and
            (BelowOf9 ?x)
            (AboveOf17 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf12 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf7 ?x)
          )
          (and
            (AboveOf18 ?x)
            (BelowOf6 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf17 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf16 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf17 ?x)
          )
          (and
            (AboveOf16 ?x)
            (BelowOf3 ?x)
          )
          (and
            (AboveOf13 ?x)
            (BelowOf2 ?x)
          )
          (and
            (BelowOf14 ?x)
            (AboveOf15 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf18 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf18 ?x)
          )
          (and
            (AboveOf18 ?x)
            (BelowOf12 ?x)
          )
        )
        (and
          (Row14 ?x)
        )
      )
      (when
        (or
          (and
            (BelowOf15 ?x)
            (AboveOf18 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf8 ?x)
          )
          (and
            (BelowOf15 ?x)
            (AboveOf16 ?x)
          )
          (and
            (AboveOf18 ?x)
            (BelowOf3 ?x)
          )
          (and
            (AboveOf15 ?x)
            (BelowOf8 ?x)
          )
          (and
            (AboveOf15 ?x)
            (BelowOf1 ?x)
          )
          (and
            (AboveOf15 ?x)
            (BelowOf9 ?x)
          )
          (and
            (BelowOf15 ?x)
            (AboveOf17 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf12 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf3 ?x)
          )
          (and
            (AboveOf18 ?x)
            (BelowOf11 ?x)
          )
          (and
            (BelowOf7 ?x)
            (AboveOf17 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf7 ?x)
          )
          (and
            (AboveOf17 ?x)
            (BelowOf12 ?x)
          )
          (and
            (BelowOf9 ?x)
            (AboveOf18 ?x)
          )
          (and
            (BelowOf14 ?x)
            (AboveOf16 ?x)
          )
          (and
            (AboveOf18 ?x)
            (BelowOf10 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf16 ?x)
          )
          (and
            (AboveOf15 ?x)
            (BelowOf4 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf18 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf11 ?x)
          )
          (and
            (AboveOf16 ?x)
            (BelowOf10 ?x)
          )
          (and
            (BelowOf13 ?x)
            (AboveOf16 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf15 ?x)
          )
          (and
            (AboveOf16 ?x)
            (BelowOf8 ?x)
          )
          (and
            (AboveOf17 ?x)
            (BelowOf6 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf4 ?x)
          )
          (and
            (BelowOf14 ?x)
            (AboveOf18 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf1 ?x)
          )
          (and
            (AboveOf16 ?x)
            (BelowOf12 ?x)
          )
          (and
            (AboveOf15 ?x)
            (BelowOf3 ?x)
          )
          (and
            (BelowOf13 ?x)
            (AboveOf17 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf18 ?x)
          )
          (and
            (AboveOf15 ?x)
            (BelowOf11 ?x)
          )
          (and
            (AboveOf18 ?x)
            (BelowOf8 ?x)
          )
          (and
            (BelowOf13 ?x)
            (AboveOf18 ?x)
          )
          (and
            (BelowOf9 ?x)
            (AboveOf17 ?x)
          )
          (and
            (BelowOf14 ?x)
            (AboveOf17 ?x)
          )
          (and
            (AboveOf16 ?x)
            (BelowOf6 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf14 ?x)
          )
          (and
            (AboveOf15 ?x)
            (BelowOf6 ?x)
          )
          (and
            (BelowOf13 ?x)
            (AboveOf15 ?x)
          )
          (and
            (AboveOf18 ?x)
            (BelowOf6 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf17 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf16 ?x)
          )
          (and
            (AboveOf17 ?x)
            (BelowOf10 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf9 ?x)
          )
          (and
            (AboveOf16 ?x)
            (BelowOf3 ?x)
          )
          (and
            (BelowOf13 ?x)
            (AboveOf14 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf2 ?x)
          )
          (and
            (AboveOf16 ?x)
            (BelowOf4 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf14 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf10 ?x)
          )
          (and
            (AboveOf17 ?x)
            (BelowOf3 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf15 ?x)
          )
          (and
            (AboveOf15 ?x)
            (BelowOf15 ?x)
          )
          (and
            (AboveOf17 ?x)
            (BelowOf1 ?x)
          )
          (and
            (AboveOf15 ?x)
            (BelowOf10 ?x)
          )
          (and
            (BelowOf14 ?x)
            (AboveOf15 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf16 ?x)
          )
          (and
            (BelowOf7 ?x)
            (AboveOf18 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf15 ?x)
          )
          (and
            (AboveOf16 ?x)
            (BelowOf9 ?x)
          )
          (and
            (AboveOf15 ?x)
            (BelowOf7 ?x)
          )
          (and
            (AboveOf15 ?x)
            (BelowOf12 ?x)
          )
          (and
            (BelowOf7 ?x)
            (AboveOf16 ?x)
          )
          (and
            (AboveOf14 ?x)
            (BelowOf6 ?x)
          )
          (and
            (BelowOf6 ?x)
            (AboveOf18 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf17 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf18 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf18 ?x)
          )
          (and
            (AboveOf17 ?x)
            (BelowOf8 ?x)
          )
          (and
            (AboveOf16 ?x)
            (BelowOf11 ?x)
          )
          (and
            (AboveOf16 ?x)
            (BelowOf1 ?x)
          )
          (and
            (AboveOf17 ?x)
            (BelowOf11 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf17 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf17 ?x)
          )
          (and
            (AboveOf18 ?x)
            (BelowOf12 ?x)
          )
        )
        (and
          (Row15 ?x)
        )
      )
      (when
        (or
          (and
            (BelowOf15 ?x)
            (AboveOf18 ?x)
          )
          (and
            (AboveOf18 ?x)
            (BelowOf3 ?x)
          )
          (and
            (AboveOf15 ?x)
            (BelowOf8 ?x)
          )
          (and
            (AboveOf15 ?x)
            (BelowOf1 ?x)
          )
          (and
            (AboveOf15 ?x)
            (BelowOf9 ?x)
          )
          (and
            (BelowOf15 ?x)
            (AboveOf17 ?x)
          )
          (and
            (BelowOf16 ?x)
            (AboveOf18 ?x)
          )
          (and
            (BelowOf6 ?x)
            (AboveOf18 ?x)
          )
          (and
            (AboveOf15 ?x)
            (BelowOf6 ?x)
          )
          (and
            (AboveOf18 ?x)
            (BelowOf11 ?x)
          )
          (and
            (BelowOf7 ?x)
            (AboveOf17 ?x)
          )
          (and
            (AboveOf17 ?x)
            (BelowOf12 ?x)
          )
          (and
            (BelowOf9 ?x)
            (AboveOf18 ?x)
          )
          (and
            (BelowOf14 ?x)
            (AboveOf16 ?x)
          )
          (and
            (AboveOf18 ?x)
            (BelowOf10 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf16 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf17 ?x)
          )
          (and
            (AboveOf15 ?x)
            (BelowOf4 ?x)
          )
          (and
            (AboveOf16 ?x)
            (BelowOf10 ?x)
          )
          (and
            (BelowOf13 ?x)
            (AboveOf16 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf15 ?x)
          )
          (and
            (AboveOf15 ?x)
            (BelowOf16 ?x)
          )
          (and
            (AboveOf16 ?x)
            (BelowOf8 ?x)
          )
          (and
            (AboveOf17 ?x)
            (BelowOf6 ?x)
          )
          (and
            (AboveOf17 ?x)
            (BelowOf11 ?x)
          )
          (and
            (AboveOf16 ?x)
            (BelowOf12 ?x)
          )
          (and
            (AboveOf15 ?x)
            (BelowOf3 ?x)
          )
          (and
            (BelowOf13 ?x)
            (AboveOf17 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf18 ?x)
          )
          (and
            (AboveOf15 ?x)
            (BelowOf11 ?x)
          )
          (and
            (BelowOf16 ?x)
            (AboveOf16 ?x)
          )
          (and
            (BelowOf13 ?x)
            (AboveOf18 ?x)
          )
          (and
            (BelowOf9 ?x)
            (AboveOf17 ?x)
          )
          (and
            (BelowOf14 ?x)
            (AboveOf17 ?x)
          )
          (and
            (AboveOf16 ?x)
            (BelowOf6 ?x)
          )
          (and
            (BelowOf16 ?x)
            (AboveOf17 ?x)
          )
          (and
            (BelowOf13 ?x)
            (AboveOf15 ?x)
          )
          (and
            (AboveOf18 ?x)
            (BelowOf6 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf17 ?x)
          )
          (and
            (AboveOf18 ?x)
            (BelowOf8 ?x)
          )
          (and
            (BelowOf14 ?x)
            (AboveOf18 ?x)
          )
          (and
            (AboveOf17 ?x)
            (BelowOf10 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf17 ?x)
          )
          (and
            (AboveOf16 ?x)
            (BelowOf3 ?x)
          )
          (and
            (BelowOf15 ?x)
            (AboveOf16 ?x)
          )
          (and
            (AboveOf16 ?x)
            (BelowOf4 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf18 ?x)
          )
          (and
            (AboveOf17 ?x)
            (BelowOf3 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf15 ?x)
          )
          (and
            (AboveOf15 ?x)
            (BelowOf15 ?x)
          )
          (and
            (AboveOf17 ?x)
            (BelowOf1 ?x)
          )
          (and
            (AboveOf15 ?x)
            (BelowOf10 ?x)
          )
          (and
            (BelowOf14 ?x)
            (AboveOf15 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf16 ?x)
          )
          (and
            (BelowOf7 ?x)
            (AboveOf18 ?x)
          )
          (and
            (AboveOf16 ?x)
            (BelowOf9 ?x)
          )
          (and
            (AboveOf15 ?x)
            (BelowOf7 ?x)
          )
          (and
            (AboveOf15 ?x)
            (BelowOf12 ?x)
          )
          (and
            (BelowOf7 ?x)
            (AboveOf16 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf18 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf18 ?x)
          )
          (and
            (AboveOf17 ?x)
            (BelowOf8 ?x)
          )
          (and
            (AboveOf16 ?x)
            (BelowOf11 ?x)
          )
          (and
            (AboveOf16 ?x)
            (BelowOf1 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf16 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf17 ?x)
          )
          (and
            (AboveOf18 ?x)
            (BelowOf12 ?x)
          )
        )
        (and
          (Row16 ?x)
        )
      )
      (when
        (or
          (and
            (BelowOf15 ?x)
            (AboveOf18 ?x)
          )
          (and
            (AboveOf18 ?x)
            (BelowOf3 ?x)
          )
          (and
            (AboveOf16 ?x)
            (BelowOf6 ?x)
          )
          (and
            (BelowOf15 ?x)
            (AboveOf17 ?x)
          )
          (and
            (BelowOf16 ?x)
            (AboveOf18 ?x)
          )
          (and
            (BelowOf6 ?x)
            (AboveOf18 ?x)
          )
          (and
            (AboveOf18 ?x)
            (BelowOf11 ?x)
          )
          (and
            (BelowOf7 ?x)
            (AboveOf17 ?x)
          )
          (and
            (AboveOf17 ?x)
            (BelowOf12 ?x)
          )
          (and
            (BelowOf9 ?x)
            (AboveOf18 ?x)
          )
          (and
            (BelowOf14 ?x)
            (AboveOf16 ?x)
          )
          (and
            (AboveOf18 ?x)
            (BelowOf10 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf16 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf17 ?x)
          )
          (and
            (BelowOf17 ?x)
            (AboveOf17 ?x)
          )
          (and
            (BelowOf13 ?x)
            (AboveOf16 ?x)
          )
          (and
            (AboveOf16 ?x)
            (BelowOf8 ?x)
          )
          (and
            (AboveOf17 ?x)
            (BelowOf6 ?x)
          )
          (and
            (AboveOf17 ?x)
            (BelowOf11 ?x)
          )
          (and
            (AboveOf16 ?x)
            (BelowOf12 ?x)
          )
          (and
            (BelowOf13 ?x)
            (AboveOf17 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf18 ?x)
          )
          (and
            (BelowOf16 ?x)
            (AboveOf16 ?x)
          )
          (and
            (BelowOf13 ?x)
            (AboveOf18 ?x)
          )
          (and
            (BelowOf9 ?x)
            (AboveOf17 ?x)
          )
          (and
            (BelowOf14 ?x)
            (AboveOf17 ?x)
          )
          (and
            (BelowOf16 ?x)
            (AboveOf17 ?x)
          )
          (and
            (AboveOf18 ?x)
            (BelowOf6 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf17 ?x)
          )
          (and
            (AboveOf18 ?x)
            (BelowOf8 ?x)
          )
          (and
            (BelowOf14 ?x)
            (AboveOf18 ?x)
          )
          (and
            (AboveOf17 ?x)
            (BelowOf10 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf17 ?x)
          )
          (and
            (AboveOf16 ?x)
            (BelowOf3 ?x)
          )
          (and
            (BelowOf15 ?x)
            (AboveOf16 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf18 ?x)
          )
          (and
            (AboveOf17 ?x)
            (BelowOf3 ?x)
          )
          (and
            (BelowOf7 ?x)
            (AboveOf18 ?x)
          )
          (and
            (AboveOf17 ?x)
            (BelowOf1 ?x)
          )
          (and
            (AboveOf16 ?x)
            (BelowOf17 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf16 ?x)
          )
          (and
            (AboveOf16 ?x)
            (BelowOf9 ?x)
          )
          (and
            (BelowOf17 ?x)
            (AboveOf18 ?x)
          )
          (and
            (BelowOf7 ?x)
            (AboveOf16 ?x)
          )
          (and
            (AboveOf16 ?x)
            (BelowOf10 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf18 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf18 ?x)
          )
          (and
            (AboveOf17 ?x)
            (BelowOf8 ?x)
          )
          (and
            (AboveOf16 ?x)
            (BelowOf11 ?x)
          )
          (and
            (AboveOf16 ?x)
            (BelowOf1 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf16 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf17 ?x)
          )
          (and
            (AboveOf18 ?x)
            (BelowOf12 ?x)
          )
        )
        (and
          (Row17 ?x)
        )
      )
      (when
        (or
          (and
            (BelowOf15 ?x)
            (AboveOf18 ?x)
          )
          (and
            (BelowOf9 ?x)
            (AboveOf17 ?x)
          )
          (and
            (AboveOf18 ?x)
            (BelowOf3 ?x)
          )
          (and
            (BelowOf15 ?x)
            (AboveOf17 ?x)
          )
          (and
            (BelowOf16 ?x)
            (AboveOf18 ?x)
          )
          (and
            (BelowOf6 ?x)
            (AboveOf18 ?x)
          )
          (and
            (AboveOf18 ?x)
            (BelowOf11 ?x)
          )
          (and
            (BelowOf7 ?x)
            (AboveOf17 ?x)
          )
          (and
            (AboveOf17 ?x)
            (BelowOf12 ?x)
          )
          (and
            (BelowOf9 ?x)
            (AboveOf18 ?x)
          )
          (and
            (BelowOf14 ?x)
            (AboveOf18 ?x)
          )
          (and
            (AboveOf18 ?x)
            (BelowOf10 ?x)
          )
          (and
            (BelowOf1 ?x)
            (AboveOf18 ?x)
          )
          (and
            (BelowOf17 ?x)
            (AboveOf17 ?x)
          )
          (and
            (AboveOf17 ?x)
            (BelowOf6 ?x)
          )
          (and
            (AboveOf17 ?x)
            (BelowOf11 ?x)
          )
          (and
            (BelowOf13 ?x)
            (AboveOf17 ?x)
          )
          (and
            (BelowOf13 ?x)
            (AboveOf18 ?x)
          )
          (and
            (AboveOf18 ?x)
            (BelowOf18 ?x)
          )
          (and
            (BelowOf14 ?x)
            (AboveOf17 ?x)
          )
          (and
            (BelowOf16 ?x)
            (AboveOf17 ?x)
          )
          (and
            (AboveOf18 ?x)
            (BelowOf6 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf17 ?x)
          )
          (and
            (AboveOf18 ?x)
            (BelowOf8 ?x)
          )
          (and
            (AboveOf17 ?x)
            (BelowOf18 ?x)
          )
          (and
            (AboveOf17 ?x)
            (BelowOf10 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf17 ?x)
          )
          (and
            (BelowOf4 ?x)
            (AboveOf18 ?x)
          )
          (and
            (AboveOf17 ?x)
            (BelowOf3 ?x)
          )
          (and
            (BelowOf7 ?x)
            (AboveOf18 ?x)
          )
          (and
            (AboveOf17 ?x)
            (BelowOf1 ?x)
          )
          (and
            (BelowOf17 ?x)
            (AboveOf18 ?x)
          )
          (and
            (BelowOf2 ?x)
            (AboveOf18 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf18 ?x)
          )
          (and
            (AboveOf17 ?x)
            (BelowOf8 ?x)
          )
          (and
            (BelowOf5 ?x)
            (AboveOf17 ?x)
          )
          (and
            (AboveOf18 ?x)
            (BelowOf12 ?x)
          )
        )
        (and
          (Row18 ?x)
        )
      )
    )
  )
  (:action CheckConsistencyAction
    :parameters ( )
    :precondition ( and
      (CheckConsistency)
      (not (Error))
    )
    :effect ( and
      (not (CheckConsistency))
      (when
        (or
          (exists (?x_16 )
            (and
              (BelowOf4 ?x_16)
              (AboveOf12 ?x_16)
            )
          )
          (exists (?x_34 )
            (and
              (RightOf18 ?x_34)
              (LeftOf5 ?x_34)
            )
          )
          (exists (?x_30 )
            (and
              (LeftOf6 ?x_30)
              (RightOf15 ?x_30)
            )
          )
          (exists (?x_26 )
            (and
              (BelowOf4 ?x_26)
              (AboveOf14 ?x_26)
            )
          )
          (exists (?x_35 )
            (and
              (BelowOf3 ?x_35)
              (AboveOf13 ?x_35)
            )
          )
          (exists (?x_33 )
            (and
              (LeftOf1 ?x_33)
              (RightOf16 ?x_33)
            )
          )
          (exists (?x_24 )
            (and
              (RightOf14 ?x_24)
              (LeftOf4 ?x_24)
            )
          )
          (exists (?x_10 )
            (and
              (LeftOf3 ?x_10)
              (RightOf14 ?x_10)
            )
          )
          (exists (?x_19 )
            (and
              (RightOf12 ?x_19)
              (LeftOf2 ?x_19)
            )
          )
          (exists (?x_24 )
            (and
              (LeftOf6 ?x_24)
              (RightOf18 ?x_24)
            )
          )
          (exists (?x_34 )
            (and
              (LeftOf6 ?x_34)
              (RightOf11 ?x_34)
            )
          )
          (exists (?x_14 )
            (and
              (AboveOf18 ?x_14)
              (BelowOf4 ?x_14)
            )
          )
          (exists (?x_16 )
            (and
              (BelowOf9 ?x_16)
              (AboveOf17 ?x_16)
            )
          )
          (exists (?x_33 )
            (and
              (LeftOf13 ?x_33)
              (RightOf16 ?x_33)
            )
          )
          (exists (?x_33 )
            (and
              (LeftOf15 ?x_33)
              (RightOf18 ?x_33)
            )
          )
          (exists (?x_18 )
            (and
              (AboveOf18 ?x_18)
              (BelowOf1 ?x_18)
            )
          )
          (exists (?x_24 )
            (and
              (RightOf12 ?x_24)
              (LeftOf4 ?x_24)
            )
          )
          (exists (?x_26 )
            (and
              (AboveOf16 ?x_26)
              (BelowOf8 ?x_26)
            )
          )
          (exists (?x_35 )
            (and
              (AboveOf17 ?x_35)
              (BelowOf9 ?x_35)
            )
          )
          (exists (?x_14 )
            (and
              (AboveOf18 ?x_14)
              (BelowOf5 ?x_14)
            )
          )
          (exists (?x_6 )
            (and
              (BelowOf6 ?x_6)
              (AboveOf12 ?x_6)
            )
          )
          (exists (?x_4 )
            (and
              (RightOf6 ?x_4)
              (LeftOf1 ?x_4)
            )
          )
          (exists (?x_32 )
            (and
              (RightOf17 ?x_32)
              (LeftOf4 ?x_32)
            )
          )
          (exists (?x_13 )
            (and
              (LeftOf1 ?x_13)
              (RightOf11 ?x_13)
            )
          )
          (exists (?x_9 )
            (and
              (BelowOf7 ?x_9)
              (AboveOf15 ?x_9)
            )
          )
          (exists (?x_0 )
            (and
              (BelowOf4 ?x_0)
              (AboveOf16 ?x_0)
            )
          )
          (exists (?x_30 )
            (and
              (LeftOf10 ?x_30)
              (RightOf14 ?x_30)
            )
          )
          (exists (?x_3 )
            (and
              (RightOf3 ?x_3)
              (LeftOf2 ?x_3)
            )
          )
          (exists (?x_25 )
            (and
              (RightOf11 ?x_25)
              (LeftOf1 ?x_25)
            )
          )
          (exists (?x_9 )
            (and
              (AboveOf11 ?x_9)
              (BelowOf1 ?x_9)
            )
          )
          (exists (?x_29 )
            (and
              (AboveOf17 ?x_29)
              (BelowOf2 ?x_29)
            )
          )
          (exists (?x_31 )
            (and
              (AboveOf17 ?x_31)
              (BelowOf5 ?x_31)
            )
          )
          (exists (?x_9 )
            (and
              (BelowOf6 ?x_9)
              (AboveOf12 ?x_9)
            )
          )
          (exists (?x_25 )
            (and
              (LeftOf6 ?x_25)
              (RightOf12 ?x_25)
            )
          )
          (exists (?x_29 )
            (and
              (BelowOf1 ?x_29)
              (AboveOf10 ?x_29)
            )
          )
          (exists (?x_7 )
            (and
              (LeftOf5 ?x_7)
              (RightOf16 ?x_7)
            )
          )
          (exists (?x_15 )
            (and
              (AboveOf18 ?x_15)
              (BelowOf3 ?x_15)
            )
          )
          (exists (?x_24 )
            (and
              (RightOf11 ?x_24)
              (LeftOf1 ?x_24)
            )
          )
          (exists (?x_22 )
            (and
              (RightOf13 ?x_22)
              (LeftOf3 ?x_22)
            )
          )
          (exists (?x_8 )
            (and
              (RightOf18 ?x_8)
              (LeftOf1 ?x_8)
            )
          )
          (exists (?x_1 )
            (and
              (BelowOf8 ?x_1)
              (AboveOf17 ?x_1)
            )
          )
          (exists (?x_0 )
            (and
              (AboveOf17 ?x_0)
              (BelowOf8 ?x_0)
            )
          )
          (exists (?x_7 )
            (and
              (LeftOf7 ?x_7)
              (RightOf16 ?x_7)
            )
          )
          (exists (?x_21 )
            (and
              (BelowOf3 ?x_21)
              (AboveOf11 ?x_21)
            )
          )
          (exists (?x_26 )
            (and
              (BelowOf1 ?x_26)
              (AboveOf17 ?x_26)
            )
          )
          (exists (?x_4 )
            (and
              (RightOf17 ?x_4)
              (LeftOf1 ?x_4)
            )
          )
          (exists (?x_19 )
            (and
              (LeftOf5 ?x_19)
              (RightOf16 ?x_19)
            )
          )
          (exists (?x_23 )
            (and
              (AboveOf14 ?x_23)
              (BelowOf2 ?x_23)
            )
          )
          (exists (?x_9 )
            (and
              (BelowOf2 ?x_9)
              (AboveOf18 ?x_9)
            )
          )
          (exists (?x_10 )
            (and
              (LeftOf9 ?x_10)
              (RightOf18 ?x_10)
            )
          )
          (exists (?x_6 )
            (and
              (AboveOf9 ?x_6)
              (BelowOf4 ?x_6)
            )
          )
          (exists (?x_4 )
            (and
              (LeftOf1 ?x_4)
              (RightOf5 ?x_4)
            )
          )
          (exists (?x_11 )
            (and
              (BelowOf4 ?x_11)
              (AboveOf18 ?x_11)
            )
          )
          (exists (?x_9 )
            (and
              (AboveOf15 ?x_9)
              (BelowOf1 ?x_9)
            )
          )
          (exists (?x_34 )
            (and
              (LeftOf4 ?x_34)
              (RightOf9 ?x_34)
            )
          )
          (exists (?x_9 )
            (and
              (AboveOf14 ?x_9)
              (BelowOf2 ?x_9)
            )
          )
          (exists (?x_35 )
            (and
              (BelowOf8 ?x_35)
              (AboveOf10 ?x_35)
            )
          )
          (exists (?x_7 )
            (and
              (RightOf15 ?x_7)
              (LeftOf6 ?x_7)
            )
          )
          (exists (?x_23 )
            (and
              (BelowOf5 ?x_23)
              (AboveOf15 ?x_23)
            )
          )
          (exists (?x_12 )
            (and
              (AboveOf18 ?x_12)
              (BelowOf15 ?x_12)
            )
          )
          (exists (?x_30 )
            (and
              (RightOf13 ?x_30)
              (LeftOf1 ?x_30)
            )
          )
          (exists (?x_31 )
            (and
              (BelowOf4 ?x_31)
              (AboveOf13 ?x_31)
            )
          )
          (exists (?x_19 )
            (and
              (RightOf11 ?x_19)
              (LeftOf1 ?x_19)
            )
          )
          (exists (?x_35 )
            (and
              (BelowOf8 ?x_35)
              (AboveOf12 ?x_35)
            )
          )
          (exists (?x_0 )
            (and
              (BelowOf3 ?x_0)
              (AboveOf17 ?x_0)
            )
          )
          (exists (?x_21 )
            (and
              (BelowOf2 ?x_21)
              (AboveOf10 ?x_21)
            )
          )
          (exists (?x_22 )
            (and
              (LeftOf11 ?x_22)
              (RightOf15 ?x_22)
            )
          )
          (exists (?x_29 )
            (and
              (AboveOf7 ?x_29)
              (BelowOf1 ?x_29)
            )
          )
          (exists (?x_30 )
            (and
              (LeftOf12 ?x_30)
              (RightOf13 ?x_30)
            )
          )
          (exists (?x_22 )
            (and
              (LeftOf8 ?x_22)
              (RightOf12 ?x_22)
            )
          )
          (exists (?x_15 )
            (and
              (BelowOf1 ?x_15)
              (AboveOf8 ?x_15)
            )
          )
          (exists (?x_19 )
            (and
              (RightOf15 ?x_19)
              (LeftOf9 ?x_19)
            )
          )
          (exists (?x_17 )
            (and
              (AboveOf5 ?x_17)
              (BelowOf4 ?x_17)
            )
          )
          (exists (?x_6 )
            (and
              (BelowOf1 ?x_6)
              (AboveOf14 ?x_6)
            )
          )
          (exists (?x_13 )
            (and
              (LeftOf1 ?x_13)
              (RightOf9 ?x_13)
            )
          )
          (exists (?x_25 )
            (and
              (RightOf16 ?x_25)
              (LeftOf4 ?x_25)
            )
          )
          (exists (?x_21 )
            (and
              (BelowOf3 ?x_21)
              (AboveOf4 ?x_21)
            )
          )
          (exists (?x_19 )
            (and
              (LeftOf4 ?x_19)
              (RightOf14 ?x_19)
            )
          )
          (exists (?x_27 )
            (and
              (LeftOf5 ?x_27)
              (RightOf8 ?x_27)
            )
          )
          (exists (?x_8 )
            (and
              (LeftOf1 ?x_8)
              (RightOf8 ?x_8)
            )
          )
          (exists (?x_27 )
            (and
              (LeftOf5 ?x_27)
              (RightOf6 ?x_27)
            )
          )
          (exists (?x_32 )
            (and
              (LeftOf6 ?x_32)
              (RightOf18 ?x_32)
            )
          )
          (exists (?x_28 )
            (and
              (LeftOf7 ?x_28)
              (RightOf18 ?x_28)
            )
          )
          (exists (?x_34 )
            (and
              (LeftOf3 ?x_34)
              (RightOf17 ?x_34)
            )
          )
          (exists (?x_0 )
            (and
              (BelowOf5 ?x_0)
              (AboveOf18 ?x_0)
            )
          )
          (exists (?x_21 )
            (and
              (AboveOf17 ?x_21)
              (BelowOf1 ?x_21)
            )
          )
          (exists (?x_19 )
            (and
              (RightOf18 ?x_19)
              (LeftOf10 ?x_19)
            )
          )
          (exists (?x_23 )
            (and
              (AboveOf10 ?x_23)
              (BelowOf4 ?x_23)
            )
          )
          (exists (?x_19 )
            (and
              (LeftOf8 ?x_19)
              (RightOf16 ?x_19)
            )
          )
          (exists (?x_3 )
            (and
              (RightOf8 ?x_3)
              (LeftOf1 ?x_3)
            )
          )
          (exists (?x_31 )
            (and
              (BelowOf8 ?x_31)
              (AboveOf14 ?x_31)
            )
          )
          (exists (?x_31 )
            (and
              (BelowOf1 ?x_31)
              (AboveOf17 ?x_31)
            )
          )
          (exists (?x_2 )
            (and
              (AboveOf14 ?x_2)
              (BelowOf11 ?x_2)
            )
          )
          (exists (?x_24 )
            (and
              (RightOf9 ?x_24)
              (LeftOf1 ?x_24)
            )
          )
          (exists (?x_34 )
            (and
              (LeftOf8 ?x_34)
              (RightOf9 ?x_34)
            )
          )
          (exists (?x_10 )
            (and
              (RightOf15 ?x_10)
              (LeftOf14 ?x_10)
            )
          )
          (exists (?x_31 )
            (and
              (AboveOf16 ?x_31)
              (BelowOf8 ?x_31)
            )
          )
          (exists (?x_10 )
            (and
              (LeftOf7 ?x_10)
              (RightOf16 ?x_10)
            )
          )
          (exists (?x_24 )
            (and
              (RightOf18 ?x_24)
              (LeftOf1 ?x_24)
            )
          )
          (exists (?x_30 )
            (and
              (LeftOf6 ?x_30)
              (RightOf17 ?x_30)
            )
          )
          (exists (?x_9 )
            (and
              (AboveOf7 ?x_9)
              (BelowOf6 ?x_9)
            )
          )
          (exists (?x_15 )
            (and
              (AboveOf16 ?x_15)
              (BelowOf5 ?x_15)
            )
          )
          (exists (?x_5 )
            (and
              (RightOf10 ?x_5)
              (LeftOf8 ?x_5)
            )
          )
          (exists (?x_23 )
            (and
              (BelowOf4 ?x_23)
              (AboveOf16 ?x_23)
            )
          )
          (exists (?x_2 )
            (and
              (AboveOf15 ?x_2)
              (BelowOf10 ?x_2)
            )
          )
          (exists (?x_0 )
            (and
              (BelowOf5 ?x_0)
              (AboveOf11 ?x_0)
            )
          )
          (exists (?x_25 )
            (and
              (LeftOf1 ?x_25)
              (RightOf8 ?x_25)
            )
          )
          (exists (?x_33 )
            (and
              (RightOf18 ?x_33)
              (LeftOf4 ?x_33)
            )
          )
          (exists (?x_24 )
            (and
              (LeftOf4 ?x_24)
              (RightOf8 ?x_24)
            )
          )
          (exists (?x_13 )
            (and
              (LeftOf4 ?x_13)
              (RightOf6 ?x_13)
            )
          )
          (exists (?x_22 )
            (and
              (LeftOf1 ?x_22)
              (RightOf16 ?x_22)
            )
          )
          (exists (?x_19 )
            (and
              (LeftOf2 ?x_19)
              (RightOf14 ?x_19)
            )
          )
          (exists (?x_27 )
            (and
              (LeftOf3 ?x_27)
              (RightOf18 ?x_27)
            )
          )
          (exists (?x_16 )
            (and
              (BelowOf7 ?x_16)
              (AboveOf11 ?x_16)
            )
          )
          (exists (?x_16 )
            (and
              (AboveOf11 ?x_16)
              (BelowOf1 ?x_16)
            )
          )
          (exists (?x_20 )
            (and
              (LeftOf1 ?x_20)
              (RightOf1 ?x_20)
            )
          )
          (exists (?x_24 )
            (and
              (RightOf11 ?x_24)
              (LeftOf2 ?x_24)
            )
          )
          (exists (?x_0 )
            (and
              (BelowOf1 ?x_0)
              (AboveOf13 ?x_0)
            )
          )
          (exists (?x_16 )
            (and
              (AboveOf16 ?x_16)
              (BelowOf3 ?x_16)
            )
          )
          (exists (?x_30 )
            (and
              (RightOf14 ?x_30)
              (LeftOf10 ?x_30)
            )
          )
          (exists (?x_8 )
            (and
              (LeftOf5 ?x_8)
              (RightOf14 ?x_8)
            )
          )
          (exists (?x_32 )
            (and
              (LeftOf3 ?x_32)
              (RightOf18 ?x_32)
            )
          )
          (exists (?x_23 )
            (and
              (BelowOf3 ?x_23)
              (AboveOf17 ?x_23)
            )
          )
          (exists (?x_11 )
            (and
              (BelowOf14 ?x_11)
              (AboveOf18 ?x_11)
            )
          )
          (exists (?x_12 )
            (and
              (AboveOf18 ?x_12)
              (BelowOf4 ?x_12)
            )
          )
          (exists (?x_7 )
            (and
              (RightOf17 ?x_7)
              (LeftOf15 ?x_7)
            )
          )
          (exists (?x_0 )
            (and
              (BelowOf6 ?x_0)
              (AboveOf13 ?x_0)
            )
          )
          (exists (?x_23 )
            (and
              (BelowOf5 ?x_23)
              (AboveOf7 ?x_23)
            )
          )
          (exists (?x_29 )
            (and
              (BelowOf1 ?x_29)
              (AboveOf18 ?x_29)
            )
          )
          (exists (?x_25 )
            (and
              (LeftOf3 ?x_25)
              (RightOf10 ?x_25)
            )
          )
          (exists (?x_10 )
            (and
              (RightOf16 ?x_10)
              (LeftOf12 ?x_10)
            )
          )
          (exists (?x_25 )
            (and
              (LeftOf4 ?x_25)
              (RightOf10 ?x_25)
            )
          )
          (exists (?x_30 )
            (and
              (RightOf18 ?x_30)
              (LeftOf4 ?x_30)
            )
          )
          (exists (?x_24 )
            (and
              (RightOf18 ?x_24)
              (LeftOf4 ?x_24)
            )
          )
          (exists (?x_12 )
            (and
              (BelowOf14 ?x_12)
              (AboveOf18 ?x_12)
            )
          )
          (exists (?x_19 )
            (and
              (RightOf11 ?x_19)
              (LeftOf4 ?x_19)
            )
          )
          (exists (?x_22 )
            (and
              (RightOf15 ?x_22)
              (LeftOf2 ?x_22)
            )
          )
          (exists (?x_16 )
            (and
              (AboveOf17 ?x_16)
              (BelowOf10 ?x_16)
            )
          )
          (exists (?x_23 )
            (and
              (AboveOf11 ?x_23)
              (BelowOf1 ?x_23)
            )
          )
          (exists (?x_10 )
            (and
              (RightOf18 ?x_10)
              (LeftOf6 ?x_10)
            )
          )
          (exists (?x_10 )
            (and
              (LeftOf7 ?x_10)
              (RightOf15 ?x_10)
            )
          )
          (exists (?x_5 )
            (and
              (RightOf16 ?x_5)
              (LeftOf2 ?x_5)
            )
          )
          (exists (?x_13 )
            (and
              (RightOf15 ?x_13)
              (LeftOf3 ?x_13)
            )
          )
          (exists (?x_32 )
            (and
              (LeftOf16 ?x_32)
              (RightOf18 ?x_32)
            )
          )
          (exists (?x_35 )
            (and
              (AboveOf16 ?x_35)
              (BelowOf2 ?x_35)
            )
          )
          (exists (?x_16 )
            (and
              (BelowOf1 ?x_16)
              (AboveOf15 ?x_16)
            )
          )
          (exists (?x_17 )
            (and
              (AboveOf12 ?x_17)
              (BelowOf3 ?x_17)
            )
          )
          (exists (?x_15 )
            (and
              (AboveOf10 ?x_15)
              (BelowOf7 ?x_15)
            )
          )
          (exists (?x_24 )
            (and
              (LeftOf4 ?x_24)
              (RightOf18 ?x_24)
            )
          )
          (exists (?x_18 )
            (and
              (AboveOf5 ?x_18)
              (BelowOf1 ?x_18)
            )
          )
          (exists (?x_26 )
            (and
              (BelowOf10 ?x_26)
              (AboveOf18 ?x_26)
            )
          )
          (exists (?x_31 )
            (and
              (BelowOf4 ?x_31)
              (AboveOf15 ?x_31)
            )
          )
          (exists (?x_35 )
            (and
              (AboveOf9 ?x_35)
              (BelowOf7 ?x_35)
            )
          )
          (exists (?x_26 )
            (and
              (BelowOf4 ?x_26)
              (AboveOf12 ?x_26)
            )
          )
          (exists (?x_7 )
            (and
              (LeftOf15 ?x_7)
              (RightOf15 ?x_7)
            )
          )
          (exists (?x_18 )
            (and
              (BelowOf1 ?x_18)
              (AboveOf9 ?x_18)
            )
          )
          (exists (?x_34 )
            (and
              (LeftOf1 ?x_34)
              (RightOf9 ?x_34)
            )
          )
          (exists (?x_8 )
            (and
              (RightOf10 ?x_8)
              (LeftOf6 ?x_8)
            )
          )
          (exists (?x_35 )
            (and
              (AboveOf11 ?x_35)
              (BelowOf4 ?x_35)
            )
          )
          (exists (?x_0 )
            (and
              (AboveOf15 ?x_0)
              (BelowOf5 ?x_0)
            )
          )
          (exists (?x_32 )
            (and
              (LeftOf15 ?x_32)
              (RightOf17 ?x_32)
            )
          )
          (exists (?x_31 )
            (and
              (BelowOf7 ?x_31)
              (AboveOf17 ?x_31)
            )
          )
          (exists (?x_17 )
            (and
              (AboveOf14 ?x_17)
              (BelowOf1 ?x_17)
            )
          )
          (exists (?x_19 )
            (and
              (LeftOf8 ?x_19)
              (RightOf11 ?x_19)
            )
          )
          (exists (?x_9 )
            (and
              (BelowOf5 ?x_9)
              (AboveOf15 ?x_9)
            )
          )
          (exists (?x_26 )
            (and
              (BelowOf7 ?x_26)
              (AboveOf17 ?x_26)
            )
          )
          (exists (?x_21 )
            (and
              (BelowOf3 ?x_21)
              (AboveOf16 ?x_21)
            )
          )
          (exists (?x_2 )
            (and
              (BelowOf11 ?x_2)
              (AboveOf16 ?x_2)
            )
          )
          (exists (?x_25 )
            (and
              (RightOf13 ?x_25)
              (LeftOf4 ?x_25)
            )
          )
          (exists (?x_1 )
            (and
              (BelowOf7 ?x_1)
              (AboveOf15 ?x_1)
            )
          )
          (exists (?x_2 )
            (and
              (AboveOf17 ?x_2)
              (BelowOf5 ?x_2)
            )
          )
          (exists (?x_15 )
            (and
              (AboveOf9 ?x_15)
              (BelowOf1 ?x_15)
            )
          )
          (exists (?x_5 )
            (and
              (RightOf13 ?x_5)
              (LeftOf5 ?x_5)
            )
          )
          (exists (?x_30 )
            (and
              (RightOf14 ?x_30)
              (LeftOf8 ?x_30)
            )
          )
          (exists (?x_21 )
            (and
              (AboveOf11 ?x_21)
              (BelowOf1 ?x_21)
            )
          )
          (exists (?x_15 )
            (and
              (BelowOf8 ?x_15)
              (AboveOf8 ?x_15)
            )
          )
          (exists (?x_4 )
            (and
              (LeftOf2 ?x_4)
              (RightOf2 ?x_4)
            )
          )
          (exists (?x_22 )
            (and
              (LeftOf9 ?x_22)
              (RightOf17 ?x_22)
            )
          )
          (exists (?x_8 )
            (and
              (RightOf13 ?x_8)
              (LeftOf8 ?x_8)
            )
          )
          (exists (?x_31 )
            (and
              (BelowOf6 ?x_31)
              (AboveOf14 ?x_31)
            )
          )
          (exists (?x_22 )
            (and
              (LeftOf10 ?x_22)
              (RightOf17 ?x_22)
            )
          )
          (exists (?x_34 )
            (and
              (RightOf10 ?x_34)
              (LeftOf1 ?x_34)
            )
          )
          (exists (?x_13 )
            (and
              (LeftOf3 ?x_13)
              (RightOf17 ?x_13)
            )
          )
          (exists (?x_34 )
            (and
              (LeftOf2 ?x_34)
              (RightOf18 ?x_34)
            )
          )
          (exists (?x_31 )
            (and
              (AboveOf13 ?x_31)
              (BelowOf5 ?x_31)
            )
          )
          (exists (?x_19 )
            (and
              (RightOf12 ?x_19)
              (LeftOf4 ?x_19)
            )
          )
          (exists (?x_10 )
            (and
              (RightOf16 ?x_10)
              (LeftOf10 ?x_10)
            )
          )
          (exists (?x_30 )
            (and
              (LeftOf10 ?x_30)
              (RightOf15 ?x_30)
            )
          )
          (exists (?x_34 )
            (and
              (LeftOf4 ?x_34)
              (RightOf15 ?x_34)
            )
          )
          (exists (?x_25 )
            (and
              (LeftOf1 ?x_25)
              (RightOf15 ?x_25)
            )
          )
          (exists (?x_31 )
            (and
              (AboveOf17 ?x_31)
              (BelowOf3 ?x_31)
            )
          )
          (exists (?x_34 )
            (and
              (RightOf12 ?x_34)
              (LeftOf3 ?x_34)
            )
          )
          (exists (?x_10 )
            (and
              (LeftOf2 ?x_10)
              (RightOf17 ?x_10)
            )
          )
          (exists (?x_31 )
            (and
              (BelowOf10 ?x_31)
              (AboveOf17 ?x_31)
            )
          )
          (exists (?x_26 )
            (and
              (AboveOf14 ?x_26)
              (BelowOf12 ?x_26)
            )
          )
          (exists (?x_16 )
            (and
              (BelowOf5 ?x_16)
              (AboveOf11 ?x_16)
            )
          )
          (exists (?x_30 )
            (and
              (LeftOf1 ?x_30)
              (RightOf15 ?x_30)
            )
          )
          (exists (?x_23 )
            (and
              (BelowOf3 ?x_23)
              (AboveOf16 ?x_23)
            )
          )
          (exists (?x_17 )
            (and
              (BelowOf2 ?x_17)
              (AboveOf11 ?x_17)
            )
          )
          (exists (?x_4 )
            (and
              (RightOf16 ?x_4)
              (LeftOf1 ?x_4)
            )
          )
          (exists (?x_31 )
            (and
              (BelowOf4 ?x_31)
              (AboveOf16 ?x_31)
            )
          )
          (exists (?x_30 )
            (and
              (RightOf17 ?x_30)
              (LeftOf10 ?x_30)
            )
          )
          (exists (?x_21 )
            (and
              (BelowOf1 ?x_21)
              (AboveOf10 ?x_21)
            )
          )
          (exists (?x_31 )
            (and
              (AboveOf15 ?x_31)
              (BelowOf8 ?x_31)
            )
          )
          (exists (?x_5 )
            (and
              (LeftOf9 ?x_5)
              (RightOf12 ?x_5)
            )
          )
          (exists (?x_19 )
            (and
              (RightOf15 ?x_19)
              (LeftOf2 ?x_19)
            )
          )
          (exists (?x_26 )
            (and
              (BelowOf10 ?x_26)
              (AboveOf16 ?x_26)
            )
          )
          (exists (?x_28 )
            (and
              (LeftOf16 ?x_28)
              (RightOf18 ?x_28)
            )
          )
          (exists (?x_24 )
            (and
              (RightOf12 ?x_24)
              (LeftOf3 ?x_24)
            )
          )
          (exists (?x_17 )
            (and
              (BelowOf1 ?x_17)
              (AboveOf18 ?x_17)
            )
          )
          (exists (?x_30 )
            (and
              (RightOf16 ?x_30)
              (LeftOf11 ?x_30)
            )
          )
          (exists (?x_22 )
            (and
              (LeftOf12 ?x_22)
              (RightOf13 ?x_22)
            )
          )
          (exists (?x_0 )
            (and
              (AboveOf11 ?x_0)
              (BelowOf9 ?x_0)
            )
          )
          (exists (?x_20 )
            (and
              (LeftOf1 ?x_20)
              (RightOf9 ?x_20)
            )
          )
          (exists (?x_22 )
            (and
              (RightOf13 ?x_22)
              (LeftOf8 ?x_22)
            )
          )
          (exists (?x_15 )
            (and
              (AboveOf10 ?x_15)
              (BelowOf2 ?x_15)
            )
          )
          (exists (?x_13 )
            (and
              (RightOf11 ?x_13)
              (LeftOf3 ?x_13)
            )
          )
          (exists (?x_0 )
            (and
              (AboveOf14 ?x_0)
              (BelowOf9 ?x_0)
            )
          )
          (exists (?x_9 )
            (and
              (BelowOf1 ?x_9)
              (AboveOf9 ?x_9)
            )
          )
          (exists (?x_31 )
            (and
              (BelowOf9 ?x_31)
              (AboveOf17 ?x_31)
            )
          )
          (exists (?x_1 )
            (and
              (BelowOf12 ?x_1)
              (AboveOf15 ?x_1)
            )
          )
          (exists (?x_22 )
            (and
              (LeftOf1 ?x_22)
              (RightOf17 ?x_22)
            )
          )
          (exists (?x_35 )
            (and
              (BelowOf3 ?x_35)
              (AboveOf15 ?x_35)
            )
          )
          (exists (?x_15 )
            (and
              (BelowOf6 ?x_15)
              (AboveOf13 ?x_15)
            )
          )
          (exists (?x_2 )
            (and
              (BelowOf13 ?x_2)
              (AboveOf18 ?x_2)
            )
          )
          (exists (?x_7 )
            (and
              (RightOf15 ?x_7)
              (LeftOf1 ?x_7)
            )
          )
          (exists (?x_0 )
            (and
              (BelowOf3 ?x_0)
              (AboveOf15 ?x_0)
            )
          )
          (exists (?x_34 )
            (and
              (LeftOf6 ?x_34)
              (RightOf9 ?x_34)
            )
          )
          (exists (?x_10 )
            (and
              (LeftOf5 ?x_10)
              (RightOf14 ?x_10)
            )
          )
          (exists (?x_3 )
            (and
              (RightOf3 ?x_3)
              (LeftOf3 ?x_3)
            )
          )
          (exists (?x_4 )
            (and
              (LeftOf2 ?x_4)
              (RightOf7 ?x_4)
            )
          )
          (exists (?x_19 )
            (and
              (RightOf15 ?x_19)
              (LeftOf1 ?x_19)
            )
          )
          (exists (?x_6 )
            (and
              (BelowOf5 ?x_6)
              (AboveOf12 ?x_6)
            )
          )
          (exists (?x_24 )
            (and
              (LeftOf3 ?x_24)
              (RightOf16 ?x_24)
            )
          )
          (exists (?x_35 )
            (and
              (BelowOf9 ?x_35)
              (AboveOf10 ?x_35)
            )
          )
          (exists (?x_19 )
            (and
              (LeftOf5 ?x_19)
              (RightOf18 ?x_19)
            )
          )
          (exists (?x_25 )
            (and
              (RightOf12 ?x_25)
              (LeftOf1 ?x_25)
            )
          )
          (exists (?x_16 )
            (and
              (AboveOf14 ?x_16)
              (BelowOf4 ?x_16)
            )
          )
          (exists (?x_14 )
            (and
              (BelowOf3 ?x_14)
              (AboveOf18 ?x_14)
            )
          )
          (exists (?x_7 )
            (and
              (RightOf17 ?x_7)
              (LeftOf6 ?x_7)
            )
          )
          (exists (?x_13 )
            (and
              (RightOf4 ?x_13)
              (LeftOf3 ?x_13)
            )
          )
          (exists (?x_6 )
            (and
              (AboveOf9 ?x_6)
              (BelowOf2 ?x_6)
            )
          )
          (exists (?x_34 )
            (and
              (LeftOf4 ?x_34)
              (RightOf18 ?x_34)
            )
          )
          (exists (?x_8 )
            (and
              (LeftOf5 ?x_8)
              (RightOf8 ?x_8)
            )
          )
          (exists (?x_12 )
            (and
              (BelowOf6 ?x_12)
              (AboveOf16 ?x_12)
            )
          )
          (exists (?x_5 )
            (and
              (RightOf16 ?x_5)
              (LeftOf4 ?x_5)
            )
          )
          (exists (?x_34 )
            (and
              (LeftOf2 ?x_34)
              (RightOf15 ?x_34)
            )
          )
          (exists (?x_28 )
            (and
              (LeftOf3 ?x_28)
              (RightOf18 ?x_28)
            )
          )
          (exists (?x_9 )
            (and
              (BelowOf3 ?x_9)
              (AboveOf10 ?x_9)
            )
          )
          (exists (?x_5 )
            (and
              (LeftOf1 ?x_5)
              (RightOf10 ?x_5)
            )
          )
          (exists (?x_33 )
            (and
              (LeftOf15 ?x_33)
              (RightOf16 ?x_33)
            )
          )
          (exists (?x_27 )
            (and
              (LeftOf5 ?x_27)
              (RightOf14 ?x_27)
            )
          )
          (exists (?x_0 )
            (and
              (BelowOf4 ?x_0)
              (AboveOf13 ?x_0)
            )
          )
          (exists (?x_34 )
            (and
              (RightOf18 ?x_34)
              (LeftOf4 ?x_34)
            )
          )
          (exists (?x_19 )
            (and
              (RightOf18 ?x_19)
              (LeftOf8 ?x_19)
            )
          )
          (exists (?x_10 )
            (and
              (RightOf17 ?x_10)
              (LeftOf3 ?x_10)
            )
          )
          (exists (?x_21 )
            (and
              (AboveOf3 ?x_21)
              (BelowOf1 ?x_21)
            )
          )
          (exists (?x_10 )
            (and
              (LeftOf2 ?x_10)
              (RightOf16 ?x_10)
            )
          )
          (exists (?x_2 )
            (and
              (AboveOf14 ?x_2)
              (BelowOf1 ?x_2)
            )
          )
          (exists (?x_19 )
            (and
              (LeftOf8 ?x_19)
              (RightOf18 ?x_19)
            )
          )
          (exists (?x_25 )
            (and
              (LeftOf3 ?x_25)
              (RightOf18 ?x_25)
            )
          )
          (exists (?x_3 )
            (and
              (RightOf15 ?x_3)
              (LeftOf1 ?x_3)
            )
          )
          (exists (?x_31 )
            (and
              (BelowOf5 ?x_31)
              (AboveOf14 ?x_31)
            )
          )
          (exists (?x_27 )
            (and
              (LeftOf5 ?x_27)
              (RightOf9 ?x_27)
            )
          )
          (exists (?x_33 )
            (and
              (LeftOf16 ?x_33)
              (RightOf16 ?x_33)
            )
          )
          (exists (?x_31 )
            (and
              (AboveOf15 ?x_31)
              (BelowOf3 ?x_31)
            )
          )
          (exists (?x_34 )
            (and
              (LeftOf7 ?x_34)
              (RightOf14 ?x_34)
            )
          )
          (exists (?x_4 )
            (and
              (LeftOf2 ?x_4)
              (RightOf13 ?x_4)
            )
          )
          (exists (?x_8 )
            (and
              (LeftOf7 ?x_8)
              (RightOf10 ?x_8)
            )
          )
          (exists (?x_13 )
            (and
              (LeftOf2 ?x_13)
              (RightOf7 ?x_13)
            )
          )
          (exists (?x_17 )
            (and
              (BelowOf2 ?x_17)
              (AboveOf6 ?x_17)
            )
          )
          (exists (?x_27 )
            (and
              (LeftOf5 ?x_27)
              (RightOf5 ?x_27)
            )
          )
          (exists (?x_9 )
            (and
              (BelowOf7 ?x_9)
              (AboveOf9 ?x_9)
            )
          )
          (exists (?x_8 )
            (and
              (RightOf11 ?x_8)
              (LeftOf3 ?x_8)
            )
          )
          (exists (?x_15 )
            (and
              (AboveOf13 ?x_15)
              (BelowOf5 ?x_15)
            )
          )
          (exists (?x_6 )
            (and
              (BelowOf1 ?x_6)
              (AboveOf15 ?x_6)
            )
          )
          (exists (?x_3 )
            (and
              (RightOf12 ?x_3)
              (LeftOf2 ?x_3)
            )
          )
          (exists (?x_5 )
            (and
              (LeftOf7 ?x_5)
              (RightOf18 ?x_5)
            )
          )
          (exists (?x_2 )
            (and
              (BelowOf8 ?x_2)
              (AboveOf16 ?x_2)
            )
          )
          (exists (?x_32 )
            (and
              (LeftOf4 ?x_32)
              (RightOf18 ?x_32)
            )
          )
          (exists (?x_19 )
            (and
              (RightOf12 ?x_19)
              (LeftOf6 ?x_19)
            )
          )
          (exists (?x_24 )
            (and
              (LeftOf6 ?x_24)
              (RightOf17 ?x_24)
            )
          )
          (exists (?x_17 )
            (and
              (AboveOf13 ?x_17)
              (BelowOf1 ?x_17)
            )
          )
          (exists (?x_19 )
            (and
              (LeftOf1 ?x_19)
              (RightOf14 ?x_19)
            )
          )
          (exists (?x_30 )
            (and
              (RightOf17 ?x_30)
              (LeftOf11 ?x_30)
            )
          )
          (exists (?x_1 )
            (and
              (BelowOf13 ?x_1)
              (AboveOf17 ?x_1)
            )
          )
          (exists (?x_19 )
            (and
              (RightOf18 ?x_19)
              (LeftOf6 ?x_19)
            )
          )
          (exists (?x_13 )
            (and
              (LeftOf1 ?x_13)
              (RightOf4 ?x_13)
            )
          )
          (exists (?x_19 )
            (and
              (RightOf12 ?x_19)
              (LeftOf11 ?x_19)
            )
          )
          (exists (?x_31 )
            (and
              (BelowOf7 ?x_31)
              (AboveOf14 ?x_31)
            )
          )
          (exists (?x_3 )
            (and
              (LeftOf3 ?x_3)
              (RightOf10 ?x_3)
            )
          )
          (exists (?x_14 )
            (and
              (AboveOf18 ?x_14)
              (BelowOf9 ?x_14)
            )
          )
          (exists (?x_27 )
            (and
              (RightOf8 ?x_27)
              (LeftOf4 ?x_27)
            )
          )
          (exists (?x_3 )
            (and
              (RightOf6 ?x_3)
              (LeftOf2 ?x_3)
            )
          )
          (exists (?x_23 )
            (and
              (AboveOf9 ?x_23)
              (BelowOf1 ?x_23)
            )
          )
          (exists (?x_25 )
            (and
              (LeftOf3 ?x_25)
              (RightOf6 ?x_25)
            )
          )
          (exists (?x_0 )
            (and
              (BelowOf7 ?x_0)
              (AboveOf18 ?x_0)
            )
          )
          (exists (?x_35 )
            (and
              (AboveOf17 ?x_35)
              (BelowOf2 ?x_35)
            )
          )
          (exists (?x_8 )
            (and
              (LeftOf1 ?x_8)
              (RightOf11 ?x_8)
            )
          )
          (exists (?x_30 )
            (and
              (RightOf13 ?x_30)
              (LeftOf11 ?x_30)
            )
          )
          (exists (?x_5 )
            (and
              (RightOf17 ?x_5)
              (LeftOf10 ?x_5)
            )
          )
          (exists (?x_22 )
            (and
              (RightOf12 ?x_22)
              (LeftOf3 ?x_22)
            )
          )
          (exists (?x_32 )
            (and
              (LeftOf6 ?x_32)
              (RightOf17 ?x_32)
            )
          )
          (exists (?x_30 )
            (and
              (LeftOf4 ?x_30)
              (RightOf15 ?x_30)
            )
          )
          (exists (?x_31 )
            (and
              (BelowOf13 ?x_31)
              (AboveOf18 ?x_31)
            )
          )
          (exists (?x_23 )
            (and
              (AboveOf6 ?x_23)
              (BelowOf4 ?x_23)
            )
          )
          (exists (?x_13 )
            (and
              (LeftOf4 ?x_13)
              (RightOf17 ?x_13)
            )
          )
          (exists (?x_34 )
            (and
              (LeftOf1 ?x_34)
              (RightOf17 ?x_34)
            )
          )
          (exists (?x_18 )
            (and
              (AboveOf11 ?x_18)
              (BelowOf1 ?x_18)
            )
          )
          (exists (?x_35 )
            (and
              (AboveOf17 ?x_35)
              (BelowOf5 ?x_35)
            )
          )
          (exists (?x_25 )
            (and
              (LeftOf3 ?x_25)
              (RightOf13 ?x_25)
            )
          )
          (exists (?x_25 )
            (and
              (LeftOf5 ?x_25)
              (RightOf11 ?x_25)
            )
          )
          (exists (?x_12 )
            (and
              (BelowOf10 ?x_12)
              (AboveOf18 ?x_12)
            )
          )
          (exists (?x_6 )
            (and
              (AboveOf16 ?x_6)
              (BelowOf5 ?x_6)
            )
          )
          (exists (?x_9 )
            (and
              (AboveOf16 ?x_9)
              (BelowOf4 ?x_9)
            )
          )
          (exists (?x_22 )
            (and
              (RightOf15 ?x_22)
              (LeftOf12 ?x_22)
            )
          )
          (exists (?x_5 )
            (and
              (RightOf15 ?x_5)
              (LeftOf9 ?x_5)
            )
          )
          (exists (?x_6 )
            (and
              (AboveOf10 ?x_6)
              (BelowOf5 ?x_6)
            )
          )
          (exists (?x_27 )
            (and
              (RightOf16 ?x_27)
              (LeftOf2 ?x_27)
            )
          )
          (exists (?x_4 )
            (and
              (RightOf2 ?x_4)
              (LeftOf1 ?x_4)
            )
          )
          (exists (?x_2 )
            (and
              (AboveOf14 ?x_2)
              (BelowOf3 ?x_2)
            )
          )
          (exists (?x_30 )
            (and
              (LeftOf5 ?x_30)
              (RightOf13 ?x_30)
            )
          )
          (exists (?x_21 )
            (and
              (BelowOf2 ?x_21)
              (AboveOf7 ?x_21)
            )
          )
          (exists (?x_34 )
            (and
              (RightOf18 ?x_34)
              (LeftOf6 ?x_34)
            )
          )
          (exists (?x_28 )
            (and
              (LeftOf1 ?x_28)
              (RightOf18 ?x_28)
            )
          )
          (exists (?x_4 )
            (and
              (LeftOf1 ?x_4)
              (RightOf15 ?x_4)
            )
          )
          (exists (?x_13 )
            (and
              (RightOf5 ?x_13)
              (LeftOf1 ?x_13)
            )
          )
          (exists (?x_15 )
            (and
              (AboveOf12 ?x_15)
              (BelowOf4 ?x_15)
            )
          )
          (exists (?x_12 )
            (and
              (AboveOf18 ?x_12)
              (BelowOf16 ?x_12)
            )
          )
          (exists (?x_27 )
            (and
              (RightOf17 ?x_27)
              (LeftOf3 ?x_27)
            )
          )
          (exists (?x_30 )
            (and
              (LeftOf8 ?x_30)
              (RightOf13 ?x_30)
            )
          )
          (exists (?x_5 )
            (and
              (LeftOf4 ?x_5)
              (RightOf17 ?x_5)
            )
          )
          (exists (?x_11 )
            (and
              (AboveOf17 ?x_11)
              (BelowOf2 ?x_11)
            )
          )
          (exists (?x_6 )
            (and
              (BelowOf1 ?x_6)
              (AboveOf6 ?x_6)
            )
          )
          (exists (?x_18 )
            (and
              (AboveOf13 ?x_18)
              (BelowOf1 ?x_18)
            )
          )
          (exists (?x_25 )
            (and
              (LeftOf6 ?x_25)
              (RightOf16 ?x_25)
            )
          )
          (exists (?x_3 )
            (and
              (RightOf6 ?x_3)
              (LeftOf3 ?x_3)
            )
          )
          (exists (?x_8 )
            (and
              (LeftOf5 ?x_8)
              (RightOf16 ?x_8)
            )
          )
          (exists (?x_2 )
            (and
              (AboveOf15 ?x_2)
              (BelowOf12 ?x_2)
            )
          )
          (exists (?x_16 )
            (and
              (AboveOf14 ?x_16)
              (BelowOf3 ?x_16)
            )
          )
          (exists (?x_21 )
            (and
              (BelowOf1 ?x_21)
              (AboveOf7 ?x_21)
            )
          )
          (exists (?x_25 )
            (and
              (RightOf6 ?x_25)
              (LeftOf1 ?x_25)
            )
          )
          (exists (?x_7 )
            (and
              (RightOf17 ?x_7)
              (LeftOf3 ?x_7)
            )
          )
          (exists (?x_17 )
            (and
              (AboveOf9 ?x_17)
              (BelowOf3 ?x_17)
            )
          )
          (exists (?x_9 )
            (and
              (BelowOf1 ?x_9)
              (AboveOf13 ?x_9)
            )
          )
          (exists (?x_34 )
            (and
              (RightOf16 ?x_34)
              (LeftOf3 ?x_34)
            )
          )
          (exists (?x_7 )
            (and
              (RightOf18 ?x_7)
              (LeftOf11 ?x_7)
            )
          )
          (exists (?x_18 )
            (and
              (AboveOf16 ?x_18)
              (BelowOf1 ?x_18)
            )
          )
          (exists (?x_2 )
            (and
              (BelowOf13 ?x_2)
              (AboveOf16 ?x_2)
            )
          )
          (exists (?x_11 )
            (and
              (AboveOf17 ?x_11)
              (BelowOf5 ?x_11)
            )
          )
          (exists (?x_24 )
            (and
              (RightOf13 ?x_24)
              (LeftOf4 ?x_24)
            )
          )
          (exists (?x_12 )
            (and
              (BelowOf5 ?x_12)
              (AboveOf16 ?x_12)
            )
          )
          (exists (?x_35 )
            (and
              (BelowOf5 ?x_35)
              (AboveOf10 ?x_35)
            )
          )
          (exists (?x_26 )
            (and
              (AboveOf18 ?x_26)
              (BelowOf10 ?x_26)
            )
          )
          (exists (?x_7 )
            (and
              (RightOf15 ?x_7)
              (LeftOf9 ?x_7)
            )
          )
          (exists (?x_1 )
            (and
              (AboveOf18 ?x_1)
              (BelowOf12 ?x_1)
            )
          )
          (exists (?x_10 )
            (and
              (LeftOf2 ?x_10)
              (RightOf14 ?x_10)
            )
          )
          (exists (?x_10 )
            (and
              (RightOf16 ?x_10)
              (LeftOf1 ?x_10)
            )
          )
          (exists (?x_26 )
            (and
              (BelowOf7 ?x_26)
              (AboveOf12 ?x_26)
            )
          )
          (exists (?x_31 )
            (and
              (AboveOf13 ?x_31)
              (BelowOf9 ?x_31)
            )
          )
          (exists (?x_1 )
            (and
              (AboveOf18 ?x_1)
              (BelowOf8 ?x_1)
            )
          )
          (exists (?x_15 )
            (and
              (BelowOf3 ?x_15)
              (AboveOf9 ?x_15)
            )
          )
          (exists (?x_26 )
            (and
              (AboveOf15 ?x_26)
              (BelowOf3 ?x_26)
            )
          )
          (exists (?x_19 )
            (and
              (RightOf17 ?x_19)
              (LeftOf3 ?x_19)
            )
          )
          (exists (?x_16 )
            (and
              (AboveOf11 ?x_16)
              (BelowOf9 ?x_16)
            )
          )
          (exists (?x_34 )
            (and
              (RightOf18 ?x_34)
              (LeftOf9 ?x_34)
            )
          )
          (exists (?x_9 )
            (and
              (BelowOf7 ?x_9)
              (AboveOf10 ?x_9)
            )
          )
          (exists (?x_9 )
            (and
              (AboveOf9 ?x_9)
              (BelowOf2 ?x_9)
            )
          )
          (exists (?x_34 )
            (and
              (RightOf14 ?x_34)
              (LeftOf5 ?x_34)
            )
          )
          (exists (?x_31 )
            (and
              (AboveOf15 ?x_31)
              (BelowOf5 ?x_31)
            )
          )
          (exists (?x_13 )
            (and
              (LeftOf2 ?x_13)
              (RightOf14 ?x_13)
            )
          )
          (exists (?x_18 )
            (and
              (BelowOf1 ?x_18)
              (AboveOf12 ?x_18)
            )
          )
          (exists (?x_34 )
            (and
              (RightOf9 ?x_34)
              (LeftOf5 ?x_34)
            )
          )
          (exists (?x_23 )
            (and
              (AboveOf11 ?x_23)
              (BelowOf4 ?x_23)
            )
          )
          (exists (?x_10 )
            (and
              (RightOf16 ?x_10)
              (LeftOf13 ?x_10)
            )
          )
          (exists (?x_24 )
            (and
              (RightOf7 ?x_24)
              (LeftOf7 ?x_24)
            )
          )
          (exists (?x_5 )
            (and
              (LeftOf9 ?x_5)
              (RightOf11 ?x_5)
            )
          )
          (exists (?x_9 )
            (and
              (BelowOf6 ?x_9)
              (AboveOf14 ?x_9)
            )
          )
          (exists (?x_23 )
            (and
              (AboveOf17 ?x_23)
              (BelowOf1 ?x_23)
            )
          )
          (exists (?x_33 )
            (and
              (LeftOf13 ?x_33)
              (RightOf18 ?x_33)
            )
          )
          (exists (?x_27 )
            (and
              (RightOf11 ?x_27)
              (LeftOf3 ?x_27)
            )
          )
          (exists (?x_6 )
            (and
              (BelowOf5 ?x_6)
              (AboveOf14 ?x_6)
            )
          )
          (exists (?x_24 )
            (and
              (LeftOf3 ?x_24)
              (RightOf9 ?x_24)
            )
          )
          (exists (?x_16 )
            (and
              (AboveOf15 ?x_16)
              (BelowOf3 ?x_16)
            )
          )
          (exists (?x_13 )
            (and
              (RightOf8 ?x_13)
              (LeftOf2 ?x_13)
            )
          )
          (exists (?x_18 )
            (and
              (AboveOf6 ?x_18)
              (BelowOf1 ?x_18)
            )
          )
          (exists (?x_17 )
            (and
              (BelowOf2 ?x_17)
              (AboveOf7 ?x_17)
            )
          )
          (exists (?x_10 )
            (and
              (RightOf17 ?x_10)
              (LeftOf5 ?x_10)
            )
          )
          (exists (?x_5 )
            (and
              (RightOf15 ?x_5)
              (LeftOf3 ?x_5)
            )
          )
          (exists (?x_11 )
            (and
              (BelowOf15 ?x_11)
              (AboveOf18 ?x_11)
            )
          )
          (exists (?x_0 )
            (and
              (BelowOf4 ?x_0)
              (AboveOf11 ?x_0)
            )
          )
          (exists (?x_13 )
            (and
              (RightOf6 ?x_13)
              (LeftOf3 ?x_13)
            )
          )
          (exists (?x_35 )
            (and
              (AboveOf15 ?x_35)
              (BelowOf7 ?x_35)
            )
          )
          (exists (?x_29 )
            (and
              (AboveOf15 ?x_29)
              (BelowOf1 ?x_29)
            )
          )
          (exists (?x_35 )
            (and
              (AboveOf17 ?x_35)
              (BelowOf4 ?x_35)
            )
          )
          (exists (?x_35 )
            (and
              (BelowOf6 ?x_35)
              (AboveOf18 ?x_35)
            )
          )
          (exists (?x_21 )
            (and
              (BelowOf3 ?x_21)
              (AboveOf10 ?x_21)
            )
          )
          (exists (?x_17 )
            (and
              (AboveOf12 ?x_17)
              (BelowOf1 ?x_17)
            )
          )
          (exists (?x_24 )
            (and
              (RightOf17 ?x_24)
              (LeftOf3 ?x_24)
            )
          )
          (exists (?x_34 )
            (and
              (RightOf15 ?x_34)
              (LeftOf3 ?x_34)
            )
          )
          (exists (?x_27 )
            (and
              (RightOf12 ?x_27)
              (LeftOf2 ?x_27)
            )
          )
          (exists (?x_8 )
            (and
              (RightOf9 ?x_8)
              (LeftOf1 ?x_8)
            )
          )
          (exists (?x_26 )
            (and
              (AboveOf13 ?x_26)
              (BelowOf5 ?x_26)
            )
          )
          (exists (?x_8 )
            (and
              (LeftOf5 ?x_8)
              (RightOf15 ?x_8)
            )
          )
          (exists (?x_1 )
            (and
              (AboveOf16 ?x_1)
              (BelowOf6 ?x_1)
            )
          )
          (exists (?x_24 )
            (and
              (LeftOf2 ?x_24)
              (RightOf15 ?x_24)
            )
          )
          (exists (?x_19 )
            (and
              (LeftOf7 ?x_19)
              (RightOf11 ?x_19)
            )
          )
          (exists (?x_27 )
            (and
              (RightOf11 ?x_27)
              (LeftOf4 ?x_27)
            )
          )
          (exists (?x_22 )
            (and
              (RightOf15 ?x_22)
              (LeftOf1 ?x_22)
            )
          )
          (exists (?x_8 )
            (and
              (RightOf11 ?x_8)
              (LeftOf8 ?x_8)
            )
          )
          (exists (?x_1 )
            (and
              (AboveOf18 ?x_1)
              (BelowOf5 ?x_1)
            )
          )
          (exists (?x_26 )
            (and
              (AboveOf15 ?x_26)
              (BelowOf8 ?x_26)
            )
          )
          (exists (?x_25 )
            (and
              (LeftOf4 ?x_25)
              (RightOf8 ?x_25)
            )
          )
          (exists (?x_8 )
            (and
              (LeftOf2 ?x_8)
              (RightOf17 ?x_8)
            )
          )
          (exists (?x_30 )
            (and
              (LeftOf10 ?x_30)
              (RightOf16 ?x_30)
            )
          )
          (exists (?x_30 )
            (and
              (LeftOf2 ?x_30)
              (RightOf18 ?x_30)
            )
          )
          (exists (?x_3 )
            (and
              (RightOf11 ?x_3)
              (LeftOf2 ?x_3)
            )
          )
          (exists (?x_35 )
            (and
              (AboveOf9 ?x_35)
              (BelowOf9 ?x_35)
            )
          )
          (exists (?x_34 )
            (and
              (LeftOf7 ?x_34)
              (RightOf10 ?x_34)
            )
          )
          (exists (?x_29 )
            (and
              (AboveOf13 ?x_29)
              (BelowOf2 ?x_29)
            )
          )
          (exists (?x_10 )
            (and
              (LeftOf11 ?x_10)
              (RightOf14 ?x_10)
            )
          )
          (exists (?x_26 )
            (and
              (AboveOf18 ?x_26)
              (BelowOf11 ?x_26)
            )
          )
          (exists (?x_13 )
            (and
              (LeftOf2 ?x_13)
              (RightOf11 ?x_13)
            )
          )
          (exists (?x_22 )
            (and
              (LeftOf3 ?x_22)
              (RightOf17 ?x_22)
            )
          )
          (exists (?x_3 )
            (and
              (LeftOf1 ?x_3)
              (RightOf9 ?x_3)
            )
          )
          (exists (?x_33 )
            (and
              (RightOf17 ?x_33)
              (LeftOf6 ?x_33)
            )
          )
          (exists (?x_8 )
            (and
              (LeftOf3 ?x_8)
              (RightOf8 ?x_8)
            )
          )
          (exists (?x_15 )
            (and
              (BelowOf3 ?x_15)
              (AboveOf17 ?x_15)
            )
          )
          (exists (?x_34 )
            (and
              (RightOf15 ?x_34)
              (LeftOf1 ?x_34)
            )
          )
          (exists (?x_10 )
            (and
              (LeftOf12 ?x_10)
              (RightOf14 ?x_10)
            )
          )
          (exists (?x_31 )
            (and
              (BelowOf8 ?x_31)
              (AboveOf17 ?x_31)
            )
          )
          (exists (?x_7 )
            (and
              (RightOf18 ?x_7)
              (LeftOf8 ?x_7)
            )
          )
          (exists (?x_16 )
            (and
              (BelowOf11 ?x_16)
              (AboveOf17 ?x_16)
            )
          )
          (exists (?x_9 )
            (and
              (BelowOf6 ?x_9)
              (AboveOf9 ?x_9)
            )
          )
          (exists (?x_26 )
            (and
              (BelowOf10 ?x_26)
              (AboveOf13 ?x_26)
            )
          )
          (exists (?x_22 )
            (and
              (LeftOf9 ?x_22)
              (RightOf15 ?x_22)
            )
          )
          (exists (?x_15 )
            (and
              (BelowOf2 ?x_15)
              (AboveOf16 ?x_15)
            )
          )
          (exists (?x_15 )
            (and
              (BelowOf2 ?x_15)
              (AboveOf8 ?x_15)
            )
          )
          (exists (?x_15 )
            (and
              (AboveOf9 ?x_15)
              (BelowOf8 ?x_15)
            )
          )
          (exists (?x_15 )
            (and
              (BelowOf1 ?x_15)
              (AboveOf14 ?x_15)
            )
          )
          (exists (?x_3 )
            (and
              (RightOf7 ?x_3)
              (LeftOf1 ?x_3)
            )
          )
          (exists (?x_15 )
            (and
              (AboveOf15 ?x_15)
              (BelowOf2 ?x_15)
            )
          )
          (exists (?x_0 )
            (and
              (AboveOf14 ?x_0)
              (BelowOf7 ?x_0)
            )
          )
          (exists (?x_1 )
            (and
              (AboveOf17 ?x_1)
              (BelowOf4 ?x_1)
            )
          )
          (exists (?x_0 )
            (and
              (AboveOf15 ?x_0)
              (BelowOf4 ?x_0)
            )
          )
          (exists (?x_23 )
            (and
              (AboveOf8 ?x_23)
              (BelowOf2 ?x_23)
            )
          )
          (exists (?x_9 )
            (and
              (AboveOf15 ?x_9)
              (BelowOf2 ?x_9)
            )
          )
          (exists (?x_27 )
            (and
              (LeftOf5 ?x_27)
              (RightOf16 ?x_27)
            )
          )
          (exists (?x_24 )
            (and
              (LeftOf6 ?x_24)
              (RightOf7 ?x_24)
            )
          )
          (exists (?x_24 )
            (and
              (LeftOf3 ?x_24)
              (RightOf8 ?x_24)
            )
          )
          (exists (?x_3 )
            (and
              (LeftOf3 ?x_3)
              (RightOf4 ?x_3)
            )
          )
          (exists (?x_35 )
            (and
              (BelowOf1 ?x_35)
              (AboveOf15 ?x_35)
            )
          )
          (exists (?x_9 )
            (and
              (AboveOf8 ?x_9)
              (BelowOf2 ?x_9)
            )
          )
          (exists (?x_31 )
            (and
              (BelowOf2 ?x_31)
              (AboveOf14 ?x_31)
            )
          )
          (exists (?x_25 )
            (and
              (RightOf8 ?x_25)
              (LeftOf4 ?x_25)
            )
          )
          (exists (?x_1 )
            (and
              (BelowOf11 ?x_1)
              (AboveOf15 ?x_1)
            )
          )
          (exists (?x_21 )
            (and
              (AboveOf13 ?x_21)
              (BelowOf2 ?x_21)
            )
          )
          (exists (?x_30 )
            (and
              (LeftOf6 ?x_30)
              (RightOf18 ?x_30)
            )
          )
          (exists (?x_23 )
            (and
              (AboveOf8 ?x_23)
              (BelowOf4 ?x_23)
            )
          )
          (exists (?x_13 )
            (and
              (RightOf18 ?x_13)
              (LeftOf3 ?x_13)
            )
          )
          (exists (?x_27 )
            (and
              (LeftOf4 ?x_27)
              (RightOf8 ?x_27)
            )
          )
          (exists (?x_8 )
            (and
              (LeftOf6 ?x_8)
              (RightOf12 ?x_8)
            )
          )
          (exists (?x_1 )
            (and
              (BelowOf9 ?x_1)
              (AboveOf16 ?x_1)
            )
          )
          (exists (?x_0 )
            (and
              (BelowOf8 ?x_0)
              (AboveOf13 ?x_0)
            )
          )
          (exists (?x_10 )
            (and
              (LeftOf9 ?x_10)
              (RightOf14 ?x_10)
            )
          )
          (exists (?x_15 )
            (and
              (AboveOf18 ?x_15)
              (BelowOf4 ?x_15)
            )
          )
          (exists (?x_31 )
            (and
              (BelowOf9 ?x_31)
              (AboveOf18 ?x_31)
            )
          )
          (exists (?x_31 )
            (and
              (AboveOf13 ?x_31)
              (BelowOf6 ?x_31)
            )
          )
          (exists (?x_7 )
            (and
              (LeftOf7 ?x_7)
              (RightOf15 ?x_7)
            )
          )
          (exists (?x_23 )
            (and
              (AboveOf18 ?x_23)
              (BelowOf1 ?x_23)
            )
          )
          (exists (?x_4 )
            (and
              (LeftOf2 ?x_4)
              (RightOf12 ?x_4)
            )
          )
          (exists (?x_24 )
            (and
              (RightOf7 ?x_24)
              (LeftOf3 ?x_24)
            )
          )
          (exists (?x_26 )
            (and
              (AboveOf14 ?x_26)
              (BelowOf6 ?x_26)
            )
          )
          (exists (?x_24 )
            (and
              (LeftOf6 ?x_24)
              (RightOf13 ?x_24)
            )
          )
          (exists (?x_12 )
            (and
              (BelowOf2 ?x_12)
              (AboveOf16 ?x_12)
            )
          )
          (exists (?x_0 )
            (and
              (BelowOf4 ?x_0)
              (AboveOf12 ?x_0)
            )
          )
          (exists (?x_9 )
            (and
              (BelowOf5 ?x_9)
              (AboveOf12 ?x_9)
            )
          )
          (exists (?x_32 )
            (and
              (RightOf18 ?x_32)
              (LeftOf12 ?x_32)
            )
          )
          (exists (?x_11 )
            (and
              (BelowOf17 ?x_11)
              (AboveOf18 ?x_11)
            )
          )
          (exists (?x_34 )
            (and
              (RightOf16 ?x_34)
              (LeftOf7 ?x_34)
            )
          )
          (exists (?x_0 )
            (and
              (BelowOf4 ?x_0)
              (AboveOf10 ?x_0)
            )
          )
          (exists (?x_28 )
            (and
              (LeftOf15 ?x_28)
              (RightOf18 ?x_28)
            )
          )
          (exists (?x_12 )
            (and
              (AboveOf17 ?x_12)
              (BelowOf16 ?x_12)
            )
          )
          (exists (?x_9 )
            (and
              (BelowOf7 ?x_9)
              (AboveOf13 ?x_9)
            )
          )
          (exists (?x_23 )
            (and
              (AboveOf12 ?x_23)
              (BelowOf2 ?x_23)
            )
          )
          (exists (?x_6 )
            (and
              (AboveOf13 ?x_6)
              (BelowOf5 ?x_6)
            )
          )
          (exists (?x_12 )
            (and
              (BelowOf14 ?x_12)
              (AboveOf16 ?x_12)
            )
          )
          (exists (?x_24 )
            (and
              (RightOf10 ?x_24)
              (LeftOf4 ?x_24)
            )
          )
          (exists (?x_10 )
            (and
              (RightOf16 ?x_10)
              (LeftOf8 ?x_10)
            )
          )
          (exists (?x_0 )
            (and
              (BelowOf6 ?x_0)
              (AboveOf14 ?x_0)
            )
          )
          (exists (?x_24 )
            (and
              (RightOf7 ?x_24)
              (LeftOf2 ?x_24)
            )
          )
          (exists (?x_10 )
            (and
              (LeftOf3 ?x_10)
              (RightOf17 ?x_10)
            )
          )
          (exists (?x_33 )
            (and
              (RightOf18 ?x_33)
              (LeftOf2 ?x_33)
            )
          )
          (exists (?x_3 )
            (and
              (LeftOf1 ?x_3)
              (RightOf12 ?x_3)
            )
          )
          (exists (?x_1 )
            (and
              (AboveOf18 ?x_1)
              (BelowOf3 ?x_1)
            )
          )
          (exists (?x_10 )
            (and
              (LeftOf11 ?x_10)
              (RightOf17 ?x_10)
            )
          )
          (exists (?x_29 )
            (and
              (BelowOf1 ?x_29)
              (AboveOf8 ?x_29)
            )
          )
          (exists (?x_29 )
            (and
              (BelowOf2 ?x_29)
              (AboveOf18 ?x_29)
            )
          )
          (exists (?x_5 )
            (and
              (RightOf15 ?x_5)
              (LeftOf8 ?x_5)
            )
          )
          (exists (?x_8 )
            (and
              (RightOf15 ?x_8)
              (LeftOf3 ?x_8)
            )
          )
          (exists (?x_8 )
            (and
              (LeftOf5 ?x_8)
              (RightOf17 ?x_8)
            )
          )
          (exists (?x_33 )
            (and
              (LeftOf3 ?x_33)
              (RightOf18 ?x_33)
            )
          )
          (exists (?x_7 )
            (and
              (RightOf18 ?x_7)
              (LeftOf1 ?x_7)
            )
          )
          (exists (?x_1 )
            (and
              (BelowOf1 ?x_1)
              (AboveOf17 ?x_1)
            )
          )
          (exists (?x_30 )
            (and
              (LeftOf11 ?x_30)
              (RightOf15 ?x_30)
            )
          )
          (exists (?x_10 )
            (and
              (RightOf15 ?x_10)
              (LeftOf8 ?x_10)
            )
          )
          (exists (?x_25 )
            (and
              (RightOf7 ?x_25)
              (LeftOf6 ?x_25)
            )
          )
          (exists (?x_27 )
            (and
              (RightOf18 ?x_27)
              (LeftOf4 ?x_27)
            )
          )
          (exists (?x_22 )
            (and
              (RightOf18 ?x_22)
              (LeftOf6 ?x_22)
            )
          )
          (exists (?x_1 )
            (and
              (AboveOf18 ?x_1)
              (BelowOf13 ?x_1)
            )
          )
          (exists (?x_22 )
            (and
              (LeftOf2 ?x_22)
              (RightOf13 ?x_22)
            )
          )
          (exists (?x_34 )
            (and
              (RightOf10 ?x_34)
              (LeftOf9 ?x_34)
            )
          )
          (exists (?x_10 )
            (and
              (LeftOf11 ?x_10)
              (RightOf15 ?x_10)
            )
          )
          (exists (?x_20 )
            (and
              (LeftOf1 ?x_20)
              (RightOf15 ?x_20)
            )
          )
          (exists (?x_12 )
            (and
              (BelowOf7 ?x_12)
              (AboveOf16 ?x_12)
            )
          )
          (exists (?x_35 )
            (and
              (BelowOf4 ?x_35)
              (AboveOf13 ?x_35)
            )
          )
          (exists (?x_27 )
            (and
              (RightOf5 ?x_27)
              (LeftOf4 ?x_27)
            )
          )
          (exists (?x_35 )
            (and
              (AboveOf12 ?x_35)
              (BelowOf1 ?x_35)
            )
          )
          (exists (?x_1 )
            (and
              (AboveOf18 ?x_1)
              (BelowOf9 ?x_1)
            )
          )
          (exists (?x_24 )
            (and
              (LeftOf6 ?x_24)
              (RightOf15 ?x_24)
            )
          )
          (exists (?x_34 )
            (and
              (RightOf16 ?x_34)
              (LeftOf8 ?x_34)
            )
          )
          (exists (?x_30 )
            (and
              (RightOf16 ?x_30)
              (LeftOf12 ?x_30)
            )
          )
          (exists (?x_3 )
            (and
              (LeftOf3 ?x_3)
              (RightOf17 ?x_3)
            )
          )
          (exists (?x_19 )
            (and
              (LeftOf4 ?x_19)
              (RightOf13 ?x_19)
            )
          )
          (exists (?x_26 )
            (and
              (BelowOf2 ?x_26)
              (AboveOf12 ?x_26)
            )
          )
          (exists (?x_26 )
            (and
              (AboveOf15 ?x_26)
              (BelowOf9 ?x_26)
            )
          )
          (exists (?x_16 )
            (and
              (BelowOf2 ?x_16)
              (AboveOf18 ?x_16)
            )
          )
          (exists (?x_5 )
            (and
              (LeftOf8 ?x_5)
              (RightOf18 ?x_5)
            )
          )
          (exists (?x_19 )
            (and
              (LeftOf7 ?x_19)
              (RightOf17 ?x_19)
            )
          )
          (exists (?x_30 )
            (and
              (LeftOf7 ?x_30)
              (RightOf18 ?x_30)
            )
          )
          (exists (?x_2 )
            (and
              (BelowOf12 ?x_2)
              (AboveOf16 ?x_2)
            )
          )
          (exists (?x_24 )
            (and
              (RightOf16 ?x_24)
              (LeftOf7 ?x_24)
            )
          )
          (exists (?x_26 )
            (and
              (BelowOf6 ?x_26)
              (AboveOf17 ?x_26)
            )
          )
          (exists (?x_33 )
            (and
              (LeftOf14 ?x_33)
              (RightOf18 ?x_33)
            )
          )
          (exists (?x_31 )
            (and
              (AboveOf18 ?x_31)
              (BelowOf7 ?x_31)
            )
          )
          (exists (?x_6 )
            (and
              (AboveOf13 ?x_6)
              (BelowOf4 ?x_6)
            )
          )
          (exists (?x_25 )
            (and
              (LeftOf3 ?x_25)
              (RightOf15 ?x_25)
            )
          )
          (exists (?x_17 )
            (and
              (BelowOf1 ?x_17)
              (AboveOf15 ?x_17)
            )
          )
          (exists (?x_16 )
            (and
              (AboveOf15 ?x_16)
              (BelowOf6 ?x_16)
            )
          )
          (exists (?x_0 )
            (and
              (BelowOf6 ?x_0)
              (AboveOf11 ?x_0)
            )
          )
          (exists (?x_30 )
            (and
              (LeftOf13 ?x_30)
              (RightOf17 ?x_30)
            )
          )
          (exists (?x_26 )
            (and
              (AboveOf13 ?x_26)
              (BelowOf11 ?x_26)
            )
          )
          (exists (?x_15 )
            (and
              (AboveOf17 ?x_15)
              (BelowOf8 ?x_15)
            )
          )
          (exists (?x_34 )
            (and
              (RightOf16 ?x_34)
              (LeftOf5 ?x_34)
            )
          )
          (exists (?x_24 )
            (and
              (LeftOf3 ?x_24)
              (RightOf14 ?x_24)
            )
          )
          (exists (?x_33 )
            (and
              (LeftOf11 ?x_33)
              (RightOf16 ?x_33)
            )
          )
          (exists (?x_27 )
            (and
              (RightOf7 ?x_27)
              (LeftOf4 ?x_27)
            )
          )
          (exists (?x_23 )
            (and
              (BelowOf5 ?x_23)
              (AboveOf16 ?x_23)
            )
          )
          (exists (?x_35 )
            (and
              (BelowOf5 ?x_35)
              (AboveOf18 ?x_35)
            )
          )
          (exists (?x_8 )
            (and
              (LeftOf2 ?x_8)
              (RightOf12 ?x_8)
            )
          )
          (exists (?x_30 )
            (and
              (RightOf16 ?x_30)
              (LeftOf4 ?x_30)
            )
          )
          (exists (?x_24 )
            (and
              (LeftOf2 ?x_24)
              (RightOf8 ?x_24)
            )
          )
          (exists (?x_2 )
            (and
              (BelowOf14 ?x_2)
              (AboveOf16 ?x_2)
            )
          )
          (exists (?x_26 )
            (and
              (AboveOf14 ?x_26)
              (BelowOf5 ?x_26)
            )
          )
          (exists (?x_1 )
            (and
              (BelowOf10 ?x_1)
              (AboveOf16 ?x_1)
            )
          )
          (exists (?x_34 )
            (and
              (LeftOf7 ?x_34)
              (RightOf13 ?x_34)
            )
          )
          (exists (?x_26 )
            (and
              (AboveOf15 ?x_26)
              (BelowOf7 ?x_26)
            )
          )
          (exists (?x_15 )
            (and
              (BelowOf6 ?x_15)
              (AboveOf8 ?x_15)
            )
          )
          (exists (?x_35 )
            (and
              (BelowOf8 ?x_35)
              (AboveOf11 ?x_35)
            )
          )
          (exists (?x_34 )
            (and
              (RightOf12 ?x_34)
              (LeftOf8 ?x_34)
            )
          )
          (exists (?x_29 )
            (and
              (AboveOf14 ?x_29)
              (BelowOf1 ?x_29)
            )
          )
          (exists (?x_3 )
            (and
              (RightOf13 ?x_3)
              (LeftOf1 ?x_3)
            )
          )
          (exists (?x_31 )
            (and
              (AboveOf18 ?x_31)
              (BelowOf6 ?x_31)
            )
          )
          (exists (?x_13 )
            (and
              (LeftOf2 ?x_13)
              (RightOf17 ?x_13)
            )
          )
          (exists (?x_7 )
            (and
              (LeftOf2 ?x_7)
              (RightOf15 ?x_7)
            )
          )
          (exists (?x_10 )
            (and
              (RightOf16 ?x_10)
              (LeftOf11 ?x_10)
            )
          )
          (exists (?x_23 )
            (and
              (AboveOf15 ?x_23)
              (BelowOf4 ?x_23)
            )
          )
          (exists (?x_31 )
            (and
              (AboveOf16 ?x_31)
              (BelowOf1 ?x_31)
            )
          )
          (exists (?x_31 )
            (and
              (BelowOf2 ?x_31)
              (AboveOf17 ?x_31)
            )
          )
          (exists (?x_30 )
            (and
              (RightOf17 ?x_30)
              (LeftOf12 ?x_30)
            )
          )
          (exists (?x_5 )
            (and
              (RightOf14 ?x_5)
              (LeftOf2 ?x_5)
            )
          )
          (exists (?x_11 )
            (and
              (AboveOf17 ?x_11)
              (BelowOf14 ?x_11)
            )
          )
          (exists (?x_30 )
            (and
              (LeftOf2 ?x_30)
              (RightOf15 ?x_30)
            )
          )
          (exists (?x_29 )
            (and
              (BelowOf2 ?x_29)
              (AboveOf10 ?x_29)
            )
          )
          (exists (?x_22 )
            (and
              (LeftOf8 ?x_22)
              (RightOf17 ?x_22)
            )
          )
          (exists (?x_23 )
            (and
              (BelowOf3 ?x_23)
              (AboveOf15 ?x_23)
            )
          )
          (exists (?x_5 )
            (and
              (RightOf13 ?x_5)
              (LeftOf8 ?x_5)
            )
          )
          (exists (?x_0 )
            (and
              (AboveOf16 ?x_0)
              (BelowOf1 ?x_0)
            )
          )
          (exists (?x_27 )
            (and
              (LeftOf5 ?x_27)
              (RightOf10 ?x_27)
            )
          )
          (exists (?x_33 )
            (and
              (RightOf17 ?x_33)
              (LeftOf1 ?x_33)
            )
          )
          (exists (?x_5 )
            (and
              (LeftOf9 ?x_5)
              (RightOf17 ?x_5)
            )
          )
          (exists (?x_19 )
            (and
              (LeftOf10 ?x_19)
              (RightOf18 ?x_19)
            )
          )
          (exists (?x_23 )
            (and
              (AboveOf17 ?x_23)
              (BelowOf2 ?x_23)
            )
          )
          (exists (?x_5 )
            (and
              (RightOf16 ?x_5)
              (LeftOf6 ?x_5)
            )
          )
          (exists (?x_31 )
            (and
              (AboveOf13 ?x_31)
              (BelowOf8 ?x_31)
            )
          )
          (exists (?x_27 )
            (and
              (LeftOf3 ?x_27)
              (RightOf7 ?x_27)
            )
          )
          (exists (?x_15 )
            (and
              (BelowOf1 ?x_15)
              (AboveOf9 ?x_15)
            )
          )
          (exists (?x_13 )
            (and
              (LeftOf4 ?x_13)
              (RightOf8 ?x_13)
            )
          )
          (exists (?x_4 )
            (and
              (RightOf15 ?x_4)
              (LeftOf1 ?x_4)
            )
          )
          (exists (?x_4 )
            (and
              (RightOf10 ?x_4)
              (LeftOf1 ?x_4)
            )
          )
          (exists (?x_27 )
            (and
              (LeftOf5 ?x_27)
              (RightOf11 ?x_27)
            )
          )
          (exists (?x_22 )
            (and
              (RightOf18 ?x_22)
              (LeftOf9 ?x_22)
            )
          )
          (exists (?x_9 )
            (and
              (BelowOf5 ?x_9)
              (AboveOf9 ?x_9)
            )
          )
          (exists (?x_33 )
            (and
              (RightOf17 ?x_33)
              (LeftOf3 ?x_33)
            )
          )
          (exists (?x_12 )
            (and
              (AboveOf18 ?x_12)
              (BelowOf9 ?x_12)
            )
          )
          (exists (?x_24 )
            (and
              (RightOf10 ?x_24)
              (LeftOf1 ?x_24)
            )
          )
          (exists (?x_17 )
            (and
              (AboveOf7 ?x_17)
              (BelowOf3 ?x_17)
            )
          )
          (exists (?x_23 )
            (and
              (AboveOf18 ?x_23)
              (BelowOf2 ?x_23)
            )
          )
          (exists (?x_30 )
            (and
              (RightOf16 ?x_30)
              (LeftOf2 ?x_30)
            )
          )
          (exists (?x_30 )
            (and
              (RightOf17 ?x_30)
              (LeftOf2 ?x_30)
            )
          )
          (exists (?x_6 )
            (and
              (BelowOf1 ?x_6)
              (AboveOf8 ?x_6)
            )
          )
          (exists (?x_8 )
            (and
              (RightOf15 ?x_8)
              (LeftOf6 ?x_8)
            )
          )
          (exists (?x_0 )
            (and
              (BelowOf3 ?x_0)
              (AboveOf18 ?x_0)
            )
          )
          (exists (?x_29 )
            (and
              (AboveOf5 ?x_29)
              (BelowOf2 ?x_29)
            )
          )
          (exists (?x_25 )
            (and
              (RightOf15 ?x_25)
              (LeftOf4 ?x_25)
            )
          )
          (exists (?x_7 )
            (and
              (RightOf18 ?x_7)
              (LeftOf6 ?x_7)
            )
          )
          (exists (?x_25 )
            (and
              (LeftOf3 ?x_25)
              (RightOf11 ?x_25)
            )
          )
          (exists (?x_35 )
            (and
              (AboveOf17 ?x_35)
              (BelowOf6 ?x_35)
            )
          )
          (exists (?x_1 )
            (and
              (AboveOf16 ?x_1)
              (BelowOf5 ?x_1)
            )
          )
          (exists (?x_22 )
            (and
              (RightOf14 ?x_22)
              (LeftOf3 ?x_22)
            )
          )
          (exists (?x_4 )
            (and
              (RightOf13 ?x_4)
              (LeftOf1 ?x_4)
            )
          )
          (exists (?x_4 )
            (and
              (RightOf18 ?x_4)
              (LeftOf2 ?x_4)
            )
          )
          (exists (?x_0 )
            (and
              (BelowOf7 ?x_0)
              (AboveOf16 ?x_0)
            )
          )
          (exists (?x_33 )
            (and
              (RightOf16 ?x_33)
              (LeftOf12 ?x_33)
            )
          )
          (exists (?x_14 )
            (and
              (AboveOf18 ?x_14)
              (BelowOf11 ?x_14)
            )
          )
          (exists (?x_24 )
            (and
              (RightOf13 ?x_24)
              (LeftOf7 ?x_24)
            )
          )
          (exists (?x_24 )
            (and
              (RightOf12 ?x_24)
              (LeftOf1 ?x_24)
            )
          )
          (exists (?x_7 )
            (and
              (LeftOf3 ?x_7)
              (RightOf16 ?x_7)
            )
          )
          (exists (?x_30 )
            (and
              (LeftOf5 ?x_30)
              (RightOf16 ?x_30)
            )
          )
          (exists (?x_2 )
            (and
              (BelowOf10 ?x_2)
              (AboveOf16 ?x_2)
            )
          )
          (exists (?x_35 )
            (and
              (BelowOf4 ?x_35)
              (AboveOf15 ?x_35)
            )
          )
          (exists (?x_20 )
            (and
              (RightOf18 ?x_20)
              (LeftOf1 ?x_20)
            )
          )
          (exists (?x_18 )
            (and
              (BelowOf1 ?x_18)
              (AboveOf17 ?x_18)
            )
          )
          (exists (?x_3 )
            (and
              (RightOf14 ?x_3)
              (LeftOf3 ?x_3)
            )
          )
          (exists (?x_32 )
            (and
              (RightOf18 ?x_32)
              (LeftOf4 ?x_32)
            )
          )
          (exists (?x_21 )
            (and
              (AboveOf10 ?x_21)
              (BelowOf2 ?x_21)
            )
          )
          (exists (?x_11 )
            (and
              (BelowOf13 ?x_11)
              (AboveOf18 ?x_11)
            )
          )
          (exists (?x_13 )
            (and
              (RightOf5 ?x_13)
              (LeftOf2 ?x_13)
            )
          )
          (exists (?x_2 )
            (and
              (AboveOf15 ?x_2)
              (BelowOf13 ?x_2)
            )
          )
          (exists (?x_26 )
            (and
              (AboveOf18 ?x_26)
              (BelowOf8 ?x_26)
            )
          )
          (exists (?x_1 )
            (and
              (BelowOf13 ?x_1)
              (AboveOf15 ?x_1)
            )
          )
          (exists (?x_0 )
            (and
              (BelowOf4 ?x_0)
              (AboveOf14 ?x_0)
            )
          )
          (exists (?x_23 )
            (and
              (AboveOf8 ?x_23)
              (BelowOf5 ?x_23)
            )
          )
          (exists (?x_9 )
            (and
              (BelowOf3 ?x_9)
              (AboveOf18 ?x_9)
            )
          )
          (exists (?x_10 )
            (and
              (RightOf14 ?x_10)
              (LeftOf8 ?x_10)
            )
          )
          (exists (?x_12 )
            (and
              (BelowOf11 ?x_12)
              (AboveOf18 ?x_12)
            )
          )
          (exists (?x_8 )
            (and
              (LeftOf4 ?x_8)
              (RightOf12 ?x_8)
            )
          )
          (exists (?x_11 )
            (and
              (BelowOf2 ?x_11)
              (AboveOf18 ?x_11)
            )
          )
          (exists (?x_15 )
            (and
              (AboveOf18 ?x_15)
              (BelowOf8 ?x_15)
            )
          )
          (exists (?x_19 )
            (and
              (LeftOf1 ?x_19)
              (RightOf16 ?x_19)
            )
          )
          (exists (?x_34 )
            (and
              (LeftOf9 ?x_34)
              (RightOf17 ?x_34)
            )
          )
          (exists (?x_31 )
            (and
              (AboveOf15 ?x_31)
              (BelowOf12 ?x_31)
            )
          )
          (exists (?x_31 )
            (and
              (BelowOf9 ?x_31)
              (AboveOf14 ?x_31)
            )
          )
          (exists (?x_6 )
            (and
              (AboveOf16 ?x_6)
              (BelowOf4 ?x_6)
            )
          )
          (exists (?x_15 )
            (and
              (AboveOf18 ?x_15)
              (BelowOf1 ?x_15)
            )
          )
          (exists (?x_14 )
            (and
              (AboveOf18 ?x_14)
              (BelowOf7 ?x_14)
            )
          )
          (exists (?x_32 )
            (and
              (LeftOf8 ?x_32)
              (RightOf18 ?x_32)
            )
          )
          (exists (?x_3 )
            (and
              (RightOf5 ?x_3)
              (LeftOf2 ?x_3)
            )
          )
          (exists (?x_6 )
            (and
              (AboveOf7 ?x_6)
              (BelowOf3 ?x_6)
            )
          )
          (exists (?x_24 )
            (and
              (LeftOf5 ?x_24)
              (RightOf11 ?x_24)
            )
          )
          (exists (?x_21 )
            (and
              (AboveOf6 ?x_21)
              (BelowOf2 ?x_21)
            )
          )
          (exists (?x_5 )
            (and
              (LeftOf2 ?x_5)
              (RightOf10 ?x_5)
            )
          )
          (exists (?x_9 )
            (and
              (AboveOf16 ?x_9)
              (BelowOf5 ?x_9)
            )
          )
          (exists (?x_15 )
            (and
              (BelowOf2 ?x_15)
              (AboveOf11 ?x_15)
            )
          )
          (exists (?x_2 )
            (and
              (AboveOf14 ?x_2)
              (BelowOf2 ?x_2)
            )
          )
          (exists (?x_13 )
            (and
              (LeftOf2 ?x_13)
              (RightOf12 ?x_13)
            )
          )
          (exists (?x_16 )
            (and
              (BelowOf1 ?x_16)
              (AboveOf12 ?x_16)
            )
          )
          (exists (?x_24 )
            (and
              (RightOf14 ?x_24)
              (LeftOf2 ?x_24)
            )
          )
          (exists (?x_25 )
            (and
              (LeftOf6 ?x_25)
              (RightOf18 ?x_25)
            )
          )
          (exists (?x_10 )
            (and
              (RightOf16 ?x_10)
              (LeftOf3 ?x_10)
            )
          )
          (exists (?x_31 )
            (and
              (BelowOf9 ?x_31)
              (AboveOf15 ?x_31)
            )
          )
          (exists (?x_23 )
            (and
              (BelowOf2 ?x_23)
              (AboveOf7 ?x_23)
            )
          )
          (exists (?x_30 )
            (and
              (RightOf14 ?x_30)
              (LeftOf5 ?x_30)
            )
          )
          (exists (?x_33 )
            (and
              (RightOf17 ?x_33)
              (LeftOf14 ?x_33)
            )
          )
          (exists (?x_26 )
            (and
              (AboveOf17 ?x_26)
              (BelowOf8 ?x_26)
            )
          )
          (exists (?x_13 )
            (and
              (LeftOf4 ?x_13)
              (RightOf16 ?x_13)
            )
          )
          (exists (?x_9 )
            (and
              (AboveOf8 ?x_9)
              (BelowOf5 ?x_9)
            )
          )
          (exists (?x_5 )
            (and
              (LeftOf10 ?x_5)
              (RightOf18 ?x_5)
            )
          )
          (exists (?x_34 )
            (and
              (RightOf10 ?x_34)
              (LeftOf4 ?x_34)
            )
          )
          (exists (?x_8 )
            (and
              (RightOf16 ?x_8)
              (LeftOf8 ?x_8)
            )
          )
          (exists (?x_28 )
            (and
              (LeftOf18 ?x_28)
              (RightOf18 ?x_28)
            )
          )
          (exists (?x_17 )
            (and
              (BelowOf2 ?x_17)
              (AboveOf4 ?x_17)
            )
          )
          (exists (?x_8 )
            (and
              (LeftOf6 ?x_8)
              (RightOf13 ?x_8)
            )
          )
          (exists (?x_10 )
            (and
              (LeftOf12 ?x_10)
              (RightOf16 ?x_10)
            )
          )
          (exists (?x_0 )
            (and
              (AboveOf11 ?x_0)
              (BelowOf1 ?x_0)
            )
          )
          (exists (?x_0 )
            (and
              (BelowOf9 ?x_0)
              (AboveOf13 ?x_0)
            )
          )
          (exists (?x_31 )
            (and
              (BelowOf1 ?x_31)
              (AboveOf14 ?x_31)
            )
          )
          (exists (?x_31 )
            (and
              (AboveOf18 ?x_31)
              (BelowOf3 ?x_31)
            )
          )
          (exists (?x_16 )
            (and
              (AboveOf11 ?x_16)
              (BelowOf3 ?x_16)
            )
          )
          (exists (?x_29 )
            (and
              (BelowOf1 ?x_29)
              (AboveOf13 ?x_29)
            )
          )
          (exists (?x_33 )
            (and
              (LeftOf4 ?x_33)
              (RightOf16 ?x_33)
            )
          )
          (exists (?x_25 )
            (and
              (LeftOf1 ?x_25)
              (RightOf18 ?x_25)
            )
          )
          (exists (?x_22 )
            (and
              (LeftOf2 ?x_22)
              (RightOf12 ?x_22)
            )
          )
          (exists (?x_2 )
            (and
              (BelowOf1 ?x_2)
              (AboveOf18 ?x_2)
            )
          )
          (exists (?x_23 )
            (and
              (BelowOf5 ?x_23)
              (AboveOf5 ?x_23)
            )
          )
          (exists (?x_31 )
            (and
              (AboveOf16 ?x_31)
              (BelowOf3 ?x_31)
            )
          )
          (exists (?x_15 )
            (and
              (BelowOf5 ?x_15)
              (AboveOf13 ?x_15)
            )
          )
          (exists (?x_27 )
            (and
              (LeftOf4 ?x_27)
              (RightOf13 ?x_27)
            )
          )
          (exists (?x_13 )
            (and
              (LeftOf4 ?x_13)
              (RightOf9 ?x_13)
            )
          )
          (exists (?x_17 )
            (and
              (AboveOf6 ?x_17)
              (BelowOf3 ?x_17)
            )
          )
          (exists (?x_34 )
            (and
              (LeftOf6 ?x_34)
              (RightOf17 ?x_34)
            )
          )
          (exists (?x_3 )
            (and
              (LeftOf3 ?x_3)
              (RightOf12 ?x_3)
            )
          )
          (exists (?x_12 )
            (and
              (AboveOf17 ?x_12)
              (BelowOf10 ?x_12)
            )
          )
          (exists (?x_1 )
            (and
              (AboveOf16 ?x_1)
              (BelowOf11 ?x_1)
            )
          )
          (exists (?x_0 )
            (and
              (BelowOf1 ?x_0)
              (AboveOf18 ?x_0)
            )
          )
          (exists (?x_12 )
            (and
              (BelowOf8 ?x_12)
              (AboveOf17 ?x_12)
            )
          )
          (exists (?x_25 )
            (and
              (RightOf7 ?x_25)
              (LeftOf4 ?x_25)
            )
          )
          (exists (?x_4 )
            (and
              (RightOf10 ?x_4)
              (LeftOf2 ?x_4)
            )
          )
          (exists (?x_5 )
            (and
              (RightOf11 ?x_5)
              (LeftOf5 ?x_5)
            )
          )
          (exists (?x_34 )
            (and
              (RightOf16 ?x_34)
              (LeftOf2 ?x_34)
            )
          )
          (exists (?x_34 )
            (and
              (RightOf18 ?x_34)
              (LeftOf1 ?x_34)
            )
          )
          (exists (?x_19 )
            (and
              (LeftOf7 ?x_19)
              (RightOf12 ?x_19)
            )
          )
          (exists (?x_15 )
            (and
              (AboveOf15 ?x_15)
              (BelowOf3 ?x_15)
            )
          )
          (exists (?x_15 )
            (and
              (AboveOf12 ?x_15)
              (BelowOf8 ?x_15)
            )
          )
          (exists (?x_32 )
            (and
              (RightOf18 ?x_32)
              (LeftOf14 ?x_32)
            )
          )
          (exists (?x_19 )
            (and
              (RightOf15 ?x_19)
              (LeftOf7 ?x_19)
            )
          )
          (exists (?x_24 )
            (and
              (LeftOf1 ?x_24)
              (RightOf15 ?x_24)
            )
          )
          (exists (?x_7 )
            (and
              (RightOf17 ?x_7)
              (LeftOf1 ?x_7)
            )
          )
          (exists (?x_1 )
            (and
              (AboveOf16 ?x_1)
              (BelowOf4 ?x_1)
            )
          )
          (exists (?x_15 )
            (and
              (AboveOf8 ?x_15)
              (BelowOf5 ?x_15)
            )
          )
          (exists (?x_15 )
            (and
              (AboveOf15 ?x_15)
              (BelowOf7 ?x_15)
            )
          )
          (exists (?x_34 )
            (and
              (RightOf10 ?x_34)
              (LeftOf3 ?x_34)
            )
          )
          (exists (?x_8 )
            (and
              (LeftOf7 ?x_8)
              (RightOf8 ?x_8)
            )
          )
          (exists (?x_8 )
            (and
              (RightOf12 ?x_8)
              (LeftOf8 ?x_8)
            )
          )
          (exists (?x_24 )
            (and
              (RightOf16 ?x_24)
              (LeftOf1 ?x_24)
            )
          )
          (exists (?x_5 )
            (and
              (LeftOf7 ?x_5)
              (RightOf17 ?x_5)
            )
          )
          (exists (?x_1 )
            (and
              (AboveOf16 ?x_1)
              (BelowOf7 ?x_1)
            )
          )
          (exists (?x_27 )
            (and
              (LeftOf4 ?x_27)
              (RightOf16 ?x_27)
            )
          )
          (exists (?x_1 )
            (and
              (AboveOf17 ?x_1)
              (BelowOf3 ?x_1)
            )
          )
          (exists (?x_34 )
            (and
              (RightOf10 ?x_34)
              (LeftOf5 ?x_34)
            )
          )
          (exists (?x_16 )
            (and
              (BelowOf8 ?x_16)
              (AboveOf11 ?x_16)
            )
          )
          (exists (?x_26 )
            (and
              (AboveOf15 ?x_26)
              (BelowOf11 ?x_26)
            )
          )
          (exists (?x_3 )
            (and
              (LeftOf3 ?x_3)
              (RightOf18 ?x_3)
            )
          )
          (exists (?x_5 )
            (and
              (LeftOf7 ?x_5)
              (RightOf11 ?x_5)
            )
          )
          (exists (?x_12 )
            (and
              (BelowOf11 ?x_12)
              (AboveOf16 ?x_12)
            )
          )
          (exists (?x_5 )
            (and
              (LeftOf6 ?x_5)
              (RightOf18 ?x_5)
            )
          )
          (exists (?x_10 )
            (and
              (RightOf18 ?x_10)
              (LeftOf5 ?x_10)
            )
          )
          (exists (?x_5 )
            (and
              (RightOf12 ?x_5)
              (LeftOf3 ?x_5)
            )
          )
          (exists (?x_0 )
            (and
              (AboveOf12 ?x_0)
              (BelowOf2 ?x_0)
            )
          )
          (exists (?x_10 )
            (and
              (LeftOf7 ?x_10)
              (RightOf18 ?x_10)
            )
          )
          (exists (?x_23 )
            (and
              (AboveOf10 ?x_23)
              (BelowOf2 ?x_23)
            )
          )
          (exists (?x_19 )
            (and
              (RightOf11 ?x_19)
              (LeftOf9 ?x_19)
            )
          )
          (exists (?x_10 )
            (and
              (RightOf15 ?x_10)
              (LeftOf3 ?x_10)
            )
          )
          (exists (?x_3 )
            (and
              (RightOf7 ?x_3)
              (LeftOf3 ?x_3)
            )
          )
          (exists (?x_31 )
            (and
              (AboveOf13 ?x_31)
              (BelowOf3 ?x_31)
            )
          )
          (exists (?x_2 )
            (and
              (AboveOf15 ?x_2)
              (BelowOf2 ?x_2)
            )
          )
          (exists (?x_16 )
            (and
              (BelowOf9 ?x_16)
              (AboveOf12 ?x_16)
            )
          )
          (exists (?x_31 )
            (and
              (AboveOf16 ?x_31)
              (BelowOf12 ?x_31)
            )
          )
          (exists (?x_26 )
            (and
              (AboveOf14 ?x_26)
              (BelowOf8 ?x_26)
            )
          )
          (exists (?x_31 )
            (and
              (BelowOf1 ?x_31)
              (AboveOf18 ?x_31)
            )
          )
          (exists (?x_19 )
            (and
              (LeftOf10 ?x_19)
              (RightOf13 ?x_19)
            )
          )
          (exists (?x_23 )
            (and
              (AboveOf16 ?x_23)
              (BelowOf1 ?x_23)
            )
          )
          (exists (?x_6 )
            (and
              (AboveOf18 ?x_6)
              (BelowOf5 ?x_6)
            )
          )
          (exists (?x_8 )
            (and
              (RightOf13 ?x_8)
              (LeftOf3 ?x_8)
            )
          )
          (exists (?x_26 )
            (and
              (AboveOf18 ?x_26)
              (BelowOf1 ?x_26)
            )
          )
          (exists (?x_21 )
            (and
              (AboveOf4 ?x_21)
              (BelowOf1 ?x_21)
            )
          )
          (exists (?x_5 )
            (and
              (RightOf15 ?x_5)
              (LeftOf2 ?x_5)
            )
          )
          (exists (?x_13 )
            (and
              (LeftOf4 ?x_13)
              (RightOf5 ?x_13)
            )
          )
          (exists (?x_5 )
            (and
              (LeftOf1 ?x_5)
              (RightOf18 ?x_5)
            )
          )
          (exists (?x_1 )
            (and
              (AboveOf15 ?x_1)
              (BelowOf3 ?x_1)
            )
          )
          (exists (?x_8 )
            (and
              (LeftOf4 ?x_8)
              (RightOf9 ?x_8)
            )
          )
          (exists (?x_26 )
            (and
              (AboveOf15 ?x_26)
              (BelowOf5 ?x_26)
            )
          )
          (exists (?x_35 )
            (and
              (AboveOf9 ?x_35)
              (BelowOf2 ?x_35)
            )
          )
          (exists (?x_0 )
            (and
              (AboveOf15 ?x_0)
              (BelowOf3 ?x_0)
            )
          )
          (exists (?x_26 )
            (and
              (BelowOf11 ?x_26)
              (AboveOf12 ?x_26)
            )
          )
          (exists (?x_2 )
            (and
              (AboveOf17 ?x_2)
              (BelowOf11 ?x_2)
            )
          )
          (exists (?x_18 )
            (and
              (AboveOf7 ?x_18)
              (BelowOf1 ?x_18)
            )
          )
          (exists (?x_31 )
            (and
              (BelowOf13 ?x_31)
              (AboveOf17 ?x_31)
            )
          )
          (exists (?x_34 )
            (and
              (RightOf10 ?x_34)
              (LeftOf6 ?x_34)
            )
          )
          (exists (?x_35 )
            (and
              (AboveOf14 ?x_35)
              (BelowOf6 ?x_35)
            )
          )
          (exists (?x_2 )
            (and
              (AboveOf17 ?x_2)
              (BelowOf14 ?x_2)
            )
          )
          (exists (?x_30 )
            (and
              (RightOf17 ?x_30)
              (LeftOf4 ?x_30)
            )
          )
          (exists (?x_24 )
            (and
              (LeftOf3 ?x_24)
              (RightOf15 ?x_24)
            )
          )
          (exists (?x_1 )
            (and
              (BelowOf5 ?x_1)
              (AboveOf15 ?x_1)
            )
          )
          (exists (?x_16 )
            (and
              (BelowOf8 ?x_16)
              (AboveOf15 ?x_16)
            )
          )
          (exists (?x_3 )
            (and
              (RightOf13 ?x_3)
              (LeftOf3 ?x_3)
            )
          )
          (exists (?x_21 )
            (and
              (AboveOf17 ?x_21)
              (BelowOf2 ?x_21)
            )
          )
          (exists (?x_12 )
            (and
              (AboveOf18 ?x_12)
              (BelowOf13 ?x_12)
            )
          )
          (exists (?x_28 )
            (and
              (LeftOf4 ?x_28)
              (RightOf18 ?x_28)
            )
          )
          (exists (?x_35 )
            (and
              (BelowOf4 ?x_35)
              (AboveOf9 ?x_35)
            )
          )
          (exists (?x_35 )
            (and
              (AboveOf12 ?x_35)
              (BelowOf5 ?x_35)
            )
          )
          (exists (?x_28 )
            (and
              (LeftOf9 ?x_28)
              (RightOf18 ?x_28)
            )
          )
          (exists (?x_34 )
            (and
              (LeftOf9 ?x_34)
              (RightOf11 ?x_34)
            )
          )
          (exists (?x_2 )
            (and
              (AboveOf14 ?x_2)
              (BelowOf10 ?x_2)
            )
          )
          (exists (?x_35 )
            (and
              (BelowOf8 ?x_35)
              (AboveOf14 ?x_35)
            )
          )
          (exists (?x_16 )
            (and
              (BelowOf7 ?x_16)
              (AboveOf17 ?x_16)
            )
          )
          (exists (?x_11 )
            (and
              (AboveOf17 ?x_11)
              (BelowOf16 ?x_11)
            )
          )
          (exists (?x_8 )
            (and
              (RightOf9 ?x_8)
              (LeftOf8 ?x_8)
            )
          )
          (exists (?x_2 )
            (and
              (BelowOf1 ?x_2)
              (AboveOf16 ?x_2)
            )
          )
          (exists (?x_16 )
            (and
              (AboveOf13 ?x_16)
              (BelowOf4 ?x_16)
            )
          )
          (exists (?x_26 )
            (and
              (AboveOf18 ?x_26)
              (BelowOf3 ?x_26)
            )
          )
          (exists (?x_0 )
            (and
              (BelowOf6 ?x_0)
              (AboveOf10 ?x_0)
            )
          )
          (exists (?x_10 )
            (and
              (RightOf15 ?x_10)
              (LeftOf4 ?x_10)
            )
          )
          (exists (?x_30 )
            (and
              (LeftOf7 ?x_30)
              (RightOf15 ?x_30)
            )
          )
          (exists (?x_26 )
            (and
              (AboveOf18 ?x_26)
              (BelowOf5 ?x_26)
            )
          )
          (exists (?x_13 )
            (and
              (LeftOf1 ?x_13)
              (RightOf13 ?x_13)
            )
          )
          (exists (?x_34 )
            (and
              (RightOf17 ?x_34)
              (LeftOf8 ?x_34)
            )
          )
          (exists (?x_15 )
            (and
              (BelowOf3 ?x_15)
              (AboveOf16 ?x_15)
            )
          )
          (exists (?x_1 )
            (and
              (AboveOf18 ?x_1)
              (BelowOf4 ?x_1)
            )
          )
          (exists (?x_24 )
            (and
              (RightOf13 ?x_24)
              (LeftOf1 ?x_24)
            )
          )
          (exists (?x_30 )
            (and
              (LeftOf8 ?x_30)
              (RightOf14 ?x_30)
            )
          )
          (exists (?x_30 )
            (and
              (RightOf16 ?x_30)
              (LeftOf1 ?x_30)
            )
          )
          (exists (?x_34 )
            (and
              (LeftOf7 ?x_34)
              (RightOf18 ?x_34)
            )
          )
          (exists (?x_10 )
            (and
              (RightOf15 ?x_10)
              (LeftOf6 ?x_10)
            )
          )
          (exists (?x_22 )
            (and
              (LeftOf2 ?x_22)
              (RightOf17 ?x_22)
            )
          )
          (exists (?x_1 )
            (and
              (BelowOf2 ?x_1)
              (AboveOf18 ?x_1)
            )
          )
          (exists (?x_35 )
            (and
              (AboveOf16 ?x_35)
              (BelowOf7 ?x_35)
            )
          )
          (exists (?x_5 )
            (and
              (RightOf11 ?x_5)
              (LeftOf10 ?x_5)
            )
          )
          (exists (?x_24 )
            (and
              (RightOf9 ?x_24)
              (LeftOf2 ?x_24)
            )
          )
          (exists (?x_14 )
            (and
              (AboveOf18 ?x_14)
              (BelowOf8 ?x_14)
            )
          )
          (exists (?x_34 )
            (and
              (RightOf13 ?x_34)
              (LeftOf4 ?x_34)
            )
          )
          (exists (?x_23 )
            (and
              (AboveOf12 ?x_23)
              (BelowOf4 ?x_23)
            )
          )
          (exists (?x_9 )
            (and
              (AboveOf12 ?x_9)
              (BelowOf4 ?x_9)
            )
          )
          (exists (?x_31 )
            (and
              (AboveOf13 ?x_31)
              (BelowOf1 ?x_31)
            )
          )
          (exists (?x_25 )
            (and
              (RightOf9 ?x_25)
              (LeftOf4 ?x_25)
            )
          )
          (exists (?x_19 )
            (and
              (RightOf13 ?x_19)
              (LeftOf6 ?x_19)
            )
          )
          (exists (?x_27 )
            (and
              (LeftOf2 ?x_27)
              (RightOf16 ?x_27)
            )
          )
          (exists (?x_25 )
            (and
              (LeftOf6 ?x_25)
              (RightOf14 ?x_25)
            )
          )
          (exists (?x_22 )
            (and
              (LeftOf2 ?x_22)
              (RightOf14 ?x_22)
            )
          )
          (exists (?x_0 )
            (and
              (AboveOf17 ?x_0)
              (BelowOf9 ?x_0)
            )
          )
          (exists (?x_22 )
            (and
              (RightOf12 ?x_22)
              (LeftOf6 ?x_22)
            )
          )
          (exists (?x_0 )
            (and
              (AboveOf12 ?x_0)
              (BelowOf8 ?x_0)
            )
          )
          (exists (?x_6 )
            (and
              (AboveOf9 ?x_6)
              (BelowOf3 ?x_6)
            )
          )
          (exists (?x_35 )
            (and
              (BelowOf3 ?x_35)
              (AboveOf9 ?x_35)
            )
          )
          (exists (?x_29 )
            (and
              (BelowOf1 ?x_29)
              (AboveOf2 ?x_29)
            )
          )
          (exists (?x_13 )
            (and
              (LeftOf4 ?x_13)
              (RightOf10 ?x_13)
            )
          )
          (exists (?x_7 )
            (and
              (RightOf18 ?x_7)
              (LeftOf5 ?x_7)
            )
          )
          (exists (?x_35 )
            (and
              (AboveOf10 ?x_35)
              (BelowOf2 ?x_35)
            )
          )
          (exists (?x_24 )
            (and
              (LeftOf5 ?x_24)
              (RightOf16 ?x_24)
            )
          )
          (exists (?x_24 )
            (and
              (RightOf12 ?x_24)
              (LeftOf2 ?x_24)
            )
          )
          (exists (?x_23 )
            (and
              (BelowOf3 ?x_23)
              (AboveOf13 ?x_23)
            )
          )
          (exists (?x_3 )
            (and
              (RightOf7 ?x_3)
              (LeftOf2 ?x_3)
            )
          )
          (exists (?x_15 )
            (and
              (AboveOf11 ?x_15)
              (BelowOf7 ?x_15)
            )
          )
          (exists (?x_26 )
            (and
              (AboveOf15 ?x_26)
              (BelowOf1 ?x_26)
            )
          )
          (exists (?x_6 )
            (and
              (BelowOf2 ?x_6)
              (AboveOf14 ?x_6)
            )
          )
          (exists (?x_33 )
            (and
              (RightOf17 ?x_33)
              (LeftOf9 ?x_33)
            )
          )
          (exists (?x_18 )
            (and
              (AboveOf10 ?x_18)
              (BelowOf1 ?x_18)
            )
          )
          (exists (?x_11 )
            (and
              (AboveOf17 ?x_11)
              (BelowOf3 ?x_11)
            )
          )
          (exists (?x_21 )
            (and
              (AboveOf16 ?x_21)
              (BelowOf2 ?x_21)
            )
          )
          (exists (?x_19 )
            (and
              (LeftOf8 ?x_19)
              (RightOf17 ?x_19)
            )
          )
          (exists (?x_33 )
            (and
              (LeftOf8 ?x_33)
              (RightOf17 ?x_33)
            )
          )
          (exists (?x_3 )
            (and
              (LeftOf1 ?x_3)
              (RightOf11 ?x_3)
            )
          )
          (exists (?x_2 )
            (and
              (BelowOf12 ?x_2)
              (AboveOf18 ?x_2)
            )
          )
          (exists (?x_25 )
            (and
              (RightOf7 ?x_25)
              (LeftOf2 ?x_25)
            )
          )
          (exists (?x_22 )
            (and
              (LeftOf4 ?x_22)
              (RightOf18 ?x_22)
            )
          )
          (exists (?x_5 )
            (and
              (RightOf10 ?x_5)
              (LeftOf10 ?x_5)
            )
          )
          (exists (?x_35 )
            (and
              (AboveOf17 ?x_35)
              (BelowOf1 ?x_35)
            )
          )
          (exists (?x_12 )
            (and
              (BelowOf16 ?x_12)
              (AboveOf16 ?x_12)
            )
          )
          (exists (?x_19 )
            (and
              (RightOf13 ?x_19)
              (LeftOf3 ?x_19)
            )
          )
          (exists (?x_10 )
            (and
              (RightOf16 ?x_10)
              (LeftOf2 ?x_10)
            )
          )
          (exists (?x_27 )
            (and
              (LeftOf1 ?x_27)
              (RightOf16 ?x_27)
            )
          )
          (exists (?x_35 )
            (and
              (AboveOf16 ?x_35)
              (BelowOf9 ?x_35)
            )
          )
          (exists (?x_27 )
            (and
              (LeftOf3 ?x_27)
              (RightOf16 ?x_27)
            )
          )
          (exists (?x_21 )
            (and
              (AboveOf14 ?x_21)
              (BelowOf2 ?x_21)
            )
          )
          (exists (?x_16 )
            (and
              (BelowOf5 ?x_16)
              (AboveOf16 ?x_16)
            )
          )
          (exists (?x_27 )
            (and
              (LeftOf3 ?x_27)
              (RightOf8 ?x_27)
            )
          )
          (exists (?x_20 )
            (and
              (LeftOf1 ?x_20)
              (RightOf12 ?x_20)
            )
          )
          (exists (?x_35 )
            (and
              (BelowOf5 ?x_35)
              (AboveOf17 ?x_35)
            )
          )
          (exists (?x_0 )
            (and
              (AboveOf17 ?x_0)
              (BelowOf1 ?x_0)
            )
          )
          (exists (?x_21 )
            (and
              (AboveOf8 ?x_21)
              (BelowOf2 ?x_21)
            )
          )
          (exists (?x_35 )
            (and
              (AboveOf12 ?x_35)
              (BelowOf8 ?x_35)
            )
          )
          (exists (?x_10 )
            (and
              (RightOf14 ?x_10)
              (LeftOf10 ?x_10)
            )
          )
          (exists (?x_1 )
            (and
              (BelowOf1 ?x_1)
              (AboveOf15 ?x_1)
            )
          )
          (exists (?x_2 )
            (and
              (AboveOf14 ?x_2)
              (BelowOf12 ?x_2)
            )
          )
          (exists (?x_2 )
            (and
              (BelowOf5 ?x_2)
              (AboveOf15 ?x_2)
            )
          )
          (exists (?x_2 )
            (and
              (AboveOf18 ?x_2)
              (BelowOf8 ?x_2)
            )
          )
          (exists (?x_4 )
            (and
              (RightOf4 ?x_4)
              (LeftOf1 ?x_4)
            )
          )
          (exists (?x_12 )
            (and
              (AboveOf18 ?x_12)
              (BelowOf2 ?x_12)
            )
          )
          (exists (?x_15 )
            (and
              (BelowOf1 ?x_15)
              (AboveOf11 ?x_15)
            )
          )
          (exists (?x_16 )
            (and
              (AboveOf17 ?x_16)
              (BelowOf1 ?x_16)
            )
          )
          (exists (?x_15 )
            (and
              (AboveOf17 ?x_15)
              (BelowOf5 ?x_15)
            )
          )
          (exists (?x_6 )
            (and
              (AboveOf18 ?x_6)
              (BelowOf6 ?x_6)
            )
          )
          (exists (?x_2 )
            (and
              (AboveOf15 ?x_2)
              (BelowOf4 ?x_2)
            )
          )
          (exists (?x_22 )
            (and
              (LeftOf4 ?x_22)
              (RightOf12 ?x_22)
            )
          )
          (exists (?x_19 )
            (and
              (LeftOf10 ?x_19)
              (RightOf11 ?x_19)
            )
          )
          (exists (?x_7 )
            (and
              (LeftOf3 ?x_7)
              (RightOf15 ?x_7)
            )
          )
          (exists (?x_19 )
            (and
              (RightOf14 ?x_19)
              (LeftOf6 ?x_19)
            )
          )
          (exists (?x_23 )
            (and
              (AboveOf7 ?x_23)
              (BelowOf1 ?x_23)
            )
          )
          (exists (?x_8 )
            (and
              (RightOf17 ?x_8)
              (LeftOf7 ?x_8)
            )
          )
          (exists (?x_31 )
            (and
              (BelowOf13 ?x_31)
              (AboveOf14 ?x_31)
            )
          )
          (exists (?x_3 )
            (and
              (RightOf13 ?x_3)
              (LeftOf2 ?x_3)
            )
          )
          (exists (?x_27 )
            (and
              (RightOf14 ?x_27)
              (LeftOf4 ?x_27)
            )
          )
          (exists (?x_17 )
            (and
              (AboveOf8 ?x_17)
              (BelowOf4 ?x_17)
            )
          )
          (exists (?x_15 )
            (and
              (BelowOf1 ?x_15)
              (AboveOf16 ?x_15)
            )
          )
          (exists (?x_17 )
            (and
              (AboveOf5 ?x_17)
              (BelowOf1 ?x_17)
            )
          )
          (exists (?x_29 )
            (and
              (AboveOf17 ?x_29)
              (BelowOf1 ?x_29)
            )
          )
          (exists (?x_1 )
            (and
              (AboveOf16 ?x_1)
              (BelowOf14 ?x_1)
            )
          )
          (exists (?x_16 )
            (and
              (AboveOf13 ?x_16)
              (BelowOf10 ?x_16)
            )
          )
          (exists (?x_5 )
            (and
              (LeftOf2 ?x_5)
              (RightOf18 ?x_5)
            )
          )
          (exists (?x_21 )
            (and
              (AboveOf16 ?x_21)
              (BelowOf1 ?x_21)
            )
          )
          (exists (?x_0 )
            (and
              (AboveOf15 ?x_0)
              (BelowOf8 ?x_0)
            )
          )
          (exists (?x_22 )
            (and
              (LeftOf7 ?x_22)
              (RightOf14 ?x_22)
            )
          )
          (exists (?x_15 )
            (and
              (BelowOf7 ?x_15)
              (AboveOf16 ?x_15)
            )
          )
          (exists (?x_19 )
            (and
              (RightOf18 ?x_19)
              (LeftOf1 ?x_19)
            )
          )
          (exists (?x_7 )
            (and
              (RightOf18 ?x_7)
              (LeftOf13 ?x_7)
            )
          )
          (exists (?x_8 )
            (and
              (LeftOf2 ?x_8)
              (RightOf14 ?x_8)
            )
          )
          (exists (?x_5 )
            (and
              (RightOf15 ?x_5)
              (LeftOf10 ?x_5)
            )
          )
          (exists (?x_24 )
            (and
              (RightOf17 ?x_24)
              (LeftOf5 ?x_24)
            )
          )
          (exists (?x_7 )
            (and
              (LeftOf13 ?x_7)
              (RightOf16 ?x_7)
            )
          )
          (exists (?x_11 )
            (and
              (BelowOf3 ?x_11)
              (AboveOf18 ?x_11)
            )
          )
          (exists (?x_27 )
            (and
              (LeftOf3 ?x_27)
              (RightOf10 ?x_27)
            )
          )
          (exists (?x_17 )
            (and
              (BelowOf4 ?x_17)
              (AboveOf9 ?x_17)
            )
          )
          (exists (?x_31 )
            (and
              (AboveOf15 ?x_31)
              (BelowOf7 ?x_31)
            )
          )
          (exists (?x_7 )
            (and
              (RightOf17 ?x_7)
              (LeftOf2 ?x_7)
            )
          )
          (exists (?x_16 )
            (and
              (AboveOf16 ?x_16)
              (BelowOf6 ?x_16)
            )
          )
          (exists (?x_12 )
            (and
              (AboveOf18 ?x_12)
              (BelowOf7 ?x_12)
            )
          )
          (exists (?x_34 )
            (and
              (RightOf15 ?x_34)
              (LeftOf9 ?x_34)
            )
          )
          (exists (?x_10 )
            (and
              (LeftOf14 ?x_10)
              (RightOf14 ?x_10)
            )
          )
          (exists (?x_15 )
            (and
              (BelowOf4 ?x_15)
              (AboveOf13 ?x_15)
            )
          )
          (exists (?x_20 )
            (and
              (RightOf3 ?x_20)
              (LeftOf1 ?x_20)
            )
          )
          (exists (?x_27 )
            (and
              (LeftOf3 ?x_27)
              (RightOf12 ?x_27)
            )
          )
          (exists (?x_23 )
            (and
              (AboveOf5 ?x_23)
              (BelowOf1 ?x_23)
            )
          )
          (exists (?x_34 )
            (and
              (RightOf13 ?x_34)
              (LeftOf6 ?x_34)
            )
          )
          (exists (?x_31 )
            (and
              (AboveOf18 ?x_31)
              (BelowOf5 ?x_31)
            )
          )
          (exists (?x_15 )
            (and
              (AboveOf14 ?x_15)
              (BelowOf2 ?x_15)
            )
          )
          (exists (?x_22 )
            (and
              (LeftOf8 ?x_22)
              (RightOf14 ?x_22)
            )
          )
          (exists (?x_35 )
            (and
              (AboveOf11 ?x_35)
              (BelowOf2 ?x_35)
            )
          )
          (exists (?x_6 )
            (and
              (BelowOf1 ?x_6)
              (AboveOf12 ?x_6)
            )
          )
          (exists (?x_7 )
            (and
              (LeftOf4 ?x_7)
              (RightOf15 ?x_7)
            )
          )
          (exists (?x_0 )
            (and
              (AboveOf12 ?x_0)
              (BelowOf4 ?x_0)
            )
          )
          (exists (?x_21 )
            (and
              (AboveOf5 ?x_21)
              (BelowOf3 ?x_21)
            )
          )
          (exists (?x_0 )
            (and
              (AboveOf15 ?x_0)
              (BelowOf9 ?x_0)
            )
          )
          (exists (?x_9 )
            (and
              (BelowOf7 ?x_9)
              (AboveOf11 ?x_9)
            )
          )
          (exists (?x_31 )
            (and
              (AboveOf13 ?x_31)
              (BelowOf10 ?x_31)
            )
          )
          (exists (?x_17 )
            (and
              (AboveOf8 ?x_17)
              (BelowOf3 ?x_17)
            )
          )
          (exists (?x_30 )
            (and
              (LeftOf5 ?x_30)
              (RightOf15 ?x_30)
            )
          )
          (exists (?x_5 )
            (and
              (LeftOf7 ?x_5)
              (RightOf14 ?x_5)
            )
          )
          (exists (?x_2 )
            (and
              (AboveOf17 ?x_2)
              (BelowOf4 ?x_2)
            )
          )
          (exists (?x_16 )
            (and
              (BelowOf5 ?x_16)
              (AboveOf15 ?x_16)
            )
          )
          (exists (?x_5 )
            (and
              (LeftOf9 ?x_5)
              (RightOf16 ?x_5)
            )
          )
          (exists (?x_19 )
            (and
              (RightOf16 ?x_19)
              (LeftOf6 ?x_19)
            )
          )
          (exists (?x_5 )
            (and
              (RightOf16 ?x_5)
              (LeftOf10 ?x_5)
            )
          )
          (exists (?x_13 )
            (and
              (LeftOf1 ?x_13)
              (RightOf7 ?x_13)
            )
          )
          (exists (?x_9 )
            (and
              (AboveOf8 ?x_9)
              (BelowOf4 ?x_9)
            )
          )
          (exists (?x_19 )
            (and
              (LeftOf10 ?x_19)
              (RightOf14 ?x_19)
            )
          )
          (exists (?x_6 )
            (and
              (AboveOf14 ?x_6)
              (BelowOf4 ?x_6)
            )
          )
          (exists (?x_6 )
            (and
              (AboveOf8 ?x_6)
              (BelowOf6 ?x_6)
            )
          )
          (exists (?x_8 )
            (and
              (RightOf10 ?x_8)
              (LeftOf3 ?x_8)
            )
          )
          (exists (?x_5 )
            (and
              (RightOf14 ?x_5)
              (LeftOf3 ?x_5)
            )
          )
          (exists (?x_5 )
            (and
              (RightOf12 ?x_5)
              (LeftOf8 ?x_5)
            )
          )
          (exists (?x_26 )
            (and
              (AboveOf13 ?x_26)
              (BelowOf3 ?x_26)
            )
          )
          (exists (?x_31 )
            (and
              (BelowOf13 ?x_31)
              (AboveOf15 ?x_31)
            )
          )
          (exists (?x_22 )
            (and
              (LeftOf12 ?x_22)
              (RightOf14 ?x_22)
            )
          )
          (exists (?x_4 )
            (and
              (LeftOf2 ?x_4)
              (RightOf6 ?x_4)
            )
          )
          (exists (?x_8 )
            (and
              (LeftOf1 ?x_8)
              (RightOf12 ?x_8)
            )
          )
          (exists (?x_6 )
            (and
              (AboveOf9 ?x_6)
              (BelowOf6 ?x_6)
            )
          )
          (exists (?x_0 )
            (and
              (AboveOf18 ?x_0)
              (BelowOf2 ?x_0)
            )
          )
          (exists (?x_9 )
            (and
              (BelowOf5 ?x_9)
              (AboveOf13 ?x_9)
            )
          )
          (exists (?x_11 )
            (and
              (BelowOf5 ?x_11)
              (AboveOf18 ?x_11)
            )
          )
          (exists (?x_6 )
            (and
              (AboveOf14 ?x_6)
              (BelowOf6 ?x_6)
            )
          )
          (exists (?x_1 )
            (and
              (BelowOf2 ?x_1)
              (AboveOf16 ?x_1)
            )
          )
          (exists (?x_10 )
            (and
              (RightOf18 ?x_10)
              (LeftOf8 ?x_10)
            )
          )
          (exists (?x_27 )
            (and
              (LeftOf1 ?x_27)
              (RightOf13 ?x_27)
            )
          )
          (exists (?x_3 )
            (and
              (RightOf5 ?x_3)
              (LeftOf3 ?x_3)
            )
          )
          (exists (?x_2 )
            (and
              (AboveOf14 ?x_2)
              (BelowOf9 ?x_2)
            )
          )
          (exists (?x_16 )
            (and
              (AboveOf17 ?x_16)
              (BelowOf6 ?x_16)
            )
          )
          (exists (?x_11 )
            (and
              (BelowOf7 ?x_11)
              (AboveOf18 ?x_11)
            )
          )
          (exists (?x_21 )
            (and
              (AboveOf3 ?x_21)
              (BelowOf2 ?x_21)
            )
          )
          (exists (?x_28 )
            (and
              (LeftOf6 ?x_28)
              (RightOf18 ?x_28)
            )
          )
          (exists (?x_26 )
            (and
              (AboveOf18 ?x_26)
              (BelowOf9 ?x_26)
            )
          )
          (exists (?x_24 )
            (and
              (LeftOf4 ?x_24)
              (RightOf10 ?x_24)
            )
          )
          (exists (?x_21 )
            (and
              (AboveOf5 ?x_21)
              (BelowOf1 ?x_21)
            )
          )
          (exists (?x_10 )
            (and
              (LeftOf6 ?x_10)
              (RightOf14 ?x_10)
            )
          )
          (exists (?x_32 )
            (and
              (LeftOf14 ?x_32)
              (RightOf18 ?x_32)
            )
          )
          (exists (?x_35 )
            (and
              (AboveOf12 ?x_35)
              (BelowOf9 ?x_35)
            )
          )
          (exists (?x_26 )
            (and
              (AboveOf12 ?x_26)
              (BelowOf8 ?x_26)
            )
          )
          (exists (?x_33 )
            (and
              (LeftOf9 ?x_33)
              (RightOf18 ?x_33)
            )
          )
          (exists (?x_7 )
            (and
              (LeftOf8 ?x_7)
              (RightOf16 ?x_7)
            )
          )
          (exists (?x_12 )
            (and
              (BelowOf2 ?x_12)
              (AboveOf18 ?x_12)
            )
          )
          (exists (?x_22 )
            (and
              (LeftOf7 ?x_22)
              (RightOf12 ?x_22)
            )
          )
          (exists (?x_8 )
            (and
              (RightOf16 ?x_8)
              (LeftOf1 ?x_8)
            )
          )
          (exists (?x_21 )
            (and
              (BelowOf3 ?x_21)
              (AboveOf18 ?x_21)
            )
          )
          (exists (?x_35 )
            (and
              (AboveOf18 ?x_35)
              (BelowOf2 ?x_35)
            )
          )
          (exists (?x_9 )
            (and
              (AboveOf8 ?x_9)
              (BelowOf3 ?x_9)
            )
          )
          (exists (?x_34 )
            (and
              (RightOf18 ?x_34)
              (LeftOf3 ?x_34)
            )
          )
          (exists (?x_24 )
            (and
              (RightOf7 ?x_24)
              (LeftOf1 ?x_24)
            )
          )
          (exists (?x_4 )
            (and
              (LeftOf2 ?x_4)
              (RightOf15 ?x_4)
            )
          )
          (exists (?x_15 )
            (and
              (AboveOf14 ?x_15)
              (BelowOf8 ?x_15)
            )
          )
          (exists (?x_27 )
            (and
              (RightOf13 ?x_27)
              (LeftOf2 ?x_27)
            )
          )
          (exists (?x_8 )
            (and
              (RightOf11 ?x_8)
              (LeftOf1 ?x_8)
            )
          )
          (exists (?x_35 )
            (and
              (AboveOf12 ?x_35)
              (BelowOf3 ?x_35)
            )
          )
          (exists (?x_35 )
            (and
              (BelowOf9 ?x_35)
              (AboveOf18 ?x_35)
            )
          )
          (exists (?x_5 )
            (and
              (RightOf17 ?x_5)
              (LeftOf8 ?x_5)
            )
          )
          (exists (?x_16 )
            (and
              (BelowOf8 ?x_16)
              (AboveOf12 ?x_16)
            )
          )
          (exists (?x_16 )
            (and
              (AboveOf14 ?x_16)
              (BelowOf6 ?x_16)
            )
          )
          (exists (?x_0 )
            (and
              (BelowOf3 ?x_0)
              (AboveOf14 ?x_0)
            )
          )
          (exists (?x_3 )
            (and
              (LeftOf1 ?x_3)
              (RightOf18 ?x_3)
            )
          )
          (exists (?x_35 )
            (and
              (BelowOf4 ?x_35)
              (AboveOf18 ?x_35)
            )
          )
          (exists (?x_35 )
            (and
              (AboveOf11 ?x_35)
              (BelowOf9 ?x_35)
            )
          )
          (exists (?x_6 )
            (and
              (AboveOf17 ?x_6)
              (BelowOf3 ?x_6)
            )
          )
          (exists (?x_12 )
            (and
              (AboveOf17 ?x_12)
              (BelowOf5 ?x_12)
            )
          )
          (exists (?x_24 )
            (and
              (RightOf17 ?x_24)
              (LeftOf2 ?x_24)
            )
          )
          (exists (?x_21 )
            (and
              (AboveOf8 ?x_21)
              (BelowOf1 ?x_21)
            )
          )
          (exists (?x_26 )
            (and
              (AboveOf15 ?x_26)
              (BelowOf6 ?x_26)
            )
          )
          (exists (?x_0 )
            (and
              (AboveOf14 ?x_0)
              (BelowOf2 ?x_0)
            )
          )
          (exists (?x_3 )
            (and
              (LeftOf1 ?x_3)
              (RightOf16 ?x_3)
            )
          )
          (exists (?x_12 )
            (and
              (AboveOf17 ?x_12)
              (BelowOf15 ?x_12)
            )
          )
          (exists (?x_32 )
            (and
              (LeftOf10 ?x_32)
              (RightOf17 ?x_32)
            )
          )
          (exists (?x_33 )
            (and
              (LeftOf5 ?x_33)
              (RightOf18 ?x_33)
            )
          )
          (exists (?x_33 )
            (and
              (RightOf17 ?x_33)
              (LeftOf12 ?x_33)
            )
          )
          (exists (?x_1 )
            (and
              (AboveOf18 ?x_1)
              (BelowOf14 ?x_1)
            )
          )
          (exists (?x_15 )
            (and
              (BelowOf1 ?x_15)
              (AboveOf13 ?x_15)
            )
          )
          (exists (?x_8 )
            (and
              (LeftOf6 ?x_8)
              (RightOf8 ?x_8)
            )
          )
          (exists (?x_32 )
            (and
              (RightOf17 ?x_32)
              (LeftOf14 ?x_32)
            )
          )
          (exists (?x_19 )
            (and
              (RightOf18 ?x_19)
              (LeftOf11 ?x_19)
            )
          )
          (exists (?x_35 )
            (and
              (AboveOf10 ?x_35)
              (BelowOf7 ?x_35)
            )
          )
          (exists (?x_15 )
            (and
              (AboveOf12 ?x_15)
              (BelowOf7 ?x_15)
            )
          )
          (exists (?x_16 )
            (and
              (AboveOf12 ?x_16)
              (BelowOf6 ?x_16)
            )
          )
          (exists (?x_25 )
            (and
              (LeftOf1 ?x_25)
              (RightOf10 ?x_25)
            )
          )
          (exists (?x_6 )
            (and
              (AboveOf15 ?x_6)
              (BelowOf4 ?x_6)
            )
          )
          (exists (?x_26 )
            (and
              (BelowOf9 ?x_26)
              (AboveOf14 ?x_26)
            )
          )
          (exists (?x_22 )
            (and
              (RightOf15 ?x_22)
              (LeftOf10 ?x_22)
            )
          )
          (exists (?x_26 )
            (and
              (BelowOf2 ?x_26)
              (AboveOf13 ?x_26)
            )
          )
          (exists (?x_33 )
            (and
              (RightOf17 ?x_33)
              (LeftOf13 ?x_33)
            )
          )
          (exists (?x_26 )
            (and
              (BelowOf9 ?x_26)
              (AboveOf17 ?x_26)
            )
          )
          (exists (?x_15 )
            (and
              (BelowOf4 ?x_15)
              (AboveOf17 ?x_15)
            )
          )
          (exists (?x_34 )
            (and
              (RightOf11 ?x_34)
              (LeftOf5 ?x_34)
            )
          )
          (exists (?x_7 )
            (and
              (RightOf17 ?x_7)
              (LeftOf13 ?x_7)
            )
          )
          (exists (?x_29 )
            (and
              (BelowOf1 ?x_29)
              (AboveOf9 ?x_29)
            )
          )
          (exists (?x_8 )
            (and
              (RightOf12 ?x_8)
              (LeftOf6 ?x_8)
            )
          )
          (exists (?x_2 )
            (and
              (AboveOf14 ?x_2)
              (BelowOf13 ?x_2)
            )
          )
          (exists (?x_5 )
            (and
              (RightOf16 ?x_5)
              (LeftOf3 ?x_5)
            )
          )
          (exists (?x_25 )
            (and
              (RightOf17 ?x_25)
              (LeftOf2 ?x_25)
            )
          )
          (exists (?x_16 )
            (and
              (AboveOf13 ?x_16)
              (BelowOf5 ?x_16)
            )
          )
          (exists (?x_1 )
            (and
              (BelowOf6 ?x_1)
              (AboveOf17 ?x_1)
            )
          )
          (exists (?x_24 )
            (and
              (LeftOf5 ?x_24)
              (RightOf18 ?x_24)
            )
          )
          (exists (?x_26 )
            (and
              (AboveOf18 ?x_26)
              (BelowOf2 ?x_26)
            )
          )
          (exists (?x_31 )
            (and
              (BelowOf2 ?x_31)
              (AboveOf15 ?x_31)
            )
          )
          (exists (?x_11 )
            (and
              (AboveOf17 ?x_11)
              (BelowOf9 ?x_11)
            )
          )
          (exists (?x_13 )
            (and
              (RightOf15 ?x_13)
              (LeftOf2 ?x_13)
            )
          )
          (exists (?x_2 )
            (and
              (AboveOf15 ?x_2)
              (BelowOf8 ?x_2)
            )
          )
          (exists (?x_22 )
            (and
              (LeftOf1 ?x_22)
              (RightOf13 ?x_22)
            )
          )
          (exists (?x_1 )
            (and
              (BelowOf2 ?x_1)
              (AboveOf15 ?x_1)
            )
          )
          (exists (?x_13 )
            (and
              (RightOf13 ?x_13)
              (LeftOf3 ?x_13)
            )
          )
          (exists (?x_24 )
            (and
              (RightOf16 ?x_24)
              (LeftOf2 ?x_24)
            )
          )
          (exists (?x_16 )
            (and
              (AboveOf12 ?x_16)
              (BelowOf3 ?x_16)
            )
          )
          (exists (?x_34 )
            (and
              (LeftOf1 ?x_34)
              (RightOf11 ?x_34)
            )
          )
          (exists (?x_9 )
            (and
              (AboveOf10 ?x_9)
              (BelowOf2 ?x_9)
            )
          )
          (exists (?x_11 )
            (and
              (BelowOf1 ?x_11)
              (AboveOf18 ?x_11)
            )
          )
          (exists (?x_8 )
            (and
              (RightOf8 ?x_8)
              (LeftOf8 ?x_8)
            )
          )
          (exists (?x_9 )
            (and
              (AboveOf11 ?x_9)
              (BelowOf6 ?x_9)
            )
          )
          (exists (?x_12 )
            (and
              (AboveOf17 ?x_12)
              (BelowOf2 ?x_12)
            )
          )
          (exists (?x_11 )
            (and
              (BelowOf6 ?x_11)
              (AboveOf18 ?x_11)
            )
          )
          (exists (?x_25 )
            (and
              (RightOf14 ?x_25)
              (LeftOf2 ?x_25)
            )
          )
          (exists (?x_15 )
            (and
              (AboveOf10 ?x_15)
              (BelowOf6 ?x_15)
            )
          )
          (exists (?x_32 )
            (and
              (RightOf17 ?x_32)
              (LeftOf7 ?x_32)
            )
          )
          (exists (?x_33 )
            (and
              (RightOf17 ?x_33)
              (LeftOf16 ?x_33)
            )
          )
          (exists (?x_11 )
            (and
              (AboveOf17 ?x_11)
              (BelowOf13 ?x_11)
            )
          )
          (exists (?x_6 )
            (and
              (BelowOf2 ?x_6)
              (AboveOf17 ?x_6)
            )
          )
          (exists (?x_27 )
            (and
              (LeftOf1 ?x_27)
              (RightOf6 ?x_27)
            )
          )
          (exists (?x_25 )
            (and
              (RightOf18 ?x_25)
              (LeftOf4 ?x_25)
            )
          )
          (exists (?x_25 )
            (and
              (LeftOf4 ?x_25)
              (RightOf18 ?x_25)
            )
          )
          (exists (?x_22 )
            (and
              (LeftOf11 ?x_22)
              (RightOf12 ?x_22)
            )
          )
          (exists (?x_17 )
            (and
              (AboveOf13 ?x_17)
              (BelowOf3 ?x_17)
            )
          )
          (exists (?x_17 )
            (and
              (AboveOf16 ?x_17)
              (BelowOf1 ?x_17)
            )
          )
          (exists (?x_21 )
            (and
              (BelowOf1 ?x_21)
              (AboveOf15 ?x_21)
            )
          )
          (exists (?x_2 )
            (and
              (BelowOf2 ?x_2)
              (AboveOf16 ?x_2)
            )
          )
          (exists (?x_10 )
            (and
              (RightOf18 ?x_10)
              (LeftOf4 ?x_10)
            )
          )
          (exists (?x_24 )
            (and
              (LeftOf6 ?x_24)
              (RightOf10 ?x_24)
            )
          )
          (exists (?x_22 )
            (and
              (RightOf18 ?x_22)
              (LeftOf3 ?x_22)
            )
          )
          (exists (?x_10 )
            (and
              (RightOf18 ?x_10)
              (LeftOf13 ?x_10)
            )
          )
          (exists (?x_7 )
            (and
              (RightOf16 ?x_7)
              (LeftOf6 ?x_7)
            )
          )
          (exists (?x_33 )
            (and
              (LeftOf14 ?x_33)
              (RightOf16 ?x_33)
            )
          )
          (exists (?x_25 )
            (and
              (RightOf9 ?x_25)
              (LeftOf2 ?x_25)
            )
          )
          (exists (?x_12 )
            (and
              (AboveOf17 ?x_12)
              (BelowOf4 ?x_12)
            )
          )
          (exists (?x_17 )
            (and
              (BelowOf4 ?x_17)
              (AboveOf17 ?x_17)
            )
          )
          (exists (?x_5 )
            (and
              (LeftOf9 ?x_5)
              (RightOf14 ?x_5)
            )
          )
          (exists (?x_30 )
            (and
              (LeftOf3 ?x_30)
              (RightOf13 ?x_30)
            )
          )
          (exists (?x_26 )
            (and
              (BelowOf3 ?x_26)
              (AboveOf12 ?x_26)
            )
          )
          (exists (?x_22 )
            (and
              (RightOf16 ?x_22)
              (LeftOf12 ?x_22)
            )
          )
          (exists (?x_19 )
            (and
              (LeftOf9 ?x_19)
              (RightOf14 ?x_19)
            )
          )
          (exists (?x_12 )
            (and
              (AboveOf17 ?x_12)
              (BelowOf9 ?x_12)
            )
          )
          (exists (?x_2 )
            (and
              (AboveOf17 ?x_2)
              (BelowOf9 ?x_2)
            )
          )
          (exists (?x_35 )
            (and
              (BelowOf2 ?x_35)
              (AboveOf10 ?x_35)
            )
          )
          (exists (?x_0 )
            (and
              (AboveOf12 ?x_0)
              (BelowOf6 ?x_0)
            )
          )
          (exists (?x_15 )
            (and
              (AboveOf10 ?x_15)
              (BelowOf3 ?x_15)
            )
          )
          (exists (?x_16 )
            (and
              (AboveOf11 ?x_16)
              (BelowOf10 ?x_16)
            )
          )
          (exists (?x_8 )
            (and
              (RightOf15 ?x_8)
              (LeftOf8 ?x_8)
            )
          )
          (exists (?x_15 )
            (and
              (AboveOf17 ?x_15)
              (BelowOf7 ?x_15)
            )
          )
          (exists (?x_4 )
            (and
              (RightOf7 ?x_4)
              (LeftOf1 ?x_4)
            )
          )
          (exists (?x_19 )
            (and
              (LeftOf10 ?x_19)
              (RightOf16 ?x_19)
            )
          )
          (exists (?x_19 )
            (and
              (RightOf15 ?x_19)
              (LeftOf10 ?x_19)
            )
          )
          (exists (?x_27 )
            (and
              (RightOf9 ?x_27)
              (LeftOf3 ?x_27)
            )
          )
          (exists (?x_12 )
            (and
              (BelowOf12 ?x_12)
              (AboveOf16 ?x_12)
            )
          )
          (exists (?x_25 )
            (and
              (RightOf7 ?x_25)
              (LeftOf5 ?x_25)
            )
          )
          (exists (?x_12 )
            (and
              (BelowOf5 ?x_12)
              (AboveOf18 ?x_12)
            )
          )
          (exists (?x_26 )
            (and
              (BelowOf7 ?x_26)
              (AboveOf15 ?x_26)
            )
          )
          (exists (?x_0 )
            (and
              (AboveOf16 ?x_0)
              (BelowOf10 ?x_0)
            )
          )
          (exists (?x_7 )
            (and
              (RightOf17 ?x_7)
              (LeftOf11 ?x_7)
            )
          )
          (exists (?x_30 )
            (and
              (RightOf17 ?x_30)
              (LeftOf7 ?x_30)
            )
          )
          (exists (?x_35 )
            (and
              (BelowOf5 ?x_35)
              (AboveOf15 ?x_35)
            )
          )
          (exists (?x_34 )
            (and
              (LeftOf4 ?x_34)
              (RightOf17 ?x_34)
            )
          )
          (exists (?x_19 )
            (and
              (LeftOf2 ?x_19)
              (RightOf16 ?x_19)
            )
          )
          (exists (?x_17 )
            (and
              (AboveOf13 ?x_17)
              (BelowOf2 ?x_17)
            )
          )
          (exists (?x_19 )
            (and
              (LeftOf8 ?x_19)
              (RightOf12 ?x_19)
            )
          )
          (exists (?x_13 )
            (and
              (RightOf7 ?x_13)
              (LeftOf3 ?x_13)
            )
          )
          (exists (?x_2 )
            (and
              (AboveOf14 ?x_2)
              (BelowOf4 ?x_2)
            )
          )
          (exists (?x_29 )
            (and
              (AboveOf9 ?x_29)
              (BelowOf2 ?x_29)
            )
          )
          (exists (?x_7 )
            (and
              (RightOf18 ?x_7)
              (LeftOf2 ?x_7)
            )
          )
          (exists (?x_21 )
            (and
              (BelowOf3 ?x_21)
              (AboveOf7 ?x_21)
            )
          )
          (exists (?x_16 )
            (and
              (BelowOf5 ?x_16)
              (AboveOf13 ?x_16)
            )
          )
          (exists (?x_26 )
            (and
              (BelowOf5 ?x_26)
              (AboveOf17 ?x_26)
            )
          )
          (exists (?x_35 )
            (and
              (AboveOf11 ?x_35)
              (BelowOf1 ?x_35)
            )
          )
          (exists (?x_26 )
            (and
              (AboveOf18 ?x_26)
              (BelowOf4 ?x_26)
            )
          )
          (exists (?x_34 )
            (and
              (RightOf13 ?x_34)
              (LeftOf1 ?x_34)
            )
          )
          (exists (?x_31 )
            (and
              (AboveOf18 ?x_31)
              (BelowOf2 ?x_31)
            )
          )
          (exists (?x_1 )
            (and
              (AboveOf18 ?x_1)
              (BelowOf2 ?x_1)
            )
          )
          (exists (?x_15 )
            (and
              (AboveOf17 ?x_15)
              (BelowOf2 ?x_15)
            )
          )
          (exists (?x_23 )
            (and
              (AboveOf13 ?x_23)
              (BelowOf1 ?x_23)
            )
          )
          (exists (?x_32 )
            (and
              (RightOf18 ?x_32)
              (LeftOf9 ?x_32)
            )
          )
          (exists (?x_15 )
            (and
              (AboveOf14 ?x_15)
              (BelowOf5 ?x_15)
            )
          )
          (exists (?x_17 )
            (and
              (BelowOf4 ?x_17)
              (AboveOf15 ?x_17)
            )
          )
          (exists (?x_12 )
            (and
              (BelowOf3 ?x_12)
              (AboveOf18 ?x_12)
            )
          )
          (exists (?x_6 )
            (and
              (BelowOf5 ?x_6)
              (AboveOf15 ?x_6)
            )
          )
          (exists (?x_33 )
            (and
              (LeftOf5 ?x_33)
              (RightOf17 ?x_33)
            )
          )
          (exists (?x_16 )
            (and
              (BelowOf8 ?x_16)
              (AboveOf16 ?x_16)
            )
          )
          (exists (?x_17 )
            (and
              (AboveOf11 ?x_17)
              (BelowOf3 ?x_17)
            )
          )
          (exists (?x_34 )
            (and
              (LeftOf2 ?x_34)
              (RightOf9 ?x_34)
            )
          )
          (exists (?x_13 )
            (and
              (RightOf5 ?x_13)
              (LeftOf3 ?x_13)
            )
          )
          (exists (?x_26 )
            (and
              (AboveOf18 ?x_26)
              (BelowOf6 ?x_26)
            )
          )
          (exists (?x_19 )
            (and
              (RightOf12 ?x_19)
              (LeftOf3 ?x_19)
            )
          )
          (exists (?x_5 )
            (and
              (RightOf15 ?x_5)
              (LeftOf6 ?x_5)
            )
          )
          (exists (?x_7 )
            (and
              (RightOf18 ?x_7)
              (LeftOf4 ?x_7)
            )
          )
          (exists (?x_27 )
            (and
              (RightOf17 ?x_27)
              (LeftOf1 ?x_27)
            )
          )
          (exists (?x_25 )
            (and
              (RightOf7 ?x_25)
              (LeftOf1 ?x_25)
            )
          )
          (exists (?x_32 )
            (and
              (RightOf17 ?x_32)
              (LeftOf17 ?x_32)
            )
          )
          (exists (?x_35 )
            (and
              (BelowOf1 ?x_35)
              (AboveOf18 ?x_35)
            )
          )
          (exists (?x_14 )
            (and
              (AboveOf18 ?x_14)
              (BelowOf2 ?x_14)
            )
          )
          (exists (?x_27 )
            (and
              (LeftOf1 ?x_27)
              (RightOf18 ?x_27)
            )
          )
          (exists (?x_35 )
            (and
              (AboveOf14 ?x_35)
              (BelowOf1 ?x_35)
            )
          )
          (exists (?x_6 )
            (and
              (AboveOf18 ?x_6)
              (BelowOf3 ?x_6)
            )
          )
          (exists (?x_16 )
            (and
              (AboveOf16 ?x_16)
              (BelowOf7 ?x_16)
            )
          )
          (exists (?x_10 )
            (and
              (RightOf15 ?x_10)
              (LeftOf5 ?x_10)
            )
          )
          (exists (?x_7 )
            (and
              (RightOf17 ?x_7)
              (LeftOf12 ?x_7)
            )
          )
          (exists (?x_12 )
            (and
              (BelowOf14 ?x_12)
              (AboveOf17 ?x_12)
            )
          )
          (exists (?x_2 )
            (and
              (AboveOf17 ?x_2)
              (BelowOf8 ?x_2)
            )
          )
          (exists (?x_24 )
            (and
              (LeftOf3 ?x_24)
              (RightOf11 ?x_24)
            )
          )
          (exists (?x_24 )
            (and
              (RightOf14 ?x_24)
              (LeftOf7 ?x_24)
            )
          )
          (exists (?x_25 )
            (and
              (LeftOf2 ?x_25)
              (RightOf15 ?x_25)
            )
          )
          (exists (?x_6 )
            (and
              (AboveOf11 ?x_6)
              (BelowOf4 ?x_6)
            )
          )
          (exists (?x_4 )
            (and
              (LeftOf2 ?x_4)
              (RightOf14 ?x_4)
            )
          )
          (exists (?x_23 )
            (and
              (BelowOf2 ?x_23)
              (AboveOf18 ?x_23)
            )
          )
          (exists (?x_11 )
            (and
              (BelowOf11 ?x_11)
              (AboveOf18 ?x_11)
            )
          )
          (exists (?x_26 )
            (and
              (AboveOf13 ?x_26)
              (BelowOf4 ?x_26)
            )
          )
          (exists (?x_25 )
            (and
              (RightOf7 ?x_25)
              (LeftOf3 ?x_25)
            )
          )
          (exists (?x_25 )
            (and
              (RightOf17 ?x_25)
              (LeftOf1 ?x_25)
            )
          )
          (exists (?x_5 )
            (and
              (LeftOf5 ?x_5)
              (RightOf10 ?x_5)
            )
          )
          (exists (?x_14 )
            (and
              (AboveOf18 ?x_14)
              (BelowOf18 ?x_14)
            )
          )
          (exists (?x_8 )
            (and
              (RightOf9 ?x_8)
              (LeftOf6 ?x_8)
            )
          )
          (exists (?x_8 )
            (and
              (RightOf15 ?x_8)
              (LeftOf5 ?x_8)
            )
          )
          (exists (?x_24 )
            (and
              (LeftOf3 ?x_24)
              (RightOf18 ?x_24)
            )
          )
          (exists (?x_21 )
            (and
              (AboveOf14 ?x_21)
              (BelowOf3 ?x_21)
            )
          )
          (exists (?x_1 )
            (and
              (BelowOf15 ?x_1)
              (AboveOf15 ?x_1)
            )
          )
          (exists (?x_22 )
            (and
              (RightOf16 ?x_22)
              (LeftOf3 ?x_22)
            )
          )
          (exists (?x_22 )
            (and
              (LeftOf9 ?x_22)
              (RightOf13 ?x_22)
            )
          )
          (exists (?x_35 )
            (and
              (BelowOf9 ?x_35)
              (AboveOf15 ?x_35)
            )
          )
          (exists (?x_35 )
            (and
              (BelowOf3 ?x_35)
              (AboveOf16 ?x_35)
            )
          )
          (exists (?x_34 )
            (and
              (RightOf16 ?x_34)
              (LeftOf4 ?x_34)
            )
          )
          (exists (?x_30 )
            (and
              (LeftOf8 ?x_30)
              (RightOf18 ?x_30)
            )
          )
          (exists (?x_23 )
            (and
              (AboveOf7 ?x_23)
              (BelowOf4 ?x_23)
            )
          )
          (exists (?x_26 )
            (and
              (BelowOf10 ?x_26)
              (AboveOf12 ?x_26)
            )
          )
          (exists (?x_16 )
            (and
              (BelowOf7 ?x_16)
              (AboveOf12 ?x_16)
            )
          )
          (exists (?x_35 )
            (and
              (BelowOf5 ?x_35)
              (AboveOf14 ?x_35)
            )
          )
          (exists (?x_30 )
            (and
              (LeftOf4 ?x_30)
              (RightOf18 ?x_30)
            )
          )
          (exists (?x_6 )
            (and
              (BelowOf5 ?x_6)
              (AboveOf6 ?x_6)
            )
          )
          (exists (?x_19 )
            (and
              (RightOf15 ?x_19)
              (LeftOf3 ?x_19)
            )
          )
          (exists (?x_30 )
            (and
              (LeftOf5 ?x_30)
              (RightOf18 ?x_30)
            )
          )
          (exists (?x_34 )
            (and
              (RightOf12 ?x_34)
              (LeftOf5 ?x_34)
            )
          )
          (exists (?x_15 )
            (and
              (AboveOf9 ?x_15)
              (BelowOf2 ?x_15)
            )
          )
          (exists (?x_9 )
            (and
              (AboveOf7 ?x_9)
              (BelowOf3 ?x_9)
            )
          )
          (exists (?x_9 )
            (and
              (BelowOf4 ?x_9)
              (AboveOf12 ?x_9)
            )
          )
          (exists (?x_32 )
            (and
              (RightOf18 ?x_32)
              (LeftOf2 ?x_32)
            )
          )
          (exists (?x_24 )
            (and
              (LeftOf6 ?x_24)
              (RightOf16 ?x_24)
            )
          )
          (exists (?x_30 )
            (and
              (LeftOf13 ?x_30)
              (RightOf13 ?x_30)
            )
          )
          (exists (?x_5 )
            (and
              (RightOf13 ?x_5)
              (LeftOf7 ?x_5)
            )
          )
          (exists (?x_10 )
            (and
              (RightOf16 ?x_10)
              (LeftOf5 ?x_10)
            )
          )
          (exists (?x_16 )
            (and
              (BelowOf11 ?x_16)
              (AboveOf14 ?x_16)
            )
          )
          (exists (?x_25 )
            (and
              (LeftOf2 ?x_25)
              (RightOf10 ?x_25)
            )
          )
          (exists (?x_31 )
            (and
              (BelowOf4 ?x_31)
              (AboveOf18 ?x_31)
            )
          )
          (exists (?x_31 )
            (and
              (AboveOf16 ?x_31)
              (BelowOf6 ?x_31)
            )
          )
          (exists (?x_34 )
            (and
              (LeftOf2 ?x_34)
              (RightOf10 ?x_34)
            )
          )
          (exists (?x_22 )
            (and
              (RightOf18 ?x_22)
              (LeftOf7 ?x_22)
            )
          )
          (exists (?x_13 )
            (and
              (RightOf18 ?x_13)
              (LeftOf1 ?x_13)
            )
          )
          (exists (?x_34 )
            (and
              (LeftOf3 ?x_34)
              (RightOf14 ?x_34)
            )
          )
          (exists (?x_35 )
            (and
              (AboveOf12 ?x_35)
              (BelowOf7 ?x_35)
            )
          )
          (exists (?x_9 )
            (and
              (BelowOf7 ?x_9)
              (AboveOf18 ?x_9)
            )
          )
          (exists (?x_23 )
            (and
              (BelowOf2 ?x_23)
              (AboveOf15 ?x_23)
            )
          )
          (exists (?x_10 )
            (and
              (LeftOf13 ?x_10)
              (RightOf17 ?x_10)
            )
          )
          (exists (?x_30 )
            (and
              (LeftOf9 ?x_30)
              (RightOf15 ?x_30)
            )
          )
          (exists (?x_12 )
            (and
              (AboveOf16 ?x_12)
              (BelowOf9 ?x_12)
            )
          )
          (exists (?x_31 )
            (and
              (AboveOf18 ?x_31)
              (BelowOf10 ?x_31)
            )
          )
          (exists (?x_27 )
            (and
              (RightOf12 ?x_27)
              (LeftOf4 ?x_27)
            )
          )
          (exists (?x_25 )
            (and
              (LeftOf5 ?x_25)
              (RightOf14 ?x_25)
            )
          )
          (exists (?x_19 )
            (and
              (LeftOf11 ?x_19)
              (RightOf13 ?x_19)
            )
          )
          (exists (?x_8 )
            (and
              (LeftOf2 ?x_8)
              (RightOf13 ?x_8)
            )
          )
          (exists (?x_0 )
            (and
              (AboveOf16 ?x_0)
              (BelowOf2 ?x_0)
            )
          )
          (exists (?x_34 )
            (and
              (LeftOf5 ?x_34)
              (RightOf11 ?x_34)
            )
          )
          (exists (?x_16 )
            (and
              (AboveOf13 ?x_16)
              (BelowOf11 ?x_16)
            )
          )
          (exists (?x_10 )
            (and
              (LeftOf2 ?x_10)
              (RightOf18 ?x_10)
            )
          )
          (exists (?x_11 )
            (and
              (AboveOf17 ?x_11)
              (BelowOf10 ?x_11)
            )
          )
          (exists (?x_22 )
            (and
              (RightOf15 ?x_22)
              (LeftOf11 ?x_22)
            )
          )
          (exists (?x_33 )
            (and
              (LeftOf3 ?x_33)
              (RightOf16 ?x_33)
            )
          )
          (exists (?x_16 )
            (and
              (BelowOf5 ?x_16)
              (AboveOf17 ?x_16)
            )
          )
          (exists (?x_6 )
            (and
              (BelowOf2 ?x_6)
              (AboveOf15 ?x_6)
            )
          )
          (exists (?x_16 )
            (and
              (AboveOf16 ?x_16)
              (BelowOf10 ?x_16)
            )
          )
          (exists (?x_6 )
            (and
              (BelowOf1 ?x_6)
              (AboveOf16 ?x_6)
            )
          )
          (exists (?x_31 )
            (and
              (BelowOf10 ?x_31)
              (AboveOf18 ?x_31)
            )
          )
          (exists (?x_5 )
            (and
              (LeftOf9 ?x_5)
              (RightOf15 ?x_5)
            )
          )
          (exists (?x_34 )
            (and
              (LeftOf7 ?x_34)
              (RightOf9 ?x_34)
            )
          )
          (exists (?x_6 )
            (and
              (BelowOf2 ?x_6)
              (AboveOf11 ?x_6)
            )
          )
          (exists (?x_5 )
            (and
              (LeftOf1 ?x_5)
              (RightOf12 ?x_5)
            )
          )
          (exists (?x_4 )
            (and
              (RightOf3 ?x_4)
              (LeftOf2 ?x_4)
            )
          )
          (exists (?x_19 )
            (and
              (RightOf12 ?x_19)
              (LeftOf1 ?x_19)
            )
          )
          (exists (?x_10 )
            (and
              (LeftOf2 ?x_10)
              (RightOf15 ?x_10)
            )
          )
          (exists (?x_1 )
            (and
              (BelowOf11 ?x_1)
              (AboveOf17 ?x_1)
            )
          )
          (exists (?x_9 )
            (and
              (AboveOf7 ?x_9)
              (BelowOf1 ?x_9)
            )
          )
          (exists (?x_23 )
            (and
              (AboveOf16 ?x_23)
              (BelowOf4 ?x_23)
            )
          )
          (exists (?x_34 )
            (and
              (LeftOf9 ?x_34)
              (RightOf14 ?x_34)
            )
          )
          (exists (?x_9 )
            (and
              (BelowOf4 ?x_9)
              (AboveOf18 ?x_9)
            )
          )
          (exists (?x_22 )
            (and
              (LeftOf4 ?x_22)
              (RightOf15 ?x_22)
            )
          )
          (exists (?x_33 )
            (and
              (LeftOf8 ?x_33)
              (RightOf16 ?x_33)
            )
          )
          (exists (?x_1 )
            (and
              (BelowOf8 ?x_1)
              (AboveOf15 ?x_1)
            )
          )
          (exists (?x_8 )
            (and
              (LeftOf2 ?x_8)
              (RightOf9 ?x_8)
            )
          )
          (exists (?x_13 )
            (and
              (LeftOf1 ?x_13)
              (RightOf5 ?x_13)
            )
          )
          (exists (?x_13 )
            (and
              (RightOf18 ?x_13)
              (LeftOf2 ?x_13)
            )
          )
          (exists (?x_22 )
            (and
              (RightOf13 ?x_22)
              (LeftOf6 ?x_22)
            )
          )
          (exists (?x_30 )
            (and
              (RightOf16 ?x_30)
              (LeftOf7 ?x_30)
            )
          )
          (exists (?x_25 )
            (and
              (LeftOf3 ?x_25)
              (RightOf8 ?x_25)
            )
          )
          (exists (?x_23 )
            (and
              (AboveOf18 ?x_23)
              (BelowOf4 ?x_23)
            )
          )
          (exists (?x_35 )
            (and
              (AboveOf14 ?x_35)
              (BelowOf4 ?x_35)
            )
          )
          (exists (?x_27 )
            (and
              (LeftOf5 ?x_27)
              (RightOf7 ?x_27)
            )
          )
          (exists (?x_13 )
            (and
              (LeftOf4 ?x_13)
              (RightOf4 ?x_13)
            )
          )
          (exists (?x_30 )
            (and
              (LeftOf3 ?x_30)
              (RightOf18 ?x_30)
            )
          )
          (exists (?x_17 )
            (and
              (AboveOf17 ?x_17)
              (BelowOf1 ?x_17)
            )
          )
          (exists (?x_13 )
            (and
              (LeftOf4 ?x_13)
              (RightOf7 ?x_13)
            )
          )
          (exists (?x_7 )
            (and
              (LeftOf9 ?x_7)
              (RightOf16 ?x_7)
            )
          )
          (exists (?x_19 )
            (and
              (RightOf11 ?x_19)
              (LeftOf3 ?x_19)
            )
          )
          (exists (?x_10 )
            (and
              (LeftOf7 ?x_10)
              (RightOf17 ?x_10)
            )
          )
          (exists (?x_8 )
            (and
              (RightOf18 ?x_8)
              (LeftOf8 ?x_8)
            )
          )
          (exists (?x_32 )
            (and
              (LeftOf8 ?x_32)
              (RightOf17 ?x_32)
            )
          )
          (exists (?x_1 )
            (and
              (BelowOf10 ?x_1)
              (AboveOf15 ?x_1)
            )
          )
          (exists (?x_27 )
            (and
              (RightOf7 ?x_27)
              (LeftOf1 ?x_27)
            )
          )
          (exists (?x_32 )
            (and
              (LeftOf16 ?x_32)
              (RightOf17 ?x_32)
            )
          )
          (exists (?x_35 )
            (and
              (BelowOf5 ?x_35)
              (AboveOf16 ?x_35)
            )
          )
          (exists (?x_9 )
            (and
              (AboveOf13 ?x_9)
              (BelowOf2 ?x_9)
            )
          )
          (exists (?x_24 )
            (and
              (RightOf17 ?x_24)
              (LeftOf1 ?x_24)
            )
          )
          (exists (?x_27 )
            (and
              (RightOf17 ?x_27)
              (LeftOf2 ?x_27)
            )
          )
          (exists (?x_26 )
            (and
              (AboveOf17 ?x_26)
              (BelowOf5 ?x_26)
            )
          )
          (exists (?x_7 )
            (and
              (RightOf17 ?x_7)
              (LeftOf9 ?x_7)
            )
          )
          (exists (?x_26 )
            (and
              (BelowOf11 ?x_26)
              (AboveOf17 ?x_26)
            )
          )
          (exists (?x_27 )
            (and
              (RightOf10 ?x_27)
              (LeftOf2 ?x_27)
            )
          )
          (exists (?x_6 )
            (and
              (AboveOf7 ?x_6)
              (BelowOf5 ?x_6)
            )
          )
          (exists (?x_8 )
            (and
              (RightOf10 ?x_8)
              (LeftOf1 ?x_8)
            )
          )
          (exists (?x_9 )
            (and
              (BelowOf6 ?x_9)
              (AboveOf13 ?x_9)
            )
          )
          (exists (?x_34 )
            (and
              (RightOf13 ?x_34)
              (LeftOf3 ?x_34)
            )
          )
          (exists (?x_24 )
            (and
              (RightOf11 ?x_24)
              (LeftOf4 ?x_24)
            )
          )
          (exists (?x_2 )
            (and
              (BelowOf5 ?x_2)
              (AboveOf14 ?x_2)
            )
          )
          (exists (?x_22 )
            (and
              (RightOf14 ?x_22)
              (LeftOf10 ?x_22)
            )
          )
          (exists (?x_33 )
            (and
              (RightOf17 ?x_33)
              (LeftOf7 ?x_33)
            )
          )
          (exists (?x_30 )
            (and
              (LeftOf13 ?x_30)
              (RightOf18 ?x_30)
            )
          )
          (exists (?x_28 )
            (and
              (LeftOf2 ?x_28)
              (RightOf18 ?x_28)
            )
          )
          (exists (?x_21 )
            (and
              (BelowOf3 ?x_21)
              (AboveOf13 ?x_21)
            )
          )
          (exists (?x_24 )
            (and
              (RightOf15 ?x_24)
              (LeftOf1 ?x_24)
            )
          )
          (exists (?x_34 )
            (and
              (RightOf18 ?x_34)
              (LeftOf8 ?x_34)
            )
          )
          (exists (?x_16 )
            (and
              (BelowOf9 ?x_16)
              (AboveOf15 ?x_16)
            )
          )
          (exists (?x_22 )
            (and
              (RightOf15 ?x_22)
              (LeftOf9 ?x_22)
            )
          )
          (exists (?x_15 )
            (and
              (AboveOf10 ?x_15)
              (BelowOf1 ?x_15)
            )
          )
          (exists (?x_21 )
            (and
              (AboveOf5 ?x_21)
              (BelowOf2 ?x_21)
            )
          )
          (exists (?x_34 )
            (and
              (LeftOf7 ?x_34)
              (RightOf12 ?x_34)
            )
          )
          (exists (?x_9 )
            (and
              (AboveOf17 ?x_9)
              (BelowOf4 ?x_9)
            )
          )
          (exists (?x_4 )
            (and
              (RightOf3 ?x_4)
              (LeftOf1 ?x_4)
            )
          )
          (exists (?x_17 )
            (and
              (BelowOf1 ?x_17)
              (AboveOf9 ?x_17)
            )
          )
          (exists (?x_5 )
            (and
              (RightOf13 ?x_5)
              (LeftOf3 ?x_5)
            )
          )
          (exists (?x_10 )
            (and
              (RightOf16 ?x_10)
              (LeftOf14 ?x_10)
            )
          )
          (exists (?x_19 )
            (and
              (RightOf15 ?x_19)
              (LeftOf6 ?x_19)
            )
          )
          (exists (?x_10 )
            (and
              (RightOf18 ?x_10)
              (LeftOf3 ?x_10)
            )
          )
          (exists (?x_18 )
            (and
              (AboveOf2 ?x_18)
              (BelowOf1 ?x_18)
            )
          )
          (exists (?x_2 )
            (and
              (AboveOf15 ?x_2)
              (BelowOf7 ?x_2)
            )
          )
          (exists (?x_9 )
            (and
              (AboveOf8 ?x_9)
              (BelowOf6 ?x_9)
            )
          )
          (exists (?x_35 )
            (and
              (AboveOf17 ?x_35)
              (BelowOf3 ?x_35)
            )
          )
          (exists (?x_19 )
            (and
              (LeftOf4 ?x_19)
              (RightOf16 ?x_19)
            )
          )
          (exists (?x_27 )
            (and
              (RightOf7 ?x_27)
              (LeftOf3 ?x_27)
            )
          )
          (exists (?x_21 )
            (and
              (AboveOf11 ?x_21)
              (BelowOf2 ?x_21)
            )
          )
          (exists (?x_3 )
            (and
              (LeftOf1 ?x_3)
              (RightOf5 ?x_3)
            )
          )
          (exists (?x_35 )
            (and
              (BelowOf2 ?x_35)
              (AboveOf15 ?x_35)
            )
          )
          (exists (?x_21 )
            (and
              (BelowOf2 ?x_21)
              (AboveOf18 ?x_21)
            )
          )
          (exists (?x_8 )
            (and
              (RightOf16 ?x_8)
              (LeftOf2 ?x_8)
            )
          )
          (exists (?x_33 )
            (and
              (LeftOf6 ?x_33)
              (RightOf16 ?x_33)
            )
          )
          (exists (?x_27 )
            (and
              (LeftOf1 ?x_27)
              (RightOf10 ?x_27)
            )
          )
          (exists (?x_9 )
            (and
              (BelowOf7 ?x_9)
              (AboveOf8 ?x_9)
            )
          )
          (exists (?x_19 )
            (and
              (LeftOf5 ?x_19)
              (RightOf13 ?x_19)
            )
          )
          (exists (?x_31 )
            (and
              (AboveOf18 ?x_31)
              (BelowOf12 ?x_31)
            )
          )
          (exists (?x_21 )
            (and
              (AboveOf14 ?x_21)
              (BelowOf1 ?x_21)
            )
          )
          (exists (?x_4 )
            (and
              (RightOf11 ?x_4)
              (LeftOf1 ?x_4)
            )
          )
          (exists (?x_22 )
            (and
              (LeftOf9 ?x_22)
              (RightOf16 ?x_22)
            )
          )
          (exists (?x_2 )
            (and
              (BelowOf7 ?x_2)
              (AboveOf16 ?x_2)
            )
          )
          (exists (?x_16 )
            (and
              (BelowOf9 ?x_16)
              (AboveOf18 ?x_16)
            )
          )
          (exists (?x_8 )
            (and
              (RightOf18 ?x_8)
              (LeftOf6 ?x_8)
            )
          )
          (exists (?x_9 )
            (and
              (BelowOf6 ?x_9)
              (AboveOf15 ?x_9)
            )
          )
          (exists (?x_6 )
            (and
              (AboveOf17 ?x_6)
              (BelowOf4 ?x_6)
            )
          )
          (exists (?x_17 )
            (and
              (AboveOf4 ?x_17)
              (BelowOf3 ?x_17)
            )
          )
          (exists (?x_5 )
            (and
              (RightOf15 ?x_5)
              (LeftOf7 ?x_5)
            )
          )
          (exists (?x_31 )
            (and
              (AboveOf17 ?x_31)
              (BelowOf6 ?x_31)
            )
          )
          (exists (?x_15 )
            (and
              (BelowOf6 ?x_15)
              (AboveOf11 ?x_15)
            )
          )
          (exists (?x_15 )
            (and
              (BelowOf6 ?x_15)
              (AboveOf9 ?x_15)
            )
          )
          (exists (?x_9 )
            (and
              (BelowOf1 ?x_9)
              (AboveOf10 ?x_9)
            )
          )
          (exists (?x_2 )
            (and
              (BelowOf5 ?x_2)
              (AboveOf16 ?x_2)
            )
          )
          (exists (?x_34 )
            (and
              (LeftOf1 ?x_34)
              (RightOf14 ?x_34)
            )
          )
          (exists (?x_5 )
            (and
              (RightOf17 ?x_5)
              (LeftOf3 ?x_5)
            )
          )
          (exists (?x_35 )
            (and
              (BelowOf5 ?x_35)
              (AboveOf9 ?x_35)
            )
          )
          (exists (?x_34 )
            (and
              (RightOf13 ?x_34)
              (LeftOf9 ?x_34)
            )
          )
          (exists (?x_22 )
            (and
              (LeftOf12 ?x_22)
              (RightOf12 ?x_22)
            )
          )
          (exists (?x_28 )
            (and
              (LeftOf17 ?x_28)
              (RightOf18 ?x_28)
            )
          )
          (exists (?x_15 )
            (and
              (AboveOf12 ?x_15)
              (BelowOf1 ?x_15)
            )
          )
          (exists (?x_30 )
            (and
              (LeftOf12 ?x_30)
              (RightOf18 ?x_30)
            )
          )
          (exists (?x_7 )
            (and
              (LeftOf3 ?x_7)
              (RightOf18 ?x_7)
            )
          )
          (exists (?x_33 )
            (and
              (LeftOf10 ?x_33)
              (RightOf18 ?x_33)
            )
          )
          (exists (?x_26 )
            (and
              (BelowOf1 ?x_26)
              (AboveOf13 ?x_26)
            )
          )
          (exists (?x_22 )
            (and
              (RightOf15 ?x_22)
              (LeftOf8 ?x_22)
            )
          )
          (exists (?x_31 )
            (and
              (AboveOf14 ?x_31)
              (BelowOf6 ?x_31)
            )
          )
          (exists (?x_24 )
            (and
              (LeftOf7 ?x_24)
              (RightOf15 ?x_24)
            )
          )
          (exists (?x_11 )
            (and
              (AboveOf17 ?x_11)
              (BelowOf4 ?x_11)
            )
          )
          (exists (?x_12 )
            (and
              (AboveOf17 ?x_12)
              (BelowOf1 ?x_12)
            )
          )
          (exists (?x_35 )
            (and
              (AboveOf12 ?x_35)
              (BelowOf4 ?x_35)
            )
          )
          (exists (?x_3 )
            (and
              (LeftOf3 ?x_3)
              (RightOf9 ?x_3)
            )
          )
          (exists (?x_1 )
            (and
              (AboveOf16 ?x_1)
              (BelowOf15 ?x_1)
            )
          )
          (exists (?x_30 )
            (and
              (RightOf17 ?x_30)
              (LeftOf1 ?x_30)
            )
          )
          (exists (?x_6 )
            (and
              (BelowOf2 ?x_6)
              (AboveOf7 ?x_6)
            )
          )
          (exists (?x_8 )
            (and
              (RightOf17 ?x_8)
              (LeftOf6 ?x_8)
            )
          )
          (exists (?x_13 )
            (and
              (RightOf8 ?x_13)
              (LeftOf1 ?x_13)
            )
          )
          (exists (?x_5 )
            (and
              (LeftOf3 ?x_5)
              (RightOf10 ?x_5)
            )
          )
          (exists (?x_17 )
            (and
              (AboveOf13 ?x_17)
              (BelowOf4 ?x_17)
            )
          )
          (exists (?x_28 )
            (and
              (LeftOf11 ?x_28)
              (RightOf18 ?x_28)
            )
          )
          (exists (?x_34 )
            (and
              (RightOf14 ?x_34)
              (LeftOf8 ?x_34)
            )
          )
          (exists (?x_26 )
            (and
              (AboveOf18 ?x_26)
              (BelowOf12 ?x_26)
            )
          )
          (exists (?x_11 )
            (and
              (BelowOf12 ?x_11)
              (AboveOf18 ?x_11)
            )
          )
          (exists (?x_1 )
            (and
              (AboveOf17 ?x_1)
              (BelowOf12 ?x_1)
            )
          )
          (exists (?x_10 )
            (and
              (RightOf15 ?x_10)
              (LeftOf10 ?x_10)
            )
          )
          (exists (?x_32 )
            (and
              (RightOf17 ?x_32)
              (LeftOf1 ?x_32)
            )
          )
          (exists (?x_2 )
            (and
              (BelowOf9 ?x_2)
              (AboveOf18 ?x_2)
            )
          )
          (exists (?x_8 )
            (and
              (LeftOf5 ?x_8)
              (RightOf18 ?x_8)
            )
          )
          (exists (?x_29 )
            (and
              (AboveOf4 ?x_29)
              (BelowOf2 ?x_29)
            )
          )
          (exists (?x_22 )
            (and
              (LeftOf1 ?x_22)
              (RightOf12 ?x_22)
            )
          )
          (exists (?x_25 )
            (and
              (RightOf14 ?x_25)
              (LeftOf4 ?x_25)
            )
          )
          (exists (?x_8 )
            (and
              (LeftOf7 ?x_8)
              (RightOf9 ?x_8)
            )
          )
          (exists (?x_5 )
            (and
              (LeftOf6 ?x_5)
              (RightOf14 ?x_5)
            )
          )
          (exists (?x_1 )
            (and
              (BelowOf9 ?x_1)
              (AboveOf17 ?x_1)
            )
          )
          (exists (?x_9 )
            (and
              (BelowOf3 ?x_9)
              (AboveOf13 ?x_9)
            )
          )
          (exists (?x_1 )
            (and
              (BelowOf1 ?x_1)
              (AboveOf16 ?x_1)
            )
          )
          (exists (?x_20 )
            (and
              (LeftOf1 ?x_20)
              (RightOf2 ?x_20)
            )
          )
          (exists (?x_30 )
            (and
              (LeftOf10 ?x_30)
              (RightOf18 ?x_30)
            )
          )
          (exists (?x_16 )
            (and
              (BelowOf5 ?x_16)
              (AboveOf18 ?x_16)
            )
          )
          (exists (?x_5 )
            (and
              (RightOf16 ?x_5)
              (LeftOf5 ?x_5)
            )
          )
          (exists (?x_9 )
            (and
              (AboveOf11 ?x_9)
              (BelowOf5 ?x_9)
            )
          )
          (exists (?x_9 )
            (and
              (AboveOf17 ?x_9)
              (BelowOf3 ?x_9)
            )
          )
          (exists (?x_16 )
            (and
              (AboveOf13 ?x_16)
              (BelowOf7 ?x_16)
            )
          )
          (exists (?x_26 )
            (and
              (AboveOf16 ?x_26)
              (BelowOf6 ?x_26)
            )
          )
          (exists (?x_18 )
            (and
              (AboveOf8 ?x_18)
              (BelowOf1 ?x_18)
            )
          )
          (exists (?x_17 )
            (and
              (BelowOf1 ?x_17)
              (AboveOf6 ?x_17)
            )
          )
          (exists (?x_29 )
            (and
              (AboveOf13 ?x_29)
              (BelowOf1 ?x_29)
            )
          )
          (exists (?x_17 )
            (and
              (AboveOf16 ?x_17)
              (BelowOf3 ?x_17)
            )
          )
          (exists (?x_27 )
            (and
              (RightOf9 ?x_27)
              (LeftOf1 ?x_27)
            )
          )
          (exists (?x_25 )
            (and
              (LeftOf3 ?x_25)
              (RightOf9 ?x_25)
            )
          )
          (exists (?x_16 )
            (and
              (BelowOf1 ?x_16)
              (AboveOf17 ?x_16)
            )
          )
          (exists (?x_7 )
            (and
              (LeftOf4 ?x_7)
              (RightOf16 ?x_7)
            )
          )
          (exists (?x_24 )
            (and
              (RightOf8 ?x_24)
              (LeftOf4 ?x_24)
            )
          )
          (exists (?x_17 )
            (and
              (AboveOf8 ?x_17)
              (BelowOf2 ?x_17)
            )
          )
          (exists (?x_23 )
            (and
              (BelowOf3 ?x_23)
              (AboveOf12 ?x_23)
            )
          )
          (exists (?x_34 )
            (and
              (LeftOf2 ?x_34)
              (RightOf11 ?x_34)
            )
          )
          (exists (?x_13 )
            (and
              (RightOf10 ?x_13)
              (LeftOf2 ?x_13)
            )
          )
          (exists (?x_30 )
            (and
              (LeftOf2 ?x_30)
              (RightOf13 ?x_30)
            )
          )
          (exists (?x_9 )
            (and
              (AboveOf14 ?x_9)
              (BelowOf1 ?x_9)
            )
          )
          (exists (?x_15 )
            (and
              (BelowOf6 ?x_15)
              (AboveOf12 ?x_15)
            )
          )
          (exists (?x_34 )
            (and
              (LeftOf6 ?x_34)
              (RightOf14 ?x_34)
            )
          )
          (exists (?x_31 )
            (and
              (BelowOf10 ?x_31)
              (AboveOf14 ?x_31)
            )
          )
          (exists (?x_35 )
            (and
              (BelowOf6 ?x_35)
              (AboveOf10 ?x_35)
            )
          )
          (exists (?x_9 )
            (and
              (AboveOf14 ?x_9)
              (BelowOf4 ?x_9)
            )
          )
          (exists (?x_9 )
            (and
              (BelowOf3 ?x_9)
              (AboveOf9 ?x_9)
            )
          )
          (exists (?x_31 )
            (and
              (BelowOf12 ?x_31)
              (AboveOf17 ?x_31)
            )
          )
          (exists (?x_22 )
            (and
              (RightOf18 ?x_22)
              (LeftOf10 ?x_22)
            )
          )
          (exists (?x_26 )
            (and
              (BelowOf2 ?x_26)
              (AboveOf17 ?x_26)
            )
          )
          (exists (?x_35 )
            (and
              (BelowOf5 ?x_35)
              (AboveOf11 ?x_35)
            )
          )
          (exists (?x_0 )
            (and
              (AboveOf14 ?x_0)
              (BelowOf8 ?x_0)
            )
          )
          (exists (?x_34 )
            (and
              (LeftOf8 ?x_34)
              (RightOf14 ?x_34)
            )
          )
          (exists (?x_31 )
            (and
              (AboveOf16 ?x_31)
              (BelowOf7 ?x_31)
            )
          )
          (exists (?x_30 )
            (and
              (LeftOf12 ?x_30)
              (RightOf15 ?x_30)
            )
          )
          (exists (?x_19 )
            (and
              (LeftOf8 ?x_19)
              (RightOf13 ?x_19)
            )
          )
          (exists (?x_7 )
            (and
              (RightOf18 ?x_7)
              (LeftOf7 ?x_7)
            )
          )
          (exists (?x_23 )
            (and
              (BelowOf5 ?x_23)
              (AboveOf13 ?x_23)
            )
          )
          (exists (?x_8 )
            (and
              (LeftOf2 ?x_8)
              (RightOf8 ?x_8)
            )
          )
          (exists (?x_27 )
            (and
              (RightOf18 ?x_27)
              (LeftOf2 ?x_27)
            )
          )
          (exists (?x_23 )
            (and
              (AboveOf5 ?x_23)
              (BelowOf2 ?x_23)
            )
          )
          (exists (?x_19 )
            (and
              (LeftOf11 ?x_19)
              (RightOf14 ?x_19)
            )
          )
          (exists (?x_30 )
            (and
              (LeftOf13 ?x_30)
              (RightOf15 ?x_30)
            )
          )
          (exists (?x_17 )
            (and
              (AboveOf15 ?x_17)
              (BelowOf3 ?x_17)
            )
          )
          (exists (?x_33 )
            (and
              (RightOf16 ?x_33)
              (LeftOf2 ?x_33)
            )
          )
          (exists (?x_16 )
            (and
              (AboveOf16 ?x_16)
              (BelowOf8 ?x_16)
            )
          )
          (exists (?x_12 )
            (and
              (BelowOf12 ?x_12)
              (AboveOf17 ?x_12)
            )
          )
          (exists (?x_6 )
            (and
              (AboveOf16 ?x_6)
              (BelowOf6 ?x_6)
            )
          )
          (exists (?x_8 )
            (and
              (LeftOf5 ?x_8)
              (RightOf12 ?x_8)
            )
          )
          (exists (?x_6 )
            (and
              (AboveOf18 ?x_6)
              (BelowOf4 ?x_6)
            )
          )
          (exists (?x_13 )
            (and
              (RightOf15 ?x_13)
              (LeftOf1 ?x_13)
            )
          )
          (exists (?x_22 )
            (and
              (LeftOf1 ?x_22)
              (RightOf14 ?x_22)
            )
          )
          (exists (?x_17 )
            (and
              (BelowOf2 ?x_17)
              (AboveOf15 ?x_17)
            )
          )
          (exists (?x_8 )
            (and
              (LeftOf2 ?x_8)
              (RightOf11 ?x_8)
            )
          )
          (exists (?x_21 )
            (and
              (AboveOf9 ?x_21)
              (BelowOf2 ?x_21)
            )
          )
          (exists (?x_0 )
            (and
              (BelowOf7 ?x_0)
              (AboveOf13 ?x_0)
            )
          )
          (exists (?x_18 )
            (and
              (AboveOf3 ?x_18)
              (BelowOf1 ?x_18)
            )
          )
          (exists (?x_0 )
            (and
              (AboveOf16 ?x_0)
              (BelowOf8 ?x_0)
            )
          )
          (exists (?x_5 )
            (and
              (LeftOf9 ?x_5)
              (RightOf13 ?x_5)
            )
          )
          (exists (?x_0 )
            (and
              (BelowOf10 ?x_0)
              (AboveOf18 ?x_0)
            )
          )
          (exists (?x_23 )
            (and
              (AboveOf17 ?x_23)
              (BelowOf4 ?x_23)
            )
          )
          (exists (?x_24 )
            (and
              (LeftOf2 ?x_24)
              (RightOf10 ?x_24)
            )
          )
          (exists (?x_11 )
            (and
              (BelowOf9 ?x_11)
              (AboveOf18 ?x_11)
            )
          )
          (exists (?x_27 )
            (and
              (RightOf5 ?x_27)
              (LeftOf2 ?x_27)
            )
          )
          (exists (?x_6 )
            (and
              (BelowOf6 ?x_6)
              (AboveOf15 ?x_6)
            )
          )
          (exists (?x_17 )
            (and
              (AboveOf5 ?x_17)
              (BelowOf2 ?x_17)
            )
          )
          (exists (?x_20 )
            (and
              (RightOf10 ?x_20)
              (LeftOf1 ?x_20)
            )
          )
          (exists (?x_8 )
            (and
              (LeftOf7 ?x_8)
              (RightOf12 ?x_8)
            )
          )
          (exists (?x_3 )
            (and
              (RightOf15 ?x_3)
              (LeftOf2 ?x_3)
            )
          )
          (exists (?x_4 )
            (and
              (RightOf5 ?x_4)
              (LeftOf1 ?x_4)
            )
          )
          (exists (?x_10 )
            (and
              (LeftOf4 ?x_10)
              (RightOf17 ?x_10)
            )
          )
          (exists (?x_34 )
            (and
              (LeftOf8 ?x_34)
              (RightOf11 ?x_34)
            )
          )
          (exists (?x_6 )
            (and
              (AboveOf7 ?x_6)
              (BelowOf4 ?x_6)
            )
          )
          (exists (?x_32 )
            (and
              (LeftOf10 ?x_32)
              (RightOf18 ?x_32)
            )
          )
          (exists (?x_6 )
            (and
              (BelowOf3 ?x_6)
              (AboveOf15 ?x_6)
            )
          )
          (exists (?x_25 )
            (and
              (RightOf17 ?x_25)
              (LeftOf6 ?x_25)
            )
          )
          (exists (?x_30 )
            (and
              (LeftOf3 ?x_30)
              (RightOf15 ?x_30)
            )
          )
          (exists (?x_32 )
            (and
              (RightOf17 ?x_32)
              (LeftOf2 ?x_32)
            )
          )
          (exists (?x_30 )
            (and
              (RightOf17 ?x_30)
              (LeftOf8 ?x_30)
            )
          )
          (exists (?x_8 )
            (and
              (LeftOf3 ?x_8)
              (RightOf14 ?x_8)
            )
          )
          (exists (?x_16 )
            (and
              (AboveOf13 ?x_16)
              (BelowOf9 ?x_16)
            )
          )
          (exists (?x_20 )
            (and
              (RightOf6 ?x_20)
              (LeftOf1 ?x_20)
            )
          )
          (exists (?x_9 )
            (and
              (AboveOf11 ?x_9)
              (BelowOf4 ?x_9)
            )
          )
          (exists (?x_19 )
            (and
              (RightOf11 ?x_19)
              (LeftOf6 ?x_19)
            )
          )
          (exists (?x_30 )
            (and
              (RightOf18 ?x_30)
              (LeftOf11 ?x_30)
            )
          )
          (exists (?x_6 )
            (and
              (AboveOf10 ?x_6)
              (BelowOf2 ?x_6)
            )
          )
          (exists (?x_16 )
            (and
              (AboveOf16 ?x_16)
              (BelowOf2 ?x_16)
            )
          )
          (exists (?x_32 )
            (and
              (RightOf18 ?x_32)
              (LeftOf1 ?x_32)
            )
          )
          (exists (?x_10 )
            (and
              (LeftOf14 ?x_10)
              (RightOf17 ?x_10)
            )
          )
          (exists (?x_8 )
            (and
              (RightOf16 ?x_8)
              (LeftOf4 ?x_8)
            )
          )
          (exists (?x_3 )
            (and
              (LeftOf2 ?x_3)
              (RightOf12 ?x_3)
            )
          )
          (exists (?x_34 )
            (and
              (RightOf16 ?x_34)
              (LeftOf6 ?x_34)
            )
          )
          (exists (?x_5 )
            (and
              (LeftOf6 ?x_5)
              (RightOf11 ?x_5)
            )
          )
          (exists (?x_1 )
            (and
              (BelowOf7 ?x_1)
              (AboveOf17 ?x_1)
            )
          )
          (exists (?x_0 )
            (and
              (BelowOf9 ?x_0)
              (AboveOf10 ?x_0)
            )
          )
          (exists (?x_9 )
            (and
              (AboveOf16 ?x_9)
              (BelowOf1 ?x_9)
            )
          )
          (exists (?x_33 )
            (and
              (RightOf17 ?x_33)
              (LeftOf4 ?x_33)
            )
          )
          (exists (?x_5 )
            (and
              (RightOf15 ?x_5)
              (LeftOf1 ?x_5)
            )
          )
          (exists (?x_2 )
            (and
              (AboveOf15 ?x_2)
              (BelowOf3 ?x_2)
            )
          )
          (exists (?x_24 )
            (and
              (RightOf16 ?x_24)
              (LeftOf4 ?x_24)
            )
          )
          (exists (?x_26 )
            (and
              (BelowOf5 ?x_26)
              (AboveOf12 ?x_26)
            )
          )
          (exists (?x_19 )
            (and
              (RightOf17 ?x_19)
              (LeftOf1 ?x_19)
            )
          )
          (exists (?x_3 )
            (and
              (RightOf16 ?x_3)
              (LeftOf3 ?x_3)
            )
          )
          (exists (?x_35 )
            (and
              (BelowOf3 ?x_35)
              (AboveOf10 ?x_35)
            )
          )
          (exists (?x_33 )
            (and
              (LeftOf10 ?x_33)
              (RightOf16 ?x_33)
            )
          )
          (exists (?x_21 )
            (and
              (AboveOf12 ?x_21)
              (BelowOf3 ?x_21)
            )
          )
          (exists (?x_3 )
            (and
              (RightOf16 ?x_3)
              (LeftOf2 ?x_3)
            )
          )
          (exists (?x_23 )
            (and
              (BelowOf3 ?x_23)
              (AboveOf9 ?x_23)
            )
          )
          (exists (?x_24 )
            (and
              (RightOf12 ?x_24)
              (LeftOf5 ?x_24)
            )
          )
          (exists (?x_5 )
            (and
              (LeftOf4 ?x_5)
              (RightOf14 ?x_5)
            )
          )
          (exists (?x_0 )
            (and
              (AboveOf14 ?x_0)
              (BelowOf6 ?x_0)
            )
          )
          (exists (?x_22 )
            (and
              (LeftOf4 ?x_22)
              (RightOf17 ?x_22)
            )
          )
          (exists (?x_35 )
            (and
              (AboveOf13 ?x_35)
              (BelowOf7 ?x_35)
            )
          )
          (exists (?x_31 )
            (and
              (AboveOf14 ?x_31)
              (BelowOf11 ?x_31)
            )
          )
          (exists (?x_27 )
            (and
              (RightOf8 ?x_27)
              (LeftOf2 ?x_27)
            )
          )
          (exists (?x_13 )
            (and
              (LeftOf1 ?x_13)
              (RightOf6 ?x_13)
            )
          )
          (exists (?x_35 )
            (and
              (AboveOf13 ?x_35)
              (BelowOf9 ?x_35)
            )
          )
          (exists (?x_24 )
            (and
              (LeftOf5 ?x_24)
              (RightOf13 ?x_24)
            )
          )
          (exists (?x_8 )
            (and
              (LeftOf4 ?x_8)
              (RightOf14 ?x_8)
            )
          )
          (exists (?x_23 )
            (and
              (BelowOf4 ?x_23)
              (AboveOf9 ?x_23)
            )
          )
          (exists (?x_7 )
            (and
              (LeftOf14 ?x_7)
              (RightOf15 ?x_7)
            )
          )
          (exists (?x_8 )
            (and
              (LeftOf2 ?x_8)
              (RightOf18 ?x_8)
            )
          )
          (exists (?x_19 )
            (and
              (LeftOf7 ?x_19)
              (RightOf14 ?x_19)
            )
          )
          (exists (?x_11 )
            (and
              (AboveOf17 ?x_11)
              (BelowOf17 ?x_11)
            )
          )
          (exists (?x_22 )
            (and
              (LeftOf4 ?x_22)
              (RightOf14 ?x_22)
            )
          )
          (exists (?x_13 )
            (and
              (LeftOf2 ?x_13)
              (RightOf9 ?x_13)
            )
          )
          (exists (?x_2 )
            (and
              (AboveOf17 ?x_2)
              (BelowOf1 ?x_2)
            )
          )
          (exists (?x_0 )
            (and
              (AboveOf11 ?x_0)
              (BelowOf2 ?x_0)
            )
          )
          (exists (?x_26 )
            (and
              (BelowOf10 ?x_26)
              (AboveOf17 ?x_26)
            )
          )
          (exists (?x_31 )
            (and
              (BelowOf4 ?x_31)
              (AboveOf17 ?x_31)
            )
          )
          (exists (?x_9 )
            (and
              (BelowOf7 ?x_9)
              (AboveOf14 ?x_9)
            )
          )
          (exists (?x_16 )
            (and
              (BelowOf10 ?x_16)
              (AboveOf18 ?x_16)
            )
          )
          (exists (?x_11 )
            (and
              (AboveOf17 ?x_11)
              (BelowOf6 ?x_11)
            )
          )
          (exists (?x_24 )
            (and
              (LeftOf3 ?x_24)
              (RightOf17 ?x_24)
            )
          )
          (exists (?x_19 )
            (and
              (LeftOf11 ?x_19)
              (RightOf16 ?x_19)
            )
          )
          (exists (?x_12 )
            (and
              (BelowOf11 ?x_12)
              (AboveOf17 ?x_12)
            )
          )
          (exists (?x_20 )
            (and
              (LeftOf1 ?x_20)
              (RightOf11 ?x_20)
            )
          )
          (exists (?x_10 )
            (and
              (RightOf15 ?x_10)
              (LeftOf9 ?x_10)
            )
          )
          (exists (?x_8 )
            (and
              (RightOf10 ?x_8)
              (LeftOf8 ?x_8)
            )
          )
          (exists (?x_13 )
            (and
              (LeftOf2 ?x_13)
              (RightOf13 ?x_13)
            )
          )
          (exists (?x_19 )
            (and
              (LeftOf6 ?x_19)
              (RightOf16 ?x_19)
            )
          )
          (exists (?x_29 )
            (and
              (AboveOf3 ?x_29)
              (BelowOf2 ?x_29)
            )
          )
          (exists (?x_26 )
            (and
              (BelowOf3 ?x_26)
              (AboveOf17 ?x_26)
            )
          )
          (exists (?x_15 )
            (and
              (BelowOf4 ?x_15)
              (AboveOf14 ?x_15)
            )
          )
          (exists (?x_35 )
            (and
              (BelowOf6 ?x_35)
              (AboveOf15 ?x_35)
            )
          )
          (exists (?x_25 )
            (and
              (LeftOf6 ?x_25)
              (RightOf6 ?x_25)
            )
          )
          (exists (?x_2 )
            (and
              (AboveOf14 ?x_2)
              (BelowOf14 ?x_2)
            )
          )
          (exists (?x_20 )
            (and
              (RightOf8 ?x_20)
              (LeftOf1 ?x_20)
            )
          )
          (exists (?x_8 )
            (and
              (RightOf9 ?x_8)
              (LeftOf3 ?x_8)
            )
          )
          (exists (?x_9 )
            (and
              (BelowOf1 ?x_9)
              (AboveOf18 ?x_9)
            )
          )
          (exists (?x_31 )
            (and
              (AboveOf17 ?x_31)
              (BelowOf11 ?x_31)
            )
          )
          (exists (?x_2 )
            (and
              (BelowOf14 ?x_2)
              (AboveOf15 ?x_2)
            )
          )
          (exists (?x_30 )
            (and
              (RightOf17 ?x_30)
              (LeftOf5 ?x_30)
            )
          )
          (exists (?x_24 )
            (and
              (LeftOf6 ?x_24)
              (RightOf14 ?x_24)
            )
          )
          (exists (?x_22 )
            (and
              (RightOf18 ?x_22)
              (LeftOf8 ?x_22)
            )
          )
          (exists (?x_10 )
            (and
              (LeftOf1 ?x_10)
              (RightOf14 ?x_10)
            )
          )
          (exists (?x_19 )
            (and
              (LeftOf9 ?x_19)
              (RightOf16 ?x_19)
            )
          )
          (exists (?x_22 )
            (and
              (RightOf18 ?x_22)
              (LeftOf12 ?x_22)
            )
          )
          (exists (?x_1 )
            (and
              (AboveOf17 ?x_1)
              (BelowOf15 ?x_1)
            )
          )
          (exists (?x_7 )
            (and
              (LeftOf14 ?x_7)
              (RightOf16 ?x_7)
            )
          )
          (exists (?x_15 )
            (and
              (BelowOf3 ?x_15)
              (AboveOf13 ?x_15)
            )
          )
          (exists (?x_10 )
            (and
              (LeftOf1 ?x_10)
              (RightOf15 ?x_10)
            )
          )
          (exists (?x_7 )
            (and
              (LeftOf10 ?x_7)
              (RightOf15 ?x_7)
            )
          )
          (exists (?x_1 )
            (and
              (AboveOf17 ?x_1)
              (BelowOf5 ?x_1)
            )
          )
          (exists (?x_3 )
            (and
              (LeftOf1 ?x_3)
              (RightOf3 ?x_3)
            )
          )
          (exists (?x_9 )
            (and
              (BelowOf6 ?x_9)
              (AboveOf10 ?x_9)
            )
          )
          (exists (?x_35 )
            (and
              (AboveOf14 ?x_35)
              (BelowOf9 ?x_35)
            )
          )
          (exists (?x_22 )
            (and
              (LeftOf1 ?x_22)
              (RightOf15 ?x_22)
            )
          )
          (exists (?x_3 )
            (and
              (LeftOf1 ?x_3)
              (RightOf6 ?x_3)
            )
          )
          (exists (?x_7 )
            (and
              (LeftOf1 ?x_7)
              (RightOf16 ?x_7)
            )
          )
          (exists (?x_35 )
            (and
              (BelowOf5 ?x_35)
              (AboveOf13 ?x_35)
            )
          )
          (exists (?x_3 )
            (and
              (RightOf10 ?x_3)
              (LeftOf2 ?x_3)
            )
          )
          (exists (?x_34 )
            (and
              (RightOf13 ?x_34)
              (LeftOf8 ?x_34)
            )
          )
          (exists (?x_11 )
            (and
              (AboveOf17 ?x_11)
              (BelowOf7 ?x_11)
            )
          )
          (exists (?x_31 )
            (and
              (AboveOf13 ?x_31)
              (BelowOf13 ?x_31)
            )
          )
          (exists (?x_0 )
            (and
              (BelowOf8 ?x_0)
              (AboveOf18 ?x_0)
            )
          )
          (exists (?x_30 )
            (and
              (RightOf13 ?x_30)
              (LeftOf9 ?x_30)
            )
          )
          (exists (?x_0 )
            (and
              (AboveOf15 ?x_0)
              (BelowOf6 ?x_0)
            )
          )
          (exists (?x_34 )
            (and
              (RightOf17 ?x_34)
              (LeftOf5 ?x_34)
            )
          )
          (exists (?x_22 )
            (and
              (LeftOf5 ?x_22)
              (RightOf12 ?x_22)
            )
          )
          (exists (?x_30 )
            (and
              (RightOf15 ?x_30)
              (LeftOf1 ?x_30)
            )
          )
          (exists (?x_7 )
            (and
              (RightOf17 ?x_7)
              (LeftOf10 ?x_7)
            )
          )
          (exists (?x_9 )
            (and
              (AboveOf11 ?x_9)
              (BelowOf3 ?x_9)
            )
          )
          (exists (?x_23 )
            (and
              (AboveOf14 ?x_23)
              (BelowOf4 ?x_23)
            )
          )
          (exists (?x_1 )
            (and
              (AboveOf18 ?x_1)
              (BelowOf1 ?x_1)
            )
          )
          (exists (?x_0 )
            (and
              (AboveOf17 ?x_0)
              (BelowOf5 ?x_0)
            )
          )
          (exists (?x_2 )
            (and
              (BelowOf9 ?x_2)
              (AboveOf16 ?x_2)
            )
          )
          (exists (?x_34 )
            (and
              (LeftOf7 ?x_34)
              (RightOf15 ?x_34)
            )
          )
          (exists (?x_15 )
            (and
              (BelowOf3 ?x_15)
              (AboveOf12 ?x_15)
            )
          )
          (exists (?x_29 )
            (and
              (AboveOf4 ?x_29)
              (BelowOf1 ?x_29)
            )
          )
          (exists (?x_35 )
            (and
              (BelowOf2 ?x_35)
              (AboveOf18 ?x_35)
            )
          )
          (exists (?x_17 )
            (and
              (BelowOf4 ?x_17)
              (AboveOf12 ?x_17)
            )
          )
          (exists (?x_30 )
            (and
              (LeftOf8 ?x_30)
              (RightOf15 ?x_30)
            )
          )
          (exists (?x_26 )
            (and
              (BelowOf2 ?x_26)
              (AboveOf14 ?x_26)
            )
          )
          (exists (?x_2 )
            (and
              (AboveOf17 ?x_2)
              (BelowOf13 ?x_2)
            )
          )
          (exists (?x_5 )
            (and
              (LeftOf4 ?x_5)
              (RightOf12 ?x_5)
            )
          )
          (exists (?x_12 )
            (and
              (BelowOf3 ?x_12)
              (AboveOf16 ?x_12)
            )
          )
          (exists (?x_15 )
            (and
              (AboveOf10 ?x_15)
              (BelowOf5 ?x_15)
            )
          )
          (exists (?x_34 )
            (and
              (LeftOf4 ?x_34)
              (RightOf14 ?x_34)
            )
          )
          (exists (?x_32 )
            (and
              (RightOf17 ?x_32)
              (LeftOf11 ?x_32)
            )
          )
          (exists (?x_2 )
            (and
              (BelowOf14 ?x_2)
              (AboveOf18 ?x_2)
            )
          )
          (exists (?x_9 )
            (and
              (AboveOf16 ?x_9)
              (BelowOf2 ?x_9)
            )
          )
          (exists (?x_31 )
            (and
              (BelowOf12 ?x_31)
              (AboveOf14 ?x_31)
            )
          )
          (exists (?x_16 )
            (and
              (AboveOf15 ?x_16)
              (BelowOf10 ?x_16)
            )
          )
          (exists (?x_27 )
            (and
              (RightOf11 ?x_27)
              (LeftOf2 ?x_27)
            )
          )
          (exists (?x_0 )
            (and
              (AboveOf17 ?x_0)
              (BelowOf2 ?x_0)
            )
          )
          (exists (?x_2 )
            (and
              (AboveOf15 ?x_2)
              (BelowOf1 ?x_2)
            )
          )
          (exists (?x_19 )
            (and
              (RightOf17 ?x_19)
              (LeftOf9 ?x_19)
            )
          )
          (exists (?x_27 )
            (and
              (LeftOf2 ?x_27)
              (RightOf6 ?x_27)
            )
          )
          (exists (?x_26 )
            (and
              (BelowOf9 ?x_26)
              (AboveOf13 ?x_26)
            )
          )
          (exists (?x_30 )
            (and
              (LeftOf7 ?x_30)
              (RightOf13 ?x_30)
            )
          )
          (exists (?x_35 )
            (and
              (BelowOf6 ?x_35)
              (AboveOf13 ?x_35)
            )
          )
          (exists (?x_23 )
            (and
              (BelowOf3 ?x_23)
              (AboveOf7 ?x_23)
            )
          )
          (exists (?x_26 )
            (and
              (BelowOf2 ?x_26)
              (AboveOf16 ?x_26)
            )
          )
          (exists (?x_24 )
            (and
              (LeftOf6 ?x_24)
              (RightOf8 ?x_24)
            )
          )
          (exists (?x_25 )
            (and
              (LeftOf3 ?x_25)
              (RightOf12 ?x_25)
            )
          )
          (exists (?x_17 )
            (and
              (AboveOf16 ?x_17)
              (BelowOf4 ?x_17)
            )
          )
          (exists (?x_22 )
            (and
              (RightOf14 ?x_22)
              (LeftOf5 ?x_22)
            )
          )
          (exists (?x_34 )
            (and
              (RightOf15 ?x_34)
              (LeftOf6 ?x_34)
            )
          )
          (exists (?x_25 )
            (and
              (RightOf15 ?x_25)
              (LeftOf1 ?x_25)
            )
          )
          (exists (?x_3 )
            (and
              (LeftOf1 ?x_3)
              (RightOf10 ?x_3)
            )
          )
          (exists (?x_23 )
            (and
              (BelowOf5 ?x_23)
              (AboveOf9 ?x_23)
            )
          )
          (exists (?x_3 )
            (and
              (RightOf14 ?x_3)
              (LeftOf2 ?x_3)
            )
          )
          (exists (?x_5 )
            (and
              (LeftOf1 ?x_5)
              (RightOf17 ?x_5)
            )
          )
          (exists (?x_22 )
            (and
              (RightOf16 ?x_22)
              (LeftOf8 ?x_22)
            )
          )
          (exists (?x_30 )
            (and
              (RightOf14 ?x_30)
              (LeftOf11 ?x_30)
            )
          )
          (exists (?x_24 )
            (and
              (RightOf17 ?x_24)
              (LeftOf7 ?x_24)
            )
          )
          (exists (?x_34 )
            (and
              (RightOf12 ?x_34)
              (LeftOf6 ?x_34)
            )
          )
          (exists (?x_4 )
            (and
              (LeftOf2 ?x_4)
              (RightOf8 ?x_4)
            )
          )
          (exists (?x_7 )
            (and
              (RightOf18 ?x_7)
              (LeftOf15 ?x_7)
            )
          )
          (exists (?x_34 )
            (and
              (LeftOf9 ?x_34)
              (RightOf12 ?x_34)
            )
          )
          (exists (?x_9 )
            (and
              (AboveOf12 ?x_9)
              (BelowOf1 ?x_9)
            )
          )
          (exists (?x_15 )
            (and
              (AboveOf15 ?x_15)
              (BelowOf1 ?x_15)
            )
          )
          (exists (?x_35 )
            (and
              (BelowOf8 ?x_35)
              (AboveOf17 ?x_35)
            )
          )
          (exists (?x_17 )
            (and
              (AboveOf14 ?x_17)
              (BelowOf4 ?x_17)
            )
          )
          (exists (?x_24 )
            (and
              (RightOf14 ?x_24)
              (LeftOf5 ?x_24)
            )
          )
          (exists (?x_29 )
            (and
              (BelowOf2 ?x_29)
              (AboveOf2 ?x_29)
            )
          )
          (exists (?x_30 )
            (and
              (LeftOf13 ?x_30)
              (RightOf16 ?x_30)
            )
          )
          (exists (?x_0 )
            (and
              (BelowOf3 ?x_0)
              (AboveOf13 ?x_0)
            )
          )
          (exists (?x_17 )
            (and
              (BelowOf2 ?x_17)
              (AboveOf9 ?x_17)
            )
          )
          (exists (?x_0 )
            (and
              (BelowOf3 ?x_0)
              (AboveOf16 ?x_0)
            )
          )
          (exists (?x_16 )
            (and
              (AboveOf13 ?x_16)
              (BelowOf2 ?x_16)
            )
          )
          (exists (?x_30 )
            (and
              (RightOf17 ?x_30)
              (LeftOf3 ?x_30)
            )
          )
          (exists (?x_32 )
            (and
              (RightOf17 ?x_32)
              (LeftOf3 ?x_32)
            )
          )
          (exists (?x_19 )
            (and
              (RightOf18 ?x_19)
              (LeftOf2 ?x_19)
            )
          )
          (exists (?x_5 )
            (and
              (LeftOf7 ?x_5)
              (RightOf13 ?x_5)
            )
          )
          (exists (?x_5 )
            (and
              (RightOf12 ?x_5)
              (LeftOf10 ?x_5)
            )
          )
          (exists (?x_0 )
            (and
              (BelowOf8 ?x_0)
              (AboveOf10 ?x_0)
            )
          )
          (exists (?x_2 )
            (and
              (BelowOf2 ?x_2)
              (AboveOf18 ?x_2)
            )
          )
          (exists (?x_30 )
            (and
              (RightOf14 ?x_30)
              (LeftOf12 ?x_30)
            )
          )
          (exists (?x_9 )
            (and
              (AboveOf17 ?x_9)
              (BelowOf6 ?x_9)
            )
          )
          (exists (?x_16 )
            (and
              (BelowOf2 ?x_16)
              (AboveOf15 ?x_16)
            )
          )
          (exists (?x_16 )
            (and
              (BelowOf5 ?x_16)
              (AboveOf14 ?x_16)
            )
          )
          (exists (?x_11 )
            (and
              (BelowOf16 ?x_11)
              (AboveOf18 ?x_11)
            )
          )
          (exists (?x_5 )
            (and
              (RightOf14 ?x_5)
              (LeftOf10 ?x_5)
            )
          )
          (exists (?x_2 )
            (and
              (AboveOf18 ?x_2)
              (BelowOf7 ?x_2)
            )
          )
          (exists (?x_13 )
            (and
              (LeftOf1 ?x_13)
              (RightOf16 ?x_13)
            )
          )
          (exists (?x_11 )
            (and
              (BelowOf10 ?x_11)
              (AboveOf18 ?x_11)
            )
          )
          (exists (?x_15 )
            (and
              (BelowOf3 ?x_15)
              (AboveOf15 ?x_15)
            )
          )
          (exists (?x_24 )
            (and
              (RightOf14 ?x_24)
              (LeftOf1 ?x_24)
            )
          )
          (exists (?x_13 )
            (and
              (LeftOf1 ?x_13)
              (RightOf17 ?x_13)
            )
          )
          (exists (?x_15 )
            (and
              (AboveOf9 ?x_15)
              (BelowOf5 ?x_15)
            )
          )
          (exists (?x_3 )
            (and
              (RightOf8 ?x_3)
              (LeftOf2 ?x_3)
            )
          )
          (exists (?x_27 )
            (and
              (RightOf11 ?x_27)
              (LeftOf1 ?x_27)
            )
          )
          (exists (?x_30 )
            (and
              (RightOf14 ?x_30)
              (LeftOf1 ?x_30)
            )
          )
          (exists (?x_22 )
            (and
              (RightOf16 ?x_22)
              (LeftOf10 ?x_22)
            )
          )
          (exists (?x_15 )
            (and
              (BelowOf7 ?x_15)
              (AboveOf8 ?x_15)
            )
          )
          (exists (?x_22 )
            (and
              (LeftOf11 ?x_22)
              (RightOf16 ?x_22)
            )
          )
          (exists (?x_30 )
            (and
              (RightOf14 ?x_30)
              (LeftOf7 ?x_30)
            )
          )
          (exists (?x_10 )
            (and
              (RightOf16 ?x_10)
              (LeftOf9 ?x_10)
            )
          )
          (exists (?x_16 )
            (and
              (AboveOf13 ?x_16)
              (BelowOf1 ?x_16)
            )
          )
          (exists (?x_35 )
            (and
              (AboveOf18 ?x_35)
              (BelowOf7 ?x_35)
            )
          )
          (exists (?x_10 )
            (and
              (LeftOf1 ?x_10)
              (RightOf17 ?x_10)
            )
          )
          (exists (?x_16 )
            (and
              (BelowOf11 ?x_16)
              (AboveOf18 ?x_16)
            )
          )
          (exists (?x_25 )
            (and
              (LeftOf2 ?x_25)
              (RightOf18 ?x_25)
            )
          )
          (exists (?x_24 )
            (and
              (LeftOf5 ?x_24)
              (RightOf8 ?x_24)
            )
          )
          (exists (?x_2 )
            (and
              (AboveOf18 ?x_2)
              (BelowOf11 ?x_2)
            )
          )
          (exists (?x_8 )
            (and
              (RightOf17 ?x_8)
              (LeftOf4 ?x_8)
            )
          )
          (exists (?x_14 )
            (and
              (AboveOf18 ?x_14)
              (BelowOf14 ?x_14)
            )
          )
          (exists (?x_35 )
            (and
              (AboveOf17 ?x_35)
              (BelowOf7 ?x_35)
            )
          )
          (exists (?x_16 )
            (and
              (BelowOf2 ?x_16)
              (AboveOf17 ?x_16)
            )
          )
          (exists (?x_4 )
            (and
              (RightOf9 ?x_4)
              (LeftOf1 ?x_4)
            )
          )
          (exists (?x_33 )
            (and
              (LeftOf13 ?x_33)
              (RightOf17 ?x_33)
            )
          )
          (exists (?x_13 )
            (and
              (LeftOf1 ?x_13)
              (RightOf15 ?x_13)
            )
          )
          (exists (?x_17 )
            (and
              (BelowOf4 ?x_17)
              (AboveOf4 ?x_17)
            )
          )
          (exists (?x_15 )
            (and
              (AboveOf9 ?x_15)
              (BelowOf7 ?x_15)
            )
          )
          (exists (?x_10 )
            (and
              (RightOf17 ?x_10)
              (LeftOf10 ?x_10)
            )
          )
          (exists (?x_34 )
            (and
              (RightOf10 ?x_34)
              (LeftOf8 ?x_34)
            )
          )
          (exists (?x_16 )
            (and
              (BelowOf3 ?x_16)
              (AboveOf18 ?x_16)
            )
          )
          (exists (?x_24 )
            (and
              (RightOf11 ?x_24)
              (LeftOf7 ?x_24)
            )
          )
          (exists (?x_16 )
            (and
              (BelowOf8 ?x_16)
              (AboveOf17 ?x_16)
            )
          )
          (exists (?x_15 )
            (and
              (AboveOf10 ?x_15)
              (BelowOf4 ?x_15)
            )
          )
          (exists (?x_27 )
            (and
              (LeftOf4 ?x_27)
              (RightOf6 ?x_27)
            )
          )
          (exists (?x_35 )
            (and
              (AboveOf11 ?x_35)
              (BelowOf7 ?x_35)
            )
          )
          (exists (?x_9 )
            (and
              (AboveOf11 ?x_9)
              (BelowOf2 ?x_9)
            )
          )
          (exists (?x_10 )
            (and
              (LeftOf1 ?x_10)
              (RightOf18 ?x_10)
            )
          )
          (exists (?x_2 )
            (and
              (BelowOf3 ?x_2)
              (AboveOf18 ?x_2)
            )
          )
          (exists (?x_21 )
            (and
              (AboveOf6 ?x_21)
              (BelowOf3 ?x_21)
            )
          )
          (exists (?x_0 )
            (and
              (AboveOf16 ?x_0)
              (BelowOf9 ?x_0)
            )
          )
          (exists (?x_2 )
            (and
              (AboveOf17 ?x_2)
              (BelowOf7 ?x_2)
            )
          )
          (exists (?x_25 )
            (and
              (LeftOf5 ?x_25)
              (RightOf13 ?x_25)
            )
          )
          (exists (?x_19 )
            (and
              (RightOf12 ?x_19)
              (LeftOf9 ?x_19)
            )
          )
          (exists (?x_34 )
            (and
              (LeftOf2 ?x_34)
              (RightOf14 ?x_34)
            )
          )
          (exists (?x_14 )
            (and
              (AboveOf18 ?x_14)
              (BelowOf1 ?x_14)
            )
          )
          (exists (?x_33 )
            (and
              (LeftOf16 ?x_33)
              (RightOf18 ?x_33)
            )
          )
          (exists (?x_8 )
            (and
              (LeftOf5 ?x_8)
              (RightOf13 ?x_8)
            )
          )
          (exists (?x_32 )
            (and
              (RightOf18 ?x_32)
              (LeftOf7 ?x_32)
            )
          )
          (exists (?x_13 )
            (and
              (RightOf12 ?x_13)
              (LeftOf3 ?x_13)
            )
          )
          (exists (?x_34 )
            (and
              (LeftOf4 ?x_34)
              (RightOf10 ?x_34)
            )
          )
          (exists (?x_8 )
            (and
              (RightOf11 ?x_8)
              (LeftOf6 ?x_8)
            )
          )
          (exists (?x_26 )
            (and
              (AboveOf16 ?x_26)
              (BelowOf5 ?x_26)
            )
          )
          (exists (?x_23 )
            (and
              (AboveOf11 ?x_23)
              (BelowOf5 ?x_23)
            )
          )
          (exists (?x_22 )
            (and
              (LeftOf9 ?x_22)
              (RightOf14 ?x_22)
            )
          )
          (exists (?x_35 )
            (and
              (BelowOf6 ?x_35)
              (AboveOf16 ?x_35)
            )
          )
          (exists (?x_6 )
            (and
              (AboveOf13 ?x_6)
              (BelowOf2 ?x_6)
            )
          )
          (exists (?x_9 )
            (and
              (AboveOf18 ?x_9)
              (BelowOf2 ?x_9)
            )
          )
          (exists (?x_8 )
            (and
              (RightOf12 ?x_8)
              (LeftOf3 ?x_8)
            )
          )
          (exists (?x_21 )
            (and
              (AboveOf9 ?x_21)
              (BelowOf1 ?x_21)
            )
          )
          (exists (?x_12 )
            (and
              (AboveOf18 ?x_12)
              (BelowOf1 ?x_12)
            )
          )
          (exists (?x_10 )
            (and
              (RightOf15 ?x_10)
              (LeftOf13 ?x_10)
            )
          )
          (exists (?x_2 )
            (and
              (BelowOf6 ?x_2)
              (AboveOf16 ?x_2)
            )
          )
          (exists (?x_10 )
            (and
              (RightOf16 ?x_10)
              (LeftOf4 ?x_10)
            )
          )
          (exists (?x_24 )
            (and
              (LeftOf7 ?x_24)
              (RightOf10 ?x_24)
            )
          )
          (exists (?x_6 )
            (and
              (BelowOf1 ?x_6)
              (AboveOf9 ?x_6)
            )
          )
          (exists (?x_19 )
            (and
              (LeftOf5 ?x_19)
              (RightOf11 ?x_19)
            )
          )
          (exists (?x_34 )
            (and
              (LeftOf3 ?x_34)
              (RightOf11 ?x_34)
            )
          )
          (exists (?x_21 )
            (and
              (AboveOf6 ?x_21)
              (BelowOf1 ?x_21)
            )
          )
          (exists (?x_34 )
            (and
              (LeftOf9 ?x_34)
              (RightOf9 ?x_34)
            )
          )
          (exists (?x_34 )
            (and
              (LeftOf2 ?x_34)
              (RightOf12 ?x_34)
            )
          )
          (exists (?x_6 )
            (and
              (BelowOf1 ?x_6)
              (AboveOf10 ?x_6)
            )
          )
          (exists (?x_24 )
            (and
              (LeftOf2 ?x_24)
              (RightOf13 ?x_24)
            )
          )
          (exists (?x_27 )
            (and
              (LeftOf5 ?x_27)
              (RightOf12 ?x_27)
            )
          )
          (exists (?x_11 )
            (and
              (BelowOf8 ?x_11)
              (AboveOf18 ?x_11)
            )
          )
          (exists (?x_6 )
            (and
              (AboveOf9 ?x_6)
              (BelowOf5 ?x_6)
            )
          )
          (exists (?x_21 )
            (and
              (AboveOf12 ?x_21)
              (BelowOf1 ?x_21)
            )
          )
          (exists (?x_29 )
            (and
              (AboveOf15 ?x_29)
              (BelowOf2 ?x_29)
            )
          )
          (exists (?x_34 )
            (and
              (LeftOf2 ?x_34)
              (RightOf16 ?x_34)
            )
          )
          (exists (?x_1 )
            (and
              (AboveOf18 ?x_1)
              (BelowOf10 ?x_1)
            )
          )
          (exists (?x_19 )
            (and
              (LeftOf5 ?x_19)
              (RightOf14 ?x_19)
            )
          )
          (exists (?x_7 )
            (and
              (LeftOf11 ?x_7)
              (RightOf16 ?x_7)
            )
          )
          (exists (?x_5 )
            (and
              (LeftOf9 ?x_5)
              (RightOf10 ?x_5)
            )
          )
          (exists (?x_12 )
            (and
              (AboveOf17 ?x_12)
              (BelowOf13 ?x_12)
            )
          )
          (exists (?x_0 )
            (and
              (BelowOf3 ?x_0)
              (AboveOf11 ?x_0)
            )
          )
          (exists (?x_35 )
            (and
              (AboveOf13 ?x_35)
              (BelowOf2 ?x_35)
            )
          )
          (exists (?x_4 )
            (and
              (RightOf12 ?x_4)
              (LeftOf1 ?x_4)
            )
          )
          (exists (?x_23 )
            (and
              (BelowOf1 ?x_23)
              (AboveOf9 ?x_23)
            )
          )
          (exists (?x_8 )
            (and
              (LeftOf7 ?x_8)
              (RightOf18 ?x_8)
            )
          )
          (exists (?x_15 )
            (and
              (BelowOf4 ?x_15)
              (AboveOf12 ?x_15)
            )
          )
          (exists (?x_26 )
            (and
              (AboveOf13 ?x_26)
              (BelowOf12 ?x_26)
            )
          )
          (exists (?x_16 )
            (and
              (BelowOf8 ?x_16)
              (AboveOf18 ?x_16)
            )
          )
          (exists (?x_27 )
            (and
              (RightOf14 ?x_27)
              (LeftOf3 ?x_27)
            )
          )
          (exists (?x_9 )
            (and
              (BelowOf4 ?x_9)
              (AboveOf13 ?x_9)
            )
          )
          (exists (?x_35 )
            (and
              (BelowOf4 ?x_35)
              (AboveOf10 ?x_35)
            )
          )
          (exists (?x_9 )
            (and
              (AboveOf17 ?x_9)
              (BelowOf2 ?x_9)
            )
          )
          (exists (?x_7 )
            (and
              (RightOf17 ?x_7)
              (LeftOf14 ?x_7)
            )
          )
          (exists (?x_20 )
            (and
              (RightOf16 ?x_20)
              (LeftOf1 ?x_20)
            )
          )
          (exists (?x_26 )
            (and
              (AboveOf15 ?x_26)
              (BelowOf12 ?x_26)
            )
          )
          (exists (?x_16 )
            (and
              (AboveOf13 ?x_16)
              (BelowOf8 ?x_16)
            )
          )
          (exists (?x_0 )
            (and
              (AboveOf12 ?x_0)
              (BelowOf7 ?x_0)
            )
          )
          (exists (?x_22 )
            (and
              (LeftOf7 ?x_22)
              (RightOf17 ?x_22)
            )
          )
          (exists (?x_10 )
            (and
              (LeftOf9 ?x_10)
              (RightOf17 ?x_10)
            )
          )
          (exists (?x_25 )
            (and
              (LeftOf3 ?x_25)
              (RightOf7 ?x_25)
            )
          )
          (exists (?x_23 )
            (and
              (AboveOf12 ?x_23)
              (BelowOf5 ?x_23)
            )
          )
          (exists (?x_25 )
            (and
              (LeftOf5 ?x_25)
              (RightOf15 ?x_25)
            )
          )
          (exists (?x_9 )
            (and
              (AboveOf7 ?x_9)
              (BelowOf7 ?x_9)
            )
          )
          (exists (?x_16 )
            (and
              (AboveOf11 ?x_16)
              (BelowOf11 ?x_16)
            )
          )
          (exists (?x_16 )
            (and
              (AboveOf11 ?x_16)
              (BelowOf2 ?x_16)
            )
          )
          (exists (?x_35 )
            (and
              (BelowOf3 ?x_35)
              (AboveOf11 ?x_35)
            )
          )
          (exists (?x_19 )
            (and
              (RightOf17 ?x_19)
              (LeftOf2 ?x_19)
            )
          )
          (exists (?x_29 )
            (and
              (AboveOf11 ?x_29)
              (BelowOf2 ?x_29)
            )
          )
          (exists (?x_33 )
            (and
              (LeftOf10 ?x_33)
              (RightOf17 ?x_33)
            )
          )
          (exists (?x_26 )
            (and
              (BelowOf4 ?x_26)
              (AboveOf16 ?x_26)
            )
          )
          (exists (?x_21 )
            (and
              (AboveOf4 ?x_21)
              (BelowOf2 ?x_21)
            )
          )
          (exists (?x_31 )
            (and
              (BelowOf10 ?x_31)
              (AboveOf15 ?x_31)
            )
          )
          (exists (?x_30 )
            (and
              (LeftOf8 ?x_30)
              (RightOf16 ?x_30)
            )
          )
          (exists (?x_35 )
            (and
              (AboveOf14 ?x_35)
              (BelowOf3 ?x_35)
            )
          )
          (exists (?x_27 )
            (and
              (LeftOf3 ?x_27)
              (RightOf6 ?x_27)
            )
          )
          (exists (?x_1 )
            (and
              (AboveOf18 ?x_1)
              (BelowOf15 ?x_1)
            )
          )
          (exists (?x_25 )
            (and
              (RightOf12 ?x_25)
              (LeftOf2 ?x_25)
            )
          )
          (exists (?x_15 )
            (and
              (AboveOf15 ?x_15)
              (BelowOf4 ?x_15)
            )
          )
          (exists (?x_12 )
            (and
              (AboveOf17 ?x_12)
              (BelowOf7 ?x_12)
            )
          )
          (exists (?x_22 )
            (and
              (RightOf18 ?x_22)
              (LeftOf11 ?x_22)
            )
          )
          (exists (?x_6 )
            (and
              (AboveOf11 ?x_6)
              (BelowOf5 ?x_6)
            )
          )
          (exists (?x_15 )
            (and
              (AboveOf15 ?x_15)
              (BelowOf8 ?x_15)
            )
          )
          (exists (?x_33 )
            (and
              (LeftOf8 ?x_33)
              (RightOf18 ?x_33)
            )
          )
          (exists (?x_1 )
            (and
              (AboveOf16 ?x_1)
              (BelowOf3 ?x_1)
            )
          )
          (exists (?x_25 )
            (and
              (RightOf9 ?x_25)
              (LeftOf1 ?x_25)
            )
          )
          (exists (?x_4 )
            (and
              (RightOf17 ?x_4)
              (LeftOf2 ?x_4)
            )
          )
          (exists (?x_17 )
            (and
              (AboveOf18 ?x_17)
              (BelowOf3 ?x_17)
            )
          )
          (exists (?x_11 )
            (and
              (AboveOf18 ?x_11)
              (BelowOf2 ?x_11)
            )
          )
          (exists (?x_5 )
            (and
              (RightOf13 ?x_5)
              (LeftOf10 ?x_5)
            )
          )
          (exists (?x_34 )
            (and
              (LeftOf2 ?x_34)
              (RightOf13 ?x_34)
            )
          )
          (exists (?x_9 )
            (and
              (AboveOf15 ?x_9)
              (BelowOf4 ?x_9)
            )
          )
          (exists (?x_35 )
            (and
              (BelowOf6 ?x_35)
              (AboveOf9 ?x_35)
            )
          )
          (exists (?x_7 )
            (and
              (LeftOf3 ?x_7)
              (RightOf17 ?x_7)
            )
          )
          (exists (?x_2 )
            (and
              (AboveOf14 ?x_2)
              (BelowOf8 ?x_2)
            )
          )
          (exists (?x_8 )
            (and
              (LeftOf4 ?x_8)
              (RightOf11 ?x_8)
            )
          )
          (exists (?x_21 )
            (and
              (BelowOf3 ?x_21)
              (AboveOf15 ?x_21)
            )
          )
          (exists (?x_27 )
            (and
              (RightOf15 ?x_27)
              (LeftOf4 ?x_27)
            )
          )
          (exists (?x_25 )
            (and
              (LeftOf5 ?x_25)
              (RightOf9 ?x_25)
            )
          )
          (exists (?x_1 )
            (and
              (BelowOf10 ?x_1)
              (AboveOf17 ?x_1)
            )
          )
          (exists (?x_19 )
            (and
              (LeftOf1 ?x_19)
              (RightOf13 ?x_19)
            )
          )
          (exists (?x_16 )
            (and
              (AboveOf11 ?x_16)
              (BelowOf6 ?x_16)
            )
          )
          (exists (?x_29 )
            (and
              (AboveOf5 ?x_29)
              (BelowOf1 ?x_29)
            )
          )
          (exists (?x_24 )
            (and
              (LeftOf5 ?x_24)
              (RightOf15 ?x_24)
            )
          )
          (exists (?x_11 )
            (and
              (AboveOf17 ?x_11)
              (BelowOf11 ?x_11)
            )
          )
          (exists (?x_33 )
            (and
              (LeftOf5 ?x_33)
              (RightOf16 ?x_33)
            )
          )
          (exists (?x_27 )
            (and
              (LeftOf4 ?x_27)
              (RightOf18 ?x_27)
            )
          )
          (exists (?x_6 )
            (and
              (BelowOf1 ?x_6)
              (AboveOf17 ?x_6)
            )
          )
          (exists (?x_8 )
            (and
              (LeftOf4 ?x_8)
              (RightOf18 ?x_8)
            )
          )
          (exists (?x_8 )
            (and
              (LeftOf8 ?x_8)
              (RightOf14 ?x_8)
            )
          )
          (exists (?x_29 )
            (and
              (AboveOf6 ?x_29)
              (BelowOf1 ?x_29)
            )
          )
          (exists (?x_19 )
            (and
              (RightOf15 ?x_19)
              (LeftOf4 ?x_19)
            )
          )
          (exists (?x_14 )
            (and
              (AboveOf18 ?x_14)
              (BelowOf17 ?x_14)
            )
          )
          (exists (?x_3 )
            (and
              (RightOf14 ?x_3)
              (LeftOf1 ?x_3)
            )
          )
          (exists (?x_33 )
            (and
              (LeftOf15 ?x_33)
              (RightOf17 ?x_33)
            )
          )
          (exists (?x_6 )
            (and
              (BelowOf1 ?x_6)
              (AboveOf7 ?x_6)
            )
          )
          (exists (?x_25 )
            (and
              (LeftOf6 ?x_25)
              (RightOf9 ?x_25)
            )
          )
          (exists (?x_9 )
            (and
              (AboveOf7 ?x_9)
              (BelowOf2 ?x_9)
            )
          )
          (exists (?x_8 )
            (and
              (RightOf17 ?x_8)
              (LeftOf3 ?x_8)
            )
          )
          (exists (?x_15 )
            (and
              (BelowOf4 ?x_15)
              (AboveOf11 ?x_15)
            )
          )
          (exists (?x_15 )
            (and
              (AboveOf11 ?x_15)
              (BelowOf8 ?x_15)
            )
          )
          (exists (?x_33 )
            (and
              (RightOf18 ?x_33)
              (LeftOf7 ?x_33)
            )
          )
          (exists (?x_22 )
            (and
              (LeftOf12 ?x_22)
              (RightOf17 ?x_22)
            )
          )
          (exists (?x_26 )
            (and
              (BelowOf2 ?x_26)
              (AboveOf18 ?x_26)
            )
          )
          (exists (?x_24 )
            (and
              (RightOf9 ?x_24)
              (LeftOf4 ?x_24)
            )
          )
          (exists (?x_21 )
            (and
              (BelowOf3 ?x_21)
              (AboveOf3 ?x_21)
            )
          )
          (exists (?x_25 )
            (and
              (LeftOf6 ?x_25)
              (RightOf10 ?x_25)
            )
          )
          (exists (?x_26 )
            (and
              (BelowOf7 ?x_26)
              (AboveOf14 ?x_26)
            )
          )
          (exists (?x_30 )
            (and
              (LeftOf3 ?x_30)
              (RightOf16 ?x_30)
            )
          )
          (exists (?x_6 )
            (and
              (AboveOf12 ?x_6)
              (BelowOf4 ?x_6)
            )
          )
          (exists (?x_16 )
            (and
              (BelowOf1 ?x_16)
              (AboveOf18 ?x_16)
            )
          )
          (exists (?x_19 )
            (and
              (LeftOf10 ?x_19)
              (RightOf12 ?x_19)
            )
          )
          (exists (?x_35 )
            (and
              (BelowOf3 ?x_35)
              (AboveOf18 ?x_35)
            )
          )
          (exists (?x_35 )
            (and
              (AboveOf14 ?x_35)
              (BelowOf2 ?x_35)
            )
          )
          (exists (?x_16 )
            (and
              (AboveOf18 ?x_16)
              (BelowOf10 ?x_16)
            )
          )
          (exists (?x_19 )
            (and
              (LeftOf7 ?x_19)
              (RightOf16 ?x_19)
            )
          )
          (exists (?x_6 )
            (and
              (AboveOf14 ?x_6)
              (BelowOf3 ?x_6)
            )
          )
          (exists (?x_5 )
            (and
              (LeftOf2 ?x_5)
              (RightOf17 ?x_5)
            )
          )
          (exists (?x_22 )
            (and
              (LeftOf11 ?x_22)
              (RightOf17 ?x_22)
            )
          )
          (exists (?x_22 )
            (and
              (RightOf15 ?x_22)
              (LeftOf3 ?x_22)
            )
          )
          (exists (?x_0 )
            (and
              (AboveOf11 ?x_0)
              (BelowOf7 ?x_0)
            )
          )
          (exists (?x_4 )
            (and
              (LeftOf2 ?x_4)
              (RightOf9 ?x_4)
            )
          )
          (exists (?x_15 )
            (and
              (BelowOf1 ?x_15)
              (AboveOf17 ?x_15)
            )
          )
          (exists (?x_21 )
            (and
              (BelowOf1 ?x_21)
              (AboveOf18 ?x_21)
            )
          )
          (exists (?x_27 )
            (and
              (LeftOf5 ?x_27)
              (RightOf13 ?x_27)
            )
          )
          (exists (?x_34 )
            (and
              (RightOf16 ?x_34)
              (LeftOf1 ?x_34)
            )
          )
          (exists (?x_0 )
            (and
              (BelowOf5 ?x_0)
              (AboveOf14 ?x_0)
            )
          )
          (exists (?x_9 )
            (and
              (BelowOf6 ?x_9)
              (AboveOf18 ?x_9)
            )
          )
          (exists (?x_28 )
            (and
              (RightOf18 ?x_28)
              (LeftOf8 ?x_28)
            )
          )
          (exists (?x_16 )
            (and
              (AboveOf14 ?x_16)
              (BelowOf10 ?x_16)
            )
          )
          (exists (?x_23 )
            (and
              (AboveOf6 ?x_23)
              (BelowOf1 ?x_23)
            )
          )
          (exists (?x_9 )
            (and
              (AboveOf16 ?x_9)
              (BelowOf6 ?x_9)
            )
          )
          (exists (?x_17 )
            (and
              (BelowOf4 ?x_17)
              (AboveOf10 ?x_17)
            )
          )
          (exists (?x_19 )
            (and
              (RightOf18 ?x_19)
              (LeftOf9 ?x_19)
            )
          )
          (exists (?x_9 )
            (and
              (AboveOf7 ?x_9)
              (BelowOf4 ?x_9)
            )
          )
          (exists (?x_35 )
            (and
              (BelowOf8 ?x_35)
              (AboveOf13 ?x_35)
            )
          )
          (exists (?x_16 )
            (and
              (BelowOf7 ?x_16)
              (AboveOf15 ?x_16)
            )
          )
          (exists (?x_19 )
            (and
              (RightOf18 ?x_19)
              (LeftOf3 ?x_19)
            )
          )
          (exists (?x_19 )
            (and
              (LeftOf7 ?x_19)
              (RightOf13 ?x_19)
            )
          )
          (exists (?x_14 )
            (and
              (BelowOf15 ?x_14)
              (AboveOf18 ?x_14)
            )
          )
          (exists (?x_6 )
            (and
              (AboveOf16 ?x_6)
              (BelowOf3 ?x_6)
            )
          )
          (exists (?x_22 )
            (and
              (RightOf14 ?x_22)
              (LeftOf6 ?x_22)
            )
          )
          (exists (?x_16 )
            (and
              (AboveOf17 ?x_16)
              (BelowOf3 ?x_16)
            )
          )
          (exists (?x_0 )
            (and
              (BelowOf6 ?x_0)
              (AboveOf18 ?x_0)
            )
          )
          (exists (?x_23 )
            (and
              (AboveOf6 ?x_23)
              (BelowOf5 ?x_23)
            )
          )
          (exists (?x_23 )
            (and
              (AboveOf10 ?x_23)
              (BelowOf1 ?x_23)
            )
          )
          (exists (?x_13 )
            (and
              (LeftOf2 ?x_13)
              (RightOf4 ?x_13)
            )
          )
          (exists (?x_12 )
            (and
              (BelowOf6 ?x_12)
              (AboveOf18 ?x_12)
            )
          )
          (exists (?x_3 )
            (and
              (RightOf4 ?x_3)
              (LeftOf2 ?x_3)
            )
          )
          (exists (?x_19 )
            (and
              (LeftOf2 ?x_19)
              (RightOf13 ?x_19)
            )
          )
          (exists (?x_15 )
            (and
              (BelowOf8 ?x_15)
              (AboveOf16 ?x_15)
            )
          )
          (exists (?x_0 )
            (and
              (BelowOf9 ?x_0)
              (AboveOf18 ?x_0)
            )
          )
          (exists (?x_33 )
            (and
              (LeftOf1 ?x_33)
              (RightOf18 ?x_33)
            )
          )
          (exists (?x_24 )
            (and
              (LeftOf2 ?x_24)
              (RightOf18 ?x_24)
            )
          )
          (exists (?x_35 )
            (and
              (BelowOf7 ?x_35)
              (AboveOf15 ?x_35)
            )
          )
          (exists (?x_23 )
            (and
              (AboveOf14 ?x_23)
              (BelowOf5 ?x_23)
            )
          )
          (exists (?x_13 )
            (and
              (RightOf8 ?x_13)
              (LeftOf3 ?x_13)
            )
          )
          (exists (?x_22 )
            (and
              (RightOf16 ?x_22)
              (LeftOf2 ?x_22)
            )
          )
          (exists (?x_26 )
            (and
              (BelowOf1 ?x_26)
              (AboveOf16 ?x_26)
            )
          )
          (exists (?x_15 )
            (and
              (AboveOf18 ?x_15)
              (BelowOf2 ?x_15)
            )
          )
          (exists (?x_17 )
            (and
              (AboveOf4 ?x_17)
              (BelowOf1 ?x_17)
            )
          )
          (exists (?x_1 )
            (and
              (BelowOf9 ?x_1)
              (AboveOf15 ?x_1)
            )
          )
          (exists (?x_10 )
            (and
              (LeftOf12 ?x_10)
              (RightOf18 ?x_10)
            )
          )
          (exists (?x_22 )
            (and
              (LeftOf6 ?x_22)
              (RightOf17 ?x_22)
            )
          )
          (exists (?x_0 )
            (and
              (BelowOf4 ?x_0)
              (AboveOf18 ?x_0)
            )
          )
          (exists (?x_5 )
            (and
              (LeftOf5 ?x_5)
              (RightOf12 ?x_5)
            )
          )
          (exists (?x_7 )
            (and
              (LeftOf13 ?x_7)
              (RightOf15 ?x_7)
            )
          )
          (exists (?x_31 )
            (and
              (AboveOf14 ?x_31)
              (BelowOf3 ?x_31)
            )
          )
          (exists (?x_1 )
            (and
              (BelowOf10 ?x_1)
              (AboveOf18 ?x_1)
            )
          )
          (exists (?x_13 )
            (and
              (LeftOf1 ?x_13)
              (RightOf12 ?x_13)
            )
          )
          (exists (?x_1 )
            (and
              (BelowOf13 ?x_1)
              (AboveOf16 ?x_1)
            )
          )
          (exists (?x_9 )
            (and
              (BelowOf5 ?x_9)
              (AboveOf18 ?x_9)
            )
          )
          (exists (?x_19 )
            (and
              (LeftOf3 ?x_19)
              (RightOf13 ?x_19)
            )
          )
          (exists (?x_16 )
            (and
              (BelowOf3 ?x_16)
              (AboveOf15 ?x_16)
            )
          )
          (exists (?x_35 )
            (and
              (BelowOf1 ?x_35)
              (AboveOf10 ?x_35)
            )
          )
          (exists (?x_22 )
            (and
              (LeftOf4 ?x_22)
              (RightOf16 ?x_22)
            )
          )
          (exists (?x_12 )
            (and
              (BelowOf10 ?x_12)
              (AboveOf16 ?x_12)
            )
          )
          (exists (?x_29 )
            (and
              (AboveOf6 ?x_29)
              (BelowOf2 ?x_29)
            )
          )
          (exists (?x_31 )
            (and
              (AboveOf18 ?x_31)
              (BelowOf8 ?x_31)
            )
          )
          (exists (?x_6 )
            (and
              (BelowOf2 ?x_6)
              (AboveOf12 ?x_6)
            )
          )
          (exists (?x_2 )
            (and
              (AboveOf17 ?x_2)
              (BelowOf12 ?x_2)
            )
          )
          (exists (?x_0 )
            (and
              (AboveOf15 ?x_0)
              (BelowOf2 ?x_0)
            )
          )
          (exists (?x_33 )
            (and
              (RightOf17 ?x_33)
              (LeftOf11 ?x_33)
            )
          )
          (exists (?x_22 )
            (and
              (RightOf13 ?x_22)
              (LeftOf5 ?x_22)
            )
          )
          (exists (?x_25 )
            (and
              (LeftOf6 ?x_25)
              (RightOf8 ?x_25)
            )
          )
          (exists (?x_22 )
            (and
              (RightOf16 ?x_22)
              (LeftOf6 ?x_22)
            )
          )
          (exists (?x_6 )
            (and
              (AboveOf9 ?x_6)
              (BelowOf1 ?x_6)
            )
          )
          (exists (?x_5 )
            (and
              (LeftOf6 ?x_5)
              (RightOf12 ?x_5)
            )
          )
          (exists (?x_22 )
            (and
              (RightOf18 ?x_22)
              (LeftOf5 ?x_22)
            )
          )
          (exists (?x_13 )
            (and
              (LeftOf4 ?x_13)
              (RightOf14 ?x_13)
            )
          )
          (exists (?x_33 )
            (and
              (RightOf18 ?x_33)
              (LeftOf14 ?x_33)
            )
          )
          (exists (?x_25 )
            (and
              (LeftOf6 ?x_25)
              (RightOf13 ?x_25)
            )
          )
          (exists (?x_23 )
            (and
              (BelowOf2 ?x_23)
              (AboveOf13 ?x_23)
            )
          )
          (exists (?x_6 )
            (and
              (AboveOf6 ?x_6)
              (BelowOf6 ?x_6)
            )
          )
          (exists (?x_7 )
            (and
              (RightOf15 ?x_7)
              (LeftOf12 ?x_7)
            )
          )
          (exists (?x_13 )
            (and
              (RightOf16 ?x_13)
              (LeftOf3 ?x_13)
            )
          )
          (exists (?x_0 )
            (and
              (BelowOf7 ?x_0)
              (AboveOf11 ?x_0)
            )
          )
          (exists (?x_9 )
            (and
              (BelowOf5 ?x_9)
              (AboveOf14 ?x_9)
            )
          )
          (exists (?x_32 )
            (and
              (RightOf17 ?x_32)
              (LeftOf13 ?x_32)
            )
          )
          (exists (?x_0 )
            (and
              (BelowOf5 ?x_0)
              (AboveOf13 ?x_0)
            )
          )
          (exists (?x_25 )
            (and
              (RightOf14 ?x_25)
              (LeftOf1 ?x_25)
            )
          )
          (exists (?x_26 )
            (and
              (AboveOf16 ?x_26)
              (BelowOf12 ?x_26)
            )
          )
          (exists (?x_30 )
            (and
              (RightOf13 ?x_30)
              (LeftOf4 ?x_30)
            )
          )
          (exists (?x_7 )
            (and
              (LeftOf2 ?x_7)
              (RightOf16 ?x_7)
            )
          )
          (exists (?x_32 )
            (and
              (RightOf18 ?x_32)
              (LeftOf11 ?x_32)
            )
          )
          (exists (?x_19 )
            (and
              (LeftOf2 ?x_19)
              (RightOf11 ?x_19)
            )
          )
          (exists (?x_7 )
            (and
              (LeftOf5 ?x_7)
              (RightOf15 ?x_7)
            )
          )
          (exists (?x_29 )
            (and
              (BelowOf1 ?x_29)
              (AboveOf12 ?x_29)
            )
          )
          (exists (?x_26 )
            (and
              (AboveOf13 ?x_26)
              (BelowOf6 ?x_26)
            )
          )
          (exists (?x_5 )
            (and
              (LeftOf1 ?x_5)
              (RightOf11 ?x_5)
            )
          )
          (exists (?x_24 )
            (and
              (LeftOf6 ?x_24)
              (RightOf11 ?x_24)
            )
          )
          (exists (?x_0 )
            (and
              (BelowOf4 ?x_0)
              (AboveOf17 ?x_0)
            )
          )
          (exists (?x_19 )
            (and
              (RightOf17 ?x_19)
              (LeftOf4 ?x_19)
            )
          )
          (exists (?x_15 )
            (and
              (BelowOf3 ?x_15)
              (AboveOf11 ?x_15)
            )
          )
          (exists (?x_15 )
            (and
              (BelowOf6 ?x_15)
              (AboveOf14 ?x_15)
            )
          )
          (exists (?x_24 )
            (and
              (RightOf12 ?x_24)
              (LeftOf7 ?x_24)
            )
          )
          (exists (?x_24 )
            (and
              (RightOf9 ?x_24)
              (LeftOf7 ?x_24)
            )
          )
          (exists (?x_0 )
            (and
              (AboveOf14 ?x_0)
              (BelowOf10 ?x_0)
            )
          )
          (exists (?x_30 )
            (and
              (LeftOf3 ?x_30)
              (RightOf14 ?x_30)
            )
          )
          (exists (?x_29 )
            (and
              (BelowOf1 ?x_29)
              (AboveOf3 ?x_29)
            )
          )
          (exists (?x_13 )
            (and
              (LeftOf3 ?x_13)
              (RightOf7 ?x_13)
            )
          )
          (exists (?x_9 )
            (and
              (AboveOf7 ?x_9)
              (BelowOf5 ?x_9)
            )
          )
          (exists (?x_6 )
            (and
              (AboveOf11 ?x_6)
              (BelowOf3 ?x_6)
            )
          )
          (exists (?x_15 )
            (and
              (AboveOf10 ?x_15)
              (BelowOf8 ?x_15)
            )
          )
          (exists (?x_2 )
            (and
              (AboveOf17 ?x_2)
              (BelowOf2 ?x_2)
            )
          )
          (exists (?x_5 )
            (and
              (LeftOf1 ?x_5)
              (RightOf16 ?x_5)
            )
          )
          (exists (?x_6 )
            (and
              (AboveOf17 ?x_6)
              (BelowOf6 ?x_6)
            )
          )
          (exists (?x_31 )
            (and
              (BelowOf4 ?x_31)
              (AboveOf14 ?x_31)
            )
          )
          (exists (?x_6 )
            (and
              (BelowOf2 ?x_6)
              (AboveOf6 ?x_6)
            )
          )
          (exists (?x_8 )
            (and
              (RightOf15 ?x_8)
              (LeftOf1 ?x_8)
            )
          )
          (exists (?x_31 )
            (and
              (AboveOf16 ?x_31)
              (BelowOf9 ?x_31)
            )
          )
          (exists (?x_19 )
            (and
              (LeftOf5 ?x_19)
              (RightOf15 ?x_19)
            )
          )
          (exists (?x_12 )
            (and
              (AboveOf16 ?x_12)
              (BelowOf1 ?x_12)
            )
          )
          (exists (?x_0 )
            (and
              (AboveOf13 ?x_0)
              (BelowOf10 ?x_0)
            )
          )
          (exists (?x_34 )
            (and
              (LeftOf2 ?x_34)
              (RightOf17 ?x_34)
            )
          )
          (exists (?x_12 )
            (and
              (BelowOf12 ?x_12)
              (AboveOf18 ?x_12)
            )
          )
          (exists (?x_33 )
            (and
              (LeftOf6 ?x_33)
              (RightOf18 ?x_33)
            )
          )
          (exists (?x_35 )
            (and
              (AboveOf12 ?x_35)
              (BelowOf6 ?x_35)
            )
          )
          (exists (?x_35 )
            (and
              (BelowOf8 ?x_35)
              (AboveOf16 ?x_35)
            )
          )
          (exists (?x_32 )
            (and
              (LeftOf5 ?x_32)
              (RightOf17 ?x_32)
            )
          )
          (exists (?x_31 )
            (and
              (AboveOf16 ?x_31)
              (BelowOf11 ?x_31)
            )
          )
          (exists (?x_15 )
            (and
              (BelowOf8 ?x_15)
              (AboveOf13 ?x_15)
            )
          )
          (exists (?x_15 )
            (and
              (BelowOf4 ?x_15)
              (AboveOf9 ?x_15)
            )
          )
          (exists (?x_16 )
            (and
              (BelowOf7 ?x_16)
              (AboveOf14 ?x_16)
            )
          )
          (exists (?x_16 )
            (and
              (AboveOf18 ?x_16)
              (BelowOf6 ?x_16)
            )
          )
          (exists (?x_12 )
            (and
              (AboveOf18 ?x_12)
              (BelowOf10 ?x_12)
            )
          )
          (exists (?x_1 )
            (and
              (BelowOf2 ?x_1)
              (AboveOf17 ?x_1)
            )
          )
          (exists (?x_15 )
            (and
              (AboveOf12 ?x_15)
              (BelowOf2 ?x_15)
            )
          )
          (exists (?x_12 )
            (and
              (BelowOf15 ?x_12)
              (AboveOf16 ?x_12)
            )
          )
          (exists (?x_8 )
            (and
              (RightOf16 ?x_8)
              (LeftOf7 ?x_8)
            )
          )
          (exists (?x_26 )
            (and
              (BelowOf7 ?x_26)
              (AboveOf18 ?x_26)
            )
          )
          (exists (?x_3 )
            (and
              (RightOf5 ?x_3)
              (LeftOf1 ?x_3)
            )
          )
          (exists (?x_16 )
            (and
              (BelowOf8 ?x_16)
              (AboveOf14 ?x_16)
            )
          )
          (exists (?x_21 )
            (and
              (AboveOf12 ?x_21)
              (BelowOf2 ?x_21)
            )
          )
          (exists (?x_5 )
            (and
              (LeftOf7 ?x_5)
              (RightOf10 ?x_5)
            )
          )
          (exists (?x_34 )
            (and
              (LeftOf1 ?x_34)
              (RightOf12 ?x_34)
            )
          )
          (exists (?x_33 )
            (and
              (LeftOf9 ?x_33)
              (RightOf16 ?x_33)
            )
          )
          (exists (?x_25 )
            (and
              (LeftOf2 ?x_25)
              (RightOf8 ?x_25)
            )
          )
          (exists (?x_6 )
            (and
              (BelowOf2 ?x_6)
              (AboveOf18 ?x_6)
            )
          )
          (exists (?x_5 )
            (and
              (LeftOf6 ?x_5)
              (RightOf13 ?x_5)
            )
          )
          (exists (?x_21 )
            (and
              (AboveOf18 ?x_21)
              (BelowOf2 ?x_21)
            )
          )
          (exists (?x_17 )
            (and
              (AboveOf11 ?x_17)
              (BelowOf4 ?x_17)
            )
          )
          (exists (?x_10 )
            (and
              (RightOf18 ?x_10)
              (LeftOf10 ?x_10)
            )
          )
          (exists (?x_5 )
            (and
              (RightOf11 ?x_5)
              (LeftOf8 ?x_5)
            )
          )
          (exists (?x_17 )
            (and
              (AboveOf16 ?x_17)
              (BelowOf2 ?x_17)
            )
          )
          (exists (?x_20 )
            (and
              (RightOf13 ?x_20)
              (LeftOf1 ?x_20)
            )
          )
          (exists (?x_16 )
            (and
              (AboveOf12 ?x_16)
              (BelowOf10 ?x_16)
            )
          )
          (exists (?x_15 )
            (and
              (AboveOf14 ?x_15)
              (BelowOf7 ?x_15)
            )
          )
          (exists (?x_17 )
            (and
              (BelowOf3 ?x_17)
              (AboveOf15 ?x_17)
            )
          )
          (exists (?x_35 )
            (and
              (AboveOf14 ?x_35)
              (BelowOf7 ?x_35)
            )
          )
          (exists (?x_16 )
            (and
              (AboveOf16 ?x_16)
              (BelowOf1 ?x_16)
            )
          )
          (exists (?x_29 )
            (and
              (AboveOf14 ?x_29)
              (BelowOf2 ?x_29)
            )
          )
          (exists (?x_22 )
            (and
              (RightOf13 ?x_22)
              (LeftOf7 ?x_22)
            )
          )
          (exists (?x_9 )
            (and
              (AboveOf14 ?x_9)
              (BelowOf3 ?x_9)
            )
          )
          (exists (?x_7 )
            (and
              (LeftOf15 ?x_7)
              (RightOf16 ?x_7)
            )
          )
          (exists (?x_15 )
            (and
              (AboveOf15 ?x_15)
              (BelowOf5 ?x_15)
            )
          )
          (exists (?x_17 )
            (and
              (AboveOf7 ?x_17)
              (BelowOf4 ?x_17)
            )
          )
          (exists (?x_23 )
            (and
              (AboveOf6 ?x_23)
              (BelowOf2 ?x_23)
            )
          )
          (exists (?x_7 )
            (and
              (LeftOf12 ?x_7)
              (RightOf16 ?x_7)
            )
          )
          (exists (?x_22 )
            (and
              (RightOf18 ?x_22)
              (LeftOf2 ?x_22)
            )
          )
          (exists (?x_19 )
            (and
              (LeftOf9 ?x_19)
              (RightOf13 ?x_19)
            )
          )
          (exists (?x_1 )
            (and
              (AboveOf18 ?x_1)
              (BelowOf7 ?x_1)
            )
          )
          (exists (?x_5 )
            (and
              (LeftOf5 ?x_5)
              (RightOf17 ?x_5)
            )
          )
          (exists (?x_8 )
            (and
              (LeftOf5 ?x_8)
              (RightOf11 ?x_8)
            )
          )
          (exists (?x_9 )
            (and
              (AboveOf12 ?x_9)
              (BelowOf2 ?x_9)
            )
          )
          (exists (?x_22 )
            (and
              (LeftOf10 ?x_22)
              (RightOf14 ?x_22)
            )
          )
          (exists (?x_24 )
            (and
              (RightOf12 ?x_24)
              (LeftOf6 ?x_24)
            )
          )
          (exists (?x_7 )
            (and
              (RightOf17 ?x_7)
              (LeftOf8 ?x_7)
            )
          )
          (exists (?x_25 )
            (and
              (RightOf16 ?x_25)
              (LeftOf1 ?x_25)
            )
          )
          (exists (?x_16 )
            (and
              (AboveOf16 ?x_16)
              (BelowOf4 ?x_16)
            )
          )
          (exists (?x_2 )
            (and
              (BelowOf6 ?x_2)
              (AboveOf18 ?x_2)
            )
          )
          (exists (?x_29 )
            (and
              (AboveOf16 ?x_29)
              (BelowOf1 ?x_29)
            )
          )
          (exists (?x_28 )
            (and
              (LeftOf12 ?x_28)
              (RightOf18 ?x_28)
            )
          )
          (exists (?x_30 )
            (and
              (LeftOf6 ?x_30)
              (RightOf16 ?x_30)
            )
          )
          (exists (?x_24 )
            (and
              (LeftOf4 ?x_24)
              (RightOf15 ?x_24)
            )
          )
          (exists (?x_25 )
            (and
              (RightOf17 ?x_25)
              (LeftOf4 ?x_25)
            )
          )
          (exists (?x_27 )
            (and
              (RightOf15 ?x_27)
              (LeftOf2 ?x_27)
            )
          )
          (exists (?x_27 )
            (and
              (RightOf9 ?x_27)
              (LeftOf4 ?x_27)
            )
          )
          (exists (?x_7 )
            (and
              (RightOf18 ?x_7)
              (LeftOf10 ?x_7)
            )
          )
          (exists (?x_10 )
            (and
              (LeftOf7 ?x_10)
              (RightOf14 ?x_10)
            )
          )
          (exists (?x_34 )
            (and
              (LeftOf7 ?x_34)
              (RightOf17 ?x_34)
            )
          )
          (exists (?x_30 )
            (and
              (RightOf17 ?x_30)
              (LeftOf9 ?x_30)
            )
          )
          (exists (?x_19 )
            (and
              (LeftOf10 ?x_19)
              (RightOf17 ?x_19)
            )
          )
          (exists (?x_10 )
            (and
              (RightOf16 ?x_10)
              (LeftOf6 ?x_10)
            )
          )
          (exists (?x_30 )
            (and
              (RightOf16 ?x_30)
              (LeftOf9 ?x_30)
            )
          )
          (exists (?x_20 )
            (and
              (LeftOf1 ?x_20)
              (RightOf17 ?x_20)
            )
          )
          (exists (?x_15 )
            (and
              (BelowOf4 ?x_15)
              (AboveOf8 ?x_15)
            )
          )
          (exists (?x_8 )
            (and
              (LeftOf2 ?x_8)
              (RightOf10 ?x_8)
            )
          )
          (exists (?x_27 )
            (and
              (RightOf10 ?x_27)
              (LeftOf4 ?x_27)
            )
          )
          (exists (?x_17 )
            (and
              (BelowOf4 ?x_17)
              (AboveOf18 ?x_17)
            )
          )
          (exists (?x_31 )
            (and
              (AboveOf16 ?x_31)
              (BelowOf10 ?x_31)
            )
          )
          (exists (?x_23 )
            (and
              (BelowOf1 ?x_23)
              (AboveOf13 ?x_23)
            )
          )
          (exists (?x_27 )
            (and
              (LeftOf1 ?x_27)
              (RightOf8 ?x_27)
            )
          )
          (exists (?x_17 )
            (and
              (AboveOf17 ?x_17)
              (BelowOf3 ?x_17)
            )
          )
          (exists (?x_26 )
            (and
              (BelowOf7 ?x_26)
              (AboveOf13 ?x_26)
            )
          )
          (exists (?x_23 )
            (and
              (BelowOf2 ?x_23)
              (AboveOf16 ?x_23)
            )
          )
          (exists (?x_5 )
            (and
              (RightOf13 ?x_5)
              (LeftOf4 ?x_5)
            )
          )
          (exists (?x_29 )
            (and
              (AboveOf12 ?x_29)
              (BelowOf2 ?x_29)
            )
          )
          (exists (?x_5 )
            (and
              (LeftOf7 ?x_5)
              (RightOf12 ?x_5)
            )
          )
          (exists (?x_31 )
            (and
              (AboveOf16 ?x_31)
              (BelowOf2 ?x_31)
            )
          )
          (exists (?x_23 )
            (and
              (BelowOf5 ?x_23)
              (AboveOf18 ?x_23)
            )
          )
          (exists (?x_6 )
            (and
              (AboveOf8 ?x_6)
              (BelowOf5 ?x_6)
            )
          )
          (exists (?x_25 )
            (and
              (LeftOf5 ?x_25)
              (RightOf8 ?x_25)
            )
          )
          (exists (?x_21 )
            (and
              (BelowOf2 ?x_21)
              (AboveOf15 ?x_21)
            )
          )
          (exists (?x_23 )
            (and
              (BelowOf3 ?x_23)
              (AboveOf11 ?x_23)
            )
          )
          (exists (?x_29 )
            (and
              (AboveOf8 ?x_29)
              (BelowOf2 ?x_29)
            )
          )
          (exists (?x_25 )
            (and
              (LeftOf5 ?x_25)
              (RightOf16 ?x_25)
            )
          )
          (exists (?x_5 )
            (and
              (RightOf13 ?x_5)
              (LeftOf2 ?x_5)
            )
          )
          (exists (?x_22 )
            (and
              (RightOf17 ?x_22)
              (LeftOf3 ?x_22)
            )
          )
          (exists (?x_12 )
            (and
              (BelowOf6 ?x_12)
              (AboveOf17 ?x_12)
            )
          )
          (exists (?x_22 )
            (and
              (RightOf16 ?x_22)
              (LeftOf5 ?x_22)
            )
          )
          (exists (?x_24 )
            (and
              (LeftOf3 ?x_24)
              (RightOf10 ?x_24)
            )
          )
          (exists (?x_2 )
            (and
              (AboveOf15 ?x_2)
              (BelowOf9 ?x_2)
            )
          )
          (exists (?x_12 )
            (and
              (BelowOf8 ?x_12)
              (AboveOf18 ?x_12)
            )
          )
          (exists (?x_0 )
            (and
              (AboveOf17 ?x_0)
              (BelowOf6 ?x_0)
            )
          )
          (exists (?x_24 )
            (and
              (LeftOf7 ?x_24)
              (RightOf18 ?x_24)
            )
          )
          (exists (?x_30 )
            (and
              (RightOf14 ?x_30)
              (LeftOf4 ?x_30)
            )
          )
          (exists (?x_0 )
            (and
              (BelowOf1 ?x_0)
              (AboveOf10 ?x_0)
            )
          )
          (exists (?x_25 )
            (and
              (LeftOf2 ?x_25)
              (RightOf6 ?x_25)
            )
          )
          (exists (?x_16 )
            (and
              (BelowOf7 ?x_16)
              (AboveOf18 ?x_16)
            )
          )
          (exists (?x_2 )
            (and
              (AboveOf17 ?x_2)
              (BelowOf3 ?x_2)
            )
          )
          (exists (?x_6 )
            (and
              (BelowOf2 ?x_6)
              (AboveOf16 ?x_6)
            )
          )
          (exists (?x_26 )
            (and
              (AboveOf15 ?x_26)
              (BelowOf10 ?x_26)
            )
          )
          (exists (?x_14 )
            (and
              (AboveOf18 ?x_14)
              (BelowOf12 ?x_14)
            )
          )
          (exists (?x_19 )
            (and
              (RightOf15 ?x_19)
              (LeftOf8 ?x_19)
            )
          )
          (exists (?x_30 )
            (and
              (LeftOf3 ?x_30)
              (RightOf17 ?x_30)
            )
          )
          (exists (?x_5 )
            (and
              (LeftOf3 ?x_5)
              (RightOf18 ?x_5)
            )
          )
          (exists (?x_5 )
            (and
              (LeftOf1 ?x_5)
              (RightOf14 ?x_5)
            )
          )
          (exists (?x_24 )
            (and
              (LeftOf3 ?x_24)
              (RightOf13 ?x_24)
            )
          )
          (exists (?x_6 )
            (and
              (AboveOf7 ?x_6)
              (BelowOf6 ?x_6)
            )
          )
          (exists (?x_17 )
            (and
              (AboveOf14 ?x_17)
              (BelowOf3 ?x_17)
            )
          )
          (exists (?x_32 )
            (and
              (LeftOf13 ?x_32)
              (RightOf18 ?x_32)
            )
          )
          (exists (?x_7 )
            (and
              (LeftOf11 ?x_7)
              (RightOf15 ?x_7)
            )
          )
          (exists (?x_22 )
            (and
              (LeftOf11 ?x_22)
              (RightOf13 ?x_22)
            )
          )
          (exists (?x_5 )
            (and
              (LeftOf4 ?x_5)
              (RightOf11 ?x_5)
            )
          )
          (exists (?x_5 )
            (and
              (LeftOf1 ?x_5)
              (RightOf15 ?x_5)
            )
          )
          (exists (?x_17 )
            (and
              (AboveOf14 ?x_17)
              (BelowOf2 ?x_17)
            )
          )
          (exists (?x_24 )
            (and
              (LeftOf6 ?x_24)
              (RightOf9 ?x_24)
            )
          )
          (exists (?x_33 )
            (and
              (LeftOf3 ?x_33)
              (RightOf17 ?x_33)
            )
          )
          (exists (?x_25 )
            (and
              (RightOf6 ?x_25)
              (LeftOf4 ?x_25)
            )
          )
          (exists (?x_3 )
            (and
              (LeftOf1 ?x_3)
              (RightOf4 ?x_3)
            )
          )
          (exists (?x_13 )
            (and
              (LeftOf4 ?x_13)
              (RightOf11 ?x_13)
            )
          )
          (exists (?x_0 )
            (and
              (BelowOf7 ?x_0)
              (AboveOf10 ?x_0)
            )
          )
          (exists (?x_21 )
            (and
              (AboveOf8 ?x_21)
              (BelowOf3 ?x_21)
            )
          )
          (exists (?x_17 )
            (and
              (AboveOf7 ?x_17)
              (BelowOf1 ?x_17)
            )
          )
          (exists (?x_19 )
            (and
              (RightOf11 ?x_19)
              (LeftOf11 ?x_19)
            )
          )
          (exists (?x_32 )
            (and
              (RightOf17 ?x_32)
              (LeftOf12 ?x_32)
            )
          )
          (exists (?x_1 )
            (and
              (AboveOf18 ?x_1)
              (BelowOf6 ?x_1)
            )
          )
          (exists (?x_10 )
            (and
              (LeftOf12 ?x_10)
              (RightOf17 ?x_10)
            )
          )
          (exists (?x_31 )
            (and
              (BelowOf1 ?x_31)
              (AboveOf15 ?x_31)
            )
          )
          (exists (?x_23 )
            (and
              (BelowOf3 ?x_23)
              (AboveOf10 ?x_23)
            )
          )
          (exists (?x_10 )
            (and
              (LeftOf4 ?x_10)
              (RightOf14 ?x_10)
            )
          )
          (exists (?x_9 )
            (and
              (BelowOf7 ?x_9)
              (AboveOf17 ?x_9)
            )
          )
          (exists (?x_24 )
            (and
              (RightOf17 ?x_24)
              (LeftOf4 ?x_24)
            )
          )
          (exists (?x_2 )
            (and
              (AboveOf15 ?x_2)
              (BelowOf11 ?x_2)
            )
          )
          (exists (?x_9 )
            (and
              (AboveOf8 ?x_9)
              (BelowOf1 ?x_9)
            )
          )
          (exists (?x_27 )
            (and
              (LeftOf4 ?x_27)
              (RightOf10 ?x_27)
            )
          )
          (exists (?x_13 )
            (and
              (LeftOf4 ?x_13)
              (RightOf13 ?x_13)
            )
          )
          (exists (?x_15 )
            (and
              (BelowOf7 ?x_15)
              (AboveOf13 ?x_15)
            )
          )
          (exists (?x_13 )
            (and
              (RightOf6 ?x_13)
              (LeftOf2 ?x_13)
            )
          )
          (exists (?x_17 )
            (and
              (BelowOf2 ?x_17)
              (AboveOf17 ?x_17)
            )
          )
          (exists (?x_6 )
            (and
              (AboveOf18 ?x_6)
              (BelowOf2 ?x_6)
            )
          )
          (exists (?x_22 )
            (and
              (RightOf15 ?x_22)
              (LeftOf7 ?x_22)
            )
          )
          (exists (?x_30 )
            (and
              (RightOf18 ?x_30)
              (LeftOf1 ?x_30)
            )
          )
          (exists (?x_16 )
            (and
              (AboveOf16 ?x_16)
              (BelowOf11 ?x_16)
            )
          )
          (exists (?x_34 )
            (and
              (RightOf13 ?x_34)
              (LeftOf7 ?x_34)
            )
          )
          (exists (?x_4 )
            (and
              (RightOf18 ?x_4)
              (LeftOf1 ?x_4)
            )
          )
          (exists (?x_7 )
            (and
              (RightOf18 ?x_7)
              (LeftOf9 ?x_7)
            )
          )
          (exists (?x_27 )
            (and
              (RightOf17 ?x_27)
              (LeftOf4 ?x_27)
            )
          )
          (exists (?x_5 )
            (and
              (RightOf14 ?x_5)
              (LeftOf8 ?x_5)
            )
          )
          (exists (?x_3 )
            (and
              (RightOf18 ?x_3)
              (LeftOf2 ?x_3)
            )
          )
          (exists (?x_14 )
            (and
              (AboveOf18 ?x_14)
              (BelowOf6 ?x_14)
            )
          )
          (exists (?x_17 )
            (and
              (AboveOf5 ?x_17)
              (BelowOf3 ?x_17)
            )
          )
          (exists (?x_15 )
            (and
              (BelowOf3 ?x_15)
              (AboveOf8 ?x_15)
            )
          )
          (exists (?x_19 )
            (and
              (RightOf14 ?x_19)
              (LeftOf3 ?x_19)
            )
          )
          (exists (?x_23 )
            (and
              (AboveOf8 ?x_23)
              (BelowOf1 ?x_23)
            )
          )
          (exists (?x_26 )
            (and
              (BelowOf4 ?x_26)
              (AboveOf17 ?x_26)
            )
          )
          (exists (?x_29 )
            (and
              (BelowOf1 ?x_29)
              (AboveOf11 ?x_29)
            )
          )
          (exists (?x_2 )
            (and
              (BelowOf6 ?x_2)
              (AboveOf15 ?x_2)
            )
          )
          (exists (?x_26 )
            (and
              (AboveOf15 ?x_26)
              (BelowOf4 ?x_26)
            )
          )
          (exists (?x_0 )
            (and
              (AboveOf17 ?x_0)
              (BelowOf7 ?x_0)
            )
          )
          (exists (?x_23 )
            (and
              (AboveOf15 ?x_23)
              (BelowOf1 ?x_23)
            )
          )
          (exists (?x_8 )
            (and
              (RightOf17 ?x_8)
              (LeftOf1 ?x_8)
            )
          )
          (exists (?x_16 )
            (and
              (AboveOf11 ?x_16)
              (BelowOf4 ?x_16)
            )
          )
          (exists (?x_22 )
            (and
              (LeftOf5 ?x_22)
              (RightOf17 ?x_22)
            )
          )
          (exists (?x_31 )
            (and
              (AboveOf13 ?x_31)
              (BelowOf12 ?x_31)
            )
          )
          (exists (?x_35 )
            (and
              (AboveOf9 ?x_35)
              (BelowOf1 ?x_35)
            )
          )
          (exists (?x_23 )
            (and
              (BelowOf4 ?x_23)
              (AboveOf13 ?x_23)
            )
          )
          (exists (?x_3 )
            (and
              (LeftOf1 ?x_3)
              (RightOf17 ?x_3)
            )
          )
          (exists (?x_27 )
            (and
              (RightOf12 ?x_27)
              (LeftOf1 ?x_27)
            )
          )
          (exists (?x_23 )
            (and
              (BelowOf5 ?x_23)
              (AboveOf10 ?x_23)
            )
          )
          (exists (?x_27 )
            (and
              (LeftOf3 ?x_27)
              (RightOf15 ?x_27)
            )
          )
          (exists (?x_17 )
            (and
              (AboveOf9 ?x_17)
              (BelowOf1 ?x_17)
            )
          )
          (exists (?x_17 )
            (and
              (AboveOf10 ?x_17)
              (BelowOf3 ?x_17)
            )
          )
          (exists (?x_19 )
            (and
              (LeftOf5 ?x_19)
              (RightOf17 ?x_19)
            )
          )
          (exists (?x_0 )
            (and
              (BelowOf5 ?x_0)
              (AboveOf17 ?x_0)
            )
          )
          (exists (?x_25 )
            (and
              (RightOf11 ?x_25)
              (LeftOf2 ?x_25)
            )
          )
          (exists (?x_16 )
            (and
              (AboveOf16 ?x_16)
              (BelowOf9 ?x_16)
            )
          )
          (exists (?x_31 )
            (and
              (AboveOf16 ?x_31)
              (BelowOf13 ?x_31)
            )
          )
          (exists (?x_32 )
            (and
              (LeftOf15 ?x_32)
              (RightOf18 ?x_32)
            )
          )
          (exists (?x_19 )
            (and
              (LeftOf3 ?x_19)
              (RightOf16 ?x_19)
            )
          )
          (exists (?x_2 )
            (and
              (AboveOf18 ?x_2)
              (BelowOf2 ?x_2)
            )
          )
          (exists (?x_27 )
            (and
              (LeftOf3 ?x_27)
              (RightOf17 ?x_27)
            )
          )
          (exists (?x_16 )
            (and
              (AboveOf13 ?x_16)
              (BelowOf6 ?x_16)
            )
          )
          (exists (?x_14 )
            (and
              (AboveOf18 ?x_14)
              (BelowOf10 ?x_14)
            )
          )
          (exists (?x_10 )
            (and
              (LeftOf12 ?x_10)
              (RightOf15 ?x_10)
            )
          )
          (exists (?x_8 )
            (and
              (LeftOf1 ?x_8)
              (RightOf14 ?x_8)
            )
          )
          (exists (?x_34 )
            (and
              (RightOf15 ?x_34)
              (LeftOf5 ?x_34)
            )
          )
          (exists (?x_8 )
            (and
              (RightOf16 ?x_8)
              (LeftOf6 ?x_8)
            )
          )
          (exists (?x_0 )
            (and
              (AboveOf12 ?x_0)
              (BelowOf9 ?x_0)
            )
          )
          (exists (?x_22 )
            (and
              (RightOf14 ?x_22)
              (LeftOf8 ?x_22)
            )
          )
          (exists (?x_0 )
            (and
              (AboveOf15 ?x_0)
              (BelowOf10 ?x_0)
            )
          )
          (exists (?x_27 )
            (and
              (RightOf7 ?x_27)
              (LeftOf2 ?x_27)
            )
          )
          (exists (?x_11 )
            (and
              (AboveOf18 ?x_11)
              (BelowOf10 ?x_11)
            )
          )
          (exists (?x_12 )
            (and
              (BelowOf3 ?x_12)
              (AboveOf17 ?x_12)
            )
          )
          (exists (?x_6 )
            (and
              (AboveOf10 ?x_6)
              (BelowOf4 ?x_6)
            )
          )
          (exists (?x_33 )
            (and
              (RightOf18 ?x_33)
              (LeftOf12 ?x_33)
            )
          )
          (exists (?x_6 )
            (and
              (BelowOf1 ?x_6)
              (AboveOf11 ?x_6)
            )
          )
          (exists (?x_11 )
            (and
              (AboveOf17 ?x_11)
              (BelowOf15 ?x_11)
            )
          )
          (exists (?x_26 )
            (and
              (AboveOf16 ?x_26)
              (BelowOf3 ?x_26)
            )
          )
          (exists (?x_15 )
            (and
              (AboveOf11 ?x_15)
              (BelowOf5 ?x_15)
            )
          )
          (exists (?x_9 )
            (and
              (AboveOf17 ?x_9)
              (BelowOf5 ?x_9)
            )
          )
          (exists (?x_14 )
            (and
              (AboveOf18 ?x_14)
              (BelowOf16 ?x_14)
            )
          )
          (exists (?x_25 )
            (and
              (LeftOf6 ?x_25)
              (RightOf15 ?x_25)
            )
          )
          (exists (?x_6 )
            (and
              (AboveOf11 ?x_6)
              (BelowOf6 ?x_6)
            )
          )
          (exists (?x_6 )
            (and
              (BelowOf4 ?x_6)
              (AboveOf12 ?x_6)
            )
          )
          (exists (?x_0 )
            (and
              (AboveOf12 ?x_0)
              (BelowOf1 ?x_0)
            )
          )
          (exists (?x_7 )
            (and
              (RightOf17 ?x_7)
              (LeftOf4 ?x_7)
            )
          )
          (exists (?x_2 )
            (and
              (BelowOf3 ?x_2)
              (AboveOf16 ?x_2)
            )
          )
          (exists (?x_26 )
            (and
              (BelowOf12 ?x_26)
              (AboveOf12 ?x_26)
            )
          )
          (exists (?x_10 )
            (and
              (RightOf17 ?x_10)
              (LeftOf8 ?x_10)
            )
          )
          (exists (?x_6 )
            (and
              (AboveOf6 ?x_6)
              (BelowOf4 ?x_6)
            )
          )
          (exists (?x_22 )
            (and
              (RightOf15 ?x_22)
              (LeftOf5 ?x_22)
            )
          )
          (exists (?x_5 )
            (and
              (RightOf15 ?x_5)
              (LeftOf5 ?x_5)
            )
          )
          (exists (?x_22 )
            (and
              (LeftOf10 ?x_22)
              (RightOf12 ?x_22)
            )
          )
          (exists (?x_26 )
            (and
              (BelowOf9 ?x_26)
              (AboveOf16 ?x_26)
            )
          )
          (exists (?x_0 )
            (and
              (BelowOf5 ?x_0)
              (AboveOf10 ?x_0)
            )
          )
          (exists (?x_22 )
            (and
              (LeftOf11 ?x_22)
              (RightOf14 ?x_22)
            )
          )
          (exists (?x_17 )
            (and
              (BelowOf2 ?x_17)
              (AboveOf18 ?x_17)
            )
          )
          (exists (?x_19 )
            (and
              (RightOf15 ?x_19)
              (LeftOf5 ?x_19)
            )
          )
          (exists (?x_1 )
            (and
              (BelowOf14 ?x_1)
              (AboveOf17 ?x_1)
            )
          )
          (exists (?x_10 )
            (and
              (LeftOf9 ?x_10)
              (RightOf15 ?x_10)
            )
          )
          (exists (?x_9 )
            (and
              (BelowOf4 ?x_9)
              (AboveOf10 ?x_9)
            )
          )
          (exists (?x_17 )
            (and
              (BelowOf2 ?x_17)
              (AboveOf12 ?x_17)
            )
          )
          (exists (?x_8 )
            (and
              (LeftOf5 ?x_8)
              (RightOf10 ?x_8)
            )
          )
          (exists (?x_17 )
            (and
              (BelowOf4 ?x_17)
              (AboveOf6 ?x_17)
            )
          )
          (exists (?x_25 )
            (and
              (RightOf12 ?x_25)
              (LeftOf5 ?x_25)
            )
          )
          (exists (?x_26 )
            (and
              (AboveOf15 ?x_26)
              (BelowOf2 ?x_26)
            )
          )
          (exists (?x_27 )
            (and
              (LeftOf3 ?x_27)
              (RightOf13 ?x_27)
            )
          )
          (exists (?x_31 )
            (and
              (AboveOf18 ?x_31)
              (BelowOf11 ?x_31)
            )
          )
          (exists (?x_25 )
            (and
              (LeftOf5 ?x_25)
              (RightOf6 ?x_25)
            )
          )
          (exists (?x_23 )
            (and
              (AboveOf5 ?x_23)
              (BelowOf4 ?x_23)
            )
          )
          (exists (?x_26 )
            (and
              (AboveOf17 ?x_26)
              (BelowOf12 ?x_26)
            )
          )
          (exists (?x_31 )
            (and
              (AboveOf13 ?x_31)
              (BelowOf2 ?x_31)
            )
          )
          (exists (?x_24 )
            (and
              (LeftOf6 ?x_24)
              (RightOf12 ?x_24)
            )
          )
          (exists (?x_8 )
            (and
              (RightOf15 ?x_8)
              (LeftOf4 ?x_8)
            )
          )
          (exists (?x_33 )
            (and
              (RightOf17 ?x_33)
              (LeftOf2 ?x_33)
            )
          )
          (exists (?x_14 )
            (and
              (AboveOf18 ?x_14)
              (BelowOf13 ?x_14)
            )
          )
          (exists (?x_23 )
            (and
              (BelowOf2 ?x_23)
              (AboveOf9 ?x_23)
            )
          )
          (exists (?x_2 )
            (and
              (AboveOf16 ?x_2)
              (BelowOf8 ?x_2)
            )
          )
          (exists (?x_12 )
            (and
              (AboveOf16 ?x_12)
              (BelowOf13 ?x_12)
            )
          )
          (exists (?x_20 )
            (and
              (LeftOf1 ?x_20)
              (RightOf4 ?x_20)
            )
          )
          (exists (?x_12 )
            (and
              (AboveOf16 ?x_12)
              (BelowOf4 ?x_12)
            )
          )
          (exists (?x_13 )
            (and
              (RightOf10 ?x_13)
              (LeftOf1 ?x_13)
            )
          )
          (exists (?x_34 )
            (and
              (LeftOf7 ?x_34)
              (RightOf11 ?x_34)
            )
          )
          (exists (?x_23 )
            (and
              (BelowOf3 ?x_23)
              (AboveOf18 ?x_23)
            )
          )
          (exists (?x_6 )
            (and
              (BelowOf1 ?x_6)
              (AboveOf13 ?x_6)
            )
          )
          (exists (?x_22 )
            (and
              (LeftOf4 ?x_22)
              (RightOf13 ?x_22)
            )
          )
          (exists (?x_0 )
            (and
              (AboveOf12 ?x_0)
              (BelowOf5 ?x_0)
            )
          )
          (exists (?x_6 )
            (and
              (AboveOf8 ?x_6)
              (BelowOf3 ?x_6)
            )
          )
          (exists (?x_15 )
            (and
              (BelowOf2 ?x_15)
              (AboveOf13 ?x_15)
            )
          )
          (exists (?x_13 )
            (and
              (RightOf10 ?x_13)
              (LeftOf3 ?x_13)
            )
          )
          (exists (?x_19 )
            (and
              (RightOf17 ?x_19)
              (LeftOf6 ?x_19)
            )
          )
          (exists (?x_30 )
            (and
              (LeftOf6 ?x_30)
              (RightOf13 ?x_30)
            )
          )
          (exists (?x_34 )
            (and
              (LeftOf4 ?x_34)
              (RightOf11 ?x_34)
            )
          )
          (exists (?x_9 )
            (and
              (BelowOf3 ?x_9)
              (AboveOf15 ?x_9)
            )
          )
          (exists (?x_23 )
            (and
              (AboveOf11 ?x_23)
              (BelowOf2 ?x_23)
            )
          )
          (exists (?x_9 )
            (and
              (BelowOf7 ?x_9)
              (AboveOf16 ?x_9)
            )
          )
          (exists (?x_26 )
            (and
              (AboveOf13 ?x_26)
              (BelowOf8 ?x_26)
            )
          )
          (exists (?x_10 )
            (and
              (RightOf17 ?x_10)
              (LeftOf6 ?x_10)
            )
          )
          (exists (?x_13 )
            (and
              (RightOf14 ?x_13)
              (LeftOf3 ?x_13)
            )
          )
          (exists (?x_11 )
            (and
              (AboveOf17 ?x_11)
              (BelowOf8 ?x_11)
            )
          )
          (exists (?x_25 )
            (and
              (LeftOf3 ?x_25)
              (RightOf17 ?x_25)
            )
          )
          (exists (?x_26 )
            (and
              (BelowOf1 ?x_26)
              (AboveOf14 ?x_26)
            )
          )
          (exists (?x_28 )
            (and
              (RightOf18 ?x_28)
              (LeftOf14 ?x_28)
            )
          )
          (exists (?x_8 )
            (and
              (LeftOf4 ?x_8)
              (RightOf8 ?x_8)
            )
          )
          (exists (?x_19 )
            (and
              (RightOf18 ?x_19)
              (LeftOf7 ?x_19)
            )
          )
          (exists (?x_32 )
            (and
              (RightOf18 ?x_32)
              (LeftOf17 ?x_32)
            )
          )
          (exists (?x_0 )
            (and
              (BelowOf10 ?x_0)
              (AboveOf10 ?x_0)
            )
          )
          (exists (?x_0 )
            (and
              (BelowOf2 ?x_0)
              (AboveOf18 ?x_0)
            )
          )
          (exists (?x_23 )
            (and
              (BelowOf5 ?x_23)
              (AboveOf17 ?x_23)
            )
          )
          (exists (?x_1 )
            (and
              (BelowOf5 ?x_1)
              (AboveOf17 ?x_1)
            )
          )
          (exists (?x_0 )
            (and
              (AboveOf15 ?x_0)
              (BelowOf7 ?x_0)
            )
          )
          (exists (?x_26 )
            (and
              (BelowOf1 ?x_26)
              (AboveOf12 ?x_26)
            )
          )
          (exists (?x_27 )
            (and
              (LeftOf5 ?x_27)
              (RightOf18 ?x_27)
            )
          )
          (exists (?x_8 )
            (and
              (LeftOf7 ?x_8)
              (RightOf13 ?x_8)
            )
          )
          (exists (?x_8 )
            (and
              (RightOf17 ?x_8)
              (LeftOf8 ?x_8)
            )
          )
          (exists (?x_2 )
            (and
              (BelowOf5 ?x_2)
              (AboveOf18 ?x_2)
            )
          )
          (exists (?x_0 )
            (and
              (AboveOf13 ?x_0)
              (BelowOf2 ?x_0)
            )
          )
          (exists (?x_0 )
            (and
              (BelowOf6 ?x_0)
              (AboveOf16 ?x_0)
            )
          )
          (exists (?x_10 )
            (and
              (LeftOf11 ?x_10)
              (RightOf18 ?x_10)
            )
          )
          (exists (?x_6 )
            (and
              (AboveOf8 ?x_6)
              (BelowOf4 ?x_6)
            )
          )
          (exists (?x_34 )
            (and
              (RightOf16 ?x_34)
              (LeftOf9 ?x_34)
            )
          )
          (exists (?x_34 )
            (and
              (RightOf13 ?x_34)
              (LeftOf5 ?x_34)
            )
          )
          (exists (?x_5 )
            (and
              (LeftOf6 ?x_5)
              (RightOf10 ?x_5)
            )
          )
          (exists (?x_32 )
            (and
              (RightOf17 ?x_32)
              (LeftOf9 ?x_32)
            )
          )
          (exists (?x_8 )
            (and
              (RightOf15 ?x_8)
              (LeftOf7 ?x_8)
            )
          )
          (exists (?x_6 )
            (and
              (AboveOf6 ?x_6)
              (BelowOf3 ?x_6)
            )
          )
          (exists (?x_33 )
            (and
              (LeftOf7 ?x_33)
              (RightOf16 ?x_33)
            )
          )
          (exists (?x_4 )
            (and
              (RightOf8 ?x_4)
              (LeftOf1 ?x_4)
            )
          )
          (exists (?x_2 )
            (and
              (AboveOf18 ?x_2)
              (BelowOf10 ?x_2)
            )
          )
          (exists (?x_0 )
            (and
              (AboveOf17 ?x_0)
              (BelowOf10 ?x_0)
            )
          )
          (exists (?x_24 )
            (and
              (LeftOf5 ?x_24)
              (RightOf10 ?x_24)
            )
          )
          (exists (?x_4 )
            (and
              (RightOf4 ?x_4)
              (LeftOf2 ?x_4)
            )
          )
          (exists (?x_4 )
            (and
              (RightOf14 ?x_4)
              (LeftOf1 ?x_4)
            )
          )
          (exists (?x_5 )
            (and
              (LeftOf9 ?x_5)
              (RightOf18 ?x_5)
            )
          )
          (exists (?x_18 )
            (and
              (BelowOf1 ?x_18)
              (AboveOf1 ?x_18)
            )
          )
          (exists (?x_33 )
            (and
              (LeftOf11 ?x_33)
              (RightOf18 ?x_33)
            )
          )
          (exists (?x_27 )
            (and
              (RightOf6 ?x_27)
              (LeftOf2 ?x_27)
            )
          )
          (exists (?x_35 )
            (and
              (AboveOf16 ?x_35)
              (BelowOf4 ?x_35)
            )
          )
          (exists (?x_13 )
            (and
              (RightOf9 ?x_13)
              (LeftOf3 ?x_13)
            )
          )
          (exists (?x_16 )
            (and
              (BelowOf4 ?x_16)
              (AboveOf18 ?x_16)
            )
          )
          (exists (?x_9 )
            (and
              (AboveOf16 ?x_9)
              (BelowOf3 ?x_9)
            )
          )
          (exists (?x_31 )
            (and
              (AboveOf13 ?x_31)
              (BelowOf11 ?x_31)
            )
          )
          (exists (?x_30 )
            (and
              (RightOf14 ?x_30)
              (LeftOf2 ?x_30)
            )
          )
          (exists (?x_33 )
            (and
              (LeftOf4 ?x_33)
              (RightOf18 ?x_33)
            )
          )
          (exists (?x_28 )
            (and
              (LeftOf10 ?x_28)
              (RightOf18 ?x_28)
            )
          )
          (exists (?x_8 )
            (and
              (LeftOf7 ?x_8)
              (RightOf11 ?x_8)
            )
          )
          (exists (?x_23 )
            (and
              (BelowOf3 ?x_23)
              (AboveOf5 ?x_23)
            )
          )
          (exists (?x_7 )
            (and
              (LeftOf8 ?x_7)
              (RightOf15 ?x_7)
            )
          )
          (exists (?x_19 )
            (and
              (RightOf17 ?x_19)
              (LeftOf11 ?x_19)
            )
          )
          (exists (?x_16 )
            (and
              (AboveOf14 ?x_16)
              (BelowOf2 ?x_16)
            )
          )
          (exists (?x_17 )
            (and
              (BelowOf1 ?x_17)
              (AboveOf17 ?x_17)
            )
          )
          (exists (?x_3 )
            (and
              (LeftOf3 ?x_3)
              (RightOf11 ?x_3)
            )
          )
          (exists (?x_23 )
            (and
              (BelowOf3 ?x_23)
              (AboveOf8 ?x_23)
            )
          )
          (exists (?x_0 )
            (and
              (BelowOf3 ?x_0)
              (AboveOf10 ?x_0)
            )
          )
          (exists (?x_26 )
            (and
              (BelowOf7 ?x_26)
              (AboveOf16 ?x_26)
            )
          )
          (exists (?x_2 )
            (and
              (BelowOf4 ?x_2)
              (AboveOf18 ?x_2)
            )
          )
          (exists (?x_17 )
            (and
              (BelowOf2 ?x_17)
              (AboveOf10 ?x_17)
            )
          )
          (exists (?x_13 )
            (and
              (LeftOf4 ?x_13)
              (RightOf12 ?x_13)
            )
          )
          (exists (?x_15 )
            (and
              (BelowOf4 ?x_15)
              (AboveOf16 ?x_15)
            )
          )
          (exists (?x_30 )
            (and
              (LeftOf13 ?x_30)
              (RightOf14 ?x_30)
            )
          )
          (exists (?x_7 )
            (and
              (RightOf18 ?x_7)
              (LeftOf14 ?x_7)
            )
          )
          (exists (?x_22 )
            (and
              (RightOf13 ?x_22)
              (LeftOf10 ?x_22)
            )
          )
          (exists (?x_30 )
            (and
              (LeftOf10 ?x_30)
              (RightOf13 ?x_30)
            )
          )
          (exists (?x_1 )
            (and
              (AboveOf16 ?x_1)
              (BelowOf12 ?x_1)
            )
          )
          (exists (?x_20 )
            (and
              (RightOf5 ?x_20)
              (LeftOf1 ?x_20)
            )
          )
          (exists (?x_8 )
            (and
              (RightOf18 ?x_8)
              (LeftOf3 ?x_8)
            )
          )
          (exists (?x_23 )
            (and
              (AboveOf11 ?x_23)
              (BelowOf3 ?x_23)
            )
          )
          (exists (?x_8 )
            (and
              (LeftOf4 ?x_8)
              (RightOf13 ?x_8)
            )
          )
          (exists (?x_35 )
            (and
              (BelowOf8 ?x_35)
              (AboveOf9 ?x_35)
            )
          )
          (exists (?x_35 )
            (and
              (BelowOf6 ?x_35)
              (AboveOf11 ?x_35)
            )
          )
          (exists (?x_6 )
            (and
              (AboveOf13 ?x_6)
              (BelowOf3 ?x_6)
            )
          )
          (exists (?x_7 )
            (and
              (LeftOf6 ?x_7)
              (RightOf16 ?x_7)
            )
          )
          (exists (?x_25 )
            (and
              (LeftOf5 ?x_25)
              (RightOf18 ?x_25)
            )
          )
          (exists (?x_2 )
            (and
              (AboveOf14 ?x_2)
              (BelowOf6 ?x_2)
            )
          )
          (exists (?x_3 )
            (and
              (RightOf9 ?x_3)
              (LeftOf2 ?x_3)
            )
          )
          (exists (?x_26 )
            (and
              (BelowOf11 ?x_26)
              (AboveOf14 ?x_26)
            )
          )
          (exists (?x_23 )
            (and
              (AboveOf14 ?x_23)
              (BelowOf3 ?x_23)
            )
          )
          (exists (?x_18 )
            (and
              (AboveOf14 ?x_18)
              (BelowOf1 ?x_18)
            )
          )
          (exists (?x_16 )
            (and
              (BelowOf11 ?x_16)
              (AboveOf12 ?x_16)
            )
          )
          (exists (?x_29 )
            (and
              (AboveOf7 ?x_29)
              (BelowOf2 ?x_29)
            )
          )
          (exists (?x_8 )
            (and
              (LeftOf6 ?x_8)
              (RightOf14 ?x_8)
            )
          )
          (exists (?x_9 )
            (and
              (BelowOf4 ?x_9)
              (AboveOf9 ?x_9)
            )
          )
          (exists (?x_16 )
            (and
              (AboveOf17 ?x_16)
              (BelowOf4 ?x_16)
            )
          )
          (exists (?x_13 )
            (and
              (LeftOf1 ?x_13)
              (RightOf14 ?x_13)
            )
          )
          (exists (?x_30 )
            (and
              (LeftOf6 ?x_30)
              (RightOf14 ?x_30)
            )
          )
          (exists (?x_13 )
            (and
              (RightOf16 ?x_13)
              (LeftOf2 ?x_13)
            )
          )
          (exists (?x_16 )
            (and
              (BelowOf11 ?x_16)
              (AboveOf15 ?x_16)
            )
          )
          (exists (?x_18 )
            (and
              (BelowOf1 ?x_18)
              (AboveOf4 ?x_18)
            )
          )
          (exists (?x_4 )
            (and
              (LeftOf2 ?x_4)
              (RightOf11 ?x_4)
            )
          )
          (exists (?x_0 )
            (and
              (AboveOf11 ?x_0)
              (BelowOf10 ?x_0)
            )
          )
          (exists (?x_28 )
            (and
              (LeftOf5 ?x_28)
              (RightOf18 ?x_28)
            )
          )
          (exists (?x_6 )
            (and
              (AboveOf10 ?x_6)
              (BelowOf6 ?x_6)
            )
          )
          (exists (?x_24 )
            (and
              (RightOf8 ?x_24)
              (LeftOf1 ?x_24)
            )
          )
          (exists (?x_16 )
            (and
              (AboveOf14 ?x_16)
              (BelowOf9 ?x_16)
            )
          )
          (exists (?x_27 )
            (and
              (LeftOf1 ?x_27)
              (RightOf5 ?x_27)
            )
          )
          (exists (?x_9 )
            (and
              (BelowOf7 ?x_9)
              (AboveOf12 ?x_9)
            )
          )
          (exists (?x_6 )
            (and
              (AboveOf13 ?x_6)
              (BelowOf6 ?x_6)
            )
          )
          (exists (?x_32 )
            (and
              (LeftOf5 ?x_32)
              (RightOf18 ?x_32)
            )
          )
          (exists (?x_8 )
            (and
              (LeftOf7 ?x_8)
              (RightOf14 ?x_8)
            )
          )
          (exists (?x_35 )
            (and
              (AboveOf12 ?x_35)
              (BelowOf2 ?x_35)
            )
          )
          (exists (?x_29 )
            (and
              (AboveOf9 ?x_29)
              (BelowOf1 ?x_29)
            )
          )
          (exists (?x_10 )
            (and
              (RightOf18 ?x_10)
              (LeftOf14 ?x_10)
            )
          )
          (exists (?x_25 )
            (and
              (RightOf12 ?x_25)
              (LeftOf4 ?x_25)
            )
          )
          (exists (?x_8 )
            (and
              (LeftOf4 ?x_8)
              (RightOf10 ?x_8)
            )
          )
          (exists (?x_25 )
            (and
              (LeftOf6 ?x_25)
              (RightOf11 ?x_25)
            )
          )
          (exists (?x_25 )
            (and
              (LeftOf5 ?x_25)
              (RightOf10 ?x_25)
            )
          )
          (exists (?x_16 )
            (and
              (BelowOf2 ?x_16)
              (AboveOf12 ?x_16)
            )
          )
          (exists (?x_26 )
            (and
              (BelowOf10 ?x_26)
              (AboveOf14 ?x_26)
            )
          )
          (exists (?x_27 )
            (and
              (LeftOf1 ?x_27)
              (RightOf15 ?x_27)
            )
          )
          (exists (?x_15 )
            (and
              (BelowOf3 ?x_15)
              (AboveOf14 ?x_15)
            )
          )
          (exists (?x_16 )
            (and
              (BelowOf6 ?x_16)
              (AboveOf18 ?x_16)
            )
          )
          (exists (?x_15 )
            (and
              (BelowOf6 ?x_15)
              (AboveOf15 ?x_15)
            )
          )
          (exists (?x_35 )
            (and
              (AboveOf13 ?x_35)
              (BelowOf1 ?x_35)
            )
          )
          (exists (?x_23 )
            (and
              (AboveOf12 ?x_23)
              (BelowOf1 ?x_23)
            )
          )
          (exists (?x_25 )
            (and
              (LeftOf2 ?x_25)
              (RightOf16 ?x_25)
            )
          )
          (exists (?x_10 )
            (and
              (LeftOf13 ?x_10)
              (RightOf14 ?x_10)
            )
          )
          (exists (?x_7 )
            (and
              (RightOf18 ?x_7)
              (LeftOf12 ?x_7)
            )
          )
          (exists (?x_9 )
            (and
              (BelowOf5 ?x_9)
              (AboveOf10 ?x_9)
            )
          )
          (exists (?x_11 )
            (and
              (AboveOf17 ?x_11)
              (BelowOf12 ?x_11)
            )
          )
          (exists (?x_3 )
            (and
              (RightOf17 ?x_3)
              (LeftOf2 ?x_3)
            )
          )
          (exists (?x_26 )
            (and
              (BelowOf6 ?x_26)
              (AboveOf12 ?x_26)
            )
          )
          (exists (?x_15 )
            (and
              (AboveOf18 ?x_15)
              (BelowOf5 ?x_15)
            )
          )
          (exists (?x_0 )
            (and
              (AboveOf14 ?x_0)
              (BelowOf1 ?x_0)
            )
          )
          (exists (?x_1 )
            (and
              (BelowOf6 ?x_1)
              (AboveOf15 ?x_1)
            )
          )
          (exists (?x_34 )
            (and
              (RightOf17 ?x_34)
              (LeftOf3 ?x_34)
            )
          )
          (exists (?x_31 )
            (and
              (AboveOf16 ?x_31)
              (BelowOf4 ?x_31)
            )
          )
          (exists (?x_31 )
            (and
              (AboveOf15 ?x_31)
              (BelowOf6 ?x_31)
            )
          )
          (exists (?x_8 )
            (and
              (RightOf16 ?x_8)
              (LeftOf3 ?x_8)
            )
          )
          (exists (?x_30 )
            (and
              (RightOf14 ?x_30)
              (LeftOf9 ?x_30)
            )
          )
          (exists (?x_30 )
            (and
              (RightOf15 ?x_30)
              (LeftOf9 ?x_30)
            )
          )
          (exists (?x_3 )
            (and
              (RightOf15 ?x_3)
              (LeftOf3 ?x_3)
            )
          )
          (exists (?x_22 )
            (and
              (RightOf18 ?x_22)
              (LeftOf4 ?x_22)
            )
          )
          (exists (?x_22 )
            (and
              (RightOf18 ?x_22)
              (LeftOf1 ?x_22)
            )
          )
          (exists (?x_5 )
            (and
              (RightOf12 ?x_5)
              (LeftOf2 ?x_5)
            )
          )
          (exists (?x_31 )
            (and
              (AboveOf15 ?x_31)
              (BelowOf11 ?x_31)
            )
          )
          (exists (?x_30 )
            (and
              (RightOf18 ?x_30)
              (LeftOf9 ?x_30)
            )
          )
          (exists (?x_16 )
            (and
              (AboveOf12 ?x_16)
              (BelowOf4 ?x_16)
            )
          )
          (exists (?x_4 )
            (and
              (LeftOf2 ?x_4)
              (RightOf16 ?x_4)
            )
          )
          (exists (?x_20 )
            (and
              (LeftOf1 ?x_20)
              (RightOf14 ?x_20)
            )
          )
          (exists (?x_3 )
            (and
              (RightOf8 ?x_3)
              (LeftOf3 ?x_3)
            )
          )
          (exists (?x_7 )
            (and
              (RightOf17 ?x_7)
              (LeftOf7 ?x_7)
            )
          )
          (exists (?x_6 )
            (and
              (BelowOf3 ?x_6)
              (AboveOf12 ?x_6)
            )
          )
          (exists (?x_2 )
            (and
              (BelowOf4 ?x_2)
              (AboveOf16 ?x_2)
            )
          )
          (exists (?x_31 )
            (and
              (AboveOf16 ?x_31)
              (BelowOf5 ?x_31)
            )
          )
          (exists (?x_0 )
            (and
              (BelowOf3 ?x_0)
              (AboveOf12 ?x_0)
            )
          )
          (exists (?x_24 )
            (and
              (RightOf7 ?x_24)
              (LeftOf5 ?x_24)
            )
          )
          (exists (?x_27 )
            (and
              (RightOf14 ?x_27)
              (LeftOf2 ?x_27)
            )
          )
          (exists (?x_34 )
            (and
              (RightOf15 ?x_34)
              (LeftOf8 ?x_34)
            )
          )
          (exists (?x_25 )
            (and
              (LeftOf3 ?x_25)
              (RightOf16 ?x_25)
            )
          )
          (exists (?x_31 )
            (and
              (AboveOf13 ?x_31)
              (BelowOf7 ?x_31)
            )
          )
          (exists (?x_9 )
            (and
              (AboveOf17 ?x_9)
              (BelowOf1 ?x_9)
            )
          )
          (exists (?x_21 )
            (and
              (AboveOf13 ?x_21)
              (BelowOf1 ?x_21)
            )
          )
          (exists (?x_0 )
            (and
              (AboveOf15 ?x_0)
              (BelowOf1 ?x_0)
            )
          )
          (exists (?x_6 )
            (and
              (AboveOf10 ?x_6)
              (BelowOf3 ?x_6)
            )
          )
          (exists (?x_26 )
            (and
              (BelowOf9 ?x_26)
              (AboveOf12 ?x_26)
            )
          )
          (exists (?x_27 )
            (and
              (RightOf14 ?x_27)
              (LeftOf1 ?x_27)
            )
          )
          (exists (?x_24 )
            (and
              (LeftOf7 ?x_24)
              (RightOf8 ?x_24)
            )
          )
          (exists (?x_8 )
            (and
              (LeftOf3 ?x_8)
              (RightOf13 ?x_8)
            )
          )
          (exists (?x_5 )
            (and
              (RightOf18 ?x_5)
              (LeftOf8 ?x_5)
            )
          )
          (exists (?x_16 )
            (and
              (BelowOf4 ?x_16)
              (AboveOf15 ?x_16)
            )
          )
          (exists (?x_15 )
            (and
              (BelowOf6 ?x_15)
              (AboveOf16 ?x_15)
            )
          )
          (exists (?x_12 )
            (and
              (BelowOf8 ?x_12)
              (AboveOf16 ?x_12)
            )
          )
          (exists (?x_9 )
            (and
              (BelowOf2 ?x_9)
              (AboveOf10 ?x_9)
            )
          )
          (exists (?x_2 )
            (and
              (BelowOf5 ?x_2)
              (AboveOf17 ?x_2)
            )
          )
          (exists (?x_25 )
            (and
              (RightOf17 ?x_25)
              (LeftOf5 ?x_25)
            )
          )
          (exists (?x_5 )
            (and
              (LeftOf5 ?x_5)
              (RightOf18 ?x_5)
            )
          )
          (exists (?x_16 )
            (and
              (AboveOf13 ?x_16)
              (BelowOf3 ?x_16)
            )
          )
          (exists (?x_5 )
            (and
              (RightOf11 ?x_5)
              (LeftOf3 ?x_5)
            )
          )
          (exists (?x_7 )
            (and
              (LeftOf10 ?x_7)
              (RightOf16 ?x_7)
            )
          )
          (exists (?x_23 )
            (and
              (AboveOf14 ?x_23)
              (BelowOf1 ?x_23)
            )
          )
          (exists (?x_21 )
            (and
              (BelowOf3 ?x_21)
              (AboveOf9 ?x_21)
            )
          )
          (exists (?x_35 )
            (and
              (BelowOf8 ?x_35)
              (AboveOf18 ?x_35)
            )
          )
          (exists (?x_7 )
            (and
              (RightOf16 ?x_7)
              (LeftOf12 ?x_7)
            )
          )
          (exists (?x_6 )
            (and
              (AboveOf13 ?x_6)
              (BelowOf1 ?x_6)
            )
          )
          (exists (?x_35 )
            (and
              (BelowOf6 ?x_35)
              (AboveOf14 ?x_35)
            )
          )
          (exists (?x_6 )
            (and
              (BelowOf1 ?x_6)
              (AboveOf18 ?x_6)
            )
          )
          (exists (?x_2 )
            (and
              (AboveOf14 ?x_2)
              (BelowOf7 ?x_2)
            )
          )
          (exists (?x_15 )
            (and
              (BelowOf6 ?x_15)
              (AboveOf17 ?x_15)
            )
          )
          (exists (?x_7 )
            (and
              (RightOf17 ?x_7)
              (LeftOf5 ?x_7)
            )
          )
          (exists (?x_5 )
            (and
              (RightOf16 ?x_5)
              (LeftOf7 ?x_5)
            )
          )
          (exists (?x_17 )
            (and
              (AboveOf8 ?x_17)
              (BelowOf1 ?x_17)
            )
          )
          (exists (?x_26 )
            (and
              (BelowOf6 ?x_26)
              (AboveOf14 ?x_26)
            )
          )
          (exists (?x_25 )
            (and
              (RightOf17 ?x_25)
              (LeftOf3 ?x_25)
            )
          )
          (exists (?x_11 )
            (and
              (AboveOf17 ?x_11)
              (BelowOf1 ?x_11)
            )
          )
          (exists (?x_15 )
            (and
              (BelowOf6 ?x_15)
              (AboveOf10 ?x_15)
            )
          )
          (exists (?x_24 )
            (and
              (RightOf9 ?x_24)
              (LeftOf5 ?x_24)
            )
          )
          (exists (?x_5 )
            (and
              (RightOf14 ?x_5)
              (LeftOf5 ?x_5)
            )
          )
          (exists (?x_25 )
            (and
              (RightOf10 ?x_25)
              (LeftOf4 ?x_25)
            )
          )
          (exists (?x_6 )
            (and
              (BelowOf5 ?x_6)
              (AboveOf17 ?x_6)
            )
          )
          (exists (?x_15 )
            (and
              (AboveOf18 ?x_15)
              (BelowOf7 ?x_15)
            )
          )
          (exists (?x_8 )
            (and
              (LeftOf5 ?x_8)
              (RightOf9 ?x_8)
            )
          )
          (exists (?x_22 )
            (and
              (RightOf16 ?x_22)
              (LeftOf7 ?x_22)
            )
          )
          (exists (?x_34 )
            (and
              (LeftOf4 ?x_34)
              (RightOf12 ?x_34)
            )
          )
          (exists (?x_17 )
            (and
              (AboveOf11 ?x_17)
              (BelowOf1 ?x_17)
            )
          )
          (exists (?x_26 )
            (and
              (AboveOf16 ?x_26)
              (BelowOf11 ?x_26)
            )
          )
          (exists (?x_22 )
            (and
              (RightOf15 ?x_22)
              (LeftOf6 ?x_22)
            )
          )
          (exists (?x_29 )
            (and
              (AboveOf16 ?x_29)
              (BelowOf2 ?x_29)
            )
          )
          (exists (?x_15 )
            (and
              (BelowOf6 ?x_15)
              (AboveOf18 ?x_15)
            )
          )
          (exists (?x_0 )
            (and
              (AboveOf12 ?x_0)
              (BelowOf10 ?x_0)
            )
          )
          (exists (?x_5 )
            (and
              (LeftOf4 ?x_5)
              (RightOf10 ?x_5)
            )
          )
          (exists (?x_26 )
            (and
              (BelowOf3 ?x_26)
              (AboveOf14 ?x_26)
            )
          )
          (exists (?x_16 )
            (and
              (AboveOf14 ?x_16)
              (BelowOf1 ?x_16)
            )
          )
          (exists (?x_34 )
            (and
              (LeftOf3 ?x_34)
              (RightOf9 ?x_34)
            )
          )
          (exists (?x_0 )
            (and
              (BelowOf5 ?x_0)
              (AboveOf16 ?x_0)
            )
          )
          (exists (?x_27 )
            (and
              (LeftOf3 ?x_27)
              (RightOf5 ?x_27)
            )
          )
          (exists (?x_5 )
            (and
              (RightOf15 ?x_5)
              (LeftOf4 ?x_5)
            )
          )
          (exists (?x_10 )
            (and
              (RightOf17 ?x_10)
              (LeftOf13 ?x_10)
            )
          )
          (exists (?x_25 )
            (and
              (LeftOf3 ?x_25)
              (RightOf14 ?x_25)
            )
          )
          (exists (?x_5 )
            (and
              (RightOf16 ?x_5)
              (LeftOf8 ?x_5)
            )
          )
          (exists (?x_9 )
            (and
              (BelowOf3 ?x_9)
              (AboveOf12 ?x_9)
            )
          )
          (exists (?x_5 )
            (and
              (LeftOf3 ?x_5)
              (RightOf17 ?x_5)
            )
          )
          (exists (?x_8 )
            (and
              (RightOf15 ?x_8)
              (LeftOf2 ?x_8)
            )
          )
          (exists (?x_19 )
            (and
              (LeftOf8 ?x_19)
              (RightOf14 ?x_19)
            )
          )
          (exists (?x_0 )
            (and
              (AboveOf11 ?x_0)
              (BelowOf8 ?x_0)
            )
          )
          (exists (?x_23 )
            (and
              (AboveOf6 ?x_23)
              (BelowOf3 ?x_23)
            )
          )
          (exists (?x_17 )
            (and
              (BelowOf1 ?x_17)
              (AboveOf10 ?x_17)
            )
          )
          (exists (?x_19 )
            (and
              (RightOf15 ?x_19)
              (LeftOf11 ?x_19)
            )
          )
          (exists (?x_27 )
            (and
              (RightOf9 ?x_27)
              (LeftOf2 ?x_27)
            )
          )
          (exists (?x_5 )
            (and
              (LeftOf1 ?x_5)
              (RightOf13 ?x_5)
            )
          )
          (exists (?x_5 )
            (and
              (LeftOf6 ?x_5)
              (RightOf17 ?x_5)
            )
          )
          (exists (?x_5 )
            (and
              (RightOf11 ?x_5)
              (LeftOf2 ?x_5)
            )
          )
          (exists (?x_15 )
            (and
              (AboveOf18 ?x_15)
              (BelowOf6 ?x_15)
            )
          )
          (exists (?x_25 )
            (and
              (RightOf13 ?x_25)
              (LeftOf1 ?x_25)
            )
          )
          (exists (?x_4 )
            (and
              (LeftOf2 ?x_4)
              (RightOf5 ?x_4)
            )
          )
          (exists (?x_28 )
            (and
              (LeftOf13 ?x_28)
              (RightOf18 ?x_28)
            )
          )
          (exists (?x_24 )
            (and
              (RightOf7 ?x_24)
              (LeftOf4 ?x_24)
            )
          )
          (exists (?x_1 )
            (and
              (AboveOf18 ?x_1)
              (BelowOf11 ?x_1)
            )
          )
          (exists (?x_30 )
            (and
              (RightOf15 ?x_30)
              (LeftOf11 ?x_30)
            )
          )
          (exists (?x_15 )
            (and
              (AboveOf12 ?x_15)
              (BelowOf5 ?x_15)
            )
          )
          (exists (?x_22 )
            (and
              (LeftOf9 ?x_22)
              (RightOf12 ?x_22)
            )
          )
          (exists (?x_31 )
            (and
              (BelowOf2 ?x_31)
              (AboveOf18 ?x_31)
            )
          )
          (exists (?x_18 )
            (and
              (AboveOf15 ?x_18)
              (BelowOf1 ?x_18)
            )
          )
          (exists (?x_25 )
            (and
              (RightOf13 ?x_25)
              (LeftOf2 ?x_25)
            )
          )
          (exists (?x_5 )
            (and
              (LeftOf4 ?x_5)
              (RightOf18 ?x_5)
            )
          )
          (exists (?x_20 )
            (and
              (LeftOf1 ?x_20)
              (RightOf7 ?x_20)
            )
          )
          (exists (?x_19 )
            (and
              (LeftOf5 ?x_19)
              (RightOf12 ?x_19)
            )
          )
          (exists (?x_1 )
            (and
              (BelowOf14 ?x_1)
              (AboveOf15 ?x_1)
            )
          )
          (exists (?x_6 )
            (and
              (BelowOf2 ?x_6)
              (AboveOf8 ?x_6)
            )
          )
          (exists (?x_2 )
            (and
              (AboveOf17 ?x_2)
              (BelowOf10 ?x_2)
            )
          )
          (exists (?x_1 )
            (and
              (BelowOf4 ?x_1)
              (AboveOf15 ?x_1)
            )
          )
          (exists (?x_35 )
            (and
              (AboveOf16 ?x_35)
              (BelowOf1 ?x_35)
            )
          )
          (exists (?x_35 )
            (and
              (BelowOf8 ?x_35)
              (AboveOf15 ?x_35)
            )
          )
          (exists (?x_16 )
            (and
              (BelowOf5 ?x_16)
              (AboveOf12 ?x_16)
            )
          )
          (exists (?x_27 )
            (and
              (LeftOf5 ?x_27)
              (RightOf15 ?x_27)
            )
          )
          (exists (?x_0 )
            (and
              (AboveOf10 ?x_0)
              (BelowOf2 ?x_0)
            )
          )
          (exists (?x_8 )
            (and
              (LeftOf1 ?x_8)
              (RightOf13 ?x_8)
            )
          )
          (exists (?x_2 )
            (and
              (AboveOf17 ?x_2)
              (BelowOf6 ?x_2)
            )
          )
          (exists (?x_13 )
            (and
              (LeftOf4 ?x_13)
              (RightOf15 ?x_13)
            )
          )
          (exists (?x_19 )
            (and
              (RightOf18 ?x_19)
              (LeftOf4 ?x_19)
            )
          )
          (exists (?x_13 )
            (and
              (LeftOf4 ?x_13)
              (RightOf18 ?x_13)
            )
          )
          (exists (?x_21 )
            (and
              (AboveOf17 ?x_21)
              (BelowOf3 ?x_21)
            )
          )
          (exists (?x_25 )
            (and
              (RightOf11 ?x_25)
              (LeftOf4 ?x_25)
            )
          )
          (exists (?x_1 )
            (and
              (BelowOf8 ?x_1)
              (AboveOf16 ?x_1)
            )
          )
          (exists (?x_13 )
            (and
              (RightOf17 ?x_13)
              (LeftOf3 ?x_13)
            )
          )
          (exists (?x_27 )
            (and
              (LeftOf5 ?x_27)
              (RightOf17 ?x_27)
            )
          )
        )
        (Error)
      )
    )
  )
)