(define (problem robotProblem)
	(:domain robot)
	(:objects robot)
	(:init
		(RightOf2 robot)
		(LeftOf18 robot)
		(AboveOf0 robot)
		(BelowOf18 robot)
	)
	(:goal (mko (and (Column2 robot) (Row1 robot))))

)