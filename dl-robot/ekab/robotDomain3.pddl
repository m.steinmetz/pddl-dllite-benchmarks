(define (domain robot)
	(:requirements :ekab)
	(:predicates
		(Columns ?x)
		(Rows ?x)
		(Column0 ?x)
		(Column1 ?x)
		(Column2 ?x)
		(RightOf0 ?x)
		(RightOf1 ?x)
		(RightOf2 ?x)
		(LeftOf1 ?x)
		(LeftOf2 ?x)
		(LeftOf3 ?x)
		(Row0 ?x)
		(Row1 ?x)
		(Row2 ?x)
		(AboveOf0 ?x)
		(AboveOf1 ?x)
		(AboveOf2 ?x)
		(BelowOf1 ?x)
		(BelowOf2 ?x)
		(BelowOf3 ?x)
	)
	(:axioms
		(isA RightOf0 Columns)
		(isA RightOf1 RightOf0)
		(isA RightOf2 RightOf1)
		(isA LeftOf3 Columns)
		(isA LeftOf2 LeftOf3)
		(isA LeftOf1 LeftOf2)
		(isA AboveOf0 Rows)
		(isA AboveOf1 AboveOf0)
		(isA AboveOf2 AboveOf1)
		(isA BelowOf3 Rows)
		(isA BelowOf2 BelowOf3)
		(isA BelowOf1 BelowOf2)
		(isA LeftOf1 (not RightOf1))
		(isA LeftOf2 (not RightOf2))
		(isA AboveOf1 (not BelowOf1))
		(isA AboveOf2 (not BelowOf2))
	)
	(:rule ruleRight
		:condition (mko(Columns ?x))
		:action moveRight
	)
	(:rule ruleLeft
		:condition (mko(Columns ?x))
		:action moveLeft
	)
	(:rule ruleUp
		:condition (mko(Rows ?x))
		:action moveUp
	)
	(:rule ruleDown
		:condition (mko(Rows ?x))
		:action moveDown
	)
	(:action moveRight
		:parameters (?x)
		:effects 
		(
		:condition (mko (RightOf0 ?x))
		:add ((RightOf1 ?x))
		)
		(
		:condition (mko (RightOf1 ?x))
		:add ((RightOf2 ?x))
		)
		(
		:condition (mko (LeftOf1 ?x))
		:add ((LeftOf2 ?x))
		:delete ((LeftOf1 ?x))
		)
		(
		:condition (mko (LeftOf2 ?x))
		:add ((LeftOf3 ?x))
		:delete ((LeftOf2 ?x))
		)
		(
		:condition (mko (Column0 ?x))
		:add ((Column1 ?x))
		:delete ((Column0 ?x))
		)
		(
		:condition (mko (Column1 ?x))
		:add ((Column2 ?x))
		:delete ((Column1 ?x))
		)
		(
		:condition (mko (and (RightOf0 ?x) (LeftOf1 ?x)))
		:add ((Column1 ?x))
		)
		(
		:condition (mko (and (RightOf1 ?x) (LeftOf2 ?x)))
		:add ((Column2 ?x))
		)
	)
	(:action moveLeft
		:parameters (?x)
		:effects 
		(
		:condition (mko (LeftOf2 ?x))
		:add ((LeftOf1 ?x))
		)
		(
		:condition (mko (LeftOf3 ?x))
		:add ((LeftOf2 ?x))
		)
		(
		:condition (mko (RightOf1 ?x))
		:add ((RightOf0 ?x))
		:delete ((RightOf1 ?x))
		)
		(
		:condition (mko (RightOf2 ?x))
		:add ((RightOf1 ?x))
		:delete ((RightOf2 ?x))
		)
		(
		:condition (mko (Column1 ?x))
		:add ((Column0 ?x))
		:delete ((Column1 ?x))
		)
		(
		:condition (mko (Column2 ?x))
		:add ((Column1 ?x))
		:delete ((Column2 ?x))
		)
		(
		:condition (mko (and (RightOf1 ?x) (LeftOf2 ?x)))
		:add ((Column0 ?x))
		)
		(
		:condition (mko (and (RightOf2 ?x) (LeftOf3 ?x)))
		:add ((Column1 ?x))
		)
	)
	(:action moveUp
		:parameters (?x)
		:effects 
		(
		:condition (mko (AboveOf0 ?x))
		:add ((AboveOf1 ?x))
		)
		(
		:condition (mko (AboveOf1 ?x))
		:add ((AboveOf2 ?x))
		)
		(
		:condition (mko (BelowOf1 ?x))
		:add ((BelowOf2 ?x))
		:delete ((BelowOf1 ?x))
		)
		(
		:condition (mko (BelowOf2 ?x))
		:add ((BelowOf3 ?x))
		:delete ((BelowOf2 ?x))
		)
		(
		:condition (mko (Row0 ?x))
		:add ((Row1 ?x))
		:delete ((Row0 ?x))
		)
		(
		:condition (mko (Row1 ?x))
		:add ((Row2 ?x))
		:delete ((Row1 ?x))
		)
		(
		:condition (mko (and (AboveOf0 ?x) (BelowOf1 ?x)))
		:add ((Row1 ?x))
		)
		(
		:condition (mko (and (AboveOf1 ?x) (BelowOf2 ?x)))
		:add ((Row2 ?x))
		)
	)
	(:action moveDown
		:parameters (?x)
		:effects 
		(
		:condition (mko (BelowOf2 ?x))
		:add ((BelowOf1 ?x))
		)
		(
		:condition (mko (BelowOf3 ?x))
		:add ((BelowOf2 ?x))
		)
		(
		:condition (mko (AboveOf1 ?x))
		:add ((AboveOf0 ?x))
		:delete ((AboveOf1 ?x))
		)
		(
		:condition (mko (AboveOf2 ?x))
		:add ((AboveOf1 ?x))
		:delete ((AboveOf2 ?x))
		)
		(
		:condition (mko (Row1 ?x))
		:add ((Row0 ?x))
		:delete ((Row1 ?x))
		)
		(
		:condition (mko (Row2 ?x))
		:add ((Row1 ?x))
		:delete ((Row2 ?x))
		)
		(
		:condition (mko (and (AboveOf1 ?x) (BelowOf2 ?x)))
		:add ((Row0 ?x))
		)
		(
		:condition (mko (and (AboveOf2 ?x) (BelowOf3 ?x)))
		:add ((Row1 ?x))
		)
	)

)