(define (domain grounded-SIMPLE-ADL-ROBOT)
(:requirements
:strips
)
(:predicates
(Flag213-)
(Flag212-)
(Flag195-)
(Flag33-)
(Flag7-)
(Flag6-)
(Flag5-)
(Flag3-)
(Flag2-)
(ROW14-ROBOT)
(ROW13-ROBOT)
(ROW12-ROBOT)
(ROW11-ROBOT)
(ROW10-ROBOT)
(ROW9-ROBOT)
(ROW8-ROBOT)
(ROW7-ROBOT)
(ROW6-ROBOT)
(ROW5-ROBOT)
(ROW4-ROBOT)
(ROW3-ROBOT)
(ROW2-ROBOT)
(ROW1-ROBOT)
(ROW0-ROBOT)
(ABOVEOF14-ROBOT)
(ABOVEOF13-ROBOT)
(ABOVEOF12-ROBOT)
(ABOVEOF11-ROBOT)
(ABOVEOF10-ROBOT)
(ABOVEOF9-ROBOT)
(ABOVEOF8-ROBOT)
(ABOVEOF7-ROBOT)
(ABOVEOF6-ROBOT)
(ABOVEOF5-ROBOT)
(ABOVEOF4-ROBOT)
(ABOVEOF3-ROBOT)
(ABOVEOF2-ROBOT)
(ABOVEOF1-ROBOT)
(BELOWOF14-ROBOT)
(BELOWOF13-ROBOT)
(BELOWOF12-ROBOT)
(BELOWOF11-ROBOT)
(BELOWOF10-ROBOT)
(BELOWOF9-ROBOT)
(BELOWOF8-ROBOT)
(BELOWOF7-ROBOT)
(BELOWOF6-ROBOT)
(BELOWOF5-ROBOT)
(BELOWOF4-ROBOT)
(BELOWOF3-ROBOT)
(BELOWOF2-ROBOT)
(BELOWOF1-ROBOT)
(CHECKCONSISTENCY-)
(ROW15-ROBOT)
(BELOWOF16-ROBOT)
(NOT-BELOWOF15-ROBOT)
(ABOVEOF15-ROBOT)
(Flag391-)
(Flag389-)
(Flag387-)
(Flag386-)
(Flag384-)
(Flag383-)
(Flag382-)
(Flag381-)
(Flag379-)
(Flag378-)
(Flag377-)
(Flag376-)
(Flag375-)
(Flag373-)
(Flag372-)
(Flag371-)
(Flag370-)
(Flag369-)
(Flag367-)
(Flag366-)
(Flag365-)
(Flag364-)
(Flag363-)
(Flag362-)
(Flag360-)
(Flag359-)
(Flag358-)
(Flag357-)
(Flag356-)
(Flag355-)
(Flag354-)
(Flag353-)
(Flag351-)
(Flag350-)
(Flag349-)
(Flag348-)
(Flag347-)
(Flag346-)
(Flag345-)
(Flag344-)
(Flag343-)
(Flag342-)
(Flag340-)
(Flag339-)
(Flag338-)
(Flag337-)
(Flag336-)
(Flag335-)
(Flag334-)
(Flag333-)
(Flag332-)
(Flag331-)
(Flag329-)
(Flag328-)
(Flag327-)
(Flag326-)
(Flag325-)
(Flag324-)
(Flag323-)
(Flag322-)
(Flag321-)
(Flag320-)
(Flag319-)
(Flag318-)
(Flag316-)
(Flag315-)
(Flag314-)
(Flag313-)
(Flag312-)
(Flag311-)
(Flag310-)
(Flag309-)
(Flag308-)
(Flag307-)
(Flag306-)
(Flag305-)
(Flag304-)
(Flag302-)
(Flag301-)
(Flag300-)
(Flag299-)
(Flag298-)
(Flag297-)
(Flag296-)
(Flag295-)
(Flag294-)
(Flag293-)
(Flag292-)
(Flag291-)
(Flag289-)
(Flag288-)
(Flag287-)
(Flag286-)
(Flag285-)
(Flag284-)
(Flag283-)
(Flag282-)
(Flag281-)
(Flag280-)
(Flag279-)
(Flag278-)
(Flag277-)
(Flag276-)
(Flag275-)
(Flag273-)
(Flag272-)
(Flag271-)
(Flag270-)
(Flag269-)
(Flag268-)
(Flag267-)
(Flag266-)
(Flag265-)
(Flag264-)
(Flag263-)
(Flag262-)
(Flag261-)
(Flag260-)
(Flag259-)
(Flag257-)
(Flag256-)
(Flag255-)
(Flag254-)
(Flag253-)
(Flag252-)
(Flag251-)
(Flag250-)
(Flag249-)
(Flag248-)
(Flag247-)
(Flag246-)
(Flag245-)
(Flag244-)
(Flag243-)
(Flag242-)
(Flag241-)
(Flag240-)
(Flag239-)
(Flag238-)
(Flag237-)
(Flag236-)
(Flag235-)
(Flag234-)
(Flag233-)
(Flag232-)
(Flag231-)
(Flag230-)
(Flag229-)
(Flag228-)
(Flag227-)
(Flag226-)
(Flag225-)
(Flag224-)
(Flag223-)
(Flag222-)
(Flag221-)
(Flag220-)
(Flag219-)
(Flag218-)
(Flag217-)
(Flag216-)
(Flag215-)
(Flag214-)
(Flag211-)
(Flag210-)
(Flag209-)
(Flag208-)
(Flag207-)
(Flag206-)
(Flag205-)
(Flag204-)
(Flag203-)
(Flag202-)
(Flag201-)
(Flag200-)
(Flag199-)
(Flag4-)
(ERROR-)
(COLUMN15-ROBOT)
(COLUMN14-ROBOT)
(COLUMN13-ROBOT)
(COLUMN12-ROBOT)
(COLUMN11-ROBOT)
(COLUMN10-ROBOT)
(COLUMN9-ROBOT)
(COLUMN8-ROBOT)
(COLUMN7-ROBOT)
(COLUMN6-ROBOT)
(COLUMN5-ROBOT)
(COLUMN4-ROBOT)
(COLUMN3-ROBOT)
(COLUMN2-ROBOT)
(COLUMN1-ROBOT)
(LEFTOF16-ROBOT)
(NOT-LEFTOF15-ROBOT)
(LEFTOF14-ROBOT)
(LEFTOF13-ROBOT)
(LEFTOF12-ROBOT)
(LEFTOF11-ROBOT)
(LEFTOF10-ROBOT)
(LEFTOF9-ROBOT)
(LEFTOF8-ROBOT)
(LEFTOF7-ROBOT)
(LEFTOF6-ROBOT)
(LEFTOF5-ROBOT)
(LEFTOF4-ROBOT)
(LEFTOF3-ROBOT)
(LEFTOF2-ROBOT)
(RIGHTOF15-ROBOT)
(RIGHTOF14-ROBOT)
(RIGHTOF13-ROBOT)
(RIGHTOF12-ROBOT)
(RIGHTOF11-ROBOT)
(RIGHTOF10-ROBOT)
(RIGHTOF9-ROBOT)
(RIGHTOF8-ROBOT)
(RIGHTOF7-ROBOT)
(RIGHTOF6-ROBOT)
(RIGHTOF5-ROBOT)
(RIGHTOF4-ROBOT)
(RIGHTOF3-ROBOT)
(RIGHTOF1-ROBOT)
(COLUMN0-ROBOT)
(NOT-RIGHTOF2-ROBOT)
(RIGHTOF0-ROBOT)
(LEFTOF1-ROBOT)
(Flag390-)
(Flag388-)
(Flag385-)
(Flag380-)
(Flag374-)
(Flag368-)
(Flag361-)
(Flag352-)
(Flag341-)
(Flag330-)
(Flag317-)
(Flag303-)
(Flag290-)
(Flag274-)
(Flag258-)
(Flag197-)
(Flag196-)
(Flag193-)
(Flag192-)
(Flag190-)
(Flag189-)
(Flag188-)
(Flag186-)
(Flag185-)
(Flag184-)
(Flag183-)
(Flag182-)
(Flag180-)
(Flag179-)
(Flag178-)
(Flag177-)
(Flag176-)
(Flag174-)
(Flag173-)
(Flag172-)
(Flag171-)
(Flag170-)
(Flag169-)
(Flag168-)
(Flag166-)
(Flag165-)
(Flag164-)
(Flag163-)
(Flag162-)
(Flag161-)
(Flag160-)
(Flag158-)
(Flag157-)
(Flag156-)
(Flag155-)
(Flag154-)
(Flag153-)
(Flag152-)
(Flag151-)
(Flag150-)
(Flag148-)
(Flag147-)
(Flag146-)
(Flag145-)
(Flag144-)
(Flag143-)
(Flag142-)
(Flag141-)
(Flag140-)
(Flag139-)
(Flag137-)
(Flag136-)
(Flag135-)
(Flag134-)
(Flag133-)
(Flag132-)
(Flag131-)
(Flag130-)
(Flag129-)
(Flag128-)
(Flag127-)
(Flag126-)
(Flag124-)
(Flag123-)
(Flag122-)
(Flag121-)
(Flag120-)
(Flag119-)
(Flag118-)
(Flag117-)
(Flag116-)
(Flag115-)
(Flag114-)
(Flag112-)
(Flag111-)
(Flag110-)
(Flag109-)
(Flag108-)
(Flag107-)
(Flag106-)
(Flag105-)
(Flag104-)
(Flag103-)
(Flag102-)
(Flag101-)
(Flag100-)
(Flag99-)
(Flag97-)
(Flag96-)
(Flag95-)
(Flag94-)
(Flag93-)
(Flag92-)
(Flag91-)
(Flag90-)
(Flag89-)
(Flag88-)
(Flag87-)
(Flag86-)
(Flag85-)
(Flag84-)
(Flag83-)
(Flag81-)
(Flag80-)
(Flag79-)
(Flag78-)
(Flag77-)
(Flag76-)
(Flag75-)
(Flag74-)
(Flag73-)
(Flag72-)
(Flag71-)
(Flag70-)
(Flag69-)
(Flag68-)
(Flag66-)
(Flag65-)
(Flag64-)
(Flag63-)
(Flag62-)
(Flag61-)
(Flag60-)
(Flag59-)
(Flag58-)
(Flag57-)
(Flag56-)
(Flag55-)
(Flag54-)
(Flag53-)
(Flag52-)
(Flag51-)
(Flag49-)
(Flag48-)
(Flag47-)
(Flag46-)
(Flag45-)
(Flag44-)
(Flag43-)
(Flag42-)
(Flag41-)
(Flag40-)
(Flag39-)
(Flag38-)
(Flag37-)
(Flag36-)
(Flag35-)
(Flag34-)
(Flag32-)
(Flag31-)
(Flag30-)
(Flag29-)
(Flag28-)
(Flag27-)
(Flag26-)
(Flag25-)
(Flag24-)
(Flag23-)
(Flag22-)
(Flag21-)
(Flag20-)
(Flag19-)
(Flag18-)
(Flag17-)
(Flag16-)
(Flag15-)
(Flag14-)
(Flag13-)
(Flag12-)
(Flag11-)
(Flag10-)
(Flag9-)
(Flag8-)
(Flag1-)
(Flag198-)
(Flag194-)
(Flag191-)
(Flag187-)
(Flag181-)
(Flag175-)
(Flag167-)
(Flag159-)
(Flag149-)
(Flag138-)
(Flag125-)
(Flag113-)
(Flag98-)
(Flag82-)
(Flag67-)
(Flag50-)
(NOT-COLUMN14-ROBOT)
(NOT-COLUMN13-ROBOT)
(NOT-COLUMN12-ROBOT)
(NOT-COLUMN11-ROBOT)
(NOT-COLUMN10-ROBOT)
(NOT-COLUMN9-ROBOT)
(NOT-COLUMN8-ROBOT)
(NOT-COLUMN7-ROBOT)
(NOT-COLUMN6-ROBOT)
(NOT-COLUMN5-ROBOT)
(NOT-COLUMN4-ROBOT)
(NOT-COLUMN3-ROBOT)
(NOT-COLUMN2-ROBOT)
(NOT-COLUMN1-ROBOT)
(NOT-COLUMN0-ROBOT)
(NOT-RIGHTOF14-ROBOT)
(NOT-RIGHTOF13-ROBOT)
(NOT-RIGHTOF12-ROBOT)
(NOT-RIGHTOF11-ROBOT)
(NOT-RIGHTOF10-ROBOT)
(NOT-RIGHTOF9-ROBOT)
(NOT-RIGHTOF8-ROBOT)
(NOT-RIGHTOF7-ROBOT)
(NOT-RIGHTOF6-ROBOT)
(NOT-RIGHTOF5-ROBOT)
(NOT-RIGHTOF4-ROBOT)
(NOT-RIGHTOF3-ROBOT)
(NOT-RIGHTOF1-ROBOT)
(RIGHTOF2-ROBOT)
(NOT-RIGHTOF0-ROBOT)
(NOT-LEFTOF14-ROBOT)
(NOT-LEFTOF13-ROBOT)
(NOT-LEFTOF12-ROBOT)
(NOT-LEFTOF11-ROBOT)
(NOT-LEFTOF10-ROBOT)
(NOT-LEFTOF9-ROBOT)
(NOT-LEFTOF8-ROBOT)
(NOT-LEFTOF7-ROBOT)
(NOT-LEFTOF6-ROBOT)
(NOT-LEFTOF5-ROBOT)
(NOT-LEFTOF4-ROBOT)
(NOT-LEFTOF3-ROBOT)
(NOT-LEFTOF2-ROBOT)
(NOT-LEFTOF1-ROBOT)
(NOT-CHECKCONSISTENCY-)
(NOT-COLUMN15-ROBOT)
(NOT-LEFTOF16-ROBOT)
(LEFTOF15-ROBOT)
(NOT-RIGHTOF15-ROBOT)
(NOT-ERROR-)
(NOT-ROW15-ROBOT)
(NOT-ROW14-ROBOT)
(NOT-ROW13-ROBOT)
(NOT-ROW12-ROBOT)
(NOT-ROW11-ROBOT)
(NOT-ROW10-ROBOT)
(NOT-ROW9-ROBOT)
(NOT-ROW8-ROBOT)
(NOT-ROW7-ROBOT)
(NOT-ROW6-ROBOT)
(NOT-ROW5-ROBOT)
(NOT-ROW4-ROBOT)
(NOT-ROW3-ROBOT)
(NOT-ROW2-ROBOT)
(NOT-ROW1-ROBOT)
(NOT-BELOWOF16-ROBOT)
(BELOWOF15-ROBOT)
(NOT-BELOWOF14-ROBOT)
(NOT-BELOWOF13-ROBOT)
(NOT-BELOWOF12-ROBOT)
(NOT-BELOWOF11-ROBOT)
(NOT-BELOWOF10-ROBOT)
(NOT-BELOWOF9-ROBOT)
(NOT-BELOWOF8-ROBOT)
(NOT-BELOWOF7-ROBOT)
(NOT-BELOWOF6-ROBOT)
(NOT-BELOWOF5-ROBOT)
(NOT-BELOWOF4-ROBOT)
(NOT-BELOWOF3-ROBOT)
(NOT-BELOWOF2-ROBOT)
(NOT-ABOVEOF15-ROBOT)
(NOT-ABOVEOF14-ROBOT)
(NOT-ABOVEOF13-ROBOT)
(NOT-ABOVEOF12-ROBOT)
(NOT-ABOVEOF11-ROBOT)
(NOT-ABOVEOF10-ROBOT)
(NOT-ABOVEOF9-ROBOT)
(NOT-ABOVEOF8-ROBOT)
(NOT-ABOVEOF7-ROBOT)
(NOT-ABOVEOF6-ROBOT)
(NOT-ABOVEOF5-ROBOT)
(NOT-ABOVEOF4-ROBOT)
(NOT-ABOVEOF3-ROBOT)
(NOT-ABOVEOF2-ROBOT)
(NOT-ABOVEOF1-ROBOT)
(NOT-ROW0-ROBOT)
(NOT-BELOWOF1-ROBOT)
)
(:derived (Flag50-)(and (Flag34-)))

(:derived (Flag50-)(and (Flag35-)))

(:derived (Flag50-)(and (Flag36-)))

(:derived (Flag50-)(and (Flag37-)))

(:derived (Flag50-)(and (Flag38-)))

(:derived (Flag50-)(and (Flag39-)))

(:derived (Flag50-)(and (Flag40-)))

(:derived (Flag50-)(and (Flag41-)))

(:derived (Flag50-)(and (Flag42-)))

(:derived (Flag50-)(and (Flag43-)))

(:derived (Flag50-)(and (Flag44-)))

(:derived (Flag50-)(and (Flag45-)))

(:derived (Flag50-)(and (Flag46-)))

(:derived (Flag50-)(and (Flag47-)))

(:derived (Flag50-)(and (Flag48-)))

(:derived (Flag50-)(and (Flag49-)))

(:derived (Flag67-)(and (Flag51-)))

(:derived (Flag67-)(and (Flag52-)))

(:derived (Flag67-)(and (Flag53-)))

(:derived (Flag67-)(and (Flag37-)))

(:derived (Flag67-)(and (Flag40-)))

(:derived (Flag67-)(and (Flag41-)))

(:derived (Flag67-)(and (Flag49-)))

(:derived (Flag67-)(and (Flag54-)))

(:derived (Flag67-)(and (Flag55-)))

(:derived (Flag67-)(and (Flag39-)))

(:derived (Flag67-)(and (Flag56-)))

(:derived (Flag67-)(and (Flag57-)))

(:derived (Flag67-)(and (Flag45-)))

(:derived (Flag67-)(and (Flag58-)))

(:derived (Flag67-)(and (Flag59-)))

(:derived (Flag67-)(and (Flag34-)))

(:derived (Flag67-)(and (Flag60-)))

(:derived (Flag67-)(and (Flag35-)))

(:derived (Flag67-)(and (Flag36-)))

(:derived (Flag67-)(and (Flag61-)))

(:derived (Flag67-)(and (Flag43-)))

(:derived (Flag67-)(and (Flag46-)))

(:derived (Flag67-)(and (Flag38-)))

(:derived (Flag67-)(and (Flag62-)))

(:derived (Flag67-)(and (Flag63-)))

(:derived (Flag67-)(and (Flag64-)))

(:derived (Flag67-)(and (Flag44-)))

(:derived (Flag67-)(and (Flag47-)))

(:derived (Flag67-)(and (Flag48-)))

(:derived (Flag67-)(and (Flag65-)))

(:derived (Flag67-)(and (Flag66-)))

(:derived (Flag82-)(and (Flag51-)))

(:derived (Flag82-)(and (Flag52-)))

(:derived (Flag82-)(and (Flag53-)))

(:derived (Flag82-)(and (Flag68-)))

(:derived (Flag82-)(and (Flag37-)))

(:derived (Flag82-)(and (Flag40-)))

(:derived (Flag82-)(and (Flag69-)))

(:derived (Flag82-)(and (Flag70-)))

(:derived (Flag82-)(and (Flag41-)))

(:derived (Flag82-)(and (Flag49-)))

(:derived (Flag82-)(and (Flag55-)))

(:derived (Flag82-)(and (Flag36-)))

(:derived (Flag82-)(and (Flag57-)))

(:derived (Flag82-)(and (Flag71-)))

(:derived (Flag82-)(and (Flag45-)))

(:derived (Flag82-)(and (Flag58-)))

(:derived (Flag82-)(and (Flag43-)))

(:derived (Flag82-)(and (Flag72-)))

(:derived (Flag82-)(and (Flag59-)))

(:derived (Flag82-)(and (Flag34-)))

(:derived (Flag82-)(and (Flag60-)))

(:derived (Flag82-)(and (Flag35-)))

(:derived (Flag82-)(and (Flag73-)))

(:derived (Flag82-)(and (Flag74-)))

(:derived (Flag82-)(and (Flag56-)))

(:derived (Flag82-)(and (Flag61-)))

(:derived (Flag82-)(and (Flag75-)))

(:derived (Flag82-)(and (Flag76-)))

(:derived (Flag82-)(and (Flag46-)))

(:derived (Flag82-)(and (Flag77-)))

(:derived (Flag82-)(and (Flag78-)))

(:derived (Flag82-)(and (Flag79-)))

(:derived (Flag82-)(and (Flag38-)))

(:derived (Flag82-)(and (Flag62-)))

(:derived (Flag82-)(and (Flag63-)))

(:derived (Flag82-)(and (Flag64-)))

(:derived (Flag82-)(and (Flag44-)))

(:derived (Flag82-)(and (Flag47-)))

(:derived (Flag82-)(and (Flag48-)))

(:derived (Flag82-)(and (Flag65-)))

(:derived (Flag82-)(and (Flag80-)))

(:derived (Flag82-)(and (Flag66-)))

(:derived (Flag82-)(and (Flag81-)))

(:derived (Flag98-)(and (Flag51-)))

(:derived (Flag98-)(and (Flag52-)))

(:derived (Flag98-)(and (Flag53-)))

(:derived (Flag98-)(and (Flag68-)))

(:derived (Flag98-)(and (Flag37-)))

(:derived (Flag98-)(and (Flag40-)))

(:derived (Flag98-)(and (Flag69-)))

(:derived (Flag98-)(and (Flag48-)))

(:derived (Flag98-)(and (Flag83-)))

(:derived (Flag98-)(and (Flag70-)))

(:derived (Flag98-)(and (Flag41-)))

(:derived (Flag98-)(and (Flag49-)))

(:derived (Flag98-)(and (Flag84-)))

(:derived (Flag98-)(and (Flag85-)))

(:derived (Flag98-)(and (Flag55-)))

(:derived (Flag98-)(and (Flag74-)))

(:derived (Flag98-)(and (Flag36-)))

(:derived (Flag98-)(and (Flag57-)))

(:derived (Flag98-)(and (Flag86-)))

(:derived (Flag98-)(and (Flag71-)))

(:derived (Flag98-)(and (Flag45-)))

(:derived (Flag98-)(and (Flag58-)))

(:derived (Flag98-)(and (Flag43-)))

(:derived (Flag98-)(and (Flag72-)))

(:derived (Flag98-)(and (Flag59-)))

(:derived (Flag98-)(and (Flag34-)))

(:derived (Flag98-)(and (Flag87-)))

(:derived (Flag98-)(and (Flag60-)))

(:derived (Flag98-)(and (Flag35-)))

(:derived (Flag98-)(and (Flag73-)))

(:derived (Flag98-)(and (Flag88-)))

(:derived (Flag98-)(and (Flag56-)))

(:derived (Flag98-)(and (Flag61-)))

(:derived (Flag98-)(and (Flag89-)))

(:derived (Flag98-)(and (Flag75-)))

(:derived (Flag98-)(and (Flag76-)))

(:derived (Flag98-)(and (Flag46-)))

(:derived (Flag98-)(and (Flag78-)))

(:derived (Flag98-)(and (Flag90-)))

(:derived (Flag98-)(and (Flag91-)))

(:derived (Flag98-)(and (Flag79-)))

(:derived (Flag98-)(and (Flag92-)))

(:derived (Flag98-)(and (Flag38-)))

(:derived (Flag98-)(and (Flag62-)))

(:derived (Flag98-)(and (Flag93-)))

(:derived (Flag98-)(and (Flag94-)))

(:derived (Flag98-)(and (Flag63-)))

(:derived (Flag98-)(and (Flag95-)))

(:derived (Flag98-)(and (Flag47-)))

(:derived (Flag98-)(and (Flag96-)))

(:derived (Flag98-)(and (Flag97-)))

(:derived (Flag98-)(and (Flag65-)))

(:derived (Flag98-)(and (Flag80-)))

(:derived (Flag98-)(and (Flag66-)))

(:derived (Flag113-)(and (Flag99-)))

(:derived (Flag113-)(and (Flag100-)))

(:derived (Flag113-)(and (Flag52-)))

(:derived (Flag113-)(and (Flag101-)))

(:derived (Flag113-)(and (Flag53-)))

(:derived (Flag113-)(and (Flag68-)))

(:derived (Flag113-)(and (Flag37-)))

(:derived (Flag113-)(and (Flag69-)))

(:derived (Flag113-)(and (Flag48-)))

(:derived (Flag113-)(and (Flag83-)))

(:derived (Flag113-)(and (Flag102-)))

(:derived (Flag113-)(and (Flag70-)))

(:derived (Flag113-)(and (Flag41-)))

(:derived (Flag113-)(and (Flag49-)))

(:derived (Flag113-)(and (Flag46-)))

(:derived (Flag113-)(and (Flag103-)))

(:derived (Flag113-)(and (Flag84-)))

(:derived (Flag113-)(and (Flag104-)))

(:derived (Flag113-)(and (Flag51-)))

(:derived (Flag113-)(and (Flag85-)))

(:derived (Flag113-)(and (Flag55-)))

(:derived (Flag113-)(and (Flag74-)))

(:derived (Flag113-)(and (Flag36-)))

(:derived (Flag113-)(and (Flag57-)))

(:derived (Flag113-)(and (Flag86-)))

(:derived (Flag113-)(and (Flag71-)))

(:derived (Flag113-)(and (Flag45-)))

(:derived (Flag113-)(and (Flag87-)))

(:derived (Flag113-)(and (Flag76-)))

(:derived (Flag113-)(and (Flag105-)))

(:derived (Flag113-)(and (Flag106-)))

(:derived (Flag113-)(and (Flag34-)))

(:derived (Flag113-)(and (Flag107-)))

(:derived (Flag113-)(and (Flag60-)))

(:derived (Flag113-)(and (Flag35-)))

(:derived (Flag113-)(and (Flag73-)))

(:derived (Flag113-)(and (Flag108-)))

(:derived (Flag113-)(and (Flag56-)))

(:derived (Flag113-)(and (Flag61-)))

(:derived (Flag113-)(and (Flag89-)))

(:derived (Flag113-)(and (Flag109-)))

(:derived (Flag113-)(and (Flag110-)))

(:derived (Flag113-)(and (Flag43-)))

(:derived (Flag113-)(and (Flag72-)))

(:derived (Flag113-)(and (Flag59-)))

(:derived (Flag113-)(and (Flag78-)))

(:derived (Flag113-)(and (Flag90-)))

(:derived (Flag113-)(and (Flag91-)))

(:derived (Flag113-)(and (Flag79-)))

(:derived (Flag113-)(and (Flag92-)))

(:derived (Flag113-)(and (Flag38-)))

(:derived (Flag113-)(and (Flag62-)))

(:derived (Flag113-)(and (Flag93-)))

(:derived (Flag113-)(and (Flag94-)))

(:derived (Flag113-)(and (Flag63-)))

(:derived (Flag113-)(and (Flag95-)))

(:derived (Flag113-)(and (Flag111-)))

(:derived (Flag113-)(and (Flag75-)))

(:derived (Flag113-)(and (Flag47-)))

(:derived (Flag113-)(and (Flag112-)))

(:derived (Flag113-)(and (Flag97-)))

(:derived (Flag113-)(and (Flag65-)))

(:derived (Flag113-)(and (Flag80-)))

(:derived (Flag113-)(and (Flag66-)))

(:derived (Flag125-)(and (Flag62-)))

(:derived (Flag125-)(and (Flag100-)))

(:derived (Flag125-)(and (Flag52-)))

(:derived (Flag125-)(and (Flag99-)))

(:derived (Flag125-)(and (Flag53-)))

(:derived (Flag125-)(and (Flag102-)))

(:derived (Flag125-)(and (Flag37-)))

(:derived (Flag125-)(and (Flag69-)))

(:derived (Flag125-)(and (Flag48-)))

(:derived (Flag125-)(and (Flag83-)))

(:derived (Flag125-)(and (Flag80-)))

(:derived (Flag125-)(and (Flag41-)))

(:derived (Flag125-)(and (Flag49-)))

(:derived (Flag125-)(and (Flag104-)))

(:derived (Flag125-)(and (Flag68-)))

(:derived (Flag125-)(and (Flag51-)))

(:derived (Flag125-)(and (Flag85-)))

(:derived (Flag125-)(and (Flag55-)))

(:derived (Flag125-)(and (Flag74-)))

(:derived (Flag125-)(and (Flag114-)))

(:derived (Flag125-)(and (Flag56-)))

(:derived (Flag125-)(and (Flag95-)))

(:derived (Flag125-)(and (Flag115-)))

(:derived (Flag125-)(and (Flag86-)))

(:derived (Flag125-)(and (Flag71-)))

(:derived (Flag125-)(and (Flag45-)))

(:derived (Flag125-)(and (Flag87-)))

(:derived (Flag125-)(and (Flag116-)))

(:derived (Flag125-)(and (Flag72-)))

(:derived (Flag125-)(and (Flag117-)))

(:derived (Flag125-)(and (Flag105-)))

(:derived (Flag125-)(and (Flag57-)))

(:derived (Flag125-)(and (Flag106-)))

(:derived (Flag125-)(and (Flag107-)))

(:derived (Flag125-)(and (Flag60-)))

(:derived (Flag125-)(and (Flag35-)))

(:derived (Flag125-)(and (Flag73-)))

(:derived (Flag125-)(and (Flag108-)))

(:derived (Flag125-)(and (Flag36-)))

(:derived (Flag125-)(and (Flag61-)))

(:derived (Flag125-)(and (Flag89-)))

(:derived (Flag125-)(and (Flag109-)))

(:derived (Flag125-)(and (Flag110-)))

(:derived (Flag125-)(and (Flag59-)))

(:derived (Flag125-)(and (Flag75-)))

(:derived (Flag125-)(and (Flag118-)))

(:derived (Flag125-)(and (Flag76-)))

(:derived (Flag125-)(and (Flag46-)))

(:derived (Flag125-)(and (Flag78-)))

(:derived (Flag125-)(and (Flag90-)))

(:derived (Flag125-)(and (Flag91-)))

(:derived (Flag125-)(and (Flag119-)))

(:derived (Flag125-)(and (Flag79-)))

(:derived (Flag125-)(and (Flag120-)))

(:derived (Flag125-)(and (Flag121-)))

(:derived (Flag125-)(and (Flag92-)))

(:derived (Flag125-)(and (Flag38-)))

(:derived (Flag125-)(and (Flag122-)))

(:derived (Flag125-)(and (Flag93-)))

(:derived (Flag125-)(and (Flag94-)))

(:derived (Flag125-)(and (Flag63-)))

(:derived (Flag125-)(and (Flag123-)))

(:derived (Flag125-)(and (Flag111-)))

(:derived (Flag125-)(and (Flag47-)))

(:derived (Flag125-)(and (Flag112-)))

(:derived (Flag125-)(and (Flag97-)))

(:derived (Flag125-)(and (Flag65-)))

(:derived (Flag125-)(and (Flag103-)))

(:derived (Flag125-)(and (Flag124-)))

(:derived (Flag138-)(and (Flag62-)))

(:derived (Flag138-)(and (Flag100-)))

(:derived (Flag138-)(and (Flag52-)))

(:derived (Flag138-)(and (Flag99-)))

(:derived (Flag138-)(and (Flag53-)))

(:derived (Flag138-)(and (Flag102-)))

(:derived (Flag138-)(and (Flag37-)))

(:derived (Flag138-)(and (Flag126-)))

(:derived (Flag138-)(and (Flag93-)))

(:derived (Flag138-)(and (Flag127-)))

(:derived (Flag138-)(and (Flag128-)))

(:derived (Flag138-)(and (Flag83-)))

(:derived (Flag138-)(and (Flag80-)))

(:derived (Flag138-)(and (Flag41-)))

(:derived (Flag138-)(and (Flag49-)))

(:derived (Flag138-)(and (Flag35-)))

(:derived (Flag138-)(and (Flag104-)))

(:derived (Flag138-)(and (Flag68-)))

(:derived (Flag138-)(and (Flag51-)))

(:derived (Flag138-)(and (Flag85-)))

(:derived (Flag138-)(and (Flag108-)))

(:derived (Flag138-)(and (Flag114-)))

(:derived (Flag138-)(and (Flag129-)))

(:derived (Flag138-)(and (Flag56-)))

(:derived (Flag138-)(and (Flag69-)))

(:derived (Flag138-)(and (Flag95-)))

(:derived (Flag138-)(and (Flag115-)))

(:derived (Flag138-)(and (Flag86-)))

(:derived (Flag138-)(and (Flag71-)))

(:derived (Flag138-)(and (Flag45-)))

(:derived (Flag138-)(and (Flag87-)))

(:derived (Flag138-)(and (Flag116-)))

(:derived (Flag138-)(and (Flag72-)))

(:derived (Flag138-)(and (Flag117-)))

(:derived (Flag138-)(and (Flag105-)))

(:derived (Flag138-)(and (Flag106-)))

(:derived (Flag138-)(and (Flag107-)))

(:derived (Flag138-)(and (Flag60-)))

(:derived (Flag138-)(and (Flag130-)))

(:derived (Flag138-)(and (Flag73-)))

(:derived (Flag138-)(and (Flag74-)))

(:derived (Flag138-)(and (Flag36-)))

(:derived (Flag138-)(and (Flag61-)))

(:derived (Flag138-)(and (Flag131-)))

(:derived (Flag138-)(and (Flag109-)))

(:derived (Flag138-)(and (Flag76-)))

(:derived (Flag138-)(and (Flag110-)))

(:derived (Flag138-)(and (Flag132-)))

(:derived (Flag138-)(and (Flag59-)))

(:derived (Flag138-)(and (Flag89-)))

(:derived (Flag138-)(and (Flag46-)))

(:derived (Flag138-)(and (Flag78-)))

(:derived (Flag138-)(and (Flag90-)))

(:derived (Flag138-)(and (Flag91-)))

(:derived (Flag138-)(and (Flag119-)))

(:derived (Flag138-)(and (Flag79-)))

(:derived (Flag138-)(and (Flag133-)))

(:derived (Flag138-)(and (Flag134-)))

(:derived (Flag138-)(and (Flag135-)))

(:derived (Flag138-)(and (Flag120-)))

(:derived (Flag138-)(and (Flag92-)))

(:derived (Flag138-)(and (Flag38-)))

(:derived (Flag138-)(and (Flag122-)))

(:derived (Flag138-)(and (Flag136-)))

(:derived (Flag138-)(and (Flag63-)))

(:derived (Flag138-)(and (Flag123-)))

(:derived (Flag138-)(and (Flag111-)))

(:derived (Flag138-)(and (Flag47-)))

(:derived (Flag138-)(and (Flag48-)))

(:derived (Flag138-)(and (Flag97-)))

(:derived (Flag138-)(and (Flag65-)))

(:derived (Flag138-)(and (Flag103-)))

(:derived (Flag138-)(and (Flag124-)))

(:derived (Flag138-)(and (Flag137-)))

(:derived (Flag149-)(and (Flag62-)))

(:derived (Flag149-)(and (Flag100-)))

(:derived (Flag149-)(and (Flag52-)))

(:derived (Flag149-)(and (Flag99-)))

(:derived (Flag149-)(and (Flag139-)))

(:derived (Flag149-)(and (Flag102-)))

(:derived (Flag149-)(and (Flag37-)))

(:derived (Flag149-)(and (Flag107-)))

(:derived (Flag149-)(and (Flag126-)))

(:derived (Flag149-)(and (Flag69-)))

(:derived (Flag149-)(and (Flag127-)))

(:derived (Flag149-)(and (Flag83-)))

(:derived (Flag149-)(and (Flag60-)))

(:derived (Flag149-)(and (Flag140-)))

(:derived (Flag149-)(and (Flag41-)))

(:derived (Flag149-)(and (Flag49-)))

(:derived (Flag149-)(and (Flag103-)))

(:derived (Flag149-)(and (Flag53-)))

(:derived (Flag149-)(and (Flag141-)))

(:derived (Flag149-)(and (Flag104-)))

(:derived (Flag149-)(and (Flag51-)))

(:derived (Flag149-)(and (Flag85-)))

(:derived (Flag149-)(and (Flag142-)))

(:derived (Flag149-)(and (Flag108-)))

(:derived (Flag149-)(and (Flag114-)))

(:derived (Flag149-)(and (Flag129-)))

(:derived (Flag149-)(and (Flag36-)))

(:derived (Flag149-)(and (Flag95-)))

(:derived (Flag149-)(and (Flag115-)))

(:derived (Flag149-)(and (Flag86-)))

(:derived (Flag149-)(and (Flag71-)))

(:derived (Flag149-)(and (Flag45-)))

(:derived (Flag149-)(and (Flag87-)))

(:derived (Flag149-)(and (Flag116-)))

(:derived (Flag149-)(and (Flag72-)))

(:derived (Flag149-)(and (Flag117-)))

(:derived (Flag149-)(and (Flag105-)))

(:derived (Flag149-)(and (Flag143-)))

(:derived (Flag149-)(and (Flag144-)))

(:derived (Flag149-)(and (Flag145-)))

(:derived (Flag149-)(and (Flag146-)))

(:derived (Flag149-)(and (Flag35-)))

(:derived (Flag149-)(and (Flag73-)))

(:derived (Flag149-)(and (Flag74-)))

(:derived (Flag149-)(and (Flag56-)))

(:derived (Flag149-)(and (Flag61-)))

(:derived (Flag149-)(and (Flag131-)))

(:derived (Flag149-)(and (Flag109-)))

(:derived (Flag149-)(and (Flag76-)))

(:derived (Flag149-)(and (Flag110-)))

(:derived (Flag149-)(and (Flag132-)))

(:derived (Flag149-)(and (Flag59-)))

(:derived (Flag149-)(and (Flag147-)))

(:derived (Flag149-)(and (Flag89-)))

(:derived (Flag149-)(and (Flag46-)))

(:derived (Flag149-)(and (Flag78-)))

(:derived (Flag149-)(and (Flag90-)))

(:derived (Flag149-)(and (Flag91-)))

(:derived (Flag149-)(and (Flag119-)))

(:derived (Flag149-)(and (Flag79-)))

(:derived (Flag149-)(and (Flag133-)))

(:derived (Flag149-)(and (Flag135-)))

(:derived (Flag149-)(and (Flag120-)))

(:derived (Flag149-)(and (Flag92-)))

(:derived (Flag149-)(and (Flag38-)))

(:derived (Flag149-)(and (Flag122-)))

(:derived (Flag149-)(and (Flag136-)))

(:derived (Flag149-)(and (Flag63-)))

(:derived (Flag149-)(and (Flag123-)))

(:derived (Flag149-)(and (Flag148-)))

(:derived (Flag149-)(and (Flag111-)))

(:derived (Flag149-)(and (Flag48-)))

(:derived (Flag149-)(and (Flag97-)))

(:derived (Flag149-)(and (Flag80-)))

(:derived (Flag149-)(and (Flag124-)))

(:derived (Flag149-)(and (Flag137-)))

(:derived (Flag159-)(and (Flag62-)))

(:derived (Flag159-)(and (Flag100-)))

(:derived (Flag159-)(and (Flag52-)))

(:derived (Flag159-)(and (Flag99-)))

(:derived (Flag159-)(and (Flag97-)))

(:derived (Flag159-)(and (Flag53-)))

(:derived (Flag159-)(and (Flag37-)))

(:derived (Flag159-)(and (Flag107-)))

(:derived (Flag159-)(and (Flag126-)))

(:derived (Flag159-)(and (Flag69-)))

(:derived (Flag159-)(and (Flag127-)))

(:derived (Flag159-)(and (Flag83-)))

(:derived (Flag159-)(and (Flag60-)))

(:derived (Flag159-)(and (Flag140-)))

(:derived (Flag159-)(and (Flag41-)))

(:derived (Flag159-)(and (Flag49-)))

(:derived (Flag159-)(and (Flag103-)))

(:derived (Flag159-)(and (Flag150-)))

(:derived (Flag159-)(and (Flag151-)))

(:derived (Flag159-)(and (Flag141-)))

(:derived (Flag159-)(and (Flag104-)))

(:derived (Flag159-)(and (Flag51-)))

(:derived (Flag159-)(and (Flag142-)))

(:derived (Flag159-)(and (Flag74-)))

(:derived (Flag159-)(and (Flag152-)))

(:derived (Flag159-)(and (Flag129-)))

(:derived (Flag159-)(and (Flag95-)))

(:derived (Flag159-)(and (Flag115-)))

(:derived (Flag159-)(and (Flag86-)))

(:derived (Flag159-)(and (Flag116-)))

(:derived (Flag159-)(and (Flag45-)))

(:derived (Flag159-)(and (Flag87-)))

(:derived (Flag159-)(and (Flag72-)))

(:derived (Flag159-)(and (Flag117-)))

(:derived (Flag159-)(and (Flag105-)))

(:derived (Flag159-)(and (Flag153-)))

(:derived (Flag159-)(and (Flag143-)))

(:derived (Flag159-)(and (Flag144-)))

(:derived (Flag159-)(and (Flag146-)))

(:derived (Flag159-)(and (Flag35-)))

(:derived (Flag159-)(and (Flag73-)))

(:derived (Flag159-)(and (Flag108-)))

(:derived (Flag159-)(and (Flag154-)))

(:derived (Flag159-)(and (Flag61-)))

(:derived (Flag159-)(and (Flag131-)))

(:derived (Flag159-)(and (Flag109-)))

(:derived (Flag159-)(and (Flag155-)))

(:derived (Flag159-)(and (Flag110-)))

(:derived (Flag159-)(and (Flag132-)))

(:derived (Flag159-)(and (Flag59-)))

(:derived (Flag159-)(and (Flag156-)))

(:derived (Flag159-)(and (Flag147-)))

(:derived (Flag159-)(and (Flag76-)))

(:derived (Flag159-)(and (Flag46-)))

(:derived (Flag159-)(and (Flag78-)))

(:derived (Flag159-)(and (Flag90-)))

(:derived (Flag159-)(and (Flag91-)))

(:derived (Flag159-)(and (Flag79-)))

(:derived (Flag159-)(and (Flag133-)))

(:derived (Flag159-)(and (Flag135-)))

(:derived (Flag159-)(and (Flag120-)))

(:derived (Flag159-)(and (Flag92-)))

(:derived (Flag159-)(and (Flag38-)))

(:derived (Flag159-)(and (Flag122-)))

(:derived (Flag159-)(and (Flag136-)))

(:derived (Flag159-)(and (Flag157-)))

(:derived (Flag159-)(and (Flag63-)))

(:derived (Flag159-)(and (Flag123-)))

(:derived (Flag159-)(and (Flag148-)))

(:derived (Flag159-)(and (Flag111-)))

(:derived (Flag159-)(and (Flag48-)))

(:derived (Flag159-)(and (Flag158-)))

(:derived (Flag159-)(and (Flag114-)))

(:derived (Flag159-)(and (Flag80-)))

(:derived (Flag159-)(and (Flag124-)))

(:derived (Flag159-)(and (Flag137-)))

(:derived (Flag167-)(and (Flag62-)))

(:derived (Flag167-)(and (Flag100-)))

(:derived (Flag167-)(and (Flag52-)))

(:derived (Flag167-)(and (Flag99-)))

(:derived (Flag167-)(and (Flag97-)))

(:derived (Flag167-)(and (Flag53-)))

(:derived (Flag167-)(and (Flag37-)))

(:derived (Flag167-)(and (Flag160-)))

(:derived (Flag167-)(and (Flag129-)))

(:derived (Flag167-)(and (Flag140-)))

(:derived (Flag167-)(and (Flag161-)))

(:derived (Flag167-)(and (Flag162-)))

(:derived (Flag167-)(and (Flag157-)))

(:derived (Flag167-)(and (Flag60-)))

(:derived (Flag167-)(and (Flag163-)))

(:derived (Flag167-)(and (Flag164-)))

(:derived (Flag167-)(and (Flag49-)))

(:derived (Flag167-)(and (Flag150-)))

(:derived (Flag167-)(and (Flag141-)))

(:derived (Flag167-)(and (Flag104-)))

(:derived (Flag167-)(and (Flag51-)))

(:derived (Flag167-)(and (Flag142-)))

(:derived (Flag167-)(and (Flag111-)))

(:derived (Flag167-)(and (Flag152-)))

(:derived (Flag167-)(and (Flag165-)))

(:derived (Flag167-)(and (Flag69-)))

(:derived (Flag167-)(and (Flag95-)))

(:derived (Flag167-)(and (Flag115-)))

(:derived (Flag167-)(and (Flag86-)))

(:derived (Flag167-)(and (Flag45-)))

(:derived (Flag167-)(and (Flag87-)))

(:derived (Flag167-)(and (Flag72-)))

(:derived (Flag167-)(and (Flag117-)))

(:derived (Flag167-)(and (Flag153-)))

(:derived (Flag167-)(and (Flag143-)))

(:derived (Flag167-)(and (Flag107-)))

(:derived (Flag167-)(and (Flag146-)))

(:derived (Flag167-)(and (Flag35-)))

(:derived (Flag167-)(and (Flag73-)))

(:derived (Flag167-)(and (Flag108-)))

(:derived (Flag167-)(and (Flag154-)))

(:derived (Flag167-)(and (Flag61-)))

(:derived (Flag167-)(and (Flag131-)))

(:derived (Flag167-)(and (Flag109-)))

(:derived (Flag167-)(and (Flag155-)))

(:derived (Flag167-)(and (Flag132-)))

(:derived (Flag167-)(and (Flag156-)))

(:derived (Flag167-)(and (Flag147-)))

(:derived (Flag167-)(and (Flag76-)))

(:derived (Flag167-)(and (Flag59-)))

(:derived (Flag167-)(and (Flag78-)))

(:derived (Flag167-)(and (Flag90-)))

(:derived (Flag167-)(and (Flag91-)))

(:derived (Flag167-)(and (Flag79-)))

(:derived (Flag167-)(and (Flag133-)))

(:derived (Flag167-)(and (Flag135-)))

(:derived (Flag167-)(and (Flag120-)))

(:derived (Flag167-)(and (Flag83-)))

(:derived (Flag167-)(and (Flag41-)))

(:derived (Flag167-)(and (Flag92-)))

(:derived (Flag167-)(and (Flag126-)))

(:derived (Flag167-)(and (Flag38-)))

(:derived (Flag167-)(and (Flag122-)))

(:derived (Flag167-)(and (Flag166-)))

(:derived (Flag167-)(and (Flag136-)))

(:derived (Flag167-)(and (Flag123-)))

(:derived (Flag167-)(and (Flag148-)))

(:derived (Flag167-)(and (Flag80-)))

(:derived (Flag167-)(and (Flag48-)))

(:derived (Flag167-)(and (Flag158-)))

(:derived (Flag167-)(and (Flag114-)))

(:derived (Flag167-)(and (Flag103-)))

(:derived (Flag167-)(and (Flag124-)))

(:derived (Flag167-)(and (Flag137-)))

(:derived (Flag175-)(and (Flag62-)))

(:derived (Flag175-)(and (Flag100-)))

(:derived (Flag175-)(and (Flag52-)))

(:derived (Flag175-)(and (Flag168-)))

(:derived (Flag175-)(and (Flag126-)))

(:derived (Flag175-)(and (Flag41-)))

(:derived (Flag175-)(and (Flag160-)))

(:derived (Flag175-)(and (Flag169-)))

(:derived (Flag175-)(and (Flag161-)))

(:derived (Flag175-)(and (Flag162-)))

(:derived (Flag175-)(and (Flag157-)))

(:derived (Flag175-)(and (Flag60-)))

(:derived (Flag175-)(and (Flag163-)))

(:derived (Flag175-)(and (Flag164-)))

(:derived (Flag175-)(and (Flag49-)))

(:derived (Flag175-)(and (Flag103-)))

(:derived (Flag175-)(and (Flag150-)))

(:derived (Flag175-)(and (Flag141-)))

(:derived (Flag175-)(and (Flag104-)))

(:derived (Flag175-)(and (Flag51-)))

(:derived (Flag175-)(and (Flag142-)))

(:derived (Flag175-)(and (Flag111-)))

(:derived (Flag175-)(and (Flag114-)))

(:derived (Flag175-)(and (Flag129-)))

(:derived (Flag175-)(and (Flag69-)))

(:derived (Flag175-)(and (Flag115-)))

(:derived (Flag175-)(and (Flag86-)))

(:derived (Flag175-)(and (Flag45-)))

(:derived (Flag175-)(and (Flag87-)))

(:derived (Flag175-)(and (Flag72-)))

(:derived (Flag175-)(and (Flag117-)))

(:derived (Flag175-)(and (Flag170-)))

(:derived (Flag175-)(and (Flag153-)))

(:derived (Flag175-)(and (Flag143-)))

(:derived (Flag175-)(and (Flag107-)))

(:derived (Flag175-)(and (Flag146-)))

(:derived (Flag175-)(and (Flag35-)))

(:derived (Flag175-)(and (Flag73-)))

(:derived (Flag175-)(and (Flag108-)))

(:derived (Flag175-)(and (Flag154-)))

(:derived (Flag175-)(and (Flag61-)))

(:derived (Flag175-)(and (Flag131-)))

(:derived (Flag175-)(and (Flag109-)))

(:derived (Flag175-)(and (Flag155-)))

(:derived (Flag175-)(and (Flag132-)))

(:derived (Flag175-)(and (Flag171-)))

(:derived (Flag175-)(and (Flag156-)))

(:derived (Flag175-)(and (Flag147-)))

(:derived (Flag175-)(and (Flag76-)))

(:derived (Flag175-)(and (Flag59-)))

(:derived (Flag175-)(and (Flag78-)))

(:derived (Flag175-)(and (Flag90-)))

(:derived (Flag175-)(and (Flag97-)))

(:derived (Flag175-)(and (Flag79-)))

(:derived (Flag175-)(and (Flag135-)))

(:derived (Flag175-)(and (Flag120-)))

(:derived (Flag175-)(and (Flag83-)))

(:derived (Flag175-)(and (Flag172-)))

(:derived (Flag175-)(and (Flag38-)))

(:derived (Flag175-)(and (Flag122-)))

(:derived (Flag175-)(and (Flag166-)))

(:derived (Flag175-)(and (Flag136-)))

(:derived (Flag175-)(and (Flag173-)))

(:derived (Flag175-)(and (Flag95-)))

(:derived (Flag175-)(and (Flag148-)))

(:derived (Flag175-)(and (Flag48-)))

(:derived (Flag175-)(and (Flag158-)))

(:derived (Flag175-)(and (Flag174-)))

(:derived (Flag175-)(and (Flag123-)))

(:derived (Flag175-)(and (Flag137-)))

(:derived (Flag181-)(and (Flag62-)))

(:derived (Flag181-)(and (Flag100-)))

(:derived (Flag181-)(and (Flag52-)))

(:derived (Flag181-)(and (Flag168-)))

(:derived (Flag181-)(and (Flag41-)))

(:derived (Flag181-)(and (Flag160-)))

(:derived (Flag181-)(and (Flag169-)))

(:derived (Flag181-)(and (Flag161-)))

(:derived (Flag181-)(and (Flag172-)))

(:derived (Flag181-)(and (Flag157-)))

(:derived (Flag181-)(and (Flag176-)))

(:derived (Flag181-)(and (Flag163-)))

(:derived (Flag181-)(and (Flag164-)))

(:derived (Flag181-)(and (Flag35-)))

(:derived (Flag181-)(and (Flag150-)))

(:derived (Flag181-)(and (Flag177-)))

(:derived (Flag181-)(and (Flag104-)))

(:derived (Flag181-)(and (Flag51-)))

(:derived (Flag181-)(and (Flag142-)))

(:derived (Flag181-)(and (Flag111-)))

(:derived (Flag181-)(and (Flag114-)))

(:derived (Flag181-)(and (Flag129-)))

(:derived (Flag181-)(and (Flag69-)))

(:derived (Flag181-)(and (Flag115-)))

(:derived (Flag181-)(and (Flag86-)))

(:derived (Flag181-)(and (Flag45-)))

(:derived (Flag181-)(and (Flag87-)))

(:derived (Flag181-)(and (Flag72-)))

(:derived (Flag181-)(and (Flag117-)))

(:derived (Flag181-)(and (Flag178-)))

(:derived (Flag181-)(and (Flag170-)))

(:derived (Flag181-)(and (Flag179-)))

(:derived (Flag181-)(and (Flag146-)))

(:derived (Flag181-)(and (Flag141-)))

(:derived (Flag181-)(and (Flag126-)))

(:derived (Flag181-)(and (Flag154-)))

(:derived (Flag181-)(and (Flag61-)))

(:derived (Flag181-)(and (Flag131-)))

(:derived (Flag181-)(and (Flag109-)))

(:derived (Flag181-)(and (Flag155-)))

(:derived (Flag181-)(and (Flag132-)))

(:derived (Flag181-)(and (Flag171-)))

(:derived (Flag181-)(and (Flag156-)))

(:derived (Flag181-)(and (Flag147-)))

(:derived (Flag181-)(and (Flag76-)))

(:derived (Flag181-)(and (Flag59-)))

(:derived (Flag181-)(and (Flag78-)))

(:derived (Flag181-)(and (Flag90-)))

(:derived (Flag181-)(and (Flag97-)))

(:derived (Flag181-)(and (Flag79-)))

(:derived (Flag181-)(and (Flag135-)))

(:derived (Flag181-)(and (Flag83-)))

(:derived (Flag181-)(and (Flag180-)))

(:derived (Flag181-)(and (Flag38-)))

(:derived (Flag181-)(and (Flag122-)))

(:derived (Flag181-)(and (Flag166-)))

(:derived (Flag181-)(and (Flag103-)))

(:derived (Flag181-)(and (Flag95-)))

(:derived (Flag181-)(and (Flag148-)))

(:derived (Flag181-)(and (Flag48-)))

(:derived (Flag181-)(and (Flag158-)))

(:derived (Flag181-)(and (Flag174-)))

(:derived (Flag181-)(and (Flag123-)))

(:derived (Flag181-)(and (Flag137-)))

(:derived (Flag187-)(and (Flag168-)))

(:derived (Flag187-)(and (Flag100-)))

(:derived (Flag187-)(and (Flag52-)))

(:derived (Flag187-)(and (Flag41-)))

(:derived (Flag187-)(and (Flag160-)))

(:derived (Flag187-)(and (Flag69-)))

(:derived (Flag187-)(and (Flag172-)))

(:derived (Flag187-)(and (Flag157-)))

(:derived (Flag187-)(and (Flag163-)))

(:derived (Flag187-)(and (Flag164-)))

(:derived (Flag187-)(and (Flag35-)))

(:derived (Flag187-)(and (Flag150-)))

(:derived (Flag187-)(and (Flag182-)))

(:derived (Flag187-)(and (Flag141-)))

(:derived (Flag187-)(and (Flag104-)))

(:derived (Flag187-)(and (Flag51-)))

(:derived (Flag187-)(and (Flag142-)))

(:derived (Flag187-)(and (Flag111-)))

(:derived (Flag187-)(and (Flag114-)))

(:derived (Flag187-)(and (Flag129-)))

(:derived (Flag187-)(and (Flag86-)))

(:derived (Flag187-)(and (Flag87-)))

(:derived (Flag187-)(and (Flag72-)))

(:derived (Flag187-)(and (Flag178-)))

(:derived (Flag187-)(and (Flag170-)))

(:derived (Flag187-)(and (Flag146-)))

(:derived (Flag187-)(and (Flag177-)))

(:derived (Flag187-)(and (Flag131-)))

(:derived (Flag187-)(and (Flag154-)))

(:derived (Flag187-)(and (Flag61-)))

(:derived (Flag187-)(and (Flag183-)))

(:derived (Flag187-)(and (Flag109-)))

(:derived (Flag187-)(and (Flag155-)))

(:derived (Flag187-)(and (Flag184-)))

(:derived (Flag187-)(and (Flag171-)))

(:derived (Flag187-)(and (Flag147-)))

(:derived (Flag187-)(and (Flag76-)))

(:derived (Flag187-)(and (Flag59-)))

(:derived (Flag187-)(and (Flag90-)))

(:derived (Flag187-)(and (Flag79-)))

(:derived (Flag187-)(and (Flag132-)))

(:derived (Flag187-)(and (Flag135-)))

(:derived (Flag187-)(and (Flag83-)))

(:derived (Flag187-)(and (Flag180-)))

(:derived (Flag187-)(and (Flag185-)))

(:derived (Flag187-)(and (Flag38-)))

(:derived (Flag187-)(and (Flag122-)))

(:derived (Flag187-)(and (Flag166-)))

(:derived (Flag187-)(and (Flag95-)))

(:derived (Flag187-)(and (Flag186-)))

(:derived (Flag187-)(and (Flag48-)))

(:derived (Flag187-)(and (Flag158-)))

(:derived (Flag187-)(and (Flag174-)))

(:derived (Flag187-)(and (Flag123-)))

(:derived (Flag187-)(and (Flag137-)))

(:derived (Flag191-)(and (Flag168-)))

(:derived (Flag191-)(and (Flag100-)))

(:derived (Flag191-)(and (Flag52-)))

(:derived (Flag191-)(and (Flag41-)))

(:derived (Flag191-)(and (Flag188-)))

(:derived (Flag191-)(and (Flag69-)))

(:derived (Flag191-)(and (Flag172-)))

(:derived (Flag191-)(and (Flag157-)))

(:derived (Flag191-)(and (Flag164-)))

(:derived (Flag191-)(and (Flag150-)))

(:derived (Flag191-)(and (Flag182-)))

(:derived (Flag191-)(and (Flag141-)))

(:derived (Flag191-)(and (Flag104-)))

(:derived (Flag191-)(and (Flag51-)))

(:derived (Flag191-)(and (Flag142-)))

(:derived (Flag191-)(and (Flag111-)))

(:derived (Flag191-)(and (Flag114-)))

(:derived (Flag191-)(and (Flag129-)))

(:derived (Flag191-)(and (Flag87-)))

(:derived (Flag191-)(and (Flag76-)))

(:derived (Flag191-)(and (Flag178-)))

(:derived (Flag191-)(and (Flag170-)))

(:derived (Flag191-)(and (Flag177-)))

(:derived (Flag191-)(and (Flag189-)))

(:derived (Flag191-)(and (Flag160-)))

(:derived (Flag191-)(and (Flag154-)))

(:derived (Flag191-)(and (Flag83-)))

(:derived (Flag191-)(and (Flag184-)))

(:derived (Flag191-)(and (Flag171-)))

(:derived (Flag191-)(and (Flag147-)))

(:derived (Flag191-)(and (Flag59-)))

(:derived (Flag191-)(and (Flag86-)))

(:derived (Flag191-)(and (Flag132-)))

(:derived (Flag191-)(and (Flag180-)))

(:derived (Flag191-)(and (Flag185-)))

(:derived (Flag191-)(and (Flag38-)))

(:derived (Flag191-)(and (Flag122-)))

(:derived (Flag191-)(and (Flag166-)))

(:derived (Flag191-)(and (Flag190-)))

(:derived (Flag191-)(and (Flag95-)))

(:derived (Flag191-)(and (Flag48-)))

(:derived (Flag191-)(and (Flag158-)))

(:derived (Flag191-)(and (Flag123-)))

(:derived (Flag191-)(and (Flag137-)))

(:derived (Flag194-)(and (Flag168-)))

(:derived (Flag194-)(and (Flag100-)))

(:derived (Flag194-)(and (Flag41-)))

(:derived (Flag194-)(and (Flag188-)))

(:derived (Flag194-)(and (Flag69-)))

(:derived (Flag194-)(and (Flag172-)))

(:derived (Flag194-)(and (Flag157-)))

(:derived (Flag194-)(and (Flag182-)))

(:derived (Flag194-)(and (Flag177-)))

(:derived (Flag194-)(and (Flag111-)))

(:derived (Flag194-)(and (Flag114-)))

(:derived (Flag194-)(and (Flag87-)))

(:derived (Flag194-)(and (Flag76-)))

(:derived (Flag194-)(and (Flag178-)))

(:derived (Flag194-)(and (Flag170-)))

(:derived (Flag194-)(and (Flag154-)))

(:derived (Flag194-)(and (Flag132-)))

(:derived (Flag194-)(and (Flag147-)))

(:derived (Flag194-)(and (Flag59-)))

(:derived (Flag194-)(and (Flag86-)))

(:derived (Flag194-)(and (Flag184-)))

(:derived (Flag194-)(and (Flag192-)))

(:derived (Flag194-)(and (Flag190-)))

(:derived (Flag194-)(and (Flag51-)))

(:derived (Flag194-)(and (Flag166-)))

(:derived (Flag194-)(and (Flag48-)))

(:derived (Flag194-)(and (Flag158-)))

(:derived (Flag194-)(and (Flag193-)))

(:derived (Flag194-)(and (Flag123-)))

(:derived (Flag194-)(and (Flag137-)))

(:derived (Flag198-)(and (Flag177-)))

(:derived (Flag198-)(and (Flag137-)))

(:derived (Flag198-)(and (Flag100-)))

(:derived (Flag198-)(and (Flag190-)))

(:derived (Flag198-)(and (Flag41-)))

(:derived (Flag198-)(and (Flag114-)))

(:derived (Flag198-)(and (Flag51-)))

(:derived (Flag198-)(and (Flag166-)))

(:derived (Flag198-)(and (Flag172-)))

(:derived (Flag198-)(and (Flag157-)))

(:derived (Flag198-)(and (Flag147-)))

(:derived (Flag198-)(and (Flag76-)))

(:derived (Flag198-)(and (Flag196-)))

(:derived (Flag198-)(and (Flag86-)))

(:derived (Flag198-)(and (Flag197-)))

(:derived (Flag198-)(and (Flag182-)))

(:derived (Flag1-)(and (COLUMN2-ROBOT)(ROW1-ROBOT)(NOT-ERROR-)(NOT-CHECKCONSISTENCY-)))

(:derived (Flag3-)(and (LEFTOF3-ROBOT)))

(:derived (Flag3-)(and (RIGHTOF7-ROBOT)))

(:derived (Flag3-)(and (RIGHTOF8-ROBOT)))

(:derived (Flag3-)(and (RIGHTOF15-ROBOT)))

(:derived (Flag3-)(and (LEFTOF2-ROBOT)))

(:derived (Flag3-)(and (LEFTOF9-ROBOT)))

(:derived (Flag3-)(and (RIGHTOF0-ROBOT)))

(:derived (Flag3-)(and (LEFTOF8-ROBOT)))

(:derived (Flag3-)(and (LEFTOF7-ROBOT)))

(:derived (Flag3-)(and (LEFTOF14-ROBOT)))

(:derived (Flag3-)(and (LEFTOF5-ROBOT)))

(:derived (Flag3-)(and (RIGHTOF1-ROBOT)))

(:derived (Flag3-)(and (LEFTOF12-ROBOT)))

(:derived (Flag3-)(and (RIGHTOF13-ROBOT)))

(:derived (Flag3-)(and (RIGHTOF14-ROBOT)))

(:derived (Flag3-)(and (RIGHTOF6-ROBOT)))

(:derived (Flag3-)(and (RIGHTOF9-ROBOT)))

(:derived (Flag3-)(and (LEFTOF6-ROBOT)))

(:derived (Flag3-)(and (RIGHTOF10-ROBOT)))

(:derived (Flag3-)(and (RIGHTOF11-ROBOT)))

(:derived (Flag3-)(and (LEFTOF11-ROBOT)))

(:derived (Flag3-)(and (RIGHTOF5-ROBOT)))

(:derived (Flag3-)(and (LEFTOF10-ROBOT)))

(:derived (Flag3-)(and (LEFTOF4-ROBOT)))

(:derived (Flag3-)(and (LEFTOF16-ROBOT)))

(:derived (Flag3-)(and (RIGHTOF4-ROBOT)))

(:derived (Flag3-)(and (LEFTOF1-ROBOT)))

(:derived (Flag3-)(and (RIGHTOF12-ROBOT)))

(:derived (Flag3-)(and (LEFTOF13-ROBOT)))

(:derived (Flag3-)(and (RIGHTOF3-ROBOT)))

(:derived (Flag5-)(and (RIGHTOF3-ROBOT)))

(:derived (Flag5-)(and (RIGHTOF8-ROBOT)))

(:derived (Flag5-)(and (RIGHTOF10-ROBOT)))

(:derived (Flag5-)(and (RIGHTOF5-ROBOT)))

(:derived (Flag5-)(and (RIGHTOF9-ROBOT)))

(:derived (Flag5-)(and (RIGHTOF14-ROBOT)))

(:derived (Flag5-)(and (RIGHTOF13-ROBOT)))

(:derived (Flag5-)(and (RIGHTOF12-ROBOT)))

(:derived (Flag5-)(and (RIGHTOF0-ROBOT)))

(:derived (Flag5-)(and (RIGHTOF4-ROBOT)))

(:derived (Flag5-)(and (RIGHTOF1-ROBOT)))

(:derived (Flag5-)(and (RIGHTOF7-ROBOT)))

(:derived (Flag5-)(and (RIGHTOF11-ROBOT)))

(:derived (Flag5-)(and (RIGHTOF15-ROBOT)))

(:derived (Flag5-)(and (RIGHTOF6-ROBOT)))

(:derived (Flag6-)(and (RIGHTOF3-ROBOT)))

(:derived (Flag6-)(and (RIGHTOF8-ROBOT)))

(:derived (Flag6-)(and (RIGHTOF5-ROBOT)))

(:derived (Flag6-)(and (RIGHTOF9-ROBOT)))

(:derived (Flag6-)(and (RIGHTOF14-ROBOT)))

(:derived (Flag6-)(and (RIGHTOF13-ROBOT)))

(:derived (Flag6-)(and (RIGHTOF12-ROBOT)))

(:derived (Flag6-)(and (RIGHTOF10-ROBOT)))

(:derived (Flag6-)(and (RIGHTOF4-ROBOT)))

(:derived (Flag6-)(and (RIGHTOF1-ROBOT)))

(:derived (Flag6-)(and (RIGHTOF7-ROBOT)))

(:derived (Flag6-)(and (RIGHTOF11-ROBOT)))

(:derived (Flag6-)(and (RIGHTOF15-ROBOT)))

(:derived (Flag6-)(and (RIGHTOF6-ROBOT)))

(:derived (Flag7-)(and (RIGHTOF3-ROBOT)))

(:derived (Flag7-)(and (RIGHTOF5-ROBOT)))

(:derived (Flag7-)(and (RIGHTOF9-ROBOT)))

(:derived (Flag7-)(and (RIGHTOF14-ROBOT)))

(:derived (Flag7-)(and (RIGHTOF13-ROBOT)))

(:derived (Flag7-)(and (RIGHTOF12-ROBOT)))

(:derived (Flag7-)(and (RIGHTOF10-ROBOT)))

(:derived (Flag7-)(and (RIGHTOF4-ROBOT)))

(:derived (Flag7-)(and (RIGHTOF8-ROBOT)))

(:derived (Flag7-)(and (RIGHTOF7-ROBOT)))

(:derived (Flag7-)(and (RIGHTOF11-ROBOT)))

(:derived (Flag7-)(and (RIGHTOF15-ROBOT)))

(:derived (Flag7-)(and (RIGHTOF6-ROBOT)))

(:derived (Flag8-)(and (RIGHTOF3-ROBOT)))

(:derived (Flag8-)(and (RIGHTOF5-ROBOT)))

(:derived (Flag8-)(and (RIGHTOF9-ROBOT)))

(:derived (Flag8-)(and (RIGHTOF14-ROBOT)))

(:derived (Flag8-)(and (RIGHTOF13-ROBOT)))

(:derived (Flag8-)(and (RIGHTOF12-ROBOT)))

(:derived (Flag8-)(and (RIGHTOF10-ROBOT)))

(:derived (Flag8-)(and (RIGHTOF4-ROBOT)))

(:derived (Flag8-)(and (RIGHTOF8-ROBOT)))

(:derived (Flag8-)(and (RIGHTOF7-ROBOT)))

(:derived (Flag8-)(and (RIGHTOF11-ROBOT)))

(:derived (Flag8-)(and (RIGHTOF15-ROBOT)))

(:derived (Flag8-)(and (RIGHTOF6-ROBOT)))

(:derived (Flag9-)(and (RIGHTOF5-ROBOT)))

(:derived (Flag9-)(and (RIGHTOF9-ROBOT)))

(:derived (Flag9-)(and (RIGHTOF14-ROBOT)))

(:derived (Flag9-)(and (RIGHTOF13-ROBOT)))

(:derived (Flag9-)(and (RIGHTOF12-ROBOT)))

(:derived (Flag9-)(and (RIGHTOF10-ROBOT)))

(:derived (Flag9-)(and (RIGHTOF4-ROBOT)))

(:derived (Flag9-)(and (RIGHTOF8-ROBOT)))

(:derived (Flag9-)(and (RIGHTOF7-ROBOT)))

(:derived (Flag9-)(and (RIGHTOF11-ROBOT)))

(:derived (Flag9-)(and (RIGHTOF15-ROBOT)))

(:derived (Flag9-)(and (RIGHTOF6-ROBOT)))

(:derived (Flag10-)(and (RIGHTOF5-ROBOT)))

(:derived (Flag10-)(and (RIGHTOF9-ROBOT)))

(:derived (Flag10-)(and (RIGHTOF14-ROBOT)))

(:derived (Flag10-)(and (RIGHTOF13-ROBOT)))

(:derived (Flag10-)(and (RIGHTOF12-ROBOT)))

(:derived (Flag10-)(and (RIGHTOF10-ROBOT)))

(:derived (Flag10-)(and (RIGHTOF8-ROBOT)))

(:derived (Flag10-)(and (RIGHTOF7-ROBOT)))

(:derived (Flag10-)(and (RIGHTOF11-ROBOT)))

(:derived (Flag10-)(and (RIGHTOF15-ROBOT)))

(:derived (Flag10-)(and (RIGHTOF6-ROBOT)))

(:derived (Flag11-)(and (RIGHTOF9-ROBOT)))

(:derived (Flag11-)(and (RIGHTOF14-ROBOT)))

(:derived (Flag11-)(and (RIGHTOF13-ROBOT)))

(:derived (Flag11-)(and (RIGHTOF12-ROBOT)))

(:derived (Flag11-)(and (RIGHTOF10-ROBOT)))

(:derived (Flag11-)(and (RIGHTOF8-ROBOT)))

(:derived (Flag11-)(and (RIGHTOF7-ROBOT)))

(:derived (Flag11-)(and (RIGHTOF11-ROBOT)))

(:derived (Flag11-)(and (RIGHTOF15-ROBOT)))

(:derived (Flag11-)(and (RIGHTOF6-ROBOT)))

(:derived (Flag12-)(and (RIGHTOF14-ROBOT)))

(:derived (Flag12-)(and (RIGHTOF13-ROBOT)))

(:derived (Flag12-)(and (RIGHTOF15-ROBOT)))

(:derived (Flag12-)(and (RIGHTOF10-ROBOT)))

(:derived (Flag12-)(and (RIGHTOF8-ROBOT)))

(:derived (Flag12-)(and (RIGHTOF12-ROBOT)))

(:derived (Flag12-)(and (RIGHTOF7-ROBOT)))

(:derived (Flag12-)(and (RIGHTOF9-ROBOT)))

(:derived (Flag12-)(and (RIGHTOF11-ROBOT)))

(:derived (Flag13-)(and (RIGHTOF14-ROBOT)))

(:derived (Flag13-)(and (RIGHTOF13-ROBOT)))

(:derived (Flag13-)(and (RIGHTOF15-ROBOT)))

(:derived (Flag13-)(and (RIGHTOF10-ROBOT)))

(:derived (Flag13-)(and (RIGHTOF8-ROBOT)))

(:derived (Flag13-)(and (RIGHTOF12-ROBOT)))

(:derived (Flag13-)(and (RIGHTOF9-ROBOT)))

(:derived (Flag13-)(and (RIGHTOF11-ROBOT)))

(:derived (Flag14-)(and (RIGHTOF14-ROBOT)))

(:derived (Flag14-)(and (RIGHTOF13-ROBOT)))

(:derived (Flag14-)(and (RIGHTOF15-ROBOT)))

(:derived (Flag14-)(and (RIGHTOF10-ROBOT)))

(:derived (Flag14-)(and (RIGHTOF12-ROBOT)))

(:derived (Flag14-)(and (RIGHTOF9-ROBOT)))

(:derived (Flag14-)(and (RIGHTOF11-ROBOT)))

(:derived (Flag15-)(and (RIGHTOF14-ROBOT)))

(:derived (Flag15-)(and (RIGHTOF13-ROBOT)))

(:derived (Flag15-)(and (RIGHTOF15-ROBOT)))

(:derived (Flag15-)(and (RIGHTOF10-ROBOT)))

(:derived (Flag15-)(and (RIGHTOF12-ROBOT)))

(:derived (Flag15-)(and (RIGHTOF11-ROBOT)))

(:derived (Flag16-)(and (RIGHTOF14-ROBOT)))

(:derived (Flag16-)(and (RIGHTOF13-ROBOT)))

(:derived (Flag16-)(and (RIGHTOF15-ROBOT)))

(:derived (Flag16-)(and (RIGHTOF12-ROBOT)))

(:derived (Flag16-)(and (RIGHTOF11-ROBOT)))

(:derived (Flag17-)(and (RIGHTOF14-ROBOT)))

(:derived (Flag17-)(and (RIGHTOF13-ROBOT)))

(:derived (Flag17-)(and (RIGHTOF15-ROBOT)))

(:derived (Flag17-)(and (RIGHTOF12-ROBOT)))

(:derived (Flag18-)(and (RIGHTOF14-ROBOT)))

(:derived (Flag18-)(and (RIGHTOF13-ROBOT)))

(:derived (Flag18-)(and (RIGHTOF15-ROBOT)))

(:derived (Flag19-)(and (RIGHTOF14-ROBOT)))

(:derived (Flag19-)(and (RIGHTOF15-ROBOT)))

(:derived (Flag20-)(and (LEFTOF1-ROBOT)))

(:derived (Flag20-)(and (LEFTOF2-ROBOT)))

(:derived (Flag21-)(and (LEFTOF1-ROBOT)))

(:derived (Flag21-)(and (LEFTOF3-ROBOT)))

(:derived (Flag21-)(and (LEFTOF2-ROBOT)))

(:derived (Flag22-)(and (LEFTOF1-ROBOT)))

(:derived (Flag22-)(and (LEFTOF3-ROBOT)))

(:derived (Flag22-)(and (LEFTOF4-ROBOT)))

(:derived (Flag22-)(and (LEFTOF2-ROBOT)))

(:derived (Flag23-)(and (LEFTOF5-ROBOT)))

(:derived (Flag23-)(and (LEFTOF1-ROBOT)))

(:derived (Flag23-)(and (LEFTOF3-ROBOT)))

(:derived (Flag23-)(and (LEFTOF4-ROBOT)))

(:derived (Flag23-)(and (LEFTOF2-ROBOT)))

(:derived (Flag24-)(and (LEFTOF2-ROBOT)))

(:derived (Flag24-)(and (LEFTOF1-ROBOT)))

(:derived (Flag24-)(and (LEFTOF6-ROBOT)))

(:derived (Flag24-)(and (LEFTOF5-ROBOT)))

(:derived (Flag24-)(and (LEFTOF3-ROBOT)))

(:derived (Flag24-)(and (LEFTOF4-ROBOT)))

(:derived (Flag25-)(and (LEFTOF2-ROBOT)))

(:derived (Flag25-)(and (LEFTOF7-ROBOT)))

(:derived (Flag25-)(and (LEFTOF1-ROBOT)))

(:derived (Flag25-)(and (LEFTOF6-ROBOT)))

(:derived (Flag25-)(and (LEFTOF5-ROBOT)))

(:derived (Flag25-)(and (LEFTOF3-ROBOT)))

(:derived (Flag25-)(and (LEFTOF4-ROBOT)))

(:derived (Flag26-)(and (LEFTOF2-ROBOT)))

(:derived (Flag26-)(and (LEFTOF7-ROBOT)))

(:derived (Flag26-)(and (LEFTOF1-ROBOT)))

(:derived (Flag26-)(and (LEFTOF6-ROBOT)))

(:derived (Flag26-)(and (LEFTOF5-ROBOT)))

(:derived (Flag26-)(and (LEFTOF3-ROBOT)))

(:derived (Flag26-)(and (LEFTOF4-ROBOT)))

(:derived (Flag26-)(and (LEFTOF8-ROBOT)))

(:derived (Flag27-)(and (LEFTOF9-ROBOT)))

(:derived (Flag27-)(and (LEFTOF2-ROBOT)))

(:derived (Flag27-)(and (LEFTOF7-ROBOT)))

(:derived (Flag27-)(and (LEFTOF1-ROBOT)))

(:derived (Flag27-)(and (LEFTOF6-ROBOT)))

(:derived (Flag27-)(and (LEFTOF5-ROBOT)))

(:derived (Flag27-)(and (LEFTOF3-ROBOT)))

(:derived (Flag27-)(and (LEFTOF4-ROBOT)))

(:derived (Flag27-)(and (LEFTOF8-ROBOT)))

(:derived (Flag28-)(and (LEFTOF10-ROBOT)))

(:derived (Flag28-)(and (LEFTOF9-ROBOT)))

(:derived (Flag28-)(and (LEFTOF2-ROBOT)))

(:derived (Flag28-)(and (LEFTOF7-ROBOT)))

(:derived (Flag28-)(and (LEFTOF1-ROBOT)))

(:derived (Flag28-)(and (LEFTOF6-ROBOT)))

(:derived (Flag28-)(and (LEFTOF5-ROBOT)))

(:derived (Flag28-)(and (LEFTOF3-ROBOT)))

(:derived (Flag28-)(and (LEFTOF4-ROBOT)))

(:derived (Flag28-)(and (LEFTOF8-ROBOT)))

(:derived (Flag29-)(and (LEFTOF10-ROBOT)))

(:derived (Flag29-)(and (LEFTOF11-ROBOT)))

(:derived (Flag29-)(and (LEFTOF9-ROBOT)))

(:derived (Flag29-)(and (LEFTOF2-ROBOT)))

(:derived (Flag29-)(and (LEFTOF7-ROBOT)))

(:derived (Flag29-)(and (LEFTOF1-ROBOT)))

(:derived (Flag29-)(and (LEFTOF6-ROBOT)))

(:derived (Flag29-)(and (LEFTOF5-ROBOT)))

(:derived (Flag29-)(and (LEFTOF3-ROBOT)))

(:derived (Flag29-)(and (LEFTOF4-ROBOT)))

(:derived (Flag29-)(and (LEFTOF8-ROBOT)))

(:derived (Flag30-)(and (LEFTOF10-ROBOT)))

(:derived (Flag30-)(and (LEFTOF11-ROBOT)))

(:derived (Flag30-)(and (LEFTOF9-ROBOT)))

(:derived (Flag30-)(and (LEFTOF2-ROBOT)))

(:derived (Flag30-)(and (LEFTOF7-ROBOT)))

(:derived (Flag30-)(and (LEFTOF1-ROBOT)))

(:derived (Flag30-)(and (LEFTOF12-ROBOT)))

(:derived (Flag30-)(and (LEFTOF6-ROBOT)))

(:derived (Flag30-)(and (LEFTOF5-ROBOT)))

(:derived (Flag30-)(and (LEFTOF3-ROBOT)))

(:derived (Flag30-)(and (LEFTOF4-ROBOT)))

(:derived (Flag30-)(and (LEFTOF8-ROBOT)))

(:derived (Flag31-)(and (LEFTOF10-ROBOT)))

(:derived (Flag31-)(and (LEFTOF11-ROBOT)))

(:derived (Flag31-)(and (LEFTOF9-ROBOT)))

(:derived (Flag31-)(and (LEFTOF2-ROBOT)))

(:derived (Flag31-)(and (LEFTOF13-ROBOT)))

(:derived (Flag31-)(and (LEFTOF1-ROBOT)))

(:derived (Flag31-)(and (LEFTOF12-ROBOT)))

(:derived (Flag31-)(and (LEFTOF6-ROBOT)))

(:derived (Flag31-)(and (LEFTOF5-ROBOT)))

(:derived (Flag31-)(and (LEFTOF3-ROBOT)))

(:derived (Flag31-)(and (LEFTOF4-ROBOT)))

(:derived (Flag31-)(and (LEFTOF7-ROBOT)))

(:derived (Flag31-)(and (LEFTOF8-ROBOT)))

(:derived (Flag32-)(and (LEFTOF10-ROBOT)))

(:derived (Flag32-)(and (LEFTOF11-ROBOT)))

(:derived (Flag32-)(and (LEFTOF9-ROBOT)))

(:derived (Flag32-)(and (LEFTOF2-ROBOT)))

(:derived (Flag32-)(and (LEFTOF13-ROBOT)))

(:derived (Flag32-)(and (LEFTOF1-ROBOT)))

(:derived (Flag32-)(and (LEFTOF12-ROBOT)))

(:derived (Flag32-)(and (LEFTOF6-ROBOT)))

(:derived (Flag32-)(and (LEFTOF5-ROBOT)))

(:derived (Flag32-)(and (LEFTOF14-ROBOT)))

(:derived (Flag32-)(and (LEFTOF3-ROBOT)))

(:derived (Flag32-)(and (LEFTOF4-ROBOT)))

(:derived (Flag32-)(and (LEFTOF7-ROBOT)))

(:derived (Flag32-)(and (LEFTOF8-ROBOT)))

(:derived (Flag33-)(and (LEFTOF10-ROBOT)))

(:derived (Flag33-)(and (LEFTOF11-ROBOT)))

(:derived (Flag33-)(and (LEFTOF9-ROBOT)))

(:derived (Flag33-)(and (LEFTOF2-ROBOT)))

(:derived (Flag33-)(and (LEFTOF13-ROBOT)))

(:derived (Flag33-)(and (LEFTOF1-ROBOT)))

(:derived (Flag33-)(and (LEFTOF12-ROBOT)))

(:derived (Flag33-)(and (LEFTOF6-ROBOT)))

(:derived (Flag33-)(and (LEFTOF5-ROBOT)))

(:derived (Flag33-)(and (LEFTOF14-ROBOT)))

(:derived (Flag33-)(and (LEFTOF3-ROBOT)))

(:derived (Flag33-)(and (LEFTOF4-ROBOT)))

(:derived (Flag33-)(and (LEFTOF7-ROBOT)))

(:derived (Flag33-)(and (LEFTOF8-ROBOT)))

(:derived (Flag34-)(and (LEFTOF1-ROBOT)(RIGHTOF4-ROBOT)))

(:derived (Flag35-)(and (LEFTOF1-ROBOT)(RIGHTOF12-ROBOT)))

(:derived (Flag36-)(and (LEFTOF1-ROBOT)(RIGHTOF7-ROBOT)))

(:derived (Flag37-)(and (LEFTOF1-ROBOT)(RIGHTOF9-ROBOT)))

(:derived (Flag38-)(and (RIGHTOF13-ROBOT)(LEFTOF1-ROBOT)))

(:derived (Flag39-)(and (LEFTOF1-ROBOT)(RIGHTOF1-ROBOT)))

(:derived (Flag40-)(and (RIGHTOF3-ROBOT)(LEFTOF1-ROBOT)))

(:derived (Flag41-)(and (LEFTOF1-ROBOT)(RIGHTOF15-ROBOT)))

(:derived (Flag42-)(and (RIGHTOF0-ROBOT)(LEFTOF1-ROBOT)))

(:derived (Flag43-)(and (LEFTOF1-ROBOT)(RIGHTOF5-ROBOT)))

(:derived (Flag44-)(and (LEFTOF1-ROBOT)(RIGHTOF2-ROBOT)))

(:derived (Flag45-)(and (RIGHTOF11-ROBOT)(LEFTOF1-ROBOT)))

(:derived (Flag46-)(and (RIGHTOF8-ROBOT)(LEFTOF1-ROBOT)))

(:derived (Flag47-)(and (LEFTOF1-ROBOT)(RIGHTOF6-ROBOT)))

(:derived (Flag48-)(and (LEFTOF1-ROBOT)(RIGHTOF14-ROBOT)))

(:derived (Flag49-)(and (RIGHTOF10-ROBOT)(LEFTOF1-ROBOT)))

(:derived (Flag51-)(and (LEFTOF2-ROBOT)(RIGHTOF15-ROBOT)))

(:derived (Flag52-)(and (RIGHTOF13-ROBOT)(LEFTOF2-ROBOT)))

(:derived (Flag53-)(and (LEFTOF2-ROBOT)(RIGHTOF9-ROBOT)))

(:derived (Flag54-)(and (LEFTOF2-ROBOT)(RIGHTOF1-ROBOT)))

(:derived (Flag55-)(and (RIGHTOF5-ROBOT)(LEFTOF2-ROBOT)))

(:derived (Flag56-)(and (LEFTOF2-ROBOT)(RIGHTOF7-ROBOT)))

(:derived (Flag57-)(and (RIGHTOF5-ROBOT)(LEFTOF1-ROBOT)))

(:derived (Flag58-)(and (RIGHTOF3-ROBOT)(LEFTOF2-ROBOT)))

(:derived (Flag59-)(and (LEFTOF2-ROBOT)(RIGHTOF14-ROBOT)))

(:derived (Flag60-)(and (RIGHTOF10-ROBOT)(LEFTOF2-ROBOT)))

(:derived (Flag61-)(and (LEFTOF2-ROBOT)(RIGHTOF12-ROBOT)))

(:derived (Flag62-)(and (RIGHTOF11-ROBOT)(LEFTOF2-ROBOT)))

(:derived (Flag63-)(and (RIGHTOF8-ROBOT)(LEFTOF2-ROBOT)))

(:derived (Flag64-)(and (RIGHTOF2-ROBOT)(LEFTOF2-ROBOT)))

(:derived (Flag65-)(and (LEFTOF2-ROBOT)(RIGHTOF6-ROBOT)))

(:derived (Flag66-)(and (LEFTOF2-ROBOT)(RIGHTOF4-ROBOT)))

(:derived (Flag68-)(and (LEFTOF3-ROBOT)(RIGHTOF6-ROBOT)))

(:derived (Flag69-)(and (LEFTOF3-ROBOT)(RIGHTOF14-ROBOT)))

(:derived (Flag70-)(and (LEFTOF3-ROBOT)(RIGHTOF4-ROBOT)))

(:derived (Flag71-)(and (LEFTOF3-ROBOT)(RIGHTOF7-ROBOT)))

(:derived (Flag72-)(and (LEFTOF3-ROBOT)(RIGHTOF12-ROBOT)))

(:derived (Flag73-)(and (LEFTOF3-ROBOT)(RIGHTOF10-ROBOT)))

(:derived (Flag74-)(and (LEFTOF3-ROBOT)(RIGHTOF8-ROBOT)))

(:derived (Flag75-)(and (LEFTOF3-ROBOT)(RIGHTOF5-ROBOT)))

(:derived (Flag76-)(and (LEFTOF3-ROBOT)(RIGHTOF15-ROBOT)))

(:derived (Flag77-)(and (RIGHTOF3-ROBOT)(LEFTOF3-ROBOT)))

(:derived (Flag78-)(and (LEFTOF3-ROBOT)(RIGHTOF11-ROBOT)))

(:derived (Flag79-)(and (RIGHTOF13-ROBOT)(LEFTOF3-ROBOT)))

(:derived (Flag80-)(and (LEFTOF3-ROBOT)(RIGHTOF9-ROBOT)))

(:derived (Flag81-)(and (LEFTOF3-ROBOT)(RIGHTOF2-ROBOT)))

(:derived (Flag83-)(and (LEFTOF3-ROBOT)(RIGHTOF13-ROBOT)))

(:derived (Flag84-)(and (LEFTOF4-ROBOT)(RIGHTOF4-ROBOT)))

(:derived (Flag85-)(and (LEFTOF4-ROBOT)(RIGHTOF7-ROBOT)))

(:derived (Flag86-)(and (LEFTOF4-ROBOT)(RIGHTOF15-ROBOT)))

(:derived (Flag87-)(and (LEFTOF4-ROBOT)(RIGHTOF14-ROBOT)))

(:derived (Flag88-)(and (LEFTOF3-ROBOT)(RIGHTOF3-ROBOT)))

(:derived (Flag89-)(and (RIGHTOF8-ROBOT)(LEFTOF4-ROBOT)))

(:derived (Flag90-)(and (LEFTOF4-ROBOT)(RIGHTOF12-ROBOT)))

(:derived (Flag91-)(and (RIGHTOF10-ROBOT)(LEFTOF4-ROBOT)))

(:derived (Flag92-)(and (LEFTOF4-ROBOT)(RIGHTOF9-ROBOT)))

(:derived (Flag93-)(and (LEFTOF4-ROBOT)(RIGHTOF6-ROBOT)))

(:derived (Flag94-)(and (LEFTOF4-ROBOT)(RIGHTOF5-ROBOT)))

(:derived (Flag95-)(and (RIGHTOF13-ROBOT)(LEFTOF4-ROBOT)))

(:derived (Flag96-)(and (RIGHTOF3-ROBOT)(LEFTOF4-ROBOT)))

(:derived (Flag97-)(and (LEFTOF4-ROBOT)(RIGHTOF11-ROBOT)))

(:derived (Flag99-)(and (RIGHTOF9-ROBOT)(LEFTOF5-ROBOT)))

(:derived (Flag100-)(and (RIGHTOF15-ROBOT)(LEFTOF5-ROBOT)))

(:derived (Flag101-)(and (RIGHTOF4-ROBOT)(LEFTOF5-ROBOT)))

(:derived (Flag102-)(and (RIGHTOF7-ROBOT)(LEFTOF5-ROBOT)))

(:derived (Flag103-)(and (RIGHTOF11-ROBOT)(LEFTOF5-ROBOT)))

(:derived (Flag104-)(and (RIGHTOF13-ROBOT)(LEFTOF5-ROBOT)))

(:derived (Flag105-)(and (LEFTOF4-ROBOT)(RIGHTOF8-ROBOT)))

(:derived (Flag106-)(and (LEFTOF5-ROBOT)(RIGHTOF6-ROBOT)))

(:derived (Flag107-)(and (RIGHTOF10-ROBOT)(LEFTOF5-ROBOT)))

(:derived (Flag108-)(and (LEFTOF4-ROBOT)(RIGHTOF10-ROBOT)))

(:derived (Flag109-)(and (LEFTOF5-ROBOT)(RIGHTOF12-ROBOT)))

(:derived (Flag110-)(and (RIGHTOF8-ROBOT)(LEFTOF5-ROBOT)))

(:derived (Flag111-)(and (RIGHTOF14-ROBOT)(LEFTOF5-ROBOT)))

(:derived (Flag112-)(and (RIGHTOF5-ROBOT)(LEFTOF5-ROBOT)))

(:derived (Flag114-)(and (RIGHTOF15-ROBOT)(LEFTOF6-ROBOT)))

(:derived (Flag115-)(and (RIGHTOF12-ROBOT)(LEFTOF6-ROBOT)))

(:derived (Flag116-)(and (RIGHTOF8-ROBOT)(LEFTOF6-ROBOT)))

(:derived (Flag117-)(and (RIGHTOF11-ROBOT)(LEFTOF6-ROBOT)))

(:derived (Flag118-)(and (RIGHTOF5-ROBOT)(LEFTOF6-ROBOT)))

(:derived (Flag119-)(and (RIGHTOF7-ROBOT)(LEFTOF6-ROBOT)))

(:derived (Flag120-)(and (RIGHTOF10-ROBOT)(LEFTOF6-ROBOT)))

(:derived (Flag121-)(and (RIGHTOF6-ROBOT)(LEFTOF6-ROBOT)))

(:derived (Flag122-)(and (RIGHTOF13-ROBOT)(LEFTOF6-ROBOT)))

(:derived (Flag123-)(and (RIGHTOF14-ROBOT)(LEFTOF6-ROBOT)))

(:derived (Flag124-)(and (RIGHTOF9-ROBOT)(LEFTOF6-ROBOT)))

(:derived (Flag126-)(and (RIGHTOF11-ROBOT)(LEFTOF7-ROBOT)))

(:derived (Flag127-)(and (RIGHTOF8-ROBOT)(LEFTOF7-ROBOT)))

(:derived (Flag128-)(and (LEFTOF7-ROBOT)(RIGHTOF7-ROBOT)))

(:derived (Flag129-)(and (RIGHTOF13-ROBOT)(LEFTOF7-ROBOT)))

(:derived (Flag130-)(and (LEFTOF6-ROBOT)(RIGHTOF6-ROBOT)))

(:derived (Flag131-)(and (LEFTOF7-ROBOT)(RIGHTOF12-ROBOT)))

(:derived (Flag132-)(and (LEFTOF7-ROBOT)(RIGHTOF14-ROBOT)))

(:derived (Flag133-)(and (LEFTOF7-ROBOT)(RIGHTOF9-ROBOT)))

(:derived (Flag134-)(and (LEFTOF7-ROBOT)(RIGHTOF6-ROBOT)))

(:derived (Flag135-)(and (LEFTOF6-ROBOT)(RIGHTOF12-ROBOT)))

(:derived (Flag136-)(and (RIGHTOF10-ROBOT)(LEFTOF7-ROBOT)))

(:derived (Flag137-)(and (LEFTOF7-ROBOT)(RIGHTOF15-ROBOT)))

(:derived (Flag139-)(and (RIGHTOF7-ROBOT)(LEFTOF8-ROBOT)))

(:derived (Flag140-)(and (LEFTOF8-ROBOT)(RIGHTOF9-ROBOT)))

(:derived (Flag141-)(and (RIGHTOF13-ROBOT)(LEFTOF8-ROBOT)))

(:derived (Flag142-)(and (RIGHTOF14-ROBOT)(LEFTOF8-ROBOT)))

(:derived (Flag143-)(and (RIGHTOF10-ROBOT)(LEFTOF8-ROBOT)))

(:derived (Flag144-)(and (RIGHTOF8-ROBOT)(LEFTOF8-ROBOT)))

(:derived (Flag145-)(and (RIGHTOF7-ROBOT)(LEFTOF7-ROBOT)))

(:derived (Flag146-)(and (LEFTOF8-ROBOT)(RIGHTOF12-ROBOT)))

(:derived (Flag147-)(and (LEFTOF8-ROBOT)(RIGHTOF15-ROBOT)))

(:derived (Flag148-)(and (RIGHTOF11-ROBOT)(LEFTOF8-ROBOT)))

(:derived (Flag150-)(and (RIGHTOF13-ROBOT)(LEFTOF9-ROBOT)))

(:derived (Flag151-)(and (RIGHTOF8-ROBOT)(LEFTOF9-ROBOT)))

(:derived (Flag152-)(and (LEFTOF9-ROBOT)(RIGHTOF9-ROBOT)))

(:derived (Flag153-)(and (RIGHTOF10-ROBOT)(LEFTOF9-ROBOT)))

(:derived (Flag154-)(and (LEFTOF8-ROBOT)(RIGHTOF14-ROBOT)))

(:derived (Flag155-)(and (LEFTOF9-ROBOT)(RIGHTOF12-ROBOT)))

(:derived (Flag156-)(and (RIGHTOF11-ROBOT)(LEFTOF9-ROBOT)))

(:derived (Flag157-)(and (LEFTOF9-ROBOT)(RIGHTOF15-ROBOT)))

(:derived (Flag158-)(and (LEFTOF9-ROBOT)(RIGHTOF14-ROBOT)))

(:derived (Flag160-)(and (RIGHTOF14-ROBOT)(LEFTOF10-ROBOT)))

(:derived (Flag161-)(and (RIGHTOF11-ROBOT)(LEFTOF10-ROBOT)))

(:derived (Flag162-)(and (RIGHTOF10-ROBOT)(LEFTOF10-ROBOT)))

(:derived (Flag163-)(and (LEFTOF10-ROBOT)(RIGHTOF12-ROBOT)))

(:derived (Flag164-)(and (RIGHTOF13-ROBOT)(LEFTOF10-ROBOT)))

(:derived (Flag165-)(and (LEFTOF10-ROBOT)(RIGHTOF9-ROBOT)))

(:derived (Flag166-)(and (LEFTOF10-ROBOT)(RIGHTOF15-ROBOT)))

(:derived (Flag168-)(and (LEFTOF11-ROBOT)(RIGHTOF14-ROBOT)))

(:derived (Flag169-)(and (RIGHTOF11-ROBOT)(LEFTOF11-ROBOT)))

(:derived (Flag170-)(and (LEFTOF10-ROBOT)(RIGHTOF14-ROBOT)))

(:derived (Flag171-)(and (RIGHTOF13-ROBOT)(LEFTOF11-ROBOT)))

(:derived (Flag172-)(and (LEFTOF11-ROBOT)(RIGHTOF15-ROBOT)))

(:derived (Flag173-)(and (RIGHTOF10-ROBOT)(LEFTOF11-ROBOT)))

(:derived (Flag174-)(and (LEFTOF11-ROBOT)(RIGHTOF12-ROBOT)))

(:derived (Flag176-)(and (RIGHTOF12-ROBOT)(LEFTOF12-ROBOT)))

(:derived (Flag177-)(and (RIGHTOF15-ROBOT)(LEFTOF12-ROBOT)))

(:derived (Flag178-)(and (RIGHTOF14-ROBOT)(LEFTOF12-ROBOT)))

(:derived (Flag179-)(and (RIGHTOF11-ROBOT)(LEFTOF12-ROBOT)))

(:derived (Flag180-)(and (RIGHTOF13-ROBOT)(LEFTOF12-ROBOT)))

(:derived (Flag182-)(and (LEFTOF13-ROBOT)(RIGHTOF15-ROBOT)))

(:derived (Flag183-)(and (LEFTOF13-ROBOT)(RIGHTOF12-ROBOT)))

(:derived (Flag184-)(and (LEFTOF13-ROBOT)(RIGHTOF14-ROBOT)))

(:derived (Flag185-)(and (RIGHTOF13-ROBOT)(LEFTOF13-ROBOT)))

(:derived (Flag186-)(and (LEFTOF12-ROBOT)(RIGHTOF12-ROBOT)))

(:derived (Flag188-)(and (LEFTOF14-ROBOT)(RIGHTOF14-ROBOT)))

(:derived (Flag189-)(and (RIGHTOF13-ROBOT)(LEFTOF14-ROBOT)))

(:derived (Flag190-)(and (LEFTOF14-ROBOT)(RIGHTOF15-ROBOT)))

(:derived (Flag192-)(and (RIGHTOF15-ROBOT)(LEFTOF15-ROBOT)))

(:derived (Flag193-)(and (RIGHTOF14-ROBOT)(LEFTOF15-ROBOT)))

(:derived (Flag195-)(and (LEFTOF10-ROBOT)))

(:derived (Flag195-)(and (LEFTOF11-ROBOT)))

(:derived (Flag195-)(and (LEFTOF9-ROBOT)))

(:derived (Flag195-)(and (LEFTOF2-ROBOT)))

(:derived (Flag195-)(and (LEFTOF13-ROBOT)))

(:derived (Flag195-)(and (LEFTOF1-ROBOT)))

(:derived (Flag195-)(and (LEFTOF12-ROBOT)))

(:derived (Flag195-)(and (LEFTOF6-ROBOT)))

(:derived (Flag195-)(and (LEFTOF5-ROBOT)))

(:derived (Flag195-)(and (LEFTOF16-ROBOT)))

(:derived (Flag195-)(and (LEFTOF14-ROBOT)))

(:derived (Flag195-)(and (LEFTOF3-ROBOT)))

(:derived (Flag195-)(and (LEFTOF4-ROBOT)))

(:derived (Flag195-)(and (LEFTOF7-ROBOT)))

(:derived (Flag195-)(and (LEFTOF8-ROBOT)))

(:derived (Flag196-)(and (LEFTOF15-ROBOT)(RIGHTOF15-ROBOT)))

(:derived (Flag197-)(and (LEFTOF16-ROBOT)(RIGHTOF15-ROBOT)))

(:derived (Flag258-)(and (Flag228-)))

(:derived (Flag258-)(and (Flag229-)))

(:derived (Flag258-)(and (Flag230-)))

(:derived (Flag258-)(and (Flag231-)))

(:derived (Flag258-)(and (Flag232-)))

(:derived (Flag258-)(and (Flag233-)))

(:derived (Flag258-)(and (Flag234-)))

(:derived (Flag258-)(and (Flag235-)))

(:derived (Flag258-)(and (Flag236-)))

(:derived (Flag258-)(and (Flag237-)))

(:derived (Flag258-)(and (Flag238-)))

(:derived (Flag258-)(and (Flag239-)))

(:derived (Flag258-)(and (Flag240-)))

(:derived (Flag258-)(and (Flag241-)))

(:derived (Flag258-)(and (Flag242-)))

(:derived (Flag258-)(and (Flag243-)))

(:derived (Flag258-)(and (Flag244-)))

(:derived (Flag258-)(and (Flag245-)))

(:derived (Flag258-)(and (Flag246-)))

(:derived (Flag258-)(and (Flag247-)))

(:derived (Flag258-)(and (Flag248-)))

(:derived (Flag258-)(and (Flag249-)))

(:derived (Flag258-)(and (Flag250-)))

(:derived (Flag258-)(and (Flag251-)))

(:derived (Flag258-)(and (Flag252-)))

(:derived (Flag258-)(and (Flag253-)))

(:derived (Flag258-)(and (Flag254-)))

(:derived (Flag258-)(and (Flag255-)))

(:derived (Flag258-)(and (Flag256-)))

(:derived (Flag258-)(and (Flag257-)))

(:derived (Flag274-)(and (Flag259-)))

(:derived (Flag274-)(and (Flag260-)))

(:derived (Flag274-)(and (Flag240-)))

(:derived (Flag274-)(and (Flag261-)))

(:derived (Flag274-)(and (Flag229-)))

(:derived (Flag274-)(and (Flag246-)))

(:derived (Flag274-)(and (Flag231-)))

(:derived (Flag274-)(and (Flag232-)))

(:derived (Flag274-)(and (Flag262-)))

(:derived (Flag274-)(and (Flag233-)))

(:derived (Flag274-)(and (Flag234-)))

(:derived (Flag274-)(and (Flag236-)))

(:derived (Flag274-)(and (Flag263-)))

(:derived (Flag274-)(and (Flag264-)))

(:derived (Flag274-)(and (Flag237-)))

(:derived (Flag274-)(and (Flag238-)))

(:derived (Flag274-)(and (Flag265-)))

(:derived (Flag274-)(and (Flag239-)))

(:derived (Flag274-)(and (Flag266-)))

(:derived (Flag274-)(and (Flag241-)))

(:derived (Flag274-)(and (Flag242-)))

(:derived (Flag274-)(and (Flag267-)))

(:derived (Flag274-)(and (Flag243-)))

(:derived (Flag274-)(and (Flag268-)))

(:derived (Flag274-)(and (Flag245-)))

(:derived (Flag274-)(and (Flag244-)))

(:derived (Flag274-)(and (Flag269-)))

(:derived (Flag274-)(and (Flag270-)))

(:derived (Flag274-)(and (Flag247-)))

(:derived (Flag274-)(and (Flag248-)))

(:derived (Flag274-)(and (Flag249-)))

(:derived (Flag274-)(and (Flag250-)))

(:derived (Flag274-)(and (Flag251-)))

(:derived (Flag274-)(and (Flag252-)))

(:derived (Flag274-)(and (Flag271-)))

(:derived (Flag274-)(and (Flag253-)))

(:derived (Flag274-)(and (Flag272-)))

(:derived (Flag274-)(and (Flag273-)))

(:derived (Flag274-)(and (Flag254-)))

(:derived (Flag274-)(and (Flag255-)))

(:derived (Flag274-)(and (Flag256-)))

(:derived (Flag274-)(and (Flag257-)))

(:derived (Flag290-)(and (Flag262-)))

(:derived (Flag290-)(and (Flag259-)))

(:derived (Flag290-)(and (Flag275-)))

(:derived (Flag290-)(and (Flag276-)))

(:derived (Flag290-)(and (Flag277-)))

(:derived (Flag290-)(and (Flag260-)))

(:derived (Flag290-)(and (Flag261-)))

(:derived (Flag290-)(and (Flag229-)))

(:derived (Flag290-)(and (Flag246-)))

(:derived (Flag290-)(and (Flag231-)))

(:derived (Flag290-)(and (Flag263-)))

(:derived (Flag290-)(and (Flag232-)))

(:derived (Flag290-)(and (Flag278-)))

(:derived (Flag290-)(and (Flag233-)))

(:derived (Flag290-)(and (Flag234-)))

(:derived (Flag290-)(and (Flag279-)))

(:derived (Flag290-)(and (Flag236-)))

(:derived (Flag290-)(and (Flag280-)))

(:derived (Flag290-)(and (Flag250-)))

(:derived (Flag290-)(and (Flag264-)))

(:derived (Flag290-)(and (Flag281-)))

(:derived (Flag290-)(and (Flag237-)))

(:derived (Flag290-)(and (Flag282-)))

(:derived (Flag290-)(and (Flag283-)))

(:derived (Flag290-)(and (Flag284-)))

(:derived (Flag290-)(and (Flag239-)))

(:derived (Flag290-)(and (Flag241-)))

(:derived (Flag290-)(and (Flag242-)))

(:derived (Flag290-)(and (Flag267-)))

(:derived (Flag290-)(and (Flag243-)))

(:derived (Flag290-)(and (Flag244-)))

(:derived (Flag290-)(and (Flag252-)))

(:derived (Flag290-)(and (Flag245-)))

(:derived (Flag290-)(and (Flag268-)))

(:derived (Flag290-)(and (Flag269-)))

(:derived (Flag290-)(and (Flag285-)))

(:derived (Flag290-)(and (Flag247-)))

(:derived (Flag290-)(and (Flag248-)))

(:derived (Flag290-)(and (Flag249-)))

(:derived (Flag290-)(and (Flag286-)))

(:derived (Flag290-)(and (Flag251-)))

(:derived (Flag290-)(and (Flag287-)))

(:derived (Flag290-)(and (Flag271-)))

(:derived (Flag290-)(and (Flag288-)))

(:derived (Flag290-)(and (Flag289-)))

(:derived (Flag290-)(and (Flag272-)))

(:derived (Flag290-)(and (Flag238-)))

(:derived (Flag290-)(and (Flag253-)))

(:derived (Flag290-)(and (Flag273-)))

(:derived (Flag290-)(and (Flag254-)))

(:derived (Flag290-)(and (Flag255-)))

(:derived (Flag290-)(and (Flag256-)))

(:derived (Flag290-)(and (Flag257-)))

(:derived (Flag303-)(and (Flag262-)))

(:derived (Flag303-)(and (Flag291-)))

(:derived (Flag303-)(and (Flag259-)))

(:derived (Flag303-)(and (Flag275-)))

(:derived (Flag303-)(and (Flag281-)))

(:derived (Flag303-)(and (Flag292-)))

(:derived (Flag303-)(and (Flag260-)))

(:derived (Flag303-)(and (Flag293-)))

(:derived (Flag303-)(and (Flag261-)))

(:derived (Flag303-)(and (Flag229-)))

(:derived (Flag303-)(and (Flag231-)))

(:derived (Flag303-)(and (Flag278-)))

(:derived (Flag303-)(and (Flag232-)))

(:derived (Flag303-)(and (Flag286-)))

(:derived (Flag303-)(and (Flag233-)))

(:derived (Flag303-)(and (Flag234-)))

(:derived (Flag303-)(and (Flag254-)))

(:derived (Flag303-)(and (Flag279-)))

(:derived (Flag303-)(and (Flag236-)))

(:derived (Flag303-)(and (Flag294-)))

(:derived (Flag303-)(and (Flag280-)))

(:derived (Flag303-)(and (Flag295-)))

(:derived (Flag303-)(and (Flag264-)))

(:derived (Flag303-)(and (Flag296-)))

(:derived (Flag303-)(and (Flag237-)))

(:derived (Flag303-)(and (Flag282-)))

(:derived (Flag303-)(and (Flag283-)))

(:derived (Flag303-)(and (Flag263-)))

(:derived (Flag303-)(and (Flag239-)))

(:derived (Flag303-)(and (Flag241-)))

(:derived (Flag303-)(and (Flag242-)))

(:derived (Flag303-)(and (Flag267-)))

(:derived (Flag303-)(and (Flag243-)))

(:derived (Flag303-)(and (Flag297-)))

(:derived (Flag303-)(and (Flag268-)))

(:derived (Flag303-)(and (Flag298-)))

(:derived (Flag303-)(and (Flag252-)))

(:derived (Flag303-)(and (Flag245-)))

(:derived (Flag303-)(and (Flag244-)))

(:derived (Flag303-)(and (Flag246-)))

(:derived (Flag303-)(and (Flag285-)))

(:derived (Flag303-)(and (Flag277-)))

(:derived (Flag303-)(and (Flag248-)))

(:derived (Flag303-)(and (Flag249-)))

(:derived (Flag303-)(and (Flag250-)))

(:derived (Flag303-)(and (Flag251-)))

(:derived (Flag303-)(and (Flag299-)))

(:derived (Flag303-)(and (Flag287-)))

(:derived (Flag303-)(and (Flag271-)))

(:derived (Flag303-)(and (Flag288-)))

(:derived (Flag303-)(and (Flag300-)))

(:derived (Flag303-)(and (Flag289-)))

(:derived (Flag303-)(and (Flag272-)))

(:derived (Flag303-)(and (Flag238-)))

(:derived (Flag303-)(and (Flag253-)))

(:derived (Flag303-)(and (Flag273-)))

(:derived (Flag303-)(and (Flag301-)))

(:derived (Flag303-)(and (Flag255-)))

(:derived (Flag303-)(and (Flag256-)))

(:derived (Flag303-)(and (Flag269-)))

(:derived (Flag303-)(and (Flag302-)))

(:derived (Flag317-)(and (Flag289-)))

(:derived (Flag317-)(and (Flag262-)))

(:derived (Flag317-)(and (Flag259-)))

(:derived (Flag317-)(and (Flag254-)))

(:derived (Flag317-)(and (Flag304-)))

(:derived (Flag317-)(and (Flag275-)))

(:derived (Flag317-)(and (Flag305-)))

(:derived (Flag317-)(and (Flag292-)))

(:derived (Flag317-)(and (Flag306-)))

(:derived (Flag317-)(and (Flag260-)))

(:derived (Flag317-)(and (Flag293-)))

(:derived (Flag317-)(and (Flag261-)))

(:derived (Flag317-)(and (Flag307-)))

(:derived (Flag317-)(and (Flag308-)))

(:derived (Flag317-)(and (Flag229-)))

(:derived (Flag317-)(and (Flag309-)))

(:derived (Flag317-)(and (Flag231-)))

(:derived (Flag317-)(and (Flag263-)))

(:derived (Flag317-)(and (Flag232-)))

(:derived (Flag317-)(and (Flag286-)))

(:derived (Flag317-)(and (Flag248-)))

(:derived (Flag317-)(and (Flag233-)))

(:derived (Flag317-)(and (Flag241-)))

(:derived (Flag317-)(and (Flag234-)))

(:derived (Flag317-)(and (Flag300-)))

(:derived (Flag317-)(and (Flag279-)))

(:derived (Flag317-)(and (Flag236-)))

(:derived (Flag317-)(and (Flag310-)))

(:derived (Flag317-)(and (Flag311-)))

(:derived (Flag317-)(and (Flag294-)))

(:derived (Flag317-)(and (Flag280-)))

(:derived (Flag317-)(and (Flag295-)))

(:derived (Flag317-)(and (Flag264-)))

(:derived (Flag317-)(and (Flag281-)))

(:derived (Flag317-)(and (Flag282-)))

(:derived (Flag317-)(and (Flag283-)))

(:derived (Flag317-)(and (Flag239-)))

(:derived (Flag317-)(and (Flag243-)))

(:derived (Flag317-)(and (Flag242-)))

(:derived (Flag317-)(and (Flag267-)))

(:derived (Flag317-)(and (Flag312-)))

(:derived (Flag317-)(and (Flag244-)))

(:derived (Flag317-)(and (Flag298-)))

(:derived (Flag317-)(and (Flag252-)))

(:derived (Flag317-)(and (Flag245-)))

(:derived (Flag317-)(and (Flag246-)))

(:derived (Flag317-)(and (Flag285-)))

(:derived (Flag317-)(and (Flag313-)))

(:derived (Flag317-)(and (Flag277-)))

(:derived (Flag317-)(and (Flag314-)))

(:derived (Flag317-)(and (Flag249-)))

(:derived (Flag317-)(and (Flag250-)))

(:derived (Flag317-)(and (Flag299-)))

(:derived (Flag317-)(and (Flag287-)))

(:derived (Flag317-)(and (Flag271-)))

(:derived (Flag317-)(and (Flag288-)))

(:derived (Flag317-)(and (Flag315-)))

(:derived (Flag317-)(and (Flag272-)))

(:derived (Flag317-)(and (Flag238-)))

(:derived (Flag317-)(and (Flag316-)))

(:derived (Flag317-)(and (Flag253-)))

(:derived (Flag317-)(and (Flag296-)))

(:derived (Flag317-)(and (Flag273-)))

(:derived (Flag317-)(and (Flag301-)))

(:derived (Flag317-)(and (Flag255-)))

(:derived (Flag317-)(and (Flag256-)))

(:derived (Flag317-)(and (Flag269-)))

(:derived (Flag317-)(and (Flag302-)))

(:derived (Flag330-)(and (Flag253-)))

(:derived (Flag330-)(and (Flag262-)))

(:derived (Flag330-)(and (Flag289-)))

(:derived (Flag330-)(and (Flag279-)))

(:derived (Flag330-)(and (Flag318-)))

(:derived (Flag330-)(and (Flag275-)))

(:derived (Flag330-)(and (Flag292-)))

(:derived (Flag330-)(and (Flag306-)))

(:derived (Flag330-)(and (Flag239-)))

(:derived (Flag330-)(and (Flag293-)))

(:derived (Flag330-)(and (Flag316-)))

(:derived (Flag330-)(and (Flag261-)))

(:derived (Flag330-)(and (Flag307-)))

(:derived (Flag330-)(and (Flag308-)))

(:derived (Flag330-)(and (Flag319-)))

(:derived (Flag330-)(and (Flag229-)))

(:derived (Flag330-)(and (Flag309-)))

(:derived (Flag330-)(and (Flag320-)))

(:derived (Flag330-)(and (Flag231-)))

(:derived (Flag330-)(and (Flag263-)))

(:derived (Flag330-)(and (Flag232-)))

(:derived (Flag330-)(and (Flag286-)))

(:derived (Flag330-)(and (Flag248-)))

(:derived (Flag330-)(and (Flag233-)))

(:derived (Flag330-)(and (Flag234-)))

(:derived (Flag330-)(and (Flag300-)))

(:derived (Flag330-)(and (Flag321-)))

(:derived (Flag330-)(and (Flag310-)))

(:derived (Flag330-)(and (Flag294-)))

(:derived (Flag330-)(and (Flag280-)))

(:derived (Flag330-)(and (Flag295-)))

(:derived (Flag330-)(and (Flag264-)))

(:derived (Flag330-)(and (Flag281-)))

(:derived (Flag330-)(and (Flag304-)))

(:derived (Flag330-)(and (Flag282-)))

(:derived (Flag330-)(and (Flag238-)))

(:derived (Flag330-)(and (Flag322-)))

(:derived (Flag330-)(and (Flag260-)))

(:derived (Flag330-)(and (Flag323-)))

(:derived (Flag330-)(and (Flag241-)))

(:derived (Flag330-)(and (Flag242-)))

(:derived (Flag330-)(and (Flag267-)))

(:derived (Flag330-)(and (Flag243-)))

(:derived (Flag330-)(and (Flag244-)))

(:derived (Flag330-)(and (Flag298-)))

(:derived (Flag330-)(and (Flag252-)))

(:derived (Flag330-)(and (Flag245-)))

(:derived (Flag330-)(and (Flag269-)))

(:derived (Flag330-)(and (Flag285-)))

(:derived (Flag330-)(and (Flag313-)))

(:derived (Flag330-)(and (Flag277-)))

(:derived (Flag330-)(and (Flag324-)))

(:derived (Flag330-)(and (Flag314-)))

(:derived (Flag330-)(and (Flag249-)))

(:derived (Flag330-)(and (Flag250-)))

(:derived (Flag330-)(and (Flag299-)))

(:derived (Flag330-)(and (Flag287-)))

(:derived (Flag330-)(and (Flag325-)))

(:derived (Flag330-)(and (Flag326-)))

(:derived (Flag330-)(and (Flag271-)))

(:derived (Flag330-)(and (Flag327-)))

(:derived (Flag330-)(and (Flag315-)))

(:derived (Flag330-)(and (Flag272-)))

(:derived (Flag330-)(and (Flag328-)))

(:derived (Flag330-)(and (Flag329-)))

(:derived (Flag330-)(and (Flag288-)))

(:derived (Flag330-)(and (Flag296-)))

(:derived (Flag330-)(and (Flag273-)))

(:derived (Flag330-)(and (Flag301-)))

(:derived (Flag330-)(and (Flag255-)))

(:derived (Flag330-)(and (Flag256-)))

(:derived (Flag330-)(and (Flag246-)))

(:derived (Flag330-)(and (Flag302-)))

(:derived (Flag341-)(and (Flag253-)))

(:derived (Flag341-)(and (Flag262-)))

(:derived (Flag341-)(and (Flag289-)))

(:derived (Flag341-)(and (Flag279-)))

(:derived (Flag341-)(and (Flag252-)))

(:derived (Flag341-)(and (Flag304-)))

(:derived (Flag341-)(and (Flag275-)))

(:derived (Flag341-)(and (Flag306-)))

(:derived (Flag341-)(and (Flag326-)))

(:derived (Flag341-)(and (Flag331-)))

(:derived (Flag341-)(and (Flag316-)))

(:derived (Flag341-)(and (Flag261-)))

(:derived (Flag341-)(and (Flag323-)))

(:derived (Flag341-)(and (Flag307-)))

(:derived (Flag341-)(and (Flag308-)))

(:derived (Flag341-)(and (Flag319-)))

(:derived (Flag341-)(and (Flag229-)))

(:derived (Flag341-)(and (Flag309-)))

(:derived (Flag341-)(and (Flag246-)))

(:derived (Flag341-)(and (Flag231-)))

(:derived (Flag341-)(and (Flag263-)))

(:derived (Flag341-)(and (Flag232-)))

(:derived (Flag341-)(and (Flag286-)))

(:derived (Flag341-)(and (Flag248-)))

(:derived (Flag341-)(and (Flag233-)))

(:derived (Flag341-)(and (Flag234-)))

(:derived (Flag341-)(and (Flag321-)))

(:derived (Flag341-)(and (Flag310-)))

(:derived (Flag341-)(and (Flag332-)))

(:derived (Flag341-)(and (Flag242-)))

(:derived (Flag341-)(and (Flag294-)))

(:derived (Flag341-)(and (Flag280-)))

(:derived (Flag341-)(and (Flag295-)))

(:derived (Flag341-)(and (Flag333-)))

(:derived (Flag341-)(and (Flag281-)))

(:derived (Flag341-)(and (Flag293-)))

(:derived (Flag341-)(and (Flag282-)))

(:derived (Flag341-)(and (Flag238-)))

(:derived (Flag341-)(and (Flag256-)))

(:derived (Flag341-)(and (Flag322-)))

(:derived (Flag341-)(and (Flag296-)))

(:derived (Flag341-)(and (Flag334-)))

(:derived (Flag341-)(and (Flag241-)))

(:derived (Flag341-)(and (Flag264-)))

(:derived (Flag341-)(and (Flag267-)))

(:derived (Flag341-)(and (Flag244-)))

(:derived (Flag341-)(and (Flag298-)))

(:derived (Flag341-)(and (Flag335-)))

(:derived (Flag341-)(and (Flag245-)))

(:derived (Flag341-)(and (Flag269-)))

(:derived (Flag341-)(and (Flag285-)))

(:derived (Flag341-)(and (Flag313-)))

(:derived (Flag341-)(and (Flag292-)))

(:derived (Flag341-)(and (Flag324-)))

(:derived (Flag341-)(and (Flag314-)))

(:derived (Flag341-)(and (Flag249-)))

(:derived (Flag341-)(and (Flag250-)))

(:derived (Flag341-)(and (Flag336-)))

(:derived (Flag341-)(and (Flag299-)))

(:derived (Flag341-)(and (Flag287-)))

(:derived (Flag341-)(and (Flag325-)))

(:derived (Flag341-)(and (Flag271-)))

(:derived (Flag341-)(and (Flag288-)))

(:derived (Flag341-)(and (Flag315-)))

(:derived (Flag341-)(and (Flag272-)))

(:derived (Flag341-)(and (Flag328-)))

(:derived (Flag341-)(and (Flag329-)))

(:derived (Flag341-)(and (Flag337-)))

(:derived (Flag341-)(and (Flag273-)))

(:derived (Flag341-)(and (Flag300-)))

(:derived (Flag341-)(and (Flag255-)))

(:derived (Flag341-)(and (Flag338-)))

(:derived (Flag341-)(and (Flag339-)))

(:derived (Flag341-)(and (Flag340-)))

(:derived (Flag341-)(and (Flag302-)))

(:derived (Flag352-)(and (Flag262-)))

(:derived (Flag352-)(and (Flag342-)))

(:derived (Flag352-)(and (Flag289-)))

(:derived (Flag352-)(and (Flag279-)))

(:derived (Flag352-)(and (Flag252-)))

(:derived (Flag352-)(and (Flag304-)))

(:derived (Flag352-)(and (Flag275-)))

(:derived (Flag352-)(and (Flag328-)))

(:derived (Flag352-)(and (Flag315-)))

(:derived (Flag352-)(and (Flag306-)))

(:derived (Flag352-)(and (Flag326-)))

(:derived (Flag352-)(and (Flag293-)))

(:derived (Flag352-)(and (Flag329-)))

(:derived (Flag352-)(and (Flag343-)))

(:derived (Flag352-)(and (Flag307-)))

(:derived (Flag352-)(and (Flag308-)))

(:derived (Flag352-)(and (Flag319-)))

(:derived (Flag352-)(and (Flag229-)))

(:derived (Flag352-)(and (Flag309-)))

(:derived (Flag352-)(and (Flag246-)))

(:derived (Flag352-)(and (Flag263-)))

(:derived (Flag352-)(and (Flag232-)))

(:derived (Flag352-)(and (Flag344-)))

(:derived (Flag352-)(and (Flag286-)))

(:derived (Flag352-)(and (Flag248-)))

(:derived (Flag352-)(and (Flag233-)))

(:derived (Flag352-)(and (Flag321-)))

(:derived (Flag352-)(and (Flag345-)))

(:derived (Flag352-)(and (Flag332-)))

(:derived (Flag352-)(and (Flag242-)))

(:derived (Flag352-)(and (Flag294-)))

(:derived (Flag352-)(and (Flag280-)))

(:derived (Flag352-)(and (Flag295-)))

(:derived (Flag352-)(and (Flag333-)))

(:derived (Flag352-)(and (Flag296-)))

(:derived (Flag352-)(and (Flag331-)))

(:derived (Flag352-)(and (Flag282-)))

(:derived (Flag352-)(and (Flag238-)))

(:derived (Flag352-)(and (Flag256-)))

(:derived (Flag352-)(and (Flag322-)))

(:derived (Flag352-)(and (Flag323-)))

(:derived (Flag352-)(and (Flag241-)))

(:derived (Flag352-)(and (Flag264-)))

(:derived (Flag352-)(and (Flag267-)))

(:derived (Flag352-)(and (Flag244-)))

(:derived (Flag352-)(and (Flag335-)))

(:derived (Flag352-)(and (Flag245-)))

(:derived (Flag352-)(and (Flag269-)))

(:derived (Flag352-)(and (Flag285-)))

(:derived (Flag352-)(and (Flag313-)))

(:derived (Flag352-)(and (Flag292-)))

(:derived (Flag352-)(and (Flag324-)))

(:derived (Flag352-)(and (Flag314-)))

(:derived (Flag352-)(and (Flag249-)))

(:derived (Flag352-)(and (Flag250-)))

(:derived (Flag352-)(and (Flag346-)))

(:derived (Flag352-)(and (Flag299-)))

(:derived (Flag352-)(and (Flag287-)))

(:derived (Flag352-)(and (Flag325-)))

(:derived (Flag352-)(and (Flag347-)))

(:derived (Flag352-)(and (Flag271-)))

(:derived (Flag352-)(and (Flag288-)))

(:derived (Flag352-)(and (Flag348-)))

(:derived (Flag352-)(and (Flag272-)))

(:derived (Flag352-)(and (Flag349-)))

(:derived (Flag352-)(and (Flag316-)))

(:derived (Flag352-)(and (Flag253-)))

(:derived (Flag352-)(and (Flag273-)))

(:derived (Flag352-)(and (Flag300-)))

(:derived (Flag352-)(and (Flag350-)))

(:derived (Flag352-)(and (Flag351-)))

(:derived (Flag352-)(and (Flag255-)))

(:derived (Flag352-)(and (Flag338-)))

(:derived (Flag352-)(and (Flag339-)))

(:derived (Flag352-)(and (Flag340-)))

(:derived (Flag352-)(and (Flag302-)))

(:derived (Flag361-)(and (Flag262-)))

(:derived (Flag361-)(and (Flag342-)))

(:derived (Flag361-)(and (Flag289-)))

(:derived (Flag361-)(and (Flag279-)))

(:derived (Flag361-)(and (Flag252-)))

(:derived (Flag361-)(and (Flag304-)))

(:derived (Flag361-)(and (Flag275-)))

(:derived (Flag361-)(and (Flag315-)))

(:derived (Flag361-)(and (Flag306-)))

(:derived (Flag361-)(and (Flag326-)))

(:derived (Flag361-)(and (Flag331-)))

(:derived (Flag361-)(and (Flag316-)))

(:derived (Flag361-)(and (Flag343-)))

(:derived (Flag361-)(and (Flag307-)))

(:derived (Flag361-)(and (Flag308-)))

(:derived (Flag361-)(and (Flag319-)))

(:derived (Flag361-)(and (Flag229-)))

(:derived (Flag361-)(and (Flag353-)))

(:derived (Flag361-)(and (Flag354-)))

(:derived (Flag361-)(and (Flag232-)))

(:derived (Flag361-)(and (Flag355-)))

(:derived (Flag361-)(and (Flag314-)))

(:derived (Flag361-)(and (Flag233-)))

(:derived (Flag361-)(and (Flag246-)))

(:derived (Flag361-)(and (Flag321-)))

(:derived (Flag361-)(and (Flag345-)))

(:derived (Flag361-)(and (Flag332-)))

(:derived (Flag361-)(and (Flag242-)))

(:derived (Flag361-)(and (Flag294-)))

(:derived (Flag361-)(and (Flag263-)))

(:derived (Flag361-)(and (Flag295-)))

(:derived (Flag361-)(and (Flag333-)))

(:derived (Flag361-)(and (Flag296-)))

(:derived (Flag361-)(and (Flag302-)))

(:derived (Flag361-)(and (Flag238-)))

(:derived (Flag361-)(and (Flag322-)))

(:derived (Flag361-)(and (Flag255-)))

(:derived (Flag361-)(and (Flag323-)))

(:derived (Flag361-)(and (Flag356-)))

(:derived (Flag361-)(and (Flag241-)))

(:derived (Flag361-)(and (Flag264-)))

(:derived (Flag361-)(and (Flag282-)))

(:derived (Flag361-)(and (Flag244-)))

(:derived (Flag361-)(and (Flag309-)))

(:derived (Flag361-)(and (Flag335-)))

(:derived (Flag361-)(and (Flag245-)))

(:derived (Flag361-)(and (Flag357-)))

(:derived (Flag361-)(and (Flag269-)))

(:derived (Flag361-)(and (Flag285-)))

(:derived (Flag361-)(and (Flag292-)))

(:derived (Flag361-)(and (Flag324-)))

(:derived (Flag361-)(and (Flag248-)))

(:derived (Flag361-)(and (Flag249-)))

(:derived (Flag361-)(and (Flag286-)))

(:derived (Flag361-)(and (Flag358-)))

(:derived (Flag361-)(and (Flag346-)))

(:derived (Flag361-)(and (Flag359-)))

(:derived (Flag361-)(and (Flag299-)))

(:derived (Flag361-)(and (Flag287-)))

(:derived (Flag361-)(and (Flag325-)))

(:derived (Flag361-)(and (Flag271-)))

(:derived (Flag361-)(and (Flag288-)))

(:derived (Flag361-)(and (Flag348-)))

(:derived (Flag361-)(and (Flag272-)))

(:derived (Flag361-)(and (Flag349-)))

(:derived (Flag361-)(and (Flag329-)))

(:derived (Flag361-)(and (Flag253-)))

(:derived (Flag361-)(and (Flag273-)))

(:derived (Flag361-)(and (Flag300-)))

(:derived (Flag361-)(and (Flag351-)))

(:derived (Flag361-)(and (Flag360-)))

(:derived (Flag361-)(and (Flag338-)))

(:derived (Flag361-)(and (Flag339-)))

(:derived (Flag361-)(and (Flag340-)))

(:derived (Flag368-)(and (Flag262-)))

(:derived (Flag368-)(and (Flag342-)))

(:derived (Flag368-)(and (Flag289-)))

(:derived (Flag368-)(and (Flag304-)))

(:derived (Flag368-)(and (Flag275-)))

(:derived (Flag368-)(and (Flag315-)))

(:derived (Flag368-)(and (Flag306-)))

(:derived (Flag368-)(and (Flag326-)))

(:derived (Flag368-)(and (Flag331-)))

(:derived (Flag368-)(and (Flag343-)))

(:derived (Flag368-)(and (Flag323-)))

(:derived (Flag368-)(and (Flag362-)))

(:derived (Flag368-)(and (Flag308-)))

(:derived (Flag368-)(and (Flag229-)))

(:derived (Flag368-)(and (Flag353-)))

(:derived (Flag368-)(and (Flag363-)))

(:derived (Flag368-)(and (Flag246-)))

(:derived (Flag368-)(and (Flag232-)))

(:derived (Flag368-)(and (Flag355-)))

(:derived (Flag368-)(and (Flag307-)))

(:derived (Flag368-)(and (Flag233-)))

(:derived (Flag368-)(and (Flag364-)))

(:derived (Flag368-)(and (Flag321-)))

(:derived (Flag368-)(and (Flag345-)))

(:derived (Flag368-)(and (Flag332-)))

(:derived (Flag368-)(and (Flag242-)))

(:derived (Flag368-)(and (Flag294-)))

(:derived (Flag368-)(and (Flag295-)))

(:derived (Flag368-)(and (Flag333-)))

(:derived (Flag368-)(and (Flag296-)))

(:derived (Flag368-)(and (Flag325-)))

(:derived (Flag368-)(and (Flag238-)))

(:derived (Flag368-)(and (Flag322-)))

(:derived (Flag368-)(and (Flag255-)))

(:derived (Flag368-)(and (Flag365-)))

(:derived (Flag368-)(and (Flag356-)))

(:derived (Flag368-)(and (Flag264-)))

(:derived (Flag368-)(and (Flag282-)))

(:derived (Flag368-)(and (Flag244-)))

(:derived (Flag368-)(and (Flag309-)))

(:derived (Flag368-)(and (Flag252-)))

(:derived (Flag368-)(and (Flag245-)))

(:derived (Flag368-)(and (Flag357-)))

(:derived (Flag368-)(and (Flag269-)))

(:derived (Flag368-)(and (Flag285-)))

(:derived (Flag368-)(and (Flag292-)))

(:derived (Flag368-)(and (Flag324-)))

(:derived (Flag368-)(and (Flag366-)))

(:derived (Flag368-)(and (Flag249-)))

(:derived (Flag368-)(and (Flag286-)))

(:derived (Flag368-)(and (Flag346-)))

(:derived (Flag368-)(and (Flag359-)))

(:derived (Flag368-)(and (Flag287-)))

(:derived (Flag368-)(and (Flag271-)))

(:derived (Flag368-)(and (Flag288-)))

(:derived (Flag368-)(and (Flag367-)))

(:derived (Flag368-)(and (Flag348-)))

(:derived (Flag368-)(and (Flag272-)))

(:derived (Flag368-)(and (Flag349-)))

(:derived (Flag368-)(and (Flag329-)))

(:derived (Flag368-)(and (Flag253-)))

(:derived (Flag368-)(and (Flag273-)))

(:derived (Flag368-)(and (Flag300-)))

(:derived (Flag368-)(and (Flag351-)))

(:derived (Flag368-)(and (Flag360-)))

(:derived (Flag368-)(and (Flag338-)))

(:derived (Flag368-)(and (Flag339-)))

(:derived (Flag368-)(and (Flag340-)))

(:derived (Flag368-)(and (Flag302-)))

(:derived (Flag374-)(and (Flag342-)))

(:derived (Flag374-)(and (Flag289-)))

(:derived (Flag374-)(and (Flag304-)))

(:derived (Flag374-)(and (Flag275-)))

(:derived (Flag374-)(and (Flag315-)))

(:derived (Flag374-)(and (Flag306-)))

(:derived (Flag374-)(and (Flag326-)))

(:derived (Flag374-)(and (Flag331-)))

(:derived (Flag374-)(and (Flag323-)))

(:derived (Flag374-)(and (Flag307-)))

(:derived (Flag374-)(and (Flag308-)))

(:derived (Flag374-)(and (Flag369-)))

(:derived (Flag374-)(and (Flag229-)))

(:derived (Flag374-)(and (Flag353-)))

(:derived (Flag374-)(and (Flag363-)))

(:derived (Flag374-)(and (Flag370-)))

(:derived (Flag374-)(and (Flag232-)))

(:derived (Flag374-)(and (Flag355-)))

(:derived (Flag374-)(and (Flag233-)))

(:derived (Flag374-)(and (Flag364-)))

(:derived (Flag374-)(and (Flag238-)))

(:derived (Flag374-)(and (Flag300-)))

(:derived (Flag374-)(and (Flag345-)))

(:derived (Flag374-)(and (Flag242-)))

(:derived (Flag374-)(and (Flag294-)))

(:derived (Flag374-)(and (Flag333-)))

(:derived (Flag374-)(and (Flag296-)))

(:derived (Flag374-)(and (Flag325-)))

(:derived (Flag374-)(and (Flag371-)))

(:derived (Flag374-)(and (Flag255-)))

(:derived (Flag374-)(and (Flag365-)))

(:derived (Flag374-)(and (Flag372-)))

(:derived (Flag374-)(and (Flag264-)))

(:derived (Flag374-)(and (Flag282-)))

(:derived (Flag374-)(and (Flag244-)))

(:derived (Flag374-)(and (Flag252-)))

(:derived (Flag374-)(and (Flag357-)))

(:derived (Flag374-)(and (Flag246-)))

(:derived (Flag374-)(and (Flag292-)))

(:derived (Flag374-)(and (Flag324-)))

(:derived (Flag374-)(and (Flag309-)))

(:derived (Flag374-)(and (Flag286-)))

(:derived (Flag374-)(and (Flag346-)))

(:derived (Flag374-)(and (Flag359-)))

(:derived (Flag374-)(and (Flag287-)))

(:derived (Flag374-)(and (Flag366-)))

(:derived (Flag374-)(and (Flag288-)))

(:derived (Flag374-)(and (Flag271-)))

(:derived (Flag374-)(and (Flag367-)))

(:derived (Flag374-)(and (Flag348-)))

(:derived (Flag374-)(and (Flag272-)))

(:derived (Flag374-)(and (Flag349-)))

(:derived (Flag374-)(and (Flag329-)))

(:derived (Flag374-)(and (Flag253-)))

(:derived (Flag374-)(and (Flag262-)))

(:derived (Flag374-)(and (Flag373-)))

(:derived (Flag374-)(and (Flag351-)))

(:derived (Flag374-)(and (Flag360-)))

(:derived (Flag374-)(and (Flag338-)))

(:derived (Flag374-)(and (Flag339-)))

(:derived (Flag374-)(and (Flag340-)))

(:derived (Flag374-)(and (Flag302-)))

(:derived (Flag380-)(and (Flag342-)))

(:derived (Flag380-)(and (Flag375-)))

(:derived (Flag380-)(and (Flag272-)))

(:derived (Flag380-)(and (Flag304-)))

(:derived (Flag380-)(and (Flag275-)))

(:derived (Flag380-)(and (Flag315-)))

(:derived (Flag380-)(and (Flag326-)))

(:derived (Flag380-)(and (Flag331-)))

(:derived (Flag380-)(and (Flag376-)))

(:derived (Flag380-)(and (Flag323-)))

(:derived (Flag380-)(and (Flag307-)))

(:derived (Flag380-)(and (Flag308-)))

(:derived (Flag380-)(and (Flag369-)))

(:derived (Flag380-)(and (Flag353-)))

(:derived (Flag380-)(and (Flag232-)))

(:derived (Flag380-)(and (Flag355-)))

(:derived (Flag380-)(and (Flag233-)))

(:derived (Flag380-)(and (Flag364-)))

(:derived (Flag380-)(and (Flag300-)))

(:derived (Flag380-)(and (Flag345-)))

(:derived (Flag380-)(and (Flag242-)))

(:derived (Flag380-)(and (Flag294-)))

(:derived (Flag380-)(and (Flag377-)))

(:derived (Flag380-)(and (Flag325-)))

(:derived (Flag380-)(and (Flag371-)))

(:derived (Flag380-)(and (Flag255-)))

(:derived (Flag380-)(and (Flag365-)))

(:derived (Flag380-)(and (Flag378-)))

(:derived (Flag380-)(and (Flag264-)))

(:derived (Flag380-)(and (Flag282-)))

(:derived (Flag380-)(and (Flag244-)))

(:derived (Flag380-)(and (Flag289-)))

(:derived (Flag380-)(and (Flag357-)))

(:derived (Flag380-)(and (Flag292-)))

(:derived (Flag380-)(and (Flag324-)))

(:derived (Flag380-)(and (Flag309-)))

(:derived (Flag380-)(and (Flag286-)))

(:derived (Flag380-)(and (Flag346-)))

(:derived (Flag380-)(and (Flag252-)))

(:derived (Flag380-)(and (Flag366-)))

(:derived (Flag380-)(and (Flag253-)))

(:derived (Flag380-)(and (Flag271-)))

(:derived (Flag380-)(and (Flag367-)))

(:derived (Flag380-)(and (Flag348-)))

(:derived (Flag380-)(and (Flag379-)))

(:derived (Flag380-)(and (Flag238-)))

(:derived (Flag380-)(and (Flag262-)))

(:derived (Flag380-)(and (Flag373-)))

(:derived (Flag380-)(and (Flag351-)))

(:derived (Flag380-)(and (Flag360-)))

(:derived (Flag380-)(and (Flag338-)))

(:derived (Flag380-)(and (Flag340-)))

(:derived (Flag380-)(and (Flag302-)))

(:derived (Flag385-)(and (Flag375-)))

(:derived (Flag385-)(and (Flag304-)))

(:derived (Flag385-)(and (Flag275-)))

(:derived (Flag385-)(and (Flag326-)))

(:derived (Flag385-)(and (Flag331-)))

(:derived (Flag385-)(and (Flag323-)))

(:derived (Flag385-)(and (Flag308-)))

(:derived (Flag385-)(and (Flag369-)))

(:derived (Flag385-)(and (Flag381-)))

(:derived (Flag385-)(and (Flag353-)))

(:derived (Flag385-)(and (Flag232-)))

(:derived (Flag385-)(and (Flag355-)))

(:derived (Flag385-)(and (Flag382-)))

(:derived (Flag385-)(and (Flag233-)))

(:derived (Flag385-)(and (Flag364-)))

(:derived (Flag385-)(and (Flag345-)))

(:derived (Flag385-)(and (Flag242-)))

(:derived (Flag385-)(and (Flag383-)))

(:derived (Flag385-)(and (Flag377-)))

(:derived (Flag385-)(and (Flag325-)))

(:derived (Flag385-)(and (Flag371-)))

(:derived (Flag385-)(and (Flag255-)))

(:derived (Flag385-)(and (Flag365-)))

(:derived (Flag385-)(and (Flag264-)))

(:derived (Flag385-)(and (Flag244-)))

(:derived (Flag385-)(and (Flag384-)))

(:derived (Flag385-)(and (Flag289-)))

(:derived (Flag385-)(and (Flag292-)))

(:derived (Flag385-)(and (Flag366-)))

(:derived (Flag385-)(and (Flag309-)))

(:derived (Flag385-)(and (Flag286-)))

(:derived (Flag385-)(and (Flag346-)))

(:derived (Flag385-)(and (Flag302-)))

(:derived (Flag385-)(and (Flag315-)))

(:derived (Flag385-)(and (Flag272-)))

(:derived (Flag385-)(and (Flag238-)))

(:derived (Flag385-)(and (Flag262-)))

(:derived (Flag385-)(and (Flag373-)))

(:derived (Flag385-)(and (Flag351-)))

(:derived (Flag385-)(and (Flag360-)))

(:derived (Flag385-)(and (Flag338-)))

(:derived (Flag385-)(and (Flag340-)))

(:derived (Flag388-)(and (Flag375-)))

(:derived (Flag388-)(and (Flag304-)))

(:derived (Flag388-)(and (Flag315-)))

(:derived (Flag388-)(and (Flag365-)))

(:derived (Flag388-)(and (Flag386-)))

(:derived (Flag388-)(and (Flag369-)))

(:derived (Flag388-)(and (Flag382-)))

(:derived (Flag388-)(and (Flag353-)))

(:derived (Flag388-)(and (Flag387-)))

(:derived (Flag388-)(and (Flag232-)))

(:derived (Flag388-)(and (Flag355-)))

(:derived (Flag388-)(and (Flag381-)))

(:derived (Flag388-)(and (Flag345-)))

(:derived (Flag388-)(and (Flag377-)))

(:derived (Flag388-)(and (Flag302-)))

(:derived (Flag388-)(and (Flag238-)))

(:derived (Flag388-)(and (Flag323-)))

(:derived (Flag388-)(and (Flag264-)))

(:derived (Flag388-)(and (Flag244-)))

(:derived (Flag388-)(and (Flag292-)))

(:derived (Flag388-)(and (Flag366-)))

(:derived (Flag388-)(and (Flag286-)))

(:derived (Flag388-)(and (Flag325-)))

(:derived (Flag388-)(and (Flag289-)))

(:derived (Flag388-)(and (Flag262-)))

(:derived (Flag388-)(and (Flag373-)))

(:derived (Flag388-)(and (Flag351-)))

(:derived (Flag388-)(and (Flag255-)))

(:derived (Flag388-)(and (Flag338-)))

(:derived (Flag388-)(and (Flag340-)))

(:derived (Flag390-)(and (Flag387-)))

(:derived (Flag390-)(and (Flag389-)))

(:derived (Flag390-)(and (Flag232-)))

(:derived (Flag390-)(and (Flag323-)))

(:derived (Flag390-)(and (Flag264-)))

(:derived (Flag390-)(and (Flag304-)))

(:derived (Flag390-)(and (Flag365-)))

(:derived (Flag390-)(and (Flag351-)))

(:derived (Flag390-)(and (Flag255-)))

(:derived (Flag390-)(and (Flag338-)))

(:derived (Flag390-)(and (Flag292-)))

(:derived (Flag390-)(and (Flag369-)))

(:derived (Flag390-)(and (Flag377-)))

(:derived (Flag390-)(and (Flag382-)))

(:derived (Flag390-)(and (Flag353-)))

(:derived (Flag390-)(and (Flag286-)))

(:derived (Flag391-)(and (Flag251-)))

(:derived (Flag391-)(and (Flag240-)))

(:derived (Flag391-)(and (Flag235-)))

(:derived (Flag391-)(and (Flag253-)))

(:derived (Flag391-)(and (Flag242-)))

(:derived (Flag391-)(and (Flag234-)))

(:derived (Flag391-)(and (Flag229-)))

(:derived (Flag391-)(and (Flag243-)))

(:derived (Flag391-)(and (Flag236-)))

(:derived (Flag391-)(and (Flag244-)))

(:derived (Flag391-)(and (Flag245-)))

(:derived (Flag391-)(and (Flag255-)))

(:derived (Flag391-)(and (Flag256-)))

(:derived (Flag391-)(and (Flag247-)))

(:derived (Flag391-)(and (Flag248-)))

(:action MOVELEFT-ROBOT
:parameters ()
:precondition
(and
(Flag4-)
)
:effect
(and
(when
(and
(Flag198-)
)
(and
(COLUMN14-ROBOT)
(not (NOT-COLUMN14-ROBOT))
)
)
(when
(and
(Flag194-)
)
(and
(COLUMN13-ROBOT)
(not (NOT-COLUMN13-ROBOT))
)
)
(when
(and
(Flag191-)
)
(and
(COLUMN12-ROBOT)
(not (NOT-COLUMN12-ROBOT))
)
)
(when
(and
(Flag187-)
)
(and
(COLUMN11-ROBOT)
(not (NOT-COLUMN11-ROBOT))
)
)
(when
(and
(Flag181-)
)
(and
(COLUMN10-ROBOT)
(not (NOT-COLUMN10-ROBOT))
)
)
(when
(and
(Flag175-)
)
(and
(COLUMN9-ROBOT)
(not (NOT-COLUMN9-ROBOT))
)
)
(when
(and
(Flag167-)
)
(and
(COLUMN8-ROBOT)
(not (NOT-COLUMN8-ROBOT))
)
)
(when
(and
(Flag159-)
)
(and
(COLUMN7-ROBOT)
(not (NOT-COLUMN7-ROBOT))
)
)
(when
(and
(Flag149-)
)
(and
(COLUMN6-ROBOT)
(not (NOT-COLUMN6-ROBOT))
)
)
(when
(and
(Flag138-)
)
(and
(COLUMN5-ROBOT)
(not (NOT-COLUMN5-ROBOT))
)
)
(when
(and
(Flag125-)
)
(and
(COLUMN4-ROBOT)
(not (NOT-COLUMN4-ROBOT))
)
)
(when
(and
(Flag113-)
)
(and
(COLUMN3-ROBOT)
(not (NOT-COLUMN3-ROBOT))
)
)
(when
(and
(Flag98-)
)
(and
(COLUMN2-ROBOT)
(not (NOT-COLUMN2-ROBOT))
)
)
(when
(and
(Flag82-)
)
(and
(COLUMN1-ROBOT)
(not (NOT-COLUMN1-ROBOT))
)
)
(when
(and
(Flag67-)
)
(and
(COLUMN0-ROBOT)
(not (NOT-COLUMN0-ROBOT))
)
)
(when
(and
(COLUMN15-ROBOT)
)
(and
(COLUMN14-ROBOT)
(NOT-COLUMN15-ROBOT)
(not (NOT-COLUMN14-ROBOT))
(not (COLUMN15-ROBOT))
)
)
(when
(and
(COLUMN14-ROBOT)
)
(and
(COLUMN13-ROBOT)
(NOT-COLUMN14-ROBOT)
(not (NOT-COLUMN13-ROBOT))
(not (COLUMN14-ROBOT))
)
)
(when
(and
(COLUMN13-ROBOT)
)
(and
(COLUMN12-ROBOT)
(NOT-COLUMN13-ROBOT)
(not (NOT-COLUMN12-ROBOT))
(not (COLUMN13-ROBOT))
)
)
(when
(and
(COLUMN12-ROBOT)
)
(and
(COLUMN11-ROBOT)
(NOT-COLUMN12-ROBOT)
(not (NOT-COLUMN11-ROBOT))
(not (COLUMN12-ROBOT))
)
)
(when
(and
(COLUMN11-ROBOT)
)
(and
(COLUMN10-ROBOT)
(NOT-COLUMN11-ROBOT)
(not (NOT-COLUMN10-ROBOT))
(not (COLUMN11-ROBOT))
)
)
(when
(and
(COLUMN10-ROBOT)
)
(and
(COLUMN9-ROBOT)
(NOT-COLUMN10-ROBOT)
(not (NOT-COLUMN9-ROBOT))
(not (COLUMN10-ROBOT))
)
)
(when
(and
(COLUMN9-ROBOT)
)
(and
(COLUMN8-ROBOT)
(NOT-COLUMN9-ROBOT)
(not (NOT-COLUMN8-ROBOT))
(not (COLUMN9-ROBOT))
)
)
(when
(and
(COLUMN8-ROBOT)
)
(and
(COLUMN7-ROBOT)
(NOT-COLUMN8-ROBOT)
(not (NOT-COLUMN7-ROBOT))
(not (COLUMN8-ROBOT))
)
)
(when
(and
(COLUMN7-ROBOT)
)
(and
(COLUMN6-ROBOT)
(NOT-COLUMN7-ROBOT)
(not (NOT-COLUMN6-ROBOT))
(not (COLUMN7-ROBOT))
)
)
(when
(and
(COLUMN6-ROBOT)
)
(and
(COLUMN5-ROBOT)
(NOT-COLUMN6-ROBOT)
(not (NOT-COLUMN5-ROBOT))
(not (COLUMN6-ROBOT))
)
)
(when
(and
(COLUMN5-ROBOT)
)
(and
(COLUMN4-ROBOT)
(NOT-COLUMN5-ROBOT)
(not (NOT-COLUMN4-ROBOT))
(not (COLUMN5-ROBOT))
)
)
(when
(and
(COLUMN4-ROBOT)
)
(and
(COLUMN3-ROBOT)
(NOT-COLUMN4-ROBOT)
(not (NOT-COLUMN3-ROBOT))
(not (COLUMN4-ROBOT))
)
)
(when
(and
(COLUMN3-ROBOT)
)
(and
(COLUMN2-ROBOT)
(NOT-COLUMN3-ROBOT)
(not (NOT-COLUMN2-ROBOT))
(not (COLUMN3-ROBOT))
)
)
(when
(and
(COLUMN2-ROBOT)
)
(and
(COLUMN1-ROBOT)
(NOT-COLUMN2-ROBOT)
(not (NOT-COLUMN1-ROBOT))
(not (COLUMN2-ROBOT))
)
)
(when
(and
(COLUMN1-ROBOT)
)
(and
(COLUMN0-ROBOT)
(NOT-COLUMN1-ROBOT)
(not (NOT-COLUMN0-ROBOT))
(not (COLUMN1-ROBOT))
)
)
(when
(and
(RIGHTOF15-ROBOT)
)
(and
(RIGHTOF14-ROBOT)
(NOT-RIGHTOF15-ROBOT)
(RIGHTOF13-ROBOT)
(NOT-RIGHTOF14-ROBOT)
(RIGHTOF12-ROBOT)
(NOT-RIGHTOF13-ROBOT)
(RIGHTOF11-ROBOT)
(NOT-RIGHTOF12-ROBOT)
(RIGHTOF10-ROBOT)
(NOT-RIGHTOF11-ROBOT)
(RIGHTOF9-ROBOT)
(NOT-RIGHTOF10-ROBOT)
(RIGHTOF8-ROBOT)
(NOT-RIGHTOF9-ROBOT)
(RIGHTOF7-ROBOT)
(NOT-RIGHTOF8-ROBOT)
(RIGHTOF6-ROBOT)
(NOT-RIGHTOF7-ROBOT)
(RIGHTOF5-ROBOT)
(NOT-RIGHTOF6-ROBOT)
(RIGHTOF4-ROBOT)
(NOT-RIGHTOF5-ROBOT)
(RIGHTOF3-ROBOT)
(NOT-RIGHTOF4-ROBOT)
(RIGHTOF2-ROBOT)
(NOT-RIGHTOF3-ROBOT)
(RIGHTOF1-ROBOT)
(NOT-RIGHTOF2-ROBOT)
(RIGHTOF0-ROBOT)
(NOT-RIGHTOF1-ROBOT)
(not (RIGHTOF15-ROBOT))
(not (NOT-RIGHTOF0-ROBOT))
)
)
(when
(and
(RIGHTOF14-ROBOT)
)
(and
(RIGHTOF13-ROBOT)
(NOT-RIGHTOF14-ROBOT)
(RIGHTOF12-ROBOT)
(NOT-RIGHTOF13-ROBOT)
(RIGHTOF11-ROBOT)
(NOT-RIGHTOF12-ROBOT)
(RIGHTOF10-ROBOT)
(NOT-RIGHTOF11-ROBOT)
(RIGHTOF9-ROBOT)
(NOT-RIGHTOF10-ROBOT)
(RIGHTOF8-ROBOT)
(NOT-RIGHTOF9-ROBOT)
(RIGHTOF7-ROBOT)
(NOT-RIGHTOF8-ROBOT)
(RIGHTOF6-ROBOT)
(NOT-RIGHTOF7-ROBOT)
(RIGHTOF5-ROBOT)
(NOT-RIGHTOF6-ROBOT)
(RIGHTOF4-ROBOT)
(NOT-RIGHTOF5-ROBOT)
(RIGHTOF3-ROBOT)
(NOT-RIGHTOF4-ROBOT)
(RIGHTOF2-ROBOT)
(NOT-RIGHTOF3-ROBOT)
(RIGHTOF1-ROBOT)
(NOT-RIGHTOF2-ROBOT)
(RIGHTOF0-ROBOT)
(NOT-RIGHTOF1-ROBOT)
(not (RIGHTOF14-ROBOT))
(not (NOT-RIGHTOF0-ROBOT))
)
)
(when
(and
(RIGHTOF13-ROBOT)
)
(and
(RIGHTOF12-ROBOT)
(NOT-RIGHTOF13-ROBOT)
(RIGHTOF11-ROBOT)
(NOT-RIGHTOF12-ROBOT)
(RIGHTOF10-ROBOT)
(NOT-RIGHTOF11-ROBOT)
(RIGHTOF9-ROBOT)
(NOT-RIGHTOF10-ROBOT)
(RIGHTOF8-ROBOT)
(NOT-RIGHTOF9-ROBOT)
(RIGHTOF7-ROBOT)
(NOT-RIGHTOF8-ROBOT)
(RIGHTOF6-ROBOT)
(NOT-RIGHTOF7-ROBOT)
(RIGHTOF5-ROBOT)
(NOT-RIGHTOF6-ROBOT)
(RIGHTOF4-ROBOT)
(NOT-RIGHTOF5-ROBOT)
(RIGHTOF3-ROBOT)
(NOT-RIGHTOF4-ROBOT)
(RIGHTOF2-ROBOT)
(NOT-RIGHTOF3-ROBOT)
(RIGHTOF1-ROBOT)
(NOT-RIGHTOF2-ROBOT)
(RIGHTOF0-ROBOT)
(NOT-RIGHTOF1-ROBOT)
(not (RIGHTOF13-ROBOT))
(not (NOT-RIGHTOF0-ROBOT))
)
)
(when
(and
(RIGHTOF12-ROBOT)
)
(and
(RIGHTOF11-ROBOT)
(NOT-RIGHTOF12-ROBOT)
(RIGHTOF10-ROBOT)
(NOT-RIGHTOF11-ROBOT)
(RIGHTOF9-ROBOT)
(NOT-RIGHTOF10-ROBOT)
(RIGHTOF8-ROBOT)
(NOT-RIGHTOF9-ROBOT)
(RIGHTOF7-ROBOT)
(NOT-RIGHTOF8-ROBOT)
(RIGHTOF6-ROBOT)
(NOT-RIGHTOF7-ROBOT)
(RIGHTOF5-ROBOT)
(NOT-RIGHTOF6-ROBOT)
(RIGHTOF4-ROBOT)
(NOT-RIGHTOF5-ROBOT)
(RIGHTOF3-ROBOT)
(NOT-RIGHTOF4-ROBOT)
(RIGHTOF2-ROBOT)
(NOT-RIGHTOF3-ROBOT)
(RIGHTOF1-ROBOT)
(NOT-RIGHTOF2-ROBOT)
(RIGHTOF0-ROBOT)
(NOT-RIGHTOF1-ROBOT)
(not (RIGHTOF12-ROBOT))
(not (NOT-RIGHTOF0-ROBOT))
)
)
(when
(and
(RIGHTOF11-ROBOT)
)
(and
(RIGHTOF10-ROBOT)
(NOT-RIGHTOF11-ROBOT)
(RIGHTOF9-ROBOT)
(NOT-RIGHTOF10-ROBOT)
(RIGHTOF8-ROBOT)
(NOT-RIGHTOF9-ROBOT)
(RIGHTOF7-ROBOT)
(NOT-RIGHTOF8-ROBOT)
(RIGHTOF6-ROBOT)
(NOT-RIGHTOF7-ROBOT)
(RIGHTOF5-ROBOT)
(NOT-RIGHTOF6-ROBOT)
(RIGHTOF4-ROBOT)
(NOT-RIGHTOF5-ROBOT)
(RIGHTOF3-ROBOT)
(NOT-RIGHTOF4-ROBOT)
(RIGHTOF2-ROBOT)
(NOT-RIGHTOF3-ROBOT)
(RIGHTOF1-ROBOT)
(NOT-RIGHTOF2-ROBOT)
(RIGHTOF0-ROBOT)
(NOT-RIGHTOF1-ROBOT)
(not (RIGHTOF11-ROBOT))
(not (NOT-RIGHTOF0-ROBOT))
)
)
(when
(and
(RIGHTOF10-ROBOT)
)
(and
(RIGHTOF9-ROBOT)
(NOT-RIGHTOF10-ROBOT)
(RIGHTOF8-ROBOT)
(NOT-RIGHTOF9-ROBOT)
(RIGHTOF7-ROBOT)
(NOT-RIGHTOF8-ROBOT)
(RIGHTOF6-ROBOT)
(NOT-RIGHTOF7-ROBOT)
(RIGHTOF5-ROBOT)
(NOT-RIGHTOF6-ROBOT)
(RIGHTOF4-ROBOT)
(NOT-RIGHTOF5-ROBOT)
(RIGHTOF3-ROBOT)
(NOT-RIGHTOF4-ROBOT)
(RIGHTOF2-ROBOT)
(NOT-RIGHTOF3-ROBOT)
(RIGHTOF1-ROBOT)
(NOT-RIGHTOF2-ROBOT)
(RIGHTOF0-ROBOT)
(NOT-RIGHTOF1-ROBOT)
(not (RIGHTOF10-ROBOT))
(not (NOT-RIGHTOF0-ROBOT))
)
)
(when
(and
(RIGHTOF9-ROBOT)
)
(and
(RIGHTOF8-ROBOT)
(NOT-RIGHTOF9-ROBOT)
(RIGHTOF7-ROBOT)
(NOT-RIGHTOF8-ROBOT)
(RIGHTOF6-ROBOT)
(NOT-RIGHTOF7-ROBOT)
(RIGHTOF5-ROBOT)
(NOT-RIGHTOF6-ROBOT)
(RIGHTOF4-ROBOT)
(NOT-RIGHTOF5-ROBOT)
(RIGHTOF3-ROBOT)
(NOT-RIGHTOF4-ROBOT)
(RIGHTOF2-ROBOT)
(NOT-RIGHTOF3-ROBOT)
(RIGHTOF1-ROBOT)
(NOT-RIGHTOF2-ROBOT)
(RIGHTOF0-ROBOT)
(NOT-RIGHTOF1-ROBOT)
(not (RIGHTOF9-ROBOT))
(not (NOT-RIGHTOF0-ROBOT))
)
)
(when
(and
(RIGHTOF8-ROBOT)
)
(and
(RIGHTOF7-ROBOT)
(NOT-RIGHTOF8-ROBOT)
(RIGHTOF6-ROBOT)
(NOT-RIGHTOF7-ROBOT)
(RIGHTOF5-ROBOT)
(NOT-RIGHTOF6-ROBOT)
(RIGHTOF4-ROBOT)
(NOT-RIGHTOF5-ROBOT)
(RIGHTOF3-ROBOT)
(NOT-RIGHTOF4-ROBOT)
(RIGHTOF2-ROBOT)
(NOT-RIGHTOF3-ROBOT)
(RIGHTOF1-ROBOT)
(NOT-RIGHTOF2-ROBOT)
(RIGHTOF0-ROBOT)
(NOT-RIGHTOF1-ROBOT)
(not (RIGHTOF8-ROBOT))
(not (NOT-RIGHTOF0-ROBOT))
)
)
(when
(and
(RIGHTOF7-ROBOT)
)
(and
(RIGHTOF6-ROBOT)
(NOT-RIGHTOF7-ROBOT)
(RIGHTOF5-ROBOT)
(NOT-RIGHTOF6-ROBOT)
(RIGHTOF4-ROBOT)
(NOT-RIGHTOF5-ROBOT)
(RIGHTOF3-ROBOT)
(NOT-RIGHTOF4-ROBOT)
(RIGHTOF2-ROBOT)
(NOT-RIGHTOF3-ROBOT)
(RIGHTOF1-ROBOT)
(NOT-RIGHTOF2-ROBOT)
(RIGHTOF0-ROBOT)
(NOT-RIGHTOF1-ROBOT)
(not (RIGHTOF7-ROBOT))
(not (NOT-RIGHTOF0-ROBOT))
)
)
(when
(and
(RIGHTOF6-ROBOT)
)
(and
(RIGHTOF5-ROBOT)
(NOT-RIGHTOF6-ROBOT)
(RIGHTOF4-ROBOT)
(NOT-RIGHTOF5-ROBOT)
(RIGHTOF3-ROBOT)
(NOT-RIGHTOF4-ROBOT)
(RIGHTOF2-ROBOT)
(NOT-RIGHTOF3-ROBOT)
(RIGHTOF1-ROBOT)
(NOT-RIGHTOF2-ROBOT)
(RIGHTOF0-ROBOT)
(NOT-RIGHTOF1-ROBOT)
(not (RIGHTOF6-ROBOT))
(not (NOT-RIGHTOF0-ROBOT))
)
)
(when
(and
(RIGHTOF5-ROBOT)
)
(and
(RIGHTOF4-ROBOT)
(NOT-RIGHTOF5-ROBOT)
(RIGHTOF3-ROBOT)
(NOT-RIGHTOF4-ROBOT)
(RIGHTOF2-ROBOT)
(NOT-RIGHTOF3-ROBOT)
(RIGHTOF1-ROBOT)
(NOT-RIGHTOF2-ROBOT)
(RIGHTOF0-ROBOT)
(NOT-RIGHTOF1-ROBOT)
(not (RIGHTOF5-ROBOT))
(not (NOT-RIGHTOF0-ROBOT))
)
)
(when
(and
(RIGHTOF4-ROBOT)
)
(and
(RIGHTOF3-ROBOT)
(NOT-RIGHTOF4-ROBOT)
(RIGHTOF2-ROBOT)
(NOT-RIGHTOF3-ROBOT)
(RIGHTOF1-ROBOT)
(NOT-RIGHTOF2-ROBOT)
(RIGHTOF0-ROBOT)
(NOT-RIGHTOF1-ROBOT)
(not (RIGHTOF4-ROBOT))
(not (NOT-RIGHTOF0-ROBOT))
)
)
(when
(and
(RIGHTOF3-ROBOT)
)
(and
(RIGHTOF2-ROBOT)
(NOT-RIGHTOF3-ROBOT)
(RIGHTOF1-ROBOT)
(NOT-RIGHTOF2-ROBOT)
(RIGHTOF0-ROBOT)
(NOT-RIGHTOF1-ROBOT)
(not (RIGHTOF3-ROBOT))
(not (NOT-RIGHTOF0-ROBOT))
)
)
(when
(and
(RIGHTOF2-ROBOT)
)
(and
(RIGHTOF1-ROBOT)
(NOT-RIGHTOF2-ROBOT)
(RIGHTOF0-ROBOT)
(NOT-RIGHTOF1-ROBOT)
(not (RIGHTOF2-ROBOT))
(not (NOT-RIGHTOF0-ROBOT))
)
)
(when
(and
(RIGHTOF1-ROBOT)
)
(and
(RIGHTOF0-ROBOT)
(NOT-RIGHTOF1-ROBOT)
(not (NOT-RIGHTOF0-ROBOT))
(not (RIGHTOF1-ROBOT))
)
)
(when
(and
(Flag195-)
)
(and
(LEFTOF15-ROBOT)
(not (NOT-LEFTOF15-ROBOT))
)
)
(when
(and
(LEFTOF8-ROBOT)
)
(and
(LEFTOF14-ROBOT)
(LEFTOF13-ROBOT)
(LEFTOF12-ROBOT)
(LEFTOF11-ROBOT)
(LEFTOF10-ROBOT)
(LEFTOF9-ROBOT)
(LEFTOF7-ROBOT)
(not (NOT-LEFTOF14-ROBOT))
(not (NOT-LEFTOF13-ROBOT))
(not (NOT-LEFTOF12-ROBOT))
(not (NOT-LEFTOF11-ROBOT))
(not (NOT-LEFTOF10-ROBOT))
(not (NOT-LEFTOF9-ROBOT))
(not (NOT-LEFTOF8-ROBOT))
(not (NOT-LEFTOF7-ROBOT))
)
)
(when
(and
(LEFTOF7-ROBOT)
)
(and
(LEFTOF14-ROBOT)
(LEFTOF13-ROBOT)
(LEFTOF12-ROBOT)
(LEFTOF11-ROBOT)
(LEFTOF10-ROBOT)
(LEFTOF9-ROBOT)
(LEFTOF8-ROBOT)
(LEFTOF6-ROBOT)
(not (NOT-LEFTOF14-ROBOT))
(not (NOT-LEFTOF13-ROBOT))
(not (NOT-LEFTOF12-ROBOT))
(not (NOT-LEFTOF11-ROBOT))
(not (NOT-LEFTOF10-ROBOT))
(not (NOT-LEFTOF9-ROBOT))
(not (NOT-LEFTOF8-ROBOT))
(not (NOT-LEFTOF7-ROBOT))
(not (NOT-LEFTOF6-ROBOT))
)
)
(when
(and
(LEFTOF4-ROBOT)
)
(and
(LEFTOF14-ROBOT)
(LEFTOF13-ROBOT)
(LEFTOF12-ROBOT)
(LEFTOF11-ROBOT)
(LEFTOF10-ROBOT)
(LEFTOF9-ROBOT)
(LEFTOF8-ROBOT)
(LEFTOF7-ROBOT)
(LEFTOF6-ROBOT)
(LEFTOF5-ROBOT)
(LEFTOF3-ROBOT)
(not (NOT-LEFTOF14-ROBOT))
(not (NOT-LEFTOF13-ROBOT))
(not (NOT-LEFTOF12-ROBOT))
(not (NOT-LEFTOF11-ROBOT))
(not (NOT-LEFTOF10-ROBOT))
(not (NOT-LEFTOF9-ROBOT))
(not (NOT-LEFTOF8-ROBOT))
(not (NOT-LEFTOF7-ROBOT))
(not (NOT-LEFTOF6-ROBOT))
(not (NOT-LEFTOF5-ROBOT))
(not (NOT-LEFTOF4-ROBOT))
(not (NOT-LEFTOF3-ROBOT))
)
)
(when
(and
(LEFTOF3-ROBOT)
)
(and
(LEFTOF14-ROBOT)
(LEFTOF13-ROBOT)
(LEFTOF12-ROBOT)
(LEFTOF11-ROBOT)
(LEFTOF10-ROBOT)
(LEFTOF9-ROBOT)
(LEFTOF8-ROBOT)
(LEFTOF7-ROBOT)
(LEFTOF6-ROBOT)
(LEFTOF5-ROBOT)
(LEFTOF4-ROBOT)
(LEFTOF2-ROBOT)
(not (NOT-LEFTOF14-ROBOT))
(not (NOT-LEFTOF13-ROBOT))
(not (NOT-LEFTOF12-ROBOT))
(not (NOT-LEFTOF11-ROBOT))
(not (NOT-LEFTOF10-ROBOT))
(not (NOT-LEFTOF9-ROBOT))
(not (NOT-LEFTOF8-ROBOT))
(not (NOT-LEFTOF7-ROBOT))
(not (NOT-LEFTOF6-ROBOT))
(not (NOT-LEFTOF5-ROBOT))
(not (NOT-LEFTOF4-ROBOT))
(not (NOT-LEFTOF3-ROBOT))
(not (NOT-LEFTOF2-ROBOT))
)
)
(when
(and
(LEFTOF14-ROBOT)
)
(and
(LEFTOF13-ROBOT)
(not (NOT-LEFTOF14-ROBOT))
(not (NOT-LEFTOF13-ROBOT))
)
)
(when
(and
(LEFTOF5-ROBOT)
)
(and
(LEFTOF14-ROBOT)
(LEFTOF13-ROBOT)
(LEFTOF12-ROBOT)
(LEFTOF11-ROBOT)
(LEFTOF10-ROBOT)
(LEFTOF9-ROBOT)
(LEFTOF8-ROBOT)
(LEFTOF7-ROBOT)
(LEFTOF6-ROBOT)
(LEFTOF4-ROBOT)
(not (NOT-LEFTOF14-ROBOT))
(not (NOT-LEFTOF13-ROBOT))
(not (NOT-LEFTOF12-ROBOT))
(not (NOT-LEFTOF11-ROBOT))
(not (NOT-LEFTOF10-ROBOT))
(not (NOT-LEFTOF9-ROBOT))
(not (NOT-LEFTOF8-ROBOT))
(not (NOT-LEFTOF7-ROBOT))
(not (NOT-LEFTOF6-ROBOT))
(not (NOT-LEFTOF5-ROBOT))
(not (NOT-LEFTOF4-ROBOT))
)
)
(when
(and
(LEFTOF6-ROBOT)
)
(and
(LEFTOF14-ROBOT)
(LEFTOF13-ROBOT)
(LEFTOF12-ROBOT)
(LEFTOF11-ROBOT)
(LEFTOF10-ROBOT)
(LEFTOF9-ROBOT)
(LEFTOF8-ROBOT)
(LEFTOF7-ROBOT)
(LEFTOF5-ROBOT)
(not (NOT-LEFTOF14-ROBOT))
(not (NOT-LEFTOF13-ROBOT))
(not (NOT-LEFTOF12-ROBOT))
(not (NOT-LEFTOF11-ROBOT))
(not (NOT-LEFTOF10-ROBOT))
(not (NOT-LEFTOF9-ROBOT))
(not (NOT-LEFTOF8-ROBOT))
(not (NOT-LEFTOF7-ROBOT))
(not (NOT-LEFTOF6-ROBOT))
(not (NOT-LEFTOF5-ROBOT))
)
)
(when
(and
(LEFTOF12-ROBOT)
)
(and
(LEFTOF14-ROBOT)
(LEFTOF13-ROBOT)
(LEFTOF11-ROBOT)
(not (NOT-LEFTOF14-ROBOT))
(not (NOT-LEFTOF13-ROBOT))
(not (NOT-LEFTOF12-ROBOT))
(not (NOT-LEFTOF11-ROBOT))
)
)
(when
(and
(LEFTOF1-ROBOT)
)
(and
(LEFTOF14-ROBOT)
(LEFTOF13-ROBOT)
(LEFTOF12-ROBOT)
(LEFTOF11-ROBOT)
(LEFTOF10-ROBOT)
(LEFTOF9-ROBOT)
(LEFTOF8-ROBOT)
(LEFTOF7-ROBOT)
(LEFTOF6-ROBOT)
(LEFTOF5-ROBOT)
(LEFTOF4-ROBOT)
(LEFTOF3-ROBOT)
(LEFTOF2-ROBOT)
(not (NOT-LEFTOF14-ROBOT))
(not (NOT-LEFTOF13-ROBOT))
(not (NOT-LEFTOF12-ROBOT))
(not (NOT-LEFTOF11-ROBOT))
(not (NOT-LEFTOF10-ROBOT))
(not (NOT-LEFTOF9-ROBOT))
(not (NOT-LEFTOF8-ROBOT))
(not (NOT-LEFTOF7-ROBOT))
(not (NOT-LEFTOF6-ROBOT))
(not (NOT-LEFTOF5-ROBOT))
(not (NOT-LEFTOF4-ROBOT))
(not (NOT-LEFTOF3-ROBOT))
(not (NOT-LEFTOF2-ROBOT))
(not (NOT-LEFTOF1-ROBOT))
)
)
(when
(and
(LEFTOF13-ROBOT)
)
(and
(LEFTOF14-ROBOT)
(LEFTOF12-ROBOT)
(not (NOT-LEFTOF14-ROBOT))
(not (NOT-LEFTOF13-ROBOT))
(not (NOT-LEFTOF12-ROBOT))
)
)
(when
(and
(LEFTOF2-ROBOT)
)
(and
(LEFTOF14-ROBOT)
(LEFTOF13-ROBOT)
(LEFTOF12-ROBOT)
(LEFTOF11-ROBOT)
(LEFTOF10-ROBOT)
(LEFTOF9-ROBOT)
(LEFTOF8-ROBOT)
(LEFTOF7-ROBOT)
(LEFTOF6-ROBOT)
(LEFTOF5-ROBOT)
(LEFTOF4-ROBOT)
(LEFTOF3-ROBOT)
(LEFTOF1-ROBOT)
(not (NOT-LEFTOF14-ROBOT))
(not (NOT-LEFTOF13-ROBOT))
(not (NOT-LEFTOF12-ROBOT))
(not (NOT-LEFTOF11-ROBOT))
(not (NOT-LEFTOF10-ROBOT))
(not (NOT-LEFTOF9-ROBOT))
(not (NOT-LEFTOF8-ROBOT))
(not (NOT-LEFTOF7-ROBOT))
(not (NOT-LEFTOF6-ROBOT))
(not (NOT-LEFTOF5-ROBOT))
(not (NOT-LEFTOF4-ROBOT))
(not (NOT-LEFTOF3-ROBOT))
(not (NOT-LEFTOF2-ROBOT))
(not (NOT-LEFTOF1-ROBOT))
)
)
(when
(and
(LEFTOF9-ROBOT)
)
(and
(LEFTOF14-ROBOT)
(LEFTOF13-ROBOT)
(LEFTOF12-ROBOT)
(LEFTOF11-ROBOT)
(LEFTOF10-ROBOT)
(LEFTOF8-ROBOT)
(not (NOT-LEFTOF14-ROBOT))
(not (NOT-LEFTOF13-ROBOT))
(not (NOT-LEFTOF12-ROBOT))
(not (NOT-LEFTOF11-ROBOT))
(not (NOT-LEFTOF10-ROBOT))
(not (NOT-LEFTOF9-ROBOT))
(not (NOT-LEFTOF8-ROBOT))
)
)
(when
(and
(LEFTOF11-ROBOT)
)
(and
(LEFTOF14-ROBOT)
(LEFTOF13-ROBOT)
(LEFTOF12-ROBOT)
(LEFTOF10-ROBOT)
(not (NOT-LEFTOF14-ROBOT))
(not (NOT-LEFTOF13-ROBOT))
(not (NOT-LEFTOF12-ROBOT))
(not (NOT-LEFTOF11-ROBOT))
(not (NOT-LEFTOF10-ROBOT))
)
)
(when
(and
(LEFTOF10-ROBOT)
)
(and
(LEFTOF14-ROBOT)
(LEFTOF13-ROBOT)
(LEFTOF12-ROBOT)
(LEFTOF11-ROBOT)
(LEFTOF9-ROBOT)
(not (NOT-LEFTOF14-ROBOT))
(not (NOT-LEFTOF13-ROBOT))
(not (NOT-LEFTOF12-ROBOT))
(not (NOT-LEFTOF11-ROBOT))
(not (NOT-LEFTOF10-ROBOT))
(not (NOT-LEFTOF9-ROBOT))
)
)
(when
(and
(LEFTOF15-ROBOT)
)
(and
(LEFTOF14-ROBOT)
(not (NOT-LEFTOF14-ROBOT))
)
)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action MOVERIGHT-ROBOT
:parameters ()
:precondition
(and
(Flag4-)
)
:effect
(and
(when
(and
(Flag194-)
)
(and
(COLUMN15-ROBOT)
(not (NOT-COLUMN15-ROBOT))
)
)
(when
(and
(Flag191-)
)
(and
(COLUMN14-ROBOT)
(not (NOT-COLUMN14-ROBOT))
)
)
(when
(and
(Flag187-)
)
(and
(COLUMN13-ROBOT)
(not (NOT-COLUMN13-ROBOT))
)
)
(when
(and
(Flag181-)
)
(and
(COLUMN12-ROBOT)
(not (NOT-COLUMN12-ROBOT))
)
)
(when
(and
(Flag175-)
)
(and
(COLUMN11-ROBOT)
(not (NOT-COLUMN11-ROBOT))
)
)
(when
(and
(Flag167-)
)
(and
(COLUMN10-ROBOT)
(not (NOT-COLUMN10-ROBOT))
)
)
(when
(and
(Flag159-)
)
(and
(COLUMN9-ROBOT)
(not (NOT-COLUMN9-ROBOT))
)
)
(when
(and
(Flag149-)
)
(and
(COLUMN8-ROBOT)
(not (NOT-COLUMN8-ROBOT))
)
)
(when
(and
(Flag138-)
)
(and
(COLUMN7-ROBOT)
(not (NOT-COLUMN7-ROBOT))
)
)
(when
(and
(Flag125-)
)
(and
(COLUMN6-ROBOT)
(not (NOT-COLUMN6-ROBOT))
)
)
(when
(and
(Flag113-)
)
(and
(COLUMN5-ROBOT)
(not (NOT-COLUMN5-ROBOT))
)
)
(when
(and
(Flag98-)
)
(and
(COLUMN4-ROBOT)
(not (NOT-COLUMN4-ROBOT))
)
)
(when
(and
(Flag82-)
)
(and
(COLUMN3-ROBOT)
(not (NOT-COLUMN3-ROBOT))
)
)
(when
(and
(Flag67-)
)
(and
(COLUMN2-ROBOT)
(not (NOT-COLUMN2-ROBOT))
)
)
(when
(and
(Flag50-)
)
(and
(COLUMN1-ROBOT)
(not (NOT-COLUMN1-ROBOT))
)
)
(when
(and
(COLUMN14-ROBOT)
)
(and
(COLUMN15-ROBOT)
(NOT-COLUMN14-ROBOT)
(not (NOT-COLUMN15-ROBOT))
(not (COLUMN14-ROBOT))
)
)
(when
(and
(COLUMN13-ROBOT)
)
(and
(COLUMN14-ROBOT)
(NOT-COLUMN13-ROBOT)
(not (NOT-COLUMN14-ROBOT))
(not (COLUMN13-ROBOT))
)
)
(when
(and
(COLUMN12-ROBOT)
)
(and
(COLUMN13-ROBOT)
(NOT-COLUMN12-ROBOT)
(not (NOT-COLUMN13-ROBOT))
(not (COLUMN12-ROBOT))
)
)
(when
(and
(COLUMN11-ROBOT)
)
(and
(COLUMN12-ROBOT)
(NOT-COLUMN11-ROBOT)
(not (NOT-COLUMN12-ROBOT))
(not (COLUMN11-ROBOT))
)
)
(when
(and
(COLUMN10-ROBOT)
)
(and
(COLUMN11-ROBOT)
(NOT-COLUMN10-ROBOT)
(not (NOT-COLUMN11-ROBOT))
(not (COLUMN10-ROBOT))
)
)
(when
(and
(COLUMN9-ROBOT)
)
(and
(COLUMN10-ROBOT)
(NOT-COLUMN9-ROBOT)
(not (NOT-COLUMN10-ROBOT))
(not (COLUMN9-ROBOT))
)
)
(when
(and
(COLUMN8-ROBOT)
)
(and
(COLUMN9-ROBOT)
(NOT-COLUMN8-ROBOT)
(not (NOT-COLUMN9-ROBOT))
(not (COLUMN8-ROBOT))
)
)
(when
(and
(COLUMN7-ROBOT)
)
(and
(COLUMN8-ROBOT)
(NOT-COLUMN7-ROBOT)
(not (NOT-COLUMN8-ROBOT))
(not (COLUMN7-ROBOT))
)
)
(when
(and
(COLUMN6-ROBOT)
)
(and
(COLUMN7-ROBOT)
(NOT-COLUMN6-ROBOT)
(not (NOT-COLUMN7-ROBOT))
(not (COLUMN6-ROBOT))
)
)
(when
(and
(COLUMN5-ROBOT)
)
(and
(COLUMN6-ROBOT)
(NOT-COLUMN5-ROBOT)
(not (NOT-COLUMN6-ROBOT))
(not (COLUMN5-ROBOT))
)
)
(when
(and
(COLUMN4-ROBOT)
)
(and
(COLUMN5-ROBOT)
(NOT-COLUMN4-ROBOT)
(not (NOT-COLUMN5-ROBOT))
(not (COLUMN4-ROBOT))
)
)
(when
(and
(COLUMN3-ROBOT)
)
(and
(COLUMN4-ROBOT)
(NOT-COLUMN3-ROBOT)
(not (NOT-COLUMN4-ROBOT))
(not (COLUMN3-ROBOT))
)
)
(when
(and
(COLUMN2-ROBOT)
)
(and
(COLUMN3-ROBOT)
(NOT-COLUMN2-ROBOT)
(not (NOT-COLUMN3-ROBOT))
(not (COLUMN2-ROBOT))
)
)
(when
(and
(COLUMN1-ROBOT)
)
(and
(COLUMN2-ROBOT)
(NOT-COLUMN1-ROBOT)
(not (NOT-COLUMN2-ROBOT))
(not (COLUMN1-ROBOT))
)
)
(when
(and
(COLUMN0-ROBOT)
)
(and
(COLUMN1-ROBOT)
(NOT-COLUMN0-ROBOT)
(not (NOT-COLUMN1-ROBOT))
(not (COLUMN0-ROBOT))
)
)
(when
(and
(Flag33-)
)
(and
(LEFTOF16-ROBOT)
(NOT-LEFTOF15-ROBOT)
(not (NOT-LEFTOF16-ROBOT))
(not (LEFTOF15-ROBOT))
)
)
(when
(and
(Flag32-)
)
(and
(LEFTOF15-ROBOT)
(NOT-LEFTOF14-ROBOT)
(not (NOT-LEFTOF15-ROBOT))
(not (LEFTOF14-ROBOT))
)
)
(when
(and
(Flag31-)
)
(and
(LEFTOF14-ROBOT)
(NOT-LEFTOF13-ROBOT)
(not (NOT-LEFTOF14-ROBOT))
(not (LEFTOF13-ROBOT))
)
)
(when
(and
(Flag30-)
)
(and
(LEFTOF13-ROBOT)
(NOT-LEFTOF12-ROBOT)
(not (NOT-LEFTOF13-ROBOT))
(not (LEFTOF12-ROBOT))
)
)
(when
(and
(Flag29-)
)
(and
(LEFTOF12-ROBOT)
(NOT-LEFTOF11-ROBOT)
(not (NOT-LEFTOF12-ROBOT))
(not (LEFTOF11-ROBOT))
)
)
(when
(and
(Flag28-)
)
(and
(LEFTOF11-ROBOT)
(NOT-LEFTOF10-ROBOT)
(not (NOT-LEFTOF11-ROBOT))
(not (LEFTOF10-ROBOT))
)
)
(when
(and
(Flag27-)
)
(and
(LEFTOF10-ROBOT)
(NOT-LEFTOF9-ROBOT)
(not (NOT-LEFTOF10-ROBOT))
(not (LEFTOF9-ROBOT))
)
)
(when
(and
(Flag26-)
)
(and
(LEFTOF9-ROBOT)
(NOT-LEFTOF8-ROBOT)
(not (NOT-LEFTOF9-ROBOT))
(not (LEFTOF8-ROBOT))
)
)
(when
(and
(Flag25-)
)
(and
(LEFTOF8-ROBOT)
(NOT-LEFTOF7-ROBOT)
(not (NOT-LEFTOF8-ROBOT))
(not (LEFTOF7-ROBOT))
)
)
(when
(and
(Flag24-)
)
(and
(LEFTOF7-ROBOT)
(NOT-LEFTOF6-ROBOT)
(not (NOT-LEFTOF7-ROBOT))
(not (LEFTOF6-ROBOT))
)
)
(when
(and
(Flag23-)
)
(and
(LEFTOF6-ROBOT)
(NOT-LEFTOF5-ROBOT)
(not (NOT-LEFTOF6-ROBOT))
(not (LEFTOF5-ROBOT))
)
)
(when
(and
(Flag22-)
)
(and
(LEFTOF5-ROBOT)
(NOT-LEFTOF4-ROBOT)
(not (NOT-LEFTOF5-ROBOT))
(not (LEFTOF4-ROBOT))
)
)
(when
(and
(Flag21-)
)
(and
(LEFTOF4-ROBOT)
(NOT-LEFTOF3-ROBOT)
(not (NOT-LEFTOF4-ROBOT))
(not (LEFTOF3-ROBOT))
)
)
(when
(and
(Flag20-)
)
(and
(LEFTOF3-ROBOT)
(NOT-LEFTOF2-ROBOT)
(not (NOT-LEFTOF3-ROBOT))
(not (LEFTOF2-ROBOT))
)
)
(when
(and
(LEFTOF1-ROBOT)
)
(and
(LEFTOF2-ROBOT)
(NOT-LEFTOF1-ROBOT)
(not (NOT-LEFTOF2-ROBOT))
(not (LEFTOF1-ROBOT))
)
)
(when
(and
(Flag19-)
)
(and
(RIGHTOF15-ROBOT)
(not (NOT-RIGHTOF15-ROBOT))
)
)
(when
(and
(Flag18-)
)
(and
(RIGHTOF14-ROBOT)
(not (NOT-RIGHTOF14-ROBOT))
)
)
(when
(and
(Flag17-)
)
(and
(RIGHTOF13-ROBOT)
(not (NOT-RIGHTOF13-ROBOT))
)
)
(when
(and
(Flag16-)
)
(and
(RIGHTOF12-ROBOT)
(not (NOT-RIGHTOF12-ROBOT))
)
)
(when
(and
(Flag15-)
)
(and
(RIGHTOF11-ROBOT)
(not (NOT-RIGHTOF11-ROBOT))
)
)
(when
(and
(Flag14-)
)
(and
(RIGHTOF10-ROBOT)
(not (NOT-RIGHTOF10-ROBOT))
)
)
(when
(and
(Flag13-)
)
(and
(RIGHTOF9-ROBOT)
(not (NOT-RIGHTOF9-ROBOT))
)
)
(when
(and
(Flag12-)
)
(and
(RIGHTOF8-ROBOT)
(not (NOT-RIGHTOF8-ROBOT))
)
)
(when
(and
(Flag11-)
)
(and
(RIGHTOF7-ROBOT)
(not (NOT-RIGHTOF7-ROBOT))
)
)
(when
(and
(Flag10-)
)
(and
(RIGHTOF6-ROBOT)
(not (NOT-RIGHTOF6-ROBOT))
)
)
(when
(and
(Flag9-)
)
(and
(RIGHTOF5-ROBOT)
(not (NOT-RIGHTOF5-ROBOT))
)
)
(when
(and
(Flag8-)
)
(and
(RIGHTOF4-ROBOT)
(not (NOT-RIGHTOF4-ROBOT))
)
)
(when
(and
(Flag7-)
)
(and
(RIGHTOF3-ROBOT)
(not (NOT-RIGHTOF3-ROBOT))
)
)
(when
(and
(Flag6-)
)
(and
(RIGHTOF2-ROBOT)
(not (NOT-RIGHTOF2-ROBOT))
)
)
(when
(and
(Flag5-)
)
(and
(RIGHTOF1-ROBOT)
(not (NOT-RIGHTOF1-ROBOT))
)
)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action CHECKCONSISTENCYACTION
:parameters ()
:precondition
(and
(NOT-ERROR-)
(CHECKCONSISTENCY-)
)
:effect
(and
(when
(and
(LEFTOF9-ROBOT)
(RIGHTOF9-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF11-ROBOT)
(LEFTOF4-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF7-ROBOT)
(ABOVEOF15-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF8-ROBOT)
(LEFTOF3-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF5-ROBOT)
(RIGHTOF15-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF14-ROBOT)
(BELOWOF6-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF10-ROBOT)
(ABOVEOF11-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF6-ROBOT)
(ABOVEOF15-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF15-ROBOT)
(BELOWOF3-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF13-ROBOT)
(LEFTOF2-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF11-ROBOT)
(BELOWOF3-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF14-ROBOT)
(BELOWOF2-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF12-ROBOT)
(BELOWOF5-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF13-ROBOT)
(LEFTOF1-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF2-ROBOT)
(RIGHTOF9-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF9-ROBOT)
(LEFTOF5-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF1-ROBOT)
(ABOVEOF13-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF11-ROBOT)
(LEFTOF2-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF15-ROBOT)
(LEFTOF2-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF12-ROBOT)
(BELOWOF12-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF8-ROBOT)
(ABOVEOF12-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF3-ROBOT)
(ABOVEOF9-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF9-ROBOT)
(LEFTOF1-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF3-ROBOT)
(RIGHTOF14-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF5-ROBOT)
(ABOVEOF11-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF4-ROBOT)
(RIGHTOF10-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF9-ROBOT)
(RIGHTOF15-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF4-ROBOT)
(ABOVEOF13-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF8-ROBOT)
(LEFTOF2-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF1-ROBOT)
(RIGHTOF11-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF2-ROBOT)
(RIGHTOF14-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF12-ROBOT)
(BELOWOF7-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF14-ROBOT)
(LEFTOF5-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF15-ROBOT)
(LEFTOF1-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF13-ROBOT)
(BELOWOF7-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF12-ROBOT)
(LEFTOF4-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF6-ROBOT)
(RIGHTOF15-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF14-ROBOT)
(BELOWOF4-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF15-ROBOT)
(BELOWOF10-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF5-ROBOT)
(ABOVEOF15-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF11-ROBOT)
(LEFTOF3-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF8-ROBOT)
(RIGHTOF14-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF9-ROBOT)
(BELOWOF1-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF10-ROBOT)
(RIGHTOF13-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF4-ROBOT)
(ABOVEOF15-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF14-ROBOT)
(LEFTOF1-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF10-ROBOT)
(LEFTOF2-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF3-ROBOT)
(RIGHTOF7-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF7-ROBOT)
(RIGHTOF13-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF8-ROBOT)
(BELOWOF6-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF12-ROBOT)
(LEFTOF5-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF3-ROBOT)
(RIGHTOF13-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF12-ROBOT)
(LEFTOF2-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF2-ROBOT)
(RIGHTOF4-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF3-ROBOT)
(RIGHTOF9-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF8-ROBOT)
(RIGHTOF13-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF6-ROBOT)
(LEFTOF2-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF5-ROBOT)
(BELOWOF1-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF11-ROBOT)
(LEFTOF8-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF1-ROBOT)
(RIGHTOF8-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF5-ROBOT)
(ABOVEOF5-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF15-ROBOT)
(BELOWOF1-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF5-ROBOT)
(RIGHTOF10-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF12-ROBOT)
(LEFTOF11-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF4-ROBOT)
(RIGHTOF14-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF3-ROBOT)
(ABOVEOF4-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF7-ROBOT)
(RIGHTOF9-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF2-ROBOT)
(ABOVEOF15-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF13-ROBOT)
(LEFTOF4-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF14-ROBOT)
(BELOWOF9-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF4-ROBOT)
(ABOVEOF11-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF10-ROBOT)
(LEFTOF8-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF1-ROBOT)
(ABOVEOF10-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF11-ROBOT)
(ABOVEOF15-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF12-ROBOT)
(BELOWOF3-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF3-ROBOT)
(ABOVEOF14-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF7-ROBOT)
(BELOWOF2-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF14-ROBOT)
(BELOWOF1-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF15-ROBOT)
(LEFTOF7-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF2-ROBOT)
(ABOVEOF5-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF12-ROBOT)
(BELOWOF2-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF10-ROBOT)
(LEFTOF3-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF15-ROBOT)
(LEFTOF11-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF9-ROBOT)
(BELOWOF8-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF6-ROBOT)
(BELOWOF1-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF12-ROBOT)
(LEFTOF12-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF8-ROBOT)
(ABOVEOF14-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF13-ROBOT)
(LEFTOF9-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF13-ROBOT)
(BELOWOF3-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF8-ROBOT)
(RIGHTOF9-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF5-ROBOT)
(RIGHTOF8-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF10-ROBOT)
(RIGHTOF14-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF6-ROBOT)
(LEFTOF5-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF14-ROBOT)
(RIGHTOF15-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF9-ROBOT)
(LEFTOF4-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF6-ROBOT)
(ABOVEOF10-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF4-ROBOT)
(RIGHTOF4-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF2-ROBOT)
(ABOVEOF13-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF12-ROBOT)
(BELOWOF10-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF2-ROBOT)
(ABOVEOF10-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF9-ROBOT)
(LEFTOF6-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF13-ROBOT)
(LEFTOF6-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF1-ROBOT)
(ABOVEOF11-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF8-ROBOT)
(ABOVEOF15-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF3-ROBOT)
(BELOWOF2-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF5-ROBOT)
(LEFTOF3-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF11-ROBOT)
(LEFTOF5-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF10-ROBOT)
(BELOWOF5-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF9-ROBOT)
(BELOWOF4-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF14-ROBOT)
(LEFTOF6-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF14-ROBOT)
(LEFTOF11-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF11-ROBOT)
(LEFTOF11-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF3-ROBOT)
(RIGHTOF12-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF13-ROBOT)
(BELOWOF5-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF8-ROBOT)
(BELOWOF1-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF5-ROBOT)
(ABOVEOF14-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF5-ROBOT)
(RIGHTOF7-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF3-ROBOT)
(RIGHTOF15-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF12-ROBOT)
(LEFTOF6-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF8-ROBOT)
(BELOWOF2-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF9-ROBOT)
(RIGHTOF12-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF11-ROBOT)
(BELOWOF2-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF6-ROBOT)
(LEFTOF1-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF12-ROBOT)
(LEFTOF10-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF7-ROBOT)
(ABOVEOF11-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF6-ROBOT)
(ABOVEOF9-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF5-ROBOT)
(ABOVEOF9-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF13-ROBOT)
(BELOWOF6-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF6-ROBOT)
(LEFTOF4-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF9-ROBOT)
(ABOVEOF15-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF14-ROBOT)
(BELOWOF7-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF15-ROBOT)
(BELOWOF15-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF9-ROBOT)
(BELOWOF2-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF14-ROBOT)
(BELOWOF12-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF7-ROBOT)
(RIGHTOF12-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF2-ROBOT)
(LEFTOF1-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF7-ROBOT)
(RIGHTOF14-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF4-ROBOT)
(RIGHTOF15-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF6-ROBOT)
(RIGHTOF10-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF4-ROBOT)
(RIGHTOF8-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF14-ROBOT)
(BELOWOF10-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF12-ROBOT)
(BELOWOF9-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF3-ROBOT)
(LEFTOF2-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF6-ROBOT)
(ABOVEOF11-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF7-ROBOT)
(RIGHTOF10-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF4-ROBOT)
(ABOVEOF12-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF12-ROBOT)
(BELOWOF6-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF8-ROBOT)
(ABOVEOF13-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF7-ROBOT)
(ABOVEOF7-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF12-ROBOT)
(LEFTOF1-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF2-ROBOT)
(RIGHTOF7-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF1-ROBOT)
(ABOVEOF3-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF2-ROBOT)
(ABOVEOF6-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF1-ROBOT)
(ABOVEOF12-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF4-ROBOT)
(ABOVEOF7-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF7-ROBOT)
(ABOVEOF10-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF13-ROBOT)
(ABOVEOF15-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF14-ROBOT)
(ABOVEOF15-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF8-ROBOT)
(BELOWOF7-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF4-ROBOT)
(BELOWOF4-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF6-ROBOT)
(LEFTOF6-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF8-ROBOT)
(ABOVEOF11-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF13-ROBOT)
(BELOWOF12-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF10-ROBOT)
(ABOVEOF13-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF10-ROBOT)
(RIGHTOF15-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF7-ROBOT)
(LEFTOF1-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF8-ROBOT)
(BELOWOF8-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF11-ROBOT)
(ABOVEOF13-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF10-ROBOT)
(BELOWOF3-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF9-ROBOT)
(BELOWOF7-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF13-ROBOT)
(RIGHTOF15-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF12-ROBOT)
(RIGHTOF15-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF6-ROBOT)
(LEFTOF3-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF14-ROBOT)
(LEFTOF12-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF8-ROBOT)
(ABOVEOF10-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF3-ROBOT)
(ABOVEOF8-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF5-ROBOT)
(LEFTOF1-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF9-ROBOT)
(RIGHTOF10-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF11-ROBOT)
(LEFTOF7-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF10-ROBOT)
(BELOWOF4-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF5-ROBOT)
(RIGHTOF13-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF5-ROBOT)
(LEFTOF2-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF4-ROBOT)
(ABOVEOF6-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF11-ROBOT)
(LEFTOF6-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF1-ROBOT)
(RIGHTOF10-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF11-ROBOT)
(LEFTOF9-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF8-ROBOT)
(RIGHTOF15-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF14-ROBOT)
(LEFTOF9-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF2-ROBOT)
(ABOVEOF2-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF9-ROBOT)
(ABOVEOF13-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF13-ROBOT)
(BELOWOF13-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF6-ROBOT)
(BELOWOF5-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF4-ROBOT)
(BELOWOF1-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF8-ROBOT)
(RIGHTOF12-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF14-ROBOT)
(BELOWOF14-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF11-ROBOT)
(BELOWOF11-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF11-ROBOT)
(BELOWOF9-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF7-ROBOT)
(BELOWOF1-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF6-ROBOT)
(BELOWOF6-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF7-ROBOT)
(RIGHTOF8-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF3-ROBOT)
(ABOVEOF7-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF12-ROBOT)
(RIGHTOF13-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF3-ROBOT)
(BELOWOF3-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF4-ROBOT)
(RIGHTOF5-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF4-ROBOT)
(BELOWOF2-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF7-ROBOT)
(LEFTOF6-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF1-ROBOT)
(ABOVEOF2-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF4-ROBOT)
(RIGHTOF7-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF8-ROBOT)
(BELOWOF4-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF11-ROBOT)
(LEFTOF10-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF13-ROBOT)
(ABOVEOF14-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF3-ROBOT)
(LEFTOF1-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF11-ROBOT)
(ABOVEOF14-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF4-ROBOT)
(LEFTOF1-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF13-ROBOT)
(RIGHTOF13-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF1-ROBOT)
(LEFTOF1-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF6-ROBOT)
(ABOVEOF7-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF14-ROBOT)
(LEFTOF13-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF12-ROBOT)
(BELOWOF11-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF6-ROBOT)
(BELOWOF3-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF13-ROBOT)
(LEFTOF11-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF15-ROBOT)
(LEFTOF15-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF8-ROBOT)
(RIGHTOF8-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF5-ROBOT)
(ABOVEOF8-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF1-ROBOT)
(BELOWOF1-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF4-ROBOT)
(LEFTOF3-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF4-ROBOT)
(ABOVEOF5-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF14-ROBOT)
(LEFTOF14-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF12-ROBOT)
(ABOVEOF15-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF9-ROBOT)
(ABOVEOF9-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF7-ROBOT)
(RIGHTOF7-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF9-ROBOT)
(ABOVEOF10-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF5-ROBOT)
(ABOVEOF7-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF3-ROBOT)
(ABOVEOF5-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF8-ROBOT)
(LEFTOF6-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF5-ROBOT)
(RIGHTOF5-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF10-ROBOT)
(LEFTOF10-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF10-ROBOT)
(ABOVEOF10-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF3-ROBOT)
(LEFTOF3-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF2-ROBOT)
(RIGHTOF2-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(NOT-CHECKCONSISTENCY-)
(not (CHECKCONSISTENCY-))
)
)
(:derived (Flag4-)(and (NOT-CHECKCONSISTENCY-)(NOT-ERROR-)(Flag3-)))

(:derived (Flag199-)(and (BELOWOF2-ROBOT)))

(:derived (Flag199-)(and (BELOWOF1-ROBOT)))

(:derived (Flag200-)(and (BELOWOF2-ROBOT)))

(:derived (Flag200-)(and (BELOWOF1-ROBOT)))

(:derived (Flag200-)(and (BELOWOF3-ROBOT)))

(:derived (Flag201-)(and (BELOWOF2-ROBOT)))

(:derived (Flag201-)(and (BELOWOF1-ROBOT)))

(:derived (Flag201-)(and (BELOWOF3-ROBOT)))

(:derived (Flag201-)(and (BELOWOF4-ROBOT)))

(:derived (Flag202-)(and (BELOWOF2-ROBOT)))

(:derived (Flag202-)(and (BELOWOF1-ROBOT)))

(:derived (Flag202-)(and (BELOWOF3-ROBOT)))

(:derived (Flag202-)(and (BELOWOF4-ROBOT)))

(:derived (Flag202-)(and (BELOWOF5-ROBOT)))

(:derived (Flag203-)(and (BELOWOF6-ROBOT)))

(:derived (Flag203-)(and (BELOWOF5-ROBOT)))

(:derived (Flag203-)(and (BELOWOF2-ROBOT)))

(:derived (Flag203-)(and (BELOWOF1-ROBOT)))

(:derived (Flag203-)(and (BELOWOF4-ROBOT)))

(:derived (Flag203-)(and (BELOWOF3-ROBOT)))

(:derived (Flag204-)(and (BELOWOF6-ROBOT)))

(:derived (Flag204-)(and (BELOWOF5-ROBOT)))

(:derived (Flag204-)(and (BELOWOF2-ROBOT)))

(:derived (Flag204-)(and (BELOWOF1-ROBOT)))

(:derived (Flag204-)(and (BELOWOF4-ROBOT)))

(:derived (Flag204-)(and (BELOWOF7-ROBOT)))

(:derived (Flag204-)(and (BELOWOF3-ROBOT)))

(:derived (Flag205-)(and (BELOWOF6-ROBOT)))

(:derived (Flag205-)(and (BELOWOF5-ROBOT)))

(:derived (Flag205-)(and (BELOWOF2-ROBOT)))

(:derived (Flag205-)(and (BELOWOF1-ROBOT)))

(:derived (Flag205-)(and (BELOWOF4-ROBOT)))

(:derived (Flag205-)(and (BELOWOF7-ROBOT)))

(:derived (Flag205-)(and (BELOWOF3-ROBOT)))

(:derived (Flag205-)(and (BELOWOF8-ROBOT)))

(:derived (Flag206-)(and (BELOWOF6-ROBOT)))

(:derived (Flag206-)(and (BELOWOF5-ROBOT)))

(:derived (Flag206-)(and (BELOWOF2-ROBOT)))

(:derived (Flag206-)(and (BELOWOF1-ROBOT)))

(:derived (Flag206-)(and (BELOWOF4-ROBOT)))

(:derived (Flag206-)(and (BELOWOF7-ROBOT)))

(:derived (Flag206-)(and (BELOWOF9-ROBOT)))

(:derived (Flag206-)(and (BELOWOF3-ROBOT)))

(:derived (Flag206-)(and (BELOWOF8-ROBOT)))

(:derived (Flag207-)(and (BELOWOF10-ROBOT)))

(:derived (Flag207-)(and (BELOWOF6-ROBOT)))

(:derived (Flag207-)(and (BELOWOF5-ROBOT)))

(:derived (Flag207-)(and (BELOWOF2-ROBOT)))

(:derived (Flag207-)(and (BELOWOF1-ROBOT)))

(:derived (Flag207-)(and (BELOWOF4-ROBOT)))

(:derived (Flag207-)(and (BELOWOF7-ROBOT)))

(:derived (Flag207-)(and (BELOWOF9-ROBOT)))

(:derived (Flag207-)(and (BELOWOF3-ROBOT)))

(:derived (Flag207-)(and (BELOWOF8-ROBOT)))

(:derived (Flag208-)(and (BELOWOF10-ROBOT)))

(:derived (Flag208-)(and (BELOWOF11-ROBOT)))

(:derived (Flag208-)(and (BELOWOF6-ROBOT)))

(:derived (Flag208-)(and (BELOWOF5-ROBOT)))

(:derived (Flag208-)(and (BELOWOF2-ROBOT)))

(:derived (Flag208-)(and (BELOWOF1-ROBOT)))

(:derived (Flag208-)(and (BELOWOF4-ROBOT)))

(:derived (Flag208-)(and (BELOWOF7-ROBOT)))

(:derived (Flag208-)(and (BELOWOF9-ROBOT)))

(:derived (Flag208-)(and (BELOWOF3-ROBOT)))

(:derived (Flag208-)(and (BELOWOF8-ROBOT)))

(:derived (Flag209-)(and (BELOWOF10-ROBOT)))

(:derived (Flag209-)(and (BELOWOF11-ROBOT)))

(:derived (Flag209-)(and (BELOWOF6-ROBOT)))

(:derived (Flag209-)(and (BELOWOF5-ROBOT)))

(:derived (Flag209-)(and (BELOWOF2-ROBOT)))

(:derived (Flag209-)(and (BELOWOF1-ROBOT)))

(:derived (Flag209-)(and (BELOWOF4-ROBOT)))

(:derived (Flag209-)(and (BELOWOF12-ROBOT)))

(:derived (Flag209-)(and (BELOWOF7-ROBOT)))

(:derived (Flag209-)(and (BELOWOF9-ROBOT)))

(:derived (Flag209-)(and (BELOWOF3-ROBOT)))

(:derived (Flag209-)(and (BELOWOF8-ROBOT)))

(:derived (Flag210-)(and (BELOWOF10-ROBOT)))

(:derived (Flag210-)(and (BELOWOF11-ROBOT)))

(:derived (Flag210-)(and (BELOWOF6-ROBOT)))

(:derived (Flag210-)(and (BELOWOF5-ROBOT)))

(:derived (Flag210-)(and (BELOWOF2-ROBOT)))

(:derived (Flag210-)(and (BELOWOF1-ROBOT)))

(:derived (Flag210-)(and (BELOWOF4-ROBOT)))

(:derived (Flag210-)(and (BELOWOF12-ROBOT)))

(:derived (Flag210-)(and (BELOWOF7-ROBOT)))

(:derived (Flag210-)(and (BELOWOF9-ROBOT)))

(:derived (Flag210-)(and (BELOWOF3-ROBOT)))

(:derived (Flag210-)(and (BELOWOF13-ROBOT)))

(:derived (Flag210-)(and (BELOWOF8-ROBOT)))

(:derived (Flag211-)(and (BELOWOF10-ROBOT)))

(:derived (Flag211-)(and (BELOWOF11-ROBOT)))

(:derived (Flag211-)(and (BELOWOF6-ROBOT)))

(:derived (Flag211-)(and (BELOWOF5-ROBOT)))

(:derived (Flag211-)(and (BELOWOF2-ROBOT)))

(:derived (Flag211-)(and (BELOWOF1-ROBOT)))

(:derived (Flag211-)(and (BELOWOF4-ROBOT)))

(:derived (Flag211-)(and (BELOWOF12-ROBOT)))

(:derived (Flag211-)(and (BELOWOF7-ROBOT)))

(:derived (Flag211-)(and (BELOWOF9-ROBOT)))

(:derived (Flag211-)(and (BELOWOF3-ROBOT)))

(:derived (Flag211-)(and (BELOWOF13-ROBOT)))

(:derived (Flag211-)(and (BELOWOF8-ROBOT)))

(:derived (Flag211-)(and (BELOWOF14-ROBOT)))

(:derived (Flag212-)(and (BELOWOF10-ROBOT)))

(:derived (Flag212-)(and (BELOWOF11-ROBOT)))

(:derived (Flag212-)(and (BELOWOF6-ROBOT)))

(:derived (Flag212-)(and (BELOWOF5-ROBOT)))

(:derived (Flag212-)(and (BELOWOF2-ROBOT)))

(:derived (Flag212-)(and (BELOWOF1-ROBOT)))

(:derived (Flag212-)(and (BELOWOF4-ROBOT)))

(:derived (Flag212-)(and (BELOWOF12-ROBOT)))

(:derived (Flag212-)(and (BELOWOF7-ROBOT)))

(:derived (Flag212-)(and (BELOWOF9-ROBOT)))

(:derived (Flag212-)(and (BELOWOF3-ROBOT)))

(:derived (Flag212-)(and (BELOWOF13-ROBOT)))

(:derived (Flag212-)(and (BELOWOF8-ROBOT)))

(:derived (Flag212-)(and (BELOWOF14-ROBOT)))

(:derived (Flag213-)(and (BELOWOF10-ROBOT)))

(:derived (Flag213-)(and (BELOWOF16-ROBOT)))

(:derived (Flag213-)(and (BELOWOF6-ROBOT)))

(:derived (Flag213-)(and (BELOWOF11-ROBOT)))

(:derived (Flag213-)(and (BELOWOF5-ROBOT)))

(:derived (Flag213-)(and (BELOWOF2-ROBOT)))

(:derived (Flag213-)(and (BELOWOF1-ROBOT)))

(:derived (Flag213-)(and (BELOWOF4-ROBOT)))

(:derived (Flag213-)(and (BELOWOF12-ROBOT)))

(:derived (Flag213-)(and (BELOWOF7-ROBOT)))

(:derived (Flag213-)(and (BELOWOF9-ROBOT)))

(:derived (Flag213-)(and (BELOWOF3-ROBOT)))

(:derived (Flag213-)(and (BELOWOF13-ROBOT)))

(:derived (Flag213-)(and (BELOWOF8-ROBOT)))

(:derived (Flag213-)(and (BELOWOF14-ROBOT)))

(:derived (Flag214-)(and (ABOVEOF6-ROBOT)))

(:derived (Flag214-)(and (ABOVEOF11-ROBOT)))

(:derived (Flag214-)(and (ABOVEOF10-ROBOT)))

(:derived (Flag214-)(and (ABOVEOF2-ROBOT)))

(:derived (Flag214-)(and (ABOVEOF7-ROBOT)))

(:derived (Flag214-)(and (ABOVEOF12-ROBOT)))

(:derived (Flag214-)(and (ABOVEOF15-ROBOT)))

(:derived (Flag214-)(and (ABOVEOF8-ROBOT)))

(:derived (Flag214-)(and (ABOVEOF9-ROBOT)))

(:derived (Flag214-)(and (ABOVEOF4-ROBOT)))

(:derived (Flag214-)(and (ABOVEOF3-ROBOT)))

(:derived (Flag214-)(and (ABOVEOF13-ROBOT)))

(:derived (Flag214-)(and (ABOVEOF14-ROBOT)))

(:derived (Flag214-)(and (ABOVEOF5-ROBOT)))

(:derived (Flag214-)(and (ABOVEOF1-ROBOT)))

(:derived (Flag215-)(and (ABOVEOF11-ROBOT)))

(:derived (Flag215-)(and (ABOVEOF6-ROBOT)))

(:derived (Flag215-)(and (ABOVEOF10-ROBOT)))

(:derived (Flag215-)(and (ABOVEOF2-ROBOT)))

(:derived (Flag215-)(and (ABOVEOF7-ROBOT)))

(:derived (Flag215-)(and (ABOVEOF12-ROBOT)))

(:derived (Flag215-)(and (ABOVEOF15-ROBOT)))

(:derived (Flag215-)(and (ABOVEOF8-ROBOT)))

(:derived (Flag215-)(and (ABOVEOF9-ROBOT)))

(:derived (Flag215-)(and (ABOVEOF4-ROBOT)))

(:derived (Flag215-)(and (ABOVEOF3-ROBOT)))

(:derived (Flag215-)(and (ABOVEOF13-ROBOT)))

(:derived (Flag215-)(and (ABOVEOF14-ROBOT)))

(:derived (Flag215-)(and (ABOVEOF5-ROBOT)))

(:derived (Flag216-)(and (ABOVEOF11-ROBOT)))

(:derived (Flag216-)(and (ABOVEOF6-ROBOT)))

(:derived (Flag216-)(and (ABOVEOF10-ROBOT)))

(:derived (Flag216-)(and (ABOVEOF8-ROBOT)))

(:derived (Flag216-)(and (ABOVEOF7-ROBOT)))

(:derived (Flag216-)(and (ABOVEOF12-ROBOT)))

(:derived (Flag216-)(and (ABOVEOF15-ROBOT)))

(:derived (Flag216-)(and (ABOVEOF9-ROBOT)))

(:derived (Flag216-)(and (ABOVEOF4-ROBOT)))

(:derived (Flag216-)(and (ABOVEOF3-ROBOT)))

(:derived (Flag216-)(and (ABOVEOF13-ROBOT)))

(:derived (Flag216-)(and (ABOVEOF14-ROBOT)))

(:derived (Flag216-)(and (ABOVEOF5-ROBOT)))

(:derived (Flag217-)(and (ABOVEOF11-ROBOT)))

(:derived (Flag217-)(and (ABOVEOF6-ROBOT)))

(:derived (Flag217-)(and (ABOVEOF10-ROBOT)))

(:derived (Flag217-)(and (ABOVEOF8-ROBOT)))

(:derived (Flag217-)(and (ABOVEOF7-ROBOT)))

(:derived (Flag217-)(and (ABOVEOF12-ROBOT)))

(:derived (Flag217-)(and (ABOVEOF15-ROBOT)))

(:derived (Flag217-)(and (ABOVEOF9-ROBOT)))

(:derived (Flag217-)(and (ABOVEOF4-ROBOT)))

(:derived (Flag217-)(and (ABOVEOF13-ROBOT)))

(:derived (Flag217-)(and (ABOVEOF14-ROBOT)))

(:derived (Flag217-)(and (ABOVEOF5-ROBOT)))

(:derived (Flag218-)(and (ABOVEOF11-ROBOT)))

(:derived (Flag218-)(and (ABOVEOF6-ROBOT)))

(:derived (Flag218-)(and (ABOVEOF7-ROBOT)))

(:derived (Flag218-)(and (ABOVEOF8-ROBOT)))

(:derived (Flag218-)(and (ABOVEOF10-ROBOT)))

(:derived (Flag218-)(and (ABOVEOF12-ROBOT)))

(:derived (Flag218-)(and (ABOVEOF15-ROBOT)))

(:derived (Flag218-)(and (ABOVEOF9-ROBOT)))

(:derived (Flag218-)(and (ABOVEOF13-ROBOT)))

(:derived (Flag218-)(and (ABOVEOF14-ROBOT)))

(:derived (Flag218-)(and (ABOVEOF5-ROBOT)))

(:derived (Flag219-)(and (ABOVEOF11-ROBOT)))

(:derived (Flag219-)(and (ABOVEOF6-ROBOT)))

(:derived (Flag219-)(and (ABOVEOF7-ROBOT)))

(:derived (Flag219-)(and (ABOVEOF8-ROBOT)))

(:derived (Flag219-)(and (ABOVEOF10-ROBOT)))

(:derived (Flag219-)(and (ABOVEOF12-ROBOT)))

(:derived (Flag219-)(and (ABOVEOF15-ROBOT)))

(:derived (Flag219-)(and (ABOVEOF9-ROBOT)))

(:derived (Flag219-)(and (ABOVEOF13-ROBOT)))

(:derived (Flag219-)(and (ABOVEOF14-ROBOT)))

(:derived (Flag220-)(and (ABOVEOF11-ROBOT)))

(:derived (Flag220-)(and (ABOVEOF7-ROBOT)))

(:derived (Flag220-)(and (ABOVEOF8-ROBOT)))

(:derived (Flag220-)(and (ABOVEOF10-ROBOT)))

(:derived (Flag220-)(and (ABOVEOF12-ROBOT)))

(:derived (Flag220-)(and (ABOVEOF15-ROBOT)))

(:derived (Flag220-)(and (ABOVEOF9-ROBOT)))

(:derived (Flag220-)(and (ABOVEOF13-ROBOT)))

(:derived (Flag220-)(and (ABOVEOF14-ROBOT)))

(:derived (Flag221-)(and (ABOVEOF11-ROBOT)))

(:derived (Flag221-)(and (ABOVEOF8-ROBOT)))

(:derived (Flag221-)(and (ABOVEOF10-ROBOT)))

(:derived (Flag221-)(and (ABOVEOF12-ROBOT)))

(:derived (Flag221-)(and (ABOVEOF15-ROBOT)))

(:derived (Flag221-)(and (ABOVEOF9-ROBOT)))

(:derived (Flag221-)(and (ABOVEOF13-ROBOT)))

(:derived (Flag221-)(and (ABOVEOF14-ROBOT)))

(:derived (Flag222-)(and (ABOVEOF11-ROBOT)))

(:derived (Flag222-)(and (ABOVEOF10-ROBOT)))

(:derived (Flag222-)(and (ABOVEOF12-ROBOT)))

(:derived (Flag222-)(and (ABOVEOF15-ROBOT)))

(:derived (Flag222-)(and (ABOVEOF9-ROBOT)))

(:derived (Flag222-)(and (ABOVEOF13-ROBOT)))

(:derived (Flag222-)(and (ABOVEOF14-ROBOT)))

(:derived (Flag223-)(and (ABOVEOF11-ROBOT)))

(:derived (Flag223-)(and (ABOVEOF10-ROBOT)))

(:derived (Flag223-)(and (ABOVEOF12-ROBOT)))

(:derived (Flag223-)(and (ABOVEOF15-ROBOT)))

(:derived (Flag223-)(and (ABOVEOF13-ROBOT)))

(:derived (Flag223-)(and (ABOVEOF14-ROBOT)))

(:derived (Flag224-)(and (ABOVEOF12-ROBOT)))

(:derived (Flag224-)(and (ABOVEOF13-ROBOT)))

(:derived (Flag224-)(and (ABOVEOF14-ROBOT)))

(:derived (Flag224-)(and (ABOVEOF11-ROBOT)))

(:derived (Flag224-)(and (ABOVEOF15-ROBOT)))

(:derived (Flag225-)(and (ABOVEOF12-ROBOT)))

(:derived (Flag225-)(and (ABOVEOF13-ROBOT)))

(:derived (Flag225-)(and (ABOVEOF14-ROBOT)))

(:derived (Flag225-)(and (ABOVEOF15-ROBOT)))

(:derived (Flag226-)(and (ABOVEOF13-ROBOT)))

(:derived (Flag226-)(and (ABOVEOF14-ROBOT)))

(:derived (Flag226-)(and (ABOVEOF15-ROBOT)))

(:derived (Flag227-)(and (ABOVEOF14-ROBOT)))

(:derived (Flag227-)(and (ABOVEOF15-ROBOT)))

(:derived (Flag228-)(and (ABOVEOF2-ROBOT)(BELOWOF2-ROBOT)))

(:derived (Flag229-)(and (ABOVEOF11-ROBOT)(BELOWOF1-ROBOT)))

(:derived (Flag230-)(and (ABOVEOF1-ROBOT)(BELOWOF2-ROBOT)))

(:derived (Flag231-)(and (ABOVEOF7-ROBOT)(BELOWOF2-ROBOT)))

(:derived (Flag232-)(and (ABOVEOF15-ROBOT)(BELOWOF2-ROBOT)))

(:derived (Flag233-)(and (BELOWOF2-ROBOT)(ABOVEOF13-ROBOT)))

(:derived (Flag234-)(and (BELOWOF1-ROBOT)(ABOVEOF7-ROBOT)))

(:derived (Flag235-)(and (BELOWOF1-ROBOT)(ABOVEOF1-ROBOT)))

(:derived (Flag236-)(and (BELOWOF1-ROBOT)(ABOVEOF5-ROBOT)))

(:derived (Flag237-)(and (ABOVEOF4-ROBOT)(BELOWOF2-ROBOT)))

(:derived (Flag238-)(and (BELOWOF2-ROBOT)(ABOVEOF14-ROBOT)))

(:derived (Flag239-)(and (ABOVEOF6-ROBOT)(BELOWOF2-ROBOT)))

(:derived (Flag240-)(and (BELOWOF1-ROBOT)(ABOVEOF2-ROBOT)))

(:derived (Flag241-)(and (ABOVEOF9-ROBOT)(BELOWOF2-ROBOT)))

(:derived (Flag242-)(and (BELOWOF1-ROBOT)(ABOVEOF13-ROBOT)))

(:derived (Flag243-)(and (ABOVEOF6-ROBOT)(BELOWOF1-ROBOT)))

(:derived (Flag244-)(and (BELOWOF1-ROBOT)(ABOVEOF14-ROBOT)))

(:derived (Flag245-)(and (ABOVEOF10-ROBOT)(BELOWOF1-ROBOT)))

(:derived (Flag246-)(and (ABOVEOF11-ROBOT)(BELOWOF2-ROBOT)))

(:derived (Flag247-)(and (ABOVEOF3-ROBOT)(BELOWOF1-ROBOT)))

(:derived (Flag248-)(and (BELOWOF1-ROBOT)(ABOVEOF9-ROBOT)))

(:derived (Flag249-)(and (ABOVEOF10-ROBOT)(BELOWOF2-ROBOT)))

(:derived (Flag250-)(and (ABOVEOF8-ROBOT)(BELOWOF2-ROBOT)))

(:derived (Flag251-)(and (BELOWOF1-ROBOT)(ABOVEOF4-ROBOT)))

(:derived (Flag252-)(and (ABOVEOF12-ROBOT)(BELOWOF2-ROBOT)))

(:derived (Flag253-)(and (ABOVEOF12-ROBOT)(BELOWOF1-ROBOT)))

(:derived (Flag254-)(and (BELOWOF2-ROBOT)(ABOVEOF5-ROBOT)))

(:derived (Flag255-)(and (BELOWOF1-ROBOT)(ABOVEOF15-ROBOT)))

(:derived (Flag256-)(and (ABOVEOF8-ROBOT)(BELOWOF1-ROBOT)))

(:derived (Flag257-)(and (ABOVEOF3-ROBOT)(BELOWOF2-ROBOT)))

(:derived (Flag259-)(and (BELOWOF3-ROBOT)(ABOVEOF5-ROBOT)))

(:derived (Flag260-)(and (BELOWOF3-ROBOT)(ABOVEOF6-ROBOT)))

(:derived (Flag261-)(and (BELOWOF3-ROBOT)(ABOVEOF7-ROBOT)))

(:derived (Flag262-)(and (BELOWOF3-ROBOT)(ABOVEOF14-ROBOT)))

(:derived (Flag263-)(and (BELOWOF3-ROBOT)(ABOVEOF9-ROBOT)))

(:derived (Flag264-)(and (BELOWOF3-ROBOT)(ABOVEOF15-ROBOT)))

(:derived (Flag265-)(and (BELOWOF3-ROBOT)(ABOVEOF2-ROBOT)))

(:derived (Flag266-)(and (ABOVEOF3-ROBOT)(BELOWOF3-ROBOT)))

(:derived (Flag267-)(and (ABOVEOF8-ROBOT)(BELOWOF3-ROBOT)))

(:derived (Flag268-)(and (BELOWOF3-ROBOT)(ABOVEOF4-ROBOT)))

(:derived (Flag269-)(and (BELOWOF3-ROBOT)(ABOVEOF10-ROBOT)))

(:derived (Flag270-)(and (BELOWOF2-ROBOT)(ABOVEOF2-ROBOT)))

(:derived (Flag271-)(and (ABOVEOF12-ROBOT)(BELOWOF3-ROBOT)))

(:derived (Flag272-)(and (BELOWOF3-ROBOT)(ABOVEOF13-ROBOT)))

(:derived (Flag273-)(and (ABOVEOF11-ROBOT)(BELOWOF3-ROBOT)))

(:derived (Flag275-)(and (BELOWOF4-ROBOT)(ABOVEOF13-ROBOT)))

(:derived (Flag276-)(and (ABOVEOF3-ROBOT)(BELOWOF4-ROBOT)))

(:derived (Flag277-)(and (ABOVEOF6-ROBOT)(BELOWOF4-ROBOT)))

(:derived (Flag278-)(and (BELOWOF4-ROBOT)(ABOVEOF4-ROBOT)))

(:derived (Flag279-)(and (ABOVEOF9-ROBOT)(BELOWOF4-ROBOT)))

(:derived (Flag280-)(and (ABOVEOF8-ROBOT)(BELOWOF4-ROBOT)))

(:derived (Flag281-)(and (BELOWOF4-ROBOT)(ABOVEOF7-ROBOT)))

(:derived (Flag282-)(and (ABOVEOF12-ROBOT)(BELOWOF4-ROBOT)))

(:derived (Flag283-)(and (BELOWOF4-ROBOT)(ABOVEOF5-ROBOT)))

(:derived (Flag284-)(and (BELOWOF3-ROBOT)(ABOVEOF3-ROBOT)))

(:derived (Flag285-)(and (ABOVEOF10-ROBOT)(BELOWOF4-ROBOT)))

(:derived (Flag286-)(and (BELOWOF4-ROBOT)(ABOVEOF15-ROBOT)))

(:derived (Flag287-)(and (BELOWOF3-ROBOT)(ABOVEOF11-ROBOT)))

(:derived (Flag288-)(and (ABOVEOF11-ROBOT)(BELOWOF4-ROBOT)))

(:derived (Flag289-)(and (BELOWOF4-ROBOT)(ABOVEOF14-ROBOT)))

(:derived (Flag291-)(and (ABOVEOF4-ROBOT)(BELOWOF5-ROBOT)))

(:derived (Flag292-)(and (ABOVEOF15-ROBOT)(BELOWOF5-ROBOT)))

(:derived (Flag293-)(and (ABOVEOF8-ROBOT)(BELOWOF5-ROBOT)))

(:derived (Flag294-)(and (ABOVEOF13-ROBOT)(BELOWOF5-ROBOT)))

(:derived (Flag295-)(and (ABOVEOF10-ROBOT)(BELOWOF5-ROBOT)))

(:derived (Flag296-)(and (ABOVEOF11-ROBOT)(BELOWOF5-ROBOT)))

(:derived (Flag297-)(and (ABOVEOF5-ROBOT)(BELOWOF5-ROBOT)))

(:derived (Flag298-)(and (ABOVEOF7-ROBOT)(BELOWOF5-ROBOT)))

(:derived (Flag299-)(and (ABOVEOF9-ROBOT)(BELOWOF5-ROBOT)))

(:derived (Flag300-)(and (ABOVEOF12-ROBOT)(BELOWOF5-ROBOT)))

(:derived (Flag301-)(and (ABOVEOF6-ROBOT)(BELOWOF5-ROBOT)))

(:derived (Flag302-)(and (ABOVEOF14-ROBOT)(BELOWOF5-ROBOT)))

(:derived (Flag304-)(and (BELOWOF6-ROBOT)(ABOVEOF15-ROBOT)))

(:derived (Flag305-)(and (BELOWOF6-ROBOT)(ABOVEOF5-ROBOT)))

(:derived (Flag306-)(and (ABOVEOF11-ROBOT)(BELOWOF6-ROBOT)))

(:derived (Flag307-)(and (ABOVEOF12-ROBOT)(BELOWOF6-ROBOT)))

(:derived (Flag308-)(and (BELOWOF5-ROBOT)(ABOVEOF13-ROBOT)))

(:derived (Flag309-)(and (BELOWOF6-ROBOT)(ABOVEOF13-ROBOT)))

(:derived (Flag310-)(and (BELOWOF6-ROBOT)(ABOVEOF7-ROBOT)))

(:derived (Flag311-)(and (BELOWOF5-ROBOT)(ABOVEOF5-ROBOT)))

(:derived (Flag312-)(and (BELOWOF6-ROBOT)(ABOVEOF6-ROBOT)))

(:derived (Flag313-)(and (ABOVEOF8-ROBOT)(BELOWOF6-ROBOT)))

(:derived (Flag314-)(and (BELOWOF6-ROBOT)(ABOVEOF9-ROBOT)))

(:derived (Flag315-)(and (BELOWOF6-ROBOT)(ABOVEOF14-ROBOT)))

(:derived (Flag316-)(and (BELOWOF6-ROBOT)(ABOVEOF10-ROBOT)))

(:derived (Flag318-)(and (ABOVEOF6-ROBOT)(BELOWOF7-ROBOT)))

(:derived (Flag319-)(and (ABOVEOF9-ROBOT)(BELOWOF7-ROBOT)))

(:derived (Flag320-)(and (ABOVEOF6-ROBOT)(BELOWOF6-ROBOT)))

(:derived (Flag321-)(and (ABOVEOF10-ROBOT)(BELOWOF7-ROBOT)))

(:derived (Flag322-)(and (ABOVEOF10-ROBOT)(BELOWOF6-ROBOT)))

(:derived (Flag323-)(and (BELOWOF7-ROBOT)(ABOVEOF15-ROBOT)))

(:derived (Flag324-)(and (ABOVEOF12-ROBOT)(BELOWOF7-ROBOT)))

(:derived (Flag325-)(and (BELOWOF7-ROBOT)(ABOVEOF14-ROBOT)))

(:derived (Flag326-)(and (BELOWOF7-ROBOT)(ABOVEOF13-ROBOT)))

(:derived (Flag327-)(and (ABOVEOF7-ROBOT)(BELOWOF7-ROBOT)))

(:derived (Flag328-)(and (ABOVEOF8-ROBOT)(BELOWOF7-ROBOT)))

(:derived (Flag329-)(and (ABOVEOF11-ROBOT)(BELOWOF7-ROBOT)))

(:derived (Flag331-)(and (BELOWOF8-ROBOT)(ABOVEOF13-ROBOT)))

(:derived (Flag332-)(and (BELOWOF8-ROBOT)(ABOVEOF10-ROBOT)))

(:derived (Flag333-)(and (ABOVEOF12-ROBOT)(BELOWOF8-ROBOT)))

(:derived (Flag334-)(and (ABOVEOF8-ROBOT)(BELOWOF8-ROBOT)))

(:derived (Flag335-)(and (BELOWOF8-ROBOT)(ABOVEOF9-ROBOT)))

(:derived (Flag336-)(and (BELOWOF7-ROBOT)(ABOVEOF7-ROBOT)))

(:derived (Flag337-)(and (BELOWOF8-ROBOT)(ABOVEOF7-ROBOT)))

(:derived (Flag338-)(and (BELOWOF8-ROBOT)(ABOVEOF15-ROBOT)))

(:derived (Flag339-)(and (BELOWOF8-ROBOT)(ABOVEOF11-ROBOT)))

(:derived (Flag340-)(and (BELOWOF8-ROBOT)(ABOVEOF14-ROBOT)))

(:derived (Flag342-)(and (BELOWOF8-ROBOT)(ABOVEOF12-ROBOT)))

(:derived (Flag343-)(and (ABOVEOF10-ROBOT)(BELOWOF9-ROBOT)))

(:derived (Flag344-)(and (BELOWOF8-ROBOT)(ABOVEOF8-ROBOT)))

(:derived (Flag345-)(and (BELOWOF9-ROBOT)(ABOVEOF14-ROBOT)))

(:derived (Flag346-)(and (BELOWOF9-ROBOT)(ABOVEOF13-ROBOT)))

(:derived (Flag347-)(and (ABOVEOF8-ROBOT)(BELOWOF9-ROBOT)))

(:derived (Flag348-)(and (ABOVEOF12-ROBOT)(BELOWOF9-ROBOT)))

(:derived (Flag349-)(and (ABOVEOF11-ROBOT)(BELOWOF9-ROBOT)))

(:derived (Flag350-)(and (BELOWOF9-ROBOT)(ABOVEOF9-ROBOT)))

(:derived (Flag351-)(and (BELOWOF9-ROBOT)(ABOVEOF15-ROBOT)))

(:derived (Flag353-)(and (BELOWOF10-ROBOT)(ABOVEOF15-ROBOT)))

(:derived (Flag354-)(and (BELOWOF10-ROBOT)(ABOVEOF9-ROBOT)))

(:derived (Flag355-)(and (BELOWOF10-ROBOT)(ABOVEOF14-ROBOT)))

(:derived (Flag356-)(and (BELOWOF10-ROBOT)(ABOVEOF10-ROBOT)))

(:derived (Flag357-)(and (ABOVEOF12-ROBOT)(BELOWOF10-ROBOT)))

(:derived (Flag358-)(and (ABOVEOF9-ROBOT)(BELOWOF9-ROBOT)))

(:derived (Flag359-)(and (ABOVEOF11-ROBOT)(BELOWOF10-ROBOT)))

(:derived (Flag360-)(and (BELOWOF10-ROBOT)(ABOVEOF13-ROBOT)))

(:derived (Flag362-)(and (BELOWOF11-ROBOT)(ABOVEOF10-ROBOT)))

(:derived (Flag363-)(and (ABOVEOF11-ROBOT)(BELOWOF11-ROBOT)))

(:derived (Flag364-)(and (BELOWOF11-ROBOT)(ABOVEOF13-ROBOT)))

(:derived (Flag365-)(and (BELOWOF11-ROBOT)(ABOVEOF15-ROBOT)))

(:derived (Flag366-)(and (BELOWOF11-ROBOT)(ABOVEOF14-ROBOT)))

(:derived (Flag367-)(and (ABOVEOF12-ROBOT)(BELOWOF11-ROBOT)))

(:derived (Flag369-)(and (BELOWOF12-ROBOT)(ABOVEOF15-ROBOT)))

(:derived (Flag370-)(and (ABOVEOF12-ROBOT)(BELOWOF12-ROBOT)))

(:derived (Flag371-)(and (BELOWOF12-ROBOT)(ABOVEOF13-ROBOT)))

(:derived (Flag372-)(and (BELOWOF12-ROBOT)(ABOVEOF11-ROBOT)))

(:derived (Flag373-)(and (BELOWOF12-ROBOT)(ABOVEOF14-ROBOT)))

(:derived (Flag375-)(and (ABOVEOF14-ROBOT)(BELOWOF13-ROBOT)))

(:derived (Flag376-)(and (ABOVEOF13-ROBOT)(BELOWOF13-ROBOT)))

(:derived (Flag377-)(and (ABOVEOF15-ROBOT)(BELOWOF13-ROBOT)))

(:derived (Flag378-)(and (ABOVEOF12-ROBOT)(BELOWOF13-ROBOT)))

(:derived (Flag379-)(and (BELOWOF12-ROBOT)(ABOVEOF12-ROBOT)))

(:derived (Flag381-)(and (BELOWOF14-ROBOT)(ABOVEOF14-ROBOT)))

(:derived (Flag382-)(and (ABOVEOF15-ROBOT)(BELOWOF14-ROBOT)))

(:derived (Flag383-)(and (BELOWOF13-ROBOT)(ABOVEOF13-ROBOT)))

(:derived (Flag384-)(and (BELOWOF14-ROBOT)(ABOVEOF13-ROBOT)))

(:derived (Flag386-)(and (BELOWOF15-ROBOT)(ABOVEOF14-ROBOT)))

(:derived (Flag387-)(and (BELOWOF15-ROBOT)(ABOVEOF15-ROBOT)))

(:derived (Flag389-)(and (BELOWOF16-ROBOT)(ABOVEOF15-ROBOT)))

(:derived (Flag391-)(and (BELOWOF1-ROBOT)))

(:action MOVEUP-ROBOT
:parameters ()
:precondition
(and
(Flag2-)
)
:effect
(and
(when
(and
(Flag388-)
)
(and
(ROW15-ROBOT)
(not (NOT-ROW15-ROBOT))
)
)
(when
(and
(Flag385-)
)
(and
(ROW14-ROBOT)
(not (NOT-ROW14-ROBOT))
)
)
(when
(and
(Flag380-)
)
(and
(ROW13-ROBOT)
(not (NOT-ROW13-ROBOT))
)
)
(when
(and
(Flag374-)
)
(and
(ROW12-ROBOT)
(not (NOT-ROW12-ROBOT))
)
)
(when
(and
(Flag368-)
)
(and
(ROW11-ROBOT)
(not (NOT-ROW11-ROBOT))
)
)
(when
(and
(Flag361-)
)
(and
(ROW10-ROBOT)
(not (NOT-ROW10-ROBOT))
)
)
(when
(and
(Flag352-)
)
(and
(ROW9-ROBOT)
(not (NOT-ROW9-ROBOT))
)
)
(when
(and
(Flag341-)
)
(and
(ROW8-ROBOT)
(not (NOT-ROW8-ROBOT))
)
)
(when
(and
(Flag330-)
)
(and
(ROW7-ROBOT)
(not (NOT-ROW7-ROBOT))
)
)
(when
(and
(Flag317-)
)
(and
(ROW6-ROBOT)
(not (NOT-ROW6-ROBOT))
)
)
(when
(and
(Flag303-)
)
(and
(ROW5-ROBOT)
(not (NOT-ROW5-ROBOT))
)
)
(when
(and
(Flag290-)
)
(and
(ROW4-ROBOT)
(not (NOT-ROW4-ROBOT))
)
)
(when
(and
(Flag274-)
)
(and
(ROW3-ROBOT)
(not (NOT-ROW3-ROBOT))
)
)
(when
(and
(Flag258-)
)
(and
(ROW2-ROBOT)
(not (NOT-ROW2-ROBOT))
)
)
(when
(and
(Flag391-)
)
(and
(ROW1-ROBOT)
(not (NOT-ROW1-ROBOT))
)
)
(when
(and
(ROW14-ROBOT)
)
(and
(ROW15-ROBOT)
(NOT-ROW14-ROBOT)
(not (NOT-ROW15-ROBOT))
(not (ROW14-ROBOT))
)
)
(when
(and
(ROW13-ROBOT)
)
(and
(ROW14-ROBOT)
(NOT-ROW13-ROBOT)
(not (NOT-ROW14-ROBOT))
(not (ROW13-ROBOT))
)
)
(when
(and
(ROW12-ROBOT)
)
(and
(ROW13-ROBOT)
(NOT-ROW12-ROBOT)
(not (NOT-ROW13-ROBOT))
(not (ROW12-ROBOT))
)
)
(when
(and
(ROW11-ROBOT)
)
(and
(ROW12-ROBOT)
(NOT-ROW11-ROBOT)
(not (NOT-ROW12-ROBOT))
(not (ROW11-ROBOT))
)
)
(when
(and
(ROW10-ROBOT)
)
(and
(ROW11-ROBOT)
(NOT-ROW10-ROBOT)
(not (NOT-ROW11-ROBOT))
(not (ROW10-ROBOT))
)
)
(when
(and
(ROW9-ROBOT)
)
(and
(ROW10-ROBOT)
(NOT-ROW9-ROBOT)
(not (NOT-ROW10-ROBOT))
(not (ROW9-ROBOT))
)
)
(when
(and
(ROW8-ROBOT)
)
(and
(ROW9-ROBOT)
(NOT-ROW8-ROBOT)
(not (NOT-ROW9-ROBOT))
(not (ROW8-ROBOT))
)
)
(when
(and
(ROW7-ROBOT)
)
(and
(ROW8-ROBOT)
(NOT-ROW7-ROBOT)
(not (NOT-ROW8-ROBOT))
(not (ROW7-ROBOT))
)
)
(when
(and
(ROW6-ROBOT)
)
(and
(ROW7-ROBOT)
(NOT-ROW6-ROBOT)
(not (NOT-ROW7-ROBOT))
(not (ROW6-ROBOT))
)
)
(when
(and
(ROW5-ROBOT)
)
(and
(ROW6-ROBOT)
(NOT-ROW5-ROBOT)
(not (NOT-ROW6-ROBOT))
(not (ROW5-ROBOT))
)
)
(when
(and
(ROW4-ROBOT)
)
(and
(ROW5-ROBOT)
(NOT-ROW4-ROBOT)
(not (NOT-ROW5-ROBOT))
(not (ROW4-ROBOT))
)
)
(when
(and
(ROW3-ROBOT)
)
(and
(ROW4-ROBOT)
(NOT-ROW3-ROBOT)
(not (NOT-ROW4-ROBOT))
(not (ROW3-ROBOT))
)
)
(when
(and
(ROW2-ROBOT)
)
(and
(ROW3-ROBOT)
(NOT-ROW2-ROBOT)
(not (NOT-ROW3-ROBOT))
(not (ROW2-ROBOT))
)
)
(when
(and
(ROW1-ROBOT)
)
(and
(ROW2-ROBOT)
(NOT-ROW1-ROBOT)
(not (NOT-ROW2-ROBOT))
(not (ROW1-ROBOT))
)
)
(when
(and
(ROW0-ROBOT)
)
(and
(ROW1-ROBOT)
(NOT-ROW0-ROBOT)
(not (NOT-ROW1-ROBOT))
(not (ROW0-ROBOT))
)
)
(when
(and
(BELOWOF14-ROBOT)
)
(and
(BELOWOF16-ROBOT)
(NOT-BELOWOF15-ROBOT)
(BELOWOF15-ROBOT)
(NOT-BELOWOF14-ROBOT)
(not (NOT-BELOWOF16-ROBOT))
(not (BELOWOF14-ROBOT))
)
)
(when
(and
(BELOWOF8-ROBOT)
)
(and
(BELOWOF16-ROBOT)
(NOT-BELOWOF15-ROBOT)
(BELOWOF15-ROBOT)
(NOT-BELOWOF14-ROBOT)
(BELOWOF14-ROBOT)
(NOT-BELOWOF13-ROBOT)
(BELOWOF13-ROBOT)
(NOT-BELOWOF12-ROBOT)
(BELOWOF12-ROBOT)
(NOT-BELOWOF11-ROBOT)
(BELOWOF11-ROBOT)
(NOT-BELOWOF10-ROBOT)
(BELOWOF10-ROBOT)
(NOT-BELOWOF9-ROBOT)
(BELOWOF9-ROBOT)
(NOT-BELOWOF8-ROBOT)
(not (NOT-BELOWOF16-ROBOT))
(not (BELOWOF8-ROBOT))
)
)
(when
(and
(BELOWOF13-ROBOT)
)
(and
(BELOWOF16-ROBOT)
(NOT-BELOWOF15-ROBOT)
(BELOWOF15-ROBOT)
(NOT-BELOWOF14-ROBOT)
(BELOWOF14-ROBOT)
(NOT-BELOWOF13-ROBOT)
(not (NOT-BELOWOF16-ROBOT))
(not (BELOWOF13-ROBOT))
)
)
(when
(and
(BELOWOF3-ROBOT)
)
(and
(BELOWOF16-ROBOT)
(NOT-BELOWOF15-ROBOT)
(BELOWOF15-ROBOT)
(NOT-BELOWOF14-ROBOT)
(BELOWOF14-ROBOT)
(NOT-BELOWOF13-ROBOT)
(BELOWOF13-ROBOT)
(NOT-BELOWOF12-ROBOT)
(BELOWOF12-ROBOT)
(NOT-BELOWOF11-ROBOT)
(BELOWOF11-ROBOT)
(NOT-BELOWOF10-ROBOT)
(BELOWOF10-ROBOT)
(NOT-BELOWOF9-ROBOT)
(BELOWOF9-ROBOT)
(NOT-BELOWOF8-ROBOT)
(BELOWOF8-ROBOT)
(NOT-BELOWOF7-ROBOT)
(BELOWOF7-ROBOT)
(NOT-BELOWOF6-ROBOT)
(BELOWOF6-ROBOT)
(NOT-BELOWOF5-ROBOT)
(BELOWOF5-ROBOT)
(NOT-BELOWOF4-ROBOT)
(BELOWOF4-ROBOT)
(NOT-BELOWOF3-ROBOT)
(not (NOT-BELOWOF16-ROBOT))
(not (BELOWOF3-ROBOT))
)
)
(when
(and
(BELOWOF9-ROBOT)
)
(and
(BELOWOF16-ROBOT)
(NOT-BELOWOF15-ROBOT)
(BELOWOF15-ROBOT)
(NOT-BELOWOF14-ROBOT)
(BELOWOF14-ROBOT)
(NOT-BELOWOF13-ROBOT)
(BELOWOF13-ROBOT)
(NOT-BELOWOF12-ROBOT)
(BELOWOF12-ROBOT)
(NOT-BELOWOF11-ROBOT)
(BELOWOF11-ROBOT)
(NOT-BELOWOF10-ROBOT)
(BELOWOF10-ROBOT)
(NOT-BELOWOF9-ROBOT)
(not (NOT-BELOWOF16-ROBOT))
(not (BELOWOF9-ROBOT))
)
)
(when
(and
(BELOWOF7-ROBOT)
)
(and
(BELOWOF16-ROBOT)
(NOT-BELOWOF15-ROBOT)
(BELOWOF15-ROBOT)
(NOT-BELOWOF14-ROBOT)
(BELOWOF14-ROBOT)
(NOT-BELOWOF13-ROBOT)
(BELOWOF13-ROBOT)
(NOT-BELOWOF12-ROBOT)
(BELOWOF12-ROBOT)
(NOT-BELOWOF11-ROBOT)
(BELOWOF11-ROBOT)
(NOT-BELOWOF10-ROBOT)
(BELOWOF10-ROBOT)
(NOT-BELOWOF9-ROBOT)
(BELOWOF9-ROBOT)
(NOT-BELOWOF8-ROBOT)
(BELOWOF8-ROBOT)
(NOT-BELOWOF7-ROBOT)
(not (NOT-BELOWOF16-ROBOT))
(not (BELOWOF7-ROBOT))
)
)
(when
(and
(BELOWOF12-ROBOT)
)
(and
(BELOWOF16-ROBOT)
(NOT-BELOWOF15-ROBOT)
(BELOWOF15-ROBOT)
(NOT-BELOWOF14-ROBOT)
(BELOWOF14-ROBOT)
(NOT-BELOWOF13-ROBOT)
(BELOWOF13-ROBOT)
(NOT-BELOWOF12-ROBOT)
(not (NOT-BELOWOF16-ROBOT))
(not (BELOWOF12-ROBOT))
)
)
(when
(and
(BELOWOF4-ROBOT)
)
(and
(BELOWOF16-ROBOT)
(NOT-BELOWOF15-ROBOT)
(BELOWOF15-ROBOT)
(NOT-BELOWOF14-ROBOT)
(BELOWOF14-ROBOT)
(NOT-BELOWOF13-ROBOT)
(BELOWOF13-ROBOT)
(NOT-BELOWOF12-ROBOT)
(BELOWOF12-ROBOT)
(NOT-BELOWOF11-ROBOT)
(BELOWOF11-ROBOT)
(NOT-BELOWOF10-ROBOT)
(BELOWOF10-ROBOT)
(NOT-BELOWOF9-ROBOT)
(BELOWOF9-ROBOT)
(NOT-BELOWOF8-ROBOT)
(BELOWOF8-ROBOT)
(NOT-BELOWOF7-ROBOT)
(BELOWOF7-ROBOT)
(NOT-BELOWOF6-ROBOT)
(BELOWOF6-ROBOT)
(NOT-BELOWOF5-ROBOT)
(BELOWOF5-ROBOT)
(NOT-BELOWOF4-ROBOT)
(not (NOT-BELOWOF16-ROBOT))
(not (BELOWOF4-ROBOT))
)
)
(when
(and
(BELOWOF1-ROBOT)
)
(and
(BELOWOF16-ROBOT)
(NOT-BELOWOF15-ROBOT)
(BELOWOF15-ROBOT)
(NOT-BELOWOF14-ROBOT)
(BELOWOF14-ROBOT)
(NOT-BELOWOF13-ROBOT)
(BELOWOF13-ROBOT)
(NOT-BELOWOF12-ROBOT)
(BELOWOF12-ROBOT)
(NOT-BELOWOF11-ROBOT)
(BELOWOF11-ROBOT)
(NOT-BELOWOF10-ROBOT)
(BELOWOF10-ROBOT)
(NOT-BELOWOF9-ROBOT)
(BELOWOF9-ROBOT)
(NOT-BELOWOF8-ROBOT)
(BELOWOF8-ROBOT)
(NOT-BELOWOF7-ROBOT)
(BELOWOF7-ROBOT)
(NOT-BELOWOF6-ROBOT)
(BELOWOF6-ROBOT)
(NOT-BELOWOF5-ROBOT)
(BELOWOF5-ROBOT)
(NOT-BELOWOF4-ROBOT)
(BELOWOF4-ROBOT)
(NOT-BELOWOF3-ROBOT)
(BELOWOF3-ROBOT)
(NOT-BELOWOF2-ROBOT)
(BELOWOF2-ROBOT)
(NOT-BELOWOF1-ROBOT)
(not (NOT-BELOWOF16-ROBOT))
(not (BELOWOF1-ROBOT))
)
)
(when
(and
(BELOWOF2-ROBOT)
)
(and
(BELOWOF16-ROBOT)
(NOT-BELOWOF15-ROBOT)
(BELOWOF15-ROBOT)
(NOT-BELOWOF14-ROBOT)
(BELOWOF14-ROBOT)
(NOT-BELOWOF13-ROBOT)
(BELOWOF13-ROBOT)
(NOT-BELOWOF12-ROBOT)
(BELOWOF12-ROBOT)
(NOT-BELOWOF11-ROBOT)
(BELOWOF11-ROBOT)
(NOT-BELOWOF10-ROBOT)
(BELOWOF10-ROBOT)
(NOT-BELOWOF9-ROBOT)
(BELOWOF9-ROBOT)
(NOT-BELOWOF8-ROBOT)
(BELOWOF8-ROBOT)
(NOT-BELOWOF7-ROBOT)
(BELOWOF7-ROBOT)
(NOT-BELOWOF6-ROBOT)
(BELOWOF6-ROBOT)
(NOT-BELOWOF5-ROBOT)
(BELOWOF5-ROBOT)
(NOT-BELOWOF4-ROBOT)
(BELOWOF4-ROBOT)
(NOT-BELOWOF3-ROBOT)
(BELOWOF3-ROBOT)
(NOT-BELOWOF2-ROBOT)
(not (NOT-BELOWOF16-ROBOT))
(not (BELOWOF2-ROBOT))
)
)
(when
(and
(BELOWOF5-ROBOT)
)
(and
(BELOWOF16-ROBOT)
(NOT-BELOWOF15-ROBOT)
(BELOWOF15-ROBOT)
(NOT-BELOWOF14-ROBOT)
(BELOWOF14-ROBOT)
(NOT-BELOWOF13-ROBOT)
(BELOWOF13-ROBOT)
(NOT-BELOWOF12-ROBOT)
(BELOWOF12-ROBOT)
(NOT-BELOWOF11-ROBOT)
(BELOWOF11-ROBOT)
(NOT-BELOWOF10-ROBOT)
(BELOWOF10-ROBOT)
(NOT-BELOWOF9-ROBOT)
(BELOWOF9-ROBOT)
(NOT-BELOWOF8-ROBOT)
(BELOWOF8-ROBOT)
(NOT-BELOWOF7-ROBOT)
(BELOWOF7-ROBOT)
(NOT-BELOWOF6-ROBOT)
(BELOWOF6-ROBOT)
(NOT-BELOWOF5-ROBOT)
(not (NOT-BELOWOF16-ROBOT))
(not (BELOWOF5-ROBOT))
)
)
(when
(and
(BELOWOF6-ROBOT)
)
(and
(BELOWOF16-ROBOT)
(NOT-BELOWOF15-ROBOT)
(BELOWOF15-ROBOT)
(NOT-BELOWOF14-ROBOT)
(BELOWOF14-ROBOT)
(NOT-BELOWOF13-ROBOT)
(BELOWOF13-ROBOT)
(NOT-BELOWOF12-ROBOT)
(BELOWOF12-ROBOT)
(NOT-BELOWOF11-ROBOT)
(BELOWOF11-ROBOT)
(NOT-BELOWOF10-ROBOT)
(BELOWOF10-ROBOT)
(NOT-BELOWOF9-ROBOT)
(BELOWOF9-ROBOT)
(NOT-BELOWOF8-ROBOT)
(BELOWOF8-ROBOT)
(NOT-BELOWOF7-ROBOT)
(BELOWOF7-ROBOT)
(NOT-BELOWOF6-ROBOT)
(not (NOT-BELOWOF16-ROBOT))
(not (BELOWOF6-ROBOT))
)
)
(when
(and
(BELOWOF15-ROBOT)
)
(and
(BELOWOF16-ROBOT)
(NOT-BELOWOF15-ROBOT)
(not (NOT-BELOWOF16-ROBOT))
(not (BELOWOF15-ROBOT))
)
)
(when
(and
(BELOWOF11-ROBOT)
)
(and
(BELOWOF16-ROBOT)
(NOT-BELOWOF15-ROBOT)
(BELOWOF15-ROBOT)
(NOT-BELOWOF14-ROBOT)
(BELOWOF14-ROBOT)
(NOT-BELOWOF13-ROBOT)
(BELOWOF13-ROBOT)
(NOT-BELOWOF12-ROBOT)
(BELOWOF12-ROBOT)
(NOT-BELOWOF11-ROBOT)
(not (NOT-BELOWOF16-ROBOT))
(not (BELOWOF11-ROBOT))
)
)
(when
(and
(BELOWOF10-ROBOT)
)
(and
(BELOWOF16-ROBOT)
(NOT-BELOWOF15-ROBOT)
(BELOWOF15-ROBOT)
(NOT-BELOWOF14-ROBOT)
(BELOWOF14-ROBOT)
(NOT-BELOWOF13-ROBOT)
(BELOWOF13-ROBOT)
(NOT-BELOWOF12-ROBOT)
(BELOWOF12-ROBOT)
(NOT-BELOWOF11-ROBOT)
(BELOWOF11-ROBOT)
(NOT-BELOWOF10-ROBOT)
(not (NOT-BELOWOF16-ROBOT))
(not (BELOWOF10-ROBOT))
)
)
(when
(and
(ABOVEOF15-ROBOT)
)
(and
(ABOVEOF14-ROBOT)
(ABOVEOF13-ROBOT)
(ABOVEOF12-ROBOT)
(ABOVEOF11-ROBOT)
(ABOVEOF10-ROBOT)
(ABOVEOF9-ROBOT)
(ABOVEOF8-ROBOT)
(ABOVEOF7-ROBOT)
(ABOVEOF6-ROBOT)
(ABOVEOF5-ROBOT)
(ABOVEOF4-ROBOT)
(ABOVEOF3-ROBOT)
(ABOVEOF2-ROBOT)
(not (NOT-ABOVEOF15-ROBOT))
(not (NOT-ABOVEOF14-ROBOT))
(not (NOT-ABOVEOF13-ROBOT))
(not (NOT-ABOVEOF12-ROBOT))
(not (NOT-ABOVEOF11-ROBOT))
(not (NOT-ABOVEOF10-ROBOT))
(not (NOT-ABOVEOF9-ROBOT))
(not (NOT-ABOVEOF8-ROBOT))
(not (NOT-ABOVEOF7-ROBOT))
(not (NOT-ABOVEOF6-ROBOT))
(not (NOT-ABOVEOF5-ROBOT))
(not (NOT-ABOVEOF4-ROBOT))
(not (NOT-ABOVEOF3-ROBOT))
(not (NOT-ABOVEOF2-ROBOT))
)
)
(when
(and
(ABOVEOF14-ROBOT)
)
(and
(ABOVEOF15-ROBOT)
(ABOVEOF13-ROBOT)
(ABOVEOF12-ROBOT)
(ABOVEOF11-ROBOT)
(ABOVEOF10-ROBOT)
(ABOVEOF9-ROBOT)
(ABOVEOF8-ROBOT)
(ABOVEOF7-ROBOT)
(ABOVEOF6-ROBOT)
(ABOVEOF5-ROBOT)
(ABOVEOF4-ROBOT)
(ABOVEOF3-ROBOT)
(ABOVEOF2-ROBOT)
(not (NOT-ABOVEOF15-ROBOT))
(not (NOT-ABOVEOF14-ROBOT))
(not (NOT-ABOVEOF13-ROBOT))
(not (NOT-ABOVEOF12-ROBOT))
(not (NOT-ABOVEOF11-ROBOT))
(not (NOT-ABOVEOF10-ROBOT))
(not (NOT-ABOVEOF9-ROBOT))
(not (NOT-ABOVEOF8-ROBOT))
(not (NOT-ABOVEOF7-ROBOT))
(not (NOT-ABOVEOF6-ROBOT))
(not (NOT-ABOVEOF5-ROBOT))
(not (NOT-ABOVEOF4-ROBOT))
(not (NOT-ABOVEOF3-ROBOT))
(not (NOT-ABOVEOF2-ROBOT))
)
)
(when
(and
(ABOVEOF13-ROBOT)
)
(and
(ABOVEOF14-ROBOT)
(ABOVEOF12-ROBOT)
(ABOVEOF11-ROBOT)
(ABOVEOF10-ROBOT)
(ABOVEOF9-ROBOT)
(ABOVEOF8-ROBOT)
(ABOVEOF7-ROBOT)
(ABOVEOF6-ROBOT)
(ABOVEOF5-ROBOT)
(ABOVEOF4-ROBOT)
(ABOVEOF3-ROBOT)
(ABOVEOF2-ROBOT)
(not (NOT-ABOVEOF14-ROBOT))
(not (NOT-ABOVEOF13-ROBOT))
(not (NOT-ABOVEOF12-ROBOT))
(not (NOT-ABOVEOF11-ROBOT))
(not (NOT-ABOVEOF10-ROBOT))
(not (NOT-ABOVEOF9-ROBOT))
(not (NOT-ABOVEOF8-ROBOT))
(not (NOT-ABOVEOF7-ROBOT))
(not (NOT-ABOVEOF6-ROBOT))
(not (NOT-ABOVEOF5-ROBOT))
(not (NOT-ABOVEOF4-ROBOT))
(not (NOT-ABOVEOF3-ROBOT))
(not (NOT-ABOVEOF2-ROBOT))
)
)
(when
(and
(ABOVEOF12-ROBOT)
)
(and
(ABOVEOF13-ROBOT)
(ABOVEOF11-ROBOT)
(ABOVEOF10-ROBOT)
(ABOVEOF9-ROBOT)
(ABOVEOF8-ROBOT)
(ABOVEOF7-ROBOT)
(ABOVEOF6-ROBOT)
(ABOVEOF5-ROBOT)
(ABOVEOF4-ROBOT)
(ABOVEOF3-ROBOT)
(ABOVEOF2-ROBOT)
(not (NOT-ABOVEOF13-ROBOT))
(not (NOT-ABOVEOF12-ROBOT))
(not (NOT-ABOVEOF11-ROBOT))
(not (NOT-ABOVEOF10-ROBOT))
(not (NOT-ABOVEOF9-ROBOT))
(not (NOT-ABOVEOF8-ROBOT))
(not (NOT-ABOVEOF7-ROBOT))
(not (NOT-ABOVEOF6-ROBOT))
(not (NOT-ABOVEOF5-ROBOT))
(not (NOT-ABOVEOF4-ROBOT))
(not (NOT-ABOVEOF3-ROBOT))
(not (NOT-ABOVEOF2-ROBOT))
)
)
(when
(and
(ABOVEOF11-ROBOT)
)
(and
(ABOVEOF12-ROBOT)
(ABOVEOF10-ROBOT)
(ABOVEOF9-ROBOT)
(ABOVEOF8-ROBOT)
(ABOVEOF7-ROBOT)
(ABOVEOF6-ROBOT)
(ABOVEOF5-ROBOT)
(ABOVEOF4-ROBOT)
(ABOVEOF3-ROBOT)
(ABOVEOF2-ROBOT)
(not (NOT-ABOVEOF12-ROBOT))
(not (NOT-ABOVEOF11-ROBOT))
(not (NOT-ABOVEOF10-ROBOT))
(not (NOT-ABOVEOF9-ROBOT))
(not (NOT-ABOVEOF8-ROBOT))
(not (NOT-ABOVEOF7-ROBOT))
(not (NOT-ABOVEOF6-ROBOT))
(not (NOT-ABOVEOF5-ROBOT))
(not (NOT-ABOVEOF4-ROBOT))
(not (NOT-ABOVEOF3-ROBOT))
(not (NOT-ABOVEOF2-ROBOT))
)
)
(when
(and
(ABOVEOF10-ROBOT)
)
(and
(ABOVEOF11-ROBOT)
(ABOVEOF9-ROBOT)
(ABOVEOF8-ROBOT)
(ABOVEOF7-ROBOT)
(ABOVEOF6-ROBOT)
(ABOVEOF5-ROBOT)
(ABOVEOF4-ROBOT)
(ABOVEOF3-ROBOT)
(ABOVEOF2-ROBOT)
(not (NOT-ABOVEOF11-ROBOT))
(not (NOT-ABOVEOF10-ROBOT))
(not (NOT-ABOVEOF9-ROBOT))
(not (NOT-ABOVEOF8-ROBOT))
(not (NOT-ABOVEOF7-ROBOT))
(not (NOT-ABOVEOF6-ROBOT))
(not (NOT-ABOVEOF5-ROBOT))
(not (NOT-ABOVEOF4-ROBOT))
(not (NOT-ABOVEOF3-ROBOT))
(not (NOT-ABOVEOF2-ROBOT))
)
)
(when
(and
(ABOVEOF9-ROBOT)
)
(and
(ABOVEOF10-ROBOT)
(ABOVEOF8-ROBOT)
(ABOVEOF7-ROBOT)
(ABOVEOF6-ROBOT)
(ABOVEOF5-ROBOT)
(ABOVEOF4-ROBOT)
(ABOVEOF3-ROBOT)
(ABOVEOF2-ROBOT)
(not (NOT-ABOVEOF10-ROBOT))
(not (NOT-ABOVEOF9-ROBOT))
(not (NOT-ABOVEOF8-ROBOT))
(not (NOT-ABOVEOF7-ROBOT))
(not (NOT-ABOVEOF6-ROBOT))
(not (NOT-ABOVEOF5-ROBOT))
(not (NOT-ABOVEOF4-ROBOT))
(not (NOT-ABOVEOF3-ROBOT))
(not (NOT-ABOVEOF2-ROBOT))
)
)
(when
(and
(ABOVEOF8-ROBOT)
)
(and
(ABOVEOF9-ROBOT)
(ABOVEOF7-ROBOT)
(ABOVEOF6-ROBOT)
(ABOVEOF5-ROBOT)
(ABOVEOF4-ROBOT)
(ABOVEOF3-ROBOT)
(ABOVEOF2-ROBOT)
(not (NOT-ABOVEOF9-ROBOT))
(not (NOT-ABOVEOF8-ROBOT))
(not (NOT-ABOVEOF7-ROBOT))
(not (NOT-ABOVEOF6-ROBOT))
(not (NOT-ABOVEOF5-ROBOT))
(not (NOT-ABOVEOF4-ROBOT))
(not (NOT-ABOVEOF3-ROBOT))
(not (NOT-ABOVEOF2-ROBOT))
)
)
(when
(and
(ABOVEOF7-ROBOT)
)
(and
(ABOVEOF8-ROBOT)
(ABOVEOF6-ROBOT)
(ABOVEOF5-ROBOT)
(ABOVEOF4-ROBOT)
(ABOVEOF3-ROBOT)
(ABOVEOF2-ROBOT)
(not (NOT-ABOVEOF8-ROBOT))
(not (NOT-ABOVEOF7-ROBOT))
(not (NOT-ABOVEOF6-ROBOT))
(not (NOT-ABOVEOF5-ROBOT))
(not (NOT-ABOVEOF4-ROBOT))
(not (NOT-ABOVEOF3-ROBOT))
(not (NOT-ABOVEOF2-ROBOT))
)
)
(when
(and
(ABOVEOF6-ROBOT)
)
(and
(ABOVEOF7-ROBOT)
(ABOVEOF5-ROBOT)
(ABOVEOF4-ROBOT)
(ABOVEOF3-ROBOT)
(ABOVEOF2-ROBOT)
(not (NOT-ABOVEOF7-ROBOT))
(not (NOT-ABOVEOF6-ROBOT))
(not (NOT-ABOVEOF5-ROBOT))
(not (NOT-ABOVEOF4-ROBOT))
(not (NOT-ABOVEOF3-ROBOT))
(not (NOT-ABOVEOF2-ROBOT))
)
)
(when
(and
(ABOVEOF5-ROBOT)
)
(and
(ABOVEOF6-ROBOT)
(ABOVEOF4-ROBOT)
(ABOVEOF3-ROBOT)
(ABOVEOF2-ROBOT)
(not (NOT-ABOVEOF6-ROBOT))
(not (NOT-ABOVEOF5-ROBOT))
(not (NOT-ABOVEOF4-ROBOT))
(not (NOT-ABOVEOF3-ROBOT))
(not (NOT-ABOVEOF2-ROBOT))
)
)
(when
(and
(ABOVEOF4-ROBOT)
)
(and
(ABOVEOF5-ROBOT)
(ABOVEOF3-ROBOT)
(ABOVEOF2-ROBOT)
(not (NOT-ABOVEOF5-ROBOT))
(not (NOT-ABOVEOF4-ROBOT))
(not (NOT-ABOVEOF3-ROBOT))
(not (NOT-ABOVEOF2-ROBOT))
)
)
(when
(and
(ABOVEOF3-ROBOT)
)
(and
(ABOVEOF4-ROBOT)
(ABOVEOF2-ROBOT)
(not (NOT-ABOVEOF4-ROBOT))
(not (NOT-ABOVEOF3-ROBOT))
(not (NOT-ABOVEOF2-ROBOT))
)
)
(when
(and
(ABOVEOF2-ROBOT)
)
(and
(ABOVEOF3-ROBOT)
(not (NOT-ABOVEOF3-ROBOT))
(not (NOT-ABOVEOF2-ROBOT))
)
)
(when
(and
(ABOVEOF1-ROBOT)
)
(and
(ABOVEOF2-ROBOT)
(not (NOT-ABOVEOF2-ROBOT))
)
)
(ABOVEOF1-ROBOT)
(CHECKCONSISTENCY-)
(not (NOT-ABOVEOF1-ROBOT))
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action MOVEDOWN-ROBOT
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
)
:effect
(and
(when
(and
(Flag390-)
)
(and
(ROW14-ROBOT)
(not (NOT-ROW14-ROBOT))
)
)
(when
(and
(Flag388-)
)
(and
(ROW13-ROBOT)
(not (NOT-ROW13-ROBOT))
)
)
(when
(and
(Flag385-)
)
(and
(ROW12-ROBOT)
(not (NOT-ROW12-ROBOT))
)
)
(when
(and
(Flag380-)
)
(and
(ROW11-ROBOT)
(not (NOT-ROW11-ROBOT))
)
)
(when
(and
(Flag374-)
)
(and
(ROW10-ROBOT)
(not (NOT-ROW10-ROBOT))
)
)
(when
(and
(Flag368-)
)
(and
(ROW9-ROBOT)
(not (NOT-ROW9-ROBOT))
)
)
(when
(and
(Flag361-)
)
(and
(ROW8-ROBOT)
(not (NOT-ROW8-ROBOT))
)
)
(when
(and
(Flag352-)
)
(and
(ROW7-ROBOT)
(not (NOT-ROW7-ROBOT))
)
)
(when
(and
(Flag341-)
)
(and
(ROW6-ROBOT)
(not (NOT-ROW6-ROBOT))
)
)
(when
(and
(Flag330-)
)
(and
(ROW5-ROBOT)
(not (NOT-ROW5-ROBOT))
)
)
(when
(and
(Flag317-)
)
(and
(ROW4-ROBOT)
(not (NOT-ROW4-ROBOT))
)
)
(when
(and
(Flag303-)
)
(and
(ROW3-ROBOT)
(not (NOT-ROW3-ROBOT))
)
)
(when
(and
(Flag290-)
)
(and
(ROW2-ROBOT)
(not (NOT-ROW2-ROBOT))
)
)
(when
(and
(Flag274-)
)
(and
(ROW1-ROBOT)
(not (NOT-ROW1-ROBOT))
)
)
(when
(and
(Flag258-)
)
(and
(ROW0-ROBOT)
(not (NOT-ROW0-ROBOT))
)
)
(when
(and
(ROW15-ROBOT)
)
(and
(ROW14-ROBOT)
(NOT-ROW15-ROBOT)
(not (NOT-ROW14-ROBOT))
(not (ROW15-ROBOT))
)
)
(when
(and
(ROW14-ROBOT)
)
(and
(ROW13-ROBOT)
(NOT-ROW14-ROBOT)
(not (NOT-ROW13-ROBOT))
(not (ROW14-ROBOT))
)
)
(when
(and
(ROW13-ROBOT)
)
(and
(ROW12-ROBOT)
(NOT-ROW13-ROBOT)
(not (NOT-ROW12-ROBOT))
(not (ROW13-ROBOT))
)
)
(when
(and
(ROW12-ROBOT)
)
(and
(ROW11-ROBOT)
(NOT-ROW12-ROBOT)
(not (NOT-ROW11-ROBOT))
(not (ROW12-ROBOT))
)
)
(when
(and
(ROW11-ROBOT)
)
(and
(ROW10-ROBOT)
(NOT-ROW11-ROBOT)
(not (NOT-ROW10-ROBOT))
(not (ROW11-ROBOT))
)
)
(when
(and
(ROW10-ROBOT)
)
(and
(ROW9-ROBOT)
(NOT-ROW10-ROBOT)
(not (NOT-ROW9-ROBOT))
(not (ROW10-ROBOT))
)
)
(when
(and
(ROW9-ROBOT)
)
(and
(ROW8-ROBOT)
(NOT-ROW9-ROBOT)
(not (NOT-ROW8-ROBOT))
(not (ROW9-ROBOT))
)
)
(when
(and
(ROW8-ROBOT)
)
(and
(ROW7-ROBOT)
(NOT-ROW8-ROBOT)
(not (NOT-ROW7-ROBOT))
(not (ROW8-ROBOT))
)
)
(when
(and
(ROW7-ROBOT)
)
(and
(ROW6-ROBOT)
(NOT-ROW7-ROBOT)
(not (NOT-ROW6-ROBOT))
(not (ROW7-ROBOT))
)
)
(when
(and
(ROW6-ROBOT)
)
(and
(ROW5-ROBOT)
(NOT-ROW6-ROBOT)
(not (NOT-ROW5-ROBOT))
(not (ROW6-ROBOT))
)
)
(when
(and
(ROW5-ROBOT)
)
(and
(ROW4-ROBOT)
(NOT-ROW5-ROBOT)
(not (NOT-ROW4-ROBOT))
(not (ROW5-ROBOT))
)
)
(when
(and
(ROW4-ROBOT)
)
(and
(ROW3-ROBOT)
(NOT-ROW4-ROBOT)
(not (NOT-ROW3-ROBOT))
(not (ROW4-ROBOT))
)
)
(when
(and
(ROW3-ROBOT)
)
(and
(ROW2-ROBOT)
(NOT-ROW3-ROBOT)
(not (NOT-ROW2-ROBOT))
(not (ROW3-ROBOT))
)
)
(when
(and
(ROW2-ROBOT)
)
(and
(ROW1-ROBOT)
(NOT-ROW2-ROBOT)
(not (NOT-ROW1-ROBOT))
(not (ROW2-ROBOT))
)
)
(when
(and
(ROW1-ROBOT)
)
(and
(ROW0-ROBOT)
(NOT-ROW1-ROBOT)
(not (NOT-ROW0-ROBOT))
(not (ROW1-ROBOT))
)
)
(when
(and
(ABOVEOF15-ROBOT)
)
(and
(ABOVEOF14-ROBOT)
(NOT-ABOVEOF15-ROBOT)
(not (NOT-ABOVEOF14-ROBOT))
(not (ABOVEOF15-ROBOT))
)
)
(when
(and
(Flag227-)
)
(and
(ABOVEOF13-ROBOT)
(NOT-ABOVEOF14-ROBOT)
(not (NOT-ABOVEOF13-ROBOT))
(not (ABOVEOF14-ROBOT))
)
)
(when
(and
(Flag226-)
)
(and
(ABOVEOF12-ROBOT)
(NOT-ABOVEOF13-ROBOT)
(not (NOT-ABOVEOF12-ROBOT))
(not (ABOVEOF13-ROBOT))
)
)
(when
(and
(Flag225-)
)
(and
(ABOVEOF11-ROBOT)
(NOT-ABOVEOF12-ROBOT)
(not (NOT-ABOVEOF11-ROBOT))
(not (ABOVEOF12-ROBOT))
)
)
(when
(and
(Flag224-)
)
(and
(ABOVEOF10-ROBOT)
(NOT-ABOVEOF11-ROBOT)
(not (NOT-ABOVEOF10-ROBOT))
(not (ABOVEOF11-ROBOT))
)
)
(when
(and
(Flag223-)
)
(and
(ABOVEOF9-ROBOT)
(NOT-ABOVEOF10-ROBOT)
(not (NOT-ABOVEOF9-ROBOT))
(not (ABOVEOF10-ROBOT))
)
)
(when
(and
(Flag222-)
)
(and
(ABOVEOF8-ROBOT)
(NOT-ABOVEOF9-ROBOT)
(not (NOT-ABOVEOF8-ROBOT))
(not (ABOVEOF9-ROBOT))
)
)
(when
(and
(Flag221-)
)
(and
(ABOVEOF7-ROBOT)
(NOT-ABOVEOF8-ROBOT)
(not (NOT-ABOVEOF7-ROBOT))
(not (ABOVEOF8-ROBOT))
)
)
(when
(and
(Flag220-)
)
(and
(ABOVEOF6-ROBOT)
(NOT-ABOVEOF7-ROBOT)
(not (NOT-ABOVEOF6-ROBOT))
(not (ABOVEOF7-ROBOT))
)
)
(when
(and
(Flag219-)
)
(and
(ABOVEOF5-ROBOT)
(NOT-ABOVEOF6-ROBOT)
(not (NOT-ABOVEOF5-ROBOT))
(not (ABOVEOF6-ROBOT))
)
)
(when
(and
(Flag218-)
)
(and
(ABOVEOF4-ROBOT)
(NOT-ABOVEOF5-ROBOT)
(not (NOT-ABOVEOF4-ROBOT))
(not (ABOVEOF5-ROBOT))
)
)
(when
(and
(Flag217-)
)
(and
(ABOVEOF3-ROBOT)
(NOT-ABOVEOF4-ROBOT)
(not (NOT-ABOVEOF3-ROBOT))
(not (ABOVEOF4-ROBOT))
)
)
(when
(and
(Flag216-)
)
(and
(ABOVEOF2-ROBOT)
(NOT-ABOVEOF3-ROBOT)
(not (NOT-ABOVEOF2-ROBOT))
(not (ABOVEOF3-ROBOT))
)
)
(when
(and
(Flag215-)
)
(and
(ABOVEOF1-ROBOT)
(NOT-ABOVEOF2-ROBOT)
(not (NOT-ABOVEOF1-ROBOT))
(not (ABOVEOF2-ROBOT))
)
)
(when
(and
(Flag214-)
)
(and
(NOT-ABOVEOF1-ROBOT)
(not (ABOVEOF1-ROBOT))
)
)
(when
(and
(Flag213-)
)
(and
(BELOWOF15-ROBOT)
(not (NOT-BELOWOF15-ROBOT))
)
)
(when
(and
(Flag212-)
)
(and
(BELOWOF14-ROBOT)
(not (NOT-BELOWOF14-ROBOT))
)
)
(when
(and
(Flag211-)
)
(and
(BELOWOF13-ROBOT)
(not (NOT-BELOWOF13-ROBOT))
)
)
(when
(and
(Flag210-)
)
(and
(BELOWOF12-ROBOT)
(not (NOT-BELOWOF12-ROBOT))
)
)
(when
(and
(Flag209-)
)
(and
(BELOWOF11-ROBOT)
(not (NOT-BELOWOF11-ROBOT))
)
)
(when
(and
(Flag208-)
)
(and
(BELOWOF10-ROBOT)
(not (NOT-BELOWOF10-ROBOT))
)
)
(when
(and
(Flag207-)
)
(and
(BELOWOF9-ROBOT)
(not (NOT-BELOWOF9-ROBOT))
)
)
(when
(and
(Flag206-)
)
(and
(BELOWOF8-ROBOT)
(not (NOT-BELOWOF8-ROBOT))
)
)
(when
(and
(Flag205-)
)
(and
(BELOWOF7-ROBOT)
(not (NOT-BELOWOF7-ROBOT))
)
)
(when
(and
(Flag204-)
)
(and
(BELOWOF6-ROBOT)
(not (NOT-BELOWOF6-ROBOT))
)
)
(when
(and
(Flag203-)
)
(and
(BELOWOF5-ROBOT)
(not (NOT-BELOWOF5-ROBOT))
)
)
(when
(and
(Flag202-)
)
(and
(BELOWOF4-ROBOT)
(not (NOT-BELOWOF4-ROBOT))
)
)
(when
(and
(Flag201-)
)
(and
(BELOWOF3-ROBOT)
(not (NOT-BELOWOF3-ROBOT))
)
)
(when
(and
(Flag200-)
)
(and
(BELOWOF2-ROBOT)
(not (NOT-BELOWOF2-ROBOT))
)
)
(when
(and
(Flag199-)
)
(and
(BELOWOF1-ROBOT)
(not (NOT-BELOWOF1-ROBOT))
)
)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:derived (Flag2-)(and (NOT-CHECKCONSISTENCY-)(NOT-ERROR-)))

(:derived (Flag3-)(and (RIGHTOF2-ROBOT)))

(:derived (Flag3-)(and (LEFTOF15-ROBOT)))

(:derived (Flag5-)(and (RIGHTOF2-ROBOT)))

(:derived (Flag6-)(and (RIGHTOF2-ROBOT)))

(:derived (Flag7-)(and (RIGHTOF2-ROBOT)))

(:derived (Flag33-)(and (LEFTOF15-ROBOT)))

(:derived (Flag195-)(and (LEFTOF15-ROBOT)))

(:derived (Flag212-)(and (BELOWOF15-ROBOT)))

(:derived (Flag213-)(and (BELOWOF15-ROBOT)))

)
