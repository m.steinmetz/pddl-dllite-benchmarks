(define (domain grounded-SIMPLE-ADL-ROBOT)
(:requirements
:strips
)
(:predicates
(Flag349-)
(Flag204-)
(Flag180-)
(Flag179-)
(Flag178-)
(Flag18-)
(Flag17-)
(Flag3-)
(Flag2-)
(ROW13-ROBOT)
(ROW12-ROBOT)
(ROW11-ROBOT)
(ROW10-ROBOT)
(ROW9-ROBOT)
(ROW8-ROBOT)
(ROW7-ROBOT)
(ROW6-ROBOT)
(ROW5-ROBOT)
(ROW4-ROBOT)
(ROW3-ROBOT)
(ROW2-ROBOT)
(ROW1-ROBOT)
(ROW0-ROBOT)
(ABOVEOF13-ROBOT)
(ABOVEOF12-ROBOT)
(ABOVEOF11-ROBOT)
(ABOVEOF10-ROBOT)
(ABOVEOF9-ROBOT)
(ABOVEOF8-ROBOT)
(ABOVEOF7-ROBOT)
(ABOVEOF6-ROBOT)
(ABOVEOF5-ROBOT)
(ABOVEOF4-ROBOT)
(ABOVEOF3-ROBOT)
(ABOVEOF2-ROBOT)
(ABOVEOF1-ROBOT)
(BELOWOF13-ROBOT)
(BELOWOF12-ROBOT)
(BELOWOF11-ROBOT)
(BELOWOF10-ROBOT)
(BELOWOF9-ROBOT)
(BELOWOF8-ROBOT)
(BELOWOF7-ROBOT)
(BELOWOF6-ROBOT)
(BELOWOF5-ROBOT)
(BELOWOF4-ROBOT)
(BELOWOF3-ROBOT)
(BELOWOF2-ROBOT)
(BELOWOF1-ROBOT)
(CHECKCONSISTENCY-)
(ROW14-ROBOT)
(BELOWOF15-ROBOT)
(NOT-BELOWOF14-ROBOT)
(ABOVEOF14-ROBOT)
(Flag352-)
(Flag176-)
(Flag174-)
(Flag173-)
(Flag172-)
(Flag170-)
(Flag169-)
(Flag168-)
(Flag167-)
(Flag165-)
(Flag164-)
(Flag163-)
(Flag162-)
(Flag160-)
(Flag159-)
(Flag158-)
(Flag157-)
(Flag156-)
(Flag154-)
(Flag153-)
(Flag152-)
(Flag151-)
(Flag150-)
(Flag149-)
(Flag148-)
(Flag146-)
(Flag145-)
(Flag144-)
(Flag143-)
(Flag142-)
(Flag141-)
(Flag140-)
(Flag139-)
(Flag138-)
(Flag136-)
(Flag135-)
(Flag134-)
(Flag133-)
(Flag132-)
(Flag131-)
(Flag130-)
(Flag129-)
(Flag128-)
(Flag126-)
(Flag125-)
(Flag124-)
(Flag123-)
(Flag122-)
(Flag121-)
(Flag120-)
(Flag119-)
(Flag118-)
(Flag117-)
(Flag116-)
(Flag114-)
(Flag113-)
(Flag112-)
(Flag111-)
(Flag110-)
(Flag109-)
(Flag108-)
(Flag107-)
(Flag106-)
(Flag105-)
(Flag104-)
(Flag103-)
(Flag101-)
(Flag100-)
(Flag99-)
(Flag98-)
(Flag97-)
(Flag96-)
(Flag95-)
(Flag94-)
(Flag93-)
(Flag92-)
(Flag91-)
(Flag89-)
(Flag88-)
(Flag87-)
(Flag86-)
(Flag85-)
(Flag84-)
(Flag83-)
(Flag82-)
(Flag81-)
(Flag80-)
(Flag79-)
(Flag78-)
(Flag77-)
(Flag76-)
(Flag74-)
(Flag73-)
(Flag72-)
(Flag71-)
(Flag70-)
(Flag69-)
(Flag68-)
(Flag67-)
(Flag66-)
(Flag65-)
(Flag64-)
(Flag63-)
(Flag62-)
(Flag61-)
(Flag59-)
(Flag58-)
(Flag57-)
(Flag56-)
(Flag55-)
(Flag54-)
(Flag53-)
(Flag52-)
(Flag51-)
(Flag50-)
(Flag49-)
(Flag48-)
(Flag47-)
(Flag46-)
(Flag45-)
(Flag44-)
(Flag43-)
(Flag42-)
(Flag41-)
(Flag40-)
(Flag39-)
(Flag38-)
(Flag37-)
(Flag36-)
(Flag35-)
(Flag34-)
(Flag33-)
(Flag32-)
(Flag31-)
(Flag30-)
(Flag29-)
(Flag28-)
(Flag27-)
(Flag26-)
(Flag25-)
(Flag24-)
(Flag23-)
(Flag22-)
(Flag21-)
(Flag20-)
(Flag19-)
(Flag16-)
(Flag15-)
(Flag14-)
(Flag13-)
(Flag12-)
(Flag11-)
(Flag10-)
(Flag9-)
(Flag8-)
(Flag7-)
(Flag6-)
(Flag5-)
(Flag4-)
(ERROR-)
(COLUMN14-ROBOT)
(COLUMN13-ROBOT)
(COLUMN12-ROBOT)
(COLUMN11-ROBOT)
(COLUMN10-ROBOT)
(COLUMN9-ROBOT)
(COLUMN8-ROBOT)
(COLUMN7-ROBOT)
(COLUMN6-ROBOT)
(COLUMN5-ROBOT)
(COLUMN4-ROBOT)
(COLUMN3-ROBOT)
(COLUMN2-ROBOT)
(COLUMN1-ROBOT)
(LEFTOF15-ROBOT)
(NOT-LEFTOF14-ROBOT)
(LEFTOF13-ROBOT)
(LEFTOF12-ROBOT)
(LEFTOF11-ROBOT)
(LEFTOF10-ROBOT)
(LEFTOF9-ROBOT)
(LEFTOF8-ROBOT)
(LEFTOF7-ROBOT)
(LEFTOF6-ROBOT)
(LEFTOF5-ROBOT)
(LEFTOF4-ROBOT)
(LEFTOF3-ROBOT)
(LEFTOF2-ROBOT)
(RIGHTOF14-ROBOT)
(RIGHTOF13-ROBOT)
(RIGHTOF12-ROBOT)
(RIGHTOF11-ROBOT)
(RIGHTOF10-ROBOT)
(RIGHTOF9-ROBOT)
(RIGHTOF8-ROBOT)
(RIGHTOF7-ROBOT)
(RIGHTOF6-ROBOT)
(RIGHTOF5-ROBOT)
(RIGHTOF4-ROBOT)
(RIGHTOF3-ROBOT)
(RIGHTOF1-ROBOT)
(COLUMN0-ROBOT)
(NOT-RIGHTOF2-ROBOT)
(RIGHTOF0-ROBOT)
(LEFTOF1-ROBOT)
(Flag350-)
(Flag347-)
(Flag346-)
(Flag344-)
(Flag343-)
(Flag342-)
(Flag341-)
(Flag339-)
(Flag338-)
(Flag337-)
(Flag336-)
(Flag334-)
(Flag333-)
(Flag332-)
(Flag331-)
(Flag330-)
(Flag329-)
(Flag327-)
(Flag326-)
(Flag325-)
(Flag324-)
(Flag323-)
(Flag322-)
(Flag320-)
(Flag319-)
(Flag318-)
(Flag317-)
(Flag316-)
(Flag315-)
(Flag314-)
(Flag313-)
(Flag311-)
(Flag310-)
(Flag309-)
(Flag308-)
(Flag307-)
(Flag306-)
(Flag305-)
(Flag304-)
(Flag303-)
(Flag301-)
(Flag300-)
(Flag299-)
(Flag298-)
(Flag297-)
(Flag296-)
(Flag295-)
(Flag294-)
(Flag293-)
(Flag292-)
(Flag291-)
(Flag289-)
(Flag288-)
(Flag287-)
(Flag286-)
(Flag285-)
(Flag284-)
(Flag283-)
(Flag282-)
(Flag281-)
(Flag280-)
(Flag278-)
(Flag277-)
(Flag276-)
(Flag275-)
(Flag274-)
(Flag273-)
(Flag272-)
(Flag271-)
(Flag270-)
(Flag269-)
(Flag268-)
(Flag267-)
(Flag266-)
(Flag264-)
(Flag263-)
(Flag262-)
(Flag261-)
(Flag260-)
(Flag259-)
(Flag258-)
(Flag257-)
(Flag256-)
(Flag255-)
(Flag254-)
(Flag253-)
(Flag252-)
(Flag251-)
(Flag249-)
(Flag248-)
(Flag247-)
(Flag246-)
(Flag245-)
(Flag244-)
(Flag243-)
(Flag242-)
(Flag241-)
(Flag240-)
(Flag239-)
(Flag238-)
(Flag237-)
(Flag235-)
(Flag234-)
(Flag233-)
(Flag232-)
(Flag231-)
(Flag230-)
(Flag229-)
(Flag228-)
(Flag227-)
(Flag226-)
(Flag225-)
(Flag224-)
(Flag223-)
(Flag222-)
(Flag221-)
(Flag219-)
(Flag218-)
(Flag217-)
(Flag216-)
(Flag215-)
(Flag214-)
(Flag213-)
(Flag212-)
(Flag211-)
(Flag210-)
(Flag209-)
(Flag208-)
(Flag207-)
(Flag206-)
(Flag205-)
(Flag203-)
(Flag202-)
(Flag201-)
(Flag200-)
(Flag199-)
(Flag198-)
(Flag197-)
(Flag196-)
(Flag195-)
(Flag194-)
(Flag193-)
(Flag192-)
(Flag191-)
(Flag190-)
(Flag189-)
(Flag188-)
(Flag187-)
(Flag186-)
(Flag185-)
(Flag184-)
(Flag183-)
(Flag182-)
(Flag181-)
(Flag177-)
(Flag175-)
(Flag171-)
(Flag166-)
(Flag161-)
(Flag155-)
(Flag147-)
(Flag137-)
(Flag127-)
(Flag115-)
(Flag102-)
(Flag90-)
(Flag75-)
(Flag60-)
(Flag1-)
(Flag351-)
(Flag348-)
(Flag345-)
(Flag340-)
(Flag335-)
(Flag328-)
(Flag321-)
(Flag312-)
(Flag302-)
(Flag290-)
(Flag279-)
(Flag265-)
(Flag250-)
(Flag236-)
(Flag220-)
(NOT-COLUMN13-ROBOT)
(NOT-COLUMN12-ROBOT)
(NOT-COLUMN11-ROBOT)
(NOT-COLUMN10-ROBOT)
(NOT-COLUMN9-ROBOT)
(NOT-COLUMN8-ROBOT)
(NOT-COLUMN7-ROBOT)
(NOT-COLUMN6-ROBOT)
(NOT-COLUMN5-ROBOT)
(NOT-COLUMN4-ROBOT)
(NOT-COLUMN3-ROBOT)
(NOT-COLUMN2-ROBOT)
(NOT-COLUMN1-ROBOT)
(NOT-COLUMN0-ROBOT)
(NOT-RIGHTOF13-ROBOT)
(NOT-RIGHTOF12-ROBOT)
(NOT-RIGHTOF11-ROBOT)
(NOT-RIGHTOF10-ROBOT)
(NOT-RIGHTOF9-ROBOT)
(NOT-RIGHTOF8-ROBOT)
(NOT-RIGHTOF7-ROBOT)
(NOT-RIGHTOF6-ROBOT)
(NOT-RIGHTOF5-ROBOT)
(NOT-RIGHTOF4-ROBOT)
(NOT-RIGHTOF3-ROBOT)
(NOT-RIGHTOF1-ROBOT)
(RIGHTOF2-ROBOT)
(NOT-RIGHTOF0-ROBOT)
(NOT-LEFTOF13-ROBOT)
(NOT-LEFTOF12-ROBOT)
(NOT-LEFTOF11-ROBOT)
(NOT-LEFTOF10-ROBOT)
(NOT-LEFTOF9-ROBOT)
(NOT-LEFTOF8-ROBOT)
(NOT-LEFTOF7-ROBOT)
(NOT-LEFTOF6-ROBOT)
(NOT-LEFTOF5-ROBOT)
(NOT-LEFTOF4-ROBOT)
(NOT-LEFTOF3-ROBOT)
(NOT-LEFTOF2-ROBOT)
(NOT-LEFTOF1-ROBOT)
(NOT-CHECKCONSISTENCY-)
(NOT-COLUMN14-ROBOT)
(NOT-LEFTOF15-ROBOT)
(LEFTOF14-ROBOT)
(NOT-RIGHTOF14-ROBOT)
(NOT-ERROR-)
(NOT-ROW14-ROBOT)
(NOT-ROW13-ROBOT)
(NOT-ROW12-ROBOT)
(NOT-ROW11-ROBOT)
(NOT-ROW10-ROBOT)
(NOT-ROW9-ROBOT)
(NOT-ROW8-ROBOT)
(NOT-ROW7-ROBOT)
(NOT-ROW6-ROBOT)
(NOT-ROW5-ROBOT)
(NOT-ROW4-ROBOT)
(NOT-ROW3-ROBOT)
(NOT-ROW2-ROBOT)
(NOT-ROW1-ROBOT)
(NOT-BELOWOF15-ROBOT)
(BELOWOF14-ROBOT)
(NOT-BELOWOF13-ROBOT)
(NOT-BELOWOF12-ROBOT)
(NOT-BELOWOF11-ROBOT)
(NOT-BELOWOF10-ROBOT)
(NOT-BELOWOF9-ROBOT)
(NOT-BELOWOF8-ROBOT)
(NOT-BELOWOF7-ROBOT)
(NOT-BELOWOF6-ROBOT)
(NOT-BELOWOF5-ROBOT)
(NOT-BELOWOF4-ROBOT)
(NOT-BELOWOF3-ROBOT)
(NOT-BELOWOF2-ROBOT)
(NOT-ABOVEOF14-ROBOT)
(NOT-ABOVEOF13-ROBOT)
(NOT-ABOVEOF12-ROBOT)
(NOT-ABOVEOF11-ROBOT)
(NOT-ABOVEOF10-ROBOT)
(NOT-ABOVEOF9-ROBOT)
(NOT-ABOVEOF8-ROBOT)
(NOT-ABOVEOF7-ROBOT)
(NOT-ABOVEOF6-ROBOT)
(NOT-ABOVEOF5-ROBOT)
(NOT-ABOVEOF4-ROBOT)
(NOT-ABOVEOF3-ROBOT)
(NOT-ABOVEOF2-ROBOT)
(NOT-ABOVEOF1-ROBOT)
(NOT-ROW0-ROBOT)
(NOT-BELOWOF1-ROBOT)
)
(:derived (Flag220-)(and (Flag205-)))

(:derived (Flag220-)(and (Flag206-)))

(:derived (Flag220-)(and (Flag207-)))

(:derived (Flag220-)(and (Flag208-)))

(:derived (Flag220-)(and (Flag209-)))

(:derived (Flag220-)(and (Flag210-)))

(:derived (Flag220-)(and (Flag211-)))

(:derived (Flag220-)(and (Flag212-)))

(:derived (Flag220-)(and (Flag213-)))

(:derived (Flag220-)(and (Flag214-)))

(:derived (Flag220-)(and (Flag215-)))

(:derived (Flag220-)(and (Flag216-)))

(:derived (Flag220-)(and (Flag217-)))

(:derived (Flag220-)(and (Flag218-)))

(:derived (Flag220-)(and (Flag219-)))

(:derived (Flag236-)(and (Flag221-)))

(:derived (Flag236-)(and (Flag222-)))

(:derived (Flag236-)(and (Flag208-)))

(:derived (Flag236-)(and (Flag211-)))

(:derived (Flag236-)(and (Flag219-)))

(:derived (Flag236-)(and (Flag223-)))

(:derived (Flag236-)(and (Flag224-)))

(:derived (Flag236-)(and (Flag210-)))

(:derived (Flag236-)(and (Flag225-)))

(:derived (Flag236-)(and (Flag226-)))

(:derived (Flag236-)(and (Flag215-)))

(:derived (Flag236-)(and (Flag227-)))

(:derived (Flag236-)(and (Flag228-)))

(:derived (Flag236-)(and (Flag205-)))

(:derived (Flag236-)(and (Flag229-)))

(:derived (Flag236-)(and (Flag206-)))

(:derived (Flag236-)(and (Flag207-)))

(:derived (Flag236-)(and (Flag230-)))

(:derived (Flag236-)(and (Flag213-)))

(:derived (Flag236-)(and (Flag216-)))

(:derived (Flag236-)(and (Flag209-)))

(:derived (Flag236-)(and (Flag231-)))

(:derived (Flag236-)(and (Flag232-)))

(:derived (Flag236-)(and (Flag233-)))

(:derived (Flag236-)(and (Flag214-)))

(:derived (Flag236-)(and (Flag217-)))

(:derived (Flag236-)(and (Flag218-)))

(:derived (Flag236-)(and (Flag234-)))

(:derived (Flag236-)(and (Flag235-)))

(:derived (Flag250-)(and (Flag221-)))

(:derived (Flag250-)(and (Flag222-)))

(:derived (Flag250-)(and (Flag237-)))

(:derived (Flag250-)(and (Flag208-)))

(:derived (Flag250-)(and (Flag211-)))

(:derived (Flag250-)(and (Flag238-)))

(:derived (Flag250-)(and (Flag239-)))

(:derived (Flag250-)(and (Flag219-)))

(:derived (Flag250-)(and (Flag224-)))

(:derived (Flag250-)(and (Flag207-)))

(:derived (Flag250-)(and (Flag226-)))

(:derived (Flag250-)(and (Flag240-)))

(:derived (Flag250-)(and (Flag215-)))

(:derived (Flag250-)(and (Flag227-)))

(:derived (Flag250-)(and (Flag213-)))

(:derived (Flag250-)(and (Flag241-)))

(:derived (Flag250-)(and (Flag228-)))

(:derived (Flag250-)(and (Flag205-)))

(:derived (Flag250-)(and (Flag229-)))

(:derived (Flag250-)(and (Flag206-)))

(:derived (Flag250-)(and (Flag242-)))

(:derived (Flag250-)(and (Flag243-)))

(:derived (Flag250-)(and (Flag225-)))

(:derived (Flag250-)(and (Flag230-)))

(:derived (Flag250-)(and (Flag244-)))

(:derived (Flag250-)(and (Flag216-)))

(:derived (Flag250-)(and (Flag245-)))

(:derived (Flag250-)(and (Flag246-)))

(:derived (Flag250-)(and (Flag247-)))

(:derived (Flag250-)(and (Flag209-)))

(:derived (Flag250-)(and (Flag231-)))

(:derived (Flag250-)(and (Flag232-)))

(:derived (Flag250-)(and (Flag233-)))

(:derived (Flag250-)(and (Flag214-)))

(:derived (Flag250-)(and (Flag217-)))

(:derived (Flag250-)(and (Flag218-)))

(:derived (Flag250-)(and (Flag234-)))

(:derived (Flag250-)(and (Flag248-)))

(:derived (Flag250-)(and (Flag235-)))

(:derived (Flag250-)(and (Flag249-)))

(:derived (Flag265-)(and (Flag221-)))

(:derived (Flag265-)(and (Flag222-)))

(:derived (Flag265-)(and (Flag237-)))

(:derived (Flag265-)(and (Flag208-)))

(:derived (Flag265-)(and (Flag211-)))

(:derived (Flag265-)(and (Flag238-)))

(:derived (Flag265-)(and (Flag251-)))

(:derived (Flag265-)(and (Flag239-)))

(:derived (Flag265-)(and (Flag219-)))

(:derived (Flag265-)(and (Flag252-)))

(:derived (Flag265-)(and (Flag253-)))

(:derived (Flag265-)(and (Flag224-)))

(:derived (Flag265-)(and (Flag243-)))

(:derived (Flag265-)(and (Flag207-)))

(:derived (Flag265-)(and (Flag226-)))

(:derived (Flag265-)(and (Flag240-)))

(:derived (Flag265-)(and (Flag215-)))

(:derived (Flag265-)(and (Flag227-)))

(:derived (Flag265-)(and (Flag213-)))

(:derived (Flag265-)(and (Flag241-)))

(:derived (Flag265-)(and (Flag228-)))

(:derived (Flag265-)(and (Flag205-)))

(:derived (Flag265-)(and (Flag254-)))

(:derived (Flag265-)(and (Flag229-)))

(:derived (Flag265-)(and (Flag206-)))

(:derived (Flag265-)(and (Flag242-)))

(:derived (Flag265-)(and (Flag255-)))

(:derived (Flag265-)(and (Flag225-)))

(:derived (Flag265-)(and (Flag230-)))

(:derived (Flag265-)(and (Flag256-)))

(:derived (Flag265-)(and (Flag244-)))

(:derived (Flag265-)(and (Flag218-)))

(:derived (Flag265-)(and (Flag216-)))

(:derived (Flag265-)(and (Flag246-)))

(:derived (Flag265-)(and (Flag257-)))

(:derived (Flag265-)(and (Flag258-)))

(:derived (Flag265-)(and (Flag247-)))

(:derived (Flag265-)(and (Flag259-)))

(:derived (Flag265-)(and (Flag209-)))

(:derived (Flag265-)(and (Flag231-)))

(:derived (Flag265-)(and (Flag260-)))

(:derived (Flag265-)(and (Flag261-)))

(:derived (Flag265-)(and (Flag232-)))

(:derived (Flag265-)(and (Flag262-)))

(:derived (Flag265-)(and (Flag217-)))

(:derived (Flag265-)(and (Flag263-)))

(:derived (Flag265-)(and (Flag264-)))

(:derived (Flag265-)(and (Flag234-)))

(:derived (Flag265-)(and (Flag248-)))

(:derived (Flag265-)(and (Flag235-)))

(:derived (Flag279-)(and (Flag266-)))

(:derived (Flag279-)(and (Flag221-)))

(:derived (Flag279-)(and (Flag222-)))

(:derived (Flag279-)(and (Flag237-)))

(:derived (Flag279-)(and (Flag208-)))

(:derived (Flag279-)(and (Flag238-)))

(:derived (Flag279-)(and (Flag218-)))

(:derived (Flag279-)(and (Flag251-)))

(:derived (Flag279-)(and (Flag267-)))

(:derived (Flag279-)(and (Flag239-)))

(:derived (Flag279-)(and (Flag219-)))

(:derived (Flag279-)(and (Flag216-)))

(:derived (Flag279-)(and (Flag268-)))

(:derived (Flag279-)(and (Flag252-)))

(:derived (Flag279-)(and (Flag269-)))

(:derived (Flag279-)(and (Flag253-)))

(:derived (Flag279-)(and (Flag224-)))

(:derived (Flag279-)(and (Flag243-)))

(:derived (Flag279-)(and (Flag207-)))

(:derived (Flag279-)(and (Flag226-)))

(:derived (Flag279-)(and (Flag240-)))

(:derived (Flag279-)(and (Flag215-)))

(:derived (Flag279-)(and (Flag254-)))

(:derived (Flag279-)(and (Flag270-)))

(:derived (Flag279-)(and (Flag271-)))

(:derived (Flag279-)(and (Flag272-)))

(:derived (Flag279-)(and (Flag205-)))

(:derived (Flag279-)(and (Flag273-)))

(:derived (Flag279-)(and (Flag229-)))

(:derived (Flag279-)(and (Flag206-)))

(:derived (Flag279-)(and (Flag242-)))

(:derived (Flag279-)(and (Flag274-)))

(:derived (Flag279-)(and (Flag225-)))

(:derived (Flag279-)(and (Flag230-)))

(:derived (Flag279-)(and (Flag256-)))

(:derived (Flag279-)(and (Flag275-)))

(:derived (Flag279-)(and (Flag276-)))

(:derived (Flag279-)(and (Flag213-)))

(:derived (Flag279-)(and (Flag241-)))

(:derived (Flag279-)(and (Flag228-)))

(:derived (Flag279-)(and (Flag246-)))

(:derived (Flag279-)(and (Flag257-)))

(:derived (Flag279-)(and (Flag258-)))

(:derived (Flag279-)(and (Flag247-)))

(:derived (Flag279-)(and (Flag259-)))

(:derived (Flag279-)(and (Flag209-)))

(:derived (Flag279-)(and (Flag231-)))

(:derived (Flag279-)(and (Flag260-)))

(:derived (Flag279-)(and (Flag261-)))

(:derived (Flag279-)(and (Flag232-)))

(:derived (Flag279-)(and (Flag262-)))

(:derived (Flag279-)(and (Flag277-)))

(:derived (Flag279-)(and (Flag244-)))

(:derived (Flag279-)(and (Flag217-)))

(:derived (Flag279-)(and (Flag278-)))

(:derived (Flag279-)(and (Flag264-)))

(:derived (Flag279-)(and (Flag234-)))

(:derived (Flag279-)(and (Flag248-)))

(:derived (Flag279-)(and (Flag235-)))

(:derived (Flag290-)(and (Flag266-)))

(:derived (Flag290-)(and (Flag221-)))

(:derived (Flag290-)(and (Flag222-)))

(:derived (Flag290-)(and (Flag267-)))

(:derived (Flag290-)(and (Flag208-)))

(:derived (Flag290-)(and (Flag238-)))

(:derived (Flag290-)(and (Flag251-)))

(:derived (Flag290-)(and (Flag231-)))

(:derived (Flag290-)(and (Flag248-)))

(:derived (Flag290-)(and (Flag219-)))

(:derived (Flag290-)(and (Flag269-)))

(:derived (Flag290-)(and (Flag237-)))

(:derived (Flag290-)(and (Flag253-)))

(:derived (Flag290-)(and (Flag224-)))

(:derived (Flag290-)(and (Flag243-)))

(:derived (Flag290-)(and (Flag226-)))

(:derived (Flag290-)(and (Flag225-)))

(:derived (Flag290-)(and (Flag262-)))

(:derived (Flag290-)(and (Flag280-)))

(:derived (Flag290-)(and (Flag240-)))

(:derived (Flag290-)(and (Flag215-)))

(:derived (Flag290-)(and (Flag254-)))

(:derived (Flag290-)(and (Flag281-)))

(:derived (Flag290-)(and (Flag241-)))

(:derived (Flag290-)(and (Flag282-)))

(:derived (Flag290-)(and (Flag271-)))

(:derived (Flag290-)(and (Flag272-)))

(:derived (Flag290-)(and (Flag273-)))

(:derived (Flag290-)(and (Flag229-)))

(:derived (Flag290-)(and (Flag206-)))

(:derived (Flag290-)(and (Flag242-)))

(:derived (Flag290-)(and (Flag274-)))

(:derived (Flag290-)(and (Flag207-)))

(:derived (Flag290-)(and (Flag230-)))

(:derived (Flag290-)(and (Flag256-)))

(:derived (Flag290-)(and (Flag275-)))

(:derived (Flag290-)(and (Flag276-)))

(:derived (Flag290-)(and (Flag228-)))

(:derived (Flag290-)(and (Flag244-)))

(:derived (Flag290-)(and (Flag283-)))

(:derived (Flag290-)(and (Flag218-)))

(:derived (Flag290-)(and (Flag216-)))

(:derived (Flag290-)(and (Flag246-)))

(:derived (Flag290-)(and (Flag257-)))

(:derived (Flag290-)(and (Flag258-)))

(:derived (Flag290-)(and (Flag284-)))

(:derived (Flag290-)(and (Flag247-)))

(:derived (Flag290-)(and (Flag285-)))

(:derived (Flag290-)(and (Flag286-)))

(:derived (Flag290-)(and (Flag259-)))

(:derived (Flag290-)(and (Flag209-)))

(:derived (Flag290-)(and (Flag287-)))

(:derived (Flag290-)(and (Flag260-)))

(:derived (Flag290-)(and (Flag261-)))

(:derived (Flag290-)(and (Flag232-)))

(:derived (Flag290-)(and (Flag288-)))

(:derived (Flag290-)(and (Flag277-)))

(:derived (Flag290-)(and (Flag217-)))

(:derived (Flag290-)(and (Flag278-)))

(:derived (Flag290-)(and (Flag264-)))

(:derived (Flag290-)(and (Flag234-)))

(:derived (Flag290-)(and (Flag268-)))

(:derived (Flag290-)(and (Flag289-)))

(:derived (Flag302-)(and (Flag266-)))

(:derived (Flag302-)(and (Flag221-)))

(:derived (Flag302-)(and (Flag222-)))

(:derived (Flag302-)(and (Flag267-)))

(:derived (Flag302-)(and (Flag208-)))

(:derived (Flag302-)(and (Flag291-)))

(:derived (Flag302-)(and (Flag260-)))

(:derived (Flag302-)(and (Flag292-)))

(:derived (Flag302-)(and (Flag293-)))

(:derived (Flag302-)(and (Flag251-)))

(:derived (Flag302-)(and (Flag231-)))

(:derived (Flag302-)(and (Flag248-)))

(:derived (Flag302-)(and (Flag219-)))

(:derived (Flag302-)(and (Flag206-)))

(:derived (Flag302-)(and (Flag269-)))

(:derived (Flag302-)(and (Flag237-)))

(:derived (Flag302-)(and (Flag253-)))

(:derived (Flag302-)(and (Flag274-)))

(:derived (Flag302-)(and (Flag294-)))

(:derived (Flag302-)(and (Flag225-)))

(:derived (Flag302-)(and (Flag238-)))

(:derived (Flag302-)(and (Flag262-)))

(:derived (Flag302-)(and (Flag280-)))

(:derived (Flag302-)(and (Flag240-)))

(:derived (Flag302-)(and (Flag215-)))

(:derived (Flag302-)(and (Flag254-)))

(:derived (Flag302-)(and (Flag281-)))

(:derived (Flag302-)(and (Flag241-)))

(:derived (Flag302-)(and (Flag282-)))

(:derived (Flag302-)(and (Flag271-)))

(:derived (Flag302-)(and (Flag272-)))

(:derived (Flag302-)(and (Flag273-)))

(:derived (Flag302-)(and (Flag229-)))

(:derived (Flag302-)(and (Flag295-)))

(:derived (Flag302-)(and (Flag242-)))

(:derived (Flag302-)(and (Flag243-)))

(:derived (Flag302-)(and (Flag207-)))

(:derived (Flag302-)(and (Flag230-)))

(:derived (Flag302-)(and (Flag296-)))

(:derived (Flag302-)(and (Flag275-)))

(:derived (Flag302-)(and (Flag276-)))

(:derived (Flag302-)(and (Flag297-)))

(:derived (Flag302-)(and (Flag228-)))

(:derived (Flag302-)(and (Flag256-)))

(:derived (Flag302-)(and (Flag216-)))

(:derived (Flag302-)(and (Flag246-)))

(:derived (Flag302-)(and (Flag257-)))

(:derived (Flag302-)(and (Flag258-)))

(:derived (Flag302-)(and (Flag284-)))

(:derived (Flag302-)(and (Flag247-)))

(:derived (Flag302-)(and (Flag298-)))

(:derived (Flag302-)(and (Flag299-)))

(:derived (Flag302-)(and (Flag300-)))

(:derived (Flag302-)(and (Flag285-)))

(:derived (Flag302-)(and (Flag259-)))

(:derived (Flag302-)(and (Flag209-)))

(:derived (Flag302-)(and (Flag287-)))

(:derived (Flag302-)(and (Flag301-)))

(:derived (Flag302-)(and (Flag232-)))

(:derived (Flag302-)(and (Flag288-)))

(:derived (Flag302-)(and (Flag277-)))

(:derived (Flag302-)(and (Flag217-)))

(:derived (Flag302-)(and (Flag218-)))

(:derived (Flag302-)(and (Flag264-)))

(:derived (Flag302-)(and (Flag234-)))

(:derived (Flag302-)(and (Flag268-)))

(:derived (Flag302-)(and (Flag289-)))

(:derived (Flag312-)(and (Flag266-)))

(:derived (Flag312-)(and (Flag221-)))

(:derived (Flag312-)(and (Flag303-)))

(:derived (Flag312-)(and (Flag267-)))

(:derived (Flag312-)(and (Flag208-)))

(:derived (Flag312-)(and (Flag273-)))

(:derived (Flag312-)(and (Flag291-)))

(:derived (Flag312-)(and (Flag238-)))

(:derived (Flag312-)(and (Flag292-)))

(:derived (Flag312-)(and (Flag251-)))

(:derived (Flag312-)(and (Flag231-)))

(:derived (Flag312-)(and (Flag229-)))

(:derived (Flag312-)(and (Flag304-)))

(:derived (Flag312-)(and (Flag219-)))

(:derived (Flag312-)(and (Flag268-)))

(:derived (Flag312-)(and (Flag222-)))

(:derived (Flag312-)(and (Flag305-)))

(:derived (Flag312-)(and (Flag269-)))

(:derived (Flag312-)(and (Flag253-)))

(:derived (Flag312-)(and (Flag306-)))

(:derived (Flag312-)(and (Flag274-)))

(:derived (Flag312-)(and (Flag294-)))

(:derived (Flag312-)(and (Flag207-)))

(:derived (Flag312-)(and (Flag262-)))

(:derived (Flag312-)(and (Flag280-)))

(:derived (Flag312-)(and (Flag240-)))

(:derived (Flag312-)(and (Flag215-)))

(:derived (Flag312-)(and (Flag254-)))

(:derived (Flag312-)(and (Flag281-)))

(:derived (Flag312-)(and (Flag241-)))

(:derived (Flag312-)(and (Flag282-)))

(:derived (Flag312-)(and (Flag271-)))

(:derived (Flag312-)(and (Flag307-)))

(:derived (Flag312-)(and (Flag308-)))

(:derived (Flag312-)(and (Flag309-)))

(:derived (Flag312-)(and (Flag310-)))

(:derived (Flag312-)(and (Flag206-)))

(:derived (Flag312-)(and (Flag242-)))

(:derived (Flag312-)(and (Flag243-)))

(:derived (Flag312-)(and (Flag225-)))

(:derived (Flag312-)(and (Flag230-)))

(:derived (Flag312-)(and (Flag296-)))

(:derived (Flag312-)(and (Flag275-)))

(:derived (Flag312-)(and (Flag276-)))

(:derived (Flag312-)(and (Flag297-)))

(:derived (Flag312-)(and (Flag228-)))

(:derived (Flag312-)(and (Flag256-)))

(:derived (Flag312-)(and (Flag216-)))

(:derived (Flag312-)(and (Flag246-)))

(:derived (Flag312-)(and (Flag257-)))

(:derived (Flag312-)(and (Flag258-)))

(:derived (Flag312-)(and (Flag284-)))

(:derived (Flag312-)(and (Flag247-)))

(:derived (Flag312-)(and (Flag298-)))

(:derived (Flag312-)(and (Flag300-)))

(:derived (Flag312-)(and (Flag285-)))

(:derived (Flag312-)(and (Flag259-)))

(:derived (Flag312-)(and (Flag209-)))

(:derived (Flag312-)(and (Flag287-)))

(:derived (Flag312-)(and (Flag301-)))

(:derived (Flag312-)(and (Flag232-)))

(:derived (Flag312-)(and (Flag288-)))

(:derived (Flag312-)(and (Flag311-)))

(:derived (Flag312-)(and (Flag277-)))

(:derived (Flag312-)(and (Flag218-)))

(:derived (Flag312-)(and (Flag264-)))

(:derived (Flag312-)(and (Flag248-)))

(:derived (Flag312-)(and (Flag289-)))

(:derived (Flag321-)(and (Flag266-)))

(:derived (Flag321-)(and (Flag221-)))

(:derived (Flag321-)(and (Flag264-)))

(:derived (Flag321-)(and (Flag222-)))

(:derived (Flag321-)(and (Flag208-)))

(:derived (Flag321-)(and (Flag273-)))

(:derived (Flag321-)(and (Flag291-)))

(:derived (Flag321-)(and (Flag238-)))

(:derived (Flag321-)(and (Flag292-)))

(:derived (Flag321-)(and (Flag251-)))

(:derived (Flag321-)(and (Flag231-)))

(:derived (Flag321-)(and (Flag229-)))

(:derived (Flag321-)(and (Flag304-)))

(:derived (Flag321-)(and (Flag219-)))

(:derived (Flag321-)(and (Flag268-)))

(:derived (Flag321-)(and (Flag313-)))

(:derived (Flag321-)(and (Flag314-)))

(:derived (Flag321-)(and (Flag305-)))

(:derived (Flag321-)(and (Flag269-)))

(:derived (Flag321-)(and (Flag306-)))

(:derived (Flag321-)(and (Flag243-)))

(:derived (Flag321-)(and (Flag315-)))

(:derived (Flag321-)(and (Flag294-)))

(:derived (Flag321-)(and (Flag262-)))

(:derived (Flag321-)(and (Flag280-)))

(:derived (Flag321-)(and (Flag281-)))

(:derived (Flag321-)(and (Flag215-)))

(:derived (Flag321-)(and (Flag254-)))

(:derived (Flag321-)(and (Flag241-)))

(:derived (Flag321-)(and (Flag282-)))

(:derived (Flag321-)(and (Flag271-)))

(:derived (Flag321-)(and (Flag316-)))

(:derived (Flag321-)(and (Flag307-)))

(:derived (Flag321-)(and (Flag308-)))

(:derived (Flag321-)(and (Flag310-)))

(:derived (Flag321-)(and (Flag206-)))

(:derived (Flag321-)(and (Flag242-)))

(:derived (Flag321-)(and (Flag274-)))

(:derived (Flag321-)(and (Flag317-)))

(:derived (Flag321-)(and (Flag230-)))

(:derived (Flag321-)(and (Flag296-)))

(:derived (Flag321-)(and (Flag275-)))

(:derived (Flag321-)(and (Flag318-)))

(:derived (Flag321-)(and (Flag276-)))

(:derived (Flag321-)(and (Flag297-)))

(:derived (Flag321-)(and (Flag228-)))

(:derived (Flag321-)(and (Flag319-)))

(:derived (Flag321-)(and (Flag216-)))

(:derived (Flag321-)(and (Flag246-)))

(:derived (Flag321-)(and (Flag257-)))

(:derived (Flag321-)(and (Flag258-)))

(:derived (Flag321-)(and (Flag247-)))

(:derived (Flag321-)(and (Flag298-)))

(:derived (Flag321-)(and (Flag300-)))

(:derived (Flag321-)(and (Flag285-)))

(:derived (Flag321-)(and (Flag259-)))

(:derived (Flag321-)(and (Flag209-)))

(:derived (Flag321-)(and (Flag287-)))

(:derived (Flag321-)(and (Flag301-)))

(:derived (Flag321-)(and (Flag232-)))

(:derived (Flag321-)(and (Flag288-)))

(:derived (Flag321-)(and (Flag311-)))

(:derived (Flag321-)(and (Flag277-)))

(:derived (Flag321-)(and (Flag218-)))

(:derived (Flag321-)(and (Flag320-)))

(:derived (Flag321-)(and (Flag248-)))

(:derived (Flag321-)(and (Flag289-)))

(:derived (Flag328-)(and (Flag266-)))

(:derived (Flag328-)(and (Flag221-)))

(:derived (Flag328-)(and (Flag264-)))

(:derived (Flag328-)(and (Flag222-)))

(:derived (Flag328-)(and (Flag208-)))

(:derived (Flag328-)(and (Flag322-)))

(:derived (Flag328-)(and (Flag294-)))

(:derived (Flag328-)(and (Flag323-)))

(:derived (Flag328-)(and (Flag324-)))

(:derived (Flag328-)(and (Flag325-)))

(:derived (Flag328-)(and (Flag251-)))

(:derived (Flag328-)(and (Flag231-)))

(:derived (Flag328-)(and (Flag229-)))

(:derived (Flag328-)(and (Flag304-)))

(:derived (Flag328-)(and (Flag326-)))

(:derived (Flag328-)(and (Flag219-)))

(:derived (Flag328-)(and (Flag268-)))

(:derived (Flag328-)(and (Flag313-)))

(:derived (Flag328-)(and (Flag305-)))

(:derived (Flag328-)(and (Flag269-)))

(:derived (Flag328-)(and (Flag306-)))

(:derived (Flag328-)(and (Flag277-)))

(:derived (Flag328-)(and (Flag315-)))

(:derived (Flag328-)(and (Flag327-)))

(:derived (Flag328-)(and (Flag238-)))

(:derived (Flag328-)(and (Flag262-)))

(:derived (Flag328-)(and (Flag280-)))

(:derived (Flag328-)(and (Flag215-)))

(:derived (Flag328-)(and (Flag254-)))

(:derived (Flag328-)(and (Flag241-)))

(:derived (Flag328-)(and (Flag282-)))

(:derived (Flag328-)(and (Flag316-)))

(:derived (Flag328-)(and (Flag307-)))

(:derived (Flag328-)(and (Flag273-)))

(:derived (Flag328-)(and (Flag310-)))

(:derived (Flag328-)(and (Flag206-)))

(:derived (Flag328-)(and (Flag242-)))

(:derived (Flag328-)(and (Flag274-)))

(:derived (Flag328-)(and (Flag317-)))

(:derived (Flag328-)(and (Flag230-)))

(:derived (Flag328-)(and (Flag296-)))

(:derived (Flag328-)(and (Flag275-)))

(:derived (Flag328-)(and (Flag319-)))

(:derived (Flag328-)(and (Flag297-)))

(:derived (Flag328-)(and (Flag318-)))

(:derived (Flag328-)(and (Flag228-)))

(:derived (Flag328-)(and (Flag246-)))

(:derived (Flag328-)(and (Flag257-)))

(:derived (Flag328-)(and (Flag258-)))

(:derived (Flag328-)(and (Flag247-)))

(:derived (Flag328-)(and (Flag298-)))

(:derived (Flag328-)(and (Flag300-)))

(:derived (Flag328-)(and (Flag285-)))

(:derived (Flag328-)(and (Flag259-)))

(:derived (Flag328-)(and (Flag291-)))

(:derived (Flag328-)(and (Flag209-)))

(:derived (Flag328-)(and (Flag287-)))

(:derived (Flag328-)(and (Flag301-)))

(:derived (Flag328-)(and (Flag288-)))

(:derived (Flag328-)(and (Flag311-)))

(:derived (Flag328-)(and (Flag218-)))

(:derived (Flag328-)(and (Flag320-)))

(:derived (Flag328-)(and (Flag248-)))

(:derived (Flag328-)(and (Flag289-)))

(:derived (Flag335-)(and (Flag329-)))

(:derived (Flag335-)(and (Flag221-)))

(:derived (Flag335-)(and (Flag291-)))

(:derived (Flag335-)(and (Flag322-)))

(:derived (Flag335-)(and (Flag330-)))

(:derived (Flag335-)(and (Flag324-)))

(:derived (Flag335-)(and (Flag325-)))

(:derived (Flag335-)(and (Flag251-)))

(:derived (Flag335-)(and (Flag231-)))

(:derived (Flag335-)(and (Flag229-)))

(:derived (Flag335-)(and (Flag323-)))

(:derived (Flag335-)(and (Flag326-)))

(:derived (Flag335-)(and (Flag219-)))

(:derived (Flag335-)(and (Flag268-)))

(:derived (Flag335-)(and (Flag313-)))

(:derived (Flag335-)(and (Flag305-)))

(:derived (Flag335-)(and (Flag269-)))

(:derived (Flag335-)(and (Flag306-)))

(:derived (Flag335-)(and (Flag277-)))

(:derived (Flag335-)(and (Flag294-)))

(:derived (Flag335-)(and (Flag238-)))

(:derived (Flag335-)(and (Flag280-)))

(:derived (Flag335-)(and (Flag215-)))

(:derived (Flag335-)(and (Flag254-)))

(:derived (Flag335-)(and (Flag241-)))

(:derived (Flag335-)(and (Flag282-)))

(:derived (Flag335-)(and (Flag331-)))

(:derived (Flag335-)(and (Flag316-)))

(:derived (Flag335-)(and (Flag307-)))

(:derived (Flag335-)(and (Flag273-)))

(:derived (Flag335-)(and (Flag310-)))

(:derived (Flag335-)(and (Flag206-)))

(:derived (Flag335-)(and (Flag242-)))

(:derived (Flag335-)(and (Flag274-)))

(:derived (Flag335-)(and (Flag317-)))

(:derived (Flag335-)(and (Flag230-)))

(:derived (Flag335-)(and (Flag296-)))

(:derived (Flag335-)(and (Flag275-)))

(:derived (Flag335-)(and (Flag318-)))

(:derived (Flag335-)(and (Flag297-)))

(:derived (Flag335-)(and (Flag332-)))

(:derived (Flag335-)(and (Flag319-)))

(:derived (Flag335-)(and (Flag228-)))

(:derived (Flag335-)(and (Flag246-)))

(:derived (Flag335-)(and (Flag257-)))

(:derived (Flag335-)(and (Flag264-)))

(:derived (Flag335-)(and (Flag247-)))

(:derived (Flag335-)(and (Flag300-)))

(:derived (Flag335-)(and (Flag285-)))

(:derived (Flag335-)(and (Flag209-)))

(:derived (Flag335-)(and (Flag287-)))

(:derived (Flag335-)(and (Flag301-)))

(:derived (Flag335-)(and (Flag333-)))

(:derived (Flag335-)(and (Flag262-)))

(:derived (Flag335-)(and (Flag311-)))

(:derived (Flag335-)(and (Flag218-)))

(:derived (Flag335-)(and (Flag320-)))

(:derived (Flag335-)(and (Flag334-)))

(:derived (Flag335-)(and (Flag288-)))

(:derived (Flag340-)(and (Flag329-)))

(:derived (Flag340-)(and (Flag221-)))

(:derived (Flag340-)(and (Flag322-)))

(:derived (Flag340-)(and (Flag330-)))

(:derived (Flag340-)(and (Flag324-)))

(:derived (Flag340-)(and (Flag251-)))

(:derived (Flag340-)(and (Flag336-)))

(:derived (Flag340-)(and (Flag231-)))

(:derived (Flag340-)(and (Flag323-)))

(:derived (Flag340-)(and (Flag326-)))

(:derived (Flag340-)(and (Flag313-)))

(:derived (Flag340-)(and (Flag305-)))

(:derived (Flag340-)(and (Flag269-)))

(:derived (Flag340-)(and (Flag306-)))

(:derived (Flag340-)(and (Flag277-)))

(:derived (Flag340-)(and (Flag294-)))

(:derived (Flag340-)(and (Flag238-)))

(:derived (Flag340-)(and (Flag280-)))

(:derived (Flag340-)(and (Flag215-)))

(:derived (Flag340-)(and (Flag254-)))

(:derived (Flag340-)(and (Flag241-)))

(:derived (Flag340-)(and (Flag282-)))

(:derived (Flag340-)(and (Flag337-)))

(:derived (Flag340-)(and (Flag331-)))

(:derived (Flag340-)(and (Flag338-)))

(:derived (Flag340-)(and (Flag310-)))

(:derived (Flag340-)(and (Flag206-)))

(:derived (Flag340-)(and (Flag291-)))

(:derived (Flag340-)(and (Flag317-)))

(:derived (Flag340-)(and (Flag230-)))

(:derived (Flag340-)(and (Flag296-)))

(:derived (Flag340-)(and (Flag275-)))

(:derived (Flag340-)(and (Flag318-)))

(:derived (Flag340-)(and (Flag297-)))

(:derived (Flag340-)(and (Flag332-)))

(:derived (Flag340-)(and (Flag319-)))

(:derived (Flag340-)(and (Flag228-)))

(:derived (Flag340-)(and (Flag246-)))

(:derived (Flag340-)(and (Flag257-)))

(:derived (Flag340-)(and (Flag264-)))

(:derived (Flag340-)(and (Flag247-)))

(:derived (Flag340-)(and (Flag300-)))

(:derived (Flag340-)(and (Flag339-)))

(:derived (Flag340-)(and (Flag209-)))

(:derived (Flag340-)(and (Flag287-)))

(:derived (Flag340-)(and (Flag268-)))

(:derived (Flag340-)(and (Flag262-)))

(:derived (Flag340-)(and (Flag311-)))

(:derived (Flag340-)(and (Flag218-)))

(:derived (Flag340-)(and (Flag320-)))

(:derived (Flag340-)(and (Flag334-)))

(:derived (Flag340-)(and (Flag288-)))

(:derived (Flag345-)(and (Flag329-)))

(:derived (Flag345-)(and (Flag221-)))

(:derived (Flag345-)(and (Flag322-)))

(:derived (Flag345-)(and (Flag238-)))

(:derived (Flag345-)(and (Flag251-)))

(:derived (Flag345-)(and (Flag323-)))

(:derived (Flag345-)(and (Flag326-)))

(:derived (Flag345-)(and (Flag313-)))

(:derived (Flag345-)(and (Flag305-)))

(:derived (Flag345-)(and (Flag269-)))

(:derived (Flag345-)(and (Flag306-)))

(:derived (Flag345-)(and (Flag277-)))

(:derived (Flag345-)(and (Flag294-)))

(:derived (Flag345-)(and (Flag254-)))

(:derived (Flag345-)(and (Flag241-)))

(:derived (Flag345-)(and (Flag337-)))

(:derived (Flag345-)(and (Flag331-)))

(:derived (Flag345-)(and (Flag310-)))

(:derived (Flag345-)(and (Flag206-)))

(:derived (Flag345-)(and (Flag341-)))

(:derived (Flag345-)(and (Flag317-)))

(:derived (Flag345-)(and (Flag230-)))

(:derived (Flag345-)(and (Flag296-)))

(:derived (Flag345-)(and (Flag275-)))

(:derived (Flag345-)(and (Flag318-)))

(:derived (Flag345-)(and (Flag342-)))

(:derived (Flag345-)(and (Flag332-)))

(:derived (Flag345-)(and (Flag228-)))

(:derived (Flag345-)(and (Flag257-)))

(:derived (Flag345-)(and (Flag247-)))

(:derived (Flag345-)(and (Flag297-)))

(:derived (Flag345-)(and (Flag300-)))

(:derived (Flag345-)(and (Flag339-)))

(:derived (Flag345-)(and (Flag343-)))

(:derived (Flag345-)(and (Flag209-)))

(:derived (Flag345-)(and (Flag287-)))

(:derived (Flag345-)(and (Flag262-)))

(:derived (Flag345-)(and (Flag344-)))

(:derived (Flag345-)(and (Flag218-)))

(:derived (Flag345-)(and (Flag320-)))

(:derived (Flag345-)(and (Flag334-)))

(:derived (Flag345-)(and (Flag288-)))

(:derived (Flag348-)(and (Flag329-)))

(:derived (Flag348-)(and (Flag221-)))

(:derived (Flag348-)(and (Flag346-)))

(:derived (Flag348-)(and (Flag238-)))

(:derived (Flag348-)(and (Flag251-)))

(:derived (Flag348-)(and (Flag326-)))

(:derived (Flag348-)(and (Flag313-)))

(:derived (Flag348-)(and (Flag305-)))

(:derived (Flag348-)(and (Flag269-)))

(:derived (Flag348-)(and (Flag306-)))

(:derived (Flag348-)(and (Flag277-)))

(:derived (Flag348-)(and (Flag294-)))

(:derived (Flag348-)(and (Flag254-)))

(:derived (Flag348-)(and (Flag337-)))

(:derived (Flag348-)(and (Flag331-)))

(:derived (Flag348-)(and (Flag347-)))

(:derived (Flag348-)(and (Flag322-)))

(:derived (Flag348-)(and (Flag317-)))

(:derived (Flag348-)(and (Flag342-)))

(:derived (Flag348-)(and (Flag332-)))

(:derived (Flag348-)(and (Flag228-)))

(:derived (Flag348-)(and (Flag297-)))

(:derived (Flag348-)(and (Flag339-)))

(:derived (Flag348-)(and (Flag343-)))

(:derived (Flag348-)(and (Flag209-)))

(:derived (Flag348-)(and (Flag287-)))

(:derived (Flag348-)(and (Flag262-)))

(:derived (Flag348-)(and (Flag218-)))

(:derived (Flag348-)(and (Flag320-)))

(:derived (Flag348-)(and (Flag288-)))

(:derived (Flag351-)(and (Flag329-)))

(:derived (Flag351-)(and (Flag317-)))

(:derived (Flag351-)(and (Flag277-)))

(:derived (Flag351-)(and (Flag337-)))

(:derived (Flag351-)(and (Flag346-)))

(:derived (Flag351-)(and (Flag238-)))

(:derived (Flag351-)(and (Flag342-)))

(:derived (Flag351-)(and (Flag254-)))

(:derived (Flag351-)(and (Flag228-)))

(:derived (Flag351-)(and (Flag218-)))

(:derived (Flag351-)(and (Flag320-)))

(:derived (Flag351-)(and (Flag350-)))

(:derived (Flag351-)(and (Flag331-)))

(:derived (Flag351-)(and (Flag288-)))

(:derived (Flag351-)(and (Flag297-)))

(:derived (Flag1-)(and (COLUMN2-ROBOT)(ROW1-ROBOT)(NOT-ERROR-)(NOT-CHECKCONSISTENCY-)))

(:derived (Flag3-)(and (LEFTOF3-ROBOT)))

(:derived (Flag3-)(and (RIGHTOF7-ROBOT)))

(:derived (Flag3-)(and (RIGHTOF8-ROBOT)))

(:derived (Flag3-)(and (LEFTOF2-ROBOT)))

(:derived (Flag3-)(and (LEFTOF9-ROBOT)))

(:derived (Flag3-)(and (LEFTOF1-ROBOT)))

(:derived (Flag3-)(and (LEFTOF8-ROBOT)))

(:derived (Flag3-)(and (LEFTOF13-ROBOT)))

(:derived (Flag3-)(and (LEFTOF5-ROBOT)))

(:derived (Flag3-)(and (RIGHTOF12-ROBOT)))

(:derived (Flag3-)(and (RIGHTOF1-ROBOT)))

(:derived (Flag3-)(and (LEFTOF12-ROBOT)))

(:derived (Flag3-)(and (RIGHTOF13-ROBOT)))

(:derived (Flag3-)(and (RIGHTOF14-ROBOT)))

(:derived (Flag3-)(and (RIGHTOF6-ROBOT)))

(:derived (Flag3-)(and (RIGHTOF9-ROBOT)))

(:derived (Flag3-)(and (LEFTOF6-ROBOT)))

(:derived (Flag3-)(and (RIGHTOF0-ROBOT)))

(:derived (Flag3-)(and (RIGHTOF11-ROBOT)))

(:derived (Flag3-)(and (LEFTOF11-ROBOT)))

(:derived (Flag3-)(and (RIGHTOF5-ROBOT)))

(:derived (Flag3-)(and (LEFTOF10-ROBOT)))

(:derived (Flag3-)(and (LEFTOF15-ROBOT)))

(:derived (Flag3-)(and (LEFTOF4-ROBOT)))

(:derived (Flag3-)(and (RIGHTOF4-ROBOT)))

(:derived (Flag3-)(and (RIGHTOF10-ROBOT)))

(:derived (Flag3-)(and (LEFTOF7-ROBOT)))

(:derived (Flag3-)(and (RIGHTOF3-ROBOT)))

(:derived (Flag60-)(and (Flag32-)))

(:derived (Flag60-)(and (Flag33-)))

(:derived (Flag60-)(and (Flag34-)))

(:derived (Flag60-)(and (Flag35-)))

(:derived (Flag60-)(and (Flag36-)))

(:derived (Flag60-)(and (Flag37-)))

(:derived (Flag60-)(and (Flag38-)))

(:derived (Flag60-)(and (Flag39-)))

(:derived (Flag60-)(and (Flag40-)))

(:derived (Flag60-)(and (Flag41-)))

(:derived (Flag60-)(and (Flag42-)))

(:derived (Flag60-)(and (Flag43-)))

(:derived (Flag60-)(and (Flag44-)))

(:derived (Flag60-)(and (Flag45-)))

(:derived (Flag60-)(and (Flag46-)))

(:derived (Flag60-)(and (Flag47-)))

(:derived (Flag60-)(and (Flag48-)))

(:derived (Flag60-)(and (Flag49-)))

(:derived (Flag60-)(and (Flag50-)))

(:derived (Flag60-)(and (Flag51-)))

(:derived (Flag60-)(and (Flag52-)))

(:derived (Flag60-)(and (Flag53-)))

(:derived (Flag60-)(and (Flag54-)))

(:derived (Flag60-)(and (Flag55-)))

(:derived (Flag60-)(and (Flag56-)))

(:derived (Flag60-)(and (Flag57-)))

(:derived (Flag60-)(and (Flag58-)))

(:derived (Flag60-)(and (Flag59-)))

(:derived (Flag75-)(and (Flag61-)))

(:derived (Flag75-)(and (Flag62-)))

(:derived (Flag75-)(and (Flag43-)))

(:derived (Flag75-)(and (Flag63-)))

(:derived (Flag75-)(and (Flag33-)))

(:derived (Flag75-)(and (Flag49-)))

(:derived (Flag75-)(and (Flag35-)))

(:derived (Flag75-)(and (Flag64-)))

(:derived (Flag75-)(and (Flag36-)))

(:derived (Flag75-)(and (Flag37-)))

(:derived (Flag75-)(and (Flag39-)))

(:derived (Flag75-)(and (Flag65-)))

(:derived (Flag75-)(and (Flag40-)))

(:derived (Flag75-)(and (Flag41-)))

(:derived (Flag75-)(and (Flag66-)))

(:derived (Flag75-)(and (Flag42-)))

(:derived (Flag75-)(and (Flag67-)))

(:derived (Flag75-)(and (Flag44-)))

(:derived (Flag75-)(and (Flag45-)))

(:derived (Flag75-)(and (Flag68-)))

(:derived (Flag75-)(and (Flag46-)))

(:derived (Flag75-)(and (Flag69-)))

(:derived (Flag75-)(and (Flag48-)))

(:derived (Flag75-)(and (Flag47-)))

(:derived (Flag75-)(and (Flag70-)))

(:derived (Flag75-)(and (Flag71-)))

(:derived (Flag75-)(and (Flag50-)))

(:derived (Flag75-)(and (Flag51-)))

(:derived (Flag75-)(and (Flag52-)))

(:derived (Flag75-)(and (Flag53-)))

(:derived (Flag75-)(and (Flag54-)))

(:derived (Flag75-)(and (Flag55-)))

(:derived (Flag75-)(and (Flag72-)))

(:derived (Flag75-)(and (Flag56-)))

(:derived (Flag75-)(and (Flag73-)))

(:derived (Flag75-)(and (Flag74-)))

(:derived (Flag75-)(and (Flag57-)))

(:derived (Flag75-)(and (Flag58-)))

(:derived (Flag75-)(and (Flag59-)))

(:derived (Flag90-)(and (Flag61-)))

(:derived (Flag90-)(and (Flag76-)))

(:derived (Flag90-)(and (Flag77-)))

(:derived (Flag90-)(and (Flag78-)))

(:derived (Flag90-)(and (Flag62-)))

(:derived (Flag90-)(and (Flag63-)))

(:derived (Flag90-)(and (Flag33-)))

(:derived (Flag90-)(and (Flag49-)))

(:derived (Flag90-)(and (Flag35-)))

(:derived (Flag90-)(and (Flag65-)))

(:derived (Flag90-)(and (Flag79-)))

(:derived (Flag90-)(and (Flag64-)))

(:derived (Flag90-)(and (Flag36-)))

(:derived (Flag90-)(and (Flag37-)))

(:derived (Flag90-)(and (Flag80-)))

(:derived (Flag90-)(and (Flag39-)))

(:derived (Flag90-)(and (Flag81-)))

(:derived (Flag90-)(and (Flag82-)))

(:derived (Flag90-)(and (Flag83-)))

(:derived (Flag90-)(and (Flag40-)))

(:derived (Flag90-)(and (Flag84-)))

(:derived (Flag90-)(and (Flag85-)))

(:derived (Flag90-)(and (Flag86-)))

(:derived (Flag90-)(and (Flag42-)))

(:derived (Flag90-)(and (Flag44-)))

(:derived (Flag90-)(and (Flag45-)))

(:derived (Flag90-)(and (Flag68-)))

(:derived (Flag90-)(and (Flag46-)))

(:derived (Flag90-)(and (Flag69-)))

(:derived (Flag90-)(and (Flag55-)))

(:derived (Flag90-)(and (Flag48-)))

(:derived (Flag90-)(and (Flag47-)))

(:derived (Flag90-)(and (Flag70-)))

(:derived (Flag90-)(and (Flag87-)))

(:derived (Flag90-)(and (Flag50-)))

(:derived (Flag90-)(and (Flag51-)))

(:derived (Flag90-)(and (Flag52-)))

(:derived (Flag90-)(and (Flag53-)))

(:derived (Flag90-)(and (Flag54-)))

(:derived (Flag90-)(and (Flag88-)))

(:derived (Flag90-)(and (Flag72-)))

(:derived (Flag90-)(and (Flag89-)))

(:derived (Flag90-)(and (Flag73-)))

(:derived (Flag90-)(and (Flag41-)))

(:derived (Flag90-)(and (Flag56-)))

(:derived (Flag90-)(and (Flag74-)))

(:derived (Flag90-)(and (Flag57-)))

(:derived (Flag90-)(and (Flag58-)))

(:derived (Flag90-)(and (Flag59-)))

(:derived (Flag102-)(and (Flag64-)))

(:derived (Flag102-)(and (Flag91-)))

(:derived (Flag102-)(and (Flag61-)))

(:derived (Flag102-)(and (Flag76-)))

(:derived (Flag102-)(and (Flag83-)))

(:derived (Flag102-)(and (Flag62-)))

(:derived (Flag102-)(and (Flag92-)))

(:derived (Flag102-)(and (Flag63-)))

(:derived (Flag102-)(and (Flag33-)))

(:derived (Flag102-)(and (Flag35-)))

(:derived (Flag102-)(and (Flag65-)))

(:derived (Flag102-)(and (Flag79-)))

(:derived (Flag102-)(and (Flag81-)))

(:derived (Flag102-)(and (Flag36-)))

(:derived (Flag102-)(and (Flag37-)))

(:derived (Flag102-)(and (Flag93-)))

(:derived (Flag102-)(and (Flag80-)))

(:derived (Flag102-)(and (Flag39-)))

(:derived (Flag102-)(and (Flag82-)))

(:derived (Flag102-)(and (Flag94-)))

(:derived (Flag102-)(and (Flag95-)))

(:derived (Flag102-)(and (Flag96-)))

(:derived (Flag102-)(and (Flag40-)))

(:derived (Flag102-)(and (Flag84-)))

(:derived (Flag102-)(and (Flag85-)))

(:derived (Flag102-)(and (Flag42-)))

(:derived (Flag102-)(and (Flag44-)))

(:derived (Flag102-)(and (Flag45-)))

(:derived (Flag102-)(and (Flag68-)))

(:derived (Flag102-)(and (Flag46-)))

(:derived (Flag102-)(and (Flag97-)))

(:derived (Flag102-)(and (Flag69-)))

(:derived (Flag102-)(and (Flag98-)))

(:derived (Flag102-)(and (Flag55-)))

(:derived (Flag102-)(and (Flag48-)))

(:derived (Flag102-)(and (Flag47-)))

(:derived (Flag102-)(and (Flag49-)))

(:derived (Flag102-)(and (Flag87-)))

(:derived (Flag102-)(and (Flag78-)))

(:derived (Flag102-)(and (Flag51-)))

(:derived (Flag102-)(and (Flag52-)))

(:derived (Flag102-)(and (Flag53-)))

(:derived (Flag102-)(and (Flag54-)))

(:derived (Flag102-)(and (Flag99-)))

(:derived (Flag102-)(and (Flag88-)))

(:derived (Flag102-)(and (Flag72-)))

(:derived (Flag102-)(and (Flag89-)))

(:derived (Flag102-)(and (Flag100-)))

(:derived (Flag102-)(and (Flag73-)))

(:derived (Flag102-)(and (Flag41-)))

(:derived (Flag102-)(and (Flag56-)))

(:derived (Flag102-)(and (Flag74-)))

(:derived (Flag102-)(and (Flag57-)))

(:derived (Flag102-)(and (Flag58-)))

(:derived (Flag102-)(and (Flag70-)))

(:derived (Flag102-)(and (Flag101-)))

(:derived (Flag115-)(and (Flag64-)))

(:derived (Flag115-)(and (Flag61-)))

(:derived (Flag115-)(and (Flag57-)))

(:derived (Flag115-)(and (Flag76-)))

(:derived (Flag115-)(and (Flag103-)))

(:derived (Flag115-)(and (Flag62-)))

(:derived (Flag115-)(and (Flag104-)))

(:derived (Flag115-)(and (Flag63-)))

(:derived (Flag115-)(and (Flag105-)))

(:derived (Flag115-)(and (Flag106-)))

(:derived (Flag115-)(and (Flag33-)))

(:derived (Flag115-)(and (Flag107-)))

(:derived (Flag115-)(and (Flag35-)))

(:derived (Flag115-)(and (Flag65-)))

(:derived (Flag115-)(and (Flag108-)))

(:derived (Flag115-)(and (Flag92-)))

(:derived (Flag115-)(and (Flag51-)))

(:derived (Flag115-)(and (Flag36-)))

(:derived (Flag115-)(and (Flag44-)))

(:derived (Flag115-)(and (Flag37-)))

(:derived (Flag115-)(and (Flag100-)))

(:derived (Flag115-)(and (Flag80-)))

(:derived (Flag115-)(and (Flag39-)))

(:derived (Flag115-)(and (Flag109-)))

(:derived (Flag115-)(and (Flag110-)))

(:derived (Flag115-)(and (Flag82-)))

(:derived (Flag115-)(and (Flag94-)))

(:derived (Flag115-)(and (Flag95-)))

(:derived (Flag115-)(and (Flag83-)))

(:derived (Flag115-)(and (Flag84-)))

(:derived (Flag115-)(and (Flag85-)))

(:derived (Flag115-)(and (Flag42-)))

(:derived (Flag115-)(and (Flag46-)))

(:derived (Flag115-)(and (Flag45-)))

(:derived (Flag115-)(and (Flag68-)))

(:derived (Flag115-)(and (Flag111-)))

(:derived (Flag115-)(and (Flag47-)))

(:derived (Flag115-)(and (Flag98-)))

(:derived (Flag115-)(and (Flag88-)))

(:derived (Flag115-)(and (Flag48-)))

(:derived (Flag115-)(and (Flag49-)))

(:derived (Flag115-)(and (Flag87-)))

(:derived (Flag115-)(and (Flag112-)))

(:derived (Flag115-)(and (Flag78-)))

(:derived (Flag115-)(and (Flag113-)))

(:derived (Flag115-)(and (Flag52-)))

(:derived (Flag115-)(and (Flag53-)))

(:derived (Flag115-)(and (Flag99-)))

(:derived (Flag115-)(and (Flag55-)))

(:derived (Flag115-)(and (Flag72-)))

(:derived (Flag115-)(and (Flag89-)))

(:derived (Flag115-)(and (Flag79-)))

(:derived (Flag115-)(and (Flag73-)))

(:derived (Flag115-)(and (Flag41-)))

(:derived (Flag115-)(and (Flag114-)))

(:derived (Flag115-)(and (Flag56-)))

(:derived (Flag115-)(and (Flag96-)))

(:derived (Flag115-)(and (Flag74-)))

(:derived (Flag115-)(and (Flag93-)))

(:derived (Flag115-)(and (Flag58-)))

(:derived (Flag115-)(and (Flag70-)))

(:derived (Flag115-)(and (Flag101-)))

(:derived (Flag127-)(and (Flag56-)))

(:derived (Flag127-)(and (Flag64-)))

(:derived (Flag127-)(and (Flag79-)))

(:derived (Flag127-)(and (Flag80-)))

(:derived (Flag127-)(and (Flag116-)))

(:derived (Flag127-)(and (Flag76-)))

(:derived (Flag127-)(and (Flag103-)))

(:derived (Flag127-)(and (Flag42-)))

(:derived (Flag127-)(and (Flag104-)))

(:derived (Flag127-)(and (Flag114-)))

(:derived (Flag127-)(and (Flag63-)))

(:derived (Flag127-)(and (Flag105-)))

(:derived (Flag127-)(and (Flag106-)))

(:derived (Flag127-)(and (Flag117-)))

(:derived (Flag127-)(and (Flag33-)))

(:derived (Flag127-)(and (Flag107-)))

(:derived (Flag127-)(and (Flag118-)))

(:derived (Flag127-)(and (Flag35-)))

(:derived (Flag127-)(and (Flag65-)))

(:derived (Flag127-)(and (Flag92-)))

(:derived (Flag127-)(and (Flag51-)))

(:derived (Flag127-)(and (Flag36-)))

(:derived (Flag127-)(and (Flag37-)))

(:derived (Flag127-)(and (Flag100-)))

(:derived (Flag127-)(and (Flag119-)))

(:derived (Flag127-)(and (Flag109-)))

(:derived (Flag127-)(and (Flag82-)))

(:derived (Flag127-)(and (Flag94-)))

(:derived (Flag127-)(and (Flag95-)))

(:derived (Flag127-)(and (Flag83-)))

(:derived (Flag127-)(and (Flag84-)))

(:derived (Flag127-)(and (Flag41-)))

(:derived (Flag127-)(and (Flag120-)))

(:derived (Flag127-)(and (Flag62-)))

(:derived (Flag127-)(and (Flag44-)))

(:derived (Flag127-)(and (Flag45-)))

(:derived (Flag127-)(and (Flag68-)))

(:derived (Flag127-)(and (Flag46-)))

(:derived (Flag127-)(and (Flag47-)))

(:derived (Flag127-)(and (Flag98-)))

(:derived (Flag127-)(and (Flag55-)))

(:derived (Flag127-)(and (Flag48-)))

(:derived (Flag127-)(and (Flag70-)))

(:derived (Flag127-)(and (Flag87-)))

(:derived (Flag127-)(and (Flag112-)))

(:derived (Flag127-)(and (Flag78-)))

(:derived (Flag127-)(and (Flag121-)))

(:derived (Flag127-)(and (Flag113-)))

(:derived (Flag127-)(and (Flag52-)))

(:derived (Flag127-)(and (Flag53-)))

(:derived (Flag127-)(and (Flag99-)))

(:derived (Flag127-)(and (Flag88-)))

(:derived (Flag127-)(and (Flag122-)))

(:derived (Flag127-)(and (Flag123-)))

(:derived (Flag127-)(and (Flag72-)))

(:derived (Flag127-)(and (Flag124-)))

(:derived (Flag127-)(and (Flag73-)))

(:derived (Flag127-)(and (Flag125-)))

(:derived (Flag127-)(and (Flag126-)))

(:derived (Flag127-)(and (Flag89-)))

(:derived (Flag127-)(and (Flag96-)))

(:derived (Flag127-)(and (Flag74-)))

(:derived (Flag127-)(and (Flag93-)))

(:derived (Flag127-)(and (Flag58-)))

(:derived (Flag127-)(and (Flag49-)))

(:derived (Flag127-)(and (Flag101-)))

(:derived (Flag137-)(and (Flag56-)))

(:derived (Flag137-)(and (Flag64-)))

(:derived (Flag137-)(and (Flag79-)))

(:derived (Flag137-)(and (Flag80-)))

(:derived (Flag137-)(and (Flag55-)))

(:derived (Flag137-)(and (Flag76-)))

(:derived (Flag137-)(and (Flag103-)))

(:derived (Flag137-)(and (Flag123-)))

(:derived (Flag137-)(and (Flag128-)))

(:derived (Flag137-)(and (Flag114-)))

(:derived (Flag137-)(and (Flag63-)))

(:derived (Flag137-)(and (Flag105-)))

(:derived (Flag137-)(and (Flag106-)))

(:derived (Flag137-)(and (Flag117-)))

(:derived (Flag137-)(and (Flag33-)))

(:derived (Flag137-)(and (Flag107-)))

(:derived (Flag137-)(and (Flag49-)))

(:derived (Flag137-)(and (Flag35-)))

(:derived (Flag137-)(and (Flag65-)))

(:derived (Flag137-)(and (Flag92-)))

(:derived (Flag137-)(and (Flag51-)))

(:derived (Flag137-)(and (Flag36-)))

(:derived (Flag137-)(and (Flag37-)))

(:derived (Flag137-)(and (Flag119-)))

(:derived (Flag137-)(and (Flag109-)))

(:derived (Flag137-)(and (Flag129-)))

(:derived (Flag137-)(and (Flag82-)))

(:derived (Flag137-)(and (Flag94-)))

(:derived (Flag137-)(and (Flag95-)))

(:derived (Flag137-)(and (Flag130-)))

(:derived (Flag137-)(and (Flag83-)))

(:derived (Flag137-)(and (Flag84-)))

(:derived (Flag137-)(and (Flag41-)))

(:derived (Flag137-)(and (Flag120-)))

(:derived (Flag137-)(and (Flag96-)))

(:derived (Flag137-)(and (Flag131-)))

(:derived (Flag137-)(and (Flag44-)))

(:derived (Flag137-)(and (Flag45-)))

(:derived (Flag137-)(and (Flag68-)))

(:derived (Flag137-)(and (Flag47-)))

(:derived (Flag137-)(and (Flag98-)))

(:derived (Flag137-)(and (Flag132-)))

(:derived (Flag137-)(and (Flag48-)))

(:derived (Flag137-)(and (Flag70-)))

(:derived (Flag137-)(and (Flag87-)))

(:derived (Flag137-)(and (Flag112-)))

(:derived (Flag137-)(and (Flag121-)))

(:derived (Flag137-)(and (Flag113-)))

(:derived (Flag137-)(and (Flag52-)))

(:derived (Flag137-)(and (Flag53-)))

(:derived (Flag137-)(and (Flag133-)))

(:derived (Flag137-)(and (Flag99-)))

(:derived (Flag137-)(and (Flag88-)))

(:derived (Flag137-)(and (Flag122-)))

(:derived (Flag137-)(and (Flag72-)))

(:derived (Flag137-)(and (Flag89-)))

(:derived (Flag137-)(and (Flag104-)))

(:derived (Flag137-)(and (Flag73-)))

(:derived (Flag137-)(and (Flag125-)))

(:derived (Flag137-)(and (Flag126-)))

(:derived (Flag137-)(and (Flag134-)))

(:derived (Flag137-)(and (Flag74-)))

(:derived (Flag137-)(and (Flag100-)))

(:derived (Flag137-)(and (Flag58-)))

(:derived (Flag137-)(and (Flag135-)))

(:derived (Flag137-)(and (Flag136-)))

(:derived (Flag137-)(and (Flag101-)))

(:derived (Flag147-)(and (Flag64-)))

(:derived (Flag147-)(and (Flag138-)))

(:derived (Flag147-)(and (Flag79-)))

(:derived (Flag147-)(and (Flag80-)))

(:derived (Flag147-)(and (Flag55-)))

(:derived (Flag147-)(and (Flag76-)))

(:derived (Flag147-)(and (Flag125-)))

(:derived (Flag147-)(and (Flag104-)))

(:derived (Flag147-)(and (Flag103-)))

(:derived (Flag147-)(and (Flag123-)))

(:derived (Flag147-)(and (Flag128-)))

(:derived (Flag147-)(and (Flag126-)))

(:derived (Flag147-)(and (Flag139-)))

(:derived (Flag147-)(and (Flag105-)))

(:derived (Flag147-)(and (Flag106-)))

(:derived (Flag147-)(and (Flag117-)))

(:derived (Flag147-)(and (Flag33-)))

(:derived (Flag147-)(and (Flag107-)))

(:derived (Flag147-)(and (Flag49-)))

(:derived (Flag147-)(and (Flag65-)))

(:derived (Flag147-)(and (Flag140-)))

(:derived (Flag147-)(and (Flag92-)))

(:derived (Flag147-)(and (Flag51-)))

(:derived (Flag147-)(and (Flag36-)))

(:derived (Flag147-)(and (Flag119-)))

(:derived (Flag147-)(and (Flag141-)))

(:derived (Flag147-)(and (Flag129-)))

(:derived (Flag147-)(and (Flag82-)))

(:derived (Flag147-)(and (Flag94-)))

(:derived (Flag147-)(and (Flag95-)))

(:derived (Flag147-)(and (Flag130-)))

(:derived (Flag147-)(and (Flag96-)))

(:derived (Flag147-)(and (Flag84-)))

(:derived (Flag147-)(and (Flag41-)))

(:derived (Flag147-)(and (Flag120-)))

(:derived (Flag147-)(and (Flag44-)))

(:derived (Flag147-)(and (Flag45-)))

(:derived (Flag147-)(and (Flag68-)))

(:derived (Flag147-)(and (Flag47-)))

(:derived (Flag147-)(and (Flag132-)))

(:derived (Flag147-)(and (Flag48-)))

(:derived (Flag147-)(and (Flag70-)))

(:derived (Flag147-)(and (Flag87-)))

(:derived (Flag147-)(and (Flag112-)))

(:derived (Flag147-)(and (Flag121-)))

(:derived (Flag147-)(and (Flag113-)))

(:derived (Flag147-)(and (Flag52-)))

(:derived (Flag147-)(and (Flag53-)))

(:derived (Flag147-)(and (Flag142-)))

(:derived (Flag147-)(and (Flag99-)))

(:derived (Flag147-)(and (Flag88-)))

(:derived (Flag147-)(and (Flag122-)))

(:derived (Flag147-)(and (Flag143-)))

(:derived (Flag147-)(and (Flag72-)))

(:derived (Flag147-)(and (Flag89-)))

(:derived (Flag147-)(and (Flag144-)))

(:derived (Flag147-)(and (Flag73-)))

(:derived (Flag147-)(and (Flag145-)))

(:derived (Flag147-)(and (Flag114-)))

(:derived (Flag147-)(and (Flag56-)))

(:derived (Flag147-)(and (Flag74-)))

(:derived (Flag147-)(and (Flag100-)))

(:derived (Flag147-)(and (Flag146-)))

(:derived (Flag147-)(and (Flag58-)))

(:derived (Flag147-)(and (Flag135-)))

(:derived (Flag147-)(and (Flag136-)))

(:derived (Flag147-)(and (Flag101-)))

(:derived (Flag155-)(and (Flag64-)))

(:derived (Flag155-)(and (Flag138-)))

(:derived (Flag155-)(and (Flag79-)))

(:derived (Flag155-)(and (Flag80-)))

(:derived (Flag155-)(and (Flag55-)))

(:derived (Flag155-)(and (Flag76-)))

(:derived (Flag155-)(and (Flag104-)))

(:derived (Flag155-)(and (Flag103-)))

(:derived (Flag155-)(and (Flag123-)))

(:derived (Flag155-)(and (Flag128-)))

(:derived (Flag155-)(and (Flag114-)))

(:derived (Flag155-)(and (Flag139-)))

(:derived (Flag155-)(and (Flag105-)))

(:derived (Flag155-)(and (Flag106-)))

(:derived (Flag155-)(and (Flag117-)))

(:derived (Flag155-)(and (Flag33-)))

(:derived (Flag155-)(and (Flag107-)))

(:derived (Flag155-)(and (Flag148-)))

(:derived (Flag155-)(and (Flag149-)))

(:derived (Flag155-)(and (Flag51-)))

(:derived (Flag155-)(and (Flag36-)))

(:derived (Flag155-)(and (Flag49-)))

(:derived (Flag155-)(and (Flag119-)))

(:derived (Flag155-)(and (Flag141-)))

(:derived (Flag155-)(and (Flag129-)))

(:derived (Flag155-)(and (Flag94-)))

(:derived (Flag155-)(and (Flag65-)))

(:derived (Flag155-)(and (Flag95-)))

(:derived (Flag155-)(and (Flag130-)))

(:derived (Flag155-)(and (Flag96-)))

(:derived (Flag155-)(and (Flag84-)))

(:derived (Flag155-)(and (Flag41-)))

(:derived (Flag155-)(and (Flag120-)))

(:derived (Flag155-)(and (Flag150-)))

(:derived (Flag155-)(and (Flag44-)))

(:derived (Flag155-)(and (Flag47-)))

(:derived (Flag155-)(and (Flag132-)))

(:derived (Flag155-)(and (Flag48-)))

(:derived (Flag155-)(and (Flag151-)))

(:derived (Flag155-)(and (Flag70-)))

(:derived (Flag155-)(and (Flag87-)))

(:derived (Flag155-)(and (Flag121-)))

(:derived (Flag155-)(and (Flag113-)))

(:derived (Flag155-)(and (Flag52-)))

(:derived (Flag155-)(and (Flag152-)))

(:derived (Flag155-)(and (Flag142-)))

(:derived (Flag155-)(and (Flag153-)))

(:derived (Flag155-)(and (Flag99-)))

(:derived (Flag155-)(and (Flag88-)))

(:derived (Flag155-)(and (Flag122-)))

(:derived (Flag155-)(and (Flag72-)))

(:derived (Flag155-)(and (Flag89-)))

(:derived (Flag155-)(and (Flag144-)))

(:derived (Flag155-)(and (Flag73-)))

(:derived (Flag155-)(and (Flag145-)))

(:derived (Flag155-)(and (Flag126-)))

(:derived (Flag155-)(and (Flag56-)))

(:derived (Flag155-)(and (Flag74-)))

(:derived (Flag155-)(and (Flag100-)))

(:derived (Flag155-)(and (Flag154-)))

(:derived (Flag155-)(and (Flag45-)))

(:derived (Flag155-)(and (Flag135-)))

(:derived (Flag155-)(and (Flag136-)))

(:derived (Flag155-)(and (Flag101-)))

(:derived (Flag161-)(and (Flag64-)))

(:derived (Flag161-)(and (Flag138-)))

(:derived (Flag161-)(and (Flag79-)))

(:derived (Flag161-)(and (Flag76-)))

(:derived (Flag161-)(and (Flag104-)))

(:derived (Flag161-)(and (Flag103-)))

(:derived (Flag161-)(and (Flag123-)))

(:derived (Flag161-)(and (Flag128-)))

(:derived (Flag161-)(and (Flag139-)))

(:derived (Flag161-)(and (Flag156-)))

(:derived (Flag161-)(and (Flag106-)))

(:derived (Flag161-)(and (Flag33-)))

(:derived (Flag161-)(and (Flag107-)))

(:derived (Flag161-)(and (Flag157-)))

(:derived (Flag161-)(and (Flag49-)))

(:derived (Flag161-)(and (Flag149-)))

(:derived (Flag161-)(and (Flag36-)))

(:derived (Flag161-)(and (Flag158-)))

(:derived (Flag161-)(and (Flag119-)))

(:derived (Flag161-)(and (Flag141-)))

(:derived (Flag161-)(and (Flag129-)))

(:derived (Flag161-)(and (Flag105-)))

(:derived (Flag161-)(and (Flag94-)))

(:derived (Flag161-)(and (Flag95-)))

(:derived (Flag161-)(and (Flag130-)))

(:derived (Flag161-)(and (Flag96-)))

(:derived (Flag161-)(and (Flag122-)))

(:derived (Flag161-)(and (Flag41-)))

(:derived (Flag161-)(and (Flag120-)))

(:derived (Flag161-)(and (Flag150-)))

(:derived (Flag161-)(and (Flag84-)))

(:derived (Flag161-)(and (Flag47-)))

(:derived (Flag161-)(and (Flag55-)))

(:derived (Flag161-)(and (Flag48-)))

(:derived (Flag161-)(and (Flag151-)))

(:derived (Flag161-)(and (Flag70-)))

(:derived (Flag161-)(and (Flag87-)))

(:derived (Flag161-)(and (Flag121-)))

(:derived (Flag161-)(and (Flag52-)))

(:derived (Flag161-)(and (Flag142-)))

(:derived (Flag161-)(and (Flag153-)))

(:derived (Flag161-)(and (Flag88-)))

(:derived (Flag161-)(and (Flag159-)))

(:derived (Flag161-)(and (Flag89-)))

(:derived (Flag161-)(and (Flag72-)))

(:derived (Flag161-)(and (Flag160-)))

(:derived (Flag161-)(and (Flag144-)))

(:derived (Flag161-)(and (Flag73-)))

(:derived (Flag161-)(and (Flag145-)))

(:derived (Flag161-)(and (Flag126-)))

(:derived (Flag161-)(and (Flag56-)))

(:derived (Flag161-)(and (Flag74-)))

(:derived (Flag161-)(and (Flag100-)))

(:derived (Flag161-)(and (Flag154-)))

(:derived (Flag161-)(and (Flag45-)))

(:derived (Flag161-)(and (Flag135-)))

(:derived (Flag161-)(and (Flag136-)))

(:derived (Flag161-)(and (Flag101-)))

(:derived (Flag166-)(and (Flag138-)))

(:derived (Flag166-)(and (Flag79-)))

(:derived (Flag166-)(and (Flag76-)))

(:derived (Flag166-)(and (Flag104-)))

(:derived (Flag166-)(and (Flag103-)))

(:derived (Flag166-)(and (Flag123-)))

(:derived (Flag166-)(and (Flag128-)))

(:derived (Flag166-)(and (Flag105-)))

(:derived (Flag166-)(and (Flag106-)))

(:derived (Flag166-)(and (Flag33-)))

(:derived (Flag166-)(and (Flag107-)))

(:derived (Flag166-)(and (Flag157-)))

(:derived (Flag166-)(and (Flag162-)))

(:derived (Flag166-)(and (Flag149-)))

(:derived (Flag166-)(and (Flag36-)))

(:derived (Flag166-)(and (Flag158-)))

(:derived (Flag166-)(and (Flag41-)))

(:derived (Flag166-)(and (Flag100-)))

(:derived (Flag166-)(and (Flag141-)))

(:derived (Flag166-)(and (Flag94-)))

(:derived (Flag166-)(and (Flag130-)))

(:derived (Flag166-)(and (Flag96-)))

(:derived (Flag166-)(and (Flag84-)))

(:derived (Flag166-)(and (Flag163-)))

(:derived (Flag166-)(and (Flag164-)))

(:derived (Flag166-)(and (Flag47-)))

(:derived (Flag166-)(and (Flag55-)))

(:derived (Flag166-)(and (Flag151-)))

(:derived (Flag166-)(and (Flag49-)))

(:derived (Flag166-)(and (Flag121-)))

(:derived (Flag166-)(and (Flag142-)))

(:derived (Flag166-)(and (Flag153-)))

(:derived (Flag166-)(and (Flag88-)))

(:derived (Flag166-)(and (Flag122-)))

(:derived (Flag166-)(and (Flag159-)))

(:derived (Flag166-)(and (Flag89-)))

(:derived (Flag166-)(and (Flag72-)))

(:derived (Flag166-)(and (Flag160-)))

(:derived (Flag166-)(and (Flag144-)))

(:derived (Flag166-)(and (Flag73-)))

(:derived (Flag166-)(and (Flag145-)))

(:derived (Flag166-)(and (Flag126-)))

(:derived (Flag166-)(and (Flag56-)))

(:derived (Flag166-)(and (Flag64-)))

(:derived (Flag166-)(and (Flag165-)))

(:derived (Flag166-)(and (Flag154-)))

(:derived (Flag166-)(and (Flag45-)))

(:derived (Flag166-)(and (Flag135-)))

(:derived (Flag166-)(and (Flag136-)))

(:derived (Flag166-)(and (Flag101-)))

(:derived (Flag171-)(and (Flag138-)))

(:derived (Flag171-)(and (Flag167-)))

(:derived (Flag171-)(and (Flag73-)))

(:derived (Flag171-)(and (Flag76-)))

(:derived (Flag171-)(and (Flag104-)))

(:derived (Flag171-)(and (Flag123-)))

(:derived (Flag171-)(and (Flag128-)))

(:derived (Flag171-)(and (Flag168-)))

(:derived (Flag171-)(and (Flag105-)))

(:derived (Flag171-)(and (Flag106-)))

(:derived (Flag171-)(and (Flag107-)))

(:derived (Flag171-)(and (Flag79-)))

(:derived (Flag171-)(and (Flag149-)))

(:derived (Flag171-)(and (Flag36-)))

(:derived (Flag171-)(and (Flag158-)))

(:derived (Flag171-)(and (Flag100-)))

(:derived (Flag171-)(and (Flag141-)))

(:derived (Flag171-)(and (Flag94-)))

(:derived (Flag171-)(and (Flag84-)))

(:derived (Flag171-)(and (Flag163-)))

(:derived (Flag171-)(and (Flag169-)))

(:derived (Flag171-)(and (Flag47-)))

(:derived (Flag171-)(and (Flag151-)))

(:derived (Flag171-)(and (Flag121-)))

(:derived (Flag171-)(and (Flag142-)))

(:derived (Flag171-)(and (Flag55-)))

(:derived (Flag171-)(and (Flag122-)))

(:derived (Flag171-)(and (Flag159-)))

(:derived (Flag171-)(and (Flag56-)))

(:derived (Flag171-)(and (Flag72-)))

(:derived (Flag171-)(and (Flag160-)))

(:derived (Flag171-)(and (Flag144-)))

(:derived (Flag171-)(and (Flag170-)))

(:derived (Flag171-)(and (Flag41-)))

(:derived (Flag171-)(and (Flag64-)))

(:derived (Flag171-)(and (Flag165-)))

(:derived (Flag171-)(and (Flag154-)))

(:derived (Flag171-)(and (Flag45-)))

(:derived (Flag171-)(and (Flag136-)))

(:derived (Flag171-)(and (Flag101-)))

(:derived (Flag175-)(and (Flag79-)))

(:derived (Flag175-)(and (Flag76-)))

(:derived (Flag175-)(and (Flag123-)))

(:derived (Flag175-)(and (Flag104-)))

(:derived (Flag175-)(and (Flag106-)))

(:derived (Flag175-)(and (Flag172-)))

(:derived (Flag175-)(and (Flag107-)))

(:derived (Flag175-)(and (Flag167-)))

(:derived (Flag175-)(and (Flag149-)))

(:derived (Flag175-)(and (Flag36-)))

(:derived (Flag175-)(and (Flag158-)))

(:derived (Flag175-)(and (Flag141-)))

(:derived (Flag175-)(and (Flag173-)))

(:derived (Flag175-)(and (Flag159-)))

(:derived (Flag175-)(and (Flag163-)))

(:derived (Flag175-)(and (Flag47-)))

(:derived (Flag175-)(and (Flag174-)))

(:derived (Flag175-)(and (Flag142-)))

(:derived (Flag175-)(and (Flag122-)))

(:derived (Flag175-)(and (Flag128-)))

(:derived (Flag175-)(and (Flag73-)))

(:derived (Flag175-)(and (Flag41-)))

(:derived (Flag175-)(and (Flag64-)))

(:derived (Flag175-)(and (Flag165-)))

(:derived (Flag175-)(and (Flag154-)))

(:derived (Flag175-)(and (Flag45-)))

(:derived (Flag175-)(and (Flag136-)))

(:derived (Flag175-)(and (Flag101-)))

(:derived (Flag177-)(and (Flag79-)))

(:derived (Flag177-)(and (Flag122-)))

(:derived (Flag177-)(and (Flag149-)))

(:derived (Flag177-)(and (Flag167-)))

(:derived (Flag177-)(and (Flag172-)))

(:derived (Flag177-)(and (Flag104-)))

(:derived (Flag177-)(and (Flag141-)))

(:derived (Flag177-)(and (Flag47-)))

(:derived (Flag177-)(and (Flag64-)))

(:derived (Flag177-)(and (Flag165-)))

(:derived (Flag177-)(and (Flag176-)))

(:derived (Flag177-)(and (Flag159-)))

(:derived (Flag177-)(and (Flag101-)))

(:derived (Flag177-)(and (Flag41-)))

(:derived (Flag177-)(and (Flag136-)))

(:derived (Flag178-)(and (RIGHTOF3-ROBOT)))

(:derived (Flag178-)(and (RIGHTOF8-ROBOT)))

(:derived (Flag178-)(and (RIGHTOF10-ROBOT)))

(:derived (Flag178-)(and (RIGHTOF5-ROBOT)))

(:derived (Flag178-)(and (RIGHTOF14-ROBOT)))

(:derived (Flag178-)(and (RIGHTOF13-ROBOT)))

(:derived (Flag178-)(and (RIGHTOF11-ROBOT)))

(:derived (Flag178-)(and (RIGHTOF0-ROBOT)))

(:derived (Flag178-)(and (RIGHTOF4-ROBOT)))

(:derived (Flag178-)(and (RIGHTOF1-ROBOT)))

(:derived (Flag178-)(and (RIGHTOF12-ROBOT)))

(:derived (Flag178-)(and (RIGHTOF7-ROBOT)))

(:derived (Flag178-)(and (RIGHTOF9-ROBOT)))

(:derived (Flag178-)(and (RIGHTOF6-ROBOT)))

(:derived (Flag179-)(and (RIGHTOF3-ROBOT)))

(:derived (Flag179-)(and (RIGHTOF8-ROBOT)))

(:derived (Flag179-)(and (RIGHTOF5-ROBOT)))

(:derived (Flag179-)(and (RIGHTOF14-ROBOT)))

(:derived (Flag179-)(and (RIGHTOF13-ROBOT)))

(:derived (Flag179-)(and (RIGHTOF11-ROBOT)))

(:derived (Flag179-)(and (RIGHTOF10-ROBOT)))

(:derived (Flag179-)(and (RIGHTOF4-ROBOT)))

(:derived (Flag179-)(and (RIGHTOF1-ROBOT)))

(:derived (Flag179-)(and (RIGHTOF12-ROBOT)))

(:derived (Flag179-)(and (RIGHTOF7-ROBOT)))

(:derived (Flag179-)(and (RIGHTOF9-ROBOT)))

(:derived (Flag179-)(and (RIGHTOF6-ROBOT)))

(:derived (Flag180-)(and (RIGHTOF3-ROBOT)))

(:derived (Flag180-)(and (RIGHTOF5-ROBOT)))

(:derived (Flag180-)(and (RIGHTOF14-ROBOT)))

(:derived (Flag180-)(and (RIGHTOF13-ROBOT)))

(:derived (Flag180-)(and (RIGHTOF11-ROBOT)))

(:derived (Flag180-)(and (RIGHTOF10-ROBOT)))

(:derived (Flag180-)(and (RIGHTOF4-ROBOT)))

(:derived (Flag180-)(and (RIGHTOF8-ROBOT)))

(:derived (Flag180-)(and (RIGHTOF12-ROBOT)))

(:derived (Flag180-)(and (RIGHTOF7-ROBOT)))

(:derived (Flag180-)(and (RIGHTOF9-ROBOT)))

(:derived (Flag180-)(and (RIGHTOF6-ROBOT)))

(:derived (Flag181-)(and (RIGHTOF3-ROBOT)))

(:derived (Flag181-)(and (RIGHTOF5-ROBOT)))

(:derived (Flag181-)(and (RIGHTOF14-ROBOT)))

(:derived (Flag181-)(and (RIGHTOF13-ROBOT)))

(:derived (Flag181-)(and (RIGHTOF11-ROBOT)))

(:derived (Flag181-)(and (RIGHTOF10-ROBOT)))

(:derived (Flag181-)(and (RIGHTOF4-ROBOT)))

(:derived (Flag181-)(and (RIGHTOF8-ROBOT)))

(:derived (Flag181-)(and (RIGHTOF12-ROBOT)))

(:derived (Flag181-)(and (RIGHTOF7-ROBOT)))

(:derived (Flag181-)(and (RIGHTOF9-ROBOT)))

(:derived (Flag181-)(and (RIGHTOF6-ROBOT)))

(:derived (Flag182-)(and (RIGHTOF5-ROBOT)))

(:derived (Flag182-)(and (RIGHTOF14-ROBOT)))

(:derived (Flag182-)(and (RIGHTOF13-ROBOT)))

(:derived (Flag182-)(and (RIGHTOF11-ROBOT)))

(:derived (Flag182-)(and (RIGHTOF10-ROBOT)))

(:derived (Flag182-)(and (RIGHTOF4-ROBOT)))

(:derived (Flag182-)(and (RIGHTOF8-ROBOT)))

(:derived (Flag182-)(and (RIGHTOF12-ROBOT)))

(:derived (Flag182-)(and (RIGHTOF7-ROBOT)))

(:derived (Flag182-)(and (RIGHTOF9-ROBOT)))

(:derived (Flag182-)(and (RIGHTOF6-ROBOT)))

(:derived (Flag183-)(and (RIGHTOF5-ROBOT)))

(:derived (Flag183-)(and (RIGHTOF14-ROBOT)))

(:derived (Flag183-)(and (RIGHTOF13-ROBOT)))

(:derived (Flag183-)(and (RIGHTOF11-ROBOT)))

(:derived (Flag183-)(and (RIGHTOF10-ROBOT)))

(:derived (Flag183-)(and (RIGHTOF8-ROBOT)))

(:derived (Flag183-)(and (RIGHTOF12-ROBOT)))

(:derived (Flag183-)(and (RIGHTOF7-ROBOT)))

(:derived (Flag183-)(and (RIGHTOF9-ROBOT)))

(:derived (Flag183-)(and (RIGHTOF6-ROBOT)))

(:derived (Flag184-)(and (RIGHTOF14-ROBOT)))

(:derived (Flag184-)(and (RIGHTOF13-ROBOT)))

(:derived (Flag184-)(and (RIGHTOF11-ROBOT)))

(:derived (Flag184-)(and (RIGHTOF10-ROBOT)))

(:derived (Flag184-)(and (RIGHTOF8-ROBOT)))

(:derived (Flag184-)(and (RIGHTOF12-ROBOT)))

(:derived (Flag184-)(and (RIGHTOF7-ROBOT)))

(:derived (Flag184-)(and (RIGHTOF9-ROBOT)))

(:derived (Flag184-)(and (RIGHTOF6-ROBOT)))

(:derived (Flag185-)(and (RIGHTOF14-ROBOT)))

(:derived (Flag185-)(and (RIGHTOF13-ROBOT)))

(:derived (Flag185-)(and (RIGHTOF11-ROBOT)))

(:derived (Flag185-)(and (RIGHTOF10-ROBOT)))

(:derived (Flag185-)(and (RIGHTOF8-ROBOT)))

(:derived (Flag185-)(and (RIGHTOF7-ROBOT)))

(:derived (Flag185-)(and (RIGHTOF9-ROBOT)))

(:derived (Flag185-)(and (RIGHTOF12-ROBOT)))

(:derived (Flag186-)(and (RIGHTOF14-ROBOT)))

(:derived (Flag186-)(and (RIGHTOF13-ROBOT)))

(:derived (Flag186-)(and (RIGHTOF11-ROBOT)))

(:derived (Flag186-)(and (RIGHTOF10-ROBOT)))

(:derived (Flag186-)(and (RIGHTOF8-ROBOT)))

(:derived (Flag186-)(and (RIGHTOF9-ROBOT)))

(:derived (Flag186-)(and (RIGHTOF12-ROBOT)))

(:derived (Flag187-)(and (RIGHTOF14-ROBOT)))

(:derived (Flag187-)(and (RIGHTOF13-ROBOT)))

(:derived (Flag187-)(and (RIGHTOF11-ROBOT)))

(:derived (Flag187-)(and (RIGHTOF10-ROBOT)))

(:derived (Flag187-)(and (RIGHTOF9-ROBOT)))

(:derived (Flag187-)(and (RIGHTOF12-ROBOT)))

(:derived (Flag188-)(and (RIGHTOF10-ROBOT)))

(:derived (Flag188-)(and (RIGHTOF14-ROBOT)))

(:derived (Flag188-)(and (RIGHTOF13-ROBOT)))

(:derived (Flag188-)(and (RIGHTOF12-ROBOT)))

(:derived (Flag188-)(and (RIGHTOF11-ROBOT)))

(:derived (Flag189-)(and (RIGHTOF14-ROBOT)))

(:derived (Flag189-)(and (RIGHTOF13-ROBOT)))

(:derived (Flag189-)(and (RIGHTOF12-ROBOT)))

(:derived (Flag189-)(and (RIGHTOF11-ROBOT)))

(:derived (Flag190-)(and (RIGHTOF14-ROBOT)))

(:derived (Flag190-)(and (RIGHTOF13-ROBOT)))

(:derived (Flag190-)(and (RIGHTOF12-ROBOT)))

(:derived (Flag191-)(and (RIGHTOF14-ROBOT)))

(:derived (Flag191-)(and (RIGHTOF13-ROBOT)))

(:derived (Flag192-)(and (LEFTOF1-ROBOT)))

(:derived (Flag192-)(and (LEFTOF2-ROBOT)))

(:derived (Flag193-)(and (LEFTOF1-ROBOT)))

(:derived (Flag193-)(and (LEFTOF3-ROBOT)))

(:derived (Flag193-)(and (LEFTOF2-ROBOT)))

(:derived (Flag194-)(and (LEFTOF1-ROBOT)))

(:derived (Flag194-)(and (LEFTOF3-ROBOT)))

(:derived (Flag194-)(and (LEFTOF4-ROBOT)))

(:derived (Flag194-)(and (LEFTOF2-ROBOT)))

(:derived (Flag195-)(and (LEFTOF5-ROBOT)))

(:derived (Flag195-)(and (LEFTOF1-ROBOT)))

(:derived (Flag195-)(and (LEFTOF3-ROBOT)))

(:derived (Flag195-)(and (LEFTOF4-ROBOT)))

(:derived (Flag195-)(and (LEFTOF2-ROBOT)))

(:derived (Flag196-)(and (LEFTOF2-ROBOT)))

(:derived (Flag196-)(and (LEFTOF1-ROBOT)))

(:derived (Flag196-)(and (LEFTOF6-ROBOT)))

(:derived (Flag196-)(and (LEFTOF5-ROBOT)))

(:derived (Flag196-)(and (LEFTOF3-ROBOT)))

(:derived (Flag196-)(and (LEFTOF4-ROBOT)))

(:derived (Flag197-)(and (LEFTOF2-ROBOT)))

(:derived (Flag197-)(and (LEFTOF7-ROBOT)))

(:derived (Flag197-)(and (LEFTOF1-ROBOT)))

(:derived (Flag197-)(and (LEFTOF6-ROBOT)))

(:derived (Flag197-)(and (LEFTOF5-ROBOT)))

(:derived (Flag197-)(and (LEFTOF3-ROBOT)))

(:derived (Flag197-)(and (LEFTOF4-ROBOT)))

(:derived (Flag198-)(and (LEFTOF2-ROBOT)))

(:derived (Flag198-)(and (LEFTOF7-ROBOT)))

(:derived (Flag198-)(and (LEFTOF1-ROBOT)))

(:derived (Flag198-)(and (LEFTOF6-ROBOT)))

(:derived (Flag198-)(and (LEFTOF5-ROBOT)))

(:derived (Flag198-)(and (LEFTOF3-ROBOT)))

(:derived (Flag198-)(and (LEFTOF4-ROBOT)))

(:derived (Flag198-)(and (LEFTOF8-ROBOT)))

(:derived (Flag199-)(and (LEFTOF9-ROBOT)))

(:derived (Flag199-)(and (LEFTOF2-ROBOT)))

(:derived (Flag199-)(and (LEFTOF7-ROBOT)))

(:derived (Flag199-)(and (LEFTOF1-ROBOT)))

(:derived (Flag199-)(and (LEFTOF6-ROBOT)))

(:derived (Flag199-)(and (LEFTOF5-ROBOT)))

(:derived (Flag199-)(and (LEFTOF3-ROBOT)))

(:derived (Flag199-)(and (LEFTOF4-ROBOT)))

(:derived (Flag199-)(and (LEFTOF8-ROBOT)))

(:derived (Flag200-)(and (LEFTOF10-ROBOT)))

(:derived (Flag200-)(and (LEFTOF9-ROBOT)))

(:derived (Flag200-)(and (LEFTOF2-ROBOT)))

(:derived (Flag200-)(and (LEFTOF7-ROBOT)))

(:derived (Flag200-)(and (LEFTOF1-ROBOT)))

(:derived (Flag200-)(and (LEFTOF6-ROBOT)))

(:derived (Flag200-)(and (LEFTOF5-ROBOT)))

(:derived (Flag200-)(and (LEFTOF3-ROBOT)))

(:derived (Flag200-)(and (LEFTOF4-ROBOT)))

(:derived (Flag200-)(and (LEFTOF8-ROBOT)))

(:derived (Flag201-)(and (LEFTOF10-ROBOT)))

(:derived (Flag201-)(and (LEFTOF11-ROBOT)))

(:derived (Flag201-)(and (LEFTOF9-ROBOT)))

(:derived (Flag201-)(and (LEFTOF2-ROBOT)))

(:derived (Flag201-)(and (LEFTOF7-ROBOT)))

(:derived (Flag201-)(and (LEFTOF1-ROBOT)))

(:derived (Flag201-)(and (LEFTOF6-ROBOT)))

(:derived (Flag201-)(and (LEFTOF5-ROBOT)))

(:derived (Flag201-)(and (LEFTOF3-ROBOT)))

(:derived (Flag201-)(and (LEFTOF4-ROBOT)))

(:derived (Flag201-)(and (LEFTOF8-ROBOT)))

(:derived (Flag202-)(and (LEFTOF10-ROBOT)))

(:derived (Flag202-)(and (LEFTOF11-ROBOT)))

(:derived (Flag202-)(and (LEFTOF9-ROBOT)))

(:derived (Flag202-)(and (LEFTOF2-ROBOT)))

(:derived (Flag202-)(and (LEFTOF7-ROBOT)))

(:derived (Flag202-)(and (LEFTOF1-ROBOT)))

(:derived (Flag202-)(and (LEFTOF12-ROBOT)))

(:derived (Flag202-)(and (LEFTOF6-ROBOT)))

(:derived (Flag202-)(and (LEFTOF5-ROBOT)))

(:derived (Flag202-)(and (LEFTOF3-ROBOT)))

(:derived (Flag202-)(and (LEFTOF4-ROBOT)))

(:derived (Flag202-)(and (LEFTOF8-ROBOT)))

(:derived (Flag203-)(and (LEFTOF10-ROBOT)))

(:derived (Flag203-)(and (LEFTOF11-ROBOT)))

(:derived (Flag203-)(and (LEFTOF9-ROBOT)))

(:derived (Flag203-)(and (LEFTOF2-ROBOT)))

(:derived (Flag203-)(and (LEFTOF13-ROBOT)))

(:derived (Flag203-)(and (LEFTOF1-ROBOT)))

(:derived (Flag203-)(and (LEFTOF12-ROBOT)))

(:derived (Flag203-)(and (LEFTOF6-ROBOT)))

(:derived (Flag203-)(and (LEFTOF5-ROBOT)))

(:derived (Flag203-)(and (LEFTOF3-ROBOT)))

(:derived (Flag203-)(and (LEFTOF4-ROBOT)))

(:derived (Flag203-)(and (LEFTOF7-ROBOT)))

(:derived (Flag203-)(and (LEFTOF8-ROBOT)))

(:derived (Flag204-)(and (LEFTOF10-ROBOT)))

(:derived (Flag204-)(and (LEFTOF11-ROBOT)))

(:derived (Flag204-)(and (LEFTOF9-ROBOT)))

(:derived (Flag204-)(and (LEFTOF2-ROBOT)))

(:derived (Flag204-)(and (LEFTOF13-ROBOT)))

(:derived (Flag204-)(and (LEFTOF1-ROBOT)))

(:derived (Flag204-)(and (LEFTOF12-ROBOT)))

(:derived (Flag204-)(and (LEFTOF6-ROBOT)))

(:derived (Flag204-)(and (LEFTOF5-ROBOT)))

(:derived (Flag204-)(and (LEFTOF3-ROBOT)))

(:derived (Flag204-)(and (LEFTOF4-ROBOT)))

(:derived (Flag204-)(and (LEFTOF7-ROBOT)))

(:derived (Flag204-)(and (LEFTOF8-ROBOT)))

(:derived (Flag205-)(and (LEFTOF1-ROBOT)(RIGHTOF4-ROBOT)))

(:derived (Flag206-)(and (LEFTOF1-ROBOT)(RIGHTOF12-ROBOT)))

(:derived (Flag207-)(and (LEFTOF1-ROBOT)(RIGHTOF7-ROBOT)))

(:derived (Flag208-)(and (LEFTOF1-ROBOT)(RIGHTOF9-ROBOT)))

(:derived (Flag209-)(and (RIGHTOF13-ROBOT)(LEFTOF1-ROBOT)))

(:derived (Flag210-)(and (LEFTOF1-ROBOT)(RIGHTOF1-ROBOT)))

(:derived (Flag211-)(and (RIGHTOF3-ROBOT)(LEFTOF1-ROBOT)))

(:derived (Flag212-)(and (RIGHTOF0-ROBOT)(LEFTOF1-ROBOT)))

(:derived (Flag213-)(and (LEFTOF1-ROBOT)(RIGHTOF5-ROBOT)))

(:derived (Flag214-)(and (LEFTOF1-ROBOT)(RIGHTOF2-ROBOT)))

(:derived (Flag215-)(and (RIGHTOF11-ROBOT)(LEFTOF1-ROBOT)))

(:derived (Flag216-)(and (RIGHTOF8-ROBOT)(LEFTOF1-ROBOT)))

(:derived (Flag217-)(and (LEFTOF1-ROBOT)(RIGHTOF6-ROBOT)))

(:derived (Flag218-)(and (LEFTOF1-ROBOT)(RIGHTOF14-ROBOT)))

(:derived (Flag219-)(and (RIGHTOF10-ROBOT)(LEFTOF1-ROBOT)))

(:derived (Flag221-)(and (RIGHTOF13-ROBOT)(LEFTOF2-ROBOT)))

(:derived (Flag222-)(and (LEFTOF2-ROBOT)(RIGHTOF9-ROBOT)))

(:derived (Flag223-)(and (LEFTOF2-ROBOT)(RIGHTOF1-ROBOT)))

(:derived (Flag224-)(and (RIGHTOF5-ROBOT)(LEFTOF2-ROBOT)))

(:derived (Flag225-)(and (LEFTOF2-ROBOT)(RIGHTOF7-ROBOT)))

(:derived (Flag226-)(and (RIGHTOF5-ROBOT)(LEFTOF1-ROBOT)))

(:derived (Flag227-)(and (RIGHTOF3-ROBOT)(LEFTOF2-ROBOT)))

(:derived (Flag228-)(and (LEFTOF2-ROBOT)(RIGHTOF14-ROBOT)))

(:derived (Flag229-)(and (RIGHTOF10-ROBOT)(LEFTOF2-ROBOT)))

(:derived (Flag230-)(and (LEFTOF2-ROBOT)(RIGHTOF12-ROBOT)))

(:derived (Flag231-)(and (RIGHTOF11-ROBOT)(LEFTOF2-ROBOT)))

(:derived (Flag232-)(and (RIGHTOF8-ROBOT)(LEFTOF2-ROBOT)))

(:derived (Flag233-)(and (RIGHTOF2-ROBOT)(LEFTOF2-ROBOT)))

(:derived (Flag234-)(and (LEFTOF2-ROBOT)(RIGHTOF6-ROBOT)))

(:derived (Flag235-)(and (LEFTOF2-ROBOT)(RIGHTOF4-ROBOT)))

(:derived (Flag237-)(and (LEFTOF3-ROBOT)(RIGHTOF6-ROBOT)))

(:derived (Flag238-)(and (LEFTOF3-ROBOT)(RIGHTOF14-ROBOT)))

(:derived (Flag239-)(and (LEFTOF3-ROBOT)(RIGHTOF4-ROBOT)))

(:derived (Flag240-)(and (LEFTOF3-ROBOT)(RIGHTOF7-ROBOT)))

(:derived (Flag241-)(and (LEFTOF3-ROBOT)(RIGHTOF12-ROBOT)))

(:derived (Flag242-)(and (LEFTOF3-ROBOT)(RIGHTOF10-ROBOT)))

(:derived (Flag243-)(and (LEFTOF3-ROBOT)(RIGHTOF8-ROBOT)))

(:derived (Flag244-)(and (LEFTOF3-ROBOT)(RIGHTOF5-ROBOT)))

(:derived (Flag245-)(and (RIGHTOF3-ROBOT)(LEFTOF3-ROBOT)))

(:derived (Flag246-)(and (LEFTOF3-ROBOT)(RIGHTOF11-ROBOT)))

(:derived (Flag247-)(and (RIGHTOF13-ROBOT)(LEFTOF3-ROBOT)))

(:derived (Flag248-)(and (LEFTOF3-ROBOT)(RIGHTOF9-ROBOT)))

(:derived (Flag249-)(and (LEFTOF3-ROBOT)(RIGHTOF2-ROBOT)))

(:derived (Flag251-)(and (LEFTOF3-ROBOT)(RIGHTOF13-ROBOT)))

(:derived (Flag252-)(and (LEFTOF4-ROBOT)(RIGHTOF4-ROBOT)))

(:derived (Flag253-)(and (LEFTOF4-ROBOT)(RIGHTOF7-ROBOT)))

(:derived (Flag254-)(and (LEFTOF4-ROBOT)(RIGHTOF14-ROBOT)))

(:derived (Flag255-)(and (LEFTOF3-ROBOT)(RIGHTOF3-ROBOT)))

(:derived (Flag256-)(and (RIGHTOF8-ROBOT)(LEFTOF4-ROBOT)))

(:derived (Flag257-)(and (LEFTOF4-ROBOT)(RIGHTOF12-ROBOT)))

(:derived (Flag258-)(and (RIGHTOF10-ROBOT)(LEFTOF4-ROBOT)))

(:derived (Flag259-)(and (LEFTOF4-ROBOT)(RIGHTOF9-ROBOT)))

(:derived (Flag260-)(and (LEFTOF4-ROBOT)(RIGHTOF6-ROBOT)))

(:derived (Flag261-)(and (LEFTOF4-ROBOT)(RIGHTOF5-ROBOT)))

(:derived (Flag262-)(and (RIGHTOF13-ROBOT)(LEFTOF4-ROBOT)))

(:derived (Flag263-)(and (RIGHTOF3-ROBOT)(LEFTOF4-ROBOT)))

(:derived (Flag264-)(and (LEFTOF4-ROBOT)(RIGHTOF11-ROBOT)))

(:derived (Flag266-)(and (RIGHTOF9-ROBOT)(LEFTOF5-ROBOT)))

(:derived (Flag267-)(and (RIGHTOF7-ROBOT)(LEFTOF5-ROBOT)))

(:derived (Flag268-)(and (RIGHTOF11-ROBOT)(LEFTOF5-ROBOT)))

(:derived (Flag269-)(and (RIGHTOF13-ROBOT)(LEFTOF5-ROBOT)))

(:derived (Flag270-)(and (RIGHTOF4-ROBOT)(LEFTOF5-ROBOT)))

(:derived (Flag271-)(and (LEFTOF4-ROBOT)(RIGHTOF8-ROBOT)))

(:derived (Flag272-)(and (LEFTOF5-ROBOT)(RIGHTOF6-ROBOT)))

(:derived (Flag273-)(and (RIGHTOF10-ROBOT)(LEFTOF5-ROBOT)))

(:derived (Flag274-)(and (LEFTOF4-ROBOT)(RIGHTOF10-ROBOT)))

(:derived (Flag275-)(and (LEFTOF5-ROBOT)(RIGHTOF12-ROBOT)))

(:derived (Flag276-)(and (RIGHTOF8-ROBOT)(LEFTOF5-ROBOT)))

(:derived (Flag277-)(and (RIGHTOF14-ROBOT)(LEFTOF5-ROBOT)))

(:derived (Flag278-)(and (RIGHTOF5-ROBOT)(LEFTOF5-ROBOT)))

(:derived (Flag280-)(and (RIGHTOF12-ROBOT)(LEFTOF6-ROBOT)))

(:derived (Flag281-)(and (RIGHTOF8-ROBOT)(LEFTOF6-ROBOT)))

(:derived (Flag282-)(and (RIGHTOF11-ROBOT)(LEFTOF6-ROBOT)))

(:derived (Flag283-)(and (RIGHTOF5-ROBOT)(LEFTOF6-ROBOT)))

(:derived (Flag284-)(and (RIGHTOF7-ROBOT)(LEFTOF6-ROBOT)))

(:derived (Flag285-)(and (RIGHTOF10-ROBOT)(LEFTOF6-ROBOT)))

(:derived (Flag286-)(and (RIGHTOF6-ROBOT)(LEFTOF6-ROBOT)))

(:derived (Flag287-)(and (RIGHTOF13-ROBOT)(LEFTOF6-ROBOT)))

(:derived (Flag288-)(and (RIGHTOF14-ROBOT)(LEFTOF6-ROBOT)))

(:derived (Flag289-)(and (RIGHTOF9-ROBOT)(LEFTOF6-ROBOT)))

(:derived (Flag291-)(and (RIGHTOF11-ROBOT)(LEFTOF7-ROBOT)))

(:derived (Flag292-)(and (RIGHTOF8-ROBOT)(LEFTOF7-ROBOT)))

(:derived (Flag293-)(and (LEFTOF7-ROBOT)(RIGHTOF7-ROBOT)))

(:derived (Flag294-)(and (RIGHTOF13-ROBOT)(LEFTOF7-ROBOT)))

(:derived (Flag295-)(and (LEFTOF6-ROBOT)(RIGHTOF6-ROBOT)))

(:derived (Flag296-)(and (LEFTOF7-ROBOT)(RIGHTOF12-ROBOT)))

(:derived (Flag297-)(and (LEFTOF7-ROBOT)(RIGHTOF14-ROBOT)))

(:derived (Flag298-)(and (LEFTOF7-ROBOT)(RIGHTOF9-ROBOT)))

(:derived (Flag299-)(and (LEFTOF7-ROBOT)(RIGHTOF6-ROBOT)))

(:derived (Flag300-)(and (LEFTOF6-ROBOT)(RIGHTOF12-ROBOT)))

(:derived (Flag301-)(and (RIGHTOF10-ROBOT)(LEFTOF7-ROBOT)))

(:derived (Flag303-)(and (RIGHTOF7-ROBOT)(LEFTOF8-ROBOT)))

(:derived (Flag304-)(and (LEFTOF8-ROBOT)(RIGHTOF9-ROBOT)))

(:derived (Flag305-)(and (RIGHTOF13-ROBOT)(LEFTOF8-ROBOT)))

(:derived (Flag306-)(and (RIGHTOF14-ROBOT)(LEFTOF8-ROBOT)))

(:derived (Flag307-)(and (RIGHTOF10-ROBOT)(LEFTOF8-ROBOT)))

(:derived (Flag308-)(and (RIGHTOF8-ROBOT)(LEFTOF8-ROBOT)))

(:derived (Flag309-)(and (RIGHTOF7-ROBOT)(LEFTOF7-ROBOT)))

(:derived (Flag310-)(and (LEFTOF8-ROBOT)(RIGHTOF12-ROBOT)))

(:derived (Flag311-)(and (RIGHTOF11-ROBOT)(LEFTOF8-ROBOT)))

(:derived (Flag313-)(and (RIGHTOF13-ROBOT)(LEFTOF9-ROBOT)))

(:derived (Flag314-)(and (RIGHTOF8-ROBOT)(LEFTOF9-ROBOT)))

(:derived (Flag315-)(and (LEFTOF9-ROBOT)(RIGHTOF9-ROBOT)))

(:derived (Flag316-)(and (RIGHTOF10-ROBOT)(LEFTOF9-ROBOT)))

(:derived (Flag317-)(and (LEFTOF8-ROBOT)(RIGHTOF14-ROBOT)))

(:derived (Flag318-)(and (LEFTOF9-ROBOT)(RIGHTOF12-ROBOT)))

(:derived (Flag319-)(and (RIGHTOF11-ROBOT)(LEFTOF9-ROBOT)))

(:derived (Flag320-)(and (LEFTOF9-ROBOT)(RIGHTOF14-ROBOT)))

(:derived (Flag322-)(and (RIGHTOF14-ROBOT)(LEFTOF10-ROBOT)))

(:derived (Flag323-)(and (LEFTOF10-ROBOT)(RIGHTOF12-ROBOT)))

(:derived (Flag324-)(and (RIGHTOF11-ROBOT)(LEFTOF10-ROBOT)))

(:derived (Flag325-)(and (RIGHTOF10-ROBOT)(LEFTOF10-ROBOT)))

(:derived (Flag326-)(and (RIGHTOF13-ROBOT)(LEFTOF10-ROBOT)))

(:derived (Flag327-)(and (LEFTOF10-ROBOT)(RIGHTOF9-ROBOT)))

(:derived (Flag329-)(and (LEFTOF11-ROBOT)(RIGHTOF14-ROBOT)))

(:derived (Flag330-)(and (RIGHTOF11-ROBOT)(LEFTOF11-ROBOT)))

(:derived (Flag331-)(and (LEFTOF10-ROBOT)(RIGHTOF14-ROBOT)))

(:derived (Flag332-)(and (RIGHTOF13-ROBOT)(LEFTOF11-ROBOT)))

(:derived (Flag333-)(and (RIGHTOF10-ROBOT)(LEFTOF11-ROBOT)))

(:derived (Flag334-)(and (LEFTOF11-ROBOT)(RIGHTOF12-ROBOT)))

(:derived (Flag336-)(and (RIGHTOF12-ROBOT)(LEFTOF12-ROBOT)))

(:derived (Flag337-)(and (RIGHTOF14-ROBOT)(LEFTOF12-ROBOT)))

(:derived (Flag338-)(and (RIGHTOF11-ROBOT)(LEFTOF12-ROBOT)))

(:derived (Flag339-)(and (RIGHTOF13-ROBOT)(LEFTOF12-ROBOT)))

(:derived (Flag341-)(and (LEFTOF13-ROBOT)(RIGHTOF12-ROBOT)))

(:derived (Flag342-)(and (LEFTOF13-ROBOT)(RIGHTOF14-ROBOT)))

(:derived (Flag343-)(and (RIGHTOF13-ROBOT)(LEFTOF13-ROBOT)))

(:derived (Flag344-)(and (LEFTOF12-ROBOT)(RIGHTOF12-ROBOT)))

(:derived (Flag346-)(and (LEFTOF14-ROBOT)(RIGHTOF14-ROBOT)))

(:derived (Flag347-)(and (RIGHTOF13-ROBOT)(LEFTOF14-ROBOT)))

(:derived (Flag349-)(and (LEFTOF15-ROBOT)))

(:derived (Flag349-)(and (LEFTOF10-ROBOT)))

(:derived (Flag349-)(and (LEFTOF11-ROBOT)))

(:derived (Flag349-)(and (LEFTOF9-ROBOT)))

(:derived (Flag349-)(and (LEFTOF2-ROBOT)))

(:derived (Flag349-)(and (LEFTOF13-ROBOT)))

(:derived (Flag349-)(and (LEFTOF1-ROBOT)))

(:derived (Flag349-)(and (LEFTOF12-ROBOT)))

(:derived (Flag349-)(and (LEFTOF6-ROBOT)))

(:derived (Flag349-)(and (LEFTOF5-ROBOT)))

(:derived (Flag349-)(and (LEFTOF3-ROBOT)))

(:derived (Flag349-)(and (LEFTOF4-ROBOT)))

(:derived (Flag349-)(and (LEFTOF7-ROBOT)))

(:derived (Flag349-)(and (LEFTOF8-ROBOT)))

(:derived (Flag350-)(and (RIGHTOF14-ROBOT)(LEFTOF15-ROBOT)))

(:derived (Flag352-)(and (Flag54-)))

(:derived (Flag352-)(and (Flag43-)))

(:derived (Flag352-)(and (Flag38-)))

(:derived (Flag352-)(and (Flag56-)))

(:derived (Flag352-)(and (Flag45-)))

(:derived (Flag352-)(and (Flag37-)))

(:derived (Flag352-)(and (Flag33-)))

(:derived (Flag352-)(and (Flag46-)))

(:derived (Flag352-)(and (Flag39-)))

(:derived (Flag352-)(and (Flag47-)))

(:derived (Flag352-)(and (Flag48-)))

(:derived (Flag352-)(and (Flag58-)))

(:derived (Flag352-)(and (Flag50-)))

(:derived (Flag352-)(and (Flag51-)))

(:action MOVELEFT-ROBOT
:parameters ()
:precondition
(and
(Flag4-)
)
:effect
(and
(when
(and
(Flag351-)
)
(and
(COLUMN13-ROBOT)
(not (NOT-COLUMN13-ROBOT))
)
)
(when
(and
(Flag348-)
)
(and
(COLUMN12-ROBOT)
(not (NOT-COLUMN12-ROBOT))
)
)
(when
(and
(Flag345-)
)
(and
(COLUMN11-ROBOT)
(not (NOT-COLUMN11-ROBOT))
)
)
(when
(and
(Flag340-)
)
(and
(COLUMN10-ROBOT)
(not (NOT-COLUMN10-ROBOT))
)
)
(when
(and
(Flag335-)
)
(and
(COLUMN9-ROBOT)
(not (NOT-COLUMN9-ROBOT))
)
)
(when
(and
(Flag328-)
)
(and
(COLUMN8-ROBOT)
(not (NOT-COLUMN8-ROBOT))
)
)
(when
(and
(Flag321-)
)
(and
(COLUMN7-ROBOT)
(not (NOT-COLUMN7-ROBOT))
)
)
(when
(and
(Flag312-)
)
(and
(COLUMN6-ROBOT)
(not (NOT-COLUMN6-ROBOT))
)
)
(when
(and
(Flag302-)
)
(and
(COLUMN5-ROBOT)
(not (NOT-COLUMN5-ROBOT))
)
)
(when
(and
(Flag290-)
)
(and
(COLUMN4-ROBOT)
(not (NOT-COLUMN4-ROBOT))
)
)
(when
(and
(Flag279-)
)
(and
(COLUMN3-ROBOT)
(not (NOT-COLUMN3-ROBOT))
)
)
(when
(and
(Flag265-)
)
(and
(COLUMN2-ROBOT)
(not (NOT-COLUMN2-ROBOT))
)
)
(when
(and
(Flag250-)
)
(and
(COLUMN1-ROBOT)
(not (NOT-COLUMN1-ROBOT))
)
)
(when
(and
(Flag236-)
)
(and
(COLUMN0-ROBOT)
(not (NOT-COLUMN0-ROBOT))
)
)
(when
(and
(COLUMN14-ROBOT)
)
(and
(COLUMN13-ROBOT)
(NOT-COLUMN14-ROBOT)
(not (NOT-COLUMN13-ROBOT))
(not (COLUMN14-ROBOT))
)
)
(when
(and
(COLUMN13-ROBOT)
)
(and
(COLUMN12-ROBOT)
(NOT-COLUMN13-ROBOT)
(not (NOT-COLUMN12-ROBOT))
(not (COLUMN13-ROBOT))
)
)
(when
(and
(COLUMN12-ROBOT)
)
(and
(COLUMN11-ROBOT)
(NOT-COLUMN12-ROBOT)
(not (NOT-COLUMN11-ROBOT))
(not (COLUMN12-ROBOT))
)
)
(when
(and
(COLUMN11-ROBOT)
)
(and
(COLUMN10-ROBOT)
(NOT-COLUMN11-ROBOT)
(not (NOT-COLUMN10-ROBOT))
(not (COLUMN11-ROBOT))
)
)
(when
(and
(COLUMN10-ROBOT)
)
(and
(COLUMN9-ROBOT)
(NOT-COLUMN10-ROBOT)
(not (NOT-COLUMN9-ROBOT))
(not (COLUMN10-ROBOT))
)
)
(when
(and
(COLUMN9-ROBOT)
)
(and
(COLUMN8-ROBOT)
(NOT-COLUMN9-ROBOT)
(not (NOT-COLUMN8-ROBOT))
(not (COLUMN9-ROBOT))
)
)
(when
(and
(COLUMN8-ROBOT)
)
(and
(COLUMN7-ROBOT)
(NOT-COLUMN8-ROBOT)
(not (NOT-COLUMN7-ROBOT))
(not (COLUMN8-ROBOT))
)
)
(when
(and
(COLUMN7-ROBOT)
)
(and
(COLUMN6-ROBOT)
(NOT-COLUMN7-ROBOT)
(not (NOT-COLUMN6-ROBOT))
(not (COLUMN7-ROBOT))
)
)
(when
(and
(COLUMN6-ROBOT)
)
(and
(COLUMN5-ROBOT)
(NOT-COLUMN6-ROBOT)
(not (NOT-COLUMN5-ROBOT))
(not (COLUMN6-ROBOT))
)
)
(when
(and
(COLUMN5-ROBOT)
)
(and
(COLUMN4-ROBOT)
(NOT-COLUMN5-ROBOT)
(not (NOT-COLUMN4-ROBOT))
(not (COLUMN5-ROBOT))
)
)
(when
(and
(COLUMN4-ROBOT)
)
(and
(COLUMN3-ROBOT)
(NOT-COLUMN4-ROBOT)
(not (NOT-COLUMN3-ROBOT))
(not (COLUMN4-ROBOT))
)
)
(when
(and
(COLUMN3-ROBOT)
)
(and
(COLUMN2-ROBOT)
(NOT-COLUMN3-ROBOT)
(not (NOT-COLUMN2-ROBOT))
(not (COLUMN3-ROBOT))
)
)
(when
(and
(COLUMN2-ROBOT)
)
(and
(COLUMN1-ROBOT)
(NOT-COLUMN2-ROBOT)
(not (NOT-COLUMN1-ROBOT))
(not (COLUMN2-ROBOT))
)
)
(when
(and
(COLUMN1-ROBOT)
)
(and
(COLUMN0-ROBOT)
(NOT-COLUMN1-ROBOT)
(not (NOT-COLUMN0-ROBOT))
(not (COLUMN1-ROBOT))
)
)
(when
(and
(RIGHTOF14-ROBOT)
)
(and
(RIGHTOF13-ROBOT)
(NOT-RIGHTOF14-ROBOT)
(RIGHTOF12-ROBOT)
(NOT-RIGHTOF13-ROBOT)
(RIGHTOF11-ROBOT)
(NOT-RIGHTOF12-ROBOT)
(RIGHTOF10-ROBOT)
(NOT-RIGHTOF11-ROBOT)
(RIGHTOF9-ROBOT)
(NOT-RIGHTOF10-ROBOT)
(RIGHTOF8-ROBOT)
(NOT-RIGHTOF9-ROBOT)
(RIGHTOF7-ROBOT)
(NOT-RIGHTOF8-ROBOT)
(RIGHTOF6-ROBOT)
(NOT-RIGHTOF7-ROBOT)
(RIGHTOF5-ROBOT)
(NOT-RIGHTOF6-ROBOT)
(RIGHTOF4-ROBOT)
(NOT-RIGHTOF5-ROBOT)
(RIGHTOF3-ROBOT)
(NOT-RIGHTOF4-ROBOT)
(RIGHTOF2-ROBOT)
(NOT-RIGHTOF3-ROBOT)
(RIGHTOF1-ROBOT)
(NOT-RIGHTOF2-ROBOT)
(RIGHTOF0-ROBOT)
(NOT-RIGHTOF1-ROBOT)
(not (RIGHTOF14-ROBOT))
(not (NOT-RIGHTOF0-ROBOT))
)
)
(when
(and
(RIGHTOF13-ROBOT)
)
(and
(RIGHTOF12-ROBOT)
(NOT-RIGHTOF13-ROBOT)
(RIGHTOF11-ROBOT)
(NOT-RIGHTOF12-ROBOT)
(RIGHTOF10-ROBOT)
(NOT-RIGHTOF11-ROBOT)
(RIGHTOF9-ROBOT)
(NOT-RIGHTOF10-ROBOT)
(RIGHTOF8-ROBOT)
(NOT-RIGHTOF9-ROBOT)
(RIGHTOF7-ROBOT)
(NOT-RIGHTOF8-ROBOT)
(RIGHTOF6-ROBOT)
(NOT-RIGHTOF7-ROBOT)
(RIGHTOF5-ROBOT)
(NOT-RIGHTOF6-ROBOT)
(RIGHTOF4-ROBOT)
(NOT-RIGHTOF5-ROBOT)
(RIGHTOF3-ROBOT)
(NOT-RIGHTOF4-ROBOT)
(RIGHTOF2-ROBOT)
(NOT-RIGHTOF3-ROBOT)
(RIGHTOF1-ROBOT)
(NOT-RIGHTOF2-ROBOT)
(RIGHTOF0-ROBOT)
(NOT-RIGHTOF1-ROBOT)
(not (RIGHTOF13-ROBOT))
(not (NOT-RIGHTOF0-ROBOT))
)
)
(when
(and
(RIGHTOF12-ROBOT)
)
(and
(RIGHTOF11-ROBOT)
(NOT-RIGHTOF12-ROBOT)
(RIGHTOF10-ROBOT)
(NOT-RIGHTOF11-ROBOT)
(RIGHTOF9-ROBOT)
(NOT-RIGHTOF10-ROBOT)
(RIGHTOF8-ROBOT)
(NOT-RIGHTOF9-ROBOT)
(RIGHTOF7-ROBOT)
(NOT-RIGHTOF8-ROBOT)
(RIGHTOF6-ROBOT)
(NOT-RIGHTOF7-ROBOT)
(RIGHTOF5-ROBOT)
(NOT-RIGHTOF6-ROBOT)
(RIGHTOF4-ROBOT)
(NOT-RIGHTOF5-ROBOT)
(RIGHTOF3-ROBOT)
(NOT-RIGHTOF4-ROBOT)
(RIGHTOF2-ROBOT)
(NOT-RIGHTOF3-ROBOT)
(RIGHTOF1-ROBOT)
(NOT-RIGHTOF2-ROBOT)
(RIGHTOF0-ROBOT)
(NOT-RIGHTOF1-ROBOT)
(not (RIGHTOF12-ROBOT))
(not (NOT-RIGHTOF0-ROBOT))
)
)
(when
(and
(RIGHTOF11-ROBOT)
)
(and
(RIGHTOF10-ROBOT)
(NOT-RIGHTOF11-ROBOT)
(RIGHTOF9-ROBOT)
(NOT-RIGHTOF10-ROBOT)
(RIGHTOF8-ROBOT)
(NOT-RIGHTOF9-ROBOT)
(RIGHTOF7-ROBOT)
(NOT-RIGHTOF8-ROBOT)
(RIGHTOF6-ROBOT)
(NOT-RIGHTOF7-ROBOT)
(RIGHTOF5-ROBOT)
(NOT-RIGHTOF6-ROBOT)
(RIGHTOF4-ROBOT)
(NOT-RIGHTOF5-ROBOT)
(RIGHTOF3-ROBOT)
(NOT-RIGHTOF4-ROBOT)
(RIGHTOF2-ROBOT)
(NOT-RIGHTOF3-ROBOT)
(RIGHTOF1-ROBOT)
(NOT-RIGHTOF2-ROBOT)
(RIGHTOF0-ROBOT)
(NOT-RIGHTOF1-ROBOT)
(not (RIGHTOF11-ROBOT))
(not (NOT-RIGHTOF0-ROBOT))
)
)
(when
(and
(RIGHTOF10-ROBOT)
)
(and
(RIGHTOF9-ROBOT)
(NOT-RIGHTOF10-ROBOT)
(RIGHTOF8-ROBOT)
(NOT-RIGHTOF9-ROBOT)
(RIGHTOF7-ROBOT)
(NOT-RIGHTOF8-ROBOT)
(RIGHTOF6-ROBOT)
(NOT-RIGHTOF7-ROBOT)
(RIGHTOF5-ROBOT)
(NOT-RIGHTOF6-ROBOT)
(RIGHTOF4-ROBOT)
(NOT-RIGHTOF5-ROBOT)
(RIGHTOF3-ROBOT)
(NOT-RIGHTOF4-ROBOT)
(RIGHTOF2-ROBOT)
(NOT-RIGHTOF3-ROBOT)
(RIGHTOF1-ROBOT)
(NOT-RIGHTOF2-ROBOT)
(RIGHTOF0-ROBOT)
(NOT-RIGHTOF1-ROBOT)
(not (RIGHTOF10-ROBOT))
(not (NOT-RIGHTOF0-ROBOT))
)
)
(when
(and
(RIGHTOF9-ROBOT)
)
(and
(RIGHTOF8-ROBOT)
(NOT-RIGHTOF9-ROBOT)
(RIGHTOF7-ROBOT)
(NOT-RIGHTOF8-ROBOT)
(RIGHTOF6-ROBOT)
(NOT-RIGHTOF7-ROBOT)
(RIGHTOF5-ROBOT)
(NOT-RIGHTOF6-ROBOT)
(RIGHTOF4-ROBOT)
(NOT-RIGHTOF5-ROBOT)
(RIGHTOF3-ROBOT)
(NOT-RIGHTOF4-ROBOT)
(RIGHTOF2-ROBOT)
(NOT-RIGHTOF3-ROBOT)
(RIGHTOF1-ROBOT)
(NOT-RIGHTOF2-ROBOT)
(RIGHTOF0-ROBOT)
(NOT-RIGHTOF1-ROBOT)
(not (RIGHTOF9-ROBOT))
(not (NOT-RIGHTOF0-ROBOT))
)
)
(when
(and
(RIGHTOF8-ROBOT)
)
(and
(RIGHTOF7-ROBOT)
(NOT-RIGHTOF8-ROBOT)
(RIGHTOF6-ROBOT)
(NOT-RIGHTOF7-ROBOT)
(RIGHTOF5-ROBOT)
(NOT-RIGHTOF6-ROBOT)
(RIGHTOF4-ROBOT)
(NOT-RIGHTOF5-ROBOT)
(RIGHTOF3-ROBOT)
(NOT-RIGHTOF4-ROBOT)
(RIGHTOF2-ROBOT)
(NOT-RIGHTOF3-ROBOT)
(RIGHTOF1-ROBOT)
(NOT-RIGHTOF2-ROBOT)
(RIGHTOF0-ROBOT)
(NOT-RIGHTOF1-ROBOT)
(not (RIGHTOF8-ROBOT))
(not (NOT-RIGHTOF0-ROBOT))
)
)
(when
(and
(RIGHTOF7-ROBOT)
)
(and
(RIGHTOF6-ROBOT)
(NOT-RIGHTOF7-ROBOT)
(RIGHTOF5-ROBOT)
(NOT-RIGHTOF6-ROBOT)
(RIGHTOF4-ROBOT)
(NOT-RIGHTOF5-ROBOT)
(RIGHTOF3-ROBOT)
(NOT-RIGHTOF4-ROBOT)
(RIGHTOF2-ROBOT)
(NOT-RIGHTOF3-ROBOT)
(RIGHTOF1-ROBOT)
(NOT-RIGHTOF2-ROBOT)
(RIGHTOF0-ROBOT)
(NOT-RIGHTOF1-ROBOT)
(not (RIGHTOF7-ROBOT))
(not (NOT-RIGHTOF0-ROBOT))
)
)
(when
(and
(RIGHTOF6-ROBOT)
)
(and
(RIGHTOF5-ROBOT)
(NOT-RIGHTOF6-ROBOT)
(RIGHTOF4-ROBOT)
(NOT-RIGHTOF5-ROBOT)
(RIGHTOF3-ROBOT)
(NOT-RIGHTOF4-ROBOT)
(RIGHTOF2-ROBOT)
(NOT-RIGHTOF3-ROBOT)
(RIGHTOF1-ROBOT)
(NOT-RIGHTOF2-ROBOT)
(RIGHTOF0-ROBOT)
(NOT-RIGHTOF1-ROBOT)
(not (RIGHTOF6-ROBOT))
(not (NOT-RIGHTOF0-ROBOT))
)
)
(when
(and
(RIGHTOF5-ROBOT)
)
(and
(RIGHTOF4-ROBOT)
(NOT-RIGHTOF5-ROBOT)
(RIGHTOF3-ROBOT)
(NOT-RIGHTOF4-ROBOT)
(RIGHTOF2-ROBOT)
(NOT-RIGHTOF3-ROBOT)
(RIGHTOF1-ROBOT)
(NOT-RIGHTOF2-ROBOT)
(RIGHTOF0-ROBOT)
(NOT-RIGHTOF1-ROBOT)
(not (RIGHTOF5-ROBOT))
(not (NOT-RIGHTOF0-ROBOT))
)
)
(when
(and
(RIGHTOF4-ROBOT)
)
(and
(RIGHTOF3-ROBOT)
(NOT-RIGHTOF4-ROBOT)
(RIGHTOF2-ROBOT)
(NOT-RIGHTOF3-ROBOT)
(RIGHTOF1-ROBOT)
(NOT-RIGHTOF2-ROBOT)
(RIGHTOF0-ROBOT)
(NOT-RIGHTOF1-ROBOT)
(not (RIGHTOF4-ROBOT))
(not (NOT-RIGHTOF0-ROBOT))
)
)
(when
(and
(RIGHTOF3-ROBOT)
)
(and
(RIGHTOF2-ROBOT)
(NOT-RIGHTOF3-ROBOT)
(RIGHTOF1-ROBOT)
(NOT-RIGHTOF2-ROBOT)
(RIGHTOF0-ROBOT)
(NOT-RIGHTOF1-ROBOT)
(not (RIGHTOF3-ROBOT))
(not (NOT-RIGHTOF0-ROBOT))
)
)
(when
(and
(RIGHTOF2-ROBOT)
)
(and
(RIGHTOF1-ROBOT)
(NOT-RIGHTOF2-ROBOT)
(RIGHTOF0-ROBOT)
(NOT-RIGHTOF1-ROBOT)
(not (RIGHTOF2-ROBOT))
(not (NOT-RIGHTOF0-ROBOT))
)
)
(when
(and
(RIGHTOF1-ROBOT)
)
(and
(RIGHTOF0-ROBOT)
(NOT-RIGHTOF1-ROBOT)
(not (NOT-RIGHTOF0-ROBOT))
(not (RIGHTOF1-ROBOT))
)
)
(when
(and
(Flag349-)
)
(and
(LEFTOF14-ROBOT)
(not (NOT-LEFTOF14-ROBOT))
)
)
(when
(and
(LEFTOF8-ROBOT)
)
(and
(LEFTOF13-ROBOT)
(LEFTOF12-ROBOT)
(LEFTOF11-ROBOT)
(LEFTOF10-ROBOT)
(LEFTOF9-ROBOT)
(LEFTOF7-ROBOT)
(not (NOT-LEFTOF13-ROBOT))
(not (NOT-LEFTOF12-ROBOT))
(not (NOT-LEFTOF11-ROBOT))
(not (NOT-LEFTOF10-ROBOT))
(not (NOT-LEFTOF9-ROBOT))
(not (NOT-LEFTOF8-ROBOT))
(not (NOT-LEFTOF7-ROBOT))
)
)
(when
(and
(LEFTOF7-ROBOT)
)
(and
(LEFTOF13-ROBOT)
(LEFTOF12-ROBOT)
(LEFTOF11-ROBOT)
(LEFTOF10-ROBOT)
(LEFTOF9-ROBOT)
(LEFTOF8-ROBOT)
(LEFTOF6-ROBOT)
(not (NOT-LEFTOF13-ROBOT))
(not (NOT-LEFTOF12-ROBOT))
(not (NOT-LEFTOF11-ROBOT))
(not (NOT-LEFTOF10-ROBOT))
(not (NOT-LEFTOF9-ROBOT))
(not (NOT-LEFTOF8-ROBOT))
(not (NOT-LEFTOF7-ROBOT))
(not (NOT-LEFTOF6-ROBOT))
)
)
(when
(and
(LEFTOF4-ROBOT)
)
(and
(LEFTOF13-ROBOT)
(LEFTOF12-ROBOT)
(LEFTOF11-ROBOT)
(LEFTOF10-ROBOT)
(LEFTOF9-ROBOT)
(LEFTOF8-ROBOT)
(LEFTOF7-ROBOT)
(LEFTOF6-ROBOT)
(LEFTOF5-ROBOT)
(LEFTOF3-ROBOT)
(not (NOT-LEFTOF13-ROBOT))
(not (NOT-LEFTOF12-ROBOT))
(not (NOT-LEFTOF11-ROBOT))
(not (NOT-LEFTOF10-ROBOT))
(not (NOT-LEFTOF9-ROBOT))
(not (NOT-LEFTOF8-ROBOT))
(not (NOT-LEFTOF7-ROBOT))
(not (NOT-LEFTOF6-ROBOT))
(not (NOT-LEFTOF5-ROBOT))
(not (NOT-LEFTOF4-ROBOT))
(not (NOT-LEFTOF3-ROBOT))
)
)
(when
(and
(LEFTOF3-ROBOT)
)
(and
(LEFTOF13-ROBOT)
(LEFTOF12-ROBOT)
(LEFTOF11-ROBOT)
(LEFTOF10-ROBOT)
(LEFTOF9-ROBOT)
(LEFTOF8-ROBOT)
(LEFTOF7-ROBOT)
(LEFTOF6-ROBOT)
(LEFTOF5-ROBOT)
(LEFTOF4-ROBOT)
(LEFTOF2-ROBOT)
(not (NOT-LEFTOF13-ROBOT))
(not (NOT-LEFTOF12-ROBOT))
(not (NOT-LEFTOF11-ROBOT))
(not (NOT-LEFTOF10-ROBOT))
(not (NOT-LEFTOF9-ROBOT))
(not (NOT-LEFTOF8-ROBOT))
(not (NOT-LEFTOF7-ROBOT))
(not (NOT-LEFTOF6-ROBOT))
(not (NOT-LEFTOF5-ROBOT))
(not (NOT-LEFTOF4-ROBOT))
(not (NOT-LEFTOF3-ROBOT))
(not (NOT-LEFTOF2-ROBOT))
)
)
(when
(and
(LEFTOF14-ROBOT)
)
(and
(LEFTOF13-ROBOT)
(not (NOT-LEFTOF13-ROBOT))
)
)
(when
(and
(LEFTOF5-ROBOT)
)
(and
(LEFTOF13-ROBOT)
(LEFTOF12-ROBOT)
(LEFTOF11-ROBOT)
(LEFTOF10-ROBOT)
(LEFTOF9-ROBOT)
(LEFTOF8-ROBOT)
(LEFTOF7-ROBOT)
(LEFTOF6-ROBOT)
(LEFTOF4-ROBOT)
(not (NOT-LEFTOF13-ROBOT))
(not (NOT-LEFTOF12-ROBOT))
(not (NOT-LEFTOF11-ROBOT))
(not (NOT-LEFTOF10-ROBOT))
(not (NOT-LEFTOF9-ROBOT))
(not (NOT-LEFTOF8-ROBOT))
(not (NOT-LEFTOF7-ROBOT))
(not (NOT-LEFTOF6-ROBOT))
(not (NOT-LEFTOF5-ROBOT))
(not (NOT-LEFTOF4-ROBOT))
)
)
(when
(and
(LEFTOF6-ROBOT)
)
(and
(LEFTOF13-ROBOT)
(LEFTOF12-ROBOT)
(LEFTOF11-ROBOT)
(LEFTOF10-ROBOT)
(LEFTOF9-ROBOT)
(LEFTOF8-ROBOT)
(LEFTOF7-ROBOT)
(LEFTOF5-ROBOT)
(not (NOT-LEFTOF13-ROBOT))
(not (NOT-LEFTOF12-ROBOT))
(not (NOT-LEFTOF11-ROBOT))
(not (NOT-LEFTOF10-ROBOT))
(not (NOT-LEFTOF9-ROBOT))
(not (NOT-LEFTOF8-ROBOT))
(not (NOT-LEFTOF7-ROBOT))
(not (NOT-LEFTOF6-ROBOT))
(not (NOT-LEFTOF5-ROBOT))
)
)
(when
(and
(LEFTOF12-ROBOT)
)
(and
(LEFTOF13-ROBOT)
(LEFTOF11-ROBOT)
(not (NOT-LEFTOF13-ROBOT))
(not (NOT-LEFTOF12-ROBOT))
(not (NOT-LEFTOF11-ROBOT))
)
)
(when
(and
(LEFTOF1-ROBOT)
)
(and
(LEFTOF13-ROBOT)
(LEFTOF12-ROBOT)
(LEFTOF11-ROBOT)
(LEFTOF10-ROBOT)
(LEFTOF9-ROBOT)
(LEFTOF8-ROBOT)
(LEFTOF7-ROBOT)
(LEFTOF6-ROBOT)
(LEFTOF5-ROBOT)
(LEFTOF4-ROBOT)
(LEFTOF3-ROBOT)
(LEFTOF2-ROBOT)
(not (NOT-LEFTOF13-ROBOT))
(not (NOT-LEFTOF12-ROBOT))
(not (NOT-LEFTOF11-ROBOT))
(not (NOT-LEFTOF10-ROBOT))
(not (NOT-LEFTOF9-ROBOT))
(not (NOT-LEFTOF8-ROBOT))
(not (NOT-LEFTOF7-ROBOT))
(not (NOT-LEFTOF6-ROBOT))
(not (NOT-LEFTOF5-ROBOT))
(not (NOT-LEFTOF4-ROBOT))
(not (NOT-LEFTOF3-ROBOT))
(not (NOT-LEFTOF2-ROBOT))
(not (NOT-LEFTOF1-ROBOT))
)
)
(when
(and
(LEFTOF13-ROBOT)
)
(and
(LEFTOF12-ROBOT)
(not (NOT-LEFTOF13-ROBOT))
(not (NOT-LEFTOF12-ROBOT))
)
)
(when
(and
(LEFTOF2-ROBOT)
)
(and
(LEFTOF13-ROBOT)
(LEFTOF12-ROBOT)
(LEFTOF11-ROBOT)
(LEFTOF10-ROBOT)
(LEFTOF9-ROBOT)
(LEFTOF8-ROBOT)
(LEFTOF7-ROBOT)
(LEFTOF6-ROBOT)
(LEFTOF5-ROBOT)
(LEFTOF4-ROBOT)
(LEFTOF3-ROBOT)
(LEFTOF1-ROBOT)
(not (NOT-LEFTOF13-ROBOT))
(not (NOT-LEFTOF12-ROBOT))
(not (NOT-LEFTOF11-ROBOT))
(not (NOT-LEFTOF10-ROBOT))
(not (NOT-LEFTOF9-ROBOT))
(not (NOT-LEFTOF8-ROBOT))
(not (NOT-LEFTOF7-ROBOT))
(not (NOT-LEFTOF6-ROBOT))
(not (NOT-LEFTOF5-ROBOT))
(not (NOT-LEFTOF4-ROBOT))
(not (NOT-LEFTOF3-ROBOT))
(not (NOT-LEFTOF2-ROBOT))
(not (NOT-LEFTOF1-ROBOT))
)
)
(when
(and
(LEFTOF9-ROBOT)
)
(and
(LEFTOF13-ROBOT)
(LEFTOF12-ROBOT)
(LEFTOF11-ROBOT)
(LEFTOF10-ROBOT)
(LEFTOF8-ROBOT)
(not (NOT-LEFTOF13-ROBOT))
(not (NOT-LEFTOF12-ROBOT))
(not (NOT-LEFTOF11-ROBOT))
(not (NOT-LEFTOF10-ROBOT))
(not (NOT-LEFTOF9-ROBOT))
(not (NOT-LEFTOF8-ROBOT))
)
)
(when
(and
(LEFTOF11-ROBOT)
)
(and
(LEFTOF13-ROBOT)
(LEFTOF12-ROBOT)
(LEFTOF10-ROBOT)
(not (NOT-LEFTOF13-ROBOT))
(not (NOT-LEFTOF12-ROBOT))
(not (NOT-LEFTOF11-ROBOT))
(not (NOT-LEFTOF10-ROBOT))
)
)
(when
(and
(LEFTOF10-ROBOT)
)
(and
(LEFTOF13-ROBOT)
(LEFTOF12-ROBOT)
(LEFTOF11-ROBOT)
(LEFTOF9-ROBOT)
(not (NOT-LEFTOF13-ROBOT))
(not (NOT-LEFTOF12-ROBOT))
(not (NOT-LEFTOF11-ROBOT))
(not (NOT-LEFTOF10-ROBOT))
(not (NOT-LEFTOF9-ROBOT))
)
)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action MOVERIGHT-ROBOT
:parameters ()
:precondition
(and
(Flag4-)
)
:effect
(and
(when
(and
(Flag348-)
)
(and
(COLUMN14-ROBOT)
(not (NOT-COLUMN14-ROBOT))
)
)
(when
(and
(Flag345-)
)
(and
(COLUMN13-ROBOT)
(not (NOT-COLUMN13-ROBOT))
)
)
(when
(and
(Flag340-)
)
(and
(COLUMN12-ROBOT)
(not (NOT-COLUMN12-ROBOT))
)
)
(when
(and
(Flag335-)
)
(and
(COLUMN11-ROBOT)
(not (NOT-COLUMN11-ROBOT))
)
)
(when
(and
(Flag328-)
)
(and
(COLUMN10-ROBOT)
(not (NOT-COLUMN10-ROBOT))
)
)
(when
(and
(Flag321-)
)
(and
(COLUMN9-ROBOT)
(not (NOT-COLUMN9-ROBOT))
)
)
(when
(and
(Flag312-)
)
(and
(COLUMN8-ROBOT)
(not (NOT-COLUMN8-ROBOT))
)
)
(when
(and
(Flag302-)
)
(and
(COLUMN7-ROBOT)
(not (NOT-COLUMN7-ROBOT))
)
)
(when
(and
(Flag290-)
)
(and
(COLUMN6-ROBOT)
(not (NOT-COLUMN6-ROBOT))
)
)
(when
(and
(Flag279-)
)
(and
(COLUMN5-ROBOT)
(not (NOT-COLUMN5-ROBOT))
)
)
(when
(and
(Flag265-)
)
(and
(COLUMN4-ROBOT)
(not (NOT-COLUMN4-ROBOT))
)
)
(when
(and
(Flag250-)
)
(and
(COLUMN3-ROBOT)
(not (NOT-COLUMN3-ROBOT))
)
)
(when
(and
(Flag236-)
)
(and
(COLUMN2-ROBOT)
(not (NOT-COLUMN2-ROBOT))
)
)
(when
(and
(Flag220-)
)
(and
(COLUMN1-ROBOT)
(not (NOT-COLUMN1-ROBOT))
)
)
(when
(and
(COLUMN13-ROBOT)
)
(and
(COLUMN14-ROBOT)
(NOT-COLUMN13-ROBOT)
(not (NOT-COLUMN14-ROBOT))
(not (COLUMN13-ROBOT))
)
)
(when
(and
(COLUMN12-ROBOT)
)
(and
(COLUMN13-ROBOT)
(NOT-COLUMN12-ROBOT)
(not (NOT-COLUMN13-ROBOT))
(not (COLUMN12-ROBOT))
)
)
(when
(and
(COLUMN11-ROBOT)
)
(and
(COLUMN12-ROBOT)
(NOT-COLUMN11-ROBOT)
(not (NOT-COLUMN12-ROBOT))
(not (COLUMN11-ROBOT))
)
)
(when
(and
(COLUMN10-ROBOT)
)
(and
(COLUMN11-ROBOT)
(NOT-COLUMN10-ROBOT)
(not (NOT-COLUMN11-ROBOT))
(not (COLUMN10-ROBOT))
)
)
(when
(and
(COLUMN9-ROBOT)
)
(and
(COLUMN10-ROBOT)
(NOT-COLUMN9-ROBOT)
(not (NOT-COLUMN10-ROBOT))
(not (COLUMN9-ROBOT))
)
)
(when
(and
(COLUMN8-ROBOT)
)
(and
(COLUMN9-ROBOT)
(NOT-COLUMN8-ROBOT)
(not (NOT-COLUMN9-ROBOT))
(not (COLUMN8-ROBOT))
)
)
(when
(and
(COLUMN7-ROBOT)
)
(and
(COLUMN8-ROBOT)
(NOT-COLUMN7-ROBOT)
(not (NOT-COLUMN8-ROBOT))
(not (COLUMN7-ROBOT))
)
)
(when
(and
(COLUMN6-ROBOT)
)
(and
(COLUMN7-ROBOT)
(NOT-COLUMN6-ROBOT)
(not (NOT-COLUMN7-ROBOT))
(not (COLUMN6-ROBOT))
)
)
(when
(and
(COLUMN5-ROBOT)
)
(and
(COLUMN6-ROBOT)
(NOT-COLUMN5-ROBOT)
(not (NOT-COLUMN6-ROBOT))
(not (COLUMN5-ROBOT))
)
)
(when
(and
(COLUMN4-ROBOT)
)
(and
(COLUMN5-ROBOT)
(NOT-COLUMN4-ROBOT)
(not (NOT-COLUMN5-ROBOT))
(not (COLUMN4-ROBOT))
)
)
(when
(and
(COLUMN3-ROBOT)
)
(and
(COLUMN4-ROBOT)
(NOT-COLUMN3-ROBOT)
(not (NOT-COLUMN4-ROBOT))
(not (COLUMN3-ROBOT))
)
)
(when
(and
(COLUMN2-ROBOT)
)
(and
(COLUMN3-ROBOT)
(NOT-COLUMN2-ROBOT)
(not (NOT-COLUMN3-ROBOT))
(not (COLUMN2-ROBOT))
)
)
(when
(and
(COLUMN1-ROBOT)
)
(and
(COLUMN2-ROBOT)
(NOT-COLUMN1-ROBOT)
(not (NOT-COLUMN2-ROBOT))
(not (COLUMN1-ROBOT))
)
)
(when
(and
(COLUMN0-ROBOT)
)
(and
(COLUMN1-ROBOT)
(NOT-COLUMN0-ROBOT)
(not (NOT-COLUMN1-ROBOT))
(not (COLUMN0-ROBOT))
)
)
(when
(and
(Flag204-)
)
(and
(LEFTOF15-ROBOT)
(NOT-LEFTOF14-ROBOT)
(not (NOT-LEFTOF15-ROBOT))
(not (LEFTOF14-ROBOT))
)
)
(when
(and
(Flag203-)
)
(and
(LEFTOF14-ROBOT)
(NOT-LEFTOF13-ROBOT)
(not (NOT-LEFTOF14-ROBOT))
(not (LEFTOF13-ROBOT))
)
)
(when
(and
(Flag202-)
)
(and
(LEFTOF13-ROBOT)
(NOT-LEFTOF12-ROBOT)
(not (NOT-LEFTOF13-ROBOT))
(not (LEFTOF12-ROBOT))
)
)
(when
(and
(Flag201-)
)
(and
(LEFTOF12-ROBOT)
(NOT-LEFTOF11-ROBOT)
(not (NOT-LEFTOF12-ROBOT))
(not (LEFTOF11-ROBOT))
)
)
(when
(and
(Flag200-)
)
(and
(LEFTOF11-ROBOT)
(NOT-LEFTOF10-ROBOT)
(not (NOT-LEFTOF11-ROBOT))
(not (LEFTOF10-ROBOT))
)
)
(when
(and
(Flag199-)
)
(and
(LEFTOF10-ROBOT)
(NOT-LEFTOF9-ROBOT)
(not (NOT-LEFTOF10-ROBOT))
(not (LEFTOF9-ROBOT))
)
)
(when
(and
(Flag198-)
)
(and
(LEFTOF9-ROBOT)
(NOT-LEFTOF8-ROBOT)
(not (NOT-LEFTOF9-ROBOT))
(not (LEFTOF8-ROBOT))
)
)
(when
(and
(Flag197-)
)
(and
(LEFTOF8-ROBOT)
(NOT-LEFTOF7-ROBOT)
(not (NOT-LEFTOF8-ROBOT))
(not (LEFTOF7-ROBOT))
)
)
(when
(and
(Flag196-)
)
(and
(LEFTOF7-ROBOT)
(NOT-LEFTOF6-ROBOT)
(not (NOT-LEFTOF7-ROBOT))
(not (LEFTOF6-ROBOT))
)
)
(when
(and
(Flag195-)
)
(and
(LEFTOF6-ROBOT)
(NOT-LEFTOF5-ROBOT)
(not (NOT-LEFTOF6-ROBOT))
(not (LEFTOF5-ROBOT))
)
)
(when
(and
(Flag194-)
)
(and
(LEFTOF5-ROBOT)
(NOT-LEFTOF4-ROBOT)
(not (NOT-LEFTOF5-ROBOT))
(not (LEFTOF4-ROBOT))
)
)
(when
(and
(Flag193-)
)
(and
(LEFTOF4-ROBOT)
(NOT-LEFTOF3-ROBOT)
(not (NOT-LEFTOF4-ROBOT))
(not (LEFTOF3-ROBOT))
)
)
(when
(and
(Flag192-)
)
(and
(LEFTOF3-ROBOT)
(NOT-LEFTOF2-ROBOT)
(not (NOT-LEFTOF3-ROBOT))
(not (LEFTOF2-ROBOT))
)
)
(when
(and
(LEFTOF1-ROBOT)
)
(and
(LEFTOF2-ROBOT)
(NOT-LEFTOF1-ROBOT)
(not (NOT-LEFTOF2-ROBOT))
(not (LEFTOF1-ROBOT))
)
)
(when
(and
(Flag191-)
)
(and
(RIGHTOF14-ROBOT)
(not (NOT-RIGHTOF14-ROBOT))
)
)
(when
(and
(Flag190-)
)
(and
(RIGHTOF13-ROBOT)
(not (NOT-RIGHTOF13-ROBOT))
)
)
(when
(and
(Flag189-)
)
(and
(RIGHTOF12-ROBOT)
(not (NOT-RIGHTOF12-ROBOT))
)
)
(when
(and
(Flag188-)
)
(and
(RIGHTOF11-ROBOT)
(not (NOT-RIGHTOF11-ROBOT))
)
)
(when
(and
(Flag187-)
)
(and
(RIGHTOF10-ROBOT)
(not (NOT-RIGHTOF10-ROBOT))
)
)
(when
(and
(Flag186-)
)
(and
(RIGHTOF9-ROBOT)
(not (NOT-RIGHTOF9-ROBOT))
)
)
(when
(and
(Flag185-)
)
(and
(RIGHTOF8-ROBOT)
(not (NOT-RIGHTOF8-ROBOT))
)
)
(when
(and
(Flag184-)
)
(and
(RIGHTOF7-ROBOT)
(not (NOT-RIGHTOF7-ROBOT))
)
)
(when
(and
(Flag183-)
)
(and
(RIGHTOF6-ROBOT)
(not (NOT-RIGHTOF6-ROBOT))
)
)
(when
(and
(Flag182-)
)
(and
(RIGHTOF5-ROBOT)
(not (NOT-RIGHTOF5-ROBOT))
)
)
(when
(and
(Flag181-)
)
(and
(RIGHTOF4-ROBOT)
(not (NOT-RIGHTOF4-ROBOT))
)
)
(when
(and
(Flag180-)
)
(and
(RIGHTOF3-ROBOT)
(not (NOT-RIGHTOF3-ROBOT))
)
)
(when
(and
(Flag179-)
)
(and
(RIGHTOF2-ROBOT)
(not (NOT-RIGHTOF2-ROBOT))
)
)
(when
(and
(Flag178-)
)
(and
(RIGHTOF1-ROBOT)
(not (NOT-RIGHTOF1-ROBOT))
)
)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action CHECKCONSISTENCYACTION
:parameters ()
:precondition
(and
(NOT-ERROR-)
(CHECKCONSISTENCY-)
)
:effect
(and
(when
(and
(RIGHTOF12-ROBOT)
(LEFTOF9-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF8-ROBOT)
(BELOWOF2-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF11-ROBOT)
(RIGHTOF14-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF9-ROBOT)
(BELOWOF6-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF2-ROBOT)
(ABOVEOF7-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF12-ROBOT)
(LEFTOF7-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF13-ROBOT)
(BELOWOF3-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF4-ROBOT)
(LEFTOF2-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF10-ROBOT)
(BELOWOF4-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF5-ROBOT)
(LEFTOF2-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF13-ROBOT)
(BELOWOF10-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF3-ROBOT)
(RIGHTOF11-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF8-ROBOT)
(ABOVEOF14-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF13-ROBOT)
(LEFTOF6-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF4-ROBOT)
(RIGHTOF13-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF4-ROBOT)
(RIGHTOF8-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF3-ROBOT)
(ABOVEOF14-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF13-ROBOT)
(LEFTOF7-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF6-ROBOT)
(RIGHTOF11-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF3-ROBOT)
(RIGHTOF7-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF3-ROBOT)
(ABOVEOF12-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF7-ROBOT)
(RIGHTOF9-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF8-ROBOT)
(ABOVEOF12-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF8-ROBOT)
(RIGHTOF13-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF13-ROBOT)
(LEFTOF5-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF11-ROBOT)
(LEFTOF1-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF5-ROBOT)
(ABOVEOF12-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF10-ROBOT)
(BELOWOF2-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF1-ROBOT)
(ABOVEOF12-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF12-ROBOT)
(LEFTOF2-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF12-ROBOT)
(LEFTOF1-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF4-ROBOT)
(ABOVEOF11-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF2-ROBOT)
(ABOVEOF13-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF12-ROBOT)
(LEFTOF3-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF11-ROBOT)
(BELOWOF6-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF4-ROBOT)
(RIGHTOF7-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF13-ROBOT)
(LEFTOF3-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF9-ROBOT)
(RIGHTOF13-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF9-ROBOT)
(LEFTOF6-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF9-ROBOT)
(LEFTOF3-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF5-ROBOT)
(LEFTOF1-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF8-ROBOT)
(RIGHTOF10-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF13-ROBOT)
(LEFTOF2-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF11-ROBOT)
(BELOWOF3-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF13-ROBOT)
(BELOWOF5-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF8-ROBOT)
(LEFTOF6-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF14-ROBOT)
(LEFTOF10-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF14-ROBOT)
(LEFTOF9-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF1-ROBOT)
(ABOVEOF7-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF1-ROBOT)
(RIGHTOF14-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF14-ROBOT)
(BELOWOF2-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF1-ROBOT)
(ABOVEOF4-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF8-ROBOT)
(LEFTOF2-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF14-ROBOT)
(LEFTOF8-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF14-ROBOT)
(BELOWOF10-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF13-ROBOT)
(LEFTOF1-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF8-ROBOT)
(BELOWOF1-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF9-ROBOT)
(LEFTOF1-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF14-ROBOT)
(LEFTOF3-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF10-ROBOT)
(BELOWOF1-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF6-ROBOT)
(ABOVEOF13-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF9-ROBOT)
(BELOWOF2-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF9-ROBOT)
(LEFTOF2-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF4-ROBOT)
(RIGHTOF12-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF4-ROBOT)
(RIGHTOF6-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF3-ROBOT)
(ABOVEOF10-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF10-ROBOT)
(LEFTOF1-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF5-ROBOT)
(RIGHTOF14-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF6-ROBOT)
(ABOVEOF14-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF10-ROBOT)
(LEFTOF2-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF12-ROBOT)
(BELOWOF6-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF8-ROBOT)
(RIGHTOF11-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF12-ROBOT)
(BELOWOF7-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF1-ROBOT)
(RIGHTOF7-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF6-ROBOT)
(RIGHTOF7-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF2-ROBOT)
(ABOVEOF12-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF7-ROBOT)
(ABOVEOF13-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF13-ROBOT)
(LEFTOF11-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF12-ROBOT)
(ABOVEOF13-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF3-ROBOT)
(RIGHTOF5-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF5-ROBOT)
(ABOVEOF11-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF14-ROBOT)
(LEFTOF4-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF14-ROBOT)
(BELOWOF5-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF8-ROBOT)
(ABOVEOF13-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF5-ROBOT)
(ABOVEOF7-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF8-ROBOT)
(RIGHTOF8-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF4-ROBOT)
(RIGHTOF10-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF12-ROBOT)
(BELOWOF4-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF4-ROBOT)
(RIGHTOF5-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF11-ROBOT)
(BELOWOF1-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF14-ROBOT)
(LEFTOF13-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF6-ROBOT)
(LEFTOF2-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF7-ROBOT)
(LEFTOF2-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF4-ROBOT)
(ABOVEOF14-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF3-ROBOT)
(RIGHTOF6-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF9-ROBOT)
(LEFTOF5-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF4-ROBOT)
(ABOVEOF7-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF14-ROBOT)
(BELOWOF14-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF11-ROBOT)
(LEFTOF2-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF3-ROBOT)
(LEFTOF2-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF14-ROBOT)
(BELOWOF7-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF5-ROBOT)
(ABOVEOF10-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF8-ROBOT)
(LEFTOF3-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF10-ROBOT)
(LEFTOF10-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF14-ROBOT)
(BELOWOF1-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF11-ROBOT)
(BELOWOF2-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF4-ROBOT)
(LEFTOF1-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF13-ROBOT)
(BELOWOF9-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF14-ROBOT)
(LEFTOF6-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF4-ROBOT)
(ABOVEOF9-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF8-ROBOT)
(RIGHTOF9-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF7-ROBOT)
(RIGHTOF8-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF11-ROBOT)
(LEFTOF5-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF14-ROBOT)
(LEFTOF7-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF13-ROBOT)
(BELOWOF1-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF6-ROBOT)
(ABOVEOF8-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF14-ROBOT)
(LEFTOF2-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF10-ROBOT)
(LEFTOF3-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF8-ROBOT)
(BELOWOF4-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF11-ROBOT)
(ABOVEOF13-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF4-ROBOT)
(RIGHTOF9-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF11-ROBOT)
(BELOWOF11-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF8-ROBOT)
(LEFTOF1-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF12-ROBOT)
(BELOWOF10-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF3-ROBOT)
(ABOVEOF9-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF9-ROBOT)
(LEFTOF9-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF5-ROBOT)
(ABOVEOF9-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF7-ROBOT)
(RIGHTOF11-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF4-ROBOT)
(ABOVEOF13-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF7-ROBOT)
(BELOWOF3-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF11-ROBOT)
(LEFTOF9-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF12-ROBOT)
(BELOWOF11-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF10-ROBOT)
(LEFTOF6-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF10-ROBOT)
(RIGHTOF13-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF9-ROBOT)
(ABOVEOF12-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF7-ROBOT)
(BELOWOF6-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF4-ROBOT)
(RIGHTOF4-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF8-ROBOT)
(ABOVEOF10-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF10-ROBOT)
(RIGHTOF12-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF6-ROBOT)
(ABOVEOF10-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF7-ROBOT)
(ABOVEOF11-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF7-ROBOT)
(ABOVEOF7-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF3-ROBOT)
(ABOVEOF6-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF9-ROBOT)
(ABOVEOF9-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF4-ROBOT)
(LEFTOF3-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF14-ROBOT)
(BELOWOF11-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF1-ROBOT)
(RIGHTOF2-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF5-ROBOT)
(BELOWOF3-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF7-ROBOT)
(ABOVEOF10-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF7-ROBOT)
(RIGHTOF10-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF11-ROBOT)
(BELOWOF8-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF1-ROBOT)
(ABOVEOF6-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF6-ROBOT)
(BELOWOF4-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF3-ROBOT)
(LEFTOF1-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF10-ROBOT)
(LEFTOF5-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF5-ROBOT)
(BELOWOF5-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF1-ROBOT)
(ABOVEOF3-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF4-ROBOT)
(BELOWOF3-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF1-ROBOT)
(RIGHTOF6-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF4-ROBOT)
(RIGHTOF11-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF5-ROBOT)
(ABOVEOF6-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF2-ROBOT)
(LEFTOF2-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF12-ROBOT)
(LEFTOF8-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF14-ROBOT)
(BELOWOF9-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF8-ROBOT)
(LEFTOF5-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF2-ROBOT)
(ABOVEOF6-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF1-ROBOT)
(ABOVEOF9-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF14-ROBOT)
(BELOWOF13-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF14-ROBOT)
(BELOWOF12-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF9-ROBOT)
(ABOVEOF10-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF3-ROBOT)
(ABOVEOF3-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF5-ROBOT)
(BELOWOF1-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF4-ROBOT)
(ABOVEOF4-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF11-ROBOT)
(LEFTOF10-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF8-ROBOT)
(BELOWOF3-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF11-ROBOT)
(BELOWOF9-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF5-ROBOT)
(RIGHTOF12-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF6-ROBOT)
(RIGHTOF12-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF12-ROBOT)
(BELOWOF12-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF14-ROBOT)
(LEFTOF12-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF11-ROBOT)
(RIGHTOF11-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF5-ROBOT)
(BELOWOF2-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF7-ROBOT)
(LEFTOF5-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF14-ROBOT)
(LEFTOF14-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF11-ROBOT)
(RIGHTOF12-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF5-ROBOT)
(ABOVEOF8-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF13-ROBOT)
(LEFTOF13-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF9-ROBOT)
(BELOWOF7-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF9-ROBOT)
(RIGHTOF10-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF6-ROBOT)
(LEFTOF5-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF2-ROBOT)
(ABOVEOF4-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF11-ROBOT)
(BELOWOF10-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF5-ROBOT)
(LEFTOF5-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF5-ROBOT)
(BELOWOF4-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF8-ROBOT)
(ABOVEOF8-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF2-ROBOT)
(ABOVEOF3-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF6-ROBOT)
(ABOVEOF6-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF6-ROBOT)
(LEFTOF6-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF13-ROBOT)
(LEFTOF12-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF10-ROBOT)
(ABOVEOF10-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF2-ROBOT)
(BELOWOF1-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF8-ROBOT)
(ABOVEOF9-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF2-ROBOT)
(BELOWOF2-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF7-ROBOT)
(ABOVEOF8-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF13-ROBOT)
(ABOVEOF13-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF1-ROBOT)
(LEFTOF1-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF7-ROBOT)
(RIGHTOF7-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF1-ROBOT)
(ABOVEOF1-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF12-ROBOT)
(LEFTOF12-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF3-ROBOT)
(RIGHTOF3-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(NOT-CHECKCONSISTENCY-)
(not (CHECKCONSISTENCY-))
)
)
(:derived (Flag4-)(and (NOT-CHECKCONSISTENCY-)(NOT-ERROR-)(Flag3-)))

(:derived (Flag5-)(and (BELOWOF2-ROBOT)))

(:derived (Flag5-)(and (BELOWOF1-ROBOT)))

(:derived (Flag6-)(and (BELOWOF2-ROBOT)))

(:derived (Flag6-)(and (BELOWOF1-ROBOT)))

(:derived (Flag6-)(and (BELOWOF3-ROBOT)))

(:derived (Flag7-)(and (BELOWOF2-ROBOT)))

(:derived (Flag7-)(and (BELOWOF1-ROBOT)))

(:derived (Flag7-)(and (BELOWOF3-ROBOT)))

(:derived (Flag7-)(and (BELOWOF4-ROBOT)))

(:derived (Flag8-)(and (BELOWOF2-ROBOT)))

(:derived (Flag8-)(and (BELOWOF1-ROBOT)))

(:derived (Flag8-)(and (BELOWOF3-ROBOT)))

(:derived (Flag8-)(and (BELOWOF4-ROBOT)))

(:derived (Flag8-)(and (BELOWOF5-ROBOT)))

(:derived (Flag9-)(and (BELOWOF6-ROBOT)))

(:derived (Flag9-)(and (BELOWOF5-ROBOT)))

(:derived (Flag9-)(and (BELOWOF2-ROBOT)))

(:derived (Flag9-)(and (BELOWOF1-ROBOT)))

(:derived (Flag9-)(and (BELOWOF4-ROBOT)))

(:derived (Flag9-)(and (BELOWOF3-ROBOT)))

(:derived (Flag10-)(and (BELOWOF6-ROBOT)))

(:derived (Flag10-)(and (BELOWOF5-ROBOT)))

(:derived (Flag10-)(and (BELOWOF2-ROBOT)))

(:derived (Flag10-)(and (BELOWOF1-ROBOT)))

(:derived (Flag10-)(and (BELOWOF4-ROBOT)))

(:derived (Flag10-)(and (BELOWOF7-ROBOT)))

(:derived (Flag10-)(and (BELOWOF3-ROBOT)))

(:derived (Flag11-)(and (BELOWOF6-ROBOT)))

(:derived (Flag11-)(and (BELOWOF5-ROBOT)))

(:derived (Flag11-)(and (BELOWOF2-ROBOT)))

(:derived (Flag11-)(and (BELOWOF1-ROBOT)))

(:derived (Flag11-)(and (BELOWOF4-ROBOT)))

(:derived (Flag11-)(and (BELOWOF7-ROBOT)))

(:derived (Flag11-)(and (BELOWOF3-ROBOT)))

(:derived (Flag11-)(and (BELOWOF8-ROBOT)))

(:derived (Flag12-)(and (BELOWOF6-ROBOT)))

(:derived (Flag12-)(and (BELOWOF5-ROBOT)))

(:derived (Flag12-)(and (BELOWOF2-ROBOT)))

(:derived (Flag12-)(and (BELOWOF1-ROBOT)))

(:derived (Flag12-)(and (BELOWOF4-ROBOT)))

(:derived (Flag12-)(and (BELOWOF7-ROBOT)))

(:derived (Flag12-)(and (BELOWOF9-ROBOT)))

(:derived (Flag12-)(and (BELOWOF3-ROBOT)))

(:derived (Flag12-)(and (BELOWOF8-ROBOT)))

(:derived (Flag13-)(and (BELOWOF10-ROBOT)))

(:derived (Flag13-)(and (BELOWOF6-ROBOT)))

(:derived (Flag13-)(and (BELOWOF5-ROBOT)))

(:derived (Flag13-)(and (BELOWOF2-ROBOT)))

(:derived (Flag13-)(and (BELOWOF1-ROBOT)))

(:derived (Flag13-)(and (BELOWOF4-ROBOT)))

(:derived (Flag13-)(and (BELOWOF7-ROBOT)))

(:derived (Flag13-)(and (BELOWOF9-ROBOT)))

(:derived (Flag13-)(and (BELOWOF3-ROBOT)))

(:derived (Flag13-)(and (BELOWOF8-ROBOT)))

(:derived (Flag14-)(and (BELOWOF10-ROBOT)))

(:derived (Flag14-)(and (BELOWOF11-ROBOT)))

(:derived (Flag14-)(and (BELOWOF6-ROBOT)))

(:derived (Flag14-)(and (BELOWOF5-ROBOT)))

(:derived (Flag14-)(and (BELOWOF2-ROBOT)))

(:derived (Flag14-)(and (BELOWOF1-ROBOT)))

(:derived (Flag14-)(and (BELOWOF4-ROBOT)))

(:derived (Flag14-)(and (BELOWOF7-ROBOT)))

(:derived (Flag14-)(and (BELOWOF9-ROBOT)))

(:derived (Flag14-)(and (BELOWOF3-ROBOT)))

(:derived (Flag14-)(and (BELOWOF8-ROBOT)))

(:derived (Flag15-)(and (BELOWOF10-ROBOT)))

(:derived (Flag15-)(and (BELOWOF11-ROBOT)))

(:derived (Flag15-)(and (BELOWOF6-ROBOT)))

(:derived (Flag15-)(and (BELOWOF5-ROBOT)))

(:derived (Flag15-)(and (BELOWOF2-ROBOT)))

(:derived (Flag15-)(and (BELOWOF1-ROBOT)))

(:derived (Flag15-)(and (BELOWOF4-ROBOT)))

(:derived (Flag15-)(and (BELOWOF12-ROBOT)))

(:derived (Flag15-)(and (BELOWOF7-ROBOT)))

(:derived (Flag15-)(and (BELOWOF9-ROBOT)))

(:derived (Flag15-)(and (BELOWOF3-ROBOT)))

(:derived (Flag15-)(and (BELOWOF8-ROBOT)))

(:derived (Flag16-)(and (BELOWOF10-ROBOT)))

(:derived (Flag16-)(and (BELOWOF11-ROBOT)))

(:derived (Flag16-)(and (BELOWOF6-ROBOT)))

(:derived (Flag16-)(and (BELOWOF5-ROBOT)))

(:derived (Flag16-)(and (BELOWOF2-ROBOT)))

(:derived (Flag16-)(and (BELOWOF1-ROBOT)))

(:derived (Flag16-)(and (BELOWOF4-ROBOT)))

(:derived (Flag16-)(and (BELOWOF12-ROBOT)))

(:derived (Flag16-)(and (BELOWOF7-ROBOT)))

(:derived (Flag16-)(and (BELOWOF9-ROBOT)))

(:derived (Flag16-)(and (BELOWOF3-ROBOT)))

(:derived (Flag16-)(and (BELOWOF13-ROBOT)))

(:derived (Flag16-)(and (BELOWOF8-ROBOT)))

(:derived (Flag17-)(and (BELOWOF10-ROBOT)))

(:derived (Flag17-)(and (BELOWOF11-ROBOT)))

(:derived (Flag17-)(and (BELOWOF6-ROBOT)))

(:derived (Flag17-)(and (BELOWOF5-ROBOT)))

(:derived (Flag17-)(and (BELOWOF2-ROBOT)))

(:derived (Flag17-)(and (BELOWOF1-ROBOT)))

(:derived (Flag17-)(and (BELOWOF4-ROBOT)))

(:derived (Flag17-)(and (BELOWOF12-ROBOT)))

(:derived (Flag17-)(and (BELOWOF7-ROBOT)))

(:derived (Flag17-)(and (BELOWOF9-ROBOT)))

(:derived (Flag17-)(and (BELOWOF3-ROBOT)))

(:derived (Flag17-)(and (BELOWOF13-ROBOT)))

(:derived (Flag17-)(and (BELOWOF8-ROBOT)))

(:derived (Flag18-)(and (BELOWOF10-ROBOT)))

(:derived (Flag18-)(and (BELOWOF11-ROBOT)))

(:derived (Flag18-)(and (BELOWOF15-ROBOT)))

(:derived (Flag18-)(and (BELOWOF6-ROBOT)))

(:derived (Flag18-)(and (BELOWOF5-ROBOT)))

(:derived (Flag18-)(and (BELOWOF2-ROBOT)))

(:derived (Flag18-)(and (BELOWOF1-ROBOT)))

(:derived (Flag18-)(and (BELOWOF4-ROBOT)))

(:derived (Flag18-)(and (BELOWOF12-ROBOT)))

(:derived (Flag18-)(and (BELOWOF7-ROBOT)))

(:derived (Flag18-)(and (BELOWOF9-ROBOT)))

(:derived (Flag18-)(and (BELOWOF3-ROBOT)))

(:derived (Flag18-)(and (BELOWOF13-ROBOT)))

(:derived (Flag18-)(and (BELOWOF8-ROBOT)))

(:derived (Flag19-)(and (ABOVEOF6-ROBOT)))

(:derived (Flag19-)(and (ABOVEOF11-ROBOT)))

(:derived (Flag19-)(and (ABOVEOF10-ROBOT)))

(:derived (Flag19-)(and (ABOVEOF2-ROBOT)))

(:derived (Flag19-)(and (ABOVEOF7-ROBOT)))

(:derived (Flag19-)(and (ABOVEOF12-ROBOT)))

(:derived (Flag19-)(and (ABOVEOF8-ROBOT)))

(:derived (Flag19-)(and (ABOVEOF9-ROBOT)))

(:derived (Flag19-)(and (ABOVEOF4-ROBOT)))

(:derived (Flag19-)(and (ABOVEOF3-ROBOT)))

(:derived (Flag19-)(and (ABOVEOF13-ROBOT)))

(:derived (Flag19-)(and (ABOVEOF14-ROBOT)))

(:derived (Flag19-)(and (ABOVEOF5-ROBOT)))

(:derived (Flag19-)(and (ABOVEOF1-ROBOT)))

(:derived (Flag20-)(and (ABOVEOF11-ROBOT)))

(:derived (Flag20-)(and (ABOVEOF6-ROBOT)))

(:derived (Flag20-)(and (ABOVEOF10-ROBOT)))

(:derived (Flag20-)(and (ABOVEOF2-ROBOT)))

(:derived (Flag20-)(and (ABOVEOF7-ROBOT)))

(:derived (Flag20-)(and (ABOVEOF12-ROBOT)))

(:derived (Flag20-)(and (ABOVEOF8-ROBOT)))

(:derived (Flag20-)(and (ABOVEOF9-ROBOT)))

(:derived (Flag20-)(and (ABOVEOF4-ROBOT)))

(:derived (Flag20-)(and (ABOVEOF3-ROBOT)))

(:derived (Flag20-)(and (ABOVEOF13-ROBOT)))

(:derived (Flag20-)(and (ABOVEOF14-ROBOT)))

(:derived (Flag20-)(and (ABOVEOF5-ROBOT)))

(:derived (Flag21-)(and (ABOVEOF11-ROBOT)))

(:derived (Flag21-)(and (ABOVEOF6-ROBOT)))

(:derived (Flag21-)(and (ABOVEOF10-ROBOT)))

(:derived (Flag21-)(and (ABOVEOF8-ROBOT)))

(:derived (Flag21-)(and (ABOVEOF7-ROBOT)))

(:derived (Flag21-)(and (ABOVEOF12-ROBOT)))

(:derived (Flag21-)(and (ABOVEOF9-ROBOT)))

(:derived (Flag21-)(and (ABOVEOF4-ROBOT)))

(:derived (Flag21-)(and (ABOVEOF3-ROBOT)))

(:derived (Flag21-)(and (ABOVEOF13-ROBOT)))

(:derived (Flag21-)(and (ABOVEOF14-ROBOT)))

(:derived (Flag21-)(and (ABOVEOF5-ROBOT)))

(:derived (Flag22-)(and (ABOVEOF11-ROBOT)))

(:derived (Flag22-)(and (ABOVEOF6-ROBOT)))

(:derived (Flag22-)(and (ABOVEOF10-ROBOT)))

(:derived (Flag22-)(and (ABOVEOF8-ROBOT)))

(:derived (Flag22-)(and (ABOVEOF7-ROBOT)))

(:derived (Flag22-)(and (ABOVEOF12-ROBOT)))

(:derived (Flag22-)(and (ABOVEOF9-ROBOT)))

(:derived (Flag22-)(and (ABOVEOF4-ROBOT)))

(:derived (Flag22-)(and (ABOVEOF13-ROBOT)))

(:derived (Flag22-)(and (ABOVEOF14-ROBOT)))

(:derived (Flag22-)(and (ABOVEOF5-ROBOT)))

(:derived (Flag23-)(and (ABOVEOF11-ROBOT)))

(:derived (Flag23-)(and (ABOVEOF6-ROBOT)))

(:derived (Flag23-)(and (ABOVEOF7-ROBOT)))

(:derived (Flag23-)(and (ABOVEOF8-ROBOT)))

(:derived (Flag23-)(and (ABOVEOF10-ROBOT)))

(:derived (Flag23-)(and (ABOVEOF12-ROBOT)))

(:derived (Flag23-)(and (ABOVEOF9-ROBOT)))

(:derived (Flag23-)(and (ABOVEOF13-ROBOT)))

(:derived (Flag23-)(and (ABOVEOF14-ROBOT)))

(:derived (Flag23-)(and (ABOVEOF5-ROBOT)))

(:derived (Flag24-)(and (ABOVEOF11-ROBOT)))

(:derived (Flag24-)(and (ABOVEOF6-ROBOT)))

(:derived (Flag24-)(and (ABOVEOF7-ROBOT)))

(:derived (Flag24-)(and (ABOVEOF8-ROBOT)))

(:derived (Flag24-)(and (ABOVEOF10-ROBOT)))

(:derived (Flag24-)(and (ABOVEOF12-ROBOT)))

(:derived (Flag24-)(and (ABOVEOF9-ROBOT)))

(:derived (Flag24-)(and (ABOVEOF13-ROBOT)))

(:derived (Flag24-)(and (ABOVEOF14-ROBOT)))

(:derived (Flag25-)(and (ABOVEOF11-ROBOT)))

(:derived (Flag25-)(and (ABOVEOF7-ROBOT)))

(:derived (Flag25-)(and (ABOVEOF8-ROBOT)))

(:derived (Flag25-)(and (ABOVEOF10-ROBOT)))

(:derived (Flag25-)(and (ABOVEOF12-ROBOT)))

(:derived (Flag25-)(and (ABOVEOF9-ROBOT)))

(:derived (Flag25-)(and (ABOVEOF13-ROBOT)))

(:derived (Flag25-)(and (ABOVEOF14-ROBOT)))

(:derived (Flag26-)(and (ABOVEOF11-ROBOT)))

(:derived (Flag26-)(and (ABOVEOF8-ROBOT)))

(:derived (Flag26-)(and (ABOVEOF10-ROBOT)))

(:derived (Flag26-)(and (ABOVEOF12-ROBOT)))

(:derived (Flag26-)(and (ABOVEOF9-ROBOT)))

(:derived (Flag26-)(and (ABOVEOF13-ROBOT)))

(:derived (Flag26-)(and (ABOVEOF14-ROBOT)))

(:derived (Flag27-)(and (ABOVEOF11-ROBOT)))

(:derived (Flag27-)(and (ABOVEOF10-ROBOT)))

(:derived (Flag27-)(and (ABOVEOF12-ROBOT)))

(:derived (Flag27-)(and (ABOVEOF9-ROBOT)))

(:derived (Flag27-)(and (ABOVEOF13-ROBOT)))

(:derived (Flag27-)(and (ABOVEOF14-ROBOT)))

(:derived (Flag28-)(and (ABOVEOF12-ROBOT)))

(:derived (Flag28-)(and (ABOVEOF13-ROBOT)))

(:derived (Flag28-)(and (ABOVEOF10-ROBOT)))

(:derived (Flag28-)(and (ABOVEOF11-ROBOT)))

(:derived (Flag28-)(and (ABOVEOF14-ROBOT)))

(:derived (Flag29-)(and (ABOVEOF12-ROBOT)))

(:derived (Flag29-)(and (ABOVEOF13-ROBOT)))

(:derived (Flag29-)(and (ABOVEOF14-ROBOT)))

(:derived (Flag29-)(and (ABOVEOF11-ROBOT)))

(:derived (Flag30-)(and (ABOVEOF12-ROBOT)))

(:derived (Flag30-)(and (ABOVEOF13-ROBOT)))

(:derived (Flag30-)(and (ABOVEOF14-ROBOT)))

(:derived (Flag31-)(and (ABOVEOF13-ROBOT)))

(:derived (Flag31-)(and (ABOVEOF14-ROBOT)))

(:derived (Flag32-)(and (ABOVEOF2-ROBOT)(BELOWOF2-ROBOT)))

(:derived (Flag33-)(and (ABOVEOF11-ROBOT)(BELOWOF1-ROBOT)))

(:derived (Flag34-)(and (ABOVEOF1-ROBOT)(BELOWOF2-ROBOT)))

(:derived (Flag35-)(and (ABOVEOF7-ROBOT)(BELOWOF2-ROBOT)))

(:derived (Flag36-)(and (BELOWOF2-ROBOT)(ABOVEOF13-ROBOT)))

(:derived (Flag37-)(and (BELOWOF1-ROBOT)(ABOVEOF7-ROBOT)))

(:derived (Flag38-)(and (BELOWOF1-ROBOT)(ABOVEOF1-ROBOT)))

(:derived (Flag39-)(and (BELOWOF1-ROBOT)(ABOVEOF5-ROBOT)))

(:derived (Flag40-)(and (ABOVEOF4-ROBOT)(BELOWOF2-ROBOT)))

(:derived (Flag41-)(and (BELOWOF2-ROBOT)(ABOVEOF14-ROBOT)))

(:derived (Flag42-)(and (ABOVEOF6-ROBOT)(BELOWOF2-ROBOT)))

(:derived (Flag43-)(and (BELOWOF1-ROBOT)(ABOVEOF2-ROBOT)))

(:derived (Flag44-)(and (ABOVEOF9-ROBOT)(BELOWOF2-ROBOT)))

(:derived (Flag45-)(and (BELOWOF1-ROBOT)(ABOVEOF13-ROBOT)))

(:derived (Flag46-)(and (ABOVEOF6-ROBOT)(BELOWOF1-ROBOT)))

(:derived (Flag47-)(and (BELOWOF1-ROBOT)(ABOVEOF14-ROBOT)))

(:derived (Flag48-)(and (ABOVEOF10-ROBOT)(BELOWOF1-ROBOT)))

(:derived (Flag49-)(and (ABOVEOF11-ROBOT)(BELOWOF2-ROBOT)))

(:derived (Flag50-)(and (ABOVEOF3-ROBOT)(BELOWOF1-ROBOT)))

(:derived (Flag51-)(and (BELOWOF1-ROBOT)(ABOVEOF9-ROBOT)))

(:derived (Flag52-)(and (ABOVEOF10-ROBOT)(BELOWOF2-ROBOT)))

(:derived (Flag53-)(and (ABOVEOF8-ROBOT)(BELOWOF2-ROBOT)))

(:derived (Flag54-)(and (BELOWOF1-ROBOT)(ABOVEOF4-ROBOT)))

(:derived (Flag55-)(and (ABOVEOF12-ROBOT)(BELOWOF2-ROBOT)))

(:derived (Flag56-)(and (ABOVEOF12-ROBOT)(BELOWOF1-ROBOT)))

(:derived (Flag57-)(and (BELOWOF2-ROBOT)(ABOVEOF5-ROBOT)))

(:derived (Flag58-)(and (ABOVEOF8-ROBOT)(BELOWOF1-ROBOT)))

(:derived (Flag59-)(and (ABOVEOF3-ROBOT)(BELOWOF2-ROBOT)))

(:derived (Flag61-)(and (BELOWOF3-ROBOT)(ABOVEOF5-ROBOT)))

(:derived (Flag62-)(and (BELOWOF3-ROBOT)(ABOVEOF6-ROBOT)))

(:derived (Flag63-)(and (BELOWOF3-ROBOT)(ABOVEOF7-ROBOT)))

(:derived (Flag64-)(and (BELOWOF3-ROBOT)(ABOVEOF14-ROBOT)))

(:derived (Flag65-)(and (BELOWOF3-ROBOT)(ABOVEOF9-ROBOT)))

(:derived (Flag66-)(and (BELOWOF3-ROBOT)(ABOVEOF2-ROBOT)))

(:derived (Flag67-)(and (ABOVEOF3-ROBOT)(BELOWOF3-ROBOT)))

(:derived (Flag68-)(and (ABOVEOF8-ROBOT)(BELOWOF3-ROBOT)))

(:derived (Flag69-)(and (BELOWOF3-ROBOT)(ABOVEOF4-ROBOT)))

(:derived (Flag70-)(and (BELOWOF3-ROBOT)(ABOVEOF10-ROBOT)))

(:derived (Flag71-)(and (BELOWOF2-ROBOT)(ABOVEOF2-ROBOT)))

(:derived (Flag72-)(and (ABOVEOF12-ROBOT)(BELOWOF3-ROBOT)))

(:derived (Flag73-)(and (BELOWOF3-ROBOT)(ABOVEOF13-ROBOT)))

(:derived (Flag74-)(and (ABOVEOF11-ROBOT)(BELOWOF3-ROBOT)))

(:derived (Flag76-)(and (BELOWOF4-ROBOT)(ABOVEOF13-ROBOT)))

(:derived (Flag77-)(and (ABOVEOF3-ROBOT)(BELOWOF4-ROBOT)))

(:derived (Flag78-)(and (ABOVEOF6-ROBOT)(BELOWOF4-ROBOT)))

(:derived (Flag79-)(and (BELOWOF4-ROBOT)(ABOVEOF14-ROBOT)))

(:derived (Flag80-)(and (ABOVEOF9-ROBOT)(BELOWOF4-ROBOT)))

(:derived (Flag81-)(and (BELOWOF4-ROBOT)(ABOVEOF4-ROBOT)))

(:derived (Flag82-)(and (ABOVEOF8-ROBOT)(BELOWOF4-ROBOT)))

(:derived (Flag83-)(and (BELOWOF4-ROBOT)(ABOVEOF7-ROBOT)))

(:derived (Flag84-)(and (ABOVEOF12-ROBOT)(BELOWOF4-ROBOT)))

(:derived (Flag85-)(and (BELOWOF4-ROBOT)(ABOVEOF5-ROBOT)))

(:derived (Flag86-)(and (BELOWOF3-ROBOT)(ABOVEOF3-ROBOT)))

(:derived (Flag87-)(and (ABOVEOF10-ROBOT)(BELOWOF4-ROBOT)))

(:derived (Flag88-)(and (BELOWOF3-ROBOT)(ABOVEOF11-ROBOT)))

(:derived (Flag89-)(and (ABOVEOF11-ROBOT)(BELOWOF4-ROBOT)))

(:derived (Flag91-)(and (ABOVEOF4-ROBOT)(BELOWOF5-ROBOT)))

(:derived (Flag92-)(and (ABOVEOF8-ROBOT)(BELOWOF5-ROBOT)))

(:derived (Flag93-)(and (ABOVEOF6-ROBOT)(BELOWOF5-ROBOT)))

(:derived (Flag94-)(and (ABOVEOF13-ROBOT)(BELOWOF5-ROBOT)))

(:derived (Flag95-)(and (ABOVEOF10-ROBOT)(BELOWOF5-ROBOT)))

(:derived (Flag96-)(and (ABOVEOF11-ROBOT)(BELOWOF5-ROBOT)))

(:derived (Flag97-)(and (ABOVEOF5-ROBOT)(BELOWOF5-ROBOT)))

(:derived (Flag98-)(and (ABOVEOF7-ROBOT)(BELOWOF5-ROBOT)))

(:derived (Flag99-)(and (ABOVEOF9-ROBOT)(BELOWOF5-ROBOT)))

(:derived (Flag100-)(and (ABOVEOF12-ROBOT)(BELOWOF5-ROBOT)))

(:derived (Flag101-)(and (ABOVEOF14-ROBOT)(BELOWOF5-ROBOT)))

(:derived (Flag103-)(and (ABOVEOF11-ROBOT)(BELOWOF6-ROBOT)))

(:derived (Flag104-)(and (BELOWOF6-ROBOT)(ABOVEOF14-ROBOT)))

(:derived (Flag105-)(and (ABOVEOF12-ROBOT)(BELOWOF6-ROBOT)))

(:derived (Flag106-)(and (BELOWOF5-ROBOT)(ABOVEOF13-ROBOT)))

(:derived (Flag107-)(and (BELOWOF6-ROBOT)(ABOVEOF13-ROBOT)))

(:derived (Flag108-)(and (BELOWOF6-ROBOT)(ABOVEOF5-ROBOT)))

(:derived (Flag109-)(and (BELOWOF6-ROBOT)(ABOVEOF7-ROBOT)))

(:derived (Flag110-)(and (BELOWOF5-ROBOT)(ABOVEOF5-ROBOT)))

(:derived (Flag111-)(and (BELOWOF6-ROBOT)(ABOVEOF6-ROBOT)))

(:derived (Flag112-)(and (ABOVEOF8-ROBOT)(BELOWOF6-ROBOT)))

(:derived (Flag113-)(and (BELOWOF6-ROBOT)(ABOVEOF9-ROBOT)))

(:derived (Flag114-)(and (BELOWOF6-ROBOT)(ABOVEOF10-ROBOT)))

(:derived (Flag116-)(and (ABOVEOF6-ROBOT)(BELOWOF7-ROBOT)))

(:derived (Flag117-)(and (ABOVEOF9-ROBOT)(BELOWOF7-ROBOT)))

(:derived (Flag118-)(and (ABOVEOF6-ROBOT)(BELOWOF6-ROBOT)))

(:derived (Flag119-)(and (ABOVEOF10-ROBOT)(BELOWOF7-ROBOT)))

(:derived (Flag120-)(and (ABOVEOF10-ROBOT)(BELOWOF6-ROBOT)))

(:derived (Flag121-)(and (ABOVEOF12-ROBOT)(BELOWOF7-ROBOT)))

(:derived (Flag122-)(and (BELOWOF7-ROBOT)(ABOVEOF14-ROBOT)))

(:derived (Flag123-)(and (BELOWOF7-ROBOT)(ABOVEOF13-ROBOT)))

(:derived (Flag124-)(and (ABOVEOF7-ROBOT)(BELOWOF7-ROBOT)))

(:derived (Flag125-)(and (ABOVEOF8-ROBOT)(BELOWOF7-ROBOT)))

(:derived (Flag126-)(and (ABOVEOF11-ROBOT)(BELOWOF7-ROBOT)))

(:derived (Flag128-)(and (BELOWOF8-ROBOT)(ABOVEOF13-ROBOT)))

(:derived (Flag129-)(and (BELOWOF8-ROBOT)(ABOVEOF10-ROBOT)))

(:derived (Flag130-)(and (ABOVEOF12-ROBOT)(BELOWOF8-ROBOT)))

(:derived (Flag131-)(and (ABOVEOF8-ROBOT)(BELOWOF8-ROBOT)))

(:derived (Flag132-)(and (BELOWOF8-ROBOT)(ABOVEOF9-ROBOT)))

(:derived (Flag133-)(and (BELOWOF7-ROBOT)(ABOVEOF7-ROBOT)))

(:derived (Flag134-)(and (BELOWOF8-ROBOT)(ABOVEOF7-ROBOT)))

(:derived (Flag135-)(and (BELOWOF8-ROBOT)(ABOVEOF11-ROBOT)))

(:derived (Flag136-)(and (BELOWOF8-ROBOT)(ABOVEOF14-ROBOT)))

(:derived (Flag138-)(and (BELOWOF8-ROBOT)(ABOVEOF12-ROBOT)))

(:derived (Flag139-)(and (ABOVEOF10-ROBOT)(BELOWOF9-ROBOT)))

(:derived (Flag140-)(and (BELOWOF8-ROBOT)(ABOVEOF8-ROBOT)))

(:derived (Flag141-)(and (BELOWOF9-ROBOT)(ABOVEOF14-ROBOT)))

(:derived (Flag142-)(and (BELOWOF9-ROBOT)(ABOVEOF13-ROBOT)))

(:derived (Flag143-)(and (ABOVEOF8-ROBOT)(BELOWOF9-ROBOT)))

(:derived (Flag144-)(and (ABOVEOF12-ROBOT)(BELOWOF9-ROBOT)))

(:derived (Flag145-)(and (ABOVEOF11-ROBOT)(BELOWOF9-ROBOT)))

(:derived (Flag146-)(and (BELOWOF9-ROBOT)(ABOVEOF9-ROBOT)))

(:derived (Flag148-)(and (BELOWOF10-ROBOT)(ABOVEOF9-ROBOT)))

(:derived (Flag149-)(and (BELOWOF10-ROBOT)(ABOVEOF14-ROBOT)))

(:derived (Flag150-)(and (BELOWOF10-ROBOT)(ABOVEOF10-ROBOT)))

(:derived (Flag151-)(and (ABOVEOF12-ROBOT)(BELOWOF10-ROBOT)))

(:derived (Flag152-)(and (ABOVEOF9-ROBOT)(BELOWOF9-ROBOT)))

(:derived (Flag153-)(and (ABOVEOF11-ROBOT)(BELOWOF10-ROBOT)))

(:derived (Flag154-)(and (BELOWOF10-ROBOT)(ABOVEOF13-ROBOT)))

(:derived (Flag156-)(and (BELOWOF11-ROBOT)(ABOVEOF10-ROBOT)))

(:derived (Flag157-)(and (ABOVEOF11-ROBOT)(BELOWOF11-ROBOT)))

(:derived (Flag158-)(and (BELOWOF11-ROBOT)(ABOVEOF13-ROBOT)))

(:derived (Flag159-)(and (BELOWOF11-ROBOT)(ABOVEOF14-ROBOT)))

(:derived (Flag160-)(and (ABOVEOF12-ROBOT)(BELOWOF11-ROBOT)))

(:derived (Flag162-)(and (ABOVEOF12-ROBOT)(BELOWOF12-ROBOT)))

(:derived (Flag163-)(and (BELOWOF12-ROBOT)(ABOVEOF13-ROBOT)))

(:derived (Flag164-)(and (BELOWOF12-ROBOT)(ABOVEOF11-ROBOT)))

(:derived (Flag165-)(and (BELOWOF12-ROBOT)(ABOVEOF14-ROBOT)))

(:derived (Flag167-)(and (ABOVEOF14-ROBOT)(BELOWOF13-ROBOT)))

(:derived (Flag168-)(and (ABOVEOF13-ROBOT)(BELOWOF13-ROBOT)))

(:derived (Flag169-)(and (ABOVEOF12-ROBOT)(BELOWOF13-ROBOT)))

(:derived (Flag170-)(and (BELOWOF12-ROBOT)(ABOVEOF12-ROBOT)))

(:derived (Flag172-)(and (BELOWOF14-ROBOT)(ABOVEOF14-ROBOT)))

(:derived (Flag173-)(and (BELOWOF13-ROBOT)(ABOVEOF13-ROBOT)))

(:derived (Flag174-)(and (BELOWOF14-ROBOT)(ABOVEOF13-ROBOT)))

(:derived (Flag176-)(and (BELOWOF15-ROBOT)(ABOVEOF14-ROBOT)))

(:derived (Flag352-)(and (BELOWOF1-ROBOT)))

(:action MOVEUP-ROBOT
:parameters ()
:precondition
(and
(Flag2-)
)
:effect
(and
(when
(and
(Flag175-)
)
(and
(ROW14-ROBOT)
(not (NOT-ROW14-ROBOT))
)
)
(when
(and
(Flag171-)
)
(and
(ROW13-ROBOT)
(not (NOT-ROW13-ROBOT))
)
)
(when
(and
(Flag166-)
)
(and
(ROW12-ROBOT)
(not (NOT-ROW12-ROBOT))
)
)
(when
(and
(Flag161-)
)
(and
(ROW11-ROBOT)
(not (NOT-ROW11-ROBOT))
)
)
(when
(and
(Flag155-)
)
(and
(ROW10-ROBOT)
(not (NOT-ROW10-ROBOT))
)
)
(when
(and
(Flag147-)
)
(and
(ROW9-ROBOT)
(not (NOT-ROW9-ROBOT))
)
)
(when
(and
(Flag137-)
)
(and
(ROW8-ROBOT)
(not (NOT-ROW8-ROBOT))
)
)
(when
(and
(Flag127-)
)
(and
(ROW7-ROBOT)
(not (NOT-ROW7-ROBOT))
)
)
(when
(and
(Flag115-)
)
(and
(ROW6-ROBOT)
(not (NOT-ROW6-ROBOT))
)
)
(when
(and
(Flag102-)
)
(and
(ROW5-ROBOT)
(not (NOT-ROW5-ROBOT))
)
)
(when
(and
(Flag90-)
)
(and
(ROW4-ROBOT)
(not (NOT-ROW4-ROBOT))
)
)
(when
(and
(Flag75-)
)
(and
(ROW3-ROBOT)
(not (NOT-ROW3-ROBOT))
)
)
(when
(and
(Flag60-)
)
(and
(ROW2-ROBOT)
(not (NOT-ROW2-ROBOT))
)
)
(when
(and
(Flag352-)
)
(and
(ROW1-ROBOT)
(not (NOT-ROW1-ROBOT))
)
)
(when
(and
(ROW13-ROBOT)
)
(and
(ROW14-ROBOT)
(NOT-ROW13-ROBOT)
(not (NOT-ROW14-ROBOT))
(not (ROW13-ROBOT))
)
)
(when
(and
(ROW12-ROBOT)
)
(and
(ROW13-ROBOT)
(NOT-ROW12-ROBOT)
(not (NOT-ROW13-ROBOT))
(not (ROW12-ROBOT))
)
)
(when
(and
(ROW11-ROBOT)
)
(and
(ROW12-ROBOT)
(NOT-ROW11-ROBOT)
(not (NOT-ROW12-ROBOT))
(not (ROW11-ROBOT))
)
)
(when
(and
(ROW10-ROBOT)
)
(and
(ROW11-ROBOT)
(NOT-ROW10-ROBOT)
(not (NOT-ROW11-ROBOT))
(not (ROW10-ROBOT))
)
)
(when
(and
(ROW9-ROBOT)
)
(and
(ROW10-ROBOT)
(NOT-ROW9-ROBOT)
(not (NOT-ROW10-ROBOT))
(not (ROW9-ROBOT))
)
)
(when
(and
(ROW8-ROBOT)
)
(and
(ROW9-ROBOT)
(NOT-ROW8-ROBOT)
(not (NOT-ROW9-ROBOT))
(not (ROW8-ROBOT))
)
)
(when
(and
(ROW7-ROBOT)
)
(and
(ROW8-ROBOT)
(NOT-ROW7-ROBOT)
(not (NOT-ROW8-ROBOT))
(not (ROW7-ROBOT))
)
)
(when
(and
(ROW6-ROBOT)
)
(and
(ROW7-ROBOT)
(NOT-ROW6-ROBOT)
(not (NOT-ROW7-ROBOT))
(not (ROW6-ROBOT))
)
)
(when
(and
(ROW5-ROBOT)
)
(and
(ROW6-ROBOT)
(NOT-ROW5-ROBOT)
(not (NOT-ROW6-ROBOT))
(not (ROW5-ROBOT))
)
)
(when
(and
(ROW4-ROBOT)
)
(and
(ROW5-ROBOT)
(NOT-ROW4-ROBOT)
(not (NOT-ROW5-ROBOT))
(not (ROW4-ROBOT))
)
)
(when
(and
(ROW3-ROBOT)
)
(and
(ROW4-ROBOT)
(NOT-ROW3-ROBOT)
(not (NOT-ROW4-ROBOT))
(not (ROW3-ROBOT))
)
)
(when
(and
(ROW2-ROBOT)
)
(and
(ROW3-ROBOT)
(NOT-ROW2-ROBOT)
(not (NOT-ROW3-ROBOT))
(not (ROW2-ROBOT))
)
)
(when
(and
(ROW1-ROBOT)
)
(and
(ROW2-ROBOT)
(NOT-ROW1-ROBOT)
(not (NOT-ROW2-ROBOT))
(not (ROW1-ROBOT))
)
)
(when
(and
(ROW0-ROBOT)
)
(and
(ROW1-ROBOT)
(NOT-ROW0-ROBOT)
(not (NOT-ROW1-ROBOT))
(not (ROW0-ROBOT))
)
)
(when
(and
(BELOWOF14-ROBOT)
)
(and
(BELOWOF15-ROBOT)
(NOT-BELOWOF14-ROBOT)
(not (NOT-BELOWOF15-ROBOT))
(not (BELOWOF14-ROBOT))
)
)
(when
(and
(BELOWOF8-ROBOT)
)
(and
(BELOWOF15-ROBOT)
(NOT-BELOWOF14-ROBOT)
(BELOWOF14-ROBOT)
(NOT-BELOWOF13-ROBOT)
(BELOWOF13-ROBOT)
(NOT-BELOWOF12-ROBOT)
(BELOWOF12-ROBOT)
(NOT-BELOWOF11-ROBOT)
(BELOWOF11-ROBOT)
(NOT-BELOWOF10-ROBOT)
(BELOWOF10-ROBOT)
(NOT-BELOWOF9-ROBOT)
(BELOWOF9-ROBOT)
(NOT-BELOWOF8-ROBOT)
(not (NOT-BELOWOF15-ROBOT))
(not (BELOWOF8-ROBOT))
)
)
(when
(and
(BELOWOF13-ROBOT)
)
(and
(BELOWOF15-ROBOT)
(NOT-BELOWOF14-ROBOT)
(BELOWOF14-ROBOT)
(NOT-BELOWOF13-ROBOT)
(not (NOT-BELOWOF15-ROBOT))
(not (BELOWOF13-ROBOT))
)
)
(when
(and
(BELOWOF3-ROBOT)
)
(and
(BELOWOF15-ROBOT)
(NOT-BELOWOF14-ROBOT)
(BELOWOF14-ROBOT)
(NOT-BELOWOF13-ROBOT)
(BELOWOF13-ROBOT)
(NOT-BELOWOF12-ROBOT)
(BELOWOF12-ROBOT)
(NOT-BELOWOF11-ROBOT)
(BELOWOF11-ROBOT)
(NOT-BELOWOF10-ROBOT)
(BELOWOF10-ROBOT)
(NOT-BELOWOF9-ROBOT)
(BELOWOF9-ROBOT)
(NOT-BELOWOF8-ROBOT)
(BELOWOF8-ROBOT)
(NOT-BELOWOF7-ROBOT)
(BELOWOF7-ROBOT)
(NOT-BELOWOF6-ROBOT)
(BELOWOF6-ROBOT)
(NOT-BELOWOF5-ROBOT)
(BELOWOF5-ROBOT)
(NOT-BELOWOF4-ROBOT)
(BELOWOF4-ROBOT)
(NOT-BELOWOF3-ROBOT)
(not (NOT-BELOWOF15-ROBOT))
(not (BELOWOF3-ROBOT))
)
)
(when
(and
(BELOWOF9-ROBOT)
)
(and
(BELOWOF15-ROBOT)
(NOT-BELOWOF14-ROBOT)
(BELOWOF14-ROBOT)
(NOT-BELOWOF13-ROBOT)
(BELOWOF13-ROBOT)
(NOT-BELOWOF12-ROBOT)
(BELOWOF12-ROBOT)
(NOT-BELOWOF11-ROBOT)
(BELOWOF11-ROBOT)
(NOT-BELOWOF10-ROBOT)
(BELOWOF10-ROBOT)
(NOT-BELOWOF9-ROBOT)
(not (NOT-BELOWOF15-ROBOT))
(not (BELOWOF9-ROBOT))
)
)
(when
(and
(BELOWOF7-ROBOT)
)
(and
(BELOWOF15-ROBOT)
(NOT-BELOWOF14-ROBOT)
(BELOWOF14-ROBOT)
(NOT-BELOWOF13-ROBOT)
(BELOWOF13-ROBOT)
(NOT-BELOWOF12-ROBOT)
(BELOWOF12-ROBOT)
(NOT-BELOWOF11-ROBOT)
(BELOWOF11-ROBOT)
(NOT-BELOWOF10-ROBOT)
(BELOWOF10-ROBOT)
(NOT-BELOWOF9-ROBOT)
(BELOWOF9-ROBOT)
(NOT-BELOWOF8-ROBOT)
(BELOWOF8-ROBOT)
(NOT-BELOWOF7-ROBOT)
(not (NOT-BELOWOF15-ROBOT))
(not (BELOWOF7-ROBOT))
)
)
(when
(and
(BELOWOF12-ROBOT)
)
(and
(BELOWOF15-ROBOT)
(NOT-BELOWOF14-ROBOT)
(BELOWOF14-ROBOT)
(NOT-BELOWOF13-ROBOT)
(BELOWOF13-ROBOT)
(NOT-BELOWOF12-ROBOT)
(not (NOT-BELOWOF15-ROBOT))
(not (BELOWOF12-ROBOT))
)
)
(when
(and
(BELOWOF4-ROBOT)
)
(and
(BELOWOF15-ROBOT)
(NOT-BELOWOF14-ROBOT)
(BELOWOF14-ROBOT)
(NOT-BELOWOF13-ROBOT)
(BELOWOF13-ROBOT)
(NOT-BELOWOF12-ROBOT)
(BELOWOF12-ROBOT)
(NOT-BELOWOF11-ROBOT)
(BELOWOF11-ROBOT)
(NOT-BELOWOF10-ROBOT)
(BELOWOF10-ROBOT)
(NOT-BELOWOF9-ROBOT)
(BELOWOF9-ROBOT)
(NOT-BELOWOF8-ROBOT)
(BELOWOF8-ROBOT)
(NOT-BELOWOF7-ROBOT)
(BELOWOF7-ROBOT)
(NOT-BELOWOF6-ROBOT)
(BELOWOF6-ROBOT)
(NOT-BELOWOF5-ROBOT)
(BELOWOF5-ROBOT)
(NOT-BELOWOF4-ROBOT)
(not (NOT-BELOWOF15-ROBOT))
(not (BELOWOF4-ROBOT))
)
)
(when
(and
(BELOWOF1-ROBOT)
)
(and
(BELOWOF15-ROBOT)
(NOT-BELOWOF14-ROBOT)
(BELOWOF14-ROBOT)
(NOT-BELOWOF13-ROBOT)
(BELOWOF13-ROBOT)
(NOT-BELOWOF12-ROBOT)
(BELOWOF12-ROBOT)
(NOT-BELOWOF11-ROBOT)
(BELOWOF11-ROBOT)
(NOT-BELOWOF10-ROBOT)
(BELOWOF10-ROBOT)
(NOT-BELOWOF9-ROBOT)
(BELOWOF9-ROBOT)
(NOT-BELOWOF8-ROBOT)
(BELOWOF8-ROBOT)
(NOT-BELOWOF7-ROBOT)
(BELOWOF7-ROBOT)
(NOT-BELOWOF6-ROBOT)
(BELOWOF6-ROBOT)
(NOT-BELOWOF5-ROBOT)
(BELOWOF5-ROBOT)
(NOT-BELOWOF4-ROBOT)
(BELOWOF4-ROBOT)
(NOT-BELOWOF3-ROBOT)
(BELOWOF3-ROBOT)
(NOT-BELOWOF2-ROBOT)
(BELOWOF2-ROBOT)
(NOT-BELOWOF1-ROBOT)
(not (NOT-BELOWOF15-ROBOT))
(not (BELOWOF1-ROBOT))
)
)
(when
(and
(BELOWOF2-ROBOT)
)
(and
(BELOWOF15-ROBOT)
(NOT-BELOWOF14-ROBOT)
(BELOWOF14-ROBOT)
(NOT-BELOWOF13-ROBOT)
(BELOWOF13-ROBOT)
(NOT-BELOWOF12-ROBOT)
(BELOWOF12-ROBOT)
(NOT-BELOWOF11-ROBOT)
(BELOWOF11-ROBOT)
(NOT-BELOWOF10-ROBOT)
(BELOWOF10-ROBOT)
(NOT-BELOWOF9-ROBOT)
(BELOWOF9-ROBOT)
(NOT-BELOWOF8-ROBOT)
(BELOWOF8-ROBOT)
(NOT-BELOWOF7-ROBOT)
(BELOWOF7-ROBOT)
(NOT-BELOWOF6-ROBOT)
(BELOWOF6-ROBOT)
(NOT-BELOWOF5-ROBOT)
(BELOWOF5-ROBOT)
(NOT-BELOWOF4-ROBOT)
(BELOWOF4-ROBOT)
(NOT-BELOWOF3-ROBOT)
(BELOWOF3-ROBOT)
(NOT-BELOWOF2-ROBOT)
(not (NOT-BELOWOF15-ROBOT))
(not (BELOWOF2-ROBOT))
)
)
(when
(and
(BELOWOF5-ROBOT)
)
(and
(BELOWOF15-ROBOT)
(NOT-BELOWOF14-ROBOT)
(BELOWOF14-ROBOT)
(NOT-BELOWOF13-ROBOT)
(BELOWOF13-ROBOT)
(NOT-BELOWOF12-ROBOT)
(BELOWOF12-ROBOT)
(NOT-BELOWOF11-ROBOT)
(BELOWOF11-ROBOT)
(NOT-BELOWOF10-ROBOT)
(BELOWOF10-ROBOT)
(NOT-BELOWOF9-ROBOT)
(BELOWOF9-ROBOT)
(NOT-BELOWOF8-ROBOT)
(BELOWOF8-ROBOT)
(NOT-BELOWOF7-ROBOT)
(BELOWOF7-ROBOT)
(NOT-BELOWOF6-ROBOT)
(BELOWOF6-ROBOT)
(NOT-BELOWOF5-ROBOT)
(not (NOT-BELOWOF15-ROBOT))
(not (BELOWOF5-ROBOT))
)
)
(when
(and
(BELOWOF6-ROBOT)
)
(and
(BELOWOF15-ROBOT)
(NOT-BELOWOF14-ROBOT)
(BELOWOF14-ROBOT)
(NOT-BELOWOF13-ROBOT)
(BELOWOF13-ROBOT)
(NOT-BELOWOF12-ROBOT)
(BELOWOF12-ROBOT)
(NOT-BELOWOF11-ROBOT)
(BELOWOF11-ROBOT)
(NOT-BELOWOF10-ROBOT)
(BELOWOF10-ROBOT)
(NOT-BELOWOF9-ROBOT)
(BELOWOF9-ROBOT)
(NOT-BELOWOF8-ROBOT)
(BELOWOF8-ROBOT)
(NOT-BELOWOF7-ROBOT)
(BELOWOF7-ROBOT)
(NOT-BELOWOF6-ROBOT)
(not (NOT-BELOWOF15-ROBOT))
(not (BELOWOF6-ROBOT))
)
)
(when
(and
(BELOWOF11-ROBOT)
)
(and
(BELOWOF15-ROBOT)
(NOT-BELOWOF14-ROBOT)
(BELOWOF14-ROBOT)
(NOT-BELOWOF13-ROBOT)
(BELOWOF13-ROBOT)
(NOT-BELOWOF12-ROBOT)
(BELOWOF12-ROBOT)
(NOT-BELOWOF11-ROBOT)
(not (NOT-BELOWOF15-ROBOT))
(not (BELOWOF11-ROBOT))
)
)
(when
(and
(BELOWOF10-ROBOT)
)
(and
(BELOWOF15-ROBOT)
(NOT-BELOWOF14-ROBOT)
(BELOWOF14-ROBOT)
(NOT-BELOWOF13-ROBOT)
(BELOWOF13-ROBOT)
(NOT-BELOWOF12-ROBOT)
(BELOWOF12-ROBOT)
(NOT-BELOWOF11-ROBOT)
(BELOWOF11-ROBOT)
(NOT-BELOWOF10-ROBOT)
(not (NOT-BELOWOF15-ROBOT))
(not (BELOWOF10-ROBOT))
)
)
(when
(and
(ABOVEOF14-ROBOT)
)
(and
(ABOVEOF13-ROBOT)
(ABOVEOF12-ROBOT)
(ABOVEOF11-ROBOT)
(ABOVEOF10-ROBOT)
(ABOVEOF9-ROBOT)
(ABOVEOF8-ROBOT)
(ABOVEOF7-ROBOT)
(ABOVEOF6-ROBOT)
(ABOVEOF5-ROBOT)
(ABOVEOF4-ROBOT)
(ABOVEOF3-ROBOT)
(ABOVEOF2-ROBOT)
(not (NOT-ABOVEOF14-ROBOT))
(not (NOT-ABOVEOF13-ROBOT))
(not (NOT-ABOVEOF12-ROBOT))
(not (NOT-ABOVEOF11-ROBOT))
(not (NOT-ABOVEOF10-ROBOT))
(not (NOT-ABOVEOF9-ROBOT))
(not (NOT-ABOVEOF8-ROBOT))
(not (NOT-ABOVEOF7-ROBOT))
(not (NOT-ABOVEOF6-ROBOT))
(not (NOT-ABOVEOF5-ROBOT))
(not (NOT-ABOVEOF4-ROBOT))
(not (NOT-ABOVEOF3-ROBOT))
(not (NOT-ABOVEOF2-ROBOT))
)
)
(when
(and
(ABOVEOF13-ROBOT)
)
(and
(ABOVEOF14-ROBOT)
(ABOVEOF12-ROBOT)
(ABOVEOF11-ROBOT)
(ABOVEOF10-ROBOT)
(ABOVEOF9-ROBOT)
(ABOVEOF8-ROBOT)
(ABOVEOF7-ROBOT)
(ABOVEOF6-ROBOT)
(ABOVEOF5-ROBOT)
(ABOVEOF4-ROBOT)
(ABOVEOF3-ROBOT)
(ABOVEOF2-ROBOT)
(not (NOT-ABOVEOF14-ROBOT))
(not (NOT-ABOVEOF13-ROBOT))
(not (NOT-ABOVEOF12-ROBOT))
(not (NOT-ABOVEOF11-ROBOT))
(not (NOT-ABOVEOF10-ROBOT))
(not (NOT-ABOVEOF9-ROBOT))
(not (NOT-ABOVEOF8-ROBOT))
(not (NOT-ABOVEOF7-ROBOT))
(not (NOT-ABOVEOF6-ROBOT))
(not (NOT-ABOVEOF5-ROBOT))
(not (NOT-ABOVEOF4-ROBOT))
(not (NOT-ABOVEOF3-ROBOT))
(not (NOT-ABOVEOF2-ROBOT))
)
)
(when
(and
(ABOVEOF12-ROBOT)
)
(and
(ABOVEOF13-ROBOT)
(ABOVEOF11-ROBOT)
(ABOVEOF10-ROBOT)
(ABOVEOF9-ROBOT)
(ABOVEOF8-ROBOT)
(ABOVEOF7-ROBOT)
(ABOVEOF6-ROBOT)
(ABOVEOF5-ROBOT)
(ABOVEOF4-ROBOT)
(ABOVEOF3-ROBOT)
(ABOVEOF2-ROBOT)
(not (NOT-ABOVEOF13-ROBOT))
(not (NOT-ABOVEOF12-ROBOT))
(not (NOT-ABOVEOF11-ROBOT))
(not (NOT-ABOVEOF10-ROBOT))
(not (NOT-ABOVEOF9-ROBOT))
(not (NOT-ABOVEOF8-ROBOT))
(not (NOT-ABOVEOF7-ROBOT))
(not (NOT-ABOVEOF6-ROBOT))
(not (NOT-ABOVEOF5-ROBOT))
(not (NOT-ABOVEOF4-ROBOT))
(not (NOT-ABOVEOF3-ROBOT))
(not (NOT-ABOVEOF2-ROBOT))
)
)
(when
(and
(ABOVEOF11-ROBOT)
)
(and
(ABOVEOF12-ROBOT)
(ABOVEOF10-ROBOT)
(ABOVEOF9-ROBOT)
(ABOVEOF8-ROBOT)
(ABOVEOF7-ROBOT)
(ABOVEOF6-ROBOT)
(ABOVEOF5-ROBOT)
(ABOVEOF4-ROBOT)
(ABOVEOF3-ROBOT)
(ABOVEOF2-ROBOT)
(not (NOT-ABOVEOF12-ROBOT))
(not (NOT-ABOVEOF11-ROBOT))
(not (NOT-ABOVEOF10-ROBOT))
(not (NOT-ABOVEOF9-ROBOT))
(not (NOT-ABOVEOF8-ROBOT))
(not (NOT-ABOVEOF7-ROBOT))
(not (NOT-ABOVEOF6-ROBOT))
(not (NOT-ABOVEOF5-ROBOT))
(not (NOT-ABOVEOF4-ROBOT))
(not (NOT-ABOVEOF3-ROBOT))
(not (NOT-ABOVEOF2-ROBOT))
)
)
(when
(and
(ABOVEOF10-ROBOT)
)
(and
(ABOVEOF11-ROBOT)
(ABOVEOF9-ROBOT)
(ABOVEOF8-ROBOT)
(ABOVEOF7-ROBOT)
(ABOVEOF6-ROBOT)
(ABOVEOF5-ROBOT)
(ABOVEOF4-ROBOT)
(ABOVEOF3-ROBOT)
(ABOVEOF2-ROBOT)
(not (NOT-ABOVEOF11-ROBOT))
(not (NOT-ABOVEOF10-ROBOT))
(not (NOT-ABOVEOF9-ROBOT))
(not (NOT-ABOVEOF8-ROBOT))
(not (NOT-ABOVEOF7-ROBOT))
(not (NOT-ABOVEOF6-ROBOT))
(not (NOT-ABOVEOF5-ROBOT))
(not (NOT-ABOVEOF4-ROBOT))
(not (NOT-ABOVEOF3-ROBOT))
(not (NOT-ABOVEOF2-ROBOT))
)
)
(when
(and
(ABOVEOF9-ROBOT)
)
(and
(ABOVEOF10-ROBOT)
(ABOVEOF8-ROBOT)
(ABOVEOF7-ROBOT)
(ABOVEOF6-ROBOT)
(ABOVEOF5-ROBOT)
(ABOVEOF4-ROBOT)
(ABOVEOF3-ROBOT)
(ABOVEOF2-ROBOT)
(not (NOT-ABOVEOF10-ROBOT))
(not (NOT-ABOVEOF9-ROBOT))
(not (NOT-ABOVEOF8-ROBOT))
(not (NOT-ABOVEOF7-ROBOT))
(not (NOT-ABOVEOF6-ROBOT))
(not (NOT-ABOVEOF5-ROBOT))
(not (NOT-ABOVEOF4-ROBOT))
(not (NOT-ABOVEOF3-ROBOT))
(not (NOT-ABOVEOF2-ROBOT))
)
)
(when
(and
(ABOVEOF8-ROBOT)
)
(and
(ABOVEOF9-ROBOT)
(ABOVEOF7-ROBOT)
(ABOVEOF6-ROBOT)
(ABOVEOF5-ROBOT)
(ABOVEOF4-ROBOT)
(ABOVEOF3-ROBOT)
(ABOVEOF2-ROBOT)
(not (NOT-ABOVEOF9-ROBOT))
(not (NOT-ABOVEOF8-ROBOT))
(not (NOT-ABOVEOF7-ROBOT))
(not (NOT-ABOVEOF6-ROBOT))
(not (NOT-ABOVEOF5-ROBOT))
(not (NOT-ABOVEOF4-ROBOT))
(not (NOT-ABOVEOF3-ROBOT))
(not (NOT-ABOVEOF2-ROBOT))
)
)
(when
(and
(ABOVEOF7-ROBOT)
)
(and
(ABOVEOF8-ROBOT)
(ABOVEOF6-ROBOT)
(ABOVEOF5-ROBOT)
(ABOVEOF4-ROBOT)
(ABOVEOF3-ROBOT)
(ABOVEOF2-ROBOT)
(not (NOT-ABOVEOF8-ROBOT))
(not (NOT-ABOVEOF7-ROBOT))
(not (NOT-ABOVEOF6-ROBOT))
(not (NOT-ABOVEOF5-ROBOT))
(not (NOT-ABOVEOF4-ROBOT))
(not (NOT-ABOVEOF3-ROBOT))
(not (NOT-ABOVEOF2-ROBOT))
)
)
(when
(and
(ABOVEOF6-ROBOT)
)
(and
(ABOVEOF7-ROBOT)
(ABOVEOF5-ROBOT)
(ABOVEOF4-ROBOT)
(ABOVEOF3-ROBOT)
(ABOVEOF2-ROBOT)
(not (NOT-ABOVEOF7-ROBOT))
(not (NOT-ABOVEOF6-ROBOT))
(not (NOT-ABOVEOF5-ROBOT))
(not (NOT-ABOVEOF4-ROBOT))
(not (NOT-ABOVEOF3-ROBOT))
(not (NOT-ABOVEOF2-ROBOT))
)
)
(when
(and
(ABOVEOF5-ROBOT)
)
(and
(ABOVEOF6-ROBOT)
(ABOVEOF4-ROBOT)
(ABOVEOF3-ROBOT)
(ABOVEOF2-ROBOT)
(not (NOT-ABOVEOF6-ROBOT))
(not (NOT-ABOVEOF5-ROBOT))
(not (NOT-ABOVEOF4-ROBOT))
(not (NOT-ABOVEOF3-ROBOT))
(not (NOT-ABOVEOF2-ROBOT))
)
)
(when
(and
(ABOVEOF4-ROBOT)
)
(and
(ABOVEOF5-ROBOT)
(ABOVEOF3-ROBOT)
(ABOVEOF2-ROBOT)
(not (NOT-ABOVEOF5-ROBOT))
(not (NOT-ABOVEOF4-ROBOT))
(not (NOT-ABOVEOF3-ROBOT))
(not (NOT-ABOVEOF2-ROBOT))
)
)
(when
(and
(ABOVEOF3-ROBOT)
)
(and
(ABOVEOF4-ROBOT)
(ABOVEOF2-ROBOT)
(not (NOT-ABOVEOF4-ROBOT))
(not (NOT-ABOVEOF3-ROBOT))
(not (NOT-ABOVEOF2-ROBOT))
)
)
(when
(and
(ABOVEOF2-ROBOT)
)
(and
(ABOVEOF3-ROBOT)
(not (NOT-ABOVEOF3-ROBOT))
(not (NOT-ABOVEOF2-ROBOT))
)
)
(when
(and
(ABOVEOF1-ROBOT)
)
(and
(ABOVEOF2-ROBOT)
(not (NOT-ABOVEOF2-ROBOT))
)
)
(ABOVEOF1-ROBOT)
(CHECKCONSISTENCY-)
(not (NOT-ABOVEOF1-ROBOT))
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action MOVEDOWN-ROBOT
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
)
:effect
(and
(when
(and
(Flag177-)
)
(and
(ROW13-ROBOT)
(not (NOT-ROW13-ROBOT))
)
)
(when
(and
(Flag175-)
)
(and
(ROW12-ROBOT)
(not (NOT-ROW12-ROBOT))
)
)
(when
(and
(Flag171-)
)
(and
(ROW11-ROBOT)
(not (NOT-ROW11-ROBOT))
)
)
(when
(and
(Flag166-)
)
(and
(ROW10-ROBOT)
(not (NOT-ROW10-ROBOT))
)
)
(when
(and
(Flag161-)
)
(and
(ROW9-ROBOT)
(not (NOT-ROW9-ROBOT))
)
)
(when
(and
(Flag155-)
)
(and
(ROW8-ROBOT)
(not (NOT-ROW8-ROBOT))
)
)
(when
(and
(Flag147-)
)
(and
(ROW7-ROBOT)
(not (NOT-ROW7-ROBOT))
)
)
(when
(and
(Flag137-)
)
(and
(ROW6-ROBOT)
(not (NOT-ROW6-ROBOT))
)
)
(when
(and
(Flag127-)
)
(and
(ROW5-ROBOT)
(not (NOT-ROW5-ROBOT))
)
)
(when
(and
(Flag115-)
)
(and
(ROW4-ROBOT)
(not (NOT-ROW4-ROBOT))
)
)
(when
(and
(Flag102-)
)
(and
(ROW3-ROBOT)
(not (NOT-ROW3-ROBOT))
)
)
(when
(and
(Flag90-)
)
(and
(ROW2-ROBOT)
(not (NOT-ROW2-ROBOT))
)
)
(when
(and
(Flag75-)
)
(and
(ROW1-ROBOT)
(not (NOT-ROW1-ROBOT))
)
)
(when
(and
(Flag60-)
)
(and
(ROW0-ROBOT)
(not (NOT-ROW0-ROBOT))
)
)
(when
(and
(ROW14-ROBOT)
)
(and
(ROW13-ROBOT)
(NOT-ROW14-ROBOT)
(not (NOT-ROW13-ROBOT))
(not (ROW14-ROBOT))
)
)
(when
(and
(ROW13-ROBOT)
)
(and
(ROW12-ROBOT)
(NOT-ROW13-ROBOT)
(not (NOT-ROW12-ROBOT))
(not (ROW13-ROBOT))
)
)
(when
(and
(ROW12-ROBOT)
)
(and
(ROW11-ROBOT)
(NOT-ROW12-ROBOT)
(not (NOT-ROW11-ROBOT))
(not (ROW12-ROBOT))
)
)
(when
(and
(ROW11-ROBOT)
)
(and
(ROW10-ROBOT)
(NOT-ROW11-ROBOT)
(not (NOT-ROW10-ROBOT))
(not (ROW11-ROBOT))
)
)
(when
(and
(ROW10-ROBOT)
)
(and
(ROW9-ROBOT)
(NOT-ROW10-ROBOT)
(not (NOT-ROW9-ROBOT))
(not (ROW10-ROBOT))
)
)
(when
(and
(ROW9-ROBOT)
)
(and
(ROW8-ROBOT)
(NOT-ROW9-ROBOT)
(not (NOT-ROW8-ROBOT))
(not (ROW9-ROBOT))
)
)
(when
(and
(ROW8-ROBOT)
)
(and
(ROW7-ROBOT)
(NOT-ROW8-ROBOT)
(not (NOT-ROW7-ROBOT))
(not (ROW8-ROBOT))
)
)
(when
(and
(ROW7-ROBOT)
)
(and
(ROW6-ROBOT)
(NOT-ROW7-ROBOT)
(not (NOT-ROW6-ROBOT))
(not (ROW7-ROBOT))
)
)
(when
(and
(ROW6-ROBOT)
)
(and
(ROW5-ROBOT)
(NOT-ROW6-ROBOT)
(not (NOT-ROW5-ROBOT))
(not (ROW6-ROBOT))
)
)
(when
(and
(ROW5-ROBOT)
)
(and
(ROW4-ROBOT)
(NOT-ROW5-ROBOT)
(not (NOT-ROW4-ROBOT))
(not (ROW5-ROBOT))
)
)
(when
(and
(ROW4-ROBOT)
)
(and
(ROW3-ROBOT)
(NOT-ROW4-ROBOT)
(not (NOT-ROW3-ROBOT))
(not (ROW4-ROBOT))
)
)
(when
(and
(ROW3-ROBOT)
)
(and
(ROW2-ROBOT)
(NOT-ROW3-ROBOT)
(not (NOT-ROW2-ROBOT))
(not (ROW3-ROBOT))
)
)
(when
(and
(ROW2-ROBOT)
)
(and
(ROW1-ROBOT)
(NOT-ROW2-ROBOT)
(not (NOT-ROW1-ROBOT))
(not (ROW2-ROBOT))
)
)
(when
(and
(ROW1-ROBOT)
)
(and
(ROW0-ROBOT)
(NOT-ROW1-ROBOT)
(not (NOT-ROW0-ROBOT))
(not (ROW1-ROBOT))
)
)
(when
(and
(ABOVEOF14-ROBOT)
)
(and
(ABOVEOF13-ROBOT)
(NOT-ABOVEOF14-ROBOT)
(not (NOT-ABOVEOF13-ROBOT))
(not (ABOVEOF14-ROBOT))
)
)
(when
(and
(Flag31-)
)
(and
(ABOVEOF12-ROBOT)
(NOT-ABOVEOF13-ROBOT)
(not (NOT-ABOVEOF12-ROBOT))
(not (ABOVEOF13-ROBOT))
)
)
(when
(and
(Flag30-)
)
(and
(ABOVEOF11-ROBOT)
(NOT-ABOVEOF12-ROBOT)
(not (NOT-ABOVEOF11-ROBOT))
(not (ABOVEOF12-ROBOT))
)
)
(when
(and
(Flag29-)
)
(and
(ABOVEOF10-ROBOT)
(NOT-ABOVEOF11-ROBOT)
(not (NOT-ABOVEOF10-ROBOT))
(not (ABOVEOF11-ROBOT))
)
)
(when
(and
(Flag28-)
)
(and
(ABOVEOF9-ROBOT)
(NOT-ABOVEOF10-ROBOT)
(not (NOT-ABOVEOF9-ROBOT))
(not (ABOVEOF10-ROBOT))
)
)
(when
(and
(Flag27-)
)
(and
(ABOVEOF8-ROBOT)
(NOT-ABOVEOF9-ROBOT)
(not (NOT-ABOVEOF8-ROBOT))
(not (ABOVEOF9-ROBOT))
)
)
(when
(and
(Flag26-)
)
(and
(ABOVEOF7-ROBOT)
(NOT-ABOVEOF8-ROBOT)
(not (NOT-ABOVEOF7-ROBOT))
(not (ABOVEOF8-ROBOT))
)
)
(when
(and
(Flag25-)
)
(and
(ABOVEOF6-ROBOT)
(NOT-ABOVEOF7-ROBOT)
(not (NOT-ABOVEOF6-ROBOT))
(not (ABOVEOF7-ROBOT))
)
)
(when
(and
(Flag24-)
)
(and
(ABOVEOF5-ROBOT)
(NOT-ABOVEOF6-ROBOT)
(not (NOT-ABOVEOF5-ROBOT))
(not (ABOVEOF6-ROBOT))
)
)
(when
(and
(Flag23-)
)
(and
(ABOVEOF4-ROBOT)
(NOT-ABOVEOF5-ROBOT)
(not (NOT-ABOVEOF4-ROBOT))
(not (ABOVEOF5-ROBOT))
)
)
(when
(and
(Flag22-)
)
(and
(ABOVEOF3-ROBOT)
(NOT-ABOVEOF4-ROBOT)
(not (NOT-ABOVEOF3-ROBOT))
(not (ABOVEOF4-ROBOT))
)
)
(when
(and
(Flag21-)
)
(and
(ABOVEOF2-ROBOT)
(NOT-ABOVEOF3-ROBOT)
(not (NOT-ABOVEOF2-ROBOT))
(not (ABOVEOF3-ROBOT))
)
)
(when
(and
(Flag20-)
)
(and
(ABOVEOF1-ROBOT)
(NOT-ABOVEOF2-ROBOT)
(not (NOT-ABOVEOF1-ROBOT))
(not (ABOVEOF2-ROBOT))
)
)
(when
(and
(Flag19-)
)
(and
(NOT-ABOVEOF1-ROBOT)
(not (ABOVEOF1-ROBOT))
)
)
(when
(and
(Flag18-)
)
(and
(BELOWOF14-ROBOT)
(not (NOT-BELOWOF14-ROBOT))
)
)
(when
(and
(Flag17-)
)
(and
(BELOWOF13-ROBOT)
(not (NOT-BELOWOF13-ROBOT))
)
)
(when
(and
(Flag16-)
)
(and
(BELOWOF12-ROBOT)
(not (NOT-BELOWOF12-ROBOT))
)
)
(when
(and
(Flag15-)
)
(and
(BELOWOF11-ROBOT)
(not (NOT-BELOWOF11-ROBOT))
)
)
(when
(and
(Flag14-)
)
(and
(BELOWOF10-ROBOT)
(not (NOT-BELOWOF10-ROBOT))
)
)
(when
(and
(Flag13-)
)
(and
(BELOWOF9-ROBOT)
(not (NOT-BELOWOF9-ROBOT))
)
)
(when
(and
(Flag12-)
)
(and
(BELOWOF8-ROBOT)
(not (NOT-BELOWOF8-ROBOT))
)
)
(when
(and
(Flag11-)
)
(and
(BELOWOF7-ROBOT)
(not (NOT-BELOWOF7-ROBOT))
)
)
(when
(and
(Flag10-)
)
(and
(BELOWOF6-ROBOT)
(not (NOT-BELOWOF6-ROBOT))
)
)
(when
(and
(Flag9-)
)
(and
(BELOWOF5-ROBOT)
(not (NOT-BELOWOF5-ROBOT))
)
)
(when
(and
(Flag8-)
)
(and
(BELOWOF4-ROBOT)
(not (NOT-BELOWOF4-ROBOT))
)
)
(when
(and
(Flag7-)
)
(and
(BELOWOF3-ROBOT)
(not (NOT-BELOWOF3-ROBOT))
)
)
(when
(and
(Flag6-)
)
(and
(BELOWOF2-ROBOT)
(not (NOT-BELOWOF2-ROBOT))
)
)
(when
(and
(Flag5-)
)
(and
(BELOWOF1-ROBOT)
(not (NOT-BELOWOF1-ROBOT))
)
)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:derived (Flag2-)(and (NOT-CHECKCONSISTENCY-)(NOT-ERROR-)))

(:derived (Flag3-)(and (RIGHTOF2-ROBOT)))

(:derived (Flag3-)(and (LEFTOF14-ROBOT)))

(:derived (Flag17-)(and (BELOWOF14-ROBOT)))

(:derived (Flag18-)(and (BELOWOF14-ROBOT)))

(:derived (Flag178-)(and (RIGHTOF2-ROBOT)))

(:derived (Flag179-)(and (RIGHTOF2-ROBOT)))

(:derived (Flag180-)(and (RIGHTOF2-ROBOT)))

(:derived (Flag204-)(and (LEFTOF14-ROBOT)))

(:derived (Flag349-)(and (LEFTOF14-ROBOT)))

)
