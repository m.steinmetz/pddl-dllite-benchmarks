(define (domain grounded-SIMPLE-ADL-ROBOT)
(:requirements
:strips
)
(:predicates
(Flag150-)
(Flag93-)
(Flag77-)
(Flag19-)
(Flag7-)
(Flag6-)
(Flag5-)
(Flag3-)
(Flag2-)
(ROW8-ROBOT)
(ROW7-ROBOT)
(ROW6-ROBOT)
(ROW5-ROBOT)
(ROW4-ROBOT)
(ROW3-ROBOT)
(ROW2-ROBOT)
(ROW1-ROBOT)
(BELOWOF9-ROBOT)
(NOT-BELOWOF8-ROBOT)
(BELOWOF7-ROBOT)
(BELOWOF6-ROBOT)
(BELOWOF5-ROBOT)
(BELOWOF4-ROBOT)
(BELOWOF3-ROBOT)
(BELOWOF2-ROBOT)
(ABOVEOF8-ROBOT)
(ABOVEOF7-ROBOT)
(ABOVEOF6-ROBOT)
(ABOVEOF5-ROBOT)
(ABOVEOF4-ROBOT)
(ABOVEOF3-ROBOT)
(ABOVEOF2-ROBOT)
(ABOVEOF1-ROBOT)
(CHECKCONSISTENCY-)
(ROW0-ROBOT)
(BELOWOF1-ROBOT)
(Flag152-)
(Flag151-)
(Flag148-)
(Flag147-)
(Flag146-)
(Flag144-)
(Flag143-)
(Flag142-)
(Flag141-)
(Flag139-)
(Flag138-)
(Flag137-)
(Flag136-)
(Flag135-)
(Flag133-)
(Flag132-)
(Flag131-)
(Flag130-)
(Flag129-)
(Flag127-)
(Flag126-)
(Flag125-)
(Flag124-)
(Flag123-)
(Flag122-)
(Flag121-)
(Flag119-)
(Flag118-)
(Flag117-)
(Flag116-)
(Flag115-)
(Flag114-)
(Flag113-)
(Flag112-)
(Flag110-)
(Flag109-)
(Flag108-)
(Flag107-)
(Flag106-)
(Flag105-)
(Flag104-)
(Flag103-)
(Flag102-)
(Flag101-)
(Flag100-)
(Flag99-)
(Flag98-)
(Flag97-)
(Flag96-)
(Flag95-)
(Flag94-)
(Flag92-)
(Flag91-)
(Flag90-)
(Flag89-)
(Flag88-)
(Flag87-)
(Flag86-)
(Flag85-)
(Flag84-)
(Flag83-)
(Flag82-)
(Flag81-)
(Flag80-)
(Flag4-)
(ERROR-)
(COLUMN8-ROBOT)
(COLUMN7-ROBOT)
(COLUMN6-ROBOT)
(COLUMN5-ROBOT)
(COLUMN4-ROBOT)
(COLUMN3-ROBOT)
(COLUMN2-ROBOT)
(COLUMN1-ROBOT)
(LEFTOF9-ROBOT)
(NOT-LEFTOF8-ROBOT)
(LEFTOF7-ROBOT)
(LEFTOF6-ROBOT)
(LEFTOF5-ROBOT)
(LEFTOF4-ROBOT)
(LEFTOF3-ROBOT)
(LEFTOF2-ROBOT)
(RIGHTOF8-ROBOT)
(RIGHTOF7-ROBOT)
(RIGHTOF6-ROBOT)
(RIGHTOF5-ROBOT)
(RIGHTOF4-ROBOT)
(RIGHTOF3-ROBOT)
(RIGHTOF1-ROBOT)
(COLUMN0-ROBOT)
(NOT-RIGHTOF2-ROBOT)
(RIGHTOF0-ROBOT)
(LEFTOF1-ROBOT)
(Flag153-)
(Flag149-)
(Flag145-)
(Flag140-)
(Flag134-)
(Flag128-)
(Flag120-)
(Flag111-)
(Flag78-)
(Flag75-)
(Flag74-)
(Flag73-)
(Flag71-)
(Flag70-)
(Flag69-)
(Flag68-)
(Flag66-)
(Flag65-)
(Flag64-)
(Flag63-)
(Flag61-)
(Flag60-)
(Flag59-)
(Flag58-)
(Flag57-)
(Flag56-)
(Flag54-)
(Flag53-)
(Flag52-)
(Flag51-)
(Flag50-)
(Flag49-)
(Flag48-)
(Flag46-)
(Flag45-)
(Flag44-)
(Flag43-)
(Flag42-)
(Flag41-)
(Flag40-)
(Flag38-)
(Flag37-)
(Flag36-)
(Flag35-)
(Flag34-)
(Flag33-)
(Flag32-)
(Flag31-)
(Flag30-)
(Flag28-)
(Flag27-)
(Flag26-)
(Flag25-)
(Flag24-)
(Flag23-)
(Flag22-)
(Flag21-)
(Flag20-)
(Flag18-)
(Flag17-)
(Flag16-)
(Flag15-)
(Flag14-)
(Flag13-)
(Flag12-)
(Flag11-)
(Flag10-)
(Flag9-)
(Flag8-)
(Flag1-)
(Flag79-)
(Flag76-)
(Flag72-)
(Flag67-)
(Flag62-)
(Flag55-)
(Flag47-)
(Flag39-)
(Flag29-)
(NOT-COLUMN7-ROBOT)
(NOT-COLUMN6-ROBOT)
(NOT-COLUMN5-ROBOT)
(NOT-COLUMN4-ROBOT)
(NOT-COLUMN3-ROBOT)
(NOT-COLUMN2-ROBOT)
(NOT-COLUMN1-ROBOT)
(NOT-COLUMN0-ROBOT)
(NOT-RIGHTOF7-ROBOT)
(NOT-RIGHTOF6-ROBOT)
(NOT-RIGHTOF5-ROBOT)
(NOT-RIGHTOF4-ROBOT)
(NOT-RIGHTOF3-ROBOT)
(NOT-RIGHTOF1-ROBOT)
(RIGHTOF2-ROBOT)
(NOT-RIGHTOF0-ROBOT)
(NOT-LEFTOF7-ROBOT)
(NOT-LEFTOF6-ROBOT)
(NOT-LEFTOF5-ROBOT)
(NOT-LEFTOF4-ROBOT)
(NOT-LEFTOF3-ROBOT)
(NOT-LEFTOF2-ROBOT)
(NOT-LEFTOF1-ROBOT)
(NOT-CHECKCONSISTENCY-)
(NOT-COLUMN8-ROBOT)
(NOT-LEFTOF9-ROBOT)
(LEFTOF8-ROBOT)
(NOT-RIGHTOF8-ROBOT)
(NOT-ERROR-)
(NOT-ROW7-ROBOT)
(NOT-ROW6-ROBOT)
(NOT-ROW5-ROBOT)
(NOT-ROW4-ROBOT)
(NOT-ROW3-ROBOT)
(NOT-ROW2-ROBOT)
(NOT-ROW1-ROBOT)
(NOT-ROW0-ROBOT)
(NOT-ABOVEOF7-ROBOT)
(NOT-ABOVEOF6-ROBOT)
(NOT-ABOVEOF5-ROBOT)
(NOT-ABOVEOF4-ROBOT)
(NOT-ABOVEOF3-ROBOT)
(NOT-ABOVEOF2-ROBOT)
(NOT-ABOVEOF1-ROBOT)
(NOT-BELOWOF7-ROBOT)
(NOT-BELOWOF6-ROBOT)
(NOT-BELOWOF5-ROBOT)
(NOT-BELOWOF4-ROBOT)
(NOT-BELOWOF3-ROBOT)
(NOT-BELOWOF2-ROBOT)
(NOT-BELOWOF1-ROBOT)
(NOT-ROW8-ROBOT)
(NOT-BELOWOF9-ROBOT)
(BELOWOF8-ROBOT)
(NOT-ABOVEOF8-ROBOT)
)
(:derived (Flag29-)(and (Flag20-)))

(:derived (Flag29-)(and (Flag21-)))

(:derived (Flag29-)(and (Flag22-)))

(:derived (Flag29-)(and (Flag23-)))

(:derived (Flag29-)(and (Flag24-)))

(:derived (Flag29-)(and (Flag25-)))

(:derived (Flag29-)(and (Flag26-)))

(:derived (Flag29-)(and (Flag27-)))

(:derived (Flag29-)(and (Flag28-)))

(:derived (Flag39-)(and (Flag20-)))

(:derived (Flag39-)(and (Flag30-)))

(:derived (Flag39-)(and (Flag31-)))

(:derived (Flag39-)(and (Flag32-)))

(:derived (Flag39-)(and (Flag33-)))

(:derived (Flag39-)(and (Flag22-)))

(:derived (Flag39-)(and (Flag23-)))

(:derived (Flag39-)(and (Flag21-)))

(:derived (Flag39-)(and (Flag34-)))

(:derived (Flag39-)(and (Flag35-)))

(:derived (Flag39-)(and (Flag25-)))

(:derived (Flag39-)(and (Flag26-)))

(:derived (Flag39-)(and (Flag36-)))

(:derived (Flag39-)(and (Flag27-)))

(:derived (Flag39-)(and (Flag28-)))

(:derived (Flag39-)(and (Flag37-)))

(:derived (Flag39-)(and (Flag38-)))

(:derived (Flag47-)(and (Flag40-)))

(:derived (Flag47-)(and (Flag23-)))

(:derived (Flag47-)(and (Flag41-)))

(:derived (Flag47-)(and (Flag32-)))

(:derived (Flag47-)(and (Flag21-)))

(:derived (Flag47-)(and (Flag34-)))

(:derived (Flag47-)(and (Flag42-)))

(:derived (Flag47-)(and (Flag36-)))

(:derived (Flag47-)(and (Flag25-)))

(:derived (Flag47-)(and (Flag20-)))

(:derived (Flag47-)(and (Flag43-)))

(:derived (Flag47-)(and (Flag33-)))

(:derived (Flag47-)(and (Flag44-)))

(:derived (Flag47-)(and (Flag27-)))

(:derived (Flag47-)(and (Flag45-)))

(:derived (Flag47-)(and (Flag30-)))

(:derived (Flag47-)(and (Flag35-)))

(:derived (Flag47-)(and (Flag26-)))

(:derived (Flag47-)(and (Flag28-)))

(:derived (Flag47-)(and (Flag37-)))

(:derived (Flag47-)(and (Flag38-)))

(:derived (Flag47-)(and (Flag46-)))

(:derived (Flag55-)(and (Flag40-)))

(:derived (Flag55-)(and (Flag23-)))

(:derived (Flag55-)(and (Flag41-)))

(:derived (Flag55-)(and (Flag48-)))

(:derived (Flag55-)(and (Flag49-)))

(:derived (Flag55-)(and (Flag32-)))

(:derived (Flag55-)(and (Flag21-)))

(:derived (Flag55-)(and (Flag34-)))

(:derived (Flag55-)(and (Flag42-)))

(:derived (Flag55-)(and (Flag36-)))

(:derived (Flag55-)(and (Flag25-)))

(:derived (Flag55-)(and (Flag20-)))

(:derived (Flag55-)(and (Flag43-)))

(:derived (Flag55-)(and (Flag33-)))

(:derived (Flag55-)(and (Flag50-)))

(:derived (Flag55-)(and (Flag44-)))

(:derived (Flag55-)(and (Flag51-)))

(:derived (Flag55-)(and (Flag27-)))

(:derived (Flag55-)(and (Flag52-)))

(:derived (Flag55-)(and (Flag53-)))

(:derived (Flag55-)(and (Flag30-)))

(:derived (Flag55-)(and (Flag28-)))

(:derived (Flag55-)(and (Flag54-)))

(:derived (Flag55-)(and (Flag37-)))

(:derived (Flag55-)(and (Flag38-)))

(:derived (Flag62-)(and (Flag40-)))

(:derived (Flag62-)(and (Flag56-)))

(:derived (Flag62-)(and (Flag41-)))

(:derived (Flag62-)(and (Flag48-)))

(:derived (Flag62-)(and (Flag49-)))

(:derived (Flag62-)(and (Flag32-)))

(:derived (Flag62-)(and (Flag33-)))

(:derived (Flag62-)(and (Flag34-)))

(:derived (Flag62-)(and (Flag42-)))

(:derived (Flag62-)(and (Flag25-)))

(:derived (Flag62-)(and (Flag57-)))

(:derived (Flag62-)(and (Flag58-)))

(:derived (Flag62-)(and (Flag59-)))

(:derived (Flag62-)(and (Flag20-)))

(:derived (Flag62-)(and (Flag43-)))

(:derived (Flag62-)(and (Flag21-)))

(:derived (Flag62-)(and (Flag51-)))

(:derived (Flag62-)(and (Flag60-)))

(:derived (Flag62-)(and (Flag44-)))

(:derived (Flag62-)(and (Flag27-)))

(:derived (Flag62-)(and (Flag52-)))

(:derived (Flag62-)(and (Flag53-)))

(:derived (Flag62-)(and (Flag30-)))

(:derived (Flag62-)(and (Flag28-)))

(:derived (Flag62-)(and (Flag61-)))

(:derived (Flag62-)(and (Flag37-)))

(:derived (Flag62-)(and (Flag38-)))

(:derived (Flag67-)(and (Flag56-)))

(:derived (Flag67-)(and (Flag49-)))

(:derived (Flag67-)(and (Flag32-)))

(:derived (Flag67-)(and (Flag33-)))

(:derived (Flag67-)(and (Flag34-)))

(:derived (Flag67-)(and (Flag63-)))

(:derived (Flag67-)(and (Flag42-)))

(:derived (Flag67-)(and (Flag58-)))

(:derived (Flag67-)(and (Flag59-)))

(:derived (Flag67-)(and (Flag40-)))

(:derived (Flag67-)(and (Flag43-)))

(:derived (Flag67-)(and (Flag21-)))

(:derived (Flag67-)(and (Flag51-)))

(:derived (Flag67-)(and (Flag60-)))

(:derived (Flag67-)(and (Flag44-)))

(:derived (Flag67-)(and (Flag64-)))

(:derived (Flag67-)(and (Flag27-)))

(:derived (Flag67-)(and (Flag65-)))

(:derived (Flag67-)(and (Flag66-)))

(:derived (Flag67-)(and (Flag52-)))

(:derived (Flag67-)(and (Flag53-)))

(:derived (Flag67-)(and (Flag30-)))

(:derived (Flag67-)(and (Flag28-)))

(:derived (Flag67-)(and (Flag61-)))

(:derived (Flag67-)(and (Flag37-)))

(:derived (Flag72-)(and (Flag56-)))

(:derived (Flag72-)(and (Flag68-)))

(:derived (Flag72-)(and (Flag69-)))

(:derived (Flag72-)(and (Flag65-)))

(:derived (Flag72-)(and (Flag33-)))

(:derived (Flag72-)(and (Flag42-)))

(:derived (Flag72-)(and (Flag63-)))

(:derived (Flag72-)(and (Flag58-)))

(:derived (Flag72-)(and (Flag59-)))

(:derived (Flag72-)(and (Flag40-)))

(:derived (Flag72-)(and (Flag43-)))

(:derived (Flag72-)(and (Flag21-)))

(:derived (Flag72-)(and (Flag51-)))

(:derived (Flag72-)(and (Flag60-)))

(:derived (Flag72-)(and (Flag27-)))

(:derived (Flag72-)(and (Flag49-)))

(:derived (Flag72-)(and (Flag70-)))

(:derived (Flag72-)(and (Flag71-)))

(:derived (Flag72-)(and (Flag52-)))

(:derived (Flag72-)(and (Flag30-)))

(:derived (Flag72-)(and (Flag28-)))

(:derived (Flag72-)(and (Flag37-)))

(:derived (Flag76-)(and (Flag73-)))

(:derived (Flag76-)(and (Flag74-)))

(:derived (Flag76-)(and (Flag65-)))

(:derived (Flag76-)(and (Flag75-)))

(:derived (Flag76-)(and (Flag56-)))

(:derived (Flag76-)(and (Flag43-)))

(:derived (Flag76-)(and (Flag51-)))

(:derived (Flag76-)(and (Flag60-)))

(:derived (Flag76-)(and (Flag63-)))

(:derived (Flag76-)(and (Flag30-)))

(:derived (Flag76-)(and (Flag69-)))

(:derived (Flag76-)(and (Flag42-)))

(:derived (Flag76-)(and (Flag27-)))

(:derived (Flag76-)(and (Flag33-)))

(:derived (Flag76-)(and (Flag58-)))

(:derived (Flag76-)(and (Flag49-)))

(:derived (Flag76-)(and (Flag21-)))

(:derived (Flag79-)(and (Flag73-)))

(:derived (Flag79-)(and (Flag43-)))

(:derived (Flag79-)(and (Flag60-)))

(:derived (Flag79-)(and (Flag30-)))

(:derived (Flag79-)(and (Flag69-)))

(:derived (Flag79-)(and (Flag63-)))

(:derived (Flag79-)(and (Flag27-)))

(:derived (Flag79-)(and (Flag58-)))

(:derived (Flag79-)(and (Flag78-)))

(:derived (Flag1-)(and (COLUMN2-ROBOT)(ROW1-ROBOT)(NOT-ERROR-)(NOT-CHECKCONSISTENCY-)))

(:derived (Flag3-)(and (RIGHTOF6-ROBOT)))

(:derived (Flag3-)(and (LEFTOF4-ROBOT)))

(:derived (Flag3-)(and (LEFTOF3-ROBOT)))

(:derived (Flag3-)(and (RIGHTOF7-ROBOT)))

(:derived (Flag3-)(and (LEFTOF5-ROBOT)))

(:derived (Flag3-)(and (RIGHTOF1-ROBOT)))

(:derived (Flag3-)(and (RIGHTOF4-ROBOT)))

(:derived (Flag3-)(and (RIGHTOF0-ROBOT)))

(:derived (Flag3-)(and (LEFTOF7-ROBOT)))

(:derived (Flag3-)(and (LEFTOF2-ROBOT)))

(:derived (Flag3-)(and (RIGHTOF8-ROBOT)))

(:derived (Flag3-)(and (LEFTOF9-ROBOT)))

(:derived (Flag3-)(and (RIGHTOF5-ROBOT)))

(:derived (Flag3-)(and (LEFTOF1-ROBOT)))

(:derived (Flag3-)(and (LEFTOF6-ROBOT)))

(:derived (Flag3-)(and (RIGHTOF3-ROBOT)))

(:derived (Flag5-)(and (RIGHTOF3-ROBOT)))

(:derived (Flag5-)(and (RIGHTOF8-ROBOT)))

(:derived (Flag5-)(and (RIGHTOF5-ROBOT)))

(:derived (Flag5-)(and (RIGHTOF0-ROBOT)))

(:derived (Flag5-)(and (RIGHTOF4-ROBOT)))

(:derived (Flag5-)(and (RIGHTOF1-ROBOT)))

(:derived (Flag5-)(and (RIGHTOF7-ROBOT)))

(:derived (Flag5-)(and (RIGHTOF6-ROBOT)))

(:derived (Flag6-)(and (RIGHTOF3-ROBOT)))

(:derived (Flag6-)(and (RIGHTOF5-ROBOT)))

(:derived (Flag6-)(and (RIGHTOF8-ROBOT)))

(:derived (Flag6-)(and (RIGHTOF4-ROBOT)))

(:derived (Flag6-)(and (RIGHTOF1-ROBOT)))

(:derived (Flag6-)(and (RIGHTOF7-ROBOT)))

(:derived (Flag6-)(and (RIGHTOF6-ROBOT)))

(:derived (Flag7-)(and (RIGHTOF3-ROBOT)))

(:derived (Flag7-)(and (RIGHTOF5-ROBOT)))

(:derived (Flag7-)(and (RIGHTOF4-ROBOT)))

(:derived (Flag7-)(and (RIGHTOF8-ROBOT)))

(:derived (Flag7-)(and (RIGHTOF7-ROBOT)))

(:derived (Flag7-)(and (RIGHTOF6-ROBOT)))

(:derived (Flag8-)(and (RIGHTOF3-ROBOT)))

(:derived (Flag8-)(and (RIGHTOF5-ROBOT)))

(:derived (Flag8-)(and (RIGHTOF4-ROBOT)))

(:derived (Flag8-)(and (RIGHTOF8-ROBOT)))

(:derived (Flag8-)(and (RIGHTOF7-ROBOT)))

(:derived (Flag8-)(and (RIGHTOF6-ROBOT)))

(:derived (Flag9-)(and (RIGHTOF8-ROBOT)))

(:derived (Flag9-)(and (RIGHTOF7-ROBOT)))

(:derived (Flag9-)(and (RIGHTOF4-ROBOT)))

(:derived (Flag9-)(and (RIGHTOF5-ROBOT)))

(:derived (Flag9-)(and (RIGHTOF6-ROBOT)))

(:derived (Flag10-)(and (RIGHTOF8-ROBOT)))

(:derived (Flag10-)(and (RIGHTOF7-ROBOT)))

(:derived (Flag10-)(and (RIGHTOF5-ROBOT)))

(:derived (Flag10-)(and (RIGHTOF6-ROBOT)))

(:derived (Flag11-)(and (RIGHTOF7-ROBOT)))

(:derived (Flag11-)(and (RIGHTOF8-ROBOT)))

(:derived (Flag11-)(and (RIGHTOF6-ROBOT)))

(:derived (Flag12-)(and (RIGHTOF7-ROBOT)))

(:derived (Flag12-)(and (RIGHTOF8-ROBOT)))

(:derived (Flag13-)(and (LEFTOF1-ROBOT)))

(:derived (Flag13-)(and (LEFTOF2-ROBOT)))

(:derived (Flag14-)(and (LEFTOF1-ROBOT)))

(:derived (Flag14-)(and (LEFTOF3-ROBOT)))

(:derived (Flag14-)(and (LEFTOF2-ROBOT)))

(:derived (Flag15-)(and (LEFTOF1-ROBOT)))

(:derived (Flag15-)(and (LEFTOF3-ROBOT)))

(:derived (Flag15-)(and (LEFTOF4-ROBOT)))

(:derived (Flag15-)(and (LEFTOF2-ROBOT)))

(:derived (Flag16-)(and (LEFTOF5-ROBOT)))

(:derived (Flag16-)(and (LEFTOF1-ROBOT)))

(:derived (Flag16-)(and (LEFTOF3-ROBOT)))

(:derived (Flag16-)(and (LEFTOF4-ROBOT)))

(:derived (Flag16-)(and (LEFTOF2-ROBOT)))

(:derived (Flag17-)(and (LEFTOF2-ROBOT)))

(:derived (Flag17-)(and (LEFTOF1-ROBOT)))

(:derived (Flag17-)(and (LEFTOF6-ROBOT)))

(:derived (Flag17-)(and (LEFTOF5-ROBOT)))

(:derived (Flag17-)(and (LEFTOF3-ROBOT)))

(:derived (Flag17-)(and (LEFTOF4-ROBOT)))

(:derived (Flag18-)(and (LEFTOF2-ROBOT)))

(:derived (Flag18-)(and (LEFTOF7-ROBOT)))

(:derived (Flag18-)(and (LEFTOF1-ROBOT)))

(:derived (Flag18-)(and (LEFTOF6-ROBOT)))

(:derived (Flag18-)(and (LEFTOF5-ROBOT)))

(:derived (Flag18-)(and (LEFTOF3-ROBOT)))

(:derived (Flag18-)(and (LEFTOF4-ROBOT)))

(:derived (Flag19-)(and (LEFTOF2-ROBOT)))

(:derived (Flag19-)(and (LEFTOF7-ROBOT)))

(:derived (Flag19-)(and (LEFTOF1-ROBOT)))

(:derived (Flag19-)(and (LEFTOF6-ROBOT)))

(:derived (Flag19-)(and (LEFTOF5-ROBOT)))

(:derived (Flag19-)(and (LEFTOF3-ROBOT)))

(:derived (Flag19-)(and (LEFTOF4-ROBOT)))

(:derived (Flag20-)(and (LEFTOF1-ROBOT)(RIGHTOF4-ROBOT)))

(:derived (Flag21-)(and (LEFTOF1-ROBOT)(RIGHTOF7-ROBOT)))

(:derived (Flag22-)(and (LEFTOF1-ROBOT)(RIGHTOF1-ROBOT)))

(:derived (Flag23-)(and (RIGHTOF3-ROBOT)(LEFTOF1-ROBOT)))

(:derived (Flag24-)(and (RIGHTOF0-ROBOT)(LEFTOF1-ROBOT)))

(:derived (Flag25-)(and (LEFTOF1-ROBOT)(RIGHTOF5-ROBOT)))

(:derived (Flag26-)(and (LEFTOF1-ROBOT)(RIGHTOF2-ROBOT)))

(:derived (Flag27-)(and (RIGHTOF8-ROBOT)(LEFTOF1-ROBOT)))

(:derived (Flag28-)(and (LEFTOF1-ROBOT)(RIGHTOF6-ROBOT)))

(:derived (Flag30-)(and (RIGHTOF8-ROBOT)(LEFTOF2-ROBOT)))

(:derived (Flag31-)(and (LEFTOF2-ROBOT)(RIGHTOF1-ROBOT)))

(:derived (Flag32-)(and (RIGHTOF5-ROBOT)(LEFTOF2-ROBOT)))

(:derived (Flag33-)(and (LEFTOF2-ROBOT)(RIGHTOF7-ROBOT)))

(:derived (Flag34-)(and (RIGHTOF5-ROBOT)(LEFTOF1-ROBOT)))

(:derived (Flag35-)(and (RIGHTOF2-ROBOT)(LEFTOF2-ROBOT)))

(:derived (Flag36-)(and (RIGHTOF3-ROBOT)(LEFTOF2-ROBOT)))

(:derived (Flag37-)(and (LEFTOF2-ROBOT)(RIGHTOF6-ROBOT)))

(:derived (Flag38-)(and (LEFTOF2-ROBOT)(RIGHTOF4-ROBOT)))

(:derived (Flag40-)(and (LEFTOF3-ROBOT)(RIGHTOF6-ROBOT)))

(:derived (Flag41-)(and (LEFTOF3-ROBOT)(RIGHTOF4-ROBOT)))

(:derived (Flag42-)(and (LEFTOF3-ROBOT)(RIGHTOF7-ROBOT)))

(:derived (Flag43-)(and (LEFTOF3-ROBOT)(RIGHTOF8-ROBOT)))

(:derived (Flag44-)(and (LEFTOF3-ROBOT)(RIGHTOF5-ROBOT)))

(:derived (Flag45-)(and (RIGHTOF3-ROBOT)(LEFTOF3-ROBOT)))

(:derived (Flag46-)(and (LEFTOF3-ROBOT)(RIGHTOF2-ROBOT)))

(:derived (Flag48-)(and (LEFTOF4-ROBOT)(RIGHTOF4-ROBOT)))

(:derived (Flag49-)(and (LEFTOF4-ROBOT)(RIGHTOF7-ROBOT)))

(:derived (Flag50-)(and (LEFTOF3-ROBOT)(RIGHTOF3-ROBOT)))

(:derived (Flag51-)(and (RIGHTOF8-ROBOT)(LEFTOF4-ROBOT)))

(:derived (Flag52-)(and (LEFTOF4-ROBOT)(RIGHTOF6-ROBOT)))

(:derived (Flag53-)(and (LEFTOF4-ROBOT)(RIGHTOF5-ROBOT)))

(:derived (Flag54-)(and (RIGHTOF3-ROBOT)(LEFTOF4-ROBOT)))

(:derived (Flag56-)(and (RIGHTOF7-ROBOT)(LEFTOF5-ROBOT)))

(:derived (Flag57-)(and (RIGHTOF4-ROBOT)(LEFTOF5-ROBOT)))

(:derived (Flag58-)(and (LEFTOF4-ROBOT)(RIGHTOF8-ROBOT)))

(:derived (Flag59-)(and (LEFTOF5-ROBOT)(RIGHTOF6-ROBOT)))

(:derived (Flag60-)(and (RIGHTOF8-ROBOT)(LEFTOF5-ROBOT)))

(:derived (Flag61-)(and (RIGHTOF5-ROBOT)(LEFTOF5-ROBOT)))

(:derived (Flag63-)(and (RIGHTOF8-ROBOT)(LEFTOF6-ROBOT)))

(:derived (Flag64-)(and (RIGHTOF5-ROBOT)(LEFTOF6-ROBOT)))

(:derived (Flag65-)(and (RIGHTOF7-ROBOT)(LEFTOF6-ROBOT)))

(:derived (Flag66-)(and (RIGHTOF6-ROBOT)(LEFTOF6-ROBOT)))

(:derived (Flag68-)(and (LEFTOF7-ROBOT)(RIGHTOF7-ROBOT)))

(:derived (Flag69-)(and (RIGHTOF8-ROBOT)(LEFTOF7-ROBOT)))

(:derived (Flag70-)(and (LEFTOF7-ROBOT)(RIGHTOF6-ROBOT)))

(:derived (Flag71-)(and (LEFTOF6-ROBOT)(RIGHTOF6-ROBOT)))

(:derived (Flag73-)(and (RIGHTOF8-ROBOT)(LEFTOF8-ROBOT)))

(:derived (Flag74-)(and (RIGHTOF7-ROBOT)(LEFTOF7-ROBOT)))

(:derived (Flag75-)(and (RIGHTOF7-ROBOT)(LEFTOF8-ROBOT)))

(:derived (Flag77-)(and (LEFTOF9-ROBOT)))

(:derived (Flag77-)(and (LEFTOF2-ROBOT)))

(:derived (Flag77-)(and (LEFTOF7-ROBOT)))

(:derived (Flag77-)(and (LEFTOF1-ROBOT)))

(:derived (Flag77-)(and (LEFTOF6-ROBOT)))

(:derived (Flag77-)(and (LEFTOF5-ROBOT)))

(:derived (Flag77-)(and (LEFTOF3-ROBOT)))

(:derived (Flag77-)(and (LEFTOF4-ROBOT)))

(:derived (Flag78-)(and (RIGHTOF8-ROBOT)(LEFTOF9-ROBOT)))

(:derived (Flag102-)(and (Flag94-)))

(:derived (Flag102-)(and (Flag95-)))

(:derived (Flag102-)(and (Flag96-)))

(:derived (Flag102-)(and (Flag97-)))

(:derived (Flag102-)(and (Flag98-)))

(:derived (Flag102-)(and (Flag99-)))

(:derived (Flag102-)(and (Flag100-)))

(:derived (Flag102-)(and (Flag101-)))

(:derived (Flag111-)(and (Flag95-)))

(:derived (Flag111-)(and (Flag103-)))

(:derived (Flag111-)(and (Flag104-)))

(:derived (Flag111-)(and (Flag94-)))

(:derived (Flag111-)(and (Flag98-)))

(:derived (Flag111-)(and (Flag97-)))

(:derived (Flag111-)(and (Flag96-)))

(:derived (Flag111-)(and (Flag99-)))

(:derived (Flag111-)(and (Flag105-)))

(:derived (Flag111-)(and (Flag106-)))

(:derived (Flag111-)(and (Flag100-)))

(:derived (Flag111-)(and (Flag101-)))

(:derived (Flag111-)(and (Flag107-)))

(:derived (Flag111-)(and (Flag108-)))

(:derived (Flag111-)(and (Flag109-)))

(:derived (Flag111-)(and (Flag110-)))

(:derived (Flag120-)(and (Flag112-)))

(:derived (Flag120-)(and (Flag113-)))

(:derived (Flag120-)(and (Flag114-)))

(:derived (Flag120-)(and (Flag95-)))

(:derived (Flag120-)(and (Flag115-)))

(:derived (Flag120-)(and (Flag116-)))

(:derived (Flag120-)(and (Flag96-)))

(:derived (Flag120-)(and (Flag99-)))

(:derived (Flag120-)(and (Flag117-)))

(:derived (Flag120-)(and (Flag101-)))

(:derived (Flag120-)(and (Flag103-)))

(:derived (Flag120-)(and (Flag106-)))

(:derived (Flag120-)(and (Flag118-)))

(:derived (Flag120-)(and (Flag119-)))

(:derived (Flag120-)(and (Flag100-)))

(:derived (Flag120-)(and (Flag105-)))

(:derived (Flag120-)(and (Flag107-)))

(:derived (Flag120-)(and (Flag97-)))

(:derived (Flag120-)(and (Flag108-)))

(:derived (Flag120-)(and (Flag109-)))

(:derived (Flag120-)(and (Flag94-)))

(:derived (Flag128-)(and (Flag115-)))

(:derived (Flag128-)(and (Flag121-)))

(:derived (Flag128-)(and (Flag122-)))

(:derived (Flag128-)(and (Flag114-)))

(:derived (Flag128-)(and (Flag118-)))

(:derived (Flag128-)(and (Flag107-)))

(:derived (Flag128-)(and (Flag97-)))

(:derived (Flag128-)(and (Flag99-)))

(:derived (Flag128-)(and (Flag123-)))

(:derived (Flag128-)(and (Flag124-)))

(:derived (Flag128-)(and (Flag125-)))

(:derived (Flag128-)(and (Flag105-)))

(:derived (Flag128-)(and (Flag126-)))

(:derived (Flag128-)(and (Flag127-)))

(:derived (Flag128-)(and (Flag103-)))

(:derived (Flag128-)(and (Flag116-)))

(:derived (Flag128-)(and (Flag96-)))

(:derived (Flag128-)(and (Flag117-)))

(:derived (Flag128-)(and (Flag101-)))

(:derived (Flag128-)(and (Flag109-)))

(:derived (Flag128-)(and (Flag94-)))

(:derived (Flag128-)(and (Flag106-)))

(:derived (Flag128-)(and (Flag100-)))

(:derived (Flag128-)(and (Flag108-)))

(:derived (Flag134-)(and (Flag129-)))

(:derived (Flag134-)(and (Flag115-)))

(:derived (Flag134-)(and (Flag114-)))

(:derived (Flag134-)(and (Flag118-)))

(:derived (Flag134-)(and (Flag107-)))

(:derived (Flag134-)(and (Flag130-)))

(:derived (Flag134-)(and (Flag97-)))

(:derived (Flag134-)(and (Flag106-)))

(:derived (Flag134-)(and (Flag99-)))

(:derived (Flag134-)(and (Flag123-)))

(:derived (Flag134-)(and (Flag124-)))

(:derived (Flag134-)(and (Flag125-)))

(:derived (Flag134-)(and (Flag105-)))

(:derived (Flag134-)(and (Flag126-)))

(:derived (Flag134-)(and (Flag103-)))

(:derived (Flag134-)(and (Flag116-)))

(:derived (Flag134-)(and (Flag96-)))

(:derived (Flag134-)(and (Flag131-)))

(:derived (Flag134-)(and (Flag117-)))

(:derived (Flag134-)(and (Flag132-)))

(:derived (Flag134-)(and (Flag122-)))

(:derived (Flag134-)(and (Flag109-)))

(:derived (Flag134-)(and (Flag94-)))

(:derived (Flag134-)(and (Flag133-)))

(:derived (Flag134-)(and (Flag100-)))

(:derived (Flag140-)(and (Flag115-)))

(:derived (Flag140-)(and (Flag114-)))

(:derived (Flag140-)(and (Flag118-)))

(:derived (Flag140-)(and (Flag107-)))

(:derived (Flag140-)(and (Flag135-)))

(:derived (Flag140-)(and (Flag130-)))

(:derived (Flag140-)(and (Flag97-)))

(:derived (Flag140-)(and (Flag106-)))

(:derived (Flag140-)(and (Flag99-)))

(:derived (Flag140-)(and (Flag136-)))

(:derived (Flag140-)(and (Flag137-)))

(:derived (Flag140-)(and (Flag124-)))

(:derived (Flag140-)(and (Flag125-)))

(:derived (Flag140-)(and (Flag126-)))

(:derived (Flag140-)(and (Flag103-)))

(:derived (Flag140-)(and (Flag96-)))

(:derived (Flag140-)(and (Flag116-)))

(:derived (Flag140-)(and (Flag138-)))

(:derived (Flag140-)(and (Flag132-)))

(:derived (Flag140-)(and (Flag139-)))

(:derived (Flag140-)(and (Flag122-)))

(:derived (Flag140-)(and (Flag109-)))

(:derived (Flag140-)(and (Flag133-)))

(:derived (Flag140-)(and (Flag100-)))

(:derived (Flag145-)(and (Flag107-)))

(:derived (Flag145-)(and (Flag141-)))

(:derived (Flag145-)(and (Flag114-)))

(:derived (Flag145-)(and (Flag130-)))

(:derived (Flag145-)(and (Flag142-)))

(:derived (Flag145-)(and (Flag100-)))

(:derived (Flag145-)(and (Flag97-)))

(:derived (Flag145-)(and (Flag96-)))

(:derived (Flag145-)(and (Flag143-)))

(:derived (Flag145-)(and (Flag136-)))

(:derived (Flag145-)(and (Flag132-)))

(:derived (Flag145-)(and (Flag103-)))

(:derived (Flag145-)(and (Flag133-)))

(:derived (Flag145-)(and (Flag124-)))

(:derived (Flag145-)(and (Flag118-)))

(:derived (Flag145-)(and (Flag125-)))

(:derived (Flag145-)(and (Flag139-)))

(:derived (Flag145-)(and (Flag122-)))

(:derived (Flag145-)(and (Flag116-)))

(:derived (Flag145-)(and (Flag109-)))

(:derived (Flag145-)(and (Flag144-)))

(:derived (Flag149-)(and (Flag107-)))

(:derived (Flag149-)(and (Flag146-)))

(:derived (Flag149-)(and (Flag130-)))

(:derived (Flag149-)(and (Flag100-)))

(:derived (Flag149-)(and (Flag116-)))

(:derived (Flag149-)(and (Flag143-)))

(:derived (Flag149-)(and (Flag136-)))

(:derived (Flag149-)(and (Flag147-)))

(:derived (Flag149-)(and (Flag148-)))

(:derived (Flag149-)(and (Flag124-)))

(:derived (Flag149-)(and (Flag118-)))

(:derived (Flag149-)(and (Flag125-)))

(:derived (Flag149-)(and (Flag139-)))

(:derived (Flag149-)(and (Flag97-)))

(:derived (Flag149-)(and (Flag109-)))

(:derived (Flag149-)(and (Flag132-)))

(:derived (Flag153-)(and (Flag151-)))

(:derived (Flag153-)(and (Flag130-)))

(:derived (Flag153-)(and (Flag152-)))

(:derived (Flag153-)(and (Flag100-)))

(:derived (Flag153-)(and (Flag116-)))

(:derived (Flag153-)(and (Flag143-)))

(:derived (Flag153-)(and (Flag124-)))

(:derived (Flag153-)(and (Flag139-)))

(:derived (Flag153-)(and (Flag109-)))

(:action MOVELEFT-ROBOT
:parameters ()
:precondition
(and
(Flag4-)
)
:effect
(and
(when
(and
(Flag79-)
)
(and
(COLUMN7-ROBOT)
(not (NOT-COLUMN7-ROBOT))
)
)
(when
(and
(Flag76-)
)
(and
(COLUMN6-ROBOT)
(not (NOT-COLUMN6-ROBOT))
)
)
(when
(and
(Flag72-)
)
(and
(COLUMN5-ROBOT)
(not (NOT-COLUMN5-ROBOT))
)
)
(when
(and
(Flag67-)
)
(and
(COLUMN4-ROBOT)
(not (NOT-COLUMN4-ROBOT))
)
)
(when
(and
(Flag62-)
)
(and
(COLUMN3-ROBOT)
(not (NOT-COLUMN3-ROBOT))
)
)
(when
(and
(Flag55-)
)
(and
(COLUMN2-ROBOT)
(not (NOT-COLUMN2-ROBOT))
)
)
(when
(and
(Flag47-)
)
(and
(COLUMN1-ROBOT)
(not (NOT-COLUMN1-ROBOT))
)
)
(when
(and
(Flag39-)
)
(and
(COLUMN0-ROBOT)
(not (NOT-COLUMN0-ROBOT))
)
)
(when
(and
(COLUMN8-ROBOT)
)
(and
(COLUMN7-ROBOT)
(NOT-COLUMN8-ROBOT)
(not (NOT-COLUMN7-ROBOT))
(not (COLUMN8-ROBOT))
)
)
(when
(and
(COLUMN7-ROBOT)
)
(and
(COLUMN6-ROBOT)
(NOT-COLUMN7-ROBOT)
(not (NOT-COLUMN6-ROBOT))
(not (COLUMN7-ROBOT))
)
)
(when
(and
(COLUMN6-ROBOT)
)
(and
(COLUMN5-ROBOT)
(NOT-COLUMN6-ROBOT)
(not (NOT-COLUMN5-ROBOT))
(not (COLUMN6-ROBOT))
)
)
(when
(and
(COLUMN5-ROBOT)
)
(and
(COLUMN4-ROBOT)
(NOT-COLUMN5-ROBOT)
(not (NOT-COLUMN4-ROBOT))
(not (COLUMN5-ROBOT))
)
)
(when
(and
(COLUMN4-ROBOT)
)
(and
(COLUMN3-ROBOT)
(NOT-COLUMN4-ROBOT)
(not (NOT-COLUMN3-ROBOT))
(not (COLUMN4-ROBOT))
)
)
(when
(and
(COLUMN3-ROBOT)
)
(and
(COLUMN2-ROBOT)
(NOT-COLUMN3-ROBOT)
(not (NOT-COLUMN2-ROBOT))
(not (COLUMN3-ROBOT))
)
)
(when
(and
(COLUMN2-ROBOT)
)
(and
(COLUMN1-ROBOT)
(NOT-COLUMN2-ROBOT)
(not (NOT-COLUMN1-ROBOT))
(not (COLUMN2-ROBOT))
)
)
(when
(and
(COLUMN1-ROBOT)
)
(and
(COLUMN0-ROBOT)
(NOT-COLUMN1-ROBOT)
(not (NOT-COLUMN0-ROBOT))
(not (COLUMN1-ROBOT))
)
)
(when
(and
(RIGHTOF8-ROBOT)
)
(and
(RIGHTOF7-ROBOT)
(NOT-RIGHTOF8-ROBOT)
(RIGHTOF6-ROBOT)
(NOT-RIGHTOF7-ROBOT)
(RIGHTOF5-ROBOT)
(NOT-RIGHTOF6-ROBOT)
(RIGHTOF4-ROBOT)
(NOT-RIGHTOF5-ROBOT)
(RIGHTOF3-ROBOT)
(NOT-RIGHTOF4-ROBOT)
(RIGHTOF2-ROBOT)
(NOT-RIGHTOF3-ROBOT)
(RIGHTOF1-ROBOT)
(NOT-RIGHTOF2-ROBOT)
(RIGHTOF0-ROBOT)
(NOT-RIGHTOF1-ROBOT)
(not (RIGHTOF8-ROBOT))
(not (NOT-RIGHTOF0-ROBOT))
)
)
(when
(and
(RIGHTOF7-ROBOT)
)
(and
(RIGHTOF6-ROBOT)
(NOT-RIGHTOF7-ROBOT)
(RIGHTOF5-ROBOT)
(NOT-RIGHTOF6-ROBOT)
(RIGHTOF4-ROBOT)
(NOT-RIGHTOF5-ROBOT)
(RIGHTOF3-ROBOT)
(NOT-RIGHTOF4-ROBOT)
(RIGHTOF2-ROBOT)
(NOT-RIGHTOF3-ROBOT)
(RIGHTOF1-ROBOT)
(NOT-RIGHTOF2-ROBOT)
(RIGHTOF0-ROBOT)
(NOT-RIGHTOF1-ROBOT)
(not (RIGHTOF7-ROBOT))
(not (NOT-RIGHTOF0-ROBOT))
)
)
(when
(and
(RIGHTOF6-ROBOT)
)
(and
(RIGHTOF5-ROBOT)
(NOT-RIGHTOF6-ROBOT)
(RIGHTOF4-ROBOT)
(NOT-RIGHTOF5-ROBOT)
(RIGHTOF3-ROBOT)
(NOT-RIGHTOF4-ROBOT)
(RIGHTOF2-ROBOT)
(NOT-RIGHTOF3-ROBOT)
(RIGHTOF1-ROBOT)
(NOT-RIGHTOF2-ROBOT)
(RIGHTOF0-ROBOT)
(NOT-RIGHTOF1-ROBOT)
(not (RIGHTOF6-ROBOT))
(not (NOT-RIGHTOF0-ROBOT))
)
)
(when
(and
(RIGHTOF5-ROBOT)
)
(and
(RIGHTOF4-ROBOT)
(NOT-RIGHTOF5-ROBOT)
(RIGHTOF3-ROBOT)
(NOT-RIGHTOF4-ROBOT)
(RIGHTOF2-ROBOT)
(NOT-RIGHTOF3-ROBOT)
(RIGHTOF1-ROBOT)
(NOT-RIGHTOF2-ROBOT)
(RIGHTOF0-ROBOT)
(NOT-RIGHTOF1-ROBOT)
(not (RIGHTOF5-ROBOT))
(not (NOT-RIGHTOF0-ROBOT))
)
)
(when
(and
(RIGHTOF4-ROBOT)
)
(and
(RIGHTOF3-ROBOT)
(NOT-RIGHTOF4-ROBOT)
(RIGHTOF2-ROBOT)
(NOT-RIGHTOF3-ROBOT)
(RIGHTOF1-ROBOT)
(NOT-RIGHTOF2-ROBOT)
(RIGHTOF0-ROBOT)
(NOT-RIGHTOF1-ROBOT)
(not (RIGHTOF4-ROBOT))
(not (NOT-RIGHTOF0-ROBOT))
)
)
(when
(and
(RIGHTOF3-ROBOT)
)
(and
(RIGHTOF2-ROBOT)
(NOT-RIGHTOF3-ROBOT)
(RIGHTOF1-ROBOT)
(NOT-RIGHTOF2-ROBOT)
(RIGHTOF0-ROBOT)
(NOT-RIGHTOF1-ROBOT)
(not (RIGHTOF3-ROBOT))
(not (NOT-RIGHTOF0-ROBOT))
)
)
(when
(and
(RIGHTOF2-ROBOT)
)
(and
(RIGHTOF1-ROBOT)
(NOT-RIGHTOF2-ROBOT)
(RIGHTOF0-ROBOT)
(NOT-RIGHTOF1-ROBOT)
(not (RIGHTOF2-ROBOT))
(not (NOT-RIGHTOF0-ROBOT))
)
)
(when
(and
(RIGHTOF1-ROBOT)
)
(and
(RIGHTOF0-ROBOT)
(NOT-RIGHTOF1-ROBOT)
(not (NOT-RIGHTOF0-ROBOT))
(not (RIGHTOF1-ROBOT))
)
)
(when
(and
(Flag77-)
)
(and
(LEFTOF8-ROBOT)
(not (NOT-LEFTOF8-ROBOT))
)
)
(when
(and
(LEFTOF8-ROBOT)
)
(and
(LEFTOF7-ROBOT)
(not (NOT-LEFTOF7-ROBOT))
)
)
(when
(and
(LEFTOF4-ROBOT)
)
(and
(LEFTOF7-ROBOT)
(LEFTOF6-ROBOT)
(LEFTOF5-ROBOT)
(LEFTOF3-ROBOT)
(not (NOT-LEFTOF7-ROBOT))
(not (NOT-LEFTOF6-ROBOT))
(not (NOT-LEFTOF5-ROBOT))
(not (NOT-LEFTOF4-ROBOT))
(not (NOT-LEFTOF3-ROBOT))
)
)
(when
(and
(LEFTOF3-ROBOT)
)
(and
(LEFTOF7-ROBOT)
(LEFTOF6-ROBOT)
(LEFTOF5-ROBOT)
(LEFTOF4-ROBOT)
(LEFTOF2-ROBOT)
(not (NOT-LEFTOF7-ROBOT))
(not (NOT-LEFTOF6-ROBOT))
(not (NOT-LEFTOF5-ROBOT))
(not (NOT-LEFTOF4-ROBOT))
(not (NOT-LEFTOF3-ROBOT))
(not (NOT-LEFTOF2-ROBOT))
)
)
(when
(and
(LEFTOF5-ROBOT)
)
(and
(LEFTOF7-ROBOT)
(LEFTOF6-ROBOT)
(LEFTOF4-ROBOT)
(not (NOT-LEFTOF7-ROBOT))
(not (NOT-LEFTOF6-ROBOT))
(not (NOT-LEFTOF5-ROBOT))
(not (NOT-LEFTOF4-ROBOT))
)
)
(when
(and
(LEFTOF6-ROBOT)
)
(and
(LEFTOF7-ROBOT)
(LEFTOF5-ROBOT)
(not (NOT-LEFTOF7-ROBOT))
(not (NOT-LEFTOF6-ROBOT))
(not (NOT-LEFTOF5-ROBOT))
)
)
(when
(and
(LEFTOF1-ROBOT)
)
(and
(LEFTOF7-ROBOT)
(LEFTOF6-ROBOT)
(LEFTOF5-ROBOT)
(LEFTOF4-ROBOT)
(LEFTOF3-ROBOT)
(LEFTOF2-ROBOT)
(not (NOT-LEFTOF7-ROBOT))
(not (NOT-LEFTOF6-ROBOT))
(not (NOT-LEFTOF5-ROBOT))
(not (NOT-LEFTOF4-ROBOT))
(not (NOT-LEFTOF3-ROBOT))
(not (NOT-LEFTOF2-ROBOT))
(not (NOT-LEFTOF1-ROBOT))
)
)
(when
(and
(LEFTOF7-ROBOT)
)
(and
(LEFTOF6-ROBOT)
(not (NOT-LEFTOF7-ROBOT))
(not (NOT-LEFTOF6-ROBOT))
)
)
(when
(and
(LEFTOF2-ROBOT)
)
(and
(LEFTOF7-ROBOT)
(LEFTOF6-ROBOT)
(LEFTOF5-ROBOT)
(LEFTOF4-ROBOT)
(LEFTOF3-ROBOT)
(LEFTOF1-ROBOT)
(not (NOT-LEFTOF7-ROBOT))
(not (NOT-LEFTOF6-ROBOT))
(not (NOT-LEFTOF5-ROBOT))
(not (NOT-LEFTOF4-ROBOT))
(not (NOT-LEFTOF3-ROBOT))
(not (NOT-LEFTOF2-ROBOT))
(not (NOT-LEFTOF1-ROBOT))
)
)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action MOVERIGHT-ROBOT
:parameters ()
:precondition
(and
(Flag4-)
)
:effect
(and
(when
(and
(Flag76-)
)
(and
(COLUMN8-ROBOT)
(not (NOT-COLUMN8-ROBOT))
)
)
(when
(and
(Flag72-)
)
(and
(COLUMN7-ROBOT)
(not (NOT-COLUMN7-ROBOT))
)
)
(when
(and
(Flag67-)
)
(and
(COLUMN6-ROBOT)
(not (NOT-COLUMN6-ROBOT))
)
)
(when
(and
(Flag62-)
)
(and
(COLUMN5-ROBOT)
(not (NOT-COLUMN5-ROBOT))
)
)
(when
(and
(Flag55-)
)
(and
(COLUMN4-ROBOT)
(not (NOT-COLUMN4-ROBOT))
)
)
(when
(and
(Flag47-)
)
(and
(COLUMN3-ROBOT)
(not (NOT-COLUMN3-ROBOT))
)
)
(when
(and
(Flag39-)
)
(and
(COLUMN2-ROBOT)
(not (NOT-COLUMN2-ROBOT))
)
)
(when
(and
(Flag29-)
)
(and
(COLUMN1-ROBOT)
(not (NOT-COLUMN1-ROBOT))
)
)
(when
(and
(COLUMN7-ROBOT)
)
(and
(COLUMN8-ROBOT)
(NOT-COLUMN7-ROBOT)
(not (NOT-COLUMN8-ROBOT))
(not (COLUMN7-ROBOT))
)
)
(when
(and
(COLUMN6-ROBOT)
)
(and
(COLUMN7-ROBOT)
(NOT-COLUMN6-ROBOT)
(not (NOT-COLUMN7-ROBOT))
(not (COLUMN6-ROBOT))
)
)
(when
(and
(COLUMN5-ROBOT)
)
(and
(COLUMN6-ROBOT)
(NOT-COLUMN5-ROBOT)
(not (NOT-COLUMN6-ROBOT))
(not (COLUMN5-ROBOT))
)
)
(when
(and
(COLUMN4-ROBOT)
)
(and
(COLUMN5-ROBOT)
(NOT-COLUMN4-ROBOT)
(not (NOT-COLUMN5-ROBOT))
(not (COLUMN4-ROBOT))
)
)
(when
(and
(COLUMN3-ROBOT)
)
(and
(COLUMN4-ROBOT)
(NOT-COLUMN3-ROBOT)
(not (NOT-COLUMN4-ROBOT))
(not (COLUMN3-ROBOT))
)
)
(when
(and
(COLUMN2-ROBOT)
)
(and
(COLUMN3-ROBOT)
(NOT-COLUMN2-ROBOT)
(not (NOT-COLUMN3-ROBOT))
(not (COLUMN2-ROBOT))
)
)
(when
(and
(COLUMN1-ROBOT)
)
(and
(COLUMN2-ROBOT)
(NOT-COLUMN1-ROBOT)
(not (NOT-COLUMN2-ROBOT))
(not (COLUMN1-ROBOT))
)
)
(when
(and
(COLUMN0-ROBOT)
)
(and
(COLUMN1-ROBOT)
(NOT-COLUMN0-ROBOT)
(not (NOT-COLUMN1-ROBOT))
(not (COLUMN0-ROBOT))
)
)
(when
(and
(Flag19-)
)
(and
(LEFTOF9-ROBOT)
(NOT-LEFTOF8-ROBOT)
(not (NOT-LEFTOF9-ROBOT))
(not (LEFTOF8-ROBOT))
)
)
(when
(and
(Flag18-)
)
(and
(LEFTOF8-ROBOT)
(NOT-LEFTOF7-ROBOT)
(not (NOT-LEFTOF8-ROBOT))
(not (LEFTOF7-ROBOT))
)
)
(when
(and
(Flag17-)
)
(and
(LEFTOF7-ROBOT)
(NOT-LEFTOF6-ROBOT)
(not (NOT-LEFTOF7-ROBOT))
(not (LEFTOF6-ROBOT))
)
)
(when
(and
(Flag16-)
)
(and
(LEFTOF6-ROBOT)
(NOT-LEFTOF5-ROBOT)
(not (NOT-LEFTOF6-ROBOT))
(not (LEFTOF5-ROBOT))
)
)
(when
(and
(Flag15-)
)
(and
(LEFTOF5-ROBOT)
(NOT-LEFTOF4-ROBOT)
(not (NOT-LEFTOF5-ROBOT))
(not (LEFTOF4-ROBOT))
)
)
(when
(and
(Flag14-)
)
(and
(LEFTOF4-ROBOT)
(NOT-LEFTOF3-ROBOT)
(not (NOT-LEFTOF4-ROBOT))
(not (LEFTOF3-ROBOT))
)
)
(when
(and
(Flag13-)
)
(and
(LEFTOF3-ROBOT)
(NOT-LEFTOF2-ROBOT)
(not (NOT-LEFTOF3-ROBOT))
(not (LEFTOF2-ROBOT))
)
)
(when
(and
(LEFTOF1-ROBOT)
)
(and
(LEFTOF2-ROBOT)
(NOT-LEFTOF1-ROBOT)
(not (NOT-LEFTOF2-ROBOT))
(not (LEFTOF1-ROBOT))
)
)
(when
(and
(Flag12-)
)
(and
(RIGHTOF8-ROBOT)
(not (NOT-RIGHTOF8-ROBOT))
)
)
(when
(and
(Flag11-)
)
(and
(RIGHTOF7-ROBOT)
(not (NOT-RIGHTOF7-ROBOT))
)
)
(when
(and
(Flag10-)
)
(and
(RIGHTOF6-ROBOT)
(not (NOT-RIGHTOF6-ROBOT))
)
)
(when
(and
(Flag9-)
)
(and
(RIGHTOF5-ROBOT)
(not (NOT-RIGHTOF5-ROBOT))
)
)
(when
(and
(Flag8-)
)
(and
(RIGHTOF4-ROBOT)
(not (NOT-RIGHTOF4-ROBOT))
)
)
(when
(and
(Flag7-)
)
(and
(RIGHTOF3-ROBOT)
(not (NOT-RIGHTOF3-ROBOT))
)
)
(when
(and
(Flag6-)
)
(and
(RIGHTOF2-ROBOT)
(not (NOT-RIGHTOF2-ROBOT))
)
)
(when
(and
(Flag5-)
)
(and
(RIGHTOF1-ROBOT)
(not (NOT-RIGHTOF1-ROBOT))
)
)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action CHECKCONSISTENCYACTION
:parameters ()
:precondition
(and
(NOT-ERROR-)
(CHECKCONSISTENCY-)
)
:effect
(and
(when
(and
(LEFTOF4-ROBOT)
(RIGHTOF6-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF7-ROBOT)
(BELOWOF1-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF4-ROBOT)
(RIGHTOF7-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF2-ROBOT)
(RIGHTOF7-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF6-ROBOT)
(LEFTOF2-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF4-ROBOT)
(LEFTOF3-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF1-ROBOT)
(ABOVEOF4-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF7-ROBOT)
(LEFTOF1-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF8-ROBOT)
(LEFTOF6-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF3-ROBOT)
(ABOVEOF8-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF8-ROBOT)
(BELOWOF6-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF8-ROBOT)
(LEFTOF5-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF5-ROBOT)
(BELOWOF3-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF1-ROBOT)
(RIGHTOF5-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF7-ROBOT)
(BELOWOF3-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF4-ROBOT)
(BELOWOF2-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF5-ROBOT)
(RIGHTOF6-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF8-ROBOT)
(BELOWOF5-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF8-ROBOT)
(BELOWOF2-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF1-ROBOT)
(ABOVEOF5-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF7-ROBOT)
(BELOWOF4-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF5-ROBOT)
(ABOVEOF6-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF2-ROBOT)
(RIGHTOF8-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF3-ROBOT)
(LEFTOF1-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF5-ROBOT)
(LEFTOF2-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF7-ROBOT)
(LEFTOF3-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF7-ROBOT)
(LEFTOF6-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF8-ROBOT)
(LEFTOF3-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF3-ROBOT)
(RIGHTOF6-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF4-ROBOT)
(RIGHTOF5-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF3-ROBOT)
(ABOVEOF4-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF4-ROBOT)
(RIGHTOF8-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF3-ROBOT)
(BELOWOF1-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF1-ROBOT)
(RIGHTOF8-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF6-ROBOT)
(LEFTOF1-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF5-ROBOT)
(BELOWOF2-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF8-ROBOT)
(BELOWOF1-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF2-ROBOT)
(ABOVEOF2-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF3-ROBOT)
(BELOWOF2-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF5-ROBOT)
(RIGHTOF7-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF4-ROBOT)
(LEFTOF2-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF7-ROBOT)
(LEFTOF7-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF7-ROBOT)
(RIGHTOF8-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF3-ROBOT)
(LEFTOF2-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF2-ROBOT)
(ABOVEOF7-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF6-ROBOT)
(BELOWOF2-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF6-ROBOT)
(ABOVEOF7-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF8-ROBOT)
(ABOVEOF8-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF1-ROBOT)
(ABOVEOF2-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF4-ROBOT)
(ABOVEOF8-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF8-ROBOT)
(LEFTOF8-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF5-ROBOT)
(LEFTOF3-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF1-ROBOT)
(RIGHTOF2-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF6-ROBOT)
(BELOWOF3-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF5-ROBOT)
(RIGHTOF5-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF5-ROBOT)
(BELOWOF4-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF3-ROBOT)
(LEFTOF3-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF5-ROBOT)
(ABOVEOF7-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF6-ROBOT)
(BELOWOF1-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF4-ROBOT)
(LEFTOF1-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF8-ROBOT)
(BELOWOF7-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF7-ROBOT)
(ABOVEOF7-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF3-ROBOT)
(BELOWOF3-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF1-ROBOT)
(ABOVEOF1-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF6-ROBOT)
(BELOWOF4-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF5-ROBOT)
(BELOWOF5-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF6-ROBOT)
(LEFTOF6-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF6-ROBOT)
(ABOVEOF6-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF2-ROBOT)
(RIGHTOF2-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF4-ROBOT)
(RIGHTOF4-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF1-ROBOT)
(LEFTOF1-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF4-ROBOT)
(ABOVEOF4-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(NOT-CHECKCONSISTENCY-)
(not (CHECKCONSISTENCY-))
)
)
(:derived (Flag4-)(and (NOT-CHECKCONSISTENCY-)(NOT-ERROR-)(Flag3-)))

(:derived (Flag80-)(and (ABOVEOF6-ROBOT)))

(:derived (Flag80-)(and (ABOVEOF1-ROBOT)))

(:derived (Flag80-)(and (ABOVEOF2-ROBOT)))

(:derived (Flag80-)(and (ABOVEOF7-ROBOT)))

(:derived (Flag80-)(and (ABOVEOF8-ROBOT)))

(:derived (Flag80-)(and (ABOVEOF4-ROBOT)))

(:derived (Flag80-)(and (ABOVEOF3-ROBOT)))

(:derived (Flag80-)(and (ABOVEOF5-ROBOT)))

(:derived (Flag81-)(and (ABOVEOF6-ROBOT)))

(:derived (Flag81-)(and (ABOVEOF2-ROBOT)))

(:derived (Flag81-)(and (ABOVEOF7-ROBOT)))

(:derived (Flag81-)(and (ABOVEOF8-ROBOT)))

(:derived (Flag81-)(and (ABOVEOF4-ROBOT)))

(:derived (Flag81-)(and (ABOVEOF3-ROBOT)))

(:derived (Flag81-)(and (ABOVEOF5-ROBOT)))

(:derived (Flag82-)(and (ABOVEOF6-ROBOT)))

(:derived (Flag82-)(and (ABOVEOF8-ROBOT)))

(:derived (Flag82-)(and (ABOVEOF7-ROBOT)))

(:derived (Flag82-)(and (ABOVEOF4-ROBOT)))

(:derived (Flag82-)(and (ABOVEOF3-ROBOT)))

(:derived (Flag82-)(and (ABOVEOF5-ROBOT)))

(:derived (Flag83-)(and (ABOVEOF4-ROBOT)))

(:derived (Flag83-)(and (ABOVEOF8-ROBOT)))

(:derived (Flag83-)(and (ABOVEOF7-ROBOT)))

(:derived (Flag83-)(and (ABOVEOF5-ROBOT)))

(:derived (Flag83-)(and (ABOVEOF6-ROBOT)))

(:derived (Flag84-)(and (ABOVEOF8-ROBOT)))

(:derived (Flag84-)(and (ABOVEOF7-ROBOT)))

(:derived (Flag84-)(and (ABOVEOF5-ROBOT)))

(:derived (Flag84-)(and (ABOVEOF6-ROBOT)))

(:derived (Flag85-)(and (ABOVEOF8-ROBOT)))

(:derived (Flag85-)(and (ABOVEOF7-ROBOT)))

(:derived (Flag85-)(and (ABOVEOF6-ROBOT)))

(:derived (Flag86-)(and (ABOVEOF8-ROBOT)))

(:derived (Flag86-)(and (ABOVEOF7-ROBOT)))

(:derived (Flag87-)(and (BELOWOF2-ROBOT)))

(:derived (Flag87-)(and (BELOWOF1-ROBOT)))

(:derived (Flag88-)(and (BELOWOF2-ROBOT)))

(:derived (Flag88-)(and (BELOWOF1-ROBOT)))

(:derived (Flag88-)(and (BELOWOF3-ROBOT)))

(:derived (Flag89-)(and (BELOWOF2-ROBOT)))

(:derived (Flag89-)(and (BELOWOF1-ROBOT)))

(:derived (Flag89-)(and (BELOWOF3-ROBOT)))

(:derived (Flag89-)(and (BELOWOF4-ROBOT)))

(:derived (Flag90-)(and (BELOWOF2-ROBOT)))

(:derived (Flag90-)(and (BELOWOF1-ROBOT)))

(:derived (Flag90-)(and (BELOWOF3-ROBOT)))

(:derived (Flag90-)(and (BELOWOF4-ROBOT)))

(:derived (Flag90-)(and (BELOWOF5-ROBOT)))

(:derived (Flag91-)(and (BELOWOF6-ROBOT)))

(:derived (Flag91-)(and (BELOWOF5-ROBOT)))

(:derived (Flag91-)(and (BELOWOF2-ROBOT)))

(:derived (Flag91-)(and (BELOWOF1-ROBOT)))

(:derived (Flag91-)(and (BELOWOF4-ROBOT)))

(:derived (Flag91-)(and (BELOWOF3-ROBOT)))

(:derived (Flag92-)(and (BELOWOF6-ROBOT)))

(:derived (Flag92-)(and (BELOWOF5-ROBOT)))

(:derived (Flag92-)(and (BELOWOF2-ROBOT)))

(:derived (Flag92-)(and (BELOWOF1-ROBOT)))

(:derived (Flag92-)(and (BELOWOF4-ROBOT)))

(:derived (Flag92-)(and (BELOWOF7-ROBOT)))

(:derived (Flag92-)(and (BELOWOF3-ROBOT)))

(:derived (Flag93-)(and (BELOWOF6-ROBOT)))

(:derived (Flag93-)(and (BELOWOF5-ROBOT)))

(:derived (Flag93-)(and (BELOWOF2-ROBOT)))

(:derived (Flag93-)(and (BELOWOF1-ROBOT)))

(:derived (Flag93-)(and (BELOWOF4-ROBOT)))

(:derived (Flag93-)(and (BELOWOF7-ROBOT)))

(:derived (Flag93-)(and (BELOWOF3-ROBOT)))

(:derived (Flag94-)(and (BELOWOF1-ROBOT)(ABOVEOF4-ROBOT)))

(:derived (Flag95-)(and (BELOWOF1-ROBOT)(ABOVEOF2-ROBOT)))

(:derived (Flag96-)(and (ABOVEOF6-ROBOT)(BELOWOF1-ROBOT)))

(:derived (Flag97-)(and (BELOWOF1-ROBOT)(ABOVEOF7-ROBOT)))

(:derived (Flag98-)(and (BELOWOF1-ROBOT)(ABOVEOF1-ROBOT)))

(:derived (Flag99-)(and (BELOWOF1-ROBOT)(ABOVEOF5-ROBOT)))

(:derived (Flag100-)(and (ABOVEOF8-ROBOT)(BELOWOF1-ROBOT)))

(:derived (Flag101-)(and (ABOVEOF3-ROBOT)(BELOWOF1-ROBOT)))

(:derived (Flag102-)(and (BELOWOF1-ROBOT)))

(:derived (Flag103-)(and (ABOVEOF6-ROBOT)(BELOWOF2-ROBOT)))

(:derived (Flag104-)(and (ABOVEOF2-ROBOT)(BELOWOF2-ROBOT)))

(:derived (Flag105-)(and (ABOVEOF4-ROBOT)(BELOWOF2-ROBOT)))

(:derived (Flag106-)(and (BELOWOF2-ROBOT)(ABOVEOF5-ROBOT)))

(:derived (Flag107-)(and (ABOVEOF7-ROBOT)(BELOWOF2-ROBOT)))

(:derived (Flag108-)(and (ABOVEOF3-ROBOT)(BELOWOF2-ROBOT)))

(:derived (Flag109-)(and (ABOVEOF8-ROBOT)(BELOWOF2-ROBOT)))

(:derived (Flag110-)(and (ABOVEOF1-ROBOT)(BELOWOF2-ROBOT)))

(:derived (Flag112-)(and (ABOVEOF3-ROBOT)(BELOWOF3-ROBOT)))

(:derived (Flag113-)(and (BELOWOF3-ROBOT)(ABOVEOF2-ROBOT)))

(:derived (Flag114-)(and (BELOWOF3-ROBOT)(ABOVEOF6-ROBOT)))

(:derived (Flag115-)(and (BELOWOF3-ROBOT)(ABOVEOF5-ROBOT)))

(:derived (Flag116-)(and (ABOVEOF8-ROBOT)(BELOWOF3-ROBOT)))

(:derived (Flag117-)(and (BELOWOF3-ROBOT)(ABOVEOF4-ROBOT)))

(:derived (Flag118-)(and (BELOWOF3-ROBOT)(ABOVEOF7-ROBOT)))

(:derived (Flag119-)(and (BELOWOF2-ROBOT)(ABOVEOF2-ROBOT)))

(:derived (Flag121-)(and (ABOVEOF3-ROBOT)(BELOWOF4-ROBOT)))

(:derived (Flag122-)(and (ABOVEOF6-ROBOT)(BELOWOF4-ROBOT)))

(:derived (Flag123-)(and (BELOWOF4-ROBOT)(ABOVEOF4-ROBOT)))

(:derived (Flag124-)(and (ABOVEOF8-ROBOT)(BELOWOF4-ROBOT)))

(:derived (Flag125-)(and (BELOWOF4-ROBOT)(ABOVEOF7-ROBOT)))

(:derived (Flag126-)(and (BELOWOF4-ROBOT)(ABOVEOF5-ROBOT)))

(:derived (Flag127-)(and (BELOWOF3-ROBOT)(ABOVEOF3-ROBOT)))

(:derived (Flag129-)(and (ABOVEOF4-ROBOT)(BELOWOF5-ROBOT)))

(:derived (Flag130-)(and (ABOVEOF8-ROBOT)(BELOWOF5-ROBOT)))

(:derived (Flag131-)(and (ABOVEOF5-ROBOT)(BELOWOF5-ROBOT)))

(:derived (Flag132-)(and (ABOVEOF7-ROBOT)(BELOWOF5-ROBOT)))

(:derived (Flag133-)(and (ABOVEOF6-ROBOT)(BELOWOF5-ROBOT)))

(:derived (Flag135-)(and (BELOWOF6-ROBOT)(ABOVEOF5-ROBOT)))

(:derived (Flag136-)(and (BELOWOF6-ROBOT)(ABOVEOF7-ROBOT)))

(:derived (Flag137-)(and (BELOWOF5-ROBOT)(ABOVEOF5-ROBOT)))

(:derived (Flag138-)(and (BELOWOF6-ROBOT)(ABOVEOF6-ROBOT)))

(:derived (Flag139-)(and (ABOVEOF8-ROBOT)(BELOWOF6-ROBOT)))

(:derived (Flag141-)(and (ABOVEOF6-ROBOT)(BELOWOF7-ROBOT)))

(:derived (Flag142-)(and (ABOVEOF7-ROBOT)(BELOWOF7-ROBOT)))

(:derived (Flag143-)(and (ABOVEOF8-ROBOT)(BELOWOF7-ROBOT)))

(:derived (Flag144-)(and (ABOVEOF6-ROBOT)(BELOWOF6-ROBOT)))

(:derived (Flag146-)(and (ABOVEOF8-ROBOT)(BELOWOF8-ROBOT)))

(:derived (Flag147-)(and (BELOWOF8-ROBOT)(ABOVEOF7-ROBOT)))

(:derived (Flag148-)(and (BELOWOF7-ROBOT)(ABOVEOF7-ROBOT)))

(:derived (Flag150-)(and (BELOWOF6-ROBOT)))

(:derived (Flag150-)(and (BELOWOF5-ROBOT)))

(:derived (Flag150-)(and (BELOWOF2-ROBOT)))

(:derived (Flag150-)(and (BELOWOF1-ROBOT)))

(:derived (Flag150-)(and (BELOWOF4-ROBOT)))

(:derived (Flag150-)(and (BELOWOF7-ROBOT)))

(:derived (Flag150-)(and (BELOWOF9-ROBOT)))

(:derived (Flag150-)(and (BELOWOF3-ROBOT)))

(:derived (Flag151-)(and (BELOWOF8-ROBOT)(ABOVEOF8-ROBOT)))

(:derived (Flag152-)(and (ABOVEOF8-ROBOT)(BELOWOF9-ROBOT)))

(:action MOVEDOWN-ROBOT
:parameters ()
:precondition
(and
(Flag2-)
)
:effect
(and
(when
(and
(Flag153-)
)
(and
(ROW7-ROBOT)
(not (NOT-ROW7-ROBOT))
)
)
(when
(and
(Flag149-)
)
(and
(ROW6-ROBOT)
(not (NOT-ROW6-ROBOT))
)
)
(when
(and
(Flag145-)
)
(and
(ROW5-ROBOT)
(not (NOT-ROW5-ROBOT))
)
)
(when
(and
(Flag140-)
)
(and
(ROW4-ROBOT)
(not (NOT-ROW4-ROBOT))
)
)
(when
(and
(Flag134-)
)
(and
(ROW3-ROBOT)
(not (NOT-ROW3-ROBOT))
)
)
(when
(and
(Flag128-)
)
(and
(ROW2-ROBOT)
(not (NOT-ROW2-ROBOT))
)
)
(when
(and
(Flag120-)
)
(and
(ROW1-ROBOT)
(not (NOT-ROW1-ROBOT))
)
)
(when
(and
(Flag111-)
)
(and
(ROW0-ROBOT)
(not (NOT-ROW0-ROBOT))
)
)
(when
(and
(ROW8-ROBOT)
)
(and
(ROW7-ROBOT)
(NOT-ROW8-ROBOT)
(not (NOT-ROW7-ROBOT))
(not (ROW8-ROBOT))
)
)
(when
(and
(ROW7-ROBOT)
)
(and
(ROW6-ROBOT)
(NOT-ROW7-ROBOT)
(not (NOT-ROW6-ROBOT))
(not (ROW7-ROBOT))
)
)
(when
(and
(ROW6-ROBOT)
)
(and
(ROW5-ROBOT)
(NOT-ROW6-ROBOT)
(not (NOT-ROW5-ROBOT))
(not (ROW6-ROBOT))
)
)
(when
(and
(ROW5-ROBOT)
)
(and
(ROW4-ROBOT)
(NOT-ROW5-ROBOT)
(not (NOT-ROW4-ROBOT))
(not (ROW5-ROBOT))
)
)
(when
(and
(ROW4-ROBOT)
)
(and
(ROW3-ROBOT)
(NOT-ROW4-ROBOT)
(not (NOT-ROW3-ROBOT))
(not (ROW4-ROBOT))
)
)
(when
(and
(ROW3-ROBOT)
)
(and
(ROW2-ROBOT)
(NOT-ROW3-ROBOT)
(not (NOT-ROW2-ROBOT))
(not (ROW3-ROBOT))
)
)
(when
(and
(ROW2-ROBOT)
)
(and
(ROW1-ROBOT)
(NOT-ROW2-ROBOT)
(not (NOT-ROW1-ROBOT))
(not (ROW2-ROBOT))
)
)
(when
(and
(ROW1-ROBOT)
)
(and
(ROW0-ROBOT)
(NOT-ROW1-ROBOT)
(not (NOT-ROW0-ROBOT))
(not (ROW1-ROBOT))
)
)
(when
(and
(ABOVEOF8-ROBOT)
)
(and
(ABOVEOF7-ROBOT)
(NOT-ABOVEOF8-ROBOT)
(ABOVEOF6-ROBOT)
(NOT-ABOVEOF7-ROBOT)
(ABOVEOF5-ROBOT)
(NOT-ABOVEOF6-ROBOT)
(ABOVEOF4-ROBOT)
(NOT-ABOVEOF5-ROBOT)
(ABOVEOF3-ROBOT)
(NOT-ABOVEOF4-ROBOT)
(ABOVEOF2-ROBOT)
(NOT-ABOVEOF3-ROBOT)
(ABOVEOF1-ROBOT)
(NOT-ABOVEOF2-ROBOT)
(NOT-ABOVEOF1-ROBOT)
(not (ABOVEOF8-ROBOT))
)
)
(when
(and
(ABOVEOF7-ROBOT)
)
(and
(ABOVEOF6-ROBOT)
(NOT-ABOVEOF7-ROBOT)
(ABOVEOF5-ROBOT)
(NOT-ABOVEOF6-ROBOT)
(ABOVEOF4-ROBOT)
(NOT-ABOVEOF5-ROBOT)
(ABOVEOF3-ROBOT)
(NOT-ABOVEOF4-ROBOT)
(ABOVEOF2-ROBOT)
(NOT-ABOVEOF3-ROBOT)
(ABOVEOF1-ROBOT)
(NOT-ABOVEOF2-ROBOT)
(NOT-ABOVEOF1-ROBOT)
(not (ABOVEOF7-ROBOT))
)
)
(when
(and
(ABOVEOF6-ROBOT)
)
(and
(ABOVEOF5-ROBOT)
(NOT-ABOVEOF6-ROBOT)
(ABOVEOF4-ROBOT)
(NOT-ABOVEOF5-ROBOT)
(ABOVEOF3-ROBOT)
(NOT-ABOVEOF4-ROBOT)
(ABOVEOF2-ROBOT)
(NOT-ABOVEOF3-ROBOT)
(ABOVEOF1-ROBOT)
(NOT-ABOVEOF2-ROBOT)
(NOT-ABOVEOF1-ROBOT)
(not (ABOVEOF6-ROBOT))
)
)
(when
(and
(ABOVEOF5-ROBOT)
)
(and
(ABOVEOF4-ROBOT)
(NOT-ABOVEOF5-ROBOT)
(ABOVEOF3-ROBOT)
(NOT-ABOVEOF4-ROBOT)
(ABOVEOF2-ROBOT)
(NOT-ABOVEOF3-ROBOT)
(ABOVEOF1-ROBOT)
(NOT-ABOVEOF2-ROBOT)
(NOT-ABOVEOF1-ROBOT)
(not (ABOVEOF5-ROBOT))
)
)
(when
(and
(ABOVEOF4-ROBOT)
)
(and
(ABOVEOF3-ROBOT)
(NOT-ABOVEOF4-ROBOT)
(ABOVEOF2-ROBOT)
(NOT-ABOVEOF3-ROBOT)
(ABOVEOF1-ROBOT)
(NOT-ABOVEOF2-ROBOT)
(NOT-ABOVEOF1-ROBOT)
(not (ABOVEOF4-ROBOT))
)
)
(when
(and
(ABOVEOF3-ROBOT)
)
(and
(ABOVEOF2-ROBOT)
(NOT-ABOVEOF3-ROBOT)
(ABOVEOF1-ROBOT)
(NOT-ABOVEOF2-ROBOT)
(NOT-ABOVEOF1-ROBOT)
(not (ABOVEOF3-ROBOT))
)
)
(when
(and
(ABOVEOF2-ROBOT)
)
(and
(ABOVEOF1-ROBOT)
(NOT-ABOVEOF2-ROBOT)
(NOT-ABOVEOF1-ROBOT)
(not (ABOVEOF2-ROBOT))
)
)
(when
(and
(ABOVEOF1-ROBOT)
)
(and
(NOT-ABOVEOF1-ROBOT)
(not (ABOVEOF1-ROBOT))
)
)
(when
(and
(Flag150-)
)
(and
(BELOWOF8-ROBOT)
(not (NOT-BELOWOF8-ROBOT))
)
)
(when
(and
(BELOWOF8-ROBOT)
)
(and
(BELOWOF7-ROBOT)
(not (NOT-BELOWOF7-ROBOT))
)
)
(when
(and
(BELOWOF3-ROBOT)
)
(and
(BELOWOF7-ROBOT)
(BELOWOF6-ROBOT)
(BELOWOF5-ROBOT)
(BELOWOF4-ROBOT)
(BELOWOF2-ROBOT)
(not (NOT-BELOWOF7-ROBOT))
(not (NOT-BELOWOF6-ROBOT))
(not (NOT-BELOWOF5-ROBOT))
(not (NOT-BELOWOF4-ROBOT))
(not (NOT-BELOWOF3-ROBOT))
(not (NOT-BELOWOF2-ROBOT))
)
)
(when
(and
(BELOWOF7-ROBOT)
)
(and
(BELOWOF6-ROBOT)
(not (NOT-BELOWOF7-ROBOT))
(not (NOT-BELOWOF6-ROBOT))
)
)
(when
(and
(BELOWOF4-ROBOT)
)
(and
(BELOWOF7-ROBOT)
(BELOWOF6-ROBOT)
(BELOWOF5-ROBOT)
(BELOWOF3-ROBOT)
(not (NOT-BELOWOF7-ROBOT))
(not (NOT-BELOWOF6-ROBOT))
(not (NOT-BELOWOF5-ROBOT))
(not (NOT-BELOWOF4-ROBOT))
(not (NOT-BELOWOF3-ROBOT))
)
)
(when
(and
(BELOWOF1-ROBOT)
)
(and
(BELOWOF7-ROBOT)
(BELOWOF6-ROBOT)
(BELOWOF5-ROBOT)
(BELOWOF4-ROBOT)
(BELOWOF3-ROBOT)
(BELOWOF2-ROBOT)
(not (NOT-BELOWOF7-ROBOT))
(not (NOT-BELOWOF6-ROBOT))
(not (NOT-BELOWOF5-ROBOT))
(not (NOT-BELOWOF4-ROBOT))
(not (NOT-BELOWOF3-ROBOT))
(not (NOT-BELOWOF2-ROBOT))
(not (NOT-BELOWOF1-ROBOT))
)
)
(when
(and
(BELOWOF2-ROBOT)
)
(and
(BELOWOF7-ROBOT)
(BELOWOF6-ROBOT)
(BELOWOF5-ROBOT)
(BELOWOF4-ROBOT)
(BELOWOF3-ROBOT)
(BELOWOF1-ROBOT)
(not (NOT-BELOWOF7-ROBOT))
(not (NOT-BELOWOF6-ROBOT))
(not (NOT-BELOWOF5-ROBOT))
(not (NOT-BELOWOF4-ROBOT))
(not (NOT-BELOWOF3-ROBOT))
(not (NOT-BELOWOF2-ROBOT))
(not (NOT-BELOWOF1-ROBOT))
)
)
(when
(and
(BELOWOF5-ROBOT)
)
(and
(BELOWOF7-ROBOT)
(BELOWOF6-ROBOT)
(BELOWOF4-ROBOT)
(not (NOT-BELOWOF7-ROBOT))
(not (NOT-BELOWOF6-ROBOT))
(not (NOT-BELOWOF5-ROBOT))
(not (NOT-BELOWOF4-ROBOT))
)
)
(when
(and
(BELOWOF6-ROBOT)
)
(and
(BELOWOF7-ROBOT)
(BELOWOF5-ROBOT)
(not (NOT-BELOWOF7-ROBOT))
(not (NOT-BELOWOF6-ROBOT))
(not (NOT-BELOWOF5-ROBOT))
)
)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action MOVEUP-ROBOT
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
)
:effect
(and
(when
(and
(Flag149-)
)
(and
(ROW8-ROBOT)
(not (NOT-ROW8-ROBOT))
)
)
(when
(and
(Flag145-)
)
(and
(ROW7-ROBOT)
(not (NOT-ROW7-ROBOT))
)
)
(when
(and
(Flag140-)
)
(and
(ROW6-ROBOT)
(not (NOT-ROW6-ROBOT))
)
)
(when
(and
(Flag134-)
)
(and
(ROW5-ROBOT)
(not (NOT-ROW5-ROBOT))
)
)
(when
(and
(Flag128-)
)
(and
(ROW4-ROBOT)
(not (NOT-ROW4-ROBOT))
)
)
(when
(and
(Flag120-)
)
(and
(ROW3-ROBOT)
(not (NOT-ROW3-ROBOT))
)
)
(when
(and
(Flag111-)
)
(and
(ROW2-ROBOT)
(not (NOT-ROW2-ROBOT))
)
)
(when
(and
(Flag102-)
)
(and
(ROW1-ROBOT)
(not (NOT-ROW1-ROBOT))
)
)
(when
(and
(ROW7-ROBOT)
)
(and
(ROW8-ROBOT)
(NOT-ROW7-ROBOT)
(not (NOT-ROW8-ROBOT))
(not (ROW7-ROBOT))
)
)
(when
(and
(ROW6-ROBOT)
)
(and
(ROW7-ROBOT)
(NOT-ROW6-ROBOT)
(not (NOT-ROW7-ROBOT))
(not (ROW6-ROBOT))
)
)
(when
(and
(ROW5-ROBOT)
)
(and
(ROW6-ROBOT)
(NOT-ROW5-ROBOT)
(not (NOT-ROW6-ROBOT))
(not (ROW5-ROBOT))
)
)
(when
(and
(ROW4-ROBOT)
)
(and
(ROW5-ROBOT)
(NOT-ROW4-ROBOT)
(not (NOT-ROW5-ROBOT))
(not (ROW4-ROBOT))
)
)
(when
(and
(ROW3-ROBOT)
)
(and
(ROW4-ROBOT)
(NOT-ROW3-ROBOT)
(not (NOT-ROW4-ROBOT))
(not (ROW3-ROBOT))
)
)
(when
(and
(ROW2-ROBOT)
)
(and
(ROW3-ROBOT)
(NOT-ROW2-ROBOT)
(not (NOT-ROW3-ROBOT))
(not (ROW2-ROBOT))
)
)
(when
(and
(ROW1-ROBOT)
)
(and
(ROW2-ROBOT)
(NOT-ROW1-ROBOT)
(not (NOT-ROW2-ROBOT))
(not (ROW1-ROBOT))
)
)
(when
(and
(ROW0-ROBOT)
)
(and
(ROW1-ROBOT)
(NOT-ROW0-ROBOT)
(not (NOT-ROW1-ROBOT))
(not (ROW0-ROBOT))
)
)
(when
(and
(Flag93-)
)
(and
(BELOWOF9-ROBOT)
(NOT-BELOWOF8-ROBOT)
(not (NOT-BELOWOF9-ROBOT))
(not (BELOWOF8-ROBOT))
)
)
(when
(and
(Flag92-)
)
(and
(BELOWOF8-ROBOT)
(NOT-BELOWOF7-ROBOT)
(not (NOT-BELOWOF8-ROBOT))
(not (BELOWOF7-ROBOT))
)
)
(when
(and
(Flag91-)
)
(and
(BELOWOF7-ROBOT)
(NOT-BELOWOF6-ROBOT)
(not (NOT-BELOWOF7-ROBOT))
(not (BELOWOF6-ROBOT))
)
)
(when
(and
(Flag90-)
)
(and
(BELOWOF6-ROBOT)
(NOT-BELOWOF5-ROBOT)
(not (NOT-BELOWOF6-ROBOT))
(not (BELOWOF5-ROBOT))
)
)
(when
(and
(Flag89-)
)
(and
(BELOWOF5-ROBOT)
(NOT-BELOWOF4-ROBOT)
(not (NOT-BELOWOF5-ROBOT))
(not (BELOWOF4-ROBOT))
)
)
(when
(and
(Flag88-)
)
(and
(BELOWOF4-ROBOT)
(NOT-BELOWOF3-ROBOT)
(not (NOT-BELOWOF4-ROBOT))
(not (BELOWOF3-ROBOT))
)
)
(when
(and
(Flag87-)
)
(and
(BELOWOF3-ROBOT)
(NOT-BELOWOF2-ROBOT)
(not (NOT-BELOWOF3-ROBOT))
(not (BELOWOF2-ROBOT))
)
)
(when
(and
(BELOWOF1-ROBOT)
)
(and
(BELOWOF2-ROBOT)
(NOT-BELOWOF1-ROBOT)
(not (NOT-BELOWOF2-ROBOT))
(not (BELOWOF1-ROBOT))
)
)
(when
(and
(Flag86-)
)
(and
(ABOVEOF8-ROBOT)
(not (NOT-ABOVEOF8-ROBOT))
)
)
(when
(and
(Flag85-)
)
(and
(ABOVEOF7-ROBOT)
(not (NOT-ABOVEOF7-ROBOT))
)
)
(when
(and
(Flag84-)
)
(and
(ABOVEOF6-ROBOT)
(not (NOT-ABOVEOF6-ROBOT))
)
)
(when
(and
(Flag83-)
)
(and
(ABOVEOF5-ROBOT)
(not (NOT-ABOVEOF5-ROBOT))
)
)
(when
(and
(Flag82-)
)
(and
(ABOVEOF4-ROBOT)
(not (NOT-ABOVEOF4-ROBOT))
)
)
(when
(and
(Flag81-)
)
(and
(ABOVEOF3-ROBOT)
(not (NOT-ABOVEOF3-ROBOT))
)
)
(when
(and
(Flag80-)
)
(and
(ABOVEOF2-ROBOT)
(not (NOT-ABOVEOF2-ROBOT))
)
)
(ABOVEOF1-ROBOT)
(CHECKCONSISTENCY-)
(not (NOT-ABOVEOF1-ROBOT))
(not (NOT-CHECKCONSISTENCY-))
)
)
(:derived (Flag2-)(and (NOT-CHECKCONSISTENCY-)(NOT-ERROR-)))

(:derived (Flag3-)(and (LEFTOF8-ROBOT)))

(:derived (Flag3-)(and (RIGHTOF2-ROBOT)))

(:derived (Flag5-)(and (RIGHTOF2-ROBOT)))

(:derived (Flag6-)(and (RIGHTOF2-ROBOT)))

(:derived (Flag7-)(and (RIGHTOF2-ROBOT)))

(:derived (Flag19-)(and (LEFTOF8-ROBOT)))

(:derived (Flag77-)(and (LEFTOF8-ROBOT)))

(:derived (Flag93-)(and (BELOWOF8-ROBOT)))

(:derived (Flag150-)(and (BELOWOF8-ROBOT)))

)
