(define (domain grounded-SIMPLE-ADL-ROBOT)
(:requirements
:strips
)
(:predicates
(Flag124-)
(Flag78-)
(Flag68-)
(Flag67-)
(Flag66-)
(Flag62-)
(Flag16-)
(Flag4-)
(Flag2-)
(ROW7-ROBOT)
(ROW6-ROBOT)
(ROW5-ROBOT)
(ROW4-ROBOT)
(ROW3-ROBOT)
(ROW2-ROBOT)
(ROW1-ROBOT)
(BELOWOF8-ROBOT)
(NOT-BELOWOF7-ROBOT)
(BELOWOF6-ROBOT)
(BELOWOF5-ROBOT)
(BELOWOF4-ROBOT)
(BELOWOF3-ROBOT)
(BELOWOF2-ROBOT)
(ABOVEOF7-ROBOT)
(ABOVEOF6-ROBOT)
(ABOVEOF5-ROBOT)
(ABOVEOF4-ROBOT)
(ABOVEOF3-ROBOT)
(ABOVEOF2-ROBOT)
(ABOVEOF1-ROBOT)
(CHECKCONSISTENCY-)
(ROW0-ROBOT)
(BELOWOF1-ROBOT)
(Flag64-)
(Flag63-)
(Flag60-)
(Flag59-)
(Flag58-)
(Flag56-)
(Flag55-)
(Flag54-)
(Flag53-)
(Flag51-)
(Flag50-)
(Flag49-)
(Flag48-)
(Flag46-)
(Flag45-)
(Flag44-)
(Flag43-)
(Flag42-)
(Flag41-)
(Flag39-)
(Flag38-)
(Flag37-)
(Flag36-)
(Flag35-)
(Flag34-)
(Flag33-)
(Flag31-)
(Flag30-)
(Flag29-)
(Flag28-)
(Flag27-)
(Flag26-)
(Flag25-)
(Flag24-)
(Flag23-)
(Flag22-)
(Flag21-)
(Flag20-)
(Flag19-)
(Flag18-)
(Flag17-)
(Flag15-)
(Flag14-)
(Flag13-)
(Flag12-)
(Flag11-)
(Flag10-)
(Flag9-)
(Flag8-)
(Flag7-)
(Flag6-)
(Flag5-)
(Flag3-)
(ERROR-)
(COLUMN7-ROBOT)
(COLUMN6-ROBOT)
(COLUMN5-ROBOT)
(COLUMN4-ROBOT)
(COLUMN3-ROBOT)
(COLUMN2-ROBOT)
(COLUMN1-ROBOT)
(LEFTOF8-ROBOT)
(NOT-LEFTOF7-ROBOT)
(LEFTOF6-ROBOT)
(LEFTOF5-ROBOT)
(LEFTOF4-ROBOT)
(LEFTOF3-ROBOT)
(LEFTOF2-ROBOT)
(RIGHTOF7-ROBOT)
(RIGHTOF6-ROBOT)
(RIGHTOF5-ROBOT)
(RIGHTOF4-ROBOT)
(RIGHTOF3-ROBOT)
(RIGHTOF1-ROBOT)
(COLUMN0-ROBOT)
(NOT-RIGHTOF2-ROBOT)
(RIGHTOF0-ROBOT)
(LEFTOF1-ROBOT)
(Flag126-)
(Flag125-)
(Flag122-)
(Flag121-)
(Flag120-)
(Flag118-)
(Flag117-)
(Flag116-)
(Flag114-)
(Flag113-)
(Flag112-)
(Flag111-)
(Flag109-)
(Flag108-)
(Flag107-)
(Flag106-)
(Flag105-)
(Flag104-)
(Flag102-)
(Flag101-)
(Flag100-)
(Flag99-)
(Flag98-)
(Flag97-)
(Flag95-)
(Flag94-)
(Flag93-)
(Flag92-)
(Flag91-)
(Flag90-)
(Flag89-)
(Flag88-)
(Flag86-)
(Flag85-)
(Flag84-)
(Flag83-)
(Flag82-)
(Flag81-)
(Flag80-)
(Flag79-)
(Flag77-)
(Flag76-)
(Flag75-)
(Flag74-)
(Flag73-)
(Flag72-)
(Flag71-)
(Flag70-)
(Flag69-)
(Flag65-)
(Flag61-)
(Flag57-)
(Flag52-)
(Flag47-)
(Flag40-)
(Flag32-)
(Flag1-)
(Flag127-)
(Flag123-)
(Flag119-)
(Flag115-)
(Flag110-)
(Flag103-)
(Flag96-)
(Flag87-)
(NOT-COLUMN6-ROBOT)
(NOT-COLUMN5-ROBOT)
(NOT-COLUMN4-ROBOT)
(NOT-COLUMN3-ROBOT)
(NOT-COLUMN2-ROBOT)
(NOT-COLUMN1-ROBOT)
(NOT-COLUMN0-ROBOT)
(NOT-RIGHTOF6-ROBOT)
(NOT-RIGHTOF5-ROBOT)
(NOT-RIGHTOF4-ROBOT)
(NOT-RIGHTOF3-ROBOT)
(NOT-RIGHTOF1-ROBOT)
(RIGHTOF2-ROBOT)
(NOT-RIGHTOF0-ROBOT)
(NOT-LEFTOF6-ROBOT)
(NOT-LEFTOF5-ROBOT)
(NOT-LEFTOF4-ROBOT)
(NOT-LEFTOF3-ROBOT)
(NOT-LEFTOF2-ROBOT)
(NOT-LEFTOF1-ROBOT)
(NOT-CHECKCONSISTENCY-)
(NOT-COLUMN7-ROBOT)
(NOT-LEFTOF8-ROBOT)
(LEFTOF7-ROBOT)
(NOT-RIGHTOF7-ROBOT)
(NOT-ERROR-)
(NOT-ROW6-ROBOT)
(NOT-ROW5-ROBOT)
(NOT-ROW4-ROBOT)
(NOT-ROW3-ROBOT)
(NOT-ROW2-ROBOT)
(NOT-ROW1-ROBOT)
(NOT-ROW0-ROBOT)
(NOT-ABOVEOF6-ROBOT)
(NOT-ABOVEOF5-ROBOT)
(NOT-ABOVEOF4-ROBOT)
(NOT-ABOVEOF3-ROBOT)
(NOT-ABOVEOF2-ROBOT)
(NOT-ABOVEOF1-ROBOT)
(NOT-BELOWOF6-ROBOT)
(NOT-BELOWOF5-ROBOT)
(NOT-BELOWOF4-ROBOT)
(NOT-BELOWOF3-ROBOT)
(NOT-BELOWOF2-ROBOT)
(NOT-BELOWOF1-ROBOT)
(NOT-ROW7-ROBOT)
(NOT-BELOWOF8-ROBOT)
(BELOWOF7-ROBOT)
(NOT-ABOVEOF7-ROBOT)
)
(:derived (Flag87-)(and (Flag79-)))

(:derived (Flag87-)(and (Flag80-)))

(:derived (Flag87-)(and (Flag81-)))

(:derived (Flag87-)(and (Flag82-)))

(:derived (Flag87-)(and (Flag83-)))

(:derived (Flag87-)(and (Flag84-)))

(:derived (Flag87-)(and (Flag85-)))

(:derived (Flag87-)(and (Flag86-)))

(:derived (Flag96-)(and (Flag79-)))

(:derived (Flag96-)(and (Flag88-)))

(:derived (Flag96-)(and (Flag89-)))

(:derived (Flag96-)(and (Flag90-)))

(:derived (Flag96-)(and (Flag81-)))

(:derived (Flag96-)(and (Flag82-)))

(:derived (Flag96-)(and (Flag80-)))

(:derived (Flag96-)(and (Flag91-)))

(:derived (Flag96-)(and (Flag92-)))

(:derived (Flag96-)(and (Flag84-)))

(:derived (Flag96-)(and (Flag85-)))

(:derived (Flag96-)(and (Flag93-)))

(:derived (Flag96-)(and (Flag86-)))

(:derived (Flag96-)(and (Flag94-)))

(:derived (Flag96-)(and (Flag95-)))

(:derived (Flag103-)(and (Flag79-)))

(:derived (Flag103-)(and (Flag84-)))

(:derived (Flag103-)(and (Flag97-)))

(:derived (Flag103-)(and (Flag89-)))

(:derived (Flag103-)(and (Flag80-)))

(:derived (Flag103-)(and (Flag82-)))

(:derived (Flag103-)(and (Flag98-)))

(:derived (Flag103-)(and (Flag90-)))

(:derived (Flag103-)(and (Flag91-)))

(:derived (Flag103-)(and (Flag92-)))

(:derived (Flag103-)(and (Flag99-)))

(:derived (Flag103-)(and (Flag85-)))

(:derived (Flag103-)(and (Flag100-)))

(:derived (Flag103-)(and (Flag93-)))

(:derived (Flag103-)(and (Flag101-)))

(:derived (Flag103-)(and (Flag86-)))

(:derived (Flag103-)(and (Flag94-)))

(:derived (Flag103-)(and (Flag95-)))

(:derived (Flag103-)(and (Flag102-)))

(:derived (Flag110-)(and (Flag79-)))

(:derived (Flag110-)(and (Flag104-)))

(:derived (Flag110-)(and (Flag105-)))

(:derived (Flag110-)(and (Flag97-)))

(:derived (Flag110-)(and (Flag89-)))

(:derived (Flag110-)(and (Flag82-)))

(:derived (Flag110-)(and (Flag106-)))

(:derived (Flag110-)(and (Flag90-)))

(:derived (Flag110-)(and (Flag107-)))

(:derived (Flag110-)(and (Flag99-)))

(:derived (Flag110-)(and (Flag91-)))

(:derived (Flag110-)(and (Flag84-)))

(:derived (Flag110-)(and (Flag101-)))

(:derived (Flag110-)(and (Flag100-)))

(:derived (Flag110-)(and (Flag93-)))

(:derived (Flag110-)(and (Flag86-)))

(:derived (Flag110-)(and (Flag108-)))

(:derived (Flag110-)(and (Flag94-)))

(:derived (Flag110-)(and (Flag109-)))

(:derived (Flag110-)(and (Flag95-)))

(:derived (Flag110-)(and (Flag80-)))

(:derived (Flag115-)(and (Flag79-)))

(:derived (Flag115-)(and (Flag104-)))

(:derived (Flag115-)(and (Flag111-)))

(:derived (Flag115-)(and (Flag89-)))

(:derived (Flag115-)(and (Flag97-)))

(:derived (Flag115-)(and (Flag106-)))

(:derived (Flag115-)(and (Flag90-)))

(:derived (Flag115-)(and (Flag107-)))

(:derived (Flag115-)(and (Flag99-)))

(:derived (Flag115-)(and (Flag91-)))

(:derived (Flag115-)(and (Flag84-)))

(:derived (Flag115-)(and (Flag101-)))

(:derived (Flag115-)(and (Flag100-)))

(:derived (Flag115-)(and (Flag80-)))

(:derived (Flag115-)(and (Flag86-)))

(:derived (Flag115-)(and (Flag112-)))

(:derived (Flag115-)(and (Flag113-)))

(:derived (Flag115-)(and (Flag94-)))

(:derived (Flag115-)(and (Flag109-)))

(:derived (Flag115-)(and (Flag95-)))

(:derived (Flag115-)(and (Flag114-)))

(:derived (Flag119-)(and (Flag104-)))

(:derived (Flag119-)(and (Flag89-)))

(:derived (Flag119-)(and (Flag97-)))

(:derived (Flag119-)(and (Flag106-)))

(:derived (Flag119-)(and (Flag90-)))

(:derived (Flag119-)(and (Flag107-)))

(:derived (Flag119-)(and (Flag91-)))

(:derived (Flag119-)(and (Flag99-)))

(:derived (Flag119-)(and (Flag116-)))

(:derived (Flag119-)(and (Flag80-)))

(:derived (Flag119-)(and (Flag101-)))

(:derived (Flag119-)(and (Flag86-)))

(:derived (Flag119-)(and (Flag112-)))

(:derived (Flag119-)(and (Flag117-)))

(:derived (Flag119-)(and (Flag94-)))

(:derived (Flag119-)(and (Flag118-)))

(:derived (Flag119-)(and (Flag113-)))

(:derived (Flag119-)(and (Flag114-)))

(:derived (Flag123-)(and (Flag120-)))

(:derived (Flag123-)(and (Flag121-)))

(:derived (Flag123-)(and (Flag118-)))

(:derived (Flag123-)(and (Flag97-)))

(:derived (Flag123-)(and (Flag113-)))

(:derived (Flag123-)(and (Flag106-)))

(:derived (Flag123-)(and (Flag80-)))

(:derived (Flag123-)(and (Flag90-)))

(:derived (Flag123-)(and (Flag122-)))

(:derived (Flag123-)(and (Flag101-)))

(:derived (Flag123-)(and (Flag86-)))

(:derived (Flag123-)(and (Flag94-)))

(:derived (Flag123-)(and (Flag104-)))

(:derived (Flag123-)(and (Flag114-)))

(:derived (Flag127-)(and (Flag125-)))

(:derived (Flag127-)(and (Flag118-)))

(:derived (Flag127-)(and (Flag126-)))

(:derived (Flag127-)(and (Flag90-)))

(:derived (Flag127-)(and (Flag113-)))

(:derived (Flag127-)(and (Flag80-)))

(:derived (Flag127-)(and (Flag101-)))

(:derived (Flag127-)(and (Flag104-)))

(:derived (Flag1-)(and (COLUMN2-ROBOT)(ROW1-ROBOT)(NOT-ERROR-)(NOT-CHECKCONSISTENCY-)))

(:derived (Flag2-)(and (LEFTOF8-ROBOT)))

(:derived (Flag2-)(and (RIGHTOF6-ROBOT)))

(:derived (Flag2-)(and (LEFTOF4-ROBOT)))

(:derived (Flag2-)(and (LEFTOF3-ROBOT)))

(:derived (Flag2-)(and (RIGHTOF7-ROBOT)))

(:derived (Flag2-)(and (LEFTOF5-ROBOT)))

(:derived (Flag2-)(and (LEFTOF6-ROBOT)))

(:derived (Flag2-)(and (RIGHTOF4-ROBOT)))

(:derived (Flag2-)(and (RIGHTOF0-ROBOT)))

(:derived (Flag2-)(and (LEFTOF2-ROBOT)))

(:derived (Flag2-)(and (RIGHTOF5-ROBOT)))

(:derived (Flag2-)(and (LEFTOF1-ROBOT)))

(:derived (Flag2-)(and (RIGHTOF1-ROBOT)))

(:derived (Flag2-)(and (RIGHTOF3-ROBOT)))

(:derived (Flag24-)(and (Flag17-)))

(:derived (Flag24-)(and (Flag18-)))

(:derived (Flag24-)(and (Flag19-)))

(:derived (Flag24-)(and (Flag20-)))

(:derived (Flag24-)(and (Flag21-)))

(:derived (Flag24-)(and (Flag22-)))

(:derived (Flag24-)(and (Flag23-)))

(:derived (Flag32-)(and (Flag18-)))

(:derived (Flag32-)(and (Flag25-)))

(:derived (Flag32-)(and (Flag26-)))

(:derived (Flag32-)(and (Flag17-)))

(:derived (Flag32-)(and (Flag21-)))

(:derived (Flag32-)(and (Flag20-)))

(:derived (Flag32-)(and (Flag19-)))

(:derived (Flag32-)(and (Flag22-)))

(:derived (Flag32-)(and (Flag27-)))

(:derived (Flag32-)(and (Flag28-)))

(:derived (Flag32-)(and (Flag23-)))

(:derived (Flag32-)(and (Flag29-)))

(:derived (Flag32-)(and (Flag30-)))

(:derived (Flag32-)(and (Flag31-)))

(:derived (Flag40-)(and (Flag18-)))

(:derived (Flag40-)(and (Flag33-)))

(:derived (Flag40-)(and (Flag34-)))

(:derived (Flag40-)(and (Flag35-)))

(:derived (Flag40-)(and (Flag36-)))

(:derived (Flag40-)(and (Flag20-)))

(:derived (Flag40-)(and (Flag19-)))

(:derived (Flag40-)(and (Flag22-)))

(:derived (Flag40-)(and (Flag37-)))

(:derived (Flag40-)(and (Flag23-)))

(:derived (Flag40-)(and (Flag25-)))

(:derived (Flag40-)(and (Flag28-)))

(:derived (Flag40-)(and (Flag38-)))

(:derived (Flag40-)(and (Flag39-)))

(:derived (Flag40-)(and (Flag27-)))

(:derived (Flag40-)(and (Flag29-)))

(:derived (Flag40-)(and (Flag30-)))

(:derived (Flag40-)(and (Flag17-)))

(:derived (Flag47-)(and (Flag23-)))

(:derived (Flag47-)(and (Flag34-)))

(:derived (Flag47-)(and (Flag17-)))

(:derived (Flag47-)(and (Flag22-)))

(:derived (Flag47-)(and (Flag19-)))

(:derived (Flag47-)(and (Flag20-)))

(:derived (Flag47-)(and (Flag41-)))

(:derived (Flag47-)(and (Flag42-)))

(:derived (Flag47-)(and (Flag37-)))

(:derived (Flag47-)(and (Flag43-)))

(:derived (Flag47-)(and (Flag44-)))

(:derived (Flag47-)(and (Flag28-)))

(:derived (Flag47-)(and (Flag38-)))

(:derived (Flag47-)(and (Flag45-)))

(:derived (Flag47-)(and (Flag27-)))

(:derived (Flag47-)(and (Flag29-)))

(:derived (Flag47-)(and (Flag36-)))

(:derived (Flag47-)(and (Flag25-)))

(:derived (Flag47-)(and (Flag30-)))

(:derived (Flag47-)(and (Flag46-)))

(:derived (Flag52-)(and (Flag17-)))

(:derived (Flag52-)(and (Flag34-)))

(:derived (Flag52-)(and (Flag48-)))

(:derived (Flag52-)(and (Flag36-)))

(:derived (Flag52-)(and (Flag49-)))

(:derived (Flag52-)(and (Flag20-)))

(:derived (Flag52-)(and (Flag28-)))

(:derived (Flag52-)(and (Flag19-)))

(:derived (Flag52-)(and (Flag22-)))

(:derived (Flag52-)(and (Flag37-)))

(:derived (Flag52-)(and (Flag43-)))

(:derived (Flag52-)(and (Flag50-)))

(:derived (Flag52-)(and (Flag44-)))

(:derived (Flag52-)(and (Flag51-)))

(:derived (Flag52-)(and (Flag38-)))

(:derived (Flag52-)(and (Flag45-)))

(:derived (Flag52-)(and (Flag27-)))

(:derived (Flag52-)(and (Flag29-)))

(:derived (Flag52-)(and (Flag25-)))

(:derived (Flag52-)(and (Flag42-)))

(:derived (Flag57-)(and (Flag29-)))

(:derived (Flag57-)(and (Flag53-)))

(:derived (Flag57-)(and (Flag36-)))

(:derived (Flag57-)(and (Flag19-)))

(:derived (Flag57-)(and (Flag20-)))

(:derived (Flag57-)(and (Flag28-)))

(:derived (Flag57-)(and (Flag54-)))

(:derived (Flag57-)(and (Flag22-)))

(:derived (Flag57-)(and (Flag55-)))

(:derived (Flag57-)(and (Flag56-)))

(:derived (Flag57-)(and (Flag50-)))

(:derived (Flag57-)(and (Flag34-)))

(:derived (Flag57-)(and (Flag51-)))

(:derived (Flag57-)(and (Flag38-)))

(:derived (Flag57-)(and (Flag45-)))

(:derived (Flag57-)(and (Flag43-)))

(:derived (Flag57-)(and (Flag25-)))

(:derived (Flag57-)(and (Flag42-)))

(:derived (Flag61-)(and (Flag29-)))

(:derived (Flag61-)(and (Flag34-)))

(:derived (Flag61-)(and (Flag58-)))

(:derived (Flag61-)(and (Flag59-)))

(:derived (Flag61-)(and (Flag19-)))

(:derived (Flag61-)(and (Flag55-)))

(:derived (Flag61-)(and (Flag50-)))

(:derived (Flag61-)(and (Flag25-)))

(:derived (Flag61-)(and (Flag51-)))

(:derived (Flag61-)(and (Flag38-)))

(:derived (Flag61-)(and (Flag45-)))

(:derived (Flag61-)(and (Flag43-)))

(:derived (Flag61-)(and (Flag20-)))

(:derived (Flag61-)(and (Flag60-)))

(:derived (Flag65-)(and (Flag29-)))

(:derived (Flag65-)(and (Flag20-)))

(:derived (Flag65-)(and (Flag55-)))

(:derived (Flag65-)(and (Flag63-)))

(:derived (Flag65-)(and (Flag64-)))

(:derived (Flag65-)(and (Flag38-)))

(:derived (Flag65-)(and (Flag45-)))

(:derived (Flag65-)(and (Flag50-)))

(:derived (Flag66-)(and (RIGHTOF3-ROBOT)))

(:derived (Flag66-)(and (RIGHTOF5-ROBOT)))

(:derived (Flag66-)(and (RIGHTOF0-ROBOT)))

(:derived (Flag66-)(and (RIGHTOF4-ROBOT)))

(:derived (Flag66-)(and (RIGHTOF1-ROBOT)))

(:derived (Flag66-)(and (RIGHTOF7-ROBOT)))

(:derived (Flag66-)(and (RIGHTOF6-ROBOT)))

(:derived (Flag67-)(and (RIGHTOF3-ROBOT)))

(:derived (Flag67-)(and (RIGHTOF5-ROBOT)))

(:derived (Flag67-)(and (RIGHTOF4-ROBOT)))

(:derived (Flag67-)(and (RIGHTOF1-ROBOT)))

(:derived (Flag67-)(and (RIGHTOF7-ROBOT)))

(:derived (Flag67-)(and (RIGHTOF6-ROBOT)))

(:derived (Flag68-)(and (RIGHTOF3-ROBOT)))

(:derived (Flag68-)(and (RIGHTOF5-ROBOT)))

(:derived (Flag68-)(and (RIGHTOF4-ROBOT)))

(:derived (Flag68-)(and (RIGHTOF7-ROBOT)))

(:derived (Flag68-)(and (RIGHTOF6-ROBOT)))

(:derived (Flag69-)(and (RIGHTOF7-ROBOT)))

(:derived (Flag69-)(and (RIGHTOF3-ROBOT)))

(:derived (Flag69-)(and (RIGHTOF4-ROBOT)))

(:derived (Flag69-)(and (RIGHTOF5-ROBOT)))

(:derived (Flag69-)(and (RIGHTOF6-ROBOT)))

(:derived (Flag70-)(and (RIGHTOF7-ROBOT)))

(:derived (Flag70-)(and (RIGHTOF4-ROBOT)))

(:derived (Flag70-)(and (RIGHTOF5-ROBOT)))

(:derived (Flag70-)(and (RIGHTOF6-ROBOT)))

(:derived (Flag71-)(and (RIGHTOF7-ROBOT)))

(:derived (Flag71-)(and (RIGHTOF5-ROBOT)))

(:derived (Flag71-)(and (RIGHTOF6-ROBOT)))

(:derived (Flag72-)(and (RIGHTOF7-ROBOT)))

(:derived (Flag72-)(and (RIGHTOF6-ROBOT)))

(:derived (Flag73-)(and (LEFTOF1-ROBOT)))

(:derived (Flag73-)(and (LEFTOF2-ROBOT)))

(:derived (Flag74-)(and (LEFTOF1-ROBOT)))

(:derived (Flag74-)(and (LEFTOF3-ROBOT)))

(:derived (Flag74-)(and (LEFTOF2-ROBOT)))

(:derived (Flag75-)(and (LEFTOF1-ROBOT)))

(:derived (Flag75-)(and (LEFTOF3-ROBOT)))

(:derived (Flag75-)(and (LEFTOF4-ROBOT)))

(:derived (Flag75-)(and (LEFTOF2-ROBOT)))

(:derived (Flag76-)(and (LEFTOF5-ROBOT)))

(:derived (Flag76-)(and (LEFTOF1-ROBOT)))

(:derived (Flag76-)(and (LEFTOF3-ROBOT)))

(:derived (Flag76-)(and (LEFTOF4-ROBOT)))

(:derived (Flag76-)(and (LEFTOF2-ROBOT)))

(:derived (Flag77-)(and (LEFTOF2-ROBOT)))

(:derived (Flag77-)(and (LEFTOF1-ROBOT)))

(:derived (Flag77-)(and (LEFTOF6-ROBOT)))

(:derived (Flag77-)(and (LEFTOF5-ROBOT)))

(:derived (Flag77-)(and (LEFTOF3-ROBOT)))

(:derived (Flag77-)(and (LEFTOF4-ROBOT)))

(:derived (Flag78-)(and (LEFTOF2-ROBOT)))

(:derived (Flag78-)(and (LEFTOF1-ROBOT)))

(:derived (Flag78-)(and (LEFTOF6-ROBOT)))

(:derived (Flag78-)(and (LEFTOF5-ROBOT)))

(:derived (Flag78-)(and (LEFTOF3-ROBOT)))

(:derived (Flag78-)(and (LEFTOF4-ROBOT)))

(:derived (Flag79-)(and (LEFTOF1-ROBOT)(RIGHTOF4-ROBOT)))

(:derived (Flag80-)(and (LEFTOF1-ROBOT)(RIGHTOF7-ROBOT)))

(:derived (Flag81-)(and (LEFTOF1-ROBOT)(RIGHTOF1-ROBOT)))

(:derived (Flag82-)(and (RIGHTOF3-ROBOT)(LEFTOF1-ROBOT)))

(:derived (Flag83-)(and (RIGHTOF0-ROBOT)(LEFTOF1-ROBOT)))

(:derived (Flag84-)(and (LEFTOF1-ROBOT)(RIGHTOF5-ROBOT)))

(:derived (Flag85-)(and (LEFTOF1-ROBOT)(RIGHTOF2-ROBOT)))

(:derived (Flag86-)(and (LEFTOF1-ROBOT)(RIGHTOF6-ROBOT)))

(:derived (Flag88-)(and (LEFTOF2-ROBOT)(RIGHTOF1-ROBOT)))

(:derived (Flag89-)(and (RIGHTOF5-ROBOT)(LEFTOF2-ROBOT)))

(:derived (Flag90-)(and (LEFTOF2-ROBOT)(RIGHTOF7-ROBOT)))

(:derived (Flag91-)(and (RIGHTOF5-ROBOT)(LEFTOF1-ROBOT)))

(:derived (Flag92-)(and (RIGHTOF2-ROBOT)(LEFTOF2-ROBOT)))

(:derived (Flag93-)(and (RIGHTOF3-ROBOT)(LEFTOF2-ROBOT)))

(:derived (Flag94-)(and (LEFTOF2-ROBOT)(RIGHTOF6-ROBOT)))

(:derived (Flag95-)(and (LEFTOF2-ROBOT)(RIGHTOF4-ROBOT)))

(:derived (Flag97-)(and (LEFTOF3-ROBOT)(RIGHTOF6-ROBOT)))

(:derived (Flag98-)(and (RIGHTOF3-ROBOT)(LEFTOF3-ROBOT)))

(:derived (Flag99-)(and (LEFTOF3-ROBOT)(RIGHTOF5-ROBOT)))

(:derived (Flag100-)(and (LEFTOF3-ROBOT)(RIGHTOF4-ROBOT)))

(:derived (Flag101-)(and (LEFTOF3-ROBOT)(RIGHTOF7-ROBOT)))

(:derived (Flag102-)(and (LEFTOF3-ROBOT)(RIGHTOF2-ROBOT)))

(:derived (Flag104-)(and (LEFTOF4-ROBOT)(RIGHTOF7-ROBOT)))

(:derived (Flag105-)(and (LEFTOF3-ROBOT)(RIGHTOF3-ROBOT)))

(:derived (Flag106-)(and (LEFTOF4-ROBOT)(RIGHTOF6-ROBOT)))

(:derived (Flag107-)(and (LEFTOF4-ROBOT)(RIGHTOF5-ROBOT)))

(:derived (Flag108-)(and (RIGHTOF3-ROBOT)(LEFTOF4-ROBOT)))

(:derived (Flag109-)(and (LEFTOF4-ROBOT)(RIGHTOF4-ROBOT)))

(:derived (Flag111-)(and (RIGHTOF4-ROBOT)(LEFTOF5-ROBOT)))

(:derived (Flag112-)(and (RIGHTOF5-ROBOT)(LEFTOF5-ROBOT)))

(:derived (Flag113-)(and (RIGHTOF7-ROBOT)(LEFTOF5-ROBOT)))

(:derived (Flag114-)(and (LEFTOF5-ROBOT)(RIGHTOF6-ROBOT)))

(:derived (Flag116-)(and (RIGHTOF5-ROBOT)(LEFTOF6-ROBOT)))

(:derived (Flag117-)(and (RIGHTOF6-ROBOT)(LEFTOF6-ROBOT)))

(:derived (Flag118-)(and (RIGHTOF7-ROBOT)(LEFTOF6-ROBOT)))

(:derived (Flag120-)(and (LEFTOF6-ROBOT)(RIGHTOF6-ROBOT)))

(:derived (Flag121-)(and (LEFTOF7-ROBOT)(RIGHTOF6-ROBOT)))

(:derived (Flag122-)(and (LEFTOF7-ROBOT)(RIGHTOF7-ROBOT)))

(:derived (Flag124-)(and (LEFTOF2-ROBOT)))

(:derived (Flag124-)(and (LEFTOF1-ROBOT)))

(:derived (Flag124-)(and (LEFTOF6-ROBOT)))

(:derived (Flag124-)(and (LEFTOF5-ROBOT)))

(:derived (Flag124-)(and (LEFTOF3-ROBOT)))

(:derived (Flag124-)(and (LEFTOF4-ROBOT)))

(:derived (Flag124-)(and (LEFTOF8-ROBOT)))

(:derived (Flag125-)(and (RIGHTOF7-ROBOT)(LEFTOF7-ROBOT)))

(:derived (Flag126-)(and (RIGHTOF7-ROBOT)(LEFTOF8-ROBOT)))

(:action MOVELEFT-ROBOT
:parameters ()
:precondition
(and
(Flag3-)
)
:effect
(and
(when
(and
(Flag127-)
)
(and
(COLUMN6-ROBOT)
(not (NOT-COLUMN6-ROBOT))
)
)
(when
(and
(Flag123-)
)
(and
(COLUMN5-ROBOT)
(not (NOT-COLUMN5-ROBOT))
)
)
(when
(and
(Flag119-)
)
(and
(COLUMN4-ROBOT)
(not (NOT-COLUMN4-ROBOT))
)
)
(when
(and
(Flag115-)
)
(and
(COLUMN3-ROBOT)
(not (NOT-COLUMN3-ROBOT))
)
)
(when
(and
(Flag110-)
)
(and
(COLUMN2-ROBOT)
(not (NOT-COLUMN2-ROBOT))
)
)
(when
(and
(Flag103-)
)
(and
(COLUMN1-ROBOT)
(not (NOT-COLUMN1-ROBOT))
)
)
(when
(and
(Flag96-)
)
(and
(COLUMN0-ROBOT)
(not (NOT-COLUMN0-ROBOT))
)
)
(when
(and
(COLUMN7-ROBOT)
)
(and
(COLUMN6-ROBOT)
(NOT-COLUMN7-ROBOT)
(not (NOT-COLUMN6-ROBOT))
(not (COLUMN7-ROBOT))
)
)
(when
(and
(COLUMN6-ROBOT)
)
(and
(COLUMN5-ROBOT)
(NOT-COLUMN6-ROBOT)
(not (NOT-COLUMN5-ROBOT))
(not (COLUMN6-ROBOT))
)
)
(when
(and
(COLUMN5-ROBOT)
)
(and
(COLUMN4-ROBOT)
(NOT-COLUMN5-ROBOT)
(not (NOT-COLUMN4-ROBOT))
(not (COLUMN5-ROBOT))
)
)
(when
(and
(COLUMN4-ROBOT)
)
(and
(COLUMN3-ROBOT)
(NOT-COLUMN4-ROBOT)
(not (NOT-COLUMN3-ROBOT))
(not (COLUMN4-ROBOT))
)
)
(when
(and
(COLUMN3-ROBOT)
)
(and
(COLUMN2-ROBOT)
(NOT-COLUMN3-ROBOT)
(not (NOT-COLUMN2-ROBOT))
(not (COLUMN3-ROBOT))
)
)
(when
(and
(COLUMN2-ROBOT)
)
(and
(COLUMN1-ROBOT)
(NOT-COLUMN2-ROBOT)
(not (NOT-COLUMN1-ROBOT))
(not (COLUMN2-ROBOT))
)
)
(when
(and
(COLUMN1-ROBOT)
)
(and
(COLUMN0-ROBOT)
(NOT-COLUMN1-ROBOT)
(not (NOT-COLUMN0-ROBOT))
(not (COLUMN1-ROBOT))
)
)
(when
(and
(RIGHTOF7-ROBOT)
)
(and
(RIGHTOF6-ROBOT)
(NOT-RIGHTOF7-ROBOT)
(RIGHTOF5-ROBOT)
(NOT-RIGHTOF6-ROBOT)
(RIGHTOF4-ROBOT)
(NOT-RIGHTOF5-ROBOT)
(RIGHTOF3-ROBOT)
(NOT-RIGHTOF4-ROBOT)
(RIGHTOF2-ROBOT)
(NOT-RIGHTOF3-ROBOT)
(RIGHTOF1-ROBOT)
(NOT-RIGHTOF2-ROBOT)
(RIGHTOF0-ROBOT)
(NOT-RIGHTOF1-ROBOT)
(not (RIGHTOF7-ROBOT))
(not (NOT-RIGHTOF0-ROBOT))
)
)
(when
(and
(RIGHTOF6-ROBOT)
)
(and
(RIGHTOF5-ROBOT)
(NOT-RIGHTOF6-ROBOT)
(RIGHTOF4-ROBOT)
(NOT-RIGHTOF5-ROBOT)
(RIGHTOF3-ROBOT)
(NOT-RIGHTOF4-ROBOT)
(RIGHTOF2-ROBOT)
(NOT-RIGHTOF3-ROBOT)
(RIGHTOF1-ROBOT)
(NOT-RIGHTOF2-ROBOT)
(RIGHTOF0-ROBOT)
(NOT-RIGHTOF1-ROBOT)
(not (RIGHTOF6-ROBOT))
(not (NOT-RIGHTOF0-ROBOT))
)
)
(when
(and
(RIGHTOF5-ROBOT)
)
(and
(RIGHTOF4-ROBOT)
(NOT-RIGHTOF5-ROBOT)
(RIGHTOF3-ROBOT)
(NOT-RIGHTOF4-ROBOT)
(RIGHTOF2-ROBOT)
(NOT-RIGHTOF3-ROBOT)
(RIGHTOF1-ROBOT)
(NOT-RIGHTOF2-ROBOT)
(RIGHTOF0-ROBOT)
(NOT-RIGHTOF1-ROBOT)
(not (RIGHTOF5-ROBOT))
(not (NOT-RIGHTOF0-ROBOT))
)
)
(when
(and
(RIGHTOF4-ROBOT)
)
(and
(RIGHTOF3-ROBOT)
(NOT-RIGHTOF4-ROBOT)
(RIGHTOF2-ROBOT)
(NOT-RIGHTOF3-ROBOT)
(RIGHTOF1-ROBOT)
(NOT-RIGHTOF2-ROBOT)
(RIGHTOF0-ROBOT)
(NOT-RIGHTOF1-ROBOT)
(not (RIGHTOF4-ROBOT))
(not (NOT-RIGHTOF0-ROBOT))
)
)
(when
(and
(RIGHTOF3-ROBOT)
)
(and
(RIGHTOF2-ROBOT)
(NOT-RIGHTOF3-ROBOT)
(RIGHTOF1-ROBOT)
(NOT-RIGHTOF2-ROBOT)
(RIGHTOF0-ROBOT)
(NOT-RIGHTOF1-ROBOT)
(not (RIGHTOF3-ROBOT))
(not (NOT-RIGHTOF0-ROBOT))
)
)
(when
(and
(RIGHTOF2-ROBOT)
)
(and
(RIGHTOF1-ROBOT)
(NOT-RIGHTOF2-ROBOT)
(RIGHTOF0-ROBOT)
(NOT-RIGHTOF1-ROBOT)
(not (RIGHTOF2-ROBOT))
(not (NOT-RIGHTOF0-ROBOT))
)
)
(when
(and
(RIGHTOF1-ROBOT)
)
(and
(RIGHTOF0-ROBOT)
(NOT-RIGHTOF1-ROBOT)
(not (NOT-RIGHTOF0-ROBOT))
(not (RIGHTOF1-ROBOT))
)
)
(when
(and
(Flag124-)
)
(and
(LEFTOF7-ROBOT)
(not (NOT-LEFTOF7-ROBOT))
)
)
(when
(and
(LEFTOF4-ROBOT)
)
(and
(LEFTOF6-ROBOT)
(LEFTOF5-ROBOT)
(LEFTOF3-ROBOT)
(not (NOT-LEFTOF6-ROBOT))
(not (NOT-LEFTOF5-ROBOT))
(not (NOT-LEFTOF4-ROBOT))
(not (NOT-LEFTOF3-ROBOT))
)
)
(when
(and
(LEFTOF3-ROBOT)
)
(and
(LEFTOF6-ROBOT)
(LEFTOF5-ROBOT)
(LEFTOF4-ROBOT)
(LEFTOF2-ROBOT)
(not (NOT-LEFTOF6-ROBOT))
(not (NOT-LEFTOF5-ROBOT))
(not (NOT-LEFTOF4-ROBOT))
(not (NOT-LEFTOF3-ROBOT))
(not (NOT-LEFTOF2-ROBOT))
)
)
(when
(and
(LEFTOF5-ROBOT)
)
(and
(LEFTOF6-ROBOT)
(LEFTOF4-ROBOT)
(not (NOT-LEFTOF6-ROBOT))
(not (NOT-LEFTOF5-ROBOT))
(not (NOT-LEFTOF4-ROBOT))
)
)
(when
(and
(LEFTOF6-ROBOT)
)
(and
(LEFTOF5-ROBOT)
(not (NOT-LEFTOF6-ROBOT))
(not (NOT-LEFTOF5-ROBOT))
)
)
(when
(and
(LEFTOF1-ROBOT)
)
(and
(LEFTOF6-ROBOT)
(LEFTOF5-ROBOT)
(LEFTOF4-ROBOT)
(LEFTOF3-ROBOT)
(LEFTOF2-ROBOT)
(not (NOT-LEFTOF6-ROBOT))
(not (NOT-LEFTOF5-ROBOT))
(not (NOT-LEFTOF4-ROBOT))
(not (NOT-LEFTOF3-ROBOT))
(not (NOT-LEFTOF2-ROBOT))
(not (NOT-LEFTOF1-ROBOT))
)
)
(when
(and
(LEFTOF7-ROBOT)
)
(and
(LEFTOF6-ROBOT)
(not (NOT-LEFTOF6-ROBOT))
)
)
(when
(and
(LEFTOF2-ROBOT)
)
(and
(LEFTOF6-ROBOT)
(LEFTOF5-ROBOT)
(LEFTOF4-ROBOT)
(LEFTOF3-ROBOT)
(LEFTOF1-ROBOT)
(not (NOT-LEFTOF6-ROBOT))
(not (NOT-LEFTOF5-ROBOT))
(not (NOT-LEFTOF4-ROBOT))
(not (NOT-LEFTOF3-ROBOT))
(not (NOT-LEFTOF2-ROBOT))
(not (NOT-LEFTOF1-ROBOT))
)
)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action MOVERIGHT-ROBOT
:parameters ()
:precondition
(and
(Flag3-)
)
:effect
(and
(when
(and
(Flag123-)
)
(and
(COLUMN7-ROBOT)
(not (NOT-COLUMN7-ROBOT))
)
)
(when
(and
(Flag119-)
)
(and
(COLUMN6-ROBOT)
(not (NOT-COLUMN6-ROBOT))
)
)
(when
(and
(Flag115-)
)
(and
(COLUMN5-ROBOT)
(not (NOT-COLUMN5-ROBOT))
)
)
(when
(and
(Flag110-)
)
(and
(COLUMN4-ROBOT)
(not (NOT-COLUMN4-ROBOT))
)
)
(when
(and
(Flag103-)
)
(and
(COLUMN3-ROBOT)
(not (NOT-COLUMN3-ROBOT))
)
)
(when
(and
(Flag96-)
)
(and
(COLUMN2-ROBOT)
(not (NOT-COLUMN2-ROBOT))
)
)
(when
(and
(Flag87-)
)
(and
(COLUMN1-ROBOT)
(not (NOT-COLUMN1-ROBOT))
)
)
(when
(and
(COLUMN6-ROBOT)
)
(and
(COLUMN7-ROBOT)
(NOT-COLUMN6-ROBOT)
(not (NOT-COLUMN7-ROBOT))
(not (COLUMN6-ROBOT))
)
)
(when
(and
(COLUMN5-ROBOT)
)
(and
(COLUMN6-ROBOT)
(NOT-COLUMN5-ROBOT)
(not (NOT-COLUMN6-ROBOT))
(not (COLUMN5-ROBOT))
)
)
(when
(and
(COLUMN4-ROBOT)
)
(and
(COLUMN5-ROBOT)
(NOT-COLUMN4-ROBOT)
(not (NOT-COLUMN5-ROBOT))
(not (COLUMN4-ROBOT))
)
)
(when
(and
(COLUMN3-ROBOT)
)
(and
(COLUMN4-ROBOT)
(NOT-COLUMN3-ROBOT)
(not (NOT-COLUMN4-ROBOT))
(not (COLUMN3-ROBOT))
)
)
(when
(and
(COLUMN2-ROBOT)
)
(and
(COLUMN3-ROBOT)
(NOT-COLUMN2-ROBOT)
(not (NOT-COLUMN3-ROBOT))
(not (COLUMN2-ROBOT))
)
)
(when
(and
(COLUMN1-ROBOT)
)
(and
(COLUMN2-ROBOT)
(NOT-COLUMN1-ROBOT)
(not (NOT-COLUMN2-ROBOT))
(not (COLUMN1-ROBOT))
)
)
(when
(and
(COLUMN0-ROBOT)
)
(and
(COLUMN1-ROBOT)
(NOT-COLUMN0-ROBOT)
(not (NOT-COLUMN1-ROBOT))
(not (COLUMN0-ROBOT))
)
)
(when
(and
(Flag78-)
)
(and
(LEFTOF8-ROBOT)
(NOT-LEFTOF7-ROBOT)
(not (NOT-LEFTOF8-ROBOT))
(not (LEFTOF7-ROBOT))
)
)
(when
(and
(Flag77-)
)
(and
(LEFTOF7-ROBOT)
(NOT-LEFTOF6-ROBOT)
(not (NOT-LEFTOF7-ROBOT))
(not (LEFTOF6-ROBOT))
)
)
(when
(and
(Flag76-)
)
(and
(LEFTOF6-ROBOT)
(NOT-LEFTOF5-ROBOT)
(not (NOT-LEFTOF6-ROBOT))
(not (LEFTOF5-ROBOT))
)
)
(when
(and
(Flag75-)
)
(and
(LEFTOF5-ROBOT)
(NOT-LEFTOF4-ROBOT)
(not (NOT-LEFTOF5-ROBOT))
(not (LEFTOF4-ROBOT))
)
)
(when
(and
(Flag74-)
)
(and
(LEFTOF4-ROBOT)
(NOT-LEFTOF3-ROBOT)
(not (NOT-LEFTOF4-ROBOT))
(not (LEFTOF3-ROBOT))
)
)
(when
(and
(Flag73-)
)
(and
(LEFTOF3-ROBOT)
(NOT-LEFTOF2-ROBOT)
(not (NOT-LEFTOF3-ROBOT))
(not (LEFTOF2-ROBOT))
)
)
(when
(and
(LEFTOF1-ROBOT)
)
(and
(LEFTOF2-ROBOT)
(NOT-LEFTOF1-ROBOT)
(not (NOT-LEFTOF2-ROBOT))
(not (LEFTOF1-ROBOT))
)
)
(when
(and
(Flag72-)
)
(and
(RIGHTOF7-ROBOT)
(not (NOT-RIGHTOF7-ROBOT))
)
)
(when
(and
(Flag71-)
)
(and
(RIGHTOF6-ROBOT)
(not (NOT-RIGHTOF6-ROBOT))
)
)
(when
(and
(Flag70-)
)
(and
(RIGHTOF5-ROBOT)
(not (NOT-RIGHTOF5-ROBOT))
)
)
(when
(and
(Flag69-)
)
(and
(RIGHTOF4-ROBOT)
(not (NOT-RIGHTOF4-ROBOT))
)
)
(when
(and
(Flag68-)
)
(and
(RIGHTOF3-ROBOT)
(not (NOT-RIGHTOF3-ROBOT))
)
)
(when
(and
(Flag67-)
)
(and
(RIGHTOF2-ROBOT)
(not (NOT-RIGHTOF2-ROBOT))
)
)
(when
(and
(Flag66-)
)
(and
(RIGHTOF1-ROBOT)
(not (NOT-RIGHTOF1-ROBOT))
)
)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action CHECKCONSISTENCYACTION
:parameters ()
:precondition
(and
(NOT-ERROR-)
(CHECKCONSISTENCY-)
)
:effect
(and
(when
(and
(BELOWOF1-ROBOT)
(ABOVEOF2-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF6-ROBOT)
(BELOWOF4-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF1-ROBOT)
(RIGHTOF4-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF5-ROBOT)
(RIGHTOF7-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF1-ROBOT)
(ABOVEOF7-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF1-ROBOT)
(RIGHTOF3-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF3-ROBOT)
(LEFTOF2-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF1-ROBOT)
(RIGHTOF5-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF7-ROBOT)
(BELOWOF4-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF7-ROBOT)
(LEFTOF3-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF6-ROBOT)
(LEFTOF2-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF4-ROBOT)
(BELOWOF2-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF6-ROBOT)
(BELOWOF3-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF6-ROBOT)
(RIGHTOF7-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF5-ROBOT)
(BELOWOF2-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF3-ROBOT)
(BELOWOF2-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF1-ROBOT)
(BELOWOF1-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF5-ROBOT)
(BELOWOF3-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF1-ROBOT)
(RIGHTOF1-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF7-ROBOT)
(LEFTOF2-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF6-ROBOT)
(LEFTOF5-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF6-ROBOT)
(RIGHTOF6-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF2-ROBOT)
(RIGHTOF2-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF2-ROBOT)
(RIGHTOF5-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF7-ROBOT)
(BELOWOF2-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF7-ROBOT)
(LEFTOF1-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF2-ROBOT)
(RIGHTOF4-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF4-ROBOT)
(RIGHTOF6-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF1-ROBOT)
(RIGHTOF6-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF7-ROBOT)
(BELOWOF3-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF6-ROBOT)
(BELOWOF1-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF6-ROBOT)
(BELOWOF6-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF4-ROBOT)
(BELOWOF1-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF5-ROBOT)
(ABOVEOF6-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF4-ROBOT)
(LEFTOF3-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF7-ROBOT)
(LEFTOF4-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF3-ROBOT)
(ABOVEOF4-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF6-ROBOT)
(LEFTOF3-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF4-ROBOT)
(ABOVEOF4-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF3-ROBOT)
(BELOWOF1-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF7-ROBOT)
(BELOWOF6-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF6-ROBOT)
(BELOWOF2-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF5-ROBOT)
(LEFTOF3-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF5-ROBOT)
(BELOWOF4-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF5-ROBOT)
(BELOWOF1-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF7-ROBOT)
(RIGHTOF7-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF5-ROBOT)
(RIGHTOF5-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF7-ROBOT)
(BELOWOF5-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF5-ROBOT)
(BELOWOF5-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF7-ROBOT)
(BELOWOF7-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF5-ROBOT)
(LEFTOF4-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF2-ROBOT)
(BELOWOF2-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF2-ROBOT)
(LEFTOF1-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF3-ROBOT)
(LEFTOF3-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF3-ROBOT)
(BELOWOF3-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF4-ROBOT)
(RIGHTOF4-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(NOT-CHECKCONSISTENCY-)
(not (CHECKCONSISTENCY-))
)
)
(:derived (Flag3-)(and (NOT-CHECKCONSISTENCY-)(NOT-ERROR-)(Flag2-)))

(:derived (Flag5-)(and (ABOVEOF6-ROBOT)))

(:derived (Flag5-)(and (ABOVEOF1-ROBOT)))

(:derived (Flag5-)(and (ABOVEOF2-ROBOT)))

(:derived (Flag5-)(and (ABOVEOF7-ROBOT)))

(:derived (Flag5-)(and (ABOVEOF4-ROBOT)))

(:derived (Flag5-)(and (ABOVEOF3-ROBOT)))

(:derived (Flag5-)(and (ABOVEOF5-ROBOT)))

(:derived (Flag6-)(and (ABOVEOF6-ROBOT)))

(:derived (Flag6-)(and (ABOVEOF2-ROBOT)))

(:derived (Flag6-)(and (ABOVEOF7-ROBOT)))

(:derived (Flag6-)(and (ABOVEOF4-ROBOT)))

(:derived (Flag6-)(and (ABOVEOF3-ROBOT)))

(:derived (Flag6-)(and (ABOVEOF5-ROBOT)))

(:derived (Flag7-)(and (ABOVEOF4-ROBOT)))

(:derived (Flag7-)(and (ABOVEOF3-ROBOT)))

(:derived (Flag7-)(and (ABOVEOF7-ROBOT)))

(:derived (Flag7-)(and (ABOVEOF5-ROBOT)))

(:derived (Flag7-)(and (ABOVEOF6-ROBOT)))

(:derived (Flag8-)(and (ABOVEOF4-ROBOT)))

(:derived (Flag8-)(and (ABOVEOF7-ROBOT)))

(:derived (Flag8-)(and (ABOVEOF5-ROBOT)))

(:derived (Flag8-)(and (ABOVEOF6-ROBOT)))

(:derived (Flag9-)(and (ABOVEOF7-ROBOT)))

(:derived (Flag9-)(and (ABOVEOF5-ROBOT)))

(:derived (Flag9-)(and (ABOVEOF6-ROBOT)))

(:derived (Flag10-)(and (ABOVEOF7-ROBOT)))

(:derived (Flag10-)(and (ABOVEOF6-ROBOT)))

(:derived (Flag11-)(and (BELOWOF2-ROBOT)))

(:derived (Flag11-)(and (BELOWOF1-ROBOT)))

(:derived (Flag12-)(and (BELOWOF2-ROBOT)))

(:derived (Flag12-)(and (BELOWOF1-ROBOT)))

(:derived (Flag12-)(and (BELOWOF3-ROBOT)))

(:derived (Flag13-)(and (BELOWOF2-ROBOT)))

(:derived (Flag13-)(and (BELOWOF1-ROBOT)))

(:derived (Flag13-)(and (BELOWOF3-ROBOT)))

(:derived (Flag13-)(and (BELOWOF4-ROBOT)))

(:derived (Flag14-)(and (BELOWOF2-ROBOT)))

(:derived (Flag14-)(and (BELOWOF1-ROBOT)))

(:derived (Flag14-)(and (BELOWOF3-ROBOT)))

(:derived (Flag14-)(and (BELOWOF4-ROBOT)))

(:derived (Flag14-)(and (BELOWOF5-ROBOT)))

(:derived (Flag15-)(and (BELOWOF6-ROBOT)))

(:derived (Flag15-)(and (BELOWOF5-ROBOT)))

(:derived (Flag15-)(and (BELOWOF2-ROBOT)))

(:derived (Flag15-)(and (BELOWOF1-ROBOT)))

(:derived (Flag15-)(and (BELOWOF4-ROBOT)))

(:derived (Flag15-)(and (BELOWOF3-ROBOT)))

(:derived (Flag16-)(and (BELOWOF6-ROBOT)))

(:derived (Flag16-)(and (BELOWOF5-ROBOT)))

(:derived (Flag16-)(and (BELOWOF2-ROBOT)))

(:derived (Flag16-)(and (BELOWOF1-ROBOT)))

(:derived (Flag16-)(and (BELOWOF4-ROBOT)))

(:derived (Flag16-)(and (BELOWOF3-ROBOT)))

(:derived (Flag17-)(and (BELOWOF1-ROBOT)(ABOVEOF4-ROBOT)))

(:derived (Flag18-)(and (BELOWOF1-ROBOT)(ABOVEOF2-ROBOT)))

(:derived (Flag19-)(and (ABOVEOF6-ROBOT)(BELOWOF1-ROBOT)))

(:derived (Flag20-)(and (BELOWOF1-ROBOT)(ABOVEOF7-ROBOT)))

(:derived (Flag21-)(and (BELOWOF1-ROBOT)(ABOVEOF1-ROBOT)))

(:derived (Flag22-)(and (BELOWOF1-ROBOT)(ABOVEOF5-ROBOT)))

(:derived (Flag23-)(and (ABOVEOF3-ROBOT)(BELOWOF1-ROBOT)))

(:derived (Flag24-)(and (BELOWOF1-ROBOT)))

(:derived (Flag25-)(and (ABOVEOF6-ROBOT)(BELOWOF2-ROBOT)))

(:derived (Flag26-)(and (ABOVEOF2-ROBOT)(BELOWOF2-ROBOT)))

(:derived (Flag27-)(and (ABOVEOF4-ROBOT)(BELOWOF2-ROBOT)))

(:derived (Flag28-)(and (BELOWOF2-ROBOT)(ABOVEOF5-ROBOT)))

(:derived (Flag29-)(and (ABOVEOF7-ROBOT)(BELOWOF2-ROBOT)))

(:derived (Flag30-)(and (ABOVEOF3-ROBOT)(BELOWOF2-ROBOT)))

(:derived (Flag31-)(and (ABOVEOF1-ROBOT)(BELOWOF2-ROBOT)))

(:derived (Flag33-)(and (BELOWOF3-ROBOT)(ABOVEOF2-ROBOT)))

(:derived (Flag34-)(and (BELOWOF3-ROBOT)(ABOVEOF6-ROBOT)))

(:derived (Flag35-)(and (ABOVEOF3-ROBOT)(BELOWOF3-ROBOT)))

(:derived (Flag36-)(and (BELOWOF3-ROBOT)(ABOVEOF5-ROBOT)))

(:derived (Flag37-)(and (BELOWOF3-ROBOT)(ABOVEOF4-ROBOT)))

(:derived (Flag38-)(and (BELOWOF3-ROBOT)(ABOVEOF7-ROBOT)))

(:derived (Flag39-)(and (BELOWOF2-ROBOT)(ABOVEOF2-ROBOT)))

(:derived (Flag41-)(and (ABOVEOF3-ROBOT)(BELOWOF4-ROBOT)))

(:derived (Flag42-)(and (BELOWOF4-ROBOT)(ABOVEOF5-ROBOT)))

(:derived (Flag43-)(and (ABOVEOF6-ROBOT)(BELOWOF4-ROBOT)))

(:derived (Flag44-)(and (BELOWOF4-ROBOT)(ABOVEOF4-ROBOT)))

(:derived (Flag45-)(and (BELOWOF4-ROBOT)(ABOVEOF7-ROBOT)))

(:derived (Flag46-)(and (BELOWOF3-ROBOT)(ABOVEOF3-ROBOT)))

(:derived (Flag48-)(and (ABOVEOF4-ROBOT)(BELOWOF5-ROBOT)))

(:derived (Flag49-)(and (ABOVEOF5-ROBOT)(BELOWOF5-ROBOT)))

(:derived (Flag50-)(and (ABOVEOF7-ROBOT)(BELOWOF5-ROBOT)))

(:derived (Flag51-)(and (ABOVEOF6-ROBOT)(BELOWOF5-ROBOT)))

(:derived (Flag53-)(and (BELOWOF6-ROBOT)(ABOVEOF5-ROBOT)))

(:derived (Flag54-)(and (BELOWOF6-ROBOT)(ABOVEOF6-ROBOT)))

(:derived (Flag55-)(and (BELOWOF6-ROBOT)(ABOVEOF7-ROBOT)))

(:derived (Flag56-)(and (BELOWOF5-ROBOT)(ABOVEOF5-ROBOT)))

(:derived (Flag58-)(and (ABOVEOF7-ROBOT)(BELOWOF7-ROBOT)))

(:derived (Flag59-)(and (ABOVEOF6-ROBOT)(BELOWOF7-ROBOT)))

(:derived (Flag60-)(and (ABOVEOF6-ROBOT)(BELOWOF6-ROBOT)))

(:derived (Flag62-)(and (BELOWOF6-ROBOT)))

(:derived (Flag62-)(and (BELOWOF5-ROBOT)))

(:derived (Flag62-)(and (BELOWOF2-ROBOT)))

(:derived (Flag62-)(and (BELOWOF1-ROBOT)))

(:derived (Flag62-)(and (BELOWOF4-ROBOT)))

(:derived (Flag62-)(and (BELOWOF3-ROBOT)))

(:derived (Flag62-)(and (BELOWOF8-ROBOT)))

(:derived (Flag63-)(and (BELOWOF8-ROBOT)(ABOVEOF7-ROBOT)))

(:derived (Flag64-)(and (BELOWOF7-ROBOT)(ABOVEOF7-ROBOT)))

(:action MOVEDOWN-ROBOT
:parameters ()
:precondition
(and
(Flag4-)
)
:effect
(and
(when
(and
(Flag65-)
)
(and
(ROW6-ROBOT)
(not (NOT-ROW6-ROBOT))
)
)
(when
(and
(Flag61-)
)
(and
(ROW5-ROBOT)
(not (NOT-ROW5-ROBOT))
)
)
(when
(and
(Flag57-)
)
(and
(ROW4-ROBOT)
(not (NOT-ROW4-ROBOT))
)
)
(when
(and
(Flag52-)
)
(and
(ROW3-ROBOT)
(not (NOT-ROW3-ROBOT))
)
)
(when
(and
(Flag47-)
)
(and
(ROW2-ROBOT)
(not (NOT-ROW2-ROBOT))
)
)
(when
(and
(Flag40-)
)
(and
(ROW1-ROBOT)
(not (NOT-ROW1-ROBOT))
)
)
(when
(and
(Flag32-)
)
(and
(ROW0-ROBOT)
(not (NOT-ROW0-ROBOT))
)
)
(when
(and
(ROW7-ROBOT)
)
(and
(ROW6-ROBOT)
(NOT-ROW7-ROBOT)
(not (NOT-ROW6-ROBOT))
(not (ROW7-ROBOT))
)
)
(when
(and
(ROW6-ROBOT)
)
(and
(ROW5-ROBOT)
(NOT-ROW6-ROBOT)
(not (NOT-ROW5-ROBOT))
(not (ROW6-ROBOT))
)
)
(when
(and
(ROW5-ROBOT)
)
(and
(ROW4-ROBOT)
(NOT-ROW5-ROBOT)
(not (NOT-ROW4-ROBOT))
(not (ROW5-ROBOT))
)
)
(when
(and
(ROW4-ROBOT)
)
(and
(ROW3-ROBOT)
(NOT-ROW4-ROBOT)
(not (NOT-ROW3-ROBOT))
(not (ROW4-ROBOT))
)
)
(when
(and
(ROW3-ROBOT)
)
(and
(ROW2-ROBOT)
(NOT-ROW3-ROBOT)
(not (NOT-ROW2-ROBOT))
(not (ROW3-ROBOT))
)
)
(when
(and
(ROW2-ROBOT)
)
(and
(ROW1-ROBOT)
(NOT-ROW2-ROBOT)
(not (NOT-ROW1-ROBOT))
(not (ROW2-ROBOT))
)
)
(when
(and
(ROW1-ROBOT)
)
(and
(ROW0-ROBOT)
(NOT-ROW1-ROBOT)
(not (NOT-ROW0-ROBOT))
(not (ROW1-ROBOT))
)
)
(when
(and
(ABOVEOF7-ROBOT)
)
(and
(ABOVEOF6-ROBOT)
(NOT-ABOVEOF7-ROBOT)
(ABOVEOF5-ROBOT)
(NOT-ABOVEOF6-ROBOT)
(ABOVEOF4-ROBOT)
(NOT-ABOVEOF5-ROBOT)
(ABOVEOF3-ROBOT)
(NOT-ABOVEOF4-ROBOT)
(ABOVEOF2-ROBOT)
(NOT-ABOVEOF3-ROBOT)
(ABOVEOF1-ROBOT)
(NOT-ABOVEOF2-ROBOT)
(NOT-ABOVEOF1-ROBOT)
(not (ABOVEOF7-ROBOT))
)
)
(when
(and
(ABOVEOF6-ROBOT)
)
(and
(ABOVEOF5-ROBOT)
(NOT-ABOVEOF6-ROBOT)
(ABOVEOF4-ROBOT)
(NOT-ABOVEOF5-ROBOT)
(ABOVEOF3-ROBOT)
(NOT-ABOVEOF4-ROBOT)
(ABOVEOF2-ROBOT)
(NOT-ABOVEOF3-ROBOT)
(ABOVEOF1-ROBOT)
(NOT-ABOVEOF2-ROBOT)
(NOT-ABOVEOF1-ROBOT)
(not (ABOVEOF6-ROBOT))
)
)
(when
(and
(ABOVEOF5-ROBOT)
)
(and
(ABOVEOF4-ROBOT)
(NOT-ABOVEOF5-ROBOT)
(ABOVEOF3-ROBOT)
(NOT-ABOVEOF4-ROBOT)
(ABOVEOF2-ROBOT)
(NOT-ABOVEOF3-ROBOT)
(ABOVEOF1-ROBOT)
(NOT-ABOVEOF2-ROBOT)
(NOT-ABOVEOF1-ROBOT)
(not (ABOVEOF5-ROBOT))
)
)
(when
(and
(ABOVEOF4-ROBOT)
)
(and
(ABOVEOF3-ROBOT)
(NOT-ABOVEOF4-ROBOT)
(ABOVEOF2-ROBOT)
(NOT-ABOVEOF3-ROBOT)
(ABOVEOF1-ROBOT)
(NOT-ABOVEOF2-ROBOT)
(NOT-ABOVEOF1-ROBOT)
(not (ABOVEOF4-ROBOT))
)
)
(when
(and
(ABOVEOF3-ROBOT)
)
(and
(ABOVEOF2-ROBOT)
(NOT-ABOVEOF3-ROBOT)
(ABOVEOF1-ROBOT)
(NOT-ABOVEOF2-ROBOT)
(NOT-ABOVEOF1-ROBOT)
(not (ABOVEOF3-ROBOT))
)
)
(when
(and
(ABOVEOF2-ROBOT)
)
(and
(ABOVEOF1-ROBOT)
(NOT-ABOVEOF2-ROBOT)
(NOT-ABOVEOF1-ROBOT)
(not (ABOVEOF2-ROBOT))
)
)
(when
(and
(ABOVEOF1-ROBOT)
)
(and
(NOT-ABOVEOF1-ROBOT)
(not (ABOVEOF1-ROBOT))
)
)
(when
(and
(Flag62-)
)
(and
(BELOWOF7-ROBOT)
(not (NOT-BELOWOF7-ROBOT))
)
)
(when
(and
(BELOWOF3-ROBOT)
)
(and
(BELOWOF6-ROBOT)
(BELOWOF5-ROBOT)
(BELOWOF4-ROBOT)
(BELOWOF2-ROBOT)
(not (NOT-BELOWOF6-ROBOT))
(not (NOT-BELOWOF5-ROBOT))
(not (NOT-BELOWOF4-ROBOT))
(not (NOT-BELOWOF3-ROBOT))
(not (NOT-BELOWOF2-ROBOT))
)
)
(when
(and
(BELOWOF7-ROBOT)
)
(and
(BELOWOF6-ROBOT)
(not (NOT-BELOWOF6-ROBOT))
)
)
(when
(and
(BELOWOF4-ROBOT)
)
(and
(BELOWOF6-ROBOT)
(BELOWOF5-ROBOT)
(BELOWOF3-ROBOT)
(not (NOT-BELOWOF6-ROBOT))
(not (NOT-BELOWOF5-ROBOT))
(not (NOT-BELOWOF4-ROBOT))
(not (NOT-BELOWOF3-ROBOT))
)
)
(when
(and
(BELOWOF1-ROBOT)
)
(and
(BELOWOF6-ROBOT)
(BELOWOF5-ROBOT)
(BELOWOF4-ROBOT)
(BELOWOF3-ROBOT)
(BELOWOF2-ROBOT)
(not (NOT-BELOWOF6-ROBOT))
(not (NOT-BELOWOF5-ROBOT))
(not (NOT-BELOWOF4-ROBOT))
(not (NOT-BELOWOF3-ROBOT))
(not (NOT-BELOWOF2-ROBOT))
(not (NOT-BELOWOF1-ROBOT))
)
)
(when
(and
(BELOWOF2-ROBOT)
)
(and
(BELOWOF6-ROBOT)
(BELOWOF5-ROBOT)
(BELOWOF4-ROBOT)
(BELOWOF3-ROBOT)
(BELOWOF1-ROBOT)
(not (NOT-BELOWOF6-ROBOT))
(not (NOT-BELOWOF5-ROBOT))
(not (NOT-BELOWOF4-ROBOT))
(not (NOT-BELOWOF3-ROBOT))
(not (NOT-BELOWOF2-ROBOT))
(not (NOT-BELOWOF1-ROBOT))
)
)
(when
(and
(BELOWOF5-ROBOT)
)
(and
(BELOWOF6-ROBOT)
(BELOWOF4-ROBOT)
(not (NOT-BELOWOF6-ROBOT))
(not (NOT-BELOWOF5-ROBOT))
(not (NOT-BELOWOF4-ROBOT))
)
)
(when
(and
(BELOWOF6-ROBOT)
)
(and
(BELOWOF5-ROBOT)
(not (NOT-BELOWOF6-ROBOT))
(not (NOT-BELOWOF5-ROBOT))
)
)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action MOVEUP-ROBOT
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
)
:effect
(and
(when
(and
(Flag61-)
)
(and
(ROW7-ROBOT)
(not (NOT-ROW7-ROBOT))
)
)
(when
(and
(Flag57-)
)
(and
(ROW6-ROBOT)
(not (NOT-ROW6-ROBOT))
)
)
(when
(and
(Flag52-)
)
(and
(ROW5-ROBOT)
(not (NOT-ROW5-ROBOT))
)
)
(when
(and
(Flag47-)
)
(and
(ROW4-ROBOT)
(not (NOT-ROW4-ROBOT))
)
)
(when
(and
(Flag40-)
)
(and
(ROW3-ROBOT)
(not (NOT-ROW3-ROBOT))
)
)
(when
(and
(Flag32-)
)
(and
(ROW2-ROBOT)
(not (NOT-ROW2-ROBOT))
)
)
(when
(and
(Flag24-)
)
(and
(ROW1-ROBOT)
(not (NOT-ROW1-ROBOT))
)
)
(when
(and
(ROW6-ROBOT)
)
(and
(ROW7-ROBOT)
(NOT-ROW6-ROBOT)
(not (NOT-ROW7-ROBOT))
(not (ROW6-ROBOT))
)
)
(when
(and
(ROW5-ROBOT)
)
(and
(ROW6-ROBOT)
(NOT-ROW5-ROBOT)
(not (NOT-ROW6-ROBOT))
(not (ROW5-ROBOT))
)
)
(when
(and
(ROW4-ROBOT)
)
(and
(ROW5-ROBOT)
(NOT-ROW4-ROBOT)
(not (NOT-ROW5-ROBOT))
(not (ROW4-ROBOT))
)
)
(when
(and
(ROW3-ROBOT)
)
(and
(ROW4-ROBOT)
(NOT-ROW3-ROBOT)
(not (NOT-ROW4-ROBOT))
(not (ROW3-ROBOT))
)
)
(when
(and
(ROW2-ROBOT)
)
(and
(ROW3-ROBOT)
(NOT-ROW2-ROBOT)
(not (NOT-ROW3-ROBOT))
(not (ROW2-ROBOT))
)
)
(when
(and
(ROW1-ROBOT)
)
(and
(ROW2-ROBOT)
(NOT-ROW1-ROBOT)
(not (NOT-ROW2-ROBOT))
(not (ROW1-ROBOT))
)
)
(when
(and
(ROW0-ROBOT)
)
(and
(ROW1-ROBOT)
(NOT-ROW0-ROBOT)
(not (NOT-ROW1-ROBOT))
(not (ROW0-ROBOT))
)
)
(when
(and
(Flag16-)
)
(and
(BELOWOF8-ROBOT)
(NOT-BELOWOF7-ROBOT)
(not (NOT-BELOWOF8-ROBOT))
(not (BELOWOF7-ROBOT))
)
)
(when
(and
(Flag15-)
)
(and
(BELOWOF7-ROBOT)
(NOT-BELOWOF6-ROBOT)
(not (NOT-BELOWOF7-ROBOT))
(not (BELOWOF6-ROBOT))
)
)
(when
(and
(Flag14-)
)
(and
(BELOWOF6-ROBOT)
(NOT-BELOWOF5-ROBOT)
(not (NOT-BELOWOF6-ROBOT))
(not (BELOWOF5-ROBOT))
)
)
(when
(and
(Flag13-)
)
(and
(BELOWOF5-ROBOT)
(NOT-BELOWOF4-ROBOT)
(not (NOT-BELOWOF5-ROBOT))
(not (BELOWOF4-ROBOT))
)
)
(when
(and
(Flag12-)
)
(and
(BELOWOF4-ROBOT)
(NOT-BELOWOF3-ROBOT)
(not (NOT-BELOWOF4-ROBOT))
(not (BELOWOF3-ROBOT))
)
)
(when
(and
(Flag11-)
)
(and
(BELOWOF3-ROBOT)
(NOT-BELOWOF2-ROBOT)
(not (NOT-BELOWOF3-ROBOT))
(not (BELOWOF2-ROBOT))
)
)
(when
(and
(BELOWOF1-ROBOT)
)
(and
(BELOWOF2-ROBOT)
(NOT-BELOWOF1-ROBOT)
(not (NOT-BELOWOF2-ROBOT))
(not (BELOWOF1-ROBOT))
)
)
(when
(and
(Flag10-)
)
(and
(ABOVEOF7-ROBOT)
(not (NOT-ABOVEOF7-ROBOT))
)
)
(when
(and
(Flag9-)
)
(and
(ABOVEOF6-ROBOT)
(not (NOT-ABOVEOF6-ROBOT))
)
)
(when
(and
(Flag8-)
)
(and
(ABOVEOF5-ROBOT)
(not (NOT-ABOVEOF5-ROBOT))
)
)
(when
(and
(Flag7-)
)
(and
(ABOVEOF4-ROBOT)
(not (NOT-ABOVEOF4-ROBOT))
)
)
(when
(and
(Flag6-)
)
(and
(ABOVEOF3-ROBOT)
(not (NOT-ABOVEOF3-ROBOT))
)
)
(when
(and
(Flag5-)
)
(and
(ABOVEOF2-ROBOT)
(not (NOT-ABOVEOF2-ROBOT))
)
)
(ABOVEOF1-ROBOT)
(CHECKCONSISTENCY-)
(not (NOT-ABOVEOF1-ROBOT))
(not (NOT-CHECKCONSISTENCY-))
)
)
(:derived (Flag2-)(and (LEFTOF7-ROBOT)))

(:derived (Flag2-)(and (RIGHTOF2-ROBOT)))

(:derived (Flag4-)(and (NOT-CHECKCONSISTENCY-)(NOT-ERROR-)))

(:derived (Flag16-)(and (BELOWOF7-ROBOT)))

(:derived (Flag62-)(and (BELOWOF7-ROBOT)))

(:derived (Flag66-)(and (RIGHTOF2-ROBOT)))

(:derived (Flag67-)(and (RIGHTOF2-ROBOT)))

(:derived (Flag68-)(and (RIGHTOF2-ROBOT)))

(:derived (Flag78-)(and (LEFTOF7-ROBOT)))

(:derived (Flag124-)(and (LEFTOF7-ROBOT)))

)
