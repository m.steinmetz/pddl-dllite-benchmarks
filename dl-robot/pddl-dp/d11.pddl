(define (domain grounded-SIMPLE-ADL-ROBOT)
(:requirements
:strips
)
(:predicates
(Flag208-)
(Flag118-)
(Flag117-)
(Flag116-)
(Flag115-)
(Flag14-)
(Flag13-)
(Flag4-)
(Flag2-)
(ROW9-ROBOT)
(ROW8-ROBOT)
(ROW7-ROBOT)
(ROW6-ROBOT)
(ROW5-ROBOT)
(ROW4-ROBOT)
(ROW3-ROBOT)
(ROW2-ROBOT)
(ROW1-ROBOT)
(ROW0-ROBOT)
(ABOVEOF9-ROBOT)
(ABOVEOF8-ROBOT)
(ABOVEOF7-ROBOT)
(ABOVEOF6-ROBOT)
(ABOVEOF5-ROBOT)
(ABOVEOF4-ROBOT)
(ABOVEOF3-ROBOT)
(ABOVEOF2-ROBOT)
(ABOVEOF1-ROBOT)
(BELOWOF9-ROBOT)
(BELOWOF8-ROBOT)
(BELOWOF7-ROBOT)
(BELOWOF6-ROBOT)
(BELOWOF5-ROBOT)
(BELOWOF4-ROBOT)
(BELOWOF3-ROBOT)
(BELOWOF2-ROBOT)
(BELOWOF1-ROBOT)
(CHECKCONSISTENCY-)
(ROW10-ROBOT)
(BELOWOF11-ROBOT)
(NOT-BELOWOF10-ROBOT)
(ABOVEOF10-ROBOT)
(Flag207-)
(Flag105-)
(Flag103-)
(Flag102-)
(Flag101-)
(Flag99-)
(Flag98-)
(Flag97-)
(Flag96-)
(Flag94-)
(Flag93-)
(Flag92-)
(Flag91-)
(Flag90-)
(Flag88-)
(Flag87-)
(Flag86-)
(Flag85-)
(Flag84-)
(Flag83-)
(Flag82-)
(Flag80-)
(Flag79-)
(Flag78-)
(Flag77-)
(Flag76-)
(Flag75-)
(Flag74-)
(Flag72-)
(Flag71-)
(Flag70-)
(Flag69-)
(Flag68-)
(Flag67-)
(Flag66-)
(Flag64-)
(Flag63-)
(Flag62-)
(Flag61-)
(Flag60-)
(Flag59-)
(Flag58-)
(Flag57-)
(Flag56-)
(Flag54-)
(Flag53-)
(Flag52-)
(Flag51-)
(Flag50-)
(Flag49-)
(Flag48-)
(Flag47-)
(Flag46-)
(Flag45-)
(Flag43-)
(Flag42-)
(Flag41-)
(Flag40-)
(Flag39-)
(Flag38-)
(Flag37-)
(Flag36-)
(Flag35-)
(Flag34-)
(Flag33-)
(Flag32-)
(Flag31-)
(Flag30-)
(Flag29-)
(Flag28-)
(Flag27-)
(Flag26-)
(Flag25-)
(Flag24-)
(Flag23-)
(Flag22-)
(Flag21-)
(Flag20-)
(Flag19-)
(Flag18-)
(Flag17-)
(Flag16-)
(Flag15-)
(Flag12-)
(Flag11-)
(Flag10-)
(Flag9-)
(Flag8-)
(Flag7-)
(Flag6-)
(Flag5-)
(Flag3-)
(ERROR-)
(COLUMN9-ROBOT)
(COLUMN8-ROBOT)
(COLUMN7-ROBOT)
(COLUMN6-ROBOT)
(COLUMN5-ROBOT)
(COLUMN4-ROBOT)
(COLUMN3-ROBOT)
(COLUMN2-ROBOT)
(COLUMN1-ROBOT)
(COLUMN0-ROBOT)
(RIGHTOF9-ROBOT)
(RIGHTOF8-ROBOT)
(RIGHTOF7-ROBOT)
(RIGHTOF6-ROBOT)
(RIGHTOF5-ROBOT)
(RIGHTOF4-ROBOT)
(RIGHTOF3-ROBOT)
(RIGHTOF1-ROBOT)
(NOT-RIGHTOF2-ROBOT)
(RIGHTOF0-ROBOT)
(LEFTOF9-ROBOT)
(LEFTOF8-ROBOT)
(LEFTOF7-ROBOT)
(LEFTOF6-ROBOT)
(LEFTOF5-ROBOT)
(LEFTOF4-ROBOT)
(LEFTOF3-ROBOT)
(LEFTOF2-ROBOT)
(LEFTOF1-ROBOT)
(COLUMN10-ROBOT)
(LEFTOF11-ROBOT)
(NOT-LEFTOF10-ROBOT)
(RIGHTOF10-ROBOT)
(Flag209-)
(Flag205-)
(Flag203-)
(Flag202-)
(Flag200-)
(Flag199-)
(Flag198-)
(Flag196-)
(Flag195-)
(Flag194-)
(Flag193-)
(Flag192-)
(Flag190-)
(Flag189-)
(Flag188-)
(Flag187-)
(Flag186-)
(Flag185-)
(Flag183-)
(Flag182-)
(Flag181-)
(Flag180-)
(Flag179-)
(Flag178-)
(Flag176-)
(Flag175-)
(Flag174-)
(Flag173-)
(Flag172-)
(Flag171-)
(Flag170-)
(Flag169-)
(Flag168-)
(Flag166-)
(Flag165-)
(Flag164-)
(Flag163-)
(Flag162-)
(Flag161-)
(Flag160-)
(Flag159-)
(Flag158-)
(Flag156-)
(Flag155-)
(Flag154-)
(Flag153-)
(Flag152-)
(Flag151-)
(Flag150-)
(Flag149-)
(Flag148-)
(Flag146-)
(Flag145-)
(Flag144-)
(Flag143-)
(Flag142-)
(Flag141-)
(Flag140-)
(Flag139-)
(Flag138-)
(Flag137-)
(Flag136-)
(Flag135-)
(Flag134-)
(Flag133-)
(Flag132-)
(Flag131-)
(Flag130-)
(Flag129-)
(Flag128-)
(Flag127-)
(Flag126-)
(Flag125-)
(Flag124-)
(Flag123-)
(Flag122-)
(Flag121-)
(Flag120-)
(Flag119-)
(Flag114-)
(Flag113-)
(Flag112-)
(Flag111-)
(Flag110-)
(Flag109-)
(Flag108-)
(Flag107-)
(Flag106-)
(Flag104-)
(Flag100-)
(Flag95-)
(Flag89-)
(Flag81-)
(Flag73-)
(Flag65-)
(Flag55-)
(Flag44-)
(Flag1-)
(Flag210-)
(Flag206-)
(Flag204-)
(Flag201-)
(Flag197-)
(Flag191-)
(Flag184-)
(Flag177-)
(Flag167-)
(Flag157-)
(Flag147-)
(NOT-COLUMN10-ROBOT)
(NOT-COLUMN9-ROBOT)
(NOT-COLUMN8-ROBOT)
(NOT-COLUMN7-ROBOT)
(NOT-COLUMN6-ROBOT)
(NOT-COLUMN5-ROBOT)
(NOT-COLUMN4-ROBOT)
(NOT-COLUMN3-ROBOT)
(NOT-COLUMN2-ROBOT)
(NOT-COLUMN1-ROBOT)
(NOT-LEFTOF11-ROBOT)
(LEFTOF10-ROBOT)
(NOT-LEFTOF9-ROBOT)
(NOT-LEFTOF8-ROBOT)
(NOT-LEFTOF7-ROBOT)
(NOT-LEFTOF6-ROBOT)
(NOT-LEFTOF5-ROBOT)
(NOT-LEFTOF4-ROBOT)
(NOT-LEFTOF3-ROBOT)
(NOT-LEFTOF2-ROBOT)
(NOT-RIGHTOF10-ROBOT)
(NOT-RIGHTOF9-ROBOT)
(NOT-RIGHTOF8-ROBOT)
(NOT-RIGHTOF7-ROBOT)
(NOT-RIGHTOF6-ROBOT)
(NOT-RIGHTOF5-ROBOT)
(NOT-RIGHTOF4-ROBOT)
(NOT-RIGHTOF3-ROBOT)
(NOT-RIGHTOF1-ROBOT)
(NOT-CHECKCONSISTENCY-)
(NOT-COLUMN0-ROBOT)
(RIGHTOF2-ROBOT)
(NOT-RIGHTOF0-ROBOT)
(NOT-LEFTOF1-ROBOT)
(NOT-ERROR-)
(NOT-ROW10-ROBOT)
(NOT-ROW9-ROBOT)
(NOT-ROW8-ROBOT)
(NOT-ROW7-ROBOT)
(NOT-ROW6-ROBOT)
(NOT-ROW5-ROBOT)
(NOT-ROW4-ROBOT)
(NOT-ROW3-ROBOT)
(NOT-ROW2-ROBOT)
(NOT-ROW1-ROBOT)
(NOT-BELOWOF11-ROBOT)
(BELOWOF10-ROBOT)
(NOT-BELOWOF9-ROBOT)
(NOT-BELOWOF8-ROBOT)
(NOT-BELOWOF7-ROBOT)
(NOT-BELOWOF6-ROBOT)
(NOT-BELOWOF5-ROBOT)
(NOT-BELOWOF4-ROBOT)
(NOT-BELOWOF3-ROBOT)
(NOT-BELOWOF2-ROBOT)
(NOT-ABOVEOF10-ROBOT)
(NOT-ABOVEOF9-ROBOT)
(NOT-ABOVEOF8-ROBOT)
(NOT-ABOVEOF7-ROBOT)
(NOT-ABOVEOF6-ROBOT)
(NOT-ABOVEOF5-ROBOT)
(NOT-ABOVEOF4-ROBOT)
(NOT-ABOVEOF3-ROBOT)
(NOT-ABOVEOF2-ROBOT)
(NOT-ABOVEOF1-ROBOT)
(NOT-ROW0-ROBOT)
(NOT-BELOWOF1-ROBOT)
)
(:derived (Flag147-)(and (Flag126-)))

(:derived (Flag147-)(and (Flag127-)))

(:derived (Flag147-)(and (Flag128-)))

(:derived (Flag147-)(and (Flag129-)))

(:derived (Flag147-)(and (Flag130-)))

(:derived (Flag147-)(and (Flag131-)))

(:derived (Flag147-)(and (Flag132-)))

(:derived (Flag147-)(and (Flag133-)))

(:derived (Flag147-)(and (Flag134-)))

(:derived (Flag147-)(and (Flag135-)))

(:derived (Flag147-)(and (Flag136-)))

(:derived (Flag147-)(and (Flag137-)))

(:derived (Flag147-)(and (Flag138-)))

(:derived (Flag147-)(and (Flag139-)))

(:derived (Flag147-)(and (Flag140-)))

(:derived (Flag147-)(and (Flag141-)))

(:derived (Flag147-)(and (Flag142-)))

(:derived (Flag147-)(and (Flag143-)))

(:derived (Flag147-)(and (Flag144-)))

(:derived (Flag147-)(and (Flag145-)))

(:derived (Flag147-)(and (Flag146-)))

(:derived (Flag157-)(and (Flag131-)))

(:derived (Flag157-)(and (Flag148-)))

(:derived (Flag157-)(and (Flag133-)))

(:derived (Flag157-)(and (Flag135-)))

(:derived (Flag157-)(and (Flag149-)))

(:derived (Flag157-)(and (Flag144-)))

(:derived (Flag157-)(and (Flag127-)))

(:derived (Flag157-)(and (Flag136-)))

(:derived (Flag157-)(and (Flag137-)))

(:derived (Flag157-)(and (Flag150-)))

(:derived (Flag157-)(and (Flag141-)))

(:derived (Flag157-)(and (Flag139-)))

(:derived (Flag157-)(and (Flag126-)))

(:derived (Flag157-)(and (Flag129-)))

(:derived (Flag157-)(and (Flag151-)))

(:derived (Flag157-)(and (Flag152-)))

(:derived (Flag157-)(and (Flag132-)))

(:derived (Flag157-)(and (Flag153-)))

(:derived (Flag157-)(and (Flag142-)))

(:derived (Flag157-)(and (Flag154-)))

(:derived (Flag157-)(and (Flag128-)))

(:derived (Flag157-)(and (Flag138-)))

(:derived (Flag157-)(and (Flag140-)))

(:derived (Flag157-)(and (Flag143-)))

(:derived (Flag157-)(and (Flag145-)))

(:derived (Flag157-)(and (Flag155-)))

(:derived (Flag157-)(and (Flag146-)))

(:derived (Flag157-)(and (Flag156-)))

(:derived (Flag167-)(and (Flag131-)))

(:derived (Flag167-)(and (Flag148-)))

(:derived (Flag167-)(and (Flag133-)))

(:derived (Flag167-)(and (Flag135-)))

(:derived (Flag167-)(and (Flag149-)))

(:derived (Flag167-)(and (Flag144-)))

(:derived (Flag167-)(and (Flag158-)))

(:derived (Flag167-)(and (Flag159-)))

(:derived (Flag167-)(and (Flag127-)))

(:derived (Flag167-)(and (Flag152-)))

(:derived (Flag167-)(and (Flag136-)))

(:derived (Flag167-)(and (Flag137-)))

(:derived (Flag167-)(and (Flag150-)))

(:derived (Flag167-)(and (Flag141-)))

(:derived (Flag167-)(and (Flag139-)))

(:derived (Flag167-)(and (Flag126-)))

(:derived (Flag167-)(and (Flag129-)))

(:derived (Flag167-)(and (Flag151-)))

(:derived (Flag167-)(and (Flag160-)))

(:derived (Flag167-)(and (Flag132-)))

(:derived (Flag167-)(and (Flag161-)))

(:derived (Flag167-)(and (Flag153-)))

(:derived (Flag167-)(and (Flag142-)))

(:derived (Flag167-)(and (Flag162-)))

(:derived (Flag167-)(and (Flag163-)))

(:derived (Flag167-)(and (Flag164-)))

(:derived (Flag167-)(and (Flag165-)))

(:derived (Flag167-)(and (Flag128-)))

(:derived (Flag167-)(and (Flag143-)))

(:derived (Flag167-)(and (Flag166-)))

(:derived (Flag167-)(and (Flag145-)))

(:derived (Flag167-)(and (Flag155-)))

(:derived (Flag167-)(and (Flag146-)))

(:derived (Flag177-)(and (Flag168-)))

(:derived (Flag177-)(and (Flag131-)))

(:derived (Flag177-)(and (Flag148-)))

(:derived (Flag177-)(and (Flag133-)))

(:derived (Flag177-)(and (Flag169-)))

(:derived (Flag177-)(and (Flag149-)))

(:derived (Flag177-)(and (Flag144-)))

(:derived (Flag177-)(and (Flag158-)))

(:derived (Flag177-)(and (Flag159-)))

(:derived (Flag177-)(and (Flag127-)))

(:derived (Flag177-)(and (Flag170-)))

(:derived (Flag177-)(and (Flag136-)))

(:derived (Flag177-)(and (Flag137-)))

(:derived (Flag177-)(and (Flag150-)))

(:derived (Flag177-)(and (Flag171-)))

(:derived (Flag177-)(and (Flag172-)))

(:derived (Flag177-)(and (Flag173-)))

(:derived (Flag177-)(and (Flag126-)))

(:derived (Flag177-)(and (Flag174-)))

(:derived (Flag177-)(and (Flag129-)))

(:derived (Flag177-)(and (Flag151-)))

(:derived (Flag177-)(and (Flag152-)))

(:derived (Flag177-)(and (Flag132-)))

(:derived (Flag177-)(and (Flag161-)))

(:derived (Flag177-)(and (Flag175-)))

(:derived (Flag177-)(and (Flag139-)))

(:derived (Flag177-)(and (Flag142-)))

(:derived (Flag177-)(and (Flag162-)))

(:derived (Flag177-)(and (Flag163-)))

(:derived (Flag177-)(and (Flag164-)))

(:derived (Flag177-)(and (Flag165-)))

(:derived (Flag177-)(and (Flag128-)))

(:derived (Flag177-)(and (Flag153-)))

(:derived (Flag177-)(and (Flag143-)))

(:derived (Flag177-)(and (Flag176-)))

(:derived (Flag177-)(and (Flag145-)))

(:derived (Flag177-)(and (Flag155-)))

(:derived (Flag177-)(and (Flag146-)))

(:derived (Flag184-)(and (Flag168-)))

(:derived (Flag184-)(and (Flag131-)))

(:derived (Flag184-)(and (Flag169-)))

(:derived (Flag184-)(and (Flag133-)))

(:derived (Flag184-)(and (Flag144-)))

(:derived (Flag184-)(and (Flag159-)))

(:derived (Flag184-)(and (Flag127-)))

(:derived (Flag184-)(and (Flag170-)))

(:derived (Flag184-)(and (Flag136-)))

(:derived (Flag184-)(and (Flag137-)))

(:derived (Flag184-)(and (Flag150-)))

(:derived (Flag184-)(and (Flag178-)))

(:derived (Flag184-)(and (Flag172-)))

(:derived (Flag184-)(and (Flag173-)))

(:derived (Flag184-)(and (Flag174-)))

(:derived (Flag184-)(and (Flag129-)))

(:derived (Flag184-)(and (Flag148-)))

(:derived (Flag184-)(and (Flag151-)))

(:derived (Flag184-)(and (Flag152-)))

(:derived (Flag184-)(and (Flag132-)))

(:derived (Flag184-)(and (Flag161-)))

(:derived (Flag184-)(and (Flag175-)))

(:derived (Flag184-)(and (Flag153-)))

(:derived (Flag184-)(and (Flag179-)))

(:derived (Flag184-)(and (Flag142-)))

(:derived (Flag184-)(and (Flag162-)))

(:derived (Flag184-)(and (Flag180-)))

(:derived (Flag184-)(and (Flag181-)))

(:derived (Flag184-)(and (Flag182-)))

(:derived (Flag184-)(and (Flag163-)))

(:derived (Flag184-)(and (Flag164-)))

(:derived (Flag184-)(and (Flag165-)))

(:derived (Flag184-)(and (Flag128-)))

(:derived (Flag184-)(and (Flag143-)))

(:derived (Flag184-)(and (Flag176-)))

(:derived (Flag184-)(and (Flag145-)))

(:derived (Flag184-)(and (Flag155-)))

(:derived (Flag184-)(and (Flag183-)))

(:derived (Flag191-)(and (Flag168-)))

(:derived (Flag191-)(and (Flag131-)))

(:derived (Flag191-)(and (Flag169-)))

(:derived (Flag191-)(and (Flag133-)))

(:derived (Flag191-)(and (Flag164-)))

(:derived (Flag191-)(and (Flag185-)))

(:derived (Flag191-)(and (Flag186-)))

(:derived (Flag191-)(and (Flag144-)))

(:derived (Flag191-)(and (Flag148-)))

(:derived (Flag191-)(and (Flag159-)))

(:derived (Flag191-)(and (Flag170-)))

(:derived (Flag191-)(and (Flag136-)))

(:derived (Flag191-)(and (Flag178-)))

(:derived (Flag191-)(and (Flag150-)))

(:derived (Flag191-)(and (Flag172-)))

(:derived (Flag191-)(and (Flag173-)))

(:derived (Flag191-)(and (Flag174-)))

(:derived (Flag191-)(and (Flag129-)))

(:derived (Flag191-)(and (Flag187-)))

(:derived (Flag191-)(and (Flag151-)))

(:derived (Flag191-)(and (Flag152-)))

(:derived (Flag191-)(and (Flag132-)))

(:derived (Flag191-)(and (Flag161-)))

(:derived (Flag191-)(and (Flag175-)))

(:derived (Flag191-)(and (Flag142-)))

(:derived (Flag191-)(and (Flag162-)))

(:derived (Flag191-)(and (Flag180-)))

(:derived (Flag191-)(and (Flag188-)))

(:derived (Flag191-)(and (Flag189-)))

(:derived (Flag191-)(and (Flag181-)))

(:derived (Flag191-)(and (Flag163-)))

(:derived (Flag191-)(and (Flag190-)))

(:derived (Flag191-)(and (Flag128-)))

(:derived (Flag191-)(and (Flag143-)))

(:derived (Flag191-)(and (Flag145-)))

(:derived (Flag191-)(and (Flag155-)))

(:derived (Flag191-)(and (Flag183-)))

(:derived (Flag197-)(and (Flag168-)))

(:derived (Flag197-)(and (Flag192-)))

(:derived (Flag197-)(and (Flag169-)))

(:derived (Flag197-)(and (Flag133-)))

(:derived (Flag197-)(and (Flag174-)))

(:derived (Flag197-)(and (Flag186-)))

(:derived (Flag197-)(and (Flag193-)))

(:derived (Flag197-)(and (Flag144-)))

(:derived (Flag197-)(and (Flag159-)))

(:derived (Flag197-)(and (Flag170-)))

(:derived (Flag197-)(and (Flag136-)))

(:derived (Flag197-)(and (Flag150-)))

(:derived (Flag197-)(and (Flag178-)))

(:derived (Flag197-)(and (Flag172-)))

(:derived (Flag197-)(and (Flag194-)))

(:derived (Flag197-)(and (Flag195-)))

(:derived (Flag197-)(and (Flag196-)))

(:derived (Flag197-)(and (Flag129-)))

(:derived (Flag197-)(and (Flag151-)))

(:derived (Flag197-)(and (Flag152-)))

(:derived (Flag197-)(and (Flag132-)))

(:derived (Flag197-)(and (Flag161-)))

(:derived (Flag197-)(and (Flag175-)))

(:derived (Flag197-)(and (Flag142-)))

(:derived (Flag197-)(and (Flag131-)))

(:derived (Flag197-)(and (Flag162-)))

(:derived (Flag197-)(and (Flag180-)))

(:derived (Flag197-)(and (Flag188-)))

(:derived (Flag197-)(and (Flag181-)))

(:derived (Flag197-)(and (Flag163-)))

(:derived (Flag197-)(and (Flag190-)))

(:derived (Flag197-)(and (Flag128-)))

(:derived (Flag197-)(and (Flag155-)))

(:derived (Flag197-)(and (Flag183-)))

(:derived (Flag201-)(and (Flag168-)))

(:derived (Flag201-)(and (Flag131-)))

(:derived (Flag201-)(and (Flag133-)))

(:derived (Flag201-)(and (Flag195-)))

(:derived (Flag201-)(and (Flag186-)))

(:derived (Flag201-)(and (Flag193-)))

(:derived (Flag201-)(and (Flag144-)))

(:derived (Flag201-)(and (Flag198-)))

(:derived (Flag201-)(and (Flag152-)))

(:derived (Flag201-)(and (Flag199-)))

(:derived (Flag201-)(and (Flag178-)))

(:derived (Flag201-)(and (Flag172-)))

(:derived (Flag201-)(and (Flag200-)))

(:derived (Flag201-)(and (Flag194-)))

(:derived (Flag201-)(and (Flag174-)))

(:derived (Flag201-)(and (Flag129-)))

(:derived (Flag201-)(and (Flag151-)))

(:derived (Flag201-)(and (Flag170-)))

(:derived (Flag201-)(and (Flag175-)))

(:derived (Flag201-)(and (Flag142-)))

(:derived (Flag201-)(and (Flag162-)))

(:derived (Flag201-)(and (Flag188-)))

(:derived (Flag201-)(and (Flag181-)))

(:derived (Flag201-)(and (Flag163-)))

(:derived (Flag201-)(and (Flag190-)))

(:derived (Flag201-)(and (Flag128-)))

(:derived (Flag201-)(and (Flag155-)))

(:derived (Flag201-)(and (Flag183-)))

(:derived (Flag204-)(and (Flag194-)))

(:derived (Flag204-)(and (Flag174-)))

(:derived (Flag204-)(and (Flag181-)))

(:derived (Flag204-)(and (Flag129-)))

(:derived (Flag204-)(and (Flag168-)))

(:derived (Flag204-)(and (Flag151-)))

(:derived (Flag204-)(and (Flag170-)))

(:derived (Flag204-)(and (Flag133-)))

(:derived (Flag204-)(and (Flag199-)))

(:derived (Flag204-)(and (Flag202-)))

(:derived (Flag204-)(and (Flag190-)))

(:derived (Flag204-)(and (Flag203-)))

(:derived (Flag204-)(and (Flag193-)))

(:derived (Flag204-)(and (Flag131-)))

(:derived (Flag204-)(and (Flag144-)))

(:derived (Flag204-)(and (Flag162-)))

(:derived (Flag204-)(and (Flag163-)))

(:derived (Flag204-)(and (Flag155-)))

(:derived (Flag204-)(and (Flag183-)))

(:derived (Flag204-)(and (Flag200-)))

(:derived (Flag204-)(and (Flag188-)))

(:derived (Flag206-)(and (Flag194-)))

(:derived (Flag206-)(and (Flag174-)))

(:derived (Flag206-)(and (Flag181-)))

(:derived (Flag206-)(and (Flag129-)))

(:derived (Flag206-)(and (Flag151-)))

(:derived (Flag206-)(and (Flag170-)))

(:derived (Flag206-)(and (Flag190-)))

(:derived (Flag206-)(and (Flag203-)))

(:derived (Flag206-)(and (Flag205-)))

(:derived (Flag206-)(and (Flag144-)))

(:derived (Flag206-)(and (Flag200-)))

(:derived (Flag210-)(and (Flag126-)))

(:derived (Flag210-)(and (Flag136-)))

(:derived (Flag210-)(and (Flag133-)))

(:derived (Flag210-)(and (Flag134-)))

(:derived (Flag210-)(and (Flag135-)))

(:derived (Flag210-)(and (Flag209-)))

(:derived (Flag210-)(and (Flag139-)))

(:derived (Flag210-)(and (Flag140-)))

(:derived (Flag210-)(and (Flag142-)))

(:derived (Flag210-)(and (Flag143-)))

(:derived (Flag210-)(and (Flag144-)))

(:derived (Flag1-)(and (COLUMN2-ROBOT)(ROW1-ROBOT)(NOT-ERROR-)(NOT-CHECKCONSISTENCY-)))

(:derived (Flag2-)(and (LEFTOF3-ROBOT)))

(:derived (Flag2-)(and (RIGHTOF7-ROBOT)))

(:derived (Flag2-)(and (RIGHTOF8-ROBOT)))

(:derived (Flag2-)(and (LEFTOF2-ROBOT)))

(:derived (Flag2-)(and (LEFTOF9-ROBOT)))

(:derived (Flag2-)(and (LEFTOF1-ROBOT)))

(:derived (Flag2-)(and (LEFTOF8-ROBOT)))

(:derived (Flag2-)(and (LEFTOF5-ROBOT)))

(:derived (Flag2-)(and (RIGHTOF1-ROBOT)))

(:derived (Flag2-)(and (RIGHTOF6-ROBOT)))

(:derived (Flag2-)(and (RIGHTOF9-ROBOT)))

(:derived (Flag2-)(and (LEFTOF6-ROBOT)))

(:derived (Flag2-)(and (RIGHTOF0-ROBOT)))

(:derived (Flag2-)(and (LEFTOF11-ROBOT)))

(:derived (Flag2-)(and (RIGHTOF5-ROBOT)))

(:derived (Flag2-)(and (LEFTOF4-ROBOT)))

(:derived (Flag2-)(and (RIGHTOF4-ROBOT)))

(:derived (Flag2-)(and (RIGHTOF10-ROBOT)))

(:derived (Flag2-)(and (LEFTOF7-ROBOT)))

(:derived (Flag2-)(and (RIGHTOF3-ROBOT)))

(:derived (Flag44-)(and (Flag24-)))

(:derived (Flag44-)(and (Flag25-)))

(:derived (Flag44-)(and (Flag26-)))

(:derived (Flag44-)(and (Flag27-)))

(:derived (Flag44-)(and (Flag28-)))

(:derived (Flag44-)(and (Flag29-)))

(:derived (Flag44-)(and (Flag30-)))

(:derived (Flag44-)(and (Flag31-)))

(:derived (Flag44-)(and (Flag32-)))

(:derived (Flag44-)(and (Flag33-)))

(:derived (Flag44-)(and (Flag34-)))

(:derived (Flag44-)(and (Flag35-)))

(:derived (Flag44-)(and (Flag36-)))

(:derived (Flag44-)(and (Flag37-)))

(:derived (Flag44-)(and (Flag38-)))

(:derived (Flag44-)(and (Flag39-)))

(:derived (Flag44-)(and (Flag40-)))

(:derived (Flag44-)(and (Flag41-)))

(:derived (Flag44-)(and (Flag42-)))

(:derived (Flag44-)(and (Flag43-)))

(:derived (Flag55-)(and (Flag45-)))

(:derived (Flag55-)(and (Flag46-)))

(:derived (Flag55-)(and (Flag24-)))

(:derived (Flag55-)(and (Flag47-)))

(:derived (Flag55-)(and (Flag39-)))

(:derived (Flag55-)(and (Flag30-)))

(:derived (Flag55-)(and (Flag28-)))

(:derived (Flag55-)(and (Flag48-)))

(:derived (Flag55-)(and (Flag34-)))

(:derived (Flag55-)(and (Flag49-)))

(:derived (Flag55-)(and (Flag25-)))

(:derived (Flag55-)(and (Flag50-)))

(:derived (Flag55-)(and (Flag29-)))

(:derived (Flag55-)(and (Flag51-)))

(:derived (Flag55-)(and (Flag32-)))

(:derived (Flag55-)(and (Flag52-)))

(:derived (Flag55-)(and (Flag31-)))

(:derived (Flag55-)(and (Flag53-)))

(:derived (Flag55-)(and (Flag54-)))

(:derived (Flag55-)(and (Flag38-)))

(:derived (Flag55-)(and (Flag40-)))

(:derived (Flag55-)(and (Flag33-)))

(:derived (Flag55-)(and (Flag42-)))

(:derived (Flag55-)(and (Flag27-)))

(:derived (Flag55-)(and (Flag35-)))

(:derived (Flag55-)(and (Flag37-)))

(:derived (Flag55-)(and (Flag41-)))

(:derived (Flag65-)(and (Flag45-)))

(:derived (Flag65-)(and (Flag56-)))

(:derived (Flag65-)(and (Flag57-)))

(:derived (Flag65-)(and (Flag46-)))

(:derived (Flag65-)(and (Flag47-)))

(:derived (Flag65-)(and (Flag39-)))

(:derived (Flag65-)(and (Flag48-)))

(:derived (Flag65-)(and (Flag30-)))

(:derived (Flag65-)(and (Flag58-)))

(:derived (Flag65-)(and (Flag28-)))

(:derived (Flag65-)(and (Flag59-)))

(:derived (Flag65-)(and (Flag60-)))

(:derived (Flag65-)(and (Flag61-)))

(:derived (Flag65-)(and (Flag34-)))

(:derived (Flag65-)(and (Flag62-)))

(:derived (Flag65-)(and (Flag63-)))

(:derived (Flag65-)(and (Flag25-)))

(:derived (Flag65-)(and (Flag29-)))

(:derived (Flag65-)(and (Flag51-)))

(:derived (Flag65-)(and (Flag32-)))

(:derived (Flag65-)(and (Flag52-)))

(:derived (Flag65-)(and (Flag31-)))

(:derived (Flag65-)(and (Flag53-)))

(:derived (Flag65-)(and (Flag64-)))

(:derived (Flag65-)(and (Flag38-)))

(:derived (Flag65-)(and (Flag40-)))

(:derived (Flag65-)(and (Flag33-)))

(:derived (Flag65-)(and (Flag42-)))

(:derived (Flag65-)(and (Flag27-)))

(:derived (Flag65-)(and (Flag35-)))

(:derived (Flag65-)(and (Flag37-)))

(:derived (Flag65-)(and (Flag41-)))

(:derived (Flag73-)(and (Flag66-)))

(:derived (Flag73-)(and (Flag45-)))

(:derived (Flag73-)(and (Flag46-)))

(:derived (Flag73-)(and (Flag47-)))

(:derived (Flag73-)(and (Flag39-)))

(:derived (Flag73-)(and (Flag48-)))

(:derived (Flag73-)(and (Flag67-)))

(:derived (Flag73-)(and (Flag30-)))

(:derived (Flag73-)(and (Flag35-)))

(:derived (Flag73-)(and (Flag58-)))

(:derived (Flag73-)(and (Flag28-)))

(:derived (Flag73-)(and (Flag59-)))

(:derived (Flag73-)(and (Flag60-)))

(:derived (Flag73-)(and (Flag68-)))

(:derived (Flag73-)(and (Flag61-)))

(:derived (Flag73-)(and (Flag34-)))

(:derived (Flag73-)(and (Flag62-)))

(:derived (Flag73-)(and (Flag25-)))

(:derived (Flag73-)(and (Flag29-)))

(:derived (Flag73-)(and (Flag51-)))

(:derived (Flag73-)(and (Flag32-)))

(:derived (Flag73-)(and (Flag69-)))

(:derived (Flag73-)(and (Flag52-)))

(:derived (Flag73-)(and (Flag70-)))

(:derived (Flag73-)(and (Flag31-)))

(:derived (Flag73-)(and (Flag53-)))

(:derived (Flag73-)(and (Flag64-)))

(:derived (Flag73-)(and (Flag57-)))

(:derived (Flag73-)(and (Flag40-)))

(:derived (Flag73-)(and (Flag33-)))

(:derived (Flag73-)(and (Flag42-)))

(:derived (Flag73-)(and (Flag27-)))

(:derived (Flag73-)(and (Flag71-)))

(:derived (Flag73-)(and (Flag72-)))

(:derived (Flag73-)(and (Flag37-)))

(:derived (Flag81-)(and (Flag45-)))

(:derived (Flag81-)(and (Flag46-)))

(:derived (Flag81-)(and (Flag47-)))

(:derived (Flag81-)(and (Flag39-)))

(:derived (Flag81-)(and (Flag74-)))

(:derived (Flag81-)(and (Flag67-)))

(:derived (Flag81-)(and (Flag40-)))

(:derived (Flag81-)(and (Flag29-)))

(:derived (Flag81-)(and (Flag30-)))

(:derived (Flag81-)(and (Flag35-)))

(:derived (Flag81-)(and (Flag58-)))

(:derived (Flag81-)(and (Flag28-)))

(:derived (Flag81-)(and (Flag75-)))

(:derived (Flag81-)(and (Flag76-)))

(:derived (Flag81-)(and (Flag48-)))

(:derived (Flag81-)(and (Flag60-)))

(:derived (Flag81-)(and (Flag68-)))

(:derived (Flag81-)(and (Flag61-)))

(:derived (Flag81-)(and (Flag62-)))

(:derived (Flag81-)(and (Flag25-)))

(:derived (Flag81-)(and (Flag32-)))

(:derived (Flag81-)(and (Flag51-)))

(:derived (Flag81-)(and (Flag77-)))

(:derived (Flag81-)(and (Flag70-)))

(:derived (Flag81-)(and (Flag31-)))

(:derived (Flag81-)(and (Flag53-)))

(:derived (Flag81-)(and (Flag64-)))

(:derived (Flag81-)(and (Flag78-)))

(:derived (Flag81-)(and (Flag57-)))

(:derived (Flag81-)(and (Flag79-)))

(:derived (Flag81-)(and (Flag33-)))

(:derived (Flag81-)(and (Flag42-)))

(:derived (Flag81-)(and (Flag71-)))

(:derived (Flag81-)(and (Flag80-)))

(:derived (Flag81-)(and (Flag72-)))

(:derived (Flag81-)(and (Flag37-)))

(:derived (Flag89-)(and (Flag58-)))

(:derived (Flag89-)(and (Flag82-)))

(:derived (Flag89-)(and (Flag46-)))

(:derived (Flag89-)(and (Flag47-)))

(:derived (Flag89-)(and (Flag83-)))

(:derived (Flag89-)(and (Flag84-)))

(:derived (Flag89-)(and (Flag39-)))

(:derived (Flag89-)(and (Flag67-)))

(:derived (Flag89-)(and (Flag40-)))

(:derived (Flag89-)(and (Flag30-)))

(:derived (Flag89-)(and (Flag85-)))

(:derived (Flag89-)(and (Flag75-)))

(:derived (Flag89-)(and (Flag48-)))

(:derived (Flag89-)(and (Flag60-)))

(:derived (Flag89-)(and (Flag68-)))

(:derived (Flag89-)(and (Flag61-)))

(:derived (Flag89-)(and (Flag86-)))

(:derived (Flag89-)(and (Flag25-)))

(:derived (Flag89-)(and (Flag29-)))

(:derived (Flag89-)(and (Flag51-)))

(:derived (Flag89-)(and (Flag32-)))

(:derived (Flag89-)(and (Flag70-)))

(:derived (Flag89-)(and (Flag31-)))

(:derived (Flag89-)(and (Flag53-)))

(:derived (Flag89-)(and (Flag64-)))

(:derived (Flag89-)(and (Flag78-)))

(:derived (Flag89-)(and (Flag57-)))

(:derived (Flag89-)(and (Flag79-)))

(:derived (Flag89-)(and (Flag33-)))

(:derived (Flag89-)(and (Flag42-)))

(:derived (Flag89-)(and (Flag71-)))

(:derived (Flag89-)(and (Flag87-)))

(:derived (Flag89-)(and (Flag88-)))

(:derived (Flag89-)(and (Flag80-)))

(:derived (Flag89-)(and (Flag72-)))

(:derived (Flag89-)(and (Flag37-)))

(:derived (Flag95-)(and (Flag58-)))

(:derived (Flag95-)(and (Flag47-)))

(:derived (Flag95-)(and (Flag83-)))

(:derived (Flag95-)(and (Flag39-)))

(:derived (Flag95-)(and (Flag67-)))

(:derived (Flag95-)(and (Flag40-)))

(:derived (Flag95-)(and (Flag30-)))

(:derived (Flag95-)(and (Flag85-)))

(:derived (Flag95-)(and (Flag75-)))

(:derived (Flag95-)(and (Flag90-)))

(:derived (Flag95-)(and (Flag48-)))

(:derived (Flag95-)(and (Flag60-)))

(:derived (Flag95-)(and (Flag68-)))

(:derived (Flag95-)(and (Flag61-)))

(:derived (Flag95-)(and (Flag86-)))

(:derived (Flag95-)(and (Flag91-)))

(:derived (Flag95-)(and (Flag29-)))

(:derived (Flag95-)(and (Flag51-)))

(:derived (Flag95-)(and (Flag70-)))

(:derived (Flag95-)(and (Flag92-)))

(:derived (Flag95-)(and (Flag31-)))

(:derived (Flag95-)(and (Flag53-)))

(:derived (Flag95-)(and (Flag64-)))

(:derived (Flag95-)(and (Flag78-)))

(:derived (Flag95-)(and (Flag79-)))

(:derived (Flag95-)(and (Flag33-)))

(:derived (Flag95-)(and (Flag42-)))

(:derived (Flag95-)(and (Flag93-)))

(:derived (Flag95-)(and (Flag71-)))

(:derived (Flag95-)(and (Flag88-)))

(:derived (Flag95-)(and (Flag80-)))

(:derived (Flag95-)(and (Flag94-)))

(:derived (Flag95-)(and (Flag37-)))

(:derived (Flag100-)(and (Flag58-)))

(:derived (Flag100-)(and (Flag96-)))

(:derived (Flag100-)(and (Flag83-)))

(:derived (Flag100-)(and (Flag97-)))

(:derived (Flag100-)(and (Flag67-)))

(:derived (Flag100-)(and (Flag40-)))

(:derived (Flag100-)(and (Flag85-)))

(:derived (Flag100-)(and (Flag90-)))

(:derived (Flag100-)(and (Flag60-)))

(:derived (Flag100-)(and (Flag48-)))

(:derived (Flag100-)(and (Flag68-)))

(:derived (Flag100-)(and (Flag86-)))

(:derived (Flag100-)(and (Flag29-)))

(:derived (Flag100-)(and (Flag51-)))

(:derived (Flag100-)(and (Flag92-)))

(:derived (Flag100-)(and (Flag31-)))

(:derived (Flag100-)(and (Flag53-)))

(:derived (Flag100-)(and (Flag64-)))

(:derived (Flag100-)(and (Flag78-)))

(:derived (Flag100-)(and (Flag79-)))

(:derived (Flag100-)(and (Flag33-)))

(:derived (Flag100-)(and (Flag42-)))

(:derived (Flag100-)(and (Flag71-)))

(:derived (Flag100-)(and (Flag98-)))

(:derived (Flag100-)(and (Flag88-)))

(:derived (Flag100-)(and (Flag80-)))

(:derived (Flag100-)(and (Flag99-)))

(:derived (Flag100-)(and (Flag37-)))

(:derived (Flag104-)(and (Flag71-)))

(:derived (Flag104-)(and (Flag101-)))

(:derived (Flag104-)(and (Flag85-)))

(:derived (Flag104-)(and (Flag29-)))

(:derived (Flag104-)(and (Flag86-)))

(:derived (Flag104-)(and (Flag58-)))

(:derived (Flag104-)(and (Flag33-)))

(:derived (Flag104-)(and (Flag80-)))

(:derived (Flag104-)(and (Flag90-)))

(:derived (Flag104-)(and (Flag40-)))

(:derived (Flag104-)(and (Flag92-)))

(:derived (Flag104-)(and (Flag31-)))

(:derived (Flag104-)(and (Flag48-)))

(:derived (Flag104-)(and (Flag68-)))

(:derived (Flag104-)(and (Flag96-)))

(:derived (Flag104-)(and (Flag64-)))

(:derived (Flag104-)(and (Flag102-)))

(:derived (Flag104-)(and (Flag83-)))

(:derived (Flag104-)(and (Flag79-)))

(:derived (Flag104-)(and (Flag53-)))

(:derived (Flag104-)(and (Flag103-)))

(:derived (Flag106-)(and (Flag86-)))

(:derived (Flag106-)(and (Flag101-)))

(:derived (Flag106-)(and (Flag85-)))

(:derived (Flag106-)(and (Flag33-)))

(:derived (Flag106-)(and (Flag90-)))

(:derived (Flag106-)(and (Flag31-)))

(:derived (Flag106-)(and (Flag68-)))

(:derived (Flag106-)(and (Flag96-)))

(:derived (Flag106-)(and (Flag64-)))

(:derived (Flag106-)(and (Flag105-)))

(:derived (Flag106-)(and (Flag53-)))

(:derived (Flag107-)(and (LEFTOF1-ROBOT)))

(:derived (Flag107-)(and (LEFTOF2-ROBOT)))

(:derived (Flag108-)(and (LEFTOF1-ROBOT)))

(:derived (Flag108-)(and (LEFTOF3-ROBOT)))

(:derived (Flag108-)(and (LEFTOF2-ROBOT)))

(:derived (Flag109-)(and (LEFTOF1-ROBOT)))

(:derived (Flag109-)(and (LEFTOF3-ROBOT)))

(:derived (Flag109-)(and (LEFTOF4-ROBOT)))

(:derived (Flag109-)(and (LEFTOF2-ROBOT)))

(:derived (Flag110-)(and (LEFTOF5-ROBOT)))

(:derived (Flag110-)(and (LEFTOF1-ROBOT)))

(:derived (Flag110-)(and (LEFTOF3-ROBOT)))

(:derived (Flag110-)(and (LEFTOF4-ROBOT)))

(:derived (Flag110-)(and (LEFTOF2-ROBOT)))

(:derived (Flag111-)(and (LEFTOF2-ROBOT)))

(:derived (Flag111-)(and (LEFTOF1-ROBOT)))

(:derived (Flag111-)(and (LEFTOF6-ROBOT)))

(:derived (Flag111-)(and (LEFTOF5-ROBOT)))

(:derived (Flag111-)(and (LEFTOF3-ROBOT)))

(:derived (Flag111-)(and (LEFTOF4-ROBOT)))

(:derived (Flag112-)(and (LEFTOF2-ROBOT)))

(:derived (Flag112-)(and (LEFTOF7-ROBOT)))

(:derived (Flag112-)(and (LEFTOF1-ROBOT)))

(:derived (Flag112-)(and (LEFTOF6-ROBOT)))

(:derived (Flag112-)(and (LEFTOF5-ROBOT)))

(:derived (Flag112-)(and (LEFTOF3-ROBOT)))

(:derived (Flag112-)(and (LEFTOF4-ROBOT)))

(:derived (Flag113-)(and (LEFTOF2-ROBOT)))

(:derived (Flag113-)(and (LEFTOF7-ROBOT)))

(:derived (Flag113-)(and (LEFTOF1-ROBOT)))

(:derived (Flag113-)(and (LEFTOF6-ROBOT)))

(:derived (Flag113-)(and (LEFTOF5-ROBOT)))

(:derived (Flag113-)(and (LEFTOF3-ROBOT)))

(:derived (Flag113-)(and (LEFTOF4-ROBOT)))

(:derived (Flag113-)(and (LEFTOF8-ROBOT)))

(:derived (Flag114-)(and (LEFTOF9-ROBOT)))

(:derived (Flag114-)(and (LEFTOF2-ROBOT)))

(:derived (Flag114-)(and (LEFTOF7-ROBOT)))

(:derived (Flag114-)(and (LEFTOF1-ROBOT)))

(:derived (Flag114-)(and (LEFTOF6-ROBOT)))

(:derived (Flag114-)(and (LEFTOF5-ROBOT)))

(:derived (Flag114-)(and (LEFTOF3-ROBOT)))

(:derived (Flag114-)(and (LEFTOF4-ROBOT)))

(:derived (Flag114-)(and (LEFTOF8-ROBOT)))

(:derived (Flag115-)(and (LEFTOF9-ROBOT)))

(:derived (Flag115-)(and (LEFTOF2-ROBOT)))

(:derived (Flag115-)(and (LEFTOF7-ROBOT)))

(:derived (Flag115-)(and (LEFTOF1-ROBOT)))

(:derived (Flag115-)(and (LEFTOF6-ROBOT)))

(:derived (Flag115-)(and (LEFTOF5-ROBOT)))

(:derived (Flag115-)(and (LEFTOF3-ROBOT)))

(:derived (Flag115-)(and (LEFTOF4-ROBOT)))

(:derived (Flag115-)(and (LEFTOF8-ROBOT)))

(:derived (Flag116-)(and (LEFTOF11-ROBOT)))

(:derived (Flag116-)(and (LEFTOF9-ROBOT)))

(:derived (Flag116-)(and (LEFTOF2-ROBOT)))

(:derived (Flag116-)(and (LEFTOF7-ROBOT)))

(:derived (Flag116-)(and (LEFTOF1-ROBOT)))

(:derived (Flag116-)(and (LEFTOF6-ROBOT)))

(:derived (Flag116-)(and (LEFTOF5-ROBOT)))

(:derived (Flag116-)(and (LEFTOF3-ROBOT)))

(:derived (Flag116-)(and (LEFTOF4-ROBOT)))

(:derived (Flag116-)(and (LEFTOF8-ROBOT)))

(:derived (Flag117-)(and (RIGHTOF3-ROBOT)))

(:derived (Flag117-)(and (RIGHTOF8-ROBOT)))

(:derived (Flag117-)(and (RIGHTOF5-ROBOT)))

(:derived (Flag117-)(and (RIGHTOF10-ROBOT)))

(:derived (Flag117-)(and (RIGHTOF4-ROBOT)))

(:derived (Flag117-)(and (RIGHTOF1-ROBOT)))

(:derived (Flag117-)(and (RIGHTOF7-ROBOT)))

(:derived (Flag117-)(and (RIGHTOF9-ROBOT)))

(:derived (Flag117-)(and (RIGHTOF6-ROBOT)))

(:derived (Flag118-)(and (RIGHTOF3-ROBOT)))

(:derived (Flag118-)(and (RIGHTOF5-ROBOT)))

(:derived (Flag118-)(and (RIGHTOF10-ROBOT)))

(:derived (Flag118-)(and (RIGHTOF4-ROBOT)))

(:derived (Flag118-)(and (RIGHTOF8-ROBOT)))

(:derived (Flag118-)(and (RIGHTOF7-ROBOT)))

(:derived (Flag118-)(and (RIGHTOF9-ROBOT)))

(:derived (Flag118-)(and (RIGHTOF6-ROBOT)))

(:derived (Flag119-)(and (RIGHTOF3-ROBOT)))

(:derived (Flag119-)(and (RIGHTOF5-ROBOT)))

(:derived (Flag119-)(and (RIGHTOF10-ROBOT)))

(:derived (Flag119-)(and (RIGHTOF4-ROBOT)))

(:derived (Flag119-)(and (RIGHTOF8-ROBOT)))

(:derived (Flag119-)(and (RIGHTOF7-ROBOT)))

(:derived (Flag119-)(and (RIGHTOF9-ROBOT)))

(:derived (Flag119-)(and (RIGHTOF6-ROBOT)))

(:derived (Flag120-)(and (RIGHTOF5-ROBOT)))

(:derived (Flag120-)(and (RIGHTOF10-ROBOT)))

(:derived (Flag120-)(and (RIGHTOF4-ROBOT)))

(:derived (Flag120-)(and (RIGHTOF8-ROBOT)))

(:derived (Flag120-)(and (RIGHTOF7-ROBOT)))

(:derived (Flag120-)(and (RIGHTOF9-ROBOT)))

(:derived (Flag120-)(and (RIGHTOF6-ROBOT)))

(:derived (Flag121-)(and (RIGHTOF5-ROBOT)))

(:derived (Flag121-)(and (RIGHTOF10-ROBOT)))

(:derived (Flag121-)(and (RIGHTOF8-ROBOT)))

(:derived (Flag121-)(and (RIGHTOF7-ROBOT)))

(:derived (Flag121-)(and (RIGHTOF9-ROBOT)))

(:derived (Flag121-)(and (RIGHTOF6-ROBOT)))

(:derived (Flag122-)(and (RIGHTOF10-ROBOT)))

(:derived (Flag122-)(and (RIGHTOF7-ROBOT)))

(:derived (Flag122-)(and (RIGHTOF9-ROBOT)))

(:derived (Flag122-)(and (RIGHTOF8-ROBOT)))

(:derived (Flag122-)(and (RIGHTOF6-ROBOT)))

(:derived (Flag123-)(and (RIGHTOF10-ROBOT)))

(:derived (Flag123-)(and (RIGHTOF7-ROBOT)))

(:derived (Flag123-)(and (RIGHTOF9-ROBOT)))

(:derived (Flag123-)(and (RIGHTOF8-ROBOT)))

(:derived (Flag124-)(and (RIGHTOF10-ROBOT)))

(:derived (Flag124-)(and (RIGHTOF9-ROBOT)))

(:derived (Flag124-)(and (RIGHTOF8-ROBOT)))

(:derived (Flag125-)(and (RIGHTOF10-ROBOT)))

(:derived (Flag125-)(and (RIGHTOF9-ROBOT)))

(:derived (Flag126-)(and (LEFTOF1-ROBOT)(RIGHTOF4-ROBOT)))

(:derived (Flag127-)(and (RIGHTOF5-ROBOT)(LEFTOF2-ROBOT)))

(:derived (Flag128-)(and (RIGHTOF8-ROBOT)(LEFTOF2-ROBOT)))

(:derived (Flag129-)(and (RIGHTOF10-ROBOT)(LEFTOF2-ROBOT)))

(:derived (Flag130-)(and (LEFTOF2-ROBOT)(RIGHTOF1-ROBOT)))

(:derived (Flag131-)(and (LEFTOF2-ROBOT)(RIGHTOF9-ROBOT)))

(:derived (Flag132-)(and (LEFTOF2-ROBOT)(RIGHTOF7-ROBOT)))

(:derived (Flag133-)(and (LEFTOF1-ROBOT)(RIGHTOF9-ROBOT)))

(:derived (Flag134-)(and (LEFTOF1-ROBOT)(RIGHTOF1-ROBOT)))

(:derived (Flag135-)(and (RIGHTOF3-ROBOT)(LEFTOF1-ROBOT)))

(:derived (Flag136-)(and (LEFTOF1-ROBOT)(RIGHTOF7-ROBOT)))

(:derived (Flag137-)(and (RIGHTOF5-ROBOT)(LEFTOF1-ROBOT)))

(:derived (Flag138-)(and (RIGHTOF2-ROBOT)(LEFTOF2-ROBOT)))

(:derived (Flag139-)(and (LEFTOF1-ROBOT)(RIGHTOF5-ROBOT)))

(:derived (Flag140-)(and (LEFTOF1-ROBOT)(RIGHTOF2-ROBOT)))

(:derived (Flag141-)(and (RIGHTOF3-ROBOT)(LEFTOF2-ROBOT)))

(:derived (Flag142-)(and (RIGHTOF8-ROBOT)(LEFTOF1-ROBOT)))

(:derived (Flag143-)(and (LEFTOF1-ROBOT)(RIGHTOF6-ROBOT)))

(:derived (Flag144-)(and (RIGHTOF10-ROBOT)(LEFTOF1-ROBOT)))

(:derived (Flag145-)(and (LEFTOF2-ROBOT)(RIGHTOF6-ROBOT)))

(:derived (Flag146-)(and (LEFTOF2-ROBOT)(RIGHTOF4-ROBOT)))

(:derived (Flag148-)(and (LEFTOF3-ROBOT)(RIGHTOF6-ROBOT)))

(:derived (Flag149-)(and (LEFTOF3-ROBOT)(RIGHTOF4-ROBOT)))

(:derived (Flag150-)(and (LEFTOF3-ROBOT)(RIGHTOF7-ROBOT)))

(:derived (Flag151-)(and (LEFTOF3-ROBOT)(RIGHTOF10-ROBOT)))

(:derived (Flag152-)(and (LEFTOF3-ROBOT)(RIGHTOF8-ROBOT)))

(:derived (Flag153-)(and (LEFTOF3-ROBOT)(RIGHTOF5-ROBOT)))

(:derived (Flag154-)(and (RIGHTOF3-ROBOT)(LEFTOF3-ROBOT)))

(:derived (Flag155-)(and (LEFTOF3-ROBOT)(RIGHTOF9-ROBOT)))

(:derived (Flag156-)(and (LEFTOF3-ROBOT)(RIGHTOF2-ROBOT)))

(:derived (Flag158-)(and (LEFTOF4-ROBOT)(RIGHTOF4-ROBOT)))

(:derived (Flag159-)(and (LEFTOF4-ROBOT)(RIGHTOF7-ROBOT)))

(:derived (Flag160-)(and (LEFTOF3-ROBOT)(RIGHTOF3-ROBOT)))

(:derived (Flag161-)(and (RIGHTOF8-ROBOT)(LEFTOF4-ROBOT)))

(:derived (Flag162-)(and (RIGHTOF10-ROBOT)(LEFTOF4-ROBOT)))

(:derived (Flag163-)(and (LEFTOF4-ROBOT)(RIGHTOF9-ROBOT)))

(:derived (Flag164-)(and (LEFTOF4-ROBOT)(RIGHTOF6-ROBOT)))

(:derived (Flag165-)(and (LEFTOF4-ROBOT)(RIGHTOF5-ROBOT)))

(:derived (Flag166-)(and (RIGHTOF3-ROBOT)(LEFTOF4-ROBOT)))

(:derived (Flag168-)(and (RIGHTOF9-ROBOT)(LEFTOF5-ROBOT)))

(:derived (Flag169-)(and (RIGHTOF7-ROBOT)(LEFTOF5-ROBOT)))

(:derived (Flag170-)(and (LEFTOF4-ROBOT)(RIGHTOF10-ROBOT)))

(:derived (Flag171-)(and (RIGHTOF4-ROBOT)(LEFTOF5-ROBOT)))

(:derived (Flag172-)(and (LEFTOF4-ROBOT)(RIGHTOF8-ROBOT)))

(:derived (Flag173-)(and (LEFTOF5-ROBOT)(RIGHTOF6-ROBOT)))

(:derived (Flag174-)(and (RIGHTOF10-ROBOT)(LEFTOF5-ROBOT)))

(:derived (Flag175-)(and (RIGHTOF8-ROBOT)(LEFTOF5-ROBOT)))

(:derived (Flag176-)(and (RIGHTOF5-ROBOT)(LEFTOF5-ROBOT)))

(:derived (Flag178-)(and (RIGHTOF8-ROBOT)(LEFTOF6-ROBOT)))

(:derived (Flag179-)(and (RIGHTOF5-ROBOT)(LEFTOF6-ROBOT)))

(:derived (Flag180-)(and (RIGHTOF7-ROBOT)(LEFTOF6-ROBOT)))

(:derived (Flag181-)(and (RIGHTOF10-ROBOT)(LEFTOF6-ROBOT)))

(:derived (Flag182-)(and (RIGHTOF6-ROBOT)(LEFTOF6-ROBOT)))

(:derived (Flag183-)(and (RIGHTOF9-ROBOT)(LEFTOF6-ROBOT)))

(:derived (Flag185-)(and (LEFTOF7-ROBOT)(RIGHTOF7-ROBOT)))

(:derived (Flag186-)(and (RIGHTOF8-ROBOT)(LEFTOF7-ROBOT)))

(:derived (Flag187-)(and (LEFTOF6-ROBOT)(RIGHTOF6-ROBOT)))

(:derived (Flag188-)(and (LEFTOF7-ROBOT)(RIGHTOF9-ROBOT)))

(:derived (Flag189-)(and (LEFTOF7-ROBOT)(RIGHTOF6-ROBOT)))

(:derived (Flag190-)(and (RIGHTOF10-ROBOT)(LEFTOF7-ROBOT)))

(:derived (Flag192-)(and (RIGHTOF7-ROBOT)(LEFTOF8-ROBOT)))

(:derived (Flag193-)(and (LEFTOF8-ROBOT)(RIGHTOF9-ROBOT)))

(:derived (Flag194-)(and (RIGHTOF10-ROBOT)(LEFTOF8-ROBOT)))

(:derived (Flag195-)(and (RIGHTOF8-ROBOT)(LEFTOF8-ROBOT)))

(:derived (Flag196-)(and (RIGHTOF7-ROBOT)(LEFTOF7-ROBOT)))

(:derived (Flag198-)(and (RIGHTOF8-ROBOT)(LEFTOF9-ROBOT)))

(:derived (Flag199-)(and (LEFTOF9-ROBOT)(RIGHTOF9-ROBOT)))

(:derived (Flag200-)(and (RIGHTOF10-ROBOT)(LEFTOF9-ROBOT)))

(:derived (Flag202-)(and (LEFTOF10-ROBOT)(RIGHTOF9-ROBOT)))

(:derived (Flag203-)(and (RIGHTOF10-ROBOT)(LEFTOF10-ROBOT)))

(:derived (Flag205-)(and (RIGHTOF10-ROBOT)(LEFTOF11-ROBOT)))

(:derived (Flag207-)(and (Flag27-)))

(:derived (Flag207-)(and (Flag24-)))

(:derived (Flag207-)(and (Flag32-)))

(:derived (Flag207-)(and (Flag30-)))

(:derived (Flag207-)(and (Flag36-)))

(:derived (Flag207-)(and (Flag28-)))

(:derived (Flag207-)(and (Flag31-)))

(:derived (Flag207-)(and (Flag37-)))

(:derived (Flag207-)(and (Flag38-)))

(:derived (Flag207-)(and (Flag40-)))

(:derived (Flag208-)(and (RIGHTOF3-ROBOT)))

(:derived (Flag208-)(and (RIGHTOF8-ROBOT)))

(:derived (Flag208-)(and (RIGHTOF10-ROBOT)))

(:derived (Flag208-)(and (RIGHTOF5-ROBOT)))

(:derived (Flag208-)(and (RIGHTOF0-ROBOT)))

(:derived (Flag208-)(and (RIGHTOF4-ROBOT)))

(:derived (Flag208-)(and (RIGHTOF1-ROBOT)))

(:derived (Flag208-)(and (RIGHTOF7-ROBOT)))

(:derived (Flag208-)(and (RIGHTOF9-ROBOT)))

(:derived (Flag208-)(and (RIGHTOF6-ROBOT)))

(:derived (Flag209-)(and (RIGHTOF0-ROBOT)(LEFTOF1-ROBOT)))

(:action MOVERIGHT-ROBOT
:parameters ()
:precondition
(and
(Flag3-)
)
:effect
(and
(when
(and
(Flag204-)
)
(and
(COLUMN10-ROBOT)
(not (NOT-COLUMN10-ROBOT))
)
)
(when
(and
(Flag201-)
)
(and
(COLUMN9-ROBOT)
(not (NOT-COLUMN9-ROBOT))
)
)
(when
(and
(Flag197-)
)
(and
(COLUMN8-ROBOT)
(not (NOT-COLUMN8-ROBOT))
)
)
(when
(and
(Flag191-)
)
(and
(COLUMN7-ROBOT)
(not (NOT-COLUMN7-ROBOT))
)
)
(when
(and
(Flag184-)
)
(and
(COLUMN6-ROBOT)
(not (NOT-COLUMN6-ROBOT))
)
)
(when
(and
(Flag177-)
)
(and
(COLUMN5-ROBOT)
(not (NOT-COLUMN5-ROBOT))
)
)
(when
(and
(Flag167-)
)
(and
(COLUMN4-ROBOT)
(not (NOT-COLUMN4-ROBOT))
)
)
(when
(and
(Flag157-)
)
(and
(COLUMN3-ROBOT)
(not (NOT-COLUMN3-ROBOT))
)
)
(when
(and
(Flag147-)
)
(and
(COLUMN2-ROBOT)
(not (NOT-COLUMN2-ROBOT))
)
)
(when
(and
(Flag210-)
)
(and
(COLUMN1-ROBOT)
(not (NOT-COLUMN1-ROBOT))
)
)
(when
(and
(COLUMN9-ROBOT)
)
(and
(COLUMN10-ROBOT)
(NOT-COLUMN9-ROBOT)
(not (NOT-COLUMN10-ROBOT))
(not (COLUMN9-ROBOT))
)
)
(when
(and
(COLUMN8-ROBOT)
)
(and
(COLUMN9-ROBOT)
(NOT-COLUMN8-ROBOT)
(not (NOT-COLUMN9-ROBOT))
(not (COLUMN8-ROBOT))
)
)
(when
(and
(COLUMN7-ROBOT)
)
(and
(COLUMN8-ROBOT)
(NOT-COLUMN7-ROBOT)
(not (NOT-COLUMN8-ROBOT))
(not (COLUMN7-ROBOT))
)
)
(when
(and
(COLUMN6-ROBOT)
)
(and
(COLUMN7-ROBOT)
(NOT-COLUMN6-ROBOT)
(not (NOT-COLUMN7-ROBOT))
(not (COLUMN6-ROBOT))
)
)
(when
(and
(COLUMN5-ROBOT)
)
(and
(COLUMN6-ROBOT)
(NOT-COLUMN5-ROBOT)
(not (NOT-COLUMN6-ROBOT))
(not (COLUMN5-ROBOT))
)
)
(when
(and
(COLUMN4-ROBOT)
)
(and
(COLUMN5-ROBOT)
(NOT-COLUMN4-ROBOT)
(not (NOT-COLUMN5-ROBOT))
(not (COLUMN4-ROBOT))
)
)
(when
(and
(COLUMN3-ROBOT)
)
(and
(COLUMN4-ROBOT)
(NOT-COLUMN3-ROBOT)
(not (NOT-COLUMN4-ROBOT))
(not (COLUMN3-ROBOT))
)
)
(when
(and
(COLUMN2-ROBOT)
)
(and
(COLUMN3-ROBOT)
(NOT-COLUMN2-ROBOT)
(not (NOT-COLUMN3-ROBOT))
(not (COLUMN2-ROBOT))
)
)
(when
(and
(COLUMN1-ROBOT)
)
(and
(COLUMN2-ROBOT)
(NOT-COLUMN1-ROBOT)
(not (NOT-COLUMN2-ROBOT))
(not (COLUMN1-ROBOT))
)
)
(when
(and
(COLUMN0-ROBOT)
)
(and
(COLUMN1-ROBOT)
(NOT-COLUMN0-ROBOT)
(not (NOT-COLUMN1-ROBOT))
(not (COLUMN0-ROBOT))
)
)
(when
(and
(LEFTOF8-ROBOT)
)
(and
(LEFTOF11-ROBOT)
(NOT-LEFTOF10-ROBOT)
(LEFTOF10-ROBOT)
(NOT-LEFTOF9-ROBOT)
(LEFTOF9-ROBOT)
(NOT-LEFTOF8-ROBOT)
(not (NOT-LEFTOF11-ROBOT))
(not (LEFTOF8-ROBOT))
)
)
(when
(and
(LEFTOF4-ROBOT)
)
(and
(LEFTOF11-ROBOT)
(NOT-LEFTOF10-ROBOT)
(LEFTOF10-ROBOT)
(NOT-LEFTOF9-ROBOT)
(LEFTOF9-ROBOT)
(NOT-LEFTOF8-ROBOT)
(LEFTOF8-ROBOT)
(NOT-LEFTOF7-ROBOT)
(LEFTOF7-ROBOT)
(NOT-LEFTOF6-ROBOT)
(LEFTOF6-ROBOT)
(NOT-LEFTOF5-ROBOT)
(LEFTOF5-ROBOT)
(NOT-LEFTOF4-ROBOT)
(not (NOT-LEFTOF11-ROBOT))
(not (LEFTOF4-ROBOT))
)
)
(when
(and
(LEFTOF3-ROBOT)
)
(and
(LEFTOF11-ROBOT)
(NOT-LEFTOF10-ROBOT)
(LEFTOF10-ROBOT)
(NOT-LEFTOF9-ROBOT)
(LEFTOF9-ROBOT)
(NOT-LEFTOF8-ROBOT)
(LEFTOF8-ROBOT)
(NOT-LEFTOF7-ROBOT)
(LEFTOF7-ROBOT)
(NOT-LEFTOF6-ROBOT)
(LEFTOF6-ROBOT)
(NOT-LEFTOF5-ROBOT)
(LEFTOF5-ROBOT)
(NOT-LEFTOF4-ROBOT)
(LEFTOF4-ROBOT)
(NOT-LEFTOF3-ROBOT)
(not (NOT-LEFTOF11-ROBOT))
(not (LEFTOF3-ROBOT))
)
)
(when
(and
(LEFTOF5-ROBOT)
)
(and
(LEFTOF11-ROBOT)
(NOT-LEFTOF10-ROBOT)
(LEFTOF10-ROBOT)
(NOT-LEFTOF9-ROBOT)
(LEFTOF9-ROBOT)
(NOT-LEFTOF8-ROBOT)
(LEFTOF8-ROBOT)
(NOT-LEFTOF7-ROBOT)
(LEFTOF7-ROBOT)
(NOT-LEFTOF6-ROBOT)
(LEFTOF6-ROBOT)
(NOT-LEFTOF5-ROBOT)
(not (NOT-LEFTOF11-ROBOT))
(not (LEFTOF5-ROBOT))
)
)
(when
(and
(LEFTOF6-ROBOT)
)
(and
(LEFTOF11-ROBOT)
(NOT-LEFTOF10-ROBOT)
(LEFTOF10-ROBOT)
(NOT-LEFTOF9-ROBOT)
(LEFTOF9-ROBOT)
(NOT-LEFTOF8-ROBOT)
(LEFTOF8-ROBOT)
(NOT-LEFTOF7-ROBOT)
(LEFTOF7-ROBOT)
(NOT-LEFTOF6-ROBOT)
(not (NOT-LEFTOF11-ROBOT))
(not (LEFTOF6-ROBOT))
)
)
(when
(and
(LEFTOF1-ROBOT)
)
(and
(LEFTOF11-ROBOT)
(NOT-LEFTOF10-ROBOT)
(LEFTOF10-ROBOT)
(NOT-LEFTOF9-ROBOT)
(LEFTOF9-ROBOT)
(NOT-LEFTOF8-ROBOT)
(LEFTOF8-ROBOT)
(NOT-LEFTOF7-ROBOT)
(LEFTOF7-ROBOT)
(NOT-LEFTOF6-ROBOT)
(LEFTOF6-ROBOT)
(NOT-LEFTOF5-ROBOT)
(LEFTOF5-ROBOT)
(NOT-LEFTOF4-ROBOT)
(LEFTOF4-ROBOT)
(NOT-LEFTOF3-ROBOT)
(LEFTOF3-ROBOT)
(NOT-LEFTOF2-ROBOT)
(LEFTOF2-ROBOT)
(NOT-LEFTOF1-ROBOT)
(not (NOT-LEFTOF11-ROBOT))
(not (LEFTOF1-ROBOT))
)
)
(when
(and
(LEFTOF7-ROBOT)
)
(and
(LEFTOF11-ROBOT)
(NOT-LEFTOF10-ROBOT)
(LEFTOF10-ROBOT)
(NOT-LEFTOF9-ROBOT)
(LEFTOF9-ROBOT)
(NOT-LEFTOF8-ROBOT)
(LEFTOF8-ROBOT)
(NOT-LEFTOF7-ROBOT)
(not (NOT-LEFTOF11-ROBOT))
(not (LEFTOF7-ROBOT))
)
)
(when
(and
(LEFTOF2-ROBOT)
)
(and
(LEFTOF11-ROBOT)
(NOT-LEFTOF10-ROBOT)
(LEFTOF10-ROBOT)
(NOT-LEFTOF9-ROBOT)
(LEFTOF9-ROBOT)
(NOT-LEFTOF8-ROBOT)
(LEFTOF8-ROBOT)
(NOT-LEFTOF7-ROBOT)
(LEFTOF7-ROBOT)
(NOT-LEFTOF6-ROBOT)
(LEFTOF6-ROBOT)
(NOT-LEFTOF5-ROBOT)
(LEFTOF5-ROBOT)
(NOT-LEFTOF4-ROBOT)
(LEFTOF4-ROBOT)
(NOT-LEFTOF3-ROBOT)
(LEFTOF3-ROBOT)
(NOT-LEFTOF2-ROBOT)
(not (NOT-LEFTOF11-ROBOT))
(not (LEFTOF2-ROBOT))
)
)
(when
(and
(LEFTOF9-ROBOT)
)
(and
(LEFTOF11-ROBOT)
(NOT-LEFTOF10-ROBOT)
(LEFTOF10-ROBOT)
(NOT-LEFTOF9-ROBOT)
(not (NOT-LEFTOF11-ROBOT))
(not (LEFTOF9-ROBOT))
)
)
(when
(and
(LEFTOF10-ROBOT)
)
(and
(LEFTOF11-ROBOT)
(NOT-LEFTOF10-ROBOT)
(not (NOT-LEFTOF11-ROBOT))
(not (LEFTOF10-ROBOT))
)
)
(when
(and
(RIGHTOF9-ROBOT)
)
(and
(RIGHTOF10-ROBOT)
(RIGHTOF8-ROBOT)
(RIGHTOF7-ROBOT)
(RIGHTOF6-ROBOT)
(RIGHTOF5-ROBOT)
(RIGHTOF4-ROBOT)
(RIGHTOF3-ROBOT)
(RIGHTOF2-ROBOT)
(not (NOT-RIGHTOF10-ROBOT))
(not (NOT-RIGHTOF9-ROBOT))
(not (NOT-RIGHTOF8-ROBOT))
(not (NOT-RIGHTOF7-ROBOT))
(not (NOT-RIGHTOF6-ROBOT))
(not (NOT-RIGHTOF5-ROBOT))
(not (NOT-RIGHTOF4-ROBOT))
(not (NOT-RIGHTOF3-ROBOT))
(not (NOT-RIGHTOF2-ROBOT))
)
)
(when
(and
(RIGHTOF10-ROBOT)
)
(and
(RIGHTOF9-ROBOT)
(RIGHTOF8-ROBOT)
(RIGHTOF7-ROBOT)
(RIGHTOF6-ROBOT)
(RIGHTOF5-ROBOT)
(RIGHTOF4-ROBOT)
(RIGHTOF3-ROBOT)
(RIGHTOF2-ROBOT)
(not (NOT-RIGHTOF10-ROBOT))
(not (NOT-RIGHTOF9-ROBOT))
(not (NOT-RIGHTOF8-ROBOT))
(not (NOT-RIGHTOF7-ROBOT))
(not (NOT-RIGHTOF6-ROBOT))
(not (NOT-RIGHTOF5-ROBOT))
(not (NOT-RIGHTOF4-ROBOT))
(not (NOT-RIGHTOF3-ROBOT))
(not (NOT-RIGHTOF2-ROBOT))
)
)
(when
(and
(RIGHTOF8-ROBOT)
)
(and
(RIGHTOF9-ROBOT)
(RIGHTOF7-ROBOT)
(RIGHTOF6-ROBOT)
(RIGHTOF5-ROBOT)
(RIGHTOF4-ROBOT)
(RIGHTOF3-ROBOT)
(RIGHTOF2-ROBOT)
(not (NOT-RIGHTOF9-ROBOT))
(not (NOT-RIGHTOF8-ROBOT))
(not (NOT-RIGHTOF7-ROBOT))
(not (NOT-RIGHTOF6-ROBOT))
(not (NOT-RIGHTOF5-ROBOT))
(not (NOT-RIGHTOF4-ROBOT))
(not (NOT-RIGHTOF3-ROBOT))
(not (NOT-RIGHTOF2-ROBOT))
)
)
(when
(and
(RIGHTOF7-ROBOT)
)
(and
(RIGHTOF8-ROBOT)
(RIGHTOF6-ROBOT)
(RIGHTOF5-ROBOT)
(RIGHTOF4-ROBOT)
(RIGHTOF3-ROBOT)
(RIGHTOF2-ROBOT)
(not (NOT-RIGHTOF8-ROBOT))
(not (NOT-RIGHTOF7-ROBOT))
(not (NOT-RIGHTOF6-ROBOT))
(not (NOT-RIGHTOF5-ROBOT))
(not (NOT-RIGHTOF4-ROBOT))
(not (NOT-RIGHTOF3-ROBOT))
(not (NOT-RIGHTOF2-ROBOT))
)
)
(when
(and
(RIGHTOF6-ROBOT)
)
(and
(RIGHTOF7-ROBOT)
(RIGHTOF5-ROBOT)
(RIGHTOF4-ROBOT)
(RIGHTOF3-ROBOT)
(RIGHTOF2-ROBOT)
(not (NOT-RIGHTOF7-ROBOT))
(not (NOT-RIGHTOF6-ROBOT))
(not (NOT-RIGHTOF5-ROBOT))
(not (NOT-RIGHTOF4-ROBOT))
(not (NOT-RIGHTOF3-ROBOT))
(not (NOT-RIGHTOF2-ROBOT))
)
)
(when
(and
(RIGHTOF5-ROBOT)
)
(and
(RIGHTOF6-ROBOT)
(RIGHTOF4-ROBOT)
(RIGHTOF3-ROBOT)
(RIGHTOF2-ROBOT)
(not (NOT-RIGHTOF6-ROBOT))
(not (NOT-RIGHTOF5-ROBOT))
(not (NOT-RIGHTOF4-ROBOT))
(not (NOT-RIGHTOF3-ROBOT))
(not (NOT-RIGHTOF2-ROBOT))
)
)
(when
(and
(RIGHTOF4-ROBOT)
)
(and
(RIGHTOF5-ROBOT)
(RIGHTOF3-ROBOT)
(RIGHTOF2-ROBOT)
(not (NOT-RIGHTOF5-ROBOT))
(not (NOT-RIGHTOF4-ROBOT))
(not (NOT-RIGHTOF3-ROBOT))
(not (NOT-RIGHTOF2-ROBOT))
)
)
(when
(and
(RIGHTOF3-ROBOT)
)
(and
(RIGHTOF4-ROBOT)
(RIGHTOF2-ROBOT)
(not (NOT-RIGHTOF4-ROBOT))
(not (NOT-RIGHTOF3-ROBOT))
(not (NOT-RIGHTOF2-ROBOT))
)
)
(when
(and
(RIGHTOF2-ROBOT)
)
(and
(RIGHTOF3-ROBOT)
(not (NOT-RIGHTOF3-ROBOT))
(not (NOT-RIGHTOF2-ROBOT))
)
)
(when
(and
(RIGHTOF1-ROBOT)
)
(and
(RIGHTOF2-ROBOT)
(not (NOT-RIGHTOF2-ROBOT))
)
)
(when
(and
(Flag208-)
)
(and
(RIGHTOF1-ROBOT)
(not (NOT-RIGHTOF1-ROBOT))
)
)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action MOVELEFT-ROBOT
:parameters ()
:precondition
(and
(Flag3-)
)
:effect
(and
(when
(and
(Flag206-)
)
(and
(COLUMN9-ROBOT)
(not (NOT-COLUMN9-ROBOT))
)
)
(when
(and
(Flag204-)
)
(and
(COLUMN8-ROBOT)
(not (NOT-COLUMN8-ROBOT))
)
)
(when
(and
(Flag201-)
)
(and
(COLUMN7-ROBOT)
(not (NOT-COLUMN7-ROBOT))
)
)
(when
(and
(Flag197-)
)
(and
(COLUMN6-ROBOT)
(not (NOT-COLUMN6-ROBOT))
)
)
(when
(and
(Flag191-)
)
(and
(COLUMN5-ROBOT)
(not (NOT-COLUMN5-ROBOT))
)
)
(when
(and
(Flag184-)
)
(and
(COLUMN4-ROBOT)
(not (NOT-COLUMN4-ROBOT))
)
)
(when
(and
(Flag177-)
)
(and
(COLUMN3-ROBOT)
(not (NOT-COLUMN3-ROBOT))
)
)
(when
(and
(Flag167-)
)
(and
(COLUMN2-ROBOT)
(not (NOT-COLUMN2-ROBOT))
)
)
(when
(and
(Flag157-)
)
(and
(COLUMN1-ROBOT)
(not (NOT-COLUMN1-ROBOT))
)
)
(when
(and
(Flag147-)
)
(and
(COLUMN0-ROBOT)
(not (NOT-COLUMN0-ROBOT))
)
)
(when
(and
(COLUMN10-ROBOT)
)
(and
(COLUMN9-ROBOT)
(NOT-COLUMN10-ROBOT)
(not (NOT-COLUMN9-ROBOT))
(not (COLUMN10-ROBOT))
)
)
(when
(and
(COLUMN9-ROBOT)
)
(and
(COLUMN8-ROBOT)
(NOT-COLUMN9-ROBOT)
(not (NOT-COLUMN8-ROBOT))
(not (COLUMN9-ROBOT))
)
)
(when
(and
(COLUMN8-ROBOT)
)
(and
(COLUMN7-ROBOT)
(NOT-COLUMN8-ROBOT)
(not (NOT-COLUMN7-ROBOT))
(not (COLUMN8-ROBOT))
)
)
(when
(and
(COLUMN7-ROBOT)
)
(and
(COLUMN6-ROBOT)
(NOT-COLUMN7-ROBOT)
(not (NOT-COLUMN6-ROBOT))
(not (COLUMN7-ROBOT))
)
)
(when
(and
(COLUMN6-ROBOT)
)
(and
(COLUMN5-ROBOT)
(NOT-COLUMN6-ROBOT)
(not (NOT-COLUMN5-ROBOT))
(not (COLUMN6-ROBOT))
)
)
(when
(and
(COLUMN5-ROBOT)
)
(and
(COLUMN4-ROBOT)
(NOT-COLUMN5-ROBOT)
(not (NOT-COLUMN4-ROBOT))
(not (COLUMN5-ROBOT))
)
)
(when
(and
(COLUMN4-ROBOT)
)
(and
(COLUMN3-ROBOT)
(NOT-COLUMN4-ROBOT)
(not (NOT-COLUMN3-ROBOT))
(not (COLUMN4-ROBOT))
)
)
(when
(and
(COLUMN3-ROBOT)
)
(and
(COLUMN2-ROBOT)
(NOT-COLUMN3-ROBOT)
(not (NOT-COLUMN2-ROBOT))
(not (COLUMN3-ROBOT))
)
)
(when
(and
(COLUMN2-ROBOT)
)
(and
(COLUMN1-ROBOT)
(NOT-COLUMN2-ROBOT)
(not (NOT-COLUMN1-ROBOT))
(not (COLUMN2-ROBOT))
)
)
(when
(and
(COLUMN1-ROBOT)
)
(and
(COLUMN0-ROBOT)
(NOT-COLUMN1-ROBOT)
(not (NOT-COLUMN0-ROBOT))
(not (COLUMN1-ROBOT))
)
)
(when
(and
(RIGHTOF10-ROBOT)
)
(and
(RIGHTOF9-ROBOT)
(NOT-RIGHTOF10-ROBOT)
(not (NOT-RIGHTOF9-ROBOT))
(not (RIGHTOF10-ROBOT))
)
)
(when
(and
(Flag125-)
)
(and
(RIGHTOF8-ROBOT)
(NOT-RIGHTOF9-ROBOT)
(not (NOT-RIGHTOF8-ROBOT))
(not (RIGHTOF9-ROBOT))
)
)
(when
(and
(Flag124-)
)
(and
(RIGHTOF7-ROBOT)
(NOT-RIGHTOF8-ROBOT)
(not (NOT-RIGHTOF7-ROBOT))
(not (RIGHTOF8-ROBOT))
)
)
(when
(and
(Flag123-)
)
(and
(RIGHTOF6-ROBOT)
(NOT-RIGHTOF7-ROBOT)
(not (NOT-RIGHTOF6-ROBOT))
(not (RIGHTOF7-ROBOT))
)
)
(when
(and
(Flag122-)
)
(and
(RIGHTOF5-ROBOT)
(NOT-RIGHTOF6-ROBOT)
(not (NOT-RIGHTOF5-ROBOT))
(not (RIGHTOF6-ROBOT))
)
)
(when
(and
(Flag121-)
)
(and
(RIGHTOF4-ROBOT)
(NOT-RIGHTOF5-ROBOT)
(not (NOT-RIGHTOF4-ROBOT))
(not (RIGHTOF5-ROBOT))
)
)
(when
(and
(Flag120-)
)
(and
(RIGHTOF3-ROBOT)
(NOT-RIGHTOF4-ROBOT)
(not (NOT-RIGHTOF3-ROBOT))
(not (RIGHTOF4-ROBOT))
)
)
(when
(and
(Flag119-)
)
(and
(RIGHTOF2-ROBOT)
(NOT-RIGHTOF3-ROBOT)
(not (NOT-RIGHTOF2-ROBOT))
(not (RIGHTOF3-ROBOT))
)
)
(when
(and
(Flag118-)
)
(and
(RIGHTOF1-ROBOT)
(NOT-RIGHTOF2-ROBOT)
(not (NOT-RIGHTOF1-ROBOT))
(not (RIGHTOF2-ROBOT))
)
)
(when
(and
(Flag117-)
)
(and
(RIGHTOF0-ROBOT)
(NOT-RIGHTOF1-ROBOT)
(not (NOT-RIGHTOF0-ROBOT))
(not (RIGHTOF1-ROBOT))
)
)
(when
(and
(Flag116-)
)
(and
(LEFTOF10-ROBOT)
(not (NOT-LEFTOF10-ROBOT))
)
)
(when
(and
(Flag115-)
)
(and
(LEFTOF9-ROBOT)
(not (NOT-LEFTOF9-ROBOT))
)
)
(when
(and
(Flag114-)
)
(and
(LEFTOF8-ROBOT)
(not (NOT-LEFTOF8-ROBOT))
)
)
(when
(and
(Flag113-)
)
(and
(LEFTOF7-ROBOT)
(not (NOT-LEFTOF7-ROBOT))
)
)
(when
(and
(Flag112-)
)
(and
(LEFTOF6-ROBOT)
(not (NOT-LEFTOF6-ROBOT))
)
)
(when
(and
(Flag111-)
)
(and
(LEFTOF5-ROBOT)
(not (NOT-LEFTOF5-ROBOT))
)
)
(when
(and
(Flag110-)
)
(and
(LEFTOF4-ROBOT)
(not (NOT-LEFTOF4-ROBOT))
)
)
(when
(and
(Flag109-)
)
(and
(LEFTOF3-ROBOT)
(not (NOT-LEFTOF3-ROBOT))
)
)
(when
(and
(Flag108-)
)
(and
(LEFTOF2-ROBOT)
(not (NOT-LEFTOF2-ROBOT))
)
)
(when
(and
(Flag107-)
)
(and
(LEFTOF1-ROBOT)
(not (NOT-LEFTOF1-ROBOT))
)
)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action CHECKCONSISTENCYACTION
:parameters ()
:precondition
(and
(NOT-ERROR-)
(CHECKCONSISTENCY-)
)
:effect
(and
(when
(and
(RIGHTOF6-ROBOT)
(LEFTOF2-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF4-ROBOT)
(LEFTOF2-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF5-ROBOT)
(ABOVEOF9-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF9-ROBOT)
(BELOWOF1-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF10-ROBOT)
(LEFTOF1-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF8-ROBOT)
(LEFTOF5-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF6-ROBOT)
(BELOWOF3-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF7-ROBOT)
(RIGHTOF9-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF10-ROBOT)
(BELOWOF2-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF8-ROBOT)
(BELOWOF1-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF5-ROBOT)
(BELOWOF2-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF2-ROBOT)
(ABOVEOF8-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF6-ROBOT)
(ABOVEOF9-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF3-ROBOT)
(ABOVEOF9-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF1-ROBOT)
(RIGHTOF8-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF1-ROBOT)
(RIGHTOF7-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF8-ROBOT)
(LEFTOF4-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF9-ROBOT)
(RIGHTOF9-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF4-ROBOT)
(BELOWOF1-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF2-ROBOT)
(RIGHTOF9-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF9-ROBOT)
(ABOVEOF10-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF4-ROBOT)
(ABOVEOF8-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF7-ROBOT)
(BELOWOF1-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF2-ROBOT)
(RIGHTOF10-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF1-ROBOT)
(RIGHTOF6-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF8-ROBOT)
(LEFTOF3-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF1-ROBOT)
(RIGHTOF9-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF9-ROBOT)
(LEFTOF5-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF6-ROBOT)
(RIGHTOF8-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF7-ROBOT)
(BELOWOF6-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF7-ROBOT)
(RIGHTOF8-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF2-ROBOT)
(BELOWOF2-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF9-ROBOT)
(BELOWOF2-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF10-ROBOT)
(BELOWOF4-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF3-ROBOT)
(BELOWOF1-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF6-ROBOT)
(BELOWOF2-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF10-ROBOT)
(BELOWOF3-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF6-ROBOT)
(BELOWOF1-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF4-ROBOT)
(RIGHTOF10-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF10-ROBOT)
(BELOWOF1-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF10-ROBOT)
(LEFTOF9-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF10-ROBOT)
(LEFTOF6-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF8-ROBOT)
(ABOVEOF10-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF5-ROBOT)
(LEFTOF3-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF9-ROBOT)
(BELOWOF7-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF10-ROBOT)
(BELOWOF6-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF10-ROBOT)
(LEFTOF3-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF3-ROBOT)
(ABOVEOF7-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF5-ROBOT)
(BELOWOF3-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF5-ROBOT)
(LEFTOF2-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF9-ROBOT)
(LEFTOF8-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF10-ROBOT)
(BELOWOF7-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF5-ROBOT)
(BELOWOF1-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF8-ROBOT)
(RIGHTOF8-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF5-ROBOT)
(RIGHTOF7-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF5-ROBOT)
(RIGHTOF10-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF1-ROBOT)
(RIGHTOF1-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF6-ROBOT)
(LEFTOF3-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF8-ROBOT)
(ABOVEOF9-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF7-ROBOT)
(BELOWOF2-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF4-ROBOT)
(ABOVEOF6-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF5-ROBOT)
(ABOVEOF10-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF9-ROBOT)
(ABOVEOF9-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF4-ROBOT)
(RIGHTOF7-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF1-ROBOT)
(RIGHTOF3-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF8-ROBOT)
(LEFTOF2-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF4-ROBOT)
(BELOWOF2-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF8-ROBOT)
(BELOWOF5-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF1-ROBOT)
(BELOWOF1-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF1-ROBOT)
(RIGHTOF5-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF3-ROBOT)
(LEFTOF3-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF4-ROBOT)
(ABOVEOF9-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF9-ROBOT)
(LEFTOF6-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF7-ROBOT)
(LEFTOF2-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF3-ROBOT)
(RIGHTOF7-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF8-ROBOT)
(BELOWOF3-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF1-ROBOT)
(RIGHTOF4-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF3-ROBOT)
(RIGHTOF9-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF6-ROBOT)
(ABOVEOF6-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF1-ROBOT)
(RIGHTOF2-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF7-ROBOT)
(BELOWOF4-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF5-ROBOT)
(LEFTOF4-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF2-ROBOT)
(BELOWOF1-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF9-ROBOT)
(LEFTOF4-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF2-ROBOT)
(RIGHTOF3-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF10-ROBOT)
(LEFTOF7-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF6-ROBOT)
(BELOWOF5-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF5-ROBOT)
(LEFTOF5-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF8-ROBOT)
(BELOWOF8-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF3-ROBOT)
(BELOWOF2-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF6-ROBOT)
(LEFTOF4-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF6-ROBOT)
(RIGHTOF6-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF7-ROBOT)
(RIGHTOF7-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF4-ROBOT)
(LEFTOF4-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF7-ROBOT)
(BELOWOF5-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF6-ROBOT)
(ABOVEOF8-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF7-ROBOT)
(LEFTOF6-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF4-ROBOT)
(BELOWOF4-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF10-ROBOT)
(RIGHTOF10-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF8-ROBOT)
(BELOWOF7-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF5-ROBOT)
(BELOWOF4-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF10-ROBOT)
(LEFTOF8-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF6-ROBOT)
(LEFTOF5-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF3-ROBOT)
(ABOVEOF4-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF7-ROBOT)
(BELOWOF7-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF4-ROBOT)
(LEFTOF3-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF10-ROBOT)
(BELOWOF10-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF2-ROBOT)
(LEFTOF2-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF3-ROBOT)
(ABOVEOF3-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF5-ROBOT)
(BELOWOF5-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(NOT-CHECKCONSISTENCY-)
(not (CHECKCONSISTENCY-))
)
)
(:derived (Flag3-)(and (NOT-CHECKCONSISTENCY-)(NOT-ERROR-)(Flag2-)))

(:derived (Flag5-)(and (BELOWOF2-ROBOT)))

(:derived (Flag5-)(and (BELOWOF1-ROBOT)))

(:derived (Flag6-)(and (BELOWOF2-ROBOT)))

(:derived (Flag6-)(and (BELOWOF1-ROBOT)))

(:derived (Flag6-)(and (BELOWOF3-ROBOT)))

(:derived (Flag7-)(and (BELOWOF2-ROBOT)))

(:derived (Flag7-)(and (BELOWOF1-ROBOT)))

(:derived (Flag7-)(and (BELOWOF3-ROBOT)))

(:derived (Flag7-)(and (BELOWOF4-ROBOT)))

(:derived (Flag8-)(and (BELOWOF2-ROBOT)))

(:derived (Flag8-)(and (BELOWOF1-ROBOT)))

(:derived (Flag8-)(and (BELOWOF3-ROBOT)))

(:derived (Flag8-)(and (BELOWOF4-ROBOT)))

(:derived (Flag8-)(and (BELOWOF5-ROBOT)))

(:derived (Flag9-)(and (BELOWOF6-ROBOT)))

(:derived (Flag9-)(and (BELOWOF5-ROBOT)))

(:derived (Flag9-)(and (BELOWOF2-ROBOT)))

(:derived (Flag9-)(and (BELOWOF1-ROBOT)))

(:derived (Flag9-)(and (BELOWOF4-ROBOT)))

(:derived (Flag9-)(and (BELOWOF3-ROBOT)))

(:derived (Flag10-)(and (BELOWOF6-ROBOT)))

(:derived (Flag10-)(and (BELOWOF5-ROBOT)))

(:derived (Flag10-)(and (BELOWOF2-ROBOT)))

(:derived (Flag10-)(and (BELOWOF1-ROBOT)))

(:derived (Flag10-)(and (BELOWOF4-ROBOT)))

(:derived (Flag10-)(and (BELOWOF7-ROBOT)))

(:derived (Flag10-)(and (BELOWOF3-ROBOT)))

(:derived (Flag11-)(and (BELOWOF6-ROBOT)))

(:derived (Flag11-)(and (BELOWOF5-ROBOT)))

(:derived (Flag11-)(and (BELOWOF2-ROBOT)))

(:derived (Flag11-)(and (BELOWOF1-ROBOT)))

(:derived (Flag11-)(and (BELOWOF4-ROBOT)))

(:derived (Flag11-)(and (BELOWOF7-ROBOT)))

(:derived (Flag11-)(and (BELOWOF3-ROBOT)))

(:derived (Flag11-)(and (BELOWOF8-ROBOT)))

(:derived (Flag12-)(and (BELOWOF6-ROBOT)))

(:derived (Flag12-)(and (BELOWOF5-ROBOT)))

(:derived (Flag12-)(and (BELOWOF2-ROBOT)))

(:derived (Flag12-)(and (BELOWOF1-ROBOT)))

(:derived (Flag12-)(and (BELOWOF4-ROBOT)))

(:derived (Flag12-)(and (BELOWOF7-ROBOT)))

(:derived (Flag12-)(and (BELOWOF9-ROBOT)))

(:derived (Flag12-)(and (BELOWOF3-ROBOT)))

(:derived (Flag12-)(and (BELOWOF8-ROBOT)))

(:derived (Flag13-)(and (BELOWOF6-ROBOT)))

(:derived (Flag13-)(and (BELOWOF5-ROBOT)))

(:derived (Flag13-)(and (BELOWOF2-ROBOT)))

(:derived (Flag13-)(and (BELOWOF1-ROBOT)))

(:derived (Flag13-)(and (BELOWOF4-ROBOT)))

(:derived (Flag13-)(and (BELOWOF7-ROBOT)))

(:derived (Flag13-)(and (BELOWOF9-ROBOT)))

(:derived (Flag13-)(and (BELOWOF3-ROBOT)))

(:derived (Flag13-)(and (BELOWOF8-ROBOT)))

(:derived (Flag14-)(and (BELOWOF11-ROBOT)))

(:derived (Flag14-)(and (BELOWOF6-ROBOT)))

(:derived (Flag14-)(and (BELOWOF5-ROBOT)))

(:derived (Flag14-)(and (BELOWOF2-ROBOT)))

(:derived (Flag14-)(and (BELOWOF1-ROBOT)))

(:derived (Flag14-)(and (BELOWOF4-ROBOT)))

(:derived (Flag14-)(and (BELOWOF7-ROBOT)))

(:derived (Flag14-)(and (BELOWOF9-ROBOT)))

(:derived (Flag14-)(and (BELOWOF3-ROBOT)))

(:derived (Flag14-)(and (BELOWOF8-ROBOT)))

(:derived (Flag15-)(and (ABOVEOF6-ROBOT)))

(:derived (Flag15-)(and (ABOVEOF1-ROBOT)))

(:derived (Flag15-)(and (ABOVEOF10-ROBOT)))

(:derived (Flag15-)(and (ABOVEOF2-ROBOT)))

(:derived (Flag15-)(and (ABOVEOF7-ROBOT)))

(:derived (Flag15-)(and (ABOVEOF8-ROBOT)))

(:derived (Flag15-)(and (ABOVEOF9-ROBOT)))

(:derived (Flag15-)(and (ABOVEOF4-ROBOT)))

(:derived (Flag15-)(and (ABOVEOF3-ROBOT)))

(:derived (Flag15-)(and (ABOVEOF5-ROBOT)))

(:derived (Flag16-)(and (ABOVEOF6-ROBOT)))

(:derived (Flag16-)(and (ABOVEOF10-ROBOT)))

(:derived (Flag16-)(and (ABOVEOF2-ROBOT)))

(:derived (Flag16-)(and (ABOVEOF7-ROBOT)))

(:derived (Flag16-)(and (ABOVEOF8-ROBOT)))

(:derived (Flag16-)(and (ABOVEOF9-ROBOT)))

(:derived (Flag16-)(and (ABOVEOF4-ROBOT)))

(:derived (Flag16-)(and (ABOVEOF3-ROBOT)))

(:derived (Flag16-)(and (ABOVEOF5-ROBOT)))

(:derived (Flag17-)(and (ABOVEOF6-ROBOT)))

(:derived (Flag17-)(and (ABOVEOF10-ROBOT)))

(:derived (Flag17-)(and (ABOVEOF8-ROBOT)))

(:derived (Flag17-)(and (ABOVEOF7-ROBOT)))

(:derived (Flag17-)(and (ABOVEOF9-ROBOT)))

(:derived (Flag17-)(and (ABOVEOF4-ROBOT)))

(:derived (Flag17-)(and (ABOVEOF3-ROBOT)))

(:derived (Flag17-)(and (ABOVEOF5-ROBOT)))

(:derived (Flag18-)(and (ABOVEOF6-ROBOT)))

(:derived (Flag18-)(and (ABOVEOF10-ROBOT)))

(:derived (Flag18-)(and (ABOVEOF8-ROBOT)))

(:derived (Flag18-)(and (ABOVEOF7-ROBOT)))

(:derived (Flag18-)(and (ABOVEOF9-ROBOT)))

(:derived (Flag18-)(and (ABOVEOF4-ROBOT)))

(:derived (Flag18-)(and (ABOVEOF5-ROBOT)))

(:derived (Flag19-)(and (ABOVEOF6-ROBOT)))

(:derived (Flag19-)(and (ABOVEOF7-ROBOT)))

(:derived (Flag19-)(and (ABOVEOF8-ROBOT)))

(:derived (Flag19-)(and (ABOVEOF10-ROBOT)))

(:derived (Flag19-)(and (ABOVEOF9-ROBOT)))

(:derived (Flag19-)(and (ABOVEOF5-ROBOT)))

(:derived (Flag20-)(and (ABOVEOF8-ROBOT)))

(:derived (Flag20-)(and (ABOVEOF9-ROBOT)))

(:derived (Flag20-)(and (ABOVEOF7-ROBOT)))

(:derived (Flag20-)(and (ABOVEOF6-ROBOT)))

(:derived (Flag20-)(and (ABOVEOF10-ROBOT)))

(:derived (Flag21-)(and (ABOVEOF8-ROBOT)))

(:derived (Flag21-)(and (ABOVEOF7-ROBOT)))

(:derived (Flag21-)(and (ABOVEOF9-ROBOT)))

(:derived (Flag21-)(and (ABOVEOF10-ROBOT)))

(:derived (Flag22-)(and (ABOVEOF8-ROBOT)))

(:derived (Flag22-)(and (ABOVEOF10-ROBOT)))

(:derived (Flag22-)(and (ABOVEOF9-ROBOT)))

(:derived (Flag23-)(and (ABOVEOF10-ROBOT)))

(:derived (Flag23-)(and (ABOVEOF9-ROBOT)))

(:derived (Flag24-)(and (BELOWOF1-ROBOT)(ABOVEOF2-ROBOT)))

(:derived (Flag25-)(and (ABOVEOF6-ROBOT)(BELOWOF2-ROBOT)))

(:derived (Flag26-)(and (ABOVEOF2-ROBOT)(BELOWOF2-ROBOT)))

(:derived (Flag27-)(and (BELOWOF1-ROBOT)(ABOVEOF4-ROBOT)))

(:derived (Flag28-)(and (BELOWOF1-ROBOT)(ABOVEOF5-ROBOT)))

(:derived (Flag29-)(and (ABOVEOF9-ROBOT)(BELOWOF2-ROBOT)))

(:derived (Flag30-)(and (BELOWOF1-ROBOT)(ABOVEOF7-ROBOT)))

(:derived (Flag31-)(and (ABOVEOF10-ROBOT)(BELOWOF1-ROBOT)))

(:derived (Flag32-)(and (ABOVEOF6-ROBOT)(BELOWOF1-ROBOT)))

(:derived (Flag33-)(and (ABOVEOF10-ROBOT)(BELOWOF2-ROBOT)))

(:derived (Flag34-)(and (ABOVEOF4-ROBOT)(BELOWOF2-ROBOT)))

(:derived (Flag35-)(and (BELOWOF2-ROBOT)(ABOVEOF5-ROBOT)))

(:derived (Flag36-)(and (BELOWOF1-ROBOT)(ABOVEOF1-ROBOT)))

(:derived (Flag37-)(and (ABOVEOF8-ROBOT)(BELOWOF1-ROBOT)))

(:derived (Flag38-)(and (ABOVEOF3-ROBOT)(BELOWOF1-ROBOT)))

(:derived (Flag39-)(and (ABOVEOF7-ROBOT)(BELOWOF2-ROBOT)))

(:derived (Flag40-)(and (BELOWOF1-ROBOT)(ABOVEOF9-ROBOT)))

(:derived (Flag41-)(and (ABOVEOF3-ROBOT)(BELOWOF2-ROBOT)))

(:derived (Flag42-)(and (ABOVEOF8-ROBOT)(BELOWOF2-ROBOT)))

(:derived (Flag43-)(and (ABOVEOF1-ROBOT)(BELOWOF2-ROBOT)))

(:derived (Flag45-)(and (BELOWOF3-ROBOT)(ABOVEOF5-ROBOT)))

(:derived (Flag46-)(and (BELOWOF3-ROBOT)(ABOVEOF6-ROBOT)))

(:derived (Flag47-)(and (BELOWOF3-ROBOT)(ABOVEOF7-ROBOT)))

(:derived (Flag48-)(and (BELOWOF3-ROBOT)(ABOVEOF9-ROBOT)))

(:derived (Flag49-)(and (BELOWOF3-ROBOT)(ABOVEOF2-ROBOT)))

(:derived (Flag50-)(and (ABOVEOF3-ROBOT)(BELOWOF3-ROBOT)))

(:derived (Flag51-)(and (ABOVEOF8-ROBOT)(BELOWOF3-ROBOT)))

(:derived (Flag52-)(and (BELOWOF3-ROBOT)(ABOVEOF4-ROBOT)))

(:derived (Flag53-)(and (BELOWOF3-ROBOT)(ABOVEOF10-ROBOT)))

(:derived (Flag54-)(and (BELOWOF2-ROBOT)(ABOVEOF2-ROBOT)))

(:derived (Flag56-)(and (ABOVEOF3-ROBOT)(BELOWOF4-ROBOT)))

(:derived (Flag57-)(and (ABOVEOF6-ROBOT)(BELOWOF4-ROBOT)))

(:derived (Flag58-)(and (ABOVEOF9-ROBOT)(BELOWOF4-ROBOT)))

(:derived (Flag59-)(and (BELOWOF4-ROBOT)(ABOVEOF4-ROBOT)))

(:derived (Flag60-)(and (ABOVEOF8-ROBOT)(BELOWOF4-ROBOT)))

(:derived (Flag61-)(and (BELOWOF4-ROBOT)(ABOVEOF7-ROBOT)))

(:derived (Flag62-)(and (BELOWOF4-ROBOT)(ABOVEOF5-ROBOT)))

(:derived (Flag63-)(and (BELOWOF3-ROBOT)(ABOVEOF3-ROBOT)))

(:derived (Flag64-)(and (ABOVEOF10-ROBOT)(BELOWOF4-ROBOT)))

(:derived (Flag66-)(and (ABOVEOF4-ROBOT)(BELOWOF5-ROBOT)))

(:derived (Flag67-)(and (ABOVEOF8-ROBOT)(BELOWOF5-ROBOT)))

(:derived (Flag68-)(and (ABOVEOF10-ROBOT)(BELOWOF5-ROBOT)))

(:derived (Flag69-)(and (ABOVEOF5-ROBOT)(BELOWOF5-ROBOT)))

(:derived (Flag70-)(and (ABOVEOF7-ROBOT)(BELOWOF5-ROBOT)))

(:derived (Flag71-)(and (ABOVEOF9-ROBOT)(BELOWOF5-ROBOT)))

(:derived (Flag72-)(and (ABOVEOF6-ROBOT)(BELOWOF5-ROBOT)))

(:derived (Flag74-)(and (BELOWOF6-ROBOT)(ABOVEOF5-ROBOT)))

(:derived (Flag75-)(and (BELOWOF6-ROBOT)(ABOVEOF7-ROBOT)))

(:derived (Flag76-)(and (BELOWOF5-ROBOT)(ABOVEOF5-ROBOT)))

(:derived (Flag77-)(and (BELOWOF6-ROBOT)(ABOVEOF6-ROBOT)))

(:derived (Flag78-)(and (ABOVEOF8-ROBOT)(BELOWOF6-ROBOT)))

(:derived (Flag79-)(and (BELOWOF6-ROBOT)(ABOVEOF9-ROBOT)))

(:derived (Flag80-)(and (BELOWOF6-ROBOT)(ABOVEOF10-ROBOT)))

(:derived (Flag82-)(and (ABOVEOF6-ROBOT)(BELOWOF7-ROBOT)))

(:derived (Flag83-)(and (ABOVEOF9-ROBOT)(BELOWOF7-ROBOT)))

(:derived (Flag84-)(and (ABOVEOF6-ROBOT)(BELOWOF6-ROBOT)))

(:derived (Flag85-)(and (ABOVEOF10-ROBOT)(BELOWOF7-ROBOT)))

(:derived (Flag86-)(and (ABOVEOF10-ROBOT)(BELOWOF6-ROBOT)))

(:derived (Flag87-)(and (ABOVEOF7-ROBOT)(BELOWOF7-ROBOT)))

(:derived (Flag88-)(and (ABOVEOF8-ROBOT)(BELOWOF7-ROBOT)))

(:derived (Flag90-)(and (BELOWOF8-ROBOT)(ABOVEOF10-ROBOT)))

(:derived (Flag91-)(and (ABOVEOF8-ROBOT)(BELOWOF8-ROBOT)))

(:derived (Flag92-)(and (BELOWOF8-ROBOT)(ABOVEOF9-ROBOT)))

(:derived (Flag93-)(and (BELOWOF7-ROBOT)(ABOVEOF7-ROBOT)))

(:derived (Flag94-)(and (BELOWOF8-ROBOT)(ABOVEOF7-ROBOT)))

(:derived (Flag96-)(and (ABOVEOF10-ROBOT)(BELOWOF9-ROBOT)))

(:derived (Flag97-)(and (BELOWOF8-ROBOT)(ABOVEOF8-ROBOT)))

(:derived (Flag98-)(and (ABOVEOF8-ROBOT)(BELOWOF9-ROBOT)))

(:derived (Flag99-)(and (BELOWOF9-ROBOT)(ABOVEOF9-ROBOT)))

(:derived (Flag101-)(and (BELOWOF10-ROBOT)(ABOVEOF10-ROBOT)))

(:derived (Flag102-)(and (ABOVEOF9-ROBOT)(BELOWOF9-ROBOT)))

(:derived (Flag103-)(and (BELOWOF10-ROBOT)(ABOVEOF9-ROBOT)))

(:derived (Flag105-)(and (BELOWOF11-ROBOT)(ABOVEOF10-ROBOT)))

(:derived (Flag207-)(and (BELOWOF1-ROBOT)))

(:action MOVEUP-ROBOT
:parameters ()
:precondition
(and
(Flag4-)
)
:effect
(and
(when
(and
(Flag104-)
)
(and
(ROW10-ROBOT)
(not (NOT-ROW10-ROBOT))
)
)
(when
(and
(Flag100-)
)
(and
(ROW9-ROBOT)
(not (NOT-ROW9-ROBOT))
)
)
(when
(and
(Flag95-)
)
(and
(ROW8-ROBOT)
(not (NOT-ROW8-ROBOT))
)
)
(when
(and
(Flag89-)
)
(and
(ROW7-ROBOT)
(not (NOT-ROW7-ROBOT))
)
)
(when
(and
(Flag81-)
)
(and
(ROW6-ROBOT)
(not (NOT-ROW6-ROBOT))
)
)
(when
(and
(Flag73-)
)
(and
(ROW5-ROBOT)
(not (NOT-ROW5-ROBOT))
)
)
(when
(and
(Flag65-)
)
(and
(ROW4-ROBOT)
(not (NOT-ROW4-ROBOT))
)
)
(when
(and
(Flag55-)
)
(and
(ROW3-ROBOT)
(not (NOT-ROW3-ROBOT))
)
)
(when
(and
(Flag44-)
)
(and
(ROW2-ROBOT)
(not (NOT-ROW2-ROBOT))
)
)
(when
(and
(Flag207-)
)
(and
(ROW1-ROBOT)
(not (NOT-ROW1-ROBOT))
)
)
(when
(and
(ROW9-ROBOT)
)
(and
(ROW10-ROBOT)
(NOT-ROW9-ROBOT)
(not (NOT-ROW10-ROBOT))
(not (ROW9-ROBOT))
)
)
(when
(and
(ROW8-ROBOT)
)
(and
(ROW9-ROBOT)
(NOT-ROW8-ROBOT)
(not (NOT-ROW9-ROBOT))
(not (ROW8-ROBOT))
)
)
(when
(and
(ROW7-ROBOT)
)
(and
(ROW8-ROBOT)
(NOT-ROW7-ROBOT)
(not (NOT-ROW8-ROBOT))
(not (ROW7-ROBOT))
)
)
(when
(and
(ROW6-ROBOT)
)
(and
(ROW7-ROBOT)
(NOT-ROW6-ROBOT)
(not (NOT-ROW7-ROBOT))
(not (ROW6-ROBOT))
)
)
(when
(and
(ROW5-ROBOT)
)
(and
(ROW6-ROBOT)
(NOT-ROW5-ROBOT)
(not (NOT-ROW6-ROBOT))
(not (ROW5-ROBOT))
)
)
(when
(and
(ROW4-ROBOT)
)
(and
(ROW5-ROBOT)
(NOT-ROW4-ROBOT)
(not (NOT-ROW5-ROBOT))
(not (ROW4-ROBOT))
)
)
(when
(and
(ROW3-ROBOT)
)
(and
(ROW4-ROBOT)
(NOT-ROW3-ROBOT)
(not (NOT-ROW4-ROBOT))
(not (ROW3-ROBOT))
)
)
(when
(and
(ROW2-ROBOT)
)
(and
(ROW3-ROBOT)
(NOT-ROW2-ROBOT)
(not (NOT-ROW3-ROBOT))
(not (ROW2-ROBOT))
)
)
(when
(and
(ROW1-ROBOT)
)
(and
(ROW2-ROBOT)
(NOT-ROW1-ROBOT)
(not (NOT-ROW2-ROBOT))
(not (ROW1-ROBOT))
)
)
(when
(and
(ROW0-ROBOT)
)
(and
(ROW1-ROBOT)
(NOT-ROW0-ROBOT)
(not (NOT-ROW1-ROBOT))
(not (ROW0-ROBOT))
)
)
(when
(and
(BELOWOF8-ROBOT)
)
(and
(BELOWOF11-ROBOT)
(NOT-BELOWOF10-ROBOT)
(BELOWOF10-ROBOT)
(NOT-BELOWOF9-ROBOT)
(BELOWOF9-ROBOT)
(NOT-BELOWOF8-ROBOT)
(not (NOT-BELOWOF11-ROBOT))
(not (BELOWOF8-ROBOT))
)
)
(when
(and
(BELOWOF3-ROBOT)
)
(and
(BELOWOF11-ROBOT)
(NOT-BELOWOF10-ROBOT)
(BELOWOF10-ROBOT)
(NOT-BELOWOF9-ROBOT)
(BELOWOF9-ROBOT)
(NOT-BELOWOF8-ROBOT)
(BELOWOF8-ROBOT)
(NOT-BELOWOF7-ROBOT)
(BELOWOF7-ROBOT)
(NOT-BELOWOF6-ROBOT)
(BELOWOF6-ROBOT)
(NOT-BELOWOF5-ROBOT)
(BELOWOF5-ROBOT)
(NOT-BELOWOF4-ROBOT)
(BELOWOF4-ROBOT)
(NOT-BELOWOF3-ROBOT)
(not (NOT-BELOWOF11-ROBOT))
(not (BELOWOF3-ROBOT))
)
)
(when
(and
(BELOWOF9-ROBOT)
)
(and
(BELOWOF11-ROBOT)
(NOT-BELOWOF10-ROBOT)
(BELOWOF10-ROBOT)
(NOT-BELOWOF9-ROBOT)
(not (NOT-BELOWOF11-ROBOT))
(not (BELOWOF9-ROBOT))
)
)
(when
(and
(BELOWOF7-ROBOT)
)
(and
(BELOWOF11-ROBOT)
(NOT-BELOWOF10-ROBOT)
(BELOWOF10-ROBOT)
(NOT-BELOWOF9-ROBOT)
(BELOWOF9-ROBOT)
(NOT-BELOWOF8-ROBOT)
(BELOWOF8-ROBOT)
(NOT-BELOWOF7-ROBOT)
(not (NOT-BELOWOF11-ROBOT))
(not (BELOWOF7-ROBOT))
)
)
(when
(and
(BELOWOF4-ROBOT)
)
(and
(BELOWOF11-ROBOT)
(NOT-BELOWOF10-ROBOT)
(BELOWOF10-ROBOT)
(NOT-BELOWOF9-ROBOT)
(BELOWOF9-ROBOT)
(NOT-BELOWOF8-ROBOT)
(BELOWOF8-ROBOT)
(NOT-BELOWOF7-ROBOT)
(BELOWOF7-ROBOT)
(NOT-BELOWOF6-ROBOT)
(BELOWOF6-ROBOT)
(NOT-BELOWOF5-ROBOT)
(BELOWOF5-ROBOT)
(NOT-BELOWOF4-ROBOT)
(not (NOT-BELOWOF11-ROBOT))
(not (BELOWOF4-ROBOT))
)
)
(when
(and
(BELOWOF1-ROBOT)
)
(and
(BELOWOF11-ROBOT)
(NOT-BELOWOF10-ROBOT)
(BELOWOF10-ROBOT)
(NOT-BELOWOF9-ROBOT)
(BELOWOF9-ROBOT)
(NOT-BELOWOF8-ROBOT)
(BELOWOF8-ROBOT)
(NOT-BELOWOF7-ROBOT)
(BELOWOF7-ROBOT)
(NOT-BELOWOF6-ROBOT)
(BELOWOF6-ROBOT)
(NOT-BELOWOF5-ROBOT)
(BELOWOF5-ROBOT)
(NOT-BELOWOF4-ROBOT)
(BELOWOF4-ROBOT)
(NOT-BELOWOF3-ROBOT)
(BELOWOF3-ROBOT)
(NOT-BELOWOF2-ROBOT)
(BELOWOF2-ROBOT)
(NOT-BELOWOF1-ROBOT)
(not (NOT-BELOWOF11-ROBOT))
(not (BELOWOF1-ROBOT))
)
)
(when
(and
(BELOWOF2-ROBOT)
)
(and
(BELOWOF11-ROBOT)
(NOT-BELOWOF10-ROBOT)
(BELOWOF10-ROBOT)
(NOT-BELOWOF9-ROBOT)
(BELOWOF9-ROBOT)
(NOT-BELOWOF8-ROBOT)
(BELOWOF8-ROBOT)
(NOT-BELOWOF7-ROBOT)
(BELOWOF7-ROBOT)
(NOT-BELOWOF6-ROBOT)
(BELOWOF6-ROBOT)
(NOT-BELOWOF5-ROBOT)
(BELOWOF5-ROBOT)
(NOT-BELOWOF4-ROBOT)
(BELOWOF4-ROBOT)
(NOT-BELOWOF3-ROBOT)
(BELOWOF3-ROBOT)
(NOT-BELOWOF2-ROBOT)
(not (NOT-BELOWOF11-ROBOT))
(not (BELOWOF2-ROBOT))
)
)
(when
(and
(BELOWOF5-ROBOT)
)
(and
(BELOWOF11-ROBOT)
(NOT-BELOWOF10-ROBOT)
(BELOWOF10-ROBOT)
(NOT-BELOWOF9-ROBOT)
(BELOWOF9-ROBOT)
(NOT-BELOWOF8-ROBOT)
(BELOWOF8-ROBOT)
(NOT-BELOWOF7-ROBOT)
(BELOWOF7-ROBOT)
(NOT-BELOWOF6-ROBOT)
(BELOWOF6-ROBOT)
(NOT-BELOWOF5-ROBOT)
(not (NOT-BELOWOF11-ROBOT))
(not (BELOWOF5-ROBOT))
)
)
(when
(and
(BELOWOF6-ROBOT)
)
(and
(BELOWOF11-ROBOT)
(NOT-BELOWOF10-ROBOT)
(BELOWOF10-ROBOT)
(NOT-BELOWOF9-ROBOT)
(BELOWOF9-ROBOT)
(NOT-BELOWOF8-ROBOT)
(BELOWOF8-ROBOT)
(NOT-BELOWOF7-ROBOT)
(BELOWOF7-ROBOT)
(NOT-BELOWOF6-ROBOT)
(not (NOT-BELOWOF11-ROBOT))
(not (BELOWOF6-ROBOT))
)
)
(when
(and
(BELOWOF10-ROBOT)
)
(and
(BELOWOF11-ROBOT)
(NOT-BELOWOF10-ROBOT)
(not (NOT-BELOWOF11-ROBOT))
(not (BELOWOF10-ROBOT))
)
)
(when
(and
(ABOVEOF9-ROBOT)
)
(and
(ABOVEOF10-ROBOT)
(ABOVEOF8-ROBOT)
(ABOVEOF7-ROBOT)
(ABOVEOF6-ROBOT)
(ABOVEOF5-ROBOT)
(ABOVEOF4-ROBOT)
(ABOVEOF3-ROBOT)
(ABOVEOF2-ROBOT)
(not (NOT-ABOVEOF10-ROBOT))
(not (NOT-ABOVEOF9-ROBOT))
(not (NOT-ABOVEOF8-ROBOT))
(not (NOT-ABOVEOF7-ROBOT))
(not (NOT-ABOVEOF6-ROBOT))
(not (NOT-ABOVEOF5-ROBOT))
(not (NOT-ABOVEOF4-ROBOT))
(not (NOT-ABOVEOF3-ROBOT))
(not (NOT-ABOVEOF2-ROBOT))
)
)
(when
(and
(ABOVEOF10-ROBOT)
)
(and
(ABOVEOF9-ROBOT)
(ABOVEOF8-ROBOT)
(ABOVEOF7-ROBOT)
(ABOVEOF6-ROBOT)
(ABOVEOF5-ROBOT)
(ABOVEOF4-ROBOT)
(ABOVEOF3-ROBOT)
(ABOVEOF2-ROBOT)
(not (NOT-ABOVEOF10-ROBOT))
(not (NOT-ABOVEOF9-ROBOT))
(not (NOT-ABOVEOF8-ROBOT))
(not (NOT-ABOVEOF7-ROBOT))
(not (NOT-ABOVEOF6-ROBOT))
(not (NOT-ABOVEOF5-ROBOT))
(not (NOT-ABOVEOF4-ROBOT))
(not (NOT-ABOVEOF3-ROBOT))
(not (NOT-ABOVEOF2-ROBOT))
)
)
(when
(and
(ABOVEOF8-ROBOT)
)
(and
(ABOVEOF9-ROBOT)
(ABOVEOF7-ROBOT)
(ABOVEOF6-ROBOT)
(ABOVEOF5-ROBOT)
(ABOVEOF4-ROBOT)
(ABOVEOF3-ROBOT)
(ABOVEOF2-ROBOT)
(not (NOT-ABOVEOF9-ROBOT))
(not (NOT-ABOVEOF8-ROBOT))
(not (NOT-ABOVEOF7-ROBOT))
(not (NOT-ABOVEOF6-ROBOT))
(not (NOT-ABOVEOF5-ROBOT))
(not (NOT-ABOVEOF4-ROBOT))
(not (NOT-ABOVEOF3-ROBOT))
(not (NOT-ABOVEOF2-ROBOT))
)
)
(when
(and
(ABOVEOF7-ROBOT)
)
(and
(ABOVEOF8-ROBOT)
(ABOVEOF6-ROBOT)
(ABOVEOF5-ROBOT)
(ABOVEOF4-ROBOT)
(ABOVEOF3-ROBOT)
(ABOVEOF2-ROBOT)
(not (NOT-ABOVEOF8-ROBOT))
(not (NOT-ABOVEOF7-ROBOT))
(not (NOT-ABOVEOF6-ROBOT))
(not (NOT-ABOVEOF5-ROBOT))
(not (NOT-ABOVEOF4-ROBOT))
(not (NOT-ABOVEOF3-ROBOT))
(not (NOT-ABOVEOF2-ROBOT))
)
)
(when
(and
(ABOVEOF6-ROBOT)
)
(and
(ABOVEOF7-ROBOT)
(ABOVEOF5-ROBOT)
(ABOVEOF4-ROBOT)
(ABOVEOF3-ROBOT)
(ABOVEOF2-ROBOT)
(not (NOT-ABOVEOF7-ROBOT))
(not (NOT-ABOVEOF6-ROBOT))
(not (NOT-ABOVEOF5-ROBOT))
(not (NOT-ABOVEOF4-ROBOT))
(not (NOT-ABOVEOF3-ROBOT))
(not (NOT-ABOVEOF2-ROBOT))
)
)
(when
(and
(ABOVEOF5-ROBOT)
)
(and
(ABOVEOF6-ROBOT)
(ABOVEOF4-ROBOT)
(ABOVEOF3-ROBOT)
(ABOVEOF2-ROBOT)
(not (NOT-ABOVEOF6-ROBOT))
(not (NOT-ABOVEOF5-ROBOT))
(not (NOT-ABOVEOF4-ROBOT))
(not (NOT-ABOVEOF3-ROBOT))
(not (NOT-ABOVEOF2-ROBOT))
)
)
(when
(and
(ABOVEOF4-ROBOT)
)
(and
(ABOVEOF5-ROBOT)
(ABOVEOF3-ROBOT)
(ABOVEOF2-ROBOT)
(not (NOT-ABOVEOF5-ROBOT))
(not (NOT-ABOVEOF4-ROBOT))
(not (NOT-ABOVEOF3-ROBOT))
(not (NOT-ABOVEOF2-ROBOT))
)
)
(when
(and
(ABOVEOF3-ROBOT)
)
(and
(ABOVEOF4-ROBOT)
(ABOVEOF2-ROBOT)
(not (NOT-ABOVEOF4-ROBOT))
(not (NOT-ABOVEOF3-ROBOT))
(not (NOT-ABOVEOF2-ROBOT))
)
)
(when
(and
(ABOVEOF2-ROBOT)
)
(and
(ABOVEOF3-ROBOT)
(not (NOT-ABOVEOF3-ROBOT))
(not (NOT-ABOVEOF2-ROBOT))
)
)
(when
(and
(ABOVEOF1-ROBOT)
)
(and
(ABOVEOF2-ROBOT)
(not (NOT-ABOVEOF2-ROBOT))
)
)
(ABOVEOF1-ROBOT)
(CHECKCONSISTENCY-)
(not (NOT-ABOVEOF1-ROBOT))
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action MOVEDOWN-ROBOT
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
)
:effect
(and
(when
(and
(Flag106-)
)
(and
(ROW9-ROBOT)
(not (NOT-ROW9-ROBOT))
)
)
(when
(and
(Flag104-)
)
(and
(ROW8-ROBOT)
(not (NOT-ROW8-ROBOT))
)
)
(when
(and
(Flag100-)
)
(and
(ROW7-ROBOT)
(not (NOT-ROW7-ROBOT))
)
)
(when
(and
(Flag95-)
)
(and
(ROW6-ROBOT)
(not (NOT-ROW6-ROBOT))
)
)
(when
(and
(Flag89-)
)
(and
(ROW5-ROBOT)
(not (NOT-ROW5-ROBOT))
)
)
(when
(and
(Flag81-)
)
(and
(ROW4-ROBOT)
(not (NOT-ROW4-ROBOT))
)
)
(when
(and
(Flag73-)
)
(and
(ROW3-ROBOT)
(not (NOT-ROW3-ROBOT))
)
)
(when
(and
(Flag65-)
)
(and
(ROW2-ROBOT)
(not (NOT-ROW2-ROBOT))
)
)
(when
(and
(Flag55-)
)
(and
(ROW1-ROBOT)
(not (NOT-ROW1-ROBOT))
)
)
(when
(and
(Flag44-)
)
(and
(ROW0-ROBOT)
(not (NOT-ROW0-ROBOT))
)
)
(when
(and
(ROW10-ROBOT)
)
(and
(ROW9-ROBOT)
(NOT-ROW10-ROBOT)
(not (NOT-ROW9-ROBOT))
(not (ROW10-ROBOT))
)
)
(when
(and
(ROW9-ROBOT)
)
(and
(ROW8-ROBOT)
(NOT-ROW9-ROBOT)
(not (NOT-ROW8-ROBOT))
(not (ROW9-ROBOT))
)
)
(when
(and
(ROW8-ROBOT)
)
(and
(ROW7-ROBOT)
(NOT-ROW8-ROBOT)
(not (NOT-ROW7-ROBOT))
(not (ROW8-ROBOT))
)
)
(when
(and
(ROW7-ROBOT)
)
(and
(ROW6-ROBOT)
(NOT-ROW7-ROBOT)
(not (NOT-ROW6-ROBOT))
(not (ROW7-ROBOT))
)
)
(when
(and
(ROW6-ROBOT)
)
(and
(ROW5-ROBOT)
(NOT-ROW6-ROBOT)
(not (NOT-ROW5-ROBOT))
(not (ROW6-ROBOT))
)
)
(when
(and
(ROW5-ROBOT)
)
(and
(ROW4-ROBOT)
(NOT-ROW5-ROBOT)
(not (NOT-ROW4-ROBOT))
(not (ROW5-ROBOT))
)
)
(when
(and
(ROW4-ROBOT)
)
(and
(ROW3-ROBOT)
(NOT-ROW4-ROBOT)
(not (NOT-ROW3-ROBOT))
(not (ROW4-ROBOT))
)
)
(when
(and
(ROW3-ROBOT)
)
(and
(ROW2-ROBOT)
(NOT-ROW3-ROBOT)
(not (NOT-ROW2-ROBOT))
(not (ROW3-ROBOT))
)
)
(when
(and
(ROW2-ROBOT)
)
(and
(ROW1-ROBOT)
(NOT-ROW2-ROBOT)
(not (NOT-ROW1-ROBOT))
(not (ROW2-ROBOT))
)
)
(when
(and
(ROW1-ROBOT)
)
(and
(ROW0-ROBOT)
(NOT-ROW1-ROBOT)
(not (NOT-ROW0-ROBOT))
(not (ROW1-ROBOT))
)
)
(when
(and
(ABOVEOF10-ROBOT)
)
(and
(ABOVEOF9-ROBOT)
(NOT-ABOVEOF10-ROBOT)
(not (NOT-ABOVEOF9-ROBOT))
(not (ABOVEOF10-ROBOT))
)
)
(when
(and
(Flag23-)
)
(and
(ABOVEOF8-ROBOT)
(NOT-ABOVEOF9-ROBOT)
(not (NOT-ABOVEOF8-ROBOT))
(not (ABOVEOF9-ROBOT))
)
)
(when
(and
(Flag22-)
)
(and
(ABOVEOF7-ROBOT)
(NOT-ABOVEOF8-ROBOT)
(not (NOT-ABOVEOF7-ROBOT))
(not (ABOVEOF8-ROBOT))
)
)
(when
(and
(Flag21-)
)
(and
(ABOVEOF6-ROBOT)
(NOT-ABOVEOF7-ROBOT)
(not (NOT-ABOVEOF6-ROBOT))
(not (ABOVEOF7-ROBOT))
)
)
(when
(and
(Flag20-)
)
(and
(ABOVEOF5-ROBOT)
(NOT-ABOVEOF6-ROBOT)
(not (NOT-ABOVEOF5-ROBOT))
(not (ABOVEOF6-ROBOT))
)
)
(when
(and
(Flag19-)
)
(and
(ABOVEOF4-ROBOT)
(NOT-ABOVEOF5-ROBOT)
(not (NOT-ABOVEOF4-ROBOT))
(not (ABOVEOF5-ROBOT))
)
)
(when
(and
(Flag18-)
)
(and
(ABOVEOF3-ROBOT)
(NOT-ABOVEOF4-ROBOT)
(not (NOT-ABOVEOF3-ROBOT))
(not (ABOVEOF4-ROBOT))
)
)
(when
(and
(Flag17-)
)
(and
(ABOVEOF2-ROBOT)
(NOT-ABOVEOF3-ROBOT)
(not (NOT-ABOVEOF2-ROBOT))
(not (ABOVEOF3-ROBOT))
)
)
(when
(and
(Flag16-)
)
(and
(ABOVEOF1-ROBOT)
(NOT-ABOVEOF2-ROBOT)
(not (NOT-ABOVEOF1-ROBOT))
(not (ABOVEOF2-ROBOT))
)
)
(when
(and
(Flag15-)
)
(and
(NOT-ABOVEOF1-ROBOT)
(not (ABOVEOF1-ROBOT))
)
)
(when
(and
(Flag14-)
)
(and
(BELOWOF10-ROBOT)
(not (NOT-BELOWOF10-ROBOT))
)
)
(when
(and
(Flag13-)
)
(and
(BELOWOF9-ROBOT)
(not (NOT-BELOWOF9-ROBOT))
)
)
(when
(and
(Flag12-)
)
(and
(BELOWOF8-ROBOT)
(not (NOT-BELOWOF8-ROBOT))
)
)
(when
(and
(Flag11-)
)
(and
(BELOWOF7-ROBOT)
(not (NOT-BELOWOF7-ROBOT))
)
)
(when
(and
(Flag10-)
)
(and
(BELOWOF6-ROBOT)
(not (NOT-BELOWOF6-ROBOT))
)
)
(when
(and
(Flag9-)
)
(and
(BELOWOF5-ROBOT)
(not (NOT-BELOWOF5-ROBOT))
)
)
(when
(and
(Flag8-)
)
(and
(BELOWOF4-ROBOT)
(not (NOT-BELOWOF4-ROBOT))
)
)
(when
(and
(Flag7-)
)
(and
(BELOWOF3-ROBOT)
(not (NOT-BELOWOF3-ROBOT))
)
)
(when
(and
(Flag6-)
)
(and
(BELOWOF2-ROBOT)
(not (NOT-BELOWOF2-ROBOT))
)
)
(when
(and
(Flag5-)
)
(and
(BELOWOF1-ROBOT)
(not (NOT-BELOWOF1-ROBOT))
)
)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:derived (Flag2-)(and (RIGHTOF2-ROBOT)))

(:derived (Flag2-)(and (LEFTOF10-ROBOT)))

(:derived (Flag4-)(and (NOT-CHECKCONSISTENCY-)(NOT-ERROR-)))

(:derived (Flag13-)(and (BELOWOF10-ROBOT)))

(:derived (Flag14-)(and (BELOWOF10-ROBOT)))

(:derived (Flag115-)(and (LEFTOF10-ROBOT)))

(:derived (Flag116-)(and (LEFTOF10-ROBOT)))

(:derived (Flag117-)(and (RIGHTOF2-ROBOT)))

(:derived (Flag118-)(and (RIGHTOF2-ROBOT)))

(:derived (Flag208-)(and (RIGHTOF2-ROBOT)))

)
