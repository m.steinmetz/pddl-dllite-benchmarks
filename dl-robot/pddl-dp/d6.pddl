(define (domain grounded-SIMPLE-ADL-ROBOT)
(:requirements
:strips
)
(:predicates
(Flag79-)
(Flag75-)
(Flag47-)
(Flag41-)
(Flag40-)
(Flag39-)
(Flag12-)
(Flag4-)
(Flag2-)
(ROW5-ROBOT)
(ROW4-ROBOT)
(ROW3-ROBOT)
(ROW2-ROBOT)
(ROW1-ROBOT)
(BELOWOF6-ROBOT)
(NOT-BELOWOF5-ROBOT)
(BELOWOF4-ROBOT)
(BELOWOF3-ROBOT)
(BELOWOF2-ROBOT)
(ABOVEOF5-ROBOT)
(ABOVEOF4-ROBOT)
(ABOVEOF3-ROBOT)
(ABOVEOF2-ROBOT)
(ABOVEOF1-ROBOT)
(CHECKCONSISTENCY-)
(ROW0-ROBOT)
(BELOWOF1-ROBOT)
(Flag77-)
(Flag76-)
(Flag37-)
(Flag36-)
(Flag34-)
(Flag33-)
(Flag32-)
(Flag31-)
(Flag29-)
(Flag28-)
(Flag27-)
(Flag26-)
(Flag25-)
(Flag23-)
(Flag22-)
(Flag21-)
(Flag20-)
(Flag19-)
(Flag18-)
(Flag17-)
(Flag16-)
(Flag15-)
(Flag14-)
(Flag13-)
(Flag11-)
(Flag10-)
(Flag9-)
(Flag8-)
(Flag7-)
(Flag6-)
(Flag5-)
(Flag3-)
(ERROR-)
(COLUMN5-ROBOT)
(COLUMN4-ROBOT)
(COLUMN3-ROBOT)
(COLUMN2-ROBOT)
(COLUMN1-ROBOT)
(LEFTOF6-ROBOT)
(NOT-LEFTOF5-ROBOT)
(LEFTOF4-ROBOT)
(LEFTOF3-ROBOT)
(LEFTOF2-ROBOT)
(RIGHTOF5-ROBOT)
(RIGHTOF4-ROBOT)
(RIGHTOF3-ROBOT)
(RIGHTOF1-ROBOT)
(COLUMN0-ROBOT)
(NOT-RIGHTOF2-ROBOT)
(RIGHTOF0-ROBOT)
(LEFTOF1-ROBOT)
(Flag80-)
(Flag78-)
(Flag73-)
(Flag72-)
(Flag70-)
(Flag69-)
(Flag68-)
(Flag67-)
(Flag65-)
(Flag64-)
(Flag63-)
(Flag62-)
(Flag60-)
(Flag59-)
(Flag58-)
(Flag57-)
(Flag56-)
(Flag55-)
(Flag53-)
(Flag52-)
(Flag51-)
(Flag50-)
(Flag49-)
(Flag48-)
(Flag46-)
(Flag45-)
(Flag44-)
(Flag43-)
(Flag42-)
(Flag38-)
(Flag35-)
(Flag30-)
(Flag24-)
(Flag1-)
(Flag81-)
(Flag74-)
(Flag71-)
(Flag66-)
(Flag61-)
(Flag54-)
(NOT-COLUMN4-ROBOT)
(NOT-COLUMN3-ROBOT)
(NOT-COLUMN2-ROBOT)
(NOT-COLUMN1-ROBOT)
(NOT-COLUMN0-ROBOT)
(NOT-RIGHTOF4-ROBOT)
(NOT-RIGHTOF3-ROBOT)
(NOT-RIGHTOF1-ROBOT)
(RIGHTOF2-ROBOT)
(NOT-RIGHTOF0-ROBOT)
(NOT-LEFTOF4-ROBOT)
(NOT-LEFTOF3-ROBOT)
(NOT-LEFTOF2-ROBOT)
(NOT-LEFTOF1-ROBOT)
(NOT-CHECKCONSISTENCY-)
(NOT-COLUMN5-ROBOT)
(NOT-LEFTOF6-ROBOT)
(LEFTOF5-ROBOT)
(NOT-RIGHTOF5-ROBOT)
(NOT-ERROR-)
(NOT-ROW4-ROBOT)
(NOT-ROW3-ROBOT)
(NOT-ROW2-ROBOT)
(NOT-ROW1-ROBOT)
(NOT-ROW0-ROBOT)
(NOT-ABOVEOF4-ROBOT)
(NOT-ABOVEOF3-ROBOT)
(NOT-ABOVEOF2-ROBOT)
(NOT-ABOVEOF1-ROBOT)
(NOT-BELOWOF4-ROBOT)
(NOT-BELOWOF3-ROBOT)
(NOT-BELOWOF2-ROBOT)
(NOT-BELOWOF1-ROBOT)
(NOT-ROW5-ROBOT)
(NOT-BELOWOF6-ROBOT)
(BELOWOF5-ROBOT)
(NOT-ABOVEOF5-ROBOT)
)
(:derived (Flag54-)(and (Flag48-)))

(:derived (Flag54-)(and (Flag49-)))

(:derived (Flag54-)(and (Flag50-)))

(:derived (Flag54-)(and (Flag51-)))

(:derived (Flag54-)(and (Flag52-)))

(:derived (Flag54-)(and (Flag53-)))

(:derived (Flag61-)(and (Flag48-)))

(:derived (Flag61-)(and (Flag55-)))

(:derived (Flag61-)(and (Flag56-)))

(:derived (Flag61-)(and (Flag49-)))

(:derived (Flag61-)(and (Flag50-)))

(:derived (Flag61-)(and (Flag57-)))

(:derived (Flag61-)(and (Flag58-)))

(:derived (Flag61-)(and (Flag52-)))

(:derived (Flag61-)(and (Flag53-)))

(:derived (Flag61-)(and (Flag59-)))

(:derived (Flag61-)(and (Flag60-)))

(:derived (Flag66-)(and (Flag48-)))

(:derived (Flag66-)(and (Flag57-)))

(:derived (Flag66-)(and (Flag56-)))

(:derived (Flag66-)(and (Flag50-)))

(:derived (Flag66-)(and (Flag62-)))

(:derived (Flag66-)(and (Flag58-)))

(:derived (Flag66-)(and (Flag52-)))

(:derived (Flag66-)(and (Flag53-)))

(:derived (Flag66-)(and (Flag63-)))

(:derived (Flag66-)(and (Flag59-)))

(:derived (Flag66-)(and (Flag64-)))

(:derived (Flag66-)(and (Flag60-)))

(:derived (Flag66-)(and (Flag65-)))

(:derived (Flag71-)(and (Flag48-)))

(:derived (Flag71-)(and (Flag57-)))

(:derived (Flag71-)(and (Flag50-)))

(:derived (Flag71-)(and (Flag56-)))

(:derived (Flag71-)(and (Flag67-)))

(:derived (Flag71-)(and (Flag68-)))

(:derived (Flag71-)(and (Flag62-)))

(:derived (Flag71-)(and (Flag52-)))

(:derived (Flag71-)(and (Flag63-)))

(:derived (Flag71-)(and (Flag59-)))

(:derived (Flag71-)(and (Flag69-)))

(:derived (Flag71-)(and (Flag70-)))

(:derived (Flag71-)(and (Flag60-)))

(:derived (Flag74-)(and (Flag48-)))

(:derived (Flag74-)(and (Flag57-)))

(:derived (Flag74-)(and (Flag72-)))

(:derived (Flag74-)(and (Flag56-)))

(:derived (Flag74-)(and (Flag68-)))

(:derived (Flag74-)(and (Flag52-)))

(:derived (Flag74-)(and (Flag62-)))

(:derived (Flag74-)(and (Flag63-)))

(:derived (Flag74-)(and (Flag73-)))

(:derived (Flag74-)(and (Flag70-)))

(:derived (Flag74-)(and (Flag60-)))

(:derived (Flag81-)(and (Flag56-)))

(:derived (Flag81-)(and (Flag68-)))

(:derived (Flag81-)(and (Flag57-)))

(:derived (Flag81-)(and (Flag62-)))

(:derived (Flag81-)(and (Flag80-)))

(:derived (Flag81-)(and (Flag73-)))

(:derived (Flag1-)(and (COLUMN2-ROBOT)(ROW1-ROBOT)(NOT-ERROR-)(NOT-CHECKCONSISTENCY-)))

(:derived (Flag2-)(and (LEFTOF4-ROBOT)))

(:derived (Flag2-)(and (LEFTOF3-ROBOT)))

(:derived (Flag2-)(and (RIGHTOF1-ROBOT)))

(:derived (Flag2-)(and (RIGHTOF4-ROBOT)))

(:derived (Flag2-)(and (RIGHTOF0-ROBOT)))

(:derived (Flag2-)(and (LEFTOF2-ROBOT)))

(:derived (Flag2-)(and (RIGHTOF5-ROBOT)))

(:derived (Flag2-)(and (LEFTOF1-ROBOT)))

(:derived (Flag2-)(and (LEFTOF6-ROBOT)))

(:derived (Flag2-)(and (RIGHTOF3-ROBOT)))

(:derived (Flag18-)(and (Flag13-)))

(:derived (Flag18-)(and (Flag14-)))

(:derived (Flag18-)(and (Flag15-)))

(:derived (Flag18-)(and (Flag16-)))

(:derived (Flag18-)(and (Flag17-)))

(:derived (Flag24-)(and (Flag14-)))

(:derived (Flag24-)(and (Flag13-)))

(:derived (Flag24-)(and (Flag19-)))

(:derived (Flag24-)(and (Flag15-)))

(:derived (Flag24-)(and (Flag16-)))

(:derived (Flag24-)(and (Flag20-)))

(:derived (Flag24-)(and (Flag21-)))

(:derived (Flag24-)(and (Flag17-)))

(:derived (Flag24-)(and (Flag22-)))

(:derived (Flag24-)(and (Flag23-)))

(:derived (Flag30-)(and (Flag14-)))

(:derived (Flag30-)(and (Flag25-)))

(:derived (Flag30-)(and (Flag26-)))

(:derived (Flag30-)(and (Flag27-)))

(:derived (Flag30-)(and (Flag16-)))

(:derived (Flag30-)(and (Flag28-)))

(:derived (Flag30-)(and (Flag17-)))

(:derived (Flag30-)(and (Flag21-)))

(:derived (Flag30-)(and (Flag29-)))

(:derived (Flag30-)(and (Flag20-)))

(:derived (Flag30-)(and (Flag22-)))

(:derived (Flag30-)(and (Flag13-)))

(:derived (Flag35-)(and (Flag13-)))

(:derived (Flag35-)(and (Flag27-)))

(:derived (Flag35-)(and (Flag16-)))

(:derived (Flag35-)(and (Flag31-)))

(:derived (Flag35-)(and (Flag32-)))

(:derived (Flag35-)(and (Flag28-)))

(:derived (Flag35-)(and (Flag17-)))

(:derived (Flag35-)(and (Flag33-)))

(:derived (Flag35-)(and (Flag21-)))

(:derived (Flag35-)(and (Flag20-)))

(:derived (Flag35-)(and (Flag22-)))

(:derived (Flag35-)(and (Flag34-)))

(:derived (Flag38-)(and (Flag13-)))

(:derived (Flag38-)(and (Flag36-)))

(:derived (Flag38-)(and (Flag27-)))

(:derived (Flag38-)(and (Flag37-)))

(:derived (Flag38-)(and (Flag16-)))

(:derived (Flag38-)(and (Flag28-)))

(:derived (Flag38-)(and (Flag33-)))

(:derived (Flag38-)(and (Flag21-)))

(:derived (Flag38-)(and (Flag20-)))

(:derived (Flag38-)(and (Flag32-)))

(:derived (Flag39-)(and (RIGHTOF3-ROBOT)))

(:derived (Flag39-)(and (RIGHTOF5-ROBOT)))

(:derived (Flag39-)(and (RIGHTOF0-ROBOT)))

(:derived (Flag39-)(and (RIGHTOF4-ROBOT)))

(:derived (Flag39-)(and (RIGHTOF1-ROBOT)))

(:derived (Flag40-)(and (RIGHTOF3-ROBOT)))

(:derived (Flag40-)(and (RIGHTOF1-ROBOT)))

(:derived (Flag40-)(and (RIGHTOF4-ROBOT)))

(:derived (Flag40-)(and (RIGHTOF5-ROBOT)))

(:derived (Flag41-)(and (RIGHTOF3-ROBOT)))

(:derived (Flag41-)(and (RIGHTOF4-ROBOT)))

(:derived (Flag41-)(and (RIGHTOF5-ROBOT)))

(:derived (Flag42-)(and (RIGHTOF3-ROBOT)))

(:derived (Flag42-)(and (RIGHTOF4-ROBOT)))

(:derived (Flag42-)(and (RIGHTOF5-ROBOT)))

(:derived (Flag43-)(and (RIGHTOF4-ROBOT)))

(:derived (Flag43-)(and (RIGHTOF5-ROBOT)))

(:derived (Flag44-)(and (LEFTOF1-ROBOT)))

(:derived (Flag44-)(and (LEFTOF2-ROBOT)))

(:derived (Flag45-)(and (LEFTOF1-ROBOT)))

(:derived (Flag45-)(and (LEFTOF3-ROBOT)))

(:derived (Flag45-)(and (LEFTOF2-ROBOT)))

(:derived (Flag46-)(and (LEFTOF1-ROBOT)))

(:derived (Flag46-)(and (LEFTOF3-ROBOT)))

(:derived (Flag46-)(and (LEFTOF4-ROBOT)))

(:derived (Flag46-)(and (LEFTOF2-ROBOT)))

(:derived (Flag47-)(and (LEFTOF1-ROBOT)))

(:derived (Flag47-)(and (LEFTOF3-ROBOT)))

(:derived (Flag47-)(and (LEFTOF4-ROBOT)))

(:derived (Flag47-)(and (LEFTOF2-ROBOT)))

(:derived (Flag48-)(and (LEFTOF1-ROBOT)(RIGHTOF4-ROBOT)))

(:derived (Flag49-)(and (LEFTOF1-ROBOT)(RIGHTOF1-ROBOT)))

(:derived (Flag50-)(and (RIGHTOF3-ROBOT)(LEFTOF1-ROBOT)))

(:derived (Flag51-)(and (RIGHTOF0-ROBOT)(LEFTOF1-ROBOT)))

(:derived (Flag52-)(and (LEFTOF1-ROBOT)(RIGHTOF5-ROBOT)))

(:derived (Flag53-)(and (LEFTOF1-ROBOT)(RIGHTOF2-ROBOT)))

(:derived (Flag55-)(and (LEFTOF2-ROBOT)(RIGHTOF1-ROBOT)))

(:derived (Flag56-)(and (RIGHTOF5-ROBOT)(LEFTOF2-ROBOT)))

(:derived (Flag57-)(and (RIGHTOF5-ROBOT)(LEFTOF1-ROBOT)))

(:derived (Flag58-)(and (RIGHTOF2-ROBOT)(LEFTOF2-ROBOT)))

(:derived (Flag59-)(and (RIGHTOF3-ROBOT)(LEFTOF2-ROBOT)))

(:derived (Flag60-)(and (LEFTOF2-ROBOT)(RIGHTOF4-ROBOT)))

(:derived (Flag62-)(and (LEFTOF3-ROBOT)(RIGHTOF5-ROBOT)))

(:derived (Flag63-)(and (LEFTOF3-ROBOT)(RIGHTOF4-ROBOT)))

(:derived (Flag64-)(and (RIGHTOF3-ROBOT)(LEFTOF3-ROBOT)))

(:derived (Flag65-)(and (LEFTOF3-ROBOT)(RIGHTOF2-ROBOT)))

(:derived (Flag67-)(and (LEFTOF3-ROBOT)(RIGHTOF3-ROBOT)))

(:derived (Flag68-)(and (LEFTOF4-ROBOT)(RIGHTOF5-ROBOT)))

(:derived (Flag69-)(and (RIGHTOF3-ROBOT)(LEFTOF4-ROBOT)))

(:derived (Flag70-)(and (LEFTOF4-ROBOT)(RIGHTOF4-ROBOT)))

(:derived (Flag72-)(and (RIGHTOF4-ROBOT)(LEFTOF5-ROBOT)))

(:derived (Flag73-)(and (RIGHTOF5-ROBOT)(LEFTOF5-ROBOT)))

(:derived (Flag78-)(and (Flag76-)))

(:derived (Flag78-)(and (Flag27-)))

(:derived (Flag78-)(and (Flag16-)))

(:derived (Flag78-)(and (Flag77-)))

(:derived (Flag78-)(and (Flag21-)))

(:derived (Flag78-)(and (Flag32-)))

(:derived (Flag79-)(and (LEFTOF2-ROBOT)))

(:derived (Flag79-)(and (LEFTOF1-ROBOT)))

(:derived (Flag79-)(and (LEFTOF6-ROBOT)))

(:derived (Flag79-)(and (LEFTOF3-ROBOT)))

(:derived (Flag79-)(and (LEFTOF4-ROBOT)))

(:derived (Flag80-)(and (RIGHTOF5-ROBOT)(LEFTOF6-ROBOT)))

(:action MOVELEFT-ROBOT
:parameters ()
:precondition
(and
(Flag3-)
)
:effect
(and
(when
(and
(Flag81-)
)
(and
(COLUMN4-ROBOT)
(not (NOT-COLUMN4-ROBOT))
)
)
(when
(and
(Flag74-)
)
(and
(COLUMN3-ROBOT)
(not (NOT-COLUMN3-ROBOT))
)
)
(when
(and
(Flag71-)
)
(and
(COLUMN2-ROBOT)
(not (NOT-COLUMN2-ROBOT))
)
)
(when
(and
(Flag66-)
)
(and
(COLUMN1-ROBOT)
(not (NOT-COLUMN1-ROBOT))
)
)
(when
(and
(Flag61-)
)
(and
(COLUMN0-ROBOT)
(not (NOT-COLUMN0-ROBOT))
)
)
(when
(and
(COLUMN5-ROBOT)
)
(and
(COLUMN4-ROBOT)
(NOT-COLUMN5-ROBOT)
(not (NOT-COLUMN4-ROBOT))
(not (COLUMN5-ROBOT))
)
)
(when
(and
(COLUMN4-ROBOT)
)
(and
(COLUMN3-ROBOT)
(NOT-COLUMN4-ROBOT)
(not (NOT-COLUMN3-ROBOT))
(not (COLUMN4-ROBOT))
)
)
(when
(and
(COLUMN3-ROBOT)
)
(and
(COLUMN2-ROBOT)
(NOT-COLUMN3-ROBOT)
(not (NOT-COLUMN2-ROBOT))
(not (COLUMN3-ROBOT))
)
)
(when
(and
(COLUMN2-ROBOT)
)
(and
(COLUMN1-ROBOT)
(NOT-COLUMN2-ROBOT)
(not (NOT-COLUMN1-ROBOT))
(not (COLUMN2-ROBOT))
)
)
(when
(and
(COLUMN1-ROBOT)
)
(and
(COLUMN0-ROBOT)
(NOT-COLUMN1-ROBOT)
(not (NOT-COLUMN0-ROBOT))
(not (COLUMN1-ROBOT))
)
)
(when
(and
(RIGHTOF5-ROBOT)
)
(and
(RIGHTOF4-ROBOT)
(NOT-RIGHTOF5-ROBOT)
(RIGHTOF3-ROBOT)
(NOT-RIGHTOF4-ROBOT)
(RIGHTOF2-ROBOT)
(NOT-RIGHTOF3-ROBOT)
(RIGHTOF1-ROBOT)
(NOT-RIGHTOF2-ROBOT)
(RIGHTOF0-ROBOT)
(NOT-RIGHTOF1-ROBOT)
(not (RIGHTOF5-ROBOT))
(not (NOT-RIGHTOF0-ROBOT))
)
)
(when
(and
(RIGHTOF4-ROBOT)
)
(and
(RIGHTOF3-ROBOT)
(NOT-RIGHTOF4-ROBOT)
(RIGHTOF2-ROBOT)
(NOT-RIGHTOF3-ROBOT)
(RIGHTOF1-ROBOT)
(NOT-RIGHTOF2-ROBOT)
(RIGHTOF0-ROBOT)
(NOT-RIGHTOF1-ROBOT)
(not (RIGHTOF4-ROBOT))
(not (NOT-RIGHTOF0-ROBOT))
)
)
(when
(and
(RIGHTOF3-ROBOT)
)
(and
(RIGHTOF2-ROBOT)
(NOT-RIGHTOF3-ROBOT)
(RIGHTOF1-ROBOT)
(NOT-RIGHTOF2-ROBOT)
(RIGHTOF0-ROBOT)
(NOT-RIGHTOF1-ROBOT)
(not (RIGHTOF3-ROBOT))
(not (NOT-RIGHTOF0-ROBOT))
)
)
(when
(and
(RIGHTOF2-ROBOT)
)
(and
(RIGHTOF1-ROBOT)
(NOT-RIGHTOF2-ROBOT)
(RIGHTOF0-ROBOT)
(NOT-RIGHTOF1-ROBOT)
(not (RIGHTOF2-ROBOT))
(not (NOT-RIGHTOF0-ROBOT))
)
)
(when
(and
(RIGHTOF1-ROBOT)
)
(and
(RIGHTOF0-ROBOT)
(NOT-RIGHTOF1-ROBOT)
(not (NOT-RIGHTOF0-ROBOT))
(not (RIGHTOF1-ROBOT))
)
)
(when
(and
(Flag79-)
)
(and
(LEFTOF5-ROBOT)
(not (NOT-LEFTOF5-ROBOT))
)
)
(when
(and
(LEFTOF2-ROBOT)
)
(and
(LEFTOF4-ROBOT)
(LEFTOF3-ROBOT)
(LEFTOF1-ROBOT)
(not (NOT-LEFTOF4-ROBOT))
(not (NOT-LEFTOF3-ROBOT))
(not (NOT-LEFTOF2-ROBOT))
(not (NOT-LEFTOF1-ROBOT))
)
)
(when
(and
(LEFTOF4-ROBOT)
)
(and
(LEFTOF3-ROBOT)
(not (NOT-LEFTOF4-ROBOT))
(not (NOT-LEFTOF3-ROBOT))
)
)
(when
(and
(LEFTOF3-ROBOT)
)
(and
(LEFTOF4-ROBOT)
(LEFTOF2-ROBOT)
(not (NOT-LEFTOF4-ROBOT))
(not (NOT-LEFTOF3-ROBOT))
(not (NOT-LEFTOF2-ROBOT))
)
)
(when
(and
(LEFTOF1-ROBOT)
)
(and
(LEFTOF4-ROBOT)
(LEFTOF3-ROBOT)
(LEFTOF2-ROBOT)
(not (NOT-LEFTOF4-ROBOT))
(not (NOT-LEFTOF3-ROBOT))
(not (NOT-LEFTOF2-ROBOT))
(not (NOT-LEFTOF1-ROBOT))
)
)
(when
(and
(LEFTOF5-ROBOT)
)
(and
(LEFTOF4-ROBOT)
(not (NOT-LEFTOF4-ROBOT))
)
)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action MOVERIGHT-ROBOT
:parameters ()
:precondition
(and
(Flag3-)
)
:effect
(and
(when
(and
(Flag74-)
)
(and
(COLUMN5-ROBOT)
(not (NOT-COLUMN5-ROBOT))
)
)
(when
(and
(Flag71-)
)
(and
(COLUMN4-ROBOT)
(not (NOT-COLUMN4-ROBOT))
)
)
(when
(and
(Flag66-)
)
(and
(COLUMN3-ROBOT)
(not (NOT-COLUMN3-ROBOT))
)
)
(when
(and
(Flag61-)
)
(and
(COLUMN2-ROBOT)
(not (NOT-COLUMN2-ROBOT))
)
)
(when
(and
(Flag54-)
)
(and
(COLUMN1-ROBOT)
(not (NOT-COLUMN1-ROBOT))
)
)
(when
(and
(COLUMN4-ROBOT)
)
(and
(COLUMN5-ROBOT)
(NOT-COLUMN4-ROBOT)
(not (NOT-COLUMN5-ROBOT))
(not (COLUMN4-ROBOT))
)
)
(when
(and
(COLUMN3-ROBOT)
)
(and
(COLUMN4-ROBOT)
(NOT-COLUMN3-ROBOT)
(not (NOT-COLUMN4-ROBOT))
(not (COLUMN3-ROBOT))
)
)
(when
(and
(COLUMN2-ROBOT)
)
(and
(COLUMN3-ROBOT)
(NOT-COLUMN2-ROBOT)
(not (NOT-COLUMN3-ROBOT))
(not (COLUMN2-ROBOT))
)
)
(when
(and
(COLUMN1-ROBOT)
)
(and
(COLUMN2-ROBOT)
(NOT-COLUMN1-ROBOT)
(not (NOT-COLUMN2-ROBOT))
(not (COLUMN1-ROBOT))
)
)
(when
(and
(COLUMN0-ROBOT)
)
(and
(COLUMN1-ROBOT)
(NOT-COLUMN0-ROBOT)
(not (NOT-COLUMN1-ROBOT))
(not (COLUMN0-ROBOT))
)
)
(when
(and
(Flag47-)
)
(and
(LEFTOF6-ROBOT)
(NOT-LEFTOF5-ROBOT)
(not (NOT-LEFTOF6-ROBOT))
(not (LEFTOF5-ROBOT))
)
)
(when
(and
(Flag46-)
)
(and
(LEFTOF5-ROBOT)
(NOT-LEFTOF4-ROBOT)
(not (NOT-LEFTOF5-ROBOT))
(not (LEFTOF4-ROBOT))
)
)
(when
(and
(Flag45-)
)
(and
(LEFTOF4-ROBOT)
(NOT-LEFTOF3-ROBOT)
(not (NOT-LEFTOF4-ROBOT))
(not (LEFTOF3-ROBOT))
)
)
(when
(and
(Flag44-)
)
(and
(LEFTOF3-ROBOT)
(NOT-LEFTOF2-ROBOT)
(not (NOT-LEFTOF3-ROBOT))
(not (LEFTOF2-ROBOT))
)
)
(when
(and
(LEFTOF1-ROBOT)
)
(and
(LEFTOF2-ROBOT)
(NOT-LEFTOF1-ROBOT)
(not (NOT-LEFTOF2-ROBOT))
(not (LEFTOF1-ROBOT))
)
)
(when
(and
(Flag43-)
)
(and
(RIGHTOF5-ROBOT)
(not (NOT-RIGHTOF5-ROBOT))
)
)
(when
(and
(Flag42-)
)
(and
(RIGHTOF4-ROBOT)
(not (NOT-RIGHTOF4-ROBOT))
)
)
(when
(and
(Flag41-)
)
(and
(RIGHTOF3-ROBOT)
(not (NOT-RIGHTOF3-ROBOT))
)
)
(when
(and
(Flag40-)
)
(and
(RIGHTOF2-ROBOT)
(not (NOT-RIGHTOF2-ROBOT))
)
)
(when
(and
(Flag39-)
)
(and
(RIGHTOF1-ROBOT)
(not (NOT-RIGHTOF1-ROBOT))
)
)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action CHECKCONSISTENCYACTION
:parameters ()
:precondition
(and
(NOT-ERROR-)
(CHECKCONSISTENCY-)
)
:effect
(and
(when
(and
(BELOWOF2-ROBOT)
(ABOVEOF5-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF3-ROBOT)
(BELOWOF1-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF1-ROBOT)
(ABOVEOF5-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF5-ROBOT)
(LEFTOF3-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF1-ROBOT)
(ABOVEOF1-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF3-ROBOT)
(ABOVEOF4-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF2-ROBOT)
(ABOVEOF3-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF5-ROBOT)
(LEFTOF4-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF2-ROBOT)
(BELOWOF2-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF5-ROBOT)
(RIGHTOF5-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF2-ROBOT)
(RIGHTOF4-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF4-ROBOT)
(BELOWOF4-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF2-ROBOT)
(ABOVEOF4-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF1-ROBOT)
(RIGHTOF3-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF2-ROBOT)
(LEFTOF2-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF3-ROBOT)
(LEFTOF3-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF5-ROBOT)
(LEFTOF1-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF1-ROBOT)
(RIGHTOF2-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF5-ROBOT)
(LEFTOF2-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF3-ROBOT)
(ABOVEOF5-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF1-ROBOT)
(RIGHTOF1-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF5-ROBOT)
(BELOWOF4-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF1-ROBOT)
(RIGHTOF4-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF1-ROBOT)
(ABOVEOF2-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF1-ROBOT)
(ABOVEOF4-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF3-ROBOT)
(ABOVEOF3-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF5-ROBOT)
(ABOVEOF5-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF4-ROBOT)
(RIGHTOF4-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF2-ROBOT)
(RIGHTOF3-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF3-ROBOT)
(RIGHTOF4-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(NOT-CHECKCONSISTENCY-)
(not (CHECKCONSISTENCY-))
)
)
(:derived (Flag3-)(and (NOT-CHECKCONSISTENCY-)(NOT-ERROR-)(Flag2-)))

(:derived (Flag5-)(and (ABOVEOF2-ROBOT)))

(:derived (Flag5-)(and (ABOVEOF3-ROBOT)))

(:derived (Flag5-)(and (ABOVEOF4-ROBOT)))

(:derived (Flag5-)(and (ABOVEOF5-ROBOT)))

(:derived (Flag5-)(and (ABOVEOF1-ROBOT)))

(:derived (Flag6-)(and (ABOVEOF2-ROBOT)))

(:derived (Flag6-)(and (ABOVEOF3-ROBOT)))

(:derived (Flag6-)(and (ABOVEOF4-ROBOT)))

(:derived (Flag6-)(and (ABOVEOF5-ROBOT)))

(:derived (Flag7-)(and (ABOVEOF4-ROBOT)))

(:derived (Flag7-)(and (ABOVEOF3-ROBOT)))

(:derived (Flag7-)(and (ABOVEOF5-ROBOT)))

(:derived (Flag8-)(and (ABOVEOF4-ROBOT)))

(:derived (Flag8-)(and (ABOVEOF5-ROBOT)))

(:derived (Flag9-)(and (BELOWOF2-ROBOT)))

(:derived (Flag9-)(and (BELOWOF1-ROBOT)))

(:derived (Flag10-)(and (BELOWOF2-ROBOT)))

(:derived (Flag10-)(and (BELOWOF1-ROBOT)))

(:derived (Flag10-)(and (BELOWOF3-ROBOT)))

(:derived (Flag11-)(and (BELOWOF2-ROBOT)))

(:derived (Flag11-)(and (BELOWOF1-ROBOT)))

(:derived (Flag11-)(and (BELOWOF3-ROBOT)))

(:derived (Flag11-)(and (BELOWOF4-ROBOT)))

(:derived (Flag12-)(and (BELOWOF2-ROBOT)))

(:derived (Flag12-)(and (BELOWOF1-ROBOT)))

(:derived (Flag12-)(and (BELOWOF3-ROBOT)))

(:derived (Flag12-)(and (BELOWOF4-ROBOT)))

(:derived (Flag13-)(and (BELOWOF1-ROBOT)(ABOVEOF4-ROBOT)))

(:derived (Flag14-)(and (BELOWOF1-ROBOT)(ABOVEOF2-ROBOT)))

(:derived (Flag15-)(and (BELOWOF1-ROBOT)(ABOVEOF1-ROBOT)))

(:derived (Flag16-)(and (BELOWOF1-ROBOT)(ABOVEOF5-ROBOT)))

(:derived (Flag17-)(and (ABOVEOF3-ROBOT)(BELOWOF1-ROBOT)))

(:derived (Flag18-)(and (BELOWOF1-ROBOT)))

(:derived (Flag19-)(and (ABOVEOF2-ROBOT)(BELOWOF2-ROBOT)))

(:derived (Flag20-)(and (ABOVEOF4-ROBOT)(BELOWOF2-ROBOT)))

(:derived (Flag21-)(and (BELOWOF2-ROBOT)(ABOVEOF5-ROBOT)))

(:derived (Flag22-)(and (ABOVEOF3-ROBOT)(BELOWOF2-ROBOT)))

(:derived (Flag23-)(and (ABOVEOF1-ROBOT)(BELOWOF2-ROBOT)))

(:derived (Flag25-)(and (BELOWOF3-ROBOT)(ABOVEOF2-ROBOT)))

(:derived (Flag26-)(and (ABOVEOF3-ROBOT)(BELOWOF3-ROBOT)))

(:derived (Flag27-)(and (BELOWOF3-ROBOT)(ABOVEOF5-ROBOT)))

(:derived (Flag28-)(and (BELOWOF3-ROBOT)(ABOVEOF4-ROBOT)))

(:derived (Flag29-)(and (BELOWOF2-ROBOT)(ABOVEOF2-ROBOT)))

(:derived (Flag31-)(and (ABOVEOF3-ROBOT)(BELOWOF4-ROBOT)))

(:derived (Flag32-)(and (BELOWOF4-ROBOT)(ABOVEOF5-ROBOT)))

(:derived (Flag33-)(and (BELOWOF4-ROBOT)(ABOVEOF4-ROBOT)))

(:derived (Flag34-)(and (BELOWOF3-ROBOT)(ABOVEOF3-ROBOT)))

(:derived (Flag36-)(and (ABOVEOF4-ROBOT)(BELOWOF5-ROBOT)))

(:derived (Flag37-)(and (ABOVEOF5-ROBOT)(BELOWOF5-ROBOT)))

(:derived (Flag75-)(and (BELOWOF6-ROBOT)))

(:derived (Flag75-)(and (BELOWOF2-ROBOT)))

(:derived (Flag75-)(and (BELOWOF1-ROBOT)))

(:derived (Flag75-)(and (BELOWOF4-ROBOT)))

(:derived (Flag75-)(and (BELOWOF3-ROBOT)))

(:derived (Flag76-)(and (BELOWOF6-ROBOT)(ABOVEOF5-ROBOT)))

(:derived (Flag77-)(and (BELOWOF5-ROBOT)(ABOVEOF5-ROBOT)))

(:action MOVEDOWN-ROBOT
:parameters ()
:precondition
(and
(Flag4-)
)
:effect
(and
(when
(and
(Flag78-)
)
(and
(ROW4-ROBOT)
(not (NOT-ROW4-ROBOT))
)
)
(when
(and
(Flag38-)
)
(and
(ROW3-ROBOT)
(not (NOT-ROW3-ROBOT))
)
)
(when
(and
(Flag35-)
)
(and
(ROW2-ROBOT)
(not (NOT-ROW2-ROBOT))
)
)
(when
(and
(Flag30-)
)
(and
(ROW1-ROBOT)
(not (NOT-ROW1-ROBOT))
)
)
(when
(and
(Flag24-)
)
(and
(ROW0-ROBOT)
(not (NOT-ROW0-ROBOT))
)
)
(when
(and
(ROW5-ROBOT)
)
(and
(ROW4-ROBOT)
(NOT-ROW5-ROBOT)
(not (NOT-ROW4-ROBOT))
(not (ROW5-ROBOT))
)
)
(when
(and
(ROW4-ROBOT)
)
(and
(ROW3-ROBOT)
(NOT-ROW4-ROBOT)
(not (NOT-ROW3-ROBOT))
(not (ROW4-ROBOT))
)
)
(when
(and
(ROW3-ROBOT)
)
(and
(ROW2-ROBOT)
(NOT-ROW3-ROBOT)
(not (NOT-ROW2-ROBOT))
(not (ROW3-ROBOT))
)
)
(when
(and
(ROW2-ROBOT)
)
(and
(ROW1-ROBOT)
(NOT-ROW2-ROBOT)
(not (NOT-ROW1-ROBOT))
(not (ROW2-ROBOT))
)
)
(when
(and
(ROW1-ROBOT)
)
(and
(ROW0-ROBOT)
(NOT-ROW1-ROBOT)
(not (NOT-ROW0-ROBOT))
(not (ROW1-ROBOT))
)
)
(when
(and
(ABOVEOF5-ROBOT)
)
(and
(ABOVEOF4-ROBOT)
(NOT-ABOVEOF5-ROBOT)
(ABOVEOF3-ROBOT)
(NOT-ABOVEOF4-ROBOT)
(ABOVEOF2-ROBOT)
(NOT-ABOVEOF3-ROBOT)
(ABOVEOF1-ROBOT)
(NOT-ABOVEOF2-ROBOT)
(NOT-ABOVEOF1-ROBOT)
(not (ABOVEOF5-ROBOT))
)
)
(when
(and
(ABOVEOF4-ROBOT)
)
(and
(ABOVEOF3-ROBOT)
(NOT-ABOVEOF4-ROBOT)
(ABOVEOF2-ROBOT)
(NOT-ABOVEOF3-ROBOT)
(ABOVEOF1-ROBOT)
(NOT-ABOVEOF2-ROBOT)
(NOT-ABOVEOF1-ROBOT)
(not (ABOVEOF4-ROBOT))
)
)
(when
(and
(ABOVEOF3-ROBOT)
)
(and
(ABOVEOF2-ROBOT)
(NOT-ABOVEOF3-ROBOT)
(ABOVEOF1-ROBOT)
(NOT-ABOVEOF2-ROBOT)
(NOT-ABOVEOF1-ROBOT)
(not (ABOVEOF3-ROBOT))
)
)
(when
(and
(ABOVEOF2-ROBOT)
)
(and
(ABOVEOF1-ROBOT)
(NOT-ABOVEOF2-ROBOT)
(NOT-ABOVEOF1-ROBOT)
(not (ABOVEOF2-ROBOT))
)
)
(when
(and
(ABOVEOF1-ROBOT)
)
(and
(NOT-ABOVEOF1-ROBOT)
(not (ABOVEOF1-ROBOT))
)
)
(when
(and
(Flag75-)
)
(and
(BELOWOF5-ROBOT)
(not (NOT-BELOWOF5-ROBOT))
)
)
(when
(and
(BELOWOF5-ROBOT)
)
(and
(BELOWOF4-ROBOT)
(not (NOT-BELOWOF4-ROBOT))
)
)
(when
(and
(BELOWOF4-ROBOT)
)
(and
(BELOWOF3-ROBOT)
(not (NOT-BELOWOF4-ROBOT))
(not (NOT-BELOWOF3-ROBOT))
)
)
(when
(and
(BELOWOF3-ROBOT)
)
(and
(BELOWOF4-ROBOT)
(BELOWOF2-ROBOT)
(not (NOT-BELOWOF4-ROBOT))
(not (NOT-BELOWOF3-ROBOT))
(not (NOT-BELOWOF2-ROBOT))
)
)
(when
(and
(BELOWOF1-ROBOT)
)
(and
(BELOWOF4-ROBOT)
(BELOWOF3-ROBOT)
(BELOWOF2-ROBOT)
(not (NOT-BELOWOF4-ROBOT))
(not (NOT-BELOWOF3-ROBOT))
(not (NOT-BELOWOF2-ROBOT))
(not (NOT-BELOWOF1-ROBOT))
)
)
(when
(and
(BELOWOF2-ROBOT)
)
(and
(BELOWOF4-ROBOT)
(BELOWOF3-ROBOT)
(BELOWOF1-ROBOT)
(not (NOT-BELOWOF4-ROBOT))
(not (NOT-BELOWOF3-ROBOT))
(not (NOT-BELOWOF2-ROBOT))
(not (NOT-BELOWOF1-ROBOT))
)
)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action MOVEUP-ROBOT
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
)
:effect
(and
(when
(and
(Flag38-)
)
(and
(ROW5-ROBOT)
(not (NOT-ROW5-ROBOT))
)
)
(when
(and
(Flag35-)
)
(and
(ROW4-ROBOT)
(not (NOT-ROW4-ROBOT))
)
)
(when
(and
(Flag30-)
)
(and
(ROW3-ROBOT)
(not (NOT-ROW3-ROBOT))
)
)
(when
(and
(Flag24-)
)
(and
(ROW2-ROBOT)
(not (NOT-ROW2-ROBOT))
)
)
(when
(and
(Flag18-)
)
(and
(ROW1-ROBOT)
(not (NOT-ROW1-ROBOT))
)
)
(when
(and
(ROW4-ROBOT)
)
(and
(ROW5-ROBOT)
(NOT-ROW4-ROBOT)
(not (NOT-ROW5-ROBOT))
(not (ROW4-ROBOT))
)
)
(when
(and
(ROW3-ROBOT)
)
(and
(ROW4-ROBOT)
(NOT-ROW3-ROBOT)
(not (NOT-ROW4-ROBOT))
(not (ROW3-ROBOT))
)
)
(when
(and
(ROW2-ROBOT)
)
(and
(ROW3-ROBOT)
(NOT-ROW2-ROBOT)
(not (NOT-ROW3-ROBOT))
(not (ROW2-ROBOT))
)
)
(when
(and
(ROW1-ROBOT)
)
(and
(ROW2-ROBOT)
(NOT-ROW1-ROBOT)
(not (NOT-ROW2-ROBOT))
(not (ROW1-ROBOT))
)
)
(when
(and
(ROW0-ROBOT)
)
(and
(ROW1-ROBOT)
(NOT-ROW0-ROBOT)
(not (NOT-ROW1-ROBOT))
(not (ROW0-ROBOT))
)
)
(when
(and
(Flag12-)
)
(and
(BELOWOF6-ROBOT)
(NOT-BELOWOF5-ROBOT)
(not (NOT-BELOWOF6-ROBOT))
(not (BELOWOF5-ROBOT))
)
)
(when
(and
(Flag11-)
)
(and
(BELOWOF5-ROBOT)
(NOT-BELOWOF4-ROBOT)
(not (NOT-BELOWOF5-ROBOT))
(not (BELOWOF4-ROBOT))
)
)
(when
(and
(Flag10-)
)
(and
(BELOWOF4-ROBOT)
(NOT-BELOWOF3-ROBOT)
(not (NOT-BELOWOF4-ROBOT))
(not (BELOWOF3-ROBOT))
)
)
(when
(and
(Flag9-)
)
(and
(BELOWOF3-ROBOT)
(NOT-BELOWOF2-ROBOT)
(not (NOT-BELOWOF3-ROBOT))
(not (BELOWOF2-ROBOT))
)
)
(when
(and
(BELOWOF1-ROBOT)
)
(and
(BELOWOF2-ROBOT)
(NOT-BELOWOF1-ROBOT)
(not (NOT-BELOWOF2-ROBOT))
(not (BELOWOF1-ROBOT))
)
)
(when
(and
(Flag8-)
)
(and
(ABOVEOF5-ROBOT)
(not (NOT-ABOVEOF5-ROBOT))
)
)
(when
(and
(Flag7-)
)
(and
(ABOVEOF4-ROBOT)
(not (NOT-ABOVEOF4-ROBOT))
)
)
(when
(and
(Flag6-)
)
(and
(ABOVEOF3-ROBOT)
(not (NOT-ABOVEOF3-ROBOT))
)
)
(when
(and
(Flag5-)
)
(and
(ABOVEOF2-ROBOT)
(not (NOT-ABOVEOF2-ROBOT))
)
)
(ABOVEOF1-ROBOT)
(CHECKCONSISTENCY-)
(not (NOT-ABOVEOF1-ROBOT))
(not (NOT-CHECKCONSISTENCY-))
)
)
(:derived (Flag2-)(and (LEFTOF5-ROBOT)))

(:derived (Flag2-)(and (RIGHTOF2-ROBOT)))

(:derived (Flag4-)(and (NOT-CHECKCONSISTENCY-)(NOT-ERROR-)))

(:derived (Flag12-)(and (BELOWOF5-ROBOT)))

(:derived (Flag39-)(and (RIGHTOF2-ROBOT)))

(:derived (Flag40-)(and (RIGHTOF2-ROBOT)))

(:derived (Flag41-)(and (RIGHTOF2-ROBOT)))

(:derived (Flag47-)(and (LEFTOF5-ROBOT)))

(:derived (Flag75-)(and (BELOWOF5-ROBOT)))

(:derived (Flag79-)(and (LEFTOF5-ROBOT)))

)
