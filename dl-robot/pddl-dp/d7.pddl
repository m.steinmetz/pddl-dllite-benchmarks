(define (domain grounded-SIMPLE-ADL-ROBOT)
(:requirements
:strips
)
(:predicates
(Flag100-)
(Flag97-)
(Flag61-)
(Flag12-)
(Flag11-)
(Flag10-)
(Flag9-)
(Flag3-)
(Flag2-)
(ROW6-ROBOT)
(ROW5-ROBOT)
(ROW4-ROBOT)
(ROW3-ROBOT)
(ROW2-ROBOT)
(ROW1-ROBOT)
(BELOWOF7-ROBOT)
(NOT-BELOWOF6-ROBOT)
(BELOWOF5-ROBOT)
(BELOWOF4-ROBOT)
(BELOWOF3-ROBOT)
(BELOWOF2-ROBOT)
(ABOVEOF6-ROBOT)
(ABOVEOF5-ROBOT)
(ABOVEOF4-ROBOT)
(ABOVEOF3-ROBOT)
(ABOVEOF2-ROBOT)
(ABOVEOF1-ROBOT)
(CHECKCONSISTENCY-)
(ROW0-ROBOT)
(BELOWOF1-ROBOT)
(Flag102-)
(Flag101-)
(Flag95-)
(Flag94-)
(Flag93-)
(Flag91-)
(Flag90-)
(Flag89-)
(Flag87-)
(Flag86-)
(Flag85-)
(Flag84-)
(Flag83-)
(Flag81-)
(Flag80-)
(Flag79-)
(Flag78-)
(Flag77-)
(Flag76-)
(Flag74-)
(Flag73-)
(Flag72-)
(Flag71-)
(Flag70-)
(Flag69-)
(Flag68-)
(Flag67-)
(Flag66-)
(Flag65-)
(Flag64-)
(Flag63-)
(Flag62-)
(Flag60-)
(Flag59-)
(Flag58-)
(Flag57-)
(Flag56-)
(Flag55-)
(Flag54-)
(Flag53-)
(Flag52-)
(Flag4-)
(ERROR-)
(COLUMN5-ROBOT)
(COLUMN4-ROBOT)
(COLUMN3-ROBOT)
(COLUMN2-ROBOT)
(COLUMN1-ROBOT)
(COLUMN0-ROBOT)
(RIGHTOF5-ROBOT)
(RIGHTOF4-ROBOT)
(RIGHTOF3-ROBOT)
(RIGHTOF1-ROBOT)
(NOT-RIGHTOF2-ROBOT)
(RIGHTOF0-ROBOT)
(LEFTOF5-ROBOT)
(LEFTOF4-ROBOT)
(LEFTOF3-ROBOT)
(LEFTOF2-ROBOT)
(LEFTOF1-ROBOT)
(COLUMN6-ROBOT)
(LEFTOF7-ROBOT)
(NOT-LEFTOF6-ROBOT)
(RIGHTOF6-ROBOT)
(Flag103-)
(Flag98-)
(Flag96-)
(Flag92-)
(Flag88-)
(Flag82-)
(Flag75-)
(Flag50-)
(Flag49-)
(Flag47-)
(Flag46-)
(Flag44-)
(Flag43-)
(Flag42-)
(Flag40-)
(Flag39-)
(Flag38-)
(Flag37-)
(Flag36-)
(Flag34-)
(Flag33-)
(Flag32-)
(Flag31-)
(Flag30-)
(Flag28-)
(Flag27-)
(Flag26-)
(Flag25-)
(Flag24-)
(Flag23-)
(Flag22-)
(Flag21-)
(Flag20-)
(Flag19-)
(Flag18-)
(Flag17-)
(Flag16-)
(Flag15-)
(Flag14-)
(Flag13-)
(Flag8-)
(Flag7-)
(Flag6-)
(Flag5-)
(Flag1-)
(Flag99-)
(Flag51-)
(Flag48-)
(Flag45-)
(Flag41-)
(Flag35-)
(Flag29-)
(NOT-COLUMN6-ROBOT)
(NOT-COLUMN5-ROBOT)
(NOT-COLUMN4-ROBOT)
(NOT-COLUMN3-ROBOT)
(NOT-COLUMN2-ROBOT)
(NOT-COLUMN1-ROBOT)
(NOT-LEFTOF7-ROBOT)
(LEFTOF6-ROBOT)
(NOT-LEFTOF5-ROBOT)
(NOT-LEFTOF4-ROBOT)
(NOT-LEFTOF3-ROBOT)
(NOT-LEFTOF2-ROBOT)
(NOT-RIGHTOF6-ROBOT)
(NOT-RIGHTOF5-ROBOT)
(NOT-RIGHTOF4-ROBOT)
(NOT-RIGHTOF3-ROBOT)
(NOT-RIGHTOF1-ROBOT)
(NOT-CHECKCONSISTENCY-)
(NOT-COLUMN0-ROBOT)
(RIGHTOF2-ROBOT)
(NOT-RIGHTOF0-ROBOT)
(NOT-LEFTOF1-ROBOT)
(NOT-ERROR-)
(NOT-ROW5-ROBOT)
(NOT-ROW4-ROBOT)
(NOT-ROW3-ROBOT)
(NOT-ROW2-ROBOT)
(NOT-ROW1-ROBOT)
(NOT-ROW0-ROBOT)
(NOT-ABOVEOF5-ROBOT)
(NOT-ABOVEOF4-ROBOT)
(NOT-ABOVEOF3-ROBOT)
(NOT-ABOVEOF2-ROBOT)
(NOT-ABOVEOF1-ROBOT)
(NOT-BELOWOF5-ROBOT)
(NOT-BELOWOF4-ROBOT)
(NOT-BELOWOF3-ROBOT)
(NOT-BELOWOF2-ROBOT)
(NOT-BELOWOF1-ROBOT)
(NOT-ROW6-ROBOT)
(NOT-BELOWOF7-ROBOT)
(BELOWOF6-ROBOT)
(NOT-ABOVEOF6-ROBOT)
)
(:derived (Flag29-)(and (Flag16-)))

(:derived (Flag29-)(and (Flag17-)))

(:derived (Flag29-)(and (Flag18-)))

(:derived (Flag29-)(and (Flag19-)))

(:derived (Flag29-)(and (Flag20-)))

(:derived (Flag29-)(and (Flag21-)))

(:derived (Flag29-)(and (Flag22-)))

(:derived (Flag29-)(and (Flag23-)))

(:derived (Flag29-)(and (Flag24-)))

(:derived (Flag29-)(and (Flag25-)))

(:derived (Flag29-)(and (Flag26-)))

(:derived (Flag29-)(and (Flag27-)))

(:derived (Flag29-)(and (Flag28-)))

(:derived (Flag35-)(and (Flag16-)))

(:derived (Flag35-)(and (Flag21-)))

(:derived (Flag35-)(and (Flag18-)))

(:derived (Flag35-)(and (Flag30-)))

(:derived (Flag35-)(and (Flag20-)))

(:derived (Flag35-)(and (Flag26-)))

(:derived (Flag35-)(and (Flag31-)))

(:derived (Flag35-)(and (Flag22-)))

(:derived (Flag35-)(and (Flag23-)))

(:derived (Flag35-)(and (Flag24-)))

(:derived (Flag35-)(and (Flag32-)))

(:derived (Flag35-)(and (Flag25-)))

(:derived (Flag35-)(and (Flag33-)))

(:derived (Flag35-)(and (Flag27-)))

(:derived (Flag35-)(and (Flag28-)))

(:derived (Flag35-)(and (Flag34-)))

(:derived (Flag41-)(and (Flag16-)))

(:derived (Flag41-)(and (Flag21-)))

(:derived (Flag41-)(and (Flag36-)))

(:derived (Flag41-)(and (Flag30-)))

(:derived (Flag41-)(and (Flag18-)))

(:derived (Flag41-)(and (Flag20-)))

(:derived (Flag41-)(and (Flag37-)))

(:derived (Flag41-)(and (Flag38-)))

(:derived (Flag41-)(and (Flag31-)))

(:derived (Flag41-)(and (Flag23-)))

(:derived (Flag41-)(and (Flag32-)))

(:derived (Flag41-)(and (Flag25-)))

(:derived (Flag41-)(and (Flag26-)))

(:derived (Flag41-)(and (Flag39-)))

(:derived (Flag41-)(and (Flag27-)))

(:derived (Flag41-)(and (Flag40-)))

(:derived (Flag41-)(and (Flag28-)))

(:derived (Flag45-)(and (Flag16-)))

(:derived (Flag45-)(and (Flag21-)))

(:derived (Flag45-)(and (Flag42-)))

(:derived (Flag45-)(and (Flag18-)))

(:derived (Flag45-)(and (Flag30-)))

(:derived (Flag45-)(and (Flag37-)))

(:derived (Flag45-)(and (Flag38-)))

(:derived (Flag45-)(and (Flag31-)))

(:derived (Flag45-)(and (Flag23-)))

(:derived (Flag45-)(and (Flag32-)))

(:derived (Flag45-)(and (Flag26-)))

(:derived (Flag45-)(and (Flag43-)))

(:derived (Flag45-)(and (Flag27-)))

(:derived (Flag45-)(and (Flag40-)))

(:derived (Flag45-)(and (Flag28-)))

(:derived (Flag45-)(and (Flag44-)))

(:derived (Flag48-)(and (Flag46-)))

(:derived (Flag48-)(and (Flag18-)))

(:derived (Flag48-)(and (Flag30-)))

(:derived (Flag48-)(and (Flag37-)))

(:derived (Flag48-)(and (Flag38-)))

(:derived (Flag48-)(and (Flag21-)))

(:derived (Flag48-)(and (Flag31-)))

(:derived (Flag48-)(and (Flag47-)))

(:derived (Flag48-)(and (Flag26-)))

(:derived (Flag48-)(and (Flag43-)))

(:derived (Flag48-)(and (Flag27-)))

(:derived (Flag48-)(and (Flag44-)))

(:derived (Flag51-)(and (Flag49-)))

(:derived (Flag51-)(and (Flag50-)))

(:derived (Flag51-)(and (Flag30-)))

(:derived (Flag51-)(and (Flag37-)))

(:derived (Flag51-)(and (Flag26-)))

(:derived (Flag51-)(and (Flag27-)))

(:derived (Flag51-)(and (Flag44-)))

(:derived (Flag99-)(and (Flag16-)))

(:derived (Flag99-)(and (Flag19-)))

(:derived (Flag99-)(and (Flag20-)))

(:derived (Flag99-)(and (Flag98-)))

(:derived (Flag99-)(and (Flag23-)))

(:derived (Flag99-)(and (Flag24-)))

(:derived (Flag99-)(and (Flag26-)))

(:derived (Flag1-)(and (COLUMN2-ROBOT)(ROW1-ROBOT)(NOT-ERROR-)(NOT-CHECKCONSISTENCY-)))

(:derived (Flag3-)(and (RIGHTOF6-ROBOT)))

(:derived (Flag3-)(and (LEFTOF4-ROBOT)))

(:derived (Flag3-)(and (LEFTOF3-ROBOT)))

(:derived (Flag3-)(and (LEFTOF5-ROBOT)))

(:derived (Flag3-)(and (RIGHTOF1-ROBOT)))

(:derived (Flag3-)(and (RIGHTOF4-ROBOT)))

(:derived (Flag3-)(and (RIGHTOF0-ROBOT)))

(:derived (Flag3-)(and (LEFTOF7-ROBOT)))

(:derived (Flag3-)(and (LEFTOF2-ROBOT)))

(:derived (Flag3-)(and (RIGHTOF5-ROBOT)))

(:derived (Flag3-)(and (LEFTOF1-ROBOT)))

(:derived (Flag3-)(and (RIGHTOF3-ROBOT)))

(:derived (Flag5-)(and (LEFTOF1-ROBOT)))

(:derived (Flag5-)(and (LEFTOF2-ROBOT)))

(:derived (Flag6-)(and (LEFTOF1-ROBOT)))

(:derived (Flag6-)(and (LEFTOF3-ROBOT)))

(:derived (Flag6-)(and (LEFTOF2-ROBOT)))

(:derived (Flag7-)(and (LEFTOF1-ROBOT)))

(:derived (Flag7-)(and (LEFTOF3-ROBOT)))

(:derived (Flag7-)(and (LEFTOF4-ROBOT)))

(:derived (Flag7-)(and (LEFTOF2-ROBOT)))

(:derived (Flag8-)(and (LEFTOF5-ROBOT)))

(:derived (Flag8-)(and (LEFTOF1-ROBOT)))

(:derived (Flag8-)(and (LEFTOF3-ROBOT)))

(:derived (Flag8-)(and (LEFTOF4-ROBOT)))

(:derived (Flag8-)(and (LEFTOF2-ROBOT)))

(:derived (Flag9-)(and (LEFTOF2-ROBOT)))

(:derived (Flag9-)(and (LEFTOF1-ROBOT)))

(:derived (Flag9-)(and (LEFTOF5-ROBOT)))

(:derived (Flag9-)(and (LEFTOF3-ROBOT)))

(:derived (Flag9-)(and (LEFTOF4-ROBOT)))

(:derived (Flag10-)(and (LEFTOF2-ROBOT)))

(:derived (Flag10-)(and (LEFTOF7-ROBOT)))

(:derived (Flag10-)(and (LEFTOF1-ROBOT)))

(:derived (Flag10-)(and (LEFTOF5-ROBOT)))

(:derived (Flag10-)(and (LEFTOF3-ROBOT)))

(:derived (Flag10-)(and (LEFTOF4-ROBOT)))

(:derived (Flag11-)(and (RIGHTOF3-ROBOT)))

(:derived (Flag11-)(and (RIGHTOF5-ROBOT)))

(:derived (Flag11-)(and (RIGHTOF4-ROBOT)))

(:derived (Flag11-)(and (RIGHTOF1-ROBOT)))

(:derived (Flag11-)(and (RIGHTOF6-ROBOT)))

(:derived (Flag12-)(and (RIGHTOF6-ROBOT)))

(:derived (Flag12-)(and (RIGHTOF3-ROBOT)))

(:derived (Flag12-)(and (RIGHTOF4-ROBOT)))

(:derived (Flag12-)(and (RIGHTOF5-ROBOT)))

(:derived (Flag13-)(and (RIGHTOF3-ROBOT)))

(:derived (Flag13-)(and (RIGHTOF4-ROBOT)))

(:derived (Flag13-)(and (RIGHTOF5-ROBOT)))

(:derived (Flag13-)(and (RIGHTOF6-ROBOT)))

(:derived (Flag14-)(and (RIGHTOF4-ROBOT)))

(:derived (Flag14-)(and (RIGHTOF5-ROBOT)))

(:derived (Flag14-)(and (RIGHTOF6-ROBOT)))

(:derived (Flag15-)(and (RIGHTOF5-ROBOT)))

(:derived (Flag15-)(and (RIGHTOF6-ROBOT)))

(:derived (Flag16-)(and (LEFTOF1-ROBOT)(RIGHTOF4-ROBOT)))

(:derived (Flag17-)(and (LEFTOF2-ROBOT)(RIGHTOF1-ROBOT)))

(:derived (Flag18-)(and (RIGHTOF5-ROBOT)(LEFTOF2-ROBOT)))

(:derived (Flag19-)(and (LEFTOF1-ROBOT)(RIGHTOF1-ROBOT)))

(:derived (Flag20-)(and (RIGHTOF3-ROBOT)(LEFTOF1-ROBOT)))

(:derived (Flag21-)(and (RIGHTOF5-ROBOT)(LEFTOF1-ROBOT)))

(:derived (Flag22-)(and (RIGHTOF2-ROBOT)(LEFTOF2-ROBOT)))

(:derived (Flag23-)(and (LEFTOF1-ROBOT)(RIGHTOF5-ROBOT)))

(:derived (Flag24-)(and (LEFTOF1-ROBOT)(RIGHTOF2-ROBOT)))

(:derived (Flag25-)(and (RIGHTOF3-ROBOT)(LEFTOF2-ROBOT)))

(:derived (Flag26-)(and (LEFTOF1-ROBOT)(RIGHTOF6-ROBOT)))

(:derived (Flag27-)(and (LEFTOF2-ROBOT)(RIGHTOF6-ROBOT)))

(:derived (Flag28-)(and (LEFTOF2-ROBOT)(RIGHTOF4-ROBOT)))

(:derived (Flag30-)(and (LEFTOF3-ROBOT)(RIGHTOF6-ROBOT)))

(:derived (Flag31-)(and (LEFTOF3-ROBOT)(RIGHTOF5-ROBOT)))

(:derived (Flag32-)(and (LEFTOF3-ROBOT)(RIGHTOF4-ROBOT)))

(:derived (Flag33-)(and (RIGHTOF3-ROBOT)(LEFTOF3-ROBOT)))

(:derived (Flag34-)(and (LEFTOF3-ROBOT)(RIGHTOF2-ROBOT)))

(:derived (Flag36-)(and (LEFTOF3-ROBOT)(RIGHTOF3-ROBOT)))

(:derived (Flag37-)(and (LEFTOF4-ROBOT)(RIGHTOF6-ROBOT)))

(:derived (Flag38-)(and (LEFTOF4-ROBOT)(RIGHTOF5-ROBOT)))

(:derived (Flag39-)(and (RIGHTOF3-ROBOT)(LEFTOF4-ROBOT)))

(:derived (Flag40-)(and (LEFTOF4-ROBOT)(RIGHTOF4-ROBOT)))

(:derived (Flag42-)(and (RIGHTOF4-ROBOT)(LEFTOF5-ROBOT)))

(:derived (Flag43-)(and (RIGHTOF5-ROBOT)(LEFTOF5-ROBOT)))

(:derived (Flag44-)(and (LEFTOF5-ROBOT)(RIGHTOF6-ROBOT)))

(:derived (Flag46-)(and (RIGHTOF6-ROBOT)(LEFTOF6-ROBOT)))

(:derived (Flag47-)(and (RIGHTOF5-ROBOT)(LEFTOF6-ROBOT)))

(:derived (Flag49-)(and (LEFTOF6-ROBOT)(RIGHTOF6-ROBOT)))

(:derived (Flag50-)(and (LEFTOF7-ROBOT)(RIGHTOF6-ROBOT)))

(:derived (Flag68-)(and (Flag62-)))

(:derived (Flag68-)(and (Flag63-)))

(:derived (Flag68-)(and (Flag64-)))

(:derived (Flag68-)(and (Flag65-)))

(:derived (Flag68-)(and (Flag66-)))

(:derived (Flag68-)(and (Flag67-)))

(:derived (Flag75-)(and (Flag63-)))

(:derived (Flag75-)(and (Flag69-)))

(:derived (Flag75-)(and (Flag70-)))

(:derived (Flag75-)(and (Flag62-)))

(:derived (Flag75-)(and (Flag65-)))

(:derived (Flag75-)(and (Flag64-)))

(:derived (Flag75-)(and (Flag66-)))

(:derived (Flag75-)(and (Flag71-)))

(:derived (Flag75-)(and (Flag72-)))

(:derived (Flag75-)(and (Flag67-)))

(:derived (Flag75-)(and (Flag73-)))

(:derived (Flag75-)(and (Flag74-)))

(:derived (Flag82-)(and (Flag63-)))

(:derived (Flag82-)(and (Flag76-)))

(:derived (Flag82-)(and (Flag77-)))

(:derived (Flag82-)(and (Flag78-)))

(:derived (Flag82-)(and (Flag79-)))

(:derived (Flag82-)(and (Flag64-)))

(:derived (Flag82-)(and (Flag66-)))

(:derived (Flag82-)(and (Flag80-)))

(:derived (Flag82-)(and (Flag67-)))

(:derived (Flag82-)(and (Flag69-)))

(:derived (Flag82-)(and (Flag72-)))

(:derived (Flag82-)(and (Flag81-)))

(:derived (Flag82-)(and (Flag71-)))

(:derived (Flag82-)(and (Flag73-)))

(:derived (Flag82-)(and (Flag62-)))

(:derived (Flag88-)(and (Flag67-)))

(:derived (Flag88-)(and (Flag77-)))

(:derived (Flag88-)(and (Flag62-)))

(:derived (Flag88-)(and (Flag66-)))

(:derived (Flag88-)(and (Flag64-)))

(:derived (Flag88-)(and (Flag83-)))

(:derived (Flag88-)(and (Flag84-)))

(:derived (Flag88-)(and (Flag80-)))

(:derived (Flag88-)(and (Flag85-)))

(:derived (Flag88-)(and (Flag86-)))

(:derived (Flag88-)(and (Flag72-)))

(:derived (Flag88-)(and (Flag71-)))

(:derived (Flag88-)(and (Flag79-)))

(:derived (Flag88-)(and (Flag69-)))

(:derived (Flag88-)(and (Flag73-)))

(:derived (Flag88-)(and (Flag87-)))

(:derived (Flag92-)(and (Flag62-)))

(:derived (Flag92-)(and (Flag77-)))

(:derived (Flag92-)(and (Flag89-)))

(:derived (Flag92-)(and (Flag79-)))

(:derived (Flag92-)(and (Flag90-)))

(:derived (Flag92-)(and (Flag72-)))

(:derived (Flag92-)(and (Flag64-)))

(:derived (Flag92-)(and (Flag66-)))

(:derived (Flag92-)(and (Flag80-)))

(:derived (Flag92-)(and (Flag85-)))

(:derived (Flag92-)(and (Flag86-)))

(:derived (Flag92-)(and (Flag91-)))

(:derived (Flag92-)(and (Flag71-)))

(:derived (Flag92-)(and (Flag69-)))

(:derived (Flag92-)(and (Flag84-)))

(:derived (Flag96-)(and (Flag77-)))

(:derived (Flag96-)(and (Flag79-)))

(:derived (Flag96-)(and (Flag64-)))

(:derived (Flag96-)(and (Flag72-)))

(:derived (Flag96-)(and (Flag93-)))

(:derived (Flag96-)(and (Flag66-)))

(:derived (Flag96-)(and (Flag94-)))

(:derived (Flag96-)(and (Flag95-)))

(:derived (Flag96-)(and (Flag91-)))

(:derived (Flag96-)(and (Flag85-)))

(:derived (Flag96-)(and (Flag69-)))

(:derived (Flag96-)(and (Flag84-)))

(:derived (Flag97-)(and (RIGHTOF3-ROBOT)))

(:derived (Flag97-)(and (RIGHTOF5-ROBOT)))

(:derived (Flag97-)(and (RIGHTOF0-ROBOT)))

(:derived (Flag97-)(and (RIGHTOF4-ROBOT)))

(:derived (Flag97-)(and (RIGHTOF1-ROBOT)))

(:derived (Flag97-)(and (RIGHTOF6-ROBOT)))

(:derived (Flag98-)(and (RIGHTOF0-ROBOT)(LEFTOF1-ROBOT)))

(:derived (Flag103-)(and (Flag69-)))

(:derived (Flag103-)(and (Flag101-)))

(:derived (Flag103-)(and (Flag64-)))

(:derived (Flag103-)(and (Flag77-)))

(:derived (Flag103-)(and (Flag91-)))

(:derived (Flag103-)(and (Flag85-)))

(:derived (Flag103-)(and (Flag102-)))

(:action MOVERIGHT-ROBOT
:parameters ()
:precondition
(and
(Flag4-)
)
:effect
(and
(when
(and
(Flag48-)
)
(and
(COLUMN6-ROBOT)
(not (NOT-COLUMN6-ROBOT))
)
)
(when
(and
(Flag45-)
)
(and
(COLUMN5-ROBOT)
(not (NOT-COLUMN5-ROBOT))
)
)
(when
(and
(Flag41-)
)
(and
(COLUMN4-ROBOT)
(not (NOT-COLUMN4-ROBOT))
)
)
(when
(and
(Flag35-)
)
(and
(COLUMN3-ROBOT)
(not (NOT-COLUMN3-ROBOT))
)
)
(when
(and
(Flag29-)
)
(and
(COLUMN2-ROBOT)
(not (NOT-COLUMN2-ROBOT))
)
)
(when
(and
(Flag99-)
)
(and
(COLUMN1-ROBOT)
(not (NOT-COLUMN1-ROBOT))
)
)
(when
(and
(COLUMN5-ROBOT)
)
(and
(COLUMN6-ROBOT)
(NOT-COLUMN5-ROBOT)
(not (NOT-COLUMN6-ROBOT))
(not (COLUMN5-ROBOT))
)
)
(when
(and
(COLUMN4-ROBOT)
)
(and
(COLUMN5-ROBOT)
(NOT-COLUMN4-ROBOT)
(not (NOT-COLUMN5-ROBOT))
(not (COLUMN4-ROBOT))
)
)
(when
(and
(COLUMN3-ROBOT)
)
(and
(COLUMN4-ROBOT)
(NOT-COLUMN3-ROBOT)
(not (NOT-COLUMN4-ROBOT))
(not (COLUMN3-ROBOT))
)
)
(when
(and
(COLUMN2-ROBOT)
)
(and
(COLUMN3-ROBOT)
(NOT-COLUMN2-ROBOT)
(not (NOT-COLUMN3-ROBOT))
(not (COLUMN2-ROBOT))
)
)
(when
(and
(COLUMN1-ROBOT)
)
(and
(COLUMN2-ROBOT)
(NOT-COLUMN1-ROBOT)
(not (NOT-COLUMN2-ROBOT))
(not (COLUMN1-ROBOT))
)
)
(when
(and
(COLUMN0-ROBOT)
)
(and
(COLUMN1-ROBOT)
(NOT-COLUMN0-ROBOT)
(not (NOT-COLUMN1-ROBOT))
(not (COLUMN0-ROBOT))
)
)
(when
(and
(LEFTOF4-ROBOT)
)
(and
(LEFTOF7-ROBOT)
(NOT-LEFTOF6-ROBOT)
(LEFTOF6-ROBOT)
(NOT-LEFTOF5-ROBOT)
(LEFTOF5-ROBOT)
(NOT-LEFTOF4-ROBOT)
(not (NOT-LEFTOF7-ROBOT))
(not (LEFTOF4-ROBOT))
)
)
(when
(and
(LEFTOF3-ROBOT)
)
(and
(LEFTOF7-ROBOT)
(NOT-LEFTOF6-ROBOT)
(LEFTOF6-ROBOT)
(NOT-LEFTOF5-ROBOT)
(LEFTOF5-ROBOT)
(NOT-LEFTOF4-ROBOT)
(LEFTOF4-ROBOT)
(NOT-LEFTOF3-ROBOT)
(not (NOT-LEFTOF7-ROBOT))
(not (LEFTOF3-ROBOT))
)
)
(when
(and
(LEFTOF5-ROBOT)
)
(and
(LEFTOF7-ROBOT)
(NOT-LEFTOF6-ROBOT)
(LEFTOF6-ROBOT)
(NOT-LEFTOF5-ROBOT)
(not (NOT-LEFTOF7-ROBOT))
(not (LEFTOF5-ROBOT))
)
)
(when
(and
(LEFTOF6-ROBOT)
)
(and
(LEFTOF7-ROBOT)
(NOT-LEFTOF6-ROBOT)
(not (NOT-LEFTOF7-ROBOT))
(not (LEFTOF6-ROBOT))
)
)
(when
(and
(LEFTOF1-ROBOT)
)
(and
(LEFTOF7-ROBOT)
(NOT-LEFTOF6-ROBOT)
(LEFTOF6-ROBOT)
(NOT-LEFTOF5-ROBOT)
(LEFTOF5-ROBOT)
(NOT-LEFTOF4-ROBOT)
(LEFTOF4-ROBOT)
(NOT-LEFTOF3-ROBOT)
(LEFTOF3-ROBOT)
(NOT-LEFTOF2-ROBOT)
(LEFTOF2-ROBOT)
(NOT-LEFTOF1-ROBOT)
(not (NOT-LEFTOF7-ROBOT))
(not (LEFTOF1-ROBOT))
)
)
(when
(and
(LEFTOF2-ROBOT)
)
(and
(LEFTOF7-ROBOT)
(NOT-LEFTOF6-ROBOT)
(LEFTOF6-ROBOT)
(NOT-LEFTOF5-ROBOT)
(LEFTOF5-ROBOT)
(NOT-LEFTOF4-ROBOT)
(LEFTOF4-ROBOT)
(NOT-LEFTOF3-ROBOT)
(LEFTOF3-ROBOT)
(NOT-LEFTOF2-ROBOT)
(not (NOT-LEFTOF7-ROBOT))
(not (LEFTOF2-ROBOT))
)
)
(when
(and
(RIGHTOF6-ROBOT)
)
(and
(RIGHTOF5-ROBOT)
(RIGHTOF4-ROBOT)
(RIGHTOF3-ROBOT)
(RIGHTOF2-ROBOT)
(not (NOT-RIGHTOF6-ROBOT))
(not (NOT-RIGHTOF5-ROBOT))
(not (NOT-RIGHTOF4-ROBOT))
(not (NOT-RIGHTOF3-ROBOT))
(not (NOT-RIGHTOF2-ROBOT))
)
)
(when
(and
(RIGHTOF5-ROBOT)
)
(and
(RIGHTOF6-ROBOT)
(RIGHTOF4-ROBOT)
(RIGHTOF3-ROBOT)
(RIGHTOF2-ROBOT)
(not (NOT-RIGHTOF6-ROBOT))
(not (NOT-RIGHTOF5-ROBOT))
(not (NOT-RIGHTOF4-ROBOT))
(not (NOT-RIGHTOF3-ROBOT))
(not (NOT-RIGHTOF2-ROBOT))
)
)
(when
(and
(RIGHTOF4-ROBOT)
)
(and
(RIGHTOF5-ROBOT)
(RIGHTOF3-ROBOT)
(RIGHTOF2-ROBOT)
(not (NOT-RIGHTOF5-ROBOT))
(not (NOT-RIGHTOF4-ROBOT))
(not (NOT-RIGHTOF3-ROBOT))
(not (NOT-RIGHTOF2-ROBOT))
)
)
(when
(and
(RIGHTOF3-ROBOT)
)
(and
(RIGHTOF4-ROBOT)
(RIGHTOF2-ROBOT)
(not (NOT-RIGHTOF4-ROBOT))
(not (NOT-RIGHTOF3-ROBOT))
(not (NOT-RIGHTOF2-ROBOT))
)
)
(when
(and
(RIGHTOF2-ROBOT)
)
(and
(RIGHTOF3-ROBOT)
(not (NOT-RIGHTOF3-ROBOT))
(not (NOT-RIGHTOF2-ROBOT))
)
)
(when
(and
(RIGHTOF1-ROBOT)
)
(and
(RIGHTOF2-ROBOT)
(not (NOT-RIGHTOF2-ROBOT))
)
)
(when
(and
(Flag97-)
)
(and
(RIGHTOF1-ROBOT)
(not (NOT-RIGHTOF1-ROBOT))
)
)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action MOVELEFT-ROBOT
:parameters ()
:precondition
(and
(Flag4-)
)
:effect
(and
(when
(and
(Flag51-)
)
(and
(COLUMN5-ROBOT)
(not (NOT-COLUMN5-ROBOT))
)
)
(when
(and
(Flag48-)
)
(and
(COLUMN4-ROBOT)
(not (NOT-COLUMN4-ROBOT))
)
)
(when
(and
(Flag45-)
)
(and
(COLUMN3-ROBOT)
(not (NOT-COLUMN3-ROBOT))
)
)
(when
(and
(Flag41-)
)
(and
(COLUMN2-ROBOT)
(not (NOT-COLUMN2-ROBOT))
)
)
(when
(and
(Flag35-)
)
(and
(COLUMN1-ROBOT)
(not (NOT-COLUMN1-ROBOT))
)
)
(when
(and
(Flag29-)
)
(and
(COLUMN0-ROBOT)
(not (NOT-COLUMN0-ROBOT))
)
)
(when
(and
(COLUMN6-ROBOT)
)
(and
(COLUMN5-ROBOT)
(NOT-COLUMN6-ROBOT)
(not (NOT-COLUMN5-ROBOT))
(not (COLUMN6-ROBOT))
)
)
(when
(and
(COLUMN5-ROBOT)
)
(and
(COLUMN4-ROBOT)
(NOT-COLUMN5-ROBOT)
(not (NOT-COLUMN4-ROBOT))
(not (COLUMN5-ROBOT))
)
)
(when
(and
(COLUMN4-ROBOT)
)
(and
(COLUMN3-ROBOT)
(NOT-COLUMN4-ROBOT)
(not (NOT-COLUMN3-ROBOT))
(not (COLUMN4-ROBOT))
)
)
(when
(and
(COLUMN3-ROBOT)
)
(and
(COLUMN2-ROBOT)
(NOT-COLUMN3-ROBOT)
(not (NOT-COLUMN2-ROBOT))
(not (COLUMN3-ROBOT))
)
)
(when
(and
(COLUMN2-ROBOT)
)
(and
(COLUMN1-ROBOT)
(NOT-COLUMN2-ROBOT)
(not (NOT-COLUMN1-ROBOT))
(not (COLUMN2-ROBOT))
)
)
(when
(and
(COLUMN1-ROBOT)
)
(and
(COLUMN0-ROBOT)
(NOT-COLUMN1-ROBOT)
(not (NOT-COLUMN0-ROBOT))
(not (COLUMN1-ROBOT))
)
)
(when
(and
(RIGHTOF6-ROBOT)
)
(and
(RIGHTOF5-ROBOT)
(NOT-RIGHTOF6-ROBOT)
(not (NOT-RIGHTOF5-ROBOT))
(not (RIGHTOF6-ROBOT))
)
)
(when
(and
(Flag15-)
)
(and
(RIGHTOF4-ROBOT)
(NOT-RIGHTOF5-ROBOT)
(not (NOT-RIGHTOF4-ROBOT))
(not (RIGHTOF5-ROBOT))
)
)
(when
(and
(Flag14-)
)
(and
(RIGHTOF3-ROBOT)
(NOT-RIGHTOF4-ROBOT)
(not (NOT-RIGHTOF3-ROBOT))
(not (RIGHTOF4-ROBOT))
)
)
(when
(and
(Flag13-)
)
(and
(RIGHTOF2-ROBOT)
(NOT-RIGHTOF3-ROBOT)
(not (NOT-RIGHTOF2-ROBOT))
(not (RIGHTOF3-ROBOT))
)
)
(when
(and
(Flag12-)
)
(and
(RIGHTOF1-ROBOT)
(NOT-RIGHTOF2-ROBOT)
(not (NOT-RIGHTOF1-ROBOT))
(not (RIGHTOF2-ROBOT))
)
)
(when
(and
(Flag11-)
)
(and
(RIGHTOF0-ROBOT)
(NOT-RIGHTOF1-ROBOT)
(not (NOT-RIGHTOF0-ROBOT))
(not (RIGHTOF1-ROBOT))
)
)
(when
(and
(Flag10-)
)
(and
(LEFTOF6-ROBOT)
(not (NOT-LEFTOF6-ROBOT))
)
)
(when
(and
(Flag9-)
)
(and
(LEFTOF5-ROBOT)
(not (NOT-LEFTOF5-ROBOT))
)
)
(when
(and
(Flag8-)
)
(and
(LEFTOF4-ROBOT)
(not (NOT-LEFTOF4-ROBOT))
)
)
(when
(and
(Flag7-)
)
(and
(LEFTOF3-ROBOT)
(not (NOT-LEFTOF3-ROBOT))
)
)
(when
(and
(Flag6-)
)
(and
(LEFTOF2-ROBOT)
(not (NOT-LEFTOF2-ROBOT))
)
)
(when
(and
(Flag5-)
)
(and
(LEFTOF1-ROBOT)
(not (NOT-LEFTOF1-ROBOT))
)
)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action CHECKCONSISTENCYACTION
:parameters ()
:precondition
(and
(NOT-ERROR-)
(CHECKCONSISTENCY-)
)
:effect
(and
(when
(and
(BELOWOF2-ROBOT)
(ABOVEOF5-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF3-ROBOT)
(ABOVEOF4-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF6-ROBOT)
(LEFTOF2-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF1-ROBOT)
(ABOVEOF3-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF2-ROBOT)
(RIGHTOF5-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF6-ROBOT)
(BELOWOF1-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF3-ROBOT)
(ABOVEOF5-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF6-ROBOT)
(BELOWOF3-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF2-ROBOT)
(BELOWOF2-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF6-ROBOT)
(BELOWOF2-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF2-ROBOT)
(LEFTOF2-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF6-ROBOT)
(LEFTOF1-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF5-ROBOT)
(BELOWOF1-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF5-ROBOT)
(LEFTOF1-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF3-ROBOT)
(LEFTOF1-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF4-ROBOT)
(RIGHTOF5-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF4-ROBOT)
(LEFTOF2-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF3-ROBOT)
(LEFTOF3-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF1-ROBOT)
(ABOVEOF4-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF6-ROBOT)
(BELOWOF4-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF1-ROBOT)
(RIGHTOF4-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF6-ROBOT)
(LEFTOF6-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF1-ROBOT)
(RIGHTOF1-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF3-ROBOT)
(RIGHTOF4-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF4-ROBOT)
(RIGHTOF6-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF2-ROBOT)
(RIGHTOF3-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF2-ROBOT)
(BELOWOF1-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF4-ROBOT)
(BELOWOF4-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF2-ROBOT)
(ABOVEOF4-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF6-ROBOT)
(BELOWOF5-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF1-ROBOT)
(ABOVEOF1-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF1-ROBOT)
(RIGHTOF2-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF5-ROBOT)
(RIGHTOF6-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF6-ROBOT)
(BELOWOF6-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF6-ROBOT)
(LEFTOF3-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF5-ROBOT)
(RIGHTOF5-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF2-ROBOT)
(ABOVEOF3-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF4-ROBOT)
(LEFTOF4-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF5-ROBOT)
(LEFTOF3-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF3-ROBOT)
(ABOVEOF3-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF5-ROBOT)
(BELOWOF4-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF5-ROBOT)
(BELOWOF5-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(NOT-CHECKCONSISTENCY-)
(not (CHECKCONSISTENCY-))
)
)
(:derived (Flag4-)(and (NOT-CHECKCONSISTENCY-)(NOT-ERROR-)(Flag3-)))

(:derived (Flag52-)(and (ABOVEOF6-ROBOT)))

(:derived (Flag52-)(and (ABOVEOF1-ROBOT)))

(:derived (Flag52-)(and (ABOVEOF2-ROBOT)))

(:derived (Flag52-)(and (ABOVEOF4-ROBOT)))

(:derived (Flag52-)(and (ABOVEOF3-ROBOT)))

(:derived (Flag52-)(and (ABOVEOF5-ROBOT)))

(:derived (Flag53-)(and (ABOVEOF2-ROBOT)))

(:derived (Flag53-)(and (ABOVEOF3-ROBOT)))

(:derived (Flag53-)(and (ABOVEOF4-ROBOT)))

(:derived (Flag53-)(and (ABOVEOF5-ROBOT)))

(:derived (Flag53-)(and (ABOVEOF6-ROBOT)))

(:derived (Flag54-)(and (ABOVEOF4-ROBOT)))

(:derived (Flag54-)(and (ABOVEOF3-ROBOT)))

(:derived (Flag54-)(and (ABOVEOF5-ROBOT)))

(:derived (Flag54-)(and (ABOVEOF6-ROBOT)))

(:derived (Flag55-)(and (ABOVEOF4-ROBOT)))

(:derived (Flag55-)(and (ABOVEOF5-ROBOT)))

(:derived (Flag55-)(and (ABOVEOF6-ROBOT)))

(:derived (Flag56-)(and (ABOVEOF5-ROBOT)))

(:derived (Flag56-)(and (ABOVEOF6-ROBOT)))

(:derived (Flag57-)(and (BELOWOF2-ROBOT)))

(:derived (Flag57-)(and (BELOWOF1-ROBOT)))

(:derived (Flag58-)(and (BELOWOF2-ROBOT)))

(:derived (Flag58-)(and (BELOWOF1-ROBOT)))

(:derived (Flag58-)(and (BELOWOF3-ROBOT)))

(:derived (Flag59-)(and (BELOWOF2-ROBOT)))

(:derived (Flag59-)(and (BELOWOF1-ROBOT)))

(:derived (Flag59-)(and (BELOWOF3-ROBOT)))

(:derived (Flag59-)(and (BELOWOF4-ROBOT)))

(:derived (Flag60-)(and (BELOWOF2-ROBOT)))

(:derived (Flag60-)(and (BELOWOF1-ROBOT)))

(:derived (Flag60-)(and (BELOWOF3-ROBOT)))

(:derived (Flag60-)(and (BELOWOF4-ROBOT)))

(:derived (Flag60-)(and (BELOWOF5-ROBOT)))

(:derived (Flag61-)(and (BELOWOF5-ROBOT)))

(:derived (Flag61-)(and (BELOWOF2-ROBOT)))

(:derived (Flag61-)(and (BELOWOF1-ROBOT)))

(:derived (Flag61-)(and (BELOWOF4-ROBOT)))

(:derived (Flag61-)(and (BELOWOF3-ROBOT)))

(:derived (Flag62-)(and (BELOWOF1-ROBOT)(ABOVEOF4-ROBOT)))

(:derived (Flag63-)(and (BELOWOF1-ROBOT)(ABOVEOF2-ROBOT)))

(:derived (Flag64-)(and (ABOVEOF6-ROBOT)(BELOWOF1-ROBOT)))

(:derived (Flag65-)(and (BELOWOF1-ROBOT)(ABOVEOF1-ROBOT)))

(:derived (Flag66-)(and (BELOWOF1-ROBOT)(ABOVEOF5-ROBOT)))

(:derived (Flag67-)(and (ABOVEOF3-ROBOT)(BELOWOF1-ROBOT)))

(:derived (Flag68-)(and (BELOWOF1-ROBOT)))

(:derived (Flag69-)(and (ABOVEOF6-ROBOT)(BELOWOF2-ROBOT)))

(:derived (Flag70-)(and (ABOVEOF2-ROBOT)(BELOWOF2-ROBOT)))

(:derived (Flag71-)(and (ABOVEOF4-ROBOT)(BELOWOF2-ROBOT)))

(:derived (Flag72-)(and (BELOWOF2-ROBOT)(ABOVEOF5-ROBOT)))

(:derived (Flag73-)(and (ABOVEOF3-ROBOT)(BELOWOF2-ROBOT)))

(:derived (Flag74-)(and (ABOVEOF1-ROBOT)(BELOWOF2-ROBOT)))

(:derived (Flag76-)(and (BELOWOF3-ROBOT)(ABOVEOF2-ROBOT)))

(:derived (Flag77-)(and (BELOWOF3-ROBOT)(ABOVEOF6-ROBOT)))

(:derived (Flag78-)(and (ABOVEOF3-ROBOT)(BELOWOF3-ROBOT)))

(:derived (Flag79-)(and (BELOWOF3-ROBOT)(ABOVEOF5-ROBOT)))

(:derived (Flag80-)(and (BELOWOF3-ROBOT)(ABOVEOF4-ROBOT)))

(:derived (Flag81-)(and (BELOWOF2-ROBOT)(ABOVEOF2-ROBOT)))

(:derived (Flag83-)(and (ABOVEOF3-ROBOT)(BELOWOF4-ROBOT)))

(:derived (Flag84-)(and (BELOWOF4-ROBOT)(ABOVEOF5-ROBOT)))

(:derived (Flag85-)(and (ABOVEOF6-ROBOT)(BELOWOF4-ROBOT)))

(:derived (Flag86-)(and (BELOWOF4-ROBOT)(ABOVEOF4-ROBOT)))

(:derived (Flag87-)(and (BELOWOF3-ROBOT)(ABOVEOF3-ROBOT)))

(:derived (Flag89-)(and (ABOVEOF4-ROBOT)(BELOWOF5-ROBOT)))

(:derived (Flag90-)(and (ABOVEOF5-ROBOT)(BELOWOF5-ROBOT)))

(:derived (Flag91-)(and (ABOVEOF6-ROBOT)(BELOWOF5-ROBOT)))

(:derived (Flag93-)(and (BELOWOF6-ROBOT)(ABOVEOF6-ROBOT)))

(:derived (Flag94-)(and (BELOWOF5-ROBOT)(ABOVEOF5-ROBOT)))

(:derived (Flag95-)(and (BELOWOF6-ROBOT)(ABOVEOF5-ROBOT)))

(:derived (Flag100-)(and (BELOWOF5-ROBOT)))

(:derived (Flag100-)(and (BELOWOF2-ROBOT)))

(:derived (Flag100-)(and (BELOWOF1-ROBOT)))

(:derived (Flag100-)(and (BELOWOF4-ROBOT)))

(:derived (Flag100-)(and (BELOWOF7-ROBOT)))

(:derived (Flag100-)(and (BELOWOF3-ROBOT)))

(:derived (Flag101-)(and (ABOVEOF6-ROBOT)(BELOWOF7-ROBOT)))

(:derived (Flag102-)(and (ABOVEOF6-ROBOT)(BELOWOF6-ROBOT)))

(:action MOVEDOWN-ROBOT
:parameters ()
:precondition
(and
(Flag2-)
)
:effect
(and
(when
(and
(Flag103-)
)
(and
(ROW5-ROBOT)
(not (NOT-ROW5-ROBOT))
)
)
(when
(and
(Flag96-)
)
(and
(ROW4-ROBOT)
(not (NOT-ROW4-ROBOT))
)
)
(when
(and
(Flag92-)
)
(and
(ROW3-ROBOT)
(not (NOT-ROW3-ROBOT))
)
)
(when
(and
(Flag88-)
)
(and
(ROW2-ROBOT)
(not (NOT-ROW2-ROBOT))
)
)
(when
(and
(Flag82-)
)
(and
(ROW1-ROBOT)
(not (NOT-ROW1-ROBOT))
)
)
(when
(and
(Flag75-)
)
(and
(ROW0-ROBOT)
(not (NOT-ROW0-ROBOT))
)
)
(when
(and
(ROW6-ROBOT)
)
(and
(ROW5-ROBOT)
(NOT-ROW6-ROBOT)
(not (NOT-ROW5-ROBOT))
(not (ROW6-ROBOT))
)
)
(when
(and
(ROW5-ROBOT)
)
(and
(ROW4-ROBOT)
(NOT-ROW5-ROBOT)
(not (NOT-ROW4-ROBOT))
(not (ROW5-ROBOT))
)
)
(when
(and
(ROW4-ROBOT)
)
(and
(ROW3-ROBOT)
(NOT-ROW4-ROBOT)
(not (NOT-ROW3-ROBOT))
(not (ROW4-ROBOT))
)
)
(when
(and
(ROW3-ROBOT)
)
(and
(ROW2-ROBOT)
(NOT-ROW3-ROBOT)
(not (NOT-ROW2-ROBOT))
(not (ROW3-ROBOT))
)
)
(when
(and
(ROW2-ROBOT)
)
(and
(ROW1-ROBOT)
(NOT-ROW2-ROBOT)
(not (NOT-ROW1-ROBOT))
(not (ROW2-ROBOT))
)
)
(when
(and
(ROW1-ROBOT)
)
(and
(ROW0-ROBOT)
(NOT-ROW1-ROBOT)
(not (NOT-ROW0-ROBOT))
(not (ROW1-ROBOT))
)
)
(when
(and
(ABOVEOF6-ROBOT)
)
(and
(ABOVEOF5-ROBOT)
(NOT-ABOVEOF6-ROBOT)
(ABOVEOF4-ROBOT)
(NOT-ABOVEOF5-ROBOT)
(ABOVEOF3-ROBOT)
(NOT-ABOVEOF4-ROBOT)
(ABOVEOF2-ROBOT)
(NOT-ABOVEOF3-ROBOT)
(ABOVEOF1-ROBOT)
(NOT-ABOVEOF2-ROBOT)
(NOT-ABOVEOF1-ROBOT)
(not (ABOVEOF6-ROBOT))
)
)
(when
(and
(ABOVEOF5-ROBOT)
)
(and
(ABOVEOF4-ROBOT)
(NOT-ABOVEOF5-ROBOT)
(ABOVEOF3-ROBOT)
(NOT-ABOVEOF4-ROBOT)
(ABOVEOF2-ROBOT)
(NOT-ABOVEOF3-ROBOT)
(ABOVEOF1-ROBOT)
(NOT-ABOVEOF2-ROBOT)
(NOT-ABOVEOF1-ROBOT)
(not (ABOVEOF5-ROBOT))
)
)
(when
(and
(ABOVEOF4-ROBOT)
)
(and
(ABOVEOF3-ROBOT)
(NOT-ABOVEOF4-ROBOT)
(ABOVEOF2-ROBOT)
(NOT-ABOVEOF3-ROBOT)
(ABOVEOF1-ROBOT)
(NOT-ABOVEOF2-ROBOT)
(NOT-ABOVEOF1-ROBOT)
(not (ABOVEOF4-ROBOT))
)
)
(when
(and
(ABOVEOF3-ROBOT)
)
(and
(ABOVEOF2-ROBOT)
(NOT-ABOVEOF3-ROBOT)
(ABOVEOF1-ROBOT)
(NOT-ABOVEOF2-ROBOT)
(NOT-ABOVEOF1-ROBOT)
(not (ABOVEOF3-ROBOT))
)
)
(when
(and
(ABOVEOF2-ROBOT)
)
(and
(ABOVEOF1-ROBOT)
(NOT-ABOVEOF2-ROBOT)
(NOT-ABOVEOF1-ROBOT)
(not (ABOVEOF2-ROBOT))
)
)
(when
(and
(ABOVEOF1-ROBOT)
)
(and
(NOT-ABOVEOF1-ROBOT)
(not (ABOVEOF1-ROBOT))
)
)
(when
(and
(Flag100-)
)
(and
(BELOWOF6-ROBOT)
(not (NOT-BELOWOF6-ROBOT))
)
)
(when
(and
(BELOWOF3-ROBOT)
)
(and
(BELOWOF5-ROBOT)
(BELOWOF4-ROBOT)
(BELOWOF2-ROBOT)
(not (NOT-BELOWOF5-ROBOT))
(not (NOT-BELOWOF4-ROBOT))
(not (NOT-BELOWOF3-ROBOT))
(not (NOT-BELOWOF2-ROBOT))
)
)
(when
(and
(BELOWOF4-ROBOT)
)
(and
(BELOWOF5-ROBOT)
(BELOWOF3-ROBOT)
(not (NOT-BELOWOF5-ROBOT))
(not (NOT-BELOWOF4-ROBOT))
(not (NOT-BELOWOF3-ROBOT))
)
)
(when
(and
(BELOWOF1-ROBOT)
)
(and
(BELOWOF5-ROBOT)
(BELOWOF4-ROBOT)
(BELOWOF3-ROBOT)
(BELOWOF2-ROBOT)
(not (NOT-BELOWOF5-ROBOT))
(not (NOT-BELOWOF4-ROBOT))
(not (NOT-BELOWOF3-ROBOT))
(not (NOT-BELOWOF2-ROBOT))
(not (NOT-BELOWOF1-ROBOT))
)
)
(when
(and
(BELOWOF2-ROBOT)
)
(and
(BELOWOF5-ROBOT)
(BELOWOF4-ROBOT)
(BELOWOF3-ROBOT)
(BELOWOF1-ROBOT)
(not (NOT-BELOWOF5-ROBOT))
(not (NOT-BELOWOF4-ROBOT))
(not (NOT-BELOWOF3-ROBOT))
(not (NOT-BELOWOF2-ROBOT))
(not (NOT-BELOWOF1-ROBOT))
)
)
(when
(and
(BELOWOF5-ROBOT)
)
(and
(BELOWOF4-ROBOT)
(not (NOT-BELOWOF5-ROBOT))
(not (NOT-BELOWOF4-ROBOT))
)
)
(when
(and
(BELOWOF6-ROBOT)
)
(and
(BELOWOF5-ROBOT)
(not (NOT-BELOWOF5-ROBOT))
)
)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action MOVEUP-ROBOT
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
)
:effect
(and
(when
(and
(Flag96-)
)
(and
(ROW6-ROBOT)
(not (NOT-ROW6-ROBOT))
)
)
(when
(and
(Flag92-)
)
(and
(ROW5-ROBOT)
(not (NOT-ROW5-ROBOT))
)
)
(when
(and
(Flag88-)
)
(and
(ROW4-ROBOT)
(not (NOT-ROW4-ROBOT))
)
)
(when
(and
(Flag82-)
)
(and
(ROW3-ROBOT)
(not (NOT-ROW3-ROBOT))
)
)
(when
(and
(Flag75-)
)
(and
(ROW2-ROBOT)
(not (NOT-ROW2-ROBOT))
)
)
(when
(and
(Flag68-)
)
(and
(ROW1-ROBOT)
(not (NOT-ROW1-ROBOT))
)
)
(when
(and
(ROW5-ROBOT)
)
(and
(ROW6-ROBOT)
(NOT-ROW5-ROBOT)
(not (NOT-ROW6-ROBOT))
(not (ROW5-ROBOT))
)
)
(when
(and
(ROW4-ROBOT)
)
(and
(ROW5-ROBOT)
(NOT-ROW4-ROBOT)
(not (NOT-ROW5-ROBOT))
(not (ROW4-ROBOT))
)
)
(when
(and
(ROW3-ROBOT)
)
(and
(ROW4-ROBOT)
(NOT-ROW3-ROBOT)
(not (NOT-ROW4-ROBOT))
(not (ROW3-ROBOT))
)
)
(when
(and
(ROW2-ROBOT)
)
(and
(ROW3-ROBOT)
(NOT-ROW2-ROBOT)
(not (NOT-ROW3-ROBOT))
(not (ROW2-ROBOT))
)
)
(when
(and
(ROW1-ROBOT)
)
(and
(ROW2-ROBOT)
(NOT-ROW1-ROBOT)
(not (NOT-ROW2-ROBOT))
(not (ROW1-ROBOT))
)
)
(when
(and
(ROW0-ROBOT)
)
(and
(ROW1-ROBOT)
(NOT-ROW0-ROBOT)
(not (NOT-ROW1-ROBOT))
(not (ROW0-ROBOT))
)
)
(when
(and
(Flag61-)
)
(and
(BELOWOF7-ROBOT)
(NOT-BELOWOF6-ROBOT)
(not (NOT-BELOWOF7-ROBOT))
(not (BELOWOF6-ROBOT))
)
)
(when
(and
(Flag60-)
)
(and
(BELOWOF6-ROBOT)
(NOT-BELOWOF5-ROBOT)
(not (NOT-BELOWOF6-ROBOT))
(not (BELOWOF5-ROBOT))
)
)
(when
(and
(Flag59-)
)
(and
(BELOWOF5-ROBOT)
(NOT-BELOWOF4-ROBOT)
(not (NOT-BELOWOF5-ROBOT))
(not (BELOWOF4-ROBOT))
)
)
(when
(and
(Flag58-)
)
(and
(BELOWOF4-ROBOT)
(NOT-BELOWOF3-ROBOT)
(not (NOT-BELOWOF4-ROBOT))
(not (BELOWOF3-ROBOT))
)
)
(when
(and
(Flag57-)
)
(and
(BELOWOF3-ROBOT)
(NOT-BELOWOF2-ROBOT)
(not (NOT-BELOWOF3-ROBOT))
(not (BELOWOF2-ROBOT))
)
)
(when
(and
(BELOWOF1-ROBOT)
)
(and
(BELOWOF2-ROBOT)
(NOT-BELOWOF1-ROBOT)
(not (NOT-BELOWOF2-ROBOT))
(not (BELOWOF1-ROBOT))
)
)
(when
(and
(Flag56-)
)
(and
(ABOVEOF6-ROBOT)
(not (NOT-ABOVEOF6-ROBOT))
)
)
(when
(and
(Flag55-)
)
(and
(ABOVEOF5-ROBOT)
(not (NOT-ABOVEOF5-ROBOT))
)
)
(when
(and
(Flag54-)
)
(and
(ABOVEOF4-ROBOT)
(not (NOT-ABOVEOF4-ROBOT))
)
)
(when
(and
(Flag53-)
)
(and
(ABOVEOF3-ROBOT)
(not (NOT-ABOVEOF3-ROBOT))
)
)
(when
(and
(Flag52-)
)
(and
(ABOVEOF2-ROBOT)
(not (NOT-ABOVEOF2-ROBOT))
)
)
(ABOVEOF1-ROBOT)
(CHECKCONSISTENCY-)
(not (NOT-ABOVEOF1-ROBOT))
(not (NOT-CHECKCONSISTENCY-))
)
)
(:derived (Flag2-)(and (NOT-CHECKCONSISTENCY-)(NOT-ERROR-)))

(:derived (Flag3-)(and (RIGHTOF2-ROBOT)))

(:derived (Flag3-)(and (LEFTOF6-ROBOT)))

(:derived (Flag9-)(and (LEFTOF6-ROBOT)))

(:derived (Flag10-)(and (LEFTOF6-ROBOT)))

(:derived (Flag11-)(and (RIGHTOF2-ROBOT)))

(:derived (Flag12-)(and (RIGHTOF2-ROBOT)))

(:derived (Flag61-)(and (BELOWOF6-ROBOT)))

(:derived (Flag97-)(and (RIGHTOF2-ROBOT)))

(:derived (Flag100-)(and (BELOWOF6-ROBOT)))

)
