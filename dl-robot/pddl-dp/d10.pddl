(define (domain grounded-SIMPLE-ADL-ROBOT)
(:requirements
:strips
)
(:predicates
(Flag177-)
(Flag98-)
(Flag97-)
(Flag21-)
(Flag7-)
(Flag6-)
(Flag5-)
(Flag3-)
(Flag2-)
(ROW8-ROBOT)
(ROW7-ROBOT)
(ROW6-ROBOT)
(ROW5-ROBOT)
(ROW4-ROBOT)
(ROW3-ROBOT)
(ROW2-ROBOT)
(ROW1-ROBOT)
(ROW0-ROBOT)
(ABOVEOF8-ROBOT)
(ABOVEOF7-ROBOT)
(ABOVEOF6-ROBOT)
(ABOVEOF5-ROBOT)
(ABOVEOF4-ROBOT)
(ABOVEOF3-ROBOT)
(ABOVEOF2-ROBOT)
(ABOVEOF1-ROBOT)
(BELOWOF8-ROBOT)
(BELOWOF7-ROBOT)
(BELOWOF6-ROBOT)
(BELOWOF5-ROBOT)
(BELOWOF4-ROBOT)
(BELOWOF3-ROBOT)
(BELOWOF2-ROBOT)
(BELOWOF1-ROBOT)
(CHECKCONSISTENCY-)
(ROW9-ROBOT)
(BELOWOF10-ROBOT)
(NOT-BELOWOF9-ROBOT)
(ABOVEOF9-ROBOT)
(Flag180-)
(Flag175-)
(Flag174-)
(Flag172-)
(Flag171-)
(Flag170-)
(Flag168-)
(Flag167-)
(Flag166-)
(Flag165-)
(Flag163-)
(Flag162-)
(Flag161-)
(Flag160-)
(Flag159-)
(Flag157-)
(Flag156-)
(Flag155-)
(Flag154-)
(Flag153-)
(Flag152-)
(Flag150-)
(Flag149-)
(Flag148-)
(Flag147-)
(Flag146-)
(Flag145-)
(Flag143-)
(Flag142-)
(Flag141-)
(Flag140-)
(Flag139-)
(Flag138-)
(Flag137-)
(Flag136-)
(Flag134-)
(Flag133-)
(Flag132-)
(Flag131-)
(Flag130-)
(Flag129-)
(Flag128-)
(Flag127-)
(Flag126-)
(Flag124-)
(Flag123-)
(Flag122-)
(Flag121-)
(Flag120-)
(Flag119-)
(Flag118-)
(Flag117-)
(Flag116-)
(Flag115-)
(Flag114-)
(Flag113-)
(Flag112-)
(Flag111-)
(Flag110-)
(Flag109-)
(Flag108-)
(Flag107-)
(Flag106-)
(Flag105-)
(Flag104-)
(Flag103-)
(Flag102-)
(Flag101-)
(Flag100-)
(Flag99-)
(Flag96-)
(Flag95-)
(Flag94-)
(Flag93-)
(Flag92-)
(Flag91-)
(Flag90-)
(Flag4-)
(ERROR-)
(COLUMN9-ROBOT)
(COLUMN8-ROBOT)
(COLUMN7-ROBOT)
(COLUMN6-ROBOT)
(COLUMN5-ROBOT)
(COLUMN4-ROBOT)
(COLUMN3-ROBOT)
(COLUMN2-ROBOT)
(COLUMN1-ROBOT)
(LEFTOF10-ROBOT)
(NOT-LEFTOF9-ROBOT)
(LEFTOF8-ROBOT)
(LEFTOF7-ROBOT)
(LEFTOF6-ROBOT)
(LEFTOF5-ROBOT)
(LEFTOF4-ROBOT)
(LEFTOF3-ROBOT)
(LEFTOF2-ROBOT)
(RIGHTOF9-ROBOT)
(RIGHTOF8-ROBOT)
(RIGHTOF7-ROBOT)
(RIGHTOF6-ROBOT)
(RIGHTOF5-ROBOT)
(RIGHTOF4-ROBOT)
(RIGHTOF3-ROBOT)
(RIGHTOF1-ROBOT)
(COLUMN0-ROBOT)
(NOT-RIGHTOF2-ROBOT)
(RIGHTOF0-ROBOT)
(LEFTOF1-ROBOT)
(Flag178-)
(Flag176-)
(Flag173-)
(Flag169-)
(Flag164-)
(Flag158-)
(Flag151-)
(Flag144-)
(Flag135-)
(Flag125-)
(Flag88-)
(Flag87-)
(Flag85-)
(Flag84-)
(Flag83-)
(Flag82-)
(Flag80-)
(Flag79-)
(Flag78-)
(Flag77-)
(Flag76-)
(Flag74-)
(Flag73-)
(Flag72-)
(Flag71-)
(Flag70-)
(Flag68-)
(Flag67-)
(Flag66-)
(Flag65-)
(Flag64-)
(Flag63-)
(Flag62-)
(Flag60-)
(Flag59-)
(Flag58-)
(Flag57-)
(Flag56-)
(Flag55-)
(Flag54-)
(Flag53-)
(Flag51-)
(Flag50-)
(Flag49-)
(Flag48-)
(Flag47-)
(Flag46-)
(Flag45-)
(Flag44-)
(Flag42-)
(Flag41-)
(Flag40-)
(Flag39-)
(Flag38-)
(Flag37-)
(Flag36-)
(Flag35-)
(Flag34-)
(Flag33-)
(Flag31-)
(Flag30-)
(Flag29-)
(Flag28-)
(Flag27-)
(Flag26-)
(Flag25-)
(Flag24-)
(Flag23-)
(Flag22-)
(Flag20-)
(Flag19-)
(Flag18-)
(Flag17-)
(Flag16-)
(Flag15-)
(Flag14-)
(Flag13-)
(Flag12-)
(Flag11-)
(Flag10-)
(Flag9-)
(Flag8-)
(Flag1-)
(Flag179-)
(Flag89-)
(Flag86-)
(Flag81-)
(Flag75-)
(Flag69-)
(Flag61-)
(Flag52-)
(Flag43-)
(Flag32-)
(NOT-COLUMN8-ROBOT)
(NOT-COLUMN7-ROBOT)
(NOT-COLUMN6-ROBOT)
(NOT-COLUMN5-ROBOT)
(NOT-COLUMN4-ROBOT)
(NOT-COLUMN3-ROBOT)
(NOT-COLUMN2-ROBOT)
(NOT-COLUMN1-ROBOT)
(NOT-COLUMN0-ROBOT)
(NOT-RIGHTOF8-ROBOT)
(NOT-RIGHTOF7-ROBOT)
(NOT-RIGHTOF6-ROBOT)
(NOT-RIGHTOF5-ROBOT)
(NOT-RIGHTOF4-ROBOT)
(NOT-RIGHTOF3-ROBOT)
(NOT-RIGHTOF1-ROBOT)
(RIGHTOF2-ROBOT)
(NOT-RIGHTOF0-ROBOT)
(NOT-LEFTOF8-ROBOT)
(NOT-LEFTOF7-ROBOT)
(NOT-LEFTOF6-ROBOT)
(NOT-LEFTOF5-ROBOT)
(NOT-LEFTOF4-ROBOT)
(NOT-LEFTOF3-ROBOT)
(NOT-LEFTOF2-ROBOT)
(NOT-LEFTOF1-ROBOT)
(NOT-CHECKCONSISTENCY-)
(NOT-COLUMN9-ROBOT)
(NOT-LEFTOF10-ROBOT)
(LEFTOF9-ROBOT)
(NOT-RIGHTOF9-ROBOT)
(NOT-ERROR-)
(NOT-ROW9-ROBOT)
(NOT-ROW8-ROBOT)
(NOT-ROW7-ROBOT)
(NOT-ROW6-ROBOT)
(NOT-ROW5-ROBOT)
(NOT-ROW4-ROBOT)
(NOT-ROW3-ROBOT)
(NOT-ROW2-ROBOT)
(NOT-ROW1-ROBOT)
(NOT-BELOWOF10-ROBOT)
(BELOWOF9-ROBOT)
(NOT-BELOWOF8-ROBOT)
(NOT-BELOWOF7-ROBOT)
(NOT-BELOWOF6-ROBOT)
(NOT-BELOWOF5-ROBOT)
(NOT-BELOWOF4-ROBOT)
(NOT-BELOWOF3-ROBOT)
(NOT-BELOWOF2-ROBOT)
(NOT-ABOVEOF9-ROBOT)
(NOT-ABOVEOF8-ROBOT)
(NOT-ABOVEOF7-ROBOT)
(NOT-ABOVEOF6-ROBOT)
(NOT-ABOVEOF5-ROBOT)
(NOT-ABOVEOF4-ROBOT)
(NOT-ABOVEOF3-ROBOT)
(NOT-ABOVEOF2-ROBOT)
(NOT-ABOVEOF1-ROBOT)
(NOT-ROW0-ROBOT)
(NOT-BELOWOF1-ROBOT)
)
(:derived (Flag32-)(and (Flag22-)))

(:derived (Flag32-)(and (Flag23-)))

(:derived (Flag32-)(and (Flag24-)))

(:derived (Flag32-)(and (Flag25-)))

(:derived (Flag32-)(and (Flag26-)))

(:derived (Flag32-)(and (Flag27-)))

(:derived (Flag32-)(and (Flag28-)))

(:derived (Flag32-)(and (Flag29-)))

(:derived (Flag32-)(and (Flag30-)))

(:derived (Flag32-)(and (Flag31-)))

(:derived (Flag43-)(and (Flag22-)))

(:derived (Flag43-)(and (Flag33-)))

(:derived (Flag43-)(and (Flag34-)))

(:derived (Flag43-)(and (Flag35-)))

(:derived (Flag43-)(and (Flag36-)))

(:derived (Flag43-)(and (Flag37-)))

(:derived (Flag43-)(and (Flag24-)))

(:derived (Flag43-)(and (Flag25-)))

(:derived (Flag43-)(and (Flag26-)))

(:derived (Flag43-)(and (Flag23-)))

(:derived (Flag43-)(and (Flag38-)))

(:derived (Flag43-)(and (Flag39-)))

(:derived (Flag43-)(and (Flag28-)))

(:derived (Flag43-)(and (Flag29-)))

(:derived (Flag43-)(and (Flag40-)))

(:derived (Flag43-)(and (Flag30-)))

(:derived (Flag43-)(and (Flag31-)))

(:derived (Flag43-)(and (Flag41-)))

(:derived (Flag43-)(and (Flag42-)))

(:derived (Flag52-)(and (Flag36-)))

(:derived (Flag52-)(and (Flag44-)))

(:derived (Flag52-)(and (Flag24-)))

(:derived (Flag52-)(and (Flag26-)))

(:derived (Flag52-)(and (Flag45-)))

(:derived (Flag52-)(and (Flag33-)))

(:derived (Flag52-)(and (Flag23-)))

(:derived (Flag52-)(and (Flag38-)))

(:derived (Flag52-)(and (Flag46-)))

(:derived (Flag52-)(and (Flag40-)))

(:derived (Flag52-)(and (Flag28-)))

(:derived (Flag52-)(and (Flag22-)))

(:derived (Flag52-)(and (Flag47-)))

(:derived (Flag52-)(and (Flag37-)))

(:derived (Flag52-)(and (Flag48-)))

(:derived (Flag52-)(and (Flag30-)))

(:derived (Flag52-)(and (Flag49-)))

(:derived (Flag52-)(and (Flag34-)))

(:derived (Flag52-)(and (Flag39-)))

(:derived (Flag52-)(and (Flag29-)))

(:derived (Flag52-)(and (Flag31-)))

(:derived (Flag52-)(and (Flag41-)))

(:derived (Flag52-)(and (Flag50-)))

(:derived (Flag52-)(and (Flag42-)))

(:derived (Flag52-)(and (Flag51-)))

(:derived (Flag61-)(and (Flag36-)))

(:derived (Flag61-)(and (Flag44-)))

(:derived (Flag61-)(and (Flag24-)))

(:derived (Flag61-)(and (Flag26-)))

(:derived (Flag61-)(and (Flag45-)))

(:derived (Flag61-)(and (Flag53-)))

(:derived (Flag61-)(and (Flag54-)))

(:derived (Flag61-)(and (Flag33-)))

(:derived (Flag61-)(and (Flag23-)))

(:derived (Flag61-)(and (Flag38-)))

(:derived (Flag61-)(and (Flag46-)))

(:derived (Flag61-)(and (Flag40-)))

(:derived (Flag61-)(and (Flag28-)))

(:derived (Flag61-)(and (Flag22-)))

(:derived (Flag61-)(and (Flag47-)))

(:derived (Flag61-)(and (Flag37-)))

(:derived (Flag61-)(and (Flag55-)))

(:derived (Flag61-)(and (Flag48-)))

(:derived (Flag61-)(and (Flag56-)))

(:derived (Flag61-)(and (Flag30-)))

(:derived (Flag61-)(and (Flag57-)))

(:derived (Flag61-)(and (Flag58-)))

(:derived (Flag61-)(and (Flag59-)))

(:derived (Flag61-)(and (Flag34-)))

(:derived (Flag61-)(and (Flag31-)))

(:derived (Flag61-)(and (Flag60-)))

(:derived (Flag61-)(and (Flag41-)))

(:derived (Flag61-)(and (Flag50-)))

(:derived (Flag61-)(and (Flag42-)))

(:derived (Flag69-)(and (Flag62-)))

(:derived (Flag69-)(and (Flag36-)))

(:derived (Flag69-)(and (Flag44-)))

(:derived (Flag69-)(and (Flag24-)))

(:derived (Flag69-)(and (Flag63-)))

(:derived (Flag69-)(and (Flag45-)))

(:derived (Flag69-)(and (Flag53-)))

(:derived (Flag69-)(and (Flag54-)))

(:derived (Flag69-)(and (Flag33-)))

(:derived (Flag69-)(and (Flag23-)))

(:derived (Flag69-)(and (Flag38-)))

(:derived (Flag69-)(and (Flag46-)))

(:derived (Flag69-)(and (Flag64-)))

(:derived (Flag69-)(and (Flag65-)))

(:derived (Flag69-)(and (Flag66-)))

(:derived (Flag69-)(and (Flag22-)))

(:derived (Flag69-)(and (Flag47-)))

(:derived (Flag69-)(and (Flag37-)))

(:derived (Flag69-)(and (Flag56-)))

(:derived (Flag69-)(and (Flag67-)))

(:derived (Flag69-)(and (Flag28-)))

(:derived (Flag69-)(and (Flag30-)))

(:derived (Flag69-)(and (Flag57-)))

(:derived (Flag69-)(and (Flag58-)))

(:derived (Flag69-)(and (Flag59-)))

(:derived (Flag69-)(and (Flag34-)))

(:derived (Flag69-)(and (Flag48-)))

(:derived (Flag69-)(and (Flag31-)))

(:derived (Flag69-)(and (Flag68-)))

(:derived (Flag69-)(and (Flag41-)))

(:derived (Flag69-)(and (Flag50-)))

(:derived (Flag69-)(and (Flag42-)))

(:derived (Flag75-)(and (Flag62-)))

(:derived (Flag75-)(and (Flag36-)))

(:derived (Flag75-)(and (Flag63-)))

(:derived (Flag75-)(and (Flag24-)))

(:derived (Flag75-)(and (Flag54-)))

(:derived (Flag75-)(and (Flag33-)))

(:derived (Flag75-)(and (Flag23-)))

(:derived (Flag75-)(and (Flag38-)))

(:derived (Flag75-)(and (Flag70-)))

(:derived (Flag75-)(and (Flag46-)))

(:derived (Flag75-)(and (Flag65-)))

(:derived (Flag75-)(and (Flag66-)))

(:derived (Flag75-)(and (Flag44-)))

(:derived (Flag75-)(and (Flag47-)))

(:derived (Flag75-)(and (Flag37-)))

(:derived (Flag75-)(and (Flag56-)))

(:derived (Flag75-)(and (Flag67-)))

(:derived (Flag75-)(and (Flag48-)))

(:derived (Flag75-)(and (Flag71-)))

(:derived (Flag75-)(and (Flag30-)))

(:derived (Flag75-)(and (Flag72-)))

(:derived (Flag75-)(and (Flag73-)))

(:derived (Flag75-)(and (Flag57-)))

(:derived (Flag75-)(and (Flag58-)))

(:derived (Flag75-)(and (Flag59-)))

(:derived (Flag75-)(and (Flag34-)))

(:derived (Flag75-)(and (Flag31-)))

(:derived (Flag75-)(and (Flag68-)))

(:derived (Flag75-)(and (Flag41-)))

(:derived (Flag75-)(and (Flag50-)))

(:derived (Flag75-)(and (Flag74-)))

(:derived (Flag81-)(and (Flag62-)))

(:derived (Flag81-)(and (Flag36-)))

(:derived (Flag81-)(and (Flag63-)))

(:derived (Flag81-)(and (Flag24-)))

(:derived (Flag81-)(and (Flag76-)))

(:derived (Flag81-)(and (Flag77-)))

(:derived (Flag81-)(and (Flag44-)))

(:derived (Flag81-)(and (Flag54-)))

(:derived (Flag81-)(and (Flag37-)))

(:derived (Flag81-)(and (Flag46-)))

(:derived (Flag81-)(and (Flag70-)))

(:derived (Flag81-)(and (Flag65-)))

(:derived (Flag81-)(and (Flag66-)))

(:derived (Flag81-)(and (Flag78-)))

(:derived (Flag81-)(and (Flag47-)))

(:derived (Flag81-)(and (Flag23-)))

(:derived (Flag81-)(and (Flag56-)))

(:derived (Flag81-)(and (Flag67-)))

(:derived (Flag81-)(and (Flag30-)))

(:derived (Flag81-)(and (Flag72-)))

(:derived (Flag81-)(and (Flag79-)))

(:derived (Flag81-)(and (Flag80-)))

(:derived (Flag81-)(and (Flag57-)))

(:derived (Flag81-)(and (Flag58-)))

(:derived (Flag81-)(and (Flag34-)))

(:derived (Flag81-)(and (Flag31-)))

(:derived (Flag81-)(and (Flag41-)))

(:derived (Flag81-)(and (Flag50-)))

(:derived (Flag81-)(and (Flag74-)))

(:derived (Flag86-)(and (Flag62-)))

(:derived (Flag86-)(and (Flag82-)))

(:derived (Flag86-)(and (Flag63-)))

(:derived (Flag86-)(and (Flag24-)))

(:derived (Flag86-)(and (Flag77-)))

(:derived (Flag86-)(and (Flag83-)))

(:derived (Flag86-)(and (Flag54-)))

(:derived (Flag86-)(and (Flag36-)))

(:derived (Flag86-)(and (Flag37-)))

(:derived (Flag86-)(and (Flag46-)))

(:derived (Flag86-)(and (Flag70-)))

(:derived (Flag86-)(and (Flag65-)))

(:derived (Flag86-)(and (Flag84-)))

(:derived (Flag86-)(and (Flag85-)))

(:derived (Flag86-)(and (Flag47-)))

(:derived (Flag86-)(and (Flag23-)))

(:derived (Flag86-)(and (Flag56-)))

(:derived (Flag86-)(and (Flag67-)))

(:derived (Flag86-)(and (Flag30-)))

(:derived (Flag86-)(and (Flag72-)))

(:derived (Flag86-)(and (Flag79-)))

(:derived (Flag86-)(and (Flag57-)))

(:derived (Flag86-)(and (Flag34-)))

(:derived (Flag86-)(and (Flag50-)))

(:derived (Flag86-)(and (Flag74-)))

(:derived (Flag89-)(and (Flag62-)))

(:derived (Flag89-)(and (Flag84-)))

(:derived (Flag89-)(and (Flag57-)))

(:derived (Flag89-)(and (Flag47-)))

(:derived (Flag89-)(and (Flag24-)))

(:derived (Flag89-)(and (Flag87-)))

(:derived (Flag89-)(and (Flag67-)))

(:derived (Flag89-)(and (Flag34-)))

(:derived (Flag89-)(and (Flag77-)))

(:derived (Flag89-)(and (Flag70-)))

(:derived (Flag89-)(and (Flag30-)))

(:derived (Flag89-)(and (Flag83-)))

(:derived (Flag89-)(and (Flag36-)))

(:derived (Flag89-)(and (Flag65-)))

(:derived (Flag89-)(and (Flag50-)))

(:derived (Flag89-)(and (Flag74-)))

(:derived (Flag89-)(and (Flag79-)))

(:derived (Flag89-)(and (Flag88-)))

(:derived (Flag179-)(and (Flag62-)))

(:derived (Flag179-)(and (Flag57-)))

(:derived (Flag179-)(and (Flag36-)))

(:derived (Flag179-)(and (Flag24-)))

(:derived (Flag179-)(and (Flag87-)))

(:derived (Flag179-)(and (Flag178-)))

(:derived (Flag179-)(and (Flag83-)))

(:derived (Flag179-)(and (Flag50-)))

(:derived (Flag179-)(and (Flag74-)))

(:derived (Flag179-)(and (Flag79-)))

(:derived (Flag1-)(and (COLUMN2-ROBOT)(ROW1-ROBOT)(NOT-ERROR-)(NOT-CHECKCONSISTENCY-)))

(:derived (Flag3-)(and (LEFTOF8-ROBOT)))

(:derived (Flag3-)(and (RIGHTOF6-ROBOT)))

(:derived (Flag3-)(and (LEFTOF4-ROBOT)))

(:derived (Flag3-)(and (LEFTOF3-ROBOT)))

(:derived (Flag3-)(and (RIGHTOF9-ROBOT)))

(:derived (Flag3-)(and (RIGHTOF7-ROBOT)))

(:derived (Flag3-)(and (LEFTOF5-ROBOT)))

(:derived (Flag3-)(and (RIGHTOF1-ROBOT)))

(:derived (Flag3-)(and (RIGHTOF4-ROBOT)))

(:derived (Flag3-)(and (RIGHTOF0-ROBOT)))

(:derived (Flag3-)(and (LEFTOF7-ROBOT)))

(:derived (Flag3-)(and (LEFTOF2-ROBOT)))

(:derived (Flag3-)(and (RIGHTOF8-ROBOT)))

(:derived (Flag3-)(and (RIGHTOF5-ROBOT)))

(:derived (Flag3-)(and (LEFTOF1-ROBOT)))

(:derived (Flag3-)(and (LEFTOF10-ROBOT)))

(:derived (Flag3-)(and (LEFTOF6-ROBOT)))

(:derived (Flag3-)(and (RIGHTOF3-ROBOT)))

(:derived (Flag5-)(and (RIGHTOF3-ROBOT)))

(:derived (Flag5-)(and (RIGHTOF8-ROBOT)))

(:derived (Flag5-)(and (RIGHTOF5-ROBOT)))

(:derived (Flag5-)(and (RIGHTOF0-ROBOT)))

(:derived (Flag5-)(and (RIGHTOF4-ROBOT)))

(:derived (Flag5-)(and (RIGHTOF1-ROBOT)))

(:derived (Flag5-)(and (RIGHTOF7-ROBOT)))

(:derived (Flag5-)(and (RIGHTOF9-ROBOT)))

(:derived (Flag5-)(and (RIGHTOF6-ROBOT)))

(:derived (Flag6-)(and (RIGHTOF3-ROBOT)))

(:derived (Flag6-)(and (RIGHTOF5-ROBOT)))

(:derived (Flag6-)(and (RIGHTOF8-ROBOT)))

(:derived (Flag6-)(and (RIGHTOF4-ROBOT)))

(:derived (Flag6-)(and (RIGHTOF1-ROBOT)))

(:derived (Flag6-)(and (RIGHTOF7-ROBOT)))

(:derived (Flag6-)(and (RIGHTOF9-ROBOT)))

(:derived (Flag6-)(and (RIGHTOF6-ROBOT)))

(:derived (Flag7-)(and (RIGHTOF3-ROBOT)))

(:derived (Flag7-)(and (RIGHTOF5-ROBOT)))

(:derived (Flag7-)(and (RIGHTOF4-ROBOT)))

(:derived (Flag7-)(and (RIGHTOF8-ROBOT)))

(:derived (Flag7-)(and (RIGHTOF7-ROBOT)))

(:derived (Flag7-)(and (RIGHTOF9-ROBOT)))

(:derived (Flag7-)(and (RIGHTOF6-ROBOT)))

(:derived (Flag8-)(and (RIGHTOF3-ROBOT)))

(:derived (Flag8-)(and (RIGHTOF5-ROBOT)))

(:derived (Flag8-)(and (RIGHTOF4-ROBOT)))

(:derived (Flag8-)(and (RIGHTOF8-ROBOT)))

(:derived (Flag8-)(and (RIGHTOF7-ROBOT)))

(:derived (Flag8-)(and (RIGHTOF9-ROBOT)))

(:derived (Flag8-)(and (RIGHTOF6-ROBOT)))

(:derived (Flag9-)(and (RIGHTOF5-ROBOT)))

(:derived (Flag9-)(and (RIGHTOF4-ROBOT)))

(:derived (Flag9-)(and (RIGHTOF8-ROBOT)))

(:derived (Flag9-)(and (RIGHTOF7-ROBOT)))

(:derived (Flag9-)(and (RIGHTOF9-ROBOT)))

(:derived (Flag9-)(and (RIGHTOF6-ROBOT)))

(:derived (Flag10-)(and (RIGHTOF8-ROBOT)))

(:derived (Flag10-)(and (RIGHTOF7-ROBOT)))

(:derived (Flag10-)(and (RIGHTOF9-ROBOT)))

(:derived (Flag10-)(and (RIGHTOF5-ROBOT)))

(:derived (Flag10-)(and (RIGHTOF6-ROBOT)))

(:derived (Flag11-)(and (RIGHTOF7-ROBOT)))

(:derived (Flag11-)(and (RIGHTOF9-ROBOT)))

(:derived (Flag11-)(and (RIGHTOF8-ROBOT)))

(:derived (Flag11-)(and (RIGHTOF6-ROBOT)))

(:derived (Flag12-)(and (RIGHTOF7-ROBOT)))

(:derived (Flag12-)(and (RIGHTOF9-ROBOT)))

(:derived (Flag12-)(and (RIGHTOF8-ROBOT)))

(:derived (Flag13-)(and (RIGHTOF9-ROBOT)))

(:derived (Flag13-)(and (RIGHTOF8-ROBOT)))

(:derived (Flag14-)(and (LEFTOF1-ROBOT)))

(:derived (Flag14-)(and (LEFTOF2-ROBOT)))

(:derived (Flag15-)(and (LEFTOF1-ROBOT)))

(:derived (Flag15-)(and (LEFTOF3-ROBOT)))

(:derived (Flag15-)(and (LEFTOF2-ROBOT)))

(:derived (Flag16-)(and (LEFTOF1-ROBOT)))

(:derived (Flag16-)(and (LEFTOF3-ROBOT)))

(:derived (Flag16-)(and (LEFTOF4-ROBOT)))

(:derived (Flag16-)(and (LEFTOF2-ROBOT)))

(:derived (Flag17-)(and (LEFTOF5-ROBOT)))

(:derived (Flag17-)(and (LEFTOF1-ROBOT)))

(:derived (Flag17-)(and (LEFTOF3-ROBOT)))

(:derived (Flag17-)(and (LEFTOF4-ROBOT)))

(:derived (Flag17-)(and (LEFTOF2-ROBOT)))

(:derived (Flag18-)(and (LEFTOF2-ROBOT)))

(:derived (Flag18-)(and (LEFTOF1-ROBOT)))

(:derived (Flag18-)(and (LEFTOF6-ROBOT)))

(:derived (Flag18-)(and (LEFTOF5-ROBOT)))

(:derived (Flag18-)(and (LEFTOF3-ROBOT)))

(:derived (Flag18-)(and (LEFTOF4-ROBOT)))

(:derived (Flag19-)(and (LEFTOF2-ROBOT)))

(:derived (Flag19-)(and (LEFTOF7-ROBOT)))

(:derived (Flag19-)(and (LEFTOF1-ROBOT)))

(:derived (Flag19-)(and (LEFTOF6-ROBOT)))

(:derived (Flag19-)(and (LEFTOF5-ROBOT)))

(:derived (Flag19-)(and (LEFTOF3-ROBOT)))

(:derived (Flag19-)(and (LEFTOF4-ROBOT)))

(:derived (Flag20-)(and (LEFTOF2-ROBOT)))

(:derived (Flag20-)(and (LEFTOF7-ROBOT)))

(:derived (Flag20-)(and (LEFTOF1-ROBOT)))

(:derived (Flag20-)(and (LEFTOF6-ROBOT)))

(:derived (Flag20-)(and (LEFTOF5-ROBOT)))

(:derived (Flag20-)(and (LEFTOF3-ROBOT)))

(:derived (Flag20-)(and (LEFTOF4-ROBOT)))

(:derived (Flag20-)(and (LEFTOF8-ROBOT)))

(:derived (Flag21-)(and (LEFTOF2-ROBOT)))

(:derived (Flag21-)(and (LEFTOF7-ROBOT)))

(:derived (Flag21-)(and (LEFTOF1-ROBOT)))

(:derived (Flag21-)(and (LEFTOF6-ROBOT)))

(:derived (Flag21-)(and (LEFTOF5-ROBOT)))

(:derived (Flag21-)(and (LEFTOF3-ROBOT)))

(:derived (Flag21-)(and (LEFTOF4-ROBOT)))

(:derived (Flag21-)(and (LEFTOF8-ROBOT)))

(:derived (Flag22-)(and (LEFTOF1-ROBOT)(RIGHTOF4-ROBOT)))

(:derived (Flag23-)(and (LEFTOF1-ROBOT)(RIGHTOF7-ROBOT)))

(:derived (Flag24-)(and (LEFTOF1-ROBOT)(RIGHTOF9-ROBOT)))

(:derived (Flag25-)(and (LEFTOF1-ROBOT)(RIGHTOF1-ROBOT)))

(:derived (Flag26-)(and (RIGHTOF3-ROBOT)(LEFTOF1-ROBOT)))

(:derived (Flag27-)(and (RIGHTOF0-ROBOT)(LEFTOF1-ROBOT)))

(:derived (Flag28-)(and (LEFTOF1-ROBOT)(RIGHTOF5-ROBOT)))

(:derived (Flag29-)(and (LEFTOF1-ROBOT)(RIGHTOF2-ROBOT)))

(:derived (Flag30-)(and (RIGHTOF8-ROBOT)(LEFTOF1-ROBOT)))

(:derived (Flag31-)(and (LEFTOF1-ROBOT)(RIGHTOF6-ROBOT)))

(:derived (Flag33-)(and (RIGHTOF5-ROBOT)(LEFTOF2-ROBOT)))

(:derived (Flag34-)(and (RIGHTOF8-ROBOT)(LEFTOF2-ROBOT)))

(:derived (Flag35-)(and (LEFTOF2-ROBOT)(RIGHTOF1-ROBOT)))

(:derived (Flag36-)(and (LEFTOF2-ROBOT)(RIGHTOF9-ROBOT)))

(:derived (Flag37-)(and (LEFTOF2-ROBOT)(RIGHTOF7-ROBOT)))

(:derived (Flag38-)(and (RIGHTOF5-ROBOT)(LEFTOF1-ROBOT)))

(:derived (Flag39-)(and (RIGHTOF2-ROBOT)(LEFTOF2-ROBOT)))

(:derived (Flag40-)(and (RIGHTOF3-ROBOT)(LEFTOF2-ROBOT)))

(:derived (Flag41-)(and (LEFTOF2-ROBOT)(RIGHTOF6-ROBOT)))

(:derived (Flag42-)(and (LEFTOF2-ROBOT)(RIGHTOF4-ROBOT)))

(:derived (Flag44-)(and (LEFTOF3-ROBOT)(RIGHTOF6-ROBOT)))

(:derived (Flag45-)(and (LEFTOF3-ROBOT)(RIGHTOF4-ROBOT)))

(:derived (Flag46-)(and (LEFTOF3-ROBOT)(RIGHTOF7-ROBOT)))

(:derived (Flag47-)(and (LEFTOF3-ROBOT)(RIGHTOF8-ROBOT)))

(:derived (Flag48-)(and (LEFTOF3-ROBOT)(RIGHTOF5-ROBOT)))

(:derived (Flag49-)(and (RIGHTOF3-ROBOT)(LEFTOF3-ROBOT)))

(:derived (Flag50-)(and (LEFTOF3-ROBOT)(RIGHTOF9-ROBOT)))

(:derived (Flag51-)(and (LEFTOF3-ROBOT)(RIGHTOF2-ROBOT)))

(:derived (Flag53-)(and (LEFTOF4-ROBOT)(RIGHTOF4-ROBOT)))

(:derived (Flag54-)(and (LEFTOF4-ROBOT)(RIGHTOF7-ROBOT)))

(:derived (Flag55-)(and (LEFTOF3-ROBOT)(RIGHTOF3-ROBOT)))

(:derived (Flag56-)(and (RIGHTOF8-ROBOT)(LEFTOF4-ROBOT)))

(:derived (Flag57-)(and (LEFTOF4-ROBOT)(RIGHTOF9-ROBOT)))

(:derived (Flag58-)(and (LEFTOF4-ROBOT)(RIGHTOF6-ROBOT)))

(:derived (Flag59-)(and (LEFTOF4-ROBOT)(RIGHTOF5-ROBOT)))

(:derived (Flag60-)(and (RIGHTOF3-ROBOT)(LEFTOF4-ROBOT)))

(:derived (Flag62-)(and (RIGHTOF9-ROBOT)(LEFTOF5-ROBOT)))

(:derived (Flag63-)(and (RIGHTOF7-ROBOT)(LEFTOF5-ROBOT)))

(:derived (Flag64-)(and (RIGHTOF4-ROBOT)(LEFTOF5-ROBOT)))

(:derived (Flag65-)(and (LEFTOF4-ROBOT)(RIGHTOF8-ROBOT)))

(:derived (Flag66-)(and (LEFTOF5-ROBOT)(RIGHTOF6-ROBOT)))

(:derived (Flag67-)(and (RIGHTOF8-ROBOT)(LEFTOF5-ROBOT)))

(:derived (Flag68-)(and (RIGHTOF5-ROBOT)(LEFTOF5-ROBOT)))

(:derived (Flag70-)(and (RIGHTOF8-ROBOT)(LEFTOF6-ROBOT)))

(:derived (Flag71-)(and (RIGHTOF5-ROBOT)(LEFTOF6-ROBOT)))

(:derived (Flag72-)(and (RIGHTOF7-ROBOT)(LEFTOF6-ROBOT)))

(:derived (Flag73-)(and (RIGHTOF6-ROBOT)(LEFTOF6-ROBOT)))

(:derived (Flag74-)(and (RIGHTOF9-ROBOT)(LEFTOF6-ROBOT)))

(:derived (Flag76-)(and (LEFTOF7-ROBOT)(RIGHTOF7-ROBOT)))

(:derived (Flag77-)(and (RIGHTOF8-ROBOT)(LEFTOF7-ROBOT)))

(:derived (Flag78-)(and (LEFTOF6-ROBOT)(RIGHTOF6-ROBOT)))

(:derived (Flag79-)(and (LEFTOF7-ROBOT)(RIGHTOF9-ROBOT)))

(:derived (Flag80-)(and (LEFTOF7-ROBOT)(RIGHTOF6-ROBOT)))

(:derived (Flag82-)(and (RIGHTOF7-ROBOT)(LEFTOF8-ROBOT)))

(:derived (Flag83-)(and (LEFTOF8-ROBOT)(RIGHTOF9-ROBOT)))

(:derived (Flag84-)(and (RIGHTOF8-ROBOT)(LEFTOF8-ROBOT)))

(:derived (Flag85-)(and (RIGHTOF7-ROBOT)(LEFTOF7-ROBOT)))

(:derived (Flag87-)(and (LEFTOF9-ROBOT)(RIGHTOF9-ROBOT)))

(:derived (Flag88-)(and (RIGHTOF8-ROBOT)(LEFTOF9-ROBOT)))

(:derived (Flag125-)(and (Flag107-)))

(:derived (Flag125-)(and (Flag108-)))

(:derived (Flag125-)(and (Flag109-)))

(:derived (Flag125-)(and (Flag110-)))

(:derived (Flag125-)(and (Flag111-)))

(:derived (Flag125-)(and (Flag112-)))

(:derived (Flag125-)(and (Flag113-)))

(:derived (Flag125-)(and (Flag114-)))

(:derived (Flag125-)(and (Flag115-)))

(:derived (Flag125-)(and (Flag116-)))

(:derived (Flag125-)(and (Flag117-)))

(:derived (Flag125-)(and (Flag118-)))

(:derived (Flag125-)(and (Flag119-)))

(:derived (Flag125-)(and (Flag120-)))

(:derived (Flag125-)(and (Flag121-)))

(:derived (Flag125-)(and (Flag122-)))

(:derived (Flag125-)(and (Flag123-)))

(:derived (Flag125-)(and (Flag124-)))

(:derived (Flag135-)(and (Flag126-)))

(:derived (Flag135-)(and (Flag127-)))

(:derived (Flag135-)(and (Flag107-)))

(:derived (Flag135-)(and (Flag128-)))

(:derived (Flag135-)(and (Flag120-)))

(:derived (Flag135-)(and (Flag112-)))

(:derived (Flag135-)(and (Flag114-)))

(:derived (Flag135-)(and (Flag129-)))

(:derived (Flag135-)(and (Flag115-)))

(:derived (Flag135-)(and (Flag130-)))

(:derived (Flag135-)(and (Flag108-)))

(:derived (Flag135-)(and (Flag110-)))

(:derived (Flag135-)(and (Flag111-)))

(:derived (Flag135-)(and (Flag131-)))

(:derived (Flag135-)(and (Flag113-)))

(:derived (Flag135-)(and (Flag132-)))

(:derived (Flag135-)(and (Flag133-)))

(:derived (Flag135-)(and (Flag119-)))

(:derived (Flag135-)(and (Flag121-)))

(:derived (Flag135-)(and (Flag123-)))

(:derived (Flag135-)(and (Flag134-)))

(:derived (Flag135-)(and (Flag116-)))

(:derived (Flag135-)(and (Flag118-)))

(:derived (Flag135-)(and (Flag122-)))

(:derived (Flag144-)(and (Flag126-)))

(:derived (Flag144-)(and (Flag136-)))

(:derived (Flag144-)(and (Flag137-)))

(:derived (Flag144-)(and (Flag127-)))

(:derived (Flag144-)(and (Flag128-)))

(:derived (Flag144-)(and (Flag120-)))

(:derived (Flag144-)(and (Flag129-)))

(:derived (Flag144-)(and (Flag112-)))

(:derived (Flag144-)(and (Flag138-)))

(:derived (Flag144-)(and (Flag114-)))

(:derived (Flag144-)(and (Flag139-)))

(:derived (Flag144-)(and (Flag140-)))

(:derived (Flag144-)(and (Flag141-)))

(:derived (Flag144-)(and (Flag115-)))

(:derived (Flag144-)(and (Flag142-)))

(:derived (Flag144-)(and (Flag143-)))

(:derived (Flag144-)(and (Flag108-)))

(:derived (Flag144-)(and (Flag111-)))

(:derived (Flag144-)(and (Flag131-)))

(:derived (Flag144-)(and (Flag113-)))

(:derived (Flag144-)(and (Flag132-)))

(:derived (Flag144-)(and (Flag119-)))

(:derived (Flag144-)(and (Flag121-)))

(:derived (Flag144-)(and (Flag123-)))

(:derived (Flag144-)(and (Flag110-)))

(:derived (Flag144-)(and (Flag116-)))

(:derived (Flag144-)(and (Flag118-)))

(:derived (Flag144-)(and (Flag122-)))

(:derived (Flag151-)(and (Flag145-)))

(:derived (Flag151-)(and (Flag126-)))

(:derived (Flag151-)(and (Flag127-)))

(:derived (Flag151-)(and (Flag128-)))

(:derived (Flag151-)(and (Flag120-)))

(:derived (Flag151-)(and (Flag129-)))

(:derived (Flag151-)(and (Flag146-)))

(:derived (Flag151-)(and (Flag112-)))

(:derived (Flag151-)(and (Flag116-)))

(:derived (Flag151-)(and (Flag138-)))

(:derived (Flag151-)(and (Flag114-)))

(:derived (Flag151-)(and (Flag139-)))

(:derived (Flag151-)(and (Flag140-)))

(:derived (Flag151-)(and (Flag141-)))

(:derived (Flag151-)(and (Flag115-)))

(:derived (Flag151-)(and (Flag142-)))

(:derived (Flag151-)(and (Flag108-)))

(:derived (Flag151-)(and (Flag111-)))

(:derived (Flag151-)(and (Flag131-)))

(:derived (Flag151-)(and (Flag113-)))

(:derived (Flag151-)(and (Flag147-)))

(:derived (Flag151-)(and (Flag132-)))

(:derived (Flag151-)(and (Flag148-)))

(:derived (Flag151-)(and (Flag137-)))

(:derived (Flag151-)(and (Flag121-)))

(:derived (Flag151-)(and (Flag123-)))

(:derived (Flag151-)(and (Flag110-)))

(:derived (Flag151-)(and (Flag149-)))

(:derived (Flag151-)(and (Flag150-)))

(:derived (Flag151-)(and (Flag118-)))

(:derived (Flag158-)(and (Flag126-)))

(:derived (Flag158-)(and (Flag127-)))

(:derived (Flag158-)(and (Flag128-)))

(:derived (Flag158-)(and (Flag120-)))

(:derived (Flag158-)(and (Flag152-)))

(:derived (Flag158-)(and (Flag146-)))

(:derived (Flag158-)(and (Flag121-)))

(:derived (Flag158-)(and (Flag111-)))

(:derived (Flag158-)(and (Flag112-)))

(:derived (Flag158-)(and (Flag116-)))

(:derived (Flag158-)(and (Flag138-)))

(:derived (Flag158-)(and (Flag114-)))

(:derived (Flag158-)(and (Flag153-)))

(:derived (Flag158-)(and (Flag154-)))

(:derived (Flag158-)(and (Flag129-)))

(:derived (Flag158-)(and (Flag140-)))

(:derived (Flag158-)(and (Flag141-)))

(:derived (Flag158-)(and (Flag142-)))

(:derived (Flag158-)(and (Flag108-)))

(:derived (Flag158-)(and (Flag113-)))

(:derived (Flag158-)(and (Flag131-)))

(:derived (Flag158-)(and (Flag155-)))

(:derived (Flag158-)(and (Flag148-)))

(:derived (Flag158-)(and (Flag156-)))

(:derived (Flag158-)(and (Flag137-)))

(:derived (Flag158-)(and (Flag157-)))

(:derived (Flag158-)(and (Flag123-)))

(:derived (Flag158-)(and (Flag149-)))

(:derived (Flag158-)(and (Flag150-)))

(:derived (Flag158-)(and (Flag118-)))

(:derived (Flag164-)(and (Flag159-)))

(:derived (Flag164-)(and (Flag127-)))

(:derived (Flag164-)(and (Flag128-)))

(:derived (Flag164-)(and (Flag160-)))

(:derived (Flag164-)(and (Flag161-)))

(:derived (Flag164-)(and (Flag120-)))

(:derived (Flag164-)(and (Flag146-)))

(:derived (Flag164-)(and (Flag121-)))

(:derived (Flag164-)(and (Flag112-)))

(:derived (Flag164-)(and (Flag138-)))

(:derived (Flag164-)(and (Flag153-)))

(:derived (Flag164-)(and (Flag129-)))

(:derived (Flag164-)(and (Flag140-)))

(:derived (Flag164-)(and (Flag141-)))

(:derived (Flag164-)(and (Flag108-)))

(:derived (Flag164-)(and (Flag111-)))

(:derived (Flag164-)(and (Flag131-)))

(:derived (Flag164-)(and (Flag113-)))

(:derived (Flag164-)(and (Flag148-)))

(:derived (Flag164-)(and (Flag156-)))

(:derived (Flag164-)(and (Flag137-)))

(:derived (Flag164-)(and (Flag157-)))

(:derived (Flag164-)(and (Flag123-)))

(:derived (Flag164-)(and (Flag149-)))

(:derived (Flag164-)(and (Flag162-)))

(:derived (Flag164-)(and (Flag163-)))

(:derived (Flag164-)(and (Flag150-)))

(:derived (Flag164-)(and (Flag118-)))

(:derived (Flag169-)(and (Flag128-)))

(:derived (Flag169-)(and (Flag160-)))

(:derived (Flag169-)(and (Flag120-)))

(:derived (Flag169-)(and (Flag146-)))

(:derived (Flag169-)(and (Flag121-)))

(:derived (Flag169-)(and (Flag112-)))

(:derived (Flag169-)(and (Flag138-)))

(:derived (Flag169-)(and (Flag153-)))

(:derived (Flag169-)(and (Flag140-)))

(:derived (Flag169-)(and (Flag129-)))

(:derived (Flag169-)(and (Flag141-)))

(:derived (Flag169-)(and (Flag165-)))

(:derived (Flag169-)(and (Flag111-)))

(:derived (Flag169-)(and (Flag131-)))

(:derived (Flag169-)(and (Flag148-)))

(:derived (Flag169-)(and (Flag166-)))

(:derived (Flag169-)(and (Flag156-)))

(:derived (Flag169-)(and (Flag157-)))

(:derived (Flag169-)(and (Flag123-)))

(:derived (Flag169-)(and (Flag167-)))

(:derived (Flag169-)(and (Flag149-)))

(:derived (Flag169-)(and (Flag163-)))

(:derived (Flag169-)(and (Flag168-)))

(:derived (Flag169-)(and (Flag118-)))

(:derived (Flag173-)(and (Flag149-)))

(:derived (Flag173-)(and (Flag170-)))

(:derived (Flag173-)(and (Flag146-)))

(:derived (Flag173-)(and (Flag171-)))

(:derived (Flag173-)(and (Flag111-)))

(:derived (Flag173-)(and (Flag118-)))

(:derived (Flag173-)(and (Flag131-)))

(:derived (Flag173-)(and (Flag138-)))

(:derived (Flag173-)(and (Flag163-)))

(:derived (Flag173-)(and (Flag166-)))

(:derived (Flag173-)(and (Flag140-)))

(:derived (Flag173-)(and (Flag129-)))

(:derived (Flag173-)(and (Flag156-)))

(:derived (Flag173-)(and (Flag172-)))

(:derived (Flag173-)(and (Flag160-)))

(:derived (Flag173-)(and (Flag121-)))

(:derived (Flag173-)(and (Flag157-)))

(:derived (Flag173-)(and (Flag123-)))

(:derived (Flag176-)(and (Flag149-)))

(:derived (Flag176-)(and (Flag111-)))

(:derived (Flag176-)(and (Flag138-)))

(:derived (Flag176-)(and (Flag157-)))

(:derived (Flag176-)(and (Flag166-)))

(:derived (Flag176-)(and (Flag129-)))

(:derived (Flag176-)(and (Flag174-)))

(:derived (Flag176-)(and (Flag160-)))

(:derived (Flag176-)(and (Flag121-)))

(:derived (Flag176-)(and (Flag175-)))

(:derived (Flag177-)(and (LEFTOF10-ROBOT)))

(:derived (Flag177-)(and (LEFTOF2-ROBOT)))

(:derived (Flag177-)(and (LEFTOF7-ROBOT)))

(:derived (Flag177-)(and (LEFTOF1-ROBOT)))

(:derived (Flag177-)(and (LEFTOF6-ROBOT)))

(:derived (Flag177-)(and (LEFTOF5-ROBOT)))

(:derived (Flag177-)(and (LEFTOF3-ROBOT)))

(:derived (Flag177-)(and (LEFTOF4-ROBOT)))

(:derived (Flag177-)(and (LEFTOF8-ROBOT)))

(:derived (Flag178-)(and (LEFTOF10-ROBOT)(RIGHTOF9-ROBOT)))

(:derived (Flag180-)(and (Flag110-)))

(:derived (Flag180-)(and (Flag107-)))

(:derived (Flag180-)(and (Flag113-)))

(:derived (Flag180-)(and (Flag112-)))

(:derived (Flag180-)(and (Flag117-)))

(:derived (Flag180-)(and (Flag114-)))

(:derived (Flag180-)(and (Flag118-)))

(:derived (Flag180-)(and (Flag119-)))

(:derived (Flag180-)(and (Flag121-)))

(:action MOVELEFT-ROBOT
:parameters ()
:precondition
(and
(Flag4-)
)
:effect
(and
(when
(and
(Flag179-)
)
(and
(COLUMN8-ROBOT)
(not (NOT-COLUMN8-ROBOT))
)
)
(when
(and
(Flag89-)
)
(and
(COLUMN7-ROBOT)
(not (NOT-COLUMN7-ROBOT))
)
)
(when
(and
(Flag86-)
)
(and
(COLUMN6-ROBOT)
(not (NOT-COLUMN6-ROBOT))
)
)
(when
(and
(Flag81-)
)
(and
(COLUMN5-ROBOT)
(not (NOT-COLUMN5-ROBOT))
)
)
(when
(and
(Flag75-)
)
(and
(COLUMN4-ROBOT)
(not (NOT-COLUMN4-ROBOT))
)
)
(when
(and
(Flag69-)
)
(and
(COLUMN3-ROBOT)
(not (NOT-COLUMN3-ROBOT))
)
)
(when
(and
(Flag61-)
)
(and
(COLUMN2-ROBOT)
(not (NOT-COLUMN2-ROBOT))
)
)
(when
(and
(Flag52-)
)
(and
(COLUMN1-ROBOT)
(not (NOT-COLUMN1-ROBOT))
)
)
(when
(and
(Flag43-)
)
(and
(COLUMN0-ROBOT)
(not (NOT-COLUMN0-ROBOT))
)
)
(when
(and
(COLUMN9-ROBOT)
)
(and
(COLUMN8-ROBOT)
(NOT-COLUMN9-ROBOT)
(not (NOT-COLUMN8-ROBOT))
(not (COLUMN9-ROBOT))
)
)
(when
(and
(COLUMN8-ROBOT)
)
(and
(COLUMN7-ROBOT)
(NOT-COLUMN8-ROBOT)
(not (NOT-COLUMN7-ROBOT))
(not (COLUMN8-ROBOT))
)
)
(when
(and
(COLUMN7-ROBOT)
)
(and
(COLUMN6-ROBOT)
(NOT-COLUMN7-ROBOT)
(not (NOT-COLUMN6-ROBOT))
(not (COLUMN7-ROBOT))
)
)
(when
(and
(COLUMN6-ROBOT)
)
(and
(COLUMN5-ROBOT)
(NOT-COLUMN6-ROBOT)
(not (NOT-COLUMN5-ROBOT))
(not (COLUMN6-ROBOT))
)
)
(when
(and
(COLUMN5-ROBOT)
)
(and
(COLUMN4-ROBOT)
(NOT-COLUMN5-ROBOT)
(not (NOT-COLUMN4-ROBOT))
(not (COLUMN5-ROBOT))
)
)
(when
(and
(COLUMN4-ROBOT)
)
(and
(COLUMN3-ROBOT)
(NOT-COLUMN4-ROBOT)
(not (NOT-COLUMN3-ROBOT))
(not (COLUMN4-ROBOT))
)
)
(when
(and
(COLUMN3-ROBOT)
)
(and
(COLUMN2-ROBOT)
(NOT-COLUMN3-ROBOT)
(not (NOT-COLUMN2-ROBOT))
(not (COLUMN3-ROBOT))
)
)
(when
(and
(COLUMN2-ROBOT)
)
(and
(COLUMN1-ROBOT)
(NOT-COLUMN2-ROBOT)
(not (NOT-COLUMN1-ROBOT))
(not (COLUMN2-ROBOT))
)
)
(when
(and
(COLUMN1-ROBOT)
)
(and
(COLUMN0-ROBOT)
(NOT-COLUMN1-ROBOT)
(not (NOT-COLUMN0-ROBOT))
(not (COLUMN1-ROBOT))
)
)
(when
(and
(RIGHTOF9-ROBOT)
)
(and
(RIGHTOF8-ROBOT)
(NOT-RIGHTOF9-ROBOT)
(RIGHTOF7-ROBOT)
(NOT-RIGHTOF8-ROBOT)
(RIGHTOF6-ROBOT)
(NOT-RIGHTOF7-ROBOT)
(RIGHTOF5-ROBOT)
(NOT-RIGHTOF6-ROBOT)
(RIGHTOF4-ROBOT)
(NOT-RIGHTOF5-ROBOT)
(RIGHTOF3-ROBOT)
(NOT-RIGHTOF4-ROBOT)
(RIGHTOF2-ROBOT)
(NOT-RIGHTOF3-ROBOT)
(RIGHTOF1-ROBOT)
(NOT-RIGHTOF2-ROBOT)
(RIGHTOF0-ROBOT)
(NOT-RIGHTOF1-ROBOT)
(not (RIGHTOF9-ROBOT))
(not (NOT-RIGHTOF0-ROBOT))
)
)
(when
(and
(RIGHTOF8-ROBOT)
)
(and
(RIGHTOF7-ROBOT)
(NOT-RIGHTOF8-ROBOT)
(RIGHTOF6-ROBOT)
(NOT-RIGHTOF7-ROBOT)
(RIGHTOF5-ROBOT)
(NOT-RIGHTOF6-ROBOT)
(RIGHTOF4-ROBOT)
(NOT-RIGHTOF5-ROBOT)
(RIGHTOF3-ROBOT)
(NOT-RIGHTOF4-ROBOT)
(RIGHTOF2-ROBOT)
(NOT-RIGHTOF3-ROBOT)
(RIGHTOF1-ROBOT)
(NOT-RIGHTOF2-ROBOT)
(RIGHTOF0-ROBOT)
(NOT-RIGHTOF1-ROBOT)
(not (RIGHTOF8-ROBOT))
(not (NOT-RIGHTOF0-ROBOT))
)
)
(when
(and
(RIGHTOF7-ROBOT)
)
(and
(RIGHTOF6-ROBOT)
(NOT-RIGHTOF7-ROBOT)
(RIGHTOF5-ROBOT)
(NOT-RIGHTOF6-ROBOT)
(RIGHTOF4-ROBOT)
(NOT-RIGHTOF5-ROBOT)
(RIGHTOF3-ROBOT)
(NOT-RIGHTOF4-ROBOT)
(RIGHTOF2-ROBOT)
(NOT-RIGHTOF3-ROBOT)
(RIGHTOF1-ROBOT)
(NOT-RIGHTOF2-ROBOT)
(RIGHTOF0-ROBOT)
(NOT-RIGHTOF1-ROBOT)
(not (RIGHTOF7-ROBOT))
(not (NOT-RIGHTOF0-ROBOT))
)
)
(when
(and
(RIGHTOF6-ROBOT)
)
(and
(RIGHTOF5-ROBOT)
(NOT-RIGHTOF6-ROBOT)
(RIGHTOF4-ROBOT)
(NOT-RIGHTOF5-ROBOT)
(RIGHTOF3-ROBOT)
(NOT-RIGHTOF4-ROBOT)
(RIGHTOF2-ROBOT)
(NOT-RIGHTOF3-ROBOT)
(RIGHTOF1-ROBOT)
(NOT-RIGHTOF2-ROBOT)
(RIGHTOF0-ROBOT)
(NOT-RIGHTOF1-ROBOT)
(not (RIGHTOF6-ROBOT))
(not (NOT-RIGHTOF0-ROBOT))
)
)
(when
(and
(RIGHTOF5-ROBOT)
)
(and
(RIGHTOF4-ROBOT)
(NOT-RIGHTOF5-ROBOT)
(RIGHTOF3-ROBOT)
(NOT-RIGHTOF4-ROBOT)
(RIGHTOF2-ROBOT)
(NOT-RIGHTOF3-ROBOT)
(RIGHTOF1-ROBOT)
(NOT-RIGHTOF2-ROBOT)
(RIGHTOF0-ROBOT)
(NOT-RIGHTOF1-ROBOT)
(not (RIGHTOF5-ROBOT))
(not (NOT-RIGHTOF0-ROBOT))
)
)
(when
(and
(RIGHTOF4-ROBOT)
)
(and
(RIGHTOF3-ROBOT)
(NOT-RIGHTOF4-ROBOT)
(RIGHTOF2-ROBOT)
(NOT-RIGHTOF3-ROBOT)
(RIGHTOF1-ROBOT)
(NOT-RIGHTOF2-ROBOT)
(RIGHTOF0-ROBOT)
(NOT-RIGHTOF1-ROBOT)
(not (RIGHTOF4-ROBOT))
(not (NOT-RIGHTOF0-ROBOT))
)
)
(when
(and
(RIGHTOF3-ROBOT)
)
(and
(RIGHTOF2-ROBOT)
(NOT-RIGHTOF3-ROBOT)
(RIGHTOF1-ROBOT)
(NOT-RIGHTOF2-ROBOT)
(RIGHTOF0-ROBOT)
(NOT-RIGHTOF1-ROBOT)
(not (RIGHTOF3-ROBOT))
(not (NOT-RIGHTOF0-ROBOT))
)
)
(when
(and
(RIGHTOF2-ROBOT)
)
(and
(RIGHTOF1-ROBOT)
(NOT-RIGHTOF2-ROBOT)
(RIGHTOF0-ROBOT)
(NOT-RIGHTOF1-ROBOT)
(not (RIGHTOF2-ROBOT))
(not (NOT-RIGHTOF0-ROBOT))
)
)
(when
(and
(RIGHTOF1-ROBOT)
)
(and
(RIGHTOF0-ROBOT)
(NOT-RIGHTOF1-ROBOT)
(not (NOT-RIGHTOF0-ROBOT))
(not (RIGHTOF1-ROBOT))
)
)
(when
(and
(Flag177-)
)
(and
(LEFTOF9-ROBOT)
(not (NOT-LEFTOF9-ROBOT))
)
)
(when
(and
(LEFTOF8-ROBOT)
)
(and
(LEFTOF7-ROBOT)
(not (NOT-LEFTOF8-ROBOT))
(not (NOT-LEFTOF7-ROBOT))
)
)
(when
(and
(LEFTOF4-ROBOT)
)
(and
(LEFTOF8-ROBOT)
(LEFTOF7-ROBOT)
(LEFTOF6-ROBOT)
(LEFTOF5-ROBOT)
(LEFTOF3-ROBOT)
(not (NOT-LEFTOF8-ROBOT))
(not (NOT-LEFTOF7-ROBOT))
(not (NOT-LEFTOF6-ROBOT))
(not (NOT-LEFTOF5-ROBOT))
(not (NOT-LEFTOF4-ROBOT))
(not (NOT-LEFTOF3-ROBOT))
)
)
(when
(and
(LEFTOF3-ROBOT)
)
(and
(LEFTOF8-ROBOT)
(LEFTOF7-ROBOT)
(LEFTOF6-ROBOT)
(LEFTOF5-ROBOT)
(LEFTOF4-ROBOT)
(LEFTOF2-ROBOT)
(not (NOT-LEFTOF8-ROBOT))
(not (NOT-LEFTOF7-ROBOT))
(not (NOT-LEFTOF6-ROBOT))
(not (NOT-LEFTOF5-ROBOT))
(not (NOT-LEFTOF4-ROBOT))
(not (NOT-LEFTOF3-ROBOT))
(not (NOT-LEFTOF2-ROBOT))
)
)
(when
(and
(LEFTOF5-ROBOT)
)
(and
(LEFTOF8-ROBOT)
(LEFTOF7-ROBOT)
(LEFTOF6-ROBOT)
(LEFTOF4-ROBOT)
(not (NOT-LEFTOF8-ROBOT))
(not (NOT-LEFTOF7-ROBOT))
(not (NOT-LEFTOF6-ROBOT))
(not (NOT-LEFTOF5-ROBOT))
(not (NOT-LEFTOF4-ROBOT))
)
)
(when
(and
(LEFTOF6-ROBOT)
)
(and
(LEFTOF8-ROBOT)
(LEFTOF7-ROBOT)
(LEFTOF5-ROBOT)
(not (NOT-LEFTOF8-ROBOT))
(not (NOT-LEFTOF7-ROBOT))
(not (NOT-LEFTOF6-ROBOT))
(not (NOT-LEFTOF5-ROBOT))
)
)
(when
(and
(LEFTOF1-ROBOT)
)
(and
(LEFTOF8-ROBOT)
(LEFTOF7-ROBOT)
(LEFTOF6-ROBOT)
(LEFTOF5-ROBOT)
(LEFTOF4-ROBOT)
(LEFTOF3-ROBOT)
(LEFTOF2-ROBOT)
(not (NOT-LEFTOF8-ROBOT))
(not (NOT-LEFTOF7-ROBOT))
(not (NOT-LEFTOF6-ROBOT))
(not (NOT-LEFTOF5-ROBOT))
(not (NOT-LEFTOF4-ROBOT))
(not (NOT-LEFTOF3-ROBOT))
(not (NOT-LEFTOF2-ROBOT))
(not (NOT-LEFTOF1-ROBOT))
)
)
(when
(and
(LEFTOF7-ROBOT)
)
(and
(LEFTOF8-ROBOT)
(LEFTOF6-ROBOT)
(not (NOT-LEFTOF8-ROBOT))
(not (NOT-LEFTOF7-ROBOT))
(not (NOT-LEFTOF6-ROBOT))
)
)
(when
(and
(LEFTOF2-ROBOT)
)
(and
(LEFTOF8-ROBOT)
(LEFTOF7-ROBOT)
(LEFTOF6-ROBOT)
(LEFTOF5-ROBOT)
(LEFTOF4-ROBOT)
(LEFTOF3-ROBOT)
(LEFTOF1-ROBOT)
(not (NOT-LEFTOF8-ROBOT))
(not (NOT-LEFTOF7-ROBOT))
(not (NOT-LEFTOF6-ROBOT))
(not (NOT-LEFTOF5-ROBOT))
(not (NOT-LEFTOF4-ROBOT))
(not (NOT-LEFTOF3-ROBOT))
(not (NOT-LEFTOF2-ROBOT))
(not (NOT-LEFTOF1-ROBOT))
)
)
(when
(and
(LEFTOF9-ROBOT)
)
(and
(LEFTOF8-ROBOT)
(not (NOT-LEFTOF8-ROBOT))
)
)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action MOVERIGHT-ROBOT
:parameters ()
:precondition
(and
(Flag4-)
)
:effect
(and
(when
(and
(Flag89-)
)
(and
(COLUMN9-ROBOT)
(not (NOT-COLUMN9-ROBOT))
)
)
(when
(and
(Flag86-)
)
(and
(COLUMN8-ROBOT)
(not (NOT-COLUMN8-ROBOT))
)
)
(when
(and
(Flag81-)
)
(and
(COLUMN7-ROBOT)
(not (NOT-COLUMN7-ROBOT))
)
)
(when
(and
(Flag75-)
)
(and
(COLUMN6-ROBOT)
(not (NOT-COLUMN6-ROBOT))
)
)
(when
(and
(Flag69-)
)
(and
(COLUMN5-ROBOT)
(not (NOT-COLUMN5-ROBOT))
)
)
(when
(and
(Flag61-)
)
(and
(COLUMN4-ROBOT)
(not (NOT-COLUMN4-ROBOT))
)
)
(when
(and
(Flag52-)
)
(and
(COLUMN3-ROBOT)
(not (NOT-COLUMN3-ROBOT))
)
)
(when
(and
(Flag43-)
)
(and
(COLUMN2-ROBOT)
(not (NOT-COLUMN2-ROBOT))
)
)
(when
(and
(Flag32-)
)
(and
(COLUMN1-ROBOT)
(not (NOT-COLUMN1-ROBOT))
)
)
(when
(and
(COLUMN8-ROBOT)
)
(and
(COLUMN9-ROBOT)
(NOT-COLUMN8-ROBOT)
(not (NOT-COLUMN9-ROBOT))
(not (COLUMN8-ROBOT))
)
)
(when
(and
(COLUMN7-ROBOT)
)
(and
(COLUMN8-ROBOT)
(NOT-COLUMN7-ROBOT)
(not (NOT-COLUMN8-ROBOT))
(not (COLUMN7-ROBOT))
)
)
(when
(and
(COLUMN6-ROBOT)
)
(and
(COLUMN7-ROBOT)
(NOT-COLUMN6-ROBOT)
(not (NOT-COLUMN7-ROBOT))
(not (COLUMN6-ROBOT))
)
)
(when
(and
(COLUMN5-ROBOT)
)
(and
(COLUMN6-ROBOT)
(NOT-COLUMN5-ROBOT)
(not (NOT-COLUMN6-ROBOT))
(not (COLUMN5-ROBOT))
)
)
(when
(and
(COLUMN4-ROBOT)
)
(and
(COLUMN5-ROBOT)
(NOT-COLUMN4-ROBOT)
(not (NOT-COLUMN5-ROBOT))
(not (COLUMN4-ROBOT))
)
)
(when
(and
(COLUMN3-ROBOT)
)
(and
(COLUMN4-ROBOT)
(NOT-COLUMN3-ROBOT)
(not (NOT-COLUMN4-ROBOT))
(not (COLUMN3-ROBOT))
)
)
(when
(and
(COLUMN2-ROBOT)
)
(and
(COLUMN3-ROBOT)
(NOT-COLUMN2-ROBOT)
(not (NOT-COLUMN3-ROBOT))
(not (COLUMN2-ROBOT))
)
)
(when
(and
(COLUMN1-ROBOT)
)
(and
(COLUMN2-ROBOT)
(NOT-COLUMN1-ROBOT)
(not (NOT-COLUMN2-ROBOT))
(not (COLUMN1-ROBOT))
)
)
(when
(and
(COLUMN0-ROBOT)
)
(and
(COLUMN1-ROBOT)
(NOT-COLUMN0-ROBOT)
(not (NOT-COLUMN1-ROBOT))
(not (COLUMN0-ROBOT))
)
)
(when
(and
(Flag21-)
)
(and
(LEFTOF10-ROBOT)
(NOT-LEFTOF9-ROBOT)
(not (NOT-LEFTOF10-ROBOT))
(not (LEFTOF9-ROBOT))
)
)
(when
(and
(Flag20-)
)
(and
(LEFTOF9-ROBOT)
(NOT-LEFTOF8-ROBOT)
(not (NOT-LEFTOF9-ROBOT))
(not (LEFTOF8-ROBOT))
)
)
(when
(and
(Flag19-)
)
(and
(LEFTOF8-ROBOT)
(NOT-LEFTOF7-ROBOT)
(not (NOT-LEFTOF8-ROBOT))
(not (LEFTOF7-ROBOT))
)
)
(when
(and
(Flag18-)
)
(and
(LEFTOF7-ROBOT)
(NOT-LEFTOF6-ROBOT)
(not (NOT-LEFTOF7-ROBOT))
(not (LEFTOF6-ROBOT))
)
)
(when
(and
(Flag17-)
)
(and
(LEFTOF6-ROBOT)
(NOT-LEFTOF5-ROBOT)
(not (NOT-LEFTOF6-ROBOT))
(not (LEFTOF5-ROBOT))
)
)
(when
(and
(Flag16-)
)
(and
(LEFTOF5-ROBOT)
(NOT-LEFTOF4-ROBOT)
(not (NOT-LEFTOF5-ROBOT))
(not (LEFTOF4-ROBOT))
)
)
(when
(and
(Flag15-)
)
(and
(LEFTOF4-ROBOT)
(NOT-LEFTOF3-ROBOT)
(not (NOT-LEFTOF4-ROBOT))
(not (LEFTOF3-ROBOT))
)
)
(when
(and
(Flag14-)
)
(and
(LEFTOF3-ROBOT)
(NOT-LEFTOF2-ROBOT)
(not (NOT-LEFTOF3-ROBOT))
(not (LEFTOF2-ROBOT))
)
)
(when
(and
(LEFTOF1-ROBOT)
)
(and
(LEFTOF2-ROBOT)
(NOT-LEFTOF1-ROBOT)
(not (NOT-LEFTOF2-ROBOT))
(not (LEFTOF1-ROBOT))
)
)
(when
(and
(Flag13-)
)
(and
(RIGHTOF9-ROBOT)
(not (NOT-RIGHTOF9-ROBOT))
)
)
(when
(and
(Flag12-)
)
(and
(RIGHTOF8-ROBOT)
(not (NOT-RIGHTOF8-ROBOT))
)
)
(when
(and
(Flag11-)
)
(and
(RIGHTOF7-ROBOT)
(not (NOT-RIGHTOF7-ROBOT))
)
)
(when
(and
(Flag10-)
)
(and
(RIGHTOF6-ROBOT)
(not (NOT-RIGHTOF6-ROBOT))
)
)
(when
(and
(Flag9-)
)
(and
(RIGHTOF5-ROBOT)
(not (NOT-RIGHTOF5-ROBOT))
)
)
(when
(and
(Flag8-)
)
(and
(RIGHTOF4-ROBOT)
(not (NOT-RIGHTOF4-ROBOT))
)
)
(when
(and
(Flag7-)
)
(and
(RIGHTOF3-ROBOT)
(not (NOT-RIGHTOF3-ROBOT))
)
)
(when
(and
(Flag6-)
)
(and
(RIGHTOF2-ROBOT)
(not (NOT-RIGHTOF2-ROBOT))
)
)
(when
(and
(Flag5-)
)
(and
(RIGHTOF1-ROBOT)
(not (NOT-RIGHTOF1-ROBOT))
)
)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action CHECKCONSISTENCYACTION
:parameters ()
:precondition
(and
(NOT-ERROR-)
(CHECKCONSISTENCY-)
)
:effect
(and
(when
(and
(RIGHTOF9-ROBOT)
(LEFTOF1-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF3-ROBOT)
(RIGHTOF5-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF9-ROBOT)
(BELOWOF6-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF5-ROBOT)
(ABOVEOF9-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF8-ROBOT)
(BELOWOF1-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF5-ROBOT)
(RIGHTOF9-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF9-ROBOT)
(RIGHTOF9-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF1-ROBOT)
(RIGHTOF5-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF7-ROBOT)
(BELOWOF4-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF1-ROBOT)
(ABOVEOF9-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF1-ROBOT)
(ABOVEOF2-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF4-ROBOT)
(BELOWOF2-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF9-ROBOT)
(LEFTOF7-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF7-ROBOT)
(BELOWOF1-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF1-ROBOT)
(RIGHTOF8-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF1-ROBOT)
(RIGHTOF7-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF8-ROBOT)
(LEFTOF4-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF9-ROBOT)
(LEFTOF3-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF8-ROBOT)
(LEFTOF2-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF1-ROBOT)
(RIGHTOF6-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF9-ROBOT)
(LEFTOF8-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF8-ROBOT)
(LEFTOF3-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF4-ROBOT)
(BELOWOF1-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF3-ROBOT)
(ABOVEOF9-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF3-ROBOT)
(BELOWOF2-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF8-ROBOT)
(BELOWOF6-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF8-ROBOT)
(ABOVEOF9-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF3-ROBOT)
(BELOWOF1-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF6-ROBOT)
(BELOWOF2-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF7-ROBOT)
(LEFTOF5-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF9-ROBOT)
(LEFTOF6-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF7-ROBOT)
(BELOWOF2-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF7-ROBOT)
(LEFTOF3-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF5-ROBOT)
(BELOWOF3-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF7-ROBOT)
(BELOWOF3-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF9-ROBOT)
(BELOWOF2-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF4-ROBOT)
(RIGHTOF5-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF3-ROBOT)
(LEFTOF3-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF4-ROBOT)
(LEFTOF1-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF6-ROBOT)
(LEFTOF3-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF5-ROBOT)
(BELOWOF1-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF7-ROBOT)
(BELOWOF5-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF6-ROBOT)
(LEFTOF2-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF7-ROBOT)
(LEFTOF4-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF6-ROBOT)
(BELOWOF6-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF9-ROBOT)
(LEFTOF4-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF4-ROBOT)
(RIGHTOF6-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF8-ROBOT)
(BELOWOF2-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF6-ROBOT)
(BELOWOF5-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF6-ROBOT)
(LEFTOF6-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF8-ROBOT)
(BELOWOF4-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF5-ROBOT)
(LEFTOF2-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF7-ROBOT)
(ABOVEOF9-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF4-ROBOT)
(ABOVEOF9-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF5-ROBOT)
(BELOWOF2-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF9-ROBOT)
(LEFTOF2-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF7-ROBOT)
(LEFTOF2-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF4-ROBOT)
(LEFTOF3-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF8-ROBOT)
(BELOWOF5-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF6-ROBOT)
(ABOVEOF7-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF1-ROBOT)
(RIGHTOF2-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF2-ROBOT)
(RIGHTOF4-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF8-ROBOT)
(BELOWOF3-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF6-ROBOT)
(RIGHTOF8-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF6-ROBOT)
(BELOWOF3-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF2-ROBOT)
(RIGHTOF2-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF5-ROBOT)
(BELOWOF4-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF1-ROBOT)
(RIGHTOF1-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF8-ROBOT)
(LEFTOF5-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF8-ROBOT)
(RIGHTOF8-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF5-ROBOT)
(BELOWOF5-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF3-ROBOT)
(BELOWOF3-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF7-ROBOT)
(LEFTOF6-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF6-ROBOT)
(BELOWOF1-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF8-ROBOT)
(LEFTOF7-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF2-ROBOT)
(ABOVEOF2-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF3-ROBOT)
(ABOVEOF4-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF8-ROBOT)
(BELOWOF8-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF6-ROBOT)
(LEFTOF5-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF8-ROBOT)
(BELOWOF7-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF4-ROBOT)
(RIGHTOF4-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF7-ROBOT)
(RIGHTOF7-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF6-ROBOT)
(BELOWOF4-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF1-ROBOT)
(BELOWOF1-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF2-ROBOT)
(RIGHTOF3-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF5-ROBOT)
(LEFTOF5-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF7-ROBOT)
(BELOWOF7-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF1-ROBOT)
(RIGHTOF3-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF4-ROBOT)
(BELOWOF4-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF9-ROBOT)
(BELOWOF9-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(NOT-CHECKCONSISTENCY-)
(not (CHECKCONSISTENCY-))
)
)
(:derived (Flag4-)(and (NOT-CHECKCONSISTENCY-)(NOT-ERROR-)(Flag3-)))

(:derived (Flag90-)(and (BELOWOF2-ROBOT)))

(:derived (Flag90-)(and (BELOWOF1-ROBOT)))

(:derived (Flag91-)(and (BELOWOF2-ROBOT)))

(:derived (Flag91-)(and (BELOWOF1-ROBOT)))

(:derived (Flag91-)(and (BELOWOF3-ROBOT)))

(:derived (Flag92-)(and (BELOWOF2-ROBOT)))

(:derived (Flag92-)(and (BELOWOF1-ROBOT)))

(:derived (Flag92-)(and (BELOWOF3-ROBOT)))

(:derived (Flag92-)(and (BELOWOF4-ROBOT)))

(:derived (Flag93-)(and (BELOWOF2-ROBOT)))

(:derived (Flag93-)(and (BELOWOF1-ROBOT)))

(:derived (Flag93-)(and (BELOWOF3-ROBOT)))

(:derived (Flag93-)(and (BELOWOF4-ROBOT)))

(:derived (Flag93-)(and (BELOWOF5-ROBOT)))

(:derived (Flag94-)(and (BELOWOF6-ROBOT)))

(:derived (Flag94-)(and (BELOWOF5-ROBOT)))

(:derived (Flag94-)(and (BELOWOF2-ROBOT)))

(:derived (Flag94-)(and (BELOWOF1-ROBOT)))

(:derived (Flag94-)(and (BELOWOF4-ROBOT)))

(:derived (Flag94-)(and (BELOWOF3-ROBOT)))

(:derived (Flag95-)(and (BELOWOF6-ROBOT)))

(:derived (Flag95-)(and (BELOWOF5-ROBOT)))

(:derived (Flag95-)(and (BELOWOF2-ROBOT)))

(:derived (Flag95-)(and (BELOWOF1-ROBOT)))

(:derived (Flag95-)(and (BELOWOF4-ROBOT)))

(:derived (Flag95-)(and (BELOWOF7-ROBOT)))

(:derived (Flag95-)(and (BELOWOF3-ROBOT)))

(:derived (Flag96-)(and (BELOWOF6-ROBOT)))

(:derived (Flag96-)(and (BELOWOF5-ROBOT)))

(:derived (Flag96-)(and (BELOWOF2-ROBOT)))

(:derived (Flag96-)(and (BELOWOF1-ROBOT)))

(:derived (Flag96-)(and (BELOWOF4-ROBOT)))

(:derived (Flag96-)(and (BELOWOF7-ROBOT)))

(:derived (Flag96-)(and (BELOWOF3-ROBOT)))

(:derived (Flag96-)(and (BELOWOF8-ROBOT)))

(:derived (Flag97-)(and (BELOWOF6-ROBOT)))

(:derived (Flag97-)(and (BELOWOF5-ROBOT)))

(:derived (Flag97-)(and (BELOWOF2-ROBOT)))

(:derived (Flag97-)(and (BELOWOF1-ROBOT)))

(:derived (Flag97-)(and (BELOWOF4-ROBOT)))

(:derived (Flag97-)(and (BELOWOF7-ROBOT)))

(:derived (Flag97-)(and (BELOWOF3-ROBOT)))

(:derived (Flag97-)(and (BELOWOF8-ROBOT)))

(:derived (Flag98-)(and (BELOWOF10-ROBOT)))

(:derived (Flag98-)(and (BELOWOF6-ROBOT)))

(:derived (Flag98-)(and (BELOWOF5-ROBOT)))

(:derived (Flag98-)(and (BELOWOF2-ROBOT)))

(:derived (Flag98-)(and (BELOWOF1-ROBOT)))

(:derived (Flag98-)(and (BELOWOF4-ROBOT)))

(:derived (Flag98-)(and (BELOWOF7-ROBOT)))

(:derived (Flag98-)(and (BELOWOF3-ROBOT)))

(:derived (Flag98-)(and (BELOWOF8-ROBOT)))

(:derived (Flag99-)(and (ABOVEOF6-ROBOT)))

(:derived (Flag99-)(and (ABOVEOF1-ROBOT)))

(:derived (Flag99-)(and (ABOVEOF2-ROBOT)))

(:derived (Flag99-)(and (ABOVEOF7-ROBOT)))

(:derived (Flag99-)(and (ABOVEOF8-ROBOT)))

(:derived (Flag99-)(and (ABOVEOF9-ROBOT)))

(:derived (Flag99-)(and (ABOVEOF4-ROBOT)))

(:derived (Flag99-)(and (ABOVEOF3-ROBOT)))

(:derived (Flag99-)(and (ABOVEOF5-ROBOT)))

(:derived (Flag100-)(and (ABOVEOF6-ROBOT)))

(:derived (Flag100-)(and (ABOVEOF2-ROBOT)))

(:derived (Flag100-)(and (ABOVEOF7-ROBOT)))

(:derived (Flag100-)(and (ABOVEOF8-ROBOT)))

(:derived (Flag100-)(and (ABOVEOF9-ROBOT)))

(:derived (Flag100-)(and (ABOVEOF4-ROBOT)))

(:derived (Flag100-)(and (ABOVEOF3-ROBOT)))

(:derived (Flag100-)(and (ABOVEOF5-ROBOT)))

(:derived (Flag101-)(and (ABOVEOF6-ROBOT)))

(:derived (Flag101-)(and (ABOVEOF8-ROBOT)))

(:derived (Flag101-)(and (ABOVEOF7-ROBOT)))

(:derived (Flag101-)(and (ABOVEOF9-ROBOT)))

(:derived (Flag101-)(and (ABOVEOF4-ROBOT)))

(:derived (Flag101-)(and (ABOVEOF3-ROBOT)))

(:derived (Flag101-)(and (ABOVEOF5-ROBOT)))

(:derived (Flag102-)(and (ABOVEOF6-ROBOT)))

(:derived (Flag102-)(and (ABOVEOF8-ROBOT)))

(:derived (Flag102-)(and (ABOVEOF7-ROBOT)))

(:derived (Flag102-)(and (ABOVEOF9-ROBOT)))

(:derived (Flag102-)(and (ABOVEOF4-ROBOT)))

(:derived (Flag102-)(and (ABOVEOF5-ROBOT)))

(:derived (Flag103-)(and (ABOVEOF8-ROBOT)))

(:derived (Flag103-)(and (ABOVEOF9-ROBOT)))

(:derived (Flag103-)(and (ABOVEOF7-ROBOT)))

(:derived (Flag103-)(and (ABOVEOF5-ROBOT)))

(:derived (Flag103-)(and (ABOVEOF6-ROBOT)))

(:derived (Flag104-)(and (ABOVEOF8-ROBOT)))

(:derived (Flag104-)(and (ABOVEOF9-ROBOT)))

(:derived (Flag104-)(and (ABOVEOF7-ROBOT)))

(:derived (Flag104-)(and (ABOVEOF6-ROBOT)))

(:derived (Flag105-)(and (ABOVEOF8-ROBOT)))

(:derived (Flag105-)(and (ABOVEOF7-ROBOT)))

(:derived (Flag105-)(and (ABOVEOF9-ROBOT)))

(:derived (Flag106-)(and (ABOVEOF8-ROBOT)))

(:derived (Flag106-)(and (ABOVEOF9-ROBOT)))

(:derived (Flag107-)(and (BELOWOF1-ROBOT)(ABOVEOF2-ROBOT)))

(:derived (Flag108-)(and (ABOVEOF6-ROBOT)(BELOWOF2-ROBOT)))

(:derived (Flag109-)(and (ABOVEOF2-ROBOT)(BELOWOF2-ROBOT)))

(:derived (Flag110-)(and (BELOWOF1-ROBOT)(ABOVEOF4-ROBOT)))

(:derived (Flag111-)(and (ABOVEOF9-ROBOT)(BELOWOF2-ROBOT)))

(:derived (Flag112-)(and (BELOWOF1-ROBOT)(ABOVEOF7-ROBOT)))

(:derived (Flag113-)(and (ABOVEOF6-ROBOT)(BELOWOF1-ROBOT)))

(:derived (Flag114-)(and (BELOWOF1-ROBOT)(ABOVEOF5-ROBOT)))

(:derived (Flag115-)(and (ABOVEOF4-ROBOT)(BELOWOF2-ROBOT)))

(:derived (Flag116-)(and (BELOWOF2-ROBOT)(ABOVEOF5-ROBOT)))

(:derived (Flag117-)(and (BELOWOF1-ROBOT)(ABOVEOF1-ROBOT)))

(:derived (Flag118-)(and (ABOVEOF8-ROBOT)(BELOWOF1-ROBOT)))

(:derived (Flag119-)(and (ABOVEOF3-ROBOT)(BELOWOF1-ROBOT)))

(:derived (Flag120-)(and (ABOVEOF7-ROBOT)(BELOWOF2-ROBOT)))

(:derived (Flag121-)(and (BELOWOF1-ROBOT)(ABOVEOF9-ROBOT)))

(:derived (Flag122-)(and (ABOVEOF3-ROBOT)(BELOWOF2-ROBOT)))

(:derived (Flag123-)(and (ABOVEOF8-ROBOT)(BELOWOF2-ROBOT)))

(:derived (Flag124-)(and (ABOVEOF1-ROBOT)(BELOWOF2-ROBOT)))

(:derived (Flag126-)(and (BELOWOF3-ROBOT)(ABOVEOF5-ROBOT)))

(:derived (Flag127-)(and (BELOWOF3-ROBOT)(ABOVEOF6-ROBOT)))

(:derived (Flag128-)(and (BELOWOF3-ROBOT)(ABOVEOF7-ROBOT)))

(:derived (Flag129-)(and (BELOWOF3-ROBOT)(ABOVEOF9-ROBOT)))

(:derived (Flag130-)(and (BELOWOF3-ROBOT)(ABOVEOF2-ROBOT)))

(:derived (Flag131-)(and (ABOVEOF8-ROBOT)(BELOWOF3-ROBOT)))

(:derived (Flag132-)(and (BELOWOF3-ROBOT)(ABOVEOF4-ROBOT)))

(:derived (Flag133-)(and (BELOWOF2-ROBOT)(ABOVEOF2-ROBOT)))

(:derived (Flag134-)(and (ABOVEOF3-ROBOT)(BELOWOF3-ROBOT)))

(:derived (Flag136-)(and (ABOVEOF3-ROBOT)(BELOWOF4-ROBOT)))

(:derived (Flag137-)(and (ABOVEOF6-ROBOT)(BELOWOF4-ROBOT)))

(:derived (Flag138-)(and (ABOVEOF9-ROBOT)(BELOWOF4-ROBOT)))

(:derived (Flag139-)(and (BELOWOF4-ROBOT)(ABOVEOF4-ROBOT)))

(:derived (Flag140-)(and (ABOVEOF8-ROBOT)(BELOWOF4-ROBOT)))

(:derived (Flag141-)(and (BELOWOF4-ROBOT)(ABOVEOF7-ROBOT)))

(:derived (Flag142-)(and (BELOWOF4-ROBOT)(ABOVEOF5-ROBOT)))

(:derived (Flag143-)(and (BELOWOF3-ROBOT)(ABOVEOF3-ROBOT)))

(:derived (Flag145-)(and (ABOVEOF4-ROBOT)(BELOWOF5-ROBOT)))

(:derived (Flag146-)(and (ABOVEOF8-ROBOT)(BELOWOF5-ROBOT)))

(:derived (Flag147-)(and (ABOVEOF5-ROBOT)(BELOWOF5-ROBOT)))

(:derived (Flag148-)(and (ABOVEOF7-ROBOT)(BELOWOF5-ROBOT)))

(:derived (Flag149-)(and (ABOVEOF9-ROBOT)(BELOWOF5-ROBOT)))

(:derived (Flag150-)(and (ABOVEOF6-ROBOT)(BELOWOF5-ROBOT)))

(:derived (Flag152-)(and (BELOWOF6-ROBOT)(ABOVEOF5-ROBOT)))

(:derived (Flag153-)(and (BELOWOF6-ROBOT)(ABOVEOF7-ROBOT)))

(:derived (Flag154-)(and (BELOWOF5-ROBOT)(ABOVEOF5-ROBOT)))

(:derived (Flag155-)(and (BELOWOF6-ROBOT)(ABOVEOF6-ROBOT)))

(:derived (Flag156-)(and (ABOVEOF8-ROBOT)(BELOWOF6-ROBOT)))

(:derived (Flag157-)(and (BELOWOF6-ROBOT)(ABOVEOF9-ROBOT)))

(:derived (Flag159-)(and (ABOVEOF6-ROBOT)(BELOWOF7-ROBOT)))

(:derived (Flag160-)(and (ABOVEOF9-ROBOT)(BELOWOF7-ROBOT)))

(:derived (Flag161-)(and (ABOVEOF6-ROBOT)(BELOWOF6-ROBOT)))

(:derived (Flag162-)(and (ABOVEOF7-ROBOT)(BELOWOF7-ROBOT)))

(:derived (Flag163-)(and (ABOVEOF8-ROBOT)(BELOWOF7-ROBOT)))

(:derived (Flag165-)(and (ABOVEOF8-ROBOT)(BELOWOF8-ROBOT)))

(:derived (Flag166-)(and (BELOWOF8-ROBOT)(ABOVEOF9-ROBOT)))

(:derived (Flag167-)(and (BELOWOF7-ROBOT)(ABOVEOF7-ROBOT)))

(:derived (Flag168-)(and (BELOWOF8-ROBOT)(ABOVEOF7-ROBOT)))

(:derived (Flag170-)(and (BELOWOF8-ROBOT)(ABOVEOF8-ROBOT)))

(:derived (Flag171-)(and (ABOVEOF8-ROBOT)(BELOWOF9-ROBOT)))

(:derived (Flag172-)(and (BELOWOF9-ROBOT)(ABOVEOF9-ROBOT)))

(:derived (Flag174-)(and (ABOVEOF9-ROBOT)(BELOWOF9-ROBOT)))

(:derived (Flag175-)(and (BELOWOF10-ROBOT)(ABOVEOF9-ROBOT)))

(:derived (Flag180-)(and (BELOWOF1-ROBOT)))

(:action MOVEUP-ROBOT
:parameters ()
:precondition
(and
(Flag2-)
)
:effect
(and
(when
(and
(Flag173-)
)
(and
(ROW9-ROBOT)
(not (NOT-ROW9-ROBOT))
)
)
(when
(and
(Flag169-)
)
(and
(ROW8-ROBOT)
(not (NOT-ROW8-ROBOT))
)
)
(when
(and
(Flag164-)
)
(and
(ROW7-ROBOT)
(not (NOT-ROW7-ROBOT))
)
)
(when
(and
(Flag158-)
)
(and
(ROW6-ROBOT)
(not (NOT-ROW6-ROBOT))
)
)
(when
(and
(Flag151-)
)
(and
(ROW5-ROBOT)
(not (NOT-ROW5-ROBOT))
)
)
(when
(and
(Flag144-)
)
(and
(ROW4-ROBOT)
(not (NOT-ROW4-ROBOT))
)
)
(when
(and
(Flag135-)
)
(and
(ROW3-ROBOT)
(not (NOT-ROW3-ROBOT))
)
)
(when
(and
(Flag125-)
)
(and
(ROW2-ROBOT)
(not (NOT-ROW2-ROBOT))
)
)
(when
(and
(Flag180-)
)
(and
(ROW1-ROBOT)
(not (NOT-ROW1-ROBOT))
)
)
(when
(and
(ROW8-ROBOT)
)
(and
(ROW9-ROBOT)
(NOT-ROW8-ROBOT)
(not (NOT-ROW9-ROBOT))
(not (ROW8-ROBOT))
)
)
(when
(and
(ROW7-ROBOT)
)
(and
(ROW8-ROBOT)
(NOT-ROW7-ROBOT)
(not (NOT-ROW8-ROBOT))
(not (ROW7-ROBOT))
)
)
(when
(and
(ROW6-ROBOT)
)
(and
(ROW7-ROBOT)
(NOT-ROW6-ROBOT)
(not (NOT-ROW7-ROBOT))
(not (ROW6-ROBOT))
)
)
(when
(and
(ROW5-ROBOT)
)
(and
(ROW6-ROBOT)
(NOT-ROW5-ROBOT)
(not (NOT-ROW6-ROBOT))
(not (ROW5-ROBOT))
)
)
(when
(and
(ROW4-ROBOT)
)
(and
(ROW5-ROBOT)
(NOT-ROW4-ROBOT)
(not (NOT-ROW5-ROBOT))
(not (ROW4-ROBOT))
)
)
(when
(and
(ROW3-ROBOT)
)
(and
(ROW4-ROBOT)
(NOT-ROW3-ROBOT)
(not (NOT-ROW4-ROBOT))
(not (ROW3-ROBOT))
)
)
(when
(and
(ROW2-ROBOT)
)
(and
(ROW3-ROBOT)
(NOT-ROW2-ROBOT)
(not (NOT-ROW3-ROBOT))
(not (ROW2-ROBOT))
)
)
(when
(and
(ROW1-ROBOT)
)
(and
(ROW2-ROBOT)
(NOT-ROW1-ROBOT)
(not (NOT-ROW2-ROBOT))
(not (ROW1-ROBOT))
)
)
(when
(and
(ROW0-ROBOT)
)
(and
(ROW1-ROBOT)
(NOT-ROW0-ROBOT)
(not (NOT-ROW1-ROBOT))
(not (ROW0-ROBOT))
)
)
(when
(and
(BELOWOF8-ROBOT)
)
(and
(BELOWOF10-ROBOT)
(NOT-BELOWOF9-ROBOT)
(BELOWOF9-ROBOT)
(NOT-BELOWOF8-ROBOT)
(not (NOT-BELOWOF10-ROBOT))
(not (BELOWOF8-ROBOT))
)
)
(when
(and
(BELOWOF3-ROBOT)
)
(and
(BELOWOF10-ROBOT)
(NOT-BELOWOF9-ROBOT)
(BELOWOF9-ROBOT)
(NOT-BELOWOF8-ROBOT)
(BELOWOF8-ROBOT)
(NOT-BELOWOF7-ROBOT)
(BELOWOF7-ROBOT)
(NOT-BELOWOF6-ROBOT)
(BELOWOF6-ROBOT)
(NOT-BELOWOF5-ROBOT)
(BELOWOF5-ROBOT)
(NOT-BELOWOF4-ROBOT)
(BELOWOF4-ROBOT)
(NOT-BELOWOF3-ROBOT)
(not (NOT-BELOWOF10-ROBOT))
(not (BELOWOF3-ROBOT))
)
)
(when
(and
(BELOWOF9-ROBOT)
)
(and
(BELOWOF10-ROBOT)
(NOT-BELOWOF9-ROBOT)
(not (NOT-BELOWOF10-ROBOT))
(not (BELOWOF9-ROBOT))
)
)
(when
(and
(BELOWOF7-ROBOT)
)
(and
(BELOWOF10-ROBOT)
(NOT-BELOWOF9-ROBOT)
(BELOWOF9-ROBOT)
(NOT-BELOWOF8-ROBOT)
(BELOWOF8-ROBOT)
(NOT-BELOWOF7-ROBOT)
(not (NOT-BELOWOF10-ROBOT))
(not (BELOWOF7-ROBOT))
)
)
(when
(and
(BELOWOF4-ROBOT)
)
(and
(BELOWOF10-ROBOT)
(NOT-BELOWOF9-ROBOT)
(BELOWOF9-ROBOT)
(NOT-BELOWOF8-ROBOT)
(BELOWOF8-ROBOT)
(NOT-BELOWOF7-ROBOT)
(BELOWOF7-ROBOT)
(NOT-BELOWOF6-ROBOT)
(BELOWOF6-ROBOT)
(NOT-BELOWOF5-ROBOT)
(BELOWOF5-ROBOT)
(NOT-BELOWOF4-ROBOT)
(not (NOT-BELOWOF10-ROBOT))
(not (BELOWOF4-ROBOT))
)
)
(when
(and
(BELOWOF1-ROBOT)
)
(and
(BELOWOF10-ROBOT)
(NOT-BELOWOF9-ROBOT)
(BELOWOF9-ROBOT)
(NOT-BELOWOF8-ROBOT)
(BELOWOF8-ROBOT)
(NOT-BELOWOF7-ROBOT)
(BELOWOF7-ROBOT)
(NOT-BELOWOF6-ROBOT)
(BELOWOF6-ROBOT)
(NOT-BELOWOF5-ROBOT)
(BELOWOF5-ROBOT)
(NOT-BELOWOF4-ROBOT)
(BELOWOF4-ROBOT)
(NOT-BELOWOF3-ROBOT)
(BELOWOF3-ROBOT)
(NOT-BELOWOF2-ROBOT)
(BELOWOF2-ROBOT)
(NOT-BELOWOF1-ROBOT)
(not (NOT-BELOWOF10-ROBOT))
(not (BELOWOF1-ROBOT))
)
)
(when
(and
(BELOWOF2-ROBOT)
)
(and
(BELOWOF10-ROBOT)
(NOT-BELOWOF9-ROBOT)
(BELOWOF9-ROBOT)
(NOT-BELOWOF8-ROBOT)
(BELOWOF8-ROBOT)
(NOT-BELOWOF7-ROBOT)
(BELOWOF7-ROBOT)
(NOT-BELOWOF6-ROBOT)
(BELOWOF6-ROBOT)
(NOT-BELOWOF5-ROBOT)
(BELOWOF5-ROBOT)
(NOT-BELOWOF4-ROBOT)
(BELOWOF4-ROBOT)
(NOT-BELOWOF3-ROBOT)
(BELOWOF3-ROBOT)
(NOT-BELOWOF2-ROBOT)
(not (NOT-BELOWOF10-ROBOT))
(not (BELOWOF2-ROBOT))
)
)
(when
(and
(BELOWOF5-ROBOT)
)
(and
(BELOWOF10-ROBOT)
(NOT-BELOWOF9-ROBOT)
(BELOWOF9-ROBOT)
(NOT-BELOWOF8-ROBOT)
(BELOWOF8-ROBOT)
(NOT-BELOWOF7-ROBOT)
(BELOWOF7-ROBOT)
(NOT-BELOWOF6-ROBOT)
(BELOWOF6-ROBOT)
(NOT-BELOWOF5-ROBOT)
(not (NOT-BELOWOF10-ROBOT))
(not (BELOWOF5-ROBOT))
)
)
(when
(and
(BELOWOF6-ROBOT)
)
(and
(BELOWOF10-ROBOT)
(NOT-BELOWOF9-ROBOT)
(BELOWOF9-ROBOT)
(NOT-BELOWOF8-ROBOT)
(BELOWOF8-ROBOT)
(NOT-BELOWOF7-ROBOT)
(BELOWOF7-ROBOT)
(NOT-BELOWOF6-ROBOT)
(not (NOT-BELOWOF10-ROBOT))
(not (BELOWOF6-ROBOT))
)
)
(when
(and
(ABOVEOF9-ROBOT)
)
(and
(ABOVEOF8-ROBOT)
(ABOVEOF7-ROBOT)
(ABOVEOF6-ROBOT)
(ABOVEOF5-ROBOT)
(ABOVEOF4-ROBOT)
(ABOVEOF3-ROBOT)
(ABOVEOF2-ROBOT)
(not (NOT-ABOVEOF9-ROBOT))
(not (NOT-ABOVEOF8-ROBOT))
(not (NOT-ABOVEOF7-ROBOT))
(not (NOT-ABOVEOF6-ROBOT))
(not (NOT-ABOVEOF5-ROBOT))
(not (NOT-ABOVEOF4-ROBOT))
(not (NOT-ABOVEOF3-ROBOT))
(not (NOT-ABOVEOF2-ROBOT))
)
)
(when
(and
(ABOVEOF8-ROBOT)
)
(and
(ABOVEOF9-ROBOT)
(ABOVEOF7-ROBOT)
(ABOVEOF6-ROBOT)
(ABOVEOF5-ROBOT)
(ABOVEOF4-ROBOT)
(ABOVEOF3-ROBOT)
(ABOVEOF2-ROBOT)
(not (NOT-ABOVEOF9-ROBOT))
(not (NOT-ABOVEOF8-ROBOT))
(not (NOT-ABOVEOF7-ROBOT))
(not (NOT-ABOVEOF6-ROBOT))
(not (NOT-ABOVEOF5-ROBOT))
(not (NOT-ABOVEOF4-ROBOT))
(not (NOT-ABOVEOF3-ROBOT))
(not (NOT-ABOVEOF2-ROBOT))
)
)
(when
(and
(ABOVEOF7-ROBOT)
)
(and
(ABOVEOF8-ROBOT)
(ABOVEOF6-ROBOT)
(ABOVEOF5-ROBOT)
(ABOVEOF4-ROBOT)
(ABOVEOF3-ROBOT)
(ABOVEOF2-ROBOT)
(not (NOT-ABOVEOF8-ROBOT))
(not (NOT-ABOVEOF7-ROBOT))
(not (NOT-ABOVEOF6-ROBOT))
(not (NOT-ABOVEOF5-ROBOT))
(not (NOT-ABOVEOF4-ROBOT))
(not (NOT-ABOVEOF3-ROBOT))
(not (NOT-ABOVEOF2-ROBOT))
)
)
(when
(and
(ABOVEOF6-ROBOT)
)
(and
(ABOVEOF7-ROBOT)
(ABOVEOF5-ROBOT)
(ABOVEOF4-ROBOT)
(ABOVEOF3-ROBOT)
(ABOVEOF2-ROBOT)
(not (NOT-ABOVEOF7-ROBOT))
(not (NOT-ABOVEOF6-ROBOT))
(not (NOT-ABOVEOF5-ROBOT))
(not (NOT-ABOVEOF4-ROBOT))
(not (NOT-ABOVEOF3-ROBOT))
(not (NOT-ABOVEOF2-ROBOT))
)
)
(when
(and
(ABOVEOF5-ROBOT)
)
(and
(ABOVEOF6-ROBOT)
(ABOVEOF4-ROBOT)
(ABOVEOF3-ROBOT)
(ABOVEOF2-ROBOT)
(not (NOT-ABOVEOF6-ROBOT))
(not (NOT-ABOVEOF5-ROBOT))
(not (NOT-ABOVEOF4-ROBOT))
(not (NOT-ABOVEOF3-ROBOT))
(not (NOT-ABOVEOF2-ROBOT))
)
)
(when
(and
(ABOVEOF4-ROBOT)
)
(and
(ABOVEOF5-ROBOT)
(ABOVEOF3-ROBOT)
(ABOVEOF2-ROBOT)
(not (NOT-ABOVEOF5-ROBOT))
(not (NOT-ABOVEOF4-ROBOT))
(not (NOT-ABOVEOF3-ROBOT))
(not (NOT-ABOVEOF2-ROBOT))
)
)
(when
(and
(ABOVEOF3-ROBOT)
)
(and
(ABOVEOF4-ROBOT)
(ABOVEOF2-ROBOT)
(not (NOT-ABOVEOF4-ROBOT))
(not (NOT-ABOVEOF3-ROBOT))
(not (NOT-ABOVEOF2-ROBOT))
)
)
(when
(and
(ABOVEOF2-ROBOT)
)
(and
(ABOVEOF3-ROBOT)
(not (NOT-ABOVEOF3-ROBOT))
(not (NOT-ABOVEOF2-ROBOT))
)
)
(when
(and
(ABOVEOF1-ROBOT)
)
(and
(ABOVEOF2-ROBOT)
(not (NOT-ABOVEOF2-ROBOT))
)
)
(ABOVEOF1-ROBOT)
(CHECKCONSISTENCY-)
(not (NOT-ABOVEOF1-ROBOT))
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action MOVEDOWN-ROBOT
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
)
:effect
(and
(when
(and
(Flag176-)
)
(and
(ROW8-ROBOT)
(not (NOT-ROW8-ROBOT))
)
)
(when
(and
(Flag173-)
)
(and
(ROW7-ROBOT)
(not (NOT-ROW7-ROBOT))
)
)
(when
(and
(Flag169-)
)
(and
(ROW6-ROBOT)
(not (NOT-ROW6-ROBOT))
)
)
(when
(and
(Flag164-)
)
(and
(ROW5-ROBOT)
(not (NOT-ROW5-ROBOT))
)
)
(when
(and
(Flag158-)
)
(and
(ROW4-ROBOT)
(not (NOT-ROW4-ROBOT))
)
)
(when
(and
(Flag151-)
)
(and
(ROW3-ROBOT)
(not (NOT-ROW3-ROBOT))
)
)
(when
(and
(Flag144-)
)
(and
(ROW2-ROBOT)
(not (NOT-ROW2-ROBOT))
)
)
(when
(and
(Flag135-)
)
(and
(ROW1-ROBOT)
(not (NOT-ROW1-ROBOT))
)
)
(when
(and
(Flag125-)
)
(and
(ROW0-ROBOT)
(not (NOT-ROW0-ROBOT))
)
)
(when
(and
(ROW9-ROBOT)
)
(and
(ROW8-ROBOT)
(NOT-ROW9-ROBOT)
(not (NOT-ROW8-ROBOT))
(not (ROW9-ROBOT))
)
)
(when
(and
(ROW8-ROBOT)
)
(and
(ROW7-ROBOT)
(NOT-ROW8-ROBOT)
(not (NOT-ROW7-ROBOT))
(not (ROW8-ROBOT))
)
)
(when
(and
(ROW7-ROBOT)
)
(and
(ROW6-ROBOT)
(NOT-ROW7-ROBOT)
(not (NOT-ROW6-ROBOT))
(not (ROW7-ROBOT))
)
)
(when
(and
(ROW6-ROBOT)
)
(and
(ROW5-ROBOT)
(NOT-ROW6-ROBOT)
(not (NOT-ROW5-ROBOT))
(not (ROW6-ROBOT))
)
)
(when
(and
(ROW5-ROBOT)
)
(and
(ROW4-ROBOT)
(NOT-ROW5-ROBOT)
(not (NOT-ROW4-ROBOT))
(not (ROW5-ROBOT))
)
)
(when
(and
(ROW4-ROBOT)
)
(and
(ROW3-ROBOT)
(NOT-ROW4-ROBOT)
(not (NOT-ROW3-ROBOT))
(not (ROW4-ROBOT))
)
)
(when
(and
(ROW3-ROBOT)
)
(and
(ROW2-ROBOT)
(NOT-ROW3-ROBOT)
(not (NOT-ROW2-ROBOT))
(not (ROW3-ROBOT))
)
)
(when
(and
(ROW2-ROBOT)
)
(and
(ROW1-ROBOT)
(NOT-ROW2-ROBOT)
(not (NOT-ROW1-ROBOT))
(not (ROW2-ROBOT))
)
)
(when
(and
(ROW1-ROBOT)
)
(and
(ROW0-ROBOT)
(NOT-ROW1-ROBOT)
(not (NOT-ROW0-ROBOT))
(not (ROW1-ROBOT))
)
)
(when
(and
(ABOVEOF9-ROBOT)
)
(and
(ABOVEOF8-ROBOT)
(NOT-ABOVEOF9-ROBOT)
(not (NOT-ABOVEOF8-ROBOT))
(not (ABOVEOF9-ROBOT))
)
)
(when
(and
(Flag106-)
)
(and
(ABOVEOF7-ROBOT)
(NOT-ABOVEOF8-ROBOT)
(not (NOT-ABOVEOF7-ROBOT))
(not (ABOVEOF8-ROBOT))
)
)
(when
(and
(Flag105-)
)
(and
(ABOVEOF6-ROBOT)
(NOT-ABOVEOF7-ROBOT)
(not (NOT-ABOVEOF6-ROBOT))
(not (ABOVEOF7-ROBOT))
)
)
(when
(and
(Flag104-)
)
(and
(ABOVEOF5-ROBOT)
(NOT-ABOVEOF6-ROBOT)
(not (NOT-ABOVEOF5-ROBOT))
(not (ABOVEOF6-ROBOT))
)
)
(when
(and
(Flag103-)
)
(and
(ABOVEOF4-ROBOT)
(NOT-ABOVEOF5-ROBOT)
(not (NOT-ABOVEOF4-ROBOT))
(not (ABOVEOF5-ROBOT))
)
)
(when
(and
(Flag102-)
)
(and
(ABOVEOF3-ROBOT)
(NOT-ABOVEOF4-ROBOT)
(not (NOT-ABOVEOF3-ROBOT))
(not (ABOVEOF4-ROBOT))
)
)
(when
(and
(Flag101-)
)
(and
(ABOVEOF2-ROBOT)
(NOT-ABOVEOF3-ROBOT)
(not (NOT-ABOVEOF2-ROBOT))
(not (ABOVEOF3-ROBOT))
)
)
(when
(and
(Flag100-)
)
(and
(ABOVEOF1-ROBOT)
(NOT-ABOVEOF2-ROBOT)
(not (NOT-ABOVEOF1-ROBOT))
(not (ABOVEOF2-ROBOT))
)
)
(when
(and
(Flag99-)
)
(and
(NOT-ABOVEOF1-ROBOT)
(not (ABOVEOF1-ROBOT))
)
)
(when
(and
(Flag98-)
)
(and
(BELOWOF9-ROBOT)
(not (NOT-BELOWOF9-ROBOT))
)
)
(when
(and
(Flag97-)
)
(and
(BELOWOF8-ROBOT)
(not (NOT-BELOWOF8-ROBOT))
)
)
(when
(and
(Flag96-)
)
(and
(BELOWOF7-ROBOT)
(not (NOT-BELOWOF7-ROBOT))
)
)
(when
(and
(Flag95-)
)
(and
(BELOWOF6-ROBOT)
(not (NOT-BELOWOF6-ROBOT))
)
)
(when
(and
(Flag94-)
)
(and
(BELOWOF5-ROBOT)
(not (NOT-BELOWOF5-ROBOT))
)
)
(when
(and
(Flag93-)
)
(and
(BELOWOF4-ROBOT)
(not (NOT-BELOWOF4-ROBOT))
)
)
(when
(and
(Flag92-)
)
(and
(BELOWOF3-ROBOT)
(not (NOT-BELOWOF3-ROBOT))
)
)
(when
(and
(Flag91-)
)
(and
(BELOWOF2-ROBOT)
(not (NOT-BELOWOF2-ROBOT))
)
)
(when
(and
(Flag90-)
)
(and
(BELOWOF1-ROBOT)
(not (NOT-BELOWOF1-ROBOT))
)
)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:derived (Flag2-)(and (NOT-CHECKCONSISTENCY-)(NOT-ERROR-)))

(:derived (Flag3-)(and (LEFTOF9-ROBOT)))

(:derived (Flag3-)(and (RIGHTOF2-ROBOT)))

(:derived (Flag5-)(and (RIGHTOF2-ROBOT)))

(:derived (Flag6-)(and (RIGHTOF2-ROBOT)))

(:derived (Flag7-)(and (RIGHTOF2-ROBOT)))

(:derived (Flag21-)(and (LEFTOF9-ROBOT)))

(:derived (Flag97-)(and (BELOWOF9-ROBOT)))

(:derived (Flag98-)(and (BELOWOF9-ROBOT)))

(:derived (Flag177-)(and (LEFTOF9-ROBOT)))

)
