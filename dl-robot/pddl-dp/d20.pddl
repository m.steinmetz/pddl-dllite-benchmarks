(define (domain grounded-SIMPLE-ADL-ROBOT)
(:requirements
:strips
)
(:predicates
(Flag567-)
(Flag564-)
(Flag319-)
(Flag25-)
(Flag24-)
(Flag23-)
(Flag22-)
(Flag3-)
(Flag2-)
(ROW19-ROBOT)
(ROW18-ROBOT)
(ROW17-ROBOT)
(ROW16-ROBOT)
(ROW15-ROBOT)
(ROW14-ROBOT)
(ROW13-ROBOT)
(ROW12-ROBOT)
(ROW11-ROBOT)
(ROW10-ROBOT)
(ROW9-ROBOT)
(ROW8-ROBOT)
(ROW7-ROBOT)
(ROW6-ROBOT)
(ROW5-ROBOT)
(ROW4-ROBOT)
(ROW3-ROBOT)
(ROW2-ROBOT)
(ROW1-ROBOT)
(BELOWOF20-ROBOT)
(NOT-BELOWOF19-ROBOT)
(BELOWOF18-ROBOT)
(BELOWOF17-ROBOT)
(BELOWOF16-ROBOT)
(BELOWOF15-ROBOT)
(BELOWOF14-ROBOT)
(BELOWOF13-ROBOT)
(BELOWOF12-ROBOT)
(BELOWOF11-ROBOT)
(BELOWOF10-ROBOT)
(BELOWOF9-ROBOT)
(BELOWOF8-ROBOT)
(BELOWOF7-ROBOT)
(BELOWOF6-ROBOT)
(BELOWOF5-ROBOT)
(BELOWOF4-ROBOT)
(BELOWOF3-ROBOT)
(BELOWOF2-ROBOT)
(ABOVEOF19-ROBOT)
(ABOVEOF18-ROBOT)
(ABOVEOF17-ROBOT)
(ABOVEOF16-ROBOT)
(ABOVEOF15-ROBOT)
(ABOVEOF14-ROBOT)
(ABOVEOF13-ROBOT)
(ABOVEOF12-ROBOT)
(ABOVEOF11-ROBOT)
(ABOVEOF10-ROBOT)
(ABOVEOF9-ROBOT)
(ABOVEOF8-ROBOT)
(ABOVEOF7-ROBOT)
(ABOVEOF6-ROBOT)
(ABOVEOF5-ROBOT)
(ABOVEOF4-ROBOT)
(ABOVEOF3-ROBOT)
(ABOVEOF2-ROBOT)
(ABOVEOF1-ROBOT)
(CHECKCONSISTENCY-)
(ROW0-ROBOT)
(BELOWOF1-ROBOT)
(Flag569-)
(Flag568-)
(Flag562-)
(Flag561-)
(Flag559-)
(Flag558-)
(Flag557-)
(Flag555-)
(Flag554-)
(Flag553-)
(Flag552-)
(Flag550-)
(Flag549-)
(Flag548-)
(Flag547-)
(Flag546-)
(Flag544-)
(Flag543-)
(Flag542-)
(Flag541-)
(Flag540-)
(Flag539-)
(Flag537-)
(Flag536-)
(Flag535-)
(Flag534-)
(Flag533-)
(Flag532-)
(Flag531-)
(Flag530-)
(Flag528-)
(Flag527-)
(Flag526-)
(Flag525-)
(Flag524-)
(Flag523-)
(Flag522-)
(Flag521-)
(Flag520-)
(Flag518-)
(Flag517-)
(Flag516-)
(Flag515-)
(Flag514-)
(Flag513-)
(Flag512-)
(Flag511-)
(Flag510-)
(Flag508-)
(Flag507-)
(Flag506-)
(Flag505-)
(Flag504-)
(Flag503-)
(Flag502-)
(Flag501-)
(Flag500-)
(Flag499-)
(Flag497-)
(Flag496-)
(Flag495-)
(Flag494-)
(Flag493-)
(Flag492-)
(Flag491-)
(Flag490-)
(Flag489-)
(Flag488-)
(Flag487-)
(Flag486-)
(Flag484-)
(Flag483-)
(Flag482-)
(Flag481-)
(Flag480-)
(Flag479-)
(Flag478-)
(Flag477-)
(Flag476-)
(Flag475-)
(Flag474-)
(Flag473-)
(Flag472-)
(Flag471-)
(Flag469-)
(Flag468-)
(Flag467-)
(Flag466-)
(Flag465-)
(Flag464-)
(Flag463-)
(Flag462-)
(Flag461-)
(Flag460-)
(Flag459-)
(Flag458-)
(Flag457-)
(Flag456-)
(Flag454-)
(Flag453-)
(Flag452-)
(Flag451-)
(Flag450-)
(Flag449-)
(Flag448-)
(Flag447-)
(Flag446-)
(Flag445-)
(Flag444-)
(Flag443-)
(Flag442-)
(Flag441-)
(Flag440-)
(Flag439-)
(Flag438-)
(Flag436-)
(Flag435-)
(Flag434-)
(Flag433-)
(Flag432-)
(Flag431-)
(Flag430-)
(Flag429-)
(Flag428-)
(Flag427-)
(Flag426-)
(Flag425-)
(Flag424-)
(Flag423-)
(Flag422-)
(Flag421-)
(Flag420-)
(Flag418-)
(Flag417-)
(Flag416-)
(Flag415-)
(Flag414-)
(Flag413-)
(Flag412-)
(Flag411-)
(Flag410-)
(Flag409-)
(Flag408-)
(Flag407-)
(Flag406-)
(Flag405-)
(Flag404-)
(Flag403-)
(Flag402-)
(Flag400-)
(Flag399-)
(Flag398-)
(Flag397-)
(Flag396-)
(Flag395-)
(Flag394-)
(Flag393-)
(Flag392-)
(Flag391-)
(Flag390-)
(Flag389-)
(Flag388-)
(Flag387-)
(Flag386-)
(Flag385-)
(Flag384-)
(Flag383-)
(Flag382-)
(Flag381-)
(Flag379-)
(Flag378-)
(Flag377-)
(Flag376-)
(Flag375-)
(Flag374-)
(Flag373-)
(Flag372-)
(Flag371-)
(Flag370-)
(Flag369-)
(Flag368-)
(Flag367-)
(Flag366-)
(Flag365-)
(Flag364-)
(Flag363-)
(Flag362-)
(Flag361-)
(Flag359-)
(Flag358-)
(Flag357-)
(Flag356-)
(Flag355-)
(Flag354-)
(Flag353-)
(Flag352-)
(Flag351-)
(Flag350-)
(Flag349-)
(Flag348-)
(Flag347-)
(Flag346-)
(Flag345-)
(Flag344-)
(Flag343-)
(Flag342-)
(Flag341-)
(Flag340-)
(Flag339-)
(Flag338-)
(Flag337-)
(Flag336-)
(Flag335-)
(Flag334-)
(Flag333-)
(Flag332-)
(Flag331-)
(Flag330-)
(Flag329-)
(Flag328-)
(Flag327-)
(Flag326-)
(Flag325-)
(Flag324-)
(Flag323-)
(Flag322-)
(Flag321-)
(Flag320-)
(Flag318-)
(Flag317-)
(Flag316-)
(Flag315-)
(Flag314-)
(Flag313-)
(Flag312-)
(Flag311-)
(Flag310-)
(Flag309-)
(Flag308-)
(Flag307-)
(Flag306-)
(Flag305-)
(Flag304-)
(Flag303-)
(Flag302-)
(Flag301-)
(Flag300-)
(Flag299-)
(Flag298-)
(Flag297-)
(Flag296-)
(Flag295-)
(Flag294-)
(Flag293-)
(Flag292-)
(Flag291-)
(Flag290-)
(Flag289-)
(Flag288-)
(Flag287-)
(Flag286-)
(Flag285-)
(Flag284-)
(Flag4-)
(ERROR-)
(COLUMN18-ROBOT)
(COLUMN17-ROBOT)
(COLUMN16-ROBOT)
(COLUMN15-ROBOT)
(COLUMN14-ROBOT)
(COLUMN13-ROBOT)
(COLUMN12-ROBOT)
(COLUMN11-ROBOT)
(COLUMN10-ROBOT)
(COLUMN9-ROBOT)
(COLUMN8-ROBOT)
(COLUMN7-ROBOT)
(COLUMN6-ROBOT)
(COLUMN5-ROBOT)
(COLUMN4-ROBOT)
(COLUMN3-ROBOT)
(COLUMN2-ROBOT)
(COLUMN1-ROBOT)
(COLUMN0-ROBOT)
(RIGHTOF18-ROBOT)
(RIGHTOF17-ROBOT)
(RIGHTOF16-ROBOT)
(RIGHTOF15-ROBOT)
(RIGHTOF14-ROBOT)
(RIGHTOF13-ROBOT)
(RIGHTOF12-ROBOT)
(RIGHTOF11-ROBOT)
(RIGHTOF10-ROBOT)
(RIGHTOF9-ROBOT)
(RIGHTOF8-ROBOT)
(RIGHTOF7-ROBOT)
(RIGHTOF6-ROBOT)
(RIGHTOF5-ROBOT)
(RIGHTOF4-ROBOT)
(RIGHTOF3-ROBOT)
(RIGHTOF1-ROBOT)
(NOT-RIGHTOF2-ROBOT)
(RIGHTOF0-ROBOT)
(LEFTOF18-ROBOT)
(LEFTOF17-ROBOT)
(LEFTOF16-ROBOT)
(LEFTOF15-ROBOT)
(LEFTOF14-ROBOT)
(LEFTOF13-ROBOT)
(LEFTOF12-ROBOT)
(LEFTOF11-ROBOT)
(LEFTOF10-ROBOT)
(LEFTOF9-ROBOT)
(LEFTOF8-ROBOT)
(LEFTOF7-ROBOT)
(LEFTOF6-ROBOT)
(LEFTOF5-ROBOT)
(LEFTOF4-ROBOT)
(LEFTOF3-ROBOT)
(LEFTOF2-ROBOT)
(LEFTOF1-ROBOT)
(COLUMN19-ROBOT)
(LEFTOF20-ROBOT)
(NOT-LEFTOF19-ROBOT)
(RIGHTOF19-ROBOT)
(Flag570-)
(Flag565-)
(Flag563-)
(Flag560-)
(Flag556-)
(Flag551-)
(Flag545-)
(Flag538-)
(Flag529-)
(Flag519-)
(Flag509-)
(Flag498-)
(Flag485-)
(Flag470-)
(Flag455-)
(Flag437-)
(Flag419-)
(Flag401-)
(Flag380-)
(Flag360-)
(Flag282-)
(Flag280-)
(Flag279-)
(Flag277-)
(Flag276-)
(Flag275-)
(Flag273-)
(Flag272-)
(Flag271-)
(Flag270-)
(Flag268-)
(Flag267-)
(Flag266-)
(Flag265-)
(Flag264-)
(Flag263-)
(Flag261-)
(Flag260-)
(Flag259-)
(Flag258-)
(Flag257-)
(Flag256-)
(Flag255-)
(Flag253-)
(Flag252-)
(Flag251-)
(Flag250-)
(Flag249-)
(Flag248-)
(Flag247-)
(Flag245-)
(Flag244-)
(Flag243-)
(Flag242-)
(Flag241-)
(Flag240-)
(Flag239-)
(Flag238-)
(Flag237-)
(Flag235-)
(Flag234-)
(Flag233-)
(Flag232-)
(Flag231-)
(Flag230-)
(Flag229-)
(Flag228-)
(Flag227-)
(Flag225-)
(Flag224-)
(Flag223-)
(Flag222-)
(Flag221-)
(Flag220-)
(Flag219-)
(Flag218-)
(Flag217-)
(Flag216-)
(Flag215-)
(Flag213-)
(Flag212-)
(Flag211-)
(Flag210-)
(Flag209-)
(Flag208-)
(Flag207-)
(Flag206-)
(Flag205-)
(Flag204-)
(Flag203-)
(Flag201-)
(Flag200-)
(Flag199-)
(Flag198-)
(Flag197-)
(Flag196-)
(Flag195-)
(Flag194-)
(Flag193-)
(Flag192-)
(Flag191-)
(Flag190-)
(Flag189-)
(Flag187-)
(Flag186-)
(Flag185-)
(Flag184-)
(Flag183-)
(Flag182-)
(Flag181-)
(Flag180-)
(Flag179-)
(Flag178-)
(Flag177-)
(Flag176-)
(Flag175-)
(Flag174-)
(Flag172-)
(Flag171-)
(Flag170-)
(Flag169-)
(Flag168-)
(Flag167-)
(Flag166-)
(Flag165-)
(Flag164-)
(Flag163-)
(Flag162-)
(Flag161-)
(Flag160-)
(Flag159-)
(Flag158-)
(Flag157-)
(Flag155-)
(Flag154-)
(Flag153-)
(Flag152-)
(Flag151-)
(Flag150-)
(Flag149-)
(Flag148-)
(Flag147-)
(Flag146-)
(Flag145-)
(Flag144-)
(Flag143-)
(Flag142-)
(Flag141-)
(Flag139-)
(Flag138-)
(Flag137-)
(Flag136-)
(Flag135-)
(Flag134-)
(Flag133-)
(Flag132-)
(Flag131-)
(Flag130-)
(Flag129-)
(Flag128-)
(Flag127-)
(Flag126-)
(Flag125-)
(Flag124-)
(Flag123-)
(Flag122-)
(Flag121-)
(Flag119-)
(Flag118-)
(Flag117-)
(Flag116-)
(Flag115-)
(Flag114-)
(Flag113-)
(Flag112-)
(Flag111-)
(Flag110-)
(Flag109-)
(Flag108-)
(Flag107-)
(Flag106-)
(Flag105-)
(Flag104-)
(Flag103-)
(Flag102-)
(Flag101-)
(Flag99-)
(Flag98-)
(Flag97-)
(Flag96-)
(Flag95-)
(Flag94-)
(Flag93-)
(Flag92-)
(Flag91-)
(Flag90-)
(Flag89-)
(Flag88-)
(Flag87-)
(Flag86-)
(Flag85-)
(Flag84-)
(Flag83-)
(Flag82-)
(Flag80-)
(Flag79-)
(Flag78-)
(Flag77-)
(Flag76-)
(Flag75-)
(Flag74-)
(Flag73-)
(Flag72-)
(Flag71-)
(Flag70-)
(Flag69-)
(Flag68-)
(Flag67-)
(Flag66-)
(Flag65-)
(Flag64-)
(Flag63-)
(Flag62-)
(Flag61-)
(Flag60-)
(Flag59-)
(Flag58-)
(Flag57-)
(Flag56-)
(Flag55-)
(Flag54-)
(Flag53-)
(Flag52-)
(Flag51-)
(Flag50-)
(Flag49-)
(Flag48-)
(Flag47-)
(Flag46-)
(Flag45-)
(Flag44-)
(Flag43-)
(Flag42-)
(Flag41-)
(Flag40-)
(Flag39-)
(Flag38-)
(Flag37-)
(Flag36-)
(Flag35-)
(Flag34-)
(Flag33-)
(Flag32-)
(Flag31-)
(Flag30-)
(Flag29-)
(Flag28-)
(Flag27-)
(Flag26-)
(Flag21-)
(Flag20-)
(Flag19-)
(Flag18-)
(Flag17-)
(Flag16-)
(Flag15-)
(Flag14-)
(Flag13-)
(Flag12-)
(Flag11-)
(Flag10-)
(Flag9-)
(Flag8-)
(Flag7-)
(Flag6-)
(Flag5-)
(Flag1-)
(Flag566-)
(Flag283-)
(Flag281-)
(Flag278-)
(Flag274-)
(Flag269-)
(Flag262-)
(Flag254-)
(Flag246-)
(Flag236-)
(Flag226-)
(Flag214-)
(Flag202-)
(Flag188-)
(Flag173-)
(Flag156-)
(Flag140-)
(Flag120-)
(Flag100-)
(Flag81-)
(NOT-COLUMN19-ROBOT)
(NOT-COLUMN18-ROBOT)
(NOT-COLUMN17-ROBOT)
(NOT-COLUMN16-ROBOT)
(NOT-COLUMN15-ROBOT)
(NOT-COLUMN14-ROBOT)
(NOT-COLUMN13-ROBOT)
(NOT-COLUMN12-ROBOT)
(NOT-COLUMN11-ROBOT)
(NOT-COLUMN10-ROBOT)
(NOT-COLUMN9-ROBOT)
(NOT-COLUMN8-ROBOT)
(NOT-COLUMN7-ROBOT)
(NOT-COLUMN6-ROBOT)
(NOT-COLUMN5-ROBOT)
(NOT-COLUMN4-ROBOT)
(NOT-COLUMN3-ROBOT)
(NOT-COLUMN2-ROBOT)
(NOT-COLUMN1-ROBOT)
(NOT-LEFTOF20-ROBOT)
(LEFTOF19-ROBOT)
(NOT-LEFTOF18-ROBOT)
(NOT-LEFTOF17-ROBOT)
(NOT-LEFTOF16-ROBOT)
(NOT-LEFTOF15-ROBOT)
(NOT-LEFTOF14-ROBOT)
(NOT-LEFTOF13-ROBOT)
(NOT-LEFTOF12-ROBOT)
(NOT-LEFTOF11-ROBOT)
(NOT-LEFTOF10-ROBOT)
(NOT-LEFTOF9-ROBOT)
(NOT-LEFTOF8-ROBOT)
(NOT-LEFTOF7-ROBOT)
(NOT-LEFTOF6-ROBOT)
(NOT-LEFTOF5-ROBOT)
(NOT-LEFTOF4-ROBOT)
(NOT-LEFTOF3-ROBOT)
(NOT-LEFTOF2-ROBOT)
(NOT-RIGHTOF19-ROBOT)
(NOT-RIGHTOF18-ROBOT)
(NOT-RIGHTOF17-ROBOT)
(NOT-RIGHTOF16-ROBOT)
(NOT-RIGHTOF15-ROBOT)
(NOT-RIGHTOF14-ROBOT)
(NOT-RIGHTOF13-ROBOT)
(NOT-RIGHTOF12-ROBOT)
(NOT-RIGHTOF11-ROBOT)
(NOT-RIGHTOF10-ROBOT)
(NOT-RIGHTOF9-ROBOT)
(NOT-RIGHTOF8-ROBOT)
(NOT-RIGHTOF7-ROBOT)
(NOT-RIGHTOF6-ROBOT)
(NOT-RIGHTOF5-ROBOT)
(NOT-RIGHTOF4-ROBOT)
(NOT-RIGHTOF3-ROBOT)
(NOT-RIGHTOF1-ROBOT)
(NOT-CHECKCONSISTENCY-)
(NOT-COLUMN0-ROBOT)
(RIGHTOF2-ROBOT)
(NOT-RIGHTOF0-ROBOT)
(NOT-LEFTOF1-ROBOT)
(NOT-ERROR-)
(NOT-ROW18-ROBOT)
(NOT-ROW17-ROBOT)
(NOT-ROW16-ROBOT)
(NOT-ROW15-ROBOT)
(NOT-ROW14-ROBOT)
(NOT-ROW13-ROBOT)
(NOT-ROW12-ROBOT)
(NOT-ROW11-ROBOT)
(NOT-ROW10-ROBOT)
(NOT-ROW9-ROBOT)
(NOT-ROW8-ROBOT)
(NOT-ROW7-ROBOT)
(NOT-ROW6-ROBOT)
(NOT-ROW5-ROBOT)
(NOT-ROW4-ROBOT)
(NOT-ROW3-ROBOT)
(NOT-ROW2-ROBOT)
(NOT-ROW1-ROBOT)
(NOT-ROW0-ROBOT)
(NOT-ABOVEOF18-ROBOT)
(NOT-ABOVEOF17-ROBOT)
(NOT-ABOVEOF16-ROBOT)
(NOT-ABOVEOF15-ROBOT)
(NOT-ABOVEOF14-ROBOT)
(NOT-ABOVEOF13-ROBOT)
(NOT-ABOVEOF12-ROBOT)
(NOT-ABOVEOF11-ROBOT)
(NOT-ABOVEOF10-ROBOT)
(NOT-ABOVEOF9-ROBOT)
(NOT-ABOVEOF8-ROBOT)
(NOT-ABOVEOF7-ROBOT)
(NOT-ABOVEOF6-ROBOT)
(NOT-ABOVEOF5-ROBOT)
(NOT-ABOVEOF4-ROBOT)
(NOT-ABOVEOF3-ROBOT)
(NOT-ABOVEOF2-ROBOT)
(NOT-ABOVEOF1-ROBOT)
(NOT-BELOWOF18-ROBOT)
(NOT-BELOWOF17-ROBOT)
(NOT-BELOWOF16-ROBOT)
(NOT-BELOWOF15-ROBOT)
(NOT-BELOWOF14-ROBOT)
(NOT-BELOWOF13-ROBOT)
(NOT-BELOWOF12-ROBOT)
(NOT-BELOWOF11-ROBOT)
(NOT-BELOWOF10-ROBOT)
(NOT-BELOWOF9-ROBOT)
(NOT-BELOWOF8-ROBOT)
(NOT-BELOWOF7-ROBOT)
(NOT-BELOWOF6-ROBOT)
(NOT-BELOWOF5-ROBOT)
(NOT-BELOWOF4-ROBOT)
(NOT-BELOWOF3-ROBOT)
(NOT-BELOWOF2-ROBOT)
(NOT-BELOWOF1-ROBOT)
(NOT-ROW19-ROBOT)
(NOT-BELOWOF20-ROBOT)
(BELOWOF19-ROBOT)
(NOT-ABOVEOF19-ROBOT)
)
(:derived (Flag81-)(and (Flag42-)))

(:derived (Flag81-)(and (Flag43-)))

(:derived (Flag81-)(and (Flag44-)))

(:derived (Flag81-)(and (Flag45-)))

(:derived (Flag81-)(and (Flag46-)))

(:derived (Flag81-)(and (Flag47-)))

(:derived (Flag81-)(and (Flag48-)))

(:derived (Flag81-)(and (Flag49-)))

(:derived (Flag81-)(and (Flag50-)))

(:derived (Flag81-)(and (Flag51-)))

(:derived (Flag81-)(and (Flag52-)))

(:derived (Flag81-)(and (Flag53-)))

(:derived (Flag81-)(and (Flag54-)))

(:derived (Flag81-)(and (Flag55-)))

(:derived (Flag81-)(and (Flag56-)))

(:derived (Flag81-)(and (Flag57-)))

(:derived (Flag81-)(and (Flag58-)))

(:derived (Flag81-)(and (Flag59-)))

(:derived (Flag81-)(and (Flag60-)))

(:derived (Flag81-)(and (Flag61-)))

(:derived (Flag81-)(and (Flag62-)))

(:derived (Flag81-)(and (Flag63-)))

(:derived (Flag81-)(and (Flag64-)))

(:derived (Flag81-)(and (Flag65-)))

(:derived (Flag81-)(and (Flag66-)))

(:derived (Flag81-)(and (Flag67-)))

(:derived (Flag81-)(and (Flag68-)))

(:derived (Flag81-)(and (Flag69-)))

(:derived (Flag81-)(and (Flag70-)))

(:derived (Flag81-)(and (Flag71-)))

(:derived (Flag81-)(and (Flag72-)))

(:derived (Flag81-)(and (Flag73-)))

(:derived (Flag81-)(and (Flag74-)))

(:derived (Flag81-)(and (Flag75-)))

(:derived (Flag81-)(and (Flag76-)))

(:derived (Flag81-)(and (Flag77-)))

(:derived (Flag81-)(and (Flag78-)))

(:derived (Flag81-)(and (Flag79-)))

(:derived (Flag81-)(and (Flag80-)))

(:derived (Flag100-)(and (Flag71-)))

(:derived (Flag100-)(and (Flag43-)))

(:derived (Flag100-)(and (Flag44-)))

(:derived (Flag100-)(and (Flag53-)))

(:derived (Flag100-)(and (Flag82-)))

(:derived (Flag100-)(and (Flag46-)))

(:derived (Flag100-)(and (Flag47-)))

(:derived (Flag100-)(and (Flag83-)))

(:derived (Flag100-)(and (Flag68-)))

(:derived (Flag100-)(and (Flag84-)))

(:derived (Flag100-)(and (Flag49-)))

(:derived (Flag100-)(and (Flag50-)))

(:derived (Flag100-)(and (Flag85-)))

(:derived (Flag100-)(and (Flag52-)))

(:derived (Flag100-)(and (Flag64-)))

(:derived (Flag100-)(and (Flag56-)))

(:derived (Flag100-)(and (Flag57-)))

(:derived (Flag100-)(and (Flag86-)))

(:derived (Flag100-)(and (Flag58-)))

(:derived (Flag100-)(and (Flag59-)))

(:derived (Flag100-)(and (Flag66-)))

(:derived (Flag100-)(and (Flag60-)))

(:derived (Flag100-)(and (Flag87-)))

(:derived (Flag100-)(and (Flag67-)))

(:derived (Flag100-)(and (Flag61-)))

(:derived (Flag100-)(and (Flag62-)))

(:derived (Flag100-)(and (Flag63-)))

(:derived (Flag100-)(and (Flag88-)))

(:derived (Flag100-)(and (Flag89-)))

(:derived (Flag100-)(and (Flag55-)))

(:derived (Flag100-)(and (Flag45-)))

(:derived (Flag100-)(and (Flag48-)))

(:derived (Flag100-)(and (Flag90-)))

(:derived (Flag100-)(and (Flag91-)))

(:derived (Flag100-)(and (Flag65-)))

(:derived (Flag100-)(and (Flag92-)))

(:derived (Flag100-)(and (Flag93-)))

(:derived (Flag100-)(and (Flag94-)))

(:derived (Flag100-)(and (Flag95-)))

(:derived (Flag100-)(and (Flag96-)))

(:derived (Flag100-)(and (Flag69-)))

(:derived (Flag100-)(and (Flag70-)))

(:derived (Flag100-)(and (Flag42-)))

(:derived (Flag100-)(and (Flag97-)))

(:derived (Flag100-)(and (Flag72-)))

(:derived (Flag100-)(and (Flag73-)))

(:derived (Flag100-)(and (Flag74-)))

(:derived (Flag100-)(and (Flag75-)))

(:derived (Flag100-)(and (Flag76-)))

(:derived (Flag100-)(and (Flag77-)))

(:derived (Flag100-)(and (Flag78-)))

(:derived (Flag100-)(and (Flag79-)))

(:derived (Flag100-)(and (Flag98-)))

(:derived (Flag100-)(and (Flag80-)))

(:derived (Flag100-)(and (Flag99-)))

(:derived (Flag120-)(and (Flag71-)))

(:derived (Flag120-)(and (Flag101-)))

(:derived (Flag120-)(and (Flag102-)))

(:derived (Flag120-)(and (Flag44-)))

(:derived (Flag120-)(and (Flag53-)))

(:derived (Flag120-)(and (Flag82-)))

(:derived (Flag120-)(and (Flag46-)))

(:derived (Flag120-)(and (Flag47-)))

(:derived (Flag120-)(and (Flag83-)))

(:derived (Flag120-)(and (Flag68-)))

(:derived (Flag120-)(and (Flag76-)))

(:derived (Flag120-)(and (Flag43-)))

(:derived (Flag120-)(and (Flag103-)))

(:derived (Flag120-)(and (Flag84-)))

(:derived (Flag120-)(and (Flag49-)))

(:derived (Flag120-)(and (Flag50-)))

(:derived (Flag120-)(and (Flag65-)))

(:derived (Flag120-)(and (Flag104-)))

(:derived (Flag120-)(and (Flag105-)))

(:derived (Flag120-)(and (Flag85-)))

(:derived (Flag120-)(and (Flag106-)))

(:derived (Flag120-)(and (Flag52-)))

(:derived (Flag120-)(and (Flag89-)))

(:derived (Flag120-)(and (Flag64-)))

(:derived (Flag120-)(and (Flag56-)))

(:derived (Flag120-)(and (Flag57-)))

(:derived (Flag120-)(and (Flag107-)))

(:derived (Flag120-)(and (Flag86-)))

(:derived (Flag120-)(and (Flag58-)))

(:derived (Flag120-)(and (Flag59-)))

(:derived (Flag120-)(and (Flag66-)))

(:derived (Flag120-)(and (Flag60-)))

(:derived (Flag120-)(and (Flag87-)))

(:derived (Flag120-)(and (Flag67-)))

(:derived (Flag120-)(and (Flag61-)))

(:derived (Flag120-)(and (Flag108-)))

(:derived (Flag120-)(and (Flag62-)))

(:derived (Flag120-)(and (Flag63-)))

(:derived (Flag120-)(and (Flag88-)))

(:derived (Flag120-)(and (Flag109-)))

(:derived (Flag120-)(and (Flag55-)))

(:derived (Flag120-)(and (Flag45-)))

(:derived (Flag120-)(and (Flag110-)))

(:derived (Flag120-)(and (Flag48-)))

(:derived (Flag120-)(and (Flag77-)))

(:derived (Flag120-)(and (Flag90-)))

(:derived (Flag120-)(and (Flag91-)))

(:derived (Flag120-)(and (Flag78-)))

(:derived (Flag120-)(and (Flag97-)))

(:derived (Flag120-)(and (Flag93-)))

(:derived (Flag120-)(and (Flag111-)))

(:derived (Flag120-)(and (Flag112-)))

(:derived (Flag120-)(and (Flag94-)))

(:derived (Flag120-)(and (Flag95-)))

(:derived (Flag120-)(and (Flag113-)))

(:derived (Flag120-)(and (Flag96-)))

(:derived (Flag120-)(and (Flag114-)))

(:derived (Flag120-)(and (Flag69-)))

(:derived (Flag120-)(and (Flag70-)))

(:derived (Flag120-)(and (Flag42-)))

(:derived (Flag120-)(and (Flag115-)))

(:derived (Flag120-)(and (Flag116-)))

(:derived (Flag120-)(and (Flag72-)))

(:derived (Flag120-)(and (Flag117-)))

(:derived (Flag120-)(and (Flag75-)))

(:derived (Flag120-)(and (Flag118-)))

(:derived (Flag120-)(and (Flag119-)))

(:derived (Flag120-)(and (Flag79-)))

(:derived (Flag120-)(and (Flag98-)))

(:derived (Flag120-)(and (Flag80-)))

(:derived (Flag140-)(and (Flag121-)))

(:derived (Flag140-)(and (Flag101-)))

(:derived (Flag140-)(and (Flag122-)))

(:derived (Flag140-)(and (Flag44-)))

(:derived (Flag140-)(and (Flag61-)))

(:derived (Flag140-)(and (Flag123-)))

(:derived (Flag140-)(and (Flag53-)))

(:derived (Flag140-)(and (Flag82-)))

(:derived (Flag140-)(and (Flag46-)))

(:derived (Flag140-)(and (Flag83-)))

(:derived (Flag140-)(and (Flag76-)))

(:derived (Flag140-)(and (Flag124-)))

(:derived (Flag140-)(and (Flag125-)))

(:derived (Flag140-)(and (Flag103-)))

(:derived (Flag140-)(and (Flag126-)))

(:derived (Flag140-)(and (Flag84-)))

(:derived (Flag140-)(and (Flag49-)))

(:derived (Flag140-)(and (Flag50-)))

(:derived (Flag140-)(and (Flag127-)))

(:derived (Flag140-)(and (Flag104-)))

(:derived (Flag140-)(and (Flag105-)))

(:derived (Flag140-)(and (Flag128-)))

(:derived (Flag140-)(and (Flag85-)))

(:derived (Flag140-)(and (Flag71-)))

(:derived (Flag140-)(and (Flag106-)))

(:derived (Flag140-)(and (Flag52-)))

(:derived (Flag140-)(and (Flag48-)))

(:derived (Flag140-)(and (Flag89-)))

(:derived (Flag140-)(and (Flag68-)))

(:derived (Flag140-)(and (Flag64-)))

(:derived (Flag140-)(and (Flag56-)))

(:derived (Flag140-)(and (Flag57-)))

(:derived (Flag140-)(and (Flag107-)))

(:derived (Flag140-)(and (Flag86-)))

(:derived (Flag140-)(and (Flag58-)))

(:derived (Flag140-)(and (Flag108-)))

(:derived (Flag140-)(and (Flag60-)))

(:derived (Flag140-)(and (Flag91-)))

(:derived (Flag140-)(and (Flag129-)))

(:derived (Flag140-)(and (Flag130-)))

(:derived (Flag140-)(and (Flag102-)))

(:derived (Flag140-)(and (Flag131-)))

(:derived (Flag140-)(and (Flag45-)))

(:derived (Flag140-)(and (Flag132-)))

(:derived (Flag140-)(and (Flag62-)))

(:derived (Flag140-)(and (Flag63-)))

(:derived (Flag140-)(and (Flag88-)))

(:derived (Flag140-)(and (Flag133-)))

(:derived (Flag140-)(and (Flag55-)))

(:derived (Flag140-)(and (Flag65-)))

(:derived (Flag140-)(and (Flag110-)))

(:derived (Flag140-)(and (Flag134-)))

(:derived (Flag140-)(and (Flag135-)))

(:derived (Flag140-)(and (Flag136-)))

(:derived (Flag140-)(and (Flag77-)))

(:derived (Flag140-)(and (Flag66-)))

(:derived (Flag140-)(and (Flag87-)))

(:derived (Flag140-)(and (Flag78-)))

(:derived (Flag140-)(and (Flag97-)))

(:derived (Flag140-)(and (Flag93-)))

(:derived (Flag140-)(and (Flag111-)))

(:derived (Flag140-)(and (Flag112-)))

(:derived (Flag140-)(and (Flag94-)))

(:derived (Flag140-)(and (Flag95-)))

(:derived (Flag140-)(and (Flag67-)))

(:derived (Flag140-)(and (Flag113-)))

(:derived (Flag140-)(and (Flag96-)))

(:derived (Flag140-)(and (Flag43-)))

(:derived (Flag140-)(and (Flag114-)))

(:derived (Flag140-)(and (Flag69-)))

(:derived (Flag140-)(and (Flag70-)))

(:derived (Flag140-)(and (Flag42-)))

(:derived (Flag140-)(and (Flag115-)))

(:derived (Flag140-)(and (Flag116-)))

(:derived (Flag140-)(and (Flag72-)))

(:derived (Flag140-)(and (Flag117-)))

(:derived (Flag140-)(and (Flag137-)))

(:derived (Flag140-)(and (Flag90-)))

(:derived (Flag140-)(and (Flag75-)))

(:derived (Flag140-)(and (Flag138-)))

(:derived (Flag140-)(and (Flag119-)))

(:derived (Flag140-)(and (Flag79-)))

(:derived (Flag140-)(and (Flag98-)))

(:derived (Flag140-)(and (Flag80-)))

(:derived (Flag140-)(and (Flag139-)))

(:derived (Flag156-)(and (Flag43-)))

(:derived (Flag156-)(and (Flag49-)))

(:derived (Flag156-)(and (Flag141-)))

(:derived (Flag156-)(and (Flag46-)))

(:derived (Flag156-)(and (Flag142-)))

(:derived (Flag156-)(and (Flag130-)))

(:derived (Flag156-)(and (Flag131-)))

(:derived (Flag156-)(and (Flag132-)))

(:derived (Flag156-)(and (Flag88-)))

(:derived (Flag156-)(and (Flag133-)))

(:derived (Flag156-)(and (Flag45-)))

(:derived (Flag156-)(and (Flag143-)))

(:derived (Flag156-)(and (Flag53-)))

(:derived (Flag156-)(and (Flag96-)))

(:derived (Flag156-)(and (Flag72-)))

(:derived (Flag156-)(and (Flag75-)))

(:derived (Flag156-)(and (Flag144-)))

(:derived (Flag156-)(and (Flag44-)))

(:derived (Flag156-)(and (Flag103-)))

(:derived (Flag156-)(and (Flag50-)))

(:derived (Flag156-)(and (Flag128-)))

(:derived (Flag156-)(and (Flag145-)))

(:derived (Flag156-)(and (Flag52-)))

(:derived (Flag156-)(and (Flag55-)))

(:derived (Flag156-)(and (Flag146-)))

(:derived (Flag156-)(and (Flag86-)))

(:derived (Flag156-)(and (Flag108-)))

(:derived (Flag156-)(and (Flag60-)))

(:derived (Flag156-)(and (Flag91-)))

(:derived (Flag156-)(and (Flag129-)))

(:derived (Flag156-)(and (Flag147-)))

(:derived (Flag156-)(and (Flag63-)))

(:derived (Flag156-)(and (Flag64-)))

(:derived (Flag156-)(and (Flag134-)))

(:derived (Flag156-)(and (Flag94-)))

(:derived (Flag156-)(and (Flag107-)))

(:derived (Flag156-)(and (Flag148-)))

(:derived (Flag156-)(and (Flag149-)))

(:derived (Flag156-)(and (Flag116-)))

(:derived (Flag156-)(and (Flag117-)))

(:derived (Flag156-)(and (Flag76-)))

(:derived (Flag156-)(and (Flag150-)))

(:derived (Flag156-)(and (Flag98-)))

(:derived (Flag156-)(and (Flag121-)))

(:derived (Flag156-)(and (Flag122-)))

(:derived (Flag156-)(and (Flag67-)))

(:derived (Flag156-)(and (Flag83-)))

(:derived (Flag156-)(and (Flag124-)))

(:derived (Flag156-)(and (Flag106-)))

(:derived (Flag156-)(and (Flag102-)))

(:derived (Flag156-)(and (Flag137-)))

(:derived (Flag156-)(and (Flag126-)))

(:derived (Flag156-)(and (Flag57-)))

(:derived (Flag156-)(and (Flag151-)))

(:derived (Flag156-)(and (Flag87-)))

(:derived (Flag156-)(and (Flag62-)))

(:derived (Flag156-)(and (Flag90-)))

(:derived (Flag156-)(and (Flag68-)))

(:derived (Flag156-)(and (Flag112-)))

(:derived (Flag156-)(and (Flag113-)))

(:derived (Flag156-)(and (Flag69-)))

(:derived (Flag156-)(and (Flag70-)))

(:derived (Flag156-)(and (Flag152-)))

(:derived (Flag156-)(and (Flag115-)))

(:derived (Flag156-)(and (Flag138-)))

(:derived (Flag156-)(and (Flag78-)))

(:derived (Flag156-)(and (Flag79-)))

(:derived (Flag156-)(and (Flag127-)))

(:derived (Flag156-)(and (Flag153-)))

(:derived (Flag156-)(and (Flag139-)))

(:derived (Flag156-)(and (Flag42-)))

(:derived (Flag156-)(and (Flag101-)))

(:derived (Flag156-)(and (Flag154-)))

(:derived (Flag156-)(and (Flag82-)))

(:derived (Flag156-)(and (Flag48-)))

(:derived (Flag156-)(and (Flag104-)))

(:derived (Flag156-)(and (Flag125-)))

(:derived (Flag156-)(and (Flag89-)))

(:derived (Flag156-)(and (Flag56-)))

(:derived (Flag156-)(and (Flag155-)))

(:derived (Flag156-)(and (Flag58-)))

(:derived (Flag156-)(and (Flag85-)))

(:derived (Flag156-)(and (Flag110-)))

(:derived (Flag156-)(and (Flag135-)))

(:derived (Flag156-)(and (Flag136-)))

(:derived (Flag156-)(and (Flag65-)))

(:derived (Flag156-)(and (Flag93-)))

(:derived (Flag156-)(and (Flag111-)))

(:derived (Flag156-)(and (Flag77-)))

(:derived (Flag156-)(and (Flag95-)))

(:derived (Flag156-)(and (Flag114-)))

(:derived (Flag156-)(and (Flag71-)))

(:derived (Flag156-)(and (Flag97-)))

(:derived (Flag156-)(and (Flag119-)))

(:derived (Flag173-)(and (Flag43-)))

(:derived (Flag173-)(and (Flag49-)))

(:derived (Flag173-)(and (Flag157-)))

(:derived (Flag173-)(and (Flag141-)))

(:derived (Flag173-)(and (Flag158-)))

(:derived (Flag173-)(and (Flag46-)))

(:derived (Flag173-)(and (Flag142-)))

(:derived (Flag173-)(and (Flag130-)))

(:derived (Flag173-)(and (Flag131-)))

(:derived (Flag173-)(and (Flag132-)))

(:derived (Flag173-)(and (Flag97-)))

(:derived (Flag173-)(and (Flag88-)))

(:derived (Flag173-)(and (Flag89-)))

(:derived (Flag173-)(and (Flag45-)))

(:derived (Flag173-)(and (Flag53-)))

(:derived (Flag173-)(and (Flag159-)))

(:derived (Flag173-)(and (Flag160-)))

(:derived (Flag173-)(and (Flag96-)))

(:derived (Flag173-)(and (Flag72-)))

(:derived (Flag173-)(and (Flag75-)))

(:derived (Flag173-)(and (Flag144-)))

(:derived (Flag173-)(and (Flag44-)))

(:derived (Flag173-)(and (Flag103-)))

(:derived (Flag173-)(and (Flag50-)))

(:derived (Flag173-)(and (Flag128-)))

(:derived (Flag173-)(and (Flag161-)))

(:derived (Flag173-)(and (Flag145-)))

(:derived (Flag173-)(and (Flag55-)))

(:derived (Flag173-)(and (Flag146-)))

(:derived (Flag173-)(and (Flag86-)))

(:derived (Flag173-)(and (Flag108-)))

(:derived (Flag173-)(and (Flag60-)))

(:derived (Flag173-)(and (Flag91-)))

(:derived (Flag173-)(and (Flag129-)))

(:derived (Flag173-)(and (Flag147-)))

(:derived (Flag173-)(and (Flag63-)))

(:derived (Flag173-)(and (Flag64-)))

(:derived (Flag173-)(and (Flag162-)))

(:derived (Flag173-)(and (Flag134-)))

(:derived (Flag173-)(and (Flag94-)))

(:derived (Flag173-)(and (Flag107-)))

(:derived (Flag173-)(and (Flag163-)))

(:derived (Flag173-)(and (Flag164-)))

(:derived (Flag173-)(and (Flag148-)))

(:derived (Flag173-)(and (Flag117-)))

(:derived (Flag173-)(and (Flag76-)))

(:derived (Flag173-)(and (Flag150-)))

(:derived (Flag173-)(and (Flag98-)))

(:derived (Flag173-)(and (Flag165-)))

(:derived (Flag173-)(and (Flag121-)))

(:derived (Flag173-)(and (Flag122-)))

(:derived (Flag173-)(and (Flag67-)))

(:derived (Flag173-)(and (Flag166-)))

(:derived (Flag173-)(and (Flag83-)))

(:derived (Flag173-)(and (Flag167-)))

(:derived (Flag173-)(and (Flag124-)))

(:derived (Flag173-)(and (Flag106-)))

(:derived (Flag173-)(and (Flag102-)))

(:derived (Flag173-)(and (Flag137-)))

(:derived (Flag173-)(and (Flag168-)))

(:derived (Flag173-)(and (Flag126-)))

(:derived (Flag173-)(and (Flag151-)))

(:derived (Flag173-)(and (Flag87-)))

(:derived (Flag173-)(and (Flag62-)))

(:derived (Flag173-)(and (Flag68-)))

(:derived (Flag173-)(and (Flag112-)))

(:derived (Flag173-)(and (Flag113-)))

(:derived (Flag173-)(and (Flag69-)))

(:derived (Flag173-)(and (Flag70-)))

(:derived (Flag173-)(and (Flag152-)))

(:derived (Flag173-)(and (Flag115-)))

(:derived (Flag173-)(and (Flag169-)))

(:derived (Flag173-)(and (Flag78-)))

(:derived (Flag173-)(and (Flag79-)))

(:derived (Flag173-)(and (Flag127-)))

(:derived (Flag173-)(and (Flag153-)))

(:derived (Flag173-)(and (Flag139-)))

(:derived (Flag173-)(and (Flag42-)))

(:derived (Flag173-)(and (Flag101-)))

(:derived (Flag173-)(and (Flag154-)))

(:derived (Flag173-)(and (Flag82-)))

(:derived (Flag173-)(and (Flag48-)))

(:derived (Flag173-)(and (Flag104-)))

(:derived (Flag173-)(and (Flag125-)))

(:derived (Flag173-)(and (Flag133-)))

(:derived (Flag173-)(and (Flag56-)))

(:derived (Flag173-)(and (Flag155-)))

(:derived (Flag173-)(and (Flag58-)))

(:derived (Flag173-)(and (Flag85-)))

(:derived (Flag173-)(and (Flag110-)))

(:derived (Flag173-)(and (Flag170-)))

(:derived (Flag173-)(and (Flag135-)))

(:derived (Flag173-)(and (Flag171-)))

(:derived (Flag173-)(and (Flag136-)))

(:derived (Flag173-)(and (Flag65-)))

(:derived (Flag173-)(and (Flag93-)))

(:derived (Flag173-)(and (Flag111-)))

(:derived (Flag173-)(and (Flag77-)))

(:derived (Flag173-)(and (Flag95-)))

(:derived (Flag173-)(and (Flag114-)))

(:derived (Flag173-)(and (Flag71-)))

(:derived (Flag173-)(and (Flag172-)))

(:derived (Flag173-)(and (Flag119-)))

(:derived (Flag188-)(and (Flag43-)))

(:derived (Flag188-)(and (Flag49-)))

(:derived (Flag188-)(and (Flag68-)))

(:derived (Flag188-)(and (Flag141-)))

(:derived (Flag188-)(and (Flag158-)))

(:derived (Flag188-)(and (Flag46-)))

(:derived (Flag188-)(and (Flag142-)))

(:derived (Flag188-)(and (Flag130-)))

(:derived (Flag188-)(and (Flag174-)))

(:derived (Flag188-)(and (Flag132-)))

(:derived (Flag188-)(and (Flag88-)))

(:derived (Flag188-)(and (Flag89-)))

(:derived (Flag188-)(and (Flag45-)))

(:derived (Flag188-)(and (Flag91-)))

(:derived (Flag188-)(and (Flag154-)))

(:derived (Flag188-)(and (Flag159-)))

(:derived (Flag188-)(and (Flag96-)))

(:derived (Flag188-)(and (Flag72-)))

(:derived (Flag188-)(and (Flag144-)))

(:derived (Flag188-)(and (Flag44-)))

(:derived (Flag188-)(and (Flag103-)))

(:derived (Flag188-)(and (Flag50-)))

(:derived (Flag188-)(and (Flag175-)))

(:derived (Flag188-)(and (Flag128-)))

(:derived (Flag188-)(and (Flag161-)))

(:derived (Flag188-)(and (Flag145-)))

(:derived (Flag188-)(and (Flag64-)))

(:derived (Flag188-)(and (Flag146-)))

(:derived (Flag188-)(and (Flag86-)))

(:derived (Flag188-)(and (Flag108-)))

(:derived (Flag188-)(and (Flag60-)))

(:derived (Flag188-)(and (Flag176-)))

(:derived (Flag188-)(and (Flag129-)))

(:derived (Flag188-)(and (Flag147-)))

(:derived (Flag188-)(and (Flag177-)))

(:derived (Flag188-)(and (Flag63-)))

(:derived (Flag188-)(and (Flag55-)))

(:derived (Flag188-)(and (Flag162-)))

(:derived (Flag188-)(and (Flag134-)))

(:derived (Flag188-)(and (Flag178-)))

(:derived (Flag188-)(and (Flag94-)))

(:derived (Flag188-)(and (Flag107-)))

(:derived (Flag188-)(and (Flag164-)))

(:derived (Flag188-)(and (Flag148-)))

(:derived (Flag188-)(and (Flag117-)))

(:derived (Flag188-)(and (Flag76-)))

(:derived (Flag188-)(and (Flag150-)))

(:derived (Flag188-)(and (Flag98-)))

(:derived (Flag188-)(and (Flag165-)))

(:derived (Flag188-)(and (Flag121-)))

(:derived (Flag188-)(and (Flag122-)))

(:derived (Flag188-)(and (Flag67-)))

(:derived (Flag188-)(and (Flag166-)))

(:derived (Flag188-)(and (Flag83-)))

(:derived (Flag188-)(and (Flag167-)))

(:derived (Flag188-)(and (Flag124-)))

(:derived (Flag188-)(and (Flag179-)))

(:derived (Flag188-)(and (Flag180-)))

(:derived (Flag188-)(and (Flag181-)))

(:derived (Flag188-)(and (Flag106-)))

(:derived (Flag188-)(and (Flag102-)))

(:derived (Flag188-)(and (Flag182-)))

(:derived (Flag188-)(and (Flag137-)))

(:derived (Flag188-)(and (Flag168-)))

(:derived (Flag188-)(and (Flag151-)))

(:derived (Flag188-)(and (Flag87-)))

(:derived (Flag188-)(and (Flag183-)))

(:derived (Flag188-)(and (Flag97-)))

(:derived (Flag188-)(and (Flag112-)))

(:derived (Flag188-)(and (Flag113-)))

(:derived (Flag188-)(and (Flag69-)))

(:derived (Flag188-)(and (Flag70-)))

(:derived (Flag188-)(and (Flag152-)))

(:derived (Flag188-)(and (Flag184-)))

(:derived (Flag188-)(and (Flag169-)))

(:derived (Flag188-)(and (Flag78-)))

(:derived (Flag188-)(and (Flag127-)))

(:derived (Flag188-)(and (Flag153-)))

(:derived (Flag188-)(and (Flag185-)))

(:derived (Flag188-)(and (Flag139-)))

(:derived (Flag188-)(and (Flag71-)))

(:derived (Flag188-)(and (Flag101-)))

(:derived (Flag188-)(and (Flag53-)))

(:derived (Flag188-)(and (Flag126-)))

(:derived (Flag188-)(and (Flag48-)))

(:derived (Flag188-)(and (Flag104-)))

(:derived (Flag188-)(and (Flag62-)))

(:derived (Flag188-)(and (Flag125-)))

(:derived (Flag188-)(and (Flag133-)))

(:derived (Flag188-)(and (Flag56-)))

(:derived (Flag188-)(and (Flag155-)))

(:derived (Flag188-)(and (Flag58-)))

(:derived (Flag188-)(and (Flag186-)))

(:derived (Flag188-)(and (Flag85-)))

(:derived (Flag188-)(and (Flag110-)))

(:derived (Flag188-)(and (Flag170-)))

(:derived (Flag188-)(and (Flag135-)))

(:derived (Flag188-)(and (Flag171-)))

(:derived (Flag188-)(and (Flag136-)))

(:derived (Flag188-)(and (Flag65-)))

(:derived (Flag188-)(and (Flag93-)))

(:derived (Flag188-)(and (Flag111-)))

(:derived (Flag188-)(and (Flag77-)))

(:derived (Flag188-)(and (Flag95-)))

(:derived (Flag188-)(and (Flag187-)))

(:derived (Flag188-)(and (Flag114-)))

(:derived (Flag188-)(and (Flag42-)))

(:derived (Flag188-)(and (Flag172-)))

(:derived (Flag188-)(and (Flag119-)))

(:derived (Flag202-)(and (Flag43-)))

(:derived (Flag202-)(and (Flag49-)))

(:derived (Flag202-)(and (Flag68-)))

(:derived (Flag202-)(and (Flag189-)))

(:derived (Flag202-)(and (Flag190-)))

(:derived (Flag202-)(and (Flag141-)))

(:derived (Flag202-)(and (Flag158-)))

(:derived (Flag202-)(and (Flag46-)))

(:derived (Flag202-)(and (Flag142-)))

(:derived (Flag202-)(and (Flag130-)))

(:derived (Flag202-)(and (Flag174-)))

(:derived (Flag202-)(and (Flag132-)))

(:derived (Flag202-)(and (Flag88-)))

(:derived (Flag202-)(and (Flag89-)))

(:derived (Flag202-)(and (Flag45-)))

(:derived (Flag202-)(and (Flag191-)))

(:derived (Flag202-)(and (Flag91-)))

(:derived (Flag202-)(and (Flag53-)))

(:derived (Flag202-)(and (Flag159-)))

(:derived (Flag202-)(and (Flag96-)))

(:derived (Flag202-)(and (Flag167-)))

(:derived (Flag202-)(and (Flag72-)))

(:derived (Flag202-)(and (Flag124-)))

(:derived (Flag202-)(and (Flag144-)))

(:derived (Flag202-)(and (Flag44-)))

(:derived (Flag202-)(and (Flag103-)))

(:derived (Flag202-)(and (Flag50-)))

(:derived (Flag202-)(and (Flag175-)))

(:derived (Flag202-)(and (Flag128-)))

(:derived (Flag202-)(and (Flag161-)))

(:derived (Flag202-)(and (Flag192-)))

(:derived (Flag202-)(and (Flag146-)))

(:derived (Flag202-)(and (Flag108-)))

(:derived (Flag202-)(and (Flag60-)))

(:derived (Flag202-)(and (Flag176-)))

(:derived (Flag202-)(and (Flag129-)))

(:derived (Flag202-)(and (Flag147-)))

(:derived (Flag202-)(and (Flag63-)))

(:derived (Flag202-)(and (Flag193-)))

(:derived (Flag202-)(and (Flag162-)))

(:derived (Flag202-)(and (Flag134-)))

(:derived (Flag202-)(and (Flag194-)))

(:derived (Flag202-)(and (Flag178-)))

(:derived (Flag202-)(and (Flag94-)))

(:derived (Flag202-)(and (Flag107-)))

(:derived (Flag202-)(and (Flag164-)))

(:derived (Flag202-)(and (Flag148-)))

(:derived (Flag202-)(and (Flag117-)))

(:derived (Flag202-)(and (Flag76-)))

(:derived (Flag202-)(and (Flag150-)))

(:derived (Flag202-)(and (Flag98-)))

(:derived (Flag202-)(and (Flag165-)))

(:derived (Flag202-)(and (Flag121-)))

(:derived (Flag202-)(and (Flag122-)))

(:derived (Flag202-)(and (Flag67-)))

(:derived (Flag202-)(and (Flag166-)))

(:derived (Flag202-)(and (Flag83-)))

(:derived (Flag202-)(and (Flag195-)))

(:derived (Flag202-)(and (Flag93-)))

(:derived (Flag202-)(and (Flag179-)))

(:derived (Flag202-)(and (Flag181-)))

(:derived (Flag202-)(and (Flag102-)))

(:derived (Flag202-)(and (Flag182-)))

(:derived (Flag202-)(and (Flag137-)))

(:derived (Flag202-)(and (Flag168-)))

(:derived (Flag202-)(and (Flag151-)))

(:derived (Flag202-)(and (Flag87-)))

(:derived (Flag202-)(and (Flag196-)))

(:derived (Flag202-)(and (Flag183-)))

(:derived (Flag202-)(and (Flag97-)))

(:derived (Flag202-)(and (Flag112-)))

(:derived (Flag202-)(and (Flag113-)))

(:derived (Flag202-)(and (Flag197-)))

(:derived (Flag202-)(and (Flag69-)))

(:derived (Flag202-)(and (Flag70-)))

(:derived (Flag202-)(and (Flag152-)))

(:derived (Flag202-)(and (Flag184-)))

(:derived (Flag202-)(and (Flag169-)))

(:derived (Flag202-)(and (Flag78-)))

(:derived (Flag202-)(and (Flag127-)))

(:derived (Flag202-)(and (Flag153-)))

(:derived (Flag202-)(and (Flag185-)))

(:derived (Flag202-)(and (Flag139-)))

(:derived (Flag202-)(and (Flag71-)))

(:derived (Flag202-)(and (Flag101-)))

(:derived (Flag202-)(and (Flag154-)))

(:derived (Flag202-)(and (Flag48-)))

(:derived (Flag202-)(and (Flag198-)))

(:derived (Flag202-)(and (Flag104-)))

(:derived (Flag202-)(and (Flag62-)))

(:derived (Flag202-)(and (Flag199-)))

(:derived (Flag202-)(and (Flag125-)))

(:derived (Flag202-)(and (Flag133-)))

(:derived (Flag202-)(and (Flag56-)))

(:derived (Flag202-)(and (Flag155-)))

(:derived (Flag202-)(and (Flag58-)))

(:derived (Flag202-)(and (Flag186-)))

(:derived (Flag202-)(and (Flag85-)))

(:derived (Flag202-)(and (Flag170-)))

(:derived (Flag202-)(and (Flag135-)))

(:derived (Flag202-)(and (Flag171-)))

(:derived (Flag202-)(and (Flag136-)))

(:derived (Flag202-)(and (Flag77-)))

(:derived (Flag202-)(and (Flag65-)))

(:derived (Flag202-)(and (Flag200-)))

(:derived (Flag202-)(and (Flag111-)))

(:derived (Flag202-)(and (Flag119-)))

(:derived (Flag202-)(and (Flag95-)))

(:derived (Flag202-)(and (Flag187-)))

(:derived (Flag202-)(and (Flag114-)))

(:derived (Flag202-)(and (Flag42-)))

(:derived (Flag202-)(and (Flag172-)))

(:derived (Flag202-)(and (Flag201-)))

(:derived (Flag214-)(and (Flag43-)))

(:derived (Flag214-)(and (Flag49-)))

(:derived (Flag214-)(and (Flag68-)))

(:derived (Flag214-)(and (Flag203-)))

(:derived (Flag214-)(and (Flag189-)))

(:derived (Flag214-)(and (Flag141-)))

(:derived (Flag214-)(and (Flag204-)))

(:derived (Flag214-)(and (Flag158-)))

(:derived (Flag214-)(and (Flag142-)))

(:derived (Flag214-)(and (Flag174-)))

(:derived (Flag214-)(and (Flag132-)))

(:derived (Flag214-)(and (Flag88-)))

(:derived (Flag214-)(and (Flag133-)))

(:derived (Flag214-)(and (Flag45-)))

(:derived (Flag214-)(and (Flag191-)))

(:derived (Flag214-)(and (Flag91-)))

(:derived (Flag214-)(and (Flag53-)))

(:derived (Flag214-)(and (Flag159-)))

(:derived (Flag214-)(and (Flag96-)))

(:derived (Flag214-)(and (Flag124-)))

(:derived (Flag214-)(and (Flag144-)))

(:derived (Flag214-)(and (Flag44-)))

(:derived (Flag214-)(and (Flag103-)))

(:derived (Flag214-)(and (Flag50-)))

(:derived (Flag214-)(and (Flag175-)))

(:derived (Flag214-)(and (Flag128-)))

(:derived (Flag214-)(and (Flag161-)))

(:derived (Flag214-)(and (Flag192-)))

(:derived (Flag214-)(and (Flag146-)))

(:derived (Flag214-)(and (Flag108-)))

(:derived (Flag214-)(and (Flag60-)))

(:derived (Flag214-)(and (Flag176-)))

(:derived (Flag214-)(and (Flag129-)))

(:derived (Flag214-)(and (Flag147-)))

(:derived (Flag214-)(and (Flag63-)))

(:derived (Flag214-)(and (Flag193-)))

(:derived (Flag214-)(and (Flag162-)))

(:derived (Flag214-)(and (Flag134-)))

(:derived (Flag214-)(and (Flag194-)))

(:derived (Flag214-)(and (Flag178-)))

(:derived (Flag214-)(and (Flag94-)))

(:derived (Flag214-)(and (Flag107-)))

(:derived (Flag214-)(and (Flag205-)))

(:derived (Flag214-)(and (Flag164-)))

(:derived (Flag214-)(and (Flag148-)))

(:derived (Flag214-)(and (Flag136-)))

(:derived (Flag214-)(and (Flag206-)))

(:derived (Flag214-)(and (Flag117-)))

(:derived (Flag214-)(and (Flag76-)))

(:derived (Flag214-)(and (Flag150-)))

(:derived (Flag214-)(and (Flag98-)))

(:derived (Flag214-)(and (Flag165-)))

(:derived (Flag214-)(and (Flag121-)))

(:derived (Flag214-)(and (Flag122-)))

(:derived (Flag214-)(and (Flag46-)))

(:derived (Flag214-)(and (Flag207-)))

(:derived (Flag214-)(and (Flag83-)))

(:derived (Flag214-)(and (Flag195-)))

(:derived (Flag214-)(and (Flag93-)))

(:derived (Flag214-)(and (Flag179-)))

(:derived (Flag214-)(and (Flag208-)))

(:derived (Flag214-)(and (Flag181-)))

(:derived (Flag214-)(and (Flag102-)))

(:derived (Flag214-)(and (Flag182-)))

(:derived (Flag214-)(and (Flag137-)))

(:derived (Flag214-)(and (Flag168-)))

(:derived (Flag214-)(and (Flag151-)))

(:derived (Flag214-)(and (Flag87-)))

(:derived (Flag214-)(and (Flag196-)))

(:derived (Flag214-)(and (Flag183-)))

(:derived (Flag214-)(and (Flag209-)))

(:derived (Flag214-)(and (Flag97-)))

(:derived (Flag214-)(and (Flag112-)))

(:derived (Flag214-)(and (Flag113-)))

(:derived (Flag214-)(and (Flag197-)))

(:derived (Flag214-)(and (Flag69-)))

(:derived (Flag214-)(and (Flag70-)))

(:derived (Flag214-)(and (Flag152-)))

(:derived (Flag214-)(and (Flag184-)))

(:derived (Flag214-)(and (Flag169-)))

(:derived (Flag214-)(and (Flag78-)))

(:derived (Flag214-)(and (Flag127-)))

(:derived (Flag214-)(and (Flag153-)))

(:derived (Flag214-)(and (Flag185-)))

(:derived (Flag214-)(and (Flag139-)))

(:derived (Flag214-)(and (Flag71-)))

(:derived (Flag214-)(and (Flag101-)))

(:derived (Flag214-)(and (Flag166-)))

(:derived (Flag214-)(and (Flag154-)))

(:derived (Flag214-)(and (Flag48-)))

(:derived (Flag214-)(and (Flag210-)))

(:derived (Flag214-)(and (Flag211-)))

(:derived (Flag214-)(and (Flag198-)))

(:derived (Flag214-)(and (Flag104-)))

(:derived (Flag214-)(and (Flag62-)))

(:derived (Flag214-)(and (Flag199-)))

(:derived (Flag214-)(and (Flag125-)))

(:derived (Flag214-)(and (Flag56-)))

(:derived (Flag214-)(and (Flag58-)))

(:derived (Flag214-)(and (Flag85-)))

(:derived (Flag214-)(and (Flag170-)))

(:derived (Flag214-)(and (Flag171-)))

(:derived (Flag214-)(and (Flag212-)))

(:derived (Flag214-)(and (Flag77-)))

(:derived (Flag214-)(and (Flag67-)))

(:derived (Flag214-)(and (Flag200-)))

(:derived (Flag214-)(and (Flag111-)))

(:derived (Flag214-)(and (Flag119-)))

(:derived (Flag214-)(and (Flag95-)))

(:derived (Flag214-)(and (Flag187-)))

(:derived (Flag214-)(and (Flag114-)))

(:derived (Flag214-)(and (Flag42-)))

(:derived (Flag214-)(and (Flag213-)))

(:derived (Flag214-)(and (Flag172-)))

(:derived (Flag214-)(and (Flag201-)))

(:derived (Flag226-)(and (Flag43-)))

(:derived (Flag226-)(and (Flag49-)))

(:derived (Flag226-)(and (Flag215-)))

(:derived (Flag226-)(and (Flag203-)))

(:derived (Flag226-)(and (Flag189-)))

(:derived (Flag226-)(and (Flag216-)))

(:derived (Flag226-)(and (Flag141-)))

(:derived (Flag226-)(and (Flag204-)))

(:derived (Flag226-)(and (Flag158-)))

(:derived (Flag226-)(and (Flag142-)))

(:derived (Flag226-)(and (Flag174-)))

(:derived (Flag226-)(and (Flag132-)))

(:derived (Flag226-)(and (Flag88-)))

(:derived (Flag226-)(and (Flag133-)))

(:derived (Flag226-)(and (Flag45-)))

(:derived (Flag226-)(and (Flag217-)))

(:derived (Flag226-)(and (Flag191-)))

(:derived (Flag226-)(and (Flag91-)))

(:derived (Flag226-)(and (Flag96-)))

(:derived (Flag226-)(and (Flag124-)))

(:derived (Flag226-)(and (Flag144-)))

(:derived (Flag226-)(and (Flag44-)))

(:derived (Flag226-)(and (Flag103-)))

(:derived (Flag226-)(and (Flag50-)))

(:derived (Flag226-)(and (Flag207-)))

(:derived (Flag226-)(and (Flag175-)))

(:derived (Flag226-)(and (Flag128-)))

(:derived (Flag226-)(and (Flag161-)))

(:derived (Flag226-)(and (Flag146-)))

(:derived (Flag226-)(and (Flag108-)))

(:derived (Flag226-)(and (Flag60-)))

(:derived (Flag226-)(and (Flag176-)))

(:derived (Flag226-)(and (Flag129-)))

(:derived (Flag226-)(and (Flag218-)))

(:derived (Flag226-)(and (Flag147-)))

(:derived (Flag226-)(and (Flag63-)))

(:derived (Flag226-)(and (Flag193-)))

(:derived (Flag226-)(and (Flag162-)))

(:derived (Flag226-)(and (Flag134-)))

(:derived (Flag226-)(and (Flag194-)))

(:derived (Flag226-)(and (Flag219-)))

(:derived (Flag226-)(and (Flag178-)))

(:derived (Flag226-)(and (Flag94-)))

(:derived (Flag226-)(and (Flag107-)))

(:derived (Flag226-)(and (Flag171-)))

(:derived (Flag226-)(and (Flag164-)))

(:derived (Flag226-)(and (Flag148-)))

(:derived (Flag226-)(and (Flag136-)))

(:derived (Flag226-)(and (Flag206-)))

(:derived (Flag226-)(and (Flag127-)))

(:derived (Flag226-)(and (Flag117-)))

(:derived (Flag226-)(and (Flag76-)))

(:derived (Flag226-)(and (Flag150-)))

(:derived (Flag226-)(and (Flag97-)))

(:derived (Flag226-)(and (Flag165-)))

(:derived (Flag226-)(and (Flag220-)))

(:derived (Flag226-)(and (Flag122-)))

(:derived (Flag226-)(and (Flag78-)))

(:derived (Flag226-)(and (Flag166-)))

(:derived (Flag226-)(and (Flag83-)))

(:derived (Flag226-)(and (Flag195-)))

(:derived (Flag226-)(and (Flag93-)))

(:derived (Flag226-)(and (Flag208-)))

(:derived (Flag226-)(and (Flag181-)))

(:derived (Flag226-)(and (Flag102-)))

(:derived (Flag226-)(and (Flag182-)))

(:derived (Flag226-)(and (Flag137-)))

(:derived (Flag226-)(and (Flag168-)))

(:derived (Flag226-)(and (Flag151-)))

(:derived (Flag226-)(and (Flag87-)))

(:derived (Flag226-)(and (Flag196-)))

(:derived (Flag226-)(and (Flag183-)))

(:derived (Flag226-)(and (Flag68-)))

(:derived (Flag226-)(and (Flag197-)))

(:derived (Flag226-)(and (Flag69-)))

(:derived (Flag226-)(and (Flag70-)))

(:derived (Flag226-)(and (Flag152-)))

(:derived (Flag226-)(and (Flag184-)))

(:derived (Flag226-)(and (Flag169-)))

(:derived (Flag226-)(and (Flag221-)))

(:derived (Flag226-)(and (Flag185-)))

(:derived (Flag226-)(and (Flag139-)))

(:derived (Flag226-)(and (Flag71-)))

(:derived (Flag226-)(and (Flag101-)))

(:derived (Flag226-)(and (Flag154-)))

(:derived (Flag226-)(and (Flag48-)))

(:derived (Flag226-)(and (Flag210-)))

(:derived (Flag226-)(and (Flag211-)))

(:derived (Flag226-)(and (Flag198-)))

(:derived (Flag226-)(and (Flag104-)))

(:derived (Flag226-)(and (Flag62-)))

(:derived (Flag226-)(and (Flag199-)))

(:derived (Flag226-)(and (Flag125-)))

(:derived (Flag226-)(and (Flag222-)))

(:derived (Flag226-)(and (Flag56-)))

(:derived (Flag226-)(and (Flag58-)))

(:derived (Flag226-)(and (Flag85-)))

(:derived (Flag226-)(and (Flag170-)))

(:derived (Flag226-)(and (Flag205-)))

(:derived (Flag226-)(and (Flag212-)))

(:derived (Flag226-)(and (Flag77-)))

(:derived (Flag226-)(and (Flag67-)))

(:derived (Flag226-)(and (Flag200-)))

(:derived (Flag226-)(and (Flag111-)))

(:derived (Flag226-)(and (Flag119-)))

(:derived (Flag226-)(and (Flag95-)))

(:derived (Flag226-)(and (Flag187-)))

(:derived (Flag226-)(and (Flag223-)))

(:derived (Flag226-)(and (Flag113-)))

(:derived (Flag226-)(and (Flag42-)))

(:derived (Flag226-)(and (Flag213-)))

(:derived (Flag226-)(and (Flag172-)))

(:derived (Flag226-)(and (Flag224-)))

(:derived (Flag226-)(and (Flag225-)))

(:derived (Flag226-)(and (Flag201-)))

(:derived (Flag236-)(and (Flag43-)))

(:derived (Flag236-)(and (Flag49-)))

(:derived (Flag236-)(and (Flag215-)))

(:derived (Flag236-)(and (Flag203-)))

(:derived (Flag236-)(and (Flag189-)))

(:derived (Flag236-)(and (Flag216-)))

(:derived (Flag236-)(and (Flag141-)))

(:derived (Flag236-)(and (Flag204-)))

(:derived (Flag236-)(and (Flag158-)))

(:derived (Flag236-)(and (Flag142-)))

(:derived (Flag236-)(and (Flag227-)))

(:derived (Flag236-)(and (Flag228-)))

(:derived (Flag236-)(and (Flag125-)))

(:derived (Flag236-)(and (Flag207-)))

(:derived (Flag236-)(and (Flag45-)))

(:derived (Flag236-)(and (Flag217-)))

(:derived (Flag236-)(and (Flag191-)))

(:derived (Flag236-)(and (Flag91-)))

(:derived (Flag236-)(and (Flag96-)))

(:derived (Flag236-)(and (Flag124-)))

(:derived (Flag236-)(and (Flag144-)))

(:derived (Flag236-)(and (Flag229-)))

(:derived (Flag236-)(and (Flag44-)))

(:derived (Flag236-)(and (Flag103-)))

(:derived (Flag236-)(and (Flag175-)))

(:derived (Flag236-)(and (Flag128-)))

(:derived (Flag236-)(and (Flag161-)))

(:derived (Flag236-)(and (Flag146-)))

(:derived (Flag236-)(and (Flag108-)))

(:derived (Flag236-)(and (Flag60-)))

(:derived (Flag236-)(and (Flag176-)))

(:derived (Flag236-)(and (Flag129-)))

(:derived (Flag236-)(and (Flag218-)))

(:derived (Flag236-)(and (Flag147-)))

(:derived (Flag236-)(and (Flag63-)))

(:derived (Flag236-)(and (Flag193-)))

(:derived (Flag236-)(and (Flag162-)))

(:derived (Flag236-)(and (Flag134-)))

(:derived (Flag236-)(and (Flag194-)))

(:derived (Flag236-)(and (Flag219-)))

(:derived (Flag236-)(and (Flag178-)))

(:derived (Flag236-)(and (Flag230-)))

(:derived (Flag236-)(and (Flag94-)))

(:derived (Flag236-)(and (Flag107-)))

(:derived (Flag236-)(and (Flag171-)))

(:derived (Flag236-)(and (Flag164-)))

(:derived (Flag236-)(and (Flag136-)))

(:derived (Flag236-)(and (Flag206-)))

(:derived (Flag236-)(and (Flag127-)))

(:derived (Flag236-)(and (Flag117-)))

(:derived (Flag236-)(and (Flag76-)))

(:derived (Flag236-)(and (Flag150-)))

(:derived (Flag236-)(and (Flag97-)))

(:derived (Flag236-)(and (Flag165-)))

(:derived (Flag236-)(and (Flag220-)))

(:derived (Flag236-)(and (Flag122-)))

(:derived (Flag236-)(and (Flag78-)))

(:derived (Flag236-)(and (Flag166-)))

(:derived (Flag236-)(and (Flag83-)))

(:derived (Flag236-)(and (Flag195-)))

(:derived (Flag236-)(and (Flag231-)))

(:derived (Flag236-)(and (Flag93-)))

(:derived (Flag236-)(and (Flag208-)))

(:derived (Flag236-)(and (Flag181-)))

(:derived (Flag236-)(and (Flag232-)))

(:derived (Flag236-)(and (Flag102-)))

(:derived (Flag236-)(and (Flag182-)))

(:derived (Flag236-)(and (Flag137-)))

(:derived (Flag236-)(and (Flag168-)))

(:derived (Flag236-)(and (Flag151-)))

(:derived (Flag236-)(and (Flag87-)))

(:derived (Flag236-)(and (Flag233-)))

(:derived (Flag236-)(and (Flag183-)))

(:derived (Flag236-)(and (Flag68-)))

(:derived (Flag236-)(and (Flag197-)))

(:derived (Flag236-)(and (Flag69-)))

(:derived (Flag236-)(and (Flag70-)))

(:derived (Flag236-)(and (Flag152-)))

(:derived (Flag236-)(and (Flag184-)))

(:derived (Flag236-)(and (Flag169-)))

(:derived (Flag236-)(and (Flag221-)))

(:derived (Flag236-)(and (Flag185-)))

(:derived (Flag236-)(and (Flag139-)))

(:derived (Flag236-)(and (Flag71-)))

(:derived (Flag236-)(and (Flag101-)))

(:derived (Flag236-)(and (Flag154-)))

(:derived (Flag236-)(and (Flag48-)))

(:derived (Flag236-)(and (Flag210-)))

(:derived (Flag236-)(and (Flag198-)))

(:derived (Flag236-)(and (Flag104-)))

(:derived (Flag236-)(and (Flag199-)))

(:derived (Flag236-)(and (Flag222-)))

(:derived (Flag236-)(and (Flag56-)))

(:derived (Flag236-)(and (Flag58-)))

(:derived (Flag236-)(and (Flag234-)))

(:derived (Flag236-)(and (Flag85-)))

(:derived (Flag236-)(and (Flag170-)))

(:derived (Flag236-)(and (Flag205-)))

(:derived (Flag236-)(and (Flag212-)))

(:derived (Flag236-)(and (Flag77-)))

(:derived (Flag236-)(and (Flag67-)))

(:derived (Flag236-)(and (Flag200-)))

(:derived (Flag236-)(and (Flag111-)))

(:derived (Flag236-)(and (Flag119-)))

(:derived (Flag236-)(and (Flag95-)))

(:derived (Flag236-)(and (Flag187-)))

(:derived (Flag236-)(and (Flag223-)))

(:derived (Flag236-)(and (Flag113-)))

(:derived (Flag236-)(and (Flag235-)))

(:derived (Flag236-)(and (Flag42-)))

(:derived (Flag236-)(and (Flag213-)))

(:derived (Flag236-)(and (Flag224-)))

(:derived (Flag236-)(and (Flag201-)))

(:derived (Flag246-)(and (Flag43-)))

(:derived (Flag246-)(and (Flag49-)))

(:derived (Flag246-)(and (Flag215-)))

(:derived (Flag246-)(and (Flag203-)))

(:derived (Flag246-)(and (Flag237-)))

(:derived (Flag246-)(and (Flag216-)))

(:derived (Flag246-)(and (Flag141-)))

(:derived (Flag246-)(and (Flag204-)))

(:derived (Flag246-)(and (Flag158-)))

(:derived (Flag246-)(and (Flag227-)))

(:derived (Flag246-)(and (Flag125-)))

(:derived (Flag246-)(and (Flag45-)))

(:derived (Flag246-)(and (Flag191-)))

(:derived (Flag246-)(and (Flag91-)))

(:derived (Flag246-)(and (Flag205-)))

(:derived (Flag246-)(and (Flag96-)))

(:derived (Flag246-)(and (Flag238-)))

(:derived (Flag246-)(and (Flag144-)))

(:derived (Flag246-)(and (Flag229-)))

(:derived (Flag246-)(and (Flag239-)))

(:derived (Flag246-)(and (Flag44-)))

(:derived (Flag246-)(and (Flag103-)))

(:derived (Flag246-)(and (Flag240-)))

(:derived (Flag246-)(and (Flag175-)))

(:derived (Flag246-)(and (Flag128-)))

(:derived (Flag246-)(and (Flag161-)))

(:derived (Flag246-)(and (Flag108-)))

(:derived (Flag246-)(and (Flag60-)))

(:derived (Flag246-)(and (Flag176-)))

(:derived (Flag246-)(and (Flag129-)))

(:derived (Flag246-)(and (Flag218-)))

(:derived (Flag246-)(and (Flag147-)))

(:derived (Flag246-)(and (Flag63-)))

(:derived (Flag246-)(and (Flag193-)))

(:derived (Flag246-)(and (Flag162-)))

(:derived (Flag246-)(and (Flag134-)))

(:derived (Flag246-)(and (Flag219-)))

(:derived (Flag246-)(and (Flag178-)))

(:derived (Flag246-)(and (Flag189-)))

(:derived (Flag246-)(and (Flag230-)))

(:derived (Flag246-)(and (Flag94-)))

(:derived (Flag246-)(and (Flag107-)))

(:derived (Flag246-)(and (Flag241-)))

(:derived (Flag246-)(and (Flag164-)))

(:derived (Flag246-)(and (Flag136-)))

(:derived (Flag246-)(and (Flag206-)))

(:derived (Flag246-)(and (Flag117-)))

(:derived (Flag246-)(and (Flag76-)))

(:derived (Flag246-)(and (Flag150-)))

(:derived (Flag246-)(and (Flag97-)))

(:derived (Flag246-)(and (Flag139-)))

(:derived (Flag246-)(and (Flag165-)))

(:derived (Flag246-)(and (Flag220-)))

(:derived (Flag246-)(and (Flag122-)))

(:derived (Flag246-)(and (Flag78-)))

(:derived (Flag246-)(and (Flag207-)))

(:derived (Flag246-)(and (Flag83-)))

(:derived (Flag246-)(and (Flag195-)))

(:derived (Flag246-)(and (Flag124-)))

(:derived (Flag246-)(and (Flag208-)))

(:derived (Flag246-)(and (Flag181-)))

(:derived (Flag246-)(and (Flag232-)))

(:derived (Flag246-)(and (Flag102-)))

(:derived (Flag246-)(and (Flag182-)))

(:derived (Flag246-)(and (Flag137-)))

(:derived (Flag246-)(and (Flag168-)))

(:derived (Flag246-)(and (Flag151-)))

(:derived (Flag246-)(and (Flag87-)))

(:derived (Flag246-)(and (Flag233-)))

(:derived (Flag246-)(and (Flag183-)))

(:derived (Flag246-)(and (Flag68-)))

(:derived (Flag246-)(and (Flag197-)))

(:derived (Flag246-)(and (Flag69-)))

(:derived (Flag246-)(and (Flag70-)))

(:derived (Flag246-)(and (Flag152-)))

(:derived (Flag246-)(and (Flag242-)))

(:derived (Flag246-)(and (Flag169-)))

(:derived (Flag246-)(and (Flag221-)))

(:derived (Flag246-)(and (Flag185-)))

(:derived (Flag246-)(and (Flag243-)))

(:derived (Flag246-)(and (Flag101-)))

(:derived (Flag246-)(and (Flag154-)))

(:derived (Flag246-)(and (Flag48-)))

(:derived (Flag246-)(and (Flag198-)))

(:derived (Flag246-)(and (Flag104-)))

(:derived (Flag246-)(and (Flag199-)))

(:derived (Flag246-)(and (Flag222-)))

(:derived (Flag246-)(and (Flag56-)))

(:derived (Flag246-)(and (Flag234-)))

(:derived (Flag246-)(and (Flag85-)))

(:derived (Flag246-)(and (Flag244-)))

(:derived (Flag246-)(and (Flag170-)))

(:derived (Flag246-)(and (Flag171-)))

(:derived (Flag246-)(and (Flag212-)))

(:derived (Flag246-)(and (Flag67-)))

(:derived (Flag246-)(and (Flag200-)))

(:derived (Flag246-)(and (Flag111-)))

(:derived (Flag246-)(and (Flag77-)))

(:derived (Flag246-)(and (Flag95-)))

(:derived (Flag246-)(and (Flag187-)))

(:derived (Flag246-)(and (Flag223-)))

(:derived (Flag246-)(and (Flag113-)))

(:derived (Flag246-)(and (Flag235-)))

(:derived (Flag246-)(and (Flag71-)))

(:derived (Flag246-)(and (Flag213-)))

(:derived (Flag246-)(and (Flag224-)))

(:derived (Flag246-)(and (Flag245-)))

(:derived (Flag246-)(and (Flag201-)))

(:derived (Flag254-)(and (Flag43-)))

(:derived (Flag254-)(and (Flag49-)))

(:derived (Flag254-)(and (Flag247-)))

(:derived (Flag254-)(and (Flag215-)))

(:derived (Flag254-)(and (Flag237-)))

(:derived (Flag254-)(and (Flag216-)))

(:derived (Flag254-)(and (Flag141-)))

(:derived (Flag254-)(and (Flag204-)))

(:derived (Flag254-)(and (Flag158-)))

(:derived (Flag254-)(and (Flag227-)))

(:derived (Flag254-)(and (Flag125-)))

(:derived (Flag254-)(and (Flag91-)))

(:derived (Flag254-)(and (Flag96-)))

(:derived (Flag254-)(and (Flag238-)))

(:derived (Flag254-)(and (Flag144-)))

(:derived (Flag254-)(and (Flag229-)))

(:derived (Flag254-)(and (Flag239-)))

(:derived (Flag254-)(and (Flag44-)))

(:derived (Flag254-)(and (Flag103-)))

(:derived (Flag254-)(and (Flag240-)))

(:derived (Flag254-)(and (Flag175-)))

(:derived (Flag254-)(and (Flag128-)))

(:derived (Flag254-)(and (Flag161-)))

(:derived (Flag254-)(and (Flag108-)))

(:derived (Flag254-)(and (Flag60-)))

(:derived (Flag254-)(and (Flag176-)))

(:derived (Flag254-)(and (Flag129-)))

(:derived (Flag254-)(and (Flag218-)))

(:derived (Flag254-)(and (Flag147-)))

(:derived (Flag254-)(and (Flag248-)))

(:derived (Flag254-)(and (Flag193-)))

(:derived (Flag254-)(and (Flag189-)))

(:derived (Flag254-)(and (Flag219-)))

(:derived (Flag254-)(and (Flag178-)))

(:derived (Flag254-)(and (Flag230-)))

(:derived (Flag254-)(and (Flag94-)))

(:derived (Flag254-)(and (Flag107-)))

(:derived (Flag254-)(and (Flag205-)))

(:derived (Flag254-)(and (Flag136-)))

(:derived (Flag254-)(and (Flag206-)))

(:derived (Flag254-)(and (Flag249-)))

(:derived (Flag254-)(and (Flag117-)))

(:derived (Flag254-)(and (Flag76-)))

(:derived (Flag254-)(and (Flag150-)))

(:derived (Flag254-)(and (Flag97-)))

(:derived (Flag254-)(and (Flag139-)))

(:derived (Flag254-)(and (Flag165-)))

(:derived (Flag254-)(and (Flag220-)))

(:derived (Flag254-)(and (Flag122-)))

(:derived (Flag254-)(and (Flag78-)))

(:derived (Flag254-)(and (Flag207-)))

(:derived (Flag254-)(and (Flag83-)))

(:derived (Flag254-)(and (Flag195-)))

(:derived (Flag254-)(and (Flag124-)))

(:derived (Flag254-)(and (Flag208-)))

(:derived (Flag254-)(and (Flag181-)))

(:derived (Flag254-)(and (Flag232-)))

(:derived (Flag254-)(and (Flag102-)))

(:derived (Flag254-)(and (Flag182-)))

(:derived (Flag254-)(and (Flag137-)))

(:derived (Flag254-)(and (Flag168-)))

(:derived (Flag254-)(and (Flag171-)))

(:derived (Flag254-)(and (Flag233-)))

(:derived (Flag254-)(and (Flag250-)))

(:derived (Flag254-)(and (Flag251-)))

(:derived (Flag254-)(and (Flag68-)))

(:derived (Flag254-)(and (Flag197-)))

(:derived (Flag254-)(and (Flag252-)))

(:derived (Flag254-)(and (Flag69-)))

(:derived (Flag254-)(and (Flag70-)))

(:derived (Flag254-)(and (Flag152-)))

(:derived (Flag254-)(and (Flag169-)))

(:derived (Flag254-)(and (Flag185-)))

(:derived (Flag254-)(and (Flag243-)))

(:derived (Flag254-)(and (Flag101-)))

(:derived (Flag254-)(and (Flag154-)))

(:derived (Flag254-)(and (Flag48-)))

(:derived (Flag254-)(and (Flag198-)))

(:derived (Flag254-)(and (Flag104-)))

(:derived (Flag254-)(and (Flag199-)))

(:derived (Flag254-)(and (Flag222-)))

(:derived (Flag254-)(and (Flag56-)))

(:derived (Flag254-)(and (Flag253-)))

(:derived (Flag254-)(and (Flag234-)))

(:derived (Flag254-)(and (Flag85-)))

(:derived (Flag254-)(and (Flag170-)))

(:derived (Flag254-)(and (Flag241-)))

(:derived (Flag254-)(and (Flag212-)))

(:derived (Flag254-)(and (Flag67-)))

(:derived (Flag254-)(and (Flag200-)))

(:derived (Flag254-)(and (Flag77-)))

(:derived (Flag254-)(and (Flag151-)))

(:derived (Flag254-)(and (Flag187-)))

(:derived (Flag254-)(and (Flag223-)))

(:derived (Flag254-)(and (Flag113-)))

(:derived (Flag254-)(and (Flag235-)))

(:derived (Flag254-)(and (Flag71-)))

(:derived (Flag254-)(and (Flag213-)))

(:derived (Flag254-)(and (Flag224-)))

(:derived (Flag254-)(and (Flag245-)))

(:derived (Flag254-)(and (Flag201-)))

(:derived (Flag262-)(and (Flag43-)))

(:derived (Flag262-)(and (Flag49-)))

(:derived (Flag262-)(and (Flag247-)))

(:derived (Flag262-)(and (Flag215-)))

(:derived (Flag262-)(and (Flag255-)))

(:derived (Flag262-)(and (Flag237-)))

(:derived (Flag262-)(and (Flag216-)))

(:derived (Flag262-)(and (Flag141-)))

(:derived (Flag262-)(and (Flag204-)))

(:derived (Flag262-)(and (Flag227-)))

(:derived (Flag262-)(and (Flag256-)))

(:derived (Flag262-)(and (Flag125-)))

(:derived (Flag262-)(and (Flag176-)))

(:derived (Flag262-)(and (Flag205-)))

(:derived (Flag262-)(and (Flag257-)))

(:derived (Flag262-)(and (Flag96-)))

(:derived (Flag262-)(and (Flag144-)))

(:derived (Flag262-)(and (Flag229-)))

(:derived (Flag262-)(and (Flag239-)))

(:derived (Flag262-)(and (Flag258-)))

(:derived (Flag262-)(and (Flag240-)))

(:derived (Flag262-)(and (Flag234-)))

(:derived (Flag262-)(and (Flag161-)))

(:derived (Flag262-)(and (Flag108-)))

(:derived (Flag262-)(and (Flag60-)))

(:derived (Flag262-)(and (Flag91-)))

(:derived (Flag262-)(and (Flag129-)))

(:derived (Flag262-)(and (Flag218-)))

(:derived (Flag262-)(and (Flag147-)))

(:derived (Flag262-)(and (Flag193-)))

(:derived (Flag262-)(and (Flag178-)))

(:derived (Flag262-)(and (Flag230-)))

(:derived (Flag262-)(and (Flag94-)))

(:derived (Flag262-)(and (Flag77-)))

(:derived (Flag262-)(and (Flag241-)))

(:derived (Flag262-)(and (Flag136-)))

(:derived (Flag262-)(and (Flag206-)))

(:derived (Flag262-)(and (Flag249-)))

(:derived (Flag262-)(and (Flag76-)))

(:derived (Flag262-)(and (Flag150-)))

(:derived (Flag262-)(and (Flag97-)))

(:derived (Flag262-)(and (Flag139-)))

(:derived (Flag262-)(and (Flag165-)))

(:derived (Flag262-)(and (Flag220-)))

(:derived (Flag262-)(and (Flag122-)))

(:derived (Flag262-)(and (Flag78-)))

(:derived (Flag262-)(and (Flag83-)))

(:derived (Flag262-)(and (Flag195-)))

(:derived (Flag262-)(and (Flag124-)))

(:derived (Flag262-)(and (Flag181-)))

(:derived (Flag262-)(and (Flag232-)))

(:derived (Flag262-)(and (Flag102-)))

(:derived (Flag262-)(and (Flag137-)))

(:derived (Flag262-)(and (Flag168-)))

(:derived (Flag262-)(and (Flag107-)))

(:derived (Flag262-)(and (Flag233-)))

(:derived (Flag262-)(and (Flag250-)))

(:derived (Flag262-)(and (Flag251-)))

(:derived (Flag262-)(and (Flag68-)))

(:derived (Flag262-)(and (Flag197-)))

(:derived (Flag262-)(and (Flag252-)))

(:derived (Flag262-)(and (Flag69-)))

(:derived (Flag262-)(and (Flag169-)))

(:derived (Flag262-)(and (Flag185-)))

(:derived (Flag262-)(and (Flag243-)))

(:derived (Flag262-)(and (Flag101-)))

(:derived (Flag262-)(and (Flag259-)))

(:derived (Flag262-)(and (Flag154-)))

(:derived (Flag262-)(and (Flag48-)))

(:derived (Flag262-)(and (Flag198-)))

(:derived (Flag262-)(and (Flag104-)))

(:derived (Flag262-)(and (Flag199-)))

(:derived (Flag262-)(and (Flag222-)))

(:derived (Flag262-)(and (Flag56-)))

(:derived (Flag262-)(and (Flag253-)))

(:derived (Flag262-)(and (Flag85-)))

(:derived (Flag262-)(and (Flag170-)))

(:derived (Flag262-)(and (Flag171-)))

(:derived (Flag262-)(and (Flag212-)))

(:derived (Flag262-)(and (Flag67-)))

(:derived (Flag262-)(and (Flag200-)))

(:derived (Flag262-)(and (Flag260-)))

(:derived (Flag262-)(and (Flag151-)))

(:derived (Flag262-)(and (Flag187-)))

(:derived (Flag262-)(and (Flag223-)))

(:derived (Flag262-)(and (Flag113-)))

(:derived (Flag262-)(and (Flag71-)))

(:derived (Flag262-)(and (Flag213-)))

(:derived (Flag262-)(and (Flag224-)))

(:derived (Flag262-)(and (Flag245-)))

(:derived (Flag262-)(and (Flag201-)))

(:derived (Flag262-)(and (Flag261-)))

(:derived (Flag269-)(and (Flag71-)))

(:derived (Flag269-)(and (Flag101-)))

(:derived (Flag269-)(and (Flag259-)))

(:derived (Flag269-)(and (Flag139-)))

(:derived (Flag269-)(and (Flag154-)))

(:derived (Flag269-)(and (Flag43-)))

(:derived (Flag269-)(and (Flag255-)))

(:derived (Flag269-)(and (Flag48-)))

(:derived (Flag269-)(and (Flag263-)))

(:derived (Flag269-)(and (Flag122-)))

(:derived (Flag269-)(and (Flag215-)))

(:derived (Flag269-)(and (Flag195-)))

(:derived (Flag269-)(and (Flag264-)))

(:derived (Flag269-)(and (Flag124-)))

(:derived (Flag269-)(and (Flag161-)))

(:derived (Flag269-)(and (Flag265-)))

(:derived (Flag269-)(and (Flag258-)))

(:derived (Flag269-)(and (Flag104-)))

(:derived (Flag269-)(and (Flag266-)))

(:derived (Flag269-)(and (Flag240-)))

(:derived (Flag269-)(and (Flag234-)))

(:derived (Flag269-)(and (Flag216-)))

(:derived (Flag269-)(and (Flag125-)))

(:derived (Flag269-)(and (Flag232-)))

(:derived (Flag269-)(and (Flag222-)))

(:derived (Flag269-)(and (Flag96-)))

(:derived (Flag269-)(and (Flag102-)))

(:derived (Flag269-)(and (Flag141-)))

(:derived (Flag269-)(and (Flag168-)))

(:derived (Flag269-)(and (Flag204-)))

(:derived (Flag269-)(and (Flag239-)))

(:derived (Flag269-)(and (Flag56-)))

(:derived (Flag269-)(and (Flag151-)))

(:derived (Flag269-)(and (Flag165-)))

(:derived (Flag269-)(and (Flag49-)))

(:derived (Flag269-)(and (Flag60-)))

(:derived (Flag269-)(and (Flag91-)))

(:derived (Flag269-)(and (Flag233-)))

(:derived (Flag269-)(and (Flag230-)))

(:derived (Flag269-)(and (Flag253-)))

(:derived (Flag269-)(and (Flag147-)))

(:derived (Flag269-)(and (Flag256-)))

(:derived (Flag269-)(and (Flag85-)))

(:derived (Flag269-)(and (Flag136-)))

(:derived (Flag269-)(and (Flag181-)))

(:derived (Flag269-)(and (Flag199-)))

(:derived (Flag269-)(and (Flag129-)))

(:derived (Flag269-)(and (Flag170-)))

(:derived (Flag269-)(and (Flag169-)))

(:derived (Flag269-)(and (Flag205-)))

(:derived (Flag269-)(and (Flag250-)))

(:derived (Flag269-)(and (Flag77-)))

(:derived (Flag269-)(and (Flag251-)))

(:derived (Flag269-)(and (Flag176-)))

(:derived (Flag269-)(and (Flag78-)))

(:derived (Flag269-)(and (Flag68-)))

(:derived (Flag269-)(and (Flag200-)))

(:derived (Flag269-)(and (Flag260-)))

(:derived (Flag269-)(and (Flag94-)))

(:derived (Flag269-)(and (Flag107-)))

(:derived (Flag269-)(and (Flag187-)))

(:derived (Flag269-)(and (Flag223-)))

(:derived (Flag269-)(and (Flag113-)))

(:derived (Flag269-)(and (Flag267-)))

(:derived (Flag269-)(and (Flag212-)))

(:derived (Flag269-)(and (Flag197-)))

(:derived (Flag269-)(and (Flag206-)))

(:derived (Flag269-)(and (Flag252-)))

(:derived (Flag269-)(and (Flag69-)))

(:derived (Flag269-)(and (Flag249-)))

(:derived (Flag269-)(and (Flag213-)))

(:derived (Flag269-)(and (Flag237-)))

(:derived (Flag269-)(and (Flag224-)))

(:derived (Flag269-)(and (Flag198-)))

(:derived (Flag269-)(and (Flag245-)))

(:derived (Flag269-)(and (Flag178-)))

(:derived (Flag269-)(and (Flag185-)))

(:derived (Flag269-)(and (Flag229-)))

(:derived (Flag269-)(and (Flag150-)))

(:derived (Flag269-)(and (Flag97-)))

(:derived (Flag269-)(and (Flag268-)))

(:derived (Flag269-)(and (Flag243-)))

(:derived (Flag274-)(and (Flag101-)))

(:derived (Flag274-)(and (Flag43-)))

(:derived (Flag274-)(and (Flag139-)))

(:derived (Flag274-)(and (Flag154-)))

(:derived (Flag274-)(and (Flag78-)))

(:derived (Flag274-)(and (Flag48-)))

(:derived (Flag274-)(and (Flag263-)))

(:derived (Flag274-)(and (Flag259-)))

(:derived (Flag274-)(and (Flag264-)))

(:derived (Flag274-)(and (Flag124-)))

(:derived (Flag274-)(and (Flag161-)))

(:derived (Flag274-)(and (Flag265-)))

(:derived (Flag274-)(and (Flag258-)))

(:derived (Flag274-)(and (Flag104-)))

(:derived (Flag274-)(and (Flag237-)))

(:derived (Flag274-)(and (Flag230-)))

(:derived (Flag274-)(and (Flag216-)))

(:derived (Flag274-)(and (Flag125-)))

(:derived (Flag274-)(and (Flag232-)))

(:derived (Flag274-)(and (Flag222-)))

(:derived (Flag274-)(and (Flag96-)))

(:derived (Flag274-)(and (Flag102-)))

(:derived (Flag274-)(and (Flag270-)))

(:derived (Flag274-)(and (Flag168-)))

(:derived (Flag274-)(and (Flag204-)))

(:derived (Flag274-)(and (Flag239-)))

(:derived (Flag274-)(and (Flag56-)))

(:derived (Flag274-)(and (Flag60-)))

(:derived (Flag274-)(and (Flag176-)))

(:derived (Flag274-)(and (Flag233-)))

(:derived (Flag274-)(and (Flag253-)))

(:derived (Flag274-)(and (Flag147-)))

(:derived (Flag274-)(and (Flag256-)))

(:derived (Flag274-)(and (Flag85-)))

(:derived (Flag274-)(and (Flag136-)))

(:derived (Flag274-)(and (Flag181-)))

(:derived (Flag274-)(and (Flag199-)))

(:derived (Flag274-)(and (Flag129-)))

(:derived (Flag274-)(and (Flag170-)))

(:derived (Flag274-)(and (Flag271-)))

(:derived (Flag274-)(and (Flag205-)))

(:derived (Flag274-)(and (Flag250-)))

(:derived (Flag274-)(and (Flag77-)))

(:derived (Flag274-)(and (Flag251-)))

(:derived (Flag274-)(and (Flag255-)))

(:derived (Flag274-)(and (Flag272-)))

(:derived (Flag274-)(and (Flag68-)))

(:derived (Flag274-)(and (Flag200-)))

(:derived (Flag274-)(and (Flag260-)))

(:derived (Flag274-)(and (Flag94-)))

(:derived (Flag274-)(and (Flag151-)))

(:derived (Flag274-)(and (Flag187-)))

(:derived (Flag274-)(and (Flag223-)))

(:derived (Flag274-)(and (Flag113-)))

(:derived (Flag274-)(and (Flag267-)))

(:derived (Flag274-)(and (Flag212-)))

(:derived (Flag274-)(and (Flag197-)))

(:derived (Flag274-)(and (Flag206-)))

(:derived (Flag274-)(and (Flag69-)))

(:derived (Flag274-)(and (Flag249-)))

(:derived (Flag274-)(and (Flag224-)))

(:derived (Flag274-)(and (Flag198-)))

(:derived (Flag274-)(and (Flag245-)))

(:derived (Flag274-)(and (Flag273-)))

(:derived (Flag274-)(and (Flag169-)))

(:derived (Flag274-)(and (Flag185-)))

(:derived (Flag274-)(and (Flag150-)))

(:derived (Flag274-)(and (Flag97-)))

(:derived (Flag274-)(and (Flag229-)))

(:derived (Flag274-)(and (Flag243-)))

(:derived (Flag278-)(and (Flag101-)))

(:derived (Flag278-)(and (Flag259-)))

(:derived (Flag278-)(and (Flag139-)))

(:derived (Flag278-)(and (Flag78-)))

(:derived (Flag278-)(and (Flag48-)))

(:derived (Flag278-)(and (Flag263-)))

(:derived (Flag278-)(and (Flag43-)))

(:derived (Flag278-)(and (Flag264-)))

(:derived (Flag278-)(and (Flag124-)))

(:derived (Flag278-)(and (Flag265-)))

(:derived (Flag278-)(and (Flag258-)))

(:derived (Flag278-)(and (Flag104-)))

(:derived (Flag278-)(and (Flag237-)))

(:derived (Flag278-)(and (Flag230-)))

(:derived (Flag278-)(and (Flag199-)))

(:derived (Flag278-)(and (Flag161-)))

(:derived (Flag278-)(and (Flag222-)))

(:derived (Flag278-)(and (Flag275-)))

(:derived (Flag278-)(and (Flag181-)))

(:derived (Flag278-)(and (Flag270-)))

(:derived (Flag278-)(and (Flag168-)))

(:derived (Flag278-)(and (Flag204-)))

(:derived (Flag278-)(and (Flag239-)))

(:derived (Flag278-)(and (Flag60-)))

(:derived (Flag278-)(and (Flag233-)))

(:derived (Flag278-)(and (Flag253-)))

(:derived (Flag278-)(and (Flag147-)))

(:derived (Flag278-)(and (Flag256-)))

(:derived (Flag278-)(and (Flag125-)))

(:derived (Flag278-)(and (Flag205-)))

(:derived (Flag278-)(and (Flag136-)))

(:derived (Flag278-)(and (Flag251-)))

(:derived (Flag278-)(and (Flag272-)))

(:derived (Flag278-)(and (Flag68-)))

(:derived (Flag278-)(and (Flag200-)))

(:derived (Flag278-)(and (Flag260-)))

(:derived (Flag278-)(and (Flag94-)))

(:derived (Flag278-)(and (Flag151-)))

(:derived (Flag278-)(and (Flag187-)))

(:derived (Flag278-)(and (Flag223-)))

(:derived (Flag278-)(and (Flag113-)))

(:derived (Flag278-)(and (Flag96-)))

(:derived (Flag278-)(and (Flag197-)))

(:derived (Flag278-)(and (Flag206-)))

(:derived (Flag278-)(and (Flag69-)))

(:derived (Flag278-)(and (Flag249-)))

(:derived (Flag278-)(and (Flag224-)))

(:derived (Flag278-)(and (Flag276-)))

(:derived (Flag278-)(and (Flag277-)))

(:derived (Flag278-)(and (Flag273-)))

(:derived (Flag278-)(and (Flag169-)))

(:derived (Flag278-)(and (Flag185-)))

(:derived (Flag278-)(and (Flag150-)))

(:derived (Flag278-)(and (Flag97-)))

(:derived (Flag278-)(and (Flag229-)))

(:derived (Flag278-)(and (Flag243-)))

(:derived (Flag281-)(and (Flag259-)))

(:derived (Flag281-)(and (Flag78-)))

(:derived (Flag281-)(and (Flag48-)))

(:derived (Flag281-)(and (Flag239-)))

(:derived (Flag281-)(and (Flag43-)))

(:derived (Flag281-)(and (Flag264-)))

(:derived (Flag281-)(and (Flag124-)))

(:derived (Flag281-)(and (Flag265-)))

(:derived (Flag281-)(and (Flag258-)))

(:derived (Flag281-)(and (Flag260-)))

(:derived (Flag281-)(and (Flag199-)))

(:derived (Flag281-)(and (Flag161-)))

(:derived (Flag281-)(and (Flag222-)))

(:derived (Flag281-)(and (Flag275-)))

(:derived (Flag281-)(and (Flag181-)))

(:derived (Flag281-)(and (Flag279-)))

(:derived (Flag281-)(and (Flag204-)))

(:derived (Flag281-)(and (Flag253-)))

(:derived (Flag281-)(and (Flag125-)))

(:derived (Flag281-)(and (Flag280-)))

(:derived (Flag281-)(and (Flag205-)))

(:derived (Flag281-)(and (Flag136-)))

(:derived (Flag281-)(and (Flag272-)))

(:derived (Flag281-)(and (Flag230-)))

(:derived (Flag281-)(and (Flag94-)))

(:derived (Flag281-)(and (Flag151-)))

(:derived (Flag281-)(and (Flag187-)))

(:derived (Flag281-)(and (Flag223-)))

(:derived (Flag281-)(and (Flag113-)))

(:derived (Flag281-)(and (Flag197-)))

(:derived (Flag281-)(and (Flag69-)))

(:derived (Flag281-)(and (Flag277-)))

(:derived (Flag281-)(and (Flag273-)))

(:derived (Flag281-)(and (Flag169-)))

(:derived (Flag281-)(and (Flag150-)))

(:derived (Flag281-)(and (Flag97-)))

(:derived (Flag281-)(and (Flag229-)))

(:derived (Flag281-)(and (Flag243-)))

(:derived (Flag283-)(and (Flag282-)))

(:derived (Flag283-)(and (Flag199-)))

(:derived (Flag283-)(and (Flag259-)))

(:derived (Flag283-)(and (Flag223-)))

(:derived (Flag283-)(and (Flag113-)))

(:derived (Flag283-)(and (Flag273-)))

(:derived (Flag283-)(and (Flag69-)))

(:derived (Flag283-)(and (Flag230-)))

(:derived (Flag283-)(and (Flag204-)))

(:derived (Flag283-)(and (Flag136-)))

(:derived (Flag283-)(and (Flag279-)))

(:derived (Flag283-)(and (Flag277-)))

(:derived (Flag283-)(and (Flag78-)))

(:derived (Flag283-)(and (Flag169-)))

(:derived (Flag283-)(and (Flag265-)))

(:derived (Flag283-)(and (Flag253-)))

(:derived (Flag283-)(and (Flag97-)))

(:derived (Flag283-)(and (Flag151-)))

(:derived (Flag283-)(and (Flag187-)))

(:derived (Flag283-)(and (Flag239-)))

(:derived (Flag566-)(and (Flag61-)))

(:derived (Flag566-)(and (Flag49-)))

(:derived (Flag566-)(and (Flag64-)))

(:derived (Flag566-)(and (Flag69-)))

(:derived (Flag566-)(and (Flag70-)))

(:derived (Flag566-)(and (Flag54-)))

(:derived (Flag566-)(and (Flag47-)))

(:derived (Flag566-)(and (Flag48-)))

(:derived (Flag566-)(and (Flag68-)))

(:derived (Flag566-)(and (Flag46-)))

(:derived (Flag566-)(and (Flag565-)))

(:derived (Flag566-)(and (Flag66-)))

(:derived (Flag566-)(and (Flag74-)))

(:derived (Flag566-)(and (Flag58-)))

(:derived (Flag566-)(and (Flag65-)))

(:derived (Flag566-)(and (Flag75-)))

(:derived (Flag566-)(and (Flag76-)))

(:derived (Flag566-)(and (Flag50-)))

(:derived (Flag566-)(and (Flag77-)))

(:derived (Flag566-)(and (Flag63-)))

(:derived (Flag1-)(and (COLUMN2-ROBOT)(ROW1-ROBOT)(NOT-ERROR-)(NOT-CHECKCONSISTENCY-)))

(:derived (Flag3-)(and (LEFTOF3-ROBOT)))

(:derived (Flag3-)(and (RIGHTOF7-ROBOT)))

(:derived (Flag3-)(and (RIGHTOF8-ROBOT)))

(:derived (Flag3-)(and (RIGHTOF15-ROBOT)))

(:derived (Flag3-)(and (LEFTOF2-ROBOT)))

(:derived (Flag3-)(and (LEFTOF9-ROBOT)))

(:derived (Flag3-)(and (RIGHTOF10-ROBOT)))

(:derived (Flag3-)(and (LEFTOF8-ROBOT)))

(:derived (Flag3-)(and (RIGHTOF12-ROBOT)))

(:derived (Flag3-)(and (LEFTOF14-ROBOT)))

(:derived (Flag3-)(and (LEFTOF5-ROBOT)))

(:derived (Flag3-)(and (RIGHTOF1-ROBOT)))

(:derived (Flag3-)(and (LEFTOF12-ROBOT)))

(:derived (Flag3-)(and (RIGHTOF13-ROBOT)))

(:derived (Flag3-)(and (RIGHTOF14-ROBOT)))

(:derived (Flag3-)(and (LEFTOF1-ROBOT)))

(:derived (Flag3-)(and (LEFTOF20-ROBOT)))

(:derived (Flag3-)(and (RIGHTOF6-ROBOT)))

(:derived (Flag3-)(and (RIGHTOF9-ROBOT)))

(:derived (Flag3-)(and (LEFTOF6-ROBOT)))

(:derived (Flag3-)(and (RIGHTOF11-ROBOT)))

(:derived (Flag3-)(and (RIGHTOF17-ROBOT)))

(:derived (Flag3-)(and (LEFTOF11-ROBOT)))

(:derived (Flag3-)(and (RIGHTOF19-ROBOT)))

(:derived (Flag3-)(and (RIGHTOF5-ROBOT)))

(:derived (Flag3-)(and (LEFTOF10-ROBOT)))

(:derived (Flag3-)(and (LEFTOF15-ROBOT)))

(:derived (Flag3-)(and (LEFTOF4-ROBOT)))

(:derived (Flag3-)(and (RIGHTOF18-ROBOT)))

(:derived (Flag3-)(and (LEFTOF16-ROBOT)))

(:derived (Flag3-)(and (RIGHTOF16-ROBOT)))

(:derived (Flag3-)(and (RIGHTOF4-ROBOT)))

(:derived (Flag3-)(and (RIGHTOF0-ROBOT)))

(:derived (Flag3-)(and (LEFTOF7-ROBOT)))

(:derived (Flag3-)(and (LEFTOF13-ROBOT)))

(:derived (Flag3-)(and (LEFTOF17-ROBOT)))

(:derived (Flag3-)(and (LEFTOF18-ROBOT)))

(:derived (Flag3-)(and (RIGHTOF3-ROBOT)))

(:derived (Flag5-)(and (LEFTOF1-ROBOT)))

(:derived (Flag5-)(and (LEFTOF2-ROBOT)))

(:derived (Flag6-)(and (LEFTOF1-ROBOT)))

(:derived (Flag6-)(and (LEFTOF3-ROBOT)))

(:derived (Flag6-)(and (LEFTOF2-ROBOT)))

(:derived (Flag7-)(and (LEFTOF1-ROBOT)))

(:derived (Flag7-)(and (LEFTOF3-ROBOT)))

(:derived (Flag7-)(and (LEFTOF4-ROBOT)))

(:derived (Flag7-)(and (LEFTOF2-ROBOT)))

(:derived (Flag8-)(and (LEFTOF5-ROBOT)))

(:derived (Flag8-)(and (LEFTOF1-ROBOT)))

(:derived (Flag8-)(and (LEFTOF3-ROBOT)))

(:derived (Flag8-)(and (LEFTOF4-ROBOT)))

(:derived (Flag8-)(and (LEFTOF2-ROBOT)))

(:derived (Flag9-)(and (LEFTOF2-ROBOT)))

(:derived (Flag9-)(and (LEFTOF1-ROBOT)))

(:derived (Flag9-)(and (LEFTOF6-ROBOT)))

(:derived (Flag9-)(and (LEFTOF5-ROBOT)))

(:derived (Flag9-)(and (LEFTOF3-ROBOT)))

(:derived (Flag9-)(and (LEFTOF4-ROBOT)))

(:derived (Flag10-)(and (LEFTOF2-ROBOT)))

(:derived (Flag10-)(and (LEFTOF7-ROBOT)))

(:derived (Flag10-)(and (LEFTOF1-ROBOT)))

(:derived (Flag10-)(and (LEFTOF6-ROBOT)))

(:derived (Flag10-)(and (LEFTOF5-ROBOT)))

(:derived (Flag10-)(and (LEFTOF3-ROBOT)))

(:derived (Flag10-)(and (LEFTOF4-ROBOT)))

(:derived (Flag11-)(and (LEFTOF2-ROBOT)))

(:derived (Flag11-)(and (LEFTOF7-ROBOT)))

(:derived (Flag11-)(and (LEFTOF1-ROBOT)))

(:derived (Flag11-)(and (LEFTOF6-ROBOT)))

(:derived (Flag11-)(and (LEFTOF5-ROBOT)))

(:derived (Flag11-)(and (LEFTOF3-ROBOT)))

(:derived (Flag11-)(and (LEFTOF4-ROBOT)))

(:derived (Flag11-)(and (LEFTOF8-ROBOT)))

(:derived (Flag12-)(and (LEFTOF9-ROBOT)))

(:derived (Flag12-)(and (LEFTOF2-ROBOT)))

(:derived (Flag12-)(and (LEFTOF7-ROBOT)))

(:derived (Flag12-)(and (LEFTOF1-ROBOT)))

(:derived (Flag12-)(and (LEFTOF6-ROBOT)))

(:derived (Flag12-)(and (LEFTOF5-ROBOT)))

(:derived (Flag12-)(and (LEFTOF3-ROBOT)))

(:derived (Flag12-)(and (LEFTOF4-ROBOT)))

(:derived (Flag12-)(and (LEFTOF8-ROBOT)))

(:derived (Flag13-)(and (LEFTOF10-ROBOT)))

(:derived (Flag13-)(and (LEFTOF9-ROBOT)))

(:derived (Flag13-)(and (LEFTOF2-ROBOT)))

(:derived (Flag13-)(and (LEFTOF7-ROBOT)))

(:derived (Flag13-)(and (LEFTOF1-ROBOT)))

(:derived (Flag13-)(and (LEFTOF6-ROBOT)))

(:derived (Flag13-)(and (LEFTOF5-ROBOT)))

(:derived (Flag13-)(and (LEFTOF3-ROBOT)))

(:derived (Flag13-)(and (LEFTOF4-ROBOT)))

(:derived (Flag13-)(and (LEFTOF8-ROBOT)))

(:derived (Flag14-)(and (LEFTOF10-ROBOT)))

(:derived (Flag14-)(and (LEFTOF11-ROBOT)))

(:derived (Flag14-)(and (LEFTOF9-ROBOT)))

(:derived (Flag14-)(and (LEFTOF2-ROBOT)))

(:derived (Flag14-)(and (LEFTOF7-ROBOT)))

(:derived (Flag14-)(and (LEFTOF1-ROBOT)))

(:derived (Flag14-)(and (LEFTOF6-ROBOT)))

(:derived (Flag14-)(and (LEFTOF5-ROBOT)))

(:derived (Flag14-)(and (LEFTOF3-ROBOT)))

(:derived (Flag14-)(and (LEFTOF4-ROBOT)))

(:derived (Flag14-)(and (LEFTOF8-ROBOT)))

(:derived (Flag15-)(and (LEFTOF10-ROBOT)))

(:derived (Flag15-)(and (LEFTOF11-ROBOT)))

(:derived (Flag15-)(and (LEFTOF9-ROBOT)))

(:derived (Flag15-)(and (LEFTOF2-ROBOT)))

(:derived (Flag15-)(and (LEFTOF7-ROBOT)))

(:derived (Flag15-)(and (LEFTOF1-ROBOT)))

(:derived (Flag15-)(and (LEFTOF12-ROBOT)))

(:derived (Flag15-)(and (LEFTOF6-ROBOT)))

(:derived (Flag15-)(and (LEFTOF5-ROBOT)))

(:derived (Flag15-)(and (LEFTOF3-ROBOT)))

(:derived (Flag15-)(and (LEFTOF4-ROBOT)))

(:derived (Flag15-)(and (LEFTOF8-ROBOT)))

(:derived (Flag16-)(and (LEFTOF10-ROBOT)))

(:derived (Flag16-)(and (LEFTOF11-ROBOT)))

(:derived (Flag16-)(and (LEFTOF9-ROBOT)))

(:derived (Flag16-)(and (LEFTOF2-ROBOT)))

(:derived (Flag16-)(and (LEFTOF13-ROBOT)))

(:derived (Flag16-)(and (LEFTOF1-ROBOT)))

(:derived (Flag16-)(and (LEFTOF12-ROBOT)))

(:derived (Flag16-)(and (LEFTOF6-ROBOT)))

(:derived (Flag16-)(and (LEFTOF5-ROBOT)))

(:derived (Flag16-)(and (LEFTOF3-ROBOT)))

(:derived (Flag16-)(and (LEFTOF4-ROBOT)))

(:derived (Flag16-)(and (LEFTOF7-ROBOT)))

(:derived (Flag16-)(and (LEFTOF8-ROBOT)))

(:derived (Flag17-)(and (LEFTOF10-ROBOT)))

(:derived (Flag17-)(and (LEFTOF11-ROBOT)))

(:derived (Flag17-)(and (LEFTOF9-ROBOT)))

(:derived (Flag17-)(and (LEFTOF2-ROBOT)))

(:derived (Flag17-)(and (LEFTOF13-ROBOT)))

(:derived (Flag17-)(and (LEFTOF1-ROBOT)))

(:derived (Flag17-)(and (LEFTOF12-ROBOT)))

(:derived (Flag17-)(and (LEFTOF6-ROBOT)))

(:derived (Flag17-)(and (LEFTOF5-ROBOT)))

(:derived (Flag17-)(and (LEFTOF14-ROBOT)))

(:derived (Flag17-)(and (LEFTOF3-ROBOT)))

(:derived (Flag17-)(and (LEFTOF4-ROBOT)))

(:derived (Flag17-)(and (LEFTOF7-ROBOT)))

(:derived (Flag17-)(and (LEFTOF8-ROBOT)))

(:derived (Flag18-)(and (LEFTOF15-ROBOT)))

(:derived (Flag18-)(and (LEFTOF10-ROBOT)))

(:derived (Flag18-)(and (LEFTOF11-ROBOT)))

(:derived (Flag18-)(and (LEFTOF9-ROBOT)))

(:derived (Flag18-)(and (LEFTOF2-ROBOT)))

(:derived (Flag18-)(and (LEFTOF13-ROBOT)))

(:derived (Flag18-)(and (LEFTOF1-ROBOT)))

(:derived (Flag18-)(and (LEFTOF12-ROBOT)))

(:derived (Flag18-)(and (LEFTOF6-ROBOT)))

(:derived (Flag18-)(and (LEFTOF5-ROBOT)))

(:derived (Flag18-)(and (LEFTOF14-ROBOT)))

(:derived (Flag18-)(and (LEFTOF3-ROBOT)))

(:derived (Flag18-)(and (LEFTOF4-ROBOT)))

(:derived (Flag18-)(and (LEFTOF7-ROBOT)))

(:derived (Flag18-)(and (LEFTOF8-ROBOT)))

(:derived (Flag19-)(and (LEFTOF15-ROBOT)))

(:derived (Flag19-)(and (LEFTOF10-ROBOT)))

(:derived (Flag19-)(and (LEFTOF11-ROBOT)))

(:derived (Flag19-)(and (LEFTOF9-ROBOT)))

(:derived (Flag19-)(and (LEFTOF2-ROBOT)))

(:derived (Flag19-)(and (LEFTOF13-ROBOT)))

(:derived (Flag19-)(and (LEFTOF1-ROBOT)))

(:derived (Flag19-)(and (LEFTOF12-ROBOT)))

(:derived (Flag19-)(and (LEFTOF6-ROBOT)))

(:derived (Flag19-)(and (LEFTOF5-ROBOT)))

(:derived (Flag19-)(and (LEFTOF16-ROBOT)))

(:derived (Flag19-)(and (LEFTOF14-ROBOT)))

(:derived (Flag19-)(and (LEFTOF3-ROBOT)))

(:derived (Flag19-)(and (LEFTOF4-ROBOT)))

(:derived (Flag19-)(and (LEFTOF7-ROBOT)))

(:derived (Flag19-)(and (LEFTOF8-ROBOT)))

(:derived (Flag20-)(and (LEFTOF15-ROBOT)))

(:derived (Flag20-)(and (LEFTOF10-ROBOT)))

(:derived (Flag20-)(and (LEFTOF17-ROBOT)))

(:derived (Flag20-)(and (LEFTOF11-ROBOT)))

(:derived (Flag20-)(and (LEFTOF9-ROBOT)))

(:derived (Flag20-)(and (LEFTOF2-ROBOT)))

(:derived (Flag20-)(and (LEFTOF13-ROBOT)))

(:derived (Flag20-)(and (LEFTOF1-ROBOT)))

(:derived (Flag20-)(and (LEFTOF12-ROBOT)))

(:derived (Flag20-)(and (LEFTOF6-ROBOT)))

(:derived (Flag20-)(and (LEFTOF5-ROBOT)))

(:derived (Flag20-)(and (LEFTOF16-ROBOT)))

(:derived (Flag20-)(and (LEFTOF14-ROBOT)))

(:derived (Flag20-)(and (LEFTOF3-ROBOT)))

(:derived (Flag20-)(and (LEFTOF4-ROBOT)))

(:derived (Flag20-)(and (LEFTOF7-ROBOT)))

(:derived (Flag20-)(and (LEFTOF8-ROBOT)))

(:derived (Flag21-)(and (LEFTOF15-ROBOT)))

(:derived (Flag21-)(and (LEFTOF10-ROBOT)))

(:derived (Flag21-)(and (LEFTOF18-ROBOT)))

(:derived (Flag21-)(and (LEFTOF17-ROBOT)))

(:derived (Flag21-)(and (LEFTOF11-ROBOT)))

(:derived (Flag21-)(and (LEFTOF9-ROBOT)))

(:derived (Flag21-)(and (LEFTOF2-ROBOT)))

(:derived (Flag21-)(and (LEFTOF13-ROBOT)))

(:derived (Flag21-)(and (LEFTOF1-ROBOT)))

(:derived (Flag21-)(and (LEFTOF12-ROBOT)))

(:derived (Flag21-)(and (LEFTOF6-ROBOT)))

(:derived (Flag21-)(and (LEFTOF5-ROBOT)))

(:derived (Flag21-)(and (LEFTOF16-ROBOT)))

(:derived (Flag21-)(and (LEFTOF14-ROBOT)))

(:derived (Flag21-)(and (LEFTOF3-ROBOT)))

(:derived (Flag21-)(and (LEFTOF4-ROBOT)))

(:derived (Flag21-)(and (LEFTOF7-ROBOT)))

(:derived (Flag21-)(and (LEFTOF8-ROBOT)))

(:derived (Flag22-)(and (LEFTOF15-ROBOT)))

(:derived (Flag22-)(and (LEFTOF10-ROBOT)))

(:derived (Flag22-)(and (LEFTOF1-ROBOT)))

(:derived (Flag22-)(and (LEFTOF18-ROBOT)))

(:derived (Flag22-)(and (LEFTOF17-ROBOT)))

(:derived (Flag22-)(and (LEFTOF11-ROBOT)))

(:derived (Flag22-)(and (LEFTOF9-ROBOT)))

(:derived (Flag22-)(and (LEFTOF2-ROBOT)))

(:derived (Flag22-)(and (LEFTOF13-ROBOT)))

(:derived (Flag22-)(and (LEFTOF12-ROBOT)))

(:derived (Flag22-)(and (LEFTOF6-ROBOT)))

(:derived (Flag22-)(and (LEFTOF5-ROBOT)))

(:derived (Flag22-)(and (LEFTOF16-ROBOT)))

(:derived (Flag22-)(and (LEFTOF14-ROBOT)))

(:derived (Flag22-)(and (LEFTOF3-ROBOT)))

(:derived (Flag22-)(and (LEFTOF4-ROBOT)))

(:derived (Flag22-)(and (LEFTOF7-ROBOT)))

(:derived (Flag22-)(and (LEFTOF8-ROBOT)))

(:derived (Flag23-)(and (LEFTOF20-ROBOT)))

(:derived (Flag23-)(and (LEFTOF15-ROBOT)))

(:derived (Flag23-)(and (LEFTOF10-ROBOT)))

(:derived (Flag23-)(and (LEFTOF1-ROBOT)))

(:derived (Flag23-)(and (LEFTOF18-ROBOT)))

(:derived (Flag23-)(and (LEFTOF17-ROBOT)))

(:derived (Flag23-)(and (LEFTOF11-ROBOT)))

(:derived (Flag23-)(and (LEFTOF9-ROBOT)))

(:derived (Flag23-)(and (LEFTOF2-ROBOT)))

(:derived (Flag23-)(and (LEFTOF13-ROBOT)))

(:derived (Flag23-)(and (LEFTOF12-ROBOT)))

(:derived (Flag23-)(and (LEFTOF6-ROBOT)))

(:derived (Flag23-)(and (LEFTOF5-ROBOT)))

(:derived (Flag23-)(and (LEFTOF16-ROBOT)))

(:derived (Flag23-)(and (LEFTOF14-ROBOT)))

(:derived (Flag23-)(and (LEFTOF3-ROBOT)))

(:derived (Flag23-)(and (LEFTOF4-ROBOT)))

(:derived (Flag23-)(and (LEFTOF7-ROBOT)))

(:derived (Flag23-)(and (LEFTOF8-ROBOT)))

(:derived (Flag24-)(and (RIGHTOF3-ROBOT)))

(:derived (Flag24-)(and (RIGHTOF8-ROBOT)))

(:derived (Flag24-)(and (RIGHTOF5-ROBOT)))

(:derived (Flag24-)(and (RIGHTOF9-ROBOT)))

(:derived (Flag24-)(and (RIGHTOF19-ROBOT)))

(:derived (Flag24-)(and (RIGHTOF17-ROBOT)))

(:derived (Flag24-)(and (RIGHTOF14-ROBOT)))

(:derived (Flag24-)(and (RIGHTOF13-ROBOT)))

(:derived (Flag24-)(and (RIGHTOF12-ROBOT)))

(:derived (Flag24-)(and (RIGHTOF10-ROBOT)))

(:derived (Flag24-)(and (RIGHTOF4-ROBOT)))

(:derived (Flag24-)(and (RIGHTOF1-ROBOT)))

(:derived (Flag24-)(and (RIGHTOF16-ROBOT)))

(:derived (Flag24-)(and (RIGHTOF18-ROBOT)))

(:derived (Flag24-)(and (RIGHTOF7-ROBOT)))

(:derived (Flag24-)(and (RIGHTOF11-ROBOT)))

(:derived (Flag24-)(and (RIGHTOF15-ROBOT)))

(:derived (Flag24-)(and (RIGHTOF6-ROBOT)))

(:derived (Flag25-)(and (RIGHTOF3-ROBOT)))

(:derived (Flag25-)(and (RIGHTOF5-ROBOT)))

(:derived (Flag25-)(and (RIGHTOF9-ROBOT)))

(:derived (Flag25-)(and (RIGHTOF19-ROBOT)))

(:derived (Flag25-)(and (RIGHTOF17-ROBOT)))

(:derived (Flag25-)(and (RIGHTOF14-ROBOT)))

(:derived (Flag25-)(and (RIGHTOF13-ROBOT)))

(:derived (Flag25-)(and (RIGHTOF12-ROBOT)))

(:derived (Flag25-)(and (RIGHTOF10-ROBOT)))

(:derived (Flag25-)(and (RIGHTOF4-ROBOT)))

(:derived (Flag25-)(and (RIGHTOF8-ROBOT)))

(:derived (Flag25-)(and (RIGHTOF16-ROBOT)))

(:derived (Flag25-)(and (RIGHTOF18-ROBOT)))

(:derived (Flag25-)(and (RIGHTOF7-ROBOT)))

(:derived (Flag25-)(and (RIGHTOF11-ROBOT)))

(:derived (Flag25-)(and (RIGHTOF15-ROBOT)))

(:derived (Flag25-)(and (RIGHTOF6-ROBOT)))

(:derived (Flag26-)(and (RIGHTOF7-ROBOT)))

(:derived (Flag26-)(and (RIGHTOF3-ROBOT)))

(:derived (Flag26-)(and (RIGHTOF5-ROBOT)))

(:derived (Flag26-)(and (RIGHTOF9-ROBOT)))

(:derived (Flag26-)(and (RIGHTOF19-ROBOT)))

(:derived (Flag26-)(and (RIGHTOF17-ROBOT)))

(:derived (Flag26-)(and (RIGHTOF14-ROBOT)))

(:derived (Flag26-)(and (RIGHTOF13-ROBOT)))

(:derived (Flag26-)(and (RIGHTOF12-ROBOT)))

(:derived (Flag26-)(and (RIGHTOF10-ROBOT)))

(:derived (Flag26-)(and (RIGHTOF4-ROBOT)))

(:derived (Flag26-)(and (RIGHTOF8-ROBOT)))

(:derived (Flag26-)(and (RIGHTOF16-ROBOT)))

(:derived (Flag26-)(and (RIGHTOF18-ROBOT)))

(:derived (Flag26-)(and (RIGHTOF11-ROBOT)))

(:derived (Flag26-)(and (RIGHTOF15-ROBOT)))

(:derived (Flag26-)(and (RIGHTOF6-ROBOT)))

(:derived (Flag27-)(and (RIGHTOF7-ROBOT)))

(:derived (Flag27-)(and (RIGHTOF5-ROBOT)))

(:derived (Flag27-)(and (RIGHTOF9-ROBOT)))

(:derived (Flag27-)(and (RIGHTOF19-ROBOT)))

(:derived (Flag27-)(and (RIGHTOF17-ROBOT)))

(:derived (Flag27-)(and (RIGHTOF14-ROBOT)))

(:derived (Flag27-)(and (RIGHTOF13-ROBOT)))

(:derived (Flag27-)(and (RIGHTOF12-ROBOT)))

(:derived (Flag27-)(and (RIGHTOF10-ROBOT)))

(:derived (Flag27-)(and (RIGHTOF4-ROBOT)))

(:derived (Flag27-)(and (RIGHTOF8-ROBOT)))

(:derived (Flag27-)(and (RIGHTOF16-ROBOT)))

(:derived (Flag27-)(and (RIGHTOF18-ROBOT)))

(:derived (Flag27-)(and (RIGHTOF11-ROBOT)))

(:derived (Flag27-)(and (RIGHTOF15-ROBOT)))

(:derived (Flag27-)(and (RIGHTOF6-ROBOT)))

(:derived (Flag28-)(and (RIGHTOF7-ROBOT)))

(:derived (Flag28-)(and (RIGHTOF5-ROBOT)))

(:derived (Flag28-)(and (RIGHTOF9-ROBOT)))

(:derived (Flag28-)(and (RIGHTOF19-ROBOT)))

(:derived (Flag28-)(and (RIGHTOF17-ROBOT)))

(:derived (Flag28-)(and (RIGHTOF14-ROBOT)))

(:derived (Flag28-)(and (RIGHTOF13-ROBOT)))

(:derived (Flag28-)(and (RIGHTOF12-ROBOT)))

(:derived (Flag28-)(and (RIGHTOF10-ROBOT)))

(:derived (Flag28-)(and (RIGHTOF8-ROBOT)))

(:derived (Flag28-)(and (RIGHTOF16-ROBOT)))

(:derived (Flag28-)(and (RIGHTOF18-ROBOT)))

(:derived (Flag28-)(and (RIGHTOF11-ROBOT)))

(:derived (Flag28-)(and (RIGHTOF15-ROBOT)))

(:derived (Flag28-)(and (RIGHTOF6-ROBOT)))

(:derived (Flag29-)(and (RIGHTOF7-ROBOT)))

(:derived (Flag29-)(and (RIGHTOF9-ROBOT)))

(:derived (Flag29-)(and (RIGHTOF19-ROBOT)))

(:derived (Flag29-)(and (RIGHTOF17-ROBOT)))

(:derived (Flag29-)(and (RIGHTOF14-ROBOT)))

(:derived (Flag29-)(and (RIGHTOF13-ROBOT)))

(:derived (Flag29-)(and (RIGHTOF12-ROBOT)))

(:derived (Flag29-)(and (RIGHTOF10-ROBOT)))

(:derived (Flag29-)(and (RIGHTOF8-ROBOT)))

(:derived (Flag29-)(and (RIGHTOF16-ROBOT)))

(:derived (Flag29-)(and (RIGHTOF18-ROBOT)))

(:derived (Flag29-)(and (RIGHTOF11-ROBOT)))

(:derived (Flag29-)(and (RIGHTOF15-ROBOT)))

(:derived (Flag29-)(and (RIGHTOF6-ROBOT)))

(:derived (Flag30-)(and (RIGHTOF9-ROBOT)))

(:derived (Flag30-)(and (RIGHTOF19-ROBOT)))

(:derived (Flag30-)(and (RIGHTOF17-ROBOT)))

(:derived (Flag30-)(and (RIGHTOF14-ROBOT)))

(:derived (Flag30-)(and (RIGHTOF13-ROBOT)))

(:derived (Flag30-)(and (RIGHTOF15-ROBOT)))

(:derived (Flag30-)(and (RIGHTOF10-ROBOT)))

(:derived (Flag30-)(and (RIGHTOF8-ROBOT)))

(:derived (Flag30-)(and (RIGHTOF12-ROBOT)))

(:derived (Flag30-)(and (RIGHTOF16-ROBOT)))

(:derived (Flag30-)(and (RIGHTOF7-ROBOT)))

(:derived (Flag30-)(and (RIGHTOF18-ROBOT)))

(:derived (Flag30-)(and (RIGHTOF11-ROBOT)))

(:derived (Flag31-)(and (RIGHTOF9-ROBOT)))

(:derived (Flag31-)(and (RIGHTOF19-ROBOT)))

(:derived (Flag31-)(and (RIGHTOF17-ROBOT)))

(:derived (Flag31-)(and (RIGHTOF14-ROBOT)))

(:derived (Flag31-)(and (RIGHTOF13-ROBOT)))

(:derived (Flag31-)(and (RIGHTOF15-ROBOT)))

(:derived (Flag31-)(and (RIGHTOF10-ROBOT)))

(:derived (Flag31-)(and (RIGHTOF8-ROBOT)))

(:derived (Flag31-)(and (RIGHTOF16-ROBOT)))

(:derived (Flag31-)(and (RIGHTOF12-ROBOT)))

(:derived (Flag31-)(and (RIGHTOF18-ROBOT)))

(:derived (Flag31-)(and (RIGHTOF11-ROBOT)))

(:derived (Flag32-)(and (RIGHTOF19-ROBOT)))

(:derived (Flag32-)(and (RIGHTOF9-ROBOT)))

(:derived (Flag32-)(and (RIGHTOF17-ROBOT)))

(:derived (Flag32-)(and (RIGHTOF14-ROBOT)))

(:derived (Flag32-)(and (RIGHTOF13-ROBOT)))

(:derived (Flag32-)(and (RIGHTOF15-ROBOT)))

(:derived (Flag32-)(and (RIGHTOF10-ROBOT)))

(:derived (Flag32-)(and (RIGHTOF16-ROBOT)))

(:derived (Flag32-)(and (RIGHTOF12-ROBOT)))

(:derived (Flag32-)(and (RIGHTOF18-ROBOT)))

(:derived (Flag32-)(and (RIGHTOF11-ROBOT)))

(:derived (Flag33-)(and (RIGHTOF18-ROBOT)))

(:derived (Flag33-)(and (RIGHTOF17-ROBOT)))

(:derived (Flag33-)(and (RIGHTOF14-ROBOT)))

(:derived (Flag33-)(and (RIGHTOF13-ROBOT)))

(:derived (Flag33-)(and (RIGHTOF15-ROBOT)))

(:derived (Flag33-)(and (RIGHTOF10-ROBOT)))

(:derived (Flag33-)(and (RIGHTOF16-ROBOT)))

(:derived (Flag33-)(and (RIGHTOF12-ROBOT)))

(:derived (Flag33-)(and (RIGHTOF19-ROBOT)))

(:derived (Flag33-)(and (RIGHTOF11-ROBOT)))

(:derived (Flag34-)(and (RIGHTOF18-ROBOT)))

(:derived (Flag34-)(and (RIGHTOF17-ROBOT)))

(:derived (Flag34-)(and (RIGHTOF14-ROBOT)))

(:derived (Flag34-)(and (RIGHTOF13-ROBOT)))

(:derived (Flag34-)(and (RIGHTOF15-ROBOT)))

(:derived (Flag34-)(and (RIGHTOF16-ROBOT)))

(:derived (Flag34-)(and (RIGHTOF12-ROBOT)))

(:derived (Flag34-)(and (RIGHTOF19-ROBOT)))

(:derived (Flag34-)(and (RIGHTOF11-ROBOT)))

(:derived (Flag35-)(and (RIGHTOF19-ROBOT)))

(:derived (Flag35-)(and (RIGHTOF17-ROBOT)))

(:derived (Flag35-)(and (RIGHTOF14-ROBOT)))

(:derived (Flag35-)(and (RIGHTOF13-ROBOT)))

(:derived (Flag35-)(and (RIGHTOF12-ROBOT)))

(:derived (Flag35-)(and (RIGHTOF16-ROBOT)))

(:derived (Flag35-)(and (RIGHTOF18-ROBOT)))

(:derived (Flag35-)(and (RIGHTOF15-ROBOT)))

(:derived (Flag36-)(and (RIGHTOF19-ROBOT)))

(:derived (Flag36-)(and (RIGHTOF17-ROBOT)))

(:derived (Flag36-)(and (RIGHTOF14-ROBOT)))

(:derived (Flag36-)(and (RIGHTOF13-ROBOT)))

(:derived (Flag36-)(and (RIGHTOF15-ROBOT)))

(:derived (Flag36-)(and (RIGHTOF16-ROBOT)))

(:derived (Flag36-)(and (RIGHTOF18-ROBOT)))

(:derived (Flag37-)(and (RIGHTOF18-ROBOT)))

(:derived (Flag37-)(and (RIGHTOF17-ROBOT)))

(:derived (Flag37-)(and (RIGHTOF14-ROBOT)))

(:derived (Flag37-)(and (RIGHTOF15-ROBOT)))

(:derived (Flag37-)(and (RIGHTOF16-ROBOT)))

(:derived (Flag37-)(and (RIGHTOF19-ROBOT)))

(:derived (Flag38-)(and (RIGHTOF16-ROBOT)))

(:derived (Flag38-)(and (RIGHTOF17-ROBOT)))

(:derived (Flag38-)(and (RIGHTOF18-ROBOT)))

(:derived (Flag38-)(and (RIGHTOF19-ROBOT)))

(:derived (Flag38-)(and (RIGHTOF15-ROBOT)))

(:derived (Flag39-)(and (RIGHTOF16-ROBOT)))

(:derived (Flag39-)(and (RIGHTOF17-ROBOT)))

(:derived (Flag39-)(and (RIGHTOF18-ROBOT)))

(:derived (Flag39-)(and (RIGHTOF19-ROBOT)))

(:derived (Flag40-)(and (RIGHTOF17-ROBOT)))

(:derived (Flag40-)(and (RIGHTOF18-ROBOT)))

(:derived (Flag40-)(and (RIGHTOF19-ROBOT)))

(:derived (Flag41-)(and (RIGHTOF18-ROBOT)))

(:derived (Flag41-)(and (RIGHTOF19-ROBOT)))

(:derived (Flag42-)(and (RIGHTOF11-ROBOT)(LEFTOF2-ROBOT)))

(:derived (Flag43-)(and (RIGHTOF18-ROBOT)(LEFTOF2-ROBOT)))

(:derived (Flag44-)(and (RIGHTOF13-ROBOT)(LEFTOF2-ROBOT)))

(:derived (Flag45-)(and (LEFTOF2-ROBOT)(RIGHTOF12-ROBOT)))

(:derived (Flag46-)(and (LEFTOF1-ROBOT)(RIGHTOF9-ROBOT)))

(:derived (Flag47-)(and (RIGHTOF3-ROBOT)(LEFTOF1-ROBOT)))

(:derived (Flag48-)(and (RIGHTOF18-ROBOT)(LEFTOF1-ROBOT)))

(:derived (Flag49-)(and (LEFTOF1-ROBOT)(RIGHTOF15-ROBOT)))

(:derived (Flag50-)(and (RIGHTOF10-ROBOT)(LEFTOF1-ROBOT)))

(:derived (Flag51-)(and (LEFTOF2-ROBOT)(RIGHTOF1-ROBOT)))

(:derived (Flag52-)(and (RIGHTOF5-ROBOT)(LEFTOF2-ROBOT)))

(:derived (Flag53-)(and (LEFTOF2-ROBOT)(RIGHTOF9-ROBOT)))

(:derived (Flag54-)(and (LEFTOF1-ROBOT)(RIGHTOF1-ROBOT)))

(:derived (Flag55-)(and (LEFTOF2-ROBOT)(RIGHTOF7-ROBOT)))

(:derived (Flag56-)(and (RIGHTOF16-ROBOT)(LEFTOF2-ROBOT)))

(:derived (Flag57-)(and (RIGHTOF5-ROBOT)(LEFTOF1-ROBOT)))

(:derived (Flag58-)(and (RIGHTOF11-ROBOT)(LEFTOF1-ROBOT)))

(:derived (Flag59-)(and (RIGHTOF3-ROBOT)(LEFTOF2-ROBOT)))

(:derived (Flag60-)(and (LEFTOF2-ROBOT)(RIGHTOF17-ROBOT)))

(:derived (Flag61-)(and (LEFTOF1-ROBOT)(RIGHTOF4-ROBOT)))

(:derived (Flag62-)(and (RIGHTOF10-ROBOT)(LEFTOF2-ROBOT)))

(:derived (Flag63-)(and (LEFTOF1-ROBOT)(RIGHTOF12-ROBOT)))

(:derived (Flag64-)(and (LEFTOF1-ROBOT)(RIGHTOF7-ROBOT)))

(:derived (Flag65-)(and (RIGHTOF8-ROBOT)(LEFTOF1-ROBOT)))

(:derived (Flag66-)(and (LEFTOF1-ROBOT)(RIGHTOF5-ROBOT)))

(:derived (Flag67-)(and (LEFTOF2-ROBOT)(RIGHTOF14-ROBOT)))

(:derived (Flag68-)(and (LEFTOF1-ROBOT)(RIGHTOF17-ROBOT)))

(:derived (Flag69-)(and (RIGHTOF19-ROBOT)(LEFTOF1-ROBOT)))

(:derived (Flag70-)(and (RIGHTOF13-ROBOT)(LEFTOF1-ROBOT)))

(:derived (Flag71-)(and (LEFTOF2-ROBOT)(RIGHTOF15-ROBOT)))

(:derived (Flag72-)(and (RIGHTOF8-ROBOT)(LEFTOF2-ROBOT)))

(:derived (Flag73-)(and (RIGHTOF2-ROBOT)(LEFTOF2-ROBOT)))

(:derived (Flag74-)(and (LEFTOF1-ROBOT)(RIGHTOF2-ROBOT)))

(:derived (Flag75-)(and (LEFTOF1-ROBOT)(RIGHTOF6-ROBOT)))

(:derived (Flag76-)(and (LEFTOF1-ROBOT)(RIGHTOF14-ROBOT)))

(:derived (Flag77-)(and (LEFTOF1-ROBOT)(RIGHTOF16-ROBOT)))

(:derived (Flag78-)(and (RIGHTOF19-ROBOT)(LEFTOF2-ROBOT)))

(:derived (Flag79-)(and (LEFTOF2-ROBOT)(RIGHTOF6-ROBOT)))

(:derived (Flag80-)(and (LEFTOF2-ROBOT)(RIGHTOF4-ROBOT)))

(:derived (Flag82-)(and (LEFTOF3-ROBOT)(RIGHTOF6-ROBOT)))

(:derived (Flag83-)(and (LEFTOF3-ROBOT)(RIGHTOF14-ROBOT)))

(:derived (Flag84-)(and (LEFTOF3-ROBOT)(RIGHTOF4-ROBOT)))

(:derived (Flag85-)(and (LEFTOF3-ROBOT)(RIGHTOF16-ROBOT)))

(:derived (Flag86-)(and (LEFTOF3-ROBOT)(RIGHTOF7-ROBOT)))

(:derived (Flag87-)(and (LEFTOF3-ROBOT)(RIGHTOF12-ROBOT)))

(:derived (Flag88-)(and (LEFTOF3-ROBOT)(RIGHTOF10-ROBOT)))

(:derived (Flag89-)(and (LEFTOF3-ROBOT)(RIGHTOF8-ROBOT)))

(:derived (Flag90-)(and (LEFTOF3-ROBOT)(RIGHTOF5-ROBOT)))

(:derived (Flag91-)(and (LEFTOF3-ROBOT)(RIGHTOF15-ROBOT)))

(:derived (Flag92-)(and (RIGHTOF3-ROBOT)(LEFTOF3-ROBOT)))

(:derived (Flag93-)(and (LEFTOF3-ROBOT)(RIGHTOF11-ROBOT)))

(:derived (Flag94-)(and (LEFTOF3-ROBOT)(RIGHTOF18-ROBOT)))

(:derived (Flag95-)(and (RIGHTOF13-ROBOT)(LEFTOF3-ROBOT)))

(:derived (Flag96-)(and (LEFTOF3-ROBOT)(RIGHTOF17-ROBOT)))

(:derived (Flag97-)(and (LEFTOF3-ROBOT)(RIGHTOF19-ROBOT)))

(:derived (Flag98-)(and (LEFTOF3-ROBOT)(RIGHTOF9-ROBOT)))

(:derived (Flag99-)(and (LEFTOF3-ROBOT)(RIGHTOF2-ROBOT)))

(:derived (Flag101-)(and (RIGHTOF18-ROBOT)(LEFTOF4-ROBOT)))

(:derived (Flag102-)(and (LEFTOF4-ROBOT)(RIGHTOF16-ROBOT)))

(:derived (Flag103-)(and (LEFTOF3-ROBOT)(RIGHTOF13-ROBOT)))

(:derived (Flag104-)(and (LEFTOF4-ROBOT)(RIGHTOF17-ROBOT)))

(:derived (Flag105-)(and (LEFTOF4-ROBOT)(RIGHTOF4-ROBOT)))

(:derived (Flag106-)(and (LEFTOF4-ROBOT)(RIGHTOF7-ROBOT)))

(:derived (Flag107-)(and (LEFTOF4-ROBOT)(RIGHTOF15-ROBOT)))

(:derived (Flag108-)(and (LEFTOF4-ROBOT)(RIGHTOF14-ROBOT)))

(:derived (Flag109-)(and (LEFTOF3-ROBOT)(RIGHTOF3-ROBOT)))

(:derived (Flag110-)(and (RIGHTOF8-ROBOT)(LEFTOF4-ROBOT)))

(:derived (Flag111-)(and (LEFTOF4-ROBOT)(RIGHTOF12-ROBOT)))

(:derived (Flag112-)(and (RIGHTOF10-ROBOT)(LEFTOF4-ROBOT)))

(:derived (Flag113-)(and (LEFTOF4-ROBOT)(RIGHTOF19-ROBOT)))

(:derived (Flag114-)(and (LEFTOF4-ROBOT)(RIGHTOF9-ROBOT)))

(:derived (Flag115-)(and (LEFTOF4-ROBOT)(RIGHTOF6-ROBOT)))

(:derived (Flag116-)(and (LEFTOF4-ROBOT)(RIGHTOF5-ROBOT)))

(:derived (Flag117-)(and (RIGHTOF13-ROBOT)(LEFTOF4-ROBOT)))

(:derived (Flag118-)(and (RIGHTOF3-ROBOT)(LEFTOF4-ROBOT)))

(:derived (Flag119-)(and (LEFTOF4-ROBOT)(RIGHTOF11-ROBOT)))

(:derived (Flag121-)(and (RIGHTOF9-ROBOT)(LEFTOF5-ROBOT)))

(:derived (Flag122-)(and (RIGHTOF15-ROBOT)(LEFTOF5-ROBOT)))

(:derived (Flag123-)(and (RIGHTOF4-ROBOT)(LEFTOF5-ROBOT)))

(:derived (Flag124-)(and (RIGHTOF18-ROBOT)(LEFTOF5-ROBOT)))

(:derived (Flag125-)(and (LEFTOF4-ROBOT)(RIGHTOF18-ROBOT)))

(:derived (Flag126-)(and (RIGHTOF7-ROBOT)(LEFTOF5-ROBOT)))

(:derived (Flag127-)(and (RIGHTOF11-ROBOT)(LEFTOF5-ROBOT)))

(:derived (Flag128-)(and (RIGHTOF13-ROBOT)(LEFTOF5-ROBOT)))

(:derived (Flag129-)(and (RIGHTOF16-ROBOT)(LEFTOF5-ROBOT)))

(:derived (Flag130-)(and (LEFTOF4-ROBOT)(RIGHTOF8-ROBOT)))

(:derived (Flag131-)(and (LEFTOF5-ROBOT)(RIGHTOF6-ROBOT)))

(:derived (Flag132-)(and (RIGHTOF10-ROBOT)(LEFTOF5-ROBOT)))

(:derived (Flag133-)(and (LEFTOF4-ROBOT)(RIGHTOF10-ROBOT)))

(:derived (Flag134-)(and (LEFTOF5-ROBOT)(RIGHTOF12-ROBOT)))

(:derived (Flag135-)(and (RIGHTOF8-ROBOT)(LEFTOF5-ROBOT)))

(:derived (Flag136-)(and (RIGHTOF19-ROBOT)(LEFTOF5-ROBOT)))

(:derived (Flag137-)(and (RIGHTOF14-ROBOT)(LEFTOF5-ROBOT)))

(:derived (Flag138-)(and (RIGHTOF5-ROBOT)(LEFTOF5-ROBOT)))

(:derived (Flag139-)(and (RIGHTOF17-ROBOT)(LEFTOF5-ROBOT)))

(:derived (Flag141-)(and (RIGHTOF15-ROBOT)(LEFTOF6-ROBOT)))

(:derived (Flag142-)(and (RIGHTOF11-ROBOT)(LEFTOF6-ROBOT)))

(:derived (Flag143-)(and (RIGHTOF5-ROBOT)(LEFTOF6-ROBOT)))

(:derived (Flag144-)(and (RIGHTOF14-ROBOT)(LEFTOF6-ROBOT)))

(:derived (Flag145-)(and (RIGHTOF7-ROBOT)(LEFTOF6-ROBOT)))

(:derived (Flag146-)(and (RIGHTOF12-ROBOT)(LEFTOF6-ROBOT)))

(:derived (Flag147-)(and (RIGHTOF17-ROBOT)(LEFTOF6-ROBOT)))

(:derived (Flag148-)(and (RIGHTOF10-ROBOT)(LEFTOF6-ROBOT)))

(:derived (Flag149-)(and (RIGHTOF6-ROBOT)(LEFTOF6-ROBOT)))

(:derived (Flag150-)(and (RIGHTOF18-ROBOT)(LEFTOF6-ROBOT)))

(:derived (Flag151-)(and (RIGHTOF19-ROBOT)(LEFTOF6-ROBOT)))

(:derived (Flag152-)(and (RIGHTOF13-ROBOT)(LEFTOF6-ROBOT)))

(:derived (Flag153-)(and (RIGHTOF9-ROBOT)(LEFTOF6-ROBOT)))

(:derived (Flag154-)(and (RIGHTOF16-ROBOT)(LEFTOF6-ROBOT)))

(:derived (Flag155-)(and (RIGHTOF8-ROBOT)(LEFTOF6-ROBOT)))

(:derived (Flag157-)(and (LEFTOF7-ROBOT)(RIGHTOF7-ROBOT)))

(:derived (Flag158-)(and (RIGHTOF13-ROBOT)(LEFTOF7-ROBOT)))

(:derived (Flag159-)(and (LEFTOF7-ROBOT)(RIGHTOF9-ROBOT)))

(:derived (Flag160-)(and (LEFTOF6-ROBOT)(RIGHTOF6-ROBOT)))

(:derived (Flag161-)(and (RIGHTOF18-ROBOT)(LEFTOF7-ROBOT)))

(:derived (Flag162-)(and (LEFTOF7-ROBOT)(RIGHTOF12-ROBOT)))

(:derived (Flag163-)(and (LEFTOF7-ROBOT)(RIGHTOF6-ROBOT)))

(:derived (Flag164-)(and (LEFTOF6-ROBOT)(RIGHTOF12-ROBOT)))

(:derived (Flag165-)(and (LEFTOF7-ROBOT)(RIGHTOF15-ROBOT)))

(:derived (Flag166-)(and (RIGHTOF11-ROBOT)(LEFTOF7-ROBOT)))

(:derived (Flag167-)(and (RIGHTOF8-ROBOT)(LEFTOF7-ROBOT)))

(:derived (Flag168-)(and (RIGHTOF17-ROBOT)(LEFTOF7-ROBOT)))

(:derived (Flag169-)(and (RIGHTOF19-ROBOT)(LEFTOF7-ROBOT)))

(:derived (Flag170-)(and (RIGHTOF16-ROBOT)(LEFTOF7-ROBOT)))

(:derived (Flag171-)(and (LEFTOF7-ROBOT)(RIGHTOF14-ROBOT)))

(:derived (Flag172-)(and (RIGHTOF10-ROBOT)(LEFTOF7-ROBOT)))

(:derived (Flag174-)(and (RIGHTOF10-ROBOT)(LEFTOF8-ROBOT)))

(:derived (Flag175-)(and (RIGHTOF13-ROBOT)(LEFTOF8-ROBOT)))

(:derived (Flag176-)(and (RIGHTOF16-ROBOT)(LEFTOF8-ROBOT)))

(:derived (Flag177-)(and (RIGHTOF7-ROBOT)(LEFTOF7-ROBOT)))

(:derived (Flag178-)(and (LEFTOF8-ROBOT)(RIGHTOF15-ROBOT)))

(:derived (Flag179-)(and (LEFTOF8-ROBOT)(RIGHTOF9-ROBOT)))

(:derived (Flag180-)(and (RIGHTOF7-ROBOT)(LEFTOF8-ROBOT)))

(:derived (Flag181-)(and (RIGHTOF18-ROBOT)(LEFTOF8-ROBOT)))

(:derived (Flag182-)(and (RIGHTOF14-ROBOT)(LEFTOF8-ROBOT)))

(:derived (Flag183-)(and (LEFTOF8-ROBOT)(RIGHTOF12-ROBOT)))

(:derived (Flag184-)(and (RIGHTOF11-ROBOT)(LEFTOF8-ROBOT)))

(:derived (Flag185-)(and (RIGHTOF17-ROBOT)(LEFTOF8-ROBOT)))

(:derived (Flag186-)(and (RIGHTOF8-ROBOT)(LEFTOF8-ROBOT)))

(:derived (Flag187-)(and (RIGHTOF19-ROBOT)(LEFTOF8-ROBOT)))

(:derived (Flag189-)(and (RIGHTOF13-ROBOT)(LEFTOF9-ROBOT)))

(:derived (Flag190-)(and (RIGHTOF8-ROBOT)(LEFTOF9-ROBOT)))

(:derived (Flag191-)(and (LEFTOF9-ROBOT)(RIGHTOF12-ROBOT)))

(:derived (Flag192-)(and (LEFTOF9-ROBOT)(RIGHTOF9-ROBOT)))

(:derived (Flag193-)(and (LEFTOF8-ROBOT)(RIGHTOF14-ROBOT)))

(:derived (Flag194-)(and (RIGHTOF11-ROBOT)(LEFTOF9-ROBOT)))

(:derived (Flag195-)(and (LEFTOF9-ROBOT)(RIGHTOF15-ROBOT)))

(:derived (Flag196-)(and (RIGHTOF10-ROBOT)(LEFTOF9-ROBOT)))

(:derived (Flag197-)(and (RIGHTOF18-ROBOT)(LEFTOF9-ROBOT)))

(:derived (Flag198-)(and (LEFTOF9-ROBOT)(RIGHTOF16-ROBOT)))

(:derived (Flag199-)(and (RIGHTOF19-ROBOT)(LEFTOF9-ROBOT)))

(:derived (Flag200-)(and (LEFTOF9-ROBOT)(RIGHTOF17-ROBOT)))

(:derived (Flag201-)(and (LEFTOF9-ROBOT)(RIGHTOF14-ROBOT)))

(:derived (Flag203-)(and (LEFTOF10-ROBOT)(RIGHTOF12-ROBOT)))

(:derived (Flag204-)(and (RIGHTOF19-ROBOT)(LEFTOF10-ROBOT)))

(:derived (Flag205-)(and (RIGHTOF18-ROBOT)(LEFTOF10-ROBOT)))

(:derived (Flag206-)(and (RIGHTOF17-ROBOT)(LEFTOF10-ROBOT)))

(:derived (Flag207-)(and (RIGHTOF14-ROBOT)(LEFTOF10-ROBOT)))

(:derived (Flag208-)(and (RIGHTOF13-ROBOT)(LEFTOF10-ROBOT)))

(:derived (Flag209-)(and (LEFTOF10-ROBOT)(RIGHTOF9-ROBOT)))

(:derived (Flag210-)(and (RIGHTOF11-ROBOT)(LEFTOF10-ROBOT)))

(:derived (Flag211-)(and (RIGHTOF10-ROBOT)(LEFTOF10-ROBOT)))

(:derived (Flag212-)(and (RIGHTOF16-ROBOT)(LEFTOF10-ROBOT)))

(:derived (Flag213-)(and (LEFTOF10-ROBOT)(RIGHTOF15-ROBOT)))

(:derived (Flag215-)(and (LEFTOF11-ROBOT)(RIGHTOF15-ROBOT)))

(:derived (Flag216-)(and (LEFTOF11-ROBOT)(RIGHTOF16-ROBOT)))

(:derived (Flag217-)(and (RIGHTOF11-ROBOT)(LEFTOF11-ROBOT)))

(:derived (Flag218-)(and (LEFTOF10-ROBOT)(RIGHTOF14-ROBOT)))

(:derived (Flag219-)(and (RIGHTOF13-ROBOT)(LEFTOF11-ROBOT)))

(:derived (Flag220-)(and (LEFTOF11-ROBOT)(RIGHTOF14-ROBOT)))

(:derived (Flag221-)(and (LEFTOF11-ROBOT)(RIGHTOF12-ROBOT)))

(:derived (Flag222-)(and (RIGHTOF18-ROBOT)(LEFTOF11-ROBOT)))

(:derived (Flag223-)(and (RIGHTOF19-ROBOT)(LEFTOF11-ROBOT)))

(:derived (Flag224-)(and (LEFTOF11-ROBOT)(RIGHTOF17-ROBOT)))

(:derived (Flag225-)(and (RIGHTOF10-ROBOT)(LEFTOF11-ROBOT)))

(:derived (Flag227-)(and (RIGHTOF14-ROBOT)(LEFTOF12-ROBOT)))

(:derived (Flag228-)(and (RIGHTOF11-ROBOT)(LEFTOF12-ROBOT)))

(:derived (Flag229-)(and (RIGHTOF18-ROBOT)(LEFTOF12-ROBOT)))

(:derived (Flag230-)(and (RIGHTOF19-ROBOT)(LEFTOF12-ROBOT)))

(:derived (Flag231-)(and (RIGHTOF12-ROBOT)(LEFTOF12-ROBOT)))

(:derived (Flag232-)(and (RIGHTOF16-ROBOT)(LEFTOF12-ROBOT)))

(:derived (Flag233-)(and (RIGHTOF17-ROBOT)(LEFTOF12-ROBOT)))

(:derived (Flag234-)(and (RIGHTOF15-ROBOT)(LEFTOF12-ROBOT)))

(:derived (Flag235-)(and (RIGHTOF13-ROBOT)(LEFTOF12-ROBOT)))

(:derived (Flag237-)(and (RIGHTOF17-ROBOT)(LEFTOF13-ROBOT)))

(:derived (Flag238-)(and (RIGHTOF13-ROBOT)(LEFTOF13-ROBOT)))

(:derived (Flag239-)(and (RIGHTOF19-ROBOT)(LEFTOF13-ROBOT)))

(:derived (Flag240-)(and (LEFTOF13-ROBOT)(RIGHTOF15-ROBOT)))

(:derived (Flag241-)(and (LEFTOF13-ROBOT)(RIGHTOF14-ROBOT)))

(:derived (Flag242-)(and (LEFTOF12-ROBOT)(RIGHTOF12-ROBOT)))

(:derived (Flag243-)(and (RIGHTOF18-ROBOT)(LEFTOF13-ROBOT)))

(:derived (Flag244-)(and (LEFTOF13-ROBOT)(RIGHTOF12-ROBOT)))

(:derived (Flag245-)(and (RIGHTOF16-ROBOT)(LEFTOF13-ROBOT)))

(:derived (Flag247-)(and (LEFTOF14-ROBOT)(RIGHTOF14-ROBOT)))

(:derived (Flag248-)(and (RIGHTOF13-ROBOT)(LEFTOF14-ROBOT)))

(:derived (Flag249-)(and (LEFTOF14-ROBOT)(RIGHTOF17-ROBOT)))

(:derived (Flag250-)(and (LEFTOF14-ROBOT)(RIGHTOF16-ROBOT)))

(:derived (Flag251-)(and (RIGHTOF18-ROBOT)(LEFTOF14-ROBOT)))

(:derived (Flag252-)(and (LEFTOF14-ROBOT)(RIGHTOF15-ROBOT)))

(:derived (Flag253-)(and (LEFTOF14-ROBOT)(RIGHTOF19-ROBOT)))

(:derived (Flag255-)(and (RIGHTOF16-ROBOT)(LEFTOF15-ROBOT)))

(:derived (Flag256-)(and (RIGHTOF17-ROBOT)(LEFTOF15-ROBOT)))

(:derived (Flag257-)(and (RIGHTOF15-ROBOT)(LEFTOF15-ROBOT)))

(:derived (Flag258-)(and (LEFTOF14-ROBOT)(RIGHTOF18-ROBOT)))

(:derived (Flag259-)(and (RIGHTOF19-ROBOT)(LEFTOF15-ROBOT)))

(:derived (Flag260-)(and (RIGHTOF18-ROBOT)(LEFTOF15-ROBOT)))

(:derived (Flag261-)(and (RIGHTOF14-ROBOT)(LEFTOF15-ROBOT)))

(:derived (Flag263-)(and (LEFTOF16-ROBOT)(RIGHTOF17-ROBOT)))

(:derived (Flag264-)(and (RIGHTOF18-ROBOT)(LEFTOF16-ROBOT)))

(:derived (Flag265-)(and (RIGHTOF19-ROBOT)(LEFTOF16-ROBOT)))

(:derived (Flag266-)(and (LEFTOF15-ROBOT)(RIGHTOF15-ROBOT)))

(:derived (Flag267-)(and (RIGHTOF16-ROBOT)(LEFTOF16-ROBOT)))

(:derived (Flag268-)(and (LEFTOF16-ROBOT)(RIGHTOF15-ROBOT)))

(:derived (Flag270-)(and (LEFTOF17-ROBOT)(RIGHTOF17-ROBOT)))

(:derived (Flag271-)(and (LEFTOF17-ROBOT)(RIGHTOF16-ROBOT)))

(:derived (Flag272-)(and (LEFTOF17-ROBOT)(RIGHTOF18-ROBOT)))

(:derived (Flag273-)(and (LEFTOF17-ROBOT)(RIGHTOF19-ROBOT)))

(:derived (Flag275-)(and (RIGHTOF18-ROBOT)(LEFTOF18-ROBOT)))

(:derived (Flag276-)(and (RIGHTOF17-ROBOT)(LEFTOF18-ROBOT)))

(:derived (Flag277-)(and (RIGHTOF19-ROBOT)(LEFTOF18-ROBOT)))

(:derived (Flag279-)(and (RIGHTOF19-ROBOT)(LEFTOF19-ROBOT)))

(:derived (Flag280-)(and (RIGHTOF18-ROBOT)(LEFTOF19-ROBOT)))

(:derived (Flag282-)(and (RIGHTOF19-ROBOT)(LEFTOF20-ROBOT)))

(:derived (Flag339-)(and (Flag320-)))

(:derived (Flag339-)(and (Flag321-)))

(:derived (Flag339-)(and (Flag322-)))

(:derived (Flag339-)(and (Flag323-)))

(:derived (Flag339-)(and (Flag324-)))

(:derived (Flag339-)(and (Flag325-)))

(:derived (Flag339-)(and (Flag326-)))

(:derived (Flag339-)(and (Flag327-)))

(:derived (Flag339-)(and (Flag328-)))

(:derived (Flag339-)(and (Flag329-)))

(:derived (Flag339-)(and (Flag330-)))

(:derived (Flag339-)(and (Flag331-)))

(:derived (Flag339-)(and (Flag332-)))

(:derived (Flag339-)(and (Flag333-)))

(:derived (Flag339-)(and (Flag334-)))

(:derived (Flag339-)(and (Flag335-)))

(:derived (Flag339-)(and (Flag336-)))

(:derived (Flag339-)(and (Flag337-)))

(:derived (Flag339-)(and (Flag338-)))

(:derived (Flag360-)(and (Flag338-)))

(:derived (Flag360-)(and (Flag321-)))

(:derived (Flag360-)(and (Flag324-)))

(:derived (Flag360-)(and (Flag340-)))

(:derived (Flag360-)(and (Flag328-)))

(:derived (Flag360-)(and (Flag341-)))

(:derived (Flag360-)(and (Flag342-)))

(:derived (Flag360-)(and (Flag343-)))

(:derived (Flag360-)(and (Flag344-)))

(:derived (Flag360-)(and (Flag345-)))

(:derived (Flag360-)(and (Flag332-)))

(:derived (Flag360-)(and (Flag327-)))

(:derived (Flag360-)(and (Flag346-)))

(:derived (Flag360-)(and (Flag330-)))

(:derived (Flag360-)(and (Flag347-)))

(:derived (Flag360-)(and (Flag348-)))

(:derived (Flag360-)(and (Flag349-)))

(:derived (Flag360-)(and (Flag350-)))

(:derived (Flag360-)(and (Flag351-)))

(:derived (Flag360-)(and (Flag322-)))

(:derived (Flag360-)(and (Flag352-)))

(:derived (Flag360-)(and (Flag326-)))

(:derived (Flag360-)(and (Flag329-)))

(:derived (Flag360-)(and (Flag331-)))

(:derived (Flag360-)(and (Flag353-)))

(:derived (Flag360-)(and (Flag333-)))

(:derived (Flag360-)(and (Flag354-)))

(:derived (Flag360-)(and (Flag336-)))

(:derived (Flag360-)(and (Flag337-)))

(:derived (Flag360-)(and (Flag355-)))

(:derived (Flag360-)(and (Flag356-)))

(:derived (Flag360-)(and (Flag320-)))

(:derived (Flag360-)(and (Flag357-)))

(:derived (Flag360-)(and (Flag325-)))

(:derived (Flag360-)(and (Flag358-)))

(:derived (Flag360-)(and (Flag323-)))

(:derived (Flag360-)(and (Flag334-)))

(:derived (Flag360-)(and (Flag335-)))

(:derived (Flag360-)(and (Flag359-)))

(:derived (Flag380-)(and (Flag338-)))

(:derived (Flag380-)(and (Flag321-)))

(:derived (Flag380-)(and (Flag361-)))

(:derived (Flag380-)(and (Flag332-)))

(:derived (Flag380-)(and (Flag340-)))

(:derived (Flag380-)(and (Flag354-)))

(:derived (Flag380-)(and (Flag362-)))

(:derived (Flag380-)(and (Flag322-)))

(:derived (Flag380-)(and (Flag363-)))

(:derived (Flag380-)(and (Flag328-)))

(:derived (Flag380-)(and (Flag349-)))

(:derived (Flag380-)(and (Flag342-)))

(:derived (Flag380-)(and (Flag343-)))

(:derived (Flag380-)(and (Flag364-)))

(:derived (Flag380-)(and (Flag365-)))

(:derived (Flag380-)(and (Flag344-)))

(:derived (Flag380-)(and (Flag345-)))

(:derived (Flag380-)(and (Flag327-)))

(:derived (Flag380-)(and (Flag366-)))

(:derived (Flag380-)(and (Flag330-)))

(:derived (Flag380-)(and (Flag367-)))

(:derived (Flag380-)(and (Flag368-)))

(:derived (Flag380-)(and (Flag347-)))

(:derived (Flag380-)(and (Flag348-)))

(:derived (Flag380-)(and (Flag350-)))

(:derived (Flag380-)(and (Flag369-)))

(:derived (Flag380-)(and (Flag351-)))

(:derived (Flag380-)(and (Flag370-)))

(:derived (Flag380-)(and (Flag352-)))

(:derived (Flag380-)(and (Flag326-)))

(:derived (Flag380-)(and (Flag371-)))

(:derived (Flag380-)(and (Flag329-)))

(:derived (Flag380-)(and (Flag372-)))

(:derived (Flag380-)(and (Flag353-)))

(:derived (Flag380-)(and (Flag333-)))

(:derived (Flag380-)(and (Flag331-)))

(:derived (Flag380-)(and (Flag373-)))

(:derived (Flag380-)(and (Flag374-)))

(:derived (Flag380-)(and (Flag336-)))

(:derived (Flag380-)(and (Flag375-)))

(:derived (Flag380-)(and (Flag337-)))

(:derived (Flag380-)(and (Flag355-)))

(:derived (Flag380-)(and (Flag356-)))

(:derived (Flag380-)(and (Flag320-)))

(:derived (Flag380-)(and (Flag357-)))

(:derived (Flag380-)(and (Flag376-)))

(:derived (Flag380-)(and (Flag325-)))

(:derived (Flag380-)(and (Flag377-)))

(:derived (Flag380-)(and (Flag378-)))

(:derived (Flag380-)(and (Flag358-)))

(:derived (Flag380-)(and (Flag323-)))

(:derived (Flag380-)(and (Flag334-)))

(:derived (Flag380-)(and (Flag335-)))

(:derived (Flag380-)(and (Flag379-)))

(:derived (Flag380-)(and (Flag359-)))

(:derived (Flag401-)(and (Flag338-)))

(:derived (Flag401-)(and (Flag340-)))

(:derived (Flag401-)(and (Flag365-)))

(:derived (Flag401-)(and (Flag361-)))

(:derived (Flag401-)(and (Flag381-)))

(:derived (Flag401-)(and (Flag382-)))

(:derived (Flag401-)(and (Flag383-)))

(:derived (Flag401-)(and (Flag384-)))

(:derived (Flag401-)(and (Flag385-)))

(:derived (Flag401-)(and (Flag354-)))

(:derived (Flag401-)(and (Flag362-)))

(:derived (Flag401-)(and (Flag363-)))

(:derived (Flag401-)(and (Flag332-)))

(:derived (Flag401-)(and (Flag328-)))

(:derived (Flag401-)(and (Flag349-)))

(:derived (Flag401-)(and (Flag342-)))

(:derived (Flag401-)(and (Flag386-)))

(:derived (Flag401-)(and (Flag343-)))

(:derived (Flag401-)(and (Flag364-)))

(:derived (Flag401-)(and (Flag387-)))

(:derived (Flag401-)(and (Flag344-)))

(:derived (Flag401-)(and (Flag345-)))

(:derived (Flag401-)(and (Flag388-)))

(:derived (Flag401-)(and (Flag327-)))

(:derived (Flag401-)(and (Flag366-)))

(:derived (Flag401-)(and (Flag330-)))

(:derived (Flag401-)(and (Flag389-)))

(:derived (Flag401-)(and (Flag367-)))

(:derived (Flag401-)(and (Flag368-)))

(:derived (Flag401-)(and (Flag347-)))

(:derived (Flag401-)(and (Flag348-)))

(:derived (Flag401-)(and (Flag390-)))

(:derived (Flag401-)(and (Flag391-)))

(:derived (Flag401-)(and (Flag392-)))

(:derived (Flag401-)(and (Flag351-)))

(:derived (Flag401-)(and (Flag321-)))

(:derived (Flag401-)(and (Flag352-)))

(:derived (Flag401-)(and (Flag393-)))

(:derived (Flag401-)(and (Flag326-)))

(:derived (Flag401-)(and (Flag371-)))

(:derived (Flag401-)(and (Flag329-)))

(:derived (Flag401-)(and (Flag372-)))

(:derived (Flag401-)(and (Flag353-)))

(:derived (Flag401-)(and (Flag357-)))

(:derived (Flag401-)(and (Flag333-)))

(:derived (Flag401-)(and (Flag331-)))

(:derived (Flag401-)(and (Flag373-)))

(:derived (Flag401-)(and (Flag394-)))

(:derived (Flag401-)(and (Flag336-)))

(:derived (Flag401-)(and (Flag375-)))

(:derived (Flag401-)(and (Flag337-)))

(:derived (Flag401-)(and (Flag355-)))

(:derived (Flag401-)(and (Flag395-)))

(:derived (Flag401-)(and (Flag396-)))

(:derived (Flag401-)(and (Flag320-)))

(:derived (Flag401-)(and (Flag397-)))

(:derived (Flag401-)(and (Flag398-)))

(:derived (Flag401-)(and (Flag376-)))

(:derived (Flag401-)(and (Flag399-)))

(:derived (Flag401-)(and (Flag400-)))

(:derived (Flag401-)(and (Flag377-)))

(:derived (Flag401-)(and (Flag350-)))

(:derived (Flag401-)(and (Flag325-)))

(:derived (Flag401-)(and (Flag378-)))

(:derived (Flag401-)(and (Flag358-)))

(:derived (Flag401-)(and (Flag323-)))

(:derived (Flag401-)(and (Flag334-)))

(:derived (Flag401-)(and (Flag335-)))

(:derived (Flag401-)(and (Flag379-)))

(:derived (Flag401-)(and (Flag356-)))

(:derived (Flag401-)(and (Flag359-)))

(:derived (Flag419-)(and (Flag338-)))

(:derived (Flag419-)(and (Flag321-)))

(:derived (Flag419-)(and (Flag402-)))

(:derived (Flag419-)(and (Flag365-)))

(:derived (Flag419-)(and (Flag403-)))

(:derived (Flag419-)(and (Flag361-)))

(:derived (Flag419-)(and (Flag381-)))

(:derived (Flag419-)(and (Flag384-)))

(:derived (Flag419-)(and (Flag382-)))

(:derived (Flag419-)(and (Flag340-)))

(:derived (Flag419-)(and (Flag398-)))

(:derived (Flag419-)(and (Flag404-)))

(:derived (Flag419-)(and (Flag362-)))

(:derived (Flag419-)(and (Flag405-)))

(:derived (Flag419-)(and (Flag363-)))

(:derived (Flag419-)(and (Flag332-)))

(:derived (Flag419-)(and (Flag406-)))

(:derived (Flag419-)(and (Flag407-)))

(:derived (Flag419-)(and (Flag328-)))

(:derived (Flag419-)(and (Flag349-)))

(:derived (Flag419-)(and (Flag342-)))

(:derived (Flag419-)(and (Flag386-)))

(:derived (Flag419-)(and (Flag343-)))

(:derived (Flag419-)(and (Flag364-)))

(:derived (Flag419-)(and (Flag395-)))

(:derived (Flag419-)(and (Flag344-)))

(:derived (Flag419-)(and (Flag345-)))

(:derived (Flag419-)(and (Flag388-)))

(:derived (Flag419-)(and (Flag327-)))

(:derived (Flag419-)(and (Flag358-)))

(:derived (Flag419-)(and (Flag366-)))

(:derived (Flag419-)(and (Flag330-)))

(:derived (Flag419-)(and (Flag389-)))

(:derived (Flag419-)(and (Flag367-)))

(:derived (Flag419-)(and (Flag408-)))

(:derived (Flag419-)(and (Flag368-)))

(:derived (Flag419-)(and (Flag347-)))

(:derived (Flag419-)(and (Flag348-)))

(:derived (Flag419-)(and (Flag409-)))

(:derived (Flag419-)(and (Flag391-)))

(:derived (Flag419-)(and (Flag390-)))

(:derived (Flag419-)(and (Flag410-)))

(:derived (Flag419-)(and (Flag351-)))

(:derived (Flag419-)(and (Flag411-)))

(:derived (Flag419-)(and (Flag352-)))

(:derived (Flag419-)(and (Flag393-)))

(:derived (Flag419-)(and (Flag326-)))

(:derived (Flag419-)(and (Flag371-)))

(:derived (Flag419-)(and (Flag329-)))

(:derived (Flag419-)(and (Flag412-)))

(:derived (Flag419-)(and (Flag372-)))

(:derived (Flag419-)(and (Flag353-)))

(:derived (Flag419-)(and (Flag413-)))

(:derived (Flag419-)(and (Flag357-)))

(:derived (Flag419-)(and (Flag333-)))

(:derived (Flag419-)(and (Flag331-)))

(:derived (Flag419-)(and (Flag373-)))

(:derived (Flag419-)(and (Flag394-)))

(:derived (Flag419-)(and (Flag385-)))

(:derived (Flag419-)(and (Flag375-)))

(:derived (Flag419-)(and (Flag337-)))

(:derived (Flag419-)(and (Flag355-)))

(:derived (Flag419-)(and (Flag356-)))

(:derived (Flag419-)(and (Flag396-)))

(:derived (Flag419-)(and (Flag320-)))

(:derived (Flag419-)(and (Flag414-)))

(:derived (Flag419-)(and (Flag397-)))

(:derived (Flag419-)(and (Flag415-)))

(:derived (Flag419-)(and (Flag376-)))

(:derived (Flag419-)(and (Flag399-)))

(:derived (Flag419-)(and (Flag416-)))

(:derived (Flag419-)(and (Flag400-)))

(:derived (Flag419-)(and (Flag377-)))

(:derived (Flag419-)(and (Flag350-)))

(:derived (Flag419-)(and (Flag325-)))

(:derived (Flag419-)(and (Flag417-)))

(:derived (Flag419-)(and (Flag378-)))

(:derived (Flag419-)(and (Flag418-)))

(:derived (Flag419-)(and (Flag323-)))

(:derived (Flag419-)(and (Flag334-)))

(:derived (Flag419-)(and (Flag335-)))

(:derived (Flag419-)(and (Flag379-)))

(:derived (Flag419-)(and (Flag387-)))

(:derived (Flag419-)(and (Flag354-)))

(:derived (Flag437-)(and (Flag415-)))

(:derived (Flag437-)(and (Flag340-)))

(:derived (Flag437-)(and (Flag406-)))

(:derived (Flag437-)(and (Flag361-)))

(:derived (Flag437-)(and (Flag342-)))

(:derived (Flag437-)(and (Flag405-)))

(:derived (Flag437-)(and (Flag345-)))

(:derived (Flag437-)(and (Flag327-)))

(:derived (Flag437-)(and (Flag366-)))

(:derived (Flag437-)(and (Flag384-)))

(:derived (Flag437-)(and (Flag420-)))

(:derived (Flag437-)(and (Flag367-)))

(:derived (Flag437-)(and (Flag408-)))

(:derived (Flag437-)(and (Flag417-)))

(:derived (Flag437-)(and (Flag353-)))

(:derived (Flag437-)(and (Flag421-)))

(:derived (Flag437-)(and (Flag352-)))

(:derived (Flag437-)(and (Flag368-)))

(:derived (Flag437-)(and (Flag422-)))

(:derived (Flag437-)(and (Flag373-)))

(:derived (Flag437-)(and (Flag394-)))

(:derived (Flag437-)(and (Flag404-)))

(:derived (Flag437-)(and (Flag337-)))

(:derived (Flag437-)(and (Flag321-)))

(:derived (Flag437-)(and (Flag414-)))

(:derived (Flag437-)(and (Flag416-)))

(:derived (Flag437-)(and (Flag323-)))

(:derived (Flag437-)(and (Flag326-)))

(:derived (Flag437-)(and (Flag379-)))

(:derived (Flag437-)(and (Flag332-)))

(:derived (Flag437-)(and (Flag382-)))

(:derived (Flag437-)(and (Flag423-)))

(:derived (Flag437-)(and (Flag363-)))

(:derived (Flag437-)(and (Flag424-)))

(:derived (Flag437-)(and (Flag425-)))

(:derived (Flag437-)(and (Flag365-)))

(:derived (Flag437-)(and (Flag330-)))

(:derived (Flag437-)(and (Flag410-)))

(:derived (Flag437-)(and (Flag347-)))

(:derived (Flag437-)(and (Flag388-)))

(:derived (Flag437-)(and (Flag331-)))

(:derived (Flag437-)(and (Flag333-)))

(:derived (Flag437-)(and (Flag426-)))

(:derived (Flag437-)(and (Flag385-)))

(:derived (Flag437-)(and (Flag375-)))

(:derived (Flag437-)(and (Flag355-)))

(:derived (Flag437-)(and (Flag395-)))

(:derived (Flag437-)(and (Flag397-)))

(:derived (Flag437-)(and (Flag325-)))

(:derived (Flag437-)(and (Flag377-)))

(:derived (Flag437-)(and (Flag427-)))

(:derived (Flag437-)(and (Flag418-)))

(:derived (Flag437-)(and (Flag334-)))

(:derived (Flag437-)(and (Flag335-)))

(:derived (Flag437-)(and (Flag338-)))

(:derived (Flag437-)(and (Flag402-)))

(:derived (Flag437-)(and (Flag428-)))

(:derived (Flag437-)(and (Flag429-)))

(:derived (Flag437-)(and (Flag407-)))

(:derived (Flag437-)(and (Flag328-)))

(:derived (Flag437-)(and (Flag430-)))

(:derived (Flag437-)(and (Flag364-)))

(:derived (Flag437-)(and (Flag381-)))

(:derived (Flag437-)(and (Flag398-)))

(:derived (Flag437-)(and (Flag390-)))

(:derived (Flag437-)(and (Flag350-)))

(:derived (Flag437-)(and (Flag351-)))

(:derived (Flag437-)(and (Flag393-)))

(:derived (Flag437-)(and (Flag371-)))

(:derived (Flag437-)(and (Flag329-)))

(:derived (Flag437-)(and (Flag413-)))

(:derived (Flag437-)(and (Flag357-)))

(:derived (Flag437-)(and (Flag431-)))

(:derived (Flag437-)(and (Flag432-)))

(:derived (Flag437-)(and (Flag356-)))

(:derived (Flag437-)(and (Flag396-)))

(:derived (Flag437-)(and (Flag411-)))

(:derived (Flag437-)(and (Flag399-)))

(:derived (Flag437-)(and (Flag378-)))

(:derived (Flag437-)(and (Flag433-)))

(:derived (Flag437-)(and (Flag400-)))

(:derived (Flag437-)(and (Flag434-)))

(:derived (Flag437-)(and (Flag435-)))

(:derived (Flag437-)(and (Flag349-)))

(:derived (Flag437-)(and (Flag343-)))

(:derived (Flag437-)(and (Flag387-)))

(:derived (Flag437-)(and (Flag344-)))

(:derived (Flag437-)(and (Flag358-)))

(:derived (Flag437-)(and (Flag436-)))

(:derived (Flag437-)(and (Flag389-)))

(:derived (Flag437-)(and (Flag409-)))

(:derived (Flag437-)(and (Flag391-)))

(:derived (Flag437-)(and (Flag362-)))

(:derived (Flag437-)(and (Flag354-)))

(:derived (Flag437-)(and (Flag376-)))

(:derived (Flag455-)(and (Flag438-)))

(:derived (Flag455-)(and (Flag415-)))

(:derived (Flag455-)(and (Flag340-)))

(:derived (Flag455-)(and (Flag406-)))

(:derived (Flag455-)(and (Flag342-)))

(:derived (Flag455-)(and (Flag405-)))

(:derived (Flag455-)(and (Flag345-)))

(:derived (Flag455-)(and (Flag327-)))

(:derived (Flag455-)(and (Flag366-)))

(:derived (Flag455-)(and (Flag384-)))

(:derived (Flag455-)(and (Flag367-)))

(:derived (Flag455-)(and (Flag408-)))

(:derived (Flag455-)(and (Flag417-)))

(:derived (Flag455-)(and (Flag353-)))

(:derived (Flag455-)(and (Flag421-)))

(:derived (Flag455-)(and (Flag439-)))

(:derived (Flag455-)(and (Flag440-)))

(:derived (Flag455-)(and (Flag352-)))

(:derived (Flag455-)(and (Flag368-)))

(:derived (Flag455-)(and (Flag373-)))

(:derived (Flag455-)(and (Flag394-)))

(:derived (Flag455-)(and (Flag404-)))

(:derived (Flag455-)(and (Flag337-)))

(:derived (Flag455-)(and (Flag321-)))

(:derived (Flag455-)(and (Flag414-)))

(:derived (Flag455-)(and (Flag441-)))

(:derived (Flag455-)(and (Flag416-)))

(:derived (Flag455-)(and (Flag323-)))

(:derived (Flag455-)(and (Flag326-)))

(:derived (Flag455-)(and (Flag379-)))

(:derived (Flag455-)(and (Flag332-)))

(:derived (Flag455-)(and (Flag382-)))

(:derived (Flag455-)(and (Flag423-)))

(:derived (Flag455-)(and (Flag442-)))

(:derived (Flag455-)(and (Flag363-)))

(:derived (Flag455-)(and (Flag424-)))

(:derived (Flag455-)(and (Flag425-)))

(:derived (Flag455-)(and (Flag443-)))

(:derived (Flag455-)(and (Flag365-)))

(:derived (Flag455-)(and (Flag410-)))

(:derived (Flag455-)(and (Flag347-)))

(:derived (Flag455-)(and (Flag444-)))

(:derived (Flag455-)(and (Flag388-)))

(:derived (Flag455-)(and (Flag331-)))

(:derived (Flag455-)(and (Flag333-)))

(:derived (Flag455-)(and (Flag426-)))

(:derived (Flag455-)(and (Flag385-)))

(:derived (Flag455-)(and (Flag375-)))

(:derived (Flag455-)(and (Flag355-)))

(:derived (Flag455-)(and (Flag395-)))

(:derived (Flag455-)(and (Flag397-)))

(:derived (Flag455-)(and (Flag325-)))

(:derived (Flag455-)(and (Flag377-)))

(:derived (Flag455-)(and (Flag427-)))

(:derived (Flag455-)(and (Flag445-)))

(:derived (Flag455-)(and (Flag418-)))

(:derived (Flag455-)(and (Flag334-)))

(:derived (Flag455-)(and (Flag335-)))

(:derived (Flag455-)(and (Flag338-)))

(:derived (Flag455-)(and (Flag402-)))

(:derived (Flag455-)(and (Flag446-)))

(:derived (Flag455-)(and (Flag428-)))

(:derived (Flag455-)(and (Flag447-)))

(:derived (Flag455-)(and (Flag429-)))

(:derived (Flag455-)(and (Flag407-)))

(:derived (Flag455-)(and (Flag328-)))

(:derived (Flag455-)(and (Flag364-)))

(:derived (Flag455-)(and (Flag381-)))

(:derived (Flag455-)(and (Flag448-)))

(:derived (Flag455-)(and (Flag398-)))

(:derived (Flag455-)(and (Flag390-)))

(:derived (Flag455-)(and (Flag350-)))

(:derived (Flag455-)(and (Flag351-)))

(:derived (Flag455-)(and (Flag393-)))

(:derived (Flag455-)(and (Flag371-)))

(:derived (Flag455-)(and (Flag329-)))

(:derived (Flag455-)(and (Flag413-)))

(:derived (Flag455-)(and (Flag357-)))

(:derived (Flag455-)(and (Flag431-)))

(:derived (Flag455-)(and (Flag432-)))

(:derived (Flag455-)(and (Flag356-)))

(:derived (Flag455-)(and (Flag396-)))

(:derived (Flag455-)(and (Flag411-)))

(:derived (Flag455-)(and (Flag399-)))

(:derived (Flag455-)(and (Flag449-)))

(:derived (Flag455-)(and (Flag450-)))

(:derived (Flag455-)(and (Flag451-)))

(:derived (Flag455-)(and (Flag378-)))

(:derived (Flag455-)(and (Flag433-)))

(:derived (Flag455-)(and (Flag400-)))

(:derived (Flag455-)(and (Flag434-)))

(:derived (Flag455-)(and (Flag452-)))

(:derived (Flag455-)(and (Flag435-)))

(:derived (Flag455-)(and (Flag349-)))

(:derived (Flag455-)(and (Flag453-)))

(:derived (Flag455-)(and (Flag343-)))

(:derived (Flag455-)(and (Flag387-)))

(:derived (Flag455-)(and (Flag344-)))

(:derived (Flag455-)(and (Flag436-)))

(:derived (Flag455-)(and (Flag389-)))

(:derived (Flag455-)(and (Flag409-)))

(:derived (Flag455-)(and (Flag362-)))

(:derived (Flag455-)(and (Flag354-)))

(:derived (Flag455-)(and (Flag454-)))

(:derived (Flag455-)(and (Flag376-)))

(:derived (Flag470-)(and (Flag415-)))

(:derived (Flag470-)(and (Flag340-)))

(:derived (Flag470-)(and (Flag406-)))

(:derived (Flag470-)(and (Flag342-)))

(:derived (Flag470-)(and (Flag405-)))

(:derived (Flag470-)(and (Flag345-)))

(:derived (Flag470-)(and (Flag327-)))

(:derived (Flag470-)(and (Flag446-)))

(:derived (Flag470-)(and (Flag384-)))

(:derived (Flag470-)(and (Flag367-)))

(:derived (Flag470-)(and (Flag408-)))

(:derived (Flag470-)(and (Flag417-)))

(:derived (Flag470-)(and (Flag353-)))

(:derived (Flag470-)(and (Flag421-)))

(:derived (Flag470-)(and (Flag439-)))

(:derived (Flag470-)(and (Flag440-)))

(:derived (Flag470-)(and (Flag352-)))

(:derived (Flag470-)(and (Flag368-)))

(:derived (Flag470-)(and (Flag373-)))

(:derived (Flag470-)(and (Flag394-)))

(:derived (Flag470-)(and (Flag404-)))

(:derived (Flag470-)(and (Flag337-)))

(:derived (Flag470-)(and (Flag321-)))

(:derived (Flag470-)(and (Flag414-)))

(:derived (Flag470-)(and (Flag416-)))

(:derived (Flag470-)(and (Flag323-)))

(:derived (Flag470-)(and (Flag326-)))

(:derived (Flag470-)(and (Flag379-)))

(:derived (Flag470-)(and (Flag456-)))

(:derived (Flag470-)(and (Flag457-)))

(:derived (Flag470-)(and (Flag332-)))

(:derived (Flag470-)(and (Flag458-)))

(:derived (Flag470-)(and (Flag382-)))

(:derived (Flag470-)(and (Flag423-)))

(:derived (Flag470-)(and (Flag459-)))

(:derived (Flag470-)(and (Flag442-)))

(:derived (Flag470-)(and (Flag363-)))

(:derived (Flag470-)(and (Flag424-)))

(:derived (Flag470-)(and (Flag425-)))

(:derived (Flag470-)(and (Flag443-)))

(:derived (Flag470-)(and (Flag365-)))

(:derived (Flag470-)(and (Flag460-)))

(:derived (Flag470-)(and (Flag410-)))

(:derived (Flag470-)(and (Flag461-)))

(:derived (Flag470-)(and (Flag444-)))

(:derived (Flag470-)(and (Flag388-)))

(:derived (Flag470-)(and (Flag331-)))

(:derived (Flag470-)(and (Flag462-)))

(:derived (Flag470-)(and (Flag333-)))

(:derived (Flag470-)(and (Flag426-)))

(:derived (Flag470-)(and (Flag375-)))

(:derived (Flag470-)(and (Flag355-)))

(:derived (Flag470-)(and (Flag395-)))

(:derived (Flag470-)(and (Flag397-)))

(:derived (Flag470-)(and (Flag325-)))

(:derived (Flag470-)(and (Flag377-)))

(:derived (Flag470-)(and (Flag427-)))

(:derived (Flag470-)(and (Flag463-)))

(:derived (Flag470-)(and (Flag445-)))

(:derived (Flag470-)(and (Flag334-)))

(:derived (Flag470-)(and (Flag335-)))

(:derived (Flag470-)(and (Flag338-)))

(:derived (Flag470-)(and (Flag402-)))

(:derived (Flag470-)(and (Flag366-)))

(:derived (Flag470-)(and (Flag428-)))

(:derived (Flag470-)(and (Flag447-)))

(:derived (Flag470-)(and (Flag429-)))

(:derived (Flag470-)(and (Flag407-)))

(:derived (Flag470-)(and (Flag328-)))

(:derived (Flag470-)(and (Flag364-)))

(:derived (Flag470-)(and (Flag381-)))

(:derived (Flag470-)(and (Flag448-)))

(:derived (Flag470-)(and (Flag398-)))

(:derived (Flag470-)(and (Flag390-)))

(:derived (Flag470-)(and (Flag350-)))

(:derived (Flag470-)(and (Flag347-)))

(:derived (Flag470-)(and (Flag464-)))

(:derived (Flag470-)(and (Flag393-)))

(:derived (Flag470-)(and (Flag371-)))

(:derived (Flag470-)(and (Flag465-)))

(:derived (Flag470-)(and (Flag357-)))

(:derived (Flag470-)(and (Flag466-)))

(:derived (Flag470-)(and (Flag431-)))

(:derived (Flag470-)(and (Flag432-)))

(:derived (Flag470-)(and (Flag356-)))

(:derived (Flag470-)(and (Flag396-)))

(:derived (Flag470-)(and (Flag411-)))

(:derived (Flag470-)(and (Flag399-)))

(:derived (Flag470-)(and (Flag449-)))

(:derived (Flag470-)(and (Flag450-)))

(:derived (Flag470-)(and (Flag451-)))

(:derived (Flag470-)(and (Flag378-)))

(:derived (Flag470-)(and (Flag433-)))

(:derived (Flag470-)(and (Flag467-)))

(:derived (Flag470-)(and (Flag400-)))

(:derived (Flag470-)(and (Flag434-)))

(:derived (Flag470-)(and (Flag452-)))

(:derived (Flag470-)(and (Flag435-)))

(:derived (Flag470-)(and (Flag349-)))

(:derived (Flag470-)(and (Flag343-)))

(:derived (Flag470-)(and (Flag387-)))

(:derived (Flag470-)(and (Flag344-)))

(:derived (Flag470-)(and (Flag436-)))

(:derived (Flag470-)(and (Flag389-)))

(:derived (Flag470-)(and (Flag468-)))

(:derived (Flag470-)(and (Flag409-)))

(:derived (Flag470-)(and (Flag354-)))

(:derived (Flag470-)(and (Flag469-)))

(:derived (Flag470-)(and (Flag454-)))

(:derived (Flag470-)(and (Flag413-)))

(:derived (Flag470-)(and (Flag376-)))

(:derived (Flag485-)(and (Flag415-)))

(:derived (Flag485-)(and (Flag340-)))

(:derived (Flag485-)(and (Flag406-)))

(:derived (Flag485-)(and (Flag405-)))

(:derived (Flag485-)(and (Flag345-)))

(:derived (Flag485-)(and (Flag446-)))

(:derived (Flag485-)(and (Flag384-)))

(:derived (Flag485-)(and (Flag367-)))

(:derived (Flag485-)(and (Flag408-)))

(:derived (Flag485-)(and (Flag417-)))

(:derived (Flag485-)(and (Flag353-)))

(:derived (Flag485-)(and (Flag421-)))

(:derived (Flag485-)(and (Flag439-)))

(:derived (Flag485-)(and (Flag471-)))

(:derived (Flag485-)(and (Flag440-)))

(:derived (Flag485-)(and (Flag352-)))

(:derived (Flag485-)(and (Flag368-)))

(:derived (Flag485-)(and (Flag373-)))

(:derived (Flag485-)(and (Flag394-)))

(:derived (Flag485-)(and (Flag404-)))

(:derived (Flag485-)(and (Flag337-)))

(:derived (Flag485-)(and (Flag472-)))

(:derived (Flag485-)(and (Flag321-)))

(:derived (Flag485-)(and (Flag414-)))

(:derived (Flag485-)(and (Flag473-)))

(:derived (Flag485-)(and (Flag416-)))

(:derived (Flag485-)(and (Flag323-)))

(:derived (Flag485-)(and (Flag326-)))

(:derived (Flag485-)(and (Flag379-)))

(:derived (Flag485-)(and (Flag456-)))

(:derived (Flag485-)(and (Flag457-)))

(:derived (Flag485-)(and (Flag474-)))

(:derived (Flag485-)(and (Flag332-)))

(:derived (Flag485-)(and (Flag458-)))

(:derived (Flag485-)(and (Flag382-)))

(:derived (Flag485-)(and (Flag423-)))

(:derived (Flag485-)(and (Flag459-)))

(:derived (Flag485-)(and (Flag442-)))

(:derived (Flag485-)(and (Flag475-)))

(:derived (Flag485-)(and (Flag424-)))

(:derived (Flag485-)(and (Flag425-)))

(:derived (Flag485-)(and (Flag443-)))

(:derived (Flag485-)(and (Flag365-)))

(:derived (Flag485-)(and (Flag476-)))

(:derived (Flag485-)(and (Flag460-)))

(:derived (Flag485-)(and (Flag410-)))

(:derived (Flag485-)(and (Flag461-)))

(:derived (Flag485-)(and (Flag433-)))

(:derived (Flag485-)(and (Flag444-)))

(:derived (Flag485-)(and (Flag388-)))

(:derived (Flag485-)(and (Flag331-)))

(:derived (Flag485-)(and (Flag462-)))

(:derived (Flag485-)(and (Flag333-)))

(:derived (Flag485-)(and (Flag426-)))

(:derived (Flag485-)(and (Flag364-)))

(:derived (Flag485-)(and (Flag375-)))

(:derived (Flag485-)(and (Flag355-)))

(:derived (Flag485-)(and (Flag395-)))

(:derived (Flag485-)(and (Flag477-)))

(:derived (Flag485-)(and (Flag397-)))

(:derived (Flag485-)(and (Flag325-)))

(:derived (Flag485-)(and (Flag377-)))

(:derived (Flag485-)(and (Flag427-)))

(:derived (Flag485-)(and (Flag445-)))

(:derived (Flag485-)(and (Flag478-)))

(:derived (Flag485-)(and (Flag334-)))

(:derived (Flag485-)(and (Flag335-)))

(:derived (Flag485-)(and (Flag338-)))

(:derived (Flag485-)(and (Flag402-)))

(:derived (Flag485-)(and (Flag366-)))

(:derived (Flag485-)(and (Flag428-)))

(:derived (Flag485-)(and (Flag447-)))

(:derived (Flag485-)(and (Flag429-)))

(:derived (Flag485-)(and (Flag479-)))

(:derived (Flag485-)(and (Flag407-)))

(:derived (Flag485-)(and (Flag328-)))

(:derived (Flag485-)(and (Flag480-)))

(:derived (Flag485-)(and (Flag381-)))

(:derived (Flag485-)(and (Flag448-)))

(:derived (Flag485-)(and (Flag390-)))

(:derived (Flag485-)(and (Flag350-)))

(:derived (Flag485-)(and (Flag347-)))

(:derived (Flag485-)(and (Flag393-)))

(:derived (Flag485-)(and (Flag371-)))

(:derived (Flag485-)(and (Flag357-)))

(:derived (Flag485-)(and (Flag466-)))

(:derived (Flag485-)(and (Flag431-)))

(:derived (Flag485-)(and (Flag432-)))

(:derived (Flag485-)(and (Flag356-)))

(:derived (Flag485-)(and (Flag396-)))

(:derived (Flag485-)(and (Flag411-)))

(:derived (Flag485-)(and (Flag399-)))

(:derived (Flag485-)(and (Flag449-)))

(:derived (Flag485-)(and (Flag450-)))

(:derived (Flag485-)(and (Flag451-)))

(:derived (Flag485-)(and (Flag378-)))

(:derived (Flag485-)(and (Flag481-)))

(:derived (Flag485-)(and (Flag467-)))

(:derived (Flag485-)(and (Flag400-)))

(:derived (Flag485-)(and (Flag434-)))

(:derived (Flag485-)(and (Flag452-)))

(:derived (Flag485-)(and (Flag435-)))

(:derived (Flag485-)(and (Flag349-)))

(:derived (Flag485-)(and (Flag343-)))

(:derived (Flag485-)(and (Flag387-)))

(:derived (Flag485-)(and (Flag344-)))

(:derived (Flag485-)(and (Flag389-)))

(:derived (Flag485-)(and (Flag468-)))

(:derived (Flag485-)(and (Flag409-)))

(:derived (Flag485-)(and (Flag354-)))

(:derived (Flag485-)(and (Flag469-)))

(:derived (Flag485-)(and (Flag454-)))

(:derived (Flag485-)(and (Flag482-)))

(:derived (Flag485-)(and (Flag483-)))

(:derived (Flag485-)(and (Flag376-)))

(:derived (Flag485-)(and (Flag484-)))

(:derived (Flag498-)(and (Flag365-)))

(:derived (Flag498-)(and (Flag415-)))

(:derived (Flag498-)(and (Flag340-)))

(:derived (Flag498-)(and (Flag406-)))

(:derived (Flag498-)(and (Flag345-)))

(:derived (Flag498-)(and (Flag446-)))

(:derived (Flag498-)(and (Flag384-)))

(:derived (Flag498-)(and (Flag408-)))

(:derived (Flag498-)(and (Flag417-)))

(:derived (Flag498-)(and (Flag353-)))

(:derived (Flag498-)(and (Flag421-)))

(:derived (Flag498-)(and (Flag439-)))

(:derived (Flag498-)(and (Flag471-)))

(:derived (Flag498-)(and (Flag440-)))

(:derived (Flag498-)(and (Flag486-)))

(:derived (Flag498-)(and (Flag352-)))

(:derived (Flag498-)(and (Flag368-)))

(:derived (Flag498-)(and (Flag487-)))

(:derived (Flag498-)(and (Flag488-)))

(:derived (Flag498-)(and (Flag373-)))

(:derived (Flag498-)(and (Flag394-)))

(:derived (Flag498-)(and (Flag404-)))

(:derived (Flag498-)(and (Flag337-)))

(:derived (Flag498-)(and (Flag472-)))

(:derived (Flag498-)(and (Flag321-)))

(:derived (Flag498-)(and (Flag414-)))

(:derived (Flag498-)(and (Flag473-)))

(:derived (Flag498-)(and (Flag416-)))

(:derived (Flag498-)(and (Flag323-)))

(:derived (Flag498-)(and (Flag326-)))

(:derived (Flag498-)(and (Flag379-)))

(:derived (Flag498-)(and (Flag456-)))

(:derived (Flag498-)(and (Flag457-)))

(:derived (Flag498-)(and (Flag474-)))

(:derived (Flag498-)(and (Flag332-)))

(:derived (Flag498-)(and (Flag458-)))

(:derived (Flag498-)(and (Flag382-)))

(:derived (Flag498-)(and (Flag423-)))

(:derived (Flag498-)(and (Flag459-)))

(:derived (Flag498-)(and (Flag442-)))

(:derived (Flag498-)(and (Flag475-)))

(:derived (Flag498-)(and (Flag424-)))

(:derived (Flag498-)(and (Flag425-)))

(:derived (Flag498-)(and (Flag443-)))

(:derived (Flag498-)(and (Flag489-)))

(:derived (Flag498-)(and (Flag476-)))

(:derived (Flag498-)(and (Flag460-)))

(:derived (Flag498-)(and (Flag410-)))

(:derived (Flag498-)(and (Flag461-)))

(:derived (Flag498-)(and (Flag433-)))

(:derived (Flag498-)(and (Flag444-)))

(:derived (Flag498-)(and (Flag388-)))

(:derived (Flag498-)(and (Flag331-)))

(:derived (Flag498-)(and (Flag490-)))

(:derived (Flag498-)(and (Flag462-)))

(:derived (Flag498-)(and (Flag333-)))

(:derived (Flag498-)(and (Flag426-)))

(:derived (Flag498-)(and (Flag375-)))

(:derived (Flag498-)(and (Flag355-)))

(:derived (Flag498-)(and (Flag395-)))

(:derived (Flag498-)(and (Flag491-)))

(:derived (Flag498-)(and (Flag397-)))

(:derived (Flag498-)(and (Flag325-)))

(:derived (Flag498-)(and (Flag377-)))

(:derived (Flag498-)(and (Flag427-)))

(:derived (Flag498-)(and (Flag445-)))

(:derived (Flag498-)(and (Flag492-)))

(:derived (Flag498-)(and (Flag338-)))

(:derived (Flag498-)(and (Flag402-)))

(:derived (Flag498-)(and (Flag366-)))

(:derived (Flag498-)(and (Flag428-)))

(:derived (Flag498-)(and (Flag447-)))

(:derived (Flag498-)(and (Flag429-)))

(:derived (Flag498-)(and (Flag479-)))

(:derived (Flag498-)(and (Flag407-)))

(:derived (Flag498-)(and (Flag328-)))

(:derived (Flag498-)(and (Flag364-)))

(:derived (Flag498-)(and (Flag493-)))

(:derived (Flag498-)(and (Flag381-)))

(:derived (Flag498-)(and (Flag448-)))

(:derived (Flag498-)(and (Flag477-)))

(:derived (Flag498-)(and (Flag390-)))

(:derived (Flag498-)(and (Flag350-)))

(:derived (Flag498-)(and (Flag347-)))

(:derived (Flag498-)(and (Flag393-)))

(:derived (Flag498-)(and (Flag357-)))

(:derived (Flag498-)(and (Flag466-)))

(:derived (Flag498-)(and (Flag432-)))

(:derived (Flag498-)(and (Flag396-)))

(:derived (Flag498-)(and (Flag411-)))

(:derived (Flag498-)(and (Flag399-)))

(:derived (Flag498-)(and (Flag449-)))

(:derived (Flag498-)(and (Flag451-)))

(:derived (Flag498-)(and (Flag378-)))

(:derived (Flag498-)(and (Flag481-)))

(:derived (Flag498-)(and (Flag467-)))

(:derived (Flag498-)(and (Flag400-)))

(:derived (Flag498-)(and (Flag434-)))

(:derived (Flag498-)(and (Flag452-)))

(:derived (Flag498-)(and (Flag435-)))

(:derived (Flag498-)(and (Flag494-)))

(:derived (Flag498-)(and (Flag343-)))

(:derived (Flag498-)(and (Flag387-)))

(:derived (Flag498-)(and (Flag344-)))

(:derived (Flag498-)(and (Flag495-)))

(:derived (Flag498-)(and (Flag496-)))

(:derived (Flag498-)(and (Flag367-)))

(:derived (Flag498-)(and (Flag468-)))

(:derived (Flag498-)(and (Flag349-)))

(:derived (Flag498-)(and (Flag409-)))

(:derived (Flag498-)(and (Flag334-)))

(:derived (Flag498-)(and (Flag354-)))

(:derived (Flag498-)(and (Flag469-)))

(:derived (Flag498-)(and (Flag454-)))

(:derived (Flag498-)(and (Flag497-)))

(:derived (Flag498-)(and (Flag482-)))

(:derived (Flag498-)(and (Flag376-)))

(:derived (Flag498-)(and (Flag484-)))

(:derived (Flag509-)(and (Flag365-)))

(:derived (Flag509-)(and (Flag415-)))

(:derived (Flag509-)(and (Flag340-)))

(:derived (Flag509-)(and (Flag406-)))

(:derived (Flag509-)(and (Flag345-)))

(:derived (Flag509-)(and (Flag446-)))

(:derived (Flag509-)(and (Flag384-)))

(:derived (Flag509-)(and (Flag408-)))

(:derived (Flag509-)(and (Flag417-)))

(:derived (Flag509-)(and (Flag353-)))

(:derived (Flag509-)(and (Flag421-)))

(:derived (Flag509-)(and (Flag439-)))

(:derived (Flag509-)(and (Flag471-)))

(:derived (Flag509-)(and (Flag440-)))

(:derived (Flag509-)(and (Flag486-)))

(:derived (Flag509-)(and (Flag368-)))

(:derived (Flag509-)(and (Flag488-)))

(:derived (Flag509-)(and (Flag373-)))

(:derived (Flag509-)(and (Flag394-)))

(:derived (Flag509-)(and (Flag404-)))

(:derived (Flag509-)(and (Flag472-)))

(:derived (Flag509-)(and (Flag321-)))

(:derived (Flag509-)(and (Flag487-)))

(:derived (Flag509-)(and (Flag473-)))

(:derived (Flag509-)(and (Flag416-)))

(:derived (Flag509-)(and (Flag323-)))

(:derived (Flag509-)(and (Flag326-)))

(:derived (Flag509-)(and (Flag379-)))

(:derived (Flag509-)(and (Flag456-)))

(:derived (Flag509-)(and (Flag457-)))

(:derived (Flag509-)(and (Flag474-)))

(:derived (Flag509-)(and (Flag332-)))

(:derived (Flag509-)(and (Flag499-)))

(:derived (Flag509-)(and (Flag458-)))

(:derived (Flag509-)(and (Flag382-)))

(:derived (Flag509-)(and (Flag500-)))

(:derived (Flag509-)(and (Flag423-)))

(:derived (Flag509-)(and (Flag459-)))

(:derived (Flag509-)(and (Flag442-)))

(:derived (Flag509-)(and (Flag475-)))

(:derived (Flag509-)(and (Flag424-)))

(:derived (Flag509-)(and (Flag425-)))

(:derived (Flag509-)(and (Flag443-)))

(:derived (Flag509-)(and (Flag489-)))

(:derived (Flag509-)(and (Flag476-)))

(:derived (Flag509-)(and (Flag460-)))

(:derived (Flag509-)(and (Flag410-)))

(:derived (Flag509-)(and (Flag461-)))

(:derived (Flag509-)(and (Flag433-)))

(:derived (Flag509-)(and (Flag444-)))

(:derived (Flag509-)(and (Flag501-)))

(:derived (Flag509-)(and (Flag388-)))

(:derived (Flag509-)(and (Flag331-)))

(:derived (Flag509-)(and (Flag490-)))

(:derived (Flag509-)(and (Flag502-)))

(:derived (Flag509-)(and (Flag333-)))

(:derived (Flag509-)(and (Flag426-)))

(:derived (Flag509-)(and (Flag375-)))

(:derived (Flag509-)(and (Flag355-)))

(:derived (Flag509-)(and (Flag395-)))

(:derived (Flag509-)(and (Flag477-)))

(:derived (Flag509-)(and (Flag397-)))

(:derived (Flag509-)(and (Flag325-)))

(:derived (Flag509-)(and (Flag377-)))

(:derived (Flag509-)(and (Flag445-)))

(:derived (Flag509-)(and (Flag492-)))

(:derived (Flag509-)(and (Flag338-)))

(:derived (Flag509-)(and (Flag402-)))

(:derived (Flag509-)(and (Flag366-)))

(:derived (Flag509-)(and (Flag428-)))

(:derived (Flag509-)(and (Flag447-)))

(:derived (Flag509-)(and (Flag429-)))

(:derived (Flag509-)(and (Flag479-)))

(:derived (Flag509-)(and (Flag407-)))

(:derived (Flag509-)(and (Flag328-)))

(:derived (Flag509-)(and (Flag364-)))

(:derived (Flag509-)(and (Flag493-)))

(:derived (Flag509-)(and (Flag503-)))

(:derived (Flag509-)(and (Flag448-)))

(:derived (Flag509-)(and (Flag504-)))

(:derived (Flag509-)(and (Flag350-)))

(:derived (Flag509-)(and (Flag505-)))

(:derived (Flag509-)(and (Flag347-)))

(:derived (Flag509-)(and (Flag393-)))

(:derived (Flag509-)(and (Flag357-)))

(:derived (Flag509-)(and (Flag466-)))

(:derived (Flag509-)(and (Flag396-)))

(:derived (Flag509-)(and (Flag411-)))

(:derived (Flag509-)(and (Flag390-)))

(:derived (Flag509-)(and (Flag399-)))

(:derived (Flag509-)(and (Flag449-)))

(:derived (Flag509-)(and (Flag451-)))

(:derived (Flag509-)(and (Flag378-)))

(:derived (Flag509-)(and (Flag481-)))

(:derived (Flag509-)(and (Flag467-)))

(:derived (Flag509-)(and (Flag400-)))

(:derived (Flag509-)(and (Flag434-)))

(:derived (Flag509-)(and (Flag506-)))

(:derived (Flag509-)(and (Flag435-)))

(:derived (Flag509-)(and (Flag507-)))

(:derived (Flag509-)(and (Flag349-)))

(:derived (Flag509-)(and (Flag343-)))

(:derived (Flag509-)(and (Flag387-)))

(:derived (Flag509-)(and (Flag344-)))

(:derived (Flag509-)(and (Flag495-)))

(:derived (Flag509-)(and (Flag496-)))

(:derived (Flag509-)(and (Flag468-)))

(:derived (Flag509-)(and (Flag409-)))

(:derived (Flag509-)(and (Flag334-)))

(:derived (Flag509-)(and (Flag354-)))

(:derived (Flag509-)(and (Flag469-)))

(:derived (Flag509-)(and (Flag454-)))

(:derived (Flag509-)(and (Flag497-)))

(:derived (Flag509-)(and (Flag482-)))

(:derived (Flag509-)(and (Flag376-)))

(:derived (Flag509-)(and (Flag508-)))

(:derived (Flag509-)(and (Flag484-)))

(:derived (Flag519-)(and (Flag510-)))

(:derived (Flag519-)(and (Flag415-)))

(:derived (Flag519-)(and (Flag340-)))

(:derived (Flag519-)(and (Flag406-)))

(:derived (Flag519-)(and (Flag345-)))

(:derived (Flag519-)(and (Flag366-)))

(:derived (Flag519-)(and (Flag384-)))

(:derived (Flag519-)(and (Flag417-)))

(:derived (Flag519-)(and (Flag353-)))

(:derived (Flag519-)(and (Flag421-)))

(:derived (Flag519-)(and (Flag439-)))

(:derived (Flag519-)(and (Flag511-)))

(:derived (Flag519-)(and (Flag471-)))

(:derived (Flag519-)(and (Flag440-)))

(:derived (Flag519-)(and (Flag368-)))

(:derived (Flag519-)(and (Flag488-)))

(:derived (Flag519-)(and (Flag404-)))

(:derived (Flag519-)(and (Flag472-)))

(:derived (Flag519-)(and (Flag321-)))

(:derived (Flag519-)(and (Flag487-)))

(:derived (Flag519-)(and (Flag473-)))

(:derived (Flag519-)(and (Flag416-)))

(:derived (Flag519-)(and (Flag323-)))

(:derived (Flag519-)(and (Flag326-)))

(:derived (Flag519-)(and (Flag379-)))

(:derived (Flag519-)(and (Flag456-)))

(:derived (Flag519-)(and (Flag457-)))

(:derived (Flag519-)(and (Flag474-)))

(:derived (Flag519-)(and (Flag332-)))

(:derived (Flag519-)(and (Flag499-)))

(:derived (Flag519-)(and (Flag458-)))

(:derived (Flag519-)(and (Flag382-)))

(:derived (Flag519-)(and (Flag500-)))

(:derived (Flag519-)(and (Flag423-)))

(:derived (Flag519-)(and (Flag459-)))

(:derived (Flag519-)(and (Flag442-)))

(:derived (Flag519-)(and (Flag424-)))

(:derived (Flag519-)(and (Flag425-)))

(:derived (Flag519-)(and (Flag512-)))

(:derived (Flag519-)(and (Flag443-)))

(:derived (Flag519-)(and (Flag489-)))

(:derived (Flag519-)(and (Flag476-)))

(:derived (Flag519-)(and (Flag410-)))

(:derived (Flag519-)(and (Flag461-)))

(:derived (Flag519-)(and (Flag433-)))

(:derived (Flag519-)(and (Flag501-)))

(:derived (Flag519-)(and (Flag388-)))

(:derived (Flag519-)(and (Flag331-)))

(:derived (Flag519-)(and (Flag490-)))

(:derived (Flag519-)(and (Flag502-)))

(:derived (Flag519-)(and (Flag426-)))

(:derived (Flag519-)(and (Flag375-)))

(:derived (Flag519-)(and (Flag395-)))

(:derived (Flag519-)(and (Flag477-)))

(:derived (Flag519-)(and (Flag397-)))

(:derived (Flag519-)(and (Flag513-)))

(:derived (Flag519-)(and (Flag325-)))

(:derived (Flag519-)(and (Flag377-)))

(:derived (Flag519-)(and (Flag445-)))

(:derived (Flag519-)(and (Flag492-)))

(:derived (Flag519-)(and (Flag338-)))

(:derived (Flag519-)(and (Flag402-)))

(:derived (Flag519-)(and (Flag428-)))

(:derived (Flag519-)(and (Flag447-)))

(:derived (Flag519-)(and (Flag429-)))

(:derived (Flag519-)(and (Flag479-)))

(:derived (Flag519-)(and (Flag407-)))

(:derived (Flag519-)(and (Flag328-)))

(:derived (Flag519-)(and (Flag364-)))

(:derived (Flag519-)(and (Flag493-)))

(:derived (Flag519-)(and (Flag503-)))

(:derived (Flag519-)(and (Flag448-)))

(:derived (Flag519-)(and (Flag504-)))

(:derived (Flag519-)(and (Flag350-)))

(:derived (Flag519-)(and (Flag505-)))

(:derived (Flag519-)(and (Flag347-)))

(:derived (Flag519-)(and (Flag393-)))

(:derived (Flag519-)(and (Flag514-)))

(:derived (Flag519-)(and (Flag357-)))

(:derived (Flag519-)(and (Flag466-)))

(:derived (Flag519-)(and (Flag396-)))

(:derived (Flag519-)(and (Flag411-)))

(:derived (Flag519-)(and (Flag390-)))

(:derived (Flag519-)(and (Flag399-)))

(:derived (Flag519-)(and (Flag449-)))

(:derived (Flag519-)(and (Flag451-)))

(:derived (Flag519-)(and (Flag365-)))

(:derived (Flag519-)(and (Flag515-)))

(:derived (Flag519-)(and (Flag481-)))

(:derived (Flag519-)(and (Flag467-)))

(:derived (Flag519-)(and (Flag400-)))

(:derived (Flag519-)(and (Flag434-)))

(:derived (Flag519-)(and (Flag435-)))

(:derived (Flag519-)(and (Flag507-)))

(:derived (Flag519-)(and (Flag349-)))

(:derived (Flag519-)(and (Flag516-)))

(:derived (Flag519-)(and (Flag343-)))

(:derived (Flag519-)(and (Flag387-)))

(:derived (Flag519-)(and (Flag344-)))

(:derived (Flag519-)(and (Flag495-)))

(:derived (Flag519-)(and (Flag496-)))

(:derived (Flag519-)(and (Flag468-)))

(:derived (Flag519-)(and (Flag409-)))

(:derived (Flag519-)(and (Flag334-)))

(:derived (Flag519-)(and (Flag517-)))

(:derived (Flag519-)(and (Flag354-)))

(:derived (Flag519-)(and (Flag469-)))

(:derived (Flag519-)(and (Flag454-)))

(:derived (Flag519-)(and (Flag497-)))

(:derived (Flag519-)(and (Flag482-)))

(:derived (Flag519-)(and (Flag376-)))

(:derived (Flag519-)(and (Flag508-)))

(:derived (Flag519-)(and (Flag484-)))

(:derived (Flag519-)(and (Flag518-)))

(:derived (Flag529-)(and (Flag510-)))

(:derived (Flag529-)(and (Flag415-)))

(:derived (Flag529-)(and (Flag340-)))

(:derived (Flag529-)(and (Flag406-)))

(:derived (Flag529-)(and (Flag345-)))

(:derived (Flag529-)(and (Flag366-)))

(:derived (Flag529-)(and (Flag384-)))

(:derived (Flag529-)(and (Flag347-)))

(:derived (Flag529-)(and (Flag353-)))

(:derived (Flag529-)(and (Flag421-)))

(:derived (Flag529-)(and (Flag439-)))

(:derived (Flag529-)(and (Flag511-)))

(:derived (Flag529-)(and (Flag471-)))

(:derived (Flag529-)(and (Flag520-)))

(:derived (Flag529-)(and (Flag440-)))

(:derived (Flag529-)(and (Flag368-)))

(:derived (Flag529-)(and (Flag488-)))

(:derived (Flag529-)(and (Flag404-)))

(:derived (Flag529-)(and (Flag521-)))

(:derived (Flag529-)(and (Flag472-)))

(:derived (Flag529-)(and (Flag321-)))

(:derived (Flag529-)(and (Flag487-)))

(:derived (Flag529-)(and (Flag500-)))

(:derived (Flag529-)(and (Flag416-)))

(:derived (Flag529-)(and (Flag323-)))

(:derived (Flag529-)(and (Flag326-)))

(:derived (Flag529-)(and (Flag379-)))

(:derived (Flag529-)(and (Flag457-)))

(:derived (Flag529-)(and (Flag474-)))

(:derived (Flag529-)(and (Flag332-)))

(:derived (Flag529-)(and (Flag499-)))

(:derived (Flag529-)(and (Flag458-)))

(:derived (Flag529-)(and (Flag382-)))

(:derived (Flag529-)(and (Flag459-)))

(:derived (Flag529-)(and (Flag442-)))

(:derived (Flag529-)(and (Flag424-)))

(:derived (Flag529-)(and (Flag425-)))

(:derived (Flag529-)(and (Flag512-)))

(:derived (Flag529-)(and (Flag443-)))

(:derived (Flag529-)(and (Flag489-)))

(:derived (Flag529-)(and (Flag476-)))

(:derived (Flag529-)(and (Flag410-)))

(:derived (Flag529-)(and (Flag461-)))

(:derived (Flag529-)(and (Flag522-)))

(:derived (Flag529-)(and (Flag433-)))

(:derived (Flag529-)(and (Flag501-)))

(:derived (Flag529-)(and (Flag388-)))

(:derived (Flag529-)(and (Flag523-)))

(:derived (Flag529-)(and (Flag331-)))

(:derived (Flag529-)(and (Flag490-)))

(:derived (Flag529-)(and (Flag502-)))

(:derived (Flag529-)(and (Flag426-)))

(:derived (Flag529-)(and (Flag375-)))

(:derived (Flag529-)(and (Flag395-)))

(:derived (Flag529-)(and (Flag477-)))

(:derived (Flag529-)(and (Flag357-)))

(:derived (Flag529-)(and (Flag513-)))

(:derived (Flag529-)(and (Flag325-)))

(:derived (Flag529-)(and (Flag377-)))

(:derived (Flag529-)(and (Flag445-)))

(:derived (Flag529-)(and (Flag492-)))

(:derived (Flag529-)(and (Flag338-)))

(:derived (Flag529-)(and (Flag402-)))

(:derived (Flag529-)(and (Flag428-)))

(:derived (Flag529-)(and (Flag447-)))

(:derived (Flag529-)(and (Flag429-)))

(:derived (Flag529-)(and (Flag524-)))

(:derived (Flag529-)(and (Flag479-)))

(:derived (Flag529-)(and (Flag407-)))

(:derived (Flag529-)(and (Flag525-)))

(:derived (Flag529-)(and (Flag364-)))

(:derived (Flag529-)(and (Flag493-)))

(:derived (Flag529-)(and (Flag503-)))

(:derived (Flag529-)(and (Flag448-)))

(:derived (Flag529-)(and (Flag504-)))

(:derived (Flag529-)(and (Flag350-)))

(:derived (Flag529-)(and (Flag505-)))

(:derived (Flag529-)(and (Flag393-)))

(:derived (Flag529-)(and (Flag466-)))

(:derived (Flag529-)(and (Flag396-)))

(:derived (Flag529-)(and (Flag411-)))

(:derived (Flag529-)(and (Flag390-)))

(:derived (Flag529-)(and (Flag449-)))

(:derived (Flag529-)(and (Flag365-)))

(:derived (Flag529-)(and (Flag515-)))

(:derived (Flag529-)(and (Flag481-)))

(:derived (Flag529-)(and (Flag467-)))

(:derived (Flag529-)(and (Flag400-)))

(:derived (Flag529-)(and (Flag434-)))

(:derived (Flag529-)(and (Flag435-)))

(:derived (Flag529-)(and (Flag349-)))

(:derived (Flag529-)(and (Flag343-)))

(:derived (Flag529-)(and (Flag387-)))

(:derived (Flag529-)(and (Flag344-)))

(:derived (Flag529-)(and (Flag495-)))

(:derived (Flag529-)(and (Flag496-)))

(:derived (Flag529-)(and (Flag526-)))

(:derived (Flag529-)(and (Flag409-)))

(:derived (Flag529-)(and (Flag334-)))

(:derived (Flag529-)(and (Flag527-)))

(:derived (Flag529-)(and (Flag517-)))

(:derived (Flag529-)(and (Flag469-)))

(:derived (Flag529-)(and (Flag454-)))

(:derived (Flag529-)(and (Flag482-)))

(:derived (Flag529-)(and (Flag376-)))

(:derived (Flag529-)(and (Flag508-)))

(:derived (Flag529-)(and (Flag484-)))

(:derived (Flag529-)(and (Flag528-)))

(:derived (Flag529-)(and (Flag518-)))

(:derived (Flag538-)(and (Flag510-)))

(:derived (Flag538-)(and (Flag415-)))

(:derived (Flag538-)(and (Flag340-)))

(:derived (Flag538-)(and (Flag406-)))

(:derived (Flag538-)(and (Flag530-)))

(:derived (Flag538-)(and (Flag345-)))

(:derived (Flag538-)(and (Flag366-)))

(:derived (Flag538-)(and (Flag384-)))

(:derived (Flag538-)(and (Flag347-)))

(:derived (Flag538-)(and (Flag353-)))

(:derived (Flag538-)(and (Flag421-)))

(:derived (Flag538-)(and (Flag439-)))

(:derived (Flag538-)(and (Flag511-)))

(:derived (Flag538-)(and (Flag471-)))

(:derived (Flag538-)(and (Flag520-)))

(:derived (Flag538-)(and (Flag440-)))

(:derived (Flag538-)(and (Flag368-)))

(:derived (Flag538-)(and (Flag404-)))

(:derived (Flag538-)(and (Flag521-)))

(:derived (Flag538-)(and (Flag472-)))

(:derived (Flag538-)(and (Flag321-)))

(:derived (Flag538-)(and (Flag487-)))

(:derived (Flag538-)(and (Flag500-)))

(:derived (Flag538-)(and (Flag323-)))

(:derived (Flag538-)(and (Flag326-)))

(:derived (Flag538-)(and (Flag379-)))

(:derived (Flag538-)(and (Flag457-)))

(:derived (Flag538-)(and (Flag332-)))

(:derived (Flag538-)(and (Flag499-)))

(:derived (Flag538-)(and (Flag458-)))

(:derived (Flag538-)(and (Flag382-)))

(:derived (Flag538-)(and (Flag459-)))

(:derived (Flag538-)(and (Flag442-)))

(:derived (Flag538-)(and (Flag425-)))

(:derived (Flag538-)(and (Flag512-)))

(:derived (Flag538-)(and (Flag443-)))

(:derived (Flag538-)(and (Flag489-)))

(:derived (Flag538-)(and (Flag476-)))

(:derived (Flag538-)(and (Flag461-)))

(:derived (Flag538-)(and (Flag522-)))

(:derived (Flag538-)(and (Flag433-)))

(:derived (Flag538-)(and (Flag501-)))

(:derived (Flag538-)(and (Flag388-)))

(:derived (Flag538-)(and (Flag523-)))

(:derived (Flag538-)(and (Flag331-)))

(:derived (Flag538-)(and (Flag490-)))

(:derived (Flag538-)(and (Flag502-)))

(:derived (Flag538-)(and (Flag426-)))

(:derived (Flag538-)(and (Flag375-)))

(:derived (Flag538-)(and (Flag395-)))

(:derived (Flag538-)(and (Flag477-)))

(:derived (Flag538-)(and (Flag531-)))

(:derived (Flag538-)(and (Flag513-)))

(:derived (Flag538-)(and (Flag377-)))

(:derived (Flag538-)(and (Flag445-)))

(:derived (Flag538-)(and (Flag492-)))

(:derived (Flag538-)(and (Flag338-)))

(:derived (Flag538-)(and (Flag402-)))

(:derived (Flag538-)(and (Flag428-)))

(:derived (Flag538-)(and (Flag447-)))

(:derived (Flag538-)(and (Flag429-)))

(:derived (Flag538-)(and (Flag479-)))

(:derived (Flag538-)(and (Flag407-)))

(:derived (Flag538-)(and (Flag532-)))

(:derived (Flag538-)(and (Flag525-)))

(:derived (Flag538-)(and (Flag364-)))

(:derived (Flag538-)(and (Flag503-)))

(:derived (Flag538-)(and (Flag448-)))

(:derived (Flag538-)(and (Flag533-)))

(:derived (Flag538-)(and (Flag504-)))

(:derived (Flag538-)(and (Flag350-)))

(:derived (Flag538-)(and (Flag505-)))

(:derived (Flag538-)(and (Flag393-)))

(:derived (Flag538-)(and (Flag534-)))

(:derived (Flag538-)(and (Flag535-)))

(:derived (Flag538-)(and (Flag466-)))

(:derived (Flag538-)(and (Flag396-)))

(:derived (Flag538-)(and (Flag411-)))

(:derived (Flag538-)(and (Flag536-)))

(:derived (Flag538-)(and (Flag449-)))

(:derived (Flag538-)(and (Flag365-)))

(:derived (Flag538-)(and (Flag515-)))

(:derived (Flag538-)(and (Flag481-)))

(:derived (Flag538-)(and (Flag467-)))

(:derived (Flag538-)(and (Flag400-)))

(:derived (Flag538-)(and (Flag434-)))

(:derived (Flag538-)(and (Flag435-)))

(:derived (Flag538-)(and (Flag349-)))

(:derived (Flag538-)(and (Flag343-)))

(:derived (Flag538-)(and (Flag387-)))

(:derived (Flag538-)(and (Flag344-)))

(:derived (Flag538-)(and (Flag495-)))

(:derived (Flag538-)(and (Flag496-)))

(:derived (Flag538-)(and (Flag526-)))

(:derived (Flag538-)(and (Flag409-)))

(:derived (Flag538-)(and (Flag537-)))

(:derived (Flag538-)(and (Flag334-)))

(:derived (Flag538-)(and (Flag517-)))

(:derived (Flag538-)(and (Flag469-)))

(:derived (Flag538-)(and (Flag493-)))

(:derived (Flag538-)(and (Flag482-)))

(:derived (Flag538-)(and (Flag518-)))

(:derived (Flag545-)(and (Flag510-)))

(:derived (Flag545-)(and (Flag415-)))

(:derived (Flag545-)(and (Flag340-)))

(:derived (Flag545-)(and (Flag406-)))

(:derived (Flag545-)(and (Flag530-)))

(:derived (Flag545-)(and (Flag366-)))

(:derived (Flag545-)(and (Flag384-)))

(:derived (Flag545-)(and (Flag347-)))

(:derived (Flag545-)(and (Flag353-)))

(:derived (Flag545-)(and (Flag421-)))

(:derived (Flag545-)(and (Flag439-)))

(:derived (Flag545-)(and (Flag471-)))

(:derived (Flag545-)(and (Flag520-)))

(:derived (Flag545-)(and (Flag440-)))

(:derived (Flag545-)(and (Flag368-)))

(:derived (Flag545-)(and (Flag404-)))

(:derived (Flag545-)(and (Flag521-)))

(:derived (Flag545-)(and (Flag321-)))

(:derived (Flag545-)(and (Flag487-)))

(:derived (Flag545-)(and (Flag500-)))

(:derived (Flag545-)(and (Flag323-)))

(:derived (Flag545-)(and (Flag379-)))

(:derived (Flag545-)(and (Flag457-)))

(:derived (Flag545-)(and (Flag539-)))

(:derived (Flag545-)(and (Flag332-)))

(:derived (Flag545-)(and (Flag499-)))

(:derived (Flag545-)(and (Flag458-)))

(:derived (Flag545-)(and (Flag442-)))

(:derived (Flag545-)(and (Flag540-)))

(:derived (Flag545-)(and (Flag512-)))

(:derived (Flag545-)(and (Flag541-)))

(:derived (Flag545-)(and (Flag443-)))

(:derived (Flag545-)(and (Flag489-)))

(:derived (Flag545-)(and (Flag476-)))

(:derived (Flag545-)(and (Flag461-)))

(:derived (Flag545-)(and (Flag522-)))

(:derived (Flag545-)(and (Flag433-)))

(:derived (Flag545-)(and (Flag501-)))

(:derived (Flag545-)(and (Flag388-)))

(:derived (Flag545-)(and (Flag523-)))

(:derived (Flag545-)(and (Flag331-)))

(:derived (Flag545-)(and (Flag490-)))

(:derived (Flag545-)(and (Flag502-)))

(:derived (Flag545-)(and (Flag426-)))

(:derived (Flag545-)(and (Flag375-)))

(:derived (Flag545-)(and (Flag395-)))

(:derived (Flag545-)(and (Flag477-)))

(:derived (Flag545-)(and (Flag531-)))

(:derived (Flag545-)(and (Flag513-)))

(:derived (Flag545-)(and (Flag445-)))

(:derived (Flag545-)(and (Flag542-)))

(:derived (Flag545-)(and (Flag334-)))

(:derived (Flag545-)(and (Flag338-)))

(:derived (Flag545-)(and (Flag402-)))

(:derived (Flag545-)(and (Flag428-)))

(:derived (Flag545-)(and (Flag429-)))

(:derived (Flag545-)(and (Flag479-)))

(:derived (Flag545-)(and (Flag407-)))

(:derived (Flag545-)(and (Flag532-)))

(:derived (Flag545-)(and (Flag525-)))

(:derived (Flag545-)(and (Flag364-)))

(:derived (Flag545-)(and (Flag448-)))

(:derived (Flag545-)(and (Flag504-)))

(:derived (Flag545-)(and (Flag350-)))

(:derived (Flag545-)(and (Flag505-)))

(:derived (Flag545-)(and (Flag393-)))

(:derived (Flag545-)(and (Flag535-)))

(:derived (Flag545-)(and (Flag466-)))

(:derived (Flag545-)(and (Flag396-)))

(:derived (Flag545-)(and (Flag411-)))

(:derived (Flag545-)(and (Flag536-)))

(:derived (Flag545-)(and (Flag543-)))

(:derived (Flag545-)(and (Flag449-)))

(:derived (Flag545-)(and (Flag365-)))

(:derived (Flag545-)(and (Flag515-)))

(:derived (Flag545-)(and (Flag481-)))

(:derived (Flag545-)(and (Flag467-)))

(:derived (Flag545-)(and (Flag400-)))

(:derived (Flag545-)(and (Flag434-)))

(:derived (Flag545-)(and (Flag495-)))

(:derived (Flag545-)(and (Flag349-)))

(:derived (Flag545-)(and (Flag343-)))

(:derived (Flag545-)(and (Flag387-)))

(:derived (Flag545-)(and (Flag344-)))

(:derived (Flag545-)(and (Flag544-)))

(:derived (Flag545-)(and (Flag496-)))

(:derived (Flag545-)(and (Flag526-)))

(:derived (Flag545-)(and (Flag409-)))

(:derived (Flag545-)(and (Flag537-)))

(:derived (Flag545-)(and (Flag517-)))

(:derived (Flag545-)(and (Flag469-)))

(:derived (Flag545-)(and (Flag493-)))

(:derived (Flag545-)(and (Flag482-)))

(:derived (Flag545-)(and (Flag518-)))

(:derived (Flag551-)(and (Flag510-)))

(:derived (Flag551-)(and (Flag340-)))

(:derived (Flag551-)(and (Flag347-)))

(:derived (Flag551-)(and (Flag332-)))

(:derived (Flag551-)(and (Flag499-)))

(:derived (Flag551-)(and (Flag458-)))

(:derived (Flag551-)(and (Flag543-)))

(:derived (Flag551-)(and (Flag434-)))

(:derived (Flag551-)(and (Flag402-)))

(:derived (Flag551-)(and (Flag415-)))

(:derived (Flag551-)(and (Flag442-)))

(:derived (Flag551-)(and (Flag512-)))

(:derived (Flag551-)(and (Flag440-)))

(:derived (Flag551-)(and (Flag479-)))

(:derived (Flag551-)(and (Flag406-)))

(:derived (Flag551-)(and (Flag407-)))

(:derived (Flag551-)(and (Flag530-)))

(:derived (Flag551-)(and (Flag495-)))

(:derived (Flag551-)(and (Flag349-)))

(:derived (Flag551-)(and (Flag541-)))

(:derived (Flag551-)(and (Flag443-)))

(:derived (Flag551-)(and (Flag364-)))

(:derived (Flag551-)(and (Flag387-)))

(:derived (Flag551-)(and (Flag344-)))

(:derived (Flag551-)(and (Flag388-)))

(:derived (Flag551-)(and (Flag544-)))

(:derived (Flag551-)(and (Flag343-)))

(:derived (Flag551-)(and (Flag366-)))

(:derived (Flag551-)(and (Flag384-)))

(:derived (Flag551-)(and (Flag496-)))

(:derived (Flag551-)(and (Flag536-)))

(:derived (Flag551-)(and (Flag448-)))

(:derived (Flag551-)(and (Flag461-)))

(:derived (Flag551-)(and (Flag323-)))

(:derived (Flag551-)(and (Flag353-)))

(:derived (Flag551-)(and (Flag421-)))

(:derived (Flag551-)(and (Flag526-)))

(:derived (Flag551-)(and (Flag546-)))

(:derived (Flag551-)(and (Flag522-)))

(:derived (Flag551-)(and (Flag433-)))

(:derived (Flag551-)(and (Flag537-)))

(:derived (Flag551-)(and (Flag471-)))

(:derived (Flag551-)(and (Flag520-)))

(:derived (Flag551-)(and (Flag505-)))

(:derived (Flag551-)(and (Flag547-)))

(:derived (Flag551-)(and (Flag501-)))

(:derived (Flag551-)(and (Flag321-)))

(:derived (Flag551-)(and (Flag523-)))

(:derived (Flag551-)(and (Flag393-)))

(:derived (Flag551-)(and (Flag368-)))

(:derived (Flag551-)(and (Flag487-)))

(:derived (Flag551-)(and (Flag477-)))

(:derived (Flag551-)(and (Flag517-)))

(:derived (Flag551-)(and (Flag490-)))

(:derived (Flag551-)(and (Flag502-)))

(:derived (Flag551-)(and (Flag535-)))

(:derived (Flag551-)(and (Flag426-)))

(:derived (Flag551-)(and (Flag469-)))

(:derived (Flag551-)(and (Flag482-)))

(:derived (Flag551-)(and (Flag404-)))

(:derived (Flag551-)(and (Flag521-)))

(:derived (Flag551-)(and (Flag493-)))

(:derived (Flag551-)(and (Flag375-)))

(:derived (Flag551-)(and (Flag395-)))

(:derived (Flag551-)(and (Flag396-)))

(:derived (Flag551-)(and (Flag338-)))

(:derived (Flag551-)(and (Flag411-)))

(:derived (Flag551-)(and (Flag548-)))

(:derived (Flag551-)(and (Flag531-)))

(:derived (Flag551-)(and (Flag549-)))

(:derived (Flag551-)(and (Flag513-)))

(:derived (Flag551-)(and (Flag449-)))

(:derived (Flag551-)(and (Flag500-)))

(:derived (Flag551-)(and (Flag428-)))

(:derived (Flag551-)(and (Flag445-)))

(:derived (Flag551-)(and (Flag550-)))

(:derived (Flag551-)(and (Flag542-)))

(:derived (Flag551-)(and (Flag481-)))

(:derived (Flag551-)(and (Flag334-)))

(:derived (Flag551-)(and (Flag467-)))

(:derived (Flag551-)(and (Flag379-)))

(:derived (Flag551-)(and (Flag518-)))

(:derived (Flag551-)(and (Flag466-)))

(:derived (Flag551-)(and (Flag539-)))

(:derived (Flag556-)(and (Flag510-)))

(:derived (Flag556-)(and (Flag340-)))

(:derived (Flag556-)(and (Flag347-)))

(:derived (Flag556-)(and (Flag332-)))

(:derived (Flag556-)(and (Flag499-)))

(:derived (Flag556-)(and (Flag458-)))

(:derived (Flag556-)(and (Flag543-)))

(:derived (Flag556-)(and (Flag434-)))

(:derived (Flag556-)(and (Flag493-)))

(:derived (Flag556-)(and (Flag415-)))

(:derived (Flag556-)(and (Flag442-)))

(:derived (Flag556-)(and (Flag552-)))

(:derived (Flag556-)(and (Flag396-)))

(:derived (Flag556-)(and (Flag479-)))

(:derived (Flag556-)(and (Flag406-)))

(:derived (Flag556-)(and (Flag407-)))

(:derived (Flag556-)(and (Flag402-)))

(:derived (Flag556-)(and (Flag553-)))

(:derived (Flag556-)(and (Flag349-)))

(:derived (Flag556-)(and (Flag443-)))

(:derived (Flag556-)(and (Flag364-)))

(:derived (Flag556-)(and (Flag387-)))

(:derived (Flag556-)(and (Flag344-)))

(:derived (Flag556-)(and (Flag544-)))

(:derived (Flag556-)(and (Flag366-)))

(:derived (Flag556-)(and (Flag384-)))

(:derived (Flag556-)(and (Flag496-)))

(:derived (Flag556-)(and (Flag536-)))

(:derived (Flag556-)(and (Flag448-)))

(:derived (Flag556-)(and (Flag461-)))

(:derived (Flag556-)(and (Flag323-)))

(:derived (Flag556-)(and (Flag353-)))

(:derived (Flag556-)(and (Flag421-)))

(:derived (Flag556-)(and (Flag546-)))

(:derived (Flag556-)(and (Flag522-)))

(:derived (Flag556-)(and (Flag537-)))

(:derived (Flag556-)(and (Flag471-)))

(:derived (Flag556-)(and (Flag520-)))

(:derived (Flag556-)(and (Flag505-)))

(:derived (Flag556-)(and (Flag547-)))

(:derived (Flag556-)(and (Flag321-)))

(:derived (Flag556-)(and (Flag523-)))

(:derived (Flag556-)(and (Flag393-)))

(:derived (Flag556-)(and (Flag487-)))

(:derived (Flag556-)(and (Flag517-)))

(:derived (Flag556-)(and (Flag490-)))

(:derived (Flag556-)(and (Flag502-)))

(:derived (Flag556-)(and (Flag535-)))

(:derived (Flag556-)(and (Flag426-)))

(:derived (Flag556-)(and (Flag469-)))

(:derived (Flag556-)(and (Flag521-)))

(:derived (Flag556-)(and (Flag554-)))

(:derived (Flag556-)(and (Flag375-)))

(:derived (Flag556-)(and (Flag513-)))

(:derived (Flag556-)(and (Flag477-)))

(:derived (Flag556-)(and (Flag338-)))

(:derived (Flag556-)(and (Flag411-)))

(:derived (Flag556-)(and (Flag548-)))

(:derived (Flag556-)(and (Flag531-)))

(:derived (Flag556-)(and (Flag482-)))

(:derived (Flag556-)(and (Flag555-)))

(:derived (Flag556-)(and (Flag449-)))

(:derived (Flag556-)(and (Flag500-)))

(:derived (Flag556-)(and (Flag445-)))

(:derived (Flag556-)(and (Flag550-)))

(:derived (Flag556-)(and (Flag542-)))

(:derived (Flag556-)(and (Flag433-)))

(:derived (Flag556-)(and (Flag379-)))

(:derived (Flag556-)(and (Flag518-)))

(:derived (Flag556-)(and (Flag466-)))

(:derived (Flag556-)(and (Flag539-)))

(:derived (Flag560-)(and (Flag510-)))

(:derived (Flag560-)(and (Flag557-)))

(:derived (Flag560-)(and (Flag458-)))

(:derived (Flag560-)(and (Flag543-)))

(:derived (Flag560-)(and (Flag434-)))

(:derived (Flag560-)(and (Flag402-)))

(:derived (Flag560-)(and (Flag415-)))

(:derived (Flag560-)(and (Flag552-)))

(:derived (Flag560-)(and (Flag396-)))

(:derived (Flag560-)(and (Flag406-)))

(:derived (Flag560-)(and (Flag349-)))

(:derived (Flag560-)(and (Flag443-)))

(:derived (Flag560-)(and (Flag364-)))

(:derived (Flag560-)(and (Flag387-)))

(:derived (Flag560-)(and (Flag344-)))

(:derived (Flag560-)(and (Flag384-)))

(:derived (Flag560-)(and (Flag496-)))

(:derived (Flag560-)(and (Flag448-)))

(:derived (Flag560-)(and (Flag558-)))

(:derived (Flag560-)(and (Flag461-)))

(:derived (Flag560-)(and (Flag323-)))

(:derived (Flag560-)(and (Flag353-)))

(:derived (Flag560-)(and (Flag421-)))

(:derived (Flag560-)(and (Flag546-)))

(:derived (Flag560-)(and (Flag522-)))

(:derived (Flag560-)(and (Flag537-)))

(:derived (Flag560-)(and (Flag559-)))

(:derived (Flag560-)(and (Flag520-)))

(:derived (Flag560-)(and (Flag505-)))

(:derived (Flag560-)(and (Flag340-)))

(:derived (Flag560-)(and (Flag393-)))

(:derived (Flag560-)(and (Flag487-)))

(:derived (Flag560-)(and (Flag523-)))

(:derived (Flag560-)(and (Flag490-)))

(:derived (Flag560-)(and (Flag502-)))

(:derived (Flag560-)(and (Flag535-)))

(:derived (Flag560-)(and (Flag426-)))

(:derived (Flag560-)(and (Flag554-)))

(:derived (Flag560-)(and (Flag375-)))

(:derived (Flag560-)(and (Flag555-)))

(:derived (Flag560-)(and (Flag477-)))

(:derived (Flag560-)(and (Flag321-)))

(:derived (Flag560-)(and (Flag548-)))

(:derived (Flag560-)(and (Flag536-)))

(:derived (Flag560-)(and (Flag482-)))

(:derived (Flag560-)(and (Flag513-)))

(:derived (Flag560-)(and (Flag449-)))

(:derived (Flag560-)(and (Flag500-)))

(:derived (Flag560-)(and (Flag445-)))

(:derived (Flag560-)(and (Flag550-)))

(:derived (Flag560-)(and (Flag542-)))

(:derived (Flag560-)(and (Flag379-)))

(:derived (Flag560-)(and (Flag518-)))

(:derived (Flag560-)(and (Flag466-)))

(:derived (Flag560-)(and (Flag471-)))

(:derived (Flag560-)(and (Flag539-)))

(:derived (Flag563-)(and (Flag510-)))

(:derived (Flag563-)(and (Flag557-)))

(:derived (Flag563-)(and (Flag543-)))

(:derived (Flag563-)(and (Flag434-)))

(:derived (Flag563-)(and (Flag415-)))

(:derived (Flag563-)(and (Flag552-)))

(:derived (Flag563-)(and (Flag406-)))

(:derived (Flag563-)(and (Flag443-)))

(:derived (Flag563-)(and (Flag387-)))

(:derived (Flag563-)(and (Flag344-)))

(:derived (Flag563-)(and (Flag448-)))

(:derived (Flag563-)(and (Flag561-)))

(:derived (Flag563-)(and (Flag461-)))

(:derived (Flag563-)(and (Flag396-)))

(:derived (Flag563-)(and (Flag522-)))

(:derived (Flag563-)(and (Flag559-)))

(:derived (Flag563-)(and (Flag520-)))

(:derived (Flag563-)(and (Flag505-)))

(:derived (Flag563-)(and (Flag340-)))

(:derived (Flag563-)(and (Flag393-)))

(:derived (Flag563-)(and (Flag548-)))

(:derived (Flag563-)(and (Flag490-)))

(:derived (Flag563-)(and (Flag535-)))

(:derived (Flag563-)(and (Flag466-)))

(:derived (Flag563-)(and (Flag562-)))

(:derived (Flag563-)(and (Flag375-)))

(:derived (Flag563-)(and (Flag477-)))

(:derived (Flag563-)(and (Flag321-)))

(:derived (Flag563-)(and (Flag487-)))

(:derived (Flag563-)(and (Flag536-)))

(:derived (Flag563-)(and (Flag482-)))

(:derived (Flag563-)(and (Flag555-)))

(:derived (Flag563-)(and (Flag500-)))

(:derived (Flag563-)(and (Flag445-)))

(:derived (Flag563-)(and (Flag550-)))

(:derived (Flag563-)(and (Flag323-)))

(:derived (Flag563-)(and (Flag379-)))

(:derived (Flag563-)(and (Flag518-)))

(:derived (Flag563-)(and (Flag539-)))

(:derived (Flag564-)(and (RIGHTOF3-ROBOT)))

(:derived (Flag564-)(and (RIGHTOF8-ROBOT)))

(:derived (Flag564-)(and (RIGHTOF10-ROBOT)))

(:derived (Flag564-)(and (RIGHTOF5-ROBOT)))

(:derived (Flag564-)(and (RIGHTOF9-ROBOT)))

(:derived (Flag564-)(and (RIGHTOF19-ROBOT)))

(:derived (Flag564-)(and (RIGHTOF17-ROBOT)))

(:derived (Flag564-)(and (RIGHTOF14-ROBOT)))

(:derived (Flag564-)(and (RIGHTOF13-ROBOT)))

(:derived (Flag564-)(and (RIGHTOF12-ROBOT)))

(:derived (Flag564-)(and (RIGHTOF0-ROBOT)))

(:derived (Flag564-)(and (RIGHTOF4-ROBOT)))

(:derived (Flag564-)(and (RIGHTOF1-ROBOT)))

(:derived (Flag564-)(and (RIGHTOF16-ROBOT)))

(:derived (Flag564-)(and (RIGHTOF18-ROBOT)))

(:derived (Flag564-)(and (RIGHTOF7-ROBOT)))

(:derived (Flag564-)(and (RIGHTOF11-ROBOT)))

(:derived (Flag564-)(and (RIGHTOF15-ROBOT)))

(:derived (Flag564-)(and (RIGHTOF6-ROBOT)))

(:derived (Flag565-)(and (RIGHTOF0-ROBOT)(LEFTOF1-ROBOT)))

(:derived (Flag570-)(and (Flag543-)))

(:derived (Flag570-)(and (Flag548-)))

(:derived (Flag570-)(and (Flag557-)))

(:derived (Flag570-)(and (Flag393-)))

(:derived (Flag570-)(and (Flag568-)))

(:derived (Flag570-)(and (Flag569-)))

(:derived (Flag570-)(and (Flag344-)))

(:derived (Flag570-)(and (Flag448-)))

(:derived (Flag570-)(and (Flag434-)))

(:derived (Flag570-)(and (Flag490-)))

(:derived (Flag570-)(and (Flag505-)))

(:derived (Flag570-)(and (Flag535-)))

(:derived (Flag570-)(and (Flag323-)))

(:derived (Flag570-)(and (Flag466-)))

(:derived (Flag570-)(and (Flag518-)))

(:derived (Flag570-)(and (Flag477-)))

(:derived (Flag570-)(and (Flag406-)))

(:derived (Flag570-)(and (Flag522-)))

(:derived (Flag570-)(and (Flag555-)))

(:derived (Flag570-)(and (Flag396-)))

(:action MOVERIGHT-ROBOT
:parameters ()
:precondition
(and
(Flag4-)
)
:effect
(and
(when
(and
(Flag281-)
)
(and
(COLUMN19-ROBOT)
(not (NOT-COLUMN19-ROBOT))
)
)
(when
(and
(Flag278-)
)
(and
(COLUMN18-ROBOT)
(not (NOT-COLUMN18-ROBOT))
)
)
(when
(and
(Flag274-)
)
(and
(COLUMN17-ROBOT)
(not (NOT-COLUMN17-ROBOT))
)
)
(when
(and
(Flag269-)
)
(and
(COLUMN16-ROBOT)
(not (NOT-COLUMN16-ROBOT))
)
)
(when
(and
(Flag262-)
)
(and
(COLUMN15-ROBOT)
(not (NOT-COLUMN15-ROBOT))
)
)
(when
(and
(Flag254-)
)
(and
(COLUMN14-ROBOT)
(not (NOT-COLUMN14-ROBOT))
)
)
(when
(and
(Flag246-)
)
(and
(COLUMN13-ROBOT)
(not (NOT-COLUMN13-ROBOT))
)
)
(when
(and
(Flag236-)
)
(and
(COLUMN12-ROBOT)
(not (NOT-COLUMN12-ROBOT))
)
)
(when
(and
(Flag226-)
)
(and
(COLUMN11-ROBOT)
(not (NOT-COLUMN11-ROBOT))
)
)
(when
(and
(Flag214-)
)
(and
(COLUMN10-ROBOT)
(not (NOT-COLUMN10-ROBOT))
)
)
(when
(and
(Flag202-)
)
(and
(COLUMN9-ROBOT)
(not (NOT-COLUMN9-ROBOT))
)
)
(when
(and
(Flag188-)
)
(and
(COLUMN8-ROBOT)
(not (NOT-COLUMN8-ROBOT))
)
)
(when
(and
(Flag173-)
)
(and
(COLUMN7-ROBOT)
(not (NOT-COLUMN7-ROBOT))
)
)
(when
(and
(Flag156-)
)
(and
(COLUMN6-ROBOT)
(not (NOT-COLUMN6-ROBOT))
)
)
(when
(and
(Flag140-)
)
(and
(COLUMN5-ROBOT)
(not (NOT-COLUMN5-ROBOT))
)
)
(when
(and
(Flag120-)
)
(and
(COLUMN4-ROBOT)
(not (NOT-COLUMN4-ROBOT))
)
)
(when
(and
(Flag100-)
)
(and
(COLUMN3-ROBOT)
(not (NOT-COLUMN3-ROBOT))
)
)
(when
(and
(Flag81-)
)
(and
(COLUMN2-ROBOT)
(not (NOT-COLUMN2-ROBOT))
)
)
(when
(and
(Flag566-)
)
(and
(COLUMN1-ROBOT)
(not (NOT-COLUMN1-ROBOT))
)
)
(when
(and
(COLUMN18-ROBOT)
)
(and
(COLUMN19-ROBOT)
(NOT-COLUMN18-ROBOT)
(not (NOT-COLUMN19-ROBOT))
(not (COLUMN18-ROBOT))
)
)
(when
(and
(COLUMN17-ROBOT)
)
(and
(COLUMN18-ROBOT)
(NOT-COLUMN17-ROBOT)
(not (NOT-COLUMN18-ROBOT))
(not (COLUMN17-ROBOT))
)
)
(when
(and
(COLUMN16-ROBOT)
)
(and
(COLUMN17-ROBOT)
(NOT-COLUMN16-ROBOT)
(not (NOT-COLUMN17-ROBOT))
(not (COLUMN16-ROBOT))
)
)
(when
(and
(COLUMN15-ROBOT)
)
(and
(COLUMN16-ROBOT)
(NOT-COLUMN15-ROBOT)
(not (NOT-COLUMN16-ROBOT))
(not (COLUMN15-ROBOT))
)
)
(when
(and
(COLUMN14-ROBOT)
)
(and
(COLUMN15-ROBOT)
(NOT-COLUMN14-ROBOT)
(not (NOT-COLUMN15-ROBOT))
(not (COLUMN14-ROBOT))
)
)
(when
(and
(COLUMN13-ROBOT)
)
(and
(COLUMN14-ROBOT)
(NOT-COLUMN13-ROBOT)
(not (NOT-COLUMN14-ROBOT))
(not (COLUMN13-ROBOT))
)
)
(when
(and
(COLUMN12-ROBOT)
)
(and
(COLUMN13-ROBOT)
(NOT-COLUMN12-ROBOT)
(not (NOT-COLUMN13-ROBOT))
(not (COLUMN12-ROBOT))
)
)
(when
(and
(COLUMN11-ROBOT)
)
(and
(COLUMN12-ROBOT)
(NOT-COLUMN11-ROBOT)
(not (NOT-COLUMN12-ROBOT))
(not (COLUMN11-ROBOT))
)
)
(when
(and
(COLUMN10-ROBOT)
)
(and
(COLUMN11-ROBOT)
(NOT-COLUMN10-ROBOT)
(not (NOT-COLUMN11-ROBOT))
(not (COLUMN10-ROBOT))
)
)
(when
(and
(COLUMN9-ROBOT)
)
(and
(COLUMN10-ROBOT)
(NOT-COLUMN9-ROBOT)
(not (NOT-COLUMN10-ROBOT))
(not (COLUMN9-ROBOT))
)
)
(when
(and
(COLUMN8-ROBOT)
)
(and
(COLUMN9-ROBOT)
(NOT-COLUMN8-ROBOT)
(not (NOT-COLUMN9-ROBOT))
(not (COLUMN8-ROBOT))
)
)
(when
(and
(COLUMN7-ROBOT)
)
(and
(COLUMN8-ROBOT)
(NOT-COLUMN7-ROBOT)
(not (NOT-COLUMN8-ROBOT))
(not (COLUMN7-ROBOT))
)
)
(when
(and
(COLUMN6-ROBOT)
)
(and
(COLUMN7-ROBOT)
(NOT-COLUMN6-ROBOT)
(not (NOT-COLUMN7-ROBOT))
(not (COLUMN6-ROBOT))
)
)
(when
(and
(COLUMN5-ROBOT)
)
(and
(COLUMN6-ROBOT)
(NOT-COLUMN5-ROBOT)
(not (NOT-COLUMN6-ROBOT))
(not (COLUMN5-ROBOT))
)
)
(when
(and
(COLUMN4-ROBOT)
)
(and
(COLUMN5-ROBOT)
(NOT-COLUMN4-ROBOT)
(not (NOT-COLUMN5-ROBOT))
(not (COLUMN4-ROBOT))
)
)
(when
(and
(COLUMN3-ROBOT)
)
(and
(COLUMN4-ROBOT)
(NOT-COLUMN3-ROBOT)
(not (NOT-COLUMN4-ROBOT))
(not (COLUMN3-ROBOT))
)
)
(when
(and
(COLUMN2-ROBOT)
)
(and
(COLUMN3-ROBOT)
(NOT-COLUMN2-ROBOT)
(not (NOT-COLUMN3-ROBOT))
(not (COLUMN2-ROBOT))
)
)
(when
(and
(COLUMN1-ROBOT)
)
(and
(COLUMN2-ROBOT)
(NOT-COLUMN1-ROBOT)
(not (NOT-COLUMN2-ROBOT))
(not (COLUMN1-ROBOT))
)
)
(when
(and
(COLUMN0-ROBOT)
)
(and
(COLUMN1-ROBOT)
(NOT-COLUMN0-ROBOT)
(not (NOT-COLUMN1-ROBOT))
(not (COLUMN0-ROBOT))
)
)
(when
(and
(LEFTOF8-ROBOT)
)
(and
(LEFTOF20-ROBOT)
(NOT-LEFTOF19-ROBOT)
(LEFTOF19-ROBOT)
(NOT-LEFTOF18-ROBOT)
(LEFTOF18-ROBOT)
(NOT-LEFTOF17-ROBOT)
(LEFTOF17-ROBOT)
(NOT-LEFTOF16-ROBOT)
(LEFTOF16-ROBOT)
(NOT-LEFTOF15-ROBOT)
(LEFTOF15-ROBOT)
(NOT-LEFTOF14-ROBOT)
(LEFTOF14-ROBOT)
(NOT-LEFTOF13-ROBOT)
(LEFTOF13-ROBOT)
(NOT-LEFTOF12-ROBOT)
(LEFTOF12-ROBOT)
(NOT-LEFTOF11-ROBOT)
(LEFTOF11-ROBOT)
(NOT-LEFTOF10-ROBOT)
(LEFTOF10-ROBOT)
(NOT-LEFTOF9-ROBOT)
(LEFTOF9-ROBOT)
(NOT-LEFTOF8-ROBOT)
(not (NOT-LEFTOF20-ROBOT))
(not (LEFTOF8-ROBOT))
)
)
(when
(and
(LEFTOF7-ROBOT)
)
(and
(LEFTOF20-ROBOT)
(NOT-LEFTOF19-ROBOT)
(LEFTOF19-ROBOT)
(NOT-LEFTOF18-ROBOT)
(LEFTOF18-ROBOT)
(NOT-LEFTOF17-ROBOT)
(LEFTOF17-ROBOT)
(NOT-LEFTOF16-ROBOT)
(LEFTOF16-ROBOT)
(NOT-LEFTOF15-ROBOT)
(LEFTOF15-ROBOT)
(NOT-LEFTOF14-ROBOT)
(LEFTOF14-ROBOT)
(NOT-LEFTOF13-ROBOT)
(LEFTOF13-ROBOT)
(NOT-LEFTOF12-ROBOT)
(LEFTOF12-ROBOT)
(NOT-LEFTOF11-ROBOT)
(LEFTOF11-ROBOT)
(NOT-LEFTOF10-ROBOT)
(LEFTOF10-ROBOT)
(NOT-LEFTOF9-ROBOT)
(LEFTOF9-ROBOT)
(NOT-LEFTOF8-ROBOT)
(LEFTOF8-ROBOT)
(NOT-LEFTOF7-ROBOT)
(not (NOT-LEFTOF20-ROBOT))
(not (LEFTOF7-ROBOT))
)
)
(when
(and
(LEFTOF4-ROBOT)
)
(and
(LEFTOF20-ROBOT)
(NOT-LEFTOF19-ROBOT)
(LEFTOF19-ROBOT)
(NOT-LEFTOF18-ROBOT)
(LEFTOF18-ROBOT)
(NOT-LEFTOF17-ROBOT)
(LEFTOF17-ROBOT)
(NOT-LEFTOF16-ROBOT)
(LEFTOF16-ROBOT)
(NOT-LEFTOF15-ROBOT)
(LEFTOF15-ROBOT)
(NOT-LEFTOF14-ROBOT)
(LEFTOF14-ROBOT)
(NOT-LEFTOF13-ROBOT)
(LEFTOF13-ROBOT)
(NOT-LEFTOF12-ROBOT)
(LEFTOF12-ROBOT)
(NOT-LEFTOF11-ROBOT)
(LEFTOF11-ROBOT)
(NOT-LEFTOF10-ROBOT)
(LEFTOF10-ROBOT)
(NOT-LEFTOF9-ROBOT)
(LEFTOF9-ROBOT)
(NOT-LEFTOF8-ROBOT)
(LEFTOF8-ROBOT)
(NOT-LEFTOF7-ROBOT)
(LEFTOF7-ROBOT)
(NOT-LEFTOF6-ROBOT)
(LEFTOF6-ROBOT)
(NOT-LEFTOF5-ROBOT)
(LEFTOF5-ROBOT)
(NOT-LEFTOF4-ROBOT)
(not (NOT-LEFTOF20-ROBOT))
(not (LEFTOF4-ROBOT))
)
)
(when
(and
(LEFTOF3-ROBOT)
)
(and
(LEFTOF20-ROBOT)
(NOT-LEFTOF19-ROBOT)
(LEFTOF19-ROBOT)
(NOT-LEFTOF18-ROBOT)
(LEFTOF18-ROBOT)
(NOT-LEFTOF17-ROBOT)
(LEFTOF17-ROBOT)
(NOT-LEFTOF16-ROBOT)
(LEFTOF16-ROBOT)
(NOT-LEFTOF15-ROBOT)
(LEFTOF15-ROBOT)
(NOT-LEFTOF14-ROBOT)
(LEFTOF14-ROBOT)
(NOT-LEFTOF13-ROBOT)
(LEFTOF13-ROBOT)
(NOT-LEFTOF12-ROBOT)
(LEFTOF12-ROBOT)
(NOT-LEFTOF11-ROBOT)
(LEFTOF11-ROBOT)
(NOT-LEFTOF10-ROBOT)
(LEFTOF10-ROBOT)
(NOT-LEFTOF9-ROBOT)
(LEFTOF9-ROBOT)
(NOT-LEFTOF8-ROBOT)
(LEFTOF8-ROBOT)
(NOT-LEFTOF7-ROBOT)
(LEFTOF7-ROBOT)
(NOT-LEFTOF6-ROBOT)
(LEFTOF6-ROBOT)
(NOT-LEFTOF5-ROBOT)
(LEFTOF5-ROBOT)
(NOT-LEFTOF4-ROBOT)
(LEFTOF4-ROBOT)
(NOT-LEFTOF3-ROBOT)
(not (NOT-LEFTOF20-ROBOT))
(not (LEFTOF3-ROBOT))
)
)
(when
(and
(LEFTOF14-ROBOT)
)
(and
(LEFTOF20-ROBOT)
(NOT-LEFTOF19-ROBOT)
(LEFTOF19-ROBOT)
(NOT-LEFTOF18-ROBOT)
(LEFTOF18-ROBOT)
(NOT-LEFTOF17-ROBOT)
(LEFTOF17-ROBOT)
(NOT-LEFTOF16-ROBOT)
(LEFTOF16-ROBOT)
(NOT-LEFTOF15-ROBOT)
(LEFTOF15-ROBOT)
(NOT-LEFTOF14-ROBOT)
(not (NOT-LEFTOF20-ROBOT))
(not (LEFTOF14-ROBOT))
)
)
(when
(and
(LEFTOF16-ROBOT)
)
(and
(LEFTOF20-ROBOT)
(NOT-LEFTOF19-ROBOT)
(LEFTOF19-ROBOT)
(NOT-LEFTOF18-ROBOT)
(LEFTOF18-ROBOT)
(NOT-LEFTOF17-ROBOT)
(LEFTOF17-ROBOT)
(NOT-LEFTOF16-ROBOT)
(not (NOT-LEFTOF20-ROBOT))
(not (LEFTOF16-ROBOT))
)
)
(when
(and
(LEFTOF5-ROBOT)
)
(and
(LEFTOF20-ROBOT)
(NOT-LEFTOF19-ROBOT)
(LEFTOF19-ROBOT)
(NOT-LEFTOF18-ROBOT)
(LEFTOF18-ROBOT)
(NOT-LEFTOF17-ROBOT)
(LEFTOF17-ROBOT)
(NOT-LEFTOF16-ROBOT)
(LEFTOF16-ROBOT)
(NOT-LEFTOF15-ROBOT)
(LEFTOF15-ROBOT)
(NOT-LEFTOF14-ROBOT)
(LEFTOF14-ROBOT)
(NOT-LEFTOF13-ROBOT)
(LEFTOF13-ROBOT)
(NOT-LEFTOF12-ROBOT)
(LEFTOF12-ROBOT)
(NOT-LEFTOF11-ROBOT)
(LEFTOF11-ROBOT)
(NOT-LEFTOF10-ROBOT)
(LEFTOF10-ROBOT)
(NOT-LEFTOF9-ROBOT)
(LEFTOF9-ROBOT)
(NOT-LEFTOF8-ROBOT)
(LEFTOF8-ROBOT)
(NOT-LEFTOF7-ROBOT)
(LEFTOF7-ROBOT)
(NOT-LEFTOF6-ROBOT)
(LEFTOF6-ROBOT)
(NOT-LEFTOF5-ROBOT)
(not (NOT-LEFTOF20-ROBOT))
(not (LEFTOF5-ROBOT))
)
)
(when
(and
(LEFTOF6-ROBOT)
)
(and
(LEFTOF20-ROBOT)
(NOT-LEFTOF19-ROBOT)
(LEFTOF19-ROBOT)
(NOT-LEFTOF18-ROBOT)
(LEFTOF18-ROBOT)
(NOT-LEFTOF17-ROBOT)
(LEFTOF17-ROBOT)
(NOT-LEFTOF16-ROBOT)
(LEFTOF16-ROBOT)
(NOT-LEFTOF15-ROBOT)
(LEFTOF15-ROBOT)
(NOT-LEFTOF14-ROBOT)
(LEFTOF14-ROBOT)
(NOT-LEFTOF13-ROBOT)
(LEFTOF13-ROBOT)
(NOT-LEFTOF12-ROBOT)
(LEFTOF12-ROBOT)
(NOT-LEFTOF11-ROBOT)
(LEFTOF11-ROBOT)
(NOT-LEFTOF10-ROBOT)
(LEFTOF10-ROBOT)
(NOT-LEFTOF9-ROBOT)
(LEFTOF9-ROBOT)
(NOT-LEFTOF8-ROBOT)
(LEFTOF8-ROBOT)
(NOT-LEFTOF7-ROBOT)
(LEFTOF7-ROBOT)
(NOT-LEFTOF6-ROBOT)
(not (NOT-LEFTOF20-ROBOT))
(not (LEFTOF6-ROBOT))
)
)
(when
(and
(LEFTOF12-ROBOT)
)
(and
(LEFTOF20-ROBOT)
(NOT-LEFTOF19-ROBOT)
(LEFTOF19-ROBOT)
(NOT-LEFTOF18-ROBOT)
(LEFTOF18-ROBOT)
(NOT-LEFTOF17-ROBOT)
(LEFTOF17-ROBOT)
(NOT-LEFTOF16-ROBOT)
(LEFTOF16-ROBOT)
(NOT-LEFTOF15-ROBOT)
(LEFTOF15-ROBOT)
(NOT-LEFTOF14-ROBOT)
(LEFTOF14-ROBOT)
(NOT-LEFTOF13-ROBOT)
(LEFTOF13-ROBOT)
(NOT-LEFTOF12-ROBOT)
(not (NOT-LEFTOF20-ROBOT))
(not (LEFTOF12-ROBOT))
)
)
(when
(and
(LEFTOF19-ROBOT)
)
(and
(LEFTOF20-ROBOT)
(NOT-LEFTOF19-ROBOT)
(not (NOT-LEFTOF20-ROBOT))
(not (LEFTOF19-ROBOT))
)
)
(when
(and
(LEFTOF13-ROBOT)
)
(and
(LEFTOF20-ROBOT)
(NOT-LEFTOF19-ROBOT)
(LEFTOF19-ROBOT)
(NOT-LEFTOF18-ROBOT)
(LEFTOF18-ROBOT)
(NOT-LEFTOF17-ROBOT)
(LEFTOF17-ROBOT)
(NOT-LEFTOF16-ROBOT)
(LEFTOF16-ROBOT)
(NOT-LEFTOF15-ROBOT)
(LEFTOF15-ROBOT)
(NOT-LEFTOF14-ROBOT)
(LEFTOF14-ROBOT)
(NOT-LEFTOF13-ROBOT)
(not (NOT-LEFTOF20-ROBOT))
(not (LEFTOF13-ROBOT))
)
)
(when
(and
(LEFTOF2-ROBOT)
)
(and
(LEFTOF20-ROBOT)
(NOT-LEFTOF19-ROBOT)
(LEFTOF19-ROBOT)
(NOT-LEFTOF18-ROBOT)
(LEFTOF18-ROBOT)
(NOT-LEFTOF17-ROBOT)
(LEFTOF17-ROBOT)
(NOT-LEFTOF16-ROBOT)
(LEFTOF16-ROBOT)
(NOT-LEFTOF15-ROBOT)
(LEFTOF15-ROBOT)
(NOT-LEFTOF14-ROBOT)
(LEFTOF14-ROBOT)
(NOT-LEFTOF13-ROBOT)
(LEFTOF13-ROBOT)
(NOT-LEFTOF12-ROBOT)
(LEFTOF12-ROBOT)
(NOT-LEFTOF11-ROBOT)
(LEFTOF11-ROBOT)
(NOT-LEFTOF10-ROBOT)
(LEFTOF10-ROBOT)
(NOT-LEFTOF9-ROBOT)
(LEFTOF9-ROBOT)
(NOT-LEFTOF8-ROBOT)
(LEFTOF8-ROBOT)
(NOT-LEFTOF7-ROBOT)
(LEFTOF7-ROBOT)
(NOT-LEFTOF6-ROBOT)
(LEFTOF6-ROBOT)
(NOT-LEFTOF5-ROBOT)
(LEFTOF5-ROBOT)
(NOT-LEFTOF4-ROBOT)
(LEFTOF4-ROBOT)
(NOT-LEFTOF3-ROBOT)
(LEFTOF3-ROBOT)
(NOT-LEFTOF2-ROBOT)
(not (NOT-LEFTOF20-ROBOT))
(not (LEFTOF2-ROBOT))
)
)
(when
(and
(LEFTOF9-ROBOT)
)
(and
(LEFTOF20-ROBOT)
(NOT-LEFTOF19-ROBOT)
(LEFTOF19-ROBOT)
(NOT-LEFTOF18-ROBOT)
(LEFTOF18-ROBOT)
(NOT-LEFTOF17-ROBOT)
(LEFTOF17-ROBOT)
(NOT-LEFTOF16-ROBOT)
(LEFTOF16-ROBOT)
(NOT-LEFTOF15-ROBOT)
(LEFTOF15-ROBOT)
(NOT-LEFTOF14-ROBOT)
(LEFTOF14-ROBOT)
(NOT-LEFTOF13-ROBOT)
(LEFTOF13-ROBOT)
(NOT-LEFTOF12-ROBOT)
(LEFTOF12-ROBOT)
(NOT-LEFTOF11-ROBOT)
(LEFTOF11-ROBOT)
(NOT-LEFTOF10-ROBOT)
(LEFTOF10-ROBOT)
(NOT-LEFTOF9-ROBOT)
(not (NOT-LEFTOF20-ROBOT))
(not (LEFTOF9-ROBOT))
)
)
(when
(and
(LEFTOF11-ROBOT)
)
(and
(LEFTOF20-ROBOT)
(NOT-LEFTOF19-ROBOT)
(LEFTOF19-ROBOT)
(NOT-LEFTOF18-ROBOT)
(LEFTOF18-ROBOT)
(NOT-LEFTOF17-ROBOT)
(LEFTOF17-ROBOT)
(NOT-LEFTOF16-ROBOT)
(LEFTOF16-ROBOT)
(NOT-LEFTOF15-ROBOT)
(LEFTOF15-ROBOT)
(NOT-LEFTOF14-ROBOT)
(LEFTOF14-ROBOT)
(NOT-LEFTOF13-ROBOT)
(LEFTOF13-ROBOT)
(NOT-LEFTOF12-ROBOT)
(LEFTOF12-ROBOT)
(NOT-LEFTOF11-ROBOT)
(not (NOT-LEFTOF20-ROBOT))
(not (LEFTOF11-ROBOT))
)
)
(when
(and
(LEFTOF17-ROBOT)
)
(and
(LEFTOF20-ROBOT)
(NOT-LEFTOF19-ROBOT)
(LEFTOF19-ROBOT)
(NOT-LEFTOF18-ROBOT)
(LEFTOF18-ROBOT)
(NOT-LEFTOF17-ROBOT)
(not (NOT-LEFTOF20-ROBOT))
(not (LEFTOF17-ROBOT))
)
)
(when
(and
(LEFTOF18-ROBOT)
)
(and
(LEFTOF20-ROBOT)
(NOT-LEFTOF19-ROBOT)
(LEFTOF19-ROBOT)
(NOT-LEFTOF18-ROBOT)
(not (NOT-LEFTOF20-ROBOT))
(not (LEFTOF18-ROBOT))
)
)
(when
(and
(LEFTOF1-ROBOT)
)
(and
(LEFTOF20-ROBOT)
(NOT-LEFTOF19-ROBOT)
(LEFTOF19-ROBOT)
(NOT-LEFTOF18-ROBOT)
(LEFTOF18-ROBOT)
(NOT-LEFTOF17-ROBOT)
(LEFTOF17-ROBOT)
(NOT-LEFTOF16-ROBOT)
(LEFTOF16-ROBOT)
(NOT-LEFTOF15-ROBOT)
(LEFTOF15-ROBOT)
(NOT-LEFTOF14-ROBOT)
(LEFTOF14-ROBOT)
(NOT-LEFTOF13-ROBOT)
(LEFTOF13-ROBOT)
(NOT-LEFTOF12-ROBOT)
(LEFTOF12-ROBOT)
(NOT-LEFTOF11-ROBOT)
(LEFTOF11-ROBOT)
(NOT-LEFTOF10-ROBOT)
(LEFTOF10-ROBOT)
(NOT-LEFTOF9-ROBOT)
(LEFTOF9-ROBOT)
(NOT-LEFTOF8-ROBOT)
(LEFTOF8-ROBOT)
(NOT-LEFTOF7-ROBOT)
(LEFTOF7-ROBOT)
(NOT-LEFTOF6-ROBOT)
(LEFTOF6-ROBOT)
(NOT-LEFTOF5-ROBOT)
(LEFTOF5-ROBOT)
(NOT-LEFTOF4-ROBOT)
(LEFTOF4-ROBOT)
(NOT-LEFTOF3-ROBOT)
(LEFTOF3-ROBOT)
(NOT-LEFTOF2-ROBOT)
(LEFTOF2-ROBOT)
(NOT-LEFTOF1-ROBOT)
(not (NOT-LEFTOF20-ROBOT))
(not (LEFTOF1-ROBOT))
)
)
(when
(and
(LEFTOF10-ROBOT)
)
(and
(LEFTOF20-ROBOT)
(NOT-LEFTOF19-ROBOT)
(LEFTOF19-ROBOT)
(NOT-LEFTOF18-ROBOT)
(LEFTOF18-ROBOT)
(NOT-LEFTOF17-ROBOT)
(LEFTOF17-ROBOT)
(NOT-LEFTOF16-ROBOT)
(LEFTOF16-ROBOT)
(NOT-LEFTOF15-ROBOT)
(LEFTOF15-ROBOT)
(NOT-LEFTOF14-ROBOT)
(LEFTOF14-ROBOT)
(NOT-LEFTOF13-ROBOT)
(LEFTOF13-ROBOT)
(NOT-LEFTOF12-ROBOT)
(LEFTOF12-ROBOT)
(NOT-LEFTOF11-ROBOT)
(LEFTOF11-ROBOT)
(NOT-LEFTOF10-ROBOT)
(not (NOT-LEFTOF20-ROBOT))
(not (LEFTOF10-ROBOT))
)
)
(when
(and
(LEFTOF15-ROBOT)
)
(and
(LEFTOF20-ROBOT)
(NOT-LEFTOF19-ROBOT)
(LEFTOF19-ROBOT)
(NOT-LEFTOF18-ROBOT)
(LEFTOF18-ROBOT)
(NOT-LEFTOF17-ROBOT)
(LEFTOF17-ROBOT)
(NOT-LEFTOF16-ROBOT)
(LEFTOF16-ROBOT)
(NOT-LEFTOF15-ROBOT)
(not (NOT-LEFTOF20-ROBOT))
(not (LEFTOF15-ROBOT))
)
)
(when
(and
(RIGHTOF19-ROBOT)
)
(and
(RIGHTOF18-ROBOT)
(RIGHTOF17-ROBOT)
(RIGHTOF16-ROBOT)
(RIGHTOF15-ROBOT)
(RIGHTOF14-ROBOT)
(RIGHTOF13-ROBOT)
(RIGHTOF12-ROBOT)
(RIGHTOF11-ROBOT)
(RIGHTOF10-ROBOT)
(RIGHTOF9-ROBOT)
(RIGHTOF8-ROBOT)
(RIGHTOF7-ROBOT)
(RIGHTOF6-ROBOT)
(RIGHTOF5-ROBOT)
(RIGHTOF4-ROBOT)
(RIGHTOF3-ROBOT)
(RIGHTOF2-ROBOT)
(not (NOT-RIGHTOF19-ROBOT))
(not (NOT-RIGHTOF18-ROBOT))
(not (NOT-RIGHTOF17-ROBOT))
(not (NOT-RIGHTOF16-ROBOT))
(not (NOT-RIGHTOF15-ROBOT))
(not (NOT-RIGHTOF14-ROBOT))
(not (NOT-RIGHTOF13-ROBOT))
(not (NOT-RIGHTOF12-ROBOT))
(not (NOT-RIGHTOF11-ROBOT))
(not (NOT-RIGHTOF10-ROBOT))
(not (NOT-RIGHTOF9-ROBOT))
(not (NOT-RIGHTOF8-ROBOT))
(not (NOT-RIGHTOF7-ROBOT))
(not (NOT-RIGHTOF6-ROBOT))
(not (NOT-RIGHTOF5-ROBOT))
(not (NOT-RIGHTOF4-ROBOT))
(not (NOT-RIGHTOF3-ROBOT))
(not (NOT-RIGHTOF2-ROBOT))
)
)
(when
(and
(RIGHTOF18-ROBOT)
)
(and
(RIGHTOF19-ROBOT)
(RIGHTOF17-ROBOT)
(RIGHTOF16-ROBOT)
(RIGHTOF15-ROBOT)
(RIGHTOF14-ROBOT)
(RIGHTOF13-ROBOT)
(RIGHTOF12-ROBOT)
(RIGHTOF11-ROBOT)
(RIGHTOF10-ROBOT)
(RIGHTOF9-ROBOT)
(RIGHTOF8-ROBOT)
(RIGHTOF7-ROBOT)
(RIGHTOF6-ROBOT)
(RIGHTOF5-ROBOT)
(RIGHTOF4-ROBOT)
(RIGHTOF3-ROBOT)
(RIGHTOF2-ROBOT)
(not (NOT-RIGHTOF19-ROBOT))
(not (NOT-RIGHTOF18-ROBOT))
(not (NOT-RIGHTOF17-ROBOT))
(not (NOT-RIGHTOF16-ROBOT))
(not (NOT-RIGHTOF15-ROBOT))
(not (NOT-RIGHTOF14-ROBOT))
(not (NOT-RIGHTOF13-ROBOT))
(not (NOT-RIGHTOF12-ROBOT))
(not (NOT-RIGHTOF11-ROBOT))
(not (NOT-RIGHTOF10-ROBOT))
(not (NOT-RIGHTOF9-ROBOT))
(not (NOT-RIGHTOF8-ROBOT))
(not (NOT-RIGHTOF7-ROBOT))
(not (NOT-RIGHTOF6-ROBOT))
(not (NOT-RIGHTOF5-ROBOT))
(not (NOT-RIGHTOF4-ROBOT))
(not (NOT-RIGHTOF3-ROBOT))
(not (NOT-RIGHTOF2-ROBOT))
)
)
(when
(and
(RIGHTOF17-ROBOT)
)
(and
(RIGHTOF18-ROBOT)
(RIGHTOF16-ROBOT)
(RIGHTOF15-ROBOT)
(RIGHTOF14-ROBOT)
(RIGHTOF13-ROBOT)
(RIGHTOF12-ROBOT)
(RIGHTOF11-ROBOT)
(RIGHTOF10-ROBOT)
(RIGHTOF9-ROBOT)
(RIGHTOF8-ROBOT)
(RIGHTOF7-ROBOT)
(RIGHTOF6-ROBOT)
(RIGHTOF5-ROBOT)
(RIGHTOF4-ROBOT)
(RIGHTOF3-ROBOT)
(RIGHTOF2-ROBOT)
(not (NOT-RIGHTOF18-ROBOT))
(not (NOT-RIGHTOF17-ROBOT))
(not (NOT-RIGHTOF16-ROBOT))
(not (NOT-RIGHTOF15-ROBOT))
(not (NOT-RIGHTOF14-ROBOT))
(not (NOT-RIGHTOF13-ROBOT))
(not (NOT-RIGHTOF12-ROBOT))
(not (NOT-RIGHTOF11-ROBOT))
(not (NOT-RIGHTOF10-ROBOT))
(not (NOT-RIGHTOF9-ROBOT))
(not (NOT-RIGHTOF8-ROBOT))
(not (NOT-RIGHTOF7-ROBOT))
(not (NOT-RIGHTOF6-ROBOT))
(not (NOT-RIGHTOF5-ROBOT))
(not (NOT-RIGHTOF4-ROBOT))
(not (NOT-RIGHTOF3-ROBOT))
(not (NOT-RIGHTOF2-ROBOT))
)
)
(when
(and
(RIGHTOF16-ROBOT)
)
(and
(RIGHTOF17-ROBOT)
(RIGHTOF15-ROBOT)
(RIGHTOF14-ROBOT)
(RIGHTOF13-ROBOT)
(RIGHTOF12-ROBOT)
(RIGHTOF11-ROBOT)
(RIGHTOF10-ROBOT)
(RIGHTOF9-ROBOT)
(RIGHTOF8-ROBOT)
(RIGHTOF7-ROBOT)
(RIGHTOF6-ROBOT)
(RIGHTOF5-ROBOT)
(RIGHTOF4-ROBOT)
(RIGHTOF3-ROBOT)
(RIGHTOF2-ROBOT)
(not (NOT-RIGHTOF17-ROBOT))
(not (NOT-RIGHTOF16-ROBOT))
(not (NOT-RIGHTOF15-ROBOT))
(not (NOT-RIGHTOF14-ROBOT))
(not (NOT-RIGHTOF13-ROBOT))
(not (NOT-RIGHTOF12-ROBOT))
(not (NOT-RIGHTOF11-ROBOT))
(not (NOT-RIGHTOF10-ROBOT))
(not (NOT-RIGHTOF9-ROBOT))
(not (NOT-RIGHTOF8-ROBOT))
(not (NOT-RIGHTOF7-ROBOT))
(not (NOT-RIGHTOF6-ROBOT))
(not (NOT-RIGHTOF5-ROBOT))
(not (NOT-RIGHTOF4-ROBOT))
(not (NOT-RIGHTOF3-ROBOT))
(not (NOT-RIGHTOF2-ROBOT))
)
)
(when
(and
(RIGHTOF15-ROBOT)
)
(and
(RIGHTOF16-ROBOT)
(RIGHTOF14-ROBOT)
(RIGHTOF13-ROBOT)
(RIGHTOF12-ROBOT)
(RIGHTOF11-ROBOT)
(RIGHTOF10-ROBOT)
(RIGHTOF9-ROBOT)
(RIGHTOF8-ROBOT)
(RIGHTOF7-ROBOT)
(RIGHTOF6-ROBOT)
(RIGHTOF5-ROBOT)
(RIGHTOF4-ROBOT)
(RIGHTOF3-ROBOT)
(RIGHTOF2-ROBOT)
(not (NOT-RIGHTOF16-ROBOT))
(not (NOT-RIGHTOF15-ROBOT))
(not (NOT-RIGHTOF14-ROBOT))
(not (NOT-RIGHTOF13-ROBOT))
(not (NOT-RIGHTOF12-ROBOT))
(not (NOT-RIGHTOF11-ROBOT))
(not (NOT-RIGHTOF10-ROBOT))
(not (NOT-RIGHTOF9-ROBOT))
(not (NOT-RIGHTOF8-ROBOT))
(not (NOT-RIGHTOF7-ROBOT))
(not (NOT-RIGHTOF6-ROBOT))
(not (NOT-RIGHTOF5-ROBOT))
(not (NOT-RIGHTOF4-ROBOT))
(not (NOT-RIGHTOF3-ROBOT))
(not (NOT-RIGHTOF2-ROBOT))
)
)
(when
(and
(RIGHTOF14-ROBOT)
)
(and
(RIGHTOF15-ROBOT)
(RIGHTOF13-ROBOT)
(RIGHTOF12-ROBOT)
(RIGHTOF11-ROBOT)
(RIGHTOF10-ROBOT)
(RIGHTOF9-ROBOT)
(RIGHTOF8-ROBOT)
(RIGHTOF7-ROBOT)
(RIGHTOF6-ROBOT)
(RIGHTOF5-ROBOT)
(RIGHTOF4-ROBOT)
(RIGHTOF3-ROBOT)
(RIGHTOF2-ROBOT)
(not (NOT-RIGHTOF15-ROBOT))
(not (NOT-RIGHTOF14-ROBOT))
(not (NOT-RIGHTOF13-ROBOT))
(not (NOT-RIGHTOF12-ROBOT))
(not (NOT-RIGHTOF11-ROBOT))
(not (NOT-RIGHTOF10-ROBOT))
(not (NOT-RIGHTOF9-ROBOT))
(not (NOT-RIGHTOF8-ROBOT))
(not (NOT-RIGHTOF7-ROBOT))
(not (NOT-RIGHTOF6-ROBOT))
(not (NOT-RIGHTOF5-ROBOT))
(not (NOT-RIGHTOF4-ROBOT))
(not (NOT-RIGHTOF3-ROBOT))
(not (NOT-RIGHTOF2-ROBOT))
)
)
(when
(and
(RIGHTOF13-ROBOT)
)
(and
(RIGHTOF14-ROBOT)
(RIGHTOF12-ROBOT)
(RIGHTOF11-ROBOT)
(RIGHTOF10-ROBOT)
(RIGHTOF9-ROBOT)
(RIGHTOF8-ROBOT)
(RIGHTOF7-ROBOT)
(RIGHTOF6-ROBOT)
(RIGHTOF5-ROBOT)
(RIGHTOF4-ROBOT)
(RIGHTOF3-ROBOT)
(RIGHTOF2-ROBOT)
(not (NOT-RIGHTOF14-ROBOT))
(not (NOT-RIGHTOF13-ROBOT))
(not (NOT-RIGHTOF12-ROBOT))
(not (NOT-RIGHTOF11-ROBOT))
(not (NOT-RIGHTOF10-ROBOT))
(not (NOT-RIGHTOF9-ROBOT))
(not (NOT-RIGHTOF8-ROBOT))
(not (NOT-RIGHTOF7-ROBOT))
(not (NOT-RIGHTOF6-ROBOT))
(not (NOT-RIGHTOF5-ROBOT))
(not (NOT-RIGHTOF4-ROBOT))
(not (NOT-RIGHTOF3-ROBOT))
(not (NOT-RIGHTOF2-ROBOT))
)
)
(when
(and
(RIGHTOF12-ROBOT)
)
(and
(RIGHTOF13-ROBOT)
(RIGHTOF11-ROBOT)
(RIGHTOF10-ROBOT)
(RIGHTOF9-ROBOT)
(RIGHTOF8-ROBOT)
(RIGHTOF7-ROBOT)
(RIGHTOF6-ROBOT)
(RIGHTOF5-ROBOT)
(RIGHTOF4-ROBOT)
(RIGHTOF3-ROBOT)
(RIGHTOF2-ROBOT)
(not (NOT-RIGHTOF13-ROBOT))
(not (NOT-RIGHTOF12-ROBOT))
(not (NOT-RIGHTOF11-ROBOT))
(not (NOT-RIGHTOF10-ROBOT))
(not (NOT-RIGHTOF9-ROBOT))
(not (NOT-RIGHTOF8-ROBOT))
(not (NOT-RIGHTOF7-ROBOT))
(not (NOT-RIGHTOF6-ROBOT))
(not (NOT-RIGHTOF5-ROBOT))
(not (NOT-RIGHTOF4-ROBOT))
(not (NOT-RIGHTOF3-ROBOT))
(not (NOT-RIGHTOF2-ROBOT))
)
)
(when
(and
(RIGHTOF11-ROBOT)
)
(and
(RIGHTOF12-ROBOT)
(RIGHTOF10-ROBOT)
(RIGHTOF9-ROBOT)
(RIGHTOF8-ROBOT)
(RIGHTOF7-ROBOT)
(RIGHTOF6-ROBOT)
(RIGHTOF5-ROBOT)
(RIGHTOF4-ROBOT)
(RIGHTOF3-ROBOT)
(RIGHTOF2-ROBOT)
(not (NOT-RIGHTOF12-ROBOT))
(not (NOT-RIGHTOF11-ROBOT))
(not (NOT-RIGHTOF10-ROBOT))
(not (NOT-RIGHTOF9-ROBOT))
(not (NOT-RIGHTOF8-ROBOT))
(not (NOT-RIGHTOF7-ROBOT))
(not (NOT-RIGHTOF6-ROBOT))
(not (NOT-RIGHTOF5-ROBOT))
(not (NOT-RIGHTOF4-ROBOT))
(not (NOT-RIGHTOF3-ROBOT))
(not (NOT-RIGHTOF2-ROBOT))
)
)
(when
(and
(RIGHTOF10-ROBOT)
)
(and
(RIGHTOF11-ROBOT)
(RIGHTOF9-ROBOT)
(RIGHTOF8-ROBOT)
(RIGHTOF7-ROBOT)
(RIGHTOF6-ROBOT)
(RIGHTOF5-ROBOT)
(RIGHTOF4-ROBOT)
(RIGHTOF3-ROBOT)
(RIGHTOF2-ROBOT)
(not (NOT-RIGHTOF11-ROBOT))
(not (NOT-RIGHTOF10-ROBOT))
(not (NOT-RIGHTOF9-ROBOT))
(not (NOT-RIGHTOF8-ROBOT))
(not (NOT-RIGHTOF7-ROBOT))
(not (NOT-RIGHTOF6-ROBOT))
(not (NOT-RIGHTOF5-ROBOT))
(not (NOT-RIGHTOF4-ROBOT))
(not (NOT-RIGHTOF3-ROBOT))
(not (NOT-RIGHTOF2-ROBOT))
)
)
(when
(and
(RIGHTOF9-ROBOT)
)
(and
(RIGHTOF10-ROBOT)
(RIGHTOF8-ROBOT)
(RIGHTOF7-ROBOT)
(RIGHTOF6-ROBOT)
(RIGHTOF5-ROBOT)
(RIGHTOF4-ROBOT)
(RIGHTOF3-ROBOT)
(RIGHTOF2-ROBOT)
(not (NOT-RIGHTOF10-ROBOT))
(not (NOT-RIGHTOF9-ROBOT))
(not (NOT-RIGHTOF8-ROBOT))
(not (NOT-RIGHTOF7-ROBOT))
(not (NOT-RIGHTOF6-ROBOT))
(not (NOT-RIGHTOF5-ROBOT))
(not (NOT-RIGHTOF4-ROBOT))
(not (NOT-RIGHTOF3-ROBOT))
(not (NOT-RIGHTOF2-ROBOT))
)
)
(when
(and
(RIGHTOF8-ROBOT)
)
(and
(RIGHTOF9-ROBOT)
(RIGHTOF7-ROBOT)
(RIGHTOF6-ROBOT)
(RIGHTOF5-ROBOT)
(RIGHTOF4-ROBOT)
(RIGHTOF3-ROBOT)
(RIGHTOF2-ROBOT)
(not (NOT-RIGHTOF9-ROBOT))
(not (NOT-RIGHTOF8-ROBOT))
(not (NOT-RIGHTOF7-ROBOT))
(not (NOT-RIGHTOF6-ROBOT))
(not (NOT-RIGHTOF5-ROBOT))
(not (NOT-RIGHTOF4-ROBOT))
(not (NOT-RIGHTOF3-ROBOT))
(not (NOT-RIGHTOF2-ROBOT))
)
)
(when
(and
(RIGHTOF7-ROBOT)
)
(and
(RIGHTOF8-ROBOT)
(RIGHTOF6-ROBOT)
(RIGHTOF5-ROBOT)
(RIGHTOF4-ROBOT)
(RIGHTOF3-ROBOT)
(RIGHTOF2-ROBOT)
(not (NOT-RIGHTOF8-ROBOT))
(not (NOT-RIGHTOF7-ROBOT))
(not (NOT-RIGHTOF6-ROBOT))
(not (NOT-RIGHTOF5-ROBOT))
(not (NOT-RIGHTOF4-ROBOT))
(not (NOT-RIGHTOF3-ROBOT))
(not (NOT-RIGHTOF2-ROBOT))
)
)
(when
(and
(RIGHTOF6-ROBOT)
)
(and
(RIGHTOF7-ROBOT)
(RIGHTOF5-ROBOT)
(RIGHTOF4-ROBOT)
(RIGHTOF3-ROBOT)
(RIGHTOF2-ROBOT)
(not (NOT-RIGHTOF7-ROBOT))
(not (NOT-RIGHTOF6-ROBOT))
(not (NOT-RIGHTOF5-ROBOT))
(not (NOT-RIGHTOF4-ROBOT))
(not (NOT-RIGHTOF3-ROBOT))
(not (NOT-RIGHTOF2-ROBOT))
)
)
(when
(and
(RIGHTOF5-ROBOT)
)
(and
(RIGHTOF6-ROBOT)
(RIGHTOF4-ROBOT)
(RIGHTOF3-ROBOT)
(RIGHTOF2-ROBOT)
(not (NOT-RIGHTOF6-ROBOT))
(not (NOT-RIGHTOF5-ROBOT))
(not (NOT-RIGHTOF4-ROBOT))
(not (NOT-RIGHTOF3-ROBOT))
(not (NOT-RIGHTOF2-ROBOT))
)
)
(when
(and
(RIGHTOF4-ROBOT)
)
(and
(RIGHTOF5-ROBOT)
(RIGHTOF3-ROBOT)
(RIGHTOF2-ROBOT)
(not (NOT-RIGHTOF5-ROBOT))
(not (NOT-RIGHTOF4-ROBOT))
(not (NOT-RIGHTOF3-ROBOT))
(not (NOT-RIGHTOF2-ROBOT))
)
)
(when
(and
(RIGHTOF3-ROBOT)
)
(and
(RIGHTOF4-ROBOT)
(RIGHTOF2-ROBOT)
(not (NOT-RIGHTOF4-ROBOT))
(not (NOT-RIGHTOF3-ROBOT))
(not (NOT-RIGHTOF2-ROBOT))
)
)
(when
(and
(RIGHTOF2-ROBOT)
)
(and
(RIGHTOF3-ROBOT)
(not (NOT-RIGHTOF3-ROBOT))
(not (NOT-RIGHTOF2-ROBOT))
)
)
(when
(and
(RIGHTOF1-ROBOT)
)
(and
(RIGHTOF2-ROBOT)
(not (NOT-RIGHTOF2-ROBOT))
)
)
(when
(and
(Flag564-)
)
(and
(RIGHTOF1-ROBOT)
(not (NOT-RIGHTOF1-ROBOT))
)
)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action MOVELEFT-ROBOT
:parameters ()
:precondition
(and
(Flag4-)
)
:effect
(and
(when
(and
(Flag283-)
)
(and
(COLUMN18-ROBOT)
(not (NOT-COLUMN18-ROBOT))
)
)
(when
(and
(Flag281-)
)
(and
(COLUMN17-ROBOT)
(not (NOT-COLUMN17-ROBOT))
)
)
(when
(and
(Flag278-)
)
(and
(COLUMN16-ROBOT)
(not (NOT-COLUMN16-ROBOT))
)
)
(when
(and
(Flag274-)
)
(and
(COLUMN15-ROBOT)
(not (NOT-COLUMN15-ROBOT))
)
)
(when
(and
(Flag269-)
)
(and
(COLUMN14-ROBOT)
(not (NOT-COLUMN14-ROBOT))
)
)
(when
(and
(Flag262-)
)
(and
(COLUMN13-ROBOT)
(not (NOT-COLUMN13-ROBOT))
)
)
(when
(and
(Flag254-)
)
(and
(COLUMN12-ROBOT)
(not (NOT-COLUMN12-ROBOT))
)
)
(when
(and
(Flag246-)
)
(and
(COLUMN11-ROBOT)
(not (NOT-COLUMN11-ROBOT))
)
)
(when
(and
(Flag236-)
)
(and
(COLUMN10-ROBOT)
(not (NOT-COLUMN10-ROBOT))
)
)
(when
(and
(Flag226-)
)
(and
(COLUMN9-ROBOT)
(not (NOT-COLUMN9-ROBOT))
)
)
(when
(and
(Flag214-)
)
(and
(COLUMN8-ROBOT)
(not (NOT-COLUMN8-ROBOT))
)
)
(when
(and
(Flag202-)
)
(and
(COLUMN7-ROBOT)
(not (NOT-COLUMN7-ROBOT))
)
)
(when
(and
(Flag188-)
)
(and
(COLUMN6-ROBOT)
(not (NOT-COLUMN6-ROBOT))
)
)
(when
(and
(Flag173-)
)
(and
(COLUMN5-ROBOT)
(not (NOT-COLUMN5-ROBOT))
)
)
(when
(and
(Flag156-)
)
(and
(COLUMN4-ROBOT)
(not (NOT-COLUMN4-ROBOT))
)
)
(when
(and
(Flag140-)
)
(and
(COLUMN3-ROBOT)
(not (NOT-COLUMN3-ROBOT))
)
)
(when
(and
(Flag120-)
)
(and
(COLUMN2-ROBOT)
(not (NOT-COLUMN2-ROBOT))
)
)
(when
(and
(Flag100-)
)
(and
(COLUMN1-ROBOT)
(not (NOT-COLUMN1-ROBOT))
)
)
(when
(and
(Flag81-)
)
(and
(COLUMN0-ROBOT)
(not (NOT-COLUMN0-ROBOT))
)
)
(when
(and
(COLUMN19-ROBOT)
)
(and
(COLUMN18-ROBOT)
(NOT-COLUMN19-ROBOT)
(not (NOT-COLUMN18-ROBOT))
(not (COLUMN19-ROBOT))
)
)
(when
(and
(COLUMN18-ROBOT)
)
(and
(COLUMN17-ROBOT)
(NOT-COLUMN18-ROBOT)
(not (NOT-COLUMN17-ROBOT))
(not (COLUMN18-ROBOT))
)
)
(when
(and
(COLUMN17-ROBOT)
)
(and
(COLUMN16-ROBOT)
(NOT-COLUMN17-ROBOT)
(not (NOT-COLUMN16-ROBOT))
(not (COLUMN17-ROBOT))
)
)
(when
(and
(COLUMN16-ROBOT)
)
(and
(COLUMN15-ROBOT)
(NOT-COLUMN16-ROBOT)
(not (NOT-COLUMN15-ROBOT))
(not (COLUMN16-ROBOT))
)
)
(when
(and
(COLUMN15-ROBOT)
)
(and
(COLUMN14-ROBOT)
(NOT-COLUMN15-ROBOT)
(not (NOT-COLUMN14-ROBOT))
(not (COLUMN15-ROBOT))
)
)
(when
(and
(COLUMN14-ROBOT)
)
(and
(COLUMN13-ROBOT)
(NOT-COLUMN14-ROBOT)
(not (NOT-COLUMN13-ROBOT))
(not (COLUMN14-ROBOT))
)
)
(when
(and
(COLUMN13-ROBOT)
)
(and
(COLUMN12-ROBOT)
(NOT-COLUMN13-ROBOT)
(not (NOT-COLUMN12-ROBOT))
(not (COLUMN13-ROBOT))
)
)
(when
(and
(COLUMN12-ROBOT)
)
(and
(COLUMN11-ROBOT)
(NOT-COLUMN12-ROBOT)
(not (NOT-COLUMN11-ROBOT))
(not (COLUMN12-ROBOT))
)
)
(when
(and
(COLUMN11-ROBOT)
)
(and
(COLUMN10-ROBOT)
(NOT-COLUMN11-ROBOT)
(not (NOT-COLUMN10-ROBOT))
(not (COLUMN11-ROBOT))
)
)
(when
(and
(COLUMN10-ROBOT)
)
(and
(COLUMN9-ROBOT)
(NOT-COLUMN10-ROBOT)
(not (NOT-COLUMN9-ROBOT))
(not (COLUMN10-ROBOT))
)
)
(when
(and
(COLUMN9-ROBOT)
)
(and
(COLUMN8-ROBOT)
(NOT-COLUMN9-ROBOT)
(not (NOT-COLUMN8-ROBOT))
(not (COLUMN9-ROBOT))
)
)
(when
(and
(COLUMN8-ROBOT)
)
(and
(COLUMN7-ROBOT)
(NOT-COLUMN8-ROBOT)
(not (NOT-COLUMN7-ROBOT))
(not (COLUMN8-ROBOT))
)
)
(when
(and
(COLUMN7-ROBOT)
)
(and
(COLUMN6-ROBOT)
(NOT-COLUMN7-ROBOT)
(not (NOT-COLUMN6-ROBOT))
(not (COLUMN7-ROBOT))
)
)
(when
(and
(COLUMN6-ROBOT)
)
(and
(COLUMN5-ROBOT)
(NOT-COLUMN6-ROBOT)
(not (NOT-COLUMN5-ROBOT))
(not (COLUMN6-ROBOT))
)
)
(when
(and
(COLUMN5-ROBOT)
)
(and
(COLUMN4-ROBOT)
(NOT-COLUMN5-ROBOT)
(not (NOT-COLUMN4-ROBOT))
(not (COLUMN5-ROBOT))
)
)
(when
(and
(COLUMN4-ROBOT)
)
(and
(COLUMN3-ROBOT)
(NOT-COLUMN4-ROBOT)
(not (NOT-COLUMN3-ROBOT))
(not (COLUMN4-ROBOT))
)
)
(when
(and
(COLUMN3-ROBOT)
)
(and
(COLUMN2-ROBOT)
(NOT-COLUMN3-ROBOT)
(not (NOT-COLUMN2-ROBOT))
(not (COLUMN3-ROBOT))
)
)
(when
(and
(COLUMN2-ROBOT)
)
(and
(COLUMN1-ROBOT)
(NOT-COLUMN2-ROBOT)
(not (NOT-COLUMN1-ROBOT))
(not (COLUMN2-ROBOT))
)
)
(when
(and
(COLUMN1-ROBOT)
)
(and
(COLUMN0-ROBOT)
(NOT-COLUMN1-ROBOT)
(not (NOT-COLUMN0-ROBOT))
(not (COLUMN1-ROBOT))
)
)
(when
(and
(RIGHTOF19-ROBOT)
)
(and
(RIGHTOF18-ROBOT)
(NOT-RIGHTOF19-ROBOT)
(not (NOT-RIGHTOF18-ROBOT))
(not (RIGHTOF19-ROBOT))
)
)
(when
(and
(Flag41-)
)
(and
(RIGHTOF17-ROBOT)
(NOT-RIGHTOF18-ROBOT)
(not (NOT-RIGHTOF17-ROBOT))
(not (RIGHTOF18-ROBOT))
)
)
(when
(and
(Flag40-)
)
(and
(RIGHTOF16-ROBOT)
(NOT-RIGHTOF17-ROBOT)
(not (NOT-RIGHTOF16-ROBOT))
(not (RIGHTOF17-ROBOT))
)
)
(when
(and
(Flag39-)
)
(and
(RIGHTOF15-ROBOT)
(NOT-RIGHTOF16-ROBOT)
(not (NOT-RIGHTOF15-ROBOT))
(not (RIGHTOF16-ROBOT))
)
)
(when
(and
(Flag38-)
)
(and
(RIGHTOF14-ROBOT)
(NOT-RIGHTOF15-ROBOT)
(not (NOT-RIGHTOF14-ROBOT))
(not (RIGHTOF15-ROBOT))
)
)
(when
(and
(Flag37-)
)
(and
(RIGHTOF13-ROBOT)
(NOT-RIGHTOF14-ROBOT)
(not (NOT-RIGHTOF13-ROBOT))
(not (RIGHTOF14-ROBOT))
)
)
(when
(and
(Flag36-)
)
(and
(RIGHTOF12-ROBOT)
(NOT-RIGHTOF13-ROBOT)
(not (NOT-RIGHTOF12-ROBOT))
(not (RIGHTOF13-ROBOT))
)
)
(when
(and
(Flag35-)
)
(and
(RIGHTOF11-ROBOT)
(NOT-RIGHTOF12-ROBOT)
(not (NOT-RIGHTOF11-ROBOT))
(not (RIGHTOF12-ROBOT))
)
)
(when
(and
(Flag34-)
)
(and
(RIGHTOF10-ROBOT)
(NOT-RIGHTOF11-ROBOT)
(not (NOT-RIGHTOF10-ROBOT))
(not (RIGHTOF11-ROBOT))
)
)
(when
(and
(Flag33-)
)
(and
(RIGHTOF9-ROBOT)
(NOT-RIGHTOF10-ROBOT)
(not (NOT-RIGHTOF9-ROBOT))
(not (RIGHTOF10-ROBOT))
)
)
(when
(and
(Flag32-)
)
(and
(RIGHTOF8-ROBOT)
(NOT-RIGHTOF9-ROBOT)
(not (NOT-RIGHTOF8-ROBOT))
(not (RIGHTOF9-ROBOT))
)
)
(when
(and
(Flag31-)
)
(and
(RIGHTOF7-ROBOT)
(NOT-RIGHTOF8-ROBOT)
(not (NOT-RIGHTOF7-ROBOT))
(not (RIGHTOF8-ROBOT))
)
)
(when
(and
(Flag30-)
)
(and
(RIGHTOF6-ROBOT)
(NOT-RIGHTOF7-ROBOT)
(not (NOT-RIGHTOF6-ROBOT))
(not (RIGHTOF7-ROBOT))
)
)
(when
(and
(Flag29-)
)
(and
(RIGHTOF5-ROBOT)
(NOT-RIGHTOF6-ROBOT)
(not (NOT-RIGHTOF5-ROBOT))
(not (RIGHTOF6-ROBOT))
)
)
(when
(and
(Flag28-)
)
(and
(RIGHTOF4-ROBOT)
(NOT-RIGHTOF5-ROBOT)
(not (NOT-RIGHTOF4-ROBOT))
(not (RIGHTOF5-ROBOT))
)
)
(when
(and
(Flag27-)
)
(and
(RIGHTOF3-ROBOT)
(NOT-RIGHTOF4-ROBOT)
(not (NOT-RIGHTOF3-ROBOT))
(not (RIGHTOF4-ROBOT))
)
)
(when
(and
(Flag26-)
)
(and
(RIGHTOF2-ROBOT)
(NOT-RIGHTOF3-ROBOT)
(not (NOT-RIGHTOF2-ROBOT))
(not (RIGHTOF3-ROBOT))
)
)
(when
(and
(Flag25-)
)
(and
(RIGHTOF1-ROBOT)
(NOT-RIGHTOF2-ROBOT)
(not (NOT-RIGHTOF1-ROBOT))
(not (RIGHTOF2-ROBOT))
)
)
(when
(and
(Flag24-)
)
(and
(RIGHTOF0-ROBOT)
(NOT-RIGHTOF1-ROBOT)
(not (NOT-RIGHTOF0-ROBOT))
(not (RIGHTOF1-ROBOT))
)
)
(when
(and
(Flag23-)
)
(and
(LEFTOF19-ROBOT)
(not (NOT-LEFTOF19-ROBOT))
)
)
(when
(and
(Flag22-)
)
(and
(LEFTOF18-ROBOT)
(not (NOT-LEFTOF18-ROBOT))
)
)
(when
(and
(Flag21-)
)
(and
(LEFTOF17-ROBOT)
(not (NOT-LEFTOF17-ROBOT))
)
)
(when
(and
(Flag20-)
)
(and
(LEFTOF16-ROBOT)
(not (NOT-LEFTOF16-ROBOT))
)
)
(when
(and
(Flag19-)
)
(and
(LEFTOF15-ROBOT)
(not (NOT-LEFTOF15-ROBOT))
)
)
(when
(and
(Flag18-)
)
(and
(LEFTOF14-ROBOT)
(not (NOT-LEFTOF14-ROBOT))
)
)
(when
(and
(Flag17-)
)
(and
(LEFTOF13-ROBOT)
(not (NOT-LEFTOF13-ROBOT))
)
)
(when
(and
(Flag16-)
)
(and
(LEFTOF12-ROBOT)
(not (NOT-LEFTOF12-ROBOT))
)
)
(when
(and
(Flag15-)
)
(and
(LEFTOF11-ROBOT)
(not (NOT-LEFTOF11-ROBOT))
)
)
(when
(and
(Flag14-)
)
(and
(LEFTOF10-ROBOT)
(not (NOT-LEFTOF10-ROBOT))
)
)
(when
(and
(Flag13-)
)
(and
(LEFTOF9-ROBOT)
(not (NOT-LEFTOF9-ROBOT))
)
)
(when
(and
(Flag12-)
)
(and
(LEFTOF8-ROBOT)
(not (NOT-LEFTOF8-ROBOT))
)
)
(when
(and
(Flag11-)
)
(and
(LEFTOF7-ROBOT)
(not (NOT-LEFTOF7-ROBOT))
)
)
(when
(and
(Flag10-)
)
(and
(LEFTOF6-ROBOT)
(not (NOT-LEFTOF6-ROBOT))
)
)
(when
(and
(Flag9-)
)
(and
(LEFTOF5-ROBOT)
(not (NOT-LEFTOF5-ROBOT))
)
)
(when
(and
(Flag8-)
)
(and
(LEFTOF4-ROBOT)
(not (NOT-LEFTOF4-ROBOT))
)
)
(when
(and
(Flag7-)
)
(and
(LEFTOF3-ROBOT)
(not (NOT-LEFTOF3-ROBOT))
)
)
(when
(and
(Flag6-)
)
(and
(LEFTOF2-ROBOT)
(not (NOT-LEFTOF2-ROBOT))
)
)
(when
(and
(Flag5-)
)
(and
(LEFTOF1-ROBOT)
(not (NOT-LEFTOF1-ROBOT))
)
)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action CHECKCONSISTENCYACTION
:parameters ()
:precondition
(and
(NOT-ERROR-)
(CHECKCONSISTENCY-)
)
:effect
(and
(when
(and
(BELOWOF6-ROBOT)
(ABOVEOF17-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF6-ROBOT)
(LEFTOF2-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF15-ROBOT)
(BELOWOF8-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF9-ROBOT)
(ABOVEOF16-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF15-ROBOT)
(BELOWOF2-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF18-ROBOT)
(LEFTOF3-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF3-ROBOT)
(RIGHTOF17-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF8-ROBOT)
(RIGHTOF18-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF7-ROBOT)
(ABOVEOF9-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF5-ROBOT)
(ABOVEOF13-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF15-ROBOT)
(LEFTOF7-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF5-ROBOT)
(LEFTOF3-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF4-ROBOT)
(RIGHTOF18-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF14-ROBOT)
(BELOWOF6-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF1-ROBOT)
(RIGHTOF13-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF2-ROBOT)
(ABOVEOF18-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF10-ROBOT)
(BELOWOF2-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF8-ROBOT)
(RIGHTOF11-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF19-ROBOT)
(LEFTOF14-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF15-ROBOT)
(ABOVEOF19-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF12-ROBOT)
(RIGHTOF18-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF16-ROBOT)
(BELOWOF1-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF14-ROBOT)
(BELOWOF1-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF13-ROBOT)
(LEFTOF4-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF2-ROBOT)
(RIGHTOF16-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF2-ROBOT)
(RIGHTOF9-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF4-ROBOT)
(BELOWOF2-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF11-ROBOT)
(BELOWOF3-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF7-ROBOT)
(LEFTOF1-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF13-ROBOT)
(ABOVEOF18-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF1-ROBOT)
(RIGHTOF14-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF15-ROBOT)
(RIGHTOF19-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF8-ROBOT)
(LEFTOF3-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF4-ROBOT)
(ABOVEOF15-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF13-ROBOT)
(BELOWOF8-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF1-ROBOT)
(RIGHTOF17-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF4-ROBOT)
(RIGHTOF15-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF7-ROBOT)
(RIGHTOF19-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF19-ROBOT)
(BELOWOF3-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF6-ROBOT)
(RIGHTOF17-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF7-ROBOT)
(ABOVEOF13-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF14-ROBOT)
(LEFTOF8-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF8-ROBOT)
(ABOVEOF12-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF6-ROBOT)
(ABOVEOF19-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF10-ROBOT)
(RIGHTOF18-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF15-ROBOT)
(LEFTOF11-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF12-ROBOT)
(LEFTOF5-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF3-ROBOT)
(ABOVEOF15-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF13-ROBOT)
(RIGHTOF19-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF16-ROBOT)
(LEFTOF8-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF5-ROBOT)
(ABOVEOF16-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF12-ROBOT)
(BELOWOF10-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF6-ROBOT)
(BELOWOF2-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF2-ROBOT)
(ABOVEOF12-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF5-ROBOT)
(ABOVEOF11-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF6-ROBOT)
(RIGHTOF15-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF18-ROBOT)
(BELOWOF6-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF2-ROBOT)
(RIGHTOF13-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF13-ROBOT)
(LEFTOF3-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF7-ROBOT)
(RIGHTOF11-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF16-ROBOT)
(BELOWOF8-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF16-ROBOT)
(BELOWOF2-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF15-ROBOT)
(LEFTOF3-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF5-ROBOT)
(RIGHTOF9-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF8-ROBOT)
(LEFTOF2-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF19-ROBOT)
(BELOWOF1-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF17-ROBOT)
(LEFTOF14-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF11-ROBOT)
(BELOWOF8-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF4-ROBOT)
(ABOVEOF10-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF19-ROBOT)
(BELOWOF9-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF3-ROBOT)
(RIGHTOF14-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF14-ROBOT)
(BELOWOF3-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF17-ROBOT)
(BELOWOF9-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF14-ROBOT)
(LEFTOF5-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF8-ROBOT)
(ABOVEOF10-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF5-ROBOT)
(BELOWOF4-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF6-ROBOT)
(LEFTOF4-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF6-ROBOT)
(ABOVEOF6-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF8-ROBOT)
(ABOVEOF19-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF2-ROBOT)
(RIGHTOF14-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF17-ROBOT)
(BELOWOF1-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF1-ROBOT)
(RIGHTOF19-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF16-ROBOT)
(LEFTOF7-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF4-ROBOT)
(LEFTOF1-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF17-ROBOT)
(BELOWOF5-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF8-ROBOT)
(ABOVEOF18-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF4-ROBOT)
(RIGHTOF8-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF19-ROBOT)
(BELOWOF7-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF9-ROBOT)
(RIGHTOF16-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF5-ROBOT)
(RIGHTOF18-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF17-ROBOT)
(LEFTOF5-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF15-ROBOT)
(LEFTOF1-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF6-ROBOT)
(BELOWOF3-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF3-ROBOT)
(ABOVEOF18-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF15-ROBOT)
(BELOWOF1-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF14-ROBOT)
(LEFTOF7-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF5-ROBOT)
(RIGHTOF11-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF11-ROBOT)
(LEFTOF3-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF19-ROBOT)
(BELOWOF13-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF3-ROBOT)
(ABOVEOF12-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF4-ROBOT)
(RIGHTOF19-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF2-ROBOT)
(ABOVEOF19-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF1-ROBOT)
(LEFTOF1-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF19-ROBOT)
(BELOWOF16-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF12-ROBOT)
(LEFTOF2-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF1-ROBOT)
(RIGHTOF12-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF18-ROBOT)
(RIGHTOF19-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF17-ROBOT)
(LEFTOF13-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF5-ROBOT)
(ABOVEOF10-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF17-ROBOT)
(LEFTOF2-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF15-ROBOT)
(LEFTOF5-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF16-ROBOT)
(LEFTOF3-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF14-ROBOT)
(LEFTOF13-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF13-ROBOT)
(LEFTOF6-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF18-ROBOT)
(BELOWOF5-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF13-ROBOT)
(LEFTOF5-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF19-ROBOT)
(LEFTOF2-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF6-ROBOT)
(ABOVEOF16-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF16-ROBOT)
(LEFTOF1-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF13-ROBOT)
(BELOWOF1-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF1-ROBOT)
(RIGHTOF8-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF16-ROBOT)
(LEFTOF13-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF7-ROBOT)
(RIGHTOF13-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF13-ROBOT)
(BELOWOF4-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF14-ROBOT)
(BELOWOF8-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF13-ROBOT)
(BELOWOF3-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF4-ROBOT)
(ABOVEOF11-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF19-ROBOT)
(LEFTOF12-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF4-ROBOT)
(RIGHTOF7-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF18-ROBOT)
(LEFTOF13-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF11-ROBOT)
(RIGHTOF19-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF9-ROBOT)
(BELOWOF1-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF9-ROBOT)
(ABOVEOF12-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF15-ROBOT)
(LEFTOF2-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF4-ROBOT)
(ABOVEOF16-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF11-ROBOT)
(BELOWOF2-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF10-ROBOT)
(RIGHTOF15-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF10-ROBOT)
(RIGHTOF17-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF18-ROBOT)
(BELOWOF10-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF15-ROBOT)
(RIGHTOF17-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF15-ROBOT)
(BELOWOF7-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF4-ROBOT)
(ABOVEOF12-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF12-ROBOT)
(RIGHTOF17-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF15-ROBOT)
(BELOWOF11-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF19-ROBOT)
(BELOWOF10-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF18-ROBOT)
(LEFTOF1-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF5-ROBOT)
(BELOWOF2-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF17-ROBOT)
(BELOWOF15-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF6-ROBOT)
(RIGHTOF16-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF10-ROBOT)
(ABOVEOF13-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF3-ROBOT)
(ABOVEOF16-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF16-ROBOT)
(BELOWOF12-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF6-ROBOT)
(RIGHTOF9-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF14-ROBOT)
(ABOVEOF16-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF2-ROBOT)
(RIGHTOF10-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF15-ROBOT)
(ABOVEOF16-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF2-ROBOT)
(RIGHTOF5-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF4-ROBOT)
(RIGHTOF16-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF6-ROBOT)
(LEFTOF1-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF17-ROBOT)
(BELOWOF3-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF4-ROBOT)
(RIGHTOF12-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF15-ROBOT)
(RIGHTOF15-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF5-ROBOT)
(ABOVEOF15-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF19-ROBOT)
(BELOWOF12-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF3-ROBOT)
(ABOVEOF10-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF12-ROBOT)
(ABOVEOF18-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF19-ROBOT)
(LEFTOF6-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF11-ROBOT)
(BELOWOF10-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF10-ROBOT)
(BELOWOF6-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF19-ROBOT)
(BELOWOF4-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF2-ROBOT)
(RIGHTOF11-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF5-ROBOT)
(ABOVEOF19-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF11-ROBOT)
(ABOVEOF16-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF7-ROBOT)
(BELOWOF1-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF12-ROBOT)
(BELOWOF1-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF9-ROBOT)
(RIGHTOF18-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF1-ROBOT)
(BELOWOF1-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF2-ROBOT)
(ABOVEOF17-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF10-ROBOT)
(LEFTOF4-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF11-ROBOT)
(RIGHTOF13-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF2-ROBOT)
(ABOVEOF9-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF16-ROBOT)
(BELOWOF10-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF10-ROBOT)
(BELOWOF1-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF2-ROBOT)
(RIGHTOF2-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF15-ROBOT)
(BELOWOF10-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF6-ROBOT)
(ABOVEOF15-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF5-ROBOT)
(ABOVEOF12-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF19-ROBOT)
(BELOWOF17-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF14-ROBOT)
(LEFTOF4-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF9-ROBOT)
(BELOWOF5-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF4-ROBOT)
(ABOVEOF17-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF5-ROBOT)
(RIGHTOF19-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF19-ROBOT)
(LEFTOF9-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF17-ROBOT)
(RIGHTOF17-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF7-ROBOT)
(ABOVEOF17-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF10-ROBOT)
(LEFTOF3-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF2-ROBOT)
(ABOVEOF13-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF14-ROBOT)
(BELOWOF5-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF13-ROBOT)
(ABOVEOF13-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF3-ROBOT)
(RIGHTOF9-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF4-ROBOT)
(RIGHTOF17-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF10-ROBOT)
(LEFTOF1-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF9-ROBOT)
(RIGHTOF17-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF6-ROBOT)
(BELOWOF4-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF1-ROBOT)
(ABOVEOF8-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF7-ROBOT)
(ABOVEOF18-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF9-ROBOT)
(ABOVEOF15-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF1-ROBOT)
(ABOVEOF18-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF17-ROBOT)
(LEFTOF7-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF16-ROBOT)
(LEFTOF14-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF11-ROBOT)
(RIGHTOF18-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF2-ROBOT)
(RIGHTOF18-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF7-ROBOT)
(RIGHTOF10-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF12-ROBOT)
(ABOVEOF13-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF8-ROBOT)
(ABOVEOF17-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF9-ROBOT)
(LEFTOF8-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF19-ROBOT)
(LEFTOF17-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF11-ROBOT)
(ABOVEOF13-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF2-ROBOT)
(ABOVEOF7-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF10-ROBOT)
(LEFTOF5-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF12-ROBOT)
(RIGHTOF14-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF19-ROBOT)
(LEFTOF3-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF13-ROBOT)
(ABOVEOF15-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF8-ROBOT)
(LEFTOF5-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF18-ROBOT)
(LEFTOF7-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF2-ROBOT)
(RIGHTOF7-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF11-ROBOT)
(ABOVEOF14-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF9-ROBOT)
(BELOWOF4-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF2-ROBOT)
(RIGHTOF3-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF13-ROBOT)
(BELOWOF6-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF4-ROBOT)
(LEFTOF2-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF14-ROBOT)
(LEFTOF11-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF14-ROBOT)
(BELOWOF9-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF11-ROBOT)
(LEFTOF1-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF1-ROBOT)
(ABOVEOF11-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF8-ROBOT)
(RIGHTOF10-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF19-ROBOT)
(BELOWOF11-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF6-ROBOT)
(ABOVEOF12-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF16-ROBOT)
(LEFTOF11-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF9-ROBOT)
(RIGHTOF15-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF10-ROBOT)
(RIGHTOF16-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF8-ROBOT)
(BELOWOF3-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF15-ROBOT)
(RIGHTOF16-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF17-ROBOT)
(BELOWOF10-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF4-ROBOT)
(RIGHTOF11-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF6-ROBOT)
(RIGHTOF18-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF14-ROBOT)
(BELOWOF10-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF13-ROBOT)
(ABOVEOF16-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF18-ROBOT)
(LEFTOF14-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF9-ROBOT)
(LEFTOF9-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF2-ROBOT)
(ABOVEOF14-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF5-ROBOT)
(RIGHTOF16-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF8-ROBOT)
(BELOWOF2-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF19-ROBOT)
(LEFTOF8-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF11-ROBOT)
(RIGHTOF17-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF12-ROBOT)
(LEFTOF3-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF7-ROBOT)
(RIGHTOF12-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF3-ROBOT)
(ABOVEOF7-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF2-ROBOT)
(LEFTOF1-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF12-ROBOT)
(ABOVEOF15-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF17-ROBOT)
(LEFTOF16-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF7-ROBOT)
(LEFTOF6-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF8-ROBOT)
(BELOWOF4-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF5-ROBOT)
(LEFTOF1-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF10-ROBOT)
(RIGHTOF19-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF12-ROBOT)
(ABOVEOF14-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF14-ROBOT)
(BELOWOF7-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF9-ROBOT)
(LEFTOF1-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF10-ROBOT)
(BELOWOF7-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF13-ROBOT)
(RIGHTOF15-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF7-ROBOT)
(LEFTOF3-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF11-ROBOT)
(BELOWOF6-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF3-ROBOT)
(BELOWOF1-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF6-ROBOT)
(LEFTOF5-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF4-ROBOT)
(ABOVEOF14-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF9-ROBOT)
(BELOWOF8-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF4-ROBOT)
(ABOVEOF18-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF4-ROBOT)
(LEFTOF3-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF8-ROBOT)
(RIGHTOF15-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF17-ROBOT)
(LEFTOF8-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF14-ROBOT)
(BELOWOF13-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF9-ROBOT)
(ABOVEOF11-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF11-ROBOT)
(LEFTOF9-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF17-ROBOT)
(BELOWOF13-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF12-ROBOT)
(LEFTOF6-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF14-ROBOT)
(BELOWOF14-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF18-ROBOT)
(BELOWOF9-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF10-ROBOT)
(LEFTOF6-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF17-ROBOT)
(BELOWOF11-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF7-ROBOT)
(ABOVEOF16-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF9-ROBOT)
(RIGHTOF12-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF9-ROBOT)
(BELOWOF3-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF7-ROBOT)
(ABOVEOF12-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF6-ROBOT)
(ABOVEOF9-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF9-ROBOT)
(RIGHTOF10-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF18-ROBOT)
(BELOWOF14-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF14-ROBOT)
(LEFTOF6-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF6-ROBOT)
(ABOVEOF8-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF6-ROBOT)
(RIGHTOF11-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF4-ROBOT)
(BELOWOF1-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF4-ROBOT)
(RIGHTOF9-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF5-ROBOT)
(BELOWOF1-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF7-ROBOT)
(ABOVEOF11-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF9-ROBOT)
(ABOVEOF13-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF10-ROBOT)
(LEFTOF10-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF19-ROBOT)
(BELOWOF14-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF13-ROBOT)
(LEFTOF10-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF11-ROBOT)
(BELOWOF11-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF14-ROBOT)
(LEFTOF14-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF16-ROBOT)
(ABOVEOF17-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF7-ROBOT)
(ABOVEOF8-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF19-ROBOT)
(BELOWOF19-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF12-ROBOT)
(BELOWOF11-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF17-ROBOT)
(BELOWOF12-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF4-ROBOT)
(ABOVEOF4-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF7-ROBOT)
(BELOWOF4-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF9-ROBOT)
(RIGHTOF14-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF15-ROBOT)
(LEFTOF12-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF14-ROBOT)
(LEFTOF10-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF13-ROBOT)
(LEFTOF8-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF12-ROBOT)
(LEFTOF10-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF6-ROBOT)
(LEFTOF3-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF19-ROBOT)
(LEFTOF16-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF16-ROBOT)
(RIGHTOF16-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF18-ROBOT)
(BELOWOF15-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF3-ROBOT)
(LEFTOF3-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF12-ROBOT)
(RIGHTOF16-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF5-ROBOT)
(BELOWOF3-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF7-ROBOT)
(BELOWOF6-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF5-ROBOT)
(LEFTOF5-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF13-ROBOT)
(LEFTOF9-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF7-ROBOT)
(RIGHTOF9-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF17-ROBOT)
(BELOWOF14-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF11-ROBOT)
(ABOVEOF18-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF10-ROBOT)
(RIGHTOF11-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF12-ROBOT)
(LEFTOF8-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF15-ROBOT)
(BELOWOF15-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF19-ROBOT)
(BELOWOF18-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF10-ROBOT)
(BELOWOF10-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF2-ROBOT)
(BELOWOF1-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF5-ROBOT)
(BELOWOF5-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF16-ROBOT)
(ABOVEOF18-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF9-ROBOT)
(BELOWOF9-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF6-ROBOT)
(BELOWOF5-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF16-ROBOT)
(RIGHTOF18-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF18-ROBOT)
(LEFTOF17-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF18-ROBOT)
(RIGHTOF18-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF7-ROBOT)
(BELOWOF7-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF5-ROBOT)
(LEFTOF4-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF13-ROBOT)
(LEFTOF12-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF3-ROBOT)
(BELOWOF2-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF3-ROBOT)
(LEFTOF1-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF8-ROBOT)
(BELOWOF8-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF10-ROBOT)
(BELOWOF9-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF4-ROBOT)
(LEFTOF4-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF15-ROBOT)
(RIGHTOF18-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF6-ROBOT)
(RIGHTOF8-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF13-ROBOT)
(LEFTOF13-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF2-ROBOT)
(ABOVEOF2-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF1-ROBOT)
(ABOVEOF6-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF15-ROBOT)
(BELOWOF14-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF3-ROBOT)
(ABOVEOF4-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF8-ROBOT)
(LEFTOF8-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF18-ROBOT)
(BELOWOF17-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF15-ROBOT)
(LEFTOF14-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF7-ROBOT)
(BELOWOF5-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF7-ROBOT)
(LEFTOF5-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF18-ROBOT)
(ABOVEOF18-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF8-ROBOT)
(LEFTOF7-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF17-ROBOT)
(BELOWOF17-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF8-ROBOT)
(BELOWOF5-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF11-ROBOT)
(RIGHTOF11-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF11-ROBOT)
(RIGHTOF12-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF16-ROBOT)
(ABOVEOF16-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF12-ROBOT)
(RIGHTOF12-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF6-ROBOT)
(RIGHTOF6-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF7-ROBOT)
(RIGHTOF7-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF19-ROBOT)
(LEFTOF19-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF12-ROBOT)
(BELOWOF12-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF3-ROBOT)
(BELOWOF3-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(NOT-CHECKCONSISTENCY-)
(not (CHECKCONSISTENCY-))
)
)
(:derived (Flag4-)(and (NOT-CHECKCONSISTENCY-)(NOT-ERROR-)(Flag3-)))

(:derived (Flag284-)(and (ABOVEOF19-ROBOT)))

(:derived (Flag284-)(and (ABOVEOF6-ROBOT)))

(:derived (Flag284-)(and (ABOVEOF11-ROBOT)))

(:derived (Flag284-)(and (ABOVEOF10-ROBOT)))

(:derived (Flag284-)(and (ABOVEOF2-ROBOT)))

(:derived (Flag284-)(and (ABOVEOF7-ROBOT)))

(:derived (Flag284-)(and (ABOVEOF12-ROBOT)))

(:derived (Flag284-)(and (ABOVEOF17-ROBOT)))

(:derived (Flag284-)(and (ABOVEOF15-ROBOT)))

(:derived (Flag284-)(and (ABOVEOF8-ROBOT)))

(:derived (Flag284-)(and (ABOVEOF9-ROBOT)))

(:derived (Flag284-)(and (ABOVEOF16-ROBOT)))

(:derived (Flag284-)(and (ABOVEOF4-ROBOT)))

(:derived (Flag284-)(and (ABOVEOF3-ROBOT)))

(:derived (Flag284-)(and (ABOVEOF13-ROBOT)))

(:derived (Flag284-)(and (ABOVEOF14-ROBOT)))

(:derived (Flag284-)(and (ABOVEOF5-ROBOT)))

(:derived (Flag284-)(and (ABOVEOF1-ROBOT)))

(:derived (Flag284-)(and (ABOVEOF18-ROBOT)))

(:derived (Flag285-)(and (ABOVEOF19-ROBOT)))

(:derived (Flag285-)(and (ABOVEOF11-ROBOT)))

(:derived (Flag285-)(and (ABOVEOF6-ROBOT)))

(:derived (Flag285-)(and (ABOVEOF10-ROBOT)))

(:derived (Flag285-)(and (ABOVEOF2-ROBOT)))

(:derived (Flag285-)(and (ABOVEOF7-ROBOT)))

(:derived (Flag285-)(and (ABOVEOF12-ROBOT)))

(:derived (Flag285-)(and (ABOVEOF17-ROBOT)))

(:derived (Flag285-)(and (ABOVEOF15-ROBOT)))

(:derived (Flag285-)(and (ABOVEOF8-ROBOT)))

(:derived (Flag285-)(and (ABOVEOF9-ROBOT)))

(:derived (Flag285-)(and (ABOVEOF16-ROBOT)))

(:derived (Flag285-)(and (ABOVEOF4-ROBOT)))

(:derived (Flag285-)(and (ABOVEOF3-ROBOT)))

(:derived (Flag285-)(and (ABOVEOF13-ROBOT)))

(:derived (Flag285-)(and (ABOVEOF14-ROBOT)))

(:derived (Flag285-)(and (ABOVEOF5-ROBOT)))

(:derived (Flag285-)(and (ABOVEOF18-ROBOT)))

(:derived (Flag286-)(and (ABOVEOF19-ROBOT)))

(:derived (Flag286-)(and (ABOVEOF11-ROBOT)))

(:derived (Flag286-)(and (ABOVEOF6-ROBOT)))

(:derived (Flag286-)(and (ABOVEOF10-ROBOT)))

(:derived (Flag286-)(and (ABOVEOF8-ROBOT)))

(:derived (Flag286-)(and (ABOVEOF7-ROBOT)))

(:derived (Flag286-)(and (ABOVEOF12-ROBOT)))

(:derived (Flag286-)(and (ABOVEOF17-ROBOT)))

(:derived (Flag286-)(and (ABOVEOF15-ROBOT)))

(:derived (Flag286-)(and (ABOVEOF9-ROBOT)))

(:derived (Flag286-)(and (ABOVEOF16-ROBOT)))

(:derived (Flag286-)(and (ABOVEOF4-ROBOT)))

(:derived (Flag286-)(and (ABOVEOF3-ROBOT)))

(:derived (Flag286-)(and (ABOVEOF13-ROBOT)))

(:derived (Flag286-)(and (ABOVEOF14-ROBOT)))

(:derived (Flag286-)(and (ABOVEOF5-ROBOT)))

(:derived (Flag286-)(and (ABOVEOF18-ROBOT)))

(:derived (Flag287-)(and (ABOVEOF19-ROBOT)))

(:derived (Flag287-)(and (ABOVEOF11-ROBOT)))

(:derived (Flag287-)(and (ABOVEOF6-ROBOT)))

(:derived (Flag287-)(and (ABOVEOF10-ROBOT)))

(:derived (Flag287-)(and (ABOVEOF8-ROBOT)))

(:derived (Flag287-)(and (ABOVEOF7-ROBOT)))

(:derived (Flag287-)(and (ABOVEOF12-ROBOT)))

(:derived (Flag287-)(and (ABOVEOF17-ROBOT)))

(:derived (Flag287-)(and (ABOVEOF15-ROBOT)))

(:derived (Flag287-)(and (ABOVEOF9-ROBOT)))

(:derived (Flag287-)(and (ABOVEOF16-ROBOT)))

(:derived (Flag287-)(and (ABOVEOF4-ROBOT)))

(:derived (Flag287-)(and (ABOVEOF13-ROBOT)))

(:derived (Flag287-)(and (ABOVEOF14-ROBOT)))

(:derived (Flag287-)(and (ABOVEOF5-ROBOT)))

(:derived (Flag287-)(and (ABOVEOF18-ROBOT)))

(:derived (Flag288-)(and (ABOVEOF19-ROBOT)))

(:derived (Flag288-)(and (ABOVEOF11-ROBOT)))

(:derived (Flag288-)(and (ABOVEOF6-ROBOT)))

(:derived (Flag288-)(and (ABOVEOF7-ROBOT)))

(:derived (Flag288-)(and (ABOVEOF8-ROBOT)))

(:derived (Flag288-)(and (ABOVEOF10-ROBOT)))

(:derived (Flag288-)(and (ABOVEOF12-ROBOT)))

(:derived (Flag288-)(and (ABOVEOF17-ROBOT)))

(:derived (Flag288-)(and (ABOVEOF15-ROBOT)))

(:derived (Flag288-)(and (ABOVEOF9-ROBOT)))

(:derived (Flag288-)(and (ABOVEOF16-ROBOT)))

(:derived (Flag288-)(and (ABOVEOF13-ROBOT)))

(:derived (Flag288-)(and (ABOVEOF14-ROBOT)))

(:derived (Flag288-)(and (ABOVEOF5-ROBOT)))

(:derived (Flag288-)(and (ABOVEOF18-ROBOT)))

(:derived (Flag289-)(and (ABOVEOF19-ROBOT)))

(:derived (Flag289-)(and (ABOVEOF11-ROBOT)))

(:derived (Flag289-)(and (ABOVEOF6-ROBOT)))

(:derived (Flag289-)(and (ABOVEOF7-ROBOT)))

(:derived (Flag289-)(and (ABOVEOF8-ROBOT)))

(:derived (Flag289-)(and (ABOVEOF10-ROBOT)))

(:derived (Flag289-)(and (ABOVEOF12-ROBOT)))

(:derived (Flag289-)(and (ABOVEOF17-ROBOT)))

(:derived (Flag289-)(and (ABOVEOF15-ROBOT)))

(:derived (Flag289-)(and (ABOVEOF9-ROBOT)))

(:derived (Flag289-)(and (ABOVEOF16-ROBOT)))

(:derived (Flag289-)(and (ABOVEOF13-ROBOT)))

(:derived (Flag289-)(and (ABOVEOF14-ROBOT)))

(:derived (Flag289-)(and (ABOVEOF18-ROBOT)))

(:derived (Flag290-)(and (ABOVEOF19-ROBOT)))

(:derived (Flag290-)(and (ABOVEOF11-ROBOT)))

(:derived (Flag290-)(and (ABOVEOF7-ROBOT)))

(:derived (Flag290-)(and (ABOVEOF8-ROBOT)))

(:derived (Flag290-)(and (ABOVEOF10-ROBOT)))

(:derived (Flag290-)(and (ABOVEOF12-ROBOT)))

(:derived (Flag290-)(and (ABOVEOF17-ROBOT)))

(:derived (Flag290-)(and (ABOVEOF15-ROBOT)))

(:derived (Flag290-)(and (ABOVEOF9-ROBOT)))

(:derived (Flag290-)(and (ABOVEOF16-ROBOT)))

(:derived (Flag290-)(and (ABOVEOF13-ROBOT)))

(:derived (Flag290-)(and (ABOVEOF14-ROBOT)))

(:derived (Flag290-)(and (ABOVEOF18-ROBOT)))

(:derived (Flag291-)(and (ABOVEOF19-ROBOT)))

(:derived (Flag291-)(and (ABOVEOF11-ROBOT)))

(:derived (Flag291-)(and (ABOVEOF8-ROBOT)))

(:derived (Flag291-)(and (ABOVEOF10-ROBOT)))

(:derived (Flag291-)(and (ABOVEOF12-ROBOT)))

(:derived (Flag291-)(and (ABOVEOF17-ROBOT)))

(:derived (Flag291-)(and (ABOVEOF15-ROBOT)))

(:derived (Flag291-)(and (ABOVEOF9-ROBOT)))

(:derived (Flag291-)(and (ABOVEOF16-ROBOT)))

(:derived (Flag291-)(and (ABOVEOF13-ROBOT)))

(:derived (Flag291-)(and (ABOVEOF14-ROBOT)))

(:derived (Flag291-)(and (ABOVEOF18-ROBOT)))

(:derived (Flag292-)(and (ABOVEOF19-ROBOT)))

(:derived (Flag292-)(and (ABOVEOF11-ROBOT)))

(:derived (Flag292-)(and (ABOVEOF10-ROBOT)))

(:derived (Flag292-)(and (ABOVEOF12-ROBOT)))

(:derived (Flag292-)(and (ABOVEOF17-ROBOT)))

(:derived (Flag292-)(and (ABOVEOF15-ROBOT)))

(:derived (Flag292-)(and (ABOVEOF9-ROBOT)))

(:derived (Flag292-)(and (ABOVEOF16-ROBOT)))

(:derived (Flag292-)(and (ABOVEOF13-ROBOT)))

(:derived (Flag292-)(and (ABOVEOF14-ROBOT)))

(:derived (Flag292-)(and (ABOVEOF18-ROBOT)))

(:derived (Flag293-)(and (ABOVEOF19-ROBOT)))

(:derived (Flag293-)(and (ABOVEOF11-ROBOT)))

(:derived (Flag293-)(and (ABOVEOF10-ROBOT)))

(:derived (Flag293-)(and (ABOVEOF12-ROBOT)))

(:derived (Flag293-)(and (ABOVEOF17-ROBOT)))

(:derived (Flag293-)(and (ABOVEOF15-ROBOT)))

(:derived (Flag293-)(and (ABOVEOF16-ROBOT)))

(:derived (Flag293-)(and (ABOVEOF13-ROBOT)))

(:derived (Flag293-)(and (ABOVEOF14-ROBOT)))

(:derived (Flag293-)(and (ABOVEOF18-ROBOT)))

(:derived (Flag294-)(and (ABOVEOF19-ROBOT)))

(:derived (Flag294-)(and (ABOVEOF11-ROBOT)))

(:derived (Flag294-)(and (ABOVEOF12-ROBOT)))

(:derived (Flag294-)(and (ABOVEOF17-ROBOT)))

(:derived (Flag294-)(and (ABOVEOF15-ROBOT)))

(:derived (Flag294-)(and (ABOVEOF16-ROBOT)))

(:derived (Flag294-)(and (ABOVEOF13-ROBOT)))

(:derived (Flag294-)(and (ABOVEOF14-ROBOT)))

(:derived (Flag294-)(and (ABOVEOF18-ROBOT)))

(:derived (Flag295-)(and (ABOVEOF19-ROBOT)))

(:derived (Flag295-)(and (ABOVEOF12-ROBOT)))

(:derived (Flag295-)(and (ABOVEOF17-ROBOT)))

(:derived (Flag295-)(and (ABOVEOF15-ROBOT)))

(:derived (Flag295-)(and (ABOVEOF16-ROBOT)))

(:derived (Flag295-)(and (ABOVEOF13-ROBOT)))

(:derived (Flag295-)(and (ABOVEOF14-ROBOT)))

(:derived (Flag295-)(and (ABOVEOF18-ROBOT)))

(:derived (Flag296-)(and (ABOVEOF19-ROBOT)))

(:derived (Flag296-)(and (ABOVEOF17-ROBOT)))

(:derived (Flag296-)(and (ABOVEOF15-ROBOT)))

(:derived (Flag296-)(and (ABOVEOF16-ROBOT)))

(:derived (Flag296-)(and (ABOVEOF13-ROBOT)))

(:derived (Flag296-)(and (ABOVEOF14-ROBOT)))

(:derived (Flag296-)(and (ABOVEOF18-ROBOT)))

(:derived (Flag297-)(and (ABOVEOF19-ROBOT)))

(:derived (Flag297-)(and (ABOVEOF17-ROBOT)))

(:derived (Flag297-)(and (ABOVEOF15-ROBOT)))

(:derived (Flag297-)(and (ABOVEOF16-ROBOT)))

(:derived (Flag297-)(and (ABOVEOF14-ROBOT)))

(:derived (Flag297-)(and (ABOVEOF18-ROBOT)))

(:derived (Flag298-)(and (ABOVEOF18-ROBOT)))

(:derived (Flag298-)(and (ABOVEOF19-ROBOT)))

(:derived (Flag298-)(and (ABOVEOF17-ROBOT)))

(:derived (Flag298-)(and (ABOVEOF15-ROBOT)))

(:derived (Flag298-)(and (ABOVEOF16-ROBOT)))

(:derived (Flag299-)(and (ABOVEOF18-ROBOT)))

(:derived (Flag299-)(and (ABOVEOF19-ROBOT)))

(:derived (Flag299-)(and (ABOVEOF17-ROBOT)))

(:derived (Flag299-)(and (ABOVEOF16-ROBOT)))

(:derived (Flag300-)(and (ABOVEOF19-ROBOT)))

(:derived (Flag300-)(and (ABOVEOF17-ROBOT)))

(:derived (Flag300-)(and (ABOVEOF18-ROBOT)))

(:derived (Flag301-)(and (ABOVEOF19-ROBOT)))

(:derived (Flag301-)(and (ABOVEOF18-ROBOT)))

(:derived (Flag302-)(and (BELOWOF2-ROBOT)))

(:derived (Flag302-)(and (BELOWOF1-ROBOT)))

(:derived (Flag303-)(and (BELOWOF2-ROBOT)))

(:derived (Flag303-)(and (BELOWOF1-ROBOT)))

(:derived (Flag303-)(and (BELOWOF3-ROBOT)))

(:derived (Flag304-)(and (BELOWOF2-ROBOT)))

(:derived (Flag304-)(and (BELOWOF1-ROBOT)))

(:derived (Flag304-)(and (BELOWOF3-ROBOT)))

(:derived (Flag304-)(and (BELOWOF4-ROBOT)))

(:derived (Flag305-)(and (BELOWOF2-ROBOT)))

(:derived (Flag305-)(and (BELOWOF1-ROBOT)))

(:derived (Flag305-)(and (BELOWOF3-ROBOT)))

(:derived (Flag305-)(and (BELOWOF4-ROBOT)))

(:derived (Flag305-)(and (BELOWOF5-ROBOT)))

(:derived (Flag306-)(and (BELOWOF6-ROBOT)))

(:derived (Flag306-)(and (BELOWOF5-ROBOT)))

(:derived (Flag306-)(and (BELOWOF2-ROBOT)))

(:derived (Flag306-)(and (BELOWOF1-ROBOT)))

(:derived (Flag306-)(and (BELOWOF4-ROBOT)))

(:derived (Flag306-)(and (BELOWOF3-ROBOT)))

(:derived (Flag307-)(and (BELOWOF6-ROBOT)))

(:derived (Flag307-)(and (BELOWOF5-ROBOT)))

(:derived (Flag307-)(and (BELOWOF2-ROBOT)))

(:derived (Flag307-)(and (BELOWOF1-ROBOT)))

(:derived (Flag307-)(and (BELOWOF4-ROBOT)))

(:derived (Flag307-)(and (BELOWOF7-ROBOT)))

(:derived (Flag307-)(and (BELOWOF3-ROBOT)))

(:derived (Flag308-)(and (BELOWOF6-ROBOT)))

(:derived (Flag308-)(and (BELOWOF5-ROBOT)))

(:derived (Flag308-)(and (BELOWOF2-ROBOT)))

(:derived (Flag308-)(and (BELOWOF1-ROBOT)))

(:derived (Flag308-)(and (BELOWOF4-ROBOT)))

(:derived (Flag308-)(and (BELOWOF7-ROBOT)))

(:derived (Flag308-)(and (BELOWOF3-ROBOT)))

(:derived (Flag308-)(and (BELOWOF8-ROBOT)))

(:derived (Flag309-)(and (BELOWOF6-ROBOT)))

(:derived (Flag309-)(and (BELOWOF5-ROBOT)))

(:derived (Flag309-)(and (BELOWOF2-ROBOT)))

(:derived (Flag309-)(and (BELOWOF1-ROBOT)))

(:derived (Flag309-)(and (BELOWOF4-ROBOT)))

(:derived (Flag309-)(and (BELOWOF7-ROBOT)))

(:derived (Flag309-)(and (BELOWOF9-ROBOT)))

(:derived (Flag309-)(and (BELOWOF3-ROBOT)))

(:derived (Flag309-)(and (BELOWOF8-ROBOT)))

(:derived (Flag310-)(and (BELOWOF10-ROBOT)))

(:derived (Flag310-)(and (BELOWOF6-ROBOT)))

(:derived (Flag310-)(and (BELOWOF5-ROBOT)))

(:derived (Flag310-)(and (BELOWOF2-ROBOT)))

(:derived (Flag310-)(and (BELOWOF1-ROBOT)))

(:derived (Flag310-)(and (BELOWOF4-ROBOT)))

(:derived (Flag310-)(and (BELOWOF7-ROBOT)))

(:derived (Flag310-)(and (BELOWOF9-ROBOT)))

(:derived (Flag310-)(and (BELOWOF3-ROBOT)))

(:derived (Flag310-)(and (BELOWOF8-ROBOT)))

(:derived (Flag311-)(and (BELOWOF10-ROBOT)))

(:derived (Flag311-)(and (BELOWOF11-ROBOT)))

(:derived (Flag311-)(and (BELOWOF6-ROBOT)))

(:derived (Flag311-)(and (BELOWOF5-ROBOT)))

(:derived (Flag311-)(and (BELOWOF2-ROBOT)))

(:derived (Flag311-)(and (BELOWOF1-ROBOT)))

(:derived (Flag311-)(and (BELOWOF4-ROBOT)))

(:derived (Flag311-)(and (BELOWOF7-ROBOT)))

(:derived (Flag311-)(and (BELOWOF9-ROBOT)))

(:derived (Flag311-)(and (BELOWOF3-ROBOT)))

(:derived (Flag311-)(and (BELOWOF8-ROBOT)))

(:derived (Flag312-)(and (BELOWOF10-ROBOT)))

(:derived (Flag312-)(and (BELOWOF11-ROBOT)))

(:derived (Flag312-)(and (BELOWOF6-ROBOT)))

(:derived (Flag312-)(and (BELOWOF5-ROBOT)))

(:derived (Flag312-)(and (BELOWOF2-ROBOT)))

(:derived (Flag312-)(and (BELOWOF1-ROBOT)))

(:derived (Flag312-)(and (BELOWOF4-ROBOT)))

(:derived (Flag312-)(and (BELOWOF12-ROBOT)))

(:derived (Flag312-)(and (BELOWOF7-ROBOT)))

(:derived (Flag312-)(and (BELOWOF9-ROBOT)))

(:derived (Flag312-)(and (BELOWOF3-ROBOT)))

(:derived (Flag312-)(and (BELOWOF8-ROBOT)))

(:derived (Flag313-)(and (BELOWOF10-ROBOT)))

(:derived (Flag313-)(and (BELOWOF11-ROBOT)))

(:derived (Flag313-)(and (BELOWOF6-ROBOT)))

(:derived (Flag313-)(and (BELOWOF5-ROBOT)))

(:derived (Flag313-)(and (BELOWOF2-ROBOT)))

(:derived (Flag313-)(and (BELOWOF1-ROBOT)))

(:derived (Flag313-)(and (BELOWOF4-ROBOT)))

(:derived (Flag313-)(and (BELOWOF12-ROBOT)))

(:derived (Flag313-)(and (BELOWOF7-ROBOT)))

(:derived (Flag313-)(and (BELOWOF9-ROBOT)))

(:derived (Flag313-)(and (BELOWOF3-ROBOT)))

(:derived (Flag313-)(and (BELOWOF13-ROBOT)))

(:derived (Flag313-)(and (BELOWOF8-ROBOT)))

(:derived (Flag314-)(and (BELOWOF10-ROBOT)))

(:derived (Flag314-)(and (BELOWOF11-ROBOT)))

(:derived (Flag314-)(and (BELOWOF6-ROBOT)))

(:derived (Flag314-)(and (BELOWOF5-ROBOT)))

(:derived (Flag314-)(and (BELOWOF2-ROBOT)))

(:derived (Flag314-)(and (BELOWOF1-ROBOT)))

(:derived (Flag314-)(and (BELOWOF4-ROBOT)))

(:derived (Flag314-)(and (BELOWOF12-ROBOT)))

(:derived (Flag314-)(and (BELOWOF7-ROBOT)))

(:derived (Flag314-)(and (BELOWOF9-ROBOT)))

(:derived (Flag314-)(and (BELOWOF3-ROBOT)))

(:derived (Flag314-)(and (BELOWOF13-ROBOT)))

(:derived (Flag314-)(and (BELOWOF8-ROBOT)))

(:derived (Flag314-)(and (BELOWOF14-ROBOT)))

(:derived (Flag315-)(and (BELOWOF10-ROBOT)))

(:derived (Flag315-)(and (BELOWOF11-ROBOT)))

(:derived (Flag315-)(and (BELOWOF15-ROBOT)))

(:derived (Flag315-)(and (BELOWOF6-ROBOT)))

(:derived (Flag315-)(and (BELOWOF5-ROBOT)))

(:derived (Flag315-)(and (BELOWOF2-ROBOT)))

(:derived (Flag315-)(and (BELOWOF1-ROBOT)))

(:derived (Flag315-)(and (BELOWOF4-ROBOT)))

(:derived (Flag315-)(and (BELOWOF12-ROBOT)))

(:derived (Flag315-)(and (BELOWOF7-ROBOT)))

(:derived (Flag315-)(and (BELOWOF9-ROBOT)))

(:derived (Flag315-)(and (BELOWOF3-ROBOT)))

(:derived (Flag315-)(and (BELOWOF13-ROBOT)))

(:derived (Flag315-)(and (BELOWOF8-ROBOT)))

(:derived (Flag315-)(and (BELOWOF14-ROBOT)))

(:derived (Flag316-)(and (BELOWOF10-ROBOT)))

(:derived (Flag316-)(and (BELOWOF16-ROBOT)))

(:derived (Flag316-)(and (BELOWOF15-ROBOT)))

(:derived (Flag316-)(and (BELOWOF6-ROBOT)))

(:derived (Flag316-)(and (BELOWOF11-ROBOT)))

(:derived (Flag316-)(and (BELOWOF5-ROBOT)))

(:derived (Flag316-)(and (BELOWOF2-ROBOT)))

(:derived (Flag316-)(and (BELOWOF1-ROBOT)))

(:derived (Flag316-)(and (BELOWOF4-ROBOT)))

(:derived (Flag316-)(and (BELOWOF12-ROBOT)))

(:derived (Flag316-)(and (BELOWOF7-ROBOT)))

(:derived (Flag316-)(and (BELOWOF9-ROBOT)))

(:derived (Flag316-)(and (BELOWOF3-ROBOT)))

(:derived (Flag316-)(and (BELOWOF13-ROBOT)))

(:derived (Flag316-)(and (BELOWOF8-ROBOT)))

(:derived (Flag316-)(and (BELOWOF14-ROBOT)))

(:derived (Flag317-)(and (BELOWOF10-ROBOT)))

(:derived (Flag317-)(and (BELOWOF16-ROBOT)))

(:derived (Flag317-)(and (BELOWOF15-ROBOT)))

(:derived (Flag317-)(and (BELOWOF6-ROBOT)))

(:derived (Flag317-)(and (BELOWOF17-ROBOT)))

(:derived (Flag317-)(and (BELOWOF5-ROBOT)))

(:derived (Flag317-)(and (BELOWOF2-ROBOT)))

(:derived (Flag317-)(and (BELOWOF1-ROBOT)))

(:derived (Flag317-)(and (BELOWOF4-ROBOT)))

(:derived (Flag317-)(and (BELOWOF11-ROBOT)))

(:derived (Flag317-)(and (BELOWOF12-ROBOT)))

(:derived (Flag317-)(and (BELOWOF7-ROBOT)))

(:derived (Flag317-)(and (BELOWOF9-ROBOT)))

(:derived (Flag317-)(and (BELOWOF3-ROBOT)))

(:derived (Flag317-)(and (BELOWOF13-ROBOT)))

(:derived (Flag317-)(and (BELOWOF8-ROBOT)))

(:derived (Flag317-)(and (BELOWOF14-ROBOT)))

(:derived (Flag318-)(and (BELOWOF10-ROBOT)))

(:derived (Flag318-)(and (BELOWOF16-ROBOT)))

(:derived (Flag318-)(and (BELOWOF15-ROBOT)))

(:derived (Flag318-)(and (BELOWOF6-ROBOT)))

(:derived (Flag318-)(and (BELOWOF17-ROBOT)))

(:derived (Flag318-)(and (BELOWOF5-ROBOT)))

(:derived (Flag318-)(and (BELOWOF2-ROBOT)))

(:derived (Flag318-)(and (BELOWOF1-ROBOT)))

(:derived (Flag318-)(and (BELOWOF4-ROBOT)))

(:derived (Flag318-)(and (BELOWOF11-ROBOT)))

(:derived (Flag318-)(and (BELOWOF18-ROBOT)))

(:derived (Flag318-)(and (BELOWOF12-ROBOT)))

(:derived (Flag318-)(and (BELOWOF7-ROBOT)))

(:derived (Flag318-)(and (BELOWOF9-ROBOT)))

(:derived (Flag318-)(and (BELOWOF3-ROBOT)))

(:derived (Flag318-)(and (BELOWOF13-ROBOT)))

(:derived (Flag318-)(and (BELOWOF8-ROBOT)))

(:derived (Flag318-)(and (BELOWOF14-ROBOT)))

(:derived (Flag319-)(and (BELOWOF3-ROBOT)))

(:derived (Flag319-)(and (BELOWOF16-ROBOT)))

(:derived (Flag319-)(and (BELOWOF15-ROBOT)))

(:derived (Flag319-)(and (BELOWOF6-ROBOT)))

(:derived (Flag319-)(and (BELOWOF17-ROBOT)))

(:derived (Flag319-)(and (BELOWOF5-ROBOT)))

(:derived (Flag319-)(and (BELOWOF2-ROBOT)))

(:derived (Flag319-)(and (BELOWOF1-ROBOT)))

(:derived (Flag319-)(and (BELOWOF4-ROBOT)))

(:derived (Flag319-)(and (BELOWOF11-ROBOT)))

(:derived (Flag319-)(and (BELOWOF18-ROBOT)))

(:derived (Flag319-)(and (BELOWOF12-ROBOT)))

(:derived (Flag319-)(and (BELOWOF7-ROBOT)))

(:derived (Flag319-)(and (BELOWOF9-ROBOT)))

(:derived (Flag319-)(and (BELOWOF10-ROBOT)))

(:derived (Flag319-)(and (BELOWOF13-ROBOT)))

(:derived (Flag319-)(and (BELOWOF8-ROBOT)))

(:derived (Flag319-)(and (BELOWOF14-ROBOT)))

(:derived (Flag320-)(and (BELOWOF1-ROBOT)(ABOVEOF4-ROBOT)))

(:derived (Flag321-)(and (ABOVEOF18-ROBOT)(BELOWOF1-ROBOT)))

(:derived (Flag322-)(and (BELOWOF1-ROBOT)(ABOVEOF2-ROBOT)))

(:derived (Flag323-)(and (ABOVEOF19-ROBOT)(BELOWOF1-ROBOT)))

(:derived (Flag324-)(and (BELOWOF1-ROBOT)(ABOVEOF1-ROBOT)))

(:derived (Flag325-)(and (ABOVEOF12-ROBOT)(BELOWOF1-ROBOT)))

(:derived (Flag326-)(and (BELOWOF1-ROBOT)(ABOVEOF13-ROBOT)))

(:derived (Flag327-)(and (BELOWOF1-ROBOT)(ABOVEOF7-ROBOT)))

(:derived (Flag328-)(and (ABOVEOF11-ROBOT)(BELOWOF1-ROBOT)))

(:derived (Flag329-)(and (ABOVEOF6-ROBOT)(BELOWOF1-ROBOT)))

(:derived (Flag330-)(and (BELOWOF1-ROBOT)(ABOVEOF5-ROBOT)))

(:derived (Flag331-)(and (BELOWOF1-ROBOT)(ABOVEOF14-ROBOT)))

(:derived (Flag332-)(and (BELOWOF1-ROBOT)(ABOVEOF16-ROBOT)))

(:derived (Flag333-)(and (ABOVEOF10-ROBOT)(BELOWOF1-ROBOT)))

(:derived (Flag334-)(and (BELOWOF1-ROBOT)(ABOVEOF15-ROBOT)))

(:derived (Flag335-)(and (ABOVEOF8-ROBOT)(BELOWOF1-ROBOT)))

(:derived (Flag336-)(and (ABOVEOF3-ROBOT)(BELOWOF1-ROBOT)))

(:derived (Flag337-)(and (BELOWOF1-ROBOT)(ABOVEOF9-ROBOT)))

(:derived (Flag338-)(and (ABOVEOF17-ROBOT)(BELOWOF1-ROBOT)))

(:derived (Flag339-)(and (BELOWOF1-ROBOT)))

(:derived (Flag340-)(and (ABOVEOF18-ROBOT)(BELOWOF2-ROBOT)))

(:derived (Flag341-)(and (ABOVEOF1-ROBOT)(BELOWOF2-ROBOT)))

(:derived (Flag342-)(and (ABOVEOF7-ROBOT)(BELOWOF2-ROBOT)))

(:derived (Flag343-)(and (ABOVEOF15-ROBOT)(BELOWOF2-ROBOT)))

(:derived (Flag344-)(and (ABOVEOF19-ROBOT)(BELOWOF2-ROBOT)))

(:derived (Flag345-)(and (BELOWOF2-ROBOT)(ABOVEOF13-ROBOT)))

(:derived (Flag346-)(and (ABOVEOF2-ROBOT)(BELOWOF2-ROBOT)))

(:derived (Flag347-)(and (ABOVEOF16-ROBOT)(BELOWOF2-ROBOT)))

(:derived (Flag348-)(and (ABOVEOF4-ROBOT)(BELOWOF2-ROBOT)))

(:derived (Flag349-)(and (BELOWOF1-ROBOT)(ABOVEOF17-ROBOT)))

(:derived (Flag350-)(and (BELOWOF2-ROBOT)(ABOVEOF14-ROBOT)))

(:derived (Flag351-)(and (ABOVEOF6-ROBOT)(BELOWOF2-ROBOT)))

(:derived (Flag352-)(and (ABOVEOF9-ROBOT)(BELOWOF2-ROBOT)))

(:derived (Flag353-)(and (ABOVEOF17-ROBOT)(BELOWOF2-ROBOT)))

(:derived (Flag354-)(and (ABOVEOF11-ROBOT)(BELOWOF2-ROBOT)))

(:derived (Flag355-)(and (ABOVEOF10-ROBOT)(BELOWOF2-ROBOT)))

(:derived (Flag356-)(and (ABOVEOF8-ROBOT)(BELOWOF2-ROBOT)))

(:derived (Flag357-)(and (ABOVEOF12-ROBOT)(BELOWOF2-ROBOT)))

(:derived (Flag358-)(and (BELOWOF2-ROBOT)(ABOVEOF5-ROBOT)))

(:derived (Flag359-)(and (ABOVEOF3-ROBOT)(BELOWOF2-ROBOT)))

(:derived (Flag361-)(and (BELOWOF3-ROBOT)(ABOVEOF5-ROBOT)))

(:derived (Flag362-)(and (BELOWOF3-ROBOT)(ABOVEOF6-ROBOT)))

(:derived (Flag363-)(and (BELOWOF3-ROBOT)(ABOVEOF7-ROBOT)))

(:derived (Flag364-)(and (BELOWOF3-ROBOT)(ABOVEOF17-ROBOT)))

(:derived (Flag365-)(and (BELOWOF3-ROBOT)(ABOVEOF14-ROBOT)))

(:derived (Flag366-)(and (BELOWOF3-ROBOT)(ABOVEOF16-ROBOT)))

(:derived (Flag367-)(and (BELOWOF3-ROBOT)(ABOVEOF9-ROBOT)))

(:derived (Flag368-)(and (BELOWOF3-ROBOT)(ABOVEOF15-ROBOT)))

(:derived (Flag369-)(and (BELOWOF3-ROBOT)(ABOVEOF2-ROBOT)))

(:derived (Flag370-)(and (ABOVEOF3-ROBOT)(BELOWOF3-ROBOT)))

(:derived (Flag371-)(and (ABOVEOF8-ROBOT)(BELOWOF3-ROBOT)))

(:derived (Flag372-)(and (BELOWOF3-ROBOT)(ABOVEOF4-ROBOT)))

(:derived (Flag373-)(and (BELOWOF3-ROBOT)(ABOVEOF10-ROBOT)))

(:derived (Flag374-)(and (BELOWOF2-ROBOT)(ABOVEOF2-ROBOT)))

(:derived (Flag375-)(and (ABOVEOF19-ROBOT)(BELOWOF3-ROBOT)))

(:derived (Flag376-)(and (ABOVEOF12-ROBOT)(BELOWOF3-ROBOT)))

(:derived (Flag377-)(and (BELOWOF3-ROBOT)(ABOVEOF13-ROBOT)))

(:derived (Flag378-)(and (ABOVEOF11-ROBOT)(BELOWOF3-ROBOT)))

(:derived (Flag379-)(and (BELOWOF3-ROBOT)(ABOVEOF18-ROBOT)))

(:derived (Flag381-)(and (ABOVEOF9-ROBOT)(BELOWOF4-ROBOT)))

(:derived (Flag382-)(and (BELOWOF4-ROBOT)(ABOVEOF13-ROBOT)))

(:derived (Flag383-)(and (ABOVEOF3-ROBOT)(BELOWOF4-ROBOT)))

(:derived (Flag384-)(and (ABOVEOF17-ROBOT)(BELOWOF4-ROBOT)))

(:derived (Flag385-)(and (ABOVEOF6-ROBOT)(BELOWOF4-ROBOT)))

(:derived (Flag386-)(and (BELOWOF4-ROBOT)(ABOVEOF4-ROBOT)))

(:derived (Flag387-)(and (ABOVEOF18-ROBOT)(BELOWOF4-ROBOT)))

(:derived (Flag388-)(and (BELOWOF4-ROBOT)(ABOVEOF16-ROBOT)))

(:derived (Flag389-)(and (ABOVEOF8-ROBOT)(BELOWOF4-ROBOT)))

(:derived (Flag390-)(and (ABOVEOF12-ROBOT)(BELOWOF4-ROBOT)))

(:derived (Flag391-)(and (BELOWOF4-ROBOT)(ABOVEOF5-ROBOT)))

(:derived (Flag392-)(and (BELOWOF3-ROBOT)(ABOVEOF3-ROBOT)))

(:derived (Flag393-)(and (ABOVEOF19-ROBOT)(BELOWOF4-ROBOT)))

(:derived (Flag394-)(and (ABOVEOF10-ROBOT)(BELOWOF4-ROBOT)))

(:derived (Flag395-)(and (BELOWOF4-ROBOT)(ABOVEOF15-ROBOT)))

(:derived (Flag396-)(and (BELOWOF3-ROBOT)(ABOVEOF19-ROBOT)))

(:derived (Flag397-)(and (BELOWOF3-ROBOT)(ABOVEOF11-ROBOT)))

(:derived (Flag398-)(and (BELOWOF4-ROBOT)(ABOVEOF7-ROBOT)))

(:derived (Flag399-)(and (ABOVEOF11-ROBOT)(BELOWOF4-ROBOT)))

(:derived (Flag400-)(and (BELOWOF4-ROBOT)(ABOVEOF14-ROBOT)))

(:derived (Flag402-)(and (ABOVEOF17-ROBOT)(BELOWOF5-ROBOT)))

(:derived (Flag403-)(and (ABOVEOF4-ROBOT)(BELOWOF5-ROBOT)))

(:derived (Flag404-)(and (ABOVEOF15-ROBOT)(BELOWOF5-ROBOT)))

(:derived (Flag405-)(and (ABOVEOF8-ROBOT)(BELOWOF5-ROBOT)))

(:derived (Flag406-)(and (ABOVEOF19-ROBOT)(BELOWOF5-ROBOT)))

(:derived (Flag407-)(and (ABOVEOF16-ROBOT)(BELOWOF5-ROBOT)))

(:derived (Flag408-)(and (ABOVEOF10-ROBOT)(BELOWOF5-ROBOT)))

(:derived (Flag409-)(and (ABOVEOF14-ROBOT)(BELOWOF5-ROBOT)))

(:derived (Flag410-)(and (ABOVEOF13-ROBOT)(BELOWOF5-ROBOT)))

(:derived (Flag411-)(and (ABOVEOF16-ROBOT)(BELOWOF4-ROBOT)))

(:derived (Flag412-)(and (ABOVEOF5-ROBOT)(BELOWOF5-ROBOT)))

(:derived (Flag413-)(and (ABOVEOF7-ROBOT)(BELOWOF5-ROBOT)))

(:derived (Flag414-)(and (ABOVEOF9-ROBOT)(BELOWOF5-ROBOT)))

(:derived (Flag415-)(and (ABOVEOF18-ROBOT)(BELOWOF5-ROBOT)))

(:derived (Flag416-)(and (ABOVEOF12-ROBOT)(BELOWOF5-ROBOT)))

(:derived (Flag417-)(and (ABOVEOF11-ROBOT)(BELOWOF5-ROBOT)))

(:derived (Flag418-)(and (ABOVEOF6-ROBOT)(BELOWOF5-ROBOT)))

(:derived (Flag420-)(and (BELOWOF5-ROBOT)(ABOVEOF5-ROBOT)))

(:derived (Flag421-)(and (BELOWOF6-ROBOT)(ABOVEOF18-ROBOT)))

(:derived (Flag422-)(and (BELOWOF6-ROBOT)(ABOVEOF6-ROBOT)))

(:derived (Flag423-)(and (ABOVEOF11-ROBOT)(BELOWOF6-ROBOT)))

(:derived (Flag424-)(and (ABOVEOF12-ROBOT)(BELOWOF6-ROBOT)))

(:derived (Flag425-)(and (BELOWOF5-ROBOT)(ABOVEOF13-ROBOT)))

(:derived (Flag426-)(and (BELOWOF6-ROBOT)(ABOVEOF17-ROBOT)))

(:derived (Flag427-)(and (BELOWOF6-ROBOT)(ABOVEOF10-ROBOT)))

(:derived (Flag428-)(and (BELOWOF6-ROBOT)(ABOVEOF15-ROBOT)))

(:derived (Flag429-)(and (BELOWOF6-ROBOT)(ABOVEOF14-ROBOT)))

(:derived (Flag430-)(and (BELOWOF6-ROBOT)(ABOVEOF5-ROBOT)))

(:derived (Flag431-)(and (ABOVEOF8-ROBOT)(BELOWOF6-ROBOT)))

(:derived (Flag432-)(and (BELOWOF6-ROBOT)(ABOVEOF9-ROBOT)))

(:derived (Flag433-)(and (BELOWOF6-ROBOT)(ABOVEOF16-ROBOT)))

(:derived (Flag434-)(and (ABOVEOF19-ROBOT)(BELOWOF6-ROBOT)))

(:derived (Flag435-)(and (BELOWOF6-ROBOT)(ABOVEOF13-ROBOT)))

(:derived (Flag436-)(and (BELOWOF6-ROBOT)(ABOVEOF7-ROBOT)))

(:derived (Flag438-)(and (ABOVEOF6-ROBOT)(BELOWOF7-ROBOT)))

(:derived (Flag439-)(and (BELOWOF7-ROBOT)(ABOVEOF14-ROBOT)))

(:derived (Flag440-)(and (BELOWOF7-ROBOT)(ABOVEOF15-ROBOT)))

(:derived (Flag441-)(and (ABOVEOF7-ROBOT)(BELOWOF7-ROBOT)))

(:derived (Flag442-)(and (ABOVEOF16-ROBOT)(BELOWOF7-ROBOT)))

(:derived (Flag443-)(and (ABOVEOF18-ROBOT)(BELOWOF7-ROBOT)))

(:derived (Flag444-)(and (ABOVEOF10-ROBOT)(BELOWOF6-ROBOT)))

(:derived (Flag445-)(and (ABOVEOF18-ROBOT)(BELOWOF6-ROBOT)))

(:derived (Flag446-)(and (ABOVEOF10-ROBOT)(BELOWOF7-ROBOT)))

(:derived (Flag447-)(and (BELOWOF7-ROBOT)(ABOVEOF13-ROBOT)))

(:derived (Flag448-)(and (ABOVEOF19-ROBOT)(BELOWOF7-ROBOT)))

(:derived (Flag449-)(and (ABOVEOF17-ROBOT)(BELOWOF7-ROBOT)))

(:derived (Flag450-)(and (ABOVEOF8-ROBOT)(BELOWOF7-ROBOT)))

(:derived (Flag451-)(and (ABOVEOF11-ROBOT)(BELOWOF7-ROBOT)))

(:derived (Flag452-)(and (ABOVEOF9-ROBOT)(BELOWOF7-ROBOT)))

(:derived (Flag453-)(and (ABOVEOF6-ROBOT)(BELOWOF6-ROBOT)))

(:derived (Flag454-)(and (ABOVEOF12-ROBOT)(BELOWOF7-ROBOT)))

(:derived (Flag456-)(and (BELOWOF8-ROBOT)(ABOVEOF11-ROBOT)))

(:derived (Flag457-)(and (BELOWOF8-ROBOT)(ABOVEOF14-ROBOT)))

(:derived (Flag458-)(and (BELOWOF8-ROBOT)(ABOVEOF17-ROBOT)))

(:derived (Flag459-)(and (BELOWOF8-ROBOT)(ABOVEOF13-ROBOT)))

(:derived (Flag460-)(and (BELOWOF8-ROBOT)(ABOVEOF10-ROBOT)))

(:derived (Flag461-)(and (BELOWOF8-ROBOT)(ABOVEOF18-ROBOT)))

(:derived (Flag462-)(and (BELOWOF8-ROBOT)(ABOVEOF9-ROBOT)))

(:derived (Flag463-)(and (BELOWOF8-ROBOT)(ABOVEOF7-ROBOT)))

(:derived (Flag464-)(and (ABOVEOF8-ROBOT)(BELOWOF8-ROBOT)))

(:derived (Flag465-)(and (BELOWOF7-ROBOT)(ABOVEOF7-ROBOT)))

(:derived (Flag466-)(and (BELOWOF8-ROBOT)(ABOVEOF19-ROBOT)))

(:derived (Flag467-)(and (BELOWOF8-ROBOT)(ABOVEOF15-ROBOT)))

(:derived (Flag468-)(and (ABOVEOF12-ROBOT)(BELOWOF8-ROBOT)))

(:derived (Flag469-)(and (BELOWOF8-ROBOT)(ABOVEOF16-ROBOT)))

(:derived (Flag471-)(and (ABOVEOF17-ROBOT)(BELOWOF9-ROBOT)))

(:derived (Flag472-)(and (BELOWOF9-ROBOT)(ABOVEOF13-ROBOT)))

(:derived (Flag473-)(and (ABOVEOF11-ROBOT)(BELOWOF9-ROBOT)))

(:derived (Flag474-)(and (BELOWOF8-ROBOT)(ABOVEOF12-ROBOT)))

(:derived (Flag475-)(and (ABOVEOF10-ROBOT)(BELOWOF9-ROBOT)))

(:derived (Flag476-)(and (BELOWOF9-ROBOT)(ABOVEOF14-ROBOT)))

(:derived (Flag477-)(and (ABOVEOF19-ROBOT)(BELOWOF9-ROBOT)))

(:derived (Flag478-)(and (BELOWOF9-ROBOT)(ABOVEOF9-ROBOT)))

(:derived (Flag479-)(and (BELOWOF9-ROBOT)(ABOVEOF16-ROBOT)))

(:derived (Flag480-)(and (BELOWOF8-ROBOT)(ABOVEOF8-ROBOT)))

(:derived (Flag481-)(and (BELOWOF9-ROBOT)(ABOVEOF15-ROBOT)))

(:derived (Flag482-)(and (ABOVEOF18-ROBOT)(BELOWOF9-ROBOT)))

(:derived (Flag483-)(and (ABOVEOF8-ROBOT)(BELOWOF9-ROBOT)))

(:derived (Flag484-)(and (ABOVEOF12-ROBOT)(BELOWOF9-ROBOT)))

(:derived (Flag486-)(and (BELOWOF10-ROBOT)(ABOVEOF10-ROBOT)))

(:derived (Flag487-)(and (BELOWOF10-ROBOT)(ABOVEOF18-ROBOT)))

(:derived (Flag488-)(and (ABOVEOF12-ROBOT)(BELOWOF10-ROBOT)))

(:derived (Flag489-)(and (BELOWOF10-ROBOT)(ABOVEOF14-ROBOT)))

(:derived (Flag490-)(and (ABOVEOF19-ROBOT)(BELOWOF10-ROBOT)))

(:derived (Flag491-)(and (ABOVEOF9-ROBOT)(BELOWOF9-ROBOT)))

(:derived (Flag492-)(and (BELOWOF10-ROBOT)(ABOVEOF13-ROBOT)))

(:derived (Flag493-)(and (BELOWOF10-ROBOT)(ABOVEOF16-ROBOT)))

(:derived (Flag494-)(and (BELOWOF10-ROBOT)(ABOVEOF9-ROBOT)))

(:derived (Flag495-)(and (BELOWOF10-ROBOT)(ABOVEOF15-ROBOT)))

(:derived (Flag496-)(and (BELOWOF10-ROBOT)(ABOVEOF17-ROBOT)))

(:derived (Flag497-)(and (ABOVEOF11-ROBOT)(BELOWOF10-ROBOT)))

(:derived (Flag499-)(and (BELOWOF11-ROBOT)(ABOVEOF16-ROBOT)))

(:derived (Flag500-)(and (BELOWOF11-ROBOT)(ABOVEOF18-ROBOT)))

(:derived (Flag501-)(and (BELOWOF11-ROBOT)(ABOVEOF15-ROBOT)))

(:derived (Flag502-)(and (BELOWOF11-ROBOT)(ABOVEOF17-ROBOT)))

(:derived (Flag503-)(and (BELOWOF11-ROBOT)(ABOVEOF13-ROBOT)))

(:derived (Flag504-)(and (BELOWOF11-ROBOT)(ABOVEOF14-ROBOT)))

(:derived (Flag505-)(and (ABOVEOF19-ROBOT)(BELOWOF11-ROBOT)))

(:derived (Flag506-)(and (BELOWOF11-ROBOT)(ABOVEOF10-ROBOT)))

(:derived (Flag507-)(and (ABOVEOF11-ROBOT)(BELOWOF11-ROBOT)))

(:derived (Flag508-)(and (ABOVEOF12-ROBOT)(BELOWOF11-ROBOT)))

(:derived (Flag510-)(and (BELOWOF12-ROBOT)(ABOVEOF18-ROBOT)))

(:derived (Flag511-)(and (BELOWOF12-ROBOT)(ABOVEOF13-ROBOT)))

(:derived (Flag512-)(and (BELOWOF12-ROBOT)(ABOVEOF15-ROBOT)))

(:derived (Flag513-)(and (BELOWOF12-ROBOT)(ABOVEOF17-ROBOT)))

(:derived (Flag514-)(and (BELOWOF12-ROBOT)(ABOVEOF11-ROBOT)))

(:derived (Flag515-)(and (BELOWOF12-ROBOT)(ABOVEOF14-ROBOT)))

(:derived (Flag516-)(and (ABOVEOF12-ROBOT)(BELOWOF12-ROBOT)))

(:derived (Flag517-)(and (BELOWOF12-ROBOT)(ABOVEOF16-ROBOT)))

(:derived (Flag518-)(and (BELOWOF12-ROBOT)(ABOVEOF19-ROBOT)))

(:derived (Flag520-)(and (ABOVEOF18-ROBOT)(BELOWOF13-ROBOT)))

(:derived (Flag521-)(and (ABOVEOF16-ROBOT)(BELOWOF13-ROBOT)))

(:derived (Flag522-)(and (ABOVEOF19-ROBOT)(BELOWOF13-ROBOT)))

(:derived (Flag523-)(and (ABOVEOF17-ROBOT)(BELOWOF13-ROBOT)))

(:derived (Flag524-)(and (ABOVEOF13-ROBOT)(BELOWOF13-ROBOT)))

(:derived (Flag525-)(and (ABOVEOF14-ROBOT)(BELOWOF13-ROBOT)))

(:derived (Flag526-)(and (ABOVEOF15-ROBOT)(BELOWOF13-ROBOT)))

(:derived (Flag527-)(and (ABOVEOF12-ROBOT)(BELOWOF13-ROBOT)))

(:derived (Flag528-)(and (BELOWOF12-ROBOT)(ABOVEOF12-ROBOT)))

(:derived (Flag530-)(and (ABOVEOF15-ROBOT)(BELOWOF14-ROBOT)))

(:derived (Flag531-)(and (ABOVEOF16-ROBOT)(BELOWOF14-ROBOT)))

(:derived (Flag532-)(and (BELOWOF14-ROBOT)(ABOVEOF14-ROBOT)))

(:derived (Flag533-)(and (BELOWOF13-ROBOT)(ABOVEOF13-ROBOT)))

(:derived (Flag534-)(and (BELOWOF14-ROBOT)(ABOVEOF13-ROBOT)))

(:derived (Flag535-)(and (ABOVEOF19-ROBOT)(BELOWOF14-ROBOT)))

(:derived (Flag536-)(and (ABOVEOF18-ROBOT)(BELOWOF14-ROBOT)))

(:derived (Flag537-)(and (ABOVEOF17-ROBOT)(BELOWOF14-ROBOT)))

(:derived (Flag539-)(and (ABOVEOF18-ROBOT)(BELOWOF15-ROBOT)))

(:derived (Flag540-)(and (BELOWOF15-ROBOT)(ABOVEOF14-ROBOT)))

(:derived (Flag541-)(and (BELOWOF15-ROBOT)(ABOVEOF15-ROBOT)))

(:derived (Flag542-)(and (ABOVEOF17-ROBOT)(BELOWOF15-ROBOT)))

(:derived (Flag543-)(and (ABOVEOF19-ROBOT)(BELOWOF15-ROBOT)))

(:derived (Flag544-)(and (ABOVEOF16-ROBOT)(BELOWOF15-ROBOT)))

(:derived (Flag546-)(and (ABOVEOF17-ROBOT)(BELOWOF16-ROBOT)))

(:derived (Flag547-)(and (ABOVEOF16-ROBOT)(BELOWOF16-ROBOT)))

(:derived (Flag548-)(and (ABOVEOF19-ROBOT)(BELOWOF16-ROBOT)))

(:derived (Flag549-)(and (BELOWOF16-ROBOT)(ABOVEOF15-ROBOT)))

(:derived (Flag550-)(and (ABOVEOF18-ROBOT)(BELOWOF16-ROBOT)))

(:derived (Flag552-)(and (ABOVEOF18-ROBOT)(BELOWOF17-ROBOT)))

(:derived (Flag553-)(and (BELOWOF17-ROBOT)(ABOVEOF16-ROBOT)))

(:derived (Flag554-)(and (ABOVEOF17-ROBOT)(BELOWOF17-ROBOT)))

(:derived (Flag555-)(and (ABOVEOF19-ROBOT)(BELOWOF17-ROBOT)))

(:derived (Flag557-)(and (ABOVEOF19-ROBOT)(BELOWOF18-ROBOT)))

(:derived (Flag558-)(and (BELOWOF18-ROBOT)(ABOVEOF17-ROBOT)))

(:derived (Flag559-)(and (BELOWOF18-ROBOT)(ABOVEOF18-ROBOT)))

(:derived (Flag561-)(and (BELOWOF19-ROBOT)(ABOVEOF18-ROBOT)))

(:derived (Flag562-)(and (ABOVEOF19-ROBOT)(BELOWOF19-ROBOT)))

(:derived (Flag567-)(and (BELOWOF3-ROBOT)))

(:derived (Flag567-)(and (BELOWOF16-ROBOT)))

(:derived (Flag567-)(and (BELOWOF11-ROBOT)))

(:derived (Flag567-)(and (BELOWOF1-ROBOT)))

(:derived (Flag567-)(and (BELOWOF15-ROBOT)))

(:derived (Flag567-)(and (BELOWOF6-ROBOT)))

(:derived (Flag567-)(and (BELOWOF17-ROBOT)))

(:derived (Flag567-)(and (BELOWOF5-ROBOT)))

(:derived (Flag567-)(and (BELOWOF2-ROBOT)))

(:derived (Flag567-)(and (BELOWOF20-ROBOT)))

(:derived (Flag567-)(and (BELOWOF4-ROBOT)))

(:derived (Flag567-)(and (BELOWOF18-ROBOT)))

(:derived (Flag567-)(and (BELOWOF12-ROBOT)))

(:derived (Flag567-)(and (BELOWOF7-ROBOT)))

(:derived (Flag567-)(and (BELOWOF9-ROBOT)))

(:derived (Flag567-)(and (BELOWOF10-ROBOT)))

(:derived (Flag567-)(and (BELOWOF13-ROBOT)))

(:derived (Flag567-)(and (BELOWOF8-ROBOT)))

(:derived (Flag567-)(and (BELOWOF14-ROBOT)))

(:derived (Flag568-)(and (BELOWOF19-ROBOT)(ABOVEOF19-ROBOT)))

(:derived (Flag569-)(and (ABOVEOF19-ROBOT)(BELOWOF20-ROBOT)))

(:action MOVEDOWN-ROBOT
:parameters ()
:precondition
(and
(Flag2-)
)
:effect
(and
(when
(and
(Flag570-)
)
(and
(ROW18-ROBOT)
(not (NOT-ROW18-ROBOT))
)
)
(when
(and
(Flag563-)
)
(and
(ROW17-ROBOT)
(not (NOT-ROW17-ROBOT))
)
)
(when
(and
(Flag560-)
)
(and
(ROW16-ROBOT)
(not (NOT-ROW16-ROBOT))
)
)
(when
(and
(Flag556-)
)
(and
(ROW15-ROBOT)
(not (NOT-ROW15-ROBOT))
)
)
(when
(and
(Flag551-)
)
(and
(ROW14-ROBOT)
(not (NOT-ROW14-ROBOT))
)
)
(when
(and
(Flag545-)
)
(and
(ROW13-ROBOT)
(not (NOT-ROW13-ROBOT))
)
)
(when
(and
(Flag538-)
)
(and
(ROW12-ROBOT)
(not (NOT-ROW12-ROBOT))
)
)
(when
(and
(Flag529-)
)
(and
(ROW11-ROBOT)
(not (NOT-ROW11-ROBOT))
)
)
(when
(and
(Flag519-)
)
(and
(ROW10-ROBOT)
(not (NOT-ROW10-ROBOT))
)
)
(when
(and
(Flag509-)
)
(and
(ROW9-ROBOT)
(not (NOT-ROW9-ROBOT))
)
)
(when
(and
(Flag498-)
)
(and
(ROW8-ROBOT)
(not (NOT-ROW8-ROBOT))
)
)
(when
(and
(Flag485-)
)
(and
(ROW7-ROBOT)
(not (NOT-ROW7-ROBOT))
)
)
(when
(and
(Flag470-)
)
(and
(ROW6-ROBOT)
(not (NOT-ROW6-ROBOT))
)
)
(when
(and
(Flag455-)
)
(and
(ROW5-ROBOT)
(not (NOT-ROW5-ROBOT))
)
)
(when
(and
(Flag437-)
)
(and
(ROW4-ROBOT)
(not (NOT-ROW4-ROBOT))
)
)
(when
(and
(Flag419-)
)
(and
(ROW3-ROBOT)
(not (NOT-ROW3-ROBOT))
)
)
(when
(and
(Flag401-)
)
(and
(ROW2-ROBOT)
(not (NOT-ROW2-ROBOT))
)
)
(when
(and
(Flag380-)
)
(and
(ROW1-ROBOT)
(not (NOT-ROW1-ROBOT))
)
)
(when
(and
(Flag360-)
)
(and
(ROW0-ROBOT)
(not (NOT-ROW0-ROBOT))
)
)
(when
(and
(ROW19-ROBOT)
)
(and
(ROW18-ROBOT)
(NOT-ROW19-ROBOT)
(not (NOT-ROW18-ROBOT))
(not (ROW19-ROBOT))
)
)
(when
(and
(ROW18-ROBOT)
)
(and
(ROW17-ROBOT)
(NOT-ROW18-ROBOT)
(not (NOT-ROW17-ROBOT))
(not (ROW18-ROBOT))
)
)
(when
(and
(ROW17-ROBOT)
)
(and
(ROW16-ROBOT)
(NOT-ROW17-ROBOT)
(not (NOT-ROW16-ROBOT))
(not (ROW17-ROBOT))
)
)
(when
(and
(ROW16-ROBOT)
)
(and
(ROW15-ROBOT)
(NOT-ROW16-ROBOT)
(not (NOT-ROW15-ROBOT))
(not (ROW16-ROBOT))
)
)
(when
(and
(ROW15-ROBOT)
)
(and
(ROW14-ROBOT)
(NOT-ROW15-ROBOT)
(not (NOT-ROW14-ROBOT))
(not (ROW15-ROBOT))
)
)
(when
(and
(ROW14-ROBOT)
)
(and
(ROW13-ROBOT)
(NOT-ROW14-ROBOT)
(not (NOT-ROW13-ROBOT))
(not (ROW14-ROBOT))
)
)
(when
(and
(ROW13-ROBOT)
)
(and
(ROW12-ROBOT)
(NOT-ROW13-ROBOT)
(not (NOT-ROW12-ROBOT))
(not (ROW13-ROBOT))
)
)
(when
(and
(ROW12-ROBOT)
)
(and
(ROW11-ROBOT)
(NOT-ROW12-ROBOT)
(not (NOT-ROW11-ROBOT))
(not (ROW12-ROBOT))
)
)
(when
(and
(ROW11-ROBOT)
)
(and
(ROW10-ROBOT)
(NOT-ROW11-ROBOT)
(not (NOT-ROW10-ROBOT))
(not (ROW11-ROBOT))
)
)
(when
(and
(ROW10-ROBOT)
)
(and
(ROW9-ROBOT)
(NOT-ROW10-ROBOT)
(not (NOT-ROW9-ROBOT))
(not (ROW10-ROBOT))
)
)
(when
(and
(ROW9-ROBOT)
)
(and
(ROW8-ROBOT)
(NOT-ROW9-ROBOT)
(not (NOT-ROW8-ROBOT))
(not (ROW9-ROBOT))
)
)
(when
(and
(ROW8-ROBOT)
)
(and
(ROW7-ROBOT)
(NOT-ROW8-ROBOT)
(not (NOT-ROW7-ROBOT))
(not (ROW8-ROBOT))
)
)
(when
(and
(ROW7-ROBOT)
)
(and
(ROW6-ROBOT)
(NOT-ROW7-ROBOT)
(not (NOT-ROW6-ROBOT))
(not (ROW7-ROBOT))
)
)
(when
(and
(ROW6-ROBOT)
)
(and
(ROW5-ROBOT)
(NOT-ROW6-ROBOT)
(not (NOT-ROW5-ROBOT))
(not (ROW6-ROBOT))
)
)
(when
(and
(ROW5-ROBOT)
)
(and
(ROW4-ROBOT)
(NOT-ROW5-ROBOT)
(not (NOT-ROW4-ROBOT))
(not (ROW5-ROBOT))
)
)
(when
(and
(ROW4-ROBOT)
)
(and
(ROW3-ROBOT)
(NOT-ROW4-ROBOT)
(not (NOT-ROW3-ROBOT))
(not (ROW4-ROBOT))
)
)
(when
(and
(ROW3-ROBOT)
)
(and
(ROW2-ROBOT)
(NOT-ROW3-ROBOT)
(not (NOT-ROW2-ROBOT))
(not (ROW3-ROBOT))
)
)
(when
(and
(ROW2-ROBOT)
)
(and
(ROW1-ROBOT)
(NOT-ROW2-ROBOT)
(not (NOT-ROW1-ROBOT))
(not (ROW2-ROBOT))
)
)
(when
(and
(ROW1-ROBOT)
)
(and
(ROW0-ROBOT)
(NOT-ROW1-ROBOT)
(not (NOT-ROW0-ROBOT))
(not (ROW1-ROBOT))
)
)
(when
(and
(ABOVEOF19-ROBOT)
)
(and
(ABOVEOF18-ROBOT)
(NOT-ABOVEOF19-ROBOT)
(ABOVEOF17-ROBOT)
(NOT-ABOVEOF18-ROBOT)
(ABOVEOF16-ROBOT)
(NOT-ABOVEOF17-ROBOT)
(ABOVEOF15-ROBOT)
(NOT-ABOVEOF16-ROBOT)
(ABOVEOF14-ROBOT)
(NOT-ABOVEOF15-ROBOT)
(ABOVEOF13-ROBOT)
(NOT-ABOVEOF14-ROBOT)
(ABOVEOF12-ROBOT)
(NOT-ABOVEOF13-ROBOT)
(ABOVEOF11-ROBOT)
(NOT-ABOVEOF12-ROBOT)
(ABOVEOF10-ROBOT)
(NOT-ABOVEOF11-ROBOT)
(ABOVEOF9-ROBOT)
(NOT-ABOVEOF10-ROBOT)
(ABOVEOF8-ROBOT)
(NOT-ABOVEOF9-ROBOT)
(ABOVEOF7-ROBOT)
(NOT-ABOVEOF8-ROBOT)
(ABOVEOF6-ROBOT)
(NOT-ABOVEOF7-ROBOT)
(ABOVEOF5-ROBOT)
(NOT-ABOVEOF6-ROBOT)
(ABOVEOF4-ROBOT)
(NOT-ABOVEOF5-ROBOT)
(ABOVEOF3-ROBOT)
(NOT-ABOVEOF4-ROBOT)
(ABOVEOF2-ROBOT)
(NOT-ABOVEOF3-ROBOT)
(ABOVEOF1-ROBOT)
(NOT-ABOVEOF2-ROBOT)
(NOT-ABOVEOF1-ROBOT)
(not (ABOVEOF19-ROBOT))
)
)
(when
(and
(ABOVEOF18-ROBOT)
)
(and
(ABOVEOF17-ROBOT)
(NOT-ABOVEOF18-ROBOT)
(ABOVEOF16-ROBOT)
(NOT-ABOVEOF17-ROBOT)
(ABOVEOF15-ROBOT)
(NOT-ABOVEOF16-ROBOT)
(ABOVEOF14-ROBOT)
(NOT-ABOVEOF15-ROBOT)
(ABOVEOF13-ROBOT)
(NOT-ABOVEOF14-ROBOT)
(ABOVEOF12-ROBOT)
(NOT-ABOVEOF13-ROBOT)
(ABOVEOF11-ROBOT)
(NOT-ABOVEOF12-ROBOT)
(ABOVEOF10-ROBOT)
(NOT-ABOVEOF11-ROBOT)
(ABOVEOF9-ROBOT)
(NOT-ABOVEOF10-ROBOT)
(ABOVEOF8-ROBOT)
(NOT-ABOVEOF9-ROBOT)
(ABOVEOF7-ROBOT)
(NOT-ABOVEOF8-ROBOT)
(ABOVEOF6-ROBOT)
(NOT-ABOVEOF7-ROBOT)
(ABOVEOF5-ROBOT)
(NOT-ABOVEOF6-ROBOT)
(ABOVEOF4-ROBOT)
(NOT-ABOVEOF5-ROBOT)
(ABOVEOF3-ROBOT)
(NOT-ABOVEOF4-ROBOT)
(ABOVEOF2-ROBOT)
(NOT-ABOVEOF3-ROBOT)
(ABOVEOF1-ROBOT)
(NOT-ABOVEOF2-ROBOT)
(NOT-ABOVEOF1-ROBOT)
(not (ABOVEOF18-ROBOT))
)
)
(when
(and
(ABOVEOF17-ROBOT)
)
(and
(ABOVEOF16-ROBOT)
(NOT-ABOVEOF17-ROBOT)
(ABOVEOF15-ROBOT)
(NOT-ABOVEOF16-ROBOT)
(ABOVEOF14-ROBOT)
(NOT-ABOVEOF15-ROBOT)
(ABOVEOF13-ROBOT)
(NOT-ABOVEOF14-ROBOT)
(ABOVEOF12-ROBOT)
(NOT-ABOVEOF13-ROBOT)
(ABOVEOF11-ROBOT)
(NOT-ABOVEOF12-ROBOT)
(ABOVEOF10-ROBOT)
(NOT-ABOVEOF11-ROBOT)
(ABOVEOF9-ROBOT)
(NOT-ABOVEOF10-ROBOT)
(ABOVEOF8-ROBOT)
(NOT-ABOVEOF9-ROBOT)
(ABOVEOF7-ROBOT)
(NOT-ABOVEOF8-ROBOT)
(ABOVEOF6-ROBOT)
(NOT-ABOVEOF7-ROBOT)
(ABOVEOF5-ROBOT)
(NOT-ABOVEOF6-ROBOT)
(ABOVEOF4-ROBOT)
(NOT-ABOVEOF5-ROBOT)
(ABOVEOF3-ROBOT)
(NOT-ABOVEOF4-ROBOT)
(ABOVEOF2-ROBOT)
(NOT-ABOVEOF3-ROBOT)
(ABOVEOF1-ROBOT)
(NOT-ABOVEOF2-ROBOT)
(NOT-ABOVEOF1-ROBOT)
(not (ABOVEOF17-ROBOT))
)
)
(when
(and
(ABOVEOF16-ROBOT)
)
(and
(ABOVEOF15-ROBOT)
(NOT-ABOVEOF16-ROBOT)
(ABOVEOF14-ROBOT)
(NOT-ABOVEOF15-ROBOT)
(ABOVEOF13-ROBOT)
(NOT-ABOVEOF14-ROBOT)
(ABOVEOF12-ROBOT)
(NOT-ABOVEOF13-ROBOT)
(ABOVEOF11-ROBOT)
(NOT-ABOVEOF12-ROBOT)
(ABOVEOF10-ROBOT)
(NOT-ABOVEOF11-ROBOT)
(ABOVEOF9-ROBOT)
(NOT-ABOVEOF10-ROBOT)
(ABOVEOF8-ROBOT)
(NOT-ABOVEOF9-ROBOT)
(ABOVEOF7-ROBOT)
(NOT-ABOVEOF8-ROBOT)
(ABOVEOF6-ROBOT)
(NOT-ABOVEOF7-ROBOT)
(ABOVEOF5-ROBOT)
(NOT-ABOVEOF6-ROBOT)
(ABOVEOF4-ROBOT)
(NOT-ABOVEOF5-ROBOT)
(ABOVEOF3-ROBOT)
(NOT-ABOVEOF4-ROBOT)
(ABOVEOF2-ROBOT)
(NOT-ABOVEOF3-ROBOT)
(ABOVEOF1-ROBOT)
(NOT-ABOVEOF2-ROBOT)
(NOT-ABOVEOF1-ROBOT)
(not (ABOVEOF16-ROBOT))
)
)
(when
(and
(ABOVEOF15-ROBOT)
)
(and
(ABOVEOF14-ROBOT)
(NOT-ABOVEOF15-ROBOT)
(ABOVEOF13-ROBOT)
(NOT-ABOVEOF14-ROBOT)
(ABOVEOF12-ROBOT)
(NOT-ABOVEOF13-ROBOT)
(ABOVEOF11-ROBOT)
(NOT-ABOVEOF12-ROBOT)
(ABOVEOF10-ROBOT)
(NOT-ABOVEOF11-ROBOT)
(ABOVEOF9-ROBOT)
(NOT-ABOVEOF10-ROBOT)
(ABOVEOF8-ROBOT)
(NOT-ABOVEOF9-ROBOT)
(ABOVEOF7-ROBOT)
(NOT-ABOVEOF8-ROBOT)
(ABOVEOF6-ROBOT)
(NOT-ABOVEOF7-ROBOT)
(ABOVEOF5-ROBOT)
(NOT-ABOVEOF6-ROBOT)
(ABOVEOF4-ROBOT)
(NOT-ABOVEOF5-ROBOT)
(ABOVEOF3-ROBOT)
(NOT-ABOVEOF4-ROBOT)
(ABOVEOF2-ROBOT)
(NOT-ABOVEOF3-ROBOT)
(ABOVEOF1-ROBOT)
(NOT-ABOVEOF2-ROBOT)
(NOT-ABOVEOF1-ROBOT)
(not (ABOVEOF15-ROBOT))
)
)
(when
(and
(ABOVEOF14-ROBOT)
)
(and
(ABOVEOF13-ROBOT)
(NOT-ABOVEOF14-ROBOT)
(ABOVEOF12-ROBOT)
(NOT-ABOVEOF13-ROBOT)
(ABOVEOF11-ROBOT)
(NOT-ABOVEOF12-ROBOT)
(ABOVEOF10-ROBOT)
(NOT-ABOVEOF11-ROBOT)
(ABOVEOF9-ROBOT)
(NOT-ABOVEOF10-ROBOT)
(ABOVEOF8-ROBOT)
(NOT-ABOVEOF9-ROBOT)
(ABOVEOF7-ROBOT)
(NOT-ABOVEOF8-ROBOT)
(ABOVEOF6-ROBOT)
(NOT-ABOVEOF7-ROBOT)
(ABOVEOF5-ROBOT)
(NOT-ABOVEOF6-ROBOT)
(ABOVEOF4-ROBOT)
(NOT-ABOVEOF5-ROBOT)
(ABOVEOF3-ROBOT)
(NOT-ABOVEOF4-ROBOT)
(ABOVEOF2-ROBOT)
(NOT-ABOVEOF3-ROBOT)
(ABOVEOF1-ROBOT)
(NOT-ABOVEOF2-ROBOT)
(NOT-ABOVEOF1-ROBOT)
(not (ABOVEOF14-ROBOT))
)
)
(when
(and
(ABOVEOF13-ROBOT)
)
(and
(ABOVEOF12-ROBOT)
(NOT-ABOVEOF13-ROBOT)
(ABOVEOF11-ROBOT)
(NOT-ABOVEOF12-ROBOT)
(ABOVEOF10-ROBOT)
(NOT-ABOVEOF11-ROBOT)
(ABOVEOF9-ROBOT)
(NOT-ABOVEOF10-ROBOT)
(ABOVEOF8-ROBOT)
(NOT-ABOVEOF9-ROBOT)
(ABOVEOF7-ROBOT)
(NOT-ABOVEOF8-ROBOT)
(ABOVEOF6-ROBOT)
(NOT-ABOVEOF7-ROBOT)
(ABOVEOF5-ROBOT)
(NOT-ABOVEOF6-ROBOT)
(ABOVEOF4-ROBOT)
(NOT-ABOVEOF5-ROBOT)
(ABOVEOF3-ROBOT)
(NOT-ABOVEOF4-ROBOT)
(ABOVEOF2-ROBOT)
(NOT-ABOVEOF3-ROBOT)
(ABOVEOF1-ROBOT)
(NOT-ABOVEOF2-ROBOT)
(NOT-ABOVEOF1-ROBOT)
(not (ABOVEOF13-ROBOT))
)
)
(when
(and
(ABOVEOF12-ROBOT)
)
(and
(ABOVEOF11-ROBOT)
(NOT-ABOVEOF12-ROBOT)
(ABOVEOF10-ROBOT)
(NOT-ABOVEOF11-ROBOT)
(ABOVEOF9-ROBOT)
(NOT-ABOVEOF10-ROBOT)
(ABOVEOF8-ROBOT)
(NOT-ABOVEOF9-ROBOT)
(ABOVEOF7-ROBOT)
(NOT-ABOVEOF8-ROBOT)
(ABOVEOF6-ROBOT)
(NOT-ABOVEOF7-ROBOT)
(ABOVEOF5-ROBOT)
(NOT-ABOVEOF6-ROBOT)
(ABOVEOF4-ROBOT)
(NOT-ABOVEOF5-ROBOT)
(ABOVEOF3-ROBOT)
(NOT-ABOVEOF4-ROBOT)
(ABOVEOF2-ROBOT)
(NOT-ABOVEOF3-ROBOT)
(ABOVEOF1-ROBOT)
(NOT-ABOVEOF2-ROBOT)
(NOT-ABOVEOF1-ROBOT)
(not (ABOVEOF12-ROBOT))
)
)
(when
(and
(ABOVEOF11-ROBOT)
)
(and
(ABOVEOF10-ROBOT)
(NOT-ABOVEOF11-ROBOT)
(ABOVEOF9-ROBOT)
(NOT-ABOVEOF10-ROBOT)
(ABOVEOF8-ROBOT)
(NOT-ABOVEOF9-ROBOT)
(ABOVEOF7-ROBOT)
(NOT-ABOVEOF8-ROBOT)
(ABOVEOF6-ROBOT)
(NOT-ABOVEOF7-ROBOT)
(ABOVEOF5-ROBOT)
(NOT-ABOVEOF6-ROBOT)
(ABOVEOF4-ROBOT)
(NOT-ABOVEOF5-ROBOT)
(ABOVEOF3-ROBOT)
(NOT-ABOVEOF4-ROBOT)
(ABOVEOF2-ROBOT)
(NOT-ABOVEOF3-ROBOT)
(ABOVEOF1-ROBOT)
(NOT-ABOVEOF2-ROBOT)
(NOT-ABOVEOF1-ROBOT)
(not (ABOVEOF11-ROBOT))
)
)
(when
(and
(ABOVEOF10-ROBOT)
)
(and
(ABOVEOF9-ROBOT)
(NOT-ABOVEOF10-ROBOT)
(ABOVEOF8-ROBOT)
(NOT-ABOVEOF9-ROBOT)
(ABOVEOF7-ROBOT)
(NOT-ABOVEOF8-ROBOT)
(ABOVEOF6-ROBOT)
(NOT-ABOVEOF7-ROBOT)
(ABOVEOF5-ROBOT)
(NOT-ABOVEOF6-ROBOT)
(ABOVEOF4-ROBOT)
(NOT-ABOVEOF5-ROBOT)
(ABOVEOF3-ROBOT)
(NOT-ABOVEOF4-ROBOT)
(ABOVEOF2-ROBOT)
(NOT-ABOVEOF3-ROBOT)
(ABOVEOF1-ROBOT)
(NOT-ABOVEOF2-ROBOT)
(NOT-ABOVEOF1-ROBOT)
(not (ABOVEOF10-ROBOT))
)
)
(when
(and
(ABOVEOF9-ROBOT)
)
(and
(ABOVEOF8-ROBOT)
(NOT-ABOVEOF9-ROBOT)
(ABOVEOF7-ROBOT)
(NOT-ABOVEOF8-ROBOT)
(ABOVEOF6-ROBOT)
(NOT-ABOVEOF7-ROBOT)
(ABOVEOF5-ROBOT)
(NOT-ABOVEOF6-ROBOT)
(ABOVEOF4-ROBOT)
(NOT-ABOVEOF5-ROBOT)
(ABOVEOF3-ROBOT)
(NOT-ABOVEOF4-ROBOT)
(ABOVEOF2-ROBOT)
(NOT-ABOVEOF3-ROBOT)
(ABOVEOF1-ROBOT)
(NOT-ABOVEOF2-ROBOT)
(NOT-ABOVEOF1-ROBOT)
(not (ABOVEOF9-ROBOT))
)
)
(when
(and
(ABOVEOF8-ROBOT)
)
(and
(ABOVEOF7-ROBOT)
(NOT-ABOVEOF8-ROBOT)
(ABOVEOF6-ROBOT)
(NOT-ABOVEOF7-ROBOT)
(ABOVEOF5-ROBOT)
(NOT-ABOVEOF6-ROBOT)
(ABOVEOF4-ROBOT)
(NOT-ABOVEOF5-ROBOT)
(ABOVEOF3-ROBOT)
(NOT-ABOVEOF4-ROBOT)
(ABOVEOF2-ROBOT)
(NOT-ABOVEOF3-ROBOT)
(ABOVEOF1-ROBOT)
(NOT-ABOVEOF2-ROBOT)
(NOT-ABOVEOF1-ROBOT)
(not (ABOVEOF8-ROBOT))
)
)
(when
(and
(ABOVEOF7-ROBOT)
)
(and
(ABOVEOF6-ROBOT)
(NOT-ABOVEOF7-ROBOT)
(ABOVEOF5-ROBOT)
(NOT-ABOVEOF6-ROBOT)
(ABOVEOF4-ROBOT)
(NOT-ABOVEOF5-ROBOT)
(ABOVEOF3-ROBOT)
(NOT-ABOVEOF4-ROBOT)
(ABOVEOF2-ROBOT)
(NOT-ABOVEOF3-ROBOT)
(ABOVEOF1-ROBOT)
(NOT-ABOVEOF2-ROBOT)
(NOT-ABOVEOF1-ROBOT)
(not (ABOVEOF7-ROBOT))
)
)
(when
(and
(ABOVEOF6-ROBOT)
)
(and
(ABOVEOF5-ROBOT)
(NOT-ABOVEOF6-ROBOT)
(ABOVEOF4-ROBOT)
(NOT-ABOVEOF5-ROBOT)
(ABOVEOF3-ROBOT)
(NOT-ABOVEOF4-ROBOT)
(ABOVEOF2-ROBOT)
(NOT-ABOVEOF3-ROBOT)
(ABOVEOF1-ROBOT)
(NOT-ABOVEOF2-ROBOT)
(NOT-ABOVEOF1-ROBOT)
(not (ABOVEOF6-ROBOT))
)
)
(when
(and
(ABOVEOF5-ROBOT)
)
(and
(ABOVEOF4-ROBOT)
(NOT-ABOVEOF5-ROBOT)
(ABOVEOF3-ROBOT)
(NOT-ABOVEOF4-ROBOT)
(ABOVEOF2-ROBOT)
(NOT-ABOVEOF3-ROBOT)
(ABOVEOF1-ROBOT)
(NOT-ABOVEOF2-ROBOT)
(NOT-ABOVEOF1-ROBOT)
(not (ABOVEOF5-ROBOT))
)
)
(when
(and
(ABOVEOF4-ROBOT)
)
(and
(ABOVEOF3-ROBOT)
(NOT-ABOVEOF4-ROBOT)
(ABOVEOF2-ROBOT)
(NOT-ABOVEOF3-ROBOT)
(ABOVEOF1-ROBOT)
(NOT-ABOVEOF2-ROBOT)
(NOT-ABOVEOF1-ROBOT)
(not (ABOVEOF4-ROBOT))
)
)
(when
(and
(ABOVEOF3-ROBOT)
)
(and
(ABOVEOF2-ROBOT)
(NOT-ABOVEOF3-ROBOT)
(ABOVEOF1-ROBOT)
(NOT-ABOVEOF2-ROBOT)
(NOT-ABOVEOF1-ROBOT)
(not (ABOVEOF3-ROBOT))
)
)
(when
(and
(ABOVEOF2-ROBOT)
)
(and
(ABOVEOF1-ROBOT)
(NOT-ABOVEOF2-ROBOT)
(NOT-ABOVEOF1-ROBOT)
(not (ABOVEOF2-ROBOT))
)
)
(when
(and
(ABOVEOF1-ROBOT)
)
(and
(NOT-ABOVEOF1-ROBOT)
(not (ABOVEOF1-ROBOT))
)
)
(when
(and
(Flag567-)
)
(and
(BELOWOF19-ROBOT)
(not (NOT-BELOWOF19-ROBOT))
)
)
(when
(and
(BELOWOF14-ROBOT)
)
(and
(BELOWOF18-ROBOT)
(BELOWOF17-ROBOT)
(BELOWOF16-ROBOT)
(BELOWOF15-ROBOT)
(BELOWOF13-ROBOT)
(not (NOT-BELOWOF18-ROBOT))
(not (NOT-BELOWOF17-ROBOT))
(not (NOT-BELOWOF16-ROBOT))
(not (NOT-BELOWOF15-ROBOT))
(not (NOT-BELOWOF14-ROBOT))
(not (NOT-BELOWOF13-ROBOT))
)
)
(when
(and
(BELOWOF8-ROBOT)
)
(and
(BELOWOF18-ROBOT)
(BELOWOF17-ROBOT)
(BELOWOF16-ROBOT)
(BELOWOF15-ROBOT)
(BELOWOF14-ROBOT)
(BELOWOF13-ROBOT)
(BELOWOF12-ROBOT)
(BELOWOF11-ROBOT)
(BELOWOF10-ROBOT)
(BELOWOF9-ROBOT)
(BELOWOF7-ROBOT)
(not (NOT-BELOWOF18-ROBOT))
(not (NOT-BELOWOF17-ROBOT))
(not (NOT-BELOWOF16-ROBOT))
(not (NOT-BELOWOF15-ROBOT))
(not (NOT-BELOWOF14-ROBOT))
(not (NOT-BELOWOF13-ROBOT))
(not (NOT-BELOWOF12-ROBOT))
(not (NOT-BELOWOF11-ROBOT))
(not (NOT-BELOWOF10-ROBOT))
(not (NOT-BELOWOF9-ROBOT))
(not (NOT-BELOWOF8-ROBOT))
(not (NOT-BELOWOF7-ROBOT))
)
)
(when
(and
(BELOWOF13-ROBOT)
)
(and
(BELOWOF18-ROBOT)
(BELOWOF17-ROBOT)
(BELOWOF16-ROBOT)
(BELOWOF15-ROBOT)
(BELOWOF14-ROBOT)
(BELOWOF12-ROBOT)
(not (NOT-BELOWOF18-ROBOT))
(not (NOT-BELOWOF17-ROBOT))
(not (NOT-BELOWOF16-ROBOT))
(not (NOT-BELOWOF15-ROBOT))
(not (NOT-BELOWOF14-ROBOT))
(not (NOT-BELOWOF13-ROBOT))
(not (NOT-BELOWOF12-ROBOT))
)
)
(when
(and
(BELOWOF10-ROBOT)
)
(and
(BELOWOF18-ROBOT)
(BELOWOF17-ROBOT)
(BELOWOF16-ROBOT)
(BELOWOF15-ROBOT)
(BELOWOF14-ROBOT)
(BELOWOF13-ROBOT)
(BELOWOF12-ROBOT)
(BELOWOF11-ROBOT)
(BELOWOF9-ROBOT)
(not (NOT-BELOWOF18-ROBOT))
(not (NOT-BELOWOF17-ROBOT))
(not (NOT-BELOWOF16-ROBOT))
(not (NOT-BELOWOF15-ROBOT))
(not (NOT-BELOWOF14-ROBOT))
(not (NOT-BELOWOF13-ROBOT))
(not (NOT-BELOWOF12-ROBOT))
(not (NOT-BELOWOF11-ROBOT))
(not (NOT-BELOWOF10-ROBOT))
(not (NOT-BELOWOF9-ROBOT))
)
)
(when
(and
(BELOWOF9-ROBOT)
)
(and
(BELOWOF18-ROBOT)
(BELOWOF17-ROBOT)
(BELOWOF16-ROBOT)
(BELOWOF15-ROBOT)
(BELOWOF14-ROBOT)
(BELOWOF13-ROBOT)
(BELOWOF12-ROBOT)
(BELOWOF11-ROBOT)
(BELOWOF10-ROBOT)
(BELOWOF8-ROBOT)
(not (NOT-BELOWOF18-ROBOT))
(not (NOT-BELOWOF17-ROBOT))
(not (NOT-BELOWOF16-ROBOT))
(not (NOT-BELOWOF15-ROBOT))
(not (NOT-BELOWOF14-ROBOT))
(not (NOT-BELOWOF13-ROBOT))
(not (NOT-BELOWOF12-ROBOT))
(not (NOT-BELOWOF11-ROBOT))
(not (NOT-BELOWOF10-ROBOT))
(not (NOT-BELOWOF9-ROBOT))
(not (NOT-BELOWOF8-ROBOT))
)
)
(when
(and
(BELOWOF7-ROBOT)
)
(and
(BELOWOF18-ROBOT)
(BELOWOF17-ROBOT)
(BELOWOF16-ROBOT)
(BELOWOF15-ROBOT)
(BELOWOF14-ROBOT)
(BELOWOF13-ROBOT)
(BELOWOF12-ROBOT)
(BELOWOF11-ROBOT)
(BELOWOF10-ROBOT)
(BELOWOF9-ROBOT)
(BELOWOF8-ROBOT)
(BELOWOF6-ROBOT)
(not (NOT-BELOWOF18-ROBOT))
(not (NOT-BELOWOF17-ROBOT))
(not (NOT-BELOWOF16-ROBOT))
(not (NOT-BELOWOF15-ROBOT))
(not (NOT-BELOWOF14-ROBOT))
(not (NOT-BELOWOF13-ROBOT))
(not (NOT-BELOWOF12-ROBOT))
(not (NOT-BELOWOF11-ROBOT))
(not (NOT-BELOWOF10-ROBOT))
(not (NOT-BELOWOF9-ROBOT))
(not (NOT-BELOWOF8-ROBOT))
(not (NOT-BELOWOF7-ROBOT))
(not (NOT-BELOWOF6-ROBOT))
)
)
(when
(and
(BELOWOF12-ROBOT)
)
(and
(BELOWOF18-ROBOT)
(BELOWOF17-ROBOT)
(BELOWOF16-ROBOT)
(BELOWOF15-ROBOT)
(BELOWOF14-ROBOT)
(BELOWOF13-ROBOT)
(BELOWOF11-ROBOT)
(not (NOT-BELOWOF18-ROBOT))
(not (NOT-BELOWOF17-ROBOT))
(not (NOT-BELOWOF16-ROBOT))
(not (NOT-BELOWOF15-ROBOT))
(not (NOT-BELOWOF14-ROBOT))
(not (NOT-BELOWOF13-ROBOT))
(not (NOT-BELOWOF12-ROBOT))
(not (NOT-BELOWOF11-ROBOT))
)
)
(when
(and
(BELOWOF18-ROBOT)
)
(and
(BELOWOF17-ROBOT)
(not (NOT-BELOWOF18-ROBOT))
(not (NOT-BELOWOF17-ROBOT))
)
)
(when
(and
(BELOWOF11-ROBOT)
)
(and
(BELOWOF18-ROBOT)
(BELOWOF17-ROBOT)
(BELOWOF16-ROBOT)
(BELOWOF15-ROBOT)
(BELOWOF14-ROBOT)
(BELOWOF13-ROBOT)
(BELOWOF12-ROBOT)
(BELOWOF10-ROBOT)
(not (NOT-BELOWOF18-ROBOT))
(not (NOT-BELOWOF17-ROBOT))
(not (NOT-BELOWOF16-ROBOT))
(not (NOT-BELOWOF15-ROBOT))
(not (NOT-BELOWOF14-ROBOT))
(not (NOT-BELOWOF13-ROBOT))
(not (NOT-BELOWOF12-ROBOT))
(not (NOT-BELOWOF11-ROBOT))
(not (NOT-BELOWOF10-ROBOT))
)
)
(when
(and
(BELOWOF4-ROBOT)
)
(and
(BELOWOF18-ROBOT)
(BELOWOF17-ROBOT)
(BELOWOF16-ROBOT)
(BELOWOF15-ROBOT)
(BELOWOF14-ROBOT)
(BELOWOF13-ROBOT)
(BELOWOF12-ROBOT)
(BELOWOF11-ROBOT)
(BELOWOF10-ROBOT)
(BELOWOF9-ROBOT)
(BELOWOF8-ROBOT)
(BELOWOF7-ROBOT)
(BELOWOF6-ROBOT)
(BELOWOF5-ROBOT)
(BELOWOF3-ROBOT)
(not (NOT-BELOWOF18-ROBOT))
(not (NOT-BELOWOF17-ROBOT))
(not (NOT-BELOWOF16-ROBOT))
(not (NOT-BELOWOF15-ROBOT))
(not (NOT-BELOWOF14-ROBOT))
(not (NOT-BELOWOF13-ROBOT))
(not (NOT-BELOWOF12-ROBOT))
(not (NOT-BELOWOF11-ROBOT))
(not (NOT-BELOWOF10-ROBOT))
(not (NOT-BELOWOF9-ROBOT))
(not (NOT-BELOWOF8-ROBOT))
(not (NOT-BELOWOF7-ROBOT))
(not (NOT-BELOWOF6-ROBOT))
(not (NOT-BELOWOF5-ROBOT))
(not (NOT-BELOWOF4-ROBOT))
(not (NOT-BELOWOF3-ROBOT))
)
)
(when
(and
(BELOWOF1-ROBOT)
)
(and
(BELOWOF18-ROBOT)
(BELOWOF17-ROBOT)
(BELOWOF16-ROBOT)
(BELOWOF15-ROBOT)
(BELOWOF14-ROBOT)
(BELOWOF13-ROBOT)
(BELOWOF12-ROBOT)
(BELOWOF11-ROBOT)
(BELOWOF10-ROBOT)
(BELOWOF9-ROBOT)
(BELOWOF8-ROBOT)
(BELOWOF7-ROBOT)
(BELOWOF6-ROBOT)
(BELOWOF5-ROBOT)
(BELOWOF4-ROBOT)
(BELOWOF3-ROBOT)
(BELOWOF2-ROBOT)
(not (NOT-BELOWOF18-ROBOT))
(not (NOT-BELOWOF17-ROBOT))
(not (NOT-BELOWOF16-ROBOT))
(not (NOT-BELOWOF15-ROBOT))
(not (NOT-BELOWOF14-ROBOT))
(not (NOT-BELOWOF13-ROBOT))
(not (NOT-BELOWOF12-ROBOT))
(not (NOT-BELOWOF11-ROBOT))
(not (NOT-BELOWOF10-ROBOT))
(not (NOT-BELOWOF9-ROBOT))
(not (NOT-BELOWOF8-ROBOT))
(not (NOT-BELOWOF7-ROBOT))
(not (NOT-BELOWOF6-ROBOT))
(not (NOT-BELOWOF5-ROBOT))
(not (NOT-BELOWOF4-ROBOT))
(not (NOT-BELOWOF3-ROBOT))
(not (NOT-BELOWOF2-ROBOT))
(not (NOT-BELOWOF1-ROBOT))
)
)
(when
(and
(BELOWOF2-ROBOT)
)
(and
(BELOWOF18-ROBOT)
(BELOWOF17-ROBOT)
(BELOWOF16-ROBOT)
(BELOWOF15-ROBOT)
(BELOWOF14-ROBOT)
(BELOWOF13-ROBOT)
(BELOWOF12-ROBOT)
(BELOWOF11-ROBOT)
(BELOWOF10-ROBOT)
(BELOWOF9-ROBOT)
(BELOWOF8-ROBOT)
(BELOWOF7-ROBOT)
(BELOWOF6-ROBOT)
(BELOWOF5-ROBOT)
(BELOWOF4-ROBOT)
(BELOWOF3-ROBOT)
(BELOWOF1-ROBOT)
(not (NOT-BELOWOF18-ROBOT))
(not (NOT-BELOWOF17-ROBOT))
(not (NOT-BELOWOF16-ROBOT))
(not (NOT-BELOWOF15-ROBOT))
(not (NOT-BELOWOF14-ROBOT))
(not (NOT-BELOWOF13-ROBOT))
(not (NOT-BELOWOF12-ROBOT))
(not (NOT-BELOWOF11-ROBOT))
(not (NOT-BELOWOF10-ROBOT))
(not (NOT-BELOWOF9-ROBOT))
(not (NOT-BELOWOF8-ROBOT))
(not (NOT-BELOWOF7-ROBOT))
(not (NOT-BELOWOF6-ROBOT))
(not (NOT-BELOWOF5-ROBOT))
(not (NOT-BELOWOF4-ROBOT))
(not (NOT-BELOWOF3-ROBOT))
(not (NOT-BELOWOF2-ROBOT))
(not (NOT-BELOWOF1-ROBOT))
)
)
(when
(and
(BELOWOF5-ROBOT)
)
(and
(BELOWOF18-ROBOT)
(BELOWOF17-ROBOT)
(BELOWOF16-ROBOT)
(BELOWOF15-ROBOT)
(BELOWOF14-ROBOT)
(BELOWOF13-ROBOT)
(BELOWOF12-ROBOT)
(BELOWOF11-ROBOT)
(BELOWOF10-ROBOT)
(BELOWOF9-ROBOT)
(BELOWOF8-ROBOT)
(BELOWOF7-ROBOT)
(BELOWOF6-ROBOT)
(BELOWOF4-ROBOT)
(not (NOT-BELOWOF18-ROBOT))
(not (NOT-BELOWOF17-ROBOT))
(not (NOT-BELOWOF16-ROBOT))
(not (NOT-BELOWOF15-ROBOT))
(not (NOT-BELOWOF14-ROBOT))
(not (NOT-BELOWOF13-ROBOT))
(not (NOT-BELOWOF12-ROBOT))
(not (NOT-BELOWOF11-ROBOT))
(not (NOT-BELOWOF10-ROBOT))
(not (NOT-BELOWOF9-ROBOT))
(not (NOT-BELOWOF8-ROBOT))
(not (NOT-BELOWOF7-ROBOT))
(not (NOT-BELOWOF6-ROBOT))
(not (NOT-BELOWOF5-ROBOT))
(not (NOT-BELOWOF4-ROBOT))
)
)
(when
(and
(BELOWOF17-ROBOT)
)
(and
(BELOWOF18-ROBOT)
(BELOWOF16-ROBOT)
(not (NOT-BELOWOF18-ROBOT))
(not (NOT-BELOWOF17-ROBOT))
(not (NOT-BELOWOF16-ROBOT))
)
)
(when
(and
(BELOWOF6-ROBOT)
)
(and
(BELOWOF18-ROBOT)
(BELOWOF17-ROBOT)
(BELOWOF16-ROBOT)
(BELOWOF15-ROBOT)
(BELOWOF14-ROBOT)
(BELOWOF13-ROBOT)
(BELOWOF12-ROBOT)
(BELOWOF11-ROBOT)
(BELOWOF10-ROBOT)
(BELOWOF9-ROBOT)
(BELOWOF8-ROBOT)
(BELOWOF7-ROBOT)
(BELOWOF5-ROBOT)
(not (NOT-BELOWOF18-ROBOT))
(not (NOT-BELOWOF17-ROBOT))
(not (NOT-BELOWOF16-ROBOT))
(not (NOT-BELOWOF15-ROBOT))
(not (NOT-BELOWOF14-ROBOT))
(not (NOT-BELOWOF13-ROBOT))
(not (NOT-BELOWOF12-ROBOT))
(not (NOT-BELOWOF11-ROBOT))
(not (NOT-BELOWOF10-ROBOT))
(not (NOT-BELOWOF9-ROBOT))
(not (NOT-BELOWOF8-ROBOT))
(not (NOT-BELOWOF7-ROBOT))
(not (NOT-BELOWOF6-ROBOT))
(not (NOT-BELOWOF5-ROBOT))
)
)
(when
(and
(BELOWOF15-ROBOT)
)
(and
(BELOWOF18-ROBOT)
(BELOWOF17-ROBOT)
(BELOWOF16-ROBOT)
(BELOWOF14-ROBOT)
(not (NOT-BELOWOF18-ROBOT))
(not (NOT-BELOWOF17-ROBOT))
(not (NOT-BELOWOF16-ROBOT))
(not (NOT-BELOWOF15-ROBOT))
(not (NOT-BELOWOF14-ROBOT))
)
)
(when
(and
(BELOWOF16-ROBOT)
)
(and
(BELOWOF18-ROBOT)
(BELOWOF17-ROBOT)
(BELOWOF15-ROBOT)
(not (NOT-BELOWOF18-ROBOT))
(not (NOT-BELOWOF17-ROBOT))
(not (NOT-BELOWOF16-ROBOT))
(not (NOT-BELOWOF15-ROBOT))
)
)
(when
(and
(BELOWOF19-ROBOT)
)
(and
(BELOWOF18-ROBOT)
(not (NOT-BELOWOF18-ROBOT))
)
)
(when
(and
(BELOWOF3-ROBOT)
)
(and
(BELOWOF18-ROBOT)
(BELOWOF17-ROBOT)
(BELOWOF16-ROBOT)
(BELOWOF15-ROBOT)
(BELOWOF14-ROBOT)
(BELOWOF13-ROBOT)
(BELOWOF12-ROBOT)
(BELOWOF11-ROBOT)
(BELOWOF10-ROBOT)
(BELOWOF9-ROBOT)
(BELOWOF8-ROBOT)
(BELOWOF7-ROBOT)
(BELOWOF6-ROBOT)
(BELOWOF5-ROBOT)
(BELOWOF4-ROBOT)
(BELOWOF2-ROBOT)
(not (NOT-BELOWOF18-ROBOT))
(not (NOT-BELOWOF17-ROBOT))
(not (NOT-BELOWOF16-ROBOT))
(not (NOT-BELOWOF15-ROBOT))
(not (NOT-BELOWOF14-ROBOT))
(not (NOT-BELOWOF13-ROBOT))
(not (NOT-BELOWOF12-ROBOT))
(not (NOT-BELOWOF11-ROBOT))
(not (NOT-BELOWOF10-ROBOT))
(not (NOT-BELOWOF9-ROBOT))
(not (NOT-BELOWOF8-ROBOT))
(not (NOT-BELOWOF7-ROBOT))
(not (NOT-BELOWOF6-ROBOT))
(not (NOT-BELOWOF5-ROBOT))
(not (NOT-BELOWOF4-ROBOT))
(not (NOT-BELOWOF3-ROBOT))
(not (NOT-BELOWOF2-ROBOT))
)
)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action MOVEUP-ROBOT
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
)
:effect
(and
(when
(and
(Flag563-)
)
(and
(ROW19-ROBOT)
(not (NOT-ROW19-ROBOT))
)
)
(when
(and
(Flag560-)
)
(and
(ROW18-ROBOT)
(not (NOT-ROW18-ROBOT))
)
)
(when
(and
(Flag556-)
)
(and
(ROW17-ROBOT)
(not (NOT-ROW17-ROBOT))
)
)
(when
(and
(Flag551-)
)
(and
(ROW16-ROBOT)
(not (NOT-ROW16-ROBOT))
)
)
(when
(and
(Flag545-)
)
(and
(ROW15-ROBOT)
(not (NOT-ROW15-ROBOT))
)
)
(when
(and
(Flag538-)
)
(and
(ROW14-ROBOT)
(not (NOT-ROW14-ROBOT))
)
)
(when
(and
(Flag529-)
)
(and
(ROW13-ROBOT)
(not (NOT-ROW13-ROBOT))
)
)
(when
(and
(Flag519-)
)
(and
(ROW12-ROBOT)
(not (NOT-ROW12-ROBOT))
)
)
(when
(and
(Flag509-)
)
(and
(ROW11-ROBOT)
(not (NOT-ROW11-ROBOT))
)
)
(when
(and
(Flag498-)
)
(and
(ROW10-ROBOT)
(not (NOT-ROW10-ROBOT))
)
)
(when
(and
(Flag485-)
)
(and
(ROW9-ROBOT)
(not (NOT-ROW9-ROBOT))
)
)
(when
(and
(Flag470-)
)
(and
(ROW8-ROBOT)
(not (NOT-ROW8-ROBOT))
)
)
(when
(and
(Flag455-)
)
(and
(ROW7-ROBOT)
(not (NOT-ROW7-ROBOT))
)
)
(when
(and
(Flag437-)
)
(and
(ROW6-ROBOT)
(not (NOT-ROW6-ROBOT))
)
)
(when
(and
(Flag419-)
)
(and
(ROW5-ROBOT)
(not (NOT-ROW5-ROBOT))
)
)
(when
(and
(Flag401-)
)
(and
(ROW4-ROBOT)
(not (NOT-ROW4-ROBOT))
)
)
(when
(and
(Flag380-)
)
(and
(ROW3-ROBOT)
(not (NOT-ROW3-ROBOT))
)
)
(when
(and
(Flag360-)
)
(and
(ROW2-ROBOT)
(not (NOT-ROW2-ROBOT))
)
)
(when
(and
(Flag339-)
)
(and
(ROW1-ROBOT)
(not (NOT-ROW1-ROBOT))
)
)
(when
(and
(ROW18-ROBOT)
)
(and
(ROW19-ROBOT)
(NOT-ROW18-ROBOT)
(not (NOT-ROW19-ROBOT))
(not (ROW18-ROBOT))
)
)
(when
(and
(ROW17-ROBOT)
)
(and
(ROW18-ROBOT)
(NOT-ROW17-ROBOT)
(not (NOT-ROW18-ROBOT))
(not (ROW17-ROBOT))
)
)
(when
(and
(ROW16-ROBOT)
)
(and
(ROW17-ROBOT)
(NOT-ROW16-ROBOT)
(not (NOT-ROW17-ROBOT))
(not (ROW16-ROBOT))
)
)
(when
(and
(ROW15-ROBOT)
)
(and
(ROW16-ROBOT)
(NOT-ROW15-ROBOT)
(not (NOT-ROW16-ROBOT))
(not (ROW15-ROBOT))
)
)
(when
(and
(ROW14-ROBOT)
)
(and
(ROW15-ROBOT)
(NOT-ROW14-ROBOT)
(not (NOT-ROW15-ROBOT))
(not (ROW14-ROBOT))
)
)
(when
(and
(ROW13-ROBOT)
)
(and
(ROW14-ROBOT)
(NOT-ROW13-ROBOT)
(not (NOT-ROW14-ROBOT))
(not (ROW13-ROBOT))
)
)
(when
(and
(ROW12-ROBOT)
)
(and
(ROW13-ROBOT)
(NOT-ROW12-ROBOT)
(not (NOT-ROW13-ROBOT))
(not (ROW12-ROBOT))
)
)
(when
(and
(ROW11-ROBOT)
)
(and
(ROW12-ROBOT)
(NOT-ROW11-ROBOT)
(not (NOT-ROW12-ROBOT))
(not (ROW11-ROBOT))
)
)
(when
(and
(ROW10-ROBOT)
)
(and
(ROW11-ROBOT)
(NOT-ROW10-ROBOT)
(not (NOT-ROW11-ROBOT))
(not (ROW10-ROBOT))
)
)
(when
(and
(ROW9-ROBOT)
)
(and
(ROW10-ROBOT)
(NOT-ROW9-ROBOT)
(not (NOT-ROW10-ROBOT))
(not (ROW9-ROBOT))
)
)
(when
(and
(ROW8-ROBOT)
)
(and
(ROW9-ROBOT)
(NOT-ROW8-ROBOT)
(not (NOT-ROW9-ROBOT))
(not (ROW8-ROBOT))
)
)
(when
(and
(ROW7-ROBOT)
)
(and
(ROW8-ROBOT)
(NOT-ROW7-ROBOT)
(not (NOT-ROW8-ROBOT))
(not (ROW7-ROBOT))
)
)
(when
(and
(ROW6-ROBOT)
)
(and
(ROW7-ROBOT)
(NOT-ROW6-ROBOT)
(not (NOT-ROW7-ROBOT))
(not (ROW6-ROBOT))
)
)
(when
(and
(ROW5-ROBOT)
)
(and
(ROW6-ROBOT)
(NOT-ROW5-ROBOT)
(not (NOT-ROW6-ROBOT))
(not (ROW5-ROBOT))
)
)
(when
(and
(ROW4-ROBOT)
)
(and
(ROW5-ROBOT)
(NOT-ROW4-ROBOT)
(not (NOT-ROW5-ROBOT))
(not (ROW4-ROBOT))
)
)
(when
(and
(ROW3-ROBOT)
)
(and
(ROW4-ROBOT)
(NOT-ROW3-ROBOT)
(not (NOT-ROW4-ROBOT))
(not (ROW3-ROBOT))
)
)
(when
(and
(ROW2-ROBOT)
)
(and
(ROW3-ROBOT)
(NOT-ROW2-ROBOT)
(not (NOT-ROW3-ROBOT))
(not (ROW2-ROBOT))
)
)
(when
(and
(ROW1-ROBOT)
)
(and
(ROW2-ROBOT)
(NOT-ROW1-ROBOT)
(not (NOT-ROW2-ROBOT))
(not (ROW1-ROBOT))
)
)
(when
(and
(ROW0-ROBOT)
)
(and
(ROW1-ROBOT)
(NOT-ROW0-ROBOT)
(not (NOT-ROW1-ROBOT))
(not (ROW0-ROBOT))
)
)
(when
(and
(Flag319-)
)
(and
(BELOWOF20-ROBOT)
(NOT-BELOWOF19-ROBOT)
(not (NOT-BELOWOF20-ROBOT))
(not (BELOWOF19-ROBOT))
)
)
(when
(and
(Flag318-)
)
(and
(BELOWOF19-ROBOT)
(NOT-BELOWOF18-ROBOT)
(not (NOT-BELOWOF19-ROBOT))
(not (BELOWOF18-ROBOT))
)
)
(when
(and
(Flag317-)
)
(and
(BELOWOF18-ROBOT)
(NOT-BELOWOF17-ROBOT)
(not (NOT-BELOWOF18-ROBOT))
(not (BELOWOF17-ROBOT))
)
)
(when
(and
(Flag316-)
)
(and
(BELOWOF17-ROBOT)
(NOT-BELOWOF16-ROBOT)
(not (NOT-BELOWOF17-ROBOT))
(not (BELOWOF16-ROBOT))
)
)
(when
(and
(Flag315-)
)
(and
(BELOWOF16-ROBOT)
(NOT-BELOWOF15-ROBOT)
(not (NOT-BELOWOF16-ROBOT))
(not (BELOWOF15-ROBOT))
)
)
(when
(and
(Flag314-)
)
(and
(BELOWOF15-ROBOT)
(NOT-BELOWOF14-ROBOT)
(not (NOT-BELOWOF15-ROBOT))
(not (BELOWOF14-ROBOT))
)
)
(when
(and
(Flag313-)
)
(and
(BELOWOF14-ROBOT)
(NOT-BELOWOF13-ROBOT)
(not (NOT-BELOWOF14-ROBOT))
(not (BELOWOF13-ROBOT))
)
)
(when
(and
(Flag312-)
)
(and
(BELOWOF13-ROBOT)
(NOT-BELOWOF12-ROBOT)
(not (NOT-BELOWOF13-ROBOT))
(not (BELOWOF12-ROBOT))
)
)
(when
(and
(Flag311-)
)
(and
(BELOWOF12-ROBOT)
(NOT-BELOWOF11-ROBOT)
(not (NOT-BELOWOF12-ROBOT))
(not (BELOWOF11-ROBOT))
)
)
(when
(and
(Flag310-)
)
(and
(BELOWOF11-ROBOT)
(NOT-BELOWOF10-ROBOT)
(not (NOT-BELOWOF11-ROBOT))
(not (BELOWOF10-ROBOT))
)
)
(when
(and
(Flag309-)
)
(and
(BELOWOF10-ROBOT)
(NOT-BELOWOF9-ROBOT)
(not (NOT-BELOWOF10-ROBOT))
(not (BELOWOF9-ROBOT))
)
)
(when
(and
(Flag308-)
)
(and
(BELOWOF9-ROBOT)
(NOT-BELOWOF8-ROBOT)
(not (NOT-BELOWOF9-ROBOT))
(not (BELOWOF8-ROBOT))
)
)
(when
(and
(Flag307-)
)
(and
(BELOWOF8-ROBOT)
(NOT-BELOWOF7-ROBOT)
(not (NOT-BELOWOF8-ROBOT))
(not (BELOWOF7-ROBOT))
)
)
(when
(and
(Flag306-)
)
(and
(BELOWOF7-ROBOT)
(NOT-BELOWOF6-ROBOT)
(not (NOT-BELOWOF7-ROBOT))
(not (BELOWOF6-ROBOT))
)
)
(when
(and
(Flag305-)
)
(and
(BELOWOF6-ROBOT)
(NOT-BELOWOF5-ROBOT)
(not (NOT-BELOWOF6-ROBOT))
(not (BELOWOF5-ROBOT))
)
)
(when
(and
(Flag304-)
)
(and
(BELOWOF5-ROBOT)
(NOT-BELOWOF4-ROBOT)
(not (NOT-BELOWOF5-ROBOT))
(not (BELOWOF4-ROBOT))
)
)
(when
(and
(Flag303-)
)
(and
(BELOWOF4-ROBOT)
(NOT-BELOWOF3-ROBOT)
(not (NOT-BELOWOF4-ROBOT))
(not (BELOWOF3-ROBOT))
)
)
(when
(and
(Flag302-)
)
(and
(BELOWOF3-ROBOT)
(NOT-BELOWOF2-ROBOT)
(not (NOT-BELOWOF3-ROBOT))
(not (BELOWOF2-ROBOT))
)
)
(when
(and
(BELOWOF1-ROBOT)
)
(and
(BELOWOF2-ROBOT)
(NOT-BELOWOF1-ROBOT)
(not (NOT-BELOWOF2-ROBOT))
(not (BELOWOF1-ROBOT))
)
)
(when
(and
(Flag301-)
)
(and
(ABOVEOF19-ROBOT)
(not (NOT-ABOVEOF19-ROBOT))
)
)
(when
(and
(Flag300-)
)
(and
(ABOVEOF18-ROBOT)
(not (NOT-ABOVEOF18-ROBOT))
)
)
(when
(and
(Flag299-)
)
(and
(ABOVEOF17-ROBOT)
(not (NOT-ABOVEOF17-ROBOT))
)
)
(when
(and
(Flag298-)
)
(and
(ABOVEOF16-ROBOT)
(not (NOT-ABOVEOF16-ROBOT))
)
)
(when
(and
(Flag297-)
)
(and
(ABOVEOF15-ROBOT)
(not (NOT-ABOVEOF15-ROBOT))
)
)
(when
(and
(Flag296-)
)
(and
(ABOVEOF14-ROBOT)
(not (NOT-ABOVEOF14-ROBOT))
)
)
(when
(and
(Flag295-)
)
(and
(ABOVEOF13-ROBOT)
(not (NOT-ABOVEOF13-ROBOT))
)
)
(when
(and
(Flag294-)
)
(and
(ABOVEOF12-ROBOT)
(not (NOT-ABOVEOF12-ROBOT))
)
)
(when
(and
(Flag293-)
)
(and
(ABOVEOF11-ROBOT)
(not (NOT-ABOVEOF11-ROBOT))
)
)
(when
(and
(Flag292-)
)
(and
(ABOVEOF10-ROBOT)
(not (NOT-ABOVEOF10-ROBOT))
)
)
(when
(and
(Flag291-)
)
(and
(ABOVEOF9-ROBOT)
(not (NOT-ABOVEOF9-ROBOT))
)
)
(when
(and
(Flag290-)
)
(and
(ABOVEOF8-ROBOT)
(not (NOT-ABOVEOF8-ROBOT))
)
)
(when
(and
(Flag289-)
)
(and
(ABOVEOF7-ROBOT)
(not (NOT-ABOVEOF7-ROBOT))
)
)
(when
(and
(Flag288-)
)
(and
(ABOVEOF6-ROBOT)
(not (NOT-ABOVEOF6-ROBOT))
)
)
(when
(and
(Flag287-)
)
(and
(ABOVEOF5-ROBOT)
(not (NOT-ABOVEOF5-ROBOT))
)
)
(when
(and
(Flag286-)
)
(and
(ABOVEOF4-ROBOT)
(not (NOT-ABOVEOF4-ROBOT))
)
)
(when
(and
(Flag285-)
)
(and
(ABOVEOF3-ROBOT)
(not (NOT-ABOVEOF3-ROBOT))
)
)
(when
(and
(Flag284-)
)
(and
(ABOVEOF2-ROBOT)
(not (NOT-ABOVEOF2-ROBOT))
)
)
(ABOVEOF1-ROBOT)
(CHECKCONSISTENCY-)
(not (NOT-ABOVEOF1-ROBOT))
(not (NOT-CHECKCONSISTENCY-))
)
)
(:derived (Flag2-)(and (NOT-CHECKCONSISTENCY-)(NOT-ERROR-)))

(:derived (Flag3-)(and (RIGHTOF2-ROBOT)))

(:derived (Flag3-)(and (LEFTOF19-ROBOT)))

(:derived (Flag22-)(and (LEFTOF19-ROBOT)))

(:derived (Flag23-)(and (LEFTOF19-ROBOT)))

(:derived (Flag24-)(and (RIGHTOF2-ROBOT)))

(:derived (Flag25-)(and (RIGHTOF2-ROBOT)))

(:derived (Flag319-)(and (BELOWOF19-ROBOT)))

(:derived (Flag564-)(and (RIGHTOF2-ROBOT)))

(:derived (Flag567-)(and (BELOWOF19-ROBOT)))

)
