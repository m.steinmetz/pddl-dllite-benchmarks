(define (domain grounded-SIMPLE-ADL-ROBOT)
(:requirements
:strips
)
(:predicates
(Flag281-)
(Flag280-)
(Flag261-)
(Flag24-)
(Flag23-)
(Flag22-)
(Flag21-)
(Flag3-)
(Flag2-)
(ROW17-ROBOT)
(ROW16-ROBOT)
(ROW15-ROBOT)
(ROW14-ROBOT)
(ROW13-ROBOT)
(ROW12-ROBOT)
(ROW11-ROBOT)
(ROW10-ROBOT)
(ROW9-ROBOT)
(ROW8-ROBOT)
(ROW7-ROBOT)
(ROW6-ROBOT)
(ROW5-ROBOT)
(ROW4-ROBOT)
(ROW3-ROBOT)
(ROW2-ROBOT)
(ROW1-ROBOT)
(ROW0-ROBOT)
(ABOVEOF17-ROBOT)
(ABOVEOF16-ROBOT)
(ABOVEOF15-ROBOT)
(ABOVEOF14-ROBOT)
(ABOVEOF13-ROBOT)
(ABOVEOF12-ROBOT)
(ABOVEOF11-ROBOT)
(ABOVEOF10-ROBOT)
(ABOVEOF9-ROBOT)
(ABOVEOF8-ROBOT)
(ABOVEOF7-ROBOT)
(ABOVEOF6-ROBOT)
(ABOVEOF5-ROBOT)
(ABOVEOF4-ROBOT)
(ABOVEOF3-ROBOT)
(ABOVEOF2-ROBOT)
(ABOVEOF1-ROBOT)
(BELOWOF17-ROBOT)
(BELOWOF16-ROBOT)
(BELOWOF15-ROBOT)
(BELOWOF14-ROBOT)
(BELOWOF13-ROBOT)
(BELOWOF12-ROBOT)
(BELOWOF11-ROBOT)
(BELOWOF10-ROBOT)
(BELOWOF9-ROBOT)
(BELOWOF8-ROBOT)
(BELOWOF7-ROBOT)
(BELOWOF6-ROBOT)
(BELOWOF5-ROBOT)
(BELOWOF4-ROBOT)
(BELOWOF3-ROBOT)
(BELOWOF2-ROBOT)
(BELOWOF1-ROBOT)
(CHECKCONSISTENCY-)
(ROW18-ROBOT)
(BELOWOF19-ROBOT)
(NOT-BELOWOF18-ROBOT)
(ABOVEOF18-ROBOT)
(Flag522-)
(Flag520-)
(Flag518-)
(Flag517-)
(Flag515-)
(Flag514-)
(Flag513-)
(Flag511-)
(Flag510-)
(Flag509-)
(Flag508-)
(Flag506-)
(Flag505-)
(Flag504-)
(Flag503-)
(Flag502-)
(Flag500-)
(Flag499-)
(Flag498-)
(Flag497-)
(Flag496-)
(Flag495-)
(Flag494-)
(Flag492-)
(Flag491-)
(Flag490-)
(Flag489-)
(Flag488-)
(Flag487-)
(Flag486-)
(Flag485-)
(Flag483-)
(Flag482-)
(Flag481-)
(Flag480-)
(Flag479-)
(Flag478-)
(Flag477-)
(Flag476-)
(Flag474-)
(Flag473-)
(Flag472-)
(Flag471-)
(Flag470-)
(Flag469-)
(Flag468-)
(Flag467-)
(Flag466-)
(Flag464-)
(Flag463-)
(Flag462-)
(Flag461-)
(Flag460-)
(Flag459-)
(Flag458-)
(Flag457-)
(Flag456-)
(Flag455-)
(Flag454-)
(Flag452-)
(Flag451-)
(Flag450-)
(Flag449-)
(Flag448-)
(Flag447-)
(Flag446-)
(Flag445-)
(Flag444-)
(Flag443-)
(Flag442-)
(Flag441-)
(Flag440-)
(Flag438-)
(Flag437-)
(Flag436-)
(Flag435-)
(Flag434-)
(Flag433-)
(Flag432-)
(Flag431-)
(Flag430-)
(Flag429-)
(Flag428-)
(Flag427-)
(Flag426-)
(Flag424-)
(Flag423-)
(Flag422-)
(Flag421-)
(Flag420-)
(Flag419-)
(Flag418-)
(Flag417-)
(Flag416-)
(Flag415-)
(Flag414-)
(Flag413-)
(Flag412-)
(Flag411-)
(Flag410-)
(Flag409-)
(Flag407-)
(Flag406-)
(Flag405-)
(Flag404-)
(Flag403-)
(Flag402-)
(Flag401-)
(Flag400-)
(Flag399-)
(Flag398-)
(Flag397-)
(Flag396-)
(Flag395-)
(Flag394-)
(Flag393-)
(Flag392-)
(Flag390-)
(Flag389-)
(Flag388-)
(Flag387-)
(Flag386-)
(Flag385-)
(Flag384-)
(Flag383-)
(Flag382-)
(Flag381-)
(Flag380-)
(Flag379-)
(Flag378-)
(Flag377-)
(Flag376-)
(Flag375-)
(Flag373-)
(Flag372-)
(Flag371-)
(Flag370-)
(Flag369-)
(Flag368-)
(Flag367-)
(Flag366-)
(Flag365-)
(Flag364-)
(Flag363-)
(Flag362-)
(Flag361-)
(Flag360-)
(Flag359-)
(Flag358-)
(Flag357-)
(Flag356-)
(Flag354-)
(Flag353-)
(Flag352-)
(Flag351-)
(Flag350-)
(Flag349-)
(Flag348-)
(Flag347-)
(Flag346-)
(Flag345-)
(Flag344-)
(Flag343-)
(Flag342-)
(Flag341-)
(Flag340-)
(Flag339-)
(Flag338-)
(Flag337-)
(Flag335-)
(Flag334-)
(Flag333-)
(Flag332-)
(Flag331-)
(Flag330-)
(Flag329-)
(Flag328-)
(Flag327-)
(Flag326-)
(Flag325-)
(Flag324-)
(Flag323-)
(Flag322-)
(Flag321-)
(Flag320-)
(Flag319-)
(Flag318-)
(Flag317-)
(Flag316-)
(Flag315-)
(Flag314-)
(Flag313-)
(Flag312-)
(Flag311-)
(Flag310-)
(Flag309-)
(Flag308-)
(Flag307-)
(Flag306-)
(Flag305-)
(Flag304-)
(Flag303-)
(Flag302-)
(Flag301-)
(Flag300-)
(Flag299-)
(Flag298-)
(Flag297-)
(Flag296-)
(Flag295-)
(Flag294-)
(Flag293-)
(Flag292-)
(Flag291-)
(Flag290-)
(Flag289-)
(Flag288-)
(Flag287-)
(Flag286-)
(Flag285-)
(Flag284-)
(Flag283-)
(Flag282-)
(Flag279-)
(Flag278-)
(Flag277-)
(Flag276-)
(Flag275-)
(Flag274-)
(Flag273-)
(Flag272-)
(Flag271-)
(Flag270-)
(Flag269-)
(Flag268-)
(Flag267-)
(Flag266-)
(Flag265-)
(Flag264-)
(Flag4-)
(ERROR-)
(COLUMN17-ROBOT)
(COLUMN16-ROBOT)
(COLUMN15-ROBOT)
(COLUMN14-ROBOT)
(COLUMN13-ROBOT)
(COLUMN12-ROBOT)
(COLUMN11-ROBOT)
(COLUMN10-ROBOT)
(COLUMN9-ROBOT)
(COLUMN8-ROBOT)
(COLUMN7-ROBOT)
(COLUMN6-ROBOT)
(COLUMN5-ROBOT)
(COLUMN4-ROBOT)
(COLUMN3-ROBOT)
(COLUMN2-ROBOT)
(COLUMN1-ROBOT)
(COLUMN0-ROBOT)
(RIGHTOF17-ROBOT)
(RIGHTOF16-ROBOT)
(RIGHTOF15-ROBOT)
(RIGHTOF14-ROBOT)
(RIGHTOF13-ROBOT)
(RIGHTOF12-ROBOT)
(RIGHTOF11-ROBOT)
(RIGHTOF10-ROBOT)
(RIGHTOF9-ROBOT)
(RIGHTOF8-ROBOT)
(RIGHTOF7-ROBOT)
(RIGHTOF6-ROBOT)
(RIGHTOF5-ROBOT)
(RIGHTOF4-ROBOT)
(RIGHTOF3-ROBOT)
(RIGHTOF1-ROBOT)
(NOT-RIGHTOF2-ROBOT)
(RIGHTOF0-ROBOT)
(LEFTOF17-ROBOT)
(LEFTOF16-ROBOT)
(LEFTOF15-ROBOT)
(LEFTOF14-ROBOT)
(LEFTOF13-ROBOT)
(LEFTOF12-ROBOT)
(LEFTOF11-ROBOT)
(LEFTOF10-ROBOT)
(LEFTOF9-ROBOT)
(LEFTOF8-ROBOT)
(LEFTOF7-ROBOT)
(LEFTOF6-ROBOT)
(LEFTOF5-ROBOT)
(LEFTOF4-ROBOT)
(LEFTOF3-ROBOT)
(LEFTOF2-ROBOT)
(LEFTOF1-ROBOT)
(COLUMN18-ROBOT)
(LEFTOF19-ROBOT)
(NOT-LEFTOF18-ROBOT)
(RIGHTOF18-ROBOT)
(Flag521-)
(Flag519-)
(Flag516-)
(Flag512-)
(Flag507-)
(Flag501-)
(Flag493-)
(Flag484-)
(Flag475-)
(Flag465-)
(Flag453-)
(Flag439-)
(Flag425-)
(Flag408-)
(Flag391-)
(Flag374-)
(Flag355-)
(Flag336-)
(Flag262-)
(Flag259-)
(Flag257-)
(Flag256-)
(Flag254-)
(Flag253-)
(Flag252-)
(Flag250-)
(Flag249-)
(Flag248-)
(Flag247-)
(Flag246-)
(Flag244-)
(Flag243-)
(Flag242-)
(Flag241-)
(Flag240-)
(Flag239-)
(Flag237-)
(Flag236-)
(Flag235-)
(Flag234-)
(Flag233-)
(Flag232-)
(Flag230-)
(Flag229-)
(Flag228-)
(Flag227-)
(Flag226-)
(Flag225-)
(Flag224-)
(Flag223-)
(Flag221-)
(Flag220-)
(Flag219-)
(Flag218-)
(Flag217-)
(Flag216-)
(Flag215-)
(Flag214-)
(Flag212-)
(Flag211-)
(Flag210-)
(Flag209-)
(Flag208-)
(Flag207-)
(Flag206-)
(Flag205-)
(Flag204-)
(Flag203-)
(Flag201-)
(Flag200-)
(Flag199-)
(Flag198-)
(Flag197-)
(Flag196-)
(Flag195-)
(Flag194-)
(Flag193-)
(Flag192-)
(Flag190-)
(Flag189-)
(Flag188-)
(Flag187-)
(Flag186-)
(Flag185-)
(Flag184-)
(Flag183-)
(Flag182-)
(Flag181-)
(Flag180-)
(Flag179-)
(Flag177-)
(Flag176-)
(Flag175-)
(Flag174-)
(Flag173-)
(Flag172-)
(Flag171-)
(Flag170-)
(Flag169-)
(Flag168-)
(Flag167-)
(Flag166-)
(Flag165-)
(Flag163-)
(Flag162-)
(Flag161-)
(Flag160-)
(Flag159-)
(Flag158-)
(Flag157-)
(Flag156-)
(Flag155-)
(Flag154-)
(Flag153-)
(Flag152-)
(Flag151-)
(Flag150-)
(Flag149-)
(Flag147-)
(Flag146-)
(Flag145-)
(Flag144-)
(Flag143-)
(Flag142-)
(Flag141-)
(Flag140-)
(Flag139-)
(Flag138-)
(Flag137-)
(Flag136-)
(Flag135-)
(Flag134-)
(Flag132-)
(Flag131-)
(Flag130-)
(Flag129-)
(Flag128-)
(Flag127-)
(Flag126-)
(Flag125-)
(Flag124-)
(Flag123-)
(Flag122-)
(Flag121-)
(Flag120-)
(Flag119-)
(Flag118-)
(Flag117-)
(Flag116-)
(Flag115-)
(Flag113-)
(Flag112-)
(Flag111-)
(Flag110-)
(Flag109-)
(Flag108-)
(Flag107-)
(Flag106-)
(Flag105-)
(Flag104-)
(Flag103-)
(Flag102-)
(Flag101-)
(Flag100-)
(Flag99-)
(Flag98-)
(Flag97-)
(Flag96-)
(Flag94-)
(Flag93-)
(Flag92-)
(Flag91-)
(Flag90-)
(Flag89-)
(Flag88-)
(Flag87-)
(Flag86-)
(Flag85-)
(Flag84-)
(Flag83-)
(Flag82-)
(Flag81-)
(Flag80-)
(Flag79-)
(Flag78-)
(Flag76-)
(Flag75-)
(Flag74-)
(Flag73-)
(Flag72-)
(Flag71-)
(Flag70-)
(Flag69-)
(Flag68-)
(Flag67-)
(Flag66-)
(Flag65-)
(Flag64-)
(Flag63-)
(Flag62-)
(Flag61-)
(Flag60-)
(Flag59-)
(Flag58-)
(Flag57-)
(Flag56-)
(Flag55-)
(Flag54-)
(Flag53-)
(Flag52-)
(Flag51-)
(Flag50-)
(Flag49-)
(Flag48-)
(Flag47-)
(Flag46-)
(Flag45-)
(Flag44-)
(Flag43-)
(Flag42-)
(Flag41-)
(Flag40-)
(Flag39-)
(Flag38-)
(Flag37-)
(Flag36-)
(Flag35-)
(Flag34-)
(Flag33-)
(Flag32-)
(Flag31-)
(Flag30-)
(Flag29-)
(Flag28-)
(Flag27-)
(Flag26-)
(Flag25-)
(Flag20-)
(Flag19-)
(Flag18-)
(Flag17-)
(Flag16-)
(Flag15-)
(Flag14-)
(Flag13-)
(Flag12-)
(Flag11-)
(Flag10-)
(Flag9-)
(Flag8-)
(Flag7-)
(Flag6-)
(Flag5-)
(Flag1-)
(Flag263-)
(Flag260-)
(Flag258-)
(Flag255-)
(Flag251-)
(Flag245-)
(Flag238-)
(Flag231-)
(Flag222-)
(Flag213-)
(Flag202-)
(Flag191-)
(Flag178-)
(Flag164-)
(Flag148-)
(Flag133-)
(Flag114-)
(Flag95-)
(Flag77-)
(NOT-COLUMN18-ROBOT)
(NOT-COLUMN17-ROBOT)
(NOT-COLUMN16-ROBOT)
(NOT-COLUMN15-ROBOT)
(NOT-COLUMN14-ROBOT)
(NOT-COLUMN13-ROBOT)
(NOT-COLUMN12-ROBOT)
(NOT-COLUMN11-ROBOT)
(NOT-COLUMN10-ROBOT)
(NOT-COLUMN9-ROBOT)
(NOT-COLUMN8-ROBOT)
(NOT-COLUMN7-ROBOT)
(NOT-COLUMN6-ROBOT)
(NOT-COLUMN5-ROBOT)
(NOT-COLUMN4-ROBOT)
(NOT-COLUMN3-ROBOT)
(NOT-COLUMN2-ROBOT)
(NOT-COLUMN1-ROBOT)
(NOT-LEFTOF19-ROBOT)
(LEFTOF18-ROBOT)
(NOT-LEFTOF17-ROBOT)
(NOT-LEFTOF16-ROBOT)
(NOT-LEFTOF15-ROBOT)
(NOT-LEFTOF14-ROBOT)
(NOT-LEFTOF13-ROBOT)
(NOT-LEFTOF12-ROBOT)
(NOT-LEFTOF11-ROBOT)
(NOT-LEFTOF10-ROBOT)
(NOT-LEFTOF9-ROBOT)
(NOT-LEFTOF8-ROBOT)
(NOT-LEFTOF7-ROBOT)
(NOT-LEFTOF6-ROBOT)
(NOT-LEFTOF5-ROBOT)
(NOT-LEFTOF4-ROBOT)
(NOT-LEFTOF3-ROBOT)
(NOT-LEFTOF2-ROBOT)
(NOT-RIGHTOF18-ROBOT)
(NOT-RIGHTOF17-ROBOT)
(NOT-RIGHTOF16-ROBOT)
(NOT-RIGHTOF15-ROBOT)
(NOT-RIGHTOF14-ROBOT)
(NOT-RIGHTOF13-ROBOT)
(NOT-RIGHTOF12-ROBOT)
(NOT-RIGHTOF11-ROBOT)
(NOT-RIGHTOF10-ROBOT)
(NOT-RIGHTOF9-ROBOT)
(NOT-RIGHTOF8-ROBOT)
(NOT-RIGHTOF7-ROBOT)
(NOT-RIGHTOF6-ROBOT)
(NOT-RIGHTOF5-ROBOT)
(NOT-RIGHTOF4-ROBOT)
(NOT-RIGHTOF3-ROBOT)
(NOT-RIGHTOF1-ROBOT)
(NOT-CHECKCONSISTENCY-)
(NOT-COLUMN0-ROBOT)
(RIGHTOF2-ROBOT)
(NOT-RIGHTOF0-ROBOT)
(NOT-LEFTOF1-ROBOT)
(NOT-ERROR-)
(NOT-ROW18-ROBOT)
(NOT-ROW17-ROBOT)
(NOT-ROW16-ROBOT)
(NOT-ROW15-ROBOT)
(NOT-ROW14-ROBOT)
(NOT-ROW13-ROBOT)
(NOT-ROW12-ROBOT)
(NOT-ROW11-ROBOT)
(NOT-ROW10-ROBOT)
(NOT-ROW9-ROBOT)
(NOT-ROW8-ROBOT)
(NOT-ROW7-ROBOT)
(NOT-ROW6-ROBOT)
(NOT-ROW5-ROBOT)
(NOT-ROW4-ROBOT)
(NOT-ROW3-ROBOT)
(NOT-ROW2-ROBOT)
(NOT-ROW1-ROBOT)
(NOT-BELOWOF19-ROBOT)
(BELOWOF18-ROBOT)
(NOT-BELOWOF17-ROBOT)
(NOT-BELOWOF16-ROBOT)
(NOT-BELOWOF15-ROBOT)
(NOT-BELOWOF14-ROBOT)
(NOT-BELOWOF13-ROBOT)
(NOT-BELOWOF12-ROBOT)
(NOT-BELOWOF11-ROBOT)
(NOT-BELOWOF10-ROBOT)
(NOT-BELOWOF9-ROBOT)
(NOT-BELOWOF8-ROBOT)
(NOT-BELOWOF7-ROBOT)
(NOT-BELOWOF6-ROBOT)
(NOT-BELOWOF5-ROBOT)
(NOT-BELOWOF4-ROBOT)
(NOT-BELOWOF3-ROBOT)
(NOT-BELOWOF2-ROBOT)
(NOT-ABOVEOF18-ROBOT)
(NOT-ABOVEOF17-ROBOT)
(NOT-ABOVEOF16-ROBOT)
(NOT-ABOVEOF15-ROBOT)
(NOT-ABOVEOF14-ROBOT)
(NOT-ABOVEOF13-ROBOT)
(NOT-ABOVEOF12-ROBOT)
(NOT-ABOVEOF11-ROBOT)
(NOT-ABOVEOF10-ROBOT)
(NOT-ABOVEOF9-ROBOT)
(NOT-ABOVEOF8-ROBOT)
(NOT-ABOVEOF7-ROBOT)
(NOT-ABOVEOF6-ROBOT)
(NOT-ABOVEOF5-ROBOT)
(NOT-ABOVEOF4-ROBOT)
(NOT-ABOVEOF3-ROBOT)
(NOT-ABOVEOF2-ROBOT)
(NOT-ABOVEOF1-ROBOT)
(NOT-ROW0-ROBOT)
(NOT-BELOWOF1-ROBOT)
)
(:derived (Flag77-)(and (Flag40-)))

(:derived (Flag77-)(and (Flag41-)))

(:derived (Flag77-)(and (Flag42-)))

(:derived (Flag77-)(and (Flag43-)))

(:derived (Flag77-)(and (Flag44-)))

(:derived (Flag77-)(and (Flag45-)))

(:derived (Flag77-)(and (Flag46-)))

(:derived (Flag77-)(and (Flag47-)))

(:derived (Flag77-)(and (Flag48-)))

(:derived (Flag77-)(and (Flag49-)))

(:derived (Flag77-)(and (Flag50-)))

(:derived (Flag77-)(and (Flag51-)))

(:derived (Flag77-)(and (Flag52-)))

(:derived (Flag77-)(and (Flag53-)))

(:derived (Flag77-)(and (Flag54-)))

(:derived (Flag77-)(and (Flag55-)))

(:derived (Flag77-)(and (Flag56-)))

(:derived (Flag77-)(and (Flag57-)))

(:derived (Flag77-)(and (Flag58-)))

(:derived (Flag77-)(and (Flag59-)))

(:derived (Flag77-)(and (Flag60-)))

(:derived (Flag77-)(and (Flag61-)))

(:derived (Flag77-)(and (Flag62-)))

(:derived (Flag77-)(and (Flag63-)))

(:derived (Flag77-)(and (Flag64-)))

(:derived (Flag77-)(and (Flag65-)))

(:derived (Flag77-)(and (Flag66-)))

(:derived (Flag77-)(and (Flag67-)))

(:derived (Flag77-)(and (Flag68-)))

(:derived (Flag77-)(and (Flag69-)))

(:derived (Flag77-)(and (Flag70-)))

(:derived (Flag77-)(and (Flag71-)))

(:derived (Flag77-)(and (Flag72-)))

(:derived (Flag77-)(and (Flag73-)))

(:derived (Flag77-)(and (Flag74-)))

(:derived (Flag77-)(and (Flag75-)))

(:derived (Flag77-)(and (Flag76-)))

(:derived (Flag95-)(and (Flag40-)))

(:derived (Flag95-)(and (Flag41-)))

(:derived (Flag95-)(and (Flag42-)))

(:derived (Flag95-)(and (Flag43-)))

(:derived (Flag95-)(and (Flag78-)))

(:derived (Flag95-)(and (Flag44-)))

(:derived (Flag95-)(and (Flag45-)))

(:derived (Flag95-)(and (Flag79-)))

(:derived (Flag95-)(and (Flag66-)))

(:derived (Flag95-)(and (Flag80-)))

(:derived (Flag95-)(and (Flag47-)))

(:derived (Flag95-)(and (Flag48-)))

(:derived (Flag95-)(and (Flag81-)))

(:derived (Flag95-)(and (Flag50-)))

(:derived (Flag95-)(and (Flag62-)))

(:derived (Flag95-)(and (Flag53-)))

(:derived (Flag95-)(and (Flag54-)))

(:derived (Flag95-)(and (Flag82-)))

(:derived (Flag95-)(and (Flag55-)))

(:derived (Flag95-)(and (Flag56-)))

(:derived (Flag95-)(and (Flag64-)))

(:derived (Flag95-)(and (Flag57-)))

(:derived (Flag95-)(and (Flag83-)))

(:derived (Flag95-)(and (Flag58-)))

(:derived (Flag95-)(and (Flag59-)))

(:derived (Flag95-)(and (Flag60-)))

(:derived (Flag95-)(and (Flag61-)))

(:derived (Flag95-)(and (Flag84-)))

(:derived (Flag95-)(and (Flag85-)))

(:derived (Flag95-)(and (Flag52-)))

(:derived (Flag95-)(and (Flag63-)))

(:derived (Flag95-)(and (Flag46-)))

(:derived (Flag95-)(and (Flag86-)))

(:derived (Flag95-)(and (Flag87-)))

(:derived (Flag95-)(and (Flag65-)))

(:derived (Flag95-)(and (Flag88-)))

(:derived (Flag95-)(and (Flag89-)))

(:derived (Flag95-)(and (Flag90-)))

(:derived (Flag95-)(and (Flag91-)))

(:derived (Flag95-)(and (Flag92-)))

(:derived (Flag95-)(and (Flag67-)))

(:derived (Flag95-)(and (Flag68-)))

(:derived (Flag95-)(and (Flag69-)))

(:derived (Flag95-)(and (Flag70-)))

(:derived (Flag95-)(and (Flag71-)))

(:derived (Flag95-)(and (Flag72-)))

(:derived (Flag95-)(and (Flag73-)))

(:derived (Flag95-)(and (Flag74-)))

(:derived (Flag95-)(and (Flag75-)))

(:derived (Flag95-)(and (Flag93-)))

(:derived (Flag95-)(and (Flag76-)))

(:derived (Flag95-)(and (Flag94-)))

(:derived (Flag114-)(and (Flag40-)))

(:derived (Flag114-)(and (Flag96-)))

(:derived (Flag114-)(and (Flag41-)))

(:derived (Flag114-)(and (Flag42-)))

(:derived (Flag114-)(and (Flag43-)))

(:derived (Flag114-)(and (Flag78-)))

(:derived (Flag114-)(and (Flag44-)))

(:derived (Flag114-)(and (Flag45-)))

(:derived (Flag114-)(and (Flag79-)))

(:derived (Flag114-)(and (Flag73-)))

(:derived (Flag114-)(and (Flag97-)))

(:derived (Flag114-)(and (Flag80-)))

(:derived (Flag114-)(and (Flag47-)))

(:derived (Flag114-)(and (Flag48-)))

(:derived (Flag114-)(and (Flag98-)))

(:derived (Flag114-)(and (Flag99-)))

(:derived (Flag114-)(and (Flag81-)))

(:derived (Flag114-)(and (Flag100-)))

(:derived (Flag114-)(and (Flag50-)))

(:derived (Flag114-)(and (Flag85-)))

(:derived (Flag114-)(and (Flag62-)))

(:derived (Flag114-)(and (Flag53-)))

(:derived (Flag114-)(and (Flag54-)))

(:derived (Flag114-)(and (Flag101-)))

(:derived (Flag114-)(and (Flag82-)))

(:derived (Flag114-)(and (Flag55-)))

(:derived (Flag114-)(and (Flag56-)))

(:derived (Flag114-)(and (Flag64-)))

(:derived (Flag114-)(and (Flag57-)))

(:derived (Flag114-)(and (Flag83-)))

(:derived (Flag114-)(and (Flag58-)))

(:derived (Flag114-)(and (Flag59-)))

(:derived (Flag114-)(and (Flag102-)))

(:derived (Flag114-)(and (Flag60-)))

(:derived (Flag114-)(and (Flag61-)))

(:derived (Flag114-)(and (Flag84-)))

(:derived (Flag114-)(and (Flag103-)))

(:derived (Flag114-)(and (Flag52-)))

(:derived (Flag114-)(and (Flag63-)))

(:derived (Flag114-)(and (Flag104-)))

(:derived (Flag114-)(and (Flag46-)))

(:derived (Flag114-)(and (Flag74-)))

(:derived (Flag114-)(and (Flag86-)))

(:derived (Flag114-)(and (Flag87-)))

(:derived (Flag114-)(and (Flag65-)))

(:derived (Flag114-)(and (Flag66-)))

(:derived (Flag114-)(and (Flag89-)))

(:derived (Flag114-)(and (Flag105-)))

(:derived (Flag114-)(and (Flag106-)))

(:derived (Flag114-)(and (Flag90-)))

(:derived (Flag114-)(and (Flag91-)))

(:derived (Flag114-)(and (Flag92-)))

(:derived (Flag114-)(and (Flag107-)))

(:derived (Flag114-)(and (Flag108-)))

(:derived (Flag114-)(and (Flag67-)))

(:derived (Flag114-)(and (Flag68-)))

(:derived (Flag114-)(and (Flag109-)))

(:derived (Flag114-)(and (Flag110-)))

(:derived (Flag114-)(and (Flag69-)))

(:derived (Flag114-)(and (Flag111-)))

(:derived (Flag114-)(and (Flag72-)))

(:derived (Flag114-)(and (Flag112-)))

(:derived (Flag114-)(and (Flag113-)))

(:derived (Flag114-)(and (Flag75-)))

(:derived (Flag114-)(and (Flag93-)))

(:derived (Flag114-)(and (Flag76-)))

(:derived (Flag133-)(and (Flag115-)))

(:derived (Flag133-)(and (Flag96-)))

(:derived (Flag133-)(and (Flag116-)))

(:derived (Flag133-)(and (Flag42-)))

(:derived (Flag133-)(and (Flag117-)))

(:derived (Flag133-)(and (Flag43-)))

(:derived (Flag133-)(and (Flag78-)))

(:derived (Flag133-)(and (Flag44-)))

(:derived (Flag133-)(and (Flag79-)))

(:derived (Flag133-)(and (Flag73-)))

(:derived (Flag133-)(and (Flag118-)))

(:derived (Flag133-)(and (Flag119-)))

(:derived (Flag133-)(and (Flag97-)))

(:derived (Flag133-)(and (Flag120-)))

(:derived (Flag133-)(and (Flag80-)))

(:derived (Flag133-)(and (Flag47-)))

(:derived (Flag133-)(and (Flag48-)))

(:derived (Flag133-)(and (Flag65-)))

(:derived (Flag133-)(and (Flag121-)))

(:derived (Flag133-)(and (Flag98-)))

(:derived (Flag133-)(and (Flag99-)))

(:derived (Flag133-)(and (Flag122-)))

(:derived (Flag133-)(and (Flag81-)))

(:derived (Flag133-)(and (Flag40-)))

(:derived (Flag133-)(and (Flag100-)))

(:derived (Flag133-)(and (Flag50-)))

(:derived (Flag133-)(and (Flag46-)))

(:derived (Flag133-)(and (Flag85-)))

(:derived (Flag133-)(and (Flag62-)))

(:derived (Flag133-)(and (Flag53-)))

(:derived (Flag133-)(and (Flag54-)))

(:derived (Flag133-)(and (Flag101-)))

(:derived (Flag133-)(and (Flag82-)))

(:derived (Flag133-)(and (Flag55-)))

(:derived (Flag133-)(and (Flag102-)))

(:derived (Flag133-)(and (Flag57-)))

(:derived (Flag133-)(and (Flag87-)))

(:derived (Flag133-)(and (Flag123-)))

(:derived (Flag133-)(and (Flag124-)))

(:derived (Flag133-)(and (Flag125-)))

(:derived (Flag133-)(and (Flag59-)))

(:derived (Flag133-)(and (Flag126-)))

(:derived (Flag133-)(and (Flag60-)))

(:derived (Flag133-)(and (Flag61-)))

(:derived (Flag133-)(and (Flag84-)))

(:derived (Flag133-)(and (Flag127-)))

(:derived (Flag133-)(and (Flag52-)))

(:derived (Flag133-)(and (Flag63-)))

(:derived (Flag133-)(and (Flag104-)))

(:derived (Flag133-)(and (Flag128-)))

(:derived (Flag133-)(and (Flag129-)))

(:derived (Flag133-)(and (Flag74-)))

(:derived (Flag133-)(and (Flag64-)))

(:derived (Flag133-)(and (Flag83-)))

(:derived (Flag133-)(and (Flag58-)))

(:derived (Flag133-)(and (Flag66-)))

(:derived (Flag133-)(and (Flag89-)))

(:derived (Flag133-)(and (Flag105-)))

(:derived (Flag133-)(and (Flag106-)))

(:derived (Flag133-)(and (Flag90-)))

(:derived (Flag133-)(and (Flag91-)))

(:derived (Flag133-)(and (Flag92-)))

(:derived (Flag133-)(and (Flag41-)))

(:derived (Flag133-)(and (Flag107-)))

(:derived (Flag133-)(and (Flag108-)))

(:derived (Flag133-)(and (Flag67-)))

(:derived (Flag133-)(and (Flag68-)))

(:derived (Flag133-)(and (Flag109-)))

(:derived (Flag133-)(and (Flag110-)))

(:derived (Flag133-)(and (Flag69-)))

(:derived (Flag133-)(and (Flag111-)))

(:derived (Flag133-)(and (Flag130-)))

(:derived (Flag133-)(and (Flag86-)))

(:derived (Flag133-)(and (Flag72-)))

(:derived (Flag133-)(and (Flag131-)))

(:derived (Flag133-)(and (Flag113-)))

(:derived (Flag133-)(and (Flag75-)))

(:derived (Flag133-)(and (Flag93-)))

(:derived (Flag133-)(and (Flag76-)))

(:derived (Flag133-)(and (Flag132-)))

(:derived (Flag148-)(and (Flag41-)))

(:derived (Flag148-)(and (Flag47-)))

(:derived (Flag148-)(and (Flag134-)))

(:derived (Flag148-)(and (Flag135-)))

(:derived (Flag148-)(and (Flag124-)))

(:derived (Flag148-)(and (Flag125-)))

(:derived (Flag148-)(and (Flag126-)))

(:derived (Flag148-)(and (Flag84-)))

(:derived (Flag148-)(and (Flag127-)))

(:derived (Flag148-)(and (Flag63-)))

(:derived (Flag148-)(and (Flag136-)))

(:derived (Flag148-)(and (Flag43-)))

(:derived (Flag148-)(and (Flag92-)))

(:derived (Flag148-)(and (Flag69-)))

(:derived (Flag148-)(and (Flag72-)))

(:derived (Flag148-)(and (Flag137-)))

(:derived (Flag148-)(and (Flag42-)))

(:derived (Flag148-)(and (Flag97-)))

(:derived (Flag148-)(and (Flag48-)))

(:derived (Flag148-)(and (Flag122-)))

(:derived (Flag148-)(and (Flag138-)))

(:derived (Flag148-)(and (Flag50-)))

(:derived (Flag148-)(and (Flag52-)))

(:derived (Flag148-)(and (Flag139-)))

(:derived (Flag148-)(and (Flag82-)))

(:derived (Flag148-)(and (Flag102-)))

(:derived (Flag148-)(and (Flag57-)))

(:derived (Flag148-)(and (Flag87-)))

(:derived (Flag148-)(and (Flag123-)))

(:derived (Flag148-)(and (Flag140-)))

(:derived (Flag148-)(and (Flag61-)))

(:derived (Flag148-)(and (Flag62-)))

(:derived (Flag148-)(and (Flag128-)))

(:derived (Flag148-)(and (Flag90-)))

(:derived (Flag148-)(and (Flag101-)))

(:derived (Flag148-)(and (Flag141-)))

(:derived (Flag148-)(and (Flag142-)))

(:derived (Flag148-)(and (Flag110-)))

(:derived (Flag148-)(and (Flag111-)))

(:derived (Flag148-)(and (Flag73-)))

(:derived (Flag148-)(and (Flag143-)))

(:derived (Flag148-)(and (Flag93-)))

(:derived (Flag148-)(and (Flag115-)))

(:derived (Flag148-)(and (Flag116-)))

(:derived (Flag148-)(and (Flag44-)))

(:derived (Flag148-)(and (Flag79-)))

(:derived (Flag148-)(and (Flag118-)))

(:derived (Flag148-)(and (Flag100-)))

(:derived (Flag148-)(and (Flag108-)))

(:derived (Flag148-)(and (Flag130-)))

(:derived (Flag148-)(and (Flag120-)))

(:derived (Flag148-)(and (Flag54-)))

(:derived (Flag148-)(and (Flag83-)))

(:derived (Flag148-)(and (Flag60-)))

(:derived (Flag148-)(and (Flag58-)))

(:derived (Flag148-)(and (Flag86-)))

(:derived (Flag148-)(and (Flag66-)))

(:derived (Flag148-)(and (Flag106-)))

(:derived (Flag148-)(and (Flag67-)))

(:derived (Flag148-)(and (Flag144-)))

(:derived (Flag148-)(and (Flag109-)))

(:derived (Flag148-)(and (Flag131-)))

(:derived (Flag148-)(and (Flag75-)))

(:derived (Flag148-)(and (Flag121-)))

(:derived (Flag148-)(and (Flag145-)))

(:derived (Flag148-)(and (Flag132-)))

(:derived (Flag148-)(and (Flag40-)))

(:derived (Flag148-)(and (Flag96-)))

(:derived (Flag148-)(and (Flag146-)))

(:derived (Flag148-)(and (Flag78-)))

(:derived (Flag148-)(and (Flag46-)))

(:derived (Flag148-)(and (Flag98-)))

(:derived (Flag148-)(and (Flag119-)))

(:derived (Flag148-)(and (Flag85-)))

(:derived (Flag148-)(and (Flag53-)))

(:derived (Flag148-)(and (Flag147-)))

(:derived (Flag148-)(and (Flag55-)))

(:derived (Flag148-)(and (Flag81-)))

(:derived (Flag148-)(and (Flag104-)))

(:derived (Flag148-)(and (Flag129-)))

(:derived (Flag148-)(and (Flag65-)))

(:derived (Flag148-)(and (Flag89-)))

(:derived (Flag148-)(and (Flag105-)))

(:derived (Flag148-)(and (Flag74-)))

(:derived (Flag148-)(and (Flag91-)))

(:derived (Flag148-)(and (Flag107-)))

(:derived (Flag148-)(and (Flag68-)))

(:derived (Flag148-)(and (Flag113-)))

(:derived (Flag164-)(and (Flag41-)))

(:derived (Flag164-)(and (Flag47-)))

(:derived (Flag164-)(and (Flag149-)))

(:derived (Flag164-)(and (Flag134-)))

(:derived (Flag164-)(and (Flag150-)))

(:derived (Flag164-)(and (Flag44-)))

(:derived (Flag164-)(and (Flag135-)))

(:derived (Flag164-)(and (Flag124-)))

(:derived (Flag164-)(and (Flag125-)))

(:derived (Flag164-)(and (Flag126-)))

(:derived (Flag164-)(and (Flag84-)))

(:derived (Flag164-)(and (Flag127-)))

(:derived (Flag164-)(and (Flag63-)))

(:derived (Flag164-)(and (Flag43-)))

(:derived (Flag164-)(and (Flag151-)))

(:derived (Flag164-)(and (Flag152-)))

(:derived (Flag164-)(and (Flag92-)))

(:derived (Flag164-)(and (Flag69-)))

(:derived (Flag164-)(and (Flag72-)))

(:derived (Flag164-)(and (Flag137-)))

(:derived (Flag164-)(and (Flag42-)))

(:derived (Flag164-)(and (Flag97-)))

(:derived (Flag164-)(and (Flag48-)))

(:derived (Flag164-)(and (Flag122-)))

(:derived (Flag164-)(and (Flag153-)))

(:derived (Flag164-)(and (Flag138-)))

(:derived (Flag164-)(and (Flag52-)))

(:derived (Flag164-)(and (Flag139-)))

(:derived (Flag164-)(and (Flag82-)))

(:derived (Flag164-)(and (Flag102-)))

(:derived (Flag164-)(and (Flag57-)))

(:derived (Flag164-)(and (Flag87-)))

(:derived (Flag164-)(and (Flag123-)))

(:derived (Flag164-)(and (Flag140-)))

(:derived (Flag164-)(and (Flag61-)))

(:derived (Flag164-)(and (Flag62-)))

(:derived (Flag164-)(and (Flag154-)))

(:derived (Flag164-)(and (Flag128-)))

(:derived (Flag164-)(and (Flag90-)))

(:derived (Flag164-)(and (Flag101-)))

(:derived (Flag164-)(and (Flag155-)))

(:derived (Flag164-)(and (Flag156-)))

(:derived (Flag164-)(and (Flag141-)))

(:derived (Flag164-)(and (Flag111-)))

(:derived (Flag164-)(and (Flag73-)))

(:derived (Flag164-)(and (Flag143-)))

(:derived (Flag164-)(and (Flag93-)))

(:derived (Flag164-)(and (Flag157-)))

(:derived (Flag164-)(and (Flag115-)))

(:derived (Flag164-)(and (Flag116-)))

(:derived (Flag164-)(and (Flag58-)))

(:derived (Flag164-)(and (Flag158-)))

(:derived (Flag164-)(and (Flag79-)))

(:derived (Flag164-)(and (Flag159-)))

(:derived (Flag164-)(and (Flag118-)))

(:derived (Flag164-)(and (Flag100-)))

(:derived (Flag164-)(and (Flag108-)))

(:derived (Flag164-)(and (Flag130-)))

(:derived (Flag164-)(and (Flag160-)))

(:derived (Flag164-)(and (Flag120-)))

(:derived (Flag164-)(and (Flag83-)))

(:derived (Flag164-)(and (Flag60-)))

(:derived (Flag164-)(and (Flag66-)))

(:derived (Flag164-)(and (Flag106-)))

(:derived (Flag164-)(and (Flag67-)))

(:derived (Flag164-)(and (Flag144-)))

(:derived (Flag164-)(and (Flag109-)))

(:derived (Flag164-)(and (Flag75-)))

(:derived (Flag164-)(and (Flag121-)))

(:derived (Flag164-)(and (Flag145-)))

(:derived (Flag164-)(and (Flag132-)))

(:derived (Flag164-)(and (Flag68-)))

(:derived (Flag164-)(and (Flag96-)))

(:derived (Flag164-)(and (Flag146-)))

(:derived (Flag164-)(and (Flag78-)))

(:derived (Flag164-)(and (Flag46-)))

(:derived (Flag164-)(and (Flag98-)))

(:derived (Flag164-)(and (Flag119-)))

(:derived (Flag164-)(and (Flag85-)))

(:derived (Flag164-)(and (Flag53-)))

(:derived (Flag164-)(and (Flag147-)))

(:derived (Flag164-)(and (Flag55-)))

(:derived (Flag164-)(and (Flag81-)))

(:derived (Flag164-)(and (Flag104-)))

(:derived (Flag164-)(and (Flag161-)))

(:derived (Flag164-)(and (Flag129-)))

(:derived (Flag164-)(and (Flag162-)))

(:derived (Flag164-)(and (Flag65-)))

(:derived (Flag164-)(and (Flag89-)))

(:derived (Flag164-)(and (Flag105-)))

(:derived (Flag164-)(and (Flag74-)))

(:derived (Flag164-)(and (Flag91-)))

(:derived (Flag164-)(and (Flag107-)))

(:derived (Flag164-)(and (Flag40-)))

(:derived (Flag164-)(and (Flag163-)))

(:derived (Flag164-)(and (Flag113-)))

(:derived (Flag178-)(and (Flag41-)))

(:derived (Flag178-)(and (Flag47-)))

(:derived (Flag178-)(and (Flag134-)))

(:derived (Flag178-)(and (Flag150-)))

(:derived (Flag178-)(and (Flag44-)))

(:derived (Flag178-)(and (Flag135-)))

(:derived (Flag178-)(and (Flag124-)))

(:derived (Flag178-)(and (Flag165-)))

(:derived (Flag178-)(and (Flag126-)))

(:derived (Flag178-)(and (Flag84-)))

(:derived (Flag178-)(and (Flag85-)))

(:derived (Flag178-)(and (Flag63-)))

(:derived (Flag178-)(and (Flag166-)))

(:derived (Flag178-)(and (Flag146-)))

(:derived (Flag178-)(and (Flag151-)))

(:derived (Flag178-)(and (Flag92-)))

(:derived (Flag178-)(and (Flag69-)))

(:derived (Flag178-)(and (Flag137-)))

(:derived (Flag178-)(and (Flag42-)))

(:derived (Flag178-)(and (Flag97-)))

(:derived (Flag178-)(and (Flag48-)))

(:derived (Flag178-)(and (Flag167-)))

(:derived (Flag178-)(and (Flag122-)))

(:derived (Flag178-)(and (Flag153-)))

(:derived (Flag178-)(and (Flag138-)))

(:derived (Flag178-)(and (Flag62-)))

(:derived (Flag178-)(and (Flag139-)))

(:derived (Flag178-)(and (Flag82-)))

(:derived (Flag178-)(and (Flag102-)))

(:derived (Flag178-)(and (Flag57-)))

(:derived (Flag178-)(and (Flag87-)))

(:derived (Flag178-)(and (Flag123-)))

(:derived (Flag178-)(and (Flag140-)))

(:derived (Flag178-)(and (Flag168-)))

(:derived (Flag178-)(and (Flag61-)))

(:derived (Flag178-)(and (Flag52-)))

(:derived (Flag178-)(and (Flag154-)))

(:derived (Flag178-)(and (Flag128-)))

(:derived (Flag178-)(and (Flag169-)))

(:derived (Flag178-)(and (Flag90-)))

(:derived (Flag178-)(and (Flag101-)))

(:derived (Flag178-)(and (Flag156-)))

(:derived (Flag178-)(and (Flag141-)))

(:derived (Flag178-)(and (Flag111-)))

(:derived (Flag178-)(and (Flag73-)))

(:derived (Flag178-)(and (Flag143-)))

(:derived (Flag178-)(and (Flag93-)))

(:derived (Flag178-)(and (Flag157-)))

(:derived (Flag178-)(and (Flag115-)))

(:derived (Flag178-)(and (Flag116-)))

(:derived (Flag178-)(and (Flag58-)))

(:derived (Flag178-)(and (Flag158-)))

(:derived (Flag178-)(and (Flag79-)))

(:derived (Flag178-)(and (Flag159-)))

(:derived (Flag178-)(and (Flag118-)))

(:derived (Flag178-)(and (Flag170-)))

(:derived (Flag178-)(and (Flag171-)))

(:derived (Flag178-)(and (Flag172-)))

(:derived (Flag178-)(and (Flag100-)))

(:derived (Flag178-)(and (Flag108-)))

(:derived (Flag178-)(and (Flag173-)))

(:derived (Flag178-)(and (Flag130-)))

(:derived (Flag178-)(and (Flag160-)))

(:derived (Flag178-)(and (Flag83-)))

(:derived (Flag178-)(and (Flag174-)))

(:derived (Flag178-)(and (Flag66-)))

(:derived (Flag178-)(and (Flag106-)))

(:derived (Flag178-)(and (Flag67-)))

(:derived (Flag178-)(and (Flag144-)))

(:derived (Flag178-)(and (Flag175-)))

(:derived (Flag178-)(and (Flag121-)))

(:derived (Flag178-)(and (Flag145-)))

(:derived (Flag178-)(and (Flag176-)))

(:derived (Flag178-)(and (Flag132-)))

(:derived (Flag178-)(and (Flag40-)))

(:derived (Flag178-)(and (Flag96-)))

(:derived (Flag178-)(and (Flag43-)))

(:derived (Flag178-)(and (Flag120-)))

(:derived (Flag178-)(and (Flag46-)))

(:derived (Flag178-)(and (Flag98-)))

(:derived (Flag178-)(and (Flag60-)))

(:derived (Flag178-)(and (Flag119-)))

(:derived (Flag178-)(and (Flag127-)))

(:derived (Flag178-)(and (Flag53-)))

(:derived (Flag178-)(and (Flag147-)))

(:derived (Flag178-)(and (Flag55-)))

(:derived (Flag178-)(and (Flag177-)))

(:derived (Flag178-)(and (Flag81-)))

(:derived (Flag178-)(and (Flag104-)))

(:derived (Flag178-)(and (Flag161-)))

(:derived (Flag178-)(and (Flag129-)))

(:derived (Flag178-)(and (Flag162-)))

(:derived (Flag178-)(and (Flag65-)))

(:derived (Flag178-)(and (Flag89-)))

(:derived (Flag178-)(and (Flag105-)))

(:derived (Flag178-)(and (Flag74-)))

(:derived (Flag178-)(and (Flag91-)))

(:derived (Flag178-)(and (Flag107-)))

(:derived (Flag178-)(and (Flag68-)))

(:derived (Flag178-)(and (Flag163-)))

(:derived (Flag178-)(and (Flag113-)))

(:derived (Flag191-)(and (Flag41-)))

(:derived (Flag191-)(and (Flag47-)))

(:derived (Flag191-)(and (Flag179-)))

(:derived (Flag191-)(and (Flag180-)))

(:derived (Flag191-)(and (Flag134-)))

(:derived (Flag191-)(and (Flag150-)))

(:derived (Flag191-)(and (Flag135-)))

(:derived (Flag191-)(and (Flag124-)))

(:derived (Flag191-)(and (Flag165-)))

(:derived (Flag191-)(and (Flag126-)))

(:derived (Flag191-)(and (Flag84-)))

(:derived (Flag191-)(and (Flag85-)))

(:derived (Flag191-)(and (Flag63-)))

(:derived (Flag191-)(and (Flag181-)))

(:derived (Flag191-)(and (Flag87-)))

(:derived (Flag191-)(and (Flag43-)))

(:derived (Flag191-)(and (Flag151-)))

(:derived (Flag191-)(and (Flag92-)))

(:derived (Flag191-)(and (Flag159-)))

(:derived (Flag191-)(and (Flag69-)))

(:derived (Flag191-)(and (Flag137-)))

(:derived (Flag191-)(and (Flag42-)))

(:derived (Flag191-)(and (Flag97-)))

(:derived (Flag191-)(and (Flag48-)))

(:derived (Flag191-)(and (Flag167-)))

(:derived (Flag191-)(and (Flag122-)))

(:derived (Flag191-)(and (Flag153-)))

(:derived (Flag191-)(and (Flag182-)))

(:derived (Flag191-)(and (Flag139-)))

(:derived (Flag191-)(and (Flag102-)))

(:derived (Flag191-)(and (Flag89-)))

(:derived (Flag191-)(and (Flag57-)))

(:derived (Flag191-)(and (Flag166-)))

(:derived (Flag191-)(and (Flag123-)))

(:derived (Flag191-)(and (Flag140-)))

(:derived (Flag191-)(and (Flag61-)))

(:derived (Flag191-)(and (Flag183-)))

(:derived (Flag191-)(and (Flag154-)))

(:derived (Flag191-)(and (Flag128-)))

(:derived (Flag191-)(and (Flag184-)))

(:derived (Flag191-)(and (Flag169-)))

(:derived (Flag191-)(and (Flag90-)))

(:derived (Flag191-)(and (Flag74-)))

(:derived (Flag191-)(and (Flag156-)))

(:derived (Flag191-)(and (Flag141-)))

(:derived (Flag191-)(and (Flag111-)))

(:derived (Flag191-)(and (Flag73-)))

(:derived (Flag191-)(and (Flag143-)))

(:derived (Flag191-)(and (Flag93-)))

(:derived (Flag191-)(and (Flag157-)))

(:derived (Flag191-)(and (Flag115-)))

(:derived (Flag191-)(and (Flag116-)))

(:derived (Flag191-)(and (Flag44-)))

(:derived (Flag191-)(and (Flag158-)))

(:derived (Flag191-)(and (Flag79-)))

(:derived (Flag191-)(and (Flag185-)))

(:derived (Flag191-)(and (Flag118-)))

(:derived (Flag191-)(and (Flag170-)))

(:derived (Flag191-)(and (Flag172-)))

(:derived (Flag191-)(and (Flag108-)))

(:derived (Flag191-)(and (Flag173-)))

(:derived (Flag191-)(and (Flag130-)))

(:derived (Flag191-)(and (Flag160-)))

(:derived (Flag191-)(and (Flag101-)))

(:derived (Flag191-)(and (Flag83-)))

(:derived (Flag191-)(and (Flag186-)))

(:derived (Flag191-)(and (Flag174-)))

(:derived (Flag191-)(and (Flag58-)))

(:derived (Flag191-)(and (Flag66-)))

(:derived (Flag191-)(and (Flag106-)))

(:derived (Flag191-)(and (Flag187-)))

(:derived (Flag191-)(and (Flag67-)))

(:derived (Flag191-)(and (Flag144-)))

(:derived (Flag191-)(and (Flag175-)))

(:derived (Flag191-)(and (Flag121-)))

(:derived (Flag191-)(and (Flag145-)))

(:derived (Flag191-)(and (Flag176-)))

(:derived (Flag191-)(and (Flag132-)))

(:derived (Flag191-)(and (Flag40-)))

(:derived (Flag191-)(and (Flag96-)))

(:derived (Flag191-)(and (Flag146-)))

(:derived (Flag191-)(and (Flag46-)))

(:derived (Flag191-)(and (Flag188-)))

(:derived (Flag191-)(and (Flag98-)))

(:derived (Flag191-)(and (Flag60-)))

(:derived (Flag191-)(and (Flag119-)))

(:derived (Flag191-)(and (Flag127-)))

(:derived (Flag191-)(and (Flag53-)))

(:derived (Flag191-)(and (Flag147-)))

(:derived (Flag191-)(and (Flag55-)))

(:derived (Flag191-)(and (Flag177-)))

(:derived (Flag191-)(and (Flag81-)))

(:derived (Flag191-)(and (Flag161-)))

(:derived (Flag191-)(and (Flag129-)))

(:derived (Flag191-)(and (Flag162-)))

(:derived (Flag191-)(and (Flag65-)))

(:derived (Flag191-)(and (Flag189-)))

(:derived (Flag191-)(and (Flag105-)))

(:derived (Flag191-)(and (Flag113-)))

(:derived (Flag191-)(and (Flag91-)))

(:derived (Flag191-)(and (Flag107-)))

(:derived (Flag191-)(and (Flag68-)))

(:derived (Flag191-)(and (Flag163-)))

(:derived (Flag191-)(and (Flag190-)))

(:derived (Flag202-)(and (Flag41-)))

(:derived (Flag202-)(and (Flag47-)))

(:derived (Flag202-)(and (Flag192-)))

(:derived (Flag202-)(and (Flag179-)))

(:derived (Flag202-)(and (Flag134-)))

(:derived (Flag202-)(and (Flag150-)))

(:derived (Flag202-)(and (Flag135-)))

(:derived (Flag202-)(and (Flag165-)))

(:derived (Flag202-)(and (Flag126-)))

(:derived (Flag202-)(and (Flag84-)))

(:derived (Flag202-)(and (Flag127-)))

(:derived (Flag202-)(and (Flag63-)))

(:derived (Flag202-)(and (Flag181-)))

(:derived (Flag202-)(and (Flag87-)))

(:derived (Flag202-)(and (Flag43-)))

(:derived (Flag202-)(and (Flag151-)))

(:derived (Flag202-)(and (Flag92-)))

(:derived (Flag202-)(and (Flag118-)))

(:derived (Flag202-)(and (Flag137-)))

(:derived (Flag202-)(and (Flag42-)))

(:derived (Flag202-)(and (Flag97-)))

(:derived (Flag202-)(and (Flag48-)))

(:derived (Flag202-)(and (Flag167-)))

(:derived (Flag202-)(and (Flag122-)))

(:derived (Flag202-)(and (Flag153-)))

(:derived (Flag202-)(and (Flag182-)))

(:derived (Flag202-)(and (Flag139-)))

(:derived (Flag202-)(and (Flag102-)))

(:derived (Flag202-)(and (Flag57-)))

(:derived (Flag202-)(and (Flag166-)))

(:derived (Flag202-)(and (Flag123-)))

(:derived (Flag202-)(and (Flag140-)))

(:derived (Flag202-)(and (Flag61-)))

(:derived (Flag202-)(and (Flag183-)))

(:derived (Flag202-)(and (Flag154-)))

(:derived (Flag202-)(and (Flag128-)))

(:derived (Flag202-)(and (Flag184-)))

(:derived (Flag202-)(and (Flag169-)))

(:derived (Flag202-)(and (Flag90-)))

(:derived (Flag202-)(and (Flag74-)))

(:derived (Flag202-)(and (Flag193-)))

(:derived (Flag202-)(and (Flag156-)))

(:derived (Flag202-)(and (Flag141-)))

(:derived (Flag202-)(and (Flag194-)))

(:derived (Flag202-)(and (Flag111-)))

(:derived (Flag202-)(and (Flag73-)))

(:derived (Flag202-)(and (Flag143-)))

(:derived (Flag202-)(and (Flag93-)))

(:derived (Flag202-)(and (Flag157-)))

(:derived (Flag202-)(and (Flag115-)))

(:derived (Flag202-)(and (Flag116-)))

(:derived (Flag202-)(and (Flag44-)))

(:derived (Flag202-)(and (Flag195-)))

(:derived (Flag202-)(and (Flag79-)))

(:derived (Flag202-)(and (Flag185-)))

(:derived (Flag202-)(and (Flag89-)))

(:derived (Flag202-)(and (Flag170-)))

(:derived (Flag202-)(and (Flag196-)))

(:derived (Flag202-)(and (Flag172-)))

(:derived (Flag202-)(and (Flag108-)))

(:derived (Flag202-)(and (Flag173-)))

(:derived (Flag202-)(and (Flag130-)))

(:derived (Flag202-)(and (Flag160-)))

(:derived (Flag202-)(and (Flag101-)))

(:derived (Flag202-)(and (Flag83-)))

(:derived (Flag202-)(and (Flag186-)))

(:derived (Flag202-)(and (Flag174-)))

(:derived (Flag202-)(and (Flag197-)))

(:derived (Flag202-)(and (Flag66-)))

(:derived (Flag202-)(and (Flag106-)))

(:derived (Flag202-)(and (Flag187-)))

(:derived (Flag202-)(and (Flag67-)))

(:derived (Flag202-)(and (Flag144-)))

(:derived (Flag202-)(and (Flag175-)))

(:derived (Flag202-)(and (Flag121-)))

(:derived (Flag202-)(and (Flag145-)))

(:derived (Flag202-)(and (Flag176-)))

(:derived (Flag202-)(and (Flag132-)))

(:derived (Flag202-)(and (Flag40-)))

(:derived (Flag202-)(and (Flag96-)))

(:derived (Flag202-)(and (Flag158-)))

(:derived (Flag202-)(and (Flag146-)))

(:derived (Flag202-)(and (Flag46-)))

(:derived (Flag202-)(and (Flag198-)))

(:derived (Flag202-)(and (Flag199-)))

(:derived (Flag202-)(and (Flag188-)))

(:derived (Flag202-)(and (Flag98-)))

(:derived (Flag202-)(and (Flag60-)))

(:derived (Flag202-)(and (Flag119-)))

(:derived (Flag202-)(and (Flag53-)))

(:derived (Flag202-)(and (Flag55-)))

(:derived (Flag202-)(and (Flag81-)))

(:derived (Flag202-)(and (Flag161-)))

(:derived (Flag202-)(and (Flag162-)))

(:derived (Flag202-)(and (Flag200-)))

(:derived (Flag202-)(and (Flag58-)))

(:derived (Flag202-)(and (Flag189-)))

(:derived (Flag202-)(and (Flag105-)))

(:derived (Flag202-)(and (Flag113-)))

(:derived (Flag202-)(and (Flag91-)))

(:derived (Flag202-)(and (Flag107-)))

(:derived (Flag202-)(and (Flag68-)))

(:derived (Flag202-)(and (Flag201-)))

(:derived (Flag202-)(and (Flag163-)))

(:derived (Flag202-)(and (Flag190-)))

(:derived (Flag213-)(and (Flag41-)))

(:derived (Flag213-)(and (Flag47-)))

(:derived (Flag213-)(and (Flag203-)))

(:derived (Flag213-)(and (Flag192-)))

(:derived (Flag213-)(and (Flag179-)))

(:derived (Flag213-)(and (Flag204-)))

(:derived (Flag213-)(and (Flag134-)))

(:derived (Flag213-)(and (Flag150-)))

(:derived (Flag213-)(and (Flag135-)))

(:derived (Flag213-)(and (Flag165-)))

(:derived (Flag213-)(and (Flag126-)))

(:derived (Flag213-)(and (Flag84-)))

(:derived (Flag213-)(and (Flag127-)))

(:derived (Flag213-)(and (Flag63-)))

(:derived (Flag213-)(and (Flag205-)))

(:derived (Flag213-)(and (Flag181-)))

(:derived (Flag213-)(and (Flag87-)))

(:derived (Flag213-)(and (Flag92-)))

(:derived (Flag213-)(and (Flag137-)))

(:derived (Flag213-)(and (Flag42-)))

(:derived (Flag213-)(and (Flag97-)))

(:derived (Flag213-)(and (Flag48-)))

(:derived (Flag213-)(and (Flag167-)))

(:derived (Flag213-)(and (Flag122-)))

(:derived (Flag213-)(and (Flag153-)))

(:derived (Flag213-)(and (Flag139-)))

(:derived (Flag213-)(and (Flag102-)))

(:derived (Flag213-)(and (Flag89-)))

(:derived (Flag213-)(and (Flag57-)))

(:derived (Flag213-)(and (Flag166-)))

(:derived (Flag213-)(and (Flag123-)))

(:derived (Flag213-)(and (Flag206-)))

(:derived (Flag213-)(and (Flag140-)))

(:derived (Flag213-)(and (Flag61-)))

(:derived (Flag213-)(and (Flag183-)))

(:derived (Flag213-)(and (Flag154-)))

(:derived (Flag213-)(and (Flag128-)))

(:derived (Flag213-)(and (Flag184-)))

(:derived (Flag213-)(and (Flag207-)))

(:derived (Flag213-)(and (Flag169-)))

(:derived (Flag213-)(and (Flag90-)))

(:derived (Flag213-)(and (Flag74-)))

(:derived (Flag213-)(and (Flag162-)))

(:derived (Flag213-)(and (Flag156-)))

(:derived (Flag213-)(and (Flag141-)))

(:derived (Flag213-)(and (Flag194-)))

(:derived (Flag213-)(and (Flag121-)))

(:derived (Flag213-)(and (Flag111-)))

(:derived (Flag213-)(and (Flag73-)))

(:derived (Flag213-)(and (Flag143-)))

(:derived (Flag213-)(and (Flag157-)))

(:derived (Flag213-)(and (Flag208-)))

(:derived (Flag213-)(and (Flag116-)))

(:derived (Flag213-)(and (Flag195-)))

(:derived (Flag213-)(and (Flag79-)))

(:derived (Flag213-)(and (Flag185-)))

(:derived (Flag213-)(and (Flag118-)))

(:derived (Flag213-)(and (Flag196-)))

(:derived (Flag213-)(and (Flag172-)))

(:derived (Flag213-)(and (Flag108-)))

(:derived (Flag213-)(and (Flag173-)))

(:derived (Flag213-)(and (Flag130-)))

(:derived (Flag213-)(and (Flag160-)))

(:derived (Flag213-)(and (Flag101-)))

(:derived (Flag213-)(and (Flag83-)))

(:derived (Flag213-)(and (Flag186-)))

(:derived (Flag213-)(and (Flag174-)))

(:derived (Flag213-)(and (Flag66-)))

(:derived (Flag213-)(and (Flag158-)))

(:derived (Flag213-)(and (Flag187-)))

(:derived (Flag213-)(and (Flag67-)))

(:derived (Flag213-)(and (Flag144-)))

(:derived (Flag213-)(and (Flag175-)))

(:derived (Flag213-)(and (Flag209-)))

(:derived (Flag213-)(and (Flag176-)))

(:derived (Flag213-)(and (Flag132-)))

(:derived (Flag213-)(and (Flag40-)))

(:derived (Flag213-)(and (Flag96-)))

(:derived (Flag213-)(and (Flag146-)))

(:derived (Flag213-)(and (Flag46-)))

(:derived (Flag213-)(and (Flag198-)))

(:derived (Flag213-)(and (Flag199-)))

(:derived (Flag213-)(and (Flag188-)))

(:derived (Flag213-)(and (Flag98-)))

(:derived (Flag213-)(and (Flag60-)))

(:derived (Flag213-)(and (Flag119-)))

(:derived (Flag213-)(and (Flag210-)))

(:derived (Flag213-)(and (Flag53-)))

(:derived (Flag213-)(and (Flag55-)))

(:derived (Flag213-)(and (Flag81-)))

(:derived (Flag213-)(and (Flag161-)))

(:derived (Flag213-)(and (Flag193-)))

(:derived (Flag213-)(and (Flag200-)))

(:derived (Flag213-)(and (Flag58-)))

(:derived (Flag213-)(and (Flag189-)))

(:derived (Flag213-)(and (Flag105-)))

(:derived (Flag213-)(and (Flag113-)))

(:derived (Flag213-)(and (Flag91-)))

(:derived (Flag213-)(and (Flag68-)))

(:derived (Flag213-)(and (Flag201-)))

(:derived (Flag213-)(and (Flag163-)))

(:derived (Flag213-)(and (Flag211-)))

(:derived (Flag213-)(and (Flag212-)))

(:derived (Flag213-)(and (Flag190-)))

(:derived (Flag222-)(and (Flag41-)))

(:derived (Flag222-)(and (Flag47-)))

(:derived (Flag222-)(and (Flag203-)))

(:derived (Flag222-)(and (Flag192-)))

(:derived (Flag222-)(and (Flag179-)))

(:derived (Flag222-)(and (Flag204-)))

(:derived (Flag222-)(and (Flag134-)))

(:derived (Flag222-)(and (Flag150-)))

(:derived (Flag222-)(and (Flag135-)))

(:derived (Flag222-)(and (Flag214-)))

(:derived (Flag222-)(and (Flag215-)))

(:derived (Flag222-)(and (Flag119-)))

(:derived (Flag222-)(and (Flag195-)))

(:derived (Flag222-)(and (Flag63-)))

(:derived (Flag222-)(and (Flag205-)))

(:derived (Flag222-)(and (Flag181-)))

(:derived (Flag222-)(and (Flag166-)))

(:derived (Flag222-)(and (Flag92-)))

(:derived (Flag222-)(and (Flag137-)))

(:derived (Flag222-)(and (Flag216-)))

(:derived (Flag222-)(and (Flag42-)))

(:derived (Flag222-)(and (Flag97-)))

(:derived (Flag222-)(and (Flag167-)))

(:derived (Flag222-)(and (Flag122-)))

(:derived (Flag222-)(and (Flag153-)))

(:derived (Flag222-)(and (Flag139-)))

(:derived (Flag222-)(and (Flag102-)))

(:derived (Flag222-)(and (Flag89-)))

(:derived (Flag222-)(and (Flag57-)))

(:derived (Flag222-)(and (Flag87-)))

(:derived (Flag222-)(and (Flag123-)))

(:derived (Flag222-)(and (Flag206-)))

(:derived (Flag222-)(and (Flag140-)))

(:derived (Flag222-)(and (Flag61-)))

(:derived (Flag222-)(and (Flag183-)))

(:derived (Flag222-)(and (Flag154-)))

(:derived (Flag222-)(and (Flag128-)))

(:derived (Flag222-)(and (Flag184-)))

(:derived (Flag222-)(and (Flag207-)))

(:derived (Flag222-)(and (Flag169-)))

(:derived (Flag222-)(and (Flag90-)))

(:derived (Flag222-)(and (Flag74-)))

(:derived (Flag222-)(and (Flag162-)))

(:derived (Flag222-)(and (Flag156-)))

(:derived (Flag222-)(and (Flag194-)))

(:derived (Flag222-)(and (Flag121-)))

(:derived (Flag222-)(and (Flag111-)))

(:derived (Flag222-)(and (Flag73-)))

(:derived (Flag222-)(and (Flag143-)))

(:derived (Flag222-)(and (Flag157-)))

(:derived (Flag222-)(and (Flag208-)))

(:derived (Flag222-)(and (Flag116-)))

(:derived (Flag222-)(and (Flag158-)))

(:derived (Flag222-)(and (Flag79-)))

(:derived (Flag222-)(and (Flag185-)))

(:derived (Flag222-)(and (Flag217-)))

(:derived (Flag222-)(and (Flag118-)))

(:derived (Flag222-)(and (Flag196-)))

(:derived (Flag222-)(and (Flag172-)))

(:derived (Flag222-)(and (Flag218-)))

(:derived (Flag222-)(and (Flag108-)))

(:derived (Flag222-)(and (Flag173-)))

(:derived (Flag222-)(and (Flag130-)))

(:derived (Flag222-)(and (Flag160-)))

(:derived (Flag222-)(and (Flag101-)))

(:derived (Flag222-)(and (Flag83-)))

(:derived (Flag222-)(and (Flag219-)))

(:derived (Flag222-)(and (Flag174-)))

(:derived (Flag222-)(and (Flag66-)))

(:derived (Flag222-)(and (Flag187-)))

(:derived (Flag222-)(and (Flag67-)))

(:derived (Flag222-)(and (Flag144-)))

(:derived (Flag222-)(and (Flag175-)))

(:derived (Flag222-)(and (Flag209-)))

(:derived (Flag222-)(and (Flag176-)))

(:derived (Flag222-)(and (Flag132-)))

(:derived (Flag222-)(and (Flag40-)))

(:derived (Flag222-)(and (Flag96-)))

(:derived (Flag222-)(and (Flag146-)))

(:derived (Flag222-)(and (Flag46-)))

(:derived (Flag222-)(and (Flag198-)))

(:derived (Flag222-)(and (Flag188-)))

(:derived (Flag222-)(and (Flag98-)))

(:derived (Flag222-)(and (Flag210-)))

(:derived (Flag222-)(and (Flag53-)))

(:derived (Flag222-)(and (Flag55-)))

(:derived (Flag222-)(and (Flag220-)))

(:derived (Flag222-)(and (Flag81-)))

(:derived (Flag222-)(and (Flag161-)))

(:derived (Flag222-)(and (Flag193-)))

(:derived (Flag222-)(and (Flag200-)))

(:derived (Flag222-)(and (Flag58-)))

(:derived (Flag222-)(and (Flag189-)))

(:derived (Flag222-)(and (Flag105-)))

(:derived (Flag222-)(and (Flag113-)))

(:derived (Flag222-)(and (Flag91-)))

(:derived (Flag222-)(and (Flag221-)))

(:derived (Flag222-)(and (Flag68-)))

(:derived (Flag222-)(and (Flag201-)))

(:derived (Flag222-)(and (Flag211-)))

(:derived (Flag222-)(and (Flag190-)))

(:derived (Flag231-)(and (Flag41-)))

(:derived (Flag231-)(and (Flag47-)))

(:derived (Flag231-)(and (Flag203-)))

(:derived (Flag231-)(and (Flag192-)))

(:derived (Flag231-)(and (Flag223-)))

(:derived (Flag231-)(and (Flag204-)))

(:derived (Flag231-)(and (Flag134-)))

(:derived (Flag231-)(and (Flag150-)))

(:derived (Flag231-)(and (Flag214-)))

(:derived (Flag231-)(and (Flag119-)))

(:derived (Flag231-)(and (Flag63-)))

(:derived (Flag231-)(and (Flag181-)))

(:derived (Flag231-)(and (Flag87-)))

(:derived (Flag231-)(and (Flag193-)))

(:derived (Flag231-)(and (Flag92-)))

(:derived (Flag231-)(and (Flag224-)))

(:derived (Flag231-)(and (Flag137-)))

(:derived (Flag231-)(and (Flag216-)))

(:derived (Flag231-)(and (Flag42-)))

(:derived (Flag231-)(and (Flag97-)))

(:derived (Flag231-)(and (Flag225-)))

(:derived (Flag231-)(and (Flag167-)))

(:derived (Flag231-)(and (Flag122-)))

(:derived (Flag231-)(and (Flag153-)))

(:derived (Flag231-)(and (Flag102-)))

(:derived (Flag231-)(and (Flag57-)))

(:derived (Flag231-)(and (Flag166-)))

(:derived (Flag231-)(and (Flag123-)))

(:derived (Flag231-)(and (Flag206-)))

(:derived (Flag231-)(and (Flag140-)))

(:derived (Flag231-)(and (Flag61-)))

(:derived (Flag231-)(and (Flag183-)))

(:derived (Flag231-)(and (Flag154-)))

(:derived (Flag231-)(and (Flag128-)))

(:derived (Flag231-)(and (Flag207-)))

(:derived (Flag231-)(and (Flag169-)))

(:derived (Flag231-)(and (Flag179-)))

(:derived (Flag231-)(and (Flag90-)))

(:derived (Flag231-)(and (Flag101-)))

(:derived (Flag231-)(and (Flag226-)))

(:derived (Flag231-)(and (Flag156-)))

(:derived (Flag231-)(and (Flag194-)))

(:derived (Flag231-)(and (Flag111-)))

(:derived (Flag231-)(and (Flag73-)))

(:derived (Flag231-)(and (Flag143-)))

(:derived (Flag231-)(and (Flag132-)))

(:derived (Flag231-)(and (Flag157-)))

(:derived (Flag231-)(and (Flag208-)))

(:derived (Flag231-)(and (Flag116-)))

(:derived (Flag231-)(and (Flag195-)))

(:derived (Flag231-)(and (Flag79-)))

(:derived (Flag231-)(and (Flag185-)))

(:derived (Flag231-)(and (Flag118-)))

(:derived (Flag231-)(and (Flag196-)))

(:derived (Flag231-)(and (Flag172-)))

(:derived (Flag231-)(and (Flag218-)))

(:derived (Flag231-)(and (Flag108-)))

(:derived (Flag231-)(and (Flag173-)))

(:derived (Flag231-)(and (Flag130-)))

(:derived (Flag231-)(and (Flag160-)))

(:derived (Flag231-)(and (Flag83-)))

(:derived (Flag231-)(and (Flag219-)))

(:derived (Flag231-)(and (Flag174-)))

(:derived (Flag231-)(and (Flag66-)))

(:derived (Flag231-)(and (Flag187-)))

(:derived (Flag231-)(and (Flag67-)))

(:derived (Flag231-)(and (Flag144-)))

(:derived (Flag231-)(and (Flag227-)))

(:derived (Flag231-)(and (Flag209-)))

(:derived (Flag231-)(and (Flag176-)))

(:derived (Flag231-)(and (Flag228-)))

(:derived (Flag231-)(and (Flag96-)))

(:derived (Flag231-)(and (Flag146-)))

(:derived (Flag231-)(and (Flag46-)))

(:derived (Flag231-)(and (Flag188-)))

(:derived (Flag231-)(and (Flag98-)))

(:derived (Flag231-)(and (Flag210-)))

(:derived (Flag231-)(and (Flag53-)))

(:derived (Flag231-)(and (Flag220-)))

(:derived (Flag231-)(and (Flag81-)))

(:derived (Flag231-)(and (Flag229-)))

(:derived (Flag231-)(and (Flag161-)))

(:derived (Flag231-)(and (Flag162-)))

(:derived (Flag231-)(and (Flag200-)))

(:derived (Flag231-)(and (Flag58-)))

(:derived (Flag231-)(and (Flag189-)))

(:derived (Flag231-)(and (Flag105-)))

(:derived (Flag231-)(and (Flag74-)))

(:derived (Flag231-)(and (Flag91-)))

(:derived (Flag231-)(and (Flag221-)))

(:derived (Flag231-)(and (Flag40-)))

(:derived (Flag231-)(and (Flag201-)))

(:derived (Flag231-)(and (Flag211-)))

(:derived (Flag231-)(and (Flag230-)))

(:derived (Flag231-)(and (Flag190-)))

(:derived (Flag238-)(and (Flag41-)))

(:derived (Flag238-)(and (Flag47-)))

(:derived (Flag238-)(and (Flag232-)))

(:derived (Flag238-)(and (Flag203-)))

(:derived (Flag238-)(and (Flag223-)))

(:derived (Flag238-)(and (Flag204-)))

(:derived (Flag238-)(and (Flag134-)))

(:derived (Flag238-)(and (Flag150-)))

(:derived (Flag238-)(and (Flag214-)))

(:derived (Flag238-)(and (Flag119-)))

(:derived (Flag238-)(and (Flag166-)))

(:derived (Flag238-)(and (Flag92-)))

(:derived (Flag238-)(and (Flag224-)))

(:derived (Flag238-)(and (Flag137-)))

(:derived (Flag238-)(and (Flag216-)))

(:derived (Flag238-)(and (Flag42-)))

(:derived (Flag238-)(and (Flag97-)))

(:derived (Flag238-)(and (Flag225-)))

(:derived (Flag238-)(and (Flag167-)))

(:derived (Flag238-)(and (Flag122-)))

(:derived (Flag238-)(and (Flag153-)))

(:derived (Flag238-)(and (Flag102-)))

(:derived (Flag238-)(and (Flag57-)))

(:derived (Flag238-)(and (Flag87-)))

(:derived (Flag238-)(and (Flag123-)))

(:derived (Flag238-)(and (Flag206-)))

(:derived (Flag238-)(and (Flag140-)))

(:derived (Flag238-)(and (Flag233-)))

(:derived (Flag238-)(and (Flag183-)))

(:derived (Flag238-)(and (Flag179-)))

(:derived (Flag238-)(and (Flag207-)))

(:derived (Flag238-)(and (Flag169-)))

(:derived (Flag238-)(and (Flag90-)))

(:derived (Flag238-)(and (Flag101-)))

(:derived (Flag238-)(and (Flag193-)))

(:derived (Flag238-)(and (Flag194-)))

(:derived (Flag238-)(and (Flag234-)))

(:derived (Flag238-)(and (Flag111-)))

(:derived (Flag238-)(and (Flag73-)))

(:derived (Flag238-)(and (Flag143-)))

(:derived (Flag238-)(and (Flag132-)))

(:derived (Flag238-)(and (Flag157-)))

(:derived (Flag238-)(and (Flag208-)))

(:derived (Flag238-)(and (Flag116-)))

(:derived (Flag238-)(and (Flag195-)))

(:derived (Flag238-)(and (Flag79-)))

(:derived (Flag238-)(and (Flag185-)))

(:derived (Flag238-)(and (Flag118-)))

(:derived (Flag238-)(and (Flag196-)))

(:derived (Flag238-)(and (Flag172-)))

(:derived (Flag238-)(and (Flag218-)))

(:derived (Flag238-)(and (Flag108-)))

(:derived (Flag238-)(and (Flag173-)))

(:derived (Flag238-)(and (Flag130-)))

(:derived (Flag238-)(and (Flag160-)))

(:derived (Flag238-)(and (Flag219-)))

(:derived (Flag238-)(and (Flag235-)))

(:derived (Flag238-)(and (Flag236-)))

(:derived (Flag238-)(and (Flag66-)))

(:derived (Flag238-)(and (Flag187-)))

(:derived (Flag238-)(and (Flag237-)))

(:derived (Flag238-)(and (Flag67-)))

(:derived (Flag238-)(and (Flag144-)))

(:derived (Flag238-)(and (Flag176-)))

(:derived (Flag238-)(and (Flag228-)))

(:derived (Flag238-)(and (Flag96-)))

(:derived (Flag238-)(and (Flag146-)))

(:derived (Flag238-)(and (Flag46-)))

(:derived (Flag238-)(and (Flag188-)))

(:derived (Flag238-)(and (Flag98-)))

(:derived (Flag238-)(and (Flag210-)))

(:derived (Flag238-)(and (Flag226-)))

(:derived (Flag238-)(and (Flag53-)))

(:derived (Flag238-)(and (Flag220-)))

(:derived (Flag238-)(and (Flag81-)))

(:derived (Flag238-)(and (Flag161-)))

(:derived (Flag238-)(and (Flag162-)))

(:derived (Flag238-)(and (Flag200-)))

(:derived (Flag238-)(and (Flag58-)))

(:derived (Flag238-)(and (Flag189-)))

(:derived (Flag238-)(and (Flag74-)))

(:derived (Flag238-)(and (Flag221-)))

(:derived (Flag238-)(and (Flag40-)))

(:derived (Flag238-)(and (Flag201-)))

(:derived (Flag238-)(and (Flag211-)))

(:derived (Flag238-)(and (Flag230-)))

(:derived (Flag238-)(and (Flag190-)))

(:derived (Flag245-)(and (Flag208-)))

(:derived (Flag245-)(and (Flag96-)))

(:derived (Flag245-)(and (Flag116-)))

(:derived (Flag245-)(and (Flag41-)))

(:derived (Flag245-)(and (Flag132-)))

(:derived (Flag245-)(and (Flag146-)))

(:derived (Flag245-)(and (Flag47-)))

(:derived (Flag245-)(and (Flag232-)))

(:derived (Flag245-)(and (Flag46-)))

(:derived (Flag245-)(and (Flag73-)))

(:derived (Flag245-)(and (Flag203-)))

(:derived (Flag245-)(and (Flag185-)))

(:derived (Flag245-)(and (Flag188-)))

(:derived (Flag245-)(and (Flag98-)))

(:derived (Flag245-)(and (Flag172-)))

(:derived (Flag245-)(and (Flag239-)))

(:derived (Flag245-)(and (Flag40-)))

(:derived (Flag245-)(and (Flag223-)))

(:derived (Flag245-)(and (Flag225-)))

(:derived (Flag245-)(and (Flag240-)))

(:derived (Flag245-)(and (Flag204-)))

(:derived (Flag245-)(and (Flag119-)))

(:derived (Flag245-)(and (Flag218-)))

(:derived (Flag245-)(and (Flag210-)))

(:derived (Flag245-)(and (Flag108-)))

(:derived (Flag245-)(and (Flag130-)))

(:derived (Flag245-)(and (Flag134-)))

(:derived (Flag245-)(and (Flag160-)))

(:derived (Flag245-)(and (Flag53-)))

(:derived (Flag245-)(and (Flag226-)))

(:derived (Flag245-)(and (Flag157-)))

(:derived (Flag245-)(and (Flag102-)))

(:derived (Flag245-)(and (Flag57-)))

(:derived (Flag245-)(and (Flag166-)))

(:derived (Flag245-)(and (Flag219-)))

(:derived (Flag245-)(and (Flag214-)))

(:derived (Flag245-)(and (Flag206-)))

(:derived (Flag245-)(and (Flag140-)))

(:derived (Flag245-)(and (Flag241-)))

(:derived (Flag245-)(and (Flag220-)))

(:derived (Flag245-)(and (Flag81-)))

(:derived (Flag245-)(and (Flag183-)))

(:derived (Flag245-)(and (Flag242-)))

(:derived (Flag245-)(and (Flag123-)))

(:derived (Flag245-)(and (Flag161-)))

(:derived (Flag245-)(and (Flag193-)))

(:derived (Flag245-)(and (Flag235-)))

(:derived (Flag245-)(and (Flag74-)))

(:derived (Flag245-)(and (Flag236-)))

(:derived (Flag245-)(and (Flag87-)))

(:derived (Flag245-)(and (Flag58-)))

(:derived (Flag245-)(and (Flag66-)))

(:derived (Flag245-)(and (Flag189-)))

(:derived (Flag245-)(and (Flag143-)))

(:derived (Flag245-)(and (Flag90-)))

(:derived (Flag245-)(and (Flag101-)))

(:derived (Flag245-)(and (Flag162-)))

(:derived (Flag245-)(and (Flag243-)))

(:derived (Flag245-)(and (Flag79-)))

(:derived (Flag245-)(and (Flag92-)))

(:derived (Flag245-)(and (Flag200-)))

(:derived (Flag245-)(and (Flag187-)))

(:derived (Flag245-)(and (Flag194-)))

(:derived (Flag245-)(and (Flag237-)))

(:derived (Flag245-)(and (Flag234-)))

(:derived (Flag245-)(and (Flag201-)))

(:derived (Flag245-)(and (Flag211-)))

(:derived (Flag245-)(and (Flag230-)))

(:derived (Flag245-)(and (Flag169-)))

(:derived (Flag245-)(and (Flag153-)))

(:derived (Flag245-)(and (Flag176-)))

(:derived (Flag245-)(and (Flag190-)))

(:derived (Flag245-)(and (Flag244-)))

(:derived (Flag245-)(and (Flag118-)))

(:derived (Flag245-)(and (Flag137-)))

(:derived (Flag245-)(and (Flag216-)))

(:derived (Flag245-)(and (Flag228-)))

(:derived (Flag251-)(and (Flag40-)))

(:derived (Flag251-)(and (Flag96-)))

(:derived (Flag251-)(and (Flag116-)))

(:derived (Flag251-)(and (Flag132-)))

(:derived (Flag251-)(and (Flag146-)))

(:derived (Flag251-)(and (Flag47-)))

(:derived (Flag251-)(and (Flag46-)))

(:derived (Flag251-)(and (Flag246-)))

(:derived (Flag251-)(and (Flag203-)))

(:derived (Flag251-)(and (Flag185-)))

(:derived (Flag251-)(and (Flag247-)))

(:derived (Flag251-)(and (Flag118-)))

(:derived (Flag251-)(and (Flag172-)))

(:derived (Flag251-)(and (Flag239-)))

(:derived (Flag251-)(and (Flag248-)))

(:derived (Flag251-)(and (Flag225-)))

(:derived (Flag251-)(and (Flag220-)))

(:derived (Flag251-)(and (Flag204-)))

(:derived (Flag251-)(and (Flag119-)))

(:derived (Flag251-)(and (Flag218-)))

(:derived (Flag251-)(and (Flag210-)))

(:derived (Flag251-)(and (Flag108-)))

(:derived (Flag251-)(and (Flag134-)))

(:derived (Flag251-)(and (Flag160-)))

(:derived (Flag251-)(and (Flag53-)))

(:derived (Flag251-)(and (Flag157-)))

(:derived (Flag251-)(and (Flag57-)))

(:derived (Flag251-)(and (Flag87-)))

(:derived (Flag251-)(and (Flag219-)))

(:derived (Flag251-)(and (Flag41-)))

(:derived (Flag251-)(and (Flag140-)))

(:derived (Flag251-)(and (Flag241-)))

(:derived (Flag251-)(and (Flag216-)))

(:derived (Flag251-)(and (Flag81-)))

(:derived (Flag251-)(and (Flag242-)))

(:derived (Flag251-)(and (Flag123-)))

(:derived (Flag251-)(and (Flag161-)))

(:derived (Flag251-)(and (Flag193-)))

(:derived (Flag251-)(and (Flag235-)))

(:derived (Flag251-)(and (Flag74-)))

(:derived (Flag251-)(and (Flag236-)))

(:derived (Flag251-)(and (Flag166-)))

(:derived (Flag251-)(and (Flag92-)))

(:derived (Flag251-)(and (Flag66-)))

(:derived (Flag251-)(and (Flag189-)))

(:derived (Flag251-)(and (Flag240-)))

(:derived (Flag251-)(and (Flag90-)))

(:derived (Flag251-)(and (Flag101-)))

(:derived (Flag251-)(and (Flag249-)))

(:derived (Flag251-)(and (Flag200-)))

(:derived (Flag251-)(and (Flag187-)))

(:derived (Flag251-)(and (Flag194-)))

(:derived (Flag251-)(and (Flag237-)))

(:derived (Flag251-)(and (Flag234-)))

(:derived (Flag251-)(and (Flag201-)))

(:derived (Flag251-)(and (Flag223-)))

(:derived (Flag251-)(and (Flag211-)))

(:derived (Flag251-)(and (Flag188-)))

(:derived (Flag251-)(and (Flag230-)))

(:derived (Flag251-)(and (Flag169-)))

(:derived (Flag251-)(and (Flag153-)))

(:derived (Flag251-)(and (Flag176-)))

(:derived (Flag251-)(and (Flag143-)))

(:derived (Flag251-)(and (Flag98-)))

(:derived (Flag251-)(and (Flag250-)))

(:derived (Flag251-)(and (Flag228-)))

(:derived (Flag255-)(and (Flag96-)))

(:derived (Flag255-)(and (Flag41-)))

(:derived (Flag255-)(and (Flag132-)))

(:derived (Flag255-)(and (Flag146-)))

(:derived (Flag255-)(and (Flag46-)))

(:derived (Flag255-)(and (Flag246-)))

(:derived (Flag255-)(and (Flag247-)))

(:derived (Flag255-)(and (Flag118-)))

(:derived (Flag255-)(and (Flag172-)))

(:derived (Flag255-)(and (Flag239-)))

(:derived (Flag255-)(and (Flag223-)))

(:derived (Flag255-)(and (Flag240-)))

(:derived (Flag255-)(and (Flag204-)))

(:derived (Flag255-)(and (Flag119-)))

(:derived (Flag255-)(and (Flag218-)))

(:derived (Flag255-)(and (Flag210-)))

(:derived (Flag255-)(and (Flag92-)))

(:derived (Flag255-)(and (Flag108-)))

(:derived (Flag255-)(and (Flag252-)))

(:derived (Flag255-)(and (Flag160-)))

(:derived (Flag255-)(and (Flag53-)))

(:derived (Flag255-)(and (Flag57-)))

(:derived (Flag255-)(and (Flag166-)))

(:derived (Flag255-)(and (Flag219-)))

(:derived (Flag255-)(and (Flag140-)))

(:derived (Flag255-)(and (Flag241-)))

(:derived (Flag255-)(and (Flag81-)))

(:derived (Flag255-)(and (Flag242-)))

(:derived (Flag255-)(and (Flag123-)))

(:derived (Flag255-)(and (Flag161-)))

(:derived (Flag255-)(and (Flag253-)))

(:derived (Flag255-)(and (Flag193-)))

(:derived (Flag255-)(and (Flag200-)))

(:derived (Flag255-)(and (Flag236-)))

(:derived (Flag255-)(and (Flag254-)))

(:derived (Flag255-)(and (Flag66-)))

(:derived (Flag255-)(and (Flag189-)))

(:derived (Flag255-)(and (Flag74-)))

(:derived (Flag255-)(and (Flag90-)))

(:derived (Flag255-)(and (Flag249-)))

(:derived (Flag255-)(and (Flag235-)))

(:derived (Flag255-)(and (Flag187-)))

(:derived (Flag255-)(and (Flag194-)))

(:derived (Flag255-)(and (Flag234-)))

(:derived (Flag255-)(and (Flag211-)))

(:derived (Flag255-)(and (Flag188-)))

(:derived (Flag255-)(and (Flag230-)))

(:derived (Flag255-)(and (Flag153-)))

(:derived (Flag255-)(and (Flag176-)))

(:derived (Flag255-)(and (Flag143-)))

(:derived (Flag255-)(and (Flag98-)))

(:derived (Flag255-)(and (Flag216-)))

(:derived (Flag255-)(and (Flag228-)))

(:derived (Flag258-)(and (Flag96-)))

(:derived (Flag258-)(and (Flag41-)))

(:derived (Flag258-)(and (Flag132-)))

(:derived (Flag258-)(and (Flag46-)))

(:derived (Flag258-)(and (Flag246-)))

(:derived (Flag258-)(and (Flag247-)))

(:derived (Flag258-)(and (Flag118-)))

(:derived (Flag258-)(and (Flag239-)))

(:derived (Flag258-)(and (Flag223-)))

(:derived (Flag258-)(and (Flag153-)))

(:derived (Flag258-)(and (Flag210-)))

(:derived (Flag258-)(and (Flag256-)))

(:derived (Flag258-)(and (Flag172-)))

(:derived (Flag258-)(and (Flag252-)))

(:derived (Flag258-)(and (Flag160-)))

(:derived (Flag258-)(and (Flag57-)))

(:derived (Flag258-)(and (Flag219-)))

(:derived (Flag258-)(and (Flag140-)))

(:derived (Flag258-)(and (Flag241-)))

(:derived (Flag258-)(and (Flag119-)))

(:derived (Flag258-)(and (Flag193-)))

(:derived (Flag258-)(and (Flag236-)))

(:derived (Flag258-)(and (Flag254-)))

(:derived (Flag258-)(and (Flag66-)))

(:derived (Flag258-)(and (Flag189-)))

(:derived (Flag258-)(and (Flag240-)))

(:derived (Flag258-)(and (Flag90-)))

(:derived (Flag258-)(and (Flag92-)))

(:derived (Flag258-)(and (Flag187-)))

(:derived (Flag258-)(and (Flag194-)))

(:derived (Flag258-)(and (Flag234-)))

(:derived (Flag258-)(and (Flag211-)))

(:derived (Flag258-)(and (Flag257-)))

(:derived (Flag258-)(and (Flag176-)))

(:derived (Flag258-)(and (Flag143-)))

(:derived (Flag258-)(and (Flag98-)))

(:derived (Flag258-)(and (Flag216-)))

(:derived (Flag258-)(and (Flag228-)))

(:derived (Flag260-)(and (Flag187-)))

(:derived (Flag260-)(and (Flag153-)))

(:derived (Flag260-)(and (Flag210-)))

(:derived (Flag260-)(and (Flag256-)))

(:derived (Flag260-)(and (Flag172-)))

(:derived (Flag260-)(and (Flag46-)))

(:derived (Flag260-)(and (Flag41-)))

(:derived (Flag260-)(and (Flag259-)))

(:derived (Flag260-)(and (Flag193-)))

(:derived (Flag260-)(and (Flag119-)))

(:derived (Flag260-)(and (Flag247-)))

(:derived (Flag260-)(and (Flag118-)))

(:derived (Flag260-)(and (Flag254-)))

(:derived (Flag260-)(and (Flag143-)))

(:derived (Flag260-)(and (Flag240-)))

(:derived (Flag260-)(and (Flag239-)))

(:derived (Flag260-)(and (Flag90-)))

(:derived (Flag260-)(and (Flag216-)))

(:derived (Flag260-)(and (Flag228-)))

(:derived (Flag263-)(and (Flag59-)))

(:derived (Flag263-)(and (Flag47-)))

(:derived (Flag263-)(and (Flag62-)))

(:derived (Flag263-)(and (Flag44-)))

(:derived (Flag263-)(and (Flag67-)))

(:derived (Flag263-)(and (Flag51-)))

(:derived (Flag263-)(and (Flag45-)))

(:derived (Flag263-)(and (Flag46-)))

(:derived (Flag263-)(and (Flag66-)))

(:derived (Flag263-)(and (Flag262-)))

(:derived (Flag263-)(and (Flag64-)))

(:derived (Flag263-)(and (Flag71-)))

(:derived (Flag263-)(and (Flag55-)))

(:derived (Flag263-)(and (Flag65-)))

(:derived (Flag263-)(and (Flag72-)))

(:derived (Flag263-)(and (Flag73-)))

(:derived (Flag263-)(and (Flag48-)))

(:derived (Flag263-)(and (Flag74-)))

(:derived (Flag263-)(and (Flag61-)))

(:derived (Flag1-)(and (COLUMN2-ROBOT)(ROW1-ROBOT)(NOT-ERROR-)(NOT-CHECKCONSISTENCY-)))

(:derived (Flag3-)(and (LEFTOF3-ROBOT)))

(:derived (Flag3-)(and (RIGHTOF7-ROBOT)))

(:derived (Flag3-)(and (RIGHTOF8-ROBOT)))

(:derived (Flag3-)(and (RIGHTOF15-ROBOT)))

(:derived (Flag3-)(and (LEFTOF2-ROBOT)))

(:derived (Flag3-)(and (LEFTOF9-ROBOT)))

(:derived (Flag3-)(and (RIGHTOF0-ROBOT)))

(:derived (Flag3-)(and (LEFTOF8-ROBOT)))

(:derived (Flag3-)(and (RIGHTOF12-ROBOT)))

(:derived (Flag3-)(and (LEFTOF14-ROBOT)))

(:derived (Flag3-)(and (LEFTOF5-ROBOT)))

(:derived (Flag3-)(and (RIGHTOF1-ROBOT)))

(:derived (Flag3-)(and (LEFTOF12-ROBOT)))

(:derived (Flag3-)(and (LEFTOF19-ROBOT)))

(:derived (Flag3-)(and (RIGHTOF13-ROBOT)))

(:derived (Flag3-)(and (RIGHTOF14-ROBOT)))

(:derived (Flag3-)(and (LEFTOF1-ROBOT)))

(:derived (Flag3-)(and (RIGHTOF6-ROBOT)))

(:derived (Flag3-)(and (RIGHTOF9-ROBOT)))

(:derived (Flag3-)(and (LEFTOF6-ROBOT)))

(:derived (Flag3-)(and (RIGHTOF11-ROBOT)))

(:derived (Flag3-)(and (RIGHTOF17-ROBOT)))

(:derived (Flag3-)(and (LEFTOF11-ROBOT)))

(:derived (Flag3-)(and (RIGHTOF5-ROBOT)))

(:derived (Flag3-)(and (LEFTOF10-ROBOT)))

(:derived (Flag3-)(and (LEFTOF15-ROBOT)))

(:derived (Flag3-)(and (LEFTOF4-ROBOT)))

(:derived (Flag3-)(and (RIGHTOF18-ROBOT)))

(:derived (Flag3-)(and (LEFTOF16-ROBOT)))

(:derived (Flag3-)(and (RIGHTOF16-ROBOT)))

(:derived (Flag3-)(and (RIGHTOF4-ROBOT)))

(:derived (Flag3-)(and (RIGHTOF10-ROBOT)))

(:derived (Flag3-)(and (LEFTOF7-ROBOT)))

(:derived (Flag3-)(and (LEFTOF13-ROBOT)))

(:derived (Flag3-)(and (LEFTOF17-ROBOT)))

(:derived (Flag3-)(and (RIGHTOF3-ROBOT)))

(:derived (Flag5-)(and (LEFTOF1-ROBOT)))

(:derived (Flag5-)(and (LEFTOF2-ROBOT)))

(:derived (Flag6-)(and (LEFTOF1-ROBOT)))

(:derived (Flag6-)(and (LEFTOF3-ROBOT)))

(:derived (Flag6-)(and (LEFTOF2-ROBOT)))

(:derived (Flag7-)(and (LEFTOF1-ROBOT)))

(:derived (Flag7-)(and (LEFTOF3-ROBOT)))

(:derived (Flag7-)(and (LEFTOF4-ROBOT)))

(:derived (Flag7-)(and (LEFTOF2-ROBOT)))

(:derived (Flag8-)(and (LEFTOF5-ROBOT)))

(:derived (Flag8-)(and (LEFTOF1-ROBOT)))

(:derived (Flag8-)(and (LEFTOF3-ROBOT)))

(:derived (Flag8-)(and (LEFTOF4-ROBOT)))

(:derived (Flag8-)(and (LEFTOF2-ROBOT)))

(:derived (Flag9-)(and (LEFTOF2-ROBOT)))

(:derived (Flag9-)(and (LEFTOF1-ROBOT)))

(:derived (Flag9-)(and (LEFTOF6-ROBOT)))

(:derived (Flag9-)(and (LEFTOF5-ROBOT)))

(:derived (Flag9-)(and (LEFTOF3-ROBOT)))

(:derived (Flag9-)(and (LEFTOF4-ROBOT)))

(:derived (Flag10-)(and (LEFTOF2-ROBOT)))

(:derived (Flag10-)(and (LEFTOF7-ROBOT)))

(:derived (Flag10-)(and (LEFTOF1-ROBOT)))

(:derived (Flag10-)(and (LEFTOF6-ROBOT)))

(:derived (Flag10-)(and (LEFTOF5-ROBOT)))

(:derived (Flag10-)(and (LEFTOF3-ROBOT)))

(:derived (Flag10-)(and (LEFTOF4-ROBOT)))

(:derived (Flag11-)(and (LEFTOF2-ROBOT)))

(:derived (Flag11-)(and (LEFTOF7-ROBOT)))

(:derived (Flag11-)(and (LEFTOF1-ROBOT)))

(:derived (Flag11-)(and (LEFTOF6-ROBOT)))

(:derived (Flag11-)(and (LEFTOF5-ROBOT)))

(:derived (Flag11-)(and (LEFTOF3-ROBOT)))

(:derived (Flag11-)(and (LEFTOF4-ROBOT)))

(:derived (Flag11-)(and (LEFTOF8-ROBOT)))

(:derived (Flag12-)(and (LEFTOF9-ROBOT)))

(:derived (Flag12-)(and (LEFTOF2-ROBOT)))

(:derived (Flag12-)(and (LEFTOF7-ROBOT)))

(:derived (Flag12-)(and (LEFTOF1-ROBOT)))

(:derived (Flag12-)(and (LEFTOF6-ROBOT)))

(:derived (Flag12-)(and (LEFTOF5-ROBOT)))

(:derived (Flag12-)(and (LEFTOF3-ROBOT)))

(:derived (Flag12-)(and (LEFTOF4-ROBOT)))

(:derived (Flag12-)(and (LEFTOF8-ROBOT)))

(:derived (Flag13-)(and (LEFTOF10-ROBOT)))

(:derived (Flag13-)(and (LEFTOF9-ROBOT)))

(:derived (Flag13-)(and (LEFTOF2-ROBOT)))

(:derived (Flag13-)(and (LEFTOF7-ROBOT)))

(:derived (Flag13-)(and (LEFTOF1-ROBOT)))

(:derived (Flag13-)(and (LEFTOF6-ROBOT)))

(:derived (Flag13-)(and (LEFTOF5-ROBOT)))

(:derived (Flag13-)(and (LEFTOF3-ROBOT)))

(:derived (Flag13-)(and (LEFTOF4-ROBOT)))

(:derived (Flag13-)(and (LEFTOF8-ROBOT)))

(:derived (Flag14-)(and (LEFTOF10-ROBOT)))

(:derived (Flag14-)(and (LEFTOF11-ROBOT)))

(:derived (Flag14-)(and (LEFTOF9-ROBOT)))

(:derived (Flag14-)(and (LEFTOF2-ROBOT)))

(:derived (Flag14-)(and (LEFTOF7-ROBOT)))

(:derived (Flag14-)(and (LEFTOF1-ROBOT)))

(:derived (Flag14-)(and (LEFTOF6-ROBOT)))

(:derived (Flag14-)(and (LEFTOF5-ROBOT)))

(:derived (Flag14-)(and (LEFTOF3-ROBOT)))

(:derived (Flag14-)(and (LEFTOF4-ROBOT)))

(:derived (Flag14-)(and (LEFTOF8-ROBOT)))

(:derived (Flag15-)(and (LEFTOF10-ROBOT)))

(:derived (Flag15-)(and (LEFTOF11-ROBOT)))

(:derived (Flag15-)(and (LEFTOF9-ROBOT)))

(:derived (Flag15-)(and (LEFTOF2-ROBOT)))

(:derived (Flag15-)(and (LEFTOF7-ROBOT)))

(:derived (Flag15-)(and (LEFTOF1-ROBOT)))

(:derived (Flag15-)(and (LEFTOF12-ROBOT)))

(:derived (Flag15-)(and (LEFTOF6-ROBOT)))

(:derived (Flag15-)(and (LEFTOF5-ROBOT)))

(:derived (Flag15-)(and (LEFTOF3-ROBOT)))

(:derived (Flag15-)(and (LEFTOF4-ROBOT)))

(:derived (Flag15-)(and (LEFTOF8-ROBOT)))

(:derived (Flag16-)(and (LEFTOF10-ROBOT)))

(:derived (Flag16-)(and (LEFTOF11-ROBOT)))

(:derived (Flag16-)(and (LEFTOF9-ROBOT)))

(:derived (Flag16-)(and (LEFTOF2-ROBOT)))

(:derived (Flag16-)(and (LEFTOF13-ROBOT)))

(:derived (Flag16-)(and (LEFTOF1-ROBOT)))

(:derived (Flag16-)(and (LEFTOF12-ROBOT)))

(:derived (Flag16-)(and (LEFTOF6-ROBOT)))

(:derived (Flag16-)(and (LEFTOF5-ROBOT)))

(:derived (Flag16-)(and (LEFTOF3-ROBOT)))

(:derived (Flag16-)(and (LEFTOF4-ROBOT)))

(:derived (Flag16-)(and (LEFTOF7-ROBOT)))

(:derived (Flag16-)(and (LEFTOF8-ROBOT)))

(:derived (Flag17-)(and (LEFTOF10-ROBOT)))

(:derived (Flag17-)(and (LEFTOF11-ROBOT)))

(:derived (Flag17-)(and (LEFTOF9-ROBOT)))

(:derived (Flag17-)(and (LEFTOF2-ROBOT)))

(:derived (Flag17-)(and (LEFTOF13-ROBOT)))

(:derived (Flag17-)(and (LEFTOF1-ROBOT)))

(:derived (Flag17-)(and (LEFTOF12-ROBOT)))

(:derived (Flag17-)(and (LEFTOF6-ROBOT)))

(:derived (Flag17-)(and (LEFTOF5-ROBOT)))

(:derived (Flag17-)(and (LEFTOF14-ROBOT)))

(:derived (Flag17-)(and (LEFTOF3-ROBOT)))

(:derived (Flag17-)(and (LEFTOF4-ROBOT)))

(:derived (Flag17-)(and (LEFTOF7-ROBOT)))

(:derived (Flag17-)(and (LEFTOF8-ROBOT)))

(:derived (Flag18-)(and (LEFTOF15-ROBOT)))

(:derived (Flag18-)(and (LEFTOF10-ROBOT)))

(:derived (Flag18-)(and (LEFTOF11-ROBOT)))

(:derived (Flag18-)(and (LEFTOF9-ROBOT)))

(:derived (Flag18-)(and (LEFTOF2-ROBOT)))

(:derived (Flag18-)(and (LEFTOF13-ROBOT)))

(:derived (Flag18-)(and (LEFTOF1-ROBOT)))

(:derived (Flag18-)(and (LEFTOF12-ROBOT)))

(:derived (Flag18-)(and (LEFTOF6-ROBOT)))

(:derived (Flag18-)(and (LEFTOF5-ROBOT)))

(:derived (Flag18-)(and (LEFTOF14-ROBOT)))

(:derived (Flag18-)(and (LEFTOF3-ROBOT)))

(:derived (Flag18-)(and (LEFTOF4-ROBOT)))

(:derived (Flag18-)(and (LEFTOF7-ROBOT)))

(:derived (Flag18-)(and (LEFTOF8-ROBOT)))

(:derived (Flag19-)(and (LEFTOF15-ROBOT)))

(:derived (Flag19-)(and (LEFTOF10-ROBOT)))

(:derived (Flag19-)(and (LEFTOF11-ROBOT)))

(:derived (Flag19-)(and (LEFTOF9-ROBOT)))

(:derived (Flag19-)(and (LEFTOF2-ROBOT)))

(:derived (Flag19-)(and (LEFTOF13-ROBOT)))

(:derived (Flag19-)(and (LEFTOF1-ROBOT)))

(:derived (Flag19-)(and (LEFTOF12-ROBOT)))

(:derived (Flag19-)(and (LEFTOF6-ROBOT)))

(:derived (Flag19-)(and (LEFTOF5-ROBOT)))

(:derived (Flag19-)(and (LEFTOF16-ROBOT)))

(:derived (Flag19-)(and (LEFTOF14-ROBOT)))

(:derived (Flag19-)(and (LEFTOF3-ROBOT)))

(:derived (Flag19-)(and (LEFTOF4-ROBOT)))

(:derived (Flag19-)(and (LEFTOF7-ROBOT)))

(:derived (Flag19-)(and (LEFTOF8-ROBOT)))

(:derived (Flag20-)(and (LEFTOF15-ROBOT)))

(:derived (Flag20-)(and (LEFTOF10-ROBOT)))

(:derived (Flag20-)(and (LEFTOF17-ROBOT)))

(:derived (Flag20-)(and (LEFTOF11-ROBOT)))

(:derived (Flag20-)(and (LEFTOF9-ROBOT)))

(:derived (Flag20-)(and (LEFTOF2-ROBOT)))

(:derived (Flag20-)(and (LEFTOF13-ROBOT)))

(:derived (Flag20-)(and (LEFTOF1-ROBOT)))

(:derived (Flag20-)(and (LEFTOF12-ROBOT)))

(:derived (Flag20-)(and (LEFTOF6-ROBOT)))

(:derived (Flag20-)(and (LEFTOF5-ROBOT)))

(:derived (Flag20-)(and (LEFTOF16-ROBOT)))

(:derived (Flag20-)(and (LEFTOF14-ROBOT)))

(:derived (Flag20-)(and (LEFTOF3-ROBOT)))

(:derived (Flag20-)(and (LEFTOF4-ROBOT)))

(:derived (Flag20-)(and (LEFTOF7-ROBOT)))

(:derived (Flag20-)(and (LEFTOF8-ROBOT)))

(:derived (Flag21-)(and (LEFTOF15-ROBOT)))

(:derived (Flag21-)(and (LEFTOF10-ROBOT)))

(:derived (Flag21-)(and (LEFTOF17-ROBOT)))

(:derived (Flag21-)(and (LEFTOF11-ROBOT)))

(:derived (Flag21-)(and (LEFTOF9-ROBOT)))

(:derived (Flag21-)(and (LEFTOF2-ROBOT)))

(:derived (Flag21-)(and (LEFTOF13-ROBOT)))

(:derived (Flag21-)(and (LEFTOF1-ROBOT)))

(:derived (Flag21-)(and (LEFTOF12-ROBOT)))

(:derived (Flag21-)(and (LEFTOF6-ROBOT)))

(:derived (Flag21-)(and (LEFTOF5-ROBOT)))

(:derived (Flag21-)(and (LEFTOF16-ROBOT)))

(:derived (Flag21-)(and (LEFTOF14-ROBOT)))

(:derived (Flag21-)(and (LEFTOF3-ROBOT)))

(:derived (Flag21-)(and (LEFTOF4-ROBOT)))

(:derived (Flag21-)(and (LEFTOF7-ROBOT)))

(:derived (Flag21-)(and (LEFTOF8-ROBOT)))

(:derived (Flag22-)(and (LEFTOF15-ROBOT)))

(:derived (Flag22-)(and (LEFTOF10-ROBOT)))

(:derived (Flag22-)(and (LEFTOF1-ROBOT)))

(:derived (Flag22-)(and (LEFTOF17-ROBOT)))

(:derived (Flag22-)(and (LEFTOF11-ROBOT)))

(:derived (Flag22-)(and (LEFTOF9-ROBOT)))

(:derived (Flag22-)(and (LEFTOF2-ROBOT)))

(:derived (Flag22-)(and (LEFTOF13-ROBOT)))

(:derived (Flag22-)(and (LEFTOF19-ROBOT)))

(:derived (Flag22-)(and (LEFTOF12-ROBOT)))

(:derived (Flag22-)(and (LEFTOF6-ROBOT)))

(:derived (Flag22-)(and (LEFTOF5-ROBOT)))

(:derived (Flag22-)(and (LEFTOF16-ROBOT)))

(:derived (Flag22-)(and (LEFTOF14-ROBOT)))

(:derived (Flag22-)(and (LEFTOF3-ROBOT)))

(:derived (Flag22-)(and (LEFTOF4-ROBOT)))

(:derived (Flag22-)(and (LEFTOF7-ROBOT)))

(:derived (Flag22-)(and (LEFTOF8-ROBOT)))

(:derived (Flag23-)(and (RIGHTOF3-ROBOT)))

(:derived (Flag23-)(and (RIGHTOF8-ROBOT)))

(:derived (Flag23-)(and (RIGHTOF5-ROBOT)))

(:derived (Flag23-)(and (RIGHTOF9-ROBOT)))

(:derived (Flag23-)(and (RIGHTOF17-ROBOT)))

(:derived (Flag23-)(and (RIGHTOF14-ROBOT)))

(:derived (Flag23-)(and (RIGHTOF13-ROBOT)))

(:derived (Flag23-)(and (RIGHTOF12-ROBOT)))

(:derived (Flag23-)(and (RIGHTOF10-ROBOT)))

(:derived (Flag23-)(and (RIGHTOF4-ROBOT)))

(:derived (Flag23-)(and (RIGHTOF1-ROBOT)))

(:derived (Flag23-)(and (RIGHTOF16-ROBOT)))

(:derived (Flag23-)(and (RIGHTOF18-ROBOT)))

(:derived (Flag23-)(and (RIGHTOF7-ROBOT)))

(:derived (Flag23-)(and (RIGHTOF11-ROBOT)))

(:derived (Flag23-)(and (RIGHTOF15-ROBOT)))

(:derived (Flag23-)(and (RIGHTOF6-ROBOT)))

(:derived (Flag24-)(and (RIGHTOF3-ROBOT)))

(:derived (Flag24-)(and (RIGHTOF5-ROBOT)))

(:derived (Flag24-)(and (RIGHTOF9-ROBOT)))

(:derived (Flag24-)(and (RIGHTOF17-ROBOT)))

(:derived (Flag24-)(and (RIGHTOF14-ROBOT)))

(:derived (Flag24-)(and (RIGHTOF13-ROBOT)))

(:derived (Flag24-)(and (RIGHTOF12-ROBOT)))

(:derived (Flag24-)(and (RIGHTOF10-ROBOT)))

(:derived (Flag24-)(and (RIGHTOF4-ROBOT)))

(:derived (Flag24-)(and (RIGHTOF8-ROBOT)))

(:derived (Flag24-)(and (RIGHTOF16-ROBOT)))

(:derived (Flag24-)(and (RIGHTOF18-ROBOT)))

(:derived (Flag24-)(and (RIGHTOF7-ROBOT)))

(:derived (Flag24-)(and (RIGHTOF11-ROBOT)))

(:derived (Flag24-)(and (RIGHTOF15-ROBOT)))

(:derived (Flag24-)(and (RIGHTOF6-ROBOT)))

(:derived (Flag25-)(and (RIGHTOF7-ROBOT)))

(:derived (Flag25-)(and (RIGHTOF3-ROBOT)))

(:derived (Flag25-)(and (RIGHTOF5-ROBOT)))

(:derived (Flag25-)(and (RIGHTOF9-ROBOT)))

(:derived (Flag25-)(and (RIGHTOF17-ROBOT)))

(:derived (Flag25-)(and (RIGHTOF14-ROBOT)))

(:derived (Flag25-)(and (RIGHTOF13-ROBOT)))

(:derived (Flag25-)(and (RIGHTOF12-ROBOT)))

(:derived (Flag25-)(and (RIGHTOF10-ROBOT)))

(:derived (Flag25-)(and (RIGHTOF4-ROBOT)))

(:derived (Flag25-)(and (RIGHTOF8-ROBOT)))

(:derived (Flag25-)(and (RIGHTOF16-ROBOT)))

(:derived (Flag25-)(and (RIGHTOF18-ROBOT)))

(:derived (Flag25-)(and (RIGHTOF11-ROBOT)))

(:derived (Flag25-)(and (RIGHTOF15-ROBOT)))

(:derived (Flag25-)(and (RIGHTOF6-ROBOT)))

(:derived (Flag26-)(and (RIGHTOF7-ROBOT)))

(:derived (Flag26-)(and (RIGHTOF5-ROBOT)))

(:derived (Flag26-)(and (RIGHTOF9-ROBOT)))

(:derived (Flag26-)(and (RIGHTOF17-ROBOT)))

(:derived (Flag26-)(and (RIGHTOF14-ROBOT)))

(:derived (Flag26-)(and (RIGHTOF13-ROBOT)))

(:derived (Flag26-)(and (RIGHTOF12-ROBOT)))

(:derived (Flag26-)(and (RIGHTOF10-ROBOT)))

(:derived (Flag26-)(and (RIGHTOF4-ROBOT)))

(:derived (Flag26-)(and (RIGHTOF8-ROBOT)))

(:derived (Flag26-)(and (RIGHTOF16-ROBOT)))

(:derived (Flag26-)(and (RIGHTOF18-ROBOT)))

(:derived (Flag26-)(and (RIGHTOF11-ROBOT)))

(:derived (Flag26-)(and (RIGHTOF15-ROBOT)))

(:derived (Flag26-)(and (RIGHTOF6-ROBOT)))

(:derived (Flag27-)(and (RIGHTOF7-ROBOT)))

(:derived (Flag27-)(and (RIGHTOF5-ROBOT)))

(:derived (Flag27-)(and (RIGHTOF9-ROBOT)))

(:derived (Flag27-)(and (RIGHTOF17-ROBOT)))

(:derived (Flag27-)(and (RIGHTOF14-ROBOT)))

(:derived (Flag27-)(and (RIGHTOF13-ROBOT)))

(:derived (Flag27-)(and (RIGHTOF12-ROBOT)))

(:derived (Flag27-)(and (RIGHTOF10-ROBOT)))

(:derived (Flag27-)(and (RIGHTOF8-ROBOT)))

(:derived (Flag27-)(and (RIGHTOF16-ROBOT)))

(:derived (Flag27-)(and (RIGHTOF18-ROBOT)))

(:derived (Flag27-)(and (RIGHTOF11-ROBOT)))

(:derived (Flag27-)(and (RIGHTOF15-ROBOT)))

(:derived (Flag27-)(and (RIGHTOF6-ROBOT)))

(:derived (Flag28-)(and (RIGHTOF7-ROBOT)))

(:derived (Flag28-)(and (RIGHTOF9-ROBOT)))

(:derived (Flag28-)(and (RIGHTOF17-ROBOT)))

(:derived (Flag28-)(and (RIGHTOF14-ROBOT)))

(:derived (Flag28-)(and (RIGHTOF13-ROBOT)))

(:derived (Flag28-)(and (RIGHTOF12-ROBOT)))

(:derived (Flag28-)(and (RIGHTOF10-ROBOT)))

(:derived (Flag28-)(and (RIGHTOF8-ROBOT)))

(:derived (Flag28-)(and (RIGHTOF16-ROBOT)))

(:derived (Flag28-)(and (RIGHTOF18-ROBOT)))

(:derived (Flag28-)(and (RIGHTOF11-ROBOT)))

(:derived (Flag28-)(and (RIGHTOF15-ROBOT)))

(:derived (Flag28-)(and (RIGHTOF6-ROBOT)))

(:derived (Flag29-)(and (RIGHTOF18-ROBOT)))

(:derived (Flag29-)(and (RIGHTOF17-ROBOT)))

(:derived (Flag29-)(and (RIGHTOF14-ROBOT)))

(:derived (Flag29-)(and (RIGHTOF13-ROBOT)))

(:derived (Flag29-)(and (RIGHTOF15-ROBOT)))

(:derived (Flag29-)(and (RIGHTOF10-ROBOT)))

(:derived (Flag29-)(and (RIGHTOF8-ROBOT)))

(:derived (Flag29-)(and (RIGHTOF12-ROBOT)))

(:derived (Flag29-)(and (RIGHTOF16-ROBOT)))

(:derived (Flag29-)(and (RIGHTOF7-ROBOT)))

(:derived (Flag29-)(and (RIGHTOF9-ROBOT)))

(:derived (Flag29-)(and (RIGHTOF11-ROBOT)))

(:derived (Flag30-)(and (RIGHTOF18-ROBOT)))

(:derived (Flag30-)(and (RIGHTOF17-ROBOT)))

(:derived (Flag30-)(and (RIGHTOF14-ROBOT)))

(:derived (Flag30-)(and (RIGHTOF13-ROBOT)))

(:derived (Flag30-)(and (RIGHTOF15-ROBOT)))

(:derived (Flag30-)(and (RIGHTOF10-ROBOT)))

(:derived (Flag30-)(and (RIGHTOF8-ROBOT)))

(:derived (Flag30-)(and (RIGHTOF16-ROBOT)))

(:derived (Flag30-)(and (RIGHTOF12-ROBOT)))

(:derived (Flag30-)(and (RIGHTOF9-ROBOT)))

(:derived (Flag30-)(and (RIGHTOF11-ROBOT)))

(:derived (Flag31-)(and (RIGHTOF9-ROBOT)))

(:derived (Flag31-)(and (RIGHTOF17-ROBOT)))

(:derived (Flag31-)(and (RIGHTOF14-ROBOT)))

(:derived (Flag31-)(and (RIGHTOF13-ROBOT)))

(:derived (Flag31-)(and (RIGHTOF15-ROBOT)))

(:derived (Flag31-)(and (RIGHTOF10-ROBOT)))

(:derived (Flag31-)(and (RIGHTOF16-ROBOT)))

(:derived (Flag31-)(and (RIGHTOF12-ROBOT)))

(:derived (Flag31-)(and (RIGHTOF18-ROBOT)))

(:derived (Flag31-)(and (RIGHTOF11-ROBOT)))

(:derived (Flag32-)(and (RIGHTOF17-ROBOT)))

(:derived (Flag32-)(and (RIGHTOF14-ROBOT)))

(:derived (Flag32-)(and (RIGHTOF13-ROBOT)))

(:derived (Flag32-)(and (RIGHTOF15-ROBOT)))

(:derived (Flag32-)(and (RIGHTOF10-ROBOT)))

(:derived (Flag32-)(and (RIGHTOF16-ROBOT)))

(:derived (Flag32-)(and (RIGHTOF12-ROBOT)))

(:derived (Flag32-)(and (RIGHTOF18-ROBOT)))

(:derived (Flag32-)(and (RIGHTOF11-ROBOT)))

(:derived (Flag33-)(and (RIGHTOF17-ROBOT)))

(:derived (Flag33-)(and (RIGHTOF14-ROBOT)))

(:derived (Flag33-)(and (RIGHTOF13-ROBOT)))

(:derived (Flag33-)(and (RIGHTOF15-ROBOT)))

(:derived (Flag33-)(and (RIGHTOF16-ROBOT)))

(:derived (Flag33-)(and (RIGHTOF12-ROBOT)))

(:derived (Flag33-)(and (RIGHTOF18-ROBOT)))

(:derived (Flag33-)(and (RIGHTOF11-ROBOT)))

(:derived (Flag34-)(and (RIGHTOF17-ROBOT)))

(:derived (Flag34-)(and (RIGHTOF14-ROBOT)))

(:derived (Flag34-)(and (RIGHTOF13-ROBOT)))

(:derived (Flag34-)(and (RIGHTOF12-ROBOT)))

(:derived (Flag34-)(and (RIGHTOF16-ROBOT)))

(:derived (Flag34-)(and (RIGHTOF18-ROBOT)))

(:derived (Flag34-)(and (RIGHTOF15-ROBOT)))

(:derived (Flag35-)(and (RIGHTOF17-ROBOT)))

(:derived (Flag35-)(and (RIGHTOF14-ROBOT)))

(:derived (Flag35-)(and (RIGHTOF13-ROBOT)))

(:derived (Flag35-)(and (RIGHTOF15-ROBOT)))

(:derived (Flag35-)(and (RIGHTOF16-ROBOT)))

(:derived (Flag35-)(and (RIGHTOF18-ROBOT)))

(:derived (Flag36-)(and (RIGHTOF16-ROBOT)))

(:derived (Flag36-)(and (RIGHTOF17-ROBOT)))

(:derived (Flag36-)(and (RIGHTOF14-ROBOT)))

(:derived (Flag36-)(and (RIGHTOF18-ROBOT)))

(:derived (Flag36-)(and (RIGHTOF15-ROBOT)))

(:derived (Flag37-)(and (RIGHTOF16-ROBOT)))

(:derived (Flag37-)(and (RIGHTOF17-ROBOT)))

(:derived (Flag37-)(and (RIGHTOF18-ROBOT)))

(:derived (Flag37-)(and (RIGHTOF15-ROBOT)))

(:derived (Flag38-)(and (RIGHTOF16-ROBOT)))

(:derived (Flag38-)(and (RIGHTOF17-ROBOT)))

(:derived (Flag38-)(and (RIGHTOF18-ROBOT)))

(:derived (Flag39-)(and (RIGHTOF17-ROBOT)))

(:derived (Flag39-)(and (RIGHTOF18-ROBOT)))

(:derived (Flag40-)(and (LEFTOF2-ROBOT)(RIGHTOF15-ROBOT)))

(:derived (Flag41-)(and (RIGHTOF18-ROBOT)(LEFTOF2-ROBOT)))

(:derived (Flag42-)(and (RIGHTOF13-ROBOT)(LEFTOF2-ROBOT)))

(:derived (Flag43-)(and (LEFTOF2-ROBOT)(RIGHTOF9-ROBOT)))

(:derived (Flag44-)(and (LEFTOF1-ROBOT)(RIGHTOF9-ROBOT)))

(:derived (Flag45-)(and (RIGHTOF3-ROBOT)(LEFTOF1-ROBOT)))

(:derived (Flag46-)(and (RIGHTOF18-ROBOT)(LEFTOF1-ROBOT)))

(:derived (Flag47-)(and (LEFTOF1-ROBOT)(RIGHTOF15-ROBOT)))

(:derived (Flag48-)(and (RIGHTOF10-ROBOT)(LEFTOF1-ROBOT)))

(:derived (Flag49-)(and (LEFTOF2-ROBOT)(RIGHTOF1-ROBOT)))

(:derived (Flag50-)(and (RIGHTOF5-ROBOT)(LEFTOF2-ROBOT)))

(:derived (Flag51-)(and (LEFTOF1-ROBOT)(RIGHTOF1-ROBOT)))

(:derived (Flag52-)(and (LEFTOF2-ROBOT)(RIGHTOF7-ROBOT)))

(:derived (Flag53-)(and (RIGHTOF16-ROBOT)(LEFTOF2-ROBOT)))

(:derived (Flag54-)(and (RIGHTOF5-ROBOT)(LEFTOF1-ROBOT)))

(:derived (Flag55-)(and (RIGHTOF11-ROBOT)(LEFTOF1-ROBOT)))

(:derived (Flag56-)(and (RIGHTOF3-ROBOT)(LEFTOF2-ROBOT)))

(:derived (Flag57-)(and (LEFTOF2-ROBOT)(RIGHTOF17-ROBOT)))

(:derived (Flag58-)(and (LEFTOF2-ROBOT)(RIGHTOF14-ROBOT)))

(:derived (Flag59-)(and (LEFTOF1-ROBOT)(RIGHTOF4-ROBOT)))

(:derived (Flag60-)(and (RIGHTOF10-ROBOT)(LEFTOF2-ROBOT)))

(:derived (Flag61-)(and (LEFTOF1-ROBOT)(RIGHTOF12-ROBOT)))

(:derived (Flag62-)(and (LEFTOF1-ROBOT)(RIGHTOF7-ROBOT)))

(:derived (Flag63-)(and (LEFTOF2-ROBOT)(RIGHTOF12-ROBOT)))

(:derived (Flag64-)(and (LEFTOF1-ROBOT)(RIGHTOF5-ROBOT)))

(:derived (Flag65-)(and (RIGHTOF8-ROBOT)(LEFTOF1-ROBOT)))

(:derived (Flag66-)(and (LEFTOF1-ROBOT)(RIGHTOF17-ROBOT)))

(:derived (Flag67-)(and (RIGHTOF13-ROBOT)(LEFTOF1-ROBOT)))

(:derived (Flag68-)(and (RIGHTOF11-ROBOT)(LEFTOF2-ROBOT)))

(:derived (Flag69-)(and (RIGHTOF8-ROBOT)(LEFTOF2-ROBOT)))

(:derived (Flag70-)(and (RIGHTOF2-ROBOT)(LEFTOF2-ROBOT)))

(:derived (Flag71-)(and (LEFTOF1-ROBOT)(RIGHTOF2-ROBOT)))

(:derived (Flag72-)(and (LEFTOF1-ROBOT)(RIGHTOF6-ROBOT)))

(:derived (Flag73-)(and (LEFTOF1-ROBOT)(RIGHTOF14-ROBOT)))

(:derived (Flag74-)(and (LEFTOF1-ROBOT)(RIGHTOF16-ROBOT)))

(:derived (Flag75-)(and (LEFTOF2-ROBOT)(RIGHTOF6-ROBOT)))

(:derived (Flag76-)(and (LEFTOF2-ROBOT)(RIGHTOF4-ROBOT)))

(:derived (Flag78-)(and (LEFTOF3-ROBOT)(RIGHTOF6-ROBOT)))

(:derived (Flag79-)(and (LEFTOF3-ROBOT)(RIGHTOF14-ROBOT)))

(:derived (Flag80-)(and (LEFTOF3-ROBOT)(RIGHTOF4-ROBOT)))

(:derived (Flag81-)(and (LEFTOF3-ROBOT)(RIGHTOF16-ROBOT)))

(:derived (Flag82-)(and (LEFTOF3-ROBOT)(RIGHTOF7-ROBOT)))

(:derived (Flag83-)(and (LEFTOF3-ROBOT)(RIGHTOF12-ROBOT)))

(:derived (Flag84-)(and (LEFTOF3-ROBOT)(RIGHTOF10-ROBOT)))

(:derived (Flag85-)(and (LEFTOF3-ROBOT)(RIGHTOF8-ROBOT)))

(:derived (Flag86-)(and (LEFTOF3-ROBOT)(RIGHTOF5-ROBOT)))

(:derived (Flag87-)(and (LEFTOF3-ROBOT)(RIGHTOF15-ROBOT)))

(:derived (Flag88-)(and (RIGHTOF3-ROBOT)(LEFTOF3-ROBOT)))

(:derived (Flag89-)(and (LEFTOF3-ROBOT)(RIGHTOF11-ROBOT)))

(:derived (Flag90-)(and (LEFTOF3-ROBOT)(RIGHTOF18-ROBOT)))

(:derived (Flag91-)(and (RIGHTOF13-ROBOT)(LEFTOF3-ROBOT)))

(:derived (Flag92-)(and (LEFTOF3-ROBOT)(RIGHTOF17-ROBOT)))

(:derived (Flag93-)(and (LEFTOF3-ROBOT)(RIGHTOF9-ROBOT)))

(:derived (Flag94-)(and (LEFTOF3-ROBOT)(RIGHTOF2-ROBOT)))

(:derived (Flag96-)(and (RIGHTOF18-ROBOT)(LEFTOF4-ROBOT)))

(:derived (Flag97-)(and (LEFTOF3-ROBOT)(RIGHTOF13-ROBOT)))

(:derived (Flag98-)(and (LEFTOF4-ROBOT)(RIGHTOF17-ROBOT)))

(:derived (Flag99-)(and (LEFTOF4-ROBOT)(RIGHTOF4-ROBOT)))

(:derived (Flag100-)(and (LEFTOF4-ROBOT)(RIGHTOF7-ROBOT)))

(:derived (Flag101-)(and (LEFTOF4-ROBOT)(RIGHTOF15-ROBOT)))

(:derived (Flag102-)(and (LEFTOF4-ROBOT)(RIGHTOF14-ROBOT)))

(:derived (Flag103-)(and (LEFTOF3-ROBOT)(RIGHTOF3-ROBOT)))

(:derived (Flag104-)(and (RIGHTOF8-ROBOT)(LEFTOF4-ROBOT)))

(:derived (Flag105-)(and (LEFTOF4-ROBOT)(RIGHTOF12-ROBOT)))

(:derived (Flag106-)(and (RIGHTOF10-ROBOT)(LEFTOF4-ROBOT)))

(:derived (Flag107-)(and (LEFTOF4-ROBOT)(RIGHTOF9-ROBOT)))

(:derived (Flag108-)(and (LEFTOF4-ROBOT)(RIGHTOF16-ROBOT)))

(:derived (Flag109-)(and (LEFTOF4-ROBOT)(RIGHTOF6-ROBOT)))

(:derived (Flag110-)(and (LEFTOF4-ROBOT)(RIGHTOF5-ROBOT)))

(:derived (Flag111-)(and (RIGHTOF13-ROBOT)(LEFTOF4-ROBOT)))

(:derived (Flag112-)(and (RIGHTOF3-ROBOT)(LEFTOF4-ROBOT)))

(:derived (Flag113-)(and (LEFTOF4-ROBOT)(RIGHTOF11-ROBOT)))

(:derived (Flag115-)(and (RIGHTOF9-ROBOT)(LEFTOF5-ROBOT)))

(:derived (Flag116-)(and (RIGHTOF15-ROBOT)(LEFTOF5-ROBOT)))

(:derived (Flag117-)(and (RIGHTOF4-ROBOT)(LEFTOF5-ROBOT)))

(:derived (Flag118-)(and (RIGHTOF18-ROBOT)(LEFTOF5-ROBOT)))

(:derived (Flag119-)(and (LEFTOF4-ROBOT)(RIGHTOF18-ROBOT)))

(:derived (Flag120-)(and (RIGHTOF7-ROBOT)(LEFTOF5-ROBOT)))

(:derived (Flag121-)(and (RIGHTOF11-ROBOT)(LEFTOF5-ROBOT)))

(:derived (Flag122-)(and (RIGHTOF13-ROBOT)(LEFTOF5-ROBOT)))

(:derived (Flag123-)(and (RIGHTOF16-ROBOT)(LEFTOF5-ROBOT)))

(:derived (Flag124-)(and (LEFTOF4-ROBOT)(RIGHTOF8-ROBOT)))

(:derived (Flag125-)(and (LEFTOF5-ROBOT)(RIGHTOF6-ROBOT)))

(:derived (Flag126-)(and (RIGHTOF10-ROBOT)(LEFTOF5-ROBOT)))

(:derived (Flag127-)(and (LEFTOF4-ROBOT)(RIGHTOF10-ROBOT)))

(:derived (Flag128-)(and (LEFTOF5-ROBOT)(RIGHTOF12-ROBOT)))

(:derived (Flag129-)(and (RIGHTOF8-ROBOT)(LEFTOF5-ROBOT)))

(:derived (Flag130-)(and (RIGHTOF14-ROBOT)(LEFTOF5-ROBOT)))

(:derived (Flag131-)(and (RIGHTOF5-ROBOT)(LEFTOF5-ROBOT)))

(:derived (Flag132-)(and (RIGHTOF17-ROBOT)(LEFTOF5-ROBOT)))

(:derived (Flag134-)(and (RIGHTOF15-ROBOT)(LEFTOF6-ROBOT)))

(:derived (Flag135-)(and (RIGHTOF11-ROBOT)(LEFTOF6-ROBOT)))

(:derived (Flag136-)(and (RIGHTOF5-ROBOT)(LEFTOF6-ROBOT)))

(:derived (Flag137-)(and (RIGHTOF14-ROBOT)(LEFTOF6-ROBOT)))

(:derived (Flag138-)(and (RIGHTOF7-ROBOT)(LEFTOF6-ROBOT)))

(:derived (Flag139-)(and (RIGHTOF12-ROBOT)(LEFTOF6-ROBOT)))

(:derived (Flag140-)(and (RIGHTOF17-ROBOT)(LEFTOF6-ROBOT)))

(:derived (Flag141-)(and (RIGHTOF10-ROBOT)(LEFTOF6-ROBOT)))

(:derived (Flag142-)(and (RIGHTOF6-ROBOT)(LEFTOF6-ROBOT)))

(:derived (Flag143-)(and (RIGHTOF18-ROBOT)(LEFTOF6-ROBOT)))

(:derived (Flag144-)(and (RIGHTOF13-ROBOT)(LEFTOF6-ROBOT)))

(:derived (Flag145-)(and (RIGHTOF9-ROBOT)(LEFTOF6-ROBOT)))

(:derived (Flag146-)(and (RIGHTOF16-ROBOT)(LEFTOF6-ROBOT)))

(:derived (Flag147-)(and (RIGHTOF8-ROBOT)(LEFTOF6-ROBOT)))

(:derived (Flag149-)(and (LEFTOF7-ROBOT)(RIGHTOF7-ROBOT)))

(:derived (Flag150-)(and (RIGHTOF13-ROBOT)(LEFTOF7-ROBOT)))

(:derived (Flag151-)(and (LEFTOF7-ROBOT)(RIGHTOF9-ROBOT)))

(:derived (Flag152-)(and (LEFTOF6-ROBOT)(RIGHTOF6-ROBOT)))

(:derived (Flag153-)(and (RIGHTOF18-ROBOT)(LEFTOF7-ROBOT)))

(:derived (Flag154-)(and (LEFTOF7-ROBOT)(RIGHTOF12-ROBOT)))

(:derived (Flag155-)(and (LEFTOF7-ROBOT)(RIGHTOF6-ROBOT)))

(:derived (Flag156-)(and (LEFTOF6-ROBOT)(RIGHTOF12-ROBOT)))

(:derived (Flag157-)(and (LEFTOF7-ROBOT)(RIGHTOF15-ROBOT)))

(:derived (Flag158-)(and (RIGHTOF11-ROBOT)(LEFTOF7-ROBOT)))

(:derived (Flag159-)(and (RIGHTOF8-ROBOT)(LEFTOF7-ROBOT)))

(:derived (Flag160-)(and (RIGHTOF17-ROBOT)(LEFTOF7-ROBOT)))

(:derived (Flag161-)(and (RIGHTOF16-ROBOT)(LEFTOF7-ROBOT)))

(:derived (Flag162-)(and (LEFTOF7-ROBOT)(RIGHTOF14-ROBOT)))

(:derived (Flag163-)(and (RIGHTOF10-ROBOT)(LEFTOF7-ROBOT)))

(:derived (Flag165-)(and (RIGHTOF10-ROBOT)(LEFTOF8-ROBOT)))

(:derived (Flag166-)(and (RIGHTOF16-ROBOT)(LEFTOF8-ROBOT)))

(:derived (Flag167-)(and (RIGHTOF13-ROBOT)(LEFTOF8-ROBOT)))

(:derived (Flag168-)(and (RIGHTOF7-ROBOT)(LEFTOF7-ROBOT)))

(:derived (Flag169-)(and (LEFTOF8-ROBOT)(RIGHTOF15-ROBOT)))

(:derived (Flag170-)(and (LEFTOF8-ROBOT)(RIGHTOF9-ROBOT)))

(:derived (Flag171-)(and (RIGHTOF7-ROBOT)(LEFTOF8-ROBOT)))

(:derived (Flag172-)(and (RIGHTOF18-ROBOT)(LEFTOF8-ROBOT)))

(:derived (Flag173-)(and (RIGHTOF14-ROBOT)(LEFTOF8-ROBOT)))

(:derived (Flag174-)(and (LEFTOF8-ROBOT)(RIGHTOF12-ROBOT)))

(:derived (Flag175-)(and (RIGHTOF11-ROBOT)(LEFTOF8-ROBOT)))

(:derived (Flag176-)(and (RIGHTOF17-ROBOT)(LEFTOF8-ROBOT)))

(:derived (Flag177-)(and (RIGHTOF8-ROBOT)(LEFTOF8-ROBOT)))

(:derived (Flag179-)(and (RIGHTOF13-ROBOT)(LEFTOF9-ROBOT)))

(:derived (Flag180-)(and (RIGHTOF8-ROBOT)(LEFTOF9-ROBOT)))

(:derived (Flag181-)(and (LEFTOF9-ROBOT)(RIGHTOF12-ROBOT)))

(:derived (Flag182-)(and (LEFTOF9-ROBOT)(RIGHTOF9-ROBOT)))

(:derived (Flag183-)(and (LEFTOF8-ROBOT)(RIGHTOF14-ROBOT)))

(:derived (Flag184-)(and (RIGHTOF11-ROBOT)(LEFTOF9-ROBOT)))

(:derived (Flag185-)(and (LEFTOF9-ROBOT)(RIGHTOF15-ROBOT)))

(:derived (Flag186-)(and (RIGHTOF10-ROBOT)(LEFTOF9-ROBOT)))

(:derived (Flag187-)(and (RIGHTOF18-ROBOT)(LEFTOF9-ROBOT)))

(:derived (Flag188-)(and (LEFTOF9-ROBOT)(RIGHTOF16-ROBOT)))

(:derived (Flag189-)(and (LEFTOF9-ROBOT)(RIGHTOF17-ROBOT)))

(:derived (Flag190-)(and (LEFTOF9-ROBOT)(RIGHTOF14-ROBOT)))

(:derived (Flag192-)(and (LEFTOF10-ROBOT)(RIGHTOF12-ROBOT)))

(:derived (Flag193-)(and (RIGHTOF18-ROBOT)(LEFTOF10-ROBOT)))

(:derived (Flag194-)(and (RIGHTOF17-ROBOT)(LEFTOF10-ROBOT)))

(:derived (Flag195-)(and (RIGHTOF14-ROBOT)(LEFTOF10-ROBOT)))

(:derived (Flag196-)(and (RIGHTOF13-ROBOT)(LEFTOF10-ROBOT)))

(:derived (Flag197-)(and (LEFTOF10-ROBOT)(RIGHTOF9-ROBOT)))

(:derived (Flag198-)(and (RIGHTOF11-ROBOT)(LEFTOF10-ROBOT)))

(:derived (Flag199-)(and (RIGHTOF10-ROBOT)(LEFTOF10-ROBOT)))

(:derived (Flag200-)(and (RIGHTOF16-ROBOT)(LEFTOF10-ROBOT)))

(:derived (Flag201-)(and (LEFTOF10-ROBOT)(RIGHTOF15-ROBOT)))

(:derived (Flag203-)(and (LEFTOF11-ROBOT)(RIGHTOF15-ROBOT)))

(:derived (Flag204-)(and (LEFTOF11-ROBOT)(RIGHTOF16-ROBOT)))

(:derived (Flag205-)(and (RIGHTOF11-ROBOT)(LEFTOF11-ROBOT)))

(:derived (Flag206-)(and (LEFTOF10-ROBOT)(RIGHTOF14-ROBOT)))

(:derived (Flag207-)(and (RIGHTOF13-ROBOT)(LEFTOF11-ROBOT)))

(:derived (Flag208-)(and (LEFTOF11-ROBOT)(RIGHTOF14-ROBOT)))

(:derived (Flag209-)(and (LEFTOF11-ROBOT)(RIGHTOF12-ROBOT)))

(:derived (Flag210-)(and (RIGHTOF18-ROBOT)(LEFTOF11-ROBOT)))

(:derived (Flag211-)(and (LEFTOF11-ROBOT)(RIGHTOF17-ROBOT)))

(:derived (Flag212-)(and (RIGHTOF10-ROBOT)(LEFTOF11-ROBOT)))

(:derived (Flag214-)(and (RIGHTOF14-ROBOT)(LEFTOF12-ROBOT)))

(:derived (Flag215-)(and (RIGHTOF11-ROBOT)(LEFTOF12-ROBOT)))

(:derived (Flag216-)(and (RIGHTOF18-ROBOT)(LEFTOF12-ROBOT)))

(:derived (Flag217-)(and (RIGHTOF12-ROBOT)(LEFTOF12-ROBOT)))

(:derived (Flag218-)(and (RIGHTOF16-ROBOT)(LEFTOF12-ROBOT)))

(:derived (Flag219-)(and (RIGHTOF17-ROBOT)(LEFTOF12-ROBOT)))

(:derived (Flag220-)(and (RIGHTOF15-ROBOT)(LEFTOF12-ROBOT)))

(:derived (Flag221-)(and (RIGHTOF13-ROBOT)(LEFTOF12-ROBOT)))

(:derived (Flag223-)(and (RIGHTOF17-ROBOT)(LEFTOF13-ROBOT)))

(:derived (Flag224-)(and (RIGHTOF13-ROBOT)(LEFTOF13-ROBOT)))

(:derived (Flag225-)(and (LEFTOF13-ROBOT)(RIGHTOF15-ROBOT)))

(:derived (Flag226-)(and (LEFTOF13-ROBOT)(RIGHTOF14-ROBOT)))

(:derived (Flag227-)(and (LEFTOF12-ROBOT)(RIGHTOF12-ROBOT)))

(:derived (Flag228-)(and (RIGHTOF18-ROBOT)(LEFTOF13-ROBOT)))

(:derived (Flag229-)(and (LEFTOF13-ROBOT)(RIGHTOF12-ROBOT)))

(:derived (Flag230-)(and (RIGHTOF16-ROBOT)(LEFTOF13-ROBOT)))

(:derived (Flag232-)(and (LEFTOF14-ROBOT)(RIGHTOF14-ROBOT)))

(:derived (Flag233-)(and (RIGHTOF13-ROBOT)(LEFTOF14-ROBOT)))

(:derived (Flag234-)(and (LEFTOF14-ROBOT)(RIGHTOF17-ROBOT)))

(:derived (Flag235-)(and (LEFTOF14-ROBOT)(RIGHTOF16-ROBOT)))

(:derived (Flag236-)(and (RIGHTOF18-ROBOT)(LEFTOF14-ROBOT)))

(:derived (Flag237-)(and (LEFTOF14-ROBOT)(RIGHTOF15-ROBOT)))

(:derived (Flag239-)(and (LEFTOF14-ROBOT)(RIGHTOF18-ROBOT)))

(:derived (Flag240-)(and (RIGHTOF18-ROBOT)(LEFTOF15-ROBOT)))

(:derived (Flag241-)(and (RIGHTOF17-ROBOT)(LEFTOF15-ROBOT)))

(:derived (Flag242-)(and (RIGHTOF16-ROBOT)(LEFTOF15-ROBOT)))

(:derived (Flag243-)(and (RIGHTOF15-ROBOT)(LEFTOF15-ROBOT)))

(:derived (Flag244-)(and (RIGHTOF14-ROBOT)(LEFTOF15-ROBOT)))

(:derived (Flag246-)(and (LEFTOF16-ROBOT)(RIGHTOF17-ROBOT)))

(:derived (Flag247-)(and (RIGHTOF18-ROBOT)(LEFTOF16-ROBOT)))

(:derived (Flag248-)(and (LEFTOF15-ROBOT)(RIGHTOF15-ROBOT)))

(:derived (Flag249-)(and (RIGHTOF16-ROBOT)(LEFTOF16-ROBOT)))

(:derived (Flag250-)(and (LEFTOF16-ROBOT)(RIGHTOF15-ROBOT)))

(:derived (Flag252-)(and (LEFTOF17-ROBOT)(RIGHTOF17-ROBOT)))

(:derived (Flag253-)(and (LEFTOF17-ROBOT)(RIGHTOF16-ROBOT)))

(:derived (Flag254-)(and (LEFTOF17-ROBOT)(RIGHTOF18-ROBOT)))

(:derived (Flag256-)(and (RIGHTOF18-ROBOT)(LEFTOF18-ROBOT)))

(:derived (Flag257-)(and (RIGHTOF17-ROBOT)(LEFTOF18-ROBOT)))

(:derived (Flag259-)(and (RIGHTOF18-ROBOT)(LEFTOF19-ROBOT)))

(:derived (Flag261-)(and (RIGHTOF3-ROBOT)))

(:derived (Flag261-)(and (RIGHTOF8-ROBOT)))

(:derived (Flag261-)(and (RIGHTOF10-ROBOT)))

(:derived (Flag261-)(and (RIGHTOF5-ROBOT)))

(:derived (Flag261-)(and (RIGHTOF9-ROBOT)))

(:derived (Flag261-)(and (RIGHTOF17-ROBOT)))

(:derived (Flag261-)(and (RIGHTOF14-ROBOT)))

(:derived (Flag261-)(and (RIGHTOF13-ROBOT)))

(:derived (Flag261-)(and (RIGHTOF12-ROBOT)))

(:derived (Flag261-)(and (RIGHTOF0-ROBOT)))

(:derived (Flag261-)(and (RIGHTOF4-ROBOT)))

(:derived (Flag261-)(and (RIGHTOF1-ROBOT)))

(:derived (Flag261-)(and (RIGHTOF16-ROBOT)))

(:derived (Flag261-)(and (RIGHTOF18-ROBOT)))

(:derived (Flag261-)(and (RIGHTOF7-ROBOT)))

(:derived (Flag261-)(and (RIGHTOF11-ROBOT)))

(:derived (Flag261-)(and (RIGHTOF15-ROBOT)))

(:derived (Flag261-)(and (RIGHTOF6-ROBOT)))

(:derived (Flag262-)(and (RIGHTOF0-ROBOT)(LEFTOF1-ROBOT)))

(:derived (Flag336-)(and (Flag299-)))

(:derived (Flag336-)(and (Flag300-)))

(:derived (Flag336-)(and (Flag301-)))

(:derived (Flag336-)(and (Flag302-)))

(:derived (Flag336-)(and (Flag303-)))

(:derived (Flag336-)(and (Flag304-)))

(:derived (Flag336-)(and (Flag305-)))

(:derived (Flag336-)(and (Flag306-)))

(:derived (Flag336-)(and (Flag307-)))

(:derived (Flag336-)(and (Flag308-)))

(:derived (Flag336-)(and (Flag309-)))

(:derived (Flag336-)(and (Flag310-)))

(:derived (Flag336-)(and (Flag311-)))

(:derived (Flag336-)(and (Flag312-)))

(:derived (Flag336-)(and (Flag313-)))

(:derived (Flag336-)(and (Flag314-)))

(:derived (Flag336-)(and (Flag315-)))

(:derived (Flag336-)(and (Flag316-)))

(:derived (Flag336-)(and (Flag317-)))

(:derived (Flag336-)(and (Flag318-)))

(:derived (Flag336-)(and (Flag319-)))

(:derived (Flag336-)(and (Flag320-)))

(:derived (Flag336-)(and (Flag321-)))

(:derived (Flag336-)(and (Flag322-)))

(:derived (Flag336-)(and (Flag323-)))

(:derived (Flag336-)(and (Flag324-)))

(:derived (Flag336-)(and (Flag325-)))

(:derived (Flag336-)(and (Flag326-)))

(:derived (Flag336-)(and (Flag327-)))

(:derived (Flag336-)(and (Flag328-)))

(:derived (Flag336-)(and (Flag329-)))

(:derived (Flag336-)(and (Flag330-)))

(:derived (Flag336-)(and (Flag331-)))

(:derived (Flag336-)(and (Flag332-)))

(:derived (Flag336-)(and (Flag333-)))

(:derived (Flag336-)(and (Flag334-)))

(:derived (Flag336-)(and (Flag335-)))

(:derived (Flag355-)(and (Flag299-)))

(:derived (Flag355-)(and (Flag300-)))

(:derived (Flag355-)(and (Flag337-)))

(:derived (Flag355-)(and (Flag308-)))

(:derived (Flag355-)(and (Flag302-)))

(:derived (Flag355-)(and (Flag324-)))

(:derived (Flag355-)(and (Flag338-)))

(:derived (Flag355-)(and (Flag317-)))

(:derived (Flag355-)(and (Flag339-)))

(:derived (Flag355-)(and (Flag303-)))

(:derived (Flag355-)(and (Flag314-)))

(:derived (Flag355-)(and (Flag305-)))

(:derived (Flag355-)(and (Flag306-)))

(:derived (Flag355-)(and (Flag340-)))

(:derived (Flag355-)(and (Flag341-)))

(:derived (Flag355-)(and (Flag307-)))

(:derived (Flag355-)(and (Flag309-)))

(:derived (Flag355-)(and (Flag342-)))

(:derived (Flag355-)(and (Flag311-)))

(:derived (Flag355-)(and (Flag343-)))

(:derived (Flag355-)(and (Flag344-)))

(:derived (Flag355-)(and (Flag312-)))

(:derived (Flag355-)(and (Flag313-)))

(:derived (Flag355-)(and (Flag315-)))

(:derived (Flag355-)(and (Flag345-)))

(:derived (Flag355-)(and (Flag316-)))

(:derived (Flag355-)(and (Flag346-)))

(:derived (Flag355-)(and (Flag318-)))

(:derived (Flag355-)(and (Flag319-)))

(:derived (Flag355-)(and (Flag347-)))

(:derived (Flag355-)(and (Flag320-)))

(:derived (Flag355-)(and (Flag348-)))

(:derived (Flag355-)(and (Flag322-)))

(:derived (Flag355-)(and (Flag323-)))

(:derived (Flag355-)(and (Flag321-)))

(:derived (Flag355-)(and (Flag349-)))

(:derived (Flag355-)(and (Flag350-)))

(:derived (Flag355-)(and (Flag325-)))

(:derived (Flag355-)(and (Flag326-)))

(:derived (Flag355-)(and (Flag327-)))

(:derived (Flag355-)(and (Flag328-)))

(:derived (Flag355-)(and (Flag329-)))

(:derived (Flag355-)(and (Flag330-)))

(:derived (Flag355-)(and (Flag351-)))

(:derived (Flag355-)(and (Flag331-)))

(:derived (Flag355-)(and (Flag352-)))

(:derived (Flag355-)(and (Flag353-)))

(:derived (Flag355-)(and (Flag332-)))

(:derived (Flag355-)(and (Flag333-)))

(:derived (Flag355-)(and (Flag334-)))

(:derived (Flag355-)(and (Flag354-)))

(:derived (Flag355-)(and (Flag335-)))

(:derived (Flag374-)(and (Flag299-)))

(:derived (Flag374-)(and (Flag302-)))

(:derived (Flag374-)(and (Flag341-)))

(:derived (Flag374-)(and (Flag337-)))

(:derived (Flag374-)(and (Flag356-)))

(:derived (Flag374-)(and (Flag357-)))

(:derived (Flag374-)(and (Flag358-)))

(:derived (Flag374-)(and (Flag359-)))

(:derived (Flag374-)(and (Flag360-)))

(:derived (Flag374-)(and (Flag324-)))

(:derived (Flag374-)(and (Flag338-)))

(:derived (Flag374-)(and (Flag339-)))

(:derived (Flag374-)(and (Flag308-)))

(:derived (Flag374-)(and (Flag303-)))

(:derived (Flag374-)(and (Flag314-)))

(:derived (Flag374-)(and (Flag305-)))

(:derived (Flag374-)(and (Flag361-)))

(:derived (Flag374-)(and (Flag306-)))

(:derived (Flag374-)(and (Flag340-)))

(:derived (Flag374-)(and (Flag362-)))

(:derived (Flag374-)(and (Flag307-)))

(:derived (Flag374-)(and (Flag363-)))

(:derived (Flag374-)(and (Flag309-)))

(:derived (Flag374-)(and (Flag342-)))

(:derived (Flag374-)(and (Flag311-)))

(:derived (Flag374-)(and (Flag364-)))

(:derived (Flag374-)(and (Flag343-)))

(:derived (Flag374-)(and (Flag344-)))

(:derived (Flag374-)(and (Flag312-)))

(:derived (Flag374-)(and (Flag313-)))

(:derived (Flag374-)(and (Flag365-)))

(:derived (Flag374-)(and (Flag366-)))

(:derived (Flag374-)(and (Flag367-)))

(:derived (Flag374-)(and (Flag316-)))

(:derived (Flag374-)(and (Flag300-)))

(:derived (Flag374-)(and (Flag318-)))

(:derived (Flag374-)(and (Flag319-)))

(:derived (Flag374-)(and (Flag347-)))

(:derived (Flag374-)(and (Flag320-)))

(:derived (Flag374-)(and (Flag348-)))

(:derived (Flag374-)(and (Flag322-)))

(:derived (Flag374-)(and (Flag330-)))

(:derived (Flag374-)(and (Flag323-)))

(:derived (Flag374-)(and (Flag321-)))

(:derived (Flag374-)(and (Flag349-)))

(:derived (Flag374-)(and (Flag368-)))

(:derived (Flag374-)(and (Flag325-)))

(:derived (Flag374-)(and (Flag326-)))

(:derived (Flag374-)(and (Flag327-)))

(:derived (Flag374-)(and (Flag369-)))

(:derived (Flag374-)(and (Flag329-)))

(:derived (Flag374-)(and (Flag370-)))

(:derived (Flag374-)(and (Flag371-)))

(:derived (Flag374-)(and (Flag351-)))

(:derived (Flag374-)(and (Flag372-)))

(:derived (Flag374-)(and (Flag373-)))

(:derived (Flag374-)(and (Flag352-)))

(:derived (Flag374-)(and (Flag315-)))

(:derived (Flag374-)(and (Flag331-)))

(:derived (Flag374-)(and (Flag353-)))

(:derived (Flag374-)(and (Flag332-)))

(:derived (Flag374-)(and (Flag333-)))

(:derived (Flag374-)(and (Flag334-)))

(:derived (Flag374-)(and (Flag354-)))

(:derived (Flag374-)(and (Flag328-)))

(:derived (Flag374-)(and (Flag335-)))

(:derived (Flag391-)(and (Flag299-)))

(:derived (Flag391-)(and (Flag300-)))

(:derived (Flag391-)(and (Flag375-)))

(:derived (Flag391-)(and (Flag341-)))

(:derived (Flag391-)(and (Flag376-)))

(:derived (Flag391-)(and (Flag337-)))

(:derived (Flag391-)(and (Flag356-)))

(:derived (Flag391-)(and (Flag359-)))

(:derived (Flag391-)(and (Flag357-)))

(:derived (Flag391-)(and (Flag302-)))

(:derived (Flag391-)(and (Flag371-)))

(:derived (Flag391-)(and (Flag377-)))

(:derived (Flag391-)(and (Flag338-)))

(:derived (Flag391-)(and (Flag378-)))

(:derived (Flag391-)(and (Flag339-)))

(:derived (Flag391-)(and (Flag308-)))

(:derived (Flag391-)(and (Flag379-)))

(:derived (Flag391-)(and (Flag303-)))

(:derived (Flag391-)(and (Flag314-)))

(:derived (Flag391-)(and (Flag305-)))

(:derived (Flag391-)(and (Flag361-)))

(:derived (Flag391-)(and (Flag306-)))

(:derived (Flag391-)(and (Flag340-)))

(:derived (Flag391-)(and (Flag369-)))

(:derived (Flag391-)(and (Flag307-)))

(:derived (Flag391-)(and (Flag363-)))

(:derived (Flag391-)(and (Flag309-)))

(:derived (Flag391-)(and (Flag332-)))

(:derived (Flag391-)(and (Flag342-)))

(:derived (Flag391-)(and (Flag311-)))

(:derived (Flag391-)(and (Flag364-)))

(:derived (Flag391-)(and (Flag343-)))

(:derived (Flag391-)(and (Flag380-)))

(:derived (Flag391-)(and (Flag344-)))

(:derived (Flag391-)(and (Flag312-)))

(:derived (Flag391-)(and (Flag313-)))

(:derived (Flag391-)(and (Flag365-)))

(:derived (Flag391-)(and (Flag366-)))

(:derived (Flag391-)(and (Flag381-)))

(:derived (Flag391-)(and (Flag316-)))

(:derived (Flag391-)(and (Flag382-)))

(:derived (Flag391-)(and (Flag318-)))

(:derived (Flag391-)(and (Flag319-)))

(:derived (Flag391-)(and (Flag347-)))

(:derived (Flag391-)(and (Flag320-)))

(:derived (Flag391-)(and (Flag383-)))

(:derived (Flag391-)(and (Flag348-)))

(:derived (Flag391-)(and (Flag322-)))

(:derived (Flag391-)(and (Flag384-)))

(:derived (Flag391-)(and (Flag330-)))

(:derived (Flag391-)(and (Flag323-)))

(:derived (Flag391-)(and (Flag321-)))

(:derived (Flag391-)(and (Flag349-)))

(:derived (Flag391-)(and (Flag368-)))

(:derived (Flag391-)(and (Flag360-)))

(:derived (Flag391-)(and (Flag326-)))

(:derived (Flag391-)(and (Flag327-)))

(:derived (Flag391-)(and (Flag328-)))

(:derived (Flag391-)(and (Flag329-)))

(:derived (Flag391-)(and (Flag385-)))

(:derived (Flag391-)(and (Flag370-)))

(:derived (Flag391-)(and (Flag386-)))

(:derived (Flag391-)(and (Flag351-)))

(:derived (Flag391-)(and (Flag372-)))

(:derived (Flag391-)(and (Flag387-)))

(:derived (Flag391-)(and (Flag373-)))

(:derived (Flag391-)(and (Flag352-)))

(:derived (Flag391-)(and (Flag315-)))

(:derived (Flag391-)(and (Flag331-)))

(:derived (Flag391-)(and (Flag388-)))

(:derived (Flag391-)(and (Flag353-)))

(:derived (Flag391-)(and (Flag389-)))

(:derived (Flag391-)(and (Flag333-)))

(:derived (Flag391-)(and (Flag334-)))

(:derived (Flag391-)(and (Flag354-)))

(:derived (Flag391-)(and (Flag362-)))

(:derived (Flag391-)(and (Flag324-)))

(:derived (Flag391-)(and (Flag390-)))

(:derived (Flag408-)(and (Flag386-)))

(:derived (Flag408-)(and (Flag302-)))

(:derived (Flag408-)(and (Flag337-)))

(:derived (Flag408-)(and (Flag305-)))

(:derived (Flag408-)(and (Flag378-)))

(:derived (Flag408-)(and (Flag307-)))

(:derived (Flag408-)(and (Flag309-)))

(:derived (Flag408-)(and (Flag342-)))

(:derived (Flag408-)(and (Flag359-)))

(:derived (Flag408-)(and (Flag392-)))

(:derived (Flag408-)(and (Flag343-)))

(:derived (Flag408-)(and (Flag380-)))

(:derived (Flag408-)(and (Flag388-)))

(:derived (Flag408-)(and (Flag322-)))

(:derived (Flag408-)(and (Flag393-)))

(:derived (Flag408-)(and (Flag318-)))

(:derived (Flag408-)(and (Flag344-)))

(:derived (Flag408-)(and (Flag394-)))

(:derived (Flag408-)(and (Flag349-)))

(:derived (Flag408-)(and (Flag368-)))

(:derived (Flag408-)(and (Flag377-)))

(:derived (Flag408-)(and (Flag326-)))

(:derived (Flag408-)(and (Flag300-)))

(:derived (Flag408-)(and (Flag385-)))

(:derived (Flag408-)(and (Flag387-)))

(:derived (Flag408-)(and (Flag319-)))

(:derived (Flag408-)(and (Flag354-)))

(:derived (Flag408-)(and (Flag308-)))

(:derived (Flag408-)(and (Flag357-)))

(:derived (Flag408-)(and (Flag395-)))

(:derived (Flag408-)(and (Flag339-)))

(:derived (Flag408-)(and (Flag396-)))

(:derived (Flag408-)(and (Flag397-)))

(:derived (Flag408-)(and (Flag341-)))

(:derived (Flag408-)(and (Flag311-)))

(:derived (Flag408-)(and (Flag381-)))

(:derived (Flag408-)(and (Flag312-)))

(:derived (Flag408-)(and (Flag363-)))

(:derived (Flag408-)(and (Flag321-)))

(:derived (Flag408-)(and (Flag323-)))

(:derived (Flag408-)(and (Flag398-)))

(:derived (Flag408-)(and (Flag360-)))

(:derived (Flag408-)(and (Flag327-)))

(:derived (Flag408-)(and (Flag369-)))

(:derived (Flag408-)(and (Flag370-)))

(:derived (Flag408-)(and (Flag331-)))

(:derived (Flag408-)(and (Flag352-)))

(:derived (Flag408-)(and (Flag399-)))

(:derived (Flag408-)(and (Flag389-)))

(:derived (Flag408-)(and (Flag333-)))

(:derived (Flag408-)(and (Flag334-)))

(:derived (Flag408-)(and (Flag299-)))

(:derived (Flag408-)(and (Flag375-)))

(:derived (Flag408-)(and (Flag400-)))

(:derived (Flag408-)(and (Flag401-)))

(:derived (Flag408-)(and (Flag379-)))

(:derived (Flag408-)(and (Flag303-)))

(:derived (Flag408-)(and (Flag402-)))

(:derived (Flag408-)(and (Flag340-)))

(:derived (Flag408-)(and (Flag356-)))

(:derived (Flag408-)(and (Flag371-)))

(:derived (Flag408-)(and (Flag365-)))

(:derived (Flag408-)(and (Flag315-)))

(:derived (Flag408-)(and (Flag316-)))

(:derived (Flag408-)(and (Flag347-)))

(:derived (Flag408-)(and (Flag320-)))

(:derived (Flag408-)(and (Flag384-)))

(:derived (Flag408-)(and (Flag330-)))

(:derived (Flag408-)(and (Flag403-)))

(:derived (Flag408-)(and (Flag404-)))

(:derived (Flag408-)(and (Flag328-)))

(:derived (Flag408-)(and (Flag382-)))

(:derived (Flag408-)(and (Flag372-)))

(:derived (Flag408-)(and (Flag353-)))

(:derived (Flag408-)(and (Flag405-)))

(:derived (Flag408-)(and (Flag373-)))

(:derived (Flag408-)(and (Flag406-)))

(:derived (Flag408-)(and (Flag314-)))

(:derived (Flag408-)(and (Flag306-)))

(:derived (Flag408-)(and (Flag362-)))

(:derived (Flag408-)(and (Flag332-)))

(:derived (Flag408-)(and (Flag407-)))

(:derived (Flag408-)(and (Flag364-)))

(:derived (Flag408-)(and (Flag390-)))

(:derived (Flag408-)(and (Flag366-)))

(:derived (Flag408-)(and (Flag338-)))

(:derived (Flag408-)(and (Flag324-)))

(:derived (Flag408-)(and (Flag351-)))

(:derived (Flag425-)(and (Flag409-)))

(:derived (Flag425-)(and (Flag386-)))

(:derived (Flag425-)(and (Flag302-)))

(:derived (Flag425-)(and (Flag305-)))

(:derived (Flag425-)(and (Flag378-)))

(:derived (Flag425-)(and (Flag307-)))

(:derived (Flag425-)(and (Flag309-)))

(:derived (Flag425-)(and (Flag342-)))

(:derived (Flag425-)(and (Flag359-)))

(:derived (Flag425-)(and (Flag343-)))

(:derived (Flag425-)(and (Flag380-)))

(:derived (Flag425-)(and (Flag388-)))

(:derived (Flag425-)(and (Flag322-)))

(:derived (Flag425-)(and (Flag393-)))

(:derived (Flag425-)(and (Flag410-)))

(:derived (Flag425-)(and (Flag411-)))

(:derived (Flag425-)(and (Flag318-)))

(:derived (Flag425-)(and (Flag344-)))

(:derived (Flag425-)(and (Flag349-)))

(:derived (Flag425-)(and (Flag368-)))

(:derived (Flag425-)(and (Flag377-)))

(:derived (Flag425-)(and (Flag326-)))

(:derived (Flag425-)(and (Flag300-)))

(:derived (Flag425-)(and (Flag385-)))

(:derived (Flag425-)(and (Flag412-)))

(:derived (Flag425-)(and (Flag387-)))

(:derived (Flag425-)(and (Flag319-)))

(:derived (Flag425-)(and (Flag354-)))

(:derived (Flag425-)(and (Flag308-)))

(:derived (Flag425-)(and (Flag357-)))

(:derived (Flag425-)(and (Flag395-)))

(:derived (Flag425-)(and (Flag413-)))

(:derived (Flag425-)(and (Flag339-)))

(:derived (Flag425-)(and (Flag396-)))

(:derived (Flag425-)(and (Flag397-)))

(:derived (Flag425-)(and (Flag414-)))

(:derived (Flag425-)(and (Flag341-)))

(:derived (Flag425-)(and (Flag381-)))

(:derived (Flag425-)(and (Flag312-)))

(:derived (Flag425-)(and (Flag415-)))

(:derived (Flag425-)(and (Flag363-)))

(:derived (Flag425-)(and (Flag321-)))

(:derived (Flag425-)(and (Flag323-)))

(:derived (Flag425-)(and (Flag398-)))

(:derived (Flag425-)(and (Flag360-)))

(:derived (Flag425-)(and (Flag327-)))

(:derived (Flag425-)(and (Flag369-)))

(:derived (Flag425-)(and (Flag370-)))

(:derived (Flag425-)(and (Flag331-)))

(:derived (Flag425-)(and (Flag352-)))

(:derived (Flag425-)(and (Flag399-)))

(:derived (Flag425-)(and (Flag416-)))

(:derived (Flag425-)(and (Flag389-)))

(:derived (Flag425-)(and (Flag333-)))

(:derived (Flag425-)(and (Flag334-)))

(:derived (Flag425-)(and (Flag299-)))

(:derived (Flag425-)(and (Flag375-)))

(:derived (Flag425-)(and (Flag417-)))

(:derived (Flag425-)(and (Flag400-)))

(:derived (Flag425-)(and (Flag418-)))

(:derived (Flag425-)(and (Flag401-)))

(:derived (Flag425-)(and (Flag379-)))

(:derived (Flag425-)(and (Flag303-)))

(:derived (Flag425-)(and (Flag340-)))

(:derived (Flag425-)(and (Flag356-)))

(:derived (Flag425-)(and (Flag371-)))

(:derived (Flag425-)(and (Flag365-)))

(:derived (Flag425-)(and (Flag315-)))

(:derived (Flag425-)(and (Flag316-)))

(:derived (Flag425-)(and (Flag347-)))

(:derived (Flag425-)(and (Flag320-)))

(:derived (Flag425-)(and (Flag384-)))

(:derived (Flag425-)(and (Flag330-)))

(:derived (Flag425-)(and (Flag403-)))

(:derived (Flag425-)(and (Flag404-)))

(:derived (Flag425-)(and (Flag328-)))

(:derived (Flag425-)(and (Flag382-)))

(:derived (Flag425-)(and (Flag372-)))

(:derived (Flag425-)(and (Flag419-)))

(:derived (Flag425-)(and (Flag420-)))

(:derived (Flag425-)(and (Flag421-)))

(:derived (Flag425-)(and (Flag353-)))

(:derived (Flag425-)(and (Flag405-)))

(:derived (Flag425-)(and (Flag373-)))

(:derived (Flag425-)(and (Flag422-)))

(:derived (Flag425-)(and (Flag406-)))

(:derived (Flag425-)(and (Flag314-)))

(:derived (Flag425-)(and (Flag423-)))

(:derived (Flag425-)(and (Flag306-)))

(:derived (Flag425-)(and (Flag362-)))

(:derived (Flag425-)(and (Flag407-)))

(:derived (Flag425-)(and (Flag364-)))

(:derived (Flag425-)(and (Flag390-)))

(:derived (Flag425-)(and (Flag338-)))

(:derived (Flag425-)(and (Flag324-)))

(:derived (Flag425-)(and (Flag424-)))

(:derived (Flag425-)(and (Flag351-)))

(:derived (Flag439-)(and (Flag386-)))

(:derived (Flag439-)(and (Flag302-)))

(:derived (Flag439-)(and (Flag305-)))

(:derived (Flag439-)(and (Flag378-)))

(:derived (Flag439-)(and (Flag307-)))

(:derived (Flag439-)(and (Flag309-)))

(:derived (Flag439-)(and (Flag417-)))

(:derived (Flag439-)(and (Flag359-)))

(:derived (Flag439-)(and (Flag343-)))

(:derived (Flag439-)(and (Flag380-)))

(:derived (Flag439-)(and (Flag388-)))

(:derived (Flag439-)(and (Flag322-)))

(:derived (Flag439-)(and (Flag393-)))

(:derived (Flag439-)(and (Flag410-)))

(:derived (Flag439-)(and (Flag411-)))

(:derived (Flag439-)(and (Flag318-)))

(:derived (Flag439-)(and (Flag344-)))

(:derived (Flag439-)(and (Flag349-)))

(:derived (Flag439-)(and (Flag368-)))

(:derived (Flag439-)(and (Flag377-)))

(:derived (Flag439-)(and (Flag326-)))

(:derived (Flag439-)(and (Flag300-)))

(:derived (Flag439-)(and (Flag385-)))

(:derived (Flag439-)(and (Flag387-)))

(:derived (Flag439-)(and (Flag319-)))

(:derived (Flag439-)(and (Flag354-)))

(:derived (Flag439-)(and (Flag426-)))

(:derived (Flag439-)(and (Flag427-)))

(:derived (Flag439-)(and (Flag308-)))

(:derived (Flag439-)(and (Flag428-)))

(:derived (Flag439-)(and (Flag357-)))

(:derived (Flag439-)(and (Flag395-)))

(:derived (Flag439-)(and (Flag429-)))

(:derived (Flag439-)(and (Flag413-)))

(:derived (Flag439-)(and (Flag339-)))

(:derived (Flag439-)(and (Flag396-)))

(:derived (Flag439-)(and (Flag397-)))

(:derived (Flag439-)(and (Flag414-)))

(:derived (Flag439-)(and (Flag341-)))

(:derived (Flag439-)(and (Flag430-)))

(:derived (Flag439-)(and (Flag381-)))

(:derived (Flag439-)(and (Flag431-)))

(:derived (Flag439-)(and (Flag415-)))

(:derived (Flag439-)(and (Flag363-)))

(:derived (Flag439-)(and (Flag321-)))

(:derived (Flag439-)(and (Flag432-)))

(:derived (Flag439-)(and (Flag323-)))

(:derived (Flag439-)(and (Flag398-)))

(:derived (Flag439-)(and (Flag327-)))

(:derived (Flag439-)(and (Flag369-)))

(:derived (Flag439-)(and (Flag370-)))

(:derived (Flag439-)(and (Flag331-)))

(:derived (Flag439-)(and (Flag352-)))

(:derived (Flag439-)(and (Flag399-)))

(:derived (Flag439-)(and (Flag433-)))

(:derived (Flag439-)(and (Flag416-)))

(:derived (Flag439-)(and (Flag333-)))

(:derived (Flag439-)(and (Flag334-)))

(:derived (Flag439-)(and (Flag299-)))

(:derived (Flag439-)(and (Flag375-)))

(:derived (Flag439-)(and (Flag342-)))

(:derived (Flag439-)(and (Flag400-)))

(:derived (Flag439-)(and (Flag418-)))

(:derived (Flag439-)(and (Flag401-)))

(:derived (Flag439-)(and (Flag379-)))

(:derived (Flag439-)(and (Flag303-)))

(:derived (Flag439-)(and (Flag340-)))

(:derived (Flag439-)(and (Flag356-)))

(:derived (Flag439-)(and (Flag371-)))

(:derived (Flag439-)(and (Flag365-)))

(:derived (Flag439-)(and (Flag315-)))

(:derived (Flag439-)(and (Flag312-)))

(:derived (Flag439-)(and (Flag434-)))

(:derived (Flag439-)(and (Flag347-)))

(:derived (Flag439-)(and (Flag435-)))

(:derived (Flag439-)(and (Flag330-)))

(:derived (Flag439-)(and (Flag403-)))

(:derived (Flag439-)(and (Flag404-)))

(:derived (Flag439-)(and (Flag328-)))

(:derived (Flag439-)(and (Flag382-)))

(:derived (Flag439-)(and (Flag372-)))

(:derived (Flag439-)(and (Flag419-)))

(:derived (Flag439-)(and (Flag420-)))

(:derived (Flag439-)(and (Flag421-)))

(:derived (Flag439-)(and (Flag353-)))

(:derived (Flag439-)(and (Flag405-)))

(:derived (Flag439-)(and (Flag436-)))

(:derived (Flag439-)(and (Flag373-)))

(:derived (Flag439-)(and (Flag422-)))

(:derived (Flag439-)(and (Flag406-)))

(:derived (Flag439-)(and (Flag314-)))

(:derived (Flag439-)(and (Flag306-)))

(:derived (Flag439-)(and (Flag362-)))

(:derived (Flag439-)(and (Flag407-)))

(:derived (Flag439-)(and (Flag364-)))

(:derived (Flag439-)(and (Flag437-)))

(:derived (Flag439-)(and (Flag390-)))

(:derived (Flag439-)(and (Flag324-)))

(:derived (Flag439-)(and (Flag438-)))

(:derived (Flag439-)(and (Flag424-)))

(:derived (Flag439-)(and (Flag384-)))

(:derived (Flag439-)(and (Flag351-)))

(:derived (Flag453-)(and (Flag386-)))

(:derived (Flag453-)(and (Flag302-)))

(:derived (Flag453-)(and (Flag378-)))

(:derived (Flag453-)(and (Flag307-)))

(:derived (Flag453-)(and (Flag417-)))

(:derived (Flag453-)(and (Flag359-)))

(:derived (Flag453-)(and (Flag343-)))

(:derived (Flag453-)(and (Flag380-)))

(:derived (Flag453-)(and (Flag388-)))

(:derived (Flag453-)(and (Flag322-)))

(:derived (Flag453-)(and (Flag393-)))

(:derived (Flag453-)(and (Flag410-)))

(:derived (Flag453-)(and (Flag440-)))

(:derived (Flag453-)(and (Flag411-)))

(:derived (Flag453-)(and (Flag318-)))

(:derived (Flag453-)(and (Flag344-)))

(:derived (Flag453-)(and (Flag349-)))

(:derived (Flag453-)(and (Flag368-)))

(:derived (Flag453-)(and (Flag377-)))

(:derived (Flag453-)(and (Flag326-)))

(:derived (Flag453-)(and (Flag441-)))

(:derived (Flag453-)(and (Flag300-)))

(:derived (Flag453-)(and (Flag385-)))

(:derived (Flag453-)(and (Flag442-)))

(:derived (Flag453-)(and (Flag387-)))

(:derived (Flag453-)(and (Flag319-)))

(:derived (Flag453-)(and (Flag354-)))

(:derived (Flag453-)(and (Flag426-)))

(:derived (Flag453-)(and (Flag427-)))

(:derived (Flag453-)(and (Flag443-)))

(:derived (Flag453-)(and (Flag308-)))

(:derived (Flag453-)(and (Flag428-)))

(:derived (Flag453-)(and (Flag357-)))

(:derived (Flag453-)(and (Flag395-)))

(:derived (Flag453-)(and (Flag429-)))

(:derived (Flag453-)(and (Flag413-)))

(:derived (Flag453-)(and (Flag444-)))

(:derived (Flag453-)(and (Flag396-)))

(:derived (Flag453-)(and (Flag397-)))

(:derived (Flag453-)(and (Flag414-)))

(:derived (Flag453-)(and (Flag341-)))

(:derived (Flag453-)(and (Flag445-)))

(:derived (Flag453-)(and (Flag430-)))

(:derived (Flag453-)(and (Flag381-)))

(:derived (Flag453-)(and (Flag431-)))

(:derived (Flag453-)(and (Flag405-)))

(:derived (Flag453-)(and (Flag415-)))

(:derived (Flag453-)(and (Flag363-)))

(:derived (Flag453-)(and (Flag321-)))

(:derived (Flag453-)(and (Flag432-)))

(:derived (Flag453-)(and (Flag323-)))

(:derived (Flag453-)(and (Flag398-)))

(:derived (Flag453-)(and (Flag340-)))

(:derived (Flag453-)(and (Flag327-)))

(:derived (Flag453-)(and (Flag369-)))

(:derived (Flag453-)(and (Flag370-)))

(:derived (Flag453-)(and (Flag331-)))

(:derived (Flag453-)(and (Flag352-)))

(:derived (Flag453-)(and (Flag399-)))

(:derived (Flag453-)(and (Flag416-)))

(:derived (Flag453-)(and (Flag446-)))

(:derived (Flag453-)(and (Flag333-)))

(:derived (Flag453-)(and (Flag334-)))

(:derived (Flag453-)(and (Flag299-)))

(:derived (Flag453-)(and (Flag375-)))

(:derived (Flag453-)(and (Flag342-)))

(:derived (Flag453-)(and (Flag400-)))

(:derived (Flag453-)(and (Flag418-)))

(:derived (Flag453-)(and (Flag401-)))

(:derived (Flag453-)(and (Flag447-)))

(:derived (Flag453-)(and (Flag379-)))

(:derived (Flag453-)(and (Flag303-)))

(:derived (Flag453-)(and (Flag448-)))

(:derived (Flag453-)(and (Flag356-)))

(:derived (Flag453-)(and (Flag365-)))

(:derived (Flag453-)(and (Flag315-)))

(:derived (Flag453-)(and (Flag312-)))

(:derived (Flag453-)(and (Flag347-)))

(:derived (Flag453-)(and (Flag330-)))

(:derived (Flag453-)(and (Flag403-)))

(:derived (Flag453-)(and (Flag404-)))

(:derived (Flag453-)(and (Flag328-)))

(:derived (Flag453-)(and (Flag382-)))

(:derived (Flag453-)(and (Flag372-)))

(:derived (Flag453-)(and (Flag419-)))

(:derived (Flag453-)(and (Flag420-)))

(:derived (Flag453-)(and (Flag421-)))

(:derived (Flag453-)(and (Flag353-)))

(:derived (Flag453-)(and (Flag449-)))

(:derived (Flag453-)(and (Flag436-)))

(:derived (Flag453-)(and (Flag373-)))

(:derived (Flag453-)(and (Flag422-)))

(:derived (Flag453-)(and (Flag406-)))

(:derived (Flag453-)(and (Flag314-)))

(:derived (Flag453-)(and (Flag306-)))

(:derived (Flag453-)(and (Flag362-)))

(:derived (Flag453-)(and (Flag364-)))

(:derived (Flag453-)(and (Flag437-)))

(:derived (Flag453-)(and (Flag390-)))

(:derived (Flag453-)(and (Flag324-)))

(:derived (Flag453-)(and (Flag438-)))

(:derived (Flag453-)(and (Flag424-)))

(:derived (Flag453-)(and (Flag450-)))

(:derived (Flag453-)(and (Flag451-)))

(:derived (Flag453-)(and (Flag351-)))

(:derived (Flag453-)(and (Flag452-)))

(:derived (Flag465-)(and (Flag341-)))

(:derived (Flag465-)(and (Flag386-)))

(:derived (Flag465-)(and (Flag302-)))

(:derived (Flag465-)(and (Flag307-)))

(:derived (Flag465-)(and (Flag417-)))

(:derived (Flag465-)(and (Flag359-)))

(:derived (Flag465-)(and (Flag380-)))

(:derived (Flag465-)(and (Flag388-)))

(:derived (Flag465-)(and (Flag322-)))

(:derived (Flag465-)(and (Flag393-)))

(:derived (Flag465-)(and (Flag410-)))

(:derived (Flag465-)(and (Flag440-)))

(:derived (Flag465-)(and (Flag411-)))

(:derived (Flag465-)(and (Flag454-)))

(:derived (Flag465-)(and (Flag318-)))

(:derived (Flag465-)(and (Flag344-)))

(:derived (Flag465-)(and (Flag455-)))

(:derived (Flag465-)(and (Flag456-)))

(:derived (Flag465-)(and (Flag349-)))

(:derived (Flag465-)(and (Flag368-)))

(:derived (Flag465-)(and (Flag377-)))

(:derived (Flag465-)(and (Flag326-)))

(:derived (Flag465-)(and (Flag441-)))

(:derived (Flag465-)(and (Flag300-)))

(:derived (Flag465-)(and (Flag385-)))

(:derived (Flag465-)(and (Flag442-)))

(:derived (Flag465-)(and (Flag387-)))

(:derived (Flag465-)(and (Flag319-)))

(:derived (Flag465-)(and (Flag354-)))

(:derived (Flag465-)(and (Flag426-)))

(:derived (Flag465-)(and (Flag427-)))

(:derived (Flag465-)(and (Flag443-)))

(:derived (Flag465-)(and (Flag308-)))

(:derived (Flag465-)(and (Flag428-)))

(:derived (Flag465-)(and (Flag357-)))

(:derived (Flag465-)(and (Flag395-)))

(:derived (Flag465-)(and (Flag429-)))

(:derived (Flag465-)(and (Flag413-)))

(:derived (Flag465-)(and (Flag444-)))

(:derived (Flag465-)(and (Flag396-)))

(:derived (Flag465-)(and (Flag397-)))

(:derived (Flag465-)(and (Flag414-)))

(:derived (Flag465-)(and (Flag457-)))

(:derived (Flag465-)(and (Flag445-)))

(:derived (Flag465-)(and (Flag430-)))

(:derived (Flag465-)(and (Flag381-)))

(:derived (Flag465-)(and (Flag431-)))

(:derived (Flag465-)(and (Flag405-)))

(:derived (Flag465-)(and (Flag415-)))

(:derived (Flag465-)(and (Flag363-)))

(:derived (Flag465-)(and (Flag321-)))

(:derived (Flag465-)(and (Flag432-)))

(:derived (Flag465-)(and (Flag323-)))

(:derived (Flag465-)(and (Flag398-)))

(:derived (Flag465-)(and (Flag327-)))

(:derived (Flag465-)(and (Flag369-)))

(:derived (Flag465-)(and (Flag458-)))

(:derived (Flag465-)(and (Flag370-)))

(:derived (Flag465-)(and (Flag331-)))

(:derived (Flag465-)(and (Flag352-)))

(:derived (Flag465-)(and (Flag399-)))

(:derived (Flag465-)(and (Flag416-)))

(:derived (Flag465-)(and (Flag459-)))

(:derived (Flag465-)(and (Flag299-)))

(:derived (Flag465-)(and (Flag375-)))

(:derived (Flag465-)(and (Flag342-)))

(:derived (Flag465-)(and (Flag400-)))

(:derived (Flag465-)(and (Flag418-)))

(:derived (Flag465-)(and (Flag401-)))

(:derived (Flag465-)(and (Flag447-)))

(:derived (Flag465-)(and (Flag379-)))

(:derived (Flag465-)(and (Flag303-)))

(:derived (Flag465-)(and (Flag340-)))

(:derived (Flag465-)(and (Flag460-)))

(:derived (Flag465-)(and (Flag356-)))

(:derived (Flag465-)(and (Flag365-)))

(:derived (Flag465-)(and (Flag315-)))

(:derived (Flag465-)(and (Flag312-)))

(:derived (Flag465-)(and (Flag330-)))

(:derived (Flag465-)(and (Flag404-)))

(:derived (Flag465-)(and (Flag382-)))

(:derived (Flag465-)(and (Flag372-)))

(:derived (Flag465-)(and (Flag419-)))

(:derived (Flag465-)(and (Flag421-)))

(:derived (Flag465-)(and (Flag353-)))

(:derived (Flag465-)(and (Flag449-)))

(:derived (Flag465-)(and (Flag436-)))

(:derived (Flag465-)(and (Flag373-)))

(:derived (Flag465-)(and (Flag422-)))

(:derived (Flag465-)(and (Flag406-)))

(:derived (Flag465-)(and (Flag461-)))

(:derived (Flag465-)(and (Flag306-)))

(:derived (Flag465-)(and (Flag362-)))

(:derived (Flag465-)(and (Flag462-)))

(:derived (Flag465-)(and (Flag463-)))

(:derived (Flag465-)(and (Flag343-)))

(:derived (Flag465-)(and (Flag437-)))

(:derived (Flag465-)(and (Flag314-)))

(:derived (Flag465-)(and (Flag390-)))

(:derived (Flag465-)(and (Flag333-)))

(:derived (Flag465-)(and (Flag324-)))

(:derived (Flag465-)(and (Flag438-)))

(:derived (Flag465-)(and (Flag424-)))

(:derived (Flag465-)(and (Flag464-)))

(:derived (Flag465-)(and (Flag450-)))

(:derived (Flag465-)(and (Flag351-)))

(:derived (Flag465-)(and (Flag452-)))

(:derived (Flag475-)(and (Flag341-)))

(:derived (Flag475-)(and (Flag386-)))

(:derived (Flag475-)(and (Flag302-)))

(:derived (Flag475-)(and (Flag307-)))

(:derived (Flag475-)(and (Flag417-)))

(:derived (Flag475-)(and (Flag359-)))

(:derived (Flag475-)(and (Flag380-)))

(:derived (Flag475-)(and (Flag388-)))

(:derived (Flag475-)(and (Flag322-)))

(:derived (Flag475-)(and (Flag393-)))

(:derived (Flag475-)(and (Flag410-)))

(:derived (Flag475-)(and (Flag440-)))

(:derived (Flag475-)(and (Flag411-)))

(:derived (Flag475-)(and (Flag454-)))

(:derived (Flag475-)(and (Flag344-)))

(:derived (Flag475-)(and (Flag456-)))

(:derived (Flag475-)(and (Flag349-)))

(:derived (Flag475-)(and (Flag368-)))

(:derived (Flag475-)(and (Flag377-)))

(:derived (Flag475-)(and (Flag441-)))

(:derived (Flag475-)(and (Flag300-)))

(:derived (Flag475-)(and (Flag455-)))

(:derived (Flag475-)(and (Flag442-)))

(:derived (Flag475-)(and (Flag387-)))

(:derived (Flag475-)(and (Flag319-)))

(:derived (Flag475-)(and (Flag354-)))

(:derived (Flag475-)(and (Flag426-)))

(:derived (Flag475-)(and (Flag427-)))

(:derived (Flag475-)(and (Flag443-)))

(:derived (Flag475-)(and (Flag308-)))

(:derived (Flag475-)(and (Flag466-)))

(:derived (Flag475-)(and (Flag428-)))

(:derived (Flag475-)(and (Flag357-)))

(:derived (Flag475-)(and (Flag467-)))

(:derived (Flag475-)(and (Flag395-)))

(:derived (Flag475-)(and (Flag429-)))

(:derived (Flag475-)(and (Flag413-)))

(:derived (Flag475-)(and (Flag444-)))

(:derived (Flag475-)(and (Flag396-)))

(:derived (Flag475-)(and (Flag397-)))

(:derived (Flag475-)(and (Flag414-)))

(:derived (Flag475-)(and (Flag457-)))

(:derived (Flag475-)(and (Flag445-)))

(:derived (Flag475-)(and (Flag430-)))

(:derived (Flag475-)(and (Flag381-)))

(:derived (Flag475-)(and (Flag431-)))

(:derived (Flag475-)(and (Flag405-)))

(:derived (Flag475-)(and (Flag415-)))

(:derived (Flag475-)(and (Flag468-)))

(:derived (Flag475-)(and (Flag363-)))

(:derived (Flag475-)(and (Flag321-)))

(:derived (Flag475-)(and (Flag469-)))

(:derived (Flag475-)(and (Flag323-)))

(:derived (Flag475-)(and (Flag398-)))

(:derived (Flag475-)(and (Flag327-)))

(:derived (Flag475-)(and (Flag369-)))

(:derived (Flag475-)(and (Flag370-)))

(:derived (Flag475-)(and (Flag331-)))

(:derived (Flag475-)(and (Flag352-)))

(:derived (Flag475-)(and (Flag416-)))

(:derived (Flag475-)(and (Flag459-)))

(:derived (Flag475-)(and (Flag299-)))

(:derived (Flag475-)(and (Flag375-)))

(:derived (Flag475-)(and (Flag342-)))

(:derived (Flag475-)(and (Flag400-)))

(:derived (Flag475-)(and (Flag418-)))

(:derived (Flag475-)(and (Flag401-)))

(:derived (Flag475-)(and (Flag447-)))

(:derived (Flag475-)(and (Flag379-)))

(:derived (Flag475-)(and (Flag303-)))

(:derived (Flag475-)(and (Flag340-)))

(:derived (Flag475-)(and (Flag460-)))

(:derived (Flag475-)(and (Flag470-)))

(:derived (Flag475-)(and (Flag471-)))

(:derived (Flag475-)(and (Flag315-)))

(:derived (Flag475-)(and (Flag312-)))

(:derived (Flag475-)(and (Flag330-)))

(:derived (Flag475-)(and (Flag382-)))

(:derived (Flag475-)(and (Flag365-)))

(:derived (Flag475-)(and (Flag372-)))

(:derived (Flag475-)(and (Flag419-)))

(:derived (Flag475-)(and (Flag421-)))

(:derived (Flag475-)(and (Flag353-)))

(:derived (Flag475-)(and (Flag449-)))

(:derived (Flag475-)(and (Flag436-)))

(:derived (Flag475-)(and (Flag373-)))

(:derived (Flag475-)(and (Flag472-)))

(:derived (Flag475-)(and (Flag406-)))

(:derived (Flag475-)(and (Flag473-)))

(:derived (Flag475-)(and (Flag314-)))

(:derived (Flag475-)(and (Flag306-)))

(:derived (Flag475-)(and (Flag362-)))

(:derived (Flag475-)(and (Flag462-)))

(:derived (Flag475-)(and (Flag463-)))

(:derived (Flag475-)(and (Flag437-)))

(:derived (Flag475-)(and (Flag390-)))

(:derived (Flag475-)(and (Flag333-)))

(:derived (Flag475-)(and (Flag324-)))

(:derived (Flag475-)(and (Flag438-)))

(:derived (Flag475-)(and (Flag424-)))

(:derived (Flag475-)(and (Flag464-)))

(:derived (Flag475-)(and (Flag450-)))

(:derived (Flag475-)(and (Flag351-)))

(:derived (Flag475-)(and (Flag474-)))

(:derived (Flag475-)(and (Flag452-)))

(:derived (Flag484-)(and (Flag476-)))

(:derived (Flag484-)(and (Flag386-)))

(:derived (Flag484-)(and (Flag302-)))

(:derived (Flag484-)(and (Flag307-)))

(:derived (Flag484-)(and (Flag342-)))

(:derived (Flag484-)(and (Flag359-)))

(:derived (Flag484-)(and (Flag388-)))

(:derived (Flag484-)(and (Flag322-)))

(:derived (Flag484-)(and (Flag393-)))

(:derived (Flag484-)(and (Flag410-)))

(:derived (Flag484-)(and (Flag477-)))

(:derived (Flag484-)(and (Flag440-)))

(:derived (Flag484-)(and (Flag411-)))

(:derived (Flag484-)(and (Flag344-)))

(:derived (Flag484-)(and (Flag456-)))

(:derived (Flag484-)(and (Flag377-)))

(:derived (Flag484-)(and (Flag441-)))

(:derived (Flag484-)(and (Flag300-)))

(:derived (Flag484-)(and (Flag455-)))

(:derived (Flag484-)(and (Flag442-)))

(:derived (Flag484-)(and (Flag387-)))

(:derived (Flag484-)(and (Flag319-)))

(:derived (Flag484-)(and (Flag354-)))

(:derived (Flag484-)(and (Flag426-)))

(:derived (Flag484-)(and (Flag427-)))

(:derived (Flag484-)(and (Flag443-)))

(:derived (Flag484-)(and (Flag308-)))

(:derived (Flag484-)(and (Flag466-)))

(:derived (Flag484-)(and (Flag428-)))

(:derived (Flag484-)(and (Flag357-)))

(:derived (Flag484-)(and (Flag467-)))

(:derived (Flag484-)(and (Flag395-)))

(:derived (Flag484-)(and (Flag429-)))

(:derived (Flag484-)(and (Flag413-)))

(:derived (Flag484-)(and (Flag396-)))

(:derived (Flag484-)(and (Flag397-)))

(:derived (Flag484-)(and (Flag478-)))

(:derived (Flag484-)(and (Flag414-)))

(:derived (Flag484-)(and (Flag457-)))

(:derived (Flag484-)(and (Flag445-)))

(:derived (Flag484-)(and (Flag381-)))

(:derived (Flag484-)(and (Flag431-)))

(:derived (Flag484-)(and (Flag405-)))

(:derived (Flag484-)(and (Flag468-)))

(:derived (Flag484-)(and (Flag363-)))

(:derived (Flag484-)(and (Flag321-)))

(:derived (Flag484-)(and (Flag469-)))

(:derived (Flag484-)(and (Flag398-)))

(:derived (Flag484-)(and (Flag369-)))

(:derived (Flag484-)(and (Flag370-)))

(:derived (Flag484-)(and (Flag479-)))

(:derived (Flag484-)(and (Flag331-)))

(:derived (Flag484-)(and (Flag352-)))

(:derived (Flag484-)(and (Flag416-)))

(:derived (Flag484-)(and (Flag459-)))

(:derived (Flag484-)(and (Flag299-)))

(:derived (Flag484-)(and (Flag375-)))

(:derived (Flag484-)(and (Flag400-)))

(:derived (Flag484-)(and (Flag418-)))

(:derived (Flag484-)(and (Flag401-)))

(:derived (Flag484-)(and (Flag447-)))

(:derived (Flag484-)(and (Flag379-)))

(:derived (Flag484-)(and (Flag303-)))

(:derived (Flag484-)(and (Flag340-)))

(:derived (Flag484-)(and (Flag460-)))

(:derived (Flag484-)(and (Flag470-)))

(:derived (Flag484-)(and (Flag471-)))

(:derived (Flag484-)(and (Flag315-)))

(:derived (Flag484-)(and (Flag312-)))

(:derived (Flag484-)(and (Flag480-)))

(:derived (Flag484-)(and (Flag330-)))

(:derived (Flag484-)(and (Flag382-)))

(:derived (Flag484-)(and (Flag365-)))

(:derived (Flag484-)(and (Flag372-)))

(:derived (Flag484-)(and (Flag419-)))

(:derived (Flag484-)(and (Flag421-)))

(:derived (Flag484-)(and (Flag341-)))

(:derived (Flag484-)(and (Flag481-)))

(:derived (Flag484-)(and (Flag449-)))

(:derived (Flag484-)(and (Flag436-)))

(:derived (Flag484-)(and (Flag373-)))

(:derived (Flag484-)(and (Flag406-)))

(:derived (Flag484-)(and (Flag473-)))

(:derived (Flag484-)(and (Flag314-)))

(:derived (Flag484-)(and (Flag482-)))

(:derived (Flag484-)(and (Flag306-)))

(:derived (Flag484-)(and (Flag362-)))

(:derived (Flag484-)(and (Flag462-)))

(:derived (Flag484-)(and (Flag463-)))

(:derived (Flag484-)(and (Flag437-)))

(:derived (Flag484-)(and (Flag390-)))

(:derived (Flag484-)(and (Flag333-)))

(:derived (Flag484-)(and (Flag483-)))

(:derived (Flag484-)(and (Flag324-)))

(:derived (Flag484-)(and (Flag438-)))

(:derived (Flag484-)(and (Flag424-)))

(:derived (Flag484-)(and (Flag464-)))

(:derived (Flag484-)(and (Flag450-)))

(:derived (Flag484-)(and (Flag351-)))

(:derived (Flag484-)(and (Flag474-)))

(:derived (Flag484-)(and (Flag452-)))

(:derived (Flag493-)(and (Flag476-)))

(:derived (Flag493-)(and (Flag386-)))

(:derived (Flag493-)(and (Flag302-)))

(:derived (Flag493-)(and (Flag307-)))

(:derived (Flag493-)(and (Flag342-)))

(:derived (Flag493-)(and (Flag359-)))

(:derived (Flag493-)(and (Flag312-)))

(:derived (Flag493-)(and (Flag322-)))

(:derived (Flag493-)(and (Flag393-)))

(:derived (Flag493-)(and (Flag410-)))

(:derived (Flag493-)(and (Flag477-)))

(:derived (Flag493-)(and (Flag440-)))

(:derived (Flag493-)(and (Flag485-)))

(:derived (Flag493-)(and (Flag411-)))

(:derived (Flag493-)(and (Flag344-)))

(:derived (Flag493-)(and (Flag456-)))

(:derived (Flag493-)(and (Flag377-)))

(:derived (Flag493-)(and (Flag486-)))

(:derived (Flag493-)(and (Flag441-)))

(:derived (Flag493-)(and (Flag300-)))

(:derived (Flag493-)(and (Flag455-)))

(:derived (Flag493-)(and (Flag467-)))

(:derived (Flag493-)(and (Flag387-)))

(:derived (Flag493-)(and (Flag319-)))

(:derived (Flag493-)(and (Flag354-)))

(:derived (Flag493-)(and (Flag427-)))

(:derived (Flag493-)(and (Flag443-)))

(:derived (Flag493-)(and (Flag308-)))

(:derived (Flag493-)(and (Flag466-)))

(:derived (Flag493-)(and (Flag428-)))

(:derived (Flag493-)(and (Flag357-)))

(:derived (Flag493-)(and (Flag429-)))

(:derived (Flag493-)(and (Flag413-)))

(:derived (Flag493-)(and (Flag396-)))

(:derived (Flag493-)(and (Flag397-)))

(:derived (Flag493-)(and (Flag478-)))

(:derived (Flag493-)(and (Flag414-)))

(:derived (Flag493-)(and (Flag457-)))

(:derived (Flag493-)(and (Flag445-)))

(:derived (Flag493-)(and (Flag381-)))

(:derived (Flag493-)(and (Flag431-)))

(:derived (Flag493-)(and (Flag405-)))

(:derived (Flag493-)(and (Flag468-)))

(:derived (Flag493-)(and (Flag363-)))

(:derived (Flag493-)(and (Flag487-)))

(:derived (Flag493-)(and (Flag321-)))

(:derived (Flag493-)(and (Flag469-)))

(:derived (Flag493-)(and (Flag398-)))

(:derived (Flag493-)(and (Flag369-)))

(:derived (Flag493-)(and (Flag330-)))

(:derived (Flag493-)(and (Flag479-)))

(:derived (Flag493-)(and (Flag331-)))

(:derived (Flag493-)(and (Flag352-)))

(:derived (Flag493-)(and (Flag416-)))

(:derived (Flag493-)(and (Flag459-)))

(:derived (Flag493-)(and (Flag299-)))

(:derived (Flag493-)(and (Flag375-)))

(:derived (Flag493-)(and (Flag400-)))

(:derived (Flag493-)(and (Flag418-)))

(:derived (Flag493-)(and (Flag401-)))

(:derived (Flag493-)(and (Flag488-)))

(:derived (Flag493-)(and (Flag447-)))

(:derived (Flag493-)(and (Flag379-)))

(:derived (Flag493-)(and (Flag489-)))

(:derived (Flag493-)(and (Flag340-)))

(:derived (Flag493-)(and (Flag460-)))

(:derived (Flag493-)(and (Flag470-)))

(:derived (Flag493-)(and (Flag471-)))

(:derived (Flag493-)(and (Flag315-)))

(:derived (Flag493-)(and (Flag382-)))

(:derived (Flag493-)(and (Flag365-)))

(:derived (Flag493-)(and (Flag419-)))

(:derived (Flag493-)(and (Flag341-)))

(:derived (Flag493-)(and (Flag481-)))

(:derived (Flag493-)(and (Flag449-)))

(:derived (Flag493-)(and (Flag436-)))

(:derived (Flag493-)(and (Flag373-)))

(:derived (Flag493-)(and (Flag406-)))

(:derived (Flag493-)(and (Flag314-)))

(:derived (Flag493-)(and (Flag306-)))

(:derived (Flag493-)(and (Flag362-)))

(:derived (Flag493-)(and (Flag462-)))

(:derived (Flag493-)(and (Flag463-)))

(:derived (Flag493-)(and (Flag490-)))

(:derived (Flag493-)(and (Flag390-)))

(:derived (Flag493-)(and (Flag333-)))

(:derived (Flag493-)(and (Flag491-)))

(:derived (Flag493-)(and (Flag483-)))

(:derived (Flag493-)(and (Flag438-)))

(:derived (Flag493-)(and (Flag424-)))

(:derived (Flag493-)(and (Flag450-)))

(:derived (Flag493-)(and (Flag351-)))

(:derived (Flag493-)(and (Flag474-)))

(:derived (Flag493-)(and (Flag452-)))

(:derived (Flag493-)(and (Flag492-)))

(:derived (Flag501-)(and (Flag476-)))

(:derived (Flag501-)(and (Flag386-)))

(:derived (Flag501-)(and (Flag302-)))

(:derived (Flag501-)(and (Flag494-)))

(:derived (Flag501-)(and (Flag307-)))

(:derived (Flag501-)(and (Flag342-)))

(:derived (Flag501-)(and (Flag359-)))

(:derived (Flag501-)(and (Flag312-)))

(:derived (Flag501-)(and (Flag322-)))

(:derived (Flag501-)(and (Flag393-)))

(:derived (Flag501-)(and (Flag410-)))

(:derived (Flag501-)(and (Flag477-)))

(:derived (Flag501-)(and (Flag440-)))

(:derived (Flag501-)(and (Flag485-)))

(:derived (Flag501-)(and (Flag411-)))

(:derived (Flag501-)(and (Flag344-)))

(:derived (Flag501-)(and (Flag377-)))

(:derived (Flag501-)(and (Flag486-)))

(:derived (Flag501-)(and (Flag441-)))

(:derived (Flag501-)(and (Flag300-)))

(:derived (Flag501-)(and (Flag455-)))

(:derived (Flag501-)(and (Flag467-)))

(:derived (Flag501-)(and (Flag319-)))

(:derived (Flag501-)(and (Flag354-)))

(:derived (Flag501-)(and (Flag427-)))

(:derived (Flag501-)(and (Flag308-)))

(:derived (Flag501-)(and (Flag466-)))

(:derived (Flag501-)(and (Flag428-)))

(:derived (Flag501-)(and (Flag357-)))

(:derived (Flag501-)(and (Flag429-)))

(:derived (Flag501-)(and (Flag413-)))

(:derived (Flag501-)(and (Flag397-)))

(:derived (Flag501-)(and (Flag478-)))

(:derived (Flag501-)(and (Flag414-)))

(:derived (Flag501-)(and (Flag457-)))

(:derived (Flag501-)(and (Flag445-)))

(:derived (Flag501-)(and (Flag431-)))

(:derived (Flag501-)(and (Flag405-)))

(:derived (Flag501-)(and (Flag468-)))

(:derived (Flag501-)(and (Flag363-)))

(:derived (Flag501-)(and (Flag487-)))

(:derived (Flag501-)(and (Flag321-)))

(:derived (Flag501-)(and (Flag469-)))

(:derived (Flag501-)(and (Flag398-)))

(:derived (Flag501-)(and (Flag369-)))

(:derived (Flag501-)(and (Flag495-)))

(:derived (Flag501-)(and (Flag479-)))

(:derived (Flag501-)(and (Flag352-)))

(:derived (Flag501-)(and (Flag416-)))

(:derived (Flag501-)(and (Flag459-)))

(:derived (Flag501-)(and (Flag299-)))

(:derived (Flag501-)(and (Flag375-)))

(:derived (Flag501-)(and (Flag400-)))

(:derived (Flag501-)(and (Flag418-)))

(:derived (Flag501-)(and (Flag401-)))

(:derived (Flag501-)(and (Flag447-)))

(:derived (Flag501-)(and (Flag379-)))

(:derived (Flag501-)(and (Flag496-)))

(:derived (Flag501-)(and (Flag489-)))

(:derived (Flag501-)(and (Flag340-)))

(:derived (Flag501-)(and (Flag470-)))

(:derived (Flag501-)(and (Flag497-)))

(:derived (Flag501-)(and (Flag471-)))

(:derived (Flag501-)(and (Flag315-)))

(:derived (Flag501-)(and (Flag498-)))

(:derived (Flag501-)(and (Flag382-)))

(:derived (Flag501-)(and (Flag499-)))

(:derived (Flag501-)(and (Flag419-)))

(:derived (Flag501-)(and (Flag341-)))

(:derived (Flag501-)(and (Flag481-)))

(:derived (Flag501-)(and (Flag449-)))

(:derived (Flag501-)(and (Flag436-)))

(:derived (Flag501-)(and (Flag373-)))

(:derived (Flag501-)(and (Flag406-)))

(:derived (Flag501-)(and (Flag314-)))

(:derived (Flag501-)(and (Flag306-)))

(:derived (Flag501-)(and (Flag362-)))

(:derived (Flag501-)(and (Flag462-)))

(:derived (Flag501-)(and (Flag463-)))

(:derived (Flag501-)(and (Flag490-)))

(:derived (Flag501-)(and (Flag390-)))

(:derived (Flag501-)(and (Flag500-)))

(:derived (Flag501-)(and (Flag333-)))

(:derived (Flag501-)(and (Flag483-)))

(:derived (Flag501-)(and (Flag438-)))

(:derived (Flag501-)(and (Flag460-)))

(:derived (Flag501-)(and (Flag450-)))

(:derived (Flag507-)(and (Flag476-)))

(:derived (Flag507-)(and (Flag375-)))

(:derived (Flag507-)(and (Flag299-)))

(:derived (Flag507-)(and (Flag469-)))

(:derived (Flag507-)(and (Flag308-)))

(:derived (Flag507-)(and (Flag466-)))

(:derived (Flag507-)(and (Flag428-)))

(:derived (Flag507-)(and (Flag386-)))

(:derived (Flag507-)(and (Flag302-)))

(:derived (Flag507-)(and (Flag359-)))

(:derived (Flag507-)(and (Flag416-)))

(:derived (Flag507-)(and (Flag401-)))

(:derived (Flag507-)(and (Flag413-)))

(:derived (Flag507-)(and (Flag478-)))

(:derived (Flag507-)(and (Flag411-)))

(:derived (Flag507-)(and (Flag447-)))

(:derived (Flag507-)(and (Flag502-)))

(:derived (Flag507-)(and (Flag414-)))

(:derived (Flag507-)(and (Flag379-)))

(:derived (Flag507-)(and (Flag494-)))

(:derived (Flag507-)(and (Flag462-)))

(:derived (Flag507-)(and (Flag314-)))

(:derived (Flag507-)(and (Flag503-)))

(:derived (Flag507-)(and (Flag306-)))

(:derived (Flag507-)(and (Flag340-)))

(:derived (Flag507-)(and (Flag457-)))

(:derived (Flag507-)(and (Flag496-)))

(:derived (Flag507-)(and (Flag363-)))

(:derived (Flag507-)(and (Flag315-)))

(:derived (Flag507-)(and (Flag489-)))

(:derived (Flag507-)(and (Flag342-)))

(:derived (Flag507-)(and (Flag445-)))

(:derived (Flag507-)(and (Flag463-)))

(:derived (Flag507-)(and (Flag312-)))

(:derived (Flag507-)(and (Flag322-)))

(:derived (Flag507-)(and (Flag393-)))

(:derived (Flag507-)(and (Flag490-)))

(:derived (Flag507-)(and (Flag400-)))

(:derived (Flag507-)(and (Flag390-)))

(:derived (Flag507-)(and (Flag405-)))

(:derived (Flag507-)(and (Flag500-)))

(:derived (Flag507-)(and (Flag440-)))

(:derived (Flag507-)(and (Flag485-)))

(:derived (Flag507-)(and (Flag431-)))

(:derived (Flag507-)(and (Flag468-)))

(:derived (Flag507-)(and (Flag300-)))

(:derived (Flag507-)(and (Flag487-)))

(:derived (Flag507-)(and (Flag344-)))

(:derived (Flag507-)(and (Flag483-)))

(:derived (Flag507-)(and (Flag321-)))

(:derived (Flag507-)(and (Flag499-)))

(:derived (Flag507-)(and (Flag373-)))

(:derived (Flag507-)(and (Flag398-)))

(:derived (Flag507-)(and (Flag438-)))

(:derived (Flag507-)(and (Flag377-)))

(:derived (Flag507-)(and (Flag486-)))

(:derived (Flag507-)(and (Flag460-)))

(:derived (Flag507-)(and (Flag471-)))

(:derived (Flag507-)(and (Flag362-)))

(:derived (Flag507-)(and (Flag369-)))

(:derived (Flag507-)(and (Flag382-)))

(:derived (Flag507-)(and (Flag455-)))

(:derived (Flag507-)(and (Flag495-)))

(:derived (Flag507-)(and (Flag450-)))

(:derived (Flag507-)(and (Flag479-)))

(:derived (Flag507-)(and (Flag410-)))

(:derived (Flag507-)(and (Flag419-)))

(:derived (Flag507-)(and (Flag467-)))

(:derived (Flag507-)(and (Flag341-)))

(:derived (Flag507-)(and (Flag481-)))

(:derived (Flag507-)(and (Flag504-)))

(:derived (Flag507-)(and (Flag449-)))

(:derived (Flag507-)(and (Flag333-)))

(:derived (Flag507-)(and (Flag436-)))

(:derived (Flag507-)(and (Flag354-)))

(:derived (Flag507-)(and (Flag505-)))

(:derived (Flag507-)(and (Flag427-)))

(:derived (Flag507-)(and (Flag506-)))

(:derived (Flag512-)(and (Flag476-)))

(:derived (Flag512-)(and (Flag375-)))

(:derived (Flag512-)(and (Flag312-)))

(:derived (Flag512-)(and (Flag308-)))

(:derived (Flag512-)(and (Flag466-)))

(:derived (Flag512-)(and (Flag428-)))

(:derived (Flag512-)(and (Flag386-)))

(:derived (Flag512-)(and (Flag302-)))

(:derived (Flag512-)(and (Flag413-)))

(:derived (Flag512-)(and (Flag478-)))

(:derived (Flag512-)(and (Flag411-)))

(:derived (Flag512-)(and (Flag447-)))

(:derived (Flag512-)(and (Flag414-)))

(:derived (Flag512-)(and (Flag379-)))

(:derived (Flag512-)(and (Flag494-)))

(:derived (Flag512-)(and (Flag462-)))

(:derived (Flag512-)(and (Flag314-)))

(:derived (Flag512-)(and (Flag503-)))

(:derived (Flag512-)(and (Flag306-)))

(:derived (Flag512-)(and (Flag340-)))

(:derived (Flag512-)(and (Flag362-)))

(:derived (Flag512-)(and (Flag363-)))

(:derived (Flag512-)(and (Flag505-)))

(:derived (Flag512-)(and (Flag342-)))

(:derived (Flag512-)(and (Flag359-)))

(:derived (Flag512-)(and (Flag463-)))

(:derived (Flag512-)(and (Flag499-)))

(:derived (Flag512-)(and (Flag431-)))

(:derived (Flag512-)(and (Flag322-)))

(:derived (Flag512-)(and (Flag393-)))

(:derived (Flag512-)(and (Flag490-)))

(:derived (Flag512-)(and (Flag508-)))

(:derived (Flag512-)(and (Flag405-)))

(:derived (Flag512-)(and (Flag500-)))

(:derived (Flag512-)(and (Flag440-)))

(:derived (Flag512-)(and (Flag485-)))

(:derived (Flag512-)(and (Flag509-)))

(:derived (Flag512-)(and (Flag468-)))

(:derived (Flag512-)(and (Flag300-)))

(:derived (Flag512-)(and (Flag487-)))

(:derived (Flag512-)(and (Flag344-)))

(:derived (Flag512-)(and (Flag483-)))

(:derived (Flag512-)(and (Flag469-)))

(:derived (Flag512-)(and (Flag398-)))

(:derived (Flag512-)(and (Flag438-)))

(:derived (Flag512-)(and (Flag510-)))

(:derived (Flag512-)(and (Flag377-)))

(:derived (Flag512-)(and (Flag486-)))

(:derived (Flag512-)(and (Flag460-)))

(:derived (Flag512-)(and (Flag369-)))

(:derived (Flag512-)(and (Flag299-)))

(:derived (Flag512-)(and (Flag382-)))

(:derived (Flag512-)(and (Flag455-)))

(:derived (Flag512-)(and (Flag495-)))

(:derived (Flag512-)(and (Flag450-)))

(:derived (Flag512-)(and (Flag479-)))

(:derived (Flag512-)(and (Flag419-)))

(:derived (Flag512-)(and (Flag467-)))

(:derived (Flag512-)(and (Flag400-)))

(:derived (Flag512-)(and (Flag416-)))

(:derived (Flag512-)(and (Flag511-)))

(:derived (Flag512-)(and (Flag504-)))

(:derived (Flag512-)(and (Flag449-)))

(:derived (Flag512-)(and (Flag333-)))

(:derived (Flag512-)(and (Flag436-)))

(:derived (Flag512-)(and (Flag354-)))

(:derived (Flag512-)(and (Flag506-)))

(:derived (Flag516-)(and (Flag476-)))

(:derived (Flag516-)(and (Flag375-)))

(:derived (Flag516-)(and (Flag312-)))

(:derived (Flag516-)(and (Flag308-)))

(:derived (Flag516-)(and (Flag466-)))

(:derived (Flag516-)(and (Flag428-)))

(:derived (Flag516-)(and (Flag386-)))

(:derived (Flag516-)(and (Flag302-)))

(:derived (Flag516-)(and (Flag460-)))

(:derived (Flag516-)(and (Flag413-)))

(:derived (Flag516-)(and (Flag513-)))

(:derived (Flag516-)(and (Flag447-)))

(:derived (Flag516-)(and (Flag379-)))

(:derived (Flag516-)(and (Flag514-)))

(:derived (Flag516-)(and (Flag314-)))

(:derived (Flag516-)(and (Flag414-)))

(:derived (Flag516-)(and (Flag340-)))

(:derived (Flag516-)(and (Flag362-)))

(:derived (Flag516-)(and (Flag505-)))

(:derived (Flag516-)(and (Flag342-)))

(:derived (Flag516-)(and (Flag359-)))

(:derived (Flag516-)(and (Flag463-)))

(:derived (Flag516-)(and (Flag499-)))

(:derived (Flag516-)(and (Flag431-)))

(:derived (Flag516-)(and (Flag322-)))

(:derived (Flag516-)(and (Flag393-)))

(:derived (Flag516-)(and (Flag508-)))

(:derived (Flag516-)(and (Flag500-)))

(:derived (Flag516-)(and (Flag440-)))

(:derived (Flag516-)(and (Flag485-)))

(:derived (Flag516-)(and (Flag509-)))

(:derived (Flag516-)(and (Flag300-)))

(:derived (Flag516-)(and (Flag487-)))

(:derived (Flag516-)(and (Flag483-)))

(:derived (Flag516-)(and (Flag469-)))

(:derived (Flag516-)(and (Flag398-)))

(:derived (Flag516-)(and (Flag438-)))

(:derived (Flag516-)(and (Flag486-)))

(:derived (Flag516-)(and (Flag515-)))

(:derived (Flag516-)(and (Flag299-)))

(:derived (Flag516-)(and (Flag382-)))

(:derived (Flag516-)(and (Flag455-)))

(:derived (Flag516-)(and (Flag495-)))

(:derived (Flag516-)(and (Flag450-)))

(:derived (Flag516-)(and (Flag479-)))

(:derived (Flag516-)(and (Flag419-)))

(:derived (Flag516-)(and (Flag467-)))

(:derived (Flag516-)(and (Flag416-)))

(:derived (Flag516-)(and (Flag511-)))

(:derived (Flag516-)(and (Flag504-)))

(:derived (Flag516-)(and (Flag405-)))

(:derived (Flag516-)(and (Flag354-)))

(:derived (Flag516-)(and (Flag506-)))

(:derived (Flag519-)(and (Flag476-)))

(:derived (Flag519-)(and (Flag375-)))

(:derived (Flag519-)(and (Flag428-)))

(:derived (Flag519-)(and (Flag386-)))

(:derived (Flag519-)(and (Flag302-)))

(:derived (Flag519-)(and (Flag513-)))

(:derived (Flag519-)(and (Flag314-)))

(:derived (Flag519-)(and (Flag414-)))

(:derived (Flag519-)(and (Flag340-)))

(:derived (Flag519-)(and (Flag362-)))

(:derived (Flag519-)(and (Flag359-)))

(:derived (Flag519-)(and (Flag463-)))

(:derived (Flag519-)(and (Flag517-)))

(:derived (Flag519-)(and (Flag431-)))

(:derived (Flag519-)(and (Flag322-)))

(:derived (Flag519-)(and (Flag393-)))

(:derived (Flag519-)(and (Flag508-)))

(:derived (Flag519-)(and (Flag500-)))

(:derived (Flag519-)(and (Flag518-)))

(:derived (Flag519-)(and (Flag485-)))

(:derived (Flag519-)(and (Flag487-)))

(:derived (Flag519-)(and (Flag469-)))

(:derived (Flag519-)(and (Flag398-)))

(:derived (Flag519-)(and (Flag515-)))

(:derived (Flag519-)(and (Flag300-)))

(:derived (Flag519-)(and (Flag455-)))

(:derived (Flag519-)(and (Flag499-)))

(:derived (Flag519-)(and (Flag450-)))

(:derived (Flag519-)(and (Flag479-)))

(:derived (Flag519-)(and (Flag419-)))

(:derived (Flag519-)(and (Flag467-)))

(:derived (Flag519-)(and (Flag416-)))

(:derived (Flag519-)(and (Flag511-)))

(:derived (Flag519-)(and (Flag504-)))

(:derived (Flag519-)(and (Flag354-)))

(:derived (Flag519-)(and (Flag440-)))

(:derived (Flag519-)(and (Flag506-)))

(:derived (Flag521-)(and (Flag485-)))

(:derived (Flag521-)(and (Flag455-)))

(:derived (Flag521-)(and (Flag499-)))

(:derived (Flag521-)(and (Flag450-)))

(:derived (Flag521-)(and (Flag416-)))

(:derived (Flag521-)(and (Flag513-)))

(:derived (Flag521-)(and (Flag476-)))

(:derived (Flag521-)(and (Flag386-)))

(:derived (Flag521-)(and (Flag302-)))

(:derived (Flag521-)(and (Flag467-)))

(:derived (Flag521-)(and (Flag300-)))

(:derived (Flag521-)(and (Flag414-)))

(:derived (Flag521-)(and (Flag520-)))

(:derived (Flag521-)(and (Flag511-)))

(:derived (Flag521-)(and (Flag431-)))

(:derived (Flag521-)(and (Flag354-)))

(:derived (Flag521-)(and (Flag362-)))

(:derived (Flag521-)(and (Flag506-)))

(:derived (Flag521-)(and (Flag518-)))

(:derived (Flag522-)(and (Flag329-)))

(:derived (Flag522-)(and (Flag300-)))

(:derived (Flag522-)(and (Flag317-)))

(:derived (Flag522-)(and (Flag301-)))

(:derived (Flag522-)(and (Flag331-)))

(:derived (Flag522-)(and (Flag319-)))

(:derived (Flag522-)(and (Flag309-)))

(:derived (Flag522-)(and (Flag303-)))

(:derived (Flag522-)(and (Flag320-)))

(:derived (Flag522-)(and (Flag311-)))

(:derived (Flag522-)(and (Flag321-)))

(:derived (Flag522-)(and (Flag308-)))

(:derived (Flag522-)(and (Flag323-)))

(:derived (Flag522-)(and (Flag333-)))

(:derived (Flag522-)(and (Flag334-)))

(:derived (Flag522-)(and (Flag325-)))

(:derived (Flag522-)(and (Flag326-)))

(:derived (Flag522-)(and (Flag299-)))

(:action MOVERIGHT-ROBOT
:parameters ()
:precondition
(and
(Flag4-)
)
:effect
(and
(when
(and
(Flag258-)
)
(and
(COLUMN18-ROBOT)
(not (NOT-COLUMN18-ROBOT))
)
)
(when
(and
(Flag255-)
)
(and
(COLUMN17-ROBOT)
(not (NOT-COLUMN17-ROBOT))
)
)
(when
(and
(Flag251-)
)
(and
(COLUMN16-ROBOT)
(not (NOT-COLUMN16-ROBOT))
)
)
(when
(and
(Flag245-)
)
(and
(COLUMN15-ROBOT)
(not (NOT-COLUMN15-ROBOT))
)
)
(when
(and
(Flag238-)
)
(and
(COLUMN14-ROBOT)
(not (NOT-COLUMN14-ROBOT))
)
)
(when
(and
(Flag231-)
)
(and
(COLUMN13-ROBOT)
(not (NOT-COLUMN13-ROBOT))
)
)
(when
(and
(Flag222-)
)
(and
(COLUMN12-ROBOT)
(not (NOT-COLUMN12-ROBOT))
)
)
(when
(and
(Flag213-)
)
(and
(COLUMN11-ROBOT)
(not (NOT-COLUMN11-ROBOT))
)
)
(when
(and
(Flag202-)
)
(and
(COLUMN10-ROBOT)
(not (NOT-COLUMN10-ROBOT))
)
)
(when
(and
(Flag191-)
)
(and
(COLUMN9-ROBOT)
(not (NOT-COLUMN9-ROBOT))
)
)
(when
(and
(Flag178-)
)
(and
(COLUMN8-ROBOT)
(not (NOT-COLUMN8-ROBOT))
)
)
(when
(and
(Flag164-)
)
(and
(COLUMN7-ROBOT)
(not (NOT-COLUMN7-ROBOT))
)
)
(when
(and
(Flag148-)
)
(and
(COLUMN6-ROBOT)
(not (NOT-COLUMN6-ROBOT))
)
)
(when
(and
(Flag133-)
)
(and
(COLUMN5-ROBOT)
(not (NOT-COLUMN5-ROBOT))
)
)
(when
(and
(Flag114-)
)
(and
(COLUMN4-ROBOT)
(not (NOT-COLUMN4-ROBOT))
)
)
(when
(and
(Flag95-)
)
(and
(COLUMN3-ROBOT)
(not (NOT-COLUMN3-ROBOT))
)
)
(when
(and
(Flag77-)
)
(and
(COLUMN2-ROBOT)
(not (NOT-COLUMN2-ROBOT))
)
)
(when
(and
(Flag263-)
)
(and
(COLUMN1-ROBOT)
(not (NOT-COLUMN1-ROBOT))
)
)
(when
(and
(COLUMN17-ROBOT)
)
(and
(COLUMN18-ROBOT)
(NOT-COLUMN17-ROBOT)
(not (NOT-COLUMN18-ROBOT))
(not (COLUMN17-ROBOT))
)
)
(when
(and
(COLUMN16-ROBOT)
)
(and
(COLUMN17-ROBOT)
(NOT-COLUMN16-ROBOT)
(not (NOT-COLUMN17-ROBOT))
(not (COLUMN16-ROBOT))
)
)
(when
(and
(COLUMN15-ROBOT)
)
(and
(COLUMN16-ROBOT)
(NOT-COLUMN15-ROBOT)
(not (NOT-COLUMN16-ROBOT))
(not (COLUMN15-ROBOT))
)
)
(when
(and
(COLUMN14-ROBOT)
)
(and
(COLUMN15-ROBOT)
(NOT-COLUMN14-ROBOT)
(not (NOT-COLUMN15-ROBOT))
(not (COLUMN14-ROBOT))
)
)
(when
(and
(COLUMN13-ROBOT)
)
(and
(COLUMN14-ROBOT)
(NOT-COLUMN13-ROBOT)
(not (NOT-COLUMN14-ROBOT))
(not (COLUMN13-ROBOT))
)
)
(when
(and
(COLUMN12-ROBOT)
)
(and
(COLUMN13-ROBOT)
(NOT-COLUMN12-ROBOT)
(not (NOT-COLUMN13-ROBOT))
(not (COLUMN12-ROBOT))
)
)
(when
(and
(COLUMN11-ROBOT)
)
(and
(COLUMN12-ROBOT)
(NOT-COLUMN11-ROBOT)
(not (NOT-COLUMN12-ROBOT))
(not (COLUMN11-ROBOT))
)
)
(when
(and
(COLUMN10-ROBOT)
)
(and
(COLUMN11-ROBOT)
(NOT-COLUMN10-ROBOT)
(not (NOT-COLUMN11-ROBOT))
(not (COLUMN10-ROBOT))
)
)
(when
(and
(COLUMN9-ROBOT)
)
(and
(COLUMN10-ROBOT)
(NOT-COLUMN9-ROBOT)
(not (NOT-COLUMN10-ROBOT))
(not (COLUMN9-ROBOT))
)
)
(when
(and
(COLUMN8-ROBOT)
)
(and
(COLUMN9-ROBOT)
(NOT-COLUMN8-ROBOT)
(not (NOT-COLUMN9-ROBOT))
(not (COLUMN8-ROBOT))
)
)
(when
(and
(COLUMN7-ROBOT)
)
(and
(COLUMN8-ROBOT)
(NOT-COLUMN7-ROBOT)
(not (NOT-COLUMN8-ROBOT))
(not (COLUMN7-ROBOT))
)
)
(when
(and
(COLUMN6-ROBOT)
)
(and
(COLUMN7-ROBOT)
(NOT-COLUMN6-ROBOT)
(not (NOT-COLUMN7-ROBOT))
(not (COLUMN6-ROBOT))
)
)
(when
(and
(COLUMN5-ROBOT)
)
(and
(COLUMN6-ROBOT)
(NOT-COLUMN5-ROBOT)
(not (NOT-COLUMN6-ROBOT))
(not (COLUMN5-ROBOT))
)
)
(when
(and
(COLUMN4-ROBOT)
)
(and
(COLUMN5-ROBOT)
(NOT-COLUMN4-ROBOT)
(not (NOT-COLUMN5-ROBOT))
(not (COLUMN4-ROBOT))
)
)
(when
(and
(COLUMN3-ROBOT)
)
(and
(COLUMN4-ROBOT)
(NOT-COLUMN3-ROBOT)
(not (NOT-COLUMN4-ROBOT))
(not (COLUMN3-ROBOT))
)
)
(when
(and
(COLUMN2-ROBOT)
)
(and
(COLUMN3-ROBOT)
(NOT-COLUMN2-ROBOT)
(not (NOT-COLUMN3-ROBOT))
(not (COLUMN2-ROBOT))
)
)
(when
(and
(COLUMN1-ROBOT)
)
(and
(COLUMN2-ROBOT)
(NOT-COLUMN1-ROBOT)
(not (NOT-COLUMN2-ROBOT))
(not (COLUMN1-ROBOT))
)
)
(when
(and
(COLUMN0-ROBOT)
)
(and
(COLUMN1-ROBOT)
(NOT-COLUMN0-ROBOT)
(not (NOT-COLUMN1-ROBOT))
(not (COLUMN0-ROBOT))
)
)
(when
(and
(LEFTOF8-ROBOT)
)
(and
(LEFTOF19-ROBOT)
(NOT-LEFTOF18-ROBOT)
(LEFTOF18-ROBOT)
(NOT-LEFTOF17-ROBOT)
(LEFTOF17-ROBOT)
(NOT-LEFTOF16-ROBOT)
(LEFTOF16-ROBOT)
(NOT-LEFTOF15-ROBOT)
(LEFTOF15-ROBOT)
(NOT-LEFTOF14-ROBOT)
(LEFTOF14-ROBOT)
(NOT-LEFTOF13-ROBOT)
(LEFTOF13-ROBOT)
(NOT-LEFTOF12-ROBOT)
(LEFTOF12-ROBOT)
(NOT-LEFTOF11-ROBOT)
(LEFTOF11-ROBOT)
(NOT-LEFTOF10-ROBOT)
(LEFTOF10-ROBOT)
(NOT-LEFTOF9-ROBOT)
(LEFTOF9-ROBOT)
(NOT-LEFTOF8-ROBOT)
(not (NOT-LEFTOF19-ROBOT))
(not (LEFTOF8-ROBOT))
)
)
(when
(and
(LEFTOF7-ROBOT)
)
(and
(LEFTOF19-ROBOT)
(NOT-LEFTOF18-ROBOT)
(LEFTOF18-ROBOT)
(NOT-LEFTOF17-ROBOT)
(LEFTOF17-ROBOT)
(NOT-LEFTOF16-ROBOT)
(LEFTOF16-ROBOT)
(NOT-LEFTOF15-ROBOT)
(LEFTOF15-ROBOT)
(NOT-LEFTOF14-ROBOT)
(LEFTOF14-ROBOT)
(NOT-LEFTOF13-ROBOT)
(LEFTOF13-ROBOT)
(NOT-LEFTOF12-ROBOT)
(LEFTOF12-ROBOT)
(NOT-LEFTOF11-ROBOT)
(LEFTOF11-ROBOT)
(NOT-LEFTOF10-ROBOT)
(LEFTOF10-ROBOT)
(NOT-LEFTOF9-ROBOT)
(LEFTOF9-ROBOT)
(NOT-LEFTOF8-ROBOT)
(LEFTOF8-ROBOT)
(NOT-LEFTOF7-ROBOT)
(not (NOT-LEFTOF19-ROBOT))
(not (LEFTOF7-ROBOT))
)
)
(when
(and
(LEFTOF4-ROBOT)
)
(and
(LEFTOF19-ROBOT)
(NOT-LEFTOF18-ROBOT)
(LEFTOF18-ROBOT)
(NOT-LEFTOF17-ROBOT)
(LEFTOF17-ROBOT)
(NOT-LEFTOF16-ROBOT)
(LEFTOF16-ROBOT)
(NOT-LEFTOF15-ROBOT)
(LEFTOF15-ROBOT)
(NOT-LEFTOF14-ROBOT)
(LEFTOF14-ROBOT)
(NOT-LEFTOF13-ROBOT)
(LEFTOF13-ROBOT)
(NOT-LEFTOF12-ROBOT)
(LEFTOF12-ROBOT)
(NOT-LEFTOF11-ROBOT)
(LEFTOF11-ROBOT)
(NOT-LEFTOF10-ROBOT)
(LEFTOF10-ROBOT)
(NOT-LEFTOF9-ROBOT)
(LEFTOF9-ROBOT)
(NOT-LEFTOF8-ROBOT)
(LEFTOF8-ROBOT)
(NOT-LEFTOF7-ROBOT)
(LEFTOF7-ROBOT)
(NOT-LEFTOF6-ROBOT)
(LEFTOF6-ROBOT)
(NOT-LEFTOF5-ROBOT)
(LEFTOF5-ROBOT)
(NOT-LEFTOF4-ROBOT)
(not (NOT-LEFTOF19-ROBOT))
(not (LEFTOF4-ROBOT))
)
)
(when
(and
(LEFTOF3-ROBOT)
)
(and
(LEFTOF19-ROBOT)
(NOT-LEFTOF18-ROBOT)
(LEFTOF18-ROBOT)
(NOT-LEFTOF17-ROBOT)
(LEFTOF17-ROBOT)
(NOT-LEFTOF16-ROBOT)
(LEFTOF16-ROBOT)
(NOT-LEFTOF15-ROBOT)
(LEFTOF15-ROBOT)
(NOT-LEFTOF14-ROBOT)
(LEFTOF14-ROBOT)
(NOT-LEFTOF13-ROBOT)
(LEFTOF13-ROBOT)
(NOT-LEFTOF12-ROBOT)
(LEFTOF12-ROBOT)
(NOT-LEFTOF11-ROBOT)
(LEFTOF11-ROBOT)
(NOT-LEFTOF10-ROBOT)
(LEFTOF10-ROBOT)
(NOT-LEFTOF9-ROBOT)
(LEFTOF9-ROBOT)
(NOT-LEFTOF8-ROBOT)
(LEFTOF8-ROBOT)
(NOT-LEFTOF7-ROBOT)
(LEFTOF7-ROBOT)
(NOT-LEFTOF6-ROBOT)
(LEFTOF6-ROBOT)
(NOT-LEFTOF5-ROBOT)
(LEFTOF5-ROBOT)
(NOT-LEFTOF4-ROBOT)
(LEFTOF4-ROBOT)
(NOT-LEFTOF3-ROBOT)
(not (NOT-LEFTOF19-ROBOT))
(not (LEFTOF3-ROBOT))
)
)
(when
(and
(LEFTOF14-ROBOT)
)
(and
(LEFTOF19-ROBOT)
(NOT-LEFTOF18-ROBOT)
(LEFTOF18-ROBOT)
(NOT-LEFTOF17-ROBOT)
(LEFTOF17-ROBOT)
(NOT-LEFTOF16-ROBOT)
(LEFTOF16-ROBOT)
(NOT-LEFTOF15-ROBOT)
(LEFTOF15-ROBOT)
(NOT-LEFTOF14-ROBOT)
(not (NOT-LEFTOF19-ROBOT))
(not (LEFTOF14-ROBOT))
)
)
(when
(and
(LEFTOF16-ROBOT)
)
(and
(LEFTOF19-ROBOT)
(NOT-LEFTOF18-ROBOT)
(LEFTOF18-ROBOT)
(NOT-LEFTOF17-ROBOT)
(LEFTOF17-ROBOT)
(NOT-LEFTOF16-ROBOT)
(not (NOT-LEFTOF19-ROBOT))
(not (LEFTOF16-ROBOT))
)
)
(when
(and
(LEFTOF5-ROBOT)
)
(and
(LEFTOF19-ROBOT)
(NOT-LEFTOF18-ROBOT)
(LEFTOF18-ROBOT)
(NOT-LEFTOF17-ROBOT)
(LEFTOF17-ROBOT)
(NOT-LEFTOF16-ROBOT)
(LEFTOF16-ROBOT)
(NOT-LEFTOF15-ROBOT)
(LEFTOF15-ROBOT)
(NOT-LEFTOF14-ROBOT)
(LEFTOF14-ROBOT)
(NOT-LEFTOF13-ROBOT)
(LEFTOF13-ROBOT)
(NOT-LEFTOF12-ROBOT)
(LEFTOF12-ROBOT)
(NOT-LEFTOF11-ROBOT)
(LEFTOF11-ROBOT)
(NOT-LEFTOF10-ROBOT)
(LEFTOF10-ROBOT)
(NOT-LEFTOF9-ROBOT)
(LEFTOF9-ROBOT)
(NOT-LEFTOF8-ROBOT)
(LEFTOF8-ROBOT)
(NOT-LEFTOF7-ROBOT)
(LEFTOF7-ROBOT)
(NOT-LEFTOF6-ROBOT)
(LEFTOF6-ROBOT)
(NOT-LEFTOF5-ROBOT)
(not (NOT-LEFTOF19-ROBOT))
(not (LEFTOF5-ROBOT))
)
)
(when
(and
(LEFTOF6-ROBOT)
)
(and
(LEFTOF19-ROBOT)
(NOT-LEFTOF18-ROBOT)
(LEFTOF18-ROBOT)
(NOT-LEFTOF17-ROBOT)
(LEFTOF17-ROBOT)
(NOT-LEFTOF16-ROBOT)
(LEFTOF16-ROBOT)
(NOT-LEFTOF15-ROBOT)
(LEFTOF15-ROBOT)
(NOT-LEFTOF14-ROBOT)
(LEFTOF14-ROBOT)
(NOT-LEFTOF13-ROBOT)
(LEFTOF13-ROBOT)
(NOT-LEFTOF12-ROBOT)
(LEFTOF12-ROBOT)
(NOT-LEFTOF11-ROBOT)
(LEFTOF11-ROBOT)
(NOT-LEFTOF10-ROBOT)
(LEFTOF10-ROBOT)
(NOT-LEFTOF9-ROBOT)
(LEFTOF9-ROBOT)
(NOT-LEFTOF8-ROBOT)
(LEFTOF8-ROBOT)
(NOT-LEFTOF7-ROBOT)
(LEFTOF7-ROBOT)
(NOT-LEFTOF6-ROBOT)
(not (NOT-LEFTOF19-ROBOT))
(not (LEFTOF6-ROBOT))
)
)
(when
(and
(LEFTOF12-ROBOT)
)
(and
(LEFTOF19-ROBOT)
(NOT-LEFTOF18-ROBOT)
(LEFTOF18-ROBOT)
(NOT-LEFTOF17-ROBOT)
(LEFTOF17-ROBOT)
(NOT-LEFTOF16-ROBOT)
(LEFTOF16-ROBOT)
(NOT-LEFTOF15-ROBOT)
(LEFTOF15-ROBOT)
(NOT-LEFTOF14-ROBOT)
(LEFTOF14-ROBOT)
(NOT-LEFTOF13-ROBOT)
(LEFTOF13-ROBOT)
(NOT-LEFTOF12-ROBOT)
(not (NOT-LEFTOF19-ROBOT))
(not (LEFTOF12-ROBOT))
)
)
(when
(and
(LEFTOF1-ROBOT)
)
(and
(LEFTOF19-ROBOT)
(NOT-LEFTOF18-ROBOT)
(LEFTOF18-ROBOT)
(NOT-LEFTOF17-ROBOT)
(LEFTOF17-ROBOT)
(NOT-LEFTOF16-ROBOT)
(LEFTOF16-ROBOT)
(NOT-LEFTOF15-ROBOT)
(LEFTOF15-ROBOT)
(NOT-LEFTOF14-ROBOT)
(LEFTOF14-ROBOT)
(NOT-LEFTOF13-ROBOT)
(LEFTOF13-ROBOT)
(NOT-LEFTOF12-ROBOT)
(LEFTOF12-ROBOT)
(NOT-LEFTOF11-ROBOT)
(LEFTOF11-ROBOT)
(NOT-LEFTOF10-ROBOT)
(LEFTOF10-ROBOT)
(NOT-LEFTOF9-ROBOT)
(LEFTOF9-ROBOT)
(NOT-LEFTOF8-ROBOT)
(LEFTOF8-ROBOT)
(NOT-LEFTOF7-ROBOT)
(LEFTOF7-ROBOT)
(NOT-LEFTOF6-ROBOT)
(LEFTOF6-ROBOT)
(NOT-LEFTOF5-ROBOT)
(LEFTOF5-ROBOT)
(NOT-LEFTOF4-ROBOT)
(LEFTOF4-ROBOT)
(NOT-LEFTOF3-ROBOT)
(LEFTOF3-ROBOT)
(NOT-LEFTOF2-ROBOT)
(LEFTOF2-ROBOT)
(NOT-LEFTOF1-ROBOT)
(not (NOT-LEFTOF19-ROBOT))
(not (LEFTOF1-ROBOT))
)
)
(when
(and
(LEFTOF13-ROBOT)
)
(and
(LEFTOF19-ROBOT)
(NOT-LEFTOF18-ROBOT)
(LEFTOF18-ROBOT)
(NOT-LEFTOF17-ROBOT)
(LEFTOF17-ROBOT)
(NOT-LEFTOF16-ROBOT)
(LEFTOF16-ROBOT)
(NOT-LEFTOF15-ROBOT)
(LEFTOF15-ROBOT)
(NOT-LEFTOF14-ROBOT)
(LEFTOF14-ROBOT)
(NOT-LEFTOF13-ROBOT)
(not (NOT-LEFTOF19-ROBOT))
(not (LEFTOF13-ROBOT))
)
)
(when
(and
(LEFTOF2-ROBOT)
)
(and
(LEFTOF19-ROBOT)
(NOT-LEFTOF18-ROBOT)
(LEFTOF18-ROBOT)
(NOT-LEFTOF17-ROBOT)
(LEFTOF17-ROBOT)
(NOT-LEFTOF16-ROBOT)
(LEFTOF16-ROBOT)
(NOT-LEFTOF15-ROBOT)
(LEFTOF15-ROBOT)
(NOT-LEFTOF14-ROBOT)
(LEFTOF14-ROBOT)
(NOT-LEFTOF13-ROBOT)
(LEFTOF13-ROBOT)
(NOT-LEFTOF12-ROBOT)
(LEFTOF12-ROBOT)
(NOT-LEFTOF11-ROBOT)
(LEFTOF11-ROBOT)
(NOT-LEFTOF10-ROBOT)
(LEFTOF10-ROBOT)
(NOT-LEFTOF9-ROBOT)
(LEFTOF9-ROBOT)
(NOT-LEFTOF8-ROBOT)
(LEFTOF8-ROBOT)
(NOT-LEFTOF7-ROBOT)
(LEFTOF7-ROBOT)
(NOT-LEFTOF6-ROBOT)
(LEFTOF6-ROBOT)
(NOT-LEFTOF5-ROBOT)
(LEFTOF5-ROBOT)
(NOT-LEFTOF4-ROBOT)
(LEFTOF4-ROBOT)
(NOT-LEFTOF3-ROBOT)
(LEFTOF3-ROBOT)
(NOT-LEFTOF2-ROBOT)
(not (NOT-LEFTOF19-ROBOT))
(not (LEFTOF2-ROBOT))
)
)
(when
(and
(LEFTOF9-ROBOT)
)
(and
(LEFTOF19-ROBOT)
(NOT-LEFTOF18-ROBOT)
(LEFTOF18-ROBOT)
(NOT-LEFTOF17-ROBOT)
(LEFTOF17-ROBOT)
(NOT-LEFTOF16-ROBOT)
(LEFTOF16-ROBOT)
(NOT-LEFTOF15-ROBOT)
(LEFTOF15-ROBOT)
(NOT-LEFTOF14-ROBOT)
(LEFTOF14-ROBOT)
(NOT-LEFTOF13-ROBOT)
(LEFTOF13-ROBOT)
(NOT-LEFTOF12-ROBOT)
(LEFTOF12-ROBOT)
(NOT-LEFTOF11-ROBOT)
(LEFTOF11-ROBOT)
(NOT-LEFTOF10-ROBOT)
(LEFTOF10-ROBOT)
(NOT-LEFTOF9-ROBOT)
(not (NOT-LEFTOF19-ROBOT))
(not (LEFTOF9-ROBOT))
)
)
(when
(and
(LEFTOF11-ROBOT)
)
(and
(LEFTOF19-ROBOT)
(NOT-LEFTOF18-ROBOT)
(LEFTOF18-ROBOT)
(NOT-LEFTOF17-ROBOT)
(LEFTOF17-ROBOT)
(NOT-LEFTOF16-ROBOT)
(LEFTOF16-ROBOT)
(NOT-LEFTOF15-ROBOT)
(LEFTOF15-ROBOT)
(NOT-LEFTOF14-ROBOT)
(LEFTOF14-ROBOT)
(NOT-LEFTOF13-ROBOT)
(LEFTOF13-ROBOT)
(NOT-LEFTOF12-ROBOT)
(LEFTOF12-ROBOT)
(NOT-LEFTOF11-ROBOT)
(not (NOT-LEFTOF19-ROBOT))
(not (LEFTOF11-ROBOT))
)
)
(when
(and
(LEFTOF17-ROBOT)
)
(and
(LEFTOF19-ROBOT)
(NOT-LEFTOF18-ROBOT)
(LEFTOF18-ROBOT)
(NOT-LEFTOF17-ROBOT)
(not (NOT-LEFTOF19-ROBOT))
(not (LEFTOF17-ROBOT))
)
)
(when
(and
(LEFTOF18-ROBOT)
)
(and
(LEFTOF19-ROBOT)
(NOT-LEFTOF18-ROBOT)
(not (NOT-LEFTOF19-ROBOT))
(not (LEFTOF18-ROBOT))
)
)
(when
(and
(LEFTOF10-ROBOT)
)
(and
(LEFTOF19-ROBOT)
(NOT-LEFTOF18-ROBOT)
(LEFTOF18-ROBOT)
(NOT-LEFTOF17-ROBOT)
(LEFTOF17-ROBOT)
(NOT-LEFTOF16-ROBOT)
(LEFTOF16-ROBOT)
(NOT-LEFTOF15-ROBOT)
(LEFTOF15-ROBOT)
(NOT-LEFTOF14-ROBOT)
(LEFTOF14-ROBOT)
(NOT-LEFTOF13-ROBOT)
(LEFTOF13-ROBOT)
(NOT-LEFTOF12-ROBOT)
(LEFTOF12-ROBOT)
(NOT-LEFTOF11-ROBOT)
(LEFTOF11-ROBOT)
(NOT-LEFTOF10-ROBOT)
(not (NOT-LEFTOF19-ROBOT))
(not (LEFTOF10-ROBOT))
)
)
(when
(and
(LEFTOF15-ROBOT)
)
(and
(LEFTOF19-ROBOT)
(NOT-LEFTOF18-ROBOT)
(LEFTOF18-ROBOT)
(NOT-LEFTOF17-ROBOT)
(LEFTOF17-ROBOT)
(NOT-LEFTOF16-ROBOT)
(LEFTOF16-ROBOT)
(NOT-LEFTOF15-ROBOT)
(not (NOT-LEFTOF19-ROBOT))
(not (LEFTOF15-ROBOT))
)
)
(when
(and
(RIGHTOF18-ROBOT)
)
(and
(RIGHTOF17-ROBOT)
(RIGHTOF16-ROBOT)
(RIGHTOF15-ROBOT)
(RIGHTOF14-ROBOT)
(RIGHTOF13-ROBOT)
(RIGHTOF12-ROBOT)
(RIGHTOF11-ROBOT)
(RIGHTOF10-ROBOT)
(RIGHTOF9-ROBOT)
(RIGHTOF8-ROBOT)
(RIGHTOF7-ROBOT)
(RIGHTOF6-ROBOT)
(RIGHTOF5-ROBOT)
(RIGHTOF4-ROBOT)
(RIGHTOF3-ROBOT)
(RIGHTOF2-ROBOT)
(not (NOT-RIGHTOF18-ROBOT))
(not (NOT-RIGHTOF17-ROBOT))
(not (NOT-RIGHTOF16-ROBOT))
(not (NOT-RIGHTOF15-ROBOT))
(not (NOT-RIGHTOF14-ROBOT))
(not (NOT-RIGHTOF13-ROBOT))
(not (NOT-RIGHTOF12-ROBOT))
(not (NOT-RIGHTOF11-ROBOT))
(not (NOT-RIGHTOF10-ROBOT))
(not (NOT-RIGHTOF9-ROBOT))
(not (NOT-RIGHTOF8-ROBOT))
(not (NOT-RIGHTOF7-ROBOT))
(not (NOT-RIGHTOF6-ROBOT))
(not (NOT-RIGHTOF5-ROBOT))
(not (NOT-RIGHTOF4-ROBOT))
(not (NOT-RIGHTOF3-ROBOT))
(not (NOT-RIGHTOF2-ROBOT))
)
)
(when
(and
(RIGHTOF17-ROBOT)
)
(and
(RIGHTOF18-ROBOT)
(RIGHTOF16-ROBOT)
(RIGHTOF15-ROBOT)
(RIGHTOF14-ROBOT)
(RIGHTOF13-ROBOT)
(RIGHTOF12-ROBOT)
(RIGHTOF11-ROBOT)
(RIGHTOF10-ROBOT)
(RIGHTOF9-ROBOT)
(RIGHTOF8-ROBOT)
(RIGHTOF7-ROBOT)
(RIGHTOF6-ROBOT)
(RIGHTOF5-ROBOT)
(RIGHTOF4-ROBOT)
(RIGHTOF3-ROBOT)
(RIGHTOF2-ROBOT)
(not (NOT-RIGHTOF18-ROBOT))
(not (NOT-RIGHTOF17-ROBOT))
(not (NOT-RIGHTOF16-ROBOT))
(not (NOT-RIGHTOF15-ROBOT))
(not (NOT-RIGHTOF14-ROBOT))
(not (NOT-RIGHTOF13-ROBOT))
(not (NOT-RIGHTOF12-ROBOT))
(not (NOT-RIGHTOF11-ROBOT))
(not (NOT-RIGHTOF10-ROBOT))
(not (NOT-RIGHTOF9-ROBOT))
(not (NOT-RIGHTOF8-ROBOT))
(not (NOT-RIGHTOF7-ROBOT))
(not (NOT-RIGHTOF6-ROBOT))
(not (NOT-RIGHTOF5-ROBOT))
(not (NOT-RIGHTOF4-ROBOT))
(not (NOT-RIGHTOF3-ROBOT))
(not (NOT-RIGHTOF2-ROBOT))
)
)
(when
(and
(RIGHTOF16-ROBOT)
)
(and
(RIGHTOF17-ROBOT)
(RIGHTOF15-ROBOT)
(RIGHTOF14-ROBOT)
(RIGHTOF13-ROBOT)
(RIGHTOF12-ROBOT)
(RIGHTOF11-ROBOT)
(RIGHTOF10-ROBOT)
(RIGHTOF9-ROBOT)
(RIGHTOF8-ROBOT)
(RIGHTOF7-ROBOT)
(RIGHTOF6-ROBOT)
(RIGHTOF5-ROBOT)
(RIGHTOF4-ROBOT)
(RIGHTOF3-ROBOT)
(RIGHTOF2-ROBOT)
(not (NOT-RIGHTOF17-ROBOT))
(not (NOT-RIGHTOF16-ROBOT))
(not (NOT-RIGHTOF15-ROBOT))
(not (NOT-RIGHTOF14-ROBOT))
(not (NOT-RIGHTOF13-ROBOT))
(not (NOT-RIGHTOF12-ROBOT))
(not (NOT-RIGHTOF11-ROBOT))
(not (NOT-RIGHTOF10-ROBOT))
(not (NOT-RIGHTOF9-ROBOT))
(not (NOT-RIGHTOF8-ROBOT))
(not (NOT-RIGHTOF7-ROBOT))
(not (NOT-RIGHTOF6-ROBOT))
(not (NOT-RIGHTOF5-ROBOT))
(not (NOT-RIGHTOF4-ROBOT))
(not (NOT-RIGHTOF3-ROBOT))
(not (NOT-RIGHTOF2-ROBOT))
)
)
(when
(and
(RIGHTOF15-ROBOT)
)
(and
(RIGHTOF16-ROBOT)
(RIGHTOF14-ROBOT)
(RIGHTOF13-ROBOT)
(RIGHTOF12-ROBOT)
(RIGHTOF11-ROBOT)
(RIGHTOF10-ROBOT)
(RIGHTOF9-ROBOT)
(RIGHTOF8-ROBOT)
(RIGHTOF7-ROBOT)
(RIGHTOF6-ROBOT)
(RIGHTOF5-ROBOT)
(RIGHTOF4-ROBOT)
(RIGHTOF3-ROBOT)
(RIGHTOF2-ROBOT)
(not (NOT-RIGHTOF16-ROBOT))
(not (NOT-RIGHTOF15-ROBOT))
(not (NOT-RIGHTOF14-ROBOT))
(not (NOT-RIGHTOF13-ROBOT))
(not (NOT-RIGHTOF12-ROBOT))
(not (NOT-RIGHTOF11-ROBOT))
(not (NOT-RIGHTOF10-ROBOT))
(not (NOT-RIGHTOF9-ROBOT))
(not (NOT-RIGHTOF8-ROBOT))
(not (NOT-RIGHTOF7-ROBOT))
(not (NOT-RIGHTOF6-ROBOT))
(not (NOT-RIGHTOF5-ROBOT))
(not (NOT-RIGHTOF4-ROBOT))
(not (NOT-RIGHTOF3-ROBOT))
(not (NOT-RIGHTOF2-ROBOT))
)
)
(when
(and
(RIGHTOF14-ROBOT)
)
(and
(RIGHTOF15-ROBOT)
(RIGHTOF13-ROBOT)
(RIGHTOF12-ROBOT)
(RIGHTOF11-ROBOT)
(RIGHTOF10-ROBOT)
(RIGHTOF9-ROBOT)
(RIGHTOF8-ROBOT)
(RIGHTOF7-ROBOT)
(RIGHTOF6-ROBOT)
(RIGHTOF5-ROBOT)
(RIGHTOF4-ROBOT)
(RIGHTOF3-ROBOT)
(RIGHTOF2-ROBOT)
(not (NOT-RIGHTOF15-ROBOT))
(not (NOT-RIGHTOF14-ROBOT))
(not (NOT-RIGHTOF13-ROBOT))
(not (NOT-RIGHTOF12-ROBOT))
(not (NOT-RIGHTOF11-ROBOT))
(not (NOT-RIGHTOF10-ROBOT))
(not (NOT-RIGHTOF9-ROBOT))
(not (NOT-RIGHTOF8-ROBOT))
(not (NOT-RIGHTOF7-ROBOT))
(not (NOT-RIGHTOF6-ROBOT))
(not (NOT-RIGHTOF5-ROBOT))
(not (NOT-RIGHTOF4-ROBOT))
(not (NOT-RIGHTOF3-ROBOT))
(not (NOT-RIGHTOF2-ROBOT))
)
)
(when
(and
(RIGHTOF13-ROBOT)
)
(and
(RIGHTOF14-ROBOT)
(RIGHTOF12-ROBOT)
(RIGHTOF11-ROBOT)
(RIGHTOF10-ROBOT)
(RIGHTOF9-ROBOT)
(RIGHTOF8-ROBOT)
(RIGHTOF7-ROBOT)
(RIGHTOF6-ROBOT)
(RIGHTOF5-ROBOT)
(RIGHTOF4-ROBOT)
(RIGHTOF3-ROBOT)
(RIGHTOF2-ROBOT)
(not (NOT-RIGHTOF14-ROBOT))
(not (NOT-RIGHTOF13-ROBOT))
(not (NOT-RIGHTOF12-ROBOT))
(not (NOT-RIGHTOF11-ROBOT))
(not (NOT-RIGHTOF10-ROBOT))
(not (NOT-RIGHTOF9-ROBOT))
(not (NOT-RIGHTOF8-ROBOT))
(not (NOT-RIGHTOF7-ROBOT))
(not (NOT-RIGHTOF6-ROBOT))
(not (NOT-RIGHTOF5-ROBOT))
(not (NOT-RIGHTOF4-ROBOT))
(not (NOT-RIGHTOF3-ROBOT))
(not (NOT-RIGHTOF2-ROBOT))
)
)
(when
(and
(RIGHTOF12-ROBOT)
)
(and
(RIGHTOF13-ROBOT)
(RIGHTOF11-ROBOT)
(RIGHTOF10-ROBOT)
(RIGHTOF9-ROBOT)
(RIGHTOF8-ROBOT)
(RIGHTOF7-ROBOT)
(RIGHTOF6-ROBOT)
(RIGHTOF5-ROBOT)
(RIGHTOF4-ROBOT)
(RIGHTOF3-ROBOT)
(RIGHTOF2-ROBOT)
(not (NOT-RIGHTOF13-ROBOT))
(not (NOT-RIGHTOF12-ROBOT))
(not (NOT-RIGHTOF11-ROBOT))
(not (NOT-RIGHTOF10-ROBOT))
(not (NOT-RIGHTOF9-ROBOT))
(not (NOT-RIGHTOF8-ROBOT))
(not (NOT-RIGHTOF7-ROBOT))
(not (NOT-RIGHTOF6-ROBOT))
(not (NOT-RIGHTOF5-ROBOT))
(not (NOT-RIGHTOF4-ROBOT))
(not (NOT-RIGHTOF3-ROBOT))
(not (NOT-RIGHTOF2-ROBOT))
)
)
(when
(and
(RIGHTOF11-ROBOT)
)
(and
(RIGHTOF12-ROBOT)
(RIGHTOF10-ROBOT)
(RIGHTOF9-ROBOT)
(RIGHTOF8-ROBOT)
(RIGHTOF7-ROBOT)
(RIGHTOF6-ROBOT)
(RIGHTOF5-ROBOT)
(RIGHTOF4-ROBOT)
(RIGHTOF3-ROBOT)
(RIGHTOF2-ROBOT)
(not (NOT-RIGHTOF12-ROBOT))
(not (NOT-RIGHTOF11-ROBOT))
(not (NOT-RIGHTOF10-ROBOT))
(not (NOT-RIGHTOF9-ROBOT))
(not (NOT-RIGHTOF8-ROBOT))
(not (NOT-RIGHTOF7-ROBOT))
(not (NOT-RIGHTOF6-ROBOT))
(not (NOT-RIGHTOF5-ROBOT))
(not (NOT-RIGHTOF4-ROBOT))
(not (NOT-RIGHTOF3-ROBOT))
(not (NOT-RIGHTOF2-ROBOT))
)
)
(when
(and
(RIGHTOF10-ROBOT)
)
(and
(RIGHTOF11-ROBOT)
(RIGHTOF9-ROBOT)
(RIGHTOF8-ROBOT)
(RIGHTOF7-ROBOT)
(RIGHTOF6-ROBOT)
(RIGHTOF5-ROBOT)
(RIGHTOF4-ROBOT)
(RIGHTOF3-ROBOT)
(RIGHTOF2-ROBOT)
(not (NOT-RIGHTOF11-ROBOT))
(not (NOT-RIGHTOF10-ROBOT))
(not (NOT-RIGHTOF9-ROBOT))
(not (NOT-RIGHTOF8-ROBOT))
(not (NOT-RIGHTOF7-ROBOT))
(not (NOT-RIGHTOF6-ROBOT))
(not (NOT-RIGHTOF5-ROBOT))
(not (NOT-RIGHTOF4-ROBOT))
(not (NOT-RIGHTOF3-ROBOT))
(not (NOT-RIGHTOF2-ROBOT))
)
)
(when
(and
(RIGHTOF9-ROBOT)
)
(and
(RIGHTOF10-ROBOT)
(RIGHTOF8-ROBOT)
(RIGHTOF7-ROBOT)
(RIGHTOF6-ROBOT)
(RIGHTOF5-ROBOT)
(RIGHTOF4-ROBOT)
(RIGHTOF3-ROBOT)
(RIGHTOF2-ROBOT)
(not (NOT-RIGHTOF10-ROBOT))
(not (NOT-RIGHTOF9-ROBOT))
(not (NOT-RIGHTOF8-ROBOT))
(not (NOT-RIGHTOF7-ROBOT))
(not (NOT-RIGHTOF6-ROBOT))
(not (NOT-RIGHTOF5-ROBOT))
(not (NOT-RIGHTOF4-ROBOT))
(not (NOT-RIGHTOF3-ROBOT))
(not (NOT-RIGHTOF2-ROBOT))
)
)
(when
(and
(RIGHTOF8-ROBOT)
)
(and
(RIGHTOF9-ROBOT)
(RIGHTOF7-ROBOT)
(RIGHTOF6-ROBOT)
(RIGHTOF5-ROBOT)
(RIGHTOF4-ROBOT)
(RIGHTOF3-ROBOT)
(RIGHTOF2-ROBOT)
(not (NOT-RIGHTOF9-ROBOT))
(not (NOT-RIGHTOF8-ROBOT))
(not (NOT-RIGHTOF7-ROBOT))
(not (NOT-RIGHTOF6-ROBOT))
(not (NOT-RIGHTOF5-ROBOT))
(not (NOT-RIGHTOF4-ROBOT))
(not (NOT-RIGHTOF3-ROBOT))
(not (NOT-RIGHTOF2-ROBOT))
)
)
(when
(and
(RIGHTOF7-ROBOT)
)
(and
(RIGHTOF8-ROBOT)
(RIGHTOF6-ROBOT)
(RIGHTOF5-ROBOT)
(RIGHTOF4-ROBOT)
(RIGHTOF3-ROBOT)
(RIGHTOF2-ROBOT)
(not (NOT-RIGHTOF8-ROBOT))
(not (NOT-RIGHTOF7-ROBOT))
(not (NOT-RIGHTOF6-ROBOT))
(not (NOT-RIGHTOF5-ROBOT))
(not (NOT-RIGHTOF4-ROBOT))
(not (NOT-RIGHTOF3-ROBOT))
(not (NOT-RIGHTOF2-ROBOT))
)
)
(when
(and
(RIGHTOF6-ROBOT)
)
(and
(RIGHTOF7-ROBOT)
(RIGHTOF5-ROBOT)
(RIGHTOF4-ROBOT)
(RIGHTOF3-ROBOT)
(RIGHTOF2-ROBOT)
(not (NOT-RIGHTOF7-ROBOT))
(not (NOT-RIGHTOF6-ROBOT))
(not (NOT-RIGHTOF5-ROBOT))
(not (NOT-RIGHTOF4-ROBOT))
(not (NOT-RIGHTOF3-ROBOT))
(not (NOT-RIGHTOF2-ROBOT))
)
)
(when
(and
(RIGHTOF5-ROBOT)
)
(and
(RIGHTOF6-ROBOT)
(RIGHTOF4-ROBOT)
(RIGHTOF3-ROBOT)
(RIGHTOF2-ROBOT)
(not (NOT-RIGHTOF6-ROBOT))
(not (NOT-RIGHTOF5-ROBOT))
(not (NOT-RIGHTOF4-ROBOT))
(not (NOT-RIGHTOF3-ROBOT))
(not (NOT-RIGHTOF2-ROBOT))
)
)
(when
(and
(RIGHTOF4-ROBOT)
)
(and
(RIGHTOF5-ROBOT)
(RIGHTOF3-ROBOT)
(RIGHTOF2-ROBOT)
(not (NOT-RIGHTOF5-ROBOT))
(not (NOT-RIGHTOF4-ROBOT))
(not (NOT-RIGHTOF3-ROBOT))
(not (NOT-RIGHTOF2-ROBOT))
)
)
(when
(and
(RIGHTOF3-ROBOT)
)
(and
(RIGHTOF4-ROBOT)
(RIGHTOF2-ROBOT)
(not (NOT-RIGHTOF4-ROBOT))
(not (NOT-RIGHTOF3-ROBOT))
(not (NOT-RIGHTOF2-ROBOT))
)
)
(when
(and
(RIGHTOF2-ROBOT)
)
(and
(RIGHTOF3-ROBOT)
(not (NOT-RIGHTOF3-ROBOT))
(not (NOT-RIGHTOF2-ROBOT))
)
)
(when
(and
(RIGHTOF1-ROBOT)
)
(and
(RIGHTOF2-ROBOT)
(not (NOT-RIGHTOF2-ROBOT))
)
)
(when
(and
(Flag261-)
)
(and
(RIGHTOF1-ROBOT)
(not (NOT-RIGHTOF1-ROBOT))
)
)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action MOVELEFT-ROBOT
:parameters ()
:precondition
(and
(Flag4-)
)
:effect
(and
(when
(and
(Flag260-)
)
(and
(COLUMN17-ROBOT)
(not (NOT-COLUMN17-ROBOT))
)
)
(when
(and
(Flag258-)
)
(and
(COLUMN16-ROBOT)
(not (NOT-COLUMN16-ROBOT))
)
)
(when
(and
(Flag255-)
)
(and
(COLUMN15-ROBOT)
(not (NOT-COLUMN15-ROBOT))
)
)
(when
(and
(Flag251-)
)
(and
(COLUMN14-ROBOT)
(not (NOT-COLUMN14-ROBOT))
)
)
(when
(and
(Flag245-)
)
(and
(COLUMN13-ROBOT)
(not (NOT-COLUMN13-ROBOT))
)
)
(when
(and
(Flag238-)
)
(and
(COLUMN12-ROBOT)
(not (NOT-COLUMN12-ROBOT))
)
)
(when
(and
(Flag231-)
)
(and
(COLUMN11-ROBOT)
(not (NOT-COLUMN11-ROBOT))
)
)
(when
(and
(Flag222-)
)
(and
(COLUMN10-ROBOT)
(not (NOT-COLUMN10-ROBOT))
)
)
(when
(and
(Flag213-)
)
(and
(COLUMN9-ROBOT)
(not (NOT-COLUMN9-ROBOT))
)
)
(when
(and
(Flag202-)
)
(and
(COLUMN8-ROBOT)
(not (NOT-COLUMN8-ROBOT))
)
)
(when
(and
(Flag191-)
)
(and
(COLUMN7-ROBOT)
(not (NOT-COLUMN7-ROBOT))
)
)
(when
(and
(Flag178-)
)
(and
(COLUMN6-ROBOT)
(not (NOT-COLUMN6-ROBOT))
)
)
(when
(and
(Flag164-)
)
(and
(COLUMN5-ROBOT)
(not (NOT-COLUMN5-ROBOT))
)
)
(when
(and
(Flag148-)
)
(and
(COLUMN4-ROBOT)
(not (NOT-COLUMN4-ROBOT))
)
)
(when
(and
(Flag133-)
)
(and
(COLUMN3-ROBOT)
(not (NOT-COLUMN3-ROBOT))
)
)
(when
(and
(Flag114-)
)
(and
(COLUMN2-ROBOT)
(not (NOT-COLUMN2-ROBOT))
)
)
(when
(and
(Flag95-)
)
(and
(COLUMN1-ROBOT)
(not (NOT-COLUMN1-ROBOT))
)
)
(when
(and
(Flag77-)
)
(and
(COLUMN0-ROBOT)
(not (NOT-COLUMN0-ROBOT))
)
)
(when
(and
(COLUMN18-ROBOT)
)
(and
(COLUMN17-ROBOT)
(NOT-COLUMN18-ROBOT)
(not (NOT-COLUMN17-ROBOT))
(not (COLUMN18-ROBOT))
)
)
(when
(and
(COLUMN17-ROBOT)
)
(and
(COLUMN16-ROBOT)
(NOT-COLUMN17-ROBOT)
(not (NOT-COLUMN16-ROBOT))
(not (COLUMN17-ROBOT))
)
)
(when
(and
(COLUMN16-ROBOT)
)
(and
(COLUMN15-ROBOT)
(NOT-COLUMN16-ROBOT)
(not (NOT-COLUMN15-ROBOT))
(not (COLUMN16-ROBOT))
)
)
(when
(and
(COLUMN15-ROBOT)
)
(and
(COLUMN14-ROBOT)
(NOT-COLUMN15-ROBOT)
(not (NOT-COLUMN14-ROBOT))
(not (COLUMN15-ROBOT))
)
)
(when
(and
(COLUMN14-ROBOT)
)
(and
(COLUMN13-ROBOT)
(NOT-COLUMN14-ROBOT)
(not (NOT-COLUMN13-ROBOT))
(not (COLUMN14-ROBOT))
)
)
(when
(and
(COLUMN13-ROBOT)
)
(and
(COLUMN12-ROBOT)
(NOT-COLUMN13-ROBOT)
(not (NOT-COLUMN12-ROBOT))
(not (COLUMN13-ROBOT))
)
)
(when
(and
(COLUMN12-ROBOT)
)
(and
(COLUMN11-ROBOT)
(NOT-COLUMN12-ROBOT)
(not (NOT-COLUMN11-ROBOT))
(not (COLUMN12-ROBOT))
)
)
(when
(and
(COLUMN11-ROBOT)
)
(and
(COLUMN10-ROBOT)
(NOT-COLUMN11-ROBOT)
(not (NOT-COLUMN10-ROBOT))
(not (COLUMN11-ROBOT))
)
)
(when
(and
(COLUMN10-ROBOT)
)
(and
(COLUMN9-ROBOT)
(NOT-COLUMN10-ROBOT)
(not (NOT-COLUMN9-ROBOT))
(not (COLUMN10-ROBOT))
)
)
(when
(and
(COLUMN9-ROBOT)
)
(and
(COLUMN8-ROBOT)
(NOT-COLUMN9-ROBOT)
(not (NOT-COLUMN8-ROBOT))
(not (COLUMN9-ROBOT))
)
)
(when
(and
(COLUMN8-ROBOT)
)
(and
(COLUMN7-ROBOT)
(NOT-COLUMN8-ROBOT)
(not (NOT-COLUMN7-ROBOT))
(not (COLUMN8-ROBOT))
)
)
(when
(and
(COLUMN7-ROBOT)
)
(and
(COLUMN6-ROBOT)
(NOT-COLUMN7-ROBOT)
(not (NOT-COLUMN6-ROBOT))
(not (COLUMN7-ROBOT))
)
)
(when
(and
(COLUMN6-ROBOT)
)
(and
(COLUMN5-ROBOT)
(NOT-COLUMN6-ROBOT)
(not (NOT-COLUMN5-ROBOT))
(not (COLUMN6-ROBOT))
)
)
(when
(and
(COLUMN5-ROBOT)
)
(and
(COLUMN4-ROBOT)
(NOT-COLUMN5-ROBOT)
(not (NOT-COLUMN4-ROBOT))
(not (COLUMN5-ROBOT))
)
)
(when
(and
(COLUMN4-ROBOT)
)
(and
(COLUMN3-ROBOT)
(NOT-COLUMN4-ROBOT)
(not (NOT-COLUMN3-ROBOT))
(not (COLUMN4-ROBOT))
)
)
(when
(and
(COLUMN3-ROBOT)
)
(and
(COLUMN2-ROBOT)
(NOT-COLUMN3-ROBOT)
(not (NOT-COLUMN2-ROBOT))
(not (COLUMN3-ROBOT))
)
)
(when
(and
(COLUMN2-ROBOT)
)
(and
(COLUMN1-ROBOT)
(NOT-COLUMN2-ROBOT)
(not (NOT-COLUMN1-ROBOT))
(not (COLUMN2-ROBOT))
)
)
(when
(and
(COLUMN1-ROBOT)
)
(and
(COLUMN0-ROBOT)
(NOT-COLUMN1-ROBOT)
(not (NOT-COLUMN0-ROBOT))
(not (COLUMN1-ROBOT))
)
)
(when
(and
(RIGHTOF18-ROBOT)
)
(and
(RIGHTOF17-ROBOT)
(NOT-RIGHTOF18-ROBOT)
(not (NOT-RIGHTOF17-ROBOT))
(not (RIGHTOF18-ROBOT))
)
)
(when
(and
(Flag39-)
)
(and
(RIGHTOF16-ROBOT)
(NOT-RIGHTOF17-ROBOT)
(not (NOT-RIGHTOF16-ROBOT))
(not (RIGHTOF17-ROBOT))
)
)
(when
(and
(Flag38-)
)
(and
(RIGHTOF15-ROBOT)
(NOT-RIGHTOF16-ROBOT)
(not (NOT-RIGHTOF15-ROBOT))
(not (RIGHTOF16-ROBOT))
)
)
(when
(and
(Flag37-)
)
(and
(RIGHTOF14-ROBOT)
(NOT-RIGHTOF15-ROBOT)
(not (NOT-RIGHTOF14-ROBOT))
(not (RIGHTOF15-ROBOT))
)
)
(when
(and
(Flag36-)
)
(and
(RIGHTOF13-ROBOT)
(NOT-RIGHTOF14-ROBOT)
(not (NOT-RIGHTOF13-ROBOT))
(not (RIGHTOF14-ROBOT))
)
)
(when
(and
(Flag35-)
)
(and
(RIGHTOF12-ROBOT)
(NOT-RIGHTOF13-ROBOT)
(not (NOT-RIGHTOF12-ROBOT))
(not (RIGHTOF13-ROBOT))
)
)
(when
(and
(Flag34-)
)
(and
(RIGHTOF11-ROBOT)
(NOT-RIGHTOF12-ROBOT)
(not (NOT-RIGHTOF11-ROBOT))
(not (RIGHTOF12-ROBOT))
)
)
(when
(and
(Flag33-)
)
(and
(RIGHTOF10-ROBOT)
(NOT-RIGHTOF11-ROBOT)
(not (NOT-RIGHTOF10-ROBOT))
(not (RIGHTOF11-ROBOT))
)
)
(when
(and
(Flag32-)
)
(and
(RIGHTOF9-ROBOT)
(NOT-RIGHTOF10-ROBOT)
(not (NOT-RIGHTOF9-ROBOT))
(not (RIGHTOF10-ROBOT))
)
)
(when
(and
(Flag31-)
)
(and
(RIGHTOF8-ROBOT)
(NOT-RIGHTOF9-ROBOT)
(not (NOT-RIGHTOF8-ROBOT))
(not (RIGHTOF9-ROBOT))
)
)
(when
(and
(Flag30-)
)
(and
(RIGHTOF7-ROBOT)
(NOT-RIGHTOF8-ROBOT)
(not (NOT-RIGHTOF7-ROBOT))
(not (RIGHTOF8-ROBOT))
)
)
(when
(and
(Flag29-)
)
(and
(RIGHTOF6-ROBOT)
(NOT-RIGHTOF7-ROBOT)
(not (NOT-RIGHTOF6-ROBOT))
(not (RIGHTOF7-ROBOT))
)
)
(when
(and
(Flag28-)
)
(and
(RIGHTOF5-ROBOT)
(NOT-RIGHTOF6-ROBOT)
(not (NOT-RIGHTOF5-ROBOT))
(not (RIGHTOF6-ROBOT))
)
)
(when
(and
(Flag27-)
)
(and
(RIGHTOF4-ROBOT)
(NOT-RIGHTOF5-ROBOT)
(not (NOT-RIGHTOF4-ROBOT))
(not (RIGHTOF5-ROBOT))
)
)
(when
(and
(Flag26-)
)
(and
(RIGHTOF3-ROBOT)
(NOT-RIGHTOF4-ROBOT)
(not (NOT-RIGHTOF3-ROBOT))
(not (RIGHTOF4-ROBOT))
)
)
(when
(and
(Flag25-)
)
(and
(RIGHTOF2-ROBOT)
(NOT-RIGHTOF3-ROBOT)
(not (NOT-RIGHTOF2-ROBOT))
(not (RIGHTOF3-ROBOT))
)
)
(when
(and
(Flag24-)
)
(and
(RIGHTOF1-ROBOT)
(NOT-RIGHTOF2-ROBOT)
(not (NOT-RIGHTOF1-ROBOT))
(not (RIGHTOF2-ROBOT))
)
)
(when
(and
(Flag23-)
)
(and
(RIGHTOF0-ROBOT)
(NOT-RIGHTOF1-ROBOT)
(not (NOT-RIGHTOF0-ROBOT))
(not (RIGHTOF1-ROBOT))
)
)
(when
(and
(Flag22-)
)
(and
(LEFTOF18-ROBOT)
(not (NOT-LEFTOF18-ROBOT))
)
)
(when
(and
(Flag21-)
)
(and
(LEFTOF17-ROBOT)
(not (NOT-LEFTOF17-ROBOT))
)
)
(when
(and
(Flag20-)
)
(and
(LEFTOF16-ROBOT)
(not (NOT-LEFTOF16-ROBOT))
)
)
(when
(and
(Flag19-)
)
(and
(LEFTOF15-ROBOT)
(not (NOT-LEFTOF15-ROBOT))
)
)
(when
(and
(Flag18-)
)
(and
(LEFTOF14-ROBOT)
(not (NOT-LEFTOF14-ROBOT))
)
)
(when
(and
(Flag17-)
)
(and
(LEFTOF13-ROBOT)
(not (NOT-LEFTOF13-ROBOT))
)
)
(when
(and
(Flag16-)
)
(and
(LEFTOF12-ROBOT)
(not (NOT-LEFTOF12-ROBOT))
)
)
(when
(and
(Flag15-)
)
(and
(LEFTOF11-ROBOT)
(not (NOT-LEFTOF11-ROBOT))
)
)
(when
(and
(Flag14-)
)
(and
(LEFTOF10-ROBOT)
(not (NOT-LEFTOF10-ROBOT))
)
)
(when
(and
(Flag13-)
)
(and
(LEFTOF9-ROBOT)
(not (NOT-LEFTOF9-ROBOT))
)
)
(when
(and
(Flag12-)
)
(and
(LEFTOF8-ROBOT)
(not (NOT-LEFTOF8-ROBOT))
)
)
(when
(and
(Flag11-)
)
(and
(LEFTOF7-ROBOT)
(not (NOT-LEFTOF7-ROBOT))
)
)
(when
(and
(Flag10-)
)
(and
(LEFTOF6-ROBOT)
(not (NOT-LEFTOF6-ROBOT))
)
)
(when
(and
(Flag9-)
)
(and
(LEFTOF5-ROBOT)
(not (NOT-LEFTOF5-ROBOT))
)
)
(when
(and
(Flag8-)
)
(and
(LEFTOF4-ROBOT)
(not (NOT-LEFTOF4-ROBOT))
)
)
(when
(and
(Flag7-)
)
(and
(LEFTOF3-ROBOT)
(not (NOT-LEFTOF3-ROBOT))
)
)
(when
(and
(Flag6-)
)
(and
(LEFTOF2-ROBOT)
(not (NOT-LEFTOF2-ROBOT))
)
)
(when
(and
(Flag5-)
)
(and
(LEFTOF1-ROBOT)
(not (NOT-LEFTOF1-ROBOT))
)
)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action CHECKCONSISTENCYACTION
:parameters ()
:precondition
(and
(NOT-ERROR-)
(CHECKCONSISTENCY-)
)
:effect
(and
(when
(and
(LEFTOF5-ROBOT)
(RIGHTOF17-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF17-ROBOT)
(LEFTOF3-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF8-ROBOT)
(ABOVEOF16-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF11-ROBOT)
(LEFTOF4-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF17-ROBOT)
(BELOWOF3-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF4-ROBOT)
(RIGHTOF18-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF4-ROBOT)
(RIGHTOF15-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF17-ROBOT)
(BELOWOF6-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF1-ROBOT)
(RIGHTOF13-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF10-ROBOT)
(BELOWOF2-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF5-ROBOT)
(RIGHTOF15-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF5-ROBOT)
(ABOVEOF12-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF8-ROBOT)
(ABOVEOF15-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF16-ROBOT)
(BELOWOF1-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF4-ROBOT)
(ABOVEOF15-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF17-ROBOT)
(BELOWOF10-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF2-ROBOT)
(ABOVEOF8-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF14-ROBOT)
(ABOVEOF15-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF5-ROBOT)
(RIGHTOF12-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF1-ROBOT)
(RIGHTOF7-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF13-ROBOT)
(LEFTOF2-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF15-ROBOT)
(BELOWOF1-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF2-ROBOT)
(ABOVEOF18-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF9-ROBOT)
(RIGHTOF12-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF15-ROBOT)
(LEFTOF11-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF18-ROBOT)
(BELOWOF11-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF7-ROBOT)
(LEFTOF4-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF13-ROBOT)
(RIGHTOF18-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF2-ROBOT)
(RIGHTOF5-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF18-ROBOT)
(BELOWOF6-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF11-ROBOT)
(LEFTOF2-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF6-ROBOT)
(RIGHTOF17-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF9-ROBOT)
(LEFTOF2-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF1-ROBOT)
(ABOVEOF10-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF6-ROBOT)
(BELOWOF3-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF11-ROBOT)
(BELOWOF8-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF8-ROBOT)
(RIGHTOF14-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF15-ROBOT)
(LEFTOF2-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF3-ROBOT)
(ABOVEOF12-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF16-ROBOT)
(LEFTOF8-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF3-ROBOT)
(RIGHTOF14-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF17-ROBOT)
(LEFTOF13-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF3-ROBOT)
(RIGHTOF5-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF5-ROBOT)
(ABOVEOF16-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF3-ROBOT)
(RIGHTOF9-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF14-ROBOT)
(BELOWOF1-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF3-ROBOT)
(ABOVEOF14-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF4-ROBOT)
(RIGHTOF10-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF12-ROBOT)
(BELOWOF10-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF16-ROBOT)
(BELOWOF2-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF15-ROBOT)
(LEFTOF6-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF16-ROBOT)
(BELOWOF11-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF11-ROBOT)
(BELOWOF1-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF4-ROBOT)
(RIGHTOF12-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF16-ROBOT)
(LEFTOF7-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF5-ROBOT)
(RIGHTOF9-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF18-ROBOT)
(BELOWOF7-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF5-ROBOT)
(ABOVEOF17-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF14-ROBOT)
(LEFTOF5-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF6-ROBOT)
(ABOVEOF10-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF17-ROBOT)
(BELOWOF1-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF6-ROBOT)
(ABOVEOF14-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF8-ROBOT)
(BELOWOF1-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF14-ROBOT)
(BELOWOF7-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF1-ROBOT)
(ABOVEOF18-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF13-ROBOT)
(BELOWOF1-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF16-ROBOT)
(LEFTOF12-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF8-ROBOT)
(ABOVEOF18-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF3-ROBOT)
(ABOVEOF9-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF10-ROBOT)
(RIGHTOF16-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF11-ROBOT)
(LEFTOF3-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF13-ROBOT)
(BELOWOF3-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF5-ROBOT)
(RIGHTOF18-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF6-ROBOT)
(ABOVEOF16-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF18-ROBOT)
(LEFTOF8-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF3-ROBOT)
(RIGHTOF13-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF7-ROBOT)
(RIGHTOF8-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF14-ROBOT)
(LEFTOF1-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF9-ROBOT)
(ABOVEOF12-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF10-ROBOT)
(BELOWOF3-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF13-ROBOT)
(BELOWOF7-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF3-ROBOT)
(RIGHTOF16-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF15-ROBOT)
(LEFTOF8-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF14-ROBOT)
(LEFTOF2-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF7-ROBOT)
(LEFTOF5-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF4-ROBOT)
(ABOVEOF16-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF17-ROBOT)
(LEFTOF7-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF8-ROBOT)
(LEFTOF3-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF2-ROBOT)
(RIGHTOF16-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF12-ROBOT)
(BELOWOF4-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF18-ROBOT)
(LEFTOF9-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF15-ROBOT)
(BELOWOF11-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF12-ROBOT)
(LEFTOF2-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF18-ROBOT)
(LEFTOF1-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF15-ROBOT)
(LEFTOF3-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF15-ROBOT)
(LEFTOF9-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF14-ROBOT)
(LEFTOF9-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF15-ROBOT)
(BELOWOF6-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF18-ROBOT)
(BELOWOF5-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF6-ROBOT)
(ABOVEOF12-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF17-ROBOT)
(LEFTOF2-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF17-ROBOT)
(BELOWOF12-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF5-ROBOT)
(ABOVEOF10-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF18-ROBOT)
(LEFTOF12-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF13-ROBOT)
(RIGHTOF14-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF12-ROBOT)
(BELOWOF1-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF1-ROBOT)
(RIGHTOF15-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF10-ROBOT)
(ABOVEOF14-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF2-ROBOT)
(ABOVEOF12-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF5-ROBOT)
(RIGHTOF10-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF6-ROBOT)
(RIGHTOF11-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF18-ROBOT)
(LEFTOF14-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF9-ROBOT)
(BELOWOF1-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF7-ROBOT)
(RIGHTOF14-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF13-ROBOT)
(BELOWOF6-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF7-ROBOT)
(ABOVEOF12-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF1-ROBOT)
(RIGHTOF5-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF14-ROBOT)
(BELOWOF9-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF8-ROBOT)
(LEFTOF1-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF11-ROBOT)
(BELOWOF10-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF1-ROBOT)
(ABOVEOF4-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF6-ROBOT)
(RIGHTOF14-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF17-ROBOT)
(BELOWOF4-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF4-ROBOT)
(ABOVEOF9-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF7-ROBOT)
(BELOWOF2-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF11-ROBOT)
(ABOVEOF12-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF11-ROBOT)
(ABOVEOF14-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF6-ROBOT)
(RIGHTOF16-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF6-ROBOT)
(ABOVEOF11-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF8-ROBOT)
(ABOVEOF9-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF4-ROBOT)
(RIGHTOF13-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF11-ROBOT)
(BELOWOF3-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF18-ROBOT)
(LEFTOF3-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF16-ROBOT)
(BELOWOF12-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF10-ROBOT)
(RIGHTOF13-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF4-ROBOT)
(ABOVEOF18-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF7-ROBOT)
(ABOVEOF16-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF3-ROBOT)
(ABOVEOF8-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF14-ROBOT)
(BELOWOF2-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF17-ROBOT)
(LEFTOF11-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF3-ROBOT)
(ABOVEOF5-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF7-ROBOT)
(RIGHTOF11-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF10-ROBOT)
(RIGHTOF18-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF13-ROBOT)
(BELOWOF11-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF16-ROBOT)
(BELOWOF3-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF6-ROBOT)
(LEFTOF2-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF11-ROBOT)
(RIGHTOF18-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF1-ROBOT)
(ABOVEOF1-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF4-ROBOT)
(LEFTOF2-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF18-ROBOT)
(BELOWOF10-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF15-ROBOT)
(LEFTOF7-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF17-ROBOT)
(LEFTOF9-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF6-ROBOT)
(RIGHTOF10-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF13-ROBOT)
(LEFTOF5-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF16-ROBOT)
(LEFTOF9-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF8-ROBOT)
(BELOWOF4-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF13-ROBOT)
(BELOWOF2-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF17-ROBOT)
(LEFTOF8-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF7-ROBOT)
(RIGHTOF13-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF15-ROBOT)
(BELOWOF7-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF10-ROBOT)
(ABOVEOF10-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF18-ROBOT)
(LEFTOF17-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF18-ROBOT)
(LEFTOF7-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF4-ROBOT)
(RIGHTOF8-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF17-ROBOT)
(BELOWOF8-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF13-ROBOT)
(BELOWOF8-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF11-ROBOT)
(BELOWOF2-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF3-ROBOT)
(ABOVEOF15-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF6-ROBOT)
(RIGHTOF13-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF10-ROBOT)
(LEFTOF3-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF3-ROBOT)
(ABOVEOF18-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF10-ROBOT)
(LEFTOF1-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF1-ROBOT)
(RIGHTOF4-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF16-ROBOT)
(BELOWOF13-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF2-ROBOT)
(ABOVEOF9-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF18-ROBOT)
(BELOWOF13-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF6-ROBOT)
(RIGHTOF12-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF5-ROBOT)
(BELOWOF4-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF5-ROBOT)
(RIGHTOF6-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF15-ROBOT)
(BELOWOF2-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF4-ROBOT)
(ABOVEOF6-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF4-ROBOT)
(ABOVEOF10-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF14-ROBOT)
(ABOVEOF17-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF11-ROBOT)
(RIGHTOF14-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF9-ROBOT)
(ABOVEOF16-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF10-ROBOT)
(RIGHTOF12-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF12-ROBOT)
(ABOVEOF12-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF17-ROBOT)
(LEFTOF4-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF18-ROBOT)
(BELOWOF16-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF11-ROBOT)
(BELOWOF5-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF17-ROBOT)
(BELOWOF15-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF7-ROBOT)
(LEFTOF2-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF15-ROBOT)
(BELOWOF10-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF12-ROBOT)
(RIGHTOF15-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF15-ROBOT)
(RIGHTOF18-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF12-ROBOT)
(LEFTOF1-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF1-ROBOT)
(RIGHTOF17-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF4-ROBOT)
(ABOVEOF13-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF13-ROBOT)
(BELOWOF12-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF11-ROBOT)
(BELOWOF4-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF17-ROBOT)
(BELOWOF7-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF18-ROBOT)
(LEFTOF2-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF2-ROBOT)
(ABOVEOF17-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF4-ROBOT)
(RIGHTOF14-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF12-ROBOT)
(RIGHTOF17-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF11-ROBOT)
(LEFTOF11-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF7-ROBOT)
(BELOWOF1-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF7-ROBOT)
(ABOVEOF10-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF6-ROBOT)
(LEFTOF4-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF6-ROBOT)
(RIGHTOF9-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF11-ROBOT)
(RIGHTOF13-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF7-ROBOT)
(BELOWOF6-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF18-ROBOT)
(BELOWOF12-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF15-ROBOT)
(BELOWOF9-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF16-ROBOT)
(LEFTOF5-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF5-ROBOT)
(RIGHTOF8-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF8-ROBOT)
(BELOWOF5-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF7-ROBOT)
(RIGHTOF12-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF16-ROBOT)
(BELOWOF10-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF2-ROBOT)
(RIGHTOF10-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF10-ROBOT)
(RIGHTOF17-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF9-ROBOT)
(LEFTOF4-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF16-ROBOT)
(LEFTOF1-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF10-ROBOT)
(RIGHTOF14-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF5-ROBOT)
(RIGHTOF11-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF9-ROBOT)
(RIGHTOF13-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF6-ROBOT)
(BELOWOF2-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF7-ROBOT)
(BELOWOF4-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF15-ROBOT)
(BELOWOF5-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF15-ROBOT)
(RIGHTOF16-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF11-ROBOT)
(LEFTOF8-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF2-ROBOT)
(RIGHTOF8-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF7-ROBOT)
(RIGHTOF10-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF8-ROBOT)
(ABOVEOF14-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF15-ROBOT)
(ABOVEOF16-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF6-ROBOT)
(RIGHTOF18-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF13-ROBOT)
(BELOWOF10-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF4-ROBOT)
(ABOVEOF14-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF10-ROBOT)
(BELOWOF8-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF7-ROBOT)
(BELOWOF5-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF3-ROBOT)
(RIGHTOF7-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF1-ROBOT)
(ABOVEOF3-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF9-ROBOT)
(LEFTOF7-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF1-ROBOT)
(RIGHTOF11-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF5-ROBOT)
(ABOVEOF13-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF5-ROBOT)
(ABOVEOF14-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF7-ROBOT)
(ABOVEOF11-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF6-ROBOT)
(BELOWOF6-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF6-ROBOT)
(RIGHTOF8-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF4-ROBOT)
(RIGHTOF16-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF13-ROBOT)
(RIGHTOF15-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF9-ROBOT)
(ABOVEOF18-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF6-ROBOT)
(BELOWOF5-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF15-ROBOT)
(ABOVEOF18-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF6-ROBOT)
(BELOWOF1-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF3-ROBOT)
(ABOVEOF3-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF15-ROBOT)
(RIGHTOF17-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF18-ROBOT)
(BELOWOF17-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF17-ROBOT)
(BELOWOF11-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF5-ROBOT)
(BELOWOF1-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF6-ROBOT)
(ABOVEOF9-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF9-ROBOT)
(LEFTOF1-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF3-ROBOT)
(RIGHTOF6-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF4-ROBOT)
(BELOWOF2-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF11-ROBOT)
(BELOWOF11-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF7-ROBOT)
(BELOWOF7-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF15-ROBOT)
(BELOWOF12-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF17-ROBOT)
(LEFTOF14-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF17-ROBOT)
(BELOWOF13-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF9-ROBOT)
(RIGHTOF10-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF11-ROBOT)
(RIGHTOF16-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF9-ROBOT)
(BELOWOF5-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF9-ROBOT)
(RIGHTOF9-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF12-ROBOT)
(LEFTOF3-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF16-ROBOT)
(RIGHTOF18-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF10-ROBOT)
(LEFTOF8-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF9-ROBOT)
(BELOWOF7-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF4-ROBOT)
(ABOVEOF4-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF18-ROBOT)
(BELOWOF14-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF7-ROBOT)
(ABOVEOF8-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF14-ROBOT)
(LEFTOF12-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF13-ROBOT)
(RIGHTOF16-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF2-ROBOT)
(ABOVEOF2-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF3-ROBOT)
(ABOVEOF7-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF9-ROBOT)
(ABOVEOF13-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF12-ROBOT)
(ABOVEOF14-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF13-ROBOT)
(BELOWOF13-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF13-ROBOT)
(LEFTOF8-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF1-ROBOT)
(RIGHTOF6-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF1-ROBOT)
(RIGHTOF3-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF10-ROBOT)
(RIGHTOF15-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF14-ROBOT)
(RIGHTOF16-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF14-ROBOT)
(BELOWOF14-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF6-ROBOT)
(RIGHTOF6-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF3-ROBOT)
(BELOWOF2-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF17-ROBOT)
(BELOWOF17-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF14-ROBOT)
(RIGHTOF15-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF9-ROBOT)
(ABOVEOF10-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF5-ROBOT)
(BELOWOF2-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF1-ROBOT)
(RIGHTOF2-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF9-ROBOT)
(ABOVEOF17-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF12-ROBOT)
(RIGHTOF12-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF4-ROBOT)
(BELOWOF3-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF8-ROBOT)
(BELOWOF6-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF2-ROBOT)
(BELOWOF1-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF16-ROBOT)
(RIGHTOF17-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF4-ROBOT)
(RIGHTOF4-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF3-ROBOT)
(LEFTOF2-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF13-ROBOT)
(RIGHTOF13-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF15-ROBOT)
(ABOVEOF15-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF18-ROBOT)
(BELOWOF18-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF17-ROBOT)
(LEFTOF17-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF8-ROBOT)
(RIGHTOF12-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF11-ROBOT)
(RIGHTOF12-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF8-ROBOT)
(LEFTOF8-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF14-ROBOT)
(BELOWOF13-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF11-ROBOT)
(BELOWOF9-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF8-ROBOT)
(ABOVEOF12-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF13-ROBOT)
(ABOVEOF15-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF14-ROBOT)
(RIGHTOF14-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF16-ROBOT)
(BELOWOF14-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF10-ROBOT)
(RIGHTOF11-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF16-ROBOT)
(ABOVEOF16-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF10-ROBOT)
(LEFTOF10-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF9-ROBOT)
(LEFTOF8-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF17-ROBOT)
(BELOWOF16-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF9-ROBOT)
(RIGHTOF11-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF4-ROBOT)
(RIGHTOF5-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF5-ROBOT)
(ABOVEOF5-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF18-ROBOT)
(RIGHTOF18-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF7-ROBOT)
(LEFTOF6-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF3-ROBOT)
(RIGHTOF4-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF9-ROBOT)
(BELOWOF9-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF7-ROBOT)
(LEFTOF7-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF5-ROBOT)
(RIGHTOF5-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF16-ROBOT)
(RIGHTOF16-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF3-ROBOT)
(LEFTOF3-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF12-ROBOT)
(RIGHTOF13-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF2-ROBOT)
(RIGHTOF2-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF8-ROBOT)
(ABOVEOF8-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF15-ROBOT)
(RIGHTOF15-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF1-ROBOT)
(RIGHTOF1-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(NOT-CHECKCONSISTENCY-)
(not (CHECKCONSISTENCY-))
)
)
(:derived (Flag4-)(and (NOT-CHECKCONSISTENCY-)(NOT-ERROR-)(Flag3-)))

(:derived (Flag264-)(and (BELOWOF2-ROBOT)))

(:derived (Flag264-)(and (BELOWOF1-ROBOT)))

(:derived (Flag265-)(and (BELOWOF2-ROBOT)))

(:derived (Flag265-)(and (BELOWOF1-ROBOT)))

(:derived (Flag265-)(and (BELOWOF3-ROBOT)))

(:derived (Flag266-)(and (BELOWOF2-ROBOT)))

(:derived (Flag266-)(and (BELOWOF1-ROBOT)))

(:derived (Flag266-)(and (BELOWOF3-ROBOT)))

(:derived (Flag266-)(and (BELOWOF4-ROBOT)))

(:derived (Flag267-)(and (BELOWOF2-ROBOT)))

(:derived (Flag267-)(and (BELOWOF1-ROBOT)))

(:derived (Flag267-)(and (BELOWOF3-ROBOT)))

(:derived (Flag267-)(and (BELOWOF4-ROBOT)))

(:derived (Flag267-)(and (BELOWOF5-ROBOT)))

(:derived (Flag268-)(and (BELOWOF6-ROBOT)))

(:derived (Flag268-)(and (BELOWOF5-ROBOT)))

(:derived (Flag268-)(and (BELOWOF2-ROBOT)))

(:derived (Flag268-)(and (BELOWOF1-ROBOT)))

(:derived (Flag268-)(and (BELOWOF4-ROBOT)))

(:derived (Flag268-)(and (BELOWOF3-ROBOT)))

(:derived (Flag269-)(and (BELOWOF6-ROBOT)))

(:derived (Flag269-)(and (BELOWOF5-ROBOT)))

(:derived (Flag269-)(and (BELOWOF2-ROBOT)))

(:derived (Flag269-)(and (BELOWOF1-ROBOT)))

(:derived (Flag269-)(and (BELOWOF4-ROBOT)))

(:derived (Flag269-)(and (BELOWOF7-ROBOT)))

(:derived (Flag269-)(and (BELOWOF3-ROBOT)))

(:derived (Flag270-)(and (BELOWOF6-ROBOT)))

(:derived (Flag270-)(and (BELOWOF5-ROBOT)))

(:derived (Flag270-)(and (BELOWOF2-ROBOT)))

(:derived (Flag270-)(and (BELOWOF1-ROBOT)))

(:derived (Flag270-)(and (BELOWOF4-ROBOT)))

(:derived (Flag270-)(and (BELOWOF7-ROBOT)))

(:derived (Flag270-)(and (BELOWOF3-ROBOT)))

(:derived (Flag270-)(and (BELOWOF8-ROBOT)))

(:derived (Flag271-)(and (BELOWOF6-ROBOT)))

(:derived (Flag271-)(and (BELOWOF5-ROBOT)))

(:derived (Flag271-)(and (BELOWOF2-ROBOT)))

(:derived (Flag271-)(and (BELOWOF1-ROBOT)))

(:derived (Flag271-)(and (BELOWOF4-ROBOT)))

(:derived (Flag271-)(and (BELOWOF7-ROBOT)))

(:derived (Flag271-)(and (BELOWOF9-ROBOT)))

(:derived (Flag271-)(and (BELOWOF3-ROBOT)))

(:derived (Flag271-)(and (BELOWOF8-ROBOT)))

(:derived (Flag272-)(and (BELOWOF10-ROBOT)))

(:derived (Flag272-)(and (BELOWOF6-ROBOT)))

(:derived (Flag272-)(and (BELOWOF5-ROBOT)))

(:derived (Flag272-)(and (BELOWOF2-ROBOT)))

(:derived (Flag272-)(and (BELOWOF1-ROBOT)))

(:derived (Flag272-)(and (BELOWOF4-ROBOT)))

(:derived (Flag272-)(and (BELOWOF7-ROBOT)))

(:derived (Flag272-)(and (BELOWOF9-ROBOT)))

(:derived (Flag272-)(and (BELOWOF3-ROBOT)))

(:derived (Flag272-)(and (BELOWOF8-ROBOT)))

(:derived (Flag273-)(and (BELOWOF10-ROBOT)))

(:derived (Flag273-)(and (BELOWOF11-ROBOT)))

(:derived (Flag273-)(and (BELOWOF6-ROBOT)))

(:derived (Flag273-)(and (BELOWOF5-ROBOT)))

(:derived (Flag273-)(and (BELOWOF2-ROBOT)))

(:derived (Flag273-)(and (BELOWOF1-ROBOT)))

(:derived (Flag273-)(and (BELOWOF4-ROBOT)))

(:derived (Flag273-)(and (BELOWOF7-ROBOT)))

(:derived (Flag273-)(and (BELOWOF9-ROBOT)))

(:derived (Flag273-)(and (BELOWOF3-ROBOT)))

(:derived (Flag273-)(and (BELOWOF8-ROBOT)))

(:derived (Flag274-)(and (BELOWOF10-ROBOT)))

(:derived (Flag274-)(and (BELOWOF11-ROBOT)))

(:derived (Flag274-)(and (BELOWOF6-ROBOT)))

(:derived (Flag274-)(and (BELOWOF5-ROBOT)))

(:derived (Flag274-)(and (BELOWOF2-ROBOT)))

(:derived (Flag274-)(and (BELOWOF1-ROBOT)))

(:derived (Flag274-)(and (BELOWOF4-ROBOT)))

(:derived (Flag274-)(and (BELOWOF12-ROBOT)))

(:derived (Flag274-)(and (BELOWOF7-ROBOT)))

(:derived (Flag274-)(and (BELOWOF9-ROBOT)))

(:derived (Flag274-)(and (BELOWOF3-ROBOT)))

(:derived (Flag274-)(and (BELOWOF8-ROBOT)))

(:derived (Flag275-)(and (BELOWOF10-ROBOT)))

(:derived (Flag275-)(and (BELOWOF11-ROBOT)))

(:derived (Flag275-)(and (BELOWOF6-ROBOT)))

(:derived (Flag275-)(and (BELOWOF5-ROBOT)))

(:derived (Flag275-)(and (BELOWOF2-ROBOT)))

(:derived (Flag275-)(and (BELOWOF1-ROBOT)))

(:derived (Flag275-)(and (BELOWOF4-ROBOT)))

(:derived (Flag275-)(and (BELOWOF12-ROBOT)))

(:derived (Flag275-)(and (BELOWOF7-ROBOT)))

(:derived (Flag275-)(and (BELOWOF9-ROBOT)))

(:derived (Flag275-)(and (BELOWOF3-ROBOT)))

(:derived (Flag275-)(and (BELOWOF13-ROBOT)))

(:derived (Flag275-)(and (BELOWOF8-ROBOT)))

(:derived (Flag276-)(and (BELOWOF10-ROBOT)))

(:derived (Flag276-)(and (BELOWOF11-ROBOT)))

(:derived (Flag276-)(and (BELOWOF6-ROBOT)))

(:derived (Flag276-)(and (BELOWOF5-ROBOT)))

(:derived (Flag276-)(and (BELOWOF2-ROBOT)))

(:derived (Flag276-)(and (BELOWOF1-ROBOT)))

(:derived (Flag276-)(and (BELOWOF4-ROBOT)))

(:derived (Flag276-)(and (BELOWOF12-ROBOT)))

(:derived (Flag276-)(and (BELOWOF7-ROBOT)))

(:derived (Flag276-)(and (BELOWOF9-ROBOT)))

(:derived (Flag276-)(and (BELOWOF3-ROBOT)))

(:derived (Flag276-)(and (BELOWOF13-ROBOT)))

(:derived (Flag276-)(and (BELOWOF8-ROBOT)))

(:derived (Flag276-)(and (BELOWOF14-ROBOT)))

(:derived (Flag277-)(and (BELOWOF10-ROBOT)))

(:derived (Flag277-)(and (BELOWOF11-ROBOT)))

(:derived (Flag277-)(and (BELOWOF15-ROBOT)))

(:derived (Flag277-)(and (BELOWOF6-ROBOT)))

(:derived (Flag277-)(and (BELOWOF5-ROBOT)))

(:derived (Flag277-)(and (BELOWOF2-ROBOT)))

(:derived (Flag277-)(and (BELOWOF1-ROBOT)))

(:derived (Flag277-)(and (BELOWOF4-ROBOT)))

(:derived (Flag277-)(and (BELOWOF12-ROBOT)))

(:derived (Flag277-)(and (BELOWOF7-ROBOT)))

(:derived (Flag277-)(and (BELOWOF9-ROBOT)))

(:derived (Flag277-)(and (BELOWOF3-ROBOT)))

(:derived (Flag277-)(and (BELOWOF13-ROBOT)))

(:derived (Flag277-)(and (BELOWOF8-ROBOT)))

(:derived (Flag277-)(and (BELOWOF14-ROBOT)))

(:derived (Flag278-)(and (BELOWOF10-ROBOT)))

(:derived (Flag278-)(and (BELOWOF16-ROBOT)))

(:derived (Flag278-)(and (BELOWOF15-ROBOT)))

(:derived (Flag278-)(and (BELOWOF6-ROBOT)))

(:derived (Flag278-)(and (BELOWOF11-ROBOT)))

(:derived (Flag278-)(and (BELOWOF5-ROBOT)))

(:derived (Flag278-)(and (BELOWOF2-ROBOT)))

(:derived (Flag278-)(and (BELOWOF1-ROBOT)))

(:derived (Flag278-)(and (BELOWOF4-ROBOT)))

(:derived (Flag278-)(and (BELOWOF12-ROBOT)))

(:derived (Flag278-)(and (BELOWOF7-ROBOT)))

(:derived (Flag278-)(and (BELOWOF9-ROBOT)))

(:derived (Flag278-)(and (BELOWOF3-ROBOT)))

(:derived (Flag278-)(and (BELOWOF13-ROBOT)))

(:derived (Flag278-)(and (BELOWOF8-ROBOT)))

(:derived (Flag278-)(and (BELOWOF14-ROBOT)))

(:derived (Flag279-)(and (BELOWOF10-ROBOT)))

(:derived (Flag279-)(and (BELOWOF16-ROBOT)))

(:derived (Flag279-)(and (BELOWOF15-ROBOT)))

(:derived (Flag279-)(and (BELOWOF6-ROBOT)))

(:derived (Flag279-)(and (BELOWOF17-ROBOT)))

(:derived (Flag279-)(and (BELOWOF5-ROBOT)))

(:derived (Flag279-)(and (BELOWOF2-ROBOT)))

(:derived (Flag279-)(and (BELOWOF1-ROBOT)))

(:derived (Flag279-)(and (BELOWOF4-ROBOT)))

(:derived (Flag279-)(and (BELOWOF11-ROBOT)))

(:derived (Flag279-)(and (BELOWOF12-ROBOT)))

(:derived (Flag279-)(and (BELOWOF7-ROBOT)))

(:derived (Flag279-)(and (BELOWOF9-ROBOT)))

(:derived (Flag279-)(and (BELOWOF3-ROBOT)))

(:derived (Flag279-)(and (BELOWOF13-ROBOT)))

(:derived (Flag279-)(and (BELOWOF8-ROBOT)))

(:derived (Flag279-)(and (BELOWOF14-ROBOT)))

(:derived (Flag280-)(and (BELOWOF10-ROBOT)))

(:derived (Flag280-)(and (BELOWOF16-ROBOT)))

(:derived (Flag280-)(and (BELOWOF15-ROBOT)))

(:derived (Flag280-)(and (BELOWOF6-ROBOT)))

(:derived (Flag280-)(and (BELOWOF17-ROBOT)))

(:derived (Flag280-)(and (BELOWOF5-ROBOT)))

(:derived (Flag280-)(and (BELOWOF2-ROBOT)))

(:derived (Flag280-)(and (BELOWOF1-ROBOT)))

(:derived (Flag280-)(and (BELOWOF4-ROBOT)))

(:derived (Flag280-)(and (BELOWOF11-ROBOT)))

(:derived (Flag280-)(and (BELOWOF12-ROBOT)))

(:derived (Flag280-)(and (BELOWOF7-ROBOT)))

(:derived (Flag280-)(and (BELOWOF9-ROBOT)))

(:derived (Flag280-)(and (BELOWOF3-ROBOT)))

(:derived (Flag280-)(and (BELOWOF13-ROBOT)))

(:derived (Flag280-)(and (BELOWOF8-ROBOT)))

(:derived (Flag280-)(and (BELOWOF14-ROBOT)))

(:derived (Flag281-)(and (BELOWOF3-ROBOT)))

(:derived (Flag281-)(and (BELOWOF19-ROBOT)))

(:derived (Flag281-)(and (BELOWOF16-ROBOT)))

(:derived (Flag281-)(and (BELOWOF15-ROBOT)))

(:derived (Flag281-)(and (BELOWOF6-ROBOT)))

(:derived (Flag281-)(and (BELOWOF17-ROBOT)))

(:derived (Flag281-)(and (BELOWOF5-ROBOT)))

(:derived (Flag281-)(and (BELOWOF2-ROBOT)))

(:derived (Flag281-)(and (BELOWOF1-ROBOT)))

(:derived (Flag281-)(and (BELOWOF4-ROBOT)))

(:derived (Flag281-)(and (BELOWOF11-ROBOT)))

(:derived (Flag281-)(and (BELOWOF12-ROBOT)))

(:derived (Flag281-)(and (BELOWOF7-ROBOT)))

(:derived (Flag281-)(and (BELOWOF9-ROBOT)))

(:derived (Flag281-)(and (BELOWOF10-ROBOT)))

(:derived (Flag281-)(and (BELOWOF13-ROBOT)))

(:derived (Flag281-)(and (BELOWOF8-ROBOT)))

(:derived (Flag281-)(and (BELOWOF14-ROBOT)))

(:derived (Flag282-)(and (ABOVEOF6-ROBOT)))

(:derived (Flag282-)(and (ABOVEOF11-ROBOT)))

(:derived (Flag282-)(and (ABOVEOF10-ROBOT)))

(:derived (Flag282-)(and (ABOVEOF2-ROBOT)))

(:derived (Flag282-)(and (ABOVEOF7-ROBOT)))

(:derived (Flag282-)(and (ABOVEOF12-ROBOT)))

(:derived (Flag282-)(and (ABOVEOF17-ROBOT)))

(:derived (Flag282-)(and (ABOVEOF15-ROBOT)))

(:derived (Flag282-)(and (ABOVEOF8-ROBOT)))

(:derived (Flag282-)(and (ABOVEOF9-ROBOT)))

(:derived (Flag282-)(and (ABOVEOF16-ROBOT)))

(:derived (Flag282-)(and (ABOVEOF4-ROBOT)))

(:derived (Flag282-)(and (ABOVEOF3-ROBOT)))

(:derived (Flag282-)(and (ABOVEOF13-ROBOT)))

(:derived (Flag282-)(and (ABOVEOF14-ROBOT)))

(:derived (Flag282-)(and (ABOVEOF5-ROBOT)))

(:derived (Flag282-)(and (ABOVEOF1-ROBOT)))

(:derived (Flag282-)(and (ABOVEOF18-ROBOT)))

(:derived (Flag283-)(and (ABOVEOF11-ROBOT)))

(:derived (Flag283-)(and (ABOVEOF6-ROBOT)))

(:derived (Flag283-)(and (ABOVEOF10-ROBOT)))

(:derived (Flag283-)(and (ABOVEOF2-ROBOT)))

(:derived (Flag283-)(and (ABOVEOF7-ROBOT)))

(:derived (Flag283-)(and (ABOVEOF12-ROBOT)))

(:derived (Flag283-)(and (ABOVEOF17-ROBOT)))

(:derived (Flag283-)(and (ABOVEOF15-ROBOT)))

(:derived (Flag283-)(and (ABOVEOF8-ROBOT)))

(:derived (Flag283-)(and (ABOVEOF9-ROBOT)))

(:derived (Flag283-)(and (ABOVEOF16-ROBOT)))

(:derived (Flag283-)(and (ABOVEOF4-ROBOT)))

(:derived (Flag283-)(and (ABOVEOF3-ROBOT)))

(:derived (Flag283-)(and (ABOVEOF13-ROBOT)))

(:derived (Flag283-)(and (ABOVEOF14-ROBOT)))

(:derived (Flag283-)(and (ABOVEOF5-ROBOT)))

(:derived (Flag283-)(and (ABOVEOF18-ROBOT)))

(:derived (Flag284-)(and (ABOVEOF11-ROBOT)))

(:derived (Flag284-)(and (ABOVEOF6-ROBOT)))

(:derived (Flag284-)(and (ABOVEOF10-ROBOT)))

(:derived (Flag284-)(and (ABOVEOF8-ROBOT)))

(:derived (Flag284-)(and (ABOVEOF7-ROBOT)))

(:derived (Flag284-)(and (ABOVEOF12-ROBOT)))

(:derived (Flag284-)(and (ABOVEOF17-ROBOT)))

(:derived (Flag284-)(and (ABOVEOF15-ROBOT)))

(:derived (Flag284-)(and (ABOVEOF9-ROBOT)))

(:derived (Flag284-)(and (ABOVEOF16-ROBOT)))

(:derived (Flag284-)(and (ABOVEOF4-ROBOT)))

(:derived (Flag284-)(and (ABOVEOF3-ROBOT)))

(:derived (Flag284-)(and (ABOVEOF13-ROBOT)))

(:derived (Flag284-)(and (ABOVEOF14-ROBOT)))

(:derived (Flag284-)(and (ABOVEOF5-ROBOT)))

(:derived (Flag284-)(and (ABOVEOF18-ROBOT)))

(:derived (Flag285-)(and (ABOVEOF11-ROBOT)))

(:derived (Flag285-)(and (ABOVEOF6-ROBOT)))

(:derived (Flag285-)(and (ABOVEOF10-ROBOT)))

(:derived (Flag285-)(and (ABOVEOF8-ROBOT)))

(:derived (Flag285-)(and (ABOVEOF7-ROBOT)))

(:derived (Flag285-)(and (ABOVEOF12-ROBOT)))

(:derived (Flag285-)(and (ABOVEOF17-ROBOT)))

(:derived (Flag285-)(and (ABOVEOF15-ROBOT)))

(:derived (Flag285-)(and (ABOVEOF9-ROBOT)))

(:derived (Flag285-)(and (ABOVEOF16-ROBOT)))

(:derived (Flag285-)(and (ABOVEOF4-ROBOT)))

(:derived (Flag285-)(and (ABOVEOF13-ROBOT)))

(:derived (Flag285-)(and (ABOVEOF14-ROBOT)))

(:derived (Flag285-)(and (ABOVEOF5-ROBOT)))

(:derived (Flag285-)(and (ABOVEOF18-ROBOT)))

(:derived (Flag286-)(and (ABOVEOF11-ROBOT)))

(:derived (Flag286-)(and (ABOVEOF6-ROBOT)))

(:derived (Flag286-)(and (ABOVEOF7-ROBOT)))

(:derived (Flag286-)(and (ABOVEOF8-ROBOT)))

(:derived (Flag286-)(and (ABOVEOF10-ROBOT)))

(:derived (Flag286-)(and (ABOVEOF12-ROBOT)))

(:derived (Flag286-)(and (ABOVEOF17-ROBOT)))

(:derived (Flag286-)(and (ABOVEOF15-ROBOT)))

(:derived (Flag286-)(and (ABOVEOF9-ROBOT)))

(:derived (Flag286-)(and (ABOVEOF16-ROBOT)))

(:derived (Flag286-)(and (ABOVEOF13-ROBOT)))

(:derived (Flag286-)(and (ABOVEOF14-ROBOT)))

(:derived (Flag286-)(and (ABOVEOF5-ROBOT)))

(:derived (Flag286-)(and (ABOVEOF18-ROBOT)))

(:derived (Flag287-)(and (ABOVEOF11-ROBOT)))

(:derived (Flag287-)(and (ABOVEOF6-ROBOT)))

(:derived (Flag287-)(and (ABOVEOF7-ROBOT)))

(:derived (Flag287-)(and (ABOVEOF8-ROBOT)))

(:derived (Flag287-)(and (ABOVEOF10-ROBOT)))

(:derived (Flag287-)(and (ABOVEOF12-ROBOT)))

(:derived (Flag287-)(and (ABOVEOF17-ROBOT)))

(:derived (Flag287-)(and (ABOVEOF15-ROBOT)))

(:derived (Flag287-)(and (ABOVEOF9-ROBOT)))

(:derived (Flag287-)(and (ABOVEOF16-ROBOT)))

(:derived (Flag287-)(and (ABOVEOF13-ROBOT)))

(:derived (Flag287-)(and (ABOVEOF14-ROBOT)))

(:derived (Flag287-)(and (ABOVEOF18-ROBOT)))

(:derived (Flag288-)(and (ABOVEOF11-ROBOT)))

(:derived (Flag288-)(and (ABOVEOF7-ROBOT)))

(:derived (Flag288-)(and (ABOVEOF8-ROBOT)))

(:derived (Flag288-)(and (ABOVEOF10-ROBOT)))

(:derived (Flag288-)(and (ABOVEOF12-ROBOT)))

(:derived (Flag288-)(and (ABOVEOF17-ROBOT)))

(:derived (Flag288-)(and (ABOVEOF15-ROBOT)))

(:derived (Flag288-)(and (ABOVEOF9-ROBOT)))

(:derived (Flag288-)(and (ABOVEOF16-ROBOT)))

(:derived (Flag288-)(and (ABOVEOF13-ROBOT)))

(:derived (Flag288-)(and (ABOVEOF14-ROBOT)))

(:derived (Flag288-)(and (ABOVEOF18-ROBOT)))

(:derived (Flag289-)(and (ABOVEOF11-ROBOT)))

(:derived (Flag289-)(and (ABOVEOF8-ROBOT)))

(:derived (Flag289-)(and (ABOVEOF10-ROBOT)))

(:derived (Flag289-)(and (ABOVEOF12-ROBOT)))

(:derived (Flag289-)(and (ABOVEOF17-ROBOT)))

(:derived (Flag289-)(and (ABOVEOF15-ROBOT)))

(:derived (Flag289-)(and (ABOVEOF9-ROBOT)))

(:derived (Flag289-)(and (ABOVEOF16-ROBOT)))

(:derived (Flag289-)(and (ABOVEOF13-ROBOT)))

(:derived (Flag289-)(and (ABOVEOF14-ROBOT)))

(:derived (Flag289-)(and (ABOVEOF18-ROBOT)))

(:derived (Flag290-)(and (ABOVEOF11-ROBOT)))

(:derived (Flag290-)(and (ABOVEOF10-ROBOT)))

(:derived (Flag290-)(and (ABOVEOF12-ROBOT)))

(:derived (Flag290-)(and (ABOVEOF17-ROBOT)))

(:derived (Flag290-)(and (ABOVEOF15-ROBOT)))

(:derived (Flag290-)(and (ABOVEOF9-ROBOT)))

(:derived (Flag290-)(and (ABOVEOF16-ROBOT)))

(:derived (Flag290-)(and (ABOVEOF13-ROBOT)))

(:derived (Flag290-)(and (ABOVEOF14-ROBOT)))

(:derived (Flag290-)(and (ABOVEOF18-ROBOT)))

(:derived (Flag291-)(and (ABOVEOF11-ROBOT)))

(:derived (Flag291-)(and (ABOVEOF10-ROBOT)))

(:derived (Flag291-)(and (ABOVEOF12-ROBOT)))

(:derived (Flag291-)(and (ABOVEOF17-ROBOT)))

(:derived (Flag291-)(and (ABOVEOF15-ROBOT)))

(:derived (Flag291-)(and (ABOVEOF16-ROBOT)))

(:derived (Flag291-)(and (ABOVEOF13-ROBOT)))

(:derived (Flag291-)(and (ABOVEOF14-ROBOT)))

(:derived (Flag291-)(and (ABOVEOF18-ROBOT)))

(:derived (Flag292-)(and (ABOVEOF11-ROBOT)))

(:derived (Flag292-)(and (ABOVEOF12-ROBOT)))

(:derived (Flag292-)(and (ABOVEOF17-ROBOT)))

(:derived (Flag292-)(and (ABOVEOF15-ROBOT)))

(:derived (Flag292-)(and (ABOVEOF16-ROBOT)))

(:derived (Flag292-)(and (ABOVEOF13-ROBOT)))

(:derived (Flag292-)(and (ABOVEOF14-ROBOT)))

(:derived (Flag292-)(and (ABOVEOF18-ROBOT)))

(:derived (Flag293-)(and (ABOVEOF12-ROBOT)))

(:derived (Flag293-)(and (ABOVEOF17-ROBOT)))

(:derived (Flag293-)(and (ABOVEOF15-ROBOT)))

(:derived (Flag293-)(and (ABOVEOF16-ROBOT)))

(:derived (Flag293-)(and (ABOVEOF13-ROBOT)))

(:derived (Flag293-)(and (ABOVEOF14-ROBOT)))

(:derived (Flag293-)(and (ABOVEOF18-ROBOT)))

(:derived (Flag294-)(and (ABOVEOF17-ROBOT)))

(:derived (Flag294-)(and (ABOVEOF15-ROBOT)))

(:derived (Flag294-)(and (ABOVEOF16-ROBOT)))

(:derived (Flag294-)(and (ABOVEOF13-ROBOT)))

(:derived (Flag294-)(and (ABOVEOF14-ROBOT)))

(:derived (Flag294-)(and (ABOVEOF18-ROBOT)))

(:derived (Flag295-)(and (ABOVEOF16-ROBOT)))

(:derived (Flag295-)(and (ABOVEOF14-ROBOT)))

(:derived (Flag295-)(and (ABOVEOF17-ROBOT)))

(:derived (Flag295-)(and (ABOVEOF15-ROBOT)))

(:derived (Flag295-)(and (ABOVEOF18-ROBOT)))

(:derived (Flag296-)(and (ABOVEOF18-ROBOT)))

(:derived (Flag296-)(and (ABOVEOF17-ROBOT)))

(:derived (Flag296-)(and (ABOVEOF15-ROBOT)))

(:derived (Flag296-)(and (ABOVEOF16-ROBOT)))

(:derived (Flag297-)(and (ABOVEOF18-ROBOT)))

(:derived (Flag297-)(and (ABOVEOF17-ROBOT)))

(:derived (Flag297-)(and (ABOVEOF16-ROBOT)))

(:derived (Flag298-)(and (ABOVEOF17-ROBOT)))

(:derived (Flag298-)(and (ABOVEOF18-ROBOT)))

(:derived (Flag299-)(and (ABOVEOF17-ROBOT)(BELOWOF1-ROBOT)))

(:derived (Flag300-)(and (ABOVEOF18-ROBOT)(BELOWOF1-ROBOT)))

(:derived (Flag301-)(and (BELOWOF1-ROBOT)(ABOVEOF1-ROBOT)))

(:derived (Flag302-)(and (ABOVEOF18-ROBOT)(BELOWOF2-ROBOT)))

(:derived (Flag303-)(and (ABOVEOF11-ROBOT)(BELOWOF1-ROBOT)))

(:derived (Flag304-)(and (ABOVEOF1-ROBOT)(BELOWOF2-ROBOT)))

(:derived (Flag305-)(and (ABOVEOF7-ROBOT)(BELOWOF2-ROBOT)))

(:derived (Flag306-)(and (ABOVEOF15-ROBOT)(BELOWOF2-ROBOT)))

(:derived (Flag307-)(and (BELOWOF2-ROBOT)(ABOVEOF13-ROBOT)))

(:derived (Flag308-)(and (BELOWOF1-ROBOT)(ABOVEOF16-ROBOT)))

(:derived (Flag309-)(and (BELOWOF1-ROBOT)(ABOVEOF7-ROBOT)))

(:derived (Flag310-)(and (ABOVEOF2-ROBOT)(BELOWOF2-ROBOT)))

(:derived (Flag311-)(and (BELOWOF1-ROBOT)(ABOVEOF5-ROBOT)))

(:derived (Flag312-)(and (ABOVEOF16-ROBOT)(BELOWOF2-ROBOT)))

(:derived (Flag313-)(and (ABOVEOF4-ROBOT)(BELOWOF2-ROBOT)))

(:derived (Flag314-)(and (BELOWOF1-ROBOT)(ABOVEOF17-ROBOT)))

(:derived (Flag315-)(and (BELOWOF2-ROBOT)(ABOVEOF14-ROBOT)))

(:derived (Flag316-)(and (ABOVEOF6-ROBOT)(BELOWOF2-ROBOT)))

(:derived (Flag317-)(and (BELOWOF1-ROBOT)(ABOVEOF2-ROBOT)))

(:derived (Flag318-)(and (ABOVEOF9-ROBOT)(BELOWOF2-ROBOT)))

(:derived (Flag319-)(and (BELOWOF1-ROBOT)(ABOVEOF13-ROBOT)))

(:derived (Flag320-)(and (ABOVEOF6-ROBOT)(BELOWOF1-ROBOT)))

(:derived (Flag321-)(and (BELOWOF1-ROBOT)(ABOVEOF14-ROBOT)))

(:derived (Flag322-)(and (ABOVEOF17-ROBOT)(BELOWOF2-ROBOT)))

(:derived (Flag323-)(and (ABOVEOF10-ROBOT)(BELOWOF1-ROBOT)))

(:derived (Flag324-)(and (ABOVEOF11-ROBOT)(BELOWOF2-ROBOT)))

(:derived (Flag325-)(and (ABOVEOF3-ROBOT)(BELOWOF1-ROBOT)))

(:derived (Flag326-)(and (BELOWOF1-ROBOT)(ABOVEOF9-ROBOT)))

(:derived (Flag327-)(and (ABOVEOF10-ROBOT)(BELOWOF2-ROBOT)))

(:derived (Flag328-)(and (ABOVEOF8-ROBOT)(BELOWOF2-ROBOT)))

(:derived (Flag329-)(and (BELOWOF1-ROBOT)(ABOVEOF4-ROBOT)))

(:derived (Flag330-)(and (ABOVEOF12-ROBOT)(BELOWOF2-ROBOT)))

(:derived (Flag331-)(and (ABOVEOF12-ROBOT)(BELOWOF1-ROBOT)))

(:derived (Flag332-)(and (BELOWOF2-ROBOT)(ABOVEOF5-ROBOT)))

(:derived (Flag333-)(and (BELOWOF1-ROBOT)(ABOVEOF15-ROBOT)))

(:derived (Flag334-)(and (ABOVEOF8-ROBOT)(BELOWOF1-ROBOT)))

(:derived (Flag335-)(and (ABOVEOF3-ROBOT)(BELOWOF2-ROBOT)))

(:derived (Flag337-)(and (BELOWOF3-ROBOT)(ABOVEOF5-ROBOT)))

(:derived (Flag338-)(and (BELOWOF3-ROBOT)(ABOVEOF6-ROBOT)))

(:derived (Flag339-)(and (BELOWOF3-ROBOT)(ABOVEOF7-ROBOT)))

(:derived (Flag340-)(and (BELOWOF3-ROBOT)(ABOVEOF17-ROBOT)))

(:derived (Flag341-)(and (BELOWOF3-ROBOT)(ABOVEOF14-ROBOT)))

(:derived (Flag342-)(and (BELOWOF3-ROBOT)(ABOVEOF16-ROBOT)))

(:derived (Flag343-)(and (BELOWOF3-ROBOT)(ABOVEOF9-ROBOT)))

(:derived (Flag344-)(and (BELOWOF3-ROBOT)(ABOVEOF15-ROBOT)))

(:derived (Flag345-)(and (BELOWOF3-ROBOT)(ABOVEOF2-ROBOT)))

(:derived (Flag346-)(and (ABOVEOF3-ROBOT)(BELOWOF3-ROBOT)))

(:derived (Flag347-)(and (ABOVEOF8-ROBOT)(BELOWOF3-ROBOT)))

(:derived (Flag348-)(and (BELOWOF3-ROBOT)(ABOVEOF4-ROBOT)))

(:derived (Flag349-)(and (BELOWOF3-ROBOT)(ABOVEOF10-ROBOT)))

(:derived (Flag350-)(and (BELOWOF2-ROBOT)(ABOVEOF2-ROBOT)))

(:derived (Flag351-)(and (ABOVEOF12-ROBOT)(BELOWOF3-ROBOT)))

(:derived (Flag352-)(and (BELOWOF3-ROBOT)(ABOVEOF13-ROBOT)))

(:derived (Flag353-)(and (ABOVEOF11-ROBOT)(BELOWOF3-ROBOT)))

(:derived (Flag354-)(and (BELOWOF3-ROBOT)(ABOVEOF18-ROBOT)))

(:derived (Flag356-)(and (ABOVEOF9-ROBOT)(BELOWOF4-ROBOT)))

(:derived (Flag357-)(and (BELOWOF4-ROBOT)(ABOVEOF13-ROBOT)))

(:derived (Flag358-)(and (ABOVEOF3-ROBOT)(BELOWOF4-ROBOT)))

(:derived (Flag359-)(and (ABOVEOF17-ROBOT)(BELOWOF4-ROBOT)))

(:derived (Flag360-)(and (ABOVEOF6-ROBOT)(BELOWOF4-ROBOT)))

(:derived (Flag361-)(and (BELOWOF4-ROBOT)(ABOVEOF4-ROBOT)))

(:derived (Flag362-)(and (ABOVEOF18-ROBOT)(BELOWOF4-ROBOT)))

(:derived (Flag363-)(and (BELOWOF4-ROBOT)(ABOVEOF16-ROBOT)))

(:derived (Flag364-)(and (ABOVEOF8-ROBOT)(BELOWOF4-ROBOT)))

(:derived (Flag365-)(and (ABOVEOF12-ROBOT)(BELOWOF4-ROBOT)))

(:derived (Flag366-)(and (BELOWOF4-ROBOT)(ABOVEOF5-ROBOT)))

(:derived (Flag367-)(and (BELOWOF3-ROBOT)(ABOVEOF3-ROBOT)))

(:derived (Flag368-)(and (ABOVEOF10-ROBOT)(BELOWOF4-ROBOT)))

(:derived (Flag369-)(and (BELOWOF4-ROBOT)(ABOVEOF15-ROBOT)))

(:derived (Flag370-)(and (BELOWOF3-ROBOT)(ABOVEOF11-ROBOT)))

(:derived (Flag371-)(and (BELOWOF4-ROBOT)(ABOVEOF7-ROBOT)))

(:derived (Flag372-)(and (ABOVEOF11-ROBOT)(BELOWOF4-ROBOT)))

(:derived (Flag373-)(and (BELOWOF4-ROBOT)(ABOVEOF14-ROBOT)))

(:derived (Flag375-)(and (ABOVEOF17-ROBOT)(BELOWOF5-ROBOT)))

(:derived (Flag376-)(and (ABOVEOF4-ROBOT)(BELOWOF5-ROBOT)))

(:derived (Flag377-)(and (ABOVEOF15-ROBOT)(BELOWOF5-ROBOT)))

(:derived (Flag378-)(and (ABOVEOF8-ROBOT)(BELOWOF5-ROBOT)))

(:derived (Flag379-)(and (ABOVEOF16-ROBOT)(BELOWOF5-ROBOT)))

(:derived (Flag380-)(and (ABOVEOF10-ROBOT)(BELOWOF5-ROBOT)))

(:derived (Flag381-)(and (ABOVEOF13-ROBOT)(BELOWOF5-ROBOT)))

(:derived (Flag382-)(and (ABOVEOF16-ROBOT)(BELOWOF4-ROBOT)))

(:derived (Flag383-)(and (ABOVEOF5-ROBOT)(BELOWOF5-ROBOT)))

(:derived (Flag384-)(and (ABOVEOF7-ROBOT)(BELOWOF5-ROBOT)))

(:derived (Flag385-)(and (ABOVEOF9-ROBOT)(BELOWOF5-ROBOT)))

(:derived (Flag386-)(and (ABOVEOF18-ROBOT)(BELOWOF5-ROBOT)))

(:derived (Flag387-)(and (ABOVEOF12-ROBOT)(BELOWOF5-ROBOT)))

(:derived (Flag388-)(and (ABOVEOF11-ROBOT)(BELOWOF5-ROBOT)))

(:derived (Flag389-)(and (ABOVEOF6-ROBOT)(BELOWOF5-ROBOT)))

(:derived (Flag390-)(and (ABOVEOF14-ROBOT)(BELOWOF5-ROBOT)))

(:derived (Flag392-)(and (BELOWOF5-ROBOT)(ABOVEOF5-ROBOT)))

(:derived (Flag393-)(and (BELOWOF6-ROBOT)(ABOVEOF18-ROBOT)))

(:derived (Flag394-)(and (BELOWOF6-ROBOT)(ABOVEOF6-ROBOT)))

(:derived (Flag395-)(and (ABOVEOF11-ROBOT)(BELOWOF6-ROBOT)))

(:derived (Flag396-)(and (ABOVEOF12-ROBOT)(BELOWOF6-ROBOT)))

(:derived (Flag397-)(and (BELOWOF5-ROBOT)(ABOVEOF13-ROBOT)))

(:derived (Flag398-)(and (BELOWOF6-ROBOT)(ABOVEOF17-ROBOT)))

(:derived (Flag399-)(and (BELOWOF6-ROBOT)(ABOVEOF10-ROBOT)))

(:derived (Flag400-)(and (BELOWOF6-ROBOT)(ABOVEOF15-ROBOT)))

(:derived (Flag401-)(and (BELOWOF6-ROBOT)(ABOVEOF14-ROBOT)))

(:derived (Flag402-)(and (BELOWOF6-ROBOT)(ABOVEOF5-ROBOT)))

(:derived (Flag403-)(and (ABOVEOF8-ROBOT)(BELOWOF6-ROBOT)))

(:derived (Flag404-)(and (BELOWOF6-ROBOT)(ABOVEOF9-ROBOT)))

(:derived (Flag405-)(and (BELOWOF6-ROBOT)(ABOVEOF16-ROBOT)))

(:derived (Flag406-)(and (BELOWOF6-ROBOT)(ABOVEOF13-ROBOT)))

(:derived (Flag407-)(and (BELOWOF6-ROBOT)(ABOVEOF7-ROBOT)))

(:derived (Flag409-)(and (ABOVEOF6-ROBOT)(BELOWOF7-ROBOT)))

(:derived (Flag410-)(and (BELOWOF7-ROBOT)(ABOVEOF14-ROBOT)))

(:derived (Flag411-)(and (BELOWOF7-ROBOT)(ABOVEOF15-ROBOT)))

(:derived (Flag412-)(and (ABOVEOF7-ROBOT)(BELOWOF7-ROBOT)))

(:derived (Flag413-)(and (ABOVEOF16-ROBOT)(BELOWOF7-ROBOT)))

(:derived (Flag414-)(and (ABOVEOF18-ROBOT)(BELOWOF7-ROBOT)))

(:derived (Flag415-)(and (ABOVEOF10-ROBOT)(BELOWOF6-ROBOT)))

(:derived (Flag416-)(and (ABOVEOF18-ROBOT)(BELOWOF6-ROBOT)))

(:derived (Flag417-)(and (ABOVEOF10-ROBOT)(BELOWOF7-ROBOT)))

(:derived (Flag418-)(and (BELOWOF7-ROBOT)(ABOVEOF13-ROBOT)))

(:derived (Flag419-)(and (ABOVEOF17-ROBOT)(BELOWOF7-ROBOT)))

(:derived (Flag420-)(and (ABOVEOF8-ROBOT)(BELOWOF7-ROBOT)))

(:derived (Flag421-)(and (ABOVEOF11-ROBOT)(BELOWOF7-ROBOT)))

(:derived (Flag422-)(and (ABOVEOF9-ROBOT)(BELOWOF7-ROBOT)))

(:derived (Flag423-)(and (ABOVEOF6-ROBOT)(BELOWOF6-ROBOT)))

(:derived (Flag424-)(and (ABOVEOF12-ROBOT)(BELOWOF7-ROBOT)))

(:derived (Flag426-)(and (BELOWOF8-ROBOT)(ABOVEOF11-ROBOT)))

(:derived (Flag427-)(and (BELOWOF8-ROBOT)(ABOVEOF14-ROBOT)))

(:derived (Flag428-)(and (BELOWOF8-ROBOT)(ABOVEOF17-ROBOT)))

(:derived (Flag429-)(and (BELOWOF8-ROBOT)(ABOVEOF13-ROBOT)))

(:derived (Flag430-)(and (BELOWOF8-ROBOT)(ABOVEOF10-ROBOT)))

(:derived (Flag431-)(and (BELOWOF8-ROBOT)(ABOVEOF18-ROBOT)))

(:derived (Flag432-)(and (BELOWOF8-ROBOT)(ABOVEOF9-ROBOT)))

(:derived (Flag433-)(and (BELOWOF8-ROBOT)(ABOVEOF7-ROBOT)))

(:derived (Flag434-)(and (ABOVEOF8-ROBOT)(BELOWOF8-ROBOT)))

(:derived (Flag435-)(and (BELOWOF7-ROBOT)(ABOVEOF7-ROBOT)))

(:derived (Flag436-)(and (BELOWOF8-ROBOT)(ABOVEOF15-ROBOT)))

(:derived (Flag437-)(and (ABOVEOF12-ROBOT)(BELOWOF8-ROBOT)))

(:derived (Flag438-)(and (BELOWOF8-ROBOT)(ABOVEOF16-ROBOT)))

(:derived (Flag440-)(and (ABOVEOF17-ROBOT)(BELOWOF9-ROBOT)))

(:derived (Flag441-)(and (BELOWOF9-ROBOT)(ABOVEOF13-ROBOT)))

(:derived (Flag442-)(and (ABOVEOF11-ROBOT)(BELOWOF9-ROBOT)))

(:derived (Flag443-)(and (BELOWOF8-ROBOT)(ABOVEOF12-ROBOT)))

(:derived (Flag444-)(and (ABOVEOF10-ROBOT)(BELOWOF9-ROBOT)))

(:derived (Flag445-)(and (BELOWOF9-ROBOT)(ABOVEOF14-ROBOT)))

(:derived (Flag446-)(and (BELOWOF9-ROBOT)(ABOVEOF9-ROBOT)))

(:derived (Flag447-)(and (BELOWOF9-ROBOT)(ABOVEOF16-ROBOT)))

(:derived (Flag448-)(and (BELOWOF8-ROBOT)(ABOVEOF8-ROBOT)))

(:derived (Flag449-)(and (BELOWOF9-ROBOT)(ABOVEOF15-ROBOT)))

(:derived (Flag450-)(and (ABOVEOF18-ROBOT)(BELOWOF9-ROBOT)))

(:derived (Flag451-)(and (ABOVEOF8-ROBOT)(BELOWOF9-ROBOT)))

(:derived (Flag452-)(and (ABOVEOF12-ROBOT)(BELOWOF9-ROBOT)))

(:derived (Flag454-)(and (BELOWOF10-ROBOT)(ABOVEOF10-ROBOT)))

(:derived (Flag455-)(and (BELOWOF10-ROBOT)(ABOVEOF18-ROBOT)))

(:derived (Flag456-)(and (ABOVEOF12-ROBOT)(BELOWOF10-ROBOT)))

(:derived (Flag457-)(and (BELOWOF10-ROBOT)(ABOVEOF14-ROBOT)))

(:derived (Flag458-)(and (ABOVEOF9-ROBOT)(BELOWOF9-ROBOT)))

(:derived (Flag459-)(and (BELOWOF10-ROBOT)(ABOVEOF13-ROBOT)))

(:derived (Flag460-)(and (BELOWOF10-ROBOT)(ABOVEOF16-ROBOT)))

(:derived (Flag461-)(and (BELOWOF10-ROBOT)(ABOVEOF9-ROBOT)))

(:derived (Flag462-)(and (BELOWOF10-ROBOT)(ABOVEOF15-ROBOT)))

(:derived (Flag463-)(and (BELOWOF10-ROBOT)(ABOVEOF17-ROBOT)))

(:derived (Flag464-)(and (ABOVEOF11-ROBOT)(BELOWOF10-ROBOT)))

(:derived (Flag466-)(and (BELOWOF11-ROBOT)(ABOVEOF16-ROBOT)))

(:derived (Flag467-)(and (BELOWOF11-ROBOT)(ABOVEOF18-ROBOT)))

(:derived (Flag468-)(and (BELOWOF11-ROBOT)(ABOVEOF15-ROBOT)))

(:derived (Flag469-)(and (BELOWOF11-ROBOT)(ABOVEOF17-ROBOT)))

(:derived (Flag470-)(and (BELOWOF11-ROBOT)(ABOVEOF13-ROBOT)))

(:derived (Flag471-)(and (BELOWOF11-ROBOT)(ABOVEOF14-ROBOT)))

(:derived (Flag472-)(and (BELOWOF11-ROBOT)(ABOVEOF10-ROBOT)))

(:derived (Flag473-)(and (ABOVEOF11-ROBOT)(BELOWOF11-ROBOT)))

(:derived (Flag474-)(and (ABOVEOF12-ROBOT)(BELOWOF11-ROBOT)))

(:derived (Flag476-)(and (BELOWOF12-ROBOT)(ABOVEOF18-ROBOT)))

(:derived (Flag477-)(and (BELOWOF12-ROBOT)(ABOVEOF13-ROBOT)))

(:derived (Flag478-)(and (BELOWOF12-ROBOT)(ABOVEOF15-ROBOT)))

(:derived (Flag479-)(and (BELOWOF12-ROBOT)(ABOVEOF17-ROBOT)))

(:derived (Flag480-)(and (BELOWOF12-ROBOT)(ABOVEOF11-ROBOT)))

(:derived (Flag481-)(and (BELOWOF12-ROBOT)(ABOVEOF14-ROBOT)))

(:derived (Flag482-)(and (ABOVEOF12-ROBOT)(BELOWOF12-ROBOT)))

(:derived (Flag483-)(and (BELOWOF12-ROBOT)(ABOVEOF16-ROBOT)))

(:derived (Flag485-)(and (ABOVEOF18-ROBOT)(BELOWOF13-ROBOT)))

(:derived (Flag486-)(and (ABOVEOF16-ROBOT)(BELOWOF13-ROBOT)))

(:derived (Flag487-)(and (ABOVEOF17-ROBOT)(BELOWOF13-ROBOT)))

(:derived (Flag488-)(and (ABOVEOF13-ROBOT)(BELOWOF13-ROBOT)))

(:derived (Flag489-)(and (ABOVEOF14-ROBOT)(BELOWOF13-ROBOT)))

(:derived (Flag490-)(and (ABOVEOF15-ROBOT)(BELOWOF13-ROBOT)))

(:derived (Flag491-)(and (ABOVEOF12-ROBOT)(BELOWOF13-ROBOT)))

(:derived (Flag492-)(and (BELOWOF12-ROBOT)(ABOVEOF12-ROBOT)))

(:derived (Flag494-)(and (ABOVEOF15-ROBOT)(BELOWOF14-ROBOT)))

(:derived (Flag495-)(and (ABOVEOF16-ROBOT)(BELOWOF14-ROBOT)))

(:derived (Flag496-)(and (BELOWOF14-ROBOT)(ABOVEOF14-ROBOT)))

(:derived (Flag497-)(and (BELOWOF13-ROBOT)(ABOVEOF13-ROBOT)))

(:derived (Flag498-)(and (BELOWOF14-ROBOT)(ABOVEOF13-ROBOT)))

(:derived (Flag499-)(and (ABOVEOF18-ROBOT)(BELOWOF14-ROBOT)))

(:derived (Flag500-)(and (ABOVEOF17-ROBOT)(BELOWOF14-ROBOT)))

(:derived (Flag502-)(and (BELOWOF15-ROBOT)(ABOVEOF14-ROBOT)))

(:derived (Flag503-)(and (BELOWOF15-ROBOT)(ABOVEOF15-ROBOT)))

(:derived (Flag504-)(and (ABOVEOF17-ROBOT)(BELOWOF15-ROBOT)))

(:derived (Flag505-)(and (ABOVEOF16-ROBOT)(BELOWOF15-ROBOT)))

(:derived (Flag506-)(and (ABOVEOF18-ROBOT)(BELOWOF15-ROBOT)))

(:derived (Flag508-)(and (ABOVEOF17-ROBOT)(BELOWOF16-ROBOT)))

(:derived (Flag509-)(and (ABOVEOF16-ROBOT)(BELOWOF16-ROBOT)))

(:derived (Flag510-)(and (BELOWOF16-ROBOT)(ABOVEOF15-ROBOT)))

(:derived (Flag511-)(and (ABOVEOF18-ROBOT)(BELOWOF16-ROBOT)))

(:derived (Flag513-)(and (ABOVEOF18-ROBOT)(BELOWOF17-ROBOT)))

(:derived (Flag514-)(and (BELOWOF17-ROBOT)(ABOVEOF16-ROBOT)))

(:derived (Flag515-)(and (ABOVEOF17-ROBOT)(BELOWOF17-ROBOT)))

(:derived (Flag517-)(and (BELOWOF18-ROBOT)(ABOVEOF17-ROBOT)))

(:derived (Flag518-)(and (BELOWOF18-ROBOT)(ABOVEOF18-ROBOT)))

(:derived (Flag520-)(and (BELOWOF19-ROBOT)(ABOVEOF18-ROBOT)))

(:derived (Flag522-)(and (BELOWOF1-ROBOT)))

(:action MOVEUP-ROBOT
:parameters ()
:precondition
(and
(Flag2-)
)
:effect
(and
(when
(and
(Flag519-)
)
(and
(ROW18-ROBOT)
(not (NOT-ROW18-ROBOT))
)
)
(when
(and
(Flag516-)
)
(and
(ROW17-ROBOT)
(not (NOT-ROW17-ROBOT))
)
)
(when
(and
(Flag512-)
)
(and
(ROW16-ROBOT)
(not (NOT-ROW16-ROBOT))
)
)
(when
(and
(Flag507-)
)
(and
(ROW15-ROBOT)
(not (NOT-ROW15-ROBOT))
)
)
(when
(and
(Flag501-)
)
(and
(ROW14-ROBOT)
(not (NOT-ROW14-ROBOT))
)
)
(when
(and
(Flag493-)
)
(and
(ROW13-ROBOT)
(not (NOT-ROW13-ROBOT))
)
)
(when
(and
(Flag484-)
)
(and
(ROW12-ROBOT)
(not (NOT-ROW12-ROBOT))
)
)
(when
(and
(Flag475-)
)
(and
(ROW11-ROBOT)
(not (NOT-ROW11-ROBOT))
)
)
(when
(and
(Flag465-)
)
(and
(ROW10-ROBOT)
(not (NOT-ROW10-ROBOT))
)
)
(when
(and
(Flag453-)
)
(and
(ROW9-ROBOT)
(not (NOT-ROW9-ROBOT))
)
)
(when
(and
(Flag439-)
)
(and
(ROW8-ROBOT)
(not (NOT-ROW8-ROBOT))
)
)
(when
(and
(Flag425-)
)
(and
(ROW7-ROBOT)
(not (NOT-ROW7-ROBOT))
)
)
(when
(and
(Flag408-)
)
(and
(ROW6-ROBOT)
(not (NOT-ROW6-ROBOT))
)
)
(when
(and
(Flag391-)
)
(and
(ROW5-ROBOT)
(not (NOT-ROW5-ROBOT))
)
)
(when
(and
(Flag374-)
)
(and
(ROW4-ROBOT)
(not (NOT-ROW4-ROBOT))
)
)
(when
(and
(Flag355-)
)
(and
(ROW3-ROBOT)
(not (NOT-ROW3-ROBOT))
)
)
(when
(and
(Flag336-)
)
(and
(ROW2-ROBOT)
(not (NOT-ROW2-ROBOT))
)
)
(when
(and
(Flag522-)
)
(and
(ROW1-ROBOT)
(not (NOT-ROW1-ROBOT))
)
)
(when
(and
(ROW17-ROBOT)
)
(and
(ROW18-ROBOT)
(NOT-ROW17-ROBOT)
(not (NOT-ROW18-ROBOT))
(not (ROW17-ROBOT))
)
)
(when
(and
(ROW16-ROBOT)
)
(and
(ROW17-ROBOT)
(NOT-ROW16-ROBOT)
(not (NOT-ROW17-ROBOT))
(not (ROW16-ROBOT))
)
)
(when
(and
(ROW15-ROBOT)
)
(and
(ROW16-ROBOT)
(NOT-ROW15-ROBOT)
(not (NOT-ROW16-ROBOT))
(not (ROW15-ROBOT))
)
)
(when
(and
(ROW14-ROBOT)
)
(and
(ROW15-ROBOT)
(NOT-ROW14-ROBOT)
(not (NOT-ROW15-ROBOT))
(not (ROW14-ROBOT))
)
)
(when
(and
(ROW13-ROBOT)
)
(and
(ROW14-ROBOT)
(NOT-ROW13-ROBOT)
(not (NOT-ROW14-ROBOT))
(not (ROW13-ROBOT))
)
)
(when
(and
(ROW12-ROBOT)
)
(and
(ROW13-ROBOT)
(NOT-ROW12-ROBOT)
(not (NOT-ROW13-ROBOT))
(not (ROW12-ROBOT))
)
)
(when
(and
(ROW11-ROBOT)
)
(and
(ROW12-ROBOT)
(NOT-ROW11-ROBOT)
(not (NOT-ROW12-ROBOT))
(not (ROW11-ROBOT))
)
)
(when
(and
(ROW10-ROBOT)
)
(and
(ROW11-ROBOT)
(NOT-ROW10-ROBOT)
(not (NOT-ROW11-ROBOT))
(not (ROW10-ROBOT))
)
)
(when
(and
(ROW9-ROBOT)
)
(and
(ROW10-ROBOT)
(NOT-ROW9-ROBOT)
(not (NOT-ROW10-ROBOT))
(not (ROW9-ROBOT))
)
)
(when
(and
(ROW8-ROBOT)
)
(and
(ROW9-ROBOT)
(NOT-ROW8-ROBOT)
(not (NOT-ROW9-ROBOT))
(not (ROW8-ROBOT))
)
)
(when
(and
(ROW7-ROBOT)
)
(and
(ROW8-ROBOT)
(NOT-ROW7-ROBOT)
(not (NOT-ROW8-ROBOT))
(not (ROW7-ROBOT))
)
)
(when
(and
(ROW6-ROBOT)
)
(and
(ROW7-ROBOT)
(NOT-ROW6-ROBOT)
(not (NOT-ROW7-ROBOT))
(not (ROW6-ROBOT))
)
)
(when
(and
(ROW5-ROBOT)
)
(and
(ROW6-ROBOT)
(NOT-ROW5-ROBOT)
(not (NOT-ROW6-ROBOT))
(not (ROW5-ROBOT))
)
)
(when
(and
(ROW4-ROBOT)
)
(and
(ROW5-ROBOT)
(NOT-ROW4-ROBOT)
(not (NOT-ROW5-ROBOT))
(not (ROW4-ROBOT))
)
)
(when
(and
(ROW3-ROBOT)
)
(and
(ROW4-ROBOT)
(NOT-ROW3-ROBOT)
(not (NOT-ROW4-ROBOT))
(not (ROW3-ROBOT))
)
)
(when
(and
(ROW2-ROBOT)
)
(and
(ROW3-ROBOT)
(NOT-ROW2-ROBOT)
(not (NOT-ROW3-ROBOT))
(not (ROW2-ROBOT))
)
)
(when
(and
(ROW1-ROBOT)
)
(and
(ROW2-ROBOT)
(NOT-ROW1-ROBOT)
(not (NOT-ROW2-ROBOT))
(not (ROW1-ROBOT))
)
)
(when
(and
(ROW0-ROBOT)
)
(and
(ROW1-ROBOT)
(NOT-ROW0-ROBOT)
(not (NOT-ROW1-ROBOT))
(not (ROW0-ROBOT))
)
)
(when
(and
(BELOWOF14-ROBOT)
)
(and
(BELOWOF19-ROBOT)
(NOT-BELOWOF18-ROBOT)
(BELOWOF18-ROBOT)
(NOT-BELOWOF17-ROBOT)
(BELOWOF17-ROBOT)
(NOT-BELOWOF16-ROBOT)
(BELOWOF16-ROBOT)
(NOT-BELOWOF15-ROBOT)
(BELOWOF15-ROBOT)
(NOT-BELOWOF14-ROBOT)
(not (NOT-BELOWOF19-ROBOT))
(not (BELOWOF14-ROBOT))
)
)
(when
(and
(BELOWOF8-ROBOT)
)
(and
(BELOWOF19-ROBOT)
(NOT-BELOWOF18-ROBOT)
(BELOWOF18-ROBOT)
(NOT-BELOWOF17-ROBOT)
(BELOWOF17-ROBOT)
(NOT-BELOWOF16-ROBOT)
(BELOWOF16-ROBOT)
(NOT-BELOWOF15-ROBOT)
(BELOWOF15-ROBOT)
(NOT-BELOWOF14-ROBOT)
(BELOWOF14-ROBOT)
(NOT-BELOWOF13-ROBOT)
(BELOWOF13-ROBOT)
(NOT-BELOWOF12-ROBOT)
(BELOWOF12-ROBOT)
(NOT-BELOWOF11-ROBOT)
(BELOWOF11-ROBOT)
(NOT-BELOWOF10-ROBOT)
(BELOWOF10-ROBOT)
(NOT-BELOWOF9-ROBOT)
(BELOWOF9-ROBOT)
(NOT-BELOWOF8-ROBOT)
(not (NOT-BELOWOF19-ROBOT))
(not (BELOWOF8-ROBOT))
)
)
(when
(and
(BELOWOF13-ROBOT)
)
(and
(BELOWOF19-ROBOT)
(NOT-BELOWOF18-ROBOT)
(BELOWOF18-ROBOT)
(NOT-BELOWOF17-ROBOT)
(BELOWOF17-ROBOT)
(NOT-BELOWOF16-ROBOT)
(BELOWOF16-ROBOT)
(NOT-BELOWOF15-ROBOT)
(BELOWOF15-ROBOT)
(NOT-BELOWOF14-ROBOT)
(BELOWOF14-ROBOT)
(NOT-BELOWOF13-ROBOT)
(not (NOT-BELOWOF19-ROBOT))
(not (BELOWOF13-ROBOT))
)
)
(when
(and
(BELOWOF3-ROBOT)
)
(and
(BELOWOF19-ROBOT)
(NOT-BELOWOF18-ROBOT)
(BELOWOF18-ROBOT)
(NOT-BELOWOF17-ROBOT)
(BELOWOF17-ROBOT)
(NOT-BELOWOF16-ROBOT)
(BELOWOF16-ROBOT)
(NOT-BELOWOF15-ROBOT)
(BELOWOF15-ROBOT)
(NOT-BELOWOF14-ROBOT)
(BELOWOF14-ROBOT)
(NOT-BELOWOF13-ROBOT)
(BELOWOF13-ROBOT)
(NOT-BELOWOF12-ROBOT)
(BELOWOF12-ROBOT)
(NOT-BELOWOF11-ROBOT)
(BELOWOF11-ROBOT)
(NOT-BELOWOF10-ROBOT)
(BELOWOF10-ROBOT)
(NOT-BELOWOF9-ROBOT)
(BELOWOF9-ROBOT)
(NOT-BELOWOF8-ROBOT)
(BELOWOF8-ROBOT)
(NOT-BELOWOF7-ROBOT)
(BELOWOF7-ROBOT)
(NOT-BELOWOF6-ROBOT)
(BELOWOF6-ROBOT)
(NOT-BELOWOF5-ROBOT)
(BELOWOF5-ROBOT)
(NOT-BELOWOF4-ROBOT)
(BELOWOF4-ROBOT)
(NOT-BELOWOF3-ROBOT)
(not (NOT-BELOWOF19-ROBOT))
(not (BELOWOF3-ROBOT))
)
)
(when
(and
(BELOWOF9-ROBOT)
)
(and
(BELOWOF19-ROBOT)
(NOT-BELOWOF18-ROBOT)
(BELOWOF18-ROBOT)
(NOT-BELOWOF17-ROBOT)
(BELOWOF17-ROBOT)
(NOT-BELOWOF16-ROBOT)
(BELOWOF16-ROBOT)
(NOT-BELOWOF15-ROBOT)
(BELOWOF15-ROBOT)
(NOT-BELOWOF14-ROBOT)
(BELOWOF14-ROBOT)
(NOT-BELOWOF13-ROBOT)
(BELOWOF13-ROBOT)
(NOT-BELOWOF12-ROBOT)
(BELOWOF12-ROBOT)
(NOT-BELOWOF11-ROBOT)
(BELOWOF11-ROBOT)
(NOT-BELOWOF10-ROBOT)
(BELOWOF10-ROBOT)
(NOT-BELOWOF9-ROBOT)
(not (NOT-BELOWOF19-ROBOT))
(not (BELOWOF9-ROBOT))
)
)
(when
(and
(BELOWOF7-ROBOT)
)
(and
(BELOWOF19-ROBOT)
(NOT-BELOWOF18-ROBOT)
(BELOWOF18-ROBOT)
(NOT-BELOWOF17-ROBOT)
(BELOWOF17-ROBOT)
(NOT-BELOWOF16-ROBOT)
(BELOWOF16-ROBOT)
(NOT-BELOWOF15-ROBOT)
(BELOWOF15-ROBOT)
(NOT-BELOWOF14-ROBOT)
(BELOWOF14-ROBOT)
(NOT-BELOWOF13-ROBOT)
(BELOWOF13-ROBOT)
(NOT-BELOWOF12-ROBOT)
(BELOWOF12-ROBOT)
(NOT-BELOWOF11-ROBOT)
(BELOWOF11-ROBOT)
(NOT-BELOWOF10-ROBOT)
(BELOWOF10-ROBOT)
(NOT-BELOWOF9-ROBOT)
(BELOWOF9-ROBOT)
(NOT-BELOWOF8-ROBOT)
(BELOWOF8-ROBOT)
(NOT-BELOWOF7-ROBOT)
(not (NOT-BELOWOF19-ROBOT))
(not (BELOWOF7-ROBOT))
)
)
(when
(and
(BELOWOF12-ROBOT)
)
(and
(BELOWOF19-ROBOT)
(NOT-BELOWOF18-ROBOT)
(BELOWOF18-ROBOT)
(NOT-BELOWOF17-ROBOT)
(BELOWOF17-ROBOT)
(NOT-BELOWOF16-ROBOT)
(BELOWOF16-ROBOT)
(NOT-BELOWOF15-ROBOT)
(BELOWOF15-ROBOT)
(NOT-BELOWOF14-ROBOT)
(BELOWOF14-ROBOT)
(NOT-BELOWOF13-ROBOT)
(BELOWOF13-ROBOT)
(NOT-BELOWOF12-ROBOT)
(not (NOT-BELOWOF19-ROBOT))
(not (BELOWOF12-ROBOT))
)
)
(when
(and
(BELOWOF18-ROBOT)
)
(and
(BELOWOF19-ROBOT)
(NOT-BELOWOF18-ROBOT)
(not (NOT-BELOWOF19-ROBOT))
(not (BELOWOF18-ROBOT))
)
)
(when
(and
(BELOWOF11-ROBOT)
)
(and
(BELOWOF19-ROBOT)
(NOT-BELOWOF18-ROBOT)
(BELOWOF18-ROBOT)
(NOT-BELOWOF17-ROBOT)
(BELOWOF17-ROBOT)
(NOT-BELOWOF16-ROBOT)
(BELOWOF16-ROBOT)
(NOT-BELOWOF15-ROBOT)
(BELOWOF15-ROBOT)
(NOT-BELOWOF14-ROBOT)
(BELOWOF14-ROBOT)
(NOT-BELOWOF13-ROBOT)
(BELOWOF13-ROBOT)
(NOT-BELOWOF12-ROBOT)
(BELOWOF12-ROBOT)
(NOT-BELOWOF11-ROBOT)
(not (NOT-BELOWOF19-ROBOT))
(not (BELOWOF11-ROBOT))
)
)
(when
(and
(BELOWOF4-ROBOT)
)
(and
(BELOWOF19-ROBOT)
(NOT-BELOWOF18-ROBOT)
(BELOWOF18-ROBOT)
(NOT-BELOWOF17-ROBOT)
(BELOWOF17-ROBOT)
(NOT-BELOWOF16-ROBOT)
(BELOWOF16-ROBOT)
(NOT-BELOWOF15-ROBOT)
(BELOWOF15-ROBOT)
(NOT-BELOWOF14-ROBOT)
(BELOWOF14-ROBOT)
(NOT-BELOWOF13-ROBOT)
(BELOWOF13-ROBOT)
(NOT-BELOWOF12-ROBOT)
(BELOWOF12-ROBOT)
(NOT-BELOWOF11-ROBOT)
(BELOWOF11-ROBOT)
(NOT-BELOWOF10-ROBOT)
(BELOWOF10-ROBOT)
(NOT-BELOWOF9-ROBOT)
(BELOWOF9-ROBOT)
(NOT-BELOWOF8-ROBOT)
(BELOWOF8-ROBOT)
(NOT-BELOWOF7-ROBOT)
(BELOWOF7-ROBOT)
(NOT-BELOWOF6-ROBOT)
(BELOWOF6-ROBOT)
(NOT-BELOWOF5-ROBOT)
(BELOWOF5-ROBOT)
(NOT-BELOWOF4-ROBOT)
(not (NOT-BELOWOF19-ROBOT))
(not (BELOWOF4-ROBOT))
)
)
(when
(and
(BELOWOF1-ROBOT)
)
(and
(BELOWOF19-ROBOT)
(NOT-BELOWOF18-ROBOT)
(BELOWOF18-ROBOT)
(NOT-BELOWOF17-ROBOT)
(BELOWOF17-ROBOT)
(NOT-BELOWOF16-ROBOT)
(BELOWOF16-ROBOT)
(NOT-BELOWOF15-ROBOT)
(BELOWOF15-ROBOT)
(NOT-BELOWOF14-ROBOT)
(BELOWOF14-ROBOT)
(NOT-BELOWOF13-ROBOT)
(BELOWOF13-ROBOT)
(NOT-BELOWOF12-ROBOT)
(BELOWOF12-ROBOT)
(NOT-BELOWOF11-ROBOT)
(BELOWOF11-ROBOT)
(NOT-BELOWOF10-ROBOT)
(BELOWOF10-ROBOT)
(NOT-BELOWOF9-ROBOT)
(BELOWOF9-ROBOT)
(NOT-BELOWOF8-ROBOT)
(BELOWOF8-ROBOT)
(NOT-BELOWOF7-ROBOT)
(BELOWOF7-ROBOT)
(NOT-BELOWOF6-ROBOT)
(BELOWOF6-ROBOT)
(NOT-BELOWOF5-ROBOT)
(BELOWOF5-ROBOT)
(NOT-BELOWOF4-ROBOT)
(BELOWOF4-ROBOT)
(NOT-BELOWOF3-ROBOT)
(BELOWOF3-ROBOT)
(NOT-BELOWOF2-ROBOT)
(BELOWOF2-ROBOT)
(NOT-BELOWOF1-ROBOT)
(not (NOT-BELOWOF19-ROBOT))
(not (BELOWOF1-ROBOT))
)
)
(when
(and
(BELOWOF2-ROBOT)
)
(and
(BELOWOF19-ROBOT)
(NOT-BELOWOF18-ROBOT)
(BELOWOF18-ROBOT)
(NOT-BELOWOF17-ROBOT)
(BELOWOF17-ROBOT)
(NOT-BELOWOF16-ROBOT)
(BELOWOF16-ROBOT)
(NOT-BELOWOF15-ROBOT)
(BELOWOF15-ROBOT)
(NOT-BELOWOF14-ROBOT)
(BELOWOF14-ROBOT)
(NOT-BELOWOF13-ROBOT)
(BELOWOF13-ROBOT)
(NOT-BELOWOF12-ROBOT)
(BELOWOF12-ROBOT)
(NOT-BELOWOF11-ROBOT)
(BELOWOF11-ROBOT)
(NOT-BELOWOF10-ROBOT)
(BELOWOF10-ROBOT)
(NOT-BELOWOF9-ROBOT)
(BELOWOF9-ROBOT)
(NOT-BELOWOF8-ROBOT)
(BELOWOF8-ROBOT)
(NOT-BELOWOF7-ROBOT)
(BELOWOF7-ROBOT)
(NOT-BELOWOF6-ROBOT)
(BELOWOF6-ROBOT)
(NOT-BELOWOF5-ROBOT)
(BELOWOF5-ROBOT)
(NOT-BELOWOF4-ROBOT)
(BELOWOF4-ROBOT)
(NOT-BELOWOF3-ROBOT)
(BELOWOF3-ROBOT)
(NOT-BELOWOF2-ROBOT)
(not (NOT-BELOWOF19-ROBOT))
(not (BELOWOF2-ROBOT))
)
)
(when
(and
(BELOWOF5-ROBOT)
)
(and
(BELOWOF19-ROBOT)
(NOT-BELOWOF18-ROBOT)
(BELOWOF18-ROBOT)
(NOT-BELOWOF17-ROBOT)
(BELOWOF17-ROBOT)
(NOT-BELOWOF16-ROBOT)
(BELOWOF16-ROBOT)
(NOT-BELOWOF15-ROBOT)
(BELOWOF15-ROBOT)
(NOT-BELOWOF14-ROBOT)
(BELOWOF14-ROBOT)
(NOT-BELOWOF13-ROBOT)
(BELOWOF13-ROBOT)
(NOT-BELOWOF12-ROBOT)
(BELOWOF12-ROBOT)
(NOT-BELOWOF11-ROBOT)
(BELOWOF11-ROBOT)
(NOT-BELOWOF10-ROBOT)
(BELOWOF10-ROBOT)
(NOT-BELOWOF9-ROBOT)
(BELOWOF9-ROBOT)
(NOT-BELOWOF8-ROBOT)
(BELOWOF8-ROBOT)
(NOT-BELOWOF7-ROBOT)
(BELOWOF7-ROBOT)
(NOT-BELOWOF6-ROBOT)
(BELOWOF6-ROBOT)
(NOT-BELOWOF5-ROBOT)
(not (NOT-BELOWOF19-ROBOT))
(not (BELOWOF5-ROBOT))
)
)
(when
(and
(BELOWOF17-ROBOT)
)
(and
(BELOWOF19-ROBOT)
(NOT-BELOWOF18-ROBOT)
(BELOWOF18-ROBOT)
(NOT-BELOWOF17-ROBOT)
(not (NOT-BELOWOF19-ROBOT))
(not (BELOWOF17-ROBOT))
)
)
(when
(and
(BELOWOF6-ROBOT)
)
(and
(BELOWOF19-ROBOT)
(NOT-BELOWOF18-ROBOT)
(BELOWOF18-ROBOT)
(NOT-BELOWOF17-ROBOT)
(BELOWOF17-ROBOT)
(NOT-BELOWOF16-ROBOT)
(BELOWOF16-ROBOT)
(NOT-BELOWOF15-ROBOT)
(BELOWOF15-ROBOT)
(NOT-BELOWOF14-ROBOT)
(BELOWOF14-ROBOT)
(NOT-BELOWOF13-ROBOT)
(BELOWOF13-ROBOT)
(NOT-BELOWOF12-ROBOT)
(BELOWOF12-ROBOT)
(NOT-BELOWOF11-ROBOT)
(BELOWOF11-ROBOT)
(NOT-BELOWOF10-ROBOT)
(BELOWOF10-ROBOT)
(NOT-BELOWOF9-ROBOT)
(BELOWOF9-ROBOT)
(NOT-BELOWOF8-ROBOT)
(BELOWOF8-ROBOT)
(NOT-BELOWOF7-ROBOT)
(BELOWOF7-ROBOT)
(NOT-BELOWOF6-ROBOT)
(not (NOT-BELOWOF19-ROBOT))
(not (BELOWOF6-ROBOT))
)
)
(when
(and
(BELOWOF15-ROBOT)
)
(and
(BELOWOF19-ROBOT)
(NOT-BELOWOF18-ROBOT)
(BELOWOF18-ROBOT)
(NOT-BELOWOF17-ROBOT)
(BELOWOF17-ROBOT)
(NOT-BELOWOF16-ROBOT)
(BELOWOF16-ROBOT)
(NOT-BELOWOF15-ROBOT)
(not (NOT-BELOWOF19-ROBOT))
(not (BELOWOF15-ROBOT))
)
)
(when
(and
(BELOWOF16-ROBOT)
)
(and
(BELOWOF19-ROBOT)
(NOT-BELOWOF18-ROBOT)
(BELOWOF18-ROBOT)
(NOT-BELOWOF17-ROBOT)
(BELOWOF17-ROBOT)
(NOT-BELOWOF16-ROBOT)
(not (NOT-BELOWOF19-ROBOT))
(not (BELOWOF16-ROBOT))
)
)
(when
(and
(BELOWOF10-ROBOT)
)
(and
(BELOWOF19-ROBOT)
(NOT-BELOWOF18-ROBOT)
(BELOWOF18-ROBOT)
(NOT-BELOWOF17-ROBOT)
(BELOWOF17-ROBOT)
(NOT-BELOWOF16-ROBOT)
(BELOWOF16-ROBOT)
(NOT-BELOWOF15-ROBOT)
(BELOWOF15-ROBOT)
(NOT-BELOWOF14-ROBOT)
(BELOWOF14-ROBOT)
(NOT-BELOWOF13-ROBOT)
(BELOWOF13-ROBOT)
(NOT-BELOWOF12-ROBOT)
(BELOWOF12-ROBOT)
(NOT-BELOWOF11-ROBOT)
(BELOWOF11-ROBOT)
(NOT-BELOWOF10-ROBOT)
(not (NOT-BELOWOF19-ROBOT))
(not (BELOWOF10-ROBOT))
)
)
(when
(and
(ABOVEOF18-ROBOT)
)
(and
(ABOVEOF17-ROBOT)
(ABOVEOF16-ROBOT)
(ABOVEOF15-ROBOT)
(ABOVEOF14-ROBOT)
(ABOVEOF13-ROBOT)
(ABOVEOF12-ROBOT)
(ABOVEOF11-ROBOT)
(ABOVEOF10-ROBOT)
(ABOVEOF9-ROBOT)
(ABOVEOF8-ROBOT)
(ABOVEOF7-ROBOT)
(ABOVEOF6-ROBOT)
(ABOVEOF5-ROBOT)
(ABOVEOF4-ROBOT)
(ABOVEOF3-ROBOT)
(ABOVEOF2-ROBOT)
(not (NOT-ABOVEOF18-ROBOT))
(not (NOT-ABOVEOF17-ROBOT))
(not (NOT-ABOVEOF16-ROBOT))
(not (NOT-ABOVEOF15-ROBOT))
(not (NOT-ABOVEOF14-ROBOT))
(not (NOT-ABOVEOF13-ROBOT))
(not (NOT-ABOVEOF12-ROBOT))
(not (NOT-ABOVEOF11-ROBOT))
(not (NOT-ABOVEOF10-ROBOT))
(not (NOT-ABOVEOF9-ROBOT))
(not (NOT-ABOVEOF8-ROBOT))
(not (NOT-ABOVEOF7-ROBOT))
(not (NOT-ABOVEOF6-ROBOT))
(not (NOT-ABOVEOF5-ROBOT))
(not (NOT-ABOVEOF4-ROBOT))
(not (NOT-ABOVEOF3-ROBOT))
(not (NOT-ABOVEOF2-ROBOT))
)
)
(when
(and
(ABOVEOF17-ROBOT)
)
(and
(ABOVEOF18-ROBOT)
(ABOVEOF16-ROBOT)
(ABOVEOF15-ROBOT)
(ABOVEOF14-ROBOT)
(ABOVEOF13-ROBOT)
(ABOVEOF12-ROBOT)
(ABOVEOF11-ROBOT)
(ABOVEOF10-ROBOT)
(ABOVEOF9-ROBOT)
(ABOVEOF8-ROBOT)
(ABOVEOF7-ROBOT)
(ABOVEOF6-ROBOT)
(ABOVEOF5-ROBOT)
(ABOVEOF4-ROBOT)
(ABOVEOF3-ROBOT)
(ABOVEOF2-ROBOT)
(not (NOT-ABOVEOF18-ROBOT))
(not (NOT-ABOVEOF17-ROBOT))
(not (NOT-ABOVEOF16-ROBOT))
(not (NOT-ABOVEOF15-ROBOT))
(not (NOT-ABOVEOF14-ROBOT))
(not (NOT-ABOVEOF13-ROBOT))
(not (NOT-ABOVEOF12-ROBOT))
(not (NOT-ABOVEOF11-ROBOT))
(not (NOT-ABOVEOF10-ROBOT))
(not (NOT-ABOVEOF9-ROBOT))
(not (NOT-ABOVEOF8-ROBOT))
(not (NOT-ABOVEOF7-ROBOT))
(not (NOT-ABOVEOF6-ROBOT))
(not (NOT-ABOVEOF5-ROBOT))
(not (NOT-ABOVEOF4-ROBOT))
(not (NOT-ABOVEOF3-ROBOT))
(not (NOT-ABOVEOF2-ROBOT))
)
)
(when
(and
(ABOVEOF16-ROBOT)
)
(and
(ABOVEOF17-ROBOT)
(ABOVEOF15-ROBOT)
(ABOVEOF14-ROBOT)
(ABOVEOF13-ROBOT)
(ABOVEOF12-ROBOT)
(ABOVEOF11-ROBOT)
(ABOVEOF10-ROBOT)
(ABOVEOF9-ROBOT)
(ABOVEOF8-ROBOT)
(ABOVEOF7-ROBOT)
(ABOVEOF6-ROBOT)
(ABOVEOF5-ROBOT)
(ABOVEOF4-ROBOT)
(ABOVEOF3-ROBOT)
(ABOVEOF2-ROBOT)
(not (NOT-ABOVEOF17-ROBOT))
(not (NOT-ABOVEOF16-ROBOT))
(not (NOT-ABOVEOF15-ROBOT))
(not (NOT-ABOVEOF14-ROBOT))
(not (NOT-ABOVEOF13-ROBOT))
(not (NOT-ABOVEOF12-ROBOT))
(not (NOT-ABOVEOF11-ROBOT))
(not (NOT-ABOVEOF10-ROBOT))
(not (NOT-ABOVEOF9-ROBOT))
(not (NOT-ABOVEOF8-ROBOT))
(not (NOT-ABOVEOF7-ROBOT))
(not (NOT-ABOVEOF6-ROBOT))
(not (NOT-ABOVEOF5-ROBOT))
(not (NOT-ABOVEOF4-ROBOT))
(not (NOT-ABOVEOF3-ROBOT))
(not (NOT-ABOVEOF2-ROBOT))
)
)
(when
(and
(ABOVEOF15-ROBOT)
)
(and
(ABOVEOF16-ROBOT)
(ABOVEOF14-ROBOT)
(ABOVEOF13-ROBOT)
(ABOVEOF12-ROBOT)
(ABOVEOF11-ROBOT)
(ABOVEOF10-ROBOT)
(ABOVEOF9-ROBOT)
(ABOVEOF8-ROBOT)
(ABOVEOF7-ROBOT)
(ABOVEOF6-ROBOT)
(ABOVEOF5-ROBOT)
(ABOVEOF4-ROBOT)
(ABOVEOF3-ROBOT)
(ABOVEOF2-ROBOT)
(not (NOT-ABOVEOF16-ROBOT))
(not (NOT-ABOVEOF15-ROBOT))
(not (NOT-ABOVEOF14-ROBOT))
(not (NOT-ABOVEOF13-ROBOT))
(not (NOT-ABOVEOF12-ROBOT))
(not (NOT-ABOVEOF11-ROBOT))
(not (NOT-ABOVEOF10-ROBOT))
(not (NOT-ABOVEOF9-ROBOT))
(not (NOT-ABOVEOF8-ROBOT))
(not (NOT-ABOVEOF7-ROBOT))
(not (NOT-ABOVEOF6-ROBOT))
(not (NOT-ABOVEOF5-ROBOT))
(not (NOT-ABOVEOF4-ROBOT))
(not (NOT-ABOVEOF3-ROBOT))
(not (NOT-ABOVEOF2-ROBOT))
)
)
(when
(and
(ABOVEOF14-ROBOT)
)
(and
(ABOVEOF15-ROBOT)
(ABOVEOF13-ROBOT)
(ABOVEOF12-ROBOT)
(ABOVEOF11-ROBOT)
(ABOVEOF10-ROBOT)
(ABOVEOF9-ROBOT)
(ABOVEOF8-ROBOT)
(ABOVEOF7-ROBOT)
(ABOVEOF6-ROBOT)
(ABOVEOF5-ROBOT)
(ABOVEOF4-ROBOT)
(ABOVEOF3-ROBOT)
(ABOVEOF2-ROBOT)
(not (NOT-ABOVEOF15-ROBOT))
(not (NOT-ABOVEOF14-ROBOT))
(not (NOT-ABOVEOF13-ROBOT))
(not (NOT-ABOVEOF12-ROBOT))
(not (NOT-ABOVEOF11-ROBOT))
(not (NOT-ABOVEOF10-ROBOT))
(not (NOT-ABOVEOF9-ROBOT))
(not (NOT-ABOVEOF8-ROBOT))
(not (NOT-ABOVEOF7-ROBOT))
(not (NOT-ABOVEOF6-ROBOT))
(not (NOT-ABOVEOF5-ROBOT))
(not (NOT-ABOVEOF4-ROBOT))
(not (NOT-ABOVEOF3-ROBOT))
(not (NOT-ABOVEOF2-ROBOT))
)
)
(when
(and
(ABOVEOF13-ROBOT)
)
(and
(ABOVEOF14-ROBOT)
(ABOVEOF12-ROBOT)
(ABOVEOF11-ROBOT)
(ABOVEOF10-ROBOT)
(ABOVEOF9-ROBOT)
(ABOVEOF8-ROBOT)
(ABOVEOF7-ROBOT)
(ABOVEOF6-ROBOT)
(ABOVEOF5-ROBOT)
(ABOVEOF4-ROBOT)
(ABOVEOF3-ROBOT)
(ABOVEOF2-ROBOT)
(not (NOT-ABOVEOF14-ROBOT))
(not (NOT-ABOVEOF13-ROBOT))
(not (NOT-ABOVEOF12-ROBOT))
(not (NOT-ABOVEOF11-ROBOT))
(not (NOT-ABOVEOF10-ROBOT))
(not (NOT-ABOVEOF9-ROBOT))
(not (NOT-ABOVEOF8-ROBOT))
(not (NOT-ABOVEOF7-ROBOT))
(not (NOT-ABOVEOF6-ROBOT))
(not (NOT-ABOVEOF5-ROBOT))
(not (NOT-ABOVEOF4-ROBOT))
(not (NOT-ABOVEOF3-ROBOT))
(not (NOT-ABOVEOF2-ROBOT))
)
)
(when
(and
(ABOVEOF12-ROBOT)
)
(and
(ABOVEOF13-ROBOT)
(ABOVEOF11-ROBOT)
(ABOVEOF10-ROBOT)
(ABOVEOF9-ROBOT)
(ABOVEOF8-ROBOT)
(ABOVEOF7-ROBOT)
(ABOVEOF6-ROBOT)
(ABOVEOF5-ROBOT)
(ABOVEOF4-ROBOT)
(ABOVEOF3-ROBOT)
(ABOVEOF2-ROBOT)
(not (NOT-ABOVEOF13-ROBOT))
(not (NOT-ABOVEOF12-ROBOT))
(not (NOT-ABOVEOF11-ROBOT))
(not (NOT-ABOVEOF10-ROBOT))
(not (NOT-ABOVEOF9-ROBOT))
(not (NOT-ABOVEOF8-ROBOT))
(not (NOT-ABOVEOF7-ROBOT))
(not (NOT-ABOVEOF6-ROBOT))
(not (NOT-ABOVEOF5-ROBOT))
(not (NOT-ABOVEOF4-ROBOT))
(not (NOT-ABOVEOF3-ROBOT))
(not (NOT-ABOVEOF2-ROBOT))
)
)
(when
(and
(ABOVEOF11-ROBOT)
)
(and
(ABOVEOF12-ROBOT)
(ABOVEOF10-ROBOT)
(ABOVEOF9-ROBOT)
(ABOVEOF8-ROBOT)
(ABOVEOF7-ROBOT)
(ABOVEOF6-ROBOT)
(ABOVEOF5-ROBOT)
(ABOVEOF4-ROBOT)
(ABOVEOF3-ROBOT)
(ABOVEOF2-ROBOT)
(not (NOT-ABOVEOF12-ROBOT))
(not (NOT-ABOVEOF11-ROBOT))
(not (NOT-ABOVEOF10-ROBOT))
(not (NOT-ABOVEOF9-ROBOT))
(not (NOT-ABOVEOF8-ROBOT))
(not (NOT-ABOVEOF7-ROBOT))
(not (NOT-ABOVEOF6-ROBOT))
(not (NOT-ABOVEOF5-ROBOT))
(not (NOT-ABOVEOF4-ROBOT))
(not (NOT-ABOVEOF3-ROBOT))
(not (NOT-ABOVEOF2-ROBOT))
)
)
(when
(and
(ABOVEOF10-ROBOT)
)
(and
(ABOVEOF11-ROBOT)
(ABOVEOF9-ROBOT)
(ABOVEOF8-ROBOT)
(ABOVEOF7-ROBOT)
(ABOVEOF6-ROBOT)
(ABOVEOF5-ROBOT)
(ABOVEOF4-ROBOT)
(ABOVEOF3-ROBOT)
(ABOVEOF2-ROBOT)
(not (NOT-ABOVEOF11-ROBOT))
(not (NOT-ABOVEOF10-ROBOT))
(not (NOT-ABOVEOF9-ROBOT))
(not (NOT-ABOVEOF8-ROBOT))
(not (NOT-ABOVEOF7-ROBOT))
(not (NOT-ABOVEOF6-ROBOT))
(not (NOT-ABOVEOF5-ROBOT))
(not (NOT-ABOVEOF4-ROBOT))
(not (NOT-ABOVEOF3-ROBOT))
(not (NOT-ABOVEOF2-ROBOT))
)
)
(when
(and
(ABOVEOF9-ROBOT)
)
(and
(ABOVEOF10-ROBOT)
(ABOVEOF8-ROBOT)
(ABOVEOF7-ROBOT)
(ABOVEOF6-ROBOT)
(ABOVEOF5-ROBOT)
(ABOVEOF4-ROBOT)
(ABOVEOF3-ROBOT)
(ABOVEOF2-ROBOT)
(not (NOT-ABOVEOF10-ROBOT))
(not (NOT-ABOVEOF9-ROBOT))
(not (NOT-ABOVEOF8-ROBOT))
(not (NOT-ABOVEOF7-ROBOT))
(not (NOT-ABOVEOF6-ROBOT))
(not (NOT-ABOVEOF5-ROBOT))
(not (NOT-ABOVEOF4-ROBOT))
(not (NOT-ABOVEOF3-ROBOT))
(not (NOT-ABOVEOF2-ROBOT))
)
)
(when
(and
(ABOVEOF8-ROBOT)
)
(and
(ABOVEOF9-ROBOT)
(ABOVEOF7-ROBOT)
(ABOVEOF6-ROBOT)
(ABOVEOF5-ROBOT)
(ABOVEOF4-ROBOT)
(ABOVEOF3-ROBOT)
(ABOVEOF2-ROBOT)
(not (NOT-ABOVEOF9-ROBOT))
(not (NOT-ABOVEOF8-ROBOT))
(not (NOT-ABOVEOF7-ROBOT))
(not (NOT-ABOVEOF6-ROBOT))
(not (NOT-ABOVEOF5-ROBOT))
(not (NOT-ABOVEOF4-ROBOT))
(not (NOT-ABOVEOF3-ROBOT))
(not (NOT-ABOVEOF2-ROBOT))
)
)
(when
(and
(ABOVEOF7-ROBOT)
)
(and
(ABOVEOF8-ROBOT)
(ABOVEOF6-ROBOT)
(ABOVEOF5-ROBOT)
(ABOVEOF4-ROBOT)
(ABOVEOF3-ROBOT)
(ABOVEOF2-ROBOT)
(not (NOT-ABOVEOF8-ROBOT))
(not (NOT-ABOVEOF7-ROBOT))
(not (NOT-ABOVEOF6-ROBOT))
(not (NOT-ABOVEOF5-ROBOT))
(not (NOT-ABOVEOF4-ROBOT))
(not (NOT-ABOVEOF3-ROBOT))
(not (NOT-ABOVEOF2-ROBOT))
)
)
(when
(and
(ABOVEOF6-ROBOT)
)
(and
(ABOVEOF7-ROBOT)
(ABOVEOF5-ROBOT)
(ABOVEOF4-ROBOT)
(ABOVEOF3-ROBOT)
(ABOVEOF2-ROBOT)
(not (NOT-ABOVEOF7-ROBOT))
(not (NOT-ABOVEOF6-ROBOT))
(not (NOT-ABOVEOF5-ROBOT))
(not (NOT-ABOVEOF4-ROBOT))
(not (NOT-ABOVEOF3-ROBOT))
(not (NOT-ABOVEOF2-ROBOT))
)
)
(when
(and
(ABOVEOF5-ROBOT)
)
(and
(ABOVEOF6-ROBOT)
(ABOVEOF4-ROBOT)
(ABOVEOF3-ROBOT)
(ABOVEOF2-ROBOT)
(not (NOT-ABOVEOF6-ROBOT))
(not (NOT-ABOVEOF5-ROBOT))
(not (NOT-ABOVEOF4-ROBOT))
(not (NOT-ABOVEOF3-ROBOT))
(not (NOT-ABOVEOF2-ROBOT))
)
)
(when
(and
(ABOVEOF4-ROBOT)
)
(and
(ABOVEOF5-ROBOT)
(ABOVEOF3-ROBOT)
(ABOVEOF2-ROBOT)
(not (NOT-ABOVEOF5-ROBOT))
(not (NOT-ABOVEOF4-ROBOT))
(not (NOT-ABOVEOF3-ROBOT))
(not (NOT-ABOVEOF2-ROBOT))
)
)
(when
(and
(ABOVEOF3-ROBOT)
)
(and
(ABOVEOF4-ROBOT)
(ABOVEOF2-ROBOT)
(not (NOT-ABOVEOF4-ROBOT))
(not (NOT-ABOVEOF3-ROBOT))
(not (NOT-ABOVEOF2-ROBOT))
)
)
(when
(and
(ABOVEOF2-ROBOT)
)
(and
(ABOVEOF3-ROBOT)
(not (NOT-ABOVEOF3-ROBOT))
(not (NOT-ABOVEOF2-ROBOT))
)
)
(when
(and
(ABOVEOF1-ROBOT)
)
(and
(ABOVEOF2-ROBOT)
(not (NOT-ABOVEOF2-ROBOT))
)
)
(ABOVEOF1-ROBOT)
(CHECKCONSISTENCY-)
(not (NOT-ABOVEOF1-ROBOT))
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action MOVEDOWN-ROBOT
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
)
:effect
(and
(when
(and
(Flag521-)
)
(and
(ROW17-ROBOT)
(not (NOT-ROW17-ROBOT))
)
)
(when
(and
(Flag519-)
)
(and
(ROW16-ROBOT)
(not (NOT-ROW16-ROBOT))
)
)
(when
(and
(Flag516-)
)
(and
(ROW15-ROBOT)
(not (NOT-ROW15-ROBOT))
)
)
(when
(and
(Flag512-)
)
(and
(ROW14-ROBOT)
(not (NOT-ROW14-ROBOT))
)
)
(when
(and
(Flag507-)
)
(and
(ROW13-ROBOT)
(not (NOT-ROW13-ROBOT))
)
)
(when
(and
(Flag501-)
)
(and
(ROW12-ROBOT)
(not (NOT-ROW12-ROBOT))
)
)
(when
(and
(Flag493-)
)
(and
(ROW11-ROBOT)
(not (NOT-ROW11-ROBOT))
)
)
(when
(and
(Flag484-)
)
(and
(ROW10-ROBOT)
(not (NOT-ROW10-ROBOT))
)
)
(when
(and
(Flag475-)
)
(and
(ROW9-ROBOT)
(not (NOT-ROW9-ROBOT))
)
)
(when
(and
(Flag465-)
)
(and
(ROW8-ROBOT)
(not (NOT-ROW8-ROBOT))
)
)
(when
(and
(Flag453-)
)
(and
(ROW7-ROBOT)
(not (NOT-ROW7-ROBOT))
)
)
(when
(and
(Flag439-)
)
(and
(ROW6-ROBOT)
(not (NOT-ROW6-ROBOT))
)
)
(when
(and
(Flag425-)
)
(and
(ROW5-ROBOT)
(not (NOT-ROW5-ROBOT))
)
)
(when
(and
(Flag408-)
)
(and
(ROW4-ROBOT)
(not (NOT-ROW4-ROBOT))
)
)
(when
(and
(Flag391-)
)
(and
(ROW3-ROBOT)
(not (NOT-ROW3-ROBOT))
)
)
(when
(and
(Flag374-)
)
(and
(ROW2-ROBOT)
(not (NOT-ROW2-ROBOT))
)
)
(when
(and
(Flag355-)
)
(and
(ROW1-ROBOT)
(not (NOT-ROW1-ROBOT))
)
)
(when
(and
(Flag336-)
)
(and
(ROW0-ROBOT)
(not (NOT-ROW0-ROBOT))
)
)
(when
(and
(ROW18-ROBOT)
)
(and
(ROW17-ROBOT)
(NOT-ROW18-ROBOT)
(not (NOT-ROW17-ROBOT))
(not (ROW18-ROBOT))
)
)
(when
(and
(ROW17-ROBOT)
)
(and
(ROW16-ROBOT)
(NOT-ROW17-ROBOT)
(not (NOT-ROW16-ROBOT))
(not (ROW17-ROBOT))
)
)
(when
(and
(ROW16-ROBOT)
)
(and
(ROW15-ROBOT)
(NOT-ROW16-ROBOT)
(not (NOT-ROW15-ROBOT))
(not (ROW16-ROBOT))
)
)
(when
(and
(ROW15-ROBOT)
)
(and
(ROW14-ROBOT)
(NOT-ROW15-ROBOT)
(not (NOT-ROW14-ROBOT))
(not (ROW15-ROBOT))
)
)
(when
(and
(ROW14-ROBOT)
)
(and
(ROW13-ROBOT)
(NOT-ROW14-ROBOT)
(not (NOT-ROW13-ROBOT))
(not (ROW14-ROBOT))
)
)
(when
(and
(ROW13-ROBOT)
)
(and
(ROW12-ROBOT)
(NOT-ROW13-ROBOT)
(not (NOT-ROW12-ROBOT))
(not (ROW13-ROBOT))
)
)
(when
(and
(ROW12-ROBOT)
)
(and
(ROW11-ROBOT)
(NOT-ROW12-ROBOT)
(not (NOT-ROW11-ROBOT))
(not (ROW12-ROBOT))
)
)
(when
(and
(ROW11-ROBOT)
)
(and
(ROW10-ROBOT)
(NOT-ROW11-ROBOT)
(not (NOT-ROW10-ROBOT))
(not (ROW11-ROBOT))
)
)
(when
(and
(ROW10-ROBOT)
)
(and
(ROW9-ROBOT)
(NOT-ROW10-ROBOT)
(not (NOT-ROW9-ROBOT))
(not (ROW10-ROBOT))
)
)
(when
(and
(ROW9-ROBOT)
)
(and
(ROW8-ROBOT)
(NOT-ROW9-ROBOT)
(not (NOT-ROW8-ROBOT))
(not (ROW9-ROBOT))
)
)
(when
(and
(ROW8-ROBOT)
)
(and
(ROW7-ROBOT)
(NOT-ROW8-ROBOT)
(not (NOT-ROW7-ROBOT))
(not (ROW8-ROBOT))
)
)
(when
(and
(ROW7-ROBOT)
)
(and
(ROW6-ROBOT)
(NOT-ROW7-ROBOT)
(not (NOT-ROW6-ROBOT))
(not (ROW7-ROBOT))
)
)
(when
(and
(ROW6-ROBOT)
)
(and
(ROW5-ROBOT)
(NOT-ROW6-ROBOT)
(not (NOT-ROW5-ROBOT))
(not (ROW6-ROBOT))
)
)
(when
(and
(ROW5-ROBOT)
)
(and
(ROW4-ROBOT)
(NOT-ROW5-ROBOT)
(not (NOT-ROW4-ROBOT))
(not (ROW5-ROBOT))
)
)
(when
(and
(ROW4-ROBOT)
)
(and
(ROW3-ROBOT)
(NOT-ROW4-ROBOT)
(not (NOT-ROW3-ROBOT))
(not (ROW4-ROBOT))
)
)
(when
(and
(ROW3-ROBOT)
)
(and
(ROW2-ROBOT)
(NOT-ROW3-ROBOT)
(not (NOT-ROW2-ROBOT))
(not (ROW3-ROBOT))
)
)
(when
(and
(ROW2-ROBOT)
)
(and
(ROW1-ROBOT)
(NOT-ROW2-ROBOT)
(not (NOT-ROW1-ROBOT))
(not (ROW2-ROBOT))
)
)
(when
(and
(ROW1-ROBOT)
)
(and
(ROW0-ROBOT)
(NOT-ROW1-ROBOT)
(not (NOT-ROW0-ROBOT))
(not (ROW1-ROBOT))
)
)
(when
(and
(ABOVEOF18-ROBOT)
)
(and
(ABOVEOF17-ROBOT)
(NOT-ABOVEOF18-ROBOT)
(not (NOT-ABOVEOF17-ROBOT))
(not (ABOVEOF18-ROBOT))
)
)
(when
(and
(Flag298-)
)
(and
(ABOVEOF16-ROBOT)
(NOT-ABOVEOF17-ROBOT)
(not (NOT-ABOVEOF16-ROBOT))
(not (ABOVEOF17-ROBOT))
)
)
(when
(and
(Flag297-)
)
(and
(ABOVEOF15-ROBOT)
(NOT-ABOVEOF16-ROBOT)
(not (NOT-ABOVEOF15-ROBOT))
(not (ABOVEOF16-ROBOT))
)
)
(when
(and
(Flag296-)
)
(and
(ABOVEOF14-ROBOT)
(NOT-ABOVEOF15-ROBOT)
(not (NOT-ABOVEOF14-ROBOT))
(not (ABOVEOF15-ROBOT))
)
)
(when
(and
(Flag295-)
)
(and
(ABOVEOF13-ROBOT)
(NOT-ABOVEOF14-ROBOT)
(not (NOT-ABOVEOF13-ROBOT))
(not (ABOVEOF14-ROBOT))
)
)
(when
(and
(Flag294-)
)
(and
(ABOVEOF12-ROBOT)
(NOT-ABOVEOF13-ROBOT)
(not (NOT-ABOVEOF12-ROBOT))
(not (ABOVEOF13-ROBOT))
)
)
(when
(and
(Flag293-)
)
(and
(ABOVEOF11-ROBOT)
(NOT-ABOVEOF12-ROBOT)
(not (NOT-ABOVEOF11-ROBOT))
(not (ABOVEOF12-ROBOT))
)
)
(when
(and
(Flag292-)
)
(and
(ABOVEOF10-ROBOT)
(NOT-ABOVEOF11-ROBOT)
(not (NOT-ABOVEOF10-ROBOT))
(not (ABOVEOF11-ROBOT))
)
)
(when
(and
(Flag291-)
)
(and
(ABOVEOF9-ROBOT)
(NOT-ABOVEOF10-ROBOT)
(not (NOT-ABOVEOF9-ROBOT))
(not (ABOVEOF10-ROBOT))
)
)
(when
(and
(Flag290-)
)
(and
(ABOVEOF8-ROBOT)
(NOT-ABOVEOF9-ROBOT)
(not (NOT-ABOVEOF8-ROBOT))
(not (ABOVEOF9-ROBOT))
)
)
(when
(and
(Flag289-)
)
(and
(ABOVEOF7-ROBOT)
(NOT-ABOVEOF8-ROBOT)
(not (NOT-ABOVEOF7-ROBOT))
(not (ABOVEOF8-ROBOT))
)
)
(when
(and
(Flag288-)
)
(and
(ABOVEOF6-ROBOT)
(NOT-ABOVEOF7-ROBOT)
(not (NOT-ABOVEOF6-ROBOT))
(not (ABOVEOF7-ROBOT))
)
)
(when
(and
(Flag287-)
)
(and
(ABOVEOF5-ROBOT)
(NOT-ABOVEOF6-ROBOT)
(not (NOT-ABOVEOF5-ROBOT))
(not (ABOVEOF6-ROBOT))
)
)
(when
(and
(Flag286-)
)
(and
(ABOVEOF4-ROBOT)
(NOT-ABOVEOF5-ROBOT)
(not (NOT-ABOVEOF4-ROBOT))
(not (ABOVEOF5-ROBOT))
)
)
(when
(and
(Flag285-)
)
(and
(ABOVEOF3-ROBOT)
(NOT-ABOVEOF4-ROBOT)
(not (NOT-ABOVEOF3-ROBOT))
(not (ABOVEOF4-ROBOT))
)
)
(when
(and
(Flag284-)
)
(and
(ABOVEOF2-ROBOT)
(NOT-ABOVEOF3-ROBOT)
(not (NOT-ABOVEOF2-ROBOT))
(not (ABOVEOF3-ROBOT))
)
)
(when
(and
(Flag283-)
)
(and
(ABOVEOF1-ROBOT)
(NOT-ABOVEOF2-ROBOT)
(not (NOT-ABOVEOF1-ROBOT))
(not (ABOVEOF2-ROBOT))
)
)
(when
(and
(Flag282-)
)
(and
(NOT-ABOVEOF1-ROBOT)
(not (ABOVEOF1-ROBOT))
)
)
(when
(and
(Flag281-)
)
(and
(BELOWOF18-ROBOT)
(not (NOT-BELOWOF18-ROBOT))
)
)
(when
(and
(Flag280-)
)
(and
(BELOWOF17-ROBOT)
(not (NOT-BELOWOF17-ROBOT))
)
)
(when
(and
(Flag279-)
)
(and
(BELOWOF16-ROBOT)
(not (NOT-BELOWOF16-ROBOT))
)
)
(when
(and
(Flag278-)
)
(and
(BELOWOF15-ROBOT)
(not (NOT-BELOWOF15-ROBOT))
)
)
(when
(and
(Flag277-)
)
(and
(BELOWOF14-ROBOT)
(not (NOT-BELOWOF14-ROBOT))
)
)
(when
(and
(Flag276-)
)
(and
(BELOWOF13-ROBOT)
(not (NOT-BELOWOF13-ROBOT))
)
)
(when
(and
(Flag275-)
)
(and
(BELOWOF12-ROBOT)
(not (NOT-BELOWOF12-ROBOT))
)
)
(when
(and
(Flag274-)
)
(and
(BELOWOF11-ROBOT)
(not (NOT-BELOWOF11-ROBOT))
)
)
(when
(and
(Flag273-)
)
(and
(BELOWOF10-ROBOT)
(not (NOT-BELOWOF10-ROBOT))
)
)
(when
(and
(Flag272-)
)
(and
(BELOWOF9-ROBOT)
(not (NOT-BELOWOF9-ROBOT))
)
)
(when
(and
(Flag271-)
)
(and
(BELOWOF8-ROBOT)
(not (NOT-BELOWOF8-ROBOT))
)
)
(when
(and
(Flag270-)
)
(and
(BELOWOF7-ROBOT)
(not (NOT-BELOWOF7-ROBOT))
)
)
(when
(and
(Flag269-)
)
(and
(BELOWOF6-ROBOT)
(not (NOT-BELOWOF6-ROBOT))
)
)
(when
(and
(Flag268-)
)
(and
(BELOWOF5-ROBOT)
(not (NOT-BELOWOF5-ROBOT))
)
)
(when
(and
(Flag267-)
)
(and
(BELOWOF4-ROBOT)
(not (NOT-BELOWOF4-ROBOT))
)
)
(when
(and
(Flag266-)
)
(and
(BELOWOF3-ROBOT)
(not (NOT-BELOWOF3-ROBOT))
)
)
(when
(and
(Flag265-)
)
(and
(BELOWOF2-ROBOT)
(not (NOT-BELOWOF2-ROBOT))
)
)
(when
(and
(Flag264-)
)
(and
(BELOWOF1-ROBOT)
(not (NOT-BELOWOF1-ROBOT))
)
)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:derived (Flag2-)(and (NOT-CHECKCONSISTENCY-)(NOT-ERROR-)))

(:derived (Flag3-)(and (RIGHTOF2-ROBOT)))

(:derived (Flag3-)(and (LEFTOF18-ROBOT)))

(:derived (Flag21-)(and (LEFTOF18-ROBOT)))

(:derived (Flag22-)(and (LEFTOF18-ROBOT)))

(:derived (Flag23-)(and (RIGHTOF2-ROBOT)))

(:derived (Flag24-)(and (RIGHTOF2-ROBOT)))

(:derived (Flag261-)(and (RIGHTOF2-ROBOT)))

(:derived (Flag280-)(and (BELOWOF18-ROBOT)))

(:derived (Flag281-)(and (BELOWOF18-ROBOT)))

)
