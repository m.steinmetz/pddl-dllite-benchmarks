(define (domain grounded-SIMPLE-ADL-ROBOT)
(:requirements
:strips
)
(:predicates
(Flag238-)
(Flag135-)
(Flag134-)
(Flag133-)
(Flag132-)
(Flag15-)
(Flag14-)
(Flag3-)
(Flag2-)
(ROW10-ROBOT)
(ROW9-ROBOT)
(ROW8-ROBOT)
(ROW7-ROBOT)
(ROW6-ROBOT)
(ROW5-ROBOT)
(ROW4-ROBOT)
(ROW3-ROBOT)
(ROW2-ROBOT)
(ROW1-ROBOT)
(ROW0-ROBOT)
(ABOVEOF10-ROBOT)
(ABOVEOF9-ROBOT)
(ABOVEOF8-ROBOT)
(ABOVEOF7-ROBOT)
(ABOVEOF6-ROBOT)
(ABOVEOF5-ROBOT)
(ABOVEOF4-ROBOT)
(ABOVEOF3-ROBOT)
(ABOVEOF2-ROBOT)
(ABOVEOF1-ROBOT)
(BELOWOF10-ROBOT)
(BELOWOF9-ROBOT)
(BELOWOF8-ROBOT)
(BELOWOF7-ROBOT)
(BELOWOF6-ROBOT)
(BELOWOF5-ROBOT)
(BELOWOF4-ROBOT)
(BELOWOF3-ROBOT)
(BELOWOF2-ROBOT)
(BELOWOF1-ROBOT)
(CHECKCONSISTENCY-)
(ROW11-ROBOT)
(BELOWOF12-ROBOT)
(NOT-BELOWOF11-ROBOT)
(ABOVEOF11-ROBOT)
(Flag241-)
(Flag121-)
(Flag119-)
(Flag118-)
(Flag116-)
(Flag115-)
(Flag114-)
(Flag113-)
(Flag111-)
(Flag110-)
(Flag109-)
(Flag108-)
(Flag107-)
(Flag105-)
(Flag104-)
(Flag103-)
(Flag102-)
(Flag101-)
(Flag100-)
(Flag98-)
(Flag97-)
(Flag96-)
(Flag95-)
(Flag94-)
(Flag93-)
(Flag92-)
(Flag91-)
(Flag89-)
(Flag88-)
(Flag87-)
(Flag86-)
(Flag85-)
(Flag84-)
(Flag83-)
(Flag82-)
(Flag80-)
(Flag79-)
(Flag78-)
(Flag77-)
(Flag76-)
(Flag75-)
(Flag74-)
(Flag73-)
(Flag71-)
(Flag70-)
(Flag69-)
(Flag68-)
(Flag67-)
(Flag66-)
(Flag65-)
(Flag64-)
(Flag63-)
(Flag62-)
(Flag61-)
(Flag59-)
(Flag58-)
(Flag57-)
(Flag56-)
(Flag55-)
(Flag54-)
(Flag53-)
(Flag52-)
(Flag51-)
(Flag50-)
(Flag49-)
(Flag47-)
(Flag46-)
(Flag45-)
(Flag44-)
(Flag43-)
(Flag42-)
(Flag41-)
(Flag40-)
(Flag39-)
(Flag38-)
(Flag37-)
(Flag36-)
(Flag35-)
(Flag34-)
(Flag33-)
(Flag32-)
(Flag31-)
(Flag30-)
(Flag29-)
(Flag28-)
(Flag27-)
(Flag26-)
(Flag25-)
(Flag24-)
(Flag23-)
(Flag22-)
(Flag21-)
(Flag20-)
(Flag19-)
(Flag18-)
(Flag17-)
(Flag16-)
(Flag13-)
(Flag12-)
(Flag11-)
(Flag10-)
(Flag9-)
(Flag8-)
(Flag7-)
(Flag6-)
(Flag5-)
(Flag4-)
(ERROR-)
(COLUMN10-ROBOT)
(COLUMN9-ROBOT)
(COLUMN8-ROBOT)
(COLUMN7-ROBOT)
(COLUMN6-ROBOT)
(COLUMN5-ROBOT)
(COLUMN4-ROBOT)
(COLUMN3-ROBOT)
(COLUMN2-ROBOT)
(COLUMN1-ROBOT)
(COLUMN0-ROBOT)
(RIGHTOF10-ROBOT)
(RIGHTOF9-ROBOT)
(RIGHTOF8-ROBOT)
(RIGHTOF7-ROBOT)
(RIGHTOF6-ROBOT)
(RIGHTOF5-ROBOT)
(RIGHTOF4-ROBOT)
(RIGHTOF3-ROBOT)
(RIGHTOF1-ROBOT)
(NOT-RIGHTOF2-ROBOT)
(RIGHTOF0-ROBOT)
(LEFTOF10-ROBOT)
(LEFTOF9-ROBOT)
(LEFTOF8-ROBOT)
(LEFTOF7-ROBOT)
(LEFTOF6-ROBOT)
(LEFTOF5-ROBOT)
(LEFTOF4-ROBOT)
(LEFTOF3-ROBOT)
(LEFTOF2-ROBOT)
(LEFTOF1-ROBOT)
(COLUMN11-ROBOT)
(LEFTOF12-ROBOT)
(NOT-LEFTOF11-ROBOT)
(RIGHTOF11-ROBOT)
(Flag239-)
(Flag236-)
(Flag234-)
(Flag233-)
(Flag231-)
(Flag230-)
(Flag229-)
(Flag227-)
(Flag226-)
(Flag225-)
(Flag224-)
(Flag222-)
(Flag221-)
(Flag220-)
(Flag219-)
(Flag218-)
(Flag217-)
(Flag215-)
(Flag214-)
(Flag213-)
(Flag212-)
(Flag211-)
(Flag210-)
(Flag209-)
(Flag207-)
(Flag206-)
(Flag205-)
(Flag204-)
(Flag203-)
(Flag202-)
(Flag201-)
(Flag199-)
(Flag198-)
(Flag197-)
(Flag196-)
(Flag195-)
(Flag194-)
(Flag193-)
(Flag192-)
(Flag191-)
(Flag190-)
(Flag188-)
(Flag187-)
(Flag186-)
(Flag185-)
(Flag184-)
(Flag183-)
(Flag182-)
(Flag181-)
(Flag180-)
(Flag179-)
(Flag177-)
(Flag176-)
(Flag175-)
(Flag174-)
(Flag173-)
(Flag172-)
(Flag171-)
(Flag170-)
(Flag169-)
(Flag168-)
(Flag166-)
(Flag165-)
(Flag164-)
(Flag163-)
(Flag162-)
(Flag161-)
(Flag160-)
(Flag159-)
(Flag158-)
(Flag157-)
(Flag156-)
(Flag155-)
(Flag154-)
(Flag153-)
(Flag152-)
(Flag151-)
(Flag150-)
(Flag149-)
(Flag148-)
(Flag147-)
(Flag146-)
(Flag145-)
(Flag144-)
(Flag143-)
(Flag142-)
(Flag141-)
(Flag140-)
(Flag139-)
(Flag138-)
(Flag137-)
(Flag136-)
(Flag131-)
(Flag130-)
(Flag129-)
(Flag128-)
(Flag127-)
(Flag126-)
(Flag125-)
(Flag124-)
(Flag123-)
(Flag122-)
(Flag120-)
(Flag117-)
(Flag112-)
(Flag106-)
(Flag99-)
(Flag90-)
(Flag81-)
(Flag72-)
(Flag60-)
(Flag48-)
(Flag1-)
(Flag240-)
(Flag237-)
(Flag235-)
(Flag232-)
(Flag228-)
(Flag223-)
(Flag216-)
(Flag208-)
(Flag200-)
(Flag189-)
(Flag178-)
(Flag167-)
(NOT-COLUMN11-ROBOT)
(NOT-COLUMN10-ROBOT)
(NOT-COLUMN9-ROBOT)
(NOT-COLUMN8-ROBOT)
(NOT-COLUMN7-ROBOT)
(NOT-COLUMN6-ROBOT)
(NOT-COLUMN5-ROBOT)
(NOT-COLUMN4-ROBOT)
(NOT-COLUMN3-ROBOT)
(NOT-COLUMN2-ROBOT)
(NOT-COLUMN1-ROBOT)
(NOT-LEFTOF12-ROBOT)
(LEFTOF11-ROBOT)
(NOT-LEFTOF10-ROBOT)
(NOT-LEFTOF9-ROBOT)
(NOT-LEFTOF8-ROBOT)
(NOT-LEFTOF7-ROBOT)
(NOT-LEFTOF6-ROBOT)
(NOT-LEFTOF5-ROBOT)
(NOT-LEFTOF4-ROBOT)
(NOT-LEFTOF3-ROBOT)
(NOT-LEFTOF2-ROBOT)
(NOT-RIGHTOF11-ROBOT)
(NOT-RIGHTOF10-ROBOT)
(NOT-RIGHTOF9-ROBOT)
(NOT-RIGHTOF8-ROBOT)
(NOT-RIGHTOF7-ROBOT)
(NOT-RIGHTOF6-ROBOT)
(NOT-RIGHTOF5-ROBOT)
(NOT-RIGHTOF4-ROBOT)
(NOT-RIGHTOF3-ROBOT)
(NOT-RIGHTOF1-ROBOT)
(NOT-CHECKCONSISTENCY-)
(NOT-COLUMN0-ROBOT)
(RIGHTOF2-ROBOT)
(NOT-RIGHTOF0-ROBOT)
(NOT-LEFTOF1-ROBOT)
(NOT-ERROR-)
(NOT-ROW11-ROBOT)
(NOT-ROW10-ROBOT)
(NOT-ROW9-ROBOT)
(NOT-ROW8-ROBOT)
(NOT-ROW7-ROBOT)
(NOT-ROW6-ROBOT)
(NOT-ROW5-ROBOT)
(NOT-ROW4-ROBOT)
(NOT-ROW3-ROBOT)
(NOT-ROW2-ROBOT)
(NOT-ROW1-ROBOT)
(NOT-BELOWOF12-ROBOT)
(BELOWOF11-ROBOT)
(NOT-BELOWOF10-ROBOT)
(NOT-BELOWOF9-ROBOT)
(NOT-BELOWOF8-ROBOT)
(NOT-BELOWOF7-ROBOT)
(NOT-BELOWOF6-ROBOT)
(NOT-BELOWOF5-ROBOT)
(NOT-BELOWOF4-ROBOT)
(NOT-BELOWOF3-ROBOT)
(NOT-BELOWOF2-ROBOT)
(NOT-ABOVEOF11-ROBOT)
(NOT-ABOVEOF10-ROBOT)
(NOT-ABOVEOF9-ROBOT)
(NOT-ABOVEOF8-ROBOT)
(NOT-ABOVEOF7-ROBOT)
(NOT-ABOVEOF6-ROBOT)
(NOT-ABOVEOF5-ROBOT)
(NOT-ABOVEOF4-ROBOT)
(NOT-ABOVEOF3-ROBOT)
(NOT-ABOVEOF2-ROBOT)
(NOT-ABOVEOF1-ROBOT)
(NOT-ROW0-ROBOT)
(NOT-BELOWOF1-ROBOT)
)
(:derived (Flag167-)(and (Flag144-)))

(:derived (Flag167-)(and (Flag145-)))

(:derived (Flag167-)(and (Flag146-)))

(:derived (Flag167-)(and (Flag147-)))

(:derived (Flag167-)(and (Flag148-)))

(:derived (Flag167-)(and (Flag149-)))

(:derived (Flag167-)(and (Flag150-)))

(:derived (Flag167-)(and (Flag151-)))

(:derived (Flag167-)(and (Flag152-)))

(:derived (Flag167-)(and (Flag153-)))

(:derived (Flag167-)(and (Flag154-)))

(:derived (Flag167-)(and (Flag155-)))

(:derived (Flag167-)(and (Flag156-)))

(:derived (Flag167-)(and (Flag157-)))

(:derived (Flag167-)(and (Flag158-)))

(:derived (Flag167-)(and (Flag159-)))

(:derived (Flag167-)(and (Flag160-)))

(:derived (Flag167-)(and (Flag161-)))

(:derived (Flag167-)(and (Flag162-)))

(:derived (Flag167-)(and (Flag163-)))

(:derived (Flag167-)(and (Flag164-)))

(:derived (Flag167-)(and (Flag165-)))

(:derived (Flag167-)(and (Flag166-)))

(:derived (Flag178-)(and (Flag144-)))

(:derived (Flag178-)(and (Flag168-)))

(:derived (Flag178-)(and (Flag145-)))

(:derived (Flag178-)(and (Flag146-)))

(:derived (Flag178-)(and (Flag169-)))

(:derived (Flag178-)(and (Flag147-)))

(:derived (Flag178-)(and (Flag149-)))

(:derived (Flag178-)(and (Flag157-)))

(:derived (Flag178-)(and (Flag152-)))

(:derived (Flag178-)(and (Flag170-)))

(:derived (Flag178-)(and (Flag153-)))

(:derived (Flag178-)(and (Flag154-)))

(:derived (Flag178-)(and (Flag158-)))

(:derived (Flag178-)(and (Flag155-)))

(:derived (Flag178-)(and (Flag156-)))

(:derived (Flag178-)(and (Flag171-)))

(:derived (Flag178-)(and (Flag172-)))

(:derived (Flag178-)(and (Flag151-)))

(:derived (Flag178-)(and (Flag173-)))

(:derived (Flag178-)(and (Flag159-)))

(:derived (Flag178-)(and (Flag174-)))

(:derived (Flag178-)(and (Flag175-)))

(:derived (Flag178-)(and (Flag160-)))

(:derived (Flag178-)(and (Flag161-)))

(:derived (Flag178-)(and (Flag162-)))

(:derived (Flag178-)(and (Flag163-)))

(:derived (Flag178-)(and (Flag164-)))

(:derived (Flag178-)(and (Flag165-)))

(:derived (Flag178-)(and (Flag176-)))

(:derived (Flag178-)(and (Flag166-)))

(:derived (Flag178-)(and (Flag177-)))

(:derived (Flag189-)(and (Flag144-)))

(:derived (Flag189-)(and (Flag168-)))

(:derived (Flag189-)(and (Flag145-)))

(:derived (Flag189-)(and (Flag146-)))

(:derived (Flag189-)(and (Flag169-)))

(:derived (Flag189-)(and (Flag147-)))

(:derived (Flag189-)(and (Flag179-)))

(:derived (Flag189-)(and (Flag180-)))

(:derived (Flag189-)(and (Flag149-)))

(:derived (Flag189-)(and (Flag172-)))

(:derived (Flag189-)(and (Flag157-)))

(:derived (Flag189-)(and (Flag152-)))

(:derived (Flag189-)(and (Flag170-)))

(:derived (Flag189-)(and (Flag153-)))

(:derived (Flag189-)(and (Flag154-)))

(:derived (Flag189-)(and (Flag158-)))

(:derived (Flag189-)(and (Flag155-)))

(:derived (Flag189-)(and (Flag156-)))

(:derived (Flag189-)(and (Flag171-)))

(:derived (Flag189-)(and (Flag181-)))

(:derived (Flag189-)(and (Flag151-)))

(:derived (Flag189-)(and (Flag182-)))

(:derived (Flag189-)(and (Flag173-)))

(:derived (Flag189-)(and (Flag159-)))

(:derived (Flag189-)(and (Flag175-)))

(:derived (Flag189-)(and (Flag183-)))

(:derived (Flag189-)(and (Flag184-)))

(:derived (Flag189-)(and (Flag160-)))

(:derived (Flag189-)(and (Flag185-)))

(:derived (Flag189-)(and (Flag186-)))

(:derived (Flag189-)(and (Flag161-)))

(:derived (Flag189-)(and (Flag164-)))

(:derived (Flag189-)(and (Flag187-)))

(:derived (Flag189-)(and (Flag188-)))

(:derived (Flag189-)(and (Flag165-)))

(:derived (Flag189-)(and (Flag176-)))

(:derived (Flag189-)(and (Flag166-)))

(:derived (Flag200-)(and (Flag190-)))

(:derived (Flag200-)(and (Flag144-)))

(:derived (Flag200-)(and (Flag168-)))

(:derived (Flag200-)(and (Flag145-)))

(:derived (Flag200-)(and (Flag191-)))

(:derived (Flag200-)(and (Flag169-)))

(:derived (Flag200-)(and (Flag147-)))

(:derived (Flag200-)(and (Flag192-)))

(:derived (Flag200-)(and (Flag179-)))

(:derived (Flag200-)(and (Flag180-)))

(:derived (Flag200-)(and (Flag149-)))

(:derived (Flag200-)(and (Flag193-)))

(:derived (Flag200-)(and (Flag157-)))

(:derived (Flag200-)(and (Flag152-)))

(:derived (Flag200-)(and (Flag170-)))

(:derived (Flag200-)(and (Flag153-)))

(:derived (Flag200-)(and (Flag194-)))

(:derived (Flag200-)(and (Flag195-)))

(:derived (Flag200-)(and (Flag196-)))

(:derived (Flag200-)(and (Flag155-)))

(:derived (Flag200-)(and (Flag197-)))

(:derived (Flag200-)(and (Flag156-)))

(:derived (Flag200-)(and (Flag171-)))

(:derived (Flag200-)(and (Flag172-)))

(:derived (Flag200-)(and (Flag151-)))

(:derived (Flag200-)(and (Flag182-)))

(:derived (Flag200-)(and (Flag198-)))

(:derived (Flag200-)(and (Flag158-)))

(:derived (Flag200-)(and (Flag159-)))

(:derived (Flag200-)(and (Flag175-)))

(:derived (Flag200-)(and (Flag183-)))

(:derived (Flag200-)(and (Flag184-)))

(:derived (Flag200-)(and (Flag160-)))

(:derived (Flag200-)(and (Flag185-)))

(:derived (Flag200-)(and (Flag186-)))

(:derived (Flag200-)(and (Flag161-)))

(:derived (Flag200-)(and (Flag173-)))

(:derived (Flag200-)(and (Flag164-)))

(:derived (Flag200-)(and (Flag199-)))

(:derived (Flag200-)(and (Flag188-)))

(:derived (Flag200-)(and (Flag165-)))

(:derived (Flag200-)(and (Flag176-)))

(:derived (Flag200-)(and (Flag166-)))

(:derived (Flag208-)(and (Flag190-)))

(:derived (Flag208-)(and (Flag144-)))

(:derived (Flag208-)(and (Flag191-)))

(:derived (Flag208-)(and (Flag145-)))

(:derived (Flag208-)(and (Flag176-)))

(:derived (Flag208-)(and (Flag147-)))

(:derived (Flag208-)(and (Flag180-)))

(:derived (Flag208-)(and (Flag149-)))

(:derived (Flag208-)(and (Flag193-)))

(:derived (Flag208-)(and (Flag151-)))

(:derived (Flag208-)(and (Flag152-)))

(:derived (Flag208-)(and (Flag170-)))

(:derived (Flag208-)(and (Flag153-)))

(:derived (Flag208-)(and (Flag201-)))

(:derived (Flag208-)(and (Flag202-)))

(:derived (Flag208-)(and (Flag195-)))

(:derived (Flag208-)(and (Flag196-)))

(:derived (Flag208-)(and (Flag197-)))

(:derived (Flag208-)(and (Flag156-)))

(:derived (Flag208-)(and (Flag168-)))

(:derived (Flag208-)(and (Flag171-)))

(:derived (Flag208-)(and (Flag172-)))

(:derived (Flag208-)(and (Flag157-)))

(:derived (Flag208-)(and (Flag182-)))

(:derived (Flag208-)(and (Flag198-)))

(:derived (Flag208-)(and (Flag173-)))

(:derived (Flag208-)(and (Flag203-)))

(:derived (Flag208-)(and (Flag159-)))

(:derived (Flag208-)(and (Flag175-)))

(:derived (Flag208-)(and (Flag183-)))

(:derived (Flag208-)(and (Flag204-)))

(:derived (Flag208-)(and (Flag205-)))

(:derived (Flag208-)(and (Flag206-)))

(:derived (Flag208-)(and (Flag184-)))

(:derived (Flag208-)(and (Flag160-)))

(:derived (Flag208-)(and (Flag185-)))

(:derived (Flag208-)(and (Flag186-)))

(:derived (Flag208-)(and (Flag161-)))

(:derived (Flag208-)(and (Flag164-)))

(:derived (Flag208-)(and (Flag199-)))

(:derived (Flag208-)(and (Flag188-)))

(:derived (Flag208-)(and (Flag165-)))

(:derived (Flag208-)(and (Flag192-)))

(:derived (Flag208-)(and (Flag207-)))

(:derived (Flag216-)(and (Flag190-)))

(:derived (Flag216-)(and (Flag144-)))

(:derived (Flag216-)(and (Flag191-)))

(:derived (Flag216-)(and (Flag145-)))

(:derived (Flag216-)(and (Flag209-)))

(:derived (Flag216-)(and (Flag185-)))

(:derived (Flag216-)(and (Flag210-)))

(:derived (Flag216-)(and (Flag211-)))

(:derived (Flag216-)(and (Flag176-)))

(:derived (Flag216-)(and (Flag147-)))

(:derived (Flag216-)(and (Flag168-)))

(:derived (Flag216-)(and (Flag180-)))

(:derived (Flag216-)(and (Flag193-)))

(:derived (Flag216-)(and (Flag157-)))

(:derived (Flag216-)(and (Flag170-)))

(:derived (Flag216-)(and (Flag153-)))

(:derived (Flag216-)(and (Flag201-)))

(:derived (Flag216-)(and (Flag202-)))

(:derived (Flag216-)(and (Flag195-)))

(:derived (Flag216-)(and (Flag196-)))

(:derived (Flag216-)(and (Flag197-)))

(:derived (Flag216-)(and (Flag156-)))

(:derived (Flag216-)(and (Flag212-)))

(:derived (Flag216-)(and (Flag171-)))

(:derived (Flag216-)(and (Flag172-)))

(:derived (Flag216-)(and (Flag151-)))

(:derived (Flag216-)(and (Flag182-)))

(:derived (Flag216-)(and (Flag198-)))

(:derived (Flag216-)(and (Flag159-)))

(:derived (Flag216-)(and (Flag175-)))

(:derived (Flag216-)(and (Flag183-)))

(:derived (Flag216-)(and (Flag204-)))

(:derived (Flag216-)(and (Flag213-)))

(:derived (Flag216-)(and (Flag214-)))

(:derived (Flag216-)(and (Flag205-)))

(:derived (Flag216-)(and (Flag184-)))

(:derived (Flag216-)(and (Flag160-)))

(:derived (Flag216-)(and (Flag215-)))

(:derived (Flag216-)(and (Flag161-)))

(:derived (Flag216-)(and (Flag164-)))

(:derived (Flag216-)(and (Flag188-)))

(:derived (Flag216-)(and (Flag165-)))

(:derived (Flag216-)(and (Flag192-)))

(:derived (Flag216-)(and (Flag207-)))

(:derived (Flag223-)(and (Flag190-)))

(:derived (Flag223-)(and (Flag217-)))

(:derived (Flag223-)(and (Flag191-)))

(:derived (Flag223-)(and (Flag145-)))

(:derived (Flag223-)(and (Flag197-)))

(:derived (Flag223-)(and (Flag209-)))

(:derived (Flag223-)(and (Flag211-)))

(:derived (Flag223-)(and (Flag218-)))

(:derived (Flag223-)(and (Flag147-)))

(:derived (Flag223-)(and (Flag192-)))

(:derived (Flag223-)(and (Flag180-)))

(:derived (Flag223-)(and (Flag193-)))

(:derived (Flag223-)(and (Flag157-)))

(:derived (Flag223-)(and (Flag144-)))

(:derived (Flag223-)(and (Flag170-)))

(:derived (Flag223-)(and (Flag153-)))

(:derived (Flag223-)(and (Flag201-)))

(:derived (Flag223-)(and (Flag202-)))

(:derived (Flag223-)(and (Flag195-)))

(:derived (Flag223-)(and (Flag219-)))

(:derived (Flag223-)(and (Flag220-)))

(:derived (Flag223-)(and (Flag221-)))

(:derived (Flag223-)(and (Flag156-)))

(:derived (Flag223-)(and (Flag171-)))

(:derived (Flag223-)(and (Flag172-)))

(:derived (Flag223-)(and (Flag151-)))

(:derived (Flag223-)(and (Flag182-)))

(:derived (Flag223-)(and (Flag198-)))

(:derived (Flag223-)(and (Flag159-)))

(:derived (Flag223-)(and (Flag175-)))

(:derived (Flag223-)(and (Flag183-)))

(:derived (Flag223-)(and (Flag204-)))

(:derived (Flag223-)(and (Flag213-)))

(:derived (Flag223-)(and (Flag205-)))

(:derived (Flag223-)(and (Flag184-)))

(:derived (Flag223-)(and (Flag160-)))

(:derived (Flag223-)(and (Flag215-)))

(:derived (Flag223-)(and (Flag161-)))

(:derived (Flag223-)(and (Flag222-)))

(:derived (Flag223-)(and (Flag188-)))

(:derived (Flag223-)(and (Flag176-)))

(:derived (Flag223-)(and (Flag207-)))

(:derived (Flag228-)(and (Flag190-)))

(:derived (Flag228-)(and (Flag144-)))

(:derived (Flag228-)(and (Flag145-)))

(:derived (Flag228-)(and (Flag220-)))

(:derived (Flag228-)(and (Flag209-)))

(:derived (Flag228-)(and (Flag211-)))

(:derived (Flag228-)(and (Flag218-)))

(:derived (Flag228-)(and (Flag147-)))

(:derived (Flag228-)(and (Flag192-)))

(:derived (Flag228-)(and (Flag224-)))

(:derived (Flag228-)(and (Flag193-)))

(:derived (Flag228-)(and (Flag225-)))

(:derived (Flag228-)(and (Flag201-)))

(:derived (Flag228-)(and (Flag153-)))

(:derived (Flag228-)(and (Flag202-)))

(:derived (Flag228-)(and (Flag195-)))

(:derived (Flag228-)(and (Flag226-)))

(:derived (Flag228-)(and (Flag219-)))

(:derived (Flag228-)(and (Flag197-)))

(:derived (Flag228-)(and (Flag156-)))

(:derived (Flag228-)(and (Flag171-)))

(:derived (Flag228-)(and (Flag172-)))

(:derived (Flag228-)(and (Flag227-)))

(:derived (Flag228-)(and (Flag198-)))

(:derived (Flag228-)(and (Flag159-)))

(:derived (Flag228-)(and (Flag175-)))

(:derived (Flag228-)(and (Flag183-)))

(:derived (Flag228-)(and (Flag213-)))

(:derived (Flag228-)(and (Flag205-)))

(:derived (Flag228-)(and (Flag184-)))

(:derived (Flag228-)(and (Flag160-)))

(:derived (Flag228-)(and (Flag215-)))

(:derived (Flag228-)(and (Flag161-)))

(:derived (Flag228-)(and (Flag222-)))

(:derived (Flag228-)(and (Flag188-)))

(:derived (Flag228-)(and (Flag176-)))

(:derived (Flag228-)(and (Flag207-)))

(:derived (Flag232-)(and (Flag190-)))

(:derived (Flag232-)(and (Flag144-)))

(:derived (Flag232-)(and (Flag145-)))

(:derived (Flag232-)(and (Flag209-)))

(:derived (Flag232-)(and (Flag215-)))

(:derived (Flag232-)(and (Flag229-)))

(:derived (Flag232-)(and (Flag230-)))

(:derived (Flag232-)(and (Flag218-)))

(:derived (Flag232-)(and (Flag147-)))

(:derived (Flag232-)(and (Flag225-)))

(:derived (Flag232-)(and (Flag153-)))

(:derived (Flag232-)(and (Flag202-)))

(:derived (Flag232-)(and (Flag226-)))

(:derived (Flag232-)(and (Flag219-)))

(:derived (Flag232-)(and (Flag197-)))

(:derived (Flag232-)(and (Flag156-)))

(:derived (Flag232-)(and (Flag171-)))

(:derived (Flag232-)(and (Flag193-)))

(:derived (Flag232-)(and (Flag231-)))

(:derived (Flag232-)(and (Flag227-)))

(:derived (Flag232-)(and (Flag175-)))

(:derived (Flag232-)(and (Flag183-)))

(:derived (Flag232-)(and (Flag213-)))

(:derived (Flag232-)(and (Flag205-)))

(:derived (Flag232-)(and (Flag184-)))

(:derived (Flag232-)(and (Flag160-)))

(:derived (Flag232-)(and (Flag176-)))

(:derived (Flag232-)(and (Flag222-)))

(:derived (Flag232-)(and (Flag188-)))

(:derived (Flag232-)(and (Flag192-)))

(:derived (Flag232-)(and (Flag207-)))

(:derived (Flag235-)(and (Flag209-)))

(:derived (Flag235-)(and (Flag233-)))

(:derived (Flag235-)(and (Flag229-)))

(:derived (Flag235-)(and (Flag230-)))

(:derived (Flag235-)(and (Flag147-)))

(:derived (Flag235-)(and (Flag153-)))

(:derived (Flag235-)(and (Flag202-)))

(:derived (Flag235-)(and (Flag226-)))

(:derived (Flag235-)(and (Flag219-)))

(:derived (Flag235-)(and (Flag197-)))

(:derived (Flag235-)(and (Flag156-)))

(:derived (Flag235-)(and (Flag171-)))

(:derived (Flag235-)(and (Flag193-)))

(:derived (Flag235-)(and (Flag227-)))

(:derived (Flag235-)(and (Flag175-)))

(:derived (Flag235-)(and (Flag205-)))

(:derived (Flag235-)(and (Flag160-)))

(:derived (Flag235-)(and (Flag215-)))

(:derived (Flag235-)(and (Flag234-)))

(:derived (Flag235-)(and (Flag222-)))

(:derived (Flag235-)(and (Flag188-)))

(:derived (Flag235-)(and (Flag192-)))

(:derived (Flag237-)(and (Flag160-)))

(:derived (Flag237-)(and (Flag209-)))

(:derived (Flag237-)(and (Flag233-)))

(:derived (Flag237-)(and (Flag227-)))

(:derived (Flag237-)(and (Flag229-)))

(:derived (Flag237-)(and (Flag222-)))

(:derived (Flag237-)(and (Flag153-)))

(:derived (Flag237-)(and (Flag175-)))

(:derived (Flag237-)(and (Flag202-)))

(:derived (Flag237-)(and (Flag188-)))

(:derived (Flag237-)(and (Flag192-)))

(:derived (Flag237-)(and (Flag236-)))

(:derived (Flag240-)(and (Flag155-)))

(:derived (Flag240-)(and (Flag157-)))

(:derived (Flag240-)(and (Flag145-)))

(:derived (Flag240-)(and (Flag150-)))

(:derived (Flag240-)(and (Flag146-)))

(:derived (Flag240-)(and (Flag239-)))

(:derived (Flag240-)(and (Flag158-)))

(:derived (Flag240-)(and (Flag163-)))

(:derived (Flag240-)(and (Flag153-)))

(:derived (Flag240-)(and (Flag159-)))

(:derived (Flag240-)(and (Flag164-)))

(:derived (Flag240-)(and (Flag147-)))

(:derived (Flag1-)(and (COLUMN2-ROBOT)(ROW1-ROBOT)(NOT-ERROR-)(NOT-CHECKCONSISTENCY-)))

(:derived (Flag3-)(and (LEFTOF3-ROBOT)))

(:derived (Flag3-)(and (RIGHTOF7-ROBOT)))

(:derived (Flag3-)(and (RIGHTOF8-ROBOT)))

(:derived (Flag3-)(and (LEFTOF2-ROBOT)))

(:derived (Flag3-)(and (LEFTOF9-ROBOT)))

(:derived (Flag3-)(and (RIGHTOF0-ROBOT)))

(:derived (Flag3-)(and (LEFTOF8-ROBOT)))

(:derived (Flag3-)(and (LEFTOF5-ROBOT)))

(:derived (Flag3-)(and (RIGHTOF1-ROBOT)))

(:derived (Flag3-)(and (LEFTOF12-ROBOT)))

(:derived (Flag3-)(and (RIGHTOF6-ROBOT)))

(:derived (Flag3-)(and (RIGHTOF9-ROBOT)))

(:derived (Flag3-)(and (LEFTOF6-ROBOT)))

(:derived (Flag3-)(and (RIGHTOF10-ROBOT)))

(:derived (Flag3-)(and (RIGHTOF11-ROBOT)))

(:derived (Flag3-)(and (RIGHTOF5-ROBOT)))

(:derived (Flag3-)(and (LEFTOF10-ROBOT)))

(:derived (Flag3-)(and (LEFTOF4-ROBOT)))

(:derived (Flag3-)(and (RIGHTOF4-ROBOT)))

(:derived (Flag3-)(and (LEFTOF1-ROBOT)))

(:derived (Flag3-)(and (LEFTOF7-ROBOT)))

(:derived (Flag3-)(and (RIGHTOF3-ROBOT)))

(:derived (Flag48-)(and (Flag26-)))

(:derived (Flag48-)(and (Flag27-)))

(:derived (Flag48-)(and (Flag28-)))

(:derived (Flag48-)(and (Flag29-)))

(:derived (Flag48-)(and (Flag30-)))

(:derived (Flag48-)(and (Flag31-)))

(:derived (Flag48-)(and (Flag32-)))

(:derived (Flag48-)(and (Flag33-)))

(:derived (Flag48-)(and (Flag34-)))

(:derived (Flag48-)(and (Flag35-)))

(:derived (Flag48-)(and (Flag36-)))

(:derived (Flag48-)(and (Flag37-)))

(:derived (Flag48-)(and (Flag38-)))

(:derived (Flag48-)(and (Flag39-)))

(:derived (Flag48-)(and (Flag40-)))

(:derived (Flag48-)(and (Flag41-)))

(:derived (Flag48-)(and (Flag42-)))

(:derived (Flag48-)(and (Flag43-)))

(:derived (Flag48-)(and (Flag44-)))

(:derived (Flag48-)(and (Flag45-)))

(:derived (Flag48-)(and (Flag46-)))

(:derived (Flag48-)(and (Flag47-)))

(:derived (Flag60-)(and (Flag49-)))

(:derived (Flag60-)(and (Flag50-)))

(:derived (Flag60-)(and (Flag35-)))

(:derived (Flag60-)(and (Flag51-)))

(:derived (Flag60-)(and (Flag27-)))

(:derived (Flag60-)(and (Flag39-)))

(:derived (Flag60-)(and (Flag29-)))

(:derived (Flag60-)(and (Flag30-)))

(:derived (Flag60-)(and (Flag32-)))

(:derived (Flag60-)(and (Flag52-)))

(:derived (Flag60-)(and (Flag33-)))

(:derived (Flag60-)(and (Flag53-)))

(:derived (Flag60-)(and (Flag34-)))

(:derived (Flag60-)(and (Flag54-)))

(:derived (Flag60-)(and (Flag36-)))

(:derived (Flag60-)(and (Flag55-)))

(:derived (Flag60-)(and (Flag37-)))

(:derived (Flag60-)(and (Flag56-)))

(:derived (Flag60-)(and (Flag38-)))

(:derived (Flag60-)(and (Flag57-)))

(:derived (Flag60-)(and (Flag58-)))

(:derived (Flag60-)(and (Flag40-)))

(:derived (Flag60-)(and (Flag41-)))

(:derived (Flag60-)(and (Flag42-)))

(:derived (Flag60-)(and (Flag43-)))

(:derived (Flag60-)(and (Flag44-)))

(:derived (Flag60-)(and (Flag59-)))

(:derived (Flag60-)(and (Flag45-)))

(:derived (Flag60-)(and (Flag46-)))

(:derived (Flag60-)(and (Flag47-)))

(:derived (Flag72-)(and (Flag49-)))

(:derived (Flag72-)(and (Flag61-)))

(:derived (Flag72-)(and (Flag62-)))

(:derived (Flag72-)(and (Flag50-)))

(:derived (Flag72-)(and (Flag51-)))

(:derived (Flag72-)(and (Flag27-)))

(:derived (Flag72-)(and (Flag39-)))

(:derived (Flag72-)(and (Flag29-)))

(:derived (Flag72-)(and (Flag52-)))

(:derived (Flag72-)(and (Flag30-)))

(:derived (Flag72-)(and (Flag63-)))

(:derived (Flag72-)(and (Flag32-)))

(:derived (Flag72-)(and (Flag64-)))

(:derived (Flag72-)(and (Flag65-)))

(:derived (Flag72-)(and (Flag66-)))

(:derived (Flag72-)(and (Flag33-)))

(:derived (Flag72-)(and (Flag67-)))

(:derived (Flag72-)(and (Flag68-)))

(:derived (Flag72-)(and (Flag34-)))

(:derived (Flag72-)(and (Flag36-)))

(:derived (Flag72-)(and (Flag55-)))

(:derived (Flag72-)(and (Flag37-)))

(:derived (Flag72-)(and (Flag56-)))

(:derived (Flag72-)(and (Flag38-)))

(:derived (Flag72-)(and (Flag57-)))

(:derived (Flag72-)(and (Flag69-)))

(:derived (Flag72-)(and (Flag40-)))

(:derived (Flag72-)(and (Flag41-)))

(:derived (Flag72-)(and (Flag42-)))

(:derived (Flag72-)(and (Flag43-)))

(:derived (Flag72-)(and (Flag44-)))

(:derived (Flag72-)(and (Flag70-)))

(:derived (Flag72-)(and (Flag71-)))

(:derived (Flag72-)(and (Flag59-)))

(:derived (Flag72-)(and (Flag45-)))

(:derived (Flag72-)(and (Flag46-)))

(:derived (Flag72-)(and (Flag47-)))

(:derived (Flag81-)(and (Flag73-)))

(:derived (Flag81-)(and (Flag49-)))

(:derived (Flag81-)(and (Flag66-)))

(:derived (Flag81-)(and (Flag50-)))

(:derived (Flag81-)(and (Flag51-)))

(:derived (Flag81-)(and (Flag27-)))

(:derived (Flag81-)(and (Flag29-)))

(:derived (Flag81-)(and (Flag52-)))

(:derived (Flag81-)(and (Flag74-)))

(:derived (Flag81-)(and (Flag30-)))

(:derived (Flag81-)(and (Flag45-)))

(:derived (Flag81-)(and (Flag63-)))

(:derived (Flag81-)(and (Flag32-)))

(:derived (Flag81-)(and (Flag64-)))

(:derived (Flag81-)(and (Flag65-)))

(:derived (Flag81-)(and (Flag75-)))

(:derived (Flag81-)(and (Flag76-)))

(:derived (Flag81-)(and (Flag33-)))

(:derived (Flag81-)(and (Flag67-)))

(:derived (Flag81-)(and (Flag34-)))

(:derived (Flag81-)(and (Flag36-)))

(:derived (Flag81-)(and (Flag55-)))

(:derived (Flag81-)(and (Flag37-)))

(:derived (Flag81-)(and (Flag77-)))

(:derived (Flag81-)(and (Flag56-)))

(:derived (Flag81-)(and (Flag78-)))

(:derived (Flag81-)(and (Flag38-)))

(:derived (Flag81-)(and (Flag39-)))

(:derived (Flag81-)(and (Flag69-)))

(:derived (Flag81-)(and (Flag62-)))

(:derived (Flag81-)(and (Flag41-)))

(:derived (Flag81-)(and (Flag42-)))

(:derived (Flag81-)(and (Flag43-)))

(:derived (Flag81-)(and (Flag44-)))

(:derived (Flag81-)(and (Flag79-)))

(:derived (Flag81-)(and (Flag70-)))

(:derived (Flag81-)(and (Flag71-)))

(:derived (Flag81-)(and (Flag59-)))

(:derived (Flag81-)(and (Flag80-)))

(:derived (Flag81-)(and (Flag46-)))

(:derived (Flag81-)(and (Flag57-)))

(:derived (Flag90-)(and (Flag49-)))

(:derived (Flag90-)(and (Flag82-)))

(:derived (Flag90-)(and (Flag50-)))

(:derived (Flag90-)(and (Flag51-)))

(:derived (Flag90-)(and (Flag27-)))

(:derived (Flag90-)(and (Flag29-)))

(:derived (Flag90-)(and (Flag83-)))

(:derived (Flag90-)(and (Flag74-)))

(:derived (Flag90-)(and (Flag41-)))

(:derived (Flag90-)(and (Flag36-)))

(:derived (Flag90-)(and (Flag30-)))

(:derived (Flag90-)(and (Flag45-)))

(:derived (Flag90-)(and (Flag63-)))

(:derived (Flag90-)(and (Flag32-)))

(:derived (Flag90-)(and (Flag84-)))

(:derived (Flag90-)(and (Flag85-)))

(:derived (Flag90-)(and (Flag52-)))

(:derived (Flag90-)(and (Flag65-)))

(:derived (Flag90-)(and (Flag75-)))

(:derived (Flag90-)(and (Flag66-)))

(:derived (Flag90-)(and (Flag67-)))

(:derived (Flag90-)(and (Flag34-)))

(:derived (Flag90-)(and (Flag37-)))

(:derived (Flag90-)(and (Flag55-)))

(:derived (Flag90-)(and (Flag86-)))

(:derived (Flag90-)(and (Flag78-)))

(:derived (Flag90-)(and (Flag38-)))

(:derived (Flag90-)(and (Flag39-)))

(:derived (Flag90-)(and (Flag69-)))

(:derived (Flag90-)(and (Flag87-)))

(:derived (Flag90-)(and (Flag62-)))

(:derived (Flag90-)(and (Flag88-)))

(:derived (Flag90-)(and (Flag42-)))

(:derived (Flag90-)(and (Flag43-)))

(:derived (Flag90-)(and (Flag79-)))

(:derived (Flag90-)(and (Flag70-)))

(:derived (Flag90-)(and (Flag71-)))

(:derived (Flag90-)(and (Flag89-)))

(:derived (Flag90-)(and (Flag76-)))

(:derived (Flag90-)(and (Flag59-)))

(:derived (Flag90-)(and (Flag80-)))

(:derived (Flag90-)(and (Flag46-)))

(:derived (Flag90-)(and (Flag57-)))

(:derived (Flag99-)(and (Flag63-)))

(:derived (Flag99-)(and (Flag91-)))

(:derived (Flag99-)(and (Flag82-)))

(:derived (Flag99-)(and (Flag50-)))

(:derived (Flag99-)(and (Flag89-)))

(:derived (Flag99-)(and (Flag51-)))

(:derived (Flag99-)(and (Flag92-)))

(:derived (Flag99-)(and (Flag27-)))

(:derived (Flag99-)(and (Flag93-)))

(:derived (Flag99-)(and (Flag29-)))

(:derived (Flag99-)(and (Flag74-)))

(:derived (Flag99-)(and (Flag41-)))

(:derived (Flag99-)(and (Flag30-)))

(:derived (Flag99-)(and (Flag94-)))

(:derived (Flag99-)(and (Flag84-)))

(:derived (Flag99-)(and (Flag52-)))

(:derived (Flag99-)(and (Flag65-)))

(:derived (Flag99-)(and (Flag75-)))

(:derived (Flag99-)(and (Flag66-)))

(:derived (Flag99-)(and (Flag95-)))

(:derived (Flag99-)(and (Flag34-)))

(:derived (Flag99-)(and (Flag36-)))

(:derived (Flag99-)(and (Flag55-)))

(:derived (Flag99-)(and (Flag37-)))

(:derived (Flag99-)(and (Flag78-)))

(:derived (Flag99-)(and (Flag38-)))

(:derived (Flag99-)(and (Flag39-)))

(:derived (Flag99-)(and (Flag69-)))

(:derived (Flag99-)(and (Flag87-)))

(:derived (Flag99-)(and (Flag62-)))

(:derived (Flag99-)(and (Flag88-)))

(:derived (Flag99-)(and (Flag42-)))

(:derived (Flag99-)(and (Flag43-)))

(:derived (Flag99-)(and (Flag79-)))

(:derived (Flag99-)(and (Flag70-)))

(:derived (Flag99-)(and (Flag96-)))

(:derived (Flag99-)(and (Flag97-)))

(:derived (Flag99-)(and (Flag98-)))

(:derived (Flag99-)(and (Flag71-)))

(:derived (Flag99-)(and (Flag76-)))

(:derived (Flag99-)(and (Flag59-)))

(:derived (Flag99-)(and (Flag80-)))

(:derived (Flag99-)(and (Flag46-)))

(:derived (Flag99-)(and (Flag57-)))

(:derived (Flag106-)(and (Flag63-)))

(:derived (Flag106-)(and (Flag82-)))

(:derived (Flag106-)(and (Flag89-)))

(:derived (Flag106-)(and (Flag51-)))

(:derived (Flag106-)(and (Flag92-)))

(:derived (Flag106-)(and (Flag27-)))

(:derived (Flag106-)(and (Flag39-)))

(:derived (Flag106-)(and (Flag29-)))

(:derived (Flag106-)(and (Flag74-)))

(:derived (Flag106-)(and (Flag41-)))

(:derived (Flag106-)(and (Flag30-)))

(:derived (Flag106-)(and (Flag94-)))

(:derived (Flag106-)(and (Flag84-)))

(:derived (Flag106-)(and (Flag100-)))

(:derived (Flag106-)(and (Flag52-)))

(:derived (Flag106-)(and (Flag65-)))

(:derived (Flag106-)(and (Flag75-)))

(:derived (Flag106-)(and (Flag66-)))

(:derived (Flag106-)(and (Flag95-)))

(:derived (Flag106-)(and (Flag76-)))

(:derived (Flag106-)(and (Flag101-)))

(:derived (Flag106-)(and (Flag36-)))

(:derived (Flag106-)(and (Flag55-)))

(:derived (Flag106-)(and (Flag78-)))

(:derived (Flag106-)(and (Flag102-)))

(:derived (Flag106-)(and (Flag38-)))

(:derived (Flag106-)(and (Flag57-)))

(:derived (Flag106-)(and (Flag69-)))

(:derived (Flag106-)(and (Flag87-)))

(:derived (Flag106-)(and (Flag88-)))

(:derived (Flag106-)(and (Flag42-)))

(:derived (Flag106-)(and (Flag43-)))

(:derived (Flag106-)(and (Flag103-)))

(:derived (Flag106-)(and (Flag79-)))

(:derived (Flag106-)(and (Flag70-)))

(:derived (Flag106-)(and (Flag71-)))

(:derived (Flag106-)(and (Flag97-)))

(:derived (Flag106-)(and (Flag98-)))

(:derived (Flag106-)(and (Flag104-)))

(:derived (Flag106-)(and (Flag59-)))

(:derived (Flag106-)(and (Flag46-)))

(:derived (Flag106-)(and (Flag105-)))

(:derived (Flag112-)(and (Flag63-)))

(:derived (Flag112-)(and (Flag97-)))

(:derived (Flag112-)(and (Flag82-)))

(:derived (Flag112-)(and (Flag89-)))

(:derived (Flag112-)(and (Flag107-)))

(:derived (Flag112-)(and (Flag92-)))

(:derived (Flag112-)(and (Flag27-)))

(:derived (Flag112-)(and (Flag39-)))

(:derived (Flag112-)(and (Flag108-)))

(:derived (Flag112-)(and (Flag74-)))

(:derived (Flag112-)(and (Flag41-)))

(:derived (Flag112-)(and (Flag94-)))

(:derived (Flag112-)(and (Flag100-)))

(:derived (Flag112-)(and (Flag52-)))

(:derived (Flag112-)(and (Flag65-)))

(:derived (Flag112-)(and (Flag75-)))

(:derived (Flag112-)(and (Flag76-)))

(:derived (Flag112-)(and (Flag95-)))

(:derived (Flag112-)(and (Flag36-)))

(:derived (Flag112-)(and (Flag55-)))

(:derived (Flag112-)(and (Flag102-)))

(:derived (Flag112-)(and (Flag38-)))

(:derived (Flag112-)(and (Flag57-)))

(:derived (Flag112-)(and (Flag69-)))

(:derived (Flag112-)(and (Flag87-)))

(:derived (Flag112-)(and (Flag88-)))

(:derived (Flag112-)(and (Flag42-)))

(:derived (Flag112-)(and (Flag43-)))

(:derived (Flag112-)(and (Flag79-)))

(:derived (Flag112-)(and (Flag70-)))

(:derived (Flag112-)(and (Flag109-)))

(:derived (Flag112-)(and (Flag71-)))

(:derived (Flag112-)(and (Flag110-)))

(:derived (Flag112-)(and (Flag98-)))

(:derived (Flag112-)(and (Flag59-)))

(:derived (Flag112-)(and (Flag111-)))

(:derived (Flag112-)(and (Flag46-)))

(:derived (Flag112-)(and (Flag105-)))

(:derived (Flag117-)(and (Flag63-)))

(:derived (Flag117-)(and (Flag82-)))

(:derived (Flag117-)(and (Flag98-)))

(:derived (Flag117-)(and (Flag107-)))

(:derived (Flag117-)(and (Flag92-)))

(:derived (Flag117-)(and (Flag27-)))

(:derived (Flag117-)(and (Flag113-)))

(:derived (Flag117-)(and (Flag41-)))

(:derived (Flag117-)(and (Flag39-)))

(:derived (Flag117-)(and (Flag94-)))

(:derived (Flag117-)(and (Flag100-)))

(:derived (Flag117-)(and (Flag52-)))

(:derived (Flag117-)(and (Flag75-)))

(:derived (Flag117-)(and (Flag76-)))

(:derived (Flag117-)(and (Flag95-)))

(:derived (Flag117-)(and (Flag114-)))

(:derived (Flag117-)(and (Flag36-)))

(:derived (Flag117-)(and (Flag102-)))

(:derived (Flag117-)(and (Flag38-)))

(:derived (Flag117-)(and (Flag57-)))

(:derived (Flag117-)(and (Flag69-)))

(:derived (Flag117-)(and (Flag88-)))

(:derived (Flag117-)(and (Flag42-)))

(:derived (Flag117-)(and (Flag115-)))

(:derived (Flag117-)(and (Flag116-)))

(:derived (Flag117-)(and (Flag79-)))

(:derived (Flag117-)(and (Flag70-)))

(:derived (Flag117-)(and (Flag71-)))

(:derived (Flag117-)(and (Flag110-)))

(:derived (Flag117-)(and (Flag89-)))

(:derived (Flag117-)(and (Flag59-)))

(:derived (Flag117-)(and (Flag105-)))

(:derived (Flag120-)(and (Flag57-)))

(:derived (Flag120-)(and (Flag82-)))

(:derived (Flag120-)(and (Flag107-)))

(:derived (Flag120-)(and (Flag118-)))

(:derived (Flag120-)(and (Flag27-)))

(:derived (Flag120-)(and (Flag119-)))

(:derived (Flag120-)(and (Flag94-)))

(:derived (Flag120-)(and (Flag100-)))

(:derived (Flag120-)(and (Flag75-)))

(:derived (Flag120-)(and (Flag76-)))

(:derived (Flag120-)(and (Flag95-)))

(:derived (Flag120-)(and (Flag114-)))

(:derived (Flag120-)(and (Flag38-)))

(:derived (Flag120-)(and (Flag39-)))

(:derived (Flag120-)(and (Flag69-)))

(:derived (Flag120-)(and (Flag42-)))

(:derived (Flag120-)(and (Flag116-)))

(:derived (Flag120-)(and (Flag70-)))

(:derived (Flag120-)(and (Flag71-)))

(:derived (Flag120-)(and (Flag110-)))

(:derived (Flag120-)(and (Flag98-)))

(:derived (Flag120-)(and (Flag59-)))

(:derived (Flag120-)(and (Flag105-)))

(:derived (Flag122-)(and (Flag116-)))

(:derived (Flag122-)(and (Flag70-)))

(:derived (Flag122-)(and (Flag71-)))

(:derived (Flag122-)(and (Flag121-)))

(:derived (Flag122-)(and (Flag110-)))

(:derived (Flag122-)(and (Flag98-)))

(:derived (Flag122-)(and (Flag82-)))

(:derived (Flag122-)(and (Flag39-)))

(:derived (Flag122-)(and (Flag76-)))

(:derived (Flag122-)(and (Flag27-)))

(:derived (Flag122-)(and (Flag105-)))

(:derived (Flag122-)(and (Flag119-)))

(:derived (Flag123-)(and (LEFTOF1-ROBOT)))

(:derived (Flag123-)(and (LEFTOF2-ROBOT)))

(:derived (Flag124-)(and (LEFTOF1-ROBOT)))

(:derived (Flag124-)(and (LEFTOF3-ROBOT)))

(:derived (Flag124-)(and (LEFTOF2-ROBOT)))

(:derived (Flag125-)(and (LEFTOF1-ROBOT)))

(:derived (Flag125-)(and (LEFTOF3-ROBOT)))

(:derived (Flag125-)(and (LEFTOF4-ROBOT)))

(:derived (Flag125-)(and (LEFTOF2-ROBOT)))

(:derived (Flag126-)(and (LEFTOF5-ROBOT)))

(:derived (Flag126-)(and (LEFTOF1-ROBOT)))

(:derived (Flag126-)(and (LEFTOF3-ROBOT)))

(:derived (Flag126-)(and (LEFTOF4-ROBOT)))

(:derived (Flag126-)(and (LEFTOF2-ROBOT)))

(:derived (Flag127-)(and (LEFTOF2-ROBOT)))

(:derived (Flag127-)(and (LEFTOF1-ROBOT)))

(:derived (Flag127-)(and (LEFTOF6-ROBOT)))

(:derived (Flag127-)(and (LEFTOF5-ROBOT)))

(:derived (Flag127-)(and (LEFTOF3-ROBOT)))

(:derived (Flag127-)(and (LEFTOF4-ROBOT)))

(:derived (Flag128-)(and (LEFTOF2-ROBOT)))

(:derived (Flag128-)(and (LEFTOF7-ROBOT)))

(:derived (Flag128-)(and (LEFTOF1-ROBOT)))

(:derived (Flag128-)(and (LEFTOF6-ROBOT)))

(:derived (Flag128-)(and (LEFTOF5-ROBOT)))

(:derived (Flag128-)(and (LEFTOF3-ROBOT)))

(:derived (Flag128-)(and (LEFTOF4-ROBOT)))

(:derived (Flag129-)(and (LEFTOF2-ROBOT)))

(:derived (Flag129-)(and (LEFTOF7-ROBOT)))

(:derived (Flag129-)(and (LEFTOF1-ROBOT)))

(:derived (Flag129-)(and (LEFTOF6-ROBOT)))

(:derived (Flag129-)(and (LEFTOF5-ROBOT)))

(:derived (Flag129-)(and (LEFTOF3-ROBOT)))

(:derived (Flag129-)(and (LEFTOF4-ROBOT)))

(:derived (Flag129-)(and (LEFTOF8-ROBOT)))

(:derived (Flag130-)(and (LEFTOF9-ROBOT)))

(:derived (Flag130-)(and (LEFTOF2-ROBOT)))

(:derived (Flag130-)(and (LEFTOF7-ROBOT)))

(:derived (Flag130-)(and (LEFTOF1-ROBOT)))

(:derived (Flag130-)(and (LEFTOF6-ROBOT)))

(:derived (Flag130-)(and (LEFTOF5-ROBOT)))

(:derived (Flag130-)(and (LEFTOF3-ROBOT)))

(:derived (Flag130-)(and (LEFTOF4-ROBOT)))

(:derived (Flag130-)(and (LEFTOF8-ROBOT)))

(:derived (Flag131-)(and (LEFTOF10-ROBOT)))

(:derived (Flag131-)(and (LEFTOF9-ROBOT)))

(:derived (Flag131-)(and (LEFTOF2-ROBOT)))

(:derived (Flag131-)(and (LEFTOF7-ROBOT)))

(:derived (Flag131-)(and (LEFTOF1-ROBOT)))

(:derived (Flag131-)(and (LEFTOF6-ROBOT)))

(:derived (Flag131-)(and (LEFTOF5-ROBOT)))

(:derived (Flag131-)(and (LEFTOF3-ROBOT)))

(:derived (Flag131-)(and (LEFTOF4-ROBOT)))

(:derived (Flag131-)(and (LEFTOF8-ROBOT)))

(:derived (Flag132-)(and (LEFTOF10-ROBOT)))

(:derived (Flag132-)(and (LEFTOF9-ROBOT)))

(:derived (Flag132-)(and (LEFTOF2-ROBOT)))

(:derived (Flag132-)(and (LEFTOF7-ROBOT)))

(:derived (Flag132-)(and (LEFTOF1-ROBOT)))

(:derived (Flag132-)(and (LEFTOF6-ROBOT)))

(:derived (Flag132-)(and (LEFTOF5-ROBOT)))

(:derived (Flag132-)(and (LEFTOF3-ROBOT)))

(:derived (Flag132-)(and (LEFTOF4-ROBOT)))

(:derived (Flag132-)(and (LEFTOF8-ROBOT)))

(:derived (Flag133-)(and (LEFTOF10-ROBOT)))

(:derived (Flag133-)(and (LEFTOF9-ROBOT)))

(:derived (Flag133-)(and (LEFTOF2-ROBOT)))

(:derived (Flag133-)(and (LEFTOF7-ROBOT)))

(:derived (Flag133-)(and (LEFTOF1-ROBOT)))

(:derived (Flag133-)(and (LEFTOF12-ROBOT)))

(:derived (Flag133-)(and (LEFTOF6-ROBOT)))

(:derived (Flag133-)(and (LEFTOF5-ROBOT)))

(:derived (Flag133-)(and (LEFTOF3-ROBOT)))

(:derived (Flag133-)(and (LEFTOF4-ROBOT)))

(:derived (Flag133-)(and (LEFTOF8-ROBOT)))

(:derived (Flag134-)(and (RIGHTOF3-ROBOT)))

(:derived (Flag134-)(and (RIGHTOF8-ROBOT)))

(:derived (Flag134-)(and (RIGHTOF5-ROBOT)))

(:derived (Flag134-)(and (RIGHTOF11-ROBOT)))

(:derived (Flag134-)(and (RIGHTOF10-ROBOT)))

(:derived (Flag134-)(and (RIGHTOF4-ROBOT)))

(:derived (Flag134-)(and (RIGHTOF1-ROBOT)))

(:derived (Flag134-)(and (RIGHTOF7-ROBOT)))

(:derived (Flag134-)(and (RIGHTOF9-ROBOT)))

(:derived (Flag134-)(and (RIGHTOF6-ROBOT)))

(:derived (Flag135-)(and (RIGHTOF3-ROBOT)))

(:derived (Flag135-)(and (RIGHTOF5-ROBOT)))

(:derived (Flag135-)(and (RIGHTOF11-ROBOT)))

(:derived (Flag135-)(and (RIGHTOF10-ROBOT)))

(:derived (Flag135-)(and (RIGHTOF4-ROBOT)))

(:derived (Flag135-)(and (RIGHTOF8-ROBOT)))

(:derived (Flag135-)(and (RIGHTOF7-ROBOT)))

(:derived (Flag135-)(and (RIGHTOF9-ROBOT)))

(:derived (Flag135-)(and (RIGHTOF6-ROBOT)))

(:derived (Flag136-)(and (RIGHTOF3-ROBOT)))

(:derived (Flag136-)(and (RIGHTOF5-ROBOT)))

(:derived (Flag136-)(and (RIGHTOF11-ROBOT)))

(:derived (Flag136-)(and (RIGHTOF10-ROBOT)))

(:derived (Flag136-)(and (RIGHTOF4-ROBOT)))

(:derived (Flag136-)(and (RIGHTOF8-ROBOT)))

(:derived (Flag136-)(and (RIGHTOF7-ROBOT)))

(:derived (Flag136-)(and (RIGHTOF9-ROBOT)))

(:derived (Flag136-)(and (RIGHTOF6-ROBOT)))

(:derived (Flag137-)(and (RIGHTOF5-ROBOT)))

(:derived (Flag137-)(and (RIGHTOF11-ROBOT)))

(:derived (Flag137-)(and (RIGHTOF10-ROBOT)))

(:derived (Flag137-)(and (RIGHTOF4-ROBOT)))

(:derived (Flag137-)(and (RIGHTOF8-ROBOT)))

(:derived (Flag137-)(and (RIGHTOF7-ROBOT)))

(:derived (Flag137-)(and (RIGHTOF9-ROBOT)))

(:derived (Flag137-)(and (RIGHTOF6-ROBOT)))

(:derived (Flag138-)(and (RIGHTOF5-ROBOT)))

(:derived (Flag138-)(and (RIGHTOF11-ROBOT)))

(:derived (Flag138-)(and (RIGHTOF10-ROBOT)))

(:derived (Flag138-)(and (RIGHTOF8-ROBOT)))

(:derived (Flag138-)(and (RIGHTOF7-ROBOT)))

(:derived (Flag138-)(and (RIGHTOF9-ROBOT)))

(:derived (Flag138-)(and (RIGHTOF6-ROBOT)))

(:derived (Flag139-)(and (RIGHTOF11-ROBOT)))

(:derived (Flag139-)(and (RIGHTOF10-ROBOT)))

(:derived (Flag139-)(and (RIGHTOF8-ROBOT)))

(:derived (Flag139-)(and (RIGHTOF7-ROBOT)))

(:derived (Flag139-)(and (RIGHTOF9-ROBOT)))

(:derived (Flag139-)(and (RIGHTOF6-ROBOT)))

(:derived (Flag140-)(and (RIGHTOF10-ROBOT)))

(:derived (Flag140-)(and (RIGHTOF7-ROBOT)))

(:derived (Flag140-)(and (RIGHTOF9-ROBOT)))

(:derived (Flag140-)(and (RIGHTOF8-ROBOT)))

(:derived (Flag140-)(and (RIGHTOF11-ROBOT)))

(:derived (Flag141-)(and (RIGHTOF10-ROBOT)))

(:derived (Flag141-)(and (RIGHTOF9-ROBOT)))

(:derived (Flag141-)(and (RIGHTOF8-ROBOT)))

(:derived (Flag141-)(and (RIGHTOF11-ROBOT)))

(:derived (Flag142-)(and (RIGHTOF10-ROBOT)))

(:derived (Flag142-)(and (RIGHTOF9-ROBOT)))

(:derived (Flag142-)(and (RIGHTOF11-ROBOT)))

(:derived (Flag143-)(and (RIGHTOF10-ROBOT)))

(:derived (Flag143-)(and (RIGHTOF11-ROBOT)))

(:derived (Flag144-)(and (LEFTOF2-ROBOT)(RIGHTOF9-ROBOT)))

(:derived (Flag145-)(and (LEFTOF1-ROBOT)(RIGHTOF9-ROBOT)))

(:derived (Flag146-)(and (RIGHTOF3-ROBOT)(LEFTOF1-ROBOT)))

(:derived (Flag147-)(and (RIGHTOF10-ROBOT)(LEFTOF1-ROBOT)))

(:derived (Flag148-)(and (LEFTOF2-ROBOT)(RIGHTOF1-ROBOT)))

(:derived (Flag149-)(and (RIGHTOF5-ROBOT)(LEFTOF2-ROBOT)))

(:derived (Flag150-)(and (LEFTOF1-ROBOT)(RIGHTOF1-ROBOT)))

(:derived (Flag151-)(and (LEFTOF2-ROBOT)(RIGHTOF7-ROBOT)))

(:derived (Flag152-)(and (RIGHTOF5-ROBOT)(LEFTOF1-ROBOT)))

(:derived (Flag153-)(and (RIGHTOF11-ROBOT)(LEFTOF1-ROBOT)))

(:derived (Flag154-)(and (RIGHTOF3-ROBOT)(LEFTOF2-ROBOT)))

(:derived (Flag155-)(and (LEFTOF1-ROBOT)(RIGHTOF4-ROBOT)))

(:derived (Flag156-)(and (RIGHTOF10-ROBOT)(LEFTOF2-ROBOT)))

(:derived (Flag157-)(and (LEFTOF1-ROBOT)(RIGHTOF7-ROBOT)))

(:derived (Flag158-)(and (LEFTOF1-ROBOT)(RIGHTOF5-ROBOT)))

(:derived (Flag159-)(and (RIGHTOF8-ROBOT)(LEFTOF1-ROBOT)))

(:derived (Flag160-)(and (RIGHTOF11-ROBOT)(LEFTOF2-ROBOT)))

(:derived (Flag161-)(and (RIGHTOF8-ROBOT)(LEFTOF2-ROBOT)))

(:derived (Flag162-)(and (RIGHTOF2-ROBOT)(LEFTOF2-ROBOT)))

(:derived (Flag163-)(and (LEFTOF1-ROBOT)(RIGHTOF2-ROBOT)))

(:derived (Flag164-)(and (LEFTOF1-ROBOT)(RIGHTOF6-ROBOT)))

(:derived (Flag165-)(and (LEFTOF2-ROBOT)(RIGHTOF6-ROBOT)))

(:derived (Flag166-)(and (LEFTOF2-ROBOT)(RIGHTOF4-ROBOT)))

(:derived (Flag168-)(and (LEFTOF3-ROBOT)(RIGHTOF6-ROBOT)))

(:derived (Flag169-)(and (LEFTOF3-ROBOT)(RIGHTOF4-ROBOT)))

(:derived (Flag170-)(and (LEFTOF3-ROBOT)(RIGHTOF7-ROBOT)))

(:derived (Flag171-)(and (LEFTOF3-ROBOT)(RIGHTOF10-ROBOT)))

(:derived (Flag172-)(and (LEFTOF3-ROBOT)(RIGHTOF8-ROBOT)))

(:derived (Flag173-)(and (LEFTOF3-ROBOT)(RIGHTOF5-ROBOT)))

(:derived (Flag174-)(and (RIGHTOF3-ROBOT)(LEFTOF3-ROBOT)))

(:derived (Flag175-)(and (LEFTOF3-ROBOT)(RIGHTOF11-ROBOT)))

(:derived (Flag176-)(and (LEFTOF3-ROBOT)(RIGHTOF9-ROBOT)))

(:derived (Flag177-)(and (LEFTOF3-ROBOT)(RIGHTOF2-ROBOT)))

(:derived (Flag179-)(and (LEFTOF4-ROBOT)(RIGHTOF4-ROBOT)))

(:derived (Flag180-)(and (LEFTOF4-ROBOT)(RIGHTOF7-ROBOT)))

(:derived (Flag181-)(and (LEFTOF3-ROBOT)(RIGHTOF3-ROBOT)))

(:derived (Flag182-)(and (RIGHTOF8-ROBOT)(LEFTOF4-ROBOT)))

(:derived (Flag183-)(and (RIGHTOF10-ROBOT)(LEFTOF4-ROBOT)))

(:derived (Flag184-)(and (LEFTOF4-ROBOT)(RIGHTOF9-ROBOT)))

(:derived (Flag185-)(and (LEFTOF4-ROBOT)(RIGHTOF6-ROBOT)))

(:derived (Flag186-)(and (LEFTOF4-ROBOT)(RIGHTOF5-ROBOT)))

(:derived (Flag187-)(and (RIGHTOF3-ROBOT)(LEFTOF4-ROBOT)))

(:derived (Flag188-)(and (LEFTOF4-ROBOT)(RIGHTOF11-ROBOT)))

(:derived (Flag190-)(and (RIGHTOF9-ROBOT)(LEFTOF5-ROBOT)))

(:derived (Flag191-)(and (RIGHTOF7-ROBOT)(LEFTOF5-ROBOT)))

(:derived (Flag192-)(and (RIGHTOF11-ROBOT)(LEFTOF5-ROBOT)))

(:derived (Flag193-)(and (LEFTOF4-ROBOT)(RIGHTOF10-ROBOT)))

(:derived (Flag194-)(and (RIGHTOF4-ROBOT)(LEFTOF5-ROBOT)))

(:derived (Flag195-)(and (LEFTOF4-ROBOT)(RIGHTOF8-ROBOT)))

(:derived (Flag196-)(and (LEFTOF5-ROBOT)(RIGHTOF6-ROBOT)))

(:derived (Flag197-)(and (RIGHTOF10-ROBOT)(LEFTOF5-ROBOT)))

(:derived (Flag198-)(and (RIGHTOF8-ROBOT)(LEFTOF5-ROBOT)))

(:derived (Flag199-)(and (RIGHTOF5-ROBOT)(LEFTOF5-ROBOT)))

(:derived (Flag201-)(and (RIGHTOF8-ROBOT)(LEFTOF6-ROBOT)))

(:derived (Flag202-)(and (RIGHTOF11-ROBOT)(LEFTOF6-ROBOT)))

(:derived (Flag203-)(and (RIGHTOF5-ROBOT)(LEFTOF6-ROBOT)))

(:derived (Flag204-)(and (RIGHTOF7-ROBOT)(LEFTOF6-ROBOT)))

(:derived (Flag205-)(and (RIGHTOF10-ROBOT)(LEFTOF6-ROBOT)))

(:derived (Flag206-)(and (RIGHTOF6-ROBOT)(LEFTOF6-ROBOT)))

(:derived (Flag207-)(and (RIGHTOF9-ROBOT)(LEFTOF6-ROBOT)))

(:derived (Flag209-)(and (RIGHTOF11-ROBOT)(LEFTOF7-ROBOT)))

(:derived (Flag210-)(and (LEFTOF7-ROBOT)(RIGHTOF7-ROBOT)))

(:derived (Flag211-)(and (RIGHTOF8-ROBOT)(LEFTOF7-ROBOT)))

(:derived (Flag212-)(and (LEFTOF7-ROBOT)(RIGHTOF6-ROBOT)))

(:derived (Flag213-)(and (LEFTOF7-ROBOT)(RIGHTOF9-ROBOT)))

(:derived (Flag214-)(and (LEFTOF6-ROBOT)(RIGHTOF6-ROBOT)))

(:derived (Flag215-)(and (RIGHTOF10-ROBOT)(LEFTOF7-ROBOT)))

(:derived (Flag217-)(and (RIGHTOF7-ROBOT)(LEFTOF8-ROBOT)))

(:derived (Flag218-)(and (LEFTOF8-ROBOT)(RIGHTOF9-ROBOT)))

(:derived (Flag219-)(and (RIGHTOF10-ROBOT)(LEFTOF8-ROBOT)))

(:derived (Flag220-)(and (RIGHTOF8-ROBOT)(LEFTOF8-ROBOT)))

(:derived (Flag221-)(and (RIGHTOF7-ROBOT)(LEFTOF7-ROBOT)))

(:derived (Flag222-)(and (RIGHTOF11-ROBOT)(LEFTOF8-ROBOT)))

(:derived (Flag224-)(and (RIGHTOF8-ROBOT)(LEFTOF9-ROBOT)))

(:derived (Flag225-)(and (LEFTOF9-ROBOT)(RIGHTOF9-ROBOT)))

(:derived (Flag226-)(and (RIGHTOF10-ROBOT)(LEFTOF9-ROBOT)))

(:derived (Flag227-)(and (RIGHTOF11-ROBOT)(LEFTOF9-ROBOT)))

(:derived (Flag229-)(and (RIGHTOF11-ROBOT)(LEFTOF10-ROBOT)))

(:derived (Flag230-)(and (RIGHTOF10-ROBOT)(LEFTOF10-ROBOT)))

(:derived (Flag231-)(and (LEFTOF10-ROBOT)(RIGHTOF9-ROBOT)))

(:derived (Flag233-)(and (RIGHTOF11-ROBOT)(LEFTOF11-ROBOT)))

(:derived (Flag234-)(and (RIGHTOF10-ROBOT)(LEFTOF11-ROBOT)))

(:derived (Flag236-)(and (RIGHTOF11-ROBOT)(LEFTOF12-ROBOT)))

(:derived (Flag238-)(and (RIGHTOF3-ROBOT)))

(:derived (Flag238-)(and (RIGHTOF8-ROBOT)))

(:derived (Flag238-)(and (RIGHTOF10-ROBOT)))

(:derived (Flag238-)(and (RIGHTOF5-ROBOT)))

(:derived (Flag238-)(and (RIGHTOF11-ROBOT)))

(:derived (Flag238-)(and (RIGHTOF0-ROBOT)))

(:derived (Flag238-)(and (RIGHTOF4-ROBOT)))

(:derived (Flag238-)(and (RIGHTOF1-ROBOT)))

(:derived (Flag238-)(and (RIGHTOF7-ROBOT)))

(:derived (Flag238-)(and (RIGHTOF9-ROBOT)))

(:derived (Flag238-)(and (RIGHTOF6-ROBOT)))

(:derived (Flag239-)(and (RIGHTOF0-ROBOT)(LEFTOF1-ROBOT)))

(:derived (Flag241-)(and (Flag44-)))

(:derived (Flag241-)(and (Flag35-)))

(:derived (Flag241-)(and (Flag31-)))

(:derived (Flag241-)(and (Flag30-)))

(:derived (Flag241-)(and (Flag27-)))

(:derived (Flag241-)(and (Flag37-)))

(:derived (Flag241-)(and (Flag32-)))

(:derived (Flag241-)(and (Flag38-)))

(:derived (Flag241-)(and (Flag46-)))

(:derived (Flag241-)(and (Flag40-)))

(:derived (Flag241-)(and (Flag41-)))

(:action MOVERIGHT-ROBOT
:parameters ()
:precondition
(and
(Flag4-)
)
:effect
(and
(when
(and
(Flag235-)
)
(and
(COLUMN11-ROBOT)
(not (NOT-COLUMN11-ROBOT))
)
)
(when
(and
(Flag232-)
)
(and
(COLUMN10-ROBOT)
(not (NOT-COLUMN10-ROBOT))
)
)
(when
(and
(Flag228-)
)
(and
(COLUMN9-ROBOT)
(not (NOT-COLUMN9-ROBOT))
)
)
(when
(and
(Flag223-)
)
(and
(COLUMN8-ROBOT)
(not (NOT-COLUMN8-ROBOT))
)
)
(when
(and
(Flag216-)
)
(and
(COLUMN7-ROBOT)
(not (NOT-COLUMN7-ROBOT))
)
)
(when
(and
(Flag208-)
)
(and
(COLUMN6-ROBOT)
(not (NOT-COLUMN6-ROBOT))
)
)
(when
(and
(Flag200-)
)
(and
(COLUMN5-ROBOT)
(not (NOT-COLUMN5-ROBOT))
)
)
(when
(and
(Flag189-)
)
(and
(COLUMN4-ROBOT)
(not (NOT-COLUMN4-ROBOT))
)
)
(when
(and
(Flag178-)
)
(and
(COLUMN3-ROBOT)
(not (NOT-COLUMN3-ROBOT))
)
)
(when
(and
(Flag167-)
)
(and
(COLUMN2-ROBOT)
(not (NOT-COLUMN2-ROBOT))
)
)
(when
(and
(Flag240-)
)
(and
(COLUMN1-ROBOT)
(not (NOT-COLUMN1-ROBOT))
)
)
(when
(and
(COLUMN10-ROBOT)
)
(and
(COLUMN11-ROBOT)
(NOT-COLUMN10-ROBOT)
(not (NOT-COLUMN11-ROBOT))
(not (COLUMN10-ROBOT))
)
)
(when
(and
(COLUMN9-ROBOT)
)
(and
(COLUMN10-ROBOT)
(NOT-COLUMN9-ROBOT)
(not (NOT-COLUMN10-ROBOT))
(not (COLUMN9-ROBOT))
)
)
(when
(and
(COLUMN8-ROBOT)
)
(and
(COLUMN9-ROBOT)
(NOT-COLUMN8-ROBOT)
(not (NOT-COLUMN9-ROBOT))
(not (COLUMN8-ROBOT))
)
)
(when
(and
(COLUMN7-ROBOT)
)
(and
(COLUMN8-ROBOT)
(NOT-COLUMN7-ROBOT)
(not (NOT-COLUMN8-ROBOT))
(not (COLUMN7-ROBOT))
)
)
(when
(and
(COLUMN6-ROBOT)
)
(and
(COLUMN7-ROBOT)
(NOT-COLUMN6-ROBOT)
(not (NOT-COLUMN7-ROBOT))
(not (COLUMN6-ROBOT))
)
)
(when
(and
(COLUMN5-ROBOT)
)
(and
(COLUMN6-ROBOT)
(NOT-COLUMN5-ROBOT)
(not (NOT-COLUMN6-ROBOT))
(not (COLUMN5-ROBOT))
)
)
(when
(and
(COLUMN4-ROBOT)
)
(and
(COLUMN5-ROBOT)
(NOT-COLUMN4-ROBOT)
(not (NOT-COLUMN5-ROBOT))
(not (COLUMN4-ROBOT))
)
)
(when
(and
(COLUMN3-ROBOT)
)
(and
(COLUMN4-ROBOT)
(NOT-COLUMN3-ROBOT)
(not (NOT-COLUMN4-ROBOT))
(not (COLUMN3-ROBOT))
)
)
(when
(and
(COLUMN2-ROBOT)
)
(and
(COLUMN3-ROBOT)
(NOT-COLUMN2-ROBOT)
(not (NOT-COLUMN3-ROBOT))
(not (COLUMN2-ROBOT))
)
)
(when
(and
(COLUMN1-ROBOT)
)
(and
(COLUMN2-ROBOT)
(NOT-COLUMN1-ROBOT)
(not (NOT-COLUMN2-ROBOT))
(not (COLUMN1-ROBOT))
)
)
(when
(and
(COLUMN0-ROBOT)
)
(and
(COLUMN1-ROBOT)
(NOT-COLUMN0-ROBOT)
(not (NOT-COLUMN1-ROBOT))
(not (COLUMN0-ROBOT))
)
)
(when
(and
(LEFTOF8-ROBOT)
)
(and
(LEFTOF12-ROBOT)
(NOT-LEFTOF11-ROBOT)
(LEFTOF11-ROBOT)
(NOT-LEFTOF10-ROBOT)
(LEFTOF10-ROBOT)
(NOT-LEFTOF9-ROBOT)
(LEFTOF9-ROBOT)
(NOT-LEFTOF8-ROBOT)
(not (NOT-LEFTOF12-ROBOT))
(not (LEFTOF8-ROBOT))
)
)
(when
(and
(LEFTOF4-ROBOT)
)
(and
(LEFTOF12-ROBOT)
(NOT-LEFTOF11-ROBOT)
(LEFTOF11-ROBOT)
(NOT-LEFTOF10-ROBOT)
(LEFTOF10-ROBOT)
(NOT-LEFTOF9-ROBOT)
(LEFTOF9-ROBOT)
(NOT-LEFTOF8-ROBOT)
(LEFTOF8-ROBOT)
(NOT-LEFTOF7-ROBOT)
(LEFTOF7-ROBOT)
(NOT-LEFTOF6-ROBOT)
(LEFTOF6-ROBOT)
(NOT-LEFTOF5-ROBOT)
(LEFTOF5-ROBOT)
(NOT-LEFTOF4-ROBOT)
(not (NOT-LEFTOF12-ROBOT))
(not (LEFTOF4-ROBOT))
)
)
(when
(and
(LEFTOF3-ROBOT)
)
(and
(LEFTOF12-ROBOT)
(NOT-LEFTOF11-ROBOT)
(LEFTOF11-ROBOT)
(NOT-LEFTOF10-ROBOT)
(LEFTOF10-ROBOT)
(NOT-LEFTOF9-ROBOT)
(LEFTOF9-ROBOT)
(NOT-LEFTOF8-ROBOT)
(LEFTOF8-ROBOT)
(NOT-LEFTOF7-ROBOT)
(LEFTOF7-ROBOT)
(NOT-LEFTOF6-ROBOT)
(LEFTOF6-ROBOT)
(NOT-LEFTOF5-ROBOT)
(LEFTOF5-ROBOT)
(NOT-LEFTOF4-ROBOT)
(LEFTOF4-ROBOT)
(NOT-LEFTOF3-ROBOT)
(not (NOT-LEFTOF12-ROBOT))
(not (LEFTOF3-ROBOT))
)
)
(when
(and
(LEFTOF5-ROBOT)
)
(and
(LEFTOF12-ROBOT)
(NOT-LEFTOF11-ROBOT)
(LEFTOF11-ROBOT)
(NOT-LEFTOF10-ROBOT)
(LEFTOF10-ROBOT)
(NOT-LEFTOF9-ROBOT)
(LEFTOF9-ROBOT)
(NOT-LEFTOF8-ROBOT)
(LEFTOF8-ROBOT)
(NOT-LEFTOF7-ROBOT)
(LEFTOF7-ROBOT)
(NOT-LEFTOF6-ROBOT)
(LEFTOF6-ROBOT)
(NOT-LEFTOF5-ROBOT)
(not (NOT-LEFTOF12-ROBOT))
(not (LEFTOF5-ROBOT))
)
)
(when
(and
(LEFTOF6-ROBOT)
)
(and
(LEFTOF12-ROBOT)
(NOT-LEFTOF11-ROBOT)
(LEFTOF11-ROBOT)
(NOT-LEFTOF10-ROBOT)
(LEFTOF10-ROBOT)
(NOT-LEFTOF9-ROBOT)
(LEFTOF9-ROBOT)
(NOT-LEFTOF8-ROBOT)
(LEFTOF8-ROBOT)
(NOT-LEFTOF7-ROBOT)
(LEFTOF7-ROBOT)
(NOT-LEFTOF6-ROBOT)
(not (NOT-LEFTOF12-ROBOT))
(not (LEFTOF6-ROBOT))
)
)
(when
(and
(LEFTOF1-ROBOT)
)
(and
(LEFTOF12-ROBOT)
(NOT-LEFTOF11-ROBOT)
(LEFTOF11-ROBOT)
(NOT-LEFTOF10-ROBOT)
(LEFTOF10-ROBOT)
(NOT-LEFTOF9-ROBOT)
(LEFTOF9-ROBOT)
(NOT-LEFTOF8-ROBOT)
(LEFTOF8-ROBOT)
(NOT-LEFTOF7-ROBOT)
(LEFTOF7-ROBOT)
(NOT-LEFTOF6-ROBOT)
(LEFTOF6-ROBOT)
(NOT-LEFTOF5-ROBOT)
(LEFTOF5-ROBOT)
(NOT-LEFTOF4-ROBOT)
(LEFTOF4-ROBOT)
(NOT-LEFTOF3-ROBOT)
(LEFTOF3-ROBOT)
(NOT-LEFTOF2-ROBOT)
(LEFTOF2-ROBOT)
(NOT-LEFTOF1-ROBOT)
(not (NOT-LEFTOF12-ROBOT))
(not (LEFTOF1-ROBOT))
)
)
(when
(and
(LEFTOF7-ROBOT)
)
(and
(LEFTOF12-ROBOT)
(NOT-LEFTOF11-ROBOT)
(LEFTOF11-ROBOT)
(NOT-LEFTOF10-ROBOT)
(LEFTOF10-ROBOT)
(NOT-LEFTOF9-ROBOT)
(LEFTOF9-ROBOT)
(NOT-LEFTOF8-ROBOT)
(LEFTOF8-ROBOT)
(NOT-LEFTOF7-ROBOT)
(not (NOT-LEFTOF12-ROBOT))
(not (LEFTOF7-ROBOT))
)
)
(when
(and
(LEFTOF2-ROBOT)
)
(and
(LEFTOF12-ROBOT)
(NOT-LEFTOF11-ROBOT)
(LEFTOF11-ROBOT)
(NOT-LEFTOF10-ROBOT)
(LEFTOF10-ROBOT)
(NOT-LEFTOF9-ROBOT)
(LEFTOF9-ROBOT)
(NOT-LEFTOF8-ROBOT)
(LEFTOF8-ROBOT)
(NOT-LEFTOF7-ROBOT)
(LEFTOF7-ROBOT)
(NOT-LEFTOF6-ROBOT)
(LEFTOF6-ROBOT)
(NOT-LEFTOF5-ROBOT)
(LEFTOF5-ROBOT)
(NOT-LEFTOF4-ROBOT)
(LEFTOF4-ROBOT)
(NOT-LEFTOF3-ROBOT)
(LEFTOF3-ROBOT)
(NOT-LEFTOF2-ROBOT)
(not (NOT-LEFTOF12-ROBOT))
(not (LEFTOF2-ROBOT))
)
)
(when
(and
(LEFTOF9-ROBOT)
)
(and
(LEFTOF12-ROBOT)
(NOT-LEFTOF11-ROBOT)
(LEFTOF11-ROBOT)
(NOT-LEFTOF10-ROBOT)
(LEFTOF10-ROBOT)
(NOT-LEFTOF9-ROBOT)
(not (NOT-LEFTOF12-ROBOT))
(not (LEFTOF9-ROBOT))
)
)
(when
(and
(LEFTOF11-ROBOT)
)
(and
(LEFTOF12-ROBOT)
(NOT-LEFTOF11-ROBOT)
(not (NOT-LEFTOF12-ROBOT))
(not (LEFTOF11-ROBOT))
)
)
(when
(and
(LEFTOF10-ROBOT)
)
(and
(LEFTOF12-ROBOT)
(NOT-LEFTOF11-ROBOT)
(LEFTOF11-ROBOT)
(NOT-LEFTOF10-ROBOT)
(not (NOT-LEFTOF12-ROBOT))
(not (LEFTOF10-ROBOT))
)
)
(when
(and
(RIGHTOF11-ROBOT)
)
(and
(RIGHTOF10-ROBOT)
(RIGHTOF9-ROBOT)
(RIGHTOF8-ROBOT)
(RIGHTOF7-ROBOT)
(RIGHTOF6-ROBOT)
(RIGHTOF5-ROBOT)
(RIGHTOF4-ROBOT)
(RIGHTOF3-ROBOT)
(RIGHTOF2-ROBOT)
(not (NOT-RIGHTOF11-ROBOT))
(not (NOT-RIGHTOF10-ROBOT))
(not (NOT-RIGHTOF9-ROBOT))
(not (NOT-RIGHTOF8-ROBOT))
(not (NOT-RIGHTOF7-ROBOT))
(not (NOT-RIGHTOF6-ROBOT))
(not (NOT-RIGHTOF5-ROBOT))
(not (NOT-RIGHTOF4-ROBOT))
(not (NOT-RIGHTOF3-ROBOT))
(not (NOT-RIGHTOF2-ROBOT))
)
)
(when
(and
(RIGHTOF10-ROBOT)
)
(and
(RIGHTOF11-ROBOT)
(RIGHTOF9-ROBOT)
(RIGHTOF8-ROBOT)
(RIGHTOF7-ROBOT)
(RIGHTOF6-ROBOT)
(RIGHTOF5-ROBOT)
(RIGHTOF4-ROBOT)
(RIGHTOF3-ROBOT)
(RIGHTOF2-ROBOT)
(not (NOT-RIGHTOF11-ROBOT))
(not (NOT-RIGHTOF10-ROBOT))
(not (NOT-RIGHTOF9-ROBOT))
(not (NOT-RIGHTOF8-ROBOT))
(not (NOT-RIGHTOF7-ROBOT))
(not (NOT-RIGHTOF6-ROBOT))
(not (NOT-RIGHTOF5-ROBOT))
(not (NOT-RIGHTOF4-ROBOT))
(not (NOT-RIGHTOF3-ROBOT))
(not (NOT-RIGHTOF2-ROBOT))
)
)
(when
(and
(RIGHTOF9-ROBOT)
)
(and
(RIGHTOF10-ROBOT)
(RIGHTOF8-ROBOT)
(RIGHTOF7-ROBOT)
(RIGHTOF6-ROBOT)
(RIGHTOF5-ROBOT)
(RIGHTOF4-ROBOT)
(RIGHTOF3-ROBOT)
(RIGHTOF2-ROBOT)
(not (NOT-RIGHTOF10-ROBOT))
(not (NOT-RIGHTOF9-ROBOT))
(not (NOT-RIGHTOF8-ROBOT))
(not (NOT-RIGHTOF7-ROBOT))
(not (NOT-RIGHTOF6-ROBOT))
(not (NOT-RIGHTOF5-ROBOT))
(not (NOT-RIGHTOF4-ROBOT))
(not (NOT-RIGHTOF3-ROBOT))
(not (NOT-RIGHTOF2-ROBOT))
)
)
(when
(and
(RIGHTOF8-ROBOT)
)
(and
(RIGHTOF9-ROBOT)
(RIGHTOF7-ROBOT)
(RIGHTOF6-ROBOT)
(RIGHTOF5-ROBOT)
(RIGHTOF4-ROBOT)
(RIGHTOF3-ROBOT)
(RIGHTOF2-ROBOT)
(not (NOT-RIGHTOF9-ROBOT))
(not (NOT-RIGHTOF8-ROBOT))
(not (NOT-RIGHTOF7-ROBOT))
(not (NOT-RIGHTOF6-ROBOT))
(not (NOT-RIGHTOF5-ROBOT))
(not (NOT-RIGHTOF4-ROBOT))
(not (NOT-RIGHTOF3-ROBOT))
(not (NOT-RIGHTOF2-ROBOT))
)
)
(when
(and
(RIGHTOF7-ROBOT)
)
(and
(RIGHTOF8-ROBOT)
(RIGHTOF6-ROBOT)
(RIGHTOF5-ROBOT)
(RIGHTOF4-ROBOT)
(RIGHTOF3-ROBOT)
(RIGHTOF2-ROBOT)
(not (NOT-RIGHTOF8-ROBOT))
(not (NOT-RIGHTOF7-ROBOT))
(not (NOT-RIGHTOF6-ROBOT))
(not (NOT-RIGHTOF5-ROBOT))
(not (NOT-RIGHTOF4-ROBOT))
(not (NOT-RIGHTOF3-ROBOT))
(not (NOT-RIGHTOF2-ROBOT))
)
)
(when
(and
(RIGHTOF6-ROBOT)
)
(and
(RIGHTOF7-ROBOT)
(RIGHTOF5-ROBOT)
(RIGHTOF4-ROBOT)
(RIGHTOF3-ROBOT)
(RIGHTOF2-ROBOT)
(not (NOT-RIGHTOF7-ROBOT))
(not (NOT-RIGHTOF6-ROBOT))
(not (NOT-RIGHTOF5-ROBOT))
(not (NOT-RIGHTOF4-ROBOT))
(not (NOT-RIGHTOF3-ROBOT))
(not (NOT-RIGHTOF2-ROBOT))
)
)
(when
(and
(RIGHTOF5-ROBOT)
)
(and
(RIGHTOF6-ROBOT)
(RIGHTOF4-ROBOT)
(RIGHTOF3-ROBOT)
(RIGHTOF2-ROBOT)
(not (NOT-RIGHTOF6-ROBOT))
(not (NOT-RIGHTOF5-ROBOT))
(not (NOT-RIGHTOF4-ROBOT))
(not (NOT-RIGHTOF3-ROBOT))
(not (NOT-RIGHTOF2-ROBOT))
)
)
(when
(and
(RIGHTOF4-ROBOT)
)
(and
(RIGHTOF5-ROBOT)
(RIGHTOF3-ROBOT)
(RIGHTOF2-ROBOT)
(not (NOT-RIGHTOF5-ROBOT))
(not (NOT-RIGHTOF4-ROBOT))
(not (NOT-RIGHTOF3-ROBOT))
(not (NOT-RIGHTOF2-ROBOT))
)
)
(when
(and
(RIGHTOF3-ROBOT)
)
(and
(RIGHTOF4-ROBOT)
(RIGHTOF2-ROBOT)
(not (NOT-RIGHTOF4-ROBOT))
(not (NOT-RIGHTOF3-ROBOT))
(not (NOT-RIGHTOF2-ROBOT))
)
)
(when
(and
(RIGHTOF2-ROBOT)
)
(and
(RIGHTOF3-ROBOT)
(not (NOT-RIGHTOF3-ROBOT))
(not (NOT-RIGHTOF2-ROBOT))
)
)
(when
(and
(RIGHTOF1-ROBOT)
)
(and
(RIGHTOF2-ROBOT)
(not (NOT-RIGHTOF2-ROBOT))
)
)
(when
(and
(Flag238-)
)
(and
(RIGHTOF1-ROBOT)
(not (NOT-RIGHTOF1-ROBOT))
)
)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action MOVELEFT-ROBOT
:parameters ()
:precondition
(and
(Flag4-)
)
:effect
(and
(when
(and
(Flag237-)
)
(and
(COLUMN10-ROBOT)
(not (NOT-COLUMN10-ROBOT))
)
)
(when
(and
(Flag235-)
)
(and
(COLUMN9-ROBOT)
(not (NOT-COLUMN9-ROBOT))
)
)
(when
(and
(Flag232-)
)
(and
(COLUMN8-ROBOT)
(not (NOT-COLUMN8-ROBOT))
)
)
(when
(and
(Flag228-)
)
(and
(COLUMN7-ROBOT)
(not (NOT-COLUMN7-ROBOT))
)
)
(when
(and
(Flag223-)
)
(and
(COLUMN6-ROBOT)
(not (NOT-COLUMN6-ROBOT))
)
)
(when
(and
(Flag216-)
)
(and
(COLUMN5-ROBOT)
(not (NOT-COLUMN5-ROBOT))
)
)
(when
(and
(Flag208-)
)
(and
(COLUMN4-ROBOT)
(not (NOT-COLUMN4-ROBOT))
)
)
(when
(and
(Flag200-)
)
(and
(COLUMN3-ROBOT)
(not (NOT-COLUMN3-ROBOT))
)
)
(when
(and
(Flag189-)
)
(and
(COLUMN2-ROBOT)
(not (NOT-COLUMN2-ROBOT))
)
)
(when
(and
(Flag178-)
)
(and
(COLUMN1-ROBOT)
(not (NOT-COLUMN1-ROBOT))
)
)
(when
(and
(Flag167-)
)
(and
(COLUMN0-ROBOT)
(not (NOT-COLUMN0-ROBOT))
)
)
(when
(and
(COLUMN11-ROBOT)
)
(and
(COLUMN10-ROBOT)
(NOT-COLUMN11-ROBOT)
(not (NOT-COLUMN10-ROBOT))
(not (COLUMN11-ROBOT))
)
)
(when
(and
(COLUMN10-ROBOT)
)
(and
(COLUMN9-ROBOT)
(NOT-COLUMN10-ROBOT)
(not (NOT-COLUMN9-ROBOT))
(not (COLUMN10-ROBOT))
)
)
(when
(and
(COLUMN9-ROBOT)
)
(and
(COLUMN8-ROBOT)
(NOT-COLUMN9-ROBOT)
(not (NOT-COLUMN8-ROBOT))
(not (COLUMN9-ROBOT))
)
)
(when
(and
(COLUMN8-ROBOT)
)
(and
(COLUMN7-ROBOT)
(NOT-COLUMN8-ROBOT)
(not (NOT-COLUMN7-ROBOT))
(not (COLUMN8-ROBOT))
)
)
(when
(and
(COLUMN7-ROBOT)
)
(and
(COLUMN6-ROBOT)
(NOT-COLUMN7-ROBOT)
(not (NOT-COLUMN6-ROBOT))
(not (COLUMN7-ROBOT))
)
)
(when
(and
(COLUMN6-ROBOT)
)
(and
(COLUMN5-ROBOT)
(NOT-COLUMN6-ROBOT)
(not (NOT-COLUMN5-ROBOT))
(not (COLUMN6-ROBOT))
)
)
(when
(and
(COLUMN5-ROBOT)
)
(and
(COLUMN4-ROBOT)
(NOT-COLUMN5-ROBOT)
(not (NOT-COLUMN4-ROBOT))
(not (COLUMN5-ROBOT))
)
)
(when
(and
(COLUMN4-ROBOT)
)
(and
(COLUMN3-ROBOT)
(NOT-COLUMN4-ROBOT)
(not (NOT-COLUMN3-ROBOT))
(not (COLUMN4-ROBOT))
)
)
(when
(and
(COLUMN3-ROBOT)
)
(and
(COLUMN2-ROBOT)
(NOT-COLUMN3-ROBOT)
(not (NOT-COLUMN2-ROBOT))
(not (COLUMN3-ROBOT))
)
)
(when
(and
(COLUMN2-ROBOT)
)
(and
(COLUMN1-ROBOT)
(NOT-COLUMN2-ROBOT)
(not (NOT-COLUMN1-ROBOT))
(not (COLUMN2-ROBOT))
)
)
(when
(and
(COLUMN1-ROBOT)
)
(and
(COLUMN0-ROBOT)
(NOT-COLUMN1-ROBOT)
(not (NOT-COLUMN0-ROBOT))
(not (COLUMN1-ROBOT))
)
)
(when
(and
(RIGHTOF11-ROBOT)
)
(and
(RIGHTOF10-ROBOT)
(NOT-RIGHTOF11-ROBOT)
(not (NOT-RIGHTOF10-ROBOT))
(not (RIGHTOF11-ROBOT))
)
)
(when
(and
(Flag143-)
)
(and
(RIGHTOF9-ROBOT)
(NOT-RIGHTOF10-ROBOT)
(not (NOT-RIGHTOF9-ROBOT))
(not (RIGHTOF10-ROBOT))
)
)
(when
(and
(Flag142-)
)
(and
(RIGHTOF8-ROBOT)
(NOT-RIGHTOF9-ROBOT)
(not (NOT-RIGHTOF8-ROBOT))
(not (RIGHTOF9-ROBOT))
)
)
(when
(and
(Flag141-)
)
(and
(RIGHTOF7-ROBOT)
(NOT-RIGHTOF8-ROBOT)
(not (NOT-RIGHTOF7-ROBOT))
(not (RIGHTOF8-ROBOT))
)
)
(when
(and
(Flag140-)
)
(and
(RIGHTOF6-ROBOT)
(NOT-RIGHTOF7-ROBOT)
(not (NOT-RIGHTOF6-ROBOT))
(not (RIGHTOF7-ROBOT))
)
)
(when
(and
(Flag139-)
)
(and
(RIGHTOF5-ROBOT)
(NOT-RIGHTOF6-ROBOT)
(not (NOT-RIGHTOF5-ROBOT))
(not (RIGHTOF6-ROBOT))
)
)
(when
(and
(Flag138-)
)
(and
(RIGHTOF4-ROBOT)
(NOT-RIGHTOF5-ROBOT)
(not (NOT-RIGHTOF4-ROBOT))
(not (RIGHTOF5-ROBOT))
)
)
(when
(and
(Flag137-)
)
(and
(RIGHTOF3-ROBOT)
(NOT-RIGHTOF4-ROBOT)
(not (NOT-RIGHTOF3-ROBOT))
(not (RIGHTOF4-ROBOT))
)
)
(when
(and
(Flag136-)
)
(and
(RIGHTOF2-ROBOT)
(NOT-RIGHTOF3-ROBOT)
(not (NOT-RIGHTOF2-ROBOT))
(not (RIGHTOF3-ROBOT))
)
)
(when
(and
(Flag135-)
)
(and
(RIGHTOF1-ROBOT)
(NOT-RIGHTOF2-ROBOT)
(not (NOT-RIGHTOF1-ROBOT))
(not (RIGHTOF2-ROBOT))
)
)
(when
(and
(Flag134-)
)
(and
(RIGHTOF0-ROBOT)
(NOT-RIGHTOF1-ROBOT)
(not (NOT-RIGHTOF0-ROBOT))
(not (RIGHTOF1-ROBOT))
)
)
(when
(and
(Flag133-)
)
(and
(LEFTOF11-ROBOT)
(not (NOT-LEFTOF11-ROBOT))
)
)
(when
(and
(Flag132-)
)
(and
(LEFTOF10-ROBOT)
(not (NOT-LEFTOF10-ROBOT))
)
)
(when
(and
(Flag131-)
)
(and
(LEFTOF9-ROBOT)
(not (NOT-LEFTOF9-ROBOT))
)
)
(when
(and
(Flag130-)
)
(and
(LEFTOF8-ROBOT)
(not (NOT-LEFTOF8-ROBOT))
)
)
(when
(and
(Flag129-)
)
(and
(LEFTOF7-ROBOT)
(not (NOT-LEFTOF7-ROBOT))
)
)
(when
(and
(Flag128-)
)
(and
(LEFTOF6-ROBOT)
(not (NOT-LEFTOF6-ROBOT))
)
)
(when
(and
(Flag127-)
)
(and
(LEFTOF5-ROBOT)
(not (NOT-LEFTOF5-ROBOT))
)
)
(when
(and
(Flag126-)
)
(and
(LEFTOF4-ROBOT)
(not (NOT-LEFTOF4-ROBOT))
)
)
(when
(and
(Flag125-)
)
(and
(LEFTOF3-ROBOT)
(not (NOT-LEFTOF3-ROBOT))
)
)
(when
(and
(Flag124-)
)
(and
(LEFTOF2-ROBOT)
(not (NOT-LEFTOF2-ROBOT))
)
)
(when
(and
(Flag123-)
)
(and
(LEFTOF1-ROBOT)
(not (NOT-LEFTOF1-ROBOT))
)
)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action CHECKCONSISTENCYACTION
:parameters ()
:precondition
(and
(NOT-ERROR-)
(CHECKCONSISTENCY-)
)
:effect
(and
(when
(and
(ABOVEOF7-ROBOT)
(BELOWOF1-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF11-ROBOT)
(BELOWOF1-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF3-ROBOT)
(RIGHTOF5-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF1-ROBOT)
(ABOVEOF4-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF2-ROBOT)
(ABOVEOF7-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF10-ROBOT)
(LEFTOF4-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF11-ROBOT)
(BELOWOF10-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF3-ROBOT)
(ABOVEOF8-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF8-ROBOT)
(ABOVEOF11-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF6-ROBOT)
(RIGHTOF11-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF11-ROBOT)
(LEFTOF5-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF5-ROBOT)
(ABOVEOF11-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF7-ROBOT)
(RIGHTOF9-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF10-ROBOT)
(BELOWOF2-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF8-ROBOT)
(BELOWOF1-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF10-ROBOT)
(LEFTOF1-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF4-ROBOT)
(ABOVEOF11-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF11-ROBOT)
(LEFTOF9-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF8-ROBOT)
(LEFTOF6-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF1-ROBOT)
(RIGHTOF7-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF4-ROBOT)
(RIGHTOF8-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF11-ROBOT)
(BELOWOF7-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF11-ROBOT)
(BELOWOF9-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF11-ROBOT)
(BELOWOF3-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF4-ROBOT)
(ABOVEOF8-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF11-ROBOT)
(LEFTOF8-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF6-ROBOT)
(LEFTOF1-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF2-ROBOT)
(RIGHTOF10-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF1-ROBOT)
(ABOVEOF2-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF11-ROBOT)
(LEFTOF7-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF1-ROBOT)
(ABOVEOF9-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF3-ROBOT)
(ABOVEOF9-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF1-ROBOT)
(RIGHTOF1-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF5-ROBOT)
(RIGHTOF9-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF10-ROBOT)
(LEFTOF5-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF2-ROBOT)
(RIGHTOF6-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF5-ROBOT)
(RIGHTOF8-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF10-ROBOT)
(BELOWOF3-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF4-ROBOT)
(RIGHTOF9-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF6-ROBOT)
(BELOWOF1-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF11-ROBOT)
(BELOWOF2-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF4-ROBOT)
(RIGHTOF11-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF1-ROBOT)
(RIGHTOF3-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF2-ROBOT)
(LEFTOF2-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF5-ROBOT)
(ABOVEOF9-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF10-ROBOT)
(BELOWOF1-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF4-ROBOT)
(RIGHTOF7-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF1-ROBOT)
(RIGHTOF5-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF9-ROBOT)
(BELOWOF2-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF8-ROBOT)
(ABOVEOF10-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF8-ROBOT)
(LEFTOF3-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF9-ROBOT)
(BELOWOF7-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF2-ROBOT)
(ABOVEOF4-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF10-ROBOT)
(BELOWOF6-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF3-ROBOT)
(LEFTOF3-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF6-ROBOT)
(RIGHTOF9-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF6-ROBOT)
(ABOVEOF11-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF8-ROBOT)
(RIGHTOF8-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF4-ROBOT)
(LEFTOF1-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF6-ROBOT)
(LEFTOF3-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF5-ROBOT)
(ABOVEOF10-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF7-ROBOT)
(LEFTOF3-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF7-ROBOT)
(RIGHTOF10-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF10-ROBOT)
(LEFTOF3-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF4-ROBOT)
(ABOVEOF10-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF3-ROBOT)
(RIGHTOF4-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF6-ROBOT)
(ABOVEOF9-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF6-ROBOT)
(BELOWOF6-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF3-ROBOT)
(ABOVEOF7-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF8-ROBOT)
(RIGHTOF9-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF8-ROBOT)
(ABOVEOF9-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF2-ROBOT)
(RIGHTOF8-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF2-ROBOT)
(RIGHTOF9-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF7-ROBOT)
(LEFTOF6-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF6-ROBOT)
(ABOVEOF8-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF1-ROBOT)
(ABOVEOF5-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF6-ROBOT)
(LEFTOF4-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF3-ROBOT)
(RIGHTOF11-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF1-ROBOT)
(RIGHTOF8-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF2-ROBOT)
(ABOVEOF6-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF10-ROBOT)
(LEFTOF10-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF11-ROBOT)
(LEFTOF1-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF2-ROBOT)
(RIGHTOF5-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF2-ROBOT)
(ABOVEOF3-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF6-ROBOT)
(LEFTOF5-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF10-ROBOT)
(ABOVEOF10-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF5-ROBOT)
(BELOWOF5-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF2-ROBOT)
(ABOVEOF8-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF1-ROBOT)
(RIGHTOF9-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF7-ROBOT)
(RIGHTOF7-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF6-ROBOT)
(BELOWOF4-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF4-ROBOT)
(BELOWOF3-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF7-ROBOT)
(LEFTOF2-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF11-ROBOT)
(LEFTOF2-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF9-ROBOT)
(BELOWOF4-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF7-ROBOT)
(BELOWOF7-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF5-ROBOT)
(BELOWOF4-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF1-ROBOT)
(RIGHTOF2-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF7-ROBOT)
(ABOVEOF8-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF5-ROBOT)
(RIGHTOF7-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF5-ROBOT)
(ABOVEOF7-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF2-ROBOT)
(RIGHTOF4-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF6-ROBOT)
(BELOWOF3-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF9-ROBOT)
(LEFTOF3-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF5-ROBOT)
(ABOVEOF8-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF11-ROBOT)
(ABOVEOF11-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF9-ROBOT)
(ABOVEOF10-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF6-ROBOT)
(RIGHTOF10-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF6-ROBOT)
(BELOWOF5-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF4-ROBOT)
(ABOVEOF7-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF2-ROBOT)
(ABOVEOF5-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF6-ROBOT)
(ABOVEOF7-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF4-ROBOT)
(RIGHTOF5-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF11-ROBOT)
(LEFTOF11-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF8-ROBOT)
(RIGHTOF10-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF5-ROBOT)
(RIGHTOF5-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF9-ROBOT)
(RIGHTOF10-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF3-ROBOT)
(BELOWOF3-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF8-ROBOT)
(BELOWOF8-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF7-ROBOT)
(ABOVEOF10-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF3-ROBOT)
(ABOVEOF5-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF7-ROBOT)
(RIGHTOF8-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF2-ROBOT)
(BELOWOF2-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF4-ROBOT)
(ABOVEOF4-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF1-ROBOT)
(ABOVEOF3-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF11-ROBOT)
(LEFTOF10-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF1-ROBOT)
(BELOWOF1-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF4-ROBOT)
(RIGHTOF4-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF6-ROBOT)
(LEFTOF6-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF9-ROBOT)
(ABOVEOF9-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF9-ROBOT)
(RIGHTOF9-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF2-ROBOT)
(RIGHTOF3-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(NOT-CHECKCONSISTENCY-)
(not (CHECKCONSISTENCY-))
)
)
(:derived (Flag4-)(and (NOT-CHECKCONSISTENCY-)(NOT-ERROR-)(Flag3-)))

(:derived (Flag5-)(and (BELOWOF2-ROBOT)))

(:derived (Flag5-)(and (BELOWOF1-ROBOT)))

(:derived (Flag6-)(and (BELOWOF2-ROBOT)))

(:derived (Flag6-)(and (BELOWOF1-ROBOT)))

(:derived (Flag6-)(and (BELOWOF3-ROBOT)))

(:derived (Flag7-)(and (BELOWOF2-ROBOT)))

(:derived (Flag7-)(and (BELOWOF1-ROBOT)))

(:derived (Flag7-)(and (BELOWOF3-ROBOT)))

(:derived (Flag7-)(and (BELOWOF4-ROBOT)))

(:derived (Flag8-)(and (BELOWOF2-ROBOT)))

(:derived (Flag8-)(and (BELOWOF1-ROBOT)))

(:derived (Flag8-)(and (BELOWOF3-ROBOT)))

(:derived (Flag8-)(and (BELOWOF4-ROBOT)))

(:derived (Flag8-)(and (BELOWOF5-ROBOT)))

(:derived (Flag9-)(and (BELOWOF6-ROBOT)))

(:derived (Flag9-)(and (BELOWOF5-ROBOT)))

(:derived (Flag9-)(and (BELOWOF2-ROBOT)))

(:derived (Flag9-)(and (BELOWOF1-ROBOT)))

(:derived (Flag9-)(and (BELOWOF4-ROBOT)))

(:derived (Flag9-)(and (BELOWOF3-ROBOT)))

(:derived (Flag10-)(and (BELOWOF6-ROBOT)))

(:derived (Flag10-)(and (BELOWOF5-ROBOT)))

(:derived (Flag10-)(and (BELOWOF2-ROBOT)))

(:derived (Flag10-)(and (BELOWOF1-ROBOT)))

(:derived (Flag10-)(and (BELOWOF4-ROBOT)))

(:derived (Flag10-)(and (BELOWOF7-ROBOT)))

(:derived (Flag10-)(and (BELOWOF3-ROBOT)))

(:derived (Flag11-)(and (BELOWOF6-ROBOT)))

(:derived (Flag11-)(and (BELOWOF5-ROBOT)))

(:derived (Flag11-)(and (BELOWOF2-ROBOT)))

(:derived (Flag11-)(and (BELOWOF1-ROBOT)))

(:derived (Flag11-)(and (BELOWOF4-ROBOT)))

(:derived (Flag11-)(and (BELOWOF7-ROBOT)))

(:derived (Flag11-)(and (BELOWOF3-ROBOT)))

(:derived (Flag11-)(and (BELOWOF8-ROBOT)))

(:derived (Flag12-)(and (BELOWOF6-ROBOT)))

(:derived (Flag12-)(and (BELOWOF5-ROBOT)))

(:derived (Flag12-)(and (BELOWOF2-ROBOT)))

(:derived (Flag12-)(and (BELOWOF1-ROBOT)))

(:derived (Flag12-)(and (BELOWOF4-ROBOT)))

(:derived (Flag12-)(and (BELOWOF7-ROBOT)))

(:derived (Flag12-)(and (BELOWOF9-ROBOT)))

(:derived (Flag12-)(and (BELOWOF3-ROBOT)))

(:derived (Flag12-)(and (BELOWOF8-ROBOT)))

(:derived (Flag13-)(and (BELOWOF10-ROBOT)))

(:derived (Flag13-)(and (BELOWOF6-ROBOT)))

(:derived (Flag13-)(and (BELOWOF5-ROBOT)))

(:derived (Flag13-)(and (BELOWOF2-ROBOT)))

(:derived (Flag13-)(and (BELOWOF1-ROBOT)))

(:derived (Flag13-)(and (BELOWOF4-ROBOT)))

(:derived (Flag13-)(and (BELOWOF7-ROBOT)))

(:derived (Flag13-)(and (BELOWOF9-ROBOT)))

(:derived (Flag13-)(and (BELOWOF3-ROBOT)))

(:derived (Flag13-)(and (BELOWOF8-ROBOT)))

(:derived (Flag14-)(and (BELOWOF10-ROBOT)))

(:derived (Flag14-)(and (BELOWOF6-ROBOT)))

(:derived (Flag14-)(and (BELOWOF5-ROBOT)))

(:derived (Flag14-)(and (BELOWOF2-ROBOT)))

(:derived (Flag14-)(and (BELOWOF1-ROBOT)))

(:derived (Flag14-)(and (BELOWOF4-ROBOT)))

(:derived (Flag14-)(and (BELOWOF7-ROBOT)))

(:derived (Flag14-)(and (BELOWOF9-ROBOT)))

(:derived (Flag14-)(and (BELOWOF3-ROBOT)))

(:derived (Flag14-)(and (BELOWOF8-ROBOT)))

(:derived (Flag15-)(and (BELOWOF10-ROBOT)))

(:derived (Flag15-)(and (BELOWOF6-ROBOT)))

(:derived (Flag15-)(and (BELOWOF5-ROBOT)))

(:derived (Flag15-)(and (BELOWOF2-ROBOT)))

(:derived (Flag15-)(and (BELOWOF1-ROBOT)))

(:derived (Flag15-)(and (BELOWOF4-ROBOT)))

(:derived (Flag15-)(and (BELOWOF12-ROBOT)))

(:derived (Flag15-)(and (BELOWOF7-ROBOT)))

(:derived (Flag15-)(and (BELOWOF9-ROBOT)))

(:derived (Flag15-)(and (BELOWOF3-ROBOT)))

(:derived (Flag15-)(and (BELOWOF8-ROBOT)))

(:derived (Flag16-)(and (ABOVEOF6-ROBOT)))

(:derived (Flag16-)(and (ABOVEOF11-ROBOT)))

(:derived (Flag16-)(and (ABOVEOF10-ROBOT)))

(:derived (Flag16-)(and (ABOVEOF2-ROBOT)))

(:derived (Flag16-)(and (ABOVEOF7-ROBOT)))

(:derived (Flag16-)(and (ABOVEOF8-ROBOT)))

(:derived (Flag16-)(and (ABOVEOF9-ROBOT)))

(:derived (Flag16-)(and (ABOVEOF4-ROBOT)))

(:derived (Flag16-)(and (ABOVEOF3-ROBOT)))

(:derived (Flag16-)(and (ABOVEOF5-ROBOT)))

(:derived (Flag16-)(and (ABOVEOF1-ROBOT)))

(:derived (Flag17-)(and (ABOVEOF11-ROBOT)))

(:derived (Flag17-)(and (ABOVEOF6-ROBOT)))

(:derived (Flag17-)(and (ABOVEOF10-ROBOT)))

(:derived (Flag17-)(and (ABOVEOF2-ROBOT)))

(:derived (Flag17-)(and (ABOVEOF7-ROBOT)))

(:derived (Flag17-)(and (ABOVEOF8-ROBOT)))

(:derived (Flag17-)(and (ABOVEOF9-ROBOT)))

(:derived (Flag17-)(and (ABOVEOF4-ROBOT)))

(:derived (Flag17-)(and (ABOVEOF3-ROBOT)))

(:derived (Flag17-)(and (ABOVEOF5-ROBOT)))

(:derived (Flag18-)(and (ABOVEOF11-ROBOT)))

(:derived (Flag18-)(and (ABOVEOF6-ROBOT)))

(:derived (Flag18-)(and (ABOVEOF10-ROBOT)))

(:derived (Flag18-)(and (ABOVEOF8-ROBOT)))

(:derived (Flag18-)(and (ABOVEOF7-ROBOT)))

(:derived (Flag18-)(and (ABOVEOF9-ROBOT)))

(:derived (Flag18-)(and (ABOVEOF4-ROBOT)))

(:derived (Flag18-)(and (ABOVEOF3-ROBOT)))

(:derived (Flag18-)(and (ABOVEOF5-ROBOT)))

(:derived (Flag19-)(and (ABOVEOF11-ROBOT)))

(:derived (Flag19-)(and (ABOVEOF6-ROBOT)))

(:derived (Flag19-)(and (ABOVEOF10-ROBOT)))

(:derived (Flag19-)(and (ABOVEOF8-ROBOT)))

(:derived (Flag19-)(and (ABOVEOF7-ROBOT)))

(:derived (Flag19-)(and (ABOVEOF9-ROBOT)))

(:derived (Flag19-)(and (ABOVEOF4-ROBOT)))

(:derived (Flag19-)(and (ABOVEOF5-ROBOT)))

(:derived (Flag20-)(and (ABOVEOF11-ROBOT)))

(:derived (Flag20-)(and (ABOVEOF6-ROBOT)))

(:derived (Flag20-)(and (ABOVEOF7-ROBOT)))

(:derived (Flag20-)(and (ABOVEOF8-ROBOT)))

(:derived (Flag20-)(and (ABOVEOF10-ROBOT)))

(:derived (Flag20-)(and (ABOVEOF9-ROBOT)))

(:derived (Flag20-)(and (ABOVEOF5-ROBOT)))

(:derived (Flag21-)(and (ABOVEOF11-ROBOT)))

(:derived (Flag21-)(and (ABOVEOF6-ROBOT)))

(:derived (Flag21-)(and (ABOVEOF7-ROBOT)))

(:derived (Flag21-)(and (ABOVEOF8-ROBOT)))

(:derived (Flag21-)(and (ABOVEOF10-ROBOT)))

(:derived (Flag21-)(and (ABOVEOF9-ROBOT)))

(:derived (Flag22-)(and (ABOVEOF8-ROBOT)))

(:derived (Flag22-)(and (ABOVEOF11-ROBOT)))

(:derived (Flag22-)(and (ABOVEOF7-ROBOT)))

(:derived (Flag22-)(and (ABOVEOF9-ROBOT)))

(:derived (Flag22-)(and (ABOVEOF10-ROBOT)))

(:derived (Flag23-)(and (ABOVEOF8-ROBOT)))

(:derived (Flag23-)(and (ABOVEOF11-ROBOT)))

(:derived (Flag23-)(and (ABOVEOF10-ROBOT)))

(:derived (Flag23-)(and (ABOVEOF9-ROBOT)))

(:derived (Flag24-)(and (ABOVEOF11-ROBOT)))

(:derived (Flag24-)(and (ABOVEOF10-ROBOT)))

(:derived (Flag24-)(and (ABOVEOF9-ROBOT)))

(:derived (Flag25-)(and (ABOVEOF10-ROBOT)))

(:derived (Flag25-)(and (ABOVEOF11-ROBOT)))

(:derived (Flag26-)(and (ABOVEOF2-ROBOT)(BELOWOF2-ROBOT)))

(:derived (Flag27-)(and (ABOVEOF11-ROBOT)(BELOWOF1-ROBOT)))

(:derived (Flag28-)(and (ABOVEOF1-ROBOT)(BELOWOF2-ROBOT)))

(:derived (Flag29-)(and (ABOVEOF7-ROBOT)(BELOWOF2-ROBOT)))

(:derived (Flag30-)(and (BELOWOF1-ROBOT)(ABOVEOF7-ROBOT)))

(:derived (Flag31-)(and (BELOWOF1-ROBOT)(ABOVEOF1-ROBOT)))

(:derived (Flag32-)(and (BELOWOF1-ROBOT)(ABOVEOF5-ROBOT)))

(:derived (Flag33-)(and (ABOVEOF4-ROBOT)(BELOWOF2-ROBOT)))

(:derived (Flag34-)(and (ABOVEOF6-ROBOT)(BELOWOF2-ROBOT)))

(:derived (Flag35-)(and (BELOWOF1-ROBOT)(ABOVEOF2-ROBOT)))

(:derived (Flag36-)(and (ABOVEOF9-ROBOT)(BELOWOF2-ROBOT)))

(:derived (Flag37-)(and (ABOVEOF6-ROBOT)(BELOWOF1-ROBOT)))

(:derived (Flag38-)(and (ABOVEOF10-ROBOT)(BELOWOF1-ROBOT)))

(:derived (Flag39-)(and (ABOVEOF11-ROBOT)(BELOWOF2-ROBOT)))

(:derived (Flag40-)(and (ABOVEOF3-ROBOT)(BELOWOF1-ROBOT)))

(:derived (Flag41-)(and (BELOWOF1-ROBOT)(ABOVEOF9-ROBOT)))

(:derived (Flag42-)(and (ABOVEOF10-ROBOT)(BELOWOF2-ROBOT)))

(:derived (Flag43-)(and (ABOVEOF8-ROBOT)(BELOWOF2-ROBOT)))

(:derived (Flag44-)(and (BELOWOF1-ROBOT)(ABOVEOF4-ROBOT)))

(:derived (Flag45-)(and (BELOWOF2-ROBOT)(ABOVEOF5-ROBOT)))

(:derived (Flag46-)(and (ABOVEOF8-ROBOT)(BELOWOF1-ROBOT)))

(:derived (Flag47-)(and (ABOVEOF3-ROBOT)(BELOWOF2-ROBOT)))

(:derived (Flag49-)(and (BELOWOF3-ROBOT)(ABOVEOF5-ROBOT)))

(:derived (Flag50-)(and (BELOWOF3-ROBOT)(ABOVEOF6-ROBOT)))

(:derived (Flag51-)(and (BELOWOF3-ROBOT)(ABOVEOF7-ROBOT)))

(:derived (Flag52-)(and (BELOWOF3-ROBOT)(ABOVEOF9-ROBOT)))

(:derived (Flag53-)(and (BELOWOF3-ROBOT)(ABOVEOF2-ROBOT)))

(:derived (Flag54-)(and (ABOVEOF3-ROBOT)(BELOWOF3-ROBOT)))

(:derived (Flag55-)(and (ABOVEOF8-ROBOT)(BELOWOF3-ROBOT)))

(:derived (Flag56-)(and (BELOWOF3-ROBOT)(ABOVEOF4-ROBOT)))

(:derived (Flag57-)(and (BELOWOF3-ROBOT)(ABOVEOF10-ROBOT)))

(:derived (Flag58-)(and (BELOWOF2-ROBOT)(ABOVEOF2-ROBOT)))

(:derived (Flag59-)(and (ABOVEOF11-ROBOT)(BELOWOF3-ROBOT)))

(:derived (Flag61-)(and (ABOVEOF3-ROBOT)(BELOWOF4-ROBOT)))

(:derived (Flag62-)(and (ABOVEOF6-ROBOT)(BELOWOF4-ROBOT)))

(:derived (Flag63-)(and (ABOVEOF9-ROBOT)(BELOWOF4-ROBOT)))

(:derived (Flag64-)(and (BELOWOF4-ROBOT)(ABOVEOF4-ROBOT)))

(:derived (Flag65-)(and (ABOVEOF8-ROBOT)(BELOWOF4-ROBOT)))

(:derived (Flag66-)(and (BELOWOF4-ROBOT)(ABOVEOF7-ROBOT)))

(:derived (Flag67-)(and (BELOWOF4-ROBOT)(ABOVEOF5-ROBOT)))

(:derived (Flag68-)(and (BELOWOF3-ROBOT)(ABOVEOF3-ROBOT)))

(:derived (Flag69-)(and (ABOVEOF10-ROBOT)(BELOWOF4-ROBOT)))

(:derived (Flag70-)(and (BELOWOF3-ROBOT)(ABOVEOF11-ROBOT)))

(:derived (Flag71-)(and (ABOVEOF11-ROBOT)(BELOWOF4-ROBOT)))

(:derived (Flag73-)(and (ABOVEOF4-ROBOT)(BELOWOF5-ROBOT)))

(:derived (Flag74-)(and (ABOVEOF8-ROBOT)(BELOWOF5-ROBOT)))

(:derived (Flag75-)(and (ABOVEOF10-ROBOT)(BELOWOF5-ROBOT)))

(:derived (Flag76-)(and (ABOVEOF11-ROBOT)(BELOWOF5-ROBOT)))

(:derived (Flag77-)(and (ABOVEOF5-ROBOT)(BELOWOF5-ROBOT)))

(:derived (Flag78-)(and (ABOVEOF7-ROBOT)(BELOWOF5-ROBOT)))

(:derived (Flag79-)(and (ABOVEOF9-ROBOT)(BELOWOF5-ROBOT)))

(:derived (Flag80-)(and (ABOVEOF6-ROBOT)(BELOWOF5-ROBOT)))

(:derived (Flag82-)(and (ABOVEOF11-ROBOT)(BELOWOF6-ROBOT)))

(:derived (Flag83-)(and (BELOWOF6-ROBOT)(ABOVEOF5-ROBOT)))

(:derived (Flag84-)(and (BELOWOF6-ROBOT)(ABOVEOF7-ROBOT)))

(:derived (Flag85-)(and (BELOWOF5-ROBOT)(ABOVEOF5-ROBOT)))

(:derived (Flag86-)(and (BELOWOF6-ROBOT)(ABOVEOF6-ROBOT)))

(:derived (Flag87-)(and (ABOVEOF8-ROBOT)(BELOWOF6-ROBOT)))

(:derived (Flag88-)(and (BELOWOF6-ROBOT)(ABOVEOF9-ROBOT)))

(:derived (Flag89-)(and (BELOWOF6-ROBOT)(ABOVEOF10-ROBOT)))

(:derived (Flag91-)(and (ABOVEOF6-ROBOT)(BELOWOF7-ROBOT)))

(:derived (Flag92-)(and (ABOVEOF9-ROBOT)(BELOWOF7-ROBOT)))

(:derived (Flag93-)(and (ABOVEOF6-ROBOT)(BELOWOF6-ROBOT)))

(:derived (Flag94-)(and (ABOVEOF10-ROBOT)(BELOWOF7-ROBOT)))

(:derived (Flag95-)(and (ABOVEOF10-ROBOT)(BELOWOF6-ROBOT)))

(:derived (Flag96-)(and (ABOVEOF7-ROBOT)(BELOWOF7-ROBOT)))

(:derived (Flag97-)(and (ABOVEOF8-ROBOT)(BELOWOF7-ROBOT)))

(:derived (Flag98-)(and (ABOVEOF11-ROBOT)(BELOWOF7-ROBOT)))

(:derived (Flag100-)(and (BELOWOF8-ROBOT)(ABOVEOF10-ROBOT)))

(:derived (Flag101-)(and (ABOVEOF8-ROBOT)(BELOWOF8-ROBOT)))

(:derived (Flag102-)(and (BELOWOF8-ROBOT)(ABOVEOF9-ROBOT)))

(:derived (Flag103-)(and (BELOWOF7-ROBOT)(ABOVEOF7-ROBOT)))

(:derived (Flag104-)(and (BELOWOF8-ROBOT)(ABOVEOF7-ROBOT)))

(:derived (Flag105-)(and (BELOWOF8-ROBOT)(ABOVEOF11-ROBOT)))

(:derived (Flag107-)(and (ABOVEOF10-ROBOT)(BELOWOF9-ROBOT)))

(:derived (Flag108-)(and (BELOWOF8-ROBOT)(ABOVEOF8-ROBOT)))

(:derived (Flag109-)(and (ABOVEOF8-ROBOT)(BELOWOF9-ROBOT)))

(:derived (Flag110-)(and (ABOVEOF11-ROBOT)(BELOWOF9-ROBOT)))

(:derived (Flag111-)(and (BELOWOF9-ROBOT)(ABOVEOF9-ROBOT)))

(:derived (Flag113-)(and (BELOWOF10-ROBOT)(ABOVEOF9-ROBOT)))

(:derived (Flag114-)(and (BELOWOF10-ROBOT)(ABOVEOF10-ROBOT)))

(:derived (Flag115-)(and (ABOVEOF9-ROBOT)(BELOWOF9-ROBOT)))

(:derived (Flag116-)(and (ABOVEOF11-ROBOT)(BELOWOF10-ROBOT)))

(:derived (Flag118-)(and (BELOWOF11-ROBOT)(ABOVEOF10-ROBOT)))

(:derived (Flag119-)(and (ABOVEOF11-ROBOT)(BELOWOF11-ROBOT)))

(:derived (Flag121-)(and (BELOWOF12-ROBOT)(ABOVEOF11-ROBOT)))

(:derived (Flag241-)(and (BELOWOF1-ROBOT)))

(:action MOVEUP-ROBOT
:parameters ()
:precondition
(and
(Flag2-)
)
:effect
(and
(when
(and
(Flag120-)
)
(and
(ROW11-ROBOT)
(not (NOT-ROW11-ROBOT))
)
)
(when
(and
(Flag117-)
)
(and
(ROW10-ROBOT)
(not (NOT-ROW10-ROBOT))
)
)
(when
(and
(Flag112-)
)
(and
(ROW9-ROBOT)
(not (NOT-ROW9-ROBOT))
)
)
(when
(and
(Flag106-)
)
(and
(ROW8-ROBOT)
(not (NOT-ROW8-ROBOT))
)
)
(when
(and
(Flag99-)
)
(and
(ROW7-ROBOT)
(not (NOT-ROW7-ROBOT))
)
)
(when
(and
(Flag90-)
)
(and
(ROW6-ROBOT)
(not (NOT-ROW6-ROBOT))
)
)
(when
(and
(Flag81-)
)
(and
(ROW5-ROBOT)
(not (NOT-ROW5-ROBOT))
)
)
(when
(and
(Flag72-)
)
(and
(ROW4-ROBOT)
(not (NOT-ROW4-ROBOT))
)
)
(when
(and
(Flag60-)
)
(and
(ROW3-ROBOT)
(not (NOT-ROW3-ROBOT))
)
)
(when
(and
(Flag48-)
)
(and
(ROW2-ROBOT)
(not (NOT-ROW2-ROBOT))
)
)
(when
(and
(Flag241-)
)
(and
(ROW1-ROBOT)
(not (NOT-ROW1-ROBOT))
)
)
(when
(and
(ROW10-ROBOT)
)
(and
(ROW11-ROBOT)
(NOT-ROW10-ROBOT)
(not (NOT-ROW11-ROBOT))
(not (ROW10-ROBOT))
)
)
(when
(and
(ROW9-ROBOT)
)
(and
(ROW10-ROBOT)
(NOT-ROW9-ROBOT)
(not (NOT-ROW10-ROBOT))
(not (ROW9-ROBOT))
)
)
(when
(and
(ROW8-ROBOT)
)
(and
(ROW9-ROBOT)
(NOT-ROW8-ROBOT)
(not (NOT-ROW9-ROBOT))
(not (ROW8-ROBOT))
)
)
(when
(and
(ROW7-ROBOT)
)
(and
(ROW8-ROBOT)
(NOT-ROW7-ROBOT)
(not (NOT-ROW8-ROBOT))
(not (ROW7-ROBOT))
)
)
(when
(and
(ROW6-ROBOT)
)
(and
(ROW7-ROBOT)
(NOT-ROW6-ROBOT)
(not (NOT-ROW7-ROBOT))
(not (ROW6-ROBOT))
)
)
(when
(and
(ROW5-ROBOT)
)
(and
(ROW6-ROBOT)
(NOT-ROW5-ROBOT)
(not (NOT-ROW6-ROBOT))
(not (ROW5-ROBOT))
)
)
(when
(and
(ROW4-ROBOT)
)
(and
(ROW5-ROBOT)
(NOT-ROW4-ROBOT)
(not (NOT-ROW5-ROBOT))
(not (ROW4-ROBOT))
)
)
(when
(and
(ROW3-ROBOT)
)
(and
(ROW4-ROBOT)
(NOT-ROW3-ROBOT)
(not (NOT-ROW4-ROBOT))
(not (ROW3-ROBOT))
)
)
(when
(and
(ROW2-ROBOT)
)
(and
(ROW3-ROBOT)
(NOT-ROW2-ROBOT)
(not (NOT-ROW3-ROBOT))
(not (ROW2-ROBOT))
)
)
(when
(and
(ROW1-ROBOT)
)
(and
(ROW2-ROBOT)
(NOT-ROW1-ROBOT)
(not (NOT-ROW2-ROBOT))
(not (ROW1-ROBOT))
)
)
(when
(and
(ROW0-ROBOT)
)
(and
(ROW1-ROBOT)
(NOT-ROW0-ROBOT)
(not (NOT-ROW1-ROBOT))
(not (ROW0-ROBOT))
)
)
(when
(and
(BELOWOF8-ROBOT)
)
(and
(BELOWOF12-ROBOT)
(NOT-BELOWOF11-ROBOT)
(BELOWOF11-ROBOT)
(NOT-BELOWOF10-ROBOT)
(BELOWOF10-ROBOT)
(NOT-BELOWOF9-ROBOT)
(BELOWOF9-ROBOT)
(NOT-BELOWOF8-ROBOT)
(not (NOT-BELOWOF12-ROBOT))
(not (BELOWOF8-ROBOT))
)
)
(when
(and
(BELOWOF3-ROBOT)
)
(and
(BELOWOF12-ROBOT)
(NOT-BELOWOF11-ROBOT)
(BELOWOF11-ROBOT)
(NOT-BELOWOF10-ROBOT)
(BELOWOF10-ROBOT)
(NOT-BELOWOF9-ROBOT)
(BELOWOF9-ROBOT)
(NOT-BELOWOF8-ROBOT)
(BELOWOF8-ROBOT)
(NOT-BELOWOF7-ROBOT)
(BELOWOF7-ROBOT)
(NOT-BELOWOF6-ROBOT)
(BELOWOF6-ROBOT)
(NOT-BELOWOF5-ROBOT)
(BELOWOF5-ROBOT)
(NOT-BELOWOF4-ROBOT)
(BELOWOF4-ROBOT)
(NOT-BELOWOF3-ROBOT)
(not (NOT-BELOWOF12-ROBOT))
(not (BELOWOF3-ROBOT))
)
)
(when
(and
(BELOWOF9-ROBOT)
)
(and
(BELOWOF12-ROBOT)
(NOT-BELOWOF11-ROBOT)
(BELOWOF11-ROBOT)
(NOT-BELOWOF10-ROBOT)
(BELOWOF10-ROBOT)
(NOT-BELOWOF9-ROBOT)
(not (NOT-BELOWOF12-ROBOT))
(not (BELOWOF9-ROBOT))
)
)
(when
(and
(BELOWOF7-ROBOT)
)
(and
(BELOWOF12-ROBOT)
(NOT-BELOWOF11-ROBOT)
(BELOWOF11-ROBOT)
(NOT-BELOWOF10-ROBOT)
(BELOWOF10-ROBOT)
(NOT-BELOWOF9-ROBOT)
(BELOWOF9-ROBOT)
(NOT-BELOWOF8-ROBOT)
(BELOWOF8-ROBOT)
(NOT-BELOWOF7-ROBOT)
(not (NOT-BELOWOF12-ROBOT))
(not (BELOWOF7-ROBOT))
)
)
(when
(and
(BELOWOF4-ROBOT)
)
(and
(BELOWOF12-ROBOT)
(NOT-BELOWOF11-ROBOT)
(BELOWOF11-ROBOT)
(NOT-BELOWOF10-ROBOT)
(BELOWOF10-ROBOT)
(NOT-BELOWOF9-ROBOT)
(BELOWOF9-ROBOT)
(NOT-BELOWOF8-ROBOT)
(BELOWOF8-ROBOT)
(NOT-BELOWOF7-ROBOT)
(BELOWOF7-ROBOT)
(NOT-BELOWOF6-ROBOT)
(BELOWOF6-ROBOT)
(NOT-BELOWOF5-ROBOT)
(BELOWOF5-ROBOT)
(NOT-BELOWOF4-ROBOT)
(not (NOT-BELOWOF12-ROBOT))
(not (BELOWOF4-ROBOT))
)
)
(when
(and
(BELOWOF1-ROBOT)
)
(and
(BELOWOF12-ROBOT)
(NOT-BELOWOF11-ROBOT)
(BELOWOF11-ROBOT)
(NOT-BELOWOF10-ROBOT)
(BELOWOF10-ROBOT)
(NOT-BELOWOF9-ROBOT)
(BELOWOF9-ROBOT)
(NOT-BELOWOF8-ROBOT)
(BELOWOF8-ROBOT)
(NOT-BELOWOF7-ROBOT)
(BELOWOF7-ROBOT)
(NOT-BELOWOF6-ROBOT)
(BELOWOF6-ROBOT)
(NOT-BELOWOF5-ROBOT)
(BELOWOF5-ROBOT)
(NOT-BELOWOF4-ROBOT)
(BELOWOF4-ROBOT)
(NOT-BELOWOF3-ROBOT)
(BELOWOF3-ROBOT)
(NOT-BELOWOF2-ROBOT)
(BELOWOF2-ROBOT)
(NOT-BELOWOF1-ROBOT)
(not (NOT-BELOWOF12-ROBOT))
(not (BELOWOF1-ROBOT))
)
)
(when
(and
(BELOWOF2-ROBOT)
)
(and
(BELOWOF12-ROBOT)
(NOT-BELOWOF11-ROBOT)
(BELOWOF11-ROBOT)
(NOT-BELOWOF10-ROBOT)
(BELOWOF10-ROBOT)
(NOT-BELOWOF9-ROBOT)
(BELOWOF9-ROBOT)
(NOT-BELOWOF8-ROBOT)
(BELOWOF8-ROBOT)
(NOT-BELOWOF7-ROBOT)
(BELOWOF7-ROBOT)
(NOT-BELOWOF6-ROBOT)
(BELOWOF6-ROBOT)
(NOT-BELOWOF5-ROBOT)
(BELOWOF5-ROBOT)
(NOT-BELOWOF4-ROBOT)
(BELOWOF4-ROBOT)
(NOT-BELOWOF3-ROBOT)
(BELOWOF3-ROBOT)
(NOT-BELOWOF2-ROBOT)
(not (NOT-BELOWOF12-ROBOT))
(not (BELOWOF2-ROBOT))
)
)
(when
(and
(BELOWOF5-ROBOT)
)
(and
(BELOWOF12-ROBOT)
(NOT-BELOWOF11-ROBOT)
(BELOWOF11-ROBOT)
(NOT-BELOWOF10-ROBOT)
(BELOWOF10-ROBOT)
(NOT-BELOWOF9-ROBOT)
(BELOWOF9-ROBOT)
(NOT-BELOWOF8-ROBOT)
(BELOWOF8-ROBOT)
(NOT-BELOWOF7-ROBOT)
(BELOWOF7-ROBOT)
(NOT-BELOWOF6-ROBOT)
(BELOWOF6-ROBOT)
(NOT-BELOWOF5-ROBOT)
(not (NOT-BELOWOF12-ROBOT))
(not (BELOWOF5-ROBOT))
)
)
(when
(and
(BELOWOF6-ROBOT)
)
(and
(BELOWOF12-ROBOT)
(NOT-BELOWOF11-ROBOT)
(BELOWOF11-ROBOT)
(NOT-BELOWOF10-ROBOT)
(BELOWOF10-ROBOT)
(NOT-BELOWOF9-ROBOT)
(BELOWOF9-ROBOT)
(NOT-BELOWOF8-ROBOT)
(BELOWOF8-ROBOT)
(NOT-BELOWOF7-ROBOT)
(BELOWOF7-ROBOT)
(NOT-BELOWOF6-ROBOT)
(not (NOT-BELOWOF12-ROBOT))
(not (BELOWOF6-ROBOT))
)
)
(when
(and
(BELOWOF11-ROBOT)
)
(and
(BELOWOF12-ROBOT)
(NOT-BELOWOF11-ROBOT)
(not (NOT-BELOWOF12-ROBOT))
(not (BELOWOF11-ROBOT))
)
)
(when
(and
(BELOWOF10-ROBOT)
)
(and
(BELOWOF12-ROBOT)
(NOT-BELOWOF11-ROBOT)
(BELOWOF11-ROBOT)
(NOT-BELOWOF10-ROBOT)
(not (NOT-BELOWOF12-ROBOT))
(not (BELOWOF10-ROBOT))
)
)
(when
(and
(ABOVEOF11-ROBOT)
)
(and
(ABOVEOF10-ROBOT)
(ABOVEOF9-ROBOT)
(ABOVEOF8-ROBOT)
(ABOVEOF7-ROBOT)
(ABOVEOF6-ROBOT)
(ABOVEOF5-ROBOT)
(ABOVEOF4-ROBOT)
(ABOVEOF3-ROBOT)
(ABOVEOF2-ROBOT)
(not (NOT-ABOVEOF11-ROBOT))
(not (NOT-ABOVEOF10-ROBOT))
(not (NOT-ABOVEOF9-ROBOT))
(not (NOT-ABOVEOF8-ROBOT))
(not (NOT-ABOVEOF7-ROBOT))
(not (NOT-ABOVEOF6-ROBOT))
(not (NOT-ABOVEOF5-ROBOT))
(not (NOT-ABOVEOF4-ROBOT))
(not (NOT-ABOVEOF3-ROBOT))
(not (NOT-ABOVEOF2-ROBOT))
)
)
(when
(and
(ABOVEOF10-ROBOT)
)
(and
(ABOVEOF11-ROBOT)
(ABOVEOF9-ROBOT)
(ABOVEOF8-ROBOT)
(ABOVEOF7-ROBOT)
(ABOVEOF6-ROBOT)
(ABOVEOF5-ROBOT)
(ABOVEOF4-ROBOT)
(ABOVEOF3-ROBOT)
(ABOVEOF2-ROBOT)
(not (NOT-ABOVEOF11-ROBOT))
(not (NOT-ABOVEOF10-ROBOT))
(not (NOT-ABOVEOF9-ROBOT))
(not (NOT-ABOVEOF8-ROBOT))
(not (NOT-ABOVEOF7-ROBOT))
(not (NOT-ABOVEOF6-ROBOT))
(not (NOT-ABOVEOF5-ROBOT))
(not (NOT-ABOVEOF4-ROBOT))
(not (NOT-ABOVEOF3-ROBOT))
(not (NOT-ABOVEOF2-ROBOT))
)
)
(when
(and
(ABOVEOF9-ROBOT)
)
(and
(ABOVEOF10-ROBOT)
(ABOVEOF8-ROBOT)
(ABOVEOF7-ROBOT)
(ABOVEOF6-ROBOT)
(ABOVEOF5-ROBOT)
(ABOVEOF4-ROBOT)
(ABOVEOF3-ROBOT)
(ABOVEOF2-ROBOT)
(not (NOT-ABOVEOF10-ROBOT))
(not (NOT-ABOVEOF9-ROBOT))
(not (NOT-ABOVEOF8-ROBOT))
(not (NOT-ABOVEOF7-ROBOT))
(not (NOT-ABOVEOF6-ROBOT))
(not (NOT-ABOVEOF5-ROBOT))
(not (NOT-ABOVEOF4-ROBOT))
(not (NOT-ABOVEOF3-ROBOT))
(not (NOT-ABOVEOF2-ROBOT))
)
)
(when
(and
(ABOVEOF8-ROBOT)
)
(and
(ABOVEOF9-ROBOT)
(ABOVEOF7-ROBOT)
(ABOVEOF6-ROBOT)
(ABOVEOF5-ROBOT)
(ABOVEOF4-ROBOT)
(ABOVEOF3-ROBOT)
(ABOVEOF2-ROBOT)
(not (NOT-ABOVEOF9-ROBOT))
(not (NOT-ABOVEOF8-ROBOT))
(not (NOT-ABOVEOF7-ROBOT))
(not (NOT-ABOVEOF6-ROBOT))
(not (NOT-ABOVEOF5-ROBOT))
(not (NOT-ABOVEOF4-ROBOT))
(not (NOT-ABOVEOF3-ROBOT))
(not (NOT-ABOVEOF2-ROBOT))
)
)
(when
(and
(ABOVEOF7-ROBOT)
)
(and
(ABOVEOF8-ROBOT)
(ABOVEOF6-ROBOT)
(ABOVEOF5-ROBOT)
(ABOVEOF4-ROBOT)
(ABOVEOF3-ROBOT)
(ABOVEOF2-ROBOT)
(not (NOT-ABOVEOF8-ROBOT))
(not (NOT-ABOVEOF7-ROBOT))
(not (NOT-ABOVEOF6-ROBOT))
(not (NOT-ABOVEOF5-ROBOT))
(not (NOT-ABOVEOF4-ROBOT))
(not (NOT-ABOVEOF3-ROBOT))
(not (NOT-ABOVEOF2-ROBOT))
)
)
(when
(and
(ABOVEOF6-ROBOT)
)
(and
(ABOVEOF7-ROBOT)
(ABOVEOF5-ROBOT)
(ABOVEOF4-ROBOT)
(ABOVEOF3-ROBOT)
(ABOVEOF2-ROBOT)
(not (NOT-ABOVEOF7-ROBOT))
(not (NOT-ABOVEOF6-ROBOT))
(not (NOT-ABOVEOF5-ROBOT))
(not (NOT-ABOVEOF4-ROBOT))
(not (NOT-ABOVEOF3-ROBOT))
(not (NOT-ABOVEOF2-ROBOT))
)
)
(when
(and
(ABOVEOF5-ROBOT)
)
(and
(ABOVEOF6-ROBOT)
(ABOVEOF4-ROBOT)
(ABOVEOF3-ROBOT)
(ABOVEOF2-ROBOT)
(not (NOT-ABOVEOF6-ROBOT))
(not (NOT-ABOVEOF5-ROBOT))
(not (NOT-ABOVEOF4-ROBOT))
(not (NOT-ABOVEOF3-ROBOT))
(not (NOT-ABOVEOF2-ROBOT))
)
)
(when
(and
(ABOVEOF4-ROBOT)
)
(and
(ABOVEOF5-ROBOT)
(ABOVEOF3-ROBOT)
(ABOVEOF2-ROBOT)
(not (NOT-ABOVEOF5-ROBOT))
(not (NOT-ABOVEOF4-ROBOT))
(not (NOT-ABOVEOF3-ROBOT))
(not (NOT-ABOVEOF2-ROBOT))
)
)
(when
(and
(ABOVEOF3-ROBOT)
)
(and
(ABOVEOF4-ROBOT)
(ABOVEOF2-ROBOT)
(not (NOT-ABOVEOF4-ROBOT))
(not (NOT-ABOVEOF3-ROBOT))
(not (NOT-ABOVEOF2-ROBOT))
)
)
(when
(and
(ABOVEOF2-ROBOT)
)
(and
(ABOVEOF3-ROBOT)
(not (NOT-ABOVEOF3-ROBOT))
(not (NOT-ABOVEOF2-ROBOT))
)
)
(when
(and
(ABOVEOF1-ROBOT)
)
(and
(ABOVEOF2-ROBOT)
(not (NOT-ABOVEOF2-ROBOT))
)
)
(ABOVEOF1-ROBOT)
(CHECKCONSISTENCY-)
(not (NOT-ABOVEOF1-ROBOT))
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action MOVEDOWN-ROBOT
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
)
:effect
(and
(when
(and
(Flag122-)
)
(and
(ROW10-ROBOT)
(not (NOT-ROW10-ROBOT))
)
)
(when
(and
(Flag120-)
)
(and
(ROW9-ROBOT)
(not (NOT-ROW9-ROBOT))
)
)
(when
(and
(Flag117-)
)
(and
(ROW8-ROBOT)
(not (NOT-ROW8-ROBOT))
)
)
(when
(and
(Flag112-)
)
(and
(ROW7-ROBOT)
(not (NOT-ROW7-ROBOT))
)
)
(when
(and
(Flag106-)
)
(and
(ROW6-ROBOT)
(not (NOT-ROW6-ROBOT))
)
)
(when
(and
(Flag99-)
)
(and
(ROW5-ROBOT)
(not (NOT-ROW5-ROBOT))
)
)
(when
(and
(Flag90-)
)
(and
(ROW4-ROBOT)
(not (NOT-ROW4-ROBOT))
)
)
(when
(and
(Flag81-)
)
(and
(ROW3-ROBOT)
(not (NOT-ROW3-ROBOT))
)
)
(when
(and
(Flag72-)
)
(and
(ROW2-ROBOT)
(not (NOT-ROW2-ROBOT))
)
)
(when
(and
(Flag60-)
)
(and
(ROW1-ROBOT)
(not (NOT-ROW1-ROBOT))
)
)
(when
(and
(Flag48-)
)
(and
(ROW0-ROBOT)
(not (NOT-ROW0-ROBOT))
)
)
(when
(and
(ROW11-ROBOT)
)
(and
(ROW10-ROBOT)
(NOT-ROW11-ROBOT)
(not (NOT-ROW10-ROBOT))
(not (ROW11-ROBOT))
)
)
(when
(and
(ROW10-ROBOT)
)
(and
(ROW9-ROBOT)
(NOT-ROW10-ROBOT)
(not (NOT-ROW9-ROBOT))
(not (ROW10-ROBOT))
)
)
(when
(and
(ROW9-ROBOT)
)
(and
(ROW8-ROBOT)
(NOT-ROW9-ROBOT)
(not (NOT-ROW8-ROBOT))
(not (ROW9-ROBOT))
)
)
(when
(and
(ROW8-ROBOT)
)
(and
(ROW7-ROBOT)
(NOT-ROW8-ROBOT)
(not (NOT-ROW7-ROBOT))
(not (ROW8-ROBOT))
)
)
(when
(and
(ROW7-ROBOT)
)
(and
(ROW6-ROBOT)
(NOT-ROW7-ROBOT)
(not (NOT-ROW6-ROBOT))
(not (ROW7-ROBOT))
)
)
(when
(and
(ROW6-ROBOT)
)
(and
(ROW5-ROBOT)
(NOT-ROW6-ROBOT)
(not (NOT-ROW5-ROBOT))
(not (ROW6-ROBOT))
)
)
(when
(and
(ROW5-ROBOT)
)
(and
(ROW4-ROBOT)
(NOT-ROW5-ROBOT)
(not (NOT-ROW4-ROBOT))
(not (ROW5-ROBOT))
)
)
(when
(and
(ROW4-ROBOT)
)
(and
(ROW3-ROBOT)
(NOT-ROW4-ROBOT)
(not (NOT-ROW3-ROBOT))
(not (ROW4-ROBOT))
)
)
(when
(and
(ROW3-ROBOT)
)
(and
(ROW2-ROBOT)
(NOT-ROW3-ROBOT)
(not (NOT-ROW2-ROBOT))
(not (ROW3-ROBOT))
)
)
(when
(and
(ROW2-ROBOT)
)
(and
(ROW1-ROBOT)
(NOT-ROW2-ROBOT)
(not (NOT-ROW1-ROBOT))
(not (ROW2-ROBOT))
)
)
(when
(and
(ROW1-ROBOT)
)
(and
(ROW0-ROBOT)
(NOT-ROW1-ROBOT)
(not (NOT-ROW0-ROBOT))
(not (ROW1-ROBOT))
)
)
(when
(and
(ABOVEOF11-ROBOT)
)
(and
(ABOVEOF10-ROBOT)
(NOT-ABOVEOF11-ROBOT)
(not (NOT-ABOVEOF10-ROBOT))
(not (ABOVEOF11-ROBOT))
)
)
(when
(and
(Flag25-)
)
(and
(ABOVEOF9-ROBOT)
(NOT-ABOVEOF10-ROBOT)
(not (NOT-ABOVEOF9-ROBOT))
(not (ABOVEOF10-ROBOT))
)
)
(when
(and
(Flag24-)
)
(and
(ABOVEOF8-ROBOT)
(NOT-ABOVEOF9-ROBOT)
(not (NOT-ABOVEOF8-ROBOT))
(not (ABOVEOF9-ROBOT))
)
)
(when
(and
(Flag23-)
)
(and
(ABOVEOF7-ROBOT)
(NOT-ABOVEOF8-ROBOT)
(not (NOT-ABOVEOF7-ROBOT))
(not (ABOVEOF8-ROBOT))
)
)
(when
(and
(Flag22-)
)
(and
(ABOVEOF6-ROBOT)
(NOT-ABOVEOF7-ROBOT)
(not (NOT-ABOVEOF6-ROBOT))
(not (ABOVEOF7-ROBOT))
)
)
(when
(and
(Flag21-)
)
(and
(ABOVEOF5-ROBOT)
(NOT-ABOVEOF6-ROBOT)
(not (NOT-ABOVEOF5-ROBOT))
(not (ABOVEOF6-ROBOT))
)
)
(when
(and
(Flag20-)
)
(and
(ABOVEOF4-ROBOT)
(NOT-ABOVEOF5-ROBOT)
(not (NOT-ABOVEOF4-ROBOT))
(not (ABOVEOF5-ROBOT))
)
)
(when
(and
(Flag19-)
)
(and
(ABOVEOF3-ROBOT)
(NOT-ABOVEOF4-ROBOT)
(not (NOT-ABOVEOF3-ROBOT))
(not (ABOVEOF4-ROBOT))
)
)
(when
(and
(Flag18-)
)
(and
(ABOVEOF2-ROBOT)
(NOT-ABOVEOF3-ROBOT)
(not (NOT-ABOVEOF2-ROBOT))
(not (ABOVEOF3-ROBOT))
)
)
(when
(and
(Flag17-)
)
(and
(ABOVEOF1-ROBOT)
(NOT-ABOVEOF2-ROBOT)
(not (NOT-ABOVEOF1-ROBOT))
(not (ABOVEOF2-ROBOT))
)
)
(when
(and
(Flag16-)
)
(and
(NOT-ABOVEOF1-ROBOT)
(not (ABOVEOF1-ROBOT))
)
)
(when
(and
(Flag15-)
)
(and
(BELOWOF11-ROBOT)
(not (NOT-BELOWOF11-ROBOT))
)
)
(when
(and
(Flag14-)
)
(and
(BELOWOF10-ROBOT)
(not (NOT-BELOWOF10-ROBOT))
)
)
(when
(and
(Flag13-)
)
(and
(BELOWOF9-ROBOT)
(not (NOT-BELOWOF9-ROBOT))
)
)
(when
(and
(Flag12-)
)
(and
(BELOWOF8-ROBOT)
(not (NOT-BELOWOF8-ROBOT))
)
)
(when
(and
(Flag11-)
)
(and
(BELOWOF7-ROBOT)
(not (NOT-BELOWOF7-ROBOT))
)
)
(when
(and
(Flag10-)
)
(and
(BELOWOF6-ROBOT)
(not (NOT-BELOWOF6-ROBOT))
)
)
(when
(and
(Flag9-)
)
(and
(BELOWOF5-ROBOT)
(not (NOT-BELOWOF5-ROBOT))
)
)
(when
(and
(Flag8-)
)
(and
(BELOWOF4-ROBOT)
(not (NOT-BELOWOF4-ROBOT))
)
)
(when
(and
(Flag7-)
)
(and
(BELOWOF3-ROBOT)
(not (NOT-BELOWOF3-ROBOT))
)
)
(when
(and
(Flag6-)
)
(and
(BELOWOF2-ROBOT)
(not (NOT-BELOWOF2-ROBOT))
)
)
(when
(and
(Flag5-)
)
(and
(BELOWOF1-ROBOT)
(not (NOT-BELOWOF1-ROBOT))
)
)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:derived (Flag2-)(and (NOT-CHECKCONSISTENCY-)(NOT-ERROR-)))

(:derived (Flag3-)(and (RIGHTOF2-ROBOT)))

(:derived (Flag3-)(and (LEFTOF11-ROBOT)))

(:derived (Flag14-)(and (BELOWOF11-ROBOT)))

(:derived (Flag15-)(and (BELOWOF11-ROBOT)))

(:derived (Flag132-)(and (LEFTOF11-ROBOT)))

(:derived (Flag133-)(and (LEFTOF11-ROBOT)))

(:derived (Flag134-)(and (RIGHTOF2-ROBOT)))

(:derived (Flag135-)(and (RIGHTOF2-ROBOT)))

(:derived (Flag238-)(and (RIGHTOF2-ROBOT)))

)
