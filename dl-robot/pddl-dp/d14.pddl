(define (domain grounded-SIMPLE-ADL-ROBOT)
(:requirements
:strips
)
(:predicates
(Flag312-)
(Flag185-)
(Flag163-)
(Flag162-)
(Flag161-)
(Flag157-)
(Flag28-)
(Flag4-)
(Flag2-)
(ROW13-ROBOT)
(ROW12-ROBOT)
(ROW11-ROBOT)
(ROW10-ROBOT)
(ROW9-ROBOT)
(ROW8-ROBOT)
(ROW7-ROBOT)
(ROW6-ROBOT)
(ROW5-ROBOT)
(ROW4-ROBOT)
(ROW3-ROBOT)
(ROW2-ROBOT)
(ROW1-ROBOT)
(BELOWOF14-ROBOT)
(NOT-BELOWOF13-ROBOT)
(BELOWOF12-ROBOT)
(BELOWOF11-ROBOT)
(BELOWOF10-ROBOT)
(BELOWOF9-ROBOT)
(BELOWOF8-ROBOT)
(BELOWOF7-ROBOT)
(BELOWOF6-ROBOT)
(BELOWOF5-ROBOT)
(BELOWOF4-ROBOT)
(BELOWOF3-ROBOT)
(BELOWOF2-ROBOT)
(ABOVEOF13-ROBOT)
(ABOVEOF12-ROBOT)
(ABOVEOF11-ROBOT)
(ABOVEOF10-ROBOT)
(ABOVEOF9-ROBOT)
(ABOVEOF8-ROBOT)
(ABOVEOF7-ROBOT)
(ABOVEOF6-ROBOT)
(ABOVEOF5-ROBOT)
(ABOVEOF4-ROBOT)
(ABOVEOF3-ROBOT)
(ABOVEOF2-ROBOT)
(ABOVEOF1-ROBOT)
(CHECKCONSISTENCY-)
(ROW0-ROBOT)
(BELOWOF1-ROBOT)
(Flag159-)
(Flag158-)
(Flag155-)
(Flag154-)
(Flag153-)
(Flag151-)
(Flag150-)
(Flag149-)
(Flag147-)
(Flag146-)
(Flag145-)
(Flag144-)
(Flag142-)
(Flag141-)
(Flag140-)
(Flag139-)
(Flag138-)
(Flag137-)
(Flag135-)
(Flag134-)
(Flag133-)
(Flag132-)
(Flag131-)
(Flag130-)
(Flag129-)
(Flag128-)
(Flag126-)
(Flag125-)
(Flag124-)
(Flag123-)
(Flag122-)
(Flag121-)
(Flag120-)
(Flag119-)
(Flag117-)
(Flag116-)
(Flag115-)
(Flag114-)
(Flag113-)
(Flag112-)
(Flag111-)
(Flag110-)
(Flag109-)
(Flag108-)
(Flag106-)
(Flag105-)
(Flag104-)
(Flag103-)
(Flag102-)
(Flag101-)
(Flag100-)
(Flag99-)
(Flag98-)
(Flag97-)
(Flag96-)
(Flag94-)
(Flag93-)
(Flag92-)
(Flag91-)
(Flag90-)
(Flag89-)
(Flag88-)
(Flag87-)
(Flag86-)
(Flag85-)
(Flag83-)
(Flag82-)
(Flag81-)
(Flag80-)
(Flag79-)
(Flag78-)
(Flag77-)
(Flag76-)
(Flag75-)
(Flag74-)
(Flag73-)
(Flag72-)
(Flag71-)
(Flag69-)
(Flag68-)
(Flag67-)
(Flag66-)
(Flag65-)
(Flag64-)
(Flag63-)
(Flag62-)
(Flag61-)
(Flag60-)
(Flag59-)
(Flag58-)
(Flag57-)
(Flag55-)
(Flag54-)
(Flag53-)
(Flag52-)
(Flag51-)
(Flag50-)
(Flag49-)
(Flag48-)
(Flag47-)
(Flag46-)
(Flag45-)
(Flag44-)
(Flag43-)
(Flag42-)
(Flag41-)
(Flag40-)
(Flag39-)
(Flag38-)
(Flag37-)
(Flag36-)
(Flag35-)
(Flag34-)
(Flag33-)
(Flag32-)
(Flag31-)
(Flag30-)
(Flag29-)
(Flag27-)
(Flag26-)
(Flag25-)
(Flag24-)
(Flag23-)
(Flag22-)
(Flag21-)
(Flag20-)
(Flag19-)
(Flag18-)
(Flag17-)
(Flag16-)
(Flag15-)
(Flag14-)
(Flag13-)
(Flag12-)
(Flag11-)
(Flag10-)
(Flag9-)
(Flag8-)
(Flag7-)
(Flag6-)
(Flag5-)
(Flag3-)
(ERROR-)
(COLUMN13-ROBOT)
(COLUMN12-ROBOT)
(COLUMN11-ROBOT)
(COLUMN10-ROBOT)
(COLUMN9-ROBOT)
(COLUMN8-ROBOT)
(COLUMN7-ROBOT)
(COLUMN6-ROBOT)
(COLUMN5-ROBOT)
(COLUMN4-ROBOT)
(COLUMN3-ROBOT)
(COLUMN2-ROBOT)
(COLUMN1-ROBOT)
(LEFTOF14-ROBOT)
(NOT-LEFTOF13-ROBOT)
(LEFTOF12-ROBOT)
(LEFTOF11-ROBOT)
(LEFTOF10-ROBOT)
(LEFTOF9-ROBOT)
(LEFTOF8-ROBOT)
(LEFTOF7-ROBOT)
(LEFTOF6-ROBOT)
(LEFTOF5-ROBOT)
(LEFTOF4-ROBOT)
(LEFTOF3-ROBOT)
(LEFTOF2-ROBOT)
(RIGHTOF13-ROBOT)
(RIGHTOF12-ROBOT)
(RIGHTOF11-ROBOT)
(RIGHTOF10-ROBOT)
(RIGHTOF9-ROBOT)
(RIGHTOF8-ROBOT)
(RIGHTOF7-ROBOT)
(RIGHTOF6-ROBOT)
(RIGHTOF5-ROBOT)
(RIGHTOF4-ROBOT)
(RIGHTOF3-ROBOT)
(RIGHTOF1-ROBOT)
(COLUMN0-ROBOT)
(NOT-RIGHTOF2-ROBOT)
(RIGHTOF0-ROBOT)
(LEFTOF1-ROBOT)
(Flag313-)
(Flag310-)
(Flag309-)
(Flag308-)
(Flag306-)
(Flag305-)
(Flag304-)
(Flag302-)
(Flag301-)
(Flag300-)
(Flag299-)
(Flag297-)
(Flag296-)
(Flag295-)
(Flag294-)
(Flag293-)
(Flag291-)
(Flag290-)
(Flag289-)
(Flag288-)
(Flag287-)
(Flag286-)
(Flag284-)
(Flag283-)
(Flag282-)
(Flag281-)
(Flag280-)
(Flag279-)
(Flag278-)
(Flag277-)
(Flag275-)
(Flag274-)
(Flag273-)
(Flag272-)
(Flag271-)
(Flag270-)
(Flag269-)
(Flag268-)
(Flag267-)
(Flag266-)
(Flag264-)
(Flag263-)
(Flag262-)
(Flag261-)
(Flag260-)
(Flag259-)
(Flag258-)
(Flag257-)
(Flag256-)
(Flag254-)
(Flag253-)
(Flag252-)
(Flag251-)
(Flag250-)
(Flag249-)
(Flag248-)
(Flag247-)
(Flag246-)
(Flag245-)
(Flag244-)
(Flag243-)
(Flag241-)
(Flag240-)
(Flag239-)
(Flag238-)
(Flag237-)
(Flag236-)
(Flag235-)
(Flag234-)
(Flag233-)
(Flag232-)
(Flag231-)
(Flag230-)
(Flag229-)
(Flag227-)
(Flag226-)
(Flag225-)
(Flag224-)
(Flag223-)
(Flag222-)
(Flag221-)
(Flag220-)
(Flag219-)
(Flag218-)
(Flag217-)
(Flag216-)
(Flag214-)
(Flag213-)
(Flag212-)
(Flag211-)
(Flag210-)
(Flag209-)
(Flag208-)
(Flag207-)
(Flag206-)
(Flag205-)
(Flag204-)
(Flag203-)
(Flag202-)
(Flag201-)
(Flag199-)
(Flag198-)
(Flag197-)
(Flag196-)
(Flag195-)
(Flag194-)
(Flag193-)
(Flag192-)
(Flag191-)
(Flag190-)
(Flag189-)
(Flag188-)
(Flag187-)
(Flag186-)
(Flag184-)
(Flag183-)
(Flag182-)
(Flag181-)
(Flag180-)
(Flag179-)
(Flag178-)
(Flag177-)
(Flag176-)
(Flag175-)
(Flag174-)
(Flag173-)
(Flag172-)
(Flag171-)
(Flag170-)
(Flag169-)
(Flag168-)
(Flag167-)
(Flag166-)
(Flag165-)
(Flag164-)
(Flag160-)
(Flag156-)
(Flag152-)
(Flag148-)
(Flag143-)
(Flag136-)
(Flag127-)
(Flag118-)
(Flag107-)
(Flag95-)
(Flag84-)
(Flag70-)
(Flag56-)
(Flag1-)
(Flag314-)
(Flag311-)
(Flag307-)
(Flag303-)
(Flag298-)
(Flag292-)
(Flag285-)
(Flag276-)
(Flag265-)
(Flag255-)
(Flag242-)
(Flag228-)
(Flag215-)
(Flag200-)
(NOT-COLUMN12-ROBOT)
(NOT-COLUMN11-ROBOT)
(NOT-COLUMN10-ROBOT)
(NOT-COLUMN9-ROBOT)
(NOT-COLUMN8-ROBOT)
(NOT-COLUMN7-ROBOT)
(NOT-COLUMN6-ROBOT)
(NOT-COLUMN5-ROBOT)
(NOT-COLUMN4-ROBOT)
(NOT-COLUMN3-ROBOT)
(NOT-COLUMN2-ROBOT)
(NOT-COLUMN1-ROBOT)
(NOT-COLUMN0-ROBOT)
(NOT-RIGHTOF12-ROBOT)
(NOT-RIGHTOF11-ROBOT)
(NOT-RIGHTOF10-ROBOT)
(NOT-RIGHTOF9-ROBOT)
(NOT-RIGHTOF8-ROBOT)
(NOT-RIGHTOF7-ROBOT)
(NOT-RIGHTOF6-ROBOT)
(NOT-RIGHTOF5-ROBOT)
(NOT-RIGHTOF4-ROBOT)
(NOT-RIGHTOF3-ROBOT)
(NOT-RIGHTOF1-ROBOT)
(RIGHTOF2-ROBOT)
(NOT-RIGHTOF0-ROBOT)
(NOT-LEFTOF12-ROBOT)
(NOT-LEFTOF11-ROBOT)
(NOT-LEFTOF10-ROBOT)
(NOT-LEFTOF9-ROBOT)
(NOT-LEFTOF8-ROBOT)
(NOT-LEFTOF7-ROBOT)
(NOT-LEFTOF6-ROBOT)
(NOT-LEFTOF5-ROBOT)
(NOT-LEFTOF4-ROBOT)
(NOT-LEFTOF3-ROBOT)
(NOT-LEFTOF2-ROBOT)
(NOT-LEFTOF1-ROBOT)
(NOT-CHECKCONSISTENCY-)
(NOT-COLUMN13-ROBOT)
(NOT-LEFTOF14-ROBOT)
(LEFTOF13-ROBOT)
(NOT-RIGHTOF13-ROBOT)
(NOT-ERROR-)
(NOT-ROW12-ROBOT)
(NOT-ROW11-ROBOT)
(NOT-ROW10-ROBOT)
(NOT-ROW9-ROBOT)
(NOT-ROW8-ROBOT)
(NOT-ROW7-ROBOT)
(NOT-ROW6-ROBOT)
(NOT-ROW5-ROBOT)
(NOT-ROW4-ROBOT)
(NOT-ROW3-ROBOT)
(NOT-ROW2-ROBOT)
(NOT-ROW1-ROBOT)
(NOT-ROW0-ROBOT)
(NOT-ABOVEOF12-ROBOT)
(NOT-ABOVEOF11-ROBOT)
(NOT-ABOVEOF10-ROBOT)
(NOT-ABOVEOF9-ROBOT)
(NOT-ABOVEOF8-ROBOT)
(NOT-ABOVEOF7-ROBOT)
(NOT-ABOVEOF6-ROBOT)
(NOT-ABOVEOF5-ROBOT)
(NOT-ABOVEOF4-ROBOT)
(NOT-ABOVEOF3-ROBOT)
(NOT-ABOVEOF2-ROBOT)
(NOT-ABOVEOF1-ROBOT)
(NOT-BELOWOF12-ROBOT)
(NOT-BELOWOF11-ROBOT)
(NOT-BELOWOF10-ROBOT)
(NOT-BELOWOF9-ROBOT)
(NOT-BELOWOF8-ROBOT)
(NOT-BELOWOF7-ROBOT)
(NOT-BELOWOF6-ROBOT)
(NOT-BELOWOF5-ROBOT)
(NOT-BELOWOF4-ROBOT)
(NOT-BELOWOF3-ROBOT)
(NOT-BELOWOF2-ROBOT)
(NOT-BELOWOF1-ROBOT)
(NOT-ROW13-ROBOT)
(NOT-BELOWOF14-ROBOT)
(BELOWOF13-ROBOT)
(NOT-ABOVEOF13-ROBOT)
)
(:derived (Flag200-)(and (Flag186-)))

(:derived (Flag200-)(and (Flag187-)))

(:derived (Flag200-)(and (Flag188-)))

(:derived (Flag200-)(and (Flag189-)))

(:derived (Flag200-)(and (Flag190-)))

(:derived (Flag200-)(and (Flag191-)))

(:derived (Flag200-)(and (Flag192-)))

(:derived (Flag200-)(and (Flag193-)))

(:derived (Flag200-)(and (Flag194-)))

(:derived (Flag200-)(and (Flag195-)))

(:derived (Flag200-)(and (Flag196-)))

(:derived (Flag200-)(and (Flag197-)))

(:derived (Flag200-)(and (Flag198-)))

(:derived (Flag200-)(and (Flag199-)))

(:derived (Flag215-)(and (Flag201-)))

(:derived (Flag215-)(and (Flag202-)))

(:derived (Flag215-)(and (Flag189-)))

(:derived (Flag215-)(and (Flag192-)))

(:derived (Flag215-)(and (Flag199-)))

(:derived (Flag215-)(and (Flag203-)))

(:derived (Flag215-)(and (Flag204-)))

(:derived (Flag215-)(and (Flag191-)))

(:derived (Flag215-)(and (Flag205-)))

(:derived (Flag215-)(and (Flag206-)))

(:derived (Flag215-)(and (Flag196-)))

(:derived (Flag215-)(and (Flag207-)))

(:derived (Flag215-)(and (Flag186-)))

(:derived (Flag215-)(and (Flag208-)))

(:derived (Flag215-)(and (Flag187-)))

(:derived (Flag215-)(and (Flag188-)))

(:derived (Flag215-)(and (Flag209-)))

(:derived (Flag215-)(and (Flag194-)))

(:derived (Flag215-)(and (Flag197-)))

(:derived (Flag215-)(and (Flag190-)))

(:derived (Flag215-)(and (Flag210-)))

(:derived (Flag215-)(and (Flag211-)))

(:derived (Flag215-)(and (Flag212-)))

(:derived (Flag215-)(and (Flag195-)))

(:derived (Flag215-)(and (Flag198-)))

(:derived (Flag215-)(and (Flag213-)))

(:derived (Flag215-)(and (Flag214-)))

(:derived (Flag228-)(and (Flag201-)))

(:derived (Flag228-)(and (Flag202-)))

(:derived (Flag228-)(and (Flag216-)))

(:derived (Flag228-)(and (Flag189-)))

(:derived (Flag228-)(and (Flag192-)))

(:derived (Flag228-)(and (Flag217-)))

(:derived (Flag228-)(and (Flag199-)))

(:derived (Flag228-)(and (Flag204-)))

(:derived (Flag228-)(and (Flag188-)))

(:derived (Flag228-)(and (Flag206-)))

(:derived (Flag228-)(and (Flag218-)))

(:derived (Flag228-)(and (Flag196-)))

(:derived (Flag228-)(and (Flag207-)))

(:derived (Flag228-)(and (Flag194-)))

(:derived (Flag228-)(and (Flag219-)))

(:derived (Flag228-)(and (Flag186-)))

(:derived (Flag228-)(and (Flag208-)))

(:derived (Flag228-)(and (Flag187-)))

(:derived (Flag228-)(and (Flag220-)))

(:derived (Flag228-)(and (Flag221-)))

(:derived (Flag228-)(and (Flag205-)))

(:derived (Flag228-)(and (Flag209-)))

(:derived (Flag228-)(and (Flag222-)))

(:derived (Flag228-)(and (Flag197-)))

(:derived (Flag228-)(and (Flag223-)))

(:derived (Flag228-)(and (Flag224-)))

(:derived (Flag228-)(and (Flag225-)))

(:derived (Flag228-)(and (Flag190-)))

(:derived (Flag228-)(and (Flag210-)))

(:derived (Flag228-)(and (Flag211-)))

(:derived (Flag228-)(and (Flag212-)))

(:derived (Flag228-)(and (Flag195-)))

(:derived (Flag228-)(and (Flag198-)))

(:derived (Flag228-)(and (Flag213-)))

(:derived (Flag228-)(and (Flag226-)))

(:derived (Flag228-)(and (Flag214-)))

(:derived (Flag228-)(and (Flag227-)))

(:derived (Flag242-)(and (Flag201-)))

(:derived (Flag242-)(and (Flag202-)))

(:derived (Flag242-)(and (Flag216-)))

(:derived (Flag242-)(and (Flag189-)))

(:derived (Flag242-)(and (Flag192-)))

(:derived (Flag242-)(and (Flag229-)))

(:derived (Flag242-)(and (Flag217-)))

(:derived (Flag242-)(and (Flag199-)))

(:derived (Flag242-)(and (Flag230-)))

(:derived (Flag242-)(and (Flag231-)))

(:derived (Flag242-)(and (Flag204-)))

(:derived (Flag242-)(and (Flag221-)))

(:derived (Flag242-)(and (Flag188-)))

(:derived (Flag242-)(and (Flag206-)))

(:derived (Flag242-)(and (Flag218-)))

(:derived (Flag242-)(and (Flag196-)))

(:derived (Flag242-)(and (Flag207-)))

(:derived (Flag242-)(and (Flag194-)))

(:derived (Flag242-)(and (Flag219-)))

(:derived (Flag242-)(and (Flag186-)))

(:derived (Flag242-)(and (Flag208-)))

(:derived (Flag242-)(and (Flag187-)))

(:derived (Flag242-)(and (Flag220-)))

(:derived (Flag242-)(and (Flag232-)))

(:derived (Flag242-)(and (Flag205-)))

(:derived (Flag242-)(and (Flag209-)))

(:derived (Flag242-)(and (Flag233-)))

(:derived (Flag242-)(and (Flag222-)))

(:derived (Flag242-)(and (Flag197-)))

(:derived (Flag242-)(and (Flag224-)))

(:derived (Flag242-)(and (Flag234-)))

(:derived (Flag242-)(and (Flag235-)))

(:derived (Flag242-)(and (Flag225-)))

(:derived (Flag242-)(and (Flag236-)))

(:derived (Flag242-)(and (Flag190-)))

(:derived (Flag242-)(and (Flag210-)))

(:derived (Flag242-)(and (Flag237-)))

(:derived (Flag242-)(and (Flag238-)))

(:derived (Flag242-)(and (Flag211-)))

(:derived (Flag242-)(and (Flag239-)))

(:derived (Flag242-)(and (Flag198-)))

(:derived (Flag242-)(and (Flag240-)))

(:derived (Flag242-)(and (Flag241-)))

(:derived (Flag242-)(and (Flag213-)))

(:derived (Flag242-)(and (Flag226-)))

(:derived (Flag242-)(and (Flag214-)))

(:derived (Flag255-)(and (Flag243-)))

(:derived (Flag255-)(and (Flag201-)))

(:derived (Flag255-)(and (Flag202-)))

(:derived (Flag255-)(and (Flag216-)))

(:derived (Flag255-)(and (Flag189-)))

(:derived (Flag255-)(and (Flag229-)))

(:derived (Flag255-)(and (Flag244-)))

(:derived (Flag255-)(and (Flag217-)))

(:derived (Flag255-)(and (Flag199-)))

(:derived (Flag255-)(and (Flag245-)))

(:derived (Flag255-)(and (Flag230-)))

(:derived (Flag255-)(and (Flag246-)))

(:derived (Flag255-)(and (Flag231-)))

(:derived (Flag255-)(and (Flag204-)))

(:derived (Flag255-)(and (Flag247-)))

(:derived (Flag255-)(and (Flag205-)))

(:derived (Flag255-)(and (Flag206-)))

(:derived (Flag255-)(and (Flag218-)))

(:derived (Flag255-)(and (Flag196-)))

(:derived (Flag255-)(and (Flag248-)))

(:derived (Flag255-)(and (Flag249-)))

(:derived (Flag255-)(and (Flag250-)))

(:derived (Flag255-)(and (Flag186-)))

(:derived (Flag255-)(and (Flag251-)))

(:derived (Flag255-)(and (Flag208-)))

(:derived (Flag255-)(and (Flag187-)))

(:derived (Flag255-)(and (Flag220-)))

(:derived (Flag255-)(and (Flag221-)))

(:derived (Flag255-)(and (Flag188-)))

(:derived (Flag255-)(and (Flag209-)))

(:derived (Flag255-)(and (Flag233-)))

(:derived (Flag255-)(and (Flag252-)))

(:derived (Flag255-)(and (Flag253-)))

(:derived (Flag255-)(and (Flag222-)))

(:derived (Flag255-)(and (Flag219-)))

(:derived (Flag255-)(and (Flag197-)))

(:derived (Flag255-)(and (Flag224-)))

(:derived (Flag255-)(and (Flag234-)))

(:derived (Flag255-)(and (Flag235-)))

(:derived (Flag255-)(and (Flag211-)))

(:derived (Flag255-)(and (Flag225-)))

(:derived (Flag255-)(and (Flag236-)))

(:derived (Flag255-)(and (Flag190-)))

(:derived (Flag255-)(and (Flag210-)))

(:derived (Flag255-)(and (Flag237-)))

(:derived (Flag255-)(and (Flag238-)))

(:derived (Flag255-)(and (Flag194-)))

(:derived (Flag255-)(and (Flag239-)))

(:derived (Flag255-)(and (Flag198-)))

(:derived (Flag255-)(and (Flag254-)))

(:derived (Flag255-)(and (Flag241-)))

(:derived (Flag255-)(and (Flag213-)))

(:derived (Flag255-)(and (Flag226-)))

(:derived (Flag255-)(and (Flag214-)))

(:derived (Flag265-)(and (Flag243-)))

(:derived (Flag265-)(and (Flag201-)))

(:derived (Flag265-)(and (Flag202-)))

(:derived (Flag265-)(and (Flag244-)))

(:derived (Flag265-)(and (Flag189-)))

(:derived (Flag265-)(and (Flag229-)))

(:derived (Flag265-)(and (Flag210-)))

(:derived (Flag265-)(and (Flag226-)))

(:derived (Flag265-)(and (Flag199-)))

(:derived (Flag265-)(and (Flag246-)))

(:derived (Flag265-)(and (Flag216-)))

(:derived (Flag265-)(and (Flag231-)))

(:derived (Flag265-)(and (Flag204-)))

(:derived (Flag265-)(and (Flag247-)))

(:derived (Flag265-)(and (Flag205-)))

(:derived (Flag265-)(and (Flag206-)))

(:derived (Flag265-)(and (Flag256-)))

(:derived (Flag265-)(and (Flag218-)))

(:derived (Flag265-)(and (Flag196-)))

(:derived (Flag265-)(and (Flag257-)))

(:derived (Flag265-)(and (Flag219-)))

(:derived (Flag265-)(and (Flag258-)))

(:derived (Flag265-)(and (Flag249-)))

(:derived (Flag265-)(and (Flag250-)))

(:derived (Flag265-)(and (Flag251-)))

(:derived (Flag265-)(and (Flag208-)))

(:derived (Flag265-)(and (Flag187-)))

(:derived (Flag265-)(and (Flag220-)))

(:derived (Flag265-)(and (Flag221-)))

(:derived (Flag265-)(and (Flag188-)))

(:derived (Flag265-)(and (Flag209-)))

(:derived (Flag265-)(and (Flag233-)))

(:derived (Flag265-)(and (Flag252-)))

(:derived (Flag265-)(and (Flag253-)))

(:derived (Flag265-)(and (Flag222-)))

(:derived (Flag265-)(and (Flag259-)))

(:derived (Flag265-)(and (Flag197-)))

(:derived (Flag265-)(and (Flag224-)))

(:derived (Flag265-)(and (Flag234-)))

(:derived (Flag265-)(and (Flag235-)))

(:derived (Flag265-)(and (Flag260-)))

(:derived (Flag265-)(and (Flag225-)))

(:derived (Flag265-)(and (Flag261-)))

(:derived (Flag265-)(and (Flag262-)))

(:derived (Flag265-)(and (Flag236-)))

(:derived (Flag265-)(and (Flag190-)))

(:derived (Flag265-)(and (Flag263-)))

(:derived (Flag265-)(and (Flag237-)))

(:derived (Flag265-)(and (Flag238-)))

(:derived (Flag265-)(and (Flag211-)))

(:derived (Flag265-)(and (Flag239-)))

(:derived (Flag265-)(and (Flag198-)))

(:derived (Flag265-)(and (Flag254-)))

(:derived (Flag265-)(and (Flag241-)))

(:derived (Flag265-)(and (Flag213-)))

(:derived (Flag265-)(and (Flag245-)))

(:derived (Flag265-)(and (Flag264-)))

(:derived (Flag276-)(and (Flag243-)))

(:derived (Flag276-)(and (Flag201-)))

(:derived (Flag276-)(and (Flag202-)))

(:derived (Flag276-)(and (Flag244-)))

(:derived (Flag276-)(and (Flag189-)))

(:derived (Flag276-)(and (Flag266-)))

(:derived (Flag276-)(and (Flag237-)))

(:derived (Flag276-)(and (Flag267-)))

(:derived (Flag276-)(and (Flag268-)))

(:derived (Flag276-)(and (Flag229-)))

(:derived (Flag276-)(and (Flag210-)))

(:derived (Flag276-)(and (Flag226-)))

(:derived (Flag276-)(and (Flag199-)))

(:derived (Flag276-)(and (Flag187-)))

(:derived (Flag276-)(and (Flag246-)))

(:derived (Flag276-)(and (Flag216-)))

(:derived (Flag276-)(and (Flag231-)))

(:derived (Flag276-)(and (Flag221-)))

(:derived (Flag276-)(and (Flag269-)))

(:derived (Flag276-)(and (Flag205-)))

(:derived (Flag276-)(and (Flag256-)))

(:derived (Flag276-)(and (Flag218-)))

(:derived (Flag276-)(and (Flag196-)))

(:derived (Flag276-)(and (Flag257-)))

(:derived (Flag276-)(and (Flag219-)))

(:derived (Flag276-)(and (Flag258-)))

(:derived (Flag276-)(and (Flag249-)))

(:derived (Flag276-)(and (Flag250-)))

(:derived (Flag276-)(and (Flag251-)))

(:derived (Flag276-)(and (Flag208-)))

(:derived (Flag276-)(and (Flag270-)))

(:derived (Flag276-)(and (Flag220-)))

(:derived (Flag276-)(and (Flag247-)))

(:derived (Flag276-)(and (Flag188-)))

(:derived (Flag276-)(and (Flag209-)))

(:derived (Flag276-)(and (Flag271-)))

(:derived (Flag276-)(and (Flag252-)))

(:derived (Flag276-)(and (Flag253-)))

(:derived (Flag276-)(and (Flag233-)))

(:derived (Flag276-)(and (Flag197-)))

(:derived (Flag276-)(and (Flag224-)))

(:derived (Flag276-)(and (Flag234-)))

(:derived (Flag276-)(and (Flag235-)))

(:derived (Flag276-)(and (Flag260-)))

(:derived (Flag276-)(and (Flag225-)))

(:derived (Flag276-)(and (Flag272-)))

(:derived (Flag276-)(and (Flag273-)))

(:derived (Flag276-)(and (Flag274-)))

(:derived (Flag276-)(and (Flag261-)))

(:derived (Flag276-)(and (Flag236-)))

(:derived (Flag276-)(and (Flag190-)))

(:derived (Flag276-)(and (Flag263-)))

(:derived (Flag276-)(and (Flag275-)))

(:derived (Flag276-)(and (Flag211-)))

(:derived (Flag276-)(and (Flag239-)))

(:derived (Flag276-)(and (Flag198-)))

(:derived (Flag276-)(and (Flag241-)))

(:derived (Flag276-)(and (Flag213-)))

(:derived (Flag276-)(and (Flag245-)))

(:derived (Flag276-)(and (Flag264-)))

(:derived (Flag285-)(and (Flag243-)))

(:derived (Flag285-)(and (Flag201-)))

(:derived (Flag285-)(and (Flag277-)))

(:derived (Flag285-)(and (Flag244-)))

(:derived (Flag285-)(and (Flag189-)))

(:derived (Flag285-)(and (Flag251-)))

(:derived (Flag285-)(and (Flag266-)))

(:derived (Flag285-)(and (Flag267-)))

(:derived (Flag285-)(and (Flag229-)))

(:derived (Flag285-)(and (Flag210-)))

(:derived (Flag285-)(and (Flag208-)))

(:derived (Flag285-)(and (Flag278-)))

(:derived (Flag285-)(and (Flag199-)))

(:derived (Flag285-)(and (Flag245-)))

(:derived (Flag285-)(and (Flag279-)))

(:derived (Flag285-)(and (Flag246-)))

(:derived (Flag285-)(and (Flag231-)))

(:derived (Flag285-)(and (Flag221-)))

(:derived (Flag285-)(and (Flag269-)))

(:derived (Flag285-)(and (Flag188-)))

(:derived (Flag285-)(and (Flag202-)))

(:derived (Flag285-)(and (Flag256-)))

(:derived (Flag285-)(and (Flag218-)))

(:derived (Flag285-)(and (Flag196-)))

(:derived (Flag285-)(and (Flag257-)))

(:derived (Flag285-)(and (Flag219-)))

(:derived (Flag285-)(and (Flag258-)))

(:derived (Flag285-)(and (Flag249-)))

(:derived (Flag285-)(and (Flag280-)))

(:derived (Flag285-)(and (Flag281-)))

(:derived (Flag285-)(and (Flag282-)))

(:derived (Flag285-)(and (Flag283-)))

(:derived (Flag285-)(and (Flag187-)))

(:derived (Flag285-)(and (Flag220-)))

(:derived (Flag285-)(and (Flag247-)))

(:derived (Flag285-)(and (Flag205-)))

(:derived (Flag285-)(and (Flag209-)))

(:derived (Flag285-)(and (Flag271-)))

(:derived (Flag285-)(and (Flag252-)))

(:derived (Flag285-)(and (Flag253-)))

(:derived (Flag285-)(and (Flag233-)))

(:derived (Flag285-)(and (Flag197-)))

(:derived (Flag285-)(and (Flag224-)))

(:derived (Flag285-)(and (Flag234-)))

(:derived (Flag285-)(and (Flag235-)))

(:derived (Flag285-)(and (Flag260-)))

(:derived (Flag285-)(and (Flag225-)))

(:derived (Flag285-)(and (Flag272-)))

(:derived (Flag285-)(and (Flag274-)))

(:derived (Flag285-)(and (Flag261-)))

(:derived (Flag285-)(and (Flag236-)))

(:derived (Flag285-)(and (Flag190-)))

(:derived (Flag285-)(and (Flag263-)))

(:derived (Flag285-)(and (Flag275-)))

(:derived (Flag285-)(and (Flag211-)))

(:derived (Flag285-)(and (Flag239-)))

(:derived (Flag285-)(and (Flag284-)))

(:derived (Flag285-)(and (Flag241-)))

(:derived (Flag285-)(and (Flag226-)))

(:derived (Flag285-)(and (Flag264-)))

(:derived (Flag292-)(and (Flag243-)))

(:derived (Flag292-)(and (Flag201-)))

(:derived (Flag292-)(and (Flag202-)))

(:derived (Flag292-)(and (Flag189-)))

(:derived (Flag292-)(and (Flag251-)))

(:derived (Flag292-)(and (Flag266-)))

(:derived (Flag292-)(and (Flag267-)))

(:derived (Flag292-)(and (Flag229-)))

(:derived (Flag292-)(and (Flag210-)))

(:derived (Flag292-)(and (Flag208-)))

(:derived (Flag292-)(and (Flag278-)))

(:derived (Flag292-)(and (Flag199-)))

(:derived (Flag292-)(and (Flag245-)))

(:derived (Flag292-)(and (Flag286-)))

(:derived (Flag292-)(and (Flag287-)))

(:derived (Flag292-)(and (Flag279-)))

(:derived (Flag292-)(and (Flag246-)))

(:derived (Flag292-)(and (Flag247-)))

(:derived (Flag292-)(and (Flag288-)))

(:derived (Flag292-)(and (Flag269-)))

(:derived (Flag292-)(and (Flag256-)))

(:derived (Flag292-)(and (Flag257-)))

(:derived (Flag292-)(and (Flag196-)))

(:derived (Flag292-)(and (Flag219-)))

(:derived (Flag292-)(and (Flag258-)))

(:derived (Flag292-)(and (Flag249-)))

(:derived (Flag292-)(and (Flag289-)))

(:derived (Flag292-)(and (Flag280-)))

(:derived (Flag292-)(and (Flag281-)))

(:derived (Flag292-)(and (Flag283-)))

(:derived (Flag292-)(and (Flag187-)))

(:derived (Flag292-)(and (Flag220-)))

(:derived (Flag292-)(and (Flag221-)))

(:derived (Flag292-)(and (Flag209-)))

(:derived (Flag292-)(and (Flag271-)))

(:derived (Flag292-)(and (Flag252-)))

(:derived (Flag292-)(and (Flag290-)))

(:derived (Flag292-)(and (Flag253-)))

(:derived (Flag292-)(and (Flag291-)))

(:derived (Flag292-)(and (Flag197-)))

(:derived (Flag292-)(and (Flag224-)))

(:derived (Flag292-)(and (Flag234-)))

(:derived (Flag292-)(and (Flag235-)))

(:derived (Flag292-)(and (Flag225-)))

(:derived (Flag292-)(and (Flag272-)))

(:derived (Flag292-)(and (Flag274-)))

(:derived (Flag292-)(and (Flag261-)))

(:derived (Flag292-)(and (Flag236-)))

(:derived (Flag292-)(and (Flag190-)))

(:derived (Flag292-)(and (Flag263-)))

(:derived (Flag292-)(and (Flag275-)))

(:derived (Flag292-)(and (Flag211-)))

(:derived (Flag292-)(and (Flag239-)))

(:derived (Flag292-)(and (Flag284-)))

(:derived (Flag292-)(and (Flag241-)))

(:derived (Flag292-)(and (Flag226-)))

(:derived (Flag292-)(and (Flag264-)))

(:derived (Flag298-)(and (Flag243-)))

(:derived (Flag298-)(and (Flag201-)))

(:derived (Flag298-)(and (Flag202-)))

(:derived (Flag298-)(and (Flag189-)))

(:derived (Flag298-)(and (Flag266-)))

(:derived (Flag298-)(and (Flag293-)))

(:derived (Flag298-)(and (Flag294-)))

(:derived (Flag298-)(and (Flag295-)))

(:derived (Flag298-)(and (Flag229-)))

(:derived (Flag298-)(and (Flag210-)))

(:derived (Flag298-)(and (Flag208-)))

(:derived (Flag298-)(and (Flag278-)))

(:derived (Flag298-)(and (Flag296-)))

(:derived (Flag298-)(and (Flag199-)))

(:derived (Flag298-)(and (Flag245-)))

(:derived (Flag298-)(and (Flag286-)))

(:derived (Flag298-)(and (Flag279-)))

(:derived (Flag298-)(and (Flag246-)))

(:derived (Flag298-)(and (Flag247-)))

(:derived (Flag298-)(and (Flag288-)))

(:derived (Flag298-)(and (Flag269-)))

(:derived (Flag298-)(and (Flag256-)))

(:derived (Flag298-)(and (Flag196-)))

(:derived (Flag298-)(and (Flag219-)))

(:derived (Flag298-)(and (Flag258-)))

(:derived (Flag298-)(and (Flag289-)))

(:derived (Flag298-)(and (Flag280-)))

(:derived (Flag298-)(and (Flag251-)))

(:derived (Flag298-)(and (Flag283-)))

(:derived (Flag298-)(and (Flag187-)))

(:derived (Flag298-)(and (Flag220-)))

(:derived (Flag298-)(and (Flag271-)))

(:derived (Flag298-)(and (Flag209-)))

(:derived (Flag298-)(and (Flag297-)))

(:derived (Flag298-)(and (Flag252-)))

(:derived (Flag298-)(and (Flag291-)))

(:derived (Flag298-)(and (Flag290-)))

(:derived (Flag298-)(and (Flag224-)))

(:derived (Flag298-)(and (Flag234-)))

(:derived (Flag298-)(and (Flag235-)))

(:derived (Flag298-)(and (Flag225-)))

(:derived (Flag298-)(and (Flag272-)))

(:derived (Flag298-)(and (Flag274-)))

(:derived (Flag298-)(and (Flag261-)))

(:derived (Flag298-)(and (Flag236-)))

(:derived (Flag298-)(and (Flag190-)))

(:derived (Flag298-)(and (Flag263-)))

(:derived (Flag298-)(and (Flag275-)))

(:derived (Flag298-)(and (Flag239-)))

(:derived (Flag298-)(and (Flag284-)))

(:derived (Flag298-)(and (Flag241-)))

(:derived (Flag298-)(and (Flag226-)))

(:derived (Flag298-)(and (Flag264-)))

(:derived (Flag303-)(and (Flag210-)))

(:derived (Flag303-)(and (Flag201-)))

(:derived (Flag303-)(and (Flag266-)))

(:derived (Flag303-)(and (Flag299-)))

(:derived (Flag303-)(and (Flag294-)))

(:derived (Flag303-)(and (Flag295-)))

(:derived (Flag303-)(and (Flag229-)))

(:derived (Flag303-)(and (Flag208-)))

(:derived (Flag303-)(and (Flag293-)))

(:derived (Flag303-)(and (Flag296-)))

(:derived (Flag303-)(and (Flag199-)))

(:derived (Flag303-)(and (Flag245-)))

(:derived (Flag303-)(and (Flag286-)))

(:derived (Flag303-)(and (Flag279-)))

(:derived (Flag303-)(and (Flag246-)))

(:derived (Flag303-)(and (Flag269-)))

(:derived (Flag303-)(and (Flag256-)))

(:derived (Flag303-)(and (Flag196-)))

(:derived (Flag303-)(and (Flag219-)))

(:derived (Flag303-)(and (Flag258-)))

(:derived (Flag303-)(and (Flag289-)))

(:derived (Flag303-)(and (Flag280-)))

(:derived (Flag303-)(and (Flag251-)))

(:derived (Flag303-)(and (Flag283-)))

(:derived (Flag303-)(and (Flag187-)))

(:derived (Flag303-)(and (Flag220-)))

(:derived (Flag303-)(and (Flag247-)))

(:derived (Flag303-)(and (Flag209-)))

(:derived (Flag303-)(and (Flag271-)))

(:derived (Flag303-)(and (Flag252-)))

(:derived (Flag303-)(and (Flag291-)))

(:derived (Flag303-)(and (Flag300-)))

(:derived (Flag303-)(and (Flag290-)))

(:derived (Flag303-)(and (Flag224-)))

(:derived (Flag303-)(and (Flag234-)))

(:derived (Flag303-)(and (Flag225-)))

(:derived (Flag303-)(and (Flag274-)))

(:derived (Flag303-)(and (Flag261-)))

(:derived (Flag303-)(and (Flag190-)))

(:derived (Flag303-)(and (Flag263-)))

(:derived (Flag303-)(and (Flag275-)))

(:derived (Flag303-)(and (Flag301-)))

(:derived (Flag303-)(and (Flag239-)))

(:derived (Flag303-)(and (Flag284-)))

(:derived (Flag303-)(and (Flag241-)))

(:derived (Flag303-)(and (Flag302-)))

(:derived (Flag307-)(and (Flag210-)))

(:derived (Flag307-)(and (Flag201-)))

(:derived (Flag307-)(and (Flag266-)))

(:derived (Flag307-)(and (Flag299-)))

(:derived (Flag307-)(and (Flag294-)))

(:derived (Flag307-)(and (Flag229-)))

(:derived (Flag307-)(and (Flag304-)))

(:derived (Flag307-)(and (Flag293-)))

(:derived (Flag307-)(and (Flag296-)))

(:derived (Flag307-)(and (Flag286-)))

(:derived (Flag307-)(and (Flag279-)))

(:derived (Flag307-)(and (Flag246-)))

(:derived (Flag307-)(and (Flag269-)))

(:derived (Flag307-)(and (Flag256-)))

(:derived (Flag307-)(and (Flag196-)))

(:derived (Flag307-)(and (Flag219-)))

(:derived (Flag307-)(and (Flag258-)))

(:derived (Flag307-)(and (Flag305-)))

(:derived (Flag307-)(and (Flag283-)))

(:derived (Flag307-)(and (Flag187-)))

(:derived (Flag307-)(and (Flag209-)))

(:derived (Flag307-)(and (Flag271-)))

(:derived (Flag307-)(and (Flag252-)))

(:derived (Flag307-)(and (Flag291-)))

(:derived (Flag307-)(and (Flag300-)))

(:derived (Flag307-)(and (Flag290-)))

(:derived (Flag307-)(and (Flag224-)))

(:derived (Flag307-)(and (Flag234-)))

(:derived (Flag307-)(and (Flag225-)))

(:derived (Flag307-)(and (Flag274-)))

(:derived (Flag307-)(and (Flag306-)))

(:derived (Flag307-)(and (Flag190-)))

(:derived (Flag307-)(and (Flag263-)))

(:derived (Flag307-)(and (Flag245-)))

(:derived (Flag307-)(and (Flag239-)))

(:derived (Flag307-)(and (Flag284-)))

(:derived (Flag307-)(and (Flag241-)))

(:derived (Flag307-)(and (Flag302-)))

(:derived (Flag311-)(and (Flag201-)))

(:derived (Flag311-)(and (Flag229-)))

(:derived (Flag311-)(and (Flag293-)))

(:derived (Flag311-)(and (Flag296-)))

(:derived (Flag311-)(and (Flag286-)))

(:derived (Flag311-)(and (Flag279-)))

(:derived (Flag311-)(and (Flag246-)))

(:derived (Flag311-)(and (Flag269-)))

(:derived (Flag311-)(and (Flag219-)))

(:derived (Flag311-)(and (Flag283-)))

(:derived (Flag311-)(and (Flag187-)))

(:derived (Flag311-)(and (Flag271-)))

(:derived (Flag311-)(and (Flag209-)))

(:derived (Flag311-)(and (Flag308-)))

(:derived (Flag311-)(and (Flag252-)))

(:derived (Flag311-)(and (Flag291-)))

(:derived (Flag311-)(and (Flag300-)))

(:derived (Flag311-)(and (Flag234-)))

(:derived (Flag311-)(and (Flag225-)))

(:derived (Flag311-)(and (Flag274-)))

(:derived (Flag311-)(and (Flag306-)))

(:derived (Flag311-)(and (Flag309-)))

(:derived (Flag311-)(and (Flag190-)))

(:derived (Flag311-)(and (Flag263-)))

(:derived (Flag311-)(and (Flag239-)))

(:derived (Flag311-)(and (Flag310-)))

(:derived (Flag311-)(and (Flag302-)))

(:derived (Flag314-)(and (Flag279-)))

(:derived (Flag314-)(and (Flag246-)))

(:derived (Flag314-)(and (Flag201-)))

(:derived (Flag314-)(and (Flag313-)))

(:derived (Flag314-)(and (Flag306-)))

(:derived (Flag314-)(and (Flag309-)))

(:derived (Flag314-)(and (Flag190-)))

(:derived (Flag314-)(and (Flag229-)))

(:derived (Flag314-)(and (Flag269-)))

(:derived (Flag314-)(and (Flag300-)))

(:derived (Flag314-)(and (Flag239-)))

(:derived (Flag314-)(and (Flag296-)))

(:derived (Flag314-)(and (Flag286-)))

(:derived (Flag314-)(and (Flag263-)))

(:derived (Flag1-)(and (COLUMN2-ROBOT)(ROW1-ROBOT)(NOT-ERROR-)(NOT-CHECKCONSISTENCY-)))

(:derived (Flag2-)(and (LEFTOF3-ROBOT)))

(:derived (Flag2-)(and (RIGHTOF7-ROBOT)))

(:derived (Flag2-)(and (RIGHTOF8-ROBOT)))

(:derived (Flag2-)(and (LEFTOF2-ROBOT)))

(:derived (Flag2-)(and (LEFTOF9-ROBOT)))

(:derived (Flag2-)(and (RIGHTOF0-ROBOT)))

(:derived (Flag2-)(and (LEFTOF8-ROBOT)))

(:derived (Flag2-)(and (LEFTOF14-ROBOT)))

(:derived (Flag2-)(and (LEFTOF5-ROBOT)))

(:derived (Flag2-)(and (RIGHTOF12-ROBOT)))

(:derived (Flag2-)(and (RIGHTOF1-ROBOT)))

(:derived (Flag2-)(and (LEFTOF12-ROBOT)))

(:derived (Flag2-)(and (RIGHTOF13-ROBOT)))

(:derived (Flag2-)(and (RIGHTOF6-ROBOT)))

(:derived (Flag2-)(and (RIGHTOF9-ROBOT)))

(:derived (Flag2-)(and (LEFTOF6-ROBOT)))

(:derived (Flag2-)(and (RIGHTOF10-ROBOT)))

(:derived (Flag2-)(and (RIGHTOF11-ROBOT)))

(:derived (Flag2-)(and (LEFTOF11-ROBOT)))

(:derived (Flag2-)(and (RIGHTOF5-ROBOT)))

(:derived (Flag2-)(and (LEFTOF10-ROBOT)))

(:derived (Flag2-)(and (LEFTOF4-ROBOT)))

(:derived (Flag2-)(and (RIGHTOF4-ROBOT)))

(:derived (Flag2-)(and (LEFTOF1-ROBOT)))

(:derived (Flag2-)(and (LEFTOF7-ROBOT)))

(:derived (Flag2-)(and (RIGHTOF3-ROBOT)))

(:derived (Flag42-)(and (Flag29-)))

(:derived (Flag42-)(and (Flag30-)))

(:derived (Flag42-)(and (Flag31-)))

(:derived (Flag42-)(and (Flag32-)))

(:derived (Flag42-)(and (Flag33-)))

(:derived (Flag42-)(and (Flag34-)))

(:derived (Flag42-)(and (Flag35-)))

(:derived (Flag42-)(and (Flag36-)))

(:derived (Flag42-)(and (Flag37-)))

(:derived (Flag42-)(and (Flag38-)))

(:derived (Flag42-)(and (Flag39-)))

(:derived (Flag42-)(and (Flag40-)))

(:derived (Flag42-)(and (Flag41-)))

(:derived (Flag56-)(and (Flag43-)))

(:derived (Flag56-)(and (Flag35-)))

(:derived (Flag56-)(and (Flag44-)))

(:derived (Flag56-)(and (Flag45-)))

(:derived (Flag56-)(and (Flag46-)))

(:derived (Flag56-)(and (Flag34-)))

(:derived (Flag56-)(and (Flag31-)))

(:derived (Flag56-)(and (Flag37-)))

(:derived (Flag56-)(and (Flag47-)))

(:derived (Flag56-)(and (Flag48-)))

(:derived (Flag56-)(and (Flag30-)))

(:derived (Flag56-)(and (Flag49-)))

(:derived (Flag56-)(and (Flag33-)))

(:derived (Flag56-)(and (Flag36-)))

(:derived (Flag56-)(and (Flag38-)))

(:derived (Flag56-)(and (Flag50-)))

(:derived (Flag56-)(and (Flag40-)))

(:derived (Flag56-)(and (Flag41-)))

(:derived (Flag56-)(and (Flag51-)))

(:derived (Flag56-)(and (Flag52-)))

(:derived (Flag56-)(and (Flag29-)))

(:derived (Flag56-)(and (Flag53-)))

(:derived (Flag56-)(and (Flag32-)))

(:derived (Flag56-)(and (Flag54-)))

(:derived (Flag56-)(and (Flag39-)))

(:derived (Flag56-)(and (Flag55-)))

(:derived (Flag70-)(and (Flag57-)))

(:derived (Flag70-)(and (Flag58-)))

(:derived (Flag70-)(and (Flag30-)))

(:derived (Flag70-)(and (Flag59-)))

(:derived (Flag70-)(and (Flag35-)))

(:derived (Flag70-)(and (Flag50-)))

(:derived (Flag70-)(and (Flag45-)))

(:derived (Flag70-)(and (Flag46-)))

(:derived (Flag70-)(and (Flag34-)))

(:derived (Flag70-)(and (Flag37-)))

(:derived (Flag70-)(and (Flag60-)))

(:derived (Flag70-)(and (Flag47-)))

(:derived (Flag70-)(and (Flag61-)))

(:derived (Flag70-)(and (Flag48-)))

(:derived (Flag70-)(and (Flag62-)))

(:derived (Flag70-)(and (Flag49-)))

(:derived (Flag70-)(and (Flag33-)))

(:derived (Flag70-)(and (Flag63-)))

(:derived (Flag70-)(and (Flag36-)))

(:derived (Flag70-)(and (Flag64-)))

(:derived (Flag70-)(and (Flag38-)))

(:derived (Flag70-)(and (Flag65-)))

(:derived (Flag70-)(and (Flag66-)))

(:derived (Flag70-)(and (Flag40-)))

(:derived (Flag70-)(and (Flag41-)))

(:derived (Flag70-)(and (Flag51-)))

(:derived (Flag70-)(and (Flag52-)))

(:derived (Flag70-)(and (Flag29-)))

(:derived (Flag70-)(and (Flag53-)))

(:derived (Flag70-)(and (Flag67-)))

(:derived (Flag70-)(and (Flag32-)))

(:derived (Flag70-)(and (Flag68-)))

(:derived (Flag70-)(and (Flag69-)))

(:derived (Flag70-)(and (Flag54-)))

(:derived (Flag70-)(and (Flag39-)))

(:derived (Flag70-)(and (Flag55-)))

(:derived (Flag84-)(and (Flag57-)))

(:derived (Flag84-)(and (Flag71-)))

(:derived (Flag84-)(and (Flag72-)))

(:derived (Flag84-)(and (Flag73-)))

(:derived (Flag84-)(and (Flag58-)))

(:derived (Flag84-)(and (Flag59-)))

(:derived (Flag84-)(and (Flag35-)))

(:derived (Flag84-)(and (Flag50-)))

(:derived (Flag84-)(and (Flag45-)))

(:derived (Flag84-)(and (Flag60-)))

(:derived (Flag84-)(and (Flag46-)))

(:derived (Flag84-)(and (Flag34-)))

(:derived (Flag84-)(and (Flag74-)))

(:derived (Flag84-)(and (Flag37-)))

(:derived (Flag84-)(and (Flag75-)))

(:derived (Flag84-)(and (Flag76-)))

(:derived (Flag84-)(and (Flag77-)))

(:derived (Flag84-)(and (Flag47-)))

(:derived (Flag84-)(and (Flag78-)))

(:derived (Flag84-)(and (Flag79-)))

(:derived (Flag84-)(and (Flag80-)))

(:derived (Flag84-)(and (Flag48-)))

(:derived (Flag84-)(and (Flag49-)))

(:derived (Flag84-)(and (Flag33-)))

(:derived (Flag84-)(and (Flag63-)))

(:derived (Flag84-)(and (Flag36-)))

(:derived (Flag84-)(and (Flag64-)))

(:derived (Flag84-)(and (Flag53-)))

(:derived (Flag84-)(and (Flag38-)))

(:derived (Flag84-)(and (Flag65-)))

(:derived (Flag84-)(and (Flag81-)))

(:derived (Flag84-)(and (Flag40-)))

(:derived (Flag84-)(and (Flag41-)))

(:derived (Flag84-)(and (Flag51-)))

(:derived (Flag84-)(and (Flag52-)))

(:derived (Flag84-)(and (Flag29-)))

(:derived (Flag84-)(and (Flag82-)))

(:derived (Flag84-)(and (Flag67-)))

(:derived (Flag84-)(and (Flag83-)))

(:derived (Flag84-)(and (Flag68-)))

(:derived (Flag84-)(and (Flag32-)))

(:derived (Flag84-)(and (Flag69-)))

(:derived (Flag84-)(and (Flag54-)))

(:derived (Flag84-)(and (Flag39-)))

(:derived (Flag84-)(and (Flag55-)))

(:derived (Flag95-)(and (Flag85-)))

(:derived (Flag95-)(and (Flag57-)))

(:derived (Flag95-)(and (Flag71-)))

(:derived (Flag95-)(and (Flag77-)))

(:derived (Flag95-)(and (Flag58-)))

(:derived (Flag95-)(and (Flag86-)))

(:derived (Flag95-)(and (Flag59-)))

(:derived (Flag95-)(and (Flag35-)))

(:derived (Flag95-)(and (Flag45-)))

(:derived (Flag95-)(and (Flag60-)))

(:derived (Flag95-)(and (Flag75-)))

(:derived (Flag95-)(and (Flag46-)))

(:derived (Flag95-)(and (Flag34-)))

(:derived (Flag95-)(and (Flag87-)))

(:derived (Flag95-)(and (Flag74-)))

(:derived (Flag95-)(and (Flag37-)))

(:derived (Flag95-)(and (Flag76-)))

(:derived (Flag95-)(and (Flag88-)))

(:derived (Flag95-)(and (Flag89-)))

(:derived (Flag95-)(and (Flag90-)))

(:derived (Flag95-)(and (Flag47-)))

(:derived (Flag95-)(and (Flag78-)))

(:derived (Flag95-)(and (Flag79-)))

(:derived (Flag95-)(and (Flag48-)))

(:derived (Flag95-)(and (Flag49-)))

(:derived (Flag95-)(and (Flag33-)))

(:derived (Flag95-)(and (Flag63-)))

(:derived (Flag95-)(and (Flag36-)))

(:derived (Flag95-)(and (Flag91-)))

(:derived (Flag95-)(and (Flag64-)))

(:derived (Flag95-)(and (Flag92-)))

(:derived (Flag95-)(and (Flag53-)))

(:derived (Flag95-)(and (Flag38-)))

(:derived (Flag95-)(and (Flag50-)))

(:derived (Flag95-)(and (Flag81-)))

(:derived (Flag95-)(and (Flag73-)))

(:derived (Flag95-)(and (Flag41-)))

(:derived (Flag95-)(and (Flag51-)))

(:derived (Flag95-)(and (Flag52-)))

(:derived (Flag95-)(and (Flag29-)))

(:derived (Flag95-)(and (Flag93-)))

(:derived (Flag95-)(and (Flag82-)))

(:derived (Flag95-)(and (Flag67-)))

(:derived (Flag95-)(and (Flag83-)))

(:derived (Flag95-)(and (Flag94-)))

(:derived (Flag95-)(and (Flag68-)))

(:derived (Flag95-)(and (Flag32-)))

(:derived (Flag95-)(and (Flag69-)))

(:derived (Flag95-)(and (Flag54-)))

(:derived (Flag95-)(and (Flag39-)))

(:derived (Flag95-)(and (Flag65-)))

(:derived (Flag107-)(and (Flag57-)))

(:derived (Flag107-)(and (Flag54-)))

(:derived (Flag107-)(and (Flag71-)))

(:derived (Flag107-)(and (Flag96-)))

(:derived (Flag107-)(and (Flag58-)))

(:derived (Flag107-)(and (Flag59-)))

(:derived (Flag107-)(and (Flag97-)))

(:derived (Flag107-)(and (Flag98-)))

(:derived (Flag107-)(and (Flag35-)))

(:derived (Flag107-)(and (Flag99-)))

(:derived (Flag107-)(and (Flag45-)))

(:derived (Flag107-)(and (Flag60-)))

(:derived (Flag107-)(and (Flag100-)))

(:derived (Flag107-)(and (Flag86-)))

(:derived (Flag107-)(and (Flag41-)))

(:derived (Flag107-)(and (Flag46-)))

(:derived (Flag107-)(and (Flag49-)))

(:derived (Flag107-)(and (Flag34-)))

(:derived (Flag107-)(and (Flag94-)))

(:derived (Flag107-)(and (Flag74-)))

(:derived (Flag107-)(and (Flag37-)))

(:derived (Flag107-)(and (Flag101-)))

(:derived (Flag107-)(and (Flag102-)))

(:derived (Flag107-)(and (Flag76-)))

(:derived (Flag107-)(and (Flag88-)))

(:derived (Flag107-)(and (Flag89-)))

(:derived (Flag107-)(and (Flag77-)))

(:derived (Flag107-)(and (Flag78-)))

(:derived (Flag107-)(and (Flag79-)))

(:derived (Flag107-)(and (Flag48-)))

(:derived (Flag107-)(and (Flag36-)))

(:derived (Flag107-)(and (Flag33-)))

(:derived (Flag107-)(and (Flag63-)))

(:derived (Flag107-)(and (Flag103-)))

(:derived (Flag107-)(and (Flag92-)))

(:derived (Flag107-)(and (Flag53-)))

(:derived (Flag107-)(and (Flag38-)))

(:derived (Flag107-)(and (Flag50-)))

(:derived (Flag107-)(and (Flag81-)))

(:derived (Flag107-)(and (Flag104-)))

(:derived (Flag107-)(and (Flag73-)))

(:derived (Flag107-)(and (Flag105-)))

(:derived (Flag107-)(and (Flag51-)))

(:derived (Flag107-)(and (Flag52-)))

(:derived (Flag107-)(and (Flag93-)))

(:derived (Flag107-)(and (Flag82-)))

(:derived (Flag107-)(and (Flag67-)))

(:derived (Flag107-)(and (Flag83-)))

(:derived (Flag107-)(and (Flag68-)))

(:derived (Flag107-)(and (Flag106-)))

(:derived (Flag107-)(and (Flag32-)))

(:derived (Flag107-)(and (Flag90-)))

(:derived (Flag107-)(and (Flag69-)))

(:derived (Flag107-)(and (Flag87-)))

(:derived (Flag107-)(and (Flag39-)))

(:derived (Flag107-)(and (Flag65-)))

(:derived (Flag118-)(and (Flag32-)))

(:derived (Flag118-)(and (Flag74-)))

(:derived (Flag118-)(and (Flag108-)))

(:derived (Flag118-)(and (Flag71-)))

(:derived (Flag118-)(and (Flag96-)))

(:derived (Flag118-)(and (Flag48-)))

(:derived (Flag118-)(and (Flag106-)))

(:derived (Flag118-)(and (Flag59-)))

(:derived (Flag118-)(and (Flag97-)))

(:derived (Flag118-)(and (Flag98-)))

(:derived (Flag118-)(and (Flag109-)))

(:derived (Flag118-)(and (Flag35-)))

(:derived (Flag118-)(and (Flag99-)))

(:derived (Flag118-)(and (Flag110-)))

(:derived (Flag118-)(and (Flag45-)))

(:derived (Flag118-)(and (Flag60-)))

(:derived (Flag118-)(and (Flag86-)))

(:derived (Flag118-)(and (Flag41-)))

(:derived (Flag118-)(and (Flag46-)))

(:derived (Flag118-)(and (Flag34-)))

(:derived (Flag118-)(and (Flag94-)))

(:derived (Flag118-)(and (Flag111-)))

(:derived (Flag118-)(and (Flag101-)))

(:derived (Flag118-)(and (Flag76-)))

(:derived (Flag118-)(and (Flag88-)))

(:derived (Flag118-)(and (Flag89-)))

(:derived (Flag118-)(and (Flag77-)))

(:derived (Flag118-)(and (Flag78-)))

(:derived (Flag118-)(and (Flag112-)))

(:derived (Flag118-)(and (Flag58-)))

(:derived (Flag118-)(and (Flag49-)))

(:derived (Flag118-)(and (Flag33-)))

(:derived (Flag118-)(and (Flag63-)))

(:derived (Flag118-)(and (Flag36-)))

(:derived (Flag118-)(and (Flag92-)))

(:derived (Flag118-)(and (Flag53-)))

(:derived (Flag118-)(and (Flag38-)))

(:derived (Flag118-)(and (Flag65-)))

(:derived (Flag118-)(and (Flag81-)))

(:derived (Flag118-)(and (Flag104-)))

(:derived (Flag118-)(and (Flag73-)))

(:derived (Flag118-)(and (Flag113-)))

(:derived (Flag118-)(and (Flag105-)))

(:derived (Flag118-)(and (Flag51-)))

(:derived (Flag118-)(and (Flag52-)))

(:derived (Flag118-)(and (Flag93-)))

(:derived (Flag118-)(and (Flag82-)))

(:derived (Flag118-)(and (Flag114-)))

(:derived (Flag118-)(and (Flag67-)))

(:derived (Flag118-)(and (Flag115-)))

(:derived (Flag118-)(and (Flag68-)))

(:derived (Flag118-)(and (Flag116-)))

(:derived (Flag118-)(and (Flag117-)))

(:derived (Flag118-)(and (Flag83-)))

(:derived (Flag118-)(and (Flag90-)))

(:derived (Flag118-)(and (Flag69-)))

(:derived (Flag118-)(and (Flag87-)))

(:derived (Flag118-)(and (Flag39-)))

(:derived (Flag118-)(and (Flag50-)))

(:derived (Flag127-)(and (Flag32-)))

(:derived (Flag127-)(and (Flag74-)))

(:derived (Flag127-)(and (Flag71-)))

(:derived (Flag127-)(and (Flag96-)))

(:derived (Flag127-)(and (Flag114-)))

(:derived (Flag127-)(and (Flag119-)))

(:derived (Flag127-)(and (Flag106-)))

(:derived (Flag127-)(and (Flag59-)))

(:derived (Flag127-)(and (Flag97-)))

(:derived (Flag127-)(and (Flag98-)))

(:derived (Flag127-)(and (Flag109-)))

(:derived (Flag127-)(and (Flag35-)))

(:derived (Flag127-)(and (Flag99-)))

(:derived (Flag127-)(and (Flag50-)))

(:derived (Flag127-)(and (Flag45-)))

(:derived (Flag127-)(and (Flag60-)))

(:derived (Flag127-)(and (Flag86-)))

(:derived (Flag127-)(and (Flag41-)))

(:derived (Flag127-)(and (Flag46-)))

(:derived (Flag127-)(and (Flag34-)))

(:derived (Flag127-)(and (Flag111-)))

(:derived (Flag127-)(and (Flag101-)))

(:derived (Flag127-)(and (Flag120-)))

(:derived (Flag127-)(and (Flag76-)))

(:derived (Flag127-)(and (Flag88-)))

(:derived (Flag127-)(and (Flag89-)))

(:derived (Flag127-)(and (Flag121-)))

(:derived (Flag127-)(and (Flag77-)))

(:derived (Flag127-)(and (Flag78-)))

(:derived (Flag127-)(and (Flag53-)))

(:derived (Flag127-)(and (Flag112-)))

(:derived (Flag127-)(and (Flag90-)))

(:derived (Flag127-)(and (Flag122-)))

(:derived (Flag127-)(and (Flag49-)))

(:derived (Flag127-)(and (Flag33-)))

(:derived (Flag127-)(and (Flag63-)))

(:derived (Flag127-)(and (Flag92-)))

(:derived (Flag127-)(and (Flag123-)))

(:derived (Flag127-)(and (Flag38-)))

(:derived (Flag127-)(and (Flag65-)))

(:derived (Flag127-)(and (Flag81-)))

(:derived (Flag127-)(and (Flag104-)))

(:derived (Flag127-)(and (Flag113-)))

(:derived (Flag127-)(and (Flag105-)))

(:derived (Flag127-)(and (Flag51-)))

(:derived (Flag127-)(and (Flag52-)))

(:derived (Flag127-)(and (Flag124-)))

(:derived (Flag127-)(and (Flag93-)))

(:derived (Flag127-)(and (Flag82-)))

(:derived (Flag127-)(and (Flag67-)))

(:derived (Flag127-)(and (Flag83-)))

(:derived (Flag127-)(and (Flag68-)))

(:derived (Flag127-)(and (Flag116-)))

(:derived (Flag127-)(and (Flag117-)))

(:derived (Flag127-)(and (Flag125-)))

(:derived (Flag127-)(and (Flag69-)))

(:derived (Flag127-)(and (Flag94-)))

(:derived (Flag127-)(and (Flag39-)))

(:derived (Flag127-)(and (Flag126-)))

(:derived (Flag136-)(and (Flag128-)))

(:derived (Flag136-)(and (Flag74-)))

(:derived (Flag136-)(and (Flag71-)))

(:derived (Flag136-)(and (Flag116-)))

(:derived (Flag136-)(and (Flag96-)))

(:derived (Flag136-)(and (Flag114-)))

(:derived (Flag136-)(and (Flag119-)))

(:derived (Flag136-)(and (Flag117-)))

(:derived (Flag136-)(and (Flag129-)))

(:derived (Flag136-)(and (Flag97-)))

(:derived (Flag136-)(and (Flag98-)))

(:derived (Flag136-)(and (Flag109-)))

(:derived (Flag136-)(and (Flag35-)))

(:derived (Flag136-)(and (Flag99-)))

(:derived (Flag136-)(and (Flag50-)))

(:derived (Flag136-)(and (Flag60-)))

(:derived (Flag136-)(and (Flag130-)))

(:derived (Flag136-)(and (Flag86-)))

(:derived (Flag136-)(and (Flag41-)))

(:derived (Flag136-)(and (Flag46-)))

(:derived (Flag136-)(and (Flag111-)))

(:derived (Flag136-)(and (Flag120-)))

(:derived (Flag136-)(and (Flag76-)))

(:derived (Flag136-)(and (Flag88-)))

(:derived (Flag136-)(and (Flag89-)))

(:derived (Flag136-)(and (Flag121-)))

(:derived (Flag136-)(and (Flag90-)))

(:derived (Flag136-)(and (Flag78-)))

(:derived (Flag136-)(and (Flag53-)))

(:derived (Flag136-)(and (Flag112-)))

(:derived (Flag136-)(and (Flag49-)))

(:derived (Flag136-)(and (Flag33-)))

(:derived (Flag136-)(and (Flag63-)))

(:derived (Flag136-)(and (Flag123-)))

(:derived (Flag136-)(and (Flag38-)))

(:derived (Flag136-)(and (Flag65-)))

(:derived (Flag136-)(and (Flag81-)))

(:derived (Flag136-)(and (Flag104-)))

(:derived (Flag136-)(and (Flag113-)))

(:derived (Flag136-)(and (Flag105-)))

(:derived (Flag136-)(and (Flag51-)))

(:derived (Flag136-)(and (Flag52-)))

(:derived (Flag136-)(and (Flag131-)))

(:derived (Flag136-)(and (Flag93-)))

(:derived (Flag136-)(and (Flag82-)))

(:derived (Flag136-)(and (Flag132-)))

(:derived (Flag136-)(and (Flag67-)))

(:derived (Flag136-)(and (Flag83-)))

(:derived (Flag136-)(and (Flag133-)))

(:derived (Flag136-)(and (Flag68-)))

(:derived (Flag136-)(and (Flag134-)))

(:derived (Flag136-)(and (Flag106-)))

(:derived (Flag136-)(and (Flag32-)))

(:derived (Flag136-)(and (Flag69-)))

(:derived (Flag136-)(and (Flag94-)))

(:derived (Flag136-)(and (Flag135-)))

(:derived (Flag136-)(and (Flag39-)))

(:derived (Flag136-)(and (Flag126-)))

(:derived (Flag143-)(and (Flag128-)))

(:derived (Flag143-)(and (Flag74-)))

(:derived (Flag143-)(and (Flag71-)))

(:derived (Flag143-)(and (Flag96-)))

(:derived (Flag143-)(and (Flag114-)))

(:derived (Flag143-)(and (Flag119-)))

(:derived (Flag143-)(and (Flag106-)))

(:derived (Flag143-)(and (Flag129-)))

(:derived (Flag143-)(and (Flag97-)))

(:derived (Flag143-)(and (Flag98-)))

(:derived (Flag143-)(and (Flag109-)))

(:derived (Flag143-)(and (Flag35-)))

(:derived (Flag143-)(and (Flag99-)))

(:derived (Flag143-)(and (Flag137-)))

(:derived (Flag143-)(and (Flag41-)))

(:derived (Flag143-)(and (Flag46-)))

(:derived (Flag143-)(and (Flag50-)))

(:derived (Flag143-)(and (Flag111-)))

(:derived (Flag143-)(and (Flag120-)))

(:derived (Flag143-)(and (Flag88-)))

(:derived (Flag143-)(and (Flag60-)))

(:derived (Flag143-)(and (Flag89-)))

(:derived (Flag143-)(and (Flag121-)))

(:derived (Flag143-)(and (Flag90-)))

(:derived (Flag143-)(and (Flag78-)))

(:derived (Flag143-)(and (Flag53-)))

(:derived (Flag143-)(and (Flag112-)))

(:derived (Flag143-)(and (Flag138-)))

(:derived (Flag143-)(and (Flag49-)))

(:derived (Flag143-)(and (Flag123-)))

(:derived (Flag143-)(and (Flag38-)))

(:derived (Flag143-)(and (Flag139-)))

(:derived (Flag143-)(and (Flag65-)))

(:derived (Flag143-)(and (Flag81-)))

(:derived (Flag143-)(and (Flag113-)))

(:derived (Flag143-)(and (Flag105-)))

(:derived (Flag143-)(and (Flag51-)))

(:derived (Flag143-)(and (Flag140-)))

(:derived (Flag143-)(and (Flag131-)))

(:derived (Flag143-)(and (Flag141-)))

(:derived (Flag143-)(and (Flag93-)))

(:derived (Flag143-)(and (Flag82-)))

(:derived (Flag143-)(and (Flag67-)))

(:derived (Flag143-)(and (Flag83-)))

(:derived (Flag143-)(and (Flag133-)))

(:derived (Flag143-)(and (Flag68-)))

(:derived (Flag143-)(and (Flag134-)))

(:derived (Flag143-)(and (Flag117-)))

(:derived (Flag143-)(and (Flag32-)))

(:derived (Flag143-)(and (Flag69-)))

(:derived (Flag143-)(and (Flag94-)))

(:derived (Flag143-)(and (Flag142-)))

(:derived (Flag143-)(and (Flag33-)))

(:derived (Flag143-)(and (Flag126-)))

(:derived (Flag148-)(and (Flag128-)))

(:derived (Flag148-)(and (Flag71-)))

(:derived (Flag148-)(and (Flag97-)))

(:derived (Flag148-)(and (Flag96-)))

(:derived (Flag148-)(and (Flag114-)))

(:derived (Flag148-)(and (Flag119-)))

(:derived (Flag148-)(and (Flag129-)))

(:derived (Flag148-)(and (Flag144-)))

(:derived (Flag148-)(and (Flag98-)))

(:derived (Flag148-)(and (Flag35-)))

(:derived (Flag148-)(and (Flag99-)))

(:derived (Flag148-)(and (Flag145-)))

(:derived (Flag148-)(and (Flag50-)))

(:derived (Flag148-)(and (Flag46-)))

(:derived (Flag148-)(and (Flag146-)))

(:derived (Flag148-)(and (Flag111-)))

(:derived (Flag148-)(and (Flag120-)))

(:derived (Flag148-)(and (Flag88-)))

(:derived (Flag148-)(and (Flag89-)))

(:derived (Flag148-)(and (Flag121-)))

(:derived (Flag148-)(and (Flag90-)))

(:derived (Flag148-)(and (Flag78-)))

(:derived (Flag148-)(and (Flag112-)))

(:derived (Flag148-)(and (Flag138-)))

(:derived (Flag148-)(and (Flag53-)))

(:derived (Flag148-)(and (Flag38-)))

(:derived (Flag148-)(and (Flag139-)))

(:derived (Flag148-)(and (Flag65-)))

(:derived (Flag148-)(and (Flag81-)))

(:derived (Flag148-)(and (Flag113-)))

(:derived (Flag148-)(and (Flag51-)))

(:derived (Flag148-)(and (Flag131-)))

(:derived (Flag148-)(and (Flag141-)))

(:derived (Flag148-)(and (Flag82-)))

(:derived (Flag148-)(and (Flag67-)))

(:derived (Flag148-)(and (Flag83-)))

(:derived (Flag148-)(and (Flag147-)))

(:derived (Flag148-)(and (Flag133-)))

(:derived (Flag148-)(and (Flag68-)))

(:derived (Flag148-)(and (Flag134-)))

(:derived (Flag148-)(and (Flag117-)))

(:derived (Flag148-)(and (Flag32-)))

(:derived (Flag148-)(and (Flag69-)))

(:derived (Flag148-)(and (Flag94-)))

(:derived (Flag148-)(and (Flag142-)))

(:derived (Flag148-)(and (Flag33-)))

(:derived (Flag148-)(and (Flag126-)))

(:derived (Flag152-)(and (Flag128-)))

(:derived (Flag152-)(and (Flag71-)))

(:derived (Flag152-)(and (Flag96-)))

(:derived (Flag152-)(and (Flag114-)))

(:derived (Flag152-)(and (Flag119-)))

(:derived (Flag152-)(and (Flag97-)))

(:derived (Flag152-)(and (Flag98-)))

(:derived (Flag152-)(and (Flag35-)))

(:derived (Flag152-)(and (Flag99-)))

(:derived (Flag152-)(and (Flag145-)))

(:derived (Flag152-)(and (Flag149-)))

(:derived (Flag152-)(and (Flag46-)))

(:derived (Flag152-)(and (Flag146-)))

(:derived (Flag152-)(and (Flag88-)))

(:derived (Flag152-)(and (Flag121-)))

(:derived (Flag152-)(and (Flag90-)))

(:derived (Flag152-)(and (Flag78-)))

(:derived (Flag152-)(and (Flag150-)))

(:derived (Flag152-)(and (Flag151-)))

(:derived (Flag152-)(and (Flag53-)))

(:derived (Flag152-)(and (Flag139-)))

(:derived (Flag152-)(and (Flag50-)))

(:derived (Flag152-)(and (Flag113-)))

(:derived (Flag152-)(and (Flag131-)))

(:derived (Flag152-)(and (Flag141-)))

(:derived (Flag152-)(and (Flag82-)))

(:derived (Flag152-)(and (Flag67-)))

(:derived (Flag152-)(and (Flag83-)))

(:derived (Flag152-)(and (Flag147-)))

(:derived (Flag152-)(and (Flag133-)))

(:derived (Flag152-)(and (Flag68-)))

(:derived (Flag152-)(and (Flag134-)))

(:derived (Flag152-)(and (Flag117-)))

(:derived (Flag152-)(and (Flag32-)))

(:derived (Flag152-)(and (Flag94-)))

(:derived (Flag152-)(and (Flag142-)))

(:derived (Flag152-)(and (Flag33-)))

(:derived (Flag152-)(and (Flag126-)))

(:derived (Flag156-)(and (Flag128-)))

(:derived (Flag156-)(and (Flag68-)))

(:derived (Flag156-)(and (Flag71-)))

(:derived (Flag156-)(and (Flag114-)))

(:derived (Flag156-)(and (Flag119-)))

(:derived (Flag156-)(and (Flag153-)))

(:derived (Flag156-)(and (Flag97-)))

(:derived (Flag156-)(and (Flag98-)))

(:derived (Flag156-)(and (Flag99-)))

(:derived (Flag156-)(and (Flag46-)))

(:derived (Flag156-)(and (Flag146-)))

(:derived (Flag156-)(and (Flag88-)))

(:derived (Flag156-)(and (Flag78-)))

(:derived (Flag156-)(and (Flag150-)))

(:derived (Flag156-)(and (Flag154-)))

(:derived (Flag156-)(and (Flag139-)))

(:derived (Flag156-)(and (Flag113-)))

(:derived (Flag156-)(and (Flag131-)))

(:derived (Flag156-)(and (Flag53-)))

(:derived (Flag156-)(and (Flag67-)))

(:derived (Flag156-)(and (Flag32-)))

(:derived (Flag156-)(and (Flag147-)))

(:derived (Flag156-)(and (Flag133-)))

(:derived (Flag156-)(and (Flag155-)))

(:derived (Flag156-)(and (Flag94-)))

(:derived (Flag156-)(and (Flag142-)))

(:derived (Flag156-)(and (Flag33-)))

(:derived (Flag160-)(and (Flag46-)))

(:derived (Flag160-)(and (Flag146-)))

(:derived (Flag160-)(and (Flag71-)))

(:derived (Flag160-)(and (Flag68-)))

(:derived (Flag160-)(and (Flag99-)))

(:derived (Flag160-)(and (Flag158-)))

(:derived (Flag160-)(and (Flag114-)))

(:derived (Flag160-)(and (Flag119-)))

(:derived (Flag160-)(and (Flag159-)))

(:derived (Flag160-)(and (Flag142-)))

(:derived (Flag160-)(and (Flag33-)))

(:derived (Flag160-)(and (Flag98-)))

(:derived (Flag160-)(and (Flag150-)))

(:derived (Flag160-)(and (Flag131-)))

(:derived (Flag161-)(and (RIGHTOF3-ROBOT)))

(:derived (Flag161-)(and (RIGHTOF8-ROBOT)))

(:derived (Flag161-)(and (RIGHTOF10-ROBOT)))

(:derived (Flag161-)(and (RIGHTOF5-ROBOT)))

(:derived (Flag161-)(and (RIGHTOF13-ROBOT)))

(:derived (Flag161-)(and (RIGHTOF11-ROBOT)))

(:derived (Flag161-)(and (RIGHTOF0-ROBOT)))

(:derived (Flag161-)(and (RIGHTOF4-ROBOT)))

(:derived (Flag161-)(and (RIGHTOF1-ROBOT)))

(:derived (Flag161-)(and (RIGHTOF12-ROBOT)))

(:derived (Flag161-)(and (RIGHTOF7-ROBOT)))

(:derived (Flag161-)(and (RIGHTOF9-ROBOT)))

(:derived (Flag161-)(and (RIGHTOF6-ROBOT)))

(:derived (Flag162-)(and (RIGHTOF3-ROBOT)))

(:derived (Flag162-)(and (RIGHTOF8-ROBOT)))

(:derived (Flag162-)(and (RIGHTOF5-ROBOT)))

(:derived (Flag162-)(and (RIGHTOF13-ROBOT)))

(:derived (Flag162-)(and (RIGHTOF11-ROBOT)))

(:derived (Flag162-)(and (RIGHTOF10-ROBOT)))

(:derived (Flag162-)(and (RIGHTOF4-ROBOT)))

(:derived (Flag162-)(and (RIGHTOF1-ROBOT)))

(:derived (Flag162-)(and (RIGHTOF12-ROBOT)))

(:derived (Flag162-)(and (RIGHTOF7-ROBOT)))

(:derived (Flag162-)(and (RIGHTOF9-ROBOT)))

(:derived (Flag162-)(and (RIGHTOF6-ROBOT)))

(:derived (Flag163-)(and (RIGHTOF3-ROBOT)))

(:derived (Flag163-)(and (RIGHTOF5-ROBOT)))

(:derived (Flag163-)(and (RIGHTOF13-ROBOT)))

(:derived (Flag163-)(and (RIGHTOF11-ROBOT)))

(:derived (Flag163-)(and (RIGHTOF10-ROBOT)))

(:derived (Flag163-)(and (RIGHTOF4-ROBOT)))

(:derived (Flag163-)(and (RIGHTOF8-ROBOT)))

(:derived (Flag163-)(and (RIGHTOF12-ROBOT)))

(:derived (Flag163-)(and (RIGHTOF7-ROBOT)))

(:derived (Flag163-)(and (RIGHTOF9-ROBOT)))

(:derived (Flag163-)(and (RIGHTOF6-ROBOT)))

(:derived (Flag164-)(and (RIGHTOF3-ROBOT)))

(:derived (Flag164-)(and (RIGHTOF5-ROBOT)))

(:derived (Flag164-)(and (RIGHTOF13-ROBOT)))

(:derived (Flag164-)(and (RIGHTOF11-ROBOT)))

(:derived (Flag164-)(and (RIGHTOF10-ROBOT)))

(:derived (Flag164-)(and (RIGHTOF4-ROBOT)))

(:derived (Flag164-)(and (RIGHTOF8-ROBOT)))

(:derived (Flag164-)(and (RIGHTOF12-ROBOT)))

(:derived (Flag164-)(and (RIGHTOF7-ROBOT)))

(:derived (Flag164-)(and (RIGHTOF9-ROBOT)))

(:derived (Flag164-)(and (RIGHTOF6-ROBOT)))

(:derived (Flag165-)(and (RIGHTOF5-ROBOT)))

(:derived (Flag165-)(and (RIGHTOF13-ROBOT)))

(:derived (Flag165-)(and (RIGHTOF11-ROBOT)))

(:derived (Flag165-)(and (RIGHTOF10-ROBOT)))

(:derived (Flag165-)(and (RIGHTOF4-ROBOT)))

(:derived (Flag165-)(and (RIGHTOF8-ROBOT)))

(:derived (Flag165-)(and (RIGHTOF12-ROBOT)))

(:derived (Flag165-)(and (RIGHTOF7-ROBOT)))

(:derived (Flag165-)(and (RIGHTOF9-ROBOT)))

(:derived (Flag165-)(and (RIGHTOF6-ROBOT)))

(:derived (Flag166-)(and (RIGHTOF5-ROBOT)))

(:derived (Flag166-)(and (RIGHTOF13-ROBOT)))

(:derived (Flag166-)(and (RIGHTOF11-ROBOT)))

(:derived (Flag166-)(and (RIGHTOF10-ROBOT)))

(:derived (Flag166-)(and (RIGHTOF8-ROBOT)))

(:derived (Flag166-)(and (RIGHTOF12-ROBOT)))

(:derived (Flag166-)(and (RIGHTOF7-ROBOT)))

(:derived (Flag166-)(and (RIGHTOF9-ROBOT)))

(:derived (Flag166-)(and (RIGHTOF6-ROBOT)))

(:derived (Flag167-)(and (RIGHTOF13-ROBOT)))

(:derived (Flag167-)(and (RIGHTOF11-ROBOT)))

(:derived (Flag167-)(and (RIGHTOF10-ROBOT)))

(:derived (Flag167-)(and (RIGHTOF8-ROBOT)))

(:derived (Flag167-)(and (RIGHTOF12-ROBOT)))

(:derived (Flag167-)(and (RIGHTOF7-ROBOT)))

(:derived (Flag167-)(and (RIGHTOF9-ROBOT)))

(:derived (Flag167-)(and (RIGHTOF6-ROBOT)))

(:derived (Flag168-)(and (RIGHTOF13-ROBOT)))

(:derived (Flag168-)(and (RIGHTOF11-ROBOT)))

(:derived (Flag168-)(and (RIGHTOF10-ROBOT)))

(:derived (Flag168-)(and (RIGHTOF8-ROBOT)))

(:derived (Flag168-)(and (RIGHTOF7-ROBOT)))

(:derived (Flag168-)(and (RIGHTOF9-ROBOT)))

(:derived (Flag168-)(and (RIGHTOF12-ROBOT)))

(:derived (Flag169-)(and (RIGHTOF13-ROBOT)))

(:derived (Flag169-)(and (RIGHTOF11-ROBOT)))

(:derived (Flag169-)(and (RIGHTOF10-ROBOT)))

(:derived (Flag169-)(and (RIGHTOF8-ROBOT)))

(:derived (Flag169-)(and (RIGHTOF9-ROBOT)))

(:derived (Flag169-)(and (RIGHTOF12-ROBOT)))

(:derived (Flag170-)(and (RIGHTOF10-ROBOT)))

(:derived (Flag170-)(and (RIGHTOF9-ROBOT)))

(:derived (Flag170-)(and (RIGHTOF13-ROBOT)))

(:derived (Flag170-)(and (RIGHTOF12-ROBOT)))

(:derived (Flag170-)(and (RIGHTOF11-ROBOT)))

(:derived (Flag171-)(and (RIGHTOF10-ROBOT)))

(:derived (Flag171-)(and (RIGHTOF13-ROBOT)))

(:derived (Flag171-)(and (RIGHTOF12-ROBOT)))

(:derived (Flag171-)(and (RIGHTOF11-ROBOT)))

(:derived (Flag172-)(and (RIGHTOF13-ROBOT)))

(:derived (Flag172-)(and (RIGHTOF12-ROBOT)))

(:derived (Flag172-)(and (RIGHTOF11-ROBOT)))

(:derived (Flag173-)(and (RIGHTOF13-ROBOT)))

(:derived (Flag173-)(and (RIGHTOF12-ROBOT)))

(:derived (Flag174-)(and (LEFTOF1-ROBOT)))

(:derived (Flag174-)(and (LEFTOF2-ROBOT)))

(:derived (Flag175-)(and (LEFTOF1-ROBOT)))

(:derived (Flag175-)(and (LEFTOF3-ROBOT)))

(:derived (Flag175-)(and (LEFTOF2-ROBOT)))

(:derived (Flag176-)(and (LEFTOF1-ROBOT)))

(:derived (Flag176-)(and (LEFTOF3-ROBOT)))

(:derived (Flag176-)(and (LEFTOF4-ROBOT)))

(:derived (Flag176-)(and (LEFTOF2-ROBOT)))

(:derived (Flag177-)(and (LEFTOF5-ROBOT)))

(:derived (Flag177-)(and (LEFTOF1-ROBOT)))

(:derived (Flag177-)(and (LEFTOF3-ROBOT)))

(:derived (Flag177-)(and (LEFTOF4-ROBOT)))

(:derived (Flag177-)(and (LEFTOF2-ROBOT)))

(:derived (Flag178-)(and (LEFTOF2-ROBOT)))

(:derived (Flag178-)(and (LEFTOF1-ROBOT)))

(:derived (Flag178-)(and (LEFTOF6-ROBOT)))

(:derived (Flag178-)(and (LEFTOF5-ROBOT)))

(:derived (Flag178-)(and (LEFTOF3-ROBOT)))

(:derived (Flag178-)(and (LEFTOF4-ROBOT)))

(:derived (Flag179-)(and (LEFTOF2-ROBOT)))

(:derived (Flag179-)(and (LEFTOF7-ROBOT)))

(:derived (Flag179-)(and (LEFTOF1-ROBOT)))

(:derived (Flag179-)(and (LEFTOF6-ROBOT)))

(:derived (Flag179-)(and (LEFTOF5-ROBOT)))

(:derived (Flag179-)(and (LEFTOF3-ROBOT)))

(:derived (Flag179-)(and (LEFTOF4-ROBOT)))

(:derived (Flag180-)(and (LEFTOF2-ROBOT)))

(:derived (Flag180-)(and (LEFTOF7-ROBOT)))

(:derived (Flag180-)(and (LEFTOF1-ROBOT)))

(:derived (Flag180-)(and (LEFTOF6-ROBOT)))

(:derived (Flag180-)(and (LEFTOF5-ROBOT)))

(:derived (Flag180-)(and (LEFTOF3-ROBOT)))

(:derived (Flag180-)(and (LEFTOF4-ROBOT)))

(:derived (Flag180-)(and (LEFTOF8-ROBOT)))

(:derived (Flag181-)(and (LEFTOF9-ROBOT)))

(:derived (Flag181-)(and (LEFTOF2-ROBOT)))

(:derived (Flag181-)(and (LEFTOF7-ROBOT)))

(:derived (Flag181-)(and (LEFTOF1-ROBOT)))

(:derived (Flag181-)(and (LEFTOF6-ROBOT)))

(:derived (Flag181-)(and (LEFTOF5-ROBOT)))

(:derived (Flag181-)(and (LEFTOF3-ROBOT)))

(:derived (Flag181-)(and (LEFTOF4-ROBOT)))

(:derived (Flag181-)(and (LEFTOF8-ROBOT)))

(:derived (Flag182-)(and (LEFTOF10-ROBOT)))

(:derived (Flag182-)(and (LEFTOF9-ROBOT)))

(:derived (Flag182-)(and (LEFTOF2-ROBOT)))

(:derived (Flag182-)(and (LEFTOF7-ROBOT)))

(:derived (Flag182-)(and (LEFTOF1-ROBOT)))

(:derived (Flag182-)(and (LEFTOF6-ROBOT)))

(:derived (Flag182-)(and (LEFTOF5-ROBOT)))

(:derived (Flag182-)(and (LEFTOF3-ROBOT)))

(:derived (Flag182-)(and (LEFTOF4-ROBOT)))

(:derived (Flag182-)(and (LEFTOF8-ROBOT)))

(:derived (Flag183-)(and (LEFTOF10-ROBOT)))

(:derived (Flag183-)(and (LEFTOF11-ROBOT)))

(:derived (Flag183-)(and (LEFTOF9-ROBOT)))

(:derived (Flag183-)(and (LEFTOF2-ROBOT)))

(:derived (Flag183-)(and (LEFTOF7-ROBOT)))

(:derived (Flag183-)(and (LEFTOF1-ROBOT)))

(:derived (Flag183-)(and (LEFTOF6-ROBOT)))

(:derived (Flag183-)(and (LEFTOF5-ROBOT)))

(:derived (Flag183-)(and (LEFTOF3-ROBOT)))

(:derived (Flag183-)(and (LEFTOF4-ROBOT)))

(:derived (Flag183-)(and (LEFTOF8-ROBOT)))

(:derived (Flag184-)(and (LEFTOF10-ROBOT)))

(:derived (Flag184-)(and (LEFTOF11-ROBOT)))

(:derived (Flag184-)(and (LEFTOF9-ROBOT)))

(:derived (Flag184-)(and (LEFTOF2-ROBOT)))

(:derived (Flag184-)(and (LEFTOF7-ROBOT)))

(:derived (Flag184-)(and (LEFTOF1-ROBOT)))

(:derived (Flag184-)(and (LEFTOF12-ROBOT)))

(:derived (Flag184-)(and (LEFTOF6-ROBOT)))

(:derived (Flag184-)(and (LEFTOF5-ROBOT)))

(:derived (Flag184-)(and (LEFTOF3-ROBOT)))

(:derived (Flag184-)(and (LEFTOF4-ROBOT)))

(:derived (Flag184-)(and (LEFTOF8-ROBOT)))

(:derived (Flag185-)(and (LEFTOF10-ROBOT)))

(:derived (Flag185-)(and (LEFTOF11-ROBOT)))

(:derived (Flag185-)(and (LEFTOF9-ROBOT)))

(:derived (Flag185-)(and (LEFTOF2-ROBOT)))

(:derived (Flag185-)(and (LEFTOF1-ROBOT)))

(:derived (Flag185-)(and (LEFTOF12-ROBOT)))

(:derived (Flag185-)(and (LEFTOF6-ROBOT)))

(:derived (Flag185-)(and (LEFTOF5-ROBOT)))

(:derived (Flag185-)(and (LEFTOF3-ROBOT)))

(:derived (Flag185-)(and (LEFTOF4-ROBOT)))

(:derived (Flag185-)(and (LEFTOF7-ROBOT)))

(:derived (Flag185-)(and (LEFTOF8-ROBOT)))

(:derived (Flag186-)(and (LEFTOF1-ROBOT)(RIGHTOF4-ROBOT)))

(:derived (Flag187-)(and (LEFTOF1-ROBOT)(RIGHTOF12-ROBOT)))

(:derived (Flag188-)(and (LEFTOF1-ROBOT)(RIGHTOF7-ROBOT)))

(:derived (Flag189-)(and (LEFTOF1-ROBOT)(RIGHTOF9-ROBOT)))

(:derived (Flag190-)(and (RIGHTOF13-ROBOT)(LEFTOF1-ROBOT)))

(:derived (Flag191-)(and (LEFTOF1-ROBOT)(RIGHTOF1-ROBOT)))

(:derived (Flag192-)(and (RIGHTOF3-ROBOT)(LEFTOF1-ROBOT)))

(:derived (Flag193-)(and (RIGHTOF0-ROBOT)(LEFTOF1-ROBOT)))

(:derived (Flag194-)(and (LEFTOF1-ROBOT)(RIGHTOF5-ROBOT)))

(:derived (Flag195-)(and (LEFTOF1-ROBOT)(RIGHTOF2-ROBOT)))

(:derived (Flag196-)(and (RIGHTOF11-ROBOT)(LEFTOF1-ROBOT)))

(:derived (Flag197-)(and (RIGHTOF8-ROBOT)(LEFTOF1-ROBOT)))

(:derived (Flag198-)(and (LEFTOF1-ROBOT)(RIGHTOF6-ROBOT)))

(:derived (Flag199-)(and (RIGHTOF10-ROBOT)(LEFTOF1-ROBOT)))

(:derived (Flag201-)(and (RIGHTOF13-ROBOT)(LEFTOF2-ROBOT)))

(:derived (Flag202-)(and (LEFTOF2-ROBOT)(RIGHTOF9-ROBOT)))

(:derived (Flag203-)(and (LEFTOF2-ROBOT)(RIGHTOF1-ROBOT)))

(:derived (Flag204-)(and (RIGHTOF5-ROBOT)(LEFTOF2-ROBOT)))

(:derived (Flag205-)(and (LEFTOF2-ROBOT)(RIGHTOF7-ROBOT)))

(:derived (Flag206-)(and (RIGHTOF5-ROBOT)(LEFTOF1-ROBOT)))

(:derived (Flag207-)(and (RIGHTOF3-ROBOT)(LEFTOF2-ROBOT)))

(:derived (Flag208-)(and (RIGHTOF10-ROBOT)(LEFTOF2-ROBOT)))

(:derived (Flag209-)(and (LEFTOF2-ROBOT)(RIGHTOF12-ROBOT)))

(:derived (Flag210-)(and (RIGHTOF11-ROBOT)(LEFTOF2-ROBOT)))

(:derived (Flag211-)(and (RIGHTOF8-ROBOT)(LEFTOF2-ROBOT)))

(:derived (Flag212-)(and (RIGHTOF2-ROBOT)(LEFTOF2-ROBOT)))

(:derived (Flag213-)(and (LEFTOF2-ROBOT)(RIGHTOF6-ROBOT)))

(:derived (Flag214-)(and (LEFTOF2-ROBOT)(RIGHTOF4-ROBOT)))

(:derived (Flag216-)(and (LEFTOF3-ROBOT)(RIGHTOF6-ROBOT)))

(:derived (Flag217-)(and (LEFTOF3-ROBOT)(RIGHTOF4-ROBOT)))

(:derived (Flag218-)(and (LEFTOF3-ROBOT)(RIGHTOF7-ROBOT)))

(:derived (Flag219-)(and (LEFTOF3-ROBOT)(RIGHTOF12-ROBOT)))

(:derived (Flag220-)(and (LEFTOF3-ROBOT)(RIGHTOF10-ROBOT)))

(:derived (Flag221-)(and (LEFTOF3-ROBOT)(RIGHTOF8-ROBOT)))

(:derived (Flag222-)(and (LEFTOF3-ROBOT)(RIGHTOF5-ROBOT)))

(:derived (Flag223-)(and (RIGHTOF3-ROBOT)(LEFTOF3-ROBOT)))

(:derived (Flag224-)(and (LEFTOF3-ROBOT)(RIGHTOF11-ROBOT)))

(:derived (Flag225-)(and (RIGHTOF13-ROBOT)(LEFTOF3-ROBOT)))

(:derived (Flag226-)(and (LEFTOF3-ROBOT)(RIGHTOF9-ROBOT)))

(:derived (Flag227-)(and (LEFTOF3-ROBOT)(RIGHTOF2-ROBOT)))

(:derived (Flag229-)(and (LEFTOF3-ROBOT)(RIGHTOF13-ROBOT)))

(:derived (Flag230-)(and (LEFTOF4-ROBOT)(RIGHTOF4-ROBOT)))

(:derived (Flag231-)(and (LEFTOF4-ROBOT)(RIGHTOF7-ROBOT)))

(:derived (Flag232-)(and (LEFTOF3-ROBOT)(RIGHTOF3-ROBOT)))

(:derived (Flag233-)(and (RIGHTOF8-ROBOT)(LEFTOF4-ROBOT)))

(:derived (Flag234-)(and (LEFTOF4-ROBOT)(RIGHTOF12-ROBOT)))

(:derived (Flag235-)(and (RIGHTOF10-ROBOT)(LEFTOF4-ROBOT)))

(:derived (Flag236-)(and (LEFTOF4-ROBOT)(RIGHTOF9-ROBOT)))

(:derived (Flag237-)(and (LEFTOF4-ROBOT)(RIGHTOF6-ROBOT)))

(:derived (Flag238-)(and (LEFTOF4-ROBOT)(RIGHTOF5-ROBOT)))

(:derived (Flag239-)(and (RIGHTOF13-ROBOT)(LEFTOF4-ROBOT)))

(:derived (Flag240-)(and (RIGHTOF3-ROBOT)(LEFTOF4-ROBOT)))

(:derived (Flag241-)(and (LEFTOF4-ROBOT)(RIGHTOF11-ROBOT)))

(:derived (Flag243-)(and (RIGHTOF9-ROBOT)(LEFTOF5-ROBOT)))

(:derived (Flag244-)(and (RIGHTOF7-ROBOT)(LEFTOF5-ROBOT)))

(:derived (Flag245-)(and (RIGHTOF11-ROBOT)(LEFTOF5-ROBOT)))

(:derived (Flag246-)(and (RIGHTOF13-ROBOT)(LEFTOF5-ROBOT)))

(:derived (Flag247-)(and (LEFTOF4-ROBOT)(RIGHTOF10-ROBOT)))

(:derived (Flag248-)(and (RIGHTOF4-ROBOT)(LEFTOF5-ROBOT)))

(:derived (Flag249-)(and (LEFTOF4-ROBOT)(RIGHTOF8-ROBOT)))

(:derived (Flag250-)(and (LEFTOF5-ROBOT)(RIGHTOF6-ROBOT)))

(:derived (Flag251-)(and (RIGHTOF10-ROBOT)(LEFTOF5-ROBOT)))

(:derived (Flag252-)(and (LEFTOF5-ROBOT)(RIGHTOF12-ROBOT)))

(:derived (Flag253-)(and (RIGHTOF8-ROBOT)(LEFTOF5-ROBOT)))

(:derived (Flag254-)(and (RIGHTOF5-ROBOT)(LEFTOF5-ROBOT)))

(:derived (Flag256-)(and (RIGHTOF12-ROBOT)(LEFTOF6-ROBOT)))

(:derived (Flag257-)(and (RIGHTOF8-ROBOT)(LEFTOF6-ROBOT)))

(:derived (Flag258-)(and (RIGHTOF11-ROBOT)(LEFTOF6-ROBOT)))

(:derived (Flag259-)(and (RIGHTOF5-ROBOT)(LEFTOF6-ROBOT)))

(:derived (Flag260-)(and (RIGHTOF7-ROBOT)(LEFTOF6-ROBOT)))

(:derived (Flag261-)(and (RIGHTOF10-ROBOT)(LEFTOF6-ROBOT)))

(:derived (Flag262-)(and (RIGHTOF6-ROBOT)(LEFTOF6-ROBOT)))

(:derived (Flag263-)(and (RIGHTOF13-ROBOT)(LEFTOF6-ROBOT)))

(:derived (Flag264-)(and (RIGHTOF9-ROBOT)(LEFTOF6-ROBOT)))

(:derived (Flag266-)(and (RIGHTOF11-ROBOT)(LEFTOF7-ROBOT)))

(:derived (Flag267-)(and (RIGHTOF8-ROBOT)(LEFTOF7-ROBOT)))

(:derived (Flag268-)(and (LEFTOF7-ROBOT)(RIGHTOF7-ROBOT)))

(:derived (Flag269-)(and (RIGHTOF13-ROBOT)(LEFTOF7-ROBOT)))

(:derived (Flag270-)(and (LEFTOF6-ROBOT)(RIGHTOF6-ROBOT)))

(:derived (Flag271-)(and (LEFTOF7-ROBOT)(RIGHTOF12-ROBOT)))

(:derived (Flag272-)(and (LEFTOF7-ROBOT)(RIGHTOF9-ROBOT)))

(:derived (Flag273-)(and (LEFTOF7-ROBOT)(RIGHTOF6-ROBOT)))

(:derived (Flag274-)(and (LEFTOF6-ROBOT)(RIGHTOF12-ROBOT)))

(:derived (Flag275-)(and (RIGHTOF10-ROBOT)(LEFTOF7-ROBOT)))

(:derived (Flag277-)(and (RIGHTOF7-ROBOT)(LEFTOF8-ROBOT)))

(:derived (Flag278-)(and (LEFTOF8-ROBOT)(RIGHTOF9-ROBOT)))

(:derived (Flag279-)(and (RIGHTOF13-ROBOT)(LEFTOF8-ROBOT)))

(:derived (Flag280-)(and (RIGHTOF10-ROBOT)(LEFTOF8-ROBOT)))

(:derived (Flag281-)(and (RIGHTOF8-ROBOT)(LEFTOF8-ROBOT)))

(:derived (Flag282-)(and (RIGHTOF7-ROBOT)(LEFTOF7-ROBOT)))

(:derived (Flag283-)(and (LEFTOF8-ROBOT)(RIGHTOF12-ROBOT)))

(:derived (Flag284-)(and (RIGHTOF11-ROBOT)(LEFTOF8-ROBOT)))

(:derived (Flag286-)(and (RIGHTOF13-ROBOT)(LEFTOF9-ROBOT)))

(:derived (Flag287-)(and (RIGHTOF8-ROBOT)(LEFTOF9-ROBOT)))

(:derived (Flag288-)(and (LEFTOF9-ROBOT)(RIGHTOF9-ROBOT)))

(:derived (Flag289-)(and (RIGHTOF10-ROBOT)(LEFTOF9-ROBOT)))

(:derived (Flag290-)(and (RIGHTOF11-ROBOT)(LEFTOF9-ROBOT)))

(:derived (Flag291-)(and (LEFTOF9-ROBOT)(RIGHTOF12-ROBOT)))

(:derived (Flag293-)(and (LEFTOF10-ROBOT)(RIGHTOF12-ROBOT)))

(:derived (Flag294-)(and (RIGHTOF11-ROBOT)(LEFTOF10-ROBOT)))

(:derived (Flag295-)(and (RIGHTOF10-ROBOT)(LEFTOF10-ROBOT)))

(:derived (Flag296-)(and (RIGHTOF13-ROBOT)(LEFTOF10-ROBOT)))

(:derived (Flag297-)(and (LEFTOF10-ROBOT)(RIGHTOF9-ROBOT)))

(:derived (Flag299-)(and (RIGHTOF11-ROBOT)(LEFTOF11-ROBOT)))

(:derived (Flag300-)(and (RIGHTOF13-ROBOT)(LEFTOF11-ROBOT)))

(:derived (Flag301-)(and (RIGHTOF10-ROBOT)(LEFTOF11-ROBOT)))

(:derived (Flag302-)(and (LEFTOF11-ROBOT)(RIGHTOF12-ROBOT)))

(:derived (Flag304-)(and (RIGHTOF12-ROBOT)(LEFTOF12-ROBOT)))

(:derived (Flag305-)(and (RIGHTOF11-ROBOT)(LEFTOF12-ROBOT)))

(:derived (Flag306-)(and (RIGHTOF13-ROBOT)(LEFTOF12-ROBOT)))

(:derived (Flag308-)(and (LEFTOF13-ROBOT)(RIGHTOF12-ROBOT)))

(:derived (Flag309-)(and (RIGHTOF13-ROBOT)(LEFTOF13-ROBOT)))

(:derived (Flag310-)(and (LEFTOF12-ROBOT)(RIGHTOF12-ROBOT)))

(:derived (Flag312-)(and (LEFTOF10-ROBOT)))

(:derived (Flag312-)(and (LEFTOF11-ROBOT)))

(:derived (Flag312-)(and (LEFTOF9-ROBOT)))

(:derived (Flag312-)(and (LEFTOF2-ROBOT)))

(:derived (Flag312-)(and (LEFTOF1-ROBOT)))

(:derived (Flag312-)(and (LEFTOF12-ROBOT)))

(:derived (Flag312-)(and (LEFTOF6-ROBOT)))

(:derived (Flag312-)(and (LEFTOF5-ROBOT)))

(:derived (Flag312-)(and (LEFTOF14-ROBOT)))

(:derived (Flag312-)(and (LEFTOF3-ROBOT)))

(:derived (Flag312-)(and (LEFTOF4-ROBOT)))

(:derived (Flag312-)(and (LEFTOF7-ROBOT)))

(:derived (Flag312-)(and (LEFTOF8-ROBOT)))

(:derived (Flag313-)(and (RIGHTOF13-ROBOT)(LEFTOF14-ROBOT)))

(:action MOVELEFT-ROBOT
:parameters ()
:precondition
(and
(Flag3-)
)
:effect
(and
(when
(and
(Flag314-)
)
(and
(COLUMN12-ROBOT)
(not (NOT-COLUMN12-ROBOT))
)
)
(when
(and
(Flag311-)
)
(and
(COLUMN11-ROBOT)
(not (NOT-COLUMN11-ROBOT))
)
)
(when
(and
(Flag307-)
)
(and
(COLUMN10-ROBOT)
(not (NOT-COLUMN10-ROBOT))
)
)
(when
(and
(Flag303-)
)
(and
(COLUMN9-ROBOT)
(not (NOT-COLUMN9-ROBOT))
)
)
(when
(and
(Flag298-)
)
(and
(COLUMN8-ROBOT)
(not (NOT-COLUMN8-ROBOT))
)
)
(when
(and
(Flag292-)
)
(and
(COLUMN7-ROBOT)
(not (NOT-COLUMN7-ROBOT))
)
)
(when
(and
(Flag285-)
)
(and
(COLUMN6-ROBOT)
(not (NOT-COLUMN6-ROBOT))
)
)
(when
(and
(Flag276-)
)
(and
(COLUMN5-ROBOT)
(not (NOT-COLUMN5-ROBOT))
)
)
(when
(and
(Flag265-)
)
(and
(COLUMN4-ROBOT)
(not (NOT-COLUMN4-ROBOT))
)
)
(when
(and
(Flag255-)
)
(and
(COLUMN3-ROBOT)
(not (NOT-COLUMN3-ROBOT))
)
)
(when
(and
(Flag242-)
)
(and
(COLUMN2-ROBOT)
(not (NOT-COLUMN2-ROBOT))
)
)
(when
(and
(Flag228-)
)
(and
(COLUMN1-ROBOT)
(not (NOT-COLUMN1-ROBOT))
)
)
(when
(and
(Flag215-)
)
(and
(COLUMN0-ROBOT)
(not (NOT-COLUMN0-ROBOT))
)
)
(when
(and
(COLUMN13-ROBOT)
)
(and
(COLUMN12-ROBOT)
(NOT-COLUMN13-ROBOT)
(not (NOT-COLUMN12-ROBOT))
(not (COLUMN13-ROBOT))
)
)
(when
(and
(COLUMN12-ROBOT)
)
(and
(COLUMN11-ROBOT)
(NOT-COLUMN12-ROBOT)
(not (NOT-COLUMN11-ROBOT))
(not (COLUMN12-ROBOT))
)
)
(when
(and
(COLUMN11-ROBOT)
)
(and
(COLUMN10-ROBOT)
(NOT-COLUMN11-ROBOT)
(not (NOT-COLUMN10-ROBOT))
(not (COLUMN11-ROBOT))
)
)
(when
(and
(COLUMN10-ROBOT)
)
(and
(COLUMN9-ROBOT)
(NOT-COLUMN10-ROBOT)
(not (NOT-COLUMN9-ROBOT))
(not (COLUMN10-ROBOT))
)
)
(when
(and
(COLUMN9-ROBOT)
)
(and
(COLUMN8-ROBOT)
(NOT-COLUMN9-ROBOT)
(not (NOT-COLUMN8-ROBOT))
(not (COLUMN9-ROBOT))
)
)
(when
(and
(COLUMN8-ROBOT)
)
(and
(COLUMN7-ROBOT)
(NOT-COLUMN8-ROBOT)
(not (NOT-COLUMN7-ROBOT))
(not (COLUMN8-ROBOT))
)
)
(when
(and
(COLUMN7-ROBOT)
)
(and
(COLUMN6-ROBOT)
(NOT-COLUMN7-ROBOT)
(not (NOT-COLUMN6-ROBOT))
(not (COLUMN7-ROBOT))
)
)
(when
(and
(COLUMN6-ROBOT)
)
(and
(COLUMN5-ROBOT)
(NOT-COLUMN6-ROBOT)
(not (NOT-COLUMN5-ROBOT))
(not (COLUMN6-ROBOT))
)
)
(when
(and
(COLUMN5-ROBOT)
)
(and
(COLUMN4-ROBOT)
(NOT-COLUMN5-ROBOT)
(not (NOT-COLUMN4-ROBOT))
(not (COLUMN5-ROBOT))
)
)
(when
(and
(COLUMN4-ROBOT)
)
(and
(COLUMN3-ROBOT)
(NOT-COLUMN4-ROBOT)
(not (NOT-COLUMN3-ROBOT))
(not (COLUMN4-ROBOT))
)
)
(when
(and
(COLUMN3-ROBOT)
)
(and
(COLUMN2-ROBOT)
(NOT-COLUMN3-ROBOT)
(not (NOT-COLUMN2-ROBOT))
(not (COLUMN3-ROBOT))
)
)
(when
(and
(COLUMN2-ROBOT)
)
(and
(COLUMN1-ROBOT)
(NOT-COLUMN2-ROBOT)
(not (NOT-COLUMN1-ROBOT))
(not (COLUMN2-ROBOT))
)
)
(when
(and
(COLUMN1-ROBOT)
)
(and
(COLUMN0-ROBOT)
(NOT-COLUMN1-ROBOT)
(not (NOT-COLUMN0-ROBOT))
(not (COLUMN1-ROBOT))
)
)
(when
(and
(RIGHTOF13-ROBOT)
)
(and
(RIGHTOF12-ROBOT)
(NOT-RIGHTOF13-ROBOT)
(RIGHTOF11-ROBOT)
(NOT-RIGHTOF12-ROBOT)
(RIGHTOF10-ROBOT)
(NOT-RIGHTOF11-ROBOT)
(RIGHTOF9-ROBOT)
(NOT-RIGHTOF10-ROBOT)
(RIGHTOF8-ROBOT)
(NOT-RIGHTOF9-ROBOT)
(RIGHTOF7-ROBOT)
(NOT-RIGHTOF8-ROBOT)
(RIGHTOF6-ROBOT)
(NOT-RIGHTOF7-ROBOT)
(RIGHTOF5-ROBOT)
(NOT-RIGHTOF6-ROBOT)
(RIGHTOF4-ROBOT)
(NOT-RIGHTOF5-ROBOT)
(RIGHTOF3-ROBOT)
(NOT-RIGHTOF4-ROBOT)
(RIGHTOF2-ROBOT)
(NOT-RIGHTOF3-ROBOT)
(RIGHTOF1-ROBOT)
(NOT-RIGHTOF2-ROBOT)
(RIGHTOF0-ROBOT)
(NOT-RIGHTOF1-ROBOT)
(not (RIGHTOF13-ROBOT))
(not (NOT-RIGHTOF0-ROBOT))
)
)
(when
(and
(RIGHTOF12-ROBOT)
)
(and
(RIGHTOF11-ROBOT)
(NOT-RIGHTOF12-ROBOT)
(RIGHTOF10-ROBOT)
(NOT-RIGHTOF11-ROBOT)
(RIGHTOF9-ROBOT)
(NOT-RIGHTOF10-ROBOT)
(RIGHTOF8-ROBOT)
(NOT-RIGHTOF9-ROBOT)
(RIGHTOF7-ROBOT)
(NOT-RIGHTOF8-ROBOT)
(RIGHTOF6-ROBOT)
(NOT-RIGHTOF7-ROBOT)
(RIGHTOF5-ROBOT)
(NOT-RIGHTOF6-ROBOT)
(RIGHTOF4-ROBOT)
(NOT-RIGHTOF5-ROBOT)
(RIGHTOF3-ROBOT)
(NOT-RIGHTOF4-ROBOT)
(RIGHTOF2-ROBOT)
(NOT-RIGHTOF3-ROBOT)
(RIGHTOF1-ROBOT)
(NOT-RIGHTOF2-ROBOT)
(RIGHTOF0-ROBOT)
(NOT-RIGHTOF1-ROBOT)
(not (RIGHTOF12-ROBOT))
(not (NOT-RIGHTOF0-ROBOT))
)
)
(when
(and
(RIGHTOF11-ROBOT)
)
(and
(RIGHTOF10-ROBOT)
(NOT-RIGHTOF11-ROBOT)
(RIGHTOF9-ROBOT)
(NOT-RIGHTOF10-ROBOT)
(RIGHTOF8-ROBOT)
(NOT-RIGHTOF9-ROBOT)
(RIGHTOF7-ROBOT)
(NOT-RIGHTOF8-ROBOT)
(RIGHTOF6-ROBOT)
(NOT-RIGHTOF7-ROBOT)
(RIGHTOF5-ROBOT)
(NOT-RIGHTOF6-ROBOT)
(RIGHTOF4-ROBOT)
(NOT-RIGHTOF5-ROBOT)
(RIGHTOF3-ROBOT)
(NOT-RIGHTOF4-ROBOT)
(RIGHTOF2-ROBOT)
(NOT-RIGHTOF3-ROBOT)
(RIGHTOF1-ROBOT)
(NOT-RIGHTOF2-ROBOT)
(RIGHTOF0-ROBOT)
(NOT-RIGHTOF1-ROBOT)
(not (RIGHTOF11-ROBOT))
(not (NOT-RIGHTOF0-ROBOT))
)
)
(when
(and
(RIGHTOF10-ROBOT)
)
(and
(RIGHTOF9-ROBOT)
(NOT-RIGHTOF10-ROBOT)
(RIGHTOF8-ROBOT)
(NOT-RIGHTOF9-ROBOT)
(RIGHTOF7-ROBOT)
(NOT-RIGHTOF8-ROBOT)
(RIGHTOF6-ROBOT)
(NOT-RIGHTOF7-ROBOT)
(RIGHTOF5-ROBOT)
(NOT-RIGHTOF6-ROBOT)
(RIGHTOF4-ROBOT)
(NOT-RIGHTOF5-ROBOT)
(RIGHTOF3-ROBOT)
(NOT-RIGHTOF4-ROBOT)
(RIGHTOF2-ROBOT)
(NOT-RIGHTOF3-ROBOT)
(RIGHTOF1-ROBOT)
(NOT-RIGHTOF2-ROBOT)
(RIGHTOF0-ROBOT)
(NOT-RIGHTOF1-ROBOT)
(not (RIGHTOF10-ROBOT))
(not (NOT-RIGHTOF0-ROBOT))
)
)
(when
(and
(RIGHTOF9-ROBOT)
)
(and
(RIGHTOF8-ROBOT)
(NOT-RIGHTOF9-ROBOT)
(RIGHTOF7-ROBOT)
(NOT-RIGHTOF8-ROBOT)
(RIGHTOF6-ROBOT)
(NOT-RIGHTOF7-ROBOT)
(RIGHTOF5-ROBOT)
(NOT-RIGHTOF6-ROBOT)
(RIGHTOF4-ROBOT)
(NOT-RIGHTOF5-ROBOT)
(RIGHTOF3-ROBOT)
(NOT-RIGHTOF4-ROBOT)
(RIGHTOF2-ROBOT)
(NOT-RIGHTOF3-ROBOT)
(RIGHTOF1-ROBOT)
(NOT-RIGHTOF2-ROBOT)
(RIGHTOF0-ROBOT)
(NOT-RIGHTOF1-ROBOT)
(not (RIGHTOF9-ROBOT))
(not (NOT-RIGHTOF0-ROBOT))
)
)
(when
(and
(RIGHTOF8-ROBOT)
)
(and
(RIGHTOF7-ROBOT)
(NOT-RIGHTOF8-ROBOT)
(RIGHTOF6-ROBOT)
(NOT-RIGHTOF7-ROBOT)
(RIGHTOF5-ROBOT)
(NOT-RIGHTOF6-ROBOT)
(RIGHTOF4-ROBOT)
(NOT-RIGHTOF5-ROBOT)
(RIGHTOF3-ROBOT)
(NOT-RIGHTOF4-ROBOT)
(RIGHTOF2-ROBOT)
(NOT-RIGHTOF3-ROBOT)
(RIGHTOF1-ROBOT)
(NOT-RIGHTOF2-ROBOT)
(RIGHTOF0-ROBOT)
(NOT-RIGHTOF1-ROBOT)
(not (RIGHTOF8-ROBOT))
(not (NOT-RIGHTOF0-ROBOT))
)
)
(when
(and
(RIGHTOF7-ROBOT)
)
(and
(RIGHTOF6-ROBOT)
(NOT-RIGHTOF7-ROBOT)
(RIGHTOF5-ROBOT)
(NOT-RIGHTOF6-ROBOT)
(RIGHTOF4-ROBOT)
(NOT-RIGHTOF5-ROBOT)
(RIGHTOF3-ROBOT)
(NOT-RIGHTOF4-ROBOT)
(RIGHTOF2-ROBOT)
(NOT-RIGHTOF3-ROBOT)
(RIGHTOF1-ROBOT)
(NOT-RIGHTOF2-ROBOT)
(RIGHTOF0-ROBOT)
(NOT-RIGHTOF1-ROBOT)
(not (RIGHTOF7-ROBOT))
(not (NOT-RIGHTOF0-ROBOT))
)
)
(when
(and
(RIGHTOF6-ROBOT)
)
(and
(RIGHTOF5-ROBOT)
(NOT-RIGHTOF6-ROBOT)
(RIGHTOF4-ROBOT)
(NOT-RIGHTOF5-ROBOT)
(RIGHTOF3-ROBOT)
(NOT-RIGHTOF4-ROBOT)
(RIGHTOF2-ROBOT)
(NOT-RIGHTOF3-ROBOT)
(RIGHTOF1-ROBOT)
(NOT-RIGHTOF2-ROBOT)
(RIGHTOF0-ROBOT)
(NOT-RIGHTOF1-ROBOT)
(not (RIGHTOF6-ROBOT))
(not (NOT-RIGHTOF0-ROBOT))
)
)
(when
(and
(RIGHTOF5-ROBOT)
)
(and
(RIGHTOF4-ROBOT)
(NOT-RIGHTOF5-ROBOT)
(RIGHTOF3-ROBOT)
(NOT-RIGHTOF4-ROBOT)
(RIGHTOF2-ROBOT)
(NOT-RIGHTOF3-ROBOT)
(RIGHTOF1-ROBOT)
(NOT-RIGHTOF2-ROBOT)
(RIGHTOF0-ROBOT)
(NOT-RIGHTOF1-ROBOT)
(not (RIGHTOF5-ROBOT))
(not (NOT-RIGHTOF0-ROBOT))
)
)
(when
(and
(RIGHTOF4-ROBOT)
)
(and
(RIGHTOF3-ROBOT)
(NOT-RIGHTOF4-ROBOT)
(RIGHTOF2-ROBOT)
(NOT-RIGHTOF3-ROBOT)
(RIGHTOF1-ROBOT)
(NOT-RIGHTOF2-ROBOT)
(RIGHTOF0-ROBOT)
(NOT-RIGHTOF1-ROBOT)
(not (RIGHTOF4-ROBOT))
(not (NOT-RIGHTOF0-ROBOT))
)
)
(when
(and
(RIGHTOF3-ROBOT)
)
(and
(RIGHTOF2-ROBOT)
(NOT-RIGHTOF3-ROBOT)
(RIGHTOF1-ROBOT)
(NOT-RIGHTOF2-ROBOT)
(RIGHTOF0-ROBOT)
(NOT-RIGHTOF1-ROBOT)
(not (RIGHTOF3-ROBOT))
(not (NOT-RIGHTOF0-ROBOT))
)
)
(when
(and
(RIGHTOF2-ROBOT)
)
(and
(RIGHTOF1-ROBOT)
(NOT-RIGHTOF2-ROBOT)
(RIGHTOF0-ROBOT)
(NOT-RIGHTOF1-ROBOT)
(not (RIGHTOF2-ROBOT))
(not (NOT-RIGHTOF0-ROBOT))
)
)
(when
(and
(RIGHTOF1-ROBOT)
)
(and
(RIGHTOF0-ROBOT)
(NOT-RIGHTOF1-ROBOT)
(not (NOT-RIGHTOF0-ROBOT))
(not (RIGHTOF1-ROBOT))
)
)
(when
(and
(Flag312-)
)
(and
(LEFTOF13-ROBOT)
(not (NOT-LEFTOF13-ROBOT))
)
)
(when
(and
(LEFTOF8-ROBOT)
)
(and
(LEFTOF12-ROBOT)
(LEFTOF11-ROBOT)
(LEFTOF10-ROBOT)
(LEFTOF9-ROBOT)
(LEFTOF7-ROBOT)
(not (NOT-LEFTOF12-ROBOT))
(not (NOT-LEFTOF11-ROBOT))
(not (NOT-LEFTOF10-ROBOT))
(not (NOT-LEFTOF9-ROBOT))
(not (NOT-LEFTOF8-ROBOT))
(not (NOT-LEFTOF7-ROBOT))
)
)
(when
(and
(LEFTOF7-ROBOT)
)
(and
(LEFTOF12-ROBOT)
(LEFTOF11-ROBOT)
(LEFTOF10-ROBOT)
(LEFTOF9-ROBOT)
(LEFTOF8-ROBOT)
(LEFTOF6-ROBOT)
(not (NOT-LEFTOF12-ROBOT))
(not (NOT-LEFTOF11-ROBOT))
(not (NOT-LEFTOF10-ROBOT))
(not (NOT-LEFTOF9-ROBOT))
(not (NOT-LEFTOF8-ROBOT))
(not (NOT-LEFTOF7-ROBOT))
(not (NOT-LEFTOF6-ROBOT))
)
)
(when
(and
(LEFTOF4-ROBOT)
)
(and
(LEFTOF12-ROBOT)
(LEFTOF11-ROBOT)
(LEFTOF10-ROBOT)
(LEFTOF9-ROBOT)
(LEFTOF8-ROBOT)
(LEFTOF7-ROBOT)
(LEFTOF6-ROBOT)
(LEFTOF5-ROBOT)
(LEFTOF3-ROBOT)
(not (NOT-LEFTOF12-ROBOT))
(not (NOT-LEFTOF11-ROBOT))
(not (NOT-LEFTOF10-ROBOT))
(not (NOT-LEFTOF9-ROBOT))
(not (NOT-LEFTOF8-ROBOT))
(not (NOT-LEFTOF7-ROBOT))
(not (NOT-LEFTOF6-ROBOT))
(not (NOT-LEFTOF5-ROBOT))
(not (NOT-LEFTOF4-ROBOT))
(not (NOT-LEFTOF3-ROBOT))
)
)
(when
(and
(LEFTOF3-ROBOT)
)
(and
(LEFTOF12-ROBOT)
(LEFTOF11-ROBOT)
(LEFTOF10-ROBOT)
(LEFTOF9-ROBOT)
(LEFTOF8-ROBOT)
(LEFTOF7-ROBOT)
(LEFTOF6-ROBOT)
(LEFTOF5-ROBOT)
(LEFTOF4-ROBOT)
(LEFTOF2-ROBOT)
(not (NOT-LEFTOF12-ROBOT))
(not (NOT-LEFTOF11-ROBOT))
(not (NOT-LEFTOF10-ROBOT))
(not (NOT-LEFTOF9-ROBOT))
(not (NOT-LEFTOF8-ROBOT))
(not (NOT-LEFTOF7-ROBOT))
(not (NOT-LEFTOF6-ROBOT))
(not (NOT-LEFTOF5-ROBOT))
(not (NOT-LEFTOF4-ROBOT))
(not (NOT-LEFTOF3-ROBOT))
(not (NOT-LEFTOF2-ROBOT))
)
)
(when
(and
(LEFTOF5-ROBOT)
)
(and
(LEFTOF12-ROBOT)
(LEFTOF11-ROBOT)
(LEFTOF10-ROBOT)
(LEFTOF9-ROBOT)
(LEFTOF8-ROBOT)
(LEFTOF7-ROBOT)
(LEFTOF6-ROBOT)
(LEFTOF4-ROBOT)
(not (NOT-LEFTOF12-ROBOT))
(not (NOT-LEFTOF11-ROBOT))
(not (NOT-LEFTOF10-ROBOT))
(not (NOT-LEFTOF9-ROBOT))
(not (NOT-LEFTOF8-ROBOT))
(not (NOT-LEFTOF7-ROBOT))
(not (NOT-LEFTOF6-ROBOT))
(not (NOT-LEFTOF5-ROBOT))
(not (NOT-LEFTOF4-ROBOT))
)
)
(when
(and
(LEFTOF6-ROBOT)
)
(and
(LEFTOF12-ROBOT)
(LEFTOF11-ROBOT)
(LEFTOF10-ROBOT)
(LEFTOF9-ROBOT)
(LEFTOF8-ROBOT)
(LEFTOF7-ROBOT)
(LEFTOF5-ROBOT)
(not (NOT-LEFTOF12-ROBOT))
(not (NOT-LEFTOF11-ROBOT))
(not (NOT-LEFTOF10-ROBOT))
(not (NOT-LEFTOF9-ROBOT))
(not (NOT-LEFTOF8-ROBOT))
(not (NOT-LEFTOF7-ROBOT))
(not (NOT-LEFTOF6-ROBOT))
(not (NOT-LEFTOF5-ROBOT))
)
)
(when
(and
(LEFTOF12-ROBOT)
)
(and
(LEFTOF11-ROBOT)
(not (NOT-LEFTOF12-ROBOT))
(not (NOT-LEFTOF11-ROBOT))
)
)
(when
(and
(LEFTOF1-ROBOT)
)
(and
(LEFTOF12-ROBOT)
(LEFTOF11-ROBOT)
(LEFTOF10-ROBOT)
(LEFTOF9-ROBOT)
(LEFTOF8-ROBOT)
(LEFTOF7-ROBOT)
(LEFTOF6-ROBOT)
(LEFTOF5-ROBOT)
(LEFTOF4-ROBOT)
(LEFTOF3-ROBOT)
(LEFTOF2-ROBOT)
(not (NOT-LEFTOF12-ROBOT))
(not (NOT-LEFTOF11-ROBOT))
(not (NOT-LEFTOF10-ROBOT))
(not (NOT-LEFTOF9-ROBOT))
(not (NOT-LEFTOF8-ROBOT))
(not (NOT-LEFTOF7-ROBOT))
(not (NOT-LEFTOF6-ROBOT))
(not (NOT-LEFTOF5-ROBOT))
(not (NOT-LEFTOF4-ROBOT))
(not (NOT-LEFTOF3-ROBOT))
(not (NOT-LEFTOF2-ROBOT))
(not (NOT-LEFTOF1-ROBOT))
)
)
(when
(and
(LEFTOF13-ROBOT)
)
(and
(LEFTOF12-ROBOT)
(not (NOT-LEFTOF12-ROBOT))
)
)
(when
(and
(LEFTOF2-ROBOT)
)
(and
(LEFTOF12-ROBOT)
(LEFTOF11-ROBOT)
(LEFTOF10-ROBOT)
(LEFTOF9-ROBOT)
(LEFTOF8-ROBOT)
(LEFTOF7-ROBOT)
(LEFTOF6-ROBOT)
(LEFTOF5-ROBOT)
(LEFTOF4-ROBOT)
(LEFTOF3-ROBOT)
(LEFTOF1-ROBOT)
(not (NOT-LEFTOF12-ROBOT))
(not (NOT-LEFTOF11-ROBOT))
(not (NOT-LEFTOF10-ROBOT))
(not (NOT-LEFTOF9-ROBOT))
(not (NOT-LEFTOF8-ROBOT))
(not (NOT-LEFTOF7-ROBOT))
(not (NOT-LEFTOF6-ROBOT))
(not (NOT-LEFTOF5-ROBOT))
(not (NOT-LEFTOF4-ROBOT))
(not (NOT-LEFTOF3-ROBOT))
(not (NOT-LEFTOF2-ROBOT))
(not (NOT-LEFTOF1-ROBOT))
)
)
(when
(and
(LEFTOF9-ROBOT)
)
(and
(LEFTOF12-ROBOT)
(LEFTOF11-ROBOT)
(LEFTOF10-ROBOT)
(LEFTOF8-ROBOT)
(not (NOT-LEFTOF12-ROBOT))
(not (NOT-LEFTOF11-ROBOT))
(not (NOT-LEFTOF10-ROBOT))
(not (NOT-LEFTOF9-ROBOT))
(not (NOT-LEFTOF8-ROBOT))
)
)
(when
(and
(LEFTOF11-ROBOT)
)
(and
(LEFTOF12-ROBOT)
(LEFTOF10-ROBOT)
(not (NOT-LEFTOF12-ROBOT))
(not (NOT-LEFTOF11-ROBOT))
(not (NOT-LEFTOF10-ROBOT))
)
)
(when
(and
(LEFTOF10-ROBOT)
)
(and
(LEFTOF12-ROBOT)
(LEFTOF11-ROBOT)
(LEFTOF9-ROBOT)
(not (NOT-LEFTOF12-ROBOT))
(not (NOT-LEFTOF11-ROBOT))
(not (NOT-LEFTOF10-ROBOT))
(not (NOT-LEFTOF9-ROBOT))
)
)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action MOVERIGHT-ROBOT
:parameters ()
:precondition
(and
(Flag3-)
)
:effect
(and
(when
(and
(Flag311-)
)
(and
(COLUMN13-ROBOT)
(not (NOT-COLUMN13-ROBOT))
)
)
(when
(and
(Flag307-)
)
(and
(COLUMN12-ROBOT)
(not (NOT-COLUMN12-ROBOT))
)
)
(when
(and
(Flag303-)
)
(and
(COLUMN11-ROBOT)
(not (NOT-COLUMN11-ROBOT))
)
)
(when
(and
(Flag298-)
)
(and
(COLUMN10-ROBOT)
(not (NOT-COLUMN10-ROBOT))
)
)
(when
(and
(Flag292-)
)
(and
(COLUMN9-ROBOT)
(not (NOT-COLUMN9-ROBOT))
)
)
(when
(and
(Flag285-)
)
(and
(COLUMN8-ROBOT)
(not (NOT-COLUMN8-ROBOT))
)
)
(when
(and
(Flag276-)
)
(and
(COLUMN7-ROBOT)
(not (NOT-COLUMN7-ROBOT))
)
)
(when
(and
(Flag265-)
)
(and
(COLUMN6-ROBOT)
(not (NOT-COLUMN6-ROBOT))
)
)
(when
(and
(Flag255-)
)
(and
(COLUMN5-ROBOT)
(not (NOT-COLUMN5-ROBOT))
)
)
(when
(and
(Flag242-)
)
(and
(COLUMN4-ROBOT)
(not (NOT-COLUMN4-ROBOT))
)
)
(when
(and
(Flag228-)
)
(and
(COLUMN3-ROBOT)
(not (NOT-COLUMN3-ROBOT))
)
)
(when
(and
(Flag215-)
)
(and
(COLUMN2-ROBOT)
(not (NOT-COLUMN2-ROBOT))
)
)
(when
(and
(Flag200-)
)
(and
(COLUMN1-ROBOT)
(not (NOT-COLUMN1-ROBOT))
)
)
(when
(and
(COLUMN12-ROBOT)
)
(and
(COLUMN13-ROBOT)
(NOT-COLUMN12-ROBOT)
(not (NOT-COLUMN13-ROBOT))
(not (COLUMN12-ROBOT))
)
)
(when
(and
(COLUMN11-ROBOT)
)
(and
(COLUMN12-ROBOT)
(NOT-COLUMN11-ROBOT)
(not (NOT-COLUMN12-ROBOT))
(not (COLUMN11-ROBOT))
)
)
(when
(and
(COLUMN10-ROBOT)
)
(and
(COLUMN11-ROBOT)
(NOT-COLUMN10-ROBOT)
(not (NOT-COLUMN11-ROBOT))
(not (COLUMN10-ROBOT))
)
)
(when
(and
(COLUMN9-ROBOT)
)
(and
(COLUMN10-ROBOT)
(NOT-COLUMN9-ROBOT)
(not (NOT-COLUMN10-ROBOT))
(not (COLUMN9-ROBOT))
)
)
(when
(and
(COLUMN8-ROBOT)
)
(and
(COLUMN9-ROBOT)
(NOT-COLUMN8-ROBOT)
(not (NOT-COLUMN9-ROBOT))
(not (COLUMN8-ROBOT))
)
)
(when
(and
(COLUMN7-ROBOT)
)
(and
(COLUMN8-ROBOT)
(NOT-COLUMN7-ROBOT)
(not (NOT-COLUMN8-ROBOT))
(not (COLUMN7-ROBOT))
)
)
(when
(and
(COLUMN6-ROBOT)
)
(and
(COLUMN7-ROBOT)
(NOT-COLUMN6-ROBOT)
(not (NOT-COLUMN7-ROBOT))
(not (COLUMN6-ROBOT))
)
)
(when
(and
(COLUMN5-ROBOT)
)
(and
(COLUMN6-ROBOT)
(NOT-COLUMN5-ROBOT)
(not (NOT-COLUMN6-ROBOT))
(not (COLUMN5-ROBOT))
)
)
(when
(and
(COLUMN4-ROBOT)
)
(and
(COLUMN5-ROBOT)
(NOT-COLUMN4-ROBOT)
(not (NOT-COLUMN5-ROBOT))
(not (COLUMN4-ROBOT))
)
)
(when
(and
(COLUMN3-ROBOT)
)
(and
(COLUMN4-ROBOT)
(NOT-COLUMN3-ROBOT)
(not (NOT-COLUMN4-ROBOT))
(not (COLUMN3-ROBOT))
)
)
(when
(and
(COLUMN2-ROBOT)
)
(and
(COLUMN3-ROBOT)
(NOT-COLUMN2-ROBOT)
(not (NOT-COLUMN3-ROBOT))
(not (COLUMN2-ROBOT))
)
)
(when
(and
(COLUMN1-ROBOT)
)
(and
(COLUMN2-ROBOT)
(NOT-COLUMN1-ROBOT)
(not (NOT-COLUMN2-ROBOT))
(not (COLUMN1-ROBOT))
)
)
(when
(and
(COLUMN0-ROBOT)
)
(and
(COLUMN1-ROBOT)
(NOT-COLUMN0-ROBOT)
(not (NOT-COLUMN1-ROBOT))
(not (COLUMN0-ROBOT))
)
)
(when
(and
(Flag185-)
)
(and
(LEFTOF14-ROBOT)
(NOT-LEFTOF13-ROBOT)
(not (NOT-LEFTOF14-ROBOT))
(not (LEFTOF13-ROBOT))
)
)
(when
(and
(Flag184-)
)
(and
(LEFTOF13-ROBOT)
(NOT-LEFTOF12-ROBOT)
(not (NOT-LEFTOF13-ROBOT))
(not (LEFTOF12-ROBOT))
)
)
(when
(and
(Flag183-)
)
(and
(LEFTOF12-ROBOT)
(NOT-LEFTOF11-ROBOT)
(not (NOT-LEFTOF12-ROBOT))
(not (LEFTOF11-ROBOT))
)
)
(when
(and
(Flag182-)
)
(and
(LEFTOF11-ROBOT)
(NOT-LEFTOF10-ROBOT)
(not (NOT-LEFTOF11-ROBOT))
(not (LEFTOF10-ROBOT))
)
)
(when
(and
(Flag181-)
)
(and
(LEFTOF10-ROBOT)
(NOT-LEFTOF9-ROBOT)
(not (NOT-LEFTOF10-ROBOT))
(not (LEFTOF9-ROBOT))
)
)
(when
(and
(Flag180-)
)
(and
(LEFTOF9-ROBOT)
(NOT-LEFTOF8-ROBOT)
(not (NOT-LEFTOF9-ROBOT))
(not (LEFTOF8-ROBOT))
)
)
(when
(and
(Flag179-)
)
(and
(LEFTOF8-ROBOT)
(NOT-LEFTOF7-ROBOT)
(not (NOT-LEFTOF8-ROBOT))
(not (LEFTOF7-ROBOT))
)
)
(when
(and
(Flag178-)
)
(and
(LEFTOF7-ROBOT)
(NOT-LEFTOF6-ROBOT)
(not (NOT-LEFTOF7-ROBOT))
(not (LEFTOF6-ROBOT))
)
)
(when
(and
(Flag177-)
)
(and
(LEFTOF6-ROBOT)
(NOT-LEFTOF5-ROBOT)
(not (NOT-LEFTOF6-ROBOT))
(not (LEFTOF5-ROBOT))
)
)
(when
(and
(Flag176-)
)
(and
(LEFTOF5-ROBOT)
(NOT-LEFTOF4-ROBOT)
(not (NOT-LEFTOF5-ROBOT))
(not (LEFTOF4-ROBOT))
)
)
(when
(and
(Flag175-)
)
(and
(LEFTOF4-ROBOT)
(NOT-LEFTOF3-ROBOT)
(not (NOT-LEFTOF4-ROBOT))
(not (LEFTOF3-ROBOT))
)
)
(when
(and
(Flag174-)
)
(and
(LEFTOF3-ROBOT)
(NOT-LEFTOF2-ROBOT)
(not (NOT-LEFTOF3-ROBOT))
(not (LEFTOF2-ROBOT))
)
)
(when
(and
(LEFTOF1-ROBOT)
)
(and
(LEFTOF2-ROBOT)
(NOT-LEFTOF1-ROBOT)
(not (NOT-LEFTOF2-ROBOT))
(not (LEFTOF1-ROBOT))
)
)
(when
(and
(Flag173-)
)
(and
(RIGHTOF13-ROBOT)
(not (NOT-RIGHTOF13-ROBOT))
)
)
(when
(and
(Flag172-)
)
(and
(RIGHTOF12-ROBOT)
(not (NOT-RIGHTOF12-ROBOT))
)
)
(when
(and
(Flag171-)
)
(and
(RIGHTOF11-ROBOT)
(not (NOT-RIGHTOF11-ROBOT))
)
)
(when
(and
(Flag170-)
)
(and
(RIGHTOF10-ROBOT)
(not (NOT-RIGHTOF10-ROBOT))
)
)
(when
(and
(Flag169-)
)
(and
(RIGHTOF9-ROBOT)
(not (NOT-RIGHTOF9-ROBOT))
)
)
(when
(and
(Flag168-)
)
(and
(RIGHTOF8-ROBOT)
(not (NOT-RIGHTOF8-ROBOT))
)
)
(when
(and
(Flag167-)
)
(and
(RIGHTOF7-ROBOT)
(not (NOT-RIGHTOF7-ROBOT))
)
)
(when
(and
(Flag166-)
)
(and
(RIGHTOF6-ROBOT)
(not (NOT-RIGHTOF6-ROBOT))
)
)
(when
(and
(Flag165-)
)
(and
(RIGHTOF5-ROBOT)
(not (NOT-RIGHTOF5-ROBOT))
)
)
(when
(and
(Flag164-)
)
(and
(RIGHTOF4-ROBOT)
(not (NOT-RIGHTOF4-ROBOT))
)
)
(when
(and
(Flag163-)
)
(and
(RIGHTOF3-ROBOT)
(not (NOT-RIGHTOF3-ROBOT))
)
)
(when
(and
(Flag162-)
)
(and
(RIGHTOF2-ROBOT)
(not (NOT-RIGHTOF2-ROBOT))
)
)
(when
(and
(Flag161-)
)
(and
(RIGHTOF1-ROBOT)
(not (NOT-RIGHTOF1-ROBOT))
)
)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action CHECKCONSISTENCYACTION
:parameters ()
:precondition
(and
(NOT-ERROR-)
(CHECKCONSISTENCY-)
)
:effect
(and
(when
(and
(BELOWOF2-ROBOT)
(ABOVEOF9-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF7-ROBOT)
(BELOWOF1-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF1-ROBOT)
(RIGHTOF12-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF3-ROBOT)
(RIGHTOF8-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF6-ROBOT)
(LEFTOF2-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF13-ROBOT)
(LEFTOF7-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF1-ROBOT)
(ABOVEOF4-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF11-ROBOT)
(LEFTOF4-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF9-ROBOT)
(RIGHTOF13-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF8-ROBOT)
(BELOWOF1-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF3-ROBOT)
(ABOVEOF8-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF10-ROBOT)
(LEFTOF1-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF3-ROBOT)
(LEFTOF1-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF2-ROBOT)
(RIGHTOF11-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF11-ROBOT)
(BELOWOF11-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF1-ROBOT)
(RIGHTOF13-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF13-ROBOT)
(BELOWOF9-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF12-ROBOT)
(BELOWOF6-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF6-ROBOT)
(LEFTOF3-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF10-ROBOT)
(RIGHTOF12-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF7-ROBOT)
(LEFTOF3-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF2-ROBOT)
(RIGHTOF12-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF2-ROBOT)
(BELOWOF1-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF3-ROBOT)
(ABOVEOF12-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF1-ROBOT)
(ABOVEOF12-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF2-ROBOT)
(BELOWOF2-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF12-ROBOT)
(LEFTOF7-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF3-ROBOT)
(ABOVEOF9-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF12-ROBOT)
(BELOWOF4-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF11-ROBOT)
(LEFTOF5-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF2-ROBOT)
(ABOVEOF11-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF2-ROBOT)
(ABOVEOF8-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF13-ROBOT)
(BELOWOF7-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF11-ROBOT)
(RIGHTOF12-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF7-ROBOT)
(LEFTOF1-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF11-ROBOT)
(BELOWOF6-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF13-ROBOT)
(LEFTOF2-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF9-ROBOT)
(ABOVEOF10-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF7-ROBOT)
(RIGHTOF9-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF6-ROBOT)
(LEFTOF6-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF2-ROBOT)
(ABOVEOF7-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF12-ROBOT)
(BELOWOF5-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF4-ROBOT)
(ABOVEOF9-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF11-ROBOT)
(BELOWOF7-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF4-ROBOT)
(RIGHTOF10-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF11-ROBOT)
(LEFTOF8-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF13-ROBOT)
(BELOWOF3-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF2-ROBOT)
(RIGHTOF5-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF2-ROBOT)
(RIGHTOF9-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF12-ROBOT)
(LEFTOF3-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF9-ROBOT)
(LEFTOF5-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF1-ROBOT)
(ABOVEOF13-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF7-ROBOT)
(BELOWOF6-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF1-ROBOT)
(RIGHTOF9-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF7-ROBOT)
(RIGHTOF8-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF6-ROBOT)
(BELOWOF3-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF8-ROBOT)
(ABOVEOF12-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF10-ROBOT)
(RIGHTOF11-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF1-ROBOT)
(RIGHTOF1-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF13-ROBOT)
(LEFTOF3-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF13-ROBOT)
(LEFTOF6-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF5-ROBOT)
(RIGHTOF12-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF7-ROBOT)
(ABOVEOF10-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF12-ROBOT)
(BELOWOF2-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF8-ROBOT)
(ABOVEOF11-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF5-ROBOT)
(RIGHTOF6-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF3-ROBOT)
(ABOVEOF10-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF7-ROBOT)
(LEFTOF7-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF6-ROBOT)
(RIGHTOF10-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF9-ROBOT)
(LEFTOF4-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF11-ROBOT)
(BELOWOF1-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF5-ROBOT)
(ABOVEOF11-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF8-ROBOT)
(BELOWOF4-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF4-ROBOT)
(RIGHTOF5-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF2-ROBOT)
(RIGHTOF10-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF13-ROBOT)
(LEFTOF4-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF3-ROBOT)
(ABOVEOF11-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF11-ROBOT)
(ABOVEOF13-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF5-ROBOT)
(ABOVEOF9-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF1-ROBOT)
(RIGHTOF5-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF8-ROBOT)
(LEFTOF2-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF8-ROBOT)
(BELOWOF7-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF12-ROBOT)
(BELOWOF7-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF12-ROBOT)
(BELOWOF11-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF8-ROBOT)
(RIGHTOF10-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF10-ROBOT)
(BELOWOF6-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF5-ROBOT)
(ABOVEOF13-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF4-ROBOT)
(ABOVEOF13-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF11-ROBOT)
(LEFTOF7-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF12-ROBOT)
(LEFTOF4-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF3-ROBOT)
(LEFTOF3-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF4-ROBOT)
(LEFTOF1-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF6-ROBOT)
(ABOVEOF13-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF8-ROBOT)
(RIGHTOF8-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF5-ROBOT)
(RIGHTOF7-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF13-ROBOT)
(BELOWOF2-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF1-ROBOT)
(RIGHTOF11-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF7-ROBOT)
(LEFTOF4-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF8-ROBOT)
(RIGHTOF9-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF3-ROBOT)
(RIGHTOF11-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF8-ROBOT)
(RIGHTOF13-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF8-ROBOT)
(ABOVEOF13-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF7-ROBOT)
(LEFTOF6-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF6-ROBOT)
(ABOVEOF9-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF13-ROBOT)
(BELOWOF12-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF10-ROBOT)
(LEFTOF5-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF9-ROBOT)
(BELOWOF7-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF9-ROBOT)
(ABOVEOF11-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF2-ROBOT)
(ABOVEOF6-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF13-ROBOT)
(LEFTOF5-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF1-ROBOT)
(ABOVEOF9-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF8-ROBOT)
(BELOWOF6-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF3-ROBOT)
(ABOVEOF7-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF1-ROBOT)
(RIGHTOF8-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF10-ROBOT)
(ABOVEOF13-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF4-ROBOT)
(ABOVEOF10-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF6-ROBOT)
(RIGHTOF11-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF13-ROBOT)
(LEFTOF11-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF1-ROBOT)
(ABOVEOF10-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF6-ROBOT)
(RIGHTOF12-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF10-ROBOT)
(BELOWOF2-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF7-ROBOT)
(RIGHTOF10-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF2-ROBOT)
(RIGHTOF4-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF10-ROBOT)
(BELOWOF8-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF8-ROBOT)
(BELOWOF5-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF9-ROBOT)
(LEFTOF6-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF2-ROBOT)
(ABOVEOF4-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF1-ROBOT)
(ABOVEOF6-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF6-ROBOT)
(LEFTOF1-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF1-ROBOT)
(ABOVEOF1-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF5-ROBOT)
(ABOVEOF10-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF13-ROBOT)
(LEFTOF10-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF11-ROBOT)
(BELOWOF4-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF9-ROBOT)
(ABOVEOF12-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF3-ROBOT)
(RIGHTOF9-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF8-ROBOT)
(RIGHTOF12-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF4-ROBOT)
(ABOVEOF5-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF10-ROBOT)
(LEFTOF10-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF1-ROBOT)
(RIGHTOF2-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF4-ROBOT)
(LEFTOF3-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF6-ROBOT)
(ABOVEOF6-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF11-ROBOT)
(BELOWOF10-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF9-ROBOT)
(ABOVEOF9-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF7-ROBOT)
(BELOWOF4-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF8-ROBOT)
(LEFTOF4-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF5-ROBOT)
(ABOVEOF5-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF5-ROBOT)
(RIGHTOF5-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF5-ROBOT)
(RIGHTOF8-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF10-ROBOT)
(LEFTOF3-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF1-ROBOT)
(ABOVEOF5-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF9-ROBOT)
(RIGHTOF11-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF2-ROBOT)
(ABOVEOF5-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF3-ROBOT)
(ABOVEOF4-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF2-ROBOT)
(ABOVEOF3-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF12-ROBOT)
(LEFTOF9-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF5-ROBOT)
(LEFTOF3-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF7-ROBOT)
(BELOWOF5-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF12-ROBOT)
(BELOWOF12-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF6-ROBOT)
(BELOWOF4-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF2-ROBOT)
(RIGHTOF7-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF8-ROBOT)
(LEFTOF6-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF4-ROBOT)
(RIGHTOF6-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF12-ROBOT)
(RIGHTOF12-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF11-ROBOT)
(RIGHTOF11-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF12-ROBOT)
(BELOWOF10-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF13-ROBOT)
(LEFTOF12-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF3-ROBOT)
(LEFTOF2-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF9-ROBOT)
(BELOWOF8-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF1-ROBOT)
(ABOVEOF3-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF6-ROBOT)
(BELOWOF5-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF9-ROBOT)
(LEFTOF9-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF13-ROBOT)
(LEFTOF13-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF3-ROBOT)
(ABOVEOF3-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF3-ROBOT)
(ABOVEOF5-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF4-ROBOT)
(ABOVEOF4-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF10-ROBOT)
(ABOVEOF10-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF4-ROBOT)
(RIGHTOF4-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF8-ROBOT)
(BELOWOF8-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF13-ROBOT)
(BELOWOF13-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF7-ROBOT)
(ABOVEOF7-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF9-ROBOT)
(RIGHTOF10-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF2-ROBOT)
(RIGHTOF2-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(NOT-CHECKCONSISTENCY-)
(not (CHECKCONSISTENCY-))
)
)
(:derived (Flag3-)(and (NOT-CHECKCONSISTENCY-)(NOT-ERROR-)(Flag2-)))

(:derived (Flag5-)(and (ABOVEOF6-ROBOT)))

(:derived (Flag5-)(and (ABOVEOF11-ROBOT)))

(:derived (Flag5-)(and (ABOVEOF10-ROBOT)))

(:derived (Flag5-)(and (ABOVEOF2-ROBOT)))

(:derived (Flag5-)(and (ABOVEOF7-ROBOT)))

(:derived (Flag5-)(and (ABOVEOF12-ROBOT)))

(:derived (Flag5-)(and (ABOVEOF8-ROBOT)))

(:derived (Flag5-)(and (ABOVEOF9-ROBOT)))

(:derived (Flag5-)(and (ABOVEOF4-ROBOT)))

(:derived (Flag5-)(and (ABOVEOF3-ROBOT)))

(:derived (Flag5-)(and (ABOVEOF13-ROBOT)))

(:derived (Flag5-)(and (ABOVEOF5-ROBOT)))

(:derived (Flag5-)(and (ABOVEOF1-ROBOT)))

(:derived (Flag6-)(and (ABOVEOF11-ROBOT)))

(:derived (Flag6-)(and (ABOVEOF6-ROBOT)))

(:derived (Flag6-)(and (ABOVEOF10-ROBOT)))

(:derived (Flag6-)(and (ABOVEOF2-ROBOT)))

(:derived (Flag6-)(and (ABOVEOF7-ROBOT)))

(:derived (Flag6-)(and (ABOVEOF12-ROBOT)))

(:derived (Flag6-)(and (ABOVEOF8-ROBOT)))

(:derived (Flag6-)(and (ABOVEOF9-ROBOT)))

(:derived (Flag6-)(and (ABOVEOF4-ROBOT)))

(:derived (Flag6-)(and (ABOVEOF3-ROBOT)))

(:derived (Flag6-)(and (ABOVEOF13-ROBOT)))

(:derived (Flag6-)(and (ABOVEOF5-ROBOT)))

(:derived (Flag7-)(and (ABOVEOF11-ROBOT)))

(:derived (Flag7-)(and (ABOVEOF6-ROBOT)))

(:derived (Flag7-)(and (ABOVEOF10-ROBOT)))

(:derived (Flag7-)(and (ABOVEOF8-ROBOT)))

(:derived (Flag7-)(and (ABOVEOF7-ROBOT)))

(:derived (Flag7-)(and (ABOVEOF12-ROBOT)))

(:derived (Flag7-)(and (ABOVEOF9-ROBOT)))

(:derived (Flag7-)(and (ABOVEOF4-ROBOT)))

(:derived (Flag7-)(and (ABOVEOF3-ROBOT)))

(:derived (Flag7-)(and (ABOVEOF13-ROBOT)))

(:derived (Flag7-)(and (ABOVEOF5-ROBOT)))

(:derived (Flag8-)(and (ABOVEOF11-ROBOT)))

(:derived (Flag8-)(and (ABOVEOF6-ROBOT)))

(:derived (Flag8-)(and (ABOVEOF10-ROBOT)))

(:derived (Flag8-)(and (ABOVEOF8-ROBOT)))

(:derived (Flag8-)(and (ABOVEOF7-ROBOT)))

(:derived (Flag8-)(and (ABOVEOF12-ROBOT)))

(:derived (Flag8-)(and (ABOVEOF9-ROBOT)))

(:derived (Flag8-)(and (ABOVEOF4-ROBOT)))

(:derived (Flag8-)(and (ABOVEOF13-ROBOT)))

(:derived (Flag8-)(and (ABOVEOF5-ROBOT)))

(:derived (Flag9-)(and (ABOVEOF11-ROBOT)))

(:derived (Flag9-)(and (ABOVEOF6-ROBOT)))

(:derived (Flag9-)(and (ABOVEOF7-ROBOT)))

(:derived (Flag9-)(and (ABOVEOF8-ROBOT)))

(:derived (Flag9-)(and (ABOVEOF10-ROBOT)))

(:derived (Flag9-)(and (ABOVEOF12-ROBOT)))

(:derived (Flag9-)(and (ABOVEOF9-ROBOT)))

(:derived (Flag9-)(and (ABOVEOF13-ROBOT)))

(:derived (Flag9-)(and (ABOVEOF5-ROBOT)))

(:derived (Flag10-)(and (ABOVEOF11-ROBOT)))

(:derived (Flag10-)(and (ABOVEOF6-ROBOT)))

(:derived (Flag10-)(and (ABOVEOF7-ROBOT)))

(:derived (Flag10-)(and (ABOVEOF8-ROBOT)))

(:derived (Flag10-)(and (ABOVEOF10-ROBOT)))

(:derived (Flag10-)(and (ABOVEOF12-ROBOT)))

(:derived (Flag10-)(and (ABOVEOF9-ROBOT)))

(:derived (Flag10-)(and (ABOVEOF13-ROBOT)))

(:derived (Flag11-)(and (ABOVEOF11-ROBOT)))

(:derived (Flag11-)(and (ABOVEOF7-ROBOT)))

(:derived (Flag11-)(and (ABOVEOF8-ROBOT)))

(:derived (Flag11-)(and (ABOVEOF10-ROBOT)))

(:derived (Flag11-)(and (ABOVEOF12-ROBOT)))

(:derived (Flag11-)(and (ABOVEOF9-ROBOT)))

(:derived (Flag11-)(and (ABOVEOF13-ROBOT)))

(:derived (Flag12-)(and (ABOVEOF11-ROBOT)))

(:derived (Flag12-)(and (ABOVEOF8-ROBOT)))

(:derived (Flag12-)(and (ABOVEOF10-ROBOT)))

(:derived (Flag12-)(and (ABOVEOF12-ROBOT)))

(:derived (Flag12-)(and (ABOVEOF9-ROBOT)))

(:derived (Flag12-)(and (ABOVEOF13-ROBOT)))

(:derived (Flag13-)(and (ABOVEOF13-ROBOT)))

(:derived (Flag13-)(and (ABOVEOF12-ROBOT)))

(:derived (Flag13-)(and (ABOVEOF11-ROBOT)))

(:derived (Flag13-)(and (ABOVEOF10-ROBOT)))

(:derived (Flag13-)(and (ABOVEOF9-ROBOT)))

(:derived (Flag14-)(and (ABOVEOF12-ROBOT)))

(:derived (Flag14-)(and (ABOVEOF13-ROBOT)))

(:derived (Flag14-)(and (ABOVEOF10-ROBOT)))

(:derived (Flag14-)(and (ABOVEOF11-ROBOT)))

(:derived (Flag15-)(and (ABOVEOF12-ROBOT)))

(:derived (Flag15-)(and (ABOVEOF13-ROBOT)))

(:derived (Flag15-)(and (ABOVEOF11-ROBOT)))

(:derived (Flag16-)(and (ABOVEOF12-ROBOT)))

(:derived (Flag16-)(and (ABOVEOF13-ROBOT)))

(:derived (Flag17-)(and (BELOWOF2-ROBOT)))

(:derived (Flag17-)(and (BELOWOF1-ROBOT)))

(:derived (Flag18-)(and (BELOWOF2-ROBOT)))

(:derived (Flag18-)(and (BELOWOF1-ROBOT)))

(:derived (Flag18-)(and (BELOWOF3-ROBOT)))

(:derived (Flag19-)(and (BELOWOF2-ROBOT)))

(:derived (Flag19-)(and (BELOWOF1-ROBOT)))

(:derived (Flag19-)(and (BELOWOF3-ROBOT)))

(:derived (Flag19-)(and (BELOWOF4-ROBOT)))

(:derived (Flag20-)(and (BELOWOF2-ROBOT)))

(:derived (Flag20-)(and (BELOWOF1-ROBOT)))

(:derived (Flag20-)(and (BELOWOF3-ROBOT)))

(:derived (Flag20-)(and (BELOWOF4-ROBOT)))

(:derived (Flag20-)(and (BELOWOF5-ROBOT)))

(:derived (Flag21-)(and (BELOWOF6-ROBOT)))

(:derived (Flag21-)(and (BELOWOF5-ROBOT)))

(:derived (Flag21-)(and (BELOWOF2-ROBOT)))

(:derived (Flag21-)(and (BELOWOF1-ROBOT)))

(:derived (Flag21-)(and (BELOWOF4-ROBOT)))

(:derived (Flag21-)(and (BELOWOF3-ROBOT)))

(:derived (Flag22-)(and (BELOWOF6-ROBOT)))

(:derived (Flag22-)(and (BELOWOF5-ROBOT)))

(:derived (Flag22-)(and (BELOWOF2-ROBOT)))

(:derived (Flag22-)(and (BELOWOF1-ROBOT)))

(:derived (Flag22-)(and (BELOWOF4-ROBOT)))

(:derived (Flag22-)(and (BELOWOF7-ROBOT)))

(:derived (Flag22-)(and (BELOWOF3-ROBOT)))

(:derived (Flag23-)(and (BELOWOF6-ROBOT)))

(:derived (Flag23-)(and (BELOWOF5-ROBOT)))

(:derived (Flag23-)(and (BELOWOF2-ROBOT)))

(:derived (Flag23-)(and (BELOWOF1-ROBOT)))

(:derived (Flag23-)(and (BELOWOF4-ROBOT)))

(:derived (Flag23-)(and (BELOWOF7-ROBOT)))

(:derived (Flag23-)(and (BELOWOF3-ROBOT)))

(:derived (Flag23-)(and (BELOWOF8-ROBOT)))

(:derived (Flag24-)(and (BELOWOF6-ROBOT)))

(:derived (Flag24-)(and (BELOWOF5-ROBOT)))

(:derived (Flag24-)(and (BELOWOF2-ROBOT)))

(:derived (Flag24-)(and (BELOWOF1-ROBOT)))

(:derived (Flag24-)(and (BELOWOF4-ROBOT)))

(:derived (Flag24-)(and (BELOWOF7-ROBOT)))

(:derived (Flag24-)(and (BELOWOF9-ROBOT)))

(:derived (Flag24-)(and (BELOWOF3-ROBOT)))

(:derived (Flag24-)(and (BELOWOF8-ROBOT)))

(:derived (Flag25-)(and (BELOWOF10-ROBOT)))

(:derived (Flag25-)(and (BELOWOF6-ROBOT)))

(:derived (Flag25-)(and (BELOWOF5-ROBOT)))

(:derived (Flag25-)(and (BELOWOF2-ROBOT)))

(:derived (Flag25-)(and (BELOWOF1-ROBOT)))

(:derived (Flag25-)(and (BELOWOF4-ROBOT)))

(:derived (Flag25-)(and (BELOWOF7-ROBOT)))

(:derived (Flag25-)(and (BELOWOF9-ROBOT)))

(:derived (Flag25-)(and (BELOWOF3-ROBOT)))

(:derived (Flag25-)(and (BELOWOF8-ROBOT)))

(:derived (Flag26-)(and (BELOWOF10-ROBOT)))

(:derived (Flag26-)(and (BELOWOF11-ROBOT)))

(:derived (Flag26-)(and (BELOWOF6-ROBOT)))

(:derived (Flag26-)(and (BELOWOF5-ROBOT)))

(:derived (Flag26-)(and (BELOWOF2-ROBOT)))

(:derived (Flag26-)(and (BELOWOF1-ROBOT)))

(:derived (Flag26-)(and (BELOWOF4-ROBOT)))

(:derived (Flag26-)(and (BELOWOF7-ROBOT)))

(:derived (Flag26-)(and (BELOWOF9-ROBOT)))

(:derived (Flag26-)(and (BELOWOF3-ROBOT)))

(:derived (Flag26-)(and (BELOWOF8-ROBOT)))

(:derived (Flag27-)(and (BELOWOF10-ROBOT)))

(:derived (Flag27-)(and (BELOWOF11-ROBOT)))

(:derived (Flag27-)(and (BELOWOF6-ROBOT)))

(:derived (Flag27-)(and (BELOWOF5-ROBOT)))

(:derived (Flag27-)(and (BELOWOF2-ROBOT)))

(:derived (Flag27-)(and (BELOWOF1-ROBOT)))

(:derived (Flag27-)(and (BELOWOF4-ROBOT)))

(:derived (Flag27-)(and (BELOWOF12-ROBOT)))

(:derived (Flag27-)(and (BELOWOF7-ROBOT)))

(:derived (Flag27-)(and (BELOWOF9-ROBOT)))

(:derived (Flag27-)(and (BELOWOF3-ROBOT)))

(:derived (Flag27-)(and (BELOWOF8-ROBOT)))

(:derived (Flag28-)(and (BELOWOF10-ROBOT)))

(:derived (Flag28-)(and (BELOWOF11-ROBOT)))

(:derived (Flag28-)(and (BELOWOF6-ROBOT)))

(:derived (Flag28-)(and (BELOWOF5-ROBOT)))

(:derived (Flag28-)(and (BELOWOF2-ROBOT)))

(:derived (Flag28-)(and (BELOWOF1-ROBOT)))

(:derived (Flag28-)(and (BELOWOF4-ROBOT)))

(:derived (Flag28-)(and (BELOWOF12-ROBOT)))

(:derived (Flag28-)(and (BELOWOF7-ROBOT)))

(:derived (Flag28-)(and (BELOWOF9-ROBOT)))

(:derived (Flag28-)(and (BELOWOF3-ROBOT)))

(:derived (Flag28-)(and (BELOWOF8-ROBOT)))

(:derived (Flag29-)(and (BELOWOF1-ROBOT)(ABOVEOF4-ROBOT)))

(:derived (Flag30-)(and (BELOWOF1-ROBOT)(ABOVEOF2-ROBOT)))

(:derived (Flag31-)(and (BELOWOF1-ROBOT)(ABOVEOF1-ROBOT)))

(:derived (Flag32-)(and (ABOVEOF12-ROBOT)(BELOWOF1-ROBOT)))

(:derived (Flag33-)(and (BELOWOF1-ROBOT)(ABOVEOF13-ROBOT)))

(:derived (Flag34-)(and (BELOWOF1-ROBOT)(ABOVEOF7-ROBOT)))

(:derived (Flag35-)(and (ABOVEOF11-ROBOT)(BELOWOF1-ROBOT)))

(:derived (Flag36-)(and (ABOVEOF6-ROBOT)(BELOWOF1-ROBOT)))

(:derived (Flag37-)(and (BELOWOF1-ROBOT)(ABOVEOF5-ROBOT)))

(:derived (Flag38-)(and (ABOVEOF10-ROBOT)(BELOWOF1-ROBOT)))

(:derived (Flag39-)(and (ABOVEOF8-ROBOT)(BELOWOF1-ROBOT)))

(:derived (Flag40-)(and (ABOVEOF3-ROBOT)(BELOWOF1-ROBOT)))

(:derived (Flag41-)(and (BELOWOF1-ROBOT)(ABOVEOF9-ROBOT)))

(:derived (Flag42-)(and (BELOWOF1-ROBOT)))

(:derived (Flag43-)(and (ABOVEOF2-ROBOT)(BELOWOF2-ROBOT)))

(:derived (Flag44-)(and (ABOVEOF1-ROBOT)(BELOWOF2-ROBOT)))

(:derived (Flag45-)(and (ABOVEOF7-ROBOT)(BELOWOF2-ROBOT)))

(:derived (Flag46-)(and (BELOWOF2-ROBOT)(ABOVEOF13-ROBOT)))

(:derived (Flag47-)(and (ABOVEOF4-ROBOT)(BELOWOF2-ROBOT)))

(:derived (Flag48-)(and (ABOVEOF6-ROBOT)(BELOWOF2-ROBOT)))

(:derived (Flag49-)(and (ABOVEOF9-ROBOT)(BELOWOF2-ROBOT)))

(:derived (Flag50-)(and (ABOVEOF11-ROBOT)(BELOWOF2-ROBOT)))

(:derived (Flag51-)(and (ABOVEOF10-ROBOT)(BELOWOF2-ROBOT)))

(:derived (Flag52-)(and (ABOVEOF8-ROBOT)(BELOWOF2-ROBOT)))

(:derived (Flag53-)(and (ABOVEOF12-ROBOT)(BELOWOF2-ROBOT)))

(:derived (Flag54-)(and (BELOWOF2-ROBOT)(ABOVEOF5-ROBOT)))

(:derived (Flag55-)(and (ABOVEOF3-ROBOT)(BELOWOF2-ROBOT)))

(:derived (Flag57-)(and (BELOWOF3-ROBOT)(ABOVEOF5-ROBOT)))

(:derived (Flag58-)(and (BELOWOF3-ROBOT)(ABOVEOF6-ROBOT)))

(:derived (Flag59-)(and (BELOWOF3-ROBOT)(ABOVEOF7-ROBOT)))

(:derived (Flag60-)(and (BELOWOF3-ROBOT)(ABOVEOF9-ROBOT)))

(:derived (Flag61-)(and (BELOWOF3-ROBOT)(ABOVEOF2-ROBOT)))

(:derived (Flag62-)(and (ABOVEOF3-ROBOT)(BELOWOF3-ROBOT)))

(:derived (Flag63-)(and (ABOVEOF8-ROBOT)(BELOWOF3-ROBOT)))

(:derived (Flag64-)(and (BELOWOF3-ROBOT)(ABOVEOF4-ROBOT)))

(:derived (Flag65-)(and (BELOWOF3-ROBOT)(ABOVEOF10-ROBOT)))

(:derived (Flag66-)(and (BELOWOF2-ROBOT)(ABOVEOF2-ROBOT)))

(:derived (Flag67-)(and (ABOVEOF12-ROBOT)(BELOWOF3-ROBOT)))

(:derived (Flag68-)(and (BELOWOF3-ROBOT)(ABOVEOF13-ROBOT)))

(:derived (Flag69-)(and (ABOVEOF11-ROBOT)(BELOWOF3-ROBOT)))

(:derived (Flag71-)(and (BELOWOF4-ROBOT)(ABOVEOF13-ROBOT)))

(:derived (Flag72-)(and (ABOVEOF3-ROBOT)(BELOWOF4-ROBOT)))

(:derived (Flag73-)(and (ABOVEOF6-ROBOT)(BELOWOF4-ROBOT)))

(:derived (Flag74-)(and (ABOVEOF9-ROBOT)(BELOWOF4-ROBOT)))

(:derived (Flag75-)(and (BELOWOF4-ROBOT)(ABOVEOF4-ROBOT)))

(:derived (Flag76-)(and (ABOVEOF8-ROBOT)(BELOWOF4-ROBOT)))

(:derived (Flag77-)(and (BELOWOF4-ROBOT)(ABOVEOF7-ROBOT)))

(:derived (Flag78-)(and (ABOVEOF12-ROBOT)(BELOWOF4-ROBOT)))

(:derived (Flag79-)(and (BELOWOF4-ROBOT)(ABOVEOF5-ROBOT)))

(:derived (Flag80-)(and (BELOWOF3-ROBOT)(ABOVEOF3-ROBOT)))

(:derived (Flag81-)(and (ABOVEOF10-ROBOT)(BELOWOF4-ROBOT)))

(:derived (Flag82-)(and (BELOWOF3-ROBOT)(ABOVEOF11-ROBOT)))

(:derived (Flag83-)(and (ABOVEOF11-ROBOT)(BELOWOF4-ROBOT)))

(:derived (Flag85-)(and (ABOVEOF4-ROBOT)(BELOWOF5-ROBOT)))

(:derived (Flag86-)(and (ABOVEOF8-ROBOT)(BELOWOF5-ROBOT)))

(:derived (Flag87-)(and (ABOVEOF6-ROBOT)(BELOWOF5-ROBOT)))

(:derived (Flag88-)(and (ABOVEOF13-ROBOT)(BELOWOF5-ROBOT)))

(:derived (Flag89-)(and (ABOVEOF10-ROBOT)(BELOWOF5-ROBOT)))

(:derived (Flag90-)(and (ABOVEOF11-ROBOT)(BELOWOF5-ROBOT)))

(:derived (Flag91-)(and (ABOVEOF5-ROBOT)(BELOWOF5-ROBOT)))

(:derived (Flag92-)(and (ABOVEOF7-ROBOT)(BELOWOF5-ROBOT)))

(:derived (Flag93-)(and (ABOVEOF9-ROBOT)(BELOWOF5-ROBOT)))

(:derived (Flag94-)(and (ABOVEOF12-ROBOT)(BELOWOF5-ROBOT)))

(:derived (Flag96-)(and (ABOVEOF11-ROBOT)(BELOWOF6-ROBOT)))

(:derived (Flag97-)(and (ABOVEOF12-ROBOT)(BELOWOF6-ROBOT)))

(:derived (Flag98-)(and (BELOWOF5-ROBOT)(ABOVEOF13-ROBOT)))

(:derived (Flag99-)(and (BELOWOF6-ROBOT)(ABOVEOF13-ROBOT)))

(:derived (Flag100-)(and (BELOWOF6-ROBOT)(ABOVEOF5-ROBOT)))

(:derived (Flag101-)(and (BELOWOF6-ROBOT)(ABOVEOF7-ROBOT)))

(:derived (Flag102-)(and (BELOWOF5-ROBOT)(ABOVEOF5-ROBOT)))

(:derived (Flag103-)(and (BELOWOF6-ROBOT)(ABOVEOF6-ROBOT)))

(:derived (Flag104-)(and (ABOVEOF8-ROBOT)(BELOWOF6-ROBOT)))

(:derived (Flag105-)(and (BELOWOF6-ROBOT)(ABOVEOF9-ROBOT)))

(:derived (Flag106-)(and (BELOWOF6-ROBOT)(ABOVEOF10-ROBOT)))

(:derived (Flag108-)(and (ABOVEOF6-ROBOT)(BELOWOF7-ROBOT)))

(:derived (Flag109-)(and (ABOVEOF9-ROBOT)(BELOWOF7-ROBOT)))

(:derived (Flag110-)(and (ABOVEOF6-ROBOT)(BELOWOF6-ROBOT)))

(:derived (Flag111-)(and (ABOVEOF10-ROBOT)(BELOWOF7-ROBOT)))

(:derived (Flag112-)(and (ABOVEOF10-ROBOT)(BELOWOF6-ROBOT)))

(:derived (Flag113-)(and (ABOVEOF12-ROBOT)(BELOWOF7-ROBOT)))

(:derived (Flag114-)(and (BELOWOF7-ROBOT)(ABOVEOF13-ROBOT)))

(:derived (Flag115-)(and (ABOVEOF7-ROBOT)(BELOWOF7-ROBOT)))

(:derived (Flag116-)(and (ABOVEOF8-ROBOT)(BELOWOF7-ROBOT)))

(:derived (Flag117-)(and (ABOVEOF11-ROBOT)(BELOWOF7-ROBOT)))

(:derived (Flag119-)(and (BELOWOF8-ROBOT)(ABOVEOF13-ROBOT)))

(:derived (Flag120-)(and (BELOWOF8-ROBOT)(ABOVEOF10-ROBOT)))

(:derived (Flag121-)(and (ABOVEOF12-ROBOT)(BELOWOF8-ROBOT)))

(:derived (Flag122-)(and (ABOVEOF8-ROBOT)(BELOWOF8-ROBOT)))

(:derived (Flag123-)(and (BELOWOF8-ROBOT)(ABOVEOF9-ROBOT)))

(:derived (Flag124-)(and (BELOWOF7-ROBOT)(ABOVEOF7-ROBOT)))

(:derived (Flag125-)(and (BELOWOF8-ROBOT)(ABOVEOF7-ROBOT)))

(:derived (Flag126-)(and (BELOWOF8-ROBOT)(ABOVEOF11-ROBOT)))

(:derived (Flag128-)(and (BELOWOF8-ROBOT)(ABOVEOF12-ROBOT)))

(:derived (Flag129-)(and (ABOVEOF10-ROBOT)(BELOWOF9-ROBOT)))

(:derived (Flag130-)(and (BELOWOF8-ROBOT)(ABOVEOF8-ROBOT)))

(:derived (Flag131-)(and (BELOWOF9-ROBOT)(ABOVEOF13-ROBOT)))

(:derived (Flag132-)(and (ABOVEOF8-ROBOT)(BELOWOF9-ROBOT)))

(:derived (Flag133-)(and (ABOVEOF12-ROBOT)(BELOWOF9-ROBOT)))

(:derived (Flag134-)(and (ABOVEOF11-ROBOT)(BELOWOF9-ROBOT)))

(:derived (Flag135-)(and (BELOWOF9-ROBOT)(ABOVEOF9-ROBOT)))

(:derived (Flag137-)(and (BELOWOF10-ROBOT)(ABOVEOF9-ROBOT)))

(:derived (Flag138-)(and (BELOWOF10-ROBOT)(ABOVEOF10-ROBOT)))

(:derived (Flag139-)(and (ABOVEOF12-ROBOT)(BELOWOF10-ROBOT)))

(:derived (Flag140-)(and (ABOVEOF9-ROBOT)(BELOWOF9-ROBOT)))

(:derived (Flag141-)(and (ABOVEOF11-ROBOT)(BELOWOF10-ROBOT)))

(:derived (Flag142-)(and (BELOWOF10-ROBOT)(ABOVEOF13-ROBOT)))

(:derived (Flag144-)(and (BELOWOF11-ROBOT)(ABOVEOF10-ROBOT)))

(:derived (Flag145-)(and (ABOVEOF11-ROBOT)(BELOWOF11-ROBOT)))

(:derived (Flag146-)(and (BELOWOF11-ROBOT)(ABOVEOF13-ROBOT)))

(:derived (Flag147-)(and (ABOVEOF12-ROBOT)(BELOWOF11-ROBOT)))

(:derived (Flag149-)(and (ABOVEOF12-ROBOT)(BELOWOF12-ROBOT)))

(:derived (Flag150-)(and (BELOWOF12-ROBOT)(ABOVEOF13-ROBOT)))

(:derived (Flag151-)(and (BELOWOF12-ROBOT)(ABOVEOF11-ROBOT)))

(:derived (Flag153-)(and (ABOVEOF13-ROBOT)(BELOWOF13-ROBOT)))

(:derived (Flag154-)(and (ABOVEOF12-ROBOT)(BELOWOF13-ROBOT)))

(:derived (Flag155-)(and (BELOWOF12-ROBOT)(ABOVEOF12-ROBOT)))

(:derived (Flag157-)(and (BELOWOF10-ROBOT)))

(:derived (Flag157-)(and (BELOWOF11-ROBOT)))

(:derived (Flag157-)(and (BELOWOF6-ROBOT)))

(:derived (Flag157-)(and (BELOWOF5-ROBOT)))

(:derived (Flag157-)(and (BELOWOF2-ROBOT)))

(:derived (Flag157-)(and (BELOWOF1-ROBOT)))

(:derived (Flag157-)(and (BELOWOF4-ROBOT)))

(:derived (Flag157-)(and (BELOWOF12-ROBOT)))

(:derived (Flag157-)(and (BELOWOF7-ROBOT)))

(:derived (Flag157-)(and (BELOWOF9-ROBOT)))

(:derived (Flag157-)(and (BELOWOF3-ROBOT)))

(:derived (Flag157-)(and (BELOWOF8-ROBOT)))

(:derived (Flag157-)(and (BELOWOF14-ROBOT)))

(:derived (Flag158-)(and (BELOWOF14-ROBOT)(ABOVEOF13-ROBOT)))

(:derived (Flag159-)(and (BELOWOF13-ROBOT)(ABOVEOF13-ROBOT)))

(:action MOVEDOWN-ROBOT
:parameters ()
:precondition
(and
(Flag4-)
)
:effect
(and
(when
(and
(Flag160-)
)
(and
(ROW12-ROBOT)
(not (NOT-ROW12-ROBOT))
)
)
(when
(and
(Flag156-)
)
(and
(ROW11-ROBOT)
(not (NOT-ROW11-ROBOT))
)
)
(when
(and
(Flag152-)
)
(and
(ROW10-ROBOT)
(not (NOT-ROW10-ROBOT))
)
)
(when
(and
(Flag148-)
)
(and
(ROW9-ROBOT)
(not (NOT-ROW9-ROBOT))
)
)
(when
(and
(Flag143-)
)
(and
(ROW8-ROBOT)
(not (NOT-ROW8-ROBOT))
)
)
(when
(and
(Flag136-)
)
(and
(ROW7-ROBOT)
(not (NOT-ROW7-ROBOT))
)
)
(when
(and
(Flag127-)
)
(and
(ROW6-ROBOT)
(not (NOT-ROW6-ROBOT))
)
)
(when
(and
(Flag118-)
)
(and
(ROW5-ROBOT)
(not (NOT-ROW5-ROBOT))
)
)
(when
(and
(Flag107-)
)
(and
(ROW4-ROBOT)
(not (NOT-ROW4-ROBOT))
)
)
(when
(and
(Flag95-)
)
(and
(ROW3-ROBOT)
(not (NOT-ROW3-ROBOT))
)
)
(when
(and
(Flag84-)
)
(and
(ROW2-ROBOT)
(not (NOT-ROW2-ROBOT))
)
)
(when
(and
(Flag70-)
)
(and
(ROW1-ROBOT)
(not (NOT-ROW1-ROBOT))
)
)
(when
(and
(Flag56-)
)
(and
(ROW0-ROBOT)
(not (NOT-ROW0-ROBOT))
)
)
(when
(and
(ROW13-ROBOT)
)
(and
(ROW12-ROBOT)
(NOT-ROW13-ROBOT)
(not (NOT-ROW12-ROBOT))
(not (ROW13-ROBOT))
)
)
(when
(and
(ROW12-ROBOT)
)
(and
(ROW11-ROBOT)
(NOT-ROW12-ROBOT)
(not (NOT-ROW11-ROBOT))
(not (ROW12-ROBOT))
)
)
(when
(and
(ROW11-ROBOT)
)
(and
(ROW10-ROBOT)
(NOT-ROW11-ROBOT)
(not (NOT-ROW10-ROBOT))
(not (ROW11-ROBOT))
)
)
(when
(and
(ROW10-ROBOT)
)
(and
(ROW9-ROBOT)
(NOT-ROW10-ROBOT)
(not (NOT-ROW9-ROBOT))
(not (ROW10-ROBOT))
)
)
(when
(and
(ROW9-ROBOT)
)
(and
(ROW8-ROBOT)
(NOT-ROW9-ROBOT)
(not (NOT-ROW8-ROBOT))
(not (ROW9-ROBOT))
)
)
(when
(and
(ROW8-ROBOT)
)
(and
(ROW7-ROBOT)
(NOT-ROW8-ROBOT)
(not (NOT-ROW7-ROBOT))
(not (ROW8-ROBOT))
)
)
(when
(and
(ROW7-ROBOT)
)
(and
(ROW6-ROBOT)
(NOT-ROW7-ROBOT)
(not (NOT-ROW6-ROBOT))
(not (ROW7-ROBOT))
)
)
(when
(and
(ROW6-ROBOT)
)
(and
(ROW5-ROBOT)
(NOT-ROW6-ROBOT)
(not (NOT-ROW5-ROBOT))
(not (ROW6-ROBOT))
)
)
(when
(and
(ROW5-ROBOT)
)
(and
(ROW4-ROBOT)
(NOT-ROW5-ROBOT)
(not (NOT-ROW4-ROBOT))
(not (ROW5-ROBOT))
)
)
(when
(and
(ROW4-ROBOT)
)
(and
(ROW3-ROBOT)
(NOT-ROW4-ROBOT)
(not (NOT-ROW3-ROBOT))
(not (ROW4-ROBOT))
)
)
(when
(and
(ROW3-ROBOT)
)
(and
(ROW2-ROBOT)
(NOT-ROW3-ROBOT)
(not (NOT-ROW2-ROBOT))
(not (ROW3-ROBOT))
)
)
(when
(and
(ROW2-ROBOT)
)
(and
(ROW1-ROBOT)
(NOT-ROW2-ROBOT)
(not (NOT-ROW1-ROBOT))
(not (ROW2-ROBOT))
)
)
(when
(and
(ROW1-ROBOT)
)
(and
(ROW0-ROBOT)
(NOT-ROW1-ROBOT)
(not (NOT-ROW0-ROBOT))
(not (ROW1-ROBOT))
)
)
(when
(and
(ABOVEOF13-ROBOT)
)
(and
(ABOVEOF12-ROBOT)
(NOT-ABOVEOF13-ROBOT)
(ABOVEOF11-ROBOT)
(NOT-ABOVEOF12-ROBOT)
(ABOVEOF10-ROBOT)
(NOT-ABOVEOF11-ROBOT)
(ABOVEOF9-ROBOT)
(NOT-ABOVEOF10-ROBOT)
(ABOVEOF8-ROBOT)
(NOT-ABOVEOF9-ROBOT)
(ABOVEOF7-ROBOT)
(NOT-ABOVEOF8-ROBOT)
(ABOVEOF6-ROBOT)
(NOT-ABOVEOF7-ROBOT)
(ABOVEOF5-ROBOT)
(NOT-ABOVEOF6-ROBOT)
(ABOVEOF4-ROBOT)
(NOT-ABOVEOF5-ROBOT)
(ABOVEOF3-ROBOT)
(NOT-ABOVEOF4-ROBOT)
(ABOVEOF2-ROBOT)
(NOT-ABOVEOF3-ROBOT)
(ABOVEOF1-ROBOT)
(NOT-ABOVEOF2-ROBOT)
(NOT-ABOVEOF1-ROBOT)
(not (ABOVEOF13-ROBOT))
)
)
(when
(and
(ABOVEOF12-ROBOT)
)
(and
(ABOVEOF11-ROBOT)
(NOT-ABOVEOF12-ROBOT)
(ABOVEOF10-ROBOT)
(NOT-ABOVEOF11-ROBOT)
(ABOVEOF9-ROBOT)
(NOT-ABOVEOF10-ROBOT)
(ABOVEOF8-ROBOT)
(NOT-ABOVEOF9-ROBOT)
(ABOVEOF7-ROBOT)
(NOT-ABOVEOF8-ROBOT)
(ABOVEOF6-ROBOT)
(NOT-ABOVEOF7-ROBOT)
(ABOVEOF5-ROBOT)
(NOT-ABOVEOF6-ROBOT)
(ABOVEOF4-ROBOT)
(NOT-ABOVEOF5-ROBOT)
(ABOVEOF3-ROBOT)
(NOT-ABOVEOF4-ROBOT)
(ABOVEOF2-ROBOT)
(NOT-ABOVEOF3-ROBOT)
(ABOVEOF1-ROBOT)
(NOT-ABOVEOF2-ROBOT)
(NOT-ABOVEOF1-ROBOT)
(not (ABOVEOF12-ROBOT))
)
)
(when
(and
(ABOVEOF11-ROBOT)
)
(and
(ABOVEOF10-ROBOT)
(NOT-ABOVEOF11-ROBOT)
(ABOVEOF9-ROBOT)
(NOT-ABOVEOF10-ROBOT)
(ABOVEOF8-ROBOT)
(NOT-ABOVEOF9-ROBOT)
(ABOVEOF7-ROBOT)
(NOT-ABOVEOF8-ROBOT)
(ABOVEOF6-ROBOT)
(NOT-ABOVEOF7-ROBOT)
(ABOVEOF5-ROBOT)
(NOT-ABOVEOF6-ROBOT)
(ABOVEOF4-ROBOT)
(NOT-ABOVEOF5-ROBOT)
(ABOVEOF3-ROBOT)
(NOT-ABOVEOF4-ROBOT)
(ABOVEOF2-ROBOT)
(NOT-ABOVEOF3-ROBOT)
(ABOVEOF1-ROBOT)
(NOT-ABOVEOF2-ROBOT)
(NOT-ABOVEOF1-ROBOT)
(not (ABOVEOF11-ROBOT))
)
)
(when
(and
(ABOVEOF10-ROBOT)
)
(and
(ABOVEOF9-ROBOT)
(NOT-ABOVEOF10-ROBOT)
(ABOVEOF8-ROBOT)
(NOT-ABOVEOF9-ROBOT)
(ABOVEOF7-ROBOT)
(NOT-ABOVEOF8-ROBOT)
(ABOVEOF6-ROBOT)
(NOT-ABOVEOF7-ROBOT)
(ABOVEOF5-ROBOT)
(NOT-ABOVEOF6-ROBOT)
(ABOVEOF4-ROBOT)
(NOT-ABOVEOF5-ROBOT)
(ABOVEOF3-ROBOT)
(NOT-ABOVEOF4-ROBOT)
(ABOVEOF2-ROBOT)
(NOT-ABOVEOF3-ROBOT)
(ABOVEOF1-ROBOT)
(NOT-ABOVEOF2-ROBOT)
(NOT-ABOVEOF1-ROBOT)
(not (ABOVEOF10-ROBOT))
)
)
(when
(and
(ABOVEOF9-ROBOT)
)
(and
(ABOVEOF8-ROBOT)
(NOT-ABOVEOF9-ROBOT)
(ABOVEOF7-ROBOT)
(NOT-ABOVEOF8-ROBOT)
(ABOVEOF6-ROBOT)
(NOT-ABOVEOF7-ROBOT)
(ABOVEOF5-ROBOT)
(NOT-ABOVEOF6-ROBOT)
(ABOVEOF4-ROBOT)
(NOT-ABOVEOF5-ROBOT)
(ABOVEOF3-ROBOT)
(NOT-ABOVEOF4-ROBOT)
(ABOVEOF2-ROBOT)
(NOT-ABOVEOF3-ROBOT)
(ABOVEOF1-ROBOT)
(NOT-ABOVEOF2-ROBOT)
(NOT-ABOVEOF1-ROBOT)
(not (ABOVEOF9-ROBOT))
)
)
(when
(and
(ABOVEOF8-ROBOT)
)
(and
(ABOVEOF7-ROBOT)
(NOT-ABOVEOF8-ROBOT)
(ABOVEOF6-ROBOT)
(NOT-ABOVEOF7-ROBOT)
(ABOVEOF5-ROBOT)
(NOT-ABOVEOF6-ROBOT)
(ABOVEOF4-ROBOT)
(NOT-ABOVEOF5-ROBOT)
(ABOVEOF3-ROBOT)
(NOT-ABOVEOF4-ROBOT)
(ABOVEOF2-ROBOT)
(NOT-ABOVEOF3-ROBOT)
(ABOVEOF1-ROBOT)
(NOT-ABOVEOF2-ROBOT)
(NOT-ABOVEOF1-ROBOT)
(not (ABOVEOF8-ROBOT))
)
)
(when
(and
(ABOVEOF7-ROBOT)
)
(and
(ABOVEOF6-ROBOT)
(NOT-ABOVEOF7-ROBOT)
(ABOVEOF5-ROBOT)
(NOT-ABOVEOF6-ROBOT)
(ABOVEOF4-ROBOT)
(NOT-ABOVEOF5-ROBOT)
(ABOVEOF3-ROBOT)
(NOT-ABOVEOF4-ROBOT)
(ABOVEOF2-ROBOT)
(NOT-ABOVEOF3-ROBOT)
(ABOVEOF1-ROBOT)
(NOT-ABOVEOF2-ROBOT)
(NOT-ABOVEOF1-ROBOT)
(not (ABOVEOF7-ROBOT))
)
)
(when
(and
(ABOVEOF6-ROBOT)
)
(and
(ABOVEOF5-ROBOT)
(NOT-ABOVEOF6-ROBOT)
(ABOVEOF4-ROBOT)
(NOT-ABOVEOF5-ROBOT)
(ABOVEOF3-ROBOT)
(NOT-ABOVEOF4-ROBOT)
(ABOVEOF2-ROBOT)
(NOT-ABOVEOF3-ROBOT)
(ABOVEOF1-ROBOT)
(NOT-ABOVEOF2-ROBOT)
(NOT-ABOVEOF1-ROBOT)
(not (ABOVEOF6-ROBOT))
)
)
(when
(and
(ABOVEOF5-ROBOT)
)
(and
(ABOVEOF4-ROBOT)
(NOT-ABOVEOF5-ROBOT)
(ABOVEOF3-ROBOT)
(NOT-ABOVEOF4-ROBOT)
(ABOVEOF2-ROBOT)
(NOT-ABOVEOF3-ROBOT)
(ABOVEOF1-ROBOT)
(NOT-ABOVEOF2-ROBOT)
(NOT-ABOVEOF1-ROBOT)
(not (ABOVEOF5-ROBOT))
)
)
(when
(and
(ABOVEOF4-ROBOT)
)
(and
(ABOVEOF3-ROBOT)
(NOT-ABOVEOF4-ROBOT)
(ABOVEOF2-ROBOT)
(NOT-ABOVEOF3-ROBOT)
(ABOVEOF1-ROBOT)
(NOT-ABOVEOF2-ROBOT)
(NOT-ABOVEOF1-ROBOT)
(not (ABOVEOF4-ROBOT))
)
)
(when
(and
(ABOVEOF3-ROBOT)
)
(and
(ABOVEOF2-ROBOT)
(NOT-ABOVEOF3-ROBOT)
(ABOVEOF1-ROBOT)
(NOT-ABOVEOF2-ROBOT)
(NOT-ABOVEOF1-ROBOT)
(not (ABOVEOF3-ROBOT))
)
)
(when
(and
(ABOVEOF2-ROBOT)
)
(and
(ABOVEOF1-ROBOT)
(NOT-ABOVEOF2-ROBOT)
(NOT-ABOVEOF1-ROBOT)
(not (ABOVEOF2-ROBOT))
)
)
(when
(and
(ABOVEOF1-ROBOT)
)
(and
(NOT-ABOVEOF1-ROBOT)
(not (ABOVEOF1-ROBOT))
)
)
(when
(and
(Flag157-)
)
(and
(BELOWOF13-ROBOT)
(not (NOT-BELOWOF13-ROBOT))
)
)
(when
(and
(BELOWOF8-ROBOT)
)
(and
(BELOWOF12-ROBOT)
(BELOWOF11-ROBOT)
(BELOWOF10-ROBOT)
(BELOWOF9-ROBOT)
(BELOWOF7-ROBOT)
(not (NOT-BELOWOF12-ROBOT))
(not (NOT-BELOWOF11-ROBOT))
(not (NOT-BELOWOF10-ROBOT))
(not (NOT-BELOWOF9-ROBOT))
(not (NOT-BELOWOF8-ROBOT))
(not (NOT-BELOWOF7-ROBOT))
)
)
(when
(and
(BELOWOF13-ROBOT)
)
(and
(BELOWOF12-ROBOT)
(not (NOT-BELOWOF12-ROBOT))
)
)
(when
(and
(BELOWOF3-ROBOT)
)
(and
(BELOWOF12-ROBOT)
(BELOWOF11-ROBOT)
(BELOWOF10-ROBOT)
(BELOWOF9-ROBOT)
(BELOWOF8-ROBOT)
(BELOWOF7-ROBOT)
(BELOWOF6-ROBOT)
(BELOWOF5-ROBOT)
(BELOWOF4-ROBOT)
(BELOWOF2-ROBOT)
(not (NOT-BELOWOF12-ROBOT))
(not (NOT-BELOWOF11-ROBOT))
(not (NOT-BELOWOF10-ROBOT))
(not (NOT-BELOWOF9-ROBOT))
(not (NOT-BELOWOF8-ROBOT))
(not (NOT-BELOWOF7-ROBOT))
(not (NOT-BELOWOF6-ROBOT))
(not (NOT-BELOWOF5-ROBOT))
(not (NOT-BELOWOF4-ROBOT))
(not (NOT-BELOWOF3-ROBOT))
(not (NOT-BELOWOF2-ROBOT))
)
)
(when
(and
(BELOWOF9-ROBOT)
)
(and
(BELOWOF12-ROBOT)
(BELOWOF11-ROBOT)
(BELOWOF10-ROBOT)
(BELOWOF8-ROBOT)
(not (NOT-BELOWOF12-ROBOT))
(not (NOT-BELOWOF11-ROBOT))
(not (NOT-BELOWOF10-ROBOT))
(not (NOT-BELOWOF9-ROBOT))
(not (NOT-BELOWOF8-ROBOT))
)
)
(when
(and
(BELOWOF7-ROBOT)
)
(and
(BELOWOF12-ROBOT)
(BELOWOF11-ROBOT)
(BELOWOF10-ROBOT)
(BELOWOF9-ROBOT)
(BELOWOF8-ROBOT)
(BELOWOF6-ROBOT)
(not (NOT-BELOWOF12-ROBOT))
(not (NOT-BELOWOF11-ROBOT))
(not (NOT-BELOWOF10-ROBOT))
(not (NOT-BELOWOF9-ROBOT))
(not (NOT-BELOWOF8-ROBOT))
(not (NOT-BELOWOF7-ROBOT))
(not (NOT-BELOWOF6-ROBOT))
)
)
(when
(and
(BELOWOF12-ROBOT)
)
(and
(BELOWOF11-ROBOT)
(not (NOT-BELOWOF12-ROBOT))
(not (NOT-BELOWOF11-ROBOT))
)
)
(when
(and
(BELOWOF4-ROBOT)
)
(and
(BELOWOF12-ROBOT)
(BELOWOF11-ROBOT)
(BELOWOF10-ROBOT)
(BELOWOF9-ROBOT)
(BELOWOF8-ROBOT)
(BELOWOF7-ROBOT)
(BELOWOF6-ROBOT)
(BELOWOF5-ROBOT)
(BELOWOF3-ROBOT)
(not (NOT-BELOWOF12-ROBOT))
(not (NOT-BELOWOF11-ROBOT))
(not (NOT-BELOWOF10-ROBOT))
(not (NOT-BELOWOF9-ROBOT))
(not (NOT-BELOWOF8-ROBOT))
(not (NOT-BELOWOF7-ROBOT))
(not (NOT-BELOWOF6-ROBOT))
(not (NOT-BELOWOF5-ROBOT))
(not (NOT-BELOWOF4-ROBOT))
(not (NOT-BELOWOF3-ROBOT))
)
)
(when
(and
(BELOWOF1-ROBOT)
)
(and
(BELOWOF12-ROBOT)
(BELOWOF11-ROBOT)
(BELOWOF10-ROBOT)
(BELOWOF9-ROBOT)
(BELOWOF8-ROBOT)
(BELOWOF7-ROBOT)
(BELOWOF6-ROBOT)
(BELOWOF5-ROBOT)
(BELOWOF4-ROBOT)
(BELOWOF3-ROBOT)
(BELOWOF2-ROBOT)
(not (NOT-BELOWOF12-ROBOT))
(not (NOT-BELOWOF11-ROBOT))
(not (NOT-BELOWOF10-ROBOT))
(not (NOT-BELOWOF9-ROBOT))
(not (NOT-BELOWOF8-ROBOT))
(not (NOT-BELOWOF7-ROBOT))
(not (NOT-BELOWOF6-ROBOT))
(not (NOT-BELOWOF5-ROBOT))
(not (NOT-BELOWOF4-ROBOT))
(not (NOT-BELOWOF3-ROBOT))
(not (NOT-BELOWOF2-ROBOT))
(not (NOT-BELOWOF1-ROBOT))
)
)
(when
(and
(BELOWOF2-ROBOT)
)
(and
(BELOWOF12-ROBOT)
(BELOWOF11-ROBOT)
(BELOWOF10-ROBOT)
(BELOWOF9-ROBOT)
(BELOWOF8-ROBOT)
(BELOWOF7-ROBOT)
(BELOWOF6-ROBOT)
(BELOWOF5-ROBOT)
(BELOWOF4-ROBOT)
(BELOWOF3-ROBOT)
(BELOWOF1-ROBOT)
(not (NOT-BELOWOF12-ROBOT))
(not (NOT-BELOWOF11-ROBOT))
(not (NOT-BELOWOF10-ROBOT))
(not (NOT-BELOWOF9-ROBOT))
(not (NOT-BELOWOF8-ROBOT))
(not (NOT-BELOWOF7-ROBOT))
(not (NOT-BELOWOF6-ROBOT))
(not (NOT-BELOWOF5-ROBOT))
(not (NOT-BELOWOF4-ROBOT))
(not (NOT-BELOWOF3-ROBOT))
(not (NOT-BELOWOF2-ROBOT))
(not (NOT-BELOWOF1-ROBOT))
)
)
(when
(and
(BELOWOF5-ROBOT)
)
(and
(BELOWOF12-ROBOT)
(BELOWOF11-ROBOT)
(BELOWOF10-ROBOT)
(BELOWOF9-ROBOT)
(BELOWOF8-ROBOT)
(BELOWOF7-ROBOT)
(BELOWOF6-ROBOT)
(BELOWOF4-ROBOT)
(not (NOT-BELOWOF12-ROBOT))
(not (NOT-BELOWOF11-ROBOT))
(not (NOT-BELOWOF10-ROBOT))
(not (NOT-BELOWOF9-ROBOT))
(not (NOT-BELOWOF8-ROBOT))
(not (NOT-BELOWOF7-ROBOT))
(not (NOT-BELOWOF6-ROBOT))
(not (NOT-BELOWOF5-ROBOT))
(not (NOT-BELOWOF4-ROBOT))
)
)
(when
(and
(BELOWOF6-ROBOT)
)
(and
(BELOWOF12-ROBOT)
(BELOWOF11-ROBOT)
(BELOWOF10-ROBOT)
(BELOWOF9-ROBOT)
(BELOWOF8-ROBOT)
(BELOWOF7-ROBOT)
(BELOWOF5-ROBOT)
(not (NOT-BELOWOF12-ROBOT))
(not (NOT-BELOWOF11-ROBOT))
(not (NOT-BELOWOF10-ROBOT))
(not (NOT-BELOWOF9-ROBOT))
(not (NOT-BELOWOF8-ROBOT))
(not (NOT-BELOWOF7-ROBOT))
(not (NOT-BELOWOF6-ROBOT))
(not (NOT-BELOWOF5-ROBOT))
)
)
(when
(and
(BELOWOF11-ROBOT)
)
(and
(BELOWOF12-ROBOT)
(BELOWOF10-ROBOT)
(not (NOT-BELOWOF12-ROBOT))
(not (NOT-BELOWOF11-ROBOT))
(not (NOT-BELOWOF10-ROBOT))
)
)
(when
(and
(BELOWOF10-ROBOT)
)
(and
(BELOWOF12-ROBOT)
(BELOWOF11-ROBOT)
(BELOWOF9-ROBOT)
(not (NOT-BELOWOF12-ROBOT))
(not (NOT-BELOWOF11-ROBOT))
(not (NOT-BELOWOF10-ROBOT))
(not (NOT-BELOWOF9-ROBOT))
)
)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action MOVEUP-ROBOT
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
)
:effect
(and
(when
(and
(Flag156-)
)
(and
(ROW13-ROBOT)
(not (NOT-ROW13-ROBOT))
)
)
(when
(and
(Flag152-)
)
(and
(ROW12-ROBOT)
(not (NOT-ROW12-ROBOT))
)
)
(when
(and
(Flag148-)
)
(and
(ROW11-ROBOT)
(not (NOT-ROW11-ROBOT))
)
)
(when
(and
(Flag143-)
)
(and
(ROW10-ROBOT)
(not (NOT-ROW10-ROBOT))
)
)
(when
(and
(Flag136-)
)
(and
(ROW9-ROBOT)
(not (NOT-ROW9-ROBOT))
)
)
(when
(and
(Flag127-)
)
(and
(ROW8-ROBOT)
(not (NOT-ROW8-ROBOT))
)
)
(when
(and
(Flag118-)
)
(and
(ROW7-ROBOT)
(not (NOT-ROW7-ROBOT))
)
)
(when
(and
(Flag107-)
)
(and
(ROW6-ROBOT)
(not (NOT-ROW6-ROBOT))
)
)
(when
(and
(Flag95-)
)
(and
(ROW5-ROBOT)
(not (NOT-ROW5-ROBOT))
)
)
(when
(and
(Flag84-)
)
(and
(ROW4-ROBOT)
(not (NOT-ROW4-ROBOT))
)
)
(when
(and
(Flag70-)
)
(and
(ROW3-ROBOT)
(not (NOT-ROW3-ROBOT))
)
)
(when
(and
(Flag56-)
)
(and
(ROW2-ROBOT)
(not (NOT-ROW2-ROBOT))
)
)
(when
(and
(Flag42-)
)
(and
(ROW1-ROBOT)
(not (NOT-ROW1-ROBOT))
)
)
(when
(and
(ROW12-ROBOT)
)
(and
(ROW13-ROBOT)
(NOT-ROW12-ROBOT)
(not (NOT-ROW13-ROBOT))
(not (ROW12-ROBOT))
)
)
(when
(and
(ROW11-ROBOT)
)
(and
(ROW12-ROBOT)
(NOT-ROW11-ROBOT)
(not (NOT-ROW12-ROBOT))
(not (ROW11-ROBOT))
)
)
(when
(and
(ROW10-ROBOT)
)
(and
(ROW11-ROBOT)
(NOT-ROW10-ROBOT)
(not (NOT-ROW11-ROBOT))
(not (ROW10-ROBOT))
)
)
(when
(and
(ROW9-ROBOT)
)
(and
(ROW10-ROBOT)
(NOT-ROW9-ROBOT)
(not (NOT-ROW10-ROBOT))
(not (ROW9-ROBOT))
)
)
(when
(and
(ROW8-ROBOT)
)
(and
(ROW9-ROBOT)
(NOT-ROW8-ROBOT)
(not (NOT-ROW9-ROBOT))
(not (ROW8-ROBOT))
)
)
(when
(and
(ROW7-ROBOT)
)
(and
(ROW8-ROBOT)
(NOT-ROW7-ROBOT)
(not (NOT-ROW8-ROBOT))
(not (ROW7-ROBOT))
)
)
(when
(and
(ROW6-ROBOT)
)
(and
(ROW7-ROBOT)
(NOT-ROW6-ROBOT)
(not (NOT-ROW7-ROBOT))
(not (ROW6-ROBOT))
)
)
(when
(and
(ROW5-ROBOT)
)
(and
(ROW6-ROBOT)
(NOT-ROW5-ROBOT)
(not (NOT-ROW6-ROBOT))
(not (ROW5-ROBOT))
)
)
(when
(and
(ROW4-ROBOT)
)
(and
(ROW5-ROBOT)
(NOT-ROW4-ROBOT)
(not (NOT-ROW5-ROBOT))
(not (ROW4-ROBOT))
)
)
(when
(and
(ROW3-ROBOT)
)
(and
(ROW4-ROBOT)
(NOT-ROW3-ROBOT)
(not (NOT-ROW4-ROBOT))
(not (ROW3-ROBOT))
)
)
(when
(and
(ROW2-ROBOT)
)
(and
(ROW3-ROBOT)
(NOT-ROW2-ROBOT)
(not (NOT-ROW3-ROBOT))
(not (ROW2-ROBOT))
)
)
(when
(and
(ROW1-ROBOT)
)
(and
(ROW2-ROBOT)
(NOT-ROW1-ROBOT)
(not (NOT-ROW2-ROBOT))
(not (ROW1-ROBOT))
)
)
(when
(and
(ROW0-ROBOT)
)
(and
(ROW1-ROBOT)
(NOT-ROW0-ROBOT)
(not (NOT-ROW1-ROBOT))
(not (ROW0-ROBOT))
)
)
(when
(and
(Flag28-)
)
(and
(BELOWOF14-ROBOT)
(NOT-BELOWOF13-ROBOT)
(not (NOT-BELOWOF14-ROBOT))
(not (BELOWOF13-ROBOT))
)
)
(when
(and
(Flag27-)
)
(and
(BELOWOF13-ROBOT)
(NOT-BELOWOF12-ROBOT)
(not (NOT-BELOWOF13-ROBOT))
(not (BELOWOF12-ROBOT))
)
)
(when
(and
(Flag26-)
)
(and
(BELOWOF12-ROBOT)
(NOT-BELOWOF11-ROBOT)
(not (NOT-BELOWOF12-ROBOT))
(not (BELOWOF11-ROBOT))
)
)
(when
(and
(Flag25-)
)
(and
(BELOWOF11-ROBOT)
(NOT-BELOWOF10-ROBOT)
(not (NOT-BELOWOF11-ROBOT))
(not (BELOWOF10-ROBOT))
)
)
(when
(and
(Flag24-)
)
(and
(BELOWOF10-ROBOT)
(NOT-BELOWOF9-ROBOT)
(not (NOT-BELOWOF10-ROBOT))
(not (BELOWOF9-ROBOT))
)
)
(when
(and
(Flag23-)
)
(and
(BELOWOF9-ROBOT)
(NOT-BELOWOF8-ROBOT)
(not (NOT-BELOWOF9-ROBOT))
(not (BELOWOF8-ROBOT))
)
)
(when
(and
(Flag22-)
)
(and
(BELOWOF8-ROBOT)
(NOT-BELOWOF7-ROBOT)
(not (NOT-BELOWOF8-ROBOT))
(not (BELOWOF7-ROBOT))
)
)
(when
(and
(Flag21-)
)
(and
(BELOWOF7-ROBOT)
(NOT-BELOWOF6-ROBOT)
(not (NOT-BELOWOF7-ROBOT))
(not (BELOWOF6-ROBOT))
)
)
(when
(and
(Flag20-)
)
(and
(BELOWOF6-ROBOT)
(NOT-BELOWOF5-ROBOT)
(not (NOT-BELOWOF6-ROBOT))
(not (BELOWOF5-ROBOT))
)
)
(when
(and
(Flag19-)
)
(and
(BELOWOF5-ROBOT)
(NOT-BELOWOF4-ROBOT)
(not (NOT-BELOWOF5-ROBOT))
(not (BELOWOF4-ROBOT))
)
)
(when
(and
(Flag18-)
)
(and
(BELOWOF4-ROBOT)
(NOT-BELOWOF3-ROBOT)
(not (NOT-BELOWOF4-ROBOT))
(not (BELOWOF3-ROBOT))
)
)
(when
(and
(Flag17-)
)
(and
(BELOWOF3-ROBOT)
(NOT-BELOWOF2-ROBOT)
(not (NOT-BELOWOF3-ROBOT))
(not (BELOWOF2-ROBOT))
)
)
(when
(and
(BELOWOF1-ROBOT)
)
(and
(BELOWOF2-ROBOT)
(NOT-BELOWOF1-ROBOT)
(not (NOT-BELOWOF2-ROBOT))
(not (BELOWOF1-ROBOT))
)
)
(when
(and
(Flag16-)
)
(and
(ABOVEOF13-ROBOT)
(not (NOT-ABOVEOF13-ROBOT))
)
)
(when
(and
(Flag15-)
)
(and
(ABOVEOF12-ROBOT)
(not (NOT-ABOVEOF12-ROBOT))
)
)
(when
(and
(Flag14-)
)
(and
(ABOVEOF11-ROBOT)
(not (NOT-ABOVEOF11-ROBOT))
)
)
(when
(and
(Flag13-)
)
(and
(ABOVEOF10-ROBOT)
(not (NOT-ABOVEOF10-ROBOT))
)
)
(when
(and
(Flag12-)
)
(and
(ABOVEOF9-ROBOT)
(not (NOT-ABOVEOF9-ROBOT))
)
)
(when
(and
(Flag11-)
)
(and
(ABOVEOF8-ROBOT)
(not (NOT-ABOVEOF8-ROBOT))
)
)
(when
(and
(Flag10-)
)
(and
(ABOVEOF7-ROBOT)
(not (NOT-ABOVEOF7-ROBOT))
)
)
(when
(and
(Flag9-)
)
(and
(ABOVEOF6-ROBOT)
(not (NOT-ABOVEOF6-ROBOT))
)
)
(when
(and
(Flag8-)
)
(and
(ABOVEOF5-ROBOT)
(not (NOT-ABOVEOF5-ROBOT))
)
)
(when
(and
(Flag7-)
)
(and
(ABOVEOF4-ROBOT)
(not (NOT-ABOVEOF4-ROBOT))
)
)
(when
(and
(Flag6-)
)
(and
(ABOVEOF3-ROBOT)
(not (NOT-ABOVEOF3-ROBOT))
)
)
(when
(and
(Flag5-)
)
(and
(ABOVEOF2-ROBOT)
(not (NOT-ABOVEOF2-ROBOT))
)
)
(ABOVEOF1-ROBOT)
(CHECKCONSISTENCY-)
(not (NOT-ABOVEOF1-ROBOT))
(not (NOT-CHECKCONSISTENCY-))
)
)
(:derived (Flag2-)(and (RIGHTOF2-ROBOT)))

(:derived (Flag2-)(and (LEFTOF13-ROBOT)))

(:derived (Flag4-)(and (NOT-CHECKCONSISTENCY-)(NOT-ERROR-)))

(:derived (Flag28-)(and (BELOWOF13-ROBOT)))

(:derived (Flag157-)(and (BELOWOF13-ROBOT)))

(:derived (Flag161-)(and (RIGHTOF2-ROBOT)))

(:derived (Flag162-)(and (RIGHTOF2-ROBOT)))

(:derived (Flag163-)(and (RIGHTOF2-ROBOT)))

(:derived (Flag185-)(and (LEFTOF13-ROBOT)))

(:derived (Flag312-)(and (LEFTOF13-ROBOT)))

)
