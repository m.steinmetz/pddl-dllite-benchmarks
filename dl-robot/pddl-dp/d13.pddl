(define (domain grounded-SIMPLE-ADL-ROBOT)
(:requirements
:strips
)
(:predicates
(Flag274-)
(Flag164-)
(Flag144-)
(Flag143-)
(Flag142-)
(Flag138-)
(Flag26-)
(Flag4-)
(Flag2-)
(ROW12-ROBOT)
(ROW11-ROBOT)
(ROW10-ROBOT)
(ROW9-ROBOT)
(ROW8-ROBOT)
(ROW7-ROBOT)
(ROW6-ROBOT)
(ROW5-ROBOT)
(ROW4-ROBOT)
(ROW3-ROBOT)
(ROW2-ROBOT)
(ROW1-ROBOT)
(BELOWOF13-ROBOT)
(NOT-BELOWOF12-ROBOT)
(BELOWOF11-ROBOT)
(BELOWOF10-ROBOT)
(BELOWOF9-ROBOT)
(BELOWOF8-ROBOT)
(BELOWOF7-ROBOT)
(BELOWOF6-ROBOT)
(BELOWOF5-ROBOT)
(BELOWOF4-ROBOT)
(BELOWOF3-ROBOT)
(BELOWOF2-ROBOT)
(ABOVEOF12-ROBOT)
(ABOVEOF11-ROBOT)
(ABOVEOF10-ROBOT)
(ABOVEOF9-ROBOT)
(ABOVEOF8-ROBOT)
(ABOVEOF7-ROBOT)
(ABOVEOF6-ROBOT)
(ABOVEOF5-ROBOT)
(ABOVEOF4-ROBOT)
(ABOVEOF3-ROBOT)
(ABOVEOF2-ROBOT)
(ABOVEOF1-ROBOT)
(CHECKCONSISTENCY-)
(ROW0-ROBOT)
(BELOWOF1-ROBOT)
(Flag140-)
(Flag139-)
(Flag136-)
(Flag135-)
(Flag133-)
(Flag132-)
(Flag131-)
(Flag129-)
(Flag128-)
(Flag127-)
(Flag126-)
(Flag125-)
(Flag123-)
(Flag122-)
(Flag121-)
(Flag120-)
(Flag119-)
(Flag118-)
(Flag117-)
(Flag115-)
(Flag114-)
(Flag113-)
(Flag112-)
(Flag111-)
(Flag110-)
(Flag109-)
(Flag107-)
(Flag106-)
(Flag105-)
(Flag104-)
(Flag103-)
(Flag102-)
(Flag101-)
(Flag100-)
(Flag99-)
(Flag97-)
(Flag96-)
(Flag95-)
(Flag94-)
(Flag93-)
(Flag92-)
(Flag91-)
(Flag90-)
(Flag89-)
(Flag87-)
(Flag86-)
(Flag85-)
(Flag84-)
(Flag83-)
(Flag82-)
(Flag81-)
(Flag80-)
(Flag79-)
(Flag77-)
(Flag76-)
(Flag75-)
(Flag74-)
(Flag73-)
(Flag72-)
(Flag71-)
(Flag70-)
(Flag69-)
(Flag68-)
(Flag67-)
(Flag66-)
(Flag64-)
(Flag63-)
(Flag62-)
(Flag61-)
(Flag60-)
(Flag59-)
(Flag58-)
(Flag57-)
(Flag56-)
(Flag55-)
(Flag54-)
(Flag53-)
(Flag51-)
(Flag50-)
(Flag49-)
(Flag48-)
(Flag47-)
(Flag46-)
(Flag45-)
(Flag44-)
(Flag43-)
(Flag42-)
(Flag41-)
(Flag40-)
(Flag39-)
(Flag38-)
(Flag37-)
(Flag36-)
(Flag35-)
(Flag34-)
(Flag33-)
(Flag32-)
(Flag31-)
(Flag30-)
(Flag29-)
(Flag28-)
(Flag27-)
(Flag25-)
(Flag24-)
(Flag23-)
(Flag22-)
(Flag21-)
(Flag20-)
(Flag19-)
(Flag18-)
(Flag17-)
(Flag16-)
(Flag15-)
(Flag14-)
(Flag13-)
(Flag12-)
(Flag11-)
(Flag10-)
(Flag9-)
(Flag8-)
(Flag7-)
(Flag6-)
(Flag5-)
(Flag3-)
(ERROR-)
(COLUMN12-ROBOT)
(COLUMN11-ROBOT)
(COLUMN10-ROBOT)
(COLUMN9-ROBOT)
(COLUMN8-ROBOT)
(COLUMN7-ROBOT)
(COLUMN6-ROBOT)
(COLUMN5-ROBOT)
(COLUMN4-ROBOT)
(COLUMN3-ROBOT)
(COLUMN2-ROBOT)
(COLUMN1-ROBOT)
(LEFTOF13-ROBOT)
(NOT-LEFTOF12-ROBOT)
(LEFTOF11-ROBOT)
(LEFTOF10-ROBOT)
(LEFTOF9-ROBOT)
(LEFTOF8-ROBOT)
(LEFTOF7-ROBOT)
(LEFTOF6-ROBOT)
(LEFTOF5-ROBOT)
(LEFTOF4-ROBOT)
(LEFTOF3-ROBOT)
(LEFTOF2-ROBOT)
(RIGHTOF12-ROBOT)
(RIGHTOF11-ROBOT)
(RIGHTOF10-ROBOT)
(RIGHTOF9-ROBOT)
(RIGHTOF8-ROBOT)
(RIGHTOF7-ROBOT)
(RIGHTOF6-ROBOT)
(RIGHTOF5-ROBOT)
(RIGHTOF4-ROBOT)
(RIGHTOF3-ROBOT)
(RIGHTOF1-ROBOT)
(COLUMN0-ROBOT)
(NOT-RIGHTOF2-ROBOT)
(RIGHTOF0-ROBOT)
(LEFTOF1-ROBOT)
(Flag276-)
(Flag275-)
(Flag272-)
(Flag271-)
(Flag269-)
(Flag268-)
(Flag267-)
(Flag265-)
(Flag264-)
(Flag263-)
(Flag262-)
(Flag260-)
(Flag259-)
(Flag258-)
(Flag257-)
(Flag256-)
(Flag254-)
(Flag253-)
(Flag252-)
(Flag251-)
(Flag250-)
(Flag249-)
(Flag248-)
(Flag246-)
(Flag245-)
(Flag244-)
(Flag243-)
(Flag242-)
(Flag241-)
(Flag240-)
(Flag239-)
(Flag238-)
(Flag236-)
(Flag235-)
(Flag234-)
(Flag233-)
(Flag232-)
(Flag231-)
(Flag230-)
(Flag229-)
(Flag227-)
(Flag226-)
(Flag225-)
(Flag224-)
(Flag223-)
(Flag222-)
(Flag221-)
(Flag220-)
(Flag219-)
(Flag218-)
(Flag217-)
(Flag215-)
(Flag214-)
(Flag213-)
(Flag212-)
(Flag211-)
(Flag210-)
(Flag209-)
(Flag208-)
(Flag207-)
(Flag206-)
(Flag205-)
(Flag203-)
(Flag202-)
(Flag201-)
(Flag200-)
(Flag199-)
(Flag198-)
(Flag197-)
(Flag196-)
(Flag195-)
(Flag194-)
(Flag193-)
(Flag191-)
(Flag190-)
(Flag189-)
(Flag188-)
(Flag187-)
(Flag186-)
(Flag185-)
(Flag184-)
(Flag183-)
(Flag182-)
(Flag181-)
(Flag180-)
(Flag179-)
(Flag177-)
(Flag176-)
(Flag175-)
(Flag174-)
(Flag173-)
(Flag172-)
(Flag171-)
(Flag170-)
(Flag169-)
(Flag168-)
(Flag167-)
(Flag166-)
(Flag165-)
(Flag163-)
(Flag162-)
(Flag161-)
(Flag160-)
(Flag159-)
(Flag158-)
(Flag157-)
(Flag156-)
(Flag155-)
(Flag154-)
(Flag153-)
(Flag152-)
(Flag151-)
(Flag150-)
(Flag149-)
(Flag148-)
(Flag147-)
(Flag146-)
(Flag145-)
(Flag141-)
(Flag137-)
(Flag134-)
(Flag130-)
(Flag124-)
(Flag116-)
(Flag108-)
(Flag98-)
(Flag88-)
(Flag78-)
(Flag65-)
(Flag52-)
(Flag1-)
(Flag277-)
(Flag273-)
(Flag270-)
(Flag266-)
(Flag261-)
(Flag255-)
(Flag247-)
(Flag237-)
(Flag228-)
(Flag216-)
(Flag204-)
(Flag192-)
(Flag178-)
(NOT-COLUMN11-ROBOT)
(NOT-COLUMN10-ROBOT)
(NOT-COLUMN9-ROBOT)
(NOT-COLUMN8-ROBOT)
(NOT-COLUMN7-ROBOT)
(NOT-COLUMN6-ROBOT)
(NOT-COLUMN5-ROBOT)
(NOT-COLUMN4-ROBOT)
(NOT-COLUMN3-ROBOT)
(NOT-COLUMN2-ROBOT)
(NOT-COLUMN1-ROBOT)
(NOT-COLUMN0-ROBOT)
(NOT-RIGHTOF11-ROBOT)
(NOT-RIGHTOF10-ROBOT)
(NOT-RIGHTOF9-ROBOT)
(NOT-RIGHTOF8-ROBOT)
(NOT-RIGHTOF7-ROBOT)
(NOT-RIGHTOF6-ROBOT)
(NOT-RIGHTOF5-ROBOT)
(NOT-RIGHTOF4-ROBOT)
(NOT-RIGHTOF3-ROBOT)
(NOT-RIGHTOF1-ROBOT)
(RIGHTOF2-ROBOT)
(NOT-RIGHTOF0-ROBOT)
(NOT-LEFTOF11-ROBOT)
(NOT-LEFTOF10-ROBOT)
(NOT-LEFTOF9-ROBOT)
(NOT-LEFTOF8-ROBOT)
(NOT-LEFTOF7-ROBOT)
(NOT-LEFTOF6-ROBOT)
(NOT-LEFTOF5-ROBOT)
(NOT-LEFTOF4-ROBOT)
(NOT-LEFTOF3-ROBOT)
(NOT-LEFTOF2-ROBOT)
(NOT-LEFTOF1-ROBOT)
(NOT-CHECKCONSISTENCY-)
(NOT-COLUMN12-ROBOT)
(NOT-LEFTOF13-ROBOT)
(LEFTOF12-ROBOT)
(NOT-RIGHTOF12-ROBOT)
(NOT-ERROR-)
(NOT-ROW11-ROBOT)
(NOT-ROW10-ROBOT)
(NOT-ROW9-ROBOT)
(NOT-ROW8-ROBOT)
(NOT-ROW7-ROBOT)
(NOT-ROW6-ROBOT)
(NOT-ROW5-ROBOT)
(NOT-ROW4-ROBOT)
(NOT-ROW3-ROBOT)
(NOT-ROW2-ROBOT)
(NOT-ROW1-ROBOT)
(NOT-ROW0-ROBOT)
(NOT-ABOVEOF11-ROBOT)
(NOT-ABOVEOF10-ROBOT)
(NOT-ABOVEOF9-ROBOT)
(NOT-ABOVEOF8-ROBOT)
(NOT-ABOVEOF7-ROBOT)
(NOT-ABOVEOF6-ROBOT)
(NOT-ABOVEOF5-ROBOT)
(NOT-ABOVEOF4-ROBOT)
(NOT-ABOVEOF3-ROBOT)
(NOT-ABOVEOF2-ROBOT)
(NOT-ABOVEOF1-ROBOT)
(NOT-BELOWOF11-ROBOT)
(NOT-BELOWOF10-ROBOT)
(NOT-BELOWOF9-ROBOT)
(NOT-BELOWOF8-ROBOT)
(NOT-BELOWOF7-ROBOT)
(NOT-BELOWOF6-ROBOT)
(NOT-BELOWOF5-ROBOT)
(NOT-BELOWOF4-ROBOT)
(NOT-BELOWOF3-ROBOT)
(NOT-BELOWOF2-ROBOT)
(NOT-BELOWOF1-ROBOT)
(NOT-ROW12-ROBOT)
(NOT-BELOWOF13-ROBOT)
(BELOWOF12-ROBOT)
(NOT-ABOVEOF12-ROBOT)
)
(:derived (Flag178-)(and (Flag165-)))

(:derived (Flag178-)(and (Flag166-)))

(:derived (Flag178-)(and (Flag167-)))

(:derived (Flag178-)(and (Flag168-)))

(:derived (Flag178-)(and (Flag169-)))

(:derived (Flag178-)(and (Flag170-)))

(:derived (Flag178-)(and (Flag171-)))

(:derived (Flag178-)(and (Flag172-)))

(:derived (Flag178-)(and (Flag173-)))

(:derived (Flag178-)(and (Flag174-)))

(:derived (Flag178-)(and (Flag175-)))

(:derived (Flag178-)(and (Flag176-)))

(:derived (Flag178-)(and (Flag177-)))

(:derived (Flag192-)(and (Flag179-)))

(:derived (Flag192-)(and (Flag168-)))

(:derived (Flag192-)(and (Flag170-)))

(:derived (Flag192-)(and (Flag177-)))

(:derived (Flag192-)(and (Flag180-)))

(:derived (Flag192-)(and (Flag181-)))

(:derived (Flag192-)(and (Flag169-)))

(:derived (Flag192-)(and (Flag182-)))

(:derived (Flag192-)(and (Flag183-)))

(:derived (Flag192-)(and (Flag174-)))

(:derived (Flag192-)(and (Flag184-)))

(:derived (Flag192-)(and (Flag165-)))

(:derived (Flag192-)(and (Flag185-)))

(:derived (Flag192-)(and (Flag166-)))

(:derived (Flag192-)(and (Flag167-)))

(:derived (Flag192-)(and (Flag186-)))

(:derived (Flag192-)(and (Flag172-)))

(:derived (Flag192-)(and (Flag175-)))

(:derived (Flag192-)(and (Flag187-)))

(:derived (Flag192-)(and (Flag188-)))

(:derived (Flag192-)(and (Flag189-)))

(:derived (Flag192-)(and (Flag173-)))

(:derived (Flag192-)(and (Flag176-)))

(:derived (Flag192-)(and (Flag190-)))

(:derived (Flag192-)(and (Flag191-)))

(:derived (Flag204-)(and (Flag179-)))

(:derived (Flag204-)(and (Flag193-)))

(:derived (Flag204-)(and (Flag168-)))

(:derived (Flag204-)(and (Flag170-)))

(:derived (Flag204-)(and (Flag194-)))

(:derived (Flag204-)(and (Flag177-)))

(:derived (Flag204-)(and (Flag181-)))

(:derived (Flag204-)(and (Flag167-)))

(:derived (Flag204-)(and (Flag183-)))

(:derived (Flag204-)(and (Flag195-)))

(:derived (Flag204-)(and (Flag174-)))

(:derived (Flag204-)(and (Flag184-)))

(:derived (Flag204-)(and (Flag172-)))

(:derived (Flag204-)(and (Flag196-)))

(:derived (Flag204-)(and (Flag165-)))

(:derived (Flag204-)(and (Flag185-)))

(:derived (Flag204-)(and (Flag166-)))

(:derived (Flag204-)(and (Flag197-)))

(:derived (Flag204-)(and (Flag198-)))

(:derived (Flag204-)(and (Flag182-)))

(:derived (Flag204-)(and (Flag186-)))

(:derived (Flag204-)(and (Flag199-)))

(:derived (Flag204-)(and (Flag175-)))

(:derived (Flag204-)(and (Flag200-)))

(:derived (Flag204-)(and (Flag201-)))

(:derived (Flag204-)(and (Flag187-)))

(:derived (Flag204-)(and (Flag188-)))

(:derived (Flag204-)(and (Flag189-)))

(:derived (Flag204-)(and (Flag173-)))

(:derived (Flag204-)(and (Flag176-)))

(:derived (Flag204-)(and (Flag190-)))

(:derived (Flag204-)(and (Flag202-)))

(:derived (Flag204-)(and (Flag191-)))

(:derived (Flag204-)(and (Flag203-)))

(:derived (Flag216-)(and (Flag179-)))

(:derived (Flag216-)(and (Flag193-)))

(:derived (Flag216-)(and (Flag168-)))

(:derived (Flag216-)(and (Flag170-)))

(:derived (Flag216-)(and (Flag194-)))

(:derived (Flag216-)(and (Flag177-)))

(:derived (Flag216-)(and (Flag205-)))

(:derived (Flag216-)(and (Flag206-)))

(:derived (Flag216-)(and (Flag181-)))

(:derived (Flag216-)(and (Flag198-)))

(:derived (Flag216-)(and (Flag167-)))

(:derived (Flag216-)(and (Flag183-)))

(:derived (Flag216-)(and (Flag195-)))

(:derived (Flag216-)(and (Flag174-)))

(:derived (Flag216-)(and (Flag184-)))

(:derived (Flag216-)(and (Flag172-)))

(:derived (Flag216-)(and (Flag196-)))

(:derived (Flag216-)(and (Flag165-)))

(:derived (Flag216-)(and (Flag185-)))

(:derived (Flag216-)(and (Flag166-)))

(:derived (Flag216-)(and (Flag197-)))

(:derived (Flag216-)(and (Flag207-)))

(:derived (Flag216-)(and (Flag182-)))

(:derived (Flag216-)(and (Flag186-)))

(:derived (Flag216-)(and (Flag208-)))

(:derived (Flag216-)(and (Flag199-)))

(:derived (Flag216-)(and (Flag175-)))

(:derived (Flag216-)(and (Flag201-)))

(:derived (Flag216-)(and (Flag209-)))

(:derived (Flag216-)(and (Flag210-)))

(:derived (Flag216-)(and (Flag211-)))

(:derived (Flag216-)(and (Flag187-)))

(:derived (Flag216-)(and (Flag212-)))

(:derived (Flag216-)(and (Flag213-)))

(:derived (Flag216-)(and (Flag188-)))

(:derived (Flag216-)(and (Flag176-)))

(:derived (Flag216-)(and (Flag214-)))

(:derived (Flag216-)(and (Flag215-)))

(:derived (Flag216-)(and (Flag190-)))

(:derived (Flag216-)(and (Flag202-)))

(:derived (Flag216-)(and (Flag191-)))

(:derived (Flag228-)(and (Flag217-)))

(:derived (Flag228-)(and (Flag179-)))

(:derived (Flag228-)(and (Flag193-)))

(:derived (Flag228-)(and (Flag168-)))

(:derived (Flag228-)(and (Flag218-)))

(:derived (Flag228-)(and (Flag194-)))

(:derived (Flag228-)(and (Flag177-)))

(:derived (Flag228-)(and (Flag219-)))

(:derived (Flag228-)(and (Flag205-)))

(:derived (Flag228-)(and (Flag206-)))

(:derived (Flag228-)(and (Flag181-)))

(:derived (Flag228-)(and (Flag220-)))

(:derived (Flag228-)(and (Flag167-)))

(:derived (Flag228-)(and (Flag183-)))

(:derived (Flag228-)(and (Flag195-)))

(:derived (Flag228-)(and (Flag174-)))

(:derived (Flag228-)(and (Flag221-)))

(:derived (Flag228-)(and (Flag222-)))

(:derived (Flag228-)(and (Flag223-)))

(:derived (Flag228-)(and (Flag165-)))

(:derived (Flag228-)(and (Flag224-)))

(:derived (Flag228-)(and (Flag185-)))

(:derived (Flag228-)(and (Flag166-)))

(:derived (Flag228-)(and (Flag197-)))

(:derived (Flag228-)(and (Flag198-)))

(:derived (Flag228-)(and (Flag182-)))

(:derived (Flag228-)(and (Flag186-)))

(:derived (Flag228-)(and (Flag208-)))

(:derived (Flag228-)(and (Flag225-)))

(:derived (Flag228-)(and (Flag226-)))

(:derived (Flag228-)(and (Flag172-)))

(:derived (Flag228-)(and (Flag196-)))

(:derived (Flag228-)(and (Flag175-)))

(:derived (Flag228-)(and (Flag201-)))

(:derived (Flag228-)(and (Flag209-)))

(:derived (Flag228-)(and (Flag210-)))

(:derived (Flag228-)(and (Flag211-)))

(:derived (Flag228-)(and (Flag187-)))

(:derived (Flag228-)(and (Flag212-)))

(:derived (Flag228-)(and (Flag213-)))

(:derived (Flag228-)(and (Flag188-)))

(:derived (Flag228-)(and (Flag199-)))

(:derived (Flag228-)(and (Flag176-)))

(:derived (Flag228-)(and (Flag227-)))

(:derived (Flag228-)(and (Flag215-)))

(:derived (Flag228-)(and (Flag190-)))

(:derived (Flag228-)(and (Flag202-)))

(:derived (Flag228-)(and (Flag191-)))

(:derived (Flag237-)(and (Flag217-)))

(:derived (Flag237-)(and (Flag179-)))

(:derived (Flag237-)(and (Flag218-)))

(:derived (Flag237-)(and (Flag168-)))

(:derived (Flag237-)(and (Flag202-)))

(:derived (Flag237-)(and (Flag177-)))

(:derived (Flag237-)(and (Flag193-)))

(:derived (Flag237-)(and (Flag206-)))

(:derived (Flag237-)(and (Flag181-)))

(:derived (Flag237-)(and (Flag220-)))

(:derived (Flag237-)(and (Flag182-)))

(:derived (Flag237-)(and (Flag183-)))

(:derived (Flag237-)(and (Flag229-)))

(:derived (Flag237-)(and (Flag195-)))

(:derived (Flag237-)(and (Flag174-)))

(:derived (Flag237-)(and (Flag230-)))

(:derived (Flag237-)(and (Flag196-)))

(:derived (Flag237-)(and (Flag231-)))

(:derived (Flag237-)(and (Flag222-)))

(:derived (Flag237-)(and (Flag223-)))

(:derived (Flag237-)(and (Flag224-)))

(:derived (Flag237-)(and (Flag185-)))

(:derived (Flag237-)(and (Flag166-)))

(:derived (Flag237-)(and (Flag197-)))

(:derived (Flag237-)(and (Flag198-)))

(:derived (Flag237-)(and (Flag167-)))

(:derived (Flag237-)(and (Flag186-)))

(:derived (Flag237-)(and (Flag208-)))

(:derived (Flag237-)(and (Flag225-)))

(:derived (Flag237-)(and (Flag226-)))

(:derived (Flag237-)(and (Flag199-)))

(:derived (Flag237-)(and (Flag232-)))

(:derived (Flag237-)(and (Flag175-)))

(:derived (Flag237-)(and (Flag201-)))

(:derived (Flag237-)(and (Flag209-)))

(:derived (Flag237-)(and (Flag210-)))

(:derived (Flag237-)(and (Flag233-)))

(:derived (Flag237-)(and (Flag234-)))

(:derived (Flag237-)(and (Flag235-)))

(:derived (Flag237-)(and (Flag211-)))

(:derived (Flag237-)(and (Flag187-)))

(:derived (Flag237-)(and (Flag212-)))

(:derived (Flag237-)(and (Flag213-)))

(:derived (Flag237-)(and (Flag188-)))

(:derived (Flag237-)(and (Flag176-)))

(:derived (Flag237-)(and (Flag227-)))

(:derived (Flag237-)(and (Flag215-)))

(:derived (Flag237-)(and (Flag190-)))

(:derived (Flag237-)(and (Flag219-)))

(:derived (Flag237-)(and (Flag236-)))

(:derived (Flag247-)(and (Flag217-)))

(:derived (Flag247-)(and (Flag179-)))

(:derived (Flag247-)(and (Flag218-)))

(:derived (Flag247-)(and (Flag168-)))

(:derived (Flag247-)(and (Flag238-)))

(:derived (Flag247-)(and (Flag212-)))

(:derived (Flag247-)(and (Flag239-)))

(:derived (Flag247-)(and (Flag240-)))

(:derived (Flag247-)(and (Flag202-)))

(:derived (Flag247-)(and (Flag177-)))

(:derived (Flag247-)(and (Flag166-)))

(:derived (Flag247-)(and (Flag193-)))

(:derived (Flag247-)(and (Flag206-)))

(:derived (Flag247-)(and (Flag198-)))

(:derived (Flag247-)(and (Flag182-)))

(:derived (Flag247-)(and (Flag229-)))

(:derived (Flag247-)(and (Flag195-)))

(:derived (Flag247-)(and (Flag174-)))

(:derived (Flag247-)(and (Flag230-)))

(:derived (Flag247-)(and (Flag196-)))

(:derived (Flag247-)(and (Flag231-)))

(:derived (Flag247-)(and (Flag222-)))

(:derived (Flag247-)(and (Flag223-)))

(:derived (Flag247-)(and (Flag224-)))

(:derived (Flag247-)(and (Flag185-)))

(:derived (Flag247-)(and (Flag241-)))

(:derived (Flag247-)(and (Flag197-)))

(:derived (Flag247-)(and (Flag220-)))

(:derived (Flag247-)(and (Flag167-)))

(:derived (Flag247-)(and (Flag186-)))

(:derived (Flag247-)(and (Flag242-)))

(:derived (Flag247-)(and (Flag225-)))

(:derived (Flag247-)(and (Flag226-)))

(:derived (Flag247-)(and (Flag208-)))

(:derived (Flag247-)(and (Flag175-)))

(:derived (Flag247-)(and (Flag201-)))

(:derived (Flag247-)(and (Flag209-)))

(:derived (Flag247-)(and (Flag210-)))

(:derived (Flag247-)(and (Flag233-)))

(:derived (Flag247-)(and (Flag243-)))

(:derived (Flag247-)(and (Flag244-)))

(:derived (Flag247-)(and (Flag245-)))

(:derived (Flag247-)(and (Flag234-)))

(:derived (Flag247-)(and (Flag211-)))

(:derived (Flag247-)(and (Flag187-)))

(:derived (Flag247-)(and (Flag246-)))

(:derived (Flag247-)(and (Flag188-)))

(:derived (Flag247-)(and (Flag176-)))

(:derived (Flag247-)(and (Flag215-)))

(:derived (Flag247-)(and (Flag190-)))

(:derived (Flag247-)(and (Flag219-)))

(:derived (Flag247-)(and (Flag236-)))

(:derived (Flag255-)(and (Flag217-)))

(:derived (Flag255-)(and (Flag248-)))

(:derived (Flag255-)(and (Flag218-)))

(:derived (Flag255-)(and (Flag168-)))

(:derived (Flag255-)(and (Flag224-)))

(:derived (Flag255-)(and (Flag238-)))

(:derived (Flag255-)(and (Flag240-)))

(:derived (Flag255-)(and (Flag185-)))

(:derived (Flag255-)(and (Flag249-)))

(:derived (Flag255-)(and (Flag177-)))

(:derived (Flag255-)(and (Flag219-)))

(:derived (Flag255-)(and (Flag206-)))

(:derived (Flag255-)(and (Flag198-)))

(:derived (Flag255-)(and (Flag167-)))

(:derived (Flag255-)(and (Flag179-)))

(:derived (Flag255-)(and (Flag229-)))

(:derived (Flag255-)(and (Flag195-)))

(:derived (Flag255-)(and (Flag174-)))

(:derived (Flag255-)(and (Flag230-)))

(:derived (Flag255-)(and (Flag196-)))

(:derived (Flag255-)(and (Flag231-)))

(:derived (Flag255-)(and (Flag222-)))

(:derived (Flag255-)(and (Flag250-)))

(:derived (Flag255-)(and (Flag251-)))

(:derived (Flag255-)(and (Flag252-)))

(:derived (Flag255-)(and (Flag253-)))

(:derived (Flag255-)(and (Flag166-)))

(:derived (Flag255-)(and (Flag197-)))

(:derived (Flag255-)(and (Flag220-)))

(:derived (Flag255-)(and (Flag182-)))

(:derived (Flag255-)(and (Flag186-)))

(:derived (Flag255-)(and (Flag242-)))

(:derived (Flag255-)(and (Flag225-)))

(:derived (Flag255-)(and (Flag226-)))

(:derived (Flag255-)(and (Flag208-)))

(:derived (Flag255-)(and (Flag175-)))

(:derived (Flag255-)(and (Flag201-)))

(:derived (Flag255-)(and (Flag209-)))

(:derived (Flag255-)(and (Flag210-)))

(:derived (Flag255-)(and (Flag233-)))

(:derived (Flag255-)(and (Flag243-)))

(:derived (Flag255-)(and (Flag245-)))

(:derived (Flag255-)(and (Flag234-)))

(:derived (Flag255-)(and (Flag211-)))

(:derived (Flag255-)(and (Flag187-)))

(:derived (Flag255-)(and (Flag246-)))

(:derived (Flag255-)(and (Flag188-)))

(:derived (Flag255-)(and (Flag254-)))

(:derived (Flag255-)(and (Flag215-)))

(:derived (Flag255-)(and (Flag202-)))

(:derived (Flag255-)(and (Flag236-)))

(:derived (Flag261-)(and (Flag217-)))

(:derived (Flag261-)(and (Flag179-)))

(:derived (Flag261-)(and (Flag168-)))

(:derived (Flag261-)(and (Flag224-)))

(:derived (Flag261-)(and (Flag238-)))

(:derived (Flag261-)(and (Flag240-)))

(:derived (Flag261-)(and (Flag185-)))

(:derived (Flag261-)(and (Flag249-)))

(:derived (Flag261-)(and (Flag177-)))

(:derived (Flag261-)(and (Flag219-)))

(:derived (Flag261-)(and (Flag256-)))

(:derived (Flag261-)(and (Flag220-)))

(:derived (Flag261-)(and (Flag257-)))

(:derived (Flag261-)(and (Flag229-)))

(:derived (Flag261-)(and (Flag230-)))

(:derived (Flag261-)(and (Flag174-)))

(:derived (Flag261-)(and (Flag196-)))

(:derived (Flag261-)(and (Flag231-)))

(:derived (Flag261-)(and (Flag222-)))

(:derived (Flag261-)(and (Flag258-)))

(:derived (Flag261-)(and (Flag250-)))

(:derived (Flag261-)(and (Flag251-)))

(:derived (Flag261-)(and (Flag253-)))

(:derived (Flag261-)(and (Flag166-)))

(:derived (Flag261-)(and (Flag197-)))

(:derived (Flag261-)(and (Flag198-)))

(:derived (Flag261-)(and (Flag186-)))

(:derived (Flag261-)(and (Flag242-)))

(:derived (Flag261-)(and (Flag225-)))

(:derived (Flag261-)(and (Flag259-)))

(:derived (Flag261-)(and (Flag226-)))

(:derived (Flag261-)(and (Flag260-)))

(:derived (Flag261-)(and (Flag175-)))

(:derived (Flag261-)(and (Flag201-)))

(:derived (Flag261-)(and (Flag209-)))

(:derived (Flag261-)(and (Flag210-)))

(:derived (Flag261-)(and (Flag243-)))

(:derived (Flag261-)(and (Flag245-)))

(:derived (Flag261-)(and (Flag234-)))

(:derived (Flag261-)(and (Flag211-)))

(:derived (Flag261-)(and (Flag187-)))

(:derived (Flag261-)(and (Flag246-)))

(:derived (Flag261-)(and (Flag188-)))

(:derived (Flag261-)(and (Flag254-)))

(:derived (Flag261-)(and (Flag215-)))

(:derived (Flag261-)(and (Flag202-)))

(:derived (Flag261-)(and (Flag236-)))

(:derived (Flag266-)(and (Flag217-)))

(:derived (Flag266-)(and (Flag179-)))

(:derived (Flag266-)(and (Flag168-)))

(:derived (Flag266-)(and (Flag238-)))

(:derived (Flag266-)(and (Flag262-)))

(:derived (Flag266-)(and (Flag263-)))

(:derived (Flag266-)(and (Flag264-)))

(:derived (Flag266-)(and (Flag185-)))

(:derived (Flag266-)(and (Flag249-)))

(:derived (Flag266-)(and (Flag177-)))

(:derived (Flag266-)(and (Flag219-)))

(:derived (Flag266-)(and (Flag220-)))

(:derived (Flag266-)(and (Flag257-)))

(:derived (Flag266-)(and (Flag229-)))

(:derived (Flag266-)(and (Flag174-)))

(:derived (Flag266-)(and (Flag196-)))

(:derived (Flag266-)(and (Flag231-)))

(:derived (Flag266-)(and (Flag258-)))

(:derived (Flag266-)(and (Flag250-)))

(:derived (Flag266-)(and (Flag224-)))

(:derived (Flag266-)(and (Flag253-)))

(:derived (Flag266-)(and (Flag166-)))

(:derived (Flag266-)(and (Flag197-)))

(:derived (Flag266-)(and (Flag265-)))

(:derived (Flag266-)(and (Flag186-)))

(:derived (Flag266-)(and (Flag242-)))

(:derived (Flag266-)(and (Flag225-)))

(:derived (Flag266-)(and (Flag259-)))

(:derived (Flag266-)(and (Flag260-)))

(:derived (Flag266-)(and (Flag201-)))

(:derived (Flag266-)(and (Flag209-)))

(:derived (Flag266-)(and (Flag210-)))

(:derived (Flag266-)(and (Flag243-)))

(:derived (Flag266-)(and (Flag245-)))

(:derived (Flag266-)(and (Flag234-)))

(:derived (Flag266-)(and (Flag211-)))

(:derived (Flag266-)(and (Flag187-)))

(:derived (Flag266-)(and (Flag246-)))

(:derived (Flag266-)(and (Flag254-)))

(:derived (Flag266-)(and (Flag215-)))

(:derived (Flag266-)(and (Flag202-)))

(:derived (Flag266-)(and (Flag236-)))

(:derived (Flag270-)(and (Flag238-)))

(:derived (Flag270-)(and (Flag267-)))

(:derived (Flag270-)(and (Flag263-)))

(:derived (Flag270-)(and (Flag264-)))

(:derived (Flag270-)(and (Flag185-)))

(:derived (Flag270-)(and (Flag262-)))

(:derived (Flag270-)(and (Flag177-)))

(:derived (Flag270-)(and (Flag219-)))

(:derived (Flag270-)(and (Flag229-)))

(:derived (Flag270-)(and (Flag174-)))

(:derived (Flag270-)(and (Flag196-)))

(:derived (Flag270-)(and (Flag231-)))

(:derived (Flag270-)(and (Flag258-)))

(:derived (Flag270-)(and (Flag250-)))

(:derived (Flag270-)(and (Flag224-)))

(:derived (Flag270-)(and (Flag253-)))

(:derived (Flag270-)(and (Flag166-)))

(:derived (Flag270-)(and (Flag197-)))

(:derived (Flag270-)(and (Flag220-)))

(:derived (Flag270-)(and (Flag186-)))

(:derived (Flag270-)(and (Flag242-)))

(:derived (Flag270-)(and (Flag225-)))

(:derived (Flag270-)(and (Flag259-)))

(:derived (Flag270-)(and (Flag260-)))

(:derived (Flag270-)(and (Flag201-)))

(:derived (Flag270-)(and (Flag209-)))

(:derived (Flag270-)(and (Flag245-)))

(:derived (Flag270-)(and (Flag234-)))

(:derived (Flag270-)(and (Flag187-)))

(:derived (Flag270-)(and (Flag246-)))

(:derived (Flag270-)(and (Flag268-)))

(:derived (Flag270-)(and (Flag254-)))

(:derived (Flag270-)(and (Flag215-)))

(:derived (Flag270-)(and (Flag269-)))

(:derived (Flag273-)(and (Flag238-)))

(:derived (Flag273-)(and (Flag267-)))

(:derived (Flag273-)(and (Flag263-)))

(:derived (Flag273-)(and (Flag271-)))

(:derived (Flag273-)(and (Flag262-)))

(:derived (Flag273-)(and (Flag229-)))

(:derived (Flag273-)(and (Flag174-)))

(:derived (Flag273-)(and (Flag196-)))

(:derived (Flag273-)(and (Flag231-)))

(:derived (Flag273-)(and (Flag272-)))

(:derived (Flag273-)(and (Flag253-)))

(:derived (Flag273-)(and (Flag166-)))

(:derived (Flag273-)(and (Flag186-)))

(:derived (Flag273-)(and (Flag242-)))

(:derived (Flag273-)(and (Flag225-)))

(:derived (Flag273-)(and (Flag259-)))

(:derived (Flag273-)(and (Flag260-)))

(:derived (Flag273-)(and (Flag201-)))

(:derived (Flag273-)(and (Flag209-)))

(:derived (Flag273-)(and (Flag245-)))

(:derived (Flag273-)(and (Flag187-)))

(:derived (Flag273-)(and (Flag219-)))

(:derived (Flag273-)(and (Flag254-)))

(:derived (Flag273-)(and (Flag215-)))

(:derived (Flag273-)(and (Flag269-)))

(:derived (Flag277-)(and (Flag245-)))

(:derived (Flag277-)(and (Flag253-)))

(:derived (Flag277-)(and (Flag166-)))

(:derived (Flag277-)(and (Flag242-)))

(:derived (Flag277-)(and (Flag186-)))

(:derived (Flag277-)(and (Flag275-)))

(:derived (Flag277-)(and (Flag225-)))

(:derived (Flag277-)(and (Flag259-)))

(:derived (Flag277-)(and (Flag276-)))

(:derived (Flag277-)(and (Flag262-)))

(:derived (Flag277-)(and (Flag196-)))

(:derived (Flag277-)(and (Flag209-)))

(:derived (Flag277-)(and (Flag269-)))

(:derived (Flag1-)(and (COLUMN2-ROBOT)(ROW1-ROBOT)(NOT-ERROR-)(NOT-CHECKCONSISTENCY-)))

(:derived (Flag2-)(and (LEFTOF3-ROBOT)))

(:derived (Flag2-)(and (RIGHTOF7-ROBOT)))

(:derived (Flag2-)(and (RIGHTOF8-ROBOT)))

(:derived (Flag2-)(and (LEFTOF2-ROBOT)))

(:derived (Flag2-)(and (LEFTOF9-ROBOT)))

(:derived (Flag2-)(and (RIGHTOF0-ROBOT)))

(:derived (Flag2-)(and (LEFTOF8-ROBOT)))

(:derived (Flag2-)(and (LEFTOF13-ROBOT)))

(:derived (Flag2-)(and (LEFTOF5-ROBOT)))

(:derived (Flag2-)(and (RIGHTOF12-ROBOT)))

(:derived (Flag2-)(and (RIGHTOF1-ROBOT)))

(:derived (Flag2-)(and (RIGHTOF6-ROBOT)))

(:derived (Flag2-)(and (RIGHTOF9-ROBOT)))

(:derived (Flag2-)(and (LEFTOF6-ROBOT)))

(:derived (Flag2-)(and (RIGHTOF10-ROBOT)))

(:derived (Flag2-)(and (RIGHTOF11-ROBOT)))

(:derived (Flag2-)(and (LEFTOF11-ROBOT)))

(:derived (Flag2-)(and (RIGHTOF5-ROBOT)))

(:derived (Flag2-)(and (LEFTOF10-ROBOT)))

(:derived (Flag2-)(and (LEFTOF4-ROBOT)))

(:derived (Flag2-)(and (RIGHTOF4-ROBOT)))

(:derived (Flag2-)(and (LEFTOF1-ROBOT)))

(:derived (Flag2-)(and (LEFTOF7-ROBOT)))

(:derived (Flag2-)(and (RIGHTOF3-ROBOT)))

(:derived (Flag39-)(and (Flag27-)))

(:derived (Flag39-)(and (Flag28-)))

(:derived (Flag39-)(and (Flag29-)))

(:derived (Flag39-)(and (Flag30-)))

(:derived (Flag39-)(and (Flag31-)))

(:derived (Flag39-)(and (Flag32-)))

(:derived (Flag39-)(and (Flag33-)))

(:derived (Flag39-)(and (Flag34-)))

(:derived (Flag39-)(and (Flag35-)))

(:derived (Flag39-)(and (Flag36-)))

(:derived (Flag39-)(and (Flag37-)))

(:derived (Flag39-)(and (Flag38-)))

(:derived (Flag52-)(and (Flag40-)))

(:derived (Flag52-)(and (Flag32-)))

(:derived (Flag52-)(and (Flag41-)))

(:derived (Flag52-)(and (Flag42-)))

(:derived (Flag52-)(and (Flag31-)))

(:derived (Flag52-)(and (Flag29-)))

(:derived (Flag52-)(and (Flag34-)))

(:derived (Flag52-)(and (Flag43-)))

(:derived (Flag52-)(and (Flag44-)))

(:derived (Flag52-)(and (Flag28-)))

(:derived (Flag52-)(and (Flag45-)))

(:derived (Flag52-)(and (Flag33-)))

(:derived (Flag52-)(and (Flag35-)))

(:derived (Flag52-)(and (Flag46-)))

(:derived (Flag52-)(and (Flag37-)))

(:derived (Flag52-)(and (Flag38-)))

(:derived (Flag52-)(and (Flag47-)))

(:derived (Flag52-)(and (Flag48-)))

(:derived (Flag52-)(and (Flag27-)))

(:derived (Flag52-)(and (Flag49-)))

(:derived (Flag52-)(and (Flag30-)))

(:derived (Flag52-)(and (Flag50-)))

(:derived (Flag52-)(and (Flag36-)))

(:derived (Flag52-)(and (Flag51-)))

(:derived (Flag65-)(and (Flag53-)))

(:derived (Flag65-)(and (Flag54-)))

(:derived (Flag65-)(and (Flag28-)))

(:derived (Flag65-)(and (Flag55-)))

(:derived (Flag65-)(and (Flag32-)))

(:derived (Flag65-)(and (Flag46-)))

(:derived (Flag65-)(and (Flag42-)))

(:derived (Flag65-)(and (Flag31-)))

(:derived (Flag65-)(and (Flag34-)))

(:derived (Flag65-)(and (Flag56-)))

(:derived (Flag65-)(and (Flag43-)))

(:derived (Flag65-)(and (Flag57-)))

(:derived (Flag65-)(and (Flag44-)))

(:derived (Flag65-)(and (Flag58-)))

(:derived (Flag65-)(and (Flag45-)))

(:derived (Flag65-)(and (Flag59-)))

(:derived (Flag65-)(and (Flag33-)))

(:derived (Flag65-)(and (Flag60-)))

(:derived (Flag65-)(and (Flag35-)))

(:derived (Flag65-)(and (Flag61-)))

(:derived (Flag65-)(and (Flag62-)))

(:derived (Flag65-)(and (Flag37-)))

(:derived (Flag65-)(and (Flag38-)))

(:derived (Flag65-)(and (Flag47-)))

(:derived (Flag65-)(and (Flag48-)))

(:derived (Flag65-)(and (Flag27-)))

(:derived (Flag65-)(and (Flag49-)))

(:derived (Flag65-)(and (Flag63-)))

(:derived (Flag65-)(and (Flag30-)))

(:derived (Flag65-)(and (Flag64-)))

(:derived (Flag65-)(and (Flag50-)))

(:derived (Flag65-)(and (Flag36-)))

(:derived (Flag65-)(and (Flag51-)))

(:derived (Flag78-)(and (Flag53-)))

(:derived (Flag78-)(and (Flag66-)))

(:derived (Flag78-)(and (Flag67-)))

(:derived (Flag78-)(and (Flag54-)))

(:derived (Flag78-)(and (Flag55-)))

(:derived (Flag78-)(and (Flag32-)))

(:derived (Flag78-)(and (Flag46-)))

(:derived (Flag78-)(and (Flag42-)))

(:derived (Flag78-)(and (Flag56-)))

(:derived (Flag78-)(and (Flag31-)))

(:derived (Flag78-)(and (Flag68-)))

(:derived (Flag78-)(and (Flag34-)))

(:derived (Flag78-)(and (Flag69-)))

(:derived (Flag78-)(and (Flag70-)))

(:derived (Flag78-)(and (Flag71-)))

(:derived (Flag78-)(and (Flag43-)))

(:derived (Flag78-)(and (Flag72-)))

(:derived (Flag78-)(and (Flag73-)))

(:derived (Flag78-)(and (Flag74-)))

(:derived (Flag78-)(and (Flag44-)))

(:derived (Flag78-)(and (Flag45-)))

(:derived (Flag78-)(and (Flag59-)))

(:derived (Flag78-)(and (Flag33-)))

(:derived (Flag78-)(and (Flag60-)))

(:derived (Flag78-)(and (Flag49-)))

(:derived (Flag78-)(and (Flag35-)))

(:derived (Flag78-)(and (Flag61-)))

(:derived (Flag78-)(and (Flag75-)))

(:derived (Flag78-)(and (Flag37-)))

(:derived (Flag78-)(and (Flag38-)))

(:derived (Flag78-)(and (Flag47-)))

(:derived (Flag78-)(and (Flag48-)))

(:derived (Flag78-)(and (Flag27-)))

(:derived (Flag78-)(and (Flag76-)))

(:derived (Flag78-)(and (Flag63-)))

(:derived (Flag78-)(and (Flag77-)))

(:derived (Flag78-)(and (Flag30-)))

(:derived (Flag78-)(and (Flag64-)))

(:derived (Flag78-)(and (Flag50-)))

(:derived (Flag78-)(and (Flag36-)))

(:derived (Flag78-)(and (Flag51-)))

(:derived (Flag88-)(and (Flag79-)))

(:derived (Flag88-)(and (Flag53-)))

(:derived (Flag88-)(and (Flag71-)))

(:derived (Flag88-)(and (Flag54-)))

(:derived (Flag88-)(and (Flag55-)))

(:derived (Flag88-)(and (Flag32-)))

(:derived (Flag88-)(and (Flag42-)))

(:derived (Flag88-)(and (Flag56-)))

(:derived (Flag88-)(and (Flag80-)))

(:derived (Flag88-)(and (Flag31-)))

(:derived (Flag88-)(and (Flag81-)))

(:derived (Flag88-)(and (Flag68-)))

(:derived (Flag88-)(and (Flag34-)))

(:derived (Flag88-)(and (Flag69-)))

(:derived (Flag88-)(and (Flag70-)))

(:derived (Flag88-)(and (Flag82-)))

(:derived (Flag88-)(and (Flag83-)))

(:derived (Flag88-)(and (Flag43-)))

(:derived (Flag88-)(and (Flag72-)))

(:derived (Flag88-)(and (Flag73-)))

(:derived (Flag88-)(and (Flag44-)))

(:derived (Flag88-)(and (Flag45-)))

(:derived (Flag88-)(and (Flag59-)))

(:derived (Flag88-)(and (Flag33-)))

(:derived (Flag88-)(and (Flag84-)))

(:derived (Flag88-)(and (Flag60-)))

(:derived (Flag88-)(and (Flag85-)))

(:derived (Flag88-)(and (Flag49-)))

(:derived (Flag88-)(and (Flag35-)))

(:derived (Flag88-)(and (Flag46-)))

(:derived (Flag88-)(and (Flag75-)))

(:derived (Flag88-)(and (Flag67-)))

(:derived (Flag88-)(and (Flag38-)))

(:derived (Flag88-)(and (Flag47-)))

(:derived (Flag88-)(and (Flag48-)))

(:derived (Flag88-)(and (Flag27-)))

(:derived (Flag88-)(and (Flag86-)))

(:derived (Flag88-)(and (Flag76-)))

(:derived (Flag88-)(and (Flag63-)))

(:derived (Flag88-)(and (Flag77-)))

(:derived (Flag88-)(and (Flag87-)))

(:derived (Flag88-)(and (Flag30-)))

(:derived (Flag88-)(and (Flag64-)))

(:derived (Flag88-)(and (Flag50-)))

(:derived (Flag88-)(and (Flag36-)))

(:derived (Flag88-)(and (Flag61-)))

(:derived (Flag98-)(and (Flag53-)))

(:derived (Flag98-)(and (Flag50-)))

(:derived (Flag98-)(and (Flag89-)))

(:derived (Flag98-)(and (Flag54-)))

(:derived (Flag98-)(and (Flag55-)))

(:derived (Flag98-)(and (Flag90-)))

(:derived (Flag98-)(and (Flag32-)))

(:derived (Flag98-)(and (Flag42-)))

(:derived (Flag98-)(and (Flag91-)))

(:derived (Flag98-)(and (Flag80-)))

(:derived (Flag98-)(and (Flag38-)))

(:derived (Flag98-)(and (Flag45-)))

(:derived (Flag98-)(and (Flag31-)))

(:derived (Flag98-)(and (Flag87-)))

(:derived (Flag98-)(and (Flag68-)))

(:derived (Flag98-)(and (Flag34-)))

(:derived (Flag98-)(and (Flag92-)))

(:derived (Flag98-)(and (Flag93-)))

(:derived (Flag98-)(and (Flag56-)))

(:derived (Flag98-)(and (Flag70-)))

(:derived (Flag98-)(and (Flag82-)))

(:derived (Flag98-)(and (Flag71-)))

(:derived (Flag98-)(and (Flag72-)))

(:derived (Flag98-)(and (Flag73-)))

(:derived (Flag98-)(and (Flag44-)))

(:derived (Flag98-)(and (Flag33-)))

(:derived (Flag98-)(and (Flag59-)))

(:derived (Flag98-)(and (Flag94-)))

(:derived (Flag98-)(and (Flag85-)))

(:derived (Flag98-)(and (Flag49-)))

(:derived (Flag98-)(and (Flag35-)))

(:derived (Flag98-)(and (Flag46-)))

(:derived (Flag98-)(and (Flag75-)))

(:derived (Flag98-)(and (Flag95-)))

(:derived (Flag98-)(and (Flag67-)))

(:derived (Flag98-)(and (Flag96-)))

(:derived (Flag98-)(and (Flag47-)))

(:derived (Flag98-)(and (Flag48-)))

(:derived (Flag98-)(and (Flag86-)))

(:derived (Flag98-)(and (Flag76-)))

(:derived (Flag98-)(and (Flag63-)))

(:derived (Flag98-)(and (Flag77-)))

(:derived (Flag98-)(and (Flag97-)))

(:derived (Flag98-)(and (Flag30-)))

(:derived (Flag98-)(and (Flag83-)))

(:derived (Flag98-)(and (Flag64-)))

(:derived (Flag98-)(and (Flag81-)))

(:derived (Flag98-)(and (Flag36-)))

(:derived (Flag98-)(and (Flag61-)))

(:derived (Flag108-)(and (Flag30-)))

(:derived (Flag108-)(and (Flag68-)))

(:derived (Flag108-)(and (Flag99-)))

(:derived (Flag108-)(and (Flag89-)))

(:derived (Flag108-)(and (Flag54-)))

(:derived (Flag108-)(and (Flag97-)))

(:derived (Flag108-)(and (Flag55-)))

(:derived (Flag108-)(and (Flag90-)))

(:derived (Flag108-)(and (Flag100-)))

(:derived (Flag108-)(and (Flag32-)))

(:derived (Flag108-)(and (Flag101-)))

(:derived (Flag108-)(and (Flag42-)))

(:derived (Flag108-)(and (Flag80-)))

(:derived (Flag108-)(and (Flag38-)))

(:derived (Flag108-)(and (Flag31-)))

(:derived (Flag108-)(and (Flag87-)))

(:derived (Flag108-)(and (Flag102-)))

(:derived (Flag108-)(and (Flag92-)))

(:derived (Flag108-)(and (Flag56-)))

(:derived (Flag108-)(and (Flag70-)))

(:derived (Flag108-)(and (Flag82-)))

(:derived (Flag108-)(and (Flag71-)))

(:derived (Flag108-)(and (Flag72-)))

(:derived (Flag108-)(and (Flag103-)))

(:derived (Flag108-)(and (Flag44-)))

(:derived (Flag108-)(and (Flag45-)))

(:derived (Flag108-)(and (Flag59-)))

(:derived (Flag108-)(and (Flag33-)))

(:derived (Flag108-)(and (Flag85-)))

(:derived (Flag108-)(and (Flag49-)))

(:derived (Flag108-)(and (Flag35-)))

(:derived (Flag108-)(and (Flag46-)))

(:derived (Flag108-)(and (Flag75-)))

(:derived (Flag108-)(and (Flag95-)))

(:derived (Flag108-)(and (Flag67-)))

(:derived (Flag108-)(and (Flag104-)))

(:derived (Flag108-)(and (Flag96-)))

(:derived (Flag108-)(and (Flag47-)))

(:derived (Flag108-)(and (Flag48-)))

(:derived (Flag108-)(and (Flag86-)))

(:derived (Flag108-)(and (Flag76-)))

(:derived (Flag108-)(and (Flag63-)))

(:derived (Flag108-)(and (Flag105-)))

(:derived (Flag108-)(and (Flag106-)))

(:derived (Flag108-)(and (Flag107-)))

(:derived (Flag108-)(and (Flag77-)))

(:derived (Flag108-)(and (Flag83-)))

(:derived (Flag108-)(and (Flag64-)))

(:derived (Flag108-)(and (Flag81-)))

(:derived (Flag108-)(and (Flag36-)))

(:derived (Flag108-)(and (Flag61-)))

(:derived (Flag116-)(and (Flag30-)))

(:derived (Flag116-)(and (Flag68-)))

(:derived (Flag116-)(and (Flag32-)))

(:derived (Flag116-)(and (Flag89-)))

(:derived (Flag116-)(and (Flag97-)))

(:derived (Flag116-)(and (Flag55-)))

(:derived (Flag116-)(and (Flag90-)))

(:derived (Flag116-)(and (Flag100-)))

(:derived (Flag116-)(and (Flag49-)))

(:derived (Flag116-)(and (Flag46-)))

(:derived (Flag116-)(and (Flag42-)))

(:derived (Flag116-)(and (Flag80-)))

(:derived (Flag116-)(and (Flag38-)))

(:derived (Flag116-)(and (Flag31-)))

(:derived (Flag116-)(and (Flag102-)))

(:derived (Flag116-)(and (Flag92-)))

(:derived (Flag116-)(and (Flag109-)))

(:derived (Flag116-)(and (Flag56-)))

(:derived (Flag116-)(and (Flag70-)))

(:derived (Flag116-)(and (Flag82-)))

(:derived (Flag116-)(and (Flag110-)))

(:derived (Flag116-)(and (Flag71-)))

(:derived (Flag116-)(and (Flag72-)))

(:derived (Flag116-)(and (Flag103-)))

(:derived (Flag116-)(and (Flag83-)))

(:derived (Flag116-)(and (Flag111-)))

(:derived (Flag116-)(and (Flag45-)))

(:derived (Flag116-)(and (Flag59-)))

(:derived (Flag116-)(and (Flag85-)))

(:derived (Flag116-)(and (Flag112-)))

(:derived (Flag116-)(and (Flag35-)))

(:derived (Flag116-)(and (Flag61-)))

(:derived (Flag116-)(and (Flag75-)))

(:derived (Flag116-)(and (Flag95-)))

(:derived (Flag116-)(and (Flag104-)))

(:derived (Flag116-)(and (Flag96-)))

(:derived (Flag116-)(and (Flag47-)))

(:derived (Flag116-)(and (Flag48-)))

(:derived (Flag116-)(and (Flag113-)))

(:derived (Flag116-)(and (Flag86-)))

(:derived (Flag116-)(and (Flag76-)))

(:derived (Flag116-)(and (Flag63-)))

(:derived (Flag116-)(and (Flag77-)))

(:derived (Flag116-)(and (Flag106-)))

(:derived (Flag116-)(and (Flag107-)))

(:derived (Flag116-)(and (Flag114-)))

(:derived (Flag116-)(and (Flag64-)))

(:derived (Flag116-)(and (Flag87-)))

(:derived (Flag116-)(and (Flag36-)))

(:derived (Flag116-)(and (Flag115-)))

(:derived (Flag124-)(and (Flag117-)))

(:derived (Flag124-)(and (Flag68-)))

(:derived (Flag124-)(and (Flag32-)))

(:derived (Flag124-)(and (Flag106-)))

(:derived (Flag124-)(and (Flag89-)))

(:derived (Flag124-)(and (Flag107-)))

(:derived (Flag124-)(and (Flag118-)))

(:derived (Flag124-)(and (Flag90-)))

(:derived (Flag124-)(and (Flag100-)))

(:derived (Flag124-)(and (Flag49-)))

(:derived (Flag124-)(and (Flag46-)))

(:derived (Flag124-)(and (Flag119-)))

(:derived (Flag124-)(and (Flag80-)))

(:derived (Flag124-)(and (Flag38-)))

(:derived (Flag124-)(and (Flag102-)))

(:derived (Flag124-)(and (Flag109-)))

(:derived (Flag124-)(and (Flag56-)))

(:derived (Flag124-)(and (Flag70-)))

(:derived (Flag124-)(and (Flag82-)))

(:derived (Flag124-)(and (Flag110-)))

(:derived (Flag124-)(and (Flag83-)))

(:derived (Flag124-)(and (Flag72-)))

(:derived (Flag124-)(and (Flag103-)))

(:derived (Flag124-)(and (Flag45-)))

(:derived (Flag124-)(and (Flag59-)))

(:derived (Flag124-)(and (Flag112-)))

(:derived (Flag124-)(and (Flag35-)))

(:derived (Flag124-)(and (Flag61-)))

(:derived (Flag124-)(and (Flag75-)))

(:derived (Flag124-)(and (Flag95-)))

(:derived (Flag124-)(and (Flag104-)))

(:derived (Flag124-)(and (Flag96-)))

(:derived (Flag124-)(and (Flag47-)))

(:derived (Flag124-)(and (Flag48-)))

(:derived (Flag124-)(and (Flag86-)))

(:derived (Flag124-)(and (Flag76-)))

(:derived (Flag124-)(and (Flag120-)))

(:derived (Flag124-)(and (Flag63-)))

(:derived (Flag124-)(and (Flag77-)))

(:derived (Flag124-)(and (Flag121-)))

(:derived (Flag124-)(and (Flag122-)))

(:derived (Flag124-)(and (Flag97-)))

(:derived (Flag124-)(and (Flag30-)))

(:derived (Flag124-)(and (Flag64-)))

(:derived (Flag124-)(and (Flag87-)))

(:derived (Flag124-)(and (Flag123-)))

(:derived (Flag124-)(and (Flag36-)))

(:derived (Flag124-)(and (Flag115-)))

(:derived (Flag130-)(and (Flag117-)))

(:derived (Flag130-)(and (Flag68-)))

(:derived (Flag130-)(and (Flag32-)))

(:derived (Flag130-)(and (Flag89-)))

(:derived (Flag130-)(and (Flag97-)))

(:derived (Flag130-)(and (Flag118-)))

(:derived (Flag130-)(and (Flag90-)))

(:derived (Flag130-)(and (Flag100-)))

(:derived (Flag130-)(and (Flag49-)))

(:derived (Flag130-)(and (Flag125-)))

(:derived (Flag130-)(and (Flag38-)))

(:derived (Flag130-)(and (Flag46-)))

(:derived (Flag130-)(and (Flag102-)))

(:derived (Flag130-)(and (Flag109-)))

(:derived (Flag130-)(and (Flag56-)))

(:derived (Flag130-)(and (Flag82-)))

(:derived (Flag130-)(and (Flag110-)))

(:derived (Flag130-)(and (Flag83-)))

(:derived (Flag130-)(and (Flag72-)))

(:derived (Flag130-)(and (Flag103-)))

(:derived (Flag130-)(and (Flag126-)))

(:derived (Flag130-)(and (Flag45-)))

(:derived (Flag130-)(and (Flag112-)))

(:derived (Flag130-)(and (Flag35-)))

(:derived (Flag130-)(and (Flag127-)))

(:derived (Flag130-)(and (Flag61-)))

(:derived (Flag130-)(and (Flag75-)))

(:derived (Flag130-)(and (Flag104-)))

(:derived (Flag130-)(and (Flag96-)))

(:derived (Flag130-)(and (Flag47-)))

(:derived (Flag130-)(and (Flag128-)))

(:derived (Flag130-)(and (Flag129-)))

(:derived (Flag130-)(and (Flag86-)))

(:derived (Flag130-)(and (Flag76-)))

(:derived (Flag130-)(and (Flag63-)))

(:derived (Flag130-)(and (Flag77-)))

(:derived (Flag130-)(and (Flag121-)))

(:derived (Flag130-)(and (Flag122-)))

(:derived (Flag130-)(and (Flag107-)))

(:derived (Flag130-)(and (Flag30-)))

(:derived (Flag130-)(and (Flag64-)))

(:derived (Flag130-)(and (Flag87-)))

(:derived (Flag130-)(and (Flag115-)))

(:derived (Flag134-)(and (Flag117-)))

(:derived (Flag134-)(and (Flag131-)))

(:derived (Flag134-)(and (Flag89-)))

(:derived (Flag134-)(and (Flag118-)))

(:derived (Flag134-)(and (Flag90-)))

(:derived (Flag134-)(and (Flag32-)))

(:derived (Flag134-)(and (Flag132-)))

(:derived (Flag134-)(and (Flag46-)))

(:derived (Flag134-)(and (Flag102-)))

(:derived (Flag134-)(and (Flag109-)))

(:derived (Flag134-)(and (Flag82-)))

(:derived (Flag134-)(and (Flag110-)))

(:derived (Flag134-)(and (Flag83-)))

(:derived (Flag134-)(and (Flag72-)))

(:derived (Flag134-)(and (Flag103-)))

(:derived (Flag134-)(and (Flag126-)))

(:derived (Flag134-)(and (Flag49-)))

(:derived (Flag134-)(and (Flag35-)))

(:derived (Flag134-)(and (Flag127-)))

(:derived (Flag134-)(and (Flag61-)))

(:derived (Flag134-)(and (Flag75-)))

(:derived (Flag134-)(and (Flag104-)))

(:derived (Flag134-)(and (Flag47-)))

(:derived (Flag134-)(and (Flag129-)))

(:derived (Flag134-)(and (Flag76-)))

(:derived (Flag134-)(and (Flag63-)))

(:derived (Flag134-)(and (Flag77-)))

(:derived (Flag134-)(and (Flag133-)))

(:derived (Flag134-)(and (Flag121-)))

(:derived (Flag134-)(and (Flag122-)))

(:derived (Flag134-)(and (Flag107-)))

(:derived (Flag134-)(and (Flag30-)))

(:derived (Flag134-)(and (Flag64-)))

(:derived (Flag134-)(and (Flag87-)))

(:derived (Flag134-)(and (Flag115-)))

(:derived (Flag137-)(and (Flag117-)))

(:derived (Flag137-)(and (Flag89-)))

(:derived (Flag137-)(and (Flag90-)))

(:derived (Flag137-)(and (Flag32-)))

(:derived (Flag137-)(and (Flag132-)))

(:derived (Flag137-)(and (Flag135-)))

(:derived (Flag137-)(and (Flag110-)))

(:derived (Flag137-)(and (Flag83-)))

(:derived (Flag137-)(and (Flag72-)))

(:derived (Flag137-)(and (Flag136-)))

(:derived (Flag137-)(and (Flag49-)))

(:derived (Flag137-)(and (Flag127-)))

(:derived (Flag137-)(and (Flag46-)))

(:derived (Flag137-)(and (Flag104-)))

(:derived (Flag137-)(and (Flag129-)))

(:derived (Flag137-)(and (Flag76-)))

(:derived (Flag137-)(and (Flag63-)))

(:derived (Flag137-)(and (Flag77-)))

(:derived (Flag137-)(and (Flag133-)))

(:derived (Flag137-)(and (Flag121-)))

(:derived (Flag137-)(and (Flag122-)))

(:derived (Flag137-)(and (Flag107-)))

(:derived (Flag137-)(and (Flag30-)))

(:derived (Flag137-)(and (Flag87-)))

(:derived (Flag137-)(and (Flag115-)))

(:derived (Flag141-)(and (Flag49-)))

(:derived (Flag141-)(and (Flag117-)))

(:derived (Flag141-)(and (Flag63-)))

(:derived (Flag141-)(and (Flag139-)))

(:derived (Flag141-)(and (Flag133-)))

(:derived (Flag141-)(and (Flag121-)))

(:derived (Flag141-)(and (Flag140-)))

(:derived (Flag141-)(and (Flag30-)))

(:derived (Flag141-)(and (Flag87-)))

(:derived (Flag141-)(and (Flag127-)))

(:derived (Flag141-)(and (Flag90-)))

(:derived (Flag141-)(and (Flag104-)))

(:derived (Flag141-)(and (Flag72-)))

(:derived (Flag142-)(and (RIGHTOF3-ROBOT)))

(:derived (Flag142-)(and (RIGHTOF8-ROBOT)))

(:derived (Flag142-)(and (RIGHTOF10-ROBOT)))

(:derived (Flag142-)(and (RIGHTOF5-ROBOT)))

(:derived (Flag142-)(and (RIGHTOF11-ROBOT)))

(:derived (Flag142-)(and (RIGHTOF0-ROBOT)))

(:derived (Flag142-)(and (RIGHTOF4-ROBOT)))

(:derived (Flag142-)(and (RIGHTOF1-ROBOT)))

(:derived (Flag142-)(and (RIGHTOF12-ROBOT)))

(:derived (Flag142-)(and (RIGHTOF7-ROBOT)))

(:derived (Flag142-)(and (RIGHTOF9-ROBOT)))

(:derived (Flag142-)(and (RIGHTOF6-ROBOT)))

(:derived (Flag143-)(and (RIGHTOF3-ROBOT)))

(:derived (Flag143-)(and (RIGHTOF8-ROBOT)))

(:derived (Flag143-)(and (RIGHTOF5-ROBOT)))

(:derived (Flag143-)(and (RIGHTOF11-ROBOT)))

(:derived (Flag143-)(and (RIGHTOF10-ROBOT)))

(:derived (Flag143-)(and (RIGHTOF4-ROBOT)))

(:derived (Flag143-)(and (RIGHTOF1-ROBOT)))

(:derived (Flag143-)(and (RIGHTOF12-ROBOT)))

(:derived (Flag143-)(and (RIGHTOF7-ROBOT)))

(:derived (Flag143-)(and (RIGHTOF9-ROBOT)))

(:derived (Flag143-)(and (RIGHTOF6-ROBOT)))

(:derived (Flag144-)(and (RIGHTOF3-ROBOT)))

(:derived (Flag144-)(and (RIGHTOF5-ROBOT)))

(:derived (Flag144-)(and (RIGHTOF11-ROBOT)))

(:derived (Flag144-)(and (RIGHTOF10-ROBOT)))

(:derived (Flag144-)(and (RIGHTOF4-ROBOT)))

(:derived (Flag144-)(and (RIGHTOF8-ROBOT)))

(:derived (Flag144-)(and (RIGHTOF12-ROBOT)))

(:derived (Flag144-)(and (RIGHTOF7-ROBOT)))

(:derived (Flag144-)(and (RIGHTOF9-ROBOT)))

(:derived (Flag144-)(and (RIGHTOF6-ROBOT)))

(:derived (Flag145-)(and (RIGHTOF3-ROBOT)))

(:derived (Flag145-)(and (RIGHTOF5-ROBOT)))

(:derived (Flag145-)(and (RIGHTOF11-ROBOT)))

(:derived (Flag145-)(and (RIGHTOF10-ROBOT)))

(:derived (Flag145-)(and (RIGHTOF4-ROBOT)))

(:derived (Flag145-)(and (RIGHTOF8-ROBOT)))

(:derived (Flag145-)(and (RIGHTOF12-ROBOT)))

(:derived (Flag145-)(and (RIGHTOF7-ROBOT)))

(:derived (Flag145-)(and (RIGHTOF9-ROBOT)))

(:derived (Flag145-)(and (RIGHTOF6-ROBOT)))

(:derived (Flag146-)(and (RIGHTOF5-ROBOT)))

(:derived (Flag146-)(and (RIGHTOF11-ROBOT)))

(:derived (Flag146-)(and (RIGHTOF10-ROBOT)))

(:derived (Flag146-)(and (RIGHTOF4-ROBOT)))

(:derived (Flag146-)(and (RIGHTOF8-ROBOT)))

(:derived (Flag146-)(and (RIGHTOF12-ROBOT)))

(:derived (Flag146-)(and (RIGHTOF7-ROBOT)))

(:derived (Flag146-)(and (RIGHTOF9-ROBOT)))

(:derived (Flag146-)(and (RIGHTOF6-ROBOT)))

(:derived (Flag147-)(and (RIGHTOF5-ROBOT)))

(:derived (Flag147-)(and (RIGHTOF11-ROBOT)))

(:derived (Flag147-)(and (RIGHTOF10-ROBOT)))

(:derived (Flag147-)(and (RIGHTOF8-ROBOT)))

(:derived (Flag147-)(and (RIGHTOF12-ROBOT)))

(:derived (Flag147-)(and (RIGHTOF7-ROBOT)))

(:derived (Flag147-)(and (RIGHTOF9-ROBOT)))

(:derived (Flag147-)(and (RIGHTOF6-ROBOT)))

(:derived (Flag148-)(and (RIGHTOF11-ROBOT)))

(:derived (Flag148-)(and (RIGHTOF10-ROBOT)))

(:derived (Flag148-)(and (RIGHTOF8-ROBOT)))

(:derived (Flag148-)(and (RIGHTOF12-ROBOT)))

(:derived (Flag148-)(and (RIGHTOF7-ROBOT)))

(:derived (Flag148-)(and (RIGHTOF9-ROBOT)))

(:derived (Flag148-)(and (RIGHTOF6-ROBOT)))

(:derived (Flag149-)(and (RIGHTOF11-ROBOT)))

(:derived (Flag149-)(and (RIGHTOF10-ROBOT)))

(:derived (Flag149-)(and (RIGHTOF8-ROBOT)))

(:derived (Flag149-)(and (RIGHTOF7-ROBOT)))

(:derived (Flag149-)(and (RIGHTOF9-ROBOT)))

(:derived (Flag149-)(and (RIGHTOF12-ROBOT)))

(:derived (Flag150-)(and (RIGHTOF10-ROBOT)))

(:derived (Flag150-)(and (RIGHTOF9-ROBOT)))

(:derived (Flag150-)(and (RIGHTOF8-ROBOT)))

(:derived (Flag150-)(and (RIGHTOF12-ROBOT)))

(:derived (Flag150-)(and (RIGHTOF11-ROBOT)))

(:derived (Flag151-)(and (RIGHTOF10-ROBOT)))

(:derived (Flag151-)(and (RIGHTOF9-ROBOT)))

(:derived (Flag151-)(and (RIGHTOF12-ROBOT)))

(:derived (Flag151-)(and (RIGHTOF11-ROBOT)))

(:derived (Flag152-)(and (RIGHTOF10-ROBOT)))

(:derived (Flag152-)(and (RIGHTOF12-ROBOT)))

(:derived (Flag152-)(and (RIGHTOF11-ROBOT)))

(:derived (Flag153-)(and (RIGHTOF12-ROBOT)))

(:derived (Flag153-)(and (RIGHTOF11-ROBOT)))

(:derived (Flag154-)(and (LEFTOF1-ROBOT)))

(:derived (Flag154-)(and (LEFTOF2-ROBOT)))

(:derived (Flag155-)(and (LEFTOF1-ROBOT)))

(:derived (Flag155-)(and (LEFTOF3-ROBOT)))

(:derived (Flag155-)(and (LEFTOF2-ROBOT)))

(:derived (Flag156-)(and (LEFTOF1-ROBOT)))

(:derived (Flag156-)(and (LEFTOF3-ROBOT)))

(:derived (Flag156-)(and (LEFTOF4-ROBOT)))

(:derived (Flag156-)(and (LEFTOF2-ROBOT)))

(:derived (Flag157-)(and (LEFTOF5-ROBOT)))

(:derived (Flag157-)(and (LEFTOF1-ROBOT)))

(:derived (Flag157-)(and (LEFTOF3-ROBOT)))

(:derived (Flag157-)(and (LEFTOF4-ROBOT)))

(:derived (Flag157-)(and (LEFTOF2-ROBOT)))

(:derived (Flag158-)(and (LEFTOF2-ROBOT)))

(:derived (Flag158-)(and (LEFTOF1-ROBOT)))

(:derived (Flag158-)(and (LEFTOF6-ROBOT)))

(:derived (Flag158-)(and (LEFTOF5-ROBOT)))

(:derived (Flag158-)(and (LEFTOF3-ROBOT)))

(:derived (Flag158-)(and (LEFTOF4-ROBOT)))

(:derived (Flag159-)(and (LEFTOF2-ROBOT)))

(:derived (Flag159-)(and (LEFTOF7-ROBOT)))

(:derived (Flag159-)(and (LEFTOF1-ROBOT)))

(:derived (Flag159-)(and (LEFTOF6-ROBOT)))

(:derived (Flag159-)(and (LEFTOF5-ROBOT)))

(:derived (Flag159-)(and (LEFTOF3-ROBOT)))

(:derived (Flag159-)(and (LEFTOF4-ROBOT)))

(:derived (Flag160-)(and (LEFTOF2-ROBOT)))

(:derived (Flag160-)(and (LEFTOF7-ROBOT)))

(:derived (Flag160-)(and (LEFTOF1-ROBOT)))

(:derived (Flag160-)(and (LEFTOF6-ROBOT)))

(:derived (Flag160-)(and (LEFTOF5-ROBOT)))

(:derived (Flag160-)(and (LEFTOF3-ROBOT)))

(:derived (Flag160-)(and (LEFTOF4-ROBOT)))

(:derived (Flag160-)(and (LEFTOF8-ROBOT)))

(:derived (Flag161-)(and (LEFTOF9-ROBOT)))

(:derived (Flag161-)(and (LEFTOF2-ROBOT)))

(:derived (Flag161-)(and (LEFTOF7-ROBOT)))

(:derived (Flag161-)(and (LEFTOF1-ROBOT)))

(:derived (Flag161-)(and (LEFTOF6-ROBOT)))

(:derived (Flag161-)(and (LEFTOF5-ROBOT)))

(:derived (Flag161-)(and (LEFTOF3-ROBOT)))

(:derived (Flag161-)(and (LEFTOF4-ROBOT)))

(:derived (Flag161-)(and (LEFTOF8-ROBOT)))

(:derived (Flag162-)(and (LEFTOF10-ROBOT)))

(:derived (Flag162-)(and (LEFTOF9-ROBOT)))

(:derived (Flag162-)(and (LEFTOF2-ROBOT)))

(:derived (Flag162-)(and (LEFTOF7-ROBOT)))

(:derived (Flag162-)(and (LEFTOF1-ROBOT)))

(:derived (Flag162-)(and (LEFTOF6-ROBOT)))

(:derived (Flag162-)(and (LEFTOF5-ROBOT)))

(:derived (Flag162-)(and (LEFTOF3-ROBOT)))

(:derived (Flag162-)(and (LEFTOF4-ROBOT)))

(:derived (Flag162-)(and (LEFTOF8-ROBOT)))

(:derived (Flag163-)(and (LEFTOF10-ROBOT)))

(:derived (Flag163-)(and (LEFTOF11-ROBOT)))

(:derived (Flag163-)(and (LEFTOF9-ROBOT)))

(:derived (Flag163-)(and (LEFTOF2-ROBOT)))

(:derived (Flag163-)(and (LEFTOF7-ROBOT)))

(:derived (Flag163-)(and (LEFTOF1-ROBOT)))

(:derived (Flag163-)(and (LEFTOF6-ROBOT)))

(:derived (Flag163-)(and (LEFTOF5-ROBOT)))

(:derived (Flag163-)(and (LEFTOF3-ROBOT)))

(:derived (Flag163-)(and (LEFTOF4-ROBOT)))

(:derived (Flag163-)(and (LEFTOF8-ROBOT)))

(:derived (Flag164-)(and (LEFTOF10-ROBOT)))

(:derived (Flag164-)(and (LEFTOF11-ROBOT)))

(:derived (Flag164-)(and (LEFTOF9-ROBOT)))

(:derived (Flag164-)(and (LEFTOF2-ROBOT)))

(:derived (Flag164-)(and (LEFTOF7-ROBOT)))

(:derived (Flag164-)(and (LEFTOF1-ROBOT)))

(:derived (Flag164-)(and (LEFTOF6-ROBOT)))

(:derived (Flag164-)(and (LEFTOF5-ROBOT)))

(:derived (Flag164-)(and (LEFTOF3-ROBOT)))

(:derived (Flag164-)(and (LEFTOF4-ROBOT)))

(:derived (Flag164-)(and (LEFTOF8-ROBOT)))

(:derived (Flag165-)(and (LEFTOF1-ROBOT)(RIGHTOF4-ROBOT)))

(:derived (Flag166-)(and (LEFTOF1-ROBOT)(RIGHTOF12-ROBOT)))

(:derived (Flag167-)(and (LEFTOF1-ROBOT)(RIGHTOF7-ROBOT)))

(:derived (Flag168-)(and (LEFTOF1-ROBOT)(RIGHTOF9-ROBOT)))

(:derived (Flag169-)(and (LEFTOF1-ROBOT)(RIGHTOF1-ROBOT)))

(:derived (Flag170-)(and (RIGHTOF3-ROBOT)(LEFTOF1-ROBOT)))

(:derived (Flag171-)(and (RIGHTOF0-ROBOT)(LEFTOF1-ROBOT)))

(:derived (Flag172-)(and (LEFTOF1-ROBOT)(RIGHTOF5-ROBOT)))

(:derived (Flag173-)(and (LEFTOF1-ROBOT)(RIGHTOF2-ROBOT)))

(:derived (Flag174-)(and (RIGHTOF11-ROBOT)(LEFTOF1-ROBOT)))

(:derived (Flag175-)(and (RIGHTOF8-ROBOT)(LEFTOF1-ROBOT)))

(:derived (Flag176-)(and (LEFTOF1-ROBOT)(RIGHTOF6-ROBOT)))

(:derived (Flag177-)(and (RIGHTOF10-ROBOT)(LEFTOF1-ROBOT)))

(:derived (Flag179-)(and (LEFTOF2-ROBOT)(RIGHTOF9-ROBOT)))

(:derived (Flag180-)(and (LEFTOF2-ROBOT)(RIGHTOF1-ROBOT)))

(:derived (Flag181-)(and (RIGHTOF5-ROBOT)(LEFTOF2-ROBOT)))

(:derived (Flag182-)(and (LEFTOF2-ROBOT)(RIGHTOF7-ROBOT)))

(:derived (Flag183-)(and (RIGHTOF5-ROBOT)(LEFTOF1-ROBOT)))

(:derived (Flag184-)(and (RIGHTOF3-ROBOT)(LEFTOF2-ROBOT)))

(:derived (Flag185-)(and (RIGHTOF10-ROBOT)(LEFTOF2-ROBOT)))

(:derived (Flag186-)(and (LEFTOF2-ROBOT)(RIGHTOF12-ROBOT)))

(:derived (Flag187-)(and (RIGHTOF11-ROBOT)(LEFTOF2-ROBOT)))

(:derived (Flag188-)(and (RIGHTOF8-ROBOT)(LEFTOF2-ROBOT)))

(:derived (Flag189-)(and (RIGHTOF2-ROBOT)(LEFTOF2-ROBOT)))

(:derived (Flag190-)(and (LEFTOF2-ROBOT)(RIGHTOF6-ROBOT)))

(:derived (Flag191-)(and (LEFTOF2-ROBOT)(RIGHTOF4-ROBOT)))

(:derived (Flag193-)(and (LEFTOF3-ROBOT)(RIGHTOF6-ROBOT)))

(:derived (Flag194-)(and (LEFTOF3-ROBOT)(RIGHTOF4-ROBOT)))

(:derived (Flag195-)(and (LEFTOF3-ROBOT)(RIGHTOF7-ROBOT)))

(:derived (Flag196-)(and (LEFTOF3-ROBOT)(RIGHTOF12-ROBOT)))

(:derived (Flag197-)(and (LEFTOF3-ROBOT)(RIGHTOF10-ROBOT)))

(:derived (Flag198-)(and (LEFTOF3-ROBOT)(RIGHTOF8-ROBOT)))

(:derived (Flag199-)(and (LEFTOF3-ROBOT)(RIGHTOF5-ROBOT)))

(:derived (Flag200-)(and (RIGHTOF3-ROBOT)(LEFTOF3-ROBOT)))

(:derived (Flag201-)(and (LEFTOF3-ROBOT)(RIGHTOF11-ROBOT)))

(:derived (Flag202-)(and (LEFTOF3-ROBOT)(RIGHTOF9-ROBOT)))

(:derived (Flag203-)(and (LEFTOF3-ROBOT)(RIGHTOF2-ROBOT)))

(:derived (Flag205-)(and (LEFTOF4-ROBOT)(RIGHTOF4-ROBOT)))

(:derived (Flag206-)(and (LEFTOF4-ROBOT)(RIGHTOF7-ROBOT)))

(:derived (Flag207-)(and (LEFTOF3-ROBOT)(RIGHTOF3-ROBOT)))

(:derived (Flag208-)(and (RIGHTOF8-ROBOT)(LEFTOF4-ROBOT)))

(:derived (Flag209-)(and (LEFTOF4-ROBOT)(RIGHTOF12-ROBOT)))

(:derived (Flag210-)(and (RIGHTOF10-ROBOT)(LEFTOF4-ROBOT)))

(:derived (Flag211-)(and (LEFTOF4-ROBOT)(RIGHTOF9-ROBOT)))

(:derived (Flag212-)(and (LEFTOF4-ROBOT)(RIGHTOF6-ROBOT)))

(:derived (Flag213-)(and (LEFTOF4-ROBOT)(RIGHTOF5-ROBOT)))

(:derived (Flag214-)(and (RIGHTOF3-ROBOT)(LEFTOF4-ROBOT)))

(:derived (Flag215-)(and (LEFTOF4-ROBOT)(RIGHTOF11-ROBOT)))

(:derived (Flag217-)(and (RIGHTOF9-ROBOT)(LEFTOF5-ROBOT)))

(:derived (Flag218-)(and (RIGHTOF7-ROBOT)(LEFTOF5-ROBOT)))

(:derived (Flag219-)(and (RIGHTOF11-ROBOT)(LEFTOF5-ROBOT)))

(:derived (Flag220-)(and (LEFTOF4-ROBOT)(RIGHTOF10-ROBOT)))

(:derived (Flag221-)(and (RIGHTOF4-ROBOT)(LEFTOF5-ROBOT)))

(:derived (Flag222-)(and (LEFTOF4-ROBOT)(RIGHTOF8-ROBOT)))

(:derived (Flag223-)(and (LEFTOF5-ROBOT)(RIGHTOF6-ROBOT)))

(:derived (Flag224-)(and (RIGHTOF10-ROBOT)(LEFTOF5-ROBOT)))

(:derived (Flag225-)(and (LEFTOF5-ROBOT)(RIGHTOF12-ROBOT)))

(:derived (Flag226-)(and (RIGHTOF8-ROBOT)(LEFTOF5-ROBOT)))

(:derived (Flag227-)(and (RIGHTOF5-ROBOT)(LEFTOF5-ROBOT)))

(:derived (Flag229-)(and (RIGHTOF12-ROBOT)(LEFTOF6-ROBOT)))

(:derived (Flag230-)(and (RIGHTOF8-ROBOT)(LEFTOF6-ROBOT)))

(:derived (Flag231-)(and (RIGHTOF11-ROBOT)(LEFTOF6-ROBOT)))

(:derived (Flag232-)(and (RIGHTOF5-ROBOT)(LEFTOF6-ROBOT)))

(:derived (Flag233-)(and (RIGHTOF7-ROBOT)(LEFTOF6-ROBOT)))

(:derived (Flag234-)(and (RIGHTOF10-ROBOT)(LEFTOF6-ROBOT)))

(:derived (Flag235-)(and (RIGHTOF6-ROBOT)(LEFTOF6-ROBOT)))

(:derived (Flag236-)(and (RIGHTOF9-ROBOT)(LEFTOF6-ROBOT)))

(:derived (Flag238-)(and (RIGHTOF11-ROBOT)(LEFTOF7-ROBOT)))

(:derived (Flag239-)(and (LEFTOF7-ROBOT)(RIGHTOF7-ROBOT)))

(:derived (Flag240-)(and (RIGHTOF8-ROBOT)(LEFTOF7-ROBOT)))

(:derived (Flag241-)(and (LEFTOF6-ROBOT)(RIGHTOF6-ROBOT)))

(:derived (Flag242-)(and (LEFTOF7-ROBOT)(RIGHTOF12-ROBOT)))

(:derived (Flag243-)(and (LEFTOF7-ROBOT)(RIGHTOF9-ROBOT)))

(:derived (Flag244-)(and (LEFTOF7-ROBOT)(RIGHTOF6-ROBOT)))

(:derived (Flag245-)(and (LEFTOF6-ROBOT)(RIGHTOF12-ROBOT)))

(:derived (Flag246-)(and (RIGHTOF10-ROBOT)(LEFTOF7-ROBOT)))

(:derived (Flag248-)(and (RIGHTOF7-ROBOT)(LEFTOF8-ROBOT)))

(:derived (Flag249-)(and (LEFTOF8-ROBOT)(RIGHTOF9-ROBOT)))

(:derived (Flag250-)(and (RIGHTOF10-ROBOT)(LEFTOF8-ROBOT)))

(:derived (Flag251-)(and (RIGHTOF8-ROBOT)(LEFTOF8-ROBOT)))

(:derived (Flag252-)(and (RIGHTOF7-ROBOT)(LEFTOF7-ROBOT)))

(:derived (Flag253-)(and (LEFTOF8-ROBOT)(RIGHTOF12-ROBOT)))

(:derived (Flag254-)(and (RIGHTOF11-ROBOT)(LEFTOF8-ROBOT)))

(:derived (Flag256-)(and (RIGHTOF8-ROBOT)(LEFTOF9-ROBOT)))

(:derived (Flag257-)(and (LEFTOF9-ROBOT)(RIGHTOF9-ROBOT)))

(:derived (Flag258-)(and (RIGHTOF10-ROBOT)(LEFTOF9-ROBOT)))

(:derived (Flag259-)(and (LEFTOF9-ROBOT)(RIGHTOF12-ROBOT)))

(:derived (Flag260-)(and (RIGHTOF11-ROBOT)(LEFTOF9-ROBOT)))

(:derived (Flag262-)(and (LEFTOF10-ROBOT)(RIGHTOF12-ROBOT)))

(:derived (Flag263-)(and (RIGHTOF11-ROBOT)(LEFTOF10-ROBOT)))

(:derived (Flag264-)(and (RIGHTOF10-ROBOT)(LEFTOF10-ROBOT)))

(:derived (Flag265-)(and (LEFTOF10-ROBOT)(RIGHTOF9-ROBOT)))

(:derived (Flag267-)(and (RIGHTOF11-ROBOT)(LEFTOF11-ROBOT)))

(:derived (Flag268-)(and (RIGHTOF10-ROBOT)(LEFTOF11-ROBOT)))

(:derived (Flag269-)(and (LEFTOF11-ROBOT)(RIGHTOF12-ROBOT)))

(:derived (Flag271-)(and (RIGHTOF12-ROBOT)(LEFTOF12-ROBOT)))

(:derived (Flag272-)(and (RIGHTOF11-ROBOT)(LEFTOF12-ROBOT)))

(:derived (Flag274-)(and (LEFTOF10-ROBOT)))

(:derived (Flag274-)(and (LEFTOF11-ROBOT)))

(:derived (Flag274-)(and (LEFTOF9-ROBOT)))

(:derived (Flag274-)(and (LEFTOF2-ROBOT)))

(:derived (Flag274-)(and (LEFTOF13-ROBOT)))

(:derived (Flag274-)(and (LEFTOF1-ROBOT)))

(:derived (Flag274-)(and (LEFTOF6-ROBOT)))

(:derived (Flag274-)(and (LEFTOF5-ROBOT)))

(:derived (Flag274-)(and (LEFTOF3-ROBOT)))

(:derived (Flag274-)(and (LEFTOF4-ROBOT)))

(:derived (Flag274-)(and (LEFTOF7-ROBOT)))

(:derived (Flag274-)(and (LEFTOF8-ROBOT)))

(:derived (Flag275-)(and (LEFTOF13-ROBOT)(RIGHTOF12-ROBOT)))

(:derived (Flag276-)(and (LEFTOF12-ROBOT)(RIGHTOF12-ROBOT)))

(:action MOVELEFT-ROBOT
:parameters ()
:precondition
(and
(Flag3-)
)
:effect
(and
(when
(and
(Flag277-)
)
(and
(COLUMN11-ROBOT)
(not (NOT-COLUMN11-ROBOT))
)
)
(when
(and
(Flag273-)
)
(and
(COLUMN10-ROBOT)
(not (NOT-COLUMN10-ROBOT))
)
)
(when
(and
(Flag270-)
)
(and
(COLUMN9-ROBOT)
(not (NOT-COLUMN9-ROBOT))
)
)
(when
(and
(Flag266-)
)
(and
(COLUMN8-ROBOT)
(not (NOT-COLUMN8-ROBOT))
)
)
(when
(and
(Flag261-)
)
(and
(COLUMN7-ROBOT)
(not (NOT-COLUMN7-ROBOT))
)
)
(when
(and
(Flag255-)
)
(and
(COLUMN6-ROBOT)
(not (NOT-COLUMN6-ROBOT))
)
)
(when
(and
(Flag247-)
)
(and
(COLUMN5-ROBOT)
(not (NOT-COLUMN5-ROBOT))
)
)
(when
(and
(Flag237-)
)
(and
(COLUMN4-ROBOT)
(not (NOT-COLUMN4-ROBOT))
)
)
(when
(and
(Flag228-)
)
(and
(COLUMN3-ROBOT)
(not (NOT-COLUMN3-ROBOT))
)
)
(when
(and
(Flag216-)
)
(and
(COLUMN2-ROBOT)
(not (NOT-COLUMN2-ROBOT))
)
)
(when
(and
(Flag204-)
)
(and
(COLUMN1-ROBOT)
(not (NOT-COLUMN1-ROBOT))
)
)
(when
(and
(Flag192-)
)
(and
(COLUMN0-ROBOT)
(not (NOT-COLUMN0-ROBOT))
)
)
(when
(and
(COLUMN12-ROBOT)
)
(and
(COLUMN11-ROBOT)
(NOT-COLUMN12-ROBOT)
(not (NOT-COLUMN11-ROBOT))
(not (COLUMN12-ROBOT))
)
)
(when
(and
(COLUMN11-ROBOT)
)
(and
(COLUMN10-ROBOT)
(NOT-COLUMN11-ROBOT)
(not (NOT-COLUMN10-ROBOT))
(not (COLUMN11-ROBOT))
)
)
(when
(and
(COLUMN10-ROBOT)
)
(and
(COLUMN9-ROBOT)
(NOT-COLUMN10-ROBOT)
(not (NOT-COLUMN9-ROBOT))
(not (COLUMN10-ROBOT))
)
)
(when
(and
(COLUMN9-ROBOT)
)
(and
(COLUMN8-ROBOT)
(NOT-COLUMN9-ROBOT)
(not (NOT-COLUMN8-ROBOT))
(not (COLUMN9-ROBOT))
)
)
(when
(and
(COLUMN8-ROBOT)
)
(and
(COLUMN7-ROBOT)
(NOT-COLUMN8-ROBOT)
(not (NOT-COLUMN7-ROBOT))
(not (COLUMN8-ROBOT))
)
)
(when
(and
(COLUMN7-ROBOT)
)
(and
(COLUMN6-ROBOT)
(NOT-COLUMN7-ROBOT)
(not (NOT-COLUMN6-ROBOT))
(not (COLUMN7-ROBOT))
)
)
(when
(and
(COLUMN6-ROBOT)
)
(and
(COLUMN5-ROBOT)
(NOT-COLUMN6-ROBOT)
(not (NOT-COLUMN5-ROBOT))
(not (COLUMN6-ROBOT))
)
)
(when
(and
(COLUMN5-ROBOT)
)
(and
(COLUMN4-ROBOT)
(NOT-COLUMN5-ROBOT)
(not (NOT-COLUMN4-ROBOT))
(not (COLUMN5-ROBOT))
)
)
(when
(and
(COLUMN4-ROBOT)
)
(and
(COLUMN3-ROBOT)
(NOT-COLUMN4-ROBOT)
(not (NOT-COLUMN3-ROBOT))
(not (COLUMN4-ROBOT))
)
)
(when
(and
(COLUMN3-ROBOT)
)
(and
(COLUMN2-ROBOT)
(NOT-COLUMN3-ROBOT)
(not (NOT-COLUMN2-ROBOT))
(not (COLUMN3-ROBOT))
)
)
(when
(and
(COLUMN2-ROBOT)
)
(and
(COLUMN1-ROBOT)
(NOT-COLUMN2-ROBOT)
(not (NOT-COLUMN1-ROBOT))
(not (COLUMN2-ROBOT))
)
)
(when
(and
(COLUMN1-ROBOT)
)
(and
(COLUMN0-ROBOT)
(NOT-COLUMN1-ROBOT)
(not (NOT-COLUMN0-ROBOT))
(not (COLUMN1-ROBOT))
)
)
(when
(and
(RIGHTOF12-ROBOT)
)
(and
(RIGHTOF11-ROBOT)
(NOT-RIGHTOF12-ROBOT)
(RIGHTOF10-ROBOT)
(NOT-RIGHTOF11-ROBOT)
(RIGHTOF9-ROBOT)
(NOT-RIGHTOF10-ROBOT)
(RIGHTOF8-ROBOT)
(NOT-RIGHTOF9-ROBOT)
(RIGHTOF7-ROBOT)
(NOT-RIGHTOF8-ROBOT)
(RIGHTOF6-ROBOT)
(NOT-RIGHTOF7-ROBOT)
(RIGHTOF5-ROBOT)
(NOT-RIGHTOF6-ROBOT)
(RIGHTOF4-ROBOT)
(NOT-RIGHTOF5-ROBOT)
(RIGHTOF3-ROBOT)
(NOT-RIGHTOF4-ROBOT)
(RIGHTOF2-ROBOT)
(NOT-RIGHTOF3-ROBOT)
(RIGHTOF1-ROBOT)
(NOT-RIGHTOF2-ROBOT)
(RIGHTOF0-ROBOT)
(NOT-RIGHTOF1-ROBOT)
(not (RIGHTOF12-ROBOT))
(not (NOT-RIGHTOF0-ROBOT))
)
)
(when
(and
(RIGHTOF11-ROBOT)
)
(and
(RIGHTOF10-ROBOT)
(NOT-RIGHTOF11-ROBOT)
(RIGHTOF9-ROBOT)
(NOT-RIGHTOF10-ROBOT)
(RIGHTOF8-ROBOT)
(NOT-RIGHTOF9-ROBOT)
(RIGHTOF7-ROBOT)
(NOT-RIGHTOF8-ROBOT)
(RIGHTOF6-ROBOT)
(NOT-RIGHTOF7-ROBOT)
(RIGHTOF5-ROBOT)
(NOT-RIGHTOF6-ROBOT)
(RIGHTOF4-ROBOT)
(NOT-RIGHTOF5-ROBOT)
(RIGHTOF3-ROBOT)
(NOT-RIGHTOF4-ROBOT)
(RIGHTOF2-ROBOT)
(NOT-RIGHTOF3-ROBOT)
(RIGHTOF1-ROBOT)
(NOT-RIGHTOF2-ROBOT)
(RIGHTOF0-ROBOT)
(NOT-RIGHTOF1-ROBOT)
(not (RIGHTOF11-ROBOT))
(not (NOT-RIGHTOF0-ROBOT))
)
)
(when
(and
(RIGHTOF10-ROBOT)
)
(and
(RIGHTOF9-ROBOT)
(NOT-RIGHTOF10-ROBOT)
(RIGHTOF8-ROBOT)
(NOT-RIGHTOF9-ROBOT)
(RIGHTOF7-ROBOT)
(NOT-RIGHTOF8-ROBOT)
(RIGHTOF6-ROBOT)
(NOT-RIGHTOF7-ROBOT)
(RIGHTOF5-ROBOT)
(NOT-RIGHTOF6-ROBOT)
(RIGHTOF4-ROBOT)
(NOT-RIGHTOF5-ROBOT)
(RIGHTOF3-ROBOT)
(NOT-RIGHTOF4-ROBOT)
(RIGHTOF2-ROBOT)
(NOT-RIGHTOF3-ROBOT)
(RIGHTOF1-ROBOT)
(NOT-RIGHTOF2-ROBOT)
(RIGHTOF0-ROBOT)
(NOT-RIGHTOF1-ROBOT)
(not (RIGHTOF10-ROBOT))
(not (NOT-RIGHTOF0-ROBOT))
)
)
(when
(and
(RIGHTOF9-ROBOT)
)
(and
(RIGHTOF8-ROBOT)
(NOT-RIGHTOF9-ROBOT)
(RIGHTOF7-ROBOT)
(NOT-RIGHTOF8-ROBOT)
(RIGHTOF6-ROBOT)
(NOT-RIGHTOF7-ROBOT)
(RIGHTOF5-ROBOT)
(NOT-RIGHTOF6-ROBOT)
(RIGHTOF4-ROBOT)
(NOT-RIGHTOF5-ROBOT)
(RIGHTOF3-ROBOT)
(NOT-RIGHTOF4-ROBOT)
(RIGHTOF2-ROBOT)
(NOT-RIGHTOF3-ROBOT)
(RIGHTOF1-ROBOT)
(NOT-RIGHTOF2-ROBOT)
(RIGHTOF0-ROBOT)
(NOT-RIGHTOF1-ROBOT)
(not (RIGHTOF9-ROBOT))
(not (NOT-RIGHTOF0-ROBOT))
)
)
(when
(and
(RIGHTOF8-ROBOT)
)
(and
(RIGHTOF7-ROBOT)
(NOT-RIGHTOF8-ROBOT)
(RIGHTOF6-ROBOT)
(NOT-RIGHTOF7-ROBOT)
(RIGHTOF5-ROBOT)
(NOT-RIGHTOF6-ROBOT)
(RIGHTOF4-ROBOT)
(NOT-RIGHTOF5-ROBOT)
(RIGHTOF3-ROBOT)
(NOT-RIGHTOF4-ROBOT)
(RIGHTOF2-ROBOT)
(NOT-RIGHTOF3-ROBOT)
(RIGHTOF1-ROBOT)
(NOT-RIGHTOF2-ROBOT)
(RIGHTOF0-ROBOT)
(NOT-RIGHTOF1-ROBOT)
(not (RIGHTOF8-ROBOT))
(not (NOT-RIGHTOF0-ROBOT))
)
)
(when
(and
(RIGHTOF7-ROBOT)
)
(and
(RIGHTOF6-ROBOT)
(NOT-RIGHTOF7-ROBOT)
(RIGHTOF5-ROBOT)
(NOT-RIGHTOF6-ROBOT)
(RIGHTOF4-ROBOT)
(NOT-RIGHTOF5-ROBOT)
(RIGHTOF3-ROBOT)
(NOT-RIGHTOF4-ROBOT)
(RIGHTOF2-ROBOT)
(NOT-RIGHTOF3-ROBOT)
(RIGHTOF1-ROBOT)
(NOT-RIGHTOF2-ROBOT)
(RIGHTOF0-ROBOT)
(NOT-RIGHTOF1-ROBOT)
(not (RIGHTOF7-ROBOT))
(not (NOT-RIGHTOF0-ROBOT))
)
)
(when
(and
(RIGHTOF6-ROBOT)
)
(and
(RIGHTOF5-ROBOT)
(NOT-RIGHTOF6-ROBOT)
(RIGHTOF4-ROBOT)
(NOT-RIGHTOF5-ROBOT)
(RIGHTOF3-ROBOT)
(NOT-RIGHTOF4-ROBOT)
(RIGHTOF2-ROBOT)
(NOT-RIGHTOF3-ROBOT)
(RIGHTOF1-ROBOT)
(NOT-RIGHTOF2-ROBOT)
(RIGHTOF0-ROBOT)
(NOT-RIGHTOF1-ROBOT)
(not (RIGHTOF6-ROBOT))
(not (NOT-RIGHTOF0-ROBOT))
)
)
(when
(and
(RIGHTOF5-ROBOT)
)
(and
(RIGHTOF4-ROBOT)
(NOT-RIGHTOF5-ROBOT)
(RIGHTOF3-ROBOT)
(NOT-RIGHTOF4-ROBOT)
(RIGHTOF2-ROBOT)
(NOT-RIGHTOF3-ROBOT)
(RIGHTOF1-ROBOT)
(NOT-RIGHTOF2-ROBOT)
(RIGHTOF0-ROBOT)
(NOT-RIGHTOF1-ROBOT)
(not (RIGHTOF5-ROBOT))
(not (NOT-RIGHTOF0-ROBOT))
)
)
(when
(and
(RIGHTOF4-ROBOT)
)
(and
(RIGHTOF3-ROBOT)
(NOT-RIGHTOF4-ROBOT)
(RIGHTOF2-ROBOT)
(NOT-RIGHTOF3-ROBOT)
(RIGHTOF1-ROBOT)
(NOT-RIGHTOF2-ROBOT)
(RIGHTOF0-ROBOT)
(NOT-RIGHTOF1-ROBOT)
(not (RIGHTOF4-ROBOT))
(not (NOT-RIGHTOF0-ROBOT))
)
)
(when
(and
(RIGHTOF3-ROBOT)
)
(and
(RIGHTOF2-ROBOT)
(NOT-RIGHTOF3-ROBOT)
(RIGHTOF1-ROBOT)
(NOT-RIGHTOF2-ROBOT)
(RIGHTOF0-ROBOT)
(NOT-RIGHTOF1-ROBOT)
(not (RIGHTOF3-ROBOT))
(not (NOT-RIGHTOF0-ROBOT))
)
)
(when
(and
(RIGHTOF2-ROBOT)
)
(and
(RIGHTOF1-ROBOT)
(NOT-RIGHTOF2-ROBOT)
(RIGHTOF0-ROBOT)
(NOT-RIGHTOF1-ROBOT)
(not (RIGHTOF2-ROBOT))
(not (NOT-RIGHTOF0-ROBOT))
)
)
(when
(and
(RIGHTOF1-ROBOT)
)
(and
(RIGHTOF0-ROBOT)
(NOT-RIGHTOF1-ROBOT)
(not (NOT-RIGHTOF0-ROBOT))
(not (RIGHTOF1-ROBOT))
)
)
(when
(and
(Flag274-)
)
(and
(LEFTOF12-ROBOT)
(not (NOT-LEFTOF12-ROBOT))
)
)
(when
(and
(LEFTOF8-ROBOT)
)
(and
(LEFTOF11-ROBOT)
(LEFTOF10-ROBOT)
(LEFTOF9-ROBOT)
(LEFTOF7-ROBOT)
(not (NOT-LEFTOF11-ROBOT))
(not (NOT-LEFTOF10-ROBOT))
(not (NOT-LEFTOF9-ROBOT))
(not (NOT-LEFTOF8-ROBOT))
(not (NOT-LEFTOF7-ROBOT))
)
)
(when
(and
(LEFTOF4-ROBOT)
)
(and
(LEFTOF11-ROBOT)
(LEFTOF10-ROBOT)
(LEFTOF9-ROBOT)
(LEFTOF8-ROBOT)
(LEFTOF7-ROBOT)
(LEFTOF6-ROBOT)
(LEFTOF5-ROBOT)
(LEFTOF3-ROBOT)
(not (NOT-LEFTOF11-ROBOT))
(not (NOT-LEFTOF10-ROBOT))
(not (NOT-LEFTOF9-ROBOT))
(not (NOT-LEFTOF8-ROBOT))
(not (NOT-LEFTOF7-ROBOT))
(not (NOT-LEFTOF6-ROBOT))
(not (NOT-LEFTOF5-ROBOT))
(not (NOT-LEFTOF4-ROBOT))
(not (NOT-LEFTOF3-ROBOT))
)
)
(when
(and
(LEFTOF3-ROBOT)
)
(and
(LEFTOF11-ROBOT)
(LEFTOF10-ROBOT)
(LEFTOF9-ROBOT)
(LEFTOF8-ROBOT)
(LEFTOF7-ROBOT)
(LEFTOF6-ROBOT)
(LEFTOF5-ROBOT)
(LEFTOF4-ROBOT)
(LEFTOF2-ROBOT)
(not (NOT-LEFTOF11-ROBOT))
(not (NOT-LEFTOF10-ROBOT))
(not (NOT-LEFTOF9-ROBOT))
(not (NOT-LEFTOF8-ROBOT))
(not (NOT-LEFTOF7-ROBOT))
(not (NOT-LEFTOF6-ROBOT))
(not (NOT-LEFTOF5-ROBOT))
(not (NOT-LEFTOF4-ROBOT))
(not (NOT-LEFTOF3-ROBOT))
(not (NOT-LEFTOF2-ROBOT))
)
)
(when
(and
(LEFTOF5-ROBOT)
)
(and
(LEFTOF11-ROBOT)
(LEFTOF10-ROBOT)
(LEFTOF9-ROBOT)
(LEFTOF8-ROBOT)
(LEFTOF7-ROBOT)
(LEFTOF6-ROBOT)
(LEFTOF4-ROBOT)
(not (NOT-LEFTOF11-ROBOT))
(not (NOT-LEFTOF10-ROBOT))
(not (NOT-LEFTOF9-ROBOT))
(not (NOT-LEFTOF8-ROBOT))
(not (NOT-LEFTOF7-ROBOT))
(not (NOT-LEFTOF6-ROBOT))
(not (NOT-LEFTOF5-ROBOT))
(not (NOT-LEFTOF4-ROBOT))
)
)
(when
(and
(LEFTOF6-ROBOT)
)
(and
(LEFTOF11-ROBOT)
(LEFTOF10-ROBOT)
(LEFTOF9-ROBOT)
(LEFTOF8-ROBOT)
(LEFTOF7-ROBOT)
(LEFTOF5-ROBOT)
(not (NOT-LEFTOF11-ROBOT))
(not (NOT-LEFTOF10-ROBOT))
(not (NOT-LEFTOF9-ROBOT))
(not (NOT-LEFTOF8-ROBOT))
(not (NOT-LEFTOF7-ROBOT))
(not (NOT-LEFTOF6-ROBOT))
(not (NOT-LEFTOF5-ROBOT))
)
)
(when
(and
(LEFTOF12-ROBOT)
)
(and
(LEFTOF11-ROBOT)
(not (NOT-LEFTOF11-ROBOT))
)
)
(when
(and
(LEFTOF1-ROBOT)
)
(and
(LEFTOF11-ROBOT)
(LEFTOF10-ROBOT)
(LEFTOF9-ROBOT)
(LEFTOF8-ROBOT)
(LEFTOF7-ROBOT)
(LEFTOF6-ROBOT)
(LEFTOF5-ROBOT)
(LEFTOF4-ROBOT)
(LEFTOF3-ROBOT)
(LEFTOF2-ROBOT)
(not (NOT-LEFTOF11-ROBOT))
(not (NOT-LEFTOF10-ROBOT))
(not (NOT-LEFTOF9-ROBOT))
(not (NOT-LEFTOF8-ROBOT))
(not (NOT-LEFTOF7-ROBOT))
(not (NOT-LEFTOF6-ROBOT))
(not (NOT-LEFTOF5-ROBOT))
(not (NOT-LEFTOF4-ROBOT))
(not (NOT-LEFTOF3-ROBOT))
(not (NOT-LEFTOF2-ROBOT))
(not (NOT-LEFTOF1-ROBOT))
)
)
(when
(and
(LEFTOF7-ROBOT)
)
(and
(LEFTOF11-ROBOT)
(LEFTOF10-ROBOT)
(LEFTOF9-ROBOT)
(LEFTOF8-ROBOT)
(LEFTOF6-ROBOT)
(not (NOT-LEFTOF11-ROBOT))
(not (NOT-LEFTOF10-ROBOT))
(not (NOT-LEFTOF9-ROBOT))
(not (NOT-LEFTOF8-ROBOT))
(not (NOT-LEFTOF7-ROBOT))
(not (NOT-LEFTOF6-ROBOT))
)
)
(when
(and
(LEFTOF2-ROBOT)
)
(and
(LEFTOF11-ROBOT)
(LEFTOF10-ROBOT)
(LEFTOF9-ROBOT)
(LEFTOF8-ROBOT)
(LEFTOF7-ROBOT)
(LEFTOF6-ROBOT)
(LEFTOF5-ROBOT)
(LEFTOF4-ROBOT)
(LEFTOF3-ROBOT)
(LEFTOF1-ROBOT)
(not (NOT-LEFTOF11-ROBOT))
(not (NOT-LEFTOF10-ROBOT))
(not (NOT-LEFTOF9-ROBOT))
(not (NOT-LEFTOF8-ROBOT))
(not (NOT-LEFTOF7-ROBOT))
(not (NOT-LEFTOF6-ROBOT))
(not (NOT-LEFTOF5-ROBOT))
(not (NOT-LEFTOF4-ROBOT))
(not (NOT-LEFTOF3-ROBOT))
(not (NOT-LEFTOF2-ROBOT))
(not (NOT-LEFTOF1-ROBOT))
)
)
(when
(and
(LEFTOF9-ROBOT)
)
(and
(LEFTOF11-ROBOT)
(LEFTOF10-ROBOT)
(LEFTOF8-ROBOT)
(not (NOT-LEFTOF11-ROBOT))
(not (NOT-LEFTOF10-ROBOT))
(not (NOT-LEFTOF9-ROBOT))
(not (NOT-LEFTOF8-ROBOT))
)
)
(when
(and
(LEFTOF11-ROBOT)
)
(and
(LEFTOF10-ROBOT)
(not (NOT-LEFTOF11-ROBOT))
(not (NOT-LEFTOF10-ROBOT))
)
)
(when
(and
(LEFTOF10-ROBOT)
)
(and
(LEFTOF11-ROBOT)
(LEFTOF9-ROBOT)
(not (NOT-LEFTOF11-ROBOT))
(not (NOT-LEFTOF10-ROBOT))
(not (NOT-LEFTOF9-ROBOT))
)
)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action MOVERIGHT-ROBOT
:parameters ()
:precondition
(and
(Flag3-)
)
:effect
(and
(when
(and
(Flag273-)
)
(and
(COLUMN12-ROBOT)
(not (NOT-COLUMN12-ROBOT))
)
)
(when
(and
(Flag270-)
)
(and
(COLUMN11-ROBOT)
(not (NOT-COLUMN11-ROBOT))
)
)
(when
(and
(Flag266-)
)
(and
(COLUMN10-ROBOT)
(not (NOT-COLUMN10-ROBOT))
)
)
(when
(and
(Flag261-)
)
(and
(COLUMN9-ROBOT)
(not (NOT-COLUMN9-ROBOT))
)
)
(when
(and
(Flag255-)
)
(and
(COLUMN8-ROBOT)
(not (NOT-COLUMN8-ROBOT))
)
)
(when
(and
(Flag247-)
)
(and
(COLUMN7-ROBOT)
(not (NOT-COLUMN7-ROBOT))
)
)
(when
(and
(Flag237-)
)
(and
(COLUMN6-ROBOT)
(not (NOT-COLUMN6-ROBOT))
)
)
(when
(and
(Flag228-)
)
(and
(COLUMN5-ROBOT)
(not (NOT-COLUMN5-ROBOT))
)
)
(when
(and
(Flag216-)
)
(and
(COLUMN4-ROBOT)
(not (NOT-COLUMN4-ROBOT))
)
)
(when
(and
(Flag204-)
)
(and
(COLUMN3-ROBOT)
(not (NOT-COLUMN3-ROBOT))
)
)
(when
(and
(Flag192-)
)
(and
(COLUMN2-ROBOT)
(not (NOT-COLUMN2-ROBOT))
)
)
(when
(and
(Flag178-)
)
(and
(COLUMN1-ROBOT)
(not (NOT-COLUMN1-ROBOT))
)
)
(when
(and
(COLUMN11-ROBOT)
)
(and
(COLUMN12-ROBOT)
(NOT-COLUMN11-ROBOT)
(not (NOT-COLUMN12-ROBOT))
(not (COLUMN11-ROBOT))
)
)
(when
(and
(COLUMN10-ROBOT)
)
(and
(COLUMN11-ROBOT)
(NOT-COLUMN10-ROBOT)
(not (NOT-COLUMN11-ROBOT))
(not (COLUMN10-ROBOT))
)
)
(when
(and
(COLUMN9-ROBOT)
)
(and
(COLUMN10-ROBOT)
(NOT-COLUMN9-ROBOT)
(not (NOT-COLUMN10-ROBOT))
(not (COLUMN9-ROBOT))
)
)
(when
(and
(COLUMN8-ROBOT)
)
(and
(COLUMN9-ROBOT)
(NOT-COLUMN8-ROBOT)
(not (NOT-COLUMN9-ROBOT))
(not (COLUMN8-ROBOT))
)
)
(when
(and
(COLUMN7-ROBOT)
)
(and
(COLUMN8-ROBOT)
(NOT-COLUMN7-ROBOT)
(not (NOT-COLUMN8-ROBOT))
(not (COLUMN7-ROBOT))
)
)
(when
(and
(COLUMN6-ROBOT)
)
(and
(COLUMN7-ROBOT)
(NOT-COLUMN6-ROBOT)
(not (NOT-COLUMN7-ROBOT))
(not (COLUMN6-ROBOT))
)
)
(when
(and
(COLUMN5-ROBOT)
)
(and
(COLUMN6-ROBOT)
(NOT-COLUMN5-ROBOT)
(not (NOT-COLUMN6-ROBOT))
(not (COLUMN5-ROBOT))
)
)
(when
(and
(COLUMN4-ROBOT)
)
(and
(COLUMN5-ROBOT)
(NOT-COLUMN4-ROBOT)
(not (NOT-COLUMN5-ROBOT))
(not (COLUMN4-ROBOT))
)
)
(when
(and
(COLUMN3-ROBOT)
)
(and
(COLUMN4-ROBOT)
(NOT-COLUMN3-ROBOT)
(not (NOT-COLUMN4-ROBOT))
(not (COLUMN3-ROBOT))
)
)
(when
(and
(COLUMN2-ROBOT)
)
(and
(COLUMN3-ROBOT)
(NOT-COLUMN2-ROBOT)
(not (NOT-COLUMN3-ROBOT))
(not (COLUMN2-ROBOT))
)
)
(when
(and
(COLUMN1-ROBOT)
)
(and
(COLUMN2-ROBOT)
(NOT-COLUMN1-ROBOT)
(not (NOT-COLUMN2-ROBOT))
(not (COLUMN1-ROBOT))
)
)
(when
(and
(COLUMN0-ROBOT)
)
(and
(COLUMN1-ROBOT)
(NOT-COLUMN0-ROBOT)
(not (NOT-COLUMN1-ROBOT))
(not (COLUMN0-ROBOT))
)
)
(when
(and
(Flag164-)
)
(and
(LEFTOF13-ROBOT)
(NOT-LEFTOF12-ROBOT)
(not (NOT-LEFTOF13-ROBOT))
(not (LEFTOF12-ROBOT))
)
)
(when
(and
(Flag163-)
)
(and
(LEFTOF12-ROBOT)
(NOT-LEFTOF11-ROBOT)
(not (NOT-LEFTOF12-ROBOT))
(not (LEFTOF11-ROBOT))
)
)
(when
(and
(Flag162-)
)
(and
(LEFTOF11-ROBOT)
(NOT-LEFTOF10-ROBOT)
(not (NOT-LEFTOF11-ROBOT))
(not (LEFTOF10-ROBOT))
)
)
(when
(and
(Flag161-)
)
(and
(LEFTOF10-ROBOT)
(NOT-LEFTOF9-ROBOT)
(not (NOT-LEFTOF10-ROBOT))
(not (LEFTOF9-ROBOT))
)
)
(when
(and
(Flag160-)
)
(and
(LEFTOF9-ROBOT)
(NOT-LEFTOF8-ROBOT)
(not (NOT-LEFTOF9-ROBOT))
(not (LEFTOF8-ROBOT))
)
)
(when
(and
(Flag159-)
)
(and
(LEFTOF8-ROBOT)
(NOT-LEFTOF7-ROBOT)
(not (NOT-LEFTOF8-ROBOT))
(not (LEFTOF7-ROBOT))
)
)
(when
(and
(Flag158-)
)
(and
(LEFTOF7-ROBOT)
(NOT-LEFTOF6-ROBOT)
(not (NOT-LEFTOF7-ROBOT))
(not (LEFTOF6-ROBOT))
)
)
(when
(and
(Flag157-)
)
(and
(LEFTOF6-ROBOT)
(NOT-LEFTOF5-ROBOT)
(not (NOT-LEFTOF6-ROBOT))
(not (LEFTOF5-ROBOT))
)
)
(when
(and
(Flag156-)
)
(and
(LEFTOF5-ROBOT)
(NOT-LEFTOF4-ROBOT)
(not (NOT-LEFTOF5-ROBOT))
(not (LEFTOF4-ROBOT))
)
)
(when
(and
(Flag155-)
)
(and
(LEFTOF4-ROBOT)
(NOT-LEFTOF3-ROBOT)
(not (NOT-LEFTOF4-ROBOT))
(not (LEFTOF3-ROBOT))
)
)
(when
(and
(Flag154-)
)
(and
(LEFTOF3-ROBOT)
(NOT-LEFTOF2-ROBOT)
(not (NOT-LEFTOF3-ROBOT))
(not (LEFTOF2-ROBOT))
)
)
(when
(and
(LEFTOF1-ROBOT)
)
(and
(LEFTOF2-ROBOT)
(NOT-LEFTOF1-ROBOT)
(not (NOT-LEFTOF2-ROBOT))
(not (LEFTOF1-ROBOT))
)
)
(when
(and
(Flag153-)
)
(and
(RIGHTOF12-ROBOT)
(not (NOT-RIGHTOF12-ROBOT))
)
)
(when
(and
(Flag152-)
)
(and
(RIGHTOF11-ROBOT)
(not (NOT-RIGHTOF11-ROBOT))
)
)
(when
(and
(Flag151-)
)
(and
(RIGHTOF10-ROBOT)
(not (NOT-RIGHTOF10-ROBOT))
)
)
(when
(and
(Flag150-)
)
(and
(RIGHTOF9-ROBOT)
(not (NOT-RIGHTOF9-ROBOT))
)
)
(when
(and
(Flag149-)
)
(and
(RIGHTOF8-ROBOT)
(not (NOT-RIGHTOF8-ROBOT))
)
)
(when
(and
(Flag148-)
)
(and
(RIGHTOF7-ROBOT)
(not (NOT-RIGHTOF7-ROBOT))
)
)
(when
(and
(Flag147-)
)
(and
(RIGHTOF6-ROBOT)
(not (NOT-RIGHTOF6-ROBOT))
)
)
(when
(and
(Flag146-)
)
(and
(RIGHTOF5-ROBOT)
(not (NOT-RIGHTOF5-ROBOT))
)
)
(when
(and
(Flag145-)
)
(and
(RIGHTOF4-ROBOT)
(not (NOT-RIGHTOF4-ROBOT))
)
)
(when
(and
(Flag144-)
)
(and
(RIGHTOF3-ROBOT)
(not (NOT-RIGHTOF3-ROBOT))
)
)
(when
(and
(Flag143-)
)
(and
(RIGHTOF2-ROBOT)
(not (NOT-RIGHTOF2-ROBOT))
)
)
(when
(and
(Flag142-)
)
(and
(RIGHTOF1-ROBOT)
(not (NOT-RIGHTOF1-ROBOT))
)
)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action CHECKCONSISTENCYACTION
:parameters ()
:precondition
(and
(NOT-ERROR-)
(CHECKCONSISTENCY-)
)
:effect
(and
(when
(and
(LEFTOF4-ROBOT)
(RIGHTOF9-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF11-ROBOT)
(BELOWOF1-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF8-ROBOT)
(LEFTOF3-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF9-ROBOT)
(BELOWOF6-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF12-ROBOT)
(BELOWOF7-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF6-ROBOT)
(ABOVEOF12-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF12-ROBOT)
(LEFTOF10-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF10-ROBOT)
(LEFTOF4-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF8-ROBOT)
(BELOWOF1-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF1-ROBOT)
(RIGHTOF12-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF9-ROBOT)
(BELOWOF2-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF4-ROBOT)
(RIGHTOF11-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF5-ROBOT)
(LEFTOF3-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF5-ROBOT)
(LEFTOF1-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF6-ROBOT)
(RIGHTOF11-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF2-ROBOT)
(RIGHTOF9-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF3-ROBOT)
(ABOVEOF9-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF5-ROBOT)
(ABOVEOF7-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF8-ROBOT)
(ABOVEOF12-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF10-ROBOT)
(BELOWOF2-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF5-ROBOT)
(BELOWOF2-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF7-ROBOT)
(BELOWOF1-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF5-ROBOT)
(ABOVEOF12-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF1-ROBOT)
(ABOVEOF12-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF2-ROBOT)
(RIGHTOF7-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF2-ROBOT)
(RIGHTOF11-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF1-ROBOT)
(RIGHTOF8-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF11-ROBOT)
(LEFTOF5-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF9-ROBOT)
(LEFTOF5-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF5-ROBOT)
(RIGHTOF12-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF4-ROBOT)
(ABOVEOF8-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF6-ROBOT)
(RIGHTOF9-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF11-ROBOT)
(LEFTOF8-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF1-ROBOT)
(RIGHTOF6-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF3-ROBOT)
(ABOVEOF10-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF1-ROBOT)
(RIGHTOF9-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF6-ROBOT)
(LEFTOF2-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF4-ROBOT)
(RIGHTOF12-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF4-ROBOT)
(RIGHTOF6-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF3-ROBOT)
(BELOWOF2-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF6-ROBOT)
(BELOWOF2-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF12-ROBOT)
(BELOWOF2-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF3-ROBOT)
(ABOVEOF12-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF8-ROBOT)
(ABOVEOF9-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF11-ROBOT)
(BELOWOF8-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF6-ROBOT)
(RIGHTOF10-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF1-ROBOT)
(RIGHTOF3-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF12-ROBOT)
(LEFTOF9-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF9-ROBOT)
(BELOWOF7-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF1-ROBOT)
(ABOVEOF9-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF6-ROBOT)
(BELOWOF1-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF10-ROBOT)
(BELOWOF1-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF7-ROBOT)
(BELOWOF2-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF7-ROBOT)
(RIGHTOF12-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF3-ROBOT)
(ABOVEOF6-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF5-ROBOT)
(BELOWOF3-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF7-ROBOT)
(BELOWOF3-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF10-ROBOT)
(ABOVEOF12-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF2-ROBOT)
(ABOVEOF4-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF8-ROBOT)
(BELOWOF7-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF1-ROBOT)
(RIGHTOF2-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF5-ROBOT)
(ABOVEOF10-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF10-ROBOT)
(LEFTOF5-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF4-ROBOT)
(RIGHTOF5-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF11-ROBOT)
(BELOWOF6-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF4-ROBOT)
(LEFTOF1-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF3-ROBOT)
(ABOVEOF4-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF6-ROBOT)
(LEFTOF3-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF3-ROBOT)
(RIGHTOF11-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF4-ROBOT)
(BELOWOF1-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF10-ROBOT)
(BELOWOF9-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF12-ROBOT)
(LEFTOF2-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF1-ROBOT)
(RIGHTOF11-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF10-ROBOT)
(BELOWOF10-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF10-ROBOT)
(LEFTOF2-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF7-ROBOT)
(RIGHTOF10-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF10-ROBOT)
(LEFTOF3-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF12-ROBOT)
(LEFTOF3-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF8-ROBOT)
(BELOWOF6-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF10-ROBOT)
(BELOWOF7-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF5-ROBOT)
(RIGHTOF7-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF11-ROBOT)
(LEFTOF11-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF5-ROBOT)
(ABOVEOF11-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF5-ROBOT)
(ABOVEOF9-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF4-ROBOT)
(ABOVEOF6-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF3-ROBOT)
(ABOVEOF11-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF5-ROBOT)
(BELOWOF5-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF9-ROBOT)
(ABOVEOF12-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF3-ROBOT)
(RIGHTOF7-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF4-ROBOT)
(RIGHTOF8-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF11-ROBOT)
(LEFTOF10-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF4-ROBOT)
(ABOVEOF10-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF2-ROBOT)
(BELOWOF1-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF4-ROBOT)
(ABOVEOF12-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF2-ROBOT)
(RIGHTOF4-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF9-ROBOT)
(ABOVEOF11-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF9-ROBOT)
(LEFTOF3-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF5-ROBOT)
(RIGHTOF6-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF7-ROBOT)
(RIGHTOF9-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF6-ROBOT)
(BELOWOF6-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF8-ROBOT)
(BELOWOF2-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF8-ROBOT)
(LEFTOF8-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF8-ROBOT)
(RIGHTOF9-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF10-ROBOT)
(RIGHTOF10-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF4-ROBOT)
(ABOVEOF9-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF8-ROBOT)
(LEFTOF5-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF1-ROBOT)
(RIGHTOF7-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF11-ROBOT)
(BELOWOF11-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF3-ROBOT)
(BELOWOF1-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF4-ROBOT)
(ABOVEOF5-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF11-ROBOT)
(BELOWOF4-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF11-ROBOT)
(BELOWOF2-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF12-ROBOT)
(LEFTOF6-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF12-ROBOT)
(LEFTOF8-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF2-ROBOT)
(RIGHTOF8-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF9-ROBOT)
(RIGHTOF10-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF11-ROBOT)
(ABOVEOF12-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF11-ROBOT)
(LEFTOF7-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF1-ROBOT)
(RIGHTOF10-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF4-ROBOT)
(ABOVEOF7-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF5-ROBOT)
(BELOWOF1-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF11-ROBOT)
(BELOWOF7-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF4-ROBOT)
(RIGHTOF7-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF6-ROBOT)
(ABOVEOF7-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF9-ROBOT)
(RIGHTOF9-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF1-ROBOT)
(ABOVEOF1-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF8-ROBOT)
(BELOWOF3-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF9-ROBOT)
(RIGHTOF11-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF8-ROBOT)
(BELOWOF8-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF5-ROBOT)
(LEFTOF2-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF2-ROBOT)
(RIGHTOF3-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF5-ROBOT)
(ABOVEOF8-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF12-ROBOT)
(LEFTOF11-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF5-ROBOT)
(ABOVEOF6-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF8-ROBOT)
(RIGHTOF10-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF7-ROBOT)
(RIGHTOF8-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF8-ROBOT)
(LEFTOF6-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF2-ROBOT)
(ABOVEOF2-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF7-ROBOT)
(LEFTOF6-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF11-ROBOT)
(BELOWOF10-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF10-ROBOT)
(BELOWOF6-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF4-ROBOT)
(LEFTOF3-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF12-ROBOT)
(ABOVEOF12-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF3-ROBOT)
(BELOWOF3-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF6-ROBOT)
(LEFTOF6-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF4-ROBOT)
(RIGHTOF4-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF8-ROBOT)
(ABOVEOF10-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF4-ROBOT)
(BELOWOF4-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF12-ROBOT)
(RIGHTOF12-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF3-ROBOT)
(LEFTOF3-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF5-ROBOT)
(LEFTOF5-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF2-ROBOT)
(RIGHTOF2-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF7-ROBOT)
(RIGHTOF7-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF7-ROBOT)
(ABOVEOF7-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF9-ROBOT)
(BELOWOF9-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF1-ROBOT)
(RIGHTOF1-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(NOT-CHECKCONSISTENCY-)
(not (CHECKCONSISTENCY-))
)
)
(:derived (Flag3-)(and (NOT-CHECKCONSISTENCY-)(NOT-ERROR-)(Flag2-)))

(:derived (Flag5-)(and (ABOVEOF6-ROBOT)))

(:derived (Flag5-)(and (ABOVEOF11-ROBOT)))

(:derived (Flag5-)(and (ABOVEOF10-ROBOT)))

(:derived (Flag5-)(and (ABOVEOF2-ROBOT)))

(:derived (Flag5-)(and (ABOVEOF7-ROBOT)))

(:derived (Flag5-)(and (ABOVEOF12-ROBOT)))

(:derived (Flag5-)(and (ABOVEOF8-ROBOT)))

(:derived (Flag5-)(and (ABOVEOF9-ROBOT)))

(:derived (Flag5-)(and (ABOVEOF4-ROBOT)))

(:derived (Flag5-)(and (ABOVEOF3-ROBOT)))

(:derived (Flag5-)(and (ABOVEOF5-ROBOT)))

(:derived (Flag5-)(and (ABOVEOF1-ROBOT)))

(:derived (Flag6-)(and (ABOVEOF11-ROBOT)))

(:derived (Flag6-)(and (ABOVEOF6-ROBOT)))

(:derived (Flag6-)(and (ABOVEOF10-ROBOT)))

(:derived (Flag6-)(and (ABOVEOF2-ROBOT)))

(:derived (Flag6-)(and (ABOVEOF7-ROBOT)))

(:derived (Flag6-)(and (ABOVEOF12-ROBOT)))

(:derived (Flag6-)(and (ABOVEOF8-ROBOT)))

(:derived (Flag6-)(and (ABOVEOF9-ROBOT)))

(:derived (Flag6-)(and (ABOVEOF4-ROBOT)))

(:derived (Flag6-)(and (ABOVEOF3-ROBOT)))

(:derived (Flag6-)(and (ABOVEOF5-ROBOT)))

(:derived (Flag7-)(and (ABOVEOF11-ROBOT)))

(:derived (Flag7-)(and (ABOVEOF6-ROBOT)))

(:derived (Flag7-)(and (ABOVEOF10-ROBOT)))

(:derived (Flag7-)(and (ABOVEOF8-ROBOT)))

(:derived (Flag7-)(and (ABOVEOF7-ROBOT)))

(:derived (Flag7-)(and (ABOVEOF12-ROBOT)))

(:derived (Flag7-)(and (ABOVEOF9-ROBOT)))

(:derived (Flag7-)(and (ABOVEOF4-ROBOT)))

(:derived (Flag7-)(and (ABOVEOF3-ROBOT)))

(:derived (Flag7-)(and (ABOVEOF5-ROBOT)))

(:derived (Flag8-)(and (ABOVEOF11-ROBOT)))

(:derived (Flag8-)(and (ABOVEOF6-ROBOT)))

(:derived (Flag8-)(and (ABOVEOF10-ROBOT)))

(:derived (Flag8-)(and (ABOVEOF8-ROBOT)))

(:derived (Flag8-)(and (ABOVEOF7-ROBOT)))

(:derived (Flag8-)(and (ABOVEOF12-ROBOT)))

(:derived (Flag8-)(and (ABOVEOF9-ROBOT)))

(:derived (Flag8-)(and (ABOVEOF4-ROBOT)))

(:derived (Flag8-)(and (ABOVEOF5-ROBOT)))

(:derived (Flag9-)(and (ABOVEOF11-ROBOT)))

(:derived (Flag9-)(and (ABOVEOF6-ROBOT)))

(:derived (Flag9-)(and (ABOVEOF7-ROBOT)))

(:derived (Flag9-)(and (ABOVEOF8-ROBOT)))

(:derived (Flag9-)(and (ABOVEOF10-ROBOT)))

(:derived (Flag9-)(and (ABOVEOF12-ROBOT)))

(:derived (Flag9-)(and (ABOVEOF9-ROBOT)))

(:derived (Flag9-)(and (ABOVEOF5-ROBOT)))

(:derived (Flag10-)(and (ABOVEOF11-ROBOT)))

(:derived (Flag10-)(and (ABOVEOF6-ROBOT)))

(:derived (Flag10-)(and (ABOVEOF7-ROBOT)))

(:derived (Flag10-)(and (ABOVEOF8-ROBOT)))

(:derived (Flag10-)(and (ABOVEOF10-ROBOT)))

(:derived (Flag10-)(and (ABOVEOF12-ROBOT)))

(:derived (Flag10-)(and (ABOVEOF9-ROBOT)))

(:derived (Flag11-)(and (ABOVEOF11-ROBOT)))

(:derived (Flag11-)(and (ABOVEOF7-ROBOT)))

(:derived (Flag11-)(and (ABOVEOF8-ROBOT)))

(:derived (Flag11-)(and (ABOVEOF10-ROBOT)))

(:derived (Flag11-)(and (ABOVEOF12-ROBOT)))

(:derived (Flag11-)(and (ABOVEOF9-ROBOT)))

(:derived (Flag12-)(and (ABOVEOF8-ROBOT)))

(:derived (Flag12-)(and (ABOVEOF12-ROBOT)))

(:derived (Flag12-)(and (ABOVEOF11-ROBOT)))

(:derived (Flag12-)(and (ABOVEOF10-ROBOT)))

(:derived (Flag12-)(and (ABOVEOF9-ROBOT)))

(:derived (Flag13-)(and (ABOVEOF12-ROBOT)))

(:derived (Flag13-)(and (ABOVEOF11-ROBOT)))

(:derived (Flag13-)(and (ABOVEOF10-ROBOT)))

(:derived (Flag13-)(and (ABOVEOF9-ROBOT)))

(:derived (Flag14-)(and (ABOVEOF12-ROBOT)))

(:derived (Flag14-)(and (ABOVEOF10-ROBOT)))

(:derived (Flag14-)(and (ABOVEOF11-ROBOT)))

(:derived (Flag15-)(and (ABOVEOF12-ROBOT)))

(:derived (Flag15-)(and (ABOVEOF11-ROBOT)))

(:derived (Flag16-)(and (BELOWOF2-ROBOT)))

(:derived (Flag16-)(and (BELOWOF1-ROBOT)))

(:derived (Flag17-)(and (BELOWOF2-ROBOT)))

(:derived (Flag17-)(and (BELOWOF1-ROBOT)))

(:derived (Flag17-)(and (BELOWOF3-ROBOT)))

(:derived (Flag18-)(and (BELOWOF2-ROBOT)))

(:derived (Flag18-)(and (BELOWOF1-ROBOT)))

(:derived (Flag18-)(and (BELOWOF3-ROBOT)))

(:derived (Flag18-)(and (BELOWOF4-ROBOT)))

(:derived (Flag19-)(and (BELOWOF2-ROBOT)))

(:derived (Flag19-)(and (BELOWOF1-ROBOT)))

(:derived (Flag19-)(and (BELOWOF3-ROBOT)))

(:derived (Flag19-)(and (BELOWOF4-ROBOT)))

(:derived (Flag19-)(and (BELOWOF5-ROBOT)))

(:derived (Flag20-)(and (BELOWOF6-ROBOT)))

(:derived (Flag20-)(and (BELOWOF5-ROBOT)))

(:derived (Flag20-)(and (BELOWOF2-ROBOT)))

(:derived (Flag20-)(and (BELOWOF1-ROBOT)))

(:derived (Flag20-)(and (BELOWOF4-ROBOT)))

(:derived (Flag20-)(and (BELOWOF3-ROBOT)))

(:derived (Flag21-)(and (BELOWOF6-ROBOT)))

(:derived (Flag21-)(and (BELOWOF5-ROBOT)))

(:derived (Flag21-)(and (BELOWOF2-ROBOT)))

(:derived (Flag21-)(and (BELOWOF1-ROBOT)))

(:derived (Flag21-)(and (BELOWOF4-ROBOT)))

(:derived (Flag21-)(and (BELOWOF7-ROBOT)))

(:derived (Flag21-)(and (BELOWOF3-ROBOT)))

(:derived (Flag22-)(and (BELOWOF6-ROBOT)))

(:derived (Flag22-)(and (BELOWOF5-ROBOT)))

(:derived (Flag22-)(and (BELOWOF2-ROBOT)))

(:derived (Flag22-)(and (BELOWOF1-ROBOT)))

(:derived (Flag22-)(and (BELOWOF4-ROBOT)))

(:derived (Flag22-)(and (BELOWOF7-ROBOT)))

(:derived (Flag22-)(and (BELOWOF3-ROBOT)))

(:derived (Flag22-)(and (BELOWOF8-ROBOT)))

(:derived (Flag23-)(and (BELOWOF6-ROBOT)))

(:derived (Flag23-)(and (BELOWOF5-ROBOT)))

(:derived (Flag23-)(and (BELOWOF2-ROBOT)))

(:derived (Flag23-)(and (BELOWOF1-ROBOT)))

(:derived (Flag23-)(and (BELOWOF4-ROBOT)))

(:derived (Flag23-)(and (BELOWOF7-ROBOT)))

(:derived (Flag23-)(and (BELOWOF9-ROBOT)))

(:derived (Flag23-)(and (BELOWOF3-ROBOT)))

(:derived (Flag23-)(and (BELOWOF8-ROBOT)))

(:derived (Flag24-)(and (BELOWOF10-ROBOT)))

(:derived (Flag24-)(and (BELOWOF6-ROBOT)))

(:derived (Flag24-)(and (BELOWOF5-ROBOT)))

(:derived (Flag24-)(and (BELOWOF2-ROBOT)))

(:derived (Flag24-)(and (BELOWOF1-ROBOT)))

(:derived (Flag24-)(and (BELOWOF4-ROBOT)))

(:derived (Flag24-)(and (BELOWOF7-ROBOT)))

(:derived (Flag24-)(and (BELOWOF9-ROBOT)))

(:derived (Flag24-)(and (BELOWOF3-ROBOT)))

(:derived (Flag24-)(and (BELOWOF8-ROBOT)))

(:derived (Flag25-)(and (BELOWOF10-ROBOT)))

(:derived (Flag25-)(and (BELOWOF11-ROBOT)))

(:derived (Flag25-)(and (BELOWOF6-ROBOT)))

(:derived (Flag25-)(and (BELOWOF5-ROBOT)))

(:derived (Flag25-)(and (BELOWOF2-ROBOT)))

(:derived (Flag25-)(and (BELOWOF1-ROBOT)))

(:derived (Flag25-)(and (BELOWOF4-ROBOT)))

(:derived (Flag25-)(and (BELOWOF7-ROBOT)))

(:derived (Flag25-)(and (BELOWOF9-ROBOT)))

(:derived (Flag25-)(and (BELOWOF3-ROBOT)))

(:derived (Flag25-)(and (BELOWOF8-ROBOT)))

(:derived (Flag26-)(and (BELOWOF10-ROBOT)))

(:derived (Flag26-)(and (BELOWOF11-ROBOT)))

(:derived (Flag26-)(and (BELOWOF6-ROBOT)))

(:derived (Flag26-)(and (BELOWOF5-ROBOT)))

(:derived (Flag26-)(and (BELOWOF2-ROBOT)))

(:derived (Flag26-)(and (BELOWOF1-ROBOT)))

(:derived (Flag26-)(and (BELOWOF4-ROBOT)))

(:derived (Flag26-)(and (BELOWOF7-ROBOT)))

(:derived (Flag26-)(and (BELOWOF9-ROBOT)))

(:derived (Flag26-)(and (BELOWOF3-ROBOT)))

(:derived (Flag26-)(and (BELOWOF8-ROBOT)))

(:derived (Flag27-)(and (BELOWOF1-ROBOT)(ABOVEOF4-ROBOT)))

(:derived (Flag28-)(and (BELOWOF1-ROBOT)(ABOVEOF2-ROBOT)))

(:derived (Flag29-)(and (BELOWOF1-ROBOT)(ABOVEOF1-ROBOT)))

(:derived (Flag30-)(and (ABOVEOF12-ROBOT)(BELOWOF1-ROBOT)))

(:derived (Flag31-)(and (BELOWOF1-ROBOT)(ABOVEOF7-ROBOT)))

(:derived (Flag32-)(and (ABOVEOF11-ROBOT)(BELOWOF1-ROBOT)))

(:derived (Flag33-)(and (ABOVEOF6-ROBOT)(BELOWOF1-ROBOT)))

(:derived (Flag34-)(and (BELOWOF1-ROBOT)(ABOVEOF5-ROBOT)))

(:derived (Flag35-)(and (ABOVEOF10-ROBOT)(BELOWOF1-ROBOT)))

(:derived (Flag36-)(and (ABOVEOF8-ROBOT)(BELOWOF1-ROBOT)))

(:derived (Flag37-)(and (ABOVEOF3-ROBOT)(BELOWOF1-ROBOT)))

(:derived (Flag38-)(and (BELOWOF1-ROBOT)(ABOVEOF9-ROBOT)))

(:derived (Flag39-)(and (BELOWOF1-ROBOT)))

(:derived (Flag40-)(and (ABOVEOF2-ROBOT)(BELOWOF2-ROBOT)))

(:derived (Flag41-)(and (ABOVEOF1-ROBOT)(BELOWOF2-ROBOT)))

(:derived (Flag42-)(and (ABOVEOF7-ROBOT)(BELOWOF2-ROBOT)))

(:derived (Flag43-)(and (ABOVEOF4-ROBOT)(BELOWOF2-ROBOT)))

(:derived (Flag44-)(and (ABOVEOF6-ROBOT)(BELOWOF2-ROBOT)))

(:derived (Flag45-)(and (ABOVEOF9-ROBOT)(BELOWOF2-ROBOT)))

(:derived (Flag46-)(and (ABOVEOF11-ROBOT)(BELOWOF2-ROBOT)))

(:derived (Flag47-)(and (ABOVEOF10-ROBOT)(BELOWOF2-ROBOT)))

(:derived (Flag48-)(and (ABOVEOF8-ROBOT)(BELOWOF2-ROBOT)))

(:derived (Flag49-)(and (ABOVEOF12-ROBOT)(BELOWOF2-ROBOT)))

(:derived (Flag50-)(and (BELOWOF2-ROBOT)(ABOVEOF5-ROBOT)))

(:derived (Flag51-)(and (ABOVEOF3-ROBOT)(BELOWOF2-ROBOT)))

(:derived (Flag53-)(and (BELOWOF3-ROBOT)(ABOVEOF5-ROBOT)))

(:derived (Flag54-)(and (BELOWOF3-ROBOT)(ABOVEOF6-ROBOT)))

(:derived (Flag55-)(and (BELOWOF3-ROBOT)(ABOVEOF7-ROBOT)))

(:derived (Flag56-)(and (BELOWOF3-ROBOT)(ABOVEOF9-ROBOT)))

(:derived (Flag57-)(and (BELOWOF3-ROBOT)(ABOVEOF2-ROBOT)))

(:derived (Flag58-)(and (ABOVEOF3-ROBOT)(BELOWOF3-ROBOT)))

(:derived (Flag59-)(and (ABOVEOF8-ROBOT)(BELOWOF3-ROBOT)))

(:derived (Flag60-)(and (BELOWOF3-ROBOT)(ABOVEOF4-ROBOT)))

(:derived (Flag61-)(and (BELOWOF3-ROBOT)(ABOVEOF10-ROBOT)))

(:derived (Flag62-)(and (BELOWOF2-ROBOT)(ABOVEOF2-ROBOT)))

(:derived (Flag63-)(and (ABOVEOF12-ROBOT)(BELOWOF3-ROBOT)))

(:derived (Flag64-)(and (ABOVEOF11-ROBOT)(BELOWOF3-ROBOT)))

(:derived (Flag66-)(and (ABOVEOF3-ROBOT)(BELOWOF4-ROBOT)))

(:derived (Flag67-)(and (ABOVEOF6-ROBOT)(BELOWOF4-ROBOT)))

(:derived (Flag68-)(and (ABOVEOF9-ROBOT)(BELOWOF4-ROBOT)))

(:derived (Flag69-)(and (BELOWOF4-ROBOT)(ABOVEOF4-ROBOT)))

(:derived (Flag70-)(and (ABOVEOF8-ROBOT)(BELOWOF4-ROBOT)))

(:derived (Flag71-)(and (BELOWOF4-ROBOT)(ABOVEOF7-ROBOT)))

(:derived (Flag72-)(and (ABOVEOF12-ROBOT)(BELOWOF4-ROBOT)))

(:derived (Flag73-)(and (BELOWOF4-ROBOT)(ABOVEOF5-ROBOT)))

(:derived (Flag74-)(and (BELOWOF3-ROBOT)(ABOVEOF3-ROBOT)))

(:derived (Flag75-)(and (ABOVEOF10-ROBOT)(BELOWOF4-ROBOT)))

(:derived (Flag76-)(and (BELOWOF3-ROBOT)(ABOVEOF11-ROBOT)))

(:derived (Flag77-)(and (ABOVEOF11-ROBOT)(BELOWOF4-ROBOT)))

(:derived (Flag79-)(and (ABOVEOF4-ROBOT)(BELOWOF5-ROBOT)))

(:derived (Flag80-)(and (ABOVEOF8-ROBOT)(BELOWOF5-ROBOT)))

(:derived (Flag81-)(and (ABOVEOF6-ROBOT)(BELOWOF5-ROBOT)))

(:derived (Flag82-)(and (ABOVEOF10-ROBOT)(BELOWOF5-ROBOT)))

(:derived (Flag83-)(and (ABOVEOF11-ROBOT)(BELOWOF5-ROBOT)))

(:derived (Flag84-)(and (ABOVEOF5-ROBOT)(BELOWOF5-ROBOT)))

(:derived (Flag85-)(and (ABOVEOF7-ROBOT)(BELOWOF5-ROBOT)))

(:derived (Flag86-)(and (ABOVEOF9-ROBOT)(BELOWOF5-ROBOT)))

(:derived (Flag87-)(and (ABOVEOF12-ROBOT)(BELOWOF5-ROBOT)))

(:derived (Flag89-)(and (ABOVEOF11-ROBOT)(BELOWOF6-ROBOT)))

(:derived (Flag90-)(and (ABOVEOF12-ROBOT)(BELOWOF6-ROBOT)))

(:derived (Flag91-)(and (BELOWOF6-ROBOT)(ABOVEOF5-ROBOT)))

(:derived (Flag92-)(and (BELOWOF6-ROBOT)(ABOVEOF7-ROBOT)))

(:derived (Flag93-)(and (BELOWOF5-ROBOT)(ABOVEOF5-ROBOT)))

(:derived (Flag94-)(and (BELOWOF6-ROBOT)(ABOVEOF6-ROBOT)))

(:derived (Flag95-)(and (ABOVEOF8-ROBOT)(BELOWOF6-ROBOT)))

(:derived (Flag96-)(and (BELOWOF6-ROBOT)(ABOVEOF9-ROBOT)))

(:derived (Flag97-)(and (BELOWOF6-ROBOT)(ABOVEOF10-ROBOT)))

(:derived (Flag99-)(and (ABOVEOF6-ROBOT)(BELOWOF7-ROBOT)))

(:derived (Flag100-)(and (ABOVEOF9-ROBOT)(BELOWOF7-ROBOT)))

(:derived (Flag101-)(and (ABOVEOF6-ROBOT)(BELOWOF6-ROBOT)))

(:derived (Flag102-)(and (ABOVEOF10-ROBOT)(BELOWOF7-ROBOT)))

(:derived (Flag103-)(and (ABOVEOF10-ROBOT)(BELOWOF6-ROBOT)))

(:derived (Flag104-)(and (ABOVEOF12-ROBOT)(BELOWOF7-ROBOT)))

(:derived (Flag105-)(and (ABOVEOF7-ROBOT)(BELOWOF7-ROBOT)))

(:derived (Flag106-)(and (ABOVEOF8-ROBOT)(BELOWOF7-ROBOT)))

(:derived (Flag107-)(and (ABOVEOF11-ROBOT)(BELOWOF7-ROBOT)))

(:derived (Flag109-)(and (BELOWOF8-ROBOT)(ABOVEOF10-ROBOT)))

(:derived (Flag110-)(and (ABOVEOF12-ROBOT)(BELOWOF8-ROBOT)))

(:derived (Flag111-)(and (ABOVEOF8-ROBOT)(BELOWOF8-ROBOT)))

(:derived (Flag112-)(and (BELOWOF8-ROBOT)(ABOVEOF9-ROBOT)))

(:derived (Flag113-)(and (BELOWOF7-ROBOT)(ABOVEOF7-ROBOT)))

(:derived (Flag114-)(and (BELOWOF8-ROBOT)(ABOVEOF7-ROBOT)))

(:derived (Flag115-)(and (BELOWOF8-ROBOT)(ABOVEOF11-ROBOT)))

(:derived (Flag117-)(and (BELOWOF8-ROBOT)(ABOVEOF12-ROBOT)))

(:derived (Flag118-)(and (ABOVEOF10-ROBOT)(BELOWOF9-ROBOT)))

(:derived (Flag119-)(and (BELOWOF8-ROBOT)(ABOVEOF8-ROBOT)))

(:derived (Flag120-)(and (ABOVEOF8-ROBOT)(BELOWOF9-ROBOT)))

(:derived (Flag121-)(and (ABOVEOF12-ROBOT)(BELOWOF9-ROBOT)))

(:derived (Flag122-)(and (ABOVEOF11-ROBOT)(BELOWOF9-ROBOT)))

(:derived (Flag123-)(and (BELOWOF9-ROBOT)(ABOVEOF9-ROBOT)))

(:derived (Flag125-)(and (BELOWOF10-ROBOT)(ABOVEOF9-ROBOT)))

(:derived (Flag126-)(and (BELOWOF10-ROBOT)(ABOVEOF10-ROBOT)))

(:derived (Flag127-)(and (ABOVEOF12-ROBOT)(BELOWOF10-ROBOT)))

(:derived (Flag128-)(and (ABOVEOF9-ROBOT)(BELOWOF9-ROBOT)))

(:derived (Flag129-)(and (ABOVEOF11-ROBOT)(BELOWOF10-ROBOT)))

(:derived (Flag131-)(and (BELOWOF11-ROBOT)(ABOVEOF10-ROBOT)))

(:derived (Flag132-)(and (ABOVEOF11-ROBOT)(BELOWOF11-ROBOT)))

(:derived (Flag133-)(and (ABOVEOF12-ROBOT)(BELOWOF11-ROBOT)))

(:derived (Flag135-)(and (ABOVEOF12-ROBOT)(BELOWOF12-ROBOT)))

(:derived (Flag136-)(and (BELOWOF12-ROBOT)(ABOVEOF11-ROBOT)))

(:derived (Flag138-)(and (BELOWOF10-ROBOT)))

(:derived (Flag138-)(and (BELOWOF11-ROBOT)))

(:derived (Flag138-)(and (BELOWOF6-ROBOT)))

(:derived (Flag138-)(and (BELOWOF5-ROBOT)))

(:derived (Flag138-)(and (BELOWOF2-ROBOT)))

(:derived (Flag138-)(and (BELOWOF1-ROBOT)))

(:derived (Flag138-)(and (BELOWOF4-ROBOT)))

(:derived (Flag138-)(and (BELOWOF7-ROBOT)))

(:derived (Flag138-)(and (BELOWOF9-ROBOT)))

(:derived (Flag138-)(and (BELOWOF3-ROBOT)))

(:derived (Flag138-)(and (BELOWOF13-ROBOT)))

(:derived (Flag138-)(and (BELOWOF8-ROBOT)))

(:derived (Flag139-)(and (ABOVEOF12-ROBOT)(BELOWOF13-ROBOT)))

(:derived (Flag140-)(and (BELOWOF12-ROBOT)(ABOVEOF12-ROBOT)))

(:action MOVEDOWN-ROBOT
:parameters ()
:precondition
(and
(Flag4-)
)
:effect
(and
(when
(and
(Flag141-)
)
(and
(ROW11-ROBOT)
(not (NOT-ROW11-ROBOT))
)
)
(when
(and
(Flag137-)
)
(and
(ROW10-ROBOT)
(not (NOT-ROW10-ROBOT))
)
)
(when
(and
(Flag134-)
)
(and
(ROW9-ROBOT)
(not (NOT-ROW9-ROBOT))
)
)
(when
(and
(Flag130-)
)
(and
(ROW8-ROBOT)
(not (NOT-ROW8-ROBOT))
)
)
(when
(and
(Flag124-)
)
(and
(ROW7-ROBOT)
(not (NOT-ROW7-ROBOT))
)
)
(when
(and
(Flag116-)
)
(and
(ROW6-ROBOT)
(not (NOT-ROW6-ROBOT))
)
)
(when
(and
(Flag108-)
)
(and
(ROW5-ROBOT)
(not (NOT-ROW5-ROBOT))
)
)
(when
(and
(Flag98-)
)
(and
(ROW4-ROBOT)
(not (NOT-ROW4-ROBOT))
)
)
(when
(and
(Flag88-)
)
(and
(ROW3-ROBOT)
(not (NOT-ROW3-ROBOT))
)
)
(when
(and
(Flag78-)
)
(and
(ROW2-ROBOT)
(not (NOT-ROW2-ROBOT))
)
)
(when
(and
(Flag65-)
)
(and
(ROW1-ROBOT)
(not (NOT-ROW1-ROBOT))
)
)
(when
(and
(Flag52-)
)
(and
(ROW0-ROBOT)
(not (NOT-ROW0-ROBOT))
)
)
(when
(and
(ROW12-ROBOT)
)
(and
(ROW11-ROBOT)
(NOT-ROW12-ROBOT)
(not (NOT-ROW11-ROBOT))
(not (ROW12-ROBOT))
)
)
(when
(and
(ROW11-ROBOT)
)
(and
(ROW10-ROBOT)
(NOT-ROW11-ROBOT)
(not (NOT-ROW10-ROBOT))
(not (ROW11-ROBOT))
)
)
(when
(and
(ROW10-ROBOT)
)
(and
(ROW9-ROBOT)
(NOT-ROW10-ROBOT)
(not (NOT-ROW9-ROBOT))
(not (ROW10-ROBOT))
)
)
(when
(and
(ROW9-ROBOT)
)
(and
(ROW8-ROBOT)
(NOT-ROW9-ROBOT)
(not (NOT-ROW8-ROBOT))
(not (ROW9-ROBOT))
)
)
(when
(and
(ROW8-ROBOT)
)
(and
(ROW7-ROBOT)
(NOT-ROW8-ROBOT)
(not (NOT-ROW7-ROBOT))
(not (ROW8-ROBOT))
)
)
(when
(and
(ROW7-ROBOT)
)
(and
(ROW6-ROBOT)
(NOT-ROW7-ROBOT)
(not (NOT-ROW6-ROBOT))
(not (ROW7-ROBOT))
)
)
(when
(and
(ROW6-ROBOT)
)
(and
(ROW5-ROBOT)
(NOT-ROW6-ROBOT)
(not (NOT-ROW5-ROBOT))
(not (ROW6-ROBOT))
)
)
(when
(and
(ROW5-ROBOT)
)
(and
(ROW4-ROBOT)
(NOT-ROW5-ROBOT)
(not (NOT-ROW4-ROBOT))
(not (ROW5-ROBOT))
)
)
(when
(and
(ROW4-ROBOT)
)
(and
(ROW3-ROBOT)
(NOT-ROW4-ROBOT)
(not (NOT-ROW3-ROBOT))
(not (ROW4-ROBOT))
)
)
(when
(and
(ROW3-ROBOT)
)
(and
(ROW2-ROBOT)
(NOT-ROW3-ROBOT)
(not (NOT-ROW2-ROBOT))
(not (ROW3-ROBOT))
)
)
(when
(and
(ROW2-ROBOT)
)
(and
(ROW1-ROBOT)
(NOT-ROW2-ROBOT)
(not (NOT-ROW1-ROBOT))
(not (ROW2-ROBOT))
)
)
(when
(and
(ROW1-ROBOT)
)
(and
(ROW0-ROBOT)
(NOT-ROW1-ROBOT)
(not (NOT-ROW0-ROBOT))
(not (ROW1-ROBOT))
)
)
(when
(and
(ABOVEOF12-ROBOT)
)
(and
(ABOVEOF11-ROBOT)
(NOT-ABOVEOF12-ROBOT)
(ABOVEOF10-ROBOT)
(NOT-ABOVEOF11-ROBOT)
(ABOVEOF9-ROBOT)
(NOT-ABOVEOF10-ROBOT)
(ABOVEOF8-ROBOT)
(NOT-ABOVEOF9-ROBOT)
(ABOVEOF7-ROBOT)
(NOT-ABOVEOF8-ROBOT)
(ABOVEOF6-ROBOT)
(NOT-ABOVEOF7-ROBOT)
(ABOVEOF5-ROBOT)
(NOT-ABOVEOF6-ROBOT)
(ABOVEOF4-ROBOT)
(NOT-ABOVEOF5-ROBOT)
(ABOVEOF3-ROBOT)
(NOT-ABOVEOF4-ROBOT)
(ABOVEOF2-ROBOT)
(NOT-ABOVEOF3-ROBOT)
(ABOVEOF1-ROBOT)
(NOT-ABOVEOF2-ROBOT)
(NOT-ABOVEOF1-ROBOT)
(not (ABOVEOF12-ROBOT))
)
)
(when
(and
(ABOVEOF11-ROBOT)
)
(and
(ABOVEOF10-ROBOT)
(NOT-ABOVEOF11-ROBOT)
(ABOVEOF9-ROBOT)
(NOT-ABOVEOF10-ROBOT)
(ABOVEOF8-ROBOT)
(NOT-ABOVEOF9-ROBOT)
(ABOVEOF7-ROBOT)
(NOT-ABOVEOF8-ROBOT)
(ABOVEOF6-ROBOT)
(NOT-ABOVEOF7-ROBOT)
(ABOVEOF5-ROBOT)
(NOT-ABOVEOF6-ROBOT)
(ABOVEOF4-ROBOT)
(NOT-ABOVEOF5-ROBOT)
(ABOVEOF3-ROBOT)
(NOT-ABOVEOF4-ROBOT)
(ABOVEOF2-ROBOT)
(NOT-ABOVEOF3-ROBOT)
(ABOVEOF1-ROBOT)
(NOT-ABOVEOF2-ROBOT)
(NOT-ABOVEOF1-ROBOT)
(not (ABOVEOF11-ROBOT))
)
)
(when
(and
(ABOVEOF10-ROBOT)
)
(and
(ABOVEOF9-ROBOT)
(NOT-ABOVEOF10-ROBOT)
(ABOVEOF8-ROBOT)
(NOT-ABOVEOF9-ROBOT)
(ABOVEOF7-ROBOT)
(NOT-ABOVEOF8-ROBOT)
(ABOVEOF6-ROBOT)
(NOT-ABOVEOF7-ROBOT)
(ABOVEOF5-ROBOT)
(NOT-ABOVEOF6-ROBOT)
(ABOVEOF4-ROBOT)
(NOT-ABOVEOF5-ROBOT)
(ABOVEOF3-ROBOT)
(NOT-ABOVEOF4-ROBOT)
(ABOVEOF2-ROBOT)
(NOT-ABOVEOF3-ROBOT)
(ABOVEOF1-ROBOT)
(NOT-ABOVEOF2-ROBOT)
(NOT-ABOVEOF1-ROBOT)
(not (ABOVEOF10-ROBOT))
)
)
(when
(and
(ABOVEOF9-ROBOT)
)
(and
(ABOVEOF8-ROBOT)
(NOT-ABOVEOF9-ROBOT)
(ABOVEOF7-ROBOT)
(NOT-ABOVEOF8-ROBOT)
(ABOVEOF6-ROBOT)
(NOT-ABOVEOF7-ROBOT)
(ABOVEOF5-ROBOT)
(NOT-ABOVEOF6-ROBOT)
(ABOVEOF4-ROBOT)
(NOT-ABOVEOF5-ROBOT)
(ABOVEOF3-ROBOT)
(NOT-ABOVEOF4-ROBOT)
(ABOVEOF2-ROBOT)
(NOT-ABOVEOF3-ROBOT)
(ABOVEOF1-ROBOT)
(NOT-ABOVEOF2-ROBOT)
(NOT-ABOVEOF1-ROBOT)
(not (ABOVEOF9-ROBOT))
)
)
(when
(and
(ABOVEOF8-ROBOT)
)
(and
(ABOVEOF7-ROBOT)
(NOT-ABOVEOF8-ROBOT)
(ABOVEOF6-ROBOT)
(NOT-ABOVEOF7-ROBOT)
(ABOVEOF5-ROBOT)
(NOT-ABOVEOF6-ROBOT)
(ABOVEOF4-ROBOT)
(NOT-ABOVEOF5-ROBOT)
(ABOVEOF3-ROBOT)
(NOT-ABOVEOF4-ROBOT)
(ABOVEOF2-ROBOT)
(NOT-ABOVEOF3-ROBOT)
(ABOVEOF1-ROBOT)
(NOT-ABOVEOF2-ROBOT)
(NOT-ABOVEOF1-ROBOT)
(not (ABOVEOF8-ROBOT))
)
)
(when
(and
(ABOVEOF7-ROBOT)
)
(and
(ABOVEOF6-ROBOT)
(NOT-ABOVEOF7-ROBOT)
(ABOVEOF5-ROBOT)
(NOT-ABOVEOF6-ROBOT)
(ABOVEOF4-ROBOT)
(NOT-ABOVEOF5-ROBOT)
(ABOVEOF3-ROBOT)
(NOT-ABOVEOF4-ROBOT)
(ABOVEOF2-ROBOT)
(NOT-ABOVEOF3-ROBOT)
(ABOVEOF1-ROBOT)
(NOT-ABOVEOF2-ROBOT)
(NOT-ABOVEOF1-ROBOT)
(not (ABOVEOF7-ROBOT))
)
)
(when
(and
(ABOVEOF6-ROBOT)
)
(and
(ABOVEOF5-ROBOT)
(NOT-ABOVEOF6-ROBOT)
(ABOVEOF4-ROBOT)
(NOT-ABOVEOF5-ROBOT)
(ABOVEOF3-ROBOT)
(NOT-ABOVEOF4-ROBOT)
(ABOVEOF2-ROBOT)
(NOT-ABOVEOF3-ROBOT)
(ABOVEOF1-ROBOT)
(NOT-ABOVEOF2-ROBOT)
(NOT-ABOVEOF1-ROBOT)
(not (ABOVEOF6-ROBOT))
)
)
(when
(and
(ABOVEOF5-ROBOT)
)
(and
(ABOVEOF4-ROBOT)
(NOT-ABOVEOF5-ROBOT)
(ABOVEOF3-ROBOT)
(NOT-ABOVEOF4-ROBOT)
(ABOVEOF2-ROBOT)
(NOT-ABOVEOF3-ROBOT)
(ABOVEOF1-ROBOT)
(NOT-ABOVEOF2-ROBOT)
(NOT-ABOVEOF1-ROBOT)
(not (ABOVEOF5-ROBOT))
)
)
(when
(and
(ABOVEOF4-ROBOT)
)
(and
(ABOVEOF3-ROBOT)
(NOT-ABOVEOF4-ROBOT)
(ABOVEOF2-ROBOT)
(NOT-ABOVEOF3-ROBOT)
(ABOVEOF1-ROBOT)
(NOT-ABOVEOF2-ROBOT)
(NOT-ABOVEOF1-ROBOT)
(not (ABOVEOF4-ROBOT))
)
)
(when
(and
(ABOVEOF3-ROBOT)
)
(and
(ABOVEOF2-ROBOT)
(NOT-ABOVEOF3-ROBOT)
(ABOVEOF1-ROBOT)
(NOT-ABOVEOF2-ROBOT)
(NOT-ABOVEOF1-ROBOT)
(not (ABOVEOF3-ROBOT))
)
)
(when
(and
(ABOVEOF2-ROBOT)
)
(and
(ABOVEOF1-ROBOT)
(NOT-ABOVEOF2-ROBOT)
(NOT-ABOVEOF1-ROBOT)
(not (ABOVEOF2-ROBOT))
)
)
(when
(and
(ABOVEOF1-ROBOT)
)
(and
(NOT-ABOVEOF1-ROBOT)
(not (ABOVEOF1-ROBOT))
)
)
(when
(and
(Flag138-)
)
(and
(BELOWOF12-ROBOT)
(not (NOT-BELOWOF12-ROBOT))
)
)
(when
(and
(BELOWOF8-ROBOT)
)
(and
(BELOWOF11-ROBOT)
(BELOWOF10-ROBOT)
(BELOWOF9-ROBOT)
(BELOWOF7-ROBOT)
(not (NOT-BELOWOF11-ROBOT))
(not (NOT-BELOWOF10-ROBOT))
(not (NOT-BELOWOF9-ROBOT))
(not (NOT-BELOWOF8-ROBOT))
(not (NOT-BELOWOF7-ROBOT))
)
)
(when
(and
(BELOWOF3-ROBOT)
)
(and
(BELOWOF11-ROBOT)
(BELOWOF10-ROBOT)
(BELOWOF9-ROBOT)
(BELOWOF8-ROBOT)
(BELOWOF7-ROBOT)
(BELOWOF6-ROBOT)
(BELOWOF5-ROBOT)
(BELOWOF4-ROBOT)
(BELOWOF2-ROBOT)
(not (NOT-BELOWOF11-ROBOT))
(not (NOT-BELOWOF10-ROBOT))
(not (NOT-BELOWOF9-ROBOT))
(not (NOT-BELOWOF8-ROBOT))
(not (NOT-BELOWOF7-ROBOT))
(not (NOT-BELOWOF6-ROBOT))
(not (NOT-BELOWOF5-ROBOT))
(not (NOT-BELOWOF4-ROBOT))
(not (NOT-BELOWOF3-ROBOT))
(not (NOT-BELOWOF2-ROBOT))
)
)
(when
(and
(BELOWOF9-ROBOT)
)
(and
(BELOWOF11-ROBOT)
(BELOWOF10-ROBOT)
(BELOWOF8-ROBOT)
(not (NOT-BELOWOF11-ROBOT))
(not (NOT-BELOWOF10-ROBOT))
(not (NOT-BELOWOF9-ROBOT))
(not (NOT-BELOWOF8-ROBOT))
)
)
(when
(and
(BELOWOF7-ROBOT)
)
(and
(BELOWOF11-ROBOT)
(BELOWOF10-ROBOT)
(BELOWOF9-ROBOT)
(BELOWOF8-ROBOT)
(BELOWOF6-ROBOT)
(not (NOT-BELOWOF11-ROBOT))
(not (NOT-BELOWOF10-ROBOT))
(not (NOT-BELOWOF9-ROBOT))
(not (NOT-BELOWOF8-ROBOT))
(not (NOT-BELOWOF7-ROBOT))
(not (NOT-BELOWOF6-ROBOT))
)
)
(when
(and
(BELOWOF12-ROBOT)
)
(and
(BELOWOF11-ROBOT)
(not (NOT-BELOWOF11-ROBOT))
)
)
(when
(and
(BELOWOF4-ROBOT)
)
(and
(BELOWOF11-ROBOT)
(BELOWOF10-ROBOT)
(BELOWOF9-ROBOT)
(BELOWOF8-ROBOT)
(BELOWOF7-ROBOT)
(BELOWOF6-ROBOT)
(BELOWOF5-ROBOT)
(BELOWOF3-ROBOT)
(not (NOT-BELOWOF11-ROBOT))
(not (NOT-BELOWOF10-ROBOT))
(not (NOT-BELOWOF9-ROBOT))
(not (NOT-BELOWOF8-ROBOT))
(not (NOT-BELOWOF7-ROBOT))
(not (NOT-BELOWOF6-ROBOT))
(not (NOT-BELOWOF5-ROBOT))
(not (NOT-BELOWOF4-ROBOT))
(not (NOT-BELOWOF3-ROBOT))
)
)
(when
(and
(BELOWOF1-ROBOT)
)
(and
(BELOWOF11-ROBOT)
(BELOWOF10-ROBOT)
(BELOWOF9-ROBOT)
(BELOWOF8-ROBOT)
(BELOWOF7-ROBOT)
(BELOWOF6-ROBOT)
(BELOWOF5-ROBOT)
(BELOWOF4-ROBOT)
(BELOWOF3-ROBOT)
(BELOWOF2-ROBOT)
(not (NOT-BELOWOF11-ROBOT))
(not (NOT-BELOWOF10-ROBOT))
(not (NOT-BELOWOF9-ROBOT))
(not (NOT-BELOWOF8-ROBOT))
(not (NOT-BELOWOF7-ROBOT))
(not (NOT-BELOWOF6-ROBOT))
(not (NOT-BELOWOF5-ROBOT))
(not (NOT-BELOWOF4-ROBOT))
(not (NOT-BELOWOF3-ROBOT))
(not (NOT-BELOWOF2-ROBOT))
(not (NOT-BELOWOF1-ROBOT))
)
)
(when
(and
(BELOWOF2-ROBOT)
)
(and
(BELOWOF11-ROBOT)
(BELOWOF10-ROBOT)
(BELOWOF9-ROBOT)
(BELOWOF8-ROBOT)
(BELOWOF7-ROBOT)
(BELOWOF6-ROBOT)
(BELOWOF5-ROBOT)
(BELOWOF4-ROBOT)
(BELOWOF3-ROBOT)
(BELOWOF1-ROBOT)
(not (NOT-BELOWOF11-ROBOT))
(not (NOT-BELOWOF10-ROBOT))
(not (NOT-BELOWOF9-ROBOT))
(not (NOT-BELOWOF8-ROBOT))
(not (NOT-BELOWOF7-ROBOT))
(not (NOT-BELOWOF6-ROBOT))
(not (NOT-BELOWOF5-ROBOT))
(not (NOT-BELOWOF4-ROBOT))
(not (NOT-BELOWOF3-ROBOT))
(not (NOT-BELOWOF2-ROBOT))
(not (NOT-BELOWOF1-ROBOT))
)
)
(when
(and
(BELOWOF5-ROBOT)
)
(and
(BELOWOF11-ROBOT)
(BELOWOF10-ROBOT)
(BELOWOF9-ROBOT)
(BELOWOF8-ROBOT)
(BELOWOF7-ROBOT)
(BELOWOF6-ROBOT)
(BELOWOF4-ROBOT)
(not (NOT-BELOWOF11-ROBOT))
(not (NOT-BELOWOF10-ROBOT))
(not (NOT-BELOWOF9-ROBOT))
(not (NOT-BELOWOF8-ROBOT))
(not (NOT-BELOWOF7-ROBOT))
(not (NOT-BELOWOF6-ROBOT))
(not (NOT-BELOWOF5-ROBOT))
(not (NOT-BELOWOF4-ROBOT))
)
)
(when
(and
(BELOWOF6-ROBOT)
)
(and
(BELOWOF11-ROBOT)
(BELOWOF10-ROBOT)
(BELOWOF9-ROBOT)
(BELOWOF8-ROBOT)
(BELOWOF7-ROBOT)
(BELOWOF5-ROBOT)
(not (NOT-BELOWOF11-ROBOT))
(not (NOT-BELOWOF10-ROBOT))
(not (NOT-BELOWOF9-ROBOT))
(not (NOT-BELOWOF8-ROBOT))
(not (NOT-BELOWOF7-ROBOT))
(not (NOT-BELOWOF6-ROBOT))
(not (NOT-BELOWOF5-ROBOT))
)
)
(when
(and
(BELOWOF11-ROBOT)
)
(and
(BELOWOF10-ROBOT)
(not (NOT-BELOWOF11-ROBOT))
(not (NOT-BELOWOF10-ROBOT))
)
)
(when
(and
(BELOWOF10-ROBOT)
)
(and
(BELOWOF11-ROBOT)
(BELOWOF9-ROBOT)
(not (NOT-BELOWOF11-ROBOT))
(not (NOT-BELOWOF10-ROBOT))
(not (NOT-BELOWOF9-ROBOT))
)
)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action MOVEUP-ROBOT
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
)
:effect
(and
(when
(and
(Flag137-)
)
(and
(ROW12-ROBOT)
(not (NOT-ROW12-ROBOT))
)
)
(when
(and
(Flag134-)
)
(and
(ROW11-ROBOT)
(not (NOT-ROW11-ROBOT))
)
)
(when
(and
(Flag130-)
)
(and
(ROW10-ROBOT)
(not (NOT-ROW10-ROBOT))
)
)
(when
(and
(Flag124-)
)
(and
(ROW9-ROBOT)
(not (NOT-ROW9-ROBOT))
)
)
(when
(and
(Flag116-)
)
(and
(ROW8-ROBOT)
(not (NOT-ROW8-ROBOT))
)
)
(when
(and
(Flag108-)
)
(and
(ROW7-ROBOT)
(not (NOT-ROW7-ROBOT))
)
)
(when
(and
(Flag98-)
)
(and
(ROW6-ROBOT)
(not (NOT-ROW6-ROBOT))
)
)
(when
(and
(Flag88-)
)
(and
(ROW5-ROBOT)
(not (NOT-ROW5-ROBOT))
)
)
(when
(and
(Flag78-)
)
(and
(ROW4-ROBOT)
(not (NOT-ROW4-ROBOT))
)
)
(when
(and
(Flag65-)
)
(and
(ROW3-ROBOT)
(not (NOT-ROW3-ROBOT))
)
)
(when
(and
(Flag52-)
)
(and
(ROW2-ROBOT)
(not (NOT-ROW2-ROBOT))
)
)
(when
(and
(Flag39-)
)
(and
(ROW1-ROBOT)
(not (NOT-ROW1-ROBOT))
)
)
(when
(and
(ROW11-ROBOT)
)
(and
(ROW12-ROBOT)
(NOT-ROW11-ROBOT)
(not (NOT-ROW12-ROBOT))
(not (ROW11-ROBOT))
)
)
(when
(and
(ROW10-ROBOT)
)
(and
(ROW11-ROBOT)
(NOT-ROW10-ROBOT)
(not (NOT-ROW11-ROBOT))
(not (ROW10-ROBOT))
)
)
(when
(and
(ROW9-ROBOT)
)
(and
(ROW10-ROBOT)
(NOT-ROW9-ROBOT)
(not (NOT-ROW10-ROBOT))
(not (ROW9-ROBOT))
)
)
(when
(and
(ROW8-ROBOT)
)
(and
(ROW9-ROBOT)
(NOT-ROW8-ROBOT)
(not (NOT-ROW9-ROBOT))
(not (ROW8-ROBOT))
)
)
(when
(and
(ROW7-ROBOT)
)
(and
(ROW8-ROBOT)
(NOT-ROW7-ROBOT)
(not (NOT-ROW8-ROBOT))
(not (ROW7-ROBOT))
)
)
(when
(and
(ROW6-ROBOT)
)
(and
(ROW7-ROBOT)
(NOT-ROW6-ROBOT)
(not (NOT-ROW7-ROBOT))
(not (ROW6-ROBOT))
)
)
(when
(and
(ROW5-ROBOT)
)
(and
(ROW6-ROBOT)
(NOT-ROW5-ROBOT)
(not (NOT-ROW6-ROBOT))
(not (ROW5-ROBOT))
)
)
(when
(and
(ROW4-ROBOT)
)
(and
(ROW5-ROBOT)
(NOT-ROW4-ROBOT)
(not (NOT-ROW5-ROBOT))
(not (ROW4-ROBOT))
)
)
(when
(and
(ROW3-ROBOT)
)
(and
(ROW4-ROBOT)
(NOT-ROW3-ROBOT)
(not (NOT-ROW4-ROBOT))
(not (ROW3-ROBOT))
)
)
(when
(and
(ROW2-ROBOT)
)
(and
(ROW3-ROBOT)
(NOT-ROW2-ROBOT)
(not (NOT-ROW3-ROBOT))
(not (ROW2-ROBOT))
)
)
(when
(and
(ROW1-ROBOT)
)
(and
(ROW2-ROBOT)
(NOT-ROW1-ROBOT)
(not (NOT-ROW2-ROBOT))
(not (ROW1-ROBOT))
)
)
(when
(and
(ROW0-ROBOT)
)
(and
(ROW1-ROBOT)
(NOT-ROW0-ROBOT)
(not (NOT-ROW1-ROBOT))
(not (ROW0-ROBOT))
)
)
(when
(and
(Flag26-)
)
(and
(BELOWOF13-ROBOT)
(NOT-BELOWOF12-ROBOT)
(not (NOT-BELOWOF13-ROBOT))
(not (BELOWOF12-ROBOT))
)
)
(when
(and
(Flag25-)
)
(and
(BELOWOF12-ROBOT)
(NOT-BELOWOF11-ROBOT)
(not (NOT-BELOWOF12-ROBOT))
(not (BELOWOF11-ROBOT))
)
)
(when
(and
(Flag24-)
)
(and
(BELOWOF11-ROBOT)
(NOT-BELOWOF10-ROBOT)
(not (NOT-BELOWOF11-ROBOT))
(not (BELOWOF10-ROBOT))
)
)
(when
(and
(Flag23-)
)
(and
(BELOWOF10-ROBOT)
(NOT-BELOWOF9-ROBOT)
(not (NOT-BELOWOF10-ROBOT))
(not (BELOWOF9-ROBOT))
)
)
(when
(and
(Flag22-)
)
(and
(BELOWOF9-ROBOT)
(NOT-BELOWOF8-ROBOT)
(not (NOT-BELOWOF9-ROBOT))
(not (BELOWOF8-ROBOT))
)
)
(when
(and
(Flag21-)
)
(and
(BELOWOF8-ROBOT)
(NOT-BELOWOF7-ROBOT)
(not (NOT-BELOWOF8-ROBOT))
(not (BELOWOF7-ROBOT))
)
)
(when
(and
(Flag20-)
)
(and
(BELOWOF7-ROBOT)
(NOT-BELOWOF6-ROBOT)
(not (NOT-BELOWOF7-ROBOT))
(not (BELOWOF6-ROBOT))
)
)
(when
(and
(Flag19-)
)
(and
(BELOWOF6-ROBOT)
(NOT-BELOWOF5-ROBOT)
(not (NOT-BELOWOF6-ROBOT))
(not (BELOWOF5-ROBOT))
)
)
(when
(and
(Flag18-)
)
(and
(BELOWOF5-ROBOT)
(NOT-BELOWOF4-ROBOT)
(not (NOT-BELOWOF5-ROBOT))
(not (BELOWOF4-ROBOT))
)
)
(when
(and
(Flag17-)
)
(and
(BELOWOF4-ROBOT)
(NOT-BELOWOF3-ROBOT)
(not (NOT-BELOWOF4-ROBOT))
(not (BELOWOF3-ROBOT))
)
)
(when
(and
(Flag16-)
)
(and
(BELOWOF3-ROBOT)
(NOT-BELOWOF2-ROBOT)
(not (NOT-BELOWOF3-ROBOT))
(not (BELOWOF2-ROBOT))
)
)
(when
(and
(BELOWOF1-ROBOT)
)
(and
(BELOWOF2-ROBOT)
(NOT-BELOWOF1-ROBOT)
(not (NOT-BELOWOF2-ROBOT))
(not (BELOWOF1-ROBOT))
)
)
(when
(and
(Flag15-)
)
(and
(ABOVEOF12-ROBOT)
(not (NOT-ABOVEOF12-ROBOT))
)
)
(when
(and
(Flag14-)
)
(and
(ABOVEOF11-ROBOT)
(not (NOT-ABOVEOF11-ROBOT))
)
)
(when
(and
(Flag13-)
)
(and
(ABOVEOF10-ROBOT)
(not (NOT-ABOVEOF10-ROBOT))
)
)
(when
(and
(Flag12-)
)
(and
(ABOVEOF9-ROBOT)
(not (NOT-ABOVEOF9-ROBOT))
)
)
(when
(and
(Flag11-)
)
(and
(ABOVEOF8-ROBOT)
(not (NOT-ABOVEOF8-ROBOT))
)
)
(when
(and
(Flag10-)
)
(and
(ABOVEOF7-ROBOT)
(not (NOT-ABOVEOF7-ROBOT))
)
)
(when
(and
(Flag9-)
)
(and
(ABOVEOF6-ROBOT)
(not (NOT-ABOVEOF6-ROBOT))
)
)
(when
(and
(Flag8-)
)
(and
(ABOVEOF5-ROBOT)
(not (NOT-ABOVEOF5-ROBOT))
)
)
(when
(and
(Flag7-)
)
(and
(ABOVEOF4-ROBOT)
(not (NOT-ABOVEOF4-ROBOT))
)
)
(when
(and
(Flag6-)
)
(and
(ABOVEOF3-ROBOT)
(not (NOT-ABOVEOF3-ROBOT))
)
)
(when
(and
(Flag5-)
)
(and
(ABOVEOF2-ROBOT)
(not (NOT-ABOVEOF2-ROBOT))
)
)
(ABOVEOF1-ROBOT)
(CHECKCONSISTENCY-)
(not (NOT-ABOVEOF1-ROBOT))
(not (NOT-CHECKCONSISTENCY-))
)
)
(:derived (Flag2-)(and (RIGHTOF2-ROBOT)))

(:derived (Flag2-)(and (LEFTOF12-ROBOT)))

(:derived (Flag4-)(and (NOT-CHECKCONSISTENCY-)(NOT-ERROR-)))

(:derived (Flag26-)(and (BELOWOF12-ROBOT)))

(:derived (Flag138-)(and (BELOWOF12-ROBOT)))

(:derived (Flag142-)(and (RIGHTOF2-ROBOT)))

(:derived (Flag143-)(and (RIGHTOF2-ROBOT)))

(:derived (Flag144-)(and (RIGHTOF2-ROBOT)))

(:derived (Flag164-)(and (LEFTOF12-ROBOT)))

(:derived (Flag274-)(and (LEFTOF12-ROBOT)))

)
