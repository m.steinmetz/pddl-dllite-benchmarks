(define (domain grounded-SIMPLE-ADL-ROBOT)
(:requirements
:strips
)
(:predicates
(Flag430-)
(Flag427-)
(Flag245-)
(Flag22-)
(Flag21-)
(Flag20-)
(Flag19-)
(Flag3-)
(Flag2-)
(ROW16-ROBOT)
(ROW15-ROBOT)
(ROW14-ROBOT)
(ROW13-ROBOT)
(ROW12-ROBOT)
(ROW11-ROBOT)
(ROW10-ROBOT)
(ROW9-ROBOT)
(ROW8-ROBOT)
(ROW7-ROBOT)
(ROW6-ROBOT)
(ROW5-ROBOT)
(ROW4-ROBOT)
(ROW3-ROBOT)
(ROW2-ROBOT)
(ROW1-ROBOT)
(BELOWOF17-ROBOT)
(NOT-BELOWOF16-ROBOT)
(BELOWOF15-ROBOT)
(BELOWOF14-ROBOT)
(BELOWOF13-ROBOT)
(BELOWOF12-ROBOT)
(BELOWOF11-ROBOT)
(BELOWOF10-ROBOT)
(BELOWOF9-ROBOT)
(BELOWOF8-ROBOT)
(BELOWOF7-ROBOT)
(BELOWOF6-ROBOT)
(BELOWOF5-ROBOT)
(BELOWOF4-ROBOT)
(BELOWOF3-ROBOT)
(BELOWOF2-ROBOT)
(ABOVEOF16-ROBOT)
(ABOVEOF15-ROBOT)
(ABOVEOF14-ROBOT)
(ABOVEOF13-ROBOT)
(ABOVEOF12-ROBOT)
(ABOVEOF11-ROBOT)
(ABOVEOF10-ROBOT)
(ABOVEOF9-ROBOT)
(ABOVEOF8-ROBOT)
(ABOVEOF7-ROBOT)
(ABOVEOF6-ROBOT)
(ABOVEOF5-ROBOT)
(ABOVEOF4-ROBOT)
(ABOVEOF3-ROBOT)
(ABOVEOF2-ROBOT)
(ABOVEOF1-ROBOT)
(CHECKCONSISTENCY-)
(ROW0-ROBOT)
(BELOWOF1-ROBOT)
(Flag431-)
(Flag425-)
(Flag424-)
(Flag422-)
(Flag421-)
(Flag420-)
(Flag418-)
(Flag417-)
(Flag416-)
(Flag415-)
(Flag414-)
(Flag412-)
(Flag411-)
(Flag410-)
(Flag409-)
(Flag408-)
(Flag407-)
(Flag405-)
(Flag404-)
(Flag403-)
(Flag402-)
(Flag401-)
(Flag400-)
(Flag398-)
(Flag397-)
(Flag396-)
(Flag395-)
(Flag394-)
(Flag393-)
(Flag392-)
(Flag390-)
(Flag389-)
(Flag388-)
(Flag387-)
(Flag386-)
(Flag385-)
(Flag384-)
(Flag383-)
(Flag382-)
(Flag380-)
(Flag379-)
(Flag378-)
(Flag377-)
(Flag376-)
(Flag375-)
(Flag374-)
(Flag373-)
(Flag372-)
(Flag371-)
(Flag370-)
(Flag368-)
(Flag367-)
(Flag366-)
(Flag365-)
(Flag364-)
(Flag363-)
(Flag362-)
(Flag361-)
(Flag360-)
(Flag359-)
(Flag358-)
(Flag356-)
(Flag355-)
(Flag354-)
(Flag353-)
(Flag352-)
(Flag351-)
(Flag350-)
(Flag349-)
(Flag348-)
(Flag347-)
(Flag346-)
(Flag345-)
(Flag344-)
(Flag342-)
(Flag341-)
(Flag340-)
(Flag339-)
(Flag338-)
(Flag337-)
(Flag336-)
(Flag335-)
(Flag334-)
(Flag333-)
(Flag332-)
(Flag331-)
(Flag330-)
(Flag329-)
(Flag327-)
(Flag326-)
(Flag325-)
(Flag324-)
(Flag323-)
(Flag322-)
(Flag321-)
(Flag320-)
(Flag319-)
(Flag318-)
(Flag317-)
(Flag316-)
(Flag315-)
(Flag314-)
(Flag312-)
(Flag311-)
(Flag310-)
(Flag309-)
(Flag308-)
(Flag307-)
(Flag306-)
(Flag305-)
(Flag304-)
(Flag303-)
(Flag302-)
(Flag301-)
(Flag300-)
(Flag299-)
(Flag298-)
(Flag297-)
(Flag295-)
(Flag294-)
(Flag293-)
(Flag292-)
(Flag291-)
(Flag290-)
(Flag289-)
(Flag288-)
(Flag287-)
(Flag286-)
(Flag285-)
(Flag284-)
(Flag283-)
(Flag282-)
(Flag281-)
(Flag280-)
(Flag278-)
(Flag277-)
(Flag276-)
(Flag275-)
(Flag274-)
(Flag273-)
(Flag272-)
(Flag271-)
(Flag270-)
(Flag269-)
(Flag268-)
(Flag267-)
(Flag266-)
(Flag265-)
(Flag264-)
(Flag263-)
(Flag262-)
(Flag261-)
(Flag260-)
(Flag259-)
(Flag258-)
(Flag257-)
(Flag256-)
(Flag255-)
(Flag254-)
(Flag253-)
(Flag252-)
(Flag251-)
(Flag250-)
(Flag249-)
(Flag248-)
(Flag247-)
(Flag246-)
(Flag244-)
(Flag243-)
(Flag242-)
(Flag241-)
(Flag240-)
(Flag239-)
(Flag238-)
(Flag237-)
(Flag236-)
(Flag235-)
(Flag234-)
(Flag233-)
(Flag232-)
(Flag231-)
(Flag230-)
(Flag229-)
(Flag228-)
(Flag227-)
(Flag226-)
(Flag225-)
(Flag224-)
(Flag223-)
(Flag222-)
(Flag221-)
(Flag220-)
(Flag219-)
(Flag218-)
(Flag217-)
(Flag216-)
(Flag4-)
(ERROR-)
(COLUMN15-ROBOT)
(COLUMN14-ROBOT)
(COLUMN13-ROBOT)
(COLUMN12-ROBOT)
(COLUMN11-ROBOT)
(COLUMN10-ROBOT)
(COLUMN9-ROBOT)
(COLUMN8-ROBOT)
(COLUMN7-ROBOT)
(COLUMN6-ROBOT)
(COLUMN5-ROBOT)
(COLUMN4-ROBOT)
(COLUMN3-ROBOT)
(COLUMN2-ROBOT)
(COLUMN1-ROBOT)
(COLUMN0-ROBOT)
(RIGHTOF15-ROBOT)
(RIGHTOF14-ROBOT)
(RIGHTOF13-ROBOT)
(RIGHTOF12-ROBOT)
(RIGHTOF11-ROBOT)
(RIGHTOF10-ROBOT)
(RIGHTOF9-ROBOT)
(RIGHTOF8-ROBOT)
(RIGHTOF7-ROBOT)
(RIGHTOF6-ROBOT)
(RIGHTOF5-ROBOT)
(RIGHTOF4-ROBOT)
(RIGHTOF3-ROBOT)
(RIGHTOF1-ROBOT)
(NOT-RIGHTOF2-ROBOT)
(RIGHTOF0-ROBOT)
(LEFTOF15-ROBOT)
(LEFTOF14-ROBOT)
(LEFTOF13-ROBOT)
(LEFTOF12-ROBOT)
(LEFTOF11-ROBOT)
(LEFTOF10-ROBOT)
(LEFTOF9-ROBOT)
(LEFTOF8-ROBOT)
(LEFTOF7-ROBOT)
(LEFTOF6-ROBOT)
(LEFTOF5-ROBOT)
(LEFTOF4-ROBOT)
(LEFTOF3-ROBOT)
(LEFTOF2-ROBOT)
(LEFTOF1-ROBOT)
(COLUMN16-ROBOT)
(LEFTOF17-ROBOT)
(NOT-LEFTOF16-ROBOT)
(RIGHTOF16-ROBOT)
(Flag432-)
(Flag428-)
(Flag426-)
(Flag423-)
(Flag419-)
(Flag413-)
(Flag406-)
(Flag399-)
(Flag391-)
(Flag381-)
(Flag369-)
(Flag357-)
(Flag343-)
(Flag328-)
(Flag313-)
(Flag296-)
(Flag279-)
(Flag214-)
(Flag212-)
(Flag211-)
(Flag210-)
(Flag208-)
(Flag207-)
(Flag206-)
(Flag204-)
(Flag203-)
(Flag202-)
(Flag201-)
(Flag199-)
(Flag198-)
(Flag197-)
(Flag196-)
(Flag195-)
(Flag194-)
(Flag192-)
(Flag191-)
(Flag190-)
(Flag189-)
(Flag188-)
(Flag187-)
(Flag185-)
(Flag184-)
(Flag183-)
(Flag182-)
(Flag181-)
(Flag180-)
(Flag179-)
(Flag178-)
(Flag176-)
(Flag175-)
(Flag174-)
(Flag173-)
(Flag172-)
(Flag171-)
(Flag170-)
(Flag169-)
(Flag167-)
(Flag166-)
(Flag165-)
(Flag164-)
(Flag163-)
(Flag162-)
(Flag161-)
(Flag160-)
(Flag159-)
(Flag158-)
(Flag156-)
(Flag155-)
(Flag154-)
(Flag153-)
(Flag152-)
(Flag151-)
(Flag150-)
(Flag149-)
(Flag148-)
(Flag147-)
(Flag146-)
(Flag144-)
(Flag143-)
(Flag142-)
(Flag141-)
(Flag140-)
(Flag139-)
(Flag138-)
(Flag137-)
(Flag136-)
(Flag135-)
(Flag134-)
(Flag133-)
(Flag132-)
(Flag130-)
(Flag129-)
(Flag128-)
(Flag127-)
(Flag126-)
(Flag125-)
(Flag124-)
(Flag123-)
(Flag122-)
(Flag121-)
(Flag120-)
(Flag119-)
(Flag117-)
(Flag116-)
(Flag115-)
(Flag114-)
(Flag113-)
(Flag112-)
(Flag111-)
(Flag110-)
(Flag109-)
(Flag108-)
(Flag107-)
(Flag106-)
(Flag105-)
(Flag104-)
(Flag103-)
(Flag101-)
(Flag100-)
(Flag99-)
(Flag98-)
(Flag97-)
(Flag96-)
(Flag95-)
(Flag94-)
(Flag93-)
(Flag92-)
(Flag91-)
(Flag90-)
(Flag89-)
(Flag88-)
(Flag87-)
(Flag86-)
(Flag84-)
(Flag83-)
(Flag82-)
(Flag81-)
(Flag80-)
(Flag79-)
(Flag78-)
(Flag77-)
(Flag76-)
(Flag75-)
(Flag74-)
(Flag73-)
(Flag72-)
(Flag71-)
(Flag70-)
(Flag68-)
(Flag67-)
(Flag66-)
(Flag65-)
(Flag64-)
(Flag63-)
(Flag62-)
(Flag61-)
(Flag60-)
(Flag59-)
(Flag58-)
(Flag57-)
(Flag56-)
(Flag55-)
(Flag54-)
(Flag53-)
(Flag52-)
(Flag51-)
(Flag50-)
(Flag49-)
(Flag48-)
(Flag47-)
(Flag46-)
(Flag45-)
(Flag44-)
(Flag43-)
(Flag42-)
(Flag41-)
(Flag40-)
(Flag39-)
(Flag38-)
(Flag37-)
(Flag36-)
(Flag35-)
(Flag34-)
(Flag33-)
(Flag32-)
(Flag31-)
(Flag30-)
(Flag29-)
(Flag28-)
(Flag27-)
(Flag26-)
(Flag25-)
(Flag24-)
(Flag23-)
(Flag18-)
(Flag17-)
(Flag16-)
(Flag15-)
(Flag14-)
(Flag13-)
(Flag12-)
(Flag11-)
(Flag10-)
(Flag9-)
(Flag8-)
(Flag7-)
(Flag6-)
(Flag5-)
(Flag1-)
(Flag429-)
(Flag215-)
(Flag213-)
(Flag209-)
(Flag205-)
(Flag200-)
(Flag193-)
(Flag186-)
(Flag177-)
(Flag168-)
(Flag157-)
(Flag145-)
(Flag131-)
(Flag118-)
(Flag102-)
(Flag85-)
(Flag69-)
(NOT-COLUMN16-ROBOT)
(NOT-COLUMN15-ROBOT)
(NOT-COLUMN14-ROBOT)
(NOT-COLUMN13-ROBOT)
(NOT-COLUMN12-ROBOT)
(NOT-COLUMN11-ROBOT)
(NOT-COLUMN10-ROBOT)
(NOT-COLUMN9-ROBOT)
(NOT-COLUMN8-ROBOT)
(NOT-COLUMN7-ROBOT)
(NOT-COLUMN6-ROBOT)
(NOT-COLUMN5-ROBOT)
(NOT-COLUMN4-ROBOT)
(NOT-COLUMN3-ROBOT)
(NOT-COLUMN2-ROBOT)
(NOT-COLUMN1-ROBOT)
(NOT-LEFTOF17-ROBOT)
(LEFTOF16-ROBOT)
(NOT-LEFTOF15-ROBOT)
(NOT-LEFTOF14-ROBOT)
(NOT-LEFTOF13-ROBOT)
(NOT-LEFTOF12-ROBOT)
(NOT-LEFTOF11-ROBOT)
(NOT-LEFTOF10-ROBOT)
(NOT-LEFTOF9-ROBOT)
(NOT-LEFTOF8-ROBOT)
(NOT-LEFTOF7-ROBOT)
(NOT-LEFTOF6-ROBOT)
(NOT-LEFTOF5-ROBOT)
(NOT-LEFTOF4-ROBOT)
(NOT-LEFTOF3-ROBOT)
(NOT-LEFTOF2-ROBOT)
(NOT-RIGHTOF16-ROBOT)
(NOT-RIGHTOF15-ROBOT)
(NOT-RIGHTOF14-ROBOT)
(NOT-RIGHTOF13-ROBOT)
(NOT-RIGHTOF12-ROBOT)
(NOT-RIGHTOF11-ROBOT)
(NOT-RIGHTOF10-ROBOT)
(NOT-RIGHTOF9-ROBOT)
(NOT-RIGHTOF8-ROBOT)
(NOT-RIGHTOF7-ROBOT)
(NOT-RIGHTOF6-ROBOT)
(NOT-RIGHTOF5-ROBOT)
(NOT-RIGHTOF4-ROBOT)
(NOT-RIGHTOF3-ROBOT)
(NOT-RIGHTOF1-ROBOT)
(NOT-CHECKCONSISTENCY-)
(NOT-COLUMN0-ROBOT)
(RIGHTOF2-ROBOT)
(NOT-RIGHTOF0-ROBOT)
(NOT-LEFTOF1-ROBOT)
(NOT-ERROR-)
(NOT-ROW15-ROBOT)
(NOT-ROW14-ROBOT)
(NOT-ROW13-ROBOT)
(NOT-ROW12-ROBOT)
(NOT-ROW11-ROBOT)
(NOT-ROW10-ROBOT)
(NOT-ROW9-ROBOT)
(NOT-ROW8-ROBOT)
(NOT-ROW7-ROBOT)
(NOT-ROW6-ROBOT)
(NOT-ROW5-ROBOT)
(NOT-ROW4-ROBOT)
(NOT-ROW3-ROBOT)
(NOT-ROW2-ROBOT)
(NOT-ROW1-ROBOT)
(NOT-ROW0-ROBOT)
(NOT-ABOVEOF15-ROBOT)
(NOT-ABOVEOF14-ROBOT)
(NOT-ABOVEOF13-ROBOT)
(NOT-ABOVEOF12-ROBOT)
(NOT-ABOVEOF11-ROBOT)
(NOT-ABOVEOF10-ROBOT)
(NOT-ABOVEOF9-ROBOT)
(NOT-ABOVEOF8-ROBOT)
(NOT-ABOVEOF7-ROBOT)
(NOT-ABOVEOF6-ROBOT)
(NOT-ABOVEOF5-ROBOT)
(NOT-ABOVEOF4-ROBOT)
(NOT-ABOVEOF3-ROBOT)
(NOT-ABOVEOF2-ROBOT)
(NOT-ABOVEOF1-ROBOT)
(NOT-BELOWOF15-ROBOT)
(NOT-BELOWOF14-ROBOT)
(NOT-BELOWOF13-ROBOT)
(NOT-BELOWOF12-ROBOT)
(NOT-BELOWOF11-ROBOT)
(NOT-BELOWOF10-ROBOT)
(NOT-BELOWOF9-ROBOT)
(NOT-BELOWOF8-ROBOT)
(NOT-BELOWOF7-ROBOT)
(NOT-BELOWOF6-ROBOT)
(NOT-BELOWOF5-ROBOT)
(NOT-BELOWOF4-ROBOT)
(NOT-BELOWOF3-ROBOT)
(NOT-BELOWOF2-ROBOT)
(NOT-BELOWOF1-ROBOT)
(NOT-ROW16-ROBOT)
(NOT-BELOWOF17-ROBOT)
(BELOWOF16-ROBOT)
(NOT-ABOVEOF16-ROBOT)
)
(:derived (Flag69-)(and (Flag36-)))

(:derived (Flag69-)(and (Flag37-)))

(:derived (Flag69-)(and (Flag38-)))

(:derived (Flag69-)(and (Flag39-)))

(:derived (Flag69-)(and (Flag40-)))

(:derived (Flag69-)(and (Flag41-)))

(:derived (Flag69-)(and (Flag42-)))

(:derived (Flag69-)(and (Flag43-)))

(:derived (Flag69-)(and (Flag44-)))

(:derived (Flag69-)(and (Flag45-)))

(:derived (Flag69-)(and (Flag46-)))

(:derived (Flag69-)(and (Flag47-)))

(:derived (Flag69-)(and (Flag48-)))

(:derived (Flag69-)(and (Flag49-)))

(:derived (Flag69-)(and (Flag50-)))

(:derived (Flag69-)(and (Flag51-)))

(:derived (Flag69-)(and (Flag52-)))

(:derived (Flag69-)(and (Flag53-)))

(:derived (Flag69-)(and (Flag54-)))

(:derived (Flag69-)(and (Flag55-)))

(:derived (Flag69-)(and (Flag56-)))

(:derived (Flag69-)(and (Flag57-)))

(:derived (Flag69-)(and (Flag58-)))

(:derived (Flag69-)(and (Flag59-)))

(:derived (Flag69-)(and (Flag60-)))

(:derived (Flag69-)(and (Flag61-)))

(:derived (Flag69-)(and (Flag62-)))

(:derived (Flag69-)(and (Flag63-)))

(:derived (Flag69-)(and (Flag64-)))

(:derived (Flag69-)(and (Flag65-)))

(:derived (Flag69-)(and (Flag66-)))

(:derived (Flag69-)(and (Flag67-)))

(:derived (Flag69-)(and (Flag68-)))

(:derived (Flag85-)(and (Flag36-)))

(:derived (Flag85-)(and (Flag37-)))

(:derived (Flag85-)(and (Flag38-)))

(:derived (Flag85-)(and (Flag70-)))

(:derived (Flag85-)(and (Flag39-)))

(:derived (Flag85-)(and (Flag40-)))

(:derived (Flag85-)(and (Flag71-)))

(:derived (Flag85-)(and (Flag72-)))

(:derived (Flag85-)(and (Flag41-)))

(:derived (Flag85-)(and (Flag42-)))

(:derived (Flag85-)(and (Flag73-)))

(:derived (Flag85-)(and (Flag44-)))

(:derived (Flag85-)(and (Flag55-)))

(:derived (Flag85-)(and (Flag47-)))

(:derived (Flag85-)(and (Flag48-)))

(:derived (Flag85-)(and (Flag74-)))

(:derived (Flag85-)(and (Flag49-)))

(:derived (Flag85-)(and (Flag50-)))

(:derived (Flag85-)(and (Flag57-)))

(:derived (Flag85-)(and (Flag75-)))

(:derived (Flag85-)(and (Flag51-)))

(:derived (Flag85-)(and (Flag52-)))

(:derived (Flag85-)(and (Flag53-)))

(:derived (Flag85-)(and (Flag54-)))

(:derived (Flag85-)(and (Flag76-)))

(:derived (Flag85-)(and (Flag77-)))

(:derived (Flag85-)(and (Flag46-)))

(:derived (Flag85-)(and (Flag56-)))

(:derived (Flag85-)(and (Flag78-)))

(:derived (Flag85-)(and (Flag79-)))

(:derived (Flag85-)(and (Flag58-)))

(:derived (Flag85-)(and (Flag80-)))

(:derived (Flag85-)(and (Flag81-)))

(:derived (Flag85-)(and (Flag82-)))

(:derived (Flag85-)(and (Flag59-)))

(:derived (Flag85-)(and (Flag60-)))

(:derived (Flag85-)(and (Flag61-)))

(:derived (Flag85-)(and (Flag62-)))

(:derived (Flag85-)(and (Flag63-)))

(:derived (Flag85-)(and (Flag64-)))

(:derived (Flag85-)(and (Flag65-)))

(:derived (Flag85-)(and (Flag66-)))

(:derived (Flag85-)(and (Flag67-)))

(:derived (Flag85-)(and (Flag83-)))

(:derived (Flag85-)(and (Flag68-)))

(:derived (Flag85-)(and (Flag84-)))

(:derived (Flag102-)(and (Flag36-)))

(:derived (Flag102-)(and (Flag37-)))

(:derived (Flag102-)(and (Flag38-)))

(:derived (Flag102-)(and (Flag70-)))

(:derived (Flag102-)(and (Flag39-)))

(:derived (Flag102-)(and (Flag40-)))

(:derived (Flag102-)(and (Flag71-)))

(:derived (Flag102-)(and (Flag65-)))

(:derived (Flag102-)(and (Flag86-)))

(:derived (Flag102-)(and (Flag72-)))

(:derived (Flag102-)(and (Flag41-)))

(:derived (Flag102-)(and (Flag42-)))

(:derived (Flag102-)(and (Flag87-)))

(:derived (Flag102-)(and (Flag73-)))

(:derived (Flag102-)(and (Flag88-)))

(:derived (Flag102-)(and (Flag44-)))

(:derived (Flag102-)(and (Flag77-)))

(:derived (Flag102-)(and (Flag55-)))

(:derived (Flag102-)(and (Flag47-)))

(:derived (Flag102-)(and (Flag48-)))

(:derived (Flag102-)(and (Flag89-)))

(:derived (Flag102-)(and (Flag74-)))

(:derived (Flag102-)(and (Flag49-)))

(:derived (Flag102-)(and (Flag50-)))

(:derived (Flag102-)(and (Flag57-)))

(:derived (Flag102-)(and (Flag75-)))

(:derived (Flag102-)(and (Flag51-)))

(:derived (Flag102-)(and (Flag52-)))

(:derived (Flag102-)(and (Flag90-)))

(:derived (Flag102-)(and (Flag53-)))

(:derived (Flag102-)(and (Flag54-)))

(:derived (Flag102-)(and (Flag76-)))

(:derived (Flag102-)(and (Flag91-)))

(:derived (Flag102-)(and (Flag46-)))

(:derived (Flag102-)(and (Flag56-)))

(:derived (Flag102-)(and (Flag92-)))

(:derived (Flag102-)(and (Flag66-)))

(:derived (Flag102-)(and (Flag78-)))

(:derived (Flag102-)(and (Flag79-)))

(:derived (Flag102-)(and (Flag58-)))

(:derived (Flag102-)(and (Flag81-)))

(:derived (Flag102-)(and (Flag93-)))

(:derived (Flag102-)(and (Flag94-)))

(:derived (Flag102-)(and (Flag82-)))

(:derived (Flag102-)(and (Flag95-)))

(:derived (Flag102-)(and (Flag96-)))

(:derived (Flag102-)(and (Flag59-)))

(:derived (Flag102-)(and (Flag60-)))

(:derived (Flag102-)(and (Flag97-)))

(:derived (Flag102-)(and (Flag98-)))

(:derived (Flag102-)(and (Flag61-)))

(:derived (Flag102-)(and (Flag99-)))

(:derived (Flag102-)(and (Flag64-)))

(:derived (Flag102-)(and (Flag100-)))

(:derived (Flag102-)(and (Flag101-)))

(:derived (Flag102-)(and (Flag67-)))

(:derived (Flag102-)(and (Flag83-)))

(:derived (Flag102-)(and (Flag68-)))

(:derived (Flag118-)(and (Flag103-)))

(:derived (Flag118-)(and (Flag104-)))

(:derived (Flag118-)(and (Flag37-)))

(:derived (Flag118-)(and (Flag105-)))

(:derived (Flag118-)(and (Flag38-)))

(:derived (Flag118-)(and (Flag70-)))

(:derived (Flag118-)(and (Flag39-)))

(:derived (Flag118-)(and (Flag71-)))

(:derived (Flag118-)(and (Flag65-)))

(:derived (Flag118-)(and (Flag86-)))

(:derived (Flag118-)(and (Flag106-)))

(:derived (Flag118-)(and (Flag72-)))

(:derived (Flag118-)(and (Flag41-)))

(:derived (Flag118-)(and (Flag42-)))

(:derived (Flag118-)(and (Flag58-)))

(:derived (Flag118-)(and (Flag107-)))

(:derived (Flag118-)(and (Flag87-)))

(:derived (Flag118-)(and (Flag108-)))

(:derived (Flag118-)(and (Flag73-)))

(:derived (Flag118-)(and (Flag36-)))

(:derived (Flag118-)(and (Flag88-)))

(:derived (Flag118-)(and (Flag44-)))

(:derived (Flag118-)(and (Flag77-)))

(:derived (Flag118-)(and (Flag55-)))

(:derived (Flag118-)(and (Flag47-)))

(:derived (Flag118-)(and (Flag48-)))

(:derived (Flag118-)(and (Flag89-)))

(:derived (Flag118-)(and (Flag74-)))

(:derived (Flag118-)(and (Flag49-)))

(:derived (Flag118-)(and (Flag90-)))

(:derived (Flag118-)(and (Flag79-)))

(:derived (Flag118-)(and (Flag109-)))

(:derived (Flag118-)(and (Flag110-)))

(:derived (Flag118-)(and (Flag111-)))

(:derived (Flag118-)(and (Flag52-)))

(:derived (Flag118-)(and (Flag112-)))

(:derived (Flag118-)(and (Flag53-)))

(:derived (Flag118-)(and (Flag54-)))

(:derived (Flag118-)(and (Flag76-)))

(:derived (Flag118-)(and (Flag113-)))

(:derived (Flag118-)(and (Flag46-)))

(:derived (Flag118-)(and (Flag56-)))

(:derived (Flag118-)(and (Flag92-)))

(:derived (Flag118-)(and (Flag114-)))

(:derived (Flag118-)(and (Flag115-)))

(:derived (Flag118-)(and (Flag66-)))

(:derived (Flag118-)(and (Flag57-)))

(:derived (Flag118-)(and (Flag75-)))

(:derived (Flag118-)(and (Flag51-)))

(:derived (Flag118-)(and (Flag81-)))

(:derived (Flag118-)(and (Flag93-)))

(:derived (Flag118-)(and (Flag94-)))

(:derived (Flag118-)(and (Flag82-)))

(:derived (Flag118-)(and (Flag95-)))

(:derived (Flag118-)(and (Flag96-)))

(:derived (Flag118-)(and (Flag59-)))

(:derived (Flag118-)(and (Flag60-)))

(:derived (Flag118-)(and (Flag97-)))

(:derived (Flag118-)(and (Flag98-)))

(:derived (Flag118-)(and (Flag61-)))

(:derived (Flag118-)(and (Flag99-)))

(:derived (Flag118-)(and (Flag116-)))

(:derived (Flag118-)(and (Flag78-)))

(:derived (Flag118-)(and (Flag64-)))

(:derived (Flag118-)(and (Flag117-)))

(:derived (Flag118-)(and (Flag101-)))

(:derived (Flag118-)(and (Flag67-)))

(:derived (Flag118-)(and (Flag83-)))

(:derived (Flag118-)(and (Flag68-)))

(:derived (Flag131-)(and (Flag60-)))

(:derived (Flag131-)(and (Flag104-)))

(:derived (Flag131-)(and (Flag37-)))

(:derived (Flag131-)(and (Flag103-)))

(:derived (Flag131-)(and (Flag38-)))

(:derived (Flag131-)(and (Flag106-)))

(:derived (Flag131-)(and (Flag39-)))

(:derived (Flag131-)(and (Flag71-)))

(:derived (Flag131-)(and (Flag65-)))

(:derived (Flag131-)(and (Flag86-)))

(:derived (Flag131-)(and (Flag83-)))

(:derived (Flag131-)(and (Flag41-)))

(:derived (Flag131-)(and (Flag42-)))

(:derived (Flag131-)(and (Flag108-)))

(:derived (Flag131-)(and (Flag73-)))

(:derived (Flag131-)(and (Flag36-)))

(:derived (Flag131-)(and (Flag88-)))

(:derived (Flag131-)(and (Flag44-)))

(:derived (Flag131-)(and (Flag109-)))

(:derived (Flag131-)(and (Flag77-)))

(:derived (Flag131-)(and (Flag119-)))

(:derived (Flag131-)(and (Flag46-)))

(:derived (Flag131-)(and (Flag47-)))

(:derived (Flag131-)(and (Flag99-)))

(:derived (Flag131-)(and (Flag120-)))

(:derived (Flag131-)(and (Flag89-)))

(:derived (Flag131-)(and (Flag74-)))

(:derived (Flag131-)(and (Flag49-)))

(:derived (Flag131-)(and (Flag90-)))

(:derived (Flag131-)(and (Flag121-)))

(:derived (Flag131-)(and (Flag75-)))

(:derived (Flag131-)(and (Flag122-)))

(:derived (Flag131-)(and (Flag110-)))

(:derived (Flag131-)(and (Flag70-)))

(:derived (Flag131-)(and (Flag48-)))

(:derived (Flag131-)(and (Flag111-)))

(:derived (Flag131-)(and (Flag112-)))

(:derived (Flag131-)(and (Flag53-)))

(:derived (Flag131-)(and (Flag54-)))

(:derived (Flag131-)(and (Flag76-)))

(:derived (Flag131-)(and (Flag113-)))

(:derived (Flag131-)(and (Flag55-)))

(:derived (Flag131-)(and (Flag56-)))

(:derived (Flag131-)(and (Flag92-)))

(:derived (Flag131-)(and (Flag114-)))

(:derived (Flag131-)(and (Flag115-)))

(:derived (Flag131-)(and (Flag51-)))

(:derived (Flag131-)(and (Flag66-)))

(:derived (Flag131-)(and (Flag78-)))

(:derived (Flag131-)(and (Flag123-)))

(:derived (Flag131-)(and (Flag79-)))

(:derived (Flag131-)(and (Flag58-)))

(:derived (Flag131-)(and (Flag124-)))

(:derived (Flag131-)(and (Flag81-)))

(:derived (Flag131-)(and (Flag93-)))

(:derived (Flag131-)(and (Flag94-)))

(:derived (Flag131-)(and (Flag125-)))

(:derived (Flag131-)(and (Flag82-)))

(:derived (Flag131-)(and (Flag126-)))

(:derived (Flag131-)(and (Flag127-)))

(:derived (Flag131-)(and (Flag95-)))

(:derived (Flag131-)(and (Flag96-)))

(:derived (Flag131-)(and (Flag59-)))

(:derived (Flag131-)(and (Flag128-)))

(:derived (Flag131-)(and (Flag97-)))

(:derived (Flag131-)(and (Flag98-)))

(:derived (Flag131-)(and (Flag61-)))

(:derived (Flag131-)(and (Flag129-)))

(:derived (Flag131-)(and (Flag116-)))

(:derived (Flag131-)(and (Flag64-)))

(:derived (Flag131-)(and (Flag117-)))

(:derived (Flag131-)(and (Flag101-)))

(:derived (Flag131-)(and (Flag67-)))

(:derived (Flag131-)(and (Flag107-)))

(:derived (Flag131-)(and (Flag130-)))

(:derived (Flag145-)(and (Flag60-)))

(:derived (Flag145-)(and (Flag104-)))

(:derived (Flag145-)(and (Flag37-)))

(:derived (Flag145-)(and (Flag103-)))

(:derived (Flag145-)(and (Flag38-)))

(:derived (Flag145-)(and (Flag106-)))

(:derived (Flag145-)(and (Flag39-)))

(:derived (Flag145-)(and (Flag132-)))

(:derived (Flag145-)(and (Flag97-)))

(:derived (Flag145-)(and (Flag133-)))

(:derived (Flag145-)(and (Flag134-)))

(:derived (Flag145-)(and (Flag86-)))

(:derived (Flag145-)(and (Flag83-)))

(:derived (Flag145-)(and (Flag41-)))

(:derived (Flag145-)(and (Flag42-)))

(:derived (Flag145-)(and (Flag54-)))

(:derived (Flag145-)(and (Flag71-)))

(:derived (Flag145-)(and (Flag108-)))

(:derived (Flag145-)(and (Flag73-)))

(:derived (Flag145-)(and (Flag36-)))

(:derived (Flag145-)(and (Flag88-)))

(:derived (Flag145-)(and (Flag96-)))

(:derived (Flag145-)(and (Flag74-)))

(:derived (Flag145-)(and (Flag113-)))

(:derived (Flag145-)(and (Flag119-)))

(:derived (Flag145-)(and (Flag135-)))

(:derived (Flag145-)(and (Flag46-)))

(:derived (Flag145-)(and (Flag47-)))

(:derived (Flag145-)(and (Flag99-)))

(:derived (Flag145-)(and (Flag120-)))

(:derived (Flag145-)(and (Flag89-)))

(:derived (Flag145-)(and (Flag121-)))

(:derived (Flag145-)(and (Flag49-)))

(:derived (Flag145-)(and (Flag90-)))

(:derived (Flag145-)(and (Flag114-)))

(:derived (Flag145-)(and (Flag75-)))

(:derived (Flag145-)(and (Flag122-)))

(:derived (Flag145-)(and (Flag110-)))

(:derived (Flag145-)(and (Flag70-)))

(:derived (Flag145-)(and (Flag111-)))

(:derived (Flag145-)(and (Flag112-)))

(:derived (Flag145-)(and (Flag53-)))

(:derived (Flag145-)(and (Flag136-)))

(:derived (Flag145-)(and (Flag76-)))

(:derived (Flag145-)(and (Flag77-)))

(:derived (Flag145-)(and (Flag55-)))

(:derived (Flag145-)(and (Flag56-)))

(:derived (Flag145-)(and (Flag137-)))

(:derived (Flag145-)(and (Flag138-)))

(:derived (Flag145-)(and (Flag79-)))

(:derived (Flag145-)(and (Flag115-)))

(:derived (Flag145-)(and (Flag139-)))

(:derived (Flag145-)(and (Flag51-)))

(:derived (Flag145-)(and (Flag66-)))

(:derived (Flag145-)(and (Flag92-)))

(:derived (Flag145-)(and (Flag58-)))

(:derived (Flag145-)(and (Flag124-)))

(:derived (Flag145-)(and (Flag81-)))

(:derived (Flag145-)(and (Flag93-)))

(:derived (Flag145-)(and (Flag94-)))

(:derived (Flag145-)(and (Flag125-)))

(:derived (Flag145-)(and (Flag82-)))

(:derived (Flag145-)(and (Flag109-)))

(:derived (Flag145-)(and (Flag140-)))

(:derived (Flag145-)(and (Flag141-)))

(:derived (Flag145-)(and (Flag142-)))

(:derived (Flag145-)(and (Flag126-)))

(:derived (Flag145-)(and (Flag95-)))

(:derived (Flag145-)(and (Flag59-)))

(:derived (Flag145-)(and (Flag128-)))

(:derived (Flag145-)(and (Flag143-)))

(:derived (Flag145-)(and (Flag61-)))

(:derived (Flag145-)(and (Flag129-)))

(:derived (Flag145-)(and (Flag116-)))

(:derived (Flag145-)(and (Flag64-)))

(:derived (Flag145-)(and (Flag65-)))

(:derived (Flag145-)(and (Flag101-)))

(:derived (Flag145-)(and (Flag67-)))

(:derived (Flag145-)(and (Flag107-)))

(:derived (Flag145-)(and (Flag130-)))

(:derived (Flag145-)(and (Flag144-)))

(:derived (Flag157-)(and (Flag60-)))

(:derived (Flag157-)(and (Flag104-)))

(:derived (Flag157-)(and (Flag37-)))

(:derived (Flag157-)(and (Flag103-)))

(:derived (Flag157-)(and (Flag146-)))

(:derived (Flag157-)(and (Flag106-)))

(:derived (Flag157-)(and (Flag39-)))

(:derived (Flag157-)(and (Flag112-)))

(:derived (Flag157-)(and (Flag132-)))

(:derived (Flag157-)(and (Flag135-)))

(:derived (Flag157-)(and (Flag133-)))

(:derived (Flag157-)(and (Flag86-)))

(:derived (Flag157-)(and (Flag81-)))

(:derived (Flag157-)(and (Flag53-)))

(:derived (Flag157-)(and (Flag147-)))

(:derived (Flag157-)(and (Flag41-)))

(:derived (Flag157-)(and (Flag42-)))

(:derived (Flag157-)(and (Flag107-)))

(:derived (Flag157-)(and (Flag71-)))

(:derived (Flag157-)(and (Flag74-)))

(:derived (Flag157-)(and (Flag108-)))

(:derived (Flag157-)(and (Flag73-)))

(:derived (Flag157-)(and (Flag36-)))

(:derived (Flag157-)(and (Flag88-)))

(:derived (Flag157-)(and (Flag96-)))

(:derived (Flag157-)(and (Flag148-)))

(:derived (Flag157-)(and (Flag113-)))

(:derived (Flag157-)(and (Flag119-)))

(:derived (Flag157-)(and (Flag109-)))

(:derived (Flag157-)(and (Flag55-)))

(:derived (Flag157-)(and (Flag47-)))

(:derived (Flag157-)(and (Flag99-)))

(:derived (Flag157-)(and (Flag120-)))

(:derived (Flag157-)(and (Flag89-)))

(:derived (Flag157-)(and (Flag121-)))

(:derived (Flag157-)(and (Flag49-)))

(:derived (Flag157-)(and (Flag90-)))

(:derived (Flag157-)(and (Flag114-)))

(:derived (Flag157-)(and (Flag79-)))

(:derived (Flag157-)(and (Flag122-)))

(:derived (Flag157-)(and (Flag110-)))

(:derived (Flag157-)(and (Flag149-)))

(:derived (Flag157-)(and (Flag150-)))

(:derived (Flag157-)(and (Flag151-)))

(:derived (Flag157-)(and (Flag152-)))

(:derived (Flag157-)(and (Flag153-)))

(:derived (Flag157-)(and (Flag54-)))

(:derived (Flag157-)(and (Flag76-)))

(:derived (Flag157-)(and (Flag77-)))

(:derived (Flag157-)(and (Flag46-)))

(:derived (Flag157-)(and (Flag56-)))

(:derived (Flag157-)(and (Flag137-)))

(:derived (Flag157-)(and (Flag138-)))

(:derived (Flag157-)(and (Flag115-)))

(:derived (Flag157-)(and (Flag139-)))

(:derived (Flag157-)(and (Flag51-)))

(:derived (Flag157-)(and (Flag66-)))

(:derived (Flag157-)(and (Flag154-)))

(:derived (Flag157-)(and (Flag92-)))

(:derived (Flag157-)(and (Flag58-)))

(:derived (Flag157-)(and (Flag124-)))

(:derived (Flag157-)(and (Flag38-)))

(:derived (Flag157-)(and (Flag93-)))

(:derived (Flag157-)(and (Flag94-)))

(:derived (Flag157-)(and (Flag125-)))

(:derived (Flag157-)(and (Flag82-)))

(:derived (Flag157-)(and (Flag140-)))

(:derived (Flag157-)(and (Flag142-)))

(:derived (Flag157-)(and (Flag126-)))

(:derived (Flag157-)(and (Flag95-)))

(:derived (Flag157-)(and (Flag59-)))

(:derived (Flag157-)(and (Flag128-)))

(:derived (Flag157-)(and (Flag143-)))

(:derived (Flag157-)(and (Flag155-)))

(:derived (Flag157-)(and (Flag61-)))

(:derived (Flag157-)(and (Flag129-)))

(:derived (Flag157-)(and (Flag156-)))

(:derived (Flag157-)(and (Flag116-)))

(:derived (Flag157-)(and (Flag75-)))

(:derived (Flag157-)(and (Flag65-)))

(:derived (Flag157-)(and (Flag101-)))

(:derived (Flag157-)(and (Flag83-)))

(:derived (Flag157-)(and (Flag130-)))

(:derived (Flag157-)(and (Flag144-)))

(:derived (Flag168-)(and (Flag60-)))

(:derived (Flag168-)(and (Flag104-)))

(:derived (Flag168-)(and (Flag37-)))

(:derived (Flag168-)(and (Flag103-)))

(:derived (Flag168-)(and (Flag101-)))

(:derived (Flag168-)(and (Flag38-)))

(:derived (Flag168-)(and (Flag39-)))

(:derived (Flag168-)(and (Flag112-)))

(:derived (Flag168-)(and (Flag132-)))

(:derived (Flag168-)(and (Flag71-)))

(:derived (Flag168-)(and (Flag133-)))

(:derived (Flag168-)(and (Flag86-)))

(:derived (Flag168-)(and (Flag158-)))

(:derived (Flag168-)(and (Flag53-)))

(:derived (Flag168-)(and (Flag147-)))

(:derived (Flag168-)(and (Flag41-)))

(:derived (Flag168-)(and (Flag42-)))

(:derived (Flag168-)(and (Flag159-)))

(:derived (Flag168-)(and (Flag107-)))

(:derived (Flag168-)(and (Flag160-)))

(:derived (Flag168-)(and (Flag161-)))

(:derived (Flag168-)(and (Flag155-)))

(:derived (Flag168-)(and (Flag108-)))

(:derived (Flag168-)(and (Flag73-)))

(:derived (Flag168-)(and (Flag36-)))

(:derived (Flag168-)(and (Flag96-)))

(:derived (Flag168-)(and (Flag109-)))

(:derived (Flag168-)(and (Flag77-)))

(:derived (Flag168-)(and (Flag162-)))

(:derived (Flag168-)(and (Flag135-)))

(:derived (Flag168-)(and (Flag138-)))

(:derived (Flag168-)(and (Flag47-)))

(:derived (Flag168-)(and (Flag99-)))

(:derived (Flag168-)(and (Flag120-)))

(:derived (Flag168-)(and (Flag89-)))

(:derived (Flag168-)(and (Flag121-)))

(:derived (Flag168-)(and (Flag49-)))

(:derived (Flag168-)(and (Flag90-)))

(:derived (Flag168-)(and (Flag79-)))

(:derived (Flag168-)(and (Flag122-)))

(:derived (Flag168-)(and (Flag110-)))

(:derived (Flag168-)(and (Flag163-)))

(:derived (Flag168-)(and (Flag150-)))

(:derived (Flag168-)(and (Flag151-)))

(:derived (Flag168-)(and (Flag153-)))

(:derived (Flag168-)(and (Flag54-)))

(:derived (Flag168-)(and (Flag76-)))

(:derived (Flag168-)(and (Flag113-)))

(:derived (Flag168-)(and (Flag164-)))

(:derived (Flag168-)(and (Flag56-)))

(:derived (Flag168-)(and (Flag137-)))

(:derived (Flag168-)(and (Flag114-)))

(:derived (Flag168-)(and (Flag165-)))

(:derived (Flag168-)(and (Flag115-)))

(:derived (Flag168-)(and (Flag139-)))

(:derived (Flag168-)(and (Flag51-)))

(:derived (Flag168-)(and (Flag66-)))

(:derived (Flag168-)(and (Flag166-)))

(:derived (Flag168-)(and (Flag154-)))

(:derived (Flag168-)(and (Flag149-)))

(:derived (Flag168-)(and (Flag58-)))

(:derived (Flag168-)(and (Flag124-)))

(:derived (Flag168-)(and (Flag81-)))

(:derived (Flag168-)(and (Flag93-)))

(:derived (Flag168-)(and (Flag94-)))

(:derived (Flag168-)(and (Flag82-)))

(:derived (Flag168-)(and (Flag140-)))

(:derived (Flag168-)(and (Flag142-)))

(:derived (Flag168-)(and (Flag126-)))

(:derived (Flag168-)(and (Flag95-)))

(:derived (Flag168-)(and (Flag59-)))

(:derived (Flag168-)(and (Flag128-)))

(:derived (Flag168-)(and (Flag143-)))

(:derived (Flag168-)(and (Flag148-)))

(:derived (Flag168-)(and (Flag61-)))

(:derived (Flag168-)(and (Flag129-)))

(:derived (Flag168-)(and (Flag156-)))

(:derived (Flag168-)(and (Flag116-)))

(:derived (Flag168-)(and (Flag75-)))

(:derived (Flag168-)(and (Flag65-)))

(:derived (Flag168-)(and (Flag167-)))

(:derived (Flag168-)(and (Flag119-)))

(:derived (Flag168-)(and (Flag83-)))

(:derived (Flag168-)(and (Flag130-)))

(:derived (Flag168-)(and (Flag144-)))

(:derived (Flag177-)(and (Flag60-)))

(:derived (Flag177-)(and (Flag104-)))

(:derived (Flag177-)(and (Flag37-)))

(:derived (Flag177-)(and (Flag103-)))

(:derived (Flag177-)(and (Flag101-)))

(:derived (Flag177-)(and (Flag38-)))

(:derived (Flag177-)(and (Flag39-)))

(:derived (Flag177-)(and (Flag41-)))

(:derived (Flag177-)(and (Flag169-)))

(:derived (Flag177-)(and (Flag135-)))

(:derived (Flag177-)(and (Flag147-)))

(:derived (Flag177-)(and (Flag170-)))

(:derived (Flag177-)(and (Flag171-)))

(:derived (Flag177-)(and (Flag159-)))

(:derived (Flag177-)(and (Flag158-)))

(:derived (Flag177-)(and (Flag53-)))

(:derived (Flag177-)(and (Flag172-)))

(:derived (Flag177-)(and (Flag173-)))

(:derived (Flag177-)(and (Flag42-)))

(:derived (Flag177-)(and (Flag160-)))

(:derived (Flag177-)(and (Flag71-)))

(:derived (Flag177-)(and (Flag155-)))

(:derived (Flag177-)(and (Flag108-)))

(:derived (Flag177-)(and (Flag73-)))

(:derived (Flag177-)(and (Flag36-)))

(:derived (Flag177-)(and (Flag96-)))

(:derived (Flag177-)(and (Flag109-)))

(:derived (Flag177-)(and (Flag116-)))

(:derived (Flag177-)(and (Flag162-)))

(:derived (Flag177-)(and (Flag174-)))

(:derived (Flag177-)(and (Flag138-)))

(:derived (Flag177-)(and (Flag47-)))

(:derived (Flag177-)(and (Flag99-)))

(:derived (Flag177-)(and (Flag120-)))

(:derived (Flag177-)(and (Flag89-)))

(:derived (Flag177-)(and (Flag49-)))

(:derived (Flag177-)(and (Flag90-)))

(:derived (Flag177-)(and (Flag79-)))

(:derived (Flag177-)(and (Flag122-)))

(:derived (Flag177-)(and (Flag163-)))

(:derived (Flag177-)(and (Flag150-)))

(:derived (Flag177-)(and (Flag112-)))

(:derived (Flag177-)(and (Flag153-)))

(:derived (Flag177-)(and (Flag54-)))

(:derived (Flag177-)(and (Flag76-)))

(:derived (Flag177-)(and (Flag113-)))

(:derived (Flag177-)(and (Flag164-)))

(:derived (Flag177-)(and (Flag56-)))

(:derived (Flag177-)(and (Flag137-)))

(:derived (Flag177-)(and (Flag114-)))

(:derived (Flag177-)(and (Flag165-)))

(:derived (Flag177-)(and (Flag139-)))

(:derived (Flag177-)(and (Flag175-)))

(:derived (Flag177-)(and (Flag66-)))

(:derived (Flag177-)(and (Flag166-)))

(:derived (Flag177-)(and (Flag154-)))

(:derived (Flag177-)(and (Flag149-)))

(:derived (Flag177-)(and (Flag51-)))

(:derived (Flag177-)(and (Flag124-)))

(:derived (Flag177-)(and (Flag81-)))

(:derived (Flag177-)(and (Flag93-)))

(:derived (Flag177-)(and (Flag94-)))

(:derived (Flag177-)(and (Flag82-)))

(:derived (Flag177-)(and (Flag140-)))

(:derived (Flag177-)(and (Flag142-)))

(:derived (Flag177-)(and (Flag126-)))

(:derived (Flag177-)(and (Flag86-)))

(:derived (Flag177-)(and (Flag95-)))

(:derived (Flag177-)(and (Flag132-)))

(:derived (Flag177-)(and (Flag59-)))

(:derived (Flag177-)(and (Flag128-)))

(:derived (Flag177-)(and (Flag176-)))

(:derived (Flag177-)(and (Flag143-)))

(:derived (Flag177-)(and (Flag148-)))

(:derived (Flag177-)(and (Flag129-)))

(:derived (Flag177-)(and (Flag156-)))

(:derived (Flag177-)(and (Flag75-)))

(:derived (Flag177-)(and (Flag83-)))

(:derived (Flag177-)(and (Flag65-)))

(:derived (Flag177-)(and (Flag167-)))

(:derived (Flag177-)(and (Flag119-)))

(:derived (Flag177-)(and (Flag107-)))

(:derived (Flag177-)(and (Flag130-)))

(:derived (Flag177-)(and (Flag144-)))

(:derived (Flag186-)(and (Flag60-)))

(:derived (Flag186-)(and (Flag104-)))

(:derived (Flag186-)(and (Flag37-)))

(:derived (Flag186-)(and (Flag178-)))

(:derived (Flag186-)(and (Flag132-)))

(:derived (Flag186-)(and (Flag124-)))

(:derived (Flag186-)(and (Flag41-)))

(:derived (Flag186-)(and (Flag169-)))

(:derived (Flag186-)(and (Flag179-)))

(:derived (Flag186-)(and (Flag170-)))

(:derived (Flag186-)(and (Flag171-)))

(:derived (Flag186-)(and (Flag159-)))

(:derived (Flag186-)(and (Flag158-)))

(:derived (Flag186-)(and (Flag53-)))

(:derived (Flag186-)(and (Flag172-)))

(:derived (Flag186-)(and (Flag173-)))

(:derived (Flag186-)(and (Flag42-)))

(:derived (Flag186-)(and (Flag107-)))

(:derived (Flag186-)(and (Flag66-)))

(:derived (Flag186-)(and (Flag160-)))

(:derived (Flag186-)(and (Flag71-)))

(:derived (Flag186-)(and (Flag155-)))

(:derived (Flag186-)(and (Flag180-)))

(:derived (Flag186-)(and (Flag73-)))

(:derived (Flag186-)(and (Flag36-)))

(:derived (Flag186-)(and (Flag96-)))

(:derived (Flag186-)(and (Flag109-)))

(:derived (Flag186-)(and (Flag116-)))

(:derived (Flag186-)(and (Flag119-)))

(:derived (Flag186-)(and (Flag135-)))

(:derived (Flag186-)(and (Flag138-)))

(:derived (Flag186-)(and (Flag47-)))

(:derived (Flag186-)(and (Flag120-)))

(:derived (Flag186-)(and (Flag89-)))

(:derived (Flag186-)(and (Flag49-)))

(:derived (Flag186-)(and (Flag90-)))

(:derived (Flag186-)(and (Flag79-)))

(:derived (Flag186-)(and (Flag122-)))

(:derived (Flag186-)(and (Flag181-)))

(:derived (Flag186-)(and (Flag163-)))

(:derived (Flag186-)(and (Flag150-)))

(:derived (Flag186-)(and (Flag112-)))

(:derived (Flag186-)(and (Flag153-)))

(:derived (Flag186-)(and (Flag54-)))

(:derived (Flag186-)(and (Flag76-)))

(:derived (Flag186-)(and (Flag113-)))

(:derived (Flag186-)(and (Flag164-)))

(:derived (Flag186-)(and (Flag56-)))

(:derived (Flag186-)(and (Flag137-)))

(:derived (Flag186-)(and (Flag114-)))

(:derived (Flag186-)(and (Flag165-)))

(:derived (Flag186-)(and (Flag139-)))

(:derived (Flag186-)(and (Flag175-)))

(:derived (Flag186-)(and (Flag182-)))

(:derived (Flag186-)(and (Flag166-)))

(:derived (Flag186-)(and (Flag154-)))

(:derived (Flag186-)(and (Flag149-)))

(:derived (Flag186-)(and (Flag51-)))

(:derived (Flag186-)(and (Flag81-)))

(:derived (Flag186-)(and (Flag93-)))

(:derived (Flag186-)(and (Flag101-)))

(:derived (Flag186-)(and (Flag82-)))

(:derived (Flag186-)(and (Flag142-)))

(:derived (Flag186-)(and (Flag126-)))

(:derived (Flag186-)(and (Flag86-)))

(:derived (Flag186-)(and (Flag183-)))

(:derived (Flag186-)(and (Flag59-)))

(:derived (Flag186-)(and (Flag128-)))

(:derived (Flag186-)(and (Flag176-)))

(:derived (Flag186-)(and (Flag143-)))

(:derived (Flag186-)(and (Flag148-)))

(:derived (Flag186-)(and (Flag184-)))

(:derived (Flag186-)(and (Flag99-)))

(:derived (Flag186-)(and (Flag156-)))

(:derived (Flag186-)(and (Flag75-)))

(:derived (Flag186-)(and (Flag65-)))

(:derived (Flag186-)(and (Flag167-)))

(:derived (Flag186-)(and (Flag108-)))

(:derived (Flag186-)(and (Flag185-)))

(:derived (Flag186-)(and (Flag129-)))

(:derived (Flag186-)(and (Flag144-)))

(:derived (Flag193-)(and (Flag178-)))

(:derived (Flag193-)(and (Flag104-)))

(:derived (Flag193-)(and (Flag37-)))

(:derived (Flag193-)(and (Flag122-)))

(:derived (Flag193-)(and (Flag124-)))

(:derived (Flag193-)(and (Flag41-)))

(:derived (Flag193-)(and (Flag169-)))

(:derived (Flag193-)(and (Flag179-)))

(:derived (Flag193-)(and (Flag170-)))

(:derived (Flag193-)(and (Flag183-)))

(:derived (Flag193-)(and (Flag159-)))

(:derived (Flag193-)(and (Flag187-)))

(:derived (Flag193-)(and (Flag60-)))

(:derived (Flag193-)(and (Flag172-)))

(:derived (Flag193-)(and (Flag173-)))

(:derived (Flag193-)(and (Flag66-)))

(:derived (Flag193-)(and (Flag160-)))

(:derived (Flag193-)(and (Flag71-)))

(:derived (Flag193-)(and (Flag188-)))

(:derived (Flag193-)(and (Flag180-)))

(:derived (Flag193-)(and (Flag189-)))

(:derived (Flag193-)(and (Flag96-)))

(:derived (Flag193-)(and (Flag148-)))

(:derived (Flag193-)(and (Flag116-)))

(:derived (Flag193-)(and (Flag119-)))

(:derived (Flag193-)(and (Flag135-)))

(:derived (Flag193-)(and (Flag138-)))

(:derived (Flag193-)(and (Flag47-)))

(:derived (Flag193-)(and (Flag120-)))

(:derived (Flag193-)(and (Flag89-)))

(:derived (Flag193-)(and (Flag49-)))

(:derived (Flag193-)(and (Flag90-)))

(:derived (Flag193-)(and (Flag54-)))

(:derived (Flag193-)(and (Flag79-)))

(:derived (Flag193-)(and (Flag109-)))

(:derived (Flag193-)(and (Flag190-)))

(:derived (Flag193-)(and (Flag181-)))

(:derived (Flag193-)(and (Flag191-)))

(:derived (Flag193-)(and (Flag75-)))

(:derived (Flag193-)(and (Flag153-)))

(:derived (Flag193-)(and (Flag155-)))

(:derived (Flag193-)(and (Flag73-)))

(:derived (Flag193-)(and (Flag132-)))

(:derived (Flag193-)(and (Flag164-)))

(:derived (Flag193-)(and (Flag56-)))

(:derived (Flag193-)(and (Flag137-)))

(:derived (Flag193-)(and (Flag114-)))

(:derived (Flag193-)(and (Flag165-)))

(:derived (Flag193-)(and (Flag139-)))

(:derived (Flag193-)(and (Flag175-)))

(:derived (Flag193-)(and (Flag182-)))

(:derived (Flag193-)(and (Flag166-)))

(:derived (Flag193-)(and (Flag154-)))

(:derived (Flag193-)(and (Flag149-)))

(:derived (Flag193-)(and (Flag51-)))

(:derived (Flag193-)(and (Flag81-)))

(:derived (Flag193-)(and (Flag93-)))

(:derived (Flag193-)(and (Flag167-)))

(:derived (Flag193-)(and (Flag82-)))

(:derived (Flag193-)(and (Flag142-)))

(:derived (Flag193-)(and (Flag86-)))

(:derived (Flag193-)(and (Flag36-)))

(:derived (Flag193-)(and (Flag192-)))

(:derived (Flag193-)(and (Flag59-)))

(:derived (Flag193-)(and (Flag128-)))

(:derived (Flag193-)(and (Flag176-)))

(:derived (Flag193-)(and (Flag107-)))

(:derived (Flag193-)(and (Flag158-)))

(:derived (Flag193-)(and (Flag99-)))

(:derived (Flag193-)(and (Flag156-)))

(:derived (Flag193-)(and (Flag65-)))

(:derived (Flag193-)(and (Flag101-)))

(:derived (Flag193-)(and (Flag108-)))

(:derived (Flag193-)(and (Flag185-)))

(:derived (Flag193-)(and (Flag129-)))

(:derived (Flag193-)(and (Flag144-)))

(:derived (Flag200-)(and (Flag178-)))

(:derived (Flag200-)(and (Flag104-)))

(:derived (Flag200-)(and (Flag37-)))

(:derived (Flag200-)(and (Flag124-)))

(:derived (Flag200-)(and (Flag41-)))

(:derived (Flag200-)(and (Flag169-)))

(:derived (Flag200-)(and (Flag71-)))

(:derived (Flag200-)(and (Flag183-)))

(:derived (Flag200-)(and (Flag159-)))

(:derived (Flag200-)(and (Flag158-)))

(:derived (Flag200-)(and (Flag172-)))

(:derived (Flag200-)(and (Flag173-)))

(:derived (Flag200-)(and (Flag54-)))

(:derived (Flag200-)(and (Flag160-)))

(:derived (Flag200-)(and (Flag194-)))

(:derived (Flag200-)(and (Flag188-)))

(:derived (Flag200-)(and (Flag180-)))

(:derived (Flag200-)(and (Flag189-)))

(:derived (Flag200-)(and (Flag96-)))

(:derived (Flag200-)(and (Flag148-)))

(:derived (Flag200-)(and (Flag116-)))

(:derived (Flag200-)(and (Flag119-)))

(:derived (Flag200-)(and (Flag135-)))

(:derived (Flag200-)(and (Flag138-)))

(:derived (Flag200-)(and (Flag47-)))

(:derived (Flag200-)(and (Flag89-)))

(:derived (Flag200-)(and (Flag90-)))

(:derived (Flag200-)(and (Flag79-)))

(:derived (Flag200-)(and (Flag109-)))

(:derived (Flag200-)(and (Flag190-)))

(:derived (Flag200-)(and (Flag181-)))

(:derived (Flag200-)(and (Flag75-)))

(:derived (Flag200-)(and (Flag153-)))

(:derived (Flag200-)(and (Flag155-)))

(:derived (Flag200-)(and (Flag73-)))

(:derived (Flag200-)(and (Flag137-)))

(:derived (Flag200-)(and (Flag164-)))

(:derived (Flag200-)(and (Flag56-)))

(:derived (Flag200-)(and (Flag195-)))

(:derived (Flag200-)(and (Flag114-)))

(:derived (Flag200-)(and (Flag165-)))

(:derived (Flag200-)(and (Flag196-)))

(:derived (Flag200-)(and (Flag175-)))

(:derived (Flag200-)(and (Flag182-)))

(:derived (Flag200-)(and (Flag154-)))

(:derived (Flag200-)(and (Flag149-)))

(:derived (Flag200-)(and (Flag51-)))

(:derived (Flag200-)(and (Flag93-)))

(:derived (Flag200-)(and (Flag66-)))

(:derived (Flag200-)(and (Flag82-)))

(:derived (Flag200-)(and (Flag139-)))

(:derived (Flag200-)(and (Flag142-)))

(:derived (Flag200-)(and (Flag86-)))

(:derived (Flag200-)(and (Flag36-)))

(:derived (Flag200-)(and (Flag192-)))

(:derived (Flag200-)(and (Flag197-)))

(:derived (Flag200-)(and (Flag59-)))

(:derived (Flag200-)(and (Flag128-)))

(:derived (Flag200-)(and (Flag176-)))

(:derived (Flag200-)(and (Flag99-)))

(:derived (Flag200-)(and (Flag198-)))

(:derived (Flag200-)(and (Flag199-)))

(:derived (Flag200-)(and (Flag65-)))

(:derived (Flag200-)(and (Flag167-)))

(:derived (Flag200-)(and (Flag108-)))

(:derived (Flag200-)(and (Flag185-)))

(:derived (Flag200-)(and (Flag129-)))

(:derived (Flag200-)(and (Flag144-)))

(:derived (Flag205-)(and (Flag178-)))

(:derived (Flag205-)(and (Flag104-)))

(:derived (Flag205-)(and (Flag37-)))

(:derived (Flag205-)(and (Flag124-)))

(:derived (Flag205-)(and (Flag41-)))

(:derived (Flag205-)(and (Flag201-)))

(:derived (Flag205-)(and (Flag71-)))

(:derived (Flag205-)(and (Flag183-)))

(:derived (Flag205-)(and (Flag159-)))

(:derived (Flag205-)(and (Flag158-)))

(:derived (Flag205-)(and (Flag173-)))

(:derived (Flag205-)(and (Flag160-)))

(:derived (Flag205-)(and (Flag194-)))

(:derived (Flag205-)(and (Flag188-)))

(:derived (Flag205-)(and (Flag180-)))

(:derived (Flag205-)(and (Flag189-)))

(:derived (Flag205-)(and (Flag96-)))

(:derived (Flag205-)(and (Flag148-)))

(:derived (Flag205-)(and (Flag116-)))

(:derived (Flag205-)(and (Flag119-)))

(:derived (Flag205-)(and (Flag135-)))

(:derived (Flag205-)(and (Flag47-)))

(:derived (Flag205-)(and (Flag90-)))

(:derived (Flag205-)(and (Flag149-)))

(:derived (Flag205-)(and (Flag109-)))

(:derived (Flag205-)(and (Flag190-)))

(:derived (Flag205-)(and (Flag181-)))

(:derived (Flag205-)(and (Flag202-)))

(:derived (Flag205-)(and (Flag73-)))

(:derived (Flag205-)(and (Flag169-)))

(:derived (Flag205-)(and (Flag164-)))

(:derived (Flag205-)(and (Flag108-)))

(:derived (Flag205-)(and (Flag138-)))

(:derived (Flag205-)(and (Flag139-)))

(:derived (Flag205-)(and (Flag203-)))

(:derived (Flag205-)(and (Flag182-)))

(:derived (Flag205-)(and (Flag154-)))

(:derived (Flag205-)(and (Flag79-)))

(:derived (Flag205-)(and (Flag51-)))

(:derived (Flag205-)(and (Flag66-)))

(:derived (Flag205-)(and (Flag89-)))

(:derived (Flag205-)(and (Flag196-)))

(:derived (Flag205-)(and (Flag86-)))

(:derived (Flag205-)(and (Flag175-)))

(:derived (Flag205-)(and (Flag36-)))

(:derived (Flag205-)(and (Flag192-)))

(:derived (Flag205-)(and (Flag197-)))

(:derived (Flag205-)(and (Flag59-)))

(:derived (Flag205-)(and (Flag128-)))

(:derived (Flag205-)(and (Flag176-)))

(:derived (Flag205-)(and (Flag155-)))

(:derived (Flag205-)(and (Flag204-)))

(:derived (Flag205-)(and (Flag99-)))

(:derived (Flag205-)(and (Flag198-)))

(:derived (Flag205-)(and (Flag65-)))

(:derived (Flag205-)(and (Flag167-)))

(:derived (Flag205-)(and (Flag129-)))

(:derived (Flag205-)(and (Flag144-)))

(:derived (Flag209-)(and (Flag178-)))

(:derived (Flag209-)(and (Flag104-)))

(:derived (Flag209-)(and (Flag124-)))

(:derived (Flag209-)(and (Flag41-)))

(:derived (Flag209-)(and (Flag201-)))

(:derived (Flag209-)(and (Flag71-)))

(:derived (Flag209-)(and (Flag183-)))

(:derived (Flag209-)(and (Flag159-)))

(:derived (Flag209-)(and (Flag158-)))

(:derived (Flag209-)(and (Flag206-)))

(:derived (Flag209-)(and (Flag194-)))

(:derived (Flag209-)(and (Flag188-)))

(:derived (Flag209-)(and (Flag180-)))

(:derived (Flag209-)(and (Flag189-)))

(:derived (Flag209-)(and (Flag96-)))

(:derived (Flag209-)(and (Flag116-)))

(:derived (Flag209-)(and (Flag119-)))

(:derived (Flag209-)(and (Flag47-)))

(:derived (Flag209-)(and (Flag90-)))

(:derived (Flag209-)(and (Flag149-)))

(:derived (Flag209-)(and (Flag109-)))

(:derived (Flag209-)(and (Flag190-)))

(:derived (Flag209-)(and (Flag181-)))

(:derived (Flag209-)(and (Flag73-)))

(:derived (Flag209-)(and (Flag164-)))

(:derived (Flag209-)(and (Flag138-)))

(:derived (Flag209-)(and (Flag196-)))

(:derived (Flag209-)(and (Flag175-)))

(:derived (Flag209-)(and (Flag154-)))

(:derived (Flag209-)(and (Flag79-)))

(:derived (Flag209-)(and (Flag51-)))

(:derived (Flag209-)(and (Flag66-)))

(:derived (Flag209-)(and (Flag89-)))

(:derived (Flag209-)(and (Flag139-)))

(:derived (Flag209-)(and (Flag207-)))

(:derived (Flag209-)(and (Flag203-)))

(:derived (Flag209-)(and (Flag204-)))

(:derived (Flag209-)(and (Flag36-)))

(:derived (Flag209-)(and (Flag176-)))

(:derived (Flag209-)(and (Flag198-)))

(:derived (Flag209-)(and (Flag65-)))

(:derived (Flag209-)(and (Flag167-)))

(:derived (Flag209-)(and (Flag208-)))

(:derived (Flag209-)(and (Flag129-)))

(:derived (Flag209-)(and (Flag144-)))

(:derived (Flag213-)(and (Flag104-)))

(:derived (Flag213-)(and (Flag124-)))

(:derived (Flag213-)(and (Flag41-)))

(:derived (Flag213-)(and (Flag183-)))

(:derived (Flag213-)(and (Flag159-)))

(:derived (Flag213-)(and (Flag158-)))

(:derived (Flag213-)(and (Flag206-)))

(:derived (Flag213-)(and (Flag210-)))

(:derived (Flag213-)(and (Flag194-)))

(:derived (Flag213-)(and (Flag188-)))

(:derived (Flag213-)(and (Flag180-)))

(:derived (Flag213-)(and (Flag189-)))

(:derived (Flag213-)(and (Flag96-)))

(:derived (Flag213-)(and (Flag119-)))

(:derived (Flag213-)(and (Flag47-)))

(:derived (Flag213-)(and (Flag149-)))

(:derived (Flag213-)(and (Flag109-)))

(:derived (Flag213-)(and (Flag73-)))

(:derived (Flag213-)(and (Flag138-)))

(:derived (Flag213-)(and (Flag175-)))

(:derived (Flag213-)(and (Flag154-)))

(:derived (Flag213-)(and (Flag79-)))

(:derived (Flag213-)(and (Flag89-)))

(:derived (Flag213-)(and (Flag211-)))

(:derived (Flag213-)(and (Flag203-)))

(:derived (Flag213-)(and (Flag204-)))

(:derived (Flag213-)(and (Flag36-)))

(:derived (Flag213-)(and (Flag176-)))

(:derived (Flag213-)(and (Flag198-)))

(:derived (Flag213-)(and (Flag66-)))

(:derived (Flag213-)(and (Flag212-)))

(:derived (Flag213-)(and (Flag144-)))

(:derived (Flag215-)(and (Flag180-)))

(:derived (Flag215-)(and (Flag211-)))

(:derived (Flag215-)(and (Flag175-)))

(:derived (Flag215-)(and (Flag73-)))

(:derived (Flag215-)(and (Flag124-)))

(:derived (Flag215-)(and (Flag96-)))

(:derived (Flag215-)(and (Flag138-)))

(:derived (Flag215-)(and (Flag214-)))

(:derived (Flag215-)(and (Flag47-)))

(:derived (Flag215-)(and (Flag203-)))

(:derived (Flag215-)(and (Flag158-)))

(:derived (Flag215-)(and (Flag198-)))

(:derived (Flag215-)(and (Flag189-)))

(:derived (Flag215-)(and (Flag149-)))

(:derived (Flag215-)(and (Flag109-)))

(:derived (Flag215-)(and (Flag66-)))

(:derived (Flag215-)(and (Flag206-)))

(:derived (Flag429-)(and (Flag52-)))

(:derived (Flag429-)(and (Flag54-)))

(:derived (Flag429-)(and (Flag55-)))

(:derived (Flag429-)(and (Flag39-)))

(:derived (Flag429-)(and (Flag59-)))

(:derived (Flag429-)(and (Flag45-)))

(:derived (Flag429-)(and (Flag40-)))

(:derived (Flag429-)(and (Flag41-)))

(:derived (Flag429-)(and (Flag428-)))

(:derived (Flag429-)(and (Flag57-)))

(:derived (Flag429-)(and (Flag63-)))

(:derived (Flag429-)(and (Flag49-)))

(:derived (Flag429-)(and (Flag58-)))

(:derived (Flag429-)(and (Flag64-)))

(:derived (Flag429-)(and (Flag65-)))

(:derived (Flag429-)(and (Flag42-)))

(:derived (Flag429-)(and (Flag66-)))

(:derived (Flag1-)(and (COLUMN2-ROBOT)(ROW1-ROBOT)(NOT-ERROR-)(NOT-CHECKCONSISTENCY-)))

(:derived (Flag3-)(and (LEFTOF3-ROBOT)))

(:derived (Flag3-)(and (RIGHTOF7-ROBOT)))

(:derived (Flag3-)(and (RIGHTOF8-ROBOT)))

(:derived (Flag3-)(and (RIGHTOF15-ROBOT)))

(:derived (Flag3-)(and (LEFTOF2-ROBOT)))

(:derived (Flag3-)(and (LEFTOF9-ROBOT)))

(:derived (Flag3-)(and (RIGHTOF0-ROBOT)))

(:derived (Flag3-)(and (LEFTOF8-ROBOT)))

(:derived (Flag3-)(and (LEFTOF7-ROBOT)))

(:derived (Flag3-)(and (LEFTOF14-ROBOT)))

(:derived (Flag3-)(and (LEFTOF5-ROBOT)))

(:derived (Flag3-)(and (RIGHTOF1-ROBOT)))

(:derived (Flag3-)(and (LEFTOF12-ROBOT)))

(:derived (Flag3-)(and (RIGHTOF13-ROBOT)))

(:derived (Flag3-)(and (RIGHTOF14-ROBOT)))

(:derived (Flag3-)(and (RIGHTOF6-ROBOT)))

(:derived (Flag3-)(and (RIGHTOF9-ROBOT)))

(:derived (Flag3-)(and (LEFTOF6-ROBOT)))

(:derived (Flag3-)(and (RIGHTOF10-ROBOT)))

(:derived (Flag3-)(and (RIGHTOF11-ROBOT)))

(:derived (Flag3-)(and (LEFTOF11-ROBOT)))

(:derived (Flag3-)(and (RIGHTOF5-ROBOT)))

(:derived (Flag3-)(and (LEFTOF10-ROBOT)))

(:derived (Flag3-)(and (LEFTOF15-ROBOT)))

(:derived (Flag3-)(and (LEFTOF4-ROBOT)))

(:derived (Flag3-)(and (RIGHTOF16-ROBOT)))

(:derived (Flag3-)(and (RIGHTOF4-ROBOT)))

(:derived (Flag3-)(and (LEFTOF1-ROBOT)))

(:derived (Flag3-)(and (RIGHTOF12-ROBOT)))

(:derived (Flag3-)(and (LEFTOF13-ROBOT)))

(:derived (Flag3-)(and (LEFTOF17-ROBOT)))

(:derived (Flag3-)(and (RIGHTOF3-ROBOT)))

(:derived (Flag5-)(and (LEFTOF1-ROBOT)))

(:derived (Flag5-)(and (LEFTOF2-ROBOT)))

(:derived (Flag6-)(and (LEFTOF1-ROBOT)))

(:derived (Flag6-)(and (LEFTOF3-ROBOT)))

(:derived (Flag6-)(and (LEFTOF2-ROBOT)))

(:derived (Flag7-)(and (LEFTOF1-ROBOT)))

(:derived (Flag7-)(and (LEFTOF3-ROBOT)))

(:derived (Flag7-)(and (LEFTOF4-ROBOT)))

(:derived (Flag7-)(and (LEFTOF2-ROBOT)))

(:derived (Flag8-)(and (LEFTOF5-ROBOT)))

(:derived (Flag8-)(and (LEFTOF1-ROBOT)))

(:derived (Flag8-)(and (LEFTOF3-ROBOT)))

(:derived (Flag8-)(and (LEFTOF4-ROBOT)))

(:derived (Flag8-)(and (LEFTOF2-ROBOT)))

(:derived (Flag9-)(and (LEFTOF2-ROBOT)))

(:derived (Flag9-)(and (LEFTOF1-ROBOT)))

(:derived (Flag9-)(and (LEFTOF6-ROBOT)))

(:derived (Flag9-)(and (LEFTOF5-ROBOT)))

(:derived (Flag9-)(and (LEFTOF3-ROBOT)))

(:derived (Flag9-)(and (LEFTOF4-ROBOT)))

(:derived (Flag10-)(and (LEFTOF2-ROBOT)))

(:derived (Flag10-)(and (LEFTOF7-ROBOT)))

(:derived (Flag10-)(and (LEFTOF1-ROBOT)))

(:derived (Flag10-)(and (LEFTOF6-ROBOT)))

(:derived (Flag10-)(and (LEFTOF5-ROBOT)))

(:derived (Flag10-)(and (LEFTOF3-ROBOT)))

(:derived (Flag10-)(and (LEFTOF4-ROBOT)))

(:derived (Flag11-)(and (LEFTOF2-ROBOT)))

(:derived (Flag11-)(and (LEFTOF7-ROBOT)))

(:derived (Flag11-)(and (LEFTOF1-ROBOT)))

(:derived (Flag11-)(and (LEFTOF6-ROBOT)))

(:derived (Flag11-)(and (LEFTOF5-ROBOT)))

(:derived (Flag11-)(and (LEFTOF3-ROBOT)))

(:derived (Flag11-)(and (LEFTOF4-ROBOT)))

(:derived (Flag11-)(and (LEFTOF8-ROBOT)))

(:derived (Flag12-)(and (LEFTOF9-ROBOT)))

(:derived (Flag12-)(and (LEFTOF2-ROBOT)))

(:derived (Flag12-)(and (LEFTOF7-ROBOT)))

(:derived (Flag12-)(and (LEFTOF1-ROBOT)))

(:derived (Flag12-)(and (LEFTOF6-ROBOT)))

(:derived (Flag12-)(and (LEFTOF5-ROBOT)))

(:derived (Flag12-)(and (LEFTOF3-ROBOT)))

(:derived (Flag12-)(and (LEFTOF4-ROBOT)))

(:derived (Flag12-)(and (LEFTOF8-ROBOT)))

(:derived (Flag13-)(and (LEFTOF10-ROBOT)))

(:derived (Flag13-)(and (LEFTOF9-ROBOT)))

(:derived (Flag13-)(and (LEFTOF2-ROBOT)))

(:derived (Flag13-)(and (LEFTOF7-ROBOT)))

(:derived (Flag13-)(and (LEFTOF1-ROBOT)))

(:derived (Flag13-)(and (LEFTOF6-ROBOT)))

(:derived (Flag13-)(and (LEFTOF5-ROBOT)))

(:derived (Flag13-)(and (LEFTOF3-ROBOT)))

(:derived (Flag13-)(and (LEFTOF4-ROBOT)))

(:derived (Flag13-)(and (LEFTOF8-ROBOT)))

(:derived (Flag14-)(and (LEFTOF10-ROBOT)))

(:derived (Flag14-)(and (LEFTOF11-ROBOT)))

(:derived (Flag14-)(and (LEFTOF9-ROBOT)))

(:derived (Flag14-)(and (LEFTOF2-ROBOT)))

(:derived (Flag14-)(and (LEFTOF7-ROBOT)))

(:derived (Flag14-)(and (LEFTOF1-ROBOT)))

(:derived (Flag14-)(and (LEFTOF6-ROBOT)))

(:derived (Flag14-)(and (LEFTOF5-ROBOT)))

(:derived (Flag14-)(and (LEFTOF3-ROBOT)))

(:derived (Flag14-)(and (LEFTOF4-ROBOT)))

(:derived (Flag14-)(and (LEFTOF8-ROBOT)))

(:derived (Flag15-)(and (LEFTOF10-ROBOT)))

(:derived (Flag15-)(and (LEFTOF11-ROBOT)))

(:derived (Flag15-)(and (LEFTOF9-ROBOT)))

(:derived (Flag15-)(and (LEFTOF2-ROBOT)))

(:derived (Flag15-)(and (LEFTOF7-ROBOT)))

(:derived (Flag15-)(and (LEFTOF1-ROBOT)))

(:derived (Flag15-)(and (LEFTOF12-ROBOT)))

(:derived (Flag15-)(and (LEFTOF6-ROBOT)))

(:derived (Flag15-)(and (LEFTOF5-ROBOT)))

(:derived (Flag15-)(and (LEFTOF3-ROBOT)))

(:derived (Flag15-)(and (LEFTOF4-ROBOT)))

(:derived (Flag15-)(and (LEFTOF8-ROBOT)))

(:derived (Flag16-)(and (LEFTOF10-ROBOT)))

(:derived (Flag16-)(and (LEFTOF11-ROBOT)))

(:derived (Flag16-)(and (LEFTOF9-ROBOT)))

(:derived (Flag16-)(and (LEFTOF2-ROBOT)))

(:derived (Flag16-)(and (LEFTOF13-ROBOT)))

(:derived (Flag16-)(and (LEFTOF1-ROBOT)))

(:derived (Flag16-)(and (LEFTOF12-ROBOT)))

(:derived (Flag16-)(and (LEFTOF6-ROBOT)))

(:derived (Flag16-)(and (LEFTOF5-ROBOT)))

(:derived (Flag16-)(and (LEFTOF3-ROBOT)))

(:derived (Flag16-)(and (LEFTOF4-ROBOT)))

(:derived (Flag16-)(and (LEFTOF7-ROBOT)))

(:derived (Flag16-)(and (LEFTOF8-ROBOT)))

(:derived (Flag17-)(and (LEFTOF10-ROBOT)))

(:derived (Flag17-)(and (LEFTOF11-ROBOT)))

(:derived (Flag17-)(and (LEFTOF9-ROBOT)))

(:derived (Flag17-)(and (LEFTOF2-ROBOT)))

(:derived (Flag17-)(and (LEFTOF13-ROBOT)))

(:derived (Flag17-)(and (LEFTOF1-ROBOT)))

(:derived (Flag17-)(and (LEFTOF12-ROBOT)))

(:derived (Flag17-)(and (LEFTOF6-ROBOT)))

(:derived (Flag17-)(and (LEFTOF5-ROBOT)))

(:derived (Flag17-)(and (LEFTOF14-ROBOT)))

(:derived (Flag17-)(and (LEFTOF3-ROBOT)))

(:derived (Flag17-)(and (LEFTOF4-ROBOT)))

(:derived (Flag17-)(and (LEFTOF7-ROBOT)))

(:derived (Flag17-)(and (LEFTOF8-ROBOT)))

(:derived (Flag18-)(and (LEFTOF15-ROBOT)))

(:derived (Flag18-)(and (LEFTOF10-ROBOT)))

(:derived (Flag18-)(and (LEFTOF11-ROBOT)))

(:derived (Flag18-)(and (LEFTOF9-ROBOT)))

(:derived (Flag18-)(and (LEFTOF2-ROBOT)))

(:derived (Flag18-)(and (LEFTOF13-ROBOT)))

(:derived (Flag18-)(and (LEFTOF1-ROBOT)))

(:derived (Flag18-)(and (LEFTOF12-ROBOT)))

(:derived (Flag18-)(and (LEFTOF6-ROBOT)))

(:derived (Flag18-)(and (LEFTOF5-ROBOT)))

(:derived (Flag18-)(and (LEFTOF14-ROBOT)))

(:derived (Flag18-)(and (LEFTOF3-ROBOT)))

(:derived (Flag18-)(and (LEFTOF4-ROBOT)))

(:derived (Flag18-)(and (LEFTOF7-ROBOT)))

(:derived (Flag18-)(and (LEFTOF8-ROBOT)))

(:derived (Flag19-)(and (LEFTOF15-ROBOT)))

(:derived (Flag19-)(and (LEFTOF10-ROBOT)))

(:derived (Flag19-)(and (LEFTOF11-ROBOT)))

(:derived (Flag19-)(and (LEFTOF9-ROBOT)))

(:derived (Flag19-)(and (LEFTOF2-ROBOT)))

(:derived (Flag19-)(and (LEFTOF13-ROBOT)))

(:derived (Flag19-)(and (LEFTOF1-ROBOT)))

(:derived (Flag19-)(and (LEFTOF12-ROBOT)))

(:derived (Flag19-)(and (LEFTOF6-ROBOT)))

(:derived (Flag19-)(and (LEFTOF5-ROBOT)))

(:derived (Flag19-)(and (LEFTOF14-ROBOT)))

(:derived (Flag19-)(and (LEFTOF3-ROBOT)))

(:derived (Flag19-)(and (LEFTOF4-ROBOT)))

(:derived (Flag19-)(and (LEFTOF7-ROBOT)))

(:derived (Flag19-)(and (LEFTOF8-ROBOT)))

(:derived (Flag20-)(and (LEFTOF15-ROBOT)))

(:derived (Flag20-)(and (LEFTOF10-ROBOT)))

(:derived (Flag20-)(and (LEFTOF17-ROBOT)))

(:derived (Flag20-)(and (LEFTOF11-ROBOT)))

(:derived (Flag20-)(and (LEFTOF9-ROBOT)))

(:derived (Flag20-)(and (LEFTOF2-ROBOT)))

(:derived (Flag20-)(and (LEFTOF13-ROBOT)))

(:derived (Flag20-)(and (LEFTOF1-ROBOT)))

(:derived (Flag20-)(and (LEFTOF12-ROBOT)))

(:derived (Flag20-)(and (LEFTOF6-ROBOT)))

(:derived (Flag20-)(and (LEFTOF5-ROBOT)))

(:derived (Flag20-)(and (LEFTOF14-ROBOT)))

(:derived (Flag20-)(and (LEFTOF3-ROBOT)))

(:derived (Flag20-)(and (LEFTOF4-ROBOT)))

(:derived (Flag20-)(and (LEFTOF7-ROBOT)))

(:derived (Flag20-)(and (LEFTOF8-ROBOT)))

(:derived (Flag21-)(and (RIGHTOF3-ROBOT)))

(:derived (Flag21-)(and (RIGHTOF5-ROBOT)))

(:derived (Flag21-)(and (RIGHTOF9-ROBOT)))

(:derived (Flag21-)(and (RIGHTOF8-ROBOT)))

(:derived (Flag21-)(and (RIGHTOF14-ROBOT)))

(:derived (Flag21-)(and (RIGHTOF13-ROBOT)))

(:derived (Flag21-)(and (RIGHTOF12-ROBOT)))

(:derived (Flag21-)(and (RIGHTOF10-ROBOT)))

(:derived (Flag21-)(and (RIGHTOF4-ROBOT)))

(:derived (Flag21-)(and (RIGHTOF1-ROBOT)))

(:derived (Flag21-)(and (RIGHTOF16-ROBOT)))

(:derived (Flag21-)(and (RIGHTOF7-ROBOT)))

(:derived (Flag21-)(and (RIGHTOF11-ROBOT)))

(:derived (Flag21-)(and (RIGHTOF15-ROBOT)))

(:derived (Flag21-)(and (RIGHTOF6-ROBOT)))

(:derived (Flag22-)(and (RIGHTOF3-ROBOT)))

(:derived (Flag22-)(and (RIGHTOF5-ROBOT)))

(:derived (Flag22-)(and (RIGHTOF9-ROBOT)))

(:derived (Flag22-)(and (RIGHTOF14-ROBOT)))

(:derived (Flag22-)(and (RIGHTOF13-ROBOT)))

(:derived (Flag22-)(and (RIGHTOF12-ROBOT)))

(:derived (Flag22-)(and (RIGHTOF10-ROBOT)))

(:derived (Flag22-)(and (RIGHTOF4-ROBOT)))

(:derived (Flag22-)(and (RIGHTOF8-ROBOT)))

(:derived (Flag22-)(and (RIGHTOF16-ROBOT)))

(:derived (Flag22-)(and (RIGHTOF7-ROBOT)))

(:derived (Flag22-)(and (RIGHTOF11-ROBOT)))

(:derived (Flag22-)(and (RIGHTOF15-ROBOT)))

(:derived (Flag22-)(and (RIGHTOF6-ROBOT)))

(:derived (Flag23-)(and (RIGHTOF3-ROBOT)))

(:derived (Flag23-)(and (RIGHTOF5-ROBOT)))

(:derived (Flag23-)(and (RIGHTOF9-ROBOT)))

(:derived (Flag23-)(and (RIGHTOF14-ROBOT)))

(:derived (Flag23-)(and (RIGHTOF13-ROBOT)))

(:derived (Flag23-)(and (RIGHTOF12-ROBOT)))

(:derived (Flag23-)(and (RIGHTOF10-ROBOT)))

(:derived (Flag23-)(and (RIGHTOF4-ROBOT)))

(:derived (Flag23-)(and (RIGHTOF8-ROBOT)))

(:derived (Flag23-)(and (RIGHTOF16-ROBOT)))

(:derived (Flag23-)(and (RIGHTOF7-ROBOT)))

(:derived (Flag23-)(and (RIGHTOF11-ROBOT)))

(:derived (Flag23-)(and (RIGHTOF15-ROBOT)))

(:derived (Flag23-)(and (RIGHTOF6-ROBOT)))

(:derived (Flag24-)(and (RIGHTOF5-ROBOT)))

(:derived (Flag24-)(and (RIGHTOF9-ROBOT)))

(:derived (Flag24-)(and (RIGHTOF14-ROBOT)))

(:derived (Flag24-)(and (RIGHTOF13-ROBOT)))

(:derived (Flag24-)(and (RIGHTOF12-ROBOT)))

(:derived (Flag24-)(and (RIGHTOF10-ROBOT)))

(:derived (Flag24-)(and (RIGHTOF4-ROBOT)))

(:derived (Flag24-)(and (RIGHTOF8-ROBOT)))

(:derived (Flag24-)(and (RIGHTOF16-ROBOT)))

(:derived (Flag24-)(and (RIGHTOF7-ROBOT)))

(:derived (Flag24-)(and (RIGHTOF11-ROBOT)))

(:derived (Flag24-)(and (RIGHTOF15-ROBOT)))

(:derived (Flag24-)(and (RIGHTOF6-ROBOT)))

(:derived (Flag25-)(and (RIGHTOF5-ROBOT)))

(:derived (Flag25-)(and (RIGHTOF9-ROBOT)))

(:derived (Flag25-)(and (RIGHTOF14-ROBOT)))

(:derived (Flag25-)(and (RIGHTOF13-ROBOT)))

(:derived (Flag25-)(and (RIGHTOF12-ROBOT)))

(:derived (Flag25-)(and (RIGHTOF10-ROBOT)))

(:derived (Flag25-)(and (RIGHTOF8-ROBOT)))

(:derived (Flag25-)(and (RIGHTOF16-ROBOT)))

(:derived (Flag25-)(and (RIGHTOF7-ROBOT)))

(:derived (Flag25-)(and (RIGHTOF11-ROBOT)))

(:derived (Flag25-)(and (RIGHTOF15-ROBOT)))

(:derived (Flag25-)(and (RIGHTOF6-ROBOT)))

(:derived (Flag26-)(and (RIGHTOF9-ROBOT)))

(:derived (Flag26-)(and (RIGHTOF14-ROBOT)))

(:derived (Flag26-)(and (RIGHTOF13-ROBOT)))

(:derived (Flag26-)(and (RIGHTOF12-ROBOT)))

(:derived (Flag26-)(and (RIGHTOF10-ROBOT)))

(:derived (Flag26-)(and (RIGHTOF8-ROBOT)))

(:derived (Flag26-)(and (RIGHTOF16-ROBOT)))

(:derived (Flag26-)(and (RIGHTOF7-ROBOT)))

(:derived (Flag26-)(and (RIGHTOF11-ROBOT)))

(:derived (Flag26-)(and (RIGHTOF15-ROBOT)))

(:derived (Flag26-)(and (RIGHTOF6-ROBOT)))

(:derived (Flag27-)(and (RIGHTOF14-ROBOT)))

(:derived (Flag27-)(and (RIGHTOF13-ROBOT)))

(:derived (Flag27-)(and (RIGHTOF15-ROBOT)))

(:derived (Flag27-)(and (RIGHTOF10-ROBOT)))

(:derived (Flag27-)(and (RIGHTOF8-ROBOT)))

(:derived (Flag27-)(and (RIGHTOF12-ROBOT)))

(:derived (Flag27-)(and (RIGHTOF16-ROBOT)))

(:derived (Flag27-)(and (RIGHTOF7-ROBOT)))

(:derived (Flag27-)(and (RIGHTOF9-ROBOT)))

(:derived (Flag27-)(and (RIGHTOF11-ROBOT)))

(:derived (Flag28-)(and (RIGHTOF14-ROBOT)))

(:derived (Flag28-)(and (RIGHTOF13-ROBOT)))

(:derived (Flag28-)(and (RIGHTOF15-ROBOT)))

(:derived (Flag28-)(and (RIGHTOF10-ROBOT)))

(:derived (Flag28-)(and (RIGHTOF8-ROBOT)))

(:derived (Flag28-)(and (RIGHTOF16-ROBOT)))

(:derived (Flag28-)(and (RIGHTOF12-ROBOT)))

(:derived (Flag28-)(and (RIGHTOF9-ROBOT)))

(:derived (Flag28-)(and (RIGHTOF11-ROBOT)))

(:derived (Flag29-)(and (RIGHTOF14-ROBOT)))

(:derived (Flag29-)(and (RIGHTOF13-ROBOT)))

(:derived (Flag29-)(and (RIGHTOF15-ROBOT)))

(:derived (Flag29-)(and (RIGHTOF10-ROBOT)))

(:derived (Flag29-)(and (RIGHTOF16-ROBOT)))

(:derived (Flag29-)(and (RIGHTOF12-ROBOT)))

(:derived (Flag29-)(and (RIGHTOF9-ROBOT)))

(:derived (Flag29-)(and (RIGHTOF11-ROBOT)))

(:derived (Flag30-)(and (RIGHTOF14-ROBOT)))

(:derived (Flag30-)(and (RIGHTOF13-ROBOT)))

(:derived (Flag30-)(and (RIGHTOF15-ROBOT)))

(:derived (Flag30-)(and (RIGHTOF10-ROBOT)))

(:derived (Flag30-)(and (RIGHTOF16-ROBOT)))

(:derived (Flag30-)(and (RIGHTOF12-ROBOT)))

(:derived (Flag30-)(and (RIGHTOF11-ROBOT)))

(:derived (Flag31-)(and (RIGHTOF14-ROBOT)))

(:derived (Flag31-)(and (RIGHTOF13-ROBOT)))

(:derived (Flag31-)(and (RIGHTOF15-ROBOT)))

(:derived (Flag31-)(and (RIGHTOF16-ROBOT)))

(:derived (Flag31-)(and (RIGHTOF12-ROBOT)))

(:derived (Flag31-)(and (RIGHTOF11-ROBOT)))

(:derived (Flag32-)(and (RIGHTOF16-ROBOT)))

(:derived (Flag32-)(and (RIGHTOF14-ROBOT)))

(:derived (Flag32-)(and (RIGHTOF13-ROBOT)))

(:derived (Flag32-)(and (RIGHTOF15-ROBOT)))

(:derived (Flag32-)(and (RIGHTOF12-ROBOT)))

(:derived (Flag33-)(and (RIGHTOF16-ROBOT)))

(:derived (Flag33-)(and (RIGHTOF14-ROBOT)))

(:derived (Flag33-)(and (RIGHTOF13-ROBOT)))

(:derived (Flag33-)(and (RIGHTOF15-ROBOT)))

(:derived (Flag34-)(and (RIGHTOF16-ROBOT)))

(:derived (Flag34-)(and (RIGHTOF14-ROBOT)))

(:derived (Flag34-)(and (RIGHTOF15-ROBOT)))

(:derived (Flag35-)(and (RIGHTOF16-ROBOT)))

(:derived (Flag35-)(and (RIGHTOF15-ROBOT)))

(:derived (Flag36-)(and (LEFTOF2-ROBOT)(RIGHTOF15-ROBOT)))

(:derived (Flag37-)(and (RIGHTOF13-ROBOT)(LEFTOF2-ROBOT)))

(:derived (Flag38-)(and (LEFTOF2-ROBOT)(RIGHTOF9-ROBOT)))

(:derived (Flag39-)(and (LEFTOF1-ROBOT)(RIGHTOF9-ROBOT)))

(:derived (Flag40-)(and (RIGHTOF3-ROBOT)(LEFTOF1-ROBOT)))

(:derived (Flag41-)(and (LEFTOF1-ROBOT)(RIGHTOF15-ROBOT)))

(:derived (Flag42-)(and (RIGHTOF10-ROBOT)(LEFTOF1-ROBOT)))

(:derived (Flag43-)(and (LEFTOF2-ROBOT)(RIGHTOF1-ROBOT)))

(:derived (Flag44-)(and (RIGHTOF5-ROBOT)(LEFTOF2-ROBOT)))

(:derived (Flag45-)(and (LEFTOF1-ROBOT)(RIGHTOF1-ROBOT)))

(:derived (Flag46-)(and (LEFTOF2-ROBOT)(RIGHTOF7-ROBOT)))

(:derived (Flag47-)(and (RIGHTOF16-ROBOT)(LEFTOF2-ROBOT)))

(:derived (Flag48-)(and (RIGHTOF5-ROBOT)(LEFTOF1-ROBOT)))

(:derived (Flag49-)(and (RIGHTOF11-ROBOT)(LEFTOF1-ROBOT)))

(:derived (Flag50-)(and (RIGHTOF3-ROBOT)(LEFTOF2-ROBOT)))

(:derived (Flag51-)(and (LEFTOF2-ROBOT)(RIGHTOF14-ROBOT)))

(:derived (Flag52-)(and (LEFTOF1-ROBOT)(RIGHTOF4-ROBOT)))

(:derived (Flag53-)(and (RIGHTOF10-ROBOT)(LEFTOF2-ROBOT)))

(:derived (Flag54-)(and (LEFTOF1-ROBOT)(RIGHTOF12-ROBOT)))

(:derived (Flag55-)(and (LEFTOF1-ROBOT)(RIGHTOF7-ROBOT)))

(:derived (Flag56-)(and (LEFTOF2-ROBOT)(RIGHTOF12-ROBOT)))

(:derived (Flag57-)(and (LEFTOF1-ROBOT)(RIGHTOF5-ROBOT)))

(:derived (Flag58-)(and (RIGHTOF8-ROBOT)(LEFTOF1-ROBOT)))

(:derived (Flag59-)(and (RIGHTOF13-ROBOT)(LEFTOF1-ROBOT)))

(:derived (Flag60-)(and (RIGHTOF11-ROBOT)(LEFTOF2-ROBOT)))

(:derived (Flag61-)(and (RIGHTOF8-ROBOT)(LEFTOF2-ROBOT)))

(:derived (Flag62-)(and (RIGHTOF2-ROBOT)(LEFTOF2-ROBOT)))

(:derived (Flag63-)(and (LEFTOF1-ROBOT)(RIGHTOF2-ROBOT)))

(:derived (Flag64-)(and (LEFTOF1-ROBOT)(RIGHTOF6-ROBOT)))

(:derived (Flag65-)(and (LEFTOF1-ROBOT)(RIGHTOF14-ROBOT)))

(:derived (Flag66-)(and (LEFTOF1-ROBOT)(RIGHTOF16-ROBOT)))

(:derived (Flag67-)(and (LEFTOF2-ROBOT)(RIGHTOF6-ROBOT)))

(:derived (Flag68-)(and (LEFTOF2-ROBOT)(RIGHTOF4-ROBOT)))

(:derived (Flag70-)(and (LEFTOF3-ROBOT)(RIGHTOF6-ROBOT)))

(:derived (Flag71-)(and (LEFTOF3-ROBOT)(RIGHTOF14-ROBOT)))

(:derived (Flag72-)(and (LEFTOF3-ROBOT)(RIGHTOF4-ROBOT)))

(:derived (Flag73-)(and (LEFTOF3-ROBOT)(RIGHTOF16-ROBOT)))

(:derived (Flag74-)(and (LEFTOF3-ROBOT)(RIGHTOF7-ROBOT)))

(:derived (Flag75-)(and (LEFTOF3-ROBOT)(RIGHTOF12-ROBOT)))

(:derived (Flag76-)(and (LEFTOF3-ROBOT)(RIGHTOF10-ROBOT)))

(:derived (Flag77-)(and (LEFTOF3-ROBOT)(RIGHTOF8-ROBOT)))

(:derived (Flag78-)(and (LEFTOF3-ROBOT)(RIGHTOF5-ROBOT)))

(:derived (Flag79-)(and (LEFTOF3-ROBOT)(RIGHTOF15-ROBOT)))

(:derived (Flag80-)(and (RIGHTOF3-ROBOT)(LEFTOF3-ROBOT)))

(:derived (Flag81-)(and (LEFTOF3-ROBOT)(RIGHTOF11-ROBOT)))

(:derived (Flag82-)(and (RIGHTOF13-ROBOT)(LEFTOF3-ROBOT)))

(:derived (Flag83-)(and (LEFTOF3-ROBOT)(RIGHTOF9-ROBOT)))

(:derived (Flag84-)(and (LEFTOF3-ROBOT)(RIGHTOF2-ROBOT)))

(:derived (Flag86-)(and (LEFTOF3-ROBOT)(RIGHTOF13-ROBOT)))

(:derived (Flag87-)(and (LEFTOF4-ROBOT)(RIGHTOF4-ROBOT)))

(:derived (Flag88-)(and (LEFTOF4-ROBOT)(RIGHTOF7-ROBOT)))

(:derived (Flag89-)(and (LEFTOF4-ROBOT)(RIGHTOF15-ROBOT)))

(:derived (Flag90-)(and (LEFTOF4-ROBOT)(RIGHTOF14-ROBOT)))

(:derived (Flag91-)(and (LEFTOF3-ROBOT)(RIGHTOF3-ROBOT)))

(:derived (Flag92-)(and (RIGHTOF8-ROBOT)(LEFTOF4-ROBOT)))

(:derived (Flag93-)(and (LEFTOF4-ROBOT)(RIGHTOF12-ROBOT)))

(:derived (Flag94-)(and (RIGHTOF10-ROBOT)(LEFTOF4-ROBOT)))

(:derived (Flag95-)(and (LEFTOF4-ROBOT)(RIGHTOF9-ROBOT)))

(:derived (Flag96-)(and (LEFTOF4-ROBOT)(RIGHTOF16-ROBOT)))

(:derived (Flag97-)(and (LEFTOF4-ROBOT)(RIGHTOF6-ROBOT)))

(:derived (Flag98-)(and (LEFTOF4-ROBOT)(RIGHTOF5-ROBOT)))

(:derived (Flag99-)(and (RIGHTOF13-ROBOT)(LEFTOF4-ROBOT)))

(:derived (Flag100-)(and (RIGHTOF3-ROBOT)(LEFTOF4-ROBOT)))

(:derived (Flag101-)(and (LEFTOF4-ROBOT)(RIGHTOF11-ROBOT)))

(:derived (Flag103-)(and (RIGHTOF9-ROBOT)(LEFTOF5-ROBOT)))

(:derived (Flag104-)(and (RIGHTOF15-ROBOT)(LEFTOF5-ROBOT)))

(:derived (Flag105-)(and (RIGHTOF4-ROBOT)(LEFTOF5-ROBOT)))

(:derived (Flag106-)(and (RIGHTOF7-ROBOT)(LEFTOF5-ROBOT)))

(:derived (Flag107-)(and (RIGHTOF11-ROBOT)(LEFTOF5-ROBOT)))

(:derived (Flag108-)(and (RIGHTOF13-ROBOT)(LEFTOF5-ROBOT)))

(:derived (Flag109-)(and (RIGHTOF16-ROBOT)(LEFTOF5-ROBOT)))

(:derived (Flag110-)(and (LEFTOF4-ROBOT)(RIGHTOF8-ROBOT)))

(:derived (Flag111-)(and (LEFTOF5-ROBOT)(RIGHTOF6-ROBOT)))

(:derived (Flag112-)(and (RIGHTOF10-ROBOT)(LEFTOF5-ROBOT)))

(:derived (Flag113-)(and (LEFTOF4-ROBOT)(RIGHTOF10-ROBOT)))

(:derived (Flag114-)(and (LEFTOF5-ROBOT)(RIGHTOF12-ROBOT)))

(:derived (Flag115-)(and (RIGHTOF8-ROBOT)(LEFTOF5-ROBOT)))

(:derived (Flag116-)(and (RIGHTOF14-ROBOT)(LEFTOF5-ROBOT)))

(:derived (Flag117-)(and (RIGHTOF5-ROBOT)(LEFTOF5-ROBOT)))

(:derived (Flag119-)(and (RIGHTOF15-ROBOT)(LEFTOF6-ROBOT)))

(:derived (Flag120-)(and (RIGHTOF12-ROBOT)(LEFTOF6-ROBOT)))

(:derived (Flag121-)(and (RIGHTOF8-ROBOT)(LEFTOF6-ROBOT)))

(:derived (Flag122-)(and (RIGHTOF11-ROBOT)(LEFTOF6-ROBOT)))

(:derived (Flag123-)(and (RIGHTOF5-ROBOT)(LEFTOF6-ROBOT)))

(:derived (Flag124-)(and (RIGHTOF16-ROBOT)(LEFTOF6-ROBOT)))

(:derived (Flag125-)(and (RIGHTOF7-ROBOT)(LEFTOF6-ROBOT)))

(:derived (Flag126-)(and (RIGHTOF10-ROBOT)(LEFTOF6-ROBOT)))

(:derived (Flag127-)(and (RIGHTOF6-ROBOT)(LEFTOF6-ROBOT)))

(:derived (Flag128-)(and (RIGHTOF13-ROBOT)(LEFTOF6-ROBOT)))

(:derived (Flag129-)(and (RIGHTOF14-ROBOT)(LEFTOF6-ROBOT)))

(:derived (Flag130-)(and (RIGHTOF9-ROBOT)(LEFTOF6-ROBOT)))

(:derived (Flag132-)(and (RIGHTOF11-ROBOT)(LEFTOF7-ROBOT)))

(:derived (Flag133-)(and (RIGHTOF8-ROBOT)(LEFTOF7-ROBOT)))

(:derived (Flag134-)(and (LEFTOF7-ROBOT)(RIGHTOF7-ROBOT)))

(:derived (Flag135-)(and (RIGHTOF13-ROBOT)(LEFTOF7-ROBOT)))

(:derived (Flag136-)(and (LEFTOF6-ROBOT)(RIGHTOF6-ROBOT)))

(:derived (Flag137-)(and (LEFTOF7-ROBOT)(RIGHTOF12-ROBOT)))

(:derived (Flag138-)(and (RIGHTOF16-ROBOT)(LEFTOF7-ROBOT)))

(:derived (Flag139-)(and (LEFTOF7-ROBOT)(RIGHTOF14-ROBOT)))

(:derived (Flag140-)(and (LEFTOF7-ROBOT)(RIGHTOF9-ROBOT)))

(:derived (Flag141-)(and (LEFTOF7-ROBOT)(RIGHTOF6-ROBOT)))

(:derived (Flag142-)(and (LEFTOF6-ROBOT)(RIGHTOF12-ROBOT)))

(:derived (Flag143-)(and (RIGHTOF10-ROBOT)(LEFTOF7-ROBOT)))

(:derived (Flag144-)(and (LEFTOF7-ROBOT)(RIGHTOF15-ROBOT)))

(:derived (Flag146-)(and (RIGHTOF7-ROBOT)(LEFTOF8-ROBOT)))

(:derived (Flag147-)(and (LEFTOF8-ROBOT)(RIGHTOF9-ROBOT)))

(:derived (Flag148-)(and (RIGHTOF14-ROBOT)(LEFTOF8-ROBOT)))

(:derived (Flag149-)(and (RIGHTOF16-ROBOT)(LEFTOF8-ROBOT)))

(:derived (Flag150-)(and (RIGHTOF10-ROBOT)(LEFTOF8-ROBOT)))

(:derived (Flag151-)(and (RIGHTOF8-ROBOT)(LEFTOF8-ROBOT)))

(:derived (Flag152-)(and (RIGHTOF7-ROBOT)(LEFTOF7-ROBOT)))

(:derived (Flag153-)(and (LEFTOF8-ROBOT)(RIGHTOF12-ROBOT)))

(:derived (Flag154-)(and (LEFTOF8-ROBOT)(RIGHTOF15-ROBOT)))

(:derived (Flag155-)(and (RIGHTOF13-ROBOT)(LEFTOF8-ROBOT)))

(:derived (Flag156-)(and (RIGHTOF11-ROBOT)(LEFTOF8-ROBOT)))

(:derived (Flag158-)(and (LEFTOF9-ROBOT)(RIGHTOF16-ROBOT)))

(:derived (Flag159-)(and (LEFTOF9-ROBOT)(RIGHTOF15-ROBOT)))

(:derived (Flag160-)(and (RIGHTOF13-ROBOT)(LEFTOF9-ROBOT)))

(:derived (Flag161-)(and (RIGHTOF8-ROBOT)(LEFTOF9-ROBOT)))

(:derived (Flag162-)(and (LEFTOF9-ROBOT)(RIGHTOF9-ROBOT)))

(:derived (Flag163-)(and (RIGHTOF10-ROBOT)(LEFTOF9-ROBOT)))

(:derived (Flag164-)(and (LEFTOF8-ROBOT)(RIGHTOF14-ROBOT)))

(:derived (Flag165-)(and (LEFTOF9-ROBOT)(RIGHTOF12-ROBOT)))

(:derived (Flag166-)(and (RIGHTOF11-ROBOT)(LEFTOF9-ROBOT)))

(:derived (Flag167-)(and (LEFTOF9-ROBOT)(RIGHTOF14-ROBOT)))

(:derived (Flag169-)(and (RIGHTOF14-ROBOT)(LEFTOF10-ROBOT)))

(:derived (Flag170-)(and (RIGHTOF11-ROBOT)(LEFTOF10-ROBOT)))

(:derived (Flag171-)(and (RIGHTOF10-ROBOT)(LEFTOF10-ROBOT)))

(:derived (Flag172-)(and (LEFTOF10-ROBOT)(RIGHTOF12-ROBOT)))

(:derived (Flag173-)(and (RIGHTOF13-ROBOT)(LEFTOF10-ROBOT)))

(:derived (Flag174-)(and (LEFTOF10-ROBOT)(RIGHTOF9-ROBOT)))

(:derived (Flag175-)(and (RIGHTOF16-ROBOT)(LEFTOF10-ROBOT)))

(:derived (Flag176-)(and (LEFTOF10-ROBOT)(RIGHTOF15-ROBOT)))

(:derived (Flag178-)(and (LEFTOF11-ROBOT)(RIGHTOF14-ROBOT)))

(:derived (Flag179-)(and (RIGHTOF11-ROBOT)(LEFTOF11-ROBOT)))

(:derived (Flag180-)(and (LEFTOF11-ROBOT)(RIGHTOF16-ROBOT)))

(:derived (Flag181-)(and (LEFTOF10-ROBOT)(RIGHTOF14-ROBOT)))

(:derived (Flag182-)(and (RIGHTOF13-ROBOT)(LEFTOF11-ROBOT)))

(:derived (Flag183-)(and (LEFTOF11-ROBOT)(RIGHTOF15-ROBOT)))

(:derived (Flag184-)(and (RIGHTOF10-ROBOT)(LEFTOF11-ROBOT)))

(:derived (Flag185-)(and (LEFTOF11-ROBOT)(RIGHTOF12-ROBOT)))

(:derived (Flag187-)(and (RIGHTOF12-ROBOT)(LEFTOF12-ROBOT)))

(:derived (Flag188-)(and (RIGHTOF15-ROBOT)(LEFTOF12-ROBOT)))

(:derived (Flag189-)(and (RIGHTOF16-ROBOT)(LEFTOF12-ROBOT)))

(:derived (Flag190-)(and (RIGHTOF14-ROBOT)(LEFTOF12-ROBOT)))

(:derived (Flag191-)(and (RIGHTOF11-ROBOT)(LEFTOF12-ROBOT)))

(:derived (Flag192-)(and (RIGHTOF13-ROBOT)(LEFTOF12-ROBOT)))

(:derived (Flag194-)(and (LEFTOF13-ROBOT)(RIGHTOF15-ROBOT)))

(:derived (Flag195-)(and (LEFTOF13-ROBOT)(RIGHTOF12-ROBOT)))

(:derived (Flag196-)(and (LEFTOF13-ROBOT)(RIGHTOF14-ROBOT)))

(:derived (Flag197-)(and (RIGHTOF13-ROBOT)(LEFTOF13-ROBOT)))

(:derived (Flag198-)(and (RIGHTOF16-ROBOT)(LEFTOF13-ROBOT)))

(:derived (Flag199-)(and (LEFTOF12-ROBOT)(RIGHTOF12-ROBOT)))

(:derived (Flag201-)(and (LEFTOF14-ROBOT)(RIGHTOF14-ROBOT)))

(:derived (Flag202-)(and (RIGHTOF13-ROBOT)(LEFTOF14-ROBOT)))

(:derived (Flag203-)(and (LEFTOF14-ROBOT)(RIGHTOF16-ROBOT)))

(:derived (Flag204-)(and (LEFTOF14-ROBOT)(RIGHTOF15-ROBOT)))

(:derived (Flag206-)(and (RIGHTOF16-ROBOT)(LEFTOF15-ROBOT)))

(:derived (Flag207-)(and (RIGHTOF15-ROBOT)(LEFTOF15-ROBOT)))

(:derived (Flag208-)(and (RIGHTOF14-ROBOT)(LEFTOF15-ROBOT)))

(:derived (Flag210-)(and (LEFTOF15-ROBOT)(RIGHTOF15-ROBOT)))

(:derived (Flag211-)(and (RIGHTOF16-ROBOT)(LEFTOF16-ROBOT)))

(:derived (Flag212-)(and (LEFTOF16-ROBOT)(RIGHTOF15-ROBOT)))

(:derived (Flag214-)(and (LEFTOF17-ROBOT)(RIGHTOF16-ROBOT)))

(:derived (Flag262-)(and (Flag246-)))

(:derived (Flag262-)(and (Flag247-)))

(:derived (Flag262-)(and (Flag248-)))

(:derived (Flag262-)(and (Flag249-)))

(:derived (Flag262-)(and (Flag250-)))

(:derived (Flag262-)(and (Flag251-)))

(:derived (Flag262-)(and (Flag252-)))

(:derived (Flag262-)(and (Flag253-)))

(:derived (Flag262-)(and (Flag254-)))

(:derived (Flag262-)(and (Flag255-)))

(:derived (Flag262-)(and (Flag256-)))

(:derived (Flag262-)(and (Flag257-)))

(:derived (Flag262-)(and (Flag258-)))

(:derived (Flag262-)(and (Flag259-)))

(:derived (Flag262-)(and (Flag260-)))

(:derived (Flag262-)(and (Flag261-)))

(:derived (Flag279-)(and (Flag248-)))

(:derived (Flag279-)(and (Flag252-)))

(:derived (Flag279-)(and (Flag263-)))

(:derived (Flag279-)(and (Flag264-)))

(:derived (Flag279-)(and (Flag265-)))

(:derived (Flag279-)(and (Flag266-)))

(:derived (Flag279-)(and (Flag256-)))

(:derived (Flag279-)(and (Flag251-)))

(:derived (Flag279-)(and (Flag267-)))

(:derived (Flag279-)(and (Flag254-)))

(:derived (Flag279-)(and (Flag268-)))

(:derived (Flag279-)(and (Flag269-)))

(:derived (Flag279-)(and (Flag270-)))

(:derived (Flag279-)(and (Flag271-)))

(:derived (Flag279-)(and (Flag247-)))

(:derived (Flag279-)(and (Flag272-)))

(:derived (Flag279-)(and (Flag250-)))

(:derived (Flag279-)(and (Flag253-)))

(:derived (Flag279-)(and (Flag255-)))

(:derived (Flag279-)(and (Flag257-)))

(:derived (Flag279-)(and (Flag273-)))

(:derived (Flag279-)(and (Flag260-)))

(:derived (Flag279-)(and (Flag261-)))

(:derived (Flag279-)(and (Flag274-)))

(:derived (Flag279-)(and (Flag275-)))

(:derived (Flag279-)(and (Flag246-)))

(:derived (Flag279-)(and (Flag276-)))

(:derived (Flag279-)(and (Flag249-)))

(:derived (Flag279-)(and (Flag277-)))

(:derived (Flag279-)(and (Flag258-)))

(:derived (Flag279-)(and (Flag259-)))

(:derived (Flag279-)(and (Flag278-)))

(:derived (Flag296-)(and (Flag280-)))

(:derived (Flag296-)(and (Flag256-)))

(:derived (Flag296-)(and (Flag281-)))

(:derived (Flag296-)(and (Flag247-)))

(:derived (Flag296-)(and (Flag282-)))

(:derived (Flag296-)(and (Flag252-)))

(:derived (Flag296-)(and (Flag273-)))

(:derived (Flag296-)(and (Flag264-)))

(:derived (Flag296-)(and (Flag265-)))

(:derived (Flag296-)(and (Flag283-)))

(:derived (Flag296-)(and (Flag266-)))

(:derived (Flag296-)(and (Flag251-)))

(:derived (Flag296-)(and (Flag284-)))

(:derived (Flag296-)(and (Flag254-)))

(:derived (Flag296-)(and (Flag285-)))

(:derived (Flag296-)(and (Flag286-)))

(:derived (Flag296-)(and (Flag268-)))

(:derived (Flag296-)(and (Flag269-)))

(:derived (Flag296-)(and (Flag270-)))

(:derived (Flag296-)(and (Flag287-)))

(:derived (Flag296-)(and (Flag271-)))

(:derived (Flag296-)(and (Flag288-)))

(:derived (Flag296-)(and (Flag272-)))

(:derived (Flag296-)(and (Flag250-)))

(:derived (Flag296-)(and (Flag289-)))

(:derived (Flag296-)(and (Flag253-)))

(:derived (Flag296-)(and (Flag290-)))

(:derived (Flag296-)(and (Flag257-)))

(:derived (Flag296-)(and (Flag255-)))

(:derived (Flag296-)(and (Flag291-)))

(:derived (Flag296-)(and (Flag292-)))

(:derived (Flag296-)(and (Flag260-)))

(:derived (Flag296-)(and (Flag261-)))

(:derived (Flag296-)(and (Flag274-)))

(:derived (Flag296-)(and (Flag275-)))

(:derived (Flag296-)(and (Flag246-)))

(:derived (Flag296-)(and (Flag276-)))

(:derived (Flag296-)(and (Flag293-)))

(:derived (Flag296-)(and (Flag249-)))

(:derived (Flag296-)(and (Flag294-)))

(:derived (Flag296-)(and (Flag295-)))

(:derived (Flag296-)(and (Flag277-)))

(:derived (Flag296-)(and (Flag258-)))

(:derived (Flag296-)(and (Flag259-)))

(:derived (Flag296-)(and (Flag278-)))

(:derived (Flag313-)(and (Flag283-)))

(:derived (Flag313-)(and (Flag280-)))

(:derived (Flag313-)(and (Flag297-)))

(:derived (Flag313-)(and (Flag298-)))

(:derived (Flag313-)(and (Flag299-)))

(:derived (Flag313-)(and (Flag300-)))

(:derived (Flag313-)(and (Flag301-)))

(:derived (Flag313-)(and (Flag281-)))

(:derived (Flag313-)(and (Flag282-)))

(:derived (Flag313-)(and (Flag256-)))

(:derived (Flag313-)(and (Flag252-)))

(:derived (Flag313-)(and (Flag273-)))

(:derived (Flag313-)(and (Flag264-)))

(:derived (Flag313-)(and (Flag285-)))

(:derived (Flag313-)(and (Flag265-)))

(:derived (Flag313-)(and (Flag302-)))

(:derived (Flag313-)(and (Flag266-)))

(:derived (Flag313-)(and (Flag303-)))

(:derived (Flag313-)(and (Flag251-)))

(:derived (Flag313-)(and (Flag284-)))

(:derived (Flag313-)(and (Flag254-)))

(:derived (Flag313-)(and (Flag304-)))

(:derived (Flag313-)(and (Flag275-)))

(:derived (Flag313-)(and (Flag286-)))

(:derived (Flag313-)(and (Flag268-)))

(:derived (Flag313-)(and (Flag269-)))

(:derived (Flag313-)(and (Flag305-)))

(:derived (Flag313-)(and (Flag306-)))

(:derived (Flag313-)(and (Flag307-)))

(:derived (Flag313-)(and (Flag271-)))

(:derived (Flag313-)(and (Flag272-)))

(:derived (Flag313-)(and (Flag250-)))

(:derived (Flag313-)(and (Flag289-)))

(:derived (Flag313-)(and (Flag253-)))

(:derived (Flag313-)(and (Flag290-)))

(:derived (Flag313-)(and (Flag276-)))

(:derived (Flag313-)(and (Flag257-)))

(:derived (Flag313-)(and (Flag255-)))

(:derived (Flag313-)(and (Flag291-)))

(:derived (Flag313-)(and (Flag308-)))

(:derived (Flag313-)(and (Flag260-)))

(:derived (Flag313-)(and (Flag261-)))

(:derived (Flag313-)(and (Flag274-)))

(:derived (Flag313-)(and (Flag309-)))

(:derived (Flag313-)(and (Flag246-)))

(:derived (Flag313-)(and (Flag310-)))

(:derived (Flag313-)(and (Flag293-)))

(:derived (Flag313-)(and (Flag311-)))

(:derived (Flag313-)(and (Flag312-)))

(:derived (Flag313-)(and (Flag294-)))

(:derived (Flag313-)(and (Flag270-)))

(:derived (Flag313-)(and (Flag249-)))

(:derived (Flag313-)(and (Flag295-)))

(:derived (Flag313-)(and (Flag277-)))

(:derived (Flag313-)(and (Flag258-)))

(:derived (Flag313-)(and (Flag259-)))

(:derived (Flag313-)(and (Flag278-)))

(:derived (Flag328-)(and (Flag283-)))

(:derived (Flag328-)(and (Flag314-)))

(:derived (Flag328-)(and (Flag280-)))

(:derived (Flag328-)(and (Flag297-)))

(:derived (Flag328-)(and (Flag298-)))

(:derived (Flag328-)(and (Flag300-)))

(:derived (Flag328-)(and (Flag315-)))

(:derived (Flag328-)(and (Flag281-)))

(:derived (Flag328-)(and (Flag316-)))

(:derived (Flag328-)(and (Flag282-)))

(:derived (Flag328-)(and (Flag256-)))

(:derived (Flag328-)(and (Flag317-)))

(:derived (Flag328-)(and (Flag252-)))

(:derived (Flag328-)(and (Flag264-)))

(:derived (Flag328-)(and (Flag302-)))

(:derived (Flag328-)(and (Flag265-)))

(:derived (Flag328-)(and (Flag309-)))

(:derived (Flag328-)(and (Flag266-)))

(:derived (Flag328-)(and (Flag303-)))

(:derived (Flag328-)(and (Flag251-)))

(:derived (Flag328-)(and (Flag277-)))

(:derived (Flag328-)(and (Flag284-)))

(:derived (Flag328-)(and (Flag254-)))

(:derived (Flag328-)(and (Flag318-)))

(:derived (Flag328-)(and (Flag304-)))

(:derived (Flag328-)(and (Flag319-)))

(:derived (Flag328-)(and (Flag286-)))

(:derived (Flag328-)(and (Flag268-)))

(:derived (Flag328-)(and (Flag269-)))

(:derived (Flag328-)(and (Flag305-)))

(:derived (Flag328-)(and (Flag306-)))

(:derived (Flag328-)(and (Flag285-)))

(:derived (Flag328-)(and (Flag271-)))

(:derived (Flag328-)(and (Flag320-)))

(:derived (Flag328-)(and (Flag272-)))

(:derived (Flag328-)(and (Flag250-)))

(:derived (Flag328-)(and (Flag289-)))

(:derived (Flag328-)(and (Flag253-)))

(:derived (Flag328-)(and (Flag321-)))

(:derived (Flag328-)(and (Flag290-)))

(:derived (Flag328-)(and (Flag322-)))

(:derived (Flag328-)(and (Flag276-)))

(:derived (Flag328-)(and (Flag257-)))

(:derived (Flag328-)(and (Flag255-)))

(:derived (Flag328-)(and (Flag273-)))

(:derived (Flag328-)(and (Flag308-)))

(:derived (Flag328-)(and (Flag301-)))

(:derived (Flag328-)(and (Flag261-)))

(:derived (Flag328-)(and (Flag274-)))

(:derived (Flag328-)(and (Flag275-)))

(:derived (Flag328-)(and (Flag246-)))

(:derived (Flag328-)(and (Flag323-)))

(:derived (Flag328-)(and (Flag310-)))

(:derived (Flag328-)(and (Flag293-)))

(:derived (Flag328-)(and (Flag311-)))

(:derived (Flag328-)(and (Flag324-)))

(:derived (Flag328-)(and (Flag312-)))

(:derived (Flag328-)(and (Flag294-)))

(:derived (Flag328-)(and (Flag270-)))

(:derived (Flag328-)(and (Flag249-)))

(:derived (Flag328-)(and (Flag325-)))

(:derived (Flag328-)(and (Flag295-)))

(:derived (Flag328-)(and (Flag326-)))

(:derived (Flag328-)(and (Flag258-)))

(:derived (Flag328-)(and (Flag259-)))

(:derived (Flag328-)(and (Flag291-)))

(:derived (Flag328-)(and (Flag327-)))

(:derived (Flag343-)(and (Flag312-)))

(:derived (Flag343-)(and (Flag283-)))

(:derived (Flag343-)(and (Flag280-)))

(:derived (Flag343-)(and (Flag297-)))

(:derived (Flag343-)(and (Flag277-)))

(:derived (Flag343-)(and (Flag329-)))

(:derived (Flag343-)(and (Flag298-)))

(:derived (Flag343-)(and (Flag330-)))

(:derived (Flag343-)(and (Flag315-)))

(:derived (Flag343-)(and (Flag331-)))

(:derived (Flag343-)(and (Flag271-)))

(:derived (Flag343-)(and (Flag316-)))

(:derived (Flag343-)(and (Flag282-)))

(:derived (Flag343-)(and (Flag256-)))

(:derived (Flag343-)(and (Flag253-)))

(:derived (Flag343-)(and (Flag332-)))

(:derived (Flag343-)(and (Flag333-)))

(:derived (Flag343-)(and (Flag317-)))

(:derived (Flag343-)(and (Flag252-)))

(:derived (Flag343-)(and (Flag334-)))

(:derived (Flag343-)(and (Flag264-)))

(:derived (Flag343-)(and (Flag285-)))

(:derived (Flag343-)(and (Flag265-)))

(:derived (Flag343-)(and (Flag309-)))

(:derived (Flag343-)(and (Flag261-)))

(:derived (Flag343-)(and (Flag266-)))

(:derived (Flag343-)(and (Flag272-)))

(:derived (Flag343-)(and (Flag251-)))

(:derived (Flag343-)(and (Flag324-)))

(:derived (Flag343-)(and (Flag284-)))

(:derived (Flag343-)(and (Flag254-)))

(:derived (Flag343-)(and (Flag335-)))

(:derived (Flag343-)(and (Flag336-)))

(:derived (Flag343-)(and (Flag318-)))

(:derived (Flag343-)(and (Flag304-)))

(:derived (Flag343-)(and (Flag319-)))

(:derived (Flag343-)(and (Flag286-)))

(:derived (Flag343-)(and (Flag268-)))

(:derived (Flag343-)(and (Flag300-)))

(:derived (Flag343-)(and (Flag305-)))

(:derived (Flag343-)(and (Flag306-)))

(:derived (Flag343-)(and (Flag281-)))

(:derived (Flag343-)(and (Flag303-)))

(:derived (Flag343-)(and (Flag250-)))

(:derived (Flag343-)(and (Flag289-)))

(:derived (Flag343-)(and (Flag337-)))

(:derived (Flag343-)(and (Flag255-)))

(:derived (Flag343-)(and (Flag322-)))

(:derived (Flag343-)(and (Flag276-)))

(:derived (Flag343-)(and (Flag257-)))

(:derived (Flag343-)(and (Flag291-)))

(:derived (Flag343-)(and (Flag308-)))

(:derived (Flag343-)(and (Flag338-)))

(:derived (Flag343-)(and (Flag301-)))

(:derived (Flag343-)(and (Flag339-)))

(:derived (Flag343-)(and (Flag274-)))

(:derived (Flag343-)(and (Flag275-)))

(:derived (Flag343-)(and (Flag320-)))

(:derived (Flag343-)(and (Flag323-)))

(:derived (Flag343-)(and (Flag310-)))

(:derived (Flag343-)(and (Flag293-)))

(:derived (Flag343-)(and (Flag311-)))

(:derived (Flag343-)(and (Flag340-)))

(:derived (Flag343-)(and (Flag294-)))

(:derived (Flag343-)(and (Flag270-)))

(:derived (Flag343-)(and (Flag341-)))

(:derived (Flag343-)(and (Flag249-)))

(:derived (Flag343-)(and (Flag325-)))

(:derived (Flag343-)(and (Flag295-)))

(:derived (Flag343-)(and (Flag326-)))

(:derived (Flag343-)(and (Flag342-)))

(:derived (Flag343-)(and (Flag258-)))

(:derived (Flag343-)(and (Flag259-)))

(:derived (Flag343-)(and (Flag273-)))

(:derived (Flag343-)(and (Flag327-)))

(:derived (Flag357-)(and (Flag249-)))

(:derived (Flag357-)(and (Flag283-)))

(:derived (Flag357-)(and (Flag312-)))

(:derived (Flag357-)(and (Flag284-)))

(:derived (Flag357-)(and (Flag344-)))

(:derived (Flag357-)(and (Flag298-)))

(:derived (Flag357-)(and (Flag315-)))

(:derived (Flag357-)(and (Flag331-)))

(:derived (Flag357-)(and (Flag271-)))

(:derived (Flag357-)(and (Flag316-)))

(:derived (Flag357-)(and (Flag345-)))

(:derived (Flag357-)(and (Flag282-)))

(:derived (Flag357-)(and (Flag256-)))

(:derived (Flag357-)(and (Flag332-)))

(:derived (Flag357-)(and (Flag333-)))

(:derived (Flag357-)(and (Flag346-)))

(:derived (Flag357-)(and (Flag317-)))

(:derived (Flag357-)(and (Flag252-)))

(:derived (Flag357-)(and (Flag334-)))

(:derived (Flag357-)(and (Flag347-)))

(:derived (Flag357-)(and (Flag264-)))

(:derived (Flag357-)(and (Flag285-)))

(:derived (Flag357-)(and (Flag265-)))

(:derived (Flag357-)(and (Flag309-)))

(:derived (Flag357-)(and (Flag261-)))

(:derived (Flag357-)(and (Flag266-)))

(:derived (Flag357-)(and (Flag303-)))

(:derived (Flag357-)(and (Flag251-)))

(:derived (Flag357-)(and (Flag324-)))

(:derived (Flag357-)(and (Flag297-)))

(:derived (Flag357-)(and (Flag335-)))

(:derived (Flag357-)(and (Flag318-)))

(:derived (Flag357-)(and (Flag304-)))

(:derived (Flag357-)(and (Flag319-)))

(:derived (Flag357-)(and (Flag286-)))

(:derived (Flag357-)(and (Flag268-)))

(:derived (Flag357-)(and (Flag300-)))

(:derived (Flag357-)(and (Flag329-)))

(:derived (Flag357-)(and (Flag305-)))

(:derived (Flag357-)(and (Flag270-)))

(:derived (Flag357-)(and (Flag348-)))

(:derived (Flag357-)(and (Flag281-)))

(:derived (Flag357-)(and (Flag349-)))

(:derived (Flag357-)(and (Flag272-)))

(:derived (Flag357-)(and (Flag250-)))

(:derived (Flag357-)(and (Flag289-)))

(:derived (Flag357-)(and (Flag253-)))

(:derived (Flag357-)(and (Flag255-)))

(:derived (Flag357-)(and (Flag322-)))

(:derived (Flag357-)(and (Flag276-)))

(:derived (Flag357-)(and (Flag257-)))

(:derived (Flag357-)(and (Flag291-)))

(:derived (Flag357-)(and (Flag308-)))

(:derived (Flag357-)(and (Flag338-)))

(:derived (Flag357-)(and (Flag301-)))

(:derived (Flag357-)(and (Flag350-)))

(:derived (Flag357-)(and (Flag351-)))

(:derived (Flag357-)(and (Flag339-)))

(:derived (Flag357-)(and (Flag274-)))

(:derived (Flag357-)(and (Flag275-)))

(:derived (Flag357-)(and (Flag320-)))

(:derived (Flag357-)(and (Flag323-)))

(:derived (Flag357-)(and (Flag310-)))

(:derived (Flag357-)(and (Flag352-)))

(:derived (Flag357-)(and (Flag353-)))

(:derived (Flag357-)(and (Flag293-)))

(:derived (Flag357-)(and (Flag354-)))

(:derived (Flag357-)(and (Flag340-)))

(:derived (Flag357-)(and (Flag294-)))

(:derived (Flag357-)(and (Flag355-)))

(:derived (Flag357-)(and (Flag341-)))

(:derived (Flag357-)(and (Flag311-)))

(:derived (Flag357-)(and (Flag325-)))

(:derived (Flag357-)(and (Flag295-)))

(:derived (Flag357-)(and (Flag326-)))

(:derived (Flag357-)(and (Flag342-)))

(:derived (Flag357-)(and (Flag258-)))

(:derived (Flag357-)(and (Flag259-)))

(:derived (Flag357-)(and (Flag356-)))

(:derived (Flag357-)(and (Flag273-)))

(:derived (Flag357-)(and (Flag327-)))

(:derived (Flag369-)(and (Flag249-)))

(:derived (Flag369-)(and (Flag358-)))

(:derived (Flag369-)(and (Flag283-)))

(:derived (Flag369-)(and (Flag312-)))

(:derived (Flag369-)(and (Flag284-)))

(:derived (Flag369-)(and (Flag276-)))

(:derived (Flag369-)(and (Flag329-)))

(:derived (Flag369-)(and (Flag298-)))

(:derived (Flag369-)(and (Flag331-)))

(:derived (Flag369-)(and (Flag353-)))

(:derived (Flag369-)(and (Flag359-)))

(:derived (Flag369-)(and (Flag345-)))

(:derived (Flag369-)(and (Flag282-)))

(:derived (Flag369-)(and (Flag256-)))

(:derived (Flag369-)(and (Flag349-)))

(:derived (Flag369-)(and (Flag332-)))

(:derived (Flag369-)(and (Flag333-)))

(:derived (Flag369-)(and (Flag346-)))

(:derived (Flag369-)(and (Flag317-)))

(:derived (Flag369-)(and (Flag252-)))

(:derived (Flag369-)(and (Flag334-)))

(:derived (Flag369-)(and (Flag273-)))

(:derived (Flag369-)(and (Flag264-)))

(:derived (Flag369-)(and (Flag285-)))

(:derived (Flag369-)(and (Flag265-)))

(:derived (Flag369-)(and (Flag309-)))

(:derived (Flag369-)(and (Flag261-)))

(:derived (Flag369-)(and (Flag266-)))

(:derived (Flag369-)(and (Flag303-)))

(:derived (Flag369-)(and (Flag251-)))

(:derived (Flag369-)(and (Flag297-)))

(:derived (Flag369-)(and (Flag335-)))

(:derived (Flag369-)(and (Flag360-)))

(:derived (Flag369-)(and (Flag250-)))

(:derived (Flag369-)(and (Flag318-)))

(:derived (Flag369-)(and (Flag304-)))

(:derived (Flag369-)(and (Flag319-)))

(:derived (Flag369-)(and (Flag361-)))

(:derived (Flag369-)(and (Flag268-)))

(:derived (Flag369-)(and (Flag316-)))

(:derived (Flag369-)(and (Flag305-)))

(:derived (Flag369-)(and (Flag270-)))

(:derived (Flag369-)(and (Flag259-)))

(:derived (Flag369-)(and (Flag348-)))

(:derived (Flag369-)(and (Flag300-)))

(:derived (Flag369-)(and (Flag362-)))

(:derived (Flag369-)(and (Flag272-)))

(:derived (Flag369-)(and (Flag286-)))

(:derived (Flag369-)(and (Flag289-)))

(:derived (Flag369-)(and (Flag255-)))

(:derived (Flag369-)(and (Flag322-)))

(:derived (Flag369-)(and (Flag363-)))

(:derived (Flag369-)(and (Flag257-)))

(:derived (Flag369-)(and (Flag291-)))

(:derived (Flag369-)(and (Flag308-)))

(:derived (Flag369-)(and (Flag338-)))

(:derived (Flag369-)(and (Flag315-)))

(:derived (Flag369-)(and (Flag350-)))

(:derived (Flag369-)(and (Flag351-)))

(:derived (Flag369-)(and (Flag339-)))

(:derived (Flag369-)(and (Flag274-)))

(:derived (Flag369-)(and (Flag275-)))

(:derived (Flag369-)(and (Flag364-)))

(:derived (Flag369-)(and (Flag320-)))

(:derived (Flag369-)(and (Flag323-)))

(:derived (Flag369-)(and (Flag310-)))

(:derived (Flag369-)(and (Flag352-)))

(:derived (Flag369-)(and (Flag293-)))

(:derived (Flag369-)(and (Flag311-)))

(:derived (Flag369-)(and (Flag340-)))

(:derived (Flag369-)(and (Flag294-)))

(:derived (Flag369-)(and (Flag355-)))

(:derived (Flag369-)(and (Flag341-)))

(:derived (Flag369-)(and (Flag365-)))

(:derived (Flag369-)(and (Flag325-)))

(:derived (Flag369-)(and (Flag295-)))

(:derived (Flag369-)(and (Flag324-)))

(:derived (Flag369-)(and (Flag342-)))

(:derived (Flag369-)(and (Flag258-)))

(:derived (Flag369-)(and (Flag366-)))

(:derived (Flag369-)(and (Flag356-)))

(:derived (Flag369-)(and (Flag367-)))

(:derived (Flag369-)(and (Flag368-)))

(:derived (Flag369-)(and (Flag327-)))

(:derived (Flag381-)(and (Flag316-)))

(:derived (Flag381-)(and (Flag266-)))

(:derived (Flag381-)(and (Flag284-)))

(:derived (Flag381-)(and (Flag285-)))

(:derived (Flag381-)(and (Flag319-)))

(:derived (Flag381-)(and (Flag325-)))

(:derived (Flag381-)(and (Flag352-)))

(:derived (Flag381-)(and (Flag349-)))

(:derived (Flag381-)(and (Flag272-)))

(:derived (Flag381-)(and (Flag286-)))

(:derived (Flag381-)(and (Flag291-)))

(:derived (Flag381-)(and (Flag308-)))

(:derived (Flag381-)(and (Flag315-)))

(:derived (Flag381-)(and (Flag261-)))

(:derived (Flag381-)(and (Flag370-)))

(:derived (Flag381-)(and (Flag323-)))

(:derived (Flag381-)(and (Flag371-)))

(:derived (Flag381-)(and (Flag324-)))

(:derived (Flag381-)(and (Flag250-)))

(:derived (Flag381-)(and (Flag367-)))

(:derived (Flag381-)(and (Flag368-)))

(:derived (Flag381-)(and (Flag372-)))

(:derived (Flag381-)(and (Flag256-)))

(:derived (Flag381-)(and (Flag298-)))

(:derived (Flag381-)(and (Flag331-)))

(:derived (Flag381-)(and (Flag359-)))

(:derived (Flag381-)(and (Flag356-)))

(:derived (Flag381-)(and (Flag373-)))

(:derived (Flag381-)(and (Flag332-)))

(:derived (Flag381-)(and (Flag333-)))

(:derived (Flag381-)(and (Flag283-)))

(:derived (Flag381-)(and (Flag374-)))

(:derived (Flag381-)(and (Flag360-)))

(:derived (Flag381-)(and (Flag318-)))

(:derived (Flag381-)(and (Flag342-)))

(:derived (Flag381-)(and (Flag348-)))

(:derived (Flag381-)(and (Flag303-)))

(:derived (Flag381-)(and (Flag255-)))

(:derived (Flag381-)(and (Flag363-)))

(:derived (Flag381-)(and (Flag257-)))

(:derived (Flag381-)(and (Flag274-)))

(:derived (Flag381-)(and (Flag309-)))

(:derived (Flag381-)(and (Flag310-)))

(:derived (Flag381-)(and (Flag249-)))

(:derived (Flag381-)(and (Flag294-)))

(:derived (Flag381-)(and (Flag341-)))

(:derived (Flag381-)(and (Flag375-)))

(:derived (Flag381-)(and (Flag258-)))

(:derived (Flag381-)(and (Flag259-)))

(:derived (Flag381-)(and (Flag351-)))

(:derived (Flag381-)(and (Flag329-)))

(:derived (Flag381-)(and (Flag353-)))

(:derived (Flag381-)(and (Flag340-)))

(:derived (Flag381-)(and (Flag376-)))

(:derived (Flag381-)(and (Flag317-)))

(:derived (Flag381-)(and (Flag252-)))

(:derived (Flag381-)(and (Flag377-)))

(:derived (Flag381-)(and (Flag297-)))

(:derived (Flag381-)(and (Flag305-)))

(:derived (Flag381-)(and (Flag270-)))

(:derived (Flag381-)(and (Flag268-)))

(:derived (Flag381-)(and (Flag289-)))

(:derived (Flag381-)(and (Flag276-)))

(:derived (Flag381-)(and (Flag338-)))

(:derived (Flag381-)(and (Flag339-)))

(:derived (Flag381-)(and (Flag275-)))

(:derived (Flag381-)(and (Flag320-)))

(:derived (Flag381-)(and (Flag311-)))

(:derived (Flag381-)(and (Flag355-)))

(:derived (Flag381-)(and (Flag345-)))

(:derived (Flag381-)(and (Flag295-)))

(:derived (Flag381-)(and (Flag378-)))

(:derived (Flag381-)(and (Flag366-)))

(:derived (Flag381-)(and (Flag312-)))

(:derived (Flag381-)(and (Flag346-)))

(:derived (Flag381-)(and (Flag334-)))

(:derived (Flag381-)(and (Flag265-)))

(:derived (Flag381-)(and (Flag304-)))

(:derived (Flag381-)(and (Flag361-)))

(:derived (Flag381-)(and (Flag327-)))

(:derived (Flag381-)(and (Flag273-)))

(:derived (Flag381-)(and (Flag358-)))

(:derived (Flag381-)(and (Flag350-)))

(:derived (Flag381-)(and (Flag379-)))

(:derived (Flag381-)(and (Flag293-)))

(:derived (Flag381-)(and (Flag380-)))

(:derived (Flag391-)(and (Flag358-)))

(:derived (Flag391-)(and (Flag283-)))

(:derived (Flag391-)(and (Flag372-)))

(:derived (Flag391-)(and (Flag312-)))

(:derived (Flag391-)(and (Flag284-)))

(:derived (Flag391-)(and (Flag276-)))

(:derived (Flag391-)(and (Flag329-)))

(:derived (Flag391-)(and (Flag298-)))

(:derived (Flag391-)(and (Flag340-)))

(:derived (Flag391-)(and (Flag273-)))

(:derived (Flag391-)(and (Flag353-)))

(:derived (Flag391-)(and (Flag359-)))

(:derived (Flag391-)(and (Flag345-)))

(:derived (Flag391-)(and (Flag320-)))

(:derived (Flag391-)(and (Flag373-)))

(:derived (Flag391-)(and (Flag256-)))

(:derived (Flag391-)(and (Flag331-)))

(:derived (Flag391-)(and (Flag332-)))

(:derived (Flag391-)(and (Flag333-)))

(:derived (Flag391-)(and (Flag346-)))

(:derived (Flag391-)(and (Flag317-)))

(:derived (Flag391-)(and (Flag252-)))

(:derived (Flag391-)(and (Flag382-)))

(:derived (Flag391-)(and (Flag383-)))

(:derived (Flag391-)(and (Flag265-)))

(:derived (Flag391-)(and (Flag384-)))

(:derived (Flag391-)(and (Flag339-)))

(:derived (Flag391-)(and (Flag266-)))

(:derived (Flag391-)(and (Flag303-)))

(:derived (Flag391-)(and (Flag270-)))

(:derived (Flag391-)(and (Flag297-)))

(:derived (Flag391-)(and (Flag374-)))

(:derived (Flag391-)(and (Flag360-)))

(:derived (Flag391-)(and (Flag250-)))

(:derived (Flag391-)(and (Flag318-)))

(:derived (Flag391-)(and (Flag285-)))

(:derived (Flag391-)(and (Flag319-)))

(:derived (Flag391-)(and (Flag361-)))

(:derived (Flag391-)(and (Flag325-)))

(:derived (Flag391-)(and (Flag327-)))

(:derived (Flag391-)(and (Flag342-)))

(:derived (Flag391-)(and (Flag268-)))

(:derived (Flag391-)(and (Flag348-)))

(:derived (Flag391-)(and (Flag258-)))

(:derived (Flag391-)(and (Flag349-)))

(:derived (Flag391-)(and (Flag385-)))

(:derived (Flag391-)(and (Flag272-)))

(:derived (Flag391-)(and (Flag286-)))

(:derived (Flag391-)(and (Flag305-)))

(:derived (Flag391-)(and (Flag255-)))

(:derived (Flag391-)(and (Flag334-)))

(:derived (Flag391-)(and (Flag363-)))

(:derived (Flag391-)(and (Flag257-)))

(:derived (Flag391-)(and (Flag386-)))

(:derived (Flag391-)(and (Flag387-)))

(:derived (Flag391-)(and (Flag291-)))

(:derived (Flag391-)(and (Flag308-)))

(:derived (Flag391-)(and (Flag315-)))

(:derived (Flag391-)(and (Flag350-)))

(:derived (Flag391-)(and (Flag351-)))

(:derived (Flag391-)(and (Flag261-)))

(:derived (Flag391-)(and (Flag274-)))

(:derived (Flag391-)(and (Flag309-)))

(:derived (Flag391-)(and (Flag388-)))

(:derived (Flag391-)(and (Flag370-)))

(:derived (Flag391-)(and (Flag389-)))

(:derived (Flag391-)(and (Flag323-)))

(:derived (Flag391-)(and (Flag310-)))

(:derived (Flag391-)(and (Flag352-)))

(:derived (Flag391-)(and (Flag293-)))

(:derived (Flag391-)(and (Flag311-)))

(:derived (Flag391-)(and (Flag380-)))

(:derived (Flag391-)(and (Flag294-)))

(:derived (Flag391-)(and (Flag371-)))

(:derived (Flag391-)(and (Flag341-)))

(:derived (Flag391-)(and (Flag249-)))

(:derived (Flag391-)(and (Flag295-)))

(:derived (Flag391-)(and (Flag324-)))

(:derived (Flag391-)(and (Flag378-)))

(:derived (Flag391-)(and (Flag390-)))

(:derived (Flag391-)(and (Flag366-)))

(:derived (Flag391-)(and (Flag356-)))

(:derived (Flag391-)(and (Flag376-)))

(:derived (Flag391-)(and (Flag367-)))

(:derived (Flag391-)(and (Flag368-)))

(:derived (Flag399-)(and (Flag312-)))

(:derived (Flag399-)(and (Flag358-)))

(:derived (Flag399-)(and (Flag283-)))

(:derived (Flag399-)(and (Flag372-)))

(:derived (Flag399-)(and (Flag291-)))

(:derived (Flag399-)(and (Flag351-)))

(:derived (Flag399-)(and (Flag392-)))

(:derived (Flag399-)(and (Flag329-)))

(:derived (Flag399-)(and (Flag298-)))

(:derived (Flag399-)(and (Flag376-)))

(:derived (Flag399-)(and (Flag331-)))

(:derived (Flag399-)(and (Flag353-)))

(:derived (Flag399-)(and (Flag359-)))

(:derived (Flag399-)(and (Flag356-)))

(:derived (Flag399-)(and (Flag320-)))

(:derived (Flag399-)(and (Flag373-)))

(:derived (Flag399-)(and (Flag256-)))

(:derived (Flag399-)(and (Flag349-)))

(:derived (Flag399-)(and (Flag332-)))

(:derived (Flag399-)(and (Flag333-)))

(:derived (Flag399-)(and (Flag317-)))

(:derived (Flag399-)(and (Flag252-)))

(:derived (Flag399-)(and (Flag382-)))

(:derived (Flag399-)(and (Flag393-)))

(:derived (Flag399-)(and (Flag265-)))

(:derived (Flag399-)(and (Flag384-)))

(:derived (Flag399-)(and (Flag266-)))

(:derived (Flag399-)(and (Flag394-)))

(:derived (Flag399-)(and (Flag270-)))

(:derived (Flag399-)(and (Flag284-)))

(:derived (Flag399-)(and (Flag374-)))

(:derived (Flag399-)(and (Flag360-)))

(:derived (Flag399-)(and (Flag250-)))

(:derived (Flag399-)(and (Flag318-)))

(:derived (Flag399-)(and (Flag319-)))

(:derived (Flag399-)(and (Flag361-)))

(:derived (Flag399-)(and (Flag325-)))

(:derived (Flag399-)(and (Flag352-)))

(:derived (Flag399-)(and (Flag342-)))

(:derived (Flag399-)(and (Flag268-)))

(:derived (Flag399-)(and (Flag348-)))

(:derived (Flag399-)(and (Flag258-)))

(:derived (Flag399-)(and (Flag395-)))

(:derived (Flag399-)(and (Flag385-)))

(:derived (Flag399-)(and (Flag303-)))

(:derived (Flag399-)(and (Flag286-)))

(:derived (Flag399-)(and (Flag305-)))

(:derived (Flag399-)(and (Flag255-)))

(:derived (Flag399-)(and (Flag334-)))

(:derived (Flag399-)(and (Flag276-)))

(:derived (Flag399-)(and (Flag257-)))

(:derived (Flag399-)(and (Flag386-)))

(:derived (Flag399-)(and (Flag387-)))

(:derived (Flag399-)(and (Flag273-)))

(:derived (Flag399-)(and (Flag308-)))

(:derived (Flag399-)(and (Flag315-)))

(:derived (Flag399-)(and (Flag350-)))

(:derived (Flag399-)(and (Flag396-)))

(:derived (Flag399-)(and (Flag397-)))

(:derived (Flag399-)(and (Flag274-)))

(:derived (Flag399-)(and (Flag309-)))

(:derived (Flag399-)(and (Flag370-)))

(:derived (Flag399-)(and (Flag389-)))

(:derived (Flag399-)(and (Flag310-)))

(:derived (Flag399-)(and (Flag293-)))

(:derived (Flag399-)(and (Flag311-)))

(:derived (Flag399-)(and (Flag398-)))

(:derived (Flag399-)(and (Flag380-)))

(:derived (Flag399-)(and (Flag294-)))

(:derived (Flag399-)(and (Flag371-)))

(:derived (Flag399-)(and (Flag345-)))

(:derived (Flag399-)(and (Flag249-)))

(:derived (Flag399-)(and (Flag295-)))

(:derived (Flag399-)(and (Flag324-)))

(:derived (Flag399-)(and (Flag378-)))

(:derived (Flag399-)(and (Flag390-)))

(:derived (Flag399-)(and (Flag366-)))

(:derived (Flag399-)(and (Flag340-)))

(:derived (Flag399-)(and (Flag367-)))

(:derived (Flag399-)(and (Flag368-)))

(:derived (Flag399-)(and (Flag327-)))

(:derived (Flag406-)(and (Flag372-)))

(:derived (Flag406-)(and (Flag312-)))

(:derived (Flag406-)(and (Flag256-)))

(:derived (Flag406-)(and (Flag392-)))

(:derived (Flag406-)(and (Flag329-)))

(:derived (Flag406-)(and (Flag298-)))

(:derived (Flag406-)(and (Flag340-)))

(:derived (Flag406-)(and (Flag331-)))

(:derived (Flag406-)(and (Flag353-)))

(:derived (Flag406-)(and (Flag359-)))

(:derived (Flag406-)(and (Flag356-)))

(:derived (Flag406-)(and (Flag349-)))

(:derived (Flag406-)(and (Flag332-)))

(:derived (Flag406-)(and (Flag333-)))

(:derived (Flag406-)(and (Flag400-)))

(:derived (Flag406-)(and (Flag268-)))

(:derived (Flag406-)(and (Flag252-)))

(:derived (Flag406-)(and (Flag382-)))

(:derived (Flag406-)(and (Flag393-)))

(:derived (Flag406-)(and (Flag376-)))

(:derived (Flag406-)(and (Flag401-)))

(:derived (Flag406-)(and (Flag317-)))

(:derived (Flag406-)(and (Flag265-)))

(:derived (Flag406-)(and (Flag384-)))

(:derived (Flag406-)(and (Flag266-)))

(:derived (Flag406-)(and (Flag394-)))

(:derived (Flag406-)(and (Flag270-)))

(:derived (Flag406-)(and (Flag324-)))

(:derived (Flag406-)(and (Flag284-)))

(:derived (Flag406-)(and (Flag374-)))

(:derived (Flag406-)(and (Flag250-)))

(:derived (Flag406-)(and (Flag318-)))

(:derived (Flag406-)(and (Flag361-)))

(:derived (Flag406-)(and (Flag325-)))

(:derived (Flag406-)(and (Flag352-)))

(:derived (Flag406-)(and (Flag402-)))

(:derived (Flag406-)(and (Flag342-)))

(:derived (Flag406-)(and (Flag258-)))

(:derived (Flag406-)(and (Flag395-)))

(:derived (Flag406-)(and (Flag320-)))

(:derived (Flag406-)(and (Flag403-)))

(:derived (Flag406-)(and (Flag286-)))

(:derived (Flag406-)(and (Flag305-)))

(:derived (Flag406-)(and (Flag404-)))

(:derived (Flag406-)(and (Flag255-)))

(:derived (Flag406-)(and (Flag276-)))

(:derived (Flag406-)(and (Flag386-)))

(:derived (Flag406-)(and (Flag387-)))

(:derived (Flag406-)(and (Flag273-)))

(:derived (Flag406-)(and (Flag358-)))

(:derived (Flag406-)(and (Flag315-)))

(:derived (Flag406-)(and (Flag350-)))

(:derived (Flag406-)(and (Flag397-)))

(:derived (Flag406-)(and (Flag334-)))

(:derived (Flag406-)(and (Flag309-)))

(:derived (Flag406-)(and (Flag370-)))

(:derived (Flag406-)(and (Flag389-)))

(:derived (Flag406-)(and (Flag310-)))

(:derived (Flag406-)(and (Flag293-)))

(:derived (Flag406-)(and (Flag311-)))

(:derived (Flag406-)(and (Flag398-)))

(:derived (Flag406-)(and (Flag380-)))

(:derived (Flag406-)(and (Flag294-)))

(:derived (Flag406-)(and (Flag371-)))

(:derived (Flag406-)(and (Flag345-)))

(:derived (Flag406-)(and (Flag249-)))

(:derived (Flag406-)(and (Flag283-)))

(:derived (Flag406-)(and (Flag405-)))

(:derived (Flag406-)(and (Flag303-)))

(:derived (Flag406-)(and (Flag378-)))

(:derived (Flag406-)(and (Flag390-)))

(:derived (Flag406-)(and (Flag366-)))

(:derived (Flag406-)(and (Flag367-)))

(:derived (Flag406-)(and (Flag368-)))

(:derived (Flag406-)(and (Flag327-)))

(:derived (Flag413-)(and (Flag372-)))

(:derived (Flag413-)(and (Flag407-)))

(:derived (Flag413-)(and (Flag408-)))

(:derived (Flag413-)(and (Flag392-)))

(:derived (Flag413-)(and (Flag329-)))

(:derived (Flag413-)(and (Flag298-)))

(:derived (Flag413-)(and (Flag340-)))

(:derived (Flag413-)(and (Flag353-)))

(:derived (Flag413-)(and (Flag359-)))

(:derived (Flag413-)(and (Flag356-)))

(:derived (Flag413-)(and (Flag409-)))

(:derived (Flag413-)(and (Flag256-)))

(:derived (Flag413-)(and (Flag349-)))

(:derived (Flag413-)(and (Flag332-)))

(:derived (Flag413-)(and (Flag333-)))

(:derived (Flag413-)(and (Flag400-)))

(:derived (Flag413-)(and (Flag387-)))

(:derived (Flag413-)(and (Flag382-)))

(:derived (Flag413-)(and (Flag376-)))

(:derived (Flag413-)(and (Flag317-)))

(:derived (Flag413-)(and (Flag265-)))

(:derived (Flag413-)(and (Flag384-)))

(:derived (Flag413-)(and (Flag266-)))

(:derived (Flag413-)(and (Flag394-)))

(:derived (Flag413-)(and (Flag324-)))

(:derived (Flag413-)(and (Flag284-)))

(:derived (Flag413-)(and (Flag374-)))

(:derived (Flag413-)(and (Flag250-)))

(:derived (Flag413-)(and (Flag318-)))

(:derived (Flag413-)(and (Flag268-)))

(:derived (Flag413-)(and (Flag410-)))

(:derived (Flag413-)(and (Flag352-)))

(:derived (Flag413-)(and (Flag402-)))

(:derived (Flag413-)(and (Flag342-)))

(:derived (Flag413-)(and (Flag258-)))

(:derived (Flag413-)(and (Flag395-)))

(:derived (Flag413-)(and (Flag303-)))

(:derived (Flag413-)(and (Flag411-)))

(:derived (Flag413-)(and (Flag286-)))

(:derived (Flag413-)(and (Flag305-)))

(:derived (Flag413-)(and (Flag404-)))

(:derived (Flag413-)(and (Flag255-)))

(:derived (Flag413-)(and (Flag312-)))

(:derived (Flag413-)(and (Flag386-)))

(:derived (Flag413-)(and (Flag358-)))

(:derived (Flag413-)(and (Flag315-)))

(:derived (Flag413-)(and (Flag412-)))

(:derived (Flag413-)(and (Flag350-)))

(:derived (Flag413-)(and (Flag334-)))

(:derived (Flag413-)(and (Flag309-)))

(:derived (Flag413-)(and (Flag370-)))

(:derived (Flag413-)(and (Flag320-)))

(:derived (Flag413-)(and (Flag276-)))

(:derived (Flag413-)(and (Flag397-)))

(:derived (Flag413-)(and (Flag249-)))

(:derived (Flag413-)(and (Flag293-)))

(:derived (Flag413-)(and (Flag398-)))

(:derived (Flag413-)(and (Flag380-)))

(:derived (Flag413-)(and (Flag294-)))

(:derived (Flag413-)(and (Flag270-)))

(:derived (Flag413-)(and (Flag283-)))

(:derived (Flag413-)(and (Flag405-)))

(:derived (Flag413-)(and (Flag378-)))

(:derived (Flag413-)(and (Flag390-)))

(:derived (Flag413-)(and (Flag366-)))

(:derived (Flag413-)(and (Flag368-)))

(:derived (Flag413-)(and (Flag327-)))

(:derived (Flag419-)(and (Flag407-)))

(:derived (Flag419-)(and (Flag256-)))

(:derived (Flag419-)(and (Flag392-)))

(:derived (Flag419-)(and (Flag329-)))

(:derived (Flag419-)(and (Flag298-)))

(:derived (Flag419-)(and (Flag353-)))

(:derived (Flag419-)(and (Flag359-)))

(:derived (Flag419-)(and (Flag356-)))

(:derived (Flag419-)(and (Flag349-)))

(:derived (Flag419-)(and (Flag376-)))

(:derived (Flag419-)(and (Flag333-)))

(:derived (Flag419-)(and (Flag400-)))

(:derived (Flag419-)(and (Flag317-)))

(:derived (Flag419-)(and (Flag414-)))

(:derived (Flag419-)(and (Flag382-)))

(:derived (Flag419-)(and (Flag265-)))

(:derived (Flag419-)(and (Flag384-)))

(:derived (Flag419-)(and (Flag415-)))

(:derived (Flag419-)(and (Flag266-)))

(:derived (Flag419-)(and (Flag394-)))

(:derived (Flag419-)(and (Flag284-)))

(:derived (Flag419-)(and (Flag374-)))

(:derived (Flag419-)(and (Flag250-)))

(:derived (Flag419-)(and (Flag416-)))

(:derived (Flag419-)(and (Flag268-)))

(:derived (Flag419-)(and (Flag410-)))

(:derived (Flag419-)(and (Flag352-)))

(:derived (Flag419-)(and (Flag402-)))

(:derived (Flag419-)(and (Flag342-)))

(:derived (Flag419-)(and (Flag258-)))

(:derived (Flag419-)(and (Flag395-)))

(:derived (Flag419-)(and (Flag303-)))

(:derived (Flag419-)(and (Flag286-)))

(:derived (Flag419-)(and (Flag404-)))

(:derived (Flag419-)(and (Flag255-)))

(:derived (Flag419-)(and (Flag417-)))

(:derived (Flag419-)(and (Flag312-)))

(:derived (Flag419-)(and (Flag358-)))

(:derived (Flag419-)(and (Flag315-)))

(:derived (Flag419-)(and (Flag412-)))

(:derived (Flag419-)(and (Flag387-)))

(:derived (Flag419-)(and (Flag334-)))

(:derived (Flag419-)(and (Flag309-)))

(:derived (Flag419-)(and (Flag370-)))

(:derived (Flag419-)(and (Flag320-)))

(:derived (Flag419-)(and (Flag418-)))

(:derived (Flag419-)(and (Flag397-)))

(:derived (Flag419-)(and (Flag340-)))

(:derived (Flag419-)(and (Flag294-)))

(:derived (Flag419-)(and (Flag270-)))

(:derived (Flag419-)(and (Flag283-)))

(:derived (Flag419-)(and (Flag405-)))

(:derived (Flag419-)(and (Flag378-)))

(:derived (Flag419-)(and (Flag390-)))

(:derived (Flag419-)(and (Flag366-)))

(:derived (Flag419-)(and (Flag368-)))

(:derived (Flag419-)(and (Flag327-)))

(:derived (Flag423-)(and (Flag407-)))

(:derived (Flag423-)(and (Flag256-)))

(:derived (Flag423-)(and (Flag392-)))

(:derived (Flag423-)(and (Flag329-)))

(:derived (Flag423-)(and (Flag340-)))

(:derived (Flag423-)(and (Flag356-)))

(:derived (Flag423-)(and (Flag349-)))

(:derived (Flag423-)(and (Flag376-)))

(:derived (Flag423-)(and (Flag420-)))

(:derived (Flag423-)(and (Flag400-)))

(:derived (Flag423-)(and (Flag317-)))

(:derived (Flag423-)(and (Flag415-)))

(:derived (Flag423-)(and (Flag382-)))

(:derived (Flag423-)(and (Flag421-)))

(:derived (Flag423-)(and (Flag265-)))

(:derived (Flag423-)(and (Flag384-)))

(:derived (Flag423-)(and (Flag414-)))

(:derived (Flag423-)(and (Flag422-)))

(:derived (Flag423-)(and (Flag284-)))

(:derived (Flag423-)(and (Flag374-)))

(:derived (Flag423-)(and (Flag268-)))

(:derived (Flag423-)(and (Flag410-)))

(:derived (Flag423-)(and (Flag352-)))

(:derived (Flag423-)(and (Flag378-)))

(:derived (Flag423-)(and (Flag395-)))

(:derived (Flag423-)(and (Flag303-)))

(:derived (Flag423-)(and (Flag286-)))

(:derived (Flag423-)(and (Flag404-)))

(:derived (Flag423-)(and (Flag255-)))

(:derived (Flag423-)(and (Flag358-)))

(:derived (Flag423-)(and (Flag315-)))

(:derived (Flag423-)(and (Flag412-)))

(:derived (Flag423-)(and (Flag387-)))

(:derived (Flag423-)(and (Flag309-)))

(:derived (Flag423-)(and (Flag320-)))

(:derived (Flag423-)(and (Flag418-)))

(:derived (Flag423-)(and (Flag397-)))

(:derived (Flag423-)(and (Flag312-)))

(:derived (Flag423-)(and (Flag270-)))

(:derived (Flag423-)(and (Flag283-)))

(:derived (Flag423-)(and (Flag405-)))

(:derived (Flag423-)(and (Flag342-)))

(:derived (Flag423-)(and (Flag258-)))

(:derived (Flag423-)(and (Flag366-)))

(:derived (Flag423-)(and (Flag368-)))

(:derived (Flag423-)(and (Flag327-)))

(:derived (Flag426-)(and (Flag256-)))

(:derived (Flag426-)(and (Flag392-)))

(:derived (Flag426-)(and (Flag329-)))

(:derived (Flag426-)(and (Flag356-)))

(:derived (Flag426-)(and (Flag349-)))

(:derived (Flag426-)(and (Flag376-)))

(:derived (Flag426-)(and (Flag400-)))

(:derived (Flag426-)(and (Flag317-)))

(:derived (Flag426-)(and (Flag415-)))

(:derived (Flag426-)(and (Flag382-)))

(:derived (Flag426-)(and (Flag421-)))

(:derived (Flag426-)(and (Flag265-)))

(:derived (Flag426-)(and (Flag422-)))

(:derived (Flag426-)(and (Flag284-)))

(:derived (Flag426-)(and (Flag268-)))

(:derived (Flag426-)(and (Flag410-)))

(:derived (Flag426-)(and (Flag378-)))

(:derived (Flag426-)(and (Flag424-)))

(:derived (Flag426-)(and (Flag395-)))

(:derived (Flag426-)(and (Flag303-)))

(:derived (Flag426-)(and (Flag286-)))

(:derived (Flag426-)(and (Flag404-)))

(:derived (Flag426-)(and (Flag358-)))

(:derived (Flag426-)(and (Flag315-)))

(:derived (Flag426-)(and (Flag412-)))

(:derived (Flag426-)(and (Flag387-)))

(:derived (Flag426-)(and (Flag309-)))

(:derived (Flag426-)(and (Flag320-)))

(:derived (Flag426-)(and (Flag418-)))

(:derived (Flag426-)(and (Flag425-)))

(:derived (Flag426-)(and (Flag342-)))

(:derived (Flag426-)(and (Flag258-)))

(:derived (Flag426-)(and (Flag366-)))

(:derived (Flag427-)(and (RIGHTOF3-ROBOT)))

(:derived (Flag427-)(and (RIGHTOF10-ROBOT)))

(:derived (Flag427-)(and (RIGHTOF5-ROBOT)))

(:derived (Flag427-)(and (RIGHTOF9-ROBOT)))

(:derived (Flag427-)(and (RIGHTOF8-ROBOT)))

(:derived (Flag427-)(and (RIGHTOF14-ROBOT)))

(:derived (Flag427-)(and (RIGHTOF13-ROBOT)))

(:derived (Flag427-)(and (RIGHTOF12-ROBOT)))

(:derived (Flag427-)(and (RIGHTOF0-ROBOT)))

(:derived (Flag427-)(and (RIGHTOF4-ROBOT)))

(:derived (Flag427-)(and (RIGHTOF1-ROBOT)))

(:derived (Flag427-)(and (RIGHTOF16-ROBOT)))

(:derived (Flag427-)(and (RIGHTOF7-ROBOT)))

(:derived (Flag427-)(and (RIGHTOF11-ROBOT)))

(:derived (Flag427-)(and (RIGHTOF15-ROBOT)))

(:derived (Flag427-)(and (RIGHTOF6-ROBOT)))

(:derived (Flag428-)(and (RIGHTOF0-ROBOT)(LEFTOF1-ROBOT)))

(:derived (Flag432-)(and (Flag320-)))

(:derived (Flag432-)(and (Flag418-)))

(:derived (Flag432-)(and (Flag256-)))

(:derived (Flag432-)(and (Flag392-)))

(:derived (Flag432-)(and (Flag422-)))

(:derived (Flag432-)(and (Flag358-)))

(:derived (Flag432-)(and (Flag284-)))

(:derived (Flag432-)(and (Flag404-)))

(:derived (Flag432-)(and (Flag424-)))

(:derived (Flag432-)(and (Flag356-)))

(:derived (Flag432-)(and (Flag342-)))

(:derived (Flag432-)(and (Flag268-)))

(:derived (Flag432-)(and (Flag376-)))

(:derived (Flag432-)(and (Flag412-)))

(:derived (Flag432-)(and (Flag387-)))

(:derived (Flag432-)(and (Flag317-)))

(:derived (Flag432-)(and (Flag431-)))

(:action MOVERIGHT-ROBOT
:parameters ()
:precondition
(and
(Flag4-)
)
:effect
(and
(when
(and
(Flag213-)
)
(and
(COLUMN16-ROBOT)
(not (NOT-COLUMN16-ROBOT))
)
)
(when
(and
(Flag209-)
)
(and
(COLUMN15-ROBOT)
(not (NOT-COLUMN15-ROBOT))
)
)
(when
(and
(Flag205-)
)
(and
(COLUMN14-ROBOT)
(not (NOT-COLUMN14-ROBOT))
)
)
(when
(and
(Flag200-)
)
(and
(COLUMN13-ROBOT)
(not (NOT-COLUMN13-ROBOT))
)
)
(when
(and
(Flag193-)
)
(and
(COLUMN12-ROBOT)
(not (NOT-COLUMN12-ROBOT))
)
)
(when
(and
(Flag186-)
)
(and
(COLUMN11-ROBOT)
(not (NOT-COLUMN11-ROBOT))
)
)
(when
(and
(Flag177-)
)
(and
(COLUMN10-ROBOT)
(not (NOT-COLUMN10-ROBOT))
)
)
(when
(and
(Flag168-)
)
(and
(COLUMN9-ROBOT)
(not (NOT-COLUMN9-ROBOT))
)
)
(when
(and
(Flag157-)
)
(and
(COLUMN8-ROBOT)
(not (NOT-COLUMN8-ROBOT))
)
)
(when
(and
(Flag145-)
)
(and
(COLUMN7-ROBOT)
(not (NOT-COLUMN7-ROBOT))
)
)
(when
(and
(Flag131-)
)
(and
(COLUMN6-ROBOT)
(not (NOT-COLUMN6-ROBOT))
)
)
(when
(and
(Flag118-)
)
(and
(COLUMN5-ROBOT)
(not (NOT-COLUMN5-ROBOT))
)
)
(when
(and
(Flag102-)
)
(and
(COLUMN4-ROBOT)
(not (NOT-COLUMN4-ROBOT))
)
)
(when
(and
(Flag85-)
)
(and
(COLUMN3-ROBOT)
(not (NOT-COLUMN3-ROBOT))
)
)
(when
(and
(Flag69-)
)
(and
(COLUMN2-ROBOT)
(not (NOT-COLUMN2-ROBOT))
)
)
(when
(and
(Flag429-)
)
(and
(COLUMN1-ROBOT)
(not (NOT-COLUMN1-ROBOT))
)
)
(when
(and
(COLUMN15-ROBOT)
)
(and
(COLUMN16-ROBOT)
(NOT-COLUMN15-ROBOT)
(not (NOT-COLUMN16-ROBOT))
(not (COLUMN15-ROBOT))
)
)
(when
(and
(COLUMN14-ROBOT)
)
(and
(COLUMN15-ROBOT)
(NOT-COLUMN14-ROBOT)
(not (NOT-COLUMN15-ROBOT))
(not (COLUMN14-ROBOT))
)
)
(when
(and
(COLUMN13-ROBOT)
)
(and
(COLUMN14-ROBOT)
(NOT-COLUMN13-ROBOT)
(not (NOT-COLUMN14-ROBOT))
(not (COLUMN13-ROBOT))
)
)
(when
(and
(COLUMN12-ROBOT)
)
(and
(COLUMN13-ROBOT)
(NOT-COLUMN12-ROBOT)
(not (NOT-COLUMN13-ROBOT))
(not (COLUMN12-ROBOT))
)
)
(when
(and
(COLUMN11-ROBOT)
)
(and
(COLUMN12-ROBOT)
(NOT-COLUMN11-ROBOT)
(not (NOT-COLUMN12-ROBOT))
(not (COLUMN11-ROBOT))
)
)
(when
(and
(COLUMN10-ROBOT)
)
(and
(COLUMN11-ROBOT)
(NOT-COLUMN10-ROBOT)
(not (NOT-COLUMN11-ROBOT))
(not (COLUMN10-ROBOT))
)
)
(when
(and
(COLUMN9-ROBOT)
)
(and
(COLUMN10-ROBOT)
(NOT-COLUMN9-ROBOT)
(not (NOT-COLUMN10-ROBOT))
(not (COLUMN9-ROBOT))
)
)
(when
(and
(COLUMN8-ROBOT)
)
(and
(COLUMN9-ROBOT)
(NOT-COLUMN8-ROBOT)
(not (NOT-COLUMN9-ROBOT))
(not (COLUMN8-ROBOT))
)
)
(when
(and
(COLUMN7-ROBOT)
)
(and
(COLUMN8-ROBOT)
(NOT-COLUMN7-ROBOT)
(not (NOT-COLUMN8-ROBOT))
(not (COLUMN7-ROBOT))
)
)
(when
(and
(COLUMN6-ROBOT)
)
(and
(COLUMN7-ROBOT)
(NOT-COLUMN6-ROBOT)
(not (NOT-COLUMN7-ROBOT))
(not (COLUMN6-ROBOT))
)
)
(when
(and
(COLUMN5-ROBOT)
)
(and
(COLUMN6-ROBOT)
(NOT-COLUMN5-ROBOT)
(not (NOT-COLUMN6-ROBOT))
(not (COLUMN5-ROBOT))
)
)
(when
(and
(COLUMN4-ROBOT)
)
(and
(COLUMN5-ROBOT)
(NOT-COLUMN4-ROBOT)
(not (NOT-COLUMN5-ROBOT))
(not (COLUMN4-ROBOT))
)
)
(when
(and
(COLUMN3-ROBOT)
)
(and
(COLUMN4-ROBOT)
(NOT-COLUMN3-ROBOT)
(not (NOT-COLUMN4-ROBOT))
(not (COLUMN3-ROBOT))
)
)
(when
(and
(COLUMN2-ROBOT)
)
(and
(COLUMN3-ROBOT)
(NOT-COLUMN2-ROBOT)
(not (NOT-COLUMN3-ROBOT))
(not (COLUMN2-ROBOT))
)
)
(when
(and
(COLUMN1-ROBOT)
)
(and
(COLUMN2-ROBOT)
(NOT-COLUMN1-ROBOT)
(not (NOT-COLUMN2-ROBOT))
(not (COLUMN1-ROBOT))
)
)
(when
(and
(COLUMN0-ROBOT)
)
(and
(COLUMN1-ROBOT)
(NOT-COLUMN0-ROBOT)
(not (NOT-COLUMN1-ROBOT))
(not (COLUMN0-ROBOT))
)
)
(when
(and
(LEFTOF8-ROBOT)
)
(and
(LEFTOF17-ROBOT)
(NOT-LEFTOF16-ROBOT)
(LEFTOF16-ROBOT)
(NOT-LEFTOF15-ROBOT)
(LEFTOF15-ROBOT)
(NOT-LEFTOF14-ROBOT)
(LEFTOF14-ROBOT)
(NOT-LEFTOF13-ROBOT)
(LEFTOF13-ROBOT)
(NOT-LEFTOF12-ROBOT)
(LEFTOF12-ROBOT)
(NOT-LEFTOF11-ROBOT)
(LEFTOF11-ROBOT)
(NOT-LEFTOF10-ROBOT)
(LEFTOF10-ROBOT)
(NOT-LEFTOF9-ROBOT)
(LEFTOF9-ROBOT)
(NOT-LEFTOF8-ROBOT)
(not (NOT-LEFTOF17-ROBOT))
(not (LEFTOF8-ROBOT))
)
)
(when
(and
(LEFTOF7-ROBOT)
)
(and
(LEFTOF17-ROBOT)
(NOT-LEFTOF16-ROBOT)
(LEFTOF16-ROBOT)
(NOT-LEFTOF15-ROBOT)
(LEFTOF15-ROBOT)
(NOT-LEFTOF14-ROBOT)
(LEFTOF14-ROBOT)
(NOT-LEFTOF13-ROBOT)
(LEFTOF13-ROBOT)
(NOT-LEFTOF12-ROBOT)
(LEFTOF12-ROBOT)
(NOT-LEFTOF11-ROBOT)
(LEFTOF11-ROBOT)
(NOT-LEFTOF10-ROBOT)
(LEFTOF10-ROBOT)
(NOT-LEFTOF9-ROBOT)
(LEFTOF9-ROBOT)
(NOT-LEFTOF8-ROBOT)
(LEFTOF8-ROBOT)
(NOT-LEFTOF7-ROBOT)
(not (NOT-LEFTOF17-ROBOT))
(not (LEFTOF7-ROBOT))
)
)
(when
(and
(LEFTOF4-ROBOT)
)
(and
(LEFTOF17-ROBOT)
(NOT-LEFTOF16-ROBOT)
(LEFTOF16-ROBOT)
(NOT-LEFTOF15-ROBOT)
(LEFTOF15-ROBOT)
(NOT-LEFTOF14-ROBOT)
(LEFTOF14-ROBOT)
(NOT-LEFTOF13-ROBOT)
(LEFTOF13-ROBOT)
(NOT-LEFTOF12-ROBOT)
(LEFTOF12-ROBOT)
(NOT-LEFTOF11-ROBOT)
(LEFTOF11-ROBOT)
(NOT-LEFTOF10-ROBOT)
(LEFTOF10-ROBOT)
(NOT-LEFTOF9-ROBOT)
(LEFTOF9-ROBOT)
(NOT-LEFTOF8-ROBOT)
(LEFTOF8-ROBOT)
(NOT-LEFTOF7-ROBOT)
(LEFTOF7-ROBOT)
(NOT-LEFTOF6-ROBOT)
(LEFTOF6-ROBOT)
(NOT-LEFTOF5-ROBOT)
(LEFTOF5-ROBOT)
(NOT-LEFTOF4-ROBOT)
(not (NOT-LEFTOF17-ROBOT))
(not (LEFTOF4-ROBOT))
)
)
(when
(and
(LEFTOF3-ROBOT)
)
(and
(LEFTOF17-ROBOT)
(NOT-LEFTOF16-ROBOT)
(LEFTOF16-ROBOT)
(NOT-LEFTOF15-ROBOT)
(LEFTOF15-ROBOT)
(NOT-LEFTOF14-ROBOT)
(LEFTOF14-ROBOT)
(NOT-LEFTOF13-ROBOT)
(LEFTOF13-ROBOT)
(NOT-LEFTOF12-ROBOT)
(LEFTOF12-ROBOT)
(NOT-LEFTOF11-ROBOT)
(LEFTOF11-ROBOT)
(NOT-LEFTOF10-ROBOT)
(LEFTOF10-ROBOT)
(NOT-LEFTOF9-ROBOT)
(LEFTOF9-ROBOT)
(NOT-LEFTOF8-ROBOT)
(LEFTOF8-ROBOT)
(NOT-LEFTOF7-ROBOT)
(LEFTOF7-ROBOT)
(NOT-LEFTOF6-ROBOT)
(LEFTOF6-ROBOT)
(NOT-LEFTOF5-ROBOT)
(LEFTOF5-ROBOT)
(NOT-LEFTOF4-ROBOT)
(LEFTOF4-ROBOT)
(NOT-LEFTOF3-ROBOT)
(not (NOT-LEFTOF17-ROBOT))
(not (LEFTOF3-ROBOT))
)
)
(when
(and
(LEFTOF14-ROBOT)
)
(and
(LEFTOF17-ROBOT)
(NOT-LEFTOF16-ROBOT)
(LEFTOF16-ROBOT)
(NOT-LEFTOF15-ROBOT)
(LEFTOF15-ROBOT)
(NOT-LEFTOF14-ROBOT)
(not (NOT-LEFTOF17-ROBOT))
(not (LEFTOF14-ROBOT))
)
)
(when
(and
(LEFTOF16-ROBOT)
)
(and
(LEFTOF17-ROBOT)
(NOT-LEFTOF16-ROBOT)
(not (NOT-LEFTOF17-ROBOT))
(not (LEFTOF16-ROBOT))
)
)
(when
(and
(LEFTOF5-ROBOT)
)
(and
(LEFTOF17-ROBOT)
(NOT-LEFTOF16-ROBOT)
(LEFTOF16-ROBOT)
(NOT-LEFTOF15-ROBOT)
(LEFTOF15-ROBOT)
(NOT-LEFTOF14-ROBOT)
(LEFTOF14-ROBOT)
(NOT-LEFTOF13-ROBOT)
(LEFTOF13-ROBOT)
(NOT-LEFTOF12-ROBOT)
(LEFTOF12-ROBOT)
(NOT-LEFTOF11-ROBOT)
(LEFTOF11-ROBOT)
(NOT-LEFTOF10-ROBOT)
(LEFTOF10-ROBOT)
(NOT-LEFTOF9-ROBOT)
(LEFTOF9-ROBOT)
(NOT-LEFTOF8-ROBOT)
(LEFTOF8-ROBOT)
(NOT-LEFTOF7-ROBOT)
(LEFTOF7-ROBOT)
(NOT-LEFTOF6-ROBOT)
(LEFTOF6-ROBOT)
(NOT-LEFTOF5-ROBOT)
(not (NOT-LEFTOF17-ROBOT))
(not (LEFTOF5-ROBOT))
)
)
(when
(and
(LEFTOF6-ROBOT)
)
(and
(LEFTOF17-ROBOT)
(NOT-LEFTOF16-ROBOT)
(LEFTOF16-ROBOT)
(NOT-LEFTOF15-ROBOT)
(LEFTOF15-ROBOT)
(NOT-LEFTOF14-ROBOT)
(LEFTOF14-ROBOT)
(NOT-LEFTOF13-ROBOT)
(LEFTOF13-ROBOT)
(NOT-LEFTOF12-ROBOT)
(LEFTOF12-ROBOT)
(NOT-LEFTOF11-ROBOT)
(LEFTOF11-ROBOT)
(NOT-LEFTOF10-ROBOT)
(LEFTOF10-ROBOT)
(NOT-LEFTOF9-ROBOT)
(LEFTOF9-ROBOT)
(NOT-LEFTOF8-ROBOT)
(LEFTOF8-ROBOT)
(NOT-LEFTOF7-ROBOT)
(LEFTOF7-ROBOT)
(NOT-LEFTOF6-ROBOT)
(not (NOT-LEFTOF17-ROBOT))
(not (LEFTOF6-ROBOT))
)
)
(when
(and
(LEFTOF12-ROBOT)
)
(and
(LEFTOF17-ROBOT)
(NOT-LEFTOF16-ROBOT)
(LEFTOF16-ROBOT)
(NOT-LEFTOF15-ROBOT)
(LEFTOF15-ROBOT)
(NOT-LEFTOF14-ROBOT)
(LEFTOF14-ROBOT)
(NOT-LEFTOF13-ROBOT)
(LEFTOF13-ROBOT)
(NOT-LEFTOF12-ROBOT)
(not (NOT-LEFTOF17-ROBOT))
(not (LEFTOF12-ROBOT))
)
)
(when
(and
(LEFTOF1-ROBOT)
)
(and
(LEFTOF17-ROBOT)
(NOT-LEFTOF16-ROBOT)
(LEFTOF16-ROBOT)
(NOT-LEFTOF15-ROBOT)
(LEFTOF15-ROBOT)
(NOT-LEFTOF14-ROBOT)
(LEFTOF14-ROBOT)
(NOT-LEFTOF13-ROBOT)
(LEFTOF13-ROBOT)
(NOT-LEFTOF12-ROBOT)
(LEFTOF12-ROBOT)
(NOT-LEFTOF11-ROBOT)
(LEFTOF11-ROBOT)
(NOT-LEFTOF10-ROBOT)
(LEFTOF10-ROBOT)
(NOT-LEFTOF9-ROBOT)
(LEFTOF9-ROBOT)
(NOT-LEFTOF8-ROBOT)
(LEFTOF8-ROBOT)
(NOT-LEFTOF7-ROBOT)
(LEFTOF7-ROBOT)
(NOT-LEFTOF6-ROBOT)
(LEFTOF6-ROBOT)
(NOT-LEFTOF5-ROBOT)
(LEFTOF5-ROBOT)
(NOT-LEFTOF4-ROBOT)
(LEFTOF4-ROBOT)
(NOT-LEFTOF3-ROBOT)
(LEFTOF3-ROBOT)
(NOT-LEFTOF2-ROBOT)
(LEFTOF2-ROBOT)
(NOT-LEFTOF1-ROBOT)
(not (NOT-LEFTOF17-ROBOT))
(not (LEFTOF1-ROBOT))
)
)
(when
(and
(LEFTOF13-ROBOT)
)
(and
(LEFTOF17-ROBOT)
(NOT-LEFTOF16-ROBOT)
(LEFTOF16-ROBOT)
(NOT-LEFTOF15-ROBOT)
(LEFTOF15-ROBOT)
(NOT-LEFTOF14-ROBOT)
(LEFTOF14-ROBOT)
(NOT-LEFTOF13-ROBOT)
(not (NOT-LEFTOF17-ROBOT))
(not (LEFTOF13-ROBOT))
)
)
(when
(and
(LEFTOF2-ROBOT)
)
(and
(LEFTOF17-ROBOT)
(NOT-LEFTOF16-ROBOT)
(LEFTOF16-ROBOT)
(NOT-LEFTOF15-ROBOT)
(LEFTOF15-ROBOT)
(NOT-LEFTOF14-ROBOT)
(LEFTOF14-ROBOT)
(NOT-LEFTOF13-ROBOT)
(LEFTOF13-ROBOT)
(NOT-LEFTOF12-ROBOT)
(LEFTOF12-ROBOT)
(NOT-LEFTOF11-ROBOT)
(LEFTOF11-ROBOT)
(NOT-LEFTOF10-ROBOT)
(LEFTOF10-ROBOT)
(NOT-LEFTOF9-ROBOT)
(LEFTOF9-ROBOT)
(NOT-LEFTOF8-ROBOT)
(LEFTOF8-ROBOT)
(NOT-LEFTOF7-ROBOT)
(LEFTOF7-ROBOT)
(NOT-LEFTOF6-ROBOT)
(LEFTOF6-ROBOT)
(NOT-LEFTOF5-ROBOT)
(LEFTOF5-ROBOT)
(NOT-LEFTOF4-ROBOT)
(LEFTOF4-ROBOT)
(NOT-LEFTOF3-ROBOT)
(LEFTOF3-ROBOT)
(NOT-LEFTOF2-ROBOT)
(not (NOT-LEFTOF17-ROBOT))
(not (LEFTOF2-ROBOT))
)
)
(when
(and
(LEFTOF9-ROBOT)
)
(and
(LEFTOF17-ROBOT)
(NOT-LEFTOF16-ROBOT)
(LEFTOF16-ROBOT)
(NOT-LEFTOF15-ROBOT)
(LEFTOF15-ROBOT)
(NOT-LEFTOF14-ROBOT)
(LEFTOF14-ROBOT)
(NOT-LEFTOF13-ROBOT)
(LEFTOF13-ROBOT)
(NOT-LEFTOF12-ROBOT)
(LEFTOF12-ROBOT)
(NOT-LEFTOF11-ROBOT)
(LEFTOF11-ROBOT)
(NOT-LEFTOF10-ROBOT)
(LEFTOF10-ROBOT)
(NOT-LEFTOF9-ROBOT)
(not (NOT-LEFTOF17-ROBOT))
(not (LEFTOF9-ROBOT))
)
)
(when
(and
(LEFTOF11-ROBOT)
)
(and
(LEFTOF17-ROBOT)
(NOT-LEFTOF16-ROBOT)
(LEFTOF16-ROBOT)
(NOT-LEFTOF15-ROBOT)
(LEFTOF15-ROBOT)
(NOT-LEFTOF14-ROBOT)
(LEFTOF14-ROBOT)
(NOT-LEFTOF13-ROBOT)
(LEFTOF13-ROBOT)
(NOT-LEFTOF12-ROBOT)
(LEFTOF12-ROBOT)
(NOT-LEFTOF11-ROBOT)
(not (NOT-LEFTOF17-ROBOT))
(not (LEFTOF11-ROBOT))
)
)
(when
(and
(LEFTOF10-ROBOT)
)
(and
(LEFTOF17-ROBOT)
(NOT-LEFTOF16-ROBOT)
(LEFTOF16-ROBOT)
(NOT-LEFTOF15-ROBOT)
(LEFTOF15-ROBOT)
(NOT-LEFTOF14-ROBOT)
(LEFTOF14-ROBOT)
(NOT-LEFTOF13-ROBOT)
(LEFTOF13-ROBOT)
(NOT-LEFTOF12-ROBOT)
(LEFTOF12-ROBOT)
(NOT-LEFTOF11-ROBOT)
(LEFTOF11-ROBOT)
(NOT-LEFTOF10-ROBOT)
(not (NOT-LEFTOF17-ROBOT))
(not (LEFTOF10-ROBOT))
)
)
(when
(and
(LEFTOF15-ROBOT)
)
(and
(LEFTOF17-ROBOT)
(NOT-LEFTOF16-ROBOT)
(LEFTOF16-ROBOT)
(NOT-LEFTOF15-ROBOT)
(not (NOT-LEFTOF17-ROBOT))
(not (LEFTOF15-ROBOT))
)
)
(when
(and
(RIGHTOF15-ROBOT)
)
(and
(RIGHTOF16-ROBOT)
(RIGHTOF14-ROBOT)
(RIGHTOF13-ROBOT)
(RIGHTOF12-ROBOT)
(RIGHTOF11-ROBOT)
(RIGHTOF10-ROBOT)
(RIGHTOF9-ROBOT)
(RIGHTOF8-ROBOT)
(RIGHTOF7-ROBOT)
(RIGHTOF6-ROBOT)
(RIGHTOF5-ROBOT)
(RIGHTOF4-ROBOT)
(RIGHTOF3-ROBOT)
(RIGHTOF2-ROBOT)
(not (NOT-RIGHTOF16-ROBOT))
(not (NOT-RIGHTOF15-ROBOT))
(not (NOT-RIGHTOF14-ROBOT))
(not (NOT-RIGHTOF13-ROBOT))
(not (NOT-RIGHTOF12-ROBOT))
(not (NOT-RIGHTOF11-ROBOT))
(not (NOT-RIGHTOF10-ROBOT))
(not (NOT-RIGHTOF9-ROBOT))
(not (NOT-RIGHTOF8-ROBOT))
(not (NOT-RIGHTOF7-ROBOT))
(not (NOT-RIGHTOF6-ROBOT))
(not (NOT-RIGHTOF5-ROBOT))
(not (NOT-RIGHTOF4-ROBOT))
(not (NOT-RIGHTOF3-ROBOT))
(not (NOT-RIGHTOF2-ROBOT))
)
)
(when
(and
(RIGHTOF16-ROBOT)
)
(and
(RIGHTOF15-ROBOT)
(RIGHTOF14-ROBOT)
(RIGHTOF13-ROBOT)
(RIGHTOF12-ROBOT)
(RIGHTOF11-ROBOT)
(RIGHTOF10-ROBOT)
(RIGHTOF9-ROBOT)
(RIGHTOF8-ROBOT)
(RIGHTOF7-ROBOT)
(RIGHTOF6-ROBOT)
(RIGHTOF5-ROBOT)
(RIGHTOF4-ROBOT)
(RIGHTOF3-ROBOT)
(RIGHTOF2-ROBOT)
(not (NOT-RIGHTOF16-ROBOT))
(not (NOT-RIGHTOF15-ROBOT))
(not (NOT-RIGHTOF14-ROBOT))
(not (NOT-RIGHTOF13-ROBOT))
(not (NOT-RIGHTOF12-ROBOT))
(not (NOT-RIGHTOF11-ROBOT))
(not (NOT-RIGHTOF10-ROBOT))
(not (NOT-RIGHTOF9-ROBOT))
(not (NOT-RIGHTOF8-ROBOT))
(not (NOT-RIGHTOF7-ROBOT))
(not (NOT-RIGHTOF6-ROBOT))
(not (NOT-RIGHTOF5-ROBOT))
(not (NOT-RIGHTOF4-ROBOT))
(not (NOT-RIGHTOF3-ROBOT))
(not (NOT-RIGHTOF2-ROBOT))
)
)
(when
(and
(RIGHTOF14-ROBOT)
)
(and
(RIGHTOF15-ROBOT)
(RIGHTOF13-ROBOT)
(RIGHTOF12-ROBOT)
(RIGHTOF11-ROBOT)
(RIGHTOF10-ROBOT)
(RIGHTOF9-ROBOT)
(RIGHTOF8-ROBOT)
(RIGHTOF7-ROBOT)
(RIGHTOF6-ROBOT)
(RIGHTOF5-ROBOT)
(RIGHTOF4-ROBOT)
(RIGHTOF3-ROBOT)
(RIGHTOF2-ROBOT)
(not (NOT-RIGHTOF15-ROBOT))
(not (NOT-RIGHTOF14-ROBOT))
(not (NOT-RIGHTOF13-ROBOT))
(not (NOT-RIGHTOF12-ROBOT))
(not (NOT-RIGHTOF11-ROBOT))
(not (NOT-RIGHTOF10-ROBOT))
(not (NOT-RIGHTOF9-ROBOT))
(not (NOT-RIGHTOF8-ROBOT))
(not (NOT-RIGHTOF7-ROBOT))
(not (NOT-RIGHTOF6-ROBOT))
(not (NOT-RIGHTOF5-ROBOT))
(not (NOT-RIGHTOF4-ROBOT))
(not (NOT-RIGHTOF3-ROBOT))
(not (NOT-RIGHTOF2-ROBOT))
)
)
(when
(and
(RIGHTOF13-ROBOT)
)
(and
(RIGHTOF14-ROBOT)
(RIGHTOF12-ROBOT)
(RIGHTOF11-ROBOT)
(RIGHTOF10-ROBOT)
(RIGHTOF9-ROBOT)
(RIGHTOF8-ROBOT)
(RIGHTOF7-ROBOT)
(RIGHTOF6-ROBOT)
(RIGHTOF5-ROBOT)
(RIGHTOF4-ROBOT)
(RIGHTOF3-ROBOT)
(RIGHTOF2-ROBOT)
(not (NOT-RIGHTOF14-ROBOT))
(not (NOT-RIGHTOF13-ROBOT))
(not (NOT-RIGHTOF12-ROBOT))
(not (NOT-RIGHTOF11-ROBOT))
(not (NOT-RIGHTOF10-ROBOT))
(not (NOT-RIGHTOF9-ROBOT))
(not (NOT-RIGHTOF8-ROBOT))
(not (NOT-RIGHTOF7-ROBOT))
(not (NOT-RIGHTOF6-ROBOT))
(not (NOT-RIGHTOF5-ROBOT))
(not (NOT-RIGHTOF4-ROBOT))
(not (NOT-RIGHTOF3-ROBOT))
(not (NOT-RIGHTOF2-ROBOT))
)
)
(when
(and
(RIGHTOF12-ROBOT)
)
(and
(RIGHTOF13-ROBOT)
(RIGHTOF11-ROBOT)
(RIGHTOF10-ROBOT)
(RIGHTOF9-ROBOT)
(RIGHTOF8-ROBOT)
(RIGHTOF7-ROBOT)
(RIGHTOF6-ROBOT)
(RIGHTOF5-ROBOT)
(RIGHTOF4-ROBOT)
(RIGHTOF3-ROBOT)
(RIGHTOF2-ROBOT)
(not (NOT-RIGHTOF13-ROBOT))
(not (NOT-RIGHTOF12-ROBOT))
(not (NOT-RIGHTOF11-ROBOT))
(not (NOT-RIGHTOF10-ROBOT))
(not (NOT-RIGHTOF9-ROBOT))
(not (NOT-RIGHTOF8-ROBOT))
(not (NOT-RIGHTOF7-ROBOT))
(not (NOT-RIGHTOF6-ROBOT))
(not (NOT-RIGHTOF5-ROBOT))
(not (NOT-RIGHTOF4-ROBOT))
(not (NOT-RIGHTOF3-ROBOT))
(not (NOT-RIGHTOF2-ROBOT))
)
)
(when
(and
(RIGHTOF11-ROBOT)
)
(and
(RIGHTOF12-ROBOT)
(RIGHTOF10-ROBOT)
(RIGHTOF9-ROBOT)
(RIGHTOF8-ROBOT)
(RIGHTOF7-ROBOT)
(RIGHTOF6-ROBOT)
(RIGHTOF5-ROBOT)
(RIGHTOF4-ROBOT)
(RIGHTOF3-ROBOT)
(RIGHTOF2-ROBOT)
(not (NOT-RIGHTOF12-ROBOT))
(not (NOT-RIGHTOF11-ROBOT))
(not (NOT-RIGHTOF10-ROBOT))
(not (NOT-RIGHTOF9-ROBOT))
(not (NOT-RIGHTOF8-ROBOT))
(not (NOT-RIGHTOF7-ROBOT))
(not (NOT-RIGHTOF6-ROBOT))
(not (NOT-RIGHTOF5-ROBOT))
(not (NOT-RIGHTOF4-ROBOT))
(not (NOT-RIGHTOF3-ROBOT))
(not (NOT-RIGHTOF2-ROBOT))
)
)
(when
(and
(RIGHTOF10-ROBOT)
)
(and
(RIGHTOF11-ROBOT)
(RIGHTOF9-ROBOT)
(RIGHTOF8-ROBOT)
(RIGHTOF7-ROBOT)
(RIGHTOF6-ROBOT)
(RIGHTOF5-ROBOT)
(RIGHTOF4-ROBOT)
(RIGHTOF3-ROBOT)
(RIGHTOF2-ROBOT)
(not (NOT-RIGHTOF11-ROBOT))
(not (NOT-RIGHTOF10-ROBOT))
(not (NOT-RIGHTOF9-ROBOT))
(not (NOT-RIGHTOF8-ROBOT))
(not (NOT-RIGHTOF7-ROBOT))
(not (NOT-RIGHTOF6-ROBOT))
(not (NOT-RIGHTOF5-ROBOT))
(not (NOT-RIGHTOF4-ROBOT))
(not (NOT-RIGHTOF3-ROBOT))
(not (NOT-RIGHTOF2-ROBOT))
)
)
(when
(and
(RIGHTOF9-ROBOT)
)
(and
(RIGHTOF10-ROBOT)
(RIGHTOF8-ROBOT)
(RIGHTOF7-ROBOT)
(RIGHTOF6-ROBOT)
(RIGHTOF5-ROBOT)
(RIGHTOF4-ROBOT)
(RIGHTOF3-ROBOT)
(RIGHTOF2-ROBOT)
(not (NOT-RIGHTOF10-ROBOT))
(not (NOT-RIGHTOF9-ROBOT))
(not (NOT-RIGHTOF8-ROBOT))
(not (NOT-RIGHTOF7-ROBOT))
(not (NOT-RIGHTOF6-ROBOT))
(not (NOT-RIGHTOF5-ROBOT))
(not (NOT-RIGHTOF4-ROBOT))
(not (NOT-RIGHTOF3-ROBOT))
(not (NOT-RIGHTOF2-ROBOT))
)
)
(when
(and
(RIGHTOF8-ROBOT)
)
(and
(RIGHTOF9-ROBOT)
(RIGHTOF7-ROBOT)
(RIGHTOF6-ROBOT)
(RIGHTOF5-ROBOT)
(RIGHTOF4-ROBOT)
(RIGHTOF3-ROBOT)
(RIGHTOF2-ROBOT)
(not (NOT-RIGHTOF9-ROBOT))
(not (NOT-RIGHTOF8-ROBOT))
(not (NOT-RIGHTOF7-ROBOT))
(not (NOT-RIGHTOF6-ROBOT))
(not (NOT-RIGHTOF5-ROBOT))
(not (NOT-RIGHTOF4-ROBOT))
(not (NOT-RIGHTOF3-ROBOT))
(not (NOT-RIGHTOF2-ROBOT))
)
)
(when
(and
(RIGHTOF7-ROBOT)
)
(and
(RIGHTOF8-ROBOT)
(RIGHTOF6-ROBOT)
(RIGHTOF5-ROBOT)
(RIGHTOF4-ROBOT)
(RIGHTOF3-ROBOT)
(RIGHTOF2-ROBOT)
(not (NOT-RIGHTOF8-ROBOT))
(not (NOT-RIGHTOF7-ROBOT))
(not (NOT-RIGHTOF6-ROBOT))
(not (NOT-RIGHTOF5-ROBOT))
(not (NOT-RIGHTOF4-ROBOT))
(not (NOT-RIGHTOF3-ROBOT))
(not (NOT-RIGHTOF2-ROBOT))
)
)
(when
(and
(RIGHTOF6-ROBOT)
)
(and
(RIGHTOF7-ROBOT)
(RIGHTOF5-ROBOT)
(RIGHTOF4-ROBOT)
(RIGHTOF3-ROBOT)
(RIGHTOF2-ROBOT)
(not (NOT-RIGHTOF7-ROBOT))
(not (NOT-RIGHTOF6-ROBOT))
(not (NOT-RIGHTOF5-ROBOT))
(not (NOT-RIGHTOF4-ROBOT))
(not (NOT-RIGHTOF3-ROBOT))
(not (NOT-RIGHTOF2-ROBOT))
)
)
(when
(and
(RIGHTOF5-ROBOT)
)
(and
(RIGHTOF6-ROBOT)
(RIGHTOF4-ROBOT)
(RIGHTOF3-ROBOT)
(RIGHTOF2-ROBOT)
(not (NOT-RIGHTOF6-ROBOT))
(not (NOT-RIGHTOF5-ROBOT))
(not (NOT-RIGHTOF4-ROBOT))
(not (NOT-RIGHTOF3-ROBOT))
(not (NOT-RIGHTOF2-ROBOT))
)
)
(when
(and
(RIGHTOF4-ROBOT)
)
(and
(RIGHTOF5-ROBOT)
(RIGHTOF3-ROBOT)
(RIGHTOF2-ROBOT)
(not (NOT-RIGHTOF5-ROBOT))
(not (NOT-RIGHTOF4-ROBOT))
(not (NOT-RIGHTOF3-ROBOT))
(not (NOT-RIGHTOF2-ROBOT))
)
)
(when
(and
(RIGHTOF3-ROBOT)
)
(and
(RIGHTOF4-ROBOT)
(RIGHTOF2-ROBOT)
(not (NOT-RIGHTOF4-ROBOT))
(not (NOT-RIGHTOF3-ROBOT))
(not (NOT-RIGHTOF2-ROBOT))
)
)
(when
(and
(RIGHTOF2-ROBOT)
)
(and
(RIGHTOF3-ROBOT)
(not (NOT-RIGHTOF3-ROBOT))
(not (NOT-RIGHTOF2-ROBOT))
)
)
(when
(and
(RIGHTOF1-ROBOT)
)
(and
(RIGHTOF2-ROBOT)
(not (NOT-RIGHTOF2-ROBOT))
)
)
(when
(and
(Flag427-)
)
(and
(RIGHTOF1-ROBOT)
(not (NOT-RIGHTOF1-ROBOT))
)
)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action MOVELEFT-ROBOT
:parameters ()
:precondition
(and
(Flag4-)
)
:effect
(and
(when
(and
(Flag215-)
)
(and
(COLUMN15-ROBOT)
(not (NOT-COLUMN15-ROBOT))
)
)
(when
(and
(Flag213-)
)
(and
(COLUMN14-ROBOT)
(not (NOT-COLUMN14-ROBOT))
)
)
(when
(and
(Flag209-)
)
(and
(COLUMN13-ROBOT)
(not (NOT-COLUMN13-ROBOT))
)
)
(when
(and
(Flag205-)
)
(and
(COLUMN12-ROBOT)
(not (NOT-COLUMN12-ROBOT))
)
)
(when
(and
(Flag200-)
)
(and
(COLUMN11-ROBOT)
(not (NOT-COLUMN11-ROBOT))
)
)
(when
(and
(Flag193-)
)
(and
(COLUMN10-ROBOT)
(not (NOT-COLUMN10-ROBOT))
)
)
(when
(and
(Flag186-)
)
(and
(COLUMN9-ROBOT)
(not (NOT-COLUMN9-ROBOT))
)
)
(when
(and
(Flag177-)
)
(and
(COLUMN8-ROBOT)
(not (NOT-COLUMN8-ROBOT))
)
)
(when
(and
(Flag168-)
)
(and
(COLUMN7-ROBOT)
(not (NOT-COLUMN7-ROBOT))
)
)
(when
(and
(Flag157-)
)
(and
(COLUMN6-ROBOT)
(not (NOT-COLUMN6-ROBOT))
)
)
(when
(and
(Flag145-)
)
(and
(COLUMN5-ROBOT)
(not (NOT-COLUMN5-ROBOT))
)
)
(when
(and
(Flag131-)
)
(and
(COLUMN4-ROBOT)
(not (NOT-COLUMN4-ROBOT))
)
)
(when
(and
(Flag118-)
)
(and
(COLUMN3-ROBOT)
(not (NOT-COLUMN3-ROBOT))
)
)
(when
(and
(Flag102-)
)
(and
(COLUMN2-ROBOT)
(not (NOT-COLUMN2-ROBOT))
)
)
(when
(and
(Flag85-)
)
(and
(COLUMN1-ROBOT)
(not (NOT-COLUMN1-ROBOT))
)
)
(when
(and
(Flag69-)
)
(and
(COLUMN0-ROBOT)
(not (NOT-COLUMN0-ROBOT))
)
)
(when
(and
(COLUMN16-ROBOT)
)
(and
(COLUMN15-ROBOT)
(NOT-COLUMN16-ROBOT)
(not (NOT-COLUMN15-ROBOT))
(not (COLUMN16-ROBOT))
)
)
(when
(and
(COLUMN15-ROBOT)
)
(and
(COLUMN14-ROBOT)
(NOT-COLUMN15-ROBOT)
(not (NOT-COLUMN14-ROBOT))
(not (COLUMN15-ROBOT))
)
)
(when
(and
(COLUMN14-ROBOT)
)
(and
(COLUMN13-ROBOT)
(NOT-COLUMN14-ROBOT)
(not (NOT-COLUMN13-ROBOT))
(not (COLUMN14-ROBOT))
)
)
(when
(and
(COLUMN13-ROBOT)
)
(and
(COLUMN12-ROBOT)
(NOT-COLUMN13-ROBOT)
(not (NOT-COLUMN12-ROBOT))
(not (COLUMN13-ROBOT))
)
)
(when
(and
(COLUMN12-ROBOT)
)
(and
(COLUMN11-ROBOT)
(NOT-COLUMN12-ROBOT)
(not (NOT-COLUMN11-ROBOT))
(not (COLUMN12-ROBOT))
)
)
(when
(and
(COLUMN11-ROBOT)
)
(and
(COLUMN10-ROBOT)
(NOT-COLUMN11-ROBOT)
(not (NOT-COLUMN10-ROBOT))
(not (COLUMN11-ROBOT))
)
)
(when
(and
(COLUMN10-ROBOT)
)
(and
(COLUMN9-ROBOT)
(NOT-COLUMN10-ROBOT)
(not (NOT-COLUMN9-ROBOT))
(not (COLUMN10-ROBOT))
)
)
(when
(and
(COLUMN9-ROBOT)
)
(and
(COLUMN8-ROBOT)
(NOT-COLUMN9-ROBOT)
(not (NOT-COLUMN8-ROBOT))
(not (COLUMN9-ROBOT))
)
)
(when
(and
(COLUMN8-ROBOT)
)
(and
(COLUMN7-ROBOT)
(NOT-COLUMN8-ROBOT)
(not (NOT-COLUMN7-ROBOT))
(not (COLUMN8-ROBOT))
)
)
(when
(and
(COLUMN7-ROBOT)
)
(and
(COLUMN6-ROBOT)
(NOT-COLUMN7-ROBOT)
(not (NOT-COLUMN6-ROBOT))
(not (COLUMN7-ROBOT))
)
)
(when
(and
(COLUMN6-ROBOT)
)
(and
(COLUMN5-ROBOT)
(NOT-COLUMN6-ROBOT)
(not (NOT-COLUMN5-ROBOT))
(not (COLUMN6-ROBOT))
)
)
(when
(and
(COLUMN5-ROBOT)
)
(and
(COLUMN4-ROBOT)
(NOT-COLUMN5-ROBOT)
(not (NOT-COLUMN4-ROBOT))
(not (COLUMN5-ROBOT))
)
)
(when
(and
(COLUMN4-ROBOT)
)
(and
(COLUMN3-ROBOT)
(NOT-COLUMN4-ROBOT)
(not (NOT-COLUMN3-ROBOT))
(not (COLUMN4-ROBOT))
)
)
(when
(and
(COLUMN3-ROBOT)
)
(and
(COLUMN2-ROBOT)
(NOT-COLUMN3-ROBOT)
(not (NOT-COLUMN2-ROBOT))
(not (COLUMN3-ROBOT))
)
)
(when
(and
(COLUMN2-ROBOT)
)
(and
(COLUMN1-ROBOT)
(NOT-COLUMN2-ROBOT)
(not (NOT-COLUMN1-ROBOT))
(not (COLUMN2-ROBOT))
)
)
(when
(and
(COLUMN1-ROBOT)
)
(and
(COLUMN0-ROBOT)
(NOT-COLUMN1-ROBOT)
(not (NOT-COLUMN0-ROBOT))
(not (COLUMN1-ROBOT))
)
)
(when
(and
(RIGHTOF16-ROBOT)
)
(and
(RIGHTOF15-ROBOT)
(NOT-RIGHTOF16-ROBOT)
(not (NOT-RIGHTOF15-ROBOT))
(not (RIGHTOF16-ROBOT))
)
)
(when
(and
(Flag35-)
)
(and
(RIGHTOF14-ROBOT)
(NOT-RIGHTOF15-ROBOT)
(not (NOT-RIGHTOF14-ROBOT))
(not (RIGHTOF15-ROBOT))
)
)
(when
(and
(Flag34-)
)
(and
(RIGHTOF13-ROBOT)
(NOT-RIGHTOF14-ROBOT)
(not (NOT-RIGHTOF13-ROBOT))
(not (RIGHTOF14-ROBOT))
)
)
(when
(and
(Flag33-)
)
(and
(RIGHTOF12-ROBOT)
(NOT-RIGHTOF13-ROBOT)
(not (NOT-RIGHTOF12-ROBOT))
(not (RIGHTOF13-ROBOT))
)
)
(when
(and
(Flag32-)
)
(and
(RIGHTOF11-ROBOT)
(NOT-RIGHTOF12-ROBOT)
(not (NOT-RIGHTOF11-ROBOT))
(not (RIGHTOF12-ROBOT))
)
)
(when
(and
(Flag31-)
)
(and
(RIGHTOF10-ROBOT)
(NOT-RIGHTOF11-ROBOT)
(not (NOT-RIGHTOF10-ROBOT))
(not (RIGHTOF11-ROBOT))
)
)
(when
(and
(Flag30-)
)
(and
(RIGHTOF9-ROBOT)
(NOT-RIGHTOF10-ROBOT)
(not (NOT-RIGHTOF9-ROBOT))
(not (RIGHTOF10-ROBOT))
)
)
(when
(and
(Flag29-)
)
(and
(RIGHTOF8-ROBOT)
(NOT-RIGHTOF9-ROBOT)
(not (NOT-RIGHTOF8-ROBOT))
(not (RIGHTOF9-ROBOT))
)
)
(when
(and
(Flag28-)
)
(and
(RIGHTOF7-ROBOT)
(NOT-RIGHTOF8-ROBOT)
(not (NOT-RIGHTOF7-ROBOT))
(not (RIGHTOF8-ROBOT))
)
)
(when
(and
(Flag27-)
)
(and
(RIGHTOF6-ROBOT)
(NOT-RIGHTOF7-ROBOT)
(not (NOT-RIGHTOF6-ROBOT))
(not (RIGHTOF7-ROBOT))
)
)
(when
(and
(Flag26-)
)
(and
(RIGHTOF5-ROBOT)
(NOT-RIGHTOF6-ROBOT)
(not (NOT-RIGHTOF5-ROBOT))
(not (RIGHTOF6-ROBOT))
)
)
(when
(and
(Flag25-)
)
(and
(RIGHTOF4-ROBOT)
(NOT-RIGHTOF5-ROBOT)
(not (NOT-RIGHTOF4-ROBOT))
(not (RIGHTOF5-ROBOT))
)
)
(when
(and
(Flag24-)
)
(and
(RIGHTOF3-ROBOT)
(NOT-RIGHTOF4-ROBOT)
(not (NOT-RIGHTOF3-ROBOT))
(not (RIGHTOF4-ROBOT))
)
)
(when
(and
(Flag23-)
)
(and
(RIGHTOF2-ROBOT)
(NOT-RIGHTOF3-ROBOT)
(not (NOT-RIGHTOF2-ROBOT))
(not (RIGHTOF3-ROBOT))
)
)
(when
(and
(Flag22-)
)
(and
(RIGHTOF1-ROBOT)
(NOT-RIGHTOF2-ROBOT)
(not (NOT-RIGHTOF1-ROBOT))
(not (RIGHTOF2-ROBOT))
)
)
(when
(and
(Flag21-)
)
(and
(RIGHTOF0-ROBOT)
(NOT-RIGHTOF1-ROBOT)
(not (NOT-RIGHTOF0-ROBOT))
(not (RIGHTOF1-ROBOT))
)
)
(when
(and
(Flag20-)
)
(and
(LEFTOF16-ROBOT)
(not (NOT-LEFTOF16-ROBOT))
)
)
(when
(and
(Flag19-)
)
(and
(LEFTOF15-ROBOT)
(not (NOT-LEFTOF15-ROBOT))
)
)
(when
(and
(Flag18-)
)
(and
(LEFTOF14-ROBOT)
(not (NOT-LEFTOF14-ROBOT))
)
)
(when
(and
(Flag17-)
)
(and
(LEFTOF13-ROBOT)
(not (NOT-LEFTOF13-ROBOT))
)
)
(when
(and
(Flag16-)
)
(and
(LEFTOF12-ROBOT)
(not (NOT-LEFTOF12-ROBOT))
)
)
(when
(and
(Flag15-)
)
(and
(LEFTOF11-ROBOT)
(not (NOT-LEFTOF11-ROBOT))
)
)
(when
(and
(Flag14-)
)
(and
(LEFTOF10-ROBOT)
(not (NOT-LEFTOF10-ROBOT))
)
)
(when
(and
(Flag13-)
)
(and
(LEFTOF9-ROBOT)
(not (NOT-LEFTOF9-ROBOT))
)
)
(when
(and
(Flag12-)
)
(and
(LEFTOF8-ROBOT)
(not (NOT-LEFTOF8-ROBOT))
)
)
(when
(and
(Flag11-)
)
(and
(LEFTOF7-ROBOT)
(not (NOT-LEFTOF7-ROBOT))
)
)
(when
(and
(Flag10-)
)
(and
(LEFTOF6-ROBOT)
(not (NOT-LEFTOF6-ROBOT))
)
)
(when
(and
(Flag9-)
)
(and
(LEFTOF5-ROBOT)
(not (NOT-LEFTOF5-ROBOT))
)
)
(when
(and
(Flag8-)
)
(and
(LEFTOF4-ROBOT)
(not (NOT-LEFTOF4-ROBOT))
)
)
(when
(and
(Flag7-)
)
(and
(LEFTOF3-ROBOT)
(not (NOT-LEFTOF3-ROBOT))
)
)
(when
(and
(Flag6-)
)
(and
(LEFTOF2-ROBOT)
(not (NOT-LEFTOF2-ROBOT))
)
)
(when
(and
(Flag5-)
)
(and
(LEFTOF1-ROBOT)
(not (NOT-LEFTOF1-ROBOT))
)
)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action CHECKCONSISTENCYACTION
:parameters ()
:precondition
(and
(NOT-ERROR-)
(CHECKCONSISTENCY-)
)
:effect
(and
(when
(and
(RIGHTOF14-ROBOT)
(LEFTOF7-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF6-ROBOT)
(ABOVEOF11-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF15-ROBOT)
(LEFTOF1-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF9-ROBOT)
(LEFTOF7-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF3-ROBOT)
(RIGHTOF5-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF11-ROBOT)
(BELOWOF9-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF4-ROBOT)
(RIGHTOF16-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF5-ROBOT)
(RIGHTOF15-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF14-ROBOT)
(BELOWOF6-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF6-ROBOT)
(ABOVEOF7-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF1-ROBOT)
(RIGHTOF8-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF15-ROBOT)
(LEFTOF9-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF7-ROBOT)
(RIGHTOF13-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF2-ROBOT)
(RIGHTOF16-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF13-ROBOT)
(LEFTOF5-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF6-ROBOT)
(LEFTOF4-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF11-ROBOT)
(BELOWOF3-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF16-ROBOT)
(BELOWOF7-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF14-ROBOT)
(BELOWOF2-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF12-ROBOT)
(BELOWOF2-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF6-ROBOT)
(RIGHTOF8-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF11-ROBOT)
(LEFTOF2-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF1-ROBOT)
(RIGHTOF13-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF15-ROBOT)
(BELOWOF4-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF9-ROBOT)
(LEFTOF2-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF15-ROBOT)
(BELOWOF6-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF9-ROBOT)
(LEFTOF1-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF12-ROBOT)
(LEFTOF5-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF8-ROBOT)
(ABOVEOF11-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF3-ROBOT)
(RIGHTOF14-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF13-ROBOT)
(RIGHTOF16-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF7-ROBOT)
(ABOVEOF13-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF16-ROBOT)
(LEFTOF5-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF5-ROBOT)
(RIGHTOF14-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF4-ROBOT)
(RIGHTOF10-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF9-ROBOT)
(LEFTOF6-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF8-ROBOT)
(ABOVEOF8-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF16-ROBOT)
(BELOWOF8-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF14-ROBOT)
(BELOWOF10-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF1-ROBOT)
(RIGHTOF11-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF2-ROBOT)
(RIGHTOF14-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF12-ROBOT)
(BELOWOF7-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF4-ROBOT)
(ABOVEOF14-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF7-ROBOT)
(BELOWOF4-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF4-ROBOT)
(ABOVEOF6-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF1-ROBOT)
(RIGHTOF14-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF6-ROBOT)
(BELOWOF1-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF6-ROBOT)
(ABOVEOF13-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF16-ROBOT)
(LEFTOF7-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF11-ROBOT)
(BELOWOF7-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF7-ROBOT)
(BELOWOF1-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF13-ROBOT)
(BELOWOF1-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF4-ROBOT)
(RIGHTOF8-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF14-ROBOT)
(BELOWOF1-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF3-ROBOT)
(RIGHTOF11-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF4-ROBOT)
(RIGHTOF15-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF12-ROBOT)
(BELOWOF1-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF10-ROBOT)
(ABOVEOF10-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF13-ROBOT)
(LEFTOF8-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF2-ROBOT)
(ABOVEOF9-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF10-ROBOT)
(BELOWOF3-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF15-ROBOT)
(BELOWOF1-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF13-ROBOT)
(LEFTOF3-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF7-ROBOT)
(LEFTOF6-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF3-ROBOT)
(ABOVEOF12-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF16-ROBOT)
(LEFTOF3-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF12-ROBOT)
(LEFTOF2-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF5-ROBOT)
(LEFTOF1-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF15-ROBOT)
(LEFTOF3-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF14-ROBOT)
(LEFTOF9-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF12-ROBOT)
(LEFTOF10-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF2-ROBOT)
(ABOVEOF8-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF5-ROBOT)
(BELOWOF1-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF3-ROBOT)
(RIGHTOF8-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF13-ROBOT)
(RIGHTOF14-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF6-ROBOT)
(ABOVEOF16-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF7-ROBOT)
(BELOWOF2-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF16-ROBOT)
(LEFTOF1-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF16-ROBOT)
(LEFTOF11-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF14-ROBOT)
(BELOWOF8-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF13-ROBOT)
(LEFTOF11-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF9-ROBOT)
(BELOWOF1-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF13-ROBOT)
(BELOWOF3-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF2-ROBOT)
(ABOVEOF15-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF9-ROBOT)
(RIGHTOF9-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF5-ROBOT)
(RIGHTOF5-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF6-ROBOT)
(ABOVEOF12-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF2-ROBOT)
(ABOVEOF4-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF4-ROBOT)
(ABOVEOF11-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF1-ROBOT)
(ABOVEOF4-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF6-ROBOT)
(RIGHTOF14-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF5-ROBOT)
(ABOVEOF7-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF15-ROBOT)
(BELOWOF11-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF10-ROBOT)
(ABOVEOF15-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF14-ROBOT)
(BELOWOF3-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF15-ROBOT)
(LEFTOF2-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF1-ROBOT)
(ABOVEOF16-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF16-ROBOT)
(BELOWOF12-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF10-ROBOT)
(ABOVEOF16-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF16-ROBOT)
(LEFTOF9-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF10-ROBOT)
(LEFTOF6-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF12-ROBOT)
(LEFTOF6-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF10-ROBOT)
(RIGHTOF13-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF16-ROBOT)
(LEFTOF6-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF3-ROBOT)
(ABOVEOF15-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF6-ROBOT)
(LEFTOF1-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF8-ROBOT)
(ABOVEOF15-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF3-ROBOT)
(ABOVEOF8-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF12-ROBOT)
(BELOWOF9-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF5-ROBOT)
(RIGHTOF10-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF9-ROBOT)
(BELOWOF7-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF6-ROBOT)
(LEFTOF2-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF4-ROBOT)
(LEFTOF1-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF11-ROBOT)
(BELOWOF10-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF11-ROBOT)
(BELOWOF5-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF6-ROBOT)
(BELOWOF3-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF8-ROBOT)
(BELOWOF4-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF8-ROBOT)
(RIGHTOF15-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF2-ROBOT)
(RIGHTOF10-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF12-ROBOT)
(LEFTOF11-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF2-ROBOT)
(RIGHTOF13-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF13-ROBOT)
(LEFTOF13-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF9-ROBOT)
(ABOVEOF15-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF11-ROBOT)
(BELOWOF2-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF2-ROBOT)
(LEFTOF1-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF2-ROBOT)
(ABOVEOF13-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF16-ROBOT)
(LEFTOF12-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF10-ROBOT)
(BELOWOF2-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF7-ROBOT)
(ABOVEOF8-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF5-ROBOT)
(RIGHTOF6-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF14-ROBOT)
(BELOWOF7-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF14-ROBOT)
(BELOWOF13-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF13-ROBOT)
(LEFTOF6-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF1-ROBOT)
(ABOVEOF11-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF3-ROBOT)
(RIGHTOF9-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF12-ROBOT)
(LEFTOF4-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF14-ROBOT)
(LEFTOF4-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF16-ROBOT)
(LEFTOF14-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF1-ROBOT)
(RIGHTOF10-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF10-ROBOT)
(ABOVEOF12-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF13-ROBOT)
(BELOWOF5-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF7-ROBOT)
(ABOVEOF15-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF12-ROBOT)
(BELOWOF8-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF6-ROBOT)
(RIGHTOF15-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF10-ROBOT)
(BELOWOF1-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF4-ROBOT)
(RIGHTOF9-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF10-ROBOT)
(BELOWOF4-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF7-ROBOT)
(LEFTOF2-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF3-ROBOT)
(ABOVEOF4-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF10-ROBOT)
(RIGHTOF14-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF16-ROBOT)
(BELOWOF14-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF12-ROBOT)
(BELOWOF11-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF12-ROBOT)
(RIGHTOF15-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF5-ROBOT)
(ABOVEOF14-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF11-ROBOT)
(ABOVEOF14-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF14-ROBOT)
(LEFTOF12-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF9-ROBOT)
(BELOWOF4-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF5-ROBOT)
(ABOVEOF10-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF12-ROBOT)
(LEFTOF1-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF9-ROBOT)
(RIGHTOF12-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF4-ROBOT)
(LEFTOF3-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF4-ROBOT)
(ABOVEOF13-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF10-ROBOT)
(LEFTOF7-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF7-ROBOT)
(LEFTOF3-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF13-ROBOT)
(LEFTOF12-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF8-ROBOT)
(BELOWOF1-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF12-ROBOT)
(LEFTOF3-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF7-ROBOT)
(RIGHTOF15-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF5-ROBOT)
(RIGHTOF11-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF11-ROBOT)
(LEFTOF7-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF16-ROBOT)
(BELOWOF9-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF12-ROBOT)
(ABOVEOF15-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF7-ROBOT)
(RIGHTOF8-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF5-ROBOT)
(ABOVEOF8-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF2-ROBOT)
(ABOVEOF16-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF4-ROBOT)
(RIGHTOF11-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF8-ROBOT)
(RIGHTOF9-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF8-ROBOT)
(RIGHTOF14-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF7-ROBOT)
(RIGHTOF12-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF5-ROBOT)
(ABOVEOF12-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF4-ROBOT)
(ABOVEOF5-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF11-ROBOT)
(RIGHTOF15-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF4-ROBOT)
(ABOVEOF12-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF10-ROBOT)
(LEFTOF3-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF2-ROBOT)
(RIGHTOF4-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF13-ROBOT)
(RIGHTOF15-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF12-ROBOT)
(LEFTOF8-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF10-ROBOT)
(RIGHTOF11-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF2-ROBOT)
(ABOVEOF6-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF1-ROBOT)
(ABOVEOF3-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF11-ROBOT)
(ABOVEOF16-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF3-ROBOT)
(BELOWOF3-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF13-ROBOT)
(LEFTOF9-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF11-ROBOT)
(ABOVEOF13-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF8-ROBOT)
(ABOVEOF13-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF10-ROBOT)
(LEFTOF8-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF13-ROBOT)
(LEFTOF4-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF14-ROBOT)
(LEFTOF14-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF15-ROBOT)
(LEFTOF10-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF3-ROBOT)
(LEFTOF2-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF6-ROBOT)
(ABOVEOF10-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF9-ROBOT)
(LEFTOF5-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF5-ROBOT)
(ABOVEOF15-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF5-ROBOT)
(ABOVEOF16-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF8-ROBOT)
(LEFTOF5-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF10-ROBOT)
(ABOVEOF13-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF10-ROBOT)
(LEFTOF9-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF4-ROBOT)
(ABOVEOF16-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF14-ROBOT)
(BELOWOF12-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF5-ROBOT)
(ABOVEOF9-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF6-ROBOT)
(ABOVEOF8-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF6-ROBOT)
(LEFTOF3-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF13-ROBOT)
(BELOWOF9-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF16-ROBOT)
(LEFTOF10-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF8-ROBOT)
(RIGHTOF16-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF2-ROBOT)
(ABOVEOF5-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF14-ROBOT)
(RIGHTOF15-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF8-ROBOT)
(ABOVEOF10-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF2-ROBOT)
(RIGHTOF8-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF14-ROBOT)
(BELOWOF9-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF2-ROBOT)
(RIGHTOF5-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF13-ROBOT)
(ABOVEOF15-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF4-ROBOT)
(RIGHTOF4-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF16-ROBOT)
(BELOWOF3-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF10-ROBOT)
(BELOWOF7-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF3-ROBOT)
(ABOVEOF7-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF5-ROBOT)
(ABOVEOF5-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF4-ROBOT)
(ABOVEOF4-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF11-ROBOT)
(RIGHTOF11-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF16-ROBOT)
(LEFTOF15-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF10-ROBOT)
(BELOWOF9-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF9-ROBOT)
(BELOWOF6-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF3-ROBOT)
(ABOVEOF9-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF6-ROBOT)
(RIGHTOF11-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF7-ROBOT)
(RIGHTOF7-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF9-ROBOT)
(RIGHTOF11-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF13-ROBOT)
(ABOVEOF16-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF14-ROBOT)
(BELOWOF14-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF15-ROBOT)
(BELOWOF15-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF4-ROBOT)
(RIGHTOF7-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF5-ROBOT)
(BELOWOF3-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF9-ROBOT)
(ABOVEOF9-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF16-ROBOT)
(LEFTOF16-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF5-ROBOT)
(LEFTOF4-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF7-ROBOT)
(LEFTOF1-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF2-ROBOT)
(BELOWOF1-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF5-ROBOT)
(RIGHTOF7-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF3-ROBOT)
(LEFTOF1-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF3-ROBOT)
(BELOWOF2-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF8-ROBOT)
(RIGHTOF11-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF15-ROBOT)
(LEFTOF15-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF2-ROBOT)
(RIGHTOF2-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF14-ROBOT)
(LEFTOF11-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF5-ROBOT)
(ABOVEOF6-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF6-ROBOT)
(RIGHTOF6-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF8-ROBOT)
(RIGHTOF8-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF14-ROBOT)
(ABOVEOF15-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF3-ROBOT)
(LEFTOF3-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF12-ROBOT)
(BELOWOF12-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF1-ROBOT)
(LEFTOF1-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF13-ROBOT)
(BELOWOF12-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF1-ROBOT)
(ABOVEOF1-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF16-ROBOT)
(ABOVEOF16-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF9-ROBOT)
(BELOWOF8-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF13-ROBOT)
(BELOWOF13-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF10-ROBOT)
(RIGHTOF10-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF15-ROBOT)
(ABOVEOF16-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF7-ROBOT)
(BELOWOF7-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF11-ROBOT)
(BELOWOF11-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF12-ROBOT)
(RIGHTOF12-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF2-ROBOT)
(BELOWOF2-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF6-ROBOT)
(BELOWOF6-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(NOT-CHECKCONSISTENCY-)
(not (CHECKCONSISTENCY-))
)
)
(:derived (Flag4-)(and (NOT-CHECKCONSISTENCY-)(NOT-ERROR-)(Flag3-)))

(:derived (Flag216-)(and (ABOVEOF6-ROBOT)))

(:derived (Flag216-)(and (ABOVEOF11-ROBOT)))

(:derived (Flag216-)(and (ABOVEOF10-ROBOT)))

(:derived (Flag216-)(and (ABOVEOF2-ROBOT)))

(:derived (Flag216-)(and (ABOVEOF7-ROBOT)))

(:derived (Flag216-)(and (ABOVEOF12-ROBOT)))

(:derived (Flag216-)(and (ABOVEOF15-ROBOT)))

(:derived (Flag216-)(and (ABOVEOF8-ROBOT)))

(:derived (Flag216-)(and (ABOVEOF9-ROBOT)))

(:derived (Flag216-)(and (ABOVEOF16-ROBOT)))

(:derived (Flag216-)(and (ABOVEOF4-ROBOT)))

(:derived (Flag216-)(and (ABOVEOF3-ROBOT)))

(:derived (Flag216-)(and (ABOVEOF13-ROBOT)))

(:derived (Flag216-)(and (ABOVEOF14-ROBOT)))

(:derived (Flag216-)(and (ABOVEOF5-ROBOT)))

(:derived (Flag216-)(and (ABOVEOF1-ROBOT)))

(:derived (Flag217-)(and (ABOVEOF11-ROBOT)))

(:derived (Flag217-)(and (ABOVEOF6-ROBOT)))

(:derived (Flag217-)(and (ABOVEOF10-ROBOT)))

(:derived (Flag217-)(and (ABOVEOF2-ROBOT)))

(:derived (Flag217-)(and (ABOVEOF7-ROBOT)))

(:derived (Flag217-)(and (ABOVEOF12-ROBOT)))

(:derived (Flag217-)(and (ABOVEOF15-ROBOT)))

(:derived (Flag217-)(and (ABOVEOF8-ROBOT)))

(:derived (Flag217-)(and (ABOVEOF9-ROBOT)))

(:derived (Flag217-)(and (ABOVEOF16-ROBOT)))

(:derived (Flag217-)(and (ABOVEOF4-ROBOT)))

(:derived (Flag217-)(and (ABOVEOF3-ROBOT)))

(:derived (Flag217-)(and (ABOVEOF13-ROBOT)))

(:derived (Flag217-)(and (ABOVEOF14-ROBOT)))

(:derived (Flag217-)(and (ABOVEOF5-ROBOT)))

(:derived (Flag218-)(and (ABOVEOF11-ROBOT)))

(:derived (Flag218-)(and (ABOVEOF6-ROBOT)))

(:derived (Flag218-)(and (ABOVEOF10-ROBOT)))

(:derived (Flag218-)(and (ABOVEOF8-ROBOT)))

(:derived (Flag218-)(and (ABOVEOF7-ROBOT)))

(:derived (Flag218-)(and (ABOVEOF12-ROBOT)))

(:derived (Flag218-)(and (ABOVEOF15-ROBOT)))

(:derived (Flag218-)(and (ABOVEOF9-ROBOT)))

(:derived (Flag218-)(and (ABOVEOF16-ROBOT)))

(:derived (Flag218-)(and (ABOVEOF4-ROBOT)))

(:derived (Flag218-)(and (ABOVEOF3-ROBOT)))

(:derived (Flag218-)(and (ABOVEOF13-ROBOT)))

(:derived (Flag218-)(and (ABOVEOF14-ROBOT)))

(:derived (Flag218-)(and (ABOVEOF5-ROBOT)))

(:derived (Flag219-)(and (ABOVEOF11-ROBOT)))

(:derived (Flag219-)(and (ABOVEOF6-ROBOT)))

(:derived (Flag219-)(and (ABOVEOF10-ROBOT)))

(:derived (Flag219-)(and (ABOVEOF8-ROBOT)))

(:derived (Flag219-)(and (ABOVEOF7-ROBOT)))

(:derived (Flag219-)(and (ABOVEOF12-ROBOT)))

(:derived (Flag219-)(and (ABOVEOF15-ROBOT)))

(:derived (Flag219-)(and (ABOVEOF9-ROBOT)))

(:derived (Flag219-)(and (ABOVEOF16-ROBOT)))

(:derived (Flag219-)(and (ABOVEOF4-ROBOT)))

(:derived (Flag219-)(and (ABOVEOF13-ROBOT)))

(:derived (Flag219-)(and (ABOVEOF14-ROBOT)))

(:derived (Flag219-)(and (ABOVEOF5-ROBOT)))

(:derived (Flag220-)(and (ABOVEOF11-ROBOT)))

(:derived (Flag220-)(and (ABOVEOF6-ROBOT)))

(:derived (Flag220-)(and (ABOVEOF7-ROBOT)))

(:derived (Flag220-)(and (ABOVEOF8-ROBOT)))

(:derived (Flag220-)(and (ABOVEOF10-ROBOT)))

(:derived (Flag220-)(and (ABOVEOF12-ROBOT)))

(:derived (Flag220-)(and (ABOVEOF15-ROBOT)))

(:derived (Flag220-)(and (ABOVEOF9-ROBOT)))

(:derived (Flag220-)(and (ABOVEOF16-ROBOT)))

(:derived (Flag220-)(and (ABOVEOF13-ROBOT)))

(:derived (Flag220-)(and (ABOVEOF14-ROBOT)))

(:derived (Flag220-)(and (ABOVEOF5-ROBOT)))

(:derived (Flag221-)(and (ABOVEOF11-ROBOT)))

(:derived (Flag221-)(and (ABOVEOF6-ROBOT)))

(:derived (Flag221-)(and (ABOVEOF7-ROBOT)))

(:derived (Flag221-)(and (ABOVEOF8-ROBOT)))

(:derived (Flag221-)(and (ABOVEOF10-ROBOT)))

(:derived (Flag221-)(and (ABOVEOF12-ROBOT)))

(:derived (Flag221-)(and (ABOVEOF15-ROBOT)))

(:derived (Flag221-)(and (ABOVEOF9-ROBOT)))

(:derived (Flag221-)(and (ABOVEOF16-ROBOT)))

(:derived (Flag221-)(and (ABOVEOF13-ROBOT)))

(:derived (Flag221-)(and (ABOVEOF14-ROBOT)))

(:derived (Flag222-)(and (ABOVEOF11-ROBOT)))

(:derived (Flag222-)(and (ABOVEOF7-ROBOT)))

(:derived (Flag222-)(and (ABOVEOF8-ROBOT)))

(:derived (Flag222-)(and (ABOVEOF10-ROBOT)))

(:derived (Flag222-)(and (ABOVEOF12-ROBOT)))

(:derived (Flag222-)(and (ABOVEOF15-ROBOT)))

(:derived (Flag222-)(and (ABOVEOF9-ROBOT)))

(:derived (Flag222-)(and (ABOVEOF16-ROBOT)))

(:derived (Flag222-)(and (ABOVEOF13-ROBOT)))

(:derived (Flag222-)(and (ABOVEOF14-ROBOT)))

(:derived (Flag223-)(and (ABOVEOF11-ROBOT)))

(:derived (Flag223-)(and (ABOVEOF8-ROBOT)))

(:derived (Flag223-)(and (ABOVEOF10-ROBOT)))

(:derived (Flag223-)(and (ABOVEOF12-ROBOT)))

(:derived (Flag223-)(and (ABOVEOF15-ROBOT)))

(:derived (Flag223-)(and (ABOVEOF9-ROBOT)))

(:derived (Flag223-)(and (ABOVEOF16-ROBOT)))

(:derived (Flag223-)(and (ABOVEOF13-ROBOT)))

(:derived (Flag223-)(and (ABOVEOF14-ROBOT)))

(:derived (Flag224-)(and (ABOVEOF11-ROBOT)))

(:derived (Flag224-)(and (ABOVEOF10-ROBOT)))

(:derived (Flag224-)(and (ABOVEOF12-ROBOT)))

(:derived (Flag224-)(and (ABOVEOF15-ROBOT)))

(:derived (Flag224-)(and (ABOVEOF9-ROBOT)))

(:derived (Flag224-)(and (ABOVEOF16-ROBOT)))

(:derived (Flag224-)(and (ABOVEOF13-ROBOT)))

(:derived (Flag224-)(and (ABOVEOF14-ROBOT)))

(:derived (Flag225-)(and (ABOVEOF11-ROBOT)))

(:derived (Flag225-)(and (ABOVEOF10-ROBOT)))

(:derived (Flag225-)(and (ABOVEOF12-ROBOT)))

(:derived (Flag225-)(and (ABOVEOF15-ROBOT)))

(:derived (Flag225-)(and (ABOVEOF16-ROBOT)))

(:derived (Flag225-)(and (ABOVEOF13-ROBOT)))

(:derived (Flag225-)(and (ABOVEOF14-ROBOT)))

(:derived (Flag226-)(and (ABOVEOF11-ROBOT)))

(:derived (Flag226-)(and (ABOVEOF12-ROBOT)))

(:derived (Flag226-)(and (ABOVEOF15-ROBOT)))

(:derived (Flag226-)(and (ABOVEOF16-ROBOT)))

(:derived (Flag226-)(and (ABOVEOF13-ROBOT)))

(:derived (Flag226-)(and (ABOVEOF14-ROBOT)))

(:derived (Flag227-)(and (ABOVEOF16-ROBOT)))

(:derived (Flag227-)(and (ABOVEOF12-ROBOT)))

(:derived (Flag227-)(and (ABOVEOF13-ROBOT)))

(:derived (Flag227-)(and (ABOVEOF14-ROBOT)))

(:derived (Flag227-)(and (ABOVEOF15-ROBOT)))

(:derived (Flag228-)(and (ABOVEOF16-ROBOT)))

(:derived (Flag228-)(and (ABOVEOF13-ROBOT)))

(:derived (Flag228-)(and (ABOVEOF14-ROBOT)))

(:derived (Flag228-)(and (ABOVEOF15-ROBOT)))

(:derived (Flag229-)(and (ABOVEOF16-ROBOT)))

(:derived (Flag229-)(and (ABOVEOF14-ROBOT)))

(:derived (Flag229-)(and (ABOVEOF15-ROBOT)))

(:derived (Flag230-)(and (ABOVEOF15-ROBOT)))

(:derived (Flag230-)(and (ABOVEOF16-ROBOT)))

(:derived (Flag231-)(and (BELOWOF2-ROBOT)))

(:derived (Flag231-)(and (BELOWOF1-ROBOT)))

(:derived (Flag232-)(and (BELOWOF2-ROBOT)))

(:derived (Flag232-)(and (BELOWOF1-ROBOT)))

(:derived (Flag232-)(and (BELOWOF3-ROBOT)))

(:derived (Flag233-)(and (BELOWOF2-ROBOT)))

(:derived (Flag233-)(and (BELOWOF1-ROBOT)))

(:derived (Flag233-)(and (BELOWOF3-ROBOT)))

(:derived (Flag233-)(and (BELOWOF4-ROBOT)))

(:derived (Flag234-)(and (BELOWOF2-ROBOT)))

(:derived (Flag234-)(and (BELOWOF1-ROBOT)))

(:derived (Flag234-)(and (BELOWOF3-ROBOT)))

(:derived (Flag234-)(and (BELOWOF4-ROBOT)))

(:derived (Flag234-)(and (BELOWOF5-ROBOT)))

(:derived (Flag235-)(and (BELOWOF6-ROBOT)))

(:derived (Flag235-)(and (BELOWOF5-ROBOT)))

(:derived (Flag235-)(and (BELOWOF2-ROBOT)))

(:derived (Flag235-)(and (BELOWOF1-ROBOT)))

(:derived (Flag235-)(and (BELOWOF4-ROBOT)))

(:derived (Flag235-)(and (BELOWOF3-ROBOT)))

(:derived (Flag236-)(and (BELOWOF6-ROBOT)))

(:derived (Flag236-)(and (BELOWOF5-ROBOT)))

(:derived (Flag236-)(and (BELOWOF2-ROBOT)))

(:derived (Flag236-)(and (BELOWOF1-ROBOT)))

(:derived (Flag236-)(and (BELOWOF4-ROBOT)))

(:derived (Flag236-)(and (BELOWOF7-ROBOT)))

(:derived (Flag236-)(and (BELOWOF3-ROBOT)))

(:derived (Flag237-)(and (BELOWOF6-ROBOT)))

(:derived (Flag237-)(and (BELOWOF5-ROBOT)))

(:derived (Flag237-)(and (BELOWOF2-ROBOT)))

(:derived (Flag237-)(and (BELOWOF1-ROBOT)))

(:derived (Flag237-)(and (BELOWOF4-ROBOT)))

(:derived (Flag237-)(and (BELOWOF7-ROBOT)))

(:derived (Flag237-)(and (BELOWOF3-ROBOT)))

(:derived (Flag237-)(and (BELOWOF8-ROBOT)))

(:derived (Flag238-)(and (BELOWOF6-ROBOT)))

(:derived (Flag238-)(and (BELOWOF5-ROBOT)))

(:derived (Flag238-)(and (BELOWOF2-ROBOT)))

(:derived (Flag238-)(and (BELOWOF1-ROBOT)))

(:derived (Flag238-)(and (BELOWOF4-ROBOT)))

(:derived (Flag238-)(and (BELOWOF7-ROBOT)))

(:derived (Flag238-)(and (BELOWOF9-ROBOT)))

(:derived (Flag238-)(and (BELOWOF3-ROBOT)))

(:derived (Flag238-)(and (BELOWOF8-ROBOT)))

(:derived (Flag239-)(and (BELOWOF10-ROBOT)))

(:derived (Flag239-)(and (BELOWOF6-ROBOT)))

(:derived (Flag239-)(and (BELOWOF5-ROBOT)))

(:derived (Flag239-)(and (BELOWOF2-ROBOT)))

(:derived (Flag239-)(and (BELOWOF1-ROBOT)))

(:derived (Flag239-)(and (BELOWOF4-ROBOT)))

(:derived (Flag239-)(and (BELOWOF7-ROBOT)))

(:derived (Flag239-)(and (BELOWOF9-ROBOT)))

(:derived (Flag239-)(and (BELOWOF3-ROBOT)))

(:derived (Flag239-)(and (BELOWOF8-ROBOT)))

(:derived (Flag240-)(and (BELOWOF10-ROBOT)))

(:derived (Flag240-)(and (BELOWOF11-ROBOT)))

(:derived (Flag240-)(and (BELOWOF6-ROBOT)))

(:derived (Flag240-)(and (BELOWOF5-ROBOT)))

(:derived (Flag240-)(and (BELOWOF2-ROBOT)))

(:derived (Flag240-)(and (BELOWOF1-ROBOT)))

(:derived (Flag240-)(and (BELOWOF4-ROBOT)))

(:derived (Flag240-)(and (BELOWOF7-ROBOT)))

(:derived (Flag240-)(and (BELOWOF9-ROBOT)))

(:derived (Flag240-)(and (BELOWOF3-ROBOT)))

(:derived (Flag240-)(and (BELOWOF8-ROBOT)))

(:derived (Flag241-)(and (BELOWOF10-ROBOT)))

(:derived (Flag241-)(and (BELOWOF11-ROBOT)))

(:derived (Flag241-)(and (BELOWOF6-ROBOT)))

(:derived (Flag241-)(and (BELOWOF5-ROBOT)))

(:derived (Flag241-)(and (BELOWOF2-ROBOT)))

(:derived (Flag241-)(and (BELOWOF1-ROBOT)))

(:derived (Flag241-)(and (BELOWOF4-ROBOT)))

(:derived (Flag241-)(and (BELOWOF12-ROBOT)))

(:derived (Flag241-)(and (BELOWOF7-ROBOT)))

(:derived (Flag241-)(and (BELOWOF9-ROBOT)))

(:derived (Flag241-)(and (BELOWOF3-ROBOT)))

(:derived (Flag241-)(and (BELOWOF8-ROBOT)))

(:derived (Flag242-)(and (BELOWOF10-ROBOT)))

(:derived (Flag242-)(and (BELOWOF11-ROBOT)))

(:derived (Flag242-)(and (BELOWOF6-ROBOT)))

(:derived (Flag242-)(and (BELOWOF5-ROBOT)))

(:derived (Flag242-)(and (BELOWOF2-ROBOT)))

(:derived (Flag242-)(and (BELOWOF1-ROBOT)))

(:derived (Flag242-)(and (BELOWOF4-ROBOT)))

(:derived (Flag242-)(and (BELOWOF12-ROBOT)))

(:derived (Flag242-)(and (BELOWOF7-ROBOT)))

(:derived (Flag242-)(and (BELOWOF9-ROBOT)))

(:derived (Flag242-)(and (BELOWOF3-ROBOT)))

(:derived (Flag242-)(and (BELOWOF13-ROBOT)))

(:derived (Flag242-)(and (BELOWOF8-ROBOT)))

(:derived (Flag243-)(and (BELOWOF10-ROBOT)))

(:derived (Flag243-)(and (BELOWOF11-ROBOT)))

(:derived (Flag243-)(and (BELOWOF6-ROBOT)))

(:derived (Flag243-)(and (BELOWOF5-ROBOT)))

(:derived (Flag243-)(and (BELOWOF2-ROBOT)))

(:derived (Flag243-)(and (BELOWOF1-ROBOT)))

(:derived (Flag243-)(and (BELOWOF4-ROBOT)))

(:derived (Flag243-)(and (BELOWOF12-ROBOT)))

(:derived (Flag243-)(and (BELOWOF7-ROBOT)))

(:derived (Flag243-)(and (BELOWOF9-ROBOT)))

(:derived (Flag243-)(and (BELOWOF3-ROBOT)))

(:derived (Flag243-)(and (BELOWOF13-ROBOT)))

(:derived (Flag243-)(and (BELOWOF8-ROBOT)))

(:derived (Flag243-)(and (BELOWOF14-ROBOT)))

(:derived (Flag244-)(and (BELOWOF10-ROBOT)))

(:derived (Flag244-)(and (BELOWOF11-ROBOT)))

(:derived (Flag244-)(and (BELOWOF15-ROBOT)))

(:derived (Flag244-)(and (BELOWOF6-ROBOT)))

(:derived (Flag244-)(and (BELOWOF5-ROBOT)))

(:derived (Flag244-)(and (BELOWOF2-ROBOT)))

(:derived (Flag244-)(and (BELOWOF1-ROBOT)))

(:derived (Flag244-)(and (BELOWOF4-ROBOT)))

(:derived (Flag244-)(and (BELOWOF12-ROBOT)))

(:derived (Flag244-)(and (BELOWOF7-ROBOT)))

(:derived (Flag244-)(and (BELOWOF9-ROBOT)))

(:derived (Flag244-)(and (BELOWOF3-ROBOT)))

(:derived (Flag244-)(and (BELOWOF13-ROBOT)))

(:derived (Flag244-)(and (BELOWOF8-ROBOT)))

(:derived (Flag244-)(and (BELOWOF14-ROBOT)))

(:derived (Flag245-)(and (BELOWOF10-ROBOT)))

(:derived (Flag245-)(and (BELOWOF15-ROBOT)))

(:derived (Flag245-)(and (BELOWOF6-ROBOT)))

(:derived (Flag245-)(and (BELOWOF11-ROBOT)))

(:derived (Flag245-)(and (BELOWOF5-ROBOT)))

(:derived (Flag245-)(and (BELOWOF2-ROBOT)))

(:derived (Flag245-)(and (BELOWOF1-ROBOT)))

(:derived (Flag245-)(and (BELOWOF4-ROBOT)))

(:derived (Flag245-)(and (BELOWOF12-ROBOT)))

(:derived (Flag245-)(and (BELOWOF7-ROBOT)))

(:derived (Flag245-)(and (BELOWOF9-ROBOT)))

(:derived (Flag245-)(and (BELOWOF3-ROBOT)))

(:derived (Flag245-)(and (BELOWOF13-ROBOT)))

(:derived (Flag245-)(and (BELOWOF8-ROBOT)))

(:derived (Flag245-)(and (BELOWOF14-ROBOT)))

(:derived (Flag246-)(and (BELOWOF1-ROBOT)(ABOVEOF4-ROBOT)))

(:derived (Flag247-)(and (BELOWOF1-ROBOT)(ABOVEOF2-ROBOT)))

(:derived (Flag248-)(and (BELOWOF1-ROBOT)(ABOVEOF1-ROBOT)))

(:derived (Flag249-)(and (ABOVEOF12-ROBOT)(BELOWOF1-ROBOT)))

(:derived (Flag250-)(and (BELOWOF1-ROBOT)(ABOVEOF13-ROBOT)))

(:derived (Flag251-)(and (BELOWOF1-ROBOT)(ABOVEOF7-ROBOT)))

(:derived (Flag252-)(and (ABOVEOF11-ROBOT)(BELOWOF1-ROBOT)))

(:derived (Flag253-)(and (ABOVEOF6-ROBOT)(BELOWOF1-ROBOT)))

(:derived (Flag254-)(and (BELOWOF1-ROBOT)(ABOVEOF5-ROBOT)))

(:derived (Flag255-)(and (BELOWOF1-ROBOT)(ABOVEOF14-ROBOT)))

(:derived (Flag256-)(and (BELOWOF1-ROBOT)(ABOVEOF16-ROBOT)))

(:derived (Flag257-)(and (ABOVEOF10-ROBOT)(BELOWOF1-ROBOT)))

(:derived (Flag258-)(and (BELOWOF1-ROBOT)(ABOVEOF15-ROBOT)))

(:derived (Flag259-)(and (ABOVEOF8-ROBOT)(BELOWOF1-ROBOT)))

(:derived (Flag260-)(and (ABOVEOF3-ROBOT)(BELOWOF1-ROBOT)))

(:derived (Flag261-)(and (BELOWOF1-ROBOT)(ABOVEOF9-ROBOT)))

(:derived (Flag262-)(and (BELOWOF1-ROBOT)))

(:derived (Flag263-)(and (ABOVEOF1-ROBOT)(BELOWOF2-ROBOT)))

(:derived (Flag264-)(and (ABOVEOF7-ROBOT)(BELOWOF2-ROBOT)))

(:derived (Flag265-)(and (ABOVEOF15-ROBOT)(BELOWOF2-ROBOT)))

(:derived (Flag266-)(and (BELOWOF2-ROBOT)(ABOVEOF13-ROBOT)))

(:derived (Flag267-)(and (ABOVEOF2-ROBOT)(BELOWOF2-ROBOT)))

(:derived (Flag268-)(and (ABOVEOF16-ROBOT)(BELOWOF2-ROBOT)))

(:derived (Flag269-)(and (ABOVEOF4-ROBOT)(BELOWOF2-ROBOT)))

(:derived (Flag270-)(and (BELOWOF2-ROBOT)(ABOVEOF14-ROBOT)))

(:derived (Flag271-)(and (ABOVEOF6-ROBOT)(BELOWOF2-ROBOT)))

(:derived (Flag272-)(and (ABOVEOF9-ROBOT)(BELOWOF2-ROBOT)))

(:derived (Flag273-)(and (ABOVEOF11-ROBOT)(BELOWOF2-ROBOT)))

(:derived (Flag274-)(and (ABOVEOF10-ROBOT)(BELOWOF2-ROBOT)))

(:derived (Flag275-)(and (ABOVEOF8-ROBOT)(BELOWOF2-ROBOT)))

(:derived (Flag276-)(and (ABOVEOF12-ROBOT)(BELOWOF2-ROBOT)))

(:derived (Flag277-)(and (BELOWOF2-ROBOT)(ABOVEOF5-ROBOT)))

(:derived (Flag278-)(and (ABOVEOF3-ROBOT)(BELOWOF2-ROBOT)))

(:derived (Flag280-)(and (BELOWOF3-ROBOT)(ABOVEOF5-ROBOT)))

(:derived (Flag281-)(and (BELOWOF3-ROBOT)(ABOVEOF6-ROBOT)))

(:derived (Flag282-)(and (BELOWOF3-ROBOT)(ABOVEOF7-ROBOT)))

(:derived (Flag283-)(and (BELOWOF3-ROBOT)(ABOVEOF14-ROBOT)))

(:derived (Flag284-)(and (BELOWOF3-ROBOT)(ABOVEOF16-ROBOT)))

(:derived (Flag285-)(and (BELOWOF3-ROBOT)(ABOVEOF9-ROBOT)))

(:derived (Flag286-)(and (BELOWOF3-ROBOT)(ABOVEOF15-ROBOT)))

(:derived (Flag287-)(and (BELOWOF3-ROBOT)(ABOVEOF2-ROBOT)))

(:derived (Flag288-)(and (ABOVEOF3-ROBOT)(BELOWOF3-ROBOT)))

(:derived (Flag289-)(and (ABOVEOF8-ROBOT)(BELOWOF3-ROBOT)))

(:derived (Flag290-)(and (BELOWOF3-ROBOT)(ABOVEOF4-ROBOT)))

(:derived (Flag291-)(and (BELOWOF3-ROBOT)(ABOVEOF10-ROBOT)))

(:derived (Flag292-)(and (BELOWOF2-ROBOT)(ABOVEOF2-ROBOT)))

(:derived (Flag293-)(and (ABOVEOF12-ROBOT)(BELOWOF3-ROBOT)))

(:derived (Flag294-)(and (BELOWOF3-ROBOT)(ABOVEOF13-ROBOT)))

(:derived (Flag295-)(and (ABOVEOF11-ROBOT)(BELOWOF3-ROBOT)))

(:derived (Flag297-)(and (ABOVEOF9-ROBOT)(BELOWOF4-ROBOT)))

(:derived (Flag298-)(and (BELOWOF4-ROBOT)(ABOVEOF13-ROBOT)))

(:derived (Flag299-)(and (ABOVEOF3-ROBOT)(BELOWOF4-ROBOT)))

(:derived (Flag300-)(and (BELOWOF4-ROBOT)(ABOVEOF7-ROBOT)))

(:derived (Flag301-)(and (ABOVEOF6-ROBOT)(BELOWOF4-ROBOT)))

(:derived (Flag302-)(and (BELOWOF4-ROBOT)(ABOVEOF4-ROBOT)))

(:derived (Flag303-)(and (BELOWOF4-ROBOT)(ABOVEOF16-ROBOT)))

(:derived (Flag304-)(and (ABOVEOF8-ROBOT)(BELOWOF4-ROBOT)))

(:derived (Flag305-)(and (ABOVEOF12-ROBOT)(BELOWOF4-ROBOT)))

(:derived (Flag306-)(and (BELOWOF4-ROBOT)(ABOVEOF5-ROBOT)))

(:derived (Flag307-)(and (BELOWOF3-ROBOT)(ABOVEOF3-ROBOT)))

(:derived (Flag308-)(and (ABOVEOF10-ROBOT)(BELOWOF4-ROBOT)))

(:derived (Flag309-)(and (BELOWOF4-ROBOT)(ABOVEOF15-ROBOT)))

(:derived (Flag310-)(and (BELOWOF3-ROBOT)(ABOVEOF11-ROBOT)))

(:derived (Flag311-)(and (ABOVEOF11-ROBOT)(BELOWOF4-ROBOT)))

(:derived (Flag312-)(and (BELOWOF4-ROBOT)(ABOVEOF14-ROBOT)))

(:derived (Flag314-)(and (ABOVEOF4-ROBOT)(BELOWOF5-ROBOT)))

(:derived (Flag315-)(and (ABOVEOF15-ROBOT)(BELOWOF5-ROBOT)))

(:derived (Flag316-)(and (ABOVEOF8-ROBOT)(BELOWOF5-ROBOT)))

(:derived (Flag317-)(and (ABOVEOF16-ROBOT)(BELOWOF5-ROBOT)))

(:derived (Flag318-)(and (ABOVEOF13-ROBOT)(BELOWOF5-ROBOT)))

(:derived (Flag319-)(and (ABOVEOF10-ROBOT)(BELOWOF5-ROBOT)))

(:derived (Flag320-)(and (ABOVEOF16-ROBOT)(BELOWOF4-ROBOT)))

(:derived (Flag321-)(and (ABOVEOF5-ROBOT)(BELOWOF5-ROBOT)))

(:derived (Flag322-)(and (ABOVEOF7-ROBOT)(BELOWOF5-ROBOT)))

(:derived (Flag323-)(and (ABOVEOF9-ROBOT)(BELOWOF5-ROBOT)))

(:derived (Flag324-)(and (ABOVEOF12-ROBOT)(BELOWOF5-ROBOT)))

(:derived (Flag325-)(and (ABOVEOF11-ROBOT)(BELOWOF5-ROBOT)))

(:derived (Flag326-)(and (ABOVEOF6-ROBOT)(BELOWOF5-ROBOT)))

(:derived (Flag327-)(and (ABOVEOF14-ROBOT)(BELOWOF5-ROBOT)))

(:derived (Flag329-)(and (BELOWOF6-ROBOT)(ABOVEOF15-ROBOT)))

(:derived (Flag330-)(and (BELOWOF6-ROBOT)(ABOVEOF5-ROBOT)))

(:derived (Flag331-)(and (ABOVEOF11-ROBOT)(BELOWOF6-ROBOT)))

(:derived (Flag332-)(and (ABOVEOF12-ROBOT)(BELOWOF6-ROBOT)))

(:derived (Flag333-)(and (BELOWOF5-ROBOT)(ABOVEOF13-ROBOT)))

(:derived (Flag334-)(and (BELOWOF6-ROBOT)(ABOVEOF13-ROBOT)))

(:derived (Flag335-)(and (BELOWOF6-ROBOT)(ABOVEOF7-ROBOT)))

(:derived (Flag336-)(and (BELOWOF5-ROBOT)(ABOVEOF5-ROBOT)))

(:derived (Flag337-)(and (BELOWOF6-ROBOT)(ABOVEOF6-ROBOT)))

(:derived (Flag338-)(and (ABOVEOF8-ROBOT)(BELOWOF6-ROBOT)))

(:derived (Flag339-)(and (BELOWOF6-ROBOT)(ABOVEOF9-ROBOT)))

(:derived (Flag340-)(and (BELOWOF6-ROBOT)(ABOVEOF14-ROBOT)))

(:derived (Flag341-)(and (BELOWOF6-ROBOT)(ABOVEOF10-ROBOT)))

(:derived (Flag342-)(and (BELOWOF6-ROBOT)(ABOVEOF16-ROBOT)))

(:derived (Flag344-)(and (ABOVEOF6-ROBOT)(BELOWOF7-ROBOT)))

(:derived (Flag345-)(and (ABOVEOF11-ROBOT)(BELOWOF7-ROBOT)))

(:derived (Flag346-)(and (ABOVEOF9-ROBOT)(BELOWOF7-ROBOT)))

(:derived (Flag347-)(and (ABOVEOF6-ROBOT)(BELOWOF6-ROBOT)))

(:derived (Flag348-)(and (ABOVEOF10-ROBOT)(BELOWOF6-ROBOT)))

(:derived (Flag349-)(and (BELOWOF7-ROBOT)(ABOVEOF15-ROBOT)))

(:derived (Flag350-)(and (ABOVEOF12-ROBOT)(BELOWOF7-ROBOT)))

(:derived (Flag351-)(and (ABOVEOF10-ROBOT)(BELOWOF7-ROBOT)))

(:derived (Flag352-)(and (BELOWOF7-ROBOT)(ABOVEOF14-ROBOT)))

(:derived (Flag353-)(and (BELOWOF7-ROBOT)(ABOVEOF13-ROBOT)))

(:derived (Flag354-)(and (ABOVEOF7-ROBOT)(BELOWOF7-ROBOT)))

(:derived (Flag355-)(and (ABOVEOF8-ROBOT)(BELOWOF7-ROBOT)))

(:derived (Flag356-)(and (ABOVEOF16-ROBOT)(BELOWOF7-ROBOT)))

(:derived (Flag358-)(and (BELOWOF8-ROBOT)(ABOVEOF16-ROBOT)))

(:derived (Flag359-)(and (BELOWOF8-ROBOT)(ABOVEOF13-ROBOT)))

(:derived (Flag360-)(and (BELOWOF8-ROBOT)(ABOVEOF10-ROBOT)))

(:derived (Flag361-)(and (ABOVEOF12-ROBOT)(BELOWOF8-ROBOT)))

(:derived (Flag362-)(and (ABOVEOF8-ROBOT)(BELOWOF8-ROBOT)))

(:derived (Flag363-)(and (BELOWOF8-ROBOT)(ABOVEOF9-ROBOT)))

(:derived (Flag364-)(and (BELOWOF7-ROBOT)(ABOVEOF7-ROBOT)))

(:derived (Flag365-)(and (BELOWOF8-ROBOT)(ABOVEOF7-ROBOT)))

(:derived (Flag366-)(and (BELOWOF8-ROBOT)(ABOVEOF15-ROBOT)))

(:derived (Flag367-)(and (BELOWOF8-ROBOT)(ABOVEOF11-ROBOT)))

(:derived (Flag368-)(and (BELOWOF8-ROBOT)(ABOVEOF14-ROBOT)))

(:derived (Flag370-)(and (BELOWOF9-ROBOT)(ABOVEOF13-ROBOT)))

(:derived (Flag371-)(and (ABOVEOF11-ROBOT)(BELOWOF9-ROBOT)))

(:derived (Flag372-)(and (BELOWOF8-ROBOT)(ABOVEOF12-ROBOT)))

(:derived (Flag373-)(and (ABOVEOF10-ROBOT)(BELOWOF9-ROBOT)))

(:derived (Flag374-)(and (BELOWOF9-ROBOT)(ABOVEOF14-ROBOT)))

(:derived (Flag375-)(and (BELOWOF9-ROBOT)(ABOVEOF9-ROBOT)))

(:derived (Flag376-)(and (BELOWOF9-ROBOT)(ABOVEOF16-ROBOT)))

(:derived (Flag377-)(and (BELOWOF8-ROBOT)(ABOVEOF8-ROBOT)))

(:derived (Flag378-)(and (BELOWOF9-ROBOT)(ABOVEOF15-ROBOT)))

(:derived (Flag379-)(and (ABOVEOF8-ROBOT)(BELOWOF9-ROBOT)))

(:derived (Flag380-)(and (ABOVEOF12-ROBOT)(BELOWOF9-ROBOT)))

(:derived (Flag382-)(and (BELOWOF10-ROBOT)(ABOVEOF15-ROBOT)))

(:derived (Flag383-)(and (BELOWOF10-ROBOT)(ABOVEOF9-ROBOT)))

(:derived (Flag384-)(and (BELOWOF10-ROBOT)(ABOVEOF14-ROBOT)))

(:derived (Flag385-)(and (BELOWOF10-ROBOT)(ABOVEOF10-ROBOT)))

(:derived (Flag386-)(and (ABOVEOF12-ROBOT)(BELOWOF10-ROBOT)))

(:derived (Flag387-)(and (BELOWOF10-ROBOT)(ABOVEOF16-ROBOT)))

(:derived (Flag388-)(and (ABOVEOF9-ROBOT)(BELOWOF9-ROBOT)))

(:derived (Flag389-)(and (ABOVEOF11-ROBOT)(BELOWOF10-ROBOT)))

(:derived (Flag390-)(and (BELOWOF10-ROBOT)(ABOVEOF13-ROBOT)))

(:derived (Flag392-)(and (BELOWOF11-ROBOT)(ABOVEOF16-ROBOT)))

(:derived (Flag393-)(and (ABOVEOF11-ROBOT)(BELOWOF11-ROBOT)))

(:derived (Flag394-)(and (BELOWOF11-ROBOT)(ABOVEOF13-ROBOT)))

(:derived (Flag395-)(and (BELOWOF11-ROBOT)(ABOVEOF15-ROBOT)))

(:derived (Flag396-)(and (BELOWOF11-ROBOT)(ABOVEOF10-ROBOT)))

(:derived (Flag397-)(and (BELOWOF11-ROBOT)(ABOVEOF14-ROBOT)))

(:derived (Flag398-)(and (ABOVEOF12-ROBOT)(BELOWOF11-ROBOT)))

(:derived (Flag400-)(and (BELOWOF12-ROBOT)(ABOVEOF15-ROBOT)))

(:derived (Flag401-)(and (ABOVEOF12-ROBOT)(BELOWOF12-ROBOT)))

(:derived (Flag402-)(and (BELOWOF12-ROBOT)(ABOVEOF13-ROBOT)))

(:derived (Flag403-)(and (BELOWOF12-ROBOT)(ABOVEOF11-ROBOT)))

(:derived (Flag404-)(and (BELOWOF12-ROBOT)(ABOVEOF16-ROBOT)))

(:derived (Flag405-)(and (BELOWOF12-ROBOT)(ABOVEOF14-ROBOT)))

(:derived (Flag407-)(and (ABOVEOF14-ROBOT)(BELOWOF13-ROBOT)))

(:derived (Flag408-)(and (BELOWOF12-ROBOT)(ABOVEOF12-ROBOT)))

(:derived (Flag409-)(and (ABOVEOF13-ROBOT)(BELOWOF13-ROBOT)))

(:derived (Flag410-)(and (ABOVEOF15-ROBOT)(BELOWOF13-ROBOT)))

(:derived (Flag411-)(and (ABOVEOF12-ROBOT)(BELOWOF13-ROBOT)))

(:derived (Flag412-)(and (ABOVEOF16-ROBOT)(BELOWOF13-ROBOT)))

(:derived (Flag414-)(and (BELOWOF14-ROBOT)(ABOVEOF14-ROBOT)))

(:derived (Flag415-)(and (ABOVEOF15-ROBOT)(BELOWOF14-ROBOT)))

(:derived (Flag416-)(and (BELOWOF13-ROBOT)(ABOVEOF13-ROBOT)))

(:derived (Flag417-)(and (BELOWOF14-ROBOT)(ABOVEOF13-ROBOT)))

(:derived (Flag418-)(and (ABOVEOF16-ROBOT)(BELOWOF14-ROBOT)))

(:derived (Flag420-)(and (BELOWOF15-ROBOT)(ABOVEOF14-ROBOT)))

(:derived (Flag421-)(and (BELOWOF15-ROBOT)(ABOVEOF15-ROBOT)))

(:derived (Flag422-)(and (ABOVEOF16-ROBOT)(BELOWOF15-ROBOT)))

(:derived (Flag424-)(and (ABOVEOF16-ROBOT)(BELOWOF16-ROBOT)))

(:derived (Flag425-)(and (BELOWOF16-ROBOT)(ABOVEOF15-ROBOT)))

(:derived (Flag430-)(and (BELOWOF10-ROBOT)))

(:derived (Flag430-)(and (BELOWOF15-ROBOT)))

(:derived (Flag430-)(and (BELOWOF6-ROBOT)))

(:derived (Flag430-)(and (BELOWOF17-ROBOT)))

(:derived (Flag430-)(and (BELOWOF5-ROBOT)))

(:derived (Flag430-)(and (BELOWOF2-ROBOT)))

(:derived (Flag430-)(and (BELOWOF1-ROBOT)))

(:derived (Flag430-)(and (BELOWOF4-ROBOT)))

(:derived (Flag430-)(and (BELOWOF11-ROBOT)))

(:derived (Flag430-)(and (BELOWOF12-ROBOT)))

(:derived (Flag430-)(and (BELOWOF7-ROBOT)))

(:derived (Flag430-)(and (BELOWOF9-ROBOT)))

(:derived (Flag430-)(and (BELOWOF3-ROBOT)))

(:derived (Flag430-)(and (BELOWOF13-ROBOT)))

(:derived (Flag430-)(and (BELOWOF8-ROBOT)))

(:derived (Flag430-)(and (BELOWOF14-ROBOT)))

(:derived (Flag431-)(and (BELOWOF17-ROBOT)(ABOVEOF16-ROBOT)))

(:action MOVEDOWN-ROBOT
:parameters ()
:precondition
(and
(Flag2-)
)
:effect
(and
(when
(and
(Flag432-)
)
(and
(ROW15-ROBOT)
(not (NOT-ROW15-ROBOT))
)
)
(when
(and
(Flag426-)
)
(and
(ROW14-ROBOT)
(not (NOT-ROW14-ROBOT))
)
)
(when
(and
(Flag423-)
)
(and
(ROW13-ROBOT)
(not (NOT-ROW13-ROBOT))
)
)
(when
(and
(Flag419-)
)
(and
(ROW12-ROBOT)
(not (NOT-ROW12-ROBOT))
)
)
(when
(and
(Flag413-)
)
(and
(ROW11-ROBOT)
(not (NOT-ROW11-ROBOT))
)
)
(when
(and
(Flag406-)
)
(and
(ROW10-ROBOT)
(not (NOT-ROW10-ROBOT))
)
)
(when
(and
(Flag399-)
)
(and
(ROW9-ROBOT)
(not (NOT-ROW9-ROBOT))
)
)
(when
(and
(Flag391-)
)
(and
(ROW8-ROBOT)
(not (NOT-ROW8-ROBOT))
)
)
(when
(and
(Flag381-)
)
(and
(ROW7-ROBOT)
(not (NOT-ROW7-ROBOT))
)
)
(when
(and
(Flag369-)
)
(and
(ROW6-ROBOT)
(not (NOT-ROW6-ROBOT))
)
)
(when
(and
(Flag357-)
)
(and
(ROW5-ROBOT)
(not (NOT-ROW5-ROBOT))
)
)
(when
(and
(Flag343-)
)
(and
(ROW4-ROBOT)
(not (NOT-ROW4-ROBOT))
)
)
(when
(and
(Flag328-)
)
(and
(ROW3-ROBOT)
(not (NOT-ROW3-ROBOT))
)
)
(when
(and
(Flag313-)
)
(and
(ROW2-ROBOT)
(not (NOT-ROW2-ROBOT))
)
)
(when
(and
(Flag296-)
)
(and
(ROW1-ROBOT)
(not (NOT-ROW1-ROBOT))
)
)
(when
(and
(Flag279-)
)
(and
(ROW0-ROBOT)
(not (NOT-ROW0-ROBOT))
)
)
(when
(and
(ROW16-ROBOT)
)
(and
(ROW15-ROBOT)
(NOT-ROW16-ROBOT)
(not (NOT-ROW15-ROBOT))
(not (ROW16-ROBOT))
)
)
(when
(and
(ROW15-ROBOT)
)
(and
(ROW14-ROBOT)
(NOT-ROW15-ROBOT)
(not (NOT-ROW14-ROBOT))
(not (ROW15-ROBOT))
)
)
(when
(and
(ROW14-ROBOT)
)
(and
(ROW13-ROBOT)
(NOT-ROW14-ROBOT)
(not (NOT-ROW13-ROBOT))
(not (ROW14-ROBOT))
)
)
(when
(and
(ROW13-ROBOT)
)
(and
(ROW12-ROBOT)
(NOT-ROW13-ROBOT)
(not (NOT-ROW12-ROBOT))
(not (ROW13-ROBOT))
)
)
(when
(and
(ROW12-ROBOT)
)
(and
(ROW11-ROBOT)
(NOT-ROW12-ROBOT)
(not (NOT-ROW11-ROBOT))
(not (ROW12-ROBOT))
)
)
(when
(and
(ROW11-ROBOT)
)
(and
(ROW10-ROBOT)
(NOT-ROW11-ROBOT)
(not (NOT-ROW10-ROBOT))
(not (ROW11-ROBOT))
)
)
(when
(and
(ROW10-ROBOT)
)
(and
(ROW9-ROBOT)
(NOT-ROW10-ROBOT)
(not (NOT-ROW9-ROBOT))
(not (ROW10-ROBOT))
)
)
(when
(and
(ROW9-ROBOT)
)
(and
(ROW8-ROBOT)
(NOT-ROW9-ROBOT)
(not (NOT-ROW8-ROBOT))
(not (ROW9-ROBOT))
)
)
(when
(and
(ROW8-ROBOT)
)
(and
(ROW7-ROBOT)
(NOT-ROW8-ROBOT)
(not (NOT-ROW7-ROBOT))
(not (ROW8-ROBOT))
)
)
(when
(and
(ROW7-ROBOT)
)
(and
(ROW6-ROBOT)
(NOT-ROW7-ROBOT)
(not (NOT-ROW6-ROBOT))
(not (ROW7-ROBOT))
)
)
(when
(and
(ROW6-ROBOT)
)
(and
(ROW5-ROBOT)
(NOT-ROW6-ROBOT)
(not (NOT-ROW5-ROBOT))
(not (ROW6-ROBOT))
)
)
(when
(and
(ROW5-ROBOT)
)
(and
(ROW4-ROBOT)
(NOT-ROW5-ROBOT)
(not (NOT-ROW4-ROBOT))
(not (ROW5-ROBOT))
)
)
(when
(and
(ROW4-ROBOT)
)
(and
(ROW3-ROBOT)
(NOT-ROW4-ROBOT)
(not (NOT-ROW3-ROBOT))
(not (ROW4-ROBOT))
)
)
(when
(and
(ROW3-ROBOT)
)
(and
(ROW2-ROBOT)
(NOT-ROW3-ROBOT)
(not (NOT-ROW2-ROBOT))
(not (ROW3-ROBOT))
)
)
(when
(and
(ROW2-ROBOT)
)
(and
(ROW1-ROBOT)
(NOT-ROW2-ROBOT)
(not (NOT-ROW1-ROBOT))
(not (ROW2-ROBOT))
)
)
(when
(and
(ROW1-ROBOT)
)
(and
(ROW0-ROBOT)
(NOT-ROW1-ROBOT)
(not (NOT-ROW0-ROBOT))
(not (ROW1-ROBOT))
)
)
(when
(and
(ABOVEOF16-ROBOT)
)
(and
(ABOVEOF15-ROBOT)
(NOT-ABOVEOF16-ROBOT)
(ABOVEOF14-ROBOT)
(NOT-ABOVEOF15-ROBOT)
(ABOVEOF13-ROBOT)
(NOT-ABOVEOF14-ROBOT)
(ABOVEOF12-ROBOT)
(NOT-ABOVEOF13-ROBOT)
(ABOVEOF11-ROBOT)
(NOT-ABOVEOF12-ROBOT)
(ABOVEOF10-ROBOT)
(NOT-ABOVEOF11-ROBOT)
(ABOVEOF9-ROBOT)
(NOT-ABOVEOF10-ROBOT)
(ABOVEOF8-ROBOT)
(NOT-ABOVEOF9-ROBOT)
(ABOVEOF7-ROBOT)
(NOT-ABOVEOF8-ROBOT)
(ABOVEOF6-ROBOT)
(NOT-ABOVEOF7-ROBOT)
(ABOVEOF5-ROBOT)
(NOT-ABOVEOF6-ROBOT)
(ABOVEOF4-ROBOT)
(NOT-ABOVEOF5-ROBOT)
(ABOVEOF3-ROBOT)
(NOT-ABOVEOF4-ROBOT)
(ABOVEOF2-ROBOT)
(NOT-ABOVEOF3-ROBOT)
(ABOVEOF1-ROBOT)
(NOT-ABOVEOF2-ROBOT)
(NOT-ABOVEOF1-ROBOT)
(not (ABOVEOF16-ROBOT))
)
)
(when
(and
(ABOVEOF15-ROBOT)
)
(and
(ABOVEOF14-ROBOT)
(NOT-ABOVEOF15-ROBOT)
(ABOVEOF13-ROBOT)
(NOT-ABOVEOF14-ROBOT)
(ABOVEOF12-ROBOT)
(NOT-ABOVEOF13-ROBOT)
(ABOVEOF11-ROBOT)
(NOT-ABOVEOF12-ROBOT)
(ABOVEOF10-ROBOT)
(NOT-ABOVEOF11-ROBOT)
(ABOVEOF9-ROBOT)
(NOT-ABOVEOF10-ROBOT)
(ABOVEOF8-ROBOT)
(NOT-ABOVEOF9-ROBOT)
(ABOVEOF7-ROBOT)
(NOT-ABOVEOF8-ROBOT)
(ABOVEOF6-ROBOT)
(NOT-ABOVEOF7-ROBOT)
(ABOVEOF5-ROBOT)
(NOT-ABOVEOF6-ROBOT)
(ABOVEOF4-ROBOT)
(NOT-ABOVEOF5-ROBOT)
(ABOVEOF3-ROBOT)
(NOT-ABOVEOF4-ROBOT)
(ABOVEOF2-ROBOT)
(NOT-ABOVEOF3-ROBOT)
(ABOVEOF1-ROBOT)
(NOT-ABOVEOF2-ROBOT)
(NOT-ABOVEOF1-ROBOT)
(not (ABOVEOF15-ROBOT))
)
)
(when
(and
(ABOVEOF14-ROBOT)
)
(and
(ABOVEOF13-ROBOT)
(NOT-ABOVEOF14-ROBOT)
(ABOVEOF12-ROBOT)
(NOT-ABOVEOF13-ROBOT)
(ABOVEOF11-ROBOT)
(NOT-ABOVEOF12-ROBOT)
(ABOVEOF10-ROBOT)
(NOT-ABOVEOF11-ROBOT)
(ABOVEOF9-ROBOT)
(NOT-ABOVEOF10-ROBOT)
(ABOVEOF8-ROBOT)
(NOT-ABOVEOF9-ROBOT)
(ABOVEOF7-ROBOT)
(NOT-ABOVEOF8-ROBOT)
(ABOVEOF6-ROBOT)
(NOT-ABOVEOF7-ROBOT)
(ABOVEOF5-ROBOT)
(NOT-ABOVEOF6-ROBOT)
(ABOVEOF4-ROBOT)
(NOT-ABOVEOF5-ROBOT)
(ABOVEOF3-ROBOT)
(NOT-ABOVEOF4-ROBOT)
(ABOVEOF2-ROBOT)
(NOT-ABOVEOF3-ROBOT)
(ABOVEOF1-ROBOT)
(NOT-ABOVEOF2-ROBOT)
(NOT-ABOVEOF1-ROBOT)
(not (ABOVEOF14-ROBOT))
)
)
(when
(and
(ABOVEOF13-ROBOT)
)
(and
(ABOVEOF12-ROBOT)
(NOT-ABOVEOF13-ROBOT)
(ABOVEOF11-ROBOT)
(NOT-ABOVEOF12-ROBOT)
(ABOVEOF10-ROBOT)
(NOT-ABOVEOF11-ROBOT)
(ABOVEOF9-ROBOT)
(NOT-ABOVEOF10-ROBOT)
(ABOVEOF8-ROBOT)
(NOT-ABOVEOF9-ROBOT)
(ABOVEOF7-ROBOT)
(NOT-ABOVEOF8-ROBOT)
(ABOVEOF6-ROBOT)
(NOT-ABOVEOF7-ROBOT)
(ABOVEOF5-ROBOT)
(NOT-ABOVEOF6-ROBOT)
(ABOVEOF4-ROBOT)
(NOT-ABOVEOF5-ROBOT)
(ABOVEOF3-ROBOT)
(NOT-ABOVEOF4-ROBOT)
(ABOVEOF2-ROBOT)
(NOT-ABOVEOF3-ROBOT)
(ABOVEOF1-ROBOT)
(NOT-ABOVEOF2-ROBOT)
(NOT-ABOVEOF1-ROBOT)
(not (ABOVEOF13-ROBOT))
)
)
(when
(and
(ABOVEOF12-ROBOT)
)
(and
(ABOVEOF11-ROBOT)
(NOT-ABOVEOF12-ROBOT)
(ABOVEOF10-ROBOT)
(NOT-ABOVEOF11-ROBOT)
(ABOVEOF9-ROBOT)
(NOT-ABOVEOF10-ROBOT)
(ABOVEOF8-ROBOT)
(NOT-ABOVEOF9-ROBOT)
(ABOVEOF7-ROBOT)
(NOT-ABOVEOF8-ROBOT)
(ABOVEOF6-ROBOT)
(NOT-ABOVEOF7-ROBOT)
(ABOVEOF5-ROBOT)
(NOT-ABOVEOF6-ROBOT)
(ABOVEOF4-ROBOT)
(NOT-ABOVEOF5-ROBOT)
(ABOVEOF3-ROBOT)
(NOT-ABOVEOF4-ROBOT)
(ABOVEOF2-ROBOT)
(NOT-ABOVEOF3-ROBOT)
(ABOVEOF1-ROBOT)
(NOT-ABOVEOF2-ROBOT)
(NOT-ABOVEOF1-ROBOT)
(not (ABOVEOF12-ROBOT))
)
)
(when
(and
(ABOVEOF11-ROBOT)
)
(and
(ABOVEOF10-ROBOT)
(NOT-ABOVEOF11-ROBOT)
(ABOVEOF9-ROBOT)
(NOT-ABOVEOF10-ROBOT)
(ABOVEOF8-ROBOT)
(NOT-ABOVEOF9-ROBOT)
(ABOVEOF7-ROBOT)
(NOT-ABOVEOF8-ROBOT)
(ABOVEOF6-ROBOT)
(NOT-ABOVEOF7-ROBOT)
(ABOVEOF5-ROBOT)
(NOT-ABOVEOF6-ROBOT)
(ABOVEOF4-ROBOT)
(NOT-ABOVEOF5-ROBOT)
(ABOVEOF3-ROBOT)
(NOT-ABOVEOF4-ROBOT)
(ABOVEOF2-ROBOT)
(NOT-ABOVEOF3-ROBOT)
(ABOVEOF1-ROBOT)
(NOT-ABOVEOF2-ROBOT)
(NOT-ABOVEOF1-ROBOT)
(not (ABOVEOF11-ROBOT))
)
)
(when
(and
(ABOVEOF10-ROBOT)
)
(and
(ABOVEOF9-ROBOT)
(NOT-ABOVEOF10-ROBOT)
(ABOVEOF8-ROBOT)
(NOT-ABOVEOF9-ROBOT)
(ABOVEOF7-ROBOT)
(NOT-ABOVEOF8-ROBOT)
(ABOVEOF6-ROBOT)
(NOT-ABOVEOF7-ROBOT)
(ABOVEOF5-ROBOT)
(NOT-ABOVEOF6-ROBOT)
(ABOVEOF4-ROBOT)
(NOT-ABOVEOF5-ROBOT)
(ABOVEOF3-ROBOT)
(NOT-ABOVEOF4-ROBOT)
(ABOVEOF2-ROBOT)
(NOT-ABOVEOF3-ROBOT)
(ABOVEOF1-ROBOT)
(NOT-ABOVEOF2-ROBOT)
(NOT-ABOVEOF1-ROBOT)
(not (ABOVEOF10-ROBOT))
)
)
(when
(and
(ABOVEOF9-ROBOT)
)
(and
(ABOVEOF8-ROBOT)
(NOT-ABOVEOF9-ROBOT)
(ABOVEOF7-ROBOT)
(NOT-ABOVEOF8-ROBOT)
(ABOVEOF6-ROBOT)
(NOT-ABOVEOF7-ROBOT)
(ABOVEOF5-ROBOT)
(NOT-ABOVEOF6-ROBOT)
(ABOVEOF4-ROBOT)
(NOT-ABOVEOF5-ROBOT)
(ABOVEOF3-ROBOT)
(NOT-ABOVEOF4-ROBOT)
(ABOVEOF2-ROBOT)
(NOT-ABOVEOF3-ROBOT)
(ABOVEOF1-ROBOT)
(NOT-ABOVEOF2-ROBOT)
(NOT-ABOVEOF1-ROBOT)
(not (ABOVEOF9-ROBOT))
)
)
(when
(and
(ABOVEOF8-ROBOT)
)
(and
(ABOVEOF7-ROBOT)
(NOT-ABOVEOF8-ROBOT)
(ABOVEOF6-ROBOT)
(NOT-ABOVEOF7-ROBOT)
(ABOVEOF5-ROBOT)
(NOT-ABOVEOF6-ROBOT)
(ABOVEOF4-ROBOT)
(NOT-ABOVEOF5-ROBOT)
(ABOVEOF3-ROBOT)
(NOT-ABOVEOF4-ROBOT)
(ABOVEOF2-ROBOT)
(NOT-ABOVEOF3-ROBOT)
(ABOVEOF1-ROBOT)
(NOT-ABOVEOF2-ROBOT)
(NOT-ABOVEOF1-ROBOT)
(not (ABOVEOF8-ROBOT))
)
)
(when
(and
(ABOVEOF7-ROBOT)
)
(and
(ABOVEOF6-ROBOT)
(NOT-ABOVEOF7-ROBOT)
(ABOVEOF5-ROBOT)
(NOT-ABOVEOF6-ROBOT)
(ABOVEOF4-ROBOT)
(NOT-ABOVEOF5-ROBOT)
(ABOVEOF3-ROBOT)
(NOT-ABOVEOF4-ROBOT)
(ABOVEOF2-ROBOT)
(NOT-ABOVEOF3-ROBOT)
(ABOVEOF1-ROBOT)
(NOT-ABOVEOF2-ROBOT)
(NOT-ABOVEOF1-ROBOT)
(not (ABOVEOF7-ROBOT))
)
)
(when
(and
(ABOVEOF6-ROBOT)
)
(and
(ABOVEOF5-ROBOT)
(NOT-ABOVEOF6-ROBOT)
(ABOVEOF4-ROBOT)
(NOT-ABOVEOF5-ROBOT)
(ABOVEOF3-ROBOT)
(NOT-ABOVEOF4-ROBOT)
(ABOVEOF2-ROBOT)
(NOT-ABOVEOF3-ROBOT)
(ABOVEOF1-ROBOT)
(NOT-ABOVEOF2-ROBOT)
(NOT-ABOVEOF1-ROBOT)
(not (ABOVEOF6-ROBOT))
)
)
(when
(and
(ABOVEOF5-ROBOT)
)
(and
(ABOVEOF4-ROBOT)
(NOT-ABOVEOF5-ROBOT)
(ABOVEOF3-ROBOT)
(NOT-ABOVEOF4-ROBOT)
(ABOVEOF2-ROBOT)
(NOT-ABOVEOF3-ROBOT)
(ABOVEOF1-ROBOT)
(NOT-ABOVEOF2-ROBOT)
(NOT-ABOVEOF1-ROBOT)
(not (ABOVEOF5-ROBOT))
)
)
(when
(and
(ABOVEOF4-ROBOT)
)
(and
(ABOVEOF3-ROBOT)
(NOT-ABOVEOF4-ROBOT)
(ABOVEOF2-ROBOT)
(NOT-ABOVEOF3-ROBOT)
(ABOVEOF1-ROBOT)
(NOT-ABOVEOF2-ROBOT)
(NOT-ABOVEOF1-ROBOT)
(not (ABOVEOF4-ROBOT))
)
)
(when
(and
(ABOVEOF3-ROBOT)
)
(and
(ABOVEOF2-ROBOT)
(NOT-ABOVEOF3-ROBOT)
(ABOVEOF1-ROBOT)
(NOT-ABOVEOF2-ROBOT)
(NOT-ABOVEOF1-ROBOT)
(not (ABOVEOF3-ROBOT))
)
)
(when
(and
(ABOVEOF2-ROBOT)
)
(and
(ABOVEOF1-ROBOT)
(NOT-ABOVEOF2-ROBOT)
(NOT-ABOVEOF1-ROBOT)
(not (ABOVEOF2-ROBOT))
)
)
(when
(and
(ABOVEOF1-ROBOT)
)
(and
(NOT-ABOVEOF1-ROBOT)
(not (ABOVEOF1-ROBOT))
)
)
(when
(and
(Flag430-)
)
(and
(BELOWOF16-ROBOT)
(not (NOT-BELOWOF16-ROBOT))
)
)
(when
(and
(BELOWOF14-ROBOT)
)
(and
(BELOWOF15-ROBOT)
(BELOWOF13-ROBOT)
(not (NOT-BELOWOF15-ROBOT))
(not (NOT-BELOWOF14-ROBOT))
(not (NOT-BELOWOF13-ROBOT))
)
)
(when
(and
(BELOWOF8-ROBOT)
)
(and
(BELOWOF15-ROBOT)
(BELOWOF14-ROBOT)
(BELOWOF13-ROBOT)
(BELOWOF12-ROBOT)
(BELOWOF11-ROBOT)
(BELOWOF10-ROBOT)
(BELOWOF9-ROBOT)
(BELOWOF7-ROBOT)
(not (NOT-BELOWOF15-ROBOT))
(not (NOT-BELOWOF14-ROBOT))
(not (NOT-BELOWOF13-ROBOT))
(not (NOT-BELOWOF12-ROBOT))
(not (NOT-BELOWOF11-ROBOT))
(not (NOT-BELOWOF10-ROBOT))
(not (NOT-BELOWOF9-ROBOT))
(not (NOT-BELOWOF8-ROBOT))
(not (NOT-BELOWOF7-ROBOT))
)
)
(when
(and
(BELOWOF13-ROBOT)
)
(and
(BELOWOF15-ROBOT)
(BELOWOF14-ROBOT)
(BELOWOF12-ROBOT)
(not (NOT-BELOWOF15-ROBOT))
(not (NOT-BELOWOF14-ROBOT))
(not (NOT-BELOWOF13-ROBOT))
(not (NOT-BELOWOF12-ROBOT))
)
)
(when
(and
(BELOWOF3-ROBOT)
)
(and
(BELOWOF15-ROBOT)
(BELOWOF14-ROBOT)
(BELOWOF13-ROBOT)
(BELOWOF12-ROBOT)
(BELOWOF11-ROBOT)
(BELOWOF10-ROBOT)
(BELOWOF9-ROBOT)
(BELOWOF8-ROBOT)
(BELOWOF7-ROBOT)
(BELOWOF6-ROBOT)
(BELOWOF5-ROBOT)
(BELOWOF4-ROBOT)
(BELOWOF2-ROBOT)
(not (NOT-BELOWOF15-ROBOT))
(not (NOT-BELOWOF14-ROBOT))
(not (NOT-BELOWOF13-ROBOT))
(not (NOT-BELOWOF12-ROBOT))
(not (NOT-BELOWOF11-ROBOT))
(not (NOT-BELOWOF10-ROBOT))
(not (NOT-BELOWOF9-ROBOT))
(not (NOT-BELOWOF8-ROBOT))
(not (NOT-BELOWOF7-ROBOT))
(not (NOT-BELOWOF6-ROBOT))
(not (NOT-BELOWOF5-ROBOT))
(not (NOT-BELOWOF4-ROBOT))
(not (NOT-BELOWOF3-ROBOT))
(not (NOT-BELOWOF2-ROBOT))
)
)
(when
(and
(BELOWOF9-ROBOT)
)
(and
(BELOWOF15-ROBOT)
(BELOWOF14-ROBOT)
(BELOWOF13-ROBOT)
(BELOWOF12-ROBOT)
(BELOWOF11-ROBOT)
(BELOWOF10-ROBOT)
(BELOWOF8-ROBOT)
(not (NOT-BELOWOF15-ROBOT))
(not (NOT-BELOWOF14-ROBOT))
(not (NOT-BELOWOF13-ROBOT))
(not (NOT-BELOWOF12-ROBOT))
(not (NOT-BELOWOF11-ROBOT))
(not (NOT-BELOWOF10-ROBOT))
(not (NOT-BELOWOF9-ROBOT))
(not (NOT-BELOWOF8-ROBOT))
)
)
(when
(and
(BELOWOF7-ROBOT)
)
(and
(BELOWOF15-ROBOT)
(BELOWOF14-ROBOT)
(BELOWOF13-ROBOT)
(BELOWOF12-ROBOT)
(BELOWOF11-ROBOT)
(BELOWOF10-ROBOT)
(BELOWOF9-ROBOT)
(BELOWOF8-ROBOT)
(BELOWOF6-ROBOT)
(not (NOT-BELOWOF15-ROBOT))
(not (NOT-BELOWOF14-ROBOT))
(not (NOT-BELOWOF13-ROBOT))
(not (NOT-BELOWOF12-ROBOT))
(not (NOT-BELOWOF11-ROBOT))
(not (NOT-BELOWOF10-ROBOT))
(not (NOT-BELOWOF9-ROBOT))
(not (NOT-BELOWOF8-ROBOT))
(not (NOT-BELOWOF7-ROBOT))
(not (NOT-BELOWOF6-ROBOT))
)
)
(when
(and
(BELOWOF12-ROBOT)
)
(and
(BELOWOF15-ROBOT)
(BELOWOF14-ROBOT)
(BELOWOF13-ROBOT)
(BELOWOF11-ROBOT)
(not (NOT-BELOWOF15-ROBOT))
(not (NOT-BELOWOF14-ROBOT))
(not (NOT-BELOWOF13-ROBOT))
(not (NOT-BELOWOF12-ROBOT))
(not (NOT-BELOWOF11-ROBOT))
)
)
(when
(and
(BELOWOF4-ROBOT)
)
(and
(BELOWOF15-ROBOT)
(BELOWOF14-ROBOT)
(BELOWOF13-ROBOT)
(BELOWOF12-ROBOT)
(BELOWOF11-ROBOT)
(BELOWOF10-ROBOT)
(BELOWOF9-ROBOT)
(BELOWOF8-ROBOT)
(BELOWOF7-ROBOT)
(BELOWOF6-ROBOT)
(BELOWOF5-ROBOT)
(BELOWOF3-ROBOT)
(not (NOT-BELOWOF15-ROBOT))
(not (NOT-BELOWOF14-ROBOT))
(not (NOT-BELOWOF13-ROBOT))
(not (NOT-BELOWOF12-ROBOT))
(not (NOT-BELOWOF11-ROBOT))
(not (NOT-BELOWOF10-ROBOT))
(not (NOT-BELOWOF9-ROBOT))
(not (NOT-BELOWOF8-ROBOT))
(not (NOT-BELOWOF7-ROBOT))
(not (NOT-BELOWOF6-ROBOT))
(not (NOT-BELOWOF5-ROBOT))
(not (NOT-BELOWOF4-ROBOT))
(not (NOT-BELOWOF3-ROBOT))
)
)
(when
(and
(BELOWOF1-ROBOT)
)
(and
(BELOWOF15-ROBOT)
(BELOWOF14-ROBOT)
(BELOWOF13-ROBOT)
(BELOWOF12-ROBOT)
(BELOWOF11-ROBOT)
(BELOWOF10-ROBOT)
(BELOWOF9-ROBOT)
(BELOWOF8-ROBOT)
(BELOWOF7-ROBOT)
(BELOWOF6-ROBOT)
(BELOWOF5-ROBOT)
(BELOWOF4-ROBOT)
(BELOWOF3-ROBOT)
(BELOWOF2-ROBOT)
(not (NOT-BELOWOF15-ROBOT))
(not (NOT-BELOWOF14-ROBOT))
(not (NOT-BELOWOF13-ROBOT))
(not (NOT-BELOWOF12-ROBOT))
(not (NOT-BELOWOF11-ROBOT))
(not (NOT-BELOWOF10-ROBOT))
(not (NOT-BELOWOF9-ROBOT))
(not (NOT-BELOWOF8-ROBOT))
(not (NOT-BELOWOF7-ROBOT))
(not (NOT-BELOWOF6-ROBOT))
(not (NOT-BELOWOF5-ROBOT))
(not (NOT-BELOWOF4-ROBOT))
(not (NOT-BELOWOF3-ROBOT))
(not (NOT-BELOWOF2-ROBOT))
(not (NOT-BELOWOF1-ROBOT))
)
)
(when
(and
(BELOWOF2-ROBOT)
)
(and
(BELOWOF15-ROBOT)
(BELOWOF14-ROBOT)
(BELOWOF13-ROBOT)
(BELOWOF12-ROBOT)
(BELOWOF11-ROBOT)
(BELOWOF10-ROBOT)
(BELOWOF9-ROBOT)
(BELOWOF8-ROBOT)
(BELOWOF7-ROBOT)
(BELOWOF6-ROBOT)
(BELOWOF5-ROBOT)
(BELOWOF4-ROBOT)
(BELOWOF3-ROBOT)
(BELOWOF1-ROBOT)
(not (NOT-BELOWOF15-ROBOT))
(not (NOT-BELOWOF14-ROBOT))
(not (NOT-BELOWOF13-ROBOT))
(not (NOT-BELOWOF12-ROBOT))
(not (NOT-BELOWOF11-ROBOT))
(not (NOT-BELOWOF10-ROBOT))
(not (NOT-BELOWOF9-ROBOT))
(not (NOT-BELOWOF8-ROBOT))
(not (NOT-BELOWOF7-ROBOT))
(not (NOT-BELOWOF6-ROBOT))
(not (NOT-BELOWOF5-ROBOT))
(not (NOT-BELOWOF4-ROBOT))
(not (NOT-BELOWOF3-ROBOT))
(not (NOT-BELOWOF2-ROBOT))
(not (NOT-BELOWOF1-ROBOT))
)
)
(when
(and
(BELOWOF5-ROBOT)
)
(and
(BELOWOF15-ROBOT)
(BELOWOF14-ROBOT)
(BELOWOF13-ROBOT)
(BELOWOF12-ROBOT)
(BELOWOF11-ROBOT)
(BELOWOF10-ROBOT)
(BELOWOF9-ROBOT)
(BELOWOF8-ROBOT)
(BELOWOF7-ROBOT)
(BELOWOF6-ROBOT)
(BELOWOF4-ROBOT)
(not (NOT-BELOWOF15-ROBOT))
(not (NOT-BELOWOF14-ROBOT))
(not (NOT-BELOWOF13-ROBOT))
(not (NOT-BELOWOF12-ROBOT))
(not (NOT-BELOWOF11-ROBOT))
(not (NOT-BELOWOF10-ROBOT))
(not (NOT-BELOWOF9-ROBOT))
(not (NOT-BELOWOF8-ROBOT))
(not (NOT-BELOWOF7-ROBOT))
(not (NOT-BELOWOF6-ROBOT))
(not (NOT-BELOWOF5-ROBOT))
(not (NOT-BELOWOF4-ROBOT))
)
)
(when
(and
(BELOWOF11-ROBOT)
)
(and
(BELOWOF15-ROBOT)
(BELOWOF14-ROBOT)
(BELOWOF13-ROBOT)
(BELOWOF12-ROBOT)
(BELOWOF10-ROBOT)
(not (NOT-BELOWOF15-ROBOT))
(not (NOT-BELOWOF14-ROBOT))
(not (NOT-BELOWOF13-ROBOT))
(not (NOT-BELOWOF12-ROBOT))
(not (NOT-BELOWOF11-ROBOT))
(not (NOT-BELOWOF10-ROBOT))
)
)
(when
(and
(BELOWOF6-ROBOT)
)
(and
(BELOWOF15-ROBOT)
(BELOWOF14-ROBOT)
(BELOWOF13-ROBOT)
(BELOWOF12-ROBOT)
(BELOWOF11-ROBOT)
(BELOWOF10-ROBOT)
(BELOWOF9-ROBOT)
(BELOWOF8-ROBOT)
(BELOWOF7-ROBOT)
(BELOWOF5-ROBOT)
(not (NOT-BELOWOF15-ROBOT))
(not (NOT-BELOWOF14-ROBOT))
(not (NOT-BELOWOF13-ROBOT))
(not (NOT-BELOWOF12-ROBOT))
(not (NOT-BELOWOF11-ROBOT))
(not (NOT-BELOWOF10-ROBOT))
(not (NOT-BELOWOF9-ROBOT))
(not (NOT-BELOWOF8-ROBOT))
(not (NOT-BELOWOF7-ROBOT))
(not (NOT-BELOWOF6-ROBOT))
(not (NOT-BELOWOF5-ROBOT))
)
)
(when
(and
(BELOWOF15-ROBOT)
)
(and
(BELOWOF14-ROBOT)
(not (NOT-BELOWOF15-ROBOT))
(not (NOT-BELOWOF14-ROBOT))
)
)
(when
(and
(BELOWOF16-ROBOT)
)
(and
(BELOWOF15-ROBOT)
(not (NOT-BELOWOF15-ROBOT))
)
)
(when
(and
(BELOWOF10-ROBOT)
)
(and
(BELOWOF15-ROBOT)
(BELOWOF14-ROBOT)
(BELOWOF13-ROBOT)
(BELOWOF12-ROBOT)
(BELOWOF11-ROBOT)
(BELOWOF9-ROBOT)
(not (NOT-BELOWOF15-ROBOT))
(not (NOT-BELOWOF14-ROBOT))
(not (NOT-BELOWOF13-ROBOT))
(not (NOT-BELOWOF12-ROBOT))
(not (NOT-BELOWOF11-ROBOT))
(not (NOT-BELOWOF10-ROBOT))
(not (NOT-BELOWOF9-ROBOT))
)
)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action MOVEUP-ROBOT
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
)
:effect
(and
(when
(and
(Flag426-)
)
(and
(ROW16-ROBOT)
(not (NOT-ROW16-ROBOT))
)
)
(when
(and
(Flag423-)
)
(and
(ROW15-ROBOT)
(not (NOT-ROW15-ROBOT))
)
)
(when
(and
(Flag419-)
)
(and
(ROW14-ROBOT)
(not (NOT-ROW14-ROBOT))
)
)
(when
(and
(Flag413-)
)
(and
(ROW13-ROBOT)
(not (NOT-ROW13-ROBOT))
)
)
(when
(and
(Flag406-)
)
(and
(ROW12-ROBOT)
(not (NOT-ROW12-ROBOT))
)
)
(when
(and
(Flag399-)
)
(and
(ROW11-ROBOT)
(not (NOT-ROW11-ROBOT))
)
)
(when
(and
(Flag391-)
)
(and
(ROW10-ROBOT)
(not (NOT-ROW10-ROBOT))
)
)
(when
(and
(Flag381-)
)
(and
(ROW9-ROBOT)
(not (NOT-ROW9-ROBOT))
)
)
(when
(and
(Flag369-)
)
(and
(ROW8-ROBOT)
(not (NOT-ROW8-ROBOT))
)
)
(when
(and
(Flag357-)
)
(and
(ROW7-ROBOT)
(not (NOT-ROW7-ROBOT))
)
)
(when
(and
(Flag343-)
)
(and
(ROW6-ROBOT)
(not (NOT-ROW6-ROBOT))
)
)
(when
(and
(Flag328-)
)
(and
(ROW5-ROBOT)
(not (NOT-ROW5-ROBOT))
)
)
(when
(and
(Flag313-)
)
(and
(ROW4-ROBOT)
(not (NOT-ROW4-ROBOT))
)
)
(when
(and
(Flag296-)
)
(and
(ROW3-ROBOT)
(not (NOT-ROW3-ROBOT))
)
)
(when
(and
(Flag279-)
)
(and
(ROW2-ROBOT)
(not (NOT-ROW2-ROBOT))
)
)
(when
(and
(Flag262-)
)
(and
(ROW1-ROBOT)
(not (NOT-ROW1-ROBOT))
)
)
(when
(and
(ROW15-ROBOT)
)
(and
(ROW16-ROBOT)
(NOT-ROW15-ROBOT)
(not (NOT-ROW16-ROBOT))
(not (ROW15-ROBOT))
)
)
(when
(and
(ROW14-ROBOT)
)
(and
(ROW15-ROBOT)
(NOT-ROW14-ROBOT)
(not (NOT-ROW15-ROBOT))
(not (ROW14-ROBOT))
)
)
(when
(and
(ROW13-ROBOT)
)
(and
(ROW14-ROBOT)
(NOT-ROW13-ROBOT)
(not (NOT-ROW14-ROBOT))
(not (ROW13-ROBOT))
)
)
(when
(and
(ROW12-ROBOT)
)
(and
(ROW13-ROBOT)
(NOT-ROW12-ROBOT)
(not (NOT-ROW13-ROBOT))
(not (ROW12-ROBOT))
)
)
(when
(and
(ROW11-ROBOT)
)
(and
(ROW12-ROBOT)
(NOT-ROW11-ROBOT)
(not (NOT-ROW12-ROBOT))
(not (ROW11-ROBOT))
)
)
(when
(and
(ROW10-ROBOT)
)
(and
(ROW11-ROBOT)
(NOT-ROW10-ROBOT)
(not (NOT-ROW11-ROBOT))
(not (ROW10-ROBOT))
)
)
(when
(and
(ROW9-ROBOT)
)
(and
(ROW10-ROBOT)
(NOT-ROW9-ROBOT)
(not (NOT-ROW10-ROBOT))
(not (ROW9-ROBOT))
)
)
(when
(and
(ROW8-ROBOT)
)
(and
(ROW9-ROBOT)
(NOT-ROW8-ROBOT)
(not (NOT-ROW9-ROBOT))
(not (ROW8-ROBOT))
)
)
(when
(and
(ROW7-ROBOT)
)
(and
(ROW8-ROBOT)
(NOT-ROW7-ROBOT)
(not (NOT-ROW8-ROBOT))
(not (ROW7-ROBOT))
)
)
(when
(and
(ROW6-ROBOT)
)
(and
(ROW7-ROBOT)
(NOT-ROW6-ROBOT)
(not (NOT-ROW7-ROBOT))
(not (ROW6-ROBOT))
)
)
(when
(and
(ROW5-ROBOT)
)
(and
(ROW6-ROBOT)
(NOT-ROW5-ROBOT)
(not (NOT-ROW6-ROBOT))
(not (ROW5-ROBOT))
)
)
(when
(and
(ROW4-ROBOT)
)
(and
(ROW5-ROBOT)
(NOT-ROW4-ROBOT)
(not (NOT-ROW5-ROBOT))
(not (ROW4-ROBOT))
)
)
(when
(and
(ROW3-ROBOT)
)
(and
(ROW4-ROBOT)
(NOT-ROW3-ROBOT)
(not (NOT-ROW4-ROBOT))
(not (ROW3-ROBOT))
)
)
(when
(and
(ROW2-ROBOT)
)
(and
(ROW3-ROBOT)
(NOT-ROW2-ROBOT)
(not (NOT-ROW3-ROBOT))
(not (ROW2-ROBOT))
)
)
(when
(and
(ROW1-ROBOT)
)
(and
(ROW2-ROBOT)
(NOT-ROW1-ROBOT)
(not (NOT-ROW2-ROBOT))
(not (ROW1-ROBOT))
)
)
(when
(and
(ROW0-ROBOT)
)
(and
(ROW1-ROBOT)
(NOT-ROW0-ROBOT)
(not (NOT-ROW1-ROBOT))
(not (ROW0-ROBOT))
)
)
(when
(and
(Flag245-)
)
(and
(BELOWOF17-ROBOT)
(NOT-BELOWOF16-ROBOT)
(not (NOT-BELOWOF17-ROBOT))
(not (BELOWOF16-ROBOT))
)
)
(when
(and
(Flag244-)
)
(and
(BELOWOF16-ROBOT)
(NOT-BELOWOF15-ROBOT)
(not (NOT-BELOWOF16-ROBOT))
(not (BELOWOF15-ROBOT))
)
)
(when
(and
(Flag243-)
)
(and
(BELOWOF15-ROBOT)
(NOT-BELOWOF14-ROBOT)
(not (NOT-BELOWOF15-ROBOT))
(not (BELOWOF14-ROBOT))
)
)
(when
(and
(Flag242-)
)
(and
(BELOWOF14-ROBOT)
(NOT-BELOWOF13-ROBOT)
(not (NOT-BELOWOF14-ROBOT))
(not (BELOWOF13-ROBOT))
)
)
(when
(and
(Flag241-)
)
(and
(BELOWOF13-ROBOT)
(NOT-BELOWOF12-ROBOT)
(not (NOT-BELOWOF13-ROBOT))
(not (BELOWOF12-ROBOT))
)
)
(when
(and
(Flag240-)
)
(and
(BELOWOF12-ROBOT)
(NOT-BELOWOF11-ROBOT)
(not (NOT-BELOWOF12-ROBOT))
(not (BELOWOF11-ROBOT))
)
)
(when
(and
(Flag239-)
)
(and
(BELOWOF11-ROBOT)
(NOT-BELOWOF10-ROBOT)
(not (NOT-BELOWOF11-ROBOT))
(not (BELOWOF10-ROBOT))
)
)
(when
(and
(Flag238-)
)
(and
(BELOWOF10-ROBOT)
(NOT-BELOWOF9-ROBOT)
(not (NOT-BELOWOF10-ROBOT))
(not (BELOWOF9-ROBOT))
)
)
(when
(and
(Flag237-)
)
(and
(BELOWOF9-ROBOT)
(NOT-BELOWOF8-ROBOT)
(not (NOT-BELOWOF9-ROBOT))
(not (BELOWOF8-ROBOT))
)
)
(when
(and
(Flag236-)
)
(and
(BELOWOF8-ROBOT)
(NOT-BELOWOF7-ROBOT)
(not (NOT-BELOWOF8-ROBOT))
(not (BELOWOF7-ROBOT))
)
)
(when
(and
(Flag235-)
)
(and
(BELOWOF7-ROBOT)
(NOT-BELOWOF6-ROBOT)
(not (NOT-BELOWOF7-ROBOT))
(not (BELOWOF6-ROBOT))
)
)
(when
(and
(Flag234-)
)
(and
(BELOWOF6-ROBOT)
(NOT-BELOWOF5-ROBOT)
(not (NOT-BELOWOF6-ROBOT))
(not (BELOWOF5-ROBOT))
)
)
(when
(and
(Flag233-)
)
(and
(BELOWOF5-ROBOT)
(NOT-BELOWOF4-ROBOT)
(not (NOT-BELOWOF5-ROBOT))
(not (BELOWOF4-ROBOT))
)
)
(when
(and
(Flag232-)
)
(and
(BELOWOF4-ROBOT)
(NOT-BELOWOF3-ROBOT)
(not (NOT-BELOWOF4-ROBOT))
(not (BELOWOF3-ROBOT))
)
)
(when
(and
(Flag231-)
)
(and
(BELOWOF3-ROBOT)
(NOT-BELOWOF2-ROBOT)
(not (NOT-BELOWOF3-ROBOT))
(not (BELOWOF2-ROBOT))
)
)
(when
(and
(BELOWOF1-ROBOT)
)
(and
(BELOWOF2-ROBOT)
(NOT-BELOWOF1-ROBOT)
(not (NOT-BELOWOF2-ROBOT))
(not (BELOWOF1-ROBOT))
)
)
(when
(and
(Flag230-)
)
(and
(ABOVEOF16-ROBOT)
(not (NOT-ABOVEOF16-ROBOT))
)
)
(when
(and
(Flag229-)
)
(and
(ABOVEOF15-ROBOT)
(not (NOT-ABOVEOF15-ROBOT))
)
)
(when
(and
(Flag228-)
)
(and
(ABOVEOF14-ROBOT)
(not (NOT-ABOVEOF14-ROBOT))
)
)
(when
(and
(Flag227-)
)
(and
(ABOVEOF13-ROBOT)
(not (NOT-ABOVEOF13-ROBOT))
)
)
(when
(and
(Flag226-)
)
(and
(ABOVEOF12-ROBOT)
(not (NOT-ABOVEOF12-ROBOT))
)
)
(when
(and
(Flag225-)
)
(and
(ABOVEOF11-ROBOT)
(not (NOT-ABOVEOF11-ROBOT))
)
)
(when
(and
(Flag224-)
)
(and
(ABOVEOF10-ROBOT)
(not (NOT-ABOVEOF10-ROBOT))
)
)
(when
(and
(Flag223-)
)
(and
(ABOVEOF9-ROBOT)
(not (NOT-ABOVEOF9-ROBOT))
)
)
(when
(and
(Flag222-)
)
(and
(ABOVEOF8-ROBOT)
(not (NOT-ABOVEOF8-ROBOT))
)
)
(when
(and
(Flag221-)
)
(and
(ABOVEOF7-ROBOT)
(not (NOT-ABOVEOF7-ROBOT))
)
)
(when
(and
(Flag220-)
)
(and
(ABOVEOF6-ROBOT)
(not (NOT-ABOVEOF6-ROBOT))
)
)
(when
(and
(Flag219-)
)
(and
(ABOVEOF5-ROBOT)
(not (NOT-ABOVEOF5-ROBOT))
)
)
(when
(and
(Flag218-)
)
(and
(ABOVEOF4-ROBOT)
(not (NOT-ABOVEOF4-ROBOT))
)
)
(when
(and
(Flag217-)
)
(and
(ABOVEOF3-ROBOT)
(not (NOT-ABOVEOF3-ROBOT))
)
)
(when
(and
(Flag216-)
)
(and
(ABOVEOF2-ROBOT)
(not (NOT-ABOVEOF2-ROBOT))
)
)
(ABOVEOF1-ROBOT)
(CHECKCONSISTENCY-)
(not (NOT-ABOVEOF1-ROBOT))
(not (NOT-CHECKCONSISTENCY-))
)
)
(:derived (Flag2-)(and (NOT-CHECKCONSISTENCY-)(NOT-ERROR-)))

(:derived (Flag3-)(and (RIGHTOF2-ROBOT)))

(:derived (Flag3-)(and (LEFTOF16-ROBOT)))

(:derived (Flag19-)(and (LEFTOF16-ROBOT)))

(:derived (Flag20-)(and (LEFTOF16-ROBOT)))

(:derived (Flag21-)(and (RIGHTOF2-ROBOT)))

(:derived (Flag22-)(and (RIGHTOF2-ROBOT)))

(:derived (Flag245-)(and (BELOWOF16-ROBOT)))

(:derived (Flag427-)(and (RIGHTOF2-ROBOT)))

(:derived (Flag430-)(and (BELOWOF16-ROBOT)))

)
