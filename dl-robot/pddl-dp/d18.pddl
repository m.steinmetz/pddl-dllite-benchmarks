(define (domain grounded-SIMPLE-ADL-ROBOT)
(:requirements
:strips
)
(:predicates
(Flag473-)
(Flag258-)
(Flag257-)
(Flag256-)
(Flag255-)
(Flag21-)
(Flag20-)
(Flag4-)
(Flag2-)
(ROW16-ROBOT)
(ROW15-ROBOT)
(ROW14-ROBOT)
(ROW13-ROBOT)
(ROW12-ROBOT)
(ROW11-ROBOT)
(ROW10-ROBOT)
(ROW9-ROBOT)
(ROW8-ROBOT)
(ROW7-ROBOT)
(ROW6-ROBOT)
(ROW5-ROBOT)
(ROW4-ROBOT)
(ROW3-ROBOT)
(ROW2-ROBOT)
(ROW1-ROBOT)
(ROW0-ROBOT)
(ABOVEOF16-ROBOT)
(ABOVEOF15-ROBOT)
(ABOVEOF14-ROBOT)
(ABOVEOF13-ROBOT)
(ABOVEOF12-ROBOT)
(ABOVEOF11-ROBOT)
(ABOVEOF10-ROBOT)
(ABOVEOF9-ROBOT)
(ABOVEOF8-ROBOT)
(ABOVEOF7-ROBOT)
(ABOVEOF6-ROBOT)
(ABOVEOF5-ROBOT)
(ABOVEOF4-ROBOT)
(ABOVEOF3-ROBOT)
(ABOVEOF2-ROBOT)
(ABOVEOF1-ROBOT)
(BELOWOF16-ROBOT)
(BELOWOF15-ROBOT)
(BELOWOF14-ROBOT)
(BELOWOF13-ROBOT)
(BELOWOF12-ROBOT)
(BELOWOF11-ROBOT)
(BELOWOF10-ROBOT)
(BELOWOF9-ROBOT)
(BELOWOF8-ROBOT)
(BELOWOF7-ROBOT)
(BELOWOF6-ROBOT)
(BELOWOF5-ROBOT)
(BELOWOF4-ROBOT)
(BELOWOF3-ROBOT)
(BELOWOF2-ROBOT)
(BELOWOF1-ROBOT)
(CHECKCONSISTENCY-)
(ROW17-ROBOT)
(BELOWOF18-ROBOT)
(NOT-BELOWOF17-ROBOT)
(ABOVEOF17-ROBOT)
(Flag472-)
(Flag238-)
(Flag236-)
(Flag235-)
(Flag233-)
(Flag232-)
(Flag231-)
(Flag229-)
(Flag228-)
(Flag227-)
(Flag226-)
(Flag224-)
(Flag223-)
(Flag222-)
(Flag221-)
(Flag220-)
(Flag219-)
(Flag217-)
(Flag216-)
(Flag215-)
(Flag214-)
(Flag213-)
(Flag212-)
(Flag211-)
(Flag209-)
(Flag208-)
(Flag207-)
(Flag206-)
(Flag205-)
(Flag204-)
(Flag203-)
(Flag201-)
(Flag200-)
(Flag199-)
(Flag198-)
(Flag197-)
(Flag196-)
(Flag195-)
(Flag194-)
(Flag192-)
(Flag191-)
(Flag190-)
(Flag189-)
(Flag188-)
(Flag187-)
(Flag186-)
(Flag185-)
(Flag184-)
(Flag183-)
(Flag181-)
(Flag180-)
(Flag179-)
(Flag178-)
(Flag177-)
(Flag176-)
(Flag175-)
(Flag174-)
(Flag173-)
(Flag172-)
(Flag171-)
(Flag170-)
(Flag168-)
(Flag167-)
(Flag166-)
(Flag165-)
(Flag164-)
(Flag163-)
(Flag162-)
(Flag161-)
(Flag160-)
(Flag159-)
(Flag158-)
(Flag157-)
(Flag155-)
(Flag154-)
(Flag153-)
(Flag152-)
(Flag151-)
(Flag150-)
(Flag149-)
(Flag148-)
(Flag147-)
(Flag146-)
(Flag145-)
(Flag144-)
(Flag143-)
(Flag142-)
(Flag140-)
(Flag139-)
(Flag138-)
(Flag137-)
(Flag136-)
(Flag135-)
(Flag134-)
(Flag133-)
(Flag132-)
(Flag131-)
(Flag130-)
(Flag129-)
(Flag128-)
(Flag127-)
(Flag126-)
(Flag124-)
(Flag123-)
(Flag122-)
(Flag121-)
(Flag120-)
(Flag119-)
(Flag118-)
(Flag117-)
(Flag116-)
(Flag115-)
(Flag114-)
(Flag113-)
(Flag112-)
(Flag111-)
(Flag110-)
(Flag108-)
(Flag107-)
(Flag106-)
(Flag105-)
(Flag104-)
(Flag103-)
(Flag102-)
(Flag101-)
(Flag100-)
(Flag99-)
(Flag98-)
(Flag97-)
(Flag96-)
(Flag95-)
(Flag94-)
(Flag93-)
(Flag92-)
(Flag90-)
(Flag89-)
(Flag88-)
(Flag87-)
(Flag86-)
(Flag85-)
(Flag84-)
(Flag83-)
(Flag82-)
(Flag81-)
(Flag80-)
(Flag79-)
(Flag78-)
(Flag77-)
(Flag76-)
(Flag75-)
(Flag74-)
(Flag72-)
(Flag71-)
(Flag70-)
(Flag69-)
(Flag68-)
(Flag67-)
(Flag66-)
(Flag65-)
(Flag64-)
(Flag63-)
(Flag62-)
(Flag61-)
(Flag60-)
(Flag59-)
(Flag58-)
(Flag57-)
(Flag56-)
(Flag55-)
(Flag54-)
(Flag53-)
(Flag52-)
(Flag51-)
(Flag50-)
(Flag49-)
(Flag48-)
(Flag47-)
(Flag46-)
(Flag45-)
(Flag44-)
(Flag43-)
(Flag42-)
(Flag41-)
(Flag40-)
(Flag39-)
(Flag38-)
(Flag37-)
(Flag36-)
(Flag35-)
(Flag34-)
(Flag33-)
(Flag32-)
(Flag31-)
(Flag30-)
(Flag29-)
(Flag28-)
(Flag27-)
(Flag26-)
(Flag25-)
(Flag24-)
(Flag23-)
(Flag22-)
(Flag19-)
(Flag18-)
(Flag17-)
(Flag16-)
(Flag15-)
(Flag14-)
(Flag13-)
(Flag12-)
(Flag11-)
(Flag10-)
(Flag9-)
(Flag8-)
(Flag7-)
(Flag6-)
(Flag5-)
(Flag3-)
(ERROR-)
(COLUMN16-ROBOT)
(COLUMN15-ROBOT)
(COLUMN14-ROBOT)
(COLUMN13-ROBOT)
(COLUMN12-ROBOT)
(COLUMN11-ROBOT)
(COLUMN10-ROBOT)
(COLUMN9-ROBOT)
(COLUMN8-ROBOT)
(COLUMN7-ROBOT)
(COLUMN6-ROBOT)
(COLUMN5-ROBOT)
(COLUMN4-ROBOT)
(COLUMN3-ROBOT)
(COLUMN2-ROBOT)
(COLUMN1-ROBOT)
(COLUMN0-ROBOT)
(RIGHTOF16-ROBOT)
(RIGHTOF15-ROBOT)
(RIGHTOF14-ROBOT)
(RIGHTOF13-ROBOT)
(RIGHTOF12-ROBOT)
(RIGHTOF11-ROBOT)
(RIGHTOF10-ROBOT)
(RIGHTOF9-ROBOT)
(RIGHTOF8-ROBOT)
(RIGHTOF7-ROBOT)
(RIGHTOF6-ROBOT)
(RIGHTOF5-ROBOT)
(RIGHTOF4-ROBOT)
(RIGHTOF3-ROBOT)
(RIGHTOF1-ROBOT)
(NOT-RIGHTOF2-ROBOT)
(RIGHTOF0-ROBOT)
(LEFTOF16-ROBOT)
(LEFTOF15-ROBOT)
(LEFTOF14-ROBOT)
(LEFTOF13-ROBOT)
(LEFTOF12-ROBOT)
(LEFTOF11-ROBOT)
(LEFTOF10-ROBOT)
(LEFTOF9-ROBOT)
(LEFTOF8-ROBOT)
(LEFTOF7-ROBOT)
(LEFTOF6-ROBOT)
(LEFTOF5-ROBOT)
(LEFTOF4-ROBOT)
(LEFTOF3-ROBOT)
(LEFTOF2-ROBOT)
(LEFTOF1-ROBOT)
(COLUMN17-ROBOT)
(LEFTOF18-ROBOT)
(NOT-LEFTOF17-ROBOT)
(RIGHTOF17-ROBOT)
(Flag474-)
(Flag470-)
(Flag468-)
(Flag467-)
(Flag465-)
(Flag464-)
(Flag463-)
(Flag462-)
(Flag460-)
(Flag459-)
(Flag458-)
(Flag457-)
(Flag455-)
(Flag454-)
(Flag453-)
(Flag452-)
(Flag451-)
(Flag449-)
(Flag448-)
(Flag447-)
(Flag446-)
(Flag445-)
(Flag444-)
(Flag443-)
(Flag441-)
(Flag440-)
(Flag439-)
(Flag438-)
(Flag437-)
(Flag436-)
(Flag435-)
(Flag433-)
(Flag432-)
(Flag431-)
(Flag430-)
(Flag429-)
(Flag428-)
(Flag427-)
(Flag426-)
(Flag425-)
(Flag423-)
(Flag422-)
(Flag421-)
(Flag420-)
(Flag419-)
(Flag418-)
(Flag417-)
(Flag416-)
(Flag415-)
(Flag413-)
(Flag412-)
(Flag411-)
(Flag410-)
(Flag409-)
(Flag408-)
(Flag407-)
(Flag406-)
(Flag405-)
(Flag404-)
(Flag403-)
(Flag401-)
(Flag400-)
(Flag399-)
(Flag398-)
(Flag397-)
(Flag396-)
(Flag395-)
(Flag394-)
(Flag393-)
(Flag392-)
(Flag391-)
(Flag390-)
(Flag388-)
(Flag387-)
(Flag386-)
(Flag385-)
(Flag384-)
(Flag383-)
(Flag382-)
(Flag381-)
(Flag380-)
(Flag379-)
(Flag378-)
(Flag377-)
(Flag376-)
(Flag375-)
(Flag373-)
(Flag372-)
(Flag371-)
(Flag370-)
(Flag369-)
(Flag368-)
(Flag367-)
(Flag366-)
(Flag365-)
(Flag364-)
(Flag363-)
(Flag362-)
(Flag361-)
(Flag359-)
(Flag358-)
(Flag357-)
(Flag356-)
(Flag355-)
(Flag354-)
(Flag353-)
(Flag352-)
(Flag351-)
(Flag350-)
(Flag349-)
(Flag348-)
(Flag347-)
(Flag346-)
(Flag345-)
(Flag344-)
(Flag342-)
(Flag341-)
(Flag340-)
(Flag339-)
(Flag338-)
(Flag337-)
(Flag336-)
(Flag335-)
(Flag334-)
(Flag333-)
(Flag332-)
(Flag331-)
(Flag330-)
(Flag329-)
(Flag328-)
(Flag327-)
(Flag326-)
(Flag324-)
(Flag323-)
(Flag322-)
(Flag321-)
(Flag320-)
(Flag319-)
(Flag318-)
(Flag317-)
(Flag316-)
(Flag315-)
(Flag314-)
(Flag313-)
(Flag312-)
(Flag311-)
(Flag310-)
(Flag309-)
(Flag307-)
(Flag306-)
(Flag305-)
(Flag304-)
(Flag303-)
(Flag302-)
(Flag301-)
(Flag300-)
(Flag299-)
(Flag298-)
(Flag297-)
(Flag296-)
(Flag295-)
(Flag294-)
(Flag293-)
(Flag292-)
(Flag291-)
(Flag290-)
(Flag289-)
(Flag288-)
(Flag287-)
(Flag286-)
(Flag285-)
(Flag284-)
(Flag283-)
(Flag282-)
(Flag281-)
(Flag280-)
(Flag279-)
(Flag278-)
(Flag277-)
(Flag276-)
(Flag275-)
(Flag274-)
(Flag273-)
(Flag272-)
(Flag271-)
(Flag270-)
(Flag269-)
(Flag268-)
(Flag267-)
(Flag266-)
(Flag265-)
(Flag264-)
(Flag263-)
(Flag262-)
(Flag261-)
(Flag260-)
(Flag259-)
(Flag254-)
(Flag253-)
(Flag252-)
(Flag251-)
(Flag250-)
(Flag249-)
(Flag248-)
(Flag247-)
(Flag246-)
(Flag245-)
(Flag244-)
(Flag243-)
(Flag242-)
(Flag241-)
(Flag240-)
(Flag239-)
(Flag237-)
(Flag234-)
(Flag230-)
(Flag225-)
(Flag218-)
(Flag210-)
(Flag202-)
(Flag193-)
(Flag182-)
(Flag169-)
(Flag156-)
(Flag141-)
(Flag125-)
(Flag109-)
(Flag91-)
(Flag73-)
(Flag1-)
(Flag475-)
(Flag471-)
(Flag469-)
(Flag466-)
(Flag461-)
(Flag456-)
(Flag450-)
(Flag442-)
(Flag434-)
(Flag424-)
(Flag414-)
(Flag402-)
(Flag389-)
(Flag374-)
(Flag360-)
(Flag343-)
(Flag325-)
(Flag308-)
(NOT-COLUMN17-ROBOT)
(NOT-COLUMN16-ROBOT)
(NOT-COLUMN15-ROBOT)
(NOT-COLUMN14-ROBOT)
(NOT-COLUMN13-ROBOT)
(NOT-COLUMN12-ROBOT)
(NOT-COLUMN11-ROBOT)
(NOT-COLUMN10-ROBOT)
(NOT-COLUMN9-ROBOT)
(NOT-COLUMN8-ROBOT)
(NOT-COLUMN7-ROBOT)
(NOT-COLUMN6-ROBOT)
(NOT-COLUMN5-ROBOT)
(NOT-COLUMN4-ROBOT)
(NOT-COLUMN3-ROBOT)
(NOT-COLUMN2-ROBOT)
(NOT-COLUMN1-ROBOT)
(NOT-LEFTOF18-ROBOT)
(LEFTOF17-ROBOT)
(NOT-LEFTOF16-ROBOT)
(NOT-LEFTOF15-ROBOT)
(NOT-LEFTOF14-ROBOT)
(NOT-LEFTOF13-ROBOT)
(NOT-LEFTOF12-ROBOT)
(NOT-LEFTOF11-ROBOT)
(NOT-LEFTOF10-ROBOT)
(NOT-LEFTOF9-ROBOT)
(NOT-LEFTOF8-ROBOT)
(NOT-LEFTOF7-ROBOT)
(NOT-LEFTOF6-ROBOT)
(NOT-LEFTOF5-ROBOT)
(NOT-LEFTOF4-ROBOT)
(NOT-LEFTOF3-ROBOT)
(NOT-LEFTOF2-ROBOT)
(NOT-RIGHTOF17-ROBOT)
(NOT-RIGHTOF16-ROBOT)
(NOT-RIGHTOF15-ROBOT)
(NOT-RIGHTOF14-ROBOT)
(NOT-RIGHTOF13-ROBOT)
(NOT-RIGHTOF12-ROBOT)
(NOT-RIGHTOF11-ROBOT)
(NOT-RIGHTOF10-ROBOT)
(NOT-RIGHTOF9-ROBOT)
(NOT-RIGHTOF8-ROBOT)
(NOT-RIGHTOF7-ROBOT)
(NOT-RIGHTOF6-ROBOT)
(NOT-RIGHTOF5-ROBOT)
(NOT-RIGHTOF4-ROBOT)
(NOT-RIGHTOF3-ROBOT)
(NOT-RIGHTOF1-ROBOT)
(NOT-CHECKCONSISTENCY-)
(NOT-COLUMN0-ROBOT)
(RIGHTOF2-ROBOT)
(NOT-RIGHTOF0-ROBOT)
(NOT-LEFTOF1-ROBOT)
(NOT-ERROR-)
(NOT-ROW17-ROBOT)
(NOT-ROW16-ROBOT)
(NOT-ROW15-ROBOT)
(NOT-ROW14-ROBOT)
(NOT-ROW13-ROBOT)
(NOT-ROW12-ROBOT)
(NOT-ROW11-ROBOT)
(NOT-ROW10-ROBOT)
(NOT-ROW9-ROBOT)
(NOT-ROW8-ROBOT)
(NOT-ROW7-ROBOT)
(NOT-ROW6-ROBOT)
(NOT-ROW5-ROBOT)
(NOT-ROW4-ROBOT)
(NOT-ROW3-ROBOT)
(NOT-ROW2-ROBOT)
(NOT-ROW1-ROBOT)
(NOT-BELOWOF18-ROBOT)
(BELOWOF17-ROBOT)
(NOT-BELOWOF16-ROBOT)
(NOT-BELOWOF15-ROBOT)
(NOT-BELOWOF14-ROBOT)
(NOT-BELOWOF13-ROBOT)
(NOT-BELOWOF12-ROBOT)
(NOT-BELOWOF11-ROBOT)
(NOT-BELOWOF10-ROBOT)
(NOT-BELOWOF9-ROBOT)
(NOT-BELOWOF8-ROBOT)
(NOT-BELOWOF7-ROBOT)
(NOT-BELOWOF6-ROBOT)
(NOT-BELOWOF5-ROBOT)
(NOT-BELOWOF4-ROBOT)
(NOT-BELOWOF3-ROBOT)
(NOT-BELOWOF2-ROBOT)
(NOT-ABOVEOF17-ROBOT)
(NOT-ABOVEOF16-ROBOT)
(NOT-ABOVEOF15-ROBOT)
(NOT-ABOVEOF14-ROBOT)
(NOT-ABOVEOF13-ROBOT)
(NOT-ABOVEOF12-ROBOT)
(NOT-ABOVEOF11-ROBOT)
(NOT-ABOVEOF10-ROBOT)
(NOT-ABOVEOF9-ROBOT)
(NOT-ABOVEOF8-ROBOT)
(NOT-ABOVEOF7-ROBOT)
(NOT-ABOVEOF6-ROBOT)
(NOT-ABOVEOF5-ROBOT)
(NOT-ABOVEOF4-ROBOT)
(NOT-ABOVEOF3-ROBOT)
(NOT-ABOVEOF2-ROBOT)
(NOT-ABOVEOF1-ROBOT)
(NOT-ROW0-ROBOT)
(NOT-BELOWOF1-ROBOT)
)
(:derived (Flag308-)(and (Flag273-)))

(:derived (Flag308-)(and (Flag274-)))

(:derived (Flag308-)(and (Flag275-)))

(:derived (Flag308-)(and (Flag276-)))

(:derived (Flag308-)(and (Flag277-)))

(:derived (Flag308-)(and (Flag278-)))

(:derived (Flag308-)(and (Flag279-)))

(:derived (Flag308-)(and (Flag280-)))

(:derived (Flag308-)(and (Flag281-)))

(:derived (Flag308-)(and (Flag282-)))

(:derived (Flag308-)(and (Flag283-)))

(:derived (Flag308-)(and (Flag284-)))

(:derived (Flag308-)(and (Flag285-)))

(:derived (Flag308-)(and (Flag286-)))

(:derived (Flag308-)(and (Flag287-)))

(:derived (Flag308-)(and (Flag288-)))

(:derived (Flag308-)(and (Flag289-)))

(:derived (Flag308-)(and (Flag290-)))

(:derived (Flag308-)(and (Flag291-)))

(:derived (Flag308-)(and (Flag292-)))

(:derived (Flag308-)(and (Flag293-)))

(:derived (Flag308-)(and (Flag294-)))

(:derived (Flag308-)(and (Flag295-)))

(:derived (Flag308-)(and (Flag296-)))

(:derived (Flag308-)(and (Flag297-)))

(:derived (Flag308-)(and (Flag298-)))

(:derived (Flag308-)(and (Flag299-)))

(:derived (Flag308-)(and (Flag300-)))

(:derived (Flag308-)(and (Flag301-)))

(:derived (Flag308-)(and (Flag302-)))

(:derived (Flag308-)(and (Flag303-)))

(:derived (Flag308-)(and (Flag304-)))

(:derived (Flag308-)(and (Flag305-)))

(:derived (Flag308-)(and (Flag306-)))

(:derived (Flag308-)(and (Flag307-)))

(:derived (Flag325-)(and (Flag273-)))

(:derived (Flag325-)(and (Flag274-)))

(:derived (Flag325-)(and (Flag275-)))

(:derived (Flag325-)(and (Flag309-)))

(:derived (Flag325-)(and (Flag276-)))

(:derived (Flag325-)(and (Flag277-)))

(:derived (Flag325-)(and (Flag310-)))

(:derived (Flag325-)(and (Flag297-)))

(:derived (Flag325-)(and (Flag311-)))

(:derived (Flag325-)(and (Flag278-)))

(:derived (Flag325-)(and (Flag279-)))

(:derived (Flag325-)(and (Flag312-)))

(:derived (Flag325-)(and (Flag281-)))

(:derived (Flag325-)(and (Flag293-)))

(:derived (Flag325-)(and (Flag284-)))

(:derived (Flag325-)(and (Flag285-)))

(:derived (Flag325-)(and (Flag313-)))

(:derived (Flag325-)(and (Flag286-)))

(:derived (Flag325-)(and (Flag287-)))

(:derived (Flag325-)(and (Flag295-)))

(:derived (Flag325-)(and (Flag288-)))

(:derived (Flag325-)(and (Flag314-)))

(:derived (Flag325-)(and (Flag289-)))

(:derived (Flag325-)(and (Flag290-)))

(:derived (Flag325-)(and (Flag291-)))

(:derived (Flag325-)(and (Flag292-)))

(:derived (Flag325-)(and (Flag315-)))

(:derived (Flag325-)(and (Flag316-)))

(:derived (Flag325-)(and (Flag283-)))

(:derived (Flag325-)(and (Flag294-)))

(:derived (Flag325-)(and (Flag317-)))

(:derived (Flag325-)(and (Flag318-)))

(:derived (Flag325-)(and (Flag296-)))

(:derived (Flag325-)(and (Flag319-)))

(:derived (Flag325-)(and (Flag320-)))

(:derived (Flag325-)(and (Flag321-)))

(:derived (Flag325-)(and (Flag322-)))

(:derived (Flag325-)(and (Flag298-)))

(:derived (Flag325-)(and (Flag299-)))

(:derived (Flag325-)(and (Flag300-)))

(:derived (Flag325-)(and (Flag301-)))

(:derived (Flag325-)(and (Flag302-)))

(:derived (Flag325-)(and (Flag303-)))

(:derived (Flag325-)(and (Flag304-)))

(:derived (Flag325-)(and (Flag305-)))

(:derived (Flag325-)(and (Flag306-)))

(:derived (Flag325-)(and (Flag323-)))

(:derived (Flag325-)(and (Flag307-)))

(:derived (Flag325-)(and (Flag324-)))

(:derived (Flag343-)(and (Flag273-)))

(:derived (Flag343-)(and (Flag274-)))

(:derived (Flag343-)(and (Flag275-)))

(:derived (Flag343-)(and (Flag309-)))

(:derived (Flag343-)(and (Flag276-)))

(:derived (Flag343-)(and (Flag277-)))

(:derived (Flag343-)(and (Flag310-)))

(:derived (Flag343-)(and (Flag304-)))

(:derived (Flag343-)(and (Flag326-)))

(:derived (Flag343-)(and (Flag311-)))

(:derived (Flag343-)(and (Flag278-)))

(:derived (Flag343-)(and (Flag279-)))

(:derived (Flag343-)(and (Flag327-)))

(:derived (Flag343-)(and (Flag328-)))

(:derived (Flag343-)(and (Flag312-)))

(:derived (Flag343-)(and (Flag329-)))

(:derived (Flag343-)(and (Flag281-)))

(:derived (Flag343-)(and (Flag316-)))

(:derived (Flag343-)(and (Flag293-)))

(:derived (Flag343-)(and (Flag284-)))

(:derived (Flag343-)(and (Flag285-)))

(:derived (Flag343-)(and (Flag330-)))

(:derived (Flag343-)(and (Flag313-)))

(:derived (Flag343-)(and (Flag286-)))

(:derived (Flag343-)(and (Flag287-)))

(:derived (Flag343-)(and (Flag295-)))

(:derived (Flag343-)(and (Flag288-)))

(:derived (Flag343-)(and (Flag314-)))

(:derived (Flag343-)(and (Flag289-)))

(:derived (Flag343-)(and (Flag290-)))

(:derived (Flag343-)(and (Flag331-)))

(:derived (Flag343-)(and (Flag291-)))

(:derived (Flag343-)(and (Flag292-)))

(:derived (Flag343-)(and (Flag315-)))

(:derived (Flag343-)(and (Flag332-)))

(:derived (Flag343-)(and (Flag283-)))

(:derived (Flag343-)(and (Flag294-)))

(:derived (Flag343-)(and (Flag333-)))

(:derived (Flag343-)(and (Flag305-)))

(:derived (Flag343-)(and (Flag317-)))

(:derived (Flag343-)(and (Flag318-)))

(:derived (Flag343-)(and (Flag296-)))

(:derived (Flag343-)(and (Flag297-)))

(:derived (Flag343-)(and (Flag320-)))

(:derived (Flag343-)(and (Flag334-)))

(:derived (Flag343-)(and (Flag335-)))

(:derived (Flag343-)(and (Flag321-)))

(:derived (Flag343-)(and (Flag322-)))

(:derived (Flag343-)(and (Flag336-)))

(:derived (Flag343-)(and (Flag337-)))

(:derived (Flag343-)(and (Flag298-)))

(:derived (Flag343-)(and (Flag299-)))

(:derived (Flag343-)(and (Flag338-)))

(:derived (Flag343-)(and (Flag339-)))

(:derived (Flag343-)(and (Flag300-)))

(:derived (Flag343-)(and (Flag340-)))

(:derived (Flag343-)(and (Flag303-)))

(:derived (Flag343-)(and (Flag341-)))

(:derived (Flag343-)(and (Flag342-)))

(:derived (Flag343-)(and (Flag306-)))

(:derived (Flag343-)(and (Flag323-)))

(:derived (Flag343-)(and (Flag307-)))

(:derived (Flag360-)(and (Flag344-)))

(:derived (Flag360-)(and (Flag345-)))

(:derived (Flag360-)(and (Flag274-)))

(:derived (Flag360-)(and (Flag346-)))

(:derived (Flag360-)(and (Flag275-)))

(:derived (Flag360-)(and (Flag309-)))

(:derived (Flag360-)(and (Flag276-)))

(:derived (Flag360-)(and (Flag310-)))

(:derived (Flag360-)(and (Flag304-)))

(:derived (Flag360-)(and (Flag326-)))

(:derived (Flag360-)(and (Flag347-)))

(:derived (Flag360-)(and (Flag311-)))

(:derived (Flag360-)(and (Flag278-)))

(:derived (Flag360-)(and (Flag279-)))

(:derived (Flag360-)(and (Flag296-)))

(:derived (Flag360-)(and (Flag348-)))

(:derived (Flag360-)(and (Flag327-)))

(:derived (Flag360-)(and (Flag328-)))

(:derived (Flag360-)(and (Flag349-)))

(:derived (Flag360-)(and (Flag312-)))

(:derived (Flag360-)(and (Flag273-)))

(:derived (Flag360-)(and (Flag329-)))

(:derived (Flag360-)(and (Flag281-)))

(:derived (Flag360-)(and (Flag316-)))

(:derived (Flag360-)(and (Flag293-)))

(:derived (Flag360-)(and (Flag284-)))

(:derived (Flag360-)(and (Flag285-)))

(:derived (Flag360-)(and (Flag330-)))

(:derived (Flag360-)(and (Flag313-)))

(:derived (Flag360-)(and (Flag286-)))

(:derived (Flag360-)(and (Flag331-)))

(:derived (Flag360-)(and (Flag288-)))

(:derived (Flag360-)(and (Flag318-)))

(:derived (Flag360-)(and (Flag350-)))

(:derived (Flag360-)(and (Flag351-)))

(:derived (Flag360-)(and (Flag352-)))

(:derived (Flag360-)(and (Flag290-)))

(:derived (Flag360-)(and (Flag353-)))

(:derived (Flag360-)(and (Flag291-)))

(:derived (Flag360-)(and (Flag292-)))

(:derived (Flag360-)(and (Flag315-)))

(:derived (Flag360-)(and (Flag354-)))

(:derived (Flag360-)(and (Flag283-)))

(:derived (Flag360-)(and (Flag294-)))

(:derived (Flag360-)(and (Flag333-)))

(:derived (Flag360-)(and (Flag355-)))

(:derived (Flag360-)(and (Flag356-)))

(:derived (Flag360-)(and (Flag305-)))

(:derived (Flag360-)(and (Flag295-)))

(:derived (Flag360-)(and (Flag314-)))

(:derived (Flag360-)(and (Flag289-)))

(:derived (Flag360-)(and (Flag297-)))

(:derived (Flag360-)(and (Flag320-)))

(:derived (Flag360-)(and (Flag334-)))

(:derived (Flag360-)(and (Flag335-)))

(:derived (Flag360-)(and (Flag321-)))

(:derived (Flag360-)(and (Flag322-)))

(:derived (Flag360-)(and (Flag336-)))

(:derived (Flag360-)(and (Flag337-)))

(:derived (Flag360-)(and (Flag298-)))

(:derived (Flag360-)(and (Flag299-)))

(:derived (Flag360-)(and (Flag338-)))

(:derived (Flag360-)(and (Flag339-)))

(:derived (Flag360-)(and (Flag300-)))

(:derived (Flag360-)(and (Flag340-)))

(:derived (Flag360-)(and (Flag357-)))

(:derived (Flag360-)(and (Flag317-)))

(:derived (Flag360-)(and (Flag303-)))

(:derived (Flag360-)(and (Flag358-)))

(:derived (Flag360-)(and (Flag342-)))

(:derived (Flag360-)(and (Flag306-)))

(:derived (Flag360-)(and (Flag323-)))

(:derived (Flag360-)(and (Flag307-)))

(:derived (Flag360-)(and (Flag359-)))

(:derived (Flag374-)(and (Flag299-)))

(:derived (Flag374-)(and (Flag345-)))

(:derived (Flag374-)(and (Flag274-)))

(:derived (Flag374-)(and (Flag344-)))

(:derived (Flag374-)(and (Flag275-)))

(:derived (Flag374-)(and (Flag347-)))

(:derived (Flag374-)(and (Flag276-)))

(:derived (Flag374-)(and (Flag310-)))

(:derived (Flag374-)(and (Flag304-)))

(:derived (Flag374-)(and (Flag361-)))

(:derived (Flag374-)(and (Flag326-)))

(:derived (Flag374-)(and (Flag327-)))

(:derived (Flag374-)(and (Flag323-)))

(:derived (Flag374-)(and (Flag278-)))

(:derived (Flag374-)(and (Flag279-)))

(:derived (Flag374-)(and (Flag349-)))

(:derived (Flag374-)(and (Flag312-)))

(:derived (Flag374-)(and (Flag273-)))

(:derived (Flag374-)(and (Flag329-)))

(:derived (Flag374-)(and (Flag281-)))

(:derived (Flag374-)(and (Flag350-)))

(:derived (Flag374-)(and (Flag316-)))

(:derived (Flag374-)(and (Flag362-)))

(:derived (Flag374-)(and (Flag283-)))

(:derived (Flag374-)(and (Flag284-)))

(:derived (Flag374-)(and (Flag340-)))

(:derived (Flag374-)(and (Flag363-)))

(:derived (Flag374-)(and (Flag330-)))

(:derived (Flag374-)(and (Flag313-)))

(:derived (Flag374-)(and (Flag286-)))

(:derived (Flag374-)(and (Flag331-)))

(:derived (Flag374-)(and (Flag364-)))

(:derived (Flag374-)(and (Flag288-)))

(:derived (Flag374-)(and (Flag314-)))

(:derived (Flag374-)(and (Flag365-)))

(:derived (Flag374-)(and (Flag351-)))

(:derived (Flag374-)(and (Flag366-)))

(:derived (Flag374-)(and (Flag309-)))

(:derived (Flag374-)(and (Flag285-)))

(:derived (Flag374-)(and (Flag352-)))

(:derived (Flag374-)(and (Flag353-)))

(:derived (Flag374-)(and (Flag291-)))

(:derived (Flag374-)(and (Flag292-)))

(:derived (Flag374-)(and (Flag315-)))

(:derived (Flag374-)(and (Flag354-)))

(:derived (Flag374-)(and (Flag293-)))

(:derived (Flag374-)(and (Flag294-)))

(:derived (Flag374-)(and (Flag333-)))

(:derived (Flag374-)(and (Flag355-)))

(:derived (Flag374-)(and (Flag356-)))

(:derived (Flag374-)(and (Flag289-)))

(:derived (Flag374-)(and (Flag305-)))

(:derived (Flag374-)(and (Flag317-)))

(:derived (Flag374-)(and (Flag367-)))

(:derived (Flag374-)(and (Flag318-)))

(:derived (Flag374-)(and (Flag296-)))

(:derived (Flag374-)(and (Flag297-)))

(:derived (Flag374-)(and (Flag320-)))

(:derived (Flag374-)(and (Flag334-)))

(:derived (Flag374-)(and (Flag335-)))

(:derived (Flag374-)(and (Flag368-)))

(:derived (Flag374-)(and (Flag321-)))

(:derived (Flag374-)(and (Flag369-)))

(:derived (Flag374-)(and (Flag322-)))

(:derived (Flag374-)(and (Flag370-)))

(:derived (Flag374-)(and (Flag336-)))

(:derived (Flag374-)(and (Flag337-)))

(:derived (Flag374-)(and (Flag298-)))

(:derived (Flag374-)(and (Flag371-)))

(:derived (Flag374-)(and (Flag338-)))

(:derived (Flag374-)(and (Flag339-)))

(:derived (Flag374-)(and (Flag300-)))

(:derived (Flag374-)(and (Flag372-)))

(:derived (Flag374-)(and (Flag357-)))

(:derived (Flag374-)(and (Flag303-)))

(:derived (Flag374-)(and (Flag358-)))

(:derived (Flag374-)(and (Flag342-)))

(:derived (Flag374-)(and (Flag306-)))

(:derived (Flag374-)(and (Flag348-)))

(:derived (Flag374-)(and (Flag373-)))

(:derived (Flag374-)(and (Flag359-)))

(:derived (Flag389-)(and (Flag278-)))

(:derived (Flag389-)(and (Flag375-)))

(:derived (Flag389-)(and (Flag362-)))

(:derived (Flag389-)(and (Flag376-)))

(:derived (Flag389-)(and (Flag365-)))

(:derived (Flag389-)(and (Flag351-)))

(:derived (Flag389-)(and (Flag352-)))

(:derived (Flag389-)(and (Flag353-)))

(:derived (Flag389-)(and (Flag315-)))

(:derived (Flag389-)(and (Flag316-)))

(:derived (Flag389-)(and (Flag294-)))

(:derived (Flag389-)(and (Flag275-)))

(:derived (Flag389-)(and (Flag377-)))

(:derived (Flag389-)(and (Flag378-)))

(:derived (Flag389-)(and (Flag322-)))

(:derived (Flag389-)(and (Flag300-)))

(:derived (Flag389-)(and (Flag303-)))

(:derived (Flag389-)(and (Flag372-)))

(:derived (Flag389-)(and (Flag274-)))

(:derived (Flag389-)(and (Flag326-)))

(:derived (Flag389-)(and (Flag279-)))

(:derived (Flag389-)(and (Flag349-)))

(:derived (Flag389-)(and (Flag368-)))

(:derived (Flag389-)(and (Flag283-)))

(:derived (Flag389-)(and (Flag363-)))

(:derived (Flag389-)(and (Flag313-)))

(:derived (Flag389-)(and (Flag331-)))

(:derived (Flag389-)(and (Flag288-)))

(:derived (Flag389-)(and (Flag318-)))

(:derived (Flag389-)(and (Flag350-)))

(:derived (Flag389-)(and (Flag366-)))

(:derived (Flag389-)(and (Flag292-)))

(:derived (Flag389-)(and (Flag293-)))

(:derived (Flag389-)(and (Flag379-)))

(:derived (Flag389-)(and (Flag355-)))

(:derived (Flag389-)(and (Flag330-)))

(:derived (Flag389-)(and (Flag380-)))

(:derived (Flag389-)(and (Flag381-)))

(:derived (Flag389-)(and (Flag369-)))

(:derived (Flag389-)(and (Flag340-)))

(:derived (Flag389-)(and (Flag304-)))

(:derived (Flag389-)(and (Flag305-)))

(:derived (Flag389-)(and (Flag323-)))

(:derived (Flag389-)(and (Flag382-)))

(:derived (Flag389-)(and (Flag344-)))

(:derived (Flag389-)(and (Flag345-)))

(:derived (Flag389-)(and (Flag276-)))

(:derived (Flag389-)(and (Flag383-)))

(:derived (Flag389-)(and (Flag310-)))

(:derived (Flag389-)(and (Flag384-)))

(:derived (Flag389-)(and (Flag329-)))

(:derived (Flag389-)(and (Flag337-)))

(:derived (Flag389-)(and (Flag357-)))

(:derived (Flag389-)(and (Flag385-)))

(:derived (Flag389-)(and (Flag347-)))

(:derived (Flag389-)(and (Flag314-)))

(:derived (Flag389-)(and (Flag291-)))

(:derived (Flag389-)(and (Flag289-)))

(:derived (Flag389-)(and (Flag297-)))

(:derived (Flag389-)(and (Flag335-)))

(:derived (Flag389-)(and (Flag298-)))

(:derived (Flag389-)(and (Flag371-)))

(:derived (Flag389-)(and (Flag338-)))

(:derived (Flag389-)(and (Flag306-)))

(:derived (Flag389-)(and (Flag348-)))

(:derived (Flag389-)(and (Flag373-)))

(:derived (Flag389-)(and (Flag359-)))

(:derived (Flag389-)(and (Flag299-)))

(:derived (Flag389-)(and (Flag361-)))

(:derived (Flag389-)(and (Flag309-)))

(:derived (Flag389-)(and (Flag327-)))

(:derived (Flag389-)(and (Flag354-)))

(:derived (Flag389-)(and (Flag284-)))

(:derived (Flag389-)(and (Flag364-)))

(:derived (Flag389-)(and (Flag286-)))

(:derived (Flag389-)(and (Flag312-)))

(:derived (Flag389-)(and (Flag333-)))

(:derived (Flag389-)(and (Flag386-)))

(:derived (Flag389-)(and (Flag356-)))

(:derived (Flag389-)(and (Flag387-)))

(:derived (Flag389-)(and (Flag296-)))

(:derived (Flag389-)(and (Flag320-)))

(:derived (Flag389-)(and (Flag334-)))

(:derived (Flag389-)(and (Flag321-)))

(:derived (Flag389-)(and (Flag336-)))

(:derived (Flag389-)(and (Flag273-)))

(:derived (Flag389-)(and (Flag388-)))

(:derived (Flag389-)(and (Flag342-)))

(:derived (Flag402-)(and (Flag278-)))

(:derived (Flag402-)(and (Flag362-)))

(:derived (Flag402-)(and (Flag376-)))

(:derived (Flag402-)(and (Flag276-)))

(:derived (Flag402-)(and (Flag365-)))

(:derived (Flag402-)(and (Flag351-)))

(:derived (Flag402-)(and (Flag390-)))

(:derived (Flag402-)(and (Flag353-)))

(:derived (Flag402-)(and (Flag315-)))

(:derived (Flag402-)(and (Flag316-)))

(:derived (Flag402-)(and (Flag294-)))

(:derived (Flag402-)(and (Flag391-)))

(:derived (Flag402-)(and (Flag275-)))

(:derived (Flag402-)(and (Flag377-)))

(:derived (Flag402-)(and (Flag322-)))

(:derived (Flag402-)(and (Flag300-)))

(:derived (Flag402-)(and (Flag372-)))

(:derived (Flag402-)(and (Flag274-)))

(:derived (Flag402-)(and (Flag326-)))

(:derived (Flag402-)(and (Flag279-)))

(:derived (Flag402-)(and (Flag392-)))

(:derived (Flag402-)(and (Flag349-)))

(:derived (Flag402-)(and (Flag368-)))

(:derived (Flag402-)(and (Flag293-)))

(:derived (Flag402-)(and (Flag363-)))

(:derived (Flag402-)(and (Flag313-)))

(:derived (Flag402-)(and (Flag331-)))

(:derived (Flag402-)(and (Flag288-)))

(:derived (Flag402-)(and (Flag318-)))

(:derived (Flag402-)(and (Flag350-)))

(:derived (Flag402-)(and (Flag366-)))

(:derived (Flag402-)(and (Flag393-)))

(:derived (Flag402-)(and (Flag292-)))

(:derived (Flag402-)(and (Flag283-)))

(:derived (Flag402-)(and (Flag379-)))

(:derived (Flag402-)(and (Flag355-)))

(:derived (Flag402-)(and (Flag394-)))

(:derived (Flag402-)(and (Flag330-)))

(:derived (Flag402-)(and (Flag381-)))

(:derived (Flag402-)(and (Flag369-)))

(:derived (Flag402-)(and (Flag340-)))

(:derived (Flag402-)(and (Flag304-)))

(:derived (Flag402-)(and (Flag305-)))

(:derived (Flag402-)(and (Flag323-)))

(:derived (Flag402-)(and (Flag382-)))

(:derived (Flag402-)(and (Flag344-)))

(:derived (Flag402-)(and (Flag345-)))

(:derived (Flag402-)(and (Flag289-)))

(:derived (Flag402-)(and (Flag383-)))

(:derived (Flag402-)(and (Flag310-)))

(:derived (Flag402-)(and (Flag384-)))

(:derived (Flag402-)(and (Flag395-)))

(:derived (Flag402-)(and (Flag396-)))

(:derived (Flag402-)(and (Flag329-)))

(:derived (Flag402-)(and (Flag337-)))

(:derived (Flag402-)(and (Flag397-)))

(:derived (Flag402-)(and (Flag357-)))

(:derived (Flag402-)(and (Flag385-)))

(:derived (Flag402-)(and (Flag314-)))

(:derived (Flag402-)(and (Flag398-)))

(:derived (Flag402-)(and (Flag297-)))

(:derived (Flag402-)(and (Flag335-)))

(:derived (Flag402-)(and (Flag298-)))

(:derived (Flag402-)(and (Flag371-)))

(:derived (Flag402-)(and (Flag399-)))

(:derived (Flag402-)(and (Flag348-)))

(:derived (Flag402-)(and (Flag373-)))

(:derived (Flag402-)(and (Flag400-)))

(:derived (Flag402-)(and (Flag359-)))

(:derived (Flag402-)(and (Flag299-)))

(:derived (Flag402-)(and (Flag361-)))

(:derived (Flag402-)(and (Flag347-)))

(:derived (Flag402-)(and (Flag327-)))

(:derived (Flag402-)(and (Flag291-)))

(:derived (Flag402-)(and (Flag354-)))

(:derived (Flag402-)(and (Flag284-)))

(:derived (Flag402-)(and (Flag364-)))

(:derived (Flag402-)(and (Flag286-)))

(:derived (Flag402-)(and (Flag401-)))

(:derived (Flag402-)(and (Flag312-)))

(:derived (Flag402-)(and (Flag333-)))

(:derived (Flag402-)(and (Flag386-)))

(:derived (Flag402-)(and (Flag356-)))

(:derived (Flag402-)(and (Flag387-)))

(:derived (Flag402-)(and (Flag296-)))

(:derived (Flag402-)(and (Flag320-)))

(:derived (Flag402-)(and (Flag334-)))

(:derived (Flag402-)(and (Flag321-)))

(:derived (Flag402-)(and (Flag336-)))

(:derived (Flag402-)(and (Flag273-)))

(:derived (Flag402-)(and (Flag388-)))

(:derived (Flag402-)(and (Flag342-)))

(:derived (Flag414-)(and (Flag278-)))

(:derived (Flag414-)(and (Flag403-)))

(:derived (Flag414-)(and (Flag404-)))

(:derived (Flag414-)(and (Flag362-)))

(:derived (Flag414-)(and (Flag376-)))

(:derived (Flag414-)(and (Flag276-)))

(:derived (Flag414-)(and (Flag365-)))

(:derived (Flag414-)(and (Flag351-)))

(:derived (Flag414-)(and (Flag390-)))

(:derived (Flag414-)(and (Flag353-)))

(:derived (Flag414-)(and (Flag315-)))

(:derived (Flag414-)(and (Flag316-)))

(:derived (Flag414-)(and (Flag294-)))

(:derived (Flag414-)(and (Flag405-)))

(:derived (Flag414-)(and (Flag391-)))

(:derived (Flag414-)(and (Flag275-)))

(:derived (Flag414-)(and (Flag377-)))

(:derived (Flag414-)(and (Flag322-)))

(:derived (Flag414-)(and (Flag384-)))

(:derived (Flag414-)(and (Flag300-)))

(:derived (Flag414-)(and (Flag372-)))

(:derived (Flag414-)(and (Flag274-)))

(:derived (Flag414-)(and (Flag326-)))

(:derived (Flag414-)(and (Flag279-)))

(:derived (Flag414-)(and (Flag392-)))

(:derived (Flag414-)(and (Flag349-)))

(:derived (Flag414-)(and (Flag406-)))

(:derived (Flag414-)(and (Flag363-)))

(:derived (Flag414-)(and (Flag331-)))

(:derived (Flag414-)(and (Flag288-)))

(:derived (Flag414-)(and (Flag318-)))

(:derived (Flag414-)(and (Flag350-)))

(:derived (Flag414-)(and (Flag366-)))

(:derived (Flag414-)(and (Flag292-)))

(:derived (Flag414-)(and (Flag407-)))

(:derived (Flag414-)(and (Flag379-)))

(:derived (Flag414-)(and (Flag355-)))

(:derived (Flag414-)(and (Flag408-)))

(:derived (Flag414-)(and (Flag394-)))

(:derived (Flag414-)(and (Flag330-)))

(:derived (Flag414-)(and (Flag381-)))

(:derived (Flag414-)(and (Flag369-)))

(:derived (Flag414-)(and (Flag340-)))

(:derived (Flag414-)(and (Flag304-)))

(:derived (Flag414-)(and (Flag305-)))

(:derived (Flag414-)(and (Flag323-)))

(:derived (Flag414-)(and (Flag382-)))

(:derived (Flag414-)(and (Flag344-)))

(:derived (Flag414-)(and (Flag345-)))

(:derived (Flag414-)(and (Flag289-)))

(:derived (Flag414-)(and (Flag383-)))

(:derived (Flag414-)(and (Flag310-)))

(:derived (Flag414-)(and (Flag409-)))

(:derived (Flag414-)(and (Flag320-)))

(:derived (Flag414-)(and (Flag395-)))

(:derived (Flag414-)(and (Flag337-)))

(:derived (Flag414-)(and (Flag397-)))

(:derived (Flag414-)(and (Flag357-)))

(:derived (Flag414-)(and (Flag385-)))

(:derived (Flag414-)(and (Flag314-)))

(:derived (Flag414-)(and (Flag410-)))

(:derived (Flag414-)(and (Flag398-)))

(:derived (Flag414-)(and (Flag297-)))

(:derived (Flag414-)(and (Flag335-)))

(:derived (Flag414-)(and (Flag298-)))

(:derived (Flag414-)(and (Flag371-)))

(:derived (Flag414-)(and (Flag399-)))

(:derived (Flag414-)(and (Flag348-)))

(:derived (Flag414-)(and (Flag373-)))

(:derived (Flag414-)(and (Flag400-)))

(:derived (Flag414-)(and (Flag359-)))

(:derived (Flag414-)(and (Flag299-)))

(:derived (Flag414-)(and (Flag361-)))

(:derived (Flag414-)(and (Flag411-)))

(:derived (Flag414-)(and (Flag327-)))

(:derived (Flag414-)(and (Flag291-)))

(:derived (Flag414-)(and (Flag354-)))

(:derived (Flag414-)(and (Flag284-)))

(:derived (Flag414-)(and (Flag364-)))

(:derived (Flag414-)(and (Flag286-)))

(:derived (Flag414-)(and (Flag401-)))

(:derived (Flag414-)(and (Flag312-)))

(:derived (Flag414-)(and (Flag386-)))

(:derived (Flag414-)(and (Flag356-)))

(:derived (Flag414-)(and (Flag387-)))

(:derived (Flag414-)(and (Flag296-)))

(:derived (Flag414-)(and (Flag412-)))

(:derived (Flag414-)(and (Flag334-)))

(:derived (Flag414-)(and (Flag342-)))

(:derived (Flag414-)(and (Flag321-)))

(:derived (Flag414-)(and (Flag336-)))

(:derived (Flag414-)(and (Flag273-)))

(:derived (Flag414-)(and (Flag388-)))

(:derived (Flag414-)(and (Flag413-)))

(:derived (Flag424-)(and (Flag278-)))

(:derived (Flag424-)(and (Flag415-)))

(:derived (Flag424-)(and (Flag403-)))

(:derived (Flag424-)(and (Flag362-)))

(:derived (Flag424-)(and (Flag376-)))

(:derived (Flag424-)(and (Flag365-)))

(:derived (Flag424-)(and (Flag390-)))

(:derived (Flag424-)(and (Flag353-)))

(:derived (Flag424-)(and (Flag315-)))

(:derived (Flag424-)(and (Flag354-)))

(:derived (Flag424-)(and (Flag294-)))

(:derived (Flag424-)(and (Flag405-)))

(:derived (Flag424-)(and (Flag391-)))

(:derived (Flag424-)(and (Flag275-)))

(:derived (Flag424-)(and (Flag377-)))

(:derived (Flag424-)(and (Flag322-)))

(:derived (Flag424-)(and (Flag372-)))

(:derived (Flag424-)(and (Flag274-)))

(:derived (Flag424-)(and (Flag326-)))

(:derived (Flag424-)(and (Flag279-)))

(:derived (Flag424-)(and (Flag392-)))

(:derived (Flag424-)(and (Flag349-)))

(:derived (Flag424-)(and (Flag406-)))

(:derived (Flag424-)(and (Flag363-)))

(:derived (Flag424-)(and (Flag331-)))

(:derived (Flag424-)(and (Flag288-)))

(:derived (Flag424-)(and (Flag318-)))

(:derived (Flag424-)(and (Flag350-)))

(:derived (Flag424-)(and (Flag366-)))

(:derived (Flag424-)(and (Flag292-)))

(:derived (Flag424-)(and (Flag407-)))

(:derived (Flag424-)(and (Flag379-)))

(:derived (Flag424-)(and (Flag355-)))

(:derived (Flag424-)(and (Flag408-)))

(:derived (Flag424-)(and (Flag394-)))

(:derived (Flag424-)(and (Flag330-)))

(:derived (Flag424-)(and (Flag381-)))

(:derived (Flag424-)(and (Flag369-)))

(:derived (Flag424-)(and (Flag416-)))

(:derived (Flag424-)(and (Flag340-)))

(:derived (Flag424-)(and (Flag304-)))

(:derived (Flag424-)(and (Flag305-)))

(:derived (Flag424-)(and (Flag323-)))

(:derived (Flag424-)(and (Flag382-)))

(:derived (Flag424-)(and (Flag344-)))

(:derived (Flag424-)(and (Flag345-)))

(:derived (Flag424-)(and (Flag276-)))

(:derived (Flag424-)(and (Flag417-)))

(:derived (Flag424-)(and (Flag310-)))

(:derived (Flag424-)(and (Flag409-)))

(:derived (Flag424-)(and (Flag320-)))

(:derived (Flag424-)(and (Flag395-)))

(:derived (Flag424-)(and (Flag418-)))

(:derived (Flag424-)(and (Flag337-)))

(:derived (Flag424-)(and (Flag397-)))

(:derived (Flag424-)(and (Flag357-)))

(:derived (Flag424-)(and (Flag385-)))

(:derived (Flag424-)(and (Flag314-)))

(:derived (Flag424-)(and (Flag410-)))

(:derived (Flag424-)(and (Flag398-)))

(:derived (Flag424-)(and (Flag419-)))

(:derived (Flag424-)(and (Flag297-)))

(:derived (Flag424-)(and (Flag335-)))

(:derived (Flag424-)(and (Flag298-)))

(:derived (Flag424-)(and (Flag371-)))

(:derived (Flag424-)(and (Flag399-)))

(:derived (Flag424-)(and (Flag348-)))

(:derived (Flag424-)(and (Flag373-)))

(:derived (Flag424-)(and (Flag400-)))

(:derived (Flag424-)(and (Flag359-)))

(:derived (Flag424-)(and (Flag299-)))

(:derived (Flag424-)(and (Flag383-)))

(:derived (Flag424-)(and (Flag361-)))

(:derived (Flag424-)(and (Flag420-)))

(:derived (Flag424-)(and (Flag421-)))

(:derived (Flag424-)(and (Flag411-)))

(:derived (Flag424-)(and (Flag327-)))

(:derived (Flag424-)(and (Flag291-)))

(:derived (Flag424-)(and (Flag284-)))

(:derived (Flag424-)(and (Flag286-)))

(:derived (Flag424-)(and (Flag312-)))

(:derived (Flag424-)(and (Flag386-)))

(:derived (Flag424-)(and (Flag387-)))

(:derived (Flag424-)(and (Flag422-)))

(:derived (Flag424-)(and (Flag289-)))

(:derived (Flag424-)(and (Flag412-)))

(:derived (Flag424-)(and (Flag334-)))

(:derived (Flag424-)(and (Flag342-)))

(:derived (Flag424-)(and (Flag321-)))

(:derived (Flag424-)(and (Flag336-)))

(:derived (Flag424-)(and (Flag273-)))

(:derived (Flag424-)(and (Flag423-)))

(:derived (Flag424-)(and (Flag388-)))

(:derived (Flag424-)(and (Flag413-)))

(:derived (Flag434-)(and (Flag278-)))

(:derived (Flag434-)(and (Flag425-)))

(:derived (Flag434-)(and (Flag415-)))

(:derived (Flag434-)(and (Flag403-)))

(:derived (Flag434-)(and (Flag426-)))

(:derived (Flag434-)(and (Flag362-)))

(:derived (Flag434-)(and (Flag376-)))

(:derived (Flag434-)(and (Flag365-)))

(:derived (Flag434-)(and (Flag390-)))

(:derived (Flag434-)(and (Flag353-)))

(:derived (Flag434-)(and (Flag315-)))

(:derived (Flag434-)(and (Flag354-)))

(:derived (Flag434-)(and (Flag294-)))

(:derived (Flag434-)(and (Flag427-)))

(:derived (Flag434-)(and (Flag405-)))

(:derived (Flag434-)(and (Flag391-)))

(:derived (Flag434-)(and (Flag322-)))

(:derived (Flag434-)(and (Flag372-)))

(:derived (Flag434-)(and (Flag274-)))

(:derived (Flag434-)(and (Flag326-)))

(:derived (Flag434-)(and (Flag279-)))

(:derived (Flag434-)(and (Flag392-)))

(:derived (Flag434-)(and (Flag349-)))

(:derived (Flag434-)(and (Flag363-)))

(:derived (Flag434-)(and (Flag331-)))

(:derived (Flag434-)(and (Flag288-)))

(:derived (Flag434-)(and (Flag318-)))

(:derived (Flag434-)(and (Flag350-)))

(:derived (Flag434-)(and (Flag428-)))

(:derived (Flag434-)(and (Flag366-)))

(:derived (Flag434-)(and (Flag292-)))

(:derived (Flag434-)(and (Flag407-)))

(:derived (Flag434-)(and (Flag379-)))

(:derived (Flag434-)(and (Flag355-)))

(:derived (Flag434-)(and (Flag408-)))

(:derived (Flag434-)(and (Flag429-)))

(:derived (Flag434-)(and (Flag394-)))

(:derived (Flag434-)(and (Flag330-)))

(:derived (Flag434-)(and (Flag381-)))

(:derived (Flag434-)(and (Flag369-)))

(:derived (Flag434-)(and (Flag416-)))

(:derived (Flag434-)(and (Flag348-)))

(:derived (Flag434-)(and (Flag340-)))

(:derived (Flag434-)(and (Flag304-)))

(:derived (Flag434-)(and (Flag305-)))

(:derived (Flag434-)(and (Flag382-)))

(:derived (Flag434-)(and (Flag430-)))

(:derived (Flag434-)(and (Flag345-)))

(:derived (Flag434-)(and (Flag417-)))

(:derived (Flag434-)(and (Flag310-)))

(:derived (Flag434-)(and (Flag409-)))

(:derived (Flag434-)(and (Flag320-)))

(:derived (Flag434-)(and (Flag418-)))

(:derived (Flag434-)(and (Flag337-)))

(:derived (Flag434-)(and (Flag397-)))

(:derived (Flag434-)(and (Flag357-)))

(:derived (Flag434-)(and (Flag385-)))

(:derived (Flag434-)(and (Flag314-)))

(:derived (Flag434-)(and (Flag410-)))

(:derived (Flag434-)(and (Flag398-)))

(:derived (Flag434-)(and (Flag297-)))

(:derived (Flag434-)(and (Flag383-)))

(:derived (Flag434-)(and (Flag298-)))

(:derived (Flag434-)(and (Flag371-)))

(:derived (Flag434-)(and (Flag399-)))

(:derived (Flag434-)(and (Flag431-)))

(:derived (Flag434-)(and (Flag400-)))

(:derived (Flag434-)(and (Flag359-)))

(:derived (Flag434-)(and (Flag299-)))

(:derived (Flag434-)(and (Flag361-)))

(:derived (Flag434-)(and (Flag420-)))

(:derived (Flag434-)(and (Flag421-)))

(:derived (Flag434-)(and (Flag411-)))

(:derived (Flag434-)(and (Flag327-)))

(:derived (Flag434-)(and (Flag291-)))

(:derived (Flag434-)(and (Flag284-)))

(:derived (Flag434-)(and (Flag286-)))

(:derived (Flag434-)(and (Flag312-)))

(:derived (Flag434-)(and (Flag386-)))

(:derived (Flag434-)(and (Flag387-)))

(:derived (Flag434-)(and (Flag422-)))

(:derived (Flag434-)(and (Flag289-)))

(:derived (Flag434-)(and (Flag412-)))

(:derived (Flag434-)(and (Flag334-)))

(:derived (Flag434-)(and (Flag342-)))

(:derived (Flag434-)(and (Flag321-)))

(:derived (Flag434-)(and (Flag273-)))

(:derived (Flag434-)(and (Flag423-)))

(:derived (Flag434-)(and (Flag388-)))

(:derived (Flag434-)(and (Flag432-)))

(:derived (Flag434-)(and (Flag433-)))

(:derived (Flag434-)(and (Flag413-)))

(:derived (Flag442-)(and (Flag278-)))

(:derived (Flag442-)(and (Flag425-)))

(:derived (Flag442-)(and (Flag415-)))

(:derived (Flag442-)(and (Flag403-)))

(:derived (Flag442-)(and (Flag426-)))

(:derived (Flag442-)(and (Flag362-)))

(:derived (Flag442-)(and (Flag376-)))

(:derived (Flag442-)(and (Flag365-)))

(:derived (Flag442-)(and (Flag435-)))

(:derived (Flag442-)(and (Flag436-)))

(:derived (Flag442-)(and (Flag417-)))

(:derived (Flag442-)(and (Flag294-)))

(:derived (Flag442-)(and (Flag427-)))

(:derived (Flag442-)(and (Flag405-)))

(:derived (Flag442-)(and (Flag391-)))

(:derived (Flag442-)(and (Flag322-)))

(:derived (Flag442-)(and (Flag372-)))

(:derived (Flag442-)(and (Flag274-)))

(:derived (Flag442-)(and (Flag326-)))

(:derived (Flag442-)(and (Flag392-)))

(:derived (Flag442-)(and (Flag349-)))

(:derived (Flag442-)(and (Flag363-)))

(:derived (Flag442-)(and (Flag331-)))

(:derived (Flag442-)(and (Flag288-)))

(:derived (Flag442-)(and (Flag318-)))

(:derived (Flag442-)(and (Flag350-)))

(:derived (Flag442-)(and (Flag428-)))

(:derived (Flag442-)(and (Flag366-)))

(:derived (Flag442-)(and (Flag292-)))

(:derived (Flag442-)(and (Flag407-)))

(:derived (Flag442-)(and (Flag379-)))

(:derived (Flag442-)(and (Flag355-)))

(:derived (Flag442-)(and (Flag408-)))

(:derived (Flag442-)(and (Flag429-)))

(:derived (Flag442-)(and (Flag394-)))

(:derived (Flag442-)(and (Flag330-)))

(:derived (Flag442-)(and (Flag381-)))

(:derived (Flag442-)(and (Flag416-)))

(:derived (Flag442-)(and (Flag348-)))

(:derived (Flag442-)(and (Flag340-)))

(:derived (Flag442-)(and (Flag304-)))

(:derived (Flag442-)(and (Flag305-)))

(:derived (Flag442-)(and (Flag382-)))

(:derived (Flag442-)(and (Flag430-)))

(:derived (Flag442-)(and (Flag345-)))

(:derived (Flag442-)(and (Flag383-)))

(:derived (Flag442-)(and (Flag310-)))

(:derived (Flag442-)(and (Flag409-)))

(:derived (Flag442-)(and (Flag437-)))

(:derived (Flag442-)(and (Flag320-)))

(:derived (Flag442-)(and (Flag418-)))

(:derived (Flag442-)(and (Flag438-)))

(:derived (Flag442-)(and (Flag337-)))

(:derived (Flag442-)(and (Flag397-)))

(:derived (Flag442-)(and (Flag357-)))

(:derived (Flag442-)(and (Flag385-)))

(:derived (Flag442-)(and (Flag314-)))

(:derived (Flag442-)(and (Flag439-)))

(:derived (Flag442-)(and (Flag398-)))

(:derived (Flag442-)(and (Flag297-)))

(:derived (Flag442-)(and (Flag298-)))

(:derived (Flag442-)(and (Flag371-)))

(:derived (Flag442-)(and (Flag399-)))

(:derived (Flag442-)(and (Flag431-)))

(:derived (Flag442-)(and (Flag400-)))

(:derived (Flag442-)(and (Flag359-)))

(:derived (Flag442-)(and (Flag299-)))

(:derived (Flag442-)(and (Flag361-)))

(:derived (Flag442-)(and (Flag420-)))

(:derived (Flag442-)(and (Flag411-)))

(:derived (Flag442-)(and (Flag327-)))

(:derived (Flag442-)(and (Flag284-)))

(:derived (Flag442-)(and (Flag286-)))

(:derived (Flag442-)(and (Flag440-)))

(:derived (Flag442-)(and (Flag312-)))

(:derived (Flag442-)(and (Flag386-)))

(:derived (Flag442-)(and (Flag387-)))

(:derived (Flag442-)(and (Flag422-)))

(:derived (Flag442-)(and (Flag289-)))

(:derived (Flag442-)(and (Flag412-)))

(:derived (Flag442-)(and (Flag334-)))

(:derived (Flag442-)(and (Flag342-)))

(:derived (Flag442-)(and (Flag321-)))

(:derived (Flag442-)(and (Flag441-)))

(:derived (Flag442-)(and (Flag273-)))

(:derived (Flag442-)(and (Flag423-)))

(:derived (Flag442-)(and (Flag432-)))

(:derived (Flag442-)(and (Flag413-)))

(:derived (Flag450-)(and (Flag430-)))

(:derived (Flag450-)(and (Flag345-)))

(:derived (Flag450-)(and (Flag274-)))

(:derived (Flag450-)(and (Flag359-)))

(:derived (Flag450-)(and (Flag361-)))

(:derived (Flag450-)(and (Flag278-)))

(:derived (Flag450-)(and (Flag417-)))

(:derived (Flag450-)(and (Flag310-)))

(:derived (Flag450-)(and (Flag425-)))

(:derived (Flag450-)(and (Flag409-)))

(:derived (Flag450-)(and (Flag411-)))

(:derived (Flag450-)(and (Flag327-)))

(:derived (Flag450-)(and (Flag415-)))

(:derived (Flag450-)(and (Flag418-)))

(:derived (Flag450-)(and (Flag292-)))

(:derived (Flag450-)(and (Flag403-)))

(:derived (Flag450-)(and (Flag443-)))

(:derived (Flag450-)(and (Flag440-)))

(:derived (Flag450-)(and (Flag426-)))

(:derived (Flag450-)(and (Flag438-)))

(:derived (Flag450-)(and (Flag337-)))

(:derived (Flag450-)(and (Flag397-)))

(:derived (Flag450-)(and (Flag357-)))

(:derived (Flag450-)(and (Flag362-)))

(:derived (Flag450-)(and (Flag385-)))

(:derived (Flag450-)(and (Flag376-)))

(:derived (Flag450-)(and (Flag386-)))

(:derived (Flag450-)(and (Flag284-)))

(:derived (Flag450-)(and (Flag330-)))

(:derived (Flag450-)(and (Flag305-)))

(:derived (Flag450-)(and (Flag331-)))

(:derived (Flag450-)(and (Flag288-)))

(:derived (Flag450-)(and (Flag318-)))

(:derived (Flag450-)(and (Flag439-)))

(:derived (Flag450-)(and (Flag435-)))

(:derived (Flag450-)(and (Flag428-)))

(:derived (Flag450-)(and (Flag366-)))

(:derived (Flag450-)(and (Flag391-)))

(:derived (Flag450-)(and (Flag398-)))

(:derived (Flag450-)(and (Flag392-)))

(:derived (Flag450-)(and (Flag312-)))

(:derived (Flag450-)(and (Flag379-)))

(:derived (Flag450-)(and (Flag407-)))

(:derived (Flag450-)(and (Flag294-)))

(:derived (Flag450-)(and (Flag444-)))

(:derived (Flag450-)(and (Flag355-)))

(:derived (Flag450-)(and (Flag405-)))

(:derived (Flag450-)(and (Flag445-)))

(:derived (Flag450-)(and (Flag422-)))

(:derived (Flag450-)(and (Flag429-)))

(:derived (Flag450-)(and (Flag394-)))

(:derived (Flag450-)(and (Flag314-)))

(:derived (Flag450-)(and (Flag289-)))

(:derived (Flag450-)(and (Flag297-)))

(:derived (Flag450-)(and (Flag412-)))

(:derived (Flag450-)(and (Flag334-)))

(:derived (Flag450-)(and (Flag350-)))

(:derived (Flag450-)(and (Flag321-)))

(:derived (Flag450-)(and (Flag387-)))

(:derived (Flag450-)(and (Flag381-)))

(:derived (Flag450-)(and (Flag322-)))

(:derived (Flag450-)(and (Flag273-)))

(:derived (Flag450-)(and (Flag416-)))

(:derived (Flag450-)(and (Flag441-)))

(:derived (Flag450-)(and (Flag446-)))

(:derived (Flag450-)(and (Flag298-)))

(:derived (Flag450-)(and (Flag371-)))

(:derived (Flag450-)(and (Flag423-)))

(:derived (Flag450-)(and (Flag447-)))

(:derived (Flag450-)(and (Flag432-)))

(:derived (Flag450-)(and (Flag340-)))

(:derived (Flag450-)(and (Flag448-)))

(:derived (Flag450-)(and (Flag326-)))

(:derived (Flag450-)(and (Flag449-)))

(:derived (Flag450-)(and (Flag304-)))

(:derived (Flag450-)(and (Flag413-)))

(:derived (Flag450-)(and (Flag349-)))

(:derived (Flag450-)(and (Flag431-)))

(:derived (Flag450-)(and (Flag372-)))

(:derived (Flag450-)(and (Flag400-)))

(:derived (Flag450-)(and (Flag382-)))

(:derived (Flag456-)(and (Flag273-)))

(:derived (Flag456-)(and (Flag345-)))

(:derived (Flag456-)(and (Flag274-)))

(:derived (Flag456-)(and (Flag430-)))

(:derived (Flag456-)(and (Flag359-)))

(:derived (Flag456-)(and (Flag361-)))

(:derived (Flag456-)(and (Flag278-)))

(:derived (Flag456-)(and (Flag451-)))

(:derived (Flag456-)(and (Flag310-)))

(:derived (Flag456-)(and (Flag425-)))

(:derived (Flag456-)(and (Flag409-)))

(:derived (Flag456-)(and (Flag411-)))

(:derived (Flag456-)(and (Flag327-)))

(:derived (Flag456-)(and (Flag418-)))

(:derived (Flag456-)(and (Flag403-)))

(:derived (Flag456-)(and (Flag443-)))

(:derived (Flag456-)(and (Flag440-)))

(:derived (Flag456-)(and (Flag426-)))

(:derived (Flag456-)(and (Flag438-)))

(:derived (Flag456-)(and (Flag337-)))

(:derived (Flag456-)(and (Flag397-)))

(:derived (Flag456-)(and (Flag357-)))

(:derived (Flag456-)(and (Flag362-)))

(:derived (Flag456-)(and (Flag385-)))

(:derived (Flag456-)(and (Flag376-)))

(:derived (Flag456-)(and (Flag284-)))

(:derived (Flag456-)(and (Flag305-)))

(:derived (Flag456-)(and (Flag331-)))

(:derived (Flag456-)(and (Flag288-)))

(:derived (Flag456-)(and (Flag318-)))

(:derived (Flag456-)(and (Flag439-)))

(:derived (Flag456-)(and (Flag435-)))

(:derived (Flag456-)(and (Flag428-)))

(:derived (Flag456-)(and (Flag366-)))

(:derived (Flag456-)(and (Flag452-)))

(:derived (Flag456-)(and (Flag312-)))

(:derived (Flag456-)(and (Flag417-)))

(:derived (Flag456-)(and (Flag407-)))

(:derived (Flag456-)(and (Flag349-)))

(:derived (Flag456-)(and (Flag386-)))

(:derived (Flag456-)(and (Flag445-)))

(:derived (Flag456-)(and (Flag453-)))

(:derived (Flag456-)(and (Flag429-)))

(:derived (Flag456-)(and (Flag394-)))

(:derived (Flag456-)(and (Flag391-)))

(:derived (Flag456-)(and (Flag289-)))

(:derived (Flag456-)(and (Flag297-)))

(:derived (Flag456-)(and (Flag412-)))

(:derived (Flag456-)(and (Flag350-)))

(:derived (Flag456-)(and (Flag330-)))

(:derived (Flag456-)(and (Flag387-)))

(:derived (Flag456-)(and (Flag371-)))

(:derived (Flag456-)(and (Flag322-)))

(:derived (Flag456-)(and (Flag422-)))

(:derived (Flag456-)(and (Flag416-)))

(:derived (Flag456-)(and (Flag441-)))

(:derived (Flag456-)(and (Flag446-)))

(:derived (Flag456-)(and (Flag298-)))

(:derived (Flag456-)(and (Flag454-)))

(:derived (Flag456-)(and (Flag423-)))

(:derived (Flag456-)(and (Flag447-)))

(:derived (Flag456-)(and (Flag392-)))

(:derived (Flag456-)(and (Flag455-)))

(:derived (Flag456-)(and (Flag432-)))

(:derived (Flag456-)(and (Flag340-)))

(:derived (Flag456-)(and (Flag448-)))

(:derived (Flag456-)(and (Flag326-)))

(:derived (Flag456-)(and (Flag304-)))

(:derived (Flag456-)(and (Flag413-)))

(:derived (Flag456-)(and (Flag372-)))

(:derived (Flag456-)(and (Flag400-)))

(:derived (Flag456-)(and (Flag382-)))

(:derived (Flag461-)(and (Flag430-)))

(:derived (Flag461-)(and (Flag345-)))

(:derived (Flag461-)(and (Flag359-)))

(:derived (Flag461-)(and (Flag361-)))

(:derived (Flag461-)(and (Flag278-)))

(:derived (Flag461-)(and (Flag451-)))

(:derived (Flag461-)(and (Flag310-)))

(:derived (Flag461-)(and (Flag425-)))

(:derived (Flag461-)(and (Flag409-)))

(:derived (Flag461-)(and (Flag411-)))

(:derived (Flag461-)(and (Flag327-)))

(:derived (Flag461-)(and (Flag457-)))

(:derived (Flag461-)(and (Flag273-)))

(:derived (Flag461-)(and (Flag447-)))

(:derived (Flag461-)(and (Flag443-)))

(:derived (Flag461-)(and (Flag440-)))

(:derived (Flag461-)(and (Flag426-)))

(:derived (Flag461-)(and (Flag438-)))

(:derived (Flag461-)(and (Flag337-)))

(:derived (Flag461-)(and (Flag357-)))

(:derived (Flag461-)(and (Flag362-)))

(:derived (Flag461-)(and (Flag385-)))

(:derived (Flag461-)(and (Flag284-)))

(:derived (Flag461-)(and (Flag331-)))

(:derived (Flag461-)(and (Flag288-)))

(:derived (Flag461-)(and (Flag391-)))

(:derived (Flag461-)(and (Flag439-)))

(:derived (Flag461-)(and (Flag435-)))

(:derived (Flag461-)(and (Flag428-)))

(:derived (Flag461-)(and (Flag366-)))

(:derived (Flag461-)(and (Flag458-)))

(:derived (Flag461-)(and (Flag312-)))

(:derived (Flag461-)(and (Flag407-)))

(:derived (Flag461-)(and (Flag350-)))

(:derived (Flag461-)(and (Flag386-)))

(:derived (Flag461-)(and (Flag445-)))

(:derived (Flag461-)(and (Flag453-)))

(:derived (Flag461-)(and (Flag394-)))

(:derived (Flag461-)(and (Flag318-)))

(:derived (Flag461-)(and (Flag289-)))

(:derived (Flag461-)(and (Flag297-)))

(:derived (Flag461-)(and (Flag412-)))

(:derived (Flag461-)(and (Flag305-)))

(:derived (Flag461-)(and (Flag330-)))

(:derived (Flag461-)(and (Flag387-)))

(:derived (Flag461-)(and (Flag459-)))

(:derived (Flag461-)(and (Flag322-)))

(:derived (Flag461-)(and (Flag422-)))

(:derived (Flag461-)(and (Flag416-)))

(:derived (Flag461-)(and (Flag455-)))

(:derived (Flag461-)(and (Flag454-)))

(:derived (Flag461-)(and (Flag423-)))

(:derived (Flag461-)(and (Flag432-)))

(:derived (Flag461-)(and (Flag448-)))

(:derived (Flag461-)(and (Flag304-)))

(:derived (Flag461-)(and (Flag413-)))

(:derived (Flag461-)(and (Flag460-)))

(:derived (Flag461-)(and (Flag372-)))

(:derived (Flag461-)(and (Flag400-)))

(:derived (Flag461-)(and (Flag382-)))

(:derived (Flag466-)(and (Flag273-)))

(:derived (Flag466-)(and (Flag345-)))

(:derived (Flag466-)(and (Flag359-)))

(:derived (Flag466-)(and (Flag361-)))

(:derived (Flag466-)(and (Flag278-)))

(:derived (Flag466-)(and (Flag462-)))

(:derived (Flag466-)(and (Flag425-)))

(:derived (Flag466-)(and (Flag409-)))

(:derived (Flag466-)(and (Flag411-)))

(:derived (Flag466-)(and (Flag327-)))

(:derived (Flag466-)(and (Flag457-)))

(:derived (Flag466-)(and (Flag463-)))

(:derived (Flag466-)(and (Flag443-)))

(:derived (Flag466-)(and (Flag440-)))

(:derived (Flag466-)(and (Flag426-)))

(:derived (Flag466-)(and (Flag438-)))

(:derived (Flag466-)(and (Flag337-)))

(:derived (Flag466-)(and (Flag362-)))

(:derived (Flag466-)(and (Flag385-)))

(:derived (Flag466-)(and (Flag284-)))

(:derived (Flag466-)(and (Flag288-)))

(:derived (Flag466-)(and (Flag318-)))

(:derived (Flag466-)(and (Flag439-)))

(:derived (Flag466-)(and (Flag366-)))

(:derived (Flag466-)(and (Flag458-)))

(:derived (Flag466-)(and (Flag312-)))

(:derived (Flag466-)(and (Flag386-)))

(:derived (Flag466-)(and (Flag422-)))

(:derived (Flag466-)(and (Flag394-)))

(:derived (Flag466-)(and (Flag391-)))

(:derived (Flag466-)(and (Flag322-)))

(:derived (Flag466-)(and (Flag297-)))

(:derived (Flag466-)(and (Flag412-)))

(:derived (Flag466-)(and (Flag350-)))

(:derived (Flag466-)(and (Flag330-)))

(:derived (Flag466-)(and (Flag464-)))

(:derived (Flag466-)(and (Flag453-)))

(:derived (Flag466-)(and (Flag416-)))

(:derived (Flag466-)(and (Flag455-)))

(:derived (Flag466-)(and (Flag454-)))

(:derived (Flag466-)(and (Flag423-)))

(:derived (Flag466-)(and (Flag447-)))

(:derived (Flag466-)(and (Flag432-)))

(:derived (Flag466-)(and (Flag448-)))

(:derived (Flag466-)(and (Flag400-)))

(:derived (Flag466-)(and (Flag305-)))

(:derived (Flag466-)(and (Flag465-)))

(:derived (Flag466-)(and (Flag382-)))

(:derived (Flag469-)(and (Flag361-)))

(:derived (Flag469-)(and (Flag462-)))

(:derived (Flag469-)(and (Flag411-)))

(:derived (Flag469-)(and (Flag327-)))

(:derived (Flag469-)(and (Flag457-)))

(:derived (Flag469-)(and (Flag447-)))

(:derived (Flag469-)(and (Flag426-)))

(:derived (Flag469-)(and (Flag438-)))

(:derived (Flag469-)(and (Flag337-)))

(:derived (Flag469-)(and (Flag467-)))

(:derived (Flag469-)(and (Flag385-)))

(:derived (Flag469-)(and (Flag284-)))

(:derived (Flag469-)(and (Flag288-)))

(:derived (Flag469-)(and (Flag391-)))

(:derived (Flag469-)(and (Flag439-)))

(:derived (Flag469-)(and (Flag366-)))

(:derived (Flag469-)(and (Flag458-)))

(:derived (Flag469-)(and (Flag312-)))

(:derived (Flag469-)(and (Flag386-)))

(:derived (Flag469-)(and (Flag468-)))

(:derived (Flag469-)(and (Flag453-)))

(:derived (Flag469-)(and (Flag322-)))

(:derived (Flag469-)(and (Flag297-)))

(:derived (Flag469-)(and (Flag412-)))

(:derived (Flag469-)(and (Flag350-)))

(:derived (Flag469-)(and (Flag464-)))

(:derived (Flag469-)(and (Flag422-)))

(:derived (Flag469-)(and (Flag416-)))

(:derived (Flag469-)(and (Flag454-)))

(:derived (Flag469-)(and (Flag432-)))

(:derived (Flag469-)(and (Flag448-)))

(:derived (Flag469-)(and (Flag305-)))

(:derived (Flag469-)(and (Flag400-)))

(:derived (Flag469-)(and (Flag359-)))

(:derived (Flag471-)(and (Flag385-)))

(:derived (Flag471-)(and (Flag322-)))

(:derived (Flag471-)(and (Flag416-)))

(:derived (Flag471-)(and (Flag467-)))

(:derived (Flag471-)(and (Flag327-)))

(:derived (Flag471-)(and (Flag454-)))

(:derived (Flag471-)(and (Flag297-)))

(:derived (Flag471-)(and (Flag462-)))

(:derived (Flag471-)(and (Flag288-)))

(:derived (Flag471-)(and (Flag432-)))

(:derived (Flag471-)(and (Flag470-)))

(:derived (Flag471-)(and (Flag400-)))

(:derived (Flag471-)(and (Flag412-)))

(:derived (Flag471-)(and (Flag439-)))

(:derived (Flag471-)(and (Flag366-)))

(:derived (Flag471-)(and (Flag458-)))

(:derived (Flag471-)(and (Flag447-)))

(:derived (Flag471-)(and (Flag359-)))

(:derived (Flag475-)(and (Flag290-)))

(:derived (Flag475-)(and (Flag292-)))

(:derived (Flag475-)(and (Flag293-)))

(:derived (Flag475-)(and (Flag276-)))

(:derived (Flag475-)(and (Flag298-)))

(:derived (Flag475-)(and (Flag282-)))

(:derived (Flag475-)(and (Flag277-)))

(:derived (Flag475-)(and (Flag278-)))

(:derived (Flag475-)(and (Flag297-)))

(:derived (Flag475-)(and (Flag474-)))

(:derived (Flag475-)(and (Flag295-)))

(:derived (Flag475-)(and (Flag302-)))

(:derived (Flag475-)(and (Flag286-)))

(:derived (Flag475-)(and (Flag296-)))

(:derived (Flag475-)(and (Flag303-)))

(:derived (Flag475-)(and (Flag304-)))

(:derived (Flag475-)(and (Flag279-)))

(:derived (Flag475-)(and (Flag305-)))

(:derived (Flag1-)(and (COLUMN2-ROBOT)(ROW1-ROBOT)(NOT-ERROR-)(NOT-CHECKCONSISTENCY-)))

(:derived (Flag2-)(and (LEFTOF3-ROBOT)))

(:derived (Flag2-)(and (RIGHTOF7-ROBOT)))

(:derived (Flag2-)(and (RIGHTOF8-ROBOT)))

(:derived (Flag2-)(and (RIGHTOF15-ROBOT)))

(:derived (Flag2-)(and (LEFTOF2-ROBOT)))

(:derived (Flag2-)(and (LEFTOF9-ROBOT)))

(:derived (Flag2-)(and (RIGHTOF0-ROBOT)))

(:derived (Flag2-)(and (LEFTOF8-ROBOT)))

(:derived (Flag2-)(and (LEFTOF7-ROBOT)))

(:derived (Flag2-)(and (LEFTOF14-ROBOT)))

(:derived (Flag2-)(and (LEFTOF5-ROBOT)))

(:derived (Flag2-)(and (RIGHTOF1-ROBOT)))

(:derived (Flag2-)(and (LEFTOF12-ROBOT)))

(:derived (Flag2-)(and (RIGHTOF13-ROBOT)))

(:derived (Flag2-)(and (RIGHTOF14-ROBOT)))

(:derived (Flag2-)(and (RIGHTOF6-ROBOT)))

(:derived (Flag2-)(and (RIGHTOF9-ROBOT)))

(:derived (Flag2-)(and (LEFTOF6-ROBOT)))

(:derived (Flag2-)(and (RIGHTOF10-ROBOT)))

(:derived (Flag2-)(and (RIGHTOF11-ROBOT)))

(:derived (Flag2-)(and (RIGHTOF17-ROBOT)))

(:derived (Flag2-)(and (LEFTOF11-ROBOT)))

(:derived (Flag2-)(and (RIGHTOF5-ROBOT)))

(:derived (Flag2-)(and (LEFTOF10-ROBOT)))

(:derived (Flag2-)(and (LEFTOF15-ROBOT)))

(:derived (Flag2-)(and (LEFTOF4-ROBOT)))

(:derived (Flag2-)(and (LEFTOF16-ROBOT)))

(:derived (Flag2-)(and (RIGHTOF16-ROBOT)))

(:derived (Flag2-)(and (RIGHTOF4-ROBOT)))

(:derived (Flag2-)(and (LEFTOF1-ROBOT)))

(:derived (Flag2-)(and (RIGHTOF12-ROBOT)))

(:derived (Flag2-)(and (LEFTOF13-ROBOT)))

(:derived (Flag2-)(and (LEFTOF18-ROBOT)))

(:derived (Flag2-)(and (RIGHTOF3-ROBOT)))

(:derived (Flag73-)(and (Flag38-)))

(:derived (Flag73-)(and (Flag39-)))

(:derived (Flag73-)(and (Flag40-)))

(:derived (Flag73-)(and (Flag41-)))

(:derived (Flag73-)(and (Flag42-)))

(:derived (Flag73-)(and (Flag43-)))

(:derived (Flag73-)(and (Flag44-)))

(:derived (Flag73-)(and (Flag45-)))

(:derived (Flag73-)(and (Flag46-)))

(:derived (Flag73-)(and (Flag47-)))

(:derived (Flag73-)(and (Flag48-)))

(:derived (Flag73-)(and (Flag49-)))

(:derived (Flag73-)(and (Flag50-)))

(:derived (Flag73-)(and (Flag51-)))

(:derived (Flag73-)(and (Flag52-)))

(:derived (Flag73-)(and (Flag53-)))

(:derived (Flag73-)(and (Flag54-)))

(:derived (Flag73-)(and (Flag55-)))

(:derived (Flag73-)(and (Flag56-)))

(:derived (Flag73-)(and (Flag57-)))

(:derived (Flag73-)(and (Flag58-)))

(:derived (Flag73-)(and (Flag59-)))

(:derived (Flag73-)(and (Flag60-)))

(:derived (Flag73-)(and (Flag61-)))

(:derived (Flag73-)(and (Flag62-)))

(:derived (Flag73-)(and (Flag63-)))

(:derived (Flag73-)(and (Flag64-)))

(:derived (Flag73-)(and (Flag65-)))

(:derived (Flag73-)(and (Flag66-)))

(:derived (Flag73-)(and (Flag67-)))

(:derived (Flag73-)(and (Flag68-)))

(:derived (Flag73-)(and (Flag69-)))

(:derived (Flag73-)(and (Flag70-)))

(:derived (Flag73-)(and (Flag71-)))

(:derived (Flag73-)(and (Flag72-)))

(:derived (Flag91-)(and (Flag38-)))

(:derived (Flag91-)(and (Flag74-)))

(:derived (Flag91-)(and (Flag45-)))

(:derived (Flag91-)(and (Flag61-)))

(:derived (Flag91-)(and (Flag75-)))

(:derived (Flag91-)(and (Flag54-)))

(:derived (Flag91-)(and (Flag76-)))

(:derived (Flag91-)(and (Flag40-)))

(:derived (Flag91-)(and (Flag51-)))

(:derived (Flag91-)(and (Flag42-)))

(:derived (Flag91-)(and (Flag43-)))

(:derived (Flag91-)(and (Flag77-)))

(:derived (Flag91-)(and (Flag78-)))

(:derived (Flag91-)(and (Flag44-)))

(:derived (Flag91-)(and (Flag46-)))

(:derived (Flag91-)(and (Flag79-)))

(:derived (Flag91-)(and (Flag48-)))

(:derived (Flag91-)(and (Flag80-)))

(:derived (Flag91-)(and (Flag81-)))

(:derived (Flag91-)(and (Flag49-)))

(:derived (Flag91-)(and (Flag50-)))

(:derived (Flag91-)(and (Flag52-)))

(:derived (Flag91-)(and (Flag82-)))

(:derived (Flag91-)(and (Flag53-)))

(:derived (Flag91-)(and (Flag83-)))

(:derived (Flag91-)(and (Flag55-)))

(:derived (Flag91-)(and (Flag56-)))

(:derived (Flag91-)(and (Flag84-)))

(:derived (Flag91-)(and (Flag57-)))

(:derived (Flag91-)(and (Flag85-)))

(:derived (Flag91-)(and (Flag59-)))

(:derived (Flag91-)(and (Flag60-)))

(:derived (Flag91-)(and (Flag58-)))

(:derived (Flag91-)(and (Flag86-)))

(:derived (Flag91-)(and (Flag87-)))

(:derived (Flag91-)(and (Flag62-)))

(:derived (Flag91-)(and (Flag63-)))

(:derived (Flag91-)(and (Flag64-)))

(:derived (Flag91-)(and (Flag65-)))

(:derived (Flag91-)(and (Flag66-)))

(:derived (Flag91-)(and (Flag67-)))

(:derived (Flag91-)(and (Flag88-)))

(:derived (Flag91-)(and (Flag68-)))

(:derived (Flag91-)(and (Flag89-)))

(:derived (Flag91-)(and (Flag90-)))

(:derived (Flag91-)(and (Flag69-)))

(:derived (Flag91-)(and (Flag70-)))

(:derived (Flag91-)(and (Flag71-)))

(:derived (Flag91-)(and (Flag72-)))

(:derived (Flag109-)(and (Flag38-)))

(:derived (Flag109-)(and (Flag78-)))

(:derived (Flag109-)(and (Flag74-)))

(:derived (Flag109-)(and (Flag92-)))

(:derived (Flag109-)(and (Flag93-)))

(:derived (Flag109-)(and (Flag94-)))

(:derived (Flag109-)(and (Flag95-)))

(:derived (Flag109-)(and (Flag96-)))

(:derived (Flag109-)(and (Flag61-)))

(:derived (Flag109-)(and (Flag75-)))

(:derived (Flag109-)(and (Flag76-)))

(:derived (Flag109-)(and (Flag45-)))

(:derived (Flag109-)(and (Flag40-)))

(:derived (Flag109-)(and (Flag51-)))

(:derived (Flag109-)(and (Flag42-)))

(:derived (Flag109-)(and (Flag80-)))

(:derived (Flag109-)(and (Flag43-)))

(:derived (Flag109-)(and (Flag77-)))

(:derived (Flag109-)(and (Flag97-)))

(:derived (Flag109-)(and (Flag44-)))

(:derived (Flag109-)(and (Flag98-)))

(:derived (Flag109-)(and (Flag46-)))

(:derived (Flag109-)(and (Flag79-)))

(:derived (Flag109-)(and (Flag48-)))

(:derived (Flag109-)(and (Flag99-)))

(:derived (Flag109-)(and (Flag65-)))

(:derived (Flag109-)(and (Flag81-)))

(:derived (Flag109-)(and (Flag49-)))

(:derived (Flag109-)(and (Flag50-)))

(:derived (Flag109-)(and (Flag100-)))

(:derived (Flag109-)(and (Flag101-)))

(:derived (Flag109-)(and (Flag102-)))

(:derived (Flag109-)(and (Flag53-)))

(:derived (Flag109-)(and (Flag55-)))

(:derived (Flag109-)(and (Flag56-)))

(:derived (Flag109-)(and (Flag84-)))

(:derived (Flag109-)(and (Flag57-)))

(:derived (Flag109-)(and (Flag85-)))

(:derived (Flag109-)(and (Flag59-)))

(:derived (Flag109-)(and (Flag67-)))

(:derived (Flag109-)(and (Flag60-)))

(:derived (Flag109-)(and (Flag58-)))

(:derived (Flag109-)(and (Flag86-)))

(:derived (Flag109-)(and (Flag103-)))

(:derived (Flag109-)(and (Flag62-)))

(:derived (Flag109-)(and (Flag63-)))

(:derived (Flag109-)(and (Flag64-)))

(:derived (Flag109-)(and (Flag104-)))

(:derived (Flag109-)(and (Flag66-)))

(:derived (Flag109-)(and (Flag105-)))

(:derived (Flag109-)(and (Flag106-)))

(:derived (Flag109-)(and (Flag88-)))

(:derived (Flag109-)(and (Flag107-)))

(:derived (Flag109-)(and (Flag108-)))

(:derived (Flag109-)(and (Flag89-)))

(:derived (Flag109-)(and (Flag52-)))

(:derived (Flag109-)(and (Flag68-)))

(:derived (Flag109-)(and (Flag90-)))

(:derived (Flag109-)(and (Flag69-)))

(:derived (Flag109-)(and (Flag70-)))

(:derived (Flag109-)(and (Flag71-)))

(:derived (Flag109-)(and (Flag72-)))

(:derived (Flag125-)(and (Flag38-)))

(:derived (Flag125-)(and (Flag110-)))

(:derived (Flag125-)(and (Flag78-)))

(:derived (Flag125-)(and (Flag111-)))

(:derived (Flag125-)(and (Flag74-)))

(:derived (Flag125-)(and (Flag92-)))

(:derived (Flag125-)(and (Flag95-)))

(:derived (Flag125-)(and (Flag93-)))

(:derived (Flag125-)(and (Flag106-)))

(:derived (Flag125-)(and (Flag112-)))

(:derived (Flag125-)(and (Flag75-)))

(:derived (Flag125-)(and (Flag113-)))

(:derived (Flag125-)(and (Flag76-)))

(:derived (Flag125-)(and (Flag45-)))

(:derived (Flag125-)(and (Flag114-)))

(:derived (Flag125-)(and (Flag40-)))

(:derived (Flag125-)(and (Flag51-)))

(:derived (Flag125-)(and (Flag42-)))

(:derived (Flag125-)(and (Flag97-)))

(:derived (Flag125-)(and (Flag43-)))

(:derived (Flag125-)(and (Flag77-)))

(:derived (Flag125-)(and (Flag104-)))

(:derived (Flag125-)(and (Flag44-)))

(:derived (Flag125-)(and (Flag98-)))

(:derived (Flag125-)(and (Flag46-)))

(:derived (Flag125-)(and (Flag69-)))

(:derived (Flag125-)(and (Flag79-)))

(:derived (Flag125-)(and (Flag48-)))

(:derived (Flag125-)(and (Flag115-)))

(:derived (Flag125-)(and (Flag99-)))

(:derived (Flag125-)(and (Flag116-)))

(:derived (Flag125-)(and (Flag81-)))

(:derived (Flag125-)(and (Flag49-)))

(:derived (Flag125-)(and (Flag50-)))

(:derived (Flag125-)(and (Flag100-)))

(:derived (Flag125-)(and (Flag101-)))

(:derived (Flag125-)(and (Flag80-)))

(:derived (Flag125-)(and (Flag53-)))

(:derived (Flag125-)(and (Flag117-)))

(:derived (Flag125-)(and (Flag55-)))

(:derived (Flag125-)(and (Flag56-)))

(:derived (Flag125-)(and (Flag84-)))

(:derived (Flag125-)(and (Flag57-)))

(:derived (Flag125-)(and (Flag118-)))

(:derived (Flag125-)(and (Flag85-)))

(:derived (Flag125-)(and (Flag59-)))

(:derived (Flag125-)(and (Flag119-)))

(:derived (Flag125-)(and (Flag67-)))

(:derived (Flag125-)(and (Flag60-)))

(:derived (Flag125-)(and (Flag58-)))

(:derived (Flag125-)(and (Flag61-)))

(:derived (Flag125-)(and (Flag103-)))

(:derived (Flag125-)(and (Flag96-)))

(:derived (Flag125-)(and (Flag63-)))

(:derived (Flag125-)(and (Flag64-)))

(:derived (Flag125-)(and (Flag65-)))

(:derived (Flag125-)(and (Flag66-)))

(:derived (Flag125-)(and (Flag120-)))

(:derived (Flag125-)(and (Flag105-)))

(:derived (Flag125-)(and (Flag88-)))

(:derived (Flag125-)(and (Flag107-)))

(:derived (Flag125-)(and (Flag121-)))

(:derived (Flag125-)(and (Flag108-)))

(:derived (Flag125-)(and (Flag89-)))

(:derived (Flag125-)(and (Flag52-)))

(:derived (Flag125-)(and (Flag68-)))

(:derived (Flag125-)(and (Flag122-)))

(:derived (Flag125-)(and (Flag90-)))

(:derived (Flag125-)(and (Flag123-)))

(:derived (Flag125-)(and (Flag70-)))

(:derived (Flag125-)(and (Flag71-)))

(:derived (Flag125-)(and (Flag86-)))

(:derived (Flag125-)(and (Flag124-)))

(:derived (Flag141-)(and (Flag38-)))

(:derived (Flag141-)(and (Flag108-)))

(:derived (Flag141-)(and (Flag110-)))

(:derived (Flag141-)(and (Flag78-)))

(:derived (Flag141-)(and (Flag74-)))

(:derived (Flag141-)(and (Flag92-)))

(:derived (Flag141-)(and (Flag69-)))

(:derived (Flag141-)(and (Flag126-)))

(:derived (Flag141-)(and (Flag93-)))

(:derived (Flag141-)(and (Flag77-)))

(:derived (Flag141-)(and (Flag48-)))

(:derived (Flag141-)(and (Flag127-)))

(:derived (Flag141-)(and (Flag112-)))

(:derived (Flag141-)(and (Flag128-)))

(:derived (Flag141-)(and (Flag75-)))

(:derived (Flag141-)(and (Flag113-)))

(:derived (Flag141-)(and (Flag76-)))

(:derived (Flag141-)(and (Flag45-)))

(:derived (Flag141-)(and (Flag57-)))

(:derived (Flag141-)(and (Flag129-)))

(:derived (Flag141-)(and (Flag130-)))

(:derived (Flag141-)(and (Flag114-)))

(:derived (Flag141-)(and (Flag40-)))

(:derived (Flag141-)(and (Flag131-)))

(:derived (Flag141-)(and (Flag51-)))

(:derived (Flag141-)(and (Flag42-)))

(:derived (Flag141-)(and (Flag80-)))

(:derived (Flag141-)(and (Flag43-)))

(:derived (Flag141-)(and (Flag86-)))

(:derived (Flag141-)(and (Flag104-)))

(:derived (Flag141-)(and (Flag63-)))

(:derived (Flag141-)(and (Flag44-)))

(:derived (Flag141-)(and (Flag55-)))

(:derived (Flag141-)(and (Flag46-)))

(:derived (Flag141-)(and (Flag121-)))

(:derived (Flag141-)(and (Flag79-)))

(:derived (Flag141-)(and (Flag95-)))

(:derived (Flag141-)(and (Flag132-)))

(:derived (Flag141-)(and (Flag133-)))

(:derived (Flag141-)(and (Flag115-)))

(:derived (Flag141-)(and (Flag99-)))

(:derived (Flag141-)(and (Flag116-)))

(:derived (Flag141-)(and (Flag81-)))

(:derived (Flag141-)(and (Flag49-)))

(:derived (Flag141-)(and (Flag59-)))

(:derived (Flag141-)(and (Flag100-)))

(:derived (Flag141-)(and (Flag101-)))

(:derived (Flag141-)(and (Flag53-)))

(:derived (Flag141-)(and (Flag98-)))

(:derived (Flag141-)(and (Flag56-)))

(:derived (Flag141-)(and (Flag84-)))

(:derived (Flag141-)(and (Flag134-)))

(:derived (Flag141-)(and (Flag58-)))

(:derived (Flag141-)(and (Flag119-)))

(:derived (Flag141-)(and (Flag67-)))

(:derived (Flag141-)(and (Flag60-)))

(:derived (Flag141-)(and (Flag135-)))

(:derived (Flag141-)(and (Flag103-)))

(:derived (Flag141-)(and (Flag136-)))

(:derived (Flag141-)(and (Flag96-)))

(:derived (Flag141-)(and (Flag137-)))

(:derived (Flag141-)(and (Flag64-)))

(:derived (Flag141-)(and (Flag65-)))

(:derived (Flag141-)(and (Flag117-)))

(:derived (Flag141-)(and (Flag120-)))

(:derived (Flag141-)(and (Flag105-)))

(:derived (Flag141-)(and (Flag106-)))

(:derived (Flag141-)(and (Flag88-)))

(:derived (Flag141-)(and (Flag107-)))

(:derived (Flag141-)(and (Flag138-)))

(:derived (Flag141-)(and (Flag89-)))

(:derived (Flag141-)(and (Flag52-)))

(:derived (Flag141-)(and (Flag139-)))

(:derived (Flag141-)(and (Flag68-)))

(:derived (Flag141-)(and (Flag122-)))

(:derived (Flag141-)(and (Flag90-)))

(:derived (Flag141-)(and (Flag123-)))

(:derived (Flag141-)(and (Flag140-)))

(:derived (Flag141-)(and (Flag70-)))

(:derived (Flag141-)(and (Flag71-)))

(:derived (Flag141-)(and (Flag61-)))

(:derived (Flag141-)(and (Flag124-)))

(:derived (Flag156-)(and (Flag142-)))

(:derived (Flag156-)(and (Flag42-)))

(:derived (Flag156-)(and (Flag113-)))

(:derived (Flag156-)(and (Flag44-)))

(:derived (Flag156-)(and (Flag46-)))

(:derived (Flag156-)(and (Flag79-)))

(:derived (Flag156-)(and (Flag95-)))

(:derived (Flag156-)(and (Flag80-)))

(:derived (Flag156-)(and (Flag116-)))

(:derived (Flag156-)(and (Flag122-)))

(:derived (Flag156-)(and (Flag59-)))

(:derived (Flag156-)(and (Flag143-)))

(:derived (Flag156-)(and (Flag144-)))

(:derived (Flag156-)(and (Flag55-)))

(:derived (Flag156-)(and (Flag81-)))

(:derived (Flag156-)(and (Flag86-)))

(:derived (Flag156-)(and (Flag103-)))

(:derived (Flag156-)(and (Flag112-)))

(:derived (Flag156-)(and (Flag63-)))

(:derived (Flag156-)(and (Flag120-)))

(:derived (Flag156-)(and (Flag145-)))

(:derived (Flag156-)(and (Flag121-)))

(:derived (Flag156-)(and (Flag56-)))

(:derived (Flag156-)(and (Flag45-)))

(:derived (Flag156-)(and (Flag93-)))

(:derived (Flag156-)(and (Flag128-)))

(:derived (Flag156-)(and (Flag146-)))

(:derived (Flag156-)(and (Flag76-)))

(:derived (Flag156-)(and (Flag129-)))

(:derived (Flag156-)(and (Flag130-)))

(:derived (Flag156-)(and (Flag78-)))

(:derived (Flag156-)(and (Flag115-)))

(:derived (Flag156-)(and (Flag49-)))

(:derived (Flag156-)(and (Flag147-)))

(:derived (Flag156-)(and (Flag98-)))

(:derived (Flag156-)(and (Flag58-)))

(:derived (Flag156-)(and (Flag60-)))

(:derived (Flag156-)(and (Flag135-)))

(:derived (Flag156-)(and (Flag96-)))

(:derived (Flag156-)(and (Flag64-)))

(:derived (Flag156-)(and (Flag104-)))

(:derived (Flag156-)(and (Flag105-)))

(:derived (Flag156-)(and (Flag68-)))

(:derived (Flag156-)(and (Flag89-)))

(:derived (Flag156-)(and (Flag139-)))

(:derived (Flag156-)(and (Flag123-)))

(:derived (Flag156-)(and (Flag70-)))

(:derived (Flag156-)(and (Flag71-)))

(:derived (Flag156-)(and (Flag38-)))

(:derived (Flag156-)(and (Flag110-)))

(:derived (Flag156-)(and (Flag148-)))

(:derived (Flag156-)(and (Flag126-)))

(:derived (Flag156-)(and (Flag149-)))

(:derived (Flag156-)(and (Flag138-)))

(:derived (Flag156-)(and (Flag114-)))

(:derived (Flag156-)(and (Flag40-)))

(:derived (Flag156-)(and (Flag77-)))

(:derived (Flag156-)(and (Flag92-)))

(:derived (Flag156-)(and (Flag106-)))

(:derived (Flag156-)(and (Flag100-)))

(:derived (Flag156-)(and (Flag52-)))

(:derived (Flag156-)(and (Flag53-)))

(:derived (Flag156-)(and (Flag84-)))

(:derived (Flag156-)(and (Flag57-)))

(:derived (Flag156-)(and (Flag119-)))

(:derived (Flag156-)(and (Flag67-)))

(:derived (Flag156-)(and (Flag136-)))

(:derived (Flag156-)(and (Flag137-)))

(:derived (Flag156-)(and (Flag65-)))

(:derived (Flag156-)(and (Flag117-)))

(:derived (Flag156-)(and (Flag107-)))

(:derived (Flag156-)(and (Flag150-)))

(:derived (Flag156-)(and (Flag151-)))

(:derived (Flag156-)(and (Flag152-)))

(:derived (Flag156-)(and (Flag90-)))

(:derived (Flag156-)(and (Flag140-)))

(:derived (Flag156-)(and (Flag108-)))

(:derived (Flag156-)(and (Flag153-)))

(:derived (Flag156-)(and (Flag131-)))

(:derived (Flag156-)(and (Flag51-)))

(:derived (Flag156-)(and (Flag154-)))

(:derived (Flag156-)(and (Flag43-)))

(:derived (Flag156-)(and (Flag132-)))

(:derived (Flag156-)(and (Flag99-)))

(:derived (Flag156-)(and (Flag124-)))

(:derived (Flag156-)(and (Flag75-)))

(:derived (Flag156-)(and (Flag61-)))

(:derived (Flag156-)(and (Flag155-)))

(:derived (Flag156-)(and (Flag88-)))

(:derived (Flag169-)(and (Flag42-)))

(:derived (Flag169-)(and (Flag113-)))

(:derived (Flag169-)(and (Flag44-)))

(:derived (Flag169-)(and (Flag46-)))

(:derived (Flag169-)(and (Flag79-)))

(:derived (Flag169-)(and (Flag95-)))

(:derived (Flag169-)(and (Flag80-)))

(:derived (Flag169-)(and (Flag116-)))

(:derived (Flag169-)(and (Flag122-)))

(:derived (Flag169-)(and (Flag59-)))

(:derived (Flag169-)(and (Flag143-)))

(:derived (Flag169-)(and (Flag144-)))

(:derived (Flag169-)(and (Flag55-)))

(:derived (Flag169-)(and (Flag81-)))

(:derived (Flag169-)(and (Flag86-)))

(:derived (Flag169-)(and (Flag103-)))

(:derived (Flag169-)(and (Flag112-)))

(:derived (Flag169-)(and (Flag63-)))

(:derived (Flag169-)(and (Flag120-)))

(:derived (Flag169-)(and (Flag121-)))

(:derived (Flag169-)(and (Flag56-)))

(:derived (Flag169-)(and (Flag157-)))

(:derived (Flag169-)(and (Flag158-)))

(:derived (Flag169-)(and (Flag45-)))

(:derived (Flag169-)(and (Flag159-)))

(:derived (Flag169-)(and (Flag93-)))

(:derived (Flag169-)(and (Flag128-)))

(:derived (Flag169-)(and (Flag160-)))

(:derived (Flag169-)(and (Flag146-)))

(:derived (Flag169-)(and (Flag76-)))

(:derived (Flag169-)(and (Flag129-)))

(:derived (Flag169-)(and (Flag130-)))

(:derived (Flag169-)(and (Flag78-)))

(:derived (Flag169-)(and (Flag161-)))

(:derived (Flag169-)(and (Flag115-)))

(:derived (Flag169-)(and (Flag147-)))

(:derived (Flag169-)(and (Flag98-)))

(:derived (Flag169-)(and (Flag58-)))

(:derived (Flag169-)(and (Flag162-)))

(:derived (Flag169-)(and (Flag60-)))

(:derived (Flag169-)(and (Flag135-)))

(:derived (Flag169-)(and (Flag64-)))

(:derived (Flag169-)(and (Flag104-)))

(:derived (Flag169-)(and (Flag105-)))

(:derived (Flag169-)(and (Flag68-)))

(:derived (Flag169-)(and (Flag89-)))

(:derived (Flag169-)(and (Flag139-)))

(:derived (Flag169-)(and (Flag163-)))

(:derived (Flag169-)(and (Flag70-)))

(:derived (Flag169-)(and (Flag71-)))

(:derived (Flag169-)(and (Flag38-)))

(:derived (Flag169-)(and (Flag110-)))

(:derived (Flag169-)(and (Flag148-)))

(:derived (Flag169-)(and (Flag126-)))

(:derived (Flag169-)(and (Flag149-)))

(:derived (Flag169-)(and (Flag138-)))

(:derived (Flag169-)(and (Flag114-)))

(:derived (Flag169-)(and (Flag40-)))

(:derived (Flag169-)(and (Flag77-)))

(:derived (Flag169-)(and (Flag92-)))

(:derived (Flag169-)(and (Flag106-)))

(:derived (Flag169-)(and (Flag100-)))

(:derived (Flag169-)(and (Flag52-)))

(:derived (Flag169-)(and (Flag49-)))

(:derived (Flag169-)(and (Flag164-)))

(:derived (Flag169-)(and (Flag84-)))

(:derived (Flag169-)(and (Flag165-)))

(:derived (Flag169-)(and (Flag67-)))

(:derived (Flag169-)(and (Flag136-)))

(:derived (Flag169-)(and (Flag137-)))

(:derived (Flag169-)(and (Flag65-)))

(:derived (Flag169-)(and (Flag117-)))

(:derived (Flag169-)(and (Flag107-)))

(:derived (Flag169-)(and (Flag150-)))

(:derived (Flag169-)(and (Flag151-)))

(:derived (Flag169-)(and (Flag152-)))

(:derived (Flag169-)(and (Flag90-)))

(:derived (Flag169-)(and (Flag140-)))

(:derived (Flag169-)(and (Flag166-)))

(:derived (Flag169-)(and (Flag108-)))

(:derived (Flag169-)(and (Flag153-)))

(:derived (Flag169-)(and (Flag131-)))

(:derived (Flag169-)(and (Flag51-)))

(:derived (Flag169-)(and (Flag43-)))

(:derived (Flag169-)(and (Flag132-)))

(:derived (Flag169-)(and (Flag99-)))

(:derived (Flag169-)(and (Flag167-)))

(:derived (Flag169-)(and (Flag124-)))

(:derived (Flag169-)(and (Flag61-)))

(:derived (Flag169-)(and (Flag168-)))

(:derived (Flag169-)(and (Flag155-)))

(:derived (Flag169-)(and (Flag119-)))

(:derived (Flag169-)(and (Flag88-)))

(:derived (Flag182-)(and (Flag113-)))

(:derived (Flag182-)(and (Flag44-)))

(:derived (Flag182-)(and (Flag148-)))

(:derived (Flag182-)(and (Flag95-)))

(:derived (Flag182-)(and (Flag80-)))

(:derived (Flag182-)(and (Flag116-)))

(:derived (Flag182-)(and (Flag122-)))

(:derived (Flag182-)(and (Flag59-)))

(:derived (Flag182-)(and (Flag143-)))

(:derived (Flag182-)(and (Flag170-)))

(:derived (Flag182-)(and (Flag144-)))

(:derived (Flag182-)(and (Flag55-)))

(:derived (Flag182-)(and (Flag81-)))

(:derived (Flag182-)(and (Flag86-)))

(:derived (Flag182-)(and (Flag103-)))

(:derived (Flag182-)(and (Flag112-)))

(:derived (Flag182-)(and (Flag63-)))

(:derived (Flag182-)(and (Flag171-)))

(:derived (Flag182-)(and (Flag120-)))

(:derived (Flag182-)(and (Flag172-)))

(:derived (Flag182-)(and (Flag121-)))

(:derived (Flag182-)(and (Flag56-)))

(:derived (Flag182-)(and (Flag157-)))

(:derived (Flag182-)(and (Flag158-)))

(:derived (Flag182-)(and (Flag173-)))

(:derived (Flag182-)(and (Flag45-)))

(:derived (Flag182-)(and (Flag159-)))

(:derived (Flag182-)(and (Flag93-)))

(:derived (Flag182-)(and (Flag128-)))

(:derived (Flag182-)(and (Flag160-)))

(:derived (Flag182-)(and (Flag146-)))

(:derived (Flag182-)(and (Flag174-)))

(:derived (Flag182-)(and (Flag129-)))

(:derived (Flag182-)(and (Flag130-)))

(:derived (Flag182-)(and (Flag78-)))

(:derived (Flag182-)(and (Flag175-)))

(:derived (Flag182-)(and (Flag161-)))

(:derived (Flag182-)(and (Flag115-)))

(:derived (Flag182-)(and (Flag140-)))

(:derived (Flag182-)(and (Flag147-)))

(:derived (Flag182-)(and (Flag98-)))

(:derived (Flag182-)(and (Flag58-)))

(:derived (Flag182-)(and (Flag162-)))

(:derived (Flag182-)(and (Flag60-)))

(:derived (Flag182-)(and (Flag135-)))

(:derived (Flag182-)(and (Flag77-)))

(:derived (Flag182-)(and (Flag64-)))

(:derived (Flag182-)(and (Flag104-)))

(:derived (Flag182-)(and (Flag105-)))

(:derived (Flag182-)(and (Flag68-)))

(:derived (Flag182-)(and (Flag89-)))

(:derived (Flag182-)(and (Flag139-)))

(:derived (Flag182-)(and (Flag176-)))

(:derived (Flag182-)(and (Flag70-)))

(:derived (Flag182-)(and (Flag71-)))

(:derived (Flag182-)(and (Flag38-)))

(:derived (Flag182-)(and (Flag110-)))

(:derived (Flag182-)(and (Flag79-)))

(:derived (Flag182-)(and (Flag126-)))

(:derived (Flag182-)(and (Flag149-)))

(:derived (Flag182-)(and (Flag138-)))

(:derived (Flag182-)(and (Flag177-)))

(:derived (Flag182-)(and (Flag114-)))

(:derived (Flag182-)(and (Flag40-)))

(:derived (Flag182-)(and (Flag178-)))

(:derived (Flag182-)(and (Flag92-)))

(:derived (Flag182-)(and (Flag100-)))

(:derived (Flag182-)(and (Flag52-)))

(:derived (Flag182-)(and (Flag49-)))

(:derived (Flag182-)(and (Flag84-)))

(:derived (Flag182-)(and (Flag67-)))

(:derived (Flag182-)(and (Flag136-)))

(:derived (Flag182-)(and (Flag137-)))

(:derived (Flag182-)(and (Flag65-)))

(:derived (Flag182-)(and (Flag117-)))

(:derived (Flag182-)(and (Flag107-)))

(:derived (Flag182-)(and (Flag150-)))

(:derived (Flag182-)(and (Flag151-)))

(:derived (Flag182-)(and (Flag152-)))

(:derived (Flag182-)(and (Flag90-)))

(:derived (Flag182-)(and (Flag179-)))

(:derived (Flag182-)(and (Flag166-)))

(:derived (Flag182-)(and (Flag108-)))

(:derived (Flag182-)(and (Flag153-)))

(:derived (Flag182-)(and (Flag131-)))

(:derived (Flag182-)(and (Flag51-)))

(:derived (Flag182-)(and (Flag43-)))

(:derived (Flag182-)(and (Flag99-)))

(:derived (Flag182-)(and (Flag167-)))

(:derived (Flag182-)(and (Flag124-)))

(:derived (Flag182-)(and (Flag61-)))

(:derived (Flag182-)(and (Flag168-)))

(:derived (Flag182-)(and (Flag155-)))

(:derived (Flag182-)(and (Flag180-)))

(:derived (Flag182-)(and (Flag88-)))

(:derived (Flag182-)(and (Flag181-)))

(:derived (Flag193-)(and (Flag78-)))

(:derived (Flag193-)(and (Flag44-)))

(:derived (Flag193-)(and (Flag148-)))

(:derived (Flag193-)(and (Flag95-)))

(:derived (Flag193-)(and (Flag116-)))

(:derived (Flag193-)(and (Flag122-)))

(:derived (Flag193-)(and (Flag59-)))

(:derived (Flag193-)(and (Flag143-)))

(:derived (Flag193-)(and (Flag170-)))

(:derived (Flag193-)(and (Flag144-)))

(:derived (Flag193-)(and (Flag183-)))

(:derived (Flag193-)(and (Flag55-)))

(:derived (Flag193-)(and (Flag81-)))

(:derived (Flag193-)(and (Flag184-)))

(:derived (Flag193-)(and (Flag86-)))

(:derived (Flag193-)(and (Flag103-)))

(:derived (Flag193-)(and (Flag112-)))

(:derived (Flag193-)(and (Flag63-)))

(:derived (Flag193-)(and (Flag171-)))

(:derived (Flag193-)(and (Flag120-)))

(:derived (Flag193-)(and (Flag172-)))

(:derived (Flag193-)(and (Flag121-)))

(:derived (Flag193-)(and (Flag56-)))

(:derived (Flag193-)(and (Flag157-)))

(:derived (Flag193-)(and (Flag158-)))

(:derived (Flag193-)(and (Flag173-)))

(:derived (Flag193-)(and (Flag45-)))

(:derived (Flag193-)(and (Flag159-)))

(:derived (Flag193-)(and (Flag93-)))

(:derived (Flag193-)(and (Flag128-)))

(:derived (Flag193-)(and (Flag160-)))

(:derived (Flag193-)(and (Flag146-)))

(:derived (Flag193-)(and (Flag174-)))

(:derived (Flag193-)(and (Flag129-)))

(:derived (Flag193-)(and (Flag130-)))

(:derived (Flag193-)(and (Flag185-)))

(:derived (Flag193-)(and (Flag175-)))

(:derived (Flag193-)(and (Flag161-)))

(:derived (Flag193-)(and (Flag115-)))

(:derived (Flag193-)(and (Flag140-)))

(:derived (Flag193-)(and (Flag147-)))

(:derived (Flag193-)(and (Flag98-)))

(:derived (Flag193-)(and (Flag58-)))

(:derived (Flag193-)(and (Flag162-)))

(:derived (Flag193-)(and (Flag60-)))

(:derived (Flag193-)(and (Flag135-)))

(:derived (Flag193-)(and (Flag64-)))

(:derived (Flag193-)(and (Flag104-)))

(:derived (Flag193-)(and (Flag186-)))

(:derived (Flag193-)(and (Flag105-)))

(:derived (Flag193-)(and (Flag68-)))

(:derived (Flag193-)(and (Flag89-)))

(:derived (Flag193-)(and (Flag139-)))

(:derived (Flag193-)(and (Flag187-)))

(:derived (Flag193-)(and (Flag38-)))

(:derived (Flag193-)(and (Flag110-)))

(:derived (Flag193-)(and (Flag79-)))

(:derived (Flag193-)(and (Flag126-)))

(:derived (Flag193-)(and (Flag149-)))

(:derived (Flag193-)(and (Flag138-)))

(:derived (Flag193-)(and (Flag177-)))

(:derived (Flag193-)(and (Flag114-)))

(:derived (Flag193-)(and (Flag40-)))

(:derived (Flag193-)(and (Flag77-)))

(:derived (Flag193-)(and (Flag188-)))

(:derived (Flag193-)(and (Flag92-)))

(:derived (Flag193-)(and (Flag100-)))

(:derived (Flag193-)(and (Flag52-)))

(:derived (Flag193-)(and (Flag49-)))

(:derived (Flag193-)(and (Flag67-)))

(:derived (Flag193-)(and (Flag137-)))

(:derived (Flag193-)(and (Flag117-)))

(:derived (Flag193-)(and (Flag107-)))

(:derived (Flag193-)(and (Flag150-)))

(:derived (Flag193-)(and (Flag152-)))

(:derived (Flag193-)(and (Flag90-)))

(:derived (Flag193-)(and (Flag179-)))

(:derived (Flag193-)(and (Flag166-)))

(:derived (Flag193-)(and (Flag108-)))

(:derived (Flag193-)(and (Flag153-)))

(:derived (Flag193-)(and (Flag131-)))

(:derived (Flag193-)(and (Flag189-)))

(:derived (Flag193-)(and (Flag43-)))

(:derived (Flag193-)(and (Flag190-)))

(:derived (Flag193-)(and (Flag191-)))

(:derived (Flag193-)(and (Flag80-)))

(:derived (Flag193-)(and (Flag167-)))

(:derived (Flag193-)(and (Flag51-)))

(:derived (Flag193-)(and (Flag124-)))

(:derived (Flag193-)(and (Flag70-)))

(:derived (Flag193-)(and (Flag61-)))

(:derived (Flag193-)(and (Flag168-)))

(:derived (Flag193-)(and (Flag155-)))

(:derived (Flag193-)(and (Flag192-)))

(:derived (Flag193-)(and (Flag88-)))

(:derived (Flag193-)(and (Flag181-)))

(:derived (Flag202-)(and (Flag78-)))

(:derived (Flag202-)(and (Flag44-)))

(:derived (Flag202-)(and (Flag148-)))

(:derived (Flag202-)(and (Flag95-)))

(:derived (Flag202-)(and (Flag116-)))

(:derived (Flag202-)(and (Flag122-)))

(:derived (Flag202-)(and (Flag59-)))

(:derived (Flag202-)(and (Flag143-)))

(:derived (Flag202-)(and (Flag170-)))

(:derived (Flag202-)(and (Flag144-)))

(:derived (Flag202-)(and (Flag183-)))

(:derived (Flag202-)(and (Flag81-)))

(:derived (Flag202-)(and (Flag184-)))

(:derived (Flag202-)(and (Flag86-)))

(:derived (Flag202-)(and (Flag103-)))

(:derived (Flag202-)(and (Flag112-)))

(:derived (Flag202-)(and (Flag171-)))

(:derived (Flag202-)(and (Flag172-)))

(:derived (Flag202-)(and (Flag121-)))

(:derived (Flag202-)(and (Flag56-)))

(:derived (Flag202-)(and (Flag157-)))

(:derived (Flag202-)(and (Flag158-)))

(:derived (Flag202-)(and (Flag173-)))

(:derived (Flag202-)(and (Flag45-)))

(:derived (Flag202-)(and (Flag194-)))

(:derived (Flag202-)(and (Flag159-)))

(:derived (Flag202-)(and (Flag93-)))

(:derived (Flag202-)(and (Flag128-)))

(:derived (Flag202-)(and (Flag160-)))

(:derived (Flag202-)(and (Flag146-)))

(:derived (Flag202-)(and (Flag174-)))

(:derived (Flag202-)(and (Flag129-)))

(:derived (Flag202-)(and (Flag130-)))

(:derived (Flag202-)(and (Flag185-)))

(:derived (Flag202-)(and (Flag175-)))

(:derived (Flag202-)(and (Flag161-)))

(:derived (Flag202-)(and (Flag115-)))

(:derived (Flag202-)(and (Flag140-)))

(:derived (Flag202-)(and (Flag147-)))

(:derived (Flag202-)(and (Flag195-)))

(:derived (Flag202-)(and (Flag98-)))

(:derived (Flag202-)(and (Flag58-)))

(:derived (Flag202-)(and (Flag196-)))

(:derived (Flag202-)(and (Flag60-)))

(:derived (Flag202-)(and (Flag135-)))

(:derived (Flag202-)(and (Flag64-)))

(:derived (Flag202-)(and (Flag104-)))

(:derived (Flag202-)(and (Flag105-)))

(:derived (Flag202-)(and (Flag68-)))

(:derived (Flag202-)(and (Flag89-)))

(:derived (Flag202-)(and (Flag187-)))

(:derived (Flag202-)(and (Flag38-)))

(:derived (Flag202-)(and (Flag110-)))

(:derived (Flag202-)(and (Flag79-)))

(:derived (Flag202-)(and (Flag126-)))

(:derived (Flag202-)(and (Flag149-)))

(:derived (Flag202-)(and (Flag138-)))

(:derived (Flag202-)(and (Flag177-)))

(:derived (Flag202-)(and (Flag114-)))

(:derived (Flag202-)(and (Flag40-)))

(:derived (Flag202-)(and (Flag77-)))

(:derived (Flag202-)(and (Flag188-)))

(:derived (Flag202-)(and (Flag197-)))

(:derived (Flag202-)(and (Flag198-)))

(:derived (Flag202-)(and (Flag52-)))

(:derived (Flag202-)(and (Flag49-)))

(:derived (Flag202-)(and (Flag67-)))

(:derived (Flag202-)(and (Flag117-)))

(:derived (Flag202-)(and (Flag100-)))

(:derived (Flag202-)(and (Flag107-)))

(:derived (Flag202-)(and (Flag150-)))

(:derived (Flag202-)(and (Flag152-)))

(:derived (Flag202-)(and (Flag90-)))

(:derived (Flag202-)(and (Flag179-)))

(:derived (Flag202-)(and (Flag166-)))

(:derived (Flag202-)(and (Flag108-)))

(:derived (Flag202-)(and (Flag199-)))

(:derived (Flag202-)(and (Flag131-)))

(:derived (Flag202-)(and (Flag200-)))

(:derived (Flag202-)(and (Flag51-)))

(:derived (Flag202-)(and (Flag43-)))

(:derived (Flag202-)(and (Flag190-)))

(:derived (Flag202-)(and (Flag191-)))

(:derived (Flag202-)(and (Flag167-)))

(:derived (Flag202-)(and (Flag124-)))

(:derived (Flag202-)(and (Flag70-)))

(:derived (Flag202-)(and (Flag61-)))

(:derived (Flag202-)(and (Flag168-)))

(:derived (Flag202-)(and (Flag155-)))

(:derived (Flag202-)(and (Flag192-)))

(:derived (Flag202-)(and (Flag88-)))

(:derived (Flag202-)(and (Flag201-)))

(:derived (Flag202-)(and (Flag181-)))

(:derived (Flag210-)(and (Flag44-)))

(:derived (Flag210-)(and (Flag79-)))

(:derived (Flag210-)(and (Flag95-)))

(:derived (Flag210-)(and (Flag49-)))

(:derived (Flag210-)(and (Flag59-)))

(:derived (Flag210-)(and (Flag143-)))

(:derived (Flag210-)(and (Flag203-)))

(:derived (Flag210-)(and (Flag170-)))

(:derived (Flag210-)(and (Flag144-)))

(:derived (Flag210-)(and (Flag81-)))

(:derived (Flag210-)(and (Flag184-)))

(:derived (Flag210-)(and (Flag112-)))

(:derived (Flag210-)(and (Flag171-)))

(:derived (Flag210-)(and (Flag172-)))

(:derived (Flag210-)(and (Flag121-)))

(:derived (Flag210-)(and (Flag56-)))

(:derived (Flag210-)(and (Flag157-)))

(:derived (Flag210-)(and (Flag158-)))

(:derived (Flag210-)(and (Flag173-)))

(:derived (Flag210-)(and (Flag45-)))

(:derived (Flag210-)(and (Flag194-)))

(:derived (Flag210-)(and (Flag159-)))

(:derived (Flag210-)(and (Flag93-)))

(:derived (Flag210-)(and (Flag128-)))

(:derived (Flag210-)(and (Flag160-)))

(:derived (Flag210-)(and (Flag146-)))

(:derived (Flag210-)(and (Flag129-)))

(:derived (Flag210-)(and (Flag130-)))

(:derived (Flag210-)(and (Flag204-)))

(:derived (Flag210-)(and (Flag185-)))

(:derived (Flag210-)(and (Flag175-)))

(:derived (Flag210-)(and (Flag115-)))

(:derived (Flag210-)(and (Flag140-)))

(:derived (Flag210-)(and (Flag195-)))

(:derived (Flag210-)(and (Flag98-)))

(:derived (Flag210-)(and (Flag58-)))

(:derived (Flag210-)(and (Flag196-)))

(:derived (Flag210-)(and (Flag135-)))

(:derived (Flag210-)(and (Flag104-)))

(:derived (Flag210-)(and (Flag105-)))

(:derived (Flag210-)(and (Flag205-)))

(:derived (Flag210-)(and (Flag68-)))

(:derived (Flag210-)(and (Flag89-)))

(:derived (Flag210-)(and (Flag187-)))

(:derived (Flag210-)(and (Flag38-)))

(:derived (Flag210-)(and (Flag110-)))

(:derived (Flag210-)(and (Flag126-)))

(:derived (Flag210-)(and (Flag149-)))

(:derived (Flag210-)(and (Flag138-)))

(:derived (Flag210-)(and (Flag177-)))

(:derived (Flag210-)(and (Flag114-)))

(:derived (Flag210-)(and (Flag40-)))

(:derived (Flag210-)(and (Flag77-)))

(:derived (Flag210-)(and (Flag188-)))

(:derived (Flag210-)(and (Flag197-)))

(:derived (Flag210-)(and (Flag198-)))

(:derived (Flag210-)(and (Flag52-)))

(:derived (Flag210-)(and (Flag122-)))

(:derived (Flag210-)(and (Flag206-)))

(:derived (Flag210-)(and (Flag67-)))

(:derived (Flag210-)(and (Flag117-)))

(:derived (Flag210-)(and (Flag100-)))

(:derived (Flag210-)(and (Flag107-)))

(:derived (Flag210-)(and (Flag150-)))

(:derived (Flag210-)(and (Flag152-)))

(:derived (Flag210-)(and (Flag78-)))

(:derived (Flag210-)(and (Flag207-)))

(:derived (Flag210-)(and (Flag179-)))

(:derived (Flag210-)(and (Flag166-)))

(:derived (Flag210-)(and (Flag108-)))

(:derived (Flag210-)(and (Flag131-)))

(:derived (Flag210-)(and (Flag200-)))

(:derived (Flag210-)(and (Flag51-)))

(:derived (Flag210-)(and (Flag208-)))

(:derived (Flag210-)(and (Flag43-)))

(:derived (Flag210-)(and (Flag190-)))

(:derived (Flag210-)(and (Flag191-)))

(:derived (Flag210-)(and (Flag167-)))

(:derived (Flag210-)(and (Flag124-)))

(:derived (Flag210-)(and (Flag70-)))

(:derived (Flag210-)(and (Flag209-)))

(:derived (Flag210-)(and (Flag61-)))

(:derived (Flag210-)(and (Flag168-)))

(:derived (Flag210-)(and (Flag155-)))

(:derived (Flag210-)(and (Flag192-)))

(:derived (Flag210-)(and (Flag88-)))

(:derived (Flag210-)(and (Flag201-)))

(:derived (Flag210-)(and (Flag181-)))

(:derived (Flag218-)(and (Flag150-)))

(:derived (Flag218-)(and (Flag110-)))

(:derived (Flag218-)(and (Flag173-)))

(:derived (Flag218-)(and (Flag211-)))

(:derived (Flag218-)(and (Flag212-)))

(:derived (Flag218-)(and (Flag194-)))

(:derived (Flag218-)(and (Flag159-)))

(:derived (Flag218-)(and (Flag93-)))

(:derived (Flag218-)(and (Flag95-)))

(:derived (Flag218-)(and (Flag138-)))

(:derived (Flag218-)(and (Flag149-)))

(:derived (Flag218-)(and (Flag160-)))

(:derived (Flag218-)(and (Flag146-)))

(:derived (Flag218-)(and (Flag213-)))

(:derived (Flag218-)(and (Flag45-)))

(:derived (Flag218-)(and (Flag144-)))

(:derived (Flag218-)(and (Flag177-)))

(:derived (Flag218-)(and (Flag130-)))

(:derived (Flag218-)(and (Flag204-)))

(:derived (Flag218-)(and (Flag114-)))

(:derived (Flag218-)(and (Flag190-)))

(:derived (Flag218-)(and (Flag51-)))

(:derived (Flag218-)(and (Flag140-)))

(:derived (Flag218-)(and (Flag43-)))

(:derived (Flag218-)(and (Flag77-)))

(:derived (Flag218-)(and (Flag185-)))

(:derived (Flag218-)(and (Flag44-)))

(:derived (Flag218-)(and (Flag197-)))

(:derived (Flag218-)(and (Flag121-)))

(:derived (Flag218-)(and (Flag79-)))

(:derived (Flag218-)(and (Flag175-)))

(:derived (Flag218-)(and (Flag191-)))

(:derived (Flag218-)(and (Flag129-)))

(:derived (Flag218-)(and (Flag115-)))

(:derived (Flag218-)(and (Flag81-)))

(:derived (Flag218-)(and (Flag49-)))

(:derived (Flag218-)(and (Flag59-)))

(:derived (Flag218-)(and (Flag214-)))

(:derived (Flag218-)(and (Flag126-)))

(:derived (Flag218-)(and (Flag143-)))

(:derived (Flag218-)(and (Flag203-)))

(:derived (Flag218-)(and (Flag38-)))

(:derived (Flag218-)(and (Flag170-)))

(:derived (Flag218-)(and (Flag70-)))

(:derived (Flag218-)(and (Flag195-)))

(:derived (Flag218-)(and (Flag98-)))

(:derived (Flag218-)(and (Flag215-)))

(:derived (Flag218-)(and (Flag56-)))

(:derived (Flag218-)(and (Flag100-)))

(:derived (Flag218-)(and (Flag216-)))

(:derived (Flag218-)(and (Flag58-)))

(:derived (Flag218-)(and (Flag196-)))

(:derived (Flag218-)(and (Flag108-)))

(:derived (Flag218-)(and (Flag184-)))

(:derived (Flag218-)(and (Flag188-)))

(:derived (Flag218-)(and (Flag135-)))

(:derived (Flag218-)(and (Flag168-)))

(:derived (Flag218-)(and (Flag112-)))

(:derived (Flag218-)(and (Flag217-)))

(:derived (Flag218-)(and (Flag155-)))

(:derived (Flag218-)(and (Flag198-)))

(:derived (Flag218-)(and (Flag131-)))

(:derived (Flag218-)(and (Flag104-)))

(:derived (Flag218-)(and (Flag171-)))

(:derived (Flag218-)(and (Flag117-)))

(:derived (Flag218-)(and (Flag67-)))

(:derived (Flag218-)(and (Flag205-)))

(:derived (Flag218-)(and (Flag88-)))

(:derived (Flag218-)(and (Flag68-)))

(:derived (Flag218-)(and (Flag201-)))

(:derived (Flag218-)(and (Flag181-)))

(:derived (Flag218-)(and (Flag89-)))

(:derived (Flag218-)(and (Flag52-)))

(:derived (Flag218-)(and (Flag209-)))

(:derived (Flag218-)(and (Flag78-)))

(:derived (Flag218-)(and (Flag207-)))

(:derived (Flag218-)(and (Flag179-)))

(:derived (Flag218-)(and (Flag187-)))

(:derived (Flag218-)(and (Flag166-)))

(:derived (Flag218-)(and (Flag158-)))

(:derived (Flag218-)(and (Flag124-)))

(:derived (Flag225-)(and (Flag38-)))

(:derived (Flag225-)(and (Flag110-)))

(:derived (Flag225-)(and (Flag211-)))

(:derived (Flag225-)(and (Flag45-)))

(:derived (Flag225-)(and (Flag194-)))

(:derived (Flag225-)(and (Flag159-)))

(:derived (Flag225-)(and (Flag93-)))

(:derived (Flag225-)(and (Flag95-)))

(:derived (Flag225-)(and (Flag138-)))

(:derived (Flag225-)(and (Flag149-)))

(:derived (Flag225-)(and (Flag160-)))

(:derived (Flag225-)(and (Flag146-)))

(:derived (Flag225-)(and (Flag144-)))

(:derived (Flag225-)(and (Flag177-)))

(:derived (Flag225-)(and (Flag130-)))

(:derived (Flag225-)(and (Flag204-)))

(:derived (Flag225-)(and (Flag114-)))

(:derived (Flag225-)(and (Flag219-)))

(:derived (Flag225-)(and (Flag190-)))

(:derived (Flag225-)(and (Flag51-)))

(:derived (Flag225-)(and (Flag43-)))

(:derived (Flag225-)(and (Flag77-)))

(:derived (Flag225-)(and (Flag185-)))

(:derived (Flag225-)(and (Flag220-)))

(:derived (Flag225-)(and (Flag44-)))

(:derived (Flag225-)(and (Flag197-)))

(:derived (Flag225-)(and (Flag79-)))

(:derived (Flag225-)(and (Flag175-)))

(:derived (Flag225-)(and (Flag191-)))

(:derived (Flag225-)(and (Flag56-)))

(:derived (Flag225-)(and (Flag221-)))

(:derived (Flag225-)(and (Flag49-)))

(:derived (Flag225-)(and (Flag59-)))

(:derived (Flag225-)(and (Flag214-)))

(:derived (Flag225-)(and (Flag126-)))

(:derived (Flag225-)(and (Flag143-)))

(:derived (Flag225-)(and (Flag203-)))

(:derived (Flag225-)(and (Flag222-)))

(:derived (Flag225-)(and (Flag170-)))

(:derived (Flag225-)(and (Flag70-)))

(:derived (Flag225-)(and (Flag195-)))

(:derived (Flag225-)(and (Flag216-)))

(:derived (Flag225-)(and (Flag81-)))

(:derived (Flag225-)(and (Flag140-)))

(:derived (Flag225-)(and (Flag209-)))

(:derived (Flag225-)(and (Flag58-)))

(:derived (Flag225-)(and (Flag223-)))

(:derived (Flag225-)(and (Flag196-)))

(:derived (Flag225-)(and (Flag108-)))

(:derived (Flag225-)(and (Flag135-)))

(:derived (Flag225-)(and (Flag168-)))

(:derived (Flag225-)(and (Flag112-)))

(:derived (Flag225-)(and (Flag217-)))

(:derived (Flag225-)(and (Flag188-)))

(:derived (Flag225-)(and (Flag198-)))

(:derived (Flag225-)(and (Flag131-)))

(:derived (Flag225-)(and (Flag104-)))

(:derived (Flag225-)(and (Flag171-)))

(:derived (Flag225-)(and (Flag117-)))

(:derived (Flag225-)(and (Flag224-)))

(:derived (Flag225-)(and (Flag205-)))

(:derived (Flag225-)(and (Flag124-)))

(:derived (Flag225-)(and (Flag150-)))

(:derived (Flag225-)(and (Flag89-)))

(:derived (Flag225-)(and (Flag52-)))

(:derived (Flag225-)(and (Flag78-)))

(:derived (Flag225-)(and (Flag207-)))

(:derived (Flag225-)(and (Flag98-)))

(:derived (Flag225-)(and (Flag179-)))

(:derived (Flag225-)(and (Flag187-)))

(:derived (Flag225-)(and (Flag166-)))

(:derived (Flag225-)(and (Flag158-)))

(:derived (Flag230-)(and (Flag38-)))

(:derived (Flag230-)(and (Flag110-)))

(:derived (Flag230-)(and (Flag211-)))

(:derived (Flag230-)(and (Flag45-)))

(:derived (Flag230-)(and (Flag194-)))

(:derived (Flag230-)(and (Flag159-)))

(:derived (Flag230-)(and (Flag95-)))

(:derived (Flag230-)(and (Flag138-)))

(:derived (Flag230-)(and (Flag146-)))

(:derived (Flag230-)(and (Flag144-)))

(:derived (Flag230-)(and (Flag177-)))

(:derived (Flag230-)(and (Flag226-)))

(:derived (Flag230-)(and (Flag204-)))

(:derived (Flag230-)(and (Flag114-)))

(:derived (Flag230-)(and (Flag220-)))

(:derived (Flag230-)(and (Flag190-)))

(:derived (Flag230-)(and (Flag51-)))

(:derived (Flag230-)(and (Flag227-)))

(:derived (Flag230-)(and (Flag43-)))

(:derived (Flag230-)(and (Flag77-)))

(:derived (Flag230-)(and (Flag185-)))

(:derived (Flag230-)(and (Flag219-)))

(:derived (Flag230-)(and (Flag98-)))

(:derived (Flag230-)(and (Flag228-)))

(:derived (Flag230-)(and (Flag79-)))

(:derived (Flag230-)(and (Flag175-)))

(:derived (Flag230-)(and (Flag191-)))

(:derived (Flag230-)(and (Flag49-)))

(:derived (Flag230-)(and (Flag59-)))

(:derived (Flag230-)(and (Flag214-)))

(:derived (Flag230-)(and (Flag126-)))

(:derived (Flag230-)(and (Flag143-)))

(:derived (Flag230-)(and (Flag140-)))

(:derived (Flag230-)(and (Flag222-)))

(:derived (Flag230-)(and (Flag170-)))

(:derived (Flag230-)(and (Flag195-)))

(:derived (Flag230-)(and (Flag216-)))

(:derived (Flag230-)(and (Flag81-)))

(:derived (Flag230-)(and (Flag209-)))

(:derived (Flag230-)(and (Flag58-)))

(:derived (Flag230-)(and (Flag196-)))

(:derived (Flag230-)(and (Flag108-)))

(:derived (Flag230-)(and (Flag135-)))

(:derived (Flag230-)(and (Flag168-)))

(:derived (Flag230-)(and (Flag112-)))

(:derived (Flag230-)(and (Flag217-)))

(:derived (Flag230-)(and (Flag188-)))

(:derived (Flag230-)(and (Flag104-)))

(:derived (Flag230-)(and (Flag117-)))

(:derived (Flag230-)(and (Flag224-)))

(:derived (Flag230-)(and (Flag205-)))

(:derived (Flag230-)(and (Flag198-)))

(:derived (Flag230-)(and (Flag150-)))

(:derived (Flag230-)(and (Flag52-)))

(:derived (Flag230-)(and (Flag78-)))

(:derived (Flag230-)(and (Flag207-)))

(:derived (Flag230-)(and (Flag229-)))

(:derived (Flag230-)(and (Flag179-)))

(:derived (Flag230-)(and (Flag70-)))

(:derived (Flag230-)(and (Flag166-)))

(:derived (Flag230-)(and (Flag158-)))

(:derived (Flag230-)(and (Flag124-)))

(:derived (Flag234-)(and (Flag38-)))

(:derived (Flag234-)(and (Flag110-)))

(:derived (Flag234-)(and (Flag45-)))

(:derived (Flag234-)(and (Flag194-)))

(:derived (Flag234-)(and (Flag159-)))

(:derived (Flag234-)(and (Flag146-)))

(:derived (Flag234-)(and (Flag144-)))

(:derived (Flag234-)(and (Flag177-)))

(:derived (Flag234-)(and (Flag204-)))

(:derived (Flag234-)(and (Flag114-)))

(:derived (Flag234-)(and (Flag220-)))

(:derived (Flag234-)(and (Flag190-)))

(:derived (Flag234-)(and (Flag51-)))

(:derived (Flag234-)(and (Flag227-)))

(:derived (Flag234-)(and (Flag43-)))

(:derived (Flag234-)(and (Flag77-)))

(:derived (Flag234-)(and (Flag98-)))

(:derived (Flag234-)(and (Flag228-)))

(:derived (Flag234-)(and (Flag79-)))

(:derived (Flag234-)(and (Flag95-)))

(:derived (Flag234-)(and (Flag191-)))

(:derived (Flag234-)(and (Flag49-)))

(:derived (Flag234-)(and (Flag59-)))

(:derived (Flag234-)(and (Flag214-)))

(:derived (Flag234-)(and (Flag231-)))

(:derived (Flag234-)(and (Flag140-)))

(:derived (Flag234-)(and (Flag222-)))

(:derived (Flag234-)(and (Flag170-)))

(:derived (Flag234-)(and (Flag232-)))

(:derived (Flag234-)(and (Flag195-)))

(:derived (Flag234-)(and (Flag216-)))

(:derived (Flag234-)(and (Flag81-)))

(:derived (Flag234-)(and (Flag209-)))

(:derived (Flag234-)(and (Flag196-)))

(:derived (Flag234-)(and (Flag135-)))

(:derived (Flag234-)(and (Flag168-)))

(:derived (Flag234-)(and (Flag112-)))

(:derived (Flag234-)(and (Flag217-)))

(:derived (Flag234-)(and (Flag188-)))

(:derived (Flag234-)(and (Flag104-)))

(:derived (Flag234-)(and (Flag117-)))

(:derived (Flag234-)(and (Flag224-)))

(:derived (Flag234-)(and (Flag233-)))

(:derived (Flag234-)(and (Flag205-)))

(:derived (Flag234-)(and (Flag150-)))

(:derived (Flag234-)(and (Flag126-)))

(:derived (Flag234-)(and (Flag229-)))

(:derived (Flag234-)(and (Flag179-)))

(:derived (Flag234-)(and (Flag70-)))

(:derived (Flag234-)(and (Flag166-)))

(:derived (Flag237-)(and (Flag38-)))

(:derived (Flag237-)(and (Flag110-)))

(:derived (Flag237-)(and (Flag45-)))

(:derived (Flag237-)(and (Flag194-)))

(:derived (Flag237-)(and (Flag159-)))

(:derived (Flag237-)(and (Flag146-)))

(:derived (Flag237-)(and (Flag177-)))

(:derived (Flag237-)(and (Flag188-)))

(:derived (Flag237-)(and (Flag235-)))

(:derived (Flag237-)(and (Flag51-)))

(:derived (Flag237-)(and (Flag114-)))

(:derived (Flag237-)(and (Flag77-)))

(:derived (Flag237-)(and (Flag228-)))

(:derived (Flag237-)(and (Flag79-)))

(:derived (Flag237-)(and (Flag95-)))

(:derived (Flag237-)(and (Flag191-)))

(:derived (Flag237-)(and (Flag49-)))

(:derived (Flag237-)(and (Flag59-)))

(:derived (Flag237-)(and (Flag231-)))

(:derived (Flag237-)(and (Flag222-)))

(:derived (Flag237-)(and (Flag170-)))

(:derived (Flag237-)(and (Flag232-)))

(:derived (Flag237-)(and (Flag216-)))

(:derived (Flag237-)(and (Flag209-)))

(:derived (Flag237-)(and (Flag196-)))

(:derived (Flag237-)(and (Flag135-)))

(:derived (Flag237-)(and (Flag168-)))

(:derived (Flag237-)(and (Flag217-)))

(:derived (Flag237-)(and (Flag236-)))

(:derived (Flag237-)(and (Flag117-)))

(:derived (Flag237-)(and (Flag224-)))

(:derived (Flag237-)(and (Flag205-)))

(:derived (Flag237-)(and (Flag150-)))

(:derived (Flag237-)(and (Flag229-)))

(:derived (Flag237-)(and (Flag140-)))

(:derived (Flag239-)(and (Flag110-)))

(:derived (Flag239-)(and (Flag77-)))

(:derived (Flag239-)(and (Flag205-)))

(:derived (Flag239-)(and (Flag216-)))

(:derived (Flag239-)(and (Flag159-)))

(:derived (Flag239-)(and (Flag150-)))

(:derived (Flag239-)(and (Flag95-)))

(:derived (Flag239-)(and (Flag191-)))

(:derived (Flag239-)(and (Flag196-)))

(:derived (Flag239-)(and (Flag229-)))

(:derived (Flag239-)(and (Flag135-)))

(:derived (Flag239-)(and (Flag238-)))

(:derived (Flag239-)(and (Flag59-)))

(:derived (Flag239-)(and (Flag51-)))

(:derived (Flag239-)(and (Flag236-)))

(:derived (Flag239-)(and (Flag231-)))

(:derived (Flag239-)(and (Flag222-)))

(:derived (Flag239-)(and (Flag170-)))

(:derived (Flag240-)(and (LEFTOF1-ROBOT)))

(:derived (Flag240-)(and (LEFTOF2-ROBOT)))

(:derived (Flag241-)(and (LEFTOF1-ROBOT)))

(:derived (Flag241-)(and (LEFTOF3-ROBOT)))

(:derived (Flag241-)(and (LEFTOF2-ROBOT)))

(:derived (Flag242-)(and (LEFTOF1-ROBOT)))

(:derived (Flag242-)(and (LEFTOF3-ROBOT)))

(:derived (Flag242-)(and (LEFTOF4-ROBOT)))

(:derived (Flag242-)(and (LEFTOF2-ROBOT)))

(:derived (Flag243-)(and (LEFTOF5-ROBOT)))

(:derived (Flag243-)(and (LEFTOF1-ROBOT)))

(:derived (Flag243-)(and (LEFTOF3-ROBOT)))

(:derived (Flag243-)(and (LEFTOF4-ROBOT)))

(:derived (Flag243-)(and (LEFTOF2-ROBOT)))

(:derived (Flag244-)(and (LEFTOF2-ROBOT)))

(:derived (Flag244-)(and (LEFTOF1-ROBOT)))

(:derived (Flag244-)(and (LEFTOF6-ROBOT)))

(:derived (Flag244-)(and (LEFTOF5-ROBOT)))

(:derived (Flag244-)(and (LEFTOF3-ROBOT)))

(:derived (Flag244-)(and (LEFTOF4-ROBOT)))

(:derived (Flag245-)(and (LEFTOF2-ROBOT)))

(:derived (Flag245-)(and (LEFTOF7-ROBOT)))

(:derived (Flag245-)(and (LEFTOF1-ROBOT)))

(:derived (Flag245-)(and (LEFTOF6-ROBOT)))

(:derived (Flag245-)(and (LEFTOF5-ROBOT)))

(:derived (Flag245-)(and (LEFTOF3-ROBOT)))

(:derived (Flag245-)(and (LEFTOF4-ROBOT)))

(:derived (Flag246-)(and (LEFTOF2-ROBOT)))

(:derived (Flag246-)(and (LEFTOF7-ROBOT)))

(:derived (Flag246-)(and (LEFTOF1-ROBOT)))

(:derived (Flag246-)(and (LEFTOF6-ROBOT)))

(:derived (Flag246-)(and (LEFTOF5-ROBOT)))

(:derived (Flag246-)(and (LEFTOF3-ROBOT)))

(:derived (Flag246-)(and (LEFTOF4-ROBOT)))

(:derived (Flag246-)(and (LEFTOF8-ROBOT)))

(:derived (Flag247-)(and (LEFTOF9-ROBOT)))

(:derived (Flag247-)(and (LEFTOF2-ROBOT)))

(:derived (Flag247-)(and (LEFTOF7-ROBOT)))

(:derived (Flag247-)(and (LEFTOF1-ROBOT)))

(:derived (Flag247-)(and (LEFTOF6-ROBOT)))

(:derived (Flag247-)(and (LEFTOF5-ROBOT)))

(:derived (Flag247-)(and (LEFTOF3-ROBOT)))

(:derived (Flag247-)(and (LEFTOF4-ROBOT)))

(:derived (Flag247-)(and (LEFTOF8-ROBOT)))

(:derived (Flag248-)(and (LEFTOF10-ROBOT)))

(:derived (Flag248-)(and (LEFTOF9-ROBOT)))

(:derived (Flag248-)(and (LEFTOF2-ROBOT)))

(:derived (Flag248-)(and (LEFTOF7-ROBOT)))

(:derived (Flag248-)(and (LEFTOF1-ROBOT)))

(:derived (Flag248-)(and (LEFTOF6-ROBOT)))

(:derived (Flag248-)(and (LEFTOF5-ROBOT)))

(:derived (Flag248-)(and (LEFTOF3-ROBOT)))

(:derived (Flag248-)(and (LEFTOF4-ROBOT)))

(:derived (Flag248-)(and (LEFTOF8-ROBOT)))

(:derived (Flag249-)(and (LEFTOF10-ROBOT)))

(:derived (Flag249-)(and (LEFTOF11-ROBOT)))

(:derived (Flag249-)(and (LEFTOF9-ROBOT)))

(:derived (Flag249-)(and (LEFTOF2-ROBOT)))

(:derived (Flag249-)(and (LEFTOF7-ROBOT)))

(:derived (Flag249-)(and (LEFTOF1-ROBOT)))

(:derived (Flag249-)(and (LEFTOF6-ROBOT)))

(:derived (Flag249-)(and (LEFTOF5-ROBOT)))

(:derived (Flag249-)(and (LEFTOF3-ROBOT)))

(:derived (Flag249-)(and (LEFTOF4-ROBOT)))

(:derived (Flag249-)(and (LEFTOF8-ROBOT)))

(:derived (Flag250-)(and (LEFTOF10-ROBOT)))

(:derived (Flag250-)(and (LEFTOF11-ROBOT)))

(:derived (Flag250-)(and (LEFTOF9-ROBOT)))

(:derived (Flag250-)(and (LEFTOF2-ROBOT)))

(:derived (Flag250-)(and (LEFTOF7-ROBOT)))

(:derived (Flag250-)(and (LEFTOF1-ROBOT)))

(:derived (Flag250-)(and (LEFTOF12-ROBOT)))

(:derived (Flag250-)(and (LEFTOF6-ROBOT)))

(:derived (Flag250-)(and (LEFTOF5-ROBOT)))

(:derived (Flag250-)(and (LEFTOF3-ROBOT)))

(:derived (Flag250-)(and (LEFTOF4-ROBOT)))

(:derived (Flag250-)(and (LEFTOF8-ROBOT)))

(:derived (Flag251-)(and (LEFTOF10-ROBOT)))

(:derived (Flag251-)(and (LEFTOF11-ROBOT)))

(:derived (Flag251-)(and (LEFTOF9-ROBOT)))

(:derived (Flag251-)(and (LEFTOF2-ROBOT)))

(:derived (Flag251-)(and (LEFTOF13-ROBOT)))

(:derived (Flag251-)(and (LEFTOF1-ROBOT)))

(:derived (Flag251-)(and (LEFTOF12-ROBOT)))

(:derived (Flag251-)(and (LEFTOF6-ROBOT)))

(:derived (Flag251-)(and (LEFTOF5-ROBOT)))

(:derived (Flag251-)(and (LEFTOF3-ROBOT)))

(:derived (Flag251-)(and (LEFTOF4-ROBOT)))

(:derived (Flag251-)(and (LEFTOF7-ROBOT)))

(:derived (Flag251-)(and (LEFTOF8-ROBOT)))

(:derived (Flag252-)(and (LEFTOF10-ROBOT)))

(:derived (Flag252-)(and (LEFTOF11-ROBOT)))

(:derived (Flag252-)(and (LEFTOF9-ROBOT)))

(:derived (Flag252-)(and (LEFTOF2-ROBOT)))

(:derived (Flag252-)(and (LEFTOF13-ROBOT)))

(:derived (Flag252-)(and (LEFTOF1-ROBOT)))

(:derived (Flag252-)(and (LEFTOF12-ROBOT)))

(:derived (Flag252-)(and (LEFTOF6-ROBOT)))

(:derived (Flag252-)(and (LEFTOF5-ROBOT)))

(:derived (Flag252-)(and (LEFTOF14-ROBOT)))

(:derived (Flag252-)(and (LEFTOF3-ROBOT)))

(:derived (Flag252-)(and (LEFTOF4-ROBOT)))

(:derived (Flag252-)(and (LEFTOF7-ROBOT)))

(:derived (Flag252-)(and (LEFTOF8-ROBOT)))

(:derived (Flag253-)(and (LEFTOF15-ROBOT)))

(:derived (Flag253-)(and (LEFTOF10-ROBOT)))

(:derived (Flag253-)(and (LEFTOF11-ROBOT)))

(:derived (Flag253-)(and (LEFTOF9-ROBOT)))

(:derived (Flag253-)(and (LEFTOF2-ROBOT)))

(:derived (Flag253-)(and (LEFTOF13-ROBOT)))

(:derived (Flag253-)(and (LEFTOF1-ROBOT)))

(:derived (Flag253-)(and (LEFTOF12-ROBOT)))

(:derived (Flag253-)(and (LEFTOF6-ROBOT)))

(:derived (Flag253-)(and (LEFTOF5-ROBOT)))

(:derived (Flag253-)(and (LEFTOF14-ROBOT)))

(:derived (Flag253-)(and (LEFTOF3-ROBOT)))

(:derived (Flag253-)(and (LEFTOF4-ROBOT)))

(:derived (Flag253-)(and (LEFTOF7-ROBOT)))

(:derived (Flag253-)(and (LEFTOF8-ROBOT)))

(:derived (Flag254-)(and (LEFTOF15-ROBOT)))

(:derived (Flag254-)(and (LEFTOF10-ROBOT)))

(:derived (Flag254-)(and (LEFTOF11-ROBOT)))

(:derived (Flag254-)(and (LEFTOF9-ROBOT)))

(:derived (Flag254-)(and (LEFTOF2-ROBOT)))

(:derived (Flag254-)(and (LEFTOF13-ROBOT)))

(:derived (Flag254-)(and (LEFTOF1-ROBOT)))

(:derived (Flag254-)(and (LEFTOF12-ROBOT)))

(:derived (Flag254-)(and (LEFTOF6-ROBOT)))

(:derived (Flag254-)(and (LEFTOF5-ROBOT)))

(:derived (Flag254-)(and (LEFTOF16-ROBOT)))

(:derived (Flag254-)(and (LEFTOF14-ROBOT)))

(:derived (Flag254-)(and (LEFTOF3-ROBOT)))

(:derived (Flag254-)(and (LEFTOF4-ROBOT)))

(:derived (Flag254-)(and (LEFTOF7-ROBOT)))

(:derived (Flag254-)(and (LEFTOF8-ROBOT)))

(:derived (Flag255-)(and (LEFTOF15-ROBOT)))

(:derived (Flag255-)(and (LEFTOF10-ROBOT)))

(:derived (Flag255-)(and (LEFTOF11-ROBOT)))

(:derived (Flag255-)(and (LEFTOF9-ROBOT)))

(:derived (Flag255-)(and (LEFTOF2-ROBOT)))

(:derived (Flag255-)(and (LEFTOF13-ROBOT)))

(:derived (Flag255-)(and (LEFTOF1-ROBOT)))

(:derived (Flag255-)(and (LEFTOF12-ROBOT)))

(:derived (Flag255-)(and (LEFTOF6-ROBOT)))

(:derived (Flag255-)(and (LEFTOF5-ROBOT)))

(:derived (Flag255-)(and (LEFTOF16-ROBOT)))

(:derived (Flag255-)(and (LEFTOF14-ROBOT)))

(:derived (Flag255-)(and (LEFTOF3-ROBOT)))

(:derived (Flag255-)(and (LEFTOF4-ROBOT)))

(:derived (Flag255-)(and (LEFTOF7-ROBOT)))

(:derived (Flag255-)(and (LEFTOF8-ROBOT)))

(:derived (Flag256-)(and (LEFTOF15-ROBOT)))

(:derived (Flag256-)(and (LEFTOF10-ROBOT)))

(:derived (Flag256-)(and (LEFTOF18-ROBOT)))

(:derived (Flag256-)(and (LEFTOF11-ROBOT)))

(:derived (Flag256-)(and (LEFTOF9-ROBOT)))

(:derived (Flag256-)(and (LEFTOF2-ROBOT)))

(:derived (Flag256-)(and (LEFTOF13-ROBOT)))

(:derived (Flag256-)(and (LEFTOF1-ROBOT)))

(:derived (Flag256-)(and (LEFTOF12-ROBOT)))

(:derived (Flag256-)(and (LEFTOF6-ROBOT)))

(:derived (Flag256-)(and (LEFTOF5-ROBOT)))

(:derived (Flag256-)(and (LEFTOF16-ROBOT)))

(:derived (Flag256-)(and (LEFTOF14-ROBOT)))

(:derived (Flag256-)(and (LEFTOF3-ROBOT)))

(:derived (Flag256-)(and (LEFTOF4-ROBOT)))

(:derived (Flag256-)(and (LEFTOF7-ROBOT)))

(:derived (Flag256-)(and (LEFTOF8-ROBOT)))

(:derived (Flag257-)(and (RIGHTOF3-ROBOT)))

(:derived (Flag257-)(and (RIGHTOF5-ROBOT)))

(:derived (Flag257-)(and (RIGHTOF9-ROBOT)))

(:derived (Flag257-)(and (RIGHTOF17-ROBOT)))

(:derived (Flag257-)(and (RIGHTOF14-ROBOT)))

(:derived (Flag257-)(and (RIGHTOF13-ROBOT)))

(:derived (Flag257-)(and (RIGHTOF12-ROBOT)))

(:derived (Flag257-)(and (RIGHTOF10-ROBOT)))

(:derived (Flag257-)(and (RIGHTOF8-ROBOT)))

(:derived (Flag257-)(and (RIGHTOF4-ROBOT)))

(:derived (Flag257-)(and (RIGHTOF1-ROBOT)))

(:derived (Flag257-)(and (RIGHTOF16-ROBOT)))

(:derived (Flag257-)(and (RIGHTOF7-ROBOT)))

(:derived (Flag257-)(and (RIGHTOF11-ROBOT)))

(:derived (Flag257-)(and (RIGHTOF15-ROBOT)))

(:derived (Flag257-)(and (RIGHTOF6-ROBOT)))

(:derived (Flag258-)(and (RIGHTOF3-ROBOT)))

(:derived (Flag258-)(and (RIGHTOF5-ROBOT)))

(:derived (Flag258-)(and (RIGHTOF9-ROBOT)))

(:derived (Flag258-)(and (RIGHTOF17-ROBOT)))

(:derived (Flag258-)(and (RIGHTOF14-ROBOT)))

(:derived (Flag258-)(and (RIGHTOF13-ROBOT)))

(:derived (Flag258-)(and (RIGHTOF12-ROBOT)))

(:derived (Flag258-)(and (RIGHTOF10-ROBOT)))

(:derived (Flag258-)(and (RIGHTOF4-ROBOT)))

(:derived (Flag258-)(and (RIGHTOF8-ROBOT)))

(:derived (Flag258-)(and (RIGHTOF16-ROBOT)))

(:derived (Flag258-)(and (RIGHTOF7-ROBOT)))

(:derived (Flag258-)(and (RIGHTOF11-ROBOT)))

(:derived (Flag258-)(and (RIGHTOF15-ROBOT)))

(:derived (Flag258-)(and (RIGHTOF6-ROBOT)))

(:derived (Flag259-)(and (RIGHTOF3-ROBOT)))

(:derived (Flag259-)(and (RIGHTOF5-ROBOT)))

(:derived (Flag259-)(and (RIGHTOF9-ROBOT)))

(:derived (Flag259-)(and (RIGHTOF17-ROBOT)))

(:derived (Flag259-)(and (RIGHTOF14-ROBOT)))

(:derived (Flag259-)(and (RIGHTOF13-ROBOT)))

(:derived (Flag259-)(and (RIGHTOF12-ROBOT)))

(:derived (Flag259-)(and (RIGHTOF10-ROBOT)))

(:derived (Flag259-)(and (RIGHTOF4-ROBOT)))

(:derived (Flag259-)(and (RIGHTOF8-ROBOT)))

(:derived (Flag259-)(and (RIGHTOF16-ROBOT)))

(:derived (Flag259-)(and (RIGHTOF7-ROBOT)))

(:derived (Flag259-)(and (RIGHTOF11-ROBOT)))

(:derived (Flag259-)(and (RIGHTOF15-ROBOT)))

(:derived (Flag259-)(and (RIGHTOF6-ROBOT)))

(:derived (Flag260-)(and (RIGHTOF5-ROBOT)))

(:derived (Flag260-)(and (RIGHTOF9-ROBOT)))

(:derived (Flag260-)(and (RIGHTOF17-ROBOT)))

(:derived (Flag260-)(and (RIGHTOF14-ROBOT)))

(:derived (Flag260-)(and (RIGHTOF13-ROBOT)))

(:derived (Flag260-)(and (RIGHTOF12-ROBOT)))

(:derived (Flag260-)(and (RIGHTOF10-ROBOT)))

(:derived (Flag260-)(and (RIGHTOF4-ROBOT)))

(:derived (Flag260-)(and (RIGHTOF8-ROBOT)))

(:derived (Flag260-)(and (RIGHTOF16-ROBOT)))

(:derived (Flag260-)(and (RIGHTOF7-ROBOT)))

(:derived (Flag260-)(and (RIGHTOF11-ROBOT)))

(:derived (Flag260-)(and (RIGHTOF15-ROBOT)))

(:derived (Flag260-)(and (RIGHTOF6-ROBOT)))

(:derived (Flag261-)(and (RIGHTOF5-ROBOT)))

(:derived (Flag261-)(and (RIGHTOF9-ROBOT)))

(:derived (Flag261-)(and (RIGHTOF17-ROBOT)))

(:derived (Flag261-)(and (RIGHTOF14-ROBOT)))

(:derived (Flag261-)(and (RIGHTOF13-ROBOT)))

(:derived (Flag261-)(and (RIGHTOF12-ROBOT)))

(:derived (Flag261-)(and (RIGHTOF10-ROBOT)))

(:derived (Flag261-)(and (RIGHTOF8-ROBOT)))

(:derived (Flag261-)(and (RIGHTOF16-ROBOT)))

(:derived (Flag261-)(and (RIGHTOF7-ROBOT)))

(:derived (Flag261-)(and (RIGHTOF11-ROBOT)))

(:derived (Flag261-)(and (RIGHTOF15-ROBOT)))

(:derived (Flag261-)(and (RIGHTOF6-ROBOT)))

(:derived (Flag262-)(and (RIGHTOF9-ROBOT)))

(:derived (Flag262-)(and (RIGHTOF17-ROBOT)))

(:derived (Flag262-)(and (RIGHTOF14-ROBOT)))

(:derived (Flag262-)(and (RIGHTOF13-ROBOT)))

(:derived (Flag262-)(and (RIGHTOF12-ROBOT)))

(:derived (Flag262-)(and (RIGHTOF10-ROBOT)))

(:derived (Flag262-)(and (RIGHTOF8-ROBOT)))

(:derived (Flag262-)(and (RIGHTOF16-ROBOT)))

(:derived (Flag262-)(and (RIGHTOF7-ROBOT)))

(:derived (Flag262-)(and (RIGHTOF11-ROBOT)))

(:derived (Flag262-)(and (RIGHTOF15-ROBOT)))

(:derived (Flag262-)(and (RIGHTOF6-ROBOT)))

(:derived (Flag263-)(and (RIGHTOF17-ROBOT)))

(:derived (Flag263-)(and (RIGHTOF14-ROBOT)))

(:derived (Flag263-)(and (RIGHTOF13-ROBOT)))

(:derived (Flag263-)(and (RIGHTOF15-ROBOT)))

(:derived (Flag263-)(and (RIGHTOF10-ROBOT)))

(:derived (Flag263-)(and (RIGHTOF8-ROBOT)))

(:derived (Flag263-)(and (RIGHTOF12-ROBOT)))

(:derived (Flag263-)(and (RIGHTOF16-ROBOT)))

(:derived (Flag263-)(and (RIGHTOF7-ROBOT)))

(:derived (Flag263-)(and (RIGHTOF9-ROBOT)))

(:derived (Flag263-)(and (RIGHTOF11-ROBOT)))

(:derived (Flag264-)(and (RIGHTOF17-ROBOT)))

(:derived (Flag264-)(and (RIGHTOF14-ROBOT)))

(:derived (Flag264-)(and (RIGHTOF13-ROBOT)))

(:derived (Flag264-)(and (RIGHTOF15-ROBOT)))

(:derived (Flag264-)(and (RIGHTOF10-ROBOT)))

(:derived (Flag264-)(and (RIGHTOF8-ROBOT)))

(:derived (Flag264-)(and (RIGHTOF16-ROBOT)))

(:derived (Flag264-)(and (RIGHTOF12-ROBOT)))

(:derived (Flag264-)(and (RIGHTOF9-ROBOT)))

(:derived (Flag264-)(and (RIGHTOF11-ROBOT)))

(:derived (Flag265-)(and (RIGHTOF17-ROBOT)))

(:derived (Flag265-)(and (RIGHTOF14-ROBOT)))

(:derived (Flag265-)(and (RIGHTOF13-ROBOT)))

(:derived (Flag265-)(and (RIGHTOF15-ROBOT)))

(:derived (Flag265-)(and (RIGHTOF10-ROBOT)))

(:derived (Flag265-)(and (RIGHTOF16-ROBOT)))

(:derived (Flag265-)(and (RIGHTOF12-ROBOT)))

(:derived (Flag265-)(and (RIGHTOF9-ROBOT)))

(:derived (Flag265-)(and (RIGHTOF11-ROBOT)))

(:derived (Flag266-)(and (RIGHTOF17-ROBOT)))

(:derived (Flag266-)(and (RIGHTOF14-ROBOT)))

(:derived (Flag266-)(and (RIGHTOF13-ROBOT)))

(:derived (Flag266-)(and (RIGHTOF15-ROBOT)))

(:derived (Flag266-)(and (RIGHTOF10-ROBOT)))

(:derived (Flag266-)(and (RIGHTOF16-ROBOT)))

(:derived (Flag266-)(and (RIGHTOF12-ROBOT)))

(:derived (Flag266-)(and (RIGHTOF11-ROBOT)))

(:derived (Flag267-)(and (RIGHTOF17-ROBOT)))

(:derived (Flag267-)(and (RIGHTOF14-ROBOT)))

(:derived (Flag267-)(and (RIGHTOF13-ROBOT)))

(:derived (Flag267-)(and (RIGHTOF15-ROBOT)))

(:derived (Flag267-)(and (RIGHTOF16-ROBOT)))

(:derived (Flag267-)(and (RIGHTOF12-ROBOT)))

(:derived (Flag267-)(and (RIGHTOF11-ROBOT)))

(:derived (Flag268-)(and (RIGHTOF17-ROBOT)))

(:derived (Flag268-)(and (RIGHTOF14-ROBOT)))

(:derived (Flag268-)(and (RIGHTOF13-ROBOT)))

(:derived (Flag268-)(and (RIGHTOF12-ROBOT)))

(:derived (Flag268-)(and (RIGHTOF16-ROBOT)))

(:derived (Flag268-)(and (RIGHTOF15-ROBOT)))

(:derived (Flag269-)(and (RIGHTOF16-ROBOT)))

(:derived (Flag269-)(and (RIGHTOF17-ROBOT)))

(:derived (Flag269-)(and (RIGHTOF14-ROBOT)))

(:derived (Flag269-)(and (RIGHTOF13-ROBOT)))

(:derived (Flag269-)(and (RIGHTOF15-ROBOT)))

(:derived (Flag270-)(and (RIGHTOF16-ROBOT)))

(:derived (Flag270-)(and (RIGHTOF17-ROBOT)))

(:derived (Flag270-)(and (RIGHTOF14-ROBOT)))

(:derived (Flag270-)(and (RIGHTOF15-ROBOT)))

(:derived (Flag271-)(and (RIGHTOF16-ROBOT)))

(:derived (Flag271-)(and (RIGHTOF17-ROBOT)))

(:derived (Flag271-)(and (RIGHTOF15-ROBOT)))

(:derived (Flag272-)(and (RIGHTOF16-ROBOT)))

(:derived (Flag272-)(and (RIGHTOF17-ROBOT)))

(:derived (Flag273-)(and (LEFTOF2-ROBOT)(RIGHTOF15-ROBOT)))

(:derived (Flag274-)(and (RIGHTOF13-ROBOT)(LEFTOF2-ROBOT)))

(:derived (Flag275-)(and (LEFTOF2-ROBOT)(RIGHTOF9-ROBOT)))

(:derived (Flag276-)(and (LEFTOF1-ROBOT)(RIGHTOF9-ROBOT)))

(:derived (Flag277-)(and (RIGHTOF3-ROBOT)(LEFTOF1-ROBOT)))

(:derived (Flag278-)(and (LEFTOF1-ROBOT)(RIGHTOF15-ROBOT)))

(:derived (Flag279-)(and (RIGHTOF10-ROBOT)(LEFTOF1-ROBOT)))

(:derived (Flag280-)(and (LEFTOF2-ROBOT)(RIGHTOF1-ROBOT)))

(:derived (Flag281-)(and (RIGHTOF5-ROBOT)(LEFTOF2-ROBOT)))

(:derived (Flag282-)(and (LEFTOF1-ROBOT)(RIGHTOF1-ROBOT)))

(:derived (Flag283-)(and (LEFTOF2-ROBOT)(RIGHTOF7-ROBOT)))

(:derived (Flag284-)(and (RIGHTOF16-ROBOT)(LEFTOF2-ROBOT)))

(:derived (Flag285-)(and (RIGHTOF5-ROBOT)(LEFTOF1-ROBOT)))

(:derived (Flag286-)(and (RIGHTOF11-ROBOT)(LEFTOF1-ROBOT)))

(:derived (Flag287-)(and (RIGHTOF3-ROBOT)(LEFTOF2-ROBOT)))

(:derived (Flag288-)(and (LEFTOF2-ROBOT)(RIGHTOF17-ROBOT)))

(:derived (Flag289-)(and (LEFTOF2-ROBOT)(RIGHTOF14-ROBOT)))

(:derived (Flag290-)(and (LEFTOF1-ROBOT)(RIGHTOF4-ROBOT)))

(:derived (Flag291-)(and (RIGHTOF10-ROBOT)(LEFTOF2-ROBOT)))

(:derived (Flag292-)(and (LEFTOF1-ROBOT)(RIGHTOF12-ROBOT)))

(:derived (Flag293-)(and (LEFTOF1-ROBOT)(RIGHTOF7-ROBOT)))

(:derived (Flag294-)(and (LEFTOF2-ROBOT)(RIGHTOF12-ROBOT)))

(:derived (Flag295-)(and (LEFTOF1-ROBOT)(RIGHTOF5-ROBOT)))

(:derived (Flag296-)(and (RIGHTOF8-ROBOT)(LEFTOF1-ROBOT)))

(:derived (Flag297-)(and (LEFTOF1-ROBOT)(RIGHTOF17-ROBOT)))

(:derived (Flag298-)(and (RIGHTOF13-ROBOT)(LEFTOF1-ROBOT)))

(:derived (Flag299-)(and (RIGHTOF11-ROBOT)(LEFTOF2-ROBOT)))

(:derived (Flag300-)(and (RIGHTOF8-ROBOT)(LEFTOF2-ROBOT)))

(:derived (Flag301-)(and (RIGHTOF2-ROBOT)(LEFTOF2-ROBOT)))

(:derived (Flag302-)(and (LEFTOF1-ROBOT)(RIGHTOF2-ROBOT)))

(:derived (Flag303-)(and (LEFTOF1-ROBOT)(RIGHTOF6-ROBOT)))

(:derived (Flag304-)(and (LEFTOF1-ROBOT)(RIGHTOF14-ROBOT)))

(:derived (Flag305-)(and (LEFTOF1-ROBOT)(RIGHTOF16-ROBOT)))

(:derived (Flag306-)(and (LEFTOF2-ROBOT)(RIGHTOF6-ROBOT)))

(:derived (Flag307-)(and (LEFTOF2-ROBOT)(RIGHTOF4-ROBOT)))

(:derived (Flag309-)(and (LEFTOF3-ROBOT)(RIGHTOF6-ROBOT)))

(:derived (Flag310-)(and (LEFTOF3-ROBOT)(RIGHTOF14-ROBOT)))

(:derived (Flag311-)(and (LEFTOF3-ROBOT)(RIGHTOF4-ROBOT)))

(:derived (Flag312-)(and (LEFTOF3-ROBOT)(RIGHTOF16-ROBOT)))

(:derived (Flag313-)(and (LEFTOF3-ROBOT)(RIGHTOF7-ROBOT)))

(:derived (Flag314-)(and (LEFTOF3-ROBOT)(RIGHTOF12-ROBOT)))

(:derived (Flag315-)(and (LEFTOF3-ROBOT)(RIGHTOF10-ROBOT)))

(:derived (Flag316-)(and (LEFTOF3-ROBOT)(RIGHTOF8-ROBOT)))

(:derived (Flag317-)(and (LEFTOF3-ROBOT)(RIGHTOF5-ROBOT)))

(:derived (Flag318-)(and (LEFTOF3-ROBOT)(RIGHTOF15-ROBOT)))

(:derived (Flag319-)(and (RIGHTOF3-ROBOT)(LEFTOF3-ROBOT)))

(:derived (Flag320-)(and (LEFTOF3-ROBOT)(RIGHTOF11-ROBOT)))

(:derived (Flag321-)(and (RIGHTOF13-ROBOT)(LEFTOF3-ROBOT)))

(:derived (Flag322-)(and (LEFTOF3-ROBOT)(RIGHTOF17-ROBOT)))

(:derived (Flag323-)(and (LEFTOF3-ROBOT)(RIGHTOF9-ROBOT)))

(:derived (Flag324-)(and (LEFTOF3-ROBOT)(RIGHTOF2-ROBOT)))

(:derived (Flag326-)(and (LEFTOF3-ROBOT)(RIGHTOF13-ROBOT)))

(:derived (Flag327-)(and (LEFTOF4-ROBOT)(RIGHTOF17-ROBOT)))

(:derived (Flag328-)(and (LEFTOF4-ROBOT)(RIGHTOF4-ROBOT)))

(:derived (Flag329-)(and (LEFTOF4-ROBOT)(RIGHTOF7-ROBOT)))

(:derived (Flag330-)(and (LEFTOF4-ROBOT)(RIGHTOF15-ROBOT)))

(:derived (Flag331-)(and (LEFTOF4-ROBOT)(RIGHTOF14-ROBOT)))

(:derived (Flag332-)(and (LEFTOF3-ROBOT)(RIGHTOF3-ROBOT)))

(:derived (Flag333-)(and (RIGHTOF8-ROBOT)(LEFTOF4-ROBOT)))

(:derived (Flag334-)(and (LEFTOF4-ROBOT)(RIGHTOF12-ROBOT)))

(:derived (Flag335-)(and (RIGHTOF10-ROBOT)(LEFTOF4-ROBOT)))

(:derived (Flag336-)(and (LEFTOF4-ROBOT)(RIGHTOF9-ROBOT)))

(:derived (Flag337-)(and (LEFTOF4-ROBOT)(RIGHTOF16-ROBOT)))

(:derived (Flag338-)(and (LEFTOF4-ROBOT)(RIGHTOF6-ROBOT)))

(:derived (Flag339-)(and (LEFTOF4-ROBOT)(RIGHTOF5-ROBOT)))

(:derived (Flag340-)(and (RIGHTOF13-ROBOT)(LEFTOF4-ROBOT)))

(:derived (Flag341-)(and (RIGHTOF3-ROBOT)(LEFTOF4-ROBOT)))

(:derived (Flag342-)(and (LEFTOF4-ROBOT)(RIGHTOF11-ROBOT)))

(:derived (Flag344-)(and (RIGHTOF9-ROBOT)(LEFTOF5-ROBOT)))

(:derived (Flag345-)(and (RIGHTOF15-ROBOT)(LEFTOF5-ROBOT)))

(:derived (Flag346-)(and (RIGHTOF4-ROBOT)(LEFTOF5-ROBOT)))

(:derived (Flag347-)(and (RIGHTOF7-ROBOT)(LEFTOF5-ROBOT)))

(:derived (Flag348-)(and (RIGHTOF11-ROBOT)(LEFTOF5-ROBOT)))

(:derived (Flag349-)(and (RIGHTOF13-ROBOT)(LEFTOF5-ROBOT)))

(:derived (Flag350-)(and (RIGHTOF16-ROBOT)(LEFTOF5-ROBOT)))

(:derived (Flag351-)(and (LEFTOF4-ROBOT)(RIGHTOF8-ROBOT)))

(:derived (Flag352-)(and (LEFTOF5-ROBOT)(RIGHTOF6-ROBOT)))

(:derived (Flag353-)(and (RIGHTOF10-ROBOT)(LEFTOF5-ROBOT)))

(:derived (Flag354-)(and (LEFTOF4-ROBOT)(RIGHTOF10-ROBOT)))

(:derived (Flag355-)(and (LEFTOF5-ROBOT)(RIGHTOF12-ROBOT)))

(:derived (Flag356-)(and (RIGHTOF8-ROBOT)(LEFTOF5-ROBOT)))

(:derived (Flag357-)(and (RIGHTOF14-ROBOT)(LEFTOF5-ROBOT)))

(:derived (Flag358-)(and (RIGHTOF5-ROBOT)(LEFTOF5-ROBOT)))

(:derived (Flag359-)(and (RIGHTOF17-ROBOT)(LEFTOF5-ROBOT)))

(:derived (Flag361-)(and (RIGHTOF16-ROBOT)(LEFTOF6-ROBOT)))

(:derived (Flag362-)(and (RIGHTOF15-ROBOT)(LEFTOF6-ROBOT)))

(:derived (Flag363-)(and (RIGHTOF12-ROBOT)(LEFTOF6-ROBOT)))

(:derived (Flag364-)(and (RIGHTOF8-ROBOT)(LEFTOF6-ROBOT)))

(:derived (Flag365-)(and (RIGHTOF11-ROBOT)(LEFTOF6-ROBOT)))

(:derived (Flag366-)(and (RIGHTOF17-ROBOT)(LEFTOF6-ROBOT)))

(:derived (Flag367-)(and (RIGHTOF5-ROBOT)(LEFTOF6-ROBOT)))

(:derived (Flag368-)(and (RIGHTOF7-ROBOT)(LEFTOF6-ROBOT)))

(:derived (Flag369-)(and (RIGHTOF10-ROBOT)(LEFTOF6-ROBOT)))

(:derived (Flag370-)(and (RIGHTOF6-ROBOT)(LEFTOF6-ROBOT)))

(:derived (Flag371-)(and (RIGHTOF13-ROBOT)(LEFTOF6-ROBOT)))

(:derived (Flag372-)(and (RIGHTOF14-ROBOT)(LEFTOF6-ROBOT)))

(:derived (Flag373-)(and (RIGHTOF9-ROBOT)(LEFTOF6-ROBOT)))

(:derived (Flag375-)(and (LEFTOF7-ROBOT)(RIGHTOF7-ROBOT)))

(:derived (Flag376-)(and (RIGHTOF13-ROBOT)(LEFTOF7-ROBOT)))

(:derived (Flag377-)(and (LEFTOF7-ROBOT)(RIGHTOF9-ROBOT)))

(:derived (Flag378-)(and (LEFTOF6-ROBOT)(RIGHTOF6-ROBOT)))

(:derived (Flag379-)(and (LEFTOF7-ROBOT)(RIGHTOF12-ROBOT)))

(:derived (Flag380-)(and (LEFTOF7-ROBOT)(RIGHTOF6-ROBOT)))

(:derived (Flag381-)(and (LEFTOF6-ROBOT)(RIGHTOF12-ROBOT)))

(:derived (Flag382-)(and (LEFTOF7-ROBOT)(RIGHTOF15-ROBOT)))

(:derived (Flag383-)(and (RIGHTOF11-ROBOT)(LEFTOF7-ROBOT)))

(:derived (Flag384-)(and (RIGHTOF8-ROBOT)(LEFTOF7-ROBOT)))

(:derived (Flag385-)(and (RIGHTOF17-ROBOT)(LEFTOF7-ROBOT)))

(:derived (Flag386-)(and (RIGHTOF16-ROBOT)(LEFTOF7-ROBOT)))

(:derived (Flag387-)(and (LEFTOF7-ROBOT)(RIGHTOF14-ROBOT)))

(:derived (Flag388-)(and (RIGHTOF10-ROBOT)(LEFTOF7-ROBOT)))

(:derived (Flag390-)(and (RIGHTOF10-ROBOT)(LEFTOF8-ROBOT)))

(:derived (Flag391-)(and (RIGHTOF16-ROBOT)(LEFTOF8-ROBOT)))

(:derived (Flag392-)(and (RIGHTOF13-ROBOT)(LEFTOF8-ROBOT)))

(:derived (Flag393-)(and (RIGHTOF7-ROBOT)(LEFTOF7-ROBOT)))

(:derived (Flag394-)(and (LEFTOF8-ROBOT)(RIGHTOF15-ROBOT)))

(:derived (Flag395-)(and (LEFTOF8-ROBOT)(RIGHTOF9-ROBOT)))

(:derived (Flag396-)(and (RIGHTOF7-ROBOT)(LEFTOF8-ROBOT)))

(:derived (Flag397-)(and (RIGHTOF14-ROBOT)(LEFTOF8-ROBOT)))

(:derived (Flag398-)(and (LEFTOF8-ROBOT)(RIGHTOF12-ROBOT)))

(:derived (Flag399-)(and (RIGHTOF11-ROBOT)(LEFTOF8-ROBOT)))

(:derived (Flag400-)(and (RIGHTOF17-ROBOT)(LEFTOF8-ROBOT)))

(:derived (Flag401-)(and (RIGHTOF8-ROBOT)(LEFTOF8-ROBOT)))

(:derived (Flag403-)(and (RIGHTOF13-ROBOT)(LEFTOF9-ROBOT)))

(:derived (Flag404-)(and (RIGHTOF8-ROBOT)(LEFTOF9-ROBOT)))

(:derived (Flag405-)(and (LEFTOF9-ROBOT)(RIGHTOF12-ROBOT)))

(:derived (Flag406-)(and (LEFTOF9-ROBOT)(RIGHTOF9-ROBOT)))

(:derived (Flag407-)(and (LEFTOF8-ROBOT)(RIGHTOF14-ROBOT)))

(:derived (Flag408-)(and (RIGHTOF11-ROBOT)(LEFTOF9-ROBOT)))

(:derived (Flag409-)(and (LEFTOF9-ROBOT)(RIGHTOF15-ROBOT)))

(:derived (Flag410-)(and (RIGHTOF10-ROBOT)(LEFTOF9-ROBOT)))

(:derived (Flag411-)(and (LEFTOF9-ROBOT)(RIGHTOF16-ROBOT)))

(:derived (Flag412-)(and (LEFTOF9-ROBOT)(RIGHTOF17-ROBOT)))

(:derived (Flag413-)(and (LEFTOF9-ROBOT)(RIGHTOF14-ROBOT)))

(:derived (Flag415-)(and (LEFTOF10-ROBOT)(RIGHTOF12-ROBOT)))

(:derived (Flag416-)(and (RIGHTOF17-ROBOT)(LEFTOF10-ROBOT)))

(:derived (Flag417-)(and (RIGHTOF14-ROBOT)(LEFTOF10-ROBOT)))

(:derived (Flag418-)(and (RIGHTOF13-ROBOT)(LEFTOF10-ROBOT)))

(:derived (Flag419-)(and (LEFTOF10-ROBOT)(RIGHTOF9-ROBOT)))

(:derived (Flag420-)(and (RIGHTOF11-ROBOT)(LEFTOF10-ROBOT)))

(:derived (Flag421-)(and (RIGHTOF10-ROBOT)(LEFTOF10-ROBOT)))

(:derived (Flag422-)(and (RIGHTOF16-ROBOT)(LEFTOF10-ROBOT)))

(:derived (Flag423-)(and (LEFTOF10-ROBOT)(RIGHTOF15-ROBOT)))

(:derived (Flag425-)(and (LEFTOF11-ROBOT)(RIGHTOF15-ROBOT)))

(:derived (Flag426-)(and (LEFTOF11-ROBOT)(RIGHTOF16-ROBOT)))

(:derived (Flag427-)(and (RIGHTOF11-ROBOT)(LEFTOF11-ROBOT)))

(:derived (Flag428-)(and (LEFTOF10-ROBOT)(RIGHTOF14-ROBOT)))

(:derived (Flag429-)(and (RIGHTOF13-ROBOT)(LEFTOF11-ROBOT)))

(:derived (Flag430-)(and (LEFTOF11-ROBOT)(RIGHTOF14-ROBOT)))

(:derived (Flag431-)(and (LEFTOF11-ROBOT)(RIGHTOF12-ROBOT)))

(:derived (Flag432-)(and (LEFTOF11-ROBOT)(RIGHTOF17-ROBOT)))

(:derived (Flag433-)(and (RIGHTOF10-ROBOT)(LEFTOF11-ROBOT)))

(:derived (Flag435-)(and (RIGHTOF14-ROBOT)(LEFTOF12-ROBOT)))

(:derived (Flag436-)(and (RIGHTOF11-ROBOT)(LEFTOF12-ROBOT)))

(:derived (Flag437-)(and (RIGHTOF12-ROBOT)(LEFTOF12-ROBOT)))

(:derived (Flag438-)(and (RIGHTOF16-ROBOT)(LEFTOF12-ROBOT)))

(:derived (Flag439-)(and (RIGHTOF17-ROBOT)(LEFTOF12-ROBOT)))

(:derived (Flag440-)(and (RIGHTOF15-ROBOT)(LEFTOF12-ROBOT)))

(:derived (Flag441-)(and (RIGHTOF13-ROBOT)(LEFTOF12-ROBOT)))

(:derived (Flag443-)(and (LEFTOF13-ROBOT)(RIGHTOF15-ROBOT)))

(:derived (Flag444-)(and (LEFTOF13-ROBOT)(RIGHTOF12-ROBOT)))

(:derived (Flag445-)(and (LEFTOF13-ROBOT)(RIGHTOF14-ROBOT)))

(:derived (Flag446-)(and (RIGHTOF13-ROBOT)(LEFTOF13-ROBOT)))

(:derived (Flag447-)(and (RIGHTOF17-ROBOT)(LEFTOF13-ROBOT)))

(:derived (Flag448-)(and (RIGHTOF16-ROBOT)(LEFTOF13-ROBOT)))

(:derived (Flag449-)(and (LEFTOF12-ROBOT)(RIGHTOF12-ROBOT)))

(:derived (Flag451-)(and (LEFTOF14-ROBOT)(RIGHTOF14-ROBOT)))

(:derived (Flag452-)(and (RIGHTOF13-ROBOT)(LEFTOF14-ROBOT)))

(:derived (Flag453-)(and (LEFTOF14-ROBOT)(RIGHTOF16-ROBOT)))

(:derived (Flag454-)(and (LEFTOF14-ROBOT)(RIGHTOF17-ROBOT)))

(:derived (Flag455-)(and (LEFTOF14-ROBOT)(RIGHTOF15-ROBOT)))

(:derived (Flag457-)(and (RIGHTOF16-ROBOT)(LEFTOF15-ROBOT)))

(:derived (Flag458-)(and (RIGHTOF17-ROBOT)(LEFTOF15-ROBOT)))

(:derived (Flag459-)(and (RIGHTOF15-ROBOT)(LEFTOF15-ROBOT)))

(:derived (Flag460-)(and (RIGHTOF14-ROBOT)(LEFTOF15-ROBOT)))

(:derived (Flag462-)(and (LEFTOF16-ROBOT)(RIGHTOF17-ROBOT)))

(:derived (Flag463-)(and (LEFTOF15-ROBOT)(RIGHTOF15-ROBOT)))

(:derived (Flag464-)(and (RIGHTOF16-ROBOT)(LEFTOF16-ROBOT)))

(:derived (Flag465-)(and (LEFTOF16-ROBOT)(RIGHTOF15-ROBOT)))

(:derived (Flag467-)(and (LEFTOF17-ROBOT)(RIGHTOF17-ROBOT)))

(:derived (Flag468-)(and (LEFTOF17-ROBOT)(RIGHTOF16-ROBOT)))

(:derived (Flag470-)(and (RIGHTOF17-ROBOT)(LEFTOF18-ROBOT)))

(:derived (Flag472-)(and (Flag66-)))

(:derived (Flag472-)(and (Flag54-)))

(:derived (Flag472-)(and (Flag39-)))

(:derived (Flag472-)(and (Flag68-)))

(:derived (Flag472-)(and (Flag56-)))

(:derived (Flag472-)(and (Flag46-)))

(:derived (Flag472-)(and (Flag40-)))

(:derived (Flag472-)(and (Flag57-)))

(:derived (Flag472-)(and (Flag48-)))

(:derived (Flag472-)(and (Flag58-)))

(:derived (Flag472-)(and (Flag45-)))

(:derived (Flag472-)(and (Flag60-)))

(:derived (Flag472-)(and (Flag70-)))

(:derived (Flag472-)(and (Flag71-)))

(:derived (Flag472-)(and (Flag62-)))

(:derived (Flag472-)(and (Flag63-)))

(:derived (Flag472-)(and (Flag38-)))

(:derived (Flag473-)(and (RIGHTOF3-ROBOT)))

(:derived (Flag473-)(and (RIGHTOF10-ROBOT)))

(:derived (Flag473-)(and (RIGHTOF5-ROBOT)))

(:derived (Flag473-)(and (RIGHTOF9-ROBOT)))

(:derived (Flag473-)(and (RIGHTOF17-ROBOT)))

(:derived (Flag473-)(and (RIGHTOF14-ROBOT)))

(:derived (Flag473-)(and (RIGHTOF13-ROBOT)))

(:derived (Flag473-)(and (RIGHTOF12-ROBOT)))

(:derived (Flag473-)(and (RIGHTOF0-ROBOT)))

(:derived (Flag473-)(and (RIGHTOF8-ROBOT)))

(:derived (Flag473-)(and (RIGHTOF4-ROBOT)))

(:derived (Flag473-)(and (RIGHTOF1-ROBOT)))

(:derived (Flag473-)(and (RIGHTOF16-ROBOT)))

(:derived (Flag473-)(and (RIGHTOF7-ROBOT)))

(:derived (Flag473-)(and (RIGHTOF11-ROBOT)))

(:derived (Flag473-)(and (RIGHTOF15-ROBOT)))

(:derived (Flag473-)(and (RIGHTOF6-ROBOT)))

(:derived (Flag474-)(and (RIGHTOF0-ROBOT)(LEFTOF1-ROBOT)))

(:action MOVERIGHT-ROBOT
:parameters ()
:precondition
(and
(Flag3-)
)
:effect
(and
(when
(and
(Flag469-)
)
(and
(COLUMN17-ROBOT)
(not (NOT-COLUMN17-ROBOT))
)
)
(when
(and
(Flag466-)
)
(and
(COLUMN16-ROBOT)
(not (NOT-COLUMN16-ROBOT))
)
)
(when
(and
(Flag461-)
)
(and
(COLUMN15-ROBOT)
(not (NOT-COLUMN15-ROBOT))
)
)
(when
(and
(Flag456-)
)
(and
(COLUMN14-ROBOT)
(not (NOT-COLUMN14-ROBOT))
)
)
(when
(and
(Flag450-)
)
(and
(COLUMN13-ROBOT)
(not (NOT-COLUMN13-ROBOT))
)
)
(when
(and
(Flag442-)
)
(and
(COLUMN12-ROBOT)
(not (NOT-COLUMN12-ROBOT))
)
)
(when
(and
(Flag434-)
)
(and
(COLUMN11-ROBOT)
(not (NOT-COLUMN11-ROBOT))
)
)
(when
(and
(Flag424-)
)
(and
(COLUMN10-ROBOT)
(not (NOT-COLUMN10-ROBOT))
)
)
(when
(and
(Flag414-)
)
(and
(COLUMN9-ROBOT)
(not (NOT-COLUMN9-ROBOT))
)
)
(when
(and
(Flag402-)
)
(and
(COLUMN8-ROBOT)
(not (NOT-COLUMN8-ROBOT))
)
)
(when
(and
(Flag389-)
)
(and
(COLUMN7-ROBOT)
(not (NOT-COLUMN7-ROBOT))
)
)
(when
(and
(Flag374-)
)
(and
(COLUMN6-ROBOT)
(not (NOT-COLUMN6-ROBOT))
)
)
(when
(and
(Flag360-)
)
(and
(COLUMN5-ROBOT)
(not (NOT-COLUMN5-ROBOT))
)
)
(when
(and
(Flag343-)
)
(and
(COLUMN4-ROBOT)
(not (NOT-COLUMN4-ROBOT))
)
)
(when
(and
(Flag325-)
)
(and
(COLUMN3-ROBOT)
(not (NOT-COLUMN3-ROBOT))
)
)
(when
(and
(Flag308-)
)
(and
(COLUMN2-ROBOT)
(not (NOT-COLUMN2-ROBOT))
)
)
(when
(and
(Flag475-)
)
(and
(COLUMN1-ROBOT)
(not (NOT-COLUMN1-ROBOT))
)
)
(when
(and
(COLUMN16-ROBOT)
)
(and
(COLUMN17-ROBOT)
(NOT-COLUMN16-ROBOT)
(not (NOT-COLUMN17-ROBOT))
(not (COLUMN16-ROBOT))
)
)
(when
(and
(COLUMN15-ROBOT)
)
(and
(COLUMN16-ROBOT)
(NOT-COLUMN15-ROBOT)
(not (NOT-COLUMN16-ROBOT))
(not (COLUMN15-ROBOT))
)
)
(when
(and
(COLUMN14-ROBOT)
)
(and
(COLUMN15-ROBOT)
(NOT-COLUMN14-ROBOT)
(not (NOT-COLUMN15-ROBOT))
(not (COLUMN14-ROBOT))
)
)
(when
(and
(COLUMN13-ROBOT)
)
(and
(COLUMN14-ROBOT)
(NOT-COLUMN13-ROBOT)
(not (NOT-COLUMN14-ROBOT))
(not (COLUMN13-ROBOT))
)
)
(when
(and
(COLUMN12-ROBOT)
)
(and
(COLUMN13-ROBOT)
(NOT-COLUMN12-ROBOT)
(not (NOT-COLUMN13-ROBOT))
(not (COLUMN12-ROBOT))
)
)
(when
(and
(COLUMN11-ROBOT)
)
(and
(COLUMN12-ROBOT)
(NOT-COLUMN11-ROBOT)
(not (NOT-COLUMN12-ROBOT))
(not (COLUMN11-ROBOT))
)
)
(when
(and
(COLUMN10-ROBOT)
)
(and
(COLUMN11-ROBOT)
(NOT-COLUMN10-ROBOT)
(not (NOT-COLUMN11-ROBOT))
(not (COLUMN10-ROBOT))
)
)
(when
(and
(COLUMN9-ROBOT)
)
(and
(COLUMN10-ROBOT)
(NOT-COLUMN9-ROBOT)
(not (NOT-COLUMN10-ROBOT))
(not (COLUMN9-ROBOT))
)
)
(when
(and
(COLUMN8-ROBOT)
)
(and
(COLUMN9-ROBOT)
(NOT-COLUMN8-ROBOT)
(not (NOT-COLUMN9-ROBOT))
(not (COLUMN8-ROBOT))
)
)
(when
(and
(COLUMN7-ROBOT)
)
(and
(COLUMN8-ROBOT)
(NOT-COLUMN7-ROBOT)
(not (NOT-COLUMN8-ROBOT))
(not (COLUMN7-ROBOT))
)
)
(when
(and
(COLUMN6-ROBOT)
)
(and
(COLUMN7-ROBOT)
(NOT-COLUMN6-ROBOT)
(not (NOT-COLUMN7-ROBOT))
(not (COLUMN6-ROBOT))
)
)
(when
(and
(COLUMN5-ROBOT)
)
(and
(COLUMN6-ROBOT)
(NOT-COLUMN5-ROBOT)
(not (NOT-COLUMN6-ROBOT))
(not (COLUMN5-ROBOT))
)
)
(when
(and
(COLUMN4-ROBOT)
)
(and
(COLUMN5-ROBOT)
(NOT-COLUMN4-ROBOT)
(not (NOT-COLUMN5-ROBOT))
(not (COLUMN4-ROBOT))
)
)
(when
(and
(COLUMN3-ROBOT)
)
(and
(COLUMN4-ROBOT)
(NOT-COLUMN3-ROBOT)
(not (NOT-COLUMN4-ROBOT))
(not (COLUMN3-ROBOT))
)
)
(when
(and
(COLUMN2-ROBOT)
)
(and
(COLUMN3-ROBOT)
(NOT-COLUMN2-ROBOT)
(not (NOT-COLUMN3-ROBOT))
(not (COLUMN2-ROBOT))
)
)
(when
(and
(COLUMN1-ROBOT)
)
(and
(COLUMN2-ROBOT)
(NOT-COLUMN1-ROBOT)
(not (NOT-COLUMN2-ROBOT))
(not (COLUMN1-ROBOT))
)
)
(when
(and
(COLUMN0-ROBOT)
)
(and
(COLUMN1-ROBOT)
(NOT-COLUMN0-ROBOT)
(not (NOT-COLUMN1-ROBOT))
(not (COLUMN0-ROBOT))
)
)
(when
(and
(LEFTOF8-ROBOT)
)
(and
(LEFTOF18-ROBOT)
(NOT-LEFTOF17-ROBOT)
(LEFTOF17-ROBOT)
(NOT-LEFTOF16-ROBOT)
(LEFTOF16-ROBOT)
(NOT-LEFTOF15-ROBOT)
(LEFTOF15-ROBOT)
(NOT-LEFTOF14-ROBOT)
(LEFTOF14-ROBOT)
(NOT-LEFTOF13-ROBOT)
(LEFTOF13-ROBOT)
(NOT-LEFTOF12-ROBOT)
(LEFTOF12-ROBOT)
(NOT-LEFTOF11-ROBOT)
(LEFTOF11-ROBOT)
(NOT-LEFTOF10-ROBOT)
(LEFTOF10-ROBOT)
(NOT-LEFTOF9-ROBOT)
(LEFTOF9-ROBOT)
(NOT-LEFTOF8-ROBOT)
(not (NOT-LEFTOF18-ROBOT))
(not (LEFTOF8-ROBOT))
)
)
(when
(and
(LEFTOF7-ROBOT)
)
(and
(LEFTOF18-ROBOT)
(NOT-LEFTOF17-ROBOT)
(LEFTOF17-ROBOT)
(NOT-LEFTOF16-ROBOT)
(LEFTOF16-ROBOT)
(NOT-LEFTOF15-ROBOT)
(LEFTOF15-ROBOT)
(NOT-LEFTOF14-ROBOT)
(LEFTOF14-ROBOT)
(NOT-LEFTOF13-ROBOT)
(LEFTOF13-ROBOT)
(NOT-LEFTOF12-ROBOT)
(LEFTOF12-ROBOT)
(NOT-LEFTOF11-ROBOT)
(LEFTOF11-ROBOT)
(NOT-LEFTOF10-ROBOT)
(LEFTOF10-ROBOT)
(NOT-LEFTOF9-ROBOT)
(LEFTOF9-ROBOT)
(NOT-LEFTOF8-ROBOT)
(LEFTOF8-ROBOT)
(NOT-LEFTOF7-ROBOT)
(not (NOT-LEFTOF18-ROBOT))
(not (LEFTOF7-ROBOT))
)
)
(when
(and
(LEFTOF4-ROBOT)
)
(and
(LEFTOF18-ROBOT)
(NOT-LEFTOF17-ROBOT)
(LEFTOF17-ROBOT)
(NOT-LEFTOF16-ROBOT)
(LEFTOF16-ROBOT)
(NOT-LEFTOF15-ROBOT)
(LEFTOF15-ROBOT)
(NOT-LEFTOF14-ROBOT)
(LEFTOF14-ROBOT)
(NOT-LEFTOF13-ROBOT)
(LEFTOF13-ROBOT)
(NOT-LEFTOF12-ROBOT)
(LEFTOF12-ROBOT)
(NOT-LEFTOF11-ROBOT)
(LEFTOF11-ROBOT)
(NOT-LEFTOF10-ROBOT)
(LEFTOF10-ROBOT)
(NOT-LEFTOF9-ROBOT)
(LEFTOF9-ROBOT)
(NOT-LEFTOF8-ROBOT)
(LEFTOF8-ROBOT)
(NOT-LEFTOF7-ROBOT)
(LEFTOF7-ROBOT)
(NOT-LEFTOF6-ROBOT)
(LEFTOF6-ROBOT)
(NOT-LEFTOF5-ROBOT)
(LEFTOF5-ROBOT)
(NOT-LEFTOF4-ROBOT)
(not (NOT-LEFTOF18-ROBOT))
(not (LEFTOF4-ROBOT))
)
)
(when
(and
(LEFTOF3-ROBOT)
)
(and
(LEFTOF18-ROBOT)
(NOT-LEFTOF17-ROBOT)
(LEFTOF17-ROBOT)
(NOT-LEFTOF16-ROBOT)
(LEFTOF16-ROBOT)
(NOT-LEFTOF15-ROBOT)
(LEFTOF15-ROBOT)
(NOT-LEFTOF14-ROBOT)
(LEFTOF14-ROBOT)
(NOT-LEFTOF13-ROBOT)
(LEFTOF13-ROBOT)
(NOT-LEFTOF12-ROBOT)
(LEFTOF12-ROBOT)
(NOT-LEFTOF11-ROBOT)
(LEFTOF11-ROBOT)
(NOT-LEFTOF10-ROBOT)
(LEFTOF10-ROBOT)
(NOT-LEFTOF9-ROBOT)
(LEFTOF9-ROBOT)
(NOT-LEFTOF8-ROBOT)
(LEFTOF8-ROBOT)
(NOT-LEFTOF7-ROBOT)
(LEFTOF7-ROBOT)
(NOT-LEFTOF6-ROBOT)
(LEFTOF6-ROBOT)
(NOT-LEFTOF5-ROBOT)
(LEFTOF5-ROBOT)
(NOT-LEFTOF4-ROBOT)
(LEFTOF4-ROBOT)
(NOT-LEFTOF3-ROBOT)
(not (NOT-LEFTOF18-ROBOT))
(not (LEFTOF3-ROBOT))
)
)
(when
(and
(LEFTOF14-ROBOT)
)
(and
(LEFTOF18-ROBOT)
(NOT-LEFTOF17-ROBOT)
(LEFTOF17-ROBOT)
(NOT-LEFTOF16-ROBOT)
(LEFTOF16-ROBOT)
(NOT-LEFTOF15-ROBOT)
(LEFTOF15-ROBOT)
(NOT-LEFTOF14-ROBOT)
(not (NOT-LEFTOF18-ROBOT))
(not (LEFTOF14-ROBOT))
)
)
(when
(and
(LEFTOF16-ROBOT)
)
(and
(LEFTOF18-ROBOT)
(NOT-LEFTOF17-ROBOT)
(LEFTOF17-ROBOT)
(NOT-LEFTOF16-ROBOT)
(not (NOT-LEFTOF18-ROBOT))
(not (LEFTOF16-ROBOT))
)
)
(when
(and
(LEFTOF5-ROBOT)
)
(and
(LEFTOF18-ROBOT)
(NOT-LEFTOF17-ROBOT)
(LEFTOF17-ROBOT)
(NOT-LEFTOF16-ROBOT)
(LEFTOF16-ROBOT)
(NOT-LEFTOF15-ROBOT)
(LEFTOF15-ROBOT)
(NOT-LEFTOF14-ROBOT)
(LEFTOF14-ROBOT)
(NOT-LEFTOF13-ROBOT)
(LEFTOF13-ROBOT)
(NOT-LEFTOF12-ROBOT)
(LEFTOF12-ROBOT)
(NOT-LEFTOF11-ROBOT)
(LEFTOF11-ROBOT)
(NOT-LEFTOF10-ROBOT)
(LEFTOF10-ROBOT)
(NOT-LEFTOF9-ROBOT)
(LEFTOF9-ROBOT)
(NOT-LEFTOF8-ROBOT)
(LEFTOF8-ROBOT)
(NOT-LEFTOF7-ROBOT)
(LEFTOF7-ROBOT)
(NOT-LEFTOF6-ROBOT)
(LEFTOF6-ROBOT)
(NOT-LEFTOF5-ROBOT)
(not (NOT-LEFTOF18-ROBOT))
(not (LEFTOF5-ROBOT))
)
)
(when
(and
(LEFTOF6-ROBOT)
)
(and
(LEFTOF18-ROBOT)
(NOT-LEFTOF17-ROBOT)
(LEFTOF17-ROBOT)
(NOT-LEFTOF16-ROBOT)
(LEFTOF16-ROBOT)
(NOT-LEFTOF15-ROBOT)
(LEFTOF15-ROBOT)
(NOT-LEFTOF14-ROBOT)
(LEFTOF14-ROBOT)
(NOT-LEFTOF13-ROBOT)
(LEFTOF13-ROBOT)
(NOT-LEFTOF12-ROBOT)
(LEFTOF12-ROBOT)
(NOT-LEFTOF11-ROBOT)
(LEFTOF11-ROBOT)
(NOT-LEFTOF10-ROBOT)
(LEFTOF10-ROBOT)
(NOT-LEFTOF9-ROBOT)
(LEFTOF9-ROBOT)
(NOT-LEFTOF8-ROBOT)
(LEFTOF8-ROBOT)
(NOT-LEFTOF7-ROBOT)
(LEFTOF7-ROBOT)
(NOT-LEFTOF6-ROBOT)
(not (NOT-LEFTOF18-ROBOT))
(not (LEFTOF6-ROBOT))
)
)
(when
(and
(LEFTOF12-ROBOT)
)
(and
(LEFTOF18-ROBOT)
(NOT-LEFTOF17-ROBOT)
(LEFTOF17-ROBOT)
(NOT-LEFTOF16-ROBOT)
(LEFTOF16-ROBOT)
(NOT-LEFTOF15-ROBOT)
(LEFTOF15-ROBOT)
(NOT-LEFTOF14-ROBOT)
(LEFTOF14-ROBOT)
(NOT-LEFTOF13-ROBOT)
(LEFTOF13-ROBOT)
(NOT-LEFTOF12-ROBOT)
(not (NOT-LEFTOF18-ROBOT))
(not (LEFTOF12-ROBOT))
)
)
(when
(and
(LEFTOF1-ROBOT)
)
(and
(LEFTOF18-ROBOT)
(NOT-LEFTOF17-ROBOT)
(LEFTOF17-ROBOT)
(NOT-LEFTOF16-ROBOT)
(LEFTOF16-ROBOT)
(NOT-LEFTOF15-ROBOT)
(LEFTOF15-ROBOT)
(NOT-LEFTOF14-ROBOT)
(LEFTOF14-ROBOT)
(NOT-LEFTOF13-ROBOT)
(LEFTOF13-ROBOT)
(NOT-LEFTOF12-ROBOT)
(LEFTOF12-ROBOT)
(NOT-LEFTOF11-ROBOT)
(LEFTOF11-ROBOT)
(NOT-LEFTOF10-ROBOT)
(LEFTOF10-ROBOT)
(NOT-LEFTOF9-ROBOT)
(LEFTOF9-ROBOT)
(NOT-LEFTOF8-ROBOT)
(LEFTOF8-ROBOT)
(NOT-LEFTOF7-ROBOT)
(LEFTOF7-ROBOT)
(NOT-LEFTOF6-ROBOT)
(LEFTOF6-ROBOT)
(NOT-LEFTOF5-ROBOT)
(LEFTOF5-ROBOT)
(NOT-LEFTOF4-ROBOT)
(LEFTOF4-ROBOT)
(NOT-LEFTOF3-ROBOT)
(LEFTOF3-ROBOT)
(NOT-LEFTOF2-ROBOT)
(LEFTOF2-ROBOT)
(NOT-LEFTOF1-ROBOT)
(not (NOT-LEFTOF18-ROBOT))
(not (LEFTOF1-ROBOT))
)
)
(when
(and
(LEFTOF13-ROBOT)
)
(and
(LEFTOF18-ROBOT)
(NOT-LEFTOF17-ROBOT)
(LEFTOF17-ROBOT)
(NOT-LEFTOF16-ROBOT)
(LEFTOF16-ROBOT)
(NOT-LEFTOF15-ROBOT)
(LEFTOF15-ROBOT)
(NOT-LEFTOF14-ROBOT)
(LEFTOF14-ROBOT)
(NOT-LEFTOF13-ROBOT)
(not (NOT-LEFTOF18-ROBOT))
(not (LEFTOF13-ROBOT))
)
)
(when
(and
(LEFTOF2-ROBOT)
)
(and
(LEFTOF18-ROBOT)
(NOT-LEFTOF17-ROBOT)
(LEFTOF17-ROBOT)
(NOT-LEFTOF16-ROBOT)
(LEFTOF16-ROBOT)
(NOT-LEFTOF15-ROBOT)
(LEFTOF15-ROBOT)
(NOT-LEFTOF14-ROBOT)
(LEFTOF14-ROBOT)
(NOT-LEFTOF13-ROBOT)
(LEFTOF13-ROBOT)
(NOT-LEFTOF12-ROBOT)
(LEFTOF12-ROBOT)
(NOT-LEFTOF11-ROBOT)
(LEFTOF11-ROBOT)
(NOT-LEFTOF10-ROBOT)
(LEFTOF10-ROBOT)
(NOT-LEFTOF9-ROBOT)
(LEFTOF9-ROBOT)
(NOT-LEFTOF8-ROBOT)
(LEFTOF8-ROBOT)
(NOT-LEFTOF7-ROBOT)
(LEFTOF7-ROBOT)
(NOT-LEFTOF6-ROBOT)
(LEFTOF6-ROBOT)
(NOT-LEFTOF5-ROBOT)
(LEFTOF5-ROBOT)
(NOT-LEFTOF4-ROBOT)
(LEFTOF4-ROBOT)
(NOT-LEFTOF3-ROBOT)
(LEFTOF3-ROBOT)
(NOT-LEFTOF2-ROBOT)
(not (NOT-LEFTOF18-ROBOT))
(not (LEFTOF2-ROBOT))
)
)
(when
(and
(LEFTOF9-ROBOT)
)
(and
(LEFTOF18-ROBOT)
(NOT-LEFTOF17-ROBOT)
(LEFTOF17-ROBOT)
(NOT-LEFTOF16-ROBOT)
(LEFTOF16-ROBOT)
(NOT-LEFTOF15-ROBOT)
(LEFTOF15-ROBOT)
(NOT-LEFTOF14-ROBOT)
(LEFTOF14-ROBOT)
(NOT-LEFTOF13-ROBOT)
(LEFTOF13-ROBOT)
(NOT-LEFTOF12-ROBOT)
(LEFTOF12-ROBOT)
(NOT-LEFTOF11-ROBOT)
(LEFTOF11-ROBOT)
(NOT-LEFTOF10-ROBOT)
(LEFTOF10-ROBOT)
(NOT-LEFTOF9-ROBOT)
(not (NOT-LEFTOF18-ROBOT))
(not (LEFTOF9-ROBOT))
)
)
(when
(and
(LEFTOF11-ROBOT)
)
(and
(LEFTOF18-ROBOT)
(NOT-LEFTOF17-ROBOT)
(LEFTOF17-ROBOT)
(NOT-LEFTOF16-ROBOT)
(LEFTOF16-ROBOT)
(NOT-LEFTOF15-ROBOT)
(LEFTOF15-ROBOT)
(NOT-LEFTOF14-ROBOT)
(LEFTOF14-ROBOT)
(NOT-LEFTOF13-ROBOT)
(LEFTOF13-ROBOT)
(NOT-LEFTOF12-ROBOT)
(LEFTOF12-ROBOT)
(NOT-LEFTOF11-ROBOT)
(not (NOT-LEFTOF18-ROBOT))
(not (LEFTOF11-ROBOT))
)
)
(when
(and
(LEFTOF17-ROBOT)
)
(and
(LEFTOF18-ROBOT)
(NOT-LEFTOF17-ROBOT)
(not (NOT-LEFTOF18-ROBOT))
(not (LEFTOF17-ROBOT))
)
)
(when
(and
(LEFTOF10-ROBOT)
)
(and
(LEFTOF18-ROBOT)
(NOT-LEFTOF17-ROBOT)
(LEFTOF17-ROBOT)
(NOT-LEFTOF16-ROBOT)
(LEFTOF16-ROBOT)
(NOT-LEFTOF15-ROBOT)
(LEFTOF15-ROBOT)
(NOT-LEFTOF14-ROBOT)
(LEFTOF14-ROBOT)
(NOT-LEFTOF13-ROBOT)
(LEFTOF13-ROBOT)
(NOT-LEFTOF12-ROBOT)
(LEFTOF12-ROBOT)
(NOT-LEFTOF11-ROBOT)
(LEFTOF11-ROBOT)
(NOT-LEFTOF10-ROBOT)
(not (NOT-LEFTOF18-ROBOT))
(not (LEFTOF10-ROBOT))
)
)
(when
(and
(LEFTOF15-ROBOT)
)
(and
(LEFTOF18-ROBOT)
(NOT-LEFTOF17-ROBOT)
(LEFTOF17-ROBOT)
(NOT-LEFTOF16-ROBOT)
(LEFTOF16-ROBOT)
(NOT-LEFTOF15-ROBOT)
(not (NOT-LEFTOF18-ROBOT))
(not (LEFTOF15-ROBOT))
)
)
(when
(and
(RIGHTOF17-ROBOT)
)
(and
(RIGHTOF16-ROBOT)
(RIGHTOF15-ROBOT)
(RIGHTOF14-ROBOT)
(RIGHTOF13-ROBOT)
(RIGHTOF12-ROBOT)
(RIGHTOF11-ROBOT)
(RIGHTOF10-ROBOT)
(RIGHTOF9-ROBOT)
(RIGHTOF8-ROBOT)
(RIGHTOF7-ROBOT)
(RIGHTOF6-ROBOT)
(RIGHTOF5-ROBOT)
(RIGHTOF4-ROBOT)
(RIGHTOF3-ROBOT)
(RIGHTOF2-ROBOT)
(not (NOT-RIGHTOF17-ROBOT))
(not (NOT-RIGHTOF16-ROBOT))
(not (NOT-RIGHTOF15-ROBOT))
(not (NOT-RIGHTOF14-ROBOT))
(not (NOT-RIGHTOF13-ROBOT))
(not (NOT-RIGHTOF12-ROBOT))
(not (NOT-RIGHTOF11-ROBOT))
(not (NOT-RIGHTOF10-ROBOT))
(not (NOT-RIGHTOF9-ROBOT))
(not (NOT-RIGHTOF8-ROBOT))
(not (NOT-RIGHTOF7-ROBOT))
(not (NOT-RIGHTOF6-ROBOT))
(not (NOT-RIGHTOF5-ROBOT))
(not (NOT-RIGHTOF4-ROBOT))
(not (NOT-RIGHTOF3-ROBOT))
(not (NOT-RIGHTOF2-ROBOT))
)
)
(when
(and
(RIGHTOF16-ROBOT)
)
(and
(RIGHTOF17-ROBOT)
(RIGHTOF15-ROBOT)
(RIGHTOF14-ROBOT)
(RIGHTOF13-ROBOT)
(RIGHTOF12-ROBOT)
(RIGHTOF11-ROBOT)
(RIGHTOF10-ROBOT)
(RIGHTOF9-ROBOT)
(RIGHTOF8-ROBOT)
(RIGHTOF7-ROBOT)
(RIGHTOF6-ROBOT)
(RIGHTOF5-ROBOT)
(RIGHTOF4-ROBOT)
(RIGHTOF3-ROBOT)
(RIGHTOF2-ROBOT)
(not (NOT-RIGHTOF17-ROBOT))
(not (NOT-RIGHTOF16-ROBOT))
(not (NOT-RIGHTOF15-ROBOT))
(not (NOT-RIGHTOF14-ROBOT))
(not (NOT-RIGHTOF13-ROBOT))
(not (NOT-RIGHTOF12-ROBOT))
(not (NOT-RIGHTOF11-ROBOT))
(not (NOT-RIGHTOF10-ROBOT))
(not (NOT-RIGHTOF9-ROBOT))
(not (NOT-RIGHTOF8-ROBOT))
(not (NOT-RIGHTOF7-ROBOT))
(not (NOT-RIGHTOF6-ROBOT))
(not (NOT-RIGHTOF5-ROBOT))
(not (NOT-RIGHTOF4-ROBOT))
(not (NOT-RIGHTOF3-ROBOT))
(not (NOT-RIGHTOF2-ROBOT))
)
)
(when
(and
(RIGHTOF15-ROBOT)
)
(and
(RIGHTOF16-ROBOT)
(RIGHTOF14-ROBOT)
(RIGHTOF13-ROBOT)
(RIGHTOF12-ROBOT)
(RIGHTOF11-ROBOT)
(RIGHTOF10-ROBOT)
(RIGHTOF9-ROBOT)
(RIGHTOF8-ROBOT)
(RIGHTOF7-ROBOT)
(RIGHTOF6-ROBOT)
(RIGHTOF5-ROBOT)
(RIGHTOF4-ROBOT)
(RIGHTOF3-ROBOT)
(RIGHTOF2-ROBOT)
(not (NOT-RIGHTOF16-ROBOT))
(not (NOT-RIGHTOF15-ROBOT))
(not (NOT-RIGHTOF14-ROBOT))
(not (NOT-RIGHTOF13-ROBOT))
(not (NOT-RIGHTOF12-ROBOT))
(not (NOT-RIGHTOF11-ROBOT))
(not (NOT-RIGHTOF10-ROBOT))
(not (NOT-RIGHTOF9-ROBOT))
(not (NOT-RIGHTOF8-ROBOT))
(not (NOT-RIGHTOF7-ROBOT))
(not (NOT-RIGHTOF6-ROBOT))
(not (NOT-RIGHTOF5-ROBOT))
(not (NOT-RIGHTOF4-ROBOT))
(not (NOT-RIGHTOF3-ROBOT))
(not (NOT-RIGHTOF2-ROBOT))
)
)
(when
(and
(RIGHTOF14-ROBOT)
)
(and
(RIGHTOF15-ROBOT)
(RIGHTOF13-ROBOT)
(RIGHTOF12-ROBOT)
(RIGHTOF11-ROBOT)
(RIGHTOF10-ROBOT)
(RIGHTOF9-ROBOT)
(RIGHTOF8-ROBOT)
(RIGHTOF7-ROBOT)
(RIGHTOF6-ROBOT)
(RIGHTOF5-ROBOT)
(RIGHTOF4-ROBOT)
(RIGHTOF3-ROBOT)
(RIGHTOF2-ROBOT)
(not (NOT-RIGHTOF15-ROBOT))
(not (NOT-RIGHTOF14-ROBOT))
(not (NOT-RIGHTOF13-ROBOT))
(not (NOT-RIGHTOF12-ROBOT))
(not (NOT-RIGHTOF11-ROBOT))
(not (NOT-RIGHTOF10-ROBOT))
(not (NOT-RIGHTOF9-ROBOT))
(not (NOT-RIGHTOF8-ROBOT))
(not (NOT-RIGHTOF7-ROBOT))
(not (NOT-RIGHTOF6-ROBOT))
(not (NOT-RIGHTOF5-ROBOT))
(not (NOT-RIGHTOF4-ROBOT))
(not (NOT-RIGHTOF3-ROBOT))
(not (NOT-RIGHTOF2-ROBOT))
)
)
(when
(and
(RIGHTOF13-ROBOT)
)
(and
(RIGHTOF14-ROBOT)
(RIGHTOF12-ROBOT)
(RIGHTOF11-ROBOT)
(RIGHTOF10-ROBOT)
(RIGHTOF9-ROBOT)
(RIGHTOF8-ROBOT)
(RIGHTOF7-ROBOT)
(RIGHTOF6-ROBOT)
(RIGHTOF5-ROBOT)
(RIGHTOF4-ROBOT)
(RIGHTOF3-ROBOT)
(RIGHTOF2-ROBOT)
(not (NOT-RIGHTOF14-ROBOT))
(not (NOT-RIGHTOF13-ROBOT))
(not (NOT-RIGHTOF12-ROBOT))
(not (NOT-RIGHTOF11-ROBOT))
(not (NOT-RIGHTOF10-ROBOT))
(not (NOT-RIGHTOF9-ROBOT))
(not (NOT-RIGHTOF8-ROBOT))
(not (NOT-RIGHTOF7-ROBOT))
(not (NOT-RIGHTOF6-ROBOT))
(not (NOT-RIGHTOF5-ROBOT))
(not (NOT-RIGHTOF4-ROBOT))
(not (NOT-RIGHTOF3-ROBOT))
(not (NOT-RIGHTOF2-ROBOT))
)
)
(when
(and
(RIGHTOF12-ROBOT)
)
(and
(RIGHTOF13-ROBOT)
(RIGHTOF11-ROBOT)
(RIGHTOF10-ROBOT)
(RIGHTOF9-ROBOT)
(RIGHTOF8-ROBOT)
(RIGHTOF7-ROBOT)
(RIGHTOF6-ROBOT)
(RIGHTOF5-ROBOT)
(RIGHTOF4-ROBOT)
(RIGHTOF3-ROBOT)
(RIGHTOF2-ROBOT)
(not (NOT-RIGHTOF13-ROBOT))
(not (NOT-RIGHTOF12-ROBOT))
(not (NOT-RIGHTOF11-ROBOT))
(not (NOT-RIGHTOF10-ROBOT))
(not (NOT-RIGHTOF9-ROBOT))
(not (NOT-RIGHTOF8-ROBOT))
(not (NOT-RIGHTOF7-ROBOT))
(not (NOT-RIGHTOF6-ROBOT))
(not (NOT-RIGHTOF5-ROBOT))
(not (NOT-RIGHTOF4-ROBOT))
(not (NOT-RIGHTOF3-ROBOT))
(not (NOT-RIGHTOF2-ROBOT))
)
)
(when
(and
(RIGHTOF11-ROBOT)
)
(and
(RIGHTOF12-ROBOT)
(RIGHTOF10-ROBOT)
(RIGHTOF9-ROBOT)
(RIGHTOF8-ROBOT)
(RIGHTOF7-ROBOT)
(RIGHTOF6-ROBOT)
(RIGHTOF5-ROBOT)
(RIGHTOF4-ROBOT)
(RIGHTOF3-ROBOT)
(RIGHTOF2-ROBOT)
(not (NOT-RIGHTOF12-ROBOT))
(not (NOT-RIGHTOF11-ROBOT))
(not (NOT-RIGHTOF10-ROBOT))
(not (NOT-RIGHTOF9-ROBOT))
(not (NOT-RIGHTOF8-ROBOT))
(not (NOT-RIGHTOF7-ROBOT))
(not (NOT-RIGHTOF6-ROBOT))
(not (NOT-RIGHTOF5-ROBOT))
(not (NOT-RIGHTOF4-ROBOT))
(not (NOT-RIGHTOF3-ROBOT))
(not (NOT-RIGHTOF2-ROBOT))
)
)
(when
(and
(RIGHTOF10-ROBOT)
)
(and
(RIGHTOF11-ROBOT)
(RIGHTOF9-ROBOT)
(RIGHTOF8-ROBOT)
(RIGHTOF7-ROBOT)
(RIGHTOF6-ROBOT)
(RIGHTOF5-ROBOT)
(RIGHTOF4-ROBOT)
(RIGHTOF3-ROBOT)
(RIGHTOF2-ROBOT)
(not (NOT-RIGHTOF11-ROBOT))
(not (NOT-RIGHTOF10-ROBOT))
(not (NOT-RIGHTOF9-ROBOT))
(not (NOT-RIGHTOF8-ROBOT))
(not (NOT-RIGHTOF7-ROBOT))
(not (NOT-RIGHTOF6-ROBOT))
(not (NOT-RIGHTOF5-ROBOT))
(not (NOT-RIGHTOF4-ROBOT))
(not (NOT-RIGHTOF3-ROBOT))
(not (NOT-RIGHTOF2-ROBOT))
)
)
(when
(and
(RIGHTOF9-ROBOT)
)
(and
(RIGHTOF10-ROBOT)
(RIGHTOF8-ROBOT)
(RIGHTOF7-ROBOT)
(RIGHTOF6-ROBOT)
(RIGHTOF5-ROBOT)
(RIGHTOF4-ROBOT)
(RIGHTOF3-ROBOT)
(RIGHTOF2-ROBOT)
(not (NOT-RIGHTOF10-ROBOT))
(not (NOT-RIGHTOF9-ROBOT))
(not (NOT-RIGHTOF8-ROBOT))
(not (NOT-RIGHTOF7-ROBOT))
(not (NOT-RIGHTOF6-ROBOT))
(not (NOT-RIGHTOF5-ROBOT))
(not (NOT-RIGHTOF4-ROBOT))
(not (NOT-RIGHTOF3-ROBOT))
(not (NOT-RIGHTOF2-ROBOT))
)
)
(when
(and
(RIGHTOF8-ROBOT)
)
(and
(RIGHTOF9-ROBOT)
(RIGHTOF7-ROBOT)
(RIGHTOF6-ROBOT)
(RIGHTOF5-ROBOT)
(RIGHTOF4-ROBOT)
(RIGHTOF3-ROBOT)
(RIGHTOF2-ROBOT)
(not (NOT-RIGHTOF9-ROBOT))
(not (NOT-RIGHTOF8-ROBOT))
(not (NOT-RIGHTOF7-ROBOT))
(not (NOT-RIGHTOF6-ROBOT))
(not (NOT-RIGHTOF5-ROBOT))
(not (NOT-RIGHTOF4-ROBOT))
(not (NOT-RIGHTOF3-ROBOT))
(not (NOT-RIGHTOF2-ROBOT))
)
)
(when
(and
(RIGHTOF7-ROBOT)
)
(and
(RIGHTOF8-ROBOT)
(RIGHTOF6-ROBOT)
(RIGHTOF5-ROBOT)
(RIGHTOF4-ROBOT)
(RIGHTOF3-ROBOT)
(RIGHTOF2-ROBOT)
(not (NOT-RIGHTOF8-ROBOT))
(not (NOT-RIGHTOF7-ROBOT))
(not (NOT-RIGHTOF6-ROBOT))
(not (NOT-RIGHTOF5-ROBOT))
(not (NOT-RIGHTOF4-ROBOT))
(not (NOT-RIGHTOF3-ROBOT))
(not (NOT-RIGHTOF2-ROBOT))
)
)
(when
(and
(RIGHTOF6-ROBOT)
)
(and
(RIGHTOF7-ROBOT)
(RIGHTOF5-ROBOT)
(RIGHTOF4-ROBOT)
(RIGHTOF3-ROBOT)
(RIGHTOF2-ROBOT)
(not (NOT-RIGHTOF7-ROBOT))
(not (NOT-RIGHTOF6-ROBOT))
(not (NOT-RIGHTOF5-ROBOT))
(not (NOT-RIGHTOF4-ROBOT))
(not (NOT-RIGHTOF3-ROBOT))
(not (NOT-RIGHTOF2-ROBOT))
)
)
(when
(and
(RIGHTOF5-ROBOT)
)
(and
(RIGHTOF6-ROBOT)
(RIGHTOF4-ROBOT)
(RIGHTOF3-ROBOT)
(RIGHTOF2-ROBOT)
(not (NOT-RIGHTOF6-ROBOT))
(not (NOT-RIGHTOF5-ROBOT))
(not (NOT-RIGHTOF4-ROBOT))
(not (NOT-RIGHTOF3-ROBOT))
(not (NOT-RIGHTOF2-ROBOT))
)
)
(when
(and
(RIGHTOF4-ROBOT)
)
(and
(RIGHTOF5-ROBOT)
(RIGHTOF3-ROBOT)
(RIGHTOF2-ROBOT)
(not (NOT-RIGHTOF5-ROBOT))
(not (NOT-RIGHTOF4-ROBOT))
(not (NOT-RIGHTOF3-ROBOT))
(not (NOT-RIGHTOF2-ROBOT))
)
)
(when
(and
(RIGHTOF3-ROBOT)
)
(and
(RIGHTOF4-ROBOT)
(RIGHTOF2-ROBOT)
(not (NOT-RIGHTOF4-ROBOT))
(not (NOT-RIGHTOF3-ROBOT))
(not (NOT-RIGHTOF2-ROBOT))
)
)
(when
(and
(RIGHTOF2-ROBOT)
)
(and
(RIGHTOF3-ROBOT)
(not (NOT-RIGHTOF3-ROBOT))
(not (NOT-RIGHTOF2-ROBOT))
)
)
(when
(and
(RIGHTOF1-ROBOT)
)
(and
(RIGHTOF2-ROBOT)
(not (NOT-RIGHTOF2-ROBOT))
)
)
(when
(and
(Flag473-)
)
(and
(RIGHTOF1-ROBOT)
(not (NOT-RIGHTOF1-ROBOT))
)
)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action MOVELEFT-ROBOT
:parameters ()
:precondition
(and
(Flag3-)
)
:effect
(and
(when
(and
(Flag471-)
)
(and
(COLUMN16-ROBOT)
(not (NOT-COLUMN16-ROBOT))
)
)
(when
(and
(Flag469-)
)
(and
(COLUMN15-ROBOT)
(not (NOT-COLUMN15-ROBOT))
)
)
(when
(and
(Flag466-)
)
(and
(COLUMN14-ROBOT)
(not (NOT-COLUMN14-ROBOT))
)
)
(when
(and
(Flag461-)
)
(and
(COLUMN13-ROBOT)
(not (NOT-COLUMN13-ROBOT))
)
)
(when
(and
(Flag456-)
)
(and
(COLUMN12-ROBOT)
(not (NOT-COLUMN12-ROBOT))
)
)
(when
(and
(Flag450-)
)
(and
(COLUMN11-ROBOT)
(not (NOT-COLUMN11-ROBOT))
)
)
(when
(and
(Flag442-)
)
(and
(COLUMN10-ROBOT)
(not (NOT-COLUMN10-ROBOT))
)
)
(when
(and
(Flag434-)
)
(and
(COLUMN9-ROBOT)
(not (NOT-COLUMN9-ROBOT))
)
)
(when
(and
(Flag424-)
)
(and
(COLUMN8-ROBOT)
(not (NOT-COLUMN8-ROBOT))
)
)
(when
(and
(Flag414-)
)
(and
(COLUMN7-ROBOT)
(not (NOT-COLUMN7-ROBOT))
)
)
(when
(and
(Flag402-)
)
(and
(COLUMN6-ROBOT)
(not (NOT-COLUMN6-ROBOT))
)
)
(when
(and
(Flag389-)
)
(and
(COLUMN5-ROBOT)
(not (NOT-COLUMN5-ROBOT))
)
)
(when
(and
(Flag374-)
)
(and
(COLUMN4-ROBOT)
(not (NOT-COLUMN4-ROBOT))
)
)
(when
(and
(Flag360-)
)
(and
(COLUMN3-ROBOT)
(not (NOT-COLUMN3-ROBOT))
)
)
(when
(and
(Flag343-)
)
(and
(COLUMN2-ROBOT)
(not (NOT-COLUMN2-ROBOT))
)
)
(when
(and
(Flag325-)
)
(and
(COLUMN1-ROBOT)
(not (NOT-COLUMN1-ROBOT))
)
)
(when
(and
(Flag308-)
)
(and
(COLUMN0-ROBOT)
(not (NOT-COLUMN0-ROBOT))
)
)
(when
(and
(COLUMN17-ROBOT)
)
(and
(COLUMN16-ROBOT)
(NOT-COLUMN17-ROBOT)
(not (NOT-COLUMN16-ROBOT))
(not (COLUMN17-ROBOT))
)
)
(when
(and
(COLUMN16-ROBOT)
)
(and
(COLUMN15-ROBOT)
(NOT-COLUMN16-ROBOT)
(not (NOT-COLUMN15-ROBOT))
(not (COLUMN16-ROBOT))
)
)
(when
(and
(COLUMN15-ROBOT)
)
(and
(COLUMN14-ROBOT)
(NOT-COLUMN15-ROBOT)
(not (NOT-COLUMN14-ROBOT))
(not (COLUMN15-ROBOT))
)
)
(when
(and
(COLUMN14-ROBOT)
)
(and
(COLUMN13-ROBOT)
(NOT-COLUMN14-ROBOT)
(not (NOT-COLUMN13-ROBOT))
(not (COLUMN14-ROBOT))
)
)
(when
(and
(COLUMN13-ROBOT)
)
(and
(COLUMN12-ROBOT)
(NOT-COLUMN13-ROBOT)
(not (NOT-COLUMN12-ROBOT))
(not (COLUMN13-ROBOT))
)
)
(when
(and
(COLUMN12-ROBOT)
)
(and
(COLUMN11-ROBOT)
(NOT-COLUMN12-ROBOT)
(not (NOT-COLUMN11-ROBOT))
(not (COLUMN12-ROBOT))
)
)
(when
(and
(COLUMN11-ROBOT)
)
(and
(COLUMN10-ROBOT)
(NOT-COLUMN11-ROBOT)
(not (NOT-COLUMN10-ROBOT))
(not (COLUMN11-ROBOT))
)
)
(when
(and
(COLUMN10-ROBOT)
)
(and
(COLUMN9-ROBOT)
(NOT-COLUMN10-ROBOT)
(not (NOT-COLUMN9-ROBOT))
(not (COLUMN10-ROBOT))
)
)
(when
(and
(COLUMN9-ROBOT)
)
(and
(COLUMN8-ROBOT)
(NOT-COLUMN9-ROBOT)
(not (NOT-COLUMN8-ROBOT))
(not (COLUMN9-ROBOT))
)
)
(when
(and
(COLUMN8-ROBOT)
)
(and
(COLUMN7-ROBOT)
(NOT-COLUMN8-ROBOT)
(not (NOT-COLUMN7-ROBOT))
(not (COLUMN8-ROBOT))
)
)
(when
(and
(COLUMN7-ROBOT)
)
(and
(COLUMN6-ROBOT)
(NOT-COLUMN7-ROBOT)
(not (NOT-COLUMN6-ROBOT))
(not (COLUMN7-ROBOT))
)
)
(when
(and
(COLUMN6-ROBOT)
)
(and
(COLUMN5-ROBOT)
(NOT-COLUMN6-ROBOT)
(not (NOT-COLUMN5-ROBOT))
(not (COLUMN6-ROBOT))
)
)
(when
(and
(COLUMN5-ROBOT)
)
(and
(COLUMN4-ROBOT)
(NOT-COLUMN5-ROBOT)
(not (NOT-COLUMN4-ROBOT))
(not (COLUMN5-ROBOT))
)
)
(when
(and
(COLUMN4-ROBOT)
)
(and
(COLUMN3-ROBOT)
(NOT-COLUMN4-ROBOT)
(not (NOT-COLUMN3-ROBOT))
(not (COLUMN4-ROBOT))
)
)
(when
(and
(COLUMN3-ROBOT)
)
(and
(COLUMN2-ROBOT)
(NOT-COLUMN3-ROBOT)
(not (NOT-COLUMN2-ROBOT))
(not (COLUMN3-ROBOT))
)
)
(when
(and
(COLUMN2-ROBOT)
)
(and
(COLUMN1-ROBOT)
(NOT-COLUMN2-ROBOT)
(not (NOT-COLUMN1-ROBOT))
(not (COLUMN2-ROBOT))
)
)
(when
(and
(COLUMN1-ROBOT)
)
(and
(COLUMN0-ROBOT)
(NOT-COLUMN1-ROBOT)
(not (NOT-COLUMN0-ROBOT))
(not (COLUMN1-ROBOT))
)
)
(when
(and
(RIGHTOF17-ROBOT)
)
(and
(RIGHTOF16-ROBOT)
(NOT-RIGHTOF17-ROBOT)
(not (NOT-RIGHTOF16-ROBOT))
(not (RIGHTOF17-ROBOT))
)
)
(when
(and
(Flag272-)
)
(and
(RIGHTOF15-ROBOT)
(NOT-RIGHTOF16-ROBOT)
(not (NOT-RIGHTOF15-ROBOT))
(not (RIGHTOF16-ROBOT))
)
)
(when
(and
(Flag271-)
)
(and
(RIGHTOF14-ROBOT)
(NOT-RIGHTOF15-ROBOT)
(not (NOT-RIGHTOF14-ROBOT))
(not (RIGHTOF15-ROBOT))
)
)
(when
(and
(Flag270-)
)
(and
(RIGHTOF13-ROBOT)
(NOT-RIGHTOF14-ROBOT)
(not (NOT-RIGHTOF13-ROBOT))
(not (RIGHTOF14-ROBOT))
)
)
(when
(and
(Flag269-)
)
(and
(RIGHTOF12-ROBOT)
(NOT-RIGHTOF13-ROBOT)
(not (NOT-RIGHTOF12-ROBOT))
(not (RIGHTOF13-ROBOT))
)
)
(when
(and
(Flag268-)
)
(and
(RIGHTOF11-ROBOT)
(NOT-RIGHTOF12-ROBOT)
(not (NOT-RIGHTOF11-ROBOT))
(not (RIGHTOF12-ROBOT))
)
)
(when
(and
(Flag267-)
)
(and
(RIGHTOF10-ROBOT)
(NOT-RIGHTOF11-ROBOT)
(not (NOT-RIGHTOF10-ROBOT))
(not (RIGHTOF11-ROBOT))
)
)
(when
(and
(Flag266-)
)
(and
(RIGHTOF9-ROBOT)
(NOT-RIGHTOF10-ROBOT)
(not (NOT-RIGHTOF9-ROBOT))
(not (RIGHTOF10-ROBOT))
)
)
(when
(and
(Flag265-)
)
(and
(RIGHTOF8-ROBOT)
(NOT-RIGHTOF9-ROBOT)
(not (NOT-RIGHTOF8-ROBOT))
(not (RIGHTOF9-ROBOT))
)
)
(when
(and
(Flag264-)
)
(and
(RIGHTOF7-ROBOT)
(NOT-RIGHTOF8-ROBOT)
(not (NOT-RIGHTOF7-ROBOT))
(not (RIGHTOF8-ROBOT))
)
)
(when
(and
(Flag263-)
)
(and
(RIGHTOF6-ROBOT)
(NOT-RIGHTOF7-ROBOT)
(not (NOT-RIGHTOF6-ROBOT))
(not (RIGHTOF7-ROBOT))
)
)
(when
(and
(Flag262-)
)
(and
(RIGHTOF5-ROBOT)
(NOT-RIGHTOF6-ROBOT)
(not (NOT-RIGHTOF5-ROBOT))
(not (RIGHTOF6-ROBOT))
)
)
(when
(and
(Flag261-)
)
(and
(RIGHTOF4-ROBOT)
(NOT-RIGHTOF5-ROBOT)
(not (NOT-RIGHTOF4-ROBOT))
(not (RIGHTOF5-ROBOT))
)
)
(when
(and
(Flag260-)
)
(and
(RIGHTOF3-ROBOT)
(NOT-RIGHTOF4-ROBOT)
(not (NOT-RIGHTOF3-ROBOT))
(not (RIGHTOF4-ROBOT))
)
)
(when
(and
(Flag259-)
)
(and
(RIGHTOF2-ROBOT)
(NOT-RIGHTOF3-ROBOT)
(not (NOT-RIGHTOF2-ROBOT))
(not (RIGHTOF3-ROBOT))
)
)
(when
(and
(Flag258-)
)
(and
(RIGHTOF1-ROBOT)
(NOT-RIGHTOF2-ROBOT)
(not (NOT-RIGHTOF1-ROBOT))
(not (RIGHTOF2-ROBOT))
)
)
(when
(and
(Flag257-)
)
(and
(RIGHTOF0-ROBOT)
(NOT-RIGHTOF1-ROBOT)
(not (NOT-RIGHTOF0-ROBOT))
(not (RIGHTOF1-ROBOT))
)
)
(when
(and
(Flag256-)
)
(and
(LEFTOF17-ROBOT)
(not (NOT-LEFTOF17-ROBOT))
)
)
(when
(and
(Flag255-)
)
(and
(LEFTOF16-ROBOT)
(not (NOT-LEFTOF16-ROBOT))
)
)
(when
(and
(Flag254-)
)
(and
(LEFTOF15-ROBOT)
(not (NOT-LEFTOF15-ROBOT))
)
)
(when
(and
(Flag253-)
)
(and
(LEFTOF14-ROBOT)
(not (NOT-LEFTOF14-ROBOT))
)
)
(when
(and
(Flag252-)
)
(and
(LEFTOF13-ROBOT)
(not (NOT-LEFTOF13-ROBOT))
)
)
(when
(and
(Flag251-)
)
(and
(LEFTOF12-ROBOT)
(not (NOT-LEFTOF12-ROBOT))
)
)
(when
(and
(Flag250-)
)
(and
(LEFTOF11-ROBOT)
(not (NOT-LEFTOF11-ROBOT))
)
)
(when
(and
(Flag249-)
)
(and
(LEFTOF10-ROBOT)
(not (NOT-LEFTOF10-ROBOT))
)
)
(when
(and
(Flag248-)
)
(and
(LEFTOF9-ROBOT)
(not (NOT-LEFTOF9-ROBOT))
)
)
(when
(and
(Flag247-)
)
(and
(LEFTOF8-ROBOT)
(not (NOT-LEFTOF8-ROBOT))
)
)
(when
(and
(Flag246-)
)
(and
(LEFTOF7-ROBOT)
(not (NOT-LEFTOF7-ROBOT))
)
)
(when
(and
(Flag245-)
)
(and
(LEFTOF6-ROBOT)
(not (NOT-LEFTOF6-ROBOT))
)
)
(when
(and
(Flag244-)
)
(and
(LEFTOF5-ROBOT)
(not (NOT-LEFTOF5-ROBOT))
)
)
(when
(and
(Flag243-)
)
(and
(LEFTOF4-ROBOT)
(not (NOT-LEFTOF4-ROBOT))
)
)
(when
(and
(Flag242-)
)
(and
(LEFTOF3-ROBOT)
(not (NOT-LEFTOF3-ROBOT))
)
)
(when
(and
(Flag241-)
)
(and
(LEFTOF2-ROBOT)
(not (NOT-LEFTOF2-ROBOT))
)
)
(when
(and
(Flag240-)
)
(and
(LEFTOF1-ROBOT)
(not (NOT-LEFTOF1-ROBOT))
)
)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action CHECKCONSISTENCYACTION
:parameters ()
:precondition
(and
(NOT-ERROR-)
(CHECKCONSISTENCY-)
)
:effect
(and
(when
(and
(RIGHTOF14-ROBOT)
(LEFTOF8-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF6-ROBOT)
(LEFTOF2-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF1-ROBOT)
(ABOVEOF7-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF17-ROBOT)
(BELOWOF3-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF10-ROBOT)
(LEFTOF4-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF4-ROBOT)
(RIGHTOF15-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF6-ROBOT)
(ABOVEOF11-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF15-ROBOT)
(LEFTOF7-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF4-ROBOT)
(RIGHTOF16-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF15-ROBOT)
(RIGHTOF17-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF2-ROBOT)
(ABOVEOF2-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF14-ROBOT)
(LEFTOF6-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF8-ROBOT)
(RIGHTOF11-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF17-ROBOT)
(LEFTOF5-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF12-ROBOT)
(BELOWOF4-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF2-ROBOT)
(ABOVEOF8-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF8-ROBOT)
(LEFTOF2-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF16-ROBOT)
(LEFTOF5-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF16-ROBOT)
(BELOWOF7-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF2-ROBOT)
(ABOVEOF17-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF7-ROBOT)
(LEFTOF1-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF8-ROBOT)
(RIGHTOF12-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF1-ROBOT)
(RIGHTOF14-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF15-ROBOT)
(ABOVEOF16-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF15-ROBOT)
(BELOWOF14-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF12-ROBOT)
(BELOWOF2-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF7-ROBOT)
(RIGHTOF17-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF13-ROBOT)
(LEFTOF6-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF9-ROBOT)
(ABOVEOF17-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF10-ROBOT)
(ABOVEOF17-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF15-ROBOT)
(BELOWOF6-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF1-ROBOT)
(ABOVEOF10-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF9-ROBOT)
(LEFTOF1-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF6-ROBOT)
(RIGHTOF16-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF14-ROBOT)
(LEFTOF3-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF5-ROBOT)
(RIGHTOF8-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF4-ROBOT)
(ABOVEOF11-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF4-ROBOT)
(ABOVEOF10-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF7-ROBOT)
(RIGHTOF13-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF8-ROBOT)
(RIGHTOF17-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF2-ROBOT)
(ABOVEOF15-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF14-ROBOT)
(BELOWOF7-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF15-ROBOT)
(LEFTOF5-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF14-ROBOT)
(BELOWOF14-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF14-ROBOT)
(RIGHTOF16-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF11-ROBOT)
(BELOWOF1-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF2-ROBOT)
(RIGHTOF14-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF5-ROBOT)
(ABOVEOF11-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF17-ROBOT)
(LEFTOF3-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF5-ROBOT)
(ABOVEOF17-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF3-ROBOT)
(RIGHTOF9-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF15-ROBOT)
(LEFTOF1-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF17-ROBOT)
(BELOWOF1-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF6-ROBOT)
(LEFTOF4-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF6-ROBOT)
(ABOVEOF13-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF8-ROBOT)
(BELOWOF1-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF7-ROBOT)
(BELOWOF2-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF14-ROBOT)
(BELOWOF4-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF6-ROBOT)
(LEFTOF5-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF14-ROBOT)
(BELOWOF1-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF13-ROBOT)
(RIGHTOF17-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF5-ROBOT)
(RIGHTOF13-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF9-ROBOT)
(RIGHTOF16-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF15-ROBOT)
(BELOWOF4-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF10-ROBOT)
(BELOWOF3-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF13-ROBOT)
(BELOWOF1-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF1-ROBOT)
(RIGHTOF16-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF13-ROBOT)
(BELOWOF7-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF7-ROBOT)
(ABOVEOF11-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF15-ROBOT)
(BELOWOF1-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF14-ROBOT)
(BELOWOF10-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF16-ROBOT)
(BELOWOF8-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF16-ROBOT)
(BELOWOF5-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF16-ROBOT)
(LEFTOF8-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF17-ROBOT)
(LEFTOF6-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF13-ROBOT)
(BELOWOF12-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF16-ROBOT)
(LEFTOF3-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF16-ROBOT)
(BELOWOF4-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF8-ROBOT)
(BELOWOF5-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF9-ROBOT)
(BELOWOF7-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF17-ROBOT)
(LEFTOF2-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF17-ROBOT)
(BELOWOF12-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF16-ROBOT)
(LEFTOF10-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF17-ROBOT)
(LEFTOF10-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF6-ROBOT)
(ABOVEOF16-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF16-ROBOT)
(LEFTOF11-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF17-ROBOT)
(BELOWOF8-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF14-ROBOT)
(BELOWOF8-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF5-ROBOT)
(RIGHTOF10-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF10-ROBOT)
(RIGHTOF12-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF13-ROBOT)
(LEFTOF11-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF11-ROBOT)
(BELOWOF3-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF1-ROBOT)
(RIGHTOF17-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF5-ROBOT)
(RIGHTOF12-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF4-ROBOT)
(RIGHTOF14-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF8-ROBOT)
(RIGHTOF13-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF3-ROBOT)
(RIGHTOF11-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF7-ROBOT)
(RIGHTOF9-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF4-ROBOT)
(LEFTOF3-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF6-ROBOT)
(ABOVEOF12-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF8-ROBOT)
(LEFTOF1-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF9-ROBOT)
(LEFTOF4-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF2-ROBOT)
(RIGHTOF5-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF10-ROBOT)
(BELOWOF5-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF15-ROBOT)
(LEFTOF9-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF16-ROBOT)
(LEFTOF2-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF3-ROBOT)
(ABOVEOF14-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF16-ROBOT)
(BELOWOF14-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF9-ROBOT)
(BELOWOF1-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF9-ROBOT)
(LEFTOF2-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF10-ROBOT)
(ABOVEOF13-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF15-ROBOT)
(LEFTOF2-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF1-ROBOT)
(ABOVEOF16-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF13-ROBOT)
(BELOWOF3-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF11-ROBOT)
(ABOVEOF17-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF2-ROBOT)
(RIGHTOF10-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF7-ROBOT)
(ABOVEOF17-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF7-ROBOT)
(RIGHTOF14-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF3-ROBOT)
(ABOVEOF15-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF16-ROBOT)
(LEFTOF7-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF10-ROBOT)
(LEFTOF3-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF6-ROBOT)
(LEFTOF1-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF4-ROBOT)
(RIGHTOF12-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF2-ROBOT)
(RIGHTOF3-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF6-ROBOT)
(ABOVEOF17-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF7-ROBOT)
(ABOVEOF15-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF8-ROBOT)
(ABOVEOF9-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF8-ROBOT)
(ABOVEOF15-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF10-ROBOT)
(LEFTOF10-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF9-ROBOT)
(LEFTOF8-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF15-ROBOT)
(BELOWOF9-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF14-ROBOT)
(BELOWOF2-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF14-ROBOT)
(LEFTOF5-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF17-ROBOT)
(ABOVEOF17-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF11-ROBOT)
(RIGHTOF17-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF8-ROBOT)
(ABOVEOF12-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF6-ROBOT)
(BELOWOF3-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF17-ROBOT)
(LEFTOF9-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF9-ROBOT)
(RIGHTOF11-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF14-ROBOT)
(LEFTOF9-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF15-ROBOT)
(LEFTOF8-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF4-ROBOT)
(RIGHTOF13-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF7-ROBOT)
(ABOVEOF12-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF5-ROBOT)
(BELOWOF3-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF12-ROBOT)
(RIGHTOF16-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF12-ROBOT)
(LEFTOF3-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF13-ROBOT)
(LEFTOF2-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF8-ROBOT)
(BELOWOF3-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF10-ROBOT)
(LEFTOF1-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF15-ROBOT)
(LEFTOF3-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF4-ROBOT)
(ABOVEOF13-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF15-ROBOT)
(LEFTOF13-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF11-ROBOT)
(LEFTOF5-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF11-ROBOT)
(BELOWOF2-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF4-ROBOT)
(RIGHTOF8-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF15-ROBOT)
(LEFTOF12-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF13-ROBOT)
(BELOWOF5-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF6-ROBOT)
(BELOWOF4-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF11-ROBOT)
(ABOVEOF13-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF11-ROBOT)
(ABOVEOF12-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF6-ROBOT)
(RIGHTOF10-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF10-ROBOT)
(LEFTOF7-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF13-ROBOT)
(LEFTOF3-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF11-ROBOT)
(LEFTOF1-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF17-ROBOT)
(LEFTOF4-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF2-ROBOT)
(RIGHTOF7-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF11-ROBOT)
(ABOVEOF14-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF1-ROBOT)
(ABOVEOF4-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF15-ROBOT)
(BELOWOF12-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF4-ROBOT)
(LEFTOF2-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF5-ROBOT)
(ABOVEOF6-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF14-ROBOT)
(BELOWOF9-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF7-ROBOT)
(BELOWOF5-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF16-ROBOT)
(BELOWOF9-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF4-ROBOT)
(ABOVEOF9-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF6-ROBOT)
(LEFTOF6-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF12-ROBOT)
(LEFTOF1-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF12-ROBOT)
(LEFTOF2-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF16-ROBOT)
(BELOWOF13-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF10-ROBOT)
(BELOWOF6-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF17-ROBOT)
(LEFTOF12-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF9-ROBOT)
(RIGHTOF13-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF4-ROBOT)
(RIGHTOF11-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF1-ROBOT)
(RIGHTOF4-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF7-ROBOT)
(BELOWOF6-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF7-ROBOT)
(LEFTOF3-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF11-ROBOT)
(ABOVEOF15-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF1-ROBOT)
(RIGHTOF2-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF4-ROBOT)
(ABOVEOF5-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF12-ROBOT)
(LEFTOF7-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF5-ROBOT)
(ABOVEOF14-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF2-ROBOT)
(ABOVEOF16-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF10-ROBOT)
(ABOVEOF12-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF9-ROBOT)
(ABOVEOF13-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF10-ROBOT)
(ABOVEOF15-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF2-ROBOT)
(ABOVEOF9-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF2-ROBOT)
(ABOVEOF6-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF9-ROBOT)
(LEFTOF5-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF5-ROBOT)
(BELOWOF5-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF4-ROBOT)
(ABOVEOF17-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF11-ROBOT)
(RIGHTOF15-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF1-ROBOT)
(ABOVEOF5-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF3-ROBOT)
(RIGHTOF8-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF6-ROBOT)
(BELOWOF1-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF16-ROBOT)
(LEFTOF15-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF7-ROBOT)
(BELOWOF3-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF16-ROBOT)
(BELOWOF3-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF5-ROBOT)
(LEFTOF1-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF7-ROBOT)
(BELOWOF7-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF5-ROBOT)
(RIGHTOF7-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF8-ROBOT)
(BELOWOF4-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF12-ROBOT)
(LEFTOF6-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF2-ROBOT)
(ABOVEOF5-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF11-ROBOT)
(ABOVEOF16-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF1-ROBOT)
(ABOVEOF12-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF15-ROBOT)
(LEFTOF6-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF14-ROBOT)
(LEFTOF11-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF6-ROBOT)
(BELOWOF6-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF1-ROBOT)
(BELOWOF1-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF11-ROBOT)
(BELOWOF9-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF8-ROBOT)
(ABOVEOF13-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF2-ROBOT)
(ABOVEOF13-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF5-ROBOT)
(ABOVEOF15-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF10-ROBOT)
(LEFTOF8-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF9-ROBOT)
(BELOWOF3-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF6-ROBOT)
(ABOVEOF14-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF13-ROBOT)
(LEFTOF1-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF3-ROBOT)
(RIGHTOF6-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF10-ROBOT)
(ABOVEOF16-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF14-ROBOT)
(RIGHTOF15-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF10-ROBOT)
(RIGHTOF15-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF9-ROBOT)
(BELOWOF5-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF2-ROBOT)
(ABOVEOF10-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF1-ROBOT)
(ABOVEOF2-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF15-ROBOT)
(BELOWOF13-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF6-ROBOT)
(RIGHTOF9-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF14-ROBOT)
(RIGHTOF14-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF12-ROBOT)
(BELOWOF5-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF10-ROBOT)
(BELOWOF7-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF12-ROBOT)
(RIGHTOF14-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF6-ROBOT)
(ABOVEOF9-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF7-ROBOT)
(RIGHTOF11-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF14-ROBOT)
(BELOWOF12-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF8-ROBOT)
(ABOVEOF10-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF6-ROBOT)
(RIGHTOF8-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF12-ROBOT)
(LEFTOF12-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF9-ROBOT)
(ABOVEOF12-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF17-ROBOT)
(LEFTOF14-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF8-ROBOT)
(ABOVEOF11-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF10-ROBOT)
(BELOWOF9-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF10-ROBOT)
(RIGHTOF11-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF17-ROBOT)
(RIGHTOF17-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF12-ROBOT)
(BELOWOF3-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF5-ROBOT)
(LEFTOF3-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF17-ROBOT)
(BELOWOF14-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF4-ROBOT)
(ABOVEOF7-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF4-ROBOT)
(BELOWOF3-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF1-ROBOT)
(RIGHTOF3-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF4-ROBOT)
(BELOWOF4-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF11-ROBOT)
(RIGHTOF11-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF17-ROBOT)
(LEFTOF16-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF7-ROBOT)
(RIGHTOF8-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF10-ROBOT)
(RIGHTOF14-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF12-ROBOT)
(LEFTOF9-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF5-ROBOT)
(RIGHTOF5-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF10-ROBOT)
(ABOVEOF11-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF14-ROBOT)
(BELOWOF13-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF13-ROBOT)
(ABOVEOF17-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF11-ROBOT)
(RIGHTOF12-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF6-ROBOT)
(RIGHTOF11-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF10-ROBOT)
(RIGHTOF13-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF2-ROBOT)
(RIGHTOF11-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF4-ROBOT)
(RIGHTOF7-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF10-ROBOT)
(BELOWOF10-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF5-ROBOT)
(LEFTOF4-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF12-ROBOT)
(ABOVEOF16-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF15-ROBOT)
(ABOVEOF17-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF10-ROBOT)
(LEFTOF9-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF3-ROBOT)
(RIGHTOF3-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF1-ROBOT)
(ABOVEOF3-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF3-ROBOT)
(BELOWOF2-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF14-ROBOT)
(LEFTOF13-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF7-ROBOT)
(LEFTOF6-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF4-ROBOT)
(BELOWOF2-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF8-ROBOT)
(BELOWOF6-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF16-ROBOT)
(LEFTOF13-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF7-ROBOT)
(ABOVEOF8-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF3-ROBOT)
(ABOVEOF3-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF15-ROBOT)
(BELOWOF15-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF15-ROBOT)
(RIGHTOF15-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF13-ROBOT)
(RIGHTOF13-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF4-ROBOT)
(RIGHTOF4-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF16-ROBOT)
(BELOWOF16-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF7-ROBOT)
(RIGHTOF7-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF8-ROBOT)
(BELOWOF8-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF8-ROBOT)
(LEFTOF8-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF16-ROBOT)
(LEFTOF16-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF16-ROBOT)
(ABOVEOF17-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF13-ROBOT)
(ABOVEOF13-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF9-ROBOT)
(BELOWOF9-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF11-ROBOT)
(BELOWOF11-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF13-ROBOT)
(LEFTOF12-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF12-ROBOT)
(ABOVEOF12-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF9-ROBOT)
(RIGHTOF9-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF2-ROBOT)
(LEFTOF2-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF1-ROBOT)
(RIGHTOF1-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(NOT-CHECKCONSISTENCY-)
(not (CHECKCONSISTENCY-))
)
)
(:derived (Flag3-)(and (NOT-CHECKCONSISTENCY-)(NOT-ERROR-)(Flag2-)))

(:derived (Flag5-)(and (BELOWOF2-ROBOT)))

(:derived (Flag5-)(and (BELOWOF1-ROBOT)))

(:derived (Flag6-)(and (BELOWOF2-ROBOT)))

(:derived (Flag6-)(and (BELOWOF1-ROBOT)))

(:derived (Flag6-)(and (BELOWOF3-ROBOT)))

(:derived (Flag7-)(and (BELOWOF2-ROBOT)))

(:derived (Flag7-)(and (BELOWOF1-ROBOT)))

(:derived (Flag7-)(and (BELOWOF3-ROBOT)))

(:derived (Flag7-)(and (BELOWOF4-ROBOT)))

(:derived (Flag8-)(and (BELOWOF2-ROBOT)))

(:derived (Flag8-)(and (BELOWOF1-ROBOT)))

(:derived (Flag8-)(and (BELOWOF3-ROBOT)))

(:derived (Flag8-)(and (BELOWOF4-ROBOT)))

(:derived (Flag8-)(and (BELOWOF5-ROBOT)))

(:derived (Flag9-)(and (BELOWOF6-ROBOT)))

(:derived (Flag9-)(and (BELOWOF5-ROBOT)))

(:derived (Flag9-)(and (BELOWOF2-ROBOT)))

(:derived (Flag9-)(and (BELOWOF1-ROBOT)))

(:derived (Flag9-)(and (BELOWOF4-ROBOT)))

(:derived (Flag9-)(and (BELOWOF3-ROBOT)))

(:derived (Flag10-)(and (BELOWOF6-ROBOT)))

(:derived (Flag10-)(and (BELOWOF5-ROBOT)))

(:derived (Flag10-)(and (BELOWOF2-ROBOT)))

(:derived (Flag10-)(and (BELOWOF1-ROBOT)))

(:derived (Flag10-)(and (BELOWOF4-ROBOT)))

(:derived (Flag10-)(and (BELOWOF7-ROBOT)))

(:derived (Flag10-)(and (BELOWOF3-ROBOT)))

(:derived (Flag11-)(and (BELOWOF6-ROBOT)))

(:derived (Flag11-)(and (BELOWOF5-ROBOT)))

(:derived (Flag11-)(and (BELOWOF2-ROBOT)))

(:derived (Flag11-)(and (BELOWOF1-ROBOT)))

(:derived (Flag11-)(and (BELOWOF4-ROBOT)))

(:derived (Flag11-)(and (BELOWOF7-ROBOT)))

(:derived (Flag11-)(and (BELOWOF3-ROBOT)))

(:derived (Flag11-)(and (BELOWOF8-ROBOT)))

(:derived (Flag12-)(and (BELOWOF6-ROBOT)))

(:derived (Flag12-)(and (BELOWOF5-ROBOT)))

(:derived (Flag12-)(and (BELOWOF2-ROBOT)))

(:derived (Flag12-)(and (BELOWOF1-ROBOT)))

(:derived (Flag12-)(and (BELOWOF4-ROBOT)))

(:derived (Flag12-)(and (BELOWOF7-ROBOT)))

(:derived (Flag12-)(and (BELOWOF9-ROBOT)))

(:derived (Flag12-)(and (BELOWOF3-ROBOT)))

(:derived (Flag12-)(and (BELOWOF8-ROBOT)))

(:derived (Flag13-)(and (BELOWOF10-ROBOT)))

(:derived (Flag13-)(and (BELOWOF6-ROBOT)))

(:derived (Flag13-)(and (BELOWOF5-ROBOT)))

(:derived (Flag13-)(and (BELOWOF2-ROBOT)))

(:derived (Flag13-)(and (BELOWOF1-ROBOT)))

(:derived (Flag13-)(and (BELOWOF4-ROBOT)))

(:derived (Flag13-)(and (BELOWOF7-ROBOT)))

(:derived (Flag13-)(and (BELOWOF9-ROBOT)))

(:derived (Flag13-)(and (BELOWOF3-ROBOT)))

(:derived (Flag13-)(and (BELOWOF8-ROBOT)))

(:derived (Flag14-)(and (BELOWOF10-ROBOT)))

(:derived (Flag14-)(and (BELOWOF11-ROBOT)))

(:derived (Flag14-)(and (BELOWOF6-ROBOT)))

(:derived (Flag14-)(and (BELOWOF5-ROBOT)))

(:derived (Flag14-)(and (BELOWOF2-ROBOT)))

(:derived (Flag14-)(and (BELOWOF1-ROBOT)))

(:derived (Flag14-)(and (BELOWOF4-ROBOT)))

(:derived (Flag14-)(and (BELOWOF7-ROBOT)))

(:derived (Flag14-)(and (BELOWOF9-ROBOT)))

(:derived (Flag14-)(and (BELOWOF3-ROBOT)))

(:derived (Flag14-)(and (BELOWOF8-ROBOT)))

(:derived (Flag15-)(and (BELOWOF10-ROBOT)))

(:derived (Flag15-)(and (BELOWOF11-ROBOT)))

(:derived (Flag15-)(and (BELOWOF6-ROBOT)))

(:derived (Flag15-)(and (BELOWOF5-ROBOT)))

(:derived (Flag15-)(and (BELOWOF2-ROBOT)))

(:derived (Flag15-)(and (BELOWOF1-ROBOT)))

(:derived (Flag15-)(and (BELOWOF4-ROBOT)))

(:derived (Flag15-)(and (BELOWOF12-ROBOT)))

(:derived (Flag15-)(and (BELOWOF7-ROBOT)))

(:derived (Flag15-)(and (BELOWOF9-ROBOT)))

(:derived (Flag15-)(and (BELOWOF3-ROBOT)))

(:derived (Flag15-)(and (BELOWOF8-ROBOT)))

(:derived (Flag16-)(and (BELOWOF10-ROBOT)))

(:derived (Flag16-)(and (BELOWOF11-ROBOT)))

(:derived (Flag16-)(and (BELOWOF6-ROBOT)))

(:derived (Flag16-)(and (BELOWOF5-ROBOT)))

(:derived (Flag16-)(and (BELOWOF2-ROBOT)))

(:derived (Flag16-)(and (BELOWOF1-ROBOT)))

(:derived (Flag16-)(and (BELOWOF4-ROBOT)))

(:derived (Flag16-)(and (BELOWOF12-ROBOT)))

(:derived (Flag16-)(and (BELOWOF7-ROBOT)))

(:derived (Flag16-)(and (BELOWOF9-ROBOT)))

(:derived (Flag16-)(and (BELOWOF3-ROBOT)))

(:derived (Flag16-)(and (BELOWOF13-ROBOT)))

(:derived (Flag16-)(and (BELOWOF8-ROBOT)))

(:derived (Flag17-)(and (BELOWOF10-ROBOT)))

(:derived (Flag17-)(and (BELOWOF11-ROBOT)))

(:derived (Flag17-)(and (BELOWOF6-ROBOT)))

(:derived (Flag17-)(and (BELOWOF5-ROBOT)))

(:derived (Flag17-)(and (BELOWOF2-ROBOT)))

(:derived (Flag17-)(and (BELOWOF1-ROBOT)))

(:derived (Flag17-)(and (BELOWOF4-ROBOT)))

(:derived (Flag17-)(and (BELOWOF12-ROBOT)))

(:derived (Flag17-)(and (BELOWOF7-ROBOT)))

(:derived (Flag17-)(and (BELOWOF9-ROBOT)))

(:derived (Flag17-)(and (BELOWOF3-ROBOT)))

(:derived (Flag17-)(and (BELOWOF13-ROBOT)))

(:derived (Flag17-)(and (BELOWOF8-ROBOT)))

(:derived (Flag17-)(and (BELOWOF14-ROBOT)))

(:derived (Flag18-)(and (BELOWOF10-ROBOT)))

(:derived (Flag18-)(and (BELOWOF11-ROBOT)))

(:derived (Flag18-)(and (BELOWOF15-ROBOT)))

(:derived (Flag18-)(and (BELOWOF6-ROBOT)))

(:derived (Flag18-)(and (BELOWOF5-ROBOT)))

(:derived (Flag18-)(and (BELOWOF2-ROBOT)))

(:derived (Flag18-)(and (BELOWOF1-ROBOT)))

(:derived (Flag18-)(and (BELOWOF4-ROBOT)))

(:derived (Flag18-)(and (BELOWOF12-ROBOT)))

(:derived (Flag18-)(and (BELOWOF7-ROBOT)))

(:derived (Flag18-)(and (BELOWOF9-ROBOT)))

(:derived (Flag18-)(and (BELOWOF3-ROBOT)))

(:derived (Flag18-)(and (BELOWOF13-ROBOT)))

(:derived (Flag18-)(and (BELOWOF8-ROBOT)))

(:derived (Flag18-)(and (BELOWOF14-ROBOT)))

(:derived (Flag19-)(and (BELOWOF10-ROBOT)))

(:derived (Flag19-)(and (BELOWOF16-ROBOT)))

(:derived (Flag19-)(and (BELOWOF15-ROBOT)))

(:derived (Flag19-)(and (BELOWOF6-ROBOT)))

(:derived (Flag19-)(and (BELOWOF11-ROBOT)))

(:derived (Flag19-)(and (BELOWOF5-ROBOT)))

(:derived (Flag19-)(and (BELOWOF2-ROBOT)))

(:derived (Flag19-)(and (BELOWOF1-ROBOT)))

(:derived (Flag19-)(and (BELOWOF4-ROBOT)))

(:derived (Flag19-)(and (BELOWOF12-ROBOT)))

(:derived (Flag19-)(and (BELOWOF7-ROBOT)))

(:derived (Flag19-)(and (BELOWOF9-ROBOT)))

(:derived (Flag19-)(and (BELOWOF3-ROBOT)))

(:derived (Flag19-)(and (BELOWOF13-ROBOT)))

(:derived (Flag19-)(and (BELOWOF8-ROBOT)))

(:derived (Flag19-)(and (BELOWOF14-ROBOT)))

(:derived (Flag20-)(and (BELOWOF10-ROBOT)))

(:derived (Flag20-)(and (BELOWOF16-ROBOT)))

(:derived (Flag20-)(and (BELOWOF15-ROBOT)))

(:derived (Flag20-)(and (BELOWOF6-ROBOT)))

(:derived (Flag20-)(and (BELOWOF5-ROBOT)))

(:derived (Flag20-)(and (BELOWOF2-ROBOT)))

(:derived (Flag20-)(and (BELOWOF1-ROBOT)))

(:derived (Flag20-)(and (BELOWOF4-ROBOT)))

(:derived (Flag20-)(and (BELOWOF11-ROBOT)))

(:derived (Flag20-)(and (BELOWOF12-ROBOT)))

(:derived (Flag20-)(and (BELOWOF7-ROBOT)))

(:derived (Flag20-)(and (BELOWOF9-ROBOT)))

(:derived (Flag20-)(and (BELOWOF3-ROBOT)))

(:derived (Flag20-)(and (BELOWOF13-ROBOT)))

(:derived (Flag20-)(and (BELOWOF8-ROBOT)))

(:derived (Flag20-)(and (BELOWOF14-ROBOT)))

(:derived (Flag21-)(and (BELOWOF10-ROBOT)))

(:derived (Flag21-)(and (BELOWOF16-ROBOT)))

(:derived (Flag21-)(and (BELOWOF15-ROBOT)))

(:derived (Flag21-)(and (BELOWOF6-ROBOT)))

(:derived (Flag21-)(and (BELOWOF5-ROBOT)))

(:derived (Flag21-)(and (BELOWOF2-ROBOT)))

(:derived (Flag21-)(and (BELOWOF1-ROBOT)))

(:derived (Flag21-)(and (BELOWOF4-ROBOT)))

(:derived (Flag21-)(and (BELOWOF11-ROBOT)))

(:derived (Flag21-)(and (BELOWOF18-ROBOT)))

(:derived (Flag21-)(and (BELOWOF12-ROBOT)))

(:derived (Flag21-)(and (BELOWOF7-ROBOT)))

(:derived (Flag21-)(and (BELOWOF9-ROBOT)))

(:derived (Flag21-)(and (BELOWOF3-ROBOT)))

(:derived (Flag21-)(and (BELOWOF13-ROBOT)))

(:derived (Flag21-)(and (BELOWOF8-ROBOT)))

(:derived (Flag21-)(and (BELOWOF14-ROBOT)))

(:derived (Flag22-)(and (ABOVEOF6-ROBOT)))

(:derived (Flag22-)(and (ABOVEOF11-ROBOT)))

(:derived (Flag22-)(and (ABOVEOF10-ROBOT)))

(:derived (Flag22-)(and (ABOVEOF2-ROBOT)))

(:derived (Flag22-)(and (ABOVEOF7-ROBOT)))

(:derived (Flag22-)(and (ABOVEOF12-ROBOT)))

(:derived (Flag22-)(and (ABOVEOF17-ROBOT)))

(:derived (Flag22-)(and (ABOVEOF15-ROBOT)))

(:derived (Flag22-)(and (ABOVEOF8-ROBOT)))

(:derived (Flag22-)(and (ABOVEOF9-ROBOT)))

(:derived (Flag22-)(and (ABOVEOF16-ROBOT)))

(:derived (Flag22-)(and (ABOVEOF4-ROBOT)))

(:derived (Flag22-)(and (ABOVEOF3-ROBOT)))

(:derived (Flag22-)(and (ABOVEOF13-ROBOT)))

(:derived (Flag22-)(and (ABOVEOF14-ROBOT)))

(:derived (Flag22-)(and (ABOVEOF5-ROBOT)))

(:derived (Flag22-)(and (ABOVEOF1-ROBOT)))

(:derived (Flag23-)(and (ABOVEOF11-ROBOT)))

(:derived (Flag23-)(and (ABOVEOF6-ROBOT)))

(:derived (Flag23-)(and (ABOVEOF10-ROBOT)))

(:derived (Flag23-)(and (ABOVEOF2-ROBOT)))

(:derived (Flag23-)(and (ABOVEOF7-ROBOT)))

(:derived (Flag23-)(and (ABOVEOF12-ROBOT)))

(:derived (Flag23-)(and (ABOVEOF17-ROBOT)))

(:derived (Flag23-)(and (ABOVEOF15-ROBOT)))

(:derived (Flag23-)(and (ABOVEOF8-ROBOT)))

(:derived (Flag23-)(and (ABOVEOF9-ROBOT)))

(:derived (Flag23-)(and (ABOVEOF16-ROBOT)))

(:derived (Flag23-)(and (ABOVEOF4-ROBOT)))

(:derived (Flag23-)(and (ABOVEOF3-ROBOT)))

(:derived (Flag23-)(and (ABOVEOF13-ROBOT)))

(:derived (Flag23-)(and (ABOVEOF14-ROBOT)))

(:derived (Flag23-)(and (ABOVEOF5-ROBOT)))

(:derived (Flag24-)(and (ABOVEOF11-ROBOT)))

(:derived (Flag24-)(and (ABOVEOF6-ROBOT)))

(:derived (Flag24-)(and (ABOVEOF10-ROBOT)))

(:derived (Flag24-)(and (ABOVEOF8-ROBOT)))

(:derived (Flag24-)(and (ABOVEOF7-ROBOT)))

(:derived (Flag24-)(and (ABOVEOF12-ROBOT)))

(:derived (Flag24-)(and (ABOVEOF17-ROBOT)))

(:derived (Flag24-)(and (ABOVEOF15-ROBOT)))

(:derived (Flag24-)(and (ABOVEOF9-ROBOT)))

(:derived (Flag24-)(and (ABOVEOF16-ROBOT)))

(:derived (Flag24-)(and (ABOVEOF4-ROBOT)))

(:derived (Flag24-)(and (ABOVEOF3-ROBOT)))

(:derived (Flag24-)(and (ABOVEOF13-ROBOT)))

(:derived (Flag24-)(and (ABOVEOF14-ROBOT)))

(:derived (Flag24-)(and (ABOVEOF5-ROBOT)))

(:derived (Flag25-)(and (ABOVEOF11-ROBOT)))

(:derived (Flag25-)(and (ABOVEOF6-ROBOT)))

(:derived (Flag25-)(and (ABOVEOF10-ROBOT)))

(:derived (Flag25-)(and (ABOVEOF8-ROBOT)))

(:derived (Flag25-)(and (ABOVEOF7-ROBOT)))

(:derived (Flag25-)(and (ABOVEOF12-ROBOT)))

(:derived (Flag25-)(and (ABOVEOF17-ROBOT)))

(:derived (Flag25-)(and (ABOVEOF15-ROBOT)))

(:derived (Flag25-)(and (ABOVEOF9-ROBOT)))

(:derived (Flag25-)(and (ABOVEOF16-ROBOT)))

(:derived (Flag25-)(and (ABOVEOF4-ROBOT)))

(:derived (Flag25-)(and (ABOVEOF13-ROBOT)))

(:derived (Flag25-)(and (ABOVEOF14-ROBOT)))

(:derived (Flag25-)(and (ABOVEOF5-ROBOT)))

(:derived (Flag26-)(and (ABOVEOF11-ROBOT)))

(:derived (Flag26-)(and (ABOVEOF6-ROBOT)))

(:derived (Flag26-)(and (ABOVEOF7-ROBOT)))

(:derived (Flag26-)(and (ABOVEOF8-ROBOT)))

(:derived (Flag26-)(and (ABOVEOF10-ROBOT)))

(:derived (Flag26-)(and (ABOVEOF12-ROBOT)))

(:derived (Flag26-)(and (ABOVEOF17-ROBOT)))

(:derived (Flag26-)(and (ABOVEOF15-ROBOT)))

(:derived (Flag26-)(and (ABOVEOF9-ROBOT)))

(:derived (Flag26-)(and (ABOVEOF16-ROBOT)))

(:derived (Flag26-)(and (ABOVEOF13-ROBOT)))

(:derived (Flag26-)(and (ABOVEOF14-ROBOT)))

(:derived (Flag26-)(and (ABOVEOF5-ROBOT)))

(:derived (Flag27-)(and (ABOVEOF11-ROBOT)))

(:derived (Flag27-)(and (ABOVEOF6-ROBOT)))

(:derived (Flag27-)(and (ABOVEOF7-ROBOT)))

(:derived (Flag27-)(and (ABOVEOF8-ROBOT)))

(:derived (Flag27-)(and (ABOVEOF10-ROBOT)))

(:derived (Flag27-)(and (ABOVEOF12-ROBOT)))

(:derived (Flag27-)(and (ABOVEOF17-ROBOT)))

(:derived (Flag27-)(and (ABOVEOF15-ROBOT)))

(:derived (Flag27-)(and (ABOVEOF9-ROBOT)))

(:derived (Flag27-)(and (ABOVEOF16-ROBOT)))

(:derived (Flag27-)(and (ABOVEOF13-ROBOT)))

(:derived (Flag27-)(and (ABOVEOF14-ROBOT)))

(:derived (Flag28-)(and (ABOVEOF11-ROBOT)))

(:derived (Flag28-)(and (ABOVEOF7-ROBOT)))

(:derived (Flag28-)(and (ABOVEOF8-ROBOT)))

(:derived (Flag28-)(and (ABOVEOF10-ROBOT)))

(:derived (Flag28-)(and (ABOVEOF12-ROBOT)))

(:derived (Flag28-)(and (ABOVEOF17-ROBOT)))

(:derived (Flag28-)(and (ABOVEOF15-ROBOT)))

(:derived (Flag28-)(and (ABOVEOF9-ROBOT)))

(:derived (Flag28-)(and (ABOVEOF16-ROBOT)))

(:derived (Flag28-)(and (ABOVEOF13-ROBOT)))

(:derived (Flag28-)(and (ABOVEOF14-ROBOT)))

(:derived (Flag29-)(and (ABOVEOF11-ROBOT)))

(:derived (Flag29-)(and (ABOVEOF8-ROBOT)))

(:derived (Flag29-)(and (ABOVEOF10-ROBOT)))

(:derived (Flag29-)(and (ABOVEOF12-ROBOT)))

(:derived (Flag29-)(and (ABOVEOF17-ROBOT)))

(:derived (Flag29-)(and (ABOVEOF15-ROBOT)))

(:derived (Flag29-)(and (ABOVEOF9-ROBOT)))

(:derived (Flag29-)(and (ABOVEOF16-ROBOT)))

(:derived (Flag29-)(and (ABOVEOF13-ROBOT)))

(:derived (Flag29-)(and (ABOVEOF14-ROBOT)))

(:derived (Flag30-)(and (ABOVEOF11-ROBOT)))

(:derived (Flag30-)(and (ABOVEOF10-ROBOT)))

(:derived (Flag30-)(and (ABOVEOF12-ROBOT)))

(:derived (Flag30-)(and (ABOVEOF17-ROBOT)))

(:derived (Flag30-)(and (ABOVEOF15-ROBOT)))

(:derived (Flag30-)(and (ABOVEOF9-ROBOT)))

(:derived (Flag30-)(and (ABOVEOF16-ROBOT)))

(:derived (Flag30-)(and (ABOVEOF13-ROBOT)))

(:derived (Flag30-)(and (ABOVEOF14-ROBOT)))

(:derived (Flag31-)(and (ABOVEOF11-ROBOT)))

(:derived (Flag31-)(and (ABOVEOF10-ROBOT)))

(:derived (Flag31-)(and (ABOVEOF12-ROBOT)))

(:derived (Flag31-)(and (ABOVEOF17-ROBOT)))

(:derived (Flag31-)(and (ABOVEOF15-ROBOT)))

(:derived (Flag31-)(and (ABOVEOF16-ROBOT)))

(:derived (Flag31-)(and (ABOVEOF13-ROBOT)))

(:derived (Flag31-)(and (ABOVEOF14-ROBOT)))

(:derived (Flag32-)(and (ABOVEOF11-ROBOT)))

(:derived (Flag32-)(and (ABOVEOF12-ROBOT)))

(:derived (Flag32-)(and (ABOVEOF17-ROBOT)))

(:derived (Flag32-)(and (ABOVEOF15-ROBOT)))

(:derived (Flag32-)(and (ABOVEOF16-ROBOT)))

(:derived (Flag32-)(and (ABOVEOF13-ROBOT)))

(:derived (Flag32-)(and (ABOVEOF14-ROBOT)))

(:derived (Flag33-)(and (ABOVEOF12-ROBOT)))

(:derived (Flag33-)(and (ABOVEOF17-ROBOT)))

(:derived (Flag33-)(and (ABOVEOF15-ROBOT)))

(:derived (Flag33-)(and (ABOVEOF16-ROBOT)))

(:derived (Flag33-)(and (ABOVEOF13-ROBOT)))

(:derived (Flag33-)(and (ABOVEOF14-ROBOT)))

(:derived (Flag34-)(and (ABOVEOF15-ROBOT)))

(:derived (Flag34-)(and (ABOVEOF17-ROBOT)))

(:derived (Flag34-)(and (ABOVEOF13-ROBOT)))

(:derived (Flag34-)(and (ABOVEOF14-ROBOT)))

(:derived (Flag34-)(and (ABOVEOF16-ROBOT)))

(:derived (Flag35-)(and (ABOVEOF16-ROBOT)))

(:derived (Flag35-)(and (ABOVEOF17-ROBOT)))

(:derived (Flag35-)(and (ABOVEOF14-ROBOT)))

(:derived (Flag35-)(and (ABOVEOF15-ROBOT)))

(:derived (Flag36-)(and (ABOVEOF17-ROBOT)))

(:derived (Flag36-)(and (ABOVEOF15-ROBOT)))

(:derived (Flag36-)(and (ABOVEOF16-ROBOT)))

(:derived (Flag37-)(and (ABOVEOF17-ROBOT)))

(:derived (Flag37-)(and (ABOVEOF16-ROBOT)))

(:derived (Flag38-)(and (ABOVEOF17-ROBOT)(BELOWOF1-ROBOT)))

(:derived (Flag39-)(and (BELOWOF1-ROBOT)(ABOVEOF1-ROBOT)))

(:derived (Flag40-)(and (ABOVEOF11-ROBOT)(BELOWOF1-ROBOT)))

(:derived (Flag41-)(and (ABOVEOF1-ROBOT)(BELOWOF2-ROBOT)))

(:derived (Flag42-)(and (ABOVEOF7-ROBOT)(BELOWOF2-ROBOT)))

(:derived (Flag43-)(and (ABOVEOF15-ROBOT)(BELOWOF2-ROBOT)))

(:derived (Flag44-)(and (BELOWOF2-ROBOT)(ABOVEOF13-ROBOT)))

(:derived (Flag45-)(and (BELOWOF1-ROBOT)(ABOVEOF16-ROBOT)))

(:derived (Flag46-)(and (BELOWOF1-ROBOT)(ABOVEOF7-ROBOT)))

(:derived (Flag47-)(and (ABOVEOF2-ROBOT)(BELOWOF2-ROBOT)))

(:derived (Flag48-)(and (BELOWOF1-ROBOT)(ABOVEOF5-ROBOT)))

(:derived (Flag49-)(and (ABOVEOF16-ROBOT)(BELOWOF2-ROBOT)))

(:derived (Flag50-)(and (ABOVEOF4-ROBOT)(BELOWOF2-ROBOT)))

(:derived (Flag51-)(and (BELOWOF1-ROBOT)(ABOVEOF17-ROBOT)))

(:derived (Flag52-)(and (BELOWOF2-ROBOT)(ABOVEOF14-ROBOT)))

(:derived (Flag53-)(and (ABOVEOF6-ROBOT)(BELOWOF2-ROBOT)))

(:derived (Flag54-)(and (BELOWOF1-ROBOT)(ABOVEOF2-ROBOT)))

(:derived (Flag55-)(and (ABOVEOF9-ROBOT)(BELOWOF2-ROBOT)))

(:derived (Flag56-)(and (BELOWOF1-ROBOT)(ABOVEOF13-ROBOT)))

(:derived (Flag57-)(and (ABOVEOF6-ROBOT)(BELOWOF1-ROBOT)))

(:derived (Flag58-)(and (BELOWOF1-ROBOT)(ABOVEOF14-ROBOT)))

(:derived (Flag59-)(and (ABOVEOF17-ROBOT)(BELOWOF2-ROBOT)))

(:derived (Flag60-)(and (ABOVEOF10-ROBOT)(BELOWOF1-ROBOT)))

(:derived (Flag61-)(and (ABOVEOF11-ROBOT)(BELOWOF2-ROBOT)))

(:derived (Flag62-)(and (ABOVEOF3-ROBOT)(BELOWOF1-ROBOT)))

(:derived (Flag63-)(and (BELOWOF1-ROBOT)(ABOVEOF9-ROBOT)))

(:derived (Flag64-)(and (ABOVEOF10-ROBOT)(BELOWOF2-ROBOT)))

(:derived (Flag65-)(and (ABOVEOF8-ROBOT)(BELOWOF2-ROBOT)))

(:derived (Flag66-)(and (BELOWOF1-ROBOT)(ABOVEOF4-ROBOT)))

(:derived (Flag67-)(and (ABOVEOF12-ROBOT)(BELOWOF2-ROBOT)))

(:derived (Flag68-)(and (ABOVEOF12-ROBOT)(BELOWOF1-ROBOT)))

(:derived (Flag69-)(and (BELOWOF2-ROBOT)(ABOVEOF5-ROBOT)))

(:derived (Flag70-)(and (BELOWOF1-ROBOT)(ABOVEOF15-ROBOT)))

(:derived (Flag71-)(and (ABOVEOF8-ROBOT)(BELOWOF1-ROBOT)))

(:derived (Flag72-)(and (ABOVEOF3-ROBOT)(BELOWOF2-ROBOT)))

(:derived (Flag74-)(and (BELOWOF3-ROBOT)(ABOVEOF5-ROBOT)))

(:derived (Flag75-)(and (BELOWOF3-ROBOT)(ABOVEOF6-ROBOT)))

(:derived (Flag76-)(and (BELOWOF3-ROBOT)(ABOVEOF7-ROBOT)))

(:derived (Flag77-)(and (BELOWOF3-ROBOT)(ABOVEOF17-ROBOT)))

(:derived (Flag78-)(and (BELOWOF3-ROBOT)(ABOVEOF14-ROBOT)))

(:derived (Flag79-)(and (BELOWOF3-ROBOT)(ABOVEOF16-ROBOT)))

(:derived (Flag80-)(and (BELOWOF3-ROBOT)(ABOVEOF9-ROBOT)))

(:derived (Flag81-)(and (BELOWOF3-ROBOT)(ABOVEOF15-ROBOT)))

(:derived (Flag82-)(and (BELOWOF3-ROBOT)(ABOVEOF2-ROBOT)))

(:derived (Flag83-)(and (ABOVEOF3-ROBOT)(BELOWOF3-ROBOT)))

(:derived (Flag84-)(and (ABOVEOF8-ROBOT)(BELOWOF3-ROBOT)))

(:derived (Flag85-)(and (BELOWOF3-ROBOT)(ABOVEOF4-ROBOT)))

(:derived (Flag86-)(and (BELOWOF3-ROBOT)(ABOVEOF10-ROBOT)))

(:derived (Flag87-)(and (BELOWOF2-ROBOT)(ABOVEOF2-ROBOT)))

(:derived (Flag88-)(and (ABOVEOF12-ROBOT)(BELOWOF3-ROBOT)))

(:derived (Flag89-)(and (BELOWOF3-ROBOT)(ABOVEOF13-ROBOT)))

(:derived (Flag90-)(and (ABOVEOF11-ROBOT)(BELOWOF3-ROBOT)))

(:derived (Flag92-)(and (ABOVEOF9-ROBOT)(BELOWOF4-ROBOT)))

(:derived (Flag93-)(and (BELOWOF4-ROBOT)(ABOVEOF13-ROBOT)))

(:derived (Flag94-)(and (ABOVEOF3-ROBOT)(BELOWOF4-ROBOT)))

(:derived (Flag95-)(and (ABOVEOF17-ROBOT)(BELOWOF4-ROBOT)))

(:derived (Flag96-)(and (ABOVEOF6-ROBOT)(BELOWOF4-ROBOT)))

(:derived (Flag97-)(and (BELOWOF4-ROBOT)(ABOVEOF4-ROBOT)))

(:derived (Flag98-)(and (BELOWOF4-ROBOT)(ABOVEOF16-ROBOT)))

(:derived (Flag99-)(and (ABOVEOF8-ROBOT)(BELOWOF4-ROBOT)))

(:derived (Flag100-)(and (ABOVEOF12-ROBOT)(BELOWOF4-ROBOT)))

(:derived (Flag101-)(and (BELOWOF4-ROBOT)(ABOVEOF5-ROBOT)))

(:derived (Flag102-)(and (BELOWOF3-ROBOT)(ABOVEOF3-ROBOT)))

(:derived (Flag103-)(and (ABOVEOF10-ROBOT)(BELOWOF4-ROBOT)))

(:derived (Flag104-)(and (BELOWOF4-ROBOT)(ABOVEOF15-ROBOT)))

(:derived (Flag105-)(and (BELOWOF3-ROBOT)(ABOVEOF11-ROBOT)))

(:derived (Flag106-)(and (BELOWOF4-ROBOT)(ABOVEOF7-ROBOT)))

(:derived (Flag107-)(and (ABOVEOF11-ROBOT)(BELOWOF4-ROBOT)))

(:derived (Flag108-)(and (BELOWOF4-ROBOT)(ABOVEOF14-ROBOT)))

(:derived (Flag110-)(and (ABOVEOF17-ROBOT)(BELOWOF5-ROBOT)))

(:derived (Flag111-)(and (ABOVEOF4-ROBOT)(BELOWOF5-ROBOT)))

(:derived (Flag112-)(and (ABOVEOF15-ROBOT)(BELOWOF5-ROBOT)))

(:derived (Flag113-)(and (ABOVEOF8-ROBOT)(BELOWOF5-ROBOT)))

(:derived (Flag114-)(and (ABOVEOF16-ROBOT)(BELOWOF5-ROBOT)))

(:derived (Flag115-)(and (ABOVEOF13-ROBOT)(BELOWOF5-ROBOT)))

(:derived (Flag116-)(and (ABOVEOF10-ROBOT)(BELOWOF5-ROBOT)))

(:derived (Flag117-)(and (ABOVEOF16-ROBOT)(BELOWOF4-ROBOT)))

(:derived (Flag118-)(and (ABOVEOF5-ROBOT)(BELOWOF5-ROBOT)))

(:derived (Flag119-)(and (ABOVEOF7-ROBOT)(BELOWOF5-ROBOT)))

(:derived (Flag120-)(and (ABOVEOF9-ROBOT)(BELOWOF5-ROBOT)))

(:derived (Flag121-)(and (ABOVEOF12-ROBOT)(BELOWOF5-ROBOT)))

(:derived (Flag122-)(and (ABOVEOF11-ROBOT)(BELOWOF5-ROBOT)))

(:derived (Flag123-)(and (ABOVEOF6-ROBOT)(BELOWOF5-ROBOT)))

(:derived (Flag124-)(and (ABOVEOF14-ROBOT)(BELOWOF5-ROBOT)))

(:derived (Flag126-)(and (BELOWOF6-ROBOT)(ABOVEOF15-ROBOT)))

(:derived (Flag127-)(and (BELOWOF6-ROBOT)(ABOVEOF5-ROBOT)))

(:derived (Flag128-)(and (ABOVEOF11-ROBOT)(BELOWOF6-ROBOT)))

(:derived (Flag129-)(and (ABOVEOF12-ROBOT)(BELOWOF6-ROBOT)))

(:derived (Flag130-)(and (BELOWOF5-ROBOT)(ABOVEOF13-ROBOT)))

(:derived (Flag131-)(and (BELOWOF6-ROBOT)(ABOVEOF13-ROBOT)))

(:derived (Flag132-)(and (BELOWOF6-ROBOT)(ABOVEOF7-ROBOT)))

(:derived (Flag133-)(and (BELOWOF5-ROBOT)(ABOVEOF5-ROBOT)))

(:derived (Flag134-)(and (BELOWOF6-ROBOT)(ABOVEOF6-ROBOT)))

(:derived (Flag135-)(and (BELOWOF6-ROBOT)(ABOVEOF17-ROBOT)))

(:derived (Flag136-)(and (ABOVEOF8-ROBOT)(BELOWOF6-ROBOT)))

(:derived (Flag137-)(and (BELOWOF6-ROBOT)(ABOVEOF9-ROBOT)))

(:derived (Flag138-)(and (BELOWOF6-ROBOT)(ABOVEOF14-ROBOT)))

(:derived (Flag139-)(and (BELOWOF6-ROBOT)(ABOVEOF10-ROBOT)))

(:derived (Flag140-)(and (BELOWOF6-ROBOT)(ABOVEOF16-ROBOT)))

(:derived (Flag142-)(and (ABOVEOF6-ROBOT)(BELOWOF7-ROBOT)))

(:derived (Flag143-)(and (BELOWOF7-ROBOT)(ABOVEOF14-ROBOT)))

(:derived (Flag144-)(and (BELOWOF7-ROBOT)(ABOVEOF15-ROBOT)))

(:derived (Flag145-)(and (ABOVEOF7-ROBOT)(BELOWOF7-ROBOT)))

(:derived (Flag146-)(and (ABOVEOF16-ROBOT)(BELOWOF7-ROBOT)))

(:derived (Flag147-)(and (ABOVEOF10-ROBOT)(BELOWOF6-ROBOT)))

(:derived (Flag148-)(and (ABOVEOF10-ROBOT)(BELOWOF7-ROBOT)))

(:derived (Flag149-)(and (BELOWOF7-ROBOT)(ABOVEOF13-ROBOT)))

(:derived (Flag150-)(and (ABOVEOF17-ROBOT)(BELOWOF7-ROBOT)))

(:derived (Flag151-)(and (ABOVEOF8-ROBOT)(BELOWOF7-ROBOT)))

(:derived (Flag152-)(and (ABOVEOF11-ROBOT)(BELOWOF7-ROBOT)))

(:derived (Flag153-)(and (ABOVEOF9-ROBOT)(BELOWOF7-ROBOT)))

(:derived (Flag154-)(and (ABOVEOF6-ROBOT)(BELOWOF6-ROBOT)))

(:derived (Flag155-)(and (ABOVEOF12-ROBOT)(BELOWOF7-ROBOT)))

(:derived (Flag157-)(and (BELOWOF8-ROBOT)(ABOVEOF11-ROBOT)))

(:derived (Flag158-)(and (BELOWOF8-ROBOT)(ABOVEOF14-ROBOT)))

(:derived (Flag159-)(and (BELOWOF8-ROBOT)(ABOVEOF17-ROBOT)))

(:derived (Flag160-)(and (BELOWOF8-ROBOT)(ABOVEOF13-ROBOT)))

(:derived (Flag161-)(and (BELOWOF8-ROBOT)(ABOVEOF10-ROBOT)))

(:derived (Flag162-)(and (BELOWOF8-ROBOT)(ABOVEOF9-ROBOT)))

(:derived (Flag163-)(and (BELOWOF8-ROBOT)(ABOVEOF7-ROBOT)))

(:derived (Flag164-)(and (ABOVEOF8-ROBOT)(BELOWOF8-ROBOT)))

(:derived (Flag165-)(and (BELOWOF7-ROBOT)(ABOVEOF7-ROBOT)))

(:derived (Flag166-)(and (BELOWOF8-ROBOT)(ABOVEOF15-ROBOT)))

(:derived (Flag167-)(and (ABOVEOF12-ROBOT)(BELOWOF8-ROBOT)))

(:derived (Flag168-)(and (BELOWOF8-ROBOT)(ABOVEOF16-ROBOT)))

(:derived (Flag170-)(and (ABOVEOF17-ROBOT)(BELOWOF9-ROBOT)))

(:derived (Flag171-)(and (BELOWOF9-ROBOT)(ABOVEOF13-ROBOT)))

(:derived (Flag172-)(and (ABOVEOF11-ROBOT)(BELOWOF9-ROBOT)))

(:derived (Flag173-)(and (BELOWOF8-ROBOT)(ABOVEOF12-ROBOT)))

(:derived (Flag174-)(and (ABOVEOF10-ROBOT)(BELOWOF9-ROBOT)))

(:derived (Flag175-)(and (BELOWOF9-ROBOT)(ABOVEOF14-ROBOT)))

(:derived (Flag176-)(and (BELOWOF9-ROBOT)(ABOVEOF9-ROBOT)))

(:derived (Flag177-)(and (BELOWOF9-ROBOT)(ABOVEOF16-ROBOT)))

(:derived (Flag178-)(and (BELOWOF8-ROBOT)(ABOVEOF8-ROBOT)))

(:derived (Flag179-)(and (BELOWOF9-ROBOT)(ABOVEOF15-ROBOT)))

(:derived (Flag180-)(and (ABOVEOF8-ROBOT)(BELOWOF9-ROBOT)))

(:derived (Flag181-)(and (ABOVEOF12-ROBOT)(BELOWOF9-ROBOT)))

(:derived (Flag183-)(and (BELOWOF10-ROBOT)(ABOVEOF10-ROBOT)))

(:derived (Flag184-)(and (ABOVEOF12-ROBOT)(BELOWOF10-ROBOT)))

(:derived (Flag185-)(and (BELOWOF10-ROBOT)(ABOVEOF14-ROBOT)))

(:derived (Flag186-)(and (ABOVEOF9-ROBOT)(BELOWOF9-ROBOT)))

(:derived (Flag187-)(and (BELOWOF10-ROBOT)(ABOVEOF13-ROBOT)))

(:derived (Flag188-)(and (BELOWOF10-ROBOT)(ABOVEOF16-ROBOT)))

(:derived (Flag189-)(and (BELOWOF10-ROBOT)(ABOVEOF9-ROBOT)))

(:derived (Flag190-)(and (BELOWOF10-ROBOT)(ABOVEOF15-ROBOT)))

(:derived (Flag191-)(and (BELOWOF10-ROBOT)(ABOVEOF17-ROBOT)))

(:derived (Flag192-)(and (ABOVEOF11-ROBOT)(BELOWOF10-ROBOT)))

(:derived (Flag194-)(and (BELOWOF11-ROBOT)(ABOVEOF16-ROBOT)))

(:derived (Flag195-)(and (BELOWOF11-ROBOT)(ABOVEOF15-ROBOT)))

(:derived (Flag196-)(and (BELOWOF11-ROBOT)(ABOVEOF17-ROBOT)))

(:derived (Flag197-)(and (BELOWOF11-ROBOT)(ABOVEOF13-ROBOT)))

(:derived (Flag198-)(and (BELOWOF11-ROBOT)(ABOVEOF14-ROBOT)))

(:derived (Flag199-)(and (BELOWOF11-ROBOT)(ABOVEOF10-ROBOT)))

(:derived (Flag200-)(and (ABOVEOF11-ROBOT)(BELOWOF11-ROBOT)))

(:derived (Flag201-)(and (ABOVEOF12-ROBOT)(BELOWOF11-ROBOT)))

(:derived (Flag203-)(and (BELOWOF12-ROBOT)(ABOVEOF13-ROBOT)))

(:derived (Flag204-)(and (BELOWOF12-ROBOT)(ABOVEOF15-ROBOT)))

(:derived (Flag205-)(and (BELOWOF12-ROBOT)(ABOVEOF17-ROBOT)))

(:derived (Flag206-)(and (BELOWOF12-ROBOT)(ABOVEOF11-ROBOT)))

(:derived (Flag207-)(and (BELOWOF12-ROBOT)(ABOVEOF14-ROBOT)))

(:derived (Flag208-)(and (ABOVEOF12-ROBOT)(BELOWOF12-ROBOT)))

(:derived (Flag209-)(and (BELOWOF12-ROBOT)(ABOVEOF16-ROBOT)))

(:derived (Flag211-)(and (ABOVEOF14-ROBOT)(BELOWOF13-ROBOT)))

(:derived (Flag212-)(and (BELOWOF12-ROBOT)(ABOVEOF12-ROBOT)))

(:derived (Flag213-)(and (ABOVEOF13-ROBOT)(BELOWOF13-ROBOT)))

(:derived (Flag214-)(and (ABOVEOF15-ROBOT)(BELOWOF13-ROBOT)))

(:derived (Flag215-)(and (ABOVEOF12-ROBOT)(BELOWOF13-ROBOT)))

(:derived (Flag216-)(and (ABOVEOF17-ROBOT)(BELOWOF13-ROBOT)))

(:derived (Flag217-)(and (ABOVEOF16-ROBOT)(BELOWOF13-ROBOT)))

(:derived (Flag219-)(and (BELOWOF14-ROBOT)(ABOVEOF14-ROBOT)))

(:derived (Flag220-)(and (ABOVEOF15-ROBOT)(BELOWOF14-ROBOT)))

(:derived (Flag221-)(and (BELOWOF13-ROBOT)(ABOVEOF13-ROBOT)))

(:derived (Flag222-)(and (ABOVEOF17-ROBOT)(BELOWOF14-ROBOT)))

(:derived (Flag223-)(and (BELOWOF14-ROBOT)(ABOVEOF13-ROBOT)))

(:derived (Flag224-)(and (ABOVEOF16-ROBOT)(BELOWOF14-ROBOT)))

(:derived (Flag226-)(and (BELOWOF15-ROBOT)(ABOVEOF14-ROBOT)))

(:derived (Flag227-)(and (BELOWOF15-ROBOT)(ABOVEOF15-ROBOT)))

(:derived (Flag228-)(and (ABOVEOF16-ROBOT)(BELOWOF15-ROBOT)))

(:derived (Flag229-)(and (ABOVEOF17-ROBOT)(BELOWOF15-ROBOT)))

(:derived (Flag231-)(and (ABOVEOF17-ROBOT)(BELOWOF16-ROBOT)))

(:derived (Flag232-)(and (ABOVEOF16-ROBOT)(BELOWOF16-ROBOT)))

(:derived (Flag233-)(and (BELOWOF16-ROBOT)(ABOVEOF15-ROBOT)))

(:derived (Flag235-)(and (BELOWOF17-ROBOT)(ABOVEOF16-ROBOT)))

(:derived (Flag236-)(and (ABOVEOF17-ROBOT)(BELOWOF17-ROBOT)))

(:derived (Flag238-)(and (BELOWOF18-ROBOT)(ABOVEOF17-ROBOT)))

(:derived (Flag472-)(and (BELOWOF1-ROBOT)))

(:action MOVEUP-ROBOT
:parameters ()
:precondition
(and
(Flag4-)
)
:effect
(and
(when
(and
(Flag237-)
)
(and
(ROW17-ROBOT)
(not (NOT-ROW17-ROBOT))
)
)
(when
(and
(Flag234-)
)
(and
(ROW16-ROBOT)
(not (NOT-ROW16-ROBOT))
)
)
(when
(and
(Flag230-)
)
(and
(ROW15-ROBOT)
(not (NOT-ROW15-ROBOT))
)
)
(when
(and
(Flag225-)
)
(and
(ROW14-ROBOT)
(not (NOT-ROW14-ROBOT))
)
)
(when
(and
(Flag218-)
)
(and
(ROW13-ROBOT)
(not (NOT-ROW13-ROBOT))
)
)
(when
(and
(Flag210-)
)
(and
(ROW12-ROBOT)
(not (NOT-ROW12-ROBOT))
)
)
(when
(and
(Flag202-)
)
(and
(ROW11-ROBOT)
(not (NOT-ROW11-ROBOT))
)
)
(when
(and
(Flag193-)
)
(and
(ROW10-ROBOT)
(not (NOT-ROW10-ROBOT))
)
)
(when
(and
(Flag182-)
)
(and
(ROW9-ROBOT)
(not (NOT-ROW9-ROBOT))
)
)
(when
(and
(Flag169-)
)
(and
(ROW8-ROBOT)
(not (NOT-ROW8-ROBOT))
)
)
(when
(and
(Flag156-)
)
(and
(ROW7-ROBOT)
(not (NOT-ROW7-ROBOT))
)
)
(when
(and
(Flag141-)
)
(and
(ROW6-ROBOT)
(not (NOT-ROW6-ROBOT))
)
)
(when
(and
(Flag125-)
)
(and
(ROW5-ROBOT)
(not (NOT-ROW5-ROBOT))
)
)
(when
(and
(Flag109-)
)
(and
(ROW4-ROBOT)
(not (NOT-ROW4-ROBOT))
)
)
(when
(and
(Flag91-)
)
(and
(ROW3-ROBOT)
(not (NOT-ROW3-ROBOT))
)
)
(when
(and
(Flag73-)
)
(and
(ROW2-ROBOT)
(not (NOT-ROW2-ROBOT))
)
)
(when
(and
(Flag472-)
)
(and
(ROW1-ROBOT)
(not (NOT-ROW1-ROBOT))
)
)
(when
(and
(ROW16-ROBOT)
)
(and
(ROW17-ROBOT)
(NOT-ROW16-ROBOT)
(not (NOT-ROW17-ROBOT))
(not (ROW16-ROBOT))
)
)
(when
(and
(ROW15-ROBOT)
)
(and
(ROW16-ROBOT)
(NOT-ROW15-ROBOT)
(not (NOT-ROW16-ROBOT))
(not (ROW15-ROBOT))
)
)
(when
(and
(ROW14-ROBOT)
)
(and
(ROW15-ROBOT)
(NOT-ROW14-ROBOT)
(not (NOT-ROW15-ROBOT))
(not (ROW14-ROBOT))
)
)
(when
(and
(ROW13-ROBOT)
)
(and
(ROW14-ROBOT)
(NOT-ROW13-ROBOT)
(not (NOT-ROW14-ROBOT))
(not (ROW13-ROBOT))
)
)
(when
(and
(ROW12-ROBOT)
)
(and
(ROW13-ROBOT)
(NOT-ROW12-ROBOT)
(not (NOT-ROW13-ROBOT))
(not (ROW12-ROBOT))
)
)
(when
(and
(ROW11-ROBOT)
)
(and
(ROW12-ROBOT)
(NOT-ROW11-ROBOT)
(not (NOT-ROW12-ROBOT))
(not (ROW11-ROBOT))
)
)
(when
(and
(ROW10-ROBOT)
)
(and
(ROW11-ROBOT)
(NOT-ROW10-ROBOT)
(not (NOT-ROW11-ROBOT))
(not (ROW10-ROBOT))
)
)
(when
(and
(ROW9-ROBOT)
)
(and
(ROW10-ROBOT)
(NOT-ROW9-ROBOT)
(not (NOT-ROW10-ROBOT))
(not (ROW9-ROBOT))
)
)
(when
(and
(ROW8-ROBOT)
)
(and
(ROW9-ROBOT)
(NOT-ROW8-ROBOT)
(not (NOT-ROW9-ROBOT))
(not (ROW8-ROBOT))
)
)
(when
(and
(ROW7-ROBOT)
)
(and
(ROW8-ROBOT)
(NOT-ROW7-ROBOT)
(not (NOT-ROW8-ROBOT))
(not (ROW7-ROBOT))
)
)
(when
(and
(ROW6-ROBOT)
)
(and
(ROW7-ROBOT)
(NOT-ROW6-ROBOT)
(not (NOT-ROW7-ROBOT))
(not (ROW6-ROBOT))
)
)
(when
(and
(ROW5-ROBOT)
)
(and
(ROW6-ROBOT)
(NOT-ROW5-ROBOT)
(not (NOT-ROW6-ROBOT))
(not (ROW5-ROBOT))
)
)
(when
(and
(ROW4-ROBOT)
)
(and
(ROW5-ROBOT)
(NOT-ROW4-ROBOT)
(not (NOT-ROW5-ROBOT))
(not (ROW4-ROBOT))
)
)
(when
(and
(ROW3-ROBOT)
)
(and
(ROW4-ROBOT)
(NOT-ROW3-ROBOT)
(not (NOT-ROW4-ROBOT))
(not (ROW3-ROBOT))
)
)
(when
(and
(ROW2-ROBOT)
)
(and
(ROW3-ROBOT)
(NOT-ROW2-ROBOT)
(not (NOT-ROW3-ROBOT))
(not (ROW2-ROBOT))
)
)
(when
(and
(ROW1-ROBOT)
)
(and
(ROW2-ROBOT)
(NOT-ROW1-ROBOT)
(not (NOT-ROW2-ROBOT))
(not (ROW1-ROBOT))
)
)
(when
(and
(ROW0-ROBOT)
)
(and
(ROW1-ROBOT)
(NOT-ROW0-ROBOT)
(not (NOT-ROW1-ROBOT))
(not (ROW0-ROBOT))
)
)
(when
(and
(BELOWOF14-ROBOT)
)
(and
(BELOWOF18-ROBOT)
(NOT-BELOWOF17-ROBOT)
(BELOWOF17-ROBOT)
(NOT-BELOWOF16-ROBOT)
(BELOWOF16-ROBOT)
(NOT-BELOWOF15-ROBOT)
(BELOWOF15-ROBOT)
(NOT-BELOWOF14-ROBOT)
(not (NOT-BELOWOF18-ROBOT))
(not (BELOWOF14-ROBOT))
)
)
(when
(and
(BELOWOF8-ROBOT)
)
(and
(BELOWOF18-ROBOT)
(NOT-BELOWOF17-ROBOT)
(BELOWOF17-ROBOT)
(NOT-BELOWOF16-ROBOT)
(BELOWOF16-ROBOT)
(NOT-BELOWOF15-ROBOT)
(BELOWOF15-ROBOT)
(NOT-BELOWOF14-ROBOT)
(BELOWOF14-ROBOT)
(NOT-BELOWOF13-ROBOT)
(BELOWOF13-ROBOT)
(NOT-BELOWOF12-ROBOT)
(BELOWOF12-ROBOT)
(NOT-BELOWOF11-ROBOT)
(BELOWOF11-ROBOT)
(NOT-BELOWOF10-ROBOT)
(BELOWOF10-ROBOT)
(NOT-BELOWOF9-ROBOT)
(BELOWOF9-ROBOT)
(NOT-BELOWOF8-ROBOT)
(not (NOT-BELOWOF18-ROBOT))
(not (BELOWOF8-ROBOT))
)
)
(when
(and
(BELOWOF13-ROBOT)
)
(and
(BELOWOF18-ROBOT)
(NOT-BELOWOF17-ROBOT)
(BELOWOF17-ROBOT)
(NOT-BELOWOF16-ROBOT)
(BELOWOF16-ROBOT)
(NOT-BELOWOF15-ROBOT)
(BELOWOF15-ROBOT)
(NOT-BELOWOF14-ROBOT)
(BELOWOF14-ROBOT)
(NOT-BELOWOF13-ROBOT)
(not (NOT-BELOWOF18-ROBOT))
(not (BELOWOF13-ROBOT))
)
)
(when
(and
(BELOWOF3-ROBOT)
)
(and
(BELOWOF18-ROBOT)
(NOT-BELOWOF17-ROBOT)
(BELOWOF17-ROBOT)
(NOT-BELOWOF16-ROBOT)
(BELOWOF16-ROBOT)
(NOT-BELOWOF15-ROBOT)
(BELOWOF15-ROBOT)
(NOT-BELOWOF14-ROBOT)
(BELOWOF14-ROBOT)
(NOT-BELOWOF13-ROBOT)
(BELOWOF13-ROBOT)
(NOT-BELOWOF12-ROBOT)
(BELOWOF12-ROBOT)
(NOT-BELOWOF11-ROBOT)
(BELOWOF11-ROBOT)
(NOT-BELOWOF10-ROBOT)
(BELOWOF10-ROBOT)
(NOT-BELOWOF9-ROBOT)
(BELOWOF9-ROBOT)
(NOT-BELOWOF8-ROBOT)
(BELOWOF8-ROBOT)
(NOT-BELOWOF7-ROBOT)
(BELOWOF7-ROBOT)
(NOT-BELOWOF6-ROBOT)
(BELOWOF6-ROBOT)
(NOT-BELOWOF5-ROBOT)
(BELOWOF5-ROBOT)
(NOT-BELOWOF4-ROBOT)
(BELOWOF4-ROBOT)
(NOT-BELOWOF3-ROBOT)
(not (NOT-BELOWOF18-ROBOT))
(not (BELOWOF3-ROBOT))
)
)
(when
(and
(BELOWOF9-ROBOT)
)
(and
(BELOWOF18-ROBOT)
(NOT-BELOWOF17-ROBOT)
(BELOWOF17-ROBOT)
(NOT-BELOWOF16-ROBOT)
(BELOWOF16-ROBOT)
(NOT-BELOWOF15-ROBOT)
(BELOWOF15-ROBOT)
(NOT-BELOWOF14-ROBOT)
(BELOWOF14-ROBOT)
(NOT-BELOWOF13-ROBOT)
(BELOWOF13-ROBOT)
(NOT-BELOWOF12-ROBOT)
(BELOWOF12-ROBOT)
(NOT-BELOWOF11-ROBOT)
(BELOWOF11-ROBOT)
(NOT-BELOWOF10-ROBOT)
(BELOWOF10-ROBOT)
(NOT-BELOWOF9-ROBOT)
(not (NOT-BELOWOF18-ROBOT))
(not (BELOWOF9-ROBOT))
)
)
(when
(and
(BELOWOF7-ROBOT)
)
(and
(BELOWOF18-ROBOT)
(NOT-BELOWOF17-ROBOT)
(BELOWOF17-ROBOT)
(NOT-BELOWOF16-ROBOT)
(BELOWOF16-ROBOT)
(NOT-BELOWOF15-ROBOT)
(BELOWOF15-ROBOT)
(NOT-BELOWOF14-ROBOT)
(BELOWOF14-ROBOT)
(NOT-BELOWOF13-ROBOT)
(BELOWOF13-ROBOT)
(NOT-BELOWOF12-ROBOT)
(BELOWOF12-ROBOT)
(NOT-BELOWOF11-ROBOT)
(BELOWOF11-ROBOT)
(NOT-BELOWOF10-ROBOT)
(BELOWOF10-ROBOT)
(NOT-BELOWOF9-ROBOT)
(BELOWOF9-ROBOT)
(NOT-BELOWOF8-ROBOT)
(BELOWOF8-ROBOT)
(NOT-BELOWOF7-ROBOT)
(not (NOT-BELOWOF18-ROBOT))
(not (BELOWOF7-ROBOT))
)
)
(when
(and
(BELOWOF12-ROBOT)
)
(and
(BELOWOF18-ROBOT)
(NOT-BELOWOF17-ROBOT)
(BELOWOF17-ROBOT)
(NOT-BELOWOF16-ROBOT)
(BELOWOF16-ROBOT)
(NOT-BELOWOF15-ROBOT)
(BELOWOF15-ROBOT)
(NOT-BELOWOF14-ROBOT)
(BELOWOF14-ROBOT)
(NOT-BELOWOF13-ROBOT)
(BELOWOF13-ROBOT)
(NOT-BELOWOF12-ROBOT)
(not (NOT-BELOWOF18-ROBOT))
(not (BELOWOF12-ROBOT))
)
)
(when
(and
(BELOWOF11-ROBOT)
)
(and
(BELOWOF18-ROBOT)
(NOT-BELOWOF17-ROBOT)
(BELOWOF17-ROBOT)
(NOT-BELOWOF16-ROBOT)
(BELOWOF16-ROBOT)
(NOT-BELOWOF15-ROBOT)
(BELOWOF15-ROBOT)
(NOT-BELOWOF14-ROBOT)
(BELOWOF14-ROBOT)
(NOT-BELOWOF13-ROBOT)
(BELOWOF13-ROBOT)
(NOT-BELOWOF12-ROBOT)
(BELOWOF12-ROBOT)
(NOT-BELOWOF11-ROBOT)
(not (NOT-BELOWOF18-ROBOT))
(not (BELOWOF11-ROBOT))
)
)
(when
(and
(BELOWOF4-ROBOT)
)
(and
(BELOWOF18-ROBOT)
(NOT-BELOWOF17-ROBOT)
(BELOWOF17-ROBOT)
(NOT-BELOWOF16-ROBOT)
(BELOWOF16-ROBOT)
(NOT-BELOWOF15-ROBOT)
(BELOWOF15-ROBOT)
(NOT-BELOWOF14-ROBOT)
(BELOWOF14-ROBOT)
(NOT-BELOWOF13-ROBOT)
(BELOWOF13-ROBOT)
(NOT-BELOWOF12-ROBOT)
(BELOWOF12-ROBOT)
(NOT-BELOWOF11-ROBOT)
(BELOWOF11-ROBOT)
(NOT-BELOWOF10-ROBOT)
(BELOWOF10-ROBOT)
(NOT-BELOWOF9-ROBOT)
(BELOWOF9-ROBOT)
(NOT-BELOWOF8-ROBOT)
(BELOWOF8-ROBOT)
(NOT-BELOWOF7-ROBOT)
(BELOWOF7-ROBOT)
(NOT-BELOWOF6-ROBOT)
(BELOWOF6-ROBOT)
(NOT-BELOWOF5-ROBOT)
(BELOWOF5-ROBOT)
(NOT-BELOWOF4-ROBOT)
(not (NOT-BELOWOF18-ROBOT))
(not (BELOWOF4-ROBOT))
)
)
(when
(and
(BELOWOF1-ROBOT)
)
(and
(BELOWOF18-ROBOT)
(NOT-BELOWOF17-ROBOT)
(BELOWOF17-ROBOT)
(NOT-BELOWOF16-ROBOT)
(BELOWOF16-ROBOT)
(NOT-BELOWOF15-ROBOT)
(BELOWOF15-ROBOT)
(NOT-BELOWOF14-ROBOT)
(BELOWOF14-ROBOT)
(NOT-BELOWOF13-ROBOT)
(BELOWOF13-ROBOT)
(NOT-BELOWOF12-ROBOT)
(BELOWOF12-ROBOT)
(NOT-BELOWOF11-ROBOT)
(BELOWOF11-ROBOT)
(NOT-BELOWOF10-ROBOT)
(BELOWOF10-ROBOT)
(NOT-BELOWOF9-ROBOT)
(BELOWOF9-ROBOT)
(NOT-BELOWOF8-ROBOT)
(BELOWOF8-ROBOT)
(NOT-BELOWOF7-ROBOT)
(BELOWOF7-ROBOT)
(NOT-BELOWOF6-ROBOT)
(BELOWOF6-ROBOT)
(NOT-BELOWOF5-ROBOT)
(BELOWOF5-ROBOT)
(NOT-BELOWOF4-ROBOT)
(BELOWOF4-ROBOT)
(NOT-BELOWOF3-ROBOT)
(BELOWOF3-ROBOT)
(NOT-BELOWOF2-ROBOT)
(BELOWOF2-ROBOT)
(NOT-BELOWOF1-ROBOT)
(not (NOT-BELOWOF18-ROBOT))
(not (BELOWOF1-ROBOT))
)
)
(when
(and
(BELOWOF2-ROBOT)
)
(and
(BELOWOF18-ROBOT)
(NOT-BELOWOF17-ROBOT)
(BELOWOF17-ROBOT)
(NOT-BELOWOF16-ROBOT)
(BELOWOF16-ROBOT)
(NOT-BELOWOF15-ROBOT)
(BELOWOF15-ROBOT)
(NOT-BELOWOF14-ROBOT)
(BELOWOF14-ROBOT)
(NOT-BELOWOF13-ROBOT)
(BELOWOF13-ROBOT)
(NOT-BELOWOF12-ROBOT)
(BELOWOF12-ROBOT)
(NOT-BELOWOF11-ROBOT)
(BELOWOF11-ROBOT)
(NOT-BELOWOF10-ROBOT)
(BELOWOF10-ROBOT)
(NOT-BELOWOF9-ROBOT)
(BELOWOF9-ROBOT)
(NOT-BELOWOF8-ROBOT)
(BELOWOF8-ROBOT)
(NOT-BELOWOF7-ROBOT)
(BELOWOF7-ROBOT)
(NOT-BELOWOF6-ROBOT)
(BELOWOF6-ROBOT)
(NOT-BELOWOF5-ROBOT)
(BELOWOF5-ROBOT)
(NOT-BELOWOF4-ROBOT)
(BELOWOF4-ROBOT)
(NOT-BELOWOF3-ROBOT)
(BELOWOF3-ROBOT)
(NOT-BELOWOF2-ROBOT)
(not (NOT-BELOWOF18-ROBOT))
(not (BELOWOF2-ROBOT))
)
)
(when
(and
(BELOWOF5-ROBOT)
)
(and
(BELOWOF18-ROBOT)
(NOT-BELOWOF17-ROBOT)
(BELOWOF17-ROBOT)
(NOT-BELOWOF16-ROBOT)
(BELOWOF16-ROBOT)
(NOT-BELOWOF15-ROBOT)
(BELOWOF15-ROBOT)
(NOT-BELOWOF14-ROBOT)
(BELOWOF14-ROBOT)
(NOT-BELOWOF13-ROBOT)
(BELOWOF13-ROBOT)
(NOT-BELOWOF12-ROBOT)
(BELOWOF12-ROBOT)
(NOT-BELOWOF11-ROBOT)
(BELOWOF11-ROBOT)
(NOT-BELOWOF10-ROBOT)
(BELOWOF10-ROBOT)
(NOT-BELOWOF9-ROBOT)
(BELOWOF9-ROBOT)
(NOT-BELOWOF8-ROBOT)
(BELOWOF8-ROBOT)
(NOT-BELOWOF7-ROBOT)
(BELOWOF7-ROBOT)
(NOT-BELOWOF6-ROBOT)
(BELOWOF6-ROBOT)
(NOT-BELOWOF5-ROBOT)
(not (NOT-BELOWOF18-ROBOT))
(not (BELOWOF5-ROBOT))
)
)
(when
(and
(BELOWOF17-ROBOT)
)
(and
(BELOWOF18-ROBOT)
(NOT-BELOWOF17-ROBOT)
(not (NOT-BELOWOF18-ROBOT))
(not (BELOWOF17-ROBOT))
)
)
(when
(and
(BELOWOF6-ROBOT)
)
(and
(BELOWOF18-ROBOT)
(NOT-BELOWOF17-ROBOT)
(BELOWOF17-ROBOT)
(NOT-BELOWOF16-ROBOT)
(BELOWOF16-ROBOT)
(NOT-BELOWOF15-ROBOT)
(BELOWOF15-ROBOT)
(NOT-BELOWOF14-ROBOT)
(BELOWOF14-ROBOT)
(NOT-BELOWOF13-ROBOT)
(BELOWOF13-ROBOT)
(NOT-BELOWOF12-ROBOT)
(BELOWOF12-ROBOT)
(NOT-BELOWOF11-ROBOT)
(BELOWOF11-ROBOT)
(NOT-BELOWOF10-ROBOT)
(BELOWOF10-ROBOT)
(NOT-BELOWOF9-ROBOT)
(BELOWOF9-ROBOT)
(NOT-BELOWOF8-ROBOT)
(BELOWOF8-ROBOT)
(NOT-BELOWOF7-ROBOT)
(BELOWOF7-ROBOT)
(NOT-BELOWOF6-ROBOT)
(not (NOT-BELOWOF18-ROBOT))
(not (BELOWOF6-ROBOT))
)
)
(when
(and
(BELOWOF15-ROBOT)
)
(and
(BELOWOF18-ROBOT)
(NOT-BELOWOF17-ROBOT)
(BELOWOF17-ROBOT)
(NOT-BELOWOF16-ROBOT)
(BELOWOF16-ROBOT)
(NOT-BELOWOF15-ROBOT)
(not (NOT-BELOWOF18-ROBOT))
(not (BELOWOF15-ROBOT))
)
)
(when
(and
(BELOWOF16-ROBOT)
)
(and
(BELOWOF18-ROBOT)
(NOT-BELOWOF17-ROBOT)
(BELOWOF17-ROBOT)
(NOT-BELOWOF16-ROBOT)
(not (NOT-BELOWOF18-ROBOT))
(not (BELOWOF16-ROBOT))
)
)
(when
(and
(BELOWOF10-ROBOT)
)
(and
(BELOWOF18-ROBOT)
(NOT-BELOWOF17-ROBOT)
(BELOWOF17-ROBOT)
(NOT-BELOWOF16-ROBOT)
(BELOWOF16-ROBOT)
(NOT-BELOWOF15-ROBOT)
(BELOWOF15-ROBOT)
(NOT-BELOWOF14-ROBOT)
(BELOWOF14-ROBOT)
(NOT-BELOWOF13-ROBOT)
(BELOWOF13-ROBOT)
(NOT-BELOWOF12-ROBOT)
(BELOWOF12-ROBOT)
(NOT-BELOWOF11-ROBOT)
(BELOWOF11-ROBOT)
(NOT-BELOWOF10-ROBOT)
(not (NOT-BELOWOF18-ROBOT))
(not (BELOWOF10-ROBOT))
)
)
(when
(and
(ABOVEOF16-ROBOT)
)
(and
(ABOVEOF17-ROBOT)
(ABOVEOF15-ROBOT)
(ABOVEOF14-ROBOT)
(ABOVEOF13-ROBOT)
(ABOVEOF12-ROBOT)
(ABOVEOF11-ROBOT)
(ABOVEOF10-ROBOT)
(ABOVEOF9-ROBOT)
(ABOVEOF8-ROBOT)
(ABOVEOF7-ROBOT)
(ABOVEOF6-ROBOT)
(ABOVEOF5-ROBOT)
(ABOVEOF4-ROBOT)
(ABOVEOF3-ROBOT)
(ABOVEOF2-ROBOT)
(not (NOT-ABOVEOF17-ROBOT))
(not (NOT-ABOVEOF16-ROBOT))
(not (NOT-ABOVEOF15-ROBOT))
(not (NOT-ABOVEOF14-ROBOT))
(not (NOT-ABOVEOF13-ROBOT))
(not (NOT-ABOVEOF12-ROBOT))
(not (NOT-ABOVEOF11-ROBOT))
(not (NOT-ABOVEOF10-ROBOT))
(not (NOT-ABOVEOF9-ROBOT))
(not (NOT-ABOVEOF8-ROBOT))
(not (NOT-ABOVEOF7-ROBOT))
(not (NOT-ABOVEOF6-ROBOT))
(not (NOT-ABOVEOF5-ROBOT))
(not (NOT-ABOVEOF4-ROBOT))
(not (NOT-ABOVEOF3-ROBOT))
(not (NOT-ABOVEOF2-ROBOT))
)
)
(when
(and
(ABOVEOF17-ROBOT)
)
(and
(ABOVEOF16-ROBOT)
(ABOVEOF15-ROBOT)
(ABOVEOF14-ROBOT)
(ABOVEOF13-ROBOT)
(ABOVEOF12-ROBOT)
(ABOVEOF11-ROBOT)
(ABOVEOF10-ROBOT)
(ABOVEOF9-ROBOT)
(ABOVEOF8-ROBOT)
(ABOVEOF7-ROBOT)
(ABOVEOF6-ROBOT)
(ABOVEOF5-ROBOT)
(ABOVEOF4-ROBOT)
(ABOVEOF3-ROBOT)
(ABOVEOF2-ROBOT)
(not (NOT-ABOVEOF17-ROBOT))
(not (NOT-ABOVEOF16-ROBOT))
(not (NOT-ABOVEOF15-ROBOT))
(not (NOT-ABOVEOF14-ROBOT))
(not (NOT-ABOVEOF13-ROBOT))
(not (NOT-ABOVEOF12-ROBOT))
(not (NOT-ABOVEOF11-ROBOT))
(not (NOT-ABOVEOF10-ROBOT))
(not (NOT-ABOVEOF9-ROBOT))
(not (NOT-ABOVEOF8-ROBOT))
(not (NOT-ABOVEOF7-ROBOT))
(not (NOT-ABOVEOF6-ROBOT))
(not (NOT-ABOVEOF5-ROBOT))
(not (NOT-ABOVEOF4-ROBOT))
(not (NOT-ABOVEOF3-ROBOT))
(not (NOT-ABOVEOF2-ROBOT))
)
)
(when
(and
(ABOVEOF15-ROBOT)
)
(and
(ABOVEOF16-ROBOT)
(ABOVEOF14-ROBOT)
(ABOVEOF13-ROBOT)
(ABOVEOF12-ROBOT)
(ABOVEOF11-ROBOT)
(ABOVEOF10-ROBOT)
(ABOVEOF9-ROBOT)
(ABOVEOF8-ROBOT)
(ABOVEOF7-ROBOT)
(ABOVEOF6-ROBOT)
(ABOVEOF5-ROBOT)
(ABOVEOF4-ROBOT)
(ABOVEOF3-ROBOT)
(ABOVEOF2-ROBOT)
(not (NOT-ABOVEOF16-ROBOT))
(not (NOT-ABOVEOF15-ROBOT))
(not (NOT-ABOVEOF14-ROBOT))
(not (NOT-ABOVEOF13-ROBOT))
(not (NOT-ABOVEOF12-ROBOT))
(not (NOT-ABOVEOF11-ROBOT))
(not (NOT-ABOVEOF10-ROBOT))
(not (NOT-ABOVEOF9-ROBOT))
(not (NOT-ABOVEOF8-ROBOT))
(not (NOT-ABOVEOF7-ROBOT))
(not (NOT-ABOVEOF6-ROBOT))
(not (NOT-ABOVEOF5-ROBOT))
(not (NOT-ABOVEOF4-ROBOT))
(not (NOT-ABOVEOF3-ROBOT))
(not (NOT-ABOVEOF2-ROBOT))
)
)
(when
(and
(ABOVEOF14-ROBOT)
)
(and
(ABOVEOF15-ROBOT)
(ABOVEOF13-ROBOT)
(ABOVEOF12-ROBOT)
(ABOVEOF11-ROBOT)
(ABOVEOF10-ROBOT)
(ABOVEOF9-ROBOT)
(ABOVEOF8-ROBOT)
(ABOVEOF7-ROBOT)
(ABOVEOF6-ROBOT)
(ABOVEOF5-ROBOT)
(ABOVEOF4-ROBOT)
(ABOVEOF3-ROBOT)
(ABOVEOF2-ROBOT)
(not (NOT-ABOVEOF15-ROBOT))
(not (NOT-ABOVEOF14-ROBOT))
(not (NOT-ABOVEOF13-ROBOT))
(not (NOT-ABOVEOF12-ROBOT))
(not (NOT-ABOVEOF11-ROBOT))
(not (NOT-ABOVEOF10-ROBOT))
(not (NOT-ABOVEOF9-ROBOT))
(not (NOT-ABOVEOF8-ROBOT))
(not (NOT-ABOVEOF7-ROBOT))
(not (NOT-ABOVEOF6-ROBOT))
(not (NOT-ABOVEOF5-ROBOT))
(not (NOT-ABOVEOF4-ROBOT))
(not (NOT-ABOVEOF3-ROBOT))
(not (NOT-ABOVEOF2-ROBOT))
)
)
(when
(and
(ABOVEOF13-ROBOT)
)
(and
(ABOVEOF14-ROBOT)
(ABOVEOF12-ROBOT)
(ABOVEOF11-ROBOT)
(ABOVEOF10-ROBOT)
(ABOVEOF9-ROBOT)
(ABOVEOF8-ROBOT)
(ABOVEOF7-ROBOT)
(ABOVEOF6-ROBOT)
(ABOVEOF5-ROBOT)
(ABOVEOF4-ROBOT)
(ABOVEOF3-ROBOT)
(ABOVEOF2-ROBOT)
(not (NOT-ABOVEOF14-ROBOT))
(not (NOT-ABOVEOF13-ROBOT))
(not (NOT-ABOVEOF12-ROBOT))
(not (NOT-ABOVEOF11-ROBOT))
(not (NOT-ABOVEOF10-ROBOT))
(not (NOT-ABOVEOF9-ROBOT))
(not (NOT-ABOVEOF8-ROBOT))
(not (NOT-ABOVEOF7-ROBOT))
(not (NOT-ABOVEOF6-ROBOT))
(not (NOT-ABOVEOF5-ROBOT))
(not (NOT-ABOVEOF4-ROBOT))
(not (NOT-ABOVEOF3-ROBOT))
(not (NOT-ABOVEOF2-ROBOT))
)
)
(when
(and
(ABOVEOF12-ROBOT)
)
(and
(ABOVEOF13-ROBOT)
(ABOVEOF11-ROBOT)
(ABOVEOF10-ROBOT)
(ABOVEOF9-ROBOT)
(ABOVEOF8-ROBOT)
(ABOVEOF7-ROBOT)
(ABOVEOF6-ROBOT)
(ABOVEOF5-ROBOT)
(ABOVEOF4-ROBOT)
(ABOVEOF3-ROBOT)
(ABOVEOF2-ROBOT)
(not (NOT-ABOVEOF13-ROBOT))
(not (NOT-ABOVEOF12-ROBOT))
(not (NOT-ABOVEOF11-ROBOT))
(not (NOT-ABOVEOF10-ROBOT))
(not (NOT-ABOVEOF9-ROBOT))
(not (NOT-ABOVEOF8-ROBOT))
(not (NOT-ABOVEOF7-ROBOT))
(not (NOT-ABOVEOF6-ROBOT))
(not (NOT-ABOVEOF5-ROBOT))
(not (NOT-ABOVEOF4-ROBOT))
(not (NOT-ABOVEOF3-ROBOT))
(not (NOT-ABOVEOF2-ROBOT))
)
)
(when
(and
(ABOVEOF11-ROBOT)
)
(and
(ABOVEOF12-ROBOT)
(ABOVEOF10-ROBOT)
(ABOVEOF9-ROBOT)
(ABOVEOF8-ROBOT)
(ABOVEOF7-ROBOT)
(ABOVEOF6-ROBOT)
(ABOVEOF5-ROBOT)
(ABOVEOF4-ROBOT)
(ABOVEOF3-ROBOT)
(ABOVEOF2-ROBOT)
(not (NOT-ABOVEOF12-ROBOT))
(not (NOT-ABOVEOF11-ROBOT))
(not (NOT-ABOVEOF10-ROBOT))
(not (NOT-ABOVEOF9-ROBOT))
(not (NOT-ABOVEOF8-ROBOT))
(not (NOT-ABOVEOF7-ROBOT))
(not (NOT-ABOVEOF6-ROBOT))
(not (NOT-ABOVEOF5-ROBOT))
(not (NOT-ABOVEOF4-ROBOT))
(not (NOT-ABOVEOF3-ROBOT))
(not (NOT-ABOVEOF2-ROBOT))
)
)
(when
(and
(ABOVEOF10-ROBOT)
)
(and
(ABOVEOF11-ROBOT)
(ABOVEOF9-ROBOT)
(ABOVEOF8-ROBOT)
(ABOVEOF7-ROBOT)
(ABOVEOF6-ROBOT)
(ABOVEOF5-ROBOT)
(ABOVEOF4-ROBOT)
(ABOVEOF3-ROBOT)
(ABOVEOF2-ROBOT)
(not (NOT-ABOVEOF11-ROBOT))
(not (NOT-ABOVEOF10-ROBOT))
(not (NOT-ABOVEOF9-ROBOT))
(not (NOT-ABOVEOF8-ROBOT))
(not (NOT-ABOVEOF7-ROBOT))
(not (NOT-ABOVEOF6-ROBOT))
(not (NOT-ABOVEOF5-ROBOT))
(not (NOT-ABOVEOF4-ROBOT))
(not (NOT-ABOVEOF3-ROBOT))
(not (NOT-ABOVEOF2-ROBOT))
)
)
(when
(and
(ABOVEOF9-ROBOT)
)
(and
(ABOVEOF10-ROBOT)
(ABOVEOF8-ROBOT)
(ABOVEOF7-ROBOT)
(ABOVEOF6-ROBOT)
(ABOVEOF5-ROBOT)
(ABOVEOF4-ROBOT)
(ABOVEOF3-ROBOT)
(ABOVEOF2-ROBOT)
(not (NOT-ABOVEOF10-ROBOT))
(not (NOT-ABOVEOF9-ROBOT))
(not (NOT-ABOVEOF8-ROBOT))
(not (NOT-ABOVEOF7-ROBOT))
(not (NOT-ABOVEOF6-ROBOT))
(not (NOT-ABOVEOF5-ROBOT))
(not (NOT-ABOVEOF4-ROBOT))
(not (NOT-ABOVEOF3-ROBOT))
(not (NOT-ABOVEOF2-ROBOT))
)
)
(when
(and
(ABOVEOF8-ROBOT)
)
(and
(ABOVEOF9-ROBOT)
(ABOVEOF7-ROBOT)
(ABOVEOF6-ROBOT)
(ABOVEOF5-ROBOT)
(ABOVEOF4-ROBOT)
(ABOVEOF3-ROBOT)
(ABOVEOF2-ROBOT)
(not (NOT-ABOVEOF9-ROBOT))
(not (NOT-ABOVEOF8-ROBOT))
(not (NOT-ABOVEOF7-ROBOT))
(not (NOT-ABOVEOF6-ROBOT))
(not (NOT-ABOVEOF5-ROBOT))
(not (NOT-ABOVEOF4-ROBOT))
(not (NOT-ABOVEOF3-ROBOT))
(not (NOT-ABOVEOF2-ROBOT))
)
)
(when
(and
(ABOVEOF7-ROBOT)
)
(and
(ABOVEOF8-ROBOT)
(ABOVEOF6-ROBOT)
(ABOVEOF5-ROBOT)
(ABOVEOF4-ROBOT)
(ABOVEOF3-ROBOT)
(ABOVEOF2-ROBOT)
(not (NOT-ABOVEOF8-ROBOT))
(not (NOT-ABOVEOF7-ROBOT))
(not (NOT-ABOVEOF6-ROBOT))
(not (NOT-ABOVEOF5-ROBOT))
(not (NOT-ABOVEOF4-ROBOT))
(not (NOT-ABOVEOF3-ROBOT))
(not (NOT-ABOVEOF2-ROBOT))
)
)
(when
(and
(ABOVEOF6-ROBOT)
)
(and
(ABOVEOF7-ROBOT)
(ABOVEOF5-ROBOT)
(ABOVEOF4-ROBOT)
(ABOVEOF3-ROBOT)
(ABOVEOF2-ROBOT)
(not (NOT-ABOVEOF7-ROBOT))
(not (NOT-ABOVEOF6-ROBOT))
(not (NOT-ABOVEOF5-ROBOT))
(not (NOT-ABOVEOF4-ROBOT))
(not (NOT-ABOVEOF3-ROBOT))
(not (NOT-ABOVEOF2-ROBOT))
)
)
(when
(and
(ABOVEOF5-ROBOT)
)
(and
(ABOVEOF6-ROBOT)
(ABOVEOF4-ROBOT)
(ABOVEOF3-ROBOT)
(ABOVEOF2-ROBOT)
(not (NOT-ABOVEOF6-ROBOT))
(not (NOT-ABOVEOF5-ROBOT))
(not (NOT-ABOVEOF4-ROBOT))
(not (NOT-ABOVEOF3-ROBOT))
(not (NOT-ABOVEOF2-ROBOT))
)
)
(when
(and
(ABOVEOF4-ROBOT)
)
(and
(ABOVEOF5-ROBOT)
(ABOVEOF3-ROBOT)
(ABOVEOF2-ROBOT)
(not (NOT-ABOVEOF5-ROBOT))
(not (NOT-ABOVEOF4-ROBOT))
(not (NOT-ABOVEOF3-ROBOT))
(not (NOT-ABOVEOF2-ROBOT))
)
)
(when
(and
(ABOVEOF3-ROBOT)
)
(and
(ABOVEOF4-ROBOT)
(ABOVEOF2-ROBOT)
(not (NOT-ABOVEOF4-ROBOT))
(not (NOT-ABOVEOF3-ROBOT))
(not (NOT-ABOVEOF2-ROBOT))
)
)
(when
(and
(ABOVEOF2-ROBOT)
)
(and
(ABOVEOF3-ROBOT)
(not (NOT-ABOVEOF3-ROBOT))
(not (NOT-ABOVEOF2-ROBOT))
)
)
(when
(and
(ABOVEOF1-ROBOT)
)
(and
(ABOVEOF2-ROBOT)
(not (NOT-ABOVEOF2-ROBOT))
)
)
(ABOVEOF1-ROBOT)
(CHECKCONSISTENCY-)
(not (NOT-ABOVEOF1-ROBOT))
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action MOVEDOWN-ROBOT
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
)
:effect
(and
(when
(and
(Flag239-)
)
(and
(ROW16-ROBOT)
(not (NOT-ROW16-ROBOT))
)
)
(when
(and
(Flag237-)
)
(and
(ROW15-ROBOT)
(not (NOT-ROW15-ROBOT))
)
)
(when
(and
(Flag234-)
)
(and
(ROW14-ROBOT)
(not (NOT-ROW14-ROBOT))
)
)
(when
(and
(Flag230-)
)
(and
(ROW13-ROBOT)
(not (NOT-ROW13-ROBOT))
)
)
(when
(and
(Flag225-)
)
(and
(ROW12-ROBOT)
(not (NOT-ROW12-ROBOT))
)
)
(when
(and
(Flag218-)
)
(and
(ROW11-ROBOT)
(not (NOT-ROW11-ROBOT))
)
)
(when
(and
(Flag210-)
)
(and
(ROW10-ROBOT)
(not (NOT-ROW10-ROBOT))
)
)
(when
(and
(Flag202-)
)
(and
(ROW9-ROBOT)
(not (NOT-ROW9-ROBOT))
)
)
(when
(and
(Flag193-)
)
(and
(ROW8-ROBOT)
(not (NOT-ROW8-ROBOT))
)
)
(when
(and
(Flag182-)
)
(and
(ROW7-ROBOT)
(not (NOT-ROW7-ROBOT))
)
)
(when
(and
(Flag169-)
)
(and
(ROW6-ROBOT)
(not (NOT-ROW6-ROBOT))
)
)
(when
(and
(Flag156-)
)
(and
(ROW5-ROBOT)
(not (NOT-ROW5-ROBOT))
)
)
(when
(and
(Flag141-)
)
(and
(ROW4-ROBOT)
(not (NOT-ROW4-ROBOT))
)
)
(when
(and
(Flag125-)
)
(and
(ROW3-ROBOT)
(not (NOT-ROW3-ROBOT))
)
)
(when
(and
(Flag109-)
)
(and
(ROW2-ROBOT)
(not (NOT-ROW2-ROBOT))
)
)
(when
(and
(Flag91-)
)
(and
(ROW1-ROBOT)
(not (NOT-ROW1-ROBOT))
)
)
(when
(and
(Flag73-)
)
(and
(ROW0-ROBOT)
(not (NOT-ROW0-ROBOT))
)
)
(when
(and
(ROW17-ROBOT)
)
(and
(ROW16-ROBOT)
(NOT-ROW17-ROBOT)
(not (NOT-ROW16-ROBOT))
(not (ROW17-ROBOT))
)
)
(when
(and
(ROW16-ROBOT)
)
(and
(ROW15-ROBOT)
(NOT-ROW16-ROBOT)
(not (NOT-ROW15-ROBOT))
(not (ROW16-ROBOT))
)
)
(when
(and
(ROW15-ROBOT)
)
(and
(ROW14-ROBOT)
(NOT-ROW15-ROBOT)
(not (NOT-ROW14-ROBOT))
(not (ROW15-ROBOT))
)
)
(when
(and
(ROW14-ROBOT)
)
(and
(ROW13-ROBOT)
(NOT-ROW14-ROBOT)
(not (NOT-ROW13-ROBOT))
(not (ROW14-ROBOT))
)
)
(when
(and
(ROW13-ROBOT)
)
(and
(ROW12-ROBOT)
(NOT-ROW13-ROBOT)
(not (NOT-ROW12-ROBOT))
(not (ROW13-ROBOT))
)
)
(when
(and
(ROW12-ROBOT)
)
(and
(ROW11-ROBOT)
(NOT-ROW12-ROBOT)
(not (NOT-ROW11-ROBOT))
(not (ROW12-ROBOT))
)
)
(when
(and
(ROW11-ROBOT)
)
(and
(ROW10-ROBOT)
(NOT-ROW11-ROBOT)
(not (NOT-ROW10-ROBOT))
(not (ROW11-ROBOT))
)
)
(when
(and
(ROW10-ROBOT)
)
(and
(ROW9-ROBOT)
(NOT-ROW10-ROBOT)
(not (NOT-ROW9-ROBOT))
(not (ROW10-ROBOT))
)
)
(when
(and
(ROW9-ROBOT)
)
(and
(ROW8-ROBOT)
(NOT-ROW9-ROBOT)
(not (NOT-ROW8-ROBOT))
(not (ROW9-ROBOT))
)
)
(when
(and
(ROW8-ROBOT)
)
(and
(ROW7-ROBOT)
(NOT-ROW8-ROBOT)
(not (NOT-ROW7-ROBOT))
(not (ROW8-ROBOT))
)
)
(when
(and
(ROW7-ROBOT)
)
(and
(ROW6-ROBOT)
(NOT-ROW7-ROBOT)
(not (NOT-ROW6-ROBOT))
(not (ROW7-ROBOT))
)
)
(when
(and
(ROW6-ROBOT)
)
(and
(ROW5-ROBOT)
(NOT-ROW6-ROBOT)
(not (NOT-ROW5-ROBOT))
(not (ROW6-ROBOT))
)
)
(when
(and
(ROW5-ROBOT)
)
(and
(ROW4-ROBOT)
(NOT-ROW5-ROBOT)
(not (NOT-ROW4-ROBOT))
(not (ROW5-ROBOT))
)
)
(when
(and
(ROW4-ROBOT)
)
(and
(ROW3-ROBOT)
(NOT-ROW4-ROBOT)
(not (NOT-ROW3-ROBOT))
(not (ROW4-ROBOT))
)
)
(when
(and
(ROW3-ROBOT)
)
(and
(ROW2-ROBOT)
(NOT-ROW3-ROBOT)
(not (NOT-ROW2-ROBOT))
(not (ROW3-ROBOT))
)
)
(when
(and
(ROW2-ROBOT)
)
(and
(ROW1-ROBOT)
(NOT-ROW2-ROBOT)
(not (NOT-ROW1-ROBOT))
(not (ROW2-ROBOT))
)
)
(when
(and
(ROW1-ROBOT)
)
(and
(ROW0-ROBOT)
(NOT-ROW1-ROBOT)
(not (NOT-ROW0-ROBOT))
(not (ROW1-ROBOT))
)
)
(when
(and
(ABOVEOF17-ROBOT)
)
(and
(ABOVEOF16-ROBOT)
(NOT-ABOVEOF17-ROBOT)
(not (NOT-ABOVEOF16-ROBOT))
(not (ABOVEOF17-ROBOT))
)
)
(when
(and
(Flag37-)
)
(and
(ABOVEOF15-ROBOT)
(NOT-ABOVEOF16-ROBOT)
(not (NOT-ABOVEOF15-ROBOT))
(not (ABOVEOF16-ROBOT))
)
)
(when
(and
(Flag36-)
)
(and
(ABOVEOF14-ROBOT)
(NOT-ABOVEOF15-ROBOT)
(not (NOT-ABOVEOF14-ROBOT))
(not (ABOVEOF15-ROBOT))
)
)
(when
(and
(Flag35-)
)
(and
(ABOVEOF13-ROBOT)
(NOT-ABOVEOF14-ROBOT)
(not (NOT-ABOVEOF13-ROBOT))
(not (ABOVEOF14-ROBOT))
)
)
(when
(and
(Flag34-)
)
(and
(ABOVEOF12-ROBOT)
(NOT-ABOVEOF13-ROBOT)
(not (NOT-ABOVEOF12-ROBOT))
(not (ABOVEOF13-ROBOT))
)
)
(when
(and
(Flag33-)
)
(and
(ABOVEOF11-ROBOT)
(NOT-ABOVEOF12-ROBOT)
(not (NOT-ABOVEOF11-ROBOT))
(not (ABOVEOF12-ROBOT))
)
)
(when
(and
(Flag32-)
)
(and
(ABOVEOF10-ROBOT)
(NOT-ABOVEOF11-ROBOT)
(not (NOT-ABOVEOF10-ROBOT))
(not (ABOVEOF11-ROBOT))
)
)
(when
(and
(Flag31-)
)
(and
(ABOVEOF9-ROBOT)
(NOT-ABOVEOF10-ROBOT)
(not (NOT-ABOVEOF9-ROBOT))
(not (ABOVEOF10-ROBOT))
)
)
(when
(and
(Flag30-)
)
(and
(ABOVEOF8-ROBOT)
(NOT-ABOVEOF9-ROBOT)
(not (NOT-ABOVEOF8-ROBOT))
(not (ABOVEOF9-ROBOT))
)
)
(when
(and
(Flag29-)
)
(and
(ABOVEOF7-ROBOT)
(NOT-ABOVEOF8-ROBOT)
(not (NOT-ABOVEOF7-ROBOT))
(not (ABOVEOF8-ROBOT))
)
)
(when
(and
(Flag28-)
)
(and
(ABOVEOF6-ROBOT)
(NOT-ABOVEOF7-ROBOT)
(not (NOT-ABOVEOF6-ROBOT))
(not (ABOVEOF7-ROBOT))
)
)
(when
(and
(Flag27-)
)
(and
(ABOVEOF5-ROBOT)
(NOT-ABOVEOF6-ROBOT)
(not (NOT-ABOVEOF5-ROBOT))
(not (ABOVEOF6-ROBOT))
)
)
(when
(and
(Flag26-)
)
(and
(ABOVEOF4-ROBOT)
(NOT-ABOVEOF5-ROBOT)
(not (NOT-ABOVEOF4-ROBOT))
(not (ABOVEOF5-ROBOT))
)
)
(when
(and
(Flag25-)
)
(and
(ABOVEOF3-ROBOT)
(NOT-ABOVEOF4-ROBOT)
(not (NOT-ABOVEOF3-ROBOT))
(not (ABOVEOF4-ROBOT))
)
)
(when
(and
(Flag24-)
)
(and
(ABOVEOF2-ROBOT)
(NOT-ABOVEOF3-ROBOT)
(not (NOT-ABOVEOF2-ROBOT))
(not (ABOVEOF3-ROBOT))
)
)
(when
(and
(Flag23-)
)
(and
(ABOVEOF1-ROBOT)
(NOT-ABOVEOF2-ROBOT)
(not (NOT-ABOVEOF1-ROBOT))
(not (ABOVEOF2-ROBOT))
)
)
(when
(and
(Flag22-)
)
(and
(NOT-ABOVEOF1-ROBOT)
(not (ABOVEOF1-ROBOT))
)
)
(when
(and
(Flag21-)
)
(and
(BELOWOF17-ROBOT)
(not (NOT-BELOWOF17-ROBOT))
)
)
(when
(and
(Flag20-)
)
(and
(BELOWOF16-ROBOT)
(not (NOT-BELOWOF16-ROBOT))
)
)
(when
(and
(Flag19-)
)
(and
(BELOWOF15-ROBOT)
(not (NOT-BELOWOF15-ROBOT))
)
)
(when
(and
(Flag18-)
)
(and
(BELOWOF14-ROBOT)
(not (NOT-BELOWOF14-ROBOT))
)
)
(when
(and
(Flag17-)
)
(and
(BELOWOF13-ROBOT)
(not (NOT-BELOWOF13-ROBOT))
)
)
(when
(and
(Flag16-)
)
(and
(BELOWOF12-ROBOT)
(not (NOT-BELOWOF12-ROBOT))
)
)
(when
(and
(Flag15-)
)
(and
(BELOWOF11-ROBOT)
(not (NOT-BELOWOF11-ROBOT))
)
)
(when
(and
(Flag14-)
)
(and
(BELOWOF10-ROBOT)
(not (NOT-BELOWOF10-ROBOT))
)
)
(when
(and
(Flag13-)
)
(and
(BELOWOF9-ROBOT)
(not (NOT-BELOWOF9-ROBOT))
)
)
(when
(and
(Flag12-)
)
(and
(BELOWOF8-ROBOT)
(not (NOT-BELOWOF8-ROBOT))
)
)
(when
(and
(Flag11-)
)
(and
(BELOWOF7-ROBOT)
(not (NOT-BELOWOF7-ROBOT))
)
)
(when
(and
(Flag10-)
)
(and
(BELOWOF6-ROBOT)
(not (NOT-BELOWOF6-ROBOT))
)
)
(when
(and
(Flag9-)
)
(and
(BELOWOF5-ROBOT)
(not (NOT-BELOWOF5-ROBOT))
)
)
(when
(and
(Flag8-)
)
(and
(BELOWOF4-ROBOT)
(not (NOT-BELOWOF4-ROBOT))
)
)
(when
(and
(Flag7-)
)
(and
(BELOWOF3-ROBOT)
(not (NOT-BELOWOF3-ROBOT))
)
)
(when
(and
(Flag6-)
)
(and
(BELOWOF2-ROBOT)
(not (NOT-BELOWOF2-ROBOT))
)
)
(when
(and
(Flag5-)
)
(and
(BELOWOF1-ROBOT)
(not (NOT-BELOWOF1-ROBOT))
)
)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:derived (Flag2-)(and (RIGHTOF2-ROBOT)))

(:derived (Flag2-)(and (LEFTOF17-ROBOT)))

(:derived (Flag4-)(and (NOT-CHECKCONSISTENCY-)(NOT-ERROR-)))

(:derived (Flag20-)(and (BELOWOF17-ROBOT)))

(:derived (Flag21-)(and (BELOWOF17-ROBOT)))

(:derived (Flag255-)(and (LEFTOF17-ROBOT)))

(:derived (Flag256-)(and (LEFTOF17-ROBOT)))

(:derived (Flag257-)(and (RIGHTOF2-ROBOT)))

(:derived (Flag258-)(and (RIGHTOF2-ROBOT)))

(:derived (Flag473-)(and (RIGHTOF2-ROBOT)))

)
