(define (domain grounded-SIMPLE-ADL-ROBOT)
(:requirements
:strips
)
(:predicates
(Flag100-)
(Flag97-)
(Flag61-)
(Flag12-)
(Flag11-)
(Flag10-)
(Flag9-)
(Flag3-)
(Flag2-)
(ROW6-ROBOT)
(ROW5-ROBOT)
(ROW4-ROBOT)
(ROW3-ROBOT)
(ROW2-ROBOT)
(ROW1-ROBOT)
(BELOWOF7-ROBOT)
(NOT-BELOWOF6-ROBOT)
(BELOWOF5-ROBOT)
(BELOWOF4-ROBOT)
(BELOWOF3-ROBOT)
(BELOWOF2-ROBOT)
(ABOVEOF6-ROBOT)
(ABOVEOF5-ROBOT)
(ABOVEOF4-ROBOT)
(ABOVEOF3-ROBOT)
(ABOVEOF2-ROBOT)
(ABOVEOF1-ROBOT)
(CHECKCONSISTENCY-)
(ROW0-ROBOT)
(BELOWOF1-ROBOT)
(Flag102-)
(Flag101-)
(Flag95-)
(Flag94-)
(Flag93-)
(Flag91-)
(Flag90-)
(Flag89-)
(Flag87-)
(Flag86-)
(Flag85-)
(Flag84-)
(Flag83-)
(Flag81-)
(Flag80-)
(Flag79-)
(Flag78-)
(Flag77-)
(Flag76-)
(Flag74-)
(Flag73-)
(Flag72-)
(Flag71-)
(Flag70-)
(Flag69-)
(Flag68-)
(Flag67-)
(Flag66-)
(Flag65-)
(Flag64-)
(Flag63-)
(Flag62-)
(Flag60-)
(Flag59-)
(Flag58-)
(Flag57-)
(Flag56-)
(Flag55-)
(Flag54-)
(Flag53-)
(Flag52-)
(Flag4-)
(ERROR-)
(COLUMN5-ROBOT)
(COLUMN4-ROBOT)
(COLUMN3-ROBOT)
(COLUMN2-ROBOT)
(COLUMN1-ROBOT)
(COLUMN0-ROBOT)
(RIGHTOF5-ROBOT)
(RIGHTOF4-ROBOT)
(RIGHTOF3-ROBOT)
(RIGHTOF1-ROBOT)
(NOT-RIGHTOF2-ROBOT)
(RIGHTOF0-ROBOT)
(LEFTOF5-ROBOT)
(LEFTOF4-ROBOT)
(LEFTOF3-ROBOT)
(LEFTOF2-ROBOT)
(LEFTOF1-ROBOT)
(COLUMN6-ROBOT)
(LEFTOF7-ROBOT)
(NOT-LEFTOF6-ROBOT)
(RIGHTOF6-ROBOT)
(Flag103-)
(Flag98-)
(Flag96-)
(Flag92-)
(Flag88-)
(Flag82-)
(Flag75-)
(Flag50-)
(Flag49-)
(Flag47-)
(Flag46-)
(Flag44-)
(Flag43-)
(Flag42-)
(Flag40-)
(Flag39-)
(Flag38-)
(Flag37-)
(Flag36-)
(Flag34-)
(Flag33-)
(Flag32-)
(Flag31-)
(Flag30-)
(Flag28-)
(Flag27-)
(Flag26-)
(Flag25-)
(Flag24-)
(Flag23-)
(Flag22-)
(Flag21-)
(Flag20-)
(Flag19-)
(Flag18-)
(Flag17-)
(Flag16-)
(Flag15-)
(Flag14-)
(Flag13-)
(Flag8-)
(Flag7-)
(Flag6-)
(Flag5-)
(Flag1-)
(Flag99-)
(Flag51-)
(Flag48-)
(Flag45-)
(Flag41-)
(Flag35-)
(Flag29-)
(NOT-COLUMN6-ROBOT)
(NOT-COLUMN5-ROBOT)
(NOT-COLUMN4-ROBOT)
(NOT-COLUMN3-ROBOT)
(NOT-COLUMN2-ROBOT)
(NOT-COLUMN1-ROBOT)
(NOT-LEFTOF7-ROBOT)
(LEFTOF6-ROBOT)
(NOT-LEFTOF5-ROBOT)
(NOT-LEFTOF4-ROBOT)
(NOT-LEFTOF3-ROBOT)
(NOT-LEFTOF2-ROBOT)
(NOT-RIGHTOF6-ROBOT)
(NOT-RIGHTOF5-ROBOT)
(NOT-RIGHTOF4-ROBOT)
(NOT-RIGHTOF3-ROBOT)
(NOT-RIGHTOF1-ROBOT)
(NOT-CHECKCONSISTENCY-)
(NOT-COLUMN0-ROBOT)
(RIGHTOF2-ROBOT)
(NOT-RIGHTOF0-ROBOT)
(NOT-LEFTOF1-ROBOT)
(NOT-ERROR-)
(NOT-ROW5-ROBOT)
(NOT-ROW4-ROBOT)
(NOT-ROW3-ROBOT)
(NOT-ROW2-ROBOT)
(NOT-ROW1-ROBOT)
(NOT-ROW0-ROBOT)
(NOT-ABOVEOF5-ROBOT)
(NOT-ABOVEOF4-ROBOT)
(NOT-ABOVEOF3-ROBOT)
(NOT-ABOVEOF2-ROBOT)
(NOT-ABOVEOF1-ROBOT)
(NOT-BELOWOF5-ROBOT)
(NOT-BELOWOF4-ROBOT)
(NOT-BELOWOF3-ROBOT)
(NOT-BELOWOF2-ROBOT)
(NOT-BELOWOF1-ROBOT)
(NOT-ROW6-ROBOT)
(NOT-BELOWOF7-ROBOT)
(BELOWOF6-ROBOT)
(NOT-ABOVEOF6-ROBOT)
(Flag100prime-)
(Flag97prime-)
(Flag61prime-)
(Flag12prime-)
(Flag11prime-)
(Flag10prime-)
(Flag9prime-)
(Flag3prime-)
(Flag2prime-)
(Flag102prime-)
(Flag101prime-)
(Flag95prime-)
(Flag94prime-)
(Flag93prime-)
(Flag91prime-)
(Flag90prime-)
(Flag89prime-)
(Flag87prime-)
(Flag86prime-)
(Flag85prime-)
(Flag84prime-)
(Flag83prime-)
(Flag81prime-)
(Flag80prime-)
(Flag79prime-)
(Flag78prime-)
(Flag77prime-)
(Flag76prime-)
(Flag74prime-)
(Flag73prime-)
(Flag72prime-)
(Flag71prime-)
(Flag70prime-)
(Flag69prime-)
(Flag68prime-)
(Flag67prime-)
(Flag66prime-)
(Flag65prime-)
(Flag64prime-)
(Flag63prime-)
(Flag62prime-)
(Flag60prime-)
(Flag59prime-)
(Flag58prime-)
(Flag57prime-)
(Flag56prime-)
(Flag55prime-)
(Flag54prime-)
(Flag53prime-)
(Flag52prime-)
(Flag4prime-)
(Flag103prime-)
(Flag98prime-)
(Flag96prime-)
(Flag92prime-)
(Flag88prime-)
(Flag82prime-)
(Flag75prime-)
(Flag50prime-)
(Flag49prime-)
(Flag47prime-)
(Flag46prime-)
(Flag44prime-)
(Flag43prime-)
(Flag42prime-)
(Flag40prime-)
(Flag39prime-)
(Flag38prime-)
(Flag37prime-)
(Flag36prime-)
(Flag34prime-)
(Flag33prime-)
(Flag32prime-)
(Flag31prime-)
(Flag30prime-)
(Flag28prime-)
(Flag27prime-)
(Flag26prime-)
(Flag25prime-)
(Flag24prime-)
(Flag23prime-)
(Flag22prime-)
(Flag21prime-)
(Flag20prime-)
(Flag19prime-)
(Flag18prime-)
(Flag17prime-)
(Flag16prime-)
(Flag15prime-)
(Flag14prime-)
(Flag13prime-)
(Flag8prime-)
(Flag7prime-)
(Flag6prime-)
(Flag5prime-)
(Flag1prime-)
(Flag99prime-)
(Flag51prime-)
(Flag48prime-)
(Flag45prime-)
(Flag41prime-)
(Flag35prime-)
(Flag29prime-)
)
(:action Flag29Action-0
:parameters ()
:precondition
(and
(Flag16-)
(Flag16prime-)
)
:effect
(and
(Flag29-)
(Flag29prime-)
)

)
(:action Flag29Action-1
:parameters ()
:precondition
(and
(Flag17-)
(Flag17prime-)
)
:effect
(and
(Flag29-)
(Flag29prime-)
)

)
(:action Flag29Action-2
:parameters ()
:precondition
(and
(Flag18-)
(Flag18prime-)
)
:effect
(and
(Flag29-)
(Flag29prime-)
)

)
(:action Flag29Action-3
:parameters ()
:precondition
(and
(Flag19-)
(Flag19prime-)
)
:effect
(and
(Flag29-)
(Flag29prime-)
)

)
(:action Flag29Action-4
:parameters ()
:precondition
(and
(Flag20-)
(Flag20prime-)
)
:effect
(and
(Flag29-)
(Flag29prime-)
)

)
(:action Flag29Action-5
:parameters ()
:precondition
(and
(Flag21-)
(Flag21prime-)
)
:effect
(and
(Flag29-)
(Flag29prime-)
)

)
(:action Flag29Action-6
:parameters ()
:precondition
(and
(Flag22-)
(Flag22prime-)
)
:effect
(and
(Flag29-)
(Flag29prime-)
)

)
(:action Flag29Action-7
:parameters ()
:precondition
(and
(Flag23-)
(Flag23prime-)
)
:effect
(and
(Flag29-)
(Flag29prime-)
)

)
(:action Flag29Action-8
:parameters ()
:precondition
(and
(Flag24-)
(Flag24prime-)
)
:effect
(and
(Flag29-)
(Flag29prime-)
)

)
(:action Flag29Action-9
:parameters ()
:precondition
(and
(Flag25-)
(Flag25prime-)
)
:effect
(and
(Flag29-)
(Flag29prime-)
)

)
(:action Flag29Action-10
:parameters ()
:precondition
(and
(Flag26-)
(Flag26prime-)
)
:effect
(and
(Flag29-)
(Flag29prime-)
)

)
(:action Flag29Action-11
:parameters ()
:precondition
(and
(Flag27-)
(Flag27prime-)
)
:effect
(and
(Flag29-)
(Flag29prime-)
)

)
(:action Flag29Action-12
:parameters ()
:precondition
(and
(Flag28-)
(Flag28prime-)
)
:effect
(and
(Flag29-)
(Flag29prime-)
)

)
(:action Prim29Action
:parameters ()
:precondition
(and
(not (Flag16-))
(Flag16prime-)
(not (Flag17-))
(Flag17prime-)
(not (Flag18-))
(Flag18prime-)
(not (Flag19-))
(Flag19prime-)
(not (Flag20-))
(Flag20prime-)
(not (Flag21-))
(Flag21prime-)
(not (Flag22-))
(Flag22prime-)
(not (Flag23-))
(Flag23prime-)
(not (Flag24-))
(Flag24prime-)
(not (Flag25-))
(Flag25prime-)
(not (Flag26-))
(Flag26prime-)
(not (Flag27-))
(Flag27prime-)
(not (Flag28-))
(Flag28prime-)
)
:effect
(and
(Flag29prime-)
(not (Flag29-))
)

)
(:action Flag35Action-0
:parameters ()
:precondition
(and
(Flag16-)
(Flag16prime-)
)
:effect
(and
(Flag35-)
(Flag35prime-)
)

)
(:action Flag35Action-1
:parameters ()
:precondition
(and
(Flag21-)
(Flag21prime-)
)
:effect
(and
(Flag35-)
(Flag35prime-)
)

)
(:action Flag35Action-2
:parameters ()
:precondition
(and
(Flag18-)
(Flag18prime-)
)
:effect
(and
(Flag35-)
(Flag35prime-)
)

)
(:action Flag35Action-3
:parameters ()
:precondition
(and
(Flag30-)
(Flag30prime-)
)
:effect
(and
(Flag35-)
(Flag35prime-)
)

)
(:action Flag35Action-4
:parameters ()
:precondition
(and
(Flag20-)
(Flag20prime-)
)
:effect
(and
(Flag35-)
(Flag35prime-)
)

)
(:action Flag35Action-5
:parameters ()
:precondition
(and
(Flag26-)
(Flag26prime-)
)
:effect
(and
(Flag35-)
(Flag35prime-)
)

)
(:action Flag35Action-6
:parameters ()
:precondition
(and
(Flag31-)
(Flag31prime-)
)
:effect
(and
(Flag35-)
(Flag35prime-)
)

)
(:action Flag35Action-7
:parameters ()
:precondition
(and
(Flag22-)
(Flag22prime-)
)
:effect
(and
(Flag35-)
(Flag35prime-)
)

)
(:action Flag35Action-8
:parameters ()
:precondition
(and
(Flag23-)
(Flag23prime-)
)
:effect
(and
(Flag35-)
(Flag35prime-)
)

)
(:action Flag35Action-9
:parameters ()
:precondition
(and
(Flag24-)
(Flag24prime-)
)
:effect
(and
(Flag35-)
(Flag35prime-)
)

)
(:action Flag35Action-10
:parameters ()
:precondition
(and
(Flag32-)
(Flag32prime-)
)
:effect
(and
(Flag35-)
(Flag35prime-)
)

)
(:action Flag35Action-11
:parameters ()
:precondition
(and
(Flag25-)
(Flag25prime-)
)
:effect
(and
(Flag35-)
(Flag35prime-)
)

)
(:action Flag35Action-12
:parameters ()
:precondition
(and
(Flag33-)
(Flag33prime-)
)
:effect
(and
(Flag35-)
(Flag35prime-)
)

)
(:action Flag35Action-13
:parameters ()
:precondition
(and
(Flag27-)
(Flag27prime-)
)
:effect
(and
(Flag35-)
(Flag35prime-)
)

)
(:action Flag35Action-14
:parameters ()
:precondition
(and
(Flag28-)
(Flag28prime-)
)
:effect
(and
(Flag35-)
(Flag35prime-)
)

)
(:action Flag35Action-15
:parameters ()
:precondition
(and
(Flag34-)
(Flag34prime-)
)
:effect
(and
(Flag35-)
(Flag35prime-)
)

)
(:action Prim35Action
:parameters ()
:precondition
(and
(not (Flag16-))
(Flag16prime-)
(not (Flag21-))
(Flag21prime-)
(not (Flag18-))
(Flag18prime-)
(not (Flag30-))
(Flag30prime-)
(not (Flag20-))
(Flag20prime-)
(not (Flag26-))
(Flag26prime-)
(not (Flag31-))
(Flag31prime-)
(not (Flag22-))
(Flag22prime-)
(not (Flag23-))
(Flag23prime-)
(not (Flag24-))
(Flag24prime-)
(not (Flag32-))
(Flag32prime-)
(not (Flag25-))
(Flag25prime-)
(not (Flag33-))
(Flag33prime-)
(not (Flag27-))
(Flag27prime-)
(not (Flag28-))
(Flag28prime-)
(not (Flag34-))
(Flag34prime-)
)
:effect
(and
(Flag35prime-)
(not (Flag35-))
)

)
(:action Flag41Action-0
:parameters ()
:precondition
(and
(Flag16-)
(Flag16prime-)
)
:effect
(and
(Flag41-)
(Flag41prime-)
)

)
(:action Flag41Action-1
:parameters ()
:precondition
(and
(Flag21-)
(Flag21prime-)
)
:effect
(and
(Flag41-)
(Flag41prime-)
)

)
(:action Flag41Action-2
:parameters ()
:precondition
(and
(Flag36-)
(Flag36prime-)
)
:effect
(and
(Flag41-)
(Flag41prime-)
)

)
(:action Flag41Action-3
:parameters ()
:precondition
(and
(Flag30-)
(Flag30prime-)
)
:effect
(and
(Flag41-)
(Flag41prime-)
)

)
(:action Flag41Action-4
:parameters ()
:precondition
(and
(Flag18-)
(Flag18prime-)
)
:effect
(and
(Flag41-)
(Flag41prime-)
)

)
(:action Flag41Action-5
:parameters ()
:precondition
(and
(Flag20-)
(Flag20prime-)
)
:effect
(and
(Flag41-)
(Flag41prime-)
)

)
(:action Flag41Action-6
:parameters ()
:precondition
(and
(Flag37-)
(Flag37prime-)
)
:effect
(and
(Flag41-)
(Flag41prime-)
)

)
(:action Flag41Action-7
:parameters ()
:precondition
(and
(Flag38-)
(Flag38prime-)
)
:effect
(and
(Flag41-)
(Flag41prime-)
)

)
(:action Flag41Action-8
:parameters ()
:precondition
(and
(Flag31-)
(Flag31prime-)
)
:effect
(and
(Flag41-)
(Flag41prime-)
)

)
(:action Flag41Action-9
:parameters ()
:precondition
(and
(Flag23-)
(Flag23prime-)
)
:effect
(and
(Flag41-)
(Flag41prime-)
)

)
(:action Flag41Action-10
:parameters ()
:precondition
(and
(Flag32-)
(Flag32prime-)
)
:effect
(and
(Flag41-)
(Flag41prime-)
)

)
(:action Flag41Action-11
:parameters ()
:precondition
(and
(Flag25-)
(Flag25prime-)
)
:effect
(and
(Flag41-)
(Flag41prime-)
)

)
(:action Flag41Action-12
:parameters ()
:precondition
(and
(Flag26-)
(Flag26prime-)
)
:effect
(and
(Flag41-)
(Flag41prime-)
)

)
(:action Flag41Action-13
:parameters ()
:precondition
(and
(Flag39-)
(Flag39prime-)
)
:effect
(and
(Flag41-)
(Flag41prime-)
)

)
(:action Flag41Action-14
:parameters ()
:precondition
(and
(Flag27-)
(Flag27prime-)
)
:effect
(and
(Flag41-)
(Flag41prime-)
)

)
(:action Flag41Action-15
:parameters ()
:precondition
(and
(Flag40-)
(Flag40prime-)
)
:effect
(and
(Flag41-)
(Flag41prime-)
)

)
(:action Flag41Action-16
:parameters ()
:precondition
(and
(Flag28-)
(Flag28prime-)
)
:effect
(and
(Flag41-)
(Flag41prime-)
)

)
(:action Prim41Action
:parameters ()
:precondition
(and
(not (Flag16-))
(Flag16prime-)
(not (Flag21-))
(Flag21prime-)
(not (Flag36-))
(Flag36prime-)
(not (Flag30-))
(Flag30prime-)
(not (Flag18-))
(Flag18prime-)
(not (Flag20-))
(Flag20prime-)
(not (Flag37-))
(Flag37prime-)
(not (Flag38-))
(Flag38prime-)
(not (Flag31-))
(Flag31prime-)
(not (Flag23-))
(Flag23prime-)
(not (Flag32-))
(Flag32prime-)
(not (Flag25-))
(Flag25prime-)
(not (Flag26-))
(Flag26prime-)
(not (Flag39-))
(Flag39prime-)
(not (Flag27-))
(Flag27prime-)
(not (Flag40-))
(Flag40prime-)
(not (Flag28-))
(Flag28prime-)
)
:effect
(and
(Flag41prime-)
(not (Flag41-))
)

)
(:action Flag45Action-0
:parameters ()
:precondition
(and
(Flag16-)
(Flag16prime-)
)
:effect
(and
(Flag45-)
(Flag45prime-)
)

)
(:action Flag45Action-1
:parameters ()
:precondition
(and
(Flag21-)
(Flag21prime-)
)
:effect
(and
(Flag45-)
(Flag45prime-)
)

)
(:action Flag45Action-2
:parameters ()
:precondition
(and
(Flag42-)
(Flag42prime-)
)
:effect
(and
(Flag45-)
(Flag45prime-)
)

)
(:action Flag45Action-3
:parameters ()
:precondition
(and
(Flag18-)
(Flag18prime-)
)
:effect
(and
(Flag45-)
(Flag45prime-)
)

)
(:action Flag45Action-4
:parameters ()
:precondition
(and
(Flag30-)
(Flag30prime-)
)
:effect
(and
(Flag45-)
(Flag45prime-)
)

)
(:action Flag45Action-5
:parameters ()
:precondition
(and
(Flag37-)
(Flag37prime-)
)
:effect
(and
(Flag45-)
(Flag45prime-)
)

)
(:action Flag45Action-6
:parameters ()
:precondition
(and
(Flag38-)
(Flag38prime-)
)
:effect
(and
(Flag45-)
(Flag45prime-)
)

)
(:action Flag45Action-7
:parameters ()
:precondition
(and
(Flag31-)
(Flag31prime-)
)
:effect
(and
(Flag45-)
(Flag45prime-)
)

)
(:action Flag45Action-8
:parameters ()
:precondition
(and
(Flag23-)
(Flag23prime-)
)
:effect
(and
(Flag45-)
(Flag45prime-)
)

)
(:action Flag45Action-9
:parameters ()
:precondition
(and
(Flag32-)
(Flag32prime-)
)
:effect
(and
(Flag45-)
(Flag45prime-)
)

)
(:action Flag45Action-10
:parameters ()
:precondition
(and
(Flag26-)
(Flag26prime-)
)
:effect
(and
(Flag45-)
(Flag45prime-)
)

)
(:action Flag45Action-11
:parameters ()
:precondition
(and
(Flag43-)
(Flag43prime-)
)
:effect
(and
(Flag45-)
(Flag45prime-)
)

)
(:action Flag45Action-12
:parameters ()
:precondition
(and
(Flag27-)
(Flag27prime-)
)
:effect
(and
(Flag45-)
(Flag45prime-)
)

)
(:action Flag45Action-13
:parameters ()
:precondition
(and
(Flag40-)
(Flag40prime-)
)
:effect
(and
(Flag45-)
(Flag45prime-)
)

)
(:action Flag45Action-14
:parameters ()
:precondition
(and
(Flag28-)
(Flag28prime-)
)
:effect
(and
(Flag45-)
(Flag45prime-)
)

)
(:action Flag45Action-15
:parameters ()
:precondition
(and
(Flag44-)
(Flag44prime-)
)
:effect
(and
(Flag45-)
(Flag45prime-)
)

)
(:action Prim45Action
:parameters ()
:precondition
(and
(not (Flag16-))
(Flag16prime-)
(not (Flag21-))
(Flag21prime-)
(not (Flag42-))
(Flag42prime-)
(not (Flag18-))
(Flag18prime-)
(not (Flag30-))
(Flag30prime-)
(not (Flag37-))
(Flag37prime-)
(not (Flag38-))
(Flag38prime-)
(not (Flag31-))
(Flag31prime-)
(not (Flag23-))
(Flag23prime-)
(not (Flag32-))
(Flag32prime-)
(not (Flag26-))
(Flag26prime-)
(not (Flag43-))
(Flag43prime-)
(not (Flag27-))
(Flag27prime-)
(not (Flag40-))
(Flag40prime-)
(not (Flag28-))
(Flag28prime-)
(not (Flag44-))
(Flag44prime-)
)
:effect
(and
(Flag45prime-)
(not (Flag45-))
)

)
(:action Flag48Action-0
:parameters ()
:precondition
(and
(Flag46-)
(Flag46prime-)
)
:effect
(and
(Flag48-)
(Flag48prime-)
)

)
(:action Flag48Action-1
:parameters ()
:precondition
(and
(Flag18-)
(Flag18prime-)
)
:effect
(and
(Flag48-)
(Flag48prime-)
)

)
(:action Flag48Action-2
:parameters ()
:precondition
(and
(Flag30-)
(Flag30prime-)
)
:effect
(and
(Flag48-)
(Flag48prime-)
)

)
(:action Flag48Action-3
:parameters ()
:precondition
(and
(Flag37-)
(Flag37prime-)
)
:effect
(and
(Flag48-)
(Flag48prime-)
)

)
(:action Flag48Action-4
:parameters ()
:precondition
(and
(Flag38-)
(Flag38prime-)
)
:effect
(and
(Flag48-)
(Flag48prime-)
)

)
(:action Flag48Action-5
:parameters ()
:precondition
(and
(Flag21-)
(Flag21prime-)
)
:effect
(and
(Flag48-)
(Flag48prime-)
)

)
(:action Flag48Action-6
:parameters ()
:precondition
(and
(Flag31-)
(Flag31prime-)
)
:effect
(and
(Flag48-)
(Flag48prime-)
)

)
(:action Flag48Action-7
:parameters ()
:precondition
(and
(Flag47-)
(Flag47prime-)
)
:effect
(and
(Flag48-)
(Flag48prime-)
)

)
(:action Flag48Action-8
:parameters ()
:precondition
(and
(Flag26-)
(Flag26prime-)
)
:effect
(and
(Flag48-)
(Flag48prime-)
)

)
(:action Flag48Action-9
:parameters ()
:precondition
(and
(Flag43-)
(Flag43prime-)
)
:effect
(and
(Flag48-)
(Flag48prime-)
)

)
(:action Flag48Action-10
:parameters ()
:precondition
(and
(Flag27-)
(Flag27prime-)
)
:effect
(and
(Flag48-)
(Flag48prime-)
)

)
(:action Flag48Action-11
:parameters ()
:precondition
(and
(Flag44-)
(Flag44prime-)
)
:effect
(and
(Flag48-)
(Flag48prime-)
)

)
(:action Prim48Action
:parameters ()
:precondition
(and
(not (Flag46-))
(Flag46prime-)
(not (Flag18-))
(Flag18prime-)
(not (Flag30-))
(Flag30prime-)
(not (Flag37-))
(Flag37prime-)
(not (Flag38-))
(Flag38prime-)
(not (Flag21-))
(Flag21prime-)
(not (Flag31-))
(Flag31prime-)
(not (Flag47-))
(Flag47prime-)
(not (Flag26-))
(Flag26prime-)
(not (Flag43-))
(Flag43prime-)
(not (Flag27-))
(Flag27prime-)
(not (Flag44-))
(Flag44prime-)
)
:effect
(and
(Flag48prime-)
(not (Flag48-))
)

)
(:action Flag51Action-0
:parameters ()
:precondition
(and
(Flag49-)
(Flag49prime-)
)
:effect
(and
(Flag51-)
(Flag51prime-)
)

)
(:action Flag51Action-1
:parameters ()
:precondition
(and
(Flag50-)
(Flag50prime-)
)
:effect
(and
(Flag51-)
(Flag51prime-)
)

)
(:action Flag51Action-2
:parameters ()
:precondition
(and
(Flag30-)
(Flag30prime-)
)
:effect
(and
(Flag51-)
(Flag51prime-)
)

)
(:action Flag51Action-3
:parameters ()
:precondition
(and
(Flag37-)
(Flag37prime-)
)
:effect
(and
(Flag51-)
(Flag51prime-)
)

)
(:action Flag51Action-4
:parameters ()
:precondition
(and
(Flag26-)
(Flag26prime-)
)
:effect
(and
(Flag51-)
(Flag51prime-)
)

)
(:action Flag51Action-5
:parameters ()
:precondition
(and
(Flag27-)
(Flag27prime-)
)
:effect
(and
(Flag51-)
(Flag51prime-)
)

)
(:action Flag51Action-6
:parameters ()
:precondition
(and
(Flag44-)
(Flag44prime-)
)
:effect
(and
(Flag51-)
(Flag51prime-)
)

)
(:action Prim51Action
:parameters ()
:precondition
(and
(not (Flag49-))
(Flag49prime-)
(not (Flag50-))
(Flag50prime-)
(not (Flag30-))
(Flag30prime-)
(not (Flag37-))
(Flag37prime-)
(not (Flag26-))
(Flag26prime-)
(not (Flag27-))
(Flag27prime-)
(not (Flag44-))
(Flag44prime-)
)
:effect
(and
(Flag51prime-)
(not (Flag51-))
)

)
(:action Flag99Action-0
:parameters ()
:precondition
(and
(Flag16-)
(Flag16prime-)
)
:effect
(and
(Flag99-)
(Flag99prime-)
)

)
(:action Flag99Action-1
:parameters ()
:precondition
(and
(Flag19-)
(Flag19prime-)
)
:effect
(and
(Flag99-)
(Flag99prime-)
)

)
(:action Flag99Action-2
:parameters ()
:precondition
(and
(Flag20-)
(Flag20prime-)
)
:effect
(and
(Flag99-)
(Flag99prime-)
)

)
(:action Flag99Action-3
:parameters ()
:precondition
(and
(Flag98-)
(Flag98prime-)
)
:effect
(and
(Flag99-)
(Flag99prime-)
)

)
(:action Flag99Action-4
:parameters ()
:precondition
(and
(Flag23-)
(Flag23prime-)
)
:effect
(and
(Flag99-)
(Flag99prime-)
)

)
(:action Flag99Action-5
:parameters ()
:precondition
(and
(Flag24-)
(Flag24prime-)
)
:effect
(and
(Flag99-)
(Flag99prime-)
)

)
(:action Flag99Action-6
:parameters ()
:precondition
(and
(Flag26-)
(Flag26prime-)
)
:effect
(and
(Flag99-)
(Flag99prime-)
)

)
(:action Prim99Action
:parameters ()
:precondition
(and
(not (Flag16-))
(Flag16prime-)
(not (Flag19-))
(Flag19prime-)
(not (Flag20-))
(Flag20prime-)
(not (Flag98-))
(Flag98prime-)
(not (Flag23-))
(Flag23prime-)
(not (Flag24-))
(Flag24prime-)
(not (Flag26-))
(Flag26prime-)
)
:effect
(and
(Flag99prime-)
(not (Flag99-))
)

)
(:action Flag1Action
:parameters ()
:precondition
(and
(COLUMN2-ROBOT)
(ROW1-ROBOT)
(NOT-ERROR-)
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(Flag1-)
(Flag1prime-)
)

)
(:action Prim1Action-0
:parameters ()
:precondition
(and
(not (COLUMN2-ROBOT))
)
:effect
(and
(Flag1prime-)
(not (Flag1-))
)

)
(:action Flag3Action-0
:parameters ()
:precondition
(and
(RIGHTOF6-ROBOT)
)
:effect
(and
(Flag3-)
(Flag3prime-)
)

)
(:action Flag3Action-1
:parameters ()
:precondition
(and
(LEFTOF4-ROBOT)
)
:effect
(and
(Flag3-)
(Flag3prime-)
)

)
(:action Flag3Action-2
:parameters ()
:precondition
(and
(LEFTOF3-ROBOT)
)
:effect
(and
(Flag3-)
(Flag3prime-)
)

)
(:action Flag3Action-3
:parameters ()
:precondition
(and
(LEFTOF5-ROBOT)
)
:effect
(and
(Flag3-)
(Flag3prime-)
)

)
(:action Flag3Action-4
:parameters ()
:precondition
(and
(RIGHTOF1-ROBOT)
)
:effect
(and
(Flag3-)
(Flag3prime-)
)

)
(:action Flag3Action-5
:parameters ()
:precondition
(and
(RIGHTOF4-ROBOT)
)
:effect
(and
(Flag3-)
(Flag3prime-)
)

)
(:action Flag3Action-6
:parameters ()
:precondition
(and
(RIGHTOF0-ROBOT)
)
:effect
(and
(Flag3-)
(Flag3prime-)
)

)
(:action Flag3Action-7
:parameters ()
:precondition
(and
(LEFTOF7-ROBOT)
)
:effect
(and
(Flag3-)
(Flag3prime-)
)

)
(:action Flag3Action-8
:parameters ()
:precondition
(and
(LEFTOF2-ROBOT)
)
:effect
(and
(Flag3-)
(Flag3prime-)
)

)
(:action Flag3Action-10
:parameters ()
:precondition
(and
(RIGHTOF5-ROBOT)
)
:effect
(and
(Flag3-)
(Flag3prime-)
)

)
(:action Flag3Action-11
:parameters ()
:precondition
(and
(LEFTOF1-ROBOT)
)
:effect
(and
(Flag3-)
(Flag3prime-)
)

)
(:action Flag3Action-13
:parameters ()
:precondition
(and
(RIGHTOF3-ROBOT)
)
:effect
(and
(Flag3-)
(Flag3prime-)
)

)
(:action Prim3Action
:parameters ()
:precondition
(and
(not (RIGHTOF6-ROBOT))
(not (LEFTOF4-ROBOT))
(not (LEFTOF3-ROBOT))
(not (LEFTOF5-ROBOT))
(not (RIGHTOF1-ROBOT))
(not (RIGHTOF4-ROBOT))
(not (RIGHTOF0-ROBOT))
(not (LEFTOF7-ROBOT))
(not (LEFTOF2-ROBOT))
(not (RIGHTOF2-ROBOT))
(not (RIGHTOF5-ROBOT))
(not (LEFTOF1-ROBOT))
(not (LEFTOF6-ROBOT))
(not (RIGHTOF3-ROBOT))
)
:effect
(and
(Flag3prime-)
(not (Flag3-))
)

)
(:action Flag5Action-0
:parameters ()
:precondition
(and
(LEFTOF1-ROBOT)
)
:effect
(and
(Flag5-)
(Flag5prime-)
)

)
(:action Flag5Action-1
:parameters ()
:precondition
(and
(LEFTOF2-ROBOT)
)
:effect
(and
(Flag5-)
(Flag5prime-)
)

)
(:action Prim5Action
:parameters ()
:precondition
(and
(not (LEFTOF1-ROBOT))
(not (LEFTOF2-ROBOT))
)
:effect
(and
(Flag5prime-)
(not (Flag5-))
)

)
(:action Flag6Action-0
:parameters ()
:precondition
(and
(LEFTOF1-ROBOT)
)
:effect
(and
(Flag6-)
(Flag6prime-)
)

)
(:action Flag6Action-1
:parameters ()
:precondition
(and
(LEFTOF3-ROBOT)
)
:effect
(and
(Flag6-)
(Flag6prime-)
)

)
(:action Flag6Action-2
:parameters ()
:precondition
(and
(LEFTOF2-ROBOT)
)
:effect
(and
(Flag6-)
(Flag6prime-)
)

)
(:action Prim6Action
:parameters ()
:precondition
(and
(not (LEFTOF1-ROBOT))
(not (LEFTOF3-ROBOT))
(not (LEFTOF2-ROBOT))
)
:effect
(and
(Flag6prime-)
(not (Flag6-))
)

)
(:action Flag7Action-0
:parameters ()
:precondition
(and
(LEFTOF1-ROBOT)
)
:effect
(and
(Flag7-)
(Flag7prime-)
)

)
(:action Flag7Action-1
:parameters ()
:precondition
(and
(LEFTOF3-ROBOT)
)
:effect
(and
(Flag7-)
(Flag7prime-)
)

)
(:action Flag7Action-2
:parameters ()
:precondition
(and
(LEFTOF4-ROBOT)
)
:effect
(and
(Flag7-)
(Flag7prime-)
)

)
(:action Flag7Action-3
:parameters ()
:precondition
(and
(LEFTOF2-ROBOT)
)
:effect
(and
(Flag7-)
(Flag7prime-)
)

)
(:action Prim7Action
:parameters ()
:precondition
(and
(not (LEFTOF1-ROBOT))
(not (LEFTOF3-ROBOT))
(not (LEFTOF4-ROBOT))
(not (LEFTOF2-ROBOT))
)
:effect
(and
(Flag7prime-)
(not (Flag7-))
)

)
(:action Flag8Action-0
:parameters ()
:precondition
(and
(LEFTOF5-ROBOT)
)
:effect
(and
(Flag8-)
(Flag8prime-)
)

)
(:action Flag8Action-1
:parameters ()
:precondition
(and
(LEFTOF1-ROBOT)
)
:effect
(and
(Flag8-)
(Flag8prime-)
)

)
(:action Flag8Action-2
:parameters ()
:precondition
(and
(LEFTOF3-ROBOT)
)
:effect
(and
(Flag8-)
(Flag8prime-)
)

)
(:action Flag8Action-3
:parameters ()
:precondition
(and
(LEFTOF4-ROBOT)
)
:effect
(and
(Flag8-)
(Flag8prime-)
)

)
(:action Flag8Action-4
:parameters ()
:precondition
(and
(LEFTOF2-ROBOT)
)
:effect
(and
(Flag8-)
(Flag8prime-)
)

)
(:action Prim8Action
:parameters ()
:precondition
(and
(not (LEFTOF5-ROBOT))
(not (LEFTOF1-ROBOT))
(not (LEFTOF3-ROBOT))
(not (LEFTOF4-ROBOT))
(not (LEFTOF2-ROBOT))
)
:effect
(and
(Flag8prime-)
(not (Flag8-))
)

)
(:action Flag9Action-0
:parameters ()
:precondition
(and
(LEFTOF2-ROBOT)
)
:effect
(and
(Flag9-)
(Flag9prime-)
)

)
(:action Flag9Action-1
:parameters ()
:precondition
(and
(LEFTOF1-ROBOT)
)
:effect
(and
(Flag9-)
(Flag9prime-)
)

)
(:action Flag9Action-3
:parameters ()
:precondition
(and
(LEFTOF5-ROBOT)
)
:effect
(and
(Flag9-)
(Flag9prime-)
)

)
(:action Flag9Action-4
:parameters ()
:precondition
(and
(LEFTOF3-ROBOT)
)
:effect
(and
(Flag9-)
(Flag9prime-)
)

)
(:action Flag9Action-5
:parameters ()
:precondition
(and
(LEFTOF4-ROBOT)
)
:effect
(and
(Flag9-)
(Flag9prime-)
)

)
(:action Prim9Action
:parameters ()
:precondition
(and
(not (LEFTOF2-ROBOT))
(not (LEFTOF1-ROBOT))
(not (LEFTOF6-ROBOT))
(not (LEFTOF5-ROBOT))
(not (LEFTOF3-ROBOT))
(not (LEFTOF4-ROBOT))
)
:effect
(and
(Flag9prime-)
(not (Flag9-))
)

)
(:action Flag10Action-0
:parameters ()
:precondition
(and
(LEFTOF2-ROBOT)
)
:effect
(and
(Flag10-)
(Flag10prime-)
)

)
(:action Flag10Action-1
:parameters ()
:precondition
(and
(LEFTOF7-ROBOT)
)
:effect
(and
(Flag10-)
(Flag10prime-)
)

)
(:action Flag10Action-2
:parameters ()
:precondition
(and
(LEFTOF1-ROBOT)
)
:effect
(and
(Flag10-)
(Flag10prime-)
)

)
(:action Flag10Action-4
:parameters ()
:precondition
(and
(LEFTOF5-ROBOT)
)
:effect
(and
(Flag10-)
(Flag10prime-)
)

)
(:action Flag10Action-5
:parameters ()
:precondition
(and
(LEFTOF3-ROBOT)
)
:effect
(and
(Flag10-)
(Flag10prime-)
)

)
(:action Flag10Action-6
:parameters ()
:precondition
(and
(LEFTOF4-ROBOT)
)
:effect
(and
(Flag10-)
(Flag10prime-)
)

)
(:action Prim10Action
:parameters ()
:precondition
(and
(not (LEFTOF2-ROBOT))
(not (LEFTOF7-ROBOT))
(not (LEFTOF1-ROBOT))
(not (LEFTOF6-ROBOT))
(not (LEFTOF5-ROBOT))
(not (LEFTOF3-ROBOT))
(not (LEFTOF4-ROBOT))
)
:effect
(and
(Flag10prime-)
(not (Flag10-))
)

)
(:action Flag11Action-0
:parameters ()
:precondition
(and
(RIGHTOF3-ROBOT)
)
:effect
(and
(Flag11-)
(Flag11prime-)
)

)
(:action Flag11Action-1
:parameters ()
:precondition
(and
(RIGHTOF5-ROBOT)
)
:effect
(and
(Flag11-)
(Flag11prime-)
)

)
(:action Flag11Action-3
:parameters ()
:precondition
(and
(RIGHTOF4-ROBOT)
)
:effect
(and
(Flag11-)
(Flag11prime-)
)

)
(:action Flag11Action-4
:parameters ()
:precondition
(and
(RIGHTOF1-ROBOT)
)
:effect
(and
(Flag11-)
(Flag11prime-)
)

)
(:action Flag11Action-5
:parameters ()
:precondition
(and
(RIGHTOF6-ROBOT)
)
:effect
(and
(Flag11-)
(Flag11prime-)
)

)
(:action Prim11Action
:parameters ()
:precondition
(and
(not (RIGHTOF3-ROBOT))
(not (RIGHTOF5-ROBOT))
(not (RIGHTOF2-ROBOT))
(not (RIGHTOF4-ROBOT))
(not (RIGHTOF1-ROBOT))
(not (RIGHTOF6-ROBOT))
)
:effect
(and
(Flag11prime-)
(not (Flag11-))
)

)
(:action Flag12Action-0
:parameters ()
:precondition
(and
(RIGHTOF6-ROBOT)
)
:effect
(and
(Flag12-)
(Flag12prime-)
)

)
(:action Flag12Action-1
:parameters ()
:precondition
(and
(RIGHTOF3-ROBOT)
)
:effect
(and
(Flag12-)
(Flag12prime-)
)

)
(:action Flag12Action-2
:parameters ()
:precondition
(and
(RIGHTOF4-ROBOT)
)
:effect
(and
(Flag12-)
(Flag12prime-)
)

)
(:action Flag12Action-3
:parameters ()
:precondition
(and
(RIGHTOF5-ROBOT)
)
:effect
(and
(Flag12-)
(Flag12prime-)
)

)
(:action Prim12Action
:parameters ()
:precondition
(and
(not (RIGHTOF6-ROBOT))
(not (RIGHTOF3-ROBOT))
(not (RIGHTOF4-ROBOT))
(not (RIGHTOF5-ROBOT))
(not (RIGHTOF2-ROBOT))
)
:effect
(and
(Flag12prime-)
(not (Flag12-))
)

)
(:action Flag13Action-0
:parameters ()
:precondition
(and
(RIGHTOF3-ROBOT)
)
:effect
(and
(Flag13-)
(Flag13prime-)
)

)
(:action Flag13Action-1
:parameters ()
:precondition
(and
(RIGHTOF4-ROBOT)
)
:effect
(and
(Flag13-)
(Flag13prime-)
)

)
(:action Flag13Action-2
:parameters ()
:precondition
(and
(RIGHTOF5-ROBOT)
)
:effect
(and
(Flag13-)
(Flag13prime-)
)

)
(:action Flag13Action-3
:parameters ()
:precondition
(and
(RIGHTOF6-ROBOT)
)
:effect
(and
(Flag13-)
(Flag13prime-)
)

)
(:action Prim13Action
:parameters ()
:precondition
(and
(not (RIGHTOF3-ROBOT))
(not (RIGHTOF4-ROBOT))
(not (RIGHTOF5-ROBOT))
(not (RIGHTOF6-ROBOT))
)
:effect
(and
(Flag13prime-)
(not (Flag13-))
)

)
(:action Flag14Action-0
:parameters ()
:precondition
(and
(RIGHTOF4-ROBOT)
)
:effect
(and
(Flag14-)
(Flag14prime-)
)

)
(:action Flag14Action-1
:parameters ()
:precondition
(and
(RIGHTOF5-ROBOT)
)
:effect
(and
(Flag14-)
(Flag14prime-)
)

)
(:action Flag14Action-2
:parameters ()
:precondition
(and
(RIGHTOF6-ROBOT)
)
:effect
(and
(Flag14-)
(Flag14prime-)
)

)
(:action Prim14Action
:parameters ()
:precondition
(and
(not (RIGHTOF4-ROBOT))
(not (RIGHTOF5-ROBOT))
(not (RIGHTOF6-ROBOT))
)
:effect
(and
(Flag14prime-)
(not (Flag14-))
)

)
(:action Flag15Action-0
:parameters ()
:precondition
(and
(RIGHTOF5-ROBOT)
)
:effect
(and
(Flag15-)
(Flag15prime-)
)

)
(:action Flag15Action-1
:parameters ()
:precondition
(and
(RIGHTOF6-ROBOT)
)
:effect
(and
(Flag15-)
(Flag15prime-)
)

)
(:action Prim15Action
:parameters ()
:precondition
(and
(not (RIGHTOF5-ROBOT))
(not (RIGHTOF6-ROBOT))
)
:effect
(and
(Flag15prime-)
(not (Flag15-))
)

)
(:action Flag16Action
:parameters ()
:precondition
(and
(LEFTOF1-ROBOT)
(RIGHTOF4-ROBOT)
)
:effect
(and
(Flag16-)
(Flag16prime-)
)

)
(:action Flag17Action
:parameters ()
:precondition
(and
(LEFTOF2-ROBOT)
(RIGHTOF1-ROBOT)
)
:effect
(and
(Flag17-)
(Flag17prime-)
)

)
(:action Flag18Action
:parameters ()
:precondition
(and
(RIGHTOF5-ROBOT)
(LEFTOF2-ROBOT)
)
:effect
(and
(Flag18-)
(Flag18prime-)
)

)
(:action Flag19Action
:parameters ()
:precondition
(and
(LEFTOF1-ROBOT)
(RIGHTOF1-ROBOT)
)
:effect
(and
(Flag19-)
(Flag19prime-)
)

)
(:action Flag20Action
:parameters ()
:precondition
(and
(RIGHTOF3-ROBOT)
(LEFTOF1-ROBOT)
)
:effect
(and
(Flag20-)
(Flag20prime-)
)

)
(:action Flag21Action
:parameters ()
:precondition
(and
(RIGHTOF5-ROBOT)
(LEFTOF1-ROBOT)
)
:effect
(and
(Flag21-)
(Flag21prime-)
)

)
(:action Flag22Action
:parameters ()
:precondition
(and
(RIGHTOF2-ROBOT)
(LEFTOF2-ROBOT)
)
:effect
(and
(Flag22-)
(Flag22prime-)
)

)
(:action Flag23Action
:parameters ()
:precondition
(and
(LEFTOF1-ROBOT)
(RIGHTOF5-ROBOT)
)
:effect
(and
(Flag23-)
(Flag23prime-)
)

)
(:action Flag24Action
:parameters ()
:precondition
(and
(LEFTOF1-ROBOT)
(RIGHTOF2-ROBOT)
)
:effect
(and
(Flag24-)
(Flag24prime-)
)

)
(:action Flag25Action
:parameters ()
:precondition
(and
(RIGHTOF3-ROBOT)
(LEFTOF2-ROBOT)
)
:effect
(and
(Flag25-)
(Flag25prime-)
)

)
(:action Flag26Action
:parameters ()
:precondition
(and
(LEFTOF1-ROBOT)
(RIGHTOF6-ROBOT)
)
:effect
(and
(Flag26-)
(Flag26prime-)
)

)
(:action Flag27Action
:parameters ()
:precondition
(and
(LEFTOF2-ROBOT)
(RIGHTOF6-ROBOT)
)
:effect
(and
(Flag27-)
(Flag27prime-)
)

)
(:action Flag28Action
:parameters ()
:precondition
(and
(LEFTOF2-ROBOT)
(RIGHTOF4-ROBOT)
)
:effect
(and
(Flag28-)
(Flag28prime-)
)

)
(:action Prim16Action-0
:parameters ()
:precondition
(and
(not (LEFTOF1-ROBOT))
)
:effect
(and
(Flag16prime-)
(not (Flag16-))
)

)
(:action Prim16Action-1
:parameters ()
:precondition
(and
(not (RIGHTOF4-ROBOT))
)
:effect
(and
(Flag16prime-)
(not (Flag16-))
)

)
(:action Prim17Action-0
:parameters ()
:precondition
(and
(not (LEFTOF2-ROBOT))
)
:effect
(and
(Flag17prime-)
(not (Flag17-))
)

)
(:action Prim17Action-1
:parameters ()
:precondition
(and
(not (RIGHTOF1-ROBOT))
)
:effect
(and
(Flag17prime-)
(not (Flag17-))
)

)
(:action Prim18Action-0
:parameters ()
:precondition
(and
(not (RIGHTOF5-ROBOT))
)
:effect
(and
(Flag18prime-)
(not (Flag18-))
)

)
(:action Prim18Action-1
:parameters ()
:precondition
(and
(not (LEFTOF2-ROBOT))
)
:effect
(and
(Flag18prime-)
(not (Flag18-))
)

)
(:action Prim19Action-0
:parameters ()
:precondition
(and
(not (LEFTOF1-ROBOT))
)
:effect
(and
(Flag19prime-)
(not (Flag19-))
)

)
(:action Prim19Action-1
:parameters ()
:precondition
(and
(not (RIGHTOF1-ROBOT))
)
:effect
(and
(Flag19prime-)
(not (Flag19-))
)

)
(:action Prim20Action-0
:parameters ()
:precondition
(and
(not (RIGHTOF3-ROBOT))
)
:effect
(and
(Flag20prime-)
(not (Flag20-))
)

)
(:action Prim20Action-1
:parameters ()
:precondition
(and
(not (LEFTOF1-ROBOT))
)
:effect
(and
(Flag20prime-)
(not (Flag20-))
)

)
(:action Prim21Action-0
:parameters ()
:precondition
(and
(not (RIGHTOF5-ROBOT))
)
:effect
(and
(Flag21prime-)
(not (Flag21-))
)

)
(:action Prim21Action-1
:parameters ()
:precondition
(and
(not (LEFTOF1-ROBOT))
)
:effect
(and
(Flag21prime-)
(not (Flag21-))
)

)
(:action Prim22Action-1
:parameters ()
:precondition
(and
(not (LEFTOF2-ROBOT))
)
:effect
(and
(Flag22prime-)
(not (Flag22-))
)

)
(:action Prim23Action-0
:parameters ()
:precondition
(and
(not (LEFTOF1-ROBOT))
)
:effect
(and
(Flag23prime-)
(not (Flag23-))
)

)
(:action Prim23Action-1
:parameters ()
:precondition
(and
(not (RIGHTOF5-ROBOT))
)
:effect
(and
(Flag23prime-)
(not (Flag23-))
)

)
(:action Prim24Action-0
:parameters ()
:precondition
(and
(not (LEFTOF1-ROBOT))
)
:effect
(and
(Flag24prime-)
(not (Flag24-))
)

)
(:action Prim25Action-0
:parameters ()
:precondition
(and
(not (RIGHTOF3-ROBOT))
)
:effect
(and
(Flag25prime-)
(not (Flag25-))
)

)
(:action Prim25Action-1
:parameters ()
:precondition
(and
(not (LEFTOF2-ROBOT))
)
:effect
(and
(Flag25prime-)
(not (Flag25-))
)

)
(:action Prim26Action-0
:parameters ()
:precondition
(and
(not (LEFTOF1-ROBOT))
)
:effect
(and
(Flag26prime-)
(not (Flag26-))
)

)
(:action Prim26Action-1
:parameters ()
:precondition
(and
(not (RIGHTOF6-ROBOT))
)
:effect
(and
(Flag26prime-)
(not (Flag26-))
)

)
(:action Prim27Action-0
:parameters ()
:precondition
(and
(not (LEFTOF2-ROBOT))
)
:effect
(and
(Flag27prime-)
(not (Flag27-))
)

)
(:action Prim27Action-1
:parameters ()
:precondition
(and
(not (RIGHTOF6-ROBOT))
)
:effect
(and
(Flag27prime-)
(not (Flag27-))
)

)
(:action Prim28Action-0
:parameters ()
:precondition
(and
(not (LEFTOF2-ROBOT))
)
:effect
(and
(Flag28prime-)
(not (Flag28-))
)

)
(:action Prim28Action-1
:parameters ()
:precondition
(and
(not (RIGHTOF4-ROBOT))
)
:effect
(and
(Flag28prime-)
(not (Flag28-))
)

)
(:action Flag30Action
:parameters ()
:precondition
(and
(LEFTOF3-ROBOT)
(RIGHTOF6-ROBOT)
)
:effect
(and
(Flag30-)
(Flag30prime-)
)

)
(:action Flag31Action
:parameters ()
:precondition
(and
(LEFTOF3-ROBOT)
(RIGHTOF5-ROBOT)
)
:effect
(and
(Flag31-)
(Flag31prime-)
)

)
(:action Flag32Action
:parameters ()
:precondition
(and
(LEFTOF3-ROBOT)
(RIGHTOF4-ROBOT)
)
:effect
(and
(Flag32-)
(Flag32prime-)
)

)
(:action Flag33Action
:parameters ()
:precondition
(and
(RIGHTOF3-ROBOT)
(LEFTOF3-ROBOT)
)
:effect
(and
(Flag33-)
(Flag33prime-)
)

)
(:action Flag34Action
:parameters ()
:precondition
(and
(LEFTOF3-ROBOT)
(RIGHTOF2-ROBOT)
)
:effect
(and
(Flag34-)
(Flag34prime-)
)

)
(:action Prim30Action-0
:parameters ()
:precondition
(and
(not (LEFTOF3-ROBOT))
)
:effect
(and
(Flag30prime-)
(not (Flag30-))
)

)
(:action Prim30Action-1
:parameters ()
:precondition
(and
(not (RIGHTOF6-ROBOT))
)
:effect
(and
(Flag30prime-)
(not (Flag30-))
)

)
(:action Prim31Action-0
:parameters ()
:precondition
(and
(not (LEFTOF3-ROBOT))
)
:effect
(and
(Flag31prime-)
(not (Flag31-))
)

)
(:action Prim31Action-1
:parameters ()
:precondition
(and
(not (RIGHTOF5-ROBOT))
)
:effect
(and
(Flag31prime-)
(not (Flag31-))
)

)
(:action Prim32Action-0
:parameters ()
:precondition
(and
(not (LEFTOF3-ROBOT))
)
:effect
(and
(Flag32prime-)
(not (Flag32-))
)

)
(:action Prim32Action-1
:parameters ()
:precondition
(and
(not (RIGHTOF4-ROBOT))
)
:effect
(and
(Flag32prime-)
(not (Flag32-))
)

)
(:action Prim33Action-0
:parameters ()
:precondition
(and
(not (RIGHTOF3-ROBOT))
)
:effect
(and
(Flag33prime-)
(not (Flag33-))
)

)
(:action Prim33Action-1
:parameters ()
:precondition
(and
(not (LEFTOF3-ROBOT))
)
:effect
(and
(Flag33prime-)
(not (Flag33-))
)

)
(:action Prim34Action-0
:parameters ()
:precondition
(and
(not (LEFTOF3-ROBOT))
)
:effect
(and
(Flag34prime-)
(not (Flag34-))
)

)
(:action Flag36Action
:parameters ()
:precondition
(and
(LEFTOF3-ROBOT)
(RIGHTOF3-ROBOT)
)
:effect
(and
(Flag36-)
(Flag36prime-)
)

)
(:action Flag37Action
:parameters ()
:precondition
(and
(LEFTOF4-ROBOT)
(RIGHTOF6-ROBOT)
)
:effect
(and
(Flag37-)
(Flag37prime-)
)

)
(:action Flag38Action
:parameters ()
:precondition
(and
(LEFTOF4-ROBOT)
(RIGHTOF5-ROBOT)
)
:effect
(and
(Flag38-)
(Flag38prime-)
)

)
(:action Flag39Action
:parameters ()
:precondition
(and
(RIGHTOF3-ROBOT)
(LEFTOF4-ROBOT)
)
:effect
(and
(Flag39-)
(Flag39prime-)
)

)
(:action Flag40Action
:parameters ()
:precondition
(and
(LEFTOF4-ROBOT)
(RIGHTOF4-ROBOT)
)
:effect
(and
(Flag40-)
(Flag40prime-)
)

)
(:action Prim36Action-0
:parameters ()
:precondition
(and
(not (LEFTOF3-ROBOT))
)
:effect
(and
(Flag36prime-)
(not (Flag36-))
)

)
(:action Prim36Action-1
:parameters ()
:precondition
(and
(not (RIGHTOF3-ROBOT))
)
:effect
(and
(Flag36prime-)
(not (Flag36-))
)

)
(:action Prim37Action-0
:parameters ()
:precondition
(and
(not (LEFTOF4-ROBOT))
)
:effect
(and
(Flag37prime-)
(not (Flag37-))
)

)
(:action Prim37Action-1
:parameters ()
:precondition
(and
(not (RIGHTOF6-ROBOT))
)
:effect
(and
(Flag37prime-)
(not (Flag37-))
)

)
(:action Prim38Action-0
:parameters ()
:precondition
(and
(not (LEFTOF4-ROBOT))
)
:effect
(and
(Flag38prime-)
(not (Flag38-))
)

)
(:action Prim38Action-1
:parameters ()
:precondition
(and
(not (RIGHTOF5-ROBOT))
)
:effect
(and
(Flag38prime-)
(not (Flag38-))
)

)
(:action Prim39Action-0
:parameters ()
:precondition
(and
(not (RIGHTOF3-ROBOT))
)
:effect
(and
(Flag39prime-)
(not (Flag39-))
)

)
(:action Prim39Action-1
:parameters ()
:precondition
(and
(not (LEFTOF4-ROBOT))
)
:effect
(and
(Flag39prime-)
(not (Flag39-))
)

)
(:action Prim40Action-0
:parameters ()
:precondition
(and
(not (LEFTOF4-ROBOT))
)
:effect
(and
(Flag40prime-)
(not (Flag40-))
)

)
(:action Prim40Action-1
:parameters ()
:precondition
(and
(not (RIGHTOF4-ROBOT))
)
:effect
(and
(Flag40prime-)
(not (Flag40-))
)

)
(:action Flag42Action
:parameters ()
:precondition
(and
(RIGHTOF4-ROBOT)
(LEFTOF5-ROBOT)
)
:effect
(and
(Flag42-)
(Flag42prime-)
)

)
(:action Flag43Action
:parameters ()
:precondition
(and
(RIGHTOF5-ROBOT)
(LEFTOF5-ROBOT)
)
:effect
(and
(Flag43-)
(Flag43prime-)
)

)
(:action Flag44Action
:parameters ()
:precondition
(and
(LEFTOF5-ROBOT)
(RIGHTOF6-ROBOT)
)
:effect
(and
(Flag44-)
(Flag44prime-)
)

)
(:action Prim42Action-0
:parameters ()
:precondition
(and
(not (RIGHTOF4-ROBOT))
)
:effect
(and
(Flag42prime-)
(not (Flag42-))
)

)
(:action Prim42Action-1
:parameters ()
:precondition
(and
(not (LEFTOF5-ROBOT))
)
:effect
(and
(Flag42prime-)
(not (Flag42-))
)

)
(:action Prim43Action-0
:parameters ()
:precondition
(and
(not (RIGHTOF5-ROBOT))
)
:effect
(and
(Flag43prime-)
(not (Flag43-))
)

)
(:action Prim43Action-1
:parameters ()
:precondition
(and
(not (LEFTOF5-ROBOT))
)
:effect
(and
(Flag43prime-)
(not (Flag43-))
)

)
(:action Prim44Action-0
:parameters ()
:precondition
(and
(not (LEFTOF5-ROBOT))
)
:effect
(and
(Flag44prime-)
(not (Flag44-))
)

)
(:action Prim44Action-1
:parameters ()
:precondition
(and
(not (RIGHTOF6-ROBOT))
)
:effect
(and
(Flag44prime-)
(not (Flag44-))
)

)
(:action Flag46Action
:parameters ()
:precondition
(and
(RIGHTOF6-ROBOT)
(LEFTOF6-ROBOT)
)
:effect
(and
(Flag46-)
(Flag46prime-)
)

)
(:action Flag47Action
:parameters ()
:precondition
(and
(RIGHTOF5-ROBOT)
(LEFTOF6-ROBOT)
)
:effect
(and
(Flag47-)
(Flag47prime-)
)

)
(:action Prim46Action-0
:parameters ()
:precondition
(and
(not (RIGHTOF6-ROBOT))
)
:effect
(and
(Flag46prime-)
(not (Flag46-))
)

)
(:action Prim47Action-0
:parameters ()
:precondition
(and
(not (RIGHTOF5-ROBOT))
)
:effect
(and
(Flag47prime-)
(not (Flag47-))
)

)
(:action Flag49Action
:parameters ()
:precondition
(and
(LEFTOF6-ROBOT)
(RIGHTOF6-ROBOT)
)
:effect
(and
(Flag49-)
(Flag49prime-)
)

)
(:action Flag50Action
:parameters ()
:precondition
(and
(LEFTOF7-ROBOT)
(RIGHTOF6-ROBOT)
)
:effect
(and
(Flag50-)
(Flag50prime-)
)

)
(:action Prim49Action-1
:parameters ()
:precondition
(and
(not (RIGHTOF6-ROBOT))
)
:effect
(and
(Flag49prime-)
(not (Flag49-))
)

)
(:action Prim50Action-0
:parameters ()
:precondition
(and
(not (LEFTOF7-ROBOT))
)
:effect
(and
(Flag50prime-)
(not (Flag50-))
)

)
(:action Prim50Action-1
:parameters ()
:precondition
(and
(not (RIGHTOF6-ROBOT))
)
:effect
(and
(Flag50prime-)
(not (Flag50-))
)

)
(:action Flag68Action-0
:parameters ()
:precondition
(and
(Flag62-)
(Flag62prime-)
)
:effect
(and
(Flag68-)
(Flag68prime-)
)

)
(:action Flag68Action-1
:parameters ()
:precondition
(and
(Flag63-)
(Flag63prime-)
)
:effect
(and
(Flag68-)
(Flag68prime-)
)

)
(:action Flag68Action-2
:parameters ()
:precondition
(and
(Flag64-)
(Flag64prime-)
)
:effect
(and
(Flag68-)
(Flag68prime-)
)

)
(:action Flag68Action-3
:parameters ()
:precondition
(and
(Flag65-)
(Flag65prime-)
)
:effect
(and
(Flag68-)
(Flag68prime-)
)

)
(:action Flag68Action-4
:parameters ()
:precondition
(and
(Flag66-)
(Flag66prime-)
)
:effect
(and
(Flag68-)
(Flag68prime-)
)

)
(:action Flag68Action-6
:parameters ()
:precondition
(and
(Flag67-)
(Flag67prime-)
)
:effect
(and
(Flag68-)
(Flag68prime-)
)

)
(:action Prim68Action
:parameters ()
:precondition
(and
(not (Flag62-))
(Flag62prime-)
(not (Flag63-))
(Flag63prime-)
(not (Flag64-))
(Flag64prime-)
(not (Flag65-))
(Flag65prime-)
(not (Flag66-))
(Flag66prime-)
(not (BELOWOF1-ROBOT))
(not (Flag67-))
(Flag67prime-)
)
:effect
(and
(Flag68prime-)
(not (Flag68-))
)

)
(:action Flag75Action-0
:parameters ()
:precondition
(and
(Flag63-)
(Flag63prime-)
)
:effect
(and
(Flag75-)
(Flag75prime-)
)

)
(:action Flag75Action-1
:parameters ()
:precondition
(and
(Flag69-)
(Flag69prime-)
)
:effect
(and
(Flag75-)
(Flag75prime-)
)

)
(:action Flag75Action-2
:parameters ()
:precondition
(and
(Flag70-)
(Flag70prime-)
)
:effect
(and
(Flag75-)
(Flag75prime-)
)

)
(:action Flag75Action-3
:parameters ()
:precondition
(and
(Flag62-)
(Flag62prime-)
)
:effect
(and
(Flag75-)
(Flag75prime-)
)

)
(:action Flag75Action-4
:parameters ()
:precondition
(and
(Flag65-)
(Flag65prime-)
)
:effect
(and
(Flag75-)
(Flag75prime-)
)

)
(:action Flag75Action-5
:parameters ()
:precondition
(and
(Flag64-)
(Flag64prime-)
)
:effect
(and
(Flag75-)
(Flag75prime-)
)

)
(:action Flag75Action-6
:parameters ()
:precondition
(and
(Flag66-)
(Flag66prime-)
)
:effect
(and
(Flag75-)
(Flag75prime-)
)

)
(:action Flag75Action-7
:parameters ()
:precondition
(and
(Flag71-)
(Flag71prime-)
)
:effect
(and
(Flag75-)
(Flag75prime-)
)

)
(:action Flag75Action-8
:parameters ()
:precondition
(and
(Flag72-)
(Flag72prime-)
)
:effect
(and
(Flag75-)
(Flag75prime-)
)

)
(:action Flag75Action-9
:parameters ()
:precondition
(and
(Flag67-)
(Flag67prime-)
)
:effect
(and
(Flag75-)
(Flag75prime-)
)

)
(:action Flag75Action-10
:parameters ()
:precondition
(and
(Flag73-)
(Flag73prime-)
)
:effect
(and
(Flag75-)
(Flag75prime-)
)

)
(:action Flag75Action-11
:parameters ()
:precondition
(and
(Flag74-)
(Flag74prime-)
)
:effect
(and
(Flag75-)
(Flag75prime-)
)

)
(:action Prim75Action
:parameters ()
:precondition
(and
(not (Flag63-))
(Flag63prime-)
(not (Flag69-))
(Flag69prime-)
(not (Flag70-))
(Flag70prime-)
(not (Flag62-))
(Flag62prime-)
(not (Flag65-))
(Flag65prime-)
(not (Flag64-))
(Flag64prime-)
(not (Flag66-))
(Flag66prime-)
(not (Flag71-))
(Flag71prime-)
(not (Flag72-))
(Flag72prime-)
(not (Flag67-))
(Flag67prime-)
(not (Flag73-))
(Flag73prime-)
(not (Flag74-))
(Flag74prime-)
)
:effect
(and
(Flag75prime-)
(not (Flag75-))
)

)
(:action Flag82Action-0
:parameters ()
:precondition
(and
(Flag63-)
(Flag63prime-)
)
:effect
(and
(Flag82-)
(Flag82prime-)
)

)
(:action Flag82Action-1
:parameters ()
:precondition
(and
(Flag76-)
(Flag76prime-)
)
:effect
(and
(Flag82-)
(Flag82prime-)
)

)
(:action Flag82Action-2
:parameters ()
:precondition
(and
(Flag77-)
(Flag77prime-)
)
:effect
(and
(Flag82-)
(Flag82prime-)
)

)
(:action Flag82Action-3
:parameters ()
:precondition
(and
(Flag78-)
(Flag78prime-)
)
:effect
(and
(Flag82-)
(Flag82prime-)
)

)
(:action Flag82Action-4
:parameters ()
:precondition
(and
(Flag79-)
(Flag79prime-)
)
:effect
(and
(Flag82-)
(Flag82prime-)
)

)
(:action Flag82Action-5
:parameters ()
:precondition
(and
(Flag64-)
(Flag64prime-)
)
:effect
(and
(Flag82-)
(Flag82prime-)
)

)
(:action Flag82Action-6
:parameters ()
:precondition
(and
(Flag66-)
(Flag66prime-)
)
:effect
(and
(Flag82-)
(Flag82prime-)
)

)
(:action Flag82Action-7
:parameters ()
:precondition
(and
(Flag80-)
(Flag80prime-)
)
:effect
(and
(Flag82-)
(Flag82prime-)
)

)
(:action Flag82Action-8
:parameters ()
:precondition
(and
(Flag67-)
(Flag67prime-)
)
:effect
(and
(Flag82-)
(Flag82prime-)
)

)
(:action Flag82Action-9
:parameters ()
:precondition
(and
(Flag69-)
(Flag69prime-)
)
:effect
(and
(Flag82-)
(Flag82prime-)
)

)
(:action Flag82Action-10
:parameters ()
:precondition
(and
(Flag72-)
(Flag72prime-)
)
:effect
(and
(Flag82-)
(Flag82prime-)
)

)
(:action Flag82Action-11
:parameters ()
:precondition
(and
(Flag81-)
(Flag81prime-)
)
:effect
(and
(Flag82-)
(Flag82prime-)
)

)
(:action Flag82Action-12
:parameters ()
:precondition
(and
(Flag71-)
(Flag71prime-)
)
:effect
(and
(Flag82-)
(Flag82prime-)
)

)
(:action Flag82Action-13
:parameters ()
:precondition
(and
(Flag73-)
(Flag73prime-)
)
:effect
(and
(Flag82-)
(Flag82prime-)
)

)
(:action Flag82Action-14
:parameters ()
:precondition
(and
(Flag62-)
(Flag62prime-)
)
:effect
(and
(Flag82-)
(Flag82prime-)
)

)
(:action Prim82Action
:parameters ()
:precondition
(and
(not (Flag63-))
(Flag63prime-)
(not (Flag76-))
(Flag76prime-)
(not (Flag77-))
(Flag77prime-)
(not (Flag78-))
(Flag78prime-)
(not (Flag79-))
(Flag79prime-)
(not (Flag64-))
(Flag64prime-)
(not (Flag66-))
(Flag66prime-)
(not (Flag80-))
(Flag80prime-)
(not (Flag67-))
(Flag67prime-)
(not (Flag69-))
(Flag69prime-)
(not (Flag72-))
(Flag72prime-)
(not (Flag81-))
(Flag81prime-)
(not (Flag71-))
(Flag71prime-)
(not (Flag73-))
(Flag73prime-)
(not (Flag62-))
(Flag62prime-)
)
:effect
(and
(Flag82prime-)
(not (Flag82-))
)

)
(:action Flag88Action-0
:parameters ()
:precondition
(and
(Flag67-)
(Flag67prime-)
)
:effect
(and
(Flag88-)
(Flag88prime-)
)

)
(:action Flag88Action-1
:parameters ()
:precondition
(and
(Flag77-)
(Flag77prime-)
)
:effect
(and
(Flag88-)
(Flag88prime-)
)

)
(:action Flag88Action-2
:parameters ()
:precondition
(and
(Flag62-)
(Flag62prime-)
)
:effect
(and
(Flag88-)
(Flag88prime-)
)

)
(:action Flag88Action-3
:parameters ()
:precondition
(and
(Flag66-)
(Flag66prime-)
)
:effect
(and
(Flag88-)
(Flag88prime-)
)

)
(:action Flag88Action-4
:parameters ()
:precondition
(and
(Flag64-)
(Flag64prime-)
)
:effect
(and
(Flag88-)
(Flag88prime-)
)

)
(:action Flag88Action-5
:parameters ()
:precondition
(and
(Flag83-)
(Flag83prime-)
)
:effect
(and
(Flag88-)
(Flag88prime-)
)

)
(:action Flag88Action-6
:parameters ()
:precondition
(and
(Flag84-)
(Flag84prime-)
)
:effect
(and
(Flag88-)
(Flag88prime-)
)

)
(:action Flag88Action-7
:parameters ()
:precondition
(and
(Flag80-)
(Flag80prime-)
)
:effect
(and
(Flag88-)
(Flag88prime-)
)

)
(:action Flag88Action-8
:parameters ()
:precondition
(and
(Flag85-)
(Flag85prime-)
)
:effect
(and
(Flag88-)
(Flag88prime-)
)

)
(:action Flag88Action-9
:parameters ()
:precondition
(and
(Flag86-)
(Flag86prime-)
)
:effect
(and
(Flag88-)
(Flag88prime-)
)

)
(:action Flag88Action-10
:parameters ()
:precondition
(and
(Flag72-)
(Flag72prime-)
)
:effect
(and
(Flag88-)
(Flag88prime-)
)

)
(:action Flag88Action-11
:parameters ()
:precondition
(and
(Flag71-)
(Flag71prime-)
)
:effect
(and
(Flag88-)
(Flag88prime-)
)

)
(:action Flag88Action-12
:parameters ()
:precondition
(and
(Flag79-)
(Flag79prime-)
)
:effect
(and
(Flag88-)
(Flag88prime-)
)

)
(:action Flag88Action-13
:parameters ()
:precondition
(and
(Flag69-)
(Flag69prime-)
)
:effect
(and
(Flag88-)
(Flag88prime-)
)

)
(:action Flag88Action-14
:parameters ()
:precondition
(and
(Flag73-)
(Flag73prime-)
)
:effect
(and
(Flag88-)
(Flag88prime-)
)

)
(:action Flag88Action-15
:parameters ()
:precondition
(and
(Flag87-)
(Flag87prime-)
)
:effect
(and
(Flag88-)
(Flag88prime-)
)

)
(:action Prim88Action
:parameters ()
:precondition
(and
(not (Flag67-))
(Flag67prime-)
(not (Flag77-))
(Flag77prime-)
(not (Flag62-))
(Flag62prime-)
(not (Flag66-))
(Flag66prime-)
(not (Flag64-))
(Flag64prime-)
(not (Flag83-))
(Flag83prime-)
(not (Flag84-))
(Flag84prime-)
(not (Flag80-))
(Flag80prime-)
(not (Flag85-))
(Flag85prime-)
(not (Flag86-))
(Flag86prime-)
(not (Flag72-))
(Flag72prime-)
(not (Flag71-))
(Flag71prime-)
(not (Flag79-))
(Flag79prime-)
(not (Flag69-))
(Flag69prime-)
(not (Flag73-))
(Flag73prime-)
(not (Flag87-))
(Flag87prime-)
)
:effect
(and
(Flag88prime-)
(not (Flag88-))
)

)
(:action Flag92Action-0
:parameters ()
:precondition
(and
(Flag62-)
(Flag62prime-)
)
:effect
(and
(Flag92-)
(Flag92prime-)
)

)
(:action Flag92Action-1
:parameters ()
:precondition
(and
(Flag77-)
(Flag77prime-)
)
:effect
(and
(Flag92-)
(Flag92prime-)
)

)
(:action Flag92Action-2
:parameters ()
:precondition
(and
(Flag89-)
(Flag89prime-)
)
:effect
(and
(Flag92-)
(Flag92prime-)
)

)
(:action Flag92Action-3
:parameters ()
:precondition
(and
(Flag79-)
(Flag79prime-)
)
:effect
(and
(Flag92-)
(Flag92prime-)
)

)
(:action Flag92Action-4
:parameters ()
:precondition
(and
(Flag90-)
(Flag90prime-)
)
:effect
(and
(Flag92-)
(Flag92prime-)
)

)
(:action Flag92Action-5
:parameters ()
:precondition
(and
(Flag72-)
(Flag72prime-)
)
:effect
(and
(Flag92-)
(Flag92prime-)
)

)
(:action Flag92Action-6
:parameters ()
:precondition
(and
(Flag64-)
(Flag64prime-)
)
:effect
(and
(Flag92-)
(Flag92prime-)
)

)
(:action Flag92Action-7
:parameters ()
:precondition
(and
(Flag66-)
(Flag66prime-)
)
:effect
(and
(Flag92-)
(Flag92prime-)
)

)
(:action Flag92Action-8
:parameters ()
:precondition
(and
(Flag80-)
(Flag80prime-)
)
:effect
(and
(Flag92-)
(Flag92prime-)
)

)
(:action Flag92Action-9
:parameters ()
:precondition
(and
(Flag85-)
(Flag85prime-)
)
:effect
(and
(Flag92-)
(Flag92prime-)
)

)
(:action Flag92Action-10
:parameters ()
:precondition
(and
(Flag86-)
(Flag86prime-)
)
:effect
(and
(Flag92-)
(Flag92prime-)
)

)
(:action Flag92Action-11
:parameters ()
:precondition
(and
(Flag91-)
(Flag91prime-)
)
:effect
(and
(Flag92-)
(Flag92prime-)
)

)
(:action Flag92Action-12
:parameters ()
:precondition
(and
(Flag71-)
(Flag71prime-)
)
:effect
(and
(Flag92-)
(Flag92prime-)
)

)
(:action Flag92Action-13
:parameters ()
:precondition
(and
(Flag69-)
(Flag69prime-)
)
:effect
(and
(Flag92-)
(Flag92prime-)
)

)
(:action Flag92Action-14
:parameters ()
:precondition
(and
(Flag84-)
(Flag84prime-)
)
:effect
(and
(Flag92-)
(Flag92prime-)
)

)
(:action Prim92Action
:parameters ()
:precondition
(and
(not (Flag62-))
(Flag62prime-)
(not (Flag77-))
(Flag77prime-)
(not (Flag89-))
(Flag89prime-)
(not (Flag79-))
(Flag79prime-)
(not (Flag90-))
(Flag90prime-)
(not (Flag72-))
(Flag72prime-)
(not (Flag64-))
(Flag64prime-)
(not (Flag66-))
(Flag66prime-)
(not (Flag80-))
(Flag80prime-)
(not (Flag85-))
(Flag85prime-)
(not (Flag86-))
(Flag86prime-)
(not (Flag91-))
(Flag91prime-)
(not (Flag71-))
(Flag71prime-)
(not (Flag69-))
(Flag69prime-)
(not (Flag84-))
(Flag84prime-)
)
:effect
(and
(Flag92prime-)
(not (Flag92-))
)

)
(:action Flag96Action-0
:parameters ()
:precondition
(and
(Flag77-)
(Flag77prime-)
)
:effect
(and
(Flag96-)
(Flag96prime-)
)

)
(:action Flag96Action-1
:parameters ()
:precondition
(and
(Flag79-)
(Flag79prime-)
)
:effect
(and
(Flag96-)
(Flag96prime-)
)

)
(:action Flag96Action-2
:parameters ()
:precondition
(and
(Flag64-)
(Flag64prime-)
)
:effect
(and
(Flag96-)
(Flag96prime-)
)

)
(:action Flag96Action-3
:parameters ()
:precondition
(and
(Flag72-)
(Flag72prime-)
)
:effect
(and
(Flag96-)
(Flag96prime-)
)

)
(:action Flag96Action-4
:parameters ()
:precondition
(and
(Flag93-)
(Flag93prime-)
)
:effect
(and
(Flag96-)
(Flag96prime-)
)

)
(:action Flag96Action-5
:parameters ()
:precondition
(and
(Flag66-)
(Flag66prime-)
)
:effect
(and
(Flag96-)
(Flag96prime-)
)

)
(:action Flag96Action-6
:parameters ()
:precondition
(and
(Flag94-)
(Flag94prime-)
)
:effect
(and
(Flag96-)
(Flag96prime-)
)

)
(:action Flag96Action-7
:parameters ()
:precondition
(and
(Flag95-)
(Flag95prime-)
)
:effect
(and
(Flag96-)
(Flag96prime-)
)

)
(:action Flag96Action-8
:parameters ()
:precondition
(and
(Flag91-)
(Flag91prime-)
)
:effect
(and
(Flag96-)
(Flag96prime-)
)

)
(:action Flag96Action-9
:parameters ()
:precondition
(and
(Flag85-)
(Flag85prime-)
)
:effect
(and
(Flag96-)
(Flag96prime-)
)

)
(:action Flag96Action-10
:parameters ()
:precondition
(and
(Flag69-)
(Flag69prime-)
)
:effect
(and
(Flag96-)
(Flag96prime-)
)

)
(:action Flag96Action-11
:parameters ()
:precondition
(and
(Flag84-)
(Flag84prime-)
)
:effect
(and
(Flag96-)
(Flag96prime-)
)

)
(:action Prim96Action
:parameters ()
:precondition
(and
(not (Flag77-))
(Flag77prime-)
(not (Flag79-))
(Flag79prime-)
(not (Flag64-))
(Flag64prime-)
(not (Flag72-))
(Flag72prime-)
(not (Flag93-))
(Flag93prime-)
(not (Flag66-))
(Flag66prime-)
(not (Flag94-))
(Flag94prime-)
(not (Flag95-))
(Flag95prime-)
(not (Flag91-))
(Flag91prime-)
(not (Flag85-))
(Flag85prime-)
(not (Flag69-))
(Flag69prime-)
(not (Flag84-))
(Flag84prime-)
)
:effect
(and
(Flag96prime-)
(not (Flag96-))
)

)
(:action Flag97Action-0
:parameters ()
:precondition
(and
(RIGHTOF3-ROBOT)
)
:effect
(and
(Flag97-)
(Flag97prime-)
)

)
(:action Flag97Action-1
:parameters ()
:precondition
(and
(RIGHTOF5-ROBOT)
)
:effect
(and
(Flag97-)
(Flag97prime-)
)

)
(:action Flag97Action-3
:parameters ()
:precondition
(and
(RIGHTOF0-ROBOT)
)
:effect
(and
(Flag97-)
(Flag97prime-)
)

)
(:action Flag97Action-4
:parameters ()
:precondition
(and
(RIGHTOF4-ROBOT)
)
:effect
(and
(Flag97-)
(Flag97prime-)
)

)
(:action Flag97Action-5
:parameters ()
:precondition
(and
(RIGHTOF1-ROBOT)
)
:effect
(and
(Flag97-)
(Flag97prime-)
)

)
(:action Flag97Action-6
:parameters ()
:precondition
(and
(RIGHTOF6-ROBOT)
)
:effect
(and
(Flag97-)
(Flag97prime-)
)

)
(:action Prim97Action
:parameters ()
:precondition
(and
(not (RIGHTOF3-ROBOT))
(not (RIGHTOF5-ROBOT))
(not (RIGHTOF2-ROBOT))
(not (RIGHTOF0-ROBOT))
(not (RIGHTOF4-ROBOT))
(not (RIGHTOF1-ROBOT))
(not (RIGHTOF6-ROBOT))
)
:effect
(and
(Flag97prime-)
(not (Flag97-))
)

)
(:action Flag98Action
:parameters ()
:precondition
(and
(RIGHTOF0-ROBOT)
(LEFTOF1-ROBOT)
)
:effect
(and
(Flag98-)
(Flag98prime-)
)

)
(:action Prim98Action-0
:parameters ()
:precondition
(and
(not (RIGHTOF0-ROBOT))
)
:effect
(and
(Flag98prime-)
(not (Flag98-))
)

)
(:action Prim98Action-1
:parameters ()
:precondition
(and
(not (LEFTOF1-ROBOT))
)
:effect
(and
(Flag98prime-)
(not (Flag98-))
)

)
(:action Flag103Action-0
:parameters ()
:precondition
(and
(Flag69-)
(Flag69prime-)
)
:effect
(and
(Flag103-)
(Flag103prime-)
)

)
(:action Flag103Action-1
:parameters ()
:precondition
(and
(Flag101-)
(Flag101prime-)
)
:effect
(and
(Flag103-)
(Flag103prime-)
)

)
(:action Flag103Action-2
:parameters ()
:precondition
(and
(Flag64-)
(Flag64prime-)
)
:effect
(and
(Flag103-)
(Flag103prime-)
)

)
(:action Flag103Action-3
:parameters ()
:precondition
(and
(Flag77-)
(Flag77prime-)
)
:effect
(and
(Flag103-)
(Flag103prime-)
)

)
(:action Flag103Action-4
:parameters ()
:precondition
(and
(Flag91-)
(Flag91prime-)
)
:effect
(and
(Flag103-)
(Flag103prime-)
)

)
(:action Flag103Action-5
:parameters ()
:precondition
(and
(Flag85-)
(Flag85prime-)
)
:effect
(and
(Flag103-)
(Flag103prime-)
)

)
(:action Flag103Action-6
:parameters ()
:precondition
(and
(Flag102-)
(Flag102prime-)
)
:effect
(and
(Flag103-)
(Flag103prime-)
)

)
(:action Prim103Action
:parameters ()
:precondition
(and
(not (Flag69-))
(Flag69prime-)
(not (Flag101-))
(Flag101prime-)
(not (Flag64-))
(Flag64prime-)
(not (Flag77-))
(Flag77prime-)
(not (Flag91-))
(Flag91prime-)
(not (Flag85-))
(Flag85prime-)
(not (Flag102-))
(Flag102prime-)
)
:effect
(and
(Flag103prime-)
(not (Flag103-))
)

)
(:action MOVERIGHT-ROBOT
:parameters ()
:precondition
(and
(Flag4-)
(Flag4prime-)
(Flag48prime-)
(Flag45prime-)
(Flag41prime-)
(Flag35prime-)
(Flag29prime-)
(Flag99prime-)
(Flag97prime-)
)
:effect
(and
(when
(and
(Flag48-)
)
(and
(COLUMN6-ROBOT)
(not (NOT-COLUMN6-ROBOT))
)
)
(when
(and
(Flag45-)
)
(and
(COLUMN5-ROBOT)
(not (NOT-COLUMN5-ROBOT))
)
)
(when
(and
(Flag41-)
)
(and
(COLUMN4-ROBOT)
(not (NOT-COLUMN4-ROBOT))
)
)
(when
(and
(Flag35-)
)
(and
(COLUMN3-ROBOT)
(not (NOT-COLUMN3-ROBOT))
)
)
(when
(and
(Flag29-)
)
(and
(COLUMN2-ROBOT)
(not (NOT-COLUMN2-ROBOT))
)
)
(when
(and
(Flag99-)
)
(and
(COLUMN1-ROBOT)
(not (NOT-COLUMN1-ROBOT))
)
)
(when
(and
(COLUMN5-ROBOT)
)
(and
(COLUMN6-ROBOT)
(NOT-COLUMN5-ROBOT)
(not (NOT-COLUMN6-ROBOT))
(not (COLUMN5-ROBOT))
)
)
(when
(and
(COLUMN4-ROBOT)
)
(and
(COLUMN5-ROBOT)
(NOT-COLUMN4-ROBOT)
(not (NOT-COLUMN5-ROBOT))
(not (COLUMN4-ROBOT))
)
)
(when
(and
(COLUMN3-ROBOT)
)
(and
(COLUMN4-ROBOT)
(NOT-COLUMN3-ROBOT)
(not (NOT-COLUMN4-ROBOT))
(not (COLUMN3-ROBOT))
)
)
(when
(and
(COLUMN2-ROBOT)
)
(and
(COLUMN3-ROBOT)
(NOT-COLUMN2-ROBOT)
(not (NOT-COLUMN3-ROBOT))
(not (COLUMN2-ROBOT))
)
)
(when
(and
(COLUMN1-ROBOT)
)
(and
(COLUMN2-ROBOT)
(NOT-COLUMN1-ROBOT)
(not (NOT-COLUMN2-ROBOT))
(not (COLUMN1-ROBOT))
)
)
(when
(and
(COLUMN0-ROBOT)
)
(and
(COLUMN1-ROBOT)
(NOT-COLUMN0-ROBOT)
(not (NOT-COLUMN1-ROBOT))
(not (COLUMN0-ROBOT))
)
)
(when
(and
(LEFTOF4-ROBOT)
)
(and
(LEFTOF7-ROBOT)
(NOT-LEFTOF6-ROBOT)
(LEFTOF6-ROBOT)
(NOT-LEFTOF5-ROBOT)
(LEFTOF5-ROBOT)
(NOT-LEFTOF4-ROBOT)
(not (NOT-LEFTOF7-ROBOT))
(not (LEFTOF4-ROBOT))
)
)
(when
(and
(LEFTOF3-ROBOT)
)
(and
(LEFTOF7-ROBOT)
(NOT-LEFTOF6-ROBOT)
(LEFTOF6-ROBOT)
(NOT-LEFTOF5-ROBOT)
(LEFTOF5-ROBOT)
(NOT-LEFTOF4-ROBOT)
(LEFTOF4-ROBOT)
(NOT-LEFTOF3-ROBOT)
(not (NOT-LEFTOF7-ROBOT))
(not (LEFTOF3-ROBOT))
)
)
(when
(and
(LEFTOF5-ROBOT)
)
(and
(LEFTOF7-ROBOT)
(NOT-LEFTOF6-ROBOT)
(LEFTOF6-ROBOT)
(NOT-LEFTOF5-ROBOT)
(not (NOT-LEFTOF7-ROBOT))
(not (LEFTOF5-ROBOT))
)
)
(when
(and
(LEFTOF6-ROBOT)
)
(and
(LEFTOF7-ROBOT)
(NOT-LEFTOF6-ROBOT)
(not (NOT-LEFTOF7-ROBOT))
(not (LEFTOF6-ROBOT))
)
)
(when
(and
(LEFTOF1-ROBOT)
)
(and
(LEFTOF7-ROBOT)
(NOT-LEFTOF6-ROBOT)
(LEFTOF6-ROBOT)
(NOT-LEFTOF5-ROBOT)
(LEFTOF5-ROBOT)
(NOT-LEFTOF4-ROBOT)
(LEFTOF4-ROBOT)
(NOT-LEFTOF3-ROBOT)
(LEFTOF3-ROBOT)
(NOT-LEFTOF2-ROBOT)
(LEFTOF2-ROBOT)
(NOT-LEFTOF1-ROBOT)
(not (NOT-LEFTOF7-ROBOT))
(not (LEFTOF1-ROBOT))
)
)
(when
(and
(LEFTOF2-ROBOT)
)
(and
(LEFTOF7-ROBOT)
(NOT-LEFTOF6-ROBOT)
(LEFTOF6-ROBOT)
(NOT-LEFTOF5-ROBOT)
(LEFTOF5-ROBOT)
(NOT-LEFTOF4-ROBOT)
(LEFTOF4-ROBOT)
(NOT-LEFTOF3-ROBOT)
(LEFTOF3-ROBOT)
(NOT-LEFTOF2-ROBOT)
(not (NOT-LEFTOF7-ROBOT))
(not (LEFTOF2-ROBOT))
)
)
(when
(and
(RIGHTOF6-ROBOT)
)
(and
(RIGHTOF5-ROBOT)
(RIGHTOF4-ROBOT)
(RIGHTOF3-ROBOT)
(RIGHTOF2-ROBOT)
(not (NOT-RIGHTOF6-ROBOT))
(not (NOT-RIGHTOF5-ROBOT))
(not (NOT-RIGHTOF4-ROBOT))
(not (NOT-RIGHTOF3-ROBOT))
(not (NOT-RIGHTOF2-ROBOT))
)
)
(when
(and
(RIGHTOF5-ROBOT)
)
(and
(RIGHTOF6-ROBOT)
(RIGHTOF4-ROBOT)
(RIGHTOF3-ROBOT)
(RIGHTOF2-ROBOT)
(not (NOT-RIGHTOF6-ROBOT))
(not (NOT-RIGHTOF5-ROBOT))
(not (NOT-RIGHTOF4-ROBOT))
(not (NOT-RIGHTOF3-ROBOT))
(not (NOT-RIGHTOF2-ROBOT))
)
)
(when
(and
(RIGHTOF4-ROBOT)
)
(and
(RIGHTOF5-ROBOT)
(RIGHTOF3-ROBOT)
(RIGHTOF2-ROBOT)
(not (NOT-RIGHTOF5-ROBOT))
(not (NOT-RIGHTOF4-ROBOT))
(not (NOT-RIGHTOF3-ROBOT))
(not (NOT-RIGHTOF2-ROBOT))
)
)
(when
(and
(RIGHTOF3-ROBOT)
)
(and
(RIGHTOF4-ROBOT)
(RIGHTOF2-ROBOT)
(not (NOT-RIGHTOF4-ROBOT))
(not (NOT-RIGHTOF3-ROBOT))
(not (NOT-RIGHTOF2-ROBOT))
)
)
(when
(and
(RIGHTOF2-ROBOT)
)
(and
(RIGHTOF3-ROBOT)
(not (NOT-RIGHTOF3-ROBOT))
(not (NOT-RIGHTOF2-ROBOT))
)
)
(when
(and
(RIGHTOF1-ROBOT)
)
(and
(RIGHTOF2-ROBOT)
(not (NOT-RIGHTOF2-ROBOT))
)
)
(when
(and
(Flag97-)
)
(and
(RIGHTOF1-ROBOT)
(not (NOT-RIGHTOF1-ROBOT))
)
)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag100prime-))
(not (Flag97prime-))
(not (Flag61prime-))
(not (Flag12prime-))
(not (Flag11prime-))
(not (Flag10prime-))
(not (Flag9prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag102prime-))
(not (Flag101prime-))
(not (Flag95prime-))
(not (Flag94prime-))
(not (Flag93prime-))
(not (Flag91prime-))
(not (Flag90prime-))
(not (Flag89prime-))
(not (Flag87prime-))
(not (Flag86prime-))
(not (Flag85prime-))
(not (Flag84prime-))
(not (Flag83prime-))
(not (Flag81prime-))
(not (Flag80prime-))
(not (Flag79prime-))
(not (Flag78prime-))
(not (Flag77prime-))
(not (Flag76prime-))
(not (Flag74prime-))
(not (Flag73prime-))
(not (Flag72prime-))
(not (Flag71prime-))
(not (Flag70prime-))
(not (Flag69prime-))
(not (Flag68prime-))
(not (Flag67prime-))
(not (Flag66prime-))
(not (Flag65prime-))
(not (Flag64prime-))
(not (Flag63prime-))
(not (Flag62prime-))
(not (Flag60prime-))
(not (Flag59prime-))
(not (Flag58prime-))
(not (Flag57prime-))
(not (Flag56prime-))
(not (Flag55prime-))
(not (Flag54prime-))
(not (Flag53prime-))
(not (Flag52prime-))
(not (Flag4prime-))
(not (Flag103prime-))
(not (Flag98prime-))
(not (Flag96prime-))
(not (Flag92prime-))
(not (Flag88prime-))
(not (Flag82prime-))
(not (Flag75prime-))
(not (Flag50prime-))
(not (Flag49prime-))
(not (Flag47prime-))
(not (Flag46prime-))
(not (Flag44prime-))
(not (Flag43prime-))
(not (Flag42prime-))
(not (Flag40prime-))
(not (Flag39prime-))
(not (Flag38prime-))
(not (Flag37prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag33prime-))
(not (Flag32prime-))
(not (Flag31prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag27prime-))
(not (Flag26prime-))
(not (Flag25prime-))
(not (Flag24prime-))
(not (Flag23prime-))
(not (Flag22prime-))
(not (Flag21prime-))
(not (Flag20prime-))
(not (Flag19prime-))
(not (Flag18prime-))
(not (Flag17prime-))
(not (Flag16prime-))
(not (Flag15prime-))
(not (Flag14prime-))
(not (Flag13prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag1prime-))
(not (Flag99prime-))
(not (Flag51prime-))
(not (Flag48prime-))
(not (Flag45prime-))
(not (Flag41prime-))
(not (Flag35prime-))
(not (Flag29prime-))
)
)
(:action MOVELEFT-ROBOT
:parameters ()
:precondition
(and
(Flag4-)
(Flag4prime-)
(Flag51prime-)
(Flag48prime-)
(Flag45prime-)
(Flag41prime-)
(Flag35prime-)
(Flag29prime-)
(Flag15prime-)
(Flag14prime-)
(Flag13prime-)
(Flag12prime-)
(Flag11prime-)
(Flag10prime-)
(Flag9prime-)
(Flag8prime-)
(Flag7prime-)
(Flag6prime-)
(Flag5prime-)
)
:effect
(and
(when
(and
(Flag51-)
)
(and
(COLUMN5-ROBOT)
(not (NOT-COLUMN5-ROBOT))
)
)
(when
(and
(Flag48-)
)
(and
(COLUMN4-ROBOT)
(not (NOT-COLUMN4-ROBOT))
)
)
(when
(and
(Flag45-)
)
(and
(COLUMN3-ROBOT)
(not (NOT-COLUMN3-ROBOT))
)
)
(when
(and
(Flag41-)
)
(and
(COLUMN2-ROBOT)
(not (NOT-COLUMN2-ROBOT))
)
)
(when
(and
(Flag35-)
)
(and
(COLUMN1-ROBOT)
(not (NOT-COLUMN1-ROBOT))
)
)
(when
(and
(Flag29-)
)
(and
(COLUMN0-ROBOT)
(not (NOT-COLUMN0-ROBOT))
)
)
(when
(and
(COLUMN6-ROBOT)
)
(and
(COLUMN5-ROBOT)
(NOT-COLUMN6-ROBOT)
(not (NOT-COLUMN5-ROBOT))
(not (COLUMN6-ROBOT))
)
)
(when
(and
(COLUMN5-ROBOT)
)
(and
(COLUMN4-ROBOT)
(NOT-COLUMN5-ROBOT)
(not (NOT-COLUMN4-ROBOT))
(not (COLUMN5-ROBOT))
)
)
(when
(and
(COLUMN4-ROBOT)
)
(and
(COLUMN3-ROBOT)
(NOT-COLUMN4-ROBOT)
(not (NOT-COLUMN3-ROBOT))
(not (COLUMN4-ROBOT))
)
)
(when
(and
(COLUMN3-ROBOT)
)
(and
(COLUMN2-ROBOT)
(NOT-COLUMN3-ROBOT)
(not (NOT-COLUMN2-ROBOT))
(not (COLUMN3-ROBOT))
)
)
(when
(and
(COLUMN2-ROBOT)
)
(and
(COLUMN1-ROBOT)
(NOT-COLUMN2-ROBOT)
(not (NOT-COLUMN1-ROBOT))
(not (COLUMN2-ROBOT))
)
)
(when
(and
(COLUMN1-ROBOT)
)
(and
(COLUMN0-ROBOT)
(NOT-COLUMN1-ROBOT)
(not (NOT-COLUMN0-ROBOT))
(not (COLUMN1-ROBOT))
)
)
(when
(and
(RIGHTOF6-ROBOT)
)
(and
(RIGHTOF5-ROBOT)
(NOT-RIGHTOF6-ROBOT)
(not (NOT-RIGHTOF5-ROBOT))
(not (RIGHTOF6-ROBOT))
)
)
(when
(and
(Flag15-)
)
(and
(RIGHTOF4-ROBOT)
(NOT-RIGHTOF5-ROBOT)
(not (NOT-RIGHTOF4-ROBOT))
(not (RIGHTOF5-ROBOT))
)
)
(when
(and
(Flag14-)
)
(and
(RIGHTOF3-ROBOT)
(NOT-RIGHTOF4-ROBOT)
(not (NOT-RIGHTOF3-ROBOT))
(not (RIGHTOF4-ROBOT))
)
)
(when
(and
(Flag13-)
)
(and
(RIGHTOF2-ROBOT)
(NOT-RIGHTOF3-ROBOT)
(not (NOT-RIGHTOF2-ROBOT))
(not (RIGHTOF3-ROBOT))
)
)
(when
(and
(Flag12-)
)
(and
(RIGHTOF1-ROBOT)
(NOT-RIGHTOF2-ROBOT)
(not (NOT-RIGHTOF1-ROBOT))
(not (RIGHTOF2-ROBOT))
)
)
(when
(and
(Flag11-)
)
(and
(RIGHTOF0-ROBOT)
(NOT-RIGHTOF1-ROBOT)
(not (NOT-RIGHTOF0-ROBOT))
(not (RIGHTOF1-ROBOT))
)
)
(when
(and
(Flag10-)
)
(and
(LEFTOF6-ROBOT)
(not (NOT-LEFTOF6-ROBOT))
)
)
(when
(and
(Flag9-)
)
(and
(LEFTOF5-ROBOT)
(not (NOT-LEFTOF5-ROBOT))
)
)
(when
(and
(Flag8-)
)
(and
(LEFTOF4-ROBOT)
(not (NOT-LEFTOF4-ROBOT))
)
)
(when
(and
(Flag7-)
)
(and
(LEFTOF3-ROBOT)
(not (NOT-LEFTOF3-ROBOT))
)
)
(when
(and
(Flag6-)
)
(and
(LEFTOF2-ROBOT)
(not (NOT-LEFTOF2-ROBOT))
)
)
(when
(and
(Flag5-)
)
(and
(LEFTOF1-ROBOT)
(not (NOT-LEFTOF1-ROBOT))
)
)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag100prime-))
(not (Flag97prime-))
(not (Flag61prime-))
(not (Flag12prime-))
(not (Flag11prime-))
(not (Flag10prime-))
(not (Flag9prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag102prime-))
(not (Flag101prime-))
(not (Flag95prime-))
(not (Flag94prime-))
(not (Flag93prime-))
(not (Flag91prime-))
(not (Flag90prime-))
(not (Flag89prime-))
(not (Flag87prime-))
(not (Flag86prime-))
(not (Flag85prime-))
(not (Flag84prime-))
(not (Flag83prime-))
(not (Flag81prime-))
(not (Flag80prime-))
(not (Flag79prime-))
(not (Flag78prime-))
(not (Flag77prime-))
(not (Flag76prime-))
(not (Flag74prime-))
(not (Flag73prime-))
(not (Flag72prime-))
(not (Flag71prime-))
(not (Flag70prime-))
(not (Flag69prime-))
(not (Flag68prime-))
(not (Flag67prime-))
(not (Flag66prime-))
(not (Flag65prime-))
(not (Flag64prime-))
(not (Flag63prime-))
(not (Flag62prime-))
(not (Flag60prime-))
(not (Flag59prime-))
(not (Flag58prime-))
(not (Flag57prime-))
(not (Flag56prime-))
(not (Flag55prime-))
(not (Flag54prime-))
(not (Flag53prime-))
(not (Flag52prime-))
(not (Flag4prime-))
(not (Flag103prime-))
(not (Flag98prime-))
(not (Flag96prime-))
(not (Flag92prime-))
(not (Flag88prime-))
(not (Flag82prime-))
(not (Flag75prime-))
(not (Flag50prime-))
(not (Flag49prime-))
(not (Flag47prime-))
(not (Flag46prime-))
(not (Flag44prime-))
(not (Flag43prime-))
(not (Flag42prime-))
(not (Flag40prime-))
(not (Flag39prime-))
(not (Flag38prime-))
(not (Flag37prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag33prime-))
(not (Flag32prime-))
(not (Flag31prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag27prime-))
(not (Flag26prime-))
(not (Flag25prime-))
(not (Flag24prime-))
(not (Flag23prime-))
(not (Flag22prime-))
(not (Flag21prime-))
(not (Flag20prime-))
(not (Flag19prime-))
(not (Flag18prime-))
(not (Flag17prime-))
(not (Flag16prime-))
(not (Flag15prime-))
(not (Flag14prime-))
(not (Flag13prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag1prime-))
(not (Flag99prime-))
(not (Flag51prime-))
(not (Flag48prime-))
(not (Flag45prime-))
(not (Flag41prime-))
(not (Flag35prime-))
(not (Flag29prime-))
)
)
(:action Prim1Action-1
:parameters ()
:precondition
(and
(not (ROW1-ROBOT))
)
:effect
(and
(Flag1prime-)
(not (Flag1-))
)

)
(:action CHECKCONSISTENCYACTION
:parameters ()
:precondition
(and
(NOT-ERROR-)
(CHECKCONSISTENCY-)
)
:effect
(and
(when
(and
(BELOWOF2-ROBOT)
(ABOVEOF5-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF3-ROBOT)
(ABOVEOF4-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF6-ROBOT)
(LEFTOF2-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF1-ROBOT)
(ABOVEOF3-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF2-ROBOT)
(RIGHTOF5-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF6-ROBOT)
(BELOWOF1-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF3-ROBOT)
(ABOVEOF5-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF6-ROBOT)
(BELOWOF3-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF2-ROBOT)
(BELOWOF2-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF6-ROBOT)
(BELOWOF2-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF2-ROBOT)
(LEFTOF2-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF6-ROBOT)
(LEFTOF1-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF5-ROBOT)
(BELOWOF1-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF5-ROBOT)
(LEFTOF1-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF3-ROBOT)
(LEFTOF1-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF4-ROBOT)
(RIGHTOF5-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF4-ROBOT)
(LEFTOF2-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF3-ROBOT)
(LEFTOF3-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF1-ROBOT)
(ABOVEOF4-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF6-ROBOT)
(BELOWOF4-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF1-ROBOT)
(RIGHTOF4-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF6-ROBOT)
(LEFTOF6-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF1-ROBOT)
(RIGHTOF1-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF3-ROBOT)
(RIGHTOF4-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF4-ROBOT)
(RIGHTOF6-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF2-ROBOT)
(RIGHTOF3-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF2-ROBOT)
(BELOWOF1-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF4-ROBOT)
(BELOWOF4-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF2-ROBOT)
(ABOVEOF4-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF6-ROBOT)
(BELOWOF5-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF1-ROBOT)
(ABOVEOF1-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF1-ROBOT)
(RIGHTOF2-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF5-ROBOT)
(RIGHTOF6-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF6-ROBOT)
(BELOWOF6-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF6-ROBOT)
(LEFTOF3-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF5-ROBOT)
(RIGHTOF5-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF2-ROBOT)
(ABOVEOF3-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF4-ROBOT)
(LEFTOF4-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF5-ROBOT)
(LEFTOF3-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF3-ROBOT)
(ABOVEOF3-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF5-ROBOT)
(BELOWOF4-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF5-ROBOT)
(BELOWOF5-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(NOT-CHECKCONSISTENCY-)
(not (CHECKCONSISTENCY-))
(not (Flag100prime-))
(not (Flag97prime-))
(not (Flag61prime-))
(not (Flag12prime-))
(not (Flag11prime-))
(not (Flag10prime-))
(not (Flag9prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag102prime-))
(not (Flag101prime-))
(not (Flag95prime-))
(not (Flag94prime-))
(not (Flag93prime-))
(not (Flag91prime-))
(not (Flag90prime-))
(not (Flag89prime-))
(not (Flag87prime-))
(not (Flag86prime-))
(not (Flag85prime-))
(not (Flag84prime-))
(not (Flag83prime-))
(not (Flag81prime-))
(not (Flag80prime-))
(not (Flag79prime-))
(not (Flag78prime-))
(not (Flag77prime-))
(not (Flag76prime-))
(not (Flag74prime-))
(not (Flag73prime-))
(not (Flag72prime-))
(not (Flag71prime-))
(not (Flag70prime-))
(not (Flag69prime-))
(not (Flag68prime-))
(not (Flag67prime-))
(not (Flag66prime-))
(not (Flag65prime-))
(not (Flag64prime-))
(not (Flag63prime-))
(not (Flag62prime-))
(not (Flag60prime-))
(not (Flag59prime-))
(not (Flag58prime-))
(not (Flag57prime-))
(not (Flag56prime-))
(not (Flag55prime-))
(not (Flag54prime-))
(not (Flag53prime-))
(not (Flag52prime-))
(not (Flag4prime-))
(not (Flag103prime-))
(not (Flag98prime-))
(not (Flag96prime-))
(not (Flag92prime-))
(not (Flag88prime-))
(not (Flag82prime-))
(not (Flag75prime-))
(not (Flag50prime-))
(not (Flag49prime-))
(not (Flag47prime-))
(not (Flag46prime-))
(not (Flag44prime-))
(not (Flag43prime-))
(not (Flag42prime-))
(not (Flag40prime-))
(not (Flag39prime-))
(not (Flag38prime-))
(not (Flag37prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag33prime-))
(not (Flag32prime-))
(not (Flag31prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag27prime-))
(not (Flag26prime-))
(not (Flag25prime-))
(not (Flag24prime-))
(not (Flag23prime-))
(not (Flag22prime-))
(not (Flag21prime-))
(not (Flag20prime-))
(not (Flag19prime-))
(not (Flag18prime-))
(not (Flag17prime-))
(not (Flag16prime-))
(not (Flag15prime-))
(not (Flag14prime-))
(not (Flag13prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag1prime-))
(not (Flag99prime-))
(not (Flag51prime-))
(not (Flag48prime-))
(not (Flag45prime-))
(not (Flag41prime-))
(not (Flag35prime-))
(not (Flag29prime-))
)
)
(:action Flag4Action
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(Flag3-)
(Flag3prime-)
)
:effect
(and
(Flag4-)
(Flag4prime-)
)

)
(:action Prim4Action-2
:parameters ()
:precondition
(and
(not (Flag3-))
(Flag3prime-)
)
:effect
(and
(Flag4prime-)
(not (Flag4-))
)

)
(:action Flag52Action-0
:parameters ()
:precondition
(and
(ABOVEOF6-ROBOT)
)
:effect
(and
(Flag52-)
(Flag52prime-)
)

)
(:action Flag52Action-1
:parameters ()
:precondition
(and
(ABOVEOF1-ROBOT)
)
:effect
(and
(Flag52-)
(Flag52prime-)
)

)
(:action Flag52Action-2
:parameters ()
:precondition
(and
(ABOVEOF2-ROBOT)
)
:effect
(and
(Flag52-)
(Flag52prime-)
)

)
(:action Flag52Action-3
:parameters ()
:precondition
(and
(ABOVEOF4-ROBOT)
)
:effect
(and
(Flag52-)
(Flag52prime-)
)

)
(:action Flag52Action-4
:parameters ()
:precondition
(and
(ABOVEOF3-ROBOT)
)
:effect
(and
(Flag52-)
(Flag52prime-)
)

)
(:action Flag52Action-5
:parameters ()
:precondition
(and
(ABOVEOF5-ROBOT)
)
:effect
(and
(Flag52-)
(Flag52prime-)
)

)
(:action Prim52Action
:parameters ()
:precondition
(and
(not (ABOVEOF6-ROBOT))
(not (ABOVEOF1-ROBOT))
(not (ABOVEOF2-ROBOT))
(not (ABOVEOF4-ROBOT))
(not (ABOVEOF3-ROBOT))
(not (ABOVEOF5-ROBOT))
)
:effect
(and
(Flag52prime-)
(not (Flag52-))
)

)
(:action Flag53Action-0
:parameters ()
:precondition
(and
(ABOVEOF2-ROBOT)
)
:effect
(and
(Flag53-)
(Flag53prime-)
)

)
(:action Flag53Action-1
:parameters ()
:precondition
(and
(ABOVEOF3-ROBOT)
)
:effect
(and
(Flag53-)
(Flag53prime-)
)

)
(:action Flag53Action-2
:parameters ()
:precondition
(and
(ABOVEOF4-ROBOT)
)
:effect
(and
(Flag53-)
(Flag53prime-)
)

)
(:action Flag53Action-3
:parameters ()
:precondition
(and
(ABOVEOF5-ROBOT)
)
:effect
(and
(Flag53-)
(Flag53prime-)
)

)
(:action Flag53Action-4
:parameters ()
:precondition
(and
(ABOVEOF6-ROBOT)
)
:effect
(and
(Flag53-)
(Flag53prime-)
)

)
(:action Prim53Action
:parameters ()
:precondition
(and
(not (ABOVEOF2-ROBOT))
(not (ABOVEOF3-ROBOT))
(not (ABOVEOF4-ROBOT))
(not (ABOVEOF5-ROBOT))
(not (ABOVEOF6-ROBOT))
)
:effect
(and
(Flag53prime-)
(not (Flag53-))
)

)
(:action Flag54Action-0
:parameters ()
:precondition
(and
(ABOVEOF4-ROBOT)
)
:effect
(and
(Flag54-)
(Flag54prime-)
)

)
(:action Flag54Action-1
:parameters ()
:precondition
(and
(ABOVEOF3-ROBOT)
)
:effect
(and
(Flag54-)
(Flag54prime-)
)

)
(:action Flag54Action-2
:parameters ()
:precondition
(and
(ABOVEOF5-ROBOT)
)
:effect
(and
(Flag54-)
(Flag54prime-)
)

)
(:action Flag54Action-3
:parameters ()
:precondition
(and
(ABOVEOF6-ROBOT)
)
:effect
(and
(Flag54-)
(Flag54prime-)
)

)
(:action Prim54Action
:parameters ()
:precondition
(and
(not (ABOVEOF4-ROBOT))
(not (ABOVEOF3-ROBOT))
(not (ABOVEOF5-ROBOT))
(not (ABOVEOF6-ROBOT))
)
:effect
(and
(Flag54prime-)
(not (Flag54-))
)

)
(:action Flag55Action-0
:parameters ()
:precondition
(and
(ABOVEOF4-ROBOT)
)
:effect
(and
(Flag55-)
(Flag55prime-)
)

)
(:action Flag55Action-1
:parameters ()
:precondition
(and
(ABOVEOF5-ROBOT)
)
:effect
(and
(Flag55-)
(Flag55prime-)
)

)
(:action Flag55Action-2
:parameters ()
:precondition
(and
(ABOVEOF6-ROBOT)
)
:effect
(and
(Flag55-)
(Flag55prime-)
)

)
(:action Prim55Action
:parameters ()
:precondition
(and
(not (ABOVEOF4-ROBOT))
(not (ABOVEOF5-ROBOT))
(not (ABOVEOF6-ROBOT))
)
:effect
(and
(Flag55prime-)
(not (Flag55-))
)

)
(:action Flag56Action-0
:parameters ()
:precondition
(and
(ABOVEOF5-ROBOT)
)
:effect
(and
(Flag56-)
(Flag56prime-)
)

)
(:action Flag56Action-1
:parameters ()
:precondition
(and
(ABOVEOF6-ROBOT)
)
:effect
(and
(Flag56-)
(Flag56prime-)
)

)
(:action Prim56Action
:parameters ()
:precondition
(and
(not (ABOVEOF5-ROBOT))
(not (ABOVEOF6-ROBOT))
)
:effect
(and
(Flag56prime-)
(not (Flag56-))
)

)
(:action Flag57Action-0
:parameters ()
:precondition
(and
(BELOWOF2-ROBOT)
)
:effect
(and
(Flag57-)
(Flag57prime-)
)

)
(:action Flag57Action-1
:parameters ()
:precondition
(and
(BELOWOF1-ROBOT)
)
:effect
(and
(Flag57-)
(Flag57prime-)
)

)
(:action Prim57Action
:parameters ()
:precondition
(and
(not (BELOWOF2-ROBOT))
(not (BELOWOF1-ROBOT))
)
:effect
(and
(Flag57prime-)
(not (Flag57-))
)

)
(:action Flag58Action-0
:parameters ()
:precondition
(and
(BELOWOF2-ROBOT)
)
:effect
(and
(Flag58-)
(Flag58prime-)
)

)
(:action Flag58Action-1
:parameters ()
:precondition
(and
(BELOWOF1-ROBOT)
)
:effect
(and
(Flag58-)
(Flag58prime-)
)

)
(:action Flag58Action-2
:parameters ()
:precondition
(and
(BELOWOF3-ROBOT)
)
:effect
(and
(Flag58-)
(Flag58prime-)
)

)
(:action Prim58Action
:parameters ()
:precondition
(and
(not (BELOWOF2-ROBOT))
(not (BELOWOF1-ROBOT))
(not (BELOWOF3-ROBOT))
)
:effect
(and
(Flag58prime-)
(not (Flag58-))
)

)
(:action Flag59Action-0
:parameters ()
:precondition
(and
(BELOWOF2-ROBOT)
)
:effect
(and
(Flag59-)
(Flag59prime-)
)

)
(:action Flag59Action-1
:parameters ()
:precondition
(and
(BELOWOF1-ROBOT)
)
:effect
(and
(Flag59-)
(Flag59prime-)
)

)
(:action Flag59Action-2
:parameters ()
:precondition
(and
(BELOWOF3-ROBOT)
)
:effect
(and
(Flag59-)
(Flag59prime-)
)

)
(:action Flag59Action-3
:parameters ()
:precondition
(and
(BELOWOF4-ROBOT)
)
:effect
(and
(Flag59-)
(Flag59prime-)
)

)
(:action Prim59Action
:parameters ()
:precondition
(and
(not (BELOWOF2-ROBOT))
(not (BELOWOF1-ROBOT))
(not (BELOWOF3-ROBOT))
(not (BELOWOF4-ROBOT))
)
:effect
(and
(Flag59prime-)
(not (Flag59-))
)

)
(:action Flag60Action-0
:parameters ()
:precondition
(and
(BELOWOF2-ROBOT)
)
:effect
(and
(Flag60-)
(Flag60prime-)
)

)
(:action Flag60Action-1
:parameters ()
:precondition
(and
(BELOWOF1-ROBOT)
)
:effect
(and
(Flag60-)
(Flag60prime-)
)

)
(:action Flag60Action-2
:parameters ()
:precondition
(and
(BELOWOF3-ROBOT)
)
:effect
(and
(Flag60-)
(Flag60prime-)
)

)
(:action Flag60Action-3
:parameters ()
:precondition
(and
(BELOWOF4-ROBOT)
)
:effect
(and
(Flag60-)
(Flag60prime-)
)

)
(:action Flag60Action-4
:parameters ()
:precondition
(and
(BELOWOF5-ROBOT)
)
:effect
(and
(Flag60-)
(Flag60prime-)
)

)
(:action Prim60Action
:parameters ()
:precondition
(and
(not (BELOWOF2-ROBOT))
(not (BELOWOF1-ROBOT))
(not (BELOWOF3-ROBOT))
(not (BELOWOF4-ROBOT))
(not (BELOWOF5-ROBOT))
)
:effect
(and
(Flag60prime-)
(not (Flag60-))
)

)
(:action Flag61Action-1
:parameters ()
:precondition
(and
(BELOWOF5-ROBOT)
)
:effect
(and
(Flag61-)
(Flag61prime-)
)

)
(:action Flag61Action-2
:parameters ()
:precondition
(and
(BELOWOF2-ROBOT)
)
:effect
(and
(Flag61-)
(Flag61prime-)
)

)
(:action Flag61Action-3
:parameters ()
:precondition
(and
(BELOWOF1-ROBOT)
)
:effect
(and
(Flag61-)
(Flag61prime-)
)

)
(:action Flag61Action-4
:parameters ()
:precondition
(and
(BELOWOF4-ROBOT)
)
:effect
(and
(Flag61-)
(Flag61prime-)
)

)
(:action Flag61Action-5
:parameters ()
:precondition
(and
(BELOWOF3-ROBOT)
)
:effect
(and
(Flag61-)
(Flag61prime-)
)

)
(:action Prim61Action
:parameters ()
:precondition
(and
(not (BELOWOF6-ROBOT))
(not (BELOWOF5-ROBOT))
(not (BELOWOF2-ROBOT))
(not (BELOWOF1-ROBOT))
(not (BELOWOF4-ROBOT))
(not (BELOWOF3-ROBOT))
)
:effect
(and
(Flag61prime-)
(not (Flag61-))
)

)
(:action Flag62Action
:parameters ()
:precondition
(and
(BELOWOF1-ROBOT)
(ABOVEOF4-ROBOT)
)
:effect
(and
(Flag62-)
(Flag62prime-)
)

)
(:action Flag63Action
:parameters ()
:precondition
(and
(BELOWOF1-ROBOT)
(ABOVEOF2-ROBOT)
)
:effect
(and
(Flag63-)
(Flag63prime-)
)

)
(:action Flag64Action
:parameters ()
:precondition
(and
(ABOVEOF6-ROBOT)
(BELOWOF1-ROBOT)
)
:effect
(and
(Flag64-)
(Flag64prime-)
)

)
(:action Flag65Action
:parameters ()
:precondition
(and
(BELOWOF1-ROBOT)
(ABOVEOF1-ROBOT)
)
:effect
(and
(Flag65-)
(Flag65prime-)
)

)
(:action Flag66Action
:parameters ()
:precondition
(and
(BELOWOF1-ROBOT)
(ABOVEOF5-ROBOT)
)
:effect
(and
(Flag66-)
(Flag66prime-)
)

)
(:action Flag67Action
:parameters ()
:precondition
(and
(ABOVEOF3-ROBOT)
(BELOWOF1-ROBOT)
)
:effect
(and
(Flag67-)
(Flag67prime-)
)

)
(:action Flag68Action-5
:parameters ()
:precondition
(and
(BELOWOF1-ROBOT)
)
:effect
(and
(Flag68-)
(Flag68prime-)
)

)
(:action Prim62Action-0
:parameters ()
:precondition
(and
(not (BELOWOF1-ROBOT))
)
:effect
(and
(Flag62prime-)
(not (Flag62-))
)

)
(:action Prim62Action-1
:parameters ()
:precondition
(and
(not (ABOVEOF4-ROBOT))
)
:effect
(and
(Flag62prime-)
(not (Flag62-))
)

)
(:action Prim63Action-0
:parameters ()
:precondition
(and
(not (BELOWOF1-ROBOT))
)
:effect
(and
(Flag63prime-)
(not (Flag63-))
)

)
(:action Prim63Action-1
:parameters ()
:precondition
(and
(not (ABOVEOF2-ROBOT))
)
:effect
(and
(Flag63prime-)
(not (Flag63-))
)

)
(:action Prim64Action-0
:parameters ()
:precondition
(and
(not (ABOVEOF6-ROBOT))
)
:effect
(and
(Flag64prime-)
(not (Flag64-))
)

)
(:action Prim64Action-1
:parameters ()
:precondition
(and
(not (BELOWOF1-ROBOT))
)
:effect
(and
(Flag64prime-)
(not (Flag64-))
)

)
(:action Prim65Action-0
:parameters ()
:precondition
(and
(not (BELOWOF1-ROBOT))
)
:effect
(and
(Flag65prime-)
(not (Flag65-))
)

)
(:action Prim65Action-1
:parameters ()
:precondition
(and
(not (ABOVEOF1-ROBOT))
)
:effect
(and
(Flag65prime-)
(not (Flag65-))
)

)
(:action Prim66Action-0
:parameters ()
:precondition
(and
(not (BELOWOF1-ROBOT))
)
:effect
(and
(Flag66prime-)
(not (Flag66-))
)

)
(:action Prim66Action-1
:parameters ()
:precondition
(and
(not (ABOVEOF5-ROBOT))
)
:effect
(and
(Flag66prime-)
(not (Flag66-))
)

)
(:action Prim67Action-0
:parameters ()
:precondition
(and
(not (ABOVEOF3-ROBOT))
)
:effect
(and
(Flag67prime-)
(not (Flag67-))
)

)
(:action Prim67Action-1
:parameters ()
:precondition
(and
(not (BELOWOF1-ROBOT))
)
:effect
(and
(Flag67prime-)
(not (Flag67-))
)

)
(:action Flag69Action
:parameters ()
:precondition
(and
(ABOVEOF6-ROBOT)
(BELOWOF2-ROBOT)
)
:effect
(and
(Flag69-)
(Flag69prime-)
)

)
(:action Flag70Action
:parameters ()
:precondition
(and
(ABOVEOF2-ROBOT)
(BELOWOF2-ROBOT)
)
:effect
(and
(Flag70-)
(Flag70prime-)
)

)
(:action Flag71Action
:parameters ()
:precondition
(and
(ABOVEOF4-ROBOT)
(BELOWOF2-ROBOT)
)
:effect
(and
(Flag71-)
(Flag71prime-)
)

)
(:action Flag72Action
:parameters ()
:precondition
(and
(BELOWOF2-ROBOT)
(ABOVEOF5-ROBOT)
)
:effect
(and
(Flag72-)
(Flag72prime-)
)

)
(:action Flag73Action
:parameters ()
:precondition
(and
(ABOVEOF3-ROBOT)
(BELOWOF2-ROBOT)
)
:effect
(and
(Flag73-)
(Flag73prime-)
)

)
(:action Flag74Action
:parameters ()
:precondition
(and
(ABOVEOF1-ROBOT)
(BELOWOF2-ROBOT)
)
:effect
(and
(Flag74-)
(Flag74prime-)
)

)
(:action Prim69Action-0
:parameters ()
:precondition
(and
(not (ABOVEOF6-ROBOT))
)
:effect
(and
(Flag69prime-)
(not (Flag69-))
)

)
(:action Prim69Action-1
:parameters ()
:precondition
(and
(not (BELOWOF2-ROBOT))
)
:effect
(and
(Flag69prime-)
(not (Flag69-))
)

)
(:action Prim70Action-0
:parameters ()
:precondition
(and
(not (ABOVEOF2-ROBOT))
)
:effect
(and
(Flag70prime-)
(not (Flag70-))
)

)
(:action Prim70Action-1
:parameters ()
:precondition
(and
(not (BELOWOF2-ROBOT))
)
:effect
(and
(Flag70prime-)
(not (Flag70-))
)

)
(:action Prim71Action-0
:parameters ()
:precondition
(and
(not (ABOVEOF4-ROBOT))
)
:effect
(and
(Flag71prime-)
(not (Flag71-))
)

)
(:action Prim71Action-1
:parameters ()
:precondition
(and
(not (BELOWOF2-ROBOT))
)
:effect
(and
(Flag71prime-)
(not (Flag71-))
)

)
(:action Prim72Action-0
:parameters ()
:precondition
(and
(not (BELOWOF2-ROBOT))
)
:effect
(and
(Flag72prime-)
(not (Flag72-))
)

)
(:action Prim72Action-1
:parameters ()
:precondition
(and
(not (ABOVEOF5-ROBOT))
)
:effect
(and
(Flag72prime-)
(not (Flag72-))
)

)
(:action Prim73Action-0
:parameters ()
:precondition
(and
(not (ABOVEOF3-ROBOT))
)
:effect
(and
(Flag73prime-)
(not (Flag73-))
)

)
(:action Prim73Action-1
:parameters ()
:precondition
(and
(not (BELOWOF2-ROBOT))
)
:effect
(and
(Flag73prime-)
(not (Flag73-))
)

)
(:action Prim74Action-0
:parameters ()
:precondition
(and
(not (ABOVEOF1-ROBOT))
)
:effect
(and
(Flag74prime-)
(not (Flag74-))
)

)
(:action Prim74Action-1
:parameters ()
:precondition
(and
(not (BELOWOF2-ROBOT))
)
:effect
(and
(Flag74prime-)
(not (Flag74-))
)

)
(:action Flag76Action
:parameters ()
:precondition
(and
(BELOWOF3-ROBOT)
(ABOVEOF2-ROBOT)
)
:effect
(and
(Flag76-)
(Flag76prime-)
)

)
(:action Flag77Action
:parameters ()
:precondition
(and
(BELOWOF3-ROBOT)
(ABOVEOF6-ROBOT)
)
:effect
(and
(Flag77-)
(Flag77prime-)
)

)
(:action Flag78Action
:parameters ()
:precondition
(and
(ABOVEOF3-ROBOT)
(BELOWOF3-ROBOT)
)
:effect
(and
(Flag78-)
(Flag78prime-)
)

)
(:action Flag79Action
:parameters ()
:precondition
(and
(BELOWOF3-ROBOT)
(ABOVEOF5-ROBOT)
)
:effect
(and
(Flag79-)
(Flag79prime-)
)

)
(:action Flag80Action
:parameters ()
:precondition
(and
(BELOWOF3-ROBOT)
(ABOVEOF4-ROBOT)
)
:effect
(and
(Flag80-)
(Flag80prime-)
)

)
(:action Flag81Action
:parameters ()
:precondition
(and
(BELOWOF2-ROBOT)
(ABOVEOF2-ROBOT)
)
:effect
(and
(Flag81-)
(Flag81prime-)
)

)
(:action Prim76Action-0
:parameters ()
:precondition
(and
(not (BELOWOF3-ROBOT))
)
:effect
(and
(Flag76prime-)
(not (Flag76-))
)

)
(:action Prim76Action-1
:parameters ()
:precondition
(and
(not (ABOVEOF2-ROBOT))
)
:effect
(and
(Flag76prime-)
(not (Flag76-))
)

)
(:action Prim77Action-0
:parameters ()
:precondition
(and
(not (BELOWOF3-ROBOT))
)
:effect
(and
(Flag77prime-)
(not (Flag77-))
)

)
(:action Prim77Action-1
:parameters ()
:precondition
(and
(not (ABOVEOF6-ROBOT))
)
:effect
(and
(Flag77prime-)
(not (Flag77-))
)

)
(:action Prim78Action-0
:parameters ()
:precondition
(and
(not (ABOVEOF3-ROBOT))
)
:effect
(and
(Flag78prime-)
(not (Flag78-))
)

)
(:action Prim78Action-1
:parameters ()
:precondition
(and
(not (BELOWOF3-ROBOT))
)
:effect
(and
(Flag78prime-)
(not (Flag78-))
)

)
(:action Prim79Action-0
:parameters ()
:precondition
(and
(not (BELOWOF3-ROBOT))
)
:effect
(and
(Flag79prime-)
(not (Flag79-))
)

)
(:action Prim79Action-1
:parameters ()
:precondition
(and
(not (ABOVEOF5-ROBOT))
)
:effect
(and
(Flag79prime-)
(not (Flag79-))
)

)
(:action Prim80Action-0
:parameters ()
:precondition
(and
(not (BELOWOF3-ROBOT))
)
:effect
(and
(Flag80prime-)
(not (Flag80-))
)

)
(:action Prim80Action-1
:parameters ()
:precondition
(and
(not (ABOVEOF4-ROBOT))
)
:effect
(and
(Flag80prime-)
(not (Flag80-))
)

)
(:action Prim81Action-0
:parameters ()
:precondition
(and
(not (BELOWOF2-ROBOT))
)
:effect
(and
(Flag81prime-)
(not (Flag81-))
)

)
(:action Prim81Action-1
:parameters ()
:precondition
(and
(not (ABOVEOF2-ROBOT))
)
:effect
(and
(Flag81prime-)
(not (Flag81-))
)

)
(:action Flag83Action
:parameters ()
:precondition
(and
(ABOVEOF3-ROBOT)
(BELOWOF4-ROBOT)
)
:effect
(and
(Flag83-)
(Flag83prime-)
)

)
(:action Flag84Action
:parameters ()
:precondition
(and
(BELOWOF4-ROBOT)
(ABOVEOF5-ROBOT)
)
:effect
(and
(Flag84-)
(Flag84prime-)
)

)
(:action Flag85Action
:parameters ()
:precondition
(and
(ABOVEOF6-ROBOT)
(BELOWOF4-ROBOT)
)
:effect
(and
(Flag85-)
(Flag85prime-)
)

)
(:action Flag86Action
:parameters ()
:precondition
(and
(BELOWOF4-ROBOT)
(ABOVEOF4-ROBOT)
)
:effect
(and
(Flag86-)
(Flag86prime-)
)

)
(:action Flag87Action
:parameters ()
:precondition
(and
(BELOWOF3-ROBOT)
(ABOVEOF3-ROBOT)
)
:effect
(and
(Flag87-)
(Flag87prime-)
)

)
(:action Prim83Action-0
:parameters ()
:precondition
(and
(not (ABOVEOF3-ROBOT))
)
:effect
(and
(Flag83prime-)
(not (Flag83-))
)

)
(:action Prim83Action-1
:parameters ()
:precondition
(and
(not (BELOWOF4-ROBOT))
)
:effect
(and
(Flag83prime-)
(not (Flag83-))
)

)
(:action Prim84Action-0
:parameters ()
:precondition
(and
(not (BELOWOF4-ROBOT))
)
:effect
(and
(Flag84prime-)
(not (Flag84-))
)

)
(:action Prim84Action-1
:parameters ()
:precondition
(and
(not (ABOVEOF5-ROBOT))
)
:effect
(and
(Flag84prime-)
(not (Flag84-))
)

)
(:action Prim85Action-0
:parameters ()
:precondition
(and
(not (ABOVEOF6-ROBOT))
)
:effect
(and
(Flag85prime-)
(not (Flag85-))
)

)
(:action Prim85Action-1
:parameters ()
:precondition
(and
(not (BELOWOF4-ROBOT))
)
:effect
(and
(Flag85prime-)
(not (Flag85-))
)

)
(:action Prim86Action-0
:parameters ()
:precondition
(and
(not (BELOWOF4-ROBOT))
)
:effect
(and
(Flag86prime-)
(not (Flag86-))
)

)
(:action Prim86Action-1
:parameters ()
:precondition
(and
(not (ABOVEOF4-ROBOT))
)
:effect
(and
(Flag86prime-)
(not (Flag86-))
)

)
(:action Prim87Action-0
:parameters ()
:precondition
(and
(not (BELOWOF3-ROBOT))
)
:effect
(and
(Flag87prime-)
(not (Flag87-))
)

)
(:action Prim87Action-1
:parameters ()
:precondition
(and
(not (ABOVEOF3-ROBOT))
)
:effect
(and
(Flag87prime-)
(not (Flag87-))
)

)
(:action Flag89Action
:parameters ()
:precondition
(and
(ABOVEOF4-ROBOT)
(BELOWOF5-ROBOT)
)
:effect
(and
(Flag89-)
(Flag89prime-)
)

)
(:action Flag90Action
:parameters ()
:precondition
(and
(ABOVEOF5-ROBOT)
(BELOWOF5-ROBOT)
)
:effect
(and
(Flag90-)
(Flag90prime-)
)

)
(:action Flag91Action
:parameters ()
:precondition
(and
(ABOVEOF6-ROBOT)
(BELOWOF5-ROBOT)
)
:effect
(and
(Flag91-)
(Flag91prime-)
)

)
(:action Prim89Action-0
:parameters ()
:precondition
(and
(not (ABOVEOF4-ROBOT))
)
:effect
(and
(Flag89prime-)
(not (Flag89-))
)

)
(:action Prim89Action-1
:parameters ()
:precondition
(and
(not (BELOWOF5-ROBOT))
)
:effect
(and
(Flag89prime-)
(not (Flag89-))
)

)
(:action Prim90Action-0
:parameters ()
:precondition
(and
(not (ABOVEOF5-ROBOT))
)
:effect
(and
(Flag90prime-)
(not (Flag90-))
)

)
(:action Prim90Action-1
:parameters ()
:precondition
(and
(not (BELOWOF5-ROBOT))
)
:effect
(and
(Flag90prime-)
(not (Flag90-))
)

)
(:action Prim91Action-0
:parameters ()
:precondition
(and
(not (ABOVEOF6-ROBOT))
)
:effect
(and
(Flag91prime-)
(not (Flag91-))
)

)
(:action Prim91Action-1
:parameters ()
:precondition
(and
(not (BELOWOF5-ROBOT))
)
:effect
(and
(Flag91prime-)
(not (Flag91-))
)

)
(:action Flag93Action
:parameters ()
:precondition
(and
(BELOWOF6-ROBOT)
(ABOVEOF6-ROBOT)
)
:effect
(and
(Flag93-)
(Flag93prime-)
)

)
(:action Flag94Action
:parameters ()
:precondition
(and
(BELOWOF5-ROBOT)
(ABOVEOF5-ROBOT)
)
:effect
(and
(Flag94-)
(Flag94prime-)
)

)
(:action Flag95Action
:parameters ()
:precondition
(and
(BELOWOF6-ROBOT)
(ABOVEOF5-ROBOT)
)
:effect
(and
(Flag95-)
(Flag95prime-)
)

)
(:action Prim93Action-1
:parameters ()
:precondition
(and
(not (ABOVEOF6-ROBOT))
)
:effect
(and
(Flag93prime-)
(not (Flag93-))
)

)
(:action Prim94Action-0
:parameters ()
:precondition
(and
(not (BELOWOF5-ROBOT))
)
:effect
(and
(Flag94prime-)
(not (Flag94-))
)

)
(:action Prim94Action-1
:parameters ()
:precondition
(and
(not (ABOVEOF5-ROBOT))
)
:effect
(and
(Flag94prime-)
(not (Flag94-))
)

)
(:action Prim95Action-1
:parameters ()
:precondition
(and
(not (ABOVEOF5-ROBOT))
)
:effect
(and
(Flag95prime-)
(not (Flag95-))
)

)
(:action Flag100Action-1
:parameters ()
:precondition
(and
(BELOWOF5-ROBOT)
)
:effect
(and
(Flag100-)
(Flag100prime-)
)

)
(:action Flag100Action-2
:parameters ()
:precondition
(and
(BELOWOF2-ROBOT)
)
:effect
(and
(Flag100-)
(Flag100prime-)
)

)
(:action Flag100Action-3
:parameters ()
:precondition
(and
(BELOWOF1-ROBOT)
)
:effect
(and
(Flag100-)
(Flag100prime-)
)

)
(:action Flag100Action-4
:parameters ()
:precondition
(and
(BELOWOF4-ROBOT)
)
:effect
(and
(Flag100-)
(Flag100prime-)
)

)
(:action Flag100Action-5
:parameters ()
:precondition
(and
(BELOWOF7-ROBOT)
)
:effect
(and
(Flag100-)
(Flag100prime-)
)

)
(:action Flag100Action-6
:parameters ()
:precondition
(and
(BELOWOF3-ROBOT)
)
:effect
(and
(Flag100-)
(Flag100prime-)
)

)
(:action Prim100Action
:parameters ()
:precondition
(and
(not (BELOWOF6-ROBOT))
(not (BELOWOF5-ROBOT))
(not (BELOWOF2-ROBOT))
(not (BELOWOF1-ROBOT))
(not (BELOWOF4-ROBOT))
(not (BELOWOF7-ROBOT))
(not (BELOWOF3-ROBOT))
)
:effect
(and
(Flag100prime-)
(not (Flag100-))
)

)
(:action Flag101Action
:parameters ()
:precondition
(and
(ABOVEOF6-ROBOT)
(BELOWOF7-ROBOT)
)
:effect
(and
(Flag101-)
(Flag101prime-)
)

)
(:action Flag102Action
:parameters ()
:precondition
(and
(ABOVEOF6-ROBOT)
(BELOWOF6-ROBOT)
)
:effect
(and
(Flag102-)
(Flag102prime-)
)

)
(:action Prim101Action-0
:parameters ()
:precondition
(and
(not (ABOVEOF6-ROBOT))
)
:effect
(and
(Flag101prime-)
(not (Flag101-))
)

)
(:action Prim101Action-1
:parameters ()
:precondition
(and
(not (BELOWOF7-ROBOT))
)
:effect
(and
(Flag101prime-)
(not (Flag101-))
)

)
(:action Prim102Action-0
:parameters ()
:precondition
(and
(not (ABOVEOF6-ROBOT))
)
:effect
(and
(Flag102prime-)
(not (Flag102-))
)

)
(:action MOVEDOWN-ROBOT
:parameters ()
:precondition
(and
(Flag2-)
(Flag2prime-)
(Flag103prime-)
(Flag96prime-)
(Flag92prime-)
(Flag88prime-)
(Flag82prime-)
(Flag75prime-)
(Flag100prime-)
)
:effect
(and
(when
(and
(Flag103-)
)
(and
(ROW5-ROBOT)
(not (NOT-ROW5-ROBOT))
)
)
(when
(and
(Flag96-)
)
(and
(ROW4-ROBOT)
(not (NOT-ROW4-ROBOT))
)
)
(when
(and
(Flag92-)
)
(and
(ROW3-ROBOT)
(not (NOT-ROW3-ROBOT))
)
)
(when
(and
(Flag88-)
)
(and
(ROW2-ROBOT)
(not (NOT-ROW2-ROBOT))
)
)
(when
(and
(Flag82-)
)
(and
(ROW1-ROBOT)
(not (NOT-ROW1-ROBOT))
)
)
(when
(and
(Flag75-)
)
(and
(ROW0-ROBOT)
(not (NOT-ROW0-ROBOT))
)
)
(when
(and
(ROW6-ROBOT)
)
(and
(ROW5-ROBOT)
(NOT-ROW6-ROBOT)
(not (NOT-ROW5-ROBOT))
(not (ROW6-ROBOT))
)
)
(when
(and
(ROW5-ROBOT)
)
(and
(ROW4-ROBOT)
(NOT-ROW5-ROBOT)
(not (NOT-ROW4-ROBOT))
(not (ROW5-ROBOT))
)
)
(when
(and
(ROW4-ROBOT)
)
(and
(ROW3-ROBOT)
(NOT-ROW4-ROBOT)
(not (NOT-ROW3-ROBOT))
(not (ROW4-ROBOT))
)
)
(when
(and
(ROW3-ROBOT)
)
(and
(ROW2-ROBOT)
(NOT-ROW3-ROBOT)
(not (NOT-ROW2-ROBOT))
(not (ROW3-ROBOT))
)
)
(when
(and
(ROW2-ROBOT)
)
(and
(ROW1-ROBOT)
(NOT-ROW2-ROBOT)
(not (NOT-ROW1-ROBOT))
(not (ROW2-ROBOT))
)
)
(when
(and
(ROW1-ROBOT)
)
(and
(ROW0-ROBOT)
(NOT-ROW1-ROBOT)
(not (NOT-ROW0-ROBOT))
(not (ROW1-ROBOT))
)
)
(when
(and
(ABOVEOF6-ROBOT)
)
(and
(ABOVEOF5-ROBOT)
(NOT-ABOVEOF6-ROBOT)
(ABOVEOF4-ROBOT)
(NOT-ABOVEOF5-ROBOT)
(ABOVEOF3-ROBOT)
(NOT-ABOVEOF4-ROBOT)
(ABOVEOF2-ROBOT)
(NOT-ABOVEOF3-ROBOT)
(ABOVEOF1-ROBOT)
(NOT-ABOVEOF2-ROBOT)
(NOT-ABOVEOF1-ROBOT)
(not (ABOVEOF6-ROBOT))
)
)
(when
(and
(ABOVEOF5-ROBOT)
)
(and
(ABOVEOF4-ROBOT)
(NOT-ABOVEOF5-ROBOT)
(ABOVEOF3-ROBOT)
(NOT-ABOVEOF4-ROBOT)
(ABOVEOF2-ROBOT)
(NOT-ABOVEOF3-ROBOT)
(ABOVEOF1-ROBOT)
(NOT-ABOVEOF2-ROBOT)
(NOT-ABOVEOF1-ROBOT)
(not (ABOVEOF5-ROBOT))
)
)
(when
(and
(ABOVEOF4-ROBOT)
)
(and
(ABOVEOF3-ROBOT)
(NOT-ABOVEOF4-ROBOT)
(ABOVEOF2-ROBOT)
(NOT-ABOVEOF3-ROBOT)
(ABOVEOF1-ROBOT)
(NOT-ABOVEOF2-ROBOT)
(NOT-ABOVEOF1-ROBOT)
(not (ABOVEOF4-ROBOT))
)
)
(when
(and
(ABOVEOF3-ROBOT)
)
(and
(ABOVEOF2-ROBOT)
(NOT-ABOVEOF3-ROBOT)
(ABOVEOF1-ROBOT)
(NOT-ABOVEOF2-ROBOT)
(NOT-ABOVEOF1-ROBOT)
(not (ABOVEOF3-ROBOT))
)
)
(when
(and
(ABOVEOF2-ROBOT)
)
(and
(ABOVEOF1-ROBOT)
(NOT-ABOVEOF2-ROBOT)
(NOT-ABOVEOF1-ROBOT)
(not (ABOVEOF2-ROBOT))
)
)
(when
(and
(ABOVEOF1-ROBOT)
)
(and
(NOT-ABOVEOF1-ROBOT)
(not (ABOVEOF1-ROBOT))
)
)
(when
(and
(Flag100-)
)
(and
(BELOWOF6-ROBOT)
(not (NOT-BELOWOF6-ROBOT))
)
)
(when
(and
(BELOWOF3-ROBOT)
)
(and
(BELOWOF5-ROBOT)
(BELOWOF4-ROBOT)
(BELOWOF2-ROBOT)
(not (NOT-BELOWOF5-ROBOT))
(not (NOT-BELOWOF4-ROBOT))
(not (NOT-BELOWOF3-ROBOT))
(not (NOT-BELOWOF2-ROBOT))
)
)
(when
(and
(BELOWOF4-ROBOT)
)
(and
(BELOWOF5-ROBOT)
(BELOWOF3-ROBOT)
(not (NOT-BELOWOF5-ROBOT))
(not (NOT-BELOWOF4-ROBOT))
(not (NOT-BELOWOF3-ROBOT))
)
)
(when
(and
(BELOWOF1-ROBOT)
)
(and
(BELOWOF5-ROBOT)
(BELOWOF4-ROBOT)
(BELOWOF3-ROBOT)
(BELOWOF2-ROBOT)
(not (NOT-BELOWOF5-ROBOT))
(not (NOT-BELOWOF4-ROBOT))
(not (NOT-BELOWOF3-ROBOT))
(not (NOT-BELOWOF2-ROBOT))
(not (NOT-BELOWOF1-ROBOT))
)
)
(when
(and
(BELOWOF2-ROBOT)
)
(and
(BELOWOF5-ROBOT)
(BELOWOF4-ROBOT)
(BELOWOF3-ROBOT)
(BELOWOF1-ROBOT)
(not (NOT-BELOWOF5-ROBOT))
(not (NOT-BELOWOF4-ROBOT))
(not (NOT-BELOWOF3-ROBOT))
(not (NOT-BELOWOF2-ROBOT))
(not (NOT-BELOWOF1-ROBOT))
)
)
(when
(and
(BELOWOF5-ROBOT)
)
(and
(BELOWOF4-ROBOT)
(not (NOT-BELOWOF5-ROBOT))
(not (NOT-BELOWOF4-ROBOT))
)
)
(when
(and
(BELOWOF6-ROBOT)
)
(and
(BELOWOF5-ROBOT)
(not (NOT-BELOWOF5-ROBOT))
)
)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag100prime-))
(not (Flag97prime-))
(not (Flag61prime-))
(not (Flag12prime-))
(not (Flag11prime-))
(not (Flag10prime-))
(not (Flag9prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag102prime-))
(not (Flag101prime-))
(not (Flag95prime-))
(not (Flag94prime-))
(not (Flag93prime-))
(not (Flag91prime-))
(not (Flag90prime-))
(not (Flag89prime-))
(not (Flag87prime-))
(not (Flag86prime-))
(not (Flag85prime-))
(not (Flag84prime-))
(not (Flag83prime-))
(not (Flag81prime-))
(not (Flag80prime-))
(not (Flag79prime-))
(not (Flag78prime-))
(not (Flag77prime-))
(not (Flag76prime-))
(not (Flag74prime-))
(not (Flag73prime-))
(not (Flag72prime-))
(not (Flag71prime-))
(not (Flag70prime-))
(not (Flag69prime-))
(not (Flag68prime-))
(not (Flag67prime-))
(not (Flag66prime-))
(not (Flag65prime-))
(not (Flag64prime-))
(not (Flag63prime-))
(not (Flag62prime-))
(not (Flag60prime-))
(not (Flag59prime-))
(not (Flag58prime-))
(not (Flag57prime-))
(not (Flag56prime-))
(not (Flag55prime-))
(not (Flag54prime-))
(not (Flag53prime-))
(not (Flag52prime-))
(not (Flag4prime-))
(not (Flag103prime-))
(not (Flag98prime-))
(not (Flag96prime-))
(not (Flag92prime-))
(not (Flag88prime-))
(not (Flag82prime-))
(not (Flag75prime-))
(not (Flag50prime-))
(not (Flag49prime-))
(not (Flag47prime-))
(not (Flag46prime-))
(not (Flag44prime-))
(not (Flag43prime-))
(not (Flag42prime-))
(not (Flag40prime-))
(not (Flag39prime-))
(not (Flag38prime-))
(not (Flag37prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag33prime-))
(not (Flag32prime-))
(not (Flag31prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag27prime-))
(not (Flag26prime-))
(not (Flag25prime-))
(not (Flag24prime-))
(not (Flag23prime-))
(not (Flag22prime-))
(not (Flag21prime-))
(not (Flag20prime-))
(not (Flag19prime-))
(not (Flag18prime-))
(not (Flag17prime-))
(not (Flag16prime-))
(not (Flag15prime-))
(not (Flag14prime-))
(not (Flag13prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag1prime-))
(not (Flag99prime-))
(not (Flag51prime-))
(not (Flag48prime-))
(not (Flag45prime-))
(not (Flag41prime-))
(not (Flag35prime-))
(not (Flag29prime-))
)
)
(:action MOVEUP-ROBOT
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(Flag96prime-)
(Flag92prime-)
(Flag88prime-)
(Flag82prime-)
(Flag75prime-)
(Flag68prime-)
(Flag61prime-)
(Flag60prime-)
(Flag59prime-)
(Flag58prime-)
(Flag57prime-)
(Flag56prime-)
(Flag55prime-)
(Flag54prime-)
(Flag53prime-)
(Flag52prime-)
)
:effect
(and
(when
(and
(Flag96-)
)
(and
(ROW6-ROBOT)
(not (NOT-ROW6-ROBOT))
)
)
(when
(and
(Flag92-)
)
(and
(ROW5-ROBOT)
(not (NOT-ROW5-ROBOT))
)
)
(when
(and
(Flag88-)
)
(and
(ROW4-ROBOT)
(not (NOT-ROW4-ROBOT))
)
)
(when
(and
(Flag82-)
)
(and
(ROW3-ROBOT)
(not (NOT-ROW3-ROBOT))
)
)
(when
(and
(Flag75-)
)
(and
(ROW2-ROBOT)
(not (NOT-ROW2-ROBOT))
)
)
(when
(and
(Flag68-)
)
(and
(ROW1-ROBOT)
(not (NOT-ROW1-ROBOT))
)
)
(when
(and
(ROW5-ROBOT)
)
(and
(ROW6-ROBOT)
(NOT-ROW5-ROBOT)
(not (NOT-ROW6-ROBOT))
(not (ROW5-ROBOT))
)
)
(when
(and
(ROW4-ROBOT)
)
(and
(ROW5-ROBOT)
(NOT-ROW4-ROBOT)
(not (NOT-ROW5-ROBOT))
(not (ROW4-ROBOT))
)
)
(when
(and
(ROW3-ROBOT)
)
(and
(ROW4-ROBOT)
(NOT-ROW3-ROBOT)
(not (NOT-ROW4-ROBOT))
(not (ROW3-ROBOT))
)
)
(when
(and
(ROW2-ROBOT)
)
(and
(ROW3-ROBOT)
(NOT-ROW2-ROBOT)
(not (NOT-ROW3-ROBOT))
(not (ROW2-ROBOT))
)
)
(when
(and
(ROW1-ROBOT)
)
(and
(ROW2-ROBOT)
(NOT-ROW1-ROBOT)
(not (NOT-ROW2-ROBOT))
(not (ROW1-ROBOT))
)
)
(when
(and
(ROW0-ROBOT)
)
(and
(ROW1-ROBOT)
(NOT-ROW0-ROBOT)
(not (NOT-ROW1-ROBOT))
(not (ROW0-ROBOT))
)
)
(when
(and
(Flag61-)
)
(and
(BELOWOF7-ROBOT)
(NOT-BELOWOF6-ROBOT)
(not (NOT-BELOWOF7-ROBOT))
(not (BELOWOF6-ROBOT))
)
)
(when
(and
(Flag60-)
)
(and
(BELOWOF6-ROBOT)
(NOT-BELOWOF5-ROBOT)
(not (NOT-BELOWOF6-ROBOT))
(not (BELOWOF5-ROBOT))
)
)
(when
(and
(Flag59-)
)
(and
(BELOWOF5-ROBOT)
(NOT-BELOWOF4-ROBOT)
(not (NOT-BELOWOF5-ROBOT))
(not (BELOWOF4-ROBOT))
)
)
(when
(and
(Flag58-)
)
(and
(BELOWOF4-ROBOT)
(NOT-BELOWOF3-ROBOT)
(not (NOT-BELOWOF4-ROBOT))
(not (BELOWOF3-ROBOT))
)
)
(when
(and
(Flag57-)
)
(and
(BELOWOF3-ROBOT)
(NOT-BELOWOF2-ROBOT)
(not (NOT-BELOWOF3-ROBOT))
(not (BELOWOF2-ROBOT))
)
)
(when
(and
(BELOWOF1-ROBOT)
)
(and
(BELOWOF2-ROBOT)
(NOT-BELOWOF1-ROBOT)
(not (NOT-BELOWOF2-ROBOT))
(not (BELOWOF1-ROBOT))
)
)
(when
(and
(Flag56-)
)
(and
(ABOVEOF6-ROBOT)
(not (NOT-ABOVEOF6-ROBOT))
)
)
(when
(and
(Flag55-)
)
(and
(ABOVEOF5-ROBOT)
(not (NOT-ABOVEOF5-ROBOT))
)
)
(when
(and
(Flag54-)
)
(and
(ABOVEOF4-ROBOT)
(not (NOT-ABOVEOF4-ROBOT))
)
)
(when
(and
(Flag53-)
)
(and
(ABOVEOF3-ROBOT)
(not (NOT-ABOVEOF3-ROBOT))
)
)
(when
(and
(Flag52-)
)
(and
(ABOVEOF2-ROBOT)
(not (NOT-ABOVEOF2-ROBOT))
)
)
(ABOVEOF1-ROBOT)
(CHECKCONSISTENCY-)
(not (NOT-ABOVEOF1-ROBOT))
(not (NOT-CHECKCONSISTENCY-))
(not (Flag100prime-))
(not (Flag97prime-))
(not (Flag61prime-))
(not (Flag12prime-))
(not (Flag11prime-))
(not (Flag10prime-))
(not (Flag9prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag102prime-))
(not (Flag101prime-))
(not (Flag95prime-))
(not (Flag94prime-))
(not (Flag93prime-))
(not (Flag91prime-))
(not (Flag90prime-))
(not (Flag89prime-))
(not (Flag87prime-))
(not (Flag86prime-))
(not (Flag85prime-))
(not (Flag84prime-))
(not (Flag83prime-))
(not (Flag81prime-))
(not (Flag80prime-))
(not (Flag79prime-))
(not (Flag78prime-))
(not (Flag77prime-))
(not (Flag76prime-))
(not (Flag74prime-))
(not (Flag73prime-))
(not (Flag72prime-))
(not (Flag71prime-))
(not (Flag70prime-))
(not (Flag69prime-))
(not (Flag68prime-))
(not (Flag67prime-))
(not (Flag66prime-))
(not (Flag65prime-))
(not (Flag64prime-))
(not (Flag63prime-))
(not (Flag62prime-))
(not (Flag60prime-))
(not (Flag59prime-))
(not (Flag58prime-))
(not (Flag57prime-))
(not (Flag56prime-))
(not (Flag55prime-))
(not (Flag54prime-))
(not (Flag53prime-))
(not (Flag52prime-))
(not (Flag4prime-))
(not (Flag103prime-))
(not (Flag98prime-))
(not (Flag96prime-))
(not (Flag92prime-))
(not (Flag88prime-))
(not (Flag82prime-))
(not (Flag75prime-))
(not (Flag50prime-))
(not (Flag49prime-))
(not (Flag47prime-))
(not (Flag46prime-))
(not (Flag44prime-))
(not (Flag43prime-))
(not (Flag42prime-))
(not (Flag40prime-))
(not (Flag39prime-))
(not (Flag38prime-))
(not (Flag37prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag33prime-))
(not (Flag32prime-))
(not (Flag31prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag27prime-))
(not (Flag26prime-))
(not (Flag25prime-))
(not (Flag24prime-))
(not (Flag23prime-))
(not (Flag22prime-))
(not (Flag21prime-))
(not (Flag20prime-))
(not (Flag19prime-))
(not (Flag18prime-))
(not (Flag17prime-))
(not (Flag16prime-))
(not (Flag15prime-))
(not (Flag14prime-))
(not (Flag13prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag1prime-))
(not (Flag99prime-))
(not (Flag51prime-))
(not (Flag48prime-))
(not (Flag45prime-))
(not (Flag41prime-))
(not (Flag35prime-))
(not (Flag29prime-))
)
)
(:action Prim1Action-2
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
(Flag1prime-)
(not (Flag1-))
)

)
(:action Prim1Action-3
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
(Flag1prime-)
(not (Flag1-))
)

)
(:action Flag2Action
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
)
:effect
(and
(Flag2-)
(Flag2prime-)
)

)
(:action Prim2Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
(Flag2prime-)
(not (Flag2-))
)

)
(:action Prim2Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
(Flag2prime-)
(not (Flag2-))
)

)
(:action Flag3Action-9
:parameters ()
:precondition
(and
(RIGHTOF2-ROBOT)
)
:effect
(and
(Flag3-)
(Flag3prime-)
)

)
(:action Flag3Action-12
:parameters ()
:precondition
(and
(LEFTOF6-ROBOT)
)
:effect
(and
(Flag3-)
(Flag3prime-)
)

)
(:action Prim4Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
(Flag4prime-)
(not (Flag4-))
)

)
(:action Prim4Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
(Flag4prime-)
(not (Flag4-))
)

)
(:action Flag9Action-2
:parameters ()
:precondition
(and
(LEFTOF6-ROBOT)
)
:effect
(and
(Flag9-)
(Flag9prime-)
)

)
(:action Flag10Action-3
:parameters ()
:precondition
(and
(LEFTOF6-ROBOT)
)
:effect
(and
(Flag10-)
(Flag10prime-)
)

)
(:action Flag11Action-2
:parameters ()
:precondition
(and
(RIGHTOF2-ROBOT)
)
:effect
(and
(Flag11-)
(Flag11prime-)
)

)
(:action Flag12Action-4
:parameters ()
:precondition
(and
(RIGHTOF2-ROBOT)
)
:effect
(and
(Flag12-)
(Flag12prime-)
)

)
(:action Prim22Action-0
:parameters ()
:precondition
(and
(not (RIGHTOF2-ROBOT))
)
:effect
(and
(Flag22prime-)
(not (Flag22-))
)

)
(:action Prim24Action-1
:parameters ()
:precondition
(and
(not (RIGHTOF2-ROBOT))
)
:effect
(and
(Flag24prime-)
(not (Flag24-))
)

)
(:action Prim34Action-1
:parameters ()
:precondition
(and
(not (RIGHTOF2-ROBOT))
)
:effect
(and
(Flag34prime-)
(not (Flag34-))
)

)
(:action Prim46Action-1
:parameters ()
:precondition
(and
(not (LEFTOF6-ROBOT))
)
:effect
(and
(Flag46prime-)
(not (Flag46-))
)

)
(:action Prim47Action-1
:parameters ()
:precondition
(and
(not (LEFTOF6-ROBOT))
)
:effect
(and
(Flag47prime-)
(not (Flag47-))
)

)
(:action Prim49Action-0
:parameters ()
:precondition
(and
(not (LEFTOF6-ROBOT))
)
:effect
(and
(Flag49prime-)
(not (Flag49-))
)

)
(:action Flag61Action-0
:parameters ()
:precondition
(and
(BELOWOF6-ROBOT)
)
:effect
(and
(Flag61-)
(Flag61prime-)
)

)
(:action Prim93Action-0
:parameters ()
:precondition
(and
(not (BELOWOF6-ROBOT))
)
:effect
(and
(Flag93prime-)
(not (Flag93-))
)

)
(:action Prim95Action-0
:parameters ()
:precondition
(and
(not (BELOWOF6-ROBOT))
)
:effect
(and
(Flag95prime-)
(not (Flag95-))
)

)
(:action Flag97Action-2
:parameters ()
:precondition
(and
(RIGHTOF2-ROBOT)
)
:effect
(and
(Flag97-)
(Flag97prime-)
)

)
(:action Flag100Action-0
:parameters ()
:precondition
(and
(BELOWOF6-ROBOT)
)
:effect
(and
(Flag100-)
(Flag100prime-)
)

)
(:action Prim102Action-1
:parameters ()
:precondition
(and
(not (BELOWOF6-ROBOT))
)
:effect
(and
(Flag102prime-)
(not (Flag102-))
)

)
)
