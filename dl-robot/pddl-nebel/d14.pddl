(define (domain grounded-SIMPLE-ADL-ROBOT)
(:requirements
:strips
)
(:predicates
(Flag312-)
(Flag185-)
(Flag163-)
(Flag162-)
(Flag161-)
(Flag157-)
(Flag28-)
(Flag4-)
(Flag2-)
(ROW13-ROBOT)
(ROW12-ROBOT)
(ROW11-ROBOT)
(ROW10-ROBOT)
(ROW9-ROBOT)
(ROW8-ROBOT)
(ROW7-ROBOT)
(ROW6-ROBOT)
(ROW5-ROBOT)
(ROW4-ROBOT)
(ROW3-ROBOT)
(ROW2-ROBOT)
(ROW1-ROBOT)
(BELOWOF14-ROBOT)
(NOT-BELOWOF13-ROBOT)
(BELOWOF12-ROBOT)
(BELOWOF11-ROBOT)
(BELOWOF10-ROBOT)
(BELOWOF9-ROBOT)
(BELOWOF8-ROBOT)
(BELOWOF7-ROBOT)
(BELOWOF6-ROBOT)
(BELOWOF5-ROBOT)
(BELOWOF4-ROBOT)
(BELOWOF3-ROBOT)
(BELOWOF2-ROBOT)
(ABOVEOF13-ROBOT)
(ABOVEOF12-ROBOT)
(ABOVEOF11-ROBOT)
(ABOVEOF10-ROBOT)
(ABOVEOF9-ROBOT)
(ABOVEOF8-ROBOT)
(ABOVEOF7-ROBOT)
(ABOVEOF6-ROBOT)
(ABOVEOF5-ROBOT)
(ABOVEOF4-ROBOT)
(ABOVEOF3-ROBOT)
(ABOVEOF2-ROBOT)
(ABOVEOF1-ROBOT)
(CHECKCONSISTENCY-)
(ROW0-ROBOT)
(BELOWOF1-ROBOT)
(Flag159-)
(Flag158-)
(Flag155-)
(Flag154-)
(Flag153-)
(Flag151-)
(Flag150-)
(Flag149-)
(Flag147-)
(Flag146-)
(Flag145-)
(Flag144-)
(Flag142-)
(Flag141-)
(Flag140-)
(Flag139-)
(Flag138-)
(Flag137-)
(Flag135-)
(Flag134-)
(Flag133-)
(Flag132-)
(Flag131-)
(Flag130-)
(Flag129-)
(Flag128-)
(Flag126-)
(Flag125-)
(Flag124-)
(Flag123-)
(Flag122-)
(Flag121-)
(Flag120-)
(Flag119-)
(Flag117-)
(Flag116-)
(Flag115-)
(Flag114-)
(Flag113-)
(Flag112-)
(Flag111-)
(Flag110-)
(Flag109-)
(Flag108-)
(Flag106-)
(Flag105-)
(Flag104-)
(Flag103-)
(Flag102-)
(Flag101-)
(Flag100-)
(Flag99-)
(Flag98-)
(Flag97-)
(Flag96-)
(Flag94-)
(Flag93-)
(Flag92-)
(Flag91-)
(Flag90-)
(Flag89-)
(Flag88-)
(Flag87-)
(Flag86-)
(Flag85-)
(Flag83-)
(Flag82-)
(Flag81-)
(Flag80-)
(Flag79-)
(Flag78-)
(Flag77-)
(Flag76-)
(Flag75-)
(Flag74-)
(Flag73-)
(Flag72-)
(Flag71-)
(Flag69-)
(Flag68-)
(Flag67-)
(Flag66-)
(Flag65-)
(Flag64-)
(Flag63-)
(Flag62-)
(Flag61-)
(Flag60-)
(Flag59-)
(Flag58-)
(Flag57-)
(Flag55-)
(Flag54-)
(Flag53-)
(Flag52-)
(Flag51-)
(Flag50-)
(Flag49-)
(Flag48-)
(Flag47-)
(Flag46-)
(Flag45-)
(Flag44-)
(Flag43-)
(Flag42-)
(Flag41-)
(Flag40-)
(Flag39-)
(Flag38-)
(Flag37-)
(Flag36-)
(Flag35-)
(Flag34-)
(Flag33-)
(Flag32-)
(Flag31-)
(Flag30-)
(Flag29-)
(Flag27-)
(Flag26-)
(Flag25-)
(Flag24-)
(Flag23-)
(Flag22-)
(Flag21-)
(Flag20-)
(Flag19-)
(Flag18-)
(Flag17-)
(Flag16-)
(Flag15-)
(Flag14-)
(Flag13-)
(Flag12-)
(Flag11-)
(Flag10-)
(Flag9-)
(Flag8-)
(Flag7-)
(Flag6-)
(Flag5-)
(Flag3-)
(ERROR-)
(COLUMN13-ROBOT)
(COLUMN12-ROBOT)
(COLUMN11-ROBOT)
(COLUMN10-ROBOT)
(COLUMN9-ROBOT)
(COLUMN8-ROBOT)
(COLUMN7-ROBOT)
(COLUMN6-ROBOT)
(COLUMN5-ROBOT)
(COLUMN4-ROBOT)
(COLUMN3-ROBOT)
(COLUMN2-ROBOT)
(COLUMN1-ROBOT)
(LEFTOF14-ROBOT)
(NOT-LEFTOF13-ROBOT)
(LEFTOF12-ROBOT)
(LEFTOF11-ROBOT)
(LEFTOF10-ROBOT)
(LEFTOF9-ROBOT)
(LEFTOF8-ROBOT)
(LEFTOF7-ROBOT)
(LEFTOF6-ROBOT)
(LEFTOF5-ROBOT)
(LEFTOF4-ROBOT)
(LEFTOF3-ROBOT)
(LEFTOF2-ROBOT)
(RIGHTOF13-ROBOT)
(RIGHTOF12-ROBOT)
(RIGHTOF11-ROBOT)
(RIGHTOF10-ROBOT)
(RIGHTOF9-ROBOT)
(RIGHTOF8-ROBOT)
(RIGHTOF7-ROBOT)
(RIGHTOF6-ROBOT)
(RIGHTOF5-ROBOT)
(RIGHTOF4-ROBOT)
(RIGHTOF3-ROBOT)
(RIGHTOF1-ROBOT)
(COLUMN0-ROBOT)
(NOT-RIGHTOF2-ROBOT)
(RIGHTOF0-ROBOT)
(LEFTOF1-ROBOT)
(Flag313-)
(Flag310-)
(Flag309-)
(Flag308-)
(Flag306-)
(Flag305-)
(Flag304-)
(Flag302-)
(Flag301-)
(Flag300-)
(Flag299-)
(Flag297-)
(Flag296-)
(Flag295-)
(Flag294-)
(Flag293-)
(Flag291-)
(Flag290-)
(Flag289-)
(Flag288-)
(Flag287-)
(Flag286-)
(Flag284-)
(Flag283-)
(Flag282-)
(Flag281-)
(Flag280-)
(Flag279-)
(Flag278-)
(Flag277-)
(Flag275-)
(Flag274-)
(Flag273-)
(Flag272-)
(Flag271-)
(Flag270-)
(Flag269-)
(Flag268-)
(Flag267-)
(Flag266-)
(Flag264-)
(Flag263-)
(Flag262-)
(Flag261-)
(Flag260-)
(Flag259-)
(Flag258-)
(Flag257-)
(Flag256-)
(Flag254-)
(Flag253-)
(Flag252-)
(Flag251-)
(Flag250-)
(Flag249-)
(Flag248-)
(Flag247-)
(Flag246-)
(Flag245-)
(Flag244-)
(Flag243-)
(Flag241-)
(Flag240-)
(Flag239-)
(Flag238-)
(Flag237-)
(Flag236-)
(Flag235-)
(Flag234-)
(Flag233-)
(Flag232-)
(Flag231-)
(Flag230-)
(Flag229-)
(Flag227-)
(Flag226-)
(Flag225-)
(Flag224-)
(Flag223-)
(Flag222-)
(Flag221-)
(Flag220-)
(Flag219-)
(Flag218-)
(Flag217-)
(Flag216-)
(Flag214-)
(Flag213-)
(Flag212-)
(Flag211-)
(Flag210-)
(Flag209-)
(Flag208-)
(Flag207-)
(Flag206-)
(Flag205-)
(Flag204-)
(Flag203-)
(Flag202-)
(Flag201-)
(Flag199-)
(Flag198-)
(Flag197-)
(Flag196-)
(Flag195-)
(Flag194-)
(Flag193-)
(Flag192-)
(Flag191-)
(Flag190-)
(Flag189-)
(Flag188-)
(Flag187-)
(Flag186-)
(Flag184-)
(Flag183-)
(Flag182-)
(Flag181-)
(Flag180-)
(Flag179-)
(Flag178-)
(Flag177-)
(Flag176-)
(Flag175-)
(Flag174-)
(Flag173-)
(Flag172-)
(Flag171-)
(Flag170-)
(Flag169-)
(Flag168-)
(Flag167-)
(Flag166-)
(Flag165-)
(Flag164-)
(Flag160-)
(Flag156-)
(Flag152-)
(Flag148-)
(Flag143-)
(Flag136-)
(Flag127-)
(Flag118-)
(Flag107-)
(Flag95-)
(Flag84-)
(Flag70-)
(Flag56-)
(Flag1-)
(Flag314-)
(Flag311-)
(Flag307-)
(Flag303-)
(Flag298-)
(Flag292-)
(Flag285-)
(Flag276-)
(Flag265-)
(Flag255-)
(Flag242-)
(Flag228-)
(Flag215-)
(Flag200-)
(NOT-COLUMN12-ROBOT)
(NOT-COLUMN11-ROBOT)
(NOT-COLUMN10-ROBOT)
(NOT-COLUMN9-ROBOT)
(NOT-COLUMN8-ROBOT)
(NOT-COLUMN7-ROBOT)
(NOT-COLUMN6-ROBOT)
(NOT-COLUMN5-ROBOT)
(NOT-COLUMN4-ROBOT)
(NOT-COLUMN3-ROBOT)
(NOT-COLUMN2-ROBOT)
(NOT-COLUMN1-ROBOT)
(NOT-COLUMN0-ROBOT)
(NOT-RIGHTOF12-ROBOT)
(NOT-RIGHTOF11-ROBOT)
(NOT-RIGHTOF10-ROBOT)
(NOT-RIGHTOF9-ROBOT)
(NOT-RIGHTOF8-ROBOT)
(NOT-RIGHTOF7-ROBOT)
(NOT-RIGHTOF6-ROBOT)
(NOT-RIGHTOF5-ROBOT)
(NOT-RIGHTOF4-ROBOT)
(NOT-RIGHTOF3-ROBOT)
(NOT-RIGHTOF1-ROBOT)
(RIGHTOF2-ROBOT)
(NOT-RIGHTOF0-ROBOT)
(NOT-LEFTOF12-ROBOT)
(NOT-LEFTOF11-ROBOT)
(NOT-LEFTOF10-ROBOT)
(NOT-LEFTOF9-ROBOT)
(NOT-LEFTOF8-ROBOT)
(NOT-LEFTOF7-ROBOT)
(NOT-LEFTOF6-ROBOT)
(NOT-LEFTOF5-ROBOT)
(NOT-LEFTOF4-ROBOT)
(NOT-LEFTOF3-ROBOT)
(NOT-LEFTOF2-ROBOT)
(NOT-LEFTOF1-ROBOT)
(NOT-CHECKCONSISTENCY-)
(NOT-COLUMN13-ROBOT)
(NOT-LEFTOF14-ROBOT)
(LEFTOF13-ROBOT)
(NOT-RIGHTOF13-ROBOT)
(NOT-ERROR-)
(NOT-ROW12-ROBOT)
(NOT-ROW11-ROBOT)
(NOT-ROW10-ROBOT)
(NOT-ROW9-ROBOT)
(NOT-ROW8-ROBOT)
(NOT-ROW7-ROBOT)
(NOT-ROW6-ROBOT)
(NOT-ROW5-ROBOT)
(NOT-ROW4-ROBOT)
(NOT-ROW3-ROBOT)
(NOT-ROW2-ROBOT)
(NOT-ROW1-ROBOT)
(NOT-ROW0-ROBOT)
(NOT-ABOVEOF12-ROBOT)
(NOT-ABOVEOF11-ROBOT)
(NOT-ABOVEOF10-ROBOT)
(NOT-ABOVEOF9-ROBOT)
(NOT-ABOVEOF8-ROBOT)
(NOT-ABOVEOF7-ROBOT)
(NOT-ABOVEOF6-ROBOT)
(NOT-ABOVEOF5-ROBOT)
(NOT-ABOVEOF4-ROBOT)
(NOT-ABOVEOF3-ROBOT)
(NOT-ABOVEOF2-ROBOT)
(NOT-ABOVEOF1-ROBOT)
(NOT-BELOWOF12-ROBOT)
(NOT-BELOWOF11-ROBOT)
(NOT-BELOWOF10-ROBOT)
(NOT-BELOWOF9-ROBOT)
(NOT-BELOWOF8-ROBOT)
(NOT-BELOWOF7-ROBOT)
(NOT-BELOWOF6-ROBOT)
(NOT-BELOWOF5-ROBOT)
(NOT-BELOWOF4-ROBOT)
(NOT-BELOWOF3-ROBOT)
(NOT-BELOWOF2-ROBOT)
(NOT-BELOWOF1-ROBOT)
(NOT-ROW13-ROBOT)
(NOT-BELOWOF14-ROBOT)
(BELOWOF13-ROBOT)
(NOT-ABOVEOF13-ROBOT)
(Flag312prime-)
(Flag185prime-)
(Flag163prime-)
(Flag162prime-)
(Flag161prime-)
(Flag157prime-)
(Flag28prime-)
(Flag4prime-)
(Flag2prime-)
(Flag159prime-)
(Flag158prime-)
(Flag155prime-)
(Flag154prime-)
(Flag153prime-)
(Flag151prime-)
(Flag150prime-)
(Flag149prime-)
(Flag147prime-)
(Flag146prime-)
(Flag145prime-)
(Flag144prime-)
(Flag142prime-)
(Flag141prime-)
(Flag140prime-)
(Flag139prime-)
(Flag138prime-)
(Flag137prime-)
(Flag135prime-)
(Flag134prime-)
(Flag133prime-)
(Flag132prime-)
(Flag131prime-)
(Flag130prime-)
(Flag129prime-)
(Flag128prime-)
(Flag126prime-)
(Flag125prime-)
(Flag124prime-)
(Flag123prime-)
(Flag122prime-)
(Flag121prime-)
(Flag120prime-)
(Flag119prime-)
(Flag117prime-)
(Flag116prime-)
(Flag115prime-)
(Flag114prime-)
(Flag113prime-)
(Flag112prime-)
(Flag111prime-)
(Flag110prime-)
(Flag109prime-)
(Flag108prime-)
(Flag106prime-)
(Flag105prime-)
(Flag104prime-)
(Flag103prime-)
(Flag102prime-)
(Flag101prime-)
(Flag100prime-)
(Flag99prime-)
(Flag98prime-)
(Flag97prime-)
(Flag96prime-)
(Flag94prime-)
(Flag93prime-)
(Flag92prime-)
(Flag91prime-)
(Flag90prime-)
(Flag89prime-)
(Flag88prime-)
(Flag87prime-)
(Flag86prime-)
(Flag85prime-)
(Flag83prime-)
(Flag82prime-)
(Flag81prime-)
(Flag80prime-)
(Flag79prime-)
(Flag78prime-)
(Flag77prime-)
(Flag76prime-)
(Flag75prime-)
(Flag74prime-)
(Flag73prime-)
(Flag72prime-)
(Flag71prime-)
(Flag69prime-)
(Flag68prime-)
(Flag67prime-)
(Flag66prime-)
(Flag65prime-)
(Flag64prime-)
(Flag63prime-)
(Flag62prime-)
(Flag61prime-)
(Flag60prime-)
(Flag59prime-)
(Flag58prime-)
(Flag57prime-)
(Flag55prime-)
(Flag54prime-)
(Flag53prime-)
(Flag52prime-)
(Flag51prime-)
(Flag50prime-)
(Flag49prime-)
(Flag48prime-)
(Flag47prime-)
(Flag46prime-)
(Flag45prime-)
(Flag44prime-)
(Flag43prime-)
(Flag42prime-)
(Flag41prime-)
(Flag40prime-)
(Flag39prime-)
(Flag38prime-)
(Flag37prime-)
(Flag36prime-)
(Flag35prime-)
(Flag34prime-)
(Flag33prime-)
(Flag32prime-)
(Flag31prime-)
(Flag30prime-)
(Flag29prime-)
(Flag27prime-)
(Flag26prime-)
(Flag25prime-)
(Flag24prime-)
(Flag23prime-)
(Flag22prime-)
(Flag21prime-)
(Flag20prime-)
(Flag19prime-)
(Flag18prime-)
(Flag17prime-)
(Flag16prime-)
(Flag15prime-)
(Flag14prime-)
(Flag13prime-)
(Flag12prime-)
(Flag11prime-)
(Flag10prime-)
(Flag9prime-)
(Flag8prime-)
(Flag7prime-)
(Flag6prime-)
(Flag5prime-)
(Flag3prime-)
(Flag313prime-)
(Flag310prime-)
(Flag309prime-)
(Flag308prime-)
(Flag306prime-)
(Flag305prime-)
(Flag304prime-)
(Flag302prime-)
(Flag301prime-)
(Flag300prime-)
(Flag299prime-)
(Flag297prime-)
(Flag296prime-)
(Flag295prime-)
(Flag294prime-)
(Flag293prime-)
(Flag291prime-)
(Flag290prime-)
(Flag289prime-)
(Flag288prime-)
(Flag287prime-)
(Flag286prime-)
(Flag284prime-)
(Flag283prime-)
(Flag282prime-)
(Flag281prime-)
(Flag280prime-)
(Flag279prime-)
(Flag278prime-)
(Flag277prime-)
(Flag275prime-)
(Flag274prime-)
(Flag273prime-)
(Flag272prime-)
(Flag271prime-)
(Flag270prime-)
(Flag269prime-)
(Flag268prime-)
(Flag267prime-)
(Flag266prime-)
(Flag264prime-)
(Flag263prime-)
(Flag262prime-)
(Flag261prime-)
(Flag260prime-)
(Flag259prime-)
(Flag258prime-)
(Flag257prime-)
(Flag256prime-)
(Flag254prime-)
(Flag253prime-)
(Flag252prime-)
(Flag251prime-)
(Flag250prime-)
(Flag249prime-)
(Flag248prime-)
(Flag247prime-)
(Flag246prime-)
(Flag245prime-)
(Flag244prime-)
(Flag243prime-)
(Flag241prime-)
(Flag240prime-)
(Flag239prime-)
(Flag238prime-)
(Flag237prime-)
(Flag236prime-)
(Flag235prime-)
(Flag234prime-)
(Flag233prime-)
(Flag232prime-)
(Flag231prime-)
(Flag230prime-)
(Flag229prime-)
(Flag227prime-)
(Flag226prime-)
(Flag225prime-)
(Flag224prime-)
(Flag223prime-)
(Flag222prime-)
(Flag221prime-)
(Flag220prime-)
(Flag219prime-)
(Flag218prime-)
(Flag217prime-)
(Flag216prime-)
(Flag214prime-)
(Flag213prime-)
(Flag212prime-)
(Flag211prime-)
(Flag210prime-)
(Flag209prime-)
(Flag208prime-)
(Flag207prime-)
(Flag206prime-)
(Flag205prime-)
(Flag204prime-)
(Flag203prime-)
(Flag202prime-)
(Flag201prime-)
(Flag199prime-)
(Flag198prime-)
(Flag197prime-)
(Flag196prime-)
(Flag195prime-)
(Flag194prime-)
(Flag193prime-)
(Flag192prime-)
(Flag191prime-)
(Flag190prime-)
(Flag189prime-)
(Flag188prime-)
(Flag187prime-)
(Flag186prime-)
(Flag184prime-)
(Flag183prime-)
(Flag182prime-)
(Flag181prime-)
(Flag180prime-)
(Flag179prime-)
(Flag178prime-)
(Flag177prime-)
(Flag176prime-)
(Flag175prime-)
(Flag174prime-)
(Flag173prime-)
(Flag172prime-)
(Flag171prime-)
(Flag170prime-)
(Flag169prime-)
(Flag168prime-)
(Flag167prime-)
(Flag166prime-)
(Flag165prime-)
(Flag164prime-)
(Flag160prime-)
(Flag156prime-)
(Flag152prime-)
(Flag148prime-)
(Flag143prime-)
(Flag136prime-)
(Flag127prime-)
(Flag118prime-)
(Flag107prime-)
(Flag95prime-)
(Flag84prime-)
(Flag70prime-)
(Flag56prime-)
(Flag1prime-)
(Flag314prime-)
(Flag311prime-)
(Flag307prime-)
(Flag303prime-)
(Flag298prime-)
(Flag292prime-)
(Flag285prime-)
(Flag276prime-)
(Flag265prime-)
(Flag255prime-)
(Flag242prime-)
(Flag228prime-)
(Flag215prime-)
(Flag200prime-)
)
(:action Flag200Action-0
:parameters ()
:precondition
(and
(Flag186-)
(Flag186prime-)
)
:effect
(and
(Flag200-)
(Flag200prime-)
)

)
(:action Flag200Action-1
:parameters ()
:precondition
(and
(Flag187-)
(Flag187prime-)
)
:effect
(and
(Flag200-)
(Flag200prime-)
)

)
(:action Flag200Action-2
:parameters ()
:precondition
(and
(Flag188-)
(Flag188prime-)
)
:effect
(and
(Flag200-)
(Flag200prime-)
)

)
(:action Flag200Action-3
:parameters ()
:precondition
(and
(Flag189-)
(Flag189prime-)
)
:effect
(and
(Flag200-)
(Flag200prime-)
)

)
(:action Flag200Action-4
:parameters ()
:precondition
(and
(Flag190-)
(Flag190prime-)
)
:effect
(and
(Flag200-)
(Flag200prime-)
)

)
(:action Flag200Action-5
:parameters ()
:precondition
(and
(Flag191-)
(Flag191prime-)
)
:effect
(and
(Flag200-)
(Flag200prime-)
)

)
(:action Flag200Action-6
:parameters ()
:precondition
(and
(Flag192-)
(Flag192prime-)
)
:effect
(and
(Flag200-)
(Flag200prime-)
)

)
(:action Flag200Action-7
:parameters ()
:precondition
(and
(Flag193-)
(Flag193prime-)
)
:effect
(and
(Flag200-)
(Flag200prime-)
)

)
(:action Flag200Action-8
:parameters ()
:precondition
(and
(Flag194-)
(Flag194prime-)
)
:effect
(and
(Flag200-)
(Flag200prime-)
)

)
(:action Flag200Action-9
:parameters ()
:precondition
(and
(Flag195-)
(Flag195prime-)
)
:effect
(and
(Flag200-)
(Flag200prime-)
)

)
(:action Flag200Action-10
:parameters ()
:precondition
(and
(Flag196-)
(Flag196prime-)
)
:effect
(and
(Flag200-)
(Flag200prime-)
)

)
(:action Flag200Action-11
:parameters ()
:precondition
(and
(Flag197-)
(Flag197prime-)
)
:effect
(and
(Flag200-)
(Flag200prime-)
)

)
(:action Flag200Action-12
:parameters ()
:precondition
(and
(Flag198-)
(Flag198prime-)
)
:effect
(and
(Flag200-)
(Flag200prime-)
)

)
(:action Flag200Action-13
:parameters ()
:precondition
(and
(Flag199-)
(Flag199prime-)
)
:effect
(and
(Flag200-)
(Flag200prime-)
)

)
(:action Prim200Action
:parameters ()
:precondition
(and
(not (Flag186-))
(Flag186prime-)
(not (Flag187-))
(Flag187prime-)
(not (Flag188-))
(Flag188prime-)
(not (Flag189-))
(Flag189prime-)
(not (Flag190-))
(Flag190prime-)
(not (Flag191-))
(Flag191prime-)
(not (Flag192-))
(Flag192prime-)
(not (Flag193-))
(Flag193prime-)
(not (Flag194-))
(Flag194prime-)
(not (Flag195-))
(Flag195prime-)
(not (Flag196-))
(Flag196prime-)
(not (Flag197-))
(Flag197prime-)
(not (Flag198-))
(Flag198prime-)
(not (Flag199-))
(Flag199prime-)
)
:effect
(and
(Flag200prime-)
(not (Flag200-))
)

)
(:action Flag215Action-0
:parameters ()
:precondition
(and
(Flag201-)
(Flag201prime-)
)
:effect
(and
(Flag215-)
(Flag215prime-)
)

)
(:action Flag215Action-1
:parameters ()
:precondition
(and
(Flag202-)
(Flag202prime-)
)
:effect
(and
(Flag215-)
(Flag215prime-)
)

)
(:action Flag215Action-2
:parameters ()
:precondition
(and
(Flag189-)
(Flag189prime-)
)
:effect
(and
(Flag215-)
(Flag215prime-)
)

)
(:action Flag215Action-3
:parameters ()
:precondition
(and
(Flag192-)
(Flag192prime-)
)
:effect
(and
(Flag215-)
(Flag215prime-)
)

)
(:action Flag215Action-4
:parameters ()
:precondition
(and
(Flag199-)
(Flag199prime-)
)
:effect
(and
(Flag215-)
(Flag215prime-)
)

)
(:action Flag215Action-5
:parameters ()
:precondition
(and
(Flag203-)
(Flag203prime-)
)
:effect
(and
(Flag215-)
(Flag215prime-)
)

)
(:action Flag215Action-6
:parameters ()
:precondition
(and
(Flag204-)
(Flag204prime-)
)
:effect
(and
(Flag215-)
(Flag215prime-)
)

)
(:action Flag215Action-7
:parameters ()
:precondition
(and
(Flag191-)
(Flag191prime-)
)
:effect
(and
(Flag215-)
(Flag215prime-)
)

)
(:action Flag215Action-8
:parameters ()
:precondition
(and
(Flag205-)
(Flag205prime-)
)
:effect
(and
(Flag215-)
(Flag215prime-)
)

)
(:action Flag215Action-9
:parameters ()
:precondition
(and
(Flag206-)
(Flag206prime-)
)
:effect
(and
(Flag215-)
(Flag215prime-)
)

)
(:action Flag215Action-10
:parameters ()
:precondition
(and
(Flag196-)
(Flag196prime-)
)
:effect
(and
(Flag215-)
(Flag215prime-)
)

)
(:action Flag215Action-11
:parameters ()
:precondition
(and
(Flag207-)
(Flag207prime-)
)
:effect
(and
(Flag215-)
(Flag215prime-)
)

)
(:action Flag215Action-12
:parameters ()
:precondition
(and
(Flag186-)
(Flag186prime-)
)
:effect
(and
(Flag215-)
(Flag215prime-)
)

)
(:action Flag215Action-13
:parameters ()
:precondition
(and
(Flag208-)
(Flag208prime-)
)
:effect
(and
(Flag215-)
(Flag215prime-)
)

)
(:action Flag215Action-14
:parameters ()
:precondition
(and
(Flag187-)
(Flag187prime-)
)
:effect
(and
(Flag215-)
(Flag215prime-)
)

)
(:action Flag215Action-15
:parameters ()
:precondition
(and
(Flag188-)
(Flag188prime-)
)
:effect
(and
(Flag215-)
(Flag215prime-)
)

)
(:action Flag215Action-16
:parameters ()
:precondition
(and
(Flag209-)
(Flag209prime-)
)
:effect
(and
(Flag215-)
(Flag215prime-)
)

)
(:action Flag215Action-17
:parameters ()
:precondition
(and
(Flag194-)
(Flag194prime-)
)
:effect
(and
(Flag215-)
(Flag215prime-)
)

)
(:action Flag215Action-18
:parameters ()
:precondition
(and
(Flag197-)
(Flag197prime-)
)
:effect
(and
(Flag215-)
(Flag215prime-)
)

)
(:action Flag215Action-19
:parameters ()
:precondition
(and
(Flag190-)
(Flag190prime-)
)
:effect
(and
(Flag215-)
(Flag215prime-)
)

)
(:action Flag215Action-20
:parameters ()
:precondition
(and
(Flag210-)
(Flag210prime-)
)
:effect
(and
(Flag215-)
(Flag215prime-)
)

)
(:action Flag215Action-21
:parameters ()
:precondition
(and
(Flag211-)
(Flag211prime-)
)
:effect
(and
(Flag215-)
(Flag215prime-)
)

)
(:action Flag215Action-22
:parameters ()
:precondition
(and
(Flag212-)
(Flag212prime-)
)
:effect
(and
(Flag215-)
(Flag215prime-)
)

)
(:action Flag215Action-23
:parameters ()
:precondition
(and
(Flag195-)
(Flag195prime-)
)
:effect
(and
(Flag215-)
(Flag215prime-)
)

)
(:action Flag215Action-24
:parameters ()
:precondition
(and
(Flag198-)
(Flag198prime-)
)
:effect
(and
(Flag215-)
(Flag215prime-)
)

)
(:action Flag215Action-25
:parameters ()
:precondition
(and
(Flag213-)
(Flag213prime-)
)
:effect
(and
(Flag215-)
(Flag215prime-)
)

)
(:action Flag215Action-26
:parameters ()
:precondition
(and
(Flag214-)
(Flag214prime-)
)
:effect
(and
(Flag215-)
(Flag215prime-)
)

)
(:action Prim215Action
:parameters ()
:precondition
(and
(not (Flag201-))
(Flag201prime-)
(not (Flag202-))
(Flag202prime-)
(not (Flag189-))
(Flag189prime-)
(not (Flag192-))
(Flag192prime-)
(not (Flag199-))
(Flag199prime-)
(not (Flag203-))
(Flag203prime-)
(not (Flag204-))
(Flag204prime-)
(not (Flag191-))
(Flag191prime-)
(not (Flag205-))
(Flag205prime-)
(not (Flag206-))
(Flag206prime-)
(not (Flag196-))
(Flag196prime-)
(not (Flag207-))
(Flag207prime-)
(not (Flag186-))
(Flag186prime-)
(not (Flag208-))
(Flag208prime-)
(not (Flag187-))
(Flag187prime-)
(not (Flag188-))
(Flag188prime-)
(not (Flag209-))
(Flag209prime-)
(not (Flag194-))
(Flag194prime-)
(not (Flag197-))
(Flag197prime-)
(not (Flag190-))
(Flag190prime-)
(not (Flag210-))
(Flag210prime-)
(not (Flag211-))
(Flag211prime-)
(not (Flag212-))
(Flag212prime-)
(not (Flag195-))
(Flag195prime-)
(not (Flag198-))
(Flag198prime-)
(not (Flag213-))
(Flag213prime-)
(not (Flag214-))
(Flag214prime-)
)
:effect
(and
(Flag215prime-)
(not (Flag215-))
)

)
(:action Flag228Action-0
:parameters ()
:precondition
(and
(Flag201-)
(Flag201prime-)
)
:effect
(and
(Flag228-)
(Flag228prime-)
)

)
(:action Flag228Action-1
:parameters ()
:precondition
(and
(Flag202-)
(Flag202prime-)
)
:effect
(and
(Flag228-)
(Flag228prime-)
)

)
(:action Flag228Action-2
:parameters ()
:precondition
(and
(Flag216-)
(Flag216prime-)
)
:effect
(and
(Flag228-)
(Flag228prime-)
)

)
(:action Flag228Action-3
:parameters ()
:precondition
(and
(Flag189-)
(Flag189prime-)
)
:effect
(and
(Flag228-)
(Flag228prime-)
)

)
(:action Flag228Action-4
:parameters ()
:precondition
(and
(Flag192-)
(Flag192prime-)
)
:effect
(and
(Flag228-)
(Flag228prime-)
)

)
(:action Flag228Action-5
:parameters ()
:precondition
(and
(Flag217-)
(Flag217prime-)
)
:effect
(and
(Flag228-)
(Flag228prime-)
)

)
(:action Flag228Action-6
:parameters ()
:precondition
(and
(Flag199-)
(Flag199prime-)
)
:effect
(and
(Flag228-)
(Flag228prime-)
)

)
(:action Flag228Action-7
:parameters ()
:precondition
(and
(Flag204-)
(Flag204prime-)
)
:effect
(and
(Flag228-)
(Flag228prime-)
)

)
(:action Flag228Action-8
:parameters ()
:precondition
(and
(Flag188-)
(Flag188prime-)
)
:effect
(and
(Flag228-)
(Flag228prime-)
)

)
(:action Flag228Action-9
:parameters ()
:precondition
(and
(Flag206-)
(Flag206prime-)
)
:effect
(and
(Flag228-)
(Flag228prime-)
)

)
(:action Flag228Action-10
:parameters ()
:precondition
(and
(Flag218-)
(Flag218prime-)
)
:effect
(and
(Flag228-)
(Flag228prime-)
)

)
(:action Flag228Action-11
:parameters ()
:precondition
(and
(Flag196-)
(Flag196prime-)
)
:effect
(and
(Flag228-)
(Flag228prime-)
)

)
(:action Flag228Action-12
:parameters ()
:precondition
(and
(Flag207-)
(Flag207prime-)
)
:effect
(and
(Flag228-)
(Flag228prime-)
)

)
(:action Flag228Action-13
:parameters ()
:precondition
(and
(Flag194-)
(Flag194prime-)
)
:effect
(and
(Flag228-)
(Flag228prime-)
)

)
(:action Flag228Action-14
:parameters ()
:precondition
(and
(Flag219-)
(Flag219prime-)
)
:effect
(and
(Flag228-)
(Flag228prime-)
)

)
(:action Flag228Action-15
:parameters ()
:precondition
(and
(Flag186-)
(Flag186prime-)
)
:effect
(and
(Flag228-)
(Flag228prime-)
)

)
(:action Flag228Action-16
:parameters ()
:precondition
(and
(Flag208-)
(Flag208prime-)
)
:effect
(and
(Flag228-)
(Flag228prime-)
)

)
(:action Flag228Action-17
:parameters ()
:precondition
(and
(Flag187-)
(Flag187prime-)
)
:effect
(and
(Flag228-)
(Flag228prime-)
)

)
(:action Flag228Action-18
:parameters ()
:precondition
(and
(Flag220-)
(Flag220prime-)
)
:effect
(and
(Flag228-)
(Flag228prime-)
)

)
(:action Flag228Action-19
:parameters ()
:precondition
(and
(Flag221-)
(Flag221prime-)
)
:effect
(and
(Flag228-)
(Flag228prime-)
)

)
(:action Flag228Action-20
:parameters ()
:precondition
(and
(Flag205-)
(Flag205prime-)
)
:effect
(and
(Flag228-)
(Flag228prime-)
)

)
(:action Flag228Action-21
:parameters ()
:precondition
(and
(Flag209-)
(Flag209prime-)
)
:effect
(and
(Flag228-)
(Flag228prime-)
)

)
(:action Flag228Action-22
:parameters ()
:precondition
(and
(Flag222-)
(Flag222prime-)
)
:effect
(and
(Flag228-)
(Flag228prime-)
)

)
(:action Flag228Action-23
:parameters ()
:precondition
(and
(Flag197-)
(Flag197prime-)
)
:effect
(and
(Flag228-)
(Flag228prime-)
)

)
(:action Flag228Action-24
:parameters ()
:precondition
(and
(Flag223-)
(Flag223prime-)
)
:effect
(and
(Flag228-)
(Flag228prime-)
)

)
(:action Flag228Action-25
:parameters ()
:precondition
(and
(Flag224-)
(Flag224prime-)
)
:effect
(and
(Flag228-)
(Flag228prime-)
)

)
(:action Flag228Action-26
:parameters ()
:precondition
(and
(Flag225-)
(Flag225prime-)
)
:effect
(and
(Flag228-)
(Flag228prime-)
)

)
(:action Flag228Action-27
:parameters ()
:precondition
(and
(Flag190-)
(Flag190prime-)
)
:effect
(and
(Flag228-)
(Flag228prime-)
)

)
(:action Flag228Action-28
:parameters ()
:precondition
(and
(Flag210-)
(Flag210prime-)
)
:effect
(and
(Flag228-)
(Flag228prime-)
)

)
(:action Flag228Action-29
:parameters ()
:precondition
(and
(Flag211-)
(Flag211prime-)
)
:effect
(and
(Flag228-)
(Flag228prime-)
)

)
(:action Flag228Action-30
:parameters ()
:precondition
(and
(Flag212-)
(Flag212prime-)
)
:effect
(and
(Flag228-)
(Flag228prime-)
)

)
(:action Flag228Action-31
:parameters ()
:precondition
(and
(Flag195-)
(Flag195prime-)
)
:effect
(and
(Flag228-)
(Flag228prime-)
)

)
(:action Flag228Action-32
:parameters ()
:precondition
(and
(Flag198-)
(Flag198prime-)
)
:effect
(and
(Flag228-)
(Flag228prime-)
)

)
(:action Flag228Action-33
:parameters ()
:precondition
(and
(Flag213-)
(Flag213prime-)
)
:effect
(and
(Flag228-)
(Flag228prime-)
)

)
(:action Flag228Action-34
:parameters ()
:precondition
(and
(Flag226-)
(Flag226prime-)
)
:effect
(and
(Flag228-)
(Flag228prime-)
)

)
(:action Flag228Action-35
:parameters ()
:precondition
(and
(Flag214-)
(Flag214prime-)
)
:effect
(and
(Flag228-)
(Flag228prime-)
)

)
(:action Flag228Action-36
:parameters ()
:precondition
(and
(Flag227-)
(Flag227prime-)
)
:effect
(and
(Flag228-)
(Flag228prime-)
)

)
(:action Prim228Action
:parameters ()
:precondition
(and
(not (Flag201-))
(Flag201prime-)
(not (Flag202-))
(Flag202prime-)
(not (Flag216-))
(Flag216prime-)
(not (Flag189-))
(Flag189prime-)
(not (Flag192-))
(Flag192prime-)
(not (Flag217-))
(Flag217prime-)
(not (Flag199-))
(Flag199prime-)
(not (Flag204-))
(Flag204prime-)
(not (Flag188-))
(Flag188prime-)
(not (Flag206-))
(Flag206prime-)
(not (Flag218-))
(Flag218prime-)
(not (Flag196-))
(Flag196prime-)
(not (Flag207-))
(Flag207prime-)
(not (Flag194-))
(Flag194prime-)
(not (Flag219-))
(Flag219prime-)
(not (Flag186-))
(Flag186prime-)
(not (Flag208-))
(Flag208prime-)
(not (Flag187-))
(Flag187prime-)
(not (Flag220-))
(Flag220prime-)
(not (Flag221-))
(Flag221prime-)
(not (Flag205-))
(Flag205prime-)
(not (Flag209-))
(Flag209prime-)
(not (Flag222-))
(Flag222prime-)
(not (Flag197-))
(Flag197prime-)
(not (Flag223-))
(Flag223prime-)
(not (Flag224-))
(Flag224prime-)
(not (Flag225-))
(Flag225prime-)
(not (Flag190-))
(Flag190prime-)
(not (Flag210-))
(Flag210prime-)
(not (Flag211-))
(Flag211prime-)
(not (Flag212-))
(Flag212prime-)
(not (Flag195-))
(Flag195prime-)
(not (Flag198-))
(Flag198prime-)
(not (Flag213-))
(Flag213prime-)
(not (Flag226-))
(Flag226prime-)
(not (Flag214-))
(Flag214prime-)
(not (Flag227-))
(Flag227prime-)
)
:effect
(and
(Flag228prime-)
(not (Flag228-))
)

)
(:action Flag242Action-0
:parameters ()
:precondition
(and
(Flag201-)
(Flag201prime-)
)
:effect
(and
(Flag242-)
(Flag242prime-)
)

)
(:action Flag242Action-1
:parameters ()
:precondition
(and
(Flag202-)
(Flag202prime-)
)
:effect
(and
(Flag242-)
(Flag242prime-)
)

)
(:action Flag242Action-2
:parameters ()
:precondition
(and
(Flag216-)
(Flag216prime-)
)
:effect
(and
(Flag242-)
(Flag242prime-)
)

)
(:action Flag242Action-3
:parameters ()
:precondition
(and
(Flag189-)
(Flag189prime-)
)
:effect
(and
(Flag242-)
(Flag242prime-)
)

)
(:action Flag242Action-4
:parameters ()
:precondition
(and
(Flag192-)
(Flag192prime-)
)
:effect
(and
(Flag242-)
(Flag242prime-)
)

)
(:action Flag242Action-5
:parameters ()
:precondition
(and
(Flag229-)
(Flag229prime-)
)
:effect
(and
(Flag242-)
(Flag242prime-)
)

)
(:action Flag242Action-6
:parameters ()
:precondition
(and
(Flag217-)
(Flag217prime-)
)
:effect
(and
(Flag242-)
(Flag242prime-)
)

)
(:action Flag242Action-7
:parameters ()
:precondition
(and
(Flag199-)
(Flag199prime-)
)
:effect
(and
(Flag242-)
(Flag242prime-)
)

)
(:action Flag242Action-8
:parameters ()
:precondition
(and
(Flag230-)
(Flag230prime-)
)
:effect
(and
(Flag242-)
(Flag242prime-)
)

)
(:action Flag242Action-9
:parameters ()
:precondition
(and
(Flag231-)
(Flag231prime-)
)
:effect
(and
(Flag242-)
(Flag242prime-)
)

)
(:action Flag242Action-10
:parameters ()
:precondition
(and
(Flag204-)
(Flag204prime-)
)
:effect
(and
(Flag242-)
(Flag242prime-)
)

)
(:action Flag242Action-11
:parameters ()
:precondition
(and
(Flag221-)
(Flag221prime-)
)
:effect
(and
(Flag242-)
(Flag242prime-)
)

)
(:action Flag242Action-12
:parameters ()
:precondition
(and
(Flag188-)
(Flag188prime-)
)
:effect
(and
(Flag242-)
(Flag242prime-)
)

)
(:action Flag242Action-13
:parameters ()
:precondition
(and
(Flag206-)
(Flag206prime-)
)
:effect
(and
(Flag242-)
(Flag242prime-)
)

)
(:action Flag242Action-14
:parameters ()
:precondition
(and
(Flag218-)
(Flag218prime-)
)
:effect
(and
(Flag242-)
(Flag242prime-)
)

)
(:action Flag242Action-15
:parameters ()
:precondition
(and
(Flag196-)
(Flag196prime-)
)
:effect
(and
(Flag242-)
(Flag242prime-)
)

)
(:action Flag242Action-16
:parameters ()
:precondition
(and
(Flag207-)
(Flag207prime-)
)
:effect
(and
(Flag242-)
(Flag242prime-)
)

)
(:action Flag242Action-17
:parameters ()
:precondition
(and
(Flag194-)
(Flag194prime-)
)
:effect
(and
(Flag242-)
(Flag242prime-)
)

)
(:action Flag242Action-18
:parameters ()
:precondition
(and
(Flag219-)
(Flag219prime-)
)
:effect
(and
(Flag242-)
(Flag242prime-)
)

)
(:action Flag242Action-19
:parameters ()
:precondition
(and
(Flag186-)
(Flag186prime-)
)
:effect
(and
(Flag242-)
(Flag242prime-)
)

)
(:action Flag242Action-20
:parameters ()
:precondition
(and
(Flag208-)
(Flag208prime-)
)
:effect
(and
(Flag242-)
(Flag242prime-)
)

)
(:action Flag242Action-21
:parameters ()
:precondition
(and
(Flag187-)
(Flag187prime-)
)
:effect
(and
(Flag242-)
(Flag242prime-)
)

)
(:action Flag242Action-22
:parameters ()
:precondition
(and
(Flag220-)
(Flag220prime-)
)
:effect
(and
(Flag242-)
(Flag242prime-)
)

)
(:action Flag242Action-23
:parameters ()
:precondition
(and
(Flag232-)
(Flag232prime-)
)
:effect
(and
(Flag242-)
(Flag242prime-)
)

)
(:action Flag242Action-24
:parameters ()
:precondition
(and
(Flag205-)
(Flag205prime-)
)
:effect
(and
(Flag242-)
(Flag242prime-)
)

)
(:action Flag242Action-25
:parameters ()
:precondition
(and
(Flag209-)
(Flag209prime-)
)
:effect
(and
(Flag242-)
(Flag242prime-)
)

)
(:action Flag242Action-26
:parameters ()
:precondition
(and
(Flag233-)
(Flag233prime-)
)
:effect
(and
(Flag242-)
(Flag242prime-)
)

)
(:action Flag242Action-27
:parameters ()
:precondition
(and
(Flag222-)
(Flag222prime-)
)
:effect
(and
(Flag242-)
(Flag242prime-)
)

)
(:action Flag242Action-28
:parameters ()
:precondition
(and
(Flag197-)
(Flag197prime-)
)
:effect
(and
(Flag242-)
(Flag242prime-)
)

)
(:action Flag242Action-29
:parameters ()
:precondition
(and
(Flag224-)
(Flag224prime-)
)
:effect
(and
(Flag242-)
(Flag242prime-)
)

)
(:action Flag242Action-30
:parameters ()
:precondition
(and
(Flag234-)
(Flag234prime-)
)
:effect
(and
(Flag242-)
(Flag242prime-)
)

)
(:action Flag242Action-31
:parameters ()
:precondition
(and
(Flag235-)
(Flag235prime-)
)
:effect
(and
(Flag242-)
(Flag242prime-)
)

)
(:action Flag242Action-32
:parameters ()
:precondition
(and
(Flag225-)
(Flag225prime-)
)
:effect
(and
(Flag242-)
(Flag242prime-)
)

)
(:action Flag242Action-33
:parameters ()
:precondition
(and
(Flag236-)
(Flag236prime-)
)
:effect
(and
(Flag242-)
(Flag242prime-)
)

)
(:action Flag242Action-34
:parameters ()
:precondition
(and
(Flag190-)
(Flag190prime-)
)
:effect
(and
(Flag242-)
(Flag242prime-)
)

)
(:action Flag242Action-35
:parameters ()
:precondition
(and
(Flag210-)
(Flag210prime-)
)
:effect
(and
(Flag242-)
(Flag242prime-)
)

)
(:action Flag242Action-36
:parameters ()
:precondition
(and
(Flag237-)
(Flag237prime-)
)
:effect
(and
(Flag242-)
(Flag242prime-)
)

)
(:action Flag242Action-37
:parameters ()
:precondition
(and
(Flag238-)
(Flag238prime-)
)
:effect
(and
(Flag242-)
(Flag242prime-)
)

)
(:action Flag242Action-38
:parameters ()
:precondition
(and
(Flag211-)
(Flag211prime-)
)
:effect
(and
(Flag242-)
(Flag242prime-)
)

)
(:action Flag242Action-39
:parameters ()
:precondition
(and
(Flag239-)
(Flag239prime-)
)
:effect
(and
(Flag242-)
(Flag242prime-)
)

)
(:action Flag242Action-40
:parameters ()
:precondition
(and
(Flag198-)
(Flag198prime-)
)
:effect
(and
(Flag242-)
(Flag242prime-)
)

)
(:action Flag242Action-41
:parameters ()
:precondition
(and
(Flag240-)
(Flag240prime-)
)
:effect
(and
(Flag242-)
(Flag242prime-)
)

)
(:action Flag242Action-42
:parameters ()
:precondition
(and
(Flag241-)
(Flag241prime-)
)
:effect
(and
(Flag242-)
(Flag242prime-)
)

)
(:action Flag242Action-43
:parameters ()
:precondition
(and
(Flag213-)
(Flag213prime-)
)
:effect
(and
(Flag242-)
(Flag242prime-)
)

)
(:action Flag242Action-44
:parameters ()
:precondition
(and
(Flag226-)
(Flag226prime-)
)
:effect
(and
(Flag242-)
(Flag242prime-)
)

)
(:action Flag242Action-45
:parameters ()
:precondition
(and
(Flag214-)
(Flag214prime-)
)
:effect
(and
(Flag242-)
(Flag242prime-)
)

)
(:action Prim242Action
:parameters ()
:precondition
(and
(not (Flag201-))
(Flag201prime-)
(not (Flag202-))
(Flag202prime-)
(not (Flag216-))
(Flag216prime-)
(not (Flag189-))
(Flag189prime-)
(not (Flag192-))
(Flag192prime-)
(not (Flag229-))
(Flag229prime-)
(not (Flag217-))
(Flag217prime-)
(not (Flag199-))
(Flag199prime-)
(not (Flag230-))
(Flag230prime-)
(not (Flag231-))
(Flag231prime-)
(not (Flag204-))
(Flag204prime-)
(not (Flag221-))
(Flag221prime-)
(not (Flag188-))
(Flag188prime-)
(not (Flag206-))
(Flag206prime-)
(not (Flag218-))
(Flag218prime-)
(not (Flag196-))
(Flag196prime-)
(not (Flag207-))
(Flag207prime-)
(not (Flag194-))
(Flag194prime-)
(not (Flag219-))
(Flag219prime-)
(not (Flag186-))
(Flag186prime-)
(not (Flag208-))
(Flag208prime-)
(not (Flag187-))
(Flag187prime-)
(not (Flag220-))
(Flag220prime-)
(not (Flag232-))
(Flag232prime-)
(not (Flag205-))
(Flag205prime-)
(not (Flag209-))
(Flag209prime-)
(not (Flag233-))
(Flag233prime-)
(not (Flag222-))
(Flag222prime-)
(not (Flag197-))
(Flag197prime-)
(not (Flag224-))
(Flag224prime-)
(not (Flag234-))
(Flag234prime-)
(not (Flag235-))
(Flag235prime-)
(not (Flag225-))
(Flag225prime-)
(not (Flag236-))
(Flag236prime-)
(not (Flag190-))
(Flag190prime-)
(not (Flag210-))
(Flag210prime-)
(not (Flag237-))
(Flag237prime-)
(not (Flag238-))
(Flag238prime-)
(not (Flag211-))
(Flag211prime-)
(not (Flag239-))
(Flag239prime-)
(not (Flag198-))
(Flag198prime-)
(not (Flag240-))
(Flag240prime-)
(not (Flag241-))
(Flag241prime-)
(not (Flag213-))
(Flag213prime-)
(not (Flag226-))
(Flag226prime-)
(not (Flag214-))
(Flag214prime-)
)
:effect
(and
(Flag242prime-)
(not (Flag242-))
)

)
(:action Flag255Action-0
:parameters ()
:precondition
(and
(Flag243-)
(Flag243prime-)
)
:effect
(and
(Flag255-)
(Flag255prime-)
)

)
(:action Flag255Action-1
:parameters ()
:precondition
(and
(Flag201-)
(Flag201prime-)
)
:effect
(and
(Flag255-)
(Flag255prime-)
)

)
(:action Flag255Action-2
:parameters ()
:precondition
(and
(Flag202-)
(Flag202prime-)
)
:effect
(and
(Flag255-)
(Flag255prime-)
)

)
(:action Flag255Action-3
:parameters ()
:precondition
(and
(Flag216-)
(Flag216prime-)
)
:effect
(and
(Flag255-)
(Flag255prime-)
)

)
(:action Flag255Action-4
:parameters ()
:precondition
(and
(Flag189-)
(Flag189prime-)
)
:effect
(and
(Flag255-)
(Flag255prime-)
)

)
(:action Flag255Action-5
:parameters ()
:precondition
(and
(Flag229-)
(Flag229prime-)
)
:effect
(and
(Flag255-)
(Flag255prime-)
)

)
(:action Flag255Action-6
:parameters ()
:precondition
(and
(Flag244-)
(Flag244prime-)
)
:effect
(and
(Flag255-)
(Flag255prime-)
)

)
(:action Flag255Action-7
:parameters ()
:precondition
(and
(Flag217-)
(Flag217prime-)
)
:effect
(and
(Flag255-)
(Flag255prime-)
)

)
(:action Flag255Action-8
:parameters ()
:precondition
(and
(Flag199-)
(Flag199prime-)
)
:effect
(and
(Flag255-)
(Flag255prime-)
)

)
(:action Flag255Action-9
:parameters ()
:precondition
(and
(Flag245-)
(Flag245prime-)
)
:effect
(and
(Flag255-)
(Flag255prime-)
)

)
(:action Flag255Action-10
:parameters ()
:precondition
(and
(Flag230-)
(Flag230prime-)
)
:effect
(and
(Flag255-)
(Flag255prime-)
)

)
(:action Flag255Action-11
:parameters ()
:precondition
(and
(Flag246-)
(Flag246prime-)
)
:effect
(and
(Flag255-)
(Flag255prime-)
)

)
(:action Flag255Action-12
:parameters ()
:precondition
(and
(Flag231-)
(Flag231prime-)
)
:effect
(and
(Flag255-)
(Flag255prime-)
)

)
(:action Flag255Action-13
:parameters ()
:precondition
(and
(Flag204-)
(Flag204prime-)
)
:effect
(and
(Flag255-)
(Flag255prime-)
)

)
(:action Flag255Action-14
:parameters ()
:precondition
(and
(Flag247-)
(Flag247prime-)
)
:effect
(and
(Flag255-)
(Flag255prime-)
)

)
(:action Flag255Action-15
:parameters ()
:precondition
(and
(Flag205-)
(Flag205prime-)
)
:effect
(and
(Flag255-)
(Flag255prime-)
)

)
(:action Flag255Action-16
:parameters ()
:precondition
(and
(Flag206-)
(Flag206prime-)
)
:effect
(and
(Flag255-)
(Flag255prime-)
)

)
(:action Flag255Action-17
:parameters ()
:precondition
(and
(Flag218-)
(Flag218prime-)
)
:effect
(and
(Flag255-)
(Flag255prime-)
)

)
(:action Flag255Action-18
:parameters ()
:precondition
(and
(Flag196-)
(Flag196prime-)
)
:effect
(and
(Flag255-)
(Flag255prime-)
)

)
(:action Flag255Action-19
:parameters ()
:precondition
(and
(Flag248-)
(Flag248prime-)
)
:effect
(and
(Flag255-)
(Flag255prime-)
)

)
(:action Flag255Action-20
:parameters ()
:precondition
(and
(Flag249-)
(Flag249prime-)
)
:effect
(and
(Flag255-)
(Flag255prime-)
)

)
(:action Flag255Action-21
:parameters ()
:precondition
(and
(Flag250-)
(Flag250prime-)
)
:effect
(and
(Flag255-)
(Flag255prime-)
)

)
(:action Flag255Action-22
:parameters ()
:precondition
(and
(Flag186-)
(Flag186prime-)
)
:effect
(and
(Flag255-)
(Flag255prime-)
)

)
(:action Flag255Action-23
:parameters ()
:precondition
(and
(Flag251-)
(Flag251prime-)
)
:effect
(and
(Flag255-)
(Flag255prime-)
)

)
(:action Flag255Action-24
:parameters ()
:precondition
(and
(Flag208-)
(Flag208prime-)
)
:effect
(and
(Flag255-)
(Flag255prime-)
)

)
(:action Flag255Action-25
:parameters ()
:precondition
(and
(Flag187-)
(Flag187prime-)
)
:effect
(and
(Flag255-)
(Flag255prime-)
)

)
(:action Flag255Action-26
:parameters ()
:precondition
(and
(Flag220-)
(Flag220prime-)
)
:effect
(and
(Flag255-)
(Flag255prime-)
)

)
(:action Flag255Action-27
:parameters ()
:precondition
(and
(Flag221-)
(Flag221prime-)
)
:effect
(and
(Flag255-)
(Flag255prime-)
)

)
(:action Flag255Action-28
:parameters ()
:precondition
(and
(Flag188-)
(Flag188prime-)
)
:effect
(and
(Flag255-)
(Flag255prime-)
)

)
(:action Flag255Action-29
:parameters ()
:precondition
(and
(Flag209-)
(Flag209prime-)
)
:effect
(and
(Flag255-)
(Flag255prime-)
)

)
(:action Flag255Action-30
:parameters ()
:precondition
(and
(Flag233-)
(Flag233prime-)
)
:effect
(and
(Flag255-)
(Flag255prime-)
)

)
(:action Flag255Action-31
:parameters ()
:precondition
(and
(Flag252-)
(Flag252prime-)
)
:effect
(and
(Flag255-)
(Flag255prime-)
)

)
(:action Flag255Action-32
:parameters ()
:precondition
(and
(Flag253-)
(Flag253prime-)
)
:effect
(and
(Flag255-)
(Flag255prime-)
)

)
(:action Flag255Action-33
:parameters ()
:precondition
(and
(Flag222-)
(Flag222prime-)
)
:effect
(and
(Flag255-)
(Flag255prime-)
)

)
(:action Flag255Action-34
:parameters ()
:precondition
(and
(Flag219-)
(Flag219prime-)
)
:effect
(and
(Flag255-)
(Flag255prime-)
)

)
(:action Flag255Action-35
:parameters ()
:precondition
(and
(Flag197-)
(Flag197prime-)
)
:effect
(and
(Flag255-)
(Flag255prime-)
)

)
(:action Flag255Action-36
:parameters ()
:precondition
(and
(Flag224-)
(Flag224prime-)
)
:effect
(and
(Flag255-)
(Flag255prime-)
)

)
(:action Flag255Action-37
:parameters ()
:precondition
(and
(Flag234-)
(Flag234prime-)
)
:effect
(and
(Flag255-)
(Flag255prime-)
)

)
(:action Flag255Action-38
:parameters ()
:precondition
(and
(Flag235-)
(Flag235prime-)
)
:effect
(and
(Flag255-)
(Flag255prime-)
)

)
(:action Flag255Action-39
:parameters ()
:precondition
(and
(Flag211-)
(Flag211prime-)
)
:effect
(and
(Flag255-)
(Flag255prime-)
)

)
(:action Flag255Action-40
:parameters ()
:precondition
(and
(Flag225-)
(Flag225prime-)
)
:effect
(and
(Flag255-)
(Flag255prime-)
)

)
(:action Flag255Action-41
:parameters ()
:precondition
(and
(Flag236-)
(Flag236prime-)
)
:effect
(and
(Flag255-)
(Flag255prime-)
)

)
(:action Flag255Action-42
:parameters ()
:precondition
(and
(Flag190-)
(Flag190prime-)
)
:effect
(and
(Flag255-)
(Flag255prime-)
)

)
(:action Flag255Action-43
:parameters ()
:precondition
(and
(Flag210-)
(Flag210prime-)
)
:effect
(and
(Flag255-)
(Flag255prime-)
)

)
(:action Flag255Action-44
:parameters ()
:precondition
(and
(Flag237-)
(Flag237prime-)
)
:effect
(and
(Flag255-)
(Flag255prime-)
)

)
(:action Flag255Action-45
:parameters ()
:precondition
(and
(Flag238-)
(Flag238prime-)
)
:effect
(and
(Flag255-)
(Flag255prime-)
)

)
(:action Flag255Action-46
:parameters ()
:precondition
(and
(Flag194-)
(Flag194prime-)
)
:effect
(and
(Flag255-)
(Flag255prime-)
)

)
(:action Flag255Action-47
:parameters ()
:precondition
(and
(Flag239-)
(Flag239prime-)
)
:effect
(and
(Flag255-)
(Flag255prime-)
)

)
(:action Flag255Action-48
:parameters ()
:precondition
(and
(Flag198-)
(Flag198prime-)
)
:effect
(and
(Flag255-)
(Flag255prime-)
)

)
(:action Flag255Action-49
:parameters ()
:precondition
(and
(Flag254-)
(Flag254prime-)
)
:effect
(and
(Flag255-)
(Flag255prime-)
)

)
(:action Flag255Action-50
:parameters ()
:precondition
(and
(Flag241-)
(Flag241prime-)
)
:effect
(and
(Flag255-)
(Flag255prime-)
)

)
(:action Flag255Action-51
:parameters ()
:precondition
(and
(Flag213-)
(Flag213prime-)
)
:effect
(and
(Flag255-)
(Flag255prime-)
)

)
(:action Flag255Action-52
:parameters ()
:precondition
(and
(Flag226-)
(Flag226prime-)
)
:effect
(and
(Flag255-)
(Flag255prime-)
)

)
(:action Flag255Action-53
:parameters ()
:precondition
(and
(Flag214-)
(Flag214prime-)
)
:effect
(and
(Flag255-)
(Flag255prime-)
)

)
(:action Prim255Action
:parameters ()
:precondition
(and
(not (Flag243-))
(Flag243prime-)
(not (Flag201-))
(Flag201prime-)
(not (Flag202-))
(Flag202prime-)
(not (Flag216-))
(Flag216prime-)
(not (Flag189-))
(Flag189prime-)
(not (Flag229-))
(Flag229prime-)
(not (Flag244-))
(Flag244prime-)
(not (Flag217-))
(Flag217prime-)
(not (Flag199-))
(Flag199prime-)
(not (Flag245-))
(Flag245prime-)
(not (Flag230-))
(Flag230prime-)
(not (Flag246-))
(Flag246prime-)
(not (Flag231-))
(Flag231prime-)
(not (Flag204-))
(Flag204prime-)
(not (Flag247-))
(Flag247prime-)
(not (Flag205-))
(Flag205prime-)
(not (Flag206-))
(Flag206prime-)
(not (Flag218-))
(Flag218prime-)
(not (Flag196-))
(Flag196prime-)
(not (Flag248-))
(Flag248prime-)
(not (Flag249-))
(Flag249prime-)
(not (Flag250-))
(Flag250prime-)
(not (Flag186-))
(Flag186prime-)
(not (Flag251-))
(Flag251prime-)
(not (Flag208-))
(Flag208prime-)
(not (Flag187-))
(Flag187prime-)
(not (Flag220-))
(Flag220prime-)
(not (Flag221-))
(Flag221prime-)
(not (Flag188-))
(Flag188prime-)
(not (Flag209-))
(Flag209prime-)
(not (Flag233-))
(Flag233prime-)
(not (Flag252-))
(Flag252prime-)
(not (Flag253-))
(Flag253prime-)
(not (Flag222-))
(Flag222prime-)
(not (Flag219-))
(Flag219prime-)
(not (Flag197-))
(Flag197prime-)
(not (Flag224-))
(Flag224prime-)
(not (Flag234-))
(Flag234prime-)
(not (Flag235-))
(Flag235prime-)
(not (Flag211-))
(Flag211prime-)
(not (Flag225-))
(Flag225prime-)
(not (Flag236-))
(Flag236prime-)
(not (Flag190-))
(Flag190prime-)
(not (Flag210-))
(Flag210prime-)
(not (Flag237-))
(Flag237prime-)
(not (Flag238-))
(Flag238prime-)
(not (Flag194-))
(Flag194prime-)
(not (Flag239-))
(Flag239prime-)
(not (Flag198-))
(Flag198prime-)
(not (Flag254-))
(Flag254prime-)
(not (Flag241-))
(Flag241prime-)
(not (Flag213-))
(Flag213prime-)
(not (Flag226-))
(Flag226prime-)
(not (Flag214-))
(Flag214prime-)
)
:effect
(and
(Flag255prime-)
(not (Flag255-))
)

)
(:action Flag265Action-0
:parameters ()
:precondition
(and
(Flag243-)
(Flag243prime-)
)
:effect
(and
(Flag265-)
(Flag265prime-)
)

)
(:action Flag265Action-1
:parameters ()
:precondition
(and
(Flag201-)
(Flag201prime-)
)
:effect
(and
(Flag265-)
(Flag265prime-)
)

)
(:action Flag265Action-2
:parameters ()
:precondition
(and
(Flag202-)
(Flag202prime-)
)
:effect
(and
(Flag265-)
(Flag265prime-)
)

)
(:action Flag265Action-3
:parameters ()
:precondition
(and
(Flag244-)
(Flag244prime-)
)
:effect
(and
(Flag265-)
(Flag265prime-)
)

)
(:action Flag265Action-4
:parameters ()
:precondition
(and
(Flag189-)
(Flag189prime-)
)
:effect
(and
(Flag265-)
(Flag265prime-)
)

)
(:action Flag265Action-5
:parameters ()
:precondition
(and
(Flag229-)
(Flag229prime-)
)
:effect
(and
(Flag265-)
(Flag265prime-)
)

)
(:action Flag265Action-6
:parameters ()
:precondition
(and
(Flag210-)
(Flag210prime-)
)
:effect
(and
(Flag265-)
(Flag265prime-)
)

)
(:action Flag265Action-7
:parameters ()
:precondition
(and
(Flag226-)
(Flag226prime-)
)
:effect
(and
(Flag265-)
(Flag265prime-)
)

)
(:action Flag265Action-8
:parameters ()
:precondition
(and
(Flag199-)
(Flag199prime-)
)
:effect
(and
(Flag265-)
(Flag265prime-)
)

)
(:action Flag265Action-9
:parameters ()
:precondition
(and
(Flag246-)
(Flag246prime-)
)
:effect
(and
(Flag265-)
(Flag265prime-)
)

)
(:action Flag265Action-10
:parameters ()
:precondition
(and
(Flag216-)
(Flag216prime-)
)
:effect
(and
(Flag265-)
(Flag265prime-)
)

)
(:action Flag265Action-11
:parameters ()
:precondition
(and
(Flag231-)
(Flag231prime-)
)
:effect
(and
(Flag265-)
(Flag265prime-)
)

)
(:action Flag265Action-12
:parameters ()
:precondition
(and
(Flag204-)
(Flag204prime-)
)
:effect
(and
(Flag265-)
(Flag265prime-)
)

)
(:action Flag265Action-13
:parameters ()
:precondition
(and
(Flag247-)
(Flag247prime-)
)
:effect
(and
(Flag265-)
(Flag265prime-)
)

)
(:action Flag265Action-14
:parameters ()
:precondition
(and
(Flag205-)
(Flag205prime-)
)
:effect
(and
(Flag265-)
(Flag265prime-)
)

)
(:action Flag265Action-15
:parameters ()
:precondition
(and
(Flag206-)
(Flag206prime-)
)
:effect
(and
(Flag265-)
(Flag265prime-)
)

)
(:action Flag265Action-16
:parameters ()
:precondition
(and
(Flag256-)
(Flag256prime-)
)
:effect
(and
(Flag265-)
(Flag265prime-)
)

)
(:action Flag265Action-17
:parameters ()
:precondition
(and
(Flag218-)
(Flag218prime-)
)
:effect
(and
(Flag265-)
(Flag265prime-)
)

)
(:action Flag265Action-18
:parameters ()
:precondition
(and
(Flag196-)
(Flag196prime-)
)
:effect
(and
(Flag265-)
(Flag265prime-)
)

)
(:action Flag265Action-19
:parameters ()
:precondition
(and
(Flag257-)
(Flag257prime-)
)
:effect
(and
(Flag265-)
(Flag265prime-)
)

)
(:action Flag265Action-20
:parameters ()
:precondition
(and
(Flag219-)
(Flag219prime-)
)
:effect
(and
(Flag265-)
(Flag265prime-)
)

)
(:action Flag265Action-21
:parameters ()
:precondition
(and
(Flag258-)
(Flag258prime-)
)
:effect
(and
(Flag265-)
(Flag265prime-)
)

)
(:action Flag265Action-22
:parameters ()
:precondition
(and
(Flag249-)
(Flag249prime-)
)
:effect
(and
(Flag265-)
(Flag265prime-)
)

)
(:action Flag265Action-23
:parameters ()
:precondition
(and
(Flag250-)
(Flag250prime-)
)
:effect
(and
(Flag265-)
(Flag265prime-)
)

)
(:action Flag265Action-24
:parameters ()
:precondition
(and
(Flag251-)
(Flag251prime-)
)
:effect
(and
(Flag265-)
(Flag265prime-)
)

)
(:action Flag265Action-25
:parameters ()
:precondition
(and
(Flag208-)
(Flag208prime-)
)
:effect
(and
(Flag265-)
(Flag265prime-)
)

)
(:action Flag265Action-26
:parameters ()
:precondition
(and
(Flag187-)
(Flag187prime-)
)
:effect
(and
(Flag265-)
(Flag265prime-)
)

)
(:action Flag265Action-27
:parameters ()
:precondition
(and
(Flag220-)
(Flag220prime-)
)
:effect
(and
(Flag265-)
(Flag265prime-)
)

)
(:action Flag265Action-28
:parameters ()
:precondition
(and
(Flag221-)
(Flag221prime-)
)
:effect
(and
(Flag265-)
(Flag265prime-)
)

)
(:action Flag265Action-29
:parameters ()
:precondition
(and
(Flag188-)
(Flag188prime-)
)
:effect
(and
(Flag265-)
(Flag265prime-)
)

)
(:action Flag265Action-30
:parameters ()
:precondition
(and
(Flag209-)
(Flag209prime-)
)
:effect
(and
(Flag265-)
(Flag265prime-)
)

)
(:action Flag265Action-31
:parameters ()
:precondition
(and
(Flag233-)
(Flag233prime-)
)
:effect
(and
(Flag265-)
(Flag265prime-)
)

)
(:action Flag265Action-32
:parameters ()
:precondition
(and
(Flag252-)
(Flag252prime-)
)
:effect
(and
(Flag265-)
(Flag265prime-)
)

)
(:action Flag265Action-33
:parameters ()
:precondition
(and
(Flag253-)
(Flag253prime-)
)
:effect
(and
(Flag265-)
(Flag265prime-)
)

)
(:action Flag265Action-34
:parameters ()
:precondition
(and
(Flag222-)
(Flag222prime-)
)
:effect
(and
(Flag265-)
(Flag265prime-)
)

)
(:action Flag265Action-35
:parameters ()
:precondition
(and
(Flag259-)
(Flag259prime-)
)
:effect
(and
(Flag265-)
(Flag265prime-)
)

)
(:action Flag265Action-36
:parameters ()
:precondition
(and
(Flag197-)
(Flag197prime-)
)
:effect
(and
(Flag265-)
(Flag265prime-)
)

)
(:action Flag265Action-37
:parameters ()
:precondition
(and
(Flag224-)
(Flag224prime-)
)
:effect
(and
(Flag265-)
(Flag265prime-)
)

)
(:action Flag265Action-38
:parameters ()
:precondition
(and
(Flag234-)
(Flag234prime-)
)
:effect
(and
(Flag265-)
(Flag265prime-)
)

)
(:action Flag265Action-39
:parameters ()
:precondition
(and
(Flag235-)
(Flag235prime-)
)
:effect
(and
(Flag265-)
(Flag265prime-)
)

)
(:action Flag265Action-40
:parameters ()
:precondition
(and
(Flag260-)
(Flag260prime-)
)
:effect
(and
(Flag265-)
(Flag265prime-)
)

)
(:action Flag265Action-41
:parameters ()
:precondition
(and
(Flag225-)
(Flag225prime-)
)
:effect
(and
(Flag265-)
(Flag265prime-)
)

)
(:action Flag265Action-42
:parameters ()
:precondition
(and
(Flag261-)
(Flag261prime-)
)
:effect
(and
(Flag265-)
(Flag265prime-)
)

)
(:action Flag265Action-43
:parameters ()
:precondition
(and
(Flag262-)
(Flag262prime-)
)
:effect
(and
(Flag265-)
(Flag265prime-)
)

)
(:action Flag265Action-44
:parameters ()
:precondition
(and
(Flag236-)
(Flag236prime-)
)
:effect
(and
(Flag265-)
(Flag265prime-)
)

)
(:action Flag265Action-45
:parameters ()
:precondition
(and
(Flag190-)
(Flag190prime-)
)
:effect
(and
(Flag265-)
(Flag265prime-)
)

)
(:action Flag265Action-46
:parameters ()
:precondition
(and
(Flag263-)
(Flag263prime-)
)
:effect
(and
(Flag265-)
(Flag265prime-)
)

)
(:action Flag265Action-47
:parameters ()
:precondition
(and
(Flag237-)
(Flag237prime-)
)
:effect
(and
(Flag265-)
(Flag265prime-)
)

)
(:action Flag265Action-48
:parameters ()
:precondition
(and
(Flag238-)
(Flag238prime-)
)
:effect
(and
(Flag265-)
(Flag265prime-)
)

)
(:action Flag265Action-49
:parameters ()
:precondition
(and
(Flag211-)
(Flag211prime-)
)
:effect
(and
(Flag265-)
(Flag265prime-)
)

)
(:action Flag265Action-50
:parameters ()
:precondition
(and
(Flag239-)
(Flag239prime-)
)
:effect
(and
(Flag265-)
(Flag265prime-)
)

)
(:action Flag265Action-51
:parameters ()
:precondition
(and
(Flag198-)
(Flag198prime-)
)
:effect
(and
(Flag265-)
(Flag265prime-)
)

)
(:action Flag265Action-52
:parameters ()
:precondition
(and
(Flag254-)
(Flag254prime-)
)
:effect
(and
(Flag265-)
(Flag265prime-)
)

)
(:action Flag265Action-53
:parameters ()
:precondition
(and
(Flag241-)
(Flag241prime-)
)
:effect
(and
(Flag265-)
(Flag265prime-)
)

)
(:action Flag265Action-54
:parameters ()
:precondition
(and
(Flag213-)
(Flag213prime-)
)
:effect
(and
(Flag265-)
(Flag265prime-)
)

)
(:action Flag265Action-55
:parameters ()
:precondition
(and
(Flag245-)
(Flag245prime-)
)
:effect
(and
(Flag265-)
(Flag265prime-)
)

)
(:action Flag265Action-56
:parameters ()
:precondition
(and
(Flag264-)
(Flag264prime-)
)
:effect
(and
(Flag265-)
(Flag265prime-)
)

)
(:action Prim265Action
:parameters ()
:precondition
(and
(not (Flag243-))
(Flag243prime-)
(not (Flag201-))
(Flag201prime-)
(not (Flag202-))
(Flag202prime-)
(not (Flag244-))
(Flag244prime-)
(not (Flag189-))
(Flag189prime-)
(not (Flag229-))
(Flag229prime-)
(not (Flag210-))
(Flag210prime-)
(not (Flag226-))
(Flag226prime-)
(not (Flag199-))
(Flag199prime-)
(not (Flag246-))
(Flag246prime-)
(not (Flag216-))
(Flag216prime-)
(not (Flag231-))
(Flag231prime-)
(not (Flag204-))
(Flag204prime-)
(not (Flag247-))
(Flag247prime-)
(not (Flag205-))
(Flag205prime-)
(not (Flag206-))
(Flag206prime-)
(not (Flag256-))
(Flag256prime-)
(not (Flag218-))
(Flag218prime-)
(not (Flag196-))
(Flag196prime-)
(not (Flag257-))
(Flag257prime-)
(not (Flag219-))
(Flag219prime-)
(not (Flag258-))
(Flag258prime-)
(not (Flag249-))
(Flag249prime-)
(not (Flag250-))
(Flag250prime-)
(not (Flag251-))
(Flag251prime-)
(not (Flag208-))
(Flag208prime-)
(not (Flag187-))
(Flag187prime-)
(not (Flag220-))
(Flag220prime-)
(not (Flag221-))
(Flag221prime-)
(not (Flag188-))
(Flag188prime-)
(not (Flag209-))
(Flag209prime-)
(not (Flag233-))
(Flag233prime-)
(not (Flag252-))
(Flag252prime-)
(not (Flag253-))
(Flag253prime-)
(not (Flag222-))
(Flag222prime-)
(not (Flag259-))
(Flag259prime-)
(not (Flag197-))
(Flag197prime-)
(not (Flag224-))
(Flag224prime-)
(not (Flag234-))
(Flag234prime-)
(not (Flag235-))
(Flag235prime-)
(not (Flag260-))
(Flag260prime-)
(not (Flag225-))
(Flag225prime-)
(not (Flag261-))
(Flag261prime-)
(not (Flag262-))
(Flag262prime-)
(not (Flag236-))
(Flag236prime-)
(not (Flag190-))
(Flag190prime-)
(not (Flag263-))
(Flag263prime-)
(not (Flag237-))
(Flag237prime-)
(not (Flag238-))
(Flag238prime-)
(not (Flag211-))
(Flag211prime-)
(not (Flag239-))
(Flag239prime-)
(not (Flag198-))
(Flag198prime-)
(not (Flag254-))
(Flag254prime-)
(not (Flag241-))
(Flag241prime-)
(not (Flag213-))
(Flag213prime-)
(not (Flag245-))
(Flag245prime-)
(not (Flag264-))
(Flag264prime-)
)
:effect
(and
(Flag265prime-)
(not (Flag265-))
)

)
(:action Flag276Action-0
:parameters ()
:precondition
(and
(Flag243-)
(Flag243prime-)
)
:effect
(and
(Flag276-)
(Flag276prime-)
)

)
(:action Flag276Action-1
:parameters ()
:precondition
(and
(Flag201-)
(Flag201prime-)
)
:effect
(and
(Flag276-)
(Flag276prime-)
)

)
(:action Flag276Action-2
:parameters ()
:precondition
(and
(Flag202-)
(Flag202prime-)
)
:effect
(and
(Flag276-)
(Flag276prime-)
)

)
(:action Flag276Action-3
:parameters ()
:precondition
(and
(Flag244-)
(Flag244prime-)
)
:effect
(and
(Flag276-)
(Flag276prime-)
)

)
(:action Flag276Action-4
:parameters ()
:precondition
(and
(Flag189-)
(Flag189prime-)
)
:effect
(and
(Flag276-)
(Flag276prime-)
)

)
(:action Flag276Action-5
:parameters ()
:precondition
(and
(Flag266-)
(Flag266prime-)
)
:effect
(and
(Flag276-)
(Flag276prime-)
)

)
(:action Flag276Action-6
:parameters ()
:precondition
(and
(Flag237-)
(Flag237prime-)
)
:effect
(and
(Flag276-)
(Flag276prime-)
)

)
(:action Flag276Action-7
:parameters ()
:precondition
(and
(Flag267-)
(Flag267prime-)
)
:effect
(and
(Flag276-)
(Flag276prime-)
)

)
(:action Flag276Action-8
:parameters ()
:precondition
(and
(Flag268-)
(Flag268prime-)
)
:effect
(and
(Flag276-)
(Flag276prime-)
)

)
(:action Flag276Action-9
:parameters ()
:precondition
(and
(Flag229-)
(Flag229prime-)
)
:effect
(and
(Flag276-)
(Flag276prime-)
)

)
(:action Flag276Action-10
:parameters ()
:precondition
(and
(Flag210-)
(Flag210prime-)
)
:effect
(and
(Flag276-)
(Flag276prime-)
)

)
(:action Flag276Action-11
:parameters ()
:precondition
(and
(Flag226-)
(Flag226prime-)
)
:effect
(and
(Flag276-)
(Flag276prime-)
)

)
(:action Flag276Action-12
:parameters ()
:precondition
(and
(Flag199-)
(Flag199prime-)
)
:effect
(and
(Flag276-)
(Flag276prime-)
)

)
(:action Flag276Action-13
:parameters ()
:precondition
(and
(Flag187-)
(Flag187prime-)
)
:effect
(and
(Flag276-)
(Flag276prime-)
)

)
(:action Flag276Action-14
:parameters ()
:precondition
(and
(Flag246-)
(Flag246prime-)
)
:effect
(and
(Flag276-)
(Flag276prime-)
)

)
(:action Flag276Action-15
:parameters ()
:precondition
(and
(Flag216-)
(Flag216prime-)
)
:effect
(and
(Flag276-)
(Flag276prime-)
)

)
(:action Flag276Action-16
:parameters ()
:precondition
(and
(Flag231-)
(Flag231prime-)
)
:effect
(and
(Flag276-)
(Flag276prime-)
)

)
(:action Flag276Action-17
:parameters ()
:precondition
(and
(Flag221-)
(Flag221prime-)
)
:effect
(and
(Flag276-)
(Flag276prime-)
)

)
(:action Flag276Action-18
:parameters ()
:precondition
(and
(Flag269-)
(Flag269prime-)
)
:effect
(and
(Flag276-)
(Flag276prime-)
)

)
(:action Flag276Action-19
:parameters ()
:precondition
(and
(Flag205-)
(Flag205prime-)
)
:effect
(and
(Flag276-)
(Flag276prime-)
)

)
(:action Flag276Action-20
:parameters ()
:precondition
(and
(Flag256-)
(Flag256prime-)
)
:effect
(and
(Flag276-)
(Flag276prime-)
)

)
(:action Flag276Action-21
:parameters ()
:precondition
(and
(Flag218-)
(Flag218prime-)
)
:effect
(and
(Flag276-)
(Flag276prime-)
)

)
(:action Flag276Action-22
:parameters ()
:precondition
(and
(Flag196-)
(Flag196prime-)
)
:effect
(and
(Flag276-)
(Flag276prime-)
)

)
(:action Flag276Action-23
:parameters ()
:precondition
(and
(Flag257-)
(Flag257prime-)
)
:effect
(and
(Flag276-)
(Flag276prime-)
)

)
(:action Flag276Action-24
:parameters ()
:precondition
(and
(Flag219-)
(Flag219prime-)
)
:effect
(and
(Flag276-)
(Flag276prime-)
)

)
(:action Flag276Action-25
:parameters ()
:precondition
(and
(Flag258-)
(Flag258prime-)
)
:effect
(and
(Flag276-)
(Flag276prime-)
)

)
(:action Flag276Action-26
:parameters ()
:precondition
(and
(Flag249-)
(Flag249prime-)
)
:effect
(and
(Flag276-)
(Flag276prime-)
)

)
(:action Flag276Action-27
:parameters ()
:precondition
(and
(Flag250-)
(Flag250prime-)
)
:effect
(and
(Flag276-)
(Flag276prime-)
)

)
(:action Flag276Action-28
:parameters ()
:precondition
(and
(Flag251-)
(Flag251prime-)
)
:effect
(and
(Flag276-)
(Flag276prime-)
)

)
(:action Flag276Action-29
:parameters ()
:precondition
(and
(Flag208-)
(Flag208prime-)
)
:effect
(and
(Flag276-)
(Flag276prime-)
)

)
(:action Flag276Action-30
:parameters ()
:precondition
(and
(Flag270-)
(Flag270prime-)
)
:effect
(and
(Flag276-)
(Flag276prime-)
)

)
(:action Flag276Action-31
:parameters ()
:precondition
(and
(Flag220-)
(Flag220prime-)
)
:effect
(and
(Flag276-)
(Flag276prime-)
)

)
(:action Flag276Action-32
:parameters ()
:precondition
(and
(Flag247-)
(Flag247prime-)
)
:effect
(and
(Flag276-)
(Flag276prime-)
)

)
(:action Flag276Action-33
:parameters ()
:precondition
(and
(Flag188-)
(Flag188prime-)
)
:effect
(and
(Flag276-)
(Flag276prime-)
)

)
(:action Flag276Action-34
:parameters ()
:precondition
(and
(Flag209-)
(Flag209prime-)
)
:effect
(and
(Flag276-)
(Flag276prime-)
)

)
(:action Flag276Action-35
:parameters ()
:precondition
(and
(Flag271-)
(Flag271prime-)
)
:effect
(and
(Flag276-)
(Flag276prime-)
)

)
(:action Flag276Action-36
:parameters ()
:precondition
(and
(Flag252-)
(Flag252prime-)
)
:effect
(and
(Flag276-)
(Flag276prime-)
)

)
(:action Flag276Action-37
:parameters ()
:precondition
(and
(Flag253-)
(Flag253prime-)
)
:effect
(and
(Flag276-)
(Flag276prime-)
)

)
(:action Flag276Action-38
:parameters ()
:precondition
(and
(Flag233-)
(Flag233prime-)
)
:effect
(and
(Flag276-)
(Flag276prime-)
)

)
(:action Flag276Action-39
:parameters ()
:precondition
(and
(Flag197-)
(Flag197prime-)
)
:effect
(and
(Flag276-)
(Flag276prime-)
)

)
(:action Flag276Action-40
:parameters ()
:precondition
(and
(Flag224-)
(Flag224prime-)
)
:effect
(and
(Flag276-)
(Flag276prime-)
)

)
(:action Flag276Action-41
:parameters ()
:precondition
(and
(Flag234-)
(Flag234prime-)
)
:effect
(and
(Flag276-)
(Flag276prime-)
)

)
(:action Flag276Action-42
:parameters ()
:precondition
(and
(Flag235-)
(Flag235prime-)
)
:effect
(and
(Flag276-)
(Flag276prime-)
)

)
(:action Flag276Action-43
:parameters ()
:precondition
(and
(Flag260-)
(Flag260prime-)
)
:effect
(and
(Flag276-)
(Flag276prime-)
)

)
(:action Flag276Action-44
:parameters ()
:precondition
(and
(Flag225-)
(Flag225prime-)
)
:effect
(and
(Flag276-)
(Flag276prime-)
)

)
(:action Flag276Action-45
:parameters ()
:precondition
(and
(Flag272-)
(Flag272prime-)
)
:effect
(and
(Flag276-)
(Flag276prime-)
)

)
(:action Flag276Action-46
:parameters ()
:precondition
(and
(Flag273-)
(Flag273prime-)
)
:effect
(and
(Flag276-)
(Flag276prime-)
)

)
(:action Flag276Action-47
:parameters ()
:precondition
(and
(Flag274-)
(Flag274prime-)
)
:effect
(and
(Flag276-)
(Flag276prime-)
)

)
(:action Flag276Action-48
:parameters ()
:precondition
(and
(Flag261-)
(Flag261prime-)
)
:effect
(and
(Flag276-)
(Flag276prime-)
)

)
(:action Flag276Action-49
:parameters ()
:precondition
(and
(Flag236-)
(Flag236prime-)
)
:effect
(and
(Flag276-)
(Flag276prime-)
)

)
(:action Flag276Action-50
:parameters ()
:precondition
(and
(Flag190-)
(Flag190prime-)
)
:effect
(and
(Flag276-)
(Flag276prime-)
)

)
(:action Flag276Action-51
:parameters ()
:precondition
(and
(Flag263-)
(Flag263prime-)
)
:effect
(and
(Flag276-)
(Flag276prime-)
)

)
(:action Flag276Action-52
:parameters ()
:precondition
(and
(Flag275-)
(Flag275prime-)
)
:effect
(and
(Flag276-)
(Flag276prime-)
)

)
(:action Flag276Action-53
:parameters ()
:precondition
(and
(Flag211-)
(Flag211prime-)
)
:effect
(and
(Flag276-)
(Flag276prime-)
)

)
(:action Flag276Action-54
:parameters ()
:precondition
(and
(Flag239-)
(Flag239prime-)
)
:effect
(and
(Flag276-)
(Flag276prime-)
)

)
(:action Flag276Action-55
:parameters ()
:precondition
(and
(Flag198-)
(Flag198prime-)
)
:effect
(and
(Flag276-)
(Flag276prime-)
)

)
(:action Flag276Action-56
:parameters ()
:precondition
(and
(Flag241-)
(Flag241prime-)
)
:effect
(and
(Flag276-)
(Flag276prime-)
)

)
(:action Flag276Action-57
:parameters ()
:precondition
(and
(Flag213-)
(Flag213prime-)
)
:effect
(and
(Flag276-)
(Flag276prime-)
)

)
(:action Flag276Action-58
:parameters ()
:precondition
(and
(Flag245-)
(Flag245prime-)
)
:effect
(and
(Flag276-)
(Flag276prime-)
)

)
(:action Flag276Action-59
:parameters ()
:precondition
(and
(Flag264-)
(Flag264prime-)
)
:effect
(and
(Flag276-)
(Flag276prime-)
)

)
(:action Prim276Action
:parameters ()
:precondition
(and
(not (Flag243-))
(Flag243prime-)
(not (Flag201-))
(Flag201prime-)
(not (Flag202-))
(Flag202prime-)
(not (Flag244-))
(Flag244prime-)
(not (Flag189-))
(Flag189prime-)
(not (Flag266-))
(Flag266prime-)
(not (Flag237-))
(Flag237prime-)
(not (Flag267-))
(Flag267prime-)
(not (Flag268-))
(Flag268prime-)
(not (Flag229-))
(Flag229prime-)
(not (Flag210-))
(Flag210prime-)
(not (Flag226-))
(Flag226prime-)
(not (Flag199-))
(Flag199prime-)
(not (Flag187-))
(Flag187prime-)
(not (Flag246-))
(Flag246prime-)
(not (Flag216-))
(Flag216prime-)
(not (Flag231-))
(Flag231prime-)
(not (Flag221-))
(Flag221prime-)
(not (Flag269-))
(Flag269prime-)
(not (Flag205-))
(Flag205prime-)
(not (Flag256-))
(Flag256prime-)
(not (Flag218-))
(Flag218prime-)
(not (Flag196-))
(Flag196prime-)
(not (Flag257-))
(Flag257prime-)
(not (Flag219-))
(Flag219prime-)
(not (Flag258-))
(Flag258prime-)
(not (Flag249-))
(Flag249prime-)
(not (Flag250-))
(Flag250prime-)
(not (Flag251-))
(Flag251prime-)
(not (Flag208-))
(Flag208prime-)
(not (Flag270-))
(Flag270prime-)
(not (Flag220-))
(Flag220prime-)
(not (Flag247-))
(Flag247prime-)
(not (Flag188-))
(Flag188prime-)
(not (Flag209-))
(Flag209prime-)
(not (Flag271-))
(Flag271prime-)
(not (Flag252-))
(Flag252prime-)
(not (Flag253-))
(Flag253prime-)
(not (Flag233-))
(Flag233prime-)
(not (Flag197-))
(Flag197prime-)
(not (Flag224-))
(Flag224prime-)
(not (Flag234-))
(Flag234prime-)
(not (Flag235-))
(Flag235prime-)
(not (Flag260-))
(Flag260prime-)
(not (Flag225-))
(Flag225prime-)
(not (Flag272-))
(Flag272prime-)
(not (Flag273-))
(Flag273prime-)
(not (Flag274-))
(Flag274prime-)
(not (Flag261-))
(Flag261prime-)
(not (Flag236-))
(Flag236prime-)
(not (Flag190-))
(Flag190prime-)
(not (Flag263-))
(Flag263prime-)
(not (Flag275-))
(Flag275prime-)
(not (Flag211-))
(Flag211prime-)
(not (Flag239-))
(Flag239prime-)
(not (Flag198-))
(Flag198prime-)
(not (Flag241-))
(Flag241prime-)
(not (Flag213-))
(Flag213prime-)
(not (Flag245-))
(Flag245prime-)
(not (Flag264-))
(Flag264prime-)
)
:effect
(and
(Flag276prime-)
(not (Flag276-))
)

)
(:action Flag285Action-0
:parameters ()
:precondition
(and
(Flag243-)
(Flag243prime-)
)
:effect
(and
(Flag285-)
(Flag285prime-)
)

)
(:action Flag285Action-1
:parameters ()
:precondition
(and
(Flag201-)
(Flag201prime-)
)
:effect
(and
(Flag285-)
(Flag285prime-)
)

)
(:action Flag285Action-2
:parameters ()
:precondition
(and
(Flag277-)
(Flag277prime-)
)
:effect
(and
(Flag285-)
(Flag285prime-)
)

)
(:action Flag285Action-3
:parameters ()
:precondition
(and
(Flag244-)
(Flag244prime-)
)
:effect
(and
(Flag285-)
(Flag285prime-)
)

)
(:action Flag285Action-4
:parameters ()
:precondition
(and
(Flag189-)
(Flag189prime-)
)
:effect
(and
(Flag285-)
(Flag285prime-)
)

)
(:action Flag285Action-5
:parameters ()
:precondition
(and
(Flag251-)
(Flag251prime-)
)
:effect
(and
(Flag285-)
(Flag285prime-)
)

)
(:action Flag285Action-6
:parameters ()
:precondition
(and
(Flag266-)
(Flag266prime-)
)
:effect
(and
(Flag285-)
(Flag285prime-)
)

)
(:action Flag285Action-7
:parameters ()
:precondition
(and
(Flag267-)
(Flag267prime-)
)
:effect
(and
(Flag285-)
(Flag285prime-)
)

)
(:action Flag285Action-8
:parameters ()
:precondition
(and
(Flag229-)
(Flag229prime-)
)
:effect
(and
(Flag285-)
(Flag285prime-)
)

)
(:action Flag285Action-9
:parameters ()
:precondition
(and
(Flag210-)
(Flag210prime-)
)
:effect
(and
(Flag285-)
(Flag285prime-)
)

)
(:action Flag285Action-10
:parameters ()
:precondition
(and
(Flag208-)
(Flag208prime-)
)
:effect
(and
(Flag285-)
(Flag285prime-)
)

)
(:action Flag285Action-11
:parameters ()
:precondition
(and
(Flag278-)
(Flag278prime-)
)
:effect
(and
(Flag285-)
(Flag285prime-)
)

)
(:action Flag285Action-12
:parameters ()
:precondition
(and
(Flag199-)
(Flag199prime-)
)
:effect
(and
(Flag285-)
(Flag285prime-)
)

)
(:action Flag285Action-13
:parameters ()
:precondition
(and
(Flag245-)
(Flag245prime-)
)
:effect
(and
(Flag285-)
(Flag285prime-)
)

)
(:action Flag285Action-14
:parameters ()
:precondition
(and
(Flag279-)
(Flag279prime-)
)
:effect
(and
(Flag285-)
(Flag285prime-)
)

)
(:action Flag285Action-15
:parameters ()
:precondition
(and
(Flag246-)
(Flag246prime-)
)
:effect
(and
(Flag285-)
(Flag285prime-)
)

)
(:action Flag285Action-16
:parameters ()
:precondition
(and
(Flag231-)
(Flag231prime-)
)
:effect
(and
(Flag285-)
(Flag285prime-)
)

)
(:action Flag285Action-17
:parameters ()
:precondition
(and
(Flag221-)
(Flag221prime-)
)
:effect
(and
(Flag285-)
(Flag285prime-)
)

)
(:action Flag285Action-18
:parameters ()
:precondition
(and
(Flag269-)
(Flag269prime-)
)
:effect
(and
(Flag285-)
(Flag285prime-)
)

)
(:action Flag285Action-19
:parameters ()
:precondition
(and
(Flag188-)
(Flag188prime-)
)
:effect
(and
(Flag285-)
(Flag285prime-)
)

)
(:action Flag285Action-20
:parameters ()
:precondition
(and
(Flag202-)
(Flag202prime-)
)
:effect
(and
(Flag285-)
(Flag285prime-)
)

)
(:action Flag285Action-21
:parameters ()
:precondition
(and
(Flag256-)
(Flag256prime-)
)
:effect
(and
(Flag285-)
(Flag285prime-)
)

)
(:action Flag285Action-22
:parameters ()
:precondition
(and
(Flag218-)
(Flag218prime-)
)
:effect
(and
(Flag285-)
(Flag285prime-)
)

)
(:action Flag285Action-23
:parameters ()
:precondition
(and
(Flag196-)
(Flag196prime-)
)
:effect
(and
(Flag285-)
(Flag285prime-)
)

)
(:action Flag285Action-24
:parameters ()
:precondition
(and
(Flag257-)
(Flag257prime-)
)
:effect
(and
(Flag285-)
(Flag285prime-)
)

)
(:action Flag285Action-25
:parameters ()
:precondition
(and
(Flag219-)
(Flag219prime-)
)
:effect
(and
(Flag285-)
(Flag285prime-)
)

)
(:action Flag285Action-26
:parameters ()
:precondition
(and
(Flag258-)
(Flag258prime-)
)
:effect
(and
(Flag285-)
(Flag285prime-)
)

)
(:action Flag285Action-27
:parameters ()
:precondition
(and
(Flag249-)
(Flag249prime-)
)
:effect
(and
(Flag285-)
(Flag285prime-)
)

)
(:action Flag285Action-28
:parameters ()
:precondition
(and
(Flag280-)
(Flag280prime-)
)
:effect
(and
(Flag285-)
(Flag285prime-)
)

)
(:action Flag285Action-29
:parameters ()
:precondition
(and
(Flag281-)
(Flag281prime-)
)
:effect
(and
(Flag285-)
(Flag285prime-)
)

)
(:action Flag285Action-30
:parameters ()
:precondition
(and
(Flag282-)
(Flag282prime-)
)
:effect
(and
(Flag285-)
(Flag285prime-)
)

)
(:action Flag285Action-31
:parameters ()
:precondition
(and
(Flag283-)
(Flag283prime-)
)
:effect
(and
(Flag285-)
(Flag285prime-)
)

)
(:action Flag285Action-32
:parameters ()
:precondition
(and
(Flag187-)
(Flag187prime-)
)
:effect
(and
(Flag285-)
(Flag285prime-)
)

)
(:action Flag285Action-33
:parameters ()
:precondition
(and
(Flag220-)
(Flag220prime-)
)
:effect
(and
(Flag285-)
(Flag285prime-)
)

)
(:action Flag285Action-34
:parameters ()
:precondition
(and
(Flag247-)
(Flag247prime-)
)
:effect
(and
(Flag285-)
(Flag285prime-)
)

)
(:action Flag285Action-35
:parameters ()
:precondition
(and
(Flag205-)
(Flag205prime-)
)
:effect
(and
(Flag285-)
(Flag285prime-)
)

)
(:action Flag285Action-36
:parameters ()
:precondition
(and
(Flag209-)
(Flag209prime-)
)
:effect
(and
(Flag285-)
(Flag285prime-)
)

)
(:action Flag285Action-37
:parameters ()
:precondition
(and
(Flag271-)
(Flag271prime-)
)
:effect
(and
(Flag285-)
(Flag285prime-)
)

)
(:action Flag285Action-38
:parameters ()
:precondition
(and
(Flag252-)
(Flag252prime-)
)
:effect
(and
(Flag285-)
(Flag285prime-)
)

)
(:action Flag285Action-39
:parameters ()
:precondition
(and
(Flag253-)
(Flag253prime-)
)
:effect
(and
(Flag285-)
(Flag285prime-)
)

)
(:action Flag285Action-40
:parameters ()
:precondition
(and
(Flag233-)
(Flag233prime-)
)
:effect
(and
(Flag285-)
(Flag285prime-)
)

)
(:action Flag285Action-41
:parameters ()
:precondition
(and
(Flag197-)
(Flag197prime-)
)
:effect
(and
(Flag285-)
(Flag285prime-)
)

)
(:action Flag285Action-42
:parameters ()
:precondition
(and
(Flag224-)
(Flag224prime-)
)
:effect
(and
(Flag285-)
(Flag285prime-)
)

)
(:action Flag285Action-43
:parameters ()
:precondition
(and
(Flag234-)
(Flag234prime-)
)
:effect
(and
(Flag285-)
(Flag285prime-)
)

)
(:action Flag285Action-44
:parameters ()
:precondition
(and
(Flag235-)
(Flag235prime-)
)
:effect
(and
(Flag285-)
(Flag285prime-)
)

)
(:action Flag285Action-45
:parameters ()
:precondition
(and
(Flag260-)
(Flag260prime-)
)
:effect
(and
(Flag285-)
(Flag285prime-)
)

)
(:action Flag285Action-46
:parameters ()
:precondition
(and
(Flag225-)
(Flag225prime-)
)
:effect
(and
(Flag285-)
(Flag285prime-)
)

)
(:action Flag285Action-47
:parameters ()
:precondition
(and
(Flag272-)
(Flag272prime-)
)
:effect
(and
(Flag285-)
(Flag285prime-)
)

)
(:action Flag285Action-48
:parameters ()
:precondition
(and
(Flag274-)
(Flag274prime-)
)
:effect
(and
(Flag285-)
(Flag285prime-)
)

)
(:action Flag285Action-49
:parameters ()
:precondition
(and
(Flag261-)
(Flag261prime-)
)
:effect
(and
(Flag285-)
(Flag285prime-)
)

)
(:action Flag285Action-50
:parameters ()
:precondition
(and
(Flag236-)
(Flag236prime-)
)
:effect
(and
(Flag285-)
(Flag285prime-)
)

)
(:action Flag285Action-51
:parameters ()
:precondition
(and
(Flag190-)
(Flag190prime-)
)
:effect
(and
(Flag285-)
(Flag285prime-)
)

)
(:action Flag285Action-52
:parameters ()
:precondition
(and
(Flag263-)
(Flag263prime-)
)
:effect
(and
(Flag285-)
(Flag285prime-)
)

)
(:action Flag285Action-53
:parameters ()
:precondition
(and
(Flag275-)
(Flag275prime-)
)
:effect
(and
(Flag285-)
(Flag285prime-)
)

)
(:action Flag285Action-54
:parameters ()
:precondition
(and
(Flag211-)
(Flag211prime-)
)
:effect
(and
(Flag285-)
(Flag285prime-)
)

)
(:action Flag285Action-55
:parameters ()
:precondition
(and
(Flag239-)
(Flag239prime-)
)
:effect
(and
(Flag285-)
(Flag285prime-)
)

)
(:action Flag285Action-56
:parameters ()
:precondition
(and
(Flag284-)
(Flag284prime-)
)
:effect
(and
(Flag285-)
(Flag285prime-)
)

)
(:action Flag285Action-57
:parameters ()
:precondition
(and
(Flag241-)
(Flag241prime-)
)
:effect
(and
(Flag285-)
(Flag285prime-)
)

)
(:action Flag285Action-58
:parameters ()
:precondition
(and
(Flag226-)
(Flag226prime-)
)
:effect
(and
(Flag285-)
(Flag285prime-)
)

)
(:action Flag285Action-59
:parameters ()
:precondition
(and
(Flag264-)
(Flag264prime-)
)
:effect
(and
(Flag285-)
(Flag285prime-)
)

)
(:action Prim285Action
:parameters ()
:precondition
(and
(not (Flag243-))
(Flag243prime-)
(not (Flag201-))
(Flag201prime-)
(not (Flag277-))
(Flag277prime-)
(not (Flag244-))
(Flag244prime-)
(not (Flag189-))
(Flag189prime-)
(not (Flag251-))
(Flag251prime-)
(not (Flag266-))
(Flag266prime-)
(not (Flag267-))
(Flag267prime-)
(not (Flag229-))
(Flag229prime-)
(not (Flag210-))
(Flag210prime-)
(not (Flag208-))
(Flag208prime-)
(not (Flag278-))
(Flag278prime-)
(not (Flag199-))
(Flag199prime-)
(not (Flag245-))
(Flag245prime-)
(not (Flag279-))
(Flag279prime-)
(not (Flag246-))
(Flag246prime-)
(not (Flag231-))
(Flag231prime-)
(not (Flag221-))
(Flag221prime-)
(not (Flag269-))
(Flag269prime-)
(not (Flag188-))
(Flag188prime-)
(not (Flag202-))
(Flag202prime-)
(not (Flag256-))
(Flag256prime-)
(not (Flag218-))
(Flag218prime-)
(not (Flag196-))
(Flag196prime-)
(not (Flag257-))
(Flag257prime-)
(not (Flag219-))
(Flag219prime-)
(not (Flag258-))
(Flag258prime-)
(not (Flag249-))
(Flag249prime-)
(not (Flag280-))
(Flag280prime-)
(not (Flag281-))
(Flag281prime-)
(not (Flag282-))
(Flag282prime-)
(not (Flag283-))
(Flag283prime-)
(not (Flag187-))
(Flag187prime-)
(not (Flag220-))
(Flag220prime-)
(not (Flag247-))
(Flag247prime-)
(not (Flag205-))
(Flag205prime-)
(not (Flag209-))
(Flag209prime-)
(not (Flag271-))
(Flag271prime-)
(not (Flag252-))
(Flag252prime-)
(not (Flag253-))
(Flag253prime-)
(not (Flag233-))
(Flag233prime-)
(not (Flag197-))
(Flag197prime-)
(not (Flag224-))
(Flag224prime-)
(not (Flag234-))
(Flag234prime-)
(not (Flag235-))
(Flag235prime-)
(not (Flag260-))
(Flag260prime-)
(not (Flag225-))
(Flag225prime-)
(not (Flag272-))
(Flag272prime-)
(not (Flag274-))
(Flag274prime-)
(not (Flag261-))
(Flag261prime-)
(not (Flag236-))
(Flag236prime-)
(not (Flag190-))
(Flag190prime-)
(not (Flag263-))
(Flag263prime-)
(not (Flag275-))
(Flag275prime-)
(not (Flag211-))
(Flag211prime-)
(not (Flag239-))
(Flag239prime-)
(not (Flag284-))
(Flag284prime-)
(not (Flag241-))
(Flag241prime-)
(not (Flag226-))
(Flag226prime-)
(not (Flag264-))
(Flag264prime-)
)
:effect
(and
(Flag285prime-)
(not (Flag285-))
)

)
(:action Flag292Action-0
:parameters ()
:precondition
(and
(Flag243-)
(Flag243prime-)
)
:effect
(and
(Flag292-)
(Flag292prime-)
)

)
(:action Flag292Action-1
:parameters ()
:precondition
(and
(Flag201-)
(Flag201prime-)
)
:effect
(and
(Flag292-)
(Flag292prime-)
)

)
(:action Flag292Action-2
:parameters ()
:precondition
(and
(Flag202-)
(Flag202prime-)
)
:effect
(and
(Flag292-)
(Flag292prime-)
)

)
(:action Flag292Action-3
:parameters ()
:precondition
(and
(Flag189-)
(Flag189prime-)
)
:effect
(and
(Flag292-)
(Flag292prime-)
)

)
(:action Flag292Action-4
:parameters ()
:precondition
(and
(Flag251-)
(Flag251prime-)
)
:effect
(and
(Flag292-)
(Flag292prime-)
)

)
(:action Flag292Action-5
:parameters ()
:precondition
(and
(Flag266-)
(Flag266prime-)
)
:effect
(and
(Flag292-)
(Flag292prime-)
)

)
(:action Flag292Action-6
:parameters ()
:precondition
(and
(Flag267-)
(Flag267prime-)
)
:effect
(and
(Flag292-)
(Flag292prime-)
)

)
(:action Flag292Action-7
:parameters ()
:precondition
(and
(Flag229-)
(Flag229prime-)
)
:effect
(and
(Flag292-)
(Flag292prime-)
)

)
(:action Flag292Action-8
:parameters ()
:precondition
(and
(Flag210-)
(Flag210prime-)
)
:effect
(and
(Flag292-)
(Flag292prime-)
)

)
(:action Flag292Action-9
:parameters ()
:precondition
(and
(Flag208-)
(Flag208prime-)
)
:effect
(and
(Flag292-)
(Flag292prime-)
)

)
(:action Flag292Action-10
:parameters ()
:precondition
(and
(Flag278-)
(Flag278prime-)
)
:effect
(and
(Flag292-)
(Flag292prime-)
)

)
(:action Flag292Action-11
:parameters ()
:precondition
(and
(Flag199-)
(Flag199prime-)
)
:effect
(and
(Flag292-)
(Flag292prime-)
)

)
(:action Flag292Action-12
:parameters ()
:precondition
(and
(Flag245-)
(Flag245prime-)
)
:effect
(and
(Flag292-)
(Flag292prime-)
)

)
(:action Flag292Action-13
:parameters ()
:precondition
(and
(Flag286-)
(Flag286prime-)
)
:effect
(and
(Flag292-)
(Flag292prime-)
)

)
(:action Flag292Action-14
:parameters ()
:precondition
(and
(Flag287-)
(Flag287prime-)
)
:effect
(and
(Flag292-)
(Flag292prime-)
)

)
(:action Flag292Action-15
:parameters ()
:precondition
(and
(Flag279-)
(Flag279prime-)
)
:effect
(and
(Flag292-)
(Flag292prime-)
)

)
(:action Flag292Action-16
:parameters ()
:precondition
(and
(Flag246-)
(Flag246prime-)
)
:effect
(and
(Flag292-)
(Flag292prime-)
)

)
(:action Flag292Action-17
:parameters ()
:precondition
(and
(Flag247-)
(Flag247prime-)
)
:effect
(and
(Flag292-)
(Flag292prime-)
)

)
(:action Flag292Action-18
:parameters ()
:precondition
(and
(Flag288-)
(Flag288prime-)
)
:effect
(and
(Flag292-)
(Flag292prime-)
)

)
(:action Flag292Action-19
:parameters ()
:precondition
(and
(Flag269-)
(Flag269prime-)
)
:effect
(and
(Flag292-)
(Flag292prime-)
)

)
(:action Flag292Action-20
:parameters ()
:precondition
(and
(Flag256-)
(Flag256prime-)
)
:effect
(and
(Flag292-)
(Flag292prime-)
)

)
(:action Flag292Action-21
:parameters ()
:precondition
(and
(Flag257-)
(Flag257prime-)
)
:effect
(and
(Flag292-)
(Flag292prime-)
)

)
(:action Flag292Action-22
:parameters ()
:precondition
(and
(Flag196-)
(Flag196prime-)
)
:effect
(and
(Flag292-)
(Flag292prime-)
)

)
(:action Flag292Action-23
:parameters ()
:precondition
(and
(Flag219-)
(Flag219prime-)
)
:effect
(and
(Flag292-)
(Flag292prime-)
)

)
(:action Flag292Action-24
:parameters ()
:precondition
(and
(Flag258-)
(Flag258prime-)
)
:effect
(and
(Flag292-)
(Flag292prime-)
)

)
(:action Flag292Action-25
:parameters ()
:precondition
(and
(Flag249-)
(Flag249prime-)
)
:effect
(and
(Flag292-)
(Flag292prime-)
)

)
(:action Flag292Action-26
:parameters ()
:precondition
(and
(Flag289-)
(Flag289prime-)
)
:effect
(and
(Flag292-)
(Flag292prime-)
)

)
(:action Flag292Action-27
:parameters ()
:precondition
(and
(Flag280-)
(Flag280prime-)
)
:effect
(and
(Flag292-)
(Flag292prime-)
)

)
(:action Flag292Action-28
:parameters ()
:precondition
(and
(Flag281-)
(Flag281prime-)
)
:effect
(and
(Flag292-)
(Flag292prime-)
)

)
(:action Flag292Action-29
:parameters ()
:precondition
(and
(Flag283-)
(Flag283prime-)
)
:effect
(and
(Flag292-)
(Flag292prime-)
)

)
(:action Flag292Action-30
:parameters ()
:precondition
(and
(Flag187-)
(Flag187prime-)
)
:effect
(and
(Flag292-)
(Flag292prime-)
)

)
(:action Flag292Action-31
:parameters ()
:precondition
(and
(Flag220-)
(Flag220prime-)
)
:effect
(and
(Flag292-)
(Flag292prime-)
)

)
(:action Flag292Action-32
:parameters ()
:precondition
(and
(Flag221-)
(Flag221prime-)
)
:effect
(and
(Flag292-)
(Flag292prime-)
)

)
(:action Flag292Action-33
:parameters ()
:precondition
(and
(Flag209-)
(Flag209prime-)
)
:effect
(and
(Flag292-)
(Flag292prime-)
)

)
(:action Flag292Action-34
:parameters ()
:precondition
(and
(Flag271-)
(Flag271prime-)
)
:effect
(and
(Flag292-)
(Flag292prime-)
)

)
(:action Flag292Action-35
:parameters ()
:precondition
(and
(Flag252-)
(Flag252prime-)
)
:effect
(and
(Flag292-)
(Flag292prime-)
)

)
(:action Flag292Action-36
:parameters ()
:precondition
(and
(Flag290-)
(Flag290prime-)
)
:effect
(and
(Flag292-)
(Flag292prime-)
)

)
(:action Flag292Action-37
:parameters ()
:precondition
(and
(Flag253-)
(Flag253prime-)
)
:effect
(and
(Flag292-)
(Flag292prime-)
)

)
(:action Flag292Action-38
:parameters ()
:precondition
(and
(Flag291-)
(Flag291prime-)
)
:effect
(and
(Flag292-)
(Flag292prime-)
)

)
(:action Flag292Action-39
:parameters ()
:precondition
(and
(Flag197-)
(Flag197prime-)
)
:effect
(and
(Flag292-)
(Flag292prime-)
)

)
(:action Flag292Action-40
:parameters ()
:precondition
(and
(Flag224-)
(Flag224prime-)
)
:effect
(and
(Flag292-)
(Flag292prime-)
)

)
(:action Flag292Action-41
:parameters ()
:precondition
(and
(Flag234-)
(Flag234prime-)
)
:effect
(and
(Flag292-)
(Flag292prime-)
)

)
(:action Flag292Action-42
:parameters ()
:precondition
(and
(Flag235-)
(Flag235prime-)
)
:effect
(and
(Flag292-)
(Flag292prime-)
)

)
(:action Flag292Action-43
:parameters ()
:precondition
(and
(Flag225-)
(Flag225prime-)
)
:effect
(and
(Flag292-)
(Flag292prime-)
)

)
(:action Flag292Action-44
:parameters ()
:precondition
(and
(Flag272-)
(Flag272prime-)
)
:effect
(and
(Flag292-)
(Flag292prime-)
)

)
(:action Flag292Action-45
:parameters ()
:precondition
(and
(Flag274-)
(Flag274prime-)
)
:effect
(and
(Flag292-)
(Flag292prime-)
)

)
(:action Flag292Action-46
:parameters ()
:precondition
(and
(Flag261-)
(Flag261prime-)
)
:effect
(and
(Flag292-)
(Flag292prime-)
)

)
(:action Flag292Action-47
:parameters ()
:precondition
(and
(Flag236-)
(Flag236prime-)
)
:effect
(and
(Flag292-)
(Flag292prime-)
)

)
(:action Flag292Action-48
:parameters ()
:precondition
(and
(Flag190-)
(Flag190prime-)
)
:effect
(and
(Flag292-)
(Flag292prime-)
)

)
(:action Flag292Action-49
:parameters ()
:precondition
(and
(Flag263-)
(Flag263prime-)
)
:effect
(and
(Flag292-)
(Flag292prime-)
)

)
(:action Flag292Action-50
:parameters ()
:precondition
(and
(Flag275-)
(Flag275prime-)
)
:effect
(and
(Flag292-)
(Flag292prime-)
)

)
(:action Flag292Action-51
:parameters ()
:precondition
(and
(Flag211-)
(Flag211prime-)
)
:effect
(and
(Flag292-)
(Flag292prime-)
)

)
(:action Flag292Action-52
:parameters ()
:precondition
(and
(Flag239-)
(Flag239prime-)
)
:effect
(and
(Flag292-)
(Flag292prime-)
)

)
(:action Flag292Action-53
:parameters ()
:precondition
(and
(Flag284-)
(Flag284prime-)
)
:effect
(and
(Flag292-)
(Flag292prime-)
)

)
(:action Flag292Action-54
:parameters ()
:precondition
(and
(Flag241-)
(Flag241prime-)
)
:effect
(and
(Flag292-)
(Flag292prime-)
)

)
(:action Flag292Action-55
:parameters ()
:precondition
(and
(Flag226-)
(Flag226prime-)
)
:effect
(and
(Flag292-)
(Flag292prime-)
)

)
(:action Flag292Action-56
:parameters ()
:precondition
(and
(Flag264-)
(Flag264prime-)
)
:effect
(and
(Flag292-)
(Flag292prime-)
)

)
(:action Prim292Action
:parameters ()
:precondition
(and
(not (Flag243-))
(Flag243prime-)
(not (Flag201-))
(Flag201prime-)
(not (Flag202-))
(Flag202prime-)
(not (Flag189-))
(Flag189prime-)
(not (Flag251-))
(Flag251prime-)
(not (Flag266-))
(Flag266prime-)
(not (Flag267-))
(Flag267prime-)
(not (Flag229-))
(Flag229prime-)
(not (Flag210-))
(Flag210prime-)
(not (Flag208-))
(Flag208prime-)
(not (Flag278-))
(Flag278prime-)
(not (Flag199-))
(Flag199prime-)
(not (Flag245-))
(Flag245prime-)
(not (Flag286-))
(Flag286prime-)
(not (Flag287-))
(Flag287prime-)
(not (Flag279-))
(Flag279prime-)
(not (Flag246-))
(Flag246prime-)
(not (Flag247-))
(Flag247prime-)
(not (Flag288-))
(Flag288prime-)
(not (Flag269-))
(Flag269prime-)
(not (Flag256-))
(Flag256prime-)
(not (Flag257-))
(Flag257prime-)
(not (Flag196-))
(Flag196prime-)
(not (Flag219-))
(Flag219prime-)
(not (Flag258-))
(Flag258prime-)
(not (Flag249-))
(Flag249prime-)
(not (Flag289-))
(Flag289prime-)
(not (Flag280-))
(Flag280prime-)
(not (Flag281-))
(Flag281prime-)
(not (Flag283-))
(Flag283prime-)
(not (Flag187-))
(Flag187prime-)
(not (Flag220-))
(Flag220prime-)
(not (Flag221-))
(Flag221prime-)
(not (Flag209-))
(Flag209prime-)
(not (Flag271-))
(Flag271prime-)
(not (Flag252-))
(Flag252prime-)
(not (Flag290-))
(Flag290prime-)
(not (Flag253-))
(Flag253prime-)
(not (Flag291-))
(Flag291prime-)
(not (Flag197-))
(Flag197prime-)
(not (Flag224-))
(Flag224prime-)
(not (Flag234-))
(Flag234prime-)
(not (Flag235-))
(Flag235prime-)
(not (Flag225-))
(Flag225prime-)
(not (Flag272-))
(Flag272prime-)
(not (Flag274-))
(Flag274prime-)
(not (Flag261-))
(Flag261prime-)
(not (Flag236-))
(Flag236prime-)
(not (Flag190-))
(Flag190prime-)
(not (Flag263-))
(Flag263prime-)
(not (Flag275-))
(Flag275prime-)
(not (Flag211-))
(Flag211prime-)
(not (Flag239-))
(Flag239prime-)
(not (Flag284-))
(Flag284prime-)
(not (Flag241-))
(Flag241prime-)
(not (Flag226-))
(Flag226prime-)
(not (Flag264-))
(Flag264prime-)
)
:effect
(and
(Flag292prime-)
(not (Flag292-))
)

)
(:action Flag298Action-0
:parameters ()
:precondition
(and
(Flag243-)
(Flag243prime-)
)
:effect
(and
(Flag298-)
(Flag298prime-)
)

)
(:action Flag298Action-1
:parameters ()
:precondition
(and
(Flag201-)
(Flag201prime-)
)
:effect
(and
(Flag298-)
(Flag298prime-)
)

)
(:action Flag298Action-2
:parameters ()
:precondition
(and
(Flag202-)
(Flag202prime-)
)
:effect
(and
(Flag298-)
(Flag298prime-)
)

)
(:action Flag298Action-3
:parameters ()
:precondition
(and
(Flag189-)
(Flag189prime-)
)
:effect
(and
(Flag298-)
(Flag298prime-)
)

)
(:action Flag298Action-4
:parameters ()
:precondition
(and
(Flag266-)
(Flag266prime-)
)
:effect
(and
(Flag298-)
(Flag298prime-)
)

)
(:action Flag298Action-5
:parameters ()
:precondition
(and
(Flag293-)
(Flag293prime-)
)
:effect
(and
(Flag298-)
(Flag298prime-)
)

)
(:action Flag298Action-6
:parameters ()
:precondition
(and
(Flag294-)
(Flag294prime-)
)
:effect
(and
(Flag298-)
(Flag298prime-)
)

)
(:action Flag298Action-7
:parameters ()
:precondition
(and
(Flag295-)
(Flag295prime-)
)
:effect
(and
(Flag298-)
(Flag298prime-)
)

)
(:action Flag298Action-8
:parameters ()
:precondition
(and
(Flag229-)
(Flag229prime-)
)
:effect
(and
(Flag298-)
(Flag298prime-)
)

)
(:action Flag298Action-9
:parameters ()
:precondition
(and
(Flag210-)
(Flag210prime-)
)
:effect
(and
(Flag298-)
(Flag298prime-)
)

)
(:action Flag298Action-10
:parameters ()
:precondition
(and
(Flag208-)
(Flag208prime-)
)
:effect
(and
(Flag298-)
(Flag298prime-)
)

)
(:action Flag298Action-11
:parameters ()
:precondition
(and
(Flag278-)
(Flag278prime-)
)
:effect
(and
(Flag298-)
(Flag298prime-)
)

)
(:action Flag298Action-12
:parameters ()
:precondition
(and
(Flag296-)
(Flag296prime-)
)
:effect
(and
(Flag298-)
(Flag298prime-)
)

)
(:action Flag298Action-13
:parameters ()
:precondition
(and
(Flag199-)
(Flag199prime-)
)
:effect
(and
(Flag298-)
(Flag298prime-)
)

)
(:action Flag298Action-14
:parameters ()
:precondition
(and
(Flag245-)
(Flag245prime-)
)
:effect
(and
(Flag298-)
(Flag298prime-)
)

)
(:action Flag298Action-15
:parameters ()
:precondition
(and
(Flag286-)
(Flag286prime-)
)
:effect
(and
(Flag298-)
(Flag298prime-)
)

)
(:action Flag298Action-16
:parameters ()
:precondition
(and
(Flag279-)
(Flag279prime-)
)
:effect
(and
(Flag298-)
(Flag298prime-)
)

)
(:action Flag298Action-17
:parameters ()
:precondition
(and
(Flag246-)
(Flag246prime-)
)
:effect
(and
(Flag298-)
(Flag298prime-)
)

)
(:action Flag298Action-18
:parameters ()
:precondition
(and
(Flag247-)
(Flag247prime-)
)
:effect
(and
(Flag298-)
(Flag298prime-)
)

)
(:action Flag298Action-19
:parameters ()
:precondition
(and
(Flag288-)
(Flag288prime-)
)
:effect
(and
(Flag298-)
(Flag298prime-)
)

)
(:action Flag298Action-20
:parameters ()
:precondition
(and
(Flag269-)
(Flag269prime-)
)
:effect
(and
(Flag298-)
(Flag298prime-)
)

)
(:action Flag298Action-21
:parameters ()
:precondition
(and
(Flag256-)
(Flag256prime-)
)
:effect
(and
(Flag298-)
(Flag298prime-)
)

)
(:action Flag298Action-22
:parameters ()
:precondition
(and
(Flag196-)
(Flag196prime-)
)
:effect
(and
(Flag298-)
(Flag298prime-)
)

)
(:action Flag298Action-23
:parameters ()
:precondition
(and
(Flag219-)
(Flag219prime-)
)
:effect
(and
(Flag298-)
(Flag298prime-)
)

)
(:action Flag298Action-24
:parameters ()
:precondition
(and
(Flag258-)
(Flag258prime-)
)
:effect
(and
(Flag298-)
(Flag298prime-)
)

)
(:action Flag298Action-25
:parameters ()
:precondition
(and
(Flag289-)
(Flag289prime-)
)
:effect
(and
(Flag298-)
(Flag298prime-)
)

)
(:action Flag298Action-26
:parameters ()
:precondition
(and
(Flag280-)
(Flag280prime-)
)
:effect
(and
(Flag298-)
(Flag298prime-)
)

)
(:action Flag298Action-27
:parameters ()
:precondition
(and
(Flag251-)
(Flag251prime-)
)
:effect
(and
(Flag298-)
(Flag298prime-)
)

)
(:action Flag298Action-28
:parameters ()
:precondition
(and
(Flag283-)
(Flag283prime-)
)
:effect
(and
(Flag298-)
(Flag298prime-)
)

)
(:action Flag298Action-29
:parameters ()
:precondition
(and
(Flag187-)
(Flag187prime-)
)
:effect
(and
(Flag298-)
(Flag298prime-)
)

)
(:action Flag298Action-30
:parameters ()
:precondition
(and
(Flag220-)
(Flag220prime-)
)
:effect
(and
(Flag298-)
(Flag298prime-)
)

)
(:action Flag298Action-31
:parameters ()
:precondition
(and
(Flag271-)
(Flag271prime-)
)
:effect
(and
(Flag298-)
(Flag298prime-)
)

)
(:action Flag298Action-32
:parameters ()
:precondition
(and
(Flag209-)
(Flag209prime-)
)
:effect
(and
(Flag298-)
(Flag298prime-)
)

)
(:action Flag298Action-33
:parameters ()
:precondition
(and
(Flag297-)
(Flag297prime-)
)
:effect
(and
(Flag298-)
(Flag298prime-)
)

)
(:action Flag298Action-34
:parameters ()
:precondition
(and
(Flag252-)
(Flag252prime-)
)
:effect
(and
(Flag298-)
(Flag298prime-)
)

)
(:action Flag298Action-35
:parameters ()
:precondition
(and
(Flag291-)
(Flag291prime-)
)
:effect
(and
(Flag298-)
(Flag298prime-)
)

)
(:action Flag298Action-36
:parameters ()
:precondition
(and
(Flag290-)
(Flag290prime-)
)
:effect
(and
(Flag298-)
(Flag298prime-)
)

)
(:action Flag298Action-37
:parameters ()
:precondition
(and
(Flag224-)
(Flag224prime-)
)
:effect
(and
(Flag298-)
(Flag298prime-)
)

)
(:action Flag298Action-38
:parameters ()
:precondition
(and
(Flag234-)
(Flag234prime-)
)
:effect
(and
(Flag298-)
(Flag298prime-)
)

)
(:action Flag298Action-39
:parameters ()
:precondition
(and
(Flag235-)
(Flag235prime-)
)
:effect
(and
(Flag298-)
(Flag298prime-)
)

)
(:action Flag298Action-40
:parameters ()
:precondition
(and
(Flag225-)
(Flag225prime-)
)
:effect
(and
(Flag298-)
(Flag298prime-)
)

)
(:action Flag298Action-41
:parameters ()
:precondition
(and
(Flag272-)
(Flag272prime-)
)
:effect
(and
(Flag298-)
(Flag298prime-)
)

)
(:action Flag298Action-42
:parameters ()
:precondition
(and
(Flag274-)
(Flag274prime-)
)
:effect
(and
(Flag298-)
(Flag298prime-)
)

)
(:action Flag298Action-43
:parameters ()
:precondition
(and
(Flag261-)
(Flag261prime-)
)
:effect
(and
(Flag298-)
(Flag298prime-)
)

)
(:action Flag298Action-44
:parameters ()
:precondition
(and
(Flag236-)
(Flag236prime-)
)
:effect
(and
(Flag298-)
(Flag298prime-)
)

)
(:action Flag298Action-45
:parameters ()
:precondition
(and
(Flag190-)
(Flag190prime-)
)
:effect
(and
(Flag298-)
(Flag298prime-)
)

)
(:action Flag298Action-46
:parameters ()
:precondition
(and
(Flag263-)
(Flag263prime-)
)
:effect
(and
(Flag298-)
(Flag298prime-)
)

)
(:action Flag298Action-47
:parameters ()
:precondition
(and
(Flag275-)
(Flag275prime-)
)
:effect
(and
(Flag298-)
(Flag298prime-)
)

)
(:action Flag298Action-48
:parameters ()
:precondition
(and
(Flag239-)
(Flag239prime-)
)
:effect
(and
(Flag298-)
(Flag298prime-)
)

)
(:action Flag298Action-49
:parameters ()
:precondition
(and
(Flag284-)
(Flag284prime-)
)
:effect
(and
(Flag298-)
(Flag298prime-)
)

)
(:action Flag298Action-50
:parameters ()
:precondition
(and
(Flag241-)
(Flag241prime-)
)
:effect
(and
(Flag298-)
(Flag298prime-)
)

)
(:action Flag298Action-51
:parameters ()
:precondition
(and
(Flag226-)
(Flag226prime-)
)
:effect
(and
(Flag298-)
(Flag298prime-)
)

)
(:action Flag298Action-52
:parameters ()
:precondition
(and
(Flag264-)
(Flag264prime-)
)
:effect
(and
(Flag298-)
(Flag298prime-)
)

)
(:action Prim298Action
:parameters ()
:precondition
(and
(not (Flag243-))
(Flag243prime-)
(not (Flag201-))
(Flag201prime-)
(not (Flag202-))
(Flag202prime-)
(not (Flag189-))
(Flag189prime-)
(not (Flag266-))
(Flag266prime-)
(not (Flag293-))
(Flag293prime-)
(not (Flag294-))
(Flag294prime-)
(not (Flag295-))
(Flag295prime-)
(not (Flag229-))
(Flag229prime-)
(not (Flag210-))
(Flag210prime-)
(not (Flag208-))
(Flag208prime-)
(not (Flag278-))
(Flag278prime-)
(not (Flag296-))
(Flag296prime-)
(not (Flag199-))
(Flag199prime-)
(not (Flag245-))
(Flag245prime-)
(not (Flag286-))
(Flag286prime-)
(not (Flag279-))
(Flag279prime-)
(not (Flag246-))
(Flag246prime-)
(not (Flag247-))
(Flag247prime-)
(not (Flag288-))
(Flag288prime-)
(not (Flag269-))
(Flag269prime-)
(not (Flag256-))
(Flag256prime-)
(not (Flag196-))
(Flag196prime-)
(not (Flag219-))
(Flag219prime-)
(not (Flag258-))
(Flag258prime-)
(not (Flag289-))
(Flag289prime-)
(not (Flag280-))
(Flag280prime-)
(not (Flag251-))
(Flag251prime-)
(not (Flag283-))
(Flag283prime-)
(not (Flag187-))
(Flag187prime-)
(not (Flag220-))
(Flag220prime-)
(not (Flag271-))
(Flag271prime-)
(not (Flag209-))
(Flag209prime-)
(not (Flag297-))
(Flag297prime-)
(not (Flag252-))
(Flag252prime-)
(not (Flag291-))
(Flag291prime-)
(not (Flag290-))
(Flag290prime-)
(not (Flag224-))
(Flag224prime-)
(not (Flag234-))
(Flag234prime-)
(not (Flag235-))
(Flag235prime-)
(not (Flag225-))
(Flag225prime-)
(not (Flag272-))
(Flag272prime-)
(not (Flag274-))
(Flag274prime-)
(not (Flag261-))
(Flag261prime-)
(not (Flag236-))
(Flag236prime-)
(not (Flag190-))
(Flag190prime-)
(not (Flag263-))
(Flag263prime-)
(not (Flag275-))
(Flag275prime-)
(not (Flag239-))
(Flag239prime-)
(not (Flag284-))
(Flag284prime-)
(not (Flag241-))
(Flag241prime-)
(not (Flag226-))
(Flag226prime-)
(not (Flag264-))
(Flag264prime-)
)
:effect
(and
(Flag298prime-)
(not (Flag298-))
)

)
(:action Flag303Action-0
:parameters ()
:precondition
(and
(Flag210-)
(Flag210prime-)
)
:effect
(and
(Flag303-)
(Flag303prime-)
)

)
(:action Flag303Action-1
:parameters ()
:precondition
(and
(Flag201-)
(Flag201prime-)
)
:effect
(and
(Flag303-)
(Flag303prime-)
)

)
(:action Flag303Action-2
:parameters ()
:precondition
(and
(Flag266-)
(Flag266prime-)
)
:effect
(and
(Flag303-)
(Flag303prime-)
)

)
(:action Flag303Action-3
:parameters ()
:precondition
(and
(Flag299-)
(Flag299prime-)
)
:effect
(and
(Flag303-)
(Flag303prime-)
)

)
(:action Flag303Action-4
:parameters ()
:precondition
(and
(Flag294-)
(Flag294prime-)
)
:effect
(and
(Flag303-)
(Flag303prime-)
)

)
(:action Flag303Action-5
:parameters ()
:precondition
(and
(Flag295-)
(Flag295prime-)
)
:effect
(and
(Flag303-)
(Flag303prime-)
)

)
(:action Flag303Action-6
:parameters ()
:precondition
(and
(Flag229-)
(Flag229prime-)
)
:effect
(and
(Flag303-)
(Flag303prime-)
)

)
(:action Flag303Action-7
:parameters ()
:precondition
(and
(Flag208-)
(Flag208prime-)
)
:effect
(and
(Flag303-)
(Flag303prime-)
)

)
(:action Flag303Action-8
:parameters ()
:precondition
(and
(Flag293-)
(Flag293prime-)
)
:effect
(and
(Flag303-)
(Flag303prime-)
)

)
(:action Flag303Action-9
:parameters ()
:precondition
(and
(Flag296-)
(Flag296prime-)
)
:effect
(and
(Flag303-)
(Flag303prime-)
)

)
(:action Flag303Action-10
:parameters ()
:precondition
(and
(Flag199-)
(Flag199prime-)
)
:effect
(and
(Flag303-)
(Flag303prime-)
)

)
(:action Flag303Action-11
:parameters ()
:precondition
(and
(Flag245-)
(Flag245prime-)
)
:effect
(and
(Flag303-)
(Flag303prime-)
)

)
(:action Flag303Action-12
:parameters ()
:precondition
(and
(Flag286-)
(Flag286prime-)
)
:effect
(and
(Flag303-)
(Flag303prime-)
)

)
(:action Flag303Action-13
:parameters ()
:precondition
(and
(Flag279-)
(Flag279prime-)
)
:effect
(and
(Flag303-)
(Flag303prime-)
)

)
(:action Flag303Action-14
:parameters ()
:precondition
(and
(Flag246-)
(Flag246prime-)
)
:effect
(and
(Flag303-)
(Flag303prime-)
)

)
(:action Flag303Action-15
:parameters ()
:precondition
(and
(Flag269-)
(Flag269prime-)
)
:effect
(and
(Flag303-)
(Flag303prime-)
)

)
(:action Flag303Action-16
:parameters ()
:precondition
(and
(Flag256-)
(Flag256prime-)
)
:effect
(and
(Flag303-)
(Flag303prime-)
)

)
(:action Flag303Action-17
:parameters ()
:precondition
(and
(Flag196-)
(Flag196prime-)
)
:effect
(and
(Flag303-)
(Flag303prime-)
)

)
(:action Flag303Action-18
:parameters ()
:precondition
(and
(Flag219-)
(Flag219prime-)
)
:effect
(and
(Flag303-)
(Flag303prime-)
)

)
(:action Flag303Action-19
:parameters ()
:precondition
(and
(Flag258-)
(Flag258prime-)
)
:effect
(and
(Flag303-)
(Flag303prime-)
)

)
(:action Flag303Action-20
:parameters ()
:precondition
(and
(Flag289-)
(Flag289prime-)
)
:effect
(and
(Flag303-)
(Flag303prime-)
)

)
(:action Flag303Action-21
:parameters ()
:precondition
(and
(Flag280-)
(Flag280prime-)
)
:effect
(and
(Flag303-)
(Flag303prime-)
)

)
(:action Flag303Action-22
:parameters ()
:precondition
(and
(Flag251-)
(Flag251prime-)
)
:effect
(and
(Flag303-)
(Flag303prime-)
)

)
(:action Flag303Action-23
:parameters ()
:precondition
(and
(Flag283-)
(Flag283prime-)
)
:effect
(and
(Flag303-)
(Flag303prime-)
)

)
(:action Flag303Action-24
:parameters ()
:precondition
(and
(Flag187-)
(Flag187prime-)
)
:effect
(and
(Flag303-)
(Flag303prime-)
)

)
(:action Flag303Action-25
:parameters ()
:precondition
(and
(Flag220-)
(Flag220prime-)
)
:effect
(and
(Flag303-)
(Flag303prime-)
)

)
(:action Flag303Action-26
:parameters ()
:precondition
(and
(Flag247-)
(Flag247prime-)
)
:effect
(and
(Flag303-)
(Flag303prime-)
)

)
(:action Flag303Action-27
:parameters ()
:precondition
(and
(Flag209-)
(Flag209prime-)
)
:effect
(and
(Flag303-)
(Flag303prime-)
)

)
(:action Flag303Action-28
:parameters ()
:precondition
(and
(Flag271-)
(Flag271prime-)
)
:effect
(and
(Flag303-)
(Flag303prime-)
)

)
(:action Flag303Action-29
:parameters ()
:precondition
(and
(Flag252-)
(Flag252prime-)
)
:effect
(and
(Flag303-)
(Flag303prime-)
)

)
(:action Flag303Action-30
:parameters ()
:precondition
(and
(Flag291-)
(Flag291prime-)
)
:effect
(and
(Flag303-)
(Flag303prime-)
)

)
(:action Flag303Action-31
:parameters ()
:precondition
(and
(Flag300-)
(Flag300prime-)
)
:effect
(and
(Flag303-)
(Flag303prime-)
)

)
(:action Flag303Action-32
:parameters ()
:precondition
(and
(Flag290-)
(Flag290prime-)
)
:effect
(and
(Flag303-)
(Flag303prime-)
)

)
(:action Flag303Action-33
:parameters ()
:precondition
(and
(Flag224-)
(Flag224prime-)
)
:effect
(and
(Flag303-)
(Flag303prime-)
)

)
(:action Flag303Action-34
:parameters ()
:precondition
(and
(Flag234-)
(Flag234prime-)
)
:effect
(and
(Flag303-)
(Flag303prime-)
)

)
(:action Flag303Action-35
:parameters ()
:precondition
(and
(Flag225-)
(Flag225prime-)
)
:effect
(and
(Flag303-)
(Flag303prime-)
)

)
(:action Flag303Action-36
:parameters ()
:precondition
(and
(Flag274-)
(Flag274prime-)
)
:effect
(and
(Flag303-)
(Flag303prime-)
)

)
(:action Flag303Action-37
:parameters ()
:precondition
(and
(Flag261-)
(Flag261prime-)
)
:effect
(and
(Flag303-)
(Flag303prime-)
)

)
(:action Flag303Action-38
:parameters ()
:precondition
(and
(Flag190-)
(Flag190prime-)
)
:effect
(and
(Flag303-)
(Flag303prime-)
)

)
(:action Flag303Action-39
:parameters ()
:precondition
(and
(Flag263-)
(Flag263prime-)
)
:effect
(and
(Flag303-)
(Flag303prime-)
)

)
(:action Flag303Action-40
:parameters ()
:precondition
(and
(Flag275-)
(Flag275prime-)
)
:effect
(and
(Flag303-)
(Flag303prime-)
)

)
(:action Flag303Action-41
:parameters ()
:precondition
(and
(Flag301-)
(Flag301prime-)
)
:effect
(and
(Flag303-)
(Flag303prime-)
)

)
(:action Flag303Action-42
:parameters ()
:precondition
(and
(Flag239-)
(Flag239prime-)
)
:effect
(and
(Flag303-)
(Flag303prime-)
)

)
(:action Flag303Action-43
:parameters ()
:precondition
(and
(Flag284-)
(Flag284prime-)
)
:effect
(and
(Flag303-)
(Flag303prime-)
)

)
(:action Flag303Action-44
:parameters ()
:precondition
(and
(Flag241-)
(Flag241prime-)
)
:effect
(and
(Flag303-)
(Flag303prime-)
)

)
(:action Flag303Action-45
:parameters ()
:precondition
(and
(Flag302-)
(Flag302prime-)
)
:effect
(and
(Flag303-)
(Flag303prime-)
)

)
(:action Prim303Action
:parameters ()
:precondition
(and
(not (Flag210-))
(Flag210prime-)
(not (Flag201-))
(Flag201prime-)
(not (Flag266-))
(Flag266prime-)
(not (Flag299-))
(Flag299prime-)
(not (Flag294-))
(Flag294prime-)
(not (Flag295-))
(Flag295prime-)
(not (Flag229-))
(Flag229prime-)
(not (Flag208-))
(Flag208prime-)
(not (Flag293-))
(Flag293prime-)
(not (Flag296-))
(Flag296prime-)
(not (Flag199-))
(Flag199prime-)
(not (Flag245-))
(Flag245prime-)
(not (Flag286-))
(Flag286prime-)
(not (Flag279-))
(Flag279prime-)
(not (Flag246-))
(Flag246prime-)
(not (Flag269-))
(Flag269prime-)
(not (Flag256-))
(Flag256prime-)
(not (Flag196-))
(Flag196prime-)
(not (Flag219-))
(Flag219prime-)
(not (Flag258-))
(Flag258prime-)
(not (Flag289-))
(Flag289prime-)
(not (Flag280-))
(Flag280prime-)
(not (Flag251-))
(Flag251prime-)
(not (Flag283-))
(Flag283prime-)
(not (Flag187-))
(Flag187prime-)
(not (Flag220-))
(Flag220prime-)
(not (Flag247-))
(Flag247prime-)
(not (Flag209-))
(Flag209prime-)
(not (Flag271-))
(Flag271prime-)
(not (Flag252-))
(Flag252prime-)
(not (Flag291-))
(Flag291prime-)
(not (Flag300-))
(Flag300prime-)
(not (Flag290-))
(Flag290prime-)
(not (Flag224-))
(Flag224prime-)
(not (Flag234-))
(Flag234prime-)
(not (Flag225-))
(Flag225prime-)
(not (Flag274-))
(Flag274prime-)
(not (Flag261-))
(Flag261prime-)
(not (Flag190-))
(Flag190prime-)
(not (Flag263-))
(Flag263prime-)
(not (Flag275-))
(Flag275prime-)
(not (Flag301-))
(Flag301prime-)
(not (Flag239-))
(Flag239prime-)
(not (Flag284-))
(Flag284prime-)
(not (Flag241-))
(Flag241prime-)
(not (Flag302-))
(Flag302prime-)
)
:effect
(and
(Flag303prime-)
(not (Flag303-))
)

)
(:action Flag307Action-0
:parameters ()
:precondition
(and
(Flag210-)
(Flag210prime-)
)
:effect
(and
(Flag307-)
(Flag307prime-)
)

)
(:action Flag307Action-1
:parameters ()
:precondition
(and
(Flag201-)
(Flag201prime-)
)
:effect
(and
(Flag307-)
(Flag307prime-)
)

)
(:action Flag307Action-2
:parameters ()
:precondition
(and
(Flag266-)
(Flag266prime-)
)
:effect
(and
(Flag307-)
(Flag307prime-)
)

)
(:action Flag307Action-3
:parameters ()
:precondition
(and
(Flag299-)
(Flag299prime-)
)
:effect
(and
(Flag307-)
(Flag307prime-)
)

)
(:action Flag307Action-4
:parameters ()
:precondition
(and
(Flag294-)
(Flag294prime-)
)
:effect
(and
(Flag307-)
(Flag307prime-)
)

)
(:action Flag307Action-5
:parameters ()
:precondition
(and
(Flag229-)
(Flag229prime-)
)
:effect
(and
(Flag307-)
(Flag307prime-)
)

)
(:action Flag307Action-6
:parameters ()
:precondition
(and
(Flag304-)
(Flag304prime-)
)
:effect
(and
(Flag307-)
(Flag307prime-)
)

)
(:action Flag307Action-7
:parameters ()
:precondition
(and
(Flag293-)
(Flag293prime-)
)
:effect
(and
(Flag307-)
(Flag307prime-)
)

)
(:action Flag307Action-8
:parameters ()
:precondition
(and
(Flag296-)
(Flag296prime-)
)
:effect
(and
(Flag307-)
(Flag307prime-)
)

)
(:action Flag307Action-9
:parameters ()
:precondition
(and
(Flag286-)
(Flag286prime-)
)
:effect
(and
(Flag307-)
(Flag307prime-)
)

)
(:action Flag307Action-10
:parameters ()
:precondition
(and
(Flag279-)
(Flag279prime-)
)
:effect
(and
(Flag307-)
(Flag307prime-)
)

)
(:action Flag307Action-11
:parameters ()
:precondition
(and
(Flag246-)
(Flag246prime-)
)
:effect
(and
(Flag307-)
(Flag307prime-)
)

)
(:action Flag307Action-12
:parameters ()
:precondition
(and
(Flag269-)
(Flag269prime-)
)
:effect
(and
(Flag307-)
(Flag307prime-)
)

)
(:action Flag307Action-13
:parameters ()
:precondition
(and
(Flag256-)
(Flag256prime-)
)
:effect
(and
(Flag307-)
(Flag307prime-)
)

)
(:action Flag307Action-14
:parameters ()
:precondition
(and
(Flag196-)
(Flag196prime-)
)
:effect
(and
(Flag307-)
(Flag307prime-)
)

)
(:action Flag307Action-15
:parameters ()
:precondition
(and
(Flag219-)
(Flag219prime-)
)
:effect
(and
(Flag307-)
(Flag307prime-)
)

)
(:action Flag307Action-16
:parameters ()
:precondition
(and
(Flag258-)
(Flag258prime-)
)
:effect
(and
(Flag307-)
(Flag307prime-)
)

)
(:action Flag307Action-17
:parameters ()
:precondition
(and
(Flag305-)
(Flag305prime-)
)
:effect
(and
(Flag307-)
(Flag307prime-)
)

)
(:action Flag307Action-18
:parameters ()
:precondition
(and
(Flag283-)
(Flag283prime-)
)
:effect
(and
(Flag307-)
(Flag307prime-)
)

)
(:action Flag307Action-19
:parameters ()
:precondition
(and
(Flag187-)
(Flag187prime-)
)
:effect
(and
(Flag307-)
(Flag307prime-)
)

)
(:action Flag307Action-20
:parameters ()
:precondition
(and
(Flag209-)
(Flag209prime-)
)
:effect
(and
(Flag307-)
(Flag307prime-)
)

)
(:action Flag307Action-21
:parameters ()
:precondition
(and
(Flag271-)
(Flag271prime-)
)
:effect
(and
(Flag307-)
(Flag307prime-)
)

)
(:action Flag307Action-22
:parameters ()
:precondition
(and
(Flag252-)
(Flag252prime-)
)
:effect
(and
(Flag307-)
(Flag307prime-)
)

)
(:action Flag307Action-23
:parameters ()
:precondition
(and
(Flag291-)
(Flag291prime-)
)
:effect
(and
(Flag307-)
(Flag307prime-)
)

)
(:action Flag307Action-24
:parameters ()
:precondition
(and
(Flag300-)
(Flag300prime-)
)
:effect
(and
(Flag307-)
(Flag307prime-)
)

)
(:action Flag307Action-25
:parameters ()
:precondition
(and
(Flag290-)
(Flag290prime-)
)
:effect
(and
(Flag307-)
(Flag307prime-)
)

)
(:action Flag307Action-26
:parameters ()
:precondition
(and
(Flag224-)
(Flag224prime-)
)
:effect
(and
(Flag307-)
(Flag307prime-)
)

)
(:action Flag307Action-27
:parameters ()
:precondition
(and
(Flag234-)
(Flag234prime-)
)
:effect
(and
(Flag307-)
(Flag307prime-)
)

)
(:action Flag307Action-28
:parameters ()
:precondition
(and
(Flag225-)
(Flag225prime-)
)
:effect
(and
(Flag307-)
(Flag307prime-)
)

)
(:action Flag307Action-29
:parameters ()
:precondition
(and
(Flag274-)
(Flag274prime-)
)
:effect
(and
(Flag307-)
(Flag307prime-)
)

)
(:action Flag307Action-30
:parameters ()
:precondition
(and
(Flag306-)
(Flag306prime-)
)
:effect
(and
(Flag307-)
(Flag307prime-)
)

)
(:action Flag307Action-31
:parameters ()
:precondition
(and
(Flag190-)
(Flag190prime-)
)
:effect
(and
(Flag307-)
(Flag307prime-)
)

)
(:action Flag307Action-32
:parameters ()
:precondition
(and
(Flag263-)
(Flag263prime-)
)
:effect
(and
(Flag307-)
(Flag307prime-)
)

)
(:action Flag307Action-33
:parameters ()
:precondition
(and
(Flag245-)
(Flag245prime-)
)
:effect
(and
(Flag307-)
(Flag307prime-)
)

)
(:action Flag307Action-34
:parameters ()
:precondition
(and
(Flag239-)
(Flag239prime-)
)
:effect
(and
(Flag307-)
(Flag307prime-)
)

)
(:action Flag307Action-35
:parameters ()
:precondition
(and
(Flag284-)
(Flag284prime-)
)
:effect
(and
(Flag307-)
(Flag307prime-)
)

)
(:action Flag307Action-36
:parameters ()
:precondition
(and
(Flag241-)
(Flag241prime-)
)
:effect
(and
(Flag307-)
(Flag307prime-)
)

)
(:action Flag307Action-37
:parameters ()
:precondition
(and
(Flag302-)
(Flag302prime-)
)
:effect
(and
(Flag307-)
(Flag307prime-)
)

)
(:action Prim307Action
:parameters ()
:precondition
(and
(not (Flag210-))
(Flag210prime-)
(not (Flag201-))
(Flag201prime-)
(not (Flag266-))
(Flag266prime-)
(not (Flag299-))
(Flag299prime-)
(not (Flag294-))
(Flag294prime-)
(not (Flag229-))
(Flag229prime-)
(not (Flag304-))
(Flag304prime-)
(not (Flag293-))
(Flag293prime-)
(not (Flag296-))
(Flag296prime-)
(not (Flag286-))
(Flag286prime-)
(not (Flag279-))
(Flag279prime-)
(not (Flag246-))
(Flag246prime-)
(not (Flag269-))
(Flag269prime-)
(not (Flag256-))
(Flag256prime-)
(not (Flag196-))
(Flag196prime-)
(not (Flag219-))
(Flag219prime-)
(not (Flag258-))
(Flag258prime-)
(not (Flag305-))
(Flag305prime-)
(not (Flag283-))
(Flag283prime-)
(not (Flag187-))
(Flag187prime-)
(not (Flag209-))
(Flag209prime-)
(not (Flag271-))
(Flag271prime-)
(not (Flag252-))
(Flag252prime-)
(not (Flag291-))
(Flag291prime-)
(not (Flag300-))
(Flag300prime-)
(not (Flag290-))
(Flag290prime-)
(not (Flag224-))
(Flag224prime-)
(not (Flag234-))
(Flag234prime-)
(not (Flag225-))
(Flag225prime-)
(not (Flag274-))
(Flag274prime-)
(not (Flag306-))
(Flag306prime-)
(not (Flag190-))
(Flag190prime-)
(not (Flag263-))
(Flag263prime-)
(not (Flag245-))
(Flag245prime-)
(not (Flag239-))
(Flag239prime-)
(not (Flag284-))
(Flag284prime-)
(not (Flag241-))
(Flag241prime-)
(not (Flag302-))
(Flag302prime-)
)
:effect
(and
(Flag307prime-)
(not (Flag307-))
)

)
(:action Flag311Action-0
:parameters ()
:precondition
(and
(Flag201-)
(Flag201prime-)
)
:effect
(and
(Flag311-)
(Flag311prime-)
)

)
(:action Flag311Action-1
:parameters ()
:precondition
(and
(Flag229-)
(Flag229prime-)
)
:effect
(and
(Flag311-)
(Flag311prime-)
)

)
(:action Flag311Action-2
:parameters ()
:precondition
(and
(Flag293-)
(Flag293prime-)
)
:effect
(and
(Flag311-)
(Flag311prime-)
)

)
(:action Flag311Action-3
:parameters ()
:precondition
(and
(Flag296-)
(Flag296prime-)
)
:effect
(and
(Flag311-)
(Flag311prime-)
)

)
(:action Flag311Action-4
:parameters ()
:precondition
(and
(Flag286-)
(Flag286prime-)
)
:effect
(and
(Flag311-)
(Flag311prime-)
)

)
(:action Flag311Action-5
:parameters ()
:precondition
(and
(Flag279-)
(Flag279prime-)
)
:effect
(and
(Flag311-)
(Flag311prime-)
)

)
(:action Flag311Action-6
:parameters ()
:precondition
(and
(Flag246-)
(Flag246prime-)
)
:effect
(and
(Flag311-)
(Flag311prime-)
)

)
(:action Flag311Action-7
:parameters ()
:precondition
(and
(Flag269-)
(Flag269prime-)
)
:effect
(and
(Flag311-)
(Flag311prime-)
)

)
(:action Flag311Action-8
:parameters ()
:precondition
(and
(Flag219-)
(Flag219prime-)
)
:effect
(and
(Flag311-)
(Flag311prime-)
)

)
(:action Flag311Action-9
:parameters ()
:precondition
(and
(Flag283-)
(Flag283prime-)
)
:effect
(and
(Flag311-)
(Flag311prime-)
)

)
(:action Flag311Action-10
:parameters ()
:precondition
(and
(Flag187-)
(Flag187prime-)
)
:effect
(and
(Flag311-)
(Flag311prime-)
)

)
(:action Flag311Action-11
:parameters ()
:precondition
(and
(Flag271-)
(Flag271prime-)
)
:effect
(and
(Flag311-)
(Flag311prime-)
)

)
(:action Flag311Action-12
:parameters ()
:precondition
(and
(Flag209-)
(Flag209prime-)
)
:effect
(and
(Flag311-)
(Flag311prime-)
)

)
(:action Flag311Action-13
:parameters ()
:precondition
(and
(Flag308-)
(Flag308prime-)
)
:effect
(and
(Flag311-)
(Flag311prime-)
)

)
(:action Flag311Action-14
:parameters ()
:precondition
(and
(Flag252-)
(Flag252prime-)
)
:effect
(and
(Flag311-)
(Flag311prime-)
)

)
(:action Flag311Action-15
:parameters ()
:precondition
(and
(Flag291-)
(Flag291prime-)
)
:effect
(and
(Flag311-)
(Flag311prime-)
)

)
(:action Flag311Action-16
:parameters ()
:precondition
(and
(Flag300-)
(Flag300prime-)
)
:effect
(and
(Flag311-)
(Flag311prime-)
)

)
(:action Flag311Action-17
:parameters ()
:precondition
(and
(Flag234-)
(Flag234prime-)
)
:effect
(and
(Flag311-)
(Flag311prime-)
)

)
(:action Flag311Action-18
:parameters ()
:precondition
(and
(Flag225-)
(Flag225prime-)
)
:effect
(and
(Flag311-)
(Flag311prime-)
)

)
(:action Flag311Action-19
:parameters ()
:precondition
(and
(Flag274-)
(Flag274prime-)
)
:effect
(and
(Flag311-)
(Flag311prime-)
)

)
(:action Flag311Action-20
:parameters ()
:precondition
(and
(Flag306-)
(Flag306prime-)
)
:effect
(and
(Flag311-)
(Flag311prime-)
)

)
(:action Flag311Action-21
:parameters ()
:precondition
(and
(Flag309-)
(Flag309prime-)
)
:effect
(and
(Flag311-)
(Flag311prime-)
)

)
(:action Flag311Action-22
:parameters ()
:precondition
(and
(Flag190-)
(Flag190prime-)
)
:effect
(and
(Flag311-)
(Flag311prime-)
)

)
(:action Flag311Action-23
:parameters ()
:precondition
(and
(Flag263-)
(Flag263prime-)
)
:effect
(and
(Flag311-)
(Flag311prime-)
)

)
(:action Flag311Action-24
:parameters ()
:precondition
(and
(Flag239-)
(Flag239prime-)
)
:effect
(and
(Flag311-)
(Flag311prime-)
)

)
(:action Flag311Action-25
:parameters ()
:precondition
(and
(Flag310-)
(Flag310prime-)
)
:effect
(and
(Flag311-)
(Flag311prime-)
)

)
(:action Flag311Action-26
:parameters ()
:precondition
(and
(Flag302-)
(Flag302prime-)
)
:effect
(and
(Flag311-)
(Flag311prime-)
)

)
(:action Prim311Action
:parameters ()
:precondition
(and
(not (Flag201-))
(Flag201prime-)
(not (Flag229-))
(Flag229prime-)
(not (Flag293-))
(Flag293prime-)
(not (Flag296-))
(Flag296prime-)
(not (Flag286-))
(Flag286prime-)
(not (Flag279-))
(Flag279prime-)
(not (Flag246-))
(Flag246prime-)
(not (Flag269-))
(Flag269prime-)
(not (Flag219-))
(Flag219prime-)
(not (Flag283-))
(Flag283prime-)
(not (Flag187-))
(Flag187prime-)
(not (Flag271-))
(Flag271prime-)
(not (Flag209-))
(Flag209prime-)
(not (Flag308-))
(Flag308prime-)
(not (Flag252-))
(Flag252prime-)
(not (Flag291-))
(Flag291prime-)
(not (Flag300-))
(Flag300prime-)
(not (Flag234-))
(Flag234prime-)
(not (Flag225-))
(Flag225prime-)
(not (Flag274-))
(Flag274prime-)
(not (Flag306-))
(Flag306prime-)
(not (Flag309-))
(Flag309prime-)
(not (Flag190-))
(Flag190prime-)
(not (Flag263-))
(Flag263prime-)
(not (Flag239-))
(Flag239prime-)
(not (Flag310-))
(Flag310prime-)
(not (Flag302-))
(Flag302prime-)
)
:effect
(and
(Flag311prime-)
(not (Flag311-))
)

)
(:action Flag314Action-0
:parameters ()
:precondition
(and
(Flag279-)
(Flag279prime-)
)
:effect
(and
(Flag314-)
(Flag314prime-)
)

)
(:action Flag314Action-1
:parameters ()
:precondition
(and
(Flag246-)
(Flag246prime-)
)
:effect
(and
(Flag314-)
(Flag314prime-)
)

)
(:action Flag314Action-2
:parameters ()
:precondition
(and
(Flag201-)
(Flag201prime-)
)
:effect
(and
(Flag314-)
(Flag314prime-)
)

)
(:action Flag314Action-3
:parameters ()
:precondition
(and
(Flag313-)
(Flag313prime-)
)
:effect
(and
(Flag314-)
(Flag314prime-)
)

)
(:action Flag314Action-4
:parameters ()
:precondition
(and
(Flag306-)
(Flag306prime-)
)
:effect
(and
(Flag314-)
(Flag314prime-)
)

)
(:action Flag314Action-5
:parameters ()
:precondition
(and
(Flag309-)
(Flag309prime-)
)
:effect
(and
(Flag314-)
(Flag314prime-)
)

)
(:action Flag314Action-6
:parameters ()
:precondition
(and
(Flag190-)
(Flag190prime-)
)
:effect
(and
(Flag314-)
(Flag314prime-)
)

)
(:action Flag314Action-7
:parameters ()
:precondition
(and
(Flag229-)
(Flag229prime-)
)
:effect
(and
(Flag314-)
(Flag314prime-)
)

)
(:action Flag314Action-8
:parameters ()
:precondition
(and
(Flag269-)
(Flag269prime-)
)
:effect
(and
(Flag314-)
(Flag314prime-)
)

)
(:action Flag314Action-9
:parameters ()
:precondition
(and
(Flag300-)
(Flag300prime-)
)
:effect
(and
(Flag314-)
(Flag314prime-)
)

)
(:action Flag314Action-10
:parameters ()
:precondition
(and
(Flag239-)
(Flag239prime-)
)
:effect
(and
(Flag314-)
(Flag314prime-)
)

)
(:action Flag314Action-11
:parameters ()
:precondition
(and
(Flag296-)
(Flag296prime-)
)
:effect
(and
(Flag314-)
(Flag314prime-)
)

)
(:action Flag314Action-12
:parameters ()
:precondition
(and
(Flag286-)
(Flag286prime-)
)
:effect
(and
(Flag314-)
(Flag314prime-)
)

)
(:action Flag314Action-13
:parameters ()
:precondition
(and
(Flag263-)
(Flag263prime-)
)
:effect
(and
(Flag314-)
(Flag314prime-)
)

)
(:action Prim314Action
:parameters ()
:precondition
(and
(not (Flag279-))
(Flag279prime-)
(not (Flag246-))
(Flag246prime-)
(not (Flag201-))
(Flag201prime-)
(not (Flag313-))
(Flag313prime-)
(not (Flag306-))
(Flag306prime-)
(not (Flag309-))
(Flag309prime-)
(not (Flag190-))
(Flag190prime-)
(not (Flag229-))
(Flag229prime-)
(not (Flag269-))
(Flag269prime-)
(not (Flag300-))
(Flag300prime-)
(not (Flag239-))
(Flag239prime-)
(not (Flag296-))
(Flag296prime-)
(not (Flag286-))
(Flag286prime-)
(not (Flag263-))
(Flag263prime-)
)
:effect
(and
(Flag314prime-)
(not (Flag314-))
)

)
(:action Flag1Action
:parameters ()
:precondition
(and
(COLUMN2-ROBOT)
(ROW1-ROBOT)
(NOT-ERROR-)
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(Flag1-)
(Flag1prime-)
)

)
(:action Prim1Action-0
:parameters ()
:precondition
(and
(not (COLUMN2-ROBOT))
)
:effect
(and
(Flag1prime-)
(not (Flag1-))
)

)
(:action Flag2Action-0
:parameters ()
:precondition
(and
(LEFTOF3-ROBOT)
)
:effect
(and
(Flag2-)
(Flag2prime-)
)

)
(:action Flag2Action-1
:parameters ()
:precondition
(and
(RIGHTOF7-ROBOT)
)
:effect
(and
(Flag2-)
(Flag2prime-)
)

)
(:action Flag2Action-2
:parameters ()
:precondition
(and
(RIGHTOF8-ROBOT)
)
:effect
(and
(Flag2-)
(Flag2prime-)
)

)
(:action Flag2Action-3
:parameters ()
:precondition
(and
(LEFTOF2-ROBOT)
)
:effect
(and
(Flag2-)
(Flag2prime-)
)

)
(:action Flag2Action-4
:parameters ()
:precondition
(and
(LEFTOF9-ROBOT)
)
:effect
(and
(Flag2-)
(Flag2prime-)
)

)
(:action Flag2Action-6
:parameters ()
:precondition
(and
(RIGHTOF0-ROBOT)
)
:effect
(and
(Flag2-)
(Flag2prime-)
)

)
(:action Flag2Action-7
:parameters ()
:precondition
(and
(LEFTOF8-ROBOT)
)
:effect
(and
(Flag2-)
(Flag2prime-)
)

)
(:action Flag2Action-9
:parameters ()
:precondition
(and
(LEFTOF14-ROBOT)
)
:effect
(and
(Flag2-)
(Flag2prime-)
)

)
(:action Flag2Action-10
:parameters ()
:precondition
(and
(LEFTOF5-ROBOT)
)
:effect
(and
(Flag2-)
(Flag2prime-)
)

)
(:action Flag2Action-11
:parameters ()
:precondition
(and
(RIGHTOF12-ROBOT)
)
:effect
(and
(Flag2-)
(Flag2prime-)
)

)
(:action Flag2Action-12
:parameters ()
:precondition
(and
(RIGHTOF1-ROBOT)
)
:effect
(and
(Flag2-)
(Flag2prime-)
)

)
(:action Flag2Action-13
:parameters ()
:precondition
(and
(LEFTOF12-ROBOT)
)
:effect
(and
(Flag2-)
(Flag2prime-)
)

)
(:action Flag2Action-14
:parameters ()
:precondition
(and
(RIGHTOF13-ROBOT)
)
:effect
(and
(Flag2-)
(Flag2prime-)
)

)
(:action Flag2Action-15
:parameters ()
:precondition
(and
(RIGHTOF6-ROBOT)
)
:effect
(and
(Flag2-)
(Flag2prime-)
)

)
(:action Flag2Action-16
:parameters ()
:precondition
(and
(RIGHTOF9-ROBOT)
)
:effect
(and
(Flag2-)
(Flag2prime-)
)

)
(:action Flag2Action-17
:parameters ()
:precondition
(and
(LEFTOF6-ROBOT)
)
:effect
(and
(Flag2-)
(Flag2prime-)
)

)
(:action Flag2Action-18
:parameters ()
:precondition
(and
(RIGHTOF10-ROBOT)
)
:effect
(and
(Flag2-)
(Flag2prime-)
)

)
(:action Flag2Action-19
:parameters ()
:precondition
(and
(RIGHTOF11-ROBOT)
)
:effect
(and
(Flag2-)
(Flag2prime-)
)

)
(:action Flag2Action-20
:parameters ()
:precondition
(and
(LEFTOF11-ROBOT)
)
:effect
(and
(Flag2-)
(Flag2prime-)
)

)
(:action Flag2Action-21
:parameters ()
:precondition
(and
(RIGHTOF5-ROBOT)
)
:effect
(and
(Flag2-)
(Flag2prime-)
)

)
(:action Flag2Action-22
:parameters ()
:precondition
(and
(LEFTOF10-ROBOT)
)
:effect
(and
(Flag2-)
(Flag2prime-)
)

)
(:action Flag2Action-23
:parameters ()
:precondition
(and
(LEFTOF4-ROBOT)
)
:effect
(and
(Flag2-)
(Flag2prime-)
)

)
(:action Flag2Action-24
:parameters ()
:precondition
(and
(RIGHTOF4-ROBOT)
)
:effect
(and
(Flag2-)
(Flag2prime-)
)

)
(:action Flag2Action-25
:parameters ()
:precondition
(and
(LEFTOF1-ROBOT)
)
:effect
(and
(Flag2-)
(Flag2prime-)
)

)
(:action Flag2Action-26
:parameters ()
:precondition
(and
(LEFTOF7-ROBOT)
)
:effect
(and
(Flag2-)
(Flag2prime-)
)

)
(:action Flag2Action-27
:parameters ()
:precondition
(and
(RIGHTOF3-ROBOT)
)
:effect
(and
(Flag2-)
(Flag2prime-)
)

)
(:action Prim2Action
:parameters ()
:precondition
(and
(not (LEFTOF3-ROBOT))
(not (RIGHTOF7-ROBOT))
(not (RIGHTOF8-ROBOT))
(not (LEFTOF2-ROBOT))
(not (LEFTOF9-ROBOT))
(not (RIGHTOF2-ROBOT))
(not (RIGHTOF0-ROBOT))
(not (LEFTOF8-ROBOT))
(not (LEFTOF13-ROBOT))
(not (LEFTOF14-ROBOT))
(not (LEFTOF5-ROBOT))
(not (RIGHTOF12-ROBOT))
(not (RIGHTOF1-ROBOT))
(not (LEFTOF12-ROBOT))
(not (RIGHTOF13-ROBOT))
(not (RIGHTOF6-ROBOT))
(not (RIGHTOF9-ROBOT))
(not (LEFTOF6-ROBOT))
(not (RIGHTOF10-ROBOT))
(not (RIGHTOF11-ROBOT))
(not (LEFTOF11-ROBOT))
(not (RIGHTOF5-ROBOT))
(not (LEFTOF10-ROBOT))
(not (LEFTOF4-ROBOT))
(not (RIGHTOF4-ROBOT))
(not (LEFTOF1-ROBOT))
(not (LEFTOF7-ROBOT))
(not (RIGHTOF3-ROBOT))
)
:effect
(and
(Flag2prime-)
(not (Flag2-))
)

)
(:action Flag42Action-0
:parameters ()
:precondition
(and
(Flag29-)
(Flag29prime-)
)
:effect
(and
(Flag42-)
(Flag42prime-)
)

)
(:action Flag42Action-1
:parameters ()
:precondition
(and
(Flag30-)
(Flag30prime-)
)
:effect
(and
(Flag42-)
(Flag42prime-)
)

)
(:action Flag42Action-2
:parameters ()
:precondition
(and
(Flag31-)
(Flag31prime-)
)
:effect
(and
(Flag42-)
(Flag42prime-)
)

)
(:action Flag42Action-3
:parameters ()
:precondition
(and
(Flag32-)
(Flag32prime-)
)
:effect
(and
(Flag42-)
(Flag42prime-)
)

)
(:action Flag42Action-4
:parameters ()
:precondition
(and
(Flag33-)
(Flag33prime-)
)
:effect
(and
(Flag42-)
(Flag42prime-)
)

)
(:action Flag42Action-5
:parameters ()
:precondition
(and
(Flag34-)
(Flag34prime-)
)
:effect
(and
(Flag42-)
(Flag42prime-)
)

)
(:action Flag42Action-6
:parameters ()
:precondition
(and
(Flag35-)
(Flag35prime-)
)
:effect
(and
(Flag42-)
(Flag42prime-)
)

)
(:action Flag42Action-7
:parameters ()
:precondition
(and
(Flag36-)
(Flag36prime-)
)
:effect
(and
(Flag42-)
(Flag42prime-)
)

)
(:action Flag42Action-8
:parameters ()
:precondition
(and
(Flag37-)
(Flag37prime-)
)
:effect
(and
(Flag42-)
(Flag42prime-)
)

)
(:action Flag42Action-9
:parameters ()
:precondition
(and
(Flag38-)
(Flag38prime-)
)
:effect
(and
(Flag42-)
(Flag42prime-)
)

)
(:action Flag42Action-11
:parameters ()
:precondition
(and
(Flag39-)
(Flag39prime-)
)
:effect
(and
(Flag42-)
(Flag42prime-)
)

)
(:action Flag42Action-12
:parameters ()
:precondition
(and
(Flag40-)
(Flag40prime-)
)
:effect
(and
(Flag42-)
(Flag42prime-)
)

)
(:action Flag42Action-13
:parameters ()
:precondition
(and
(Flag41-)
(Flag41prime-)
)
:effect
(and
(Flag42-)
(Flag42prime-)
)

)
(:action Prim42Action
:parameters ()
:precondition
(and
(not (Flag29-))
(Flag29prime-)
(not (Flag30-))
(Flag30prime-)
(not (Flag31-))
(Flag31prime-)
(not (Flag32-))
(Flag32prime-)
(not (Flag33-))
(Flag33prime-)
(not (Flag34-))
(Flag34prime-)
(not (Flag35-))
(Flag35prime-)
(not (Flag36-))
(Flag36prime-)
(not (Flag37-))
(Flag37prime-)
(not (Flag38-))
(Flag38prime-)
(not (BELOWOF1-ROBOT))
(not (Flag39-))
(Flag39prime-)
(not (Flag40-))
(Flag40prime-)
(not (Flag41-))
(Flag41prime-)
)
:effect
(and
(Flag42prime-)
(not (Flag42-))
)

)
(:action Flag56Action-0
:parameters ()
:precondition
(and
(Flag43-)
(Flag43prime-)
)
:effect
(and
(Flag56-)
(Flag56prime-)
)

)
(:action Flag56Action-1
:parameters ()
:precondition
(and
(Flag35-)
(Flag35prime-)
)
:effect
(and
(Flag56-)
(Flag56prime-)
)

)
(:action Flag56Action-2
:parameters ()
:precondition
(and
(Flag44-)
(Flag44prime-)
)
:effect
(and
(Flag56-)
(Flag56prime-)
)

)
(:action Flag56Action-3
:parameters ()
:precondition
(and
(Flag45-)
(Flag45prime-)
)
:effect
(and
(Flag56-)
(Flag56prime-)
)

)
(:action Flag56Action-4
:parameters ()
:precondition
(and
(Flag46-)
(Flag46prime-)
)
:effect
(and
(Flag56-)
(Flag56prime-)
)

)
(:action Flag56Action-5
:parameters ()
:precondition
(and
(Flag34-)
(Flag34prime-)
)
:effect
(and
(Flag56-)
(Flag56prime-)
)

)
(:action Flag56Action-6
:parameters ()
:precondition
(and
(Flag31-)
(Flag31prime-)
)
:effect
(and
(Flag56-)
(Flag56prime-)
)

)
(:action Flag56Action-7
:parameters ()
:precondition
(and
(Flag37-)
(Flag37prime-)
)
:effect
(and
(Flag56-)
(Flag56prime-)
)

)
(:action Flag56Action-8
:parameters ()
:precondition
(and
(Flag47-)
(Flag47prime-)
)
:effect
(and
(Flag56-)
(Flag56prime-)
)

)
(:action Flag56Action-9
:parameters ()
:precondition
(and
(Flag48-)
(Flag48prime-)
)
:effect
(and
(Flag56-)
(Flag56prime-)
)

)
(:action Flag56Action-10
:parameters ()
:precondition
(and
(Flag30-)
(Flag30prime-)
)
:effect
(and
(Flag56-)
(Flag56prime-)
)

)
(:action Flag56Action-11
:parameters ()
:precondition
(and
(Flag49-)
(Flag49prime-)
)
:effect
(and
(Flag56-)
(Flag56prime-)
)

)
(:action Flag56Action-12
:parameters ()
:precondition
(and
(Flag33-)
(Flag33prime-)
)
:effect
(and
(Flag56-)
(Flag56prime-)
)

)
(:action Flag56Action-13
:parameters ()
:precondition
(and
(Flag36-)
(Flag36prime-)
)
:effect
(and
(Flag56-)
(Flag56prime-)
)

)
(:action Flag56Action-14
:parameters ()
:precondition
(and
(Flag38-)
(Flag38prime-)
)
:effect
(and
(Flag56-)
(Flag56prime-)
)

)
(:action Flag56Action-15
:parameters ()
:precondition
(and
(Flag50-)
(Flag50prime-)
)
:effect
(and
(Flag56-)
(Flag56prime-)
)

)
(:action Flag56Action-16
:parameters ()
:precondition
(and
(Flag40-)
(Flag40prime-)
)
:effect
(and
(Flag56-)
(Flag56prime-)
)

)
(:action Flag56Action-17
:parameters ()
:precondition
(and
(Flag41-)
(Flag41prime-)
)
:effect
(and
(Flag56-)
(Flag56prime-)
)

)
(:action Flag56Action-18
:parameters ()
:precondition
(and
(Flag51-)
(Flag51prime-)
)
:effect
(and
(Flag56-)
(Flag56prime-)
)

)
(:action Flag56Action-19
:parameters ()
:precondition
(and
(Flag52-)
(Flag52prime-)
)
:effect
(and
(Flag56-)
(Flag56prime-)
)

)
(:action Flag56Action-20
:parameters ()
:precondition
(and
(Flag29-)
(Flag29prime-)
)
:effect
(and
(Flag56-)
(Flag56prime-)
)

)
(:action Flag56Action-21
:parameters ()
:precondition
(and
(Flag53-)
(Flag53prime-)
)
:effect
(and
(Flag56-)
(Flag56prime-)
)

)
(:action Flag56Action-22
:parameters ()
:precondition
(and
(Flag32-)
(Flag32prime-)
)
:effect
(and
(Flag56-)
(Flag56prime-)
)

)
(:action Flag56Action-23
:parameters ()
:precondition
(and
(Flag54-)
(Flag54prime-)
)
:effect
(and
(Flag56-)
(Flag56prime-)
)

)
(:action Flag56Action-24
:parameters ()
:precondition
(and
(Flag39-)
(Flag39prime-)
)
:effect
(and
(Flag56-)
(Flag56prime-)
)

)
(:action Flag56Action-25
:parameters ()
:precondition
(and
(Flag55-)
(Flag55prime-)
)
:effect
(and
(Flag56-)
(Flag56prime-)
)

)
(:action Prim56Action
:parameters ()
:precondition
(and
(not (Flag43-))
(Flag43prime-)
(not (Flag35-))
(Flag35prime-)
(not (Flag44-))
(Flag44prime-)
(not (Flag45-))
(Flag45prime-)
(not (Flag46-))
(Flag46prime-)
(not (Flag34-))
(Flag34prime-)
(not (Flag31-))
(Flag31prime-)
(not (Flag37-))
(Flag37prime-)
(not (Flag47-))
(Flag47prime-)
(not (Flag48-))
(Flag48prime-)
(not (Flag30-))
(Flag30prime-)
(not (Flag49-))
(Flag49prime-)
(not (Flag33-))
(Flag33prime-)
(not (Flag36-))
(Flag36prime-)
(not (Flag38-))
(Flag38prime-)
(not (Flag50-))
(Flag50prime-)
(not (Flag40-))
(Flag40prime-)
(not (Flag41-))
(Flag41prime-)
(not (Flag51-))
(Flag51prime-)
(not (Flag52-))
(Flag52prime-)
(not (Flag29-))
(Flag29prime-)
(not (Flag53-))
(Flag53prime-)
(not (Flag32-))
(Flag32prime-)
(not (Flag54-))
(Flag54prime-)
(not (Flag39-))
(Flag39prime-)
(not (Flag55-))
(Flag55prime-)
)
:effect
(and
(Flag56prime-)
(not (Flag56-))
)

)
(:action Flag70Action-0
:parameters ()
:precondition
(and
(Flag57-)
(Flag57prime-)
)
:effect
(and
(Flag70-)
(Flag70prime-)
)

)
(:action Flag70Action-1
:parameters ()
:precondition
(and
(Flag58-)
(Flag58prime-)
)
:effect
(and
(Flag70-)
(Flag70prime-)
)

)
(:action Flag70Action-2
:parameters ()
:precondition
(and
(Flag30-)
(Flag30prime-)
)
:effect
(and
(Flag70-)
(Flag70prime-)
)

)
(:action Flag70Action-3
:parameters ()
:precondition
(and
(Flag59-)
(Flag59prime-)
)
:effect
(and
(Flag70-)
(Flag70prime-)
)

)
(:action Flag70Action-4
:parameters ()
:precondition
(and
(Flag35-)
(Flag35prime-)
)
:effect
(and
(Flag70-)
(Flag70prime-)
)

)
(:action Flag70Action-5
:parameters ()
:precondition
(and
(Flag50-)
(Flag50prime-)
)
:effect
(and
(Flag70-)
(Flag70prime-)
)

)
(:action Flag70Action-6
:parameters ()
:precondition
(and
(Flag45-)
(Flag45prime-)
)
:effect
(and
(Flag70-)
(Flag70prime-)
)

)
(:action Flag70Action-7
:parameters ()
:precondition
(and
(Flag46-)
(Flag46prime-)
)
:effect
(and
(Flag70-)
(Flag70prime-)
)

)
(:action Flag70Action-8
:parameters ()
:precondition
(and
(Flag34-)
(Flag34prime-)
)
:effect
(and
(Flag70-)
(Flag70prime-)
)

)
(:action Flag70Action-9
:parameters ()
:precondition
(and
(Flag37-)
(Flag37prime-)
)
:effect
(and
(Flag70-)
(Flag70prime-)
)

)
(:action Flag70Action-10
:parameters ()
:precondition
(and
(Flag60-)
(Flag60prime-)
)
:effect
(and
(Flag70-)
(Flag70prime-)
)

)
(:action Flag70Action-11
:parameters ()
:precondition
(and
(Flag47-)
(Flag47prime-)
)
:effect
(and
(Flag70-)
(Flag70prime-)
)

)
(:action Flag70Action-12
:parameters ()
:precondition
(and
(Flag61-)
(Flag61prime-)
)
:effect
(and
(Flag70-)
(Flag70prime-)
)

)
(:action Flag70Action-13
:parameters ()
:precondition
(and
(Flag48-)
(Flag48prime-)
)
:effect
(and
(Flag70-)
(Flag70prime-)
)

)
(:action Flag70Action-14
:parameters ()
:precondition
(and
(Flag62-)
(Flag62prime-)
)
:effect
(and
(Flag70-)
(Flag70prime-)
)

)
(:action Flag70Action-15
:parameters ()
:precondition
(and
(Flag49-)
(Flag49prime-)
)
:effect
(and
(Flag70-)
(Flag70prime-)
)

)
(:action Flag70Action-16
:parameters ()
:precondition
(and
(Flag33-)
(Flag33prime-)
)
:effect
(and
(Flag70-)
(Flag70prime-)
)

)
(:action Flag70Action-17
:parameters ()
:precondition
(and
(Flag63-)
(Flag63prime-)
)
:effect
(and
(Flag70-)
(Flag70prime-)
)

)
(:action Flag70Action-18
:parameters ()
:precondition
(and
(Flag36-)
(Flag36prime-)
)
:effect
(and
(Flag70-)
(Flag70prime-)
)

)
(:action Flag70Action-19
:parameters ()
:precondition
(and
(Flag64-)
(Flag64prime-)
)
:effect
(and
(Flag70-)
(Flag70prime-)
)

)
(:action Flag70Action-20
:parameters ()
:precondition
(and
(Flag38-)
(Flag38prime-)
)
:effect
(and
(Flag70-)
(Flag70prime-)
)

)
(:action Flag70Action-21
:parameters ()
:precondition
(and
(Flag65-)
(Flag65prime-)
)
:effect
(and
(Flag70-)
(Flag70prime-)
)

)
(:action Flag70Action-22
:parameters ()
:precondition
(and
(Flag66-)
(Flag66prime-)
)
:effect
(and
(Flag70-)
(Flag70prime-)
)

)
(:action Flag70Action-23
:parameters ()
:precondition
(and
(Flag40-)
(Flag40prime-)
)
:effect
(and
(Flag70-)
(Flag70prime-)
)

)
(:action Flag70Action-24
:parameters ()
:precondition
(and
(Flag41-)
(Flag41prime-)
)
:effect
(and
(Flag70-)
(Flag70prime-)
)

)
(:action Flag70Action-25
:parameters ()
:precondition
(and
(Flag51-)
(Flag51prime-)
)
:effect
(and
(Flag70-)
(Flag70prime-)
)

)
(:action Flag70Action-26
:parameters ()
:precondition
(and
(Flag52-)
(Flag52prime-)
)
:effect
(and
(Flag70-)
(Flag70prime-)
)

)
(:action Flag70Action-27
:parameters ()
:precondition
(and
(Flag29-)
(Flag29prime-)
)
:effect
(and
(Flag70-)
(Flag70prime-)
)

)
(:action Flag70Action-28
:parameters ()
:precondition
(and
(Flag53-)
(Flag53prime-)
)
:effect
(and
(Flag70-)
(Flag70prime-)
)

)
(:action Flag70Action-29
:parameters ()
:precondition
(and
(Flag67-)
(Flag67prime-)
)
:effect
(and
(Flag70-)
(Flag70prime-)
)

)
(:action Flag70Action-30
:parameters ()
:precondition
(and
(Flag32-)
(Flag32prime-)
)
:effect
(and
(Flag70-)
(Flag70prime-)
)

)
(:action Flag70Action-31
:parameters ()
:precondition
(and
(Flag68-)
(Flag68prime-)
)
:effect
(and
(Flag70-)
(Flag70prime-)
)

)
(:action Flag70Action-32
:parameters ()
:precondition
(and
(Flag69-)
(Flag69prime-)
)
:effect
(and
(Flag70-)
(Flag70prime-)
)

)
(:action Flag70Action-33
:parameters ()
:precondition
(and
(Flag54-)
(Flag54prime-)
)
:effect
(and
(Flag70-)
(Flag70prime-)
)

)
(:action Flag70Action-34
:parameters ()
:precondition
(and
(Flag39-)
(Flag39prime-)
)
:effect
(and
(Flag70-)
(Flag70prime-)
)

)
(:action Flag70Action-35
:parameters ()
:precondition
(and
(Flag55-)
(Flag55prime-)
)
:effect
(and
(Flag70-)
(Flag70prime-)
)

)
(:action Prim70Action
:parameters ()
:precondition
(and
(not (Flag57-))
(Flag57prime-)
(not (Flag58-))
(Flag58prime-)
(not (Flag30-))
(Flag30prime-)
(not (Flag59-))
(Flag59prime-)
(not (Flag35-))
(Flag35prime-)
(not (Flag50-))
(Flag50prime-)
(not (Flag45-))
(Flag45prime-)
(not (Flag46-))
(Flag46prime-)
(not (Flag34-))
(Flag34prime-)
(not (Flag37-))
(Flag37prime-)
(not (Flag60-))
(Flag60prime-)
(not (Flag47-))
(Flag47prime-)
(not (Flag61-))
(Flag61prime-)
(not (Flag48-))
(Flag48prime-)
(not (Flag62-))
(Flag62prime-)
(not (Flag49-))
(Flag49prime-)
(not (Flag33-))
(Flag33prime-)
(not (Flag63-))
(Flag63prime-)
(not (Flag36-))
(Flag36prime-)
(not (Flag64-))
(Flag64prime-)
(not (Flag38-))
(Flag38prime-)
(not (Flag65-))
(Flag65prime-)
(not (Flag66-))
(Flag66prime-)
(not (Flag40-))
(Flag40prime-)
(not (Flag41-))
(Flag41prime-)
(not (Flag51-))
(Flag51prime-)
(not (Flag52-))
(Flag52prime-)
(not (Flag29-))
(Flag29prime-)
(not (Flag53-))
(Flag53prime-)
(not (Flag67-))
(Flag67prime-)
(not (Flag32-))
(Flag32prime-)
(not (Flag68-))
(Flag68prime-)
(not (Flag69-))
(Flag69prime-)
(not (Flag54-))
(Flag54prime-)
(not (Flag39-))
(Flag39prime-)
(not (Flag55-))
(Flag55prime-)
)
:effect
(and
(Flag70prime-)
(not (Flag70-))
)

)
(:action Flag84Action-0
:parameters ()
:precondition
(and
(Flag57-)
(Flag57prime-)
)
:effect
(and
(Flag84-)
(Flag84prime-)
)

)
(:action Flag84Action-1
:parameters ()
:precondition
(and
(Flag71-)
(Flag71prime-)
)
:effect
(and
(Flag84-)
(Flag84prime-)
)

)
(:action Flag84Action-2
:parameters ()
:precondition
(and
(Flag72-)
(Flag72prime-)
)
:effect
(and
(Flag84-)
(Flag84prime-)
)

)
(:action Flag84Action-3
:parameters ()
:precondition
(and
(Flag73-)
(Flag73prime-)
)
:effect
(and
(Flag84-)
(Flag84prime-)
)

)
(:action Flag84Action-4
:parameters ()
:precondition
(and
(Flag58-)
(Flag58prime-)
)
:effect
(and
(Flag84-)
(Flag84prime-)
)

)
(:action Flag84Action-5
:parameters ()
:precondition
(and
(Flag59-)
(Flag59prime-)
)
:effect
(and
(Flag84-)
(Flag84prime-)
)

)
(:action Flag84Action-6
:parameters ()
:precondition
(and
(Flag35-)
(Flag35prime-)
)
:effect
(and
(Flag84-)
(Flag84prime-)
)

)
(:action Flag84Action-7
:parameters ()
:precondition
(and
(Flag50-)
(Flag50prime-)
)
:effect
(and
(Flag84-)
(Flag84prime-)
)

)
(:action Flag84Action-8
:parameters ()
:precondition
(and
(Flag45-)
(Flag45prime-)
)
:effect
(and
(Flag84-)
(Flag84prime-)
)

)
(:action Flag84Action-9
:parameters ()
:precondition
(and
(Flag60-)
(Flag60prime-)
)
:effect
(and
(Flag84-)
(Flag84prime-)
)

)
(:action Flag84Action-10
:parameters ()
:precondition
(and
(Flag46-)
(Flag46prime-)
)
:effect
(and
(Flag84-)
(Flag84prime-)
)

)
(:action Flag84Action-11
:parameters ()
:precondition
(and
(Flag34-)
(Flag34prime-)
)
:effect
(and
(Flag84-)
(Flag84prime-)
)

)
(:action Flag84Action-12
:parameters ()
:precondition
(and
(Flag74-)
(Flag74prime-)
)
:effect
(and
(Flag84-)
(Flag84prime-)
)

)
(:action Flag84Action-13
:parameters ()
:precondition
(and
(Flag37-)
(Flag37prime-)
)
:effect
(and
(Flag84-)
(Flag84prime-)
)

)
(:action Flag84Action-14
:parameters ()
:precondition
(and
(Flag75-)
(Flag75prime-)
)
:effect
(and
(Flag84-)
(Flag84prime-)
)

)
(:action Flag84Action-15
:parameters ()
:precondition
(and
(Flag76-)
(Flag76prime-)
)
:effect
(and
(Flag84-)
(Flag84prime-)
)

)
(:action Flag84Action-16
:parameters ()
:precondition
(and
(Flag77-)
(Flag77prime-)
)
:effect
(and
(Flag84-)
(Flag84prime-)
)

)
(:action Flag84Action-17
:parameters ()
:precondition
(and
(Flag47-)
(Flag47prime-)
)
:effect
(and
(Flag84-)
(Flag84prime-)
)

)
(:action Flag84Action-18
:parameters ()
:precondition
(and
(Flag78-)
(Flag78prime-)
)
:effect
(and
(Flag84-)
(Flag84prime-)
)

)
(:action Flag84Action-19
:parameters ()
:precondition
(and
(Flag79-)
(Flag79prime-)
)
:effect
(and
(Flag84-)
(Flag84prime-)
)

)
(:action Flag84Action-20
:parameters ()
:precondition
(and
(Flag80-)
(Flag80prime-)
)
:effect
(and
(Flag84-)
(Flag84prime-)
)

)
(:action Flag84Action-21
:parameters ()
:precondition
(and
(Flag48-)
(Flag48prime-)
)
:effect
(and
(Flag84-)
(Flag84prime-)
)

)
(:action Flag84Action-22
:parameters ()
:precondition
(and
(Flag49-)
(Flag49prime-)
)
:effect
(and
(Flag84-)
(Flag84prime-)
)

)
(:action Flag84Action-23
:parameters ()
:precondition
(and
(Flag33-)
(Flag33prime-)
)
:effect
(and
(Flag84-)
(Flag84prime-)
)

)
(:action Flag84Action-24
:parameters ()
:precondition
(and
(Flag63-)
(Flag63prime-)
)
:effect
(and
(Flag84-)
(Flag84prime-)
)

)
(:action Flag84Action-25
:parameters ()
:precondition
(and
(Flag36-)
(Flag36prime-)
)
:effect
(and
(Flag84-)
(Flag84prime-)
)

)
(:action Flag84Action-26
:parameters ()
:precondition
(and
(Flag64-)
(Flag64prime-)
)
:effect
(and
(Flag84-)
(Flag84prime-)
)

)
(:action Flag84Action-27
:parameters ()
:precondition
(and
(Flag53-)
(Flag53prime-)
)
:effect
(and
(Flag84-)
(Flag84prime-)
)

)
(:action Flag84Action-28
:parameters ()
:precondition
(and
(Flag38-)
(Flag38prime-)
)
:effect
(and
(Flag84-)
(Flag84prime-)
)

)
(:action Flag84Action-29
:parameters ()
:precondition
(and
(Flag65-)
(Flag65prime-)
)
:effect
(and
(Flag84-)
(Flag84prime-)
)

)
(:action Flag84Action-30
:parameters ()
:precondition
(and
(Flag81-)
(Flag81prime-)
)
:effect
(and
(Flag84-)
(Flag84prime-)
)

)
(:action Flag84Action-31
:parameters ()
:precondition
(and
(Flag40-)
(Flag40prime-)
)
:effect
(and
(Flag84-)
(Flag84prime-)
)

)
(:action Flag84Action-32
:parameters ()
:precondition
(and
(Flag41-)
(Flag41prime-)
)
:effect
(and
(Flag84-)
(Flag84prime-)
)

)
(:action Flag84Action-33
:parameters ()
:precondition
(and
(Flag51-)
(Flag51prime-)
)
:effect
(and
(Flag84-)
(Flag84prime-)
)

)
(:action Flag84Action-34
:parameters ()
:precondition
(and
(Flag52-)
(Flag52prime-)
)
:effect
(and
(Flag84-)
(Flag84prime-)
)

)
(:action Flag84Action-35
:parameters ()
:precondition
(and
(Flag29-)
(Flag29prime-)
)
:effect
(and
(Flag84-)
(Flag84prime-)
)

)
(:action Flag84Action-36
:parameters ()
:precondition
(and
(Flag82-)
(Flag82prime-)
)
:effect
(and
(Flag84-)
(Flag84prime-)
)

)
(:action Flag84Action-37
:parameters ()
:precondition
(and
(Flag67-)
(Flag67prime-)
)
:effect
(and
(Flag84-)
(Flag84prime-)
)

)
(:action Flag84Action-38
:parameters ()
:precondition
(and
(Flag83-)
(Flag83prime-)
)
:effect
(and
(Flag84-)
(Flag84prime-)
)

)
(:action Flag84Action-39
:parameters ()
:precondition
(and
(Flag68-)
(Flag68prime-)
)
:effect
(and
(Flag84-)
(Flag84prime-)
)

)
(:action Flag84Action-40
:parameters ()
:precondition
(and
(Flag32-)
(Flag32prime-)
)
:effect
(and
(Flag84-)
(Flag84prime-)
)

)
(:action Flag84Action-41
:parameters ()
:precondition
(and
(Flag69-)
(Flag69prime-)
)
:effect
(and
(Flag84-)
(Flag84prime-)
)

)
(:action Flag84Action-42
:parameters ()
:precondition
(and
(Flag54-)
(Flag54prime-)
)
:effect
(and
(Flag84-)
(Flag84prime-)
)

)
(:action Flag84Action-43
:parameters ()
:precondition
(and
(Flag39-)
(Flag39prime-)
)
:effect
(and
(Flag84-)
(Flag84prime-)
)

)
(:action Flag84Action-44
:parameters ()
:precondition
(and
(Flag55-)
(Flag55prime-)
)
:effect
(and
(Flag84-)
(Flag84prime-)
)

)
(:action Prim84Action
:parameters ()
:precondition
(and
(not (Flag57-))
(Flag57prime-)
(not (Flag71-))
(Flag71prime-)
(not (Flag72-))
(Flag72prime-)
(not (Flag73-))
(Flag73prime-)
(not (Flag58-))
(Flag58prime-)
(not (Flag59-))
(Flag59prime-)
(not (Flag35-))
(Flag35prime-)
(not (Flag50-))
(Flag50prime-)
(not (Flag45-))
(Flag45prime-)
(not (Flag60-))
(Flag60prime-)
(not (Flag46-))
(Flag46prime-)
(not (Flag34-))
(Flag34prime-)
(not (Flag74-))
(Flag74prime-)
(not (Flag37-))
(Flag37prime-)
(not (Flag75-))
(Flag75prime-)
(not (Flag76-))
(Flag76prime-)
(not (Flag77-))
(Flag77prime-)
(not (Flag47-))
(Flag47prime-)
(not (Flag78-))
(Flag78prime-)
(not (Flag79-))
(Flag79prime-)
(not (Flag80-))
(Flag80prime-)
(not (Flag48-))
(Flag48prime-)
(not (Flag49-))
(Flag49prime-)
(not (Flag33-))
(Flag33prime-)
(not (Flag63-))
(Flag63prime-)
(not (Flag36-))
(Flag36prime-)
(not (Flag64-))
(Flag64prime-)
(not (Flag53-))
(Flag53prime-)
(not (Flag38-))
(Flag38prime-)
(not (Flag65-))
(Flag65prime-)
(not (Flag81-))
(Flag81prime-)
(not (Flag40-))
(Flag40prime-)
(not (Flag41-))
(Flag41prime-)
(not (Flag51-))
(Flag51prime-)
(not (Flag52-))
(Flag52prime-)
(not (Flag29-))
(Flag29prime-)
(not (Flag82-))
(Flag82prime-)
(not (Flag67-))
(Flag67prime-)
(not (Flag83-))
(Flag83prime-)
(not (Flag68-))
(Flag68prime-)
(not (Flag32-))
(Flag32prime-)
(not (Flag69-))
(Flag69prime-)
(not (Flag54-))
(Flag54prime-)
(not (Flag39-))
(Flag39prime-)
(not (Flag55-))
(Flag55prime-)
)
:effect
(and
(Flag84prime-)
(not (Flag84-))
)

)
(:action Flag95Action-0
:parameters ()
:precondition
(and
(Flag85-)
(Flag85prime-)
)
:effect
(and
(Flag95-)
(Flag95prime-)
)

)
(:action Flag95Action-1
:parameters ()
:precondition
(and
(Flag57-)
(Flag57prime-)
)
:effect
(and
(Flag95-)
(Flag95prime-)
)

)
(:action Flag95Action-2
:parameters ()
:precondition
(and
(Flag71-)
(Flag71prime-)
)
:effect
(and
(Flag95-)
(Flag95prime-)
)

)
(:action Flag95Action-3
:parameters ()
:precondition
(and
(Flag77-)
(Flag77prime-)
)
:effect
(and
(Flag95-)
(Flag95prime-)
)

)
(:action Flag95Action-4
:parameters ()
:precondition
(and
(Flag58-)
(Flag58prime-)
)
:effect
(and
(Flag95-)
(Flag95prime-)
)

)
(:action Flag95Action-5
:parameters ()
:precondition
(and
(Flag86-)
(Flag86prime-)
)
:effect
(and
(Flag95-)
(Flag95prime-)
)

)
(:action Flag95Action-6
:parameters ()
:precondition
(and
(Flag59-)
(Flag59prime-)
)
:effect
(and
(Flag95-)
(Flag95prime-)
)

)
(:action Flag95Action-7
:parameters ()
:precondition
(and
(Flag35-)
(Flag35prime-)
)
:effect
(and
(Flag95-)
(Flag95prime-)
)

)
(:action Flag95Action-8
:parameters ()
:precondition
(and
(Flag45-)
(Flag45prime-)
)
:effect
(and
(Flag95-)
(Flag95prime-)
)

)
(:action Flag95Action-9
:parameters ()
:precondition
(and
(Flag60-)
(Flag60prime-)
)
:effect
(and
(Flag95-)
(Flag95prime-)
)

)
(:action Flag95Action-10
:parameters ()
:precondition
(and
(Flag75-)
(Flag75prime-)
)
:effect
(and
(Flag95-)
(Flag95prime-)
)

)
(:action Flag95Action-11
:parameters ()
:precondition
(and
(Flag46-)
(Flag46prime-)
)
:effect
(and
(Flag95-)
(Flag95prime-)
)

)
(:action Flag95Action-12
:parameters ()
:precondition
(and
(Flag34-)
(Flag34prime-)
)
:effect
(and
(Flag95-)
(Flag95prime-)
)

)
(:action Flag95Action-13
:parameters ()
:precondition
(and
(Flag87-)
(Flag87prime-)
)
:effect
(and
(Flag95-)
(Flag95prime-)
)

)
(:action Flag95Action-14
:parameters ()
:precondition
(and
(Flag74-)
(Flag74prime-)
)
:effect
(and
(Flag95-)
(Flag95prime-)
)

)
(:action Flag95Action-15
:parameters ()
:precondition
(and
(Flag37-)
(Flag37prime-)
)
:effect
(and
(Flag95-)
(Flag95prime-)
)

)
(:action Flag95Action-16
:parameters ()
:precondition
(and
(Flag76-)
(Flag76prime-)
)
:effect
(and
(Flag95-)
(Flag95prime-)
)

)
(:action Flag95Action-17
:parameters ()
:precondition
(and
(Flag88-)
(Flag88prime-)
)
:effect
(and
(Flag95-)
(Flag95prime-)
)

)
(:action Flag95Action-18
:parameters ()
:precondition
(and
(Flag89-)
(Flag89prime-)
)
:effect
(and
(Flag95-)
(Flag95prime-)
)

)
(:action Flag95Action-19
:parameters ()
:precondition
(and
(Flag90-)
(Flag90prime-)
)
:effect
(and
(Flag95-)
(Flag95prime-)
)

)
(:action Flag95Action-20
:parameters ()
:precondition
(and
(Flag47-)
(Flag47prime-)
)
:effect
(and
(Flag95-)
(Flag95prime-)
)

)
(:action Flag95Action-21
:parameters ()
:precondition
(and
(Flag78-)
(Flag78prime-)
)
:effect
(and
(Flag95-)
(Flag95prime-)
)

)
(:action Flag95Action-22
:parameters ()
:precondition
(and
(Flag79-)
(Flag79prime-)
)
:effect
(and
(Flag95-)
(Flag95prime-)
)

)
(:action Flag95Action-23
:parameters ()
:precondition
(and
(Flag48-)
(Flag48prime-)
)
:effect
(and
(Flag95-)
(Flag95prime-)
)

)
(:action Flag95Action-24
:parameters ()
:precondition
(and
(Flag49-)
(Flag49prime-)
)
:effect
(and
(Flag95-)
(Flag95prime-)
)

)
(:action Flag95Action-25
:parameters ()
:precondition
(and
(Flag33-)
(Flag33prime-)
)
:effect
(and
(Flag95-)
(Flag95prime-)
)

)
(:action Flag95Action-26
:parameters ()
:precondition
(and
(Flag63-)
(Flag63prime-)
)
:effect
(and
(Flag95-)
(Flag95prime-)
)

)
(:action Flag95Action-27
:parameters ()
:precondition
(and
(Flag36-)
(Flag36prime-)
)
:effect
(and
(Flag95-)
(Flag95prime-)
)

)
(:action Flag95Action-28
:parameters ()
:precondition
(and
(Flag91-)
(Flag91prime-)
)
:effect
(and
(Flag95-)
(Flag95prime-)
)

)
(:action Flag95Action-29
:parameters ()
:precondition
(and
(Flag64-)
(Flag64prime-)
)
:effect
(and
(Flag95-)
(Flag95prime-)
)

)
(:action Flag95Action-30
:parameters ()
:precondition
(and
(Flag92-)
(Flag92prime-)
)
:effect
(and
(Flag95-)
(Flag95prime-)
)

)
(:action Flag95Action-31
:parameters ()
:precondition
(and
(Flag53-)
(Flag53prime-)
)
:effect
(and
(Flag95-)
(Flag95prime-)
)

)
(:action Flag95Action-32
:parameters ()
:precondition
(and
(Flag38-)
(Flag38prime-)
)
:effect
(and
(Flag95-)
(Flag95prime-)
)

)
(:action Flag95Action-33
:parameters ()
:precondition
(and
(Flag50-)
(Flag50prime-)
)
:effect
(and
(Flag95-)
(Flag95prime-)
)

)
(:action Flag95Action-34
:parameters ()
:precondition
(and
(Flag81-)
(Flag81prime-)
)
:effect
(and
(Flag95-)
(Flag95prime-)
)

)
(:action Flag95Action-35
:parameters ()
:precondition
(and
(Flag73-)
(Flag73prime-)
)
:effect
(and
(Flag95-)
(Flag95prime-)
)

)
(:action Flag95Action-36
:parameters ()
:precondition
(and
(Flag41-)
(Flag41prime-)
)
:effect
(and
(Flag95-)
(Flag95prime-)
)

)
(:action Flag95Action-37
:parameters ()
:precondition
(and
(Flag51-)
(Flag51prime-)
)
:effect
(and
(Flag95-)
(Flag95prime-)
)

)
(:action Flag95Action-38
:parameters ()
:precondition
(and
(Flag52-)
(Flag52prime-)
)
:effect
(and
(Flag95-)
(Flag95prime-)
)

)
(:action Flag95Action-39
:parameters ()
:precondition
(and
(Flag29-)
(Flag29prime-)
)
:effect
(and
(Flag95-)
(Flag95prime-)
)

)
(:action Flag95Action-40
:parameters ()
:precondition
(and
(Flag93-)
(Flag93prime-)
)
:effect
(and
(Flag95-)
(Flag95prime-)
)

)
(:action Flag95Action-41
:parameters ()
:precondition
(and
(Flag82-)
(Flag82prime-)
)
:effect
(and
(Flag95-)
(Flag95prime-)
)

)
(:action Flag95Action-42
:parameters ()
:precondition
(and
(Flag67-)
(Flag67prime-)
)
:effect
(and
(Flag95-)
(Flag95prime-)
)

)
(:action Flag95Action-43
:parameters ()
:precondition
(and
(Flag83-)
(Flag83prime-)
)
:effect
(and
(Flag95-)
(Flag95prime-)
)

)
(:action Flag95Action-44
:parameters ()
:precondition
(and
(Flag94-)
(Flag94prime-)
)
:effect
(and
(Flag95-)
(Flag95prime-)
)

)
(:action Flag95Action-45
:parameters ()
:precondition
(and
(Flag68-)
(Flag68prime-)
)
:effect
(and
(Flag95-)
(Flag95prime-)
)

)
(:action Flag95Action-46
:parameters ()
:precondition
(and
(Flag32-)
(Flag32prime-)
)
:effect
(and
(Flag95-)
(Flag95prime-)
)

)
(:action Flag95Action-47
:parameters ()
:precondition
(and
(Flag69-)
(Flag69prime-)
)
:effect
(and
(Flag95-)
(Flag95prime-)
)

)
(:action Flag95Action-48
:parameters ()
:precondition
(and
(Flag54-)
(Flag54prime-)
)
:effect
(and
(Flag95-)
(Flag95prime-)
)

)
(:action Flag95Action-49
:parameters ()
:precondition
(and
(Flag39-)
(Flag39prime-)
)
:effect
(and
(Flag95-)
(Flag95prime-)
)

)
(:action Flag95Action-50
:parameters ()
:precondition
(and
(Flag65-)
(Flag65prime-)
)
:effect
(and
(Flag95-)
(Flag95prime-)
)

)
(:action Prim95Action
:parameters ()
:precondition
(and
(not (Flag85-))
(Flag85prime-)
(not (Flag57-))
(Flag57prime-)
(not (Flag71-))
(Flag71prime-)
(not (Flag77-))
(Flag77prime-)
(not (Flag58-))
(Flag58prime-)
(not (Flag86-))
(Flag86prime-)
(not (Flag59-))
(Flag59prime-)
(not (Flag35-))
(Flag35prime-)
(not (Flag45-))
(Flag45prime-)
(not (Flag60-))
(Flag60prime-)
(not (Flag75-))
(Flag75prime-)
(not (Flag46-))
(Flag46prime-)
(not (Flag34-))
(Flag34prime-)
(not (Flag87-))
(Flag87prime-)
(not (Flag74-))
(Flag74prime-)
(not (Flag37-))
(Flag37prime-)
(not (Flag76-))
(Flag76prime-)
(not (Flag88-))
(Flag88prime-)
(not (Flag89-))
(Flag89prime-)
(not (Flag90-))
(Flag90prime-)
(not (Flag47-))
(Flag47prime-)
(not (Flag78-))
(Flag78prime-)
(not (Flag79-))
(Flag79prime-)
(not (Flag48-))
(Flag48prime-)
(not (Flag49-))
(Flag49prime-)
(not (Flag33-))
(Flag33prime-)
(not (Flag63-))
(Flag63prime-)
(not (Flag36-))
(Flag36prime-)
(not (Flag91-))
(Flag91prime-)
(not (Flag64-))
(Flag64prime-)
(not (Flag92-))
(Flag92prime-)
(not (Flag53-))
(Flag53prime-)
(not (Flag38-))
(Flag38prime-)
(not (Flag50-))
(Flag50prime-)
(not (Flag81-))
(Flag81prime-)
(not (Flag73-))
(Flag73prime-)
(not (Flag41-))
(Flag41prime-)
(not (Flag51-))
(Flag51prime-)
(not (Flag52-))
(Flag52prime-)
(not (Flag29-))
(Flag29prime-)
(not (Flag93-))
(Flag93prime-)
(not (Flag82-))
(Flag82prime-)
(not (Flag67-))
(Flag67prime-)
(not (Flag83-))
(Flag83prime-)
(not (Flag94-))
(Flag94prime-)
(not (Flag68-))
(Flag68prime-)
(not (Flag32-))
(Flag32prime-)
(not (Flag69-))
(Flag69prime-)
(not (Flag54-))
(Flag54prime-)
(not (Flag39-))
(Flag39prime-)
(not (Flag65-))
(Flag65prime-)
)
:effect
(and
(Flag95prime-)
(not (Flag95-))
)

)
(:action Flag107Action-0
:parameters ()
:precondition
(and
(Flag57-)
(Flag57prime-)
)
:effect
(and
(Flag107-)
(Flag107prime-)
)

)
(:action Flag107Action-1
:parameters ()
:precondition
(and
(Flag54-)
(Flag54prime-)
)
:effect
(and
(Flag107-)
(Flag107prime-)
)

)
(:action Flag107Action-2
:parameters ()
:precondition
(and
(Flag71-)
(Flag71prime-)
)
:effect
(and
(Flag107-)
(Flag107prime-)
)

)
(:action Flag107Action-3
:parameters ()
:precondition
(and
(Flag96-)
(Flag96prime-)
)
:effect
(and
(Flag107-)
(Flag107prime-)
)

)
(:action Flag107Action-4
:parameters ()
:precondition
(and
(Flag58-)
(Flag58prime-)
)
:effect
(and
(Flag107-)
(Flag107prime-)
)

)
(:action Flag107Action-5
:parameters ()
:precondition
(and
(Flag59-)
(Flag59prime-)
)
:effect
(and
(Flag107-)
(Flag107prime-)
)

)
(:action Flag107Action-6
:parameters ()
:precondition
(and
(Flag97-)
(Flag97prime-)
)
:effect
(and
(Flag107-)
(Flag107prime-)
)

)
(:action Flag107Action-7
:parameters ()
:precondition
(and
(Flag98-)
(Flag98prime-)
)
:effect
(and
(Flag107-)
(Flag107prime-)
)

)
(:action Flag107Action-8
:parameters ()
:precondition
(and
(Flag35-)
(Flag35prime-)
)
:effect
(and
(Flag107-)
(Flag107prime-)
)

)
(:action Flag107Action-9
:parameters ()
:precondition
(and
(Flag99-)
(Flag99prime-)
)
:effect
(and
(Flag107-)
(Flag107prime-)
)

)
(:action Flag107Action-10
:parameters ()
:precondition
(and
(Flag45-)
(Flag45prime-)
)
:effect
(and
(Flag107-)
(Flag107prime-)
)

)
(:action Flag107Action-11
:parameters ()
:precondition
(and
(Flag60-)
(Flag60prime-)
)
:effect
(and
(Flag107-)
(Flag107prime-)
)

)
(:action Flag107Action-12
:parameters ()
:precondition
(and
(Flag100-)
(Flag100prime-)
)
:effect
(and
(Flag107-)
(Flag107prime-)
)

)
(:action Flag107Action-13
:parameters ()
:precondition
(and
(Flag86-)
(Flag86prime-)
)
:effect
(and
(Flag107-)
(Flag107prime-)
)

)
(:action Flag107Action-14
:parameters ()
:precondition
(and
(Flag41-)
(Flag41prime-)
)
:effect
(and
(Flag107-)
(Flag107prime-)
)

)
(:action Flag107Action-15
:parameters ()
:precondition
(and
(Flag46-)
(Flag46prime-)
)
:effect
(and
(Flag107-)
(Flag107prime-)
)

)
(:action Flag107Action-16
:parameters ()
:precondition
(and
(Flag49-)
(Flag49prime-)
)
:effect
(and
(Flag107-)
(Flag107prime-)
)

)
(:action Flag107Action-17
:parameters ()
:precondition
(and
(Flag34-)
(Flag34prime-)
)
:effect
(and
(Flag107-)
(Flag107prime-)
)

)
(:action Flag107Action-18
:parameters ()
:precondition
(and
(Flag94-)
(Flag94prime-)
)
:effect
(and
(Flag107-)
(Flag107prime-)
)

)
(:action Flag107Action-19
:parameters ()
:precondition
(and
(Flag74-)
(Flag74prime-)
)
:effect
(and
(Flag107-)
(Flag107prime-)
)

)
(:action Flag107Action-20
:parameters ()
:precondition
(and
(Flag37-)
(Flag37prime-)
)
:effect
(and
(Flag107-)
(Flag107prime-)
)

)
(:action Flag107Action-21
:parameters ()
:precondition
(and
(Flag101-)
(Flag101prime-)
)
:effect
(and
(Flag107-)
(Flag107prime-)
)

)
(:action Flag107Action-22
:parameters ()
:precondition
(and
(Flag102-)
(Flag102prime-)
)
:effect
(and
(Flag107-)
(Flag107prime-)
)

)
(:action Flag107Action-23
:parameters ()
:precondition
(and
(Flag76-)
(Flag76prime-)
)
:effect
(and
(Flag107-)
(Flag107prime-)
)

)
(:action Flag107Action-24
:parameters ()
:precondition
(and
(Flag88-)
(Flag88prime-)
)
:effect
(and
(Flag107-)
(Flag107prime-)
)

)
(:action Flag107Action-25
:parameters ()
:precondition
(and
(Flag89-)
(Flag89prime-)
)
:effect
(and
(Flag107-)
(Flag107prime-)
)

)
(:action Flag107Action-26
:parameters ()
:precondition
(and
(Flag77-)
(Flag77prime-)
)
:effect
(and
(Flag107-)
(Flag107prime-)
)

)
(:action Flag107Action-27
:parameters ()
:precondition
(and
(Flag78-)
(Flag78prime-)
)
:effect
(and
(Flag107-)
(Flag107prime-)
)

)
(:action Flag107Action-28
:parameters ()
:precondition
(and
(Flag79-)
(Flag79prime-)
)
:effect
(and
(Flag107-)
(Flag107prime-)
)

)
(:action Flag107Action-29
:parameters ()
:precondition
(and
(Flag48-)
(Flag48prime-)
)
:effect
(and
(Flag107-)
(Flag107prime-)
)

)
(:action Flag107Action-30
:parameters ()
:precondition
(and
(Flag36-)
(Flag36prime-)
)
:effect
(and
(Flag107-)
(Flag107prime-)
)

)
(:action Flag107Action-31
:parameters ()
:precondition
(and
(Flag33-)
(Flag33prime-)
)
:effect
(and
(Flag107-)
(Flag107prime-)
)

)
(:action Flag107Action-32
:parameters ()
:precondition
(and
(Flag63-)
(Flag63prime-)
)
:effect
(and
(Flag107-)
(Flag107prime-)
)

)
(:action Flag107Action-33
:parameters ()
:precondition
(and
(Flag103-)
(Flag103prime-)
)
:effect
(and
(Flag107-)
(Flag107prime-)
)

)
(:action Flag107Action-34
:parameters ()
:precondition
(and
(Flag92-)
(Flag92prime-)
)
:effect
(and
(Flag107-)
(Flag107prime-)
)

)
(:action Flag107Action-35
:parameters ()
:precondition
(and
(Flag53-)
(Flag53prime-)
)
:effect
(and
(Flag107-)
(Flag107prime-)
)

)
(:action Flag107Action-36
:parameters ()
:precondition
(and
(Flag38-)
(Flag38prime-)
)
:effect
(and
(Flag107-)
(Flag107prime-)
)

)
(:action Flag107Action-37
:parameters ()
:precondition
(and
(Flag50-)
(Flag50prime-)
)
:effect
(and
(Flag107-)
(Flag107prime-)
)

)
(:action Flag107Action-38
:parameters ()
:precondition
(and
(Flag81-)
(Flag81prime-)
)
:effect
(and
(Flag107-)
(Flag107prime-)
)

)
(:action Flag107Action-39
:parameters ()
:precondition
(and
(Flag104-)
(Flag104prime-)
)
:effect
(and
(Flag107-)
(Flag107prime-)
)

)
(:action Flag107Action-40
:parameters ()
:precondition
(and
(Flag73-)
(Flag73prime-)
)
:effect
(and
(Flag107-)
(Flag107prime-)
)

)
(:action Flag107Action-41
:parameters ()
:precondition
(and
(Flag105-)
(Flag105prime-)
)
:effect
(and
(Flag107-)
(Flag107prime-)
)

)
(:action Flag107Action-42
:parameters ()
:precondition
(and
(Flag51-)
(Flag51prime-)
)
:effect
(and
(Flag107-)
(Flag107prime-)
)

)
(:action Flag107Action-43
:parameters ()
:precondition
(and
(Flag52-)
(Flag52prime-)
)
:effect
(and
(Flag107-)
(Flag107prime-)
)

)
(:action Flag107Action-44
:parameters ()
:precondition
(and
(Flag93-)
(Flag93prime-)
)
:effect
(and
(Flag107-)
(Flag107prime-)
)

)
(:action Flag107Action-45
:parameters ()
:precondition
(and
(Flag82-)
(Flag82prime-)
)
:effect
(and
(Flag107-)
(Flag107prime-)
)

)
(:action Flag107Action-46
:parameters ()
:precondition
(and
(Flag67-)
(Flag67prime-)
)
:effect
(and
(Flag107-)
(Flag107prime-)
)

)
(:action Flag107Action-47
:parameters ()
:precondition
(and
(Flag83-)
(Flag83prime-)
)
:effect
(and
(Flag107-)
(Flag107prime-)
)

)
(:action Flag107Action-48
:parameters ()
:precondition
(and
(Flag68-)
(Flag68prime-)
)
:effect
(and
(Flag107-)
(Flag107prime-)
)

)
(:action Flag107Action-49
:parameters ()
:precondition
(and
(Flag106-)
(Flag106prime-)
)
:effect
(and
(Flag107-)
(Flag107prime-)
)

)
(:action Flag107Action-50
:parameters ()
:precondition
(and
(Flag32-)
(Flag32prime-)
)
:effect
(and
(Flag107-)
(Flag107prime-)
)

)
(:action Flag107Action-51
:parameters ()
:precondition
(and
(Flag90-)
(Flag90prime-)
)
:effect
(and
(Flag107-)
(Flag107prime-)
)

)
(:action Flag107Action-52
:parameters ()
:precondition
(and
(Flag69-)
(Flag69prime-)
)
:effect
(and
(Flag107-)
(Flag107prime-)
)

)
(:action Flag107Action-53
:parameters ()
:precondition
(and
(Flag87-)
(Flag87prime-)
)
:effect
(and
(Flag107-)
(Flag107prime-)
)

)
(:action Flag107Action-54
:parameters ()
:precondition
(and
(Flag39-)
(Flag39prime-)
)
:effect
(and
(Flag107-)
(Flag107prime-)
)

)
(:action Flag107Action-55
:parameters ()
:precondition
(and
(Flag65-)
(Flag65prime-)
)
:effect
(and
(Flag107-)
(Flag107prime-)
)

)
(:action Prim107Action
:parameters ()
:precondition
(and
(not (Flag57-))
(Flag57prime-)
(not (Flag54-))
(Flag54prime-)
(not (Flag71-))
(Flag71prime-)
(not (Flag96-))
(Flag96prime-)
(not (Flag58-))
(Flag58prime-)
(not (Flag59-))
(Flag59prime-)
(not (Flag97-))
(Flag97prime-)
(not (Flag98-))
(Flag98prime-)
(not (Flag35-))
(Flag35prime-)
(not (Flag99-))
(Flag99prime-)
(not (Flag45-))
(Flag45prime-)
(not (Flag60-))
(Flag60prime-)
(not (Flag100-))
(Flag100prime-)
(not (Flag86-))
(Flag86prime-)
(not (Flag41-))
(Flag41prime-)
(not (Flag46-))
(Flag46prime-)
(not (Flag49-))
(Flag49prime-)
(not (Flag34-))
(Flag34prime-)
(not (Flag94-))
(Flag94prime-)
(not (Flag74-))
(Flag74prime-)
(not (Flag37-))
(Flag37prime-)
(not (Flag101-))
(Flag101prime-)
(not (Flag102-))
(Flag102prime-)
(not (Flag76-))
(Flag76prime-)
(not (Flag88-))
(Flag88prime-)
(not (Flag89-))
(Flag89prime-)
(not (Flag77-))
(Flag77prime-)
(not (Flag78-))
(Flag78prime-)
(not (Flag79-))
(Flag79prime-)
(not (Flag48-))
(Flag48prime-)
(not (Flag36-))
(Flag36prime-)
(not (Flag33-))
(Flag33prime-)
(not (Flag63-))
(Flag63prime-)
(not (Flag103-))
(Flag103prime-)
(not (Flag92-))
(Flag92prime-)
(not (Flag53-))
(Flag53prime-)
(not (Flag38-))
(Flag38prime-)
(not (Flag50-))
(Flag50prime-)
(not (Flag81-))
(Flag81prime-)
(not (Flag104-))
(Flag104prime-)
(not (Flag73-))
(Flag73prime-)
(not (Flag105-))
(Flag105prime-)
(not (Flag51-))
(Flag51prime-)
(not (Flag52-))
(Flag52prime-)
(not (Flag93-))
(Flag93prime-)
(not (Flag82-))
(Flag82prime-)
(not (Flag67-))
(Flag67prime-)
(not (Flag83-))
(Flag83prime-)
(not (Flag68-))
(Flag68prime-)
(not (Flag106-))
(Flag106prime-)
(not (Flag32-))
(Flag32prime-)
(not (Flag90-))
(Flag90prime-)
(not (Flag69-))
(Flag69prime-)
(not (Flag87-))
(Flag87prime-)
(not (Flag39-))
(Flag39prime-)
(not (Flag65-))
(Flag65prime-)
)
:effect
(and
(Flag107prime-)
(not (Flag107-))
)

)
(:action Flag118Action-0
:parameters ()
:precondition
(and
(Flag32-)
(Flag32prime-)
)
:effect
(and
(Flag118-)
(Flag118prime-)
)

)
(:action Flag118Action-1
:parameters ()
:precondition
(and
(Flag74-)
(Flag74prime-)
)
:effect
(and
(Flag118-)
(Flag118prime-)
)

)
(:action Flag118Action-2
:parameters ()
:precondition
(and
(Flag108-)
(Flag108prime-)
)
:effect
(and
(Flag118-)
(Flag118prime-)
)

)
(:action Flag118Action-3
:parameters ()
:precondition
(and
(Flag71-)
(Flag71prime-)
)
:effect
(and
(Flag118-)
(Flag118prime-)
)

)
(:action Flag118Action-4
:parameters ()
:precondition
(and
(Flag96-)
(Flag96prime-)
)
:effect
(and
(Flag118-)
(Flag118prime-)
)

)
(:action Flag118Action-5
:parameters ()
:precondition
(and
(Flag48-)
(Flag48prime-)
)
:effect
(and
(Flag118-)
(Flag118prime-)
)

)
(:action Flag118Action-6
:parameters ()
:precondition
(and
(Flag106-)
(Flag106prime-)
)
:effect
(and
(Flag118-)
(Flag118prime-)
)

)
(:action Flag118Action-7
:parameters ()
:precondition
(and
(Flag59-)
(Flag59prime-)
)
:effect
(and
(Flag118-)
(Flag118prime-)
)

)
(:action Flag118Action-8
:parameters ()
:precondition
(and
(Flag97-)
(Flag97prime-)
)
:effect
(and
(Flag118-)
(Flag118prime-)
)

)
(:action Flag118Action-9
:parameters ()
:precondition
(and
(Flag98-)
(Flag98prime-)
)
:effect
(and
(Flag118-)
(Flag118prime-)
)

)
(:action Flag118Action-10
:parameters ()
:precondition
(and
(Flag109-)
(Flag109prime-)
)
:effect
(and
(Flag118-)
(Flag118prime-)
)

)
(:action Flag118Action-11
:parameters ()
:precondition
(and
(Flag35-)
(Flag35prime-)
)
:effect
(and
(Flag118-)
(Flag118prime-)
)

)
(:action Flag118Action-12
:parameters ()
:precondition
(and
(Flag99-)
(Flag99prime-)
)
:effect
(and
(Flag118-)
(Flag118prime-)
)

)
(:action Flag118Action-13
:parameters ()
:precondition
(and
(Flag110-)
(Flag110prime-)
)
:effect
(and
(Flag118-)
(Flag118prime-)
)

)
(:action Flag118Action-14
:parameters ()
:precondition
(and
(Flag45-)
(Flag45prime-)
)
:effect
(and
(Flag118-)
(Flag118prime-)
)

)
(:action Flag118Action-15
:parameters ()
:precondition
(and
(Flag60-)
(Flag60prime-)
)
:effect
(and
(Flag118-)
(Flag118prime-)
)

)
(:action Flag118Action-16
:parameters ()
:precondition
(and
(Flag86-)
(Flag86prime-)
)
:effect
(and
(Flag118-)
(Flag118prime-)
)

)
(:action Flag118Action-17
:parameters ()
:precondition
(and
(Flag41-)
(Flag41prime-)
)
:effect
(and
(Flag118-)
(Flag118prime-)
)

)
(:action Flag118Action-18
:parameters ()
:precondition
(and
(Flag46-)
(Flag46prime-)
)
:effect
(and
(Flag118-)
(Flag118prime-)
)

)
(:action Flag118Action-19
:parameters ()
:precondition
(and
(Flag34-)
(Flag34prime-)
)
:effect
(and
(Flag118-)
(Flag118prime-)
)

)
(:action Flag118Action-20
:parameters ()
:precondition
(and
(Flag94-)
(Flag94prime-)
)
:effect
(and
(Flag118-)
(Flag118prime-)
)

)
(:action Flag118Action-21
:parameters ()
:precondition
(and
(Flag111-)
(Flag111prime-)
)
:effect
(and
(Flag118-)
(Flag118prime-)
)

)
(:action Flag118Action-22
:parameters ()
:precondition
(and
(Flag101-)
(Flag101prime-)
)
:effect
(and
(Flag118-)
(Flag118prime-)
)

)
(:action Flag118Action-23
:parameters ()
:precondition
(and
(Flag76-)
(Flag76prime-)
)
:effect
(and
(Flag118-)
(Flag118prime-)
)

)
(:action Flag118Action-24
:parameters ()
:precondition
(and
(Flag88-)
(Flag88prime-)
)
:effect
(and
(Flag118-)
(Flag118prime-)
)

)
(:action Flag118Action-25
:parameters ()
:precondition
(and
(Flag89-)
(Flag89prime-)
)
:effect
(and
(Flag118-)
(Flag118prime-)
)

)
(:action Flag118Action-26
:parameters ()
:precondition
(and
(Flag77-)
(Flag77prime-)
)
:effect
(and
(Flag118-)
(Flag118prime-)
)

)
(:action Flag118Action-27
:parameters ()
:precondition
(and
(Flag78-)
(Flag78prime-)
)
:effect
(and
(Flag118-)
(Flag118prime-)
)

)
(:action Flag118Action-28
:parameters ()
:precondition
(and
(Flag112-)
(Flag112prime-)
)
:effect
(and
(Flag118-)
(Flag118prime-)
)

)
(:action Flag118Action-29
:parameters ()
:precondition
(and
(Flag58-)
(Flag58prime-)
)
:effect
(and
(Flag118-)
(Flag118prime-)
)

)
(:action Flag118Action-30
:parameters ()
:precondition
(and
(Flag49-)
(Flag49prime-)
)
:effect
(and
(Flag118-)
(Flag118prime-)
)

)
(:action Flag118Action-31
:parameters ()
:precondition
(and
(Flag33-)
(Flag33prime-)
)
:effect
(and
(Flag118-)
(Flag118prime-)
)

)
(:action Flag118Action-32
:parameters ()
:precondition
(and
(Flag63-)
(Flag63prime-)
)
:effect
(and
(Flag118-)
(Flag118prime-)
)

)
(:action Flag118Action-33
:parameters ()
:precondition
(and
(Flag36-)
(Flag36prime-)
)
:effect
(and
(Flag118-)
(Flag118prime-)
)

)
(:action Flag118Action-34
:parameters ()
:precondition
(and
(Flag92-)
(Flag92prime-)
)
:effect
(and
(Flag118-)
(Flag118prime-)
)

)
(:action Flag118Action-35
:parameters ()
:precondition
(and
(Flag53-)
(Flag53prime-)
)
:effect
(and
(Flag118-)
(Flag118prime-)
)

)
(:action Flag118Action-36
:parameters ()
:precondition
(and
(Flag38-)
(Flag38prime-)
)
:effect
(and
(Flag118-)
(Flag118prime-)
)

)
(:action Flag118Action-37
:parameters ()
:precondition
(and
(Flag65-)
(Flag65prime-)
)
:effect
(and
(Flag118-)
(Flag118prime-)
)

)
(:action Flag118Action-38
:parameters ()
:precondition
(and
(Flag81-)
(Flag81prime-)
)
:effect
(and
(Flag118-)
(Flag118prime-)
)

)
(:action Flag118Action-39
:parameters ()
:precondition
(and
(Flag104-)
(Flag104prime-)
)
:effect
(and
(Flag118-)
(Flag118prime-)
)

)
(:action Flag118Action-40
:parameters ()
:precondition
(and
(Flag73-)
(Flag73prime-)
)
:effect
(and
(Flag118-)
(Flag118prime-)
)

)
(:action Flag118Action-41
:parameters ()
:precondition
(and
(Flag113-)
(Flag113prime-)
)
:effect
(and
(Flag118-)
(Flag118prime-)
)

)
(:action Flag118Action-42
:parameters ()
:precondition
(and
(Flag105-)
(Flag105prime-)
)
:effect
(and
(Flag118-)
(Flag118prime-)
)

)
(:action Flag118Action-43
:parameters ()
:precondition
(and
(Flag51-)
(Flag51prime-)
)
:effect
(and
(Flag118-)
(Flag118prime-)
)

)
(:action Flag118Action-44
:parameters ()
:precondition
(and
(Flag52-)
(Flag52prime-)
)
:effect
(and
(Flag118-)
(Flag118prime-)
)

)
(:action Flag118Action-45
:parameters ()
:precondition
(and
(Flag93-)
(Flag93prime-)
)
:effect
(and
(Flag118-)
(Flag118prime-)
)

)
(:action Flag118Action-46
:parameters ()
:precondition
(and
(Flag82-)
(Flag82prime-)
)
:effect
(and
(Flag118-)
(Flag118prime-)
)

)
(:action Flag118Action-47
:parameters ()
:precondition
(and
(Flag114-)
(Flag114prime-)
)
:effect
(and
(Flag118-)
(Flag118prime-)
)

)
(:action Flag118Action-48
:parameters ()
:precondition
(and
(Flag67-)
(Flag67prime-)
)
:effect
(and
(Flag118-)
(Flag118prime-)
)

)
(:action Flag118Action-49
:parameters ()
:precondition
(and
(Flag115-)
(Flag115prime-)
)
:effect
(and
(Flag118-)
(Flag118prime-)
)

)
(:action Flag118Action-50
:parameters ()
:precondition
(and
(Flag68-)
(Flag68prime-)
)
:effect
(and
(Flag118-)
(Flag118prime-)
)

)
(:action Flag118Action-51
:parameters ()
:precondition
(and
(Flag116-)
(Flag116prime-)
)
:effect
(and
(Flag118-)
(Flag118prime-)
)

)
(:action Flag118Action-52
:parameters ()
:precondition
(and
(Flag117-)
(Flag117prime-)
)
:effect
(and
(Flag118-)
(Flag118prime-)
)

)
(:action Flag118Action-53
:parameters ()
:precondition
(and
(Flag83-)
(Flag83prime-)
)
:effect
(and
(Flag118-)
(Flag118prime-)
)

)
(:action Flag118Action-54
:parameters ()
:precondition
(and
(Flag90-)
(Flag90prime-)
)
:effect
(and
(Flag118-)
(Flag118prime-)
)

)
(:action Flag118Action-55
:parameters ()
:precondition
(and
(Flag69-)
(Flag69prime-)
)
:effect
(and
(Flag118-)
(Flag118prime-)
)

)
(:action Flag118Action-56
:parameters ()
:precondition
(and
(Flag87-)
(Flag87prime-)
)
:effect
(and
(Flag118-)
(Flag118prime-)
)

)
(:action Flag118Action-57
:parameters ()
:precondition
(and
(Flag39-)
(Flag39prime-)
)
:effect
(and
(Flag118-)
(Flag118prime-)
)

)
(:action Flag118Action-58
:parameters ()
:precondition
(and
(Flag50-)
(Flag50prime-)
)
:effect
(and
(Flag118-)
(Flag118prime-)
)

)
(:action Prim118Action
:parameters ()
:precondition
(and
(not (Flag32-))
(Flag32prime-)
(not (Flag74-))
(Flag74prime-)
(not (Flag108-))
(Flag108prime-)
(not (Flag71-))
(Flag71prime-)
(not (Flag96-))
(Flag96prime-)
(not (Flag48-))
(Flag48prime-)
(not (Flag106-))
(Flag106prime-)
(not (Flag59-))
(Flag59prime-)
(not (Flag97-))
(Flag97prime-)
(not (Flag98-))
(Flag98prime-)
(not (Flag109-))
(Flag109prime-)
(not (Flag35-))
(Flag35prime-)
(not (Flag99-))
(Flag99prime-)
(not (Flag110-))
(Flag110prime-)
(not (Flag45-))
(Flag45prime-)
(not (Flag60-))
(Flag60prime-)
(not (Flag86-))
(Flag86prime-)
(not (Flag41-))
(Flag41prime-)
(not (Flag46-))
(Flag46prime-)
(not (Flag34-))
(Flag34prime-)
(not (Flag94-))
(Flag94prime-)
(not (Flag111-))
(Flag111prime-)
(not (Flag101-))
(Flag101prime-)
(not (Flag76-))
(Flag76prime-)
(not (Flag88-))
(Flag88prime-)
(not (Flag89-))
(Flag89prime-)
(not (Flag77-))
(Flag77prime-)
(not (Flag78-))
(Flag78prime-)
(not (Flag112-))
(Flag112prime-)
(not (Flag58-))
(Flag58prime-)
(not (Flag49-))
(Flag49prime-)
(not (Flag33-))
(Flag33prime-)
(not (Flag63-))
(Flag63prime-)
(not (Flag36-))
(Flag36prime-)
(not (Flag92-))
(Flag92prime-)
(not (Flag53-))
(Flag53prime-)
(not (Flag38-))
(Flag38prime-)
(not (Flag65-))
(Flag65prime-)
(not (Flag81-))
(Flag81prime-)
(not (Flag104-))
(Flag104prime-)
(not (Flag73-))
(Flag73prime-)
(not (Flag113-))
(Flag113prime-)
(not (Flag105-))
(Flag105prime-)
(not (Flag51-))
(Flag51prime-)
(not (Flag52-))
(Flag52prime-)
(not (Flag93-))
(Flag93prime-)
(not (Flag82-))
(Flag82prime-)
(not (Flag114-))
(Flag114prime-)
(not (Flag67-))
(Flag67prime-)
(not (Flag115-))
(Flag115prime-)
(not (Flag68-))
(Flag68prime-)
(not (Flag116-))
(Flag116prime-)
(not (Flag117-))
(Flag117prime-)
(not (Flag83-))
(Flag83prime-)
(not (Flag90-))
(Flag90prime-)
(not (Flag69-))
(Flag69prime-)
(not (Flag87-))
(Flag87prime-)
(not (Flag39-))
(Flag39prime-)
(not (Flag50-))
(Flag50prime-)
)
:effect
(and
(Flag118prime-)
(not (Flag118-))
)

)
(:action Flag127Action-0
:parameters ()
:precondition
(and
(Flag32-)
(Flag32prime-)
)
:effect
(and
(Flag127-)
(Flag127prime-)
)

)
(:action Flag127Action-1
:parameters ()
:precondition
(and
(Flag74-)
(Flag74prime-)
)
:effect
(and
(Flag127-)
(Flag127prime-)
)

)
(:action Flag127Action-2
:parameters ()
:precondition
(and
(Flag71-)
(Flag71prime-)
)
:effect
(and
(Flag127-)
(Flag127prime-)
)

)
(:action Flag127Action-3
:parameters ()
:precondition
(and
(Flag96-)
(Flag96prime-)
)
:effect
(and
(Flag127-)
(Flag127prime-)
)

)
(:action Flag127Action-4
:parameters ()
:precondition
(and
(Flag114-)
(Flag114prime-)
)
:effect
(and
(Flag127-)
(Flag127prime-)
)

)
(:action Flag127Action-5
:parameters ()
:precondition
(and
(Flag119-)
(Flag119prime-)
)
:effect
(and
(Flag127-)
(Flag127prime-)
)

)
(:action Flag127Action-6
:parameters ()
:precondition
(and
(Flag106-)
(Flag106prime-)
)
:effect
(and
(Flag127-)
(Flag127prime-)
)

)
(:action Flag127Action-7
:parameters ()
:precondition
(and
(Flag59-)
(Flag59prime-)
)
:effect
(and
(Flag127-)
(Flag127prime-)
)

)
(:action Flag127Action-8
:parameters ()
:precondition
(and
(Flag97-)
(Flag97prime-)
)
:effect
(and
(Flag127-)
(Flag127prime-)
)

)
(:action Flag127Action-9
:parameters ()
:precondition
(and
(Flag98-)
(Flag98prime-)
)
:effect
(and
(Flag127-)
(Flag127prime-)
)

)
(:action Flag127Action-10
:parameters ()
:precondition
(and
(Flag109-)
(Flag109prime-)
)
:effect
(and
(Flag127-)
(Flag127prime-)
)

)
(:action Flag127Action-11
:parameters ()
:precondition
(and
(Flag35-)
(Flag35prime-)
)
:effect
(and
(Flag127-)
(Flag127prime-)
)

)
(:action Flag127Action-12
:parameters ()
:precondition
(and
(Flag99-)
(Flag99prime-)
)
:effect
(and
(Flag127-)
(Flag127prime-)
)

)
(:action Flag127Action-13
:parameters ()
:precondition
(and
(Flag50-)
(Flag50prime-)
)
:effect
(and
(Flag127-)
(Flag127prime-)
)

)
(:action Flag127Action-14
:parameters ()
:precondition
(and
(Flag45-)
(Flag45prime-)
)
:effect
(and
(Flag127-)
(Flag127prime-)
)

)
(:action Flag127Action-15
:parameters ()
:precondition
(and
(Flag60-)
(Flag60prime-)
)
:effect
(and
(Flag127-)
(Flag127prime-)
)

)
(:action Flag127Action-16
:parameters ()
:precondition
(and
(Flag86-)
(Flag86prime-)
)
:effect
(and
(Flag127-)
(Flag127prime-)
)

)
(:action Flag127Action-17
:parameters ()
:precondition
(and
(Flag41-)
(Flag41prime-)
)
:effect
(and
(Flag127-)
(Flag127prime-)
)

)
(:action Flag127Action-18
:parameters ()
:precondition
(and
(Flag46-)
(Flag46prime-)
)
:effect
(and
(Flag127-)
(Flag127prime-)
)

)
(:action Flag127Action-19
:parameters ()
:precondition
(and
(Flag34-)
(Flag34prime-)
)
:effect
(and
(Flag127-)
(Flag127prime-)
)

)
(:action Flag127Action-20
:parameters ()
:precondition
(and
(Flag111-)
(Flag111prime-)
)
:effect
(and
(Flag127-)
(Flag127prime-)
)

)
(:action Flag127Action-21
:parameters ()
:precondition
(and
(Flag101-)
(Flag101prime-)
)
:effect
(and
(Flag127-)
(Flag127prime-)
)

)
(:action Flag127Action-22
:parameters ()
:precondition
(and
(Flag120-)
(Flag120prime-)
)
:effect
(and
(Flag127-)
(Flag127prime-)
)

)
(:action Flag127Action-23
:parameters ()
:precondition
(and
(Flag76-)
(Flag76prime-)
)
:effect
(and
(Flag127-)
(Flag127prime-)
)

)
(:action Flag127Action-24
:parameters ()
:precondition
(and
(Flag88-)
(Flag88prime-)
)
:effect
(and
(Flag127-)
(Flag127prime-)
)

)
(:action Flag127Action-25
:parameters ()
:precondition
(and
(Flag89-)
(Flag89prime-)
)
:effect
(and
(Flag127-)
(Flag127prime-)
)

)
(:action Flag127Action-26
:parameters ()
:precondition
(and
(Flag121-)
(Flag121prime-)
)
:effect
(and
(Flag127-)
(Flag127prime-)
)

)
(:action Flag127Action-27
:parameters ()
:precondition
(and
(Flag77-)
(Flag77prime-)
)
:effect
(and
(Flag127-)
(Flag127prime-)
)

)
(:action Flag127Action-28
:parameters ()
:precondition
(and
(Flag78-)
(Flag78prime-)
)
:effect
(and
(Flag127-)
(Flag127prime-)
)

)
(:action Flag127Action-29
:parameters ()
:precondition
(and
(Flag53-)
(Flag53prime-)
)
:effect
(and
(Flag127-)
(Flag127prime-)
)

)
(:action Flag127Action-30
:parameters ()
:precondition
(and
(Flag112-)
(Flag112prime-)
)
:effect
(and
(Flag127-)
(Flag127prime-)
)

)
(:action Flag127Action-31
:parameters ()
:precondition
(and
(Flag90-)
(Flag90prime-)
)
:effect
(and
(Flag127-)
(Flag127prime-)
)

)
(:action Flag127Action-32
:parameters ()
:precondition
(and
(Flag122-)
(Flag122prime-)
)
:effect
(and
(Flag127-)
(Flag127prime-)
)

)
(:action Flag127Action-33
:parameters ()
:precondition
(and
(Flag49-)
(Flag49prime-)
)
:effect
(and
(Flag127-)
(Flag127prime-)
)

)
(:action Flag127Action-34
:parameters ()
:precondition
(and
(Flag33-)
(Flag33prime-)
)
:effect
(and
(Flag127-)
(Flag127prime-)
)

)
(:action Flag127Action-35
:parameters ()
:precondition
(and
(Flag63-)
(Flag63prime-)
)
:effect
(and
(Flag127-)
(Flag127prime-)
)

)
(:action Flag127Action-36
:parameters ()
:precondition
(and
(Flag92-)
(Flag92prime-)
)
:effect
(and
(Flag127-)
(Flag127prime-)
)

)
(:action Flag127Action-37
:parameters ()
:precondition
(and
(Flag123-)
(Flag123prime-)
)
:effect
(and
(Flag127-)
(Flag127prime-)
)

)
(:action Flag127Action-38
:parameters ()
:precondition
(and
(Flag38-)
(Flag38prime-)
)
:effect
(and
(Flag127-)
(Flag127prime-)
)

)
(:action Flag127Action-39
:parameters ()
:precondition
(and
(Flag65-)
(Flag65prime-)
)
:effect
(and
(Flag127-)
(Flag127prime-)
)

)
(:action Flag127Action-40
:parameters ()
:precondition
(and
(Flag81-)
(Flag81prime-)
)
:effect
(and
(Flag127-)
(Flag127prime-)
)

)
(:action Flag127Action-41
:parameters ()
:precondition
(and
(Flag104-)
(Flag104prime-)
)
:effect
(and
(Flag127-)
(Flag127prime-)
)

)
(:action Flag127Action-42
:parameters ()
:precondition
(and
(Flag113-)
(Flag113prime-)
)
:effect
(and
(Flag127-)
(Flag127prime-)
)

)
(:action Flag127Action-43
:parameters ()
:precondition
(and
(Flag105-)
(Flag105prime-)
)
:effect
(and
(Flag127-)
(Flag127prime-)
)

)
(:action Flag127Action-44
:parameters ()
:precondition
(and
(Flag51-)
(Flag51prime-)
)
:effect
(and
(Flag127-)
(Flag127prime-)
)

)
(:action Flag127Action-45
:parameters ()
:precondition
(and
(Flag52-)
(Flag52prime-)
)
:effect
(and
(Flag127-)
(Flag127prime-)
)

)
(:action Flag127Action-46
:parameters ()
:precondition
(and
(Flag124-)
(Flag124prime-)
)
:effect
(and
(Flag127-)
(Flag127prime-)
)

)
(:action Flag127Action-47
:parameters ()
:precondition
(and
(Flag93-)
(Flag93prime-)
)
:effect
(and
(Flag127-)
(Flag127prime-)
)

)
(:action Flag127Action-48
:parameters ()
:precondition
(and
(Flag82-)
(Flag82prime-)
)
:effect
(and
(Flag127-)
(Flag127prime-)
)

)
(:action Flag127Action-49
:parameters ()
:precondition
(and
(Flag67-)
(Flag67prime-)
)
:effect
(and
(Flag127-)
(Flag127prime-)
)

)
(:action Flag127Action-50
:parameters ()
:precondition
(and
(Flag83-)
(Flag83prime-)
)
:effect
(and
(Flag127-)
(Flag127prime-)
)

)
(:action Flag127Action-51
:parameters ()
:precondition
(and
(Flag68-)
(Flag68prime-)
)
:effect
(and
(Flag127-)
(Flag127prime-)
)

)
(:action Flag127Action-52
:parameters ()
:precondition
(and
(Flag116-)
(Flag116prime-)
)
:effect
(and
(Flag127-)
(Flag127prime-)
)

)
(:action Flag127Action-53
:parameters ()
:precondition
(and
(Flag117-)
(Flag117prime-)
)
:effect
(and
(Flag127-)
(Flag127prime-)
)

)
(:action Flag127Action-54
:parameters ()
:precondition
(and
(Flag125-)
(Flag125prime-)
)
:effect
(and
(Flag127-)
(Flag127prime-)
)

)
(:action Flag127Action-55
:parameters ()
:precondition
(and
(Flag69-)
(Flag69prime-)
)
:effect
(and
(Flag127-)
(Flag127prime-)
)

)
(:action Flag127Action-56
:parameters ()
:precondition
(and
(Flag94-)
(Flag94prime-)
)
:effect
(and
(Flag127-)
(Flag127prime-)
)

)
(:action Flag127Action-57
:parameters ()
:precondition
(and
(Flag39-)
(Flag39prime-)
)
:effect
(and
(Flag127-)
(Flag127prime-)
)

)
(:action Flag127Action-58
:parameters ()
:precondition
(and
(Flag126-)
(Flag126prime-)
)
:effect
(and
(Flag127-)
(Flag127prime-)
)

)
(:action Prim127Action
:parameters ()
:precondition
(and
(not (Flag32-))
(Flag32prime-)
(not (Flag74-))
(Flag74prime-)
(not (Flag71-))
(Flag71prime-)
(not (Flag96-))
(Flag96prime-)
(not (Flag114-))
(Flag114prime-)
(not (Flag119-))
(Flag119prime-)
(not (Flag106-))
(Flag106prime-)
(not (Flag59-))
(Flag59prime-)
(not (Flag97-))
(Flag97prime-)
(not (Flag98-))
(Flag98prime-)
(not (Flag109-))
(Flag109prime-)
(not (Flag35-))
(Flag35prime-)
(not (Flag99-))
(Flag99prime-)
(not (Flag50-))
(Flag50prime-)
(not (Flag45-))
(Flag45prime-)
(not (Flag60-))
(Flag60prime-)
(not (Flag86-))
(Flag86prime-)
(not (Flag41-))
(Flag41prime-)
(not (Flag46-))
(Flag46prime-)
(not (Flag34-))
(Flag34prime-)
(not (Flag111-))
(Flag111prime-)
(not (Flag101-))
(Flag101prime-)
(not (Flag120-))
(Flag120prime-)
(not (Flag76-))
(Flag76prime-)
(not (Flag88-))
(Flag88prime-)
(not (Flag89-))
(Flag89prime-)
(not (Flag121-))
(Flag121prime-)
(not (Flag77-))
(Flag77prime-)
(not (Flag78-))
(Flag78prime-)
(not (Flag53-))
(Flag53prime-)
(not (Flag112-))
(Flag112prime-)
(not (Flag90-))
(Flag90prime-)
(not (Flag122-))
(Flag122prime-)
(not (Flag49-))
(Flag49prime-)
(not (Flag33-))
(Flag33prime-)
(not (Flag63-))
(Flag63prime-)
(not (Flag92-))
(Flag92prime-)
(not (Flag123-))
(Flag123prime-)
(not (Flag38-))
(Flag38prime-)
(not (Flag65-))
(Flag65prime-)
(not (Flag81-))
(Flag81prime-)
(not (Flag104-))
(Flag104prime-)
(not (Flag113-))
(Flag113prime-)
(not (Flag105-))
(Flag105prime-)
(not (Flag51-))
(Flag51prime-)
(not (Flag52-))
(Flag52prime-)
(not (Flag124-))
(Flag124prime-)
(not (Flag93-))
(Flag93prime-)
(not (Flag82-))
(Flag82prime-)
(not (Flag67-))
(Flag67prime-)
(not (Flag83-))
(Flag83prime-)
(not (Flag68-))
(Flag68prime-)
(not (Flag116-))
(Flag116prime-)
(not (Flag117-))
(Flag117prime-)
(not (Flag125-))
(Flag125prime-)
(not (Flag69-))
(Flag69prime-)
(not (Flag94-))
(Flag94prime-)
(not (Flag39-))
(Flag39prime-)
(not (Flag126-))
(Flag126prime-)
)
:effect
(and
(Flag127prime-)
(not (Flag127-))
)

)
(:action Flag136Action-0
:parameters ()
:precondition
(and
(Flag128-)
(Flag128prime-)
)
:effect
(and
(Flag136-)
(Flag136prime-)
)

)
(:action Flag136Action-1
:parameters ()
:precondition
(and
(Flag74-)
(Flag74prime-)
)
:effect
(and
(Flag136-)
(Flag136prime-)
)

)
(:action Flag136Action-2
:parameters ()
:precondition
(and
(Flag71-)
(Flag71prime-)
)
:effect
(and
(Flag136-)
(Flag136prime-)
)

)
(:action Flag136Action-3
:parameters ()
:precondition
(and
(Flag116-)
(Flag116prime-)
)
:effect
(and
(Flag136-)
(Flag136prime-)
)

)
(:action Flag136Action-4
:parameters ()
:precondition
(and
(Flag96-)
(Flag96prime-)
)
:effect
(and
(Flag136-)
(Flag136prime-)
)

)
(:action Flag136Action-5
:parameters ()
:precondition
(and
(Flag114-)
(Flag114prime-)
)
:effect
(and
(Flag136-)
(Flag136prime-)
)

)
(:action Flag136Action-6
:parameters ()
:precondition
(and
(Flag119-)
(Flag119prime-)
)
:effect
(and
(Flag136-)
(Flag136prime-)
)

)
(:action Flag136Action-7
:parameters ()
:precondition
(and
(Flag117-)
(Flag117prime-)
)
:effect
(and
(Flag136-)
(Flag136prime-)
)

)
(:action Flag136Action-8
:parameters ()
:precondition
(and
(Flag129-)
(Flag129prime-)
)
:effect
(and
(Flag136-)
(Flag136prime-)
)

)
(:action Flag136Action-9
:parameters ()
:precondition
(and
(Flag97-)
(Flag97prime-)
)
:effect
(and
(Flag136-)
(Flag136prime-)
)

)
(:action Flag136Action-10
:parameters ()
:precondition
(and
(Flag98-)
(Flag98prime-)
)
:effect
(and
(Flag136-)
(Flag136prime-)
)

)
(:action Flag136Action-11
:parameters ()
:precondition
(and
(Flag109-)
(Flag109prime-)
)
:effect
(and
(Flag136-)
(Flag136prime-)
)

)
(:action Flag136Action-12
:parameters ()
:precondition
(and
(Flag35-)
(Flag35prime-)
)
:effect
(and
(Flag136-)
(Flag136prime-)
)

)
(:action Flag136Action-13
:parameters ()
:precondition
(and
(Flag99-)
(Flag99prime-)
)
:effect
(and
(Flag136-)
(Flag136prime-)
)

)
(:action Flag136Action-14
:parameters ()
:precondition
(and
(Flag50-)
(Flag50prime-)
)
:effect
(and
(Flag136-)
(Flag136prime-)
)

)
(:action Flag136Action-15
:parameters ()
:precondition
(and
(Flag60-)
(Flag60prime-)
)
:effect
(and
(Flag136-)
(Flag136prime-)
)

)
(:action Flag136Action-16
:parameters ()
:precondition
(and
(Flag130-)
(Flag130prime-)
)
:effect
(and
(Flag136-)
(Flag136prime-)
)

)
(:action Flag136Action-17
:parameters ()
:precondition
(and
(Flag86-)
(Flag86prime-)
)
:effect
(and
(Flag136-)
(Flag136prime-)
)

)
(:action Flag136Action-18
:parameters ()
:precondition
(and
(Flag41-)
(Flag41prime-)
)
:effect
(and
(Flag136-)
(Flag136prime-)
)

)
(:action Flag136Action-19
:parameters ()
:precondition
(and
(Flag46-)
(Flag46prime-)
)
:effect
(and
(Flag136-)
(Flag136prime-)
)

)
(:action Flag136Action-20
:parameters ()
:precondition
(and
(Flag111-)
(Flag111prime-)
)
:effect
(and
(Flag136-)
(Flag136prime-)
)

)
(:action Flag136Action-21
:parameters ()
:precondition
(and
(Flag120-)
(Flag120prime-)
)
:effect
(and
(Flag136-)
(Flag136prime-)
)

)
(:action Flag136Action-22
:parameters ()
:precondition
(and
(Flag76-)
(Flag76prime-)
)
:effect
(and
(Flag136-)
(Flag136prime-)
)

)
(:action Flag136Action-23
:parameters ()
:precondition
(and
(Flag88-)
(Flag88prime-)
)
:effect
(and
(Flag136-)
(Flag136prime-)
)

)
(:action Flag136Action-24
:parameters ()
:precondition
(and
(Flag89-)
(Flag89prime-)
)
:effect
(and
(Flag136-)
(Flag136prime-)
)

)
(:action Flag136Action-25
:parameters ()
:precondition
(and
(Flag121-)
(Flag121prime-)
)
:effect
(and
(Flag136-)
(Flag136prime-)
)

)
(:action Flag136Action-26
:parameters ()
:precondition
(and
(Flag90-)
(Flag90prime-)
)
:effect
(and
(Flag136-)
(Flag136prime-)
)

)
(:action Flag136Action-27
:parameters ()
:precondition
(and
(Flag78-)
(Flag78prime-)
)
:effect
(and
(Flag136-)
(Flag136prime-)
)

)
(:action Flag136Action-28
:parameters ()
:precondition
(and
(Flag53-)
(Flag53prime-)
)
:effect
(and
(Flag136-)
(Flag136prime-)
)

)
(:action Flag136Action-29
:parameters ()
:precondition
(and
(Flag112-)
(Flag112prime-)
)
:effect
(and
(Flag136-)
(Flag136prime-)
)

)
(:action Flag136Action-30
:parameters ()
:precondition
(and
(Flag49-)
(Flag49prime-)
)
:effect
(and
(Flag136-)
(Flag136prime-)
)

)
(:action Flag136Action-31
:parameters ()
:precondition
(and
(Flag33-)
(Flag33prime-)
)
:effect
(and
(Flag136-)
(Flag136prime-)
)

)
(:action Flag136Action-32
:parameters ()
:precondition
(and
(Flag63-)
(Flag63prime-)
)
:effect
(and
(Flag136-)
(Flag136prime-)
)

)
(:action Flag136Action-33
:parameters ()
:precondition
(and
(Flag123-)
(Flag123prime-)
)
:effect
(and
(Flag136-)
(Flag136prime-)
)

)
(:action Flag136Action-34
:parameters ()
:precondition
(and
(Flag38-)
(Flag38prime-)
)
:effect
(and
(Flag136-)
(Flag136prime-)
)

)
(:action Flag136Action-35
:parameters ()
:precondition
(and
(Flag65-)
(Flag65prime-)
)
:effect
(and
(Flag136-)
(Flag136prime-)
)

)
(:action Flag136Action-36
:parameters ()
:precondition
(and
(Flag81-)
(Flag81prime-)
)
:effect
(and
(Flag136-)
(Flag136prime-)
)

)
(:action Flag136Action-37
:parameters ()
:precondition
(and
(Flag104-)
(Flag104prime-)
)
:effect
(and
(Flag136-)
(Flag136prime-)
)

)
(:action Flag136Action-38
:parameters ()
:precondition
(and
(Flag113-)
(Flag113prime-)
)
:effect
(and
(Flag136-)
(Flag136prime-)
)

)
(:action Flag136Action-39
:parameters ()
:precondition
(and
(Flag105-)
(Flag105prime-)
)
:effect
(and
(Flag136-)
(Flag136prime-)
)

)
(:action Flag136Action-40
:parameters ()
:precondition
(and
(Flag51-)
(Flag51prime-)
)
:effect
(and
(Flag136-)
(Flag136prime-)
)

)
(:action Flag136Action-41
:parameters ()
:precondition
(and
(Flag52-)
(Flag52prime-)
)
:effect
(and
(Flag136-)
(Flag136prime-)
)

)
(:action Flag136Action-42
:parameters ()
:precondition
(and
(Flag131-)
(Flag131prime-)
)
:effect
(and
(Flag136-)
(Flag136prime-)
)

)
(:action Flag136Action-43
:parameters ()
:precondition
(and
(Flag93-)
(Flag93prime-)
)
:effect
(and
(Flag136-)
(Flag136prime-)
)

)
(:action Flag136Action-44
:parameters ()
:precondition
(and
(Flag82-)
(Flag82prime-)
)
:effect
(and
(Flag136-)
(Flag136prime-)
)

)
(:action Flag136Action-45
:parameters ()
:precondition
(and
(Flag132-)
(Flag132prime-)
)
:effect
(and
(Flag136-)
(Flag136prime-)
)

)
(:action Flag136Action-46
:parameters ()
:precondition
(and
(Flag67-)
(Flag67prime-)
)
:effect
(and
(Flag136-)
(Flag136prime-)
)

)
(:action Flag136Action-47
:parameters ()
:precondition
(and
(Flag83-)
(Flag83prime-)
)
:effect
(and
(Flag136-)
(Flag136prime-)
)

)
(:action Flag136Action-48
:parameters ()
:precondition
(and
(Flag133-)
(Flag133prime-)
)
:effect
(and
(Flag136-)
(Flag136prime-)
)

)
(:action Flag136Action-49
:parameters ()
:precondition
(and
(Flag68-)
(Flag68prime-)
)
:effect
(and
(Flag136-)
(Flag136prime-)
)

)
(:action Flag136Action-50
:parameters ()
:precondition
(and
(Flag134-)
(Flag134prime-)
)
:effect
(and
(Flag136-)
(Flag136prime-)
)

)
(:action Flag136Action-51
:parameters ()
:precondition
(and
(Flag106-)
(Flag106prime-)
)
:effect
(and
(Flag136-)
(Flag136prime-)
)

)
(:action Flag136Action-52
:parameters ()
:precondition
(and
(Flag32-)
(Flag32prime-)
)
:effect
(and
(Flag136-)
(Flag136prime-)
)

)
(:action Flag136Action-53
:parameters ()
:precondition
(and
(Flag69-)
(Flag69prime-)
)
:effect
(and
(Flag136-)
(Flag136prime-)
)

)
(:action Flag136Action-54
:parameters ()
:precondition
(and
(Flag94-)
(Flag94prime-)
)
:effect
(and
(Flag136-)
(Flag136prime-)
)

)
(:action Flag136Action-55
:parameters ()
:precondition
(and
(Flag135-)
(Flag135prime-)
)
:effect
(and
(Flag136-)
(Flag136prime-)
)

)
(:action Flag136Action-56
:parameters ()
:precondition
(and
(Flag39-)
(Flag39prime-)
)
:effect
(and
(Flag136-)
(Flag136prime-)
)

)
(:action Flag136Action-57
:parameters ()
:precondition
(and
(Flag126-)
(Flag126prime-)
)
:effect
(and
(Flag136-)
(Flag136prime-)
)

)
(:action Prim136Action
:parameters ()
:precondition
(and
(not (Flag128-))
(Flag128prime-)
(not (Flag74-))
(Flag74prime-)
(not (Flag71-))
(Flag71prime-)
(not (Flag116-))
(Flag116prime-)
(not (Flag96-))
(Flag96prime-)
(not (Flag114-))
(Flag114prime-)
(not (Flag119-))
(Flag119prime-)
(not (Flag117-))
(Flag117prime-)
(not (Flag129-))
(Flag129prime-)
(not (Flag97-))
(Flag97prime-)
(not (Flag98-))
(Flag98prime-)
(not (Flag109-))
(Flag109prime-)
(not (Flag35-))
(Flag35prime-)
(not (Flag99-))
(Flag99prime-)
(not (Flag50-))
(Flag50prime-)
(not (Flag60-))
(Flag60prime-)
(not (Flag130-))
(Flag130prime-)
(not (Flag86-))
(Flag86prime-)
(not (Flag41-))
(Flag41prime-)
(not (Flag46-))
(Flag46prime-)
(not (Flag111-))
(Flag111prime-)
(not (Flag120-))
(Flag120prime-)
(not (Flag76-))
(Flag76prime-)
(not (Flag88-))
(Flag88prime-)
(not (Flag89-))
(Flag89prime-)
(not (Flag121-))
(Flag121prime-)
(not (Flag90-))
(Flag90prime-)
(not (Flag78-))
(Flag78prime-)
(not (Flag53-))
(Flag53prime-)
(not (Flag112-))
(Flag112prime-)
(not (Flag49-))
(Flag49prime-)
(not (Flag33-))
(Flag33prime-)
(not (Flag63-))
(Flag63prime-)
(not (Flag123-))
(Flag123prime-)
(not (Flag38-))
(Flag38prime-)
(not (Flag65-))
(Flag65prime-)
(not (Flag81-))
(Flag81prime-)
(not (Flag104-))
(Flag104prime-)
(not (Flag113-))
(Flag113prime-)
(not (Flag105-))
(Flag105prime-)
(not (Flag51-))
(Flag51prime-)
(not (Flag52-))
(Flag52prime-)
(not (Flag131-))
(Flag131prime-)
(not (Flag93-))
(Flag93prime-)
(not (Flag82-))
(Flag82prime-)
(not (Flag132-))
(Flag132prime-)
(not (Flag67-))
(Flag67prime-)
(not (Flag83-))
(Flag83prime-)
(not (Flag133-))
(Flag133prime-)
(not (Flag68-))
(Flag68prime-)
(not (Flag134-))
(Flag134prime-)
(not (Flag106-))
(Flag106prime-)
(not (Flag32-))
(Flag32prime-)
(not (Flag69-))
(Flag69prime-)
(not (Flag94-))
(Flag94prime-)
(not (Flag135-))
(Flag135prime-)
(not (Flag39-))
(Flag39prime-)
(not (Flag126-))
(Flag126prime-)
)
:effect
(and
(Flag136prime-)
(not (Flag136-))
)

)
(:action Flag143Action-0
:parameters ()
:precondition
(and
(Flag128-)
(Flag128prime-)
)
:effect
(and
(Flag143-)
(Flag143prime-)
)

)
(:action Flag143Action-1
:parameters ()
:precondition
(and
(Flag74-)
(Flag74prime-)
)
:effect
(and
(Flag143-)
(Flag143prime-)
)

)
(:action Flag143Action-2
:parameters ()
:precondition
(and
(Flag71-)
(Flag71prime-)
)
:effect
(and
(Flag143-)
(Flag143prime-)
)

)
(:action Flag143Action-3
:parameters ()
:precondition
(and
(Flag96-)
(Flag96prime-)
)
:effect
(and
(Flag143-)
(Flag143prime-)
)

)
(:action Flag143Action-4
:parameters ()
:precondition
(and
(Flag114-)
(Flag114prime-)
)
:effect
(and
(Flag143-)
(Flag143prime-)
)

)
(:action Flag143Action-5
:parameters ()
:precondition
(and
(Flag119-)
(Flag119prime-)
)
:effect
(and
(Flag143-)
(Flag143prime-)
)

)
(:action Flag143Action-6
:parameters ()
:precondition
(and
(Flag106-)
(Flag106prime-)
)
:effect
(and
(Flag143-)
(Flag143prime-)
)

)
(:action Flag143Action-7
:parameters ()
:precondition
(and
(Flag129-)
(Flag129prime-)
)
:effect
(and
(Flag143-)
(Flag143prime-)
)

)
(:action Flag143Action-8
:parameters ()
:precondition
(and
(Flag97-)
(Flag97prime-)
)
:effect
(and
(Flag143-)
(Flag143prime-)
)

)
(:action Flag143Action-9
:parameters ()
:precondition
(and
(Flag98-)
(Flag98prime-)
)
:effect
(and
(Flag143-)
(Flag143prime-)
)

)
(:action Flag143Action-10
:parameters ()
:precondition
(and
(Flag109-)
(Flag109prime-)
)
:effect
(and
(Flag143-)
(Flag143prime-)
)

)
(:action Flag143Action-11
:parameters ()
:precondition
(and
(Flag35-)
(Flag35prime-)
)
:effect
(and
(Flag143-)
(Flag143prime-)
)

)
(:action Flag143Action-12
:parameters ()
:precondition
(and
(Flag99-)
(Flag99prime-)
)
:effect
(and
(Flag143-)
(Flag143prime-)
)

)
(:action Flag143Action-13
:parameters ()
:precondition
(and
(Flag137-)
(Flag137prime-)
)
:effect
(and
(Flag143-)
(Flag143prime-)
)

)
(:action Flag143Action-14
:parameters ()
:precondition
(and
(Flag41-)
(Flag41prime-)
)
:effect
(and
(Flag143-)
(Flag143prime-)
)

)
(:action Flag143Action-15
:parameters ()
:precondition
(and
(Flag46-)
(Flag46prime-)
)
:effect
(and
(Flag143-)
(Flag143prime-)
)

)
(:action Flag143Action-16
:parameters ()
:precondition
(and
(Flag50-)
(Flag50prime-)
)
:effect
(and
(Flag143-)
(Flag143prime-)
)

)
(:action Flag143Action-17
:parameters ()
:precondition
(and
(Flag111-)
(Flag111prime-)
)
:effect
(and
(Flag143-)
(Flag143prime-)
)

)
(:action Flag143Action-18
:parameters ()
:precondition
(and
(Flag120-)
(Flag120prime-)
)
:effect
(and
(Flag143-)
(Flag143prime-)
)

)
(:action Flag143Action-19
:parameters ()
:precondition
(and
(Flag88-)
(Flag88prime-)
)
:effect
(and
(Flag143-)
(Flag143prime-)
)

)
(:action Flag143Action-20
:parameters ()
:precondition
(and
(Flag60-)
(Flag60prime-)
)
:effect
(and
(Flag143-)
(Flag143prime-)
)

)
(:action Flag143Action-21
:parameters ()
:precondition
(and
(Flag89-)
(Flag89prime-)
)
:effect
(and
(Flag143-)
(Flag143prime-)
)

)
(:action Flag143Action-22
:parameters ()
:precondition
(and
(Flag121-)
(Flag121prime-)
)
:effect
(and
(Flag143-)
(Flag143prime-)
)

)
(:action Flag143Action-23
:parameters ()
:precondition
(and
(Flag90-)
(Flag90prime-)
)
:effect
(and
(Flag143-)
(Flag143prime-)
)

)
(:action Flag143Action-24
:parameters ()
:precondition
(and
(Flag78-)
(Flag78prime-)
)
:effect
(and
(Flag143-)
(Flag143prime-)
)

)
(:action Flag143Action-25
:parameters ()
:precondition
(and
(Flag53-)
(Flag53prime-)
)
:effect
(and
(Flag143-)
(Flag143prime-)
)

)
(:action Flag143Action-26
:parameters ()
:precondition
(and
(Flag112-)
(Flag112prime-)
)
:effect
(and
(Flag143-)
(Flag143prime-)
)

)
(:action Flag143Action-27
:parameters ()
:precondition
(and
(Flag138-)
(Flag138prime-)
)
:effect
(and
(Flag143-)
(Flag143prime-)
)

)
(:action Flag143Action-28
:parameters ()
:precondition
(and
(Flag49-)
(Flag49prime-)
)
:effect
(and
(Flag143-)
(Flag143prime-)
)

)
(:action Flag143Action-29
:parameters ()
:precondition
(and
(Flag123-)
(Flag123prime-)
)
:effect
(and
(Flag143-)
(Flag143prime-)
)

)
(:action Flag143Action-30
:parameters ()
:precondition
(and
(Flag38-)
(Flag38prime-)
)
:effect
(and
(Flag143-)
(Flag143prime-)
)

)
(:action Flag143Action-31
:parameters ()
:precondition
(and
(Flag139-)
(Flag139prime-)
)
:effect
(and
(Flag143-)
(Flag143prime-)
)

)
(:action Flag143Action-32
:parameters ()
:precondition
(and
(Flag65-)
(Flag65prime-)
)
:effect
(and
(Flag143-)
(Flag143prime-)
)

)
(:action Flag143Action-33
:parameters ()
:precondition
(and
(Flag81-)
(Flag81prime-)
)
:effect
(and
(Flag143-)
(Flag143prime-)
)

)
(:action Flag143Action-34
:parameters ()
:precondition
(and
(Flag113-)
(Flag113prime-)
)
:effect
(and
(Flag143-)
(Flag143prime-)
)

)
(:action Flag143Action-35
:parameters ()
:precondition
(and
(Flag105-)
(Flag105prime-)
)
:effect
(and
(Flag143-)
(Flag143prime-)
)

)
(:action Flag143Action-36
:parameters ()
:precondition
(and
(Flag51-)
(Flag51prime-)
)
:effect
(and
(Flag143-)
(Flag143prime-)
)

)
(:action Flag143Action-37
:parameters ()
:precondition
(and
(Flag140-)
(Flag140prime-)
)
:effect
(and
(Flag143-)
(Flag143prime-)
)

)
(:action Flag143Action-38
:parameters ()
:precondition
(and
(Flag131-)
(Flag131prime-)
)
:effect
(and
(Flag143-)
(Flag143prime-)
)

)
(:action Flag143Action-39
:parameters ()
:precondition
(and
(Flag141-)
(Flag141prime-)
)
:effect
(and
(Flag143-)
(Flag143prime-)
)

)
(:action Flag143Action-40
:parameters ()
:precondition
(and
(Flag93-)
(Flag93prime-)
)
:effect
(and
(Flag143-)
(Flag143prime-)
)

)
(:action Flag143Action-41
:parameters ()
:precondition
(and
(Flag82-)
(Flag82prime-)
)
:effect
(and
(Flag143-)
(Flag143prime-)
)

)
(:action Flag143Action-42
:parameters ()
:precondition
(and
(Flag67-)
(Flag67prime-)
)
:effect
(and
(Flag143-)
(Flag143prime-)
)

)
(:action Flag143Action-43
:parameters ()
:precondition
(and
(Flag83-)
(Flag83prime-)
)
:effect
(and
(Flag143-)
(Flag143prime-)
)

)
(:action Flag143Action-44
:parameters ()
:precondition
(and
(Flag133-)
(Flag133prime-)
)
:effect
(and
(Flag143-)
(Flag143prime-)
)

)
(:action Flag143Action-45
:parameters ()
:precondition
(and
(Flag68-)
(Flag68prime-)
)
:effect
(and
(Flag143-)
(Flag143prime-)
)

)
(:action Flag143Action-46
:parameters ()
:precondition
(and
(Flag134-)
(Flag134prime-)
)
:effect
(and
(Flag143-)
(Flag143prime-)
)

)
(:action Flag143Action-47
:parameters ()
:precondition
(and
(Flag117-)
(Flag117prime-)
)
:effect
(and
(Flag143-)
(Flag143prime-)
)

)
(:action Flag143Action-48
:parameters ()
:precondition
(and
(Flag32-)
(Flag32prime-)
)
:effect
(and
(Flag143-)
(Flag143prime-)
)

)
(:action Flag143Action-49
:parameters ()
:precondition
(and
(Flag69-)
(Flag69prime-)
)
:effect
(and
(Flag143-)
(Flag143prime-)
)

)
(:action Flag143Action-50
:parameters ()
:precondition
(and
(Flag94-)
(Flag94prime-)
)
:effect
(and
(Flag143-)
(Flag143prime-)
)

)
(:action Flag143Action-51
:parameters ()
:precondition
(and
(Flag142-)
(Flag142prime-)
)
:effect
(and
(Flag143-)
(Flag143prime-)
)

)
(:action Flag143Action-52
:parameters ()
:precondition
(and
(Flag33-)
(Flag33prime-)
)
:effect
(and
(Flag143-)
(Flag143prime-)
)

)
(:action Flag143Action-53
:parameters ()
:precondition
(and
(Flag126-)
(Flag126prime-)
)
:effect
(and
(Flag143-)
(Flag143prime-)
)

)
(:action Prim143Action
:parameters ()
:precondition
(and
(not (Flag128-))
(Flag128prime-)
(not (Flag74-))
(Flag74prime-)
(not (Flag71-))
(Flag71prime-)
(not (Flag96-))
(Flag96prime-)
(not (Flag114-))
(Flag114prime-)
(not (Flag119-))
(Flag119prime-)
(not (Flag106-))
(Flag106prime-)
(not (Flag129-))
(Flag129prime-)
(not (Flag97-))
(Flag97prime-)
(not (Flag98-))
(Flag98prime-)
(not (Flag109-))
(Flag109prime-)
(not (Flag35-))
(Flag35prime-)
(not (Flag99-))
(Flag99prime-)
(not (Flag137-))
(Flag137prime-)
(not (Flag41-))
(Flag41prime-)
(not (Flag46-))
(Flag46prime-)
(not (Flag50-))
(Flag50prime-)
(not (Flag111-))
(Flag111prime-)
(not (Flag120-))
(Flag120prime-)
(not (Flag88-))
(Flag88prime-)
(not (Flag60-))
(Flag60prime-)
(not (Flag89-))
(Flag89prime-)
(not (Flag121-))
(Flag121prime-)
(not (Flag90-))
(Flag90prime-)
(not (Flag78-))
(Flag78prime-)
(not (Flag53-))
(Flag53prime-)
(not (Flag112-))
(Flag112prime-)
(not (Flag138-))
(Flag138prime-)
(not (Flag49-))
(Flag49prime-)
(not (Flag123-))
(Flag123prime-)
(not (Flag38-))
(Flag38prime-)
(not (Flag139-))
(Flag139prime-)
(not (Flag65-))
(Flag65prime-)
(not (Flag81-))
(Flag81prime-)
(not (Flag113-))
(Flag113prime-)
(not (Flag105-))
(Flag105prime-)
(not (Flag51-))
(Flag51prime-)
(not (Flag140-))
(Flag140prime-)
(not (Flag131-))
(Flag131prime-)
(not (Flag141-))
(Flag141prime-)
(not (Flag93-))
(Flag93prime-)
(not (Flag82-))
(Flag82prime-)
(not (Flag67-))
(Flag67prime-)
(not (Flag83-))
(Flag83prime-)
(not (Flag133-))
(Flag133prime-)
(not (Flag68-))
(Flag68prime-)
(not (Flag134-))
(Flag134prime-)
(not (Flag117-))
(Flag117prime-)
(not (Flag32-))
(Flag32prime-)
(not (Flag69-))
(Flag69prime-)
(not (Flag94-))
(Flag94prime-)
(not (Flag142-))
(Flag142prime-)
(not (Flag33-))
(Flag33prime-)
(not (Flag126-))
(Flag126prime-)
)
:effect
(and
(Flag143prime-)
(not (Flag143-))
)

)
(:action Flag148Action-0
:parameters ()
:precondition
(and
(Flag128-)
(Flag128prime-)
)
:effect
(and
(Flag148-)
(Flag148prime-)
)

)
(:action Flag148Action-1
:parameters ()
:precondition
(and
(Flag71-)
(Flag71prime-)
)
:effect
(and
(Flag148-)
(Flag148prime-)
)

)
(:action Flag148Action-2
:parameters ()
:precondition
(and
(Flag97-)
(Flag97prime-)
)
:effect
(and
(Flag148-)
(Flag148prime-)
)

)
(:action Flag148Action-3
:parameters ()
:precondition
(and
(Flag96-)
(Flag96prime-)
)
:effect
(and
(Flag148-)
(Flag148prime-)
)

)
(:action Flag148Action-4
:parameters ()
:precondition
(and
(Flag114-)
(Flag114prime-)
)
:effect
(and
(Flag148-)
(Flag148prime-)
)

)
(:action Flag148Action-5
:parameters ()
:precondition
(and
(Flag119-)
(Flag119prime-)
)
:effect
(and
(Flag148-)
(Flag148prime-)
)

)
(:action Flag148Action-6
:parameters ()
:precondition
(and
(Flag129-)
(Flag129prime-)
)
:effect
(and
(Flag148-)
(Flag148prime-)
)

)
(:action Flag148Action-7
:parameters ()
:precondition
(and
(Flag144-)
(Flag144prime-)
)
:effect
(and
(Flag148-)
(Flag148prime-)
)

)
(:action Flag148Action-8
:parameters ()
:precondition
(and
(Flag98-)
(Flag98prime-)
)
:effect
(and
(Flag148-)
(Flag148prime-)
)

)
(:action Flag148Action-9
:parameters ()
:precondition
(and
(Flag35-)
(Flag35prime-)
)
:effect
(and
(Flag148-)
(Flag148prime-)
)

)
(:action Flag148Action-10
:parameters ()
:precondition
(and
(Flag99-)
(Flag99prime-)
)
:effect
(and
(Flag148-)
(Flag148prime-)
)

)
(:action Flag148Action-11
:parameters ()
:precondition
(and
(Flag145-)
(Flag145prime-)
)
:effect
(and
(Flag148-)
(Flag148prime-)
)

)
(:action Flag148Action-12
:parameters ()
:precondition
(and
(Flag50-)
(Flag50prime-)
)
:effect
(and
(Flag148-)
(Flag148prime-)
)

)
(:action Flag148Action-13
:parameters ()
:precondition
(and
(Flag46-)
(Flag46prime-)
)
:effect
(and
(Flag148-)
(Flag148prime-)
)

)
(:action Flag148Action-14
:parameters ()
:precondition
(and
(Flag146-)
(Flag146prime-)
)
:effect
(and
(Flag148-)
(Flag148prime-)
)

)
(:action Flag148Action-15
:parameters ()
:precondition
(and
(Flag111-)
(Flag111prime-)
)
:effect
(and
(Flag148-)
(Flag148prime-)
)

)
(:action Flag148Action-16
:parameters ()
:precondition
(and
(Flag120-)
(Flag120prime-)
)
:effect
(and
(Flag148-)
(Flag148prime-)
)

)
(:action Flag148Action-17
:parameters ()
:precondition
(and
(Flag88-)
(Flag88prime-)
)
:effect
(and
(Flag148-)
(Flag148prime-)
)

)
(:action Flag148Action-18
:parameters ()
:precondition
(and
(Flag89-)
(Flag89prime-)
)
:effect
(and
(Flag148-)
(Flag148prime-)
)

)
(:action Flag148Action-19
:parameters ()
:precondition
(and
(Flag121-)
(Flag121prime-)
)
:effect
(and
(Flag148-)
(Flag148prime-)
)

)
(:action Flag148Action-20
:parameters ()
:precondition
(and
(Flag90-)
(Flag90prime-)
)
:effect
(and
(Flag148-)
(Flag148prime-)
)

)
(:action Flag148Action-21
:parameters ()
:precondition
(and
(Flag78-)
(Flag78prime-)
)
:effect
(and
(Flag148-)
(Flag148prime-)
)

)
(:action Flag148Action-22
:parameters ()
:precondition
(and
(Flag112-)
(Flag112prime-)
)
:effect
(and
(Flag148-)
(Flag148prime-)
)

)
(:action Flag148Action-23
:parameters ()
:precondition
(and
(Flag138-)
(Flag138prime-)
)
:effect
(and
(Flag148-)
(Flag148prime-)
)

)
(:action Flag148Action-24
:parameters ()
:precondition
(and
(Flag53-)
(Flag53prime-)
)
:effect
(and
(Flag148-)
(Flag148prime-)
)

)
(:action Flag148Action-25
:parameters ()
:precondition
(and
(Flag38-)
(Flag38prime-)
)
:effect
(and
(Flag148-)
(Flag148prime-)
)

)
(:action Flag148Action-26
:parameters ()
:precondition
(and
(Flag139-)
(Flag139prime-)
)
:effect
(and
(Flag148-)
(Flag148prime-)
)

)
(:action Flag148Action-27
:parameters ()
:precondition
(and
(Flag65-)
(Flag65prime-)
)
:effect
(and
(Flag148-)
(Flag148prime-)
)

)
(:action Flag148Action-28
:parameters ()
:precondition
(and
(Flag81-)
(Flag81prime-)
)
:effect
(and
(Flag148-)
(Flag148prime-)
)

)
(:action Flag148Action-29
:parameters ()
:precondition
(and
(Flag113-)
(Flag113prime-)
)
:effect
(and
(Flag148-)
(Flag148prime-)
)

)
(:action Flag148Action-30
:parameters ()
:precondition
(and
(Flag51-)
(Flag51prime-)
)
:effect
(and
(Flag148-)
(Flag148prime-)
)

)
(:action Flag148Action-31
:parameters ()
:precondition
(and
(Flag131-)
(Flag131prime-)
)
:effect
(and
(Flag148-)
(Flag148prime-)
)

)
(:action Flag148Action-32
:parameters ()
:precondition
(and
(Flag141-)
(Flag141prime-)
)
:effect
(and
(Flag148-)
(Flag148prime-)
)

)
(:action Flag148Action-33
:parameters ()
:precondition
(and
(Flag82-)
(Flag82prime-)
)
:effect
(and
(Flag148-)
(Flag148prime-)
)

)
(:action Flag148Action-34
:parameters ()
:precondition
(and
(Flag67-)
(Flag67prime-)
)
:effect
(and
(Flag148-)
(Flag148prime-)
)

)
(:action Flag148Action-35
:parameters ()
:precondition
(and
(Flag83-)
(Flag83prime-)
)
:effect
(and
(Flag148-)
(Flag148prime-)
)

)
(:action Flag148Action-36
:parameters ()
:precondition
(and
(Flag147-)
(Flag147prime-)
)
:effect
(and
(Flag148-)
(Flag148prime-)
)

)
(:action Flag148Action-37
:parameters ()
:precondition
(and
(Flag133-)
(Flag133prime-)
)
:effect
(and
(Flag148-)
(Flag148prime-)
)

)
(:action Flag148Action-38
:parameters ()
:precondition
(and
(Flag68-)
(Flag68prime-)
)
:effect
(and
(Flag148-)
(Flag148prime-)
)

)
(:action Flag148Action-39
:parameters ()
:precondition
(and
(Flag134-)
(Flag134prime-)
)
:effect
(and
(Flag148-)
(Flag148prime-)
)

)
(:action Flag148Action-40
:parameters ()
:precondition
(and
(Flag117-)
(Flag117prime-)
)
:effect
(and
(Flag148-)
(Flag148prime-)
)

)
(:action Flag148Action-41
:parameters ()
:precondition
(and
(Flag32-)
(Flag32prime-)
)
:effect
(and
(Flag148-)
(Flag148prime-)
)

)
(:action Flag148Action-42
:parameters ()
:precondition
(and
(Flag69-)
(Flag69prime-)
)
:effect
(and
(Flag148-)
(Flag148prime-)
)

)
(:action Flag148Action-43
:parameters ()
:precondition
(and
(Flag94-)
(Flag94prime-)
)
:effect
(and
(Flag148-)
(Flag148prime-)
)

)
(:action Flag148Action-44
:parameters ()
:precondition
(and
(Flag142-)
(Flag142prime-)
)
:effect
(and
(Flag148-)
(Flag148prime-)
)

)
(:action Flag148Action-45
:parameters ()
:precondition
(and
(Flag33-)
(Flag33prime-)
)
:effect
(and
(Flag148-)
(Flag148prime-)
)

)
(:action Flag148Action-46
:parameters ()
:precondition
(and
(Flag126-)
(Flag126prime-)
)
:effect
(and
(Flag148-)
(Flag148prime-)
)

)
(:action Prim148Action
:parameters ()
:precondition
(and
(not (Flag128-))
(Flag128prime-)
(not (Flag71-))
(Flag71prime-)
(not (Flag97-))
(Flag97prime-)
(not (Flag96-))
(Flag96prime-)
(not (Flag114-))
(Flag114prime-)
(not (Flag119-))
(Flag119prime-)
(not (Flag129-))
(Flag129prime-)
(not (Flag144-))
(Flag144prime-)
(not (Flag98-))
(Flag98prime-)
(not (Flag35-))
(Flag35prime-)
(not (Flag99-))
(Flag99prime-)
(not (Flag145-))
(Flag145prime-)
(not (Flag50-))
(Flag50prime-)
(not (Flag46-))
(Flag46prime-)
(not (Flag146-))
(Flag146prime-)
(not (Flag111-))
(Flag111prime-)
(not (Flag120-))
(Flag120prime-)
(not (Flag88-))
(Flag88prime-)
(not (Flag89-))
(Flag89prime-)
(not (Flag121-))
(Flag121prime-)
(not (Flag90-))
(Flag90prime-)
(not (Flag78-))
(Flag78prime-)
(not (Flag112-))
(Flag112prime-)
(not (Flag138-))
(Flag138prime-)
(not (Flag53-))
(Flag53prime-)
(not (Flag38-))
(Flag38prime-)
(not (Flag139-))
(Flag139prime-)
(not (Flag65-))
(Flag65prime-)
(not (Flag81-))
(Flag81prime-)
(not (Flag113-))
(Flag113prime-)
(not (Flag51-))
(Flag51prime-)
(not (Flag131-))
(Flag131prime-)
(not (Flag141-))
(Flag141prime-)
(not (Flag82-))
(Flag82prime-)
(not (Flag67-))
(Flag67prime-)
(not (Flag83-))
(Flag83prime-)
(not (Flag147-))
(Flag147prime-)
(not (Flag133-))
(Flag133prime-)
(not (Flag68-))
(Flag68prime-)
(not (Flag134-))
(Flag134prime-)
(not (Flag117-))
(Flag117prime-)
(not (Flag32-))
(Flag32prime-)
(not (Flag69-))
(Flag69prime-)
(not (Flag94-))
(Flag94prime-)
(not (Flag142-))
(Flag142prime-)
(not (Flag33-))
(Flag33prime-)
(not (Flag126-))
(Flag126prime-)
)
:effect
(and
(Flag148prime-)
(not (Flag148-))
)

)
(:action Flag152Action-0
:parameters ()
:precondition
(and
(Flag128-)
(Flag128prime-)
)
:effect
(and
(Flag152-)
(Flag152prime-)
)

)
(:action Flag152Action-1
:parameters ()
:precondition
(and
(Flag71-)
(Flag71prime-)
)
:effect
(and
(Flag152-)
(Flag152prime-)
)

)
(:action Flag152Action-2
:parameters ()
:precondition
(and
(Flag96-)
(Flag96prime-)
)
:effect
(and
(Flag152-)
(Flag152prime-)
)

)
(:action Flag152Action-3
:parameters ()
:precondition
(and
(Flag114-)
(Flag114prime-)
)
:effect
(and
(Flag152-)
(Flag152prime-)
)

)
(:action Flag152Action-4
:parameters ()
:precondition
(and
(Flag119-)
(Flag119prime-)
)
:effect
(and
(Flag152-)
(Flag152prime-)
)

)
(:action Flag152Action-5
:parameters ()
:precondition
(and
(Flag97-)
(Flag97prime-)
)
:effect
(and
(Flag152-)
(Flag152prime-)
)

)
(:action Flag152Action-6
:parameters ()
:precondition
(and
(Flag98-)
(Flag98prime-)
)
:effect
(and
(Flag152-)
(Flag152prime-)
)

)
(:action Flag152Action-7
:parameters ()
:precondition
(and
(Flag35-)
(Flag35prime-)
)
:effect
(and
(Flag152-)
(Flag152prime-)
)

)
(:action Flag152Action-8
:parameters ()
:precondition
(and
(Flag99-)
(Flag99prime-)
)
:effect
(and
(Flag152-)
(Flag152prime-)
)

)
(:action Flag152Action-9
:parameters ()
:precondition
(and
(Flag145-)
(Flag145prime-)
)
:effect
(and
(Flag152-)
(Flag152prime-)
)

)
(:action Flag152Action-10
:parameters ()
:precondition
(and
(Flag149-)
(Flag149prime-)
)
:effect
(and
(Flag152-)
(Flag152prime-)
)

)
(:action Flag152Action-11
:parameters ()
:precondition
(and
(Flag46-)
(Flag46prime-)
)
:effect
(and
(Flag152-)
(Flag152prime-)
)

)
(:action Flag152Action-12
:parameters ()
:precondition
(and
(Flag146-)
(Flag146prime-)
)
:effect
(and
(Flag152-)
(Flag152prime-)
)

)
(:action Flag152Action-13
:parameters ()
:precondition
(and
(Flag88-)
(Flag88prime-)
)
:effect
(and
(Flag152-)
(Flag152prime-)
)

)
(:action Flag152Action-14
:parameters ()
:precondition
(and
(Flag121-)
(Flag121prime-)
)
:effect
(and
(Flag152-)
(Flag152prime-)
)

)
(:action Flag152Action-15
:parameters ()
:precondition
(and
(Flag90-)
(Flag90prime-)
)
:effect
(and
(Flag152-)
(Flag152prime-)
)

)
(:action Flag152Action-16
:parameters ()
:precondition
(and
(Flag78-)
(Flag78prime-)
)
:effect
(and
(Flag152-)
(Flag152prime-)
)

)
(:action Flag152Action-17
:parameters ()
:precondition
(and
(Flag150-)
(Flag150prime-)
)
:effect
(and
(Flag152-)
(Flag152prime-)
)

)
(:action Flag152Action-18
:parameters ()
:precondition
(and
(Flag151-)
(Flag151prime-)
)
:effect
(and
(Flag152-)
(Flag152prime-)
)

)
(:action Flag152Action-19
:parameters ()
:precondition
(and
(Flag53-)
(Flag53prime-)
)
:effect
(and
(Flag152-)
(Flag152prime-)
)

)
(:action Flag152Action-20
:parameters ()
:precondition
(and
(Flag139-)
(Flag139prime-)
)
:effect
(and
(Flag152-)
(Flag152prime-)
)

)
(:action Flag152Action-21
:parameters ()
:precondition
(and
(Flag50-)
(Flag50prime-)
)
:effect
(and
(Flag152-)
(Flag152prime-)
)

)
(:action Flag152Action-22
:parameters ()
:precondition
(and
(Flag113-)
(Flag113prime-)
)
:effect
(and
(Flag152-)
(Flag152prime-)
)

)
(:action Flag152Action-23
:parameters ()
:precondition
(and
(Flag131-)
(Flag131prime-)
)
:effect
(and
(Flag152-)
(Flag152prime-)
)

)
(:action Flag152Action-24
:parameters ()
:precondition
(and
(Flag141-)
(Flag141prime-)
)
:effect
(and
(Flag152-)
(Flag152prime-)
)

)
(:action Flag152Action-25
:parameters ()
:precondition
(and
(Flag82-)
(Flag82prime-)
)
:effect
(and
(Flag152-)
(Flag152prime-)
)

)
(:action Flag152Action-26
:parameters ()
:precondition
(and
(Flag67-)
(Flag67prime-)
)
:effect
(and
(Flag152-)
(Flag152prime-)
)

)
(:action Flag152Action-27
:parameters ()
:precondition
(and
(Flag83-)
(Flag83prime-)
)
:effect
(and
(Flag152-)
(Flag152prime-)
)

)
(:action Flag152Action-28
:parameters ()
:precondition
(and
(Flag147-)
(Flag147prime-)
)
:effect
(and
(Flag152-)
(Flag152prime-)
)

)
(:action Flag152Action-29
:parameters ()
:precondition
(and
(Flag133-)
(Flag133prime-)
)
:effect
(and
(Flag152-)
(Flag152prime-)
)

)
(:action Flag152Action-30
:parameters ()
:precondition
(and
(Flag68-)
(Flag68prime-)
)
:effect
(and
(Flag152-)
(Flag152prime-)
)

)
(:action Flag152Action-31
:parameters ()
:precondition
(and
(Flag134-)
(Flag134prime-)
)
:effect
(and
(Flag152-)
(Flag152prime-)
)

)
(:action Flag152Action-32
:parameters ()
:precondition
(and
(Flag117-)
(Flag117prime-)
)
:effect
(and
(Flag152-)
(Flag152prime-)
)

)
(:action Flag152Action-33
:parameters ()
:precondition
(and
(Flag32-)
(Flag32prime-)
)
:effect
(and
(Flag152-)
(Flag152prime-)
)

)
(:action Flag152Action-34
:parameters ()
:precondition
(and
(Flag94-)
(Flag94prime-)
)
:effect
(and
(Flag152-)
(Flag152prime-)
)

)
(:action Flag152Action-35
:parameters ()
:precondition
(and
(Flag142-)
(Flag142prime-)
)
:effect
(and
(Flag152-)
(Flag152prime-)
)

)
(:action Flag152Action-36
:parameters ()
:precondition
(and
(Flag33-)
(Flag33prime-)
)
:effect
(and
(Flag152-)
(Flag152prime-)
)

)
(:action Flag152Action-37
:parameters ()
:precondition
(and
(Flag126-)
(Flag126prime-)
)
:effect
(and
(Flag152-)
(Flag152prime-)
)

)
(:action Prim152Action
:parameters ()
:precondition
(and
(not (Flag128-))
(Flag128prime-)
(not (Flag71-))
(Flag71prime-)
(not (Flag96-))
(Flag96prime-)
(not (Flag114-))
(Flag114prime-)
(not (Flag119-))
(Flag119prime-)
(not (Flag97-))
(Flag97prime-)
(not (Flag98-))
(Flag98prime-)
(not (Flag35-))
(Flag35prime-)
(not (Flag99-))
(Flag99prime-)
(not (Flag145-))
(Flag145prime-)
(not (Flag149-))
(Flag149prime-)
(not (Flag46-))
(Flag46prime-)
(not (Flag146-))
(Flag146prime-)
(not (Flag88-))
(Flag88prime-)
(not (Flag121-))
(Flag121prime-)
(not (Flag90-))
(Flag90prime-)
(not (Flag78-))
(Flag78prime-)
(not (Flag150-))
(Flag150prime-)
(not (Flag151-))
(Flag151prime-)
(not (Flag53-))
(Flag53prime-)
(not (Flag139-))
(Flag139prime-)
(not (Flag50-))
(Flag50prime-)
(not (Flag113-))
(Flag113prime-)
(not (Flag131-))
(Flag131prime-)
(not (Flag141-))
(Flag141prime-)
(not (Flag82-))
(Flag82prime-)
(not (Flag67-))
(Flag67prime-)
(not (Flag83-))
(Flag83prime-)
(not (Flag147-))
(Flag147prime-)
(not (Flag133-))
(Flag133prime-)
(not (Flag68-))
(Flag68prime-)
(not (Flag134-))
(Flag134prime-)
(not (Flag117-))
(Flag117prime-)
(not (Flag32-))
(Flag32prime-)
(not (Flag94-))
(Flag94prime-)
(not (Flag142-))
(Flag142prime-)
(not (Flag33-))
(Flag33prime-)
(not (Flag126-))
(Flag126prime-)
)
:effect
(and
(Flag152prime-)
(not (Flag152-))
)

)
(:action Flag156Action-0
:parameters ()
:precondition
(and
(Flag128-)
(Flag128prime-)
)
:effect
(and
(Flag156-)
(Flag156prime-)
)

)
(:action Flag156Action-1
:parameters ()
:precondition
(and
(Flag68-)
(Flag68prime-)
)
:effect
(and
(Flag156-)
(Flag156prime-)
)

)
(:action Flag156Action-2
:parameters ()
:precondition
(and
(Flag71-)
(Flag71prime-)
)
:effect
(and
(Flag156-)
(Flag156prime-)
)

)
(:action Flag156Action-3
:parameters ()
:precondition
(and
(Flag114-)
(Flag114prime-)
)
:effect
(and
(Flag156-)
(Flag156prime-)
)

)
(:action Flag156Action-4
:parameters ()
:precondition
(and
(Flag119-)
(Flag119prime-)
)
:effect
(and
(Flag156-)
(Flag156prime-)
)

)
(:action Flag156Action-5
:parameters ()
:precondition
(and
(Flag153-)
(Flag153prime-)
)
:effect
(and
(Flag156-)
(Flag156prime-)
)

)
(:action Flag156Action-6
:parameters ()
:precondition
(and
(Flag97-)
(Flag97prime-)
)
:effect
(and
(Flag156-)
(Flag156prime-)
)

)
(:action Flag156Action-7
:parameters ()
:precondition
(and
(Flag98-)
(Flag98prime-)
)
:effect
(and
(Flag156-)
(Flag156prime-)
)

)
(:action Flag156Action-8
:parameters ()
:precondition
(and
(Flag99-)
(Flag99prime-)
)
:effect
(and
(Flag156-)
(Flag156prime-)
)

)
(:action Flag156Action-9
:parameters ()
:precondition
(and
(Flag46-)
(Flag46prime-)
)
:effect
(and
(Flag156-)
(Flag156prime-)
)

)
(:action Flag156Action-10
:parameters ()
:precondition
(and
(Flag146-)
(Flag146prime-)
)
:effect
(and
(Flag156-)
(Flag156prime-)
)

)
(:action Flag156Action-11
:parameters ()
:precondition
(and
(Flag88-)
(Flag88prime-)
)
:effect
(and
(Flag156-)
(Flag156prime-)
)

)
(:action Flag156Action-12
:parameters ()
:precondition
(and
(Flag78-)
(Flag78prime-)
)
:effect
(and
(Flag156-)
(Flag156prime-)
)

)
(:action Flag156Action-13
:parameters ()
:precondition
(and
(Flag150-)
(Flag150prime-)
)
:effect
(and
(Flag156-)
(Flag156prime-)
)

)
(:action Flag156Action-14
:parameters ()
:precondition
(and
(Flag154-)
(Flag154prime-)
)
:effect
(and
(Flag156-)
(Flag156prime-)
)

)
(:action Flag156Action-15
:parameters ()
:precondition
(and
(Flag139-)
(Flag139prime-)
)
:effect
(and
(Flag156-)
(Flag156prime-)
)

)
(:action Flag156Action-16
:parameters ()
:precondition
(and
(Flag113-)
(Flag113prime-)
)
:effect
(and
(Flag156-)
(Flag156prime-)
)

)
(:action Flag156Action-17
:parameters ()
:precondition
(and
(Flag131-)
(Flag131prime-)
)
:effect
(and
(Flag156-)
(Flag156prime-)
)

)
(:action Flag156Action-18
:parameters ()
:precondition
(and
(Flag53-)
(Flag53prime-)
)
:effect
(and
(Flag156-)
(Flag156prime-)
)

)
(:action Flag156Action-19
:parameters ()
:precondition
(and
(Flag67-)
(Flag67prime-)
)
:effect
(and
(Flag156-)
(Flag156prime-)
)

)
(:action Flag156Action-20
:parameters ()
:precondition
(and
(Flag32-)
(Flag32prime-)
)
:effect
(and
(Flag156-)
(Flag156prime-)
)

)
(:action Flag156Action-21
:parameters ()
:precondition
(and
(Flag147-)
(Flag147prime-)
)
:effect
(and
(Flag156-)
(Flag156prime-)
)

)
(:action Flag156Action-22
:parameters ()
:precondition
(and
(Flag133-)
(Flag133prime-)
)
:effect
(and
(Flag156-)
(Flag156prime-)
)

)
(:action Flag156Action-23
:parameters ()
:precondition
(and
(Flag155-)
(Flag155prime-)
)
:effect
(and
(Flag156-)
(Flag156prime-)
)

)
(:action Flag156Action-24
:parameters ()
:precondition
(and
(Flag94-)
(Flag94prime-)
)
:effect
(and
(Flag156-)
(Flag156prime-)
)

)
(:action Flag156Action-25
:parameters ()
:precondition
(and
(Flag142-)
(Flag142prime-)
)
:effect
(and
(Flag156-)
(Flag156prime-)
)

)
(:action Flag156Action-26
:parameters ()
:precondition
(and
(Flag33-)
(Flag33prime-)
)
:effect
(and
(Flag156-)
(Flag156prime-)
)

)
(:action Prim156Action
:parameters ()
:precondition
(and
(not (Flag128-))
(Flag128prime-)
(not (Flag68-))
(Flag68prime-)
(not (Flag71-))
(Flag71prime-)
(not (Flag114-))
(Flag114prime-)
(not (Flag119-))
(Flag119prime-)
(not (Flag153-))
(Flag153prime-)
(not (Flag97-))
(Flag97prime-)
(not (Flag98-))
(Flag98prime-)
(not (Flag99-))
(Flag99prime-)
(not (Flag46-))
(Flag46prime-)
(not (Flag146-))
(Flag146prime-)
(not (Flag88-))
(Flag88prime-)
(not (Flag78-))
(Flag78prime-)
(not (Flag150-))
(Flag150prime-)
(not (Flag154-))
(Flag154prime-)
(not (Flag139-))
(Flag139prime-)
(not (Flag113-))
(Flag113prime-)
(not (Flag131-))
(Flag131prime-)
(not (Flag53-))
(Flag53prime-)
(not (Flag67-))
(Flag67prime-)
(not (Flag32-))
(Flag32prime-)
(not (Flag147-))
(Flag147prime-)
(not (Flag133-))
(Flag133prime-)
(not (Flag155-))
(Flag155prime-)
(not (Flag94-))
(Flag94prime-)
(not (Flag142-))
(Flag142prime-)
(not (Flag33-))
(Flag33prime-)
)
:effect
(and
(Flag156prime-)
(not (Flag156-))
)

)
(:action Flag160Action-0
:parameters ()
:precondition
(and
(Flag46-)
(Flag46prime-)
)
:effect
(and
(Flag160-)
(Flag160prime-)
)

)
(:action Flag160Action-1
:parameters ()
:precondition
(and
(Flag146-)
(Flag146prime-)
)
:effect
(and
(Flag160-)
(Flag160prime-)
)

)
(:action Flag160Action-2
:parameters ()
:precondition
(and
(Flag71-)
(Flag71prime-)
)
:effect
(and
(Flag160-)
(Flag160prime-)
)

)
(:action Flag160Action-3
:parameters ()
:precondition
(and
(Flag68-)
(Flag68prime-)
)
:effect
(and
(Flag160-)
(Flag160prime-)
)

)
(:action Flag160Action-4
:parameters ()
:precondition
(and
(Flag99-)
(Flag99prime-)
)
:effect
(and
(Flag160-)
(Flag160prime-)
)

)
(:action Flag160Action-5
:parameters ()
:precondition
(and
(Flag158-)
(Flag158prime-)
)
:effect
(and
(Flag160-)
(Flag160prime-)
)

)
(:action Flag160Action-6
:parameters ()
:precondition
(and
(Flag114-)
(Flag114prime-)
)
:effect
(and
(Flag160-)
(Flag160prime-)
)

)
(:action Flag160Action-7
:parameters ()
:precondition
(and
(Flag119-)
(Flag119prime-)
)
:effect
(and
(Flag160-)
(Flag160prime-)
)

)
(:action Flag160Action-8
:parameters ()
:precondition
(and
(Flag159-)
(Flag159prime-)
)
:effect
(and
(Flag160-)
(Flag160prime-)
)

)
(:action Flag160Action-9
:parameters ()
:precondition
(and
(Flag142-)
(Flag142prime-)
)
:effect
(and
(Flag160-)
(Flag160prime-)
)

)
(:action Flag160Action-10
:parameters ()
:precondition
(and
(Flag33-)
(Flag33prime-)
)
:effect
(and
(Flag160-)
(Flag160prime-)
)

)
(:action Flag160Action-11
:parameters ()
:precondition
(and
(Flag98-)
(Flag98prime-)
)
:effect
(and
(Flag160-)
(Flag160prime-)
)

)
(:action Flag160Action-12
:parameters ()
:precondition
(and
(Flag150-)
(Flag150prime-)
)
:effect
(and
(Flag160-)
(Flag160prime-)
)

)
(:action Flag160Action-13
:parameters ()
:precondition
(and
(Flag131-)
(Flag131prime-)
)
:effect
(and
(Flag160-)
(Flag160prime-)
)

)
(:action Prim160Action
:parameters ()
:precondition
(and
(not (Flag46-))
(Flag46prime-)
(not (Flag146-))
(Flag146prime-)
(not (Flag71-))
(Flag71prime-)
(not (Flag68-))
(Flag68prime-)
(not (Flag99-))
(Flag99prime-)
(not (Flag158-))
(Flag158prime-)
(not (Flag114-))
(Flag114prime-)
(not (Flag119-))
(Flag119prime-)
(not (Flag159-))
(Flag159prime-)
(not (Flag142-))
(Flag142prime-)
(not (Flag33-))
(Flag33prime-)
(not (Flag98-))
(Flag98prime-)
(not (Flag150-))
(Flag150prime-)
(not (Flag131-))
(Flag131prime-)
)
:effect
(and
(Flag160prime-)
(not (Flag160-))
)

)
(:action Flag161Action-0
:parameters ()
:precondition
(and
(RIGHTOF3-ROBOT)
)
:effect
(and
(Flag161-)
(Flag161prime-)
)

)
(:action Flag161Action-1
:parameters ()
:precondition
(and
(RIGHTOF8-ROBOT)
)
:effect
(and
(Flag161-)
(Flag161prime-)
)

)
(:action Flag161Action-2
:parameters ()
:precondition
(and
(RIGHTOF10-ROBOT)
)
:effect
(and
(Flag161-)
(Flag161prime-)
)

)
(:action Flag161Action-3
:parameters ()
:precondition
(and
(RIGHTOF5-ROBOT)
)
:effect
(and
(Flag161-)
(Flag161prime-)
)

)
(:action Flag161Action-5
:parameters ()
:precondition
(and
(RIGHTOF13-ROBOT)
)
:effect
(and
(Flag161-)
(Flag161prime-)
)

)
(:action Flag161Action-6
:parameters ()
:precondition
(and
(RIGHTOF11-ROBOT)
)
:effect
(and
(Flag161-)
(Flag161prime-)
)

)
(:action Flag161Action-7
:parameters ()
:precondition
(and
(RIGHTOF0-ROBOT)
)
:effect
(and
(Flag161-)
(Flag161prime-)
)

)
(:action Flag161Action-8
:parameters ()
:precondition
(and
(RIGHTOF4-ROBOT)
)
:effect
(and
(Flag161-)
(Flag161prime-)
)

)
(:action Flag161Action-9
:parameters ()
:precondition
(and
(RIGHTOF1-ROBOT)
)
:effect
(and
(Flag161-)
(Flag161prime-)
)

)
(:action Flag161Action-10
:parameters ()
:precondition
(and
(RIGHTOF12-ROBOT)
)
:effect
(and
(Flag161-)
(Flag161prime-)
)

)
(:action Flag161Action-11
:parameters ()
:precondition
(and
(RIGHTOF7-ROBOT)
)
:effect
(and
(Flag161-)
(Flag161prime-)
)

)
(:action Flag161Action-12
:parameters ()
:precondition
(and
(RIGHTOF9-ROBOT)
)
:effect
(and
(Flag161-)
(Flag161prime-)
)

)
(:action Flag161Action-13
:parameters ()
:precondition
(and
(RIGHTOF6-ROBOT)
)
:effect
(and
(Flag161-)
(Flag161prime-)
)

)
(:action Prim161Action
:parameters ()
:precondition
(and
(not (RIGHTOF3-ROBOT))
(not (RIGHTOF8-ROBOT))
(not (RIGHTOF10-ROBOT))
(not (RIGHTOF5-ROBOT))
(not (RIGHTOF2-ROBOT))
(not (RIGHTOF13-ROBOT))
(not (RIGHTOF11-ROBOT))
(not (RIGHTOF0-ROBOT))
(not (RIGHTOF4-ROBOT))
(not (RIGHTOF1-ROBOT))
(not (RIGHTOF12-ROBOT))
(not (RIGHTOF7-ROBOT))
(not (RIGHTOF9-ROBOT))
(not (RIGHTOF6-ROBOT))
)
:effect
(and
(Flag161prime-)
(not (Flag161-))
)

)
(:action Flag162Action-0
:parameters ()
:precondition
(and
(RIGHTOF3-ROBOT)
)
:effect
(and
(Flag162-)
(Flag162prime-)
)

)
(:action Flag162Action-1
:parameters ()
:precondition
(and
(RIGHTOF8-ROBOT)
)
:effect
(and
(Flag162-)
(Flag162prime-)
)

)
(:action Flag162Action-2
:parameters ()
:precondition
(and
(RIGHTOF5-ROBOT)
)
:effect
(and
(Flag162-)
(Flag162prime-)
)

)
(:action Flag162Action-4
:parameters ()
:precondition
(and
(RIGHTOF13-ROBOT)
)
:effect
(and
(Flag162-)
(Flag162prime-)
)

)
(:action Flag162Action-5
:parameters ()
:precondition
(and
(RIGHTOF11-ROBOT)
)
:effect
(and
(Flag162-)
(Flag162prime-)
)

)
(:action Flag162Action-6
:parameters ()
:precondition
(and
(RIGHTOF10-ROBOT)
)
:effect
(and
(Flag162-)
(Flag162prime-)
)

)
(:action Flag162Action-7
:parameters ()
:precondition
(and
(RIGHTOF4-ROBOT)
)
:effect
(and
(Flag162-)
(Flag162prime-)
)

)
(:action Flag162Action-8
:parameters ()
:precondition
(and
(RIGHTOF1-ROBOT)
)
:effect
(and
(Flag162-)
(Flag162prime-)
)

)
(:action Flag162Action-9
:parameters ()
:precondition
(and
(RIGHTOF12-ROBOT)
)
:effect
(and
(Flag162-)
(Flag162prime-)
)

)
(:action Flag162Action-10
:parameters ()
:precondition
(and
(RIGHTOF7-ROBOT)
)
:effect
(and
(Flag162-)
(Flag162prime-)
)

)
(:action Flag162Action-11
:parameters ()
:precondition
(and
(RIGHTOF9-ROBOT)
)
:effect
(and
(Flag162-)
(Flag162prime-)
)

)
(:action Flag162Action-12
:parameters ()
:precondition
(and
(RIGHTOF6-ROBOT)
)
:effect
(and
(Flag162-)
(Flag162prime-)
)

)
(:action Prim162Action
:parameters ()
:precondition
(and
(not (RIGHTOF3-ROBOT))
(not (RIGHTOF8-ROBOT))
(not (RIGHTOF5-ROBOT))
(not (RIGHTOF2-ROBOT))
(not (RIGHTOF13-ROBOT))
(not (RIGHTOF11-ROBOT))
(not (RIGHTOF10-ROBOT))
(not (RIGHTOF4-ROBOT))
(not (RIGHTOF1-ROBOT))
(not (RIGHTOF12-ROBOT))
(not (RIGHTOF7-ROBOT))
(not (RIGHTOF9-ROBOT))
(not (RIGHTOF6-ROBOT))
)
:effect
(and
(Flag162prime-)
(not (Flag162-))
)

)
(:action Flag163Action-0
:parameters ()
:precondition
(and
(RIGHTOF3-ROBOT)
)
:effect
(and
(Flag163-)
(Flag163prime-)
)

)
(:action Flag163Action-1
:parameters ()
:precondition
(and
(RIGHTOF5-ROBOT)
)
:effect
(and
(Flag163-)
(Flag163prime-)
)

)
(:action Flag163Action-3
:parameters ()
:precondition
(and
(RIGHTOF13-ROBOT)
)
:effect
(and
(Flag163-)
(Flag163prime-)
)

)
(:action Flag163Action-4
:parameters ()
:precondition
(and
(RIGHTOF11-ROBOT)
)
:effect
(and
(Flag163-)
(Flag163prime-)
)

)
(:action Flag163Action-5
:parameters ()
:precondition
(and
(RIGHTOF10-ROBOT)
)
:effect
(and
(Flag163-)
(Flag163prime-)
)

)
(:action Flag163Action-6
:parameters ()
:precondition
(and
(RIGHTOF4-ROBOT)
)
:effect
(and
(Flag163-)
(Flag163prime-)
)

)
(:action Flag163Action-7
:parameters ()
:precondition
(and
(RIGHTOF8-ROBOT)
)
:effect
(and
(Flag163-)
(Flag163prime-)
)

)
(:action Flag163Action-8
:parameters ()
:precondition
(and
(RIGHTOF12-ROBOT)
)
:effect
(and
(Flag163-)
(Flag163prime-)
)

)
(:action Flag163Action-9
:parameters ()
:precondition
(and
(RIGHTOF7-ROBOT)
)
:effect
(and
(Flag163-)
(Flag163prime-)
)

)
(:action Flag163Action-10
:parameters ()
:precondition
(and
(RIGHTOF9-ROBOT)
)
:effect
(and
(Flag163-)
(Flag163prime-)
)

)
(:action Flag163Action-11
:parameters ()
:precondition
(and
(RIGHTOF6-ROBOT)
)
:effect
(and
(Flag163-)
(Flag163prime-)
)

)
(:action Prim163Action
:parameters ()
:precondition
(and
(not (RIGHTOF3-ROBOT))
(not (RIGHTOF5-ROBOT))
(not (RIGHTOF2-ROBOT))
(not (RIGHTOF13-ROBOT))
(not (RIGHTOF11-ROBOT))
(not (RIGHTOF10-ROBOT))
(not (RIGHTOF4-ROBOT))
(not (RIGHTOF8-ROBOT))
(not (RIGHTOF12-ROBOT))
(not (RIGHTOF7-ROBOT))
(not (RIGHTOF9-ROBOT))
(not (RIGHTOF6-ROBOT))
)
:effect
(and
(Flag163prime-)
(not (Flag163-))
)

)
(:action Flag164Action-0
:parameters ()
:precondition
(and
(RIGHTOF3-ROBOT)
)
:effect
(and
(Flag164-)
(Flag164prime-)
)

)
(:action Flag164Action-1
:parameters ()
:precondition
(and
(RIGHTOF5-ROBOT)
)
:effect
(and
(Flag164-)
(Flag164prime-)
)

)
(:action Flag164Action-2
:parameters ()
:precondition
(and
(RIGHTOF13-ROBOT)
)
:effect
(and
(Flag164-)
(Flag164prime-)
)

)
(:action Flag164Action-3
:parameters ()
:precondition
(and
(RIGHTOF11-ROBOT)
)
:effect
(and
(Flag164-)
(Flag164prime-)
)

)
(:action Flag164Action-4
:parameters ()
:precondition
(and
(RIGHTOF10-ROBOT)
)
:effect
(and
(Flag164-)
(Flag164prime-)
)

)
(:action Flag164Action-5
:parameters ()
:precondition
(and
(RIGHTOF4-ROBOT)
)
:effect
(and
(Flag164-)
(Flag164prime-)
)

)
(:action Flag164Action-6
:parameters ()
:precondition
(and
(RIGHTOF8-ROBOT)
)
:effect
(and
(Flag164-)
(Flag164prime-)
)

)
(:action Flag164Action-7
:parameters ()
:precondition
(and
(RIGHTOF12-ROBOT)
)
:effect
(and
(Flag164-)
(Flag164prime-)
)

)
(:action Flag164Action-8
:parameters ()
:precondition
(and
(RIGHTOF7-ROBOT)
)
:effect
(and
(Flag164-)
(Flag164prime-)
)

)
(:action Flag164Action-9
:parameters ()
:precondition
(and
(RIGHTOF9-ROBOT)
)
:effect
(and
(Flag164-)
(Flag164prime-)
)

)
(:action Flag164Action-10
:parameters ()
:precondition
(and
(RIGHTOF6-ROBOT)
)
:effect
(and
(Flag164-)
(Flag164prime-)
)

)
(:action Prim164Action
:parameters ()
:precondition
(and
(not (RIGHTOF3-ROBOT))
(not (RIGHTOF5-ROBOT))
(not (RIGHTOF13-ROBOT))
(not (RIGHTOF11-ROBOT))
(not (RIGHTOF10-ROBOT))
(not (RIGHTOF4-ROBOT))
(not (RIGHTOF8-ROBOT))
(not (RIGHTOF12-ROBOT))
(not (RIGHTOF7-ROBOT))
(not (RIGHTOF9-ROBOT))
(not (RIGHTOF6-ROBOT))
)
:effect
(and
(Flag164prime-)
(not (Flag164-))
)

)
(:action Flag165Action-0
:parameters ()
:precondition
(and
(RIGHTOF5-ROBOT)
)
:effect
(and
(Flag165-)
(Flag165prime-)
)

)
(:action Flag165Action-1
:parameters ()
:precondition
(and
(RIGHTOF13-ROBOT)
)
:effect
(and
(Flag165-)
(Flag165prime-)
)

)
(:action Flag165Action-2
:parameters ()
:precondition
(and
(RIGHTOF11-ROBOT)
)
:effect
(and
(Flag165-)
(Flag165prime-)
)

)
(:action Flag165Action-3
:parameters ()
:precondition
(and
(RIGHTOF10-ROBOT)
)
:effect
(and
(Flag165-)
(Flag165prime-)
)

)
(:action Flag165Action-4
:parameters ()
:precondition
(and
(RIGHTOF4-ROBOT)
)
:effect
(and
(Flag165-)
(Flag165prime-)
)

)
(:action Flag165Action-5
:parameters ()
:precondition
(and
(RIGHTOF8-ROBOT)
)
:effect
(and
(Flag165-)
(Flag165prime-)
)

)
(:action Flag165Action-6
:parameters ()
:precondition
(and
(RIGHTOF12-ROBOT)
)
:effect
(and
(Flag165-)
(Flag165prime-)
)

)
(:action Flag165Action-7
:parameters ()
:precondition
(and
(RIGHTOF7-ROBOT)
)
:effect
(and
(Flag165-)
(Flag165prime-)
)

)
(:action Flag165Action-8
:parameters ()
:precondition
(and
(RIGHTOF9-ROBOT)
)
:effect
(and
(Flag165-)
(Flag165prime-)
)

)
(:action Flag165Action-9
:parameters ()
:precondition
(and
(RIGHTOF6-ROBOT)
)
:effect
(and
(Flag165-)
(Flag165prime-)
)

)
(:action Prim165Action
:parameters ()
:precondition
(and
(not (RIGHTOF5-ROBOT))
(not (RIGHTOF13-ROBOT))
(not (RIGHTOF11-ROBOT))
(not (RIGHTOF10-ROBOT))
(not (RIGHTOF4-ROBOT))
(not (RIGHTOF8-ROBOT))
(not (RIGHTOF12-ROBOT))
(not (RIGHTOF7-ROBOT))
(not (RIGHTOF9-ROBOT))
(not (RIGHTOF6-ROBOT))
)
:effect
(and
(Flag165prime-)
(not (Flag165-))
)

)
(:action Flag166Action-0
:parameters ()
:precondition
(and
(RIGHTOF5-ROBOT)
)
:effect
(and
(Flag166-)
(Flag166prime-)
)

)
(:action Flag166Action-1
:parameters ()
:precondition
(and
(RIGHTOF13-ROBOT)
)
:effect
(and
(Flag166-)
(Flag166prime-)
)

)
(:action Flag166Action-2
:parameters ()
:precondition
(and
(RIGHTOF11-ROBOT)
)
:effect
(and
(Flag166-)
(Flag166prime-)
)

)
(:action Flag166Action-3
:parameters ()
:precondition
(and
(RIGHTOF10-ROBOT)
)
:effect
(and
(Flag166-)
(Flag166prime-)
)

)
(:action Flag166Action-4
:parameters ()
:precondition
(and
(RIGHTOF8-ROBOT)
)
:effect
(and
(Flag166-)
(Flag166prime-)
)

)
(:action Flag166Action-5
:parameters ()
:precondition
(and
(RIGHTOF12-ROBOT)
)
:effect
(and
(Flag166-)
(Flag166prime-)
)

)
(:action Flag166Action-6
:parameters ()
:precondition
(and
(RIGHTOF7-ROBOT)
)
:effect
(and
(Flag166-)
(Flag166prime-)
)

)
(:action Flag166Action-7
:parameters ()
:precondition
(and
(RIGHTOF9-ROBOT)
)
:effect
(and
(Flag166-)
(Flag166prime-)
)

)
(:action Flag166Action-8
:parameters ()
:precondition
(and
(RIGHTOF6-ROBOT)
)
:effect
(and
(Flag166-)
(Flag166prime-)
)

)
(:action Prim166Action
:parameters ()
:precondition
(and
(not (RIGHTOF5-ROBOT))
(not (RIGHTOF13-ROBOT))
(not (RIGHTOF11-ROBOT))
(not (RIGHTOF10-ROBOT))
(not (RIGHTOF8-ROBOT))
(not (RIGHTOF12-ROBOT))
(not (RIGHTOF7-ROBOT))
(not (RIGHTOF9-ROBOT))
(not (RIGHTOF6-ROBOT))
)
:effect
(and
(Flag166prime-)
(not (Flag166-))
)

)
(:action Flag167Action-0
:parameters ()
:precondition
(and
(RIGHTOF13-ROBOT)
)
:effect
(and
(Flag167-)
(Flag167prime-)
)

)
(:action Flag167Action-1
:parameters ()
:precondition
(and
(RIGHTOF11-ROBOT)
)
:effect
(and
(Flag167-)
(Flag167prime-)
)

)
(:action Flag167Action-2
:parameters ()
:precondition
(and
(RIGHTOF10-ROBOT)
)
:effect
(and
(Flag167-)
(Flag167prime-)
)

)
(:action Flag167Action-3
:parameters ()
:precondition
(and
(RIGHTOF8-ROBOT)
)
:effect
(and
(Flag167-)
(Flag167prime-)
)

)
(:action Flag167Action-4
:parameters ()
:precondition
(and
(RIGHTOF12-ROBOT)
)
:effect
(and
(Flag167-)
(Flag167prime-)
)

)
(:action Flag167Action-5
:parameters ()
:precondition
(and
(RIGHTOF7-ROBOT)
)
:effect
(and
(Flag167-)
(Flag167prime-)
)

)
(:action Flag167Action-6
:parameters ()
:precondition
(and
(RIGHTOF9-ROBOT)
)
:effect
(and
(Flag167-)
(Flag167prime-)
)

)
(:action Flag167Action-7
:parameters ()
:precondition
(and
(RIGHTOF6-ROBOT)
)
:effect
(and
(Flag167-)
(Flag167prime-)
)

)
(:action Prim167Action
:parameters ()
:precondition
(and
(not (RIGHTOF13-ROBOT))
(not (RIGHTOF11-ROBOT))
(not (RIGHTOF10-ROBOT))
(not (RIGHTOF8-ROBOT))
(not (RIGHTOF12-ROBOT))
(not (RIGHTOF7-ROBOT))
(not (RIGHTOF9-ROBOT))
(not (RIGHTOF6-ROBOT))
)
:effect
(and
(Flag167prime-)
(not (Flag167-))
)

)
(:action Flag168Action-0
:parameters ()
:precondition
(and
(RIGHTOF13-ROBOT)
)
:effect
(and
(Flag168-)
(Flag168prime-)
)

)
(:action Flag168Action-1
:parameters ()
:precondition
(and
(RIGHTOF11-ROBOT)
)
:effect
(and
(Flag168-)
(Flag168prime-)
)

)
(:action Flag168Action-2
:parameters ()
:precondition
(and
(RIGHTOF10-ROBOT)
)
:effect
(and
(Flag168-)
(Flag168prime-)
)

)
(:action Flag168Action-3
:parameters ()
:precondition
(and
(RIGHTOF8-ROBOT)
)
:effect
(and
(Flag168-)
(Flag168prime-)
)

)
(:action Flag168Action-4
:parameters ()
:precondition
(and
(RIGHTOF7-ROBOT)
)
:effect
(and
(Flag168-)
(Flag168prime-)
)

)
(:action Flag168Action-5
:parameters ()
:precondition
(and
(RIGHTOF9-ROBOT)
)
:effect
(and
(Flag168-)
(Flag168prime-)
)

)
(:action Flag168Action-6
:parameters ()
:precondition
(and
(RIGHTOF12-ROBOT)
)
:effect
(and
(Flag168-)
(Flag168prime-)
)

)
(:action Prim168Action
:parameters ()
:precondition
(and
(not (RIGHTOF13-ROBOT))
(not (RIGHTOF11-ROBOT))
(not (RIGHTOF10-ROBOT))
(not (RIGHTOF8-ROBOT))
(not (RIGHTOF7-ROBOT))
(not (RIGHTOF9-ROBOT))
(not (RIGHTOF12-ROBOT))
)
:effect
(and
(Flag168prime-)
(not (Flag168-))
)

)
(:action Flag169Action-0
:parameters ()
:precondition
(and
(RIGHTOF13-ROBOT)
)
:effect
(and
(Flag169-)
(Flag169prime-)
)

)
(:action Flag169Action-1
:parameters ()
:precondition
(and
(RIGHTOF11-ROBOT)
)
:effect
(and
(Flag169-)
(Flag169prime-)
)

)
(:action Flag169Action-2
:parameters ()
:precondition
(and
(RIGHTOF10-ROBOT)
)
:effect
(and
(Flag169-)
(Flag169prime-)
)

)
(:action Flag169Action-3
:parameters ()
:precondition
(and
(RIGHTOF8-ROBOT)
)
:effect
(and
(Flag169-)
(Flag169prime-)
)

)
(:action Flag169Action-4
:parameters ()
:precondition
(and
(RIGHTOF9-ROBOT)
)
:effect
(and
(Flag169-)
(Flag169prime-)
)

)
(:action Flag169Action-5
:parameters ()
:precondition
(and
(RIGHTOF12-ROBOT)
)
:effect
(and
(Flag169-)
(Flag169prime-)
)

)
(:action Prim169Action
:parameters ()
:precondition
(and
(not (RIGHTOF13-ROBOT))
(not (RIGHTOF11-ROBOT))
(not (RIGHTOF10-ROBOT))
(not (RIGHTOF8-ROBOT))
(not (RIGHTOF9-ROBOT))
(not (RIGHTOF12-ROBOT))
)
:effect
(and
(Flag169prime-)
(not (Flag169-))
)

)
(:action Flag170Action-0
:parameters ()
:precondition
(and
(RIGHTOF10-ROBOT)
)
:effect
(and
(Flag170-)
(Flag170prime-)
)

)
(:action Flag170Action-1
:parameters ()
:precondition
(and
(RIGHTOF9-ROBOT)
)
:effect
(and
(Flag170-)
(Flag170prime-)
)

)
(:action Flag170Action-2
:parameters ()
:precondition
(and
(RIGHTOF13-ROBOT)
)
:effect
(and
(Flag170-)
(Flag170prime-)
)

)
(:action Flag170Action-3
:parameters ()
:precondition
(and
(RIGHTOF12-ROBOT)
)
:effect
(and
(Flag170-)
(Flag170prime-)
)

)
(:action Flag170Action-4
:parameters ()
:precondition
(and
(RIGHTOF11-ROBOT)
)
:effect
(and
(Flag170-)
(Flag170prime-)
)

)
(:action Prim170Action
:parameters ()
:precondition
(and
(not (RIGHTOF10-ROBOT))
(not (RIGHTOF9-ROBOT))
(not (RIGHTOF13-ROBOT))
(not (RIGHTOF12-ROBOT))
(not (RIGHTOF11-ROBOT))
)
:effect
(and
(Flag170prime-)
(not (Flag170-))
)

)
(:action Flag171Action-0
:parameters ()
:precondition
(and
(RIGHTOF10-ROBOT)
)
:effect
(and
(Flag171-)
(Flag171prime-)
)

)
(:action Flag171Action-1
:parameters ()
:precondition
(and
(RIGHTOF13-ROBOT)
)
:effect
(and
(Flag171-)
(Flag171prime-)
)

)
(:action Flag171Action-2
:parameters ()
:precondition
(and
(RIGHTOF12-ROBOT)
)
:effect
(and
(Flag171-)
(Flag171prime-)
)

)
(:action Flag171Action-3
:parameters ()
:precondition
(and
(RIGHTOF11-ROBOT)
)
:effect
(and
(Flag171-)
(Flag171prime-)
)

)
(:action Prim171Action
:parameters ()
:precondition
(and
(not (RIGHTOF10-ROBOT))
(not (RIGHTOF13-ROBOT))
(not (RIGHTOF12-ROBOT))
(not (RIGHTOF11-ROBOT))
)
:effect
(and
(Flag171prime-)
(not (Flag171-))
)

)
(:action Flag172Action-0
:parameters ()
:precondition
(and
(RIGHTOF13-ROBOT)
)
:effect
(and
(Flag172-)
(Flag172prime-)
)

)
(:action Flag172Action-1
:parameters ()
:precondition
(and
(RIGHTOF12-ROBOT)
)
:effect
(and
(Flag172-)
(Flag172prime-)
)

)
(:action Flag172Action-2
:parameters ()
:precondition
(and
(RIGHTOF11-ROBOT)
)
:effect
(and
(Flag172-)
(Flag172prime-)
)

)
(:action Prim172Action
:parameters ()
:precondition
(and
(not (RIGHTOF13-ROBOT))
(not (RIGHTOF12-ROBOT))
(not (RIGHTOF11-ROBOT))
)
:effect
(and
(Flag172prime-)
(not (Flag172-))
)

)
(:action Flag173Action-0
:parameters ()
:precondition
(and
(RIGHTOF13-ROBOT)
)
:effect
(and
(Flag173-)
(Flag173prime-)
)

)
(:action Flag173Action-1
:parameters ()
:precondition
(and
(RIGHTOF12-ROBOT)
)
:effect
(and
(Flag173-)
(Flag173prime-)
)

)
(:action Prim173Action
:parameters ()
:precondition
(and
(not (RIGHTOF13-ROBOT))
(not (RIGHTOF12-ROBOT))
)
:effect
(and
(Flag173prime-)
(not (Flag173-))
)

)
(:action Flag174Action-0
:parameters ()
:precondition
(and
(LEFTOF1-ROBOT)
)
:effect
(and
(Flag174-)
(Flag174prime-)
)

)
(:action Flag174Action-1
:parameters ()
:precondition
(and
(LEFTOF2-ROBOT)
)
:effect
(and
(Flag174-)
(Flag174prime-)
)

)
(:action Prim174Action
:parameters ()
:precondition
(and
(not (LEFTOF1-ROBOT))
(not (LEFTOF2-ROBOT))
)
:effect
(and
(Flag174prime-)
(not (Flag174-))
)

)
(:action Flag175Action-0
:parameters ()
:precondition
(and
(LEFTOF1-ROBOT)
)
:effect
(and
(Flag175-)
(Flag175prime-)
)

)
(:action Flag175Action-1
:parameters ()
:precondition
(and
(LEFTOF3-ROBOT)
)
:effect
(and
(Flag175-)
(Flag175prime-)
)

)
(:action Flag175Action-2
:parameters ()
:precondition
(and
(LEFTOF2-ROBOT)
)
:effect
(and
(Flag175-)
(Flag175prime-)
)

)
(:action Prim175Action
:parameters ()
:precondition
(and
(not (LEFTOF1-ROBOT))
(not (LEFTOF3-ROBOT))
(not (LEFTOF2-ROBOT))
)
:effect
(and
(Flag175prime-)
(not (Flag175-))
)

)
(:action Flag176Action-0
:parameters ()
:precondition
(and
(LEFTOF1-ROBOT)
)
:effect
(and
(Flag176-)
(Flag176prime-)
)

)
(:action Flag176Action-1
:parameters ()
:precondition
(and
(LEFTOF3-ROBOT)
)
:effect
(and
(Flag176-)
(Flag176prime-)
)

)
(:action Flag176Action-2
:parameters ()
:precondition
(and
(LEFTOF4-ROBOT)
)
:effect
(and
(Flag176-)
(Flag176prime-)
)

)
(:action Flag176Action-3
:parameters ()
:precondition
(and
(LEFTOF2-ROBOT)
)
:effect
(and
(Flag176-)
(Flag176prime-)
)

)
(:action Prim176Action
:parameters ()
:precondition
(and
(not (LEFTOF1-ROBOT))
(not (LEFTOF3-ROBOT))
(not (LEFTOF4-ROBOT))
(not (LEFTOF2-ROBOT))
)
:effect
(and
(Flag176prime-)
(not (Flag176-))
)

)
(:action Flag177Action-0
:parameters ()
:precondition
(and
(LEFTOF5-ROBOT)
)
:effect
(and
(Flag177-)
(Flag177prime-)
)

)
(:action Flag177Action-1
:parameters ()
:precondition
(and
(LEFTOF1-ROBOT)
)
:effect
(and
(Flag177-)
(Flag177prime-)
)

)
(:action Flag177Action-2
:parameters ()
:precondition
(and
(LEFTOF3-ROBOT)
)
:effect
(and
(Flag177-)
(Flag177prime-)
)

)
(:action Flag177Action-3
:parameters ()
:precondition
(and
(LEFTOF4-ROBOT)
)
:effect
(and
(Flag177-)
(Flag177prime-)
)

)
(:action Flag177Action-4
:parameters ()
:precondition
(and
(LEFTOF2-ROBOT)
)
:effect
(and
(Flag177-)
(Flag177prime-)
)

)
(:action Prim177Action
:parameters ()
:precondition
(and
(not (LEFTOF5-ROBOT))
(not (LEFTOF1-ROBOT))
(not (LEFTOF3-ROBOT))
(not (LEFTOF4-ROBOT))
(not (LEFTOF2-ROBOT))
)
:effect
(and
(Flag177prime-)
(not (Flag177-))
)

)
(:action Flag178Action-0
:parameters ()
:precondition
(and
(LEFTOF2-ROBOT)
)
:effect
(and
(Flag178-)
(Flag178prime-)
)

)
(:action Flag178Action-1
:parameters ()
:precondition
(and
(LEFTOF1-ROBOT)
)
:effect
(and
(Flag178-)
(Flag178prime-)
)

)
(:action Flag178Action-2
:parameters ()
:precondition
(and
(LEFTOF6-ROBOT)
)
:effect
(and
(Flag178-)
(Flag178prime-)
)

)
(:action Flag178Action-3
:parameters ()
:precondition
(and
(LEFTOF5-ROBOT)
)
:effect
(and
(Flag178-)
(Flag178prime-)
)

)
(:action Flag178Action-4
:parameters ()
:precondition
(and
(LEFTOF3-ROBOT)
)
:effect
(and
(Flag178-)
(Flag178prime-)
)

)
(:action Flag178Action-5
:parameters ()
:precondition
(and
(LEFTOF4-ROBOT)
)
:effect
(and
(Flag178-)
(Flag178prime-)
)

)
(:action Prim178Action
:parameters ()
:precondition
(and
(not (LEFTOF2-ROBOT))
(not (LEFTOF1-ROBOT))
(not (LEFTOF6-ROBOT))
(not (LEFTOF5-ROBOT))
(not (LEFTOF3-ROBOT))
(not (LEFTOF4-ROBOT))
)
:effect
(and
(Flag178prime-)
(not (Flag178-))
)

)
(:action Flag179Action-0
:parameters ()
:precondition
(and
(LEFTOF2-ROBOT)
)
:effect
(and
(Flag179-)
(Flag179prime-)
)

)
(:action Flag179Action-1
:parameters ()
:precondition
(and
(LEFTOF7-ROBOT)
)
:effect
(and
(Flag179-)
(Flag179prime-)
)

)
(:action Flag179Action-2
:parameters ()
:precondition
(and
(LEFTOF1-ROBOT)
)
:effect
(and
(Flag179-)
(Flag179prime-)
)

)
(:action Flag179Action-3
:parameters ()
:precondition
(and
(LEFTOF6-ROBOT)
)
:effect
(and
(Flag179-)
(Flag179prime-)
)

)
(:action Flag179Action-4
:parameters ()
:precondition
(and
(LEFTOF5-ROBOT)
)
:effect
(and
(Flag179-)
(Flag179prime-)
)

)
(:action Flag179Action-5
:parameters ()
:precondition
(and
(LEFTOF3-ROBOT)
)
:effect
(and
(Flag179-)
(Flag179prime-)
)

)
(:action Flag179Action-6
:parameters ()
:precondition
(and
(LEFTOF4-ROBOT)
)
:effect
(and
(Flag179-)
(Flag179prime-)
)

)
(:action Prim179Action
:parameters ()
:precondition
(and
(not (LEFTOF2-ROBOT))
(not (LEFTOF7-ROBOT))
(not (LEFTOF1-ROBOT))
(not (LEFTOF6-ROBOT))
(not (LEFTOF5-ROBOT))
(not (LEFTOF3-ROBOT))
(not (LEFTOF4-ROBOT))
)
:effect
(and
(Flag179prime-)
(not (Flag179-))
)

)
(:action Flag180Action-0
:parameters ()
:precondition
(and
(LEFTOF2-ROBOT)
)
:effect
(and
(Flag180-)
(Flag180prime-)
)

)
(:action Flag180Action-1
:parameters ()
:precondition
(and
(LEFTOF7-ROBOT)
)
:effect
(and
(Flag180-)
(Flag180prime-)
)

)
(:action Flag180Action-2
:parameters ()
:precondition
(and
(LEFTOF1-ROBOT)
)
:effect
(and
(Flag180-)
(Flag180prime-)
)

)
(:action Flag180Action-3
:parameters ()
:precondition
(and
(LEFTOF6-ROBOT)
)
:effect
(and
(Flag180-)
(Flag180prime-)
)

)
(:action Flag180Action-4
:parameters ()
:precondition
(and
(LEFTOF5-ROBOT)
)
:effect
(and
(Flag180-)
(Flag180prime-)
)

)
(:action Flag180Action-5
:parameters ()
:precondition
(and
(LEFTOF3-ROBOT)
)
:effect
(and
(Flag180-)
(Flag180prime-)
)

)
(:action Flag180Action-6
:parameters ()
:precondition
(and
(LEFTOF4-ROBOT)
)
:effect
(and
(Flag180-)
(Flag180prime-)
)

)
(:action Flag180Action-7
:parameters ()
:precondition
(and
(LEFTOF8-ROBOT)
)
:effect
(and
(Flag180-)
(Flag180prime-)
)

)
(:action Prim180Action
:parameters ()
:precondition
(and
(not (LEFTOF2-ROBOT))
(not (LEFTOF7-ROBOT))
(not (LEFTOF1-ROBOT))
(not (LEFTOF6-ROBOT))
(not (LEFTOF5-ROBOT))
(not (LEFTOF3-ROBOT))
(not (LEFTOF4-ROBOT))
(not (LEFTOF8-ROBOT))
)
:effect
(and
(Flag180prime-)
(not (Flag180-))
)

)
(:action Flag181Action-0
:parameters ()
:precondition
(and
(LEFTOF9-ROBOT)
)
:effect
(and
(Flag181-)
(Flag181prime-)
)

)
(:action Flag181Action-1
:parameters ()
:precondition
(and
(LEFTOF2-ROBOT)
)
:effect
(and
(Flag181-)
(Flag181prime-)
)

)
(:action Flag181Action-2
:parameters ()
:precondition
(and
(LEFTOF7-ROBOT)
)
:effect
(and
(Flag181-)
(Flag181prime-)
)

)
(:action Flag181Action-3
:parameters ()
:precondition
(and
(LEFTOF1-ROBOT)
)
:effect
(and
(Flag181-)
(Flag181prime-)
)

)
(:action Flag181Action-4
:parameters ()
:precondition
(and
(LEFTOF6-ROBOT)
)
:effect
(and
(Flag181-)
(Flag181prime-)
)

)
(:action Flag181Action-5
:parameters ()
:precondition
(and
(LEFTOF5-ROBOT)
)
:effect
(and
(Flag181-)
(Flag181prime-)
)

)
(:action Flag181Action-6
:parameters ()
:precondition
(and
(LEFTOF3-ROBOT)
)
:effect
(and
(Flag181-)
(Flag181prime-)
)

)
(:action Flag181Action-7
:parameters ()
:precondition
(and
(LEFTOF4-ROBOT)
)
:effect
(and
(Flag181-)
(Flag181prime-)
)

)
(:action Flag181Action-8
:parameters ()
:precondition
(and
(LEFTOF8-ROBOT)
)
:effect
(and
(Flag181-)
(Flag181prime-)
)

)
(:action Prim181Action
:parameters ()
:precondition
(and
(not (LEFTOF9-ROBOT))
(not (LEFTOF2-ROBOT))
(not (LEFTOF7-ROBOT))
(not (LEFTOF1-ROBOT))
(not (LEFTOF6-ROBOT))
(not (LEFTOF5-ROBOT))
(not (LEFTOF3-ROBOT))
(not (LEFTOF4-ROBOT))
(not (LEFTOF8-ROBOT))
)
:effect
(and
(Flag181prime-)
(not (Flag181-))
)

)
(:action Flag182Action-0
:parameters ()
:precondition
(and
(LEFTOF10-ROBOT)
)
:effect
(and
(Flag182-)
(Flag182prime-)
)

)
(:action Flag182Action-1
:parameters ()
:precondition
(and
(LEFTOF9-ROBOT)
)
:effect
(and
(Flag182-)
(Flag182prime-)
)

)
(:action Flag182Action-2
:parameters ()
:precondition
(and
(LEFTOF2-ROBOT)
)
:effect
(and
(Flag182-)
(Flag182prime-)
)

)
(:action Flag182Action-3
:parameters ()
:precondition
(and
(LEFTOF7-ROBOT)
)
:effect
(and
(Flag182-)
(Flag182prime-)
)

)
(:action Flag182Action-4
:parameters ()
:precondition
(and
(LEFTOF1-ROBOT)
)
:effect
(and
(Flag182-)
(Flag182prime-)
)

)
(:action Flag182Action-5
:parameters ()
:precondition
(and
(LEFTOF6-ROBOT)
)
:effect
(and
(Flag182-)
(Flag182prime-)
)

)
(:action Flag182Action-6
:parameters ()
:precondition
(and
(LEFTOF5-ROBOT)
)
:effect
(and
(Flag182-)
(Flag182prime-)
)

)
(:action Flag182Action-7
:parameters ()
:precondition
(and
(LEFTOF3-ROBOT)
)
:effect
(and
(Flag182-)
(Flag182prime-)
)

)
(:action Flag182Action-8
:parameters ()
:precondition
(and
(LEFTOF4-ROBOT)
)
:effect
(and
(Flag182-)
(Flag182prime-)
)

)
(:action Flag182Action-9
:parameters ()
:precondition
(and
(LEFTOF8-ROBOT)
)
:effect
(and
(Flag182-)
(Flag182prime-)
)

)
(:action Prim182Action
:parameters ()
:precondition
(and
(not (LEFTOF10-ROBOT))
(not (LEFTOF9-ROBOT))
(not (LEFTOF2-ROBOT))
(not (LEFTOF7-ROBOT))
(not (LEFTOF1-ROBOT))
(not (LEFTOF6-ROBOT))
(not (LEFTOF5-ROBOT))
(not (LEFTOF3-ROBOT))
(not (LEFTOF4-ROBOT))
(not (LEFTOF8-ROBOT))
)
:effect
(and
(Flag182prime-)
(not (Flag182-))
)

)
(:action Flag183Action-0
:parameters ()
:precondition
(and
(LEFTOF10-ROBOT)
)
:effect
(and
(Flag183-)
(Flag183prime-)
)

)
(:action Flag183Action-1
:parameters ()
:precondition
(and
(LEFTOF11-ROBOT)
)
:effect
(and
(Flag183-)
(Flag183prime-)
)

)
(:action Flag183Action-2
:parameters ()
:precondition
(and
(LEFTOF9-ROBOT)
)
:effect
(and
(Flag183-)
(Flag183prime-)
)

)
(:action Flag183Action-3
:parameters ()
:precondition
(and
(LEFTOF2-ROBOT)
)
:effect
(and
(Flag183-)
(Flag183prime-)
)

)
(:action Flag183Action-4
:parameters ()
:precondition
(and
(LEFTOF7-ROBOT)
)
:effect
(and
(Flag183-)
(Flag183prime-)
)

)
(:action Flag183Action-5
:parameters ()
:precondition
(and
(LEFTOF1-ROBOT)
)
:effect
(and
(Flag183-)
(Flag183prime-)
)

)
(:action Flag183Action-6
:parameters ()
:precondition
(and
(LEFTOF6-ROBOT)
)
:effect
(and
(Flag183-)
(Flag183prime-)
)

)
(:action Flag183Action-7
:parameters ()
:precondition
(and
(LEFTOF5-ROBOT)
)
:effect
(and
(Flag183-)
(Flag183prime-)
)

)
(:action Flag183Action-8
:parameters ()
:precondition
(and
(LEFTOF3-ROBOT)
)
:effect
(and
(Flag183-)
(Flag183prime-)
)

)
(:action Flag183Action-9
:parameters ()
:precondition
(and
(LEFTOF4-ROBOT)
)
:effect
(and
(Flag183-)
(Flag183prime-)
)

)
(:action Flag183Action-10
:parameters ()
:precondition
(and
(LEFTOF8-ROBOT)
)
:effect
(and
(Flag183-)
(Flag183prime-)
)

)
(:action Prim183Action
:parameters ()
:precondition
(and
(not (LEFTOF10-ROBOT))
(not (LEFTOF11-ROBOT))
(not (LEFTOF9-ROBOT))
(not (LEFTOF2-ROBOT))
(not (LEFTOF7-ROBOT))
(not (LEFTOF1-ROBOT))
(not (LEFTOF6-ROBOT))
(not (LEFTOF5-ROBOT))
(not (LEFTOF3-ROBOT))
(not (LEFTOF4-ROBOT))
(not (LEFTOF8-ROBOT))
)
:effect
(and
(Flag183prime-)
(not (Flag183-))
)

)
(:action Flag184Action-0
:parameters ()
:precondition
(and
(LEFTOF10-ROBOT)
)
:effect
(and
(Flag184-)
(Flag184prime-)
)

)
(:action Flag184Action-1
:parameters ()
:precondition
(and
(LEFTOF11-ROBOT)
)
:effect
(and
(Flag184-)
(Flag184prime-)
)

)
(:action Flag184Action-2
:parameters ()
:precondition
(and
(LEFTOF9-ROBOT)
)
:effect
(and
(Flag184-)
(Flag184prime-)
)

)
(:action Flag184Action-3
:parameters ()
:precondition
(and
(LEFTOF2-ROBOT)
)
:effect
(and
(Flag184-)
(Flag184prime-)
)

)
(:action Flag184Action-4
:parameters ()
:precondition
(and
(LEFTOF7-ROBOT)
)
:effect
(and
(Flag184-)
(Flag184prime-)
)

)
(:action Flag184Action-5
:parameters ()
:precondition
(and
(LEFTOF1-ROBOT)
)
:effect
(and
(Flag184-)
(Flag184prime-)
)

)
(:action Flag184Action-6
:parameters ()
:precondition
(and
(LEFTOF12-ROBOT)
)
:effect
(and
(Flag184-)
(Flag184prime-)
)

)
(:action Flag184Action-7
:parameters ()
:precondition
(and
(LEFTOF6-ROBOT)
)
:effect
(and
(Flag184-)
(Flag184prime-)
)

)
(:action Flag184Action-8
:parameters ()
:precondition
(and
(LEFTOF5-ROBOT)
)
:effect
(and
(Flag184-)
(Flag184prime-)
)

)
(:action Flag184Action-9
:parameters ()
:precondition
(and
(LEFTOF3-ROBOT)
)
:effect
(and
(Flag184-)
(Flag184prime-)
)

)
(:action Flag184Action-10
:parameters ()
:precondition
(and
(LEFTOF4-ROBOT)
)
:effect
(and
(Flag184-)
(Flag184prime-)
)

)
(:action Flag184Action-11
:parameters ()
:precondition
(and
(LEFTOF8-ROBOT)
)
:effect
(and
(Flag184-)
(Flag184prime-)
)

)
(:action Prim184Action
:parameters ()
:precondition
(and
(not (LEFTOF10-ROBOT))
(not (LEFTOF11-ROBOT))
(not (LEFTOF9-ROBOT))
(not (LEFTOF2-ROBOT))
(not (LEFTOF7-ROBOT))
(not (LEFTOF1-ROBOT))
(not (LEFTOF12-ROBOT))
(not (LEFTOF6-ROBOT))
(not (LEFTOF5-ROBOT))
(not (LEFTOF3-ROBOT))
(not (LEFTOF4-ROBOT))
(not (LEFTOF8-ROBOT))
)
:effect
(and
(Flag184prime-)
(not (Flag184-))
)

)
(:action Flag185Action-0
:parameters ()
:precondition
(and
(LEFTOF10-ROBOT)
)
:effect
(and
(Flag185-)
(Flag185prime-)
)

)
(:action Flag185Action-1
:parameters ()
:precondition
(and
(LEFTOF11-ROBOT)
)
:effect
(and
(Flag185-)
(Flag185prime-)
)

)
(:action Flag185Action-2
:parameters ()
:precondition
(and
(LEFTOF9-ROBOT)
)
:effect
(and
(Flag185-)
(Flag185prime-)
)

)
(:action Flag185Action-3
:parameters ()
:precondition
(and
(LEFTOF2-ROBOT)
)
:effect
(and
(Flag185-)
(Flag185prime-)
)

)
(:action Flag185Action-5
:parameters ()
:precondition
(and
(LEFTOF1-ROBOT)
)
:effect
(and
(Flag185-)
(Flag185prime-)
)

)
(:action Flag185Action-6
:parameters ()
:precondition
(and
(LEFTOF12-ROBOT)
)
:effect
(and
(Flag185-)
(Flag185prime-)
)

)
(:action Flag185Action-7
:parameters ()
:precondition
(and
(LEFTOF6-ROBOT)
)
:effect
(and
(Flag185-)
(Flag185prime-)
)

)
(:action Flag185Action-8
:parameters ()
:precondition
(and
(LEFTOF5-ROBOT)
)
:effect
(and
(Flag185-)
(Flag185prime-)
)

)
(:action Flag185Action-9
:parameters ()
:precondition
(and
(LEFTOF3-ROBOT)
)
:effect
(and
(Flag185-)
(Flag185prime-)
)

)
(:action Flag185Action-10
:parameters ()
:precondition
(and
(LEFTOF4-ROBOT)
)
:effect
(and
(Flag185-)
(Flag185prime-)
)

)
(:action Flag185Action-11
:parameters ()
:precondition
(and
(LEFTOF7-ROBOT)
)
:effect
(and
(Flag185-)
(Flag185prime-)
)

)
(:action Flag185Action-12
:parameters ()
:precondition
(and
(LEFTOF8-ROBOT)
)
:effect
(and
(Flag185-)
(Flag185prime-)
)

)
(:action Prim185Action
:parameters ()
:precondition
(and
(not (LEFTOF10-ROBOT))
(not (LEFTOF11-ROBOT))
(not (LEFTOF9-ROBOT))
(not (LEFTOF2-ROBOT))
(not (LEFTOF13-ROBOT))
(not (LEFTOF1-ROBOT))
(not (LEFTOF12-ROBOT))
(not (LEFTOF6-ROBOT))
(not (LEFTOF5-ROBOT))
(not (LEFTOF3-ROBOT))
(not (LEFTOF4-ROBOT))
(not (LEFTOF7-ROBOT))
(not (LEFTOF8-ROBOT))
)
:effect
(and
(Flag185prime-)
(not (Flag185-))
)

)
(:action Flag186Action
:parameters ()
:precondition
(and
(LEFTOF1-ROBOT)
(RIGHTOF4-ROBOT)
)
:effect
(and
(Flag186-)
(Flag186prime-)
)

)
(:action Flag187Action
:parameters ()
:precondition
(and
(LEFTOF1-ROBOT)
(RIGHTOF12-ROBOT)
)
:effect
(and
(Flag187-)
(Flag187prime-)
)

)
(:action Flag188Action
:parameters ()
:precondition
(and
(LEFTOF1-ROBOT)
(RIGHTOF7-ROBOT)
)
:effect
(and
(Flag188-)
(Flag188prime-)
)

)
(:action Flag189Action
:parameters ()
:precondition
(and
(LEFTOF1-ROBOT)
(RIGHTOF9-ROBOT)
)
:effect
(and
(Flag189-)
(Flag189prime-)
)

)
(:action Flag190Action
:parameters ()
:precondition
(and
(RIGHTOF13-ROBOT)
(LEFTOF1-ROBOT)
)
:effect
(and
(Flag190-)
(Flag190prime-)
)

)
(:action Flag191Action
:parameters ()
:precondition
(and
(LEFTOF1-ROBOT)
(RIGHTOF1-ROBOT)
)
:effect
(and
(Flag191-)
(Flag191prime-)
)

)
(:action Flag192Action
:parameters ()
:precondition
(and
(RIGHTOF3-ROBOT)
(LEFTOF1-ROBOT)
)
:effect
(and
(Flag192-)
(Flag192prime-)
)

)
(:action Flag193Action
:parameters ()
:precondition
(and
(RIGHTOF0-ROBOT)
(LEFTOF1-ROBOT)
)
:effect
(and
(Flag193-)
(Flag193prime-)
)

)
(:action Flag194Action
:parameters ()
:precondition
(and
(LEFTOF1-ROBOT)
(RIGHTOF5-ROBOT)
)
:effect
(and
(Flag194-)
(Flag194prime-)
)

)
(:action Flag195Action
:parameters ()
:precondition
(and
(LEFTOF1-ROBOT)
(RIGHTOF2-ROBOT)
)
:effect
(and
(Flag195-)
(Flag195prime-)
)

)
(:action Flag196Action
:parameters ()
:precondition
(and
(RIGHTOF11-ROBOT)
(LEFTOF1-ROBOT)
)
:effect
(and
(Flag196-)
(Flag196prime-)
)

)
(:action Flag197Action
:parameters ()
:precondition
(and
(RIGHTOF8-ROBOT)
(LEFTOF1-ROBOT)
)
:effect
(and
(Flag197-)
(Flag197prime-)
)

)
(:action Flag198Action
:parameters ()
:precondition
(and
(LEFTOF1-ROBOT)
(RIGHTOF6-ROBOT)
)
:effect
(and
(Flag198-)
(Flag198prime-)
)

)
(:action Flag199Action
:parameters ()
:precondition
(and
(RIGHTOF10-ROBOT)
(LEFTOF1-ROBOT)
)
:effect
(and
(Flag199-)
(Flag199prime-)
)

)
(:action Prim186Action-0
:parameters ()
:precondition
(and
(not (LEFTOF1-ROBOT))
)
:effect
(and
(Flag186prime-)
(not (Flag186-))
)

)
(:action Prim186Action-1
:parameters ()
:precondition
(and
(not (RIGHTOF4-ROBOT))
)
:effect
(and
(Flag186prime-)
(not (Flag186-))
)

)
(:action Prim187Action-0
:parameters ()
:precondition
(and
(not (LEFTOF1-ROBOT))
)
:effect
(and
(Flag187prime-)
(not (Flag187-))
)

)
(:action Prim187Action-1
:parameters ()
:precondition
(and
(not (RIGHTOF12-ROBOT))
)
:effect
(and
(Flag187prime-)
(not (Flag187-))
)

)
(:action Prim188Action-0
:parameters ()
:precondition
(and
(not (LEFTOF1-ROBOT))
)
:effect
(and
(Flag188prime-)
(not (Flag188-))
)

)
(:action Prim188Action-1
:parameters ()
:precondition
(and
(not (RIGHTOF7-ROBOT))
)
:effect
(and
(Flag188prime-)
(not (Flag188-))
)

)
(:action Prim189Action-0
:parameters ()
:precondition
(and
(not (LEFTOF1-ROBOT))
)
:effect
(and
(Flag189prime-)
(not (Flag189-))
)

)
(:action Prim189Action-1
:parameters ()
:precondition
(and
(not (RIGHTOF9-ROBOT))
)
:effect
(and
(Flag189prime-)
(not (Flag189-))
)

)
(:action Prim190Action-0
:parameters ()
:precondition
(and
(not (RIGHTOF13-ROBOT))
)
:effect
(and
(Flag190prime-)
(not (Flag190-))
)

)
(:action Prim190Action-1
:parameters ()
:precondition
(and
(not (LEFTOF1-ROBOT))
)
:effect
(and
(Flag190prime-)
(not (Flag190-))
)

)
(:action Prim191Action-0
:parameters ()
:precondition
(and
(not (LEFTOF1-ROBOT))
)
:effect
(and
(Flag191prime-)
(not (Flag191-))
)

)
(:action Prim191Action-1
:parameters ()
:precondition
(and
(not (RIGHTOF1-ROBOT))
)
:effect
(and
(Flag191prime-)
(not (Flag191-))
)

)
(:action Prim192Action-0
:parameters ()
:precondition
(and
(not (RIGHTOF3-ROBOT))
)
:effect
(and
(Flag192prime-)
(not (Flag192-))
)

)
(:action Prim192Action-1
:parameters ()
:precondition
(and
(not (LEFTOF1-ROBOT))
)
:effect
(and
(Flag192prime-)
(not (Flag192-))
)

)
(:action Prim193Action-0
:parameters ()
:precondition
(and
(not (RIGHTOF0-ROBOT))
)
:effect
(and
(Flag193prime-)
(not (Flag193-))
)

)
(:action Prim193Action-1
:parameters ()
:precondition
(and
(not (LEFTOF1-ROBOT))
)
:effect
(and
(Flag193prime-)
(not (Flag193-))
)

)
(:action Prim194Action-0
:parameters ()
:precondition
(and
(not (LEFTOF1-ROBOT))
)
:effect
(and
(Flag194prime-)
(not (Flag194-))
)

)
(:action Prim194Action-1
:parameters ()
:precondition
(and
(not (RIGHTOF5-ROBOT))
)
:effect
(and
(Flag194prime-)
(not (Flag194-))
)

)
(:action Prim195Action-0
:parameters ()
:precondition
(and
(not (LEFTOF1-ROBOT))
)
:effect
(and
(Flag195prime-)
(not (Flag195-))
)

)
(:action Prim196Action-0
:parameters ()
:precondition
(and
(not (RIGHTOF11-ROBOT))
)
:effect
(and
(Flag196prime-)
(not (Flag196-))
)

)
(:action Prim196Action-1
:parameters ()
:precondition
(and
(not (LEFTOF1-ROBOT))
)
:effect
(and
(Flag196prime-)
(not (Flag196-))
)

)
(:action Prim197Action-0
:parameters ()
:precondition
(and
(not (RIGHTOF8-ROBOT))
)
:effect
(and
(Flag197prime-)
(not (Flag197-))
)

)
(:action Prim197Action-1
:parameters ()
:precondition
(and
(not (LEFTOF1-ROBOT))
)
:effect
(and
(Flag197prime-)
(not (Flag197-))
)

)
(:action Prim198Action-0
:parameters ()
:precondition
(and
(not (LEFTOF1-ROBOT))
)
:effect
(and
(Flag198prime-)
(not (Flag198-))
)

)
(:action Prim198Action-1
:parameters ()
:precondition
(and
(not (RIGHTOF6-ROBOT))
)
:effect
(and
(Flag198prime-)
(not (Flag198-))
)

)
(:action Prim199Action-0
:parameters ()
:precondition
(and
(not (RIGHTOF10-ROBOT))
)
:effect
(and
(Flag199prime-)
(not (Flag199-))
)

)
(:action Prim199Action-1
:parameters ()
:precondition
(and
(not (LEFTOF1-ROBOT))
)
:effect
(and
(Flag199prime-)
(not (Flag199-))
)

)
(:action Flag201Action
:parameters ()
:precondition
(and
(RIGHTOF13-ROBOT)
(LEFTOF2-ROBOT)
)
:effect
(and
(Flag201-)
(Flag201prime-)
)

)
(:action Flag202Action
:parameters ()
:precondition
(and
(LEFTOF2-ROBOT)
(RIGHTOF9-ROBOT)
)
:effect
(and
(Flag202-)
(Flag202prime-)
)

)
(:action Flag203Action
:parameters ()
:precondition
(and
(LEFTOF2-ROBOT)
(RIGHTOF1-ROBOT)
)
:effect
(and
(Flag203-)
(Flag203prime-)
)

)
(:action Flag204Action
:parameters ()
:precondition
(and
(RIGHTOF5-ROBOT)
(LEFTOF2-ROBOT)
)
:effect
(and
(Flag204-)
(Flag204prime-)
)

)
(:action Flag205Action
:parameters ()
:precondition
(and
(LEFTOF2-ROBOT)
(RIGHTOF7-ROBOT)
)
:effect
(and
(Flag205-)
(Flag205prime-)
)

)
(:action Flag206Action
:parameters ()
:precondition
(and
(RIGHTOF5-ROBOT)
(LEFTOF1-ROBOT)
)
:effect
(and
(Flag206-)
(Flag206prime-)
)

)
(:action Flag207Action
:parameters ()
:precondition
(and
(RIGHTOF3-ROBOT)
(LEFTOF2-ROBOT)
)
:effect
(and
(Flag207-)
(Flag207prime-)
)

)
(:action Flag208Action
:parameters ()
:precondition
(and
(RIGHTOF10-ROBOT)
(LEFTOF2-ROBOT)
)
:effect
(and
(Flag208-)
(Flag208prime-)
)

)
(:action Flag209Action
:parameters ()
:precondition
(and
(LEFTOF2-ROBOT)
(RIGHTOF12-ROBOT)
)
:effect
(and
(Flag209-)
(Flag209prime-)
)

)
(:action Flag210Action
:parameters ()
:precondition
(and
(RIGHTOF11-ROBOT)
(LEFTOF2-ROBOT)
)
:effect
(and
(Flag210-)
(Flag210prime-)
)

)
(:action Flag211Action
:parameters ()
:precondition
(and
(RIGHTOF8-ROBOT)
(LEFTOF2-ROBOT)
)
:effect
(and
(Flag211-)
(Flag211prime-)
)

)
(:action Flag212Action
:parameters ()
:precondition
(and
(RIGHTOF2-ROBOT)
(LEFTOF2-ROBOT)
)
:effect
(and
(Flag212-)
(Flag212prime-)
)

)
(:action Flag213Action
:parameters ()
:precondition
(and
(LEFTOF2-ROBOT)
(RIGHTOF6-ROBOT)
)
:effect
(and
(Flag213-)
(Flag213prime-)
)

)
(:action Flag214Action
:parameters ()
:precondition
(and
(LEFTOF2-ROBOT)
(RIGHTOF4-ROBOT)
)
:effect
(and
(Flag214-)
(Flag214prime-)
)

)
(:action Prim201Action-0
:parameters ()
:precondition
(and
(not (RIGHTOF13-ROBOT))
)
:effect
(and
(Flag201prime-)
(not (Flag201-))
)

)
(:action Prim201Action-1
:parameters ()
:precondition
(and
(not (LEFTOF2-ROBOT))
)
:effect
(and
(Flag201prime-)
(not (Flag201-))
)

)
(:action Prim202Action-0
:parameters ()
:precondition
(and
(not (LEFTOF2-ROBOT))
)
:effect
(and
(Flag202prime-)
(not (Flag202-))
)

)
(:action Prim202Action-1
:parameters ()
:precondition
(and
(not (RIGHTOF9-ROBOT))
)
:effect
(and
(Flag202prime-)
(not (Flag202-))
)

)
(:action Prim203Action-0
:parameters ()
:precondition
(and
(not (LEFTOF2-ROBOT))
)
:effect
(and
(Flag203prime-)
(not (Flag203-))
)

)
(:action Prim203Action-1
:parameters ()
:precondition
(and
(not (RIGHTOF1-ROBOT))
)
:effect
(and
(Flag203prime-)
(not (Flag203-))
)

)
(:action Prim204Action-0
:parameters ()
:precondition
(and
(not (RIGHTOF5-ROBOT))
)
:effect
(and
(Flag204prime-)
(not (Flag204-))
)

)
(:action Prim204Action-1
:parameters ()
:precondition
(and
(not (LEFTOF2-ROBOT))
)
:effect
(and
(Flag204prime-)
(not (Flag204-))
)

)
(:action Prim205Action-0
:parameters ()
:precondition
(and
(not (LEFTOF2-ROBOT))
)
:effect
(and
(Flag205prime-)
(not (Flag205-))
)

)
(:action Prim205Action-1
:parameters ()
:precondition
(and
(not (RIGHTOF7-ROBOT))
)
:effect
(and
(Flag205prime-)
(not (Flag205-))
)

)
(:action Prim206Action-0
:parameters ()
:precondition
(and
(not (RIGHTOF5-ROBOT))
)
:effect
(and
(Flag206prime-)
(not (Flag206-))
)

)
(:action Prim206Action-1
:parameters ()
:precondition
(and
(not (LEFTOF1-ROBOT))
)
:effect
(and
(Flag206prime-)
(not (Flag206-))
)

)
(:action Prim207Action-0
:parameters ()
:precondition
(and
(not (RIGHTOF3-ROBOT))
)
:effect
(and
(Flag207prime-)
(not (Flag207-))
)

)
(:action Prim207Action-1
:parameters ()
:precondition
(and
(not (LEFTOF2-ROBOT))
)
:effect
(and
(Flag207prime-)
(not (Flag207-))
)

)
(:action Prim208Action-0
:parameters ()
:precondition
(and
(not (RIGHTOF10-ROBOT))
)
:effect
(and
(Flag208prime-)
(not (Flag208-))
)

)
(:action Prim208Action-1
:parameters ()
:precondition
(and
(not (LEFTOF2-ROBOT))
)
:effect
(and
(Flag208prime-)
(not (Flag208-))
)

)
(:action Prim209Action-0
:parameters ()
:precondition
(and
(not (LEFTOF2-ROBOT))
)
:effect
(and
(Flag209prime-)
(not (Flag209-))
)

)
(:action Prim209Action-1
:parameters ()
:precondition
(and
(not (RIGHTOF12-ROBOT))
)
:effect
(and
(Flag209prime-)
(not (Flag209-))
)

)
(:action Prim210Action-0
:parameters ()
:precondition
(and
(not (RIGHTOF11-ROBOT))
)
:effect
(and
(Flag210prime-)
(not (Flag210-))
)

)
(:action Prim210Action-1
:parameters ()
:precondition
(and
(not (LEFTOF2-ROBOT))
)
:effect
(and
(Flag210prime-)
(not (Flag210-))
)

)
(:action Prim211Action-0
:parameters ()
:precondition
(and
(not (RIGHTOF8-ROBOT))
)
:effect
(and
(Flag211prime-)
(not (Flag211-))
)

)
(:action Prim211Action-1
:parameters ()
:precondition
(and
(not (LEFTOF2-ROBOT))
)
:effect
(and
(Flag211prime-)
(not (Flag211-))
)

)
(:action Prim212Action-1
:parameters ()
:precondition
(and
(not (LEFTOF2-ROBOT))
)
:effect
(and
(Flag212prime-)
(not (Flag212-))
)

)
(:action Prim213Action-0
:parameters ()
:precondition
(and
(not (LEFTOF2-ROBOT))
)
:effect
(and
(Flag213prime-)
(not (Flag213-))
)

)
(:action Prim213Action-1
:parameters ()
:precondition
(and
(not (RIGHTOF6-ROBOT))
)
:effect
(and
(Flag213prime-)
(not (Flag213-))
)

)
(:action Prim214Action-0
:parameters ()
:precondition
(and
(not (LEFTOF2-ROBOT))
)
:effect
(and
(Flag214prime-)
(not (Flag214-))
)

)
(:action Prim214Action-1
:parameters ()
:precondition
(and
(not (RIGHTOF4-ROBOT))
)
:effect
(and
(Flag214prime-)
(not (Flag214-))
)

)
(:action Flag216Action
:parameters ()
:precondition
(and
(LEFTOF3-ROBOT)
(RIGHTOF6-ROBOT)
)
:effect
(and
(Flag216-)
(Flag216prime-)
)

)
(:action Flag217Action
:parameters ()
:precondition
(and
(LEFTOF3-ROBOT)
(RIGHTOF4-ROBOT)
)
:effect
(and
(Flag217-)
(Flag217prime-)
)

)
(:action Flag218Action
:parameters ()
:precondition
(and
(LEFTOF3-ROBOT)
(RIGHTOF7-ROBOT)
)
:effect
(and
(Flag218-)
(Flag218prime-)
)

)
(:action Flag219Action
:parameters ()
:precondition
(and
(LEFTOF3-ROBOT)
(RIGHTOF12-ROBOT)
)
:effect
(and
(Flag219-)
(Flag219prime-)
)

)
(:action Flag220Action
:parameters ()
:precondition
(and
(LEFTOF3-ROBOT)
(RIGHTOF10-ROBOT)
)
:effect
(and
(Flag220-)
(Flag220prime-)
)

)
(:action Flag221Action
:parameters ()
:precondition
(and
(LEFTOF3-ROBOT)
(RIGHTOF8-ROBOT)
)
:effect
(and
(Flag221-)
(Flag221prime-)
)

)
(:action Flag222Action
:parameters ()
:precondition
(and
(LEFTOF3-ROBOT)
(RIGHTOF5-ROBOT)
)
:effect
(and
(Flag222-)
(Flag222prime-)
)

)
(:action Flag223Action
:parameters ()
:precondition
(and
(RIGHTOF3-ROBOT)
(LEFTOF3-ROBOT)
)
:effect
(and
(Flag223-)
(Flag223prime-)
)

)
(:action Flag224Action
:parameters ()
:precondition
(and
(LEFTOF3-ROBOT)
(RIGHTOF11-ROBOT)
)
:effect
(and
(Flag224-)
(Flag224prime-)
)

)
(:action Flag225Action
:parameters ()
:precondition
(and
(RIGHTOF13-ROBOT)
(LEFTOF3-ROBOT)
)
:effect
(and
(Flag225-)
(Flag225prime-)
)

)
(:action Flag226Action
:parameters ()
:precondition
(and
(LEFTOF3-ROBOT)
(RIGHTOF9-ROBOT)
)
:effect
(and
(Flag226-)
(Flag226prime-)
)

)
(:action Flag227Action
:parameters ()
:precondition
(and
(LEFTOF3-ROBOT)
(RIGHTOF2-ROBOT)
)
:effect
(and
(Flag227-)
(Flag227prime-)
)

)
(:action Prim216Action-0
:parameters ()
:precondition
(and
(not (LEFTOF3-ROBOT))
)
:effect
(and
(Flag216prime-)
(not (Flag216-))
)

)
(:action Prim216Action-1
:parameters ()
:precondition
(and
(not (RIGHTOF6-ROBOT))
)
:effect
(and
(Flag216prime-)
(not (Flag216-))
)

)
(:action Prim217Action-0
:parameters ()
:precondition
(and
(not (LEFTOF3-ROBOT))
)
:effect
(and
(Flag217prime-)
(not (Flag217-))
)

)
(:action Prim217Action-1
:parameters ()
:precondition
(and
(not (RIGHTOF4-ROBOT))
)
:effect
(and
(Flag217prime-)
(not (Flag217-))
)

)
(:action Prim218Action-0
:parameters ()
:precondition
(and
(not (LEFTOF3-ROBOT))
)
:effect
(and
(Flag218prime-)
(not (Flag218-))
)

)
(:action Prim218Action-1
:parameters ()
:precondition
(and
(not (RIGHTOF7-ROBOT))
)
:effect
(and
(Flag218prime-)
(not (Flag218-))
)

)
(:action Prim219Action-0
:parameters ()
:precondition
(and
(not (LEFTOF3-ROBOT))
)
:effect
(and
(Flag219prime-)
(not (Flag219-))
)

)
(:action Prim219Action-1
:parameters ()
:precondition
(and
(not (RIGHTOF12-ROBOT))
)
:effect
(and
(Flag219prime-)
(not (Flag219-))
)

)
(:action Prim220Action-0
:parameters ()
:precondition
(and
(not (LEFTOF3-ROBOT))
)
:effect
(and
(Flag220prime-)
(not (Flag220-))
)

)
(:action Prim220Action-1
:parameters ()
:precondition
(and
(not (RIGHTOF10-ROBOT))
)
:effect
(and
(Flag220prime-)
(not (Flag220-))
)

)
(:action Prim221Action-0
:parameters ()
:precondition
(and
(not (LEFTOF3-ROBOT))
)
:effect
(and
(Flag221prime-)
(not (Flag221-))
)

)
(:action Prim221Action-1
:parameters ()
:precondition
(and
(not (RIGHTOF8-ROBOT))
)
:effect
(and
(Flag221prime-)
(not (Flag221-))
)

)
(:action Prim222Action-0
:parameters ()
:precondition
(and
(not (LEFTOF3-ROBOT))
)
:effect
(and
(Flag222prime-)
(not (Flag222-))
)

)
(:action Prim222Action-1
:parameters ()
:precondition
(and
(not (RIGHTOF5-ROBOT))
)
:effect
(and
(Flag222prime-)
(not (Flag222-))
)

)
(:action Prim223Action-0
:parameters ()
:precondition
(and
(not (RIGHTOF3-ROBOT))
)
:effect
(and
(Flag223prime-)
(not (Flag223-))
)

)
(:action Prim223Action-1
:parameters ()
:precondition
(and
(not (LEFTOF3-ROBOT))
)
:effect
(and
(Flag223prime-)
(not (Flag223-))
)

)
(:action Prim224Action-0
:parameters ()
:precondition
(and
(not (LEFTOF3-ROBOT))
)
:effect
(and
(Flag224prime-)
(not (Flag224-))
)

)
(:action Prim224Action-1
:parameters ()
:precondition
(and
(not (RIGHTOF11-ROBOT))
)
:effect
(and
(Flag224prime-)
(not (Flag224-))
)

)
(:action Prim225Action-0
:parameters ()
:precondition
(and
(not (RIGHTOF13-ROBOT))
)
:effect
(and
(Flag225prime-)
(not (Flag225-))
)

)
(:action Prim225Action-1
:parameters ()
:precondition
(and
(not (LEFTOF3-ROBOT))
)
:effect
(and
(Flag225prime-)
(not (Flag225-))
)

)
(:action Prim226Action-0
:parameters ()
:precondition
(and
(not (LEFTOF3-ROBOT))
)
:effect
(and
(Flag226prime-)
(not (Flag226-))
)

)
(:action Prim226Action-1
:parameters ()
:precondition
(and
(not (RIGHTOF9-ROBOT))
)
:effect
(and
(Flag226prime-)
(not (Flag226-))
)

)
(:action Prim227Action-0
:parameters ()
:precondition
(and
(not (LEFTOF3-ROBOT))
)
:effect
(and
(Flag227prime-)
(not (Flag227-))
)

)
(:action Flag229Action
:parameters ()
:precondition
(and
(LEFTOF3-ROBOT)
(RIGHTOF13-ROBOT)
)
:effect
(and
(Flag229-)
(Flag229prime-)
)

)
(:action Flag230Action
:parameters ()
:precondition
(and
(LEFTOF4-ROBOT)
(RIGHTOF4-ROBOT)
)
:effect
(and
(Flag230-)
(Flag230prime-)
)

)
(:action Flag231Action
:parameters ()
:precondition
(and
(LEFTOF4-ROBOT)
(RIGHTOF7-ROBOT)
)
:effect
(and
(Flag231-)
(Flag231prime-)
)

)
(:action Flag232Action
:parameters ()
:precondition
(and
(LEFTOF3-ROBOT)
(RIGHTOF3-ROBOT)
)
:effect
(and
(Flag232-)
(Flag232prime-)
)

)
(:action Flag233Action
:parameters ()
:precondition
(and
(RIGHTOF8-ROBOT)
(LEFTOF4-ROBOT)
)
:effect
(and
(Flag233-)
(Flag233prime-)
)

)
(:action Flag234Action
:parameters ()
:precondition
(and
(LEFTOF4-ROBOT)
(RIGHTOF12-ROBOT)
)
:effect
(and
(Flag234-)
(Flag234prime-)
)

)
(:action Flag235Action
:parameters ()
:precondition
(and
(RIGHTOF10-ROBOT)
(LEFTOF4-ROBOT)
)
:effect
(and
(Flag235-)
(Flag235prime-)
)

)
(:action Flag236Action
:parameters ()
:precondition
(and
(LEFTOF4-ROBOT)
(RIGHTOF9-ROBOT)
)
:effect
(and
(Flag236-)
(Flag236prime-)
)

)
(:action Flag237Action
:parameters ()
:precondition
(and
(LEFTOF4-ROBOT)
(RIGHTOF6-ROBOT)
)
:effect
(and
(Flag237-)
(Flag237prime-)
)

)
(:action Flag238Action
:parameters ()
:precondition
(and
(LEFTOF4-ROBOT)
(RIGHTOF5-ROBOT)
)
:effect
(and
(Flag238-)
(Flag238prime-)
)

)
(:action Flag239Action
:parameters ()
:precondition
(and
(RIGHTOF13-ROBOT)
(LEFTOF4-ROBOT)
)
:effect
(and
(Flag239-)
(Flag239prime-)
)

)
(:action Flag240Action
:parameters ()
:precondition
(and
(RIGHTOF3-ROBOT)
(LEFTOF4-ROBOT)
)
:effect
(and
(Flag240-)
(Flag240prime-)
)

)
(:action Flag241Action
:parameters ()
:precondition
(and
(LEFTOF4-ROBOT)
(RIGHTOF11-ROBOT)
)
:effect
(and
(Flag241-)
(Flag241prime-)
)

)
(:action Prim229Action-0
:parameters ()
:precondition
(and
(not (LEFTOF3-ROBOT))
)
:effect
(and
(Flag229prime-)
(not (Flag229-))
)

)
(:action Prim229Action-1
:parameters ()
:precondition
(and
(not (RIGHTOF13-ROBOT))
)
:effect
(and
(Flag229prime-)
(not (Flag229-))
)

)
(:action Prim230Action-0
:parameters ()
:precondition
(and
(not (LEFTOF4-ROBOT))
)
:effect
(and
(Flag230prime-)
(not (Flag230-))
)

)
(:action Prim230Action-1
:parameters ()
:precondition
(and
(not (RIGHTOF4-ROBOT))
)
:effect
(and
(Flag230prime-)
(not (Flag230-))
)

)
(:action Prim231Action-0
:parameters ()
:precondition
(and
(not (LEFTOF4-ROBOT))
)
:effect
(and
(Flag231prime-)
(not (Flag231-))
)

)
(:action Prim231Action-1
:parameters ()
:precondition
(and
(not (RIGHTOF7-ROBOT))
)
:effect
(and
(Flag231prime-)
(not (Flag231-))
)

)
(:action Prim232Action-0
:parameters ()
:precondition
(and
(not (LEFTOF3-ROBOT))
)
:effect
(and
(Flag232prime-)
(not (Flag232-))
)

)
(:action Prim232Action-1
:parameters ()
:precondition
(and
(not (RIGHTOF3-ROBOT))
)
:effect
(and
(Flag232prime-)
(not (Flag232-))
)

)
(:action Prim233Action-0
:parameters ()
:precondition
(and
(not (RIGHTOF8-ROBOT))
)
:effect
(and
(Flag233prime-)
(not (Flag233-))
)

)
(:action Prim233Action-1
:parameters ()
:precondition
(and
(not (LEFTOF4-ROBOT))
)
:effect
(and
(Flag233prime-)
(not (Flag233-))
)

)
(:action Prim234Action-0
:parameters ()
:precondition
(and
(not (LEFTOF4-ROBOT))
)
:effect
(and
(Flag234prime-)
(not (Flag234-))
)

)
(:action Prim234Action-1
:parameters ()
:precondition
(and
(not (RIGHTOF12-ROBOT))
)
:effect
(and
(Flag234prime-)
(not (Flag234-))
)

)
(:action Prim235Action-0
:parameters ()
:precondition
(and
(not (RIGHTOF10-ROBOT))
)
:effect
(and
(Flag235prime-)
(not (Flag235-))
)

)
(:action Prim235Action-1
:parameters ()
:precondition
(and
(not (LEFTOF4-ROBOT))
)
:effect
(and
(Flag235prime-)
(not (Flag235-))
)

)
(:action Prim236Action-0
:parameters ()
:precondition
(and
(not (LEFTOF4-ROBOT))
)
:effect
(and
(Flag236prime-)
(not (Flag236-))
)

)
(:action Prim236Action-1
:parameters ()
:precondition
(and
(not (RIGHTOF9-ROBOT))
)
:effect
(and
(Flag236prime-)
(not (Flag236-))
)

)
(:action Prim237Action-0
:parameters ()
:precondition
(and
(not (LEFTOF4-ROBOT))
)
:effect
(and
(Flag237prime-)
(not (Flag237-))
)

)
(:action Prim237Action-1
:parameters ()
:precondition
(and
(not (RIGHTOF6-ROBOT))
)
:effect
(and
(Flag237prime-)
(not (Flag237-))
)

)
(:action Prim238Action-0
:parameters ()
:precondition
(and
(not (LEFTOF4-ROBOT))
)
:effect
(and
(Flag238prime-)
(not (Flag238-))
)

)
(:action Prim238Action-1
:parameters ()
:precondition
(and
(not (RIGHTOF5-ROBOT))
)
:effect
(and
(Flag238prime-)
(not (Flag238-))
)

)
(:action Prim239Action-0
:parameters ()
:precondition
(and
(not (RIGHTOF13-ROBOT))
)
:effect
(and
(Flag239prime-)
(not (Flag239-))
)

)
(:action Prim239Action-1
:parameters ()
:precondition
(and
(not (LEFTOF4-ROBOT))
)
:effect
(and
(Flag239prime-)
(not (Flag239-))
)

)
(:action Prim240Action-0
:parameters ()
:precondition
(and
(not (RIGHTOF3-ROBOT))
)
:effect
(and
(Flag240prime-)
(not (Flag240-))
)

)
(:action Prim240Action-1
:parameters ()
:precondition
(and
(not (LEFTOF4-ROBOT))
)
:effect
(and
(Flag240prime-)
(not (Flag240-))
)

)
(:action Prim241Action-0
:parameters ()
:precondition
(and
(not (LEFTOF4-ROBOT))
)
:effect
(and
(Flag241prime-)
(not (Flag241-))
)

)
(:action Prim241Action-1
:parameters ()
:precondition
(and
(not (RIGHTOF11-ROBOT))
)
:effect
(and
(Flag241prime-)
(not (Flag241-))
)

)
(:action Flag243Action
:parameters ()
:precondition
(and
(RIGHTOF9-ROBOT)
(LEFTOF5-ROBOT)
)
:effect
(and
(Flag243-)
(Flag243prime-)
)

)
(:action Flag244Action
:parameters ()
:precondition
(and
(RIGHTOF7-ROBOT)
(LEFTOF5-ROBOT)
)
:effect
(and
(Flag244-)
(Flag244prime-)
)

)
(:action Flag245Action
:parameters ()
:precondition
(and
(RIGHTOF11-ROBOT)
(LEFTOF5-ROBOT)
)
:effect
(and
(Flag245-)
(Flag245prime-)
)

)
(:action Flag246Action
:parameters ()
:precondition
(and
(RIGHTOF13-ROBOT)
(LEFTOF5-ROBOT)
)
:effect
(and
(Flag246-)
(Flag246prime-)
)

)
(:action Flag247Action
:parameters ()
:precondition
(and
(LEFTOF4-ROBOT)
(RIGHTOF10-ROBOT)
)
:effect
(and
(Flag247-)
(Flag247prime-)
)

)
(:action Flag248Action
:parameters ()
:precondition
(and
(RIGHTOF4-ROBOT)
(LEFTOF5-ROBOT)
)
:effect
(and
(Flag248-)
(Flag248prime-)
)

)
(:action Flag249Action
:parameters ()
:precondition
(and
(LEFTOF4-ROBOT)
(RIGHTOF8-ROBOT)
)
:effect
(and
(Flag249-)
(Flag249prime-)
)

)
(:action Flag250Action
:parameters ()
:precondition
(and
(LEFTOF5-ROBOT)
(RIGHTOF6-ROBOT)
)
:effect
(and
(Flag250-)
(Flag250prime-)
)

)
(:action Flag251Action
:parameters ()
:precondition
(and
(RIGHTOF10-ROBOT)
(LEFTOF5-ROBOT)
)
:effect
(and
(Flag251-)
(Flag251prime-)
)

)
(:action Flag252Action
:parameters ()
:precondition
(and
(LEFTOF5-ROBOT)
(RIGHTOF12-ROBOT)
)
:effect
(and
(Flag252-)
(Flag252prime-)
)

)
(:action Flag253Action
:parameters ()
:precondition
(and
(RIGHTOF8-ROBOT)
(LEFTOF5-ROBOT)
)
:effect
(and
(Flag253-)
(Flag253prime-)
)

)
(:action Flag254Action
:parameters ()
:precondition
(and
(RIGHTOF5-ROBOT)
(LEFTOF5-ROBOT)
)
:effect
(and
(Flag254-)
(Flag254prime-)
)

)
(:action Prim243Action-0
:parameters ()
:precondition
(and
(not (RIGHTOF9-ROBOT))
)
:effect
(and
(Flag243prime-)
(not (Flag243-))
)

)
(:action Prim243Action-1
:parameters ()
:precondition
(and
(not (LEFTOF5-ROBOT))
)
:effect
(and
(Flag243prime-)
(not (Flag243-))
)

)
(:action Prim244Action-0
:parameters ()
:precondition
(and
(not (RIGHTOF7-ROBOT))
)
:effect
(and
(Flag244prime-)
(not (Flag244-))
)

)
(:action Prim244Action-1
:parameters ()
:precondition
(and
(not (LEFTOF5-ROBOT))
)
:effect
(and
(Flag244prime-)
(not (Flag244-))
)

)
(:action Prim245Action-0
:parameters ()
:precondition
(and
(not (RIGHTOF11-ROBOT))
)
:effect
(and
(Flag245prime-)
(not (Flag245-))
)

)
(:action Prim245Action-1
:parameters ()
:precondition
(and
(not (LEFTOF5-ROBOT))
)
:effect
(and
(Flag245prime-)
(not (Flag245-))
)

)
(:action Prim246Action-0
:parameters ()
:precondition
(and
(not (RIGHTOF13-ROBOT))
)
:effect
(and
(Flag246prime-)
(not (Flag246-))
)

)
(:action Prim246Action-1
:parameters ()
:precondition
(and
(not (LEFTOF5-ROBOT))
)
:effect
(and
(Flag246prime-)
(not (Flag246-))
)

)
(:action Prim247Action-0
:parameters ()
:precondition
(and
(not (LEFTOF4-ROBOT))
)
:effect
(and
(Flag247prime-)
(not (Flag247-))
)

)
(:action Prim247Action-1
:parameters ()
:precondition
(and
(not (RIGHTOF10-ROBOT))
)
:effect
(and
(Flag247prime-)
(not (Flag247-))
)

)
(:action Prim248Action-0
:parameters ()
:precondition
(and
(not (RIGHTOF4-ROBOT))
)
:effect
(and
(Flag248prime-)
(not (Flag248-))
)

)
(:action Prim248Action-1
:parameters ()
:precondition
(and
(not (LEFTOF5-ROBOT))
)
:effect
(and
(Flag248prime-)
(not (Flag248-))
)

)
(:action Prim249Action-0
:parameters ()
:precondition
(and
(not (LEFTOF4-ROBOT))
)
:effect
(and
(Flag249prime-)
(not (Flag249-))
)

)
(:action Prim249Action-1
:parameters ()
:precondition
(and
(not (RIGHTOF8-ROBOT))
)
:effect
(and
(Flag249prime-)
(not (Flag249-))
)

)
(:action Prim250Action-0
:parameters ()
:precondition
(and
(not (LEFTOF5-ROBOT))
)
:effect
(and
(Flag250prime-)
(not (Flag250-))
)

)
(:action Prim250Action-1
:parameters ()
:precondition
(and
(not (RIGHTOF6-ROBOT))
)
:effect
(and
(Flag250prime-)
(not (Flag250-))
)

)
(:action Prim251Action-0
:parameters ()
:precondition
(and
(not (RIGHTOF10-ROBOT))
)
:effect
(and
(Flag251prime-)
(not (Flag251-))
)

)
(:action Prim251Action-1
:parameters ()
:precondition
(and
(not (LEFTOF5-ROBOT))
)
:effect
(and
(Flag251prime-)
(not (Flag251-))
)

)
(:action Prim252Action-0
:parameters ()
:precondition
(and
(not (LEFTOF5-ROBOT))
)
:effect
(and
(Flag252prime-)
(not (Flag252-))
)

)
(:action Prim252Action-1
:parameters ()
:precondition
(and
(not (RIGHTOF12-ROBOT))
)
:effect
(and
(Flag252prime-)
(not (Flag252-))
)

)
(:action Prim253Action-0
:parameters ()
:precondition
(and
(not (RIGHTOF8-ROBOT))
)
:effect
(and
(Flag253prime-)
(not (Flag253-))
)

)
(:action Prim253Action-1
:parameters ()
:precondition
(and
(not (LEFTOF5-ROBOT))
)
:effect
(and
(Flag253prime-)
(not (Flag253-))
)

)
(:action Prim254Action-0
:parameters ()
:precondition
(and
(not (RIGHTOF5-ROBOT))
)
:effect
(and
(Flag254prime-)
(not (Flag254-))
)

)
(:action Prim254Action-1
:parameters ()
:precondition
(and
(not (LEFTOF5-ROBOT))
)
:effect
(and
(Flag254prime-)
(not (Flag254-))
)

)
(:action Flag256Action
:parameters ()
:precondition
(and
(RIGHTOF12-ROBOT)
(LEFTOF6-ROBOT)
)
:effect
(and
(Flag256-)
(Flag256prime-)
)

)
(:action Flag257Action
:parameters ()
:precondition
(and
(RIGHTOF8-ROBOT)
(LEFTOF6-ROBOT)
)
:effect
(and
(Flag257-)
(Flag257prime-)
)

)
(:action Flag258Action
:parameters ()
:precondition
(and
(RIGHTOF11-ROBOT)
(LEFTOF6-ROBOT)
)
:effect
(and
(Flag258-)
(Flag258prime-)
)

)
(:action Flag259Action
:parameters ()
:precondition
(and
(RIGHTOF5-ROBOT)
(LEFTOF6-ROBOT)
)
:effect
(and
(Flag259-)
(Flag259prime-)
)

)
(:action Flag260Action
:parameters ()
:precondition
(and
(RIGHTOF7-ROBOT)
(LEFTOF6-ROBOT)
)
:effect
(and
(Flag260-)
(Flag260prime-)
)

)
(:action Flag261Action
:parameters ()
:precondition
(and
(RIGHTOF10-ROBOT)
(LEFTOF6-ROBOT)
)
:effect
(and
(Flag261-)
(Flag261prime-)
)

)
(:action Flag262Action
:parameters ()
:precondition
(and
(RIGHTOF6-ROBOT)
(LEFTOF6-ROBOT)
)
:effect
(and
(Flag262-)
(Flag262prime-)
)

)
(:action Flag263Action
:parameters ()
:precondition
(and
(RIGHTOF13-ROBOT)
(LEFTOF6-ROBOT)
)
:effect
(and
(Flag263-)
(Flag263prime-)
)

)
(:action Flag264Action
:parameters ()
:precondition
(and
(RIGHTOF9-ROBOT)
(LEFTOF6-ROBOT)
)
:effect
(and
(Flag264-)
(Flag264prime-)
)

)
(:action Prim256Action-0
:parameters ()
:precondition
(and
(not (RIGHTOF12-ROBOT))
)
:effect
(and
(Flag256prime-)
(not (Flag256-))
)

)
(:action Prim256Action-1
:parameters ()
:precondition
(and
(not (LEFTOF6-ROBOT))
)
:effect
(and
(Flag256prime-)
(not (Flag256-))
)

)
(:action Prim257Action-0
:parameters ()
:precondition
(and
(not (RIGHTOF8-ROBOT))
)
:effect
(and
(Flag257prime-)
(not (Flag257-))
)

)
(:action Prim257Action-1
:parameters ()
:precondition
(and
(not (LEFTOF6-ROBOT))
)
:effect
(and
(Flag257prime-)
(not (Flag257-))
)

)
(:action Prim258Action-0
:parameters ()
:precondition
(and
(not (RIGHTOF11-ROBOT))
)
:effect
(and
(Flag258prime-)
(not (Flag258-))
)

)
(:action Prim258Action-1
:parameters ()
:precondition
(and
(not (LEFTOF6-ROBOT))
)
:effect
(and
(Flag258prime-)
(not (Flag258-))
)

)
(:action Prim259Action-0
:parameters ()
:precondition
(and
(not (RIGHTOF5-ROBOT))
)
:effect
(and
(Flag259prime-)
(not (Flag259-))
)

)
(:action Prim259Action-1
:parameters ()
:precondition
(and
(not (LEFTOF6-ROBOT))
)
:effect
(and
(Flag259prime-)
(not (Flag259-))
)

)
(:action Prim260Action-0
:parameters ()
:precondition
(and
(not (RIGHTOF7-ROBOT))
)
:effect
(and
(Flag260prime-)
(not (Flag260-))
)

)
(:action Prim260Action-1
:parameters ()
:precondition
(and
(not (LEFTOF6-ROBOT))
)
:effect
(and
(Flag260prime-)
(not (Flag260-))
)

)
(:action Prim261Action-0
:parameters ()
:precondition
(and
(not (RIGHTOF10-ROBOT))
)
:effect
(and
(Flag261prime-)
(not (Flag261-))
)

)
(:action Prim261Action-1
:parameters ()
:precondition
(and
(not (LEFTOF6-ROBOT))
)
:effect
(and
(Flag261prime-)
(not (Flag261-))
)

)
(:action Prim262Action-0
:parameters ()
:precondition
(and
(not (RIGHTOF6-ROBOT))
)
:effect
(and
(Flag262prime-)
(not (Flag262-))
)

)
(:action Prim262Action-1
:parameters ()
:precondition
(and
(not (LEFTOF6-ROBOT))
)
:effect
(and
(Flag262prime-)
(not (Flag262-))
)

)
(:action Prim263Action-0
:parameters ()
:precondition
(and
(not (RIGHTOF13-ROBOT))
)
:effect
(and
(Flag263prime-)
(not (Flag263-))
)

)
(:action Prim263Action-1
:parameters ()
:precondition
(and
(not (LEFTOF6-ROBOT))
)
:effect
(and
(Flag263prime-)
(not (Flag263-))
)

)
(:action Prim264Action-0
:parameters ()
:precondition
(and
(not (RIGHTOF9-ROBOT))
)
:effect
(and
(Flag264prime-)
(not (Flag264-))
)

)
(:action Prim264Action-1
:parameters ()
:precondition
(and
(not (LEFTOF6-ROBOT))
)
:effect
(and
(Flag264prime-)
(not (Flag264-))
)

)
(:action Flag266Action
:parameters ()
:precondition
(and
(RIGHTOF11-ROBOT)
(LEFTOF7-ROBOT)
)
:effect
(and
(Flag266-)
(Flag266prime-)
)

)
(:action Flag267Action
:parameters ()
:precondition
(and
(RIGHTOF8-ROBOT)
(LEFTOF7-ROBOT)
)
:effect
(and
(Flag267-)
(Flag267prime-)
)

)
(:action Flag268Action
:parameters ()
:precondition
(and
(LEFTOF7-ROBOT)
(RIGHTOF7-ROBOT)
)
:effect
(and
(Flag268-)
(Flag268prime-)
)

)
(:action Flag269Action
:parameters ()
:precondition
(and
(RIGHTOF13-ROBOT)
(LEFTOF7-ROBOT)
)
:effect
(and
(Flag269-)
(Flag269prime-)
)

)
(:action Flag270Action
:parameters ()
:precondition
(and
(LEFTOF6-ROBOT)
(RIGHTOF6-ROBOT)
)
:effect
(and
(Flag270-)
(Flag270prime-)
)

)
(:action Flag271Action
:parameters ()
:precondition
(and
(LEFTOF7-ROBOT)
(RIGHTOF12-ROBOT)
)
:effect
(and
(Flag271-)
(Flag271prime-)
)

)
(:action Flag272Action
:parameters ()
:precondition
(and
(LEFTOF7-ROBOT)
(RIGHTOF9-ROBOT)
)
:effect
(and
(Flag272-)
(Flag272prime-)
)

)
(:action Flag273Action
:parameters ()
:precondition
(and
(LEFTOF7-ROBOT)
(RIGHTOF6-ROBOT)
)
:effect
(and
(Flag273-)
(Flag273prime-)
)

)
(:action Flag274Action
:parameters ()
:precondition
(and
(LEFTOF6-ROBOT)
(RIGHTOF12-ROBOT)
)
:effect
(and
(Flag274-)
(Flag274prime-)
)

)
(:action Flag275Action
:parameters ()
:precondition
(and
(RIGHTOF10-ROBOT)
(LEFTOF7-ROBOT)
)
:effect
(and
(Flag275-)
(Flag275prime-)
)

)
(:action Prim266Action-0
:parameters ()
:precondition
(and
(not (RIGHTOF11-ROBOT))
)
:effect
(and
(Flag266prime-)
(not (Flag266-))
)

)
(:action Prim266Action-1
:parameters ()
:precondition
(and
(not (LEFTOF7-ROBOT))
)
:effect
(and
(Flag266prime-)
(not (Flag266-))
)

)
(:action Prim267Action-0
:parameters ()
:precondition
(and
(not (RIGHTOF8-ROBOT))
)
:effect
(and
(Flag267prime-)
(not (Flag267-))
)

)
(:action Prim267Action-1
:parameters ()
:precondition
(and
(not (LEFTOF7-ROBOT))
)
:effect
(and
(Flag267prime-)
(not (Flag267-))
)

)
(:action Prim268Action-0
:parameters ()
:precondition
(and
(not (LEFTOF7-ROBOT))
)
:effect
(and
(Flag268prime-)
(not (Flag268-))
)

)
(:action Prim268Action-1
:parameters ()
:precondition
(and
(not (RIGHTOF7-ROBOT))
)
:effect
(and
(Flag268prime-)
(not (Flag268-))
)

)
(:action Prim269Action-0
:parameters ()
:precondition
(and
(not (RIGHTOF13-ROBOT))
)
:effect
(and
(Flag269prime-)
(not (Flag269-))
)

)
(:action Prim269Action-1
:parameters ()
:precondition
(and
(not (LEFTOF7-ROBOT))
)
:effect
(and
(Flag269prime-)
(not (Flag269-))
)

)
(:action Prim270Action-0
:parameters ()
:precondition
(and
(not (LEFTOF6-ROBOT))
)
:effect
(and
(Flag270prime-)
(not (Flag270-))
)

)
(:action Prim270Action-1
:parameters ()
:precondition
(and
(not (RIGHTOF6-ROBOT))
)
:effect
(and
(Flag270prime-)
(not (Flag270-))
)

)
(:action Prim271Action-0
:parameters ()
:precondition
(and
(not (LEFTOF7-ROBOT))
)
:effect
(and
(Flag271prime-)
(not (Flag271-))
)

)
(:action Prim271Action-1
:parameters ()
:precondition
(and
(not (RIGHTOF12-ROBOT))
)
:effect
(and
(Flag271prime-)
(not (Flag271-))
)

)
(:action Prim272Action-0
:parameters ()
:precondition
(and
(not (LEFTOF7-ROBOT))
)
:effect
(and
(Flag272prime-)
(not (Flag272-))
)

)
(:action Prim272Action-1
:parameters ()
:precondition
(and
(not (RIGHTOF9-ROBOT))
)
:effect
(and
(Flag272prime-)
(not (Flag272-))
)

)
(:action Prim273Action-0
:parameters ()
:precondition
(and
(not (LEFTOF7-ROBOT))
)
:effect
(and
(Flag273prime-)
(not (Flag273-))
)

)
(:action Prim273Action-1
:parameters ()
:precondition
(and
(not (RIGHTOF6-ROBOT))
)
:effect
(and
(Flag273prime-)
(not (Flag273-))
)

)
(:action Prim274Action-0
:parameters ()
:precondition
(and
(not (LEFTOF6-ROBOT))
)
:effect
(and
(Flag274prime-)
(not (Flag274-))
)

)
(:action Prim274Action-1
:parameters ()
:precondition
(and
(not (RIGHTOF12-ROBOT))
)
:effect
(and
(Flag274prime-)
(not (Flag274-))
)

)
(:action Prim275Action-0
:parameters ()
:precondition
(and
(not (RIGHTOF10-ROBOT))
)
:effect
(and
(Flag275prime-)
(not (Flag275-))
)

)
(:action Prim275Action-1
:parameters ()
:precondition
(and
(not (LEFTOF7-ROBOT))
)
:effect
(and
(Flag275prime-)
(not (Flag275-))
)

)
(:action Flag277Action
:parameters ()
:precondition
(and
(RIGHTOF7-ROBOT)
(LEFTOF8-ROBOT)
)
:effect
(and
(Flag277-)
(Flag277prime-)
)

)
(:action Flag278Action
:parameters ()
:precondition
(and
(LEFTOF8-ROBOT)
(RIGHTOF9-ROBOT)
)
:effect
(and
(Flag278-)
(Flag278prime-)
)

)
(:action Flag279Action
:parameters ()
:precondition
(and
(RIGHTOF13-ROBOT)
(LEFTOF8-ROBOT)
)
:effect
(and
(Flag279-)
(Flag279prime-)
)

)
(:action Flag280Action
:parameters ()
:precondition
(and
(RIGHTOF10-ROBOT)
(LEFTOF8-ROBOT)
)
:effect
(and
(Flag280-)
(Flag280prime-)
)

)
(:action Flag281Action
:parameters ()
:precondition
(and
(RIGHTOF8-ROBOT)
(LEFTOF8-ROBOT)
)
:effect
(and
(Flag281-)
(Flag281prime-)
)

)
(:action Flag282Action
:parameters ()
:precondition
(and
(RIGHTOF7-ROBOT)
(LEFTOF7-ROBOT)
)
:effect
(and
(Flag282-)
(Flag282prime-)
)

)
(:action Flag283Action
:parameters ()
:precondition
(and
(LEFTOF8-ROBOT)
(RIGHTOF12-ROBOT)
)
:effect
(and
(Flag283-)
(Flag283prime-)
)

)
(:action Flag284Action
:parameters ()
:precondition
(and
(RIGHTOF11-ROBOT)
(LEFTOF8-ROBOT)
)
:effect
(and
(Flag284-)
(Flag284prime-)
)

)
(:action Prim277Action-0
:parameters ()
:precondition
(and
(not (RIGHTOF7-ROBOT))
)
:effect
(and
(Flag277prime-)
(not (Flag277-))
)

)
(:action Prim277Action-1
:parameters ()
:precondition
(and
(not (LEFTOF8-ROBOT))
)
:effect
(and
(Flag277prime-)
(not (Flag277-))
)

)
(:action Prim278Action-0
:parameters ()
:precondition
(and
(not (LEFTOF8-ROBOT))
)
:effect
(and
(Flag278prime-)
(not (Flag278-))
)

)
(:action Prim278Action-1
:parameters ()
:precondition
(and
(not (RIGHTOF9-ROBOT))
)
:effect
(and
(Flag278prime-)
(not (Flag278-))
)

)
(:action Prim279Action-0
:parameters ()
:precondition
(and
(not (RIGHTOF13-ROBOT))
)
:effect
(and
(Flag279prime-)
(not (Flag279-))
)

)
(:action Prim279Action-1
:parameters ()
:precondition
(and
(not (LEFTOF8-ROBOT))
)
:effect
(and
(Flag279prime-)
(not (Flag279-))
)

)
(:action Prim280Action-0
:parameters ()
:precondition
(and
(not (RIGHTOF10-ROBOT))
)
:effect
(and
(Flag280prime-)
(not (Flag280-))
)

)
(:action Prim280Action-1
:parameters ()
:precondition
(and
(not (LEFTOF8-ROBOT))
)
:effect
(and
(Flag280prime-)
(not (Flag280-))
)

)
(:action Prim281Action-0
:parameters ()
:precondition
(and
(not (RIGHTOF8-ROBOT))
)
:effect
(and
(Flag281prime-)
(not (Flag281-))
)

)
(:action Prim281Action-1
:parameters ()
:precondition
(and
(not (LEFTOF8-ROBOT))
)
:effect
(and
(Flag281prime-)
(not (Flag281-))
)

)
(:action Prim282Action-0
:parameters ()
:precondition
(and
(not (RIGHTOF7-ROBOT))
)
:effect
(and
(Flag282prime-)
(not (Flag282-))
)

)
(:action Prim282Action-1
:parameters ()
:precondition
(and
(not (LEFTOF7-ROBOT))
)
:effect
(and
(Flag282prime-)
(not (Flag282-))
)

)
(:action Prim283Action-0
:parameters ()
:precondition
(and
(not (LEFTOF8-ROBOT))
)
:effect
(and
(Flag283prime-)
(not (Flag283-))
)

)
(:action Prim283Action-1
:parameters ()
:precondition
(and
(not (RIGHTOF12-ROBOT))
)
:effect
(and
(Flag283prime-)
(not (Flag283-))
)

)
(:action Prim284Action-0
:parameters ()
:precondition
(and
(not (RIGHTOF11-ROBOT))
)
:effect
(and
(Flag284prime-)
(not (Flag284-))
)

)
(:action Prim284Action-1
:parameters ()
:precondition
(and
(not (LEFTOF8-ROBOT))
)
:effect
(and
(Flag284prime-)
(not (Flag284-))
)

)
(:action Flag286Action
:parameters ()
:precondition
(and
(RIGHTOF13-ROBOT)
(LEFTOF9-ROBOT)
)
:effect
(and
(Flag286-)
(Flag286prime-)
)

)
(:action Flag287Action
:parameters ()
:precondition
(and
(RIGHTOF8-ROBOT)
(LEFTOF9-ROBOT)
)
:effect
(and
(Flag287-)
(Flag287prime-)
)

)
(:action Flag288Action
:parameters ()
:precondition
(and
(LEFTOF9-ROBOT)
(RIGHTOF9-ROBOT)
)
:effect
(and
(Flag288-)
(Flag288prime-)
)

)
(:action Flag289Action
:parameters ()
:precondition
(and
(RIGHTOF10-ROBOT)
(LEFTOF9-ROBOT)
)
:effect
(and
(Flag289-)
(Flag289prime-)
)

)
(:action Flag290Action
:parameters ()
:precondition
(and
(RIGHTOF11-ROBOT)
(LEFTOF9-ROBOT)
)
:effect
(and
(Flag290-)
(Flag290prime-)
)

)
(:action Flag291Action
:parameters ()
:precondition
(and
(LEFTOF9-ROBOT)
(RIGHTOF12-ROBOT)
)
:effect
(and
(Flag291-)
(Flag291prime-)
)

)
(:action Prim286Action-0
:parameters ()
:precondition
(and
(not (RIGHTOF13-ROBOT))
)
:effect
(and
(Flag286prime-)
(not (Flag286-))
)

)
(:action Prim286Action-1
:parameters ()
:precondition
(and
(not (LEFTOF9-ROBOT))
)
:effect
(and
(Flag286prime-)
(not (Flag286-))
)

)
(:action Prim287Action-0
:parameters ()
:precondition
(and
(not (RIGHTOF8-ROBOT))
)
:effect
(and
(Flag287prime-)
(not (Flag287-))
)

)
(:action Prim287Action-1
:parameters ()
:precondition
(and
(not (LEFTOF9-ROBOT))
)
:effect
(and
(Flag287prime-)
(not (Flag287-))
)

)
(:action Prim288Action-0
:parameters ()
:precondition
(and
(not (LEFTOF9-ROBOT))
)
:effect
(and
(Flag288prime-)
(not (Flag288-))
)

)
(:action Prim288Action-1
:parameters ()
:precondition
(and
(not (RIGHTOF9-ROBOT))
)
:effect
(and
(Flag288prime-)
(not (Flag288-))
)

)
(:action Prim289Action-0
:parameters ()
:precondition
(and
(not (RIGHTOF10-ROBOT))
)
:effect
(and
(Flag289prime-)
(not (Flag289-))
)

)
(:action Prim289Action-1
:parameters ()
:precondition
(and
(not (LEFTOF9-ROBOT))
)
:effect
(and
(Flag289prime-)
(not (Flag289-))
)

)
(:action Prim290Action-0
:parameters ()
:precondition
(and
(not (RIGHTOF11-ROBOT))
)
:effect
(and
(Flag290prime-)
(not (Flag290-))
)

)
(:action Prim290Action-1
:parameters ()
:precondition
(and
(not (LEFTOF9-ROBOT))
)
:effect
(and
(Flag290prime-)
(not (Flag290-))
)

)
(:action Prim291Action-0
:parameters ()
:precondition
(and
(not (LEFTOF9-ROBOT))
)
:effect
(and
(Flag291prime-)
(not (Flag291-))
)

)
(:action Prim291Action-1
:parameters ()
:precondition
(and
(not (RIGHTOF12-ROBOT))
)
:effect
(and
(Flag291prime-)
(not (Flag291-))
)

)
(:action Flag293Action
:parameters ()
:precondition
(and
(LEFTOF10-ROBOT)
(RIGHTOF12-ROBOT)
)
:effect
(and
(Flag293-)
(Flag293prime-)
)

)
(:action Flag294Action
:parameters ()
:precondition
(and
(RIGHTOF11-ROBOT)
(LEFTOF10-ROBOT)
)
:effect
(and
(Flag294-)
(Flag294prime-)
)

)
(:action Flag295Action
:parameters ()
:precondition
(and
(RIGHTOF10-ROBOT)
(LEFTOF10-ROBOT)
)
:effect
(and
(Flag295-)
(Flag295prime-)
)

)
(:action Flag296Action
:parameters ()
:precondition
(and
(RIGHTOF13-ROBOT)
(LEFTOF10-ROBOT)
)
:effect
(and
(Flag296-)
(Flag296prime-)
)

)
(:action Flag297Action
:parameters ()
:precondition
(and
(LEFTOF10-ROBOT)
(RIGHTOF9-ROBOT)
)
:effect
(and
(Flag297-)
(Flag297prime-)
)

)
(:action Prim293Action-0
:parameters ()
:precondition
(and
(not (LEFTOF10-ROBOT))
)
:effect
(and
(Flag293prime-)
(not (Flag293-))
)

)
(:action Prim293Action-1
:parameters ()
:precondition
(and
(not (RIGHTOF12-ROBOT))
)
:effect
(and
(Flag293prime-)
(not (Flag293-))
)

)
(:action Prim294Action-0
:parameters ()
:precondition
(and
(not (RIGHTOF11-ROBOT))
)
:effect
(and
(Flag294prime-)
(not (Flag294-))
)

)
(:action Prim294Action-1
:parameters ()
:precondition
(and
(not (LEFTOF10-ROBOT))
)
:effect
(and
(Flag294prime-)
(not (Flag294-))
)

)
(:action Prim295Action-0
:parameters ()
:precondition
(and
(not (RIGHTOF10-ROBOT))
)
:effect
(and
(Flag295prime-)
(not (Flag295-))
)

)
(:action Prim295Action-1
:parameters ()
:precondition
(and
(not (LEFTOF10-ROBOT))
)
:effect
(and
(Flag295prime-)
(not (Flag295-))
)

)
(:action Prim296Action-0
:parameters ()
:precondition
(and
(not (RIGHTOF13-ROBOT))
)
:effect
(and
(Flag296prime-)
(not (Flag296-))
)

)
(:action Prim296Action-1
:parameters ()
:precondition
(and
(not (LEFTOF10-ROBOT))
)
:effect
(and
(Flag296prime-)
(not (Flag296-))
)

)
(:action Prim297Action-0
:parameters ()
:precondition
(and
(not (LEFTOF10-ROBOT))
)
:effect
(and
(Flag297prime-)
(not (Flag297-))
)

)
(:action Prim297Action-1
:parameters ()
:precondition
(and
(not (RIGHTOF9-ROBOT))
)
:effect
(and
(Flag297prime-)
(not (Flag297-))
)

)
(:action Flag299Action
:parameters ()
:precondition
(and
(RIGHTOF11-ROBOT)
(LEFTOF11-ROBOT)
)
:effect
(and
(Flag299-)
(Flag299prime-)
)

)
(:action Flag300Action
:parameters ()
:precondition
(and
(RIGHTOF13-ROBOT)
(LEFTOF11-ROBOT)
)
:effect
(and
(Flag300-)
(Flag300prime-)
)

)
(:action Flag301Action
:parameters ()
:precondition
(and
(RIGHTOF10-ROBOT)
(LEFTOF11-ROBOT)
)
:effect
(and
(Flag301-)
(Flag301prime-)
)

)
(:action Flag302Action
:parameters ()
:precondition
(and
(LEFTOF11-ROBOT)
(RIGHTOF12-ROBOT)
)
:effect
(and
(Flag302-)
(Flag302prime-)
)

)
(:action Prim299Action-0
:parameters ()
:precondition
(and
(not (RIGHTOF11-ROBOT))
)
:effect
(and
(Flag299prime-)
(not (Flag299-))
)

)
(:action Prim299Action-1
:parameters ()
:precondition
(and
(not (LEFTOF11-ROBOT))
)
:effect
(and
(Flag299prime-)
(not (Flag299-))
)

)
(:action Prim300Action-0
:parameters ()
:precondition
(and
(not (RIGHTOF13-ROBOT))
)
:effect
(and
(Flag300prime-)
(not (Flag300-))
)

)
(:action Prim300Action-1
:parameters ()
:precondition
(and
(not (LEFTOF11-ROBOT))
)
:effect
(and
(Flag300prime-)
(not (Flag300-))
)

)
(:action Prim301Action-0
:parameters ()
:precondition
(and
(not (RIGHTOF10-ROBOT))
)
:effect
(and
(Flag301prime-)
(not (Flag301-))
)

)
(:action Prim301Action-1
:parameters ()
:precondition
(and
(not (LEFTOF11-ROBOT))
)
:effect
(and
(Flag301prime-)
(not (Flag301-))
)

)
(:action Prim302Action-0
:parameters ()
:precondition
(and
(not (LEFTOF11-ROBOT))
)
:effect
(and
(Flag302prime-)
(not (Flag302-))
)

)
(:action Prim302Action-1
:parameters ()
:precondition
(and
(not (RIGHTOF12-ROBOT))
)
:effect
(and
(Flag302prime-)
(not (Flag302-))
)

)
(:action Flag304Action
:parameters ()
:precondition
(and
(RIGHTOF12-ROBOT)
(LEFTOF12-ROBOT)
)
:effect
(and
(Flag304-)
(Flag304prime-)
)

)
(:action Flag305Action
:parameters ()
:precondition
(and
(RIGHTOF11-ROBOT)
(LEFTOF12-ROBOT)
)
:effect
(and
(Flag305-)
(Flag305prime-)
)

)
(:action Flag306Action
:parameters ()
:precondition
(and
(RIGHTOF13-ROBOT)
(LEFTOF12-ROBOT)
)
:effect
(and
(Flag306-)
(Flag306prime-)
)

)
(:action Prim304Action-0
:parameters ()
:precondition
(and
(not (RIGHTOF12-ROBOT))
)
:effect
(and
(Flag304prime-)
(not (Flag304-))
)

)
(:action Prim304Action-1
:parameters ()
:precondition
(and
(not (LEFTOF12-ROBOT))
)
:effect
(and
(Flag304prime-)
(not (Flag304-))
)

)
(:action Prim305Action-0
:parameters ()
:precondition
(and
(not (RIGHTOF11-ROBOT))
)
:effect
(and
(Flag305prime-)
(not (Flag305-))
)

)
(:action Prim305Action-1
:parameters ()
:precondition
(and
(not (LEFTOF12-ROBOT))
)
:effect
(and
(Flag305prime-)
(not (Flag305-))
)

)
(:action Prim306Action-0
:parameters ()
:precondition
(and
(not (RIGHTOF13-ROBOT))
)
:effect
(and
(Flag306prime-)
(not (Flag306-))
)

)
(:action Prim306Action-1
:parameters ()
:precondition
(and
(not (LEFTOF12-ROBOT))
)
:effect
(and
(Flag306prime-)
(not (Flag306-))
)

)
(:action Flag308Action
:parameters ()
:precondition
(and
(LEFTOF13-ROBOT)
(RIGHTOF12-ROBOT)
)
:effect
(and
(Flag308-)
(Flag308prime-)
)

)
(:action Flag309Action
:parameters ()
:precondition
(and
(RIGHTOF13-ROBOT)
(LEFTOF13-ROBOT)
)
:effect
(and
(Flag309-)
(Flag309prime-)
)

)
(:action Flag310Action
:parameters ()
:precondition
(and
(LEFTOF12-ROBOT)
(RIGHTOF12-ROBOT)
)
:effect
(and
(Flag310-)
(Flag310prime-)
)

)
(:action Prim308Action-1
:parameters ()
:precondition
(and
(not (RIGHTOF12-ROBOT))
)
:effect
(and
(Flag308prime-)
(not (Flag308-))
)

)
(:action Prim309Action-0
:parameters ()
:precondition
(and
(not (RIGHTOF13-ROBOT))
)
:effect
(and
(Flag309prime-)
(not (Flag309-))
)

)
(:action Prim310Action-0
:parameters ()
:precondition
(and
(not (LEFTOF12-ROBOT))
)
:effect
(and
(Flag310prime-)
(not (Flag310-))
)

)
(:action Prim310Action-1
:parameters ()
:precondition
(and
(not (RIGHTOF12-ROBOT))
)
:effect
(and
(Flag310prime-)
(not (Flag310-))
)

)
(:action Flag312Action-0
:parameters ()
:precondition
(and
(LEFTOF10-ROBOT)
)
:effect
(and
(Flag312-)
(Flag312prime-)
)

)
(:action Flag312Action-1
:parameters ()
:precondition
(and
(LEFTOF11-ROBOT)
)
:effect
(and
(Flag312-)
(Flag312prime-)
)

)
(:action Flag312Action-2
:parameters ()
:precondition
(and
(LEFTOF9-ROBOT)
)
:effect
(and
(Flag312-)
(Flag312prime-)
)

)
(:action Flag312Action-3
:parameters ()
:precondition
(and
(LEFTOF2-ROBOT)
)
:effect
(and
(Flag312-)
(Flag312prime-)
)

)
(:action Flag312Action-5
:parameters ()
:precondition
(and
(LEFTOF1-ROBOT)
)
:effect
(and
(Flag312-)
(Flag312prime-)
)

)
(:action Flag312Action-6
:parameters ()
:precondition
(and
(LEFTOF12-ROBOT)
)
:effect
(and
(Flag312-)
(Flag312prime-)
)

)
(:action Flag312Action-7
:parameters ()
:precondition
(and
(LEFTOF6-ROBOT)
)
:effect
(and
(Flag312-)
(Flag312prime-)
)

)
(:action Flag312Action-8
:parameters ()
:precondition
(and
(LEFTOF5-ROBOT)
)
:effect
(and
(Flag312-)
(Flag312prime-)
)

)
(:action Flag312Action-9
:parameters ()
:precondition
(and
(LEFTOF14-ROBOT)
)
:effect
(and
(Flag312-)
(Flag312prime-)
)

)
(:action Flag312Action-10
:parameters ()
:precondition
(and
(LEFTOF3-ROBOT)
)
:effect
(and
(Flag312-)
(Flag312prime-)
)

)
(:action Flag312Action-11
:parameters ()
:precondition
(and
(LEFTOF4-ROBOT)
)
:effect
(and
(Flag312-)
(Flag312prime-)
)

)
(:action Flag312Action-12
:parameters ()
:precondition
(and
(LEFTOF7-ROBOT)
)
:effect
(and
(Flag312-)
(Flag312prime-)
)

)
(:action Flag312Action-13
:parameters ()
:precondition
(and
(LEFTOF8-ROBOT)
)
:effect
(and
(Flag312-)
(Flag312prime-)
)

)
(:action Prim312Action
:parameters ()
:precondition
(and
(not (LEFTOF10-ROBOT))
(not (LEFTOF11-ROBOT))
(not (LEFTOF9-ROBOT))
(not (LEFTOF2-ROBOT))
(not (LEFTOF13-ROBOT))
(not (LEFTOF1-ROBOT))
(not (LEFTOF12-ROBOT))
(not (LEFTOF6-ROBOT))
(not (LEFTOF5-ROBOT))
(not (LEFTOF14-ROBOT))
(not (LEFTOF3-ROBOT))
(not (LEFTOF4-ROBOT))
(not (LEFTOF7-ROBOT))
(not (LEFTOF8-ROBOT))
)
:effect
(and
(Flag312prime-)
(not (Flag312-))
)

)
(:action Flag313Action
:parameters ()
:precondition
(and
(RIGHTOF13-ROBOT)
(LEFTOF14-ROBOT)
)
:effect
(and
(Flag313-)
(Flag313prime-)
)

)
(:action Prim313Action-0
:parameters ()
:precondition
(and
(not (RIGHTOF13-ROBOT))
)
:effect
(and
(Flag313prime-)
(not (Flag313-))
)

)
(:action Prim313Action-1
:parameters ()
:precondition
(and
(not (LEFTOF14-ROBOT))
)
:effect
(and
(Flag313prime-)
(not (Flag313-))
)

)
(:action MOVELEFT-ROBOT
:parameters ()
:precondition
(and
(Flag3-)
(Flag3prime-)
(Flag314prime-)
(Flag311prime-)
(Flag307prime-)
(Flag303prime-)
(Flag298prime-)
(Flag292prime-)
(Flag285prime-)
(Flag276prime-)
(Flag265prime-)
(Flag255prime-)
(Flag242prime-)
(Flag228prime-)
(Flag215prime-)
(Flag312prime-)
)
:effect
(and
(when
(and
(Flag314-)
)
(and
(COLUMN12-ROBOT)
(not (NOT-COLUMN12-ROBOT))
)
)
(when
(and
(Flag311-)
)
(and
(COLUMN11-ROBOT)
(not (NOT-COLUMN11-ROBOT))
)
)
(when
(and
(Flag307-)
)
(and
(COLUMN10-ROBOT)
(not (NOT-COLUMN10-ROBOT))
)
)
(when
(and
(Flag303-)
)
(and
(COLUMN9-ROBOT)
(not (NOT-COLUMN9-ROBOT))
)
)
(when
(and
(Flag298-)
)
(and
(COLUMN8-ROBOT)
(not (NOT-COLUMN8-ROBOT))
)
)
(when
(and
(Flag292-)
)
(and
(COLUMN7-ROBOT)
(not (NOT-COLUMN7-ROBOT))
)
)
(when
(and
(Flag285-)
)
(and
(COLUMN6-ROBOT)
(not (NOT-COLUMN6-ROBOT))
)
)
(when
(and
(Flag276-)
)
(and
(COLUMN5-ROBOT)
(not (NOT-COLUMN5-ROBOT))
)
)
(when
(and
(Flag265-)
)
(and
(COLUMN4-ROBOT)
(not (NOT-COLUMN4-ROBOT))
)
)
(when
(and
(Flag255-)
)
(and
(COLUMN3-ROBOT)
(not (NOT-COLUMN3-ROBOT))
)
)
(when
(and
(Flag242-)
)
(and
(COLUMN2-ROBOT)
(not (NOT-COLUMN2-ROBOT))
)
)
(when
(and
(Flag228-)
)
(and
(COLUMN1-ROBOT)
(not (NOT-COLUMN1-ROBOT))
)
)
(when
(and
(Flag215-)
)
(and
(COLUMN0-ROBOT)
(not (NOT-COLUMN0-ROBOT))
)
)
(when
(and
(COLUMN13-ROBOT)
)
(and
(COLUMN12-ROBOT)
(NOT-COLUMN13-ROBOT)
(not (NOT-COLUMN12-ROBOT))
(not (COLUMN13-ROBOT))
)
)
(when
(and
(COLUMN12-ROBOT)
)
(and
(COLUMN11-ROBOT)
(NOT-COLUMN12-ROBOT)
(not (NOT-COLUMN11-ROBOT))
(not (COLUMN12-ROBOT))
)
)
(when
(and
(COLUMN11-ROBOT)
)
(and
(COLUMN10-ROBOT)
(NOT-COLUMN11-ROBOT)
(not (NOT-COLUMN10-ROBOT))
(not (COLUMN11-ROBOT))
)
)
(when
(and
(COLUMN10-ROBOT)
)
(and
(COLUMN9-ROBOT)
(NOT-COLUMN10-ROBOT)
(not (NOT-COLUMN9-ROBOT))
(not (COLUMN10-ROBOT))
)
)
(when
(and
(COLUMN9-ROBOT)
)
(and
(COLUMN8-ROBOT)
(NOT-COLUMN9-ROBOT)
(not (NOT-COLUMN8-ROBOT))
(not (COLUMN9-ROBOT))
)
)
(when
(and
(COLUMN8-ROBOT)
)
(and
(COLUMN7-ROBOT)
(NOT-COLUMN8-ROBOT)
(not (NOT-COLUMN7-ROBOT))
(not (COLUMN8-ROBOT))
)
)
(when
(and
(COLUMN7-ROBOT)
)
(and
(COLUMN6-ROBOT)
(NOT-COLUMN7-ROBOT)
(not (NOT-COLUMN6-ROBOT))
(not (COLUMN7-ROBOT))
)
)
(when
(and
(COLUMN6-ROBOT)
)
(and
(COLUMN5-ROBOT)
(NOT-COLUMN6-ROBOT)
(not (NOT-COLUMN5-ROBOT))
(not (COLUMN6-ROBOT))
)
)
(when
(and
(COLUMN5-ROBOT)
)
(and
(COLUMN4-ROBOT)
(NOT-COLUMN5-ROBOT)
(not (NOT-COLUMN4-ROBOT))
(not (COLUMN5-ROBOT))
)
)
(when
(and
(COLUMN4-ROBOT)
)
(and
(COLUMN3-ROBOT)
(NOT-COLUMN4-ROBOT)
(not (NOT-COLUMN3-ROBOT))
(not (COLUMN4-ROBOT))
)
)
(when
(and
(COLUMN3-ROBOT)
)
(and
(COLUMN2-ROBOT)
(NOT-COLUMN3-ROBOT)
(not (NOT-COLUMN2-ROBOT))
(not (COLUMN3-ROBOT))
)
)
(when
(and
(COLUMN2-ROBOT)
)
(and
(COLUMN1-ROBOT)
(NOT-COLUMN2-ROBOT)
(not (NOT-COLUMN1-ROBOT))
(not (COLUMN2-ROBOT))
)
)
(when
(and
(COLUMN1-ROBOT)
)
(and
(COLUMN0-ROBOT)
(NOT-COLUMN1-ROBOT)
(not (NOT-COLUMN0-ROBOT))
(not (COLUMN1-ROBOT))
)
)
(when
(and
(RIGHTOF13-ROBOT)
)
(and
(RIGHTOF12-ROBOT)
(NOT-RIGHTOF13-ROBOT)
(RIGHTOF11-ROBOT)
(NOT-RIGHTOF12-ROBOT)
(RIGHTOF10-ROBOT)
(NOT-RIGHTOF11-ROBOT)
(RIGHTOF9-ROBOT)
(NOT-RIGHTOF10-ROBOT)
(RIGHTOF8-ROBOT)
(NOT-RIGHTOF9-ROBOT)
(RIGHTOF7-ROBOT)
(NOT-RIGHTOF8-ROBOT)
(RIGHTOF6-ROBOT)
(NOT-RIGHTOF7-ROBOT)
(RIGHTOF5-ROBOT)
(NOT-RIGHTOF6-ROBOT)
(RIGHTOF4-ROBOT)
(NOT-RIGHTOF5-ROBOT)
(RIGHTOF3-ROBOT)
(NOT-RIGHTOF4-ROBOT)
(RIGHTOF2-ROBOT)
(NOT-RIGHTOF3-ROBOT)
(RIGHTOF1-ROBOT)
(NOT-RIGHTOF2-ROBOT)
(RIGHTOF0-ROBOT)
(NOT-RIGHTOF1-ROBOT)
(not (RIGHTOF13-ROBOT))
(not (NOT-RIGHTOF0-ROBOT))
)
)
(when
(and
(RIGHTOF12-ROBOT)
)
(and
(RIGHTOF11-ROBOT)
(NOT-RIGHTOF12-ROBOT)
(RIGHTOF10-ROBOT)
(NOT-RIGHTOF11-ROBOT)
(RIGHTOF9-ROBOT)
(NOT-RIGHTOF10-ROBOT)
(RIGHTOF8-ROBOT)
(NOT-RIGHTOF9-ROBOT)
(RIGHTOF7-ROBOT)
(NOT-RIGHTOF8-ROBOT)
(RIGHTOF6-ROBOT)
(NOT-RIGHTOF7-ROBOT)
(RIGHTOF5-ROBOT)
(NOT-RIGHTOF6-ROBOT)
(RIGHTOF4-ROBOT)
(NOT-RIGHTOF5-ROBOT)
(RIGHTOF3-ROBOT)
(NOT-RIGHTOF4-ROBOT)
(RIGHTOF2-ROBOT)
(NOT-RIGHTOF3-ROBOT)
(RIGHTOF1-ROBOT)
(NOT-RIGHTOF2-ROBOT)
(RIGHTOF0-ROBOT)
(NOT-RIGHTOF1-ROBOT)
(not (RIGHTOF12-ROBOT))
(not (NOT-RIGHTOF0-ROBOT))
)
)
(when
(and
(RIGHTOF11-ROBOT)
)
(and
(RIGHTOF10-ROBOT)
(NOT-RIGHTOF11-ROBOT)
(RIGHTOF9-ROBOT)
(NOT-RIGHTOF10-ROBOT)
(RIGHTOF8-ROBOT)
(NOT-RIGHTOF9-ROBOT)
(RIGHTOF7-ROBOT)
(NOT-RIGHTOF8-ROBOT)
(RIGHTOF6-ROBOT)
(NOT-RIGHTOF7-ROBOT)
(RIGHTOF5-ROBOT)
(NOT-RIGHTOF6-ROBOT)
(RIGHTOF4-ROBOT)
(NOT-RIGHTOF5-ROBOT)
(RIGHTOF3-ROBOT)
(NOT-RIGHTOF4-ROBOT)
(RIGHTOF2-ROBOT)
(NOT-RIGHTOF3-ROBOT)
(RIGHTOF1-ROBOT)
(NOT-RIGHTOF2-ROBOT)
(RIGHTOF0-ROBOT)
(NOT-RIGHTOF1-ROBOT)
(not (RIGHTOF11-ROBOT))
(not (NOT-RIGHTOF0-ROBOT))
)
)
(when
(and
(RIGHTOF10-ROBOT)
)
(and
(RIGHTOF9-ROBOT)
(NOT-RIGHTOF10-ROBOT)
(RIGHTOF8-ROBOT)
(NOT-RIGHTOF9-ROBOT)
(RIGHTOF7-ROBOT)
(NOT-RIGHTOF8-ROBOT)
(RIGHTOF6-ROBOT)
(NOT-RIGHTOF7-ROBOT)
(RIGHTOF5-ROBOT)
(NOT-RIGHTOF6-ROBOT)
(RIGHTOF4-ROBOT)
(NOT-RIGHTOF5-ROBOT)
(RIGHTOF3-ROBOT)
(NOT-RIGHTOF4-ROBOT)
(RIGHTOF2-ROBOT)
(NOT-RIGHTOF3-ROBOT)
(RIGHTOF1-ROBOT)
(NOT-RIGHTOF2-ROBOT)
(RIGHTOF0-ROBOT)
(NOT-RIGHTOF1-ROBOT)
(not (RIGHTOF10-ROBOT))
(not (NOT-RIGHTOF0-ROBOT))
)
)
(when
(and
(RIGHTOF9-ROBOT)
)
(and
(RIGHTOF8-ROBOT)
(NOT-RIGHTOF9-ROBOT)
(RIGHTOF7-ROBOT)
(NOT-RIGHTOF8-ROBOT)
(RIGHTOF6-ROBOT)
(NOT-RIGHTOF7-ROBOT)
(RIGHTOF5-ROBOT)
(NOT-RIGHTOF6-ROBOT)
(RIGHTOF4-ROBOT)
(NOT-RIGHTOF5-ROBOT)
(RIGHTOF3-ROBOT)
(NOT-RIGHTOF4-ROBOT)
(RIGHTOF2-ROBOT)
(NOT-RIGHTOF3-ROBOT)
(RIGHTOF1-ROBOT)
(NOT-RIGHTOF2-ROBOT)
(RIGHTOF0-ROBOT)
(NOT-RIGHTOF1-ROBOT)
(not (RIGHTOF9-ROBOT))
(not (NOT-RIGHTOF0-ROBOT))
)
)
(when
(and
(RIGHTOF8-ROBOT)
)
(and
(RIGHTOF7-ROBOT)
(NOT-RIGHTOF8-ROBOT)
(RIGHTOF6-ROBOT)
(NOT-RIGHTOF7-ROBOT)
(RIGHTOF5-ROBOT)
(NOT-RIGHTOF6-ROBOT)
(RIGHTOF4-ROBOT)
(NOT-RIGHTOF5-ROBOT)
(RIGHTOF3-ROBOT)
(NOT-RIGHTOF4-ROBOT)
(RIGHTOF2-ROBOT)
(NOT-RIGHTOF3-ROBOT)
(RIGHTOF1-ROBOT)
(NOT-RIGHTOF2-ROBOT)
(RIGHTOF0-ROBOT)
(NOT-RIGHTOF1-ROBOT)
(not (RIGHTOF8-ROBOT))
(not (NOT-RIGHTOF0-ROBOT))
)
)
(when
(and
(RIGHTOF7-ROBOT)
)
(and
(RIGHTOF6-ROBOT)
(NOT-RIGHTOF7-ROBOT)
(RIGHTOF5-ROBOT)
(NOT-RIGHTOF6-ROBOT)
(RIGHTOF4-ROBOT)
(NOT-RIGHTOF5-ROBOT)
(RIGHTOF3-ROBOT)
(NOT-RIGHTOF4-ROBOT)
(RIGHTOF2-ROBOT)
(NOT-RIGHTOF3-ROBOT)
(RIGHTOF1-ROBOT)
(NOT-RIGHTOF2-ROBOT)
(RIGHTOF0-ROBOT)
(NOT-RIGHTOF1-ROBOT)
(not (RIGHTOF7-ROBOT))
(not (NOT-RIGHTOF0-ROBOT))
)
)
(when
(and
(RIGHTOF6-ROBOT)
)
(and
(RIGHTOF5-ROBOT)
(NOT-RIGHTOF6-ROBOT)
(RIGHTOF4-ROBOT)
(NOT-RIGHTOF5-ROBOT)
(RIGHTOF3-ROBOT)
(NOT-RIGHTOF4-ROBOT)
(RIGHTOF2-ROBOT)
(NOT-RIGHTOF3-ROBOT)
(RIGHTOF1-ROBOT)
(NOT-RIGHTOF2-ROBOT)
(RIGHTOF0-ROBOT)
(NOT-RIGHTOF1-ROBOT)
(not (RIGHTOF6-ROBOT))
(not (NOT-RIGHTOF0-ROBOT))
)
)
(when
(and
(RIGHTOF5-ROBOT)
)
(and
(RIGHTOF4-ROBOT)
(NOT-RIGHTOF5-ROBOT)
(RIGHTOF3-ROBOT)
(NOT-RIGHTOF4-ROBOT)
(RIGHTOF2-ROBOT)
(NOT-RIGHTOF3-ROBOT)
(RIGHTOF1-ROBOT)
(NOT-RIGHTOF2-ROBOT)
(RIGHTOF0-ROBOT)
(NOT-RIGHTOF1-ROBOT)
(not (RIGHTOF5-ROBOT))
(not (NOT-RIGHTOF0-ROBOT))
)
)
(when
(and
(RIGHTOF4-ROBOT)
)
(and
(RIGHTOF3-ROBOT)
(NOT-RIGHTOF4-ROBOT)
(RIGHTOF2-ROBOT)
(NOT-RIGHTOF3-ROBOT)
(RIGHTOF1-ROBOT)
(NOT-RIGHTOF2-ROBOT)
(RIGHTOF0-ROBOT)
(NOT-RIGHTOF1-ROBOT)
(not (RIGHTOF4-ROBOT))
(not (NOT-RIGHTOF0-ROBOT))
)
)
(when
(and
(RIGHTOF3-ROBOT)
)
(and
(RIGHTOF2-ROBOT)
(NOT-RIGHTOF3-ROBOT)
(RIGHTOF1-ROBOT)
(NOT-RIGHTOF2-ROBOT)
(RIGHTOF0-ROBOT)
(NOT-RIGHTOF1-ROBOT)
(not (RIGHTOF3-ROBOT))
(not (NOT-RIGHTOF0-ROBOT))
)
)
(when
(and
(RIGHTOF2-ROBOT)
)
(and
(RIGHTOF1-ROBOT)
(NOT-RIGHTOF2-ROBOT)
(RIGHTOF0-ROBOT)
(NOT-RIGHTOF1-ROBOT)
(not (RIGHTOF2-ROBOT))
(not (NOT-RIGHTOF0-ROBOT))
)
)
(when
(and
(RIGHTOF1-ROBOT)
)
(and
(RIGHTOF0-ROBOT)
(NOT-RIGHTOF1-ROBOT)
(not (NOT-RIGHTOF0-ROBOT))
(not (RIGHTOF1-ROBOT))
)
)
(when
(and
(Flag312-)
)
(and
(LEFTOF13-ROBOT)
(not (NOT-LEFTOF13-ROBOT))
)
)
(when
(and
(LEFTOF8-ROBOT)
)
(and
(LEFTOF12-ROBOT)
(LEFTOF11-ROBOT)
(LEFTOF10-ROBOT)
(LEFTOF9-ROBOT)
(LEFTOF7-ROBOT)
(not (NOT-LEFTOF12-ROBOT))
(not (NOT-LEFTOF11-ROBOT))
(not (NOT-LEFTOF10-ROBOT))
(not (NOT-LEFTOF9-ROBOT))
(not (NOT-LEFTOF8-ROBOT))
(not (NOT-LEFTOF7-ROBOT))
)
)
(when
(and
(LEFTOF7-ROBOT)
)
(and
(LEFTOF12-ROBOT)
(LEFTOF11-ROBOT)
(LEFTOF10-ROBOT)
(LEFTOF9-ROBOT)
(LEFTOF8-ROBOT)
(LEFTOF6-ROBOT)
(not (NOT-LEFTOF12-ROBOT))
(not (NOT-LEFTOF11-ROBOT))
(not (NOT-LEFTOF10-ROBOT))
(not (NOT-LEFTOF9-ROBOT))
(not (NOT-LEFTOF8-ROBOT))
(not (NOT-LEFTOF7-ROBOT))
(not (NOT-LEFTOF6-ROBOT))
)
)
(when
(and
(LEFTOF4-ROBOT)
)
(and
(LEFTOF12-ROBOT)
(LEFTOF11-ROBOT)
(LEFTOF10-ROBOT)
(LEFTOF9-ROBOT)
(LEFTOF8-ROBOT)
(LEFTOF7-ROBOT)
(LEFTOF6-ROBOT)
(LEFTOF5-ROBOT)
(LEFTOF3-ROBOT)
(not (NOT-LEFTOF12-ROBOT))
(not (NOT-LEFTOF11-ROBOT))
(not (NOT-LEFTOF10-ROBOT))
(not (NOT-LEFTOF9-ROBOT))
(not (NOT-LEFTOF8-ROBOT))
(not (NOT-LEFTOF7-ROBOT))
(not (NOT-LEFTOF6-ROBOT))
(not (NOT-LEFTOF5-ROBOT))
(not (NOT-LEFTOF4-ROBOT))
(not (NOT-LEFTOF3-ROBOT))
)
)
(when
(and
(LEFTOF3-ROBOT)
)
(and
(LEFTOF12-ROBOT)
(LEFTOF11-ROBOT)
(LEFTOF10-ROBOT)
(LEFTOF9-ROBOT)
(LEFTOF8-ROBOT)
(LEFTOF7-ROBOT)
(LEFTOF6-ROBOT)
(LEFTOF5-ROBOT)
(LEFTOF4-ROBOT)
(LEFTOF2-ROBOT)
(not (NOT-LEFTOF12-ROBOT))
(not (NOT-LEFTOF11-ROBOT))
(not (NOT-LEFTOF10-ROBOT))
(not (NOT-LEFTOF9-ROBOT))
(not (NOT-LEFTOF8-ROBOT))
(not (NOT-LEFTOF7-ROBOT))
(not (NOT-LEFTOF6-ROBOT))
(not (NOT-LEFTOF5-ROBOT))
(not (NOT-LEFTOF4-ROBOT))
(not (NOT-LEFTOF3-ROBOT))
(not (NOT-LEFTOF2-ROBOT))
)
)
(when
(and
(LEFTOF5-ROBOT)
)
(and
(LEFTOF12-ROBOT)
(LEFTOF11-ROBOT)
(LEFTOF10-ROBOT)
(LEFTOF9-ROBOT)
(LEFTOF8-ROBOT)
(LEFTOF7-ROBOT)
(LEFTOF6-ROBOT)
(LEFTOF4-ROBOT)
(not (NOT-LEFTOF12-ROBOT))
(not (NOT-LEFTOF11-ROBOT))
(not (NOT-LEFTOF10-ROBOT))
(not (NOT-LEFTOF9-ROBOT))
(not (NOT-LEFTOF8-ROBOT))
(not (NOT-LEFTOF7-ROBOT))
(not (NOT-LEFTOF6-ROBOT))
(not (NOT-LEFTOF5-ROBOT))
(not (NOT-LEFTOF4-ROBOT))
)
)
(when
(and
(LEFTOF6-ROBOT)
)
(and
(LEFTOF12-ROBOT)
(LEFTOF11-ROBOT)
(LEFTOF10-ROBOT)
(LEFTOF9-ROBOT)
(LEFTOF8-ROBOT)
(LEFTOF7-ROBOT)
(LEFTOF5-ROBOT)
(not (NOT-LEFTOF12-ROBOT))
(not (NOT-LEFTOF11-ROBOT))
(not (NOT-LEFTOF10-ROBOT))
(not (NOT-LEFTOF9-ROBOT))
(not (NOT-LEFTOF8-ROBOT))
(not (NOT-LEFTOF7-ROBOT))
(not (NOT-LEFTOF6-ROBOT))
(not (NOT-LEFTOF5-ROBOT))
)
)
(when
(and
(LEFTOF12-ROBOT)
)
(and
(LEFTOF11-ROBOT)
(not (NOT-LEFTOF12-ROBOT))
(not (NOT-LEFTOF11-ROBOT))
)
)
(when
(and
(LEFTOF1-ROBOT)
)
(and
(LEFTOF12-ROBOT)
(LEFTOF11-ROBOT)
(LEFTOF10-ROBOT)
(LEFTOF9-ROBOT)
(LEFTOF8-ROBOT)
(LEFTOF7-ROBOT)
(LEFTOF6-ROBOT)
(LEFTOF5-ROBOT)
(LEFTOF4-ROBOT)
(LEFTOF3-ROBOT)
(LEFTOF2-ROBOT)
(not (NOT-LEFTOF12-ROBOT))
(not (NOT-LEFTOF11-ROBOT))
(not (NOT-LEFTOF10-ROBOT))
(not (NOT-LEFTOF9-ROBOT))
(not (NOT-LEFTOF8-ROBOT))
(not (NOT-LEFTOF7-ROBOT))
(not (NOT-LEFTOF6-ROBOT))
(not (NOT-LEFTOF5-ROBOT))
(not (NOT-LEFTOF4-ROBOT))
(not (NOT-LEFTOF3-ROBOT))
(not (NOT-LEFTOF2-ROBOT))
(not (NOT-LEFTOF1-ROBOT))
)
)
(when
(and
(LEFTOF13-ROBOT)
)
(and
(LEFTOF12-ROBOT)
(not (NOT-LEFTOF12-ROBOT))
)
)
(when
(and
(LEFTOF2-ROBOT)
)
(and
(LEFTOF12-ROBOT)
(LEFTOF11-ROBOT)
(LEFTOF10-ROBOT)
(LEFTOF9-ROBOT)
(LEFTOF8-ROBOT)
(LEFTOF7-ROBOT)
(LEFTOF6-ROBOT)
(LEFTOF5-ROBOT)
(LEFTOF4-ROBOT)
(LEFTOF3-ROBOT)
(LEFTOF1-ROBOT)
(not (NOT-LEFTOF12-ROBOT))
(not (NOT-LEFTOF11-ROBOT))
(not (NOT-LEFTOF10-ROBOT))
(not (NOT-LEFTOF9-ROBOT))
(not (NOT-LEFTOF8-ROBOT))
(not (NOT-LEFTOF7-ROBOT))
(not (NOT-LEFTOF6-ROBOT))
(not (NOT-LEFTOF5-ROBOT))
(not (NOT-LEFTOF4-ROBOT))
(not (NOT-LEFTOF3-ROBOT))
(not (NOT-LEFTOF2-ROBOT))
(not (NOT-LEFTOF1-ROBOT))
)
)
(when
(and
(LEFTOF9-ROBOT)
)
(and
(LEFTOF12-ROBOT)
(LEFTOF11-ROBOT)
(LEFTOF10-ROBOT)
(LEFTOF8-ROBOT)
(not (NOT-LEFTOF12-ROBOT))
(not (NOT-LEFTOF11-ROBOT))
(not (NOT-LEFTOF10-ROBOT))
(not (NOT-LEFTOF9-ROBOT))
(not (NOT-LEFTOF8-ROBOT))
)
)
(when
(and
(LEFTOF11-ROBOT)
)
(and
(LEFTOF12-ROBOT)
(LEFTOF10-ROBOT)
(not (NOT-LEFTOF12-ROBOT))
(not (NOT-LEFTOF11-ROBOT))
(not (NOT-LEFTOF10-ROBOT))
)
)
(when
(and
(LEFTOF10-ROBOT)
)
(and
(LEFTOF12-ROBOT)
(LEFTOF11-ROBOT)
(LEFTOF9-ROBOT)
(not (NOT-LEFTOF12-ROBOT))
(not (NOT-LEFTOF11-ROBOT))
(not (NOT-LEFTOF10-ROBOT))
(not (NOT-LEFTOF9-ROBOT))
)
)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag312prime-))
(not (Flag185prime-))
(not (Flag163prime-))
(not (Flag162prime-))
(not (Flag161prime-))
(not (Flag157prime-))
(not (Flag28prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag159prime-))
(not (Flag158prime-))
(not (Flag155prime-))
(not (Flag154prime-))
(not (Flag153prime-))
(not (Flag151prime-))
(not (Flag150prime-))
(not (Flag149prime-))
(not (Flag147prime-))
(not (Flag146prime-))
(not (Flag145prime-))
(not (Flag144prime-))
(not (Flag142prime-))
(not (Flag141prime-))
(not (Flag140prime-))
(not (Flag139prime-))
(not (Flag138prime-))
(not (Flag137prime-))
(not (Flag135prime-))
(not (Flag134prime-))
(not (Flag133prime-))
(not (Flag132prime-))
(not (Flag131prime-))
(not (Flag130prime-))
(not (Flag129prime-))
(not (Flag128prime-))
(not (Flag126prime-))
(not (Flag125prime-))
(not (Flag124prime-))
(not (Flag123prime-))
(not (Flag122prime-))
(not (Flag121prime-))
(not (Flag120prime-))
(not (Flag119prime-))
(not (Flag117prime-))
(not (Flag116prime-))
(not (Flag115prime-))
(not (Flag114prime-))
(not (Flag113prime-))
(not (Flag112prime-))
(not (Flag111prime-))
(not (Flag110prime-))
(not (Flag109prime-))
(not (Flag108prime-))
(not (Flag106prime-))
(not (Flag105prime-))
(not (Flag104prime-))
(not (Flag103prime-))
(not (Flag102prime-))
(not (Flag101prime-))
(not (Flag100prime-))
(not (Flag99prime-))
(not (Flag98prime-))
(not (Flag97prime-))
(not (Flag96prime-))
(not (Flag94prime-))
(not (Flag93prime-))
(not (Flag92prime-))
(not (Flag91prime-))
(not (Flag90prime-))
(not (Flag89prime-))
(not (Flag88prime-))
(not (Flag87prime-))
(not (Flag86prime-))
(not (Flag85prime-))
(not (Flag83prime-))
(not (Flag82prime-))
(not (Flag81prime-))
(not (Flag80prime-))
(not (Flag79prime-))
(not (Flag78prime-))
(not (Flag77prime-))
(not (Flag76prime-))
(not (Flag75prime-))
(not (Flag74prime-))
(not (Flag73prime-))
(not (Flag72prime-))
(not (Flag71prime-))
(not (Flag69prime-))
(not (Flag68prime-))
(not (Flag67prime-))
(not (Flag66prime-))
(not (Flag65prime-))
(not (Flag64prime-))
(not (Flag63prime-))
(not (Flag62prime-))
(not (Flag61prime-))
(not (Flag60prime-))
(not (Flag59prime-))
(not (Flag58prime-))
(not (Flag57prime-))
(not (Flag55prime-))
(not (Flag54prime-))
(not (Flag53prime-))
(not (Flag52prime-))
(not (Flag51prime-))
(not (Flag50prime-))
(not (Flag49prime-))
(not (Flag48prime-))
(not (Flag47prime-))
(not (Flag46prime-))
(not (Flag45prime-))
(not (Flag44prime-))
(not (Flag43prime-))
(not (Flag42prime-))
(not (Flag41prime-))
(not (Flag40prime-))
(not (Flag39prime-))
(not (Flag38prime-))
(not (Flag37prime-))
(not (Flag36prime-))
(not (Flag35prime-))
(not (Flag34prime-))
(not (Flag33prime-))
(not (Flag32prime-))
(not (Flag31prime-))
(not (Flag30prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag26prime-))
(not (Flag25prime-))
(not (Flag24prime-))
(not (Flag23prime-))
(not (Flag22prime-))
(not (Flag21prime-))
(not (Flag20prime-))
(not (Flag19prime-))
(not (Flag18prime-))
(not (Flag17prime-))
(not (Flag16prime-))
(not (Flag15prime-))
(not (Flag14prime-))
(not (Flag13prime-))
(not (Flag12prime-))
(not (Flag11prime-))
(not (Flag10prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag313prime-))
(not (Flag310prime-))
(not (Flag309prime-))
(not (Flag308prime-))
(not (Flag306prime-))
(not (Flag305prime-))
(not (Flag304prime-))
(not (Flag302prime-))
(not (Flag301prime-))
(not (Flag300prime-))
(not (Flag299prime-))
(not (Flag297prime-))
(not (Flag296prime-))
(not (Flag295prime-))
(not (Flag294prime-))
(not (Flag293prime-))
(not (Flag291prime-))
(not (Flag290prime-))
(not (Flag289prime-))
(not (Flag288prime-))
(not (Flag287prime-))
(not (Flag286prime-))
(not (Flag284prime-))
(not (Flag283prime-))
(not (Flag282prime-))
(not (Flag281prime-))
(not (Flag280prime-))
(not (Flag279prime-))
(not (Flag278prime-))
(not (Flag277prime-))
(not (Flag275prime-))
(not (Flag274prime-))
(not (Flag273prime-))
(not (Flag272prime-))
(not (Flag271prime-))
(not (Flag270prime-))
(not (Flag269prime-))
(not (Flag268prime-))
(not (Flag267prime-))
(not (Flag266prime-))
(not (Flag264prime-))
(not (Flag263prime-))
(not (Flag262prime-))
(not (Flag261prime-))
(not (Flag260prime-))
(not (Flag259prime-))
(not (Flag258prime-))
(not (Flag257prime-))
(not (Flag256prime-))
(not (Flag254prime-))
(not (Flag253prime-))
(not (Flag252prime-))
(not (Flag251prime-))
(not (Flag250prime-))
(not (Flag249prime-))
(not (Flag248prime-))
(not (Flag247prime-))
(not (Flag246prime-))
(not (Flag245prime-))
(not (Flag244prime-))
(not (Flag243prime-))
(not (Flag241prime-))
(not (Flag240prime-))
(not (Flag239prime-))
(not (Flag238prime-))
(not (Flag237prime-))
(not (Flag236prime-))
(not (Flag235prime-))
(not (Flag234prime-))
(not (Flag233prime-))
(not (Flag232prime-))
(not (Flag231prime-))
(not (Flag230prime-))
(not (Flag229prime-))
(not (Flag227prime-))
(not (Flag226prime-))
(not (Flag225prime-))
(not (Flag224prime-))
(not (Flag223prime-))
(not (Flag222prime-))
(not (Flag221prime-))
(not (Flag220prime-))
(not (Flag219prime-))
(not (Flag218prime-))
(not (Flag217prime-))
(not (Flag216prime-))
(not (Flag214prime-))
(not (Flag213prime-))
(not (Flag212prime-))
(not (Flag211prime-))
(not (Flag210prime-))
(not (Flag209prime-))
(not (Flag208prime-))
(not (Flag207prime-))
(not (Flag206prime-))
(not (Flag205prime-))
(not (Flag204prime-))
(not (Flag203prime-))
(not (Flag202prime-))
(not (Flag201prime-))
(not (Flag199prime-))
(not (Flag198prime-))
(not (Flag197prime-))
(not (Flag196prime-))
(not (Flag195prime-))
(not (Flag194prime-))
(not (Flag193prime-))
(not (Flag192prime-))
(not (Flag191prime-))
(not (Flag190prime-))
(not (Flag189prime-))
(not (Flag188prime-))
(not (Flag187prime-))
(not (Flag186prime-))
(not (Flag184prime-))
(not (Flag183prime-))
(not (Flag182prime-))
(not (Flag181prime-))
(not (Flag180prime-))
(not (Flag179prime-))
(not (Flag178prime-))
(not (Flag177prime-))
(not (Flag176prime-))
(not (Flag175prime-))
(not (Flag174prime-))
(not (Flag173prime-))
(not (Flag172prime-))
(not (Flag171prime-))
(not (Flag170prime-))
(not (Flag169prime-))
(not (Flag168prime-))
(not (Flag167prime-))
(not (Flag166prime-))
(not (Flag165prime-))
(not (Flag164prime-))
(not (Flag160prime-))
(not (Flag156prime-))
(not (Flag152prime-))
(not (Flag148prime-))
(not (Flag143prime-))
(not (Flag136prime-))
(not (Flag127prime-))
(not (Flag118prime-))
(not (Flag107prime-))
(not (Flag95prime-))
(not (Flag84prime-))
(not (Flag70prime-))
(not (Flag56prime-))
(not (Flag1prime-))
(not (Flag314prime-))
(not (Flag311prime-))
(not (Flag307prime-))
(not (Flag303prime-))
(not (Flag298prime-))
(not (Flag292prime-))
(not (Flag285prime-))
(not (Flag276prime-))
(not (Flag265prime-))
(not (Flag255prime-))
(not (Flag242prime-))
(not (Flag228prime-))
(not (Flag215prime-))
(not (Flag200prime-))
)
)
(:action MOVERIGHT-ROBOT
:parameters ()
:precondition
(and
(Flag3-)
(Flag3prime-)
(Flag311prime-)
(Flag307prime-)
(Flag303prime-)
(Flag298prime-)
(Flag292prime-)
(Flag285prime-)
(Flag276prime-)
(Flag265prime-)
(Flag255prime-)
(Flag242prime-)
(Flag228prime-)
(Flag215prime-)
(Flag200prime-)
(Flag185prime-)
(Flag184prime-)
(Flag183prime-)
(Flag182prime-)
(Flag181prime-)
(Flag180prime-)
(Flag179prime-)
(Flag178prime-)
(Flag177prime-)
(Flag176prime-)
(Flag175prime-)
(Flag174prime-)
(Flag173prime-)
(Flag172prime-)
(Flag171prime-)
(Flag170prime-)
(Flag169prime-)
(Flag168prime-)
(Flag167prime-)
(Flag166prime-)
(Flag165prime-)
(Flag164prime-)
(Flag163prime-)
(Flag162prime-)
(Flag161prime-)
)
:effect
(and
(when
(and
(Flag311-)
)
(and
(COLUMN13-ROBOT)
(not (NOT-COLUMN13-ROBOT))
)
)
(when
(and
(Flag307-)
)
(and
(COLUMN12-ROBOT)
(not (NOT-COLUMN12-ROBOT))
)
)
(when
(and
(Flag303-)
)
(and
(COLUMN11-ROBOT)
(not (NOT-COLUMN11-ROBOT))
)
)
(when
(and
(Flag298-)
)
(and
(COLUMN10-ROBOT)
(not (NOT-COLUMN10-ROBOT))
)
)
(when
(and
(Flag292-)
)
(and
(COLUMN9-ROBOT)
(not (NOT-COLUMN9-ROBOT))
)
)
(when
(and
(Flag285-)
)
(and
(COLUMN8-ROBOT)
(not (NOT-COLUMN8-ROBOT))
)
)
(when
(and
(Flag276-)
)
(and
(COLUMN7-ROBOT)
(not (NOT-COLUMN7-ROBOT))
)
)
(when
(and
(Flag265-)
)
(and
(COLUMN6-ROBOT)
(not (NOT-COLUMN6-ROBOT))
)
)
(when
(and
(Flag255-)
)
(and
(COLUMN5-ROBOT)
(not (NOT-COLUMN5-ROBOT))
)
)
(when
(and
(Flag242-)
)
(and
(COLUMN4-ROBOT)
(not (NOT-COLUMN4-ROBOT))
)
)
(when
(and
(Flag228-)
)
(and
(COLUMN3-ROBOT)
(not (NOT-COLUMN3-ROBOT))
)
)
(when
(and
(Flag215-)
)
(and
(COLUMN2-ROBOT)
(not (NOT-COLUMN2-ROBOT))
)
)
(when
(and
(Flag200-)
)
(and
(COLUMN1-ROBOT)
(not (NOT-COLUMN1-ROBOT))
)
)
(when
(and
(COLUMN12-ROBOT)
)
(and
(COLUMN13-ROBOT)
(NOT-COLUMN12-ROBOT)
(not (NOT-COLUMN13-ROBOT))
(not (COLUMN12-ROBOT))
)
)
(when
(and
(COLUMN11-ROBOT)
)
(and
(COLUMN12-ROBOT)
(NOT-COLUMN11-ROBOT)
(not (NOT-COLUMN12-ROBOT))
(not (COLUMN11-ROBOT))
)
)
(when
(and
(COLUMN10-ROBOT)
)
(and
(COLUMN11-ROBOT)
(NOT-COLUMN10-ROBOT)
(not (NOT-COLUMN11-ROBOT))
(not (COLUMN10-ROBOT))
)
)
(when
(and
(COLUMN9-ROBOT)
)
(and
(COLUMN10-ROBOT)
(NOT-COLUMN9-ROBOT)
(not (NOT-COLUMN10-ROBOT))
(not (COLUMN9-ROBOT))
)
)
(when
(and
(COLUMN8-ROBOT)
)
(and
(COLUMN9-ROBOT)
(NOT-COLUMN8-ROBOT)
(not (NOT-COLUMN9-ROBOT))
(not (COLUMN8-ROBOT))
)
)
(when
(and
(COLUMN7-ROBOT)
)
(and
(COLUMN8-ROBOT)
(NOT-COLUMN7-ROBOT)
(not (NOT-COLUMN8-ROBOT))
(not (COLUMN7-ROBOT))
)
)
(when
(and
(COLUMN6-ROBOT)
)
(and
(COLUMN7-ROBOT)
(NOT-COLUMN6-ROBOT)
(not (NOT-COLUMN7-ROBOT))
(not (COLUMN6-ROBOT))
)
)
(when
(and
(COLUMN5-ROBOT)
)
(and
(COLUMN6-ROBOT)
(NOT-COLUMN5-ROBOT)
(not (NOT-COLUMN6-ROBOT))
(not (COLUMN5-ROBOT))
)
)
(when
(and
(COLUMN4-ROBOT)
)
(and
(COLUMN5-ROBOT)
(NOT-COLUMN4-ROBOT)
(not (NOT-COLUMN5-ROBOT))
(not (COLUMN4-ROBOT))
)
)
(when
(and
(COLUMN3-ROBOT)
)
(and
(COLUMN4-ROBOT)
(NOT-COLUMN3-ROBOT)
(not (NOT-COLUMN4-ROBOT))
(not (COLUMN3-ROBOT))
)
)
(when
(and
(COLUMN2-ROBOT)
)
(and
(COLUMN3-ROBOT)
(NOT-COLUMN2-ROBOT)
(not (NOT-COLUMN3-ROBOT))
(not (COLUMN2-ROBOT))
)
)
(when
(and
(COLUMN1-ROBOT)
)
(and
(COLUMN2-ROBOT)
(NOT-COLUMN1-ROBOT)
(not (NOT-COLUMN2-ROBOT))
(not (COLUMN1-ROBOT))
)
)
(when
(and
(COLUMN0-ROBOT)
)
(and
(COLUMN1-ROBOT)
(NOT-COLUMN0-ROBOT)
(not (NOT-COLUMN1-ROBOT))
(not (COLUMN0-ROBOT))
)
)
(when
(and
(Flag185-)
)
(and
(LEFTOF14-ROBOT)
(NOT-LEFTOF13-ROBOT)
(not (NOT-LEFTOF14-ROBOT))
(not (LEFTOF13-ROBOT))
)
)
(when
(and
(Flag184-)
)
(and
(LEFTOF13-ROBOT)
(NOT-LEFTOF12-ROBOT)
(not (NOT-LEFTOF13-ROBOT))
(not (LEFTOF12-ROBOT))
)
)
(when
(and
(Flag183-)
)
(and
(LEFTOF12-ROBOT)
(NOT-LEFTOF11-ROBOT)
(not (NOT-LEFTOF12-ROBOT))
(not (LEFTOF11-ROBOT))
)
)
(when
(and
(Flag182-)
)
(and
(LEFTOF11-ROBOT)
(NOT-LEFTOF10-ROBOT)
(not (NOT-LEFTOF11-ROBOT))
(not (LEFTOF10-ROBOT))
)
)
(when
(and
(Flag181-)
)
(and
(LEFTOF10-ROBOT)
(NOT-LEFTOF9-ROBOT)
(not (NOT-LEFTOF10-ROBOT))
(not (LEFTOF9-ROBOT))
)
)
(when
(and
(Flag180-)
)
(and
(LEFTOF9-ROBOT)
(NOT-LEFTOF8-ROBOT)
(not (NOT-LEFTOF9-ROBOT))
(not (LEFTOF8-ROBOT))
)
)
(when
(and
(Flag179-)
)
(and
(LEFTOF8-ROBOT)
(NOT-LEFTOF7-ROBOT)
(not (NOT-LEFTOF8-ROBOT))
(not (LEFTOF7-ROBOT))
)
)
(when
(and
(Flag178-)
)
(and
(LEFTOF7-ROBOT)
(NOT-LEFTOF6-ROBOT)
(not (NOT-LEFTOF7-ROBOT))
(not (LEFTOF6-ROBOT))
)
)
(when
(and
(Flag177-)
)
(and
(LEFTOF6-ROBOT)
(NOT-LEFTOF5-ROBOT)
(not (NOT-LEFTOF6-ROBOT))
(not (LEFTOF5-ROBOT))
)
)
(when
(and
(Flag176-)
)
(and
(LEFTOF5-ROBOT)
(NOT-LEFTOF4-ROBOT)
(not (NOT-LEFTOF5-ROBOT))
(not (LEFTOF4-ROBOT))
)
)
(when
(and
(Flag175-)
)
(and
(LEFTOF4-ROBOT)
(NOT-LEFTOF3-ROBOT)
(not (NOT-LEFTOF4-ROBOT))
(not (LEFTOF3-ROBOT))
)
)
(when
(and
(Flag174-)
)
(and
(LEFTOF3-ROBOT)
(NOT-LEFTOF2-ROBOT)
(not (NOT-LEFTOF3-ROBOT))
(not (LEFTOF2-ROBOT))
)
)
(when
(and
(LEFTOF1-ROBOT)
)
(and
(LEFTOF2-ROBOT)
(NOT-LEFTOF1-ROBOT)
(not (NOT-LEFTOF2-ROBOT))
(not (LEFTOF1-ROBOT))
)
)
(when
(and
(Flag173-)
)
(and
(RIGHTOF13-ROBOT)
(not (NOT-RIGHTOF13-ROBOT))
)
)
(when
(and
(Flag172-)
)
(and
(RIGHTOF12-ROBOT)
(not (NOT-RIGHTOF12-ROBOT))
)
)
(when
(and
(Flag171-)
)
(and
(RIGHTOF11-ROBOT)
(not (NOT-RIGHTOF11-ROBOT))
)
)
(when
(and
(Flag170-)
)
(and
(RIGHTOF10-ROBOT)
(not (NOT-RIGHTOF10-ROBOT))
)
)
(when
(and
(Flag169-)
)
(and
(RIGHTOF9-ROBOT)
(not (NOT-RIGHTOF9-ROBOT))
)
)
(when
(and
(Flag168-)
)
(and
(RIGHTOF8-ROBOT)
(not (NOT-RIGHTOF8-ROBOT))
)
)
(when
(and
(Flag167-)
)
(and
(RIGHTOF7-ROBOT)
(not (NOT-RIGHTOF7-ROBOT))
)
)
(when
(and
(Flag166-)
)
(and
(RIGHTOF6-ROBOT)
(not (NOT-RIGHTOF6-ROBOT))
)
)
(when
(and
(Flag165-)
)
(and
(RIGHTOF5-ROBOT)
(not (NOT-RIGHTOF5-ROBOT))
)
)
(when
(and
(Flag164-)
)
(and
(RIGHTOF4-ROBOT)
(not (NOT-RIGHTOF4-ROBOT))
)
)
(when
(and
(Flag163-)
)
(and
(RIGHTOF3-ROBOT)
(not (NOT-RIGHTOF3-ROBOT))
)
)
(when
(and
(Flag162-)
)
(and
(RIGHTOF2-ROBOT)
(not (NOT-RIGHTOF2-ROBOT))
)
)
(when
(and
(Flag161-)
)
(and
(RIGHTOF1-ROBOT)
(not (NOT-RIGHTOF1-ROBOT))
)
)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag312prime-))
(not (Flag185prime-))
(not (Flag163prime-))
(not (Flag162prime-))
(not (Flag161prime-))
(not (Flag157prime-))
(not (Flag28prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag159prime-))
(not (Flag158prime-))
(not (Flag155prime-))
(not (Flag154prime-))
(not (Flag153prime-))
(not (Flag151prime-))
(not (Flag150prime-))
(not (Flag149prime-))
(not (Flag147prime-))
(not (Flag146prime-))
(not (Flag145prime-))
(not (Flag144prime-))
(not (Flag142prime-))
(not (Flag141prime-))
(not (Flag140prime-))
(not (Flag139prime-))
(not (Flag138prime-))
(not (Flag137prime-))
(not (Flag135prime-))
(not (Flag134prime-))
(not (Flag133prime-))
(not (Flag132prime-))
(not (Flag131prime-))
(not (Flag130prime-))
(not (Flag129prime-))
(not (Flag128prime-))
(not (Flag126prime-))
(not (Flag125prime-))
(not (Flag124prime-))
(not (Flag123prime-))
(not (Flag122prime-))
(not (Flag121prime-))
(not (Flag120prime-))
(not (Flag119prime-))
(not (Flag117prime-))
(not (Flag116prime-))
(not (Flag115prime-))
(not (Flag114prime-))
(not (Flag113prime-))
(not (Flag112prime-))
(not (Flag111prime-))
(not (Flag110prime-))
(not (Flag109prime-))
(not (Flag108prime-))
(not (Flag106prime-))
(not (Flag105prime-))
(not (Flag104prime-))
(not (Flag103prime-))
(not (Flag102prime-))
(not (Flag101prime-))
(not (Flag100prime-))
(not (Flag99prime-))
(not (Flag98prime-))
(not (Flag97prime-))
(not (Flag96prime-))
(not (Flag94prime-))
(not (Flag93prime-))
(not (Flag92prime-))
(not (Flag91prime-))
(not (Flag90prime-))
(not (Flag89prime-))
(not (Flag88prime-))
(not (Flag87prime-))
(not (Flag86prime-))
(not (Flag85prime-))
(not (Flag83prime-))
(not (Flag82prime-))
(not (Flag81prime-))
(not (Flag80prime-))
(not (Flag79prime-))
(not (Flag78prime-))
(not (Flag77prime-))
(not (Flag76prime-))
(not (Flag75prime-))
(not (Flag74prime-))
(not (Flag73prime-))
(not (Flag72prime-))
(not (Flag71prime-))
(not (Flag69prime-))
(not (Flag68prime-))
(not (Flag67prime-))
(not (Flag66prime-))
(not (Flag65prime-))
(not (Flag64prime-))
(not (Flag63prime-))
(not (Flag62prime-))
(not (Flag61prime-))
(not (Flag60prime-))
(not (Flag59prime-))
(not (Flag58prime-))
(not (Flag57prime-))
(not (Flag55prime-))
(not (Flag54prime-))
(not (Flag53prime-))
(not (Flag52prime-))
(not (Flag51prime-))
(not (Flag50prime-))
(not (Flag49prime-))
(not (Flag48prime-))
(not (Flag47prime-))
(not (Flag46prime-))
(not (Flag45prime-))
(not (Flag44prime-))
(not (Flag43prime-))
(not (Flag42prime-))
(not (Flag41prime-))
(not (Flag40prime-))
(not (Flag39prime-))
(not (Flag38prime-))
(not (Flag37prime-))
(not (Flag36prime-))
(not (Flag35prime-))
(not (Flag34prime-))
(not (Flag33prime-))
(not (Flag32prime-))
(not (Flag31prime-))
(not (Flag30prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag26prime-))
(not (Flag25prime-))
(not (Flag24prime-))
(not (Flag23prime-))
(not (Flag22prime-))
(not (Flag21prime-))
(not (Flag20prime-))
(not (Flag19prime-))
(not (Flag18prime-))
(not (Flag17prime-))
(not (Flag16prime-))
(not (Flag15prime-))
(not (Flag14prime-))
(not (Flag13prime-))
(not (Flag12prime-))
(not (Flag11prime-))
(not (Flag10prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag313prime-))
(not (Flag310prime-))
(not (Flag309prime-))
(not (Flag308prime-))
(not (Flag306prime-))
(not (Flag305prime-))
(not (Flag304prime-))
(not (Flag302prime-))
(not (Flag301prime-))
(not (Flag300prime-))
(not (Flag299prime-))
(not (Flag297prime-))
(not (Flag296prime-))
(not (Flag295prime-))
(not (Flag294prime-))
(not (Flag293prime-))
(not (Flag291prime-))
(not (Flag290prime-))
(not (Flag289prime-))
(not (Flag288prime-))
(not (Flag287prime-))
(not (Flag286prime-))
(not (Flag284prime-))
(not (Flag283prime-))
(not (Flag282prime-))
(not (Flag281prime-))
(not (Flag280prime-))
(not (Flag279prime-))
(not (Flag278prime-))
(not (Flag277prime-))
(not (Flag275prime-))
(not (Flag274prime-))
(not (Flag273prime-))
(not (Flag272prime-))
(not (Flag271prime-))
(not (Flag270prime-))
(not (Flag269prime-))
(not (Flag268prime-))
(not (Flag267prime-))
(not (Flag266prime-))
(not (Flag264prime-))
(not (Flag263prime-))
(not (Flag262prime-))
(not (Flag261prime-))
(not (Flag260prime-))
(not (Flag259prime-))
(not (Flag258prime-))
(not (Flag257prime-))
(not (Flag256prime-))
(not (Flag254prime-))
(not (Flag253prime-))
(not (Flag252prime-))
(not (Flag251prime-))
(not (Flag250prime-))
(not (Flag249prime-))
(not (Flag248prime-))
(not (Flag247prime-))
(not (Flag246prime-))
(not (Flag245prime-))
(not (Flag244prime-))
(not (Flag243prime-))
(not (Flag241prime-))
(not (Flag240prime-))
(not (Flag239prime-))
(not (Flag238prime-))
(not (Flag237prime-))
(not (Flag236prime-))
(not (Flag235prime-))
(not (Flag234prime-))
(not (Flag233prime-))
(not (Flag232prime-))
(not (Flag231prime-))
(not (Flag230prime-))
(not (Flag229prime-))
(not (Flag227prime-))
(not (Flag226prime-))
(not (Flag225prime-))
(not (Flag224prime-))
(not (Flag223prime-))
(not (Flag222prime-))
(not (Flag221prime-))
(not (Flag220prime-))
(not (Flag219prime-))
(not (Flag218prime-))
(not (Flag217prime-))
(not (Flag216prime-))
(not (Flag214prime-))
(not (Flag213prime-))
(not (Flag212prime-))
(not (Flag211prime-))
(not (Flag210prime-))
(not (Flag209prime-))
(not (Flag208prime-))
(not (Flag207prime-))
(not (Flag206prime-))
(not (Flag205prime-))
(not (Flag204prime-))
(not (Flag203prime-))
(not (Flag202prime-))
(not (Flag201prime-))
(not (Flag199prime-))
(not (Flag198prime-))
(not (Flag197prime-))
(not (Flag196prime-))
(not (Flag195prime-))
(not (Flag194prime-))
(not (Flag193prime-))
(not (Flag192prime-))
(not (Flag191prime-))
(not (Flag190prime-))
(not (Flag189prime-))
(not (Flag188prime-))
(not (Flag187prime-))
(not (Flag186prime-))
(not (Flag184prime-))
(not (Flag183prime-))
(not (Flag182prime-))
(not (Flag181prime-))
(not (Flag180prime-))
(not (Flag179prime-))
(not (Flag178prime-))
(not (Flag177prime-))
(not (Flag176prime-))
(not (Flag175prime-))
(not (Flag174prime-))
(not (Flag173prime-))
(not (Flag172prime-))
(not (Flag171prime-))
(not (Flag170prime-))
(not (Flag169prime-))
(not (Flag168prime-))
(not (Flag167prime-))
(not (Flag166prime-))
(not (Flag165prime-))
(not (Flag164prime-))
(not (Flag160prime-))
(not (Flag156prime-))
(not (Flag152prime-))
(not (Flag148prime-))
(not (Flag143prime-))
(not (Flag136prime-))
(not (Flag127prime-))
(not (Flag118prime-))
(not (Flag107prime-))
(not (Flag95prime-))
(not (Flag84prime-))
(not (Flag70prime-))
(not (Flag56prime-))
(not (Flag1prime-))
(not (Flag314prime-))
(not (Flag311prime-))
(not (Flag307prime-))
(not (Flag303prime-))
(not (Flag298prime-))
(not (Flag292prime-))
(not (Flag285prime-))
(not (Flag276prime-))
(not (Flag265prime-))
(not (Flag255prime-))
(not (Flag242prime-))
(not (Flag228prime-))
(not (Flag215prime-))
(not (Flag200prime-))
)
)
(:action Prim1Action-1
:parameters ()
:precondition
(and
(not (ROW1-ROBOT))
)
:effect
(and
(Flag1prime-)
(not (Flag1-))
)

)
(:action CHECKCONSISTENCYACTION
:parameters ()
:precondition
(and
(NOT-ERROR-)
(CHECKCONSISTENCY-)
)
:effect
(and
(when
(and
(BELOWOF2-ROBOT)
(ABOVEOF9-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF7-ROBOT)
(BELOWOF1-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF1-ROBOT)
(RIGHTOF12-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF3-ROBOT)
(RIGHTOF8-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF6-ROBOT)
(LEFTOF2-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF13-ROBOT)
(LEFTOF7-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF1-ROBOT)
(ABOVEOF4-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF11-ROBOT)
(LEFTOF4-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF9-ROBOT)
(RIGHTOF13-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF8-ROBOT)
(BELOWOF1-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF3-ROBOT)
(ABOVEOF8-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF10-ROBOT)
(LEFTOF1-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF3-ROBOT)
(LEFTOF1-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF2-ROBOT)
(RIGHTOF11-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF11-ROBOT)
(BELOWOF11-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF1-ROBOT)
(RIGHTOF13-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF13-ROBOT)
(BELOWOF9-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF12-ROBOT)
(BELOWOF6-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF6-ROBOT)
(LEFTOF3-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF10-ROBOT)
(RIGHTOF12-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF7-ROBOT)
(LEFTOF3-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF2-ROBOT)
(RIGHTOF12-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF2-ROBOT)
(BELOWOF1-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF3-ROBOT)
(ABOVEOF12-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF1-ROBOT)
(ABOVEOF12-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF2-ROBOT)
(BELOWOF2-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF12-ROBOT)
(LEFTOF7-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF3-ROBOT)
(ABOVEOF9-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF12-ROBOT)
(BELOWOF4-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF11-ROBOT)
(LEFTOF5-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF2-ROBOT)
(ABOVEOF11-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF2-ROBOT)
(ABOVEOF8-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF13-ROBOT)
(BELOWOF7-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF11-ROBOT)
(RIGHTOF12-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF7-ROBOT)
(LEFTOF1-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF11-ROBOT)
(BELOWOF6-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF13-ROBOT)
(LEFTOF2-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF9-ROBOT)
(ABOVEOF10-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF7-ROBOT)
(RIGHTOF9-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF6-ROBOT)
(LEFTOF6-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF2-ROBOT)
(ABOVEOF7-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF12-ROBOT)
(BELOWOF5-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF4-ROBOT)
(ABOVEOF9-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF11-ROBOT)
(BELOWOF7-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF4-ROBOT)
(RIGHTOF10-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF11-ROBOT)
(LEFTOF8-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF13-ROBOT)
(BELOWOF3-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF2-ROBOT)
(RIGHTOF5-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF2-ROBOT)
(RIGHTOF9-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF12-ROBOT)
(LEFTOF3-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF9-ROBOT)
(LEFTOF5-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF1-ROBOT)
(ABOVEOF13-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF7-ROBOT)
(BELOWOF6-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF1-ROBOT)
(RIGHTOF9-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF7-ROBOT)
(RIGHTOF8-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF6-ROBOT)
(BELOWOF3-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF8-ROBOT)
(ABOVEOF12-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF10-ROBOT)
(RIGHTOF11-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF1-ROBOT)
(RIGHTOF1-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF13-ROBOT)
(LEFTOF3-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF13-ROBOT)
(LEFTOF6-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF5-ROBOT)
(RIGHTOF12-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF7-ROBOT)
(ABOVEOF10-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF12-ROBOT)
(BELOWOF2-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF8-ROBOT)
(ABOVEOF11-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF5-ROBOT)
(RIGHTOF6-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF3-ROBOT)
(ABOVEOF10-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF7-ROBOT)
(LEFTOF7-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF6-ROBOT)
(RIGHTOF10-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF9-ROBOT)
(LEFTOF4-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF11-ROBOT)
(BELOWOF1-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF5-ROBOT)
(ABOVEOF11-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF8-ROBOT)
(BELOWOF4-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF4-ROBOT)
(RIGHTOF5-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF2-ROBOT)
(RIGHTOF10-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF13-ROBOT)
(LEFTOF4-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF3-ROBOT)
(ABOVEOF11-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF11-ROBOT)
(ABOVEOF13-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF5-ROBOT)
(ABOVEOF9-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF1-ROBOT)
(RIGHTOF5-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF8-ROBOT)
(LEFTOF2-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF8-ROBOT)
(BELOWOF7-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF12-ROBOT)
(BELOWOF7-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF12-ROBOT)
(BELOWOF11-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF8-ROBOT)
(RIGHTOF10-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF10-ROBOT)
(BELOWOF6-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF5-ROBOT)
(ABOVEOF13-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF4-ROBOT)
(ABOVEOF13-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF11-ROBOT)
(LEFTOF7-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF12-ROBOT)
(LEFTOF4-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF3-ROBOT)
(LEFTOF3-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF4-ROBOT)
(LEFTOF1-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF6-ROBOT)
(ABOVEOF13-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF8-ROBOT)
(RIGHTOF8-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF5-ROBOT)
(RIGHTOF7-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF13-ROBOT)
(BELOWOF2-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF1-ROBOT)
(RIGHTOF11-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF7-ROBOT)
(LEFTOF4-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF8-ROBOT)
(RIGHTOF9-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF3-ROBOT)
(RIGHTOF11-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF8-ROBOT)
(RIGHTOF13-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF8-ROBOT)
(ABOVEOF13-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF7-ROBOT)
(LEFTOF6-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF6-ROBOT)
(ABOVEOF9-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF13-ROBOT)
(BELOWOF12-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF10-ROBOT)
(LEFTOF5-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF9-ROBOT)
(BELOWOF7-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF9-ROBOT)
(ABOVEOF11-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF2-ROBOT)
(ABOVEOF6-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF13-ROBOT)
(LEFTOF5-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF1-ROBOT)
(ABOVEOF9-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF8-ROBOT)
(BELOWOF6-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF3-ROBOT)
(ABOVEOF7-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF1-ROBOT)
(RIGHTOF8-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF10-ROBOT)
(ABOVEOF13-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF4-ROBOT)
(ABOVEOF10-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF6-ROBOT)
(RIGHTOF11-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF13-ROBOT)
(LEFTOF11-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF1-ROBOT)
(ABOVEOF10-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF6-ROBOT)
(RIGHTOF12-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF10-ROBOT)
(BELOWOF2-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF7-ROBOT)
(RIGHTOF10-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF2-ROBOT)
(RIGHTOF4-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF10-ROBOT)
(BELOWOF8-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF8-ROBOT)
(BELOWOF5-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF9-ROBOT)
(LEFTOF6-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF2-ROBOT)
(ABOVEOF4-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF1-ROBOT)
(ABOVEOF6-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF6-ROBOT)
(LEFTOF1-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF1-ROBOT)
(ABOVEOF1-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF5-ROBOT)
(ABOVEOF10-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF13-ROBOT)
(LEFTOF10-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF11-ROBOT)
(BELOWOF4-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF9-ROBOT)
(ABOVEOF12-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF3-ROBOT)
(RIGHTOF9-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF8-ROBOT)
(RIGHTOF12-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF4-ROBOT)
(ABOVEOF5-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF10-ROBOT)
(LEFTOF10-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF1-ROBOT)
(RIGHTOF2-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF4-ROBOT)
(LEFTOF3-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF6-ROBOT)
(ABOVEOF6-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF11-ROBOT)
(BELOWOF10-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF9-ROBOT)
(ABOVEOF9-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF7-ROBOT)
(BELOWOF4-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF8-ROBOT)
(LEFTOF4-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF5-ROBOT)
(ABOVEOF5-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF5-ROBOT)
(RIGHTOF5-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF5-ROBOT)
(RIGHTOF8-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF10-ROBOT)
(LEFTOF3-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF1-ROBOT)
(ABOVEOF5-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF9-ROBOT)
(RIGHTOF11-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF2-ROBOT)
(ABOVEOF5-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF3-ROBOT)
(ABOVEOF4-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF2-ROBOT)
(ABOVEOF3-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF12-ROBOT)
(LEFTOF9-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF5-ROBOT)
(LEFTOF3-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF7-ROBOT)
(BELOWOF5-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF12-ROBOT)
(BELOWOF12-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF6-ROBOT)
(BELOWOF4-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF2-ROBOT)
(RIGHTOF7-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF8-ROBOT)
(LEFTOF6-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF4-ROBOT)
(RIGHTOF6-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF12-ROBOT)
(RIGHTOF12-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF11-ROBOT)
(RIGHTOF11-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF12-ROBOT)
(BELOWOF10-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF13-ROBOT)
(LEFTOF12-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF3-ROBOT)
(LEFTOF2-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF9-ROBOT)
(BELOWOF8-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF1-ROBOT)
(ABOVEOF3-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF6-ROBOT)
(BELOWOF5-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF9-ROBOT)
(LEFTOF9-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF13-ROBOT)
(LEFTOF13-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF3-ROBOT)
(ABOVEOF3-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF3-ROBOT)
(ABOVEOF5-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF4-ROBOT)
(ABOVEOF4-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF10-ROBOT)
(ABOVEOF10-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF4-ROBOT)
(RIGHTOF4-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF8-ROBOT)
(BELOWOF8-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF13-ROBOT)
(BELOWOF13-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF7-ROBOT)
(ABOVEOF7-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF9-ROBOT)
(RIGHTOF10-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF2-ROBOT)
(RIGHTOF2-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(NOT-CHECKCONSISTENCY-)
(not (CHECKCONSISTENCY-))
(not (Flag312prime-))
(not (Flag185prime-))
(not (Flag163prime-))
(not (Flag162prime-))
(not (Flag161prime-))
(not (Flag157prime-))
(not (Flag28prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag159prime-))
(not (Flag158prime-))
(not (Flag155prime-))
(not (Flag154prime-))
(not (Flag153prime-))
(not (Flag151prime-))
(not (Flag150prime-))
(not (Flag149prime-))
(not (Flag147prime-))
(not (Flag146prime-))
(not (Flag145prime-))
(not (Flag144prime-))
(not (Flag142prime-))
(not (Flag141prime-))
(not (Flag140prime-))
(not (Flag139prime-))
(not (Flag138prime-))
(not (Flag137prime-))
(not (Flag135prime-))
(not (Flag134prime-))
(not (Flag133prime-))
(not (Flag132prime-))
(not (Flag131prime-))
(not (Flag130prime-))
(not (Flag129prime-))
(not (Flag128prime-))
(not (Flag126prime-))
(not (Flag125prime-))
(not (Flag124prime-))
(not (Flag123prime-))
(not (Flag122prime-))
(not (Flag121prime-))
(not (Flag120prime-))
(not (Flag119prime-))
(not (Flag117prime-))
(not (Flag116prime-))
(not (Flag115prime-))
(not (Flag114prime-))
(not (Flag113prime-))
(not (Flag112prime-))
(not (Flag111prime-))
(not (Flag110prime-))
(not (Flag109prime-))
(not (Flag108prime-))
(not (Flag106prime-))
(not (Flag105prime-))
(not (Flag104prime-))
(not (Flag103prime-))
(not (Flag102prime-))
(not (Flag101prime-))
(not (Flag100prime-))
(not (Flag99prime-))
(not (Flag98prime-))
(not (Flag97prime-))
(not (Flag96prime-))
(not (Flag94prime-))
(not (Flag93prime-))
(not (Flag92prime-))
(not (Flag91prime-))
(not (Flag90prime-))
(not (Flag89prime-))
(not (Flag88prime-))
(not (Flag87prime-))
(not (Flag86prime-))
(not (Flag85prime-))
(not (Flag83prime-))
(not (Flag82prime-))
(not (Flag81prime-))
(not (Flag80prime-))
(not (Flag79prime-))
(not (Flag78prime-))
(not (Flag77prime-))
(not (Flag76prime-))
(not (Flag75prime-))
(not (Flag74prime-))
(not (Flag73prime-))
(not (Flag72prime-))
(not (Flag71prime-))
(not (Flag69prime-))
(not (Flag68prime-))
(not (Flag67prime-))
(not (Flag66prime-))
(not (Flag65prime-))
(not (Flag64prime-))
(not (Flag63prime-))
(not (Flag62prime-))
(not (Flag61prime-))
(not (Flag60prime-))
(not (Flag59prime-))
(not (Flag58prime-))
(not (Flag57prime-))
(not (Flag55prime-))
(not (Flag54prime-))
(not (Flag53prime-))
(not (Flag52prime-))
(not (Flag51prime-))
(not (Flag50prime-))
(not (Flag49prime-))
(not (Flag48prime-))
(not (Flag47prime-))
(not (Flag46prime-))
(not (Flag45prime-))
(not (Flag44prime-))
(not (Flag43prime-))
(not (Flag42prime-))
(not (Flag41prime-))
(not (Flag40prime-))
(not (Flag39prime-))
(not (Flag38prime-))
(not (Flag37prime-))
(not (Flag36prime-))
(not (Flag35prime-))
(not (Flag34prime-))
(not (Flag33prime-))
(not (Flag32prime-))
(not (Flag31prime-))
(not (Flag30prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag26prime-))
(not (Flag25prime-))
(not (Flag24prime-))
(not (Flag23prime-))
(not (Flag22prime-))
(not (Flag21prime-))
(not (Flag20prime-))
(not (Flag19prime-))
(not (Flag18prime-))
(not (Flag17prime-))
(not (Flag16prime-))
(not (Flag15prime-))
(not (Flag14prime-))
(not (Flag13prime-))
(not (Flag12prime-))
(not (Flag11prime-))
(not (Flag10prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag313prime-))
(not (Flag310prime-))
(not (Flag309prime-))
(not (Flag308prime-))
(not (Flag306prime-))
(not (Flag305prime-))
(not (Flag304prime-))
(not (Flag302prime-))
(not (Flag301prime-))
(not (Flag300prime-))
(not (Flag299prime-))
(not (Flag297prime-))
(not (Flag296prime-))
(not (Flag295prime-))
(not (Flag294prime-))
(not (Flag293prime-))
(not (Flag291prime-))
(not (Flag290prime-))
(not (Flag289prime-))
(not (Flag288prime-))
(not (Flag287prime-))
(not (Flag286prime-))
(not (Flag284prime-))
(not (Flag283prime-))
(not (Flag282prime-))
(not (Flag281prime-))
(not (Flag280prime-))
(not (Flag279prime-))
(not (Flag278prime-))
(not (Flag277prime-))
(not (Flag275prime-))
(not (Flag274prime-))
(not (Flag273prime-))
(not (Flag272prime-))
(not (Flag271prime-))
(not (Flag270prime-))
(not (Flag269prime-))
(not (Flag268prime-))
(not (Flag267prime-))
(not (Flag266prime-))
(not (Flag264prime-))
(not (Flag263prime-))
(not (Flag262prime-))
(not (Flag261prime-))
(not (Flag260prime-))
(not (Flag259prime-))
(not (Flag258prime-))
(not (Flag257prime-))
(not (Flag256prime-))
(not (Flag254prime-))
(not (Flag253prime-))
(not (Flag252prime-))
(not (Flag251prime-))
(not (Flag250prime-))
(not (Flag249prime-))
(not (Flag248prime-))
(not (Flag247prime-))
(not (Flag246prime-))
(not (Flag245prime-))
(not (Flag244prime-))
(not (Flag243prime-))
(not (Flag241prime-))
(not (Flag240prime-))
(not (Flag239prime-))
(not (Flag238prime-))
(not (Flag237prime-))
(not (Flag236prime-))
(not (Flag235prime-))
(not (Flag234prime-))
(not (Flag233prime-))
(not (Flag232prime-))
(not (Flag231prime-))
(not (Flag230prime-))
(not (Flag229prime-))
(not (Flag227prime-))
(not (Flag226prime-))
(not (Flag225prime-))
(not (Flag224prime-))
(not (Flag223prime-))
(not (Flag222prime-))
(not (Flag221prime-))
(not (Flag220prime-))
(not (Flag219prime-))
(not (Flag218prime-))
(not (Flag217prime-))
(not (Flag216prime-))
(not (Flag214prime-))
(not (Flag213prime-))
(not (Flag212prime-))
(not (Flag211prime-))
(not (Flag210prime-))
(not (Flag209prime-))
(not (Flag208prime-))
(not (Flag207prime-))
(not (Flag206prime-))
(not (Flag205prime-))
(not (Flag204prime-))
(not (Flag203prime-))
(not (Flag202prime-))
(not (Flag201prime-))
(not (Flag199prime-))
(not (Flag198prime-))
(not (Flag197prime-))
(not (Flag196prime-))
(not (Flag195prime-))
(not (Flag194prime-))
(not (Flag193prime-))
(not (Flag192prime-))
(not (Flag191prime-))
(not (Flag190prime-))
(not (Flag189prime-))
(not (Flag188prime-))
(not (Flag187prime-))
(not (Flag186prime-))
(not (Flag184prime-))
(not (Flag183prime-))
(not (Flag182prime-))
(not (Flag181prime-))
(not (Flag180prime-))
(not (Flag179prime-))
(not (Flag178prime-))
(not (Flag177prime-))
(not (Flag176prime-))
(not (Flag175prime-))
(not (Flag174prime-))
(not (Flag173prime-))
(not (Flag172prime-))
(not (Flag171prime-))
(not (Flag170prime-))
(not (Flag169prime-))
(not (Flag168prime-))
(not (Flag167prime-))
(not (Flag166prime-))
(not (Flag165prime-))
(not (Flag164prime-))
(not (Flag160prime-))
(not (Flag156prime-))
(not (Flag152prime-))
(not (Flag148prime-))
(not (Flag143prime-))
(not (Flag136prime-))
(not (Flag127prime-))
(not (Flag118prime-))
(not (Flag107prime-))
(not (Flag95prime-))
(not (Flag84prime-))
(not (Flag70prime-))
(not (Flag56prime-))
(not (Flag1prime-))
(not (Flag314prime-))
(not (Flag311prime-))
(not (Flag307prime-))
(not (Flag303prime-))
(not (Flag298prime-))
(not (Flag292prime-))
(not (Flag285prime-))
(not (Flag276prime-))
(not (Flag265prime-))
(not (Flag255prime-))
(not (Flag242prime-))
(not (Flag228prime-))
(not (Flag215prime-))
(not (Flag200prime-))
)
)
(:action Flag3Action
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(Flag2-)
(Flag2prime-)
)
:effect
(and
(Flag3-)
(Flag3prime-)
)

)
(:action Prim3Action-2
:parameters ()
:precondition
(and
(not (Flag2-))
(Flag2prime-)
)
:effect
(and
(Flag3prime-)
(not (Flag3-))
)

)
(:action Flag5Action-0
:parameters ()
:precondition
(and
(ABOVEOF6-ROBOT)
)
:effect
(and
(Flag5-)
(Flag5prime-)
)

)
(:action Flag5Action-1
:parameters ()
:precondition
(and
(ABOVEOF11-ROBOT)
)
:effect
(and
(Flag5-)
(Flag5prime-)
)

)
(:action Flag5Action-2
:parameters ()
:precondition
(and
(ABOVEOF10-ROBOT)
)
:effect
(and
(Flag5-)
(Flag5prime-)
)

)
(:action Flag5Action-3
:parameters ()
:precondition
(and
(ABOVEOF2-ROBOT)
)
:effect
(and
(Flag5-)
(Flag5prime-)
)

)
(:action Flag5Action-4
:parameters ()
:precondition
(and
(ABOVEOF7-ROBOT)
)
:effect
(and
(Flag5-)
(Flag5prime-)
)

)
(:action Flag5Action-5
:parameters ()
:precondition
(and
(ABOVEOF12-ROBOT)
)
:effect
(and
(Flag5-)
(Flag5prime-)
)

)
(:action Flag5Action-6
:parameters ()
:precondition
(and
(ABOVEOF8-ROBOT)
)
:effect
(and
(Flag5-)
(Flag5prime-)
)

)
(:action Flag5Action-7
:parameters ()
:precondition
(and
(ABOVEOF9-ROBOT)
)
:effect
(and
(Flag5-)
(Flag5prime-)
)

)
(:action Flag5Action-8
:parameters ()
:precondition
(and
(ABOVEOF4-ROBOT)
)
:effect
(and
(Flag5-)
(Flag5prime-)
)

)
(:action Flag5Action-9
:parameters ()
:precondition
(and
(ABOVEOF3-ROBOT)
)
:effect
(and
(Flag5-)
(Flag5prime-)
)

)
(:action Flag5Action-10
:parameters ()
:precondition
(and
(ABOVEOF13-ROBOT)
)
:effect
(and
(Flag5-)
(Flag5prime-)
)

)
(:action Flag5Action-11
:parameters ()
:precondition
(and
(ABOVEOF5-ROBOT)
)
:effect
(and
(Flag5-)
(Flag5prime-)
)

)
(:action Flag5Action-12
:parameters ()
:precondition
(and
(ABOVEOF1-ROBOT)
)
:effect
(and
(Flag5-)
(Flag5prime-)
)

)
(:action Prim5Action
:parameters ()
:precondition
(and
(not (ABOVEOF6-ROBOT))
(not (ABOVEOF11-ROBOT))
(not (ABOVEOF10-ROBOT))
(not (ABOVEOF2-ROBOT))
(not (ABOVEOF7-ROBOT))
(not (ABOVEOF12-ROBOT))
(not (ABOVEOF8-ROBOT))
(not (ABOVEOF9-ROBOT))
(not (ABOVEOF4-ROBOT))
(not (ABOVEOF3-ROBOT))
(not (ABOVEOF13-ROBOT))
(not (ABOVEOF5-ROBOT))
(not (ABOVEOF1-ROBOT))
)
:effect
(and
(Flag5prime-)
(not (Flag5-))
)

)
(:action Flag6Action-0
:parameters ()
:precondition
(and
(ABOVEOF11-ROBOT)
)
:effect
(and
(Flag6-)
(Flag6prime-)
)

)
(:action Flag6Action-1
:parameters ()
:precondition
(and
(ABOVEOF6-ROBOT)
)
:effect
(and
(Flag6-)
(Flag6prime-)
)

)
(:action Flag6Action-2
:parameters ()
:precondition
(and
(ABOVEOF10-ROBOT)
)
:effect
(and
(Flag6-)
(Flag6prime-)
)

)
(:action Flag6Action-3
:parameters ()
:precondition
(and
(ABOVEOF2-ROBOT)
)
:effect
(and
(Flag6-)
(Flag6prime-)
)

)
(:action Flag6Action-4
:parameters ()
:precondition
(and
(ABOVEOF7-ROBOT)
)
:effect
(and
(Flag6-)
(Flag6prime-)
)

)
(:action Flag6Action-5
:parameters ()
:precondition
(and
(ABOVEOF12-ROBOT)
)
:effect
(and
(Flag6-)
(Flag6prime-)
)

)
(:action Flag6Action-6
:parameters ()
:precondition
(and
(ABOVEOF8-ROBOT)
)
:effect
(and
(Flag6-)
(Flag6prime-)
)

)
(:action Flag6Action-7
:parameters ()
:precondition
(and
(ABOVEOF9-ROBOT)
)
:effect
(and
(Flag6-)
(Flag6prime-)
)

)
(:action Flag6Action-8
:parameters ()
:precondition
(and
(ABOVEOF4-ROBOT)
)
:effect
(and
(Flag6-)
(Flag6prime-)
)

)
(:action Flag6Action-9
:parameters ()
:precondition
(and
(ABOVEOF3-ROBOT)
)
:effect
(and
(Flag6-)
(Flag6prime-)
)

)
(:action Flag6Action-10
:parameters ()
:precondition
(and
(ABOVEOF13-ROBOT)
)
:effect
(and
(Flag6-)
(Flag6prime-)
)

)
(:action Flag6Action-11
:parameters ()
:precondition
(and
(ABOVEOF5-ROBOT)
)
:effect
(and
(Flag6-)
(Flag6prime-)
)

)
(:action Prim6Action
:parameters ()
:precondition
(and
(not (ABOVEOF11-ROBOT))
(not (ABOVEOF6-ROBOT))
(not (ABOVEOF10-ROBOT))
(not (ABOVEOF2-ROBOT))
(not (ABOVEOF7-ROBOT))
(not (ABOVEOF12-ROBOT))
(not (ABOVEOF8-ROBOT))
(not (ABOVEOF9-ROBOT))
(not (ABOVEOF4-ROBOT))
(not (ABOVEOF3-ROBOT))
(not (ABOVEOF13-ROBOT))
(not (ABOVEOF5-ROBOT))
)
:effect
(and
(Flag6prime-)
(not (Flag6-))
)

)
(:action Flag7Action-0
:parameters ()
:precondition
(and
(ABOVEOF11-ROBOT)
)
:effect
(and
(Flag7-)
(Flag7prime-)
)

)
(:action Flag7Action-1
:parameters ()
:precondition
(and
(ABOVEOF6-ROBOT)
)
:effect
(and
(Flag7-)
(Flag7prime-)
)

)
(:action Flag7Action-2
:parameters ()
:precondition
(and
(ABOVEOF10-ROBOT)
)
:effect
(and
(Flag7-)
(Flag7prime-)
)

)
(:action Flag7Action-3
:parameters ()
:precondition
(and
(ABOVEOF8-ROBOT)
)
:effect
(and
(Flag7-)
(Flag7prime-)
)

)
(:action Flag7Action-4
:parameters ()
:precondition
(and
(ABOVEOF7-ROBOT)
)
:effect
(and
(Flag7-)
(Flag7prime-)
)

)
(:action Flag7Action-5
:parameters ()
:precondition
(and
(ABOVEOF12-ROBOT)
)
:effect
(and
(Flag7-)
(Flag7prime-)
)

)
(:action Flag7Action-6
:parameters ()
:precondition
(and
(ABOVEOF9-ROBOT)
)
:effect
(and
(Flag7-)
(Flag7prime-)
)

)
(:action Flag7Action-7
:parameters ()
:precondition
(and
(ABOVEOF4-ROBOT)
)
:effect
(and
(Flag7-)
(Flag7prime-)
)

)
(:action Flag7Action-8
:parameters ()
:precondition
(and
(ABOVEOF3-ROBOT)
)
:effect
(and
(Flag7-)
(Flag7prime-)
)

)
(:action Flag7Action-9
:parameters ()
:precondition
(and
(ABOVEOF13-ROBOT)
)
:effect
(and
(Flag7-)
(Flag7prime-)
)

)
(:action Flag7Action-10
:parameters ()
:precondition
(and
(ABOVEOF5-ROBOT)
)
:effect
(and
(Flag7-)
(Flag7prime-)
)

)
(:action Prim7Action
:parameters ()
:precondition
(and
(not (ABOVEOF11-ROBOT))
(not (ABOVEOF6-ROBOT))
(not (ABOVEOF10-ROBOT))
(not (ABOVEOF8-ROBOT))
(not (ABOVEOF7-ROBOT))
(not (ABOVEOF12-ROBOT))
(not (ABOVEOF9-ROBOT))
(not (ABOVEOF4-ROBOT))
(not (ABOVEOF3-ROBOT))
(not (ABOVEOF13-ROBOT))
(not (ABOVEOF5-ROBOT))
)
:effect
(and
(Flag7prime-)
(not (Flag7-))
)

)
(:action Flag8Action-0
:parameters ()
:precondition
(and
(ABOVEOF11-ROBOT)
)
:effect
(and
(Flag8-)
(Flag8prime-)
)

)
(:action Flag8Action-1
:parameters ()
:precondition
(and
(ABOVEOF6-ROBOT)
)
:effect
(and
(Flag8-)
(Flag8prime-)
)

)
(:action Flag8Action-2
:parameters ()
:precondition
(and
(ABOVEOF10-ROBOT)
)
:effect
(and
(Flag8-)
(Flag8prime-)
)

)
(:action Flag8Action-3
:parameters ()
:precondition
(and
(ABOVEOF8-ROBOT)
)
:effect
(and
(Flag8-)
(Flag8prime-)
)

)
(:action Flag8Action-4
:parameters ()
:precondition
(and
(ABOVEOF7-ROBOT)
)
:effect
(and
(Flag8-)
(Flag8prime-)
)

)
(:action Flag8Action-5
:parameters ()
:precondition
(and
(ABOVEOF12-ROBOT)
)
:effect
(and
(Flag8-)
(Flag8prime-)
)

)
(:action Flag8Action-6
:parameters ()
:precondition
(and
(ABOVEOF9-ROBOT)
)
:effect
(and
(Flag8-)
(Flag8prime-)
)

)
(:action Flag8Action-7
:parameters ()
:precondition
(and
(ABOVEOF4-ROBOT)
)
:effect
(and
(Flag8-)
(Flag8prime-)
)

)
(:action Flag8Action-8
:parameters ()
:precondition
(and
(ABOVEOF13-ROBOT)
)
:effect
(and
(Flag8-)
(Flag8prime-)
)

)
(:action Flag8Action-9
:parameters ()
:precondition
(and
(ABOVEOF5-ROBOT)
)
:effect
(and
(Flag8-)
(Flag8prime-)
)

)
(:action Prim8Action
:parameters ()
:precondition
(and
(not (ABOVEOF11-ROBOT))
(not (ABOVEOF6-ROBOT))
(not (ABOVEOF10-ROBOT))
(not (ABOVEOF8-ROBOT))
(not (ABOVEOF7-ROBOT))
(not (ABOVEOF12-ROBOT))
(not (ABOVEOF9-ROBOT))
(not (ABOVEOF4-ROBOT))
(not (ABOVEOF13-ROBOT))
(not (ABOVEOF5-ROBOT))
)
:effect
(and
(Flag8prime-)
(not (Flag8-))
)

)
(:action Flag9Action-0
:parameters ()
:precondition
(and
(ABOVEOF11-ROBOT)
)
:effect
(and
(Flag9-)
(Flag9prime-)
)

)
(:action Flag9Action-1
:parameters ()
:precondition
(and
(ABOVEOF6-ROBOT)
)
:effect
(and
(Flag9-)
(Flag9prime-)
)

)
(:action Flag9Action-2
:parameters ()
:precondition
(and
(ABOVEOF7-ROBOT)
)
:effect
(and
(Flag9-)
(Flag9prime-)
)

)
(:action Flag9Action-3
:parameters ()
:precondition
(and
(ABOVEOF8-ROBOT)
)
:effect
(and
(Flag9-)
(Flag9prime-)
)

)
(:action Flag9Action-4
:parameters ()
:precondition
(and
(ABOVEOF10-ROBOT)
)
:effect
(and
(Flag9-)
(Flag9prime-)
)

)
(:action Flag9Action-5
:parameters ()
:precondition
(and
(ABOVEOF12-ROBOT)
)
:effect
(and
(Flag9-)
(Flag9prime-)
)

)
(:action Flag9Action-6
:parameters ()
:precondition
(and
(ABOVEOF9-ROBOT)
)
:effect
(and
(Flag9-)
(Flag9prime-)
)

)
(:action Flag9Action-7
:parameters ()
:precondition
(and
(ABOVEOF13-ROBOT)
)
:effect
(and
(Flag9-)
(Flag9prime-)
)

)
(:action Flag9Action-8
:parameters ()
:precondition
(and
(ABOVEOF5-ROBOT)
)
:effect
(and
(Flag9-)
(Flag9prime-)
)

)
(:action Prim9Action
:parameters ()
:precondition
(and
(not (ABOVEOF11-ROBOT))
(not (ABOVEOF6-ROBOT))
(not (ABOVEOF7-ROBOT))
(not (ABOVEOF8-ROBOT))
(not (ABOVEOF10-ROBOT))
(not (ABOVEOF12-ROBOT))
(not (ABOVEOF9-ROBOT))
(not (ABOVEOF13-ROBOT))
(not (ABOVEOF5-ROBOT))
)
:effect
(and
(Flag9prime-)
(not (Flag9-))
)

)
(:action Flag10Action-0
:parameters ()
:precondition
(and
(ABOVEOF11-ROBOT)
)
:effect
(and
(Flag10-)
(Flag10prime-)
)

)
(:action Flag10Action-1
:parameters ()
:precondition
(and
(ABOVEOF6-ROBOT)
)
:effect
(and
(Flag10-)
(Flag10prime-)
)

)
(:action Flag10Action-2
:parameters ()
:precondition
(and
(ABOVEOF7-ROBOT)
)
:effect
(and
(Flag10-)
(Flag10prime-)
)

)
(:action Flag10Action-3
:parameters ()
:precondition
(and
(ABOVEOF8-ROBOT)
)
:effect
(and
(Flag10-)
(Flag10prime-)
)

)
(:action Flag10Action-4
:parameters ()
:precondition
(and
(ABOVEOF10-ROBOT)
)
:effect
(and
(Flag10-)
(Flag10prime-)
)

)
(:action Flag10Action-5
:parameters ()
:precondition
(and
(ABOVEOF12-ROBOT)
)
:effect
(and
(Flag10-)
(Flag10prime-)
)

)
(:action Flag10Action-6
:parameters ()
:precondition
(and
(ABOVEOF9-ROBOT)
)
:effect
(and
(Flag10-)
(Flag10prime-)
)

)
(:action Flag10Action-7
:parameters ()
:precondition
(and
(ABOVEOF13-ROBOT)
)
:effect
(and
(Flag10-)
(Flag10prime-)
)

)
(:action Prim10Action
:parameters ()
:precondition
(and
(not (ABOVEOF11-ROBOT))
(not (ABOVEOF6-ROBOT))
(not (ABOVEOF7-ROBOT))
(not (ABOVEOF8-ROBOT))
(not (ABOVEOF10-ROBOT))
(not (ABOVEOF12-ROBOT))
(not (ABOVEOF9-ROBOT))
(not (ABOVEOF13-ROBOT))
)
:effect
(and
(Flag10prime-)
(not (Flag10-))
)

)
(:action Flag11Action-0
:parameters ()
:precondition
(and
(ABOVEOF11-ROBOT)
)
:effect
(and
(Flag11-)
(Flag11prime-)
)

)
(:action Flag11Action-1
:parameters ()
:precondition
(and
(ABOVEOF7-ROBOT)
)
:effect
(and
(Flag11-)
(Flag11prime-)
)

)
(:action Flag11Action-2
:parameters ()
:precondition
(and
(ABOVEOF8-ROBOT)
)
:effect
(and
(Flag11-)
(Flag11prime-)
)

)
(:action Flag11Action-3
:parameters ()
:precondition
(and
(ABOVEOF10-ROBOT)
)
:effect
(and
(Flag11-)
(Flag11prime-)
)

)
(:action Flag11Action-4
:parameters ()
:precondition
(and
(ABOVEOF12-ROBOT)
)
:effect
(and
(Flag11-)
(Flag11prime-)
)

)
(:action Flag11Action-5
:parameters ()
:precondition
(and
(ABOVEOF9-ROBOT)
)
:effect
(and
(Flag11-)
(Flag11prime-)
)

)
(:action Flag11Action-6
:parameters ()
:precondition
(and
(ABOVEOF13-ROBOT)
)
:effect
(and
(Flag11-)
(Flag11prime-)
)

)
(:action Prim11Action
:parameters ()
:precondition
(and
(not (ABOVEOF11-ROBOT))
(not (ABOVEOF7-ROBOT))
(not (ABOVEOF8-ROBOT))
(not (ABOVEOF10-ROBOT))
(not (ABOVEOF12-ROBOT))
(not (ABOVEOF9-ROBOT))
(not (ABOVEOF13-ROBOT))
)
:effect
(and
(Flag11prime-)
(not (Flag11-))
)

)
(:action Flag12Action-0
:parameters ()
:precondition
(and
(ABOVEOF11-ROBOT)
)
:effect
(and
(Flag12-)
(Flag12prime-)
)

)
(:action Flag12Action-1
:parameters ()
:precondition
(and
(ABOVEOF8-ROBOT)
)
:effect
(and
(Flag12-)
(Flag12prime-)
)

)
(:action Flag12Action-2
:parameters ()
:precondition
(and
(ABOVEOF10-ROBOT)
)
:effect
(and
(Flag12-)
(Flag12prime-)
)

)
(:action Flag12Action-3
:parameters ()
:precondition
(and
(ABOVEOF12-ROBOT)
)
:effect
(and
(Flag12-)
(Flag12prime-)
)

)
(:action Flag12Action-4
:parameters ()
:precondition
(and
(ABOVEOF9-ROBOT)
)
:effect
(and
(Flag12-)
(Flag12prime-)
)

)
(:action Flag12Action-5
:parameters ()
:precondition
(and
(ABOVEOF13-ROBOT)
)
:effect
(and
(Flag12-)
(Flag12prime-)
)

)
(:action Prim12Action
:parameters ()
:precondition
(and
(not (ABOVEOF11-ROBOT))
(not (ABOVEOF8-ROBOT))
(not (ABOVEOF10-ROBOT))
(not (ABOVEOF12-ROBOT))
(not (ABOVEOF9-ROBOT))
(not (ABOVEOF13-ROBOT))
)
:effect
(and
(Flag12prime-)
(not (Flag12-))
)

)
(:action Flag13Action-0
:parameters ()
:precondition
(and
(ABOVEOF13-ROBOT)
)
:effect
(and
(Flag13-)
(Flag13prime-)
)

)
(:action Flag13Action-1
:parameters ()
:precondition
(and
(ABOVEOF12-ROBOT)
)
:effect
(and
(Flag13-)
(Flag13prime-)
)

)
(:action Flag13Action-2
:parameters ()
:precondition
(and
(ABOVEOF11-ROBOT)
)
:effect
(and
(Flag13-)
(Flag13prime-)
)

)
(:action Flag13Action-3
:parameters ()
:precondition
(and
(ABOVEOF10-ROBOT)
)
:effect
(and
(Flag13-)
(Flag13prime-)
)

)
(:action Flag13Action-4
:parameters ()
:precondition
(and
(ABOVEOF9-ROBOT)
)
:effect
(and
(Flag13-)
(Flag13prime-)
)

)
(:action Prim13Action
:parameters ()
:precondition
(and
(not (ABOVEOF13-ROBOT))
(not (ABOVEOF12-ROBOT))
(not (ABOVEOF11-ROBOT))
(not (ABOVEOF10-ROBOT))
(not (ABOVEOF9-ROBOT))
)
:effect
(and
(Flag13prime-)
(not (Flag13-))
)

)
(:action Flag14Action-0
:parameters ()
:precondition
(and
(ABOVEOF12-ROBOT)
)
:effect
(and
(Flag14-)
(Flag14prime-)
)

)
(:action Flag14Action-1
:parameters ()
:precondition
(and
(ABOVEOF13-ROBOT)
)
:effect
(and
(Flag14-)
(Flag14prime-)
)

)
(:action Flag14Action-2
:parameters ()
:precondition
(and
(ABOVEOF10-ROBOT)
)
:effect
(and
(Flag14-)
(Flag14prime-)
)

)
(:action Flag14Action-3
:parameters ()
:precondition
(and
(ABOVEOF11-ROBOT)
)
:effect
(and
(Flag14-)
(Flag14prime-)
)

)
(:action Prim14Action
:parameters ()
:precondition
(and
(not (ABOVEOF12-ROBOT))
(not (ABOVEOF13-ROBOT))
(not (ABOVEOF10-ROBOT))
(not (ABOVEOF11-ROBOT))
)
:effect
(and
(Flag14prime-)
(not (Flag14-))
)

)
(:action Flag15Action-0
:parameters ()
:precondition
(and
(ABOVEOF12-ROBOT)
)
:effect
(and
(Flag15-)
(Flag15prime-)
)

)
(:action Flag15Action-1
:parameters ()
:precondition
(and
(ABOVEOF13-ROBOT)
)
:effect
(and
(Flag15-)
(Flag15prime-)
)

)
(:action Flag15Action-2
:parameters ()
:precondition
(and
(ABOVEOF11-ROBOT)
)
:effect
(and
(Flag15-)
(Flag15prime-)
)

)
(:action Prim15Action
:parameters ()
:precondition
(and
(not (ABOVEOF12-ROBOT))
(not (ABOVEOF13-ROBOT))
(not (ABOVEOF11-ROBOT))
)
:effect
(and
(Flag15prime-)
(not (Flag15-))
)

)
(:action Flag16Action-0
:parameters ()
:precondition
(and
(ABOVEOF12-ROBOT)
)
:effect
(and
(Flag16-)
(Flag16prime-)
)

)
(:action Flag16Action-1
:parameters ()
:precondition
(and
(ABOVEOF13-ROBOT)
)
:effect
(and
(Flag16-)
(Flag16prime-)
)

)
(:action Prim16Action
:parameters ()
:precondition
(and
(not (ABOVEOF12-ROBOT))
(not (ABOVEOF13-ROBOT))
)
:effect
(and
(Flag16prime-)
(not (Flag16-))
)

)
(:action Flag17Action-0
:parameters ()
:precondition
(and
(BELOWOF2-ROBOT)
)
:effect
(and
(Flag17-)
(Flag17prime-)
)

)
(:action Flag17Action-1
:parameters ()
:precondition
(and
(BELOWOF1-ROBOT)
)
:effect
(and
(Flag17-)
(Flag17prime-)
)

)
(:action Prim17Action
:parameters ()
:precondition
(and
(not (BELOWOF2-ROBOT))
(not (BELOWOF1-ROBOT))
)
:effect
(and
(Flag17prime-)
(not (Flag17-))
)

)
(:action Flag18Action-0
:parameters ()
:precondition
(and
(BELOWOF2-ROBOT)
)
:effect
(and
(Flag18-)
(Flag18prime-)
)

)
(:action Flag18Action-1
:parameters ()
:precondition
(and
(BELOWOF1-ROBOT)
)
:effect
(and
(Flag18-)
(Flag18prime-)
)

)
(:action Flag18Action-2
:parameters ()
:precondition
(and
(BELOWOF3-ROBOT)
)
:effect
(and
(Flag18-)
(Flag18prime-)
)

)
(:action Prim18Action
:parameters ()
:precondition
(and
(not (BELOWOF2-ROBOT))
(not (BELOWOF1-ROBOT))
(not (BELOWOF3-ROBOT))
)
:effect
(and
(Flag18prime-)
(not (Flag18-))
)

)
(:action Flag19Action-0
:parameters ()
:precondition
(and
(BELOWOF2-ROBOT)
)
:effect
(and
(Flag19-)
(Flag19prime-)
)

)
(:action Flag19Action-1
:parameters ()
:precondition
(and
(BELOWOF1-ROBOT)
)
:effect
(and
(Flag19-)
(Flag19prime-)
)

)
(:action Flag19Action-2
:parameters ()
:precondition
(and
(BELOWOF3-ROBOT)
)
:effect
(and
(Flag19-)
(Flag19prime-)
)

)
(:action Flag19Action-3
:parameters ()
:precondition
(and
(BELOWOF4-ROBOT)
)
:effect
(and
(Flag19-)
(Flag19prime-)
)

)
(:action Prim19Action
:parameters ()
:precondition
(and
(not (BELOWOF2-ROBOT))
(not (BELOWOF1-ROBOT))
(not (BELOWOF3-ROBOT))
(not (BELOWOF4-ROBOT))
)
:effect
(and
(Flag19prime-)
(not (Flag19-))
)

)
(:action Flag20Action-0
:parameters ()
:precondition
(and
(BELOWOF2-ROBOT)
)
:effect
(and
(Flag20-)
(Flag20prime-)
)

)
(:action Flag20Action-1
:parameters ()
:precondition
(and
(BELOWOF1-ROBOT)
)
:effect
(and
(Flag20-)
(Flag20prime-)
)

)
(:action Flag20Action-2
:parameters ()
:precondition
(and
(BELOWOF3-ROBOT)
)
:effect
(and
(Flag20-)
(Flag20prime-)
)

)
(:action Flag20Action-3
:parameters ()
:precondition
(and
(BELOWOF4-ROBOT)
)
:effect
(and
(Flag20-)
(Flag20prime-)
)

)
(:action Flag20Action-4
:parameters ()
:precondition
(and
(BELOWOF5-ROBOT)
)
:effect
(and
(Flag20-)
(Flag20prime-)
)

)
(:action Prim20Action
:parameters ()
:precondition
(and
(not (BELOWOF2-ROBOT))
(not (BELOWOF1-ROBOT))
(not (BELOWOF3-ROBOT))
(not (BELOWOF4-ROBOT))
(not (BELOWOF5-ROBOT))
)
:effect
(and
(Flag20prime-)
(not (Flag20-))
)

)
(:action Flag21Action-0
:parameters ()
:precondition
(and
(BELOWOF6-ROBOT)
)
:effect
(and
(Flag21-)
(Flag21prime-)
)

)
(:action Flag21Action-1
:parameters ()
:precondition
(and
(BELOWOF5-ROBOT)
)
:effect
(and
(Flag21-)
(Flag21prime-)
)

)
(:action Flag21Action-2
:parameters ()
:precondition
(and
(BELOWOF2-ROBOT)
)
:effect
(and
(Flag21-)
(Flag21prime-)
)

)
(:action Flag21Action-3
:parameters ()
:precondition
(and
(BELOWOF1-ROBOT)
)
:effect
(and
(Flag21-)
(Flag21prime-)
)

)
(:action Flag21Action-4
:parameters ()
:precondition
(and
(BELOWOF4-ROBOT)
)
:effect
(and
(Flag21-)
(Flag21prime-)
)

)
(:action Flag21Action-5
:parameters ()
:precondition
(and
(BELOWOF3-ROBOT)
)
:effect
(and
(Flag21-)
(Flag21prime-)
)

)
(:action Prim21Action
:parameters ()
:precondition
(and
(not (BELOWOF6-ROBOT))
(not (BELOWOF5-ROBOT))
(not (BELOWOF2-ROBOT))
(not (BELOWOF1-ROBOT))
(not (BELOWOF4-ROBOT))
(not (BELOWOF3-ROBOT))
)
:effect
(and
(Flag21prime-)
(not (Flag21-))
)

)
(:action Flag22Action-0
:parameters ()
:precondition
(and
(BELOWOF6-ROBOT)
)
:effect
(and
(Flag22-)
(Flag22prime-)
)

)
(:action Flag22Action-1
:parameters ()
:precondition
(and
(BELOWOF5-ROBOT)
)
:effect
(and
(Flag22-)
(Flag22prime-)
)

)
(:action Flag22Action-2
:parameters ()
:precondition
(and
(BELOWOF2-ROBOT)
)
:effect
(and
(Flag22-)
(Flag22prime-)
)

)
(:action Flag22Action-3
:parameters ()
:precondition
(and
(BELOWOF1-ROBOT)
)
:effect
(and
(Flag22-)
(Flag22prime-)
)

)
(:action Flag22Action-4
:parameters ()
:precondition
(and
(BELOWOF4-ROBOT)
)
:effect
(and
(Flag22-)
(Flag22prime-)
)

)
(:action Flag22Action-5
:parameters ()
:precondition
(and
(BELOWOF7-ROBOT)
)
:effect
(and
(Flag22-)
(Flag22prime-)
)

)
(:action Flag22Action-6
:parameters ()
:precondition
(and
(BELOWOF3-ROBOT)
)
:effect
(and
(Flag22-)
(Flag22prime-)
)

)
(:action Prim22Action
:parameters ()
:precondition
(and
(not (BELOWOF6-ROBOT))
(not (BELOWOF5-ROBOT))
(not (BELOWOF2-ROBOT))
(not (BELOWOF1-ROBOT))
(not (BELOWOF4-ROBOT))
(not (BELOWOF7-ROBOT))
(not (BELOWOF3-ROBOT))
)
:effect
(and
(Flag22prime-)
(not (Flag22-))
)

)
(:action Flag23Action-0
:parameters ()
:precondition
(and
(BELOWOF6-ROBOT)
)
:effect
(and
(Flag23-)
(Flag23prime-)
)

)
(:action Flag23Action-1
:parameters ()
:precondition
(and
(BELOWOF5-ROBOT)
)
:effect
(and
(Flag23-)
(Flag23prime-)
)

)
(:action Flag23Action-2
:parameters ()
:precondition
(and
(BELOWOF2-ROBOT)
)
:effect
(and
(Flag23-)
(Flag23prime-)
)

)
(:action Flag23Action-3
:parameters ()
:precondition
(and
(BELOWOF1-ROBOT)
)
:effect
(and
(Flag23-)
(Flag23prime-)
)

)
(:action Flag23Action-4
:parameters ()
:precondition
(and
(BELOWOF4-ROBOT)
)
:effect
(and
(Flag23-)
(Flag23prime-)
)

)
(:action Flag23Action-5
:parameters ()
:precondition
(and
(BELOWOF7-ROBOT)
)
:effect
(and
(Flag23-)
(Flag23prime-)
)

)
(:action Flag23Action-6
:parameters ()
:precondition
(and
(BELOWOF3-ROBOT)
)
:effect
(and
(Flag23-)
(Flag23prime-)
)

)
(:action Flag23Action-7
:parameters ()
:precondition
(and
(BELOWOF8-ROBOT)
)
:effect
(and
(Flag23-)
(Flag23prime-)
)

)
(:action Prim23Action
:parameters ()
:precondition
(and
(not (BELOWOF6-ROBOT))
(not (BELOWOF5-ROBOT))
(not (BELOWOF2-ROBOT))
(not (BELOWOF1-ROBOT))
(not (BELOWOF4-ROBOT))
(not (BELOWOF7-ROBOT))
(not (BELOWOF3-ROBOT))
(not (BELOWOF8-ROBOT))
)
:effect
(and
(Flag23prime-)
(not (Flag23-))
)

)
(:action Flag24Action-0
:parameters ()
:precondition
(and
(BELOWOF6-ROBOT)
)
:effect
(and
(Flag24-)
(Flag24prime-)
)

)
(:action Flag24Action-1
:parameters ()
:precondition
(and
(BELOWOF5-ROBOT)
)
:effect
(and
(Flag24-)
(Flag24prime-)
)

)
(:action Flag24Action-2
:parameters ()
:precondition
(and
(BELOWOF2-ROBOT)
)
:effect
(and
(Flag24-)
(Flag24prime-)
)

)
(:action Flag24Action-3
:parameters ()
:precondition
(and
(BELOWOF1-ROBOT)
)
:effect
(and
(Flag24-)
(Flag24prime-)
)

)
(:action Flag24Action-4
:parameters ()
:precondition
(and
(BELOWOF4-ROBOT)
)
:effect
(and
(Flag24-)
(Flag24prime-)
)

)
(:action Flag24Action-5
:parameters ()
:precondition
(and
(BELOWOF7-ROBOT)
)
:effect
(and
(Flag24-)
(Flag24prime-)
)

)
(:action Flag24Action-6
:parameters ()
:precondition
(and
(BELOWOF9-ROBOT)
)
:effect
(and
(Flag24-)
(Flag24prime-)
)

)
(:action Flag24Action-7
:parameters ()
:precondition
(and
(BELOWOF3-ROBOT)
)
:effect
(and
(Flag24-)
(Flag24prime-)
)

)
(:action Flag24Action-8
:parameters ()
:precondition
(and
(BELOWOF8-ROBOT)
)
:effect
(and
(Flag24-)
(Flag24prime-)
)

)
(:action Prim24Action
:parameters ()
:precondition
(and
(not (BELOWOF6-ROBOT))
(not (BELOWOF5-ROBOT))
(not (BELOWOF2-ROBOT))
(not (BELOWOF1-ROBOT))
(not (BELOWOF4-ROBOT))
(not (BELOWOF7-ROBOT))
(not (BELOWOF9-ROBOT))
(not (BELOWOF3-ROBOT))
(not (BELOWOF8-ROBOT))
)
:effect
(and
(Flag24prime-)
(not (Flag24-))
)

)
(:action Flag25Action-0
:parameters ()
:precondition
(and
(BELOWOF10-ROBOT)
)
:effect
(and
(Flag25-)
(Flag25prime-)
)

)
(:action Flag25Action-1
:parameters ()
:precondition
(and
(BELOWOF6-ROBOT)
)
:effect
(and
(Flag25-)
(Flag25prime-)
)

)
(:action Flag25Action-2
:parameters ()
:precondition
(and
(BELOWOF5-ROBOT)
)
:effect
(and
(Flag25-)
(Flag25prime-)
)

)
(:action Flag25Action-3
:parameters ()
:precondition
(and
(BELOWOF2-ROBOT)
)
:effect
(and
(Flag25-)
(Flag25prime-)
)

)
(:action Flag25Action-4
:parameters ()
:precondition
(and
(BELOWOF1-ROBOT)
)
:effect
(and
(Flag25-)
(Flag25prime-)
)

)
(:action Flag25Action-5
:parameters ()
:precondition
(and
(BELOWOF4-ROBOT)
)
:effect
(and
(Flag25-)
(Flag25prime-)
)

)
(:action Flag25Action-6
:parameters ()
:precondition
(and
(BELOWOF7-ROBOT)
)
:effect
(and
(Flag25-)
(Flag25prime-)
)

)
(:action Flag25Action-7
:parameters ()
:precondition
(and
(BELOWOF9-ROBOT)
)
:effect
(and
(Flag25-)
(Flag25prime-)
)

)
(:action Flag25Action-8
:parameters ()
:precondition
(and
(BELOWOF3-ROBOT)
)
:effect
(and
(Flag25-)
(Flag25prime-)
)

)
(:action Flag25Action-9
:parameters ()
:precondition
(and
(BELOWOF8-ROBOT)
)
:effect
(and
(Flag25-)
(Flag25prime-)
)

)
(:action Prim25Action
:parameters ()
:precondition
(and
(not (BELOWOF10-ROBOT))
(not (BELOWOF6-ROBOT))
(not (BELOWOF5-ROBOT))
(not (BELOWOF2-ROBOT))
(not (BELOWOF1-ROBOT))
(not (BELOWOF4-ROBOT))
(not (BELOWOF7-ROBOT))
(not (BELOWOF9-ROBOT))
(not (BELOWOF3-ROBOT))
(not (BELOWOF8-ROBOT))
)
:effect
(and
(Flag25prime-)
(not (Flag25-))
)

)
(:action Flag26Action-0
:parameters ()
:precondition
(and
(BELOWOF10-ROBOT)
)
:effect
(and
(Flag26-)
(Flag26prime-)
)

)
(:action Flag26Action-1
:parameters ()
:precondition
(and
(BELOWOF11-ROBOT)
)
:effect
(and
(Flag26-)
(Flag26prime-)
)

)
(:action Flag26Action-2
:parameters ()
:precondition
(and
(BELOWOF6-ROBOT)
)
:effect
(and
(Flag26-)
(Flag26prime-)
)

)
(:action Flag26Action-3
:parameters ()
:precondition
(and
(BELOWOF5-ROBOT)
)
:effect
(and
(Flag26-)
(Flag26prime-)
)

)
(:action Flag26Action-4
:parameters ()
:precondition
(and
(BELOWOF2-ROBOT)
)
:effect
(and
(Flag26-)
(Flag26prime-)
)

)
(:action Flag26Action-5
:parameters ()
:precondition
(and
(BELOWOF1-ROBOT)
)
:effect
(and
(Flag26-)
(Flag26prime-)
)

)
(:action Flag26Action-6
:parameters ()
:precondition
(and
(BELOWOF4-ROBOT)
)
:effect
(and
(Flag26-)
(Flag26prime-)
)

)
(:action Flag26Action-7
:parameters ()
:precondition
(and
(BELOWOF7-ROBOT)
)
:effect
(and
(Flag26-)
(Flag26prime-)
)

)
(:action Flag26Action-8
:parameters ()
:precondition
(and
(BELOWOF9-ROBOT)
)
:effect
(and
(Flag26-)
(Flag26prime-)
)

)
(:action Flag26Action-9
:parameters ()
:precondition
(and
(BELOWOF3-ROBOT)
)
:effect
(and
(Flag26-)
(Flag26prime-)
)

)
(:action Flag26Action-10
:parameters ()
:precondition
(and
(BELOWOF8-ROBOT)
)
:effect
(and
(Flag26-)
(Flag26prime-)
)

)
(:action Prim26Action
:parameters ()
:precondition
(and
(not (BELOWOF10-ROBOT))
(not (BELOWOF11-ROBOT))
(not (BELOWOF6-ROBOT))
(not (BELOWOF5-ROBOT))
(not (BELOWOF2-ROBOT))
(not (BELOWOF1-ROBOT))
(not (BELOWOF4-ROBOT))
(not (BELOWOF7-ROBOT))
(not (BELOWOF9-ROBOT))
(not (BELOWOF3-ROBOT))
(not (BELOWOF8-ROBOT))
)
:effect
(and
(Flag26prime-)
(not (Flag26-))
)

)
(:action Flag27Action-0
:parameters ()
:precondition
(and
(BELOWOF10-ROBOT)
)
:effect
(and
(Flag27-)
(Flag27prime-)
)

)
(:action Flag27Action-1
:parameters ()
:precondition
(and
(BELOWOF11-ROBOT)
)
:effect
(and
(Flag27-)
(Flag27prime-)
)

)
(:action Flag27Action-2
:parameters ()
:precondition
(and
(BELOWOF6-ROBOT)
)
:effect
(and
(Flag27-)
(Flag27prime-)
)

)
(:action Flag27Action-3
:parameters ()
:precondition
(and
(BELOWOF5-ROBOT)
)
:effect
(and
(Flag27-)
(Flag27prime-)
)

)
(:action Flag27Action-4
:parameters ()
:precondition
(and
(BELOWOF2-ROBOT)
)
:effect
(and
(Flag27-)
(Flag27prime-)
)

)
(:action Flag27Action-5
:parameters ()
:precondition
(and
(BELOWOF1-ROBOT)
)
:effect
(and
(Flag27-)
(Flag27prime-)
)

)
(:action Flag27Action-6
:parameters ()
:precondition
(and
(BELOWOF4-ROBOT)
)
:effect
(and
(Flag27-)
(Flag27prime-)
)

)
(:action Flag27Action-7
:parameters ()
:precondition
(and
(BELOWOF12-ROBOT)
)
:effect
(and
(Flag27-)
(Flag27prime-)
)

)
(:action Flag27Action-8
:parameters ()
:precondition
(and
(BELOWOF7-ROBOT)
)
:effect
(and
(Flag27-)
(Flag27prime-)
)

)
(:action Flag27Action-9
:parameters ()
:precondition
(and
(BELOWOF9-ROBOT)
)
:effect
(and
(Flag27-)
(Flag27prime-)
)

)
(:action Flag27Action-10
:parameters ()
:precondition
(and
(BELOWOF3-ROBOT)
)
:effect
(and
(Flag27-)
(Flag27prime-)
)

)
(:action Flag27Action-11
:parameters ()
:precondition
(and
(BELOWOF8-ROBOT)
)
:effect
(and
(Flag27-)
(Flag27prime-)
)

)
(:action Prim27Action
:parameters ()
:precondition
(and
(not (BELOWOF10-ROBOT))
(not (BELOWOF11-ROBOT))
(not (BELOWOF6-ROBOT))
(not (BELOWOF5-ROBOT))
(not (BELOWOF2-ROBOT))
(not (BELOWOF1-ROBOT))
(not (BELOWOF4-ROBOT))
(not (BELOWOF12-ROBOT))
(not (BELOWOF7-ROBOT))
(not (BELOWOF9-ROBOT))
(not (BELOWOF3-ROBOT))
(not (BELOWOF8-ROBOT))
)
:effect
(and
(Flag27prime-)
(not (Flag27-))
)

)
(:action Flag28Action-0
:parameters ()
:precondition
(and
(BELOWOF10-ROBOT)
)
:effect
(and
(Flag28-)
(Flag28prime-)
)

)
(:action Flag28Action-1
:parameters ()
:precondition
(and
(BELOWOF11-ROBOT)
)
:effect
(and
(Flag28-)
(Flag28prime-)
)

)
(:action Flag28Action-2
:parameters ()
:precondition
(and
(BELOWOF6-ROBOT)
)
:effect
(and
(Flag28-)
(Flag28prime-)
)

)
(:action Flag28Action-3
:parameters ()
:precondition
(and
(BELOWOF5-ROBOT)
)
:effect
(and
(Flag28-)
(Flag28prime-)
)

)
(:action Flag28Action-4
:parameters ()
:precondition
(and
(BELOWOF2-ROBOT)
)
:effect
(and
(Flag28-)
(Flag28prime-)
)

)
(:action Flag28Action-5
:parameters ()
:precondition
(and
(BELOWOF1-ROBOT)
)
:effect
(and
(Flag28-)
(Flag28prime-)
)

)
(:action Flag28Action-6
:parameters ()
:precondition
(and
(BELOWOF4-ROBOT)
)
:effect
(and
(Flag28-)
(Flag28prime-)
)

)
(:action Flag28Action-7
:parameters ()
:precondition
(and
(BELOWOF12-ROBOT)
)
:effect
(and
(Flag28-)
(Flag28prime-)
)

)
(:action Flag28Action-8
:parameters ()
:precondition
(and
(BELOWOF7-ROBOT)
)
:effect
(and
(Flag28-)
(Flag28prime-)
)

)
(:action Flag28Action-9
:parameters ()
:precondition
(and
(BELOWOF9-ROBOT)
)
:effect
(and
(Flag28-)
(Flag28prime-)
)

)
(:action Flag28Action-10
:parameters ()
:precondition
(and
(BELOWOF3-ROBOT)
)
:effect
(and
(Flag28-)
(Flag28prime-)
)

)
(:action Flag28Action-12
:parameters ()
:precondition
(and
(BELOWOF8-ROBOT)
)
:effect
(and
(Flag28-)
(Flag28prime-)
)

)
(:action Prim28Action
:parameters ()
:precondition
(and
(not (BELOWOF10-ROBOT))
(not (BELOWOF11-ROBOT))
(not (BELOWOF6-ROBOT))
(not (BELOWOF5-ROBOT))
(not (BELOWOF2-ROBOT))
(not (BELOWOF1-ROBOT))
(not (BELOWOF4-ROBOT))
(not (BELOWOF12-ROBOT))
(not (BELOWOF7-ROBOT))
(not (BELOWOF9-ROBOT))
(not (BELOWOF3-ROBOT))
(not (BELOWOF13-ROBOT))
(not (BELOWOF8-ROBOT))
)
:effect
(and
(Flag28prime-)
(not (Flag28-))
)

)
(:action Flag29Action
:parameters ()
:precondition
(and
(BELOWOF1-ROBOT)
(ABOVEOF4-ROBOT)
)
:effect
(and
(Flag29-)
(Flag29prime-)
)

)
(:action Flag30Action
:parameters ()
:precondition
(and
(BELOWOF1-ROBOT)
(ABOVEOF2-ROBOT)
)
:effect
(and
(Flag30-)
(Flag30prime-)
)

)
(:action Flag31Action
:parameters ()
:precondition
(and
(BELOWOF1-ROBOT)
(ABOVEOF1-ROBOT)
)
:effect
(and
(Flag31-)
(Flag31prime-)
)

)
(:action Flag32Action
:parameters ()
:precondition
(and
(ABOVEOF12-ROBOT)
(BELOWOF1-ROBOT)
)
:effect
(and
(Flag32-)
(Flag32prime-)
)

)
(:action Flag33Action
:parameters ()
:precondition
(and
(BELOWOF1-ROBOT)
(ABOVEOF13-ROBOT)
)
:effect
(and
(Flag33-)
(Flag33prime-)
)

)
(:action Flag34Action
:parameters ()
:precondition
(and
(BELOWOF1-ROBOT)
(ABOVEOF7-ROBOT)
)
:effect
(and
(Flag34-)
(Flag34prime-)
)

)
(:action Flag35Action
:parameters ()
:precondition
(and
(ABOVEOF11-ROBOT)
(BELOWOF1-ROBOT)
)
:effect
(and
(Flag35-)
(Flag35prime-)
)

)
(:action Flag36Action
:parameters ()
:precondition
(and
(ABOVEOF6-ROBOT)
(BELOWOF1-ROBOT)
)
:effect
(and
(Flag36-)
(Flag36prime-)
)

)
(:action Flag37Action
:parameters ()
:precondition
(and
(BELOWOF1-ROBOT)
(ABOVEOF5-ROBOT)
)
:effect
(and
(Flag37-)
(Flag37prime-)
)

)
(:action Flag38Action
:parameters ()
:precondition
(and
(ABOVEOF10-ROBOT)
(BELOWOF1-ROBOT)
)
:effect
(and
(Flag38-)
(Flag38prime-)
)

)
(:action Flag39Action
:parameters ()
:precondition
(and
(ABOVEOF8-ROBOT)
(BELOWOF1-ROBOT)
)
:effect
(and
(Flag39-)
(Flag39prime-)
)

)
(:action Flag40Action
:parameters ()
:precondition
(and
(ABOVEOF3-ROBOT)
(BELOWOF1-ROBOT)
)
:effect
(and
(Flag40-)
(Flag40prime-)
)

)
(:action Flag41Action
:parameters ()
:precondition
(and
(BELOWOF1-ROBOT)
(ABOVEOF9-ROBOT)
)
:effect
(and
(Flag41-)
(Flag41prime-)
)

)
(:action Flag42Action-10
:parameters ()
:precondition
(and
(BELOWOF1-ROBOT)
)
:effect
(and
(Flag42-)
(Flag42prime-)
)

)
(:action Prim29Action-0
:parameters ()
:precondition
(and
(not (BELOWOF1-ROBOT))
)
:effect
(and
(Flag29prime-)
(not (Flag29-))
)

)
(:action Prim29Action-1
:parameters ()
:precondition
(and
(not (ABOVEOF4-ROBOT))
)
:effect
(and
(Flag29prime-)
(not (Flag29-))
)

)
(:action Prim30Action-0
:parameters ()
:precondition
(and
(not (BELOWOF1-ROBOT))
)
:effect
(and
(Flag30prime-)
(not (Flag30-))
)

)
(:action Prim30Action-1
:parameters ()
:precondition
(and
(not (ABOVEOF2-ROBOT))
)
:effect
(and
(Flag30prime-)
(not (Flag30-))
)

)
(:action Prim31Action-0
:parameters ()
:precondition
(and
(not (BELOWOF1-ROBOT))
)
:effect
(and
(Flag31prime-)
(not (Flag31-))
)

)
(:action Prim31Action-1
:parameters ()
:precondition
(and
(not (ABOVEOF1-ROBOT))
)
:effect
(and
(Flag31prime-)
(not (Flag31-))
)

)
(:action Prim32Action-0
:parameters ()
:precondition
(and
(not (ABOVEOF12-ROBOT))
)
:effect
(and
(Flag32prime-)
(not (Flag32-))
)

)
(:action Prim32Action-1
:parameters ()
:precondition
(and
(not (BELOWOF1-ROBOT))
)
:effect
(and
(Flag32prime-)
(not (Flag32-))
)

)
(:action Prim33Action-0
:parameters ()
:precondition
(and
(not (BELOWOF1-ROBOT))
)
:effect
(and
(Flag33prime-)
(not (Flag33-))
)

)
(:action Prim33Action-1
:parameters ()
:precondition
(and
(not (ABOVEOF13-ROBOT))
)
:effect
(and
(Flag33prime-)
(not (Flag33-))
)

)
(:action Prim34Action-0
:parameters ()
:precondition
(and
(not (BELOWOF1-ROBOT))
)
:effect
(and
(Flag34prime-)
(not (Flag34-))
)

)
(:action Prim34Action-1
:parameters ()
:precondition
(and
(not (ABOVEOF7-ROBOT))
)
:effect
(and
(Flag34prime-)
(not (Flag34-))
)

)
(:action Prim35Action-0
:parameters ()
:precondition
(and
(not (ABOVEOF11-ROBOT))
)
:effect
(and
(Flag35prime-)
(not (Flag35-))
)

)
(:action Prim35Action-1
:parameters ()
:precondition
(and
(not (BELOWOF1-ROBOT))
)
:effect
(and
(Flag35prime-)
(not (Flag35-))
)

)
(:action Prim36Action-0
:parameters ()
:precondition
(and
(not (ABOVEOF6-ROBOT))
)
:effect
(and
(Flag36prime-)
(not (Flag36-))
)

)
(:action Prim36Action-1
:parameters ()
:precondition
(and
(not (BELOWOF1-ROBOT))
)
:effect
(and
(Flag36prime-)
(not (Flag36-))
)

)
(:action Prim37Action-0
:parameters ()
:precondition
(and
(not (BELOWOF1-ROBOT))
)
:effect
(and
(Flag37prime-)
(not (Flag37-))
)

)
(:action Prim37Action-1
:parameters ()
:precondition
(and
(not (ABOVEOF5-ROBOT))
)
:effect
(and
(Flag37prime-)
(not (Flag37-))
)

)
(:action Prim38Action-0
:parameters ()
:precondition
(and
(not (ABOVEOF10-ROBOT))
)
:effect
(and
(Flag38prime-)
(not (Flag38-))
)

)
(:action Prim38Action-1
:parameters ()
:precondition
(and
(not (BELOWOF1-ROBOT))
)
:effect
(and
(Flag38prime-)
(not (Flag38-))
)

)
(:action Prim39Action-0
:parameters ()
:precondition
(and
(not (ABOVEOF8-ROBOT))
)
:effect
(and
(Flag39prime-)
(not (Flag39-))
)

)
(:action Prim39Action-1
:parameters ()
:precondition
(and
(not (BELOWOF1-ROBOT))
)
:effect
(and
(Flag39prime-)
(not (Flag39-))
)

)
(:action Prim40Action-0
:parameters ()
:precondition
(and
(not (ABOVEOF3-ROBOT))
)
:effect
(and
(Flag40prime-)
(not (Flag40-))
)

)
(:action Prim40Action-1
:parameters ()
:precondition
(and
(not (BELOWOF1-ROBOT))
)
:effect
(and
(Flag40prime-)
(not (Flag40-))
)

)
(:action Prim41Action-0
:parameters ()
:precondition
(and
(not (BELOWOF1-ROBOT))
)
:effect
(and
(Flag41prime-)
(not (Flag41-))
)

)
(:action Prim41Action-1
:parameters ()
:precondition
(and
(not (ABOVEOF9-ROBOT))
)
:effect
(and
(Flag41prime-)
(not (Flag41-))
)

)
(:action Flag43Action
:parameters ()
:precondition
(and
(ABOVEOF2-ROBOT)
(BELOWOF2-ROBOT)
)
:effect
(and
(Flag43-)
(Flag43prime-)
)

)
(:action Flag44Action
:parameters ()
:precondition
(and
(ABOVEOF1-ROBOT)
(BELOWOF2-ROBOT)
)
:effect
(and
(Flag44-)
(Flag44prime-)
)

)
(:action Flag45Action
:parameters ()
:precondition
(and
(ABOVEOF7-ROBOT)
(BELOWOF2-ROBOT)
)
:effect
(and
(Flag45-)
(Flag45prime-)
)

)
(:action Flag46Action
:parameters ()
:precondition
(and
(BELOWOF2-ROBOT)
(ABOVEOF13-ROBOT)
)
:effect
(and
(Flag46-)
(Flag46prime-)
)

)
(:action Flag47Action
:parameters ()
:precondition
(and
(ABOVEOF4-ROBOT)
(BELOWOF2-ROBOT)
)
:effect
(and
(Flag47-)
(Flag47prime-)
)

)
(:action Flag48Action
:parameters ()
:precondition
(and
(ABOVEOF6-ROBOT)
(BELOWOF2-ROBOT)
)
:effect
(and
(Flag48-)
(Flag48prime-)
)

)
(:action Flag49Action
:parameters ()
:precondition
(and
(ABOVEOF9-ROBOT)
(BELOWOF2-ROBOT)
)
:effect
(and
(Flag49-)
(Flag49prime-)
)

)
(:action Flag50Action
:parameters ()
:precondition
(and
(ABOVEOF11-ROBOT)
(BELOWOF2-ROBOT)
)
:effect
(and
(Flag50-)
(Flag50prime-)
)

)
(:action Flag51Action
:parameters ()
:precondition
(and
(ABOVEOF10-ROBOT)
(BELOWOF2-ROBOT)
)
:effect
(and
(Flag51-)
(Flag51prime-)
)

)
(:action Flag52Action
:parameters ()
:precondition
(and
(ABOVEOF8-ROBOT)
(BELOWOF2-ROBOT)
)
:effect
(and
(Flag52-)
(Flag52prime-)
)

)
(:action Flag53Action
:parameters ()
:precondition
(and
(ABOVEOF12-ROBOT)
(BELOWOF2-ROBOT)
)
:effect
(and
(Flag53-)
(Flag53prime-)
)

)
(:action Flag54Action
:parameters ()
:precondition
(and
(BELOWOF2-ROBOT)
(ABOVEOF5-ROBOT)
)
:effect
(and
(Flag54-)
(Flag54prime-)
)

)
(:action Flag55Action
:parameters ()
:precondition
(and
(ABOVEOF3-ROBOT)
(BELOWOF2-ROBOT)
)
:effect
(and
(Flag55-)
(Flag55prime-)
)

)
(:action Prim43Action-0
:parameters ()
:precondition
(and
(not (ABOVEOF2-ROBOT))
)
:effect
(and
(Flag43prime-)
(not (Flag43-))
)

)
(:action Prim43Action-1
:parameters ()
:precondition
(and
(not (BELOWOF2-ROBOT))
)
:effect
(and
(Flag43prime-)
(not (Flag43-))
)

)
(:action Prim44Action-0
:parameters ()
:precondition
(and
(not (ABOVEOF1-ROBOT))
)
:effect
(and
(Flag44prime-)
(not (Flag44-))
)

)
(:action Prim44Action-1
:parameters ()
:precondition
(and
(not (BELOWOF2-ROBOT))
)
:effect
(and
(Flag44prime-)
(not (Flag44-))
)

)
(:action Prim45Action-0
:parameters ()
:precondition
(and
(not (ABOVEOF7-ROBOT))
)
:effect
(and
(Flag45prime-)
(not (Flag45-))
)

)
(:action Prim45Action-1
:parameters ()
:precondition
(and
(not (BELOWOF2-ROBOT))
)
:effect
(and
(Flag45prime-)
(not (Flag45-))
)

)
(:action Prim46Action-0
:parameters ()
:precondition
(and
(not (BELOWOF2-ROBOT))
)
:effect
(and
(Flag46prime-)
(not (Flag46-))
)

)
(:action Prim46Action-1
:parameters ()
:precondition
(and
(not (ABOVEOF13-ROBOT))
)
:effect
(and
(Flag46prime-)
(not (Flag46-))
)

)
(:action Prim47Action-0
:parameters ()
:precondition
(and
(not (ABOVEOF4-ROBOT))
)
:effect
(and
(Flag47prime-)
(not (Flag47-))
)

)
(:action Prim47Action-1
:parameters ()
:precondition
(and
(not (BELOWOF2-ROBOT))
)
:effect
(and
(Flag47prime-)
(not (Flag47-))
)

)
(:action Prim48Action-0
:parameters ()
:precondition
(and
(not (ABOVEOF6-ROBOT))
)
:effect
(and
(Flag48prime-)
(not (Flag48-))
)

)
(:action Prim48Action-1
:parameters ()
:precondition
(and
(not (BELOWOF2-ROBOT))
)
:effect
(and
(Flag48prime-)
(not (Flag48-))
)

)
(:action Prim49Action-0
:parameters ()
:precondition
(and
(not (ABOVEOF9-ROBOT))
)
:effect
(and
(Flag49prime-)
(not (Flag49-))
)

)
(:action Prim49Action-1
:parameters ()
:precondition
(and
(not (BELOWOF2-ROBOT))
)
:effect
(and
(Flag49prime-)
(not (Flag49-))
)

)
(:action Prim50Action-0
:parameters ()
:precondition
(and
(not (ABOVEOF11-ROBOT))
)
:effect
(and
(Flag50prime-)
(not (Flag50-))
)

)
(:action Prim50Action-1
:parameters ()
:precondition
(and
(not (BELOWOF2-ROBOT))
)
:effect
(and
(Flag50prime-)
(not (Flag50-))
)

)
(:action Prim51Action-0
:parameters ()
:precondition
(and
(not (ABOVEOF10-ROBOT))
)
:effect
(and
(Flag51prime-)
(not (Flag51-))
)

)
(:action Prim51Action-1
:parameters ()
:precondition
(and
(not (BELOWOF2-ROBOT))
)
:effect
(and
(Flag51prime-)
(not (Flag51-))
)

)
(:action Prim52Action-0
:parameters ()
:precondition
(and
(not (ABOVEOF8-ROBOT))
)
:effect
(and
(Flag52prime-)
(not (Flag52-))
)

)
(:action Prim52Action-1
:parameters ()
:precondition
(and
(not (BELOWOF2-ROBOT))
)
:effect
(and
(Flag52prime-)
(not (Flag52-))
)

)
(:action Prim53Action-0
:parameters ()
:precondition
(and
(not (ABOVEOF12-ROBOT))
)
:effect
(and
(Flag53prime-)
(not (Flag53-))
)

)
(:action Prim53Action-1
:parameters ()
:precondition
(and
(not (BELOWOF2-ROBOT))
)
:effect
(and
(Flag53prime-)
(not (Flag53-))
)

)
(:action Prim54Action-0
:parameters ()
:precondition
(and
(not (BELOWOF2-ROBOT))
)
:effect
(and
(Flag54prime-)
(not (Flag54-))
)

)
(:action Prim54Action-1
:parameters ()
:precondition
(and
(not (ABOVEOF5-ROBOT))
)
:effect
(and
(Flag54prime-)
(not (Flag54-))
)

)
(:action Prim55Action-0
:parameters ()
:precondition
(and
(not (ABOVEOF3-ROBOT))
)
:effect
(and
(Flag55prime-)
(not (Flag55-))
)

)
(:action Prim55Action-1
:parameters ()
:precondition
(and
(not (BELOWOF2-ROBOT))
)
:effect
(and
(Flag55prime-)
(not (Flag55-))
)

)
(:action Flag57Action
:parameters ()
:precondition
(and
(BELOWOF3-ROBOT)
(ABOVEOF5-ROBOT)
)
:effect
(and
(Flag57-)
(Flag57prime-)
)

)
(:action Flag58Action
:parameters ()
:precondition
(and
(BELOWOF3-ROBOT)
(ABOVEOF6-ROBOT)
)
:effect
(and
(Flag58-)
(Flag58prime-)
)

)
(:action Flag59Action
:parameters ()
:precondition
(and
(BELOWOF3-ROBOT)
(ABOVEOF7-ROBOT)
)
:effect
(and
(Flag59-)
(Flag59prime-)
)

)
(:action Flag60Action
:parameters ()
:precondition
(and
(BELOWOF3-ROBOT)
(ABOVEOF9-ROBOT)
)
:effect
(and
(Flag60-)
(Flag60prime-)
)

)
(:action Flag61Action
:parameters ()
:precondition
(and
(BELOWOF3-ROBOT)
(ABOVEOF2-ROBOT)
)
:effect
(and
(Flag61-)
(Flag61prime-)
)

)
(:action Flag62Action
:parameters ()
:precondition
(and
(ABOVEOF3-ROBOT)
(BELOWOF3-ROBOT)
)
:effect
(and
(Flag62-)
(Flag62prime-)
)

)
(:action Flag63Action
:parameters ()
:precondition
(and
(ABOVEOF8-ROBOT)
(BELOWOF3-ROBOT)
)
:effect
(and
(Flag63-)
(Flag63prime-)
)

)
(:action Flag64Action
:parameters ()
:precondition
(and
(BELOWOF3-ROBOT)
(ABOVEOF4-ROBOT)
)
:effect
(and
(Flag64-)
(Flag64prime-)
)

)
(:action Flag65Action
:parameters ()
:precondition
(and
(BELOWOF3-ROBOT)
(ABOVEOF10-ROBOT)
)
:effect
(and
(Flag65-)
(Flag65prime-)
)

)
(:action Flag66Action
:parameters ()
:precondition
(and
(BELOWOF2-ROBOT)
(ABOVEOF2-ROBOT)
)
:effect
(and
(Flag66-)
(Flag66prime-)
)

)
(:action Flag67Action
:parameters ()
:precondition
(and
(ABOVEOF12-ROBOT)
(BELOWOF3-ROBOT)
)
:effect
(and
(Flag67-)
(Flag67prime-)
)

)
(:action Flag68Action
:parameters ()
:precondition
(and
(BELOWOF3-ROBOT)
(ABOVEOF13-ROBOT)
)
:effect
(and
(Flag68-)
(Flag68prime-)
)

)
(:action Flag69Action
:parameters ()
:precondition
(and
(ABOVEOF11-ROBOT)
(BELOWOF3-ROBOT)
)
:effect
(and
(Flag69-)
(Flag69prime-)
)

)
(:action Prim57Action-0
:parameters ()
:precondition
(and
(not (BELOWOF3-ROBOT))
)
:effect
(and
(Flag57prime-)
(not (Flag57-))
)

)
(:action Prim57Action-1
:parameters ()
:precondition
(and
(not (ABOVEOF5-ROBOT))
)
:effect
(and
(Flag57prime-)
(not (Flag57-))
)

)
(:action Prim58Action-0
:parameters ()
:precondition
(and
(not (BELOWOF3-ROBOT))
)
:effect
(and
(Flag58prime-)
(not (Flag58-))
)

)
(:action Prim58Action-1
:parameters ()
:precondition
(and
(not (ABOVEOF6-ROBOT))
)
:effect
(and
(Flag58prime-)
(not (Flag58-))
)

)
(:action Prim59Action-0
:parameters ()
:precondition
(and
(not (BELOWOF3-ROBOT))
)
:effect
(and
(Flag59prime-)
(not (Flag59-))
)

)
(:action Prim59Action-1
:parameters ()
:precondition
(and
(not (ABOVEOF7-ROBOT))
)
:effect
(and
(Flag59prime-)
(not (Flag59-))
)

)
(:action Prim60Action-0
:parameters ()
:precondition
(and
(not (BELOWOF3-ROBOT))
)
:effect
(and
(Flag60prime-)
(not (Flag60-))
)

)
(:action Prim60Action-1
:parameters ()
:precondition
(and
(not (ABOVEOF9-ROBOT))
)
:effect
(and
(Flag60prime-)
(not (Flag60-))
)

)
(:action Prim61Action-0
:parameters ()
:precondition
(and
(not (BELOWOF3-ROBOT))
)
:effect
(and
(Flag61prime-)
(not (Flag61-))
)

)
(:action Prim61Action-1
:parameters ()
:precondition
(and
(not (ABOVEOF2-ROBOT))
)
:effect
(and
(Flag61prime-)
(not (Flag61-))
)

)
(:action Prim62Action-0
:parameters ()
:precondition
(and
(not (ABOVEOF3-ROBOT))
)
:effect
(and
(Flag62prime-)
(not (Flag62-))
)

)
(:action Prim62Action-1
:parameters ()
:precondition
(and
(not (BELOWOF3-ROBOT))
)
:effect
(and
(Flag62prime-)
(not (Flag62-))
)

)
(:action Prim63Action-0
:parameters ()
:precondition
(and
(not (ABOVEOF8-ROBOT))
)
:effect
(and
(Flag63prime-)
(not (Flag63-))
)

)
(:action Prim63Action-1
:parameters ()
:precondition
(and
(not (BELOWOF3-ROBOT))
)
:effect
(and
(Flag63prime-)
(not (Flag63-))
)

)
(:action Prim64Action-0
:parameters ()
:precondition
(and
(not (BELOWOF3-ROBOT))
)
:effect
(and
(Flag64prime-)
(not (Flag64-))
)

)
(:action Prim64Action-1
:parameters ()
:precondition
(and
(not (ABOVEOF4-ROBOT))
)
:effect
(and
(Flag64prime-)
(not (Flag64-))
)

)
(:action Prim65Action-0
:parameters ()
:precondition
(and
(not (BELOWOF3-ROBOT))
)
:effect
(and
(Flag65prime-)
(not (Flag65-))
)

)
(:action Prim65Action-1
:parameters ()
:precondition
(and
(not (ABOVEOF10-ROBOT))
)
:effect
(and
(Flag65prime-)
(not (Flag65-))
)

)
(:action Prim66Action-0
:parameters ()
:precondition
(and
(not (BELOWOF2-ROBOT))
)
:effect
(and
(Flag66prime-)
(not (Flag66-))
)

)
(:action Prim66Action-1
:parameters ()
:precondition
(and
(not (ABOVEOF2-ROBOT))
)
:effect
(and
(Flag66prime-)
(not (Flag66-))
)

)
(:action Prim67Action-0
:parameters ()
:precondition
(and
(not (ABOVEOF12-ROBOT))
)
:effect
(and
(Flag67prime-)
(not (Flag67-))
)

)
(:action Prim67Action-1
:parameters ()
:precondition
(and
(not (BELOWOF3-ROBOT))
)
:effect
(and
(Flag67prime-)
(not (Flag67-))
)

)
(:action Prim68Action-0
:parameters ()
:precondition
(and
(not (BELOWOF3-ROBOT))
)
:effect
(and
(Flag68prime-)
(not (Flag68-))
)

)
(:action Prim68Action-1
:parameters ()
:precondition
(and
(not (ABOVEOF13-ROBOT))
)
:effect
(and
(Flag68prime-)
(not (Flag68-))
)

)
(:action Prim69Action-0
:parameters ()
:precondition
(and
(not (ABOVEOF11-ROBOT))
)
:effect
(and
(Flag69prime-)
(not (Flag69-))
)

)
(:action Prim69Action-1
:parameters ()
:precondition
(and
(not (BELOWOF3-ROBOT))
)
:effect
(and
(Flag69prime-)
(not (Flag69-))
)

)
(:action Flag71Action
:parameters ()
:precondition
(and
(BELOWOF4-ROBOT)
(ABOVEOF13-ROBOT)
)
:effect
(and
(Flag71-)
(Flag71prime-)
)

)
(:action Flag72Action
:parameters ()
:precondition
(and
(ABOVEOF3-ROBOT)
(BELOWOF4-ROBOT)
)
:effect
(and
(Flag72-)
(Flag72prime-)
)

)
(:action Flag73Action
:parameters ()
:precondition
(and
(ABOVEOF6-ROBOT)
(BELOWOF4-ROBOT)
)
:effect
(and
(Flag73-)
(Flag73prime-)
)

)
(:action Flag74Action
:parameters ()
:precondition
(and
(ABOVEOF9-ROBOT)
(BELOWOF4-ROBOT)
)
:effect
(and
(Flag74-)
(Flag74prime-)
)

)
(:action Flag75Action
:parameters ()
:precondition
(and
(BELOWOF4-ROBOT)
(ABOVEOF4-ROBOT)
)
:effect
(and
(Flag75-)
(Flag75prime-)
)

)
(:action Flag76Action
:parameters ()
:precondition
(and
(ABOVEOF8-ROBOT)
(BELOWOF4-ROBOT)
)
:effect
(and
(Flag76-)
(Flag76prime-)
)

)
(:action Flag77Action
:parameters ()
:precondition
(and
(BELOWOF4-ROBOT)
(ABOVEOF7-ROBOT)
)
:effect
(and
(Flag77-)
(Flag77prime-)
)

)
(:action Flag78Action
:parameters ()
:precondition
(and
(ABOVEOF12-ROBOT)
(BELOWOF4-ROBOT)
)
:effect
(and
(Flag78-)
(Flag78prime-)
)

)
(:action Flag79Action
:parameters ()
:precondition
(and
(BELOWOF4-ROBOT)
(ABOVEOF5-ROBOT)
)
:effect
(and
(Flag79-)
(Flag79prime-)
)

)
(:action Flag80Action
:parameters ()
:precondition
(and
(BELOWOF3-ROBOT)
(ABOVEOF3-ROBOT)
)
:effect
(and
(Flag80-)
(Flag80prime-)
)

)
(:action Flag81Action
:parameters ()
:precondition
(and
(ABOVEOF10-ROBOT)
(BELOWOF4-ROBOT)
)
:effect
(and
(Flag81-)
(Flag81prime-)
)

)
(:action Flag82Action
:parameters ()
:precondition
(and
(BELOWOF3-ROBOT)
(ABOVEOF11-ROBOT)
)
:effect
(and
(Flag82-)
(Flag82prime-)
)

)
(:action Flag83Action
:parameters ()
:precondition
(and
(ABOVEOF11-ROBOT)
(BELOWOF4-ROBOT)
)
:effect
(and
(Flag83-)
(Flag83prime-)
)

)
(:action Prim71Action-0
:parameters ()
:precondition
(and
(not (BELOWOF4-ROBOT))
)
:effect
(and
(Flag71prime-)
(not (Flag71-))
)

)
(:action Prim71Action-1
:parameters ()
:precondition
(and
(not (ABOVEOF13-ROBOT))
)
:effect
(and
(Flag71prime-)
(not (Flag71-))
)

)
(:action Prim72Action-0
:parameters ()
:precondition
(and
(not (ABOVEOF3-ROBOT))
)
:effect
(and
(Flag72prime-)
(not (Flag72-))
)

)
(:action Prim72Action-1
:parameters ()
:precondition
(and
(not (BELOWOF4-ROBOT))
)
:effect
(and
(Flag72prime-)
(not (Flag72-))
)

)
(:action Prim73Action-0
:parameters ()
:precondition
(and
(not (ABOVEOF6-ROBOT))
)
:effect
(and
(Flag73prime-)
(not (Flag73-))
)

)
(:action Prim73Action-1
:parameters ()
:precondition
(and
(not (BELOWOF4-ROBOT))
)
:effect
(and
(Flag73prime-)
(not (Flag73-))
)

)
(:action Prim74Action-0
:parameters ()
:precondition
(and
(not (ABOVEOF9-ROBOT))
)
:effect
(and
(Flag74prime-)
(not (Flag74-))
)

)
(:action Prim74Action-1
:parameters ()
:precondition
(and
(not (BELOWOF4-ROBOT))
)
:effect
(and
(Flag74prime-)
(not (Flag74-))
)

)
(:action Prim75Action-0
:parameters ()
:precondition
(and
(not (BELOWOF4-ROBOT))
)
:effect
(and
(Flag75prime-)
(not (Flag75-))
)

)
(:action Prim75Action-1
:parameters ()
:precondition
(and
(not (ABOVEOF4-ROBOT))
)
:effect
(and
(Flag75prime-)
(not (Flag75-))
)

)
(:action Prim76Action-0
:parameters ()
:precondition
(and
(not (ABOVEOF8-ROBOT))
)
:effect
(and
(Flag76prime-)
(not (Flag76-))
)

)
(:action Prim76Action-1
:parameters ()
:precondition
(and
(not (BELOWOF4-ROBOT))
)
:effect
(and
(Flag76prime-)
(not (Flag76-))
)

)
(:action Prim77Action-0
:parameters ()
:precondition
(and
(not (BELOWOF4-ROBOT))
)
:effect
(and
(Flag77prime-)
(not (Flag77-))
)

)
(:action Prim77Action-1
:parameters ()
:precondition
(and
(not (ABOVEOF7-ROBOT))
)
:effect
(and
(Flag77prime-)
(not (Flag77-))
)

)
(:action Prim78Action-0
:parameters ()
:precondition
(and
(not (ABOVEOF12-ROBOT))
)
:effect
(and
(Flag78prime-)
(not (Flag78-))
)

)
(:action Prim78Action-1
:parameters ()
:precondition
(and
(not (BELOWOF4-ROBOT))
)
:effect
(and
(Flag78prime-)
(not (Flag78-))
)

)
(:action Prim79Action-0
:parameters ()
:precondition
(and
(not (BELOWOF4-ROBOT))
)
:effect
(and
(Flag79prime-)
(not (Flag79-))
)

)
(:action Prim79Action-1
:parameters ()
:precondition
(and
(not (ABOVEOF5-ROBOT))
)
:effect
(and
(Flag79prime-)
(not (Flag79-))
)

)
(:action Prim80Action-0
:parameters ()
:precondition
(and
(not (BELOWOF3-ROBOT))
)
:effect
(and
(Flag80prime-)
(not (Flag80-))
)

)
(:action Prim80Action-1
:parameters ()
:precondition
(and
(not (ABOVEOF3-ROBOT))
)
:effect
(and
(Flag80prime-)
(not (Flag80-))
)

)
(:action Prim81Action-0
:parameters ()
:precondition
(and
(not (ABOVEOF10-ROBOT))
)
:effect
(and
(Flag81prime-)
(not (Flag81-))
)

)
(:action Prim81Action-1
:parameters ()
:precondition
(and
(not (BELOWOF4-ROBOT))
)
:effect
(and
(Flag81prime-)
(not (Flag81-))
)

)
(:action Prim82Action-0
:parameters ()
:precondition
(and
(not (BELOWOF3-ROBOT))
)
:effect
(and
(Flag82prime-)
(not (Flag82-))
)

)
(:action Prim82Action-1
:parameters ()
:precondition
(and
(not (ABOVEOF11-ROBOT))
)
:effect
(and
(Flag82prime-)
(not (Flag82-))
)

)
(:action Prim83Action-0
:parameters ()
:precondition
(and
(not (ABOVEOF11-ROBOT))
)
:effect
(and
(Flag83prime-)
(not (Flag83-))
)

)
(:action Prim83Action-1
:parameters ()
:precondition
(and
(not (BELOWOF4-ROBOT))
)
:effect
(and
(Flag83prime-)
(not (Flag83-))
)

)
(:action Flag85Action
:parameters ()
:precondition
(and
(ABOVEOF4-ROBOT)
(BELOWOF5-ROBOT)
)
:effect
(and
(Flag85-)
(Flag85prime-)
)

)
(:action Flag86Action
:parameters ()
:precondition
(and
(ABOVEOF8-ROBOT)
(BELOWOF5-ROBOT)
)
:effect
(and
(Flag86-)
(Flag86prime-)
)

)
(:action Flag87Action
:parameters ()
:precondition
(and
(ABOVEOF6-ROBOT)
(BELOWOF5-ROBOT)
)
:effect
(and
(Flag87-)
(Flag87prime-)
)

)
(:action Flag88Action
:parameters ()
:precondition
(and
(ABOVEOF13-ROBOT)
(BELOWOF5-ROBOT)
)
:effect
(and
(Flag88-)
(Flag88prime-)
)

)
(:action Flag89Action
:parameters ()
:precondition
(and
(ABOVEOF10-ROBOT)
(BELOWOF5-ROBOT)
)
:effect
(and
(Flag89-)
(Flag89prime-)
)

)
(:action Flag90Action
:parameters ()
:precondition
(and
(ABOVEOF11-ROBOT)
(BELOWOF5-ROBOT)
)
:effect
(and
(Flag90-)
(Flag90prime-)
)

)
(:action Flag91Action
:parameters ()
:precondition
(and
(ABOVEOF5-ROBOT)
(BELOWOF5-ROBOT)
)
:effect
(and
(Flag91-)
(Flag91prime-)
)

)
(:action Flag92Action
:parameters ()
:precondition
(and
(ABOVEOF7-ROBOT)
(BELOWOF5-ROBOT)
)
:effect
(and
(Flag92-)
(Flag92prime-)
)

)
(:action Flag93Action
:parameters ()
:precondition
(and
(ABOVEOF9-ROBOT)
(BELOWOF5-ROBOT)
)
:effect
(and
(Flag93-)
(Flag93prime-)
)

)
(:action Flag94Action
:parameters ()
:precondition
(and
(ABOVEOF12-ROBOT)
(BELOWOF5-ROBOT)
)
:effect
(and
(Flag94-)
(Flag94prime-)
)

)
(:action Prim85Action-0
:parameters ()
:precondition
(and
(not (ABOVEOF4-ROBOT))
)
:effect
(and
(Flag85prime-)
(not (Flag85-))
)

)
(:action Prim85Action-1
:parameters ()
:precondition
(and
(not (BELOWOF5-ROBOT))
)
:effect
(and
(Flag85prime-)
(not (Flag85-))
)

)
(:action Prim86Action-0
:parameters ()
:precondition
(and
(not (ABOVEOF8-ROBOT))
)
:effect
(and
(Flag86prime-)
(not (Flag86-))
)

)
(:action Prim86Action-1
:parameters ()
:precondition
(and
(not (BELOWOF5-ROBOT))
)
:effect
(and
(Flag86prime-)
(not (Flag86-))
)

)
(:action Prim87Action-0
:parameters ()
:precondition
(and
(not (ABOVEOF6-ROBOT))
)
:effect
(and
(Flag87prime-)
(not (Flag87-))
)

)
(:action Prim87Action-1
:parameters ()
:precondition
(and
(not (BELOWOF5-ROBOT))
)
:effect
(and
(Flag87prime-)
(not (Flag87-))
)

)
(:action Prim88Action-0
:parameters ()
:precondition
(and
(not (ABOVEOF13-ROBOT))
)
:effect
(and
(Flag88prime-)
(not (Flag88-))
)

)
(:action Prim88Action-1
:parameters ()
:precondition
(and
(not (BELOWOF5-ROBOT))
)
:effect
(and
(Flag88prime-)
(not (Flag88-))
)

)
(:action Prim89Action-0
:parameters ()
:precondition
(and
(not (ABOVEOF10-ROBOT))
)
:effect
(and
(Flag89prime-)
(not (Flag89-))
)

)
(:action Prim89Action-1
:parameters ()
:precondition
(and
(not (BELOWOF5-ROBOT))
)
:effect
(and
(Flag89prime-)
(not (Flag89-))
)

)
(:action Prim90Action-0
:parameters ()
:precondition
(and
(not (ABOVEOF11-ROBOT))
)
:effect
(and
(Flag90prime-)
(not (Flag90-))
)

)
(:action Prim90Action-1
:parameters ()
:precondition
(and
(not (BELOWOF5-ROBOT))
)
:effect
(and
(Flag90prime-)
(not (Flag90-))
)

)
(:action Prim91Action-0
:parameters ()
:precondition
(and
(not (ABOVEOF5-ROBOT))
)
:effect
(and
(Flag91prime-)
(not (Flag91-))
)

)
(:action Prim91Action-1
:parameters ()
:precondition
(and
(not (BELOWOF5-ROBOT))
)
:effect
(and
(Flag91prime-)
(not (Flag91-))
)

)
(:action Prim92Action-0
:parameters ()
:precondition
(and
(not (ABOVEOF7-ROBOT))
)
:effect
(and
(Flag92prime-)
(not (Flag92-))
)

)
(:action Prim92Action-1
:parameters ()
:precondition
(and
(not (BELOWOF5-ROBOT))
)
:effect
(and
(Flag92prime-)
(not (Flag92-))
)

)
(:action Prim93Action-0
:parameters ()
:precondition
(and
(not (ABOVEOF9-ROBOT))
)
:effect
(and
(Flag93prime-)
(not (Flag93-))
)

)
(:action Prim93Action-1
:parameters ()
:precondition
(and
(not (BELOWOF5-ROBOT))
)
:effect
(and
(Flag93prime-)
(not (Flag93-))
)

)
(:action Prim94Action-0
:parameters ()
:precondition
(and
(not (ABOVEOF12-ROBOT))
)
:effect
(and
(Flag94prime-)
(not (Flag94-))
)

)
(:action Prim94Action-1
:parameters ()
:precondition
(and
(not (BELOWOF5-ROBOT))
)
:effect
(and
(Flag94prime-)
(not (Flag94-))
)

)
(:action Flag96Action
:parameters ()
:precondition
(and
(ABOVEOF11-ROBOT)
(BELOWOF6-ROBOT)
)
:effect
(and
(Flag96-)
(Flag96prime-)
)

)
(:action Flag97Action
:parameters ()
:precondition
(and
(ABOVEOF12-ROBOT)
(BELOWOF6-ROBOT)
)
:effect
(and
(Flag97-)
(Flag97prime-)
)

)
(:action Flag98Action
:parameters ()
:precondition
(and
(BELOWOF5-ROBOT)
(ABOVEOF13-ROBOT)
)
:effect
(and
(Flag98-)
(Flag98prime-)
)

)
(:action Flag99Action
:parameters ()
:precondition
(and
(BELOWOF6-ROBOT)
(ABOVEOF13-ROBOT)
)
:effect
(and
(Flag99-)
(Flag99prime-)
)

)
(:action Flag100Action
:parameters ()
:precondition
(and
(BELOWOF6-ROBOT)
(ABOVEOF5-ROBOT)
)
:effect
(and
(Flag100-)
(Flag100prime-)
)

)
(:action Flag101Action
:parameters ()
:precondition
(and
(BELOWOF6-ROBOT)
(ABOVEOF7-ROBOT)
)
:effect
(and
(Flag101-)
(Flag101prime-)
)

)
(:action Flag102Action
:parameters ()
:precondition
(and
(BELOWOF5-ROBOT)
(ABOVEOF5-ROBOT)
)
:effect
(and
(Flag102-)
(Flag102prime-)
)

)
(:action Flag103Action
:parameters ()
:precondition
(and
(BELOWOF6-ROBOT)
(ABOVEOF6-ROBOT)
)
:effect
(and
(Flag103-)
(Flag103prime-)
)

)
(:action Flag104Action
:parameters ()
:precondition
(and
(ABOVEOF8-ROBOT)
(BELOWOF6-ROBOT)
)
:effect
(and
(Flag104-)
(Flag104prime-)
)

)
(:action Flag105Action
:parameters ()
:precondition
(and
(BELOWOF6-ROBOT)
(ABOVEOF9-ROBOT)
)
:effect
(and
(Flag105-)
(Flag105prime-)
)

)
(:action Flag106Action
:parameters ()
:precondition
(and
(BELOWOF6-ROBOT)
(ABOVEOF10-ROBOT)
)
:effect
(and
(Flag106-)
(Flag106prime-)
)

)
(:action Prim96Action-0
:parameters ()
:precondition
(and
(not (ABOVEOF11-ROBOT))
)
:effect
(and
(Flag96prime-)
(not (Flag96-))
)

)
(:action Prim96Action-1
:parameters ()
:precondition
(and
(not (BELOWOF6-ROBOT))
)
:effect
(and
(Flag96prime-)
(not (Flag96-))
)

)
(:action Prim97Action-0
:parameters ()
:precondition
(and
(not (ABOVEOF12-ROBOT))
)
:effect
(and
(Flag97prime-)
(not (Flag97-))
)

)
(:action Prim97Action-1
:parameters ()
:precondition
(and
(not (BELOWOF6-ROBOT))
)
:effect
(and
(Flag97prime-)
(not (Flag97-))
)

)
(:action Prim98Action-0
:parameters ()
:precondition
(and
(not (BELOWOF5-ROBOT))
)
:effect
(and
(Flag98prime-)
(not (Flag98-))
)

)
(:action Prim98Action-1
:parameters ()
:precondition
(and
(not (ABOVEOF13-ROBOT))
)
:effect
(and
(Flag98prime-)
(not (Flag98-))
)

)
(:action Prim99Action-0
:parameters ()
:precondition
(and
(not (BELOWOF6-ROBOT))
)
:effect
(and
(Flag99prime-)
(not (Flag99-))
)

)
(:action Prim99Action-1
:parameters ()
:precondition
(and
(not (ABOVEOF13-ROBOT))
)
:effect
(and
(Flag99prime-)
(not (Flag99-))
)

)
(:action Prim100Action-0
:parameters ()
:precondition
(and
(not (BELOWOF6-ROBOT))
)
:effect
(and
(Flag100prime-)
(not (Flag100-))
)

)
(:action Prim100Action-1
:parameters ()
:precondition
(and
(not (ABOVEOF5-ROBOT))
)
:effect
(and
(Flag100prime-)
(not (Flag100-))
)

)
(:action Prim101Action-0
:parameters ()
:precondition
(and
(not (BELOWOF6-ROBOT))
)
:effect
(and
(Flag101prime-)
(not (Flag101-))
)

)
(:action Prim101Action-1
:parameters ()
:precondition
(and
(not (ABOVEOF7-ROBOT))
)
:effect
(and
(Flag101prime-)
(not (Flag101-))
)

)
(:action Prim102Action-0
:parameters ()
:precondition
(and
(not (BELOWOF5-ROBOT))
)
:effect
(and
(Flag102prime-)
(not (Flag102-))
)

)
(:action Prim102Action-1
:parameters ()
:precondition
(and
(not (ABOVEOF5-ROBOT))
)
:effect
(and
(Flag102prime-)
(not (Flag102-))
)

)
(:action Prim103Action-0
:parameters ()
:precondition
(and
(not (BELOWOF6-ROBOT))
)
:effect
(and
(Flag103prime-)
(not (Flag103-))
)

)
(:action Prim103Action-1
:parameters ()
:precondition
(and
(not (ABOVEOF6-ROBOT))
)
:effect
(and
(Flag103prime-)
(not (Flag103-))
)

)
(:action Prim104Action-0
:parameters ()
:precondition
(and
(not (ABOVEOF8-ROBOT))
)
:effect
(and
(Flag104prime-)
(not (Flag104-))
)

)
(:action Prim104Action-1
:parameters ()
:precondition
(and
(not (BELOWOF6-ROBOT))
)
:effect
(and
(Flag104prime-)
(not (Flag104-))
)

)
(:action Prim105Action-0
:parameters ()
:precondition
(and
(not (BELOWOF6-ROBOT))
)
:effect
(and
(Flag105prime-)
(not (Flag105-))
)

)
(:action Prim105Action-1
:parameters ()
:precondition
(and
(not (ABOVEOF9-ROBOT))
)
:effect
(and
(Flag105prime-)
(not (Flag105-))
)

)
(:action Prim106Action-0
:parameters ()
:precondition
(and
(not (BELOWOF6-ROBOT))
)
:effect
(and
(Flag106prime-)
(not (Flag106-))
)

)
(:action Prim106Action-1
:parameters ()
:precondition
(and
(not (ABOVEOF10-ROBOT))
)
:effect
(and
(Flag106prime-)
(not (Flag106-))
)

)
(:action Flag108Action
:parameters ()
:precondition
(and
(ABOVEOF6-ROBOT)
(BELOWOF7-ROBOT)
)
:effect
(and
(Flag108-)
(Flag108prime-)
)

)
(:action Flag109Action
:parameters ()
:precondition
(and
(ABOVEOF9-ROBOT)
(BELOWOF7-ROBOT)
)
:effect
(and
(Flag109-)
(Flag109prime-)
)

)
(:action Flag110Action
:parameters ()
:precondition
(and
(ABOVEOF6-ROBOT)
(BELOWOF6-ROBOT)
)
:effect
(and
(Flag110-)
(Flag110prime-)
)

)
(:action Flag111Action
:parameters ()
:precondition
(and
(ABOVEOF10-ROBOT)
(BELOWOF7-ROBOT)
)
:effect
(and
(Flag111-)
(Flag111prime-)
)

)
(:action Flag112Action
:parameters ()
:precondition
(and
(ABOVEOF10-ROBOT)
(BELOWOF6-ROBOT)
)
:effect
(and
(Flag112-)
(Flag112prime-)
)

)
(:action Flag113Action
:parameters ()
:precondition
(and
(ABOVEOF12-ROBOT)
(BELOWOF7-ROBOT)
)
:effect
(and
(Flag113-)
(Flag113prime-)
)

)
(:action Flag114Action
:parameters ()
:precondition
(and
(BELOWOF7-ROBOT)
(ABOVEOF13-ROBOT)
)
:effect
(and
(Flag114-)
(Flag114prime-)
)

)
(:action Flag115Action
:parameters ()
:precondition
(and
(ABOVEOF7-ROBOT)
(BELOWOF7-ROBOT)
)
:effect
(and
(Flag115-)
(Flag115prime-)
)

)
(:action Flag116Action
:parameters ()
:precondition
(and
(ABOVEOF8-ROBOT)
(BELOWOF7-ROBOT)
)
:effect
(and
(Flag116-)
(Flag116prime-)
)

)
(:action Flag117Action
:parameters ()
:precondition
(and
(ABOVEOF11-ROBOT)
(BELOWOF7-ROBOT)
)
:effect
(and
(Flag117-)
(Flag117prime-)
)

)
(:action Prim108Action-0
:parameters ()
:precondition
(and
(not (ABOVEOF6-ROBOT))
)
:effect
(and
(Flag108prime-)
(not (Flag108-))
)

)
(:action Prim108Action-1
:parameters ()
:precondition
(and
(not (BELOWOF7-ROBOT))
)
:effect
(and
(Flag108prime-)
(not (Flag108-))
)

)
(:action Prim109Action-0
:parameters ()
:precondition
(and
(not (ABOVEOF9-ROBOT))
)
:effect
(and
(Flag109prime-)
(not (Flag109-))
)

)
(:action Prim109Action-1
:parameters ()
:precondition
(and
(not (BELOWOF7-ROBOT))
)
:effect
(and
(Flag109prime-)
(not (Flag109-))
)

)
(:action Prim110Action-0
:parameters ()
:precondition
(and
(not (ABOVEOF6-ROBOT))
)
:effect
(and
(Flag110prime-)
(not (Flag110-))
)

)
(:action Prim110Action-1
:parameters ()
:precondition
(and
(not (BELOWOF6-ROBOT))
)
:effect
(and
(Flag110prime-)
(not (Flag110-))
)

)
(:action Prim111Action-0
:parameters ()
:precondition
(and
(not (ABOVEOF10-ROBOT))
)
:effect
(and
(Flag111prime-)
(not (Flag111-))
)

)
(:action Prim111Action-1
:parameters ()
:precondition
(and
(not (BELOWOF7-ROBOT))
)
:effect
(and
(Flag111prime-)
(not (Flag111-))
)

)
(:action Prim112Action-0
:parameters ()
:precondition
(and
(not (ABOVEOF10-ROBOT))
)
:effect
(and
(Flag112prime-)
(not (Flag112-))
)

)
(:action Prim112Action-1
:parameters ()
:precondition
(and
(not (BELOWOF6-ROBOT))
)
:effect
(and
(Flag112prime-)
(not (Flag112-))
)

)
(:action Prim113Action-0
:parameters ()
:precondition
(and
(not (ABOVEOF12-ROBOT))
)
:effect
(and
(Flag113prime-)
(not (Flag113-))
)

)
(:action Prim113Action-1
:parameters ()
:precondition
(and
(not (BELOWOF7-ROBOT))
)
:effect
(and
(Flag113prime-)
(not (Flag113-))
)

)
(:action Prim114Action-0
:parameters ()
:precondition
(and
(not (BELOWOF7-ROBOT))
)
:effect
(and
(Flag114prime-)
(not (Flag114-))
)

)
(:action Prim114Action-1
:parameters ()
:precondition
(and
(not (ABOVEOF13-ROBOT))
)
:effect
(and
(Flag114prime-)
(not (Flag114-))
)

)
(:action Prim115Action-0
:parameters ()
:precondition
(and
(not (ABOVEOF7-ROBOT))
)
:effect
(and
(Flag115prime-)
(not (Flag115-))
)

)
(:action Prim115Action-1
:parameters ()
:precondition
(and
(not (BELOWOF7-ROBOT))
)
:effect
(and
(Flag115prime-)
(not (Flag115-))
)

)
(:action Prim116Action-0
:parameters ()
:precondition
(and
(not (ABOVEOF8-ROBOT))
)
:effect
(and
(Flag116prime-)
(not (Flag116-))
)

)
(:action Prim116Action-1
:parameters ()
:precondition
(and
(not (BELOWOF7-ROBOT))
)
:effect
(and
(Flag116prime-)
(not (Flag116-))
)

)
(:action Prim117Action-0
:parameters ()
:precondition
(and
(not (ABOVEOF11-ROBOT))
)
:effect
(and
(Flag117prime-)
(not (Flag117-))
)

)
(:action Prim117Action-1
:parameters ()
:precondition
(and
(not (BELOWOF7-ROBOT))
)
:effect
(and
(Flag117prime-)
(not (Flag117-))
)

)
(:action Flag119Action
:parameters ()
:precondition
(and
(BELOWOF8-ROBOT)
(ABOVEOF13-ROBOT)
)
:effect
(and
(Flag119-)
(Flag119prime-)
)

)
(:action Flag120Action
:parameters ()
:precondition
(and
(BELOWOF8-ROBOT)
(ABOVEOF10-ROBOT)
)
:effect
(and
(Flag120-)
(Flag120prime-)
)

)
(:action Flag121Action
:parameters ()
:precondition
(and
(ABOVEOF12-ROBOT)
(BELOWOF8-ROBOT)
)
:effect
(and
(Flag121-)
(Flag121prime-)
)

)
(:action Flag122Action
:parameters ()
:precondition
(and
(ABOVEOF8-ROBOT)
(BELOWOF8-ROBOT)
)
:effect
(and
(Flag122-)
(Flag122prime-)
)

)
(:action Flag123Action
:parameters ()
:precondition
(and
(BELOWOF8-ROBOT)
(ABOVEOF9-ROBOT)
)
:effect
(and
(Flag123-)
(Flag123prime-)
)

)
(:action Flag124Action
:parameters ()
:precondition
(and
(BELOWOF7-ROBOT)
(ABOVEOF7-ROBOT)
)
:effect
(and
(Flag124-)
(Flag124prime-)
)

)
(:action Flag125Action
:parameters ()
:precondition
(and
(BELOWOF8-ROBOT)
(ABOVEOF7-ROBOT)
)
:effect
(and
(Flag125-)
(Flag125prime-)
)

)
(:action Flag126Action
:parameters ()
:precondition
(and
(BELOWOF8-ROBOT)
(ABOVEOF11-ROBOT)
)
:effect
(and
(Flag126-)
(Flag126prime-)
)

)
(:action Prim119Action-0
:parameters ()
:precondition
(and
(not (BELOWOF8-ROBOT))
)
:effect
(and
(Flag119prime-)
(not (Flag119-))
)

)
(:action Prim119Action-1
:parameters ()
:precondition
(and
(not (ABOVEOF13-ROBOT))
)
:effect
(and
(Flag119prime-)
(not (Flag119-))
)

)
(:action Prim120Action-0
:parameters ()
:precondition
(and
(not (BELOWOF8-ROBOT))
)
:effect
(and
(Flag120prime-)
(not (Flag120-))
)

)
(:action Prim120Action-1
:parameters ()
:precondition
(and
(not (ABOVEOF10-ROBOT))
)
:effect
(and
(Flag120prime-)
(not (Flag120-))
)

)
(:action Prim121Action-0
:parameters ()
:precondition
(and
(not (ABOVEOF12-ROBOT))
)
:effect
(and
(Flag121prime-)
(not (Flag121-))
)

)
(:action Prim121Action-1
:parameters ()
:precondition
(and
(not (BELOWOF8-ROBOT))
)
:effect
(and
(Flag121prime-)
(not (Flag121-))
)

)
(:action Prim122Action-0
:parameters ()
:precondition
(and
(not (ABOVEOF8-ROBOT))
)
:effect
(and
(Flag122prime-)
(not (Flag122-))
)

)
(:action Prim122Action-1
:parameters ()
:precondition
(and
(not (BELOWOF8-ROBOT))
)
:effect
(and
(Flag122prime-)
(not (Flag122-))
)

)
(:action Prim123Action-0
:parameters ()
:precondition
(and
(not (BELOWOF8-ROBOT))
)
:effect
(and
(Flag123prime-)
(not (Flag123-))
)

)
(:action Prim123Action-1
:parameters ()
:precondition
(and
(not (ABOVEOF9-ROBOT))
)
:effect
(and
(Flag123prime-)
(not (Flag123-))
)

)
(:action Prim124Action-0
:parameters ()
:precondition
(and
(not (BELOWOF7-ROBOT))
)
:effect
(and
(Flag124prime-)
(not (Flag124-))
)

)
(:action Prim124Action-1
:parameters ()
:precondition
(and
(not (ABOVEOF7-ROBOT))
)
:effect
(and
(Flag124prime-)
(not (Flag124-))
)

)
(:action Prim125Action-0
:parameters ()
:precondition
(and
(not (BELOWOF8-ROBOT))
)
:effect
(and
(Flag125prime-)
(not (Flag125-))
)

)
(:action Prim125Action-1
:parameters ()
:precondition
(and
(not (ABOVEOF7-ROBOT))
)
:effect
(and
(Flag125prime-)
(not (Flag125-))
)

)
(:action Prim126Action-0
:parameters ()
:precondition
(and
(not (BELOWOF8-ROBOT))
)
:effect
(and
(Flag126prime-)
(not (Flag126-))
)

)
(:action Prim126Action-1
:parameters ()
:precondition
(and
(not (ABOVEOF11-ROBOT))
)
:effect
(and
(Flag126prime-)
(not (Flag126-))
)

)
(:action Flag128Action
:parameters ()
:precondition
(and
(BELOWOF8-ROBOT)
(ABOVEOF12-ROBOT)
)
:effect
(and
(Flag128-)
(Flag128prime-)
)

)
(:action Flag129Action
:parameters ()
:precondition
(and
(ABOVEOF10-ROBOT)
(BELOWOF9-ROBOT)
)
:effect
(and
(Flag129-)
(Flag129prime-)
)

)
(:action Flag130Action
:parameters ()
:precondition
(and
(BELOWOF8-ROBOT)
(ABOVEOF8-ROBOT)
)
:effect
(and
(Flag130-)
(Flag130prime-)
)

)
(:action Flag131Action
:parameters ()
:precondition
(and
(BELOWOF9-ROBOT)
(ABOVEOF13-ROBOT)
)
:effect
(and
(Flag131-)
(Flag131prime-)
)

)
(:action Flag132Action
:parameters ()
:precondition
(and
(ABOVEOF8-ROBOT)
(BELOWOF9-ROBOT)
)
:effect
(and
(Flag132-)
(Flag132prime-)
)

)
(:action Flag133Action
:parameters ()
:precondition
(and
(ABOVEOF12-ROBOT)
(BELOWOF9-ROBOT)
)
:effect
(and
(Flag133-)
(Flag133prime-)
)

)
(:action Flag134Action
:parameters ()
:precondition
(and
(ABOVEOF11-ROBOT)
(BELOWOF9-ROBOT)
)
:effect
(and
(Flag134-)
(Flag134prime-)
)

)
(:action Flag135Action
:parameters ()
:precondition
(and
(BELOWOF9-ROBOT)
(ABOVEOF9-ROBOT)
)
:effect
(and
(Flag135-)
(Flag135prime-)
)

)
(:action Prim128Action-0
:parameters ()
:precondition
(and
(not (BELOWOF8-ROBOT))
)
:effect
(and
(Flag128prime-)
(not (Flag128-))
)

)
(:action Prim128Action-1
:parameters ()
:precondition
(and
(not (ABOVEOF12-ROBOT))
)
:effect
(and
(Flag128prime-)
(not (Flag128-))
)

)
(:action Prim129Action-0
:parameters ()
:precondition
(and
(not (ABOVEOF10-ROBOT))
)
:effect
(and
(Flag129prime-)
(not (Flag129-))
)

)
(:action Prim129Action-1
:parameters ()
:precondition
(and
(not (BELOWOF9-ROBOT))
)
:effect
(and
(Flag129prime-)
(not (Flag129-))
)

)
(:action Prim130Action-0
:parameters ()
:precondition
(and
(not (BELOWOF8-ROBOT))
)
:effect
(and
(Flag130prime-)
(not (Flag130-))
)

)
(:action Prim130Action-1
:parameters ()
:precondition
(and
(not (ABOVEOF8-ROBOT))
)
:effect
(and
(Flag130prime-)
(not (Flag130-))
)

)
(:action Prim131Action-0
:parameters ()
:precondition
(and
(not (BELOWOF9-ROBOT))
)
:effect
(and
(Flag131prime-)
(not (Flag131-))
)

)
(:action Prim131Action-1
:parameters ()
:precondition
(and
(not (ABOVEOF13-ROBOT))
)
:effect
(and
(Flag131prime-)
(not (Flag131-))
)

)
(:action Prim132Action-0
:parameters ()
:precondition
(and
(not (ABOVEOF8-ROBOT))
)
:effect
(and
(Flag132prime-)
(not (Flag132-))
)

)
(:action Prim132Action-1
:parameters ()
:precondition
(and
(not (BELOWOF9-ROBOT))
)
:effect
(and
(Flag132prime-)
(not (Flag132-))
)

)
(:action Prim133Action-0
:parameters ()
:precondition
(and
(not (ABOVEOF12-ROBOT))
)
:effect
(and
(Flag133prime-)
(not (Flag133-))
)

)
(:action Prim133Action-1
:parameters ()
:precondition
(and
(not (BELOWOF9-ROBOT))
)
:effect
(and
(Flag133prime-)
(not (Flag133-))
)

)
(:action Prim134Action-0
:parameters ()
:precondition
(and
(not (ABOVEOF11-ROBOT))
)
:effect
(and
(Flag134prime-)
(not (Flag134-))
)

)
(:action Prim134Action-1
:parameters ()
:precondition
(and
(not (BELOWOF9-ROBOT))
)
:effect
(and
(Flag134prime-)
(not (Flag134-))
)

)
(:action Prim135Action-0
:parameters ()
:precondition
(and
(not (BELOWOF9-ROBOT))
)
:effect
(and
(Flag135prime-)
(not (Flag135-))
)

)
(:action Prim135Action-1
:parameters ()
:precondition
(and
(not (ABOVEOF9-ROBOT))
)
:effect
(and
(Flag135prime-)
(not (Flag135-))
)

)
(:action Flag137Action
:parameters ()
:precondition
(and
(BELOWOF10-ROBOT)
(ABOVEOF9-ROBOT)
)
:effect
(and
(Flag137-)
(Flag137prime-)
)

)
(:action Flag138Action
:parameters ()
:precondition
(and
(BELOWOF10-ROBOT)
(ABOVEOF10-ROBOT)
)
:effect
(and
(Flag138-)
(Flag138prime-)
)

)
(:action Flag139Action
:parameters ()
:precondition
(and
(ABOVEOF12-ROBOT)
(BELOWOF10-ROBOT)
)
:effect
(and
(Flag139-)
(Flag139prime-)
)

)
(:action Flag140Action
:parameters ()
:precondition
(and
(ABOVEOF9-ROBOT)
(BELOWOF9-ROBOT)
)
:effect
(and
(Flag140-)
(Flag140prime-)
)

)
(:action Flag141Action
:parameters ()
:precondition
(and
(ABOVEOF11-ROBOT)
(BELOWOF10-ROBOT)
)
:effect
(and
(Flag141-)
(Flag141prime-)
)

)
(:action Flag142Action
:parameters ()
:precondition
(and
(BELOWOF10-ROBOT)
(ABOVEOF13-ROBOT)
)
:effect
(and
(Flag142-)
(Flag142prime-)
)

)
(:action Prim137Action-0
:parameters ()
:precondition
(and
(not (BELOWOF10-ROBOT))
)
:effect
(and
(Flag137prime-)
(not (Flag137-))
)

)
(:action Prim137Action-1
:parameters ()
:precondition
(and
(not (ABOVEOF9-ROBOT))
)
:effect
(and
(Flag137prime-)
(not (Flag137-))
)

)
(:action Prim138Action-0
:parameters ()
:precondition
(and
(not (BELOWOF10-ROBOT))
)
:effect
(and
(Flag138prime-)
(not (Flag138-))
)

)
(:action Prim138Action-1
:parameters ()
:precondition
(and
(not (ABOVEOF10-ROBOT))
)
:effect
(and
(Flag138prime-)
(not (Flag138-))
)

)
(:action Prim139Action-0
:parameters ()
:precondition
(and
(not (ABOVEOF12-ROBOT))
)
:effect
(and
(Flag139prime-)
(not (Flag139-))
)

)
(:action Prim139Action-1
:parameters ()
:precondition
(and
(not (BELOWOF10-ROBOT))
)
:effect
(and
(Flag139prime-)
(not (Flag139-))
)

)
(:action Prim140Action-0
:parameters ()
:precondition
(and
(not (ABOVEOF9-ROBOT))
)
:effect
(and
(Flag140prime-)
(not (Flag140-))
)

)
(:action Prim140Action-1
:parameters ()
:precondition
(and
(not (BELOWOF9-ROBOT))
)
:effect
(and
(Flag140prime-)
(not (Flag140-))
)

)
(:action Prim141Action-0
:parameters ()
:precondition
(and
(not (ABOVEOF11-ROBOT))
)
:effect
(and
(Flag141prime-)
(not (Flag141-))
)

)
(:action Prim141Action-1
:parameters ()
:precondition
(and
(not (BELOWOF10-ROBOT))
)
:effect
(and
(Flag141prime-)
(not (Flag141-))
)

)
(:action Prim142Action-0
:parameters ()
:precondition
(and
(not (BELOWOF10-ROBOT))
)
:effect
(and
(Flag142prime-)
(not (Flag142-))
)

)
(:action Prim142Action-1
:parameters ()
:precondition
(and
(not (ABOVEOF13-ROBOT))
)
:effect
(and
(Flag142prime-)
(not (Flag142-))
)

)
(:action Flag144Action
:parameters ()
:precondition
(and
(BELOWOF11-ROBOT)
(ABOVEOF10-ROBOT)
)
:effect
(and
(Flag144-)
(Flag144prime-)
)

)
(:action Flag145Action
:parameters ()
:precondition
(and
(ABOVEOF11-ROBOT)
(BELOWOF11-ROBOT)
)
:effect
(and
(Flag145-)
(Flag145prime-)
)

)
(:action Flag146Action
:parameters ()
:precondition
(and
(BELOWOF11-ROBOT)
(ABOVEOF13-ROBOT)
)
:effect
(and
(Flag146-)
(Flag146prime-)
)

)
(:action Flag147Action
:parameters ()
:precondition
(and
(ABOVEOF12-ROBOT)
(BELOWOF11-ROBOT)
)
:effect
(and
(Flag147-)
(Flag147prime-)
)

)
(:action Prim144Action-0
:parameters ()
:precondition
(and
(not (BELOWOF11-ROBOT))
)
:effect
(and
(Flag144prime-)
(not (Flag144-))
)

)
(:action Prim144Action-1
:parameters ()
:precondition
(and
(not (ABOVEOF10-ROBOT))
)
:effect
(and
(Flag144prime-)
(not (Flag144-))
)

)
(:action Prim145Action-0
:parameters ()
:precondition
(and
(not (ABOVEOF11-ROBOT))
)
:effect
(and
(Flag145prime-)
(not (Flag145-))
)

)
(:action Prim145Action-1
:parameters ()
:precondition
(and
(not (BELOWOF11-ROBOT))
)
:effect
(and
(Flag145prime-)
(not (Flag145-))
)

)
(:action Prim146Action-0
:parameters ()
:precondition
(and
(not (BELOWOF11-ROBOT))
)
:effect
(and
(Flag146prime-)
(not (Flag146-))
)

)
(:action Prim146Action-1
:parameters ()
:precondition
(and
(not (ABOVEOF13-ROBOT))
)
:effect
(and
(Flag146prime-)
(not (Flag146-))
)

)
(:action Prim147Action-0
:parameters ()
:precondition
(and
(not (ABOVEOF12-ROBOT))
)
:effect
(and
(Flag147prime-)
(not (Flag147-))
)

)
(:action Prim147Action-1
:parameters ()
:precondition
(and
(not (BELOWOF11-ROBOT))
)
:effect
(and
(Flag147prime-)
(not (Flag147-))
)

)
(:action Flag149Action
:parameters ()
:precondition
(and
(ABOVEOF12-ROBOT)
(BELOWOF12-ROBOT)
)
:effect
(and
(Flag149-)
(Flag149prime-)
)

)
(:action Flag150Action
:parameters ()
:precondition
(and
(BELOWOF12-ROBOT)
(ABOVEOF13-ROBOT)
)
:effect
(and
(Flag150-)
(Flag150prime-)
)

)
(:action Flag151Action
:parameters ()
:precondition
(and
(BELOWOF12-ROBOT)
(ABOVEOF11-ROBOT)
)
:effect
(and
(Flag151-)
(Flag151prime-)
)

)
(:action Prim149Action-0
:parameters ()
:precondition
(and
(not (ABOVEOF12-ROBOT))
)
:effect
(and
(Flag149prime-)
(not (Flag149-))
)

)
(:action Prim149Action-1
:parameters ()
:precondition
(and
(not (BELOWOF12-ROBOT))
)
:effect
(and
(Flag149prime-)
(not (Flag149-))
)

)
(:action Prim150Action-0
:parameters ()
:precondition
(and
(not (BELOWOF12-ROBOT))
)
:effect
(and
(Flag150prime-)
(not (Flag150-))
)

)
(:action Prim150Action-1
:parameters ()
:precondition
(and
(not (ABOVEOF13-ROBOT))
)
:effect
(and
(Flag150prime-)
(not (Flag150-))
)

)
(:action Prim151Action-0
:parameters ()
:precondition
(and
(not (BELOWOF12-ROBOT))
)
:effect
(and
(Flag151prime-)
(not (Flag151-))
)

)
(:action Prim151Action-1
:parameters ()
:precondition
(and
(not (ABOVEOF11-ROBOT))
)
:effect
(and
(Flag151prime-)
(not (Flag151-))
)

)
(:action Flag153Action
:parameters ()
:precondition
(and
(ABOVEOF13-ROBOT)
(BELOWOF13-ROBOT)
)
:effect
(and
(Flag153-)
(Flag153prime-)
)

)
(:action Flag154Action
:parameters ()
:precondition
(and
(ABOVEOF12-ROBOT)
(BELOWOF13-ROBOT)
)
:effect
(and
(Flag154-)
(Flag154prime-)
)

)
(:action Flag155Action
:parameters ()
:precondition
(and
(BELOWOF12-ROBOT)
(ABOVEOF12-ROBOT)
)
:effect
(and
(Flag155-)
(Flag155prime-)
)

)
(:action Prim153Action-0
:parameters ()
:precondition
(and
(not (ABOVEOF13-ROBOT))
)
:effect
(and
(Flag153prime-)
(not (Flag153-))
)

)
(:action Prim154Action-0
:parameters ()
:precondition
(and
(not (ABOVEOF12-ROBOT))
)
:effect
(and
(Flag154prime-)
(not (Flag154-))
)

)
(:action Prim155Action-0
:parameters ()
:precondition
(and
(not (BELOWOF12-ROBOT))
)
:effect
(and
(Flag155prime-)
(not (Flag155-))
)

)
(:action Prim155Action-1
:parameters ()
:precondition
(and
(not (ABOVEOF12-ROBOT))
)
:effect
(and
(Flag155prime-)
(not (Flag155-))
)

)
(:action Flag157Action-0
:parameters ()
:precondition
(and
(BELOWOF10-ROBOT)
)
:effect
(and
(Flag157-)
(Flag157prime-)
)

)
(:action Flag157Action-1
:parameters ()
:precondition
(and
(BELOWOF11-ROBOT)
)
:effect
(and
(Flag157-)
(Flag157prime-)
)

)
(:action Flag157Action-2
:parameters ()
:precondition
(and
(BELOWOF6-ROBOT)
)
:effect
(and
(Flag157-)
(Flag157prime-)
)

)
(:action Flag157Action-3
:parameters ()
:precondition
(and
(BELOWOF5-ROBOT)
)
:effect
(and
(Flag157-)
(Flag157prime-)
)

)
(:action Flag157Action-4
:parameters ()
:precondition
(and
(BELOWOF2-ROBOT)
)
:effect
(and
(Flag157-)
(Flag157prime-)
)

)
(:action Flag157Action-5
:parameters ()
:precondition
(and
(BELOWOF1-ROBOT)
)
:effect
(and
(Flag157-)
(Flag157prime-)
)

)
(:action Flag157Action-6
:parameters ()
:precondition
(and
(BELOWOF4-ROBOT)
)
:effect
(and
(Flag157-)
(Flag157prime-)
)

)
(:action Flag157Action-7
:parameters ()
:precondition
(and
(BELOWOF12-ROBOT)
)
:effect
(and
(Flag157-)
(Flag157prime-)
)

)
(:action Flag157Action-8
:parameters ()
:precondition
(and
(BELOWOF7-ROBOT)
)
:effect
(and
(Flag157-)
(Flag157prime-)
)

)
(:action Flag157Action-9
:parameters ()
:precondition
(and
(BELOWOF9-ROBOT)
)
:effect
(and
(Flag157-)
(Flag157prime-)
)

)
(:action Flag157Action-10
:parameters ()
:precondition
(and
(BELOWOF3-ROBOT)
)
:effect
(and
(Flag157-)
(Flag157prime-)
)

)
(:action Flag157Action-12
:parameters ()
:precondition
(and
(BELOWOF8-ROBOT)
)
:effect
(and
(Flag157-)
(Flag157prime-)
)

)
(:action Flag157Action-13
:parameters ()
:precondition
(and
(BELOWOF14-ROBOT)
)
:effect
(and
(Flag157-)
(Flag157prime-)
)

)
(:action Prim157Action
:parameters ()
:precondition
(and
(not (BELOWOF10-ROBOT))
(not (BELOWOF11-ROBOT))
(not (BELOWOF6-ROBOT))
(not (BELOWOF5-ROBOT))
(not (BELOWOF2-ROBOT))
(not (BELOWOF1-ROBOT))
(not (BELOWOF4-ROBOT))
(not (BELOWOF12-ROBOT))
(not (BELOWOF7-ROBOT))
(not (BELOWOF9-ROBOT))
(not (BELOWOF3-ROBOT))
(not (BELOWOF13-ROBOT))
(not (BELOWOF8-ROBOT))
(not (BELOWOF14-ROBOT))
)
:effect
(and
(Flag157prime-)
(not (Flag157-))
)

)
(:action Flag158Action
:parameters ()
:precondition
(and
(BELOWOF14-ROBOT)
(ABOVEOF13-ROBOT)
)
:effect
(and
(Flag158-)
(Flag158prime-)
)

)
(:action Flag159Action
:parameters ()
:precondition
(and
(BELOWOF13-ROBOT)
(ABOVEOF13-ROBOT)
)
:effect
(and
(Flag159-)
(Flag159prime-)
)

)
(:action Prim158Action-0
:parameters ()
:precondition
(and
(not (BELOWOF14-ROBOT))
)
:effect
(and
(Flag158prime-)
(not (Flag158-))
)

)
(:action Prim158Action-1
:parameters ()
:precondition
(and
(not (ABOVEOF13-ROBOT))
)
:effect
(and
(Flag158prime-)
(not (Flag158-))
)

)
(:action Prim159Action-1
:parameters ()
:precondition
(and
(not (ABOVEOF13-ROBOT))
)
:effect
(and
(Flag159prime-)
(not (Flag159-))
)

)
(:action MOVEDOWN-ROBOT
:parameters ()
:precondition
(and
(Flag4-)
(Flag4prime-)
(Flag160prime-)
(Flag156prime-)
(Flag152prime-)
(Flag148prime-)
(Flag143prime-)
(Flag136prime-)
(Flag127prime-)
(Flag118prime-)
(Flag107prime-)
(Flag95prime-)
(Flag84prime-)
(Flag70prime-)
(Flag56prime-)
(Flag157prime-)
)
:effect
(and
(when
(and
(Flag160-)
)
(and
(ROW12-ROBOT)
(not (NOT-ROW12-ROBOT))
)
)
(when
(and
(Flag156-)
)
(and
(ROW11-ROBOT)
(not (NOT-ROW11-ROBOT))
)
)
(when
(and
(Flag152-)
)
(and
(ROW10-ROBOT)
(not (NOT-ROW10-ROBOT))
)
)
(when
(and
(Flag148-)
)
(and
(ROW9-ROBOT)
(not (NOT-ROW9-ROBOT))
)
)
(when
(and
(Flag143-)
)
(and
(ROW8-ROBOT)
(not (NOT-ROW8-ROBOT))
)
)
(when
(and
(Flag136-)
)
(and
(ROW7-ROBOT)
(not (NOT-ROW7-ROBOT))
)
)
(when
(and
(Flag127-)
)
(and
(ROW6-ROBOT)
(not (NOT-ROW6-ROBOT))
)
)
(when
(and
(Flag118-)
)
(and
(ROW5-ROBOT)
(not (NOT-ROW5-ROBOT))
)
)
(when
(and
(Flag107-)
)
(and
(ROW4-ROBOT)
(not (NOT-ROW4-ROBOT))
)
)
(when
(and
(Flag95-)
)
(and
(ROW3-ROBOT)
(not (NOT-ROW3-ROBOT))
)
)
(when
(and
(Flag84-)
)
(and
(ROW2-ROBOT)
(not (NOT-ROW2-ROBOT))
)
)
(when
(and
(Flag70-)
)
(and
(ROW1-ROBOT)
(not (NOT-ROW1-ROBOT))
)
)
(when
(and
(Flag56-)
)
(and
(ROW0-ROBOT)
(not (NOT-ROW0-ROBOT))
)
)
(when
(and
(ROW13-ROBOT)
)
(and
(ROW12-ROBOT)
(NOT-ROW13-ROBOT)
(not (NOT-ROW12-ROBOT))
(not (ROW13-ROBOT))
)
)
(when
(and
(ROW12-ROBOT)
)
(and
(ROW11-ROBOT)
(NOT-ROW12-ROBOT)
(not (NOT-ROW11-ROBOT))
(not (ROW12-ROBOT))
)
)
(when
(and
(ROW11-ROBOT)
)
(and
(ROW10-ROBOT)
(NOT-ROW11-ROBOT)
(not (NOT-ROW10-ROBOT))
(not (ROW11-ROBOT))
)
)
(when
(and
(ROW10-ROBOT)
)
(and
(ROW9-ROBOT)
(NOT-ROW10-ROBOT)
(not (NOT-ROW9-ROBOT))
(not (ROW10-ROBOT))
)
)
(when
(and
(ROW9-ROBOT)
)
(and
(ROW8-ROBOT)
(NOT-ROW9-ROBOT)
(not (NOT-ROW8-ROBOT))
(not (ROW9-ROBOT))
)
)
(when
(and
(ROW8-ROBOT)
)
(and
(ROW7-ROBOT)
(NOT-ROW8-ROBOT)
(not (NOT-ROW7-ROBOT))
(not (ROW8-ROBOT))
)
)
(when
(and
(ROW7-ROBOT)
)
(and
(ROW6-ROBOT)
(NOT-ROW7-ROBOT)
(not (NOT-ROW6-ROBOT))
(not (ROW7-ROBOT))
)
)
(when
(and
(ROW6-ROBOT)
)
(and
(ROW5-ROBOT)
(NOT-ROW6-ROBOT)
(not (NOT-ROW5-ROBOT))
(not (ROW6-ROBOT))
)
)
(when
(and
(ROW5-ROBOT)
)
(and
(ROW4-ROBOT)
(NOT-ROW5-ROBOT)
(not (NOT-ROW4-ROBOT))
(not (ROW5-ROBOT))
)
)
(when
(and
(ROW4-ROBOT)
)
(and
(ROW3-ROBOT)
(NOT-ROW4-ROBOT)
(not (NOT-ROW3-ROBOT))
(not (ROW4-ROBOT))
)
)
(when
(and
(ROW3-ROBOT)
)
(and
(ROW2-ROBOT)
(NOT-ROW3-ROBOT)
(not (NOT-ROW2-ROBOT))
(not (ROW3-ROBOT))
)
)
(when
(and
(ROW2-ROBOT)
)
(and
(ROW1-ROBOT)
(NOT-ROW2-ROBOT)
(not (NOT-ROW1-ROBOT))
(not (ROW2-ROBOT))
)
)
(when
(and
(ROW1-ROBOT)
)
(and
(ROW0-ROBOT)
(NOT-ROW1-ROBOT)
(not (NOT-ROW0-ROBOT))
(not (ROW1-ROBOT))
)
)
(when
(and
(ABOVEOF13-ROBOT)
)
(and
(ABOVEOF12-ROBOT)
(NOT-ABOVEOF13-ROBOT)
(ABOVEOF11-ROBOT)
(NOT-ABOVEOF12-ROBOT)
(ABOVEOF10-ROBOT)
(NOT-ABOVEOF11-ROBOT)
(ABOVEOF9-ROBOT)
(NOT-ABOVEOF10-ROBOT)
(ABOVEOF8-ROBOT)
(NOT-ABOVEOF9-ROBOT)
(ABOVEOF7-ROBOT)
(NOT-ABOVEOF8-ROBOT)
(ABOVEOF6-ROBOT)
(NOT-ABOVEOF7-ROBOT)
(ABOVEOF5-ROBOT)
(NOT-ABOVEOF6-ROBOT)
(ABOVEOF4-ROBOT)
(NOT-ABOVEOF5-ROBOT)
(ABOVEOF3-ROBOT)
(NOT-ABOVEOF4-ROBOT)
(ABOVEOF2-ROBOT)
(NOT-ABOVEOF3-ROBOT)
(ABOVEOF1-ROBOT)
(NOT-ABOVEOF2-ROBOT)
(NOT-ABOVEOF1-ROBOT)
(not (ABOVEOF13-ROBOT))
)
)
(when
(and
(ABOVEOF12-ROBOT)
)
(and
(ABOVEOF11-ROBOT)
(NOT-ABOVEOF12-ROBOT)
(ABOVEOF10-ROBOT)
(NOT-ABOVEOF11-ROBOT)
(ABOVEOF9-ROBOT)
(NOT-ABOVEOF10-ROBOT)
(ABOVEOF8-ROBOT)
(NOT-ABOVEOF9-ROBOT)
(ABOVEOF7-ROBOT)
(NOT-ABOVEOF8-ROBOT)
(ABOVEOF6-ROBOT)
(NOT-ABOVEOF7-ROBOT)
(ABOVEOF5-ROBOT)
(NOT-ABOVEOF6-ROBOT)
(ABOVEOF4-ROBOT)
(NOT-ABOVEOF5-ROBOT)
(ABOVEOF3-ROBOT)
(NOT-ABOVEOF4-ROBOT)
(ABOVEOF2-ROBOT)
(NOT-ABOVEOF3-ROBOT)
(ABOVEOF1-ROBOT)
(NOT-ABOVEOF2-ROBOT)
(NOT-ABOVEOF1-ROBOT)
(not (ABOVEOF12-ROBOT))
)
)
(when
(and
(ABOVEOF11-ROBOT)
)
(and
(ABOVEOF10-ROBOT)
(NOT-ABOVEOF11-ROBOT)
(ABOVEOF9-ROBOT)
(NOT-ABOVEOF10-ROBOT)
(ABOVEOF8-ROBOT)
(NOT-ABOVEOF9-ROBOT)
(ABOVEOF7-ROBOT)
(NOT-ABOVEOF8-ROBOT)
(ABOVEOF6-ROBOT)
(NOT-ABOVEOF7-ROBOT)
(ABOVEOF5-ROBOT)
(NOT-ABOVEOF6-ROBOT)
(ABOVEOF4-ROBOT)
(NOT-ABOVEOF5-ROBOT)
(ABOVEOF3-ROBOT)
(NOT-ABOVEOF4-ROBOT)
(ABOVEOF2-ROBOT)
(NOT-ABOVEOF3-ROBOT)
(ABOVEOF1-ROBOT)
(NOT-ABOVEOF2-ROBOT)
(NOT-ABOVEOF1-ROBOT)
(not (ABOVEOF11-ROBOT))
)
)
(when
(and
(ABOVEOF10-ROBOT)
)
(and
(ABOVEOF9-ROBOT)
(NOT-ABOVEOF10-ROBOT)
(ABOVEOF8-ROBOT)
(NOT-ABOVEOF9-ROBOT)
(ABOVEOF7-ROBOT)
(NOT-ABOVEOF8-ROBOT)
(ABOVEOF6-ROBOT)
(NOT-ABOVEOF7-ROBOT)
(ABOVEOF5-ROBOT)
(NOT-ABOVEOF6-ROBOT)
(ABOVEOF4-ROBOT)
(NOT-ABOVEOF5-ROBOT)
(ABOVEOF3-ROBOT)
(NOT-ABOVEOF4-ROBOT)
(ABOVEOF2-ROBOT)
(NOT-ABOVEOF3-ROBOT)
(ABOVEOF1-ROBOT)
(NOT-ABOVEOF2-ROBOT)
(NOT-ABOVEOF1-ROBOT)
(not (ABOVEOF10-ROBOT))
)
)
(when
(and
(ABOVEOF9-ROBOT)
)
(and
(ABOVEOF8-ROBOT)
(NOT-ABOVEOF9-ROBOT)
(ABOVEOF7-ROBOT)
(NOT-ABOVEOF8-ROBOT)
(ABOVEOF6-ROBOT)
(NOT-ABOVEOF7-ROBOT)
(ABOVEOF5-ROBOT)
(NOT-ABOVEOF6-ROBOT)
(ABOVEOF4-ROBOT)
(NOT-ABOVEOF5-ROBOT)
(ABOVEOF3-ROBOT)
(NOT-ABOVEOF4-ROBOT)
(ABOVEOF2-ROBOT)
(NOT-ABOVEOF3-ROBOT)
(ABOVEOF1-ROBOT)
(NOT-ABOVEOF2-ROBOT)
(NOT-ABOVEOF1-ROBOT)
(not (ABOVEOF9-ROBOT))
)
)
(when
(and
(ABOVEOF8-ROBOT)
)
(and
(ABOVEOF7-ROBOT)
(NOT-ABOVEOF8-ROBOT)
(ABOVEOF6-ROBOT)
(NOT-ABOVEOF7-ROBOT)
(ABOVEOF5-ROBOT)
(NOT-ABOVEOF6-ROBOT)
(ABOVEOF4-ROBOT)
(NOT-ABOVEOF5-ROBOT)
(ABOVEOF3-ROBOT)
(NOT-ABOVEOF4-ROBOT)
(ABOVEOF2-ROBOT)
(NOT-ABOVEOF3-ROBOT)
(ABOVEOF1-ROBOT)
(NOT-ABOVEOF2-ROBOT)
(NOT-ABOVEOF1-ROBOT)
(not (ABOVEOF8-ROBOT))
)
)
(when
(and
(ABOVEOF7-ROBOT)
)
(and
(ABOVEOF6-ROBOT)
(NOT-ABOVEOF7-ROBOT)
(ABOVEOF5-ROBOT)
(NOT-ABOVEOF6-ROBOT)
(ABOVEOF4-ROBOT)
(NOT-ABOVEOF5-ROBOT)
(ABOVEOF3-ROBOT)
(NOT-ABOVEOF4-ROBOT)
(ABOVEOF2-ROBOT)
(NOT-ABOVEOF3-ROBOT)
(ABOVEOF1-ROBOT)
(NOT-ABOVEOF2-ROBOT)
(NOT-ABOVEOF1-ROBOT)
(not (ABOVEOF7-ROBOT))
)
)
(when
(and
(ABOVEOF6-ROBOT)
)
(and
(ABOVEOF5-ROBOT)
(NOT-ABOVEOF6-ROBOT)
(ABOVEOF4-ROBOT)
(NOT-ABOVEOF5-ROBOT)
(ABOVEOF3-ROBOT)
(NOT-ABOVEOF4-ROBOT)
(ABOVEOF2-ROBOT)
(NOT-ABOVEOF3-ROBOT)
(ABOVEOF1-ROBOT)
(NOT-ABOVEOF2-ROBOT)
(NOT-ABOVEOF1-ROBOT)
(not (ABOVEOF6-ROBOT))
)
)
(when
(and
(ABOVEOF5-ROBOT)
)
(and
(ABOVEOF4-ROBOT)
(NOT-ABOVEOF5-ROBOT)
(ABOVEOF3-ROBOT)
(NOT-ABOVEOF4-ROBOT)
(ABOVEOF2-ROBOT)
(NOT-ABOVEOF3-ROBOT)
(ABOVEOF1-ROBOT)
(NOT-ABOVEOF2-ROBOT)
(NOT-ABOVEOF1-ROBOT)
(not (ABOVEOF5-ROBOT))
)
)
(when
(and
(ABOVEOF4-ROBOT)
)
(and
(ABOVEOF3-ROBOT)
(NOT-ABOVEOF4-ROBOT)
(ABOVEOF2-ROBOT)
(NOT-ABOVEOF3-ROBOT)
(ABOVEOF1-ROBOT)
(NOT-ABOVEOF2-ROBOT)
(NOT-ABOVEOF1-ROBOT)
(not (ABOVEOF4-ROBOT))
)
)
(when
(and
(ABOVEOF3-ROBOT)
)
(and
(ABOVEOF2-ROBOT)
(NOT-ABOVEOF3-ROBOT)
(ABOVEOF1-ROBOT)
(NOT-ABOVEOF2-ROBOT)
(NOT-ABOVEOF1-ROBOT)
(not (ABOVEOF3-ROBOT))
)
)
(when
(and
(ABOVEOF2-ROBOT)
)
(and
(ABOVEOF1-ROBOT)
(NOT-ABOVEOF2-ROBOT)
(NOT-ABOVEOF1-ROBOT)
(not (ABOVEOF2-ROBOT))
)
)
(when
(and
(ABOVEOF1-ROBOT)
)
(and
(NOT-ABOVEOF1-ROBOT)
(not (ABOVEOF1-ROBOT))
)
)
(when
(and
(Flag157-)
)
(and
(BELOWOF13-ROBOT)
(not (NOT-BELOWOF13-ROBOT))
)
)
(when
(and
(BELOWOF8-ROBOT)
)
(and
(BELOWOF12-ROBOT)
(BELOWOF11-ROBOT)
(BELOWOF10-ROBOT)
(BELOWOF9-ROBOT)
(BELOWOF7-ROBOT)
(not (NOT-BELOWOF12-ROBOT))
(not (NOT-BELOWOF11-ROBOT))
(not (NOT-BELOWOF10-ROBOT))
(not (NOT-BELOWOF9-ROBOT))
(not (NOT-BELOWOF8-ROBOT))
(not (NOT-BELOWOF7-ROBOT))
)
)
(when
(and
(BELOWOF13-ROBOT)
)
(and
(BELOWOF12-ROBOT)
(not (NOT-BELOWOF12-ROBOT))
)
)
(when
(and
(BELOWOF3-ROBOT)
)
(and
(BELOWOF12-ROBOT)
(BELOWOF11-ROBOT)
(BELOWOF10-ROBOT)
(BELOWOF9-ROBOT)
(BELOWOF8-ROBOT)
(BELOWOF7-ROBOT)
(BELOWOF6-ROBOT)
(BELOWOF5-ROBOT)
(BELOWOF4-ROBOT)
(BELOWOF2-ROBOT)
(not (NOT-BELOWOF12-ROBOT))
(not (NOT-BELOWOF11-ROBOT))
(not (NOT-BELOWOF10-ROBOT))
(not (NOT-BELOWOF9-ROBOT))
(not (NOT-BELOWOF8-ROBOT))
(not (NOT-BELOWOF7-ROBOT))
(not (NOT-BELOWOF6-ROBOT))
(not (NOT-BELOWOF5-ROBOT))
(not (NOT-BELOWOF4-ROBOT))
(not (NOT-BELOWOF3-ROBOT))
(not (NOT-BELOWOF2-ROBOT))
)
)
(when
(and
(BELOWOF9-ROBOT)
)
(and
(BELOWOF12-ROBOT)
(BELOWOF11-ROBOT)
(BELOWOF10-ROBOT)
(BELOWOF8-ROBOT)
(not (NOT-BELOWOF12-ROBOT))
(not (NOT-BELOWOF11-ROBOT))
(not (NOT-BELOWOF10-ROBOT))
(not (NOT-BELOWOF9-ROBOT))
(not (NOT-BELOWOF8-ROBOT))
)
)
(when
(and
(BELOWOF7-ROBOT)
)
(and
(BELOWOF12-ROBOT)
(BELOWOF11-ROBOT)
(BELOWOF10-ROBOT)
(BELOWOF9-ROBOT)
(BELOWOF8-ROBOT)
(BELOWOF6-ROBOT)
(not (NOT-BELOWOF12-ROBOT))
(not (NOT-BELOWOF11-ROBOT))
(not (NOT-BELOWOF10-ROBOT))
(not (NOT-BELOWOF9-ROBOT))
(not (NOT-BELOWOF8-ROBOT))
(not (NOT-BELOWOF7-ROBOT))
(not (NOT-BELOWOF6-ROBOT))
)
)
(when
(and
(BELOWOF12-ROBOT)
)
(and
(BELOWOF11-ROBOT)
(not (NOT-BELOWOF12-ROBOT))
(not (NOT-BELOWOF11-ROBOT))
)
)
(when
(and
(BELOWOF4-ROBOT)
)
(and
(BELOWOF12-ROBOT)
(BELOWOF11-ROBOT)
(BELOWOF10-ROBOT)
(BELOWOF9-ROBOT)
(BELOWOF8-ROBOT)
(BELOWOF7-ROBOT)
(BELOWOF6-ROBOT)
(BELOWOF5-ROBOT)
(BELOWOF3-ROBOT)
(not (NOT-BELOWOF12-ROBOT))
(not (NOT-BELOWOF11-ROBOT))
(not (NOT-BELOWOF10-ROBOT))
(not (NOT-BELOWOF9-ROBOT))
(not (NOT-BELOWOF8-ROBOT))
(not (NOT-BELOWOF7-ROBOT))
(not (NOT-BELOWOF6-ROBOT))
(not (NOT-BELOWOF5-ROBOT))
(not (NOT-BELOWOF4-ROBOT))
(not (NOT-BELOWOF3-ROBOT))
)
)
(when
(and
(BELOWOF1-ROBOT)
)
(and
(BELOWOF12-ROBOT)
(BELOWOF11-ROBOT)
(BELOWOF10-ROBOT)
(BELOWOF9-ROBOT)
(BELOWOF8-ROBOT)
(BELOWOF7-ROBOT)
(BELOWOF6-ROBOT)
(BELOWOF5-ROBOT)
(BELOWOF4-ROBOT)
(BELOWOF3-ROBOT)
(BELOWOF2-ROBOT)
(not (NOT-BELOWOF12-ROBOT))
(not (NOT-BELOWOF11-ROBOT))
(not (NOT-BELOWOF10-ROBOT))
(not (NOT-BELOWOF9-ROBOT))
(not (NOT-BELOWOF8-ROBOT))
(not (NOT-BELOWOF7-ROBOT))
(not (NOT-BELOWOF6-ROBOT))
(not (NOT-BELOWOF5-ROBOT))
(not (NOT-BELOWOF4-ROBOT))
(not (NOT-BELOWOF3-ROBOT))
(not (NOT-BELOWOF2-ROBOT))
(not (NOT-BELOWOF1-ROBOT))
)
)
(when
(and
(BELOWOF2-ROBOT)
)
(and
(BELOWOF12-ROBOT)
(BELOWOF11-ROBOT)
(BELOWOF10-ROBOT)
(BELOWOF9-ROBOT)
(BELOWOF8-ROBOT)
(BELOWOF7-ROBOT)
(BELOWOF6-ROBOT)
(BELOWOF5-ROBOT)
(BELOWOF4-ROBOT)
(BELOWOF3-ROBOT)
(BELOWOF1-ROBOT)
(not (NOT-BELOWOF12-ROBOT))
(not (NOT-BELOWOF11-ROBOT))
(not (NOT-BELOWOF10-ROBOT))
(not (NOT-BELOWOF9-ROBOT))
(not (NOT-BELOWOF8-ROBOT))
(not (NOT-BELOWOF7-ROBOT))
(not (NOT-BELOWOF6-ROBOT))
(not (NOT-BELOWOF5-ROBOT))
(not (NOT-BELOWOF4-ROBOT))
(not (NOT-BELOWOF3-ROBOT))
(not (NOT-BELOWOF2-ROBOT))
(not (NOT-BELOWOF1-ROBOT))
)
)
(when
(and
(BELOWOF5-ROBOT)
)
(and
(BELOWOF12-ROBOT)
(BELOWOF11-ROBOT)
(BELOWOF10-ROBOT)
(BELOWOF9-ROBOT)
(BELOWOF8-ROBOT)
(BELOWOF7-ROBOT)
(BELOWOF6-ROBOT)
(BELOWOF4-ROBOT)
(not (NOT-BELOWOF12-ROBOT))
(not (NOT-BELOWOF11-ROBOT))
(not (NOT-BELOWOF10-ROBOT))
(not (NOT-BELOWOF9-ROBOT))
(not (NOT-BELOWOF8-ROBOT))
(not (NOT-BELOWOF7-ROBOT))
(not (NOT-BELOWOF6-ROBOT))
(not (NOT-BELOWOF5-ROBOT))
(not (NOT-BELOWOF4-ROBOT))
)
)
(when
(and
(BELOWOF6-ROBOT)
)
(and
(BELOWOF12-ROBOT)
(BELOWOF11-ROBOT)
(BELOWOF10-ROBOT)
(BELOWOF9-ROBOT)
(BELOWOF8-ROBOT)
(BELOWOF7-ROBOT)
(BELOWOF5-ROBOT)
(not (NOT-BELOWOF12-ROBOT))
(not (NOT-BELOWOF11-ROBOT))
(not (NOT-BELOWOF10-ROBOT))
(not (NOT-BELOWOF9-ROBOT))
(not (NOT-BELOWOF8-ROBOT))
(not (NOT-BELOWOF7-ROBOT))
(not (NOT-BELOWOF6-ROBOT))
(not (NOT-BELOWOF5-ROBOT))
)
)
(when
(and
(BELOWOF11-ROBOT)
)
(and
(BELOWOF12-ROBOT)
(BELOWOF10-ROBOT)
(not (NOT-BELOWOF12-ROBOT))
(not (NOT-BELOWOF11-ROBOT))
(not (NOT-BELOWOF10-ROBOT))
)
)
(when
(and
(BELOWOF10-ROBOT)
)
(and
(BELOWOF12-ROBOT)
(BELOWOF11-ROBOT)
(BELOWOF9-ROBOT)
(not (NOT-BELOWOF12-ROBOT))
(not (NOT-BELOWOF11-ROBOT))
(not (NOT-BELOWOF10-ROBOT))
(not (NOT-BELOWOF9-ROBOT))
)
)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag312prime-))
(not (Flag185prime-))
(not (Flag163prime-))
(not (Flag162prime-))
(not (Flag161prime-))
(not (Flag157prime-))
(not (Flag28prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag159prime-))
(not (Flag158prime-))
(not (Flag155prime-))
(not (Flag154prime-))
(not (Flag153prime-))
(not (Flag151prime-))
(not (Flag150prime-))
(not (Flag149prime-))
(not (Flag147prime-))
(not (Flag146prime-))
(not (Flag145prime-))
(not (Flag144prime-))
(not (Flag142prime-))
(not (Flag141prime-))
(not (Flag140prime-))
(not (Flag139prime-))
(not (Flag138prime-))
(not (Flag137prime-))
(not (Flag135prime-))
(not (Flag134prime-))
(not (Flag133prime-))
(not (Flag132prime-))
(not (Flag131prime-))
(not (Flag130prime-))
(not (Flag129prime-))
(not (Flag128prime-))
(not (Flag126prime-))
(not (Flag125prime-))
(not (Flag124prime-))
(not (Flag123prime-))
(not (Flag122prime-))
(not (Flag121prime-))
(not (Flag120prime-))
(not (Flag119prime-))
(not (Flag117prime-))
(not (Flag116prime-))
(not (Flag115prime-))
(not (Flag114prime-))
(not (Flag113prime-))
(not (Flag112prime-))
(not (Flag111prime-))
(not (Flag110prime-))
(not (Flag109prime-))
(not (Flag108prime-))
(not (Flag106prime-))
(not (Flag105prime-))
(not (Flag104prime-))
(not (Flag103prime-))
(not (Flag102prime-))
(not (Flag101prime-))
(not (Flag100prime-))
(not (Flag99prime-))
(not (Flag98prime-))
(not (Flag97prime-))
(not (Flag96prime-))
(not (Flag94prime-))
(not (Flag93prime-))
(not (Flag92prime-))
(not (Flag91prime-))
(not (Flag90prime-))
(not (Flag89prime-))
(not (Flag88prime-))
(not (Flag87prime-))
(not (Flag86prime-))
(not (Flag85prime-))
(not (Flag83prime-))
(not (Flag82prime-))
(not (Flag81prime-))
(not (Flag80prime-))
(not (Flag79prime-))
(not (Flag78prime-))
(not (Flag77prime-))
(not (Flag76prime-))
(not (Flag75prime-))
(not (Flag74prime-))
(not (Flag73prime-))
(not (Flag72prime-))
(not (Flag71prime-))
(not (Flag69prime-))
(not (Flag68prime-))
(not (Flag67prime-))
(not (Flag66prime-))
(not (Flag65prime-))
(not (Flag64prime-))
(not (Flag63prime-))
(not (Flag62prime-))
(not (Flag61prime-))
(not (Flag60prime-))
(not (Flag59prime-))
(not (Flag58prime-))
(not (Flag57prime-))
(not (Flag55prime-))
(not (Flag54prime-))
(not (Flag53prime-))
(not (Flag52prime-))
(not (Flag51prime-))
(not (Flag50prime-))
(not (Flag49prime-))
(not (Flag48prime-))
(not (Flag47prime-))
(not (Flag46prime-))
(not (Flag45prime-))
(not (Flag44prime-))
(not (Flag43prime-))
(not (Flag42prime-))
(not (Flag41prime-))
(not (Flag40prime-))
(not (Flag39prime-))
(not (Flag38prime-))
(not (Flag37prime-))
(not (Flag36prime-))
(not (Flag35prime-))
(not (Flag34prime-))
(not (Flag33prime-))
(not (Flag32prime-))
(not (Flag31prime-))
(not (Flag30prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag26prime-))
(not (Flag25prime-))
(not (Flag24prime-))
(not (Flag23prime-))
(not (Flag22prime-))
(not (Flag21prime-))
(not (Flag20prime-))
(not (Flag19prime-))
(not (Flag18prime-))
(not (Flag17prime-))
(not (Flag16prime-))
(not (Flag15prime-))
(not (Flag14prime-))
(not (Flag13prime-))
(not (Flag12prime-))
(not (Flag11prime-))
(not (Flag10prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag313prime-))
(not (Flag310prime-))
(not (Flag309prime-))
(not (Flag308prime-))
(not (Flag306prime-))
(not (Flag305prime-))
(not (Flag304prime-))
(not (Flag302prime-))
(not (Flag301prime-))
(not (Flag300prime-))
(not (Flag299prime-))
(not (Flag297prime-))
(not (Flag296prime-))
(not (Flag295prime-))
(not (Flag294prime-))
(not (Flag293prime-))
(not (Flag291prime-))
(not (Flag290prime-))
(not (Flag289prime-))
(not (Flag288prime-))
(not (Flag287prime-))
(not (Flag286prime-))
(not (Flag284prime-))
(not (Flag283prime-))
(not (Flag282prime-))
(not (Flag281prime-))
(not (Flag280prime-))
(not (Flag279prime-))
(not (Flag278prime-))
(not (Flag277prime-))
(not (Flag275prime-))
(not (Flag274prime-))
(not (Flag273prime-))
(not (Flag272prime-))
(not (Flag271prime-))
(not (Flag270prime-))
(not (Flag269prime-))
(not (Flag268prime-))
(not (Flag267prime-))
(not (Flag266prime-))
(not (Flag264prime-))
(not (Flag263prime-))
(not (Flag262prime-))
(not (Flag261prime-))
(not (Flag260prime-))
(not (Flag259prime-))
(not (Flag258prime-))
(not (Flag257prime-))
(not (Flag256prime-))
(not (Flag254prime-))
(not (Flag253prime-))
(not (Flag252prime-))
(not (Flag251prime-))
(not (Flag250prime-))
(not (Flag249prime-))
(not (Flag248prime-))
(not (Flag247prime-))
(not (Flag246prime-))
(not (Flag245prime-))
(not (Flag244prime-))
(not (Flag243prime-))
(not (Flag241prime-))
(not (Flag240prime-))
(not (Flag239prime-))
(not (Flag238prime-))
(not (Flag237prime-))
(not (Flag236prime-))
(not (Flag235prime-))
(not (Flag234prime-))
(not (Flag233prime-))
(not (Flag232prime-))
(not (Flag231prime-))
(not (Flag230prime-))
(not (Flag229prime-))
(not (Flag227prime-))
(not (Flag226prime-))
(not (Flag225prime-))
(not (Flag224prime-))
(not (Flag223prime-))
(not (Flag222prime-))
(not (Flag221prime-))
(not (Flag220prime-))
(not (Flag219prime-))
(not (Flag218prime-))
(not (Flag217prime-))
(not (Flag216prime-))
(not (Flag214prime-))
(not (Flag213prime-))
(not (Flag212prime-))
(not (Flag211prime-))
(not (Flag210prime-))
(not (Flag209prime-))
(not (Flag208prime-))
(not (Flag207prime-))
(not (Flag206prime-))
(not (Flag205prime-))
(not (Flag204prime-))
(not (Flag203prime-))
(not (Flag202prime-))
(not (Flag201prime-))
(not (Flag199prime-))
(not (Flag198prime-))
(not (Flag197prime-))
(not (Flag196prime-))
(not (Flag195prime-))
(not (Flag194prime-))
(not (Flag193prime-))
(not (Flag192prime-))
(not (Flag191prime-))
(not (Flag190prime-))
(not (Flag189prime-))
(not (Flag188prime-))
(not (Flag187prime-))
(not (Flag186prime-))
(not (Flag184prime-))
(not (Flag183prime-))
(not (Flag182prime-))
(not (Flag181prime-))
(not (Flag180prime-))
(not (Flag179prime-))
(not (Flag178prime-))
(not (Flag177prime-))
(not (Flag176prime-))
(not (Flag175prime-))
(not (Flag174prime-))
(not (Flag173prime-))
(not (Flag172prime-))
(not (Flag171prime-))
(not (Flag170prime-))
(not (Flag169prime-))
(not (Flag168prime-))
(not (Flag167prime-))
(not (Flag166prime-))
(not (Flag165prime-))
(not (Flag164prime-))
(not (Flag160prime-))
(not (Flag156prime-))
(not (Flag152prime-))
(not (Flag148prime-))
(not (Flag143prime-))
(not (Flag136prime-))
(not (Flag127prime-))
(not (Flag118prime-))
(not (Flag107prime-))
(not (Flag95prime-))
(not (Flag84prime-))
(not (Flag70prime-))
(not (Flag56prime-))
(not (Flag1prime-))
(not (Flag314prime-))
(not (Flag311prime-))
(not (Flag307prime-))
(not (Flag303prime-))
(not (Flag298prime-))
(not (Flag292prime-))
(not (Flag285prime-))
(not (Flag276prime-))
(not (Flag265prime-))
(not (Flag255prime-))
(not (Flag242prime-))
(not (Flag228prime-))
(not (Flag215prime-))
(not (Flag200prime-))
)
)
(:action MOVEUP-ROBOT
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(Flag156prime-)
(Flag152prime-)
(Flag148prime-)
(Flag143prime-)
(Flag136prime-)
(Flag127prime-)
(Flag118prime-)
(Flag107prime-)
(Flag95prime-)
(Flag84prime-)
(Flag70prime-)
(Flag56prime-)
(Flag42prime-)
(Flag28prime-)
(Flag27prime-)
(Flag26prime-)
(Flag25prime-)
(Flag24prime-)
(Flag23prime-)
(Flag22prime-)
(Flag21prime-)
(Flag20prime-)
(Flag19prime-)
(Flag18prime-)
(Flag17prime-)
(Flag16prime-)
(Flag15prime-)
(Flag14prime-)
(Flag13prime-)
(Flag12prime-)
(Flag11prime-)
(Flag10prime-)
(Flag9prime-)
(Flag8prime-)
(Flag7prime-)
(Flag6prime-)
(Flag5prime-)
)
:effect
(and
(when
(and
(Flag156-)
)
(and
(ROW13-ROBOT)
(not (NOT-ROW13-ROBOT))
)
)
(when
(and
(Flag152-)
)
(and
(ROW12-ROBOT)
(not (NOT-ROW12-ROBOT))
)
)
(when
(and
(Flag148-)
)
(and
(ROW11-ROBOT)
(not (NOT-ROW11-ROBOT))
)
)
(when
(and
(Flag143-)
)
(and
(ROW10-ROBOT)
(not (NOT-ROW10-ROBOT))
)
)
(when
(and
(Flag136-)
)
(and
(ROW9-ROBOT)
(not (NOT-ROW9-ROBOT))
)
)
(when
(and
(Flag127-)
)
(and
(ROW8-ROBOT)
(not (NOT-ROW8-ROBOT))
)
)
(when
(and
(Flag118-)
)
(and
(ROW7-ROBOT)
(not (NOT-ROW7-ROBOT))
)
)
(when
(and
(Flag107-)
)
(and
(ROW6-ROBOT)
(not (NOT-ROW6-ROBOT))
)
)
(when
(and
(Flag95-)
)
(and
(ROW5-ROBOT)
(not (NOT-ROW5-ROBOT))
)
)
(when
(and
(Flag84-)
)
(and
(ROW4-ROBOT)
(not (NOT-ROW4-ROBOT))
)
)
(when
(and
(Flag70-)
)
(and
(ROW3-ROBOT)
(not (NOT-ROW3-ROBOT))
)
)
(when
(and
(Flag56-)
)
(and
(ROW2-ROBOT)
(not (NOT-ROW2-ROBOT))
)
)
(when
(and
(Flag42-)
)
(and
(ROW1-ROBOT)
(not (NOT-ROW1-ROBOT))
)
)
(when
(and
(ROW12-ROBOT)
)
(and
(ROW13-ROBOT)
(NOT-ROW12-ROBOT)
(not (NOT-ROW13-ROBOT))
(not (ROW12-ROBOT))
)
)
(when
(and
(ROW11-ROBOT)
)
(and
(ROW12-ROBOT)
(NOT-ROW11-ROBOT)
(not (NOT-ROW12-ROBOT))
(not (ROW11-ROBOT))
)
)
(when
(and
(ROW10-ROBOT)
)
(and
(ROW11-ROBOT)
(NOT-ROW10-ROBOT)
(not (NOT-ROW11-ROBOT))
(not (ROW10-ROBOT))
)
)
(when
(and
(ROW9-ROBOT)
)
(and
(ROW10-ROBOT)
(NOT-ROW9-ROBOT)
(not (NOT-ROW10-ROBOT))
(not (ROW9-ROBOT))
)
)
(when
(and
(ROW8-ROBOT)
)
(and
(ROW9-ROBOT)
(NOT-ROW8-ROBOT)
(not (NOT-ROW9-ROBOT))
(not (ROW8-ROBOT))
)
)
(when
(and
(ROW7-ROBOT)
)
(and
(ROW8-ROBOT)
(NOT-ROW7-ROBOT)
(not (NOT-ROW8-ROBOT))
(not (ROW7-ROBOT))
)
)
(when
(and
(ROW6-ROBOT)
)
(and
(ROW7-ROBOT)
(NOT-ROW6-ROBOT)
(not (NOT-ROW7-ROBOT))
(not (ROW6-ROBOT))
)
)
(when
(and
(ROW5-ROBOT)
)
(and
(ROW6-ROBOT)
(NOT-ROW5-ROBOT)
(not (NOT-ROW6-ROBOT))
(not (ROW5-ROBOT))
)
)
(when
(and
(ROW4-ROBOT)
)
(and
(ROW5-ROBOT)
(NOT-ROW4-ROBOT)
(not (NOT-ROW5-ROBOT))
(not (ROW4-ROBOT))
)
)
(when
(and
(ROW3-ROBOT)
)
(and
(ROW4-ROBOT)
(NOT-ROW3-ROBOT)
(not (NOT-ROW4-ROBOT))
(not (ROW3-ROBOT))
)
)
(when
(and
(ROW2-ROBOT)
)
(and
(ROW3-ROBOT)
(NOT-ROW2-ROBOT)
(not (NOT-ROW3-ROBOT))
(not (ROW2-ROBOT))
)
)
(when
(and
(ROW1-ROBOT)
)
(and
(ROW2-ROBOT)
(NOT-ROW1-ROBOT)
(not (NOT-ROW2-ROBOT))
(not (ROW1-ROBOT))
)
)
(when
(and
(ROW0-ROBOT)
)
(and
(ROW1-ROBOT)
(NOT-ROW0-ROBOT)
(not (NOT-ROW1-ROBOT))
(not (ROW0-ROBOT))
)
)
(when
(and
(Flag28-)
)
(and
(BELOWOF14-ROBOT)
(NOT-BELOWOF13-ROBOT)
(not (NOT-BELOWOF14-ROBOT))
(not (BELOWOF13-ROBOT))
)
)
(when
(and
(Flag27-)
)
(and
(BELOWOF13-ROBOT)
(NOT-BELOWOF12-ROBOT)
(not (NOT-BELOWOF13-ROBOT))
(not (BELOWOF12-ROBOT))
)
)
(when
(and
(Flag26-)
)
(and
(BELOWOF12-ROBOT)
(NOT-BELOWOF11-ROBOT)
(not (NOT-BELOWOF12-ROBOT))
(not (BELOWOF11-ROBOT))
)
)
(when
(and
(Flag25-)
)
(and
(BELOWOF11-ROBOT)
(NOT-BELOWOF10-ROBOT)
(not (NOT-BELOWOF11-ROBOT))
(not (BELOWOF10-ROBOT))
)
)
(when
(and
(Flag24-)
)
(and
(BELOWOF10-ROBOT)
(NOT-BELOWOF9-ROBOT)
(not (NOT-BELOWOF10-ROBOT))
(not (BELOWOF9-ROBOT))
)
)
(when
(and
(Flag23-)
)
(and
(BELOWOF9-ROBOT)
(NOT-BELOWOF8-ROBOT)
(not (NOT-BELOWOF9-ROBOT))
(not (BELOWOF8-ROBOT))
)
)
(when
(and
(Flag22-)
)
(and
(BELOWOF8-ROBOT)
(NOT-BELOWOF7-ROBOT)
(not (NOT-BELOWOF8-ROBOT))
(not (BELOWOF7-ROBOT))
)
)
(when
(and
(Flag21-)
)
(and
(BELOWOF7-ROBOT)
(NOT-BELOWOF6-ROBOT)
(not (NOT-BELOWOF7-ROBOT))
(not (BELOWOF6-ROBOT))
)
)
(when
(and
(Flag20-)
)
(and
(BELOWOF6-ROBOT)
(NOT-BELOWOF5-ROBOT)
(not (NOT-BELOWOF6-ROBOT))
(not (BELOWOF5-ROBOT))
)
)
(when
(and
(Flag19-)
)
(and
(BELOWOF5-ROBOT)
(NOT-BELOWOF4-ROBOT)
(not (NOT-BELOWOF5-ROBOT))
(not (BELOWOF4-ROBOT))
)
)
(when
(and
(Flag18-)
)
(and
(BELOWOF4-ROBOT)
(NOT-BELOWOF3-ROBOT)
(not (NOT-BELOWOF4-ROBOT))
(not (BELOWOF3-ROBOT))
)
)
(when
(and
(Flag17-)
)
(and
(BELOWOF3-ROBOT)
(NOT-BELOWOF2-ROBOT)
(not (NOT-BELOWOF3-ROBOT))
(not (BELOWOF2-ROBOT))
)
)
(when
(and
(BELOWOF1-ROBOT)
)
(and
(BELOWOF2-ROBOT)
(NOT-BELOWOF1-ROBOT)
(not (NOT-BELOWOF2-ROBOT))
(not (BELOWOF1-ROBOT))
)
)
(when
(and
(Flag16-)
)
(and
(ABOVEOF13-ROBOT)
(not (NOT-ABOVEOF13-ROBOT))
)
)
(when
(and
(Flag15-)
)
(and
(ABOVEOF12-ROBOT)
(not (NOT-ABOVEOF12-ROBOT))
)
)
(when
(and
(Flag14-)
)
(and
(ABOVEOF11-ROBOT)
(not (NOT-ABOVEOF11-ROBOT))
)
)
(when
(and
(Flag13-)
)
(and
(ABOVEOF10-ROBOT)
(not (NOT-ABOVEOF10-ROBOT))
)
)
(when
(and
(Flag12-)
)
(and
(ABOVEOF9-ROBOT)
(not (NOT-ABOVEOF9-ROBOT))
)
)
(when
(and
(Flag11-)
)
(and
(ABOVEOF8-ROBOT)
(not (NOT-ABOVEOF8-ROBOT))
)
)
(when
(and
(Flag10-)
)
(and
(ABOVEOF7-ROBOT)
(not (NOT-ABOVEOF7-ROBOT))
)
)
(when
(and
(Flag9-)
)
(and
(ABOVEOF6-ROBOT)
(not (NOT-ABOVEOF6-ROBOT))
)
)
(when
(and
(Flag8-)
)
(and
(ABOVEOF5-ROBOT)
(not (NOT-ABOVEOF5-ROBOT))
)
)
(when
(and
(Flag7-)
)
(and
(ABOVEOF4-ROBOT)
(not (NOT-ABOVEOF4-ROBOT))
)
)
(when
(and
(Flag6-)
)
(and
(ABOVEOF3-ROBOT)
(not (NOT-ABOVEOF3-ROBOT))
)
)
(when
(and
(Flag5-)
)
(and
(ABOVEOF2-ROBOT)
(not (NOT-ABOVEOF2-ROBOT))
)
)
(ABOVEOF1-ROBOT)
(CHECKCONSISTENCY-)
(not (NOT-ABOVEOF1-ROBOT))
(not (NOT-CHECKCONSISTENCY-))
(not (Flag312prime-))
(not (Flag185prime-))
(not (Flag163prime-))
(not (Flag162prime-))
(not (Flag161prime-))
(not (Flag157prime-))
(not (Flag28prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag159prime-))
(not (Flag158prime-))
(not (Flag155prime-))
(not (Flag154prime-))
(not (Flag153prime-))
(not (Flag151prime-))
(not (Flag150prime-))
(not (Flag149prime-))
(not (Flag147prime-))
(not (Flag146prime-))
(not (Flag145prime-))
(not (Flag144prime-))
(not (Flag142prime-))
(not (Flag141prime-))
(not (Flag140prime-))
(not (Flag139prime-))
(not (Flag138prime-))
(not (Flag137prime-))
(not (Flag135prime-))
(not (Flag134prime-))
(not (Flag133prime-))
(not (Flag132prime-))
(not (Flag131prime-))
(not (Flag130prime-))
(not (Flag129prime-))
(not (Flag128prime-))
(not (Flag126prime-))
(not (Flag125prime-))
(not (Flag124prime-))
(not (Flag123prime-))
(not (Flag122prime-))
(not (Flag121prime-))
(not (Flag120prime-))
(not (Flag119prime-))
(not (Flag117prime-))
(not (Flag116prime-))
(not (Flag115prime-))
(not (Flag114prime-))
(not (Flag113prime-))
(not (Flag112prime-))
(not (Flag111prime-))
(not (Flag110prime-))
(not (Flag109prime-))
(not (Flag108prime-))
(not (Flag106prime-))
(not (Flag105prime-))
(not (Flag104prime-))
(not (Flag103prime-))
(not (Flag102prime-))
(not (Flag101prime-))
(not (Flag100prime-))
(not (Flag99prime-))
(not (Flag98prime-))
(not (Flag97prime-))
(not (Flag96prime-))
(not (Flag94prime-))
(not (Flag93prime-))
(not (Flag92prime-))
(not (Flag91prime-))
(not (Flag90prime-))
(not (Flag89prime-))
(not (Flag88prime-))
(not (Flag87prime-))
(not (Flag86prime-))
(not (Flag85prime-))
(not (Flag83prime-))
(not (Flag82prime-))
(not (Flag81prime-))
(not (Flag80prime-))
(not (Flag79prime-))
(not (Flag78prime-))
(not (Flag77prime-))
(not (Flag76prime-))
(not (Flag75prime-))
(not (Flag74prime-))
(not (Flag73prime-))
(not (Flag72prime-))
(not (Flag71prime-))
(not (Flag69prime-))
(not (Flag68prime-))
(not (Flag67prime-))
(not (Flag66prime-))
(not (Flag65prime-))
(not (Flag64prime-))
(not (Flag63prime-))
(not (Flag62prime-))
(not (Flag61prime-))
(not (Flag60prime-))
(not (Flag59prime-))
(not (Flag58prime-))
(not (Flag57prime-))
(not (Flag55prime-))
(not (Flag54prime-))
(not (Flag53prime-))
(not (Flag52prime-))
(not (Flag51prime-))
(not (Flag50prime-))
(not (Flag49prime-))
(not (Flag48prime-))
(not (Flag47prime-))
(not (Flag46prime-))
(not (Flag45prime-))
(not (Flag44prime-))
(not (Flag43prime-))
(not (Flag42prime-))
(not (Flag41prime-))
(not (Flag40prime-))
(not (Flag39prime-))
(not (Flag38prime-))
(not (Flag37prime-))
(not (Flag36prime-))
(not (Flag35prime-))
(not (Flag34prime-))
(not (Flag33prime-))
(not (Flag32prime-))
(not (Flag31prime-))
(not (Flag30prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag26prime-))
(not (Flag25prime-))
(not (Flag24prime-))
(not (Flag23prime-))
(not (Flag22prime-))
(not (Flag21prime-))
(not (Flag20prime-))
(not (Flag19prime-))
(not (Flag18prime-))
(not (Flag17prime-))
(not (Flag16prime-))
(not (Flag15prime-))
(not (Flag14prime-))
(not (Flag13prime-))
(not (Flag12prime-))
(not (Flag11prime-))
(not (Flag10prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag313prime-))
(not (Flag310prime-))
(not (Flag309prime-))
(not (Flag308prime-))
(not (Flag306prime-))
(not (Flag305prime-))
(not (Flag304prime-))
(not (Flag302prime-))
(not (Flag301prime-))
(not (Flag300prime-))
(not (Flag299prime-))
(not (Flag297prime-))
(not (Flag296prime-))
(not (Flag295prime-))
(not (Flag294prime-))
(not (Flag293prime-))
(not (Flag291prime-))
(not (Flag290prime-))
(not (Flag289prime-))
(not (Flag288prime-))
(not (Flag287prime-))
(not (Flag286prime-))
(not (Flag284prime-))
(not (Flag283prime-))
(not (Flag282prime-))
(not (Flag281prime-))
(not (Flag280prime-))
(not (Flag279prime-))
(not (Flag278prime-))
(not (Flag277prime-))
(not (Flag275prime-))
(not (Flag274prime-))
(not (Flag273prime-))
(not (Flag272prime-))
(not (Flag271prime-))
(not (Flag270prime-))
(not (Flag269prime-))
(not (Flag268prime-))
(not (Flag267prime-))
(not (Flag266prime-))
(not (Flag264prime-))
(not (Flag263prime-))
(not (Flag262prime-))
(not (Flag261prime-))
(not (Flag260prime-))
(not (Flag259prime-))
(not (Flag258prime-))
(not (Flag257prime-))
(not (Flag256prime-))
(not (Flag254prime-))
(not (Flag253prime-))
(not (Flag252prime-))
(not (Flag251prime-))
(not (Flag250prime-))
(not (Flag249prime-))
(not (Flag248prime-))
(not (Flag247prime-))
(not (Flag246prime-))
(not (Flag245prime-))
(not (Flag244prime-))
(not (Flag243prime-))
(not (Flag241prime-))
(not (Flag240prime-))
(not (Flag239prime-))
(not (Flag238prime-))
(not (Flag237prime-))
(not (Flag236prime-))
(not (Flag235prime-))
(not (Flag234prime-))
(not (Flag233prime-))
(not (Flag232prime-))
(not (Flag231prime-))
(not (Flag230prime-))
(not (Flag229prime-))
(not (Flag227prime-))
(not (Flag226prime-))
(not (Flag225prime-))
(not (Flag224prime-))
(not (Flag223prime-))
(not (Flag222prime-))
(not (Flag221prime-))
(not (Flag220prime-))
(not (Flag219prime-))
(not (Flag218prime-))
(not (Flag217prime-))
(not (Flag216prime-))
(not (Flag214prime-))
(not (Flag213prime-))
(not (Flag212prime-))
(not (Flag211prime-))
(not (Flag210prime-))
(not (Flag209prime-))
(not (Flag208prime-))
(not (Flag207prime-))
(not (Flag206prime-))
(not (Flag205prime-))
(not (Flag204prime-))
(not (Flag203prime-))
(not (Flag202prime-))
(not (Flag201prime-))
(not (Flag199prime-))
(not (Flag198prime-))
(not (Flag197prime-))
(not (Flag196prime-))
(not (Flag195prime-))
(not (Flag194prime-))
(not (Flag193prime-))
(not (Flag192prime-))
(not (Flag191prime-))
(not (Flag190prime-))
(not (Flag189prime-))
(not (Flag188prime-))
(not (Flag187prime-))
(not (Flag186prime-))
(not (Flag184prime-))
(not (Flag183prime-))
(not (Flag182prime-))
(not (Flag181prime-))
(not (Flag180prime-))
(not (Flag179prime-))
(not (Flag178prime-))
(not (Flag177prime-))
(not (Flag176prime-))
(not (Flag175prime-))
(not (Flag174prime-))
(not (Flag173prime-))
(not (Flag172prime-))
(not (Flag171prime-))
(not (Flag170prime-))
(not (Flag169prime-))
(not (Flag168prime-))
(not (Flag167prime-))
(not (Flag166prime-))
(not (Flag165prime-))
(not (Flag164prime-))
(not (Flag160prime-))
(not (Flag156prime-))
(not (Flag152prime-))
(not (Flag148prime-))
(not (Flag143prime-))
(not (Flag136prime-))
(not (Flag127prime-))
(not (Flag118prime-))
(not (Flag107prime-))
(not (Flag95prime-))
(not (Flag84prime-))
(not (Flag70prime-))
(not (Flag56prime-))
(not (Flag1prime-))
(not (Flag314prime-))
(not (Flag311prime-))
(not (Flag307prime-))
(not (Flag303prime-))
(not (Flag298prime-))
(not (Flag292prime-))
(not (Flag285prime-))
(not (Flag276prime-))
(not (Flag265prime-))
(not (Flag255prime-))
(not (Flag242prime-))
(not (Flag228prime-))
(not (Flag215prime-))
(not (Flag200prime-))
)
)
(:action Prim1Action-2
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
(Flag1prime-)
(not (Flag1-))
)

)
(:action Prim1Action-3
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
(Flag1prime-)
(not (Flag1-))
)

)
(:action Flag2Action-5
:parameters ()
:precondition
(and
(RIGHTOF2-ROBOT)
)
:effect
(and
(Flag2-)
(Flag2prime-)
)

)
(:action Flag2Action-8
:parameters ()
:precondition
(and
(LEFTOF13-ROBOT)
)
:effect
(and
(Flag2-)
(Flag2prime-)
)

)
(:action Prim3Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
(Flag3prime-)
(not (Flag3-))
)

)
(:action Prim3Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
(Flag3prime-)
(not (Flag3-))
)

)
(:action Flag4Action
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
)
:effect
(and
(Flag4-)
(Flag4prime-)
)

)
(:action Prim4Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
(Flag4prime-)
(not (Flag4-))
)

)
(:action Prim4Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
(Flag4prime-)
(not (Flag4-))
)

)
(:action Flag28Action-11
:parameters ()
:precondition
(and
(BELOWOF13-ROBOT)
)
:effect
(and
(Flag28-)
(Flag28prime-)
)

)
(:action Prim153Action-1
:parameters ()
:precondition
(and
(not (BELOWOF13-ROBOT))
)
:effect
(and
(Flag153prime-)
(not (Flag153-))
)

)
(:action Prim154Action-1
:parameters ()
:precondition
(and
(not (BELOWOF13-ROBOT))
)
:effect
(and
(Flag154prime-)
(not (Flag154-))
)

)
(:action Flag157Action-11
:parameters ()
:precondition
(and
(BELOWOF13-ROBOT)
)
:effect
(and
(Flag157-)
(Flag157prime-)
)

)
(:action Prim159Action-0
:parameters ()
:precondition
(and
(not (BELOWOF13-ROBOT))
)
:effect
(and
(Flag159prime-)
(not (Flag159-))
)

)
(:action Flag161Action-4
:parameters ()
:precondition
(and
(RIGHTOF2-ROBOT)
)
:effect
(and
(Flag161-)
(Flag161prime-)
)

)
(:action Flag162Action-3
:parameters ()
:precondition
(and
(RIGHTOF2-ROBOT)
)
:effect
(and
(Flag162-)
(Flag162prime-)
)

)
(:action Flag163Action-2
:parameters ()
:precondition
(and
(RIGHTOF2-ROBOT)
)
:effect
(and
(Flag163-)
(Flag163prime-)
)

)
(:action Flag185Action-4
:parameters ()
:precondition
(and
(LEFTOF13-ROBOT)
)
:effect
(and
(Flag185-)
(Flag185prime-)
)

)
(:action Prim195Action-1
:parameters ()
:precondition
(and
(not (RIGHTOF2-ROBOT))
)
:effect
(and
(Flag195prime-)
(not (Flag195-))
)

)
(:action Prim212Action-0
:parameters ()
:precondition
(and
(not (RIGHTOF2-ROBOT))
)
:effect
(and
(Flag212prime-)
(not (Flag212-))
)

)
(:action Prim227Action-1
:parameters ()
:precondition
(and
(not (RIGHTOF2-ROBOT))
)
:effect
(and
(Flag227prime-)
(not (Flag227-))
)

)
(:action Prim308Action-0
:parameters ()
:precondition
(and
(not (LEFTOF13-ROBOT))
)
:effect
(and
(Flag308prime-)
(not (Flag308-))
)

)
(:action Prim309Action-1
:parameters ()
:precondition
(and
(not (LEFTOF13-ROBOT))
)
:effect
(and
(Flag309prime-)
(not (Flag309-))
)

)
(:action Flag312Action-4
:parameters ()
:precondition
(and
(LEFTOF13-ROBOT)
)
:effect
(and
(Flag312-)
(Flag312prime-)
)

)
)
