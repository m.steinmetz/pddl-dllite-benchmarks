(define (domain grounded-SIMPLE-ADL-ROBOT)
(:requirements
:strips
)
(:predicates
(Flag238-)
(Flag135-)
(Flag134-)
(Flag133-)
(Flag132-)
(Flag15-)
(Flag14-)
(Flag3-)
(Flag2-)
(ROW10-ROBOT)
(ROW9-ROBOT)
(ROW8-ROBOT)
(ROW7-ROBOT)
(ROW6-ROBOT)
(ROW5-ROBOT)
(ROW4-ROBOT)
(ROW3-ROBOT)
(ROW2-ROBOT)
(ROW1-ROBOT)
(ROW0-ROBOT)
(ABOVEOF10-ROBOT)
(ABOVEOF9-ROBOT)
(ABOVEOF8-ROBOT)
(ABOVEOF7-ROBOT)
(ABOVEOF6-ROBOT)
(ABOVEOF5-ROBOT)
(ABOVEOF4-ROBOT)
(ABOVEOF3-ROBOT)
(ABOVEOF2-ROBOT)
(ABOVEOF1-ROBOT)
(BELOWOF10-ROBOT)
(BELOWOF9-ROBOT)
(BELOWOF8-ROBOT)
(BELOWOF7-ROBOT)
(BELOWOF6-ROBOT)
(BELOWOF5-ROBOT)
(BELOWOF4-ROBOT)
(BELOWOF3-ROBOT)
(BELOWOF2-ROBOT)
(BELOWOF1-ROBOT)
(CHECKCONSISTENCY-)
(ROW11-ROBOT)
(BELOWOF12-ROBOT)
(NOT-BELOWOF11-ROBOT)
(ABOVEOF11-ROBOT)
(Flag241-)
(Flag121-)
(Flag119-)
(Flag118-)
(Flag116-)
(Flag115-)
(Flag114-)
(Flag113-)
(Flag111-)
(Flag110-)
(Flag109-)
(Flag108-)
(Flag107-)
(Flag105-)
(Flag104-)
(Flag103-)
(Flag102-)
(Flag101-)
(Flag100-)
(Flag98-)
(Flag97-)
(Flag96-)
(Flag95-)
(Flag94-)
(Flag93-)
(Flag92-)
(Flag91-)
(Flag89-)
(Flag88-)
(Flag87-)
(Flag86-)
(Flag85-)
(Flag84-)
(Flag83-)
(Flag82-)
(Flag80-)
(Flag79-)
(Flag78-)
(Flag77-)
(Flag76-)
(Flag75-)
(Flag74-)
(Flag73-)
(Flag71-)
(Flag70-)
(Flag69-)
(Flag68-)
(Flag67-)
(Flag66-)
(Flag65-)
(Flag64-)
(Flag63-)
(Flag62-)
(Flag61-)
(Flag59-)
(Flag58-)
(Flag57-)
(Flag56-)
(Flag55-)
(Flag54-)
(Flag53-)
(Flag52-)
(Flag51-)
(Flag50-)
(Flag49-)
(Flag47-)
(Flag46-)
(Flag45-)
(Flag44-)
(Flag43-)
(Flag42-)
(Flag41-)
(Flag40-)
(Flag39-)
(Flag38-)
(Flag37-)
(Flag36-)
(Flag35-)
(Flag34-)
(Flag33-)
(Flag32-)
(Flag31-)
(Flag30-)
(Flag29-)
(Flag28-)
(Flag27-)
(Flag26-)
(Flag25-)
(Flag24-)
(Flag23-)
(Flag22-)
(Flag21-)
(Flag20-)
(Flag19-)
(Flag18-)
(Flag17-)
(Flag16-)
(Flag13-)
(Flag12-)
(Flag11-)
(Flag10-)
(Flag9-)
(Flag8-)
(Flag7-)
(Flag6-)
(Flag5-)
(Flag4-)
(ERROR-)
(COLUMN10-ROBOT)
(COLUMN9-ROBOT)
(COLUMN8-ROBOT)
(COLUMN7-ROBOT)
(COLUMN6-ROBOT)
(COLUMN5-ROBOT)
(COLUMN4-ROBOT)
(COLUMN3-ROBOT)
(COLUMN2-ROBOT)
(COLUMN1-ROBOT)
(COLUMN0-ROBOT)
(RIGHTOF10-ROBOT)
(RIGHTOF9-ROBOT)
(RIGHTOF8-ROBOT)
(RIGHTOF7-ROBOT)
(RIGHTOF6-ROBOT)
(RIGHTOF5-ROBOT)
(RIGHTOF4-ROBOT)
(RIGHTOF3-ROBOT)
(RIGHTOF1-ROBOT)
(NOT-RIGHTOF2-ROBOT)
(RIGHTOF0-ROBOT)
(LEFTOF10-ROBOT)
(LEFTOF9-ROBOT)
(LEFTOF8-ROBOT)
(LEFTOF7-ROBOT)
(LEFTOF6-ROBOT)
(LEFTOF5-ROBOT)
(LEFTOF4-ROBOT)
(LEFTOF3-ROBOT)
(LEFTOF2-ROBOT)
(LEFTOF1-ROBOT)
(COLUMN11-ROBOT)
(LEFTOF12-ROBOT)
(NOT-LEFTOF11-ROBOT)
(RIGHTOF11-ROBOT)
(Flag239-)
(Flag236-)
(Flag234-)
(Flag233-)
(Flag231-)
(Flag230-)
(Flag229-)
(Flag227-)
(Flag226-)
(Flag225-)
(Flag224-)
(Flag222-)
(Flag221-)
(Flag220-)
(Flag219-)
(Flag218-)
(Flag217-)
(Flag215-)
(Flag214-)
(Flag213-)
(Flag212-)
(Flag211-)
(Flag210-)
(Flag209-)
(Flag207-)
(Flag206-)
(Flag205-)
(Flag204-)
(Flag203-)
(Flag202-)
(Flag201-)
(Flag199-)
(Flag198-)
(Flag197-)
(Flag196-)
(Flag195-)
(Flag194-)
(Flag193-)
(Flag192-)
(Flag191-)
(Flag190-)
(Flag188-)
(Flag187-)
(Flag186-)
(Flag185-)
(Flag184-)
(Flag183-)
(Flag182-)
(Flag181-)
(Flag180-)
(Flag179-)
(Flag177-)
(Flag176-)
(Flag175-)
(Flag174-)
(Flag173-)
(Flag172-)
(Flag171-)
(Flag170-)
(Flag169-)
(Flag168-)
(Flag166-)
(Flag165-)
(Flag164-)
(Flag163-)
(Flag162-)
(Flag161-)
(Flag160-)
(Flag159-)
(Flag158-)
(Flag157-)
(Flag156-)
(Flag155-)
(Flag154-)
(Flag153-)
(Flag152-)
(Flag151-)
(Flag150-)
(Flag149-)
(Flag148-)
(Flag147-)
(Flag146-)
(Flag145-)
(Flag144-)
(Flag143-)
(Flag142-)
(Flag141-)
(Flag140-)
(Flag139-)
(Flag138-)
(Flag137-)
(Flag136-)
(Flag131-)
(Flag130-)
(Flag129-)
(Flag128-)
(Flag127-)
(Flag126-)
(Flag125-)
(Flag124-)
(Flag123-)
(Flag122-)
(Flag120-)
(Flag117-)
(Flag112-)
(Flag106-)
(Flag99-)
(Flag90-)
(Flag81-)
(Flag72-)
(Flag60-)
(Flag48-)
(Flag1-)
(Flag240-)
(Flag237-)
(Flag235-)
(Flag232-)
(Flag228-)
(Flag223-)
(Flag216-)
(Flag208-)
(Flag200-)
(Flag189-)
(Flag178-)
(Flag167-)
(NOT-COLUMN11-ROBOT)
(NOT-COLUMN10-ROBOT)
(NOT-COLUMN9-ROBOT)
(NOT-COLUMN8-ROBOT)
(NOT-COLUMN7-ROBOT)
(NOT-COLUMN6-ROBOT)
(NOT-COLUMN5-ROBOT)
(NOT-COLUMN4-ROBOT)
(NOT-COLUMN3-ROBOT)
(NOT-COLUMN2-ROBOT)
(NOT-COLUMN1-ROBOT)
(NOT-LEFTOF12-ROBOT)
(LEFTOF11-ROBOT)
(NOT-LEFTOF10-ROBOT)
(NOT-LEFTOF9-ROBOT)
(NOT-LEFTOF8-ROBOT)
(NOT-LEFTOF7-ROBOT)
(NOT-LEFTOF6-ROBOT)
(NOT-LEFTOF5-ROBOT)
(NOT-LEFTOF4-ROBOT)
(NOT-LEFTOF3-ROBOT)
(NOT-LEFTOF2-ROBOT)
(NOT-RIGHTOF11-ROBOT)
(NOT-RIGHTOF10-ROBOT)
(NOT-RIGHTOF9-ROBOT)
(NOT-RIGHTOF8-ROBOT)
(NOT-RIGHTOF7-ROBOT)
(NOT-RIGHTOF6-ROBOT)
(NOT-RIGHTOF5-ROBOT)
(NOT-RIGHTOF4-ROBOT)
(NOT-RIGHTOF3-ROBOT)
(NOT-RIGHTOF1-ROBOT)
(NOT-CHECKCONSISTENCY-)
(NOT-COLUMN0-ROBOT)
(RIGHTOF2-ROBOT)
(NOT-RIGHTOF0-ROBOT)
(NOT-LEFTOF1-ROBOT)
(NOT-ERROR-)
(NOT-ROW11-ROBOT)
(NOT-ROW10-ROBOT)
(NOT-ROW9-ROBOT)
(NOT-ROW8-ROBOT)
(NOT-ROW7-ROBOT)
(NOT-ROW6-ROBOT)
(NOT-ROW5-ROBOT)
(NOT-ROW4-ROBOT)
(NOT-ROW3-ROBOT)
(NOT-ROW2-ROBOT)
(NOT-ROW1-ROBOT)
(NOT-BELOWOF12-ROBOT)
(BELOWOF11-ROBOT)
(NOT-BELOWOF10-ROBOT)
(NOT-BELOWOF9-ROBOT)
(NOT-BELOWOF8-ROBOT)
(NOT-BELOWOF7-ROBOT)
(NOT-BELOWOF6-ROBOT)
(NOT-BELOWOF5-ROBOT)
(NOT-BELOWOF4-ROBOT)
(NOT-BELOWOF3-ROBOT)
(NOT-BELOWOF2-ROBOT)
(NOT-ABOVEOF11-ROBOT)
(NOT-ABOVEOF10-ROBOT)
(NOT-ABOVEOF9-ROBOT)
(NOT-ABOVEOF8-ROBOT)
(NOT-ABOVEOF7-ROBOT)
(NOT-ABOVEOF6-ROBOT)
(NOT-ABOVEOF5-ROBOT)
(NOT-ABOVEOF4-ROBOT)
(NOT-ABOVEOF3-ROBOT)
(NOT-ABOVEOF2-ROBOT)
(NOT-ABOVEOF1-ROBOT)
(NOT-ROW0-ROBOT)
(NOT-BELOWOF1-ROBOT)
(Flag238prime-)
(Flag135prime-)
(Flag134prime-)
(Flag133prime-)
(Flag132prime-)
(Flag15prime-)
(Flag14prime-)
(Flag3prime-)
(Flag2prime-)
(Flag241prime-)
(Flag121prime-)
(Flag119prime-)
(Flag118prime-)
(Flag116prime-)
(Flag115prime-)
(Flag114prime-)
(Flag113prime-)
(Flag111prime-)
(Flag110prime-)
(Flag109prime-)
(Flag108prime-)
(Flag107prime-)
(Flag105prime-)
(Flag104prime-)
(Flag103prime-)
(Flag102prime-)
(Flag101prime-)
(Flag100prime-)
(Flag98prime-)
(Flag97prime-)
(Flag96prime-)
(Flag95prime-)
(Flag94prime-)
(Flag93prime-)
(Flag92prime-)
(Flag91prime-)
(Flag89prime-)
(Flag88prime-)
(Flag87prime-)
(Flag86prime-)
(Flag85prime-)
(Flag84prime-)
(Flag83prime-)
(Flag82prime-)
(Flag80prime-)
(Flag79prime-)
(Flag78prime-)
(Flag77prime-)
(Flag76prime-)
(Flag75prime-)
(Flag74prime-)
(Flag73prime-)
(Flag71prime-)
(Flag70prime-)
(Flag69prime-)
(Flag68prime-)
(Flag67prime-)
(Flag66prime-)
(Flag65prime-)
(Flag64prime-)
(Flag63prime-)
(Flag62prime-)
(Flag61prime-)
(Flag59prime-)
(Flag58prime-)
(Flag57prime-)
(Flag56prime-)
(Flag55prime-)
(Flag54prime-)
(Flag53prime-)
(Flag52prime-)
(Flag51prime-)
(Flag50prime-)
(Flag49prime-)
(Flag47prime-)
(Flag46prime-)
(Flag45prime-)
(Flag44prime-)
(Flag43prime-)
(Flag42prime-)
(Flag41prime-)
(Flag40prime-)
(Flag39prime-)
(Flag38prime-)
(Flag37prime-)
(Flag36prime-)
(Flag35prime-)
(Flag34prime-)
(Flag33prime-)
(Flag32prime-)
(Flag31prime-)
(Flag30prime-)
(Flag29prime-)
(Flag28prime-)
(Flag27prime-)
(Flag26prime-)
(Flag25prime-)
(Flag24prime-)
(Flag23prime-)
(Flag22prime-)
(Flag21prime-)
(Flag20prime-)
(Flag19prime-)
(Flag18prime-)
(Flag17prime-)
(Flag16prime-)
(Flag13prime-)
(Flag12prime-)
(Flag11prime-)
(Flag10prime-)
(Flag9prime-)
(Flag8prime-)
(Flag7prime-)
(Flag6prime-)
(Flag5prime-)
(Flag4prime-)
(Flag239prime-)
(Flag236prime-)
(Flag234prime-)
(Flag233prime-)
(Flag231prime-)
(Flag230prime-)
(Flag229prime-)
(Flag227prime-)
(Flag226prime-)
(Flag225prime-)
(Flag224prime-)
(Flag222prime-)
(Flag221prime-)
(Flag220prime-)
(Flag219prime-)
(Flag218prime-)
(Flag217prime-)
(Flag215prime-)
(Flag214prime-)
(Flag213prime-)
(Flag212prime-)
(Flag211prime-)
(Flag210prime-)
(Flag209prime-)
(Flag207prime-)
(Flag206prime-)
(Flag205prime-)
(Flag204prime-)
(Flag203prime-)
(Flag202prime-)
(Flag201prime-)
(Flag199prime-)
(Flag198prime-)
(Flag197prime-)
(Flag196prime-)
(Flag195prime-)
(Flag194prime-)
(Flag193prime-)
(Flag192prime-)
(Flag191prime-)
(Flag190prime-)
(Flag188prime-)
(Flag187prime-)
(Flag186prime-)
(Flag185prime-)
(Flag184prime-)
(Flag183prime-)
(Flag182prime-)
(Flag181prime-)
(Flag180prime-)
(Flag179prime-)
(Flag177prime-)
(Flag176prime-)
(Flag175prime-)
(Flag174prime-)
(Flag173prime-)
(Flag172prime-)
(Flag171prime-)
(Flag170prime-)
(Flag169prime-)
(Flag168prime-)
(Flag166prime-)
(Flag165prime-)
(Flag164prime-)
(Flag163prime-)
(Flag162prime-)
(Flag161prime-)
(Flag160prime-)
(Flag159prime-)
(Flag158prime-)
(Flag157prime-)
(Flag156prime-)
(Flag155prime-)
(Flag154prime-)
(Flag153prime-)
(Flag152prime-)
(Flag151prime-)
(Flag150prime-)
(Flag149prime-)
(Flag148prime-)
(Flag147prime-)
(Flag146prime-)
(Flag145prime-)
(Flag144prime-)
(Flag143prime-)
(Flag142prime-)
(Flag141prime-)
(Flag140prime-)
(Flag139prime-)
(Flag138prime-)
(Flag137prime-)
(Flag136prime-)
(Flag131prime-)
(Flag130prime-)
(Flag129prime-)
(Flag128prime-)
(Flag127prime-)
(Flag126prime-)
(Flag125prime-)
(Flag124prime-)
(Flag123prime-)
(Flag122prime-)
(Flag120prime-)
(Flag117prime-)
(Flag112prime-)
(Flag106prime-)
(Flag99prime-)
(Flag90prime-)
(Flag81prime-)
(Flag72prime-)
(Flag60prime-)
(Flag48prime-)
(Flag1prime-)
(Flag240prime-)
(Flag237prime-)
(Flag235prime-)
(Flag232prime-)
(Flag228prime-)
(Flag223prime-)
(Flag216prime-)
(Flag208prime-)
(Flag200prime-)
(Flag189prime-)
(Flag178prime-)
(Flag167prime-)
)
(:action Flag167Action-0
:parameters ()
:precondition
(and
(Flag144-)
(Flag144prime-)
)
:effect
(and
(Flag167-)
(Flag167prime-)
)

)
(:action Flag167Action-1
:parameters ()
:precondition
(and
(Flag145-)
(Flag145prime-)
)
:effect
(and
(Flag167-)
(Flag167prime-)
)

)
(:action Flag167Action-2
:parameters ()
:precondition
(and
(Flag146-)
(Flag146prime-)
)
:effect
(and
(Flag167-)
(Flag167prime-)
)

)
(:action Flag167Action-3
:parameters ()
:precondition
(and
(Flag147-)
(Flag147prime-)
)
:effect
(and
(Flag167-)
(Flag167prime-)
)

)
(:action Flag167Action-4
:parameters ()
:precondition
(and
(Flag148-)
(Flag148prime-)
)
:effect
(and
(Flag167-)
(Flag167prime-)
)

)
(:action Flag167Action-5
:parameters ()
:precondition
(and
(Flag149-)
(Flag149prime-)
)
:effect
(and
(Flag167-)
(Flag167prime-)
)

)
(:action Flag167Action-6
:parameters ()
:precondition
(and
(Flag150-)
(Flag150prime-)
)
:effect
(and
(Flag167-)
(Flag167prime-)
)

)
(:action Flag167Action-7
:parameters ()
:precondition
(and
(Flag151-)
(Flag151prime-)
)
:effect
(and
(Flag167-)
(Flag167prime-)
)

)
(:action Flag167Action-8
:parameters ()
:precondition
(and
(Flag152-)
(Flag152prime-)
)
:effect
(and
(Flag167-)
(Flag167prime-)
)

)
(:action Flag167Action-9
:parameters ()
:precondition
(and
(Flag153-)
(Flag153prime-)
)
:effect
(and
(Flag167-)
(Flag167prime-)
)

)
(:action Flag167Action-10
:parameters ()
:precondition
(and
(Flag154-)
(Flag154prime-)
)
:effect
(and
(Flag167-)
(Flag167prime-)
)

)
(:action Flag167Action-11
:parameters ()
:precondition
(and
(Flag155-)
(Flag155prime-)
)
:effect
(and
(Flag167-)
(Flag167prime-)
)

)
(:action Flag167Action-12
:parameters ()
:precondition
(and
(Flag156-)
(Flag156prime-)
)
:effect
(and
(Flag167-)
(Flag167prime-)
)

)
(:action Flag167Action-13
:parameters ()
:precondition
(and
(Flag157-)
(Flag157prime-)
)
:effect
(and
(Flag167-)
(Flag167prime-)
)

)
(:action Flag167Action-14
:parameters ()
:precondition
(and
(Flag158-)
(Flag158prime-)
)
:effect
(and
(Flag167-)
(Flag167prime-)
)

)
(:action Flag167Action-15
:parameters ()
:precondition
(and
(Flag159-)
(Flag159prime-)
)
:effect
(and
(Flag167-)
(Flag167prime-)
)

)
(:action Flag167Action-16
:parameters ()
:precondition
(and
(Flag160-)
(Flag160prime-)
)
:effect
(and
(Flag167-)
(Flag167prime-)
)

)
(:action Flag167Action-17
:parameters ()
:precondition
(and
(Flag161-)
(Flag161prime-)
)
:effect
(and
(Flag167-)
(Flag167prime-)
)

)
(:action Flag167Action-18
:parameters ()
:precondition
(and
(Flag162-)
(Flag162prime-)
)
:effect
(and
(Flag167-)
(Flag167prime-)
)

)
(:action Flag167Action-19
:parameters ()
:precondition
(and
(Flag163-)
(Flag163prime-)
)
:effect
(and
(Flag167-)
(Flag167prime-)
)

)
(:action Flag167Action-20
:parameters ()
:precondition
(and
(Flag164-)
(Flag164prime-)
)
:effect
(and
(Flag167-)
(Flag167prime-)
)

)
(:action Flag167Action-21
:parameters ()
:precondition
(and
(Flag165-)
(Flag165prime-)
)
:effect
(and
(Flag167-)
(Flag167prime-)
)

)
(:action Flag167Action-22
:parameters ()
:precondition
(and
(Flag166-)
(Flag166prime-)
)
:effect
(and
(Flag167-)
(Flag167prime-)
)

)
(:action Prim167Action
:parameters ()
:precondition
(and
(not (Flag144-))
(Flag144prime-)
(not (Flag145-))
(Flag145prime-)
(not (Flag146-))
(Flag146prime-)
(not (Flag147-))
(Flag147prime-)
(not (Flag148-))
(Flag148prime-)
(not (Flag149-))
(Flag149prime-)
(not (Flag150-))
(Flag150prime-)
(not (Flag151-))
(Flag151prime-)
(not (Flag152-))
(Flag152prime-)
(not (Flag153-))
(Flag153prime-)
(not (Flag154-))
(Flag154prime-)
(not (Flag155-))
(Flag155prime-)
(not (Flag156-))
(Flag156prime-)
(not (Flag157-))
(Flag157prime-)
(not (Flag158-))
(Flag158prime-)
(not (Flag159-))
(Flag159prime-)
(not (Flag160-))
(Flag160prime-)
(not (Flag161-))
(Flag161prime-)
(not (Flag162-))
(Flag162prime-)
(not (Flag163-))
(Flag163prime-)
(not (Flag164-))
(Flag164prime-)
(not (Flag165-))
(Flag165prime-)
(not (Flag166-))
(Flag166prime-)
)
:effect
(and
(Flag167prime-)
(not (Flag167-))
)

)
(:action Flag178Action-0
:parameters ()
:precondition
(and
(Flag144-)
(Flag144prime-)
)
:effect
(and
(Flag178-)
(Flag178prime-)
)

)
(:action Flag178Action-1
:parameters ()
:precondition
(and
(Flag168-)
(Flag168prime-)
)
:effect
(and
(Flag178-)
(Flag178prime-)
)

)
(:action Flag178Action-2
:parameters ()
:precondition
(and
(Flag145-)
(Flag145prime-)
)
:effect
(and
(Flag178-)
(Flag178prime-)
)

)
(:action Flag178Action-3
:parameters ()
:precondition
(and
(Flag146-)
(Flag146prime-)
)
:effect
(and
(Flag178-)
(Flag178prime-)
)

)
(:action Flag178Action-4
:parameters ()
:precondition
(and
(Flag169-)
(Flag169prime-)
)
:effect
(and
(Flag178-)
(Flag178prime-)
)

)
(:action Flag178Action-5
:parameters ()
:precondition
(and
(Flag147-)
(Flag147prime-)
)
:effect
(and
(Flag178-)
(Flag178prime-)
)

)
(:action Flag178Action-6
:parameters ()
:precondition
(and
(Flag149-)
(Flag149prime-)
)
:effect
(and
(Flag178-)
(Flag178prime-)
)

)
(:action Flag178Action-7
:parameters ()
:precondition
(and
(Flag157-)
(Flag157prime-)
)
:effect
(and
(Flag178-)
(Flag178prime-)
)

)
(:action Flag178Action-8
:parameters ()
:precondition
(and
(Flag152-)
(Flag152prime-)
)
:effect
(and
(Flag178-)
(Flag178prime-)
)

)
(:action Flag178Action-9
:parameters ()
:precondition
(and
(Flag170-)
(Flag170prime-)
)
:effect
(and
(Flag178-)
(Flag178prime-)
)

)
(:action Flag178Action-10
:parameters ()
:precondition
(and
(Flag153-)
(Flag153prime-)
)
:effect
(and
(Flag178-)
(Flag178prime-)
)

)
(:action Flag178Action-11
:parameters ()
:precondition
(and
(Flag154-)
(Flag154prime-)
)
:effect
(and
(Flag178-)
(Flag178prime-)
)

)
(:action Flag178Action-12
:parameters ()
:precondition
(and
(Flag158-)
(Flag158prime-)
)
:effect
(and
(Flag178-)
(Flag178prime-)
)

)
(:action Flag178Action-13
:parameters ()
:precondition
(and
(Flag155-)
(Flag155prime-)
)
:effect
(and
(Flag178-)
(Flag178prime-)
)

)
(:action Flag178Action-14
:parameters ()
:precondition
(and
(Flag156-)
(Flag156prime-)
)
:effect
(and
(Flag178-)
(Flag178prime-)
)

)
(:action Flag178Action-15
:parameters ()
:precondition
(and
(Flag171-)
(Flag171prime-)
)
:effect
(and
(Flag178-)
(Flag178prime-)
)

)
(:action Flag178Action-16
:parameters ()
:precondition
(and
(Flag172-)
(Flag172prime-)
)
:effect
(and
(Flag178-)
(Flag178prime-)
)

)
(:action Flag178Action-17
:parameters ()
:precondition
(and
(Flag151-)
(Flag151prime-)
)
:effect
(and
(Flag178-)
(Flag178prime-)
)

)
(:action Flag178Action-18
:parameters ()
:precondition
(and
(Flag173-)
(Flag173prime-)
)
:effect
(and
(Flag178-)
(Flag178prime-)
)

)
(:action Flag178Action-19
:parameters ()
:precondition
(and
(Flag159-)
(Flag159prime-)
)
:effect
(and
(Flag178-)
(Flag178prime-)
)

)
(:action Flag178Action-20
:parameters ()
:precondition
(and
(Flag174-)
(Flag174prime-)
)
:effect
(and
(Flag178-)
(Flag178prime-)
)

)
(:action Flag178Action-21
:parameters ()
:precondition
(and
(Flag175-)
(Flag175prime-)
)
:effect
(and
(Flag178-)
(Flag178prime-)
)

)
(:action Flag178Action-22
:parameters ()
:precondition
(and
(Flag160-)
(Flag160prime-)
)
:effect
(and
(Flag178-)
(Flag178prime-)
)

)
(:action Flag178Action-23
:parameters ()
:precondition
(and
(Flag161-)
(Flag161prime-)
)
:effect
(and
(Flag178-)
(Flag178prime-)
)

)
(:action Flag178Action-24
:parameters ()
:precondition
(and
(Flag162-)
(Flag162prime-)
)
:effect
(and
(Flag178-)
(Flag178prime-)
)

)
(:action Flag178Action-25
:parameters ()
:precondition
(and
(Flag163-)
(Flag163prime-)
)
:effect
(and
(Flag178-)
(Flag178prime-)
)

)
(:action Flag178Action-26
:parameters ()
:precondition
(and
(Flag164-)
(Flag164prime-)
)
:effect
(and
(Flag178-)
(Flag178prime-)
)

)
(:action Flag178Action-27
:parameters ()
:precondition
(and
(Flag165-)
(Flag165prime-)
)
:effect
(and
(Flag178-)
(Flag178prime-)
)

)
(:action Flag178Action-28
:parameters ()
:precondition
(and
(Flag176-)
(Flag176prime-)
)
:effect
(and
(Flag178-)
(Flag178prime-)
)

)
(:action Flag178Action-29
:parameters ()
:precondition
(and
(Flag166-)
(Flag166prime-)
)
:effect
(and
(Flag178-)
(Flag178prime-)
)

)
(:action Flag178Action-30
:parameters ()
:precondition
(and
(Flag177-)
(Flag177prime-)
)
:effect
(and
(Flag178-)
(Flag178prime-)
)

)
(:action Prim178Action
:parameters ()
:precondition
(and
(not (Flag144-))
(Flag144prime-)
(not (Flag168-))
(Flag168prime-)
(not (Flag145-))
(Flag145prime-)
(not (Flag146-))
(Flag146prime-)
(not (Flag169-))
(Flag169prime-)
(not (Flag147-))
(Flag147prime-)
(not (Flag149-))
(Flag149prime-)
(not (Flag157-))
(Flag157prime-)
(not (Flag152-))
(Flag152prime-)
(not (Flag170-))
(Flag170prime-)
(not (Flag153-))
(Flag153prime-)
(not (Flag154-))
(Flag154prime-)
(not (Flag158-))
(Flag158prime-)
(not (Flag155-))
(Flag155prime-)
(not (Flag156-))
(Flag156prime-)
(not (Flag171-))
(Flag171prime-)
(not (Flag172-))
(Flag172prime-)
(not (Flag151-))
(Flag151prime-)
(not (Flag173-))
(Flag173prime-)
(not (Flag159-))
(Flag159prime-)
(not (Flag174-))
(Flag174prime-)
(not (Flag175-))
(Flag175prime-)
(not (Flag160-))
(Flag160prime-)
(not (Flag161-))
(Flag161prime-)
(not (Flag162-))
(Flag162prime-)
(not (Flag163-))
(Flag163prime-)
(not (Flag164-))
(Flag164prime-)
(not (Flag165-))
(Flag165prime-)
(not (Flag176-))
(Flag176prime-)
(not (Flag166-))
(Flag166prime-)
(not (Flag177-))
(Flag177prime-)
)
:effect
(and
(Flag178prime-)
(not (Flag178-))
)

)
(:action Flag189Action-0
:parameters ()
:precondition
(and
(Flag144-)
(Flag144prime-)
)
:effect
(and
(Flag189-)
(Flag189prime-)
)

)
(:action Flag189Action-1
:parameters ()
:precondition
(and
(Flag168-)
(Flag168prime-)
)
:effect
(and
(Flag189-)
(Flag189prime-)
)

)
(:action Flag189Action-2
:parameters ()
:precondition
(and
(Flag145-)
(Flag145prime-)
)
:effect
(and
(Flag189-)
(Flag189prime-)
)

)
(:action Flag189Action-3
:parameters ()
:precondition
(and
(Flag146-)
(Flag146prime-)
)
:effect
(and
(Flag189-)
(Flag189prime-)
)

)
(:action Flag189Action-4
:parameters ()
:precondition
(and
(Flag169-)
(Flag169prime-)
)
:effect
(and
(Flag189-)
(Flag189prime-)
)

)
(:action Flag189Action-5
:parameters ()
:precondition
(and
(Flag147-)
(Flag147prime-)
)
:effect
(and
(Flag189-)
(Flag189prime-)
)

)
(:action Flag189Action-6
:parameters ()
:precondition
(and
(Flag179-)
(Flag179prime-)
)
:effect
(and
(Flag189-)
(Flag189prime-)
)

)
(:action Flag189Action-7
:parameters ()
:precondition
(and
(Flag180-)
(Flag180prime-)
)
:effect
(and
(Flag189-)
(Flag189prime-)
)

)
(:action Flag189Action-8
:parameters ()
:precondition
(and
(Flag149-)
(Flag149prime-)
)
:effect
(and
(Flag189-)
(Flag189prime-)
)

)
(:action Flag189Action-9
:parameters ()
:precondition
(and
(Flag172-)
(Flag172prime-)
)
:effect
(and
(Flag189-)
(Flag189prime-)
)

)
(:action Flag189Action-10
:parameters ()
:precondition
(and
(Flag157-)
(Flag157prime-)
)
:effect
(and
(Flag189-)
(Flag189prime-)
)

)
(:action Flag189Action-11
:parameters ()
:precondition
(and
(Flag152-)
(Flag152prime-)
)
:effect
(and
(Flag189-)
(Flag189prime-)
)

)
(:action Flag189Action-12
:parameters ()
:precondition
(and
(Flag170-)
(Flag170prime-)
)
:effect
(and
(Flag189-)
(Flag189prime-)
)

)
(:action Flag189Action-13
:parameters ()
:precondition
(and
(Flag153-)
(Flag153prime-)
)
:effect
(and
(Flag189-)
(Flag189prime-)
)

)
(:action Flag189Action-14
:parameters ()
:precondition
(and
(Flag154-)
(Flag154prime-)
)
:effect
(and
(Flag189-)
(Flag189prime-)
)

)
(:action Flag189Action-15
:parameters ()
:precondition
(and
(Flag158-)
(Flag158prime-)
)
:effect
(and
(Flag189-)
(Flag189prime-)
)

)
(:action Flag189Action-16
:parameters ()
:precondition
(and
(Flag155-)
(Flag155prime-)
)
:effect
(and
(Flag189-)
(Flag189prime-)
)

)
(:action Flag189Action-17
:parameters ()
:precondition
(and
(Flag156-)
(Flag156prime-)
)
:effect
(and
(Flag189-)
(Flag189prime-)
)

)
(:action Flag189Action-18
:parameters ()
:precondition
(and
(Flag171-)
(Flag171prime-)
)
:effect
(and
(Flag189-)
(Flag189prime-)
)

)
(:action Flag189Action-19
:parameters ()
:precondition
(and
(Flag181-)
(Flag181prime-)
)
:effect
(and
(Flag189-)
(Flag189prime-)
)

)
(:action Flag189Action-20
:parameters ()
:precondition
(and
(Flag151-)
(Flag151prime-)
)
:effect
(and
(Flag189-)
(Flag189prime-)
)

)
(:action Flag189Action-21
:parameters ()
:precondition
(and
(Flag182-)
(Flag182prime-)
)
:effect
(and
(Flag189-)
(Flag189prime-)
)

)
(:action Flag189Action-22
:parameters ()
:precondition
(and
(Flag173-)
(Flag173prime-)
)
:effect
(and
(Flag189-)
(Flag189prime-)
)

)
(:action Flag189Action-23
:parameters ()
:precondition
(and
(Flag159-)
(Flag159prime-)
)
:effect
(and
(Flag189-)
(Flag189prime-)
)

)
(:action Flag189Action-24
:parameters ()
:precondition
(and
(Flag175-)
(Flag175prime-)
)
:effect
(and
(Flag189-)
(Flag189prime-)
)

)
(:action Flag189Action-25
:parameters ()
:precondition
(and
(Flag183-)
(Flag183prime-)
)
:effect
(and
(Flag189-)
(Flag189prime-)
)

)
(:action Flag189Action-26
:parameters ()
:precondition
(and
(Flag184-)
(Flag184prime-)
)
:effect
(and
(Flag189-)
(Flag189prime-)
)

)
(:action Flag189Action-27
:parameters ()
:precondition
(and
(Flag160-)
(Flag160prime-)
)
:effect
(and
(Flag189-)
(Flag189prime-)
)

)
(:action Flag189Action-28
:parameters ()
:precondition
(and
(Flag185-)
(Flag185prime-)
)
:effect
(and
(Flag189-)
(Flag189prime-)
)

)
(:action Flag189Action-29
:parameters ()
:precondition
(and
(Flag186-)
(Flag186prime-)
)
:effect
(and
(Flag189-)
(Flag189prime-)
)

)
(:action Flag189Action-30
:parameters ()
:precondition
(and
(Flag161-)
(Flag161prime-)
)
:effect
(and
(Flag189-)
(Flag189prime-)
)

)
(:action Flag189Action-31
:parameters ()
:precondition
(and
(Flag164-)
(Flag164prime-)
)
:effect
(and
(Flag189-)
(Flag189prime-)
)

)
(:action Flag189Action-32
:parameters ()
:precondition
(and
(Flag187-)
(Flag187prime-)
)
:effect
(and
(Flag189-)
(Flag189prime-)
)

)
(:action Flag189Action-33
:parameters ()
:precondition
(and
(Flag188-)
(Flag188prime-)
)
:effect
(and
(Flag189-)
(Flag189prime-)
)

)
(:action Flag189Action-34
:parameters ()
:precondition
(and
(Flag165-)
(Flag165prime-)
)
:effect
(and
(Flag189-)
(Flag189prime-)
)

)
(:action Flag189Action-35
:parameters ()
:precondition
(and
(Flag176-)
(Flag176prime-)
)
:effect
(and
(Flag189-)
(Flag189prime-)
)

)
(:action Flag189Action-36
:parameters ()
:precondition
(and
(Flag166-)
(Flag166prime-)
)
:effect
(and
(Flag189-)
(Flag189prime-)
)

)
(:action Prim189Action
:parameters ()
:precondition
(and
(not (Flag144-))
(Flag144prime-)
(not (Flag168-))
(Flag168prime-)
(not (Flag145-))
(Flag145prime-)
(not (Flag146-))
(Flag146prime-)
(not (Flag169-))
(Flag169prime-)
(not (Flag147-))
(Flag147prime-)
(not (Flag179-))
(Flag179prime-)
(not (Flag180-))
(Flag180prime-)
(not (Flag149-))
(Flag149prime-)
(not (Flag172-))
(Flag172prime-)
(not (Flag157-))
(Flag157prime-)
(not (Flag152-))
(Flag152prime-)
(not (Flag170-))
(Flag170prime-)
(not (Flag153-))
(Flag153prime-)
(not (Flag154-))
(Flag154prime-)
(not (Flag158-))
(Flag158prime-)
(not (Flag155-))
(Flag155prime-)
(not (Flag156-))
(Flag156prime-)
(not (Flag171-))
(Flag171prime-)
(not (Flag181-))
(Flag181prime-)
(not (Flag151-))
(Flag151prime-)
(not (Flag182-))
(Flag182prime-)
(not (Flag173-))
(Flag173prime-)
(not (Flag159-))
(Flag159prime-)
(not (Flag175-))
(Flag175prime-)
(not (Flag183-))
(Flag183prime-)
(not (Flag184-))
(Flag184prime-)
(not (Flag160-))
(Flag160prime-)
(not (Flag185-))
(Flag185prime-)
(not (Flag186-))
(Flag186prime-)
(not (Flag161-))
(Flag161prime-)
(not (Flag164-))
(Flag164prime-)
(not (Flag187-))
(Flag187prime-)
(not (Flag188-))
(Flag188prime-)
(not (Flag165-))
(Flag165prime-)
(not (Flag176-))
(Flag176prime-)
(not (Flag166-))
(Flag166prime-)
)
:effect
(and
(Flag189prime-)
(not (Flag189-))
)

)
(:action Flag200Action-0
:parameters ()
:precondition
(and
(Flag190-)
(Flag190prime-)
)
:effect
(and
(Flag200-)
(Flag200prime-)
)

)
(:action Flag200Action-1
:parameters ()
:precondition
(and
(Flag144-)
(Flag144prime-)
)
:effect
(and
(Flag200-)
(Flag200prime-)
)

)
(:action Flag200Action-2
:parameters ()
:precondition
(and
(Flag168-)
(Flag168prime-)
)
:effect
(and
(Flag200-)
(Flag200prime-)
)

)
(:action Flag200Action-3
:parameters ()
:precondition
(and
(Flag145-)
(Flag145prime-)
)
:effect
(and
(Flag200-)
(Flag200prime-)
)

)
(:action Flag200Action-4
:parameters ()
:precondition
(and
(Flag191-)
(Flag191prime-)
)
:effect
(and
(Flag200-)
(Flag200prime-)
)

)
(:action Flag200Action-5
:parameters ()
:precondition
(and
(Flag169-)
(Flag169prime-)
)
:effect
(and
(Flag200-)
(Flag200prime-)
)

)
(:action Flag200Action-6
:parameters ()
:precondition
(and
(Flag147-)
(Flag147prime-)
)
:effect
(and
(Flag200-)
(Flag200prime-)
)

)
(:action Flag200Action-7
:parameters ()
:precondition
(and
(Flag192-)
(Flag192prime-)
)
:effect
(and
(Flag200-)
(Flag200prime-)
)

)
(:action Flag200Action-8
:parameters ()
:precondition
(and
(Flag179-)
(Flag179prime-)
)
:effect
(and
(Flag200-)
(Flag200prime-)
)

)
(:action Flag200Action-9
:parameters ()
:precondition
(and
(Flag180-)
(Flag180prime-)
)
:effect
(and
(Flag200-)
(Flag200prime-)
)

)
(:action Flag200Action-10
:parameters ()
:precondition
(and
(Flag149-)
(Flag149prime-)
)
:effect
(and
(Flag200-)
(Flag200prime-)
)

)
(:action Flag200Action-11
:parameters ()
:precondition
(and
(Flag193-)
(Flag193prime-)
)
:effect
(and
(Flag200-)
(Flag200prime-)
)

)
(:action Flag200Action-12
:parameters ()
:precondition
(and
(Flag157-)
(Flag157prime-)
)
:effect
(and
(Flag200-)
(Flag200prime-)
)

)
(:action Flag200Action-13
:parameters ()
:precondition
(and
(Flag152-)
(Flag152prime-)
)
:effect
(and
(Flag200-)
(Flag200prime-)
)

)
(:action Flag200Action-14
:parameters ()
:precondition
(and
(Flag170-)
(Flag170prime-)
)
:effect
(and
(Flag200-)
(Flag200prime-)
)

)
(:action Flag200Action-15
:parameters ()
:precondition
(and
(Flag153-)
(Flag153prime-)
)
:effect
(and
(Flag200-)
(Flag200prime-)
)

)
(:action Flag200Action-16
:parameters ()
:precondition
(and
(Flag194-)
(Flag194prime-)
)
:effect
(and
(Flag200-)
(Flag200prime-)
)

)
(:action Flag200Action-17
:parameters ()
:precondition
(and
(Flag195-)
(Flag195prime-)
)
:effect
(and
(Flag200-)
(Flag200prime-)
)

)
(:action Flag200Action-18
:parameters ()
:precondition
(and
(Flag196-)
(Flag196prime-)
)
:effect
(and
(Flag200-)
(Flag200prime-)
)

)
(:action Flag200Action-19
:parameters ()
:precondition
(and
(Flag155-)
(Flag155prime-)
)
:effect
(and
(Flag200-)
(Flag200prime-)
)

)
(:action Flag200Action-20
:parameters ()
:precondition
(and
(Flag197-)
(Flag197prime-)
)
:effect
(and
(Flag200-)
(Flag200prime-)
)

)
(:action Flag200Action-21
:parameters ()
:precondition
(and
(Flag156-)
(Flag156prime-)
)
:effect
(and
(Flag200-)
(Flag200prime-)
)

)
(:action Flag200Action-22
:parameters ()
:precondition
(and
(Flag171-)
(Flag171prime-)
)
:effect
(and
(Flag200-)
(Flag200prime-)
)

)
(:action Flag200Action-23
:parameters ()
:precondition
(and
(Flag172-)
(Flag172prime-)
)
:effect
(and
(Flag200-)
(Flag200prime-)
)

)
(:action Flag200Action-24
:parameters ()
:precondition
(and
(Flag151-)
(Flag151prime-)
)
:effect
(and
(Flag200-)
(Flag200prime-)
)

)
(:action Flag200Action-25
:parameters ()
:precondition
(and
(Flag182-)
(Flag182prime-)
)
:effect
(and
(Flag200-)
(Flag200prime-)
)

)
(:action Flag200Action-26
:parameters ()
:precondition
(and
(Flag198-)
(Flag198prime-)
)
:effect
(and
(Flag200-)
(Flag200prime-)
)

)
(:action Flag200Action-27
:parameters ()
:precondition
(and
(Flag158-)
(Flag158prime-)
)
:effect
(and
(Flag200-)
(Flag200prime-)
)

)
(:action Flag200Action-28
:parameters ()
:precondition
(and
(Flag159-)
(Flag159prime-)
)
:effect
(and
(Flag200-)
(Flag200prime-)
)

)
(:action Flag200Action-29
:parameters ()
:precondition
(and
(Flag175-)
(Flag175prime-)
)
:effect
(and
(Flag200-)
(Flag200prime-)
)

)
(:action Flag200Action-30
:parameters ()
:precondition
(and
(Flag183-)
(Flag183prime-)
)
:effect
(and
(Flag200-)
(Flag200prime-)
)

)
(:action Flag200Action-31
:parameters ()
:precondition
(and
(Flag184-)
(Flag184prime-)
)
:effect
(and
(Flag200-)
(Flag200prime-)
)

)
(:action Flag200Action-32
:parameters ()
:precondition
(and
(Flag160-)
(Flag160prime-)
)
:effect
(and
(Flag200-)
(Flag200prime-)
)

)
(:action Flag200Action-33
:parameters ()
:precondition
(and
(Flag185-)
(Flag185prime-)
)
:effect
(and
(Flag200-)
(Flag200prime-)
)

)
(:action Flag200Action-34
:parameters ()
:precondition
(and
(Flag186-)
(Flag186prime-)
)
:effect
(and
(Flag200-)
(Flag200prime-)
)

)
(:action Flag200Action-35
:parameters ()
:precondition
(and
(Flag161-)
(Flag161prime-)
)
:effect
(and
(Flag200-)
(Flag200prime-)
)

)
(:action Flag200Action-36
:parameters ()
:precondition
(and
(Flag173-)
(Flag173prime-)
)
:effect
(and
(Flag200-)
(Flag200prime-)
)

)
(:action Flag200Action-37
:parameters ()
:precondition
(and
(Flag164-)
(Flag164prime-)
)
:effect
(and
(Flag200-)
(Flag200prime-)
)

)
(:action Flag200Action-38
:parameters ()
:precondition
(and
(Flag199-)
(Flag199prime-)
)
:effect
(and
(Flag200-)
(Flag200prime-)
)

)
(:action Flag200Action-39
:parameters ()
:precondition
(and
(Flag188-)
(Flag188prime-)
)
:effect
(and
(Flag200-)
(Flag200prime-)
)

)
(:action Flag200Action-40
:parameters ()
:precondition
(and
(Flag165-)
(Flag165prime-)
)
:effect
(and
(Flag200-)
(Flag200prime-)
)

)
(:action Flag200Action-41
:parameters ()
:precondition
(and
(Flag176-)
(Flag176prime-)
)
:effect
(and
(Flag200-)
(Flag200prime-)
)

)
(:action Flag200Action-42
:parameters ()
:precondition
(and
(Flag166-)
(Flag166prime-)
)
:effect
(and
(Flag200-)
(Flag200prime-)
)

)
(:action Prim200Action
:parameters ()
:precondition
(and
(not (Flag190-))
(Flag190prime-)
(not (Flag144-))
(Flag144prime-)
(not (Flag168-))
(Flag168prime-)
(not (Flag145-))
(Flag145prime-)
(not (Flag191-))
(Flag191prime-)
(not (Flag169-))
(Flag169prime-)
(not (Flag147-))
(Flag147prime-)
(not (Flag192-))
(Flag192prime-)
(not (Flag179-))
(Flag179prime-)
(not (Flag180-))
(Flag180prime-)
(not (Flag149-))
(Flag149prime-)
(not (Flag193-))
(Flag193prime-)
(not (Flag157-))
(Flag157prime-)
(not (Flag152-))
(Flag152prime-)
(not (Flag170-))
(Flag170prime-)
(not (Flag153-))
(Flag153prime-)
(not (Flag194-))
(Flag194prime-)
(not (Flag195-))
(Flag195prime-)
(not (Flag196-))
(Flag196prime-)
(not (Flag155-))
(Flag155prime-)
(not (Flag197-))
(Flag197prime-)
(not (Flag156-))
(Flag156prime-)
(not (Flag171-))
(Flag171prime-)
(not (Flag172-))
(Flag172prime-)
(not (Flag151-))
(Flag151prime-)
(not (Flag182-))
(Flag182prime-)
(not (Flag198-))
(Flag198prime-)
(not (Flag158-))
(Flag158prime-)
(not (Flag159-))
(Flag159prime-)
(not (Flag175-))
(Flag175prime-)
(not (Flag183-))
(Flag183prime-)
(not (Flag184-))
(Flag184prime-)
(not (Flag160-))
(Flag160prime-)
(not (Flag185-))
(Flag185prime-)
(not (Flag186-))
(Flag186prime-)
(not (Flag161-))
(Flag161prime-)
(not (Flag173-))
(Flag173prime-)
(not (Flag164-))
(Flag164prime-)
(not (Flag199-))
(Flag199prime-)
(not (Flag188-))
(Flag188prime-)
(not (Flag165-))
(Flag165prime-)
(not (Flag176-))
(Flag176prime-)
(not (Flag166-))
(Flag166prime-)
)
:effect
(and
(Flag200prime-)
(not (Flag200-))
)

)
(:action Flag208Action-0
:parameters ()
:precondition
(and
(Flag190-)
(Flag190prime-)
)
:effect
(and
(Flag208-)
(Flag208prime-)
)

)
(:action Flag208Action-1
:parameters ()
:precondition
(and
(Flag144-)
(Flag144prime-)
)
:effect
(and
(Flag208-)
(Flag208prime-)
)

)
(:action Flag208Action-2
:parameters ()
:precondition
(and
(Flag191-)
(Flag191prime-)
)
:effect
(and
(Flag208-)
(Flag208prime-)
)

)
(:action Flag208Action-3
:parameters ()
:precondition
(and
(Flag145-)
(Flag145prime-)
)
:effect
(and
(Flag208-)
(Flag208prime-)
)

)
(:action Flag208Action-4
:parameters ()
:precondition
(and
(Flag176-)
(Flag176prime-)
)
:effect
(and
(Flag208-)
(Flag208prime-)
)

)
(:action Flag208Action-5
:parameters ()
:precondition
(and
(Flag147-)
(Flag147prime-)
)
:effect
(and
(Flag208-)
(Flag208prime-)
)

)
(:action Flag208Action-6
:parameters ()
:precondition
(and
(Flag180-)
(Flag180prime-)
)
:effect
(and
(Flag208-)
(Flag208prime-)
)

)
(:action Flag208Action-7
:parameters ()
:precondition
(and
(Flag149-)
(Flag149prime-)
)
:effect
(and
(Flag208-)
(Flag208prime-)
)

)
(:action Flag208Action-8
:parameters ()
:precondition
(and
(Flag193-)
(Flag193prime-)
)
:effect
(and
(Flag208-)
(Flag208prime-)
)

)
(:action Flag208Action-9
:parameters ()
:precondition
(and
(Flag151-)
(Flag151prime-)
)
:effect
(and
(Flag208-)
(Flag208prime-)
)

)
(:action Flag208Action-10
:parameters ()
:precondition
(and
(Flag152-)
(Flag152prime-)
)
:effect
(and
(Flag208-)
(Flag208prime-)
)

)
(:action Flag208Action-11
:parameters ()
:precondition
(and
(Flag170-)
(Flag170prime-)
)
:effect
(and
(Flag208-)
(Flag208prime-)
)

)
(:action Flag208Action-12
:parameters ()
:precondition
(and
(Flag153-)
(Flag153prime-)
)
:effect
(and
(Flag208-)
(Flag208prime-)
)

)
(:action Flag208Action-13
:parameters ()
:precondition
(and
(Flag201-)
(Flag201prime-)
)
:effect
(and
(Flag208-)
(Flag208prime-)
)

)
(:action Flag208Action-14
:parameters ()
:precondition
(and
(Flag202-)
(Flag202prime-)
)
:effect
(and
(Flag208-)
(Flag208prime-)
)

)
(:action Flag208Action-15
:parameters ()
:precondition
(and
(Flag195-)
(Flag195prime-)
)
:effect
(and
(Flag208-)
(Flag208prime-)
)

)
(:action Flag208Action-16
:parameters ()
:precondition
(and
(Flag196-)
(Flag196prime-)
)
:effect
(and
(Flag208-)
(Flag208prime-)
)

)
(:action Flag208Action-17
:parameters ()
:precondition
(and
(Flag197-)
(Flag197prime-)
)
:effect
(and
(Flag208-)
(Flag208prime-)
)

)
(:action Flag208Action-18
:parameters ()
:precondition
(and
(Flag156-)
(Flag156prime-)
)
:effect
(and
(Flag208-)
(Flag208prime-)
)

)
(:action Flag208Action-19
:parameters ()
:precondition
(and
(Flag168-)
(Flag168prime-)
)
:effect
(and
(Flag208-)
(Flag208prime-)
)

)
(:action Flag208Action-20
:parameters ()
:precondition
(and
(Flag171-)
(Flag171prime-)
)
:effect
(and
(Flag208-)
(Flag208prime-)
)

)
(:action Flag208Action-21
:parameters ()
:precondition
(and
(Flag172-)
(Flag172prime-)
)
:effect
(and
(Flag208-)
(Flag208prime-)
)

)
(:action Flag208Action-22
:parameters ()
:precondition
(and
(Flag157-)
(Flag157prime-)
)
:effect
(and
(Flag208-)
(Flag208prime-)
)

)
(:action Flag208Action-23
:parameters ()
:precondition
(and
(Flag182-)
(Flag182prime-)
)
:effect
(and
(Flag208-)
(Flag208prime-)
)

)
(:action Flag208Action-24
:parameters ()
:precondition
(and
(Flag198-)
(Flag198prime-)
)
:effect
(and
(Flag208-)
(Flag208prime-)
)

)
(:action Flag208Action-25
:parameters ()
:precondition
(and
(Flag173-)
(Flag173prime-)
)
:effect
(and
(Flag208-)
(Flag208prime-)
)

)
(:action Flag208Action-26
:parameters ()
:precondition
(and
(Flag203-)
(Flag203prime-)
)
:effect
(and
(Flag208-)
(Flag208prime-)
)

)
(:action Flag208Action-27
:parameters ()
:precondition
(and
(Flag159-)
(Flag159prime-)
)
:effect
(and
(Flag208-)
(Flag208prime-)
)

)
(:action Flag208Action-28
:parameters ()
:precondition
(and
(Flag175-)
(Flag175prime-)
)
:effect
(and
(Flag208-)
(Flag208prime-)
)

)
(:action Flag208Action-29
:parameters ()
:precondition
(and
(Flag183-)
(Flag183prime-)
)
:effect
(and
(Flag208-)
(Flag208prime-)
)

)
(:action Flag208Action-30
:parameters ()
:precondition
(and
(Flag204-)
(Flag204prime-)
)
:effect
(and
(Flag208-)
(Flag208prime-)
)

)
(:action Flag208Action-31
:parameters ()
:precondition
(and
(Flag205-)
(Flag205prime-)
)
:effect
(and
(Flag208-)
(Flag208prime-)
)

)
(:action Flag208Action-32
:parameters ()
:precondition
(and
(Flag206-)
(Flag206prime-)
)
:effect
(and
(Flag208-)
(Flag208prime-)
)

)
(:action Flag208Action-33
:parameters ()
:precondition
(and
(Flag184-)
(Flag184prime-)
)
:effect
(and
(Flag208-)
(Flag208prime-)
)

)
(:action Flag208Action-34
:parameters ()
:precondition
(and
(Flag160-)
(Flag160prime-)
)
:effect
(and
(Flag208-)
(Flag208prime-)
)

)
(:action Flag208Action-35
:parameters ()
:precondition
(and
(Flag185-)
(Flag185prime-)
)
:effect
(and
(Flag208-)
(Flag208prime-)
)

)
(:action Flag208Action-36
:parameters ()
:precondition
(and
(Flag186-)
(Flag186prime-)
)
:effect
(and
(Flag208-)
(Flag208prime-)
)

)
(:action Flag208Action-37
:parameters ()
:precondition
(and
(Flag161-)
(Flag161prime-)
)
:effect
(and
(Flag208-)
(Flag208prime-)
)

)
(:action Flag208Action-38
:parameters ()
:precondition
(and
(Flag164-)
(Flag164prime-)
)
:effect
(and
(Flag208-)
(Flag208prime-)
)

)
(:action Flag208Action-39
:parameters ()
:precondition
(and
(Flag199-)
(Flag199prime-)
)
:effect
(and
(Flag208-)
(Flag208prime-)
)

)
(:action Flag208Action-40
:parameters ()
:precondition
(and
(Flag188-)
(Flag188prime-)
)
:effect
(and
(Flag208-)
(Flag208prime-)
)

)
(:action Flag208Action-41
:parameters ()
:precondition
(and
(Flag165-)
(Flag165prime-)
)
:effect
(and
(Flag208-)
(Flag208prime-)
)

)
(:action Flag208Action-42
:parameters ()
:precondition
(and
(Flag192-)
(Flag192prime-)
)
:effect
(and
(Flag208-)
(Flag208prime-)
)

)
(:action Flag208Action-43
:parameters ()
:precondition
(and
(Flag207-)
(Flag207prime-)
)
:effect
(and
(Flag208-)
(Flag208prime-)
)

)
(:action Prim208Action
:parameters ()
:precondition
(and
(not (Flag190-))
(Flag190prime-)
(not (Flag144-))
(Flag144prime-)
(not (Flag191-))
(Flag191prime-)
(not (Flag145-))
(Flag145prime-)
(not (Flag176-))
(Flag176prime-)
(not (Flag147-))
(Flag147prime-)
(not (Flag180-))
(Flag180prime-)
(not (Flag149-))
(Flag149prime-)
(not (Flag193-))
(Flag193prime-)
(not (Flag151-))
(Flag151prime-)
(not (Flag152-))
(Flag152prime-)
(not (Flag170-))
(Flag170prime-)
(not (Flag153-))
(Flag153prime-)
(not (Flag201-))
(Flag201prime-)
(not (Flag202-))
(Flag202prime-)
(not (Flag195-))
(Flag195prime-)
(not (Flag196-))
(Flag196prime-)
(not (Flag197-))
(Flag197prime-)
(not (Flag156-))
(Flag156prime-)
(not (Flag168-))
(Flag168prime-)
(not (Flag171-))
(Flag171prime-)
(not (Flag172-))
(Flag172prime-)
(not (Flag157-))
(Flag157prime-)
(not (Flag182-))
(Flag182prime-)
(not (Flag198-))
(Flag198prime-)
(not (Flag173-))
(Flag173prime-)
(not (Flag203-))
(Flag203prime-)
(not (Flag159-))
(Flag159prime-)
(not (Flag175-))
(Flag175prime-)
(not (Flag183-))
(Flag183prime-)
(not (Flag204-))
(Flag204prime-)
(not (Flag205-))
(Flag205prime-)
(not (Flag206-))
(Flag206prime-)
(not (Flag184-))
(Flag184prime-)
(not (Flag160-))
(Flag160prime-)
(not (Flag185-))
(Flag185prime-)
(not (Flag186-))
(Flag186prime-)
(not (Flag161-))
(Flag161prime-)
(not (Flag164-))
(Flag164prime-)
(not (Flag199-))
(Flag199prime-)
(not (Flag188-))
(Flag188prime-)
(not (Flag165-))
(Flag165prime-)
(not (Flag192-))
(Flag192prime-)
(not (Flag207-))
(Flag207prime-)
)
:effect
(and
(Flag208prime-)
(not (Flag208-))
)

)
(:action Flag216Action-0
:parameters ()
:precondition
(and
(Flag190-)
(Flag190prime-)
)
:effect
(and
(Flag216-)
(Flag216prime-)
)

)
(:action Flag216Action-1
:parameters ()
:precondition
(and
(Flag144-)
(Flag144prime-)
)
:effect
(and
(Flag216-)
(Flag216prime-)
)

)
(:action Flag216Action-2
:parameters ()
:precondition
(and
(Flag191-)
(Flag191prime-)
)
:effect
(and
(Flag216-)
(Flag216prime-)
)

)
(:action Flag216Action-3
:parameters ()
:precondition
(and
(Flag145-)
(Flag145prime-)
)
:effect
(and
(Flag216-)
(Flag216prime-)
)

)
(:action Flag216Action-4
:parameters ()
:precondition
(and
(Flag209-)
(Flag209prime-)
)
:effect
(and
(Flag216-)
(Flag216prime-)
)

)
(:action Flag216Action-5
:parameters ()
:precondition
(and
(Flag185-)
(Flag185prime-)
)
:effect
(and
(Flag216-)
(Flag216prime-)
)

)
(:action Flag216Action-6
:parameters ()
:precondition
(and
(Flag210-)
(Flag210prime-)
)
:effect
(and
(Flag216-)
(Flag216prime-)
)

)
(:action Flag216Action-7
:parameters ()
:precondition
(and
(Flag211-)
(Flag211prime-)
)
:effect
(and
(Flag216-)
(Flag216prime-)
)

)
(:action Flag216Action-8
:parameters ()
:precondition
(and
(Flag176-)
(Flag176prime-)
)
:effect
(and
(Flag216-)
(Flag216prime-)
)

)
(:action Flag216Action-9
:parameters ()
:precondition
(and
(Flag147-)
(Flag147prime-)
)
:effect
(and
(Flag216-)
(Flag216prime-)
)

)
(:action Flag216Action-10
:parameters ()
:precondition
(and
(Flag168-)
(Flag168prime-)
)
:effect
(and
(Flag216-)
(Flag216prime-)
)

)
(:action Flag216Action-11
:parameters ()
:precondition
(and
(Flag180-)
(Flag180prime-)
)
:effect
(and
(Flag216-)
(Flag216prime-)
)

)
(:action Flag216Action-12
:parameters ()
:precondition
(and
(Flag193-)
(Flag193prime-)
)
:effect
(and
(Flag216-)
(Flag216prime-)
)

)
(:action Flag216Action-13
:parameters ()
:precondition
(and
(Flag157-)
(Flag157prime-)
)
:effect
(and
(Flag216-)
(Flag216prime-)
)

)
(:action Flag216Action-14
:parameters ()
:precondition
(and
(Flag170-)
(Flag170prime-)
)
:effect
(and
(Flag216-)
(Flag216prime-)
)

)
(:action Flag216Action-15
:parameters ()
:precondition
(and
(Flag153-)
(Flag153prime-)
)
:effect
(and
(Flag216-)
(Flag216prime-)
)

)
(:action Flag216Action-16
:parameters ()
:precondition
(and
(Flag201-)
(Flag201prime-)
)
:effect
(and
(Flag216-)
(Flag216prime-)
)

)
(:action Flag216Action-17
:parameters ()
:precondition
(and
(Flag202-)
(Flag202prime-)
)
:effect
(and
(Flag216-)
(Flag216prime-)
)

)
(:action Flag216Action-18
:parameters ()
:precondition
(and
(Flag195-)
(Flag195prime-)
)
:effect
(and
(Flag216-)
(Flag216prime-)
)

)
(:action Flag216Action-19
:parameters ()
:precondition
(and
(Flag196-)
(Flag196prime-)
)
:effect
(and
(Flag216-)
(Flag216prime-)
)

)
(:action Flag216Action-20
:parameters ()
:precondition
(and
(Flag197-)
(Flag197prime-)
)
:effect
(and
(Flag216-)
(Flag216prime-)
)

)
(:action Flag216Action-21
:parameters ()
:precondition
(and
(Flag156-)
(Flag156prime-)
)
:effect
(and
(Flag216-)
(Flag216prime-)
)

)
(:action Flag216Action-22
:parameters ()
:precondition
(and
(Flag212-)
(Flag212prime-)
)
:effect
(and
(Flag216-)
(Flag216prime-)
)

)
(:action Flag216Action-23
:parameters ()
:precondition
(and
(Flag171-)
(Flag171prime-)
)
:effect
(and
(Flag216-)
(Flag216prime-)
)

)
(:action Flag216Action-24
:parameters ()
:precondition
(and
(Flag172-)
(Flag172prime-)
)
:effect
(and
(Flag216-)
(Flag216prime-)
)

)
(:action Flag216Action-25
:parameters ()
:precondition
(and
(Flag151-)
(Flag151prime-)
)
:effect
(and
(Flag216-)
(Flag216prime-)
)

)
(:action Flag216Action-26
:parameters ()
:precondition
(and
(Flag182-)
(Flag182prime-)
)
:effect
(and
(Flag216-)
(Flag216prime-)
)

)
(:action Flag216Action-27
:parameters ()
:precondition
(and
(Flag198-)
(Flag198prime-)
)
:effect
(and
(Flag216-)
(Flag216prime-)
)

)
(:action Flag216Action-28
:parameters ()
:precondition
(and
(Flag159-)
(Flag159prime-)
)
:effect
(and
(Flag216-)
(Flag216prime-)
)

)
(:action Flag216Action-29
:parameters ()
:precondition
(and
(Flag175-)
(Flag175prime-)
)
:effect
(and
(Flag216-)
(Flag216prime-)
)

)
(:action Flag216Action-30
:parameters ()
:precondition
(and
(Flag183-)
(Flag183prime-)
)
:effect
(and
(Flag216-)
(Flag216prime-)
)

)
(:action Flag216Action-31
:parameters ()
:precondition
(and
(Flag204-)
(Flag204prime-)
)
:effect
(and
(Flag216-)
(Flag216prime-)
)

)
(:action Flag216Action-32
:parameters ()
:precondition
(and
(Flag213-)
(Flag213prime-)
)
:effect
(and
(Flag216-)
(Flag216prime-)
)

)
(:action Flag216Action-33
:parameters ()
:precondition
(and
(Flag214-)
(Flag214prime-)
)
:effect
(and
(Flag216-)
(Flag216prime-)
)

)
(:action Flag216Action-34
:parameters ()
:precondition
(and
(Flag205-)
(Flag205prime-)
)
:effect
(and
(Flag216-)
(Flag216prime-)
)

)
(:action Flag216Action-35
:parameters ()
:precondition
(and
(Flag184-)
(Flag184prime-)
)
:effect
(and
(Flag216-)
(Flag216prime-)
)

)
(:action Flag216Action-36
:parameters ()
:precondition
(and
(Flag160-)
(Flag160prime-)
)
:effect
(and
(Flag216-)
(Flag216prime-)
)

)
(:action Flag216Action-37
:parameters ()
:precondition
(and
(Flag215-)
(Flag215prime-)
)
:effect
(and
(Flag216-)
(Flag216prime-)
)

)
(:action Flag216Action-38
:parameters ()
:precondition
(and
(Flag161-)
(Flag161prime-)
)
:effect
(and
(Flag216-)
(Flag216prime-)
)

)
(:action Flag216Action-39
:parameters ()
:precondition
(and
(Flag164-)
(Flag164prime-)
)
:effect
(and
(Flag216-)
(Flag216prime-)
)

)
(:action Flag216Action-40
:parameters ()
:precondition
(and
(Flag188-)
(Flag188prime-)
)
:effect
(and
(Flag216-)
(Flag216prime-)
)

)
(:action Flag216Action-41
:parameters ()
:precondition
(and
(Flag165-)
(Flag165prime-)
)
:effect
(and
(Flag216-)
(Flag216prime-)
)

)
(:action Flag216Action-42
:parameters ()
:precondition
(and
(Flag192-)
(Flag192prime-)
)
:effect
(and
(Flag216-)
(Flag216prime-)
)

)
(:action Flag216Action-43
:parameters ()
:precondition
(and
(Flag207-)
(Flag207prime-)
)
:effect
(and
(Flag216-)
(Flag216prime-)
)

)
(:action Prim216Action
:parameters ()
:precondition
(and
(not (Flag190-))
(Flag190prime-)
(not (Flag144-))
(Flag144prime-)
(not (Flag191-))
(Flag191prime-)
(not (Flag145-))
(Flag145prime-)
(not (Flag209-))
(Flag209prime-)
(not (Flag185-))
(Flag185prime-)
(not (Flag210-))
(Flag210prime-)
(not (Flag211-))
(Flag211prime-)
(not (Flag176-))
(Flag176prime-)
(not (Flag147-))
(Flag147prime-)
(not (Flag168-))
(Flag168prime-)
(not (Flag180-))
(Flag180prime-)
(not (Flag193-))
(Flag193prime-)
(not (Flag157-))
(Flag157prime-)
(not (Flag170-))
(Flag170prime-)
(not (Flag153-))
(Flag153prime-)
(not (Flag201-))
(Flag201prime-)
(not (Flag202-))
(Flag202prime-)
(not (Flag195-))
(Flag195prime-)
(not (Flag196-))
(Flag196prime-)
(not (Flag197-))
(Flag197prime-)
(not (Flag156-))
(Flag156prime-)
(not (Flag212-))
(Flag212prime-)
(not (Flag171-))
(Flag171prime-)
(not (Flag172-))
(Flag172prime-)
(not (Flag151-))
(Flag151prime-)
(not (Flag182-))
(Flag182prime-)
(not (Flag198-))
(Flag198prime-)
(not (Flag159-))
(Flag159prime-)
(not (Flag175-))
(Flag175prime-)
(not (Flag183-))
(Flag183prime-)
(not (Flag204-))
(Flag204prime-)
(not (Flag213-))
(Flag213prime-)
(not (Flag214-))
(Flag214prime-)
(not (Flag205-))
(Flag205prime-)
(not (Flag184-))
(Flag184prime-)
(not (Flag160-))
(Flag160prime-)
(not (Flag215-))
(Flag215prime-)
(not (Flag161-))
(Flag161prime-)
(not (Flag164-))
(Flag164prime-)
(not (Flag188-))
(Flag188prime-)
(not (Flag165-))
(Flag165prime-)
(not (Flag192-))
(Flag192prime-)
(not (Flag207-))
(Flag207prime-)
)
:effect
(and
(Flag216prime-)
(not (Flag216-))
)

)
(:action Flag223Action-0
:parameters ()
:precondition
(and
(Flag190-)
(Flag190prime-)
)
:effect
(and
(Flag223-)
(Flag223prime-)
)

)
(:action Flag223Action-1
:parameters ()
:precondition
(and
(Flag217-)
(Flag217prime-)
)
:effect
(and
(Flag223-)
(Flag223prime-)
)

)
(:action Flag223Action-2
:parameters ()
:precondition
(and
(Flag191-)
(Flag191prime-)
)
:effect
(and
(Flag223-)
(Flag223prime-)
)

)
(:action Flag223Action-3
:parameters ()
:precondition
(and
(Flag145-)
(Flag145prime-)
)
:effect
(and
(Flag223-)
(Flag223prime-)
)

)
(:action Flag223Action-4
:parameters ()
:precondition
(and
(Flag197-)
(Flag197prime-)
)
:effect
(and
(Flag223-)
(Flag223prime-)
)

)
(:action Flag223Action-5
:parameters ()
:precondition
(and
(Flag209-)
(Flag209prime-)
)
:effect
(and
(Flag223-)
(Flag223prime-)
)

)
(:action Flag223Action-6
:parameters ()
:precondition
(and
(Flag211-)
(Flag211prime-)
)
:effect
(and
(Flag223-)
(Flag223prime-)
)

)
(:action Flag223Action-7
:parameters ()
:precondition
(and
(Flag218-)
(Flag218prime-)
)
:effect
(and
(Flag223-)
(Flag223prime-)
)

)
(:action Flag223Action-8
:parameters ()
:precondition
(and
(Flag147-)
(Flag147prime-)
)
:effect
(and
(Flag223-)
(Flag223prime-)
)

)
(:action Flag223Action-9
:parameters ()
:precondition
(and
(Flag192-)
(Flag192prime-)
)
:effect
(and
(Flag223-)
(Flag223prime-)
)

)
(:action Flag223Action-10
:parameters ()
:precondition
(and
(Flag180-)
(Flag180prime-)
)
:effect
(and
(Flag223-)
(Flag223prime-)
)

)
(:action Flag223Action-11
:parameters ()
:precondition
(and
(Flag193-)
(Flag193prime-)
)
:effect
(and
(Flag223-)
(Flag223prime-)
)

)
(:action Flag223Action-12
:parameters ()
:precondition
(and
(Flag157-)
(Flag157prime-)
)
:effect
(and
(Flag223-)
(Flag223prime-)
)

)
(:action Flag223Action-13
:parameters ()
:precondition
(and
(Flag144-)
(Flag144prime-)
)
:effect
(and
(Flag223-)
(Flag223prime-)
)

)
(:action Flag223Action-14
:parameters ()
:precondition
(and
(Flag170-)
(Flag170prime-)
)
:effect
(and
(Flag223-)
(Flag223prime-)
)

)
(:action Flag223Action-15
:parameters ()
:precondition
(and
(Flag153-)
(Flag153prime-)
)
:effect
(and
(Flag223-)
(Flag223prime-)
)

)
(:action Flag223Action-16
:parameters ()
:precondition
(and
(Flag201-)
(Flag201prime-)
)
:effect
(and
(Flag223-)
(Flag223prime-)
)

)
(:action Flag223Action-17
:parameters ()
:precondition
(and
(Flag202-)
(Flag202prime-)
)
:effect
(and
(Flag223-)
(Flag223prime-)
)

)
(:action Flag223Action-18
:parameters ()
:precondition
(and
(Flag195-)
(Flag195prime-)
)
:effect
(and
(Flag223-)
(Flag223prime-)
)

)
(:action Flag223Action-19
:parameters ()
:precondition
(and
(Flag219-)
(Flag219prime-)
)
:effect
(and
(Flag223-)
(Flag223prime-)
)

)
(:action Flag223Action-20
:parameters ()
:precondition
(and
(Flag220-)
(Flag220prime-)
)
:effect
(and
(Flag223-)
(Flag223prime-)
)

)
(:action Flag223Action-21
:parameters ()
:precondition
(and
(Flag221-)
(Flag221prime-)
)
:effect
(and
(Flag223-)
(Flag223prime-)
)

)
(:action Flag223Action-22
:parameters ()
:precondition
(and
(Flag156-)
(Flag156prime-)
)
:effect
(and
(Flag223-)
(Flag223prime-)
)

)
(:action Flag223Action-23
:parameters ()
:precondition
(and
(Flag171-)
(Flag171prime-)
)
:effect
(and
(Flag223-)
(Flag223prime-)
)

)
(:action Flag223Action-24
:parameters ()
:precondition
(and
(Flag172-)
(Flag172prime-)
)
:effect
(and
(Flag223-)
(Flag223prime-)
)

)
(:action Flag223Action-25
:parameters ()
:precondition
(and
(Flag151-)
(Flag151prime-)
)
:effect
(and
(Flag223-)
(Flag223prime-)
)

)
(:action Flag223Action-26
:parameters ()
:precondition
(and
(Flag182-)
(Flag182prime-)
)
:effect
(and
(Flag223-)
(Flag223prime-)
)

)
(:action Flag223Action-27
:parameters ()
:precondition
(and
(Flag198-)
(Flag198prime-)
)
:effect
(and
(Flag223-)
(Flag223prime-)
)

)
(:action Flag223Action-28
:parameters ()
:precondition
(and
(Flag159-)
(Flag159prime-)
)
:effect
(and
(Flag223-)
(Flag223prime-)
)

)
(:action Flag223Action-29
:parameters ()
:precondition
(and
(Flag175-)
(Flag175prime-)
)
:effect
(and
(Flag223-)
(Flag223prime-)
)

)
(:action Flag223Action-30
:parameters ()
:precondition
(and
(Flag183-)
(Flag183prime-)
)
:effect
(and
(Flag223-)
(Flag223prime-)
)

)
(:action Flag223Action-31
:parameters ()
:precondition
(and
(Flag204-)
(Flag204prime-)
)
:effect
(and
(Flag223-)
(Flag223prime-)
)

)
(:action Flag223Action-32
:parameters ()
:precondition
(and
(Flag213-)
(Flag213prime-)
)
:effect
(and
(Flag223-)
(Flag223prime-)
)

)
(:action Flag223Action-33
:parameters ()
:precondition
(and
(Flag205-)
(Flag205prime-)
)
:effect
(and
(Flag223-)
(Flag223prime-)
)

)
(:action Flag223Action-34
:parameters ()
:precondition
(and
(Flag184-)
(Flag184prime-)
)
:effect
(and
(Flag223-)
(Flag223prime-)
)

)
(:action Flag223Action-35
:parameters ()
:precondition
(and
(Flag160-)
(Flag160prime-)
)
:effect
(and
(Flag223-)
(Flag223prime-)
)

)
(:action Flag223Action-36
:parameters ()
:precondition
(and
(Flag215-)
(Flag215prime-)
)
:effect
(and
(Flag223-)
(Flag223prime-)
)

)
(:action Flag223Action-37
:parameters ()
:precondition
(and
(Flag161-)
(Flag161prime-)
)
:effect
(and
(Flag223-)
(Flag223prime-)
)

)
(:action Flag223Action-38
:parameters ()
:precondition
(and
(Flag222-)
(Flag222prime-)
)
:effect
(and
(Flag223-)
(Flag223prime-)
)

)
(:action Flag223Action-39
:parameters ()
:precondition
(and
(Flag188-)
(Flag188prime-)
)
:effect
(and
(Flag223-)
(Flag223prime-)
)

)
(:action Flag223Action-40
:parameters ()
:precondition
(and
(Flag176-)
(Flag176prime-)
)
:effect
(and
(Flag223-)
(Flag223prime-)
)

)
(:action Flag223Action-41
:parameters ()
:precondition
(and
(Flag207-)
(Flag207prime-)
)
:effect
(and
(Flag223-)
(Flag223prime-)
)

)
(:action Prim223Action
:parameters ()
:precondition
(and
(not (Flag190-))
(Flag190prime-)
(not (Flag217-))
(Flag217prime-)
(not (Flag191-))
(Flag191prime-)
(not (Flag145-))
(Flag145prime-)
(not (Flag197-))
(Flag197prime-)
(not (Flag209-))
(Flag209prime-)
(not (Flag211-))
(Flag211prime-)
(not (Flag218-))
(Flag218prime-)
(not (Flag147-))
(Flag147prime-)
(not (Flag192-))
(Flag192prime-)
(not (Flag180-))
(Flag180prime-)
(not (Flag193-))
(Flag193prime-)
(not (Flag157-))
(Flag157prime-)
(not (Flag144-))
(Flag144prime-)
(not (Flag170-))
(Flag170prime-)
(not (Flag153-))
(Flag153prime-)
(not (Flag201-))
(Flag201prime-)
(not (Flag202-))
(Flag202prime-)
(not (Flag195-))
(Flag195prime-)
(not (Flag219-))
(Flag219prime-)
(not (Flag220-))
(Flag220prime-)
(not (Flag221-))
(Flag221prime-)
(not (Flag156-))
(Flag156prime-)
(not (Flag171-))
(Flag171prime-)
(not (Flag172-))
(Flag172prime-)
(not (Flag151-))
(Flag151prime-)
(not (Flag182-))
(Flag182prime-)
(not (Flag198-))
(Flag198prime-)
(not (Flag159-))
(Flag159prime-)
(not (Flag175-))
(Flag175prime-)
(not (Flag183-))
(Flag183prime-)
(not (Flag204-))
(Flag204prime-)
(not (Flag213-))
(Flag213prime-)
(not (Flag205-))
(Flag205prime-)
(not (Flag184-))
(Flag184prime-)
(not (Flag160-))
(Flag160prime-)
(not (Flag215-))
(Flag215prime-)
(not (Flag161-))
(Flag161prime-)
(not (Flag222-))
(Flag222prime-)
(not (Flag188-))
(Flag188prime-)
(not (Flag176-))
(Flag176prime-)
(not (Flag207-))
(Flag207prime-)
)
:effect
(and
(Flag223prime-)
(not (Flag223-))
)

)
(:action Flag228Action-0
:parameters ()
:precondition
(and
(Flag190-)
(Flag190prime-)
)
:effect
(and
(Flag228-)
(Flag228prime-)
)

)
(:action Flag228Action-1
:parameters ()
:precondition
(and
(Flag144-)
(Flag144prime-)
)
:effect
(and
(Flag228-)
(Flag228prime-)
)

)
(:action Flag228Action-2
:parameters ()
:precondition
(and
(Flag145-)
(Flag145prime-)
)
:effect
(and
(Flag228-)
(Flag228prime-)
)

)
(:action Flag228Action-3
:parameters ()
:precondition
(and
(Flag220-)
(Flag220prime-)
)
:effect
(and
(Flag228-)
(Flag228prime-)
)

)
(:action Flag228Action-4
:parameters ()
:precondition
(and
(Flag209-)
(Flag209prime-)
)
:effect
(and
(Flag228-)
(Flag228prime-)
)

)
(:action Flag228Action-5
:parameters ()
:precondition
(and
(Flag211-)
(Flag211prime-)
)
:effect
(and
(Flag228-)
(Flag228prime-)
)

)
(:action Flag228Action-6
:parameters ()
:precondition
(and
(Flag218-)
(Flag218prime-)
)
:effect
(and
(Flag228-)
(Flag228prime-)
)

)
(:action Flag228Action-7
:parameters ()
:precondition
(and
(Flag147-)
(Flag147prime-)
)
:effect
(and
(Flag228-)
(Flag228prime-)
)

)
(:action Flag228Action-8
:parameters ()
:precondition
(and
(Flag192-)
(Flag192prime-)
)
:effect
(and
(Flag228-)
(Flag228prime-)
)

)
(:action Flag228Action-9
:parameters ()
:precondition
(and
(Flag224-)
(Flag224prime-)
)
:effect
(and
(Flag228-)
(Flag228prime-)
)

)
(:action Flag228Action-10
:parameters ()
:precondition
(and
(Flag193-)
(Flag193prime-)
)
:effect
(and
(Flag228-)
(Flag228prime-)
)

)
(:action Flag228Action-11
:parameters ()
:precondition
(and
(Flag225-)
(Flag225prime-)
)
:effect
(and
(Flag228-)
(Flag228prime-)
)

)
(:action Flag228Action-12
:parameters ()
:precondition
(and
(Flag201-)
(Flag201prime-)
)
:effect
(and
(Flag228-)
(Flag228prime-)
)

)
(:action Flag228Action-13
:parameters ()
:precondition
(and
(Flag153-)
(Flag153prime-)
)
:effect
(and
(Flag228-)
(Flag228prime-)
)

)
(:action Flag228Action-14
:parameters ()
:precondition
(and
(Flag202-)
(Flag202prime-)
)
:effect
(and
(Flag228-)
(Flag228prime-)
)

)
(:action Flag228Action-15
:parameters ()
:precondition
(and
(Flag195-)
(Flag195prime-)
)
:effect
(and
(Flag228-)
(Flag228prime-)
)

)
(:action Flag228Action-16
:parameters ()
:precondition
(and
(Flag226-)
(Flag226prime-)
)
:effect
(and
(Flag228-)
(Flag228prime-)
)

)
(:action Flag228Action-17
:parameters ()
:precondition
(and
(Flag219-)
(Flag219prime-)
)
:effect
(and
(Flag228-)
(Flag228prime-)
)

)
(:action Flag228Action-18
:parameters ()
:precondition
(and
(Flag197-)
(Flag197prime-)
)
:effect
(and
(Flag228-)
(Flag228prime-)
)

)
(:action Flag228Action-19
:parameters ()
:precondition
(and
(Flag156-)
(Flag156prime-)
)
:effect
(and
(Flag228-)
(Flag228prime-)
)

)
(:action Flag228Action-20
:parameters ()
:precondition
(and
(Flag171-)
(Flag171prime-)
)
:effect
(and
(Flag228-)
(Flag228prime-)
)

)
(:action Flag228Action-21
:parameters ()
:precondition
(and
(Flag172-)
(Flag172prime-)
)
:effect
(and
(Flag228-)
(Flag228prime-)
)

)
(:action Flag228Action-22
:parameters ()
:precondition
(and
(Flag227-)
(Flag227prime-)
)
:effect
(and
(Flag228-)
(Flag228prime-)
)

)
(:action Flag228Action-23
:parameters ()
:precondition
(and
(Flag198-)
(Flag198prime-)
)
:effect
(and
(Flag228-)
(Flag228prime-)
)

)
(:action Flag228Action-24
:parameters ()
:precondition
(and
(Flag159-)
(Flag159prime-)
)
:effect
(and
(Flag228-)
(Flag228prime-)
)

)
(:action Flag228Action-25
:parameters ()
:precondition
(and
(Flag175-)
(Flag175prime-)
)
:effect
(and
(Flag228-)
(Flag228prime-)
)

)
(:action Flag228Action-26
:parameters ()
:precondition
(and
(Flag183-)
(Flag183prime-)
)
:effect
(and
(Flag228-)
(Flag228prime-)
)

)
(:action Flag228Action-27
:parameters ()
:precondition
(and
(Flag213-)
(Flag213prime-)
)
:effect
(and
(Flag228-)
(Flag228prime-)
)

)
(:action Flag228Action-28
:parameters ()
:precondition
(and
(Flag205-)
(Flag205prime-)
)
:effect
(and
(Flag228-)
(Flag228prime-)
)

)
(:action Flag228Action-29
:parameters ()
:precondition
(and
(Flag184-)
(Flag184prime-)
)
:effect
(and
(Flag228-)
(Flag228prime-)
)

)
(:action Flag228Action-30
:parameters ()
:precondition
(and
(Flag160-)
(Flag160prime-)
)
:effect
(and
(Flag228-)
(Flag228prime-)
)

)
(:action Flag228Action-31
:parameters ()
:precondition
(and
(Flag215-)
(Flag215prime-)
)
:effect
(and
(Flag228-)
(Flag228prime-)
)

)
(:action Flag228Action-32
:parameters ()
:precondition
(and
(Flag161-)
(Flag161prime-)
)
:effect
(and
(Flag228-)
(Flag228prime-)
)

)
(:action Flag228Action-33
:parameters ()
:precondition
(and
(Flag222-)
(Flag222prime-)
)
:effect
(and
(Flag228-)
(Flag228prime-)
)

)
(:action Flag228Action-34
:parameters ()
:precondition
(and
(Flag188-)
(Flag188prime-)
)
:effect
(and
(Flag228-)
(Flag228prime-)
)

)
(:action Flag228Action-35
:parameters ()
:precondition
(and
(Flag176-)
(Flag176prime-)
)
:effect
(and
(Flag228-)
(Flag228prime-)
)

)
(:action Flag228Action-36
:parameters ()
:precondition
(and
(Flag207-)
(Flag207prime-)
)
:effect
(and
(Flag228-)
(Flag228prime-)
)

)
(:action Prim228Action
:parameters ()
:precondition
(and
(not (Flag190-))
(Flag190prime-)
(not (Flag144-))
(Flag144prime-)
(not (Flag145-))
(Flag145prime-)
(not (Flag220-))
(Flag220prime-)
(not (Flag209-))
(Flag209prime-)
(not (Flag211-))
(Flag211prime-)
(not (Flag218-))
(Flag218prime-)
(not (Flag147-))
(Flag147prime-)
(not (Flag192-))
(Flag192prime-)
(not (Flag224-))
(Flag224prime-)
(not (Flag193-))
(Flag193prime-)
(not (Flag225-))
(Flag225prime-)
(not (Flag201-))
(Flag201prime-)
(not (Flag153-))
(Flag153prime-)
(not (Flag202-))
(Flag202prime-)
(not (Flag195-))
(Flag195prime-)
(not (Flag226-))
(Flag226prime-)
(not (Flag219-))
(Flag219prime-)
(not (Flag197-))
(Flag197prime-)
(not (Flag156-))
(Flag156prime-)
(not (Flag171-))
(Flag171prime-)
(not (Flag172-))
(Flag172prime-)
(not (Flag227-))
(Flag227prime-)
(not (Flag198-))
(Flag198prime-)
(not (Flag159-))
(Flag159prime-)
(not (Flag175-))
(Flag175prime-)
(not (Flag183-))
(Flag183prime-)
(not (Flag213-))
(Flag213prime-)
(not (Flag205-))
(Flag205prime-)
(not (Flag184-))
(Flag184prime-)
(not (Flag160-))
(Flag160prime-)
(not (Flag215-))
(Flag215prime-)
(not (Flag161-))
(Flag161prime-)
(not (Flag222-))
(Flag222prime-)
(not (Flag188-))
(Flag188prime-)
(not (Flag176-))
(Flag176prime-)
(not (Flag207-))
(Flag207prime-)
)
:effect
(and
(Flag228prime-)
(not (Flag228-))
)

)
(:action Flag232Action-0
:parameters ()
:precondition
(and
(Flag190-)
(Flag190prime-)
)
:effect
(and
(Flag232-)
(Flag232prime-)
)

)
(:action Flag232Action-1
:parameters ()
:precondition
(and
(Flag144-)
(Flag144prime-)
)
:effect
(and
(Flag232-)
(Flag232prime-)
)

)
(:action Flag232Action-2
:parameters ()
:precondition
(and
(Flag145-)
(Flag145prime-)
)
:effect
(and
(Flag232-)
(Flag232prime-)
)

)
(:action Flag232Action-3
:parameters ()
:precondition
(and
(Flag209-)
(Flag209prime-)
)
:effect
(and
(Flag232-)
(Flag232prime-)
)

)
(:action Flag232Action-4
:parameters ()
:precondition
(and
(Flag215-)
(Flag215prime-)
)
:effect
(and
(Flag232-)
(Flag232prime-)
)

)
(:action Flag232Action-5
:parameters ()
:precondition
(and
(Flag229-)
(Flag229prime-)
)
:effect
(and
(Flag232-)
(Flag232prime-)
)

)
(:action Flag232Action-6
:parameters ()
:precondition
(and
(Flag230-)
(Flag230prime-)
)
:effect
(and
(Flag232-)
(Flag232prime-)
)

)
(:action Flag232Action-7
:parameters ()
:precondition
(and
(Flag218-)
(Flag218prime-)
)
:effect
(and
(Flag232-)
(Flag232prime-)
)

)
(:action Flag232Action-8
:parameters ()
:precondition
(and
(Flag147-)
(Flag147prime-)
)
:effect
(and
(Flag232-)
(Flag232prime-)
)

)
(:action Flag232Action-9
:parameters ()
:precondition
(and
(Flag225-)
(Flag225prime-)
)
:effect
(and
(Flag232-)
(Flag232prime-)
)

)
(:action Flag232Action-10
:parameters ()
:precondition
(and
(Flag153-)
(Flag153prime-)
)
:effect
(and
(Flag232-)
(Flag232prime-)
)

)
(:action Flag232Action-11
:parameters ()
:precondition
(and
(Flag202-)
(Flag202prime-)
)
:effect
(and
(Flag232-)
(Flag232prime-)
)

)
(:action Flag232Action-12
:parameters ()
:precondition
(and
(Flag226-)
(Flag226prime-)
)
:effect
(and
(Flag232-)
(Flag232prime-)
)

)
(:action Flag232Action-13
:parameters ()
:precondition
(and
(Flag219-)
(Flag219prime-)
)
:effect
(and
(Flag232-)
(Flag232prime-)
)

)
(:action Flag232Action-14
:parameters ()
:precondition
(and
(Flag197-)
(Flag197prime-)
)
:effect
(and
(Flag232-)
(Flag232prime-)
)

)
(:action Flag232Action-15
:parameters ()
:precondition
(and
(Flag156-)
(Flag156prime-)
)
:effect
(and
(Flag232-)
(Flag232prime-)
)

)
(:action Flag232Action-16
:parameters ()
:precondition
(and
(Flag171-)
(Flag171prime-)
)
:effect
(and
(Flag232-)
(Flag232prime-)
)

)
(:action Flag232Action-17
:parameters ()
:precondition
(and
(Flag193-)
(Flag193prime-)
)
:effect
(and
(Flag232-)
(Flag232prime-)
)

)
(:action Flag232Action-18
:parameters ()
:precondition
(and
(Flag231-)
(Flag231prime-)
)
:effect
(and
(Flag232-)
(Flag232prime-)
)

)
(:action Flag232Action-19
:parameters ()
:precondition
(and
(Flag227-)
(Flag227prime-)
)
:effect
(and
(Flag232-)
(Flag232prime-)
)

)
(:action Flag232Action-20
:parameters ()
:precondition
(and
(Flag175-)
(Flag175prime-)
)
:effect
(and
(Flag232-)
(Flag232prime-)
)

)
(:action Flag232Action-21
:parameters ()
:precondition
(and
(Flag183-)
(Flag183prime-)
)
:effect
(and
(Flag232-)
(Flag232prime-)
)

)
(:action Flag232Action-22
:parameters ()
:precondition
(and
(Flag213-)
(Flag213prime-)
)
:effect
(and
(Flag232-)
(Flag232prime-)
)

)
(:action Flag232Action-23
:parameters ()
:precondition
(and
(Flag205-)
(Flag205prime-)
)
:effect
(and
(Flag232-)
(Flag232prime-)
)

)
(:action Flag232Action-24
:parameters ()
:precondition
(and
(Flag184-)
(Flag184prime-)
)
:effect
(and
(Flag232-)
(Flag232prime-)
)

)
(:action Flag232Action-25
:parameters ()
:precondition
(and
(Flag160-)
(Flag160prime-)
)
:effect
(and
(Flag232-)
(Flag232prime-)
)

)
(:action Flag232Action-26
:parameters ()
:precondition
(and
(Flag176-)
(Flag176prime-)
)
:effect
(and
(Flag232-)
(Flag232prime-)
)

)
(:action Flag232Action-27
:parameters ()
:precondition
(and
(Flag222-)
(Flag222prime-)
)
:effect
(and
(Flag232-)
(Flag232prime-)
)

)
(:action Flag232Action-28
:parameters ()
:precondition
(and
(Flag188-)
(Flag188prime-)
)
:effect
(and
(Flag232-)
(Flag232prime-)
)

)
(:action Flag232Action-29
:parameters ()
:precondition
(and
(Flag192-)
(Flag192prime-)
)
:effect
(and
(Flag232-)
(Flag232prime-)
)

)
(:action Flag232Action-30
:parameters ()
:precondition
(and
(Flag207-)
(Flag207prime-)
)
:effect
(and
(Flag232-)
(Flag232prime-)
)

)
(:action Prim232Action
:parameters ()
:precondition
(and
(not (Flag190-))
(Flag190prime-)
(not (Flag144-))
(Flag144prime-)
(not (Flag145-))
(Flag145prime-)
(not (Flag209-))
(Flag209prime-)
(not (Flag215-))
(Flag215prime-)
(not (Flag229-))
(Flag229prime-)
(not (Flag230-))
(Flag230prime-)
(not (Flag218-))
(Flag218prime-)
(not (Flag147-))
(Flag147prime-)
(not (Flag225-))
(Flag225prime-)
(not (Flag153-))
(Flag153prime-)
(not (Flag202-))
(Flag202prime-)
(not (Flag226-))
(Flag226prime-)
(not (Flag219-))
(Flag219prime-)
(not (Flag197-))
(Flag197prime-)
(not (Flag156-))
(Flag156prime-)
(not (Flag171-))
(Flag171prime-)
(not (Flag193-))
(Flag193prime-)
(not (Flag231-))
(Flag231prime-)
(not (Flag227-))
(Flag227prime-)
(not (Flag175-))
(Flag175prime-)
(not (Flag183-))
(Flag183prime-)
(not (Flag213-))
(Flag213prime-)
(not (Flag205-))
(Flag205prime-)
(not (Flag184-))
(Flag184prime-)
(not (Flag160-))
(Flag160prime-)
(not (Flag176-))
(Flag176prime-)
(not (Flag222-))
(Flag222prime-)
(not (Flag188-))
(Flag188prime-)
(not (Flag192-))
(Flag192prime-)
(not (Flag207-))
(Flag207prime-)
)
:effect
(and
(Flag232prime-)
(not (Flag232-))
)

)
(:action Flag235Action-0
:parameters ()
:precondition
(and
(Flag209-)
(Flag209prime-)
)
:effect
(and
(Flag235-)
(Flag235prime-)
)

)
(:action Flag235Action-1
:parameters ()
:precondition
(and
(Flag233-)
(Flag233prime-)
)
:effect
(and
(Flag235-)
(Flag235prime-)
)

)
(:action Flag235Action-2
:parameters ()
:precondition
(and
(Flag229-)
(Flag229prime-)
)
:effect
(and
(Flag235-)
(Flag235prime-)
)

)
(:action Flag235Action-3
:parameters ()
:precondition
(and
(Flag230-)
(Flag230prime-)
)
:effect
(and
(Flag235-)
(Flag235prime-)
)

)
(:action Flag235Action-4
:parameters ()
:precondition
(and
(Flag147-)
(Flag147prime-)
)
:effect
(and
(Flag235-)
(Flag235prime-)
)

)
(:action Flag235Action-5
:parameters ()
:precondition
(and
(Flag153-)
(Flag153prime-)
)
:effect
(and
(Flag235-)
(Flag235prime-)
)

)
(:action Flag235Action-6
:parameters ()
:precondition
(and
(Flag202-)
(Flag202prime-)
)
:effect
(and
(Flag235-)
(Flag235prime-)
)

)
(:action Flag235Action-7
:parameters ()
:precondition
(and
(Flag226-)
(Flag226prime-)
)
:effect
(and
(Flag235-)
(Flag235prime-)
)

)
(:action Flag235Action-8
:parameters ()
:precondition
(and
(Flag219-)
(Flag219prime-)
)
:effect
(and
(Flag235-)
(Flag235prime-)
)

)
(:action Flag235Action-9
:parameters ()
:precondition
(and
(Flag197-)
(Flag197prime-)
)
:effect
(and
(Flag235-)
(Flag235prime-)
)

)
(:action Flag235Action-10
:parameters ()
:precondition
(and
(Flag156-)
(Flag156prime-)
)
:effect
(and
(Flag235-)
(Flag235prime-)
)

)
(:action Flag235Action-11
:parameters ()
:precondition
(and
(Flag171-)
(Flag171prime-)
)
:effect
(and
(Flag235-)
(Flag235prime-)
)

)
(:action Flag235Action-12
:parameters ()
:precondition
(and
(Flag193-)
(Flag193prime-)
)
:effect
(and
(Flag235-)
(Flag235prime-)
)

)
(:action Flag235Action-13
:parameters ()
:precondition
(and
(Flag227-)
(Flag227prime-)
)
:effect
(and
(Flag235-)
(Flag235prime-)
)

)
(:action Flag235Action-14
:parameters ()
:precondition
(and
(Flag175-)
(Flag175prime-)
)
:effect
(and
(Flag235-)
(Flag235prime-)
)

)
(:action Flag235Action-15
:parameters ()
:precondition
(and
(Flag205-)
(Flag205prime-)
)
:effect
(and
(Flag235-)
(Flag235prime-)
)

)
(:action Flag235Action-16
:parameters ()
:precondition
(and
(Flag160-)
(Flag160prime-)
)
:effect
(and
(Flag235-)
(Flag235prime-)
)

)
(:action Flag235Action-17
:parameters ()
:precondition
(and
(Flag215-)
(Flag215prime-)
)
:effect
(and
(Flag235-)
(Flag235prime-)
)

)
(:action Flag235Action-18
:parameters ()
:precondition
(and
(Flag234-)
(Flag234prime-)
)
:effect
(and
(Flag235-)
(Flag235prime-)
)

)
(:action Flag235Action-19
:parameters ()
:precondition
(and
(Flag222-)
(Flag222prime-)
)
:effect
(and
(Flag235-)
(Flag235prime-)
)

)
(:action Flag235Action-20
:parameters ()
:precondition
(and
(Flag188-)
(Flag188prime-)
)
:effect
(and
(Flag235-)
(Flag235prime-)
)

)
(:action Flag235Action-21
:parameters ()
:precondition
(and
(Flag192-)
(Flag192prime-)
)
:effect
(and
(Flag235-)
(Flag235prime-)
)

)
(:action Prim235Action
:parameters ()
:precondition
(and
(not (Flag209-))
(Flag209prime-)
(not (Flag233-))
(Flag233prime-)
(not (Flag229-))
(Flag229prime-)
(not (Flag230-))
(Flag230prime-)
(not (Flag147-))
(Flag147prime-)
(not (Flag153-))
(Flag153prime-)
(not (Flag202-))
(Flag202prime-)
(not (Flag226-))
(Flag226prime-)
(not (Flag219-))
(Flag219prime-)
(not (Flag197-))
(Flag197prime-)
(not (Flag156-))
(Flag156prime-)
(not (Flag171-))
(Flag171prime-)
(not (Flag193-))
(Flag193prime-)
(not (Flag227-))
(Flag227prime-)
(not (Flag175-))
(Flag175prime-)
(not (Flag205-))
(Flag205prime-)
(not (Flag160-))
(Flag160prime-)
(not (Flag215-))
(Flag215prime-)
(not (Flag234-))
(Flag234prime-)
(not (Flag222-))
(Flag222prime-)
(not (Flag188-))
(Flag188prime-)
(not (Flag192-))
(Flag192prime-)
)
:effect
(and
(Flag235prime-)
(not (Flag235-))
)

)
(:action Flag237Action-0
:parameters ()
:precondition
(and
(Flag160-)
(Flag160prime-)
)
:effect
(and
(Flag237-)
(Flag237prime-)
)

)
(:action Flag237Action-1
:parameters ()
:precondition
(and
(Flag209-)
(Flag209prime-)
)
:effect
(and
(Flag237-)
(Flag237prime-)
)

)
(:action Flag237Action-2
:parameters ()
:precondition
(and
(Flag233-)
(Flag233prime-)
)
:effect
(and
(Flag237-)
(Flag237prime-)
)

)
(:action Flag237Action-3
:parameters ()
:precondition
(and
(Flag227-)
(Flag227prime-)
)
:effect
(and
(Flag237-)
(Flag237prime-)
)

)
(:action Flag237Action-4
:parameters ()
:precondition
(and
(Flag229-)
(Flag229prime-)
)
:effect
(and
(Flag237-)
(Flag237prime-)
)

)
(:action Flag237Action-5
:parameters ()
:precondition
(and
(Flag222-)
(Flag222prime-)
)
:effect
(and
(Flag237-)
(Flag237prime-)
)

)
(:action Flag237Action-6
:parameters ()
:precondition
(and
(Flag153-)
(Flag153prime-)
)
:effect
(and
(Flag237-)
(Flag237prime-)
)

)
(:action Flag237Action-7
:parameters ()
:precondition
(and
(Flag175-)
(Flag175prime-)
)
:effect
(and
(Flag237-)
(Flag237prime-)
)

)
(:action Flag237Action-8
:parameters ()
:precondition
(and
(Flag202-)
(Flag202prime-)
)
:effect
(and
(Flag237-)
(Flag237prime-)
)

)
(:action Flag237Action-9
:parameters ()
:precondition
(and
(Flag188-)
(Flag188prime-)
)
:effect
(and
(Flag237-)
(Flag237prime-)
)

)
(:action Flag237Action-10
:parameters ()
:precondition
(and
(Flag192-)
(Flag192prime-)
)
:effect
(and
(Flag237-)
(Flag237prime-)
)

)
(:action Flag237Action-11
:parameters ()
:precondition
(and
(Flag236-)
(Flag236prime-)
)
:effect
(and
(Flag237-)
(Flag237prime-)
)

)
(:action Prim237Action
:parameters ()
:precondition
(and
(not (Flag160-))
(Flag160prime-)
(not (Flag209-))
(Flag209prime-)
(not (Flag233-))
(Flag233prime-)
(not (Flag227-))
(Flag227prime-)
(not (Flag229-))
(Flag229prime-)
(not (Flag222-))
(Flag222prime-)
(not (Flag153-))
(Flag153prime-)
(not (Flag175-))
(Flag175prime-)
(not (Flag202-))
(Flag202prime-)
(not (Flag188-))
(Flag188prime-)
(not (Flag192-))
(Flag192prime-)
(not (Flag236-))
(Flag236prime-)
)
:effect
(and
(Flag237prime-)
(not (Flag237-))
)

)
(:action Flag240Action-0
:parameters ()
:precondition
(and
(Flag155-)
(Flag155prime-)
)
:effect
(and
(Flag240-)
(Flag240prime-)
)

)
(:action Flag240Action-1
:parameters ()
:precondition
(and
(Flag157-)
(Flag157prime-)
)
:effect
(and
(Flag240-)
(Flag240prime-)
)

)
(:action Flag240Action-2
:parameters ()
:precondition
(and
(Flag145-)
(Flag145prime-)
)
:effect
(and
(Flag240-)
(Flag240prime-)
)

)
(:action Flag240Action-3
:parameters ()
:precondition
(and
(Flag150-)
(Flag150prime-)
)
:effect
(and
(Flag240-)
(Flag240prime-)
)

)
(:action Flag240Action-4
:parameters ()
:precondition
(and
(Flag146-)
(Flag146prime-)
)
:effect
(and
(Flag240-)
(Flag240prime-)
)

)
(:action Flag240Action-5
:parameters ()
:precondition
(and
(Flag239-)
(Flag239prime-)
)
:effect
(and
(Flag240-)
(Flag240prime-)
)

)
(:action Flag240Action-6
:parameters ()
:precondition
(and
(Flag158-)
(Flag158prime-)
)
:effect
(and
(Flag240-)
(Flag240prime-)
)

)
(:action Flag240Action-7
:parameters ()
:precondition
(and
(Flag163-)
(Flag163prime-)
)
:effect
(and
(Flag240-)
(Flag240prime-)
)

)
(:action Flag240Action-8
:parameters ()
:precondition
(and
(Flag153-)
(Flag153prime-)
)
:effect
(and
(Flag240-)
(Flag240prime-)
)

)
(:action Flag240Action-9
:parameters ()
:precondition
(and
(Flag159-)
(Flag159prime-)
)
:effect
(and
(Flag240-)
(Flag240prime-)
)

)
(:action Flag240Action-10
:parameters ()
:precondition
(and
(Flag164-)
(Flag164prime-)
)
:effect
(and
(Flag240-)
(Flag240prime-)
)

)
(:action Flag240Action-11
:parameters ()
:precondition
(and
(Flag147-)
(Flag147prime-)
)
:effect
(and
(Flag240-)
(Flag240prime-)
)

)
(:action Prim240Action
:parameters ()
:precondition
(and
(not (Flag155-))
(Flag155prime-)
(not (Flag157-))
(Flag157prime-)
(not (Flag145-))
(Flag145prime-)
(not (Flag150-))
(Flag150prime-)
(not (Flag146-))
(Flag146prime-)
(not (Flag239-))
(Flag239prime-)
(not (Flag158-))
(Flag158prime-)
(not (Flag163-))
(Flag163prime-)
(not (Flag153-))
(Flag153prime-)
(not (Flag159-))
(Flag159prime-)
(not (Flag164-))
(Flag164prime-)
(not (Flag147-))
(Flag147prime-)
)
:effect
(and
(Flag240prime-)
(not (Flag240-))
)

)
(:action Flag1Action
:parameters ()
:precondition
(and
(COLUMN2-ROBOT)
(ROW1-ROBOT)
(NOT-ERROR-)
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(Flag1-)
(Flag1prime-)
)

)
(:action Prim1Action-0
:parameters ()
:precondition
(and
(not (COLUMN2-ROBOT))
)
:effect
(and
(Flag1prime-)
(not (Flag1-))
)

)
(:action Flag3Action-0
:parameters ()
:precondition
(and
(LEFTOF3-ROBOT)
)
:effect
(and
(Flag3-)
(Flag3prime-)
)

)
(:action Flag3Action-1
:parameters ()
:precondition
(and
(RIGHTOF7-ROBOT)
)
:effect
(and
(Flag3-)
(Flag3prime-)
)

)
(:action Flag3Action-2
:parameters ()
:precondition
(and
(RIGHTOF8-ROBOT)
)
:effect
(and
(Flag3-)
(Flag3prime-)
)

)
(:action Flag3Action-3
:parameters ()
:precondition
(and
(LEFTOF2-ROBOT)
)
:effect
(and
(Flag3-)
(Flag3prime-)
)

)
(:action Flag3Action-4
:parameters ()
:precondition
(and
(LEFTOF9-ROBOT)
)
:effect
(and
(Flag3-)
(Flag3prime-)
)

)
(:action Flag3Action-6
:parameters ()
:precondition
(and
(RIGHTOF0-ROBOT)
)
:effect
(and
(Flag3-)
(Flag3prime-)
)

)
(:action Flag3Action-7
:parameters ()
:precondition
(and
(LEFTOF8-ROBOT)
)
:effect
(and
(Flag3-)
(Flag3prime-)
)

)
(:action Flag3Action-8
:parameters ()
:precondition
(and
(LEFTOF5-ROBOT)
)
:effect
(and
(Flag3-)
(Flag3prime-)
)

)
(:action Flag3Action-9
:parameters ()
:precondition
(and
(RIGHTOF1-ROBOT)
)
:effect
(and
(Flag3-)
(Flag3prime-)
)

)
(:action Flag3Action-10
:parameters ()
:precondition
(and
(LEFTOF12-ROBOT)
)
:effect
(and
(Flag3-)
(Flag3prime-)
)

)
(:action Flag3Action-11
:parameters ()
:precondition
(and
(RIGHTOF6-ROBOT)
)
:effect
(and
(Flag3-)
(Flag3prime-)
)

)
(:action Flag3Action-12
:parameters ()
:precondition
(and
(RIGHTOF9-ROBOT)
)
:effect
(and
(Flag3-)
(Flag3prime-)
)

)
(:action Flag3Action-13
:parameters ()
:precondition
(and
(LEFTOF6-ROBOT)
)
:effect
(and
(Flag3-)
(Flag3prime-)
)

)
(:action Flag3Action-14
:parameters ()
:precondition
(and
(RIGHTOF10-ROBOT)
)
:effect
(and
(Flag3-)
(Flag3prime-)
)

)
(:action Flag3Action-15
:parameters ()
:precondition
(and
(RIGHTOF11-ROBOT)
)
:effect
(and
(Flag3-)
(Flag3prime-)
)

)
(:action Flag3Action-17
:parameters ()
:precondition
(and
(RIGHTOF5-ROBOT)
)
:effect
(and
(Flag3-)
(Flag3prime-)
)

)
(:action Flag3Action-18
:parameters ()
:precondition
(and
(LEFTOF10-ROBOT)
)
:effect
(and
(Flag3-)
(Flag3prime-)
)

)
(:action Flag3Action-19
:parameters ()
:precondition
(and
(LEFTOF4-ROBOT)
)
:effect
(and
(Flag3-)
(Flag3prime-)
)

)
(:action Flag3Action-20
:parameters ()
:precondition
(and
(RIGHTOF4-ROBOT)
)
:effect
(and
(Flag3-)
(Flag3prime-)
)

)
(:action Flag3Action-21
:parameters ()
:precondition
(and
(LEFTOF1-ROBOT)
)
:effect
(and
(Flag3-)
(Flag3prime-)
)

)
(:action Flag3Action-22
:parameters ()
:precondition
(and
(LEFTOF7-ROBOT)
)
:effect
(and
(Flag3-)
(Flag3prime-)
)

)
(:action Flag3Action-23
:parameters ()
:precondition
(and
(RIGHTOF3-ROBOT)
)
:effect
(and
(Flag3-)
(Flag3prime-)
)

)
(:action Prim3Action
:parameters ()
:precondition
(and
(not (LEFTOF3-ROBOT))
(not (RIGHTOF7-ROBOT))
(not (RIGHTOF8-ROBOT))
(not (LEFTOF2-ROBOT))
(not (LEFTOF9-ROBOT))
(not (RIGHTOF2-ROBOT))
(not (RIGHTOF0-ROBOT))
(not (LEFTOF8-ROBOT))
(not (LEFTOF5-ROBOT))
(not (RIGHTOF1-ROBOT))
(not (LEFTOF12-ROBOT))
(not (RIGHTOF6-ROBOT))
(not (RIGHTOF9-ROBOT))
(not (LEFTOF6-ROBOT))
(not (RIGHTOF10-ROBOT))
(not (RIGHTOF11-ROBOT))
(not (LEFTOF11-ROBOT))
(not (RIGHTOF5-ROBOT))
(not (LEFTOF10-ROBOT))
(not (LEFTOF4-ROBOT))
(not (RIGHTOF4-ROBOT))
(not (LEFTOF1-ROBOT))
(not (LEFTOF7-ROBOT))
(not (RIGHTOF3-ROBOT))
)
:effect
(and
(Flag3prime-)
(not (Flag3-))
)

)
(:action Flag48Action-0
:parameters ()
:precondition
(and
(Flag26-)
(Flag26prime-)
)
:effect
(and
(Flag48-)
(Flag48prime-)
)

)
(:action Flag48Action-1
:parameters ()
:precondition
(and
(Flag27-)
(Flag27prime-)
)
:effect
(and
(Flag48-)
(Flag48prime-)
)

)
(:action Flag48Action-2
:parameters ()
:precondition
(and
(Flag28-)
(Flag28prime-)
)
:effect
(and
(Flag48-)
(Flag48prime-)
)

)
(:action Flag48Action-3
:parameters ()
:precondition
(and
(Flag29-)
(Flag29prime-)
)
:effect
(and
(Flag48-)
(Flag48prime-)
)

)
(:action Flag48Action-4
:parameters ()
:precondition
(and
(Flag30-)
(Flag30prime-)
)
:effect
(and
(Flag48-)
(Flag48prime-)
)

)
(:action Flag48Action-5
:parameters ()
:precondition
(and
(Flag31-)
(Flag31prime-)
)
:effect
(and
(Flag48-)
(Flag48prime-)
)

)
(:action Flag48Action-6
:parameters ()
:precondition
(and
(Flag32-)
(Flag32prime-)
)
:effect
(and
(Flag48-)
(Flag48prime-)
)

)
(:action Flag48Action-7
:parameters ()
:precondition
(and
(Flag33-)
(Flag33prime-)
)
:effect
(and
(Flag48-)
(Flag48prime-)
)

)
(:action Flag48Action-8
:parameters ()
:precondition
(and
(Flag34-)
(Flag34prime-)
)
:effect
(and
(Flag48-)
(Flag48prime-)
)

)
(:action Flag48Action-9
:parameters ()
:precondition
(and
(Flag35-)
(Flag35prime-)
)
:effect
(and
(Flag48-)
(Flag48prime-)
)

)
(:action Flag48Action-10
:parameters ()
:precondition
(and
(Flag36-)
(Flag36prime-)
)
:effect
(and
(Flag48-)
(Flag48prime-)
)

)
(:action Flag48Action-11
:parameters ()
:precondition
(and
(Flag37-)
(Flag37prime-)
)
:effect
(and
(Flag48-)
(Flag48prime-)
)

)
(:action Flag48Action-12
:parameters ()
:precondition
(and
(Flag38-)
(Flag38prime-)
)
:effect
(and
(Flag48-)
(Flag48prime-)
)

)
(:action Flag48Action-13
:parameters ()
:precondition
(and
(Flag39-)
(Flag39prime-)
)
:effect
(and
(Flag48-)
(Flag48prime-)
)

)
(:action Flag48Action-14
:parameters ()
:precondition
(and
(Flag40-)
(Flag40prime-)
)
:effect
(and
(Flag48-)
(Flag48prime-)
)

)
(:action Flag48Action-15
:parameters ()
:precondition
(and
(Flag41-)
(Flag41prime-)
)
:effect
(and
(Flag48-)
(Flag48prime-)
)

)
(:action Flag48Action-16
:parameters ()
:precondition
(and
(Flag42-)
(Flag42prime-)
)
:effect
(and
(Flag48-)
(Flag48prime-)
)

)
(:action Flag48Action-17
:parameters ()
:precondition
(and
(Flag43-)
(Flag43prime-)
)
:effect
(and
(Flag48-)
(Flag48prime-)
)

)
(:action Flag48Action-18
:parameters ()
:precondition
(and
(Flag44-)
(Flag44prime-)
)
:effect
(and
(Flag48-)
(Flag48prime-)
)

)
(:action Flag48Action-19
:parameters ()
:precondition
(and
(Flag45-)
(Flag45prime-)
)
:effect
(and
(Flag48-)
(Flag48prime-)
)

)
(:action Flag48Action-20
:parameters ()
:precondition
(and
(Flag46-)
(Flag46prime-)
)
:effect
(and
(Flag48-)
(Flag48prime-)
)

)
(:action Flag48Action-21
:parameters ()
:precondition
(and
(Flag47-)
(Flag47prime-)
)
:effect
(and
(Flag48-)
(Flag48prime-)
)

)
(:action Prim48Action
:parameters ()
:precondition
(and
(not (Flag26-))
(Flag26prime-)
(not (Flag27-))
(Flag27prime-)
(not (Flag28-))
(Flag28prime-)
(not (Flag29-))
(Flag29prime-)
(not (Flag30-))
(Flag30prime-)
(not (Flag31-))
(Flag31prime-)
(not (Flag32-))
(Flag32prime-)
(not (Flag33-))
(Flag33prime-)
(not (Flag34-))
(Flag34prime-)
(not (Flag35-))
(Flag35prime-)
(not (Flag36-))
(Flag36prime-)
(not (Flag37-))
(Flag37prime-)
(not (Flag38-))
(Flag38prime-)
(not (Flag39-))
(Flag39prime-)
(not (Flag40-))
(Flag40prime-)
(not (Flag41-))
(Flag41prime-)
(not (Flag42-))
(Flag42prime-)
(not (Flag43-))
(Flag43prime-)
(not (Flag44-))
(Flag44prime-)
(not (Flag45-))
(Flag45prime-)
(not (Flag46-))
(Flag46prime-)
(not (Flag47-))
(Flag47prime-)
)
:effect
(and
(Flag48prime-)
(not (Flag48-))
)

)
(:action Flag60Action-0
:parameters ()
:precondition
(and
(Flag49-)
(Flag49prime-)
)
:effect
(and
(Flag60-)
(Flag60prime-)
)

)
(:action Flag60Action-1
:parameters ()
:precondition
(and
(Flag50-)
(Flag50prime-)
)
:effect
(and
(Flag60-)
(Flag60prime-)
)

)
(:action Flag60Action-2
:parameters ()
:precondition
(and
(Flag35-)
(Flag35prime-)
)
:effect
(and
(Flag60-)
(Flag60prime-)
)

)
(:action Flag60Action-3
:parameters ()
:precondition
(and
(Flag51-)
(Flag51prime-)
)
:effect
(and
(Flag60-)
(Flag60prime-)
)

)
(:action Flag60Action-4
:parameters ()
:precondition
(and
(Flag27-)
(Flag27prime-)
)
:effect
(and
(Flag60-)
(Flag60prime-)
)

)
(:action Flag60Action-5
:parameters ()
:precondition
(and
(Flag39-)
(Flag39prime-)
)
:effect
(and
(Flag60-)
(Flag60prime-)
)

)
(:action Flag60Action-6
:parameters ()
:precondition
(and
(Flag29-)
(Flag29prime-)
)
:effect
(and
(Flag60-)
(Flag60prime-)
)

)
(:action Flag60Action-7
:parameters ()
:precondition
(and
(Flag30-)
(Flag30prime-)
)
:effect
(and
(Flag60-)
(Flag60prime-)
)

)
(:action Flag60Action-8
:parameters ()
:precondition
(and
(Flag32-)
(Flag32prime-)
)
:effect
(and
(Flag60-)
(Flag60prime-)
)

)
(:action Flag60Action-9
:parameters ()
:precondition
(and
(Flag52-)
(Flag52prime-)
)
:effect
(and
(Flag60-)
(Flag60prime-)
)

)
(:action Flag60Action-10
:parameters ()
:precondition
(and
(Flag33-)
(Flag33prime-)
)
:effect
(and
(Flag60-)
(Flag60prime-)
)

)
(:action Flag60Action-11
:parameters ()
:precondition
(and
(Flag53-)
(Flag53prime-)
)
:effect
(and
(Flag60-)
(Flag60prime-)
)

)
(:action Flag60Action-12
:parameters ()
:precondition
(and
(Flag34-)
(Flag34prime-)
)
:effect
(and
(Flag60-)
(Flag60prime-)
)

)
(:action Flag60Action-13
:parameters ()
:precondition
(and
(Flag54-)
(Flag54prime-)
)
:effect
(and
(Flag60-)
(Flag60prime-)
)

)
(:action Flag60Action-14
:parameters ()
:precondition
(and
(Flag36-)
(Flag36prime-)
)
:effect
(and
(Flag60-)
(Flag60prime-)
)

)
(:action Flag60Action-15
:parameters ()
:precondition
(and
(Flag55-)
(Flag55prime-)
)
:effect
(and
(Flag60-)
(Flag60prime-)
)

)
(:action Flag60Action-16
:parameters ()
:precondition
(and
(Flag37-)
(Flag37prime-)
)
:effect
(and
(Flag60-)
(Flag60prime-)
)

)
(:action Flag60Action-17
:parameters ()
:precondition
(and
(Flag56-)
(Flag56prime-)
)
:effect
(and
(Flag60-)
(Flag60prime-)
)

)
(:action Flag60Action-18
:parameters ()
:precondition
(and
(Flag38-)
(Flag38prime-)
)
:effect
(and
(Flag60-)
(Flag60prime-)
)

)
(:action Flag60Action-19
:parameters ()
:precondition
(and
(Flag57-)
(Flag57prime-)
)
:effect
(and
(Flag60-)
(Flag60prime-)
)

)
(:action Flag60Action-20
:parameters ()
:precondition
(and
(Flag58-)
(Flag58prime-)
)
:effect
(and
(Flag60-)
(Flag60prime-)
)

)
(:action Flag60Action-21
:parameters ()
:precondition
(and
(Flag40-)
(Flag40prime-)
)
:effect
(and
(Flag60-)
(Flag60prime-)
)

)
(:action Flag60Action-22
:parameters ()
:precondition
(and
(Flag41-)
(Flag41prime-)
)
:effect
(and
(Flag60-)
(Flag60prime-)
)

)
(:action Flag60Action-23
:parameters ()
:precondition
(and
(Flag42-)
(Flag42prime-)
)
:effect
(and
(Flag60-)
(Flag60prime-)
)

)
(:action Flag60Action-24
:parameters ()
:precondition
(and
(Flag43-)
(Flag43prime-)
)
:effect
(and
(Flag60-)
(Flag60prime-)
)

)
(:action Flag60Action-25
:parameters ()
:precondition
(and
(Flag44-)
(Flag44prime-)
)
:effect
(and
(Flag60-)
(Flag60prime-)
)

)
(:action Flag60Action-26
:parameters ()
:precondition
(and
(Flag59-)
(Flag59prime-)
)
:effect
(and
(Flag60-)
(Flag60prime-)
)

)
(:action Flag60Action-27
:parameters ()
:precondition
(and
(Flag45-)
(Flag45prime-)
)
:effect
(and
(Flag60-)
(Flag60prime-)
)

)
(:action Flag60Action-28
:parameters ()
:precondition
(and
(Flag46-)
(Flag46prime-)
)
:effect
(and
(Flag60-)
(Flag60prime-)
)

)
(:action Flag60Action-29
:parameters ()
:precondition
(and
(Flag47-)
(Flag47prime-)
)
:effect
(and
(Flag60-)
(Flag60prime-)
)

)
(:action Prim60Action
:parameters ()
:precondition
(and
(not (Flag49-))
(Flag49prime-)
(not (Flag50-))
(Flag50prime-)
(not (Flag35-))
(Flag35prime-)
(not (Flag51-))
(Flag51prime-)
(not (Flag27-))
(Flag27prime-)
(not (Flag39-))
(Flag39prime-)
(not (Flag29-))
(Flag29prime-)
(not (Flag30-))
(Flag30prime-)
(not (Flag32-))
(Flag32prime-)
(not (Flag52-))
(Flag52prime-)
(not (Flag33-))
(Flag33prime-)
(not (Flag53-))
(Flag53prime-)
(not (Flag34-))
(Flag34prime-)
(not (Flag54-))
(Flag54prime-)
(not (Flag36-))
(Flag36prime-)
(not (Flag55-))
(Flag55prime-)
(not (Flag37-))
(Flag37prime-)
(not (Flag56-))
(Flag56prime-)
(not (Flag38-))
(Flag38prime-)
(not (Flag57-))
(Flag57prime-)
(not (Flag58-))
(Flag58prime-)
(not (Flag40-))
(Flag40prime-)
(not (Flag41-))
(Flag41prime-)
(not (Flag42-))
(Flag42prime-)
(not (Flag43-))
(Flag43prime-)
(not (Flag44-))
(Flag44prime-)
(not (Flag59-))
(Flag59prime-)
(not (Flag45-))
(Flag45prime-)
(not (Flag46-))
(Flag46prime-)
(not (Flag47-))
(Flag47prime-)
)
:effect
(and
(Flag60prime-)
(not (Flag60-))
)

)
(:action Flag72Action-0
:parameters ()
:precondition
(and
(Flag49-)
(Flag49prime-)
)
:effect
(and
(Flag72-)
(Flag72prime-)
)

)
(:action Flag72Action-1
:parameters ()
:precondition
(and
(Flag61-)
(Flag61prime-)
)
:effect
(and
(Flag72-)
(Flag72prime-)
)

)
(:action Flag72Action-2
:parameters ()
:precondition
(and
(Flag62-)
(Flag62prime-)
)
:effect
(and
(Flag72-)
(Flag72prime-)
)

)
(:action Flag72Action-3
:parameters ()
:precondition
(and
(Flag50-)
(Flag50prime-)
)
:effect
(and
(Flag72-)
(Flag72prime-)
)

)
(:action Flag72Action-4
:parameters ()
:precondition
(and
(Flag51-)
(Flag51prime-)
)
:effect
(and
(Flag72-)
(Flag72prime-)
)

)
(:action Flag72Action-5
:parameters ()
:precondition
(and
(Flag27-)
(Flag27prime-)
)
:effect
(and
(Flag72-)
(Flag72prime-)
)

)
(:action Flag72Action-6
:parameters ()
:precondition
(and
(Flag39-)
(Flag39prime-)
)
:effect
(and
(Flag72-)
(Flag72prime-)
)

)
(:action Flag72Action-7
:parameters ()
:precondition
(and
(Flag29-)
(Flag29prime-)
)
:effect
(and
(Flag72-)
(Flag72prime-)
)

)
(:action Flag72Action-8
:parameters ()
:precondition
(and
(Flag52-)
(Flag52prime-)
)
:effect
(and
(Flag72-)
(Flag72prime-)
)

)
(:action Flag72Action-9
:parameters ()
:precondition
(and
(Flag30-)
(Flag30prime-)
)
:effect
(and
(Flag72-)
(Flag72prime-)
)

)
(:action Flag72Action-10
:parameters ()
:precondition
(and
(Flag63-)
(Flag63prime-)
)
:effect
(and
(Flag72-)
(Flag72prime-)
)

)
(:action Flag72Action-11
:parameters ()
:precondition
(and
(Flag32-)
(Flag32prime-)
)
:effect
(and
(Flag72-)
(Flag72prime-)
)

)
(:action Flag72Action-12
:parameters ()
:precondition
(and
(Flag64-)
(Flag64prime-)
)
:effect
(and
(Flag72-)
(Flag72prime-)
)

)
(:action Flag72Action-13
:parameters ()
:precondition
(and
(Flag65-)
(Flag65prime-)
)
:effect
(and
(Flag72-)
(Flag72prime-)
)

)
(:action Flag72Action-14
:parameters ()
:precondition
(and
(Flag66-)
(Flag66prime-)
)
:effect
(and
(Flag72-)
(Flag72prime-)
)

)
(:action Flag72Action-15
:parameters ()
:precondition
(and
(Flag33-)
(Flag33prime-)
)
:effect
(and
(Flag72-)
(Flag72prime-)
)

)
(:action Flag72Action-16
:parameters ()
:precondition
(and
(Flag67-)
(Flag67prime-)
)
:effect
(and
(Flag72-)
(Flag72prime-)
)

)
(:action Flag72Action-17
:parameters ()
:precondition
(and
(Flag68-)
(Flag68prime-)
)
:effect
(and
(Flag72-)
(Flag72prime-)
)

)
(:action Flag72Action-18
:parameters ()
:precondition
(and
(Flag34-)
(Flag34prime-)
)
:effect
(and
(Flag72-)
(Flag72prime-)
)

)
(:action Flag72Action-19
:parameters ()
:precondition
(and
(Flag36-)
(Flag36prime-)
)
:effect
(and
(Flag72-)
(Flag72prime-)
)

)
(:action Flag72Action-20
:parameters ()
:precondition
(and
(Flag55-)
(Flag55prime-)
)
:effect
(and
(Flag72-)
(Flag72prime-)
)

)
(:action Flag72Action-21
:parameters ()
:precondition
(and
(Flag37-)
(Flag37prime-)
)
:effect
(and
(Flag72-)
(Flag72prime-)
)

)
(:action Flag72Action-22
:parameters ()
:precondition
(and
(Flag56-)
(Flag56prime-)
)
:effect
(and
(Flag72-)
(Flag72prime-)
)

)
(:action Flag72Action-23
:parameters ()
:precondition
(and
(Flag38-)
(Flag38prime-)
)
:effect
(and
(Flag72-)
(Flag72prime-)
)

)
(:action Flag72Action-24
:parameters ()
:precondition
(and
(Flag57-)
(Flag57prime-)
)
:effect
(and
(Flag72-)
(Flag72prime-)
)

)
(:action Flag72Action-25
:parameters ()
:precondition
(and
(Flag69-)
(Flag69prime-)
)
:effect
(and
(Flag72-)
(Flag72prime-)
)

)
(:action Flag72Action-26
:parameters ()
:precondition
(and
(Flag40-)
(Flag40prime-)
)
:effect
(and
(Flag72-)
(Flag72prime-)
)

)
(:action Flag72Action-27
:parameters ()
:precondition
(and
(Flag41-)
(Flag41prime-)
)
:effect
(and
(Flag72-)
(Flag72prime-)
)

)
(:action Flag72Action-28
:parameters ()
:precondition
(and
(Flag42-)
(Flag42prime-)
)
:effect
(and
(Flag72-)
(Flag72prime-)
)

)
(:action Flag72Action-29
:parameters ()
:precondition
(and
(Flag43-)
(Flag43prime-)
)
:effect
(and
(Flag72-)
(Flag72prime-)
)

)
(:action Flag72Action-30
:parameters ()
:precondition
(and
(Flag44-)
(Flag44prime-)
)
:effect
(and
(Flag72-)
(Flag72prime-)
)

)
(:action Flag72Action-31
:parameters ()
:precondition
(and
(Flag70-)
(Flag70prime-)
)
:effect
(and
(Flag72-)
(Flag72prime-)
)

)
(:action Flag72Action-32
:parameters ()
:precondition
(and
(Flag71-)
(Flag71prime-)
)
:effect
(and
(Flag72-)
(Flag72prime-)
)

)
(:action Flag72Action-33
:parameters ()
:precondition
(and
(Flag59-)
(Flag59prime-)
)
:effect
(and
(Flag72-)
(Flag72prime-)
)

)
(:action Flag72Action-34
:parameters ()
:precondition
(and
(Flag45-)
(Flag45prime-)
)
:effect
(and
(Flag72-)
(Flag72prime-)
)

)
(:action Flag72Action-35
:parameters ()
:precondition
(and
(Flag46-)
(Flag46prime-)
)
:effect
(and
(Flag72-)
(Flag72prime-)
)

)
(:action Flag72Action-36
:parameters ()
:precondition
(and
(Flag47-)
(Flag47prime-)
)
:effect
(and
(Flag72-)
(Flag72prime-)
)

)
(:action Prim72Action
:parameters ()
:precondition
(and
(not (Flag49-))
(Flag49prime-)
(not (Flag61-))
(Flag61prime-)
(not (Flag62-))
(Flag62prime-)
(not (Flag50-))
(Flag50prime-)
(not (Flag51-))
(Flag51prime-)
(not (Flag27-))
(Flag27prime-)
(not (Flag39-))
(Flag39prime-)
(not (Flag29-))
(Flag29prime-)
(not (Flag52-))
(Flag52prime-)
(not (Flag30-))
(Flag30prime-)
(not (Flag63-))
(Flag63prime-)
(not (Flag32-))
(Flag32prime-)
(not (Flag64-))
(Flag64prime-)
(not (Flag65-))
(Flag65prime-)
(not (Flag66-))
(Flag66prime-)
(not (Flag33-))
(Flag33prime-)
(not (Flag67-))
(Flag67prime-)
(not (Flag68-))
(Flag68prime-)
(not (Flag34-))
(Flag34prime-)
(not (Flag36-))
(Flag36prime-)
(not (Flag55-))
(Flag55prime-)
(not (Flag37-))
(Flag37prime-)
(not (Flag56-))
(Flag56prime-)
(not (Flag38-))
(Flag38prime-)
(not (Flag57-))
(Flag57prime-)
(not (Flag69-))
(Flag69prime-)
(not (Flag40-))
(Flag40prime-)
(not (Flag41-))
(Flag41prime-)
(not (Flag42-))
(Flag42prime-)
(not (Flag43-))
(Flag43prime-)
(not (Flag44-))
(Flag44prime-)
(not (Flag70-))
(Flag70prime-)
(not (Flag71-))
(Flag71prime-)
(not (Flag59-))
(Flag59prime-)
(not (Flag45-))
(Flag45prime-)
(not (Flag46-))
(Flag46prime-)
(not (Flag47-))
(Flag47prime-)
)
:effect
(and
(Flag72prime-)
(not (Flag72-))
)

)
(:action Flag81Action-0
:parameters ()
:precondition
(and
(Flag73-)
(Flag73prime-)
)
:effect
(and
(Flag81-)
(Flag81prime-)
)

)
(:action Flag81Action-1
:parameters ()
:precondition
(and
(Flag49-)
(Flag49prime-)
)
:effect
(and
(Flag81-)
(Flag81prime-)
)

)
(:action Flag81Action-2
:parameters ()
:precondition
(and
(Flag66-)
(Flag66prime-)
)
:effect
(and
(Flag81-)
(Flag81prime-)
)

)
(:action Flag81Action-3
:parameters ()
:precondition
(and
(Flag50-)
(Flag50prime-)
)
:effect
(and
(Flag81-)
(Flag81prime-)
)

)
(:action Flag81Action-4
:parameters ()
:precondition
(and
(Flag51-)
(Flag51prime-)
)
:effect
(and
(Flag81-)
(Flag81prime-)
)

)
(:action Flag81Action-5
:parameters ()
:precondition
(and
(Flag27-)
(Flag27prime-)
)
:effect
(and
(Flag81-)
(Flag81prime-)
)

)
(:action Flag81Action-6
:parameters ()
:precondition
(and
(Flag29-)
(Flag29prime-)
)
:effect
(and
(Flag81-)
(Flag81prime-)
)

)
(:action Flag81Action-7
:parameters ()
:precondition
(and
(Flag52-)
(Flag52prime-)
)
:effect
(and
(Flag81-)
(Flag81prime-)
)

)
(:action Flag81Action-8
:parameters ()
:precondition
(and
(Flag74-)
(Flag74prime-)
)
:effect
(and
(Flag81-)
(Flag81prime-)
)

)
(:action Flag81Action-9
:parameters ()
:precondition
(and
(Flag30-)
(Flag30prime-)
)
:effect
(and
(Flag81-)
(Flag81prime-)
)

)
(:action Flag81Action-10
:parameters ()
:precondition
(and
(Flag45-)
(Flag45prime-)
)
:effect
(and
(Flag81-)
(Flag81prime-)
)

)
(:action Flag81Action-11
:parameters ()
:precondition
(and
(Flag63-)
(Flag63prime-)
)
:effect
(and
(Flag81-)
(Flag81prime-)
)

)
(:action Flag81Action-12
:parameters ()
:precondition
(and
(Flag32-)
(Flag32prime-)
)
:effect
(and
(Flag81-)
(Flag81prime-)
)

)
(:action Flag81Action-13
:parameters ()
:precondition
(and
(Flag64-)
(Flag64prime-)
)
:effect
(and
(Flag81-)
(Flag81prime-)
)

)
(:action Flag81Action-14
:parameters ()
:precondition
(and
(Flag65-)
(Flag65prime-)
)
:effect
(and
(Flag81-)
(Flag81prime-)
)

)
(:action Flag81Action-15
:parameters ()
:precondition
(and
(Flag75-)
(Flag75prime-)
)
:effect
(and
(Flag81-)
(Flag81prime-)
)

)
(:action Flag81Action-16
:parameters ()
:precondition
(and
(Flag76-)
(Flag76prime-)
)
:effect
(and
(Flag81-)
(Flag81prime-)
)

)
(:action Flag81Action-17
:parameters ()
:precondition
(and
(Flag33-)
(Flag33prime-)
)
:effect
(and
(Flag81-)
(Flag81prime-)
)

)
(:action Flag81Action-18
:parameters ()
:precondition
(and
(Flag67-)
(Flag67prime-)
)
:effect
(and
(Flag81-)
(Flag81prime-)
)

)
(:action Flag81Action-19
:parameters ()
:precondition
(and
(Flag34-)
(Flag34prime-)
)
:effect
(and
(Flag81-)
(Flag81prime-)
)

)
(:action Flag81Action-20
:parameters ()
:precondition
(and
(Flag36-)
(Flag36prime-)
)
:effect
(and
(Flag81-)
(Flag81prime-)
)

)
(:action Flag81Action-21
:parameters ()
:precondition
(and
(Flag55-)
(Flag55prime-)
)
:effect
(and
(Flag81-)
(Flag81prime-)
)

)
(:action Flag81Action-22
:parameters ()
:precondition
(and
(Flag37-)
(Flag37prime-)
)
:effect
(and
(Flag81-)
(Flag81prime-)
)

)
(:action Flag81Action-23
:parameters ()
:precondition
(and
(Flag77-)
(Flag77prime-)
)
:effect
(and
(Flag81-)
(Flag81prime-)
)

)
(:action Flag81Action-24
:parameters ()
:precondition
(and
(Flag56-)
(Flag56prime-)
)
:effect
(and
(Flag81-)
(Flag81prime-)
)

)
(:action Flag81Action-25
:parameters ()
:precondition
(and
(Flag78-)
(Flag78prime-)
)
:effect
(and
(Flag81-)
(Flag81prime-)
)

)
(:action Flag81Action-26
:parameters ()
:precondition
(and
(Flag38-)
(Flag38prime-)
)
:effect
(and
(Flag81-)
(Flag81prime-)
)

)
(:action Flag81Action-27
:parameters ()
:precondition
(and
(Flag39-)
(Flag39prime-)
)
:effect
(and
(Flag81-)
(Flag81prime-)
)

)
(:action Flag81Action-28
:parameters ()
:precondition
(and
(Flag69-)
(Flag69prime-)
)
:effect
(and
(Flag81-)
(Flag81prime-)
)

)
(:action Flag81Action-29
:parameters ()
:precondition
(and
(Flag62-)
(Flag62prime-)
)
:effect
(and
(Flag81-)
(Flag81prime-)
)

)
(:action Flag81Action-30
:parameters ()
:precondition
(and
(Flag41-)
(Flag41prime-)
)
:effect
(and
(Flag81-)
(Flag81prime-)
)

)
(:action Flag81Action-31
:parameters ()
:precondition
(and
(Flag42-)
(Flag42prime-)
)
:effect
(and
(Flag81-)
(Flag81prime-)
)

)
(:action Flag81Action-32
:parameters ()
:precondition
(and
(Flag43-)
(Flag43prime-)
)
:effect
(and
(Flag81-)
(Flag81prime-)
)

)
(:action Flag81Action-33
:parameters ()
:precondition
(and
(Flag44-)
(Flag44prime-)
)
:effect
(and
(Flag81-)
(Flag81prime-)
)

)
(:action Flag81Action-34
:parameters ()
:precondition
(and
(Flag79-)
(Flag79prime-)
)
:effect
(and
(Flag81-)
(Flag81prime-)
)

)
(:action Flag81Action-35
:parameters ()
:precondition
(and
(Flag70-)
(Flag70prime-)
)
:effect
(and
(Flag81-)
(Flag81prime-)
)

)
(:action Flag81Action-36
:parameters ()
:precondition
(and
(Flag71-)
(Flag71prime-)
)
:effect
(and
(Flag81-)
(Flag81prime-)
)

)
(:action Flag81Action-37
:parameters ()
:precondition
(and
(Flag59-)
(Flag59prime-)
)
:effect
(and
(Flag81-)
(Flag81prime-)
)

)
(:action Flag81Action-38
:parameters ()
:precondition
(and
(Flag80-)
(Flag80prime-)
)
:effect
(and
(Flag81-)
(Flag81prime-)
)

)
(:action Flag81Action-39
:parameters ()
:precondition
(and
(Flag46-)
(Flag46prime-)
)
:effect
(and
(Flag81-)
(Flag81prime-)
)

)
(:action Flag81Action-40
:parameters ()
:precondition
(and
(Flag57-)
(Flag57prime-)
)
:effect
(and
(Flag81-)
(Flag81prime-)
)

)
(:action Prim81Action
:parameters ()
:precondition
(and
(not (Flag73-))
(Flag73prime-)
(not (Flag49-))
(Flag49prime-)
(not (Flag66-))
(Flag66prime-)
(not (Flag50-))
(Flag50prime-)
(not (Flag51-))
(Flag51prime-)
(not (Flag27-))
(Flag27prime-)
(not (Flag29-))
(Flag29prime-)
(not (Flag52-))
(Flag52prime-)
(not (Flag74-))
(Flag74prime-)
(not (Flag30-))
(Flag30prime-)
(not (Flag45-))
(Flag45prime-)
(not (Flag63-))
(Flag63prime-)
(not (Flag32-))
(Flag32prime-)
(not (Flag64-))
(Flag64prime-)
(not (Flag65-))
(Flag65prime-)
(not (Flag75-))
(Flag75prime-)
(not (Flag76-))
(Flag76prime-)
(not (Flag33-))
(Flag33prime-)
(not (Flag67-))
(Flag67prime-)
(not (Flag34-))
(Flag34prime-)
(not (Flag36-))
(Flag36prime-)
(not (Flag55-))
(Flag55prime-)
(not (Flag37-))
(Flag37prime-)
(not (Flag77-))
(Flag77prime-)
(not (Flag56-))
(Flag56prime-)
(not (Flag78-))
(Flag78prime-)
(not (Flag38-))
(Flag38prime-)
(not (Flag39-))
(Flag39prime-)
(not (Flag69-))
(Flag69prime-)
(not (Flag62-))
(Flag62prime-)
(not (Flag41-))
(Flag41prime-)
(not (Flag42-))
(Flag42prime-)
(not (Flag43-))
(Flag43prime-)
(not (Flag44-))
(Flag44prime-)
(not (Flag79-))
(Flag79prime-)
(not (Flag70-))
(Flag70prime-)
(not (Flag71-))
(Flag71prime-)
(not (Flag59-))
(Flag59prime-)
(not (Flag80-))
(Flag80prime-)
(not (Flag46-))
(Flag46prime-)
(not (Flag57-))
(Flag57prime-)
)
:effect
(and
(Flag81prime-)
(not (Flag81-))
)

)
(:action Flag90Action-0
:parameters ()
:precondition
(and
(Flag49-)
(Flag49prime-)
)
:effect
(and
(Flag90-)
(Flag90prime-)
)

)
(:action Flag90Action-1
:parameters ()
:precondition
(and
(Flag82-)
(Flag82prime-)
)
:effect
(and
(Flag90-)
(Flag90prime-)
)

)
(:action Flag90Action-2
:parameters ()
:precondition
(and
(Flag50-)
(Flag50prime-)
)
:effect
(and
(Flag90-)
(Flag90prime-)
)

)
(:action Flag90Action-3
:parameters ()
:precondition
(and
(Flag51-)
(Flag51prime-)
)
:effect
(and
(Flag90-)
(Flag90prime-)
)

)
(:action Flag90Action-4
:parameters ()
:precondition
(and
(Flag27-)
(Flag27prime-)
)
:effect
(and
(Flag90-)
(Flag90prime-)
)

)
(:action Flag90Action-5
:parameters ()
:precondition
(and
(Flag29-)
(Flag29prime-)
)
:effect
(and
(Flag90-)
(Flag90prime-)
)

)
(:action Flag90Action-6
:parameters ()
:precondition
(and
(Flag83-)
(Flag83prime-)
)
:effect
(and
(Flag90-)
(Flag90prime-)
)

)
(:action Flag90Action-7
:parameters ()
:precondition
(and
(Flag74-)
(Flag74prime-)
)
:effect
(and
(Flag90-)
(Flag90prime-)
)

)
(:action Flag90Action-8
:parameters ()
:precondition
(and
(Flag41-)
(Flag41prime-)
)
:effect
(and
(Flag90-)
(Flag90prime-)
)

)
(:action Flag90Action-9
:parameters ()
:precondition
(and
(Flag36-)
(Flag36prime-)
)
:effect
(and
(Flag90-)
(Flag90prime-)
)

)
(:action Flag90Action-10
:parameters ()
:precondition
(and
(Flag30-)
(Flag30prime-)
)
:effect
(and
(Flag90-)
(Flag90prime-)
)

)
(:action Flag90Action-11
:parameters ()
:precondition
(and
(Flag45-)
(Flag45prime-)
)
:effect
(and
(Flag90-)
(Flag90prime-)
)

)
(:action Flag90Action-12
:parameters ()
:precondition
(and
(Flag63-)
(Flag63prime-)
)
:effect
(and
(Flag90-)
(Flag90prime-)
)

)
(:action Flag90Action-13
:parameters ()
:precondition
(and
(Flag32-)
(Flag32prime-)
)
:effect
(and
(Flag90-)
(Flag90prime-)
)

)
(:action Flag90Action-14
:parameters ()
:precondition
(and
(Flag84-)
(Flag84prime-)
)
:effect
(and
(Flag90-)
(Flag90prime-)
)

)
(:action Flag90Action-15
:parameters ()
:precondition
(and
(Flag85-)
(Flag85prime-)
)
:effect
(and
(Flag90-)
(Flag90prime-)
)

)
(:action Flag90Action-16
:parameters ()
:precondition
(and
(Flag52-)
(Flag52prime-)
)
:effect
(and
(Flag90-)
(Flag90prime-)
)

)
(:action Flag90Action-17
:parameters ()
:precondition
(and
(Flag65-)
(Flag65prime-)
)
:effect
(and
(Flag90-)
(Flag90prime-)
)

)
(:action Flag90Action-18
:parameters ()
:precondition
(and
(Flag75-)
(Flag75prime-)
)
:effect
(and
(Flag90-)
(Flag90prime-)
)

)
(:action Flag90Action-19
:parameters ()
:precondition
(and
(Flag66-)
(Flag66prime-)
)
:effect
(and
(Flag90-)
(Flag90prime-)
)

)
(:action Flag90Action-20
:parameters ()
:precondition
(and
(Flag67-)
(Flag67prime-)
)
:effect
(and
(Flag90-)
(Flag90prime-)
)

)
(:action Flag90Action-21
:parameters ()
:precondition
(and
(Flag34-)
(Flag34prime-)
)
:effect
(and
(Flag90-)
(Flag90prime-)
)

)
(:action Flag90Action-22
:parameters ()
:precondition
(and
(Flag37-)
(Flag37prime-)
)
:effect
(and
(Flag90-)
(Flag90prime-)
)

)
(:action Flag90Action-23
:parameters ()
:precondition
(and
(Flag55-)
(Flag55prime-)
)
:effect
(and
(Flag90-)
(Flag90prime-)
)

)
(:action Flag90Action-24
:parameters ()
:precondition
(and
(Flag86-)
(Flag86prime-)
)
:effect
(and
(Flag90-)
(Flag90prime-)
)

)
(:action Flag90Action-25
:parameters ()
:precondition
(and
(Flag78-)
(Flag78prime-)
)
:effect
(and
(Flag90-)
(Flag90prime-)
)

)
(:action Flag90Action-26
:parameters ()
:precondition
(and
(Flag38-)
(Flag38prime-)
)
:effect
(and
(Flag90-)
(Flag90prime-)
)

)
(:action Flag90Action-27
:parameters ()
:precondition
(and
(Flag39-)
(Flag39prime-)
)
:effect
(and
(Flag90-)
(Flag90prime-)
)

)
(:action Flag90Action-28
:parameters ()
:precondition
(and
(Flag69-)
(Flag69prime-)
)
:effect
(and
(Flag90-)
(Flag90prime-)
)

)
(:action Flag90Action-29
:parameters ()
:precondition
(and
(Flag87-)
(Flag87prime-)
)
:effect
(and
(Flag90-)
(Flag90prime-)
)

)
(:action Flag90Action-30
:parameters ()
:precondition
(and
(Flag62-)
(Flag62prime-)
)
:effect
(and
(Flag90-)
(Flag90prime-)
)

)
(:action Flag90Action-31
:parameters ()
:precondition
(and
(Flag88-)
(Flag88prime-)
)
:effect
(and
(Flag90-)
(Flag90prime-)
)

)
(:action Flag90Action-32
:parameters ()
:precondition
(and
(Flag42-)
(Flag42prime-)
)
:effect
(and
(Flag90-)
(Flag90prime-)
)

)
(:action Flag90Action-33
:parameters ()
:precondition
(and
(Flag43-)
(Flag43prime-)
)
:effect
(and
(Flag90-)
(Flag90prime-)
)

)
(:action Flag90Action-34
:parameters ()
:precondition
(and
(Flag79-)
(Flag79prime-)
)
:effect
(and
(Flag90-)
(Flag90prime-)
)

)
(:action Flag90Action-35
:parameters ()
:precondition
(and
(Flag70-)
(Flag70prime-)
)
:effect
(and
(Flag90-)
(Flag90prime-)
)

)
(:action Flag90Action-36
:parameters ()
:precondition
(and
(Flag71-)
(Flag71prime-)
)
:effect
(and
(Flag90-)
(Flag90prime-)
)

)
(:action Flag90Action-37
:parameters ()
:precondition
(and
(Flag89-)
(Flag89prime-)
)
:effect
(and
(Flag90-)
(Flag90prime-)
)

)
(:action Flag90Action-38
:parameters ()
:precondition
(and
(Flag76-)
(Flag76prime-)
)
:effect
(and
(Flag90-)
(Flag90prime-)
)

)
(:action Flag90Action-39
:parameters ()
:precondition
(and
(Flag59-)
(Flag59prime-)
)
:effect
(and
(Flag90-)
(Flag90prime-)
)

)
(:action Flag90Action-40
:parameters ()
:precondition
(and
(Flag80-)
(Flag80prime-)
)
:effect
(and
(Flag90-)
(Flag90prime-)
)

)
(:action Flag90Action-41
:parameters ()
:precondition
(and
(Flag46-)
(Flag46prime-)
)
:effect
(and
(Flag90-)
(Flag90prime-)
)

)
(:action Flag90Action-42
:parameters ()
:precondition
(and
(Flag57-)
(Flag57prime-)
)
:effect
(and
(Flag90-)
(Flag90prime-)
)

)
(:action Prim90Action
:parameters ()
:precondition
(and
(not (Flag49-))
(Flag49prime-)
(not (Flag82-))
(Flag82prime-)
(not (Flag50-))
(Flag50prime-)
(not (Flag51-))
(Flag51prime-)
(not (Flag27-))
(Flag27prime-)
(not (Flag29-))
(Flag29prime-)
(not (Flag83-))
(Flag83prime-)
(not (Flag74-))
(Flag74prime-)
(not (Flag41-))
(Flag41prime-)
(not (Flag36-))
(Flag36prime-)
(not (Flag30-))
(Flag30prime-)
(not (Flag45-))
(Flag45prime-)
(not (Flag63-))
(Flag63prime-)
(not (Flag32-))
(Flag32prime-)
(not (Flag84-))
(Flag84prime-)
(not (Flag85-))
(Flag85prime-)
(not (Flag52-))
(Flag52prime-)
(not (Flag65-))
(Flag65prime-)
(not (Flag75-))
(Flag75prime-)
(not (Flag66-))
(Flag66prime-)
(not (Flag67-))
(Flag67prime-)
(not (Flag34-))
(Flag34prime-)
(not (Flag37-))
(Flag37prime-)
(not (Flag55-))
(Flag55prime-)
(not (Flag86-))
(Flag86prime-)
(not (Flag78-))
(Flag78prime-)
(not (Flag38-))
(Flag38prime-)
(not (Flag39-))
(Flag39prime-)
(not (Flag69-))
(Flag69prime-)
(not (Flag87-))
(Flag87prime-)
(not (Flag62-))
(Flag62prime-)
(not (Flag88-))
(Flag88prime-)
(not (Flag42-))
(Flag42prime-)
(not (Flag43-))
(Flag43prime-)
(not (Flag79-))
(Flag79prime-)
(not (Flag70-))
(Flag70prime-)
(not (Flag71-))
(Flag71prime-)
(not (Flag89-))
(Flag89prime-)
(not (Flag76-))
(Flag76prime-)
(not (Flag59-))
(Flag59prime-)
(not (Flag80-))
(Flag80prime-)
(not (Flag46-))
(Flag46prime-)
(not (Flag57-))
(Flag57prime-)
)
:effect
(and
(Flag90prime-)
(not (Flag90-))
)

)
(:action Flag99Action-0
:parameters ()
:precondition
(and
(Flag63-)
(Flag63prime-)
)
:effect
(and
(Flag99-)
(Flag99prime-)
)

)
(:action Flag99Action-1
:parameters ()
:precondition
(and
(Flag91-)
(Flag91prime-)
)
:effect
(and
(Flag99-)
(Flag99prime-)
)

)
(:action Flag99Action-2
:parameters ()
:precondition
(and
(Flag82-)
(Flag82prime-)
)
:effect
(and
(Flag99-)
(Flag99prime-)
)

)
(:action Flag99Action-3
:parameters ()
:precondition
(and
(Flag50-)
(Flag50prime-)
)
:effect
(and
(Flag99-)
(Flag99prime-)
)

)
(:action Flag99Action-4
:parameters ()
:precondition
(and
(Flag89-)
(Flag89prime-)
)
:effect
(and
(Flag99-)
(Flag99prime-)
)

)
(:action Flag99Action-5
:parameters ()
:precondition
(and
(Flag51-)
(Flag51prime-)
)
:effect
(and
(Flag99-)
(Flag99prime-)
)

)
(:action Flag99Action-6
:parameters ()
:precondition
(and
(Flag92-)
(Flag92prime-)
)
:effect
(and
(Flag99-)
(Flag99prime-)
)

)
(:action Flag99Action-7
:parameters ()
:precondition
(and
(Flag27-)
(Flag27prime-)
)
:effect
(and
(Flag99-)
(Flag99prime-)
)

)
(:action Flag99Action-8
:parameters ()
:precondition
(and
(Flag93-)
(Flag93prime-)
)
:effect
(and
(Flag99-)
(Flag99prime-)
)

)
(:action Flag99Action-9
:parameters ()
:precondition
(and
(Flag29-)
(Flag29prime-)
)
:effect
(and
(Flag99-)
(Flag99prime-)
)

)
(:action Flag99Action-10
:parameters ()
:precondition
(and
(Flag74-)
(Flag74prime-)
)
:effect
(and
(Flag99-)
(Flag99prime-)
)

)
(:action Flag99Action-11
:parameters ()
:precondition
(and
(Flag41-)
(Flag41prime-)
)
:effect
(and
(Flag99-)
(Flag99prime-)
)

)
(:action Flag99Action-12
:parameters ()
:precondition
(and
(Flag30-)
(Flag30prime-)
)
:effect
(and
(Flag99-)
(Flag99prime-)
)

)
(:action Flag99Action-13
:parameters ()
:precondition
(and
(Flag94-)
(Flag94prime-)
)
:effect
(and
(Flag99-)
(Flag99prime-)
)

)
(:action Flag99Action-14
:parameters ()
:precondition
(and
(Flag84-)
(Flag84prime-)
)
:effect
(and
(Flag99-)
(Flag99prime-)
)

)
(:action Flag99Action-15
:parameters ()
:precondition
(and
(Flag52-)
(Flag52prime-)
)
:effect
(and
(Flag99-)
(Flag99prime-)
)

)
(:action Flag99Action-16
:parameters ()
:precondition
(and
(Flag65-)
(Flag65prime-)
)
:effect
(and
(Flag99-)
(Flag99prime-)
)

)
(:action Flag99Action-17
:parameters ()
:precondition
(and
(Flag75-)
(Flag75prime-)
)
:effect
(and
(Flag99-)
(Flag99prime-)
)

)
(:action Flag99Action-18
:parameters ()
:precondition
(and
(Flag66-)
(Flag66prime-)
)
:effect
(and
(Flag99-)
(Flag99prime-)
)

)
(:action Flag99Action-19
:parameters ()
:precondition
(and
(Flag95-)
(Flag95prime-)
)
:effect
(and
(Flag99-)
(Flag99prime-)
)

)
(:action Flag99Action-20
:parameters ()
:precondition
(and
(Flag34-)
(Flag34prime-)
)
:effect
(and
(Flag99-)
(Flag99prime-)
)

)
(:action Flag99Action-21
:parameters ()
:precondition
(and
(Flag36-)
(Flag36prime-)
)
:effect
(and
(Flag99-)
(Flag99prime-)
)

)
(:action Flag99Action-22
:parameters ()
:precondition
(and
(Flag55-)
(Flag55prime-)
)
:effect
(and
(Flag99-)
(Flag99prime-)
)

)
(:action Flag99Action-23
:parameters ()
:precondition
(and
(Flag37-)
(Flag37prime-)
)
:effect
(and
(Flag99-)
(Flag99prime-)
)

)
(:action Flag99Action-24
:parameters ()
:precondition
(and
(Flag78-)
(Flag78prime-)
)
:effect
(and
(Flag99-)
(Flag99prime-)
)

)
(:action Flag99Action-25
:parameters ()
:precondition
(and
(Flag38-)
(Flag38prime-)
)
:effect
(and
(Flag99-)
(Flag99prime-)
)

)
(:action Flag99Action-26
:parameters ()
:precondition
(and
(Flag39-)
(Flag39prime-)
)
:effect
(and
(Flag99-)
(Flag99prime-)
)

)
(:action Flag99Action-27
:parameters ()
:precondition
(and
(Flag69-)
(Flag69prime-)
)
:effect
(and
(Flag99-)
(Flag99prime-)
)

)
(:action Flag99Action-28
:parameters ()
:precondition
(and
(Flag87-)
(Flag87prime-)
)
:effect
(and
(Flag99-)
(Flag99prime-)
)

)
(:action Flag99Action-29
:parameters ()
:precondition
(and
(Flag62-)
(Flag62prime-)
)
:effect
(and
(Flag99-)
(Flag99prime-)
)

)
(:action Flag99Action-30
:parameters ()
:precondition
(and
(Flag88-)
(Flag88prime-)
)
:effect
(and
(Flag99-)
(Flag99prime-)
)

)
(:action Flag99Action-31
:parameters ()
:precondition
(and
(Flag42-)
(Flag42prime-)
)
:effect
(and
(Flag99-)
(Flag99prime-)
)

)
(:action Flag99Action-32
:parameters ()
:precondition
(and
(Flag43-)
(Flag43prime-)
)
:effect
(and
(Flag99-)
(Flag99prime-)
)

)
(:action Flag99Action-33
:parameters ()
:precondition
(and
(Flag79-)
(Flag79prime-)
)
:effect
(and
(Flag99-)
(Flag99prime-)
)

)
(:action Flag99Action-34
:parameters ()
:precondition
(and
(Flag70-)
(Flag70prime-)
)
:effect
(and
(Flag99-)
(Flag99prime-)
)

)
(:action Flag99Action-35
:parameters ()
:precondition
(and
(Flag96-)
(Flag96prime-)
)
:effect
(and
(Flag99-)
(Flag99prime-)
)

)
(:action Flag99Action-36
:parameters ()
:precondition
(and
(Flag97-)
(Flag97prime-)
)
:effect
(and
(Flag99-)
(Flag99prime-)
)

)
(:action Flag99Action-37
:parameters ()
:precondition
(and
(Flag98-)
(Flag98prime-)
)
:effect
(and
(Flag99-)
(Flag99prime-)
)

)
(:action Flag99Action-38
:parameters ()
:precondition
(and
(Flag71-)
(Flag71prime-)
)
:effect
(and
(Flag99-)
(Flag99prime-)
)

)
(:action Flag99Action-39
:parameters ()
:precondition
(and
(Flag76-)
(Flag76prime-)
)
:effect
(and
(Flag99-)
(Flag99prime-)
)

)
(:action Flag99Action-40
:parameters ()
:precondition
(and
(Flag59-)
(Flag59prime-)
)
:effect
(and
(Flag99-)
(Flag99prime-)
)

)
(:action Flag99Action-41
:parameters ()
:precondition
(and
(Flag80-)
(Flag80prime-)
)
:effect
(and
(Flag99-)
(Flag99prime-)
)

)
(:action Flag99Action-42
:parameters ()
:precondition
(and
(Flag46-)
(Flag46prime-)
)
:effect
(and
(Flag99-)
(Flag99prime-)
)

)
(:action Flag99Action-43
:parameters ()
:precondition
(and
(Flag57-)
(Flag57prime-)
)
:effect
(and
(Flag99-)
(Flag99prime-)
)

)
(:action Prim99Action
:parameters ()
:precondition
(and
(not (Flag63-))
(Flag63prime-)
(not (Flag91-))
(Flag91prime-)
(not (Flag82-))
(Flag82prime-)
(not (Flag50-))
(Flag50prime-)
(not (Flag89-))
(Flag89prime-)
(not (Flag51-))
(Flag51prime-)
(not (Flag92-))
(Flag92prime-)
(not (Flag27-))
(Flag27prime-)
(not (Flag93-))
(Flag93prime-)
(not (Flag29-))
(Flag29prime-)
(not (Flag74-))
(Flag74prime-)
(not (Flag41-))
(Flag41prime-)
(not (Flag30-))
(Flag30prime-)
(not (Flag94-))
(Flag94prime-)
(not (Flag84-))
(Flag84prime-)
(not (Flag52-))
(Flag52prime-)
(not (Flag65-))
(Flag65prime-)
(not (Flag75-))
(Flag75prime-)
(not (Flag66-))
(Flag66prime-)
(not (Flag95-))
(Flag95prime-)
(not (Flag34-))
(Flag34prime-)
(not (Flag36-))
(Flag36prime-)
(not (Flag55-))
(Flag55prime-)
(not (Flag37-))
(Flag37prime-)
(not (Flag78-))
(Flag78prime-)
(not (Flag38-))
(Flag38prime-)
(not (Flag39-))
(Flag39prime-)
(not (Flag69-))
(Flag69prime-)
(not (Flag87-))
(Flag87prime-)
(not (Flag62-))
(Flag62prime-)
(not (Flag88-))
(Flag88prime-)
(not (Flag42-))
(Flag42prime-)
(not (Flag43-))
(Flag43prime-)
(not (Flag79-))
(Flag79prime-)
(not (Flag70-))
(Flag70prime-)
(not (Flag96-))
(Flag96prime-)
(not (Flag97-))
(Flag97prime-)
(not (Flag98-))
(Flag98prime-)
(not (Flag71-))
(Flag71prime-)
(not (Flag76-))
(Flag76prime-)
(not (Flag59-))
(Flag59prime-)
(not (Flag80-))
(Flag80prime-)
(not (Flag46-))
(Flag46prime-)
(not (Flag57-))
(Flag57prime-)
)
:effect
(and
(Flag99prime-)
(not (Flag99-))
)

)
(:action Flag106Action-0
:parameters ()
:precondition
(and
(Flag63-)
(Flag63prime-)
)
:effect
(and
(Flag106-)
(Flag106prime-)
)

)
(:action Flag106Action-1
:parameters ()
:precondition
(and
(Flag82-)
(Flag82prime-)
)
:effect
(and
(Flag106-)
(Flag106prime-)
)

)
(:action Flag106Action-2
:parameters ()
:precondition
(and
(Flag89-)
(Flag89prime-)
)
:effect
(and
(Flag106-)
(Flag106prime-)
)

)
(:action Flag106Action-3
:parameters ()
:precondition
(and
(Flag51-)
(Flag51prime-)
)
:effect
(and
(Flag106-)
(Flag106prime-)
)

)
(:action Flag106Action-4
:parameters ()
:precondition
(and
(Flag92-)
(Flag92prime-)
)
:effect
(and
(Flag106-)
(Flag106prime-)
)

)
(:action Flag106Action-5
:parameters ()
:precondition
(and
(Flag27-)
(Flag27prime-)
)
:effect
(and
(Flag106-)
(Flag106prime-)
)

)
(:action Flag106Action-6
:parameters ()
:precondition
(and
(Flag39-)
(Flag39prime-)
)
:effect
(and
(Flag106-)
(Flag106prime-)
)

)
(:action Flag106Action-7
:parameters ()
:precondition
(and
(Flag29-)
(Flag29prime-)
)
:effect
(and
(Flag106-)
(Flag106prime-)
)

)
(:action Flag106Action-8
:parameters ()
:precondition
(and
(Flag74-)
(Flag74prime-)
)
:effect
(and
(Flag106-)
(Flag106prime-)
)

)
(:action Flag106Action-9
:parameters ()
:precondition
(and
(Flag41-)
(Flag41prime-)
)
:effect
(and
(Flag106-)
(Flag106prime-)
)

)
(:action Flag106Action-10
:parameters ()
:precondition
(and
(Flag30-)
(Flag30prime-)
)
:effect
(and
(Flag106-)
(Flag106prime-)
)

)
(:action Flag106Action-11
:parameters ()
:precondition
(and
(Flag94-)
(Flag94prime-)
)
:effect
(and
(Flag106-)
(Flag106prime-)
)

)
(:action Flag106Action-12
:parameters ()
:precondition
(and
(Flag84-)
(Flag84prime-)
)
:effect
(and
(Flag106-)
(Flag106prime-)
)

)
(:action Flag106Action-13
:parameters ()
:precondition
(and
(Flag100-)
(Flag100prime-)
)
:effect
(and
(Flag106-)
(Flag106prime-)
)

)
(:action Flag106Action-14
:parameters ()
:precondition
(and
(Flag52-)
(Flag52prime-)
)
:effect
(and
(Flag106-)
(Flag106prime-)
)

)
(:action Flag106Action-15
:parameters ()
:precondition
(and
(Flag65-)
(Flag65prime-)
)
:effect
(and
(Flag106-)
(Flag106prime-)
)

)
(:action Flag106Action-16
:parameters ()
:precondition
(and
(Flag75-)
(Flag75prime-)
)
:effect
(and
(Flag106-)
(Flag106prime-)
)

)
(:action Flag106Action-17
:parameters ()
:precondition
(and
(Flag66-)
(Flag66prime-)
)
:effect
(and
(Flag106-)
(Flag106prime-)
)

)
(:action Flag106Action-18
:parameters ()
:precondition
(and
(Flag95-)
(Flag95prime-)
)
:effect
(and
(Flag106-)
(Flag106prime-)
)

)
(:action Flag106Action-19
:parameters ()
:precondition
(and
(Flag76-)
(Flag76prime-)
)
:effect
(and
(Flag106-)
(Flag106prime-)
)

)
(:action Flag106Action-20
:parameters ()
:precondition
(and
(Flag101-)
(Flag101prime-)
)
:effect
(and
(Flag106-)
(Flag106prime-)
)

)
(:action Flag106Action-21
:parameters ()
:precondition
(and
(Flag36-)
(Flag36prime-)
)
:effect
(and
(Flag106-)
(Flag106prime-)
)

)
(:action Flag106Action-22
:parameters ()
:precondition
(and
(Flag55-)
(Flag55prime-)
)
:effect
(and
(Flag106-)
(Flag106prime-)
)

)
(:action Flag106Action-23
:parameters ()
:precondition
(and
(Flag78-)
(Flag78prime-)
)
:effect
(and
(Flag106-)
(Flag106prime-)
)

)
(:action Flag106Action-24
:parameters ()
:precondition
(and
(Flag102-)
(Flag102prime-)
)
:effect
(and
(Flag106-)
(Flag106prime-)
)

)
(:action Flag106Action-25
:parameters ()
:precondition
(and
(Flag38-)
(Flag38prime-)
)
:effect
(and
(Flag106-)
(Flag106prime-)
)

)
(:action Flag106Action-26
:parameters ()
:precondition
(and
(Flag57-)
(Flag57prime-)
)
:effect
(and
(Flag106-)
(Flag106prime-)
)

)
(:action Flag106Action-27
:parameters ()
:precondition
(and
(Flag69-)
(Flag69prime-)
)
:effect
(and
(Flag106-)
(Flag106prime-)
)

)
(:action Flag106Action-28
:parameters ()
:precondition
(and
(Flag87-)
(Flag87prime-)
)
:effect
(and
(Flag106-)
(Flag106prime-)
)

)
(:action Flag106Action-29
:parameters ()
:precondition
(and
(Flag88-)
(Flag88prime-)
)
:effect
(and
(Flag106-)
(Flag106prime-)
)

)
(:action Flag106Action-30
:parameters ()
:precondition
(and
(Flag42-)
(Flag42prime-)
)
:effect
(and
(Flag106-)
(Flag106prime-)
)

)
(:action Flag106Action-31
:parameters ()
:precondition
(and
(Flag43-)
(Flag43prime-)
)
:effect
(and
(Flag106-)
(Flag106prime-)
)

)
(:action Flag106Action-32
:parameters ()
:precondition
(and
(Flag103-)
(Flag103prime-)
)
:effect
(and
(Flag106-)
(Flag106prime-)
)

)
(:action Flag106Action-33
:parameters ()
:precondition
(and
(Flag79-)
(Flag79prime-)
)
:effect
(and
(Flag106-)
(Flag106prime-)
)

)
(:action Flag106Action-34
:parameters ()
:precondition
(and
(Flag70-)
(Flag70prime-)
)
:effect
(and
(Flag106-)
(Flag106prime-)
)

)
(:action Flag106Action-35
:parameters ()
:precondition
(and
(Flag71-)
(Flag71prime-)
)
:effect
(and
(Flag106-)
(Flag106prime-)
)

)
(:action Flag106Action-36
:parameters ()
:precondition
(and
(Flag97-)
(Flag97prime-)
)
:effect
(and
(Flag106-)
(Flag106prime-)
)

)
(:action Flag106Action-37
:parameters ()
:precondition
(and
(Flag98-)
(Flag98prime-)
)
:effect
(and
(Flag106-)
(Flag106prime-)
)

)
(:action Flag106Action-38
:parameters ()
:precondition
(and
(Flag104-)
(Flag104prime-)
)
:effect
(and
(Flag106-)
(Flag106prime-)
)

)
(:action Flag106Action-39
:parameters ()
:precondition
(and
(Flag59-)
(Flag59prime-)
)
:effect
(and
(Flag106-)
(Flag106prime-)
)

)
(:action Flag106Action-40
:parameters ()
:precondition
(and
(Flag46-)
(Flag46prime-)
)
:effect
(and
(Flag106-)
(Flag106prime-)
)

)
(:action Flag106Action-41
:parameters ()
:precondition
(and
(Flag105-)
(Flag105prime-)
)
:effect
(and
(Flag106-)
(Flag106prime-)
)

)
(:action Prim106Action
:parameters ()
:precondition
(and
(not (Flag63-))
(Flag63prime-)
(not (Flag82-))
(Flag82prime-)
(not (Flag89-))
(Flag89prime-)
(not (Flag51-))
(Flag51prime-)
(not (Flag92-))
(Flag92prime-)
(not (Flag27-))
(Flag27prime-)
(not (Flag39-))
(Flag39prime-)
(not (Flag29-))
(Flag29prime-)
(not (Flag74-))
(Flag74prime-)
(not (Flag41-))
(Flag41prime-)
(not (Flag30-))
(Flag30prime-)
(not (Flag94-))
(Flag94prime-)
(not (Flag84-))
(Flag84prime-)
(not (Flag100-))
(Flag100prime-)
(not (Flag52-))
(Flag52prime-)
(not (Flag65-))
(Flag65prime-)
(not (Flag75-))
(Flag75prime-)
(not (Flag66-))
(Flag66prime-)
(not (Flag95-))
(Flag95prime-)
(not (Flag76-))
(Flag76prime-)
(not (Flag101-))
(Flag101prime-)
(not (Flag36-))
(Flag36prime-)
(not (Flag55-))
(Flag55prime-)
(not (Flag78-))
(Flag78prime-)
(not (Flag102-))
(Flag102prime-)
(not (Flag38-))
(Flag38prime-)
(not (Flag57-))
(Flag57prime-)
(not (Flag69-))
(Flag69prime-)
(not (Flag87-))
(Flag87prime-)
(not (Flag88-))
(Flag88prime-)
(not (Flag42-))
(Flag42prime-)
(not (Flag43-))
(Flag43prime-)
(not (Flag103-))
(Flag103prime-)
(not (Flag79-))
(Flag79prime-)
(not (Flag70-))
(Flag70prime-)
(not (Flag71-))
(Flag71prime-)
(not (Flag97-))
(Flag97prime-)
(not (Flag98-))
(Flag98prime-)
(not (Flag104-))
(Flag104prime-)
(not (Flag59-))
(Flag59prime-)
(not (Flag46-))
(Flag46prime-)
(not (Flag105-))
(Flag105prime-)
)
:effect
(and
(Flag106prime-)
(not (Flag106-))
)

)
(:action Flag112Action-0
:parameters ()
:precondition
(and
(Flag63-)
(Flag63prime-)
)
:effect
(and
(Flag112-)
(Flag112prime-)
)

)
(:action Flag112Action-1
:parameters ()
:precondition
(and
(Flag97-)
(Flag97prime-)
)
:effect
(and
(Flag112-)
(Flag112prime-)
)

)
(:action Flag112Action-2
:parameters ()
:precondition
(and
(Flag82-)
(Flag82prime-)
)
:effect
(and
(Flag112-)
(Flag112prime-)
)

)
(:action Flag112Action-3
:parameters ()
:precondition
(and
(Flag89-)
(Flag89prime-)
)
:effect
(and
(Flag112-)
(Flag112prime-)
)

)
(:action Flag112Action-4
:parameters ()
:precondition
(and
(Flag107-)
(Flag107prime-)
)
:effect
(and
(Flag112-)
(Flag112prime-)
)

)
(:action Flag112Action-5
:parameters ()
:precondition
(and
(Flag92-)
(Flag92prime-)
)
:effect
(and
(Flag112-)
(Flag112prime-)
)

)
(:action Flag112Action-6
:parameters ()
:precondition
(and
(Flag27-)
(Flag27prime-)
)
:effect
(and
(Flag112-)
(Flag112prime-)
)

)
(:action Flag112Action-7
:parameters ()
:precondition
(and
(Flag39-)
(Flag39prime-)
)
:effect
(and
(Flag112-)
(Flag112prime-)
)

)
(:action Flag112Action-8
:parameters ()
:precondition
(and
(Flag108-)
(Flag108prime-)
)
:effect
(and
(Flag112-)
(Flag112prime-)
)

)
(:action Flag112Action-9
:parameters ()
:precondition
(and
(Flag74-)
(Flag74prime-)
)
:effect
(and
(Flag112-)
(Flag112prime-)
)

)
(:action Flag112Action-10
:parameters ()
:precondition
(and
(Flag41-)
(Flag41prime-)
)
:effect
(and
(Flag112-)
(Flag112prime-)
)

)
(:action Flag112Action-11
:parameters ()
:precondition
(and
(Flag94-)
(Flag94prime-)
)
:effect
(and
(Flag112-)
(Flag112prime-)
)

)
(:action Flag112Action-12
:parameters ()
:precondition
(and
(Flag100-)
(Flag100prime-)
)
:effect
(and
(Flag112-)
(Flag112prime-)
)

)
(:action Flag112Action-13
:parameters ()
:precondition
(and
(Flag52-)
(Flag52prime-)
)
:effect
(and
(Flag112-)
(Flag112prime-)
)

)
(:action Flag112Action-14
:parameters ()
:precondition
(and
(Flag65-)
(Flag65prime-)
)
:effect
(and
(Flag112-)
(Flag112prime-)
)

)
(:action Flag112Action-15
:parameters ()
:precondition
(and
(Flag75-)
(Flag75prime-)
)
:effect
(and
(Flag112-)
(Flag112prime-)
)

)
(:action Flag112Action-16
:parameters ()
:precondition
(and
(Flag76-)
(Flag76prime-)
)
:effect
(and
(Flag112-)
(Flag112prime-)
)

)
(:action Flag112Action-17
:parameters ()
:precondition
(and
(Flag95-)
(Flag95prime-)
)
:effect
(and
(Flag112-)
(Flag112prime-)
)

)
(:action Flag112Action-18
:parameters ()
:precondition
(and
(Flag36-)
(Flag36prime-)
)
:effect
(and
(Flag112-)
(Flag112prime-)
)

)
(:action Flag112Action-19
:parameters ()
:precondition
(and
(Flag55-)
(Flag55prime-)
)
:effect
(and
(Flag112-)
(Flag112prime-)
)

)
(:action Flag112Action-20
:parameters ()
:precondition
(and
(Flag102-)
(Flag102prime-)
)
:effect
(and
(Flag112-)
(Flag112prime-)
)

)
(:action Flag112Action-21
:parameters ()
:precondition
(and
(Flag38-)
(Flag38prime-)
)
:effect
(and
(Flag112-)
(Flag112prime-)
)

)
(:action Flag112Action-22
:parameters ()
:precondition
(and
(Flag57-)
(Flag57prime-)
)
:effect
(and
(Flag112-)
(Flag112prime-)
)

)
(:action Flag112Action-23
:parameters ()
:precondition
(and
(Flag69-)
(Flag69prime-)
)
:effect
(and
(Flag112-)
(Flag112prime-)
)

)
(:action Flag112Action-24
:parameters ()
:precondition
(and
(Flag87-)
(Flag87prime-)
)
:effect
(and
(Flag112-)
(Flag112prime-)
)

)
(:action Flag112Action-25
:parameters ()
:precondition
(and
(Flag88-)
(Flag88prime-)
)
:effect
(and
(Flag112-)
(Flag112prime-)
)

)
(:action Flag112Action-26
:parameters ()
:precondition
(and
(Flag42-)
(Flag42prime-)
)
:effect
(and
(Flag112-)
(Flag112prime-)
)

)
(:action Flag112Action-27
:parameters ()
:precondition
(and
(Flag43-)
(Flag43prime-)
)
:effect
(and
(Flag112-)
(Flag112prime-)
)

)
(:action Flag112Action-28
:parameters ()
:precondition
(and
(Flag79-)
(Flag79prime-)
)
:effect
(and
(Flag112-)
(Flag112prime-)
)

)
(:action Flag112Action-29
:parameters ()
:precondition
(and
(Flag70-)
(Flag70prime-)
)
:effect
(and
(Flag112-)
(Flag112prime-)
)

)
(:action Flag112Action-30
:parameters ()
:precondition
(and
(Flag109-)
(Flag109prime-)
)
:effect
(and
(Flag112-)
(Flag112prime-)
)

)
(:action Flag112Action-31
:parameters ()
:precondition
(and
(Flag71-)
(Flag71prime-)
)
:effect
(and
(Flag112-)
(Flag112prime-)
)

)
(:action Flag112Action-32
:parameters ()
:precondition
(and
(Flag110-)
(Flag110prime-)
)
:effect
(and
(Flag112-)
(Flag112prime-)
)

)
(:action Flag112Action-33
:parameters ()
:precondition
(and
(Flag98-)
(Flag98prime-)
)
:effect
(and
(Flag112-)
(Flag112prime-)
)

)
(:action Flag112Action-34
:parameters ()
:precondition
(and
(Flag59-)
(Flag59prime-)
)
:effect
(and
(Flag112-)
(Flag112prime-)
)

)
(:action Flag112Action-35
:parameters ()
:precondition
(and
(Flag111-)
(Flag111prime-)
)
:effect
(and
(Flag112-)
(Flag112prime-)
)

)
(:action Flag112Action-36
:parameters ()
:precondition
(and
(Flag46-)
(Flag46prime-)
)
:effect
(and
(Flag112-)
(Flag112prime-)
)

)
(:action Flag112Action-37
:parameters ()
:precondition
(and
(Flag105-)
(Flag105prime-)
)
:effect
(and
(Flag112-)
(Flag112prime-)
)

)
(:action Prim112Action
:parameters ()
:precondition
(and
(not (Flag63-))
(Flag63prime-)
(not (Flag97-))
(Flag97prime-)
(not (Flag82-))
(Flag82prime-)
(not (Flag89-))
(Flag89prime-)
(not (Flag107-))
(Flag107prime-)
(not (Flag92-))
(Flag92prime-)
(not (Flag27-))
(Flag27prime-)
(not (Flag39-))
(Flag39prime-)
(not (Flag108-))
(Flag108prime-)
(not (Flag74-))
(Flag74prime-)
(not (Flag41-))
(Flag41prime-)
(not (Flag94-))
(Flag94prime-)
(not (Flag100-))
(Flag100prime-)
(not (Flag52-))
(Flag52prime-)
(not (Flag65-))
(Flag65prime-)
(not (Flag75-))
(Flag75prime-)
(not (Flag76-))
(Flag76prime-)
(not (Flag95-))
(Flag95prime-)
(not (Flag36-))
(Flag36prime-)
(not (Flag55-))
(Flag55prime-)
(not (Flag102-))
(Flag102prime-)
(not (Flag38-))
(Flag38prime-)
(not (Flag57-))
(Flag57prime-)
(not (Flag69-))
(Flag69prime-)
(not (Flag87-))
(Flag87prime-)
(not (Flag88-))
(Flag88prime-)
(not (Flag42-))
(Flag42prime-)
(not (Flag43-))
(Flag43prime-)
(not (Flag79-))
(Flag79prime-)
(not (Flag70-))
(Flag70prime-)
(not (Flag109-))
(Flag109prime-)
(not (Flag71-))
(Flag71prime-)
(not (Flag110-))
(Flag110prime-)
(not (Flag98-))
(Flag98prime-)
(not (Flag59-))
(Flag59prime-)
(not (Flag111-))
(Flag111prime-)
(not (Flag46-))
(Flag46prime-)
(not (Flag105-))
(Flag105prime-)
)
:effect
(and
(Flag112prime-)
(not (Flag112-))
)

)
(:action Flag117Action-0
:parameters ()
:precondition
(and
(Flag63-)
(Flag63prime-)
)
:effect
(and
(Flag117-)
(Flag117prime-)
)

)
(:action Flag117Action-1
:parameters ()
:precondition
(and
(Flag82-)
(Flag82prime-)
)
:effect
(and
(Flag117-)
(Flag117prime-)
)

)
(:action Flag117Action-2
:parameters ()
:precondition
(and
(Flag98-)
(Flag98prime-)
)
:effect
(and
(Flag117-)
(Flag117prime-)
)

)
(:action Flag117Action-3
:parameters ()
:precondition
(and
(Flag107-)
(Flag107prime-)
)
:effect
(and
(Flag117-)
(Flag117prime-)
)

)
(:action Flag117Action-4
:parameters ()
:precondition
(and
(Flag92-)
(Flag92prime-)
)
:effect
(and
(Flag117-)
(Flag117prime-)
)

)
(:action Flag117Action-5
:parameters ()
:precondition
(and
(Flag27-)
(Flag27prime-)
)
:effect
(and
(Flag117-)
(Flag117prime-)
)

)
(:action Flag117Action-6
:parameters ()
:precondition
(and
(Flag113-)
(Flag113prime-)
)
:effect
(and
(Flag117-)
(Flag117prime-)
)

)
(:action Flag117Action-7
:parameters ()
:precondition
(and
(Flag41-)
(Flag41prime-)
)
:effect
(and
(Flag117-)
(Flag117prime-)
)

)
(:action Flag117Action-8
:parameters ()
:precondition
(and
(Flag39-)
(Flag39prime-)
)
:effect
(and
(Flag117-)
(Flag117prime-)
)

)
(:action Flag117Action-9
:parameters ()
:precondition
(and
(Flag94-)
(Flag94prime-)
)
:effect
(and
(Flag117-)
(Flag117prime-)
)

)
(:action Flag117Action-10
:parameters ()
:precondition
(and
(Flag100-)
(Flag100prime-)
)
:effect
(and
(Flag117-)
(Flag117prime-)
)

)
(:action Flag117Action-11
:parameters ()
:precondition
(and
(Flag52-)
(Flag52prime-)
)
:effect
(and
(Flag117-)
(Flag117prime-)
)

)
(:action Flag117Action-12
:parameters ()
:precondition
(and
(Flag75-)
(Flag75prime-)
)
:effect
(and
(Flag117-)
(Flag117prime-)
)

)
(:action Flag117Action-13
:parameters ()
:precondition
(and
(Flag76-)
(Flag76prime-)
)
:effect
(and
(Flag117-)
(Flag117prime-)
)

)
(:action Flag117Action-14
:parameters ()
:precondition
(and
(Flag95-)
(Flag95prime-)
)
:effect
(and
(Flag117-)
(Flag117prime-)
)

)
(:action Flag117Action-15
:parameters ()
:precondition
(and
(Flag114-)
(Flag114prime-)
)
:effect
(and
(Flag117-)
(Flag117prime-)
)

)
(:action Flag117Action-16
:parameters ()
:precondition
(and
(Flag36-)
(Flag36prime-)
)
:effect
(and
(Flag117-)
(Flag117prime-)
)

)
(:action Flag117Action-17
:parameters ()
:precondition
(and
(Flag102-)
(Flag102prime-)
)
:effect
(and
(Flag117-)
(Flag117prime-)
)

)
(:action Flag117Action-18
:parameters ()
:precondition
(and
(Flag38-)
(Flag38prime-)
)
:effect
(and
(Flag117-)
(Flag117prime-)
)

)
(:action Flag117Action-19
:parameters ()
:precondition
(and
(Flag57-)
(Flag57prime-)
)
:effect
(and
(Flag117-)
(Flag117prime-)
)

)
(:action Flag117Action-20
:parameters ()
:precondition
(and
(Flag69-)
(Flag69prime-)
)
:effect
(and
(Flag117-)
(Flag117prime-)
)

)
(:action Flag117Action-21
:parameters ()
:precondition
(and
(Flag88-)
(Flag88prime-)
)
:effect
(and
(Flag117-)
(Flag117prime-)
)

)
(:action Flag117Action-22
:parameters ()
:precondition
(and
(Flag42-)
(Flag42prime-)
)
:effect
(and
(Flag117-)
(Flag117prime-)
)

)
(:action Flag117Action-23
:parameters ()
:precondition
(and
(Flag115-)
(Flag115prime-)
)
:effect
(and
(Flag117-)
(Flag117prime-)
)

)
(:action Flag117Action-24
:parameters ()
:precondition
(and
(Flag116-)
(Flag116prime-)
)
:effect
(and
(Flag117-)
(Flag117prime-)
)

)
(:action Flag117Action-25
:parameters ()
:precondition
(and
(Flag79-)
(Flag79prime-)
)
:effect
(and
(Flag117-)
(Flag117prime-)
)

)
(:action Flag117Action-26
:parameters ()
:precondition
(and
(Flag70-)
(Flag70prime-)
)
:effect
(and
(Flag117-)
(Flag117prime-)
)

)
(:action Flag117Action-27
:parameters ()
:precondition
(and
(Flag71-)
(Flag71prime-)
)
:effect
(and
(Flag117-)
(Flag117prime-)
)

)
(:action Flag117Action-28
:parameters ()
:precondition
(and
(Flag110-)
(Flag110prime-)
)
:effect
(and
(Flag117-)
(Flag117prime-)
)

)
(:action Flag117Action-29
:parameters ()
:precondition
(and
(Flag89-)
(Flag89prime-)
)
:effect
(and
(Flag117-)
(Flag117prime-)
)

)
(:action Flag117Action-30
:parameters ()
:precondition
(and
(Flag59-)
(Flag59prime-)
)
:effect
(and
(Flag117-)
(Flag117prime-)
)

)
(:action Flag117Action-31
:parameters ()
:precondition
(and
(Flag105-)
(Flag105prime-)
)
:effect
(and
(Flag117-)
(Flag117prime-)
)

)
(:action Prim117Action
:parameters ()
:precondition
(and
(not (Flag63-))
(Flag63prime-)
(not (Flag82-))
(Flag82prime-)
(not (Flag98-))
(Flag98prime-)
(not (Flag107-))
(Flag107prime-)
(not (Flag92-))
(Flag92prime-)
(not (Flag27-))
(Flag27prime-)
(not (Flag113-))
(Flag113prime-)
(not (Flag41-))
(Flag41prime-)
(not (Flag39-))
(Flag39prime-)
(not (Flag94-))
(Flag94prime-)
(not (Flag100-))
(Flag100prime-)
(not (Flag52-))
(Flag52prime-)
(not (Flag75-))
(Flag75prime-)
(not (Flag76-))
(Flag76prime-)
(not (Flag95-))
(Flag95prime-)
(not (Flag114-))
(Flag114prime-)
(not (Flag36-))
(Flag36prime-)
(not (Flag102-))
(Flag102prime-)
(not (Flag38-))
(Flag38prime-)
(not (Flag57-))
(Flag57prime-)
(not (Flag69-))
(Flag69prime-)
(not (Flag88-))
(Flag88prime-)
(not (Flag42-))
(Flag42prime-)
(not (Flag115-))
(Flag115prime-)
(not (Flag116-))
(Flag116prime-)
(not (Flag79-))
(Flag79prime-)
(not (Flag70-))
(Flag70prime-)
(not (Flag71-))
(Flag71prime-)
(not (Flag110-))
(Flag110prime-)
(not (Flag89-))
(Flag89prime-)
(not (Flag59-))
(Flag59prime-)
(not (Flag105-))
(Flag105prime-)
)
:effect
(and
(Flag117prime-)
(not (Flag117-))
)

)
(:action Flag120Action-0
:parameters ()
:precondition
(and
(Flag57-)
(Flag57prime-)
)
:effect
(and
(Flag120-)
(Flag120prime-)
)

)
(:action Flag120Action-1
:parameters ()
:precondition
(and
(Flag82-)
(Flag82prime-)
)
:effect
(and
(Flag120-)
(Flag120prime-)
)

)
(:action Flag120Action-2
:parameters ()
:precondition
(and
(Flag107-)
(Flag107prime-)
)
:effect
(and
(Flag120-)
(Flag120prime-)
)

)
(:action Flag120Action-3
:parameters ()
:precondition
(and
(Flag118-)
(Flag118prime-)
)
:effect
(and
(Flag120-)
(Flag120prime-)
)

)
(:action Flag120Action-4
:parameters ()
:precondition
(and
(Flag27-)
(Flag27prime-)
)
:effect
(and
(Flag120-)
(Flag120prime-)
)

)
(:action Flag120Action-5
:parameters ()
:precondition
(and
(Flag119-)
(Flag119prime-)
)
:effect
(and
(Flag120-)
(Flag120prime-)
)

)
(:action Flag120Action-6
:parameters ()
:precondition
(and
(Flag94-)
(Flag94prime-)
)
:effect
(and
(Flag120-)
(Flag120prime-)
)

)
(:action Flag120Action-7
:parameters ()
:precondition
(and
(Flag100-)
(Flag100prime-)
)
:effect
(and
(Flag120-)
(Flag120prime-)
)

)
(:action Flag120Action-8
:parameters ()
:precondition
(and
(Flag75-)
(Flag75prime-)
)
:effect
(and
(Flag120-)
(Flag120prime-)
)

)
(:action Flag120Action-9
:parameters ()
:precondition
(and
(Flag76-)
(Flag76prime-)
)
:effect
(and
(Flag120-)
(Flag120prime-)
)

)
(:action Flag120Action-10
:parameters ()
:precondition
(and
(Flag95-)
(Flag95prime-)
)
:effect
(and
(Flag120-)
(Flag120prime-)
)

)
(:action Flag120Action-11
:parameters ()
:precondition
(and
(Flag114-)
(Flag114prime-)
)
:effect
(and
(Flag120-)
(Flag120prime-)
)

)
(:action Flag120Action-12
:parameters ()
:precondition
(and
(Flag38-)
(Flag38prime-)
)
:effect
(and
(Flag120-)
(Flag120prime-)
)

)
(:action Flag120Action-13
:parameters ()
:precondition
(and
(Flag39-)
(Flag39prime-)
)
:effect
(and
(Flag120-)
(Flag120prime-)
)

)
(:action Flag120Action-14
:parameters ()
:precondition
(and
(Flag69-)
(Flag69prime-)
)
:effect
(and
(Flag120-)
(Flag120prime-)
)

)
(:action Flag120Action-15
:parameters ()
:precondition
(and
(Flag42-)
(Flag42prime-)
)
:effect
(and
(Flag120-)
(Flag120prime-)
)

)
(:action Flag120Action-16
:parameters ()
:precondition
(and
(Flag116-)
(Flag116prime-)
)
:effect
(and
(Flag120-)
(Flag120prime-)
)

)
(:action Flag120Action-17
:parameters ()
:precondition
(and
(Flag70-)
(Flag70prime-)
)
:effect
(and
(Flag120-)
(Flag120prime-)
)

)
(:action Flag120Action-18
:parameters ()
:precondition
(and
(Flag71-)
(Flag71prime-)
)
:effect
(and
(Flag120-)
(Flag120prime-)
)

)
(:action Flag120Action-19
:parameters ()
:precondition
(and
(Flag110-)
(Flag110prime-)
)
:effect
(and
(Flag120-)
(Flag120prime-)
)

)
(:action Flag120Action-20
:parameters ()
:precondition
(and
(Flag98-)
(Flag98prime-)
)
:effect
(and
(Flag120-)
(Flag120prime-)
)

)
(:action Flag120Action-21
:parameters ()
:precondition
(and
(Flag59-)
(Flag59prime-)
)
:effect
(and
(Flag120-)
(Flag120prime-)
)

)
(:action Flag120Action-22
:parameters ()
:precondition
(and
(Flag105-)
(Flag105prime-)
)
:effect
(and
(Flag120-)
(Flag120prime-)
)

)
(:action Prim120Action
:parameters ()
:precondition
(and
(not (Flag57-))
(Flag57prime-)
(not (Flag82-))
(Flag82prime-)
(not (Flag107-))
(Flag107prime-)
(not (Flag118-))
(Flag118prime-)
(not (Flag27-))
(Flag27prime-)
(not (Flag119-))
(Flag119prime-)
(not (Flag94-))
(Flag94prime-)
(not (Flag100-))
(Flag100prime-)
(not (Flag75-))
(Flag75prime-)
(not (Flag76-))
(Flag76prime-)
(not (Flag95-))
(Flag95prime-)
(not (Flag114-))
(Flag114prime-)
(not (Flag38-))
(Flag38prime-)
(not (Flag39-))
(Flag39prime-)
(not (Flag69-))
(Flag69prime-)
(not (Flag42-))
(Flag42prime-)
(not (Flag116-))
(Flag116prime-)
(not (Flag70-))
(Flag70prime-)
(not (Flag71-))
(Flag71prime-)
(not (Flag110-))
(Flag110prime-)
(not (Flag98-))
(Flag98prime-)
(not (Flag59-))
(Flag59prime-)
(not (Flag105-))
(Flag105prime-)
)
:effect
(and
(Flag120prime-)
(not (Flag120-))
)

)
(:action Flag122Action-0
:parameters ()
:precondition
(and
(Flag116-)
(Flag116prime-)
)
:effect
(and
(Flag122-)
(Flag122prime-)
)

)
(:action Flag122Action-1
:parameters ()
:precondition
(and
(Flag70-)
(Flag70prime-)
)
:effect
(and
(Flag122-)
(Flag122prime-)
)

)
(:action Flag122Action-2
:parameters ()
:precondition
(and
(Flag71-)
(Flag71prime-)
)
:effect
(and
(Flag122-)
(Flag122prime-)
)

)
(:action Flag122Action-3
:parameters ()
:precondition
(and
(Flag121-)
(Flag121prime-)
)
:effect
(and
(Flag122-)
(Flag122prime-)
)

)
(:action Flag122Action-4
:parameters ()
:precondition
(and
(Flag110-)
(Flag110prime-)
)
:effect
(and
(Flag122-)
(Flag122prime-)
)

)
(:action Flag122Action-5
:parameters ()
:precondition
(and
(Flag98-)
(Flag98prime-)
)
:effect
(and
(Flag122-)
(Flag122prime-)
)

)
(:action Flag122Action-6
:parameters ()
:precondition
(and
(Flag82-)
(Flag82prime-)
)
:effect
(and
(Flag122-)
(Flag122prime-)
)

)
(:action Flag122Action-7
:parameters ()
:precondition
(and
(Flag39-)
(Flag39prime-)
)
:effect
(and
(Flag122-)
(Flag122prime-)
)

)
(:action Flag122Action-8
:parameters ()
:precondition
(and
(Flag76-)
(Flag76prime-)
)
:effect
(and
(Flag122-)
(Flag122prime-)
)

)
(:action Flag122Action-9
:parameters ()
:precondition
(and
(Flag27-)
(Flag27prime-)
)
:effect
(and
(Flag122-)
(Flag122prime-)
)

)
(:action Flag122Action-10
:parameters ()
:precondition
(and
(Flag105-)
(Flag105prime-)
)
:effect
(and
(Flag122-)
(Flag122prime-)
)

)
(:action Flag122Action-11
:parameters ()
:precondition
(and
(Flag119-)
(Flag119prime-)
)
:effect
(and
(Flag122-)
(Flag122prime-)
)

)
(:action Prim122Action
:parameters ()
:precondition
(and
(not (Flag116-))
(Flag116prime-)
(not (Flag70-))
(Flag70prime-)
(not (Flag71-))
(Flag71prime-)
(not (Flag121-))
(Flag121prime-)
(not (Flag110-))
(Flag110prime-)
(not (Flag98-))
(Flag98prime-)
(not (Flag82-))
(Flag82prime-)
(not (Flag39-))
(Flag39prime-)
(not (Flag76-))
(Flag76prime-)
(not (Flag27-))
(Flag27prime-)
(not (Flag105-))
(Flag105prime-)
(not (Flag119-))
(Flag119prime-)
)
:effect
(and
(Flag122prime-)
(not (Flag122-))
)

)
(:action Flag123Action-0
:parameters ()
:precondition
(and
(LEFTOF1-ROBOT)
)
:effect
(and
(Flag123-)
(Flag123prime-)
)

)
(:action Flag123Action-1
:parameters ()
:precondition
(and
(LEFTOF2-ROBOT)
)
:effect
(and
(Flag123-)
(Flag123prime-)
)

)
(:action Prim123Action
:parameters ()
:precondition
(and
(not (LEFTOF1-ROBOT))
(not (LEFTOF2-ROBOT))
)
:effect
(and
(Flag123prime-)
(not (Flag123-))
)

)
(:action Flag124Action-0
:parameters ()
:precondition
(and
(LEFTOF1-ROBOT)
)
:effect
(and
(Flag124-)
(Flag124prime-)
)

)
(:action Flag124Action-1
:parameters ()
:precondition
(and
(LEFTOF3-ROBOT)
)
:effect
(and
(Flag124-)
(Flag124prime-)
)

)
(:action Flag124Action-2
:parameters ()
:precondition
(and
(LEFTOF2-ROBOT)
)
:effect
(and
(Flag124-)
(Flag124prime-)
)

)
(:action Prim124Action
:parameters ()
:precondition
(and
(not (LEFTOF1-ROBOT))
(not (LEFTOF3-ROBOT))
(not (LEFTOF2-ROBOT))
)
:effect
(and
(Flag124prime-)
(not (Flag124-))
)

)
(:action Flag125Action-0
:parameters ()
:precondition
(and
(LEFTOF1-ROBOT)
)
:effect
(and
(Flag125-)
(Flag125prime-)
)

)
(:action Flag125Action-1
:parameters ()
:precondition
(and
(LEFTOF3-ROBOT)
)
:effect
(and
(Flag125-)
(Flag125prime-)
)

)
(:action Flag125Action-2
:parameters ()
:precondition
(and
(LEFTOF4-ROBOT)
)
:effect
(and
(Flag125-)
(Flag125prime-)
)

)
(:action Flag125Action-3
:parameters ()
:precondition
(and
(LEFTOF2-ROBOT)
)
:effect
(and
(Flag125-)
(Flag125prime-)
)

)
(:action Prim125Action
:parameters ()
:precondition
(and
(not (LEFTOF1-ROBOT))
(not (LEFTOF3-ROBOT))
(not (LEFTOF4-ROBOT))
(not (LEFTOF2-ROBOT))
)
:effect
(and
(Flag125prime-)
(not (Flag125-))
)

)
(:action Flag126Action-0
:parameters ()
:precondition
(and
(LEFTOF5-ROBOT)
)
:effect
(and
(Flag126-)
(Flag126prime-)
)

)
(:action Flag126Action-1
:parameters ()
:precondition
(and
(LEFTOF1-ROBOT)
)
:effect
(and
(Flag126-)
(Flag126prime-)
)

)
(:action Flag126Action-2
:parameters ()
:precondition
(and
(LEFTOF3-ROBOT)
)
:effect
(and
(Flag126-)
(Flag126prime-)
)

)
(:action Flag126Action-3
:parameters ()
:precondition
(and
(LEFTOF4-ROBOT)
)
:effect
(and
(Flag126-)
(Flag126prime-)
)

)
(:action Flag126Action-4
:parameters ()
:precondition
(and
(LEFTOF2-ROBOT)
)
:effect
(and
(Flag126-)
(Flag126prime-)
)

)
(:action Prim126Action
:parameters ()
:precondition
(and
(not (LEFTOF5-ROBOT))
(not (LEFTOF1-ROBOT))
(not (LEFTOF3-ROBOT))
(not (LEFTOF4-ROBOT))
(not (LEFTOF2-ROBOT))
)
:effect
(and
(Flag126prime-)
(not (Flag126-))
)

)
(:action Flag127Action-0
:parameters ()
:precondition
(and
(LEFTOF2-ROBOT)
)
:effect
(and
(Flag127-)
(Flag127prime-)
)

)
(:action Flag127Action-1
:parameters ()
:precondition
(and
(LEFTOF1-ROBOT)
)
:effect
(and
(Flag127-)
(Flag127prime-)
)

)
(:action Flag127Action-2
:parameters ()
:precondition
(and
(LEFTOF6-ROBOT)
)
:effect
(and
(Flag127-)
(Flag127prime-)
)

)
(:action Flag127Action-3
:parameters ()
:precondition
(and
(LEFTOF5-ROBOT)
)
:effect
(and
(Flag127-)
(Flag127prime-)
)

)
(:action Flag127Action-4
:parameters ()
:precondition
(and
(LEFTOF3-ROBOT)
)
:effect
(and
(Flag127-)
(Flag127prime-)
)

)
(:action Flag127Action-5
:parameters ()
:precondition
(and
(LEFTOF4-ROBOT)
)
:effect
(and
(Flag127-)
(Flag127prime-)
)

)
(:action Prim127Action
:parameters ()
:precondition
(and
(not (LEFTOF2-ROBOT))
(not (LEFTOF1-ROBOT))
(not (LEFTOF6-ROBOT))
(not (LEFTOF5-ROBOT))
(not (LEFTOF3-ROBOT))
(not (LEFTOF4-ROBOT))
)
:effect
(and
(Flag127prime-)
(not (Flag127-))
)

)
(:action Flag128Action-0
:parameters ()
:precondition
(and
(LEFTOF2-ROBOT)
)
:effect
(and
(Flag128-)
(Flag128prime-)
)

)
(:action Flag128Action-1
:parameters ()
:precondition
(and
(LEFTOF7-ROBOT)
)
:effect
(and
(Flag128-)
(Flag128prime-)
)

)
(:action Flag128Action-2
:parameters ()
:precondition
(and
(LEFTOF1-ROBOT)
)
:effect
(and
(Flag128-)
(Flag128prime-)
)

)
(:action Flag128Action-3
:parameters ()
:precondition
(and
(LEFTOF6-ROBOT)
)
:effect
(and
(Flag128-)
(Flag128prime-)
)

)
(:action Flag128Action-4
:parameters ()
:precondition
(and
(LEFTOF5-ROBOT)
)
:effect
(and
(Flag128-)
(Flag128prime-)
)

)
(:action Flag128Action-5
:parameters ()
:precondition
(and
(LEFTOF3-ROBOT)
)
:effect
(and
(Flag128-)
(Flag128prime-)
)

)
(:action Flag128Action-6
:parameters ()
:precondition
(and
(LEFTOF4-ROBOT)
)
:effect
(and
(Flag128-)
(Flag128prime-)
)

)
(:action Prim128Action
:parameters ()
:precondition
(and
(not (LEFTOF2-ROBOT))
(not (LEFTOF7-ROBOT))
(not (LEFTOF1-ROBOT))
(not (LEFTOF6-ROBOT))
(not (LEFTOF5-ROBOT))
(not (LEFTOF3-ROBOT))
(not (LEFTOF4-ROBOT))
)
:effect
(and
(Flag128prime-)
(not (Flag128-))
)

)
(:action Flag129Action-0
:parameters ()
:precondition
(and
(LEFTOF2-ROBOT)
)
:effect
(and
(Flag129-)
(Flag129prime-)
)

)
(:action Flag129Action-1
:parameters ()
:precondition
(and
(LEFTOF7-ROBOT)
)
:effect
(and
(Flag129-)
(Flag129prime-)
)

)
(:action Flag129Action-2
:parameters ()
:precondition
(and
(LEFTOF1-ROBOT)
)
:effect
(and
(Flag129-)
(Flag129prime-)
)

)
(:action Flag129Action-3
:parameters ()
:precondition
(and
(LEFTOF6-ROBOT)
)
:effect
(and
(Flag129-)
(Flag129prime-)
)

)
(:action Flag129Action-4
:parameters ()
:precondition
(and
(LEFTOF5-ROBOT)
)
:effect
(and
(Flag129-)
(Flag129prime-)
)

)
(:action Flag129Action-5
:parameters ()
:precondition
(and
(LEFTOF3-ROBOT)
)
:effect
(and
(Flag129-)
(Flag129prime-)
)

)
(:action Flag129Action-6
:parameters ()
:precondition
(and
(LEFTOF4-ROBOT)
)
:effect
(and
(Flag129-)
(Flag129prime-)
)

)
(:action Flag129Action-7
:parameters ()
:precondition
(and
(LEFTOF8-ROBOT)
)
:effect
(and
(Flag129-)
(Flag129prime-)
)

)
(:action Prim129Action
:parameters ()
:precondition
(and
(not (LEFTOF2-ROBOT))
(not (LEFTOF7-ROBOT))
(not (LEFTOF1-ROBOT))
(not (LEFTOF6-ROBOT))
(not (LEFTOF5-ROBOT))
(not (LEFTOF3-ROBOT))
(not (LEFTOF4-ROBOT))
(not (LEFTOF8-ROBOT))
)
:effect
(and
(Flag129prime-)
(not (Flag129-))
)

)
(:action Flag130Action-0
:parameters ()
:precondition
(and
(LEFTOF9-ROBOT)
)
:effect
(and
(Flag130-)
(Flag130prime-)
)

)
(:action Flag130Action-1
:parameters ()
:precondition
(and
(LEFTOF2-ROBOT)
)
:effect
(and
(Flag130-)
(Flag130prime-)
)

)
(:action Flag130Action-2
:parameters ()
:precondition
(and
(LEFTOF7-ROBOT)
)
:effect
(and
(Flag130-)
(Flag130prime-)
)

)
(:action Flag130Action-3
:parameters ()
:precondition
(and
(LEFTOF1-ROBOT)
)
:effect
(and
(Flag130-)
(Flag130prime-)
)

)
(:action Flag130Action-4
:parameters ()
:precondition
(and
(LEFTOF6-ROBOT)
)
:effect
(and
(Flag130-)
(Flag130prime-)
)

)
(:action Flag130Action-5
:parameters ()
:precondition
(and
(LEFTOF5-ROBOT)
)
:effect
(and
(Flag130-)
(Flag130prime-)
)

)
(:action Flag130Action-6
:parameters ()
:precondition
(and
(LEFTOF3-ROBOT)
)
:effect
(and
(Flag130-)
(Flag130prime-)
)

)
(:action Flag130Action-7
:parameters ()
:precondition
(and
(LEFTOF4-ROBOT)
)
:effect
(and
(Flag130-)
(Flag130prime-)
)

)
(:action Flag130Action-8
:parameters ()
:precondition
(and
(LEFTOF8-ROBOT)
)
:effect
(and
(Flag130-)
(Flag130prime-)
)

)
(:action Prim130Action
:parameters ()
:precondition
(and
(not (LEFTOF9-ROBOT))
(not (LEFTOF2-ROBOT))
(not (LEFTOF7-ROBOT))
(not (LEFTOF1-ROBOT))
(not (LEFTOF6-ROBOT))
(not (LEFTOF5-ROBOT))
(not (LEFTOF3-ROBOT))
(not (LEFTOF4-ROBOT))
(not (LEFTOF8-ROBOT))
)
:effect
(and
(Flag130prime-)
(not (Flag130-))
)

)
(:action Flag131Action-0
:parameters ()
:precondition
(and
(LEFTOF10-ROBOT)
)
:effect
(and
(Flag131-)
(Flag131prime-)
)

)
(:action Flag131Action-1
:parameters ()
:precondition
(and
(LEFTOF9-ROBOT)
)
:effect
(and
(Flag131-)
(Flag131prime-)
)

)
(:action Flag131Action-2
:parameters ()
:precondition
(and
(LEFTOF2-ROBOT)
)
:effect
(and
(Flag131-)
(Flag131prime-)
)

)
(:action Flag131Action-3
:parameters ()
:precondition
(and
(LEFTOF7-ROBOT)
)
:effect
(and
(Flag131-)
(Flag131prime-)
)

)
(:action Flag131Action-4
:parameters ()
:precondition
(and
(LEFTOF1-ROBOT)
)
:effect
(and
(Flag131-)
(Flag131prime-)
)

)
(:action Flag131Action-5
:parameters ()
:precondition
(and
(LEFTOF6-ROBOT)
)
:effect
(and
(Flag131-)
(Flag131prime-)
)

)
(:action Flag131Action-6
:parameters ()
:precondition
(and
(LEFTOF5-ROBOT)
)
:effect
(and
(Flag131-)
(Flag131prime-)
)

)
(:action Flag131Action-7
:parameters ()
:precondition
(and
(LEFTOF3-ROBOT)
)
:effect
(and
(Flag131-)
(Flag131prime-)
)

)
(:action Flag131Action-8
:parameters ()
:precondition
(and
(LEFTOF4-ROBOT)
)
:effect
(and
(Flag131-)
(Flag131prime-)
)

)
(:action Flag131Action-9
:parameters ()
:precondition
(and
(LEFTOF8-ROBOT)
)
:effect
(and
(Flag131-)
(Flag131prime-)
)

)
(:action Prim131Action
:parameters ()
:precondition
(and
(not (LEFTOF10-ROBOT))
(not (LEFTOF9-ROBOT))
(not (LEFTOF2-ROBOT))
(not (LEFTOF7-ROBOT))
(not (LEFTOF1-ROBOT))
(not (LEFTOF6-ROBOT))
(not (LEFTOF5-ROBOT))
(not (LEFTOF3-ROBOT))
(not (LEFTOF4-ROBOT))
(not (LEFTOF8-ROBOT))
)
:effect
(and
(Flag131prime-)
(not (Flag131-))
)

)
(:action Flag132Action-0
:parameters ()
:precondition
(and
(LEFTOF10-ROBOT)
)
:effect
(and
(Flag132-)
(Flag132prime-)
)

)
(:action Flag132Action-2
:parameters ()
:precondition
(and
(LEFTOF9-ROBOT)
)
:effect
(and
(Flag132-)
(Flag132prime-)
)

)
(:action Flag132Action-3
:parameters ()
:precondition
(and
(LEFTOF2-ROBOT)
)
:effect
(and
(Flag132-)
(Flag132prime-)
)

)
(:action Flag132Action-4
:parameters ()
:precondition
(and
(LEFTOF7-ROBOT)
)
:effect
(and
(Flag132-)
(Flag132prime-)
)

)
(:action Flag132Action-5
:parameters ()
:precondition
(and
(LEFTOF1-ROBOT)
)
:effect
(and
(Flag132-)
(Flag132prime-)
)

)
(:action Flag132Action-6
:parameters ()
:precondition
(and
(LEFTOF6-ROBOT)
)
:effect
(and
(Flag132-)
(Flag132prime-)
)

)
(:action Flag132Action-7
:parameters ()
:precondition
(and
(LEFTOF5-ROBOT)
)
:effect
(and
(Flag132-)
(Flag132prime-)
)

)
(:action Flag132Action-8
:parameters ()
:precondition
(and
(LEFTOF3-ROBOT)
)
:effect
(and
(Flag132-)
(Flag132prime-)
)

)
(:action Flag132Action-9
:parameters ()
:precondition
(and
(LEFTOF4-ROBOT)
)
:effect
(and
(Flag132-)
(Flag132prime-)
)

)
(:action Flag132Action-10
:parameters ()
:precondition
(and
(LEFTOF8-ROBOT)
)
:effect
(and
(Flag132-)
(Flag132prime-)
)

)
(:action Prim132Action
:parameters ()
:precondition
(and
(not (LEFTOF10-ROBOT))
(not (LEFTOF11-ROBOT))
(not (LEFTOF9-ROBOT))
(not (LEFTOF2-ROBOT))
(not (LEFTOF7-ROBOT))
(not (LEFTOF1-ROBOT))
(not (LEFTOF6-ROBOT))
(not (LEFTOF5-ROBOT))
(not (LEFTOF3-ROBOT))
(not (LEFTOF4-ROBOT))
(not (LEFTOF8-ROBOT))
)
:effect
(and
(Flag132prime-)
(not (Flag132-))
)

)
(:action Flag133Action-0
:parameters ()
:precondition
(and
(LEFTOF10-ROBOT)
)
:effect
(and
(Flag133-)
(Flag133prime-)
)

)
(:action Flag133Action-2
:parameters ()
:precondition
(and
(LEFTOF9-ROBOT)
)
:effect
(and
(Flag133-)
(Flag133prime-)
)

)
(:action Flag133Action-3
:parameters ()
:precondition
(and
(LEFTOF2-ROBOT)
)
:effect
(and
(Flag133-)
(Flag133prime-)
)

)
(:action Flag133Action-4
:parameters ()
:precondition
(and
(LEFTOF7-ROBOT)
)
:effect
(and
(Flag133-)
(Flag133prime-)
)

)
(:action Flag133Action-5
:parameters ()
:precondition
(and
(LEFTOF1-ROBOT)
)
:effect
(and
(Flag133-)
(Flag133prime-)
)

)
(:action Flag133Action-6
:parameters ()
:precondition
(and
(LEFTOF12-ROBOT)
)
:effect
(and
(Flag133-)
(Flag133prime-)
)

)
(:action Flag133Action-7
:parameters ()
:precondition
(and
(LEFTOF6-ROBOT)
)
:effect
(and
(Flag133-)
(Flag133prime-)
)

)
(:action Flag133Action-8
:parameters ()
:precondition
(and
(LEFTOF5-ROBOT)
)
:effect
(and
(Flag133-)
(Flag133prime-)
)

)
(:action Flag133Action-9
:parameters ()
:precondition
(and
(LEFTOF3-ROBOT)
)
:effect
(and
(Flag133-)
(Flag133prime-)
)

)
(:action Flag133Action-10
:parameters ()
:precondition
(and
(LEFTOF4-ROBOT)
)
:effect
(and
(Flag133-)
(Flag133prime-)
)

)
(:action Flag133Action-11
:parameters ()
:precondition
(and
(LEFTOF8-ROBOT)
)
:effect
(and
(Flag133-)
(Flag133prime-)
)

)
(:action Prim133Action
:parameters ()
:precondition
(and
(not (LEFTOF10-ROBOT))
(not (LEFTOF11-ROBOT))
(not (LEFTOF9-ROBOT))
(not (LEFTOF2-ROBOT))
(not (LEFTOF7-ROBOT))
(not (LEFTOF1-ROBOT))
(not (LEFTOF12-ROBOT))
(not (LEFTOF6-ROBOT))
(not (LEFTOF5-ROBOT))
(not (LEFTOF3-ROBOT))
(not (LEFTOF4-ROBOT))
(not (LEFTOF8-ROBOT))
)
:effect
(and
(Flag133prime-)
(not (Flag133-))
)

)
(:action Flag134Action-0
:parameters ()
:precondition
(and
(RIGHTOF3-ROBOT)
)
:effect
(and
(Flag134-)
(Flag134prime-)
)

)
(:action Flag134Action-1
:parameters ()
:precondition
(and
(RIGHTOF8-ROBOT)
)
:effect
(and
(Flag134-)
(Flag134prime-)
)

)
(:action Flag134Action-2
:parameters ()
:precondition
(and
(RIGHTOF5-ROBOT)
)
:effect
(and
(Flag134-)
(Flag134prime-)
)

)
(:action Flag134Action-4
:parameters ()
:precondition
(and
(RIGHTOF11-ROBOT)
)
:effect
(and
(Flag134-)
(Flag134prime-)
)

)
(:action Flag134Action-5
:parameters ()
:precondition
(and
(RIGHTOF10-ROBOT)
)
:effect
(and
(Flag134-)
(Flag134prime-)
)

)
(:action Flag134Action-6
:parameters ()
:precondition
(and
(RIGHTOF4-ROBOT)
)
:effect
(and
(Flag134-)
(Flag134prime-)
)

)
(:action Flag134Action-7
:parameters ()
:precondition
(and
(RIGHTOF1-ROBOT)
)
:effect
(and
(Flag134-)
(Flag134prime-)
)

)
(:action Flag134Action-8
:parameters ()
:precondition
(and
(RIGHTOF7-ROBOT)
)
:effect
(and
(Flag134-)
(Flag134prime-)
)

)
(:action Flag134Action-9
:parameters ()
:precondition
(and
(RIGHTOF9-ROBOT)
)
:effect
(and
(Flag134-)
(Flag134prime-)
)

)
(:action Flag134Action-10
:parameters ()
:precondition
(and
(RIGHTOF6-ROBOT)
)
:effect
(and
(Flag134-)
(Flag134prime-)
)

)
(:action Prim134Action
:parameters ()
:precondition
(and
(not (RIGHTOF3-ROBOT))
(not (RIGHTOF8-ROBOT))
(not (RIGHTOF5-ROBOT))
(not (RIGHTOF2-ROBOT))
(not (RIGHTOF11-ROBOT))
(not (RIGHTOF10-ROBOT))
(not (RIGHTOF4-ROBOT))
(not (RIGHTOF1-ROBOT))
(not (RIGHTOF7-ROBOT))
(not (RIGHTOF9-ROBOT))
(not (RIGHTOF6-ROBOT))
)
:effect
(and
(Flag134prime-)
(not (Flag134-))
)

)
(:action Flag135Action-0
:parameters ()
:precondition
(and
(RIGHTOF3-ROBOT)
)
:effect
(and
(Flag135-)
(Flag135prime-)
)

)
(:action Flag135Action-1
:parameters ()
:precondition
(and
(RIGHTOF5-ROBOT)
)
:effect
(and
(Flag135-)
(Flag135prime-)
)

)
(:action Flag135Action-3
:parameters ()
:precondition
(and
(RIGHTOF11-ROBOT)
)
:effect
(and
(Flag135-)
(Flag135prime-)
)

)
(:action Flag135Action-4
:parameters ()
:precondition
(and
(RIGHTOF10-ROBOT)
)
:effect
(and
(Flag135-)
(Flag135prime-)
)

)
(:action Flag135Action-5
:parameters ()
:precondition
(and
(RIGHTOF4-ROBOT)
)
:effect
(and
(Flag135-)
(Flag135prime-)
)

)
(:action Flag135Action-6
:parameters ()
:precondition
(and
(RIGHTOF8-ROBOT)
)
:effect
(and
(Flag135-)
(Flag135prime-)
)

)
(:action Flag135Action-7
:parameters ()
:precondition
(and
(RIGHTOF7-ROBOT)
)
:effect
(and
(Flag135-)
(Flag135prime-)
)

)
(:action Flag135Action-8
:parameters ()
:precondition
(and
(RIGHTOF9-ROBOT)
)
:effect
(and
(Flag135-)
(Flag135prime-)
)

)
(:action Flag135Action-9
:parameters ()
:precondition
(and
(RIGHTOF6-ROBOT)
)
:effect
(and
(Flag135-)
(Flag135prime-)
)

)
(:action Prim135Action
:parameters ()
:precondition
(and
(not (RIGHTOF3-ROBOT))
(not (RIGHTOF5-ROBOT))
(not (RIGHTOF2-ROBOT))
(not (RIGHTOF11-ROBOT))
(not (RIGHTOF10-ROBOT))
(not (RIGHTOF4-ROBOT))
(not (RIGHTOF8-ROBOT))
(not (RIGHTOF7-ROBOT))
(not (RIGHTOF9-ROBOT))
(not (RIGHTOF6-ROBOT))
)
:effect
(and
(Flag135prime-)
(not (Flag135-))
)

)
(:action Flag136Action-0
:parameters ()
:precondition
(and
(RIGHTOF3-ROBOT)
)
:effect
(and
(Flag136-)
(Flag136prime-)
)

)
(:action Flag136Action-1
:parameters ()
:precondition
(and
(RIGHTOF5-ROBOT)
)
:effect
(and
(Flag136-)
(Flag136prime-)
)

)
(:action Flag136Action-2
:parameters ()
:precondition
(and
(RIGHTOF11-ROBOT)
)
:effect
(and
(Flag136-)
(Flag136prime-)
)

)
(:action Flag136Action-3
:parameters ()
:precondition
(and
(RIGHTOF10-ROBOT)
)
:effect
(and
(Flag136-)
(Flag136prime-)
)

)
(:action Flag136Action-4
:parameters ()
:precondition
(and
(RIGHTOF4-ROBOT)
)
:effect
(and
(Flag136-)
(Flag136prime-)
)

)
(:action Flag136Action-5
:parameters ()
:precondition
(and
(RIGHTOF8-ROBOT)
)
:effect
(and
(Flag136-)
(Flag136prime-)
)

)
(:action Flag136Action-6
:parameters ()
:precondition
(and
(RIGHTOF7-ROBOT)
)
:effect
(and
(Flag136-)
(Flag136prime-)
)

)
(:action Flag136Action-7
:parameters ()
:precondition
(and
(RIGHTOF9-ROBOT)
)
:effect
(and
(Flag136-)
(Flag136prime-)
)

)
(:action Flag136Action-8
:parameters ()
:precondition
(and
(RIGHTOF6-ROBOT)
)
:effect
(and
(Flag136-)
(Flag136prime-)
)

)
(:action Prim136Action
:parameters ()
:precondition
(and
(not (RIGHTOF3-ROBOT))
(not (RIGHTOF5-ROBOT))
(not (RIGHTOF11-ROBOT))
(not (RIGHTOF10-ROBOT))
(not (RIGHTOF4-ROBOT))
(not (RIGHTOF8-ROBOT))
(not (RIGHTOF7-ROBOT))
(not (RIGHTOF9-ROBOT))
(not (RIGHTOF6-ROBOT))
)
:effect
(and
(Flag136prime-)
(not (Flag136-))
)

)
(:action Flag137Action-0
:parameters ()
:precondition
(and
(RIGHTOF5-ROBOT)
)
:effect
(and
(Flag137-)
(Flag137prime-)
)

)
(:action Flag137Action-1
:parameters ()
:precondition
(and
(RIGHTOF11-ROBOT)
)
:effect
(and
(Flag137-)
(Flag137prime-)
)

)
(:action Flag137Action-2
:parameters ()
:precondition
(and
(RIGHTOF10-ROBOT)
)
:effect
(and
(Flag137-)
(Flag137prime-)
)

)
(:action Flag137Action-3
:parameters ()
:precondition
(and
(RIGHTOF4-ROBOT)
)
:effect
(and
(Flag137-)
(Flag137prime-)
)

)
(:action Flag137Action-4
:parameters ()
:precondition
(and
(RIGHTOF8-ROBOT)
)
:effect
(and
(Flag137-)
(Flag137prime-)
)

)
(:action Flag137Action-5
:parameters ()
:precondition
(and
(RIGHTOF7-ROBOT)
)
:effect
(and
(Flag137-)
(Flag137prime-)
)

)
(:action Flag137Action-6
:parameters ()
:precondition
(and
(RIGHTOF9-ROBOT)
)
:effect
(and
(Flag137-)
(Flag137prime-)
)

)
(:action Flag137Action-7
:parameters ()
:precondition
(and
(RIGHTOF6-ROBOT)
)
:effect
(and
(Flag137-)
(Flag137prime-)
)

)
(:action Prim137Action
:parameters ()
:precondition
(and
(not (RIGHTOF5-ROBOT))
(not (RIGHTOF11-ROBOT))
(not (RIGHTOF10-ROBOT))
(not (RIGHTOF4-ROBOT))
(not (RIGHTOF8-ROBOT))
(not (RIGHTOF7-ROBOT))
(not (RIGHTOF9-ROBOT))
(not (RIGHTOF6-ROBOT))
)
:effect
(and
(Flag137prime-)
(not (Flag137-))
)

)
(:action Flag138Action-0
:parameters ()
:precondition
(and
(RIGHTOF5-ROBOT)
)
:effect
(and
(Flag138-)
(Flag138prime-)
)

)
(:action Flag138Action-1
:parameters ()
:precondition
(and
(RIGHTOF11-ROBOT)
)
:effect
(and
(Flag138-)
(Flag138prime-)
)

)
(:action Flag138Action-2
:parameters ()
:precondition
(and
(RIGHTOF10-ROBOT)
)
:effect
(and
(Flag138-)
(Flag138prime-)
)

)
(:action Flag138Action-3
:parameters ()
:precondition
(and
(RIGHTOF8-ROBOT)
)
:effect
(and
(Flag138-)
(Flag138prime-)
)

)
(:action Flag138Action-4
:parameters ()
:precondition
(and
(RIGHTOF7-ROBOT)
)
:effect
(and
(Flag138-)
(Flag138prime-)
)

)
(:action Flag138Action-5
:parameters ()
:precondition
(and
(RIGHTOF9-ROBOT)
)
:effect
(and
(Flag138-)
(Flag138prime-)
)

)
(:action Flag138Action-6
:parameters ()
:precondition
(and
(RIGHTOF6-ROBOT)
)
:effect
(and
(Flag138-)
(Flag138prime-)
)

)
(:action Prim138Action
:parameters ()
:precondition
(and
(not (RIGHTOF5-ROBOT))
(not (RIGHTOF11-ROBOT))
(not (RIGHTOF10-ROBOT))
(not (RIGHTOF8-ROBOT))
(not (RIGHTOF7-ROBOT))
(not (RIGHTOF9-ROBOT))
(not (RIGHTOF6-ROBOT))
)
:effect
(and
(Flag138prime-)
(not (Flag138-))
)

)
(:action Flag139Action-0
:parameters ()
:precondition
(and
(RIGHTOF11-ROBOT)
)
:effect
(and
(Flag139-)
(Flag139prime-)
)

)
(:action Flag139Action-1
:parameters ()
:precondition
(and
(RIGHTOF10-ROBOT)
)
:effect
(and
(Flag139-)
(Flag139prime-)
)

)
(:action Flag139Action-2
:parameters ()
:precondition
(and
(RIGHTOF8-ROBOT)
)
:effect
(and
(Flag139-)
(Flag139prime-)
)

)
(:action Flag139Action-3
:parameters ()
:precondition
(and
(RIGHTOF7-ROBOT)
)
:effect
(and
(Flag139-)
(Flag139prime-)
)

)
(:action Flag139Action-4
:parameters ()
:precondition
(and
(RIGHTOF9-ROBOT)
)
:effect
(and
(Flag139-)
(Flag139prime-)
)

)
(:action Flag139Action-5
:parameters ()
:precondition
(and
(RIGHTOF6-ROBOT)
)
:effect
(and
(Flag139-)
(Flag139prime-)
)

)
(:action Prim139Action
:parameters ()
:precondition
(and
(not (RIGHTOF11-ROBOT))
(not (RIGHTOF10-ROBOT))
(not (RIGHTOF8-ROBOT))
(not (RIGHTOF7-ROBOT))
(not (RIGHTOF9-ROBOT))
(not (RIGHTOF6-ROBOT))
)
:effect
(and
(Flag139prime-)
(not (Flag139-))
)

)
(:action Flag140Action-0
:parameters ()
:precondition
(and
(RIGHTOF10-ROBOT)
)
:effect
(and
(Flag140-)
(Flag140prime-)
)

)
(:action Flag140Action-1
:parameters ()
:precondition
(and
(RIGHTOF7-ROBOT)
)
:effect
(and
(Flag140-)
(Flag140prime-)
)

)
(:action Flag140Action-2
:parameters ()
:precondition
(and
(RIGHTOF9-ROBOT)
)
:effect
(and
(Flag140-)
(Flag140prime-)
)

)
(:action Flag140Action-3
:parameters ()
:precondition
(and
(RIGHTOF8-ROBOT)
)
:effect
(and
(Flag140-)
(Flag140prime-)
)

)
(:action Flag140Action-4
:parameters ()
:precondition
(and
(RIGHTOF11-ROBOT)
)
:effect
(and
(Flag140-)
(Flag140prime-)
)

)
(:action Prim140Action
:parameters ()
:precondition
(and
(not (RIGHTOF10-ROBOT))
(not (RIGHTOF7-ROBOT))
(not (RIGHTOF9-ROBOT))
(not (RIGHTOF8-ROBOT))
(not (RIGHTOF11-ROBOT))
)
:effect
(and
(Flag140prime-)
(not (Flag140-))
)

)
(:action Flag141Action-0
:parameters ()
:precondition
(and
(RIGHTOF10-ROBOT)
)
:effect
(and
(Flag141-)
(Flag141prime-)
)

)
(:action Flag141Action-1
:parameters ()
:precondition
(and
(RIGHTOF9-ROBOT)
)
:effect
(and
(Flag141-)
(Flag141prime-)
)

)
(:action Flag141Action-2
:parameters ()
:precondition
(and
(RIGHTOF8-ROBOT)
)
:effect
(and
(Flag141-)
(Flag141prime-)
)

)
(:action Flag141Action-3
:parameters ()
:precondition
(and
(RIGHTOF11-ROBOT)
)
:effect
(and
(Flag141-)
(Flag141prime-)
)

)
(:action Prim141Action
:parameters ()
:precondition
(and
(not (RIGHTOF10-ROBOT))
(not (RIGHTOF9-ROBOT))
(not (RIGHTOF8-ROBOT))
(not (RIGHTOF11-ROBOT))
)
:effect
(and
(Flag141prime-)
(not (Flag141-))
)

)
(:action Flag142Action-0
:parameters ()
:precondition
(and
(RIGHTOF10-ROBOT)
)
:effect
(and
(Flag142-)
(Flag142prime-)
)

)
(:action Flag142Action-1
:parameters ()
:precondition
(and
(RIGHTOF9-ROBOT)
)
:effect
(and
(Flag142-)
(Flag142prime-)
)

)
(:action Flag142Action-2
:parameters ()
:precondition
(and
(RIGHTOF11-ROBOT)
)
:effect
(and
(Flag142-)
(Flag142prime-)
)

)
(:action Prim142Action
:parameters ()
:precondition
(and
(not (RIGHTOF10-ROBOT))
(not (RIGHTOF9-ROBOT))
(not (RIGHTOF11-ROBOT))
)
:effect
(and
(Flag142prime-)
(not (Flag142-))
)

)
(:action Flag143Action-0
:parameters ()
:precondition
(and
(RIGHTOF10-ROBOT)
)
:effect
(and
(Flag143-)
(Flag143prime-)
)

)
(:action Flag143Action-1
:parameters ()
:precondition
(and
(RIGHTOF11-ROBOT)
)
:effect
(and
(Flag143-)
(Flag143prime-)
)

)
(:action Prim143Action
:parameters ()
:precondition
(and
(not (RIGHTOF10-ROBOT))
(not (RIGHTOF11-ROBOT))
)
:effect
(and
(Flag143prime-)
(not (Flag143-))
)

)
(:action Flag144Action
:parameters ()
:precondition
(and
(LEFTOF2-ROBOT)
(RIGHTOF9-ROBOT)
)
:effect
(and
(Flag144-)
(Flag144prime-)
)

)
(:action Flag145Action
:parameters ()
:precondition
(and
(LEFTOF1-ROBOT)
(RIGHTOF9-ROBOT)
)
:effect
(and
(Flag145-)
(Flag145prime-)
)

)
(:action Flag146Action
:parameters ()
:precondition
(and
(RIGHTOF3-ROBOT)
(LEFTOF1-ROBOT)
)
:effect
(and
(Flag146-)
(Flag146prime-)
)

)
(:action Flag147Action
:parameters ()
:precondition
(and
(RIGHTOF10-ROBOT)
(LEFTOF1-ROBOT)
)
:effect
(and
(Flag147-)
(Flag147prime-)
)

)
(:action Flag148Action
:parameters ()
:precondition
(and
(LEFTOF2-ROBOT)
(RIGHTOF1-ROBOT)
)
:effect
(and
(Flag148-)
(Flag148prime-)
)

)
(:action Flag149Action
:parameters ()
:precondition
(and
(RIGHTOF5-ROBOT)
(LEFTOF2-ROBOT)
)
:effect
(and
(Flag149-)
(Flag149prime-)
)

)
(:action Flag150Action
:parameters ()
:precondition
(and
(LEFTOF1-ROBOT)
(RIGHTOF1-ROBOT)
)
:effect
(and
(Flag150-)
(Flag150prime-)
)

)
(:action Flag151Action
:parameters ()
:precondition
(and
(LEFTOF2-ROBOT)
(RIGHTOF7-ROBOT)
)
:effect
(and
(Flag151-)
(Flag151prime-)
)

)
(:action Flag152Action
:parameters ()
:precondition
(and
(RIGHTOF5-ROBOT)
(LEFTOF1-ROBOT)
)
:effect
(and
(Flag152-)
(Flag152prime-)
)

)
(:action Flag153Action
:parameters ()
:precondition
(and
(RIGHTOF11-ROBOT)
(LEFTOF1-ROBOT)
)
:effect
(and
(Flag153-)
(Flag153prime-)
)

)
(:action Flag154Action
:parameters ()
:precondition
(and
(RIGHTOF3-ROBOT)
(LEFTOF2-ROBOT)
)
:effect
(and
(Flag154-)
(Flag154prime-)
)

)
(:action Flag155Action
:parameters ()
:precondition
(and
(LEFTOF1-ROBOT)
(RIGHTOF4-ROBOT)
)
:effect
(and
(Flag155-)
(Flag155prime-)
)

)
(:action Flag156Action
:parameters ()
:precondition
(and
(RIGHTOF10-ROBOT)
(LEFTOF2-ROBOT)
)
:effect
(and
(Flag156-)
(Flag156prime-)
)

)
(:action Flag157Action
:parameters ()
:precondition
(and
(LEFTOF1-ROBOT)
(RIGHTOF7-ROBOT)
)
:effect
(and
(Flag157-)
(Flag157prime-)
)

)
(:action Flag158Action
:parameters ()
:precondition
(and
(LEFTOF1-ROBOT)
(RIGHTOF5-ROBOT)
)
:effect
(and
(Flag158-)
(Flag158prime-)
)

)
(:action Flag159Action
:parameters ()
:precondition
(and
(RIGHTOF8-ROBOT)
(LEFTOF1-ROBOT)
)
:effect
(and
(Flag159-)
(Flag159prime-)
)

)
(:action Flag160Action
:parameters ()
:precondition
(and
(RIGHTOF11-ROBOT)
(LEFTOF2-ROBOT)
)
:effect
(and
(Flag160-)
(Flag160prime-)
)

)
(:action Flag161Action
:parameters ()
:precondition
(and
(RIGHTOF8-ROBOT)
(LEFTOF2-ROBOT)
)
:effect
(and
(Flag161-)
(Flag161prime-)
)

)
(:action Flag162Action
:parameters ()
:precondition
(and
(RIGHTOF2-ROBOT)
(LEFTOF2-ROBOT)
)
:effect
(and
(Flag162-)
(Flag162prime-)
)

)
(:action Flag163Action
:parameters ()
:precondition
(and
(LEFTOF1-ROBOT)
(RIGHTOF2-ROBOT)
)
:effect
(and
(Flag163-)
(Flag163prime-)
)

)
(:action Flag164Action
:parameters ()
:precondition
(and
(LEFTOF1-ROBOT)
(RIGHTOF6-ROBOT)
)
:effect
(and
(Flag164-)
(Flag164prime-)
)

)
(:action Flag165Action
:parameters ()
:precondition
(and
(LEFTOF2-ROBOT)
(RIGHTOF6-ROBOT)
)
:effect
(and
(Flag165-)
(Flag165prime-)
)

)
(:action Flag166Action
:parameters ()
:precondition
(and
(LEFTOF2-ROBOT)
(RIGHTOF4-ROBOT)
)
:effect
(and
(Flag166-)
(Flag166prime-)
)

)
(:action Prim144Action-0
:parameters ()
:precondition
(and
(not (LEFTOF2-ROBOT))
)
:effect
(and
(Flag144prime-)
(not (Flag144-))
)

)
(:action Prim144Action-1
:parameters ()
:precondition
(and
(not (RIGHTOF9-ROBOT))
)
:effect
(and
(Flag144prime-)
(not (Flag144-))
)

)
(:action Prim145Action-0
:parameters ()
:precondition
(and
(not (LEFTOF1-ROBOT))
)
:effect
(and
(Flag145prime-)
(not (Flag145-))
)

)
(:action Prim145Action-1
:parameters ()
:precondition
(and
(not (RIGHTOF9-ROBOT))
)
:effect
(and
(Flag145prime-)
(not (Flag145-))
)

)
(:action Prim146Action-0
:parameters ()
:precondition
(and
(not (RIGHTOF3-ROBOT))
)
:effect
(and
(Flag146prime-)
(not (Flag146-))
)

)
(:action Prim146Action-1
:parameters ()
:precondition
(and
(not (LEFTOF1-ROBOT))
)
:effect
(and
(Flag146prime-)
(not (Flag146-))
)

)
(:action Prim147Action-0
:parameters ()
:precondition
(and
(not (RIGHTOF10-ROBOT))
)
:effect
(and
(Flag147prime-)
(not (Flag147-))
)

)
(:action Prim147Action-1
:parameters ()
:precondition
(and
(not (LEFTOF1-ROBOT))
)
:effect
(and
(Flag147prime-)
(not (Flag147-))
)

)
(:action Prim148Action-0
:parameters ()
:precondition
(and
(not (LEFTOF2-ROBOT))
)
:effect
(and
(Flag148prime-)
(not (Flag148-))
)

)
(:action Prim148Action-1
:parameters ()
:precondition
(and
(not (RIGHTOF1-ROBOT))
)
:effect
(and
(Flag148prime-)
(not (Flag148-))
)

)
(:action Prim149Action-0
:parameters ()
:precondition
(and
(not (RIGHTOF5-ROBOT))
)
:effect
(and
(Flag149prime-)
(not (Flag149-))
)

)
(:action Prim149Action-1
:parameters ()
:precondition
(and
(not (LEFTOF2-ROBOT))
)
:effect
(and
(Flag149prime-)
(not (Flag149-))
)

)
(:action Prim150Action-0
:parameters ()
:precondition
(and
(not (LEFTOF1-ROBOT))
)
:effect
(and
(Flag150prime-)
(not (Flag150-))
)

)
(:action Prim150Action-1
:parameters ()
:precondition
(and
(not (RIGHTOF1-ROBOT))
)
:effect
(and
(Flag150prime-)
(not (Flag150-))
)

)
(:action Prim151Action-0
:parameters ()
:precondition
(and
(not (LEFTOF2-ROBOT))
)
:effect
(and
(Flag151prime-)
(not (Flag151-))
)

)
(:action Prim151Action-1
:parameters ()
:precondition
(and
(not (RIGHTOF7-ROBOT))
)
:effect
(and
(Flag151prime-)
(not (Flag151-))
)

)
(:action Prim152Action-0
:parameters ()
:precondition
(and
(not (RIGHTOF5-ROBOT))
)
:effect
(and
(Flag152prime-)
(not (Flag152-))
)

)
(:action Prim152Action-1
:parameters ()
:precondition
(and
(not (LEFTOF1-ROBOT))
)
:effect
(and
(Flag152prime-)
(not (Flag152-))
)

)
(:action Prim153Action-0
:parameters ()
:precondition
(and
(not (RIGHTOF11-ROBOT))
)
:effect
(and
(Flag153prime-)
(not (Flag153-))
)

)
(:action Prim153Action-1
:parameters ()
:precondition
(and
(not (LEFTOF1-ROBOT))
)
:effect
(and
(Flag153prime-)
(not (Flag153-))
)

)
(:action Prim154Action-0
:parameters ()
:precondition
(and
(not (RIGHTOF3-ROBOT))
)
:effect
(and
(Flag154prime-)
(not (Flag154-))
)

)
(:action Prim154Action-1
:parameters ()
:precondition
(and
(not (LEFTOF2-ROBOT))
)
:effect
(and
(Flag154prime-)
(not (Flag154-))
)

)
(:action Prim155Action-0
:parameters ()
:precondition
(and
(not (LEFTOF1-ROBOT))
)
:effect
(and
(Flag155prime-)
(not (Flag155-))
)

)
(:action Prim155Action-1
:parameters ()
:precondition
(and
(not (RIGHTOF4-ROBOT))
)
:effect
(and
(Flag155prime-)
(not (Flag155-))
)

)
(:action Prim156Action-0
:parameters ()
:precondition
(and
(not (RIGHTOF10-ROBOT))
)
:effect
(and
(Flag156prime-)
(not (Flag156-))
)

)
(:action Prim156Action-1
:parameters ()
:precondition
(and
(not (LEFTOF2-ROBOT))
)
:effect
(and
(Flag156prime-)
(not (Flag156-))
)

)
(:action Prim157Action-0
:parameters ()
:precondition
(and
(not (LEFTOF1-ROBOT))
)
:effect
(and
(Flag157prime-)
(not (Flag157-))
)

)
(:action Prim157Action-1
:parameters ()
:precondition
(and
(not (RIGHTOF7-ROBOT))
)
:effect
(and
(Flag157prime-)
(not (Flag157-))
)

)
(:action Prim158Action-0
:parameters ()
:precondition
(and
(not (LEFTOF1-ROBOT))
)
:effect
(and
(Flag158prime-)
(not (Flag158-))
)

)
(:action Prim158Action-1
:parameters ()
:precondition
(and
(not (RIGHTOF5-ROBOT))
)
:effect
(and
(Flag158prime-)
(not (Flag158-))
)

)
(:action Prim159Action-0
:parameters ()
:precondition
(and
(not (RIGHTOF8-ROBOT))
)
:effect
(and
(Flag159prime-)
(not (Flag159-))
)

)
(:action Prim159Action-1
:parameters ()
:precondition
(and
(not (LEFTOF1-ROBOT))
)
:effect
(and
(Flag159prime-)
(not (Flag159-))
)

)
(:action Prim160Action-0
:parameters ()
:precondition
(and
(not (RIGHTOF11-ROBOT))
)
:effect
(and
(Flag160prime-)
(not (Flag160-))
)

)
(:action Prim160Action-1
:parameters ()
:precondition
(and
(not (LEFTOF2-ROBOT))
)
:effect
(and
(Flag160prime-)
(not (Flag160-))
)

)
(:action Prim161Action-0
:parameters ()
:precondition
(and
(not (RIGHTOF8-ROBOT))
)
:effect
(and
(Flag161prime-)
(not (Flag161-))
)

)
(:action Prim161Action-1
:parameters ()
:precondition
(and
(not (LEFTOF2-ROBOT))
)
:effect
(and
(Flag161prime-)
(not (Flag161-))
)

)
(:action Prim162Action-1
:parameters ()
:precondition
(and
(not (LEFTOF2-ROBOT))
)
:effect
(and
(Flag162prime-)
(not (Flag162-))
)

)
(:action Prim163Action-0
:parameters ()
:precondition
(and
(not (LEFTOF1-ROBOT))
)
:effect
(and
(Flag163prime-)
(not (Flag163-))
)

)
(:action Prim164Action-0
:parameters ()
:precondition
(and
(not (LEFTOF1-ROBOT))
)
:effect
(and
(Flag164prime-)
(not (Flag164-))
)

)
(:action Prim164Action-1
:parameters ()
:precondition
(and
(not (RIGHTOF6-ROBOT))
)
:effect
(and
(Flag164prime-)
(not (Flag164-))
)

)
(:action Prim165Action-0
:parameters ()
:precondition
(and
(not (LEFTOF2-ROBOT))
)
:effect
(and
(Flag165prime-)
(not (Flag165-))
)

)
(:action Prim165Action-1
:parameters ()
:precondition
(and
(not (RIGHTOF6-ROBOT))
)
:effect
(and
(Flag165prime-)
(not (Flag165-))
)

)
(:action Prim166Action-0
:parameters ()
:precondition
(and
(not (LEFTOF2-ROBOT))
)
:effect
(and
(Flag166prime-)
(not (Flag166-))
)

)
(:action Prim166Action-1
:parameters ()
:precondition
(and
(not (RIGHTOF4-ROBOT))
)
:effect
(and
(Flag166prime-)
(not (Flag166-))
)

)
(:action Flag168Action
:parameters ()
:precondition
(and
(LEFTOF3-ROBOT)
(RIGHTOF6-ROBOT)
)
:effect
(and
(Flag168-)
(Flag168prime-)
)

)
(:action Flag169Action
:parameters ()
:precondition
(and
(LEFTOF3-ROBOT)
(RIGHTOF4-ROBOT)
)
:effect
(and
(Flag169-)
(Flag169prime-)
)

)
(:action Flag170Action
:parameters ()
:precondition
(and
(LEFTOF3-ROBOT)
(RIGHTOF7-ROBOT)
)
:effect
(and
(Flag170-)
(Flag170prime-)
)

)
(:action Flag171Action
:parameters ()
:precondition
(and
(LEFTOF3-ROBOT)
(RIGHTOF10-ROBOT)
)
:effect
(and
(Flag171-)
(Flag171prime-)
)

)
(:action Flag172Action
:parameters ()
:precondition
(and
(LEFTOF3-ROBOT)
(RIGHTOF8-ROBOT)
)
:effect
(and
(Flag172-)
(Flag172prime-)
)

)
(:action Flag173Action
:parameters ()
:precondition
(and
(LEFTOF3-ROBOT)
(RIGHTOF5-ROBOT)
)
:effect
(and
(Flag173-)
(Flag173prime-)
)

)
(:action Flag174Action
:parameters ()
:precondition
(and
(RIGHTOF3-ROBOT)
(LEFTOF3-ROBOT)
)
:effect
(and
(Flag174-)
(Flag174prime-)
)

)
(:action Flag175Action
:parameters ()
:precondition
(and
(LEFTOF3-ROBOT)
(RIGHTOF11-ROBOT)
)
:effect
(and
(Flag175-)
(Flag175prime-)
)

)
(:action Flag176Action
:parameters ()
:precondition
(and
(LEFTOF3-ROBOT)
(RIGHTOF9-ROBOT)
)
:effect
(and
(Flag176-)
(Flag176prime-)
)

)
(:action Flag177Action
:parameters ()
:precondition
(and
(LEFTOF3-ROBOT)
(RIGHTOF2-ROBOT)
)
:effect
(and
(Flag177-)
(Flag177prime-)
)

)
(:action Prim168Action-0
:parameters ()
:precondition
(and
(not (LEFTOF3-ROBOT))
)
:effect
(and
(Flag168prime-)
(not (Flag168-))
)

)
(:action Prim168Action-1
:parameters ()
:precondition
(and
(not (RIGHTOF6-ROBOT))
)
:effect
(and
(Flag168prime-)
(not (Flag168-))
)

)
(:action Prim169Action-0
:parameters ()
:precondition
(and
(not (LEFTOF3-ROBOT))
)
:effect
(and
(Flag169prime-)
(not (Flag169-))
)

)
(:action Prim169Action-1
:parameters ()
:precondition
(and
(not (RIGHTOF4-ROBOT))
)
:effect
(and
(Flag169prime-)
(not (Flag169-))
)

)
(:action Prim170Action-0
:parameters ()
:precondition
(and
(not (LEFTOF3-ROBOT))
)
:effect
(and
(Flag170prime-)
(not (Flag170-))
)

)
(:action Prim170Action-1
:parameters ()
:precondition
(and
(not (RIGHTOF7-ROBOT))
)
:effect
(and
(Flag170prime-)
(not (Flag170-))
)

)
(:action Prim171Action-0
:parameters ()
:precondition
(and
(not (LEFTOF3-ROBOT))
)
:effect
(and
(Flag171prime-)
(not (Flag171-))
)

)
(:action Prim171Action-1
:parameters ()
:precondition
(and
(not (RIGHTOF10-ROBOT))
)
:effect
(and
(Flag171prime-)
(not (Flag171-))
)

)
(:action Prim172Action-0
:parameters ()
:precondition
(and
(not (LEFTOF3-ROBOT))
)
:effect
(and
(Flag172prime-)
(not (Flag172-))
)

)
(:action Prim172Action-1
:parameters ()
:precondition
(and
(not (RIGHTOF8-ROBOT))
)
:effect
(and
(Flag172prime-)
(not (Flag172-))
)

)
(:action Prim173Action-0
:parameters ()
:precondition
(and
(not (LEFTOF3-ROBOT))
)
:effect
(and
(Flag173prime-)
(not (Flag173-))
)

)
(:action Prim173Action-1
:parameters ()
:precondition
(and
(not (RIGHTOF5-ROBOT))
)
:effect
(and
(Flag173prime-)
(not (Flag173-))
)

)
(:action Prim174Action-0
:parameters ()
:precondition
(and
(not (RIGHTOF3-ROBOT))
)
:effect
(and
(Flag174prime-)
(not (Flag174-))
)

)
(:action Prim174Action-1
:parameters ()
:precondition
(and
(not (LEFTOF3-ROBOT))
)
:effect
(and
(Flag174prime-)
(not (Flag174-))
)

)
(:action Prim175Action-0
:parameters ()
:precondition
(and
(not (LEFTOF3-ROBOT))
)
:effect
(and
(Flag175prime-)
(not (Flag175-))
)

)
(:action Prim175Action-1
:parameters ()
:precondition
(and
(not (RIGHTOF11-ROBOT))
)
:effect
(and
(Flag175prime-)
(not (Flag175-))
)

)
(:action Prim176Action-0
:parameters ()
:precondition
(and
(not (LEFTOF3-ROBOT))
)
:effect
(and
(Flag176prime-)
(not (Flag176-))
)

)
(:action Prim176Action-1
:parameters ()
:precondition
(and
(not (RIGHTOF9-ROBOT))
)
:effect
(and
(Flag176prime-)
(not (Flag176-))
)

)
(:action Prim177Action-0
:parameters ()
:precondition
(and
(not (LEFTOF3-ROBOT))
)
:effect
(and
(Flag177prime-)
(not (Flag177-))
)

)
(:action Flag179Action
:parameters ()
:precondition
(and
(LEFTOF4-ROBOT)
(RIGHTOF4-ROBOT)
)
:effect
(and
(Flag179-)
(Flag179prime-)
)

)
(:action Flag180Action
:parameters ()
:precondition
(and
(LEFTOF4-ROBOT)
(RIGHTOF7-ROBOT)
)
:effect
(and
(Flag180-)
(Flag180prime-)
)

)
(:action Flag181Action
:parameters ()
:precondition
(and
(LEFTOF3-ROBOT)
(RIGHTOF3-ROBOT)
)
:effect
(and
(Flag181-)
(Flag181prime-)
)

)
(:action Flag182Action
:parameters ()
:precondition
(and
(RIGHTOF8-ROBOT)
(LEFTOF4-ROBOT)
)
:effect
(and
(Flag182-)
(Flag182prime-)
)

)
(:action Flag183Action
:parameters ()
:precondition
(and
(RIGHTOF10-ROBOT)
(LEFTOF4-ROBOT)
)
:effect
(and
(Flag183-)
(Flag183prime-)
)

)
(:action Flag184Action
:parameters ()
:precondition
(and
(LEFTOF4-ROBOT)
(RIGHTOF9-ROBOT)
)
:effect
(and
(Flag184-)
(Flag184prime-)
)

)
(:action Flag185Action
:parameters ()
:precondition
(and
(LEFTOF4-ROBOT)
(RIGHTOF6-ROBOT)
)
:effect
(and
(Flag185-)
(Flag185prime-)
)

)
(:action Flag186Action
:parameters ()
:precondition
(and
(LEFTOF4-ROBOT)
(RIGHTOF5-ROBOT)
)
:effect
(and
(Flag186-)
(Flag186prime-)
)

)
(:action Flag187Action
:parameters ()
:precondition
(and
(RIGHTOF3-ROBOT)
(LEFTOF4-ROBOT)
)
:effect
(and
(Flag187-)
(Flag187prime-)
)

)
(:action Flag188Action
:parameters ()
:precondition
(and
(LEFTOF4-ROBOT)
(RIGHTOF11-ROBOT)
)
:effect
(and
(Flag188-)
(Flag188prime-)
)

)
(:action Prim179Action-0
:parameters ()
:precondition
(and
(not (LEFTOF4-ROBOT))
)
:effect
(and
(Flag179prime-)
(not (Flag179-))
)

)
(:action Prim179Action-1
:parameters ()
:precondition
(and
(not (RIGHTOF4-ROBOT))
)
:effect
(and
(Flag179prime-)
(not (Flag179-))
)

)
(:action Prim180Action-0
:parameters ()
:precondition
(and
(not (LEFTOF4-ROBOT))
)
:effect
(and
(Flag180prime-)
(not (Flag180-))
)

)
(:action Prim180Action-1
:parameters ()
:precondition
(and
(not (RIGHTOF7-ROBOT))
)
:effect
(and
(Flag180prime-)
(not (Flag180-))
)

)
(:action Prim181Action-0
:parameters ()
:precondition
(and
(not (LEFTOF3-ROBOT))
)
:effect
(and
(Flag181prime-)
(not (Flag181-))
)

)
(:action Prim181Action-1
:parameters ()
:precondition
(and
(not (RIGHTOF3-ROBOT))
)
:effect
(and
(Flag181prime-)
(not (Flag181-))
)

)
(:action Prim182Action-0
:parameters ()
:precondition
(and
(not (RIGHTOF8-ROBOT))
)
:effect
(and
(Flag182prime-)
(not (Flag182-))
)

)
(:action Prim182Action-1
:parameters ()
:precondition
(and
(not (LEFTOF4-ROBOT))
)
:effect
(and
(Flag182prime-)
(not (Flag182-))
)

)
(:action Prim183Action-0
:parameters ()
:precondition
(and
(not (RIGHTOF10-ROBOT))
)
:effect
(and
(Flag183prime-)
(not (Flag183-))
)

)
(:action Prim183Action-1
:parameters ()
:precondition
(and
(not (LEFTOF4-ROBOT))
)
:effect
(and
(Flag183prime-)
(not (Flag183-))
)

)
(:action Prim184Action-0
:parameters ()
:precondition
(and
(not (LEFTOF4-ROBOT))
)
:effect
(and
(Flag184prime-)
(not (Flag184-))
)

)
(:action Prim184Action-1
:parameters ()
:precondition
(and
(not (RIGHTOF9-ROBOT))
)
:effect
(and
(Flag184prime-)
(not (Flag184-))
)

)
(:action Prim185Action-0
:parameters ()
:precondition
(and
(not (LEFTOF4-ROBOT))
)
:effect
(and
(Flag185prime-)
(not (Flag185-))
)

)
(:action Prim185Action-1
:parameters ()
:precondition
(and
(not (RIGHTOF6-ROBOT))
)
:effect
(and
(Flag185prime-)
(not (Flag185-))
)

)
(:action Prim186Action-0
:parameters ()
:precondition
(and
(not (LEFTOF4-ROBOT))
)
:effect
(and
(Flag186prime-)
(not (Flag186-))
)

)
(:action Prim186Action-1
:parameters ()
:precondition
(and
(not (RIGHTOF5-ROBOT))
)
:effect
(and
(Flag186prime-)
(not (Flag186-))
)

)
(:action Prim187Action-0
:parameters ()
:precondition
(and
(not (RIGHTOF3-ROBOT))
)
:effect
(and
(Flag187prime-)
(not (Flag187-))
)

)
(:action Prim187Action-1
:parameters ()
:precondition
(and
(not (LEFTOF4-ROBOT))
)
:effect
(and
(Flag187prime-)
(not (Flag187-))
)

)
(:action Prim188Action-0
:parameters ()
:precondition
(and
(not (LEFTOF4-ROBOT))
)
:effect
(and
(Flag188prime-)
(not (Flag188-))
)

)
(:action Prim188Action-1
:parameters ()
:precondition
(and
(not (RIGHTOF11-ROBOT))
)
:effect
(and
(Flag188prime-)
(not (Flag188-))
)

)
(:action Flag190Action
:parameters ()
:precondition
(and
(RIGHTOF9-ROBOT)
(LEFTOF5-ROBOT)
)
:effect
(and
(Flag190-)
(Flag190prime-)
)

)
(:action Flag191Action
:parameters ()
:precondition
(and
(RIGHTOF7-ROBOT)
(LEFTOF5-ROBOT)
)
:effect
(and
(Flag191-)
(Flag191prime-)
)

)
(:action Flag192Action
:parameters ()
:precondition
(and
(RIGHTOF11-ROBOT)
(LEFTOF5-ROBOT)
)
:effect
(and
(Flag192-)
(Flag192prime-)
)

)
(:action Flag193Action
:parameters ()
:precondition
(and
(LEFTOF4-ROBOT)
(RIGHTOF10-ROBOT)
)
:effect
(and
(Flag193-)
(Flag193prime-)
)

)
(:action Flag194Action
:parameters ()
:precondition
(and
(RIGHTOF4-ROBOT)
(LEFTOF5-ROBOT)
)
:effect
(and
(Flag194-)
(Flag194prime-)
)

)
(:action Flag195Action
:parameters ()
:precondition
(and
(LEFTOF4-ROBOT)
(RIGHTOF8-ROBOT)
)
:effect
(and
(Flag195-)
(Flag195prime-)
)

)
(:action Flag196Action
:parameters ()
:precondition
(and
(LEFTOF5-ROBOT)
(RIGHTOF6-ROBOT)
)
:effect
(and
(Flag196-)
(Flag196prime-)
)

)
(:action Flag197Action
:parameters ()
:precondition
(and
(RIGHTOF10-ROBOT)
(LEFTOF5-ROBOT)
)
:effect
(and
(Flag197-)
(Flag197prime-)
)

)
(:action Flag198Action
:parameters ()
:precondition
(and
(RIGHTOF8-ROBOT)
(LEFTOF5-ROBOT)
)
:effect
(and
(Flag198-)
(Flag198prime-)
)

)
(:action Flag199Action
:parameters ()
:precondition
(and
(RIGHTOF5-ROBOT)
(LEFTOF5-ROBOT)
)
:effect
(and
(Flag199-)
(Flag199prime-)
)

)
(:action Prim190Action-0
:parameters ()
:precondition
(and
(not (RIGHTOF9-ROBOT))
)
:effect
(and
(Flag190prime-)
(not (Flag190-))
)

)
(:action Prim190Action-1
:parameters ()
:precondition
(and
(not (LEFTOF5-ROBOT))
)
:effect
(and
(Flag190prime-)
(not (Flag190-))
)

)
(:action Prim191Action-0
:parameters ()
:precondition
(and
(not (RIGHTOF7-ROBOT))
)
:effect
(and
(Flag191prime-)
(not (Flag191-))
)

)
(:action Prim191Action-1
:parameters ()
:precondition
(and
(not (LEFTOF5-ROBOT))
)
:effect
(and
(Flag191prime-)
(not (Flag191-))
)

)
(:action Prim192Action-0
:parameters ()
:precondition
(and
(not (RIGHTOF11-ROBOT))
)
:effect
(and
(Flag192prime-)
(not (Flag192-))
)

)
(:action Prim192Action-1
:parameters ()
:precondition
(and
(not (LEFTOF5-ROBOT))
)
:effect
(and
(Flag192prime-)
(not (Flag192-))
)

)
(:action Prim193Action-0
:parameters ()
:precondition
(and
(not (LEFTOF4-ROBOT))
)
:effect
(and
(Flag193prime-)
(not (Flag193-))
)

)
(:action Prim193Action-1
:parameters ()
:precondition
(and
(not (RIGHTOF10-ROBOT))
)
:effect
(and
(Flag193prime-)
(not (Flag193-))
)

)
(:action Prim194Action-0
:parameters ()
:precondition
(and
(not (RIGHTOF4-ROBOT))
)
:effect
(and
(Flag194prime-)
(not (Flag194-))
)

)
(:action Prim194Action-1
:parameters ()
:precondition
(and
(not (LEFTOF5-ROBOT))
)
:effect
(and
(Flag194prime-)
(not (Flag194-))
)

)
(:action Prim195Action-0
:parameters ()
:precondition
(and
(not (LEFTOF4-ROBOT))
)
:effect
(and
(Flag195prime-)
(not (Flag195-))
)

)
(:action Prim195Action-1
:parameters ()
:precondition
(and
(not (RIGHTOF8-ROBOT))
)
:effect
(and
(Flag195prime-)
(not (Flag195-))
)

)
(:action Prim196Action-0
:parameters ()
:precondition
(and
(not (LEFTOF5-ROBOT))
)
:effect
(and
(Flag196prime-)
(not (Flag196-))
)

)
(:action Prim196Action-1
:parameters ()
:precondition
(and
(not (RIGHTOF6-ROBOT))
)
:effect
(and
(Flag196prime-)
(not (Flag196-))
)

)
(:action Prim197Action-0
:parameters ()
:precondition
(and
(not (RIGHTOF10-ROBOT))
)
:effect
(and
(Flag197prime-)
(not (Flag197-))
)

)
(:action Prim197Action-1
:parameters ()
:precondition
(and
(not (LEFTOF5-ROBOT))
)
:effect
(and
(Flag197prime-)
(not (Flag197-))
)

)
(:action Prim198Action-0
:parameters ()
:precondition
(and
(not (RIGHTOF8-ROBOT))
)
:effect
(and
(Flag198prime-)
(not (Flag198-))
)

)
(:action Prim198Action-1
:parameters ()
:precondition
(and
(not (LEFTOF5-ROBOT))
)
:effect
(and
(Flag198prime-)
(not (Flag198-))
)

)
(:action Prim199Action-0
:parameters ()
:precondition
(and
(not (RIGHTOF5-ROBOT))
)
:effect
(and
(Flag199prime-)
(not (Flag199-))
)

)
(:action Prim199Action-1
:parameters ()
:precondition
(and
(not (LEFTOF5-ROBOT))
)
:effect
(and
(Flag199prime-)
(not (Flag199-))
)

)
(:action Flag201Action
:parameters ()
:precondition
(and
(RIGHTOF8-ROBOT)
(LEFTOF6-ROBOT)
)
:effect
(and
(Flag201-)
(Flag201prime-)
)

)
(:action Flag202Action
:parameters ()
:precondition
(and
(RIGHTOF11-ROBOT)
(LEFTOF6-ROBOT)
)
:effect
(and
(Flag202-)
(Flag202prime-)
)

)
(:action Flag203Action
:parameters ()
:precondition
(and
(RIGHTOF5-ROBOT)
(LEFTOF6-ROBOT)
)
:effect
(and
(Flag203-)
(Flag203prime-)
)

)
(:action Flag204Action
:parameters ()
:precondition
(and
(RIGHTOF7-ROBOT)
(LEFTOF6-ROBOT)
)
:effect
(and
(Flag204-)
(Flag204prime-)
)

)
(:action Flag205Action
:parameters ()
:precondition
(and
(RIGHTOF10-ROBOT)
(LEFTOF6-ROBOT)
)
:effect
(and
(Flag205-)
(Flag205prime-)
)

)
(:action Flag206Action
:parameters ()
:precondition
(and
(RIGHTOF6-ROBOT)
(LEFTOF6-ROBOT)
)
:effect
(and
(Flag206-)
(Flag206prime-)
)

)
(:action Flag207Action
:parameters ()
:precondition
(and
(RIGHTOF9-ROBOT)
(LEFTOF6-ROBOT)
)
:effect
(and
(Flag207-)
(Flag207prime-)
)

)
(:action Prim201Action-0
:parameters ()
:precondition
(and
(not (RIGHTOF8-ROBOT))
)
:effect
(and
(Flag201prime-)
(not (Flag201-))
)

)
(:action Prim201Action-1
:parameters ()
:precondition
(and
(not (LEFTOF6-ROBOT))
)
:effect
(and
(Flag201prime-)
(not (Flag201-))
)

)
(:action Prim202Action-0
:parameters ()
:precondition
(and
(not (RIGHTOF11-ROBOT))
)
:effect
(and
(Flag202prime-)
(not (Flag202-))
)

)
(:action Prim202Action-1
:parameters ()
:precondition
(and
(not (LEFTOF6-ROBOT))
)
:effect
(and
(Flag202prime-)
(not (Flag202-))
)

)
(:action Prim203Action-0
:parameters ()
:precondition
(and
(not (RIGHTOF5-ROBOT))
)
:effect
(and
(Flag203prime-)
(not (Flag203-))
)

)
(:action Prim203Action-1
:parameters ()
:precondition
(and
(not (LEFTOF6-ROBOT))
)
:effect
(and
(Flag203prime-)
(not (Flag203-))
)

)
(:action Prim204Action-0
:parameters ()
:precondition
(and
(not (RIGHTOF7-ROBOT))
)
:effect
(and
(Flag204prime-)
(not (Flag204-))
)

)
(:action Prim204Action-1
:parameters ()
:precondition
(and
(not (LEFTOF6-ROBOT))
)
:effect
(and
(Flag204prime-)
(not (Flag204-))
)

)
(:action Prim205Action-0
:parameters ()
:precondition
(and
(not (RIGHTOF10-ROBOT))
)
:effect
(and
(Flag205prime-)
(not (Flag205-))
)

)
(:action Prim205Action-1
:parameters ()
:precondition
(and
(not (LEFTOF6-ROBOT))
)
:effect
(and
(Flag205prime-)
(not (Flag205-))
)

)
(:action Prim206Action-0
:parameters ()
:precondition
(and
(not (RIGHTOF6-ROBOT))
)
:effect
(and
(Flag206prime-)
(not (Flag206-))
)

)
(:action Prim206Action-1
:parameters ()
:precondition
(and
(not (LEFTOF6-ROBOT))
)
:effect
(and
(Flag206prime-)
(not (Flag206-))
)

)
(:action Prim207Action-0
:parameters ()
:precondition
(and
(not (RIGHTOF9-ROBOT))
)
:effect
(and
(Flag207prime-)
(not (Flag207-))
)

)
(:action Prim207Action-1
:parameters ()
:precondition
(and
(not (LEFTOF6-ROBOT))
)
:effect
(and
(Flag207prime-)
(not (Flag207-))
)

)
(:action Flag209Action
:parameters ()
:precondition
(and
(RIGHTOF11-ROBOT)
(LEFTOF7-ROBOT)
)
:effect
(and
(Flag209-)
(Flag209prime-)
)

)
(:action Flag210Action
:parameters ()
:precondition
(and
(LEFTOF7-ROBOT)
(RIGHTOF7-ROBOT)
)
:effect
(and
(Flag210-)
(Flag210prime-)
)

)
(:action Flag211Action
:parameters ()
:precondition
(and
(RIGHTOF8-ROBOT)
(LEFTOF7-ROBOT)
)
:effect
(and
(Flag211-)
(Flag211prime-)
)

)
(:action Flag212Action
:parameters ()
:precondition
(and
(LEFTOF7-ROBOT)
(RIGHTOF6-ROBOT)
)
:effect
(and
(Flag212-)
(Flag212prime-)
)

)
(:action Flag213Action
:parameters ()
:precondition
(and
(LEFTOF7-ROBOT)
(RIGHTOF9-ROBOT)
)
:effect
(and
(Flag213-)
(Flag213prime-)
)

)
(:action Flag214Action
:parameters ()
:precondition
(and
(LEFTOF6-ROBOT)
(RIGHTOF6-ROBOT)
)
:effect
(and
(Flag214-)
(Flag214prime-)
)

)
(:action Flag215Action
:parameters ()
:precondition
(and
(RIGHTOF10-ROBOT)
(LEFTOF7-ROBOT)
)
:effect
(and
(Flag215-)
(Flag215prime-)
)

)
(:action Prim209Action-0
:parameters ()
:precondition
(and
(not (RIGHTOF11-ROBOT))
)
:effect
(and
(Flag209prime-)
(not (Flag209-))
)

)
(:action Prim209Action-1
:parameters ()
:precondition
(and
(not (LEFTOF7-ROBOT))
)
:effect
(and
(Flag209prime-)
(not (Flag209-))
)

)
(:action Prim210Action-0
:parameters ()
:precondition
(and
(not (LEFTOF7-ROBOT))
)
:effect
(and
(Flag210prime-)
(not (Flag210-))
)

)
(:action Prim210Action-1
:parameters ()
:precondition
(and
(not (RIGHTOF7-ROBOT))
)
:effect
(and
(Flag210prime-)
(not (Flag210-))
)

)
(:action Prim211Action-0
:parameters ()
:precondition
(and
(not (RIGHTOF8-ROBOT))
)
:effect
(and
(Flag211prime-)
(not (Flag211-))
)

)
(:action Prim211Action-1
:parameters ()
:precondition
(and
(not (LEFTOF7-ROBOT))
)
:effect
(and
(Flag211prime-)
(not (Flag211-))
)

)
(:action Prim212Action-0
:parameters ()
:precondition
(and
(not (LEFTOF7-ROBOT))
)
:effect
(and
(Flag212prime-)
(not (Flag212-))
)

)
(:action Prim212Action-1
:parameters ()
:precondition
(and
(not (RIGHTOF6-ROBOT))
)
:effect
(and
(Flag212prime-)
(not (Flag212-))
)

)
(:action Prim213Action-0
:parameters ()
:precondition
(and
(not (LEFTOF7-ROBOT))
)
:effect
(and
(Flag213prime-)
(not (Flag213-))
)

)
(:action Prim213Action-1
:parameters ()
:precondition
(and
(not (RIGHTOF9-ROBOT))
)
:effect
(and
(Flag213prime-)
(not (Flag213-))
)

)
(:action Prim214Action-0
:parameters ()
:precondition
(and
(not (LEFTOF6-ROBOT))
)
:effect
(and
(Flag214prime-)
(not (Flag214-))
)

)
(:action Prim214Action-1
:parameters ()
:precondition
(and
(not (RIGHTOF6-ROBOT))
)
:effect
(and
(Flag214prime-)
(not (Flag214-))
)

)
(:action Prim215Action-0
:parameters ()
:precondition
(and
(not (RIGHTOF10-ROBOT))
)
:effect
(and
(Flag215prime-)
(not (Flag215-))
)

)
(:action Prim215Action-1
:parameters ()
:precondition
(and
(not (LEFTOF7-ROBOT))
)
:effect
(and
(Flag215prime-)
(not (Flag215-))
)

)
(:action Flag217Action
:parameters ()
:precondition
(and
(RIGHTOF7-ROBOT)
(LEFTOF8-ROBOT)
)
:effect
(and
(Flag217-)
(Flag217prime-)
)

)
(:action Flag218Action
:parameters ()
:precondition
(and
(LEFTOF8-ROBOT)
(RIGHTOF9-ROBOT)
)
:effect
(and
(Flag218-)
(Flag218prime-)
)

)
(:action Flag219Action
:parameters ()
:precondition
(and
(RIGHTOF10-ROBOT)
(LEFTOF8-ROBOT)
)
:effect
(and
(Flag219-)
(Flag219prime-)
)

)
(:action Flag220Action
:parameters ()
:precondition
(and
(RIGHTOF8-ROBOT)
(LEFTOF8-ROBOT)
)
:effect
(and
(Flag220-)
(Flag220prime-)
)

)
(:action Flag221Action
:parameters ()
:precondition
(and
(RIGHTOF7-ROBOT)
(LEFTOF7-ROBOT)
)
:effect
(and
(Flag221-)
(Flag221prime-)
)

)
(:action Flag222Action
:parameters ()
:precondition
(and
(RIGHTOF11-ROBOT)
(LEFTOF8-ROBOT)
)
:effect
(and
(Flag222-)
(Flag222prime-)
)

)
(:action Prim217Action-0
:parameters ()
:precondition
(and
(not (RIGHTOF7-ROBOT))
)
:effect
(and
(Flag217prime-)
(not (Flag217-))
)

)
(:action Prim217Action-1
:parameters ()
:precondition
(and
(not (LEFTOF8-ROBOT))
)
:effect
(and
(Flag217prime-)
(not (Flag217-))
)

)
(:action Prim218Action-0
:parameters ()
:precondition
(and
(not (LEFTOF8-ROBOT))
)
:effect
(and
(Flag218prime-)
(not (Flag218-))
)

)
(:action Prim218Action-1
:parameters ()
:precondition
(and
(not (RIGHTOF9-ROBOT))
)
:effect
(and
(Flag218prime-)
(not (Flag218-))
)

)
(:action Prim219Action-0
:parameters ()
:precondition
(and
(not (RIGHTOF10-ROBOT))
)
:effect
(and
(Flag219prime-)
(not (Flag219-))
)

)
(:action Prim219Action-1
:parameters ()
:precondition
(and
(not (LEFTOF8-ROBOT))
)
:effect
(and
(Flag219prime-)
(not (Flag219-))
)

)
(:action Prim220Action-0
:parameters ()
:precondition
(and
(not (RIGHTOF8-ROBOT))
)
:effect
(and
(Flag220prime-)
(not (Flag220-))
)

)
(:action Prim220Action-1
:parameters ()
:precondition
(and
(not (LEFTOF8-ROBOT))
)
:effect
(and
(Flag220prime-)
(not (Flag220-))
)

)
(:action Prim221Action-0
:parameters ()
:precondition
(and
(not (RIGHTOF7-ROBOT))
)
:effect
(and
(Flag221prime-)
(not (Flag221-))
)

)
(:action Prim221Action-1
:parameters ()
:precondition
(and
(not (LEFTOF7-ROBOT))
)
:effect
(and
(Flag221prime-)
(not (Flag221-))
)

)
(:action Prim222Action-0
:parameters ()
:precondition
(and
(not (RIGHTOF11-ROBOT))
)
:effect
(and
(Flag222prime-)
(not (Flag222-))
)

)
(:action Prim222Action-1
:parameters ()
:precondition
(and
(not (LEFTOF8-ROBOT))
)
:effect
(and
(Flag222prime-)
(not (Flag222-))
)

)
(:action Flag224Action
:parameters ()
:precondition
(and
(RIGHTOF8-ROBOT)
(LEFTOF9-ROBOT)
)
:effect
(and
(Flag224-)
(Flag224prime-)
)

)
(:action Flag225Action
:parameters ()
:precondition
(and
(LEFTOF9-ROBOT)
(RIGHTOF9-ROBOT)
)
:effect
(and
(Flag225-)
(Flag225prime-)
)

)
(:action Flag226Action
:parameters ()
:precondition
(and
(RIGHTOF10-ROBOT)
(LEFTOF9-ROBOT)
)
:effect
(and
(Flag226-)
(Flag226prime-)
)

)
(:action Flag227Action
:parameters ()
:precondition
(and
(RIGHTOF11-ROBOT)
(LEFTOF9-ROBOT)
)
:effect
(and
(Flag227-)
(Flag227prime-)
)

)
(:action Prim224Action-0
:parameters ()
:precondition
(and
(not (RIGHTOF8-ROBOT))
)
:effect
(and
(Flag224prime-)
(not (Flag224-))
)

)
(:action Prim224Action-1
:parameters ()
:precondition
(and
(not (LEFTOF9-ROBOT))
)
:effect
(and
(Flag224prime-)
(not (Flag224-))
)

)
(:action Prim225Action-0
:parameters ()
:precondition
(and
(not (LEFTOF9-ROBOT))
)
:effect
(and
(Flag225prime-)
(not (Flag225-))
)

)
(:action Prim225Action-1
:parameters ()
:precondition
(and
(not (RIGHTOF9-ROBOT))
)
:effect
(and
(Flag225prime-)
(not (Flag225-))
)

)
(:action Prim226Action-0
:parameters ()
:precondition
(and
(not (RIGHTOF10-ROBOT))
)
:effect
(and
(Flag226prime-)
(not (Flag226-))
)

)
(:action Prim226Action-1
:parameters ()
:precondition
(and
(not (LEFTOF9-ROBOT))
)
:effect
(and
(Flag226prime-)
(not (Flag226-))
)

)
(:action Prim227Action-0
:parameters ()
:precondition
(and
(not (RIGHTOF11-ROBOT))
)
:effect
(and
(Flag227prime-)
(not (Flag227-))
)

)
(:action Prim227Action-1
:parameters ()
:precondition
(and
(not (LEFTOF9-ROBOT))
)
:effect
(and
(Flag227prime-)
(not (Flag227-))
)

)
(:action Flag229Action
:parameters ()
:precondition
(and
(RIGHTOF11-ROBOT)
(LEFTOF10-ROBOT)
)
:effect
(and
(Flag229-)
(Flag229prime-)
)

)
(:action Flag230Action
:parameters ()
:precondition
(and
(RIGHTOF10-ROBOT)
(LEFTOF10-ROBOT)
)
:effect
(and
(Flag230-)
(Flag230prime-)
)

)
(:action Flag231Action
:parameters ()
:precondition
(and
(LEFTOF10-ROBOT)
(RIGHTOF9-ROBOT)
)
:effect
(and
(Flag231-)
(Flag231prime-)
)

)
(:action Prim229Action-0
:parameters ()
:precondition
(and
(not (RIGHTOF11-ROBOT))
)
:effect
(and
(Flag229prime-)
(not (Flag229-))
)

)
(:action Prim229Action-1
:parameters ()
:precondition
(and
(not (LEFTOF10-ROBOT))
)
:effect
(and
(Flag229prime-)
(not (Flag229-))
)

)
(:action Prim230Action-0
:parameters ()
:precondition
(and
(not (RIGHTOF10-ROBOT))
)
:effect
(and
(Flag230prime-)
(not (Flag230-))
)

)
(:action Prim230Action-1
:parameters ()
:precondition
(and
(not (LEFTOF10-ROBOT))
)
:effect
(and
(Flag230prime-)
(not (Flag230-))
)

)
(:action Prim231Action-0
:parameters ()
:precondition
(and
(not (LEFTOF10-ROBOT))
)
:effect
(and
(Flag231prime-)
(not (Flag231-))
)

)
(:action Prim231Action-1
:parameters ()
:precondition
(and
(not (RIGHTOF9-ROBOT))
)
:effect
(and
(Flag231prime-)
(not (Flag231-))
)

)
(:action Flag233Action
:parameters ()
:precondition
(and
(RIGHTOF11-ROBOT)
(LEFTOF11-ROBOT)
)
:effect
(and
(Flag233-)
(Flag233prime-)
)

)
(:action Flag234Action
:parameters ()
:precondition
(and
(RIGHTOF10-ROBOT)
(LEFTOF11-ROBOT)
)
:effect
(and
(Flag234-)
(Flag234prime-)
)

)
(:action Prim233Action-0
:parameters ()
:precondition
(and
(not (RIGHTOF11-ROBOT))
)
:effect
(and
(Flag233prime-)
(not (Flag233-))
)

)
(:action Prim234Action-0
:parameters ()
:precondition
(and
(not (RIGHTOF10-ROBOT))
)
:effect
(and
(Flag234prime-)
(not (Flag234-))
)

)
(:action Flag236Action
:parameters ()
:precondition
(and
(RIGHTOF11-ROBOT)
(LEFTOF12-ROBOT)
)
:effect
(and
(Flag236-)
(Flag236prime-)
)

)
(:action Prim236Action-0
:parameters ()
:precondition
(and
(not (RIGHTOF11-ROBOT))
)
:effect
(and
(Flag236prime-)
(not (Flag236-))
)

)
(:action Prim236Action-1
:parameters ()
:precondition
(and
(not (LEFTOF12-ROBOT))
)
:effect
(and
(Flag236prime-)
(not (Flag236-))
)

)
(:action Flag238Action-0
:parameters ()
:precondition
(and
(RIGHTOF3-ROBOT)
)
:effect
(and
(Flag238-)
(Flag238prime-)
)

)
(:action Flag238Action-1
:parameters ()
:precondition
(and
(RIGHTOF8-ROBOT)
)
:effect
(and
(Flag238-)
(Flag238prime-)
)

)
(:action Flag238Action-2
:parameters ()
:precondition
(and
(RIGHTOF10-ROBOT)
)
:effect
(and
(Flag238-)
(Flag238prime-)
)

)
(:action Flag238Action-3
:parameters ()
:precondition
(and
(RIGHTOF5-ROBOT)
)
:effect
(and
(Flag238-)
(Flag238prime-)
)

)
(:action Flag238Action-5
:parameters ()
:precondition
(and
(RIGHTOF11-ROBOT)
)
:effect
(and
(Flag238-)
(Flag238prime-)
)

)
(:action Flag238Action-6
:parameters ()
:precondition
(and
(RIGHTOF0-ROBOT)
)
:effect
(and
(Flag238-)
(Flag238prime-)
)

)
(:action Flag238Action-7
:parameters ()
:precondition
(and
(RIGHTOF4-ROBOT)
)
:effect
(and
(Flag238-)
(Flag238prime-)
)

)
(:action Flag238Action-8
:parameters ()
:precondition
(and
(RIGHTOF1-ROBOT)
)
:effect
(and
(Flag238-)
(Flag238prime-)
)

)
(:action Flag238Action-9
:parameters ()
:precondition
(and
(RIGHTOF7-ROBOT)
)
:effect
(and
(Flag238-)
(Flag238prime-)
)

)
(:action Flag238Action-10
:parameters ()
:precondition
(and
(RIGHTOF9-ROBOT)
)
:effect
(and
(Flag238-)
(Flag238prime-)
)

)
(:action Flag238Action-11
:parameters ()
:precondition
(and
(RIGHTOF6-ROBOT)
)
:effect
(and
(Flag238-)
(Flag238prime-)
)

)
(:action Prim238Action
:parameters ()
:precondition
(and
(not (RIGHTOF3-ROBOT))
(not (RIGHTOF8-ROBOT))
(not (RIGHTOF10-ROBOT))
(not (RIGHTOF5-ROBOT))
(not (RIGHTOF2-ROBOT))
(not (RIGHTOF11-ROBOT))
(not (RIGHTOF0-ROBOT))
(not (RIGHTOF4-ROBOT))
(not (RIGHTOF1-ROBOT))
(not (RIGHTOF7-ROBOT))
(not (RIGHTOF9-ROBOT))
(not (RIGHTOF6-ROBOT))
)
:effect
(and
(Flag238prime-)
(not (Flag238-))
)

)
(:action Flag239Action
:parameters ()
:precondition
(and
(RIGHTOF0-ROBOT)
(LEFTOF1-ROBOT)
)
:effect
(and
(Flag239-)
(Flag239prime-)
)

)
(:action Prim239Action-0
:parameters ()
:precondition
(and
(not (RIGHTOF0-ROBOT))
)
:effect
(and
(Flag239prime-)
(not (Flag239-))
)

)
(:action Prim239Action-1
:parameters ()
:precondition
(and
(not (LEFTOF1-ROBOT))
)
:effect
(and
(Flag239prime-)
(not (Flag239-))
)

)
(:action Flag241Action-0
:parameters ()
:precondition
(and
(Flag44-)
(Flag44prime-)
)
:effect
(and
(Flag241-)
(Flag241prime-)
)

)
(:action Flag241Action-1
:parameters ()
:precondition
(and
(Flag35-)
(Flag35prime-)
)
:effect
(and
(Flag241-)
(Flag241prime-)
)

)
(:action Flag241Action-2
:parameters ()
:precondition
(and
(Flag31-)
(Flag31prime-)
)
:effect
(and
(Flag241-)
(Flag241prime-)
)

)
(:action Flag241Action-3
:parameters ()
:precondition
(and
(Flag30-)
(Flag30prime-)
)
:effect
(and
(Flag241-)
(Flag241prime-)
)

)
(:action Flag241Action-4
:parameters ()
:precondition
(and
(Flag27-)
(Flag27prime-)
)
:effect
(and
(Flag241-)
(Flag241prime-)
)

)
(:action Flag241Action-5
:parameters ()
:precondition
(and
(Flag37-)
(Flag37prime-)
)
:effect
(and
(Flag241-)
(Flag241prime-)
)

)
(:action Flag241Action-6
:parameters ()
:precondition
(and
(Flag32-)
(Flag32prime-)
)
:effect
(and
(Flag241-)
(Flag241prime-)
)

)
(:action Flag241Action-7
:parameters ()
:precondition
(and
(Flag38-)
(Flag38prime-)
)
:effect
(and
(Flag241-)
(Flag241prime-)
)

)
(:action Flag241Action-9
:parameters ()
:precondition
(and
(Flag46-)
(Flag46prime-)
)
:effect
(and
(Flag241-)
(Flag241prime-)
)

)
(:action Flag241Action-10
:parameters ()
:precondition
(and
(Flag40-)
(Flag40prime-)
)
:effect
(and
(Flag241-)
(Flag241prime-)
)

)
(:action Flag241Action-11
:parameters ()
:precondition
(and
(Flag41-)
(Flag41prime-)
)
:effect
(and
(Flag241-)
(Flag241prime-)
)

)
(:action Prim241Action
:parameters ()
:precondition
(and
(not (Flag44-))
(Flag44prime-)
(not (Flag35-))
(Flag35prime-)
(not (Flag31-))
(Flag31prime-)
(not (Flag30-))
(Flag30prime-)
(not (Flag27-))
(Flag27prime-)
(not (Flag37-))
(Flag37prime-)
(not (Flag32-))
(Flag32prime-)
(not (Flag38-))
(Flag38prime-)
(not (BELOWOF1-ROBOT))
(not (Flag46-))
(Flag46prime-)
(not (Flag40-))
(Flag40prime-)
(not (Flag41-))
(Flag41prime-)
)
:effect
(and
(Flag241prime-)
(not (Flag241-))
)

)
(:action MOVERIGHT-ROBOT
:parameters ()
:precondition
(and
(Flag4-)
(Flag4prime-)
(Flag235prime-)
(Flag232prime-)
(Flag228prime-)
(Flag223prime-)
(Flag216prime-)
(Flag208prime-)
(Flag200prime-)
(Flag189prime-)
(Flag178prime-)
(Flag167prime-)
(Flag240prime-)
(Flag238prime-)
)
:effect
(and
(when
(and
(Flag235-)
)
(and
(COLUMN11-ROBOT)
(not (NOT-COLUMN11-ROBOT))
)
)
(when
(and
(Flag232-)
)
(and
(COLUMN10-ROBOT)
(not (NOT-COLUMN10-ROBOT))
)
)
(when
(and
(Flag228-)
)
(and
(COLUMN9-ROBOT)
(not (NOT-COLUMN9-ROBOT))
)
)
(when
(and
(Flag223-)
)
(and
(COLUMN8-ROBOT)
(not (NOT-COLUMN8-ROBOT))
)
)
(when
(and
(Flag216-)
)
(and
(COLUMN7-ROBOT)
(not (NOT-COLUMN7-ROBOT))
)
)
(when
(and
(Flag208-)
)
(and
(COLUMN6-ROBOT)
(not (NOT-COLUMN6-ROBOT))
)
)
(when
(and
(Flag200-)
)
(and
(COLUMN5-ROBOT)
(not (NOT-COLUMN5-ROBOT))
)
)
(when
(and
(Flag189-)
)
(and
(COLUMN4-ROBOT)
(not (NOT-COLUMN4-ROBOT))
)
)
(when
(and
(Flag178-)
)
(and
(COLUMN3-ROBOT)
(not (NOT-COLUMN3-ROBOT))
)
)
(when
(and
(Flag167-)
)
(and
(COLUMN2-ROBOT)
(not (NOT-COLUMN2-ROBOT))
)
)
(when
(and
(Flag240-)
)
(and
(COLUMN1-ROBOT)
(not (NOT-COLUMN1-ROBOT))
)
)
(when
(and
(COLUMN10-ROBOT)
)
(and
(COLUMN11-ROBOT)
(NOT-COLUMN10-ROBOT)
(not (NOT-COLUMN11-ROBOT))
(not (COLUMN10-ROBOT))
)
)
(when
(and
(COLUMN9-ROBOT)
)
(and
(COLUMN10-ROBOT)
(NOT-COLUMN9-ROBOT)
(not (NOT-COLUMN10-ROBOT))
(not (COLUMN9-ROBOT))
)
)
(when
(and
(COLUMN8-ROBOT)
)
(and
(COLUMN9-ROBOT)
(NOT-COLUMN8-ROBOT)
(not (NOT-COLUMN9-ROBOT))
(not (COLUMN8-ROBOT))
)
)
(when
(and
(COLUMN7-ROBOT)
)
(and
(COLUMN8-ROBOT)
(NOT-COLUMN7-ROBOT)
(not (NOT-COLUMN8-ROBOT))
(not (COLUMN7-ROBOT))
)
)
(when
(and
(COLUMN6-ROBOT)
)
(and
(COLUMN7-ROBOT)
(NOT-COLUMN6-ROBOT)
(not (NOT-COLUMN7-ROBOT))
(not (COLUMN6-ROBOT))
)
)
(when
(and
(COLUMN5-ROBOT)
)
(and
(COLUMN6-ROBOT)
(NOT-COLUMN5-ROBOT)
(not (NOT-COLUMN6-ROBOT))
(not (COLUMN5-ROBOT))
)
)
(when
(and
(COLUMN4-ROBOT)
)
(and
(COLUMN5-ROBOT)
(NOT-COLUMN4-ROBOT)
(not (NOT-COLUMN5-ROBOT))
(not (COLUMN4-ROBOT))
)
)
(when
(and
(COLUMN3-ROBOT)
)
(and
(COLUMN4-ROBOT)
(NOT-COLUMN3-ROBOT)
(not (NOT-COLUMN4-ROBOT))
(not (COLUMN3-ROBOT))
)
)
(when
(and
(COLUMN2-ROBOT)
)
(and
(COLUMN3-ROBOT)
(NOT-COLUMN2-ROBOT)
(not (NOT-COLUMN3-ROBOT))
(not (COLUMN2-ROBOT))
)
)
(when
(and
(COLUMN1-ROBOT)
)
(and
(COLUMN2-ROBOT)
(NOT-COLUMN1-ROBOT)
(not (NOT-COLUMN2-ROBOT))
(not (COLUMN1-ROBOT))
)
)
(when
(and
(COLUMN0-ROBOT)
)
(and
(COLUMN1-ROBOT)
(NOT-COLUMN0-ROBOT)
(not (NOT-COLUMN1-ROBOT))
(not (COLUMN0-ROBOT))
)
)
(when
(and
(LEFTOF8-ROBOT)
)
(and
(LEFTOF12-ROBOT)
(NOT-LEFTOF11-ROBOT)
(LEFTOF11-ROBOT)
(NOT-LEFTOF10-ROBOT)
(LEFTOF10-ROBOT)
(NOT-LEFTOF9-ROBOT)
(LEFTOF9-ROBOT)
(NOT-LEFTOF8-ROBOT)
(not (NOT-LEFTOF12-ROBOT))
(not (LEFTOF8-ROBOT))
)
)
(when
(and
(LEFTOF4-ROBOT)
)
(and
(LEFTOF12-ROBOT)
(NOT-LEFTOF11-ROBOT)
(LEFTOF11-ROBOT)
(NOT-LEFTOF10-ROBOT)
(LEFTOF10-ROBOT)
(NOT-LEFTOF9-ROBOT)
(LEFTOF9-ROBOT)
(NOT-LEFTOF8-ROBOT)
(LEFTOF8-ROBOT)
(NOT-LEFTOF7-ROBOT)
(LEFTOF7-ROBOT)
(NOT-LEFTOF6-ROBOT)
(LEFTOF6-ROBOT)
(NOT-LEFTOF5-ROBOT)
(LEFTOF5-ROBOT)
(NOT-LEFTOF4-ROBOT)
(not (NOT-LEFTOF12-ROBOT))
(not (LEFTOF4-ROBOT))
)
)
(when
(and
(LEFTOF3-ROBOT)
)
(and
(LEFTOF12-ROBOT)
(NOT-LEFTOF11-ROBOT)
(LEFTOF11-ROBOT)
(NOT-LEFTOF10-ROBOT)
(LEFTOF10-ROBOT)
(NOT-LEFTOF9-ROBOT)
(LEFTOF9-ROBOT)
(NOT-LEFTOF8-ROBOT)
(LEFTOF8-ROBOT)
(NOT-LEFTOF7-ROBOT)
(LEFTOF7-ROBOT)
(NOT-LEFTOF6-ROBOT)
(LEFTOF6-ROBOT)
(NOT-LEFTOF5-ROBOT)
(LEFTOF5-ROBOT)
(NOT-LEFTOF4-ROBOT)
(LEFTOF4-ROBOT)
(NOT-LEFTOF3-ROBOT)
(not (NOT-LEFTOF12-ROBOT))
(not (LEFTOF3-ROBOT))
)
)
(when
(and
(LEFTOF5-ROBOT)
)
(and
(LEFTOF12-ROBOT)
(NOT-LEFTOF11-ROBOT)
(LEFTOF11-ROBOT)
(NOT-LEFTOF10-ROBOT)
(LEFTOF10-ROBOT)
(NOT-LEFTOF9-ROBOT)
(LEFTOF9-ROBOT)
(NOT-LEFTOF8-ROBOT)
(LEFTOF8-ROBOT)
(NOT-LEFTOF7-ROBOT)
(LEFTOF7-ROBOT)
(NOT-LEFTOF6-ROBOT)
(LEFTOF6-ROBOT)
(NOT-LEFTOF5-ROBOT)
(not (NOT-LEFTOF12-ROBOT))
(not (LEFTOF5-ROBOT))
)
)
(when
(and
(LEFTOF6-ROBOT)
)
(and
(LEFTOF12-ROBOT)
(NOT-LEFTOF11-ROBOT)
(LEFTOF11-ROBOT)
(NOT-LEFTOF10-ROBOT)
(LEFTOF10-ROBOT)
(NOT-LEFTOF9-ROBOT)
(LEFTOF9-ROBOT)
(NOT-LEFTOF8-ROBOT)
(LEFTOF8-ROBOT)
(NOT-LEFTOF7-ROBOT)
(LEFTOF7-ROBOT)
(NOT-LEFTOF6-ROBOT)
(not (NOT-LEFTOF12-ROBOT))
(not (LEFTOF6-ROBOT))
)
)
(when
(and
(LEFTOF1-ROBOT)
)
(and
(LEFTOF12-ROBOT)
(NOT-LEFTOF11-ROBOT)
(LEFTOF11-ROBOT)
(NOT-LEFTOF10-ROBOT)
(LEFTOF10-ROBOT)
(NOT-LEFTOF9-ROBOT)
(LEFTOF9-ROBOT)
(NOT-LEFTOF8-ROBOT)
(LEFTOF8-ROBOT)
(NOT-LEFTOF7-ROBOT)
(LEFTOF7-ROBOT)
(NOT-LEFTOF6-ROBOT)
(LEFTOF6-ROBOT)
(NOT-LEFTOF5-ROBOT)
(LEFTOF5-ROBOT)
(NOT-LEFTOF4-ROBOT)
(LEFTOF4-ROBOT)
(NOT-LEFTOF3-ROBOT)
(LEFTOF3-ROBOT)
(NOT-LEFTOF2-ROBOT)
(LEFTOF2-ROBOT)
(NOT-LEFTOF1-ROBOT)
(not (NOT-LEFTOF12-ROBOT))
(not (LEFTOF1-ROBOT))
)
)
(when
(and
(LEFTOF7-ROBOT)
)
(and
(LEFTOF12-ROBOT)
(NOT-LEFTOF11-ROBOT)
(LEFTOF11-ROBOT)
(NOT-LEFTOF10-ROBOT)
(LEFTOF10-ROBOT)
(NOT-LEFTOF9-ROBOT)
(LEFTOF9-ROBOT)
(NOT-LEFTOF8-ROBOT)
(LEFTOF8-ROBOT)
(NOT-LEFTOF7-ROBOT)
(not (NOT-LEFTOF12-ROBOT))
(not (LEFTOF7-ROBOT))
)
)
(when
(and
(LEFTOF2-ROBOT)
)
(and
(LEFTOF12-ROBOT)
(NOT-LEFTOF11-ROBOT)
(LEFTOF11-ROBOT)
(NOT-LEFTOF10-ROBOT)
(LEFTOF10-ROBOT)
(NOT-LEFTOF9-ROBOT)
(LEFTOF9-ROBOT)
(NOT-LEFTOF8-ROBOT)
(LEFTOF8-ROBOT)
(NOT-LEFTOF7-ROBOT)
(LEFTOF7-ROBOT)
(NOT-LEFTOF6-ROBOT)
(LEFTOF6-ROBOT)
(NOT-LEFTOF5-ROBOT)
(LEFTOF5-ROBOT)
(NOT-LEFTOF4-ROBOT)
(LEFTOF4-ROBOT)
(NOT-LEFTOF3-ROBOT)
(LEFTOF3-ROBOT)
(NOT-LEFTOF2-ROBOT)
(not (NOT-LEFTOF12-ROBOT))
(not (LEFTOF2-ROBOT))
)
)
(when
(and
(LEFTOF9-ROBOT)
)
(and
(LEFTOF12-ROBOT)
(NOT-LEFTOF11-ROBOT)
(LEFTOF11-ROBOT)
(NOT-LEFTOF10-ROBOT)
(LEFTOF10-ROBOT)
(NOT-LEFTOF9-ROBOT)
(not (NOT-LEFTOF12-ROBOT))
(not (LEFTOF9-ROBOT))
)
)
(when
(and
(LEFTOF11-ROBOT)
)
(and
(LEFTOF12-ROBOT)
(NOT-LEFTOF11-ROBOT)
(not (NOT-LEFTOF12-ROBOT))
(not (LEFTOF11-ROBOT))
)
)
(when
(and
(LEFTOF10-ROBOT)
)
(and
(LEFTOF12-ROBOT)
(NOT-LEFTOF11-ROBOT)
(LEFTOF11-ROBOT)
(NOT-LEFTOF10-ROBOT)
(not (NOT-LEFTOF12-ROBOT))
(not (LEFTOF10-ROBOT))
)
)
(when
(and
(RIGHTOF11-ROBOT)
)
(and
(RIGHTOF10-ROBOT)
(RIGHTOF9-ROBOT)
(RIGHTOF8-ROBOT)
(RIGHTOF7-ROBOT)
(RIGHTOF6-ROBOT)
(RIGHTOF5-ROBOT)
(RIGHTOF4-ROBOT)
(RIGHTOF3-ROBOT)
(RIGHTOF2-ROBOT)
(not (NOT-RIGHTOF11-ROBOT))
(not (NOT-RIGHTOF10-ROBOT))
(not (NOT-RIGHTOF9-ROBOT))
(not (NOT-RIGHTOF8-ROBOT))
(not (NOT-RIGHTOF7-ROBOT))
(not (NOT-RIGHTOF6-ROBOT))
(not (NOT-RIGHTOF5-ROBOT))
(not (NOT-RIGHTOF4-ROBOT))
(not (NOT-RIGHTOF3-ROBOT))
(not (NOT-RIGHTOF2-ROBOT))
)
)
(when
(and
(RIGHTOF10-ROBOT)
)
(and
(RIGHTOF11-ROBOT)
(RIGHTOF9-ROBOT)
(RIGHTOF8-ROBOT)
(RIGHTOF7-ROBOT)
(RIGHTOF6-ROBOT)
(RIGHTOF5-ROBOT)
(RIGHTOF4-ROBOT)
(RIGHTOF3-ROBOT)
(RIGHTOF2-ROBOT)
(not (NOT-RIGHTOF11-ROBOT))
(not (NOT-RIGHTOF10-ROBOT))
(not (NOT-RIGHTOF9-ROBOT))
(not (NOT-RIGHTOF8-ROBOT))
(not (NOT-RIGHTOF7-ROBOT))
(not (NOT-RIGHTOF6-ROBOT))
(not (NOT-RIGHTOF5-ROBOT))
(not (NOT-RIGHTOF4-ROBOT))
(not (NOT-RIGHTOF3-ROBOT))
(not (NOT-RIGHTOF2-ROBOT))
)
)
(when
(and
(RIGHTOF9-ROBOT)
)
(and
(RIGHTOF10-ROBOT)
(RIGHTOF8-ROBOT)
(RIGHTOF7-ROBOT)
(RIGHTOF6-ROBOT)
(RIGHTOF5-ROBOT)
(RIGHTOF4-ROBOT)
(RIGHTOF3-ROBOT)
(RIGHTOF2-ROBOT)
(not (NOT-RIGHTOF10-ROBOT))
(not (NOT-RIGHTOF9-ROBOT))
(not (NOT-RIGHTOF8-ROBOT))
(not (NOT-RIGHTOF7-ROBOT))
(not (NOT-RIGHTOF6-ROBOT))
(not (NOT-RIGHTOF5-ROBOT))
(not (NOT-RIGHTOF4-ROBOT))
(not (NOT-RIGHTOF3-ROBOT))
(not (NOT-RIGHTOF2-ROBOT))
)
)
(when
(and
(RIGHTOF8-ROBOT)
)
(and
(RIGHTOF9-ROBOT)
(RIGHTOF7-ROBOT)
(RIGHTOF6-ROBOT)
(RIGHTOF5-ROBOT)
(RIGHTOF4-ROBOT)
(RIGHTOF3-ROBOT)
(RIGHTOF2-ROBOT)
(not (NOT-RIGHTOF9-ROBOT))
(not (NOT-RIGHTOF8-ROBOT))
(not (NOT-RIGHTOF7-ROBOT))
(not (NOT-RIGHTOF6-ROBOT))
(not (NOT-RIGHTOF5-ROBOT))
(not (NOT-RIGHTOF4-ROBOT))
(not (NOT-RIGHTOF3-ROBOT))
(not (NOT-RIGHTOF2-ROBOT))
)
)
(when
(and
(RIGHTOF7-ROBOT)
)
(and
(RIGHTOF8-ROBOT)
(RIGHTOF6-ROBOT)
(RIGHTOF5-ROBOT)
(RIGHTOF4-ROBOT)
(RIGHTOF3-ROBOT)
(RIGHTOF2-ROBOT)
(not (NOT-RIGHTOF8-ROBOT))
(not (NOT-RIGHTOF7-ROBOT))
(not (NOT-RIGHTOF6-ROBOT))
(not (NOT-RIGHTOF5-ROBOT))
(not (NOT-RIGHTOF4-ROBOT))
(not (NOT-RIGHTOF3-ROBOT))
(not (NOT-RIGHTOF2-ROBOT))
)
)
(when
(and
(RIGHTOF6-ROBOT)
)
(and
(RIGHTOF7-ROBOT)
(RIGHTOF5-ROBOT)
(RIGHTOF4-ROBOT)
(RIGHTOF3-ROBOT)
(RIGHTOF2-ROBOT)
(not (NOT-RIGHTOF7-ROBOT))
(not (NOT-RIGHTOF6-ROBOT))
(not (NOT-RIGHTOF5-ROBOT))
(not (NOT-RIGHTOF4-ROBOT))
(not (NOT-RIGHTOF3-ROBOT))
(not (NOT-RIGHTOF2-ROBOT))
)
)
(when
(and
(RIGHTOF5-ROBOT)
)
(and
(RIGHTOF6-ROBOT)
(RIGHTOF4-ROBOT)
(RIGHTOF3-ROBOT)
(RIGHTOF2-ROBOT)
(not (NOT-RIGHTOF6-ROBOT))
(not (NOT-RIGHTOF5-ROBOT))
(not (NOT-RIGHTOF4-ROBOT))
(not (NOT-RIGHTOF3-ROBOT))
(not (NOT-RIGHTOF2-ROBOT))
)
)
(when
(and
(RIGHTOF4-ROBOT)
)
(and
(RIGHTOF5-ROBOT)
(RIGHTOF3-ROBOT)
(RIGHTOF2-ROBOT)
(not (NOT-RIGHTOF5-ROBOT))
(not (NOT-RIGHTOF4-ROBOT))
(not (NOT-RIGHTOF3-ROBOT))
(not (NOT-RIGHTOF2-ROBOT))
)
)
(when
(and
(RIGHTOF3-ROBOT)
)
(and
(RIGHTOF4-ROBOT)
(RIGHTOF2-ROBOT)
(not (NOT-RIGHTOF4-ROBOT))
(not (NOT-RIGHTOF3-ROBOT))
(not (NOT-RIGHTOF2-ROBOT))
)
)
(when
(and
(RIGHTOF2-ROBOT)
)
(and
(RIGHTOF3-ROBOT)
(not (NOT-RIGHTOF3-ROBOT))
(not (NOT-RIGHTOF2-ROBOT))
)
)
(when
(and
(RIGHTOF1-ROBOT)
)
(and
(RIGHTOF2-ROBOT)
(not (NOT-RIGHTOF2-ROBOT))
)
)
(when
(and
(Flag238-)
)
(and
(RIGHTOF1-ROBOT)
(not (NOT-RIGHTOF1-ROBOT))
)
)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag238prime-))
(not (Flag135prime-))
(not (Flag134prime-))
(not (Flag133prime-))
(not (Flag132prime-))
(not (Flag15prime-))
(not (Flag14prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag241prime-))
(not (Flag121prime-))
(not (Flag119prime-))
(not (Flag118prime-))
(not (Flag116prime-))
(not (Flag115prime-))
(not (Flag114prime-))
(not (Flag113prime-))
(not (Flag111prime-))
(not (Flag110prime-))
(not (Flag109prime-))
(not (Flag108prime-))
(not (Flag107prime-))
(not (Flag105prime-))
(not (Flag104prime-))
(not (Flag103prime-))
(not (Flag102prime-))
(not (Flag101prime-))
(not (Flag100prime-))
(not (Flag98prime-))
(not (Flag97prime-))
(not (Flag96prime-))
(not (Flag95prime-))
(not (Flag94prime-))
(not (Flag93prime-))
(not (Flag92prime-))
(not (Flag91prime-))
(not (Flag89prime-))
(not (Flag88prime-))
(not (Flag87prime-))
(not (Flag86prime-))
(not (Flag85prime-))
(not (Flag84prime-))
(not (Flag83prime-))
(not (Flag82prime-))
(not (Flag80prime-))
(not (Flag79prime-))
(not (Flag78prime-))
(not (Flag77prime-))
(not (Flag76prime-))
(not (Flag75prime-))
(not (Flag74prime-))
(not (Flag73prime-))
(not (Flag71prime-))
(not (Flag70prime-))
(not (Flag69prime-))
(not (Flag68prime-))
(not (Flag67prime-))
(not (Flag66prime-))
(not (Flag65prime-))
(not (Flag64prime-))
(not (Flag63prime-))
(not (Flag62prime-))
(not (Flag61prime-))
(not (Flag59prime-))
(not (Flag58prime-))
(not (Flag57prime-))
(not (Flag56prime-))
(not (Flag55prime-))
(not (Flag54prime-))
(not (Flag53prime-))
(not (Flag52prime-))
(not (Flag51prime-))
(not (Flag50prime-))
(not (Flag49prime-))
(not (Flag47prime-))
(not (Flag46prime-))
(not (Flag45prime-))
(not (Flag44prime-))
(not (Flag43prime-))
(not (Flag42prime-))
(not (Flag41prime-))
(not (Flag40prime-))
(not (Flag39prime-))
(not (Flag38prime-))
(not (Flag37prime-))
(not (Flag36prime-))
(not (Flag35prime-))
(not (Flag34prime-))
(not (Flag33prime-))
(not (Flag32prime-))
(not (Flag31prime-))
(not (Flag30prime-))
(not (Flag29prime-))
(not (Flag28prime-))
(not (Flag27prime-))
(not (Flag26prime-))
(not (Flag25prime-))
(not (Flag24prime-))
(not (Flag23prime-))
(not (Flag22prime-))
(not (Flag21prime-))
(not (Flag20prime-))
(not (Flag19prime-))
(not (Flag18prime-))
(not (Flag17prime-))
(not (Flag16prime-))
(not (Flag13prime-))
(not (Flag12prime-))
(not (Flag11prime-))
(not (Flag10prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag239prime-))
(not (Flag236prime-))
(not (Flag234prime-))
(not (Flag233prime-))
(not (Flag231prime-))
(not (Flag230prime-))
(not (Flag229prime-))
(not (Flag227prime-))
(not (Flag226prime-))
(not (Flag225prime-))
(not (Flag224prime-))
(not (Flag222prime-))
(not (Flag221prime-))
(not (Flag220prime-))
(not (Flag219prime-))
(not (Flag218prime-))
(not (Flag217prime-))
(not (Flag215prime-))
(not (Flag214prime-))
(not (Flag213prime-))
(not (Flag212prime-))
(not (Flag211prime-))
(not (Flag210prime-))
(not (Flag209prime-))
(not (Flag207prime-))
(not (Flag206prime-))
(not (Flag205prime-))
(not (Flag204prime-))
(not (Flag203prime-))
(not (Flag202prime-))
(not (Flag201prime-))
(not (Flag199prime-))
(not (Flag198prime-))
(not (Flag197prime-))
(not (Flag196prime-))
(not (Flag195prime-))
(not (Flag194prime-))
(not (Flag193prime-))
(not (Flag192prime-))
(not (Flag191prime-))
(not (Flag190prime-))
(not (Flag188prime-))
(not (Flag187prime-))
(not (Flag186prime-))
(not (Flag185prime-))
(not (Flag184prime-))
(not (Flag183prime-))
(not (Flag182prime-))
(not (Flag181prime-))
(not (Flag180prime-))
(not (Flag179prime-))
(not (Flag177prime-))
(not (Flag176prime-))
(not (Flag175prime-))
(not (Flag174prime-))
(not (Flag173prime-))
(not (Flag172prime-))
(not (Flag171prime-))
(not (Flag170prime-))
(not (Flag169prime-))
(not (Flag168prime-))
(not (Flag166prime-))
(not (Flag165prime-))
(not (Flag164prime-))
(not (Flag163prime-))
(not (Flag162prime-))
(not (Flag161prime-))
(not (Flag160prime-))
(not (Flag159prime-))
(not (Flag158prime-))
(not (Flag157prime-))
(not (Flag156prime-))
(not (Flag155prime-))
(not (Flag154prime-))
(not (Flag153prime-))
(not (Flag152prime-))
(not (Flag151prime-))
(not (Flag150prime-))
(not (Flag149prime-))
(not (Flag148prime-))
(not (Flag147prime-))
(not (Flag146prime-))
(not (Flag145prime-))
(not (Flag144prime-))
(not (Flag143prime-))
(not (Flag142prime-))
(not (Flag141prime-))
(not (Flag140prime-))
(not (Flag139prime-))
(not (Flag138prime-))
(not (Flag137prime-))
(not (Flag136prime-))
(not (Flag131prime-))
(not (Flag130prime-))
(not (Flag129prime-))
(not (Flag128prime-))
(not (Flag127prime-))
(not (Flag126prime-))
(not (Flag125prime-))
(not (Flag124prime-))
(not (Flag123prime-))
(not (Flag122prime-))
(not (Flag120prime-))
(not (Flag117prime-))
(not (Flag112prime-))
(not (Flag106prime-))
(not (Flag99prime-))
(not (Flag90prime-))
(not (Flag81prime-))
(not (Flag72prime-))
(not (Flag60prime-))
(not (Flag48prime-))
(not (Flag1prime-))
(not (Flag240prime-))
(not (Flag237prime-))
(not (Flag235prime-))
(not (Flag232prime-))
(not (Flag228prime-))
(not (Flag223prime-))
(not (Flag216prime-))
(not (Flag208prime-))
(not (Flag200prime-))
(not (Flag189prime-))
(not (Flag178prime-))
(not (Flag167prime-))
)
)
(:action MOVELEFT-ROBOT
:parameters ()
:precondition
(and
(Flag4-)
(Flag4prime-)
(Flag237prime-)
(Flag235prime-)
(Flag232prime-)
(Flag228prime-)
(Flag223prime-)
(Flag216prime-)
(Flag208prime-)
(Flag200prime-)
(Flag189prime-)
(Flag178prime-)
(Flag167prime-)
(Flag143prime-)
(Flag142prime-)
(Flag141prime-)
(Flag140prime-)
(Flag139prime-)
(Flag138prime-)
(Flag137prime-)
(Flag136prime-)
(Flag135prime-)
(Flag134prime-)
(Flag133prime-)
(Flag132prime-)
(Flag131prime-)
(Flag130prime-)
(Flag129prime-)
(Flag128prime-)
(Flag127prime-)
(Flag126prime-)
(Flag125prime-)
(Flag124prime-)
(Flag123prime-)
)
:effect
(and
(when
(and
(Flag237-)
)
(and
(COLUMN10-ROBOT)
(not (NOT-COLUMN10-ROBOT))
)
)
(when
(and
(Flag235-)
)
(and
(COLUMN9-ROBOT)
(not (NOT-COLUMN9-ROBOT))
)
)
(when
(and
(Flag232-)
)
(and
(COLUMN8-ROBOT)
(not (NOT-COLUMN8-ROBOT))
)
)
(when
(and
(Flag228-)
)
(and
(COLUMN7-ROBOT)
(not (NOT-COLUMN7-ROBOT))
)
)
(when
(and
(Flag223-)
)
(and
(COLUMN6-ROBOT)
(not (NOT-COLUMN6-ROBOT))
)
)
(when
(and
(Flag216-)
)
(and
(COLUMN5-ROBOT)
(not (NOT-COLUMN5-ROBOT))
)
)
(when
(and
(Flag208-)
)
(and
(COLUMN4-ROBOT)
(not (NOT-COLUMN4-ROBOT))
)
)
(when
(and
(Flag200-)
)
(and
(COLUMN3-ROBOT)
(not (NOT-COLUMN3-ROBOT))
)
)
(when
(and
(Flag189-)
)
(and
(COLUMN2-ROBOT)
(not (NOT-COLUMN2-ROBOT))
)
)
(when
(and
(Flag178-)
)
(and
(COLUMN1-ROBOT)
(not (NOT-COLUMN1-ROBOT))
)
)
(when
(and
(Flag167-)
)
(and
(COLUMN0-ROBOT)
(not (NOT-COLUMN0-ROBOT))
)
)
(when
(and
(COLUMN11-ROBOT)
)
(and
(COLUMN10-ROBOT)
(NOT-COLUMN11-ROBOT)
(not (NOT-COLUMN10-ROBOT))
(not (COLUMN11-ROBOT))
)
)
(when
(and
(COLUMN10-ROBOT)
)
(and
(COLUMN9-ROBOT)
(NOT-COLUMN10-ROBOT)
(not (NOT-COLUMN9-ROBOT))
(not (COLUMN10-ROBOT))
)
)
(when
(and
(COLUMN9-ROBOT)
)
(and
(COLUMN8-ROBOT)
(NOT-COLUMN9-ROBOT)
(not (NOT-COLUMN8-ROBOT))
(not (COLUMN9-ROBOT))
)
)
(when
(and
(COLUMN8-ROBOT)
)
(and
(COLUMN7-ROBOT)
(NOT-COLUMN8-ROBOT)
(not (NOT-COLUMN7-ROBOT))
(not (COLUMN8-ROBOT))
)
)
(when
(and
(COLUMN7-ROBOT)
)
(and
(COLUMN6-ROBOT)
(NOT-COLUMN7-ROBOT)
(not (NOT-COLUMN6-ROBOT))
(not (COLUMN7-ROBOT))
)
)
(when
(and
(COLUMN6-ROBOT)
)
(and
(COLUMN5-ROBOT)
(NOT-COLUMN6-ROBOT)
(not (NOT-COLUMN5-ROBOT))
(not (COLUMN6-ROBOT))
)
)
(when
(and
(COLUMN5-ROBOT)
)
(and
(COLUMN4-ROBOT)
(NOT-COLUMN5-ROBOT)
(not (NOT-COLUMN4-ROBOT))
(not (COLUMN5-ROBOT))
)
)
(when
(and
(COLUMN4-ROBOT)
)
(and
(COLUMN3-ROBOT)
(NOT-COLUMN4-ROBOT)
(not (NOT-COLUMN3-ROBOT))
(not (COLUMN4-ROBOT))
)
)
(when
(and
(COLUMN3-ROBOT)
)
(and
(COLUMN2-ROBOT)
(NOT-COLUMN3-ROBOT)
(not (NOT-COLUMN2-ROBOT))
(not (COLUMN3-ROBOT))
)
)
(when
(and
(COLUMN2-ROBOT)
)
(and
(COLUMN1-ROBOT)
(NOT-COLUMN2-ROBOT)
(not (NOT-COLUMN1-ROBOT))
(not (COLUMN2-ROBOT))
)
)
(when
(and
(COLUMN1-ROBOT)
)
(and
(COLUMN0-ROBOT)
(NOT-COLUMN1-ROBOT)
(not (NOT-COLUMN0-ROBOT))
(not (COLUMN1-ROBOT))
)
)
(when
(and
(RIGHTOF11-ROBOT)
)
(and
(RIGHTOF10-ROBOT)
(NOT-RIGHTOF11-ROBOT)
(not (NOT-RIGHTOF10-ROBOT))
(not (RIGHTOF11-ROBOT))
)
)
(when
(and
(Flag143-)
)
(and
(RIGHTOF9-ROBOT)
(NOT-RIGHTOF10-ROBOT)
(not (NOT-RIGHTOF9-ROBOT))
(not (RIGHTOF10-ROBOT))
)
)
(when
(and
(Flag142-)
)
(and
(RIGHTOF8-ROBOT)
(NOT-RIGHTOF9-ROBOT)
(not (NOT-RIGHTOF8-ROBOT))
(not (RIGHTOF9-ROBOT))
)
)
(when
(and
(Flag141-)
)
(and
(RIGHTOF7-ROBOT)
(NOT-RIGHTOF8-ROBOT)
(not (NOT-RIGHTOF7-ROBOT))
(not (RIGHTOF8-ROBOT))
)
)
(when
(and
(Flag140-)
)
(and
(RIGHTOF6-ROBOT)
(NOT-RIGHTOF7-ROBOT)
(not (NOT-RIGHTOF6-ROBOT))
(not (RIGHTOF7-ROBOT))
)
)
(when
(and
(Flag139-)
)
(and
(RIGHTOF5-ROBOT)
(NOT-RIGHTOF6-ROBOT)
(not (NOT-RIGHTOF5-ROBOT))
(not (RIGHTOF6-ROBOT))
)
)
(when
(and
(Flag138-)
)
(and
(RIGHTOF4-ROBOT)
(NOT-RIGHTOF5-ROBOT)
(not (NOT-RIGHTOF4-ROBOT))
(not (RIGHTOF5-ROBOT))
)
)
(when
(and
(Flag137-)
)
(and
(RIGHTOF3-ROBOT)
(NOT-RIGHTOF4-ROBOT)
(not (NOT-RIGHTOF3-ROBOT))
(not (RIGHTOF4-ROBOT))
)
)
(when
(and
(Flag136-)
)
(and
(RIGHTOF2-ROBOT)
(NOT-RIGHTOF3-ROBOT)
(not (NOT-RIGHTOF2-ROBOT))
(not (RIGHTOF3-ROBOT))
)
)
(when
(and
(Flag135-)
)
(and
(RIGHTOF1-ROBOT)
(NOT-RIGHTOF2-ROBOT)
(not (NOT-RIGHTOF1-ROBOT))
(not (RIGHTOF2-ROBOT))
)
)
(when
(and
(Flag134-)
)
(and
(RIGHTOF0-ROBOT)
(NOT-RIGHTOF1-ROBOT)
(not (NOT-RIGHTOF0-ROBOT))
(not (RIGHTOF1-ROBOT))
)
)
(when
(and
(Flag133-)
)
(and
(LEFTOF11-ROBOT)
(not (NOT-LEFTOF11-ROBOT))
)
)
(when
(and
(Flag132-)
)
(and
(LEFTOF10-ROBOT)
(not (NOT-LEFTOF10-ROBOT))
)
)
(when
(and
(Flag131-)
)
(and
(LEFTOF9-ROBOT)
(not (NOT-LEFTOF9-ROBOT))
)
)
(when
(and
(Flag130-)
)
(and
(LEFTOF8-ROBOT)
(not (NOT-LEFTOF8-ROBOT))
)
)
(when
(and
(Flag129-)
)
(and
(LEFTOF7-ROBOT)
(not (NOT-LEFTOF7-ROBOT))
)
)
(when
(and
(Flag128-)
)
(and
(LEFTOF6-ROBOT)
(not (NOT-LEFTOF6-ROBOT))
)
)
(when
(and
(Flag127-)
)
(and
(LEFTOF5-ROBOT)
(not (NOT-LEFTOF5-ROBOT))
)
)
(when
(and
(Flag126-)
)
(and
(LEFTOF4-ROBOT)
(not (NOT-LEFTOF4-ROBOT))
)
)
(when
(and
(Flag125-)
)
(and
(LEFTOF3-ROBOT)
(not (NOT-LEFTOF3-ROBOT))
)
)
(when
(and
(Flag124-)
)
(and
(LEFTOF2-ROBOT)
(not (NOT-LEFTOF2-ROBOT))
)
)
(when
(and
(Flag123-)
)
(and
(LEFTOF1-ROBOT)
(not (NOT-LEFTOF1-ROBOT))
)
)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag238prime-))
(not (Flag135prime-))
(not (Flag134prime-))
(not (Flag133prime-))
(not (Flag132prime-))
(not (Flag15prime-))
(not (Flag14prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag241prime-))
(not (Flag121prime-))
(not (Flag119prime-))
(not (Flag118prime-))
(not (Flag116prime-))
(not (Flag115prime-))
(not (Flag114prime-))
(not (Flag113prime-))
(not (Flag111prime-))
(not (Flag110prime-))
(not (Flag109prime-))
(not (Flag108prime-))
(not (Flag107prime-))
(not (Flag105prime-))
(not (Flag104prime-))
(not (Flag103prime-))
(not (Flag102prime-))
(not (Flag101prime-))
(not (Flag100prime-))
(not (Flag98prime-))
(not (Flag97prime-))
(not (Flag96prime-))
(not (Flag95prime-))
(not (Flag94prime-))
(not (Flag93prime-))
(not (Flag92prime-))
(not (Flag91prime-))
(not (Flag89prime-))
(not (Flag88prime-))
(not (Flag87prime-))
(not (Flag86prime-))
(not (Flag85prime-))
(not (Flag84prime-))
(not (Flag83prime-))
(not (Flag82prime-))
(not (Flag80prime-))
(not (Flag79prime-))
(not (Flag78prime-))
(not (Flag77prime-))
(not (Flag76prime-))
(not (Flag75prime-))
(not (Flag74prime-))
(not (Flag73prime-))
(not (Flag71prime-))
(not (Flag70prime-))
(not (Flag69prime-))
(not (Flag68prime-))
(not (Flag67prime-))
(not (Flag66prime-))
(not (Flag65prime-))
(not (Flag64prime-))
(not (Flag63prime-))
(not (Flag62prime-))
(not (Flag61prime-))
(not (Flag59prime-))
(not (Flag58prime-))
(not (Flag57prime-))
(not (Flag56prime-))
(not (Flag55prime-))
(not (Flag54prime-))
(not (Flag53prime-))
(not (Flag52prime-))
(not (Flag51prime-))
(not (Flag50prime-))
(not (Flag49prime-))
(not (Flag47prime-))
(not (Flag46prime-))
(not (Flag45prime-))
(not (Flag44prime-))
(not (Flag43prime-))
(not (Flag42prime-))
(not (Flag41prime-))
(not (Flag40prime-))
(not (Flag39prime-))
(not (Flag38prime-))
(not (Flag37prime-))
(not (Flag36prime-))
(not (Flag35prime-))
(not (Flag34prime-))
(not (Flag33prime-))
(not (Flag32prime-))
(not (Flag31prime-))
(not (Flag30prime-))
(not (Flag29prime-))
(not (Flag28prime-))
(not (Flag27prime-))
(not (Flag26prime-))
(not (Flag25prime-))
(not (Flag24prime-))
(not (Flag23prime-))
(not (Flag22prime-))
(not (Flag21prime-))
(not (Flag20prime-))
(not (Flag19prime-))
(not (Flag18prime-))
(not (Flag17prime-))
(not (Flag16prime-))
(not (Flag13prime-))
(not (Flag12prime-))
(not (Flag11prime-))
(not (Flag10prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag239prime-))
(not (Flag236prime-))
(not (Flag234prime-))
(not (Flag233prime-))
(not (Flag231prime-))
(not (Flag230prime-))
(not (Flag229prime-))
(not (Flag227prime-))
(not (Flag226prime-))
(not (Flag225prime-))
(not (Flag224prime-))
(not (Flag222prime-))
(not (Flag221prime-))
(not (Flag220prime-))
(not (Flag219prime-))
(not (Flag218prime-))
(not (Flag217prime-))
(not (Flag215prime-))
(not (Flag214prime-))
(not (Flag213prime-))
(not (Flag212prime-))
(not (Flag211prime-))
(not (Flag210prime-))
(not (Flag209prime-))
(not (Flag207prime-))
(not (Flag206prime-))
(not (Flag205prime-))
(not (Flag204prime-))
(not (Flag203prime-))
(not (Flag202prime-))
(not (Flag201prime-))
(not (Flag199prime-))
(not (Flag198prime-))
(not (Flag197prime-))
(not (Flag196prime-))
(not (Flag195prime-))
(not (Flag194prime-))
(not (Flag193prime-))
(not (Flag192prime-))
(not (Flag191prime-))
(not (Flag190prime-))
(not (Flag188prime-))
(not (Flag187prime-))
(not (Flag186prime-))
(not (Flag185prime-))
(not (Flag184prime-))
(not (Flag183prime-))
(not (Flag182prime-))
(not (Flag181prime-))
(not (Flag180prime-))
(not (Flag179prime-))
(not (Flag177prime-))
(not (Flag176prime-))
(not (Flag175prime-))
(not (Flag174prime-))
(not (Flag173prime-))
(not (Flag172prime-))
(not (Flag171prime-))
(not (Flag170prime-))
(not (Flag169prime-))
(not (Flag168prime-))
(not (Flag166prime-))
(not (Flag165prime-))
(not (Flag164prime-))
(not (Flag163prime-))
(not (Flag162prime-))
(not (Flag161prime-))
(not (Flag160prime-))
(not (Flag159prime-))
(not (Flag158prime-))
(not (Flag157prime-))
(not (Flag156prime-))
(not (Flag155prime-))
(not (Flag154prime-))
(not (Flag153prime-))
(not (Flag152prime-))
(not (Flag151prime-))
(not (Flag150prime-))
(not (Flag149prime-))
(not (Flag148prime-))
(not (Flag147prime-))
(not (Flag146prime-))
(not (Flag145prime-))
(not (Flag144prime-))
(not (Flag143prime-))
(not (Flag142prime-))
(not (Flag141prime-))
(not (Flag140prime-))
(not (Flag139prime-))
(not (Flag138prime-))
(not (Flag137prime-))
(not (Flag136prime-))
(not (Flag131prime-))
(not (Flag130prime-))
(not (Flag129prime-))
(not (Flag128prime-))
(not (Flag127prime-))
(not (Flag126prime-))
(not (Flag125prime-))
(not (Flag124prime-))
(not (Flag123prime-))
(not (Flag122prime-))
(not (Flag120prime-))
(not (Flag117prime-))
(not (Flag112prime-))
(not (Flag106prime-))
(not (Flag99prime-))
(not (Flag90prime-))
(not (Flag81prime-))
(not (Flag72prime-))
(not (Flag60prime-))
(not (Flag48prime-))
(not (Flag1prime-))
(not (Flag240prime-))
(not (Flag237prime-))
(not (Flag235prime-))
(not (Flag232prime-))
(not (Flag228prime-))
(not (Flag223prime-))
(not (Flag216prime-))
(not (Flag208prime-))
(not (Flag200prime-))
(not (Flag189prime-))
(not (Flag178prime-))
(not (Flag167prime-))
)
)
(:action Prim1Action-1
:parameters ()
:precondition
(and
(not (ROW1-ROBOT))
)
:effect
(and
(Flag1prime-)
(not (Flag1-))
)

)
(:action CHECKCONSISTENCYACTION
:parameters ()
:precondition
(and
(NOT-ERROR-)
(CHECKCONSISTENCY-)
)
:effect
(and
(when
(and
(ABOVEOF7-ROBOT)
(BELOWOF1-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF11-ROBOT)
(BELOWOF1-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF3-ROBOT)
(RIGHTOF5-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF1-ROBOT)
(ABOVEOF4-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF2-ROBOT)
(ABOVEOF7-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF10-ROBOT)
(LEFTOF4-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF11-ROBOT)
(BELOWOF10-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF3-ROBOT)
(ABOVEOF8-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF8-ROBOT)
(ABOVEOF11-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF6-ROBOT)
(RIGHTOF11-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF11-ROBOT)
(LEFTOF5-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF5-ROBOT)
(ABOVEOF11-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF7-ROBOT)
(RIGHTOF9-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF10-ROBOT)
(BELOWOF2-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF8-ROBOT)
(BELOWOF1-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF10-ROBOT)
(LEFTOF1-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF4-ROBOT)
(ABOVEOF11-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF11-ROBOT)
(LEFTOF9-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF8-ROBOT)
(LEFTOF6-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF1-ROBOT)
(RIGHTOF7-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF4-ROBOT)
(RIGHTOF8-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF11-ROBOT)
(BELOWOF7-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF11-ROBOT)
(BELOWOF9-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF11-ROBOT)
(BELOWOF3-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF4-ROBOT)
(ABOVEOF8-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF11-ROBOT)
(LEFTOF8-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF6-ROBOT)
(LEFTOF1-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF2-ROBOT)
(RIGHTOF10-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF1-ROBOT)
(ABOVEOF2-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF11-ROBOT)
(LEFTOF7-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF1-ROBOT)
(ABOVEOF9-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF3-ROBOT)
(ABOVEOF9-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF1-ROBOT)
(RIGHTOF1-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF5-ROBOT)
(RIGHTOF9-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF10-ROBOT)
(LEFTOF5-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF2-ROBOT)
(RIGHTOF6-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF5-ROBOT)
(RIGHTOF8-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF10-ROBOT)
(BELOWOF3-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF4-ROBOT)
(RIGHTOF9-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF6-ROBOT)
(BELOWOF1-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF11-ROBOT)
(BELOWOF2-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF4-ROBOT)
(RIGHTOF11-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF1-ROBOT)
(RIGHTOF3-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF2-ROBOT)
(LEFTOF2-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF5-ROBOT)
(ABOVEOF9-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF10-ROBOT)
(BELOWOF1-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF4-ROBOT)
(RIGHTOF7-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF1-ROBOT)
(RIGHTOF5-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF9-ROBOT)
(BELOWOF2-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF8-ROBOT)
(ABOVEOF10-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF8-ROBOT)
(LEFTOF3-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF9-ROBOT)
(BELOWOF7-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF2-ROBOT)
(ABOVEOF4-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF10-ROBOT)
(BELOWOF6-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF3-ROBOT)
(LEFTOF3-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF6-ROBOT)
(RIGHTOF9-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF6-ROBOT)
(ABOVEOF11-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF8-ROBOT)
(RIGHTOF8-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF4-ROBOT)
(LEFTOF1-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF6-ROBOT)
(LEFTOF3-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF5-ROBOT)
(ABOVEOF10-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF7-ROBOT)
(LEFTOF3-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF7-ROBOT)
(RIGHTOF10-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF10-ROBOT)
(LEFTOF3-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF4-ROBOT)
(ABOVEOF10-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF3-ROBOT)
(RIGHTOF4-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF6-ROBOT)
(ABOVEOF9-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF6-ROBOT)
(BELOWOF6-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF3-ROBOT)
(ABOVEOF7-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF8-ROBOT)
(RIGHTOF9-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF8-ROBOT)
(ABOVEOF9-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF2-ROBOT)
(RIGHTOF8-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF2-ROBOT)
(RIGHTOF9-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF7-ROBOT)
(LEFTOF6-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF6-ROBOT)
(ABOVEOF8-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF1-ROBOT)
(ABOVEOF5-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF6-ROBOT)
(LEFTOF4-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF3-ROBOT)
(RIGHTOF11-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF1-ROBOT)
(RIGHTOF8-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF2-ROBOT)
(ABOVEOF6-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF10-ROBOT)
(LEFTOF10-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF11-ROBOT)
(LEFTOF1-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF2-ROBOT)
(RIGHTOF5-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF2-ROBOT)
(ABOVEOF3-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF6-ROBOT)
(LEFTOF5-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF10-ROBOT)
(ABOVEOF10-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF5-ROBOT)
(BELOWOF5-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF2-ROBOT)
(ABOVEOF8-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF1-ROBOT)
(RIGHTOF9-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF7-ROBOT)
(RIGHTOF7-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF6-ROBOT)
(BELOWOF4-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF4-ROBOT)
(BELOWOF3-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF7-ROBOT)
(LEFTOF2-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF11-ROBOT)
(LEFTOF2-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF9-ROBOT)
(BELOWOF4-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF7-ROBOT)
(BELOWOF7-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF5-ROBOT)
(BELOWOF4-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF1-ROBOT)
(RIGHTOF2-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF7-ROBOT)
(ABOVEOF8-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF5-ROBOT)
(RIGHTOF7-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF5-ROBOT)
(ABOVEOF7-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF2-ROBOT)
(RIGHTOF4-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF6-ROBOT)
(BELOWOF3-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF9-ROBOT)
(LEFTOF3-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF5-ROBOT)
(ABOVEOF8-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF11-ROBOT)
(ABOVEOF11-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF9-ROBOT)
(ABOVEOF10-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF6-ROBOT)
(RIGHTOF10-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF6-ROBOT)
(BELOWOF5-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF4-ROBOT)
(ABOVEOF7-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF2-ROBOT)
(ABOVEOF5-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF6-ROBOT)
(ABOVEOF7-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF4-ROBOT)
(RIGHTOF5-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF11-ROBOT)
(LEFTOF11-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF8-ROBOT)
(RIGHTOF10-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF5-ROBOT)
(RIGHTOF5-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF9-ROBOT)
(RIGHTOF10-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF3-ROBOT)
(BELOWOF3-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF8-ROBOT)
(BELOWOF8-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF7-ROBOT)
(ABOVEOF10-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF3-ROBOT)
(ABOVEOF5-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF7-ROBOT)
(RIGHTOF8-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF2-ROBOT)
(BELOWOF2-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF4-ROBOT)
(ABOVEOF4-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF1-ROBOT)
(ABOVEOF3-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF11-ROBOT)
(LEFTOF10-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF1-ROBOT)
(BELOWOF1-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF4-ROBOT)
(RIGHTOF4-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF6-ROBOT)
(LEFTOF6-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF9-ROBOT)
(ABOVEOF9-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF9-ROBOT)
(RIGHTOF9-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF2-ROBOT)
(RIGHTOF3-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(NOT-CHECKCONSISTENCY-)
(not (CHECKCONSISTENCY-))
(not (Flag238prime-))
(not (Flag135prime-))
(not (Flag134prime-))
(not (Flag133prime-))
(not (Flag132prime-))
(not (Flag15prime-))
(not (Flag14prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag241prime-))
(not (Flag121prime-))
(not (Flag119prime-))
(not (Flag118prime-))
(not (Flag116prime-))
(not (Flag115prime-))
(not (Flag114prime-))
(not (Flag113prime-))
(not (Flag111prime-))
(not (Flag110prime-))
(not (Flag109prime-))
(not (Flag108prime-))
(not (Flag107prime-))
(not (Flag105prime-))
(not (Flag104prime-))
(not (Flag103prime-))
(not (Flag102prime-))
(not (Flag101prime-))
(not (Flag100prime-))
(not (Flag98prime-))
(not (Flag97prime-))
(not (Flag96prime-))
(not (Flag95prime-))
(not (Flag94prime-))
(not (Flag93prime-))
(not (Flag92prime-))
(not (Flag91prime-))
(not (Flag89prime-))
(not (Flag88prime-))
(not (Flag87prime-))
(not (Flag86prime-))
(not (Flag85prime-))
(not (Flag84prime-))
(not (Flag83prime-))
(not (Flag82prime-))
(not (Flag80prime-))
(not (Flag79prime-))
(not (Flag78prime-))
(not (Flag77prime-))
(not (Flag76prime-))
(not (Flag75prime-))
(not (Flag74prime-))
(not (Flag73prime-))
(not (Flag71prime-))
(not (Flag70prime-))
(not (Flag69prime-))
(not (Flag68prime-))
(not (Flag67prime-))
(not (Flag66prime-))
(not (Flag65prime-))
(not (Flag64prime-))
(not (Flag63prime-))
(not (Flag62prime-))
(not (Flag61prime-))
(not (Flag59prime-))
(not (Flag58prime-))
(not (Flag57prime-))
(not (Flag56prime-))
(not (Flag55prime-))
(not (Flag54prime-))
(not (Flag53prime-))
(not (Flag52prime-))
(not (Flag51prime-))
(not (Flag50prime-))
(not (Flag49prime-))
(not (Flag47prime-))
(not (Flag46prime-))
(not (Flag45prime-))
(not (Flag44prime-))
(not (Flag43prime-))
(not (Flag42prime-))
(not (Flag41prime-))
(not (Flag40prime-))
(not (Flag39prime-))
(not (Flag38prime-))
(not (Flag37prime-))
(not (Flag36prime-))
(not (Flag35prime-))
(not (Flag34prime-))
(not (Flag33prime-))
(not (Flag32prime-))
(not (Flag31prime-))
(not (Flag30prime-))
(not (Flag29prime-))
(not (Flag28prime-))
(not (Flag27prime-))
(not (Flag26prime-))
(not (Flag25prime-))
(not (Flag24prime-))
(not (Flag23prime-))
(not (Flag22prime-))
(not (Flag21prime-))
(not (Flag20prime-))
(not (Flag19prime-))
(not (Flag18prime-))
(not (Flag17prime-))
(not (Flag16prime-))
(not (Flag13prime-))
(not (Flag12prime-))
(not (Flag11prime-))
(not (Flag10prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag239prime-))
(not (Flag236prime-))
(not (Flag234prime-))
(not (Flag233prime-))
(not (Flag231prime-))
(not (Flag230prime-))
(not (Flag229prime-))
(not (Flag227prime-))
(not (Flag226prime-))
(not (Flag225prime-))
(not (Flag224prime-))
(not (Flag222prime-))
(not (Flag221prime-))
(not (Flag220prime-))
(not (Flag219prime-))
(not (Flag218prime-))
(not (Flag217prime-))
(not (Flag215prime-))
(not (Flag214prime-))
(not (Flag213prime-))
(not (Flag212prime-))
(not (Flag211prime-))
(not (Flag210prime-))
(not (Flag209prime-))
(not (Flag207prime-))
(not (Flag206prime-))
(not (Flag205prime-))
(not (Flag204prime-))
(not (Flag203prime-))
(not (Flag202prime-))
(not (Flag201prime-))
(not (Flag199prime-))
(not (Flag198prime-))
(not (Flag197prime-))
(not (Flag196prime-))
(not (Flag195prime-))
(not (Flag194prime-))
(not (Flag193prime-))
(not (Flag192prime-))
(not (Flag191prime-))
(not (Flag190prime-))
(not (Flag188prime-))
(not (Flag187prime-))
(not (Flag186prime-))
(not (Flag185prime-))
(not (Flag184prime-))
(not (Flag183prime-))
(not (Flag182prime-))
(not (Flag181prime-))
(not (Flag180prime-))
(not (Flag179prime-))
(not (Flag177prime-))
(not (Flag176prime-))
(not (Flag175prime-))
(not (Flag174prime-))
(not (Flag173prime-))
(not (Flag172prime-))
(not (Flag171prime-))
(not (Flag170prime-))
(not (Flag169prime-))
(not (Flag168prime-))
(not (Flag166prime-))
(not (Flag165prime-))
(not (Flag164prime-))
(not (Flag163prime-))
(not (Flag162prime-))
(not (Flag161prime-))
(not (Flag160prime-))
(not (Flag159prime-))
(not (Flag158prime-))
(not (Flag157prime-))
(not (Flag156prime-))
(not (Flag155prime-))
(not (Flag154prime-))
(not (Flag153prime-))
(not (Flag152prime-))
(not (Flag151prime-))
(not (Flag150prime-))
(not (Flag149prime-))
(not (Flag148prime-))
(not (Flag147prime-))
(not (Flag146prime-))
(not (Flag145prime-))
(not (Flag144prime-))
(not (Flag143prime-))
(not (Flag142prime-))
(not (Flag141prime-))
(not (Flag140prime-))
(not (Flag139prime-))
(not (Flag138prime-))
(not (Flag137prime-))
(not (Flag136prime-))
(not (Flag131prime-))
(not (Flag130prime-))
(not (Flag129prime-))
(not (Flag128prime-))
(not (Flag127prime-))
(not (Flag126prime-))
(not (Flag125prime-))
(not (Flag124prime-))
(not (Flag123prime-))
(not (Flag122prime-))
(not (Flag120prime-))
(not (Flag117prime-))
(not (Flag112prime-))
(not (Flag106prime-))
(not (Flag99prime-))
(not (Flag90prime-))
(not (Flag81prime-))
(not (Flag72prime-))
(not (Flag60prime-))
(not (Flag48prime-))
(not (Flag1prime-))
(not (Flag240prime-))
(not (Flag237prime-))
(not (Flag235prime-))
(not (Flag232prime-))
(not (Flag228prime-))
(not (Flag223prime-))
(not (Flag216prime-))
(not (Flag208prime-))
(not (Flag200prime-))
(not (Flag189prime-))
(not (Flag178prime-))
(not (Flag167prime-))
)
)
(:action Flag4Action
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(Flag3-)
(Flag3prime-)
)
:effect
(and
(Flag4-)
(Flag4prime-)
)

)
(:action Prim4Action-2
:parameters ()
:precondition
(and
(not (Flag3-))
(Flag3prime-)
)
:effect
(and
(Flag4prime-)
(not (Flag4-))
)

)
(:action Flag5Action-0
:parameters ()
:precondition
(and
(BELOWOF2-ROBOT)
)
:effect
(and
(Flag5-)
(Flag5prime-)
)

)
(:action Flag5Action-1
:parameters ()
:precondition
(and
(BELOWOF1-ROBOT)
)
:effect
(and
(Flag5-)
(Flag5prime-)
)

)
(:action Prim5Action
:parameters ()
:precondition
(and
(not (BELOWOF2-ROBOT))
(not (BELOWOF1-ROBOT))
)
:effect
(and
(Flag5prime-)
(not (Flag5-))
)

)
(:action Flag6Action-0
:parameters ()
:precondition
(and
(BELOWOF2-ROBOT)
)
:effect
(and
(Flag6-)
(Flag6prime-)
)

)
(:action Flag6Action-1
:parameters ()
:precondition
(and
(BELOWOF1-ROBOT)
)
:effect
(and
(Flag6-)
(Flag6prime-)
)

)
(:action Flag6Action-2
:parameters ()
:precondition
(and
(BELOWOF3-ROBOT)
)
:effect
(and
(Flag6-)
(Flag6prime-)
)

)
(:action Prim6Action
:parameters ()
:precondition
(and
(not (BELOWOF2-ROBOT))
(not (BELOWOF1-ROBOT))
(not (BELOWOF3-ROBOT))
)
:effect
(and
(Flag6prime-)
(not (Flag6-))
)

)
(:action Flag7Action-0
:parameters ()
:precondition
(and
(BELOWOF2-ROBOT)
)
:effect
(and
(Flag7-)
(Flag7prime-)
)

)
(:action Flag7Action-1
:parameters ()
:precondition
(and
(BELOWOF1-ROBOT)
)
:effect
(and
(Flag7-)
(Flag7prime-)
)

)
(:action Flag7Action-2
:parameters ()
:precondition
(and
(BELOWOF3-ROBOT)
)
:effect
(and
(Flag7-)
(Flag7prime-)
)

)
(:action Flag7Action-3
:parameters ()
:precondition
(and
(BELOWOF4-ROBOT)
)
:effect
(and
(Flag7-)
(Flag7prime-)
)

)
(:action Prim7Action
:parameters ()
:precondition
(and
(not (BELOWOF2-ROBOT))
(not (BELOWOF1-ROBOT))
(not (BELOWOF3-ROBOT))
(not (BELOWOF4-ROBOT))
)
:effect
(and
(Flag7prime-)
(not (Flag7-))
)

)
(:action Flag8Action-0
:parameters ()
:precondition
(and
(BELOWOF2-ROBOT)
)
:effect
(and
(Flag8-)
(Flag8prime-)
)

)
(:action Flag8Action-1
:parameters ()
:precondition
(and
(BELOWOF1-ROBOT)
)
:effect
(and
(Flag8-)
(Flag8prime-)
)

)
(:action Flag8Action-2
:parameters ()
:precondition
(and
(BELOWOF3-ROBOT)
)
:effect
(and
(Flag8-)
(Flag8prime-)
)

)
(:action Flag8Action-3
:parameters ()
:precondition
(and
(BELOWOF4-ROBOT)
)
:effect
(and
(Flag8-)
(Flag8prime-)
)

)
(:action Flag8Action-4
:parameters ()
:precondition
(and
(BELOWOF5-ROBOT)
)
:effect
(and
(Flag8-)
(Flag8prime-)
)

)
(:action Prim8Action
:parameters ()
:precondition
(and
(not (BELOWOF2-ROBOT))
(not (BELOWOF1-ROBOT))
(not (BELOWOF3-ROBOT))
(not (BELOWOF4-ROBOT))
(not (BELOWOF5-ROBOT))
)
:effect
(and
(Flag8prime-)
(not (Flag8-))
)

)
(:action Flag9Action-0
:parameters ()
:precondition
(and
(BELOWOF6-ROBOT)
)
:effect
(and
(Flag9-)
(Flag9prime-)
)

)
(:action Flag9Action-1
:parameters ()
:precondition
(and
(BELOWOF5-ROBOT)
)
:effect
(and
(Flag9-)
(Flag9prime-)
)

)
(:action Flag9Action-2
:parameters ()
:precondition
(and
(BELOWOF2-ROBOT)
)
:effect
(and
(Flag9-)
(Flag9prime-)
)

)
(:action Flag9Action-3
:parameters ()
:precondition
(and
(BELOWOF1-ROBOT)
)
:effect
(and
(Flag9-)
(Flag9prime-)
)

)
(:action Flag9Action-4
:parameters ()
:precondition
(and
(BELOWOF4-ROBOT)
)
:effect
(and
(Flag9-)
(Flag9prime-)
)

)
(:action Flag9Action-5
:parameters ()
:precondition
(and
(BELOWOF3-ROBOT)
)
:effect
(and
(Flag9-)
(Flag9prime-)
)

)
(:action Prim9Action
:parameters ()
:precondition
(and
(not (BELOWOF6-ROBOT))
(not (BELOWOF5-ROBOT))
(not (BELOWOF2-ROBOT))
(not (BELOWOF1-ROBOT))
(not (BELOWOF4-ROBOT))
(not (BELOWOF3-ROBOT))
)
:effect
(and
(Flag9prime-)
(not (Flag9-))
)

)
(:action Flag10Action-0
:parameters ()
:precondition
(and
(BELOWOF6-ROBOT)
)
:effect
(and
(Flag10-)
(Flag10prime-)
)

)
(:action Flag10Action-1
:parameters ()
:precondition
(and
(BELOWOF5-ROBOT)
)
:effect
(and
(Flag10-)
(Flag10prime-)
)

)
(:action Flag10Action-2
:parameters ()
:precondition
(and
(BELOWOF2-ROBOT)
)
:effect
(and
(Flag10-)
(Flag10prime-)
)

)
(:action Flag10Action-3
:parameters ()
:precondition
(and
(BELOWOF1-ROBOT)
)
:effect
(and
(Flag10-)
(Flag10prime-)
)

)
(:action Flag10Action-4
:parameters ()
:precondition
(and
(BELOWOF4-ROBOT)
)
:effect
(and
(Flag10-)
(Flag10prime-)
)

)
(:action Flag10Action-5
:parameters ()
:precondition
(and
(BELOWOF7-ROBOT)
)
:effect
(and
(Flag10-)
(Flag10prime-)
)

)
(:action Flag10Action-6
:parameters ()
:precondition
(and
(BELOWOF3-ROBOT)
)
:effect
(and
(Flag10-)
(Flag10prime-)
)

)
(:action Prim10Action
:parameters ()
:precondition
(and
(not (BELOWOF6-ROBOT))
(not (BELOWOF5-ROBOT))
(not (BELOWOF2-ROBOT))
(not (BELOWOF1-ROBOT))
(not (BELOWOF4-ROBOT))
(not (BELOWOF7-ROBOT))
(not (BELOWOF3-ROBOT))
)
:effect
(and
(Flag10prime-)
(not (Flag10-))
)

)
(:action Flag11Action-0
:parameters ()
:precondition
(and
(BELOWOF6-ROBOT)
)
:effect
(and
(Flag11-)
(Flag11prime-)
)

)
(:action Flag11Action-1
:parameters ()
:precondition
(and
(BELOWOF5-ROBOT)
)
:effect
(and
(Flag11-)
(Flag11prime-)
)

)
(:action Flag11Action-2
:parameters ()
:precondition
(and
(BELOWOF2-ROBOT)
)
:effect
(and
(Flag11-)
(Flag11prime-)
)

)
(:action Flag11Action-3
:parameters ()
:precondition
(and
(BELOWOF1-ROBOT)
)
:effect
(and
(Flag11-)
(Flag11prime-)
)

)
(:action Flag11Action-4
:parameters ()
:precondition
(and
(BELOWOF4-ROBOT)
)
:effect
(and
(Flag11-)
(Flag11prime-)
)

)
(:action Flag11Action-5
:parameters ()
:precondition
(and
(BELOWOF7-ROBOT)
)
:effect
(and
(Flag11-)
(Flag11prime-)
)

)
(:action Flag11Action-6
:parameters ()
:precondition
(and
(BELOWOF3-ROBOT)
)
:effect
(and
(Flag11-)
(Flag11prime-)
)

)
(:action Flag11Action-7
:parameters ()
:precondition
(and
(BELOWOF8-ROBOT)
)
:effect
(and
(Flag11-)
(Flag11prime-)
)

)
(:action Prim11Action
:parameters ()
:precondition
(and
(not (BELOWOF6-ROBOT))
(not (BELOWOF5-ROBOT))
(not (BELOWOF2-ROBOT))
(not (BELOWOF1-ROBOT))
(not (BELOWOF4-ROBOT))
(not (BELOWOF7-ROBOT))
(not (BELOWOF3-ROBOT))
(not (BELOWOF8-ROBOT))
)
:effect
(and
(Flag11prime-)
(not (Flag11-))
)

)
(:action Flag12Action-0
:parameters ()
:precondition
(and
(BELOWOF6-ROBOT)
)
:effect
(and
(Flag12-)
(Flag12prime-)
)

)
(:action Flag12Action-1
:parameters ()
:precondition
(and
(BELOWOF5-ROBOT)
)
:effect
(and
(Flag12-)
(Flag12prime-)
)

)
(:action Flag12Action-2
:parameters ()
:precondition
(and
(BELOWOF2-ROBOT)
)
:effect
(and
(Flag12-)
(Flag12prime-)
)

)
(:action Flag12Action-3
:parameters ()
:precondition
(and
(BELOWOF1-ROBOT)
)
:effect
(and
(Flag12-)
(Flag12prime-)
)

)
(:action Flag12Action-4
:parameters ()
:precondition
(and
(BELOWOF4-ROBOT)
)
:effect
(and
(Flag12-)
(Flag12prime-)
)

)
(:action Flag12Action-5
:parameters ()
:precondition
(and
(BELOWOF7-ROBOT)
)
:effect
(and
(Flag12-)
(Flag12prime-)
)

)
(:action Flag12Action-6
:parameters ()
:precondition
(and
(BELOWOF9-ROBOT)
)
:effect
(and
(Flag12-)
(Flag12prime-)
)

)
(:action Flag12Action-7
:parameters ()
:precondition
(and
(BELOWOF3-ROBOT)
)
:effect
(and
(Flag12-)
(Flag12prime-)
)

)
(:action Flag12Action-8
:parameters ()
:precondition
(and
(BELOWOF8-ROBOT)
)
:effect
(and
(Flag12-)
(Flag12prime-)
)

)
(:action Prim12Action
:parameters ()
:precondition
(and
(not (BELOWOF6-ROBOT))
(not (BELOWOF5-ROBOT))
(not (BELOWOF2-ROBOT))
(not (BELOWOF1-ROBOT))
(not (BELOWOF4-ROBOT))
(not (BELOWOF7-ROBOT))
(not (BELOWOF9-ROBOT))
(not (BELOWOF3-ROBOT))
(not (BELOWOF8-ROBOT))
)
:effect
(and
(Flag12prime-)
(not (Flag12-))
)

)
(:action Flag13Action-0
:parameters ()
:precondition
(and
(BELOWOF10-ROBOT)
)
:effect
(and
(Flag13-)
(Flag13prime-)
)

)
(:action Flag13Action-1
:parameters ()
:precondition
(and
(BELOWOF6-ROBOT)
)
:effect
(and
(Flag13-)
(Flag13prime-)
)

)
(:action Flag13Action-2
:parameters ()
:precondition
(and
(BELOWOF5-ROBOT)
)
:effect
(and
(Flag13-)
(Flag13prime-)
)

)
(:action Flag13Action-3
:parameters ()
:precondition
(and
(BELOWOF2-ROBOT)
)
:effect
(and
(Flag13-)
(Flag13prime-)
)

)
(:action Flag13Action-4
:parameters ()
:precondition
(and
(BELOWOF1-ROBOT)
)
:effect
(and
(Flag13-)
(Flag13prime-)
)

)
(:action Flag13Action-5
:parameters ()
:precondition
(and
(BELOWOF4-ROBOT)
)
:effect
(and
(Flag13-)
(Flag13prime-)
)

)
(:action Flag13Action-6
:parameters ()
:precondition
(and
(BELOWOF7-ROBOT)
)
:effect
(and
(Flag13-)
(Flag13prime-)
)

)
(:action Flag13Action-7
:parameters ()
:precondition
(and
(BELOWOF9-ROBOT)
)
:effect
(and
(Flag13-)
(Flag13prime-)
)

)
(:action Flag13Action-8
:parameters ()
:precondition
(and
(BELOWOF3-ROBOT)
)
:effect
(and
(Flag13-)
(Flag13prime-)
)

)
(:action Flag13Action-9
:parameters ()
:precondition
(and
(BELOWOF8-ROBOT)
)
:effect
(and
(Flag13-)
(Flag13prime-)
)

)
(:action Prim13Action
:parameters ()
:precondition
(and
(not (BELOWOF10-ROBOT))
(not (BELOWOF6-ROBOT))
(not (BELOWOF5-ROBOT))
(not (BELOWOF2-ROBOT))
(not (BELOWOF1-ROBOT))
(not (BELOWOF4-ROBOT))
(not (BELOWOF7-ROBOT))
(not (BELOWOF9-ROBOT))
(not (BELOWOF3-ROBOT))
(not (BELOWOF8-ROBOT))
)
:effect
(and
(Flag13prime-)
(not (Flag13-))
)

)
(:action Flag14Action-0
:parameters ()
:precondition
(and
(BELOWOF10-ROBOT)
)
:effect
(and
(Flag14-)
(Flag14prime-)
)

)
(:action Flag14Action-2
:parameters ()
:precondition
(and
(BELOWOF6-ROBOT)
)
:effect
(and
(Flag14-)
(Flag14prime-)
)

)
(:action Flag14Action-3
:parameters ()
:precondition
(and
(BELOWOF5-ROBOT)
)
:effect
(and
(Flag14-)
(Flag14prime-)
)

)
(:action Flag14Action-4
:parameters ()
:precondition
(and
(BELOWOF2-ROBOT)
)
:effect
(and
(Flag14-)
(Flag14prime-)
)

)
(:action Flag14Action-5
:parameters ()
:precondition
(and
(BELOWOF1-ROBOT)
)
:effect
(and
(Flag14-)
(Flag14prime-)
)

)
(:action Flag14Action-6
:parameters ()
:precondition
(and
(BELOWOF4-ROBOT)
)
:effect
(and
(Flag14-)
(Flag14prime-)
)

)
(:action Flag14Action-7
:parameters ()
:precondition
(and
(BELOWOF7-ROBOT)
)
:effect
(and
(Flag14-)
(Flag14prime-)
)

)
(:action Flag14Action-8
:parameters ()
:precondition
(and
(BELOWOF9-ROBOT)
)
:effect
(and
(Flag14-)
(Flag14prime-)
)

)
(:action Flag14Action-9
:parameters ()
:precondition
(and
(BELOWOF3-ROBOT)
)
:effect
(and
(Flag14-)
(Flag14prime-)
)

)
(:action Flag14Action-10
:parameters ()
:precondition
(and
(BELOWOF8-ROBOT)
)
:effect
(and
(Flag14-)
(Flag14prime-)
)

)
(:action Prim14Action
:parameters ()
:precondition
(and
(not (BELOWOF10-ROBOT))
(not (BELOWOF11-ROBOT))
(not (BELOWOF6-ROBOT))
(not (BELOWOF5-ROBOT))
(not (BELOWOF2-ROBOT))
(not (BELOWOF1-ROBOT))
(not (BELOWOF4-ROBOT))
(not (BELOWOF7-ROBOT))
(not (BELOWOF9-ROBOT))
(not (BELOWOF3-ROBOT))
(not (BELOWOF8-ROBOT))
)
:effect
(and
(Flag14prime-)
(not (Flag14-))
)

)
(:action Flag15Action-0
:parameters ()
:precondition
(and
(BELOWOF10-ROBOT)
)
:effect
(and
(Flag15-)
(Flag15prime-)
)

)
(:action Flag15Action-2
:parameters ()
:precondition
(and
(BELOWOF6-ROBOT)
)
:effect
(and
(Flag15-)
(Flag15prime-)
)

)
(:action Flag15Action-3
:parameters ()
:precondition
(and
(BELOWOF5-ROBOT)
)
:effect
(and
(Flag15-)
(Flag15prime-)
)

)
(:action Flag15Action-4
:parameters ()
:precondition
(and
(BELOWOF2-ROBOT)
)
:effect
(and
(Flag15-)
(Flag15prime-)
)

)
(:action Flag15Action-5
:parameters ()
:precondition
(and
(BELOWOF1-ROBOT)
)
:effect
(and
(Flag15-)
(Flag15prime-)
)

)
(:action Flag15Action-6
:parameters ()
:precondition
(and
(BELOWOF4-ROBOT)
)
:effect
(and
(Flag15-)
(Flag15prime-)
)

)
(:action Flag15Action-7
:parameters ()
:precondition
(and
(BELOWOF12-ROBOT)
)
:effect
(and
(Flag15-)
(Flag15prime-)
)

)
(:action Flag15Action-8
:parameters ()
:precondition
(and
(BELOWOF7-ROBOT)
)
:effect
(and
(Flag15-)
(Flag15prime-)
)

)
(:action Flag15Action-9
:parameters ()
:precondition
(and
(BELOWOF9-ROBOT)
)
:effect
(and
(Flag15-)
(Flag15prime-)
)

)
(:action Flag15Action-10
:parameters ()
:precondition
(and
(BELOWOF3-ROBOT)
)
:effect
(and
(Flag15-)
(Flag15prime-)
)

)
(:action Flag15Action-11
:parameters ()
:precondition
(and
(BELOWOF8-ROBOT)
)
:effect
(and
(Flag15-)
(Flag15prime-)
)

)
(:action Prim15Action
:parameters ()
:precondition
(and
(not (BELOWOF10-ROBOT))
(not (BELOWOF11-ROBOT))
(not (BELOWOF6-ROBOT))
(not (BELOWOF5-ROBOT))
(not (BELOWOF2-ROBOT))
(not (BELOWOF1-ROBOT))
(not (BELOWOF4-ROBOT))
(not (BELOWOF12-ROBOT))
(not (BELOWOF7-ROBOT))
(not (BELOWOF9-ROBOT))
(not (BELOWOF3-ROBOT))
(not (BELOWOF8-ROBOT))
)
:effect
(and
(Flag15prime-)
(not (Flag15-))
)

)
(:action Flag16Action-0
:parameters ()
:precondition
(and
(ABOVEOF6-ROBOT)
)
:effect
(and
(Flag16-)
(Flag16prime-)
)

)
(:action Flag16Action-1
:parameters ()
:precondition
(and
(ABOVEOF11-ROBOT)
)
:effect
(and
(Flag16-)
(Flag16prime-)
)

)
(:action Flag16Action-2
:parameters ()
:precondition
(and
(ABOVEOF10-ROBOT)
)
:effect
(and
(Flag16-)
(Flag16prime-)
)

)
(:action Flag16Action-3
:parameters ()
:precondition
(and
(ABOVEOF2-ROBOT)
)
:effect
(and
(Flag16-)
(Flag16prime-)
)

)
(:action Flag16Action-4
:parameters ()
:precondition
(and
(ABOVEOF7-ROBOT)
)
:effect
(and
(Flag16-)
(Flag16prime-)
)

)
(:action Flag16Action-5
:parameters ()
:precondition
(and
(ABOVEOF8-ROBOT)
)
:effect
(and
(Flag16-)
(Flag16prime-)
)

)
(:action Flag16Action-6
:parameters ()
:precondition
(and
(ABOVEOF9-ROBOT)
)
:effect
(and
(Flag16-)
(Flag16prime-)
)

)
(:action Flag16Action-7
:parameters ()
:precondition
(and
(ABOVEOF4-ROBOT)
)
:effect
(and
(Flag16-)
(Flag16prime-)
)

)
(:action Flag16Action-8
:parameters ()
:precondition
(and
(ABOVEOF3-ROBOT)
)
:effect
(and
(Flag16-)
(Flag16prime-)
)

)
(:action Flag16Action-9
:parameters ()
:precondition
(and
(ABOVEOF5-ROBOT)
)
:effect
(and
(Flag16-)
(Flag16prime-)
)

)
(:action Flag16Action-10
:parameters ()
:precondition
(and
(ABOVEOF1-ROBOT)
)
:effect
(and
(Flag16-)
(Flag16prime-)
)

)
(:action Prim16Action
:parameters ()
:precondition
(and
(not (ABOVEOF6-ROBOT))
(not (ABOVEOF11-ROBOT))
(not (ABOVEOF10-ROBOT))
(not (ABOVEOF2-ROBOT))
(not (ABOVEOF7-ROBOT))
(not (ABOVEOF8-ROBOT))
(not (ABOVEOF9-ROBOT))
(not (ABOVEOF4-ROBOT))
(not (ABOVEOF3-ROBOT))
(not (ABOVEOF5-ROBOT))
(not (ABOVEOF1-ROBOT))
)
:effect
(and
(Flag16prime-)
(not (Flag16-))
)

)
(:action Flag17Action-0
:parameters ()
:precondition
(and
(ABOVEOF11-ROBOT)
)
:effect
(and
(Flag17-)
(Flag17prime-)
)

)
(:action Flag17Action-1
:parameters ()
:precondition
(and
(ABOVEOF6-ROBOT)
)
:effect
(and
(Flag17-)
(Flag17prime-)
)

)
(:action Flag17Action-2
:parameters ()
:precondition
(and
(ABOVEOF10-ROBOT)
)
:effect
(and
(Flag17-)
(Flag17prime-)
)

)
(:action Flag17Action-3
:parameters ()
:precondition
(and
(ABOVEOF2-ROBOT)
)
:effect
(and
(Flag17-)
(Flag17prime-)
)

)
(:action Flag17Action-4
:parameters ()
:precondition
(and
(ABOVEOF7-ROBOT)
)
:effect
(and
(Flag17-)
(Flag17prime-)
)

)
(:action Flag17Action-5
:parameters ()
:precondition
(and
(ABOVEOF8-ROBOT)
)
:effect
(and
(Flag17-)
(Flag17prime-)
)

)
(:action Flag17Action-6
:parameters ()
:precondition
(and
(ABOVEOF9-ROBOT)
)
:effect
(and
(Flag17-)
(Flag17prime-)
)

)
(:action Flag17Action-7
:parameters ()
:precondition
(and
(ABOVEOF4-ROBOT)
)
:effect
(and
(Flag17-)
(Flag17prime-)
)

)
(:action Flag17Action-8
:parameters ()
:precondition
(and
(ABOVEOF3-ROBOT)
)
:effect
(and
(Flag17-)
(Flag17prime-)
)

)
(:action Flag17Action-9
:parameters ()
:precondition
(and
(ABOVEOF5-ROBOT)
)
:effect
(and
(Flag17-)
(Flag17prime-)
)

)
(:action Prim17Action
:parameters ()
:precondition
(and
(not (ABOVEOF11-ROBOT))
(not (ABOVEOF6-ROBOT))
(not (ABOVEOF10-ROBOT))
(not (ABOVEOF2-ROBOT))
(not (ABOVEOF7-ROBOT))
(not (ABOVEOF8-ROBOT))
(not (ABOVEOF9-ROBOT))
(not (ABOVEOF4-ROBOT))
(not (ABOVEOF3-ROBOT))
(not (ABOVEOF5-ROBOT))
)
:effect
(and
(Flag17prime-)
(not (Flag17-))
)

)
(:action Flag18Action-0
:parameters ()
:precondition
(and
(ABOVEOF11-ROBOT)
)
:effect
(and
(Flag18-)
(Flag18prime-)
)

)
(:action Flag18Action-1
:parameters ()
:precondition
(and
(ABOVEOF6-ROBOT)
)
:effect
(and
(Flag18-)
(Flag18prime-)
)

)
(:action Flag18Action-2
:parameters ()
:precondition
(and
(ABOVEOF10-ROBOT)
)
:effect
(and
(Flag18-)
(Flag18prime-)
)

)
(:action Flag18Action-3
:parameters ()
:precondition
(and
(ABOVEOF8-ROBOT)
)
:effect
(and
(Flag18-)
(Flag18prime-)
)

)
(:action Flag18Action-4
:parameters ()
:precondition
(and
(ABOVEOF7-ROBOT)
)
:effect
(and
(Flag18-)
(Flag18prime-)
)

)
(:action Flag18Action-5
:parameters ()
:precondition
(and
(ABOVEOF9-ROBOT)
)
:effect
(and
(Flag18-)
(Flag18prime-)
)

)
(:action Flag18Action-6
:parameters ()
:precondition
(and
(ABOVEOF4-ROBOT)
)
:effect
(and
(Flag18-)
(Flag18prime-)
)

)
(:action Flag18Action-7
:parameters ()
:precondition
(and
(ABOVEOF3-ROBOT)
)
:effect
(and
(Flag18-)
(Flag18prime-)
)

)
(:action Flag18Action-8
:parameters ()
:precondition
(and
(ABOVEOF5-ROBOT)
)
:effect
(and
(Flag18-)
(Flag18prime-)
)

)
(:action Prim18Action
:parameters ()
:precondition
(and
(not (ABOVEOF11-ROBOT))
(not (ABOVEOF6-ROBOT))
(not (ABOVEOF10-ROBOT))
(not (ABOVEOF8-ROBOT))
(not (ABOVEOF7-ROBOT))
(not (ABOVEOF9-ROBOT))
(not (ABOVEOF4-ROBOT))
(not (ABOVEOF3-ROBOT))
(not (ABOVEOF5-ROBOT))
)
:effect
(and
(Flag18prime-)
(not (Flag18-))
)

)
(:action Flag19Action-0
:parameters ()
:precondition
(and
(ABOVEOF11-ROBOT)
)
:effect
(and
(Flag19-)
(Flag19prime-)
)

)
(:action Flag19Action-1
:parameters ()
:precondition
(and
(ABOVEOF6-ROBOT)
)
:effect
(and
(Flag19-)
(Flag19prime-)
)

)
(:action Flag19Action-2
:parameters ()
:precondition
(and
(ABOVEOF10-ROBOT)
)
:effect
(and
(Flag19-)
(Flag19prime-)
)

)
(:action Flag19Action-3
:parameters ()
:precondition
(and
(ABOVEOF8-ROBOT)
)
:effect
(and
(Flag19-)
(Flag19prime-)
)

)
(:action Flag19Action-4
:parameters ()
:precondition
(and
(ABOVEOF7-ROBOT)
)
:effect
(and
(Flag19-)
(Flag19prime-)
)

)
(:action Flag19Action-5
:parameters ()
:precondition
(and
(ABOVEOF9-ROBOT)
)
:effect
(and
(Flag19-)
(Flag19prime-)
)

)
(:action Flag19Action-6
:parameters ()
:precondition
(and
(ABOVEOF4-ROBOT)
)
:effect
(and
(Flag19-)
(Flag19prime-)
)

)
(:action Flag19Action-7
:parameters ()
:precondition
(and
(ABOVEOF5-ROBOT)
)
:effect
(and
(Flag19-)
(Flag19prime-)
)

)
(:action Prim19Action
:parameters ()
:precondition
(and
(not (ABOVEOF11-ROBOT))
(not (ABOVEOF6-ROBOT))
(not (ABOVEOF10-ROBOT))
(not (ABOVEOF8-ROBOT))
(not (ABOVEOF7-ROBOT))
(not (ABOVEOF9-ROBOT))
(not (ABOVEOF4-ROBOT))
(not (ABOVEOF5-ROBOT))
)
:effect
(and
(Flag19prime-)
(not (Flag19-))
)

)
(:action Flag20Action-0
:parameters ()
:precondition
(and
(ABOVEOF11-ROBOT)
)
:effect
(and
(Flag20-)
(Flag20prime-)
)

)
(:action Flag20Action-1
:parameters ()
:precondition
(and
(ABOVEOF6-ROBOT)
)
:effect
(and
(Flag20-)
(Flag20prime-)
)

)
(:action Flag20Action-2
:parameters ()
:precondition
(and
(ABOVEOF7-ROBOT)
)
:effect
(and
(Flag20-)
(Flag20prime-)
)

)
(:action Flag20Action-3
:parameters ()
:precondition
(and
(ABOVEOF8-ROBOT)
)
:effect
(and
(Flag20-)
(Flag20prime-)
)

)
(:action Flag20Action-4
:parameters ()
:precondition
(and
(ABOVEOF10-ROBOT)
)
:effect
(and
(Flag20-)
(Flag20prime-)
)

)
(:action Flag20Action-5
:parameters ()
:precondition
(and
(ABOVEOF9-ROBOT)
)
:effect
(and
(Flag20-)
(Flag20prime-)
)

)
(:action Flag20Action-6
:parameters ()
:precondition
(and
(ABOVEOF5-ROBOT)
)
:effect
(and
(Flag20-)
(Flag20prime-)
)

)
(:action Prim20Action
:parameters ()
:precondition
(and
(not (ABOVEOF11-ROBOT))
(not (ABOVEOF6-ROBOT))
(not (ABOVEOF7-ROBOT))
(not (ABOVEOF8-ROBOT))
(not (ABOVEOF10-ROBOT))
(not (ABOVEOF9-ROBOT))
(not (ABOVEOF5-ROBOT))
)
:effect
(and
(Flag20prime-)
(not (Flag20-))
)

)
(:action Flag21Action-0
:parameters ()
:precondition
(and
(ABOVEOF11-ROBOT)
)
:effect
(and
(Flag21-)
(Flag21prime-)
)

)
(:action Flag21Action-1
:parameters ()
:precondition
(and
(ABOVEOF6-ROBOT)
)
:effect
(and
(Flag21-)
(Flag21prime-)
)

)
(:action Flag21Action-2
:parameters ()
:precondition
(and
(ABOVEOF7-ROBOT)
)
:effect
(and
(Flag21-)
(Flag21prime-)
)

)
(:action Flag21Action-3
:parameters ()
:precondition
(and
(ABOVEOF8-ROBOT)
)
:effect
(and
(Flag21-)
(Flag21prime-)
)

)
(:action Flag21Action-4
:parameters ()
:precondition
(and
(ABOVEOF10-ROBOT)
)
:effect
(and
(Flag21-)
(Flag21prime-)
)

)
(:action Flag21Action-5
:parameters ()
:precondition
(and
(ABOVEOF9-ROBOT)
)
:effect
(and
(Flag21-)
(Flag21prime-)
)

)
(:action Prim21Action
:parameters ()
:precondition
(and
(not (ABOVEOF11-ROBOT))
(not (ABOVEOF6-ROBOT))
(not (ABOVEOF7-ROBOT))
(not (ABOVEOF8-ROBOT))
(not (ABOVEOF10-ROBOT))
(not (ABOVEOF9-ROBOT))
)
:effect
(and
(Flag21prime-)
(not (Flag21-))
)

)
(:action Flag22Action-0
:parameters ()
:precondition
(and
(ABOVEOF8-ROBOT)
)
:effect
(and
(Flag22-)
(Flag22prime-)
)

)
(:action Flag22Action-1
:parameters ()
:precondition
(and
(ABOVEOF11-ROBOT)
)
:effect
(and
(Flag22-)
(Flag22prime-)
)

)
(:action Flag22Action-2
:parameters ()
:precondition
(and
(ABOVEOF7-ROBOT)
)
:effect
(and
(Flag22-)
(Flag22prime-)
)

)
(:action Flag22Action-3
:parameters ()
:precondition
(and
(ABOVEOF9-ROBOT)
)
:effect
(and
(Flag22-)
(Flag22prime-)
)

)
(:action Flag22Action-4
:parameters ()
:precondition
(and
(ABOVEOF10-ROBOT)
)
:effect
(and
(Flag22-)
(Flag22prime-)
)

)
(:action Prim22Action
:parameters ()
:precondition
(and
(not (ABOVEOF8-ROBOT))
(not (ABOVEOF11-ROBOT))
(not (ABOVEOF7-ROBOT))
(not (ABOVEOF9-ROBOT))
(not (ABOVEOF10-ROBOT))
)
:effect
(and
(Flag22prime-)
(not (Flag22-))
)

)
(:action Flag23Action-0
:parameters ()
:precondition
(and
(ABOVEOF8-ROBOT)
)
:effect
(and
(Flag23-)
(Flag23prime-)
)

)
(:action Flag23Action-1
:parameters ()
:precondition
(and
(ABOVEOF11-ROBOT)
)
:effect
(and
(Flag23-)
(Flag23prime-)
)

)
(:action Flag23Action-2
:parameters ()
:precondition
(and
(ABOVEOF10-ROBOT)
)
:effect
(and
(Flag23-)
(Flag23prime-)
)

)
(:action Flag23Action-3
:parameters ()
:precondition
(and
(ABOVEOF9-ROBOT)
)
:effect
(and
(Flag23-)
(Flag23prime-)
)

)
(:action Prim23Action
:parameters ()
:precondition
(and
(not (ABOVEOF8-ROBOT))
(not (ABOVEOF11-ROBOT))
(not (ABOVEOF10-ROBOT))
(not (ABOVEOF9-ROBOT))
)
:effect
(and
(Flag23prime-)
(not (Flag23-))
)

)
(:action Flag24Action-0
:parameters ()
:precondition
(and
(ABOVEOF11-ROBOT)
)
:effect
(and
(Flag24-)
(Flag24prime-)
)

)
(:action Flag24Action-1
:parameters ()
:precondition
(and
(ABOVEOF10-ROBOT)
)
:effect
(and
(Flag24-)
(Flag24prime-)
)

)
(:action Flag24Action-2
:parameters ()
:precondition
(and
(ABOVEOF9-ROBOT)
)
:effect
(and
(Flag24-)
(Flag24prime-)
)

)
(:action Prim24Action
:parameters ()
:precondition
(and
(not (ABOVEOF11-ROBOT))
(not (ABOVEOF10-ROBOT))
(not (ABOVEOF9-ROBOT))
)
:effect
(and
(Flag24prime-)
(not (Flag24-))
)

)
(:action Flag25Action-0
:parameters ()
:precondition
(and
(ABOVEOF10-ROBOT)
)
:effect
(and
(Flag25-)
(Flag25prime-)
)

)
(:action Flag25Action-1
:parameters ()
:precondition
(and
(ABOVEOF11-ROBOT)
)
:effect
(and
(Flag25-)
(Flag25prime-)
)

)
(:action Prim25Action
:parameters ()
:precondition
(and
(not (ABOVEOF10-ROBOT))
(not (ABOVEOF11-ROBOT))
)
:effect
(and
(Flag25prime-)
(not (Flag25-))
)

)
(:action Flag26Action
:parameters ()
:precondition
(and
(ABOVEOF2-ROBOT)
(BELOWOF2-ROBOT)
)
:effect
(and
(Flag26-)
(Flag26prime-)
)

)
(:action Flag27Action
:parameters ()
:precondition
(and
(ABOVEOF11-ROBOT)
(BELOWOF1-ROBOT)
)
:effect
(and
(Flag27-)
(Flag27prime-)
)

)
(:action Flag28Action
:parameters ()
:precondition
(and
(ABOVEOF1-ROBOT)
(BELOWOF2-ROBOT)
)
:effect
(and
(Flag28-)
(Flag28prime-)
)

)
(:action Flag29Action
:parameters ()
:precondition
(and
(ABOVEOF7-ROBOT)
(BELOWOF2-ROBOT)
)
:effect
(and
(Flag29-)
(Flag29prime-)
)

)
(:action Flag30Action
:parameters ()
:precondition
(and
(BELOWOF1-ROBOT)
(ABOVEOF7-ROBOT)
)
:effect
(and
(Flag30-)
(Flag30prime-)
)

)
(:action Flag31Action
:parameters ()
:precondition
(and
(BELOWOF1-ROBOT)
(ABOVEOF1-ROBOT)
)
:effect
(and
(Flag31-)
(Flag31prime-)
)

)
(:action Flag32Action
:parameters ()
:precondition
(and
(BELOWOF1-ROBOT)
(ABOVEOF5-ROBOT)
)
:effect
(and
(Flag32-)
(Flag32prime-)
)

)
(:action Flag33Action
:parameters ()
:precondition
(and
(ABOVEOF4-ROBOT)
(BELOWOF2-ROBOT)
)
:effect
(and
(Flag33-)
(Flag33prime-)
)

)
(:action Flag34Action
:parameters ()
:precondition
(and
(ABOVEOF6-ROBOT)
(BELOWOF2-ROBOT)
)
:effect
(and
(Flag34-)
(Flag34prime-)
)

)
(:action Flag35Action
:parameters ()
:precondition
(and
(BELOWOF1-ROBOT)
(ABOVEOF2-ROBOT)
)
:effect
(and
(Flag35-)
(Flag35prime-)
)

)
(:action Flag36Action
:parameters ()
:precondition
(and
(ABOVEOF9-ROBOT)
(BELOWOF2-ROBOT)
)
:effect
(and
(Flag36-)
(Flag36prime-)
)

)
(:action Flag37Action
:parameters ()
:precondition
(and
(ABOVEOF6-ROBOT)
(BELOWOF1-ROBOT)
)
:effect
(and
(Flag37-)
(Flag37prime-)
)

)
(:action Flag38Action
:parameters ()
:precondition
(and
(ABOVEOF10-ROBOT)
(BELOWOF1-ROBOT)
)
:effect
(and
(Flag38-)
(Flag38prime-)
)

)
(:action Flag39Action
:parameters ()
:precondition
(and
(ABOVEOF11-ROBOT)
(BELOWOF2-ROBOT)
)
:effect
(and
(Flag39-)
(Flag39prime-)
)

)
(:action Flag40Action
:parameters ()
:precondition
(and
(ABOVEOF3-ROBOT)
(BELOWOF1-ROBOT)
)
:effect
(and
(Flag40-)
(Flag40prime-)
)

)
(:action Flag41Action
:parameters ()
:precondition
(and
(BELOWOF1-ROBOT)
(ABOVEOF9-ROBOT)
)
:effect
(and
(Flag41-)
(Flag41prime-)
)

)
(:action Flag42Action
:parameters ()
:precondition
(and
(ABOVEOF10-ROBOT)
(BELOWOF2-ROBOT)
)
:effect
(and
(Flag42-)
(Flag42prime-)
)

)
(:action Flag43Action
:parameters ()
:precondition
(and
(ABOVEOF8-ROBOT)
(BELOWOF2-ROBOT)
)
:effect
(and
(Flag43-)
(Flag43prime-)
)

)
(:action Flag44Action
:parameters ()
:precondition
(and
(BELOWOF1-ROBOT)
(ABOVEOF4-ROBOT)
)
:effect
(and
(Flag44-)
(Flag44prime-)
)

)
(:action Flag45Action
:parameters ()
:precondition
(and
(BELOWOF2-ROBOT)
(ABOVEOF5-ROBOT)
)
:effect
(and
(Flag45-)
(Flag45prime-)
)

)
(:action Flag46Action
:parameters ()
:precondition
(and
(ABOVEOF8-ROBOT)
(BELOWOF1-ROBOT)
)
:effect
(and
(Flag46-)
(Flag46prime-)
)

)
(:action Flag47Action
:parameters ()
:precondition
(and
(ABOVEOF3-ROBOT)
(BELOWOF2-ROBOT)
)
:effect
(and
(Flag47-)
(Flag47prime-)
)

)
(:action Prim26Action-0
:parameters ()
:precondition
(and
(not (ABOVEOF2-ROBOT))
)
:effect
(and
(Flag26prime-)
(not (Flag26-))
)

)
(:action Prim26Action-1
:parameters ()
:precondition
(and
(not (BELOWOF2-ROBOT))
)
:effect
(and
(Flag26prime-)
(not (Flag26-))
)

)
(:action Prim27Action-0
:parameters ()
:precondition
(and
(not (ABOVEOF11-ROBOT))
)
:effect
(and
(Flag27prime-)
(not (Flag27-))
)

)
(:action Prim27Action-1
:parameters ()
:precondition
(and
(not (BELOWOF1-ROBOT))
)
:effect
(and
(Flag27prime-)
(not (Flag27-))
)

)
(:action Prim28Action-0
:parameters ()
:precondition
(and
(not (ABOVEOF1-ROBOT))
)
:effect
(and
(Flag28prime-)
(not (Flag28-))
)

)
(:action Prim28Action-1
:parameters ()
:precondition
(and
(not (BELOWOF2-ROBOT))
)
:effect
(and
(Flag28prime-)
(not (Flag28-))
)

)
(:action Prim29Action-0
:parameters ()
:precondition
(and
(not (ABOVEOF7-ROBOT))
)
:effect
(and
(Flag29prime-)
(not (Flag29-))
)

)
(:action Prim29Action-1
:parameters ()
:precondition
(and
(not (BELOWOF2-ROBOT))
)
:effect
(and
(Flag29prime-)
(not (Flag29-))
)

)
(:action Prim30Action-0
:parameters ()
:precondition
(and
(not (BELOWOF1-ROBOT))
)
:effect
(and
(Flag30prime-)
(not (Flag30-))
)

)
(:action Prim30Action-1
:parameters ()
:precondition
(and
(not (ABOVEOF7-ROBOT))
)
:effect
(and
(Flag30prime-)
(not (Flag30-))
)

)
(:action Prim31Action-0
:parameters ()
:precondition
(and
(not (BELOWOF1-ROBOT))
)
:effect
(and
(Flag31prime-)
(not (Flag31-))
)

)
(:action Prim31Action-1
:parameters ()
:precondition
(and
(not (ABOVEOF1-ROBOT))
)
:effect
(and
(Flag31prime-)
(not (Flag31-))
)

)
(:action Prim32Action-0
:parameters ()
:precondition
(and
(not (BELOWOF1-ROBOT))
)
:effect
(and
(Flag32prime-)
(not (Flag32-))
)

)
(:action Prim32Action-1
:parameters ()
:precondition
(and
(not (ABOVEOF5-ROBOT))
)
:effect
(and
(Flag32prime-)
(not (Flag32-))
)

)
(:action Prim33Action-0
:parameters ()
:precondition
(and
(not (ABOVEOF4-ROBOT))
)
:effect
(and
(Flag33prime-)
(not (Flag33-))
)

)
(:action Prim33Action-1
:parameters ()
:precondition
(and
(not (BELOWOF2-ROBOT))
)
:effect
(and
(Flag33prime-)
(not (Flag33-))
)

)
(:action Prim34Action-0
:parameters ()
:precondition
(and
(not (ABOVEOF6-ROBOT))
)
:effect
(and
(Flag34prime-)
(not (Flag34-))
)

)
(:action Prim34Action-1
:parameters ()
:precondition
(and
(not (BELOWOF2-ROBOT))
)
:effect
(and
(Flag34prime-)
(not (Flag34-))
)

)
(:action Prim35Action-0
:parameters ()
:precondition
(and
(not (BELOWOF1-ROBOT))
)
:effect
(and
(Flag35prime-)
(not (Flag35-))
)

)
(:action Prim35Action-1
:parameters ()
:precondition
(and
(not (ABOVEOF2-ROBOT))
)
:effect
(and
(Flag35prime-)
(not (Flag35-))
)

)
(:action Prim36Action-0
:parameters ()
:precondition
(and
(not (ABOVEOF9-ROBOT))
)
:effect
(and
(Flag36prime-)
(not (Flag36-))
)

)
(:action Prim36Action-1
:parameters ()
:precondition
(and
(not (BELOWOF2-ROBOT))
)
:effect
(and
(Flag36prime-)
(not (Flag36-))
)

)
(:action Prim37Action-0
:parameters ()
:precondition
(and
(not (ABOVEOF6-ROBOT))
)
:effect
(and
(Flag37prime-)
(not (Flag37-))
)

)
(:action Prim37Action-1
:parameters ()
:precondition
(and
(not (BELOWOF1-ROBOT))
)
:effect
(and
(Flag37prime-)
(not (Flag37-))
)

)
(:action Prim38Action-0
:parameters ()
:precondition
(and
(not (ABOVEOF10-ROBOT))
)
:effect
(and
(Flag38prime-)
(not (Flag38-))
)

)
(:action Prim38Action-1
:parameters ()
:precondition
(and
(not (BELOWOF1-ROBOT))
)
:effect
(and
(Flag38prime-)
(not (Flag38-))
)

)
(:action Prim39Action-0
:parameters ()
:precondition
(and
(not (ABOVEOF11-ROBOT))
)
:effect
(and
(Flag39prime-)
(not (Flag39-))
)

)
(:action Prim39Action-1
:parameters ()
:precondition
(and
(not (BELOWOF2-ROBOT))
)
:effect
(and
(Flag39prime-)
(not (Flag39-))
)

)
(:action Prim40Action-0
:parameters ()
:precondition
(and
(not (ABOVEOF3-ROBOT))
)
:effect
(and
(Flag40prime-)
(not (Flag40-))
)

)
(:action Prim40Action-1
:parameters ()
:precondition
(and
(not (BELOWOF1-ROBOT))
)
:effect
(and
(Flag40prime-)
(not (Flag40-))
)

)
(:action Prim41Action-0
:parameters ()
:precondition
(and
(not (BELOWOF1-ROBOT))
)
:effect
(and
(Flag41prime-)
(not (Flag41-))
)

)
(:action Prim41Action-1
:parameters ()
:precondition
(and
(not (ABOVEOF9-ROBOT))
)
:effect
(and
(Flag41prime-)
(not (Flag41-))
)

)
(:action Prim42Action-0
:parameters ()
:precondition
(and
(not (ABOVEOF10-ROBOT))
)
:effect
(and
(Flag42prime-)
(not (Flag42-))
)

)
(:action Prim42Action-1
:parameters ()
:precondition
(and
(not (BELOWOF2-ROBOT))
)
:effect
(and
(Flag42prime-)
(not (Flag42-))
)

)
(:action Prim43Action-0
:parameters ()
:precondition
(and
(not (ABOVEOF8-ROBOT))
)
:effect
(and
(Flag43prime-)
(not (Flag43-))
)

)
(:action Prim43Action-1
:parameters ()
:precondition
(and
(not (BELOWOF2-ROBOT))
)
:effect
(and
(Flag43prime-)
(not (Flag43-))
)

)
(:action Prim44Action-0
:parameters ()
:precondition
(and
(not (BELOWOF1-ROBOT))
)
:effect
(and
(Flag44prime-)
(not (Flag44-))
)

)
(:action Prim44Action-1
:parameters ()
:precondition
(and
(not (ABOVEOF4-ROBOT))
)
:effect
(and
(Flag44prime-)
(not (Flag44-))
)

)
(:action Prim45Action-0
:parameters ()
:precondition
(and
(not (BELOWOF2-ROBOT))
)
:effect
(and
(Flag45prime-)
(not (Flag45-))
)

)
(:action Prim45Action-1
:parameters ()
:precondition
(and
(not (ABOVEOF5-ROBOT))
)
:effect
(and
(Flag45prime-)
(not (Flag45-))
)

)
(:action Prim46Action-0
:parameters ()
:precondition
(and
(not (ABOVEOF8-ROBOT))
)
:effect
(and
(Flag46prime-)
(not (Flag46-))
)

)
(:action Prim46Action-1
:parameters ()
:precondition
(and
(not (BELOWOF1-ROBOT))
)
:effect
(and
(Flag46prime-)
(not (Flag46-))
)

)
(:action Prim47Action-0
:parameters ()
:precondition
(and
(not (ABOVEOF3-ROBOT))
)
:effect
(and
(Flag47prime-)
(not (Flag47-))
)

)
(:action Prim47Action-1
:parameters ()
:precondition
(and
(not (BELOWOF2-ROBOT))
)
:effect
(and
(Flag47prime-)
(not (Flag47-))
)

)
(:action Flag49Action
:parameters ()
:precondition
(and
(BELOWOF3-ROBOT)
(ABOVEOF5-ROBOT)
)
:effect
(and
(Flag49-)
(Flag49prime-)
)

)
(:action Flag50Action
:parameters ()
:precondition
(and
(BELOWOF3-ROBOT)
(ABOVEOF6-ROBOT)
)
:effect
(and
(Flag50-)
(Flag50prime-)
)

)
(:action Flag51Action
:parameters ()
:precondition
(and
(BELOWOF3-ROBOT)
(ABOVEOF7-ROBOT)
)
:effect
(and
(Flag51-)
(Flag51prime-)
)

)
(:action Flag52Action
:parameters ()
:precondition
(and
(BELOWOF3-ROBOT)
(ABOVEOF9-ROBOT)
)
:effect
(and
(Flag52-)
(Flag52prime-)
)

)
(:action Flag53Action
:parameters ()
:precondition
(and
(BELOWOF3-ROBOT)
(ABOVEOF2-ROBOT)
)
:effect
(and
(Flag53-)
(Flag53prime-)
)

)
(:action Flag54Action
:parameters ()
:precondition
(and
(ABOVEOF3-ROBOT)
(BELOWOF3-ROBOT)
)
:effect
(and
(Flag54-)
(Flag54prime-)
)

)
(:action Flag55Action
:parameters ()
:precondition
(and
(ABOVEOF8-ROBOT)
(BELOWOF3-ROBOT)
)
:effect
(and
(Flag55-)
(Flag55prime-)
)

)
(:action Flag56Action
:parameters ()
:precondition
(and
(BELOWOF3-ROBOT)
(ABOVEOF4-ROBOT)
)
:effect
(and
(Flag56-)
(Flag56prime-)
)

)
(:action Flag57Action
:parameters ()
:precondition
(and
(BELOWOF3-ROBOT)
(ABOVEOF10-ROBOT)
)
:effect
(and
(Flag57-)
(Flag57prime-)
)

)
(:action Flag58Action
:parameters ()
:precondition
(and
(BELOWOF2-ROBOT)
(ABOVEOF2-ROBOT)
)
:effect
(and
(Flag58-)
(Flag58prime-)
)

)
(:action Flag59Action
:parameters ()
:precondition
(and
(ABOVEOF11-ROBOT)
(BELOWOF3-ROBOT)
)
:effect
(and
(Flag59-)
(Flag59prime-)
)

)
(:action Prim49Action-0
:parameters ()
:precondition
(and
(not (BELOWOF3-ROBOT))
)
:effect
(and
(Flag49prime-)
(not (Flag49-))
)

)
(:action Prim49Action-1
:parameters ()
:precondition
(and
(not (ABOVEOF5-ROBOT))
)
:effect
(and
(Flag49prime-)
(not (Flag49-))
)

)
(:action Prim50Action-0
:parameters ()
:precondition
(and
(not (BELOWOF3-ROBOT))
)
:effect
(and
(Flag50prime-)
(not (Flag50-))
)

)
(:action Prim50Action-1
:parameters ()
:precondition
(and
(not (ABOVEOF6-ROBOT))
)
:effect
(and
(Flag50prime-)
(not (Flag50-))
)

)
(:action Prim51Action-0
:parameters ()
:precondition
(and
(not (BELOWOF3-ROBOT))
)
:effect
(and
(Flag51prime-)
(not (Flag51-))
)

)
(:action Prim51Action-1
:parameters ()
:precondition
(and
(not (ABOVEOF7-ROBOT))
)
:effect
(and
(Flag51prime-)
(not (Flag51-))
)

)
(:action Prim52Action-0
:parameters ()
:precondition
(and
(not (BELOWOF3-ROBOT))
)
:effect
(and
(Flag52prime-)
(not (Flag52-))
)

)
(:action Prim52Action-1
:parameters ()
:precondition
(and
(not (ABOVEOF9-ROBOT))
)
:effect
(and
(Flag52prime-)
(not (Flag52-))
)

)
(:action Prim53Action-0
:parameters ()
:precondition
(and
(not (BELOWOF3-ROBOT))
)
:effect
(and
(Flag53prime-)
(not (Flag53-))
)

)
(:action Prim53Action-1
:parameters ()
:precondition
(and
(not (ABOVEOF2-ROBOT))
)
:effect
(and
(Flag53prime-)
(not (Flag53-))
)

)
(:action Prim54Action-0
:parameters ()
:precondition
(and
(not (ABOVEOF3-ROBOT))
)
:effect
(and
(Flag54prime-)
(not (Flag54-))
)

)
(:action Prim54Action-1
:parameters ()
:precondition
(and
(not (BELOWOF3-ROBOT))
)
:effect
(and
(Flag54prime-)
(not (Flag54-))
)

)
(:action Prim55Action-0
:parameters ()
:precondition
(and
(not (ABOVEOF8-ROBOT))
)
:effect
(and
(Flag55prime-)
(not (Flag55-))
)

)
(:action Prim55Action-1
:parameters ()
:precondition
(and
(not (BELOWOF3-ROBOT))
)
:effect
(and
(Flag55prime-)
(not (Flag55-))
)

)
(:action Prim56Action-0
:parameters ()
:precondition
(and
(not (BELOWOF3-ROBOT))
)
:effect
(and
(Flag56prime-)
(not (Flag56-))
)

)
(:action Prim56Action-1
:parameters ()
:precondition
(and
(not (ABOVEOF4-ROBOT))
)
:effect
(and
(Flag56prime-)
(not (Flag56-))
)

)
(:action Prim57Action-0
:parameters ()
:precondition
(and
(not (BELOWOF3-ROBOT))
)
:effect
(and
(Flag57prime-)
(not (Flag57-))
)

)
(:action Prim57Action-1
:parameters ()
:precondition
(and
(not (ABOVEOF10-ROBOT))
)
:effect
(and
(Flag57prime-)
(not (Flag57-))
)

)
(:action Prim58Action-0
:parameters ()
:precondition
(and
(not (BELOWOF2-ROBOT))
)
:effect
(and
(Flag58prime-)
(not (Flag58-))
)

)
(:action Prim58Action-1
:parameters ()
:precondition
(and
(not (ABOVEOF2-ROBOT))
)
:effect
(and
(Flag58prime-)
(not (Flag58-))
)

)
(:action Prim59Action-0
:parameters ()
:precondition
(and
(not (ABOVEOF11-ROBOT))
)
:effect
(and
(Flag59prime-)
(not (Flag59-))
)

)
(:action Prim59Action-1
:parameters ()
:precondition
(and
(not (BELOWOF3-ROBOT))
)
:effect
(and
(Flag59prime-)
(not (Flag59-))
)

)
(:action Flag61Action
:parameters ()
:precondition
(and
(ABOVEOF3-ROBOT)
(BELOWOF4-ROBOT)
)
:effect
(and
(Flag61-)
(Flag61prime-)
)

)
(:action Flag62Action
:parameters ()
:precondition
(and
(ABOVEOF6-ROBOT)
(BELOWOF4-ROBOT)
)
:effect
(and
(Flag62-)
(Flag62prime-)
)

)
(:action Flag63Action
:parameters ()
:precondition
(and
(ABOVEOF9-ROBOT)
(BELOWOF4-ROBOT)
)
:effect
(and
(Flag63-)
(Flag63prime-)
)

)
(:action Flag64Action
:parameters ()
:precondition
(and
(BELOWOF4-ROBOT)
(ABOVEOF4-ROBOT)
)
:effect
(and
(Flag64-)
(Flag64prime-)
)

)
(:action Flag65Action
:parameters ()
:precondition
(and
(ABOVEOF8-ROBOT)
(BELOWOF4-ROBOT)
)
:effect
(and
(Flag65-)
(Flag65prime-)
)

)
(:action Flag66Action
:parameters ()
:precondition
(and
(BELOWOF4-ROBOT)
(ABOVEOF7-ROBOT)
)
:effect
(and
(Flag66-)
(Flag66prime-)
)

)
(:action Flag67Action
:parameters ()
:precondition
(and
(BELOWOF4-ROBOT)
(ABOVEOF5-ROBOT)
)
:effect
(and
(Flag67-)
(Flag67prime-)
)

)
(:action Flag68Action
:parameters ()
:precondition
(and
(BELOWOF3-ROBOT)
(ABOVEOF3-ROBOT)
)
:effect
(and
(Flag68-)
(Flag68prime-)
)

)
(:action Flag69Action
:parameters ()
:precondition
(and
(ABOVEOF10-ROBOT)
(BELOWOF4-ROBOT)
)
:effect
(and
(Flag69-)
(Flag69prime-)
)

)
(:action Flag70Action
:parameters ()
:precondition
(and
(BELOWOF3-ROBOT)
(ABOVEOF11-ROBOT)
)
:effect
(and
(Flag70-)
(Flag70prime-)
)

)
(:action Flag71Action
:parameters ()
:precondition
(and
(ABOVEOF11-ROBOT)
(BELOWOF4-ROBOT)
)
:effect
(and
(Flag71-)
(Flag71prime-)
)

)
(:action Prim61Action-0
:parameters ()
:precondition
(and
(not (ABOVEOF3-ROBOT))
)
:effect
(and
(Flag61prime-)
(not (Flag61-))
)

)
(:action Prim61Action-1
:parameters ()
:precondition
(and
(not (BELOWOF4-ROBOT))
)
:effect
(and
(Flag61prime-)
(not (Flag61-))
)

)
(:action Prim62Action-0
:parameters ()
:precondition
(and
(not (ABOVEOF6-ROBOT))
)
:effect
(and
(Flag62prime-)
(not (Flag62-))
)

)
(:action Prim62Action-1
:parameters ()
:precondition
(and
(not (BELOWOF4-ROBOT))
)
:effect
(and
(Flag62prime-)
(not (Flag62-))
)

)
(:action Prim63Action-0
:parameters ()
:precondition
(and
(not (ABOVEOF9-ROBOT))
)
:effect
(and
(Flag63prime-)
(not (Flag63-))
)

)
(:action Prim63Action-1
:parameters ()
:precondition
(and
(not (BELOWOF4-ROBOT))
)
:effect
(and
(Flag63prime-)
(not (Flag63-))
)

)
(:action Prim64Action-0
:parameters ()
:precondition
(and
(not (BELOWOF4-ROBOT))
)
:effect
(and
(Flag64prime-)
(not (Flag64-))
)

)
(:action Prim64Action-1
:parameters ()
:precondition
(and
(not (ABOVEOF4-ROBOT))
)
:effect
(and
(Flag64prime-)
(not (Flag64-))
)

)
(:action Prim65Action-0
:parameters ()
:precondition
(and
(not (ABOVEOF8-ROBOT))
)
:effect
(and
(Flag65prime-)
(not (Flag65-))
)

)
(:action Prim65Action-1
:parameters ()
:precondition
(and
(not (BELOWOF4-ROBOT))
)
:effect
(and
(Flag65prime-)
(not (Flag65-))
)

)
(:action Prim66Action-0
:parameters ()
:precondition
(and
(not (BELOWOF4-ROBOT))
)
:effect
(and
(Flag66prime-)
(not (Flag66-))
)

)
(:action Prim66Action-1
:parameters ()
:precondition
(and
(not (ABOVEOF7-ROBOT))
)
:effect
(and
(Flag66prime-)
(not (Flag66-))
)

)
(:action Prim67Action-0
:parameters ()
:precondition
(and
(not (BELOWOF4-ROBOT))
)
:effect
(and
(Flag67prime-)
(not (Flag67-))
)

)
(:action Prim67Action-1
:parameters ()
:precondition
(and
(not (ABOVEOF5-ROBOT))
)
:effect
(and
(Flag67prime-)
(not (Flag67-))
)

)
(:action Prim68Action-0
:parameters ()
:precondition
(and
(not (BELOWOF3-ROBOT))
)
:effect
(and
(Flag68prime-)
(not (Flag68-))
)

)
(:action Prim68Action-1
:parameters ()
:precondition
(and
(not (ABOVEOF3-ROBOT))
)
:effect
(and
(Flag68prime-)
(not (Flag68-))
)

)
(:action Prim69Action-0
:parameters ()
:precondition
(and
(not (ABOVEOF10-ROBOT))
)
:effect
(and
(Flag69prime-)
(not (Flag69-))
)

)
(:action Prim69Action-1
:parameters ()
:precondition
(and
(not (BELOWOF4-ROBOT))
)
:effect
(and
(Flag69prime-)
(not (Flag69-))
)

)
(:action Prim70Action-0
:parameters ()
:precondition
(and
(not (BELOWOF3-ROBOT))
)
:effect
(and
(Flag70prime-)
(not (Flag70-))
)

)
(:action Prim70Action-1
:parameters ()
:precondition
(and
(not (ABOVEOF11-ROBOT))
)
:effect
(and
(Flag70prime-)
(not (Flag70-))
)

)
(:action Prim71Action-0
:parameters ()
:precondition
(and
(not (ABOVEOF11-ROBOT))
)
:effect
(and
(Flag71prime-)
(not (Flag71-))
)

)
(:action Prim71Action-1
:parameters ()
:precondition
(and
(not (BELOWOF4-ROBOT))
)
:effect
(and
(Flag71prime-)
(not (Flag71-))
)

)
(:action Flag73Action
:parameters ()
:precondition
(and
(ABOVEOF4-ROBOT)
(BELOWOF5-ROBOT)
)
:effect
(and
(Flag73-)
(Flag73prime-)
)

)
(:action Flag74Action
:parameters ()
:precondition
(and
(ABOVEOF8-ROBOT)
(BELOWOF5-ROBOT)
)
:effect
(and
(Flag74-)
(Flag74prime-)
)

)
(:action Flag75Action
:parameters ()
:precondition
(and
(ABOVEOF10-ROBOT)
(BELOWOF5-ROBOT)
)
:effect
(and
(Flag75-)
(Flag75prime-)
)

)
(:action Flag76Action
:parameters ()
:precondition
(and
(ABOVEOF11-ROBOT)
(BELOWOF5-ROBOT)
)
:effect
(and
(Flag76-)
(Flag76prime-)
)

)
(:action Flag77Action
:parameters ()
:precondition
(and
(ABOVEOF5-ROBOT)
(BELOWOF5-ROBOT)
)
:effect
(and
(Flag77-)
(Flag77prime-)
)

)
(:action Flag78Action
:parameters ()
:precondition
(and
(ABOVEOF7-ROBOT)
(BELOWOF5-ROBOT)
)
:effect
(and
(Flag78-)
(Flag78prime-)
)

)
(:action Flag79Action
:parameters ()
:precondition
(and
(ABOVEOF9-ROBOT)
(BELOWOF5-ROBOT)
)
:effect
(and
(Flag79-)
(Flag79prime-)
)

)
(:action Flag80Action
:parameters ()
:precondition
(and
(ABOVEOF6-ROBOT)
(BELOWOF5-ROBOT)
)
:effect
(and
(Flag80-)
(Flag80prime-)
)

)
(:action Prim73Action-0
:parameters ()
:precondition
(and
(not (ABOVEOF4-ROBOT))
)
:effect
(and
(Flag73prime-)
(not (Flag73-))
)

)
(:action Prim73Action-1
:parameters ()
:precondition
(and
(not (BELOWOF5-ROBOT))
)
:effect
(and
(Flag73prime-)
(not (Flag73-))
)

)
(:action Prim74Action-0
:parameters ()
:precondition
(and
(not (ABOVEOF8-ROBOT))
)
:effect
(and
(Flag74prime-)
(not (Flag74-))
)

)
(:action Prim74Action-1
:parameters ()
:precondition
(and
(not (BELOWOF5-ROBOT))
)
:effect
(and
(Flag74prime-)
(not (Flag74-))
)

)
(:action Prim75Action-0
:parameters ()
:precondition
(and
(not (ABOVEOF10-ROBOT))
)
:effect
(and
(Flag75prime-)
(not (Flag75-))
)

)
(:action Prim75Action-1
:parameters ()
:precondition
(and
(not (BELOWOF5-ROBOT))
)
:effect
(and
(Flag75prime-)
(not (Flag75-))
)

)
(:action Prim76Action-0
:parameters ()
:precondition
(and
(not (ABOVEOF11-ROBOT))
)
:effect
(and
(Flag76prime-)
(not (Flag76-))
)

)
(:action Prim76Action-1
:parameters ()
:precondition
(and
(not (BELOWOF5-ROBOT))
)
:effect
(and
(Flag76prime-)
(not (Flag76-))
)

)
(:action Prim77Action-0
:parameters ()
:precondition
(and
(not (ABOVEOF5-ROBOT))
)
:effect
(and
(Flag77prime-)
(not (Flag77-))
)

)
(:action Prim77Action-1
:parameters ()
:precondition
(and
(not (BELOWOF5-ROBOT))
)
:effect
(and
(Flag77prime-)
(not (Flag77-))
)

)
(:action Prim78Action-0
:parameters ()
:precondition
(and
(not (ABOVEOF7-ROBOT))
)
:effect
(and
(Flag78prime-)
(not (Flag78-))
)

)
(:action Prim78Action-1
:parameters ()
:precondition
(and
(not (BELOWOF5-ROBOT))
)
:effect
(and
(Flag78prime-)
(not (Flag78-))
)

)
(:action Prim79Action-0
:parameters ()
:precondition
(and
(not (ABOVEOF9-ROBOT))
)
:effect
(and
(Flag79prime-)
(not (Flag79-))
)

)
(:action Prim79Action-1
:parameters ()
:precondition
(and
(not (BELOWOF5-ROBOT))
)
:effect
(and
(Flag79prime-)
(not (Flag79-))
)

)
(:action Prim80Action-0
:parameters ()
:precondition
(and
(not (ABOVEOF6-ROBOT))
)
:effect
(and
(Flag80prime-)
(not (Flag80-))
)

)
(:action Prim80Action-1
:parameters ()
:precondition
(and
(not (BELOWOF5-ROBOT))
)
:effect
(and
(Flag80prime-)
(not (Flag80-))
)

)
(:action Flag82Action
:parameters ()
:precondition
(and
(ABOVEOF11-ROBOT)
(BELOWOF6-ROBOT)
)
:effect
(and
(Flag82-)
(Flag82prime-)
)

)
(:action Flag83Action
:parameters ()
:precondition
(and
(BELOWOF6-ROBOT)
(ABOVEOF5-ROBOT)
)
:effect
(and
(Flag83-)
(Flag83prime-)
)

)
(:action Flag84Action
:parameters ()
:precondition
(and
(BELOWOF6-ROBOT)
(ABOVEOF7-ROBOT)
)
:effect
(and
(Flag84-)
(Flag84prime-)
)

)
(:action Flag85Action
:parameters ()
:precondition
(and
(BELOWOF5-ROBOT)
(ABOVEOF5-ROBOT)
)
:effect
(and
(Flag85-)
(Flag85prime-)
)

)
(:action Flag86Action
:parameters ()
:precondition
(and
(BELOWOF6-ROBOT)
(ABOVEOF6-ROBOT)
)
:effect
(and
(Flag86-)
(Flag86prime-)
)

)
(:action Flag87Action
:parameters ()
:precondition
(and
(ABOVEOF8-ROBOT)
(BELOWOF6-ROBOT)
)
:effect
(and
(Flag87-)
(Flag87prime-)
)

)
(:action Flag88Action
:parameters ()
:precondition
(and
(BELOWOF6-ROBOT)
(ABOVEOF9-ROBOT)
)
:effect
(and
(Flag88-)
(Flag88prime-)
)

)
(:action Flag89Action
:parameters ()
:precondition
(and
(BELOWOF6-ROBOT)
(ABOVEOF10-ROBOT)
)
:effect
(and
(Flag89-)
(Flag89prime-)
)

)
(:action Prim82Action-0
:parameters ()
:precondition
(and
(not (ABOVEOF11-ROBOT))
)
:effect
(and
(Flag82prime-)
(not (Flag82-))
)

)
(:action Prim82Action-1
:parameters ()
:precondition
(and
(not (BELOWOF6-ROBOT))
)
:effect
(and
(Flag82prime-)
(not (Flag82-))
)

)
(:action Prim83Action-0
:parameters ()
:precondition
(and
(not (BELOWOF6-ROBOT))
)
:effect
(and
(Flag83prime-)
(not (Flag83-))
)

)
(:action Prim83Action-1
:parameters ()
:precondition
(and
(not (ABOVEOF5-ROBOT))
)
:effect
(and
(Flag83prime-)
(not (Flag83-))
)

)
(:action Prim84Action-0
:parameters ()
:precondition
(and
(not (BELOWOF6-ROBOT))
)
:effect
(and
(Flag84prime-)
(not (Flag84-))
)

)
(:action Prim84Action-1
:parameters ()
:precondition
(and
(not (ABOVEOF7-ROBOT))
)
:effect
(and
(Flag84prime-)
(not (Flag84-))
)

)
(:action Prim85Action-0
:parameters ()
:precondition
(and
(not (BELOWOF5-ROBOT))
)
:effect
(and
(Flag85prime-)
(not (Flag85-))
)

)
(:action Prim85Action-1
:parameters ()
:precondition
(and
(not (ABOVEOF5-ROBOT))
)
:effect
(and
(Flag85prime-)
(not (Flag85-))
)

)
(:action Prim86Action-0
:parameters ()
:precondition
(and
(not (BELOWOF6-ROBOT))
)
:effect
(and
(Flag86prime-)
(not (Flag86-))
)

)
(:action Prim86Action-1
:parameters ()
:precondition
(and
(not (ABOVEOF6-ROBOT))
)
:effect
(and
(Flag86prime-)
(not (Flag86-))
)

)
(:action Prim87Action-0
:parameters ()
:precondition
(and
(not (ABOVEOF8-ROBOT))
)
:effect
(and
(Flag87prime-)
(not (Flag87-))
)

)
(:action Prim87Action-1
:parameters ()
:precondition
(and
(not (BELOWOF6-ROBOT))
)
:effect
(and
(Flag87prime-)
(not (Flag87-))
)

)
(:action Prim88Action-0
:parameters ()
:precondition
(and
(not (BELOWOF6-ROBOT))
)
:effect
(and
(Flag88prime-)
(not (Flag88-))
)

)
(:action Prim88Action-1
:parameters ()
:precondition
(and
(not (ABOVEOF9-ROBOT))
)
:effect
(and
(Flag88prime-)
(not (Flag88-))
)

)
(:action Prim89Action-0
:parameters ()
:precondition
(and
(not (BELOWOF6-ROBOT))
)
:effect
(and
(Flag89prime-)
(not (Flag89-))
)

)
(:action Prim89Action-1
:parameters ()
:precondition
(and
(not (ABOVEOF10-ROBOT))
)
:effect
(and
(Flag89prime-)
(not (Flag89-))
)

)
(:action Flag91Action
:parameters ()
:precondition
(and
(ABOVEOF6-ROBOT)
(BELOWOF7-ROBOT)
)
:effect
(and
(Flag91-)
(Flag91prime-)
)

)
(:action Flag92Action
:parameters ()
:precondition
(and
(ABOVEOF9-ROBOT)
(BELOWOF7-ROBOT)
)
:effect
(and
(Flag92-)
(Flag92prime-)
)

)
(:action Flag93Action
:parameters ()
:precondition
(and
(ABOVEOF6-ROBOT)
(BELOWOF6-ROBOT)
)
:effect
(and
(Flag93-)
(Flag93prime-)
)

)
(:action Flag94Action
:parameters ()
:precondition
(and
(ABOVEOF10-ROBOT)
(BELOWOF7-ROBOT)
)
:effect
(and
(Flag94-)
(Flag94prime-)
)

)
(:action Flag95Action
:parameters ()
:precondition
(and
(ABOVEOF10-ROBOT)
(BELOWOF6-ROBOT)
)
:effect
(and
(Flag95-)
(Flag95prime-)
)

)
(:action Flag96Action
:parameters ()
:precondition
(and
(ABOVEOF7-ROBOT)
(BELOWOF7-ROBOT)
)
:effect
(and
(Flag96-)
(Flag96prime-)
)

)
(:action Flag97Action
:parameters ()
:precondition
(and
(ABOVEOF8-ROBOT)
(BELOWOF7-ROBOT)
)
:effect
(and
(Flag97-)
(Flag97prime-)
)

)
(:action Flag98Action
:parameters ()
:precondition
(and
(ABOVEOF11-ROBOT)
(BELOWOF7-ROBOT)
)
:effect
(and
(Flag98-)
(Flag98prime-)
)

)
(:action Prim91Action-0
:parameters ()
:precondition
(and
(not (ABOVEOF6-ROBOT))
)
:effect
(and
(Flag91prime-)
(not (Flag91-))
)

)
(:action Prim91Action-1
:parameters ()
:precondition
(and
(not (BELOWOF7-ROBOT))
)
:effect
(and
(Flag91prime-)
(not (Flag91-))
)

)
(:action Prim92Action-0
:parameters ()
:precondition
(and
(not (ABOVEOF9-ROBOT))
)
:effect
(and
(Flag92prime-)
(not (Flag92-))
)

)
(:action Prim92Action-1
:parameters ()
:precondition
(and
(not (BELOWOF7-ROBOT))
)
:effect
(and
(Flag92prime-)
(not (Flag92-))
)

)
(:action Prim93Action-0
:parameters ()
:precondition
(and
(not (ABOVEOF6-ROBOT))
)
:effect
(and
(Flag93prime-)
(not (Flag93-))
)

)
(:action Prim93Action-1
:parameters ()
:precondition
(and
(not (BELOWOF6-ROBOT))
)
:effect
(and
(Flag93prime-)
(not (Flag93-))
)

)
(:action Prim94Action-0
:parameters ()
:precondition
(and
(not (ABOVEOF10-ROBOT))
)
:effect
(and
(Flag94prime-)
(not (Flag94-))
)

)
(:action Prim94Action-1
:parameters ()
:precondition
(and
(not (BELOWOF7-ROBOT))
)
:effect
(and
(Flag94prime-)
(not (Flag94-))
)

)
(:action Prim95Action-0
:parameters ()
:precondition
(and
(not (ABOVEOF10-ROBOT))
)
:effect
(and
(Flag95prime-)
(not (Flag95-))
)

)
(:action Prim95Action-1
:parameters ()
:precondition
(and
(not (BELOWOF6-ROBOT))
)
:effect
(and
(Flag95prime-)
(not (Flag95-))
)

)
(:action Prim96Action-0
:parameters ()
:precondition
(and
(not (ABOVEOF7-ROBOT))
)
:effect
(and
(Flag96prime-)
(not (Flag96-))
)

)
(:action Prim96Action-1
:parameters ()
:precondition
(and
(not (BELOWOF7-ROBOT))
)
:effect
(and
(Flag96prime-)
(not (Flag96-))
)

)
(:action Prim97Action-0
:parameters ()
:precondition
(and
(not (ABOVEOF8-ROBOT))
)
:effect
(and
(Flag97prime-)
(not (Flag97-))
)

)
(:action Prim97Action-1
:parameters ()
:precondition
(and
(not (BELOWOF7-ROBOT))
)
:effect
(and
(Flag97prime-)
(not (Flag97-))
)

)
(:action Prim98Action-0
:parameters ()
:precondition
(and
(not (ABOVEOF11-ROBOT))
)
:effect
(and
(Flag98prime-)
(not (Flag98-))
)

)
(:action Prim98Action-1
:parameters ()
:precondition
(and
(not (BELOWOF7-ROBOT))
)
:effect
(and
(Flag98prime-)
(not (Flag98-))
)

)
(:action Flag100Action
:parameters ()
:precondition
(and
(BELOWOF8-ROBOT)
(ABOVEOF10-ROBOT)
)
:effect
(and
(Flag100-)
(Flag100prime-)
)

)
(:action Flag101Action
:parameters ()
:precondition
(and
(ABOVEOF8-ROBOT)
(BELOWOF8-ROBOT)
)
:effect
(and
(Flag101-)
(Flag101prime-)
)

)
(:action Flag102Action
:parameters ()
:precondition
(and
(BELOWOF8-ROBOT)
(ABOVEOF9-ROBOT)
)
:effect
(and
(Flag102-)
(Flag102prime-)
)

)
(:action Flag103Action
:parameters ()
:precondition
(and
(BELOWOF7-ROBOT)
(ABOVEOF7-ROBOT)
)
:effect
(and
(Flag103-)
(Flag103prime-)
)

)
(:action Flag104Action
:parameters ()
:precondition
(and
(BELOWOF8-ROBOT)
(ABOVEOF7-ROBOT)
)
:effect
(and
(Flag104-)
(Flag104prime-)
)

)
(:action Flag105Action
:parameters ()
:precondition
(and
(BELOWOF8-ROBOT)
(ABOVEOF11-ROBOT)
)
:effect
(and
(Flag105-)
(Flag105prime-)
)

)
(:action Prim100Action-0
:parameters ()
:precondition
(and
(not (BELOWOF8-ROBOT))
)
:effect
(and
(Flag100prime-)
(not (Flag100-))
)

)
(:action Prim100Action-1
:parameters ()
:precondition
(and
(not (ABOVEOF10-ROBOT))
)
:effect
(and
(Flag100prime-)
(not (Flag100-))
)

)
(:action Prim101Action-0
:parameters ()
:precondition
(and
(not (ABOVEOF8-ROBOT))
)
:effect
(and
(Flag101prime-)
(not (Flag101-))
)

)
(:action Prim101Action-1
:parameters ()
:precondition
(and
(not (BELOWOF8-ROBOT))
)
:effect
(and
(Flag101prime-)
(not (Flag101-))
)

)
(:action Prim102Action-0
:parameters ()
:precondition
(and
(not (BELOWOF8-ROBOT))
)
:effect
(and
(Flag102prime-)
(not (Flag102-))
)

)
(:action Prim102Action-1
:parameters ()
:precondition
(and
(not (ABOVEOF9-ROBOT))
)
:effect
(and
(Flag102prime-)
(not (Flag102-))
)

)
(:action Prim103Action-0
:parameters ()
:precondition
(and
(not (BELOWOF7-ROBOT))
)
:effect
(and
(Flag103prime-)
(not (Flag103-))
)

)
(:action Prim103Action-1
:parameters ()
:precondition
(and
(not (ABOVEOF7-ROBOT))
)
:effect
(and
(Flag103prime-)
(not (Flag103-))
)

)
(:action Prim104Action-0
:parameters ()
:precondition
(and
(not (BELOWOF8-ROBOT))
)
:effect
(and
(Flag104prime-)
(not (Flag104-))
)

)
(:action Prim104Action-1
:parameters ()
:precondition
(and
(not (ABOVEOF7-ROBOT))
)
:effect
(and
(Flag104prime-)
(not (Flag104-))
)

)
(:action Prim105Action-0
:parameters ()
:precondition
(and
(not (BELOWOF8-ROBOT))
)
:effect
(and
(Flag105prime-)
(not (Flag105-))
)

)
(:action Prim105Action-1
:parameters ()
:precondition
(and
(not (ABOVEOF11-ROBOT))
)
:effect
(and
(Flag105prime-)
(not (Flag105-))
)

)
(:action Flag107Action
:parameters ()
:precondition
(and
(ABOVEOF10-ROBOT)
(BELOWOF9-ROBOT)
)
:effect
(and
(Flag107-)
(Flag107prime-)
)

)
(:action Flag108Action
:parameters ()
:precondition
(and
(BELOWOF8-ROBOT)
(ABOVEOF8-ROBOT)
)
:effect
(and
(Flag108-)
(Flag108prime-)
)

)
(:action Flag109Action
:parameters ()
:precondition
(and
(ABOVEOF8-ROBOT)
(BELOWOF9-ROBOT)
)
:effect
(and
(Flag109-)
(Flag109prime-)
)

)
(:action Flag110Action
:parameters ()
:precondition
(and
(ABOVEOF11-ROBOT)
(BELOWOF9-ROBOT)
)
:effect
(and
(Flag110-)
(Flag110prime-)
)

)
(:action Flag111Action
:parameters ()
:precondition
(and
(BELOWOF9-ROBOT)
(ABOVEOF9-ROBOT)
)
:effect
(and
(Flag111-)
(Flag111prime-)
)

)
(:action Prim107Action-0
:parameters ()
:precondition
(and
(not (ABOVEOF10-ROBOT))
)
:effect
(and
(Flag107prime-)
(not (Flag107-))
)

)
(:action Prim107Action-1
:parameters ()
:precondition
(and
(not (BELOWOF9-ROBOT))
)
:effect
(and
(Flag107prime-)
(not (Flag107-))
)

)
(:action Prim108Action-0
:parameters ()
:precondition
(and
(not (BELOWOF8-ROBOT))
)
:effect
(and
(Flag108prime-)
(not (Flag108-))
)

)
(:action Prim108Action-1
:parameters ()
:precondition
(and
(not (ABOVEOF8-ROBOT))
)
:effect
(and
(Flag108prime-)
(not (Flag108-))
)

)
(:action Prim109Action-0
:parameters ()
:precondition
(and
(not (ABOVEOF8-ROBOT))
)
:effect
(and
(Flag109prime-)
(not (Flag109-))
)

)
(:action Prim109Action-1
:parameters ()
:precondition
(and
(not (BELOWOF9-ROBOT))
)
:effect
(and
(Flag109prime-)
(not (Flag109-))
)

)
(:action Prim110Action-0
:parameters ()
:precondition
(and
(not (ABOVEOF11-ROBOT))
)
:effect
(and
(Flag110prime-)
(not (Flag110-))
)

)
(:action Prim110Action-1
:parameters ()
:precondition
(and
(not (BELOWOF9-ROBOT))
)
:effect
(and
(Flag110prime-)
(not (Flag110-))
)

)
(:action Prim111Action-0
:parameters ()
:precondition
(and
(not (BELOWOF9-ROBOT))
)
:effect
(and
(Flag111prime-)
(not (Flag111-))
)

)
(:action Prim111Action-1
:parameters ()
:precondition
(and
(not (ABOVEOF9-ROBOT))
)
:effect
(and
(Flag111prime-)
(not (Flag111-))
)

)
(:action Flag113Action
:parameters ()
:precondition
(and
(BELOWOF10-ROBOT)
(ABOVEOF9-ROBOT)
)
:effect
(and
(Flag113-)
(Flag113prime-)
)

)
(:action Flag114Action
:parameters ()
:precondition
(and
(BELOWOF10-ROBOT)
(ABOVEOF10-ROBOT)
)
:effect
(and
(Flag114-)
(Flag114prime-)
)

)
(:action Flag115Action
:parameters ()
:precondition
(and
(ABOVEOF9-ROBOT)
(BELOWOF9-ROBOT)
)
:effect
(and
(Flag115-)
(Flag115prime-)
)

)
(:action Flag116Action
:parameters ()
:precondition
(and
(ABOVEOF11-ROBOT)
(BELOWOF10-ROBOT)
)
:effect
(and
(Flag116-)
(Flag116prime-)
)

)
(:action Prim113Action-0
:parameters ()
:precondition
(and
(not (BELOWOF10-ROBOT))
)
:effect
(and
(Flag113prime-)
(not (Flag113-))
)

)
(:action Prim113Action-1
:parameters ()
:precondition
(and
(not (ABOVEOF9-ROBOT))
)
:effect
(and
(Flag113prime-)
(not (Flag113-))
)

)
(:action Prim114Action-0
:parameters ()
:precondition
(and
(not (BELOWOF10-ROBOT))
)
:effect
(and
(Flag114prime-)
(not (Flag114-))
)

)
(:action Prim114Action-1
:parameters ()
:precondition
(and
(not (ABOVEOF10-ROBOT))
)
:effect
(and
(Flag114prime-)
(not (Flag114-))
)

)
(:action Prim115Action-0
:parameters ()
:precondition
(and
(not (ABOVEOF9-ROBOT))
)
:effect
(and
(Flag115prime-)
(not (Flag115-))
)

)
(:action Prim115Action-1
:parameters ()
:precondition
(and
(not (BELOWOF9-ROBOT))
)
:effect
(and
(Flag115prime-)
(not (Flag115-))
)

)
(:action Prim116Action-0
:parameters ()
:precondition
(and
(not (ABOVEOF11-ROBOT))
)
:effect
(and
(Flag116prime-)
(not (Flag116-))
)

)
(:action Prim116Action-1
:parameters ()
:precondition
(and
(not (BELOWOF10-ROBOT))
)
:effect
(and
(Flag116prime-)
(not (Flag116-))
)

)
(:action Flag118Action
:parameters ()
:precondition
(and
(BELOWOF11-ROBOT)
(ABOVEOF10-ROBOT)
)
:effect
(and
(Flag118-)
(Flag118prime-)
)

)
(:action Flag119Action
:parameters ()
:precondition
(and
(ABOVEOF11-ROBOT)
(BELOWOF11-ROBOT)
)
:effect
(and
(Flag119-)
(Flag119prime-)
)

)
(:action Prim118Action-1
:parameters ()
:precondition
(and
(not (ABOVEOF10-ROBOT))
)
:effect
(and
(Flag118prime-)
(not (Flag118-))
)

)
(:action Prim119Action-0
:parameters ()
:precondition
(and
(not (ABOVEOF11-ROBOT))
)
:effect
(and
(Flag119prime-)
(not (Flag119-))
)

)
(:action Flag121Action
:parameters ()
:precondition
(and
(BELOWOF12-ROBOT)
(ABOVEOF11-ROBOT)
)
:effect
(and
(Flag121-)
(Flag121prime-)
)

)
(:action Prim121Action-0
:parameters ()
:precondition
(and
(not (BELOWOF12-ROBOT))
)
:effect
(and
(Flag121prime-)
(not (Flag121-))
)

)
(:action Prim121Action-1
:parameters ()
:precondition
(and
(not (ABOVEOF11-ROBOT))
)
:effect
(and
(Flag121prime-)
(not (Flag121-))
)

)
(:action Flag241Action-8
:parameters ()
:precondition
(and
(BELOWOF1-ROBOT)
)
:effect
(and
(Flag241-)
(Flag241prime-)
)

)
(:action MOVEUP-ROBOT
:parameters ()
:precondition
(and
(Flag2-)
(Flag2prime-)
(Flag120prime-)
(Flag117prime-)
(Flag112prime-)
(Flag106prime-)
(Flag99prime-)
(Flag90prime-)
(Flag81prime-)
(Flag72prime-)
(Flag60prime-)
(Flag48prime-)
(Flag241prime-)
)
:effect
(and
(when
(and
(Flag120-)
)
(and
(ROW11-ROBOT)
(not (NOT-ROW11-ROBOT))
)
)
(when
(and
(Flag117-)
)
(and
(ROW10-ROBOT)
(not (NOT-ROW10-ROBOT))
)
)
(when
(and
(Flag112-)
)
(and
(ROW9-ROBOT)
(not (NOT-ROW9-ROBOT))
)
)
(when
(and
(Flag106-)
)
(and
(ROW8-ROBOT)
(not (NOT-ROW8-ROBOT))
)
)
(when
(and
(Flag99-)
)
(and
(ROW7-ROBOT)
(not (NOT-ROW7-ROBOT))
)
)
(when
(and
(Flag90-)
)
(and
(ROW6-ROBOT)
(not (NOT-ROW6-ROBOT))
)
)
(when
(and
(Flag81-)
)
(and
(ROW5-ROBOT)
(not (NOT-ROW5-ROBOT))
)
)
(when
(and
(Flag72-)
)
(and
(ROW4-ROBOT)
(not (NOT-ROW4-ROBOT))
)
)
(when
(and
(Flag60-)
)
(and
(ROW3-ROBOT)
(not (NOT-ROW3-ROBOT))
)
)
(when
(and
(Flag48-)
)
(and
(ROW2-ROBOT)
(not (NOT-ROW2-ROBOT))
)
)
(when
(and
(Flag241-)
)
(and
(ROW1-ROBOT)
(not (NOT-ROW1-ROBOT))
)
)
(when
(and
(ROW10-ROBOT)
)
(and
(ROW11-ROBOT)
(NOT-ROW10-ROBOT)
(not (NOT-ROW11-ROBOT))
(not (ROW10-ROBOT))
)
)
(when
(and
(ROW9-ROBOT)
)
(and
(ROW10-ROBOT)
(NOT-ROW9-ROBOT)
(not (NOT-ROW10-ROBOT))
(not (ROW9-ROBOT))
)
)
(when
(and
(ROW8-ROBOT)
)
(and
(ROW9-ROBOT)
(NOT-ROW8-ROBOT)
(not (NOT-ROW9-ROBOT))
(not (ROW8-ROBOT))
)
)
(when
(and
(ROW7-ROBOT)
)
(and
(ROW8-ROBOT)
(NOT-ROW7-ROBOT)
(not (NOT-ROW8-ROBOT))
(not (ROW7-ROBOT))
)
)
(when
(and
(ROW6-ROBOT)
)
(and
(ROW7-ROBOT)
(NOT-ROW6-ROBOT)
(not (NOT-ROW7-ROBOT))
(not (ROW6-ROBOT))
)
)
(when
(and
(ROW5-ROBOT)
)
(and
(ROW6-ROBOT)
(NOT-ROW5-ROBOT)
(not (NOT-ROW6-ROBOT))
(not (ROW5-ROBOT))
)
)
(when
(and
(ROW4-ROBOT)
)
(and
(ROW5-ROBOT)
(NOT-ROW4-ROBOT)
(not (NOT-ROW5-ROBOT))
(not (ROW4-ROBOT))
)
)
(when
(and
(ROW3-ROBOT)
)
(and
(ROW4-ROBOT)
(NOT-ROW3-ROBOT)
(not (NOT-ROW4-ROBOT))
(not (ROW3-ROBOT))
)
)
(when
(and
(ROW2-ROBOT)
)
(and
(ROW3-ROBOT)
(NOT-ROW2-ROBOT)
(not (NOT-ROW3-ROBOT))
(not (ROW2-ROBOT))
)
)
(when
(and
(ROW1-ROBOT)
)
(and
(ROW2-ROBOT)
(NOT-ROW1-ROBOT)
(not (NOT-ROW2-ROBOT))
(not (ROW1-ROBOT))
)
)
(when
(and
(ROW0-ROBOT)
)
(and
(ROW1-ROBOT)
(NOT-ROW0-ROBOT)
(not (NOT-ROW1-ROBOT))
(not (ROW0-ROBOT))
)
)
(when
(and
(BELOWOF8-ROBOT)
)
(and
(BELOWOF12-ROBOT)
(NOT-BELOWOF11-ROBOT)
(BELOWOF11-ROBOT)
(NOT-BELOWOF10-ROBOT)
(BELOWOF10-ROBOT)
(NOT-BELOWOF9-ROBOT)
(BELOWOF9-ROBOT)
(NOT-BELOWOF8-ROBOT)
(not (NOT-BELOWOF12-ROBOT))
(not (BELOWOF8-ROBOT))
)
)
(when
(and
(BELOWOF3-ROBOT)
)
(and
(BELOWOF12-ROBOT)
(NOT-BELOWOF11-ROBOT)
(BELOWOF11-ROBOT)
(NOT-BELOWOF10-ROBOT)
(BELOWOF10-ROBOT)
(NOT-BELOWOF9-ROBOT)
(BELOWOF9-ROBOT)
(NOT-BELOWOF8-ROBOT)
(BELOWOF8-ROBOT)
(NOT-BELOWOF7-ROBOT)
(BELOWOF7-ROBOT)
(NOT-BELOWOF6-ROBOT)
(BELOWOF6-ROBOT)
(NOT-BELOWOF5-ROBOT)
(BELOWOF5-ROBOT)
(NOT-BELOWOF4-ROBOT)
(BELOWOF4-ROBOT)
(NOT-BELOWOF3-ROBOT)
(not (NOT-BELOWOF12-ROBOT))
(not (BELOWOF3-ROBOT))
)
)
(when
(and
(BELOWOF9-ROBOT)
)
(and
(BELOWOF12-ROBOT)
(NOT-BELOWOF11-ROBOT)
(BELOWOF11-ROBOT)
(NOT-BELOWOF10-ROBOT)
(BELOWOF10-ROBOT)
(NOT-BELOWOF9-ROBOT)
(not (NOT-BELOWOF12-ROBOT))
(not (BELOWOF9-ROBOT))
)
)
(when
(and
(BELOWOF7-ROBOT)
)
(and
(BELOWOF12-ROBOT)
(NOT-BELOWOF11-ROBOT)
(BELOWOF11-ROBOT)
(NOT-BELOWOF10-ROBOT)
(BELOWOF10-ROBOT)
(NOT-BELOWOF9-ROBOT)
(BELOWOF9-ROBOT)
(NOT-BELOWOF8-ROBOT)
(BELOWOF8-ROBOT)
(NOT-BELOWOF7-ROBOT)
(not (NOT-BELOWOF12-ROBOT))
(not (BELOWOF7-ROBOT))
)
)
(when
(and
(BELOWOF4-ROBOT)
)
(and
(BELOWOF12-ROBOT)
(NOT-BELOWOF11-ROBOT)
(BELOWOF11-ROBOT)
(NOT-BELOWOF10-ROBOT)
(BELOWOF10-ROBOT)
(NOT-BELOWOF9-ROBOT)
(BELOWOF9-ROBOT)
(NOT-BELOWOF8-ROBOT)
(BELOWOF8-ROBOT)
(NOT-BELOWOF7-ROBOT)
(BELOWOF7-ROBOT)
(NOT-BELOWOF6-ROBOT)
(BELOWOF6-ROBOT)
(NOT-BELOWOF5-ROBOT)
(BELOWOF5-ROBOT)
(NOT-BELOWOF4-ROBOT)
(not (NOT-BELOWOF12-ROBOT))
(not (BELOWOF4-ROBOT))
)
)
(when
(and
(BELOWOF1-ROBOT)
)
(and
(BELOWOF12-ROBOT)
(NOT-BELOWOF11-ROBOT)
(BELOWOF11-ROBOT)
(NOT-BELOWOF10-ROBOT)
(BELOWOF10-ROBOT)
(NOT-BELOWOF9-ROBOT)
(BELOWOF9-ROBOT)
(NOT-BELOWOF8-ROBOT)
(BELOWOF8-ROBOT)
(NOT-BELOWOF7-ROBOT)
(BELOWOF7-ROBOT)
(NOT-BELOWOF6-ROBOT)
(BELOWOF6-ROBOT)
(NOT-BELOWOF5-ROBOT)
(BELOWOF5-ROBOT)
(NOT-BELOWOF4-ROBOT)
(BELOWOF4-ROBOT)
(NOT-BELOWOF3-ROBOT)
(BELOWOF3-ROBOT)
(NOT-BELOWOF2-ROBOT)
(BELOWOF2-ROBOT)
(NOT-BELOWOF1-ROBOT)
(not (NOT-BELOWOF12-ROBOT))
(not (BELOWOF1-ROBOT))
)
)
(when
(and
(BELOWOF2-ROBOT)
)
(and
(BELOWOF12-ROBOT)
(NOT-BELOWOF11-ROBOT)
(BELOWOF11-ROBOT)
(NOT-BELOWOF10-ROBOT)
(BELOWOF10-ROBOT)
(NOT-BELOWOF9-ROBOT)
(BELOWOF9-ROBOT)
(NOT-BELOWOF8-ROBOT)
(BELOWOF8-ROBOT)
(NOT-BELOWOF7-ROBOT)
(BELOWOF7-ROBOT)
(NOT-BELOWOF6-ROBOT)
(BELOWOF6-ROBOT)
(NOT-BELOWOF5-ROBOT)
(BELOWOF5-ROBOT)
(NOT-BELOWOF4-ROBOT)
(BELOWOF4-ROBOT)
(NOT-BELOWOF3-ROBOT)
(BELOWOF3-ROBOT)
(NOT-BELOWOF2-ROBOT)
(not (NOT-BELOWOF12-ROBOT))
(not (BELOWOF2-ROBOT))
)
)
(when
(and
(BELOWOF5-ROBOT)
)
(and
(BELOWOF12-ROBOT)
(NOT-BELOWOF11-ROBOT)
(BELOWOF11-ROBOT)
(NOT-BELOWOF10-ROBOT)
(BELOWOF10-ROBOT)
(NOT-BELOWOF9-ROBOT)
(BELOWOF9-ROBOT)
(NOT-BELOWOF8-ROBOT)
(BELOWOF8-ROBOT)
(NOT-BELOWOF7-ROBOT)
(BELOWOF7-ROBOT)
(NOT-BELOWOF6-ROBOT)
(BELOWOF6-ROBOT)
(NOT-BELOWOF5-ROBOT)
(not (NOT-BELOWOF12-ROBOT))
(not (BELOWOF5-ROBOT))
)
)
(when
(and
(BELOWOF6-ROBOT)
)
(and
(BELOWOF12-ROBOT)
(NOT-BELOWOF11-ROBOT)
(BELOWOF11-ROBOT)
(NOT-BELOWOF10-ROBOT)
(BELOWOF10-ROBOT)
(NOT-BELOWOF9-ROBOT)
(BELOWOF9-ROBOT)
(NOT-BELOWOF8-ROBOT)
(BELOWOF8-ROBOT)
(NOT-BELOWOF7-ROBOT)
(BELOWOF7-ROBOT)
(NOT-BELOWOF6-ROBOT)
(not (NOT-BELOWOF12-ROBOT))
(not (BELOWOF6-ROBOT))
)
)
(when
(and
(BELOWOF11-ROBOT)
)
(and
(BELOWOF12-ROBOT)
(NOT-BELOWOF11-ROBOT)
(not (NOT-BELOWOF12-ROBOT))
(not (BELOWOF11-ROBOT))
)
)
(when
(and
(BELOWOF10-ROBOT)
)
(and
(BELOWOF12-ROBOT)
(NOT-BELOWOF11-ROBOT)
(BELOWOF11-ROBOT)
(NOT-BELOWOF10-ROBOT)
(not (NOT-BELOWOF12-ROBOT))
(not (BELOWOF10-ROBOT))
)
)
(when
(and
(ABOVEOF11-ROBOT)
)
(and
(ABOVEOF10-ROBOT)
(ABOVEOF9-ROBOT)
(ABOVEOF8-ROBOT)
(ABOVEOF7-ROBOT)
(ABOVEOF6-ROBOT)
(ABOVEOF5-ROBOT)
(ABOVEOF4-ROBOT)
(ABOVEOF3-ROBOT)
(ABOVEOF2-ROBOT)
(not (NOT-ABOVEOF11-ROBOT))
(not (NOT-ABOVEOF10-ROBOT))
(not (NOT-ABOVEOF9-ROBOT))
(not (NOT-ABOVEOF8-ROBOT))
(not (NOT-ABOVEOF7-ROBOT))
(not (NOT-ABOVEOF6-ROBOT))
(not (NOT-ABOVEOF5-ROBOT))
(not (NOT-ABOVEOF4-ROBOT))
(not (NOT-ABOVEOF3-ROBOT))
(not (NOT-ABOVEOF2-ROBOT))
)
)
(when
(and
(ABOVEOF10-ROBOT)
)
(and
(ABOVEOF11-ROBOT)
(ABOVEOF9-ROBOT)
(ABOVEOF8-ROBOT)
(ABOVEOF7-ROBOT)
(ABOVEOF6-ROBOT)
(ABOVEOF5-ROBOT)
(ABOVEOF4-ROBOT)
(ABOVEOF3-ROBOT)
(ABOVEOF2-ROBOT)
(not (NOT-ABOVEOF11-ROBOT))
(not (NOT-ABOVEOF10-ROBOT))
(not (NOT-ABOVEOF9-ROBOT))
(not (NOT-ABOVEOF8-ROBOT))
(not (NOT-ABOVEOF7-ROBOT))
(not (NOT-ABOVEOF6-ROBOT))
(not (NOT-ABOVEOF5-ROBOT))
(not (NOT-ABOVEOF4-ROBOT))
(not (NOT-ABOVEOF3-ROBOT))
(not (NOT-ABOVEOF2-ROBOT))
)
)
(when
(and
(ABOVEOF9-ROBOT)
)
(and
(ABOVEOF10-ROBOT)
(ABOVEOF8-ROBOT)
(ABOVEOF7-ROBOT)
(ABOVEOF6-ROBOT)
(ABOVEOF5-ROBOT)
(ABOVEOF4-ROBOT)
(ABOVEOF3-ROBOT)
(ABOVEOF2-ROBOT)
(not (NOT-ABOVEOF10-ROBOT))
(not (NOT-ABOVEOF9-ROBOT))
(not (NOT-ABOVEOF8-ROBOT))
(not (NOT-ABOVEOF7-ROBOT))
(not (NOT-ABOVEOF6-ROBOT))
(not (NOT-ABOVEOF5-ROBOT))
(not (NOT-ABOVEOF4-ROBOT))
(not (NOT-ABOVEOF3-ROBOT))
(not (NOT-ABOVEOF2-ROBOT))
)
)
(when
(and
(ABOVEOF8-ROBOT)
)
(and
(ABOVEOF9-ROBOT)
(ABOVEOF7-ROBOT)
(ABOVEOF6-ROBOT)
(ABOVEOF5-ROBOT)
(ABOVEOF4-ROBOT)
(ABOVEOF3-ROBOT)
(ABOVEOF2-ROBOT)
(not (NOT-ABOVEOF9-ROBOT))
(not (NOT-ABOVEOF8-ROBOT))
(not (NOT-ABOVEOF7-ROBOT))
(not (NOT-ABOVEOF6-ROBOT))
(not (NOT-ABOVEOF5-ROBOT))
(not (NOT-ABOVEOF4-ROBOT))
(not (NOT-ABOVEOF3-ROBOT))
(not (NOT-ABOVEOF2-ROBOT))
)
)
(when
(and
(ABOVEOF7-ROBOT)
)
(and
(ABOVEOF8-ROBOT)
(ABOVEOF6-ROBOT)
(ABOVEOF5-ROBOT)
(ABOVEOF4-ROBOT)
(ABOVEOF3-ROBOT)
(ABOVEOF2-ROBOT)
(not (NOT-ABOVEOF8-ROBOT))
(not (NOT-ABOVEOF7-ROBOT))
(not (NOT-ABOVEOF6-ROBOT))
(not (NOT-ABOVEOF5-ROBOT))
(not (NOT-ABOVEOF4-ROBOT))
(not (NOT-ABOVEOF3-ROBOT))
(not (NOT-ABOVEOF2-ROBOT))
)
)
(when
(and
(ABOVEOF6-ROBOT)
)
(and
(ABOVEOF7-ROBOT)
(ABOVEOF5-ROBOT)
(ABOVEOF4-ROBOT)
(ABOVEOF3-ROBOT)
(ABOVEOF2-ROBOT)
(not (NOT-ABOVEOF7-ROBOT))
(not (NOT-ABOVEOF6-ROBOT))
(not (NOT-ABOVEOF5-ROBOT))
(not (NOT-ABOVEOF4-ROBOT))
(not (NOT-ABOVEOF3-ROBOT))
(not (NOT-ABOVEOF2-ROBOT))
)
)
(when
(and
(ABOVEOF5-ROBOT)
)
(and
(ABOVEOF6-ROBOT)
(ABOVEOF4-ROBOT)
(ABOVEOF3-ROBOT)
(ABOVEOF2-ROBOT)
(not (NOT-ABOVEOF6-ROBOT))
(not (NOT-ABOVEOF5-ROBOT))
(not (NOT-ABOVEOF4-ROBOT))
(not (NOT-ABOVEOF3-ROBOT))
(not (NOT-ABOVEOF2-ROBOT))
)
)
(when
(and
(ABOVEOF4-ROBOT)
)
(and
(ABOVEOF5-ROBOT)
(ABOVEOF3-ROBOT)
(ABOVEOF2-ROBOT)
(not (NOT-ABOVEOF5-ROBOT))
(not (NOT-ABOVEOF4-ROBOT))
(not (NOT-ABOVEOF3-ROBOT))
(not (NOT-ABOVEOF2-ROBOT))
)
)
(when
(and
(ABOVEOF3-ROBOT)
)
(and
(ABOVEOF4-ROBOT)
(ABOVEOF2-ROBOT)
(not (NOT-ABOVEOF4-ROBOT))
(not (NOT-ABOVEOF3-ROBOT))
(not (NOT-ABOVEOF2-ROBOT))
)
)
(when
(and
(ABOVEOF2-ROBOT)
)
(and
(ABOVEOF3-ROBOT)
(not (NOT-ABOVEOF3-ROBOT))
(not (NOT-ABOVEOF2-ROBOT))
)
)
(when
(and
(ABOVEOF1-ROBOT)
)
(and
(ABOVEOF2-ROBOT)
(not (NOT-ABOVEOF2-ROBOT))
)
)
(ABOVEOF1-ROBOT)
(CHECKCONSISTENCY-)
(not (NOT-ABOVEOF1-ROBOT))
(not (NOT-CHECKCONSISTENCY-))
(not (Flag238prime-))
(not (Flag135prime-))
(not (Flag134prime-))
(not (Flag133prime-))
(not (Flag132prime-))
(not (Flag15prime-))
(not (Flag14prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag241prime-))
(not (Flag121prime-))
(not (Flag119prime-))
(not (Flag118prime-))
(not (Flag116prime-))
(not (Flag115prime-))
(not (Flag114prime-))
(not (Flag113prime-))
(not (Flag111prime-))
(not (Flag110prime-))
(not (Flag109prime-))
(not (Flag108prime-))
(not (Flag107prime-))
(not (Flag105prime-))
(not (Flag104prime-))
(not (Flag103prime-))
(not (Flag102prime-))
(not (Flag101prime-))
(not (Flag100prime-))
(not (Flag98prime-))
(not (Flag97prime-))
(not (Flag96prime-))
(not (Flag95prime-))
(not (Flag94prime-))
(not (Flag93prime-))
(not (Flag92prime-))
(not (Flag91prime-))
(not (Flag89prime-))
(not (Flag88prime-))
(not (Flag87prime-))
(not (Flag86prime-))
(not (Flag85prime-))
(not (Flag84prime-))
(not (Flag83prime-))
(not (Flag82prime-))
(not (Flag80prime-))
(not (Flag79prime-))
(not (Flag78prime-))
(not (Flag77prime-))
(not (Flag76prime-))
(not (Flag75prime-))
(not (Flag74prime-))
(not (Flag73prime-))
(not (Flag71prime-))
(not (Flag70prime-))
(not (Flag69prime-))
(not (Flag68prime-))
(not (Flag67prime-))
(not (Flag66prime-))
(not (Flag65prime-))
(not (Flag64prime-))
(not (Flag63prime-))
(not (Flag62prime-))
(not (Flag61prime-))
(not (Flag59prime-))
(not (Flag58prime-))
(not (Flag57prime-))
(not (Flag56prime-))
(not (Flag55prime-))
(not (Flag54prime-))
(not (Flag53prime-))
(not (Flag52prime-))
(not (Flag51prime-))
(not (Flag50prime-))
(not (Flag49prime-))
(not (Flag47prime-))
(not (Flag46prime-))
(not (Flag45prime-))
(not (Flag44prime-))
(not (Flag43prime-))
(not (Flag42prime-))
(not (Flag41prime-))
(not (Flag40prime-))
(not (Flag39prime-))
(not (Flag38prime-))
(not (Flag37prime-))
(not (Flag36prime-))
(not (Flag35prime-))
(not (Flag34prime-))
(not (Flag33prime-))
(not (Flag32prime-))
(not (Flag31prime-))
(not (Flag30prime-))
(not (Flag29prime-))
(not (Flag28prime-))
(not (Flag27prime-))
(not (Flag26prime-))
(not (Flag25prime-))
(not (Flag24prime-))
(not (Flag23prime-))
(not (Flag22prime-))
(not (Flag21prime-))
(not (Flag20prime-))
(not (Flag19prime-))
(not (Flag18prime-))
(not (Flag17prime-))
(not (Flag16prime-))
(not (Flag13prime-))
(not (Flag12prime-))
(not (Flag11prime-))
(not (Flag10prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag239prime-))
(not (Flag236prime-))
(not (Flag234prime-))
(not (Flag233prime-))
(not (Flag231prime-))
(not (Flag230prime-))
(not (Flag229prime-))
(not (Flag227prime-))
(not (Flag226prime-))
(not (Flag225prime-))
(not (Flag224prime-))
(not (Flag222prime-))
(not (Flag221prime-))
(not (Flag220prime-))
(not (Flag219prime-))
(not (Flag218prime-))
(not (Flag217prime-))
(not (Flag215prime-))
(not (Flag214prime-))
(not (Flag213prime-))
(not (Flag212prime-))
(not (Flag211prime-))
(not (Flag210prime-))
(not (Flag209prime-))
(not (Flag207prime-))
(not (Flag206prime-))
(not (Flag205prime-))
(not (Flag204prime-))
(not (Flag203prime-))
(not (Flag202prime-))
(not (Flag201prime-))
(not (Flag199prime-))
(not (Flag198prime-))
(not (Flag197prime-))
(not (Flag196prime-))
(not (Flag195prime-))
(not (Flag194prime-))
(not (Flag193prime-))
(not (Flag192prime-))
(not (Flag191prime-))
(not (Flag190prime-))
(not (Flag188prime-))
(not (Flag187prime-))
(not (Flag186prime-))
(not (Flag185prime-))
(not (Flag184prime-))
(not (Flag183prime-))
(not (Flag182prime-))
(not (Flag181prime-))
(not (Flag180prime-))
(not (Flag179prime-))
(not (Flag177prime-))
(not (Flag176prime-))
(not (Flag175prime-))
(not (Flag174prime-))
(not (Flag173prime-))
(not (Flag172prime-))
(not (Flag171prime-))
(not (Flag170prime-))
(not (Flag169prime-))
(not (Flag168prime-))
(not (Flag166prime-))
(not (Flag165prime-))
(not (Flag164prime-))
(not (Flag163prime-))
(not (Flag162prime-))
(not (Flag161prime-))
(not (Flag160prime-))
(not (Flag159prime-))
(not (Flag158prime-))
(not (Flag157prime-))
(not (Flag156prime-))
(not (Flag155prime-))
(not (Flag154prime-))
(not (Flag153prime-))
(not (Flag152prime-))
(not (Flag151prime-))
(not (Flag150prime-))
(not (Flag149prime-))
(not (Flag148prime-))
(not (Flag147prime-))
(not (Flag146prime-))
(not (Flag145prime-))
(not (Flag144prime-))
(not (Flag143prime-))
(not (Flag142prime-))
(not (Flag141prime-))
(not (Flag140prime-))
(not (Flag139prime-))
(not (Flag138prime-))
(not (Flag137prime-))
(not (Flag136prime-))
(not (Flag131prime-))
(not (Flag130prime-))
(not (Flag129prime-))
(not (Flag128prime-))
(not (Flag127prime-))
(not (Flag126prime-))
(not (Flag125prime-))
(not (Flag124prime-))
(not (Flag123prime-))
(not (Flag122prime-))
(not (Flag120prime-))
(not (Flag117prime-))
(not (Flag112prime-))
(not (Flag106prime-))
(not (Flag99prime-))
(not (Flag90prime-))
(not (Flag81prime-))
(not (Flag72prime-))
(not (Flag60prime-))
(not (Flag48prime-))
(not (Flag1prime-))
(not (Flag240prime-))
(not (Flag237prime-))
(not (Flag235prime-))
(not (Flag232prime-))
(not (Flag228prime-))
(not (Flag223prime-))
(not (Flag216prime-))
(not (Flag208prime-))
(not (Flag200prime-))
(not (Flag189prime-))
(not (Flag178prime-))
(not (Flag167prime-))
)
)
(:action MOVEDOWN-ROBOT
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(Flag122prime-)
(Flag120prime-)
(Flag117prime-)
(Flag112prime-)
(Flag106prime-)
(Flag99prime-)
(Flag90prime-)
(Flag81prime-)
(Flag72prime-)
(Flag60prime-)
(Flag48prime-)
(Flag25prime-)
(Flag24prime-)
(Flag23prime-)
(Flag22prime-)
(Flag21prime-)
(Flag20prime-)
(Flag19prime-)
(Flag18prime-)
(Flag17prime-)
(Flag16prime-)
(Flag15prime-)
(Flag14prime-)
(Flag13prime-)
(Flag12prime-)
(Flag11prime-)
(Flag10prime-)
(Flag9prime-)
(Flag8prime-)
(Flag7prime-)
(Flag6prime-)
(Flag5prime-)
)
:effect
(and
(when
(and
(Flag122-)
)
(and
(ROW10-ROBOT)
(not (NOT-ROW10-ROBOT))
)
)
(when
(and
(Flag120-)
)
(and
(ROW9-ROBOT)
(not (NOT-ROW9-ROBOT))
)
)
(when
(and
(Flag117-)
)
(and
(ROW8-ROBOT)
(not (NOT-ROW8-ROBOT))
)
)
(when
(and
(Flag112-)
)
(and
(ROW7-ROBOT)
(not (NOT-ROW7-ROBOT))
)
)
(when
(and
(Flag106-)
)
(and
(ROW6-ROBOT)
(not (NOT-ROW6-ROBOT))
)
)
(when
(and
(Flag99-)
)
(and
(ROW5-ROBOT)
(not (NOT-ROW5-ROBOT))
)
)
(when
(and
(Flag90-)
)
(and
(ROW4-ROBOT)
(not (NOT-ROW4-ROBOT))
)
)
(when
(and
(Flag81-)
)
(and
(ROW3-ROBOT)
(not (NOT-ROW3-ROBOT))
)
)
(when
(and
(Flag72-)
)
(and
(ROW2-ROBOT)
(not (NOT-ROW2-ROBOT))
)
)
(when
(and
(Flag60-)
)
(and
(ROW1-ROBOT)
(not (NOT-ROW1-ROBOT))
)
)
(when
(and
(Flag48-)
)
(and
(ROW0-ROBOT)
(not (NOT-ROW0-ROBOT))
)
)
(when
(and
(ROW11-ROBOT)
)
(and
(ROW10-ROBOT)
(NOT-ROW11-ROBOT)
(not (NOT-ROW10-ROBOT))
(not (ROW11-ROBOT))
)
)
(when
(and
(ROW10-ROBOT)
)
(and
(ROW9-ROBOT)
(NOT-ROW10-ROBOT)
(not (NOT-ROW9-ROBOT))
(not (ROW10-ROBOT))
)
)
(when
(and
(ROW9-ROBOT)
)
(and
(ROW8-ROBOT)
(NOT-ROW9-ROBOT)
(not (NOT-ROW8-ROBOT))
(not (ROW9-ROBOT))
)
)
(when
(and
(ROW8-ROBOT)
)
(and
(ROW7-ROBOT)
(NOT-ROW8-ROBOT)
(not (NOT-ROW7-ROBOT))
(not (ROW8-ROBOT))
)
)
(when
(and
(ROW7-ROBOT)
)
(and
(ROW6-ROBOT)
(NOT-ROW7-ROBOT)
(not (NOT-ROW6-ROBOT))
(not (ROW7-ROBOT))
)
)
(when
(and
(ROW6-ROBOT)
)
(and
(ROW5-ROBOT)
(NOT-ROW6-ROBOT)
(not (NOT-ROW5-ROBOT))
(not (ROW6-ROBOT))
)
)
(when
(and
(ROW5-ROBOT)
)
(and
(ROW4-ROBOT)
(NOT-ROW5-ROBOT)
(not (NOT-ROW4-ROBOT))
(not (ROW5-ROBOT))
)
)
(when
(and
(ROW4-ROBOT)
)
(and
(ROW3-ROBOT)
(NOT-ROW4-ROBOT)
(not (NOT-ROW3-ROBOT))
(not (ROW4-ROBOT))
)
)
(when
(and
(ROW3-ROBOT)
)
(and
(ROW2-ROBOT)
(NOT-ROW3-ROBOT)
(not (NOT-ROW2-ROBOT))
(not (ROW3-ROBOT))
)
)
(when
(and
(ROW2-ROBOT)
)
(and
(ROW1-ROBOT)
(NOT-ROW2-ROBOT)
(not (NOT-ROW1-ROBOT))
(not (ROW2-ROBOT))
)
)
(when
(and
(ROW1-ROBOT)
)
(and
(ROW0-ROBOT)
(NOT-ROW1-ROBOT)
(not (NOT-ROW0-ROBOT))
(not (ROW1-ROBOT))
)
)
(when
(and
(ABOVEOF11-ROBOT)
)
(and
(ABOVEOF10-ROBOT)
(NOT-ABOVEOF11-ROBOT)
(not (NOT-ABOVEOF10-ROBOT))
(not (ABOVEOF11-ROBOT))
)
)
(when
(and
(Flag25-)
)
(and
(ABOVEOF9-ROBOT)
(NOT-ABOVEOF10-ROBOT)
(not (NOT-ABOVEOF9-ROBOT))
(not (ABOVEOF10-ROBOT))
)
)
(when
(and
(Flag24-)
)
(and
(ABOVEOF8-ROBOT)
(NOT-ABOVEOF9-ROBOT)
(not (NOT-ABOVEOF8-ROBOT))
(not (ABOVEOF9-ROBOT))
)
)
(when
(and
(Flag23-)
)
(and
(ABOVEOF7-ROBOT)
(NOT-ABOVEOF8-ROBOT)
(not (NOT-ABOVEOF7-ROBOT))
(not (ABOVEOF8-ROBOT))
)
)
(when
(and
(Flag22-)
)
(and
(ABOVEOF6-ROBOT)
(NOT-ABOVEOF7-ROBOT)
(not (NOT-ABOVEOF6-ROBOT))
(not (ABOVEOF7-ROBOT))
)
)
(when
(and
(Flag21-)
)
(and
(ABOVEOF5-ROBOT)
(NOT-ABOVEOF6-ROBOT)
(not (NOT-ABOVEOF5-ROBOT))
(not (ABOVEOF6-ROBOT))
)
)
(when
(and
(Flag20-)
)
(and
(ABOVEOF4-ROBOT)
(NOT-ABOVEOF5-ROBOT)
(not (NOT-ABOVEOF4-ROBOT))
(not (ABOVEOF5-ROBOT))
)
)
(when
(and
(Flag19-)
)
(and
(ABOVEOF3-ROBOT)
(NOT-ABOVEOF4-ROBOT)
(not (NOT-ABOVEOF3-ROBOT))
(not (ABOVEOF4-ROBOT))
)
)
(when
(and
(Flag18-)
)
(and
(ABOVEOF2-ROBOT)
(NOT-ABOVEOF3-ROBOT)
(not (NOT-ABOVEOF2-ROBOT))
(not (ABOVEOF3-ROBOT))
)
)
(when
(and
(Flag17-)
)
(and
(ABOVEOF1-ROBOT)
(NOT-ABOVEOF2-ROBOT)
(not (NOT-ABOVEOF1-ROBOT))
(not (ABOVEOF2-ROBOT))
)
)
(when
(and
(Flag16-)
)
(and
(NOT-ABOVEOF1-ROBOT)
(not (ABOVEOF1-ROBOT))
)
)
(when
(and
(Flag15-)
)
(and
(BELOWOF11-ROBOT)
(not (NOT-BELOWOF11-ROBOT))
)
)
(when
(and
(Flag14-)
)
(and
(BELOWOF10-ROBOT)
(not (NOT-BELOWOF10-ROBOT))
)
)
(when
(and
(Flag13-)
)
(and
(BELOWOF9-ROBOT)
(not (NOT-BELOWOF9-ROBOT))
)
)
(when
(and
(Flag12-)
)
(and
(BELOWOF8-ROBOT)
(not (NOT-BELOWOF8-ROBOT))
)
)
(when
(and
(Flag11-)
)
(and
(BELOWOF7-ROBOT)
(not (NOT-BELOWOF7-ROBOT))
)
)
(when
(and
(Flag10-)
)
(and
(BELOWOF6-ROBOT)
(not (NOT-BELOWOF6-ROBOT))
)
)
(when
(and
(Flag9-)
)
(and
(BELOWOF5-ROBOT)
(not (NOT-BELOWOF5-ROBOT))
)
)
(when
(and
(Flag8-)
)
(and
(BELOWOF4-ROBOT)
(not (NOT-BELOWOF4-ROBOT))
)
)
(when
(and
(Flag7-)
)
(and
(BELOWOF3-ROBOT)
(not (NOT-BELOWOF3-ROBOT))
)
)
(when
(and
(Flag6-)
)
(and
(BELOWOF2-ROBOT)
(not (NOT-BELOWOF2-ROBOT))
)
)
(when
(and
(Flag5-)
)
(and
(BELOWOF1-ROBOT)
(not (NOT-BELOWOF1-ROBOT))
)
)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag238prime-))
(not (Flag135prime-))
(not (Flag134prime-))
(not (Flag133prime-))
(not (Flag132prime-))
(not (Flag15prime-))
(not (Flag14prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag241prime-))
(not (Flag121prime-))
(not (Flag119prime-))
(not (Flag118prime-))
(not (Flag116prime-))
(not (Flag115prime-))
(not (Flag114prime-))
(not (Flag113prime-))
(not (Flag111prime-))
(not (Flag110prime-))
(not (Flag109prime-))
(not (Flag108prime-))
(not (Flag107prime-))
(not (Flag105prime-))
(not (Flag104prime-))
(not (Flag103prime-))
(not (Flag102prime-))
(not (Flag101prime-))
(not (Flag100prime-))
(not (Flag98prime-))
(not (Flag97prime-))
(not (Flag96prime-))
(not (Flag95prime-))
(not (Flag94prime-))
(not (Flag93prime-))
(not (Flag92prime-))
(not (Flag91prime-))
(not (Flag89prime-))
(not (Flag88prime-))
(not (Flag87prime-))
(not (Flag86prime-))
(not (Flag85prime-))
(not (Flag84prime-))
(not (Flag83prime-))
(not (Flag82prime-))
(not (Flag80prime-))
(not (Flag79prime-))
(not (Flag78prime-))
(not (Flag77prime-))
(not (Flag76prime-))
(not (Flag75prime-))
(not (Flag74prime-))
(not (Flag73prime-))
(not (Flag71prime-))
(not (Flag70prime-))
(not (Flag69prime-))
(not (Flag68prime-))
(not (Flag67prime-))
(not (Flag66prime-))
(not (Flag65prime-))
(not (Flag64prime-))
(not (Flag63prime-))
(not (Flag62prime-))
(not (Flag61prime-))
(not (Flag59prime-))
(not (Flag58prime-))
(not (Flag57prime-))
(not (Flag56prime-))
(not (Flag55prime-))
(not (Flag54prime-))
(not (Flag53prime-))
(not (Flag52prime-))
(not (Flag51prime-))
(not (Flag50prime-))
(not (Flag49prime-))
(not (Flag47prime-))
(not (Flag46prime-))
(not (Flag45prime-))
(not (Flag44prime-))
(not (Flag43prime-))
(not (Flag42prime-))
(not (Flag41prime-))
(not (Flag40prime-))
(not (Flag39prime-))
(not (Flag38prime-))
(not (Flag37prime-))
(not (Flag36prime-))
(not (Flag35prime-))
(not (Flag34prime-))
(not (Flag33prime-))
(not (Flag32prime-))
(not (Flag31prime-))
(not (Flag30prime-))
(not (Flag29prime-))
(not (Flag28prime-))
(not (Flag27prime-))
(not (Flag26prime-))
(not (Flag25prime-))
(not (Flag24prime-))
(not (Flag23prime-))
(not (Flag22prime-))
(not (Flag21prime-))
(not (Flag20prime-))
(not (Flag19prime-))
(not (Flag18prime-))
(not (Flag17prime-))
(not (Flag16prime-))
(not (Flag13prime-))
(not (Flag12prime-))
(not (Flag11prime-))
(not (Flag10prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag239prime-))
(not (Flag236prime-))
(not (Flag234prime-))
(not (Flag233prime-))
(not (Flag231prime-))
(not (Flag230prime-))
(not (Flag229prime-))
(not (Flag227prime-))
(not (Flag226prime-))
(not (Flag225prime-))
(not (Flag224prime-))
(not (Flag222prime-))
(not (Flag221prime-))
(not (Flag220prime-))
(not (Flag219prime-))
(not (Flag218prime-))
(not (Flag217prime-))
(not (Flag215prime-))
(not (Flag214prime-))
(not (Flag213prime-))
(not (Flag212prime-))
(not (Flag211prime-))
(not (Flag210prime-))
(not (Flag209prime-))
(not (Flag207prime-))
(not (Flag206prime-))
(not (Flag205prime-))
(not (Flag204prime-))
(not (Flag203prime-))
(not (Flag202prime-))
(not (Flag201prime-))
(not (Flag199prime-))
(not (Flag198prime-))
(not (Flag197prime-))
(not (Flag196prime-))
(not (Flag195prime-))
(not (Flag194prime-))
(not (Flag193prime-))
(not (Flag192prime-))
(not (Flag191prime-))
(not (Flag190prime-))
(not (Flag188prime-))
(not (Flag187prime-))
(not (Flag186prime-))
(not (Flag185prime-))
(not (Flag184prime-))
(not (Flag183prime-))
(not (Flag182prime-))
(not (Flag181prime-))
(not (Flag180prime-))
(not (Flag179prime-))
(not (Flag177prime-))
(not (Flag176prime-))
(not (Flag175prime-))
(not (Flag174prime-))
(not (Flag173prime-))
(not (Flag172prime-))
(not (Flag171prime-))
(not (Flag170prime-))
(not (Flag169prime-))
(not (Flag168prime-))
(not (Flag166prime-))
(not (Flag165prime-))
(not (Flag164prime-))
(not (Flag163prime-))
(not (Flag162prime-))
(not (Flag161prime-))
(not (Flag160prime-))
(not (Flag159prime-))
(not (Flag158prime-))
(not (Flag157prime-))
(not (Flag156prime-))
(not (Flag155prime-))
(not (Flag154prime-))
(not (Flag153prime-))
(not (Flag152prime-))
(not (Flag151prime-))
(not (Flag150prime-))
(not (Flag149prime-))
(not (Flag148prime-))
(not (Flag147prime-))
(not (Flag146prime-))
(not (Flag145prime-))
(not (Flag144prime-))
(not (Flag143prime-))
(not (Flag142prime-))
(not (Flag141prime-))
(not (Flag140prime-))
(not (Flag139prime-))
(not (Flag138prime-))
(not (Flag137prime-))
(not (Flag136prime-))
(not (Flag131prime-))
(not (Flag130prime-))
(not (Flag129prime-))
(not (Flag128prime-))
(not (Flag127prime-))
(not (Flag126prime-))
(not (Flag125prime-))
(not (Flag124prime-))
(not (Flag123prime-))
(not (Flag122prime-))
(not (Flag120prime-))
(not (Flag117prime-))
(not (Flag112prime-))
(not (Flag106prime-))
(not (Flag99prime-))
(not (Flag90prime-))
(not (Flag81prime-))
(not (Flag72prime-))
(not (Flag60prime-))
(not (Flag48prime-))
(not (Flag1prime-))
(not (Flag240prime-))
(not (Flag237prime-))
(not (Flag235prime-))
(not (Flag232prime-))
(not (Flag228prime-))
(not (Flag223prime-))
(not (Flag216prime-))
(not (Flag208prime-))
(not (Flag200prime-))
(not (Flag189prime-))
(not (Flag178prime-))
(not (Flag167prime-))
)
)
(:action Prim1Action-2
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
(Flag1prime-)
(not (Flag1-))
)

)
(:action Prim1Action-3
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
(Flag1prime-)
(not (Flag1-))
)

)
(:action Flag2Action
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
)
:effect
(and
(Flag2-)
(Flag2prime-)
)

)
(:action Prim2Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
(Flag2prime-)
(not (Flag2-))
)

)
(:action Prim2Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
(Flag2prime-)
(not (Flag2-))
)

)
(:action Flag3Action-5
:parameters ()
:precondition
(and
(RIGHTOF2-ROBOT)
)
:effect
(and
(Flag3-)
(Flag3prime-)
)

)
(:action Flag3Action-16
:parameters ()
:precondition
(and
(LEFTOF11-ROBOT)
)
:effect
(and
(Flag3-)
(Flag3prime-)
)

)
(:action Prim4Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
(Flag4prime-)
(not (Flag4-))
)

)
(:action Prim4Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
(Flag4prime-)
(not (Flag4-))
)

)
(:action Flag14Action-1
:parameters ()
:precondition
(and
(BELOWOF11-ROBOT)
)
:effect
(and
(Flag14-)
(Flag14prime-)
)

)
(:action Flag15Action-1
:parameters ()
:precondition
(and
(BELOWOF11-ROBOT)
)
:effect
(and
(Flag15-)
(Flag15prime-)
)

)
(:action Prim118Action-0
:parameters ()
:precondition
(and
(not (BELOWOF11-ROBOT))
)
:effect
(and
(Flag118prime-)
(not (Flag118-))
)

)
(:action Prim119Action-1
:parameters ()
:precondition
(and
(not (BELOWOF11-ROBOT))
)
:effect
(and
(Flag119prime-)
(not (Flag119-))
)

)
(:action Flag132Action-1
:parameters ()
:precondition
(and
(LEFTOF11-ROBOT)
)
:effect
(and
(Flag132-)
(Flag132prime-)
)

)
(:action Flag133Action-1
:parameters ()
:precondition
(and
(LEFTOF11-ROBOT)
)
:effect
(and
(Flag133-)
(Flag133prime-)
)

)
(:action Flag134Action-3
:parameters ()
:precondition
(and
(RIGHTOF2-ROBOT)
)
:effect
(and
(Flag134-)
(Flag134prime-)
)

)
(:action Flag135Action-2
:parameters ()
:precondition
(and
(RIGHTOF2-ROBOT)
)
:effect
(and
(Flag135-)
(Flag135prime-)
)

)
(:action Prim162Action-0
:parameters ()
:precondition
(and
(not (RIGHTOF2-ROBOT))
)
:effect
(and
(Flag162prime-)
(not (Flag162-))
)

)
(:action Prim163Action-1
:parameters ()
:precondition
(and
(not (RIGHTOF2-ROBOT))
)
:effect
(and
(Flag163prime-)
(not (Flag163-))
)

)
(:action Prim177Action-1
:parameters ()
:precondition
(and
(not (RIGHTOF2-ROBOT))
)
:effect
(and
(Flag177prime-)
(not (Flag177-))
)

)
(:action Prim233Action-1
:parameters ()
:precondition
(and
(not (LEFTOF11-ROBOT))
)
:effect
(and
(Flag233prime-)
(not (Flag233-))
)

)
(:action Prim234Action-1
:parameters ()
:precondition
(and
(not (LEFTOF11-ROBOT))
)
:effect
(and
(Flag234prime-)
(not (Flag234-))
)

)
(:action Flag238Action-4
:parameters ()
:precondition
(and
(RIGHTOF2-ROBOT)
)
:effect
(and
(Flag238-)
(Flag238prime-)
)

)
)
