(define (domain grounded-SIMPLE-ADL-ROBOT)
(:requirements
:strips
)
(:predicates
(Flag26-)
(Flag19-)
(Flag15-)
(Flag12-)
(Flag7-)
(Flag6-)
(Flag5-)
(Flag3-)
(Flag2-)
(ROW2-ROBOT)
(ROW1-ROBOT)
(BELOWOF3-ROBOT)
(NOT-BELOWOF2-ROBOT)
(ABOVEOF2-ROBOT)
(ABOVEOF1-ROBOT)
(CHECKCONSISTENCY-)
(ROW0-ROBOT)
(BELOWOF1-ROBOT)
(Flag28-)
(Flag27-)
(Flag24-)
(Flag23-)
(Flag22-)
(Flag21-)
(Flag20-)
(Flag18-)
(Flag17-)
(Flag14-)
(Flag4-)
(ERROR-)
(COLUMN2-ROBOT)
(COLUMN1-ROBOT)
(LEFTOF3-ROBOT)
(NOT-LEFTOF2-ROBOT)
(RIGHTOF1-ROBOT)
(COLUMN0-ROBOT)
(NOT-RIGHTOF2-ROBOT)
(RIGHTOF0-ROBOT)
(LEFTOF1-ROBOT)
(Flag29-)
(Flag25-)
(Flag16-)
(Flag13-)
(Flag10-)
(Flag9-)
(Flag8-)
(Flag1-)
(Flag11-)
(NOT-COLUMN1-ROBOT)
(NOT-COLUMN0-ROBOT)
(NOT-RIGHTOF1-ROBOT)
(RIGHTOF2-ROBOT)
(NOT-RIGHTOF0-ROBOT)
(NOT-LEFTOF1-ROBOT)
(NOT-CHECKCONSISTENCY-)
(NOT-COLUMN2-ROBOT)
(NOT-LEFTOF3-ROBOT)
(LEFTOF2-ROBOT)
(NOT-ERROR-)
(NOT-ROW1-ROBOT)
(NOT-ROW0-ROBOT)
(NOT-ABOVEOF1-ROBOT)
(NOT-BELOWOF1-ROBOT)
(NOT-ROW2-ROBOT)
(NOT-BELOWOF3-ROBOT)
(BELOWOF2-ROBOT)
(NOT-ABOVEOF2-ROBOT)
(Flag26prime-)
(Flag19prime-)
(Flag15prime-)
(Flag12prime-)
(Flag7prime-)
(Flag6prime-)
(Flag5prime-)
(Flag3prime-)
(Flag2prime-)
(Flag28prime-)
(Flag27prime-)
(Flag24prime-)
(Flag23prime-)
(Flag22prime-)
(Flag21prime-)
(Flag20prime-)
(Flag18prime-)
(Flag17prime-)
(Flag14prime-)
(Flag4prime-)
(Flag29prime-)
(Flag25prime-)
(Flag16prime-)
(Flag13prime-)
(Flag10prime-)
(Flag9prime-)
(Flag8prime-)
(Flag1prime-)
(Flag11prime-)
)
(:action Flag11Action-0
:parameters ()
:precondition
(and
(Flag8-)
(Flag8prime-)
)
:effect
(and
(Flag11-)
(Flag11prime-)
)

)
(:action Flag11Action-1
:parameters ()
:precondition
(and
(Flag9-)
(Flag9prime-)
)
:effect
(and
(Flag11-)
(Flag11prime-)
)

)
(:action Flag11Action-2
:parameters ()
:precondition
(and
(Flag10-)
(Flag10prime-)
)
:effect
(and
(Flag11-)
(Flag11prime-)
)

)
(:action Prim11Action
:parameters ()
:precondition
(and
(not (Flag8-))
(Flag8prime-)
(not (Flag9-))
(Flag9prime-)
(not (Flag10-))
(Flag10prime-)
)
:effect
(and
(Flag11prime-)
(not (Flag11-))
)

)
(:action Flag14Action-1
:parameters ()
:precondition
(and
(Flag9-)
(Flag9prime-)
)
:effect
(and
(Flag14-)
(Flag14prime-)
)

)
(:action Flag14Action-2
:parameters ()
:precondition
(and
(Flag10-)
(Flag10prime-)
)
:effect
(and
(Flag14-)
(Flag14prime-)
)

)
(:action Flag14Action-3
:parameters ()
:precondition
(and
(Flag13-)
(Flag13prime-)
)
:effect
(and
(Flag14-)
(Flag14prime-)
)

)
(:action Prim14Action
:parameters ()
:precondition
(and
(not (Flag12-))
(Flag12prime-)
(not (Flag9-))
(Flag9prime-)
(not (Flag10-))
(Flag10prime-)
(not (Flag13-))
(Flag13prime-)
)
:effect
(and
(Flag14prime-)
(not (Flag14-))
)

)
(:action Flag17Action-1
:parameters ()
:precondition
(and
(Flag10-)
(Flag10prime-)
)
:effect
(and
(Flag17-)
(Flag17prime-)
)

)
(:action Flag17Action-2
:parameters ()
:precondition
(and
(Flag16-)
(Flag16prime-)
)
:effect
(and
(Flag17-)
(Flag17prime-)
)

)
(:action Prim17Action
:parameters ()
:precondition
(and
(not (Flag12-))
(Flag12prime-)
(not (Flag10-))
(Flag10prime-)
(not (Flag16-))
(Flag16prime-)
)
:effect
(and
(Flag17prime-)
(not (Flag17-))
)

)
(:action Flag1Action
:parameters ()
:precondition
(and
(COLUMN2-ROBOT)
(ROW1-ROBOT)
(NOT-ERROR-)
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(Flag1-)
(Flag1prime-)
)

)
(:action Prim1Action-0
:parameters ()
:precondition
(and
(not (COLUMN2-ROBOT))
)
:effect
(and
(Flag1prime-)
(not (Flag1-))
)

)
(:action Flag3Action-0
:parameters ()
:precondition
(and
(LEFTOF3-ROBOT)
)
:effect
(and
(Flag3-)
(Flag3prime-)
)

)
(:action Flag3Action-1
:parameters ()
:precondition
(and
(RIGHTOF1-ROBOT)
)
:effect
(and
(Flag3-)
(Flag3prime-)
)

)
(:action Flag3Action-2
:parameters ()
:precondition
(and
(LEFTOF1-ROBOT)
)
:effect
(and
(Flag3-)
(Flag3prime-)
)

)
(:action Flag3Action-5
:parameters ()
:precondition
(and
(RIGHTOF0-ROBOT)
)
:effect
(and
(Flag3-)
(Flag3prime-)
)

)
(:action Prim3Action
:parameters ()
:precondition
(and
(not (LEFTOF3-ROBOT))
(not (RIGHTOF1-ROBOT))
(not (LEFTOF1-ROBOT))
(not (LEFTOF2-ROBOT))
(not (RIGHTOF2-ROBOT))
(not (RIGHTOF0-ROBOT))
)
:effect
(and
(Flag3prime-)
(not (Flag3-))
)

)
(:action Flag5Action-0
:parameters ()
:precondition
(and
(RIGHTOF0-ROBOT)
)
:effect
(and
(Flag5-)
(Flag5prime-)
)

)
(:action Flag5Action-1
:parameters ()
:precondition
(and
(RIGHTOF1-ROBOT)
)
:effect
(and
(Flag5-)
(Flag5prime-)
)

)
(:action Prim5Action
:parameters ()
:precondition
(and
(not (RIGHTOF0-ROBOT))
(not (RIGHTOF1-ROBOT))
(not (RIGHTOF2-ROBOT))
)
:effect
(and
(Flag5prime-)
(not (Flag5-))
)

)
(:action Flag6Action-0
:parameters ()
:precondition
(and
(RIGHTOF1-ROBOT)
)
:effect
(and
(Flag6-)
(Flag6prime-)
)

)
(:action Prim6Action
:parameters ()
:precondition
(and
(not (RIGHTOF1-ROBOT))
(not (RIGHTOF2-ROBOT))
)
:effect
(and
(Flag6prime-)
(not (Flag6-))
)

)
(:action Flag7Action-0
:parameters ()
:precondition
(and
(LEFTOF1-ROBOT)
)
:effect
(and
(Flag7-)
(Flag7prime-)
)

)
(:action Prim7Action
:parameters ()
:precondition
(and
(not (LEFTOF1-ROBOT))
(not (LEFTOF2-ROBOT))
)
:effect
(and
(Flag7prime-)
(not (Flag7-))
)

)
(:action Flag8Action
:parameters ()
:precondition
(and
(RIGHTOF0-ROBOT)
(LEFTOF1-ROBOT)
)
:effect
(and
(Flag8-)
(Flag8prime-)
)

)
(:action Flag9Action
:parameters ()
:precondition
(and
(LEFTOF1-ROBOT)
(RIGHTOF1-ROBOT)
)
:effect
(and
(Flag9-)
(Flag9prime-)
)

)
(:action Flag10Action
:parameters ()
:precondition
(and
(LEFTOF1-ROBOT)
(RIGHTOF2-ROBOT)
)
:effect
(and
(Flag10-)
(Flag10prime-)
)

)
(:action Prim8Action-0
:parameters ()
:precondition
(and
(not (RIGHTOF0-ROBOT))
)
:effect
(and
(Flag8prime-)
(not (Flag8-))
)

)
(:action Prim8Action-1
:parameters ()
:precondition
(and
(not (LEFTOF1-ROBOT))
)
:effect
(and
(Flag8prime-)
(not (Flag8-))
)

)
(:action Prim9Action-0
:parameters ()
:precondition
(and
(not (LEFTOF1-ROBOT))
)
:effect
(and
(Flag9prime-)
(not (Flag9-))
)

)
(:action Prim9Action-1
:parameters ()
:precondition
(and
(not (RIGHTOF1-ROBOT))
)
:effect
(and
(Flag9prime-)
(not (Flag9-))
)

)
(:action Prim10Action-0
:parameters ()
:precondition
(and
(not (LEFTOF1-ROBOT))
)
:effect
(and
(Flag10prime-)
(not (Flag10-))
)

)
(:action Flag13Action
:parameters ()
:precondition
(and
(LEFTOF2-ROBOT)
(RIGHTOF1-ROBOT)
)
:effect
(and
(Flag13-)
(Flag13prime-)
)

)
(:action Prim13Action-1
:parameters ()
:precondition
(and
(not (RIGHTOF1-ROBOT))
)
:effect
(and
(Flag13prime-)
(not (Flag13-))
)

)
(:action Flag15Action-0
:parameters ()
:precondition
(and
(LEFTOF1-ROBOT)
)
:effect
(and
(Flag15-)
(Flag15prime-)
)

)
(:action Flag15Action-1
:parameters ()
:precondition
(and
(LEFTOF3-ROBOT)
)
:effect
(and
(Flag15-)
(Flag15prime-)
)

)
(:action Prim15Action
:parameters ()
:precondition
(and
(not (LEFTOF1-ROBOT))
(not (LEFTOF3-ROBOT))
(not (LEFTOF2-ROBOT))
)
:effect
(and
(Flag15prime-)
(not (Flag15-))
)

)
(:action Flag16Action
:parameters ()
:precondition
(and
(LEFTOF3-ROBOT)
(RIGHTOF2-ROBOT)
)
:effect
(and
(Flag16-)
(Flag16prime-)
)

)
(:action Prim16Action-0
:parameters ()
:precondition
(and
(not (LEFTOF3-ROBOT))
)
:effect
(and
(Flag16prime-)
(not (Flag16-))
)

)
(:action Flag22Action-0
:parameters ()
:precondition
(and
(Flag20-)
(Flag20prime-)
)
:effect
(and
(Flag22-)
(Flag22prime-)
)

)
(:action Flag22Action-2
:parameters ()
:precondition
(and
(Flag21-)
(Flag21prime-)
)
:effect
(and
(Flag22-)
(Flag22prime-)
)

)
(:action Prim22Action
:parameters ()
:precondition
(and
(not (Flag20-))
(Flag20prime-)
(not (BELOWOF1-ROBOT))
(not (Flag21-))
(Flag21prime-)
)
:effect
(and
(Flag22prime-)
(not (Flag22-))
)

)
(:action Flag25Action-0
:parameters ()
:precondition
(and
(Flag20-)
(Flag20prime-)
)
:effect
(and
(Flag25-)
(Flag25prime-)
)

)
(:action Flag25Action-1
:parameters ()
:precondition
(and
(Flag23-)
(Flag23prime-)
)
:effect
(and
(Flag25-)
(Flag25prime-)
)

)
(:action Flag25Action-2
:parameters ()
:precondition
(and
(Flag21-)
(Flag21prime-)
)
:effect
(and
(Flag25-)
(Flag25prime-)
)

)
(:action Flag25Action-3
:parameters ()
:precondition
(and
(Flag24-)
(Flag24prime-)
)
:effect
(and
(Flag25-)
(Flag25prime-)
)

)
(:action Prim25Action
:parameters ()
:precondition
(and
(not (Flag20-))
(Flag20prime-)
(not (Flag23-))
(Flag23prime-)
(not (Flag21-))
(Flag21prime-)
(not (Flag24-))
(Flag24prime-)
)
:effect
(and
(Flag25prime-)
(not (Flag25-))
)

)
(:action Flag29Action-0
:parameters ()
:precondition
(and
(Flag20-)
(Flag20prime-)
)
:effect
(and
(Flag29-)
(Flag29prime-)
)

)
(:action Flag29Action-1
:parameters ()
:precondition
(and
(Flag27-)
(Flag27prime-)
)
:effect
(and
(Flag29-)
(Flag29prime-)
)

)
(:action Flag29Action-2
:parameters ()
:precondition
(and
(Flag28-)
(Flag28prime-)
)
:effect
(and
(Flag29-)
(Flag29prime-)
)

)
(:action Prim29Action
:parameters ()
:precondition
(and
(not (Flag20-))
(Flag20prime-)
(not (Flag27-))
(Flag27prime-)
(not (Flag28-))
(Flag28prime-)
)
:effect
(and
(Flag29prime-)
(not (Flag29-))
)

)
(:action MOVELEFT-ROBOT
:parameters ()
:precondition
(and
(Flag4-)
(Flag4prime-)
(Flag17prime-)
(Flag14prime-)
(Flag15prime-)
)
:effect
(and
(when
(and
(Flag17-)
)
(and
(COLUMN1-ROBOT)
(not (NOT-COLUMN1-ROBOT))
)
)
(when
(and
(Flag14-)
)
(and
(COLUMN0-ROBOT)
(not (NOT-COLUMN0-ROBOT))
)
)
(when
(and
(COLUMN2-ROBOT)
)
(and
(COLUMN1-ROBOT)
(NOT-COLUMN2-ROBOT)
(not (NOT-COLUMN1-ROBOT))
(not (COLUMN2-ROBOT))
)
)
(when
(and
(COLUMN1-ROBOT)
)
(and
(COLUMN0-ROBOT)
(NOT-COLUMN1-ROBOT)
(not (NOT-COLUMN0-ROBOT))
(not (COLUMN1-ROBOT))
)
)
(when
(and
(RIGHTOF2-ROBOT)
)
(and
(RIGHTOF1-ROBOT)
(NOT-RIGHTOF2-ROBOT)
(RIGHTOF0-ROBOT)
(NOT-RIGHTOF1-ROBOT)
(not (RIGHTOF2-ROBOT))
(not (NOT-RIGHTOF0-ROBOT))
)
)
(when
(and
(RIGHTOF1-ROBOT)
)
(and
(RIGHTOF0-ROBOT)
(NOT-RIGHTOF1-ROBOT)
(not (NOT-RIGHTOF0-ROBOT))
(not (RIGHTOF1-ROBOT))
)
)
(when
(and
(Flag15-)
)
(and
(LEFTOF2-ROBOT)
(not (NOT-LEFTOF2-ROBOT))
)
)
(when
(and
(LEFTOF2-ROBOT)
)
(and
(LEFTOF1-ROBOT)
(not (NOT-LEFTOF1-ROBOT))
)
)
(when
(and
(LEFTOF1-ROBOT)
)
(and
(not (NOT-LEFTOF1-ROBOT))
)
)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag26prime-))
(not (Flag19prime-))
(not (Flag15prime-))
(not (Flag12prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag28prime-))
(not (Flag27prime-))
(not (Flag24prime-))
(not (Flag23prime-))
(not (Flag22prime-))
(not (Flag21prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag17prime-))
(not (Flag14prime-))
(not (Flag4prime-))
(not (Flag29prime-))
(not (Flag25prime-))
(not (Flag16prime-))
(not (Flag13prime-))
(not (Flag10prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag1prime-))
(not (Flag11prime-))
)
)
(:action MOVERIGHT-ROBOT
:parameters ()
:precondition
(and
(Flag4-)
(Flag4prime-)
(Flag14prime-)
(Flag11prime-)
(Flag7prime-)
(Flag6prime-)
(Flag5prime-)
)
:effect
(and
(when
(and
(Flag14-)
)
(and
(COLUMN2-ROBOT)
(not (NOT-COLUMN2-ROBOT))
)
)
(when
(and
(Flag11-)
)
(and
(COLUMN1-ROBOT)
(not (NOT-COLUMN1-ROBOT))
)
)
(when
(and
(COLUMN1-ROBOT)
)
(and
(COLUMN2-ROBOT)
(NOT-COLUMN1-ROBOT)
(not (NOT-COLUMN2-ROBOT))
(not (COLUMN1-ROBOT))
)
)
(when
(and
(COLUMN0-ROBOT)
)
(and
(COLUMN1-ROBOT)
(NOT-COLUMN0-ROBOT)
(not (NOT-COLUMN1-ROBOT))
(not (COLUMN0-ROBOT))
)
)
(when
(and
(Flag7-)
)
(and
(LEFTOF3-ROBOT)
(NOT-LEFTOF2-ROBOT)
(not (NOT-LEFTOF3-ROBOT))
(not (LEFTOF2-ROBOT))
)
)
(when
(and
(LEFTOF1-ROBOT)
)
(and
(LEFTOF2-ROBOT)
(NOT-LEFTOF1-ROBOT)
(not (NOT-LEFTOF2-ROBOT))
(not (LEFTOF1-ROBOT))
)
)
(when
(and
(Flag6-)
)
(and
(RIGHTOF2-ROBOT)
(not (NOT-RIGHTOF2-ROBOT))
)
)
(when
(and
(Flag5-)
)
(and
(RIGHTOF1-ROBOT)
(not (NOT-RIGHTOF1-ROBOT))
)
)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag26prime-))
(not (Flag19prime-))
(not (Flag15prime-))
(not (Flag12prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag28prime-))
(not (Flag27prime-))
(not (Flag24prime-))
(not (Flag23prime-))
(not (Flag22prime-))
(not (Flag21prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag17prime-))
(not (Flag14prime-))
(not (Flag4prime-))
(not (Flag29prime-))
(not (Flag25prime-))
(not (Flag16prime-))
(not (Flag13prime-))
(not (Flag10prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag1prime-))
(not (Flag11prime-))
)
)
(:action Prim1Action-1
:parameters ()
:precondition
(and
(not (ROW1-ROBOT))
)
:effect
(and
(Flag1prime-)
(not (Flag1-))
)

)
(:action CHECKCONSISTENCYACTION
:parameters ()
:precondition
(and
(NOT-ERROR-)
(CHECKCONSISTENCY-)
)
:effect
(and
(when
(and
(BELOWOF1-ROBOT)
(ABOVEOF2-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF2-ROBOT)
(RIGHTOF2-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF2-ROBOT)
(BELOWOF2-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF1-ROBOT)
(RIGHTOF1-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF2-ROBOT)
(LEFTOF1-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF1-ROBOT)
(ABOVEOF1-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(NOT-CHECKCONSISTENCY-)
(not (CHECKCONSISTENCY-))
(not (Flag26prime-))
(not (Flag19prime-))
(not (Flag15prime-))
(not (Flag12prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag28prime-))
(not (Flag27prime-))
(not (Flag24prime-))
(not (Flag23prime-))
(not (Flag22prime-))
(not (Flag21prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag17prime-))
(not (Flag14prime-))
(not (Flag4prime-))
(not (Flag29prime-))
(not (Flag25prime-))
(not (Flag16prime-))
(not (Flag13prime-))
(not (Flag10prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag1prime-))
(not (Flag11prime-))
)
)
(:action Flag4Action
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(Flag3-)
(Flag3prime-)
)
:effect
(and
(Flag4-)
(Flag4prime-)
)

)
(:action Prim4Action-2
:parameters ()
:precondition
(and
(not (Flag3-))
(Flag3prime-)
)
:effect
(and
(Flag4prime-)
(not (Flag4-))
)

)
(:action Flag14Action-0
:parameters ()
:precondition
(and
(Flag12-)
(Flag12prime-)
)
:effect
(and
(Flag14-)
(Flag14prime-)
)

)
(:action Flag17Action-0
:parameters ()
:precondition
(and
(Flag12-)
(Flag12prime-)
)
:effect
(and
(Flag17-)
(Flag17prime-)
)

)
(:action Flag18Action-0
:parameters ()
:precondition
(and
(ABOVEOF2-ROBOT)
)
:effect
(and
(Flag18-)
(Flag18prime-)
)

)
(:action Flag18Action-1
:parameters ()
:precondition
(and
(ABOVEOF1-ROBOT)
)
:effect
(and
(Flag18-)
(Flag18prime-)
)

)
(:action Prim18Action
:parameters ()
:precondition
(and
(not (ABOVEOF2-ROBOT))
(not (ABOVEOF1-ROBOT))
)
:effect
(and
(Flag18prime-)
(not (Flag18-))
)

)
(:action Flag19Action-1
:parameters ()
:precondition
(and
(BELOWOF1-ROBOT)
)
:effect
(and
(Flag19-)
(Flag19prime-)
)

)
(:action Prim19Action
:parameters ()
:precondition
(and
(not (BELOWOF2-ROBOT))
(not (BELOWOF1-ROBOT))
)
:effect
(and
(Flag19prime-)
(not (Flag19-))
)

)
(:action Flag20Action
:parameters ()
:precondition
(and
(BELOWOF1-ROBOT)
(ABOVEOF2-ROBOT)
)
:effect
(and
(Flag20-)
(Flag20prime-)
)

)
(:action Flag21Action
:parameters ()
:precondition
(and
(BELOWOF1-ROBOT)
(ABOVEOF1-ROBOT)
)
:effect
(and
(Flag21-)
(Flag21prime-)
)

)
(:action Flag22Action-1
:parameters ()
:precondition
(and
(BELOWOF1-ROBOT)
)
:effect
(and
(Flag22-)
(Flag22prime-)
)

)
(:action Prim20Action-0
:parameters ()
:precondition
(and
(not (BELOWOF1-ROBOT))
)
:effect
(and
(Flag20prime-)
(not (Flag20-))
)

)
(:action Prim20Action-1
:parameters ()
:precondition
(and
(not (ABOVEOF2-ROBOT))
)
:effect
(and
(Flag20prime-)
(not (Flag20-))
)

)
(:action Prim21Action-0
:parameters ()
:precondition
(and
(not (BELOWOF1-ROBOT))
)
:effect
(and
(Flag21prime-)
(not (Flag21-))
)

)
(:action Prim21Action-1
:parameters ()
:precondition
(and
(not (ABOVEOF1-ROBOT))
)
:effect
(and
(Flag21prime-)
(not (Flag21-))
)

)
(:action Flag23Action
:parameters ()
:precondition
(and
(ABOVEOF2-ROBOT)
(BELOWOF2-ROBOT)
)
:effect
(and
(Flag23-)
(Flag23prime-)
)

)
(:action Flag24Action
:parameters ()
:precondition
(and
(ABOVEOF1-ROBOT)
(BELOWOF2-ROBOT)
)
:effect
(and
(Flag24-)
(Flag24prime-)
)

)
(:action Prim23Action-0
:parameters ()
:precondition
(and
(not (ABOVEOF2-ROBOT))
)
:effect
(and
(Flag23prime-)
(not (Flag23-))
)

)
(:action Prim24Action-0
:parameters ()
:precondition
(and
(not (ABOVEOF1-ROBOT))
)
:effect
(and
(Flag24prime-)
(not (Flag24-))
)

)
(:action Flag26Action-1
:parameters ()
:precondition
(and
(BELOWOF1-ROBOT)
)
:effect
(and
(Flag26-)
(Flag26prime-)
)

)
(:action Flag26Action-2
:parameters ()
:precondition
(and
(BELOWOF3-ROBOT)
)
:effect
(and
(Flag26-)
(Flag26prime-)
)

)
(:action Prim26Action
:parameters ()
:precondition
(and
(not (BELOWOF2-ROBOT))
(not (BELOWOF1-ROBOT))
(not (BELOWOF3-ROBOT))
)
:effect
(and
(Flag26prime-)
(not (Flag26-))
)

)
(:action Flag27Action
:parameters ()
:precondition
(and
(BELOWOF3-ROBOT)
(ABOVEOF2-ROBOT)
)
:effect
(and
(Flag27-)
(Flag27prime-)
)

)
(:action Flag28Action
:parameters ()
:precondition
(and
(BELOWOF2-ROBOT)
(ABOVEOF2-ROBOT)
)
:effect
(and
(Flag28-)
(Flag28prime-)
)

)
(:action Prim27Action-0
:parameters ()
:precondition
(and
(not (BELOWOF3-ROBOT))
)
:effect
(and
(Flag27prime-)
(not (Flag27-))
)

)
(:action Prim27Action-1
:parameters ()
:precondition
(and
(not (ABOVEOF2-ROBOT))
)
:effect
(and
(Flag27prime-)
(not (Flag27-))
)

)
(:action Prim28Action-1
:parameters ()
:precondition
(and
(not (ABOVEOF2-ROBOT))
)
:effect
(and
(Flag28prime-)
(not (Flag28-))
)

)
(:action MOVEDOWN-ROBOT
:parameters ()
:precondition
(and
(Flag2-)
(Flag2prime-)
(Flag29prime-)
(Flag25prime-)
(Flag26prime-)
)
:effect
(and
(when
(and
(Flag29-)
)
(and
(ROW1-ROBOT)
(not (NOT-ROW1-ROBOT))
)
)
(when
(and
(Flag25-)
)
(and
(ROW0-ROBOT)
(not (NOT-ROW0-ROBOT))
)
)
(when
(and
(ROW2-ROBOT)
)
(and
(ROW1-ROBOT)
(NOT-ROW2-ROBOT)
(not (NOT-ROW1-ROBOT))
(not (ROW2-ROBOT))
)
)
(when
(and
(ROW1-ROBOT)
)
(and
(ROW0-ROBOT)
(NOT-ROW1-ROBOT)
(not (NOT-ROW0-ROBOT))
(not (ROW1-ROBOT))
)
)
(when
(and
(ABOVEOF2-ROBOT)
)
(and
(ABOVEOF1-ROBOT)
(NOT-ABOVEOF2-ROBOT)
(NOT-ABOVEOF1-ROBOT)
(not (ABOVEOF2-ROBOT))
)
)
(when
(and
(ABOVEOF1-ROBOT)
)
(and
(NOT-ABOVEOF1-ROBOT)
(not (ABOVEOF1-ROBOT))
)
)
(when
(and
(Flag26-)
)
(and
(BELOWOF2-ROBOT)
(not (NOT-BELOWOF2-ROBOT))
)
)
(when
(and
(BELOWOF1-ROBOT)
)
(and
(not (NOT-BELOWOF1-ROBOT))
)
)
(when
(and
(BELOWOF2-ROBOT)
)
(and
(BELOWOF1-ROBOT)
(not (NOT-BELOWOF1-ROBOT))
)
)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag26prime-))
(not (Flag19prime-))
(not (Flag15prime-))
(not (Flag12prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag28prime-))
(not (Flag27prime-))
(not (Flag24prime-))
(not (Flag23prime-))
(not (Flag22prime-))
(not (Flag21prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag17prime-))
(not (Flag14prime-))
(not (Flag4prime-))
(not (Flag29prime-))
(not (Flag25prime-))
(not (Flag16prime-))
(not (Flag13prime-))
(not (Flag10prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag1prime-))
(not (Flag11prime-))
)
)
(:action MOVEUP-ROBOT
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(Flag25prime-)
(Flag22prime-)
(Flag19prime-)
(Flag18prime-)
)
:effect
(and
(when
(and
(Flag25-)
)
(and
(ROW2-ROBOT)
(not (NOT-ROW2-ROBOT))
)
)
(when
(and
(Flag22-)
)
(and
(ROW1-ROBOT)
(not (NOT-ROW1-ROBOT))
)
)
(when
(and
(ROW1-ROBOT)
)
(and
(ROW2-ROBOT)
(NOT-ROW1-ROBOT)
(not (NOT-ROW2-ROBOT))
(not (ROW1-ROBOT))
)
)
(when
(and
(ROW0-ROBOT)
)
(and
(ROW1-ROBOT)
(NOT-ROW0-ROBOT)
(not (NOT-ROW1-ROBOT))
(not (ROW0-ROBOT))
)
)
(when
(and
(Flag19-)
)
(and
(BELOWOF3-ROBOT)
(NOT-BELOWOF2-ROBOT)
(not (NOT-BELOWOF3-ROBOT))
(not (BELOWOF2-ROBOT))
)
)
(when
(and
(BELOWOF1-ROBOT)
)
(and
(BELOWOF2-ROBOT)
(NOT-BELOWOF1-ROBOT)
(not (NOT-BELOWOF2-ROBOT))
(not (BELOWOF1-ROBOT))
)
)
(when
(and
(Flag18-)
)
(and
(ABOVEOF2-ROBOT)
(not (NOT-ABOVEOF2-ROBOT))
)
)
(ABOVEOF1-ROBOT)
(CHECKCONSISTENCY-)
(not (NOT-ABOVEOF1-ROBOT))
(not (NOT-CHECKCONSISTENCY-))
(not (Flag26prime-))
(not (Flag19prime-))
(not (Flag15prime-))
(not (Flag12prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag28prime-))
(not (Flag27prime-))
(not (Flag24prime-))
(not (Flag23prime-))
(not (Flag22prime-))
(not (Flag21prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag17prime-))
(not (Flag14prime-))
(not (Flag4prime-))
(not (Flag29prime-))
(not (Flag25prime-))
(not (Flag16prime-))
(not (Flag13prime-))
(not (Flag10prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag1prime-))
(not (Flag11prime-))
)
)
(:action Prim1Action-2
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
(Flag1prime-)
(not (Flag1-))
)

)
(:action Prim1Action-3
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
(Flag1prime-)
(not (Flag1-))
)

)
(:action Flag2Action
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
)
:effect
(and
(Flag2-)
(Flag2prime-)
)

)
(:action Prim2Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
(Flag2prime-)
(not (Flag2-))
)

)
(:action Prim2Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
(Flag2prime-)
(not (Flag2-))
)

)
(:action Flag3Action-3
:parameters ()
:precondition
(and
(LEFTOF2-ROBOT)
)
:effect
(and
(Flag3-)
(Flag3prime-)
)

)
(:action Flag3Action-4
:parameters ()
:precondition
(and
(RIGHTOF2-ROBOT)
)
:effect
(and
(Flag3-)
(Flag3prime-)
)

)
(:action Prim4Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
(Flag4prime-)
(not (Flag4-))
)

)
(:action Prim4Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
(Flag4prime-)
(not (Flag4-))
)

)
(:action Flag5Action-2
:parameters ()
:precondition
(and
(RIGHTOF2-ROBOT)
)
:effect
(and
(Flag5-)
(Flag5prime-)
)

)
(:action Flag6Action-1
:parameters ()
:precondition
(and
(RIGHTOF2-ROBOT)
)
:effect
(and
(Flag6-)
(Flag6prime-)
)

)
(:action Flag7Action-1
:parameters ()
:precondition
(and
(LEFTOF2-ROBOT)
)
:effect
(and
(Flag7-)
(Flag7prime-)
)

)
(:action Prim10Action-1
:parameters ()
:precondition
(and
(not (RIGHTOF2-ROBOT))
)
:effect
(and
(Flag10prime-)
(not (Flag10-))
)

)
(:action Flag12Action
:parameters ()
:precondition
(and
(RIGHTOF2-ROBOT)
(LEFTOF2-ROBOT)
)
:effect
(and
(Flag12-)
(Flag12prime-)
)

)
(:action Prim12Action-0
:parameters ()
:precondition
(and
(not (RIGHTOF2-ROBOT))
)
:effect
(and
(Flag12prime-)
(not (Flag12-))
)

)
(:action Prim12Action-1
:parameters ()
:precondition
(and
(not (LEFTOF2-ROBOT))
)
:effect
(and
(Flag12prime-)
(not (Flag12-))
)

)
(:action Prim13Action-0
:parameters ()
:precondition
(and
(not (LEFTOF2-ROBOT))
)
:effect
(and
(Flag13prime-)
(not (Flag13-))
)

)
(:action Flag15Action-2
:parameters ()
:precondition
(and
(LEFTOF2-ROBOT)
)
:effect
(and
(Flag15-)
(Flag15prime-)
)

)
(:action Prim16Action-1
:parameters ()
:precondition
(and
(not (RIGHTOF2-ROBOT))
)
:effect
(and
(Flag16prime-)
(not (Flag16-))
)

)
(:action Flag19Action-0
:parameters ()
:precondition
(and
(BELOWOF2-ROBOT)
)
:effect
(and
(Flag19-)
(Flag19prime-)
)

)
(:action Prim23Action-1
:parameters ()
:precondition
(and
(not (BELOWOF2-ROBOT))
)
:effect
(and
(Flag23prime-)
(not (Flag23-))
)

)
(:action Prim24Action-1
:parameters ()
:precondition
(and
(not (BELOWOF2-ROBOT))
)
:effect
(and
(Flag24prime-)
(not (Flag24-))
)

)
(:action Flag26Action-0
:parameters ()
:precondition
(and
(BELOWOF2-ROBOT)
)
:effect
(and
(Flag26-)
(Flag26prime-)
)

)
(:action Prim28Action-0
:parameters ()
:precondition
(and
(not (BELOWOF2-ROBOT))
)
:effect
(and
(Flag28prime-)
(not (Flag28-))
)

)
)
