(define (domain grounded-SIMPLE-ADL-ROBOT)
(:requirements
:strips
)
(:predicates
(Flag281-)
(Flag280-)
(Flag261-)
(Flag24-)
(Flag23-)
(Flag22-)
(Flag21-)
(Flag3-)
(Flag2-)
(ROW17-ROBOT)
(ROW16-ROBOT)
(ROW15-ROBOT)
(ROW14-ROBOT)
(ROW13-ROBOT)
(ROW12-ROBOT)
(ROW11-ROBOT)
(ROW10-ROBOT)
(ROW9-ROBOT)
(ROW8-ROBOT)
(ROW7-ROBOT)
(ROW6-ROBOT)
(ROW5-ROBOT)
(ROW4-ROBOT)
(ROW3-ROBOT)
(ROW2-ROBOT)
(ROW1-ROBOT)
(ROW0-ROBOT)
(ABOVEOF17-ROBOT)
(ABOVEOF16-ROBOT)
(ABOVEOF15-ROBOT)
(ABOVEOF14-ROBOT)
(ABOVEOF13-ROBOT)
(ABOVEOF12-ROBOT)
(ABOVEOF11-ROBOT)
(ABOVEOF10-ROBOT)
(ABOVEOF9-ROBOT)
(ABOVEOF8-ROBOT)
(ABOVEOF7-ROBOT)
(ABOVEOF6-ROBOT)
(ABOVEOF5-ROBOT)
(ABOVEOF4-ROBOT)
(ABOVEOF3-ROBOT)
(ABOVEOF2-ROBOT)
(ABOVEOF1-ROBOT)
(BELOWOF17-ROBOT)
(BELOWOF16-ROBOT)
(BELOWOF15-ROBOT)
(BELOWOF14-ROBOT)
(BELOWOF13-ROBOT)
(BELOWOF12-ROBOT)
(BELOWOF11-ROBOT)
(BELOWOF10-ROBOT)
(BELOWOF9-ROBOT)
(BELOWOF8-ROBOT)
(BELOWOF7-ROBOT)
(BELOWOF6-ROBOT)
(BELOWOF5-ROBOT)
(BELOWOF4-ROBOT)
(BELOWOF3-ROBOT)
(BELOWOF2-ROBOT)
(BELOWOF1-ROBOT)
(CHECKCONSISTENCY-)
(ROW18-ROBOT)
(BELOWOF19-ROBOT)
(NOT-BELOWOF18-ROBOT)
(ABOVEOF18-ROBOT)
(Flag522-)
(Flag520-)
(Flag518-)
(Flag517-)
(Flag515-)
(Flag514-)
(Flag513-)
(Flag511-)
(Flag510-)
(Flag509-)
(Flag508-)
(Flag506-)
(Flag505-)
(Flag504-)
(Flag503-)
(Flag502-)
(Flag500-)
(Flag499-)
(Flag498-)
(Flag497-)
(Flag496-)
(Flag495-)
(Flag494-)
(Flag492-)
(Flag491-)
(Flag490-)
(Flag489-)
(Flag488-)
(Flag487-)
(Flag486-)
(Flag485-)
(Flag483-)
(Flag482-)
(Flag481-)
(Flag480-)
(Flag479-)
(Flag478-)
(Flag477-)
(Flag476-)
(Flag474-)
(Flag473-)
(Flag472-)
(Flag471-)
(Flag470-)
(Flag469-)
(Flag468-)
(Flag467-)
(Flag466-)
(Flag464-)
(Flag463-)
(Flag462-)
(Flag461-)
(Flag460-)
(Flag459-)
(Flag458-)
(Flag457-)
(Flag456-)
(Flag455-)
(Flag454-)
(Flag452-)
(Flag451-)
(Flag450-)
(Flag449-)
(Flag448-)
(Flag447-)
(Flag446-)
(Flag445-)
(Flag444-)
(Flag443-)
(Flag442-)
(Flag441-)
(Flag440-)
(Flag438-)
(Flag437-)
(Flag436-)
(Flag435-)
(Flag434-)
(Flag433-)
(Flag432-)
(Flag431-)
(Flag430-)
(Flag429-)
(Flag428-)
(Flag427-)
(Flag426-)
(Flag424-)
(Flag423-)
(Flag422-)
(Flag421-)
(Flag420-)
(Flag419-)
(Flag418-)
(Flag417-)
(Flag416-)
(Flag415-)
(Flag414-)
(Flag413-)
(Flag412-)
(Flag411-)
(Flag410-)
(Flag409-)
(Flag407-)
(Flag406-)
(Flag405-)
(Flag404-)
(Flag403-)
(Flag402-)
(Flag401-)
(Flag400-)
(Flag399-)
(Flag398-)
(Flag397-)
(Flag396-)
(Flag395-)
(Flag394-)
(Flag393-)
(Flag392-)
(Flag390-)
(Flag389-)
(Flag388-)
(Flag387-)
(Flag386-)
(Flag385-)
(Flag384-)
(Flag383-)
(Flag382-)
(Flag381-)
(Flag380-)
(Flag379-)
(Flag378-)
(Flag377-)
(Flag376-)
(Flag375-)
(Flag373-)
(Flag372-)
(Flag371-)
(Flag370-)
(Flag369-)
(Flag368-)
(Flag367-)
(Flag366-)
(Flag365-)
(Flag364-)
(Flag363-)
(Flag362-)
(Flag361-)
(Flag360-)
(Flag359-)
(Flag358-)
(Flag357-)
(Flag356-)
(Flag354-)
(Flag353-)
(Flag352-)
(Flag351-)
(Flag350-)
(Flag349-)
(Flag348-)
(Flag347-)
(Flag346-)
(Flag345-)
(Flag344-)
(Flag343-)
(Flag342-)
(Flag341-)
(Flag340-)
(Flag339-)
(Flag338-)
(Flag337-)
(Flag335-)
(Flag334-)
(Flag333-)
(Flag332-)
(Flag331-)
(Flag330-)
(Flag329-)
(Flag328-)
(Flag327-)
(Flag326-)
(Flag325-)
(Flag324-)
(Flag323-)
(Flag322-)
(Flag321-)
(Flag320-)
(Flag319-)
(Flag318-)
(Flag317-)
(Flag316-)
(Flag315-)
(Flag314-)
(Flag313-)
(Flag312-)
(Flag311-)
(Flag310-)
(Flag309-)
(Flag308-)
(Flag307-)
(Flag306-)
(Flag305-)
(Flag304-)
(Flag303-)
(Flag302-)
(Flag301-)
(Flag300-)
(Flag299-)
(Flag298-)
(Flag297-)
(Flag296-)
(Flag295-)
(Flag294-)
(Flag293-)
(Flag292-)
(Flag291-)
(Flag290-)
(Flag289-)
(Flag288-)
(Flag287-)
(Flag286-)
(Flag285-)
(Flag284-)
(Flag283-)
(Flag282-)
(Flag279-)
(Flag278-)
(Flag277-)
(Flag276-)
(Flag275-)
(Flag274-)
(Flag273-)
(Flag272-)
(Flag271-)
(Flag270-)
(Flag269-)
(Flag268-)
(Flag267-)
(Flag266-)
(Flag265-)
(Flag264-)
(Flag4-)
(ERROR-)
(COLUMN17-ROBOT)
(COLUMN16-ROBOT)
(COLUMN15-ROBOT)
(COLUMN14-ROBOT)
(COLUMN13-ROBOT)
(COLUMN12-ROBOT)
(COLUMN11-ROBOT)
(COLUMN10-ROBOT)
(COLUMN9-ROBOT)
(COLUMN8-ROBOT)
(COLUMN7-ROBOT)
(COLUMN6-ROBOT)
(COLUMN5-ROBOT)
(COLUMN4-ROBOT)
(COLUMN3-ROBOT)
(COLUMN2-ROBOT)
(COLUMN1-ROBOT)
(COLUMN0-ROBOT)
(RIGHTOF17-ROBOT)
(RIGHTOF16-ROBOT)
(RIGHTOF15-ROBOT)
(RIGHTOF14-ROBOT)
(RIGHTOF13-ROBOT)
(RIGHTOF12-ROBOT)
(RIGHTOF11-ROBOT)
(RIGHTOF10-ROBOT)
(RIGHTOF9-ROBOT)
(RIGHTOF8-ROBOT)
(RIGHTOF7-ROBOT)
(RIGHTOF6-ROBOT)
(RIGHTOF5-ROBOT)
(RIGHTOF4-ROBOT)
(RIGHTOF3-ROBOT)
(RIGHTOF1-ROBOT)
(NOT-RIGHTOF2-ROBOT)
(RIGHTOF0-ROBOT)
(LEFTOF17-ROBOT)
(LEFTOF16-ROBOT)
(LEFTOF15-ROBOT)
(LEFTOF14-ROBOT)
(LEFTOF13-ROBOT)
(LEFTOF12-ROBOT)
(LEFTOF11-ROBOT)
(LEFTOF10-ROBOT)
(LEFTOF9-ROBOT)
(LEFTOF8-ROBOT)
(LEFTOF7-ROBOT)
(LEFTOF6-ROBOT)
(LEFTOF5-ROBOT)
(LEFTOF4-ROBOT)
(LEFTOF3-ROBOT)
(LEFTOF2-ROBOT)
(LEFTOF1-ROBOT)
(COLUMN18-ROBOT)
(LEFTOF19-ROBOT)
(NOT-LEFTOF18-ROBOT)
(RIGHTOF18-ROBOT)
(Flag521-)
(Flag519-)
(Flag516-)
(Flag512-)
(Flag507-)
(Flag501-)
(Flag493-)
(Flag484-)
(Flag475-)
(Flag465-)
(Flag453-)
(Flag439-)
(Flag425-)
(Flag408-)
(Flag391-)
(Flag374-)
(Flag355-)
(Flag336-)
(Flag262-)
(Flag259-)
(Flag257-)
(Flag256-)
(Flag254-)
(Flag253-)
(Flag252-)
(Flag250-)
(Flag249-)
(Flag248-)
(Flag247-)
(Flag246-)
(Flag244-)
(Flag243-)
(Flag242-)
(Flag241-)
(Flag240-)
(Flag239-)
(Flag237-)
(Flag236-)
(Flag235-)
(Flag234-)
(Flag233-)
(Flag232-)
(Flag230-)
(Flag229-)
(Flag228-)
(Flag227-)
(Flag226-)
(Flag225-)
(Flag224-)
(Flag223-)
(Flag221-)
(Flag220-)
(Flag219-)
(Flag218-)
(Flag217-)
(Flag216-)
(Flag215-)
(Flag214-)
(Flag212-)
(Flag211-)
(Flag210-)
(Flag209-)
(Flag208-)
(Flag207-)
(Flag206-)
(Flag205-)
(Flag204-)
(Flag203-)
(Flag201-)
(Flag200-)
(Flag199-)
(Flag198-)
(Flag197-)
(Flag196-)
(Flag195-)
(Flag194-)
(Flag193-)
(Flag192-)
(Flag190-)
(Flag189-)
(Flag188-)
(Flag187-)
(Flag186-)
(Flag185-)
(Flag184-)
(Flag183-)
(Flag182-)
(Flag181-)
(Flag180-)
(Flag179-)
(Flag177-)
(Flag176-)
(Flag175-)
(Flag174-)
(Flag173-)
(Flag172-)
(Flag171-)
(Flag170-)
(Flag169-)
(Flag168-)
(Flag167-)
(Flag166-)
(Flag165-)
(Flag163-)
(Flag162-)
(Flag161-)
(Flag160-)
(Flag159-)
(Flag158-)
(Flag157-)
(Flag156-)
(Flag155-)
(Flag154-)
(Flag153-)
(Flag152-)
(Flag151-)
(Flag150-)
(Flag149-)
(Flag147-)
(Flag146-)
(Flag145-)
(Flag144-)
(Flag143-)
(Flag142-)
(Flag141-)
(Flag140-)
(Flag139-)
(Flag138-)
(Flag137-)
(Flag136-)
(Flag135-)
(Flag134-)
(Flag132-)
(Flag131-)
(Flag130-)
(Flag129-)
(Flag128-)
(Flag127-)
(Flag126-)
(Flag125-)
(Flag124-)
(Flag123-)
(Flag122-)
(Flag121-)
(Flag120-)
(Flag119-)
(Flag118-)
(Flag117-)
(Flag116-)
(Flag115-)
(Flag113-)
(Flag112-)
(Flag111-)
(Flag110-)
(Flag109-)
(Flag108-)
(Flag107-)
(Flag106-)
(Flag105-)
(Flag104-)
(Flag103-)
(Flag102-)
(Flag101-)
(Flag100-)
(Flag99-)
(Flag98-)
(Flag97-)
(Flag96-)
(Flag94-)
(Flag93-)
(Flag92-)
(Flag91-)
(Flag90-)
(Flag89-)
(Flag88-)
(Flag87-)
(Flag86-)
(Flag85-)
(Flag84-)
(Flag83-)
(Flag82-)
(Flag81-)
(Flag80-)
(Flag79-)
(Flag78-)
(Flag76-)
(Flag75-)
(Flag74-)
(Flag73-)
(Flag72-)
(Flag71-)
(Flag70-)
(Flag69-)
(Flag68-)
(Flag67-)
(Flag66-)
(Flag65-)
(Flag64-)
(Flag63-)
(Flag62-)
(Flag61-)
(Flag60-)
(Flag59-)
(Flag58-)
(Flag57-)
(Flag56-)
(Flag55-)
(Flag54-)
(Flag53-)
(Flag52-)
(Flag51-)
(Flag50-)
(Flag49-)
(Flag48-)
(Flag47-)
(Flag46-)
(Flag45-)
(Flag44-)
(Flag43-)
(Flag42-)
(Flag41-)
(Flag40-)
(Flag39-)
(Flag38-)
(Flag37-)
(Flag36-)
(Flag35-)
(Flag34-)
(Flag33-)
(Flag32-)
(Flag31-)
(Flag30-)
(Flag29-)
(Flag28-)
(Flag27-)
(Flag26-)
(Flag25-)
(Flag20-)
(Flag19-)
(Flag18-)
(Flag17-)
(Flag16-)
(Flag15-)
(Flag14-)
(Flag13-)
(Flag12-)
(Flag11-)
(Flag10-)
(Flag9-)
(Flag8-)
(Flag7-)
(Flag6-)
(Flag5-)
(Flag1-)
(Flag263-)
(Flag260-)
(Flag258-)
(Flag255-)
(Flag251-)
(Flag245-)
(Flag238-)
(Flag231-)
(Flag222-)
(Flag213-)
(Flag202-)
(Flag191-)
(Flag178-)
(Flag164-)
(Flag148-)
(Flag133-)
(Flag114-)
(Flag95-)
(Flag77-)
(NOT-COLUMN18-ROBOT)
(NOT-COLUMN17-ROBOT)
(NOT-COLUMN16-ROBOT)
(NOT-COLUMN15-ROBOT)
(NOT-COLUMN14-ROBOT)
(NOT-COLUMN13-ROBOT)
(NOT-COLUMN12-ROBOT)
(NOT-COLUMN11-ROBOT)
(NOT-COLUMN10-ROBOT)
(NOT-COLUMN9-ROBOT)
(NOT-COLUMN8-ROBOT)
(NOT-COLUMN7-ROBOT)
(NOT-COLUMN6-ROBOT)
(NOT-COLUMN5-ROBOT)
(NOT-COLUMN4-ROBOT)
(NOT-COLUMN3-ROBOT)
(NOT-COLUMN2-ROBOT)
(NOT-COLUMN1-ROBOT)
(NOT-LEFTOF19-ROBOT)
(LEFTOF18-ROBOT)
(NOT-LEFTOF17-ROBOT)
(NOT-LEFTOF16-ROBOT)
(NOT-LEFTOF15-ROBOT)
(NOT-LEFTOF14-ROBOT)
(NOT-LEFTOF13-ROBOT)
(NOT-LEFTOF12-ROBOT)
(NOT-LEFTOF11-ROBOT)
(NOT-LEFTOF10-ROBOT)
(NOT-LEFTOF9-ROBOT)
(NOT-LEFTOF8-ROBOT)
(NOT-LEFTOF7-ROBOT)
(NOT-LEFTOF6-ROBOT)
(NOT-LEFTOF5-ROBOT)
(NOT-LEFTOF4-ROBOT)
(NOT-LEFTOF3-ROBOT)
(NOT-LEFTOF2-ROBOT)
(NOT-RIGHTOF18-ROBOT)
(NOT-RIGHTOF17-ROBOT)
(NOT-RIGHTOF16-ROBOT)
(NOT-RIGHTOF15-ROBOT)
(NOT-RIGHTOF14-ROBOT)
(NOT-RIGHTOF13-ROBOT)
(NOT-RIGHTOF12-ROBOT)
(NOT-RIGHTOF11-ROBOT)
(NOT-RIGHTOF10-ROBOT)
(NOT-RIGHTOF9-ROBOT)
(NOT-RIGHTOF8-ROBOT)
(NOT-RIGHTOF7-ROBOT)
(NOT-RIGHTOF6-ROBOT)
(NOT-RIGHTOF5-ROBOT)
(NOT-RIGHTOF4-ROBOT)
(NOT-RIGHTOF3-ROBOT)
(NOT-RIGHTOF1-ROBOT)
(NOT-CHECKCONSISTENCY-)
(NOT-COLUMN0-ROBOT)
(RIGHTOF2-ROBOT)
(NOT-RIGHTOF0-ROBOT)
(NOT-LEFTOF1-ROBOT)
(NOT-ERROR-)
(NOT-ROW18-ROBOT)
(NOT-ROW17-ROBOT)
(NOT-ROW16-ROBOT)
(NOT-ROW15-ROBOT)
(NOT-ROW14-ROBOT)
(NOT-ROW13-ROBOT)
(NOT-ROW12-ROBOT)
(NOT-ROW11-ROBOT)
(NOT-ROW10-ROBOT)
(NOT-ROW9-ROBOT)
(NOT-ROW8-ROBOT)
(NOT-ROW7-ROBOT)
(NOT-ROW6-ROBOT)
(NOT-ROW5-ROBOT)
(NOT-ROW4-ROBOT)
(NOT-ROW3-ROBOT)
(NOT-ROW2-ROBOT)
(NOT-ROW1-ROBOT)
(NOT-BELOWOF19-ROBOT)
(BELOWOF18-ROBOT)
(NOT-BELOWOF17-ROBOT)
(NOT-BELOWOF16-ROBOT)
(NOT-BELOWOF15-ROBOT)
(NOT-BELOWOF14-ROBOT)
(NOT-BELOWOF13-ROBOT)
(NOT-BELOWOF12-ROBOT)
(NOT-BELOWOF11-ROBOT)
(NOT-BELOWOF10-ROBOT)
(NOT-BELOWOF9-ROBOT)
(NOT-BELOWOF8-ROBOT)
(NOT-BELOWOF7-ROBOT)
(NOT-BELOWOF6-ROBOT)
(NOT-BELOWOF5-ROBOT)
(NOT-BELOWOF4-ROBOT)
(NOT-BELOWOF3-ROBOT)
(NOT-BELOWOF2-ROBOT)
(NOT-ABOVEOF18-ROBOT)
(NOT-ABOVEOF17-ROBOT)
(NOT-ABOVEOF16-ROBOT)
(NOT-ABOVEOF15-ROBOT)
(NOT-ABOVEOF14-ROBOT)
(NOT-ABOVEOF13-ROBOT)
(NOT-ABOVEOF12-ROBOT)
(NOT-ABOVEOF11-ROBOT)
(NOT-ABOVEOF10-ROBOT)
(NOT-ABOVEOF9-ROBOT)
(NOT-ABOVEOF8-ROBOT)
(NOT-ABOVEOF7-ROBOT)
(NOT-ABOVEOF6-ROBOT)
(NOT-ABOVEOF5-ROBOT)
(NOT-ABOVEOF4-ROBOT)
(NOT-ABOVEOF3-ROBOT)
(NOT-ABOVEOF2-ROBOT)
(NOT-ABOVEOF1-ROBOT)
(NOT-ROW0-ROBOT)
(NOT-BELOWOF1-ROBOT)
(Flag281prime-)
(Flag280prime-)
(Flag261prime-)
(Flag24prime-)
(Flag23prime-)
(Flag22prime-)
(Flag21prime-)
(Flag3prime-)
(Flag2prime-)
(Flag522prime-)
(Flag520prime-)
(Flag518prime-)
(Flag517prime-)
(Flag515prime-)
(Flag514prime-)
(Flag513prime-)
(Flag511prime-)
(Flag510prime-)
(Flag509prime-)
(Flag508prime-)
(Flag506prime-)
(Flag505prime-)
(Flag504prime-)
(Flag503prime-)
(Flag502prime-)
(Flag500prime-)
(Flag499prime-)
(Flag498prime-)
(Flag497prime-)
(Flag496prime-)
(Flag495prime-)
(Flag494prime-)
(Flag492prime-)
(Flag491prime-)
(Flag490prime-)
(Flag489prime-)
(Flag488prime-)
(Flag487prime-)
(Flag486prime-)
(Flag485prime-)
(Flag483prime-)
(Flag482prime-)
(Flag481prime-)
(Flag480prime-)
(Flag479prime-)
(Flag478prime-)
(Flag477prime-)
(Flag476prime-)
(Flag474prime-)
(Flag473prime-)
(Flag472prime-)
(Flag471prime-)
(Flag470prime-)
(Flag469prime-)
(Flag468prime-)
(Flag467prime-)
(Flag466prime-)
(Flag464prime-)
(Flag463prime-)
(Flag462prime-)
(Flag461prime-)
(Flag460prime-)
(Flag459prime-)
(Flag458prime-)
(Flag457prime-)
(Flag456prime-)
(Flag455prime-)
(Flag454prime-)
(Flag452prime-)
(Flag451prime-)
(Flag450prime-)
(Flag449prime-)
(Flag448prime-)
(Flag447prime-)
(Flag446prime-)
(Flag445prime-)
(Flag444prime-)
(Flag443prime-)
(Flag442prime-)
(Flag441prime-)
(Flag440prime-)
(Flag438prime-)
(Flag437prime-)
(Flag436prime-)
(Flag435prime-)
(Flag434prime-)
(Flag433prime-)
(Flag432prime-)
(Flag431prime-)
(Flag430prime-)
(Flag429prime-)
(Flag428prime-)
(Flag427prime-)
(Flag426prime-)
(Flag424prime-)
(Flag423prime-)
(Flag422prime-)
(Flag421prime-)
(Flag420prime-)
(Flag419prime-)
(Flag418prime-)
(Flag417prime-)
(Flag416prime-)
(Flag415prime-)
(Flag414prime-)
(Flag413prime-)
(Flag412prime-)
(Flag411prime-)
(Flag410prime-)
(Flag409prime-)
(Flag407prime-)
(Flag406prime-)
(Flag405prime-)
(Flag404prime-)
(Flag403prime-)
(Flag402prime-)
(Flag401prime-)
(Flag400prime-)
(Flag399prime-)
(Flag398prime-)
(Flag397prime-)
(Flag396prime-)
(Flag395prime-)
(Flag394prime-)
(Flag393prime-)
(Flag392prime-)
(Flag390prime-)
(Flag389prime-)
(Flag388prime-)
(Flag387prime-)
(Flag386prime-)
(Flag385prime-)
(Flag384prime-)
(Flag383prime-)
(Flag382prime-)
(Flag381prime-)
(Flag380prime-)
(Flag379prime-)
(Flag378prime-)
(Flag377prime-)
(Flag376prime-)
(Flag375prime-)
(Flag373prime-)
(Flag372prime-)
(Flag371prime-)
(Flag370prime-)
(Flag369prime-)
(Flag368prime-)
(Flag367prime-)
(Flag366prime-)
(Flag365prime-)
(Flag364prime-)
(Flag363prime-)
(Flag362prime-)
(Flag361prime-)
(Flag360prime-)
(Flag359prime-)
(Flag358prime-)
(Flag357prime-)
(Flag356prime-)
(Flag354prime-)
(Flag353prime-)
(Flag352prime-)
(Flag351prime-)
(Flag350prime-)
(Flag349prime-)
(Flag348prime-)
(Flag347prime-)
(Flag346prime-)
(Flag345prime-)
(Flag344prime-)
(Flag343prime-)
(Flag342prime-)
(Flag341prime-)
(Flag340prime-)
(Flag339prime-)
(Flag338prime-)
(Flag337prime-)
(Flag335prime-)
(Flag334prime-)
(Flag333prime-)
(Flag332prime-)
(Flag331prime-)
(Flag330prime-)
(Flag329prime-)
(Flag328prime-)
(Flag327prime-)
(Flag326prime-)
(Flag325prime-)
(Flag324prime-)
(Flag323prime-)
(Flag322prime-)
(Flag321prime-)
(Flag320prime-)
(Flag319prime-)
(Flag318prime-)
(Flag317prime-)
(Flag316prime-)
(Flag315prime-)
(Flag314prime-)
(Flag313prime-)
(Flag312prime-)
(Flag311prime-)
(Flag310prime-)
(Flag309prime-)
(Flag308prime-)
(Flag307prime-)
(Flag306prime-)
(Flag305prime-)
(Flag304prime-)
(Flag303prime-)
(Flag302prime-)
(Flag301prime-)
(Flag300prime-)
(Flag299prime-)
(Flag298prime-)
(Flag297prime-)
(Flag296prime-)
(Flag295prime-)
(Flag294prime-)
(Flag293prime-)
(Flag292prime-)
(Flag291prime-)
(Flag290prime-)
(Flag289prime-)
(Flag288prime-)
(Flag287prime-)
(Flag286prime-)
(Flag285prime-)
(Flag284prime-)
(Flag283prime-)
(Flag282prime-)
(Flag279prime-)
(Flag278prime-)
(Flag277prime-)
(Flag276prime-)
(Flag275prime-)
(Flag274prime-)
(Flag273prime-)
(Flag272prime-)
(Flag271prime-)
(Flag270prime-)
(Flag269prime-)
(Flag268prime-)
(Flag267prime-)
(Flag266prime-)
(Flag265prime-)
(Flag264prime-)
(Flag4prime-)
(Flag521prime-)
(Flag519prime-)
(Flag516prime-)
(Flag512prime-)
(Flag507prime-)
(Flag501prime-)
(Flag493prime-)
(Flag484prime-)
(Flag475prime-)
(Flag465prime-)
(Flag453prime-)
(Flag439prime-)
(Flag425prime-)
(Flag408prime-)
(Flag391prime-)
(Flag374prime-)
(Flag355prime-)
(Flag336prime-)
(Flag262prime-)
(Flag259prime-)
(Flag257prime-)
(Flag256prime-)
(Flag254prime-)
(Flag253prime-)
(Flag252prime-)
(Flag250prime-)
(Flag249prime-)
(Flag248prime-)
(Flag247prime-)
(Flag246prime-)
(Flag244prime-)
(Flag243prime-)
(Flag242prime-)
(Flag241prime-)
(Flag240prime-)
(Flag239prime-)
(Flag237prime-)
(Flag236prime-)
(Flag235prime-)
(Flag234prime-)
(Flag233prime-)
(Flag232prime-)
(Flag230prime-)
(Flag229prime-)
(Flag228prime-)
(Flag227prime-)
(Flag226prime-)
(Flag225prime-)
(Flag224prime-)
(Flag223prime-)
(Flag221prime-)
(Flag220prime-)
(Flag219prime-)
(Flag218prime-)
(Flag217prime-)
(Flag216prime-)
(Flag215prime-)
(Flag214prime-)
(Flag212prime-)
(Flag211prime-)
(Flag210prime-)
(Flag209prime-)
(Flag208prime-)
(Flag207prime-)
(Flag206prime-)
(Flag205prime-)
(Flag204prime-)
(Flag203prime-)
(Flag201prime-)
(Flag200prime-)
(Flag199prime-)
(Flag198prime-)
(Flag197prime-)
(Flag196prime-)
(Flag195prime-)
(Flag194prime-)
(Flag193prime-)
(Flag192prime-)
(Flag190prime-)
(Flag189prime-)
(Flag188prime-)
(Flag187prime-)
(Flag186prime-)
(Flag185prime-)
(Flag184prime-)
(Flag183prime-)
(Flag182prime-)
(Flag181prime-)
(Flag180prime-)
(Flag179prime-)
(Flag177prime-)
(Flag176prime-)
(Flag175prime-)
(Flag174prime-)
(Flag173prime-)
(Flag172prime-)
(Flag171prime-)
(Flag170prime-)
(Flag169prime-)
(Flag168prime-)
(Flag167prime-)
(Flag166prime-)
(Flag165prime-)
(Flag163prime-)
(Flag162prime-)
(Flag161prime-)
(Flag160prime-)
(Flag159prime-)
(Flag158prime-)
(Flag157prime-)
(Flag156prime-)
(Flag155prime-)
(Flag154prime-)
(Flag153prime-)
(Flag152prime-)
(Flag151prime-)
(Flag150prime-)
(Flag149prime-)
(Flag147prime-)
(Flag146prime-)
(Flag145prime-)
(Flag144prime-)
(Flag143prime-)
(Flag142prime-)
(Flag141prime-)
(Flag140prime-)
(Flag139prime-)
(Flag138prime-)
(Flag137prime-)
(Flag136prime-)
(Flag135prime-)
(Flag134prime-)
(Flag132prime-)
(Flag131prime-)
(Flag130prime-)
(Flag129prime-)
(Flag128prime-)
(Flag127prime-)
(Flag126prime-)
(Flag125prime-)
(Flag124prime-)
(Flag123prime-)
(Flag122prime-)
(Flag121prime-)
(Flag120prime-)
(Flag119prime-)
(Flag118prime-)
(Flag117prime-)
(Flag116prime-)
(Flag115prime-)
(Flag113prime-)
(Flag112prime-)
(Flag111prime-)
(Flag110prime-)
(Flag109prime-)
(Flag108prime-)
(Flag107prime-)
(Flag106prime-)
(Flag105prime-)
(Flag104prime-)
(Flag103prime-)
(Flag102prime-)
(Flag101prime-)
(Flag100prime-)
(Flag99prime-)
(Flag98prime-)
(Flag97prime-)
(Flag96prime-)
(Flag94prime-)
(Flag93prime-)
(Flag92prime-)
(Flag91prime-)
(Flag90prime-)
(Flag89prime-)
(Flag88prime-)
(Flag87prime-)
(Flag86prime-)
(Flag85prime-)
(Flag84prime-)
(Flag83prime-)
(Flag82prime-)
(Flag81prime-)
(Flag80prime-)
(Flag79prime-)
(Flag78prime-)
(Flag76prime-)
(Flag75prime-)
(Flag74prime-)
(Flag73prime-)
(Flag72prime-)
(Flag71prime-)
(Flag70prime-)
(Flag69prime-)
(Flag68prime-)
(Flag67prime-)
(Flag66prime-)
(Flag65prime-)
(Flag64prime-)
(Flag63prime-)
(Flag62prime-)
(Flag61prime-)
(Flag60prime-)
(Flag59prime-)
(Flag58prime-)
(Flag57prime-)
(Flag56prime-)
(Flag55prime-)
(Flag54prime-)
(Flag53prime-)
(Flag52prime-)
(Flag51prime-)
(Flag50prime-)
(Flag49prime-)
(Flag48prime-)
(Flag47prime-)
(Flag46prime-)
(Flag45prime-)
(Flag44prime-)
(Flag43prime-)
(Flag42prime-)
(Flag41prime-)
(Flag40prime-)
(Flag39prime-)
(Flag38prime-)
(Flag37prime-)
(Flag36prime-)
(Flag35prime-)
(Flag34prime-)
(Flag33prime-)
(Flag32prime-)
(Flag31prime-)
(Flag30prime-)
(Flag29prime-)
(Flag28prime-)
(Flag27prime-)
(Flag26prime-)
(Flag25prime-)
(Flag20prime-)
(Flag19prime-)
(Flag18prime-)
(Flag17prime-)
(Flag16prime-)
(Flag15prime-)
(Flag14prime-)
(Flag13prime-)
(Flag12prime-)
(Flag11prime-)
(Flag10prime-)
(Flag9prime-)
(Flag8prime-)
(Flag7prime-)
(Flag6prime-)
(Flag5prime-)
(Flag1prime-)
(Flag263prime-)
(Flag260prime-)
(Flag258prime-)
(Flag255prime-)
(Flag251prime-)
(Flag245prime-)
(Flag238prime-)
(Flag231prime-)
(Flag222prime-)
(Flag213prime-)
(Flag202prime-)
(Flag191prime-)
(Flag178prime-)
(Flag164prime-)
(Flag148prime-)
(Flag133prime-)
(Flag114prime-)
(Flag95prime-)
(Flag77prime-)
)
(:action Flag77Action-0
:parameters ()
:precondition
(and
(Flag40-)
(Flag40prime-)
)
:effect
(and
(Flag77-)
(Flag77prime-)
)

)
(:action Flag77Action-1
:parameters ()
:precondition
(and
(Flag41-)
(Flag41prime-)
)
:effect
(and
(Flag77-)
(Flag77prime-)
)

)
(:action Flag77Action-2
:parameters ()
:precondition
(and
(Flag42-)
(Flag42prime-)
)
:effect
(and
(Flag77-)
(Flag77prime-)
)

)
(:action Flag77Action-3
:parameters ()
:precondition
(and
(Flag43-)
(Flag43prime-)
)
:effect
(and
(Flag77-)
(Flag77prime-)
)

)
(:action Flag77Action-4
:parameters ()
:precondition
(and
(Flag44-)
(Flag44prime-)
)
:effect
(and
(Flag77-)
(Flag77prime-)
)

)
(:action Flag77Action-5
:parameters ()
:precondition
(and
(Flag45-)
(Flag45prime-)
)
:effect
(and
(Flag77-)
(Flag77prime-)
)

)
(:action Flag77Action-6
:parameters ()
:precondition
(and
(Flag46-)
(Flag46prime-)
)
:effect
(and
(Flag77-)
(Flag77prime-)
)

)
(:action Flag77Action-7
:parameters ()
:precondition
(and
(Flag47-)
(Flag47prime-)
)
:effect
(and
(Flag77-)
(Flag77prime-)
)

)
(:action Flag77Action-8
:parameters ()
:precondition
(and
(Flag48-)
(Flag48prime-)
)
:effect
(and
(Flag77-)
(Flag77prime-)
)

)
(:action Flag77Action-9
:parameters ()
:precondition
(and
(Flag49-)
(Flag49prime-)
)
:effect
(and
(Flag77-)
(Flag77prime-)
)

)
(:action Flag77Action-10
:parameters ()
:precondition
(and
(Flag50-)
(Flag50prime-)
)
:effect
(and
(Flag77-)
(Flag77prime-)
)

)
(:action Flag77Action-11
:parameters ()
:precondition
(and
(Flag51-)
(Flag51prime-)
)
:effect
(and
(Flag77-)
(Flag77prime-)
)

)
(:action Flag77Action-12
:parameters ()
:precondition
(and
(Flag52-)
(Flag52prime-)
)
:effect
(and
(Flag77-)
(Flag77prime-)
)

)
(:action Flag77Action-13
:parameters ()
:precondition
(and
(Flag53-)
(Flag53prime-)
)
:effect
(and
(Flag77-)
(Flag77prime-)
)

)
(:action Flag77Action-14
:parameters ()
:precondition
(and
(Flag54-)
(Flag54prime-)
)
:effect
(and
(Flag77-)
(Flag77prime-)
)

)
(:action Flag77Action-15
:parameters ()
:precondition
(and
(Flag55-)
(Flag55prime-)
)
:effect
(and
(Flag77-)
(Flag77prime-)
)

)
(:action Flag77Action-16
:parameters ()
:precondition
(and
(Flag56-)
(Flag56prime-)
)
:effect
(and
(Flag77-)
(Flag77prime-)
)

)
(:action Flag77Action-17
:parameters ()
:precondition
(and
(Flag57-)
(Flag57prime-)
)
:effect
(and
(Flag77-)
(Flag77prime-)
)

)
(:action Flag77Action-18
:parameters ()
:precondition
(and
(Flag58-)
(Flag58prime-)
)
:effect
(and
(Flag77-)
(Flag77prime-)
)

)
(:action Flag77Action-19
:parameters ()
:precondition
(and
(Flag59-)
(Flag59prime-)
)
:effect
(and
(Flag77-)
(Flag77prime-)
)

)
(:action Flag77Action-20
:parameters ()
:precondition
(and
(Flag60-)
(Flag60prime-)
)
:effect
(and
(Flag77-)
(Flag77prime-)
)

)
(:action Flag77Action-21
:parameters ()
:precondition
(and
(Flag61-)
(Flag61prime-)
)
:effect
(and
(Flag77-)
(Flag77prime-)
)

)
(:action Flag77Action-22
:parameters ()
:precondition
(and
(Flag62-)
(Flag62prime-)
)
:effect
(and
(Flag77-)
(Flag77prime-)
)

)
(:action Flag77Action-23
:parameters ()
:precondition
(and
(Flag63-)
(Flag63prime-)
)
:effect
(and
(Flag77-)
(Flag77prime-)
)

)
(:action Flag77Action-24
:parameters ()
:precondition
(and
(Flag64-)
(Flag64prime-)
)
:effect
(and
(Flag77-)
(Flag77prime-)
)

)
(:action Flag77Action-25
:parameters ()
:precondition
(and
(Flag65-)
(Flag65prime-)
)
:effect
(and
(Flag77-)
(Flag77prime-)
)

)
(:action Flag77Action-26
:parameters ()
:precondition
(and
(Flag66-)
(Flag66prime-)
)
:effect
(and
(Flag77-)
(Flag77prime-)
)

)
(:action Flag77Action-27
:parameters ()
:precondition
(and
(Flag67-)
(Flag67prime-)
)
:effect
(and
(Flag77-)
(Flag77prime-)
)

)
(:action Flag77Action-28
:parameters ()
:precondition
(and
(Flag68-)
(Flag68prime-)
)
:effect
(and
(Flag77-)
(Flag77prime-)
)

)
(:action Flag77Action-29
:parameters ()
:precondition
(and
(Flag69-)
(Flag69prime-)
)
:effect
(and
(Flag77-)
(Flag77prime-)
)

)
(:action Flag77Action-30
:parameters ()
:precondition
(and
(Flag70-)
(Flag70prime-)
)
:effect
(and
(Flag77-)
(Flag77prime-)
)

)
(:action Flag77Action-31
:parameters ()
:precondition
(and
(Flag71-)
(Flag71prime-)
)
:effect
(and
(Flag77-)
(Flag77prime-)
)

)
(:action Flag77Action-32
:parameters ()
:precondition
(and
(Flag72-)
(Flag72prime-)
)
:effect
(and
(Flag77-)
(Flag77prime-)
)

)
(:action Flag77Action-33
:parameters ()
:precondition
(and
(Flag73-)
(Flag73prime-)
)
:effect
(and
(Flag77-)
(Flag77prime-)
)

)
(:action Flag77Action-34
:parameters ()
:precondition
(and
(Flag74-)
(Flag74prime-)
)
:effect
(and
(Flag77-)
(Flag77prime-)
)

)
(:action Flag77Action-35
:parameters ()
:precondition
(and
(Flag75-)
(Flag75prime-)
)
:effect
(and
(Flag77-)
(Flag77prime-)
)

)
(:action Flag77Action-36
:parameters ()
:precondition
(and
(Flag76-)
(Flag76prime-)
)
:effect
(and
(Flag77-)
(Flag77prime-)
)

)
(:action Prim77Action
:parameters ()
:precondition
(and
(not (Flag40-))
(Flag40prime-)
(not (Flag41-))
(Flag41prime-)
(not (Flag42-))
(Flag42prime-)
(not (Flag43-))
(Flag43prime-)
(not (Flag44-))
(Flag44prime-)
(not (Flag45-))
(Flag45prime-)
(not (Flag46-))
(Flag46prime-)
(not (Flag47-))
(Flag47prime-)
(not (Flag48-))
(Flag48prime-)
(not (Flag49-))
(Flag49prime-)
(not (Flag50-))
(Flag50prime-)
(not (Flag51-))
(Flag51prime-)
(not (Flag52-))
(Flag52prime-)
(not (Flag53-))
(Flag53prime-)
(not (Flag54-))
(Flag54prime-)
(not (Flag55-))
(Flag55prime-)
(not (Flag56-))
(Flag56prime-)
(not (Flag57-))
(Flag57prime-)
(not (Flag58-))
(Flag58prime-)
(not (Flag59-))
(Flag59prime-)
(not (Flag60-))
(Flag60prime-)
(not (Flag61-))
(Flag61prime-)
(not (Flag62-))
(Flag62prime-)
(not (Flag63-))
(Flag63prime-)
(not (Flag64-))
(Flag64prime-)
(not (Flag65-))
(Flag65prime-)
(not (Flag66-))
(Flag66prime-)
(not (Flag67-))
(Flag67prime-)
(not (Flag68-))
(Flag68prime-)
(not (Flag69-))
(Flag69prime-)
(not (Flag70-))
(Flag70prime-)
(not (Flag71-))
(Flag71prime-)
(not (Flag72-))
(Flag72prime-)
(not (Flag73-))
(Flag73prime-)
(not (Flag74-))
(Flag74prime-)
(not (Flag75-))
(Flag75prime-)
(not (Flag76-))
(Flag76prime-)
)
:effect
(and
(Flag77prime-)
(not (Flag77-))
)

)
(:action Flag95Action-0
:parameters ()
:precondition
(and
(Flag40-)
(Flag40prime-)
)
:effect
(and
(Flag95-)
(Flag95prime-)
)

)
(:action Flag95Action-1
:parameters ()
:precondition
(and
(Flag41-)
(Flag41prime-)
)
:effect
(and
(Flag95-)
(Flag95prime-)
)

)
(:action Flag95Action-2
:parameters ()
:precondition
(and
(Flag42-)
(Flag42prime-)
)
:effect
(and
(Flag95-)
(Flag95prime-)
)

)
(:action Flag95Action-3
:parameters ()
:precondition
(and
(Flag43-)
(Flag43prime-)
)
:effect
(and
(Flag95-)
(Flag95prime-)
)

)
(:action Flag95Action-4
:parameters ()
:precondition
(and
(Flag78-)
(Flag78prime-)
)
:effect
(and
(Flag95-)
(Flag95prime-)
)

)
(:action Flag95Action-5
:parameters ()
:precondition
(and
(Flag44-)
(Flag44prime-)
)
:effect
(and
(Flag95-)
(Flag95prime-)
)

)
(:action Flag95Action-6
:parameters ()
:precondition
(and
(Flag45-)
(Flag45prime-)
)
:effect
(and
(Flag95-)
(Flag95prime-)
)

)
(:action Flag95Action-7
:parameters ()
:precondition
(and
(Flag79-)
(Flag79prime-)
)
:effect
(and
(Flag95-)
(Flag95prime-)
)

)
(:action Flag95Action-8
:parameters ()
:precondition
(and
(Flag66-)
(Flag66prime-)
)
:effect
(and
(Flag95-)
(Flag95prime-)
)

)
(:action Flag95Action-9
:parameters ()
:precondition
(and
(Flag80-)
(Flag80prime-)
)
:effect
(and
(Flag95-)
(Flag95prime-)
)

)
(:action Flag95Action-10
:parameters ()
:precondition
(and
(Flag47-)
(Flag47prime-)
)
:effect
(and
(Flag95-)
(Flag95prime-)
)

)
(:action Flag95Action-11
:parameters ()
:precondition
(and
(Flag48-)
(Flag48prime-)
)
:effect
(and
(Flag95-)
(Flag95prime-)
)

)
(:action Flag95Action-12
:parameters ()
:precondition
(and
(Flag81-)
(Flag81prime-)
)
:effect
(and
(Flag95-)
(Flag95prime-)
)

)
(:action Flag95Action-13
:parameters ()
:precondition
(and
(Flag50-)
(Flag50prime-)
)
:effect
(and
(Flag95-)
(Flag95prime-)
)

)
(:action Flag95Action-14
:parameters ()
:precondition
(and
(Flag62-)
(Flag62prime-)
)
:effect
(and
(Flag95-)
(Flag95prime-)
)

)
(:action Flag95Action-15
:parameters ()
:precondition
(and
(Flag53-)
(Flag53prime-)
)
:effect
(and
(Flag95-)
(Flag95prime-)
)

)
(:action Flag95Action-16
:parameters ()
:precondition
(and
(Flag54-)
(Flag54prime-)
)
:effect
(and
(Flag95-)
(Flag95prime-)
)

)
(:action Flag95Action-17
:parameters ()
:precondition
(and
(Flag82-)
(Flag82prime-)
)
:effect
(and
(Flag95-)
(Flag95prime-)
)

)
(:action Flag95Action-18
:parameters ()
:precondition
(and
(Flag55-)
(Flag55prime-)
)
:effect
(and
(Flag95-)
(Flag95prime-)
)

)
(:action Flag95Action-19
:parameters ()
:precondition
(and
(Flag56-)
(Flag56prime-)
)
:effect
(and
(Flag95-)
(Flag95prime-)
)

)
(:action Flag95Action-20
:parameters ()
:precondition
(and
(Flag64-)
(Flag64prime-)
)
:effect
(and
(Flag95-)
(Flag95prime-)
)

)
(:action Flag95Action-21
:parameters ()
:precondition
(and
(Flag57-)
(Flag57prime-)
)
:effect
(and
(Flag95-)
(Flag95prime-)
)

)
(:action Flag95Action-22
:parameters ()
:precondition
(and
(Flag83-)
(Flag83prime-)
)
:effect
(and
(Flag95-)
(Flag95prime-)
)

)
(:action Flag95Action-23
:parameters ()
:precondition
(and
(Flag58-)
(Flag58prime-)
)
:effect
(and
(Flag95-)
(Flag95prime-)
)

)
(:action Flag95Action-24
:parameters ()
:precondition
(and
(Flag59-)
(Flag59prime-)
)
:effect
(and
(Flag95-)
(Flag95prime-)
)

)
(:action Flag95Action-25
:parameters ()
:precondition
(and
(Flag60-)
(Flag60prime-)
)
:effect
(and
(Flag95-)
(Flag95prime-)
)

)
(:action Flag95Action-26
:parameters ()
:precondition
(and
(Flag61-)
(Flag61prime-)
)
:effect
(and
(Flag95-)
(Flag95prime-)
)

)
(:action Flag95Action-27
:parameters ()
:precondition
(and
(Flag84-)
(Flag84prime-)
)
:effect
(and
(Flag95-)
(Flag95prime-)
)

)
(:action Flag95Action-28
:parameters ()
:precondition
(and
(Flag85-)
(Flag85prime-)
)
:effect
(and
(Flag95-)
(Flag95prime-)
)

)
(:action Flag95Action-29
:parameters ()
:precondition
(and
(Flag52-)
(Flag52prime-)
)
:effect
(and
(Flag95-)
(Flag95prime-)
)

)
(:action Flag95Action-30
:parameters ()
:precondition
(and
(Flag63-)
(Flag63prime-)
)
:effect
(and
(Flag95-)
(Flag95prime-)
)

)
(:action Flag95Action-31
:parameters ()
:precondition
(and
(Flag46-)
(Flag46prime-)
)
:effect
(and
(Flag95-)
(Flag95prime-)
)

)
(:action Flag95Action-32
:parameters ()
:precondition
(and
(Flag86-)
(Flag86prime-)
)
:effect
(and
(Flag95-)
(Flag95prime-)
)

)
(:action Flag95Action-33
:parameters ()
:precondition
(and
(Flag87-)
(Flag87prime-)
)
:effect
(and
(Flag95-)
(Flag95prime-)
)

)
(:action Flag95Action-34
:parameters ()
:precondition
(and
(Flag65-)
(Flag65prime-)
)
:effect
(and
(Flag95-)
(Flag95prime-)
)

)
(:action Flag95Action-35
:parameters ()
:precondition
(and
(Flag88-)
(Flag88prime-)
)
:effect
(and
(Flag95-)
(Flag95prime-)
)

)
(:action Flag95Action-36
:parameters ()
:precondition
(and
(Flag89-)
(Flag89prime-)
)
:effect
(and
(Flag95-)
(Flag95prime-)
)

)
(:action Flag95Action-37
:parameters ()
:precondition
(and
(Flag90-)
(Flag90prime-)
)
:effect
(and
(Flag95-)
(Flag95prime-)
)

)
(:action Flag95Action-38
:parameters ()
:precondition
(and
(Flag91-)
(Flag91prime-)
)
:effect
(and
(Flag95-)
(Flag95prime-)
)

)
(:action Flag95Action-39
:parameters ()
:precondition
(and
(Flag92-)
(Flag92prime-)
)
:effect
(and
(Flag95-)
(Flag95prime-)
)

)
(:action Flag95Action-40
:parameters ()
:precondition
(and
(Flag67-)
(Flag67prime-)
)
:effect
(and
(Flag95-)
(Flag95prime-)
)

)
(:action Flag95Action-41
:parameters ()
:precondition
(and
(Flag68-)
(Flag68prime-)
)
:effect
(and
(Flag95-)
(Flag95prime-)
)

)
(:action Flag95Action-42
:parameters ()
:precondition
(and
(Flag69-)
(Flag69prime-)
)
:effect
(and
(Flag95-)
(Flag95prime-)
)

)
(:action Flag95Action-43
:parameters ()
:precondition
(and
(Flag70-)
(Flag70prime-)
)
:effect
(and
(Flag95-)
(Flag95prime-)
)

)
(:action Flag95Action-44
:parameters ()
:precondition
(and
(Flag71-)
(Flag71prime-)
)
:effect
(and
(Flag95-)
(Flag95prime-)
)

)
(:action Flag95Action-45
:parameters ()
:precondition
(and
(Flag72-)
(Flag72prime-)
)
:effect
(and
(Flag95-)
(Flag95prime-)
)

)
(:action Flag95Action-46
:parameters ()
:precondition
(and
(Flag73-)
(Flag73prime-)
)
:effect
(and
(Flag95-)
(Flag95prime-)
)

)
(:action Flag95Action-47
:parameters ()
:precondition
(and
(Flag74-)
(Flag74prime-)
)
:effect
(and
(Flag95-)
(Flag95prime-)
)

)
(:action Flag95Action-48
:parameters ()
:precondition
(and
(Flag75-)
(Flag75prime-)
)
:effect
(and
(Flag95-)
(Flag95prime-)
)

)
(:action Flag95Action-49
:parameters ()
:precondition
(and
(Flag93-)
(Flag93prime-)
)
:effect
(and
(Flag95-)
(Flag95prime-)
)

)
(:action Flag95Action-50
:parameters ()
:precondition
(and
(Flag76-)
(Flag76prime-)
)
:effect
(and
(Flag95-)
(Flag95prime-)
)

)
(:action Flag95Action-51
:parameters ()
:precondition
(and
(Flag94-)
(Flag94prime-)
)
:effect
(and
(Flag95-)
(Flag95prime-)
)

)
(:action Prim95Action
:parameters ()
:precondition
(and
(not (Flag40-))
(Flag40prime-)
(not (Flag41-))
(Flag41prime-)
(not (Flag42-))
(Flag42prime-)
(not (Flag43-))
(Flag43prime-)
(not (Flag78-))
(Flag78prime-)
(not (Flag44-))
(Flag44prime-)
(not (Flag45-))
(Flag45prime-)
(not (Flag79-))
(Flag79prime-)
(not (Flag66-))
(Flag66prime-)
(not (Flag80-))
(Flag80prime-)
(not (Flag47-))
(Flag47prime-)
(not (Flag48-))
(Flag48prime-)
(not (Flag81-))
(Flag81prime-)
(not (Flag50-))
(Flag50prime-)
(not (Flag62-))
(Flag62prime-)
(not (Flag53-))
(Flag53prime-)
(not (Flag54-))
(Flag54prime-)
(not (Flag82-))
(Flag82prime-)
(not (Flag55-))
(Flag55prime-)
(not (Flag56-))
(Flag56prime-)
(not (Flag64-))
(Flag64prime-)
(not (Flag57-))
(Flag57prime-)
(not (Flag83-))
(Flag83prime-)
(not (Flag58-))
(Flag58prime-)
(not (Flag59-))
(Flag59prime-)
(not (Flag60-))
(Flag60prime-)
(not (Flag61-))
(Flag61prime-)
(not (Flag84-))
(Flag84prime-)
(not (Flag85-))
(Flag85prime-)
(not (Flag52-))
(Flag52prime-)
(not (Flag63-))
(Flag63prime-)
(not (Flag46-))
(Flag46prime-)
(not (Flag86-))
(Flag86prime-)
(not (Flag87-))
(Flag87prime-)
(not (Flag65-))
(Flag65prime-)
(not (Flag88-))
(Flag88prime-)
(not (Flag89-))
(Flag89prime-)
(not (Flag90-))
(Flag90prime-)
(not (Flag91-))
(Flag91prime-)
(not (Flag92-))
(Flag92prime-)
(not (Flag67-))
(Flag67prime-)
(not (Flag68-))
(Flag68prime-)
(not (Flag69-))
(Flag69prime-)
(not (Flag70-))
(Flag70prime-)
(not (Flag71-))
(Flag71prime-)
(not (Flag72-))
(Flag72prime-)
(not (Flag73-))
(Flag73prime-)
(not (Flag74-))
(Flag74prime-)
(not (Flag75-))
(Flag75prime-)
(not (Flag93-))
(Flag93prime-)
(not (Flag76-))
(Flag76prime-)
(not (Flag94-))
(Flag94prime-)
)
:effect
(and
(Flag95prime-)
(not (Flag95-))
)

)
(:action Flag114Action-0
:parameters ()
:precondition
(and
(Flag40-)
(Flag40prime-)
)
:effect
(and
(Flag114-)
(Flag114prime-)
)

)
(:action Flag114Action-1
:parameters ()
:precondition
(and
(Flag96-)
(Flag96prime-)
)
:effect
(and
(Flag114-)
(Flag114prime-)
)

)
(:action Flag114Action-2
:parameters ()
:precondition
(and
(Flag41-)
(Flag41prime-)
)
:effect
(and
(Flag114-)
(Flag114prime-)
)

)
(:action Flag114Action-3
:parameters ()
:precondition
(and
(Flag42-)
(Flag42prime-)
)
:effect
(and
(Flag114-)
(Flag114prime-)
)

)
(:action Flag114Action-4
:parameters ()
:precondition
(and
(Flag43-)
(Flag43prime-)
)
:effect
(and
(Flag114-)
(Flag114prime-)
)

)
(:action Flag114Action-5
:parameters ()
:precondition
(and
(Flag78-)
(Flag78prime-)
)
:effect
(and
(Flag114-)
(Flag114prime-)
)

)
(:action Flag114Action-6
:parameters ()
:precondition
(and
(Flag44-)
(Flag44prime-)
)
:effect
(and
(Flag114-)
(Flag114prime-)
)

)
(:action Flag114Action-7
:parameters ()
:precondition
(and
(Flag45-)
(Flag45prime-)
)
:effect
(and
(Flag114-)
(Flag114prime-)
)

)
(:action Flag114Action-8
:parameters ()
:precondition
(and
(Flag79-)
(Flag79prime-)
)
:effect
(and
(Flag114-)
(Flag114prime-)
)

)
(:action Flag114Action-9
:parameters ()
:precondition
(and
(Flag73-)
(Flag73prime-)
)
:effect
(and
(Flag114-)
(Flag114prime-)
)

)
(:action Flag114Action-10
:parameters ()
:precondition
(and
(Flag97-)
(Flag97prime-)
)
:effect
(and
(Flag114-)
(Flag114prime-)
)

)
(:action Flag114Action-11
:parameters ()
:precondition
(and
(Flag80-)
(Flag80prime-)
)
:effect
(and
(Flag114-)
(Flag114prime-)
)

)
(:action Flag114Action-12
:parameters ()
:precondition
(and
(Flag47-)
(Flag47prime-)
)
:effect
(and
(Flag114-)
(Flag114prime-)
)

)
(:action Flag114Action-13
:parameters ()
:precondition
(and
(Flag48-)
(Flag48prime-)
)
:effect
(and
(Flag114-)
(Flag114prime-)
)

)
(:action Flag114Action-14
:parameters ()
:precondition
(and
(Flag98-)
(Flag98prime-)
)
:effect
(and
(Flag114-)
(Flag114prime-)
)

)
(:action Flag114Action-15
:parameters ()
:precondition
(and
(Flag99-)
(Flag99prime-)
)
:effect
(and
(Flag114-)
(Flag114prime-)
)

)
(:action Flag114Action-16
:parameters ()
:precondition
(and
(Flag81-)
(Flag81prime-)
)
:effect
(and
(Flag114-)
(Flag114prime-)
)

)
(:action Flag114Action-17
:parameters ()
:precondition
(and
(Flag100-)
(Flag100prime-)
)
:effect
(and
(Flag114-)
(Flag114prime-)
)

)
(:action Flag114Action-18
:parameters ()
:precondition
(and
(Flag50-)
(Flag50prime-)
)
:effect
(and
(Flag114-)
(Flag114prime-)
)

)
(:action Flag114Action-19
:parameters ()
:precondition
(and
(Flag85-)
(Flag85prime-)
)
:effect
(and
(Flag114-)
(Flag114prime-)
)

)
(:action Flag114Action-20
:parameters ()
:precondition
(and
(Flag62-)
(Flag62prime-)
)
:effect
(and
(Flag114-)
(Flag114prime-)
)

)
(:action Flag114Action-21
:parameters ()
:precondition
(and
(Flag53-)
(Flag53prime-)
)
:effect
(and
(Flag114-)
(Flag114prime-)
)

)
(:action Flag114Action-22
:parameters ()
:precondition
(and
(Flag54-)
(Flag54prime-)
)
:effect
(and
(Flag114-)
(Flag114prime-)
)

)
(:action Flag114Action-23
:parameters ()
:precondition
(and
(Flag101-)
(Flag101prime-)
)
:effect
(and
(Flag114-)
(Flag114prime-)
)

)
(:action Flag114Action-24
:parameters ()
:precondition
(and
(Flag82-)
(Flag82prime-)
)
:effect
(and
(Flag114-)
(Flag114prime-)
)

)
(:action Flag114Action-25
:parameters ()
:precondition
(and
(Flag55-)
(Flag55prime-)
)
:effect
(and
(Flag114-)
(Flag114prime-)
)

)
(:action Flag114Action-26
:parameters ()
:precondition
(and
(Flag56-)
(Flag56prime-)
)
:effect
(and
(Flag114-)
(Flag114prime-)
)

)
(:action Flag114Action-27
:parameters ()
:precondition
(and
(Flag64-)
(Flag64prime-)
)
:effect
(and
(Flag114-)
(Flag114prime-)
)

)
(:action Flag114Action-28
:parameters ()
:precondition
(and
(Flag57-)
(Flag57prime-)
)
:effect
(and
(Flag114-)
(Flag114prime-)
)

)
(:action Flag114Action-29
:parameters ()
:precondition
(and
(Flag83-)
(Flag83prime-)
)
:effect
(and
(Flag114-)
(Flag114prime-)
)

)
(:action Flag114Action-30
:parameters ()
:precondition
(and
(Flag58-)
(Flag58prime-)
)
:effect
(and
(Flag114-)
(Flag114prime-)
)

)
(:action Flag114Action-31
:parameters ()
:precondition
(and
(Flag59-)
(Flag59prime-)
)
:effect
(and
(Flag114-)
(Flag114prime-)
)

)
(:action Flag114Action-32
:parameters ()
:precondition
(and
(Flag102-)
(Flag102prime-)
)
:effect
(and
(Flag114-)
(Flag114prime-)
)

)
(:action Flag114Action-33
:parameters ()
:precondition
(and
(Flag60-)
(Flag60prime-)
)
:effect
(and
(Flag114-)
(Flag114prime-)
)

)
(:action Flag114Action-34
:parameters ()
:precondition
(and
(Flag61-)
(Flag61prime-)
)
:effect
(and
(Flag114-)
(Flag114prime-)
)

)
(:action Flag114Action-35
:parameters ()
:precondition
(and
(Flag84-)
(Flag84prime-)
)
:effect
(and
(Flag114-)
(Flag114prime-)
)

)
(:action Flag114Action-36
:parameters ()
:precondition
(and
(Flag103-)
(Flag103prime-)
)
:effect
(and
(Flag114-)
(Flag114prime-)
)

)
(:action Flag114Action-37
:parameters ()
:precondition
(and
(Flag52-)
(Flag52prime-)
)
:effect
(and
(Flag114-)
(Flag114prime-)
)

)
(:action Flag114Action-38
:parameters ()
:precondition
(and
(Flag63-)
(Flag63prime-)
)
:effect
(and
(Flag114-)
(Flag114prime-)
)

)
(:action Flag114Action-39
:parameters ()
:precondition
(and
(Flag104-)
(Flag104prime-)
)
:effect
(and
(Flag114-)
(Flag114prime-)
)

)
(:action Flag114Action-40
:parameters ()
:precondition
(and
(Flag46-)
(Flag46prime-)
)
:effect
(and
(Flag114-)
(Flag114prime-)
)

)
(:action Flag114Action-41
:parameters ()
:precondition
(and
(Flag74-)
(Flag74prime-)
)
:effect
(and
(Flag114-)
(Flag114prime-)
)

)
(:action Flag114Action-42
:parameters ()
:precondition
(and
(Flag86-)
(Flag86prime-)
)
:effect
(and
(Flag114-)
(Flag114prime-)
)

)
(:action Flag114Action-43
:parameters ()
:precondition
(and
(Flag87-)
(Flag87prime-)
)
:effect
(and
(Flag114-)
(Flag114prime-)
)

)
(:action Flag114Action-44
:parameters ()
:precondition
(and
(Flag65-)
(Flag65prime-)
)
:effect
(and
(Flag114-)
(Flag114prime-)
)

)
(:action Flag114Action-45
:parameters ()
:precondition
(and
(Flag66-)
(Flag66prime-)
)
:effect
(and
(Flag114-)
(Flag114prime-)
)

)
(:action Flag114Action-46
:parameters ()
:precondition
(and
(Flag89-)
(Flag89prime-)
)
:effect
(and
(Flag114-)
(Flag114prime-)
)

)
(:action Flag114Action-47
:parameters ()
:precondition
(and
(Flag105-)
(Flag105prime-)
)
:effect
(and
(Flag114-)
(Flag114prime-)
)

)
(:action Flag114Action-48
:parameters ()
:precondition
(and
(Flag106-)
(Flag106prime-)
)
:effect
(and
(Flag114-)
(Flag114prime-)
)

)
(:action Flag114Action-49
:parameters ()
:precondition
(and
(Flag90-)
(Flag90prime-)
)
:effect
(and
(Flag114-)
(Flag114prime-)
)

)
(:action Flag114Action-50
:parameters ()
:precondition
(and
(Flag91-)
(Flag91prime-)
)
:effect
(and
(Flag114-)
(Flag114prime-)
)

)
(:action Flag114Action-51
:parameters ()
:precondition
(and
(Flag92-)
(Flag92prime-)
)
:effect
(and
(Flag114-)
(Flag114prime-)
)

)
(:action Flag114Action-52
:parameters ()
:precondition
(and
(Flag107-)
(Flag107prime-)
)
:effect
(and
(Flag114-)
(Flag114prime-)
)

)
(:action Flag114Action-53
:parameters ()
:precondition
(and
(Flag108-)
(Flag108prime-)
)
:effect
(and
(Flag114-)
(Flag114prime-)
)

)
(:action Flag114Action-54
:parameters ()
:precondition
(and
(Flag67-)
(Flag67prime-)
)
:effect
(and
(Flag114-)
(Flag114prime-)
)

)
(:action Flag114Action-55
:parameters ()
:precondition
(and
(Flag68-)
(Flag68prime-)
)
:effect
(and
(Flag114-)
(Flag114prime-)
)

)
(:action Flag114Action-56
:parameters ()
:precondition
(and
(Flag109-)
(Flag109prime-)
)
:effect
(and
(Flag114-)
(Flag114prime-)
)

)
(:action Flag114Action-57
:parameters ()
:precondition
(and
(Flag110-)
(Flag110prime-)
)
:effect
(and
(Flag114-)
(Flag114prime-)
)

)
(:action Flag114Action-58
:parameters ()
:precondition
(and
(Flag69-)
(Flag69prime-)
)
:effect
(and
(Flag114-)
(Flag114prime-)
)

)
(:action Flag114Action-59
:parameters ()
:precondition
(and
(Flag111-)
(Flag111prime-)
)
:effect
(and
(Flag114-)
(Flag114prime-)
)

)
(:action Flag114Action-60
:parameters ()
:precondition
(and
(Flag72-)
(Flag72prime-)
)
:effect
(and
(Flag114-)
(Flag114prime-)
)

)
(:action Flag114Action-61
:parameters ()
:precondition
(and
(Flag112-)
(Flag112prime-)
)
:effect
(and
(Flag114-)
(Flag114prime-)
)

)
(:action Flag114Action-62
:parameters ()
:precondition
(and
(Flag113-)
(Flag113prime-)
)
:effect
(and
(Flag114-)
(Flag114prime-)
)

)
(:action Flag114Action-63
:parameters ()
:precondition
(and
(Flag75-)
(Flag75prime-)
)
:effect
(and
(Flag114-)
(Flag114prime-)
)

)
(:action Flag114Action-64
:parameters ()
:precondition
(and
(Flag93-)
(Flag93prime-)
)
:effect
(and
(Flag114-)
(Flag114prime-)
)

)
(:action Flag114Action-65
:parameters ()
:precondition
(and
(Flag76-)
(Flag76prime-)
)
:effect
(and
(Flag114-)
(Flag114prime-)
)

)
(:action Prim114Action
:parameters ()
:precondition
(and
(not (Flag40-))
(Flag40prime-)
(not (Flag96-))
(Flag96prime-)
(not (Flag41-))
(Flag41prime-)
(not (Flag42-))
(Flag42prime-)
(not (Flag43-))
(Flag43prime-)
(not (Flag78-))
(Flag78prime-)
(not (Flag44-))
(Flag44prime-)
(not (Flag45-))
(Flag45prime-)
(not (Flag79-))
(Flag79prime-)
(not (Flag73-))
(Flag73prime-)
(not (Flag97-))
(Flag97prime-)
(not (Flag80-))
(Flag80prime-)
(not (Flag47-))
(Flag47prime-)
(not (Flag48-))
(Flag48prime-)
(not (Flag98-))
(Flag98prime-)
(not (Flag99-))
(Flag99prime-)
(not (Flag81-))
(Flag81prime-)
(not (Flag100-))
(Flag100prime-)
(not (Flag50-))
(Flag50prime-)
(not (Flag85-))
(Flag85prime-)
(not (Flag62-))
(Flag62prime-)
(not (Flag53-))
(Flag53prime-)
(not (Flag54-))
(Flag54prime-)
(not (Flag101-))
(Flag101prime-)
(not (Flag82-))
(Flag82prime-)
(not (Flag55-))
(Flag55prime-)
(not (Flag56-))
(Flag56prime-)
(not (Flag64-))
(Flag64prime-)
(not (Flag57-))
(Flag57prime-)
(not (Flag83-))
(Flag83prime-)
(not (Flag58-))
(Flag58prime-)
(not (Flag59-))
(Flag59prime-)
(not (Flag102-))
(Flag102prime-)
(not (Flag60-))
(Flag60prime-)
(not (Flag61-))
(Flag61prime-)
(not (Flag84-))
(Flag84prime-)
(not (Flag103-))
(Flag103prime-)
(not (Flag52-))
(Flag52prime-)
(not (Flag63-))
(Flag63prime-)
(not (Flag104-))
(Flag104prime-)
(not (Flag46-))
(Flag46prime-)
(not (Flag74-))
(Flag74prime-)
(not (Flag86-))
(Flag86prime-)
(not (Flag87-))
(Flag87prime-)
(not (Flag65-))
(Flag65prime-)
(not (Flag66-))
(Flag66prime-)
(not (Flag89-))
(Flag89prime-)
(not (Flag105-))
(Flag105prime-)
(not (Flag106-))
(Flag106prime-)
(not (Flag90-))
(Flag90prime-)
(not (Flag91-))
(Flag91prime-)
(not (Flag92-))
(Flag92prime-)
(not (Flag107-))
(Flag107prime-)
(not (Flag108-))
(Flag108prime-)
(not (Flag67-))
(Flag67prime-)
(not (Flag68-))
(Flag68prime-)
(not (Flag109-))
(Flag109prime-)
(not (Flag110-))
(Flag110prime-)
(not (Flag69-))
(Flag69prime-)
(not (Flag111-))
(Flag111prime-)
(not (Flag72-))
(Flag72prime-)
(not (Flag112-))
(Flag112prime-)
(not (Flag113-))
(Flag113prime-)
(not (Flag75-))
(Flag75prime-)
(not (Flag93-))
(Flag93prime-)
(not (Flag76-))
(Flag76prime-)
)
:effect
(and
(Flag114prime-)
(not (Flag114-))
)

)
(:action Flag133Action-0
:parameters ()
:precondition
(and
(Flag115-)
(Flag115prime-)
)
:effect
(and
(Flag133-)
(Flag133prime-)
)

)
(:action Flag133Action-1
:parameters ()
:precondition
(and
(Flag96-)
(Flag96prime-)
)
:effect
(and
(Flag133-)
(Flag133prime-)
)

)
(:action Flag133Action-2
:parameters ()
:precondition
(and
(Flag116-)
(Flag116prime-)
)
:effect
(and
(Flag133-)
(Flag133prime-)
)

)
(:action Flag133Action-3
:parameters ()
:precondition
(and
(Flag42-)
(Flag42prime-)
)
:effect
(and
(Flag133-)
(Flag133prime-)
)

)
(:action Flag133Action-4
:parameters ()
:precondition
(and
(Flag117-)
(Flag117prime-)
)
:effect
(and
(Flag133-)
(Flag133prime-)
)

)
(:action Flag133Action-5
:parameters ()
:precondition
(and
(Flag43-)
(Flag43prime-)
)
:effect
(and
(Flag133-)
(Flag133prime-)
)

)
(:action Flag133Action-6
:parameters ()
:precondition
(and
(Flag78-)
(Flag78prime-)
)
:effect
(and
(Flag133-)
(Flag133prime-)
)

)
(:action Flag133Action-7
:parameters ()
:precondition
(and
(Flag44-)
(Flag44prime-)
)
:effect
(and
(Flag133-)
(Flag133prime-)
)

)
(:action Flag133Action-8
:parameters ()
:precondition
(and
(Flag79-)
(Flag79prime-)
)
:effect
(and
(Flag133-)
(Flag133prime-)
)

)
(:action Flag133Action-9
:parameters ()
:precondition
(and
(Flag73-)
(Flag73prime-)
)
:effect
(and
(Flag133-)
(Flag133prime-)
)

)
(:action Flag133Action-10
:parameters ()
:precondition
(and
(Flag118-)
(Flag118prime-)
)
:effect
(and
(Flag133-)
(Flag133prime-)
)

)
(:action Flag133Action-11
:parameters ()
:precondition
(and
(Flag119-)
(Flag119prime-)
)
:effect
(and
(Flag133-)
(Flag133prime-)
)

)
(:action Flag133Action-12
:parameters ()
:precondition
(and
(Flag97-)
(Flag97prime-)
)
:effect
(and
(Flag133-)
(Flag133prime-)
)

)
(:action Flag133Action-13
:parameters ()
:precondition
(and
(Flag120-)
(Flag120prime-)
)
:effect
(and
(Flag133-)
(Flag133prime-)
)

)
(:action Flag133Action-14
:parameters ()
:precondition
(and
(Flag80-)
(Flag80prime-)
)
:effect
(and
(Flag133-)
(Flag133prime-)
)

)
(:action Flag133Action-15
:parameters ()
:precondition
(and
(Flag47-)
(Flag47prime-)
)
:effect
(and
(Flag133-)
(Flag133prime-)
)

)
(:action Flag133Action-16
:parameters ()
:precondition
(and
(Flag48-)
(Flag48prime-)
)
:effect
(and
(Flag133-)
(Flag133prime-)
)

)
(:action Flag133Action-17
:parameters ()
:precondition
(and
(Flag65-)
(Flag65prime-)
)
:effect
(and
(Flag133-)
(Flag133prime-)
)

)
(:action Flag133Action-18
:parameters ()
:precondition
(and
(Flag121-)
(Flag121prime-)
)
:effect
(and
(Flag133-)
(Flag133prime-)
)

)
(:action Flag133Action-19
:parameters ()
:precondition
(and
(Flag98-)
(Flag98prime-)
)
:effect
(and
(Flag133-)
(Flag133prime-)
)

)
(:action Flag133Action-20
:parameters ()
:precondition
(and
(Flag99-)
(Flag99prime-)
)
:effect
(and
(Flag133-)
(Flag133prime-)
)

)
(:action Flag133Action-21
:parameters ()
:precondition
(and
(Flag122-)
(Flag122prime-)
)
:effect
(and
(Flag133-)
(Flag133prime-)
)

)
(:action Flag133Action-22
:parameters ()
:precondition
(and
(Flag81-)
(Flag81prime-)
)
:effect
(and
(Flag133-)
(Flag133prime-)
)

)
(:action Flag133Action-23
:parameters ()
:precondition
(and
(Flag40-)
(Flag40prime-)
)
:effect
(and
(Flag133-)
(Flag133prime-)
)

)
(:action Flag133Action-24
:parameters ()
:precondition
(and
(Flag100-)
(Flag100prime-)
)
:effect
(and
(Flag133-)
(Flag133prime-)
)

)
(:action Flag133Action-25
:parameters ()
:precondition
(and
(Flag50-)
(Flag50prime-)
)
:effect
(and
(Flag133-)
(Flag133prime-)
)

)
(:action Flag133Action-26
:parameters ()
:precondition
(and
(Flag46-)
(Flag46prime-)
)
:effect
(and
(Flag133-)
(Flag133prime-)
)

)
(:action Flag133Action-27
:parameters ()
:precondition
(and
(Flag85-)
(Flag85prime-)
)
:effect
(and
(Flag133-)
(Flag133prime-)
)

)
(:action Flag133Action-28
:parameters ()
:precondition
(and
(Flag62-)
(Flag62prime-)
)
:effect
(and
(Flag133-)
(Flag133prime-)
)

)
(:action Flag133Action-29
:parameters ()
:precondition
(and
(Flag53-)
(Flag53prime-)
)
:effect
(and
(Flag133-)
(Flag133prime-)
)

)
(:action Flag133Action-30
:parameters ()
:precondition
(and
(Flag54-)
(Flag54prime-)
)
:effect
(and
(Flag133-)
(Flag133prime-)
)

)
(:action Flag133Action-31
:parameters ()
:precondition
(and
(Flag101-)
(Flag101prime-)
)
:effect
(and
(Flag133-)
(Flag133prime-)
)

)
(:action Flag133Action-32
:parameters ()
:precondition
(and
(Flag82-)
(Flag82prime-)
)
:effect
(and
(Flag133-)
(Flag133prime-)
)

)
(:action Flag133Action-33
:parameters ()
:precondition
(and
(Flag55-)
(Flag55prime-)
)
:effect
(and
(Flag133-)
(Flag133prime-)
)

)
(:action Flag133Action-34
:parameters ()
:precondition
(and
(Flag102-)
(Flag102prime-)
)
:effect
(and
(Flag133-)
(Flag133prime-)
)

)
(:action Flag133Action-35
:parameters ()
:precondition
(and
(Flag57-)
(Flag57prime-)
)
:effect
(and
(Flag133-)
(Flag133prime-)
)

)
(:action Flag133Action-36
:parameters ()
:precondition
(and
(Flag87-)
(Flag87prime-)
)
:effect
(and
(Flag133-)
(Flag133prime-)
)

)
(:action Flag133Action-37
:parameters ()
:precondition
(and
(Flag123-)
(Flag123prime-)
)
:effect
(and
(Flag133-)
(Flag133prime-)
)

)
(:action Flag133Action-38
:parameters ()
:precondition
(and
(Flag124-)
(Flag124prime-)
)
:effect
(and
(Flag133-)
(Flag133prime-)
)

)
(:action Flag133Action-39
:parameters ()
:precondition
(and
(Flag125-)
(Flag125prime-)
)
:effect
(and
(Flag133-)
(Flag133prime-)
)

)
(:action Flag133Action-40
:parameters ()
:precondition
(and
(Flag59-)
(Flag59prime-)
)
:effect
(and
(Flag133-)
(Flag133prime-)
)

)
(:action Flag133Action-41
:parameters ()
:precondition
(and
(Flag126-)
(Flag126prime-)
)
:effect
(and
(Flag133-)
(Flag133prime-)
)

)
(:action Flag133Action-42
:parameters ()
:precondition
(and
(Flag60-)
(Flag60prime-)
)
:effect
(and
(Flag133-)
(Flag133prime-)
)

)
(:action Flag133Action-43
:parameters ()
:precondition
(and
(Flag61-)
(Flag61prime-)
)
:effect
(and
(Flag133-)
(Flag133prime-)
)

)
(:action Flag133Action-44
:parameters ()
:precondition
(and
(Flag84-)
(Flag84prime-)
)
:effect
(and
(Flag133-)
(Flag133prime-)
)

)
(:action Flag133Action-45
:parameters ()
:precondition
(and
(Flag127-)
(Flag127prime-)
)
:effect
(and
(Flag133-)
(Flag133prime-)
)

)
(:action Flag133Action-46
:parameters ()
:precondition
(and
(Flag52-)
(Flag52prime-)
)
:effect
(and
(Flag133-)
(Flag133prime-)
)

)
(:action Flag133Action-47
:parameters ()
:precondition
(and
(Flag63-)
(Flag63prime-)
)
:effect
(and
(Flag133-)
(Flag133prime-)
)

)
(:action Flag133Action-48
:parameters ()
:precondition
(and
(Flag104-)
(Flag104prime-)
)
:effect
(and
(Flag133-)
(Flag133prime-)
)

)
(:action Flag133Action-49
:parameters ()
:precondition
(and
(Flag128-)
(Flag128prime-)
)
:effect
(and
(Flag133-)
(Flag133prime-)
)

)
(:action Flag133Action-50
:parameters ()
:precondition
(and
(Flag129-)
(Flag129prime-)
)
:effect
(and
(Flag133-)
(Flag133prime-)
)

)
(:action Flag133Action-51
:parameters ()
:precondition
(and
(Flag74-)
(Flag74prime-)
)
:effect
(and
(Flag133-)
(Flag133prime-)
)

)
(:action Flag133Action-52
:parameters ()
:precondition
(and
(Flag64-)
(Flag64prime-)
)
:effect
(and
(Flag133-)
(Flag133prime-)
)

)
(:action Flag133Action-53
:parameters ()
:precondition
(and
(Flag83-)
(Flag83prime-)
)
:effect
(and
(Flag133-)
(Flag133prime-)
)

)
(:action Flag133Action-54
:parameters ()
:precondition
(and
(Flag58-)
(Flag58prime-)
)
:effect
(and
(Flag133-)
(Flag133prime-)
)

)
(:action Flag133Action-55
:parameters ()
:precondition
(and
(Flag66-)
(Flag66prime-)
)
:effect
(and
(Flag133-)
(Flag133prime-)
)

)
(:action Flag133Action-56
:parameters ()
:precondition
(and
(Flag89-)
(Flag89prime-)
)
:effect
(and
(Flag133-)
(Flag133prime-)
)

)
(:action Flag133Action-57
:parameters ()
:precondition
(and
(Flag105-)
(Flag105prime-)
)
:effect
(and
(Flag133-)
(Flag133prime-)
)

)
(:action Flag133Action-58
:parameters ()
:precondition
(and
(Flag106-)
(Flag106prime-)
)
:effect
(and
(Flag133-)
(Flag133prime-)
)

)
(:action Flag133Action-59
:parameters ()
:precondition
(and
(Flag90-)
(Flag90prime-)
)
:effect
(and
(Flag133-)
(Flag133prime-)
)

)
(:action Flag133Action-60
:parameters ()
:precondition
(and
(Flag91-)
(Flag91prime-)
)
:effect
(and
(Flag133-)
(Flag133prime-)
)

)
(:action Flag133Action-61
:parameters ()
:precondition
(and
(Flag92-)
(Flag92prime-)
)
:effect
(and
(Flag133-)
(Flag133prime-)
)

)
(:action Flag133Action-62
:parameters ()
:precondition
(and
(Flag41-)
(Flag41prime-)
)
:effect
(and
(Flag133-)
(Flag133prime-)
)

)
(:action Flag133Action-63
:parameters ()
:precondition
(and
(Flag107-)
(Flag107prime-)
)
:effect
(and
(Flag133-)
(Flag133prime-)
)

)
(:action Flag133Action-64
:parameters ()
:precondition
(and
(Flag108-)
(Flag108prime-)
)
:effect
(and
(Flag133-)
(Flag133prime-)
)

)
(:action Flag133Action-65
:parameters ()
:precondition
(and
(Flag67-)
(Flag67prime-)
)
:effect
(and
(Flag133-)
(Flag133prime-)
)

)
(:action Flag133Action-66
:parameters ()
:precondition
(and
(Flag68-)
(Flag68prime-)
)
:effect
(and
(Flag133-)
(Flag133prime-)
)

)
(:action Flag133Action-67
:parameters ()
:precondition
(and
(Flag109-)
(Flag109prime-)
)
:effect
(and
(Flag133-)
(Flag133prime-)
)

)
(:action Flag133Action-68
:parameters ()
:precondition
(and
(Flag110-)
(Flag110prime-)
)
:effect
(and
(Flag133-)
(Flag133prime-)
)

)
(:action Flag133Action-69
:parameters ()
:precondition
(and
(Flag69-)
(Flag69prime-)
)
:effect
(and
(Flag133-)
(Flag133prime-)
)

)
(:action Flag133Action-70
:parameters ()
:precondition
(and
(Flag111-)
(Flag111prime-)
)
:effect
(and
(Flag133-)
(Flag133prime-)
)

)
(:action Flag133Action-71
:parameters ()
:precondition
(and
(Flag130-)
(Flag130prime-)
)
:effect
(and
(Flag133-)
(Flag133prime-)
)

)
(:action Flag133Action-72
:parameters ()
:precondition
(and
(Flag86-)
(Flag86prime-)
)
:effect
(and
(Flag133-)
(Flag133prime-)
)

)
(:action Flag133Action-73
:parameters ()
:precondition
(and
(Flag72-)
(Flag72prime-)
)
:effect
(and
(Flag133-)
(Flag133prime-)
)

)
(:action Flag133Action-74
:parameters ()
:precondition
(and
(Flag131-)
(Flag131prime-)
)
:effect
(and
(Flag133-)
(Flag133prime-)
)

)
(:action Flag133Action-75
:parameters ()
:precondition
(and
(Flag113-)
(Flag113prime-)
)
:effect
(and
(Flag133-)
(Flag133prime-)
)

)
(:action Flag133Action-76
:parameters ()
:precondition
(and
(Flag75-)
(Flag75prime-)
)
:effect
(and
(Flag133-)
(Flag133prime-)
)

)
(:action Flag133Action-77
:parameters ()
:precondition
(and
(Flag93-)
(Flag93prime-)
)
:effect
(and
(Flag133-)
(Flag133prime-)
)

)
(:action Flag133Action-78
:parameters ()
:precondition
(and
(Flag76-)
(Flag76prime-)
)
:effect
(and
(Flag133-)
(Flag133prime-)
)

)
(:action Flag133Action-79
:parameters ()
:precondition
(and
(Flag132-)
(Flag132prime-)
)
:effect
(and
(Flag133-)
(Flag133prime-)
)

)
(:action Prim133Action
:parameters ()
:precondition
(and
(not (Flag115-))
(Flag115prime-)
(not (Flag96-))
(Flag96prime-)
(not (Flag116-))
(Flag116prime-)
(not (Flag42-))
(Flag42prime-)
(not (Flag117-))
(Flag117prime-)
(not (Flag43-))
(Flag43prime-)
(not (Flag78-))
(Flag78prime-)
(not (Flag44-))
(Flag44prime-)
(not (Flag79-))
(Flag79prime-)
(not (Flag73-))
(Flag73prime-)
(not (Flag118-))
(Flag118prime-)
(not (Flag119-))
(Flag119prime-)
(not (Flag97-))
(Flag97prime-)
(not (Flag120-))
(Flag120prime-)
(not (Flag80-))
(Flag80prime-)
(not (Flag47-))
(Flag47prime-)
(not (Flag48-))
(Flag48prime-)
(not (Flag65-))
(Flag65prime-)
(not (Flag121-))
(Flag121prime-)
(not (Flag98-))
(Flag98prime-)
(not (Flag99-))
(Flag99prime-)
(not (Flag122-))
(Flag122prime-)
(not (Flag81-))
(Flag81prime-)
(not (Flag40-))
(Flag40prime-)
(not (Flag100-))
(Flag100prime-)
(not (Flag50-))
(Flag50prime-)
(not (Flag46-))
(Flag46prime-)
(not (Flag85-))
(Flag85prime-)
(not (Flag62-))
(Flag62prime-)
(not (Flag53-))
(Flag53prime-)
(not (Flag54-))
(Flag54prime-)
(not (Flag101-))
(Flag101prime-)
(not (Flag82-))
(Flag82prime-)
(not (Flag55-))
(Flag55prime-)
(not (Flag102-))
(Flag102prime-)
(not (Flag57-))
(Flag57prime-)
(not (Flag87-))
(Flag87prime-)
(not (Flag123-))
(Flag123prime-)
(not (Flag124-))
(Flag124prime-)
(not (Flag125-))
(Flag125prime-)
(not (Flag59-))
(Flag59prime-)
(not (Flag126-))
(Flag126prime-)
(not (Flag60-))
(Flag60prime-)
(not (Flag61-))
(Flag61prime-)
(not (Flag84-))
(Flag84prime-)
(not (Flag127-))
(Flag127prime-)
(not (Flag52-))
(Flag52prime-)
(not (Flag63-))
(Flag63prime-)
(not (Flag104-))
(Flag104prime-)
(not (Flag128-))
(Flag128prime-)
(not (Flag129-))
(Flag129prime-)
(not (Flag74-))
(Flag74prime-)
(not (Flag64-))
(Flag64prime-)
(not (Flag83-))
(Flag83prime-)
(not (Flag58-))
(Flag58prime-)
(not (Flag66-))
(Flag66prime-)
(not (Flag89-))
(Flag89prime-)
(not (Flag105-))
(Flag105prime-)
(not (Flag106-))
(Flag106prime-)
(not (Flag90-))
(Flag90prime-)
(not (Flag91-))
(Flag91prime-)
(not (Flag92-))
(Flag92prime-)
(not (Flag41-))
(Flag41prime-)
(not (Flag107-))
(Flag107prime-)
(not (Flag108-))
(Flag108prime-)
(not (Flag67-))
(Flag67prime-)
(not (Flag68-))
(Flag68prime-)
(not (Flag109-))
(Flag109prime-)
(not (Flag110-))
(Flag110prime-)
(not (Flag69-))
(Flag69prime-)
(not (Flag111-))
(Flag111prime-)
(not (Flag130-))
(Flag130prime-)
(not (Flag86-))
(Flag86prime-)
(not (Flag72-))
(Flag72prime-)
(not (Flag131-))
(Flag131prime-)
(not (Flag113-))
(Flag113prime-)
(not (Flag75-))
(Flag75prime-)
(not (Flag93-))
(Flag93prime-)
(not (Flag76-))
(Flag76prime-)
(not (Flag132-))
(Flag132prime-)
)
:effect
(and
(Flag133prime-)
(not (Flag133-))
)

)
(:action Flag148Action-0
:parameters ()
:precondition
(and
(Flag41-)
(Flag41prime-)
)
:effect
(and
(Flag148-)
(Flag148prime-)
)

)
(:action Flag148Action-1
:parameters ()
:precondition
(and
(Flag47-)
(Flag47prime-)
)
:effect
(and
(Flag148-)
(Flag148prime-)
)

)
(:action Flag148Action-2
:parameters ()
:precondition
(and
(Flag134-)
(Flag134prime-)
)
:effect
(and
(Flag148-)
(Flag148prime-)
)

)
(:action Flag148Action-3
:parameters ()
:precondition
(and
(Flag135-)
(Flag135prime-)
)
:effect
(and
(Flag148-)
(Flag148prime-)
)

)
(:action Flag148Action-4
:parameters ()
:precondition
(and
(Flag124-)
(Flag124prime-)
)
:effect
(and
(Flag148-)
(Flag148prime-)
)

)
(:action Flag148Action-5
:parameters ()
:precondition
(and
(Flag125-)
(Flag125prime-)
)
:effect
(and
(Flag148-)
(Flag148prime-)
)

)
(:action Flag148Action-6
:parameters ()
:precondition
(and
(Flag126-)
(Flag126prime-)
)
:effect
(and
(Flag148-)
(Flag148prime-)
)

)
(:action Flag148Action-7
:parameters ()
:precondition
(and
(Flag84-)
(Flag84prime-)
)
:effect
(and
(Flag148-)
(Flag148prime-)
)

)
(:action Flag148Action-8
:parameters ()
:precondition
(and
(Flag127-)
(Flag127prime-)
)
:effect
(and
(Flag148-)
(Flag148prime-)
)

)
(:action Flag148Action-9
:parameters ()
:precondition
(and
(Flag63-)
(Flag63prime-)
)
:effect
(and
(Flag148-)
(Flag148prime-)
)

)
(:action Flag148Action-10
:parameters ()
:precondition
(and
(Flag136-)
(Flag136prime-)
)
:effect
(and
(Flag148-)
(Flag148prime-)
)

)
(:action Flag148Action-11
:parameters ()
:precondition
(and
(Flag43-)
(Flag43prime-)
)
:effect
(and
(Flag148-)
(Flag148prime-)
)

)
(:action Flag148Action-12
:parameters ()
:precondition
(and
(Flag92-)
(Flag92prime-)
)
:effect
(and
(Flag148-)
(Flag148prime-)
)

)
(:action Flag148Action-13
:parameters ()
:precondition
(and
(Flag69-)
(Flag69prime-)
)
:effect
(and
(Flag148-)
(Flag148prime-)
)

)
(:action Flag148Action-14
:parameters ()
:precondition
(and
(Flag72-)
(Flag72prime-)
)
:effect
(and
(Flag148-)
(Flag148prime-)
)

)
(:action Flag148Action-15
:parameters ()
:precondition
(and
(Flag137-)
(Flag137prime-)
)
:effect
(and
(Flag148-)
(Flag148prime-)
)

)
(:action Flag148Action-16
:parameters ()
:precondition
(and
(Flag42-)
(Flag42prime-)
)
:effect
(and
(Flag148-)
(Flag148prime-)
)

)
(:action Flag148Action-17
:parameters ()
:precondition
(and
(Flag97-)
(Flag97prime-)
)
:effect
(and
(Flag148-)
(Flag148prime-)
)

)
(:action Flag148Action-18
:parameters ()
:precondition
(and
(Flag48-)
(Flag48prime-)
)
:effect
(and
(Flag148-)
(Flag148prime-)
)

)
(:action Flag148Action-19
:parameters ()
:precondition
(and
(Flag122-)
(Flag122prime-)
)
:effect
(and
(Flag148-)
(Flag148prime-)
)

)
(:action Flag148Action-20
:parameters ()
:precondition
(and
(Flag138-)
(Flag138prime-)
)
:effect
(and
(Flag148-)
(Flag148prime-)
)

)
(:action Flag148Action-21
:parameters ()
:precondition
(and
(Flag50-)
(Flag50prime-)
)
:effect
(and
(Flag148-)
(Flag148prime-)
)

)
(:action Flag148Action-22
:parameters ()
:precondition
(and
(Flag52-)
(Flag52prime-)
)
:effect
(and
(Flag148-)
(Flag148prime-)
)

)
(:action Flag148Action-23
:parameters ()
:precondition
(and
(Flag139-)
(Flag139prime-)
)
:effect
(and
(Flag148-)
(Flag148prime-)
)

)
(:action Flag148Action-24
:parameters ()
:precondition
(and
(Flag82-)
(Flag82prime-)
)
:effect
(and
(Flag148-)
(Flag148prime-)
)

)
(:action Flag148Action-25
:parameters ()
:precondition
(and
(Flag102-)
(Flag102prime-)
)
:effect
(and
(Flag148-)
(Flag148prime-)
)

)
(:action Flag148Action-26
:parameters ()
:precondition
(and
(Flag57-)
(Flag57prime-)
)
:effect
(and
(Flag148-)
(Flag148prime-)
)

)
(:action Flag148Action-27
:parameters ()
:precondition
(and
(Flag87-)
(Flag87prime-)
)
:effect
(and
(Flag148-)
(Flag148prime-)
)

)
(:action Flag148Action-28
:parameters ()
:precondition
(and
(Flag123-)
(Flag123prime-)
)
:effect
(and
(Flag148-)
(Flag148prime-)
)

)
(:action Flag148Action-29
:parameters ()
:precondition
(and
(Flag140-)
(Flag140prime-)
)
:effect
(and
(Flag148-)
(Flag148prime-)
)

)
(:action Flag148Action-30
:parameters ()
:precondition
(and
(Flag61-)
(Flag61prime-)
)
:effect
(and
(Flag148-)
(Flag148prime-)
)

)
(:action Flag148Action-31
:parameters ()
:precondition
(and
(Flag62-)
(Flag62prime-)
)
:effect
(and
(Flag148-)
(Flag148prime-)
)

)
(:action Flag148Action-32
:parameters ()
:precondition
(and
(Flag128-)
(Flag128prime-)
)
:effect
(and
(Flag148-)
(Flag148prime-)
)

)
(:action Flag148Action-33
:parameters ()
:precondition
(and
(Flag90-)
(Flag90prime-)
)
:effect
(and
(Flag148-)
(Flag148prime-)
)

)
(:action Flag148Action-34
:parameters ()
:precondition
(and
(Flag101-)
(Flag101prime-)
)
:effect
(and
(Flag148-)
(Flag148prime-)
)

)
(:action Flag148Action-35
:parameters ()
:precondition
(and
(Flag141-)
(Flag141prime-)
)
:effect
(and
(Flag148-)
(Flag148prime-)
)

)
(:action Flag148Action-36
:parameters ()
:precondition
(and
(Flag142-)
(Flag142prime-)
)
:effect
(and
(Flag148-)
(Flag148prime-)
)

)
(:action Flag148Action-37
:parameters ()
:precondition
(and
(Flag110-)
(Flag110prime-)
)
:effect
(and
(Flag148-)
(Flag148prime-)
)

)
(:action Flag148Action-38
:parameters ()
:precondition
(and
(Flag111-)
(Flag111prime-)
)
:effect
(and
(Flag148-)
(Flag148prime-)
)

)
(:action Flag148Action-39
:parameters ()
:precondition
(and
(Flag73-)
(Flag73prime-)
)
:effect
(and
(Flag148-)
(Flag148prime-)
)

)
(:action Flag148Action-40
:parameters ()
:precondition
(and
(Flag143-)
(Flag143prime-)
)
:effect
(and
(Flag148-)
(Flag148prime-)
)

)
(:action Flag148Action-41
:parameters ()
:precondition
(and
(Flag93-)
(Flag93prime-)
)
:effect
(and
(Flag148-)
(Flag148prime-)
)

)
(:action Flag148Action-42
:parameters ()
:precondition
(and
(Flag115-)
(Flag115prime-)
)
:effect
(and
(Flag148-)
(Flag148prime-)
)

)
(:action Flag148Action-43
:parameters ()
:precondition
(and
(Flag116-)
(Flag116prime-)
)
:effect
(and
(Flag148-)
(Flag148prime-)
)

)
(:action Flag148Action-44
:parameters ()
:precondition
(and
(Flag44-)
(Flag44prime-)
)
:effect
(and
(Flag148-)
(Flag148prime-)
)

)
(:action Flag148Action-45
:parameters ()
:precondition
(and
(Flag79-)
(Flag79prime-)
)
:effect
(and
(Flag148-)
(Flag148prime-)
)

)
(:action Flag148Action-46
:parameters ()
:precondition
(and
(Flag118-)
(Flag118prime-)
)
:effect
(and
(Flag148-)
(Flag148prime-)
)

)
(:action Flag148Action-47
:parameters ()
:precondition
(and
(Flag100-)
(Flag100prime-)
)
:effect
(and
(Flag148-)
(Flag148prime-)
)

)
(:action Flag148Action-48
:parameters ()
:precondition
(and
(Flag108-)
(Flag108prime-)
)
:effect
(and
(Flag148-)
(Flag148prime-)
)

)
(:action Flag148Action-49
:parameters ()
:precondition
(and
(Flag130-)
(Flag130prime-)
)
:effect
(and
(Flag148-)
(Flag148prime-)
)

)
(:action Flag148Action-50
:parameters ()
:precondition
(and
(Flag120-)
(Flag120prime-)
)
:effect
(and
(Flag148-)
(Flag148prime-)
)

)
(:action Flag148Action-51
:parameters ()
:precondition
(and
(Flag54-)
(Flag54prime-)
)
:effect
(and
(Flag148-)
(Flag148prime-)
)

)
(:action Flag148Action-52
:parameters ()
:precondition
(and
(Flag83-)
(Flag83prime-)
)
:effect
(and
(Flag148-)
(Flag148prime-)
)

)
(:action Flag148Action-53
:parameters ()
:precondition
(and
(Flag60-)
(Flag60prime-)
)
:effect
(and
(Flag148-)
(Flag148prime-)
)

)
(:action Flag148Action-54
:parameters ()
:precondition
(and
(Flag58-)
(Flag58prime-)
)
:effect
(and
(Flag148-)
(Flag148prime-)
)

)
(:action Flag148Action-55
:parameters ()
:precondition
(and
(Flag86-)
(Flag86prime-)
)
:effect
(and
(Flag148-)
(Flag148prime-)
)

)
(:action Flag148Action-56
:parameters ()
:precondition
(and
(Flag66-)
(Flag66prime-)
)
:effect
(and
(Flag148-)
(Flag148prime-)
)

)
(:action Flag148Action-57
:parameters ()
:precondition
(and
(Flag106-)
(Flag106prime-)
)
:effect
(and
(Flag148-)
(Flag148prime-)
)

)
(:action Flag148Action-58
:parameters ()
:precondition
(and
(Flag67-)
(Flag67prime-)
)
:effect
(and
(Flag148-)
(Flag148prime-)
)

)
(:action Flag148Action-59
:parameters ()
:precondition
(and
(Flag144-)
(Flag144prime-)
)
:effect
(and
(Flag148-)
(Flag148prime-)
)

)
(:action Flag148Action-60
:parameters ()
:precondition
(and
(Flag109-)
(Flag109prime-)
)
:effect
(and
(Flag148-)
(Flag148prime-)
)

)
(:action Flag148Action-61
:parameters ()
:precondition
(and
(Flag131-)
(Flag131prime-)
)
:effect
(and
(Flag148-)
(Flag148prime-)
)

)
(:action Flag148Action-62
:parameters ()
:precondition
(and
(Flag75-)
(Flag75prime-)
)
:effect
(and
(Flag148-)
(Flag148prime-)
)

)
(:action Flag148Action-63
:parameters ()
:precondition
(and
(Flag121-)
(Flag121prime-)
)
:effect
(and
(Flag148-)
(Flag148prime-)
)

)
(:action Flag148Action-64
:parameters ()
:precondition
(and
(Flag145-)
(Flag145prime-)
)
:effect
(and
(Flag148-)
(Flag148prime-)
)

)
(:action Flag148Action-65
:parameters ()
:precondition
(and
(Flag132-)
(Flag132prime-)
)
:effect
(and
(Flag148-)
(Flag148prime-)
)

)
(:action Flag148Action-66
:parameters ()
:precondition
(and
(Flag40-)
(Flag40prime-)
)
:effect
(and
(Flag148-)
(Flag148prime-)
)

)
(:action Flag148Action-67
:parameters ()
:precondition
(and
(Flag96-)
(Flag96prime-)
)
:effect
(and
(Flag148-)
(Flag148prime-)
)

)
(:action Flag148Action-68
:parameters ()
:precondition
(and
(Flag146-)
(Flag146prime-)
)
:effect
(and
(Flag148-)
(Flag148prime-)
)

)
(:action Flag148Action-69
:parameters ()
:precondition
(and
(Flag78-)
(Flag78prime-)
)
:effect
(and
(Flag148-)
(Flag148prime-)
)

)
(:action Flag148Action-70
:parameters ()
:precondition
(and
(Flag46-)
(Flag46prime-)
)
:effect
(and
(Flag148-)
(Flag148prime-)
)

)
(:action Flag148Action-71
:parameters ()
:precondition
(and
(Flag98-)
(Flag98prime-)
)
:effect
(and
(Flag148-)
(Flag148prime-)
)

)
(:action Flag148Action-72
:parameters ()
:precondition
(and
(Flag119-)
(Flag119prime-)
)
:effect
(and
(Flag148-)
(Flag148prime-)
)

)
(:action Flag148Action-73
:parameters ()
:precondition
(and
(Flag85-)
(Flag85prime-)
)
:effect
(and
(Flag148-)
(Flag148prime-)
)

)
(:action Flag148Action-74
:parameters ()
:precondition
(and
(Flag53-)
(Flag53prime-)
)
:effect
(and
(Flag148-)
(Flag148prime-)
)

)
(:action Flag148Action-75
:parameters ()
:precondition
(and
(Flag147-)
(Flag147prime-)
)
:effect
(and
(Flag148-)
(Flag148prime-)
)

)
(:action Flag148Action-76
:parameters ()
:precondition
(and
(Flag55-)
(Flag55prime-)
)
:effect
(and
(Flag148-)
(Flag148prime-)
)

)
(:action Flag148Action-77
:parameters ()
:precondition
(and
(Flag81-)
(Flag81prime-)
)
:effect
(and
(Flag148-)
(Flag148prime-)
)

)
(:action Flag148Action-78
:parameters ()
:precondition
(and
(Flag104-)
(Flag104prime-)
)
:effect
(and
(Flag148-)
(Flag148prime-)
)

)
(:action Flag148Action-79
:parameters ()
:precondition
(and
(Flag129-)
(Flag129prime-)
)
:effect
(and
(Flag148-)
(Flag148prime-)
)

)
(:action Flag148Action-80
:parameters ()
:precondition
(and
(Flag65-)
(Flag65prime-)
)
:effect
(and
(Flag148-)
(Flag148prime-)
)

)
(:action Flag148Action-81
:parameters ()
:precondition
(and
(Flag89-)
(Flag89prime-)
)
:effect
(and
(Flag148-)
(Flag148prime-)
)

)
(:action Flag148Action-82
:parameters ()
:precondition
(and
(Flag105-)
(Flag105prime-)
)
:effect
(and
(Flag148-)
(Flag148prime-)
)

)
(:action Flag148Action-83
:parameters ()
:precondition
(and
(Flag74-)
(Flag74prime-)
)
:effect
(and
(Flag148-)
(Flag148prime-)
)

)
(:action Flag148Action-84
:parameters ()
:precondition
(and
(Flag91-)
(Flag91prime-)
)
:effect
(and
(Flag148-)
(Flag148prime-)
)

)
(:action Flag148Action-85
:parameters ()
:precondition
(and
(Flag107-)
(Flag107prime-)
)
:effect
(and
(Flag148-)
(Flag148prime-)
)

)
(:action Flag148Action-86
:parameters ()
:precondition
(and
(Flag68-)
(Flag68prime-)
)
:effect
(and
(Flag148-)
(Flag148prime-)
)

)
(:action Flag148Action-87
:parameters ()
:precondition
(and
(Flag113-)
(Flag113prime-)
)
:effect
(and
(Flag148-)
(Flag148prime-)
)

)
(:action Prim148Action
:parameters ()
:precondition
(and
(not (Flag41-))
(Flag41prime-)
(not (Flag47-))
(Flag47prime-)
(not (Flag134-))
(Flag134prime-)
(not (Flag135-))
(Flag135prime-)
(not (Flag124-))
(Flag124prime-)
(not (Flag125-))
(Flag125prime-)
(not (Flag126-))
(Flag126prime-)
(not (Flag84-))
(Flag84prime-)
(not (Flag127-))
(Flag127prime-)
(not (Flag63-))
(Flag63prime-)
(not (Flag136-))
(Flag136prime-)
(not (Flag43-))
(Flag43prime-)
(not (Flag92-))
(Flag92prime-)
(not (Flag69-))
(Flag69prime-)
(not (Flag72-))
(Flag72prime-)
(not (Flag137-))
(Flag137prime-)
(not (Flag42-))
(Flag42prime-)
(not (Flag97-))
(Flag97prime-)
(not (Flag48-))
(Flag48prime-)
(not (Flag122-))
(Flag122prime-)
(not (Flag138-))
(Flag138prime-)
(not (Flag50-))
(Flag50prime-)
(not (Flag52-))
(Flag52prime-)
(not (Flag139-))
(Flag139prime-)
(not (Flag82-))
(Flag82prime-)
(not (Flag102-))
(Flag102prime-)
(not (Flag57-))
(Flag57prime-)
(not (Flag87-))
(Flag87prime-)
(not (Flag123-))
(Flag123prime-)
(not (Flag140-))
(Flag140prime-)
(not (Flag61-))
(Flag61prime-)
(not (Flag62-))
(Flag62prime-)
(not (Flag128-))
(Flag128prime-)
(not (Flag90-))
(Flag90prime-)
(not (Flag101-))
(Flag101prime-)
(not (Flag141-))
(Flag141prime-)
(not (Flag142-))
(Flag142prime-)
(not (Flag110-))
(Flag110prime-)
(not (Flag111-))
(Flag111prime-)
(not (Flag73-))
(Flag73prime-)
(not (Flag143-))
(Flag143prime-)
(not (Flag93-))
(Flag93prime-)
(not (Flag115-))
(Flag115prime-)
(not (Flag116-))
(Flag116prime-)
(not (Flag44-))
(Flag44prime-)
(not (Flag79-))
(Flag79prime-)
(not (Flag118-))
(Flag118prime-)
(not (Flag100-))
(Flag100prime-)
(not (Flag108-))
(Flag108prime-)
(not (Flag130-))
(Flag130prime-)
(not (Flag120-))
(Flag120prime-)
(not (Flag54-))
(Flag54prime-)
(not (Flag83-))
(Flag83prime-)
(not (Flag60-))
(Flag60prime-)
(not (Flag58-))
(Flag58prime-)
(not (Flag86-))
(Flag86prime-)
(not (Flag66-))
(Flag66prime-)
(not (Flag106-))
(Flag106prime-)
(not (Flag67-))
(Flag67prime-)
(not (Flag144-))
(Flag144prime-)
(not (Flag109-))
(Flag109prime-)
(not (Flag131-))
(Flag131prime-)
(not (Flag75-))
(Flag75prime-)
(not (Flag121-))
(Flag121prime-)
(not (Flag145-))
(Flag145prime-)
(not (Flag132-))
(Flag132prime-)
(not (Flag40-))
(Flag40prime-)
(not (Flag96-))
(Flag96prime-)
(not (Flag146-))
(Flag146prime-)
(not (Flag78-))
(Flag78prime-)
(not (Flag46-))
(Flag46prime-)
(not (Flag98-))
(Flag98prime-)
(not (Flag119-))
(Flag119prime-)
(not (Flag85-))
(Flag85prime-)
(not (Flag53-))
(Flag53prime-)
(not (Flag147-))
(Flag147prime-)
(not (Flag55-))
(Flag55prime-)
(not (Flag81-))
(Flag81prime-)
(not (Flag104-))
(Flag104prime-)
(not (Flag129-))
(Flag129prime-)
(not (Flag65-))
(Flag65prime-)
(not (Flag89-))
(Flag89prime-)
(not (Flag105-))
(Flag105prime-)
(not (Flag74-))
(Flag74prime-)
(not (Flag91-))
(Flag91prime-)
(not (Flag107-))
(Flag107prime-)
(not (Flag68-))
(Flag68prime-)
(not (Flag113-))
(Flag113prime-)
)
:effect
(and
(Flag148prime-)
(not (Flag148-))
)

)
(:action Flag164Action-0
:parameters ()
:precondition
(and
(Flag41-)
(Flag41prime-)
)
:effect
(and
(Flag164-)
(Flag164prime-)
)

)
(:action Flag164Action-1
:parameters ()
:precondition
(and
(Flag47-)
(Flag47prime-)
)
:effect
(and
(Flag164-)
(Flag164prime-)
)

)
(:action Flag164Action-2
:parameters ()
:precondition
(and
(Flag149-)
(Flag149prime-)
)
:effect
(and
(Flag164-)
(Flag164prime-)
)

)
(:action Flag164Action-3
:parameters ()
:precondition
(and
(Flag134-)
(Flag134prime-)
)
:effect
(and
(Flag164-)
(Flag164prime-)
)

)
(:action Flag164Action-4
:parameters ()
:precondition
(and
(Flag150-)
(Flag150prime-)
)
:effect
(and
(Flag164-)
(Flag164prime-)
)

)
(:action Flag164Action-5
:parameters ()
:precondition
(and
(Flag44-)
(Flag44prime-)
)
:effect
(and
(Flag164-)
(Flag164prime-)
)

)
(:action Flag164Action-6
:parameters ()
:precondition
(and
(Flag135-)
(Flag135prime-)
)
:effect
(and
(Flag164-)
(Flag164prime-)
)

)
(:action Flag164Action-7
:parameters ()
:precondition
(and
(Flag124-)
(Flag124prime-)
)
:effect
(and
(Flag164-)
(Flag164prime-)
)

)
(:action Flag164Action-8
:parameters ()
:precondition
(and
(Flag125-)
(Flag125prime-)
)
:effect
(and
(Flag164-)
(Flag164prime-)
)

)
(:action Flag164Action-9
:parameters ()
:precondition
(and
(Flag126-)
(Flag126prime-)
)
:effect
(and
(Flag164-)
(Flag164prime-)
)

)
(:action Flag164Action-10
:parameters ()
:precondition
(and
(Flag84-)
(Flag84prime-)
)
:effect
(and
(Flag164-)
(Flag164prime-)
)

)
(:action Flag164Action-11
:parameters ()
:precondition
(and
(Flag127-)
(Flag127prime-)
)
:effect
(and
(Flag164-)
(Flag164prime-)
)

)
(:action Flag164Action-12
:parameters ()
:precondition
(and
(Flag63-)
(Flag63prime-)
)
:effect
(and
(Flag164-)
(Flag164prime-)
)

)
(:action Flag164Action-13
:parameters ()
:precondition
(and
(Flag43-)
(Flag43prime-)
)
:effect
(and
(Flag164-)
(Flag164prime-)
)

)
(:action Flag164Action-14
:parameters ()
:precondition
(and
(Flag151-)
(Flag151prime-)
)
:effect
(and
(Flag164-)
(Flag164prime-)
)

)
(:action Flag164Action-15
:parameters ()
:precondition
(and
(Flag152-)
(Flag152prime-)
)
:effect
(and
(Flag164-)
(Flag164prime-)
)

)
(:action Flag164Action-16
:parameters ()
:precondition
(and
(Flag92-)
(Flag92prime-)
)
:effect
(and
(Flag164-)
(Flag164prime-)
)

)
(:action Flag164Action-17
:parameters ()
:precondition
(and
(Flag69-)
(Flag69prime-)
)
:effect
(and
(Flag164-)
(Flag164prime-)
)

)
(:action Flag164Action-18
:parameters ()
:precondition
(and
(Flag72-)
(Flag72prime-)
)
:effect
(and
(Flag164-)
(Flag164prime-)
)

)
(:action Flag164Action-19
:parameters ()
:precondition
(and
(Flag137-)
(Flag137prime-)
)
:effect
(and
(Flag164-)
(Flag164prime-)
)

)
(:action Flag164Action-20
:parameters ()
:precondition
(and
(Flag42-)
(Flag42prime-)
)
:effect
(and
(Flag164-)
(Flag164prime-)
)

)
(:action Flag164Action-21
:parameters ()
:precondition
(and
(Flag97-)
(Flag97prime-)
)
:effect
(and
(Flag164-)
(Flag164prime-)
)

)
(:action Flag164Action-22
:parameters ()
:precondition
(and
(Flag48-)
(Flag48prime-)
)
:effect
(and
(Flag164-)
(Flag164prime-)
)

)
(:action Flag164Action-23
:parameters ()
:precondition
(and
(Flag122-)
(Flag122prime-)
)
:effect
(and
(Flag164-)
(Flag164prime-)
)

)
(:action Flag164Action-24
:parameters ()
:precondition
(and
(Flag153-)
(Flag153prime-)
)
:effect
(and
(Flag164-)
(Flag164prime-)
)

)
(:action Flag164Action-25
:parameters ()
:precondition
(and
(Flag138-)
(Flag138prime-)
)
:effect
(and
(Flag164-)
(Flag164prime-)
)

)
(:action Flag164Action-26
:parameters ()
:precondition
(and
(Flag52-)
(Flag52prime-)
)
:effect
(and
(Flag164-)
(Flag164prime-)
)

)
(:action Flag164Action-27
:parameters ()
:precondition
(and
(Flag139-)
(Flag139prime-)
)
:effect
(and
(Flag164-)
(Flag164prime-)
)

)
(:action Flag164Action-28
:parameters ()
:precondition
(and
(Flag82-)
(Flag82prime-)
)
:effect
(and
(Flag164-)
(Flag164prime-)
)

)
(:action Flag164Action-29
:parameters ()
:precondition
(and
(Flag102-)
(Flag102prime-)
)
:effect
(and
(Flag164-)
(Flag164prime-)
)

)
(:action Flag164Action-30
:parameters ()
:precondition
(and
(Flag57-)
(Flag57prime-)
)
:effect
(and
(Flag164-)
(Flag164prime-)
)

)
(:action Flag164Action-31
:parameters ()
:precondition
(and
(Flag87-)
(Flag87prime-)
)
:effect
(and
(Flag164-)
(Flag164prime-)
)

)
(:action Flag164Action-32
:parameters ()
:precondition
(and
(Flag123-)
(Flag123prime-)
)
:effect
(and
(Flag164-)
(Flag164prime-)
)

)
(:action Flag164Action-33
:parameters ()
:precondition
(and
(Flag140-)
(Flag140prime-)
)
:effect
(and
(Flag164-)
(Flag164prime-)
)

)
(:action Flag164Action-34
:parameters ()
:precondition
(and
(Flag61-)
(Flag61prime-)
)
:effect
(and
(Flag164-)
(Flag164prime-)
)

)
(:action Flag164Action-35
:parameters ()
:precondition
(and
(Flag62-)
(Flag62prime-)
)
:effect
(and
(Flag164-)
(Flag164prime-)
)

)
(:action Flag164Action-36
:parameters ()
:precondition
(and
(Flag154-)
(Flag154prime-)
)
:effect
(and
(Flag164-)
(Flag164prime-)
)

)
(:action Flag164Action-37
:parameters ()
:precondition
(and
(Flag128-)
(Flag128prime-)
)
:effect
(and
(Flag164-)
(Flag164prime-)
)

)
(:action Flag164Action-38
:parameters ()
:precondition
(and
(Flag90-)
(Flag90prime-)
)
:effect
(and
(Flag164-)
(Flag164prime-)
)

)
(:action Flag164Action-39
:parameters ()
:precondition
(and
(Flag101-)
(Flag101prime-)
)
:effect
(and
(Flag164-)
(Flag164prime-)
)

)
(:action Flag164Action-40
:parameters ()
:precondition
(and
(Flag155-)
(Flag155prime-)
)
:effect
(and
(Flag164-)
(Flag164prime-)
)

)
(:action Flag164Action-41
:parameters ()
:precondition
(and
(Flag156-)
(Flag156prime-)
)
:effect
(and
(Flag164-)
(Flag164prime-)
)

)
(:action Flag164Action-42
:parameters ()
:precondition
(and
(Flag141-)
(Flag141prime-)
)
:effect
(and
(Flag164-)
(Flag164prime-)
)

)
(:action Flag164Action-43
:parameters ()
:precondition
(and
(Flag111-)
(Flag111prime-)
)
:effect
(and
(Flag164-)
(Flag164prime-)
)

)
(:action Flag164Action-44
:parameters ()
:precondition
(and
(Flag73-)
(Flag73prime-)
)
:effect
(and
(Flag164-)
(Flag164prime-)
)

)
(:action Flag164Action-45
:parameters ()
:precondition
(and
(Flag143-)
(Flag143prime-)
)
:effect
(and
(Flag164-)
(Flag164prime-)
)

)
(:action Flag164Action-46
:parameters ()
:precondition
(and
(Flag93-)
(Flag93prime-)
)
:effect
(and
(Flag164-)
(Flag164prime-)
)

)
(:action Flag164Action-47
:parameters ()
:precondition
(and
(Flag157-)
(Flag157prime-)
)
:effect
(and
(Flag164-)
(Flag164prime-)
)

)
(:action Flag164Action-48
:parameters ()
:precondition
(and
(Flag115-)
(Flag115prime-)
)
:effect
(and
(Flag164-)
(Flag164prime-)
)

)
(:action Flag164Action-49
:parameters ()
:precondition
(and
(Flag116-)
(Flag116prime-)
)
:effect
(and
(Flag164-)
(Flag164prime-)
)

)
(:action Flag164Action-50
:parameters ()
:precondition
(and
(Flag58-)
(Flag58prime-)
)
:effect
(and
(Flag164-)
(Flag164prime-)
)

)
(:action Flag164Action-51
:parameters ()
:precondition
(and
(Flag158-)
(Flag158prime-)
)
:effect
(and
(Flag164-)
(Flag164prime-)
)

)
(:action Flag164Action-52
:parameters ()
:precondition
(and
(Flag79-)
(Flag79prime-)
)
:effect
(and
(Flag164-)
(Flag164prime-)
)

)
(:action Flag164Action-53
:parameters ()
:precondition
(and
(Flag159-)
(Flag159prime-)
)
:effect
(and
(Flag164-)
(Flag164prime-)
)

)
(:action Flag164Action-54
:parameters ()
:precondition
(and
(Flag118-)
(Flag118prime-)
)
:effect
(and
(Flag164-)
(Flag164prime-)
)

)
(:action Flag164Action-55
:parameters ()
:precondition
(and
(Flag100-)
(Flag100prime-)
)
:effect
(and
(Flag164-)
(Flag164prime-)
)

)
(:action Flag164Action-56
:parameters ()
:precondition
(and
(Flag108-)
(Flag108prime-)
)
:effect
(and
(Flag164-)
(Flag164prime-)
)

)
(:action Flag164Action-57
:parameters ()
:precondition
(and
(Flag130-)
(Flag130prime-)
)
:effect
(and
(Flag164-)
(Flag164prime-)
)

)
(:action Flag164Action-58
:parameters ()
:precondition
(and
(Flag160-)
(Flag160prime-)
)
:effect
(and
(Flag164-)
(Flag164prime-)
)

)
(:action Flag164Action-59
:parameters ()
:precondition
(and
(Flag120-)
(Flag120prime-)
)
:effect
(and
(Flag164-)
(Flag164prime-)
)

)
(:action Flag164Action-60
:parameters ()
:precondition
(and
(Flag83-)
(Flag83prime-)
)
:effect
(and
(Flag164-)
(Flag164prime-)
)

)
(:action Flag164Action-61
:parameters ()
:precondition
(and
(Flag60-)
(Flag60prime-)
)
:effect
(and
(Flag164-)
(Flag164prime-)
)

)
(:action Flag164Action-62
:parameters ()
:precondition
(and
(Flag66-)
(Flag66prime-)
)
:effect
(and
(Flag164-)
(Flag164prime-)
)

)
(:action Flag164Action-63
:parameters ()
:precondition
(and
(Flag106-)
(Flag106prime-)
)
:effect
(and
(Flag164-)
(Flag164prime-)
)

)
(:action Flag164Action-64
:parameters ()
:precondition
(and
(Flag67-)
(Flag67prime-)
)
:effect
(and
(Flag164-)
(Flag164prime-)
)

)
(:action Flag164Action-65
:parameters ()
:precondition
(and
(Flag144-)
(Flag144prime-)
)
:effect
(and
(Flag164-)
(Flag164prime-)
)

)
(:action Flag164Action-66
:parameters ()
:precondition
(and
(Flag109-)
(Flag109prime-)
)
:effect
(and
(Flag164-)
(Flag164prime-)
)

)
(:action Flag164Action-67
:parameters ()
:precondition
(and
(Flag75-)
(Flag75prime-)
)
:effect
(and
(Flag164-)
(Flag164prime-)
)

)
(:action Flag164Action-68
:parameters ()
:precondition
(and
(Flag121-)
(Flag121prime-)
)
:effect
(and
(Flag164-)
(Flag164prime-)
)

)
(:action Flag164Action-69
:parameters ()
:precondition
(and
(Flag145-)
(Flag145prime-)
)
:effect
(and
(Flag164-)
(Flag164prime-)
)

)
(:action Flag164Action-70
:parameters ()
:precondition
(and
(Flag132-)
(Flag132prime-)
)
:effect
(and
(Flag164-)
(Flag164prime-)
)

)
(:action Flag164Action-71
:parameters ()
:precondition
(and
(Flag68-)
(Flag68prime-)
)
:effect
(and
(Flag164-)
(Flag164prime-)
)

)
(:action Flag164Action-72
:parameters ()
:precondition
(and
(Flag96-)
(Flag96prime-)
)
:effect
(and
(Flag164-)
(Flag164prime-)
)

)
(:action Flag164Action-73
:parameters ()
:precondition
(and
(Flag146-)
(Flag146prime-)
)
:effect
(and
(Flag164-)
(Flag164prime-)
)

)
(:action Flag164Action-74
:parameters ()
:precondition
(and
(Flag78-)
(Flag78prime-)
)
:effect
(and
(Flag164-)
(Flag164prime-)
)

)
(:action Flag164Action-75
:parameters ()
:precondition
(and
(Flag46-)
(Flag46prime-)
)
:effect
(and
(Flag164-)
(Flag164prime-)
)

)
(:action Flag164Action-76
:parameters ()
:precondition
(and
(Flag98-)
(Flag98prime-)
)
:effect
(and
(Flag164-)
(Flag164prime-)
)

)
(:action Flag164Action-77
:parameters ()
:precondition
(and
(Flag119-)
(Flag119prime-)
)
:effect
(and
(Flag164-)
(Flag164prime-)
)

)
(:action Flag164Action-78
:parameters ()
:precondition
(and
(Flag85-)
(Flag85prime-)
)
:effect
(and
(Flag164-)
(Flag164prime-)
)

)
(:action Flag164Action-79
:parameters ()
:precondition
(and
(Flag53-)
(Flag53prime-)
)
:effect
(and
(Flag164-)
(Flag164prime-)
)

)
(:action Flag164Action-80
:parameters ()
:precondition
(and
(Flag147-)
(Flag147prime-)
)
:effect
(and
(Flag164-)
(Flag164prime-)
)

)
(:action Flag164Action-81
:parameters ()
:precondition
(and
(Flag55-)
(Flag55prime-)
)
:effect
(and
(Flag164-)
(Flag164prime-)
)

)
(:action Flag164Action-82
:parameters ()
:precondition
(and
(Flag81-)
(Flag81prime-)
)
:effect
(and
(Flag164-)
(Flag164prime-)
)

)
(:action Flag164Action-83
:parameters ()
:precondition
(and
(Flag104-)
(Flag104prime-)
)
:effect
(and
(Flag164-)
(Flag164prime-)
)

)
(:action Flag164Action-84
:parameters ()
:precondition
(and
(Flag161-)
(Flag161prime-)
)
:effect
(and
(Flag164-)
(Flag164prime-)
)

)
(:action Flag164Action-85
:parameters ()
:precondition
(and
(Flag129-)
(Flag129prime-)
)
:effect
(and
(Flag164-)
(Flag164prime-)
)

)
(:action Flag164Action-86
:parameters ()
:precondition
(and
(Flag162-)
(Flag162prime-)
)
:effect
(and
(Flag164-)
(Flag164prime-)
)

)
(:action Flag164Action-87
:parameters ()
:precondition
(and
(Flag65-)
(Flag65prime-)
)
:effect
(and
(Flag164-)
(Flag164prime-)
)

)
(:action Flag164Action-88
:parameters ()
:precondition
(and
(Flag89-)
(Flag89prime-)
)
:effect
(and
(Flag164-)
(Flag164prime-)
)

)
(:action Flag164Action-89
:parameters ()
:precondition
(and
(Flag105-)
(Flag105prime-)
)
:effect
(and
(Flag164-)
(Flag164prime-)
)

)
(:action Flag164Action-90
:parameters ()
:precondition
(and
(Flag74-)
(Flag74prime-)
)
:effect
(and
(Flag164-)
(Flag164prime-)
)

)
(:action Flag164Action-91
:parameters ()
:precondition
(and
(Flag91-)
(Flag91prime-)
)
:effect
(and
(Flag164-)
(Flag164prime-)
)

)
(:action Flag164Action-92
:parameters ()
:precondition
(and
(Flag107-)
(Flag107prime-)
)
:effect
(and
(Flag164-)
(Flag164prime-)
)

)
(:action Flag164Action-93
:parameters ()
:precondition
(and
(Flag40-)
(Flag40prime-)
)
:effect
(and
(Flag164-)
(Flag164prime-)
)

)
(:action Flag164Action-94
:parameters ()
:precondition
(and
(Flag163-)
(Flag163prime-)
)
:effect
(and
(Flag164-)
(Flag164prime-)
)

)
(:action Flag164Action-95
:parameters ()
:precondition
(and
(Flag113-)
(Flag113prime-)
)
:effect
(and
(Flag164-)
(Flag164prime-)
)

)
(:action Prim164Action
:parameters ()
:precondition
(and
(not (Flag41-))
(Flag41prime-)
(not (Flag47-))
(Flag47prime-)
(not (Flag149-))
(Flag149prime-)
(not (Flag134-))
(Flag134prime-)
(not (Flag150-))
(Flag150prime-)
(not (Flag44-))
(Flag44prime-)
(not (Flag135-))
(Flag135prime-)
(not (Flag124-))
(Flag124prime-)
(not (Flag125-))
(Flag125prime-)
(not (Flag126-))
(Flag126prime-)
(not (Flag84-))
(Flag84prime-)
(not (Flag127-))
(Flag127prime-)
(not (Flag63-))
(Flag63prime-)
(not (Flag43-))
(Flag43prime-)
(not (Flag151-))
(Flag151prime-)
(not (Flag152-))
(Flag152prime-)
(not (Flag92-))
(Flag92prime-)
(not (Flag69-))
(Flag69prime-)
(not (Flag72-))
(Flag72prime-)
(not (Flag137-))
(Flag137prime-)
(not (Flag42-))
(Flag42prime-)
(not (Flag97-))
(Flag97prime-)
(not (Flag48-))
(Flag48prime-)
(not (Flag122-))
(Flag122prime-)
(not (Flag153-))
(Flag153prime-)
(not (Flag138-))
(Flag138prime-)
(not (Flag52-))
(Flag52prime-)
(not (Flag139-))
(Flag139prime-)
(not (Flag82-))
(Flag82prime-)
(not (Flag102-))
(Flag102prime-)
(not (Flag57-))
(Flag57prime-)
(not (Flag87-))
(Flag87prime-)
(not (Flag123-))
(Flag123prime-)
(not (Flag140-))
(Flag140prime-)
(not (Flag61-))
(Flag61prime-)
(not (Flag62-))
(Flag62prime-)
(not (Flag154-))
(Flag154prime-)
(not (Flag128-))
(Flag128prime-)
(not (Flag90-))
(Flag90prime-)
(not (Flag101-))
(Flag101prime-)
(not (Flag155-))
(Flag155prime-)
(not (Flag156-))
(Flag156prime-)
(not (Flag141-))
(Flag141prime-)
(not (Flag111-))
(Flag111prime-)
(not (Flag73-))
(Flag73prime-)
(not (Flag143-))
(Flag143prime-)
(not (Flag93-))
(Flag93prime-)
(not (Flag157-))
(Flag157prime-)
(not (Flag115-))
(Flag115prime-)
(not (Flag116-))
(Flag116prime-)
(not (Flag58-))
(Flag58prime-)
(not (Flag158-))
(Flag158prime-)
(not (Flag79-))
(Flag79prime-)
(not (Flag159-))
(Flag159prime-)
(not (Flag118-))
(Flag118prime-)
(not (Flag100-))
(Flag100prime-)
(not (Flag108-))
(Flag108prime-)
(not (Flag130-))
(Flag130prime-)
(not (Flag160-))
(Flag160prime-)
(not (Flag120-))
(Flag120prime-)
(not (Flag83-))
(Flag83prime-)
(not (Flag60-))
(Flag60prime-)
(not (Flag66-))
(Flag66prime-)
(not (Flag106-))
(Flag106prime-)
(not (Flag67-))
(Flag67prime-)
(not (Flag144-))
(Flag144prime-)
(not (Flag109-))
(Flag109prime-)
(not (Flag75-))
(Flag75prime-)
(not (Flag121-))
(Flag121prime-)
(not (Flag145-))
(Flag145prime-)
(not (Flag132-))
(Flag132prime-)
(not (Flag68-))
(Flag68prime-)
(not (Flag96-))
(Flag96prime-)
(not (Flag146-))
(Flag146prime-)
(not (Flag78-))
(Flag78prime-)
(not (Flag46-))
(Flag46prime-)
(not (Flag98-))
(Flag98prime-)
(not (Flag119-))
(Flag119prime-)
(not (Flag85-))
(Flag85prime-)
(not (Flag53-))
(Flag53prime-)
(not (Flag147-))
(Flag147prime-)
(not (Flag55-))
(Flag55prime-)
(not (Flag81-))
(Flag81prime-)
(not (Flag104-))
(Flag104prime-)
(not (Flag161-))
(Flag161prime-)
(not (Flag129-))
(Flag129prime-)
(not (Flag162-))
(Flag162prime-)
(not (Flag65-))
(Flag65prime-)
(not (Flag89-))
(Flag89prime-)
(not (Flag105-))
(Flag105prime-)
(not (Flag74-))
(Flag74prime-)
(not (Flag91-))
(Flag91prime-)
(not (Flag107-))
(Flag107prime-)
(not (Flag40-))
(Flag40prime-)
(not (Flag163-))
(Flag163prime-)
(not (Flag113-))
(Flag113prime-)
)
:effect
(and
(Flag164prime-)
(not (Flag164-))
)

)
(:action Flag178Action-0
:parameters ()
:precondition
(and
(Flag41-)
(Flag41prime-)
)
:effect
(and
(Flag178-)
(Flag178prime-)
)

)
(:action Flag178Action-1
:parameters ()
:precondition
(and
(Flag47-)
(Flag47prime-)
)
:effect
(and
(Flag178-)
(Flag178prime-)
)

)
(:action Flag178Action-2
:parameters ()
:precondition
(and
(Flag134-)
(Flag134prime-)
)
:effect
(and
(Flag178-)
(Flag178prime-)
)

)
(:action Flag178Action-3
:parameters ()
:precondition
(and
(Flag150-)
(Flag150prime-)
)
:effect
(and
(Flag178-)
(Flag178prime-)
)

)
(:action Flag178Action-4
:parameters ()
:precondition
(and
(Flag44-)
(Flag44prime-)
)
:effect
(and
(Flag178-)
(Flag178prime-)
)

)
(:action Flag178Action-5
:parameters ()
:precondition
(and
(Flag135-)
(Flag135prime-)
)
:effect
(and
(Flag178-)
(Flag178prime-)
)

)
(:action Flag178Action-6
:parameters ()
:precondition
(and
(Flag124-)
(Flag124prime-)
)
:effect
(and
(Flag178-)
(Flag178prime-)
)

)
(:action Flag178Action-7
:parameters ()
:precondition
(and
(Flag165-)
(Flag165prime-)
)
:effect
(and
(Flag178-)
(Flag178prime-)
)

)
(:action Flag178Action-8
:parameters ()
:precondition
(and
(Flag126-)
(Flag126prime-)
)
:effect
(and
(Flag178-)
(Flag178prime-)
)

)
(:action Flag178Action-9
:parameters ()
:precondition
(and
(Flag84-)
(Flag84prime-)
)
:effect
(and
(Flag178-)
(Flag178prime-)
)

)
(:action Flag178Action-10
:parameters ()
:precondition
(and
(Flag85-)
(Flag85prime-)
)
:effect
(and
(Flag178-)
(Flag178prime-)
)

)
(:action Flag178Action-11
:parameters ()
:precondition
(and
(Flag63-)
(Flag63prime-)
)
:effect
(and
(Flag178-)
(Flag178prime-)
)

)
(:action Flag178Action-12
:parameters ()
:precondition
(and
(Flag166-)
(Flag166prime-)
)
:effect
(and
(Flag178-)
(Flag178prime-)
)

)
(:action Flag178Action-13
:parameters ()
:precondition
(and
(Flag146-)
(Flag146prime-)
)
:effect
(and
(Flag178-)
(Flag178prime-)
)

)
(:action Flag178Action-14
:parameters ()
:precondition
(and
(Flag151-)
(Flag151prime-)
)
:effect
(and
(Flag178-)
(Flag178prime-)
)

)
(:action Flag178Action-15
:parameters ()
:precondition
(and
(Flag92-)
(Flag92prime-)
)
:effect
(and
(Flag178-)
(Flag178prime-)
)

)
(:action Flag178Action-16
:parameters ()
:precondition
(and
(Flag69-)
(Flag69prime-)
)
:effect
(and
(Flag178-)
(Flag178prime-)
)

)
(:action Flag178Action-17
:parameters ()
:precondition
(and
(Flag137-)
(Flag137prime-)
)
:effect
(and
(Flag178-)
(Flag178prime-)
)

)
(:action Flag178Action-18
:parameters ()
:precondition
(and
(Flag42-)
(Flag42prime-)
)
:effect
(and
(Flag178-)
(Flag178prime-)
)

)
(:action Flag178Action-19
:parameters ()
:precondition
(and
(Flag97-)
(Flag97prime-)
)
:effect
(and
(Flag178-)
(Flag178prime-)
)

)
(:action Flag178Action-20
:parameters ()
:precondition
(and
(Flag48-)
(Flag48prime-)
)
:effect
(and
(Flag178-)
(Flag178prime-)
)

)
(:action Flag178Action-21
:parameters ()
:precondition
(and
(Flag167-)
(Flag167prime-)
)
:effect
(and
(Flag178-)
(Flag178prime-)
)

)
(:action Flag178Action-22
:parameters ()
:precondition
(and
(Flag122-)
(Flag122prime-)
)
:effect
(and
(Flag178-)
(Flag178prime-)
)

)
(:action Flag178Action-23
:parameters ()
:precondition
(and
(Flag153-)
(Flag153prime-)
)
:effect
(and
(Flag178-)
(Flag178prime-)
)

)
(:action Flag178Action-24
:parameters ()
:precondition
(and
(Flag138-)
(Flag138prime-)
)
:effect
(and
(Flag178-)
(Flag178prime-)
)

)
(:action Flag178Action-25
:parameters ()
:precondition
(and
(Flag62-)
(Flag62prime-)
)
:effect
(and
(Flag178-)
(Flag178prime-)
)

)
(:action Flag178Action-26
:parameters ()
:precondition
(and
(Flag139-)
(Flag139prime-)
)
:effect
(and
(Flag178-)
(Flag178prime-)
)

)
(:action Flag178Action-27
:parameters ()
:precondition
(and
(Flag82-)
(Flag82prime-)
)
:effect
(and
(Flag178-)
(Flag178prime-)
)

)
(:action Flag178Action-28
:parameters ()
:precondition
(and
(Flag102-)
(Flag102prime-)
)
:effect
(and
(Flag178-)
(Flag178prime-)
)

)
(:action Flag178Action-29
:parameters ()
:precondition
(and
(Flag57-)
(Flag57prime-)
)
:effect
(and
(Flag178-)
(Flag178prime-)
)

)
(:action Flag178Action-30
:parameters ()
:precondition
(and
(Flag87-)
(Flag87prime-)
)
:effect
(and
(Flag178-)
(Flag178prime-)
)

)
(:action Flag178Action-31
:parameters ()
:precondition
(and
(Flag123-)
(Flag123prime-)
)
:effect
(and
(Flag178-)
(Flag178prime-)
)

)
(:action Flag178Action-32
:parameters ()
:precondition
(and
(Flag140-)
(Flag140prime-)
)
:effect
(and
(Flag178-)
(Flag178prime-)
)

)
(:action Flag178Action-33
:parameters ()
:precondition
(and
(Flag168-)
(Flag168prime-)
)
:effect
(and
(Flag178-)
(Flag178prime-)
)

)
(:action Flag178Action-34
:parameters ()
:precondition
(and
(Flag61-)
(Flag61prime-)
)
:effect
(and
(Flag178-)
(Flag178prime-)
)

)
(:action Flag178Action-35
:parameters ()
:precondition
(and
(Flag52-)
(Flag52prime-)
)
:effect
(and
(Flag178-)
(Flag178prime-)
)

)
(:action Flag178Action-36
:parameters ()
:precondition
(and
(Flag154-)
(Flag154prime-)
)
:effect
(and
(Flag178-)
(Flag178prime-)
)

)
(:action Flag178Action-37
:parameters ()
:precondition
(and
(Flag128-)
(Flag128prime-)
)
:effect
(and
(Flag178-)
(Flag178prime-)
)

)
(:action Flag178Action-38
:parameters ()
:precondition
(and
(Flag169-)
(Flag169prime-)
)
:effect
(and
(Flag178-)
(Flag178prime-)
)

)
(:action Flag178Action-39
:parameters ()
:precondition
(and
(Flag90-)
(Flag90prime-)
)
:effect
(and
(Flag178-)
(Flag178prime-)
)

)
(:action Flag178Action-40
:parameters ()
:precondition
(and
(Flag101-)
(Flag101prime-)
)
:effect
(and
(Flag178-)
(Flag178prime-)
)

)
(:action Flag178Action-41
:parameters ()
:precondition
(and
(Flag156-)
(Flag156prime-)
)
:effect
(and
(Flag178-)
(Flag178prime-)
)

)
(:action Flag178Action-42
:parameters ()
:precondition
(and
(Flag141-)
(Flag141prime-)
)
:effect
(and
(Flag178-)
(Flag178prime-)
)

)
(:action Flag178Action-43
:parameters ()
:precondition
(and
(Flag111-)
(Flag111prime-)
)
:effect
(and
(Flag178-)
(Flag178prime-)
)

)
(:action Flag178Action-44
:parameters ()
:precondition
(and
(Flag73-)
(Flag73prime-)
)
:effect
(and
(Flag178-)
(Flag178prime-)
)

)
(:action Flag178Action-45
:parameters ()
:precondition
(and
(Flag143-)
(Flag143prime-)
)
:effect
(and
(Flag178-)
(Flag178prime-)
)

)
(:action Flag178Action-46
:parameters ()
:precondition
(and
(Flag93-)
(Flag93prime-)
)
:effect
(and
(Flag178-)
(Flag178prime-)
)

)
(:action Flag178Action-47
:parameters ()
:precondition
(and
(Flag157-)
(Flag157prime-)
)
:effect
(and
(Flag178-)
(Flag178prime-)
)

)
(:action Flag178Action-48
:parameters ()
:precondition
(and
(Flag115-)
(Flag115prime-)
)
:effect
(and
(Flag178-)
(Flag178prime-)
)

)
(:action Flag178Action-49
:parameters ()
:precondition
(and
(Flag116-)
(Flag116prime-)
)
:effect
(and
(Flag178-)
(Flag178prime-)
)

)
(:action Flag178Action-50
:parameters ()
:precondition
(and
(Flag58-)
(Flag58prime-)
)
:effect
(and
(Flag178-)
(Flag178prime-)
)

)
(:action Flag178Action-51
:parameters ()
:precondition
(and
(Flag158-)
(Flag158prime-)
)
:effect
(and
(Flag178-)
(Flag178prime-)
)

)
(:action Flag178Action-52
:parameters ()
:precondition
(and
(Flag79-)
(Flag79prime-)
)
:effect
(and
(Flag178-)
(Flag178prime-)
)

)
(:action Flag178Action-53
:parameters ()
:precondition
(and
(Flag159-)
(Flag159prime-)
)
:effect
(and
(Flag178-)
(Flag178prime-)
)

)
(:action Flag178Action-54
:parameters ()
:precondition
(and
(Flag118-)
(Flag118prime-)
)
:effect
(and
(Flag178-)
(Flag178prime-)
)

)
(:action Flag178Action-55
:parameters ()
:precondition
(and
(Flag170-)
(Flag170prime-)
)
:effect
(and
(Flag178-)
(Flag178prime-)
)

)
(:action Flag178Action-56
:parameters ()
:precondition
(and
(Flag171-)
(Flag171prime-)
)
:effect
(and
(Flag178-)
(Flag178prime-)
)

)
(:action Flag178Action-57
:parameters ()
:precondition
(and
(Flag172-)
(Flag172prime-)
)
:effect
(and
(Flag178-)
(Flag178prime-)
)

)
(:action Flag178Action-58
:parameters ()
:precondition
(and
(Flag100-)
(Flag100prime-)
)
:effect
(and
(Flag178-)
(Flag178prime-)
)

)
(:action Flag178Action-59
:parameters ()
:precondition
(and
(Flag108-)
(Flag108prime-)
)
:effect
(and
(Flag178-)
(Flag178prime-)
)

)
(:action Flag178Action-60
:parameters ()
:precondition
(and
(Flag173-)
(Flag173prime-)
)
:effect
(and
(Flag178-)
(Flag178prime-)
)

)
(:action Flag178Action-61
:parameters ()
:precondition
(and
(Flag130-)
(Flag130prime-)
)
:effect
(and
(Flag178-)
(Flag178prime-)
)

)
(:action Flag178Action-62
:parameters ()
:precondition
(and
(Flag160-)
(Flag160prime-)
)
:effect
(and
(Flag178-)
(Flag178prime-)
)

)
(:action Flag178Action-63
:parameters ()
:precondition
(and
(Flag83-)
(Flag83prime-)
)
:effect
(and
(Flag178-)
(Flag178prime-)
)

)
(:action Flag178Action-64
:parameters ()
:precondition
(and
(Flag174-)
(Flag174prime-)
)
:effect
(and
(Flag178-)
(Flag178prime-)
)

)
(:action Flag178Action-65
:parameters ()
:precondition
(and
(Flag66-)
(Flag66prime-)
)
:effect
(and
(Flag178-)
(Flag178prime-)
)

)
(:action Flag178Action-66
:parameters ()
:precondition
(and
(Flag106-)
(Flag106prime-)
)
:effect
(and
(Flag178-)
(Flag178prime-)
)

)
(:action Flag178Action-67
:parameters ()
:precondition
(and
(Flag67-)
(Flag67prime-)
)
:effect
(and
(Flag178-)
(Flag178prime-)
)

)
(:action Flag178Action-68
:parameters ()
:precondition
(and
(Flag144-)
(Flag144prime-)
)
:effect
(and
(Flag178-)
(Flag178prime-)
)

)
(:action Flag178Action-69
:parameters ()
:precondition
(and
(Flag175-)
(Flag175prime-)
)
:effect
(and
(Flag178-)
(Flag178prime-)
)

)
(:action Flag178Action-70
:parameters ()
:precondition
(and
(Flag121-)
(Flag121prime-)
)
:effect
(and
(Flag178-)
(Flag178prime-)
)

)
(:action Flag178Action-71
:parameters ()
:precondition
(and
(Flag145-)
(Flag145prime-)
)
:effect
(and
(Flag178-)
(Flag178prime-)
)

)
(:action Flag178Action-72
:parameters ()
:precondition
(and
(Flag176-)
(Flag176prime-)
)
:effect
(and
(Flag178-)
(Flag178prime-)
)

)
(:action Flag178Action-73
:parameters ()
:precondition
(and
(Flag132-)
(Flag132prime-)
)
:effect
(and
(Flag178-)
(Flag178prime-)
)

)
(:action Flag178Action-74
:parameters ()
:precondition
(and
(Flag40-)
(Flag40prime-)
)
:effect
(and
(Flag178-)
(Flag178prime-)
)

)
(:action Flag178Action-75
:parameters ()
:precondition
(and
(Flag96-)
(Flag96prime-)
)
:effect
(and
(Flag178-)
(Flag178prime-)
)

)
(:action Flag178Action-76
:parameters ()
:precondition
(and
(Flag43-)
(Flag43prime-)
)
:effect
(and
(Flag178-)
(Flag178prime-)
)

)
(:action Flag178Action-77
:parameters ()
:precondition
(and
(Flag120-)
(Flag120prime-)
)
:effect
(and
(Flag178-)
(Flag178prime-)
)

)
(:action Flag178Action-78
:parameters ()
:precondition
(and
(Flag46-)
(Flag46prime-)
)
:effect
(and
(Flag178-)
(Flag178prime-)
)

)
(:action Flag178Action-79
:parameters ()
:precondition
(and
(Flag98-)
(Flag98prime-)
)
:effect
(and
(Flag178-)
(Flag178prime-)
)

)
(:action Flag178Action-80
:parameters ()
:precondition
(and
(Flag60-)
(Flag60prime-)
)
:effect
(and
(Flag178-)
(Flag178prime-)
)

)
(:action Flag178Action-81
:parameters ()
:precondition
(and
(Flag119-)
(Flag119prime-)
)
:effect
(and
(Flag178-)
(Flag178prime-)
)

)
(:action Flag178Action-82
:parameters ()
:precondition
(and
(Flag127-)
(Flag127prime-)
)
:effect
(and
(Flag178-)
(Flag178prime-)
)

)
(:action Flag178Action-83
:parameters ()
:precondition
(and
(Flag53-)
(Flag53prime-)
)
:effect
(and
(Flag178-)
(Flag178prime-)
)

)
(:action Flag178Action-84
:parameters ()
:precondition
(and
(Flag147-)
(Flag147prime-)
)
:effect
(and
(Flag178-)
(Flag178prime-)
)

)
(:action Flag178Action-85
:parameters ()
:precondition
(and
(Flag55-)
(Flag55prime-)
)
:effect
(and
(Flag178-)
(Flag178prime-)
)

)
(:action Flag178Action-86
:parameters ()
:precondition
(and
(Flag177-)
(Flag177prime-)
)
:effect
(and
(Flag178-)
(Flag178prime-)
)

)
(:action Flag178Action-87
:parameters ()
:precondition
(and
(Flag81-)
(Flag81prime-)
)
:effect
(and
(Flag178-)
(Flag178prime-)
)

)
(:action Flag178Action-88
:parameters ()
:precondition
(and
(Flag104-)
(Flag104prime-)
)
:effect
(and
(Flag178-)
(Flag178prime-)
)

)
(:action Flag178Action-89
:parameters ()
:precondition
(and
(Flag161-)
(Flag161prime-)
)
:effect
(and
(Flag178-)
(Flag178prime-)
)

)
(:action Flag178Action-90
:parameters ()
:precondition
(and
(Flag129-)
(Flag129prime-)
)
:effect
(and
(Flag178-)
(Flag178prime-)
)

)
(:action Flag178Action-91
:parameters ()
:precondition
(and
(Flag162-)
(Flag162prime-)
)
:effect
(and
(Flag178-)
(Flag178prime-)
)

)
(:action Flag178Action-92
:parameters ()
:precondition
(and
(Flag65-)
(Flag65prime-)
)
:effect
(and
(Flag178-)
(Flag178prime-)
)

)
(:action Flag178Action-93
:parameters ()
:precondition
(and
(Flag89-)
(Flag89prime-)
)
:effect
(and
(Flag178-)
(Flag178prime-)
)

)
(:action Flag178Action-94
:parameters ()
:precondition
(and
(Flag105-)
(Flag105prime-)
)
:effect
(and
(Flag178-)
(Flag178prime-)
)

)
(:action Flag178Action-95
:parameters ()
:precondition
(and
(Flag74-)
(Flag74prime-)
)
:effect
(and
(Flag178-)
(Flag178prime-)
)

)
(:action Flag178Action-96
:parameters ()
:precondition
(and
(Flag91-)
(Flag91prime-)
)
:effect
(and
(Flag178-)
(Flag178prime-)
)

)
(:action Flag178Action-97
:parameters ()
:precondition
(and
(Flag107-)
(Flag107prime-)
)
:effect
(and
(Flag178-)
(Flag178prime-)
)

)
(:action Flag178Action-98
:parameters ()
:precondition
(and
(Flag68-)
(Flag68prime-)
)
:effect
(and
(Flag178-)
(Flag178prime-)
)

)
(:action Flag178Action-99
:parameters ()
:precondition
(and
(Flag163-)
(Flag163prime-)
)
:effect
(and
(Flag178-)
(Flag178prime-)
)

)
(:action Flag178Action-100
:parameters ()
:precondition
(and
(Flag113-)
(Flag113prime-)
)
:effect
(and
(Flag178-)
(Flag178prime-)
)

)
(:action Prim178Action
:parameters ()
:precondition
(and
(not (Flag41-))
(Flag41prime-)
(not (Flag47-))
(Flag47prime-)
(not (Flag134-))
(Flag134prime-)
(not (Flag150-))
(Flag150prime-)
(not (Flag44-))
(Flag44prime-)
(not (Flag135-))
(Flag135prime-)
(not (Flag124-))
(Flag124prime-)
(not (Flag165-))
(Flag165prime-)
(not (Flag126-))
(Flag126prime-)
(not (Flag84-))
(Flag84prime-)
(not (Flag85-))
(Flag85prime-)
(not (Flag63-))
(Flag63prime-)
(not (Flag166-))
(Flag166prime-)
(not (Flag146-))
(Flag146prime-)
(not (Flag151-))
(Flag151prime-)
(not (Flag92-))
(Flag92prime-)
(not (Flag69-))
(Flag69prime-)
(not (Flag137-))
(Flag137prime-)
(not (Flag42-))
(Flag42prime-)
(not (Flag97-))
(Flag97prime-)
(not (Flag48-))
(Flag48prime-)
(not (Flag167-))
(Flag167prime-)
(not (Flag122-))
(Flag122prime-)
(not (Flag153-))
(Flag153prime-)
(not (Flag138-))
(Flag138prime-)
(not (Flag62-))
(Flag62prime-)
(not (Flag139-))
(Flag139prime-)
(not (Flag82-))
(Flag82prime-)
(not (Flag102-))
(Flag102prime-)
(not (Flag57-))
(Flag57prime-)
(not (Flag87-))
(Flag87prime-)
(not (Flag123-))
(Flag123prime-)
(not (Flag140-))
(Flag140prime-)
(not (Flag168-))
(Flag168prime-)
(not (Flag61-))
(Flag61prime-)
(not (Flag52-))
(Flag52prime-)
(not (Flag154-))
(Flag154prime-)
(not (Flag128-))
(Flag128prime-)
(not (Flag169-))
(Flag169prime-)
(not (Flag90-))
(Flag90prime-)
(not (Flag101-))
(Flag101prime-)
(not (Flag156-))
(Flag156prime-)
(not (Flag141-))
(Flag141prime-)
(not (Flag111-))
(Flag111prime-)
(not (Flag73-))
(Flag73prime-)
(not (Flag143-))
(Flag143prime-)
(not (Flag93-))
(Flag93prime-)
(not (Flag157-))
(Flag157prime-)
(not (Flag115-))
(Flag115prime-)
(not (Flag116-))
(Flag116prime-)
(not (Flag58-))
(Flag58prime-)
(not (Flag158-))
(Flag158prime-)
(not (Flag79-))
(Flag79prime-)
(not (Flag159-))
(Flag159prime-)
(not (Flag118-))
(Flag118prime-)
(not (Flag170-))
(Flag170prime-)
(not (Flag171-))
(Flag171prime-)
(not (Flag172-))
(Flag172prime-)
(not (Flag100-))
(Flag100prime-)
(not (Flag108-))
(Flag108prime-)
(not (Flag173-))
(Flag173prime-)
(not (Flag130-))
(Flag130prime-)
(not (Flag160-))
(Flag160prime-)
(not (Flag83-))
(Flag83prime-)
(not (Flag174-))
(Flag174prime-)
(not (Flag66-))
(Flag66prime-)
(not (Flag106-))
(Flag106prime-)
(not (Flag67-))
(Flag67prime-)
(not (Flag144-))
(Flag144prime-)
(not (Flag175-))
(Flag175prime-)
(not (Flag121-))
(Flag121prime-)
(not (Flag145-))
(Flag145prime-)
(not (Flag176-))
(Flag176prime-)
(not (Flag132-))
(Flag132prime-)
(not (Flag40-))
(Flag40prime-)
(not (Flag96-))
(Flag96prime-)
(not (Flag43-))
(Flag43prime-)
(not (Flag120-))
(Flag120prime-)
(not (Flag46-))
(Flag46prime-)
(not (Flag98-))
(Flag98prime-)
(not (Flag60-))
(Flag60prime-)
(not (Flag119-))
(Flag119prime-)
(not (Flag127-))
(Flag127prime-)
(not (Flag53-))
(Flag53prime-)
(not (Flag147-))
(Flag147prime-)
(not (Flag55-))
(Flag55prime-)
(not (Flag177-))
(Flag177prime-)
(not (Flag81-))
(Flag81prime-)
(not (Flag104-))
(Flag104prime-)
(not (Flag161-))
(Flag161prime-)
(not (Flag129-))
(Flag129prime-)
(not (Flag162-))
(Flag162prime-)
(not (Flag65-))
(Flag65prime-)
(not (Flag89-))
(Flag89prime-)
(not (Flag105-))
(Flag105prime-)
(not (Flag74-))
(Flag74prime-)
(not (Flag91-))
(Flag91prime-)
(not (Flag107-))
(Flag107prime-)
(not (Flag68-))
(Flag68prime-)
(not (Flag163-))
(Flag163prime-)
(not (Flag113-))
(Flag113prime-)
)
:effect
(and
(Flag178prime-)
(not (Flag178-))
)

)
(:action Flag191Action-0
:parameters ()
:precondition
(and
(Flag41-)
(Flag41prime-)
)
:effect
(and
(Flag191-)
(Flag191prime-)
)

)
(:action Flag191Action-1
:parameters ()
:precondition
(and
(Flag47-)
(Flag47prime-)
)
:effect
(and
(Flag191-)
(Flag191prime-)
)

)
(:action Flag191Action-2
:parameters ()
:precondition
(and
(Flag179-)
(Flag179prime-)
)
:effect
(and
(Flag191-)
(Flag191prime-)
)

)
(:action Flag191Action-3
:parameters ()
:precondition
(and
(Flag180-)
(Flag180prime-)
)
:effect
(and
(Flag191-)
(Flag191prime-)
)

)
(:action Flag191Action-4
:parameters ()
:precondition
(and
(Flag134-)
(Flag134prime-)
)
:effect
(and
(Flag191-)
(Flag191prime-)
)

)
(:action Flag191Action-5
:parameters ()
:precondition
(and
(Flag150-)
(Flag150prime-)
)
:effect
(and
(Flag191-)
(Flag191prime-)
)

)
(:action Flag191Action-6
:parameters ()
:precondition
(and
(Flag135-)
(Flag135prime-)
)
:effect
(and
(Flag191-)
(Flag191prime-)
)

)
(:action Flag191Action-7
:parameters ()
:precondition
(and
(Flag124-)
(Flag124prime-)
)
:effect
(and
(Flag191-)
(Flag191prime-)
)

)
(:action Flag191Action-8
:parameters ()
:precondition
(and
(Flag165-)
(Flag165prime-)
)
:effect
(and
(Flag191-)
(Flag191prime-)
)

)
(:action Flag191Action-9
:parameters ()
:precondition
(and
(Flag126-)
(Flag126prime-)
)
:effect
(and
(Flag191-)
(Flag191prime-)
)

)
(:action Flag191Action-10
:parameters ()
:precondition
(and
(Flag84-)
(Flag84prime-)
)
:effect
(and
(Flag191-)
(Flag191prime-)
)

)
(:action Flag191Action-11
:parameters ()
:precondition
(and
(Flag85-)
(Flag85prime-)
)
:effect
(and
(Flag191-)
(Flag191prime-)
)

)
(:action Flag191Action-12
:parameters ()
:precondition
(and
(Flag63-)
(Flag63prime-)
)
:effect
(and
(Flag191-)
(Flag191prime-)
)

)
(:action Flag191Action-13
:parameters ()
:precondition
(and
(Flag181-)
(Flag181prime-)
)
:effect
(and
(Flag191-)
(Flag191prime-)
)

)
(:action Flag191Action-14
:parameters ()
:precondition
(and
(Flag87-)
(Flag87prime-)
)
:effect
(and
(Flag191-)
(Flag191prime-)
)

)
(:action Flag191Action-15
:parameters ()
:precondition
(and
(Flag43-)
(Flag43prime-)
)
:effect
(and
(Flag191-)
(Flag191prime-)
)

)
(:action Flag191Action-16
:parameters ()
:precondition
(and
(Flag151-)
(Flag151prime-)
)
:effect
(and
(Flag191-)
(Flag191prime-)
)

)
(:action Flag191Action-17
:parameters ()
:precondition
(and
(Flag92-)
(Flag92prime-)
)
:effect
(and
(Flag191-)
(Flag191prime-)
)

)
(:action Flag191Action-18
:parameters ()
:precondition
(and
(Flag159-)
(Flag159prime-)
)
:effect
(and
(Flag191-)
(Flag191prime-)
)

)
(:action Flag191Action-19
:parameters ()
:precondition
(and
(Flag69-)
(Flag69prime-)
)
:effect
(and
(Flag191-)
(Flag191prime-)
)

)
(:action Flag191Action-20
:parameters ()
:precondition
(and
(Flag137-)
(Flag137prime-)
)
:effect
(and
(Flag191-)
(Flag191prime-)
)

)
(:action Flag191Action-21
:parameters ()
:precondition
(and
(Flag42-)
(Flag42prime-)
)
:effect
(and
(Flag191-)
(Flag191prime-)
)

)
(:action Flag191Action-22
:parameters ()
:precondition
(and
(Flag97-)
(Flag97prime-)
)
:effect
(and
(Flag191-)
(Flag191prime-)
)

)
(:action Flag191Action-23
:parameters ()
:precondition
(and
(Flag48-)
(Flag48prime-)
)
:effect
(and
(Flag191-)
(Flag191prime-)
)

)
(:action Flag191Action-24
:parameters ()
:precondition
(and
(Flag167-)
(Flag167prime-)
)
:effect
(and
(Flag191-)
(Flag191prime-)
)

)
(:action Flag191Action-25
:parameters ()
:precondition
(and
(Flag122-)
(Flag122prime-)
)
:effect
(and
(Flag191-)
(Flag191prime-)
)

)
(:action Flag191Action-26
:parameters ()
:precondition
(and
(Flag153-)
(Flag153prime-)
)
:effect
(and
(Flag191-)
(Flag191prime-)
)

)
(:action Flag191Action-27
:parameters ()
:precondition
(and
(Flag182-)
(Flag182prime-)
)
:effect
(and
(Flag191-)
(Flag191prime-)
)

)
(:action Flag191Action-28
:parameters ()
:precondition
(and
(Flag139-)
(Flag139prime-)
)
:effect
(and
(Flag191-)
(Flag191prime-)
)

)
(:action Flag191Action-29
:parameters ()
:precondition
(and
(Flag102-)
(Flag102prime-)
)
:effect
(and
(Flag191-)
(Flag191prime-)
)

)
(:action Flag191Action-30
:parameters ()
:precondition
(and
(Flag89-)
(Flag89prime-)
)
:effect
(and
(Flag191-)
(Flag191prime-)
)

)
(:action Flag191Action-31
:parameters ()
:precondition
(and
(Flag57-)
(Flag57prime-)
)
:effect
(and
(Flag191-)
(Flag191prime-)
)

)
(:action Flag191Action-32
:parameters ()
:precondition
(and
(Flag166-)
(Flag166prime-)
)
:effect
(and
(Flag191-)
(Flag191prime-)
)

)
(:action Flag191Action-33
:parameters ()
:precondition
(and
(Flag123-)
(Flag123prime-)
)
:effect
(and
(Flag191-)
(Flag191prime-)
)

)
(:action Flag191Action-34
:parameters ()
:precondition
(and
(Flag140-)
(Flag140prime-)
)
:effect
(and
(Flag191-)
(Flag191prime-)
)

)
(:action Flag191Action-35
:parameters ()
:precondition
(and
(Flag61-)
(Flag61prime-)
)
:effect
(and
(Flag191-)
(Flag191prime-)
)

)
(:action Flag191Action-36
:parameters ()
:precondition
(and
(Flag183-)
(Flag183prime-)
)
:effect
(and
(Flag191-)
(Flag191prime-)
)

)
(:action Flag191Action-37
:parameters ()
:precondition
(and
(Flag154-)
(Flag154prime-)
)
:effect
(and
(Flag191-)
(Flag191prime-)
)

)
(:action Flag191Action-38
:parameters ()
:precondition
(and
(Flag128-)
(Flag128prime-)
)
:effect
(and
(Flag191-)
(Flag191prime-)
)

)
(:action Flag191Action-39
:parameters ()
:precondition
(and
(Flag184-)
(Flag184prime-)
)
:effect
(and
(Flag191-)
(Flag191prime-)
)

)
(:action Flag191Action-40
:parameters ()
:precondition
(and
(Flag169-)
(Flag169prime-)
)
:effect
(and
(Flag191-)
(Flag191prime-)
)

)
(:action Flag191Action-41
:parameters ()
:precondition
(and
(Flag90-)
(Flag90prime-)
)
:effect
(and
(Flag191-)
(Flag191prime-)
)

)
(:action Flag191Action-42
:parameters ()
:precondition
(and
(Flag74-)
(Flag74prime-)
)
:effect
(and
(Flag191-)
(Flag191prime-)
)

)
(:action Flag191Action-43
:parameters ()
:precondition
(and
(Flag156-)
(Flag156prime-)
)
:effect
(and
(Flag191-)
(Flag191prime-)
)

)
(:action Flag191Action-44
:parameters ()
:precondition
(and
(Flag141-)
(Flag141prime-)
)
:effect
(and
(Flag191-)
(Flag191prime-)
)

)
(:action Flag191Action-45
:parameters ()
:precondition
(and
(Flag111-)
(Flag111prime-)
)
:effect
(and
(Flag191-)
(Flag191prime-)
)

)
(:action Flag191Action-46
:parameters ()
:precondition
(and
(Flag73-)
(Flag73prime-)
)
:effect
(and
(Flag191-)
(Flag191prime-)
)

)
(:action Flag191Action-47
:parameters ()
:precondition
(and
(Flag143-)
(Flag143prime-)
)
:effect
(and
(Flag191-)
(Flag191prime-)
)

)
(:action Flag191Action-48
:parameters ()
:precondition
(and
(Flag93-)
(Flag93prime-)
)
:effect
(and
(Flag191-)
(Flag191prime-)
)

)
(:action Flag191Action-49
:parameters ()
:precondition
(and
(Flag157-)
(Flag157prime-)
)
:effect
(and
(Flag191-)
(Flag191prime-)
)

)
(:action Flag191Action-50
:parameters ()
:precondition
(and
(Flag115-)
(Flag115prime-)
)
:effect
(and
(Flag191-)
(Flag191prime-)
)

)
(:action Flag191Action-51
:parameters ()
:precondition
(and
(Flag116-)
(Flag116prime-)
)
:effect
(and
(Flag191-)
(Flag191prime-)
)

)
(:action Flag191Action-52
:parameters ()
:precondition
(and
(Flag44-)
(Flag44prime-)
)
:effect
(and
(Flag191-)
(Flag191prime-)
)

)
(:action Flag191Action-53
:parameters ()
:precondition
(and
(Flag158-)
(Flag158prime-)
)
:effect
(and
(Flag191-)
(Flag191prime-)
)

)
(:action Flag191Action-54
:parameters ()
:precondition
(and
(Flag79-)
(Flag79prime-)
)
:effect
(and
(Flag191-)
(Flag191prime-)
)

)
(:action Flag191Action-55
:parameters ()
:precondition
(and
(Flag185-)
(Flag185prime-)
)
:effect
(and
(Flag191-)
(Flag191prime-)
)

)
(:action Flag191Action-56
:parameters ()
:precondition
(and
(Flag118-)
(Flag118prime-)
)
:effect
(and
(Flag191-)
(Flag191prime-)
)

)
(:action Flag191Action-57
:parameters ()
:precondition
(and
(Flag170-)
(Flag170prime-)
)
:effect
(and
(Flag191-)
(Flag191prime-)
)

)
(:action Flag191Action-58
:parameters ()
:precondition
(and
(Flag172-)
(Flag172prime-)
)
:effect
(and
(Flag191-)
(Flag191prime-)
)

)
(:action Flag191Action-59
:parameters ()
:precondition
(and
(Flag108-)
(Flag108prime-)
)
:effect
(and
(Flag191-)
(Flag191prime-)
)

)
(:action Flag191Action-60
:parameters ()
:precondition
(and
(Flag173-)
(Flag173prime-)
)
:effect
(and
(Flag191-)
(Flag191prime-)
)

)
(:action Flag191Action-61
:parameters ()
:precondition
(and
(Flag130-)
(Flag130prime-)
)
:effect
(and
(Flag191-)
(Flag191prime-)
)

)
(:action Flag191Action-62
:parameters ()
:precondition
(and
(Flag160-)
(Flag160prime-)
)
:effect
(and
(Flag191-)
(Flag191prime-)
)

)
(:action Flag191Action-63
:parameters ()
:precondition
(and
(Flag101-)
(Flag101prime-)
)
:effect
(and
(Flag191-)
(Flag191prime-)
)

)
(:action Flag191Action-64
:parameters ()
:precondition
(and
(Flag83-)
(Flag83prime-)
)
:effect
(and
(Flag191-)
(Flag191prime-)
)

)
(:action Flag191Action-65
:parameters ()
:precondition
(and
(Flag186-)
(Flag186prime-)
)
:effect
(and
(Flag191-)
(Flag191prime-)
)

)
(:action Flag191Action-66
:parameters ()
:precondition
(and
(Flag174-)
(Flag174prime-)
)
:effect
(and
(Flag191-)
(Flag191prime-)
)

)
(:action Flag191Action-67
:parameters ()
:precondition
(and
(Flag58-)
(Flag58prime-)
)
:effect
(and
(Flag191-)
(Flag191prime-)
)

)
(:action Flag191Action-68
:parameters ()
:precondition
(and
(Flag66-)
(Flag66prime-)
)
:effect
(and
(Flag191-)
(Flag191prime-)
)

)
(:action Flag191Action-69
:parameters ()
:precondition
(and
(Flag106-)
(Flag106prime-)
)
:effect
(and
(Flag191-)
(Flag191prime-)
)

)
(:action Flag191Action-70
:parameters ()
:precondition
(and
(Flag187-)
(Flag187prime-)
)
:effect
(and
(Flag191-)
(Flag191prime-)
)

)
(:action Flag191Action-71
:parameters ()
:precondition
(and
(Flag67-)
(Flag67prime-)
)
:effect
(and
(Flag191-)
(Flag191prime-)
)

)
(:action Flag191Action-72
:parameters ()
:precondition
(and
(Flag144-)
(Flag144prime-)
)
:effect
(and
(Flag191-)
(Flag191prime-)
)

)
(:action Flag191Action-73
:parameters ()
:precondition
(and
(Flag175-)
(Flag175prime-)
)
:effect
(and
(Flag191-)
(Flag191prime-)
)

)
(:action Flag191Action-74
:parameters ()
:precondition
(and
(Flag121-)
(Flag121prime-)
)
:effect
(and
(Flag191-)
(Flag191prime-)
)

)
(:action Flag191Action-75
:parameters ()
:precondition
(and
(Flag145-)
(Flag145prime-)
)
:effect
(and
(Flag191-)
(Flag191prime-)
)

)
(:action Flag191Action-76
:parameters ()
:precondition
(and
(Flag176-)
(Flag176prime-)
)
:effect
(and
(Flag191-)
(Flag191prime-)
)

)
(:action Flag191Action-77
:parameters ()
:precondition
(and
(Flag132-)
(Flag132prime-)
)
:effect
(and
(Flag191-)
(Flag191prime-)
)

)
(:action Flag191Action-78
:parameters ()
:precondition
(and
(Flag40-)
(Flag40prime-)
)
:effect
(and
(Flag191-)
(Flag191prime-)
)

)
(:action Flag191Action-79
:parameters ()
:precondition
(and
(Flag96-)
(Flag96prime-)
)
:effect
(and
(Flag191-)
(Flag191prime-)
)

)
(:action Flag191Action-80
:parameters ()
:precondition
(and
(Flag146-)
(Flag146prime-)
)
:effect
(and
(Flag191-)
(Flag191prime-)
)

)
(:action Flag191Action-81
:parameters ()
:precondition
(and
(Flag46-)
(Flag46prime-)
)
:effect
(and
(Flag191-)
(Flag191prime-)
)

)
(:action Flag191Action-82
:parameters ()
:precondition
(and
(Flag188-)
(Flag188prime-)
)
:effect
(and
(Flag191-)
(Flag191prime-)
)

)
(:action Flag191Action-83
:parameters ()
:precondition
(and
(Flag98-)
(Flag98prime-)
)
:effect
(and
(Flag191-)
(Flag191prime-)
)

)
(:action Flag191Action-84
:parameters ()
:precondition
(and
(Flag60-)
(Flag60prime-)
)
:effect
(and
(Flag191-)
(Flag191prime-)
)

)
(:action Flag191Action-85
:parameters ()
:precondition
(and
(Flag119-)
(Flag119prime-)
)
:effect
(and
(Flag191-)
(Flag191prime-)
)

)
(:action Flag191Action-86
:parameters ()
:precondition
(and
(Flag127-)
(Flag127prime-)
)
:effect
(and
(Flag191-)
(Flag191prime-)
)

)
(:action Flag191Action-87
:parameters ()
:precondition
(and
(Flag53-)
(Flag53prime-)
)
:effect
(and
(Flag191-)
(Flag191prime-)
)

)
(:action Flag191Action-88
:parameters ()
:precondition
(and
(Flag147-)
(Flag147prime-)
)
:effect
(and
(Flag191-)
(Flag191prime-)
)

)
(:action Flag191Action-89
:parameters ()
:precondition
(and
(Flag55-)
(Flag55prime-)
)
:effect
(and
(Flag191-)
(Flag191prime-)
)

)
(:action Flag191Action-90
:parameters ()
:precondition
(and
(Flag177-)
(Flag177prime-)
)
:effect
(and
(Flag191-)
(Flag191prime-)
)

)
(:action Flag191Action-91
:parameters ()
:precondition
(and
(Flag81-)
(Flag81prime-)
)
:effect
(and
(Flag191-)
(Flag191prime-)
)

)
(:action Flag191Action-92
:parameters ()
:precondition
(and
(Flag161-)
(Flag161prime-)
)
:effect
(and
(Flag191-)
(Flag191prime-)
)

)
(:action Flag191Action-93
:parameters ()
:precondition
(and
(Flag129-)
(Flag129prime-)
)
:effect
(and
(Flag191-)
(Flag191prime-)
)

)
(:action Flag191Action-94
:parameters ()
:precondition
(and
(Flag162-)
(Flag162prime-)
)
:effect
(and
(Flag191-)
(Flag191prime-)
)

)
(:action Flag191Action-95
:parameters ()
:precondition
(and
(Flag65-)
(Flag65prime-)
)
:effect
(and
(Flag191-)
(Flag191prime-)
)

)
(:action Flag191Action-96
:parameters ()
:precondition
(and
(Flag189-)
(Flag189prime-)
)
:effect
(and
(Flag191-)
(Flag191prime-)
)

)
(:action Flag191Action-97
:parameters ()
:precondition
(and
(Flag105-)
(Flag105prime-)
)
:effect
(and
(Flag191-)
(Flag191prime-)
)

)
(:action Flag191Action-98
:parameters ()
:precondition
(and
(Flag113-)
(Flag113prime-)
)
:effect
(and
(Flag191-)
(Flag191prime-)
)

)
(:action Flag191Action-99
:parameters ()
:precondition
(and
(Flag91-)
(Flag91prime-)
)
:effect
(and
(Flag191-)
(Flag191prime-)
)

)
(:action Flag191Action-100
:parameters ()
:precondition
(and
(Flag107-)
(Flag107prime-)
)
:effect
(and
(Flag191-)
(Flag191prime-)
)

)
(:action Flag191Action-101
:parameters ()
:precondition
(and
(Flag68-)
(Flag68prime-)
)
:effect
(and
(Flag191-)
(Flag191prime-)
)

)
(:action Flag191Action-102
:parameters ()
:precondition
(and
(Flag163-)
(Flag163prime-)
)
:effect
(and
(Flag191-)
(Flag191prime-)
)

)
(:action Flag191Action-103
:parameters ()
:precondition
(and
(Flag190-)
(Flag190prime-)
)
:effect
(and
(Flag191-)
(Flag191prime-)
)

)
(:action Prim191Action
:parameters ()
:precondition
(and
(not (Flag41-))
(Flag41prime-)
(not (Flag47-))
(Flag47prime-)
(not (Flag179-))
(Flag179prime-)
(not (Flag180-))
(Flag180prime-)
(not (Flag134-))
(Flag134prime-)
(not (Flag150-))
(Flag150prime-)
(not (Flag135-))
(Flag135prime-)
(not (Flag124-))
(Flag124prime-)
(not (Flag165-))
(Flag165prime-)
(not (Flag126-))
(Flag126prime-)
(not (Flag84-))
(Flag84prime-)
(not (Flag85-))
(Flag85prime-)
(not (Flag63-))
(Flag63prime-)
(not (Flag181-))
(Flag181prime-)
(not (Flag87-))
(Flag87prime-)
(not (Flag43-))
(Flag43prime-)
(not (Flag151-))
(Flag151prime-)
(not (Flag92-))
(Flag92prime-)
(not (Flag159-))
(Flag159prime-)
(not (Flag69-))
(Flag69prime-)
(not (Flag137-))
(Flag137prime-)
(not (Flag42-))
(Flag42prime-)
(not (Flag97-))
(Flag97prime-)
(not (Flag48-))
(Flag48prime-)
(not (Flag167-))
(Flag167prime-)
(not (Flag122-))
(Flag122prime-)
(not (Flag153-))
(Flag153prime-)
(not (Flag182-))
(Flag182prime-)
(not (Flag139-))
(Flag139prime-)
(not (Flag102-))
(Flag102prime-)
(not (Flag89-))
(Flag89prime-)
(not (Flag57-))
(Flag57prime-)
(not (Flag166-))
(Flag166prime-)
(not (Flag123-))
(Flag123prime-)
(not (Flag140-))
(Flag140prime-)
(not (Flag61-))
(Flag61prime-)
(not (Flag183-))
(Flag183prime-)
(not (Flag154-))
(Flag154prime-)
(not (Flag128-))
(Flag128prime-)
(not (Flag184-))
(Flag184prime-)
(not (Flag169-))
(Flag169prime-)
(not (Flag90-))
(Flag90prime-)
(not (Flag74-))
(Flag74prime-)
(not (Flag156-))
(Flag156prime-)
(not (Flag141-))
(Flag141prime-)
(not (Flag111-))
(Flag111prime-)
(not (Flag73-))
(Flag73prime-)
(not (Flag143-))
(Flag143prime-)
(not (Flag93-))
(Flag93prime-)
(not (Flag157-))
(Flag157prime-)
(not (Flag115-))
(Flag115prime-)
(not (Flag116-))
(Flag116prime-)
(not (Flag44-))
(Flag44prime-)
(not (Flag158-))
(Flag158prime-)
(not (Flag79-))
(Flag79prime-)
(not (Flag185-))
(Flag185prime-)
(not (Flag118-))
(Flag118prime-)
(not (Flag170-))
(Flag170prime-)
(not (Flag172-))
(Flag172prime-)
(not (Flag108-))
(Flag108prime-)
(not (Flag173-))
(Flag173prime-)
(not (Flag130-))
(Flag130prime-)
(not (Flag160-))
(Flag160prime-)
(not (Flag101-))
(Flag101prime-)
(not (Flag83-))
(Flag83prime-)
(not (Flag186-))
(Flag186prime-)
(not (Flag174-))
(Flag174prime-)
(not (Flag58-))
(Flag58prime-)
(not (Flag66-))
(Flag66prime-)
(not (Flag106-))
(Flag106prime-)
(not (Flag187-))
(Flag187prime-)
(not (Flag67-))
(Flag67prime-)
(not (Flag144-))
(Flag144prime-)
(not (Flag175-))
(Flag175prime-)
(not (Flag121-))
(Flag121prime-)
(not (Flag145-))
(Flag145prime-)
(not (Flag176-))
(Flag176prime-)
(not (Flag132-))
(Flag132prime-)
(not (Flag40-))
(Flag40prime-)
(not (Flag96-))
(Flag96prime-)
(not (Flag146-))
(Flag146prime-)
(not (Flag46-))
(Flag46prime-)
(not (Flag188-))
(Flag188prime-)
(not (Flag98-))
(Flag98prime-)
(not (Flag60-))
(Flag60prime-)
(not (Flag119-))
(Flag119prime-)
(not (Flag127-))
(Flag127prime-)
(not (Flag53-))
(Flag53prime-)
(not (Flag147-))
(Flag147prime-)
(not (Flag55-))
(Flag55prime-)
(not (Flag177-))
(Flag177prime-)
(not (Flag81-))
(Flag81prime-)
(not (Flag161-))
(Flag161prime-)
(not (Flag129-))
(Flag129prime-)
(not (Flag162-))
(Flag162prime-)
(not (Flag65-))
(Flag65prime-)
(not (Flag189-))
(Flag189prime-)
(not (Flag105-))
(Flag105prime-)
(not (Flag113-))
(Flag113prime-)
(not (Flag91-))
(Flag91prime-)
(not (Flag107-))
(Flag107prime-)
(not (Flag68-))
(Flag68prime-)
(not (Flag163-))
(Flag163prime-)
(not (Flag190-))
(Flag190prime-)
)
:effect
(and
(Flag191prime-)
(not (Flag191-))
)

)
(:action Flag202Action-0
:parameters ()
:precondition
(and
(Flag41-)
(Flag41prime-)
)
:effect
(and
(Flag202-)
(Flag202prime-)
)

)
(:action Flag202Action-1
:parameters ()
:precondition
(and
(Flag47-)
(Flag47prime-)
)
:effect
(and
(Flag202-)
(Flag202prime-)
)

)
(:action Flag202Action-2
:parameters ()
:precondition
(and
(Flag192-)
(Flag192prime-)
)
:effect
(and
(Flag202-)
(Flag202prime-)
)

)
(:action Flag202Action-3
:parameters ()
:precondition
(and
(Flag179-)
(Flag179prime-)
)
:effect
(and
(Flag202-)
(Flag202prime-)
)

)
(:action Flag202Action-4
:parameters ()
:precondition
(and
(Flag134-)
(Flag134prime-)
)
:effect
(and
(Flag202-)
(Flag202prime-)
)

)
(:action Flag202Action-5
:parameters ()
:precondition
(and
(Flag150-)
(Flag150prime-)
)
:effect
(and
(Flag202-)
(Flag202prime-)
)

)
(:action Flag202Action-6
:parameters ()
:precondition
(and
(Flag135-)
(Flag135prime-)
)
:effect
(and
(Flag202-)
(Flag202prime-)
)

)
(:action Flag202Action-7
:parameters ()
:precondition
(and
(Flag165-)
(Flag165prime-)
)
:effect
(and
(Flag202-)
(Flag202prime-)
)

)
(:action Flag202Action-8
:parameters ()
:precondition
(and
(Flag126-)
(Flag126prime-)
)
:effect
(and
(Flag202-)
(Flag202prime-)
)

)
(:action Flag202Action-9
:parameters ()
:precondition
(and
(Flag84-)
(Flag84prime-)
)
:effect
(and
(Flag202-)
(Flag202prime-)
)

)
(:action Flag202Action-10
:parameters ()
:precondition
(and
(Flag127-)
(Flag127prime-)
)
:effect
(and
(Flag202-)
(Flag202prime-)
)

)
(:action Flag202Action-11
:parameters ()
:precondition
(and
(Flag63-)
(Flag63prime-)
)
:effect
(and
(Flag202-)
(Flag202prime-)
)

)
(:action Flag202Action-12
:parameters ()
:precondition
(and
(Flag181-)
(Flag181prime-)
)
:effect
(and
(Flag202-)
(Flag202prime-)
)

)
(:action Flag202Action-13
:parameters ()
:precondition
(and
(Flag87-)
(Flag87prime-)
)
:effect
(and
(Flag202-)
(Flag202prime-)
)

)
(:action Flag202Action-14
:parameters ()
:precondition
(and
(Flag43-)
(Flag43prime-)
)
:effect
(and
(Flag202-)
(Flag202prime-)
)

)
(:action Flag202Action-15
:parameters ()
:precondition
(and
(Flag151-)
(Flag151prime-)
)
:effect
(and
(Flag202-)
(Flag202prime-)
)

)
(:action Flag202Action-16
:parameters ()
:precondition
(and
(Flag92-)
(Flag92prime-)
)
:effect
(and
(Flag202-)
(Flag202prime-)
)

)
(:action Flag202Action-17
:parameters ()
:precondition
(and
(Flag118-)
(Flag118prime-)
)
:effect
(and
(Flag202-)
(Flag202prime-)
)

)
(:action Flag202Action-18
:parameters ()
:precondition
(and
(Flag137-)
(Flag137prime-)
)
:effect
(and
(Flag202-)
(Flag202prime-)
)

)
(:action Flag202Action-19
:parameters ()
:precondition
(and
(Flag42-)
(Flag42prime-)
)
:effect
(and
(Flag202-)
(Flag202prime-)
)

)
(:action Flag202Action-20
:parameters ()
:precondition
(and
(Flag97-)
(Flag97prime-)
)
:effect
(and
(Flag202-)
(Flag202prime-)
)

)
(:action Flag202Action-21
:parameters ()
:precondition
(and
(Flag48-)
(Flag48prime-)
)
:effect
(and
(Flag202-)
(Flag202prime-)
)

)
(:action Flag202Action-22
:parameters ()
:precondition
(and
(Flag167-)
(Flag167prime-)
)
:effect
(and
(Flag202-)
(Flag202prime-)
)

)
(:action Flag202Action-23
:parameters ()
:precondition
(and
(Flag122-)
(Flag122prime-)
)
:effect
(and
(Flag202-)
(Flag202prime-)
)

)
(:action Flag202Action-24
:parameters ()
:precondition
(and
(Flag153-)
(Flag153prime-)
)
:effect
(and
(Flag202-)
(Flag202prime-)
)

)
(:action Flag202Action-25
:parameters ()
:precondition
(and
(Flag182-)
(Flag182prime-)
)
:effect
(and
(Flag202-)
(Flag202prime-)
)

)
(:action Flag202Action-26
:parameters ()
:precondition
(and
(Flag139-)
(Flag139prime-)
)
:effect
(and
(Flag202-)
(Flag202prime-)
)

)
(:action Flag202Action-27
:parameters ()
:precondition
(and
(Flag102-)
(Flag102prime-)
)
:effect
(and
(Flag202-)
(Flag202prime-)
)

)
(:action Flag202Action-28
:parameters ()
:precondition
(and
(Flag57-)
(Flag57prime-)
)
:effect
(and
(Flag202-)
(Flag202prime-)
)

)
(:action Flag202Action-29
:parameters ()
:precondition
(and
(Flag166-)
(Flag166prime-)
)
:effect
(and
(Flag202-)
(Flag202prime-)
)

)
(:action Flag202Action-30
:parameters ()
:precondition
(and
(Flag123-)
(Flag123prime-)
)
:effect
(and
(Flag202-)
(Flag202prime-)
)

)
(:action Flag202Action-31
:parameters ()
:precondition
(and
(Flag140-)
(Flag140prime-)
)
:effect
(and
(Flag202-)
(Flag202prime-)
)

)
(:action Flag202Action-32
:parameters ()
:precondition
(and
(Flag61-)
(Flag61prime-)
)
:effect
(and
(Flag202-)
(Flag202prime-)
)

)
(:action Flag202Action-33
:parameters ()
:precondition
(and
(Flag183-)
(Flag183prime-)
)
:effect
(and
(Flag202-)
(Flag202prime-)
)

)
(:action Flag202Action-34
:parameters ()
:precondition
(and
(Flag154-)
(Flag154prime-)
)
:effect
(and
(Flag202-)
(Flag202prime-)
)

)
(:action Flag202Action-35
:parameters ()
:precondition
(and
(Flag128-)
(Flag128prime-)
)
:effect
(and
(Flag202-)
(Flag202prime-)
)

)
(:action Flag202Action-36
:parameters ()
:precondition
(and
(Flag184-)
(Flag184prime-)
)
:effect
(and
(Flag202-)
(Flag202prime-)
)

)
(:action Flag202Action-37
:parameters ()
:precondition
(and
(Flag169-)
(Flag169prime-)
)
:effect
(and
(Flag202-)
(Flag202prime-)
)

)
(:action Flag202Action-38
:parameters ()
:precondition
(and
(Flag90-)
(Flag90prime-)
)
:effect
(and
(Flag202-)
(Flag202prime-)
)

)
(:action Flag202Action-39
:parameters ()
:precondition
(and
(Flag74-)
(Flag74prime-)
)
:effect
(and
(Flag202-)
(Flag202prime-)
)

)
(:action Flag202Action-40
:parameters ()
:precondition
(and
(Flag193-)
(Flag193prime-)
)
:effect
(and
(Flag202-)
(Flag202prime-)
)

)
(:action Flag202Action-41
:parameters ()
:precondition
(and
(Flag156-)
(Flag156prime-)
)
:effect
(and
(Flag202-)
(Flag202prime-)
)

)
(:action Flag202Action-42
:parameters ()
:precondition
(and
(Flag141-)
(Flag141prime-)
)
:effect
(and
(Flag202-)
(Flag202prime-)
)

)
(:action Flag202Action-43
:parameters ()
:precondition
(and
(Flag194-)
(Flag194prime-)
)
:effect
(and
(Flag202-)
(Flag202prime-)
)

)
(:action Flag202Action-44
:parameters ()
:precondition
(and
(Flag111-)
(Flag111prime-)
)
:effect
(and
(Flag202-)
(Flag202prime-)
)

)
(:action Flag202Action-45
:parameters ()
:precondition
(and
(Flag73-)
(Flag73prime-)
)
:effect
(and
(Flag202-)
(Flag202prime-)
)

)
(:action Flag202Action-46
:parameters ()
:precondition
(and
(Flag143-)
(Flag143prime-)
)
:effect
(and
(Flag202-)
(Flag202prime-)
)

)
(:action Flag202Action-47
:parameters ()
:precondition
(and
(Flag93-)
(Flag93prime-)
)
:effect
(and
(Flag202-)
(Flag202prime-)
)

)
(:action Flag202Action-48
:parameters ()
:precondition
(and
(Flag157-)
(Flag157prime-)
)
:effect
(and
(Flag202-)
(Flag202prime-)
)

)
(:action Flag202Action-49
:parameters ()
:precondition
(and
(Flag115-)
(Flag115prime-)
)
:effect
(and
(Flag202-)
(Flag202prime-)
)

)
(:action Flag202Action-50
:parameters ()
:precondition
(and
(Flag116-)
(Flag116prime-)
)
:effect
(and
(Flag202-)
(Flag202prime-)
)

)
(:action Flag202Action-51
:parameters ()
:precondition
(and
(Flag44-)
(Flag44prime-)
)
:effect
(and
(Flag202-)
(Flag202prime-)
)

)
(:action Flag202Action-52
:parameters ()
:precondition
(and
(Flag195-)
(Flag195prime-)
)
:effect
(and
(Flag202-)
(Flag202prime-)
)

)
(:action Flag202Action-53
:parameters ()
:precondition
(and
(Flag79-)
(Flag79prime-)
)
:effect
(and
(Flag202-)
(Flag202prime-)
)

)
(:action Flag202Action-54
:parameters ()
:precondition
(and
(Flag185-)
(Flag185prime-)
)
:effect
(and
(Flag202-)
(Flag202prime-)
)

)
(:action Flag202Action-55
:parameters ()
:precondition
(and
(Flag89-)
(Flag89prime-)
)
:effect
(and
(Flag202-)
(Flag202prime-)
)

)
(:action Flag202Action-56
:parameters ()
:precondition
(and
(Flag170-)
(Flag170prime-)
)
:effect
(and
(Flag202-)
(Flag202prime-)
)

)
(:action Flag202Action-57
:parameters ()
:precondition
(and
(Flag196-)
(Flag196prime-)
)
:effect
(and
(Flag202-)
(Flag202prime-)
)

)
(:action Flag202Action-58
:parameters ()
:precondition
(and
(Flag172-)
(Flag172prime-)
)
:effect
(and
(Flag202-)
(Flag202prime-)
)

)
(:action Flag202Action-59
:parameters ()
:precondition
(and
(Flag108-)
(Flag108prime-)
)
:effect
(and
(Flag202-)
(Flag202prime-)
)

)
(:action Flag202Action-60
:parameters ()
:precondition
(and
(Flag173-)
(Flag173prime-)
)
:effect
(and
(Flag202-)
(Flag202prime-)
)

)
(:action Flag202Action-61
:parameters ()
:precondition
(and
(Flag130-)
(Flag130prime-)
)
:effect
(and
(Flag202-)
(Flag202prime-)
)

)
(:action Flag202Action-62
:parameters ()
:precondition
(and
(Flag160-)
(Flag160prime-)
)
:effect
(and
(Flag202-)
(Flag202prime-)
)

)
(:action Flag202Action-63
:parameters ()
:precondition
(and
(Flag101-)
(Flag101prime-)
)
:effect
(and
(Flag202-)
(Flag202prime-)
)

)
(:action Flag202Action-64
:parameters ()
:precondition
(and
(Flag83-)
(Flag83prime-)
)
:effect
(and
(Flag202-)
(Flag202prime-)
)

)
(:action Flag202Action-65
:parameters ()
:precondition
(and
(Flag186-)
(Flag186prime-)
)
:effect
(and
(Flag202-)
(Flag202prime-)
)

)
(:action Flag202Action-66
:parameters ()
:precondition
(and
(Flag174-)
(Flag174prime-)
)
:effect
(and
(Flag202-)
(Flag202prime-)
)

)
(:action Flag202Action-67
:parameters ()
:precondition
(and
(Flag197-)
(Flag197prime-)
)
:effect
(and
(Flag202-)
(Flag202prime-)
)

)
(:action Flag202Action-68
:parameters ()
:precondition
(and
(Flag66-)
(Flag66prime-)
)
:effect
(and
(Flag202-)
(Flag202prime-)
)

)
(:action Flag202Action-69
:parameters ()
:precondition
(and
(Flag106-)
(Flag106prime-)
)
:effect
(and
(Flag202-)
(Flag202prime-)
)

)
(:action Flag202Action-70
:parameters ()
:precondition
(and
(Flag187-)
(Flag187prime-)
)
:effect
(and
(Flag202-)
(Flag202prime-)
)

)
(:action Flag202Action-71
:parameters ()
:precondition
(and
(Flag67-)
(Flag67prime-)
)
:effect
(and
(Flag202-)
(Flag202prime-)
)

)
(:action Flag202Action-72
:parameters ()
:precondition
(and
(Flag144-)
(Flag144prime-)
)
:effect
(and
(Flag202-)
(Flag202prime-)
)

)
(:action Flag202Action-73
:parameters ()
:precondition
(and
(Flag175-)
(Flag175prime-)
)
:effect
(and
(Flag202-)
(Flag202prime-)
)

)
(:action Flag202Action-74
:parameters ()
:precondition
(and
(Flag121-)
(Flag121prime-)
)
:effect
(and
(Flag202-)
(Flag202prime-)
)

)
(:action Flag202Action-75
:parameters ()
:precondition
(and
(Flag145-)
(Flag145prime-)
)
:effect
(and
(Flag202-)
(Flag202prime-)
)

)
(:action Flag202Action-76
:parameters ()
:precondition
(and
(Flag176-)
(Flag176prime-)
)
:effect
(and
(Flag202-)
(Flag202prime-)
)

)
(:action Flag202Action-77
:parameters ()
:precondition
(and
(Flag132-)
(Flag132prime-)
)
:effect
(and
(Flag202-)
(Flag202prime-)
)

)
(:action Flag202Action-78
:parameters ()
:precondition
(and
(Flag40-)
(Flag40prime-)
)
:effect
(and
(Flag202-)
(Flag202prime-)
)

)
(:action Flag202Action-79
:parameters ()
:precondition
(and
(Flag96-)
(Flag96prime-)
)
:effect
(and
(Flag202-)
(Flag202prime-)
)

)
(:action Flag202Action-80
:parameters ()
:precondition
(and
(Flag158-)
(Flag158prime-)
)
:effect
(and
(Flag202-)
(Flag202prime-)
)

)
(:action Flag202Action-81
:parameters ()
:precondition
(and
(Flag146-)
(Flag146prime-)
)
:effect
(and
(Flag202-)
(Flag202prime-)
)

)
(:action Flag202Action-82
:parameters ()
:precondition
(and
(Flag46-)
(Flag46prime-)
)
:effect
(and
(Flag202-)
(Flag202prime-)
)

)
(:action Flag202Action-83
:parameters ()
:precondition
(and
(Flag198-)
(Flag198prime-)
)
:effect
(and
(Flag202-)
(Flag202prime-)
)

)
(:action Flag202Action-84
:parameters ()
:precondition
(and
(Flag199-)
(Flag199prime-)
)
:effect
(and
(Flag202-)
(Flag202prime-)
)

)
(:action Flag202Action-85
:parameters ()
:precondition
(and
(Flag188-)
(Flag188prime-)
)
:effect
(and
(Flag202-)
(Flag202prime-)
)

)
(:action Flag202Action-86
:parameters ()
:precondition
(and
(Flag98-)
(Flag98prime-)
)
:effect
(and
(Flag202-)
(Flag202prime-)
)

)
(:action Flag202Action-87
:parameters ()
:precondition
(and
(Flag60-)
(Flag60prime-)
)
:effect
(and
(Flag202-)
(Flag202prime-)
)

)
(:action Flag202Action-88
:parameters ()
:precondition
(and
(Flag119-)
(Flag119prime-)
)
:effect
(and
(Flag202-)
(Flag202prime-)
)

)
(:action Flag202Action-89
:parameters ()
:precondition
(and
(Flag53-)
(Flag53prime-)
)
:effect
(and
(Flag202-)
(Flag202prime-)
)

)
(:action Flag202Action-90
:parameters ()
:precondition
(and
(Flag55-)
(Flag55prime-)
)
:effect
(and
(Flag202-)
(Flag202prime-)
)

)
(:action Flag202Action-91
:parameters ()
:precondition
(and
(Flag81-)
(Flag81prime-)
)
:effect
(and
(Flag202-)
(Flag202prime-)
)

)
(:action Flag202Action-92
:parameters ()
:precondition
(and
(Flag161-)
(Flag161prime-)
)
:effect
(and
(Flag202-)
(Flag202prime-)
)

)
(:action Flag202Action-93
:parameters ()
:precondition
(and
(Flag162-)
(Flag162prime-)
)
:effect
(and
(Flag202-)
(Flag202prime-)
)

)
(:action Flag202Action-94
:parameters ()
:precondition
(and
(Flag200-)
(Flag200prime-)
)
:effect
(and
(Flag202-)
(Flag202prime-)
)

)
(:action Flag202Action-95
:parameters ()
:precondition
(and
(Flag58-)
(Flag58prime-)
)
:effect
(and
(Flag202-)
(Flag202prime-)
)

)
(:action Flag202Action-96
:parameters ()
:precondition
(and
(Flag189-)
(Flag189prime-)
)
:effect
(and
(Flag202-)
(Flag202prime-)
)

)
(:action Flag202Action-97
:parameters ()
:precondition
(and
(Flag105-)
(Flag105prime-)
)
:effect
(and
(Flag202-)
(Flag202prime-)
)

)
(:action Flag202Action-98
:parameters ()
:precondition
(and
(Flag113-)
(Flag113prime-)
)
:effect
(and
(Flag202-)
(Flag202prime-)
)

)
(:action Flag202Action-99
:parameters ()
:precondition
(and
(Flag91-)
(Flag91prime-)
)
:effect
(and
(Flag202-)
(Flag202prime-)
)

)
(:action Flag202Action-100
:parameters ()
:precondition
(and
(Flag107-)
(Flag107prime-)
)
:effect
(and
(Flag202-)
(Flag202prime-)
)

)
(:action Flag202Action-101
:parameters ()
:precondition
(and
(Flag68-)
(Flag68prime-)
)
:effect
(and
(Flag202-)
(Flag202prime-)
)

)
(:action Flag202Action-102
:parameters ()
:precondition
(and
(Flag201-)
(Flag201prime-)
)
:effect
(and
(Flag202-)
(Flag202prime-)
)

)
(:action Flag202Action-103
:parameters ()
:precondition
(and
(Flag163-)
(Flag163prime-)
)
:effect
(and
(Flag202-)
(Flag202prime-)
)

)
(:action Flag202Action-104
:parameters ()
:precondition
(and
(Flag190-)
(Flag190prime-)
)
:effect
(and
(Flag202-)
(Flag202prime-)
)

)
(:action Prim202Action
:parameters ()
:precondition
(and
(not (Flag41-))
(Flag41prime-)
(not (Flag47-))
(Flag47prime-)
(not (Flag192-))
(Flag192prime-)
(not (Flag179-))
(Flag179prime-)
(not (Flag134-))
(Flag134prime-)
(not (Flag150-))
(Flag150prime-)
(not (Flag135-))
(Flag135prime-)
(not (Flag165-))
(Flag165prime-)
(not (Flag126-))
(Flag126prime-)
(not (Flag84-))
(Flag84prime-)
(not (Flag127-))
(Flag127prime-)
(not (Flag63-))
(Flag63prime-)
(not (Flag181-))
(Flag181prime-)
(not (Flag87-))
(Flag87prime-)
(not (Flag43-))
(Flag43prime-)
(not (Flag151-))
(Flag151prime-)
(not (Flag92-))
(Flag92prime-)
(not (Flag118-))
(Flag118prime-)
(not (Flag137-))
(Flag137prime-)
(not (Flag42-))
(Flag42prime-)
(not (Flag97-))
(Flag97prime-)
(not (Flag48-))
(Flag48prime-)
(not (Flag167-))
(Flag167prime-)
(not (Flag122-))
(Flag122prime-)
(not (Flag153-))
(Flag153prime-)
(not (Flag182-))
(Flag182prime-)
(not (Flag139-))
(Flag139prime-)
(not (Flag102-))
(Flag102prime-)
(not (Flag57-))
(Flag57prime-)
(not (Flag166-))
(Flag166prime-)
(not (Flag123-))
(Flag123prime-)
(not (Flag140-))
(Flag140prime-)
(not (Flag61-))
(Flag61prime-)
(not (Flag183-))
(Flag183prime-)
(not (Flag154-))
(Flag154prime-)
(not (Flag128-))
(Flag128prime-)
(not (Flag184-))
(Flag184prime-)
(not (Flag169-))
(Flag169prime-)
(not (Flag90-))
(Flag90prime-)
(not (Flag74-))
(Flag74prime-)
(not (Flag193-))
(Flag193prime-)
(not (Flag156-))
(Flag156prime-)
(not (Flag141-))
(Flag141prime-)
(not (Flag194-))
(Flag194prime-)
(not (Flag111-))
(Flag111prime-)
(not (Flag73-))
(Flag73prime-)
(not (Flag143-))
(Flag143prime-)
(not (Flag93-))
(Flag93prime-)
(not (Flag157-))
(Flag157prime-)
(not (Flag115-))
(Flag115prime-)
(not (Flag116-))
(Flag116prime-)
(not (Flag44-))
(Flag44prime-)
(not (Flag195-))
(Flag195prime-)
(not (Flag79-))
(Flag79prime-)
(not (Flag185-))
(Flag185prime-)
(not (Flag89-))
(Flag89prime-)
(not (Flag170-))
(Flag170prime-)
(not (Flag196-))
(Flag196prime-)
(not (Flag172-))
(Flag172prime-)
(not (Flag108-))
(Flag108prime-)
(not (Flag173-))
(Flag173prime-)
(not (Flag130-))
(Flag130prime-)
(not (Flag160-))
(Flag160prime-)
(not (Flag101-))
(Flag101prime-)
(not (Flag83-))
(Flag83prime-)
(not (Flag186-))
(Flag186prime-)
(not (Flag174-))
(Flag174prime-)
(not (Flag197-))
(Flag197prime-)
(not (Flag66-))
(Flag66prime-)
(not (Flag106-))
(Flag106prime-)
(not (Flag187-))
(Flag187prime-)
(not (Flag67-))
(Flag67prime-)
(not (Flag144-))
(Flag144prime-)
(not (Flag175-))
(Flag175prime-)
(not (Flag121-))
(Flag121prime-)
(not (Flag145-))
(Flag145prime-)
(not (Flag176-))
(Flag176prime-)
(not (Flag132-))
(Flag132prime-)
(not (Flag40-))
(Flag40prime-)
(not (Flag96-))
(Flag96prime-)
(not (Flag158-))
(Flag158prime-)
(not (Flag146-))
(Flag146prime-)
(not (Flag46-))
(Flag46prime-)
(not (Flag198-))
(Flag198prime-)
(not (Flag199-))
(Flag199prime-)
(not (Flag188-))
(Flag188prime-)
(not (Flag98-))
(Flag98prime-)
(not (Flag60-))
(Flag60prime-)
(not (Flag119-))
(Flag119prime-)
(not (Flag53-))
(Flag53prime-)
(not (Flag55-))
(Flag55prime-)
(not (Flag81-))
(Flag81prime-)
(not (Flag161-))
(Flag161prime-)
(not (Flag162-))
(Flag162prime-)
(not (Flag200-))
(Flag200prime-)
(not (Flag58-))
(Flag58prime-)
(not (Flag189-))
(Flag189prime-)
(not (Flag105-))
(Flag105prime-)
(not (Flag113-))
(Flag113prime-)
(not (Flag91-))
(Flag91prime-)
(not (Flag107-))
(Flag107prime-)
(not (Flag68-))
(Flag68prime-)
(not (Flag201-))
(Flag201prime-)
(not (Flag163-))
(Flag163prime-)
(not (Flag190-))
(Flag190prime-)
)
:effect
(and
(Flag202prime-)
(not (Flag202-))
)

)
(:action Flag213Action-0
:parameters ()
:precondition
(and
(Flag41-)
(Flag41prime-)
)
:effect
(and
(Flag213-)
(Flag213prime-)
)

)
(:action Flag213Action-1
:parameters ()
:precondition
(and
(Flag47-)
(Flag47prime-)
)
:effect
(and
(Flag213-)
(Flag213prime-)
)

)
(:action Flag213Action-2
:parameters ()
:precondition
(and
(Flag203-)
(Flag203prime-)
)
:effect
(and
(Flag213-)
(Flag213prime-)
)

)
(:action Flag213Action-3
:parameters ()
:precondition
(and
(Flag192-)
(Flag192prime-)
)
:effect
(and
(Flag213-)
(Flag213prime-)
)

)
(:action Flag213Action-4
:parameters ()
:precondition
(and
(Flag179-)
(Flag179prime-)
)
:effect
(and
(Flag213-)
(Flag213prime-)
)

)
(:action Flag213Action-5
:parameters ()
:precondition
(and
(Flag204-)
(Flag204prime-)
)
:effect
(and
(Flag213-)
(Flag213prime-)
)

)
(:action Flag213Action-6
:parameters ()
:precondition
(and
(Flag134-)
(Flag134prime-)
)
:effect
(and
(Flag213-)
(Flag213prime-)
)

)
(:action Flag213Action-7
:parameters ()
:precondition
(and
(Flag150-)
(Flag150prime-)
)
:effect
(and
(Flag213-)
(Flag213prime-)
)

)
(:action Flag213Action-8
:parameters ()
:precondition
(and
(Flag135-)
(Flag135prime-)
)
:effect
(and
(Flag213-)
(Flag213prime-)
)

)
(:action Flag213Action-9
:parameters ()
:precondition
(and
(Flag165-)
(Flag165prime-)
)
:effect
(and
(Flag213-)
(Flag213prime-)
)

)
(:action Flag213Action-10
:parameters ()
:precondition
(and
(Flag126-)
(Flag126prime-)
)
:effect
(and
(Flag213-)
(Flag213prime-)
)

)
(:action Flag213Action-11
:parameters ()
:precondition
(and
(Flag84-)
(Flag84prime-)
)
:effect
(and
(Flag213-)
(Flag213prime-)
)

)
(:action Flag213Action-12
:parameters ()
:precondition
(and
(Flag127-)
(Flag127prime-)
)
:effect
(and
(Flag213-)
(Flag213prime-)
)

)
(:action Flag213Action-13
:parameters ()
:precondition
(and
(Flag63-)
(Flag63prime-)
)
:effect
(and
(Flag213-)
(Flag213prime-)
)

)
(:action Flag213Action-14
:parameters ()
:precondition
(and
(Flag205-)
(Flag205prime-)
)
:effect
(and
(Flag213-)
(Flag213prime-)
)

)
(:action Flag213Action-15
:parameters ()
:precondition
(and
(Flag181-)
(Flag181prime-)
)
:effect
(and
(Flag213-)
(Flag213prime-)
)

)
(:action Flag213Action-16
:parameters ()
:precondition
(and
(Flag87-)
(Flag87prime-)
)
:effect
(and
(Flag213-)
(Flag213prime-)
)

)
(:action Flag213Action-17
:parameters ()
:precondition
(and
(Flag92-)
(Flag92prime-)
)
:effect
(and
(Flag213-)
(Flag213prime-)
)

)
(:action Flag213Action-18
:parameters ()
:precondition
(and
(Flag137-)
(Flag137prime-)
)
:effect
(and
(Flag213-)
(Flag213prime-)
)

)
(:action Flag213Action-19
:parameters ()
:precondition
(and
(Flag42-)
(Flag42prime-)
)
:effect
(and
(Flag213-)
(Flag213prime-)
)

)
(:action Flag213Action-20
:parameters ()
:precondition
(and
(Flag97-)
(Flag97prime-)
)
:effect
(and
(Flag213-)
(Flag213prime-)
)

)
(:action Flag213Action-21
:parameters ()
:precondition
(and
(Flag48-)
(Flag48prime-)
)
:effect
(and
(Flag213-)
(Flag213prime-)
)

)
(:action Flag213Action-22
:parameters ()
:precondition
(and
(Flag167-)
(Flag167prime-)
)
:effect
(and
(Flag213-)
(Flag213prime-)
)

)
(:action Flag213Action-23
:parameters ()
:precondition
(and
(Flag122-)
(Flag122prime-)
)
:effect
(and
(Flag213-)
(Flag213prime-)
)

)
(:action Flag213Action-24
:parameters ()
:precondition
(and
(Flag153-)
(Flag153prime-)
)
:effect
(and
(Flag213-)
(Flag213prime-)
)

)
(:action Flag213Action-25
:parameters ()
:precondition
(and
(Flag139-)
(Flag139prime-)
)
:effect
(and
(Flag213-)
(Flag213prime-)
)

)
(:action Flag213Action-26
:parameters ()
:precondition
(and
(Flag102-)
(Flag102prime-)
)
:effect
(and
(Flag213-)
(Flag213prime-)
)

)
(:action Flag213Action-27
:parameters ()
:precondition
(and
(Flag89-)
(Flag89prime-)
)
:effect
(and
(Flag213-)
(Flag213prime-)
)

)
(:action Flag213Action-28
:parameters ()
:precondition
(and
(Flag57-)
(Flag57prime-)
)
:effect
(and
(Flag213-)
(Flag213prime-)
)

)
(:action Flag213Action-29
:parameters ()
:precondition
(and
(Flag166-)
(Flag166prime-)
)
:effect
(and
(Flag213-)
(Flag213prime-)
)

)
(:action Flag213Action-30
:parameters ()
:precondition
(and
(Flag123-)
(Flag123prime-)
)
:effect
(and
(Flag213-)
(Flag213prime-)
)

)
(:action Flag213Action-31
:parameters ()
:precondition
(and
(Flag206-)
(Flag206prime-)
)
:effect
(and
(Flag213-)
(Flag213prime-)
)

)
(:action Flag213Action-32
:parameters ()
:precondition
(and
(Flag140-)
(Flag140prime-)
)
:effect
(and
(Flag213-)
(Flag213prime-)
)

)
(:action Flag213Action-33
:parameters ()
:precondition
(and
(Flag61-)
(Flag61prime-)
)
:effect
(and
(Flag213-)
(Flag213prime-)
)

)
(:action Flag213Action-34
:parameters ()
:precondition
(and
(Flag183-)
(Flag183prime-)
)
:effect
(and
(Flag213-)
(Flag213prime-)
)

)
(:action Flag213Action-35
:parameters ()
:precondition
(and
(Flag154-)
(Flag154prime-)
)
:effect
(and
(Flag213-)
(Flag213prime-)
)

)
(:action Flag213Action-36
:parameters ()
:precondition
(and
(Flag128-)
(Flag128prime-)
)
:effect
(and
(Flag213-)
(Flag213prime-)
)

)
(:action Flag213Action-37
:parameters ()
:precondition
(and
(Flag184-)
(Flag184prime-)
)
:effect
(and
(Flag213-)
(Flag213prime-)
)

)
(:action Flag213Action-38
:parameters ()
:precondition
(and
(Flag207-)
(Flag207prime-)
)
:effect
(and
(Flag213-)
(Flag213prime-)
)

)
(:action Flag213Action-39
:parameters ()
:precondition
(and
(Flag169-)
(Flag169prime-)
)
:effect
(and
(Flag213-)
(Flag213prime-)
)

)
(:action Flag213Action-40
:parameters ()
:precondition
(and
(Flag90-)
(Flag90prime-)
)
:effect
(and
(Flag213-)
(Flag213prime-)
)

)
(:action Flag213Action-41
:parameters ()
:precondition
(and
(Flag74-)
(Flag74prime-)
)
:effect
(and
(Flag213-)
(Flag213prime-)
)

)
(:action Flag213Action-42
:parameters ()
:precondition
(and
(Flag162-)
(Flag162prime-)
)
:effect
(and
(Flag213-)
(Flag213prime-)
)

)
(:action Flag213Action-43
:parameters ()
:precondition
(and
(Flag156-)
(Flag156prime-)
)
:effect
(and
(Flag213-)
(Flag213prime-)
)

)
(:action Flag213Action-44
:parameters ()
:precondition
(and
(Flag141-)
(Flag141prime-)
)
:effect
(and
(Flag213-)
(Flag213prime-)
)

)
(:action Flag213Action-45
:parameters ()
:precondition
(and
(Flag194-)
(Flag194prime-)
)
:effect
(and
(Flag213-)
(Flag213prime-)
)

)
(:action Flag213Action-46
:parameters ()
:precondition
(and
(Flag121-)
(Flag121prime-)
)
:effect
(and
(Flag213-)
(Flag213prime-)
)

)
(:action Flag213Action-47
:parameters ()
:precondition
(and
(Flag111-)
(Flag111prime-)
)
:effect
(and
(Flag213-)
(Flag213prime-)
)

)
(:action Flag213Action-48
:parameters ()
:precondition
(and
(Flag73-)
(Flag73prime-)
)
:effect
(and
(Flag213-)
(Flag213prime-)
)

)
(:action Flag213Action-49
:parameters ()
:precondition
(and
(Flag143-)
(Flag143prime-)
)
:effect
(and
(Flag213-)
(Flag213prime-)
)

)
(:action Flag213Action-50
:parameters ()
:precondition
(and
(Flag157-)
(Flag157prime-)
)
:effect
(and
(Flag213-)
(Flag213prime-)
)

)
(:action Flag213Action-51
:parameters ()
:precondition
(and
(Flag208-)
(Flag208prime-)
)
:effect
(and
(Flag213-)
(Flag213prime-)
)

)
(:action Flag213Action-52
:parameters ()
:precondition
(and
(Flag116-)
(Flag116prime-)
)
:effect
(and
(Flag213-)
(Flag213prime-)
)

)
(:action Flag213Action-53
:parameters ()
:precondition
(and
(Flag195-)
(Flag195prime-)
)
:effect
(and
(Flag213-)
(Flag213prime-)
)

)
(:action Flag213Action-54
:parameters ()
:precondition
(and
(Flag79-)
(Flag79prime-)
)
:effect
(and
(Flag213-)
(Flag213prime-)
)

)
(:action Flag213Action-55
:parameters ()
:precondition
(and
(Flag185-)
(Flag185prime-)
)
:effect
(and
(Flag213-)
(Flag213prime-)
)

)
(:action Flag213Action-56
:parameters ()
:precondition
(and
(Flag118-)
(Flag118prime-)
)
:effect
(and
(Flag213-)
(Flag213prime-)
)

)
(:action Flag213Action-57
:parameters ()
:precondition
(and
(Flag196-)
(Flag196prime-)
)
:effect
(and
(Flag213-)
(Flag213prime-)
)

)
(:action Flag213Action-58
:parameters ()
:precondition
(and
(Flag172-)
(Flag172prime-)
)
:effect
(and
(Flag213-)
(Flag213prime-)
)

)
(:action Flag213Action-59
:parameters ()
:precondition
(and
(Flag108-)
(Flag108prime-)
)
:effect
(and
(Flag213-)
(Flag213prime-)
)

)
(:action Flag213Action-60
:parameters ()
:precondition
(and
(Flag173-)
(Flag173prime-)
)
:effect
(and
(Flag213-)
(Flag213prime-)
)

)
(:action Flag213Action-61
:parameters ()
:precondition
(and
(Flag130-)
(Flag130prime-)
)
:effect
(and
(Flag213-)
(Flag213prime-)
)

)
(:action Flag213Action-62
:parameters ()
:precondition
(and
(Flag160-)
(Flag160prime-)
)
:effect
(and
(Flag213-)
(Flag213prime-)
)

)
(:action Flag213Action-63
:parameters ()
:precondition
(and
(Flag101-)
(Flag101prime-)
)
:effect
(and
(Flag213-)
(Flag213prime-)
)

)
(:action Flag213Action-64
:parameters ()
:precondition
(and
(Flag83-)
(Flag83prime-)
)
:effect
(and
(Flag213-)
(Flag213prime-)
)

)
(:action Flag213Action-65
:parameters ()
:precondition
(and
(Flag186-)
(Flag186prime-)
)
:effect
(and
(Flag213-)
(Flag213prime-)
)

)
(:action Flag213Action-66
:parameters ()
:precondition
(and
(Flag174-)
(Flag174prime-)
)
:effect
(and
(Flag213-)
(Flag213prime-)
)

)
(:action Flag213Action-67
:parameters ()
:precondition
(and
(Flag66-)
(Flag66prime-)
)
:effect
(and
(Flag213-)
(Flag213prime-)
)

)
(:action Flag213Action-68
:parameters ()
:precondition
(and
(Flag158-)
(Flag158prime-)
)
:effect
(and
(Flag213-)
(Flag213prime-)
)

)
(:action Flag213Action-69
:parameters ()
:precondition
(and
(Flag187-)
(Flag187prime-)
)
:effect
(and
(Flag213-)
(Flag213prime-)
)

)
(:action Flag213Action-70
:parameters ()
:precondition
(and
(Flag67-)
(Flag67prime-)
)
:effect
(and
(Flag213-)
(Flag213prime-)
)

)
(:action Flag213Action-71
:parameters ()
:precondition
(and
(Flag144-)
(Flag144prime-)
)
:effect
(and
(Flag213-)
(Flag213prime-)
)

)
(:action Flag213Action-72
:parameters ()
:precondition
(and
(Flag175-)
(Flag175prime-)
)
:effect
(and
(Flag213-)
(Flag213prime-)
)

)
(:action Flag213Action-73
:parameters ()
:precondition
(and
(Flag209-)
(Flag209prime-)
)
:effect
(and
(Flag213-)
(Flag213prime-)
)

)
(:action Flag213Action-74
:parameters ()
:precondition
(and
(Flag176-)
(Flag176prime-)
)
:effect
(and
(Flag213-)
(Flag213prime-)
)

)
(:action Flag213Action-75
:parameters ()
:precondition
(and
(Flag132-)
(Flag132prime-)
)
:effect
(and
(Flag213-)
(Flag213prime-)
)

)
(:action Flag213Action-76
:parameters ()
:precondition
(and
(Flag40-)
(Flag40prime-)
)
:effect
(and
(Flag213-)
(Flag213prime-)
)

)
(:action Flag213Action-77
:parameters ()
:precondition
(and
(Flag96-)
(Flag96prime-)
)
:effect
(and
(Flag213-)
(Flag213prime-)
)

)
(:action Flag213Action-78
:parameters ()
:precondition
(and
(Flag146-)
(Flag146prime-)
)
:effect
(and
(Flag213-)
(Flag213prime-)
)

)
(:action Flag213Action-79
:parameters ()
:precondition
(and
(Flag46-)
(Flag46prime-)
)
:effect
(and
(Flag213-)
(Flag213prime-)
)

)
(:action Flag213Action-80
:parameters ()
:precondition
(and
(Flag198-)
(Flag198prime-)
)
:effect
(and
(Flag213-)
(Flag213prime-)
)

)
(:action Flag213Action-81
:parameters ()
:precondition
(and
(Flag199-)
(Flag199prime-)
)
:effect
(and
(Flag213-)
(Flag213prime-)
)

)
(:action Flag213Action-82
:parameters ()
:precondition
(and
(Flag188-)
(Flag188prime-)
)
:effect
(and
(Flag213-)
(Flag213prime-)
)

)
(:action Flag213Action-83
:parameters ()
:precondition
(and
(Flag98-)
(Flag98prime-)
)
:effect
(and
(Flag213-)
(Flag213prime-)
)

)
(:action Flag213Action-84
:parameters ()
:precondition
(and
(Flag60-)
(Flag60prime-)
)
:effect
(and
(Flag213-)
(Flag213prime-)
)

)
(:action Flag213Action-85
:parameters ()
:precondition
(and
(Flag119-)
(Flag119prime-)
)
:effect
(and
(Flag213-)
(Flag213prime-)
)

)
(:action Flag213Action-86
:parameters ()
:precondition
(and
(Flag210-)
(Flag210prime-)
)
:effect
(and
(Flag213-)
(Flag213prime-)
)

)
(:action Flag213Action-87
:parameters ()
:precondition
(and
(Flag53-)
(Flag53prime-)
)
:effect
(and
(Flag213-)
(Flag213prime-)
)

)
(:action Flag213Action-88
:parameters ()
:precondition
(and
(Flag55-)
(Flag55prime-)
)
:effect
(and
(Flag213-)
(Flag213prime-)
)

)
(:action Flag213Action-89
:parameters ()
:precondition
(and
(Flag81-)
(Flag81prime-)
)
:effect
(and
(Flag213-)
(Flag213prime-)
)

)
(:action Flag213Action-90
:parameters ()
:precondition
(and
(Flag161-)
(Flag161prime-)
)
:effect
(and
(Flag213-)
(Flag213prime-)
)

)
(:action Flag213Action-91
:parameters ()
:precondition
(and
(Flag193-)
(Flag193prime-)
)
:effect
(and
(Flag213-)
(Flag213prime-)
)

)
(:action Flag213Action-92
:parameters ()
:precondition
(and
(Flag200-)
(Flag200prime-)
)
:effect
(and
(Flag213-)
(Flag213prime-)
)

)
(:action Flag213Action-93
:parameters ()
:precondition
(and
(Flag58-)
(Flag58prime-)
)
:effect
(and
(Flag213-)
(Flag213prime-)
)

)
(:action Flag213Action-94
:parameters ()
:precondition
(and
(Flag189-)
(Flag189prime-)
)
:effect
(and
(Flag213-)
(Flag213prime-)
)

)
(:action Flag213Action-95
:parameters ()
:precondition
(and
(Flag105-)
(Flag105prime-)
)
:effect
(and
(Flag213-)
(Flag213prime-)
)

)
(:action Flag213Action-96
:parameters ()
:precondition
(and
(Flag113-)
(Flag113prime-)
)
:effect
(and
(Flag213-)
(Flag213prime-)
)

)
(:action Flag213Action-97
:parameters ()
:precondition
(and
(Flag91-)
(Flag91prime-)
)
:effect
(and
(Flag213-)
(Flag213prime-)
)

)
(:action Flag213Action-98
:parameters ()
:precondition
(and
(Flag68-)
(Flag68prime-)
)
:effect
(and
(Flag213-)
(Flag213prime-)
)

)
(:action Flag213Action-99
:parameters ()
:precondition
(and
(Flag201-)
(Flag201prime-)
)
:effect
(and
(Flag213-)
(Flag213prime-)
)

)
(:action Flag213Action-100
:parameters ()
:precondition
(and
(Flag163-)
(Flag163prime-)
)
:effect
(and
(Flag213-)
(Flag213prime-)
)

)
(:action Flag213Action-101
:parameters ()
:precondition
(and
(Flag211-)
(Flag211prime-)
)
:effect
(and
(Flag213-)
(Flag213prime-)
)

)
(:action Flag213Action-102
:parameters ()
:precondition
(and
(Flag212-)
(Flag212prime-)
)
:effect
(and
(Flag213-)
(Flag213prime-)
)

)
(:action Flag213Action-103
:parameters ()
:precondition
(and
(Flag190-)
(Flag190prime-)
)
:effect
(and
(Flag213-)
(Flag213prime-)
)

)
(:action Prim213Action
:parameters ()
:precondition
(and
(not (Flag41-))
(Flag41prime-)
(not (Flag47-))
(Flag47prime-)
(not (Flag203-))
(Flag203prime-)
(not (Flag192-))
(Flag192prime-)
(not (Flag179-))
(Flag179prime-)
(not (Flag204-))
(Flag204prime-)
(not (Flag134-))
(Flag134prime-)
(not (Flag150-))
(Flag150prime-)
(not (Flag135-))
(Flag135prime-)
(not (Flag165-))
(Flag165prime-)
(not (Flag126-))
(Flag126prime-)
(not (Flag84-))
(Flag84prime-)
(not (Flag127-))
(Flag127prime-)
(not (Flag63-))
(Flag63prime-)
(not (Flag205-))
(Flag205prime-)
(not (Flag181-))
(Flag181prime-)
(not (Flag87-))
(Flag87prime-)
(not (Flag92-))
(Flag92prime-)
(not (Flag137-))
(Flag137prime-)
(not (Flag42-))
(Flag42prime-)
(not (Flag97-))
(Flag97prime-)
(not (Flag48-))
(Flag48prime-)
(not (Flag167-))
(Flag167prime-)
(not (Flag122-))
(Flag122prime-)
(not (Flag153-))
(Flag153prime-)
(not (Flag139-))
(Flag139prime-)
(not (Flag102-))
(Flag102prime-)
(not (Flag89-))
(Flag89prime-)
(not (Flag57-))
(Flag57prime-)
(not (Flag166-))
(Flag166prime-)
(not (Flag123-))
(Flag123prime-)
(not (Flag206-))
(Flag206prime-)
(not (Flag140-))
(Flag140prime-)
(not (Flag61-))
(Flag61prime-)
(not (Flag183-))
(Flag183prime-)
(not (Flag154-))
(Flag154prime-)
(not (Flag128-))
(Flag128prime-)
(not (Flag184-))
(Flag184prime-)
(not (Flag207-))
(Flag207prime-)
(not (Flag169-))
(Flag169prime-)
(not (Flag90-))
(Flag90prime-)
(not (Flag74-))
(Flag74prime-)
(not (Flag162-))
(Flag162prime-)
(not (Flag156-))
(Flag156prime-)
(not (Flag141-))
(Flag141prime-)
(not (Flag194-))
(Flag194prime-)
(not (Flag121-))
(Flag121prime-)
(not (Flag111-))
(Flag111prime-)
(not (Flag73-))
(Flag73prime-)
(not (Flag143-))
(Flag143prime-)
(not (Flag157-))
(Flag157prime-)
(not (Flag208-))
(Flag208prime-)
(not (Flag116-))
(Flag116prime-)
(not (Flag195-))
(Flag195prime-)
(not (Flag79-))
(Flag79prime-)
(not (Flag185-))
(Flag185prime-)
(not (Flag118-))
(Flag118prime-)
(not (Flag196-))
(Flag196prime-)
(not (Flag172-))
(Flag172prime-)
(not (Flag108-))
(Flag108prime-)
(not (Flag173-))
(Flag173prime-)
(not (Flag130-))
(Flag130prime-)
(not (Flag160-))
(Flag160prime-)
(not (Flag101-))
(Flag101prime-)
(not (Flag83-))
(Flag83prime-)
(not (Flag186-))
(Flag186prime-)
(not (Flag174-))
(Flag174prime-)
(not (Flag66-))
(Flag66prime-)
(not (Flag158-))
(Flag158prime-)
(not (Flag187-))
(Flag187prime-)
(not (Flag67-))
(Flag67prime-)
(not (Flag144-))
(Flag144prime-)
(not (Flag175-))
(Flag175prime-)
(not (Flag209-))
(Flag209prime-)
(not (Flag176-))
(Flag176prime-)
(not (Flag132-))
(Flag132prime-)
(not (Flag40-))
(Flag40prime-)
(not (Flag96-))
(Flag96prime-)
(not (Flag146-))
(Flag146prime-)
(not (Flag46-))
(Flag46prime-)
(not (Flag198-))
(Flag198prime-)
(not (Flag199-))
(Flag199prime-)
(not (Flag188-))
(Flag188prime-)
(not (Flag98-))
(Flag98prime-)
(not (Flag60-))
(Flag60prime-)
(not (Flag119-))
(Flag119prime-)
(not (Flag210-))
(Flag210prime-)
(not (Flag53-))
(Flag53prime-)
(not (Flag55-))
(Flag55prime-)
(not (Flag81-))
(Flag81prime-)
(not (Flag161-))
(Flag161prime-)
(not (Flag193-))
(Flag193prime-)
(not (Flag200-))
(Flag200prime-)
(not (Flag58-))
(Flag58prime-)
(not (Flag189-))
(Flag189prime-)
(not (Flag105-))
(Flag105prime-)
(not (Flag113-))
(Flag113prime-)
(not (Flag91-))
(Flag91prime-)
(not (Flag68-))
(Flag68prime-)
(not (Flag201-))
(Flag201prime-)
(not (Flag163-))
(Flag163prime-)
(not (Flag211-))
(Flag211prime-)
(not (Flag212-))
(Flag212prime-)
(not (Flag190-))
(Flag190prime-)
)
:effect
(and
(Flag213prime-)
(not (Flag213-))
)

)
(:action Flag222Action-0
:parameters ()
:precondition
(and
(Flag41-)
(Flag41prime-)
)
:effect
(and
(Flag222-)
(Flag222prime-)
)

)
(:action Flag222Action-1
:parameters ()
:precondition
(and
(Flag47-)
(Flag47prime-)
)
:effect
(and
(Flag222-)
(Flag222prime-)
)

)
(:action Flag222Action-2
:parameters ()
:precondition
(and
(Flag203-)
(Flag203prime-)
)
:effect
(and
(Flag222-)
(Flag222prime-)
)

)
(:action Flag222Action-3
:parameters ()
:precondition
(and
(Flag192-)
(Flag192prime-)
)
:effect
(and
(Flag222-)
(Flag222prime-)
)

)
(:action Flag222Action-4
:parameters ()
:precondition
(and
(Flag179-)
(Flag179prime-)
)
:effect
(and
(Flag222-)
(Flag222prime-)
)

)
(:action Flag222Action-5
:parameters ()
:precondition
(and
(Flag204-)
(Flag204prime-)
)
:effect
(and
(Flag222-)
(Flag222prime-)
)

)
(:action Flag222Action-6
:parameters ()
:precondition
(and
(Flag134-)
(Flag134prime-)
)
:effect
(and
(Flag222-)
(Flag222prime-)
)

)
(:action Flag222Action-7
:parameters ()
:precondition
(and
(Flag150-)
(Flag150prime-)
)
:effect
(and
(Flag222-)
(Flag222prime-)
)

)
(:action Flag222Action-8
:parameters ()
:precondition
(and
(Flag135-)
(Flag135prime-)
)
:effect
(and
(Flag222-)
(Flag222prime-)
)

)
(:action Flag222Action-9
:parameters ()
:precondition
(and
(Flag214-)
(Flag214prime-)
)
:effect
(and
(Flag222-)
(Flag222prime-)
)

)
(:action Flag222Action-10
:parameters ()
:precondition
(and
(Flag215-)
(Flag215prime-)
)
:effect
(and
(Flag222-)
(Flag222prime-)
)

)
(:action Flag222Action-11
:parameters ()
:precondition
(and
(Flag119-)
(Flag119prime-)
)
:effect
(and
(Flag222-)
(Flag222prime-)
)

)
(:action Flag222Action-12
:parameters ()
:precondition
(and
(Flag195-)
(Flag195prime-)
)
:effect
(and
(Flag222-)
(Flag222prime-)
)

)
(:action Flag222Action-13
:parameters ()
:precondition
(and
(Flag63-)
(Flag63prime-)
)
:effect
(and
(Flag222-)
(Flag222prime-)
)

)
(:action Flag222Action-14
:parameters ()
:precondition
(and
(Flag205-)
(Flag205prime-)
)
:effect
(and
(Flag222-)
(Flag222prime-)
)

)
(:action Flag222Action-15
:parameters ()
:precondition
(and
(Flag181-)
(Flag181prime-)
)
:effect
(and
(Flag222-)
(Flag222prime-)
)

)
(:action Flag222Action-16
:parameters ()
:precondition
(and
(Flag166-)
(Flag166prime-)
)
:effect
(and
(Flag222-)
(Flag222prime-)
)

)
(:action Flag222Action-17
:parameters ()
:precondition
(and
(Flag92-)
(Flag92prime-)
)
:effect
(and
(Flag222-)
(Flag222prime-)
)

)
(:action Flag222Action-18
:parameters ()
:precondition
(and
(Flag137-)
(Flag137prime-)
)
:effect
(and
(Flag222-)
(Flag222prime-)
)

)
(:action Flag222Action-19
:parameters ()
:precondition
(and
(Flag216-)
(Flag216prime-)
)
:effect
(and
(Flag222-)
(Flag222prime-)
)

)
(:action Flag222Action-20
:parameters ()
:precondition
(and
(Flag42-)
(Flag42prime-)
)
:effect
(and
(Flag222-)
(Flag222prime-)
)

)
(:action Flag222Action-21
:parameters ()
:precondition
(and
(Flag97-)
(Flag97prime-)
)
:effect
(and
(Flag222-)
(Flag222prime-)
)

)
(:action Flag222Action-22
:parameters ()
:precondition
(and
(Flag167-)
(Flag167prime-)
)
:effect
(and
(Flag222-)
(Flag222prime-)
)

)
(:action Flag222Action-23
:parameters ()
:precondition
(and
(Flag122-)
(Flag122prime-)
)
:effect
(and
(Flag222-)
(Flag222prime-)
)

)
(:action Flag222Action-24
:parameters ()
:precondition
(and
(Flag153-)
(Flag153prime-)
)
:effect
(and
(Flag222-)
(Flag222prime-)
)

)
(:action Flag222Action-25
:parameters ()
:precondition
(and
(Flag139-)
(Flag139prime-)
)
:effect
(and
(Flag222-)
(Flag222prime-)
)

)
(:action Flag222Action-26
:parameters ()
:precondition
(and
(Flag102-)
(Flag102prime-)
)
:effect
(and
(Flag222-)
(Flag222prime-)
)

)
(:action Flag222Action-27
:parameters ()
:precondition
(and
(Flag89-)
(Flag89prime-)
)
:effect
(and
(Flag222-)
(Flag222prime-)
)

)
(:action Flag222Action-28
:parameters ()
:precondition
(and
(Flag57-)
(Flag57prime-)
)
:effect
(and
(Flag222-)
(Flag222prime-)
)

)
(:action Flag222Action-29
:parameters ()
:precondition
(and
(Flag87-)
(Flag87prime-)
)
:effect
(and
(Flag222-)
(Flag222prime-)
)

)
(:action Flag222Action-30
:parameters ()
:precondition
(and
(Flag123-)
(Flag123prime-)
)
:effect
(and
(Flag222-)
(Flag222prime-)
)

)
(:action Flag222Action-31
:parameters ()
:precondition
(and
(Flag206-)
(Flag206prime-)
)
:effect
(and
(Flag222-)
(Flag222prime-)
)

)
(:action Flag222Action-32
:parameters ()
:precondition
(and
(Flag140-)
(Flag140prime-)
)
:effect
(and
(Flag222-)
(Flag222prime-)
)

)
(:action Flag222Action-33
:parameters ()
:precondition
(and
(Flag61-)
(Flag61prime-)
)
:effect
(and
(Flag222-)
(Flag222prime-)
)

)
(:action Flag222Action-34
:parameters ()
:precondition
(and
(Flag183-)
(Flag183prime-)
)
:effect
(and
(Flag222-)
(Flag222prime-)
)

)
(:action Flag222Action-35
:parameters ()
:precondition
(and
(Flag154-)
(Flag154prime-)
)
:effect
(and
(Flag222-)
(Flag222prime-)
)

)
(:action Flag222Action-36
:parameters ()
:precondition
(and
(Flag128-)
(Flag128prime-)
)
:effect
(and
(Flag222-)
(Flag222prime-)
)

)
(:action Flag222Action-37
:parameters ()
:precondition
(and
(Flag184-)
(Flag184prime-)
)
:effect
(and
(Flag222-)
(Flag222prime-)
)

)
(:action Flag222Action-38
:parameters ()
:precondition
(and
(Flag207-)
(Flag207prime-)
)
:effect
(and
(Flag222-)
(Flag222prime-)
)

)
(:action Flag222Action-39
:parameters ()
:precondition
(and
(Flag169-)
(Flag169prime-)
)
:effect
(and
(Flag222-)
(Flag222prime-)
)

)
(:action Flag222Action-40
:parameters ()
:precondition
(and
(Flag90-)
(Flag90prime-)
)
:effect
(and
(Flag222-)
(Flag222prime-)
)

)
(:action Flag222Action-41
:parameters ()
:precondition
(and
(Flag74-)
(Flag74prime-)
)
:effect
(and
(Flag222-)
(Flag222prime-)
)

)
(:action Flag222Action-42
:parameters ()
:precondition
(and
(Flag162-)
(Flag162prime-)
)
:effect
(and
(Flag222-)
(Flag222prime-)
)

)
(:action Flag222Action-43
:parameters ()
:precondition
(and
(Flag156-)
(Flag156prime-)
)
:effect
(and
(Flag222-)
(Flag222prime-)
)

)
(:action Flag222Action-44
:parameters ()
:precondition
(and
(Flag194-)
(Flag194prime-)
)
:effect
(and
(Flag222-)
(Flag222prime-)
)

)
(:action Flag222Action-45
:parameters ()
:precondition
(and
(Flag121-)
(Flag121prime-)
)
:effect
(and
(Flag222-)
(Flag222prime-)
)

)
(:action Flag222Action-46
:parameters ()
:precondition
(and
(Flag111-)
(Flag111prime-)
)
:effect
(and
(Flag222-)
(Flag222prime-)
)

)
(:action Flag222Action-47
:parameters ()
:precondition
(and
(Flag73-)
(Flag73prime-)
)
:effect
(and
(Flag222-)
(Flag222prime-)
)

)
(:action Flag222Action-48
:parameters ()
:precondition
(and
(Flag143-)
(Flag143prime-)
)
:effect
(and
(Flag222-)
(Flag222prime-)
)

)
(:action Flag222Action-49
:parameters ()
:precondition
(and
(Flag157-)
(Flag157prime-)
)
:effect
(and
(Flag222-)
(Flag222prime-)
)

)
(:action Flag222Action-50
:parameters ()
:precondition
(and
(Flag208-)
(Flag208prime-)
)
:effect
(and
(Flag222-)
(Flag222prime-)
)

)
(:action Flag222Action-51
:parameters ()
:precondition
(and
(Flag116-)
(Flag116prime-)
)
:effect
(and
(Flag222-)
(Flag222prime-)
)

)
(:action Flag222Action-52
:parameters ()
:precondition
(and
(Flag158-)
(Flag158prime-)
)
:effect
(and
(Flag222-)
(Flag222prime-)
)

)
(:action Flag222Action-53
:parameters ()
:precondition
(and
(Flag79-)
(Flag79prime-)
)
:effect
(and
(Flag222-)
(Flag222prime-)
)

)
(:action Flag222Action-54
:parameters ()
:precondition
(and
(Flag185-)
(Flag185prime-)
)
:effect
(and
(Flag222-)
(Flag222prime-)
)

)
(:action Flag222Action-55
:parameters ()
:precondition
(and
(Flag217-)
(Flag217prime-)
)
:effect
(and
(Flag222-)
(Flag222prime-)
)

)
(:action Flag222Action-56
:parameters ()
:precondition
(and
(Flag118-)
(Flag118prime-)
)
:effect
(and
(Flag222-)
(Flag222prime-)
)

)
(:action Flag222Action-57
:parameters ()
:precondition
(and
(Flag196-)
(Flag196prime-)
)
:effect
(and
(Flag222-)
(Flag222prime-)
)

)
(:action Flag222Action-58
:parameters ()
:precondition
(and
(Flag172-)
(Flag172prime-)
)
:effect
(and
(Flag222-)
(Flag222prime-)
)

)
(:action Flag222Action-59
:parameters ()
:precondition
(and
(Flag218-)
(Flag218prime-)
)
:effect
(and
(Flag222-)
(Flag222prime-)
)

)
(:action Flag222Action-60
:parameters ()
:precondition
(and
(Flag108-)
(Flag108prime-)
)
:effect
(and
(Flag222-)
(Flag222prime-)
)

)
(:action Flag222Action-61
:parameters ()
:precondition
(and
(Flag173-)
(Flag173prime-)
)
:effect
(and
(Flag222-)
(Flag222prime-)
)

)
(:action Flag222Action-62
:parameters ()
:precondition
(and
(Flag130-)
(Flag130prime-)
)
:effect
(and
(Flag222-)
(Flag222prime-)
)

)
(:action Flag222Action-63
:parameters ()
:precondition
(and
(Flag160-)
(Flag160prime-)
)
:effect
(and
(Flag222-)
(Flag222prime-)
)

)
(:action Flag222Action-64
:parameters ()
:precondition
(and
(Flag101-)
(Flag101prime-)
)
:effect
(and
(Flag222-)
(Flag222prime-)
)

)
(:action Flag222Action-65
:parameters ()
:precondition
(and
(Flag83-)
(Flag83prime-)
)
:effect
(and
(Flag222-)
(Flag222prime-)
)

)
(:action Flag222Action-66
:parameters ()
:precondition
(and
(Flag219-)
(Flag219prime-)
)
:effect
(and
(Flag222-)
(Flag222prime-)
)

)
(:action Flag222Action-67
:parameters ()
:precondition
(and
(Flag174-)
(Flag174prime-)
)
:effect
(and
(Flag222-)
(Flag222prime-)
)

)
(:action Flag222Action-68
:parameters ()
:precondition
(and
(Flag66-)
(Flag66prime-)
)
:effect
(and
(Flag222-)
(Flag222prime-)
)

)
(:action Flag222Action-69
:parameters ()
:precondition
(and
(Flag187-)
(Flag187prime-)
)
:effect
(and
(Flag222-)
(Flag222prime-)
)

)
(:action Flag222Action-70
:parameters ()
:precondition
(and
(Flag67-)
(Flag67prime-)
)
:effect
(and
(Flag222-)
(Flag222prime-)
)

)
(:action Flag222Action-71
:parameters ()
:precondition
(and
(Flag144-)
(Flag144prime-)
)
:effect
(and
(Flag222-)
(Flag222prime-)
)

)
(:action Flag222Action-72
:parameters ()
:precondition
(and
(Flag175-)
(Flag175prime-)
)
:effect
(and
(Flag222-)
(Flag222prime-)
)

)
(:action Flag222Action-73
:parameters ()
:precondition
(and
(Flag209-)
(Flag209prime-)
)
:effect
(and
(Flag222-)
(Flag222prime-)
)

)
(:action Flag222Action-74
:parameters ()
:precondition
(and
(Flag176-)
(Flag176prime-)
)
:effect
(and
(Flag222-)
(Flag222prime-)
)

)
(:action Flag222Action-75
:parameters ()
:precondition
(and
(Flag132-)
(Flag132prime-)
)
:effect
(and
(Flag222-)
(Flag222prime-)
)

)
(:action Flag222Action-76
:parameters ()
:precondition
(and
(Flag40-)
(Flag40prime-)
)
:effect
(and
(Flag222-)
(Flag222prime-)
)

)
(:action Flag222Action-77
:parameters ()
:precondition
(and
(Flag96-)
(Flag96prime-)
)
:effect
(and
(Flag222-)
(Flag222prime-)
)

)
(:action Flag222Action-78
:parameters ()
:precondition
(and
(Flag146-)
(Flag146prime-)
)
:effect
(and
(Flag222-)
(Flag222prime-)
)

)
(:action Flag222Action-79
:parameters ()
:precondition
(and
(Flag46-)
(Flag46prime-)
)
:effect
(and
(Flag222-)
(Flag222prime-)
)

)
(:action Flag222Action-80
:parameters ()
:precondition
(and
(Flag198-)
(Flag198prime-)
)
:effect
(and
(Flag222-)
(Flag222prime-)
)

)
(:action Flag222Action-81
:parameters ()
:precondition
(and
(Flag188-)
(Flag188prime-)
)
:effect
(and
(Flag222-)
(Flag222prime-)
)

)
(:action Flag222Action-82
:parameters ()
:precondition
(and
(Flag98-)
(Flag98prime-)
)
:effect
(and
(Flag222-)
(Flag222prime-)
)

)
(:action Flag222Action-83
:parameters ()
:precondition
(and
(Flag210-)
(Flag210prime-)
)
:effect
(and
(Flag222-)
(Flag222prime-)
)

)
(:action Flag222Action-84
:parameters ()
:precondition
(and
(Flag53-)
(Flag53prime-)
)
:effect
(and
(Flag222-)
(Flag222prime-)
)

)
(:action Flag222Action-85
:parameters ()
:precondition
(and
(Flag55-)
(Flag55prime-)
)
:effect
(and
(Flag222-)
(Flag222prime-)
)

)
(:action Flag222Action-86
:parameters ()
:precondition
(and
(Flag220-)
(Flag220prime-)
)
:effect
(and
(Flag222-)
(Flag222prime-)
)

)
(:action Flag222Action-87
:parameters ()
:precondition
(and
(Flag81-)
(Flag81prime-)
)
:effect
(and
(Flag222-)
(Flag222prime-)
)

)
(:action Flag222Action-88
:parameters ()
:precondition
(and
(Flag161-)
(Flag161prime-)
)
:effect
(and
(Flag222-)
(Flag222prime-)
)

)
(:action Flag222Action-89
:parameters ()
:precondition
(and
(Flag193-)
(Flag193prime-)
)
:effect
(and
(Flag222-)
(Flag222prime-)
)

)
(:action Flag222Action-90
:parameters ()
:precondition
(and
(Flag200-)
(Flag200prime-)
)
:effect
(and
(Flag222-)
(Flag222prime-)
)

)
(:action Flag222Action-91
:parameters ()
:precondition
(and
(Flag58-)
(Flag58prime-)
)
:effect
(and
(Flag222-)
(Flag222prime-)
)

)
(:action Flag222Action-92
:parameters ()
:precondition
(and
(Flag189-)
(Flag189prime-)
)
:effect
(and
(Flag222-)
(Flag222prime-)
)

)
(:action Flag222Action-93
:parameters ()
:precondition
(and
(Flag105-)
(Flag105prime-)
)
:effect
(and
(Flag222-)
(Flag222prime-)
)

)
(:action Flag222Action-94
:parameters ()
:precondition
(and
(Flag113-)
(Flag113prime-)
)
:effect
(and
(Flag222-)
(Flag222prime-)
)

)
(:action Flag222Action-95
:parameters ()
:precondition
(and
(Flag91-)
(Flag91prime-)
)
:effect
(and
(Flag222-)
(Flag222prime-)
)

)
(:action Flag222Action-96
:parameters ()
:precondition
(and
(Flag221-)
(Flag221prime-)
)
:effect
(and
(Flag222-)
(Flag222prime-)
)

)
(:action Flag222Action-97
:parameters ()
:precondition
(and
(Flag68-)
(Flag68prime-)
)
:effect
(and
(Flag222-)
(Flag222prime-)
)

)
(:action Flag222Action-98
:parameters ()
:precondition
(and
(Flag201-)
(Flag201prime-)
)
:effect
(and
(Flag222-)
(Flag222prime-)
)

)
(:action Flag222Action-99
:parameters ()
:precondition
(and
(Flag211-)
(Flag211prime-)
)
:effect
(and
(Flag222-)
(Flag222prime-)
)

)
(:action Flag222Action-100
:parameters ()
:precondition
(and
(Flag190-)
(Flag190prime-)
)
:effect
(and
(Flag222-)
(Flag222prime-)
)

)
(:action Prim222Action
:parameters ()
:precondition
(and
(not (Flag41-))
(Flag41prime-)
(not (Flag47-))
(Flag47prime-)
(not (Flag203-))
(Flag203prime-)
(not (Flag192-))
(Flag192prime-)
(not (Flag179-))
(Flag179prime-)
(not (Flag204-))
(Flag204prime-)
(not (Flag134-))
(Flag134prime-)
(not (Flag150-))
(Flag150prime-)
(not (Flag135-))
(Flag135prime-)
(not (Flag214-))
(Flag214prime-)
(not (Flag215-))
(Flag215prime-)
(not (Flag119-))
(Flag119prime-)
(not (Flag195-))
(Flag195prime-)
(not (Flag63-))
(Flag63prime-)
(not (Flag205-))
(Flag205prime-)
(not (Flag181-))
(Flag181prime-)
(not (Flag166-))
(Flag166prime-)
(not (Flag92-))
(Flag92prime-)
(not (Flag137-))
(Flag137prime-)
(not (Flag216-))
(Flag216prime-)
(not (Flag42-))
(Flag42prime-)
(not (Flag97-))
(Flag97prime-)
(not (Flag167-))
(Flag167prime-)
(not (Flag122-))
(Flag122prime-)
(not (Flag153-))
(Flag153prime-)
(not (Flag139-))
(Flag139prime-)
(not (Flag102-))
(Flag102prime-)
(not (Flag89-))
(Flag89prime-)
(not (Flag57-))
(Flag57prime-)
(not (Flag87-))
(Flag87prime-)
(not (Flag123-))
(Flag123prime-)
(not (Flag206-))
(Flag206prime-)
(not (Flag140-))
(Flag140prime-)
(not (Flag61-))
(Flag61prime-)
(not (Flag183-))
(Flag183prime-)
(not (Flag154-))
(Flag154prime-)
(not (Flag128-))
(Flag128prime-)
(not (Flag184-))
(Flag184prime-)
(not (Flag207-))
(Flag207prime-)
(not (Flag169-))
(Flag169prime-)
(not (Flag90-))
(Flag90prime-)
(not (Flag74-))
(Flag74prime-)
(not (Flag162-))
(Flag162prime-)
(not (Flag156-))
(Flag156prime-)
(not (Flag194-))
(Flag194prime-)
(not (Flag121-))
(Flag121prime-)
(not (Flag111-))
(Flag111prime-)
(not (Flag73-))
(Flag73prime-)
(not (Flag143-))
(Flag143prime-)
(not (Flag157-))
(Flag157prime-)
(not (Flag208-))
(Flag208prime-)
(not (Flag116-))
(Flag116prime-)
(not (Flag158-))
(Flag158prime-)
(not (Flag79-))
(Flag79prime-)
(not (Flag185-))
(Flag185prime-)
(not (Flag217-))
(Flag217prime-)
(not (Flag118-))
(Flag118prime-)
(not (Flag196-))
(Flag196prime-)
(not (Flag172-))
(Flag172prime-)
(not (Flag218-))
(Flag218prime-)
(not (Flag108-))
(Flag108prime-)
(not (Flag173-))
(Flag173prime-)
(not (Flag130-))
(Flag130prime-)
(not (Flag160-))
(Flag160prime-)
(not (Flag101-))
(Flag101prime-)
(not (Flag83-))
(Flag83prime-)
(not (Flag219-))
(Flag219prime-)
(not (Flag174-))
(Flag174prime-)
(not (Flag66-))
(Flag66prime-)
(not (Flag187-))
(Flag187prime-)
(not (Flag67-))
(Flag67prime-)
(not (Flag144-))
(Flag144prime-)
(not (Flag175-))
(Flag175prime-)
(not (Flag209-))
(Flag209prime-)
(not (Flag176-))
(Flag176prime-)
(not (Flag132-))
(Flag132prime-)
(not (Flag40-))
(Flag40prime-)
(not (Flag96-))
(Flag96prime-)
(not (Flag146-))
(Flag146prime-)
(not (Flag46-))
(Flag46prime-)
(not (Flag198-))
(Flag198prime-)
(not (Flag188-))
(Flag188prime-)
(not (Flag98-))
(Flag98prime-)
(not (Flag210-))
(Flag210prime-)
(not (Flag53-))
(Flag53prime-)
(not (Flag55-))
(Flag55prime-)
(not (Flag220-))
(Flag220prime-)
(not (Flag81-))
(Flag81prime-)
(not (Flag161-))
(Flag161prime-)
(not (Flag193-))
(Flag193prime-)
(not (Flag200-))
(Flag200prime-)
(not (Flag58-))
(Flag58prime-)
(not (Flag189-))
(Flag189prime-)
(not (Flag105-))
(Flag105prime-)
(not (Flag113-))
(Flag113prime-)
(not (Flag91-))
(Flag91prime-)
(not (Flag221-))
(Flag221prime-)
(not (Flag68-))
(Flag68prime-)
(not (Flag201-))
(Flag201prime-)
(not (Flag211-))
(Flag211prime-)
(not (Flag190-))
(Flag190prime-)
)
:effect
(and
(Flag222prime-)
(not (Flag222-))
)

)
(:action Flag231Action-0
:parameters ()
:precondition
(and
(Flag41-)
(Flag41prime-)
)
:effect
(and
(Flag231-)
(Flag231prime-)
)

)
(:action Flag231Action-1
:parameters ()
:precondition
(and
(Flag47-)
(Flag47prime-)
)
:effect
(and
(Flag231-)
(Flag231prime-)
)

)
(:action Flag231Action-2
:parameters ()
:precondition
(and
(Flag203-)
(Flag203prime-)
)
:effect
(and
(Flag231-)
(Flag231prime-)
)

)
(:action Flag231Action-3
:parameters ()
:precondition
(and
(Flag192-)
(Flag192prime-)
)
:effect
(and
(Flag231-)
(Flag231prime-)
)

)
(:action Flag231Action-4
:parameters ()
:precondition
(and
(Flag223-)
(Flag223prime-)
)
:effect
(and
(Flag231-)
(Flag231prime-)
)

)
(:action Flag231Action-5
:parameters ()
:precondition
(and
(Flag204-)
(Flag204prime-)
)
:effect
(and
(Flag231-)
(Flag231prime-)
)

)
(:action Flag231Action-6
:parameters ()
:precondition
(and
(Flag134-)
(Flag134prime-)
)
:effect
(and
(Flag231-)
(Flag231prime-)
)

)
(:action Flag231Action-7
:parameters ()
:precondition
(and
(Flag150-)
(Flag150prime-)
)
:effect
(and
(Flag231-)
(Flag231prime-)
)

)
(:action Flag231Action-8
:parameters ()
:precondition
(and
(Flag214-)
(Flag214prime-)
)
:effect
(and
(Flag231-)
(Flag231prime-)
)

)
(:action Flag231Action-9
:parameters ()
:precondition
(and
(Flag119-)
(Flag119prime-)
)
:effect
(and
(Flag231-)
(Flag231prime-)
)

)
(:action Flag231Action-10
:parameters ()
:precondition
(and
(Flag63-)
(Flag63prime-)
)
:effect
(and
(Flag231-)
(Flag231prime-)
)

)
(:action Flag231Action-11
:parameters ()
:precondition
(and
(Flag181-)
(Flag181prime-)
)
:effect
(and
(Flag231-)
(Flag231prime-)
)

)
(:action Flag231Action-12
:parameters ()
:precondition
(and
(Flag87-)
(Flag87prime-)
)
:effect
(and
(Flag231-)
(Flag231prime-)
)

)
(:action Flag231Action-13
:parameters ()
:precondition
(and
(Flag193-)
(Flag193prime-)
)
:effect
(and
(Flag231-)
(Flag231prime-)
)

)
(:action Flag231Action-14
:parameters ()
:precondition
(and
(Flag92-)
(Flag92prime-)
)
:effect
(and
(Flag231-)
(Flag231prime-)
)

)
(:action Flag231Action-15
:parameters ()
:precondition
(and
(Flag224-)
(Flag224prime-)
)
:effect
(and
(Flag231-)
(Flag231prime-)
)

)
(:action Flag231Action-16
:parameters ()
:precondition
(and
(Flag137-)
(Flag137prime-)
)
:effect
(and
(Flag231-)
(Flag231prime-)
)

)
(:action Flag231Action-17
:parameters ()
:precondition
(and
(Flag216-)
(Flag216prime-)
)
:effect
(and
(Flag231-)
(Flag231prime-)
)

)
(:action Flag231Action-18
:parameters ()
:precondition
(and
(Flag42-)
(Flag42prime-)
)
:effect
(and
(Flag231-)
(Flag231prime-)
)

)
(:action Flag231Action-19
:parameters ()
:precondition
(and
(Flag97-)
(Flag97prime-)
)
:effect
(and
(Flag231-)
(Flag231prime-)
)

)
(:action Flag231Action-20
:parameters ()
:precondition
(and
(Flag225-)
(Flag225prime-)
)
:effect
(and
(Flag231-)
(Flag231prime-)
)

)
(:action Flag231Action-21
:parameters ()
:precondition
(and
(Flag167-)
(Flag167prime-)
)
:effect
(and
(Flag231-)
(Flag231prime-)
)

)
(:action Flag231Action-22
:parameters ()
:precondition
(and
(Flag122-)
(Flag122prime-)
)
:effect
(and
(Flag231-)
(Flag231prime-)
)

)
(:action Flag231Action-23
:parameters ()
:precondition
(and
(Flag153-)
(Flag153prime-)
)
:effect
(and
(Flag231-)
(Flag231prime-)
)

)
(:action Flag231Action-24
:parameters ()
:precondition
(and
(Flag102-)
(Flag102prime-)
)
:effect
(and
(Flag231-)
(Flag231prime-)
)

)
(:action Flag231Action-25
:parameters ()
:precondition
(and
(Flag57-)
(Flag57prime-)
)
:effect
(and
(Flag231-)
(Flag231prime-)
)

)
(:action Flag231Action-26
:parameters ()
:precondition
(and
(Flag166-)
(Flag166prime-)
)
:effect
(and
(Flag231-)
(Flag231prime-)
)

)
(:action Flag231Action-27
:parameters ()
:precondition
(and
(Flag123-)
(Flag123prime-)
)
:effect
(and
(Flag231-)
(Flag231prime-)
)

)
(:action Flag231Action-28
:parameters ()
:precondition
(and
(Flag206-)
(Flag206prime-)
)
:effect
(and
(Flag231-)
(Flag231prime-)
)

)
(:action Flag231Action-29
:parameters ()
:precondition
(and
(Flag140-)
(Flag140prime-)
)
:effect
(and
(Flag231-)
(Flag231prime-)
)

)
(:action Flag231Action-30
:parameters ()
:precondition
(and
(Flag61-)
(Flag61prime-)
)
:effect
(and
(Flag231-)
(Flag231prime-)
)

)
(:action Flag231Action-31
:parameters ()
:precondition
(and
(Flag183-)
(Flag183prime-)
)
:effect
(and
(Flag231-)
(Flag231prime-)
)

)
(:action Flag231Action-32
:parameters ()
:precondition
(and
(Flag154-)
(Flag154prime-)
)
:effect
(and
(Flag231-)
(Flag231prime-)
)

)
(:action Flag231Action-33
:parameters ()
:precondition
(and
(Flag128-)
(Flag128prime-)
)
:effect
(and
(Flag231-)
(Flag231prime-)
)

)
(:action Flag231Action-34
:parameters ()
:precondition
(and
(Flag207-)
(Flag207prime-)
)
:effect
(and
(Flag231-)
(Flag231prime-)
)

)
(:action Flag231Action-35
:parameters ()
:precondition
(and
(Flag169-)
(Flag169prime-)
)
:effect
(and
(Flag231-)
(Flag231prime-)
)

)
(:action Flag231Action-36
:parameters ()
:precondition
(and
(Flag179-)
(Flag179prime-)
)
:effect
(and
(Flag231-)
(Flag231prime-)
)

)
(:action Flag231Action-37
:parameters ()
:precondition
(and
(Flag90-)
(Flag90prime-)
)
:effect
(and
(Flag231-)
(Flag231prime-)
)

)
(:action Flag231Action-38
:parameters ()
:precondition
(and
(Flag101-)
(Flag101prime-)
)
:effect
(and
(Flag231-)
(Flag231prime-)
)

)
(:action Flag231Action-39
:parameters ()
:precondition
(and
(Flag226-)
(Flag226prime-)
)
:effect
(and
(Flag231-)
(Flag231prime-)
)

)
(:action Flag231Action-40
:parameters ()
:precondition
(and
(Flag156-)
(Flag156prime-)
)
:effect
(and
(Flag231-)
(Flag231prime-)
)

)
(:action Flag231Action-41
:parameters ()
:precondition
(and
(Flag194-)
(Flag194prime-)
)
:effect
(and
(Flag231-)
(Flag231prime-)
)

)
(:action Flag231Action-42
:parameters ()
:precondition
(and
(Flag111-)
(Flag111prime-)
)
:effect
(and
(Flag231-)
(Flag231prime-)
)

)
(:action Flag231Action-43
:parameters ()
:precondition
(and
(Flag73-)
(Flag73prime-)
)
:effect
(and
(Flag231-)
(Flag231prime-)
)

)
(:action Flag231Action-44
:parameters ()
:precondition
(and
(Flag143-)
(Flag143prime-)
)
:effect
(and
(Flag231-)
(Flag231prime-)
)

)
(:action Flag231Action-45
:parameters ()
:precondition
(and
(Flag132-)
(Flag132prime-)
)
:effect
(and
(Flag231-)
(Flag231prime-)
)

)
(:action Flag231Action-46
:parameters ()
:precondition
(and
(Flag157-)
(Flag157prime-)
)
:effect
(and
(Flag231-)
(Flag231prime-)
)

)
(:action Flag231Action-47
:parameters ()
:precondition
(and
(Flag208-)
(Flag208prime-)
)
:effect
(and
(Flag231-)
(Flag231prime-)
)

)
(:action Flag231Action-48
:parameters ()
:precondition
(and
(Flag116-)
(Flag116prime-)
)
:effect
(and
(Flag231-)
(Flag231prime-)
)

)
(:action Flag231Action-49
:parameters ()
:precondition
(and
(Flag195-)
(Flag195prime-)
)
:effect
(and
(Flag231-)
(Flag231prime-)
)

)
(:action Flag231Action-50
:parameters ()
:precondition
(and
(Flag79-)
(Flag79prime-)
)
:effect
(and
(Flag231-)
(Flag231prime-)
)

)
(:action Flag231Action-51
:parameters ()
:precondition
(and
(Flag185-)
(Flag185prime-)
)
:effect
(and
(Flag231-)
(Flag231prime-)
)

)
(:action Flag231Action-52
:parameters ()
:precondition
(and
(Flag118-)
(Flag118prime-)
)
:effect
(and
(Flag231-)
(Flag231prime-)
)

)
(:action Flag231Action-53
:parameters ()
:precondition
(and
(Flag196-)
(Flag196prime-)
)
:effect
(and
(Flag231-)
(Flag231prime-)
)

)
(:action Flag231Action-54
:parameters ()
:precondition
(and
(Flag172-)
(Flag172prime-)
)
:effect
(and
(Flag231-)
(Flag231prime-)
)

)
(:action Flag231Action-55
:parameters ()
:precondition
(and
(Flag218-)
(Flag218prime-)
)
:effect
(and
(Flag231-)
(Flag231prime-)
)

)
(:action Flag231Action-56
:parameters ()
:precondition
(and
(Flag108-)
(Flag108prime-)
)
:effect
(and
(Flag231-)
(Flag231prime-)
)

)
(:action Flag231Action-57
:parameters ()
:precondition
(and
(Flag173-)
(Flag173prime-)
)
:effect
(and
(Flag231-)
(Flag231prime-)
)

)
(:action Flag231Action-58
:parameters ()
:precondition
(and
(Flag130-)
(Flag130prime-)
)
:effect
(and
(Flag231-)
(Flag231prime-)
)

)
(:action Flag231Action-59
:parameters ()
:precondition
(and
(Flag160-)
(Flag160prime-)
)
:effect
(and
(Flag231-)
(Flag231prime-)
)

)
(:action Flag231Action-60
:parameters ()
:precondition
(and
(Flag83-)
(Flag83prime-)
)
:effect
(and
(Flag231-)
(Flag231prime-)
)

)
(:action Flag231Action-61
:parameters ()
:precondition
(and
(Flag219-)
(Flag219prime-)
)
:effect
(and
(Flag231-)
(Flag231prime-)
)

)
(:action Flag231Action-62
:parameters ()
:precondition
(and
(Flag174-)
(Flag174prime-)
)
:effect
(and
(Flag231-)
(Flag231prime-)
)

)
(:action Flag231Action-63
:parameters ()
:precondition
(and
(Flag66-)
(Flag66prime-)
)
:effect
(and
(Flag231-)
(Flag231prime-)
)

)
(:action Flag231Action-64
:parameters ()
:precondition
(and
(Flag187-)
(Flag187prime-)
)
:effect
(and
(Flag231-)
(Flag231prime-)
)

)
(:action Flag231Action-65
:parameters ()
:precondition
(and
(Flag67-)
(Flag67prime-)
)
:effect
(and
(Flag231-)
(Flag231prime-)
)

)
(:action Flag231Action-66
:parameters ()
:precondition
(and
(Flag144-)
(Flag144prime-)
)
:effect
(and
(Flag231-)
(Flag231prime-)
)

)
(:action Flag231Action-67
:parameters ()
:precondition
(and
(Flag227-)
(Flag227prime-)
)
:effect
(and
(Flag231-)
(Flag231prime-)
)

)
(:action Flag231Action-68
:parameters ()
:precondition
(and
(Flag209-)
(Flag209prime-)
)
:effect
(and
(Flag231-)
(Flag231prime-)
)

)
(:action Flag231Action-69
:parameters ()
:precondition
(and
(Flag176-)
(Flag176prime-)
)
:effect
(and
(Flag231-)
(Flag231prime-)
)

)
(:action Flag231Action-70
:parameters ()
:precondition
(and
(Flag228-)
(Flag228prime-)
)
:effect
(and
(Flag231-)
(Flag231prime-)
)

)
(:action Flag231Action-71
:parameters ()
:precondition
(and
(Flag96-)
(Flag96prime-)
)
:effect
(and
(Flag231-)
(Flag231prime-)
)

)
(:action Flag231Action-72
:parameters ()
:precondition
(and
(Flag146-)
(Flag146prime-)
)
:effect
(and
(Flag231-)
(Flag231prime-)
)

)
(:action Flag231Action-73
:parameters ()
:precondition
(and
(Flag46-)
(Flag46prime-)
)
:effect
(and
(Flag231-)
(Flag231prime-)
)

)
(:action Flag231Action-74
:parameters ()
:precondition
(and
(Flag188-)
(Flag188prime-)
)
:effect
(and
(Flag231-)
(Flag231prime-)
)

)
(:action Flag231Action-75
:parameters ()
:precondition
(and
(Flag98-)
(Flag98prime-)
)
:effect
(and
(Flag231-)
(Flag231prime-)
)

)
(:action Flag231Action-76
:parameters ()
:precondition
(and
(Flag210-)
(Flag210prime-)
)
:effect
(and
(Flag231-)
(Flag231prime-)
)

)
(:action Flag231Action-77
:parameters ()
:precondition
(and
(Flag53-)
(Flag53prime-)
)
:effect
(and
(Flag231-)
(Flag231prime-)
)

)
(:action Flag231Action-78
:parameters ()
:precondition
(and
(Flag220-)
(Flag220prime-)
)
:effect
(and
(Flag231-)
(Flag231prime-)
)

)
(:action Flag231Action-79
:parameters ()
:precondition
(and
(Flag81-)
(Flag81prime-)
)
:effect
(and
(Flag231-)
(Flag231prime-)
)

)
(:action Flag231Action-80
:parameters ()
:precondition
(and
(Flag229-)
(Flag229prime-)
)
:effect
(and
(Flag231-)
(Flag231prime-)
)

)
(:action Flag231Action-81
:parameters ()
:precondition
(and
(Flag161-)
(Flag161prime-)
)
:effect
(and
(Flag231-)
(Flag231prime-)
)

)
(:action Flag231Action-82
:parameters ()
:precondition
(and
(Flag162-)
(Flag162prime-)
)
:effect
(and
(Flag231-)
(Flag231prime-)
)

)
(:action Flag231Action-83
:parameters ()
:precondition
(and
(Flag200-)
(Flag200prime-)
)
:effect
(and
(Flag231-)
(Flag231prime-)
)

)
(:action Flag231Action-84
:parameters ()
:precondition
(and
(Flag58-)
(Flag58prime-)
)
:effect
(and
(Flag231-)
(Flag231prime-)
)

)
(:action Flag231Action-85
:parameters ()
:precondition
(and
(Flag189-)
(Flag189prime-)
)
:effect
(and
(Flag231-)
(Flag231prime-)
)

)
(:action Flag231Action-86
:parameters ()
:precondition
(and
(Flag105-)
(Flag105prime-)
)
:effect
(and
(Flag231-)
(Flag231prime-)
)

)
(:action Flag231Action-87
:parameters ()
:precondition
(and
(Flag74-)
(Flag74prime-)
)
:effect
(and
(Flag231-)
(Flag231prime-)
)

)
(:action Flag231Action-88
:parameters ()
:precondition
(and
(Flag91-)
(Flag91prime-)
)
:effect
(and
(Flag231-)
(Flag231prime-)
)

)
(:action Flag231Action-89
:parameters ()
:precondition
(and
(Flag221-)
(Flag221prime-)
)
:effect
(and
(Flag231-)
(Flag231prime-)
)

)
(:action Flag231Action-90
:parameters ()
:precondition
(and
(Flag40-)
(Flag40prime-)
)
:effect
(and
(Flag231-)
(Flag231prime-)
)

)
(:action Flag231Action-91
:parameters ()
:precondition
(and
(Flag201-)
(Flag201prime-)
)
:effect
(and
(Flag231-)
(Flag231prime-)
)

)
(:action Flag231Action-92
:parameters ()
:precondition
(and
(Flag211-)
(Flag211prime-)
)
:effect
(and
(Flag231-)
(Flag231prime-)
)

)
(:action Flag231Action-93
:parameters ()
:precondition
(and
(Flag230-)
(Flag230prime-)
)
:effect
(and
(Flag231-)
(Flag231prime-)
)

)
(:action Flag231Action-94
:parameters ()
:precondition
(and
(Flag190-)
(Flag190prime-)
)
:effect
(and
(Flag231-)
(Flag231prime-)
)

)
(:action Prim231Action
:parameters ()
:precondition
(and
(not (Flag41-))
(Flag41prime-)
(not (Flag47-))
(Flag47prime-)
(not (Flag203-))
(Flag203prime-)
(not (Flag192-))
(Flag192prime-)
(not (Flag223-))
(Flag223prime-)
(not (Flag204-))
(Flag204prime-)
(not (Flag134-))
(Flag134prime-)
(not (Flag150-))
(Flag150prime-)
(not (Flag214-))
(Flag214prime-)
(not (Flag119-))
(Flag119prime-)
(not (Flag63-))
(Flag63prime-)
(not (Flag181-))
(Flag181prime-)
(not (Flag87-))
(Flag87prime-)
(not (Flag193-))
(Flag193prime-)
(not (Flag92-))
(Flag92prime-)
(not (Flag224-))
(Flag224prime-)
(not (Flag137-))
(Flag137prime-)
(not (Flag216-))
(Flag216prime-)
(not (Flag42-))
(Flag42prime-)
(not (Flag97-))
(Flag97prime-)
(not (Flag225-))
(Flag225prime-)
(not (Flag167-))
(Flag167prime-)
(not (Flag122-))
(Flag122prime-)
(not (Flag153-))
(Flag153prime-)
(not (Flag102-))
(Flag102prime-)
(not (Flag57-))
(Flag57prime-)
(not (Flag166-))
(Flag166prime-)
(not (Flag123-))
(Flag123prime-)
(not (Flag206-))
(Flag206prime-)
(not (Flag140-))
(Flag140prime-)
(not (Flag61-))
(Flag61prime-)
(not (Flag183-))
(Flag183prime-)
(not (Flag154-))
(Flag154prime-)
(not (Flag128-))
(Flag128prime-)
(not (Flag207-))
(Flag207prime-)
(not (Flag169-))
(Flag169prime-)
(not (Flag179-))
(Flag179prime-)
(not (Flag90-))
(Flag90prime-)
(not (Flag101-))
(Flag101prime-)
(not (Flag226-))
(Flag226prime-)
(not (Flag156-))
(Flag156prime-)
(not (Flag194-))
(Flag194prime-)
(not (Flag111-))
(Flag111prime-)
(not (Flag73-))
(Flag73prime-)
(not (Flag143-))
(Flag143prime-)
(not (Flag132-))
(Flag132prime-)
(not (Flag157-))
(Flag157prime-)
(not (Flag208-))
(Flag208prime-)
(not (Flag116-))
(Flag116prime-)
(not (Flag195-))
(Flag195prime-)
(not (Flag79-))
(Flag79prime-)
(not (Flag185-))
(Flag185prime-)
(not (Flag118-))
(Flag118prime-)
(not (Flag196-))
(Flag196prime-)
(not (Flag172-))
(Flag172prime-)
(not (Flag218-))
(Flag218prime-)
(not (Flag108-))
(Flag108prime-)
(not (Flag173-))
(Flag173prime-)
(not (Flag130-))
(Flag130prime-)
(not (Flag160-))
(Flag160prime-)
(not (Flag83-))
(Flag83prime-)
(not (Flag219-))
(Flag219prime-)
(not (Flag174-))
(Flag174prime-)
(not (Flag66-))
(Flag66prime-)
(not (Flag187-))
(Flag187prime-)
(not (Flag67-))
(Flag67prime-)
(not (Flag144-))
(Flag144prime-)
(not (Flag227-))
(Flag227prime-)
(not (Flag209-))
(Flag209prime-)
(not (Flag176-))
(Flag176prime-)
(not (Flag228-))
(Flag228prime-)
(not (Flag96-))
(Flag96prime-)
(not (Flag146-))
(Flag146prime-)
(not (Flag46-))
(Flag46prime-)
(not (Flag188-))
(Flag188prime-)
(not (Flag98-))
(Flag98prime-)
(not (Flag210-))
(Flag210prime-)
(not (Flag53-))
(Flag53prime-)
(not (Flag220-))
(Flag220prime-)
(not (Flag81-))
(Flag81prime-)
(not (Flag229-))
(Flag229prime-)
(not (Flag161-))
(Flag161prime-)
(not (Flag162-))
(Flag162prime-)
(not (Flag200-))
(Flag200prime-)
(not (Flag58-))
(Flag58prime-)
(not (Flag189-))
(Flag189prime-)
(not (Flag105-))
(Flag105prime-)
(not (Flag74-))
(Flag74prime-)
(not (Flag91-))
(Flag91prime-)
(not (Flag221-))
(Flag221prime-)
(not (Flag40-))
(Flag40prime-)
(not (Flag201-))
(Flag201prime-)
(not (Flag211-))
(Flag211prime-)
(not (Flag230-))
(Flag230prime-)
(not (Flag190-))
(Flag190prime-)
)
:effect
(and
(Flag231prime-)
(not (Flag231-))
)

)
(:action Flag238Action-0
:parameters ()
:precondition
(and
(Flag41-)
(Flag41prime-)
)
:effect
(and
(Flag238-)
(Flag238prime-)
)

)
(:action Flag238Action-1
:parameters ()
:precondition
(and
(Flag47-)
(Flag47prime-)
)
:effect
(and
(Flag238-)
(Flag238prime-)
)

)
(:action Flag238Action-2
:parameters ()
:precondition
(and
(Flag232-)
(Flag232prime-)
)
:effect
(and
(Flag238-)
(Flag238prime-)
)

)
(:action Flag238Action-3
:parameters ()
:precondition
(and
(Flag203-)
(Flag203prime-)
)
:effect
(and
(Flag238-)
(Flag238prime-)
)

)
(:action Flag238Action-4
:parameters ()
:precondition
(and
(Flag223-)
(Flag223prime-)
)
:effect
(and
(Flag238-)
(Flag238prime-)
)

)
(:action Flag238Action-5
:parameters ()
:precondition
(and
(Flag204-)
(Flag204prime-)
)
:effect
(and
(Flag238-)
(Flag238prime-)
)

)
(:action Flag238Action-6
:parameters ()
:precondition
(and
(Flag134-)
(Flag134prime-)
)
:effect
(and
(Flag238-)
(Flag238prime-)
)

)
(:action Flag238Action-7
:parameters ()
:precondition
(and
(Flag150-)
(Flag150prime-)
)
:effect
(and
(Flag238-)
(Flag238prime-)
)

)
(:action Flag238Action-8
:parameters ()
:precondition
(and
(Flag214-)
(Flag214prime-)
)
:effect
(and
(Flag238-)
(Flag238prime-)
)

)
(:action Flag238Action-9
:parameters ()
:precondition
(and
(Flag119-)
(Flag119prime-)
)
:effect
(and
(Flag238-)
(Flag238prime-)
)

)
(:action Flag238Action-10
:parameters ()
:precondition
(and
(Flag166-)
(Flag166prime-)
)
:effect
(and
(Flag238-)
(Flag238prime-)
)

)
(:action Flag238Action-11
:parameters ()
:precondition
(and
(Flag92-)
(Flag92prime-)
)
:effect
(and
(Flag238-)
(Flag238prime-)
)

)
(:action Flag238Action-12
:parameters ()
:precondition
(and
(Flag224-)
(Flag224prime-)
)
:effect
(and
(Flag238-)
(Flag238prime-)
)

)
(:action Flag238Action-13
:parameters ()
:precondition
(and
(Flag137-)
(Flag137prime-)
)
:effect
(and
(Flag238-)
(Flag238prime-)
)

)
(:action Flag238Action-14
:parameters ()
:precondition
(and
(Flag216-)
(Flag216prime-)
)
:effect
(and
(Flag238-)
(Flag238prime-)
)

)
(:action Flag238Action-15
:parameters ()
:precondition
(and
(Flag42-)
(Flag42prime-)
)
:effect
(and
(Flag238-)
(Flag238prime-)
)

)
(:action Flag238Action-16
:parameters ()
:precondition
(and
(Flag97-)
(Flag97prime-)
)
:effect
(and
(Flag238-)
(Flag238prime-)
)

)
(:action Flag238Action-17
:parameters ()
:precondition
(and
(Flag225-)
(Flag225prime-)
)
:effect
(and
(Flag238-)
(Flag238prime-)
)

)
(:action Flag238Action-18
:parameters ()
:precondition
(and
(Flag167-)
(Flag167prime-)
)
:effect
(and
(Flag238-)
(Flag238prime-)
)

)
(:action Flag238Action-19
:parameters ()
:precondition
(and
(Flag122-)
(Flag122prime-)
)
:effect
(and
(Flag238-)
(Flag238prime-)
)

)
(:action Flag238Action-20
:parameters ()
:precondition
(and
(Flag153-)
(Flag153prime-)
)
:effect
(and
(Flag238-)
(Flag238prime-)
)

)
(:action Flag238Action-21
:parameters ()
:precondition
(and
(Flag102-)
(Flag102prime-)
)
:effect
(and
(Flag238-)
(Flag238prime-)
)

)
(:action Flag238Action-22
:parameters ()
:precondition
(and
(Flag57-)
(Flag57prime-)
)
:effect
(and
(Flag238-)
(Flag238prime-)
)

)
(:action Flag238Action-23
:parameters ()
:precondition
(and
(Flag87-)
(Flag87prime-)
)
:effect
(and
(Flag238-)
(Flag238prime-)
)

)
(:action Flag238Action-24
:parameters ()
:precondition
(and
(Flag123-)
(Flag123prime-)
)
:effect
(and
(Flag238-)
(Flag238prime-)
)

)
(:action Flag238Action-25
:parameters ()
:precondition
(and
(Flag206-)
(Flag206prime-)
)
:effect
(and
(Flag238-)
(Flag238prime-)
)

)
(:action Flag238Action-26
:parameters ()
:precondition
(and
(Flag140-)
(Flag140prime-)
)
:effect
(and
(Flag238-)
(Flag238prime-)
)

)
(:action Flag238Action-27
:parameters ()
:precondition
(and
(Flag233-)
(Flag233prime-)
)
:effect
(and
(Flag238-)
(Flag238prime-)
)

)
(:action Flag238Action-28
:parameters ()
:precondition
(and
(Flag183-)
(Flag183prime-)
)
:effect
(and
(Flag238-)
(Flag238prime-)
)

)
(:action Flag238Action-29
:parameters ()
:precondition
(and
(Flag179-)
(Flag179prime-)
)
:effect
(and
(Flag238-)
(Flag238prime-)
)

)
(:action Flag238Action-30
:parameters ()
:precondition
(and
(Flag207-)
(Flag207prime-)
)
:effect
(and
(Flag238-)
(Flag238prime-)
)

)
(:action Flag238Action-31
:parameters ()
:precondition
(and
(Flag169-)
(Flag169prime-)
)
:effect
(and
(Flag238-)
(Flag238prime-)
)

)
(:action Flag238Action-32
:parameters ()
:precondition
(and
(Flag90-)
(Flag90prime-)
)
:effect
(and
(Flag238-)
(Flag238prime-)
)

)
(:action Flag238Action-33
:parameters ()
:precondition
(and
(Flag101-)
(Flag101prime-)
)
:effect
(and
(Flag238-)
(Flag238prime-)
)

)
(:action Flag238Action-34
:parameters ()
:precondition
(and
(Flag193-)
(Flag193prime-)
)
:effect
(and
(Flag238-)
(Flag238prime-)
)

)
(:action Flag238Action-35
:parameters ()
:precondition
(and
(Flag194-)
(Flag194prime-)
)
:effect
(and
(Flag238-)
(Flag238prime-)
)

)
(:action Flag238Action-36
:parameters ()
:precondition
(and
(Flag234-)
(Flag234prime-)
)
:effect
(and
(Flag238-)
(Flag238prime-)
)

)
(:action Flag238Action-37
:parameters ()
:precondition
(and
(Flag111-)
(Flag111prime-)
)
:effect
(and
(Flag238-)
(Flag238prime-)
)

)
(:action Flag238Action-38
:parameters ()
:precondition
(and
(Flag73-)
(Flag73prime-)
)
:effect
(and
(Flag238-)
(Flag238prime-)
)

)
(:action Flag238Action-39
:parameters ()
:precondition
(and
(Flag143-)
(Flag143prime-)
)
:effect
(and
(Flag238-)
(Flag238prime-)
)

)
(:action Flag238Action-40
:parameters ()
:precondition
(and
(Flag132-)
(Flag132prime-)
)
:effect
(and
(Flag238-)
(Flag238prime-)
)

)
(:action Flag238Action-41
:parameters ()
:precondition
(and
(Flag157-)
(Flag157prime-)
)
:effect
(and
(Flag238-)
(Flag238prime-)
)

)
(:action Flag238Action-42
:parameters ()
:precondition
(and
(Flag208-)
(Flag208prime-)
)
:effect
(and
(Flag238-)
(Flag238prime-)
)

)
(:action Flag238Action-43
:parameters ()
:precondition
(and
(Flag116-)
(Flag116prime-)
)
:effect
(and
(Flag238-)
(Flag238prime-)
)

)
(:action Flag238Action-44
:parameters ()
:precondition
(and
(Flag195-)
(Flag195prime-)
)
:effect
(and
(Flag238-)
(Flag238prime-)
)

)
(:action Flag238Action-45
:parameters ()
:precondition
(and
(Flag79-)
(Flag79prime-)
)
:effect
(and
(Flag238-)
(Flag238prime-)
)

)
(:action Flag238Action-46
:parameters ()
:precondition
(and
(Flag185-)
(Flag185prime-)
)
:effect
(and
(Flag238-)
(Flag238prime-)
)

)
(:action Flag238Action-47
:parameters ()
:precondition
(and
(Flag118-)
(Flag118prime-)
)
:effect
(and
(Flag238-)
(Flag238prime-)
)

)
(:action Flag238Action-48
:parameters ()
:precondition
(and
(Flag196-)
(Flag196prime-)
)
:effect
(and
(Flag238-)
(Flag238prime-)
)

)
(:action Flag238Action-49
:parameters ()
:precondition
(and
(Flag172-)
(Flag172prime-)
)
:effect
(and
(Flag238-)
(Flag238prime-)
)

)
(:action Flag238Action-50
:parameters ()
:precondition
(and
(Flag218-)
(Flag218prime-)
)
:effect
(and
(Flag238-)
(Flag238prime-)
)

)
(:action Flag238Action-51
:parameters ()
:precondition
(and
(Flag108-)
(Flag108prime-)
)
:effect
(and
(Flag238-)
(Flag238prime-)
)

)
(:action Flag238Action-52
:parameters ()
:precondition
(and
(Flag173-)
(Flag173prime-)
)
:effect
(and
(Flag238-)
(Flag238prime-)
)

)
(:action Flag238Action-53
:parameters ()
:precondition
(and
(Flag130-)
(Flag130prime-)
)
:effect
(and
(Flag238-)
(Flag238prime-)
)

)
(:action Flag238Action-54
:parameters ()
:precondition
(and
(Flag160-)
(Flag160prime-)
)
:effect
(and
(Flag238-)
(Flag238prime-)
)

)
(:action Flag238Action-55
:parameters ()
:precondition
(and
(Flag219-)
(Flag219prime-)
)
:effect
(and
(Flag238-)
(Flag238prime-)
)

)
(:action Flag238Action-56
:parameters ()
:precondition
(and
(Flag235-)
(Flag235prime-)
)
:effect
(and
(Flag238-)
(Flag238prime-)
)

)
(:action Flag238Action-57
:parameters ()
:precondition
(and
(Flag236-)
(Flag236prime-)
)
:effect
(and
(Flag238-)
(Flag238prime-)
)

)
(:action Flag238Action-58
:parameters ()
:precondition
(and
(Flag66-)
(Flag66prime-)
)
:effect
(and
(Flag238-)
(Flag238prime-)
)

)
(:action Flag238Action-59
:parameters ()
:precondition
(and
(Flag187-)
(Flag187prime-)
)
:effect
(and
(Flag238-)
(Flag238prime-)
)

)
(:action Flag238Action-60
:parameters ()
:precondition
(and
(Flag237-)
(Flag237prime-)
)
:effect
(and
(Flag238-)
(Flag238prime-)
)

)
(:action Flag238Action-61
:parameters ()
:precondition
(and
(Flag67-)
(Flag67prime-)
)
:effect
(and
(Flag238-)
(Flag238prime-)
)

)
(:action Flag238Action-62
:parameters ()
:precondition
(and
(Flag144-)
(Flag144prime-)
)
:effect
(and
(Flag238-)
(Flag238prime-)
)

)
(:action Flag238Action-63
:parameters ()
:precondition
(and
(Flag176-)
(Flag176prime-)
)
:effect
(and
(Flag238-)
(Flag238prime-)
)

)
(:action Flag238Action-64
:parameters ()
:precondition
(and
(Flag228-)
(Flag228prime-)
)
:effect
(and
(Flag238-)
(Flag238prime-)
)

)
(:action Flag238Action-65
:parameters ()
:precondition
(and
(Flag96-)
(Flag96prime-)
)
:effect
(and
(Flag238-)
(Flag238prime-)
)

)
(:action Flag238Action-66
:parameters ()
:precondition
(and
(Flag146-)
(Flag146prime-)
)
:effect
(and
(Flag238-)
(Flag238prime-)
)

)
(:action Flag238Action-67
:parameters ()
:precondition
(and
(Flag46-)
(Flag46prime-)
)
:effect
(and
(Flag238-)
(Flag238prime-)
)

)
(:action Flag238Action-68
:parameters ()
:precondition
(and
(Flag188-)
(Flag188prime-)
)
:effect
(and
(Flag238-)
(Flag238prime-)
)

)
(:action Flag238Action-69
:parameters ()
:precondition
(and
(Flag98-)
(Flag98prime-)
)
:effect
(and
(Flag238-)
(Flag238prime-)
)

)
(:action Flag238Action-70
:parameters ()
:precondition
(and
(Flag210-)
(Flag210prime-)
)
:effect
(and
(Flag238-)
(Flag238prime-)
)

)
(:action Flag238Action-71
:parameters ()
:precondition
(and
(Flag226-)
(Flag226prime-)
)
:effect
(and
(Flag238-)
(Flag238prime-)
)

)
(:action Flag238Action-72
:parameters ()
:precondition
(and
(Flag53-)
(Flag53prime-)
)
:effect
(and
(Flag238-)
(Flag238prime-)
)

)
(:action Flag238Action-73
:parameters ()
:precondition
(and
(Flag220-)
(Flag220prime-)
)
:effect
(and
(Flag238-)
(Flag238prime-)
)

)
(:action Flag238Action-74
:parameters ()
:precondition
(and
(Flag81-)
(Flag81prime-)
)
:effect
(and
(Flag238-)
(Flag238prime-)
)

)
(:action Flag238Action-75
:parameters ()
:precondition
(and
(Flag161-)
(Flag161prime-)
)
:effect
(and
(Flag238-)
(Flag238prime-)
)

)
(:action Flag238Action-76
:parameters ()
:precondition
(and
(Flag162-)
(Flag162prime-)
)
:effect
(and
(Flag238-)
(Flag238prime-)
)

)
(:action Flag238Action-77
:parameters ()
:precondition
(and
(Flag200-)
(Flag200prime-)
)
:effect
(and
(Flag238-)
(Flag238prime-)
)

)
(:action Flag238Action-78
:parameters ()
:precondition
(and
(Flag58-)
(Flag58prime-)
)
:effect
(and
(Flag238-)
(Flag238prime-)
)

)
(:action Flag238Action-79
:parameters ()
:precondition
(and
(Flag189-)
(Flag189prime-)
)
:effect
(and
(Flag238-)
(Flag238prime-)
)

)
(:action Flag238Action-80
:parameters ()
:precondition
(and
(Flag74-)
(Flag74prime-)
)
:effect
(and
(Flag238-)
(Flag238prime-)
)

)
(:action Flag238Action-81
:parameters ()
:precondition
(and
(Flag221-)
(Flag221prime-)
)
:effect
(and
(Flag238-)
(Flag238prime-)
)

)
(:action Flag238Action-82
:parameters ()
:precondition
(and
(Flag40-)
(Flag40prime-)
)
:effect
(and
(Flag238-)
(Flag238prime-)
)

)
(:action Flag238Action-83
:parameters ()
:precondition
(and
(Flag201-)
(Flag201prime-)
)
:effect
(and
(Flag238-)
(Flag238prime-)
)

)
(:action Flag238Action-84
:parameters ()
:precondition
(and
(Flag211-)
(Flag211prime-)
)
:effect
(and
(Flag238-)
(Flag238prime-)
)

)
(:action Flag238Action-85
:parameters ()
:precondition
(and
(Flag230-)
(Flag230prime-)
)
:effect
(and
(Flag238-)
(Flag238prime-)
)

)
(:action Flag238Action-86
:parameters ()
:precondition
(and
(Flag190-)
(Flag190prime-)
)
:effect
(and
(Flag238-)
(Flag238prime-)
)

)
(:action Prim238Action
:parameters ()
:precondition
(and
(not (Flag41-))
(Flag41prime-)
(not (Flag47-))
(Flag47prime-)
(not (Flag232-))
(Flag232prime-)
(not (Flag203-))
(Flag203prime-)
(not (Flag223-))
(Flag223prime-)
(not (Flag204-))
(Flag204prime-)
(not (Flag134-))
(Flag134prime-)
(not (Flag150-))
(Flag150prime-)
(not (Flag214-))
(Flag214prime-)
(not (Flag119-))
(Flag119prime-)
(not (Flag166-))
(Flag166prime-)
(not (Flag92-))
(Flag92prime-)
(not (Flag224-))
(Flag224prime-)
(not (Flag137-))
(Flag137prime-)
(not (Flag216-))
(Flag216prime-)
(not (Flag42-))
(Flag42prime-)
(not (Flag97-))
(Flag97prime-)
(not (Flag225-))
(Flag225prime-)
(not (Flag167-))
(Flag167prime-)
(not (Flag122-))
(Flag122prime-)
(not (Flag153-))
(Flag153prime-)
(not (Flag102-))
(Flag102prime-)
(not (Flag57-))
(Flag57prime-)
(not (Flag87-))
(Flag87prime-)
(not (Flag123-))
(Flag123prime-)
(not (Flag206-))
(Flag206prime-)
(not (Flag140-))
(Flag140prime-)
(not (Flag233-))
(Flag233prime-)
(not (Flag183-))
(Flag183prime-)
(not (Flag179-))
(Flag179prime-)
(not (Flag207-))
(Flag207prime-)
(not (Flag169-))
(Flag169prime-)
(not (Flag90-))
(Flag90prime-)
(not (Flag101-))
(Flag101prime-)
(not (Flag193-))
(Flag193prime-)
(not (Flag194-))
(Flag194prime-)
(not (Flag234-))
(Flag234prime-)
(not (Flag111-))
(Flag111prime-)
(not (Flag73-))
(Flag73prime-)
(not (Flag143-))
(Flag143prime-)
(not (Flag132-))
(Flag132prime-)
(not (Flag157-))
(Flag157prime-)
(not (Flag208-))
(Flag208prime-)
(not (Flag116-))
(Flag116prime-)
(not (Flag195-))
(Flag195prime-)
(not (Flag79-))
(Flag79prime-)
(not (Flag185-))
(Flag185prime-)
(not (Flag118-))
(Flag118prime-)
(not (Flag196-))
(Flag196prime-)
(not (Flag172-))
(Flag172prime-)
(not (Flag218-))
(Flag218prime-)
(not (Flag108-))
(Flag108prime-)
(not (Flag173-))
(Flag173prime-)
(not (Flag130-))
(Flag130prime-)
(not (Flag160-))
(Flag160prime-)
(not (Flag219-))
(Flag219prime-)
(not (Flag235-))
(Flag235prime-)
(not (Flag236-))
(Flag236prime-)
(not (Flag66-))
(Flag66prime-)
(not (Flag187-))
(Flag187prime-)
(not (Flag237-))
(Flag237prime-)
(not (Flag67-))
(Flag67prime-)
(not (Flag144-))
(Flag144prime-)
(not (Flag176-))
(Flag176prime-)
(not (Flag228-))
(Flag228prime-)
(not (Flag96-))
(Flag96prime-)
(not (Flag146-))
(Flag146prime-)
(not (Flag46-))
(Flag46prime-)
(not (Flag188-))
(Flag188prime-)
(not (Flag98-))
(Flag98prime-)
(not (Flag210-))
(Flag210prime-)
(not (Flag226-))
(Flag226prime-)
(not (Flag53-))
(Flag53prime-)
(not (Flag220-))
(Flag220prime-)
(not (Flag81-))
(Flag81prime-)
(not (Flag161-))
(Flag161prime-)
(not (Flag162-))
(Flag162prime-)
(not (Flag200-))
(Flag200prime-)
(not (Flag58-))
(Flag58prime-)
(not (Flag189-))
(Flag189prime-)
(not (Flag74-))
(Flag74prime-)
(not (Flag221-))
(Flag221prime-)
(not (Flag40-))
(Flag40prime-)
(not (Flag201-))
(Flag201prime-)
(not (Flag211-))
(Flag211prime-)
(not (Flag230-))
(Flag230prime-)
(not (Flag190-))
(Flag190prime-)
)
:effect
(and
(Flag238prime-)
(not (Flag238-))
)

)
(:action Flag245Action-0
:parameters ()
:precondition
(and
(Flag208-)
(Flag208prime-)
)
:effect
(and
(Flag245-)
(Flag245prime-)
)

)
(:action Flag245Action-1
:parameters ()
:precondition
(and
(Flag96-)
(Flag96prime-)
)
:effect
(and
(Flag245-)
(Flag245prime-)
)

)
(:action Flag245Action-2
:parameters ()
:precondition
(and
(Flag116-)
(Flag116prime-)
)
:effect
(and
(Flag245-)
(Flag245prime-)
)

)
(:action Flag245Action-3
:parameters ()
:precondition
(and
(Flag41-)
(Flag41prime-)
)
:effect
(and
(Flag245-)
(Flag245prime-)
)

)
(:action Flag245Action-4
:parameters ()
:precondition
(and
(Flag132-)
(Flag132prime-)
)
:effect
(and
(Flag245-)
(Flag245prime-)
)

)
(:action Flag245Action-5
:parameters ()
:precondition
(and
(Flag146-)
(Flag146prime-)
)
:effect
(and
(Flag245-)
(Flag245prime-)
)

)
(:action Flag245Action-6
:parameters ()
:precondition
(and
(Flag47-)
(Flag47prime-)
)
:effect
(and
(Flag245-)
(Flag245prime-)
)

)
(:action Flag245Action-7
:parameters ()
:precondition
(and
(Flag232-)
(Flag232prime-)
)
:effect
(and
(Flag245-)
(Flag245prime-)
)

)
(:action Flag245Action-8
:parameters ()
:precondition
(and
(Flag46-)
(Flag46prime-)
)
:effect
(and
(Flag245-)
(Flag245prime-)
)

)
(:action Flag245Action-9
:parameters ()
:precondition
(and
(Flag73-)
(Flag73prime-)
)
:effect
(and
(Flag245-)
(Flag245prime-)
)

)
(:action Flag245Action-10
:parameters ()
:precondition
(and
(Flag203-)
(Flag203prime-)
)
:effect
(and
(Flag245-)
(Flag245prime-)
)

)
(:action Flag245Action-11
:parameters ()
:precondition
(and
(Flag185-)
(Flag185prime-)
)
:effect
(and
(Flag245-)
(Flag245prime-)
)

)
(:action Flag245Action-12
:parameters ()
:precondition
(and
(Flag188-)
(Flag188prime-)
)
:effect
(and
(Flag245-)
(Flag245prime-)
)

)
(:action Flag245Action-13
:parameters ()
:precondition
(and
(Flag98-)
(Flag98prime-)
)
:effect
(and
(Flag245-)
(Flag245prime-)
)

)
(:action Flag245Action-14
:parameters ()
:precondition
(and
(Flag172-)
(Flag172prime-)
)
:effect
(and
(Flag245-)
(Flag245prime-)
)

)
(:action Flag245Action-15
:parameters ()
:precondition
(and
(Flag239-)
(Flag239prime-)
)
:effect
(and
(Flag245-)
(Flag245prime-)
)

)
(:action Flag245Action-16
:parameters ()
:precondition
(and
(Flag40-)
(Flag40prime-)
)
:effect
(and
(Flag245-)
(Flag245prime-)
)

)
(:action Flag245Action-17
:parameters ()
:precondition
(and
(Flag223-)
(Flag223prime-)
)
:effect
(and
(Flag245-)
(Flag245prime-)
)

)
(:action Flag245Action-18
:parameters ()
:precondition
(and
(Flag225-)
(Flag225prime-)
)
:effect
(and
(Flag245-)
(Flag245prime-)
)

)
(:action Flag245Action-19
:parameters ()
:precondition
(and
(Flag240-)
(Flag240prime-)
)
:effect
(and
(Flag245-)
(Flag245prime-)
)

)
(:action Flag245Action-20
:parameters ()
:precondition
(and
(Flag204-)
(Flag204prime-)
)
:effect
(and
(Flag245-)
(Flag245prime-)
)

)
(:action Flag245Action-21
:parameters ()
:precondition
(and
(Flag119-)
(Flag119prime-)
)
:effect
(and
(Flag245-)
(Flag245prime-)
)

)
(:action Flag245Action-22
:parameters ()
:precondition
(and
(Flag218-)
(Flag218prime-)
)
:effect
(and
(Flag245-)
(Flag245prime-)
)

)
(:action Flag245Action-23
:parameters ()
:precondition
(and
(Flag210-)
(Flag210prime-)
)
:effect
(and
(Flag245-)
(Flag245prime-)
)

)
(:action Flag245Action-24
:parameters ()
:precondition
(and
(Flag108-)
(Flag108prime-)
)
:effect
(and
(Flag245-)
(Flag245prime-)
)

)
(:action Flag245Action-25
:parameters ()
:precondition
(and
(Flag130-)
(Flag130prime-)
)
:effect
(and
(Flag245-)
(Flag245prime-)
)

)
(:action Flag245Action-26
:parameters ()
:precondition
(and
(Flag134-)
(Flag134prime-)
)
:effect
(and
(Flag245-)
(Flag245prime-)
)

)
(:action Flag245Action-27
:parameters ()
:precondition
(and
(Flag160-)
(Flag160prime-)
)
:effect
(and
(Flag245-)
(Flag245prime-)
)

)
(:action Flag245Action-28
:parameters ()
:precondition
(and
(Flag53-)
(Flag53prime-)
)
:effect
(and
(Flag245-)
(Flag245prime-)
)

)
(:action Flag245Action-29
:parameters ()
:precondition
(and
(Flag226-)
(Flag226prime-)
)
:effect
(and
(Flag245-)
(Flag245prime-)
)

)
(:action Flag245Action-30
:parameters ()
:precondition
(and
(Flag157-)
(Flag157prime-)
)
:effect
(and
(Flag245-)
(Flag245prime-)
)

)
(:action Flag245Action-31
:parameters ()
:precondition
(and
(Flag102-)
(Flag102prime-)
)
:effect
(and
(Flag245-)
(Flag245prime-)
)

)
(:action Flag245Action-32
:parameters ()
:precondition
(and
(Flag57-)
(Flag57prime-)
)
:effect
(and
(Flag245-)
(Flag245prime-)
)

)
(:action Flag245Action-33
:parameters ()
:precondition
(and
(Flag166-)
(Flag166prime-)
)
:effect
(and
(Flag245-)
(Flag245prime-)
)

)
(:action Flag245Action-34
:parameters ()
:precondition
(and
(Flag219-)
(Flag219prime-)
)
:effect
(and
(Flag245-)
(Flag245prime-)
)

)
(:action Flag245Action-35
:parameters ()
:precondition
(and
(Flag214-)
(Flag214prime-)
)
:effect
(and
(Flag245-)
(Flag245prime-)
)

)
(:action Flag245Action-36
:parameters ()
:precondition
(and
(Flag206-)
(Flag206prime-)
)
:effect
(and
(Flag245-)
(Flag245prime-)
)

)
(:action Flag245Action-37
:parameters ()
:precondition
(and
(Flag140-)
(Flag140prime-)
)
:effect
(and
(Flag245-)
(Flag245prime-)
)

)
(:action Flag245Action-38
:parameters ()
:precondition
(and
(Flag241-)
(Flag241prime-)
)
:effect
(and
(Flag245-)
(Flag245prime-)
)

)
(:action Flag245Action-39
:parameters ()
:precondition
(and
(Flag220-)
(Flag220prime-)
)
:effect
(and
(Flag245-)
(Flag245prime-)
)

)
(:action Flag245Action-40
:parameters ()
:precondition
(and
(Flag81-)
(Flag81prime-)
)
:effect
(and
(Flag245-)
(Flag245prime-)
)

)
(:action Flag245Action-41
:parameters ()
:precondition
(and
(Flag183-)
(Flag183prime-)
)
:effect
(and
(Flag245-)
(Flag245prime-)
)

)
(:action Flag245Action-42
:parameters ()
:precondition
(and
(Flag242-)
(Flag242prime-)
)
:effect
(and
(Flag245-)
(Flag245prime-)
)

)
(:action Flag245Action-43
:parameters ()
:precondition
(and
(Flag123-)
(Flag123prime-)
)
:effect
(and
(Flag245-)
(Flag245prime-)
)

)
(:action Flag245Action-44
:parameters ()
:precondition
(and
(Flag161-)
(Flag161prime-)
)
:effect
(and
(Flag245-)
(Flag245prime-)
)

)
(:action Flag245Action-45
:parameters ()
:precondition
(and
(Flag193-)
(Flag193prime-)
)
:effect
(and
(Flag245-)
(Flag245prime-)
)

)
(:action Flag245Action-46
:parameters ()
:precondition
(and
(Flag235-)
(Flag235prime-)
)
:effect
(and
(Flag245-)
(Flag245prime-)
)

)
(:action Flag245Action-47
:parameters ()
:precondition
(and
(Flag74-)
(Flag74prime-)
)
:effect
(and
(Flag245-)
(Flag245prime-)
)

)
(:action Flag245Action-48
:parameters ()
:precondition
(and
(Flag236-)
(Flag236prime-)
)
:effect
(and
(Flag245-)
(Flag245prime-)
)

)
(:action Flag245Action-49
:parameters ()
:precondition
(and
(Flag87-)
(Flag87prime-)
)
:effect
(and
(Flag245-)
(Flag245prime-)
)

)
(:action Flag245Action-50
:parameters ()
:precondition
(and
(Flag58-)
(Flag58prime-)
)
:effect
(and
(Flag245-)
(Flag245prime-)
)

)
(:action Flag245Action-51
:parameters ()
:precondition
(and
(Flag66-)
(Flag66prime-)
)
:effect
(and
(Flag245-)
(Flag245prime-)
)

)
(:action Flag245Action-52
:parameters ()
:precondition
(and
(Flag189-)
(Flag189prime-)
)
:effect
(and
(Flag245-)
(Flag245prime-)
)

)
(:action Flag245Action-53
:parameters ()
:precondition
(and
(Flag143-)
(Flag143prime-)
)
:effect
(and
(Flag245-)
(Flag245prime-)
)

)
(:action Flag245Action-54
:parameters ()
:precondition
(and
(Flag90-)
(Flag90prime-)
)
:effect
(and
(Flag245-)
(Flag245prime-)
)

)
(:action Flag245Action-55
:parameters ()
:precondition
(and
(Flag101-)
(Flag101prime-)
)
:effect
(and
(Flag245-)
(Flag245prime-)
)

)
(:action Flag245Action-56
:parameters ()
:precondition
(and
(Flag162-)
(Flag162prime-)
)
:effect
(and
(Flag245-)
(Flag245prime-)
)

)
(:action Flag245Action-57
:parameters ()
:precondition
(and
(Flag243-)
(Flag243prime-)
)
:effect
(and
(Flag245-)
(Flag245prime-)
)

)
(:action Flag245Action-58
:parameters ()
:precondition
(and
(Flag79-)
(Flag79prime-)
)
:effect
(and
(Flag245-)
(Flag245prime-)
)

)
(:action Flag245Action-59
:parameters ()
:precondition
(and
(Flag92-)
(Flag92prime-)
)
:effect
(and
(Flag245-)
(Flag245prime-)
)

)
(:action Flag245Action-60
:parameters ()
:precondition
(and
(Flag200-)
(Flag200prime-)
)
:effect
(and
(Flag245-)
(Flag245prime-)
)

)
(:action Flag245Action-61
:parameters ()
:precondition
(and
(Flag187-)
(Flag187prime-)
)
:effect
(and
(Flag245-)
(Flag245prime-)
)

)
(:action Flag245Action-62
:parameters ()
:precondition
(and
(Flag194-)
(Flag194prime-)
)
:effect
(and
(Flag245-)
(Flag245prime-)
)

)
(:action Flag245Action-63
:parameters ()
:precondition
(and
(Flag237-)
(Flag237prime-)
)
:effect
(and
(Flag245-)
(Flag245prime-)
)

)
(:action Flag245Action-64
:parameters ()
:precondition
(and
(Flag234-)
(Flag234prime-)
)
:effect
(and
(Flag245-)
(Flag245prime-)
)

)
(:action Flag245Action-65
:parameters ()
:precondition
(and
(Flag201-)
(Flag201prime-)
)
:effect
(and
(Flag245-)
(Flag245prime-)
)

)
(:action Flag245Action-66
:parameters ()
:precondition
(and
(Flag211-)
(Flag211prime-)
)
:effect
(and
(Flag245-)
(Flag245prime-)
)

)
(:action Flag245Action-67
:parameters ()
:precondition
(and
(Flag230-)
(Flag230prime-)
)
:effect
(and
(Flag245-)
(Flag245prime-)
)

)
(:action Flag245Action-68
:parameters ()
:precondition
(and
(Flag169-)
(Flag169prime-)
)
:effect
(and
(Flag245-)
(Flag245prime-)
)

)
(:action Flag245Action-69
:parameters ()
:precondition
(and
(Flag153-)
(Flag153prime-)
)
:effect
(and
(Flag245-)
(Flag245prime-)
)

)
(:action Flag245Action-70
:parameters ()
:precondition
(and
(Flag176-)
(Flag176prime-)
)
:effect
(and
(Flag245-)
(Flag245prime-)
)

)
(:action Flag245Action-71
:parameters ()
:precondition
(and
(Flag190-)
(Flag190prime-)
)
:effect
(and
(Flag245-)
(Flag245prime-)
)

)
(:action Flag245Action-72
:parameters ()
:precondition
(and
(Flag244-)
(Flag244prime-)
)
:effect
(and
(Flag245-)
(Flag245prime-)
)

)
(:action Flag245Action-73
:parameters ()
:precondition
(and
(Flag118-)
(Flag118prime-)
)
:effect
(and
(Flag245-)
(Flag245prime-)
)

)
(:action Flag245Action-74
:parameters ()
:precondition
(and
(Flag137-)
(Flag137prime-)
)
:effect
(and
(Flag245-)
(Flag245prime-)
)

)
(:action Flag245Action-75
:parameters ()
:precondition
(and
(Flag216-)
(Flag216prime-)
)
:effect
(and
(Flag245-)
(Flag245prime-)
)

)
(:action Flag245Action-76
:parameters ()
:precondition
(and
(Flag228-)
(Flag228prime-)
)
:effect
(and
(Flag245-)
(Flag245prime-)
)

)
(:action Prim245Action
:parameters ()
:precondition
(and
(not (Flag208-))
(Flag208prime-)
(not (Flag96-))
(Flag96prime-)
(not (Flag116-))
(Flag116prime-)
(not (Flag41-))
(Flag41prime-)
(not (Flag132-))
(Flag132prime-)
(not (Flag146-))
(Flag146prime-)
(not (Flag47-))
(Flag47prime-)
(not (Flag232-))
(Flag232prime-)
(not (Flag46-))
(Flag46prime-)
(not (Flag73-))
(Flag73prime-)
(not (Flag203-))
(Flag203prime-)
(not (Flag185-))
(Flag185prime-)
(not (Flag188-))
(Flag188prime-)
(not (Flag98-))
(Flag98prime-)
(not (Flag172-))
(Flag172prime-)
(not (Flag239-))
(Flag239prime-)
(not (Flag40-))
(Flag40prime-)
(not (Flag223-))
(Flag223prime-)
(not (Flag225-))
(Flag225prime-)
(not (Flag240-))
(Flag240prime-)
(not (Flag204-))
(Flag204prime-)
(not (Flag119-))
(Flag119prime-)
(not (Flag218-))
(Flag218prime-)
(not (Flag210-))
(Flag210prime-)
(not (Flag108-))
(Flag108prime-)
(not (Flag130-))
(Flag130prime-)
(not (Flag134-))
(Flag134prime-)
(not (Flag160-))
(Flag160prime-)
(not (Flag53-))
(Flag53prime-)
(not (Flag226-))
(Flag226prime-)
(not (Flag157-))
(Flag157prime-)
(not (Flag102-))
(Flag102prime-)
(not (Flag57-))
(Flag57prime-)
(not (Flag166-))
(Flag166prime-)
(not (Flag219-))
(Flag219prime-)
(not (Flag214-))
(Flag214prime-)
(not (Flag206-))
(Flag206prime-)
(not (Flag140-))
(Flag140prime-)
(not (Flag241-))
(Flag241prime-)
(not (Flag220-))
(Flag220prime-)
(not (Flag81-))
(Flag81prime-)
(not (Flag183-))
(Flag183prime-)
(not (Flag242-))
(Flag242prime-)
(not (Flag123-))
(Flag123prime-)
(not (Flag161-))
(Flag161prime-)
(not (Flag193-))
(Flag193prime-)
(not (Flag235-))
(Flag235prime-)
(not (Flag74-))
(Flag74prime-)
(not (Flag236-))
(Flag236prime-)
(not (Flag87-))
(Flag87prime-)
(not (Flag58-))
(Flag58prime-)
(not (Flag66-))
(Flag66prime-)
(not (Flag189-))
(Flag189prime-)
(not (Flag143-))
(Flag143prime-)
(not (Flag90-))
(Flag90prime-)
(not (Flag101-))
(Flag101prime-)
(not (Flag162-))
(Flag162prime-)
(not (Flag243-))
(Flag243prime-)
(not (Flag79-))
(Flag79prime-)
(not (Flag92-))
(Flag92prime-)
(not (Flag200-))
(Flag200prime-)
(not (Flag187-))
(Flag187prime-)
(not (Flag194-))
(Flag194prime-)
(not (Flag237-))
(Flag237prime-)
(not (Flag234-))
(Flag234prime-)
(not (Flag201-))
(Flag201prime-)
(not (Flag211-))
(Flag211prime-)
(not (Flag230-))
(Flag230prime-)
(not (Flag169-))
(Flag169prime-)
(not (Flag153-))
(Flag153prime-)
(not (Flag176-))
(Flag176prime-)
(not (Flag190-))
(Flag190prime-)
(not (Flag244-))
(Flag244prime-)
(not (Flag118-))
(Flag118prime-)
(not (Flag137-))
(Flag137prime-)
(not (Flag216-))
(Flag216prime-)
(not (Flag228-))
(Flag228prime-)
)
:effect
(and
(Flag245prime-)
(not (Flag245-))
)

)
(:action Flag251Action-0
:parameters ()
:precondition
(and
(Flag40-)
(Flag40prime-)
)
:effect
(and
(Flag251-)
(Flag251prime-)
)

)
(:action Flag251Action-1
:parameters ()
:precondition
(and
(Flag96-)
(Flag96prime-)
)
:effect
(and
(Flag251-)
(Flag251prime-)
)

)
(:action Flag251Action-2
:parameters ()
:precondition
(and
(Flag116-)
(Flag116prime-)
)
:effect
(and
(Flag251-)
(Flag251prime-)
)

)
(:action Flag251Action-3
:parameters ()
:precondition
(and
(Flag132-)
(Flag132prime-)
)
:effect
(and
(Flag251-)
(Flag251prime-)
)

)
(:action Flag251Action-4
:parameters ()
:precondition
(and
(Flag146-)
(Flag146prime-)
)
:effect
(and
(Flag251-)
(Flag251prime-)
)

)
(:action Flag251Action-5
:parameters ()
:precondition
(and
(Flag47-)
(Flag47prime-)
)
:effect
(and
(Flag251-)
(Flag251prime-)
)

)
(:action Flag251Action-6
:parameters ()
:precondition
(and
(Flag46-)
(Flag46prime-)
)
:effect
(and
(Flag251-)
(Flag251prime-)
)

)
(:action Flag251Action-7
:parameters ()
:precondition
(and
(Flag246-)
(Flag246prime-)
)
:effect
(and
(Flag251-)
(Flag251prime-)
)

)
(:action Flag251Action-8
:parameters ()
:precondition
(and
(Flag203-)
(Flag203prime-)
)
:effect
(and
(Flag251-)
(Flag251prime-)
)

)
(:action Flag251Action-9
:parameters ()
:precondition
(and
(Flag185-)
(Flag185prime-)
)
:effect
(and
(Flag251-)
(Flag251prime-)
)

)
(:action Flag251Action-10
:parameters ()
:precondition
(and
(Flag247-)
(Flag247prime-)
)
:effect
(and
(Flag251-)
(Flag251prime-)
)

)
(:action Flag251Action-11
:parameters ()
:precondition
(and
(Flag118-)
(Flag118prime-)
)
:effect
(and
(Flag251-)
(Flag251prime-)
)

)
(:action Flag251Action-12
:parameters ()
:precondition
(and
(Flag172-)
(Flag172prime-)
)
:effect
(and
(Flag251-)
(Flag251prime-)
)

)
(:action Flag251Action-13
:parameters ()
:precondition
(and
(Flag239-)
(Flag239prime-)
)
:effect
(and
(Flag251-)
(Flag251prime-)
)

)
(:action Flag251Action-14
:parameters ()
:precondition
(and
(Flag248-)
(Flag248prime-)
)
:effect
(and
(Flag251-)
(Flag251prime-)
)

)
(:action Flag251Action-15
:parameters ()
:precondition
(and
(Flag225-)
(Flag225prime-)
)
:effect
(and
(Flag251-)
(Flag251prime-)
)

)
(:action Flag251Action-16
:parameters ()
:precondition
(and
(Flag220-)
(Flag220prime-)
)
:effect
(and
(Flag251-)
(Flag251prime-)
)

)
(:action Flag251Action-17
:parameters ()
:precondition
(and
(Flag204-)
(Flag204prime-)
)
:effect
(and
(Flag251-)
(Flag251prime-)
)

)
(:action Flag251Action-18
:parameters ()
:precondition
(and
(Flag119-)
(Flag119prime-)
)
:effect
(and
(Flag251-)
(Flag251prime-)
)

)
(:action Flag251Action-19
:parameters ()
:precondition
(and
(Flag218-)
(Flag218prime-)
)
:effect
(and
(Flag251-)
(Flag251prime-)
)

)
(:action Flag251Action-20
:parameters ()
:precondition
(and
(Flag210-)
(Flag210prime-)
)
:effect
(and
(Flag251-)
(Flag251prime-)
)

)
(:action Flag251Action-21
:parameters ()
:precondition
(and
(Flag108-)
(Flag108prime-)
)
:effect
(and
(Flag251-)
(Flag251prime-)
)

)
(:action Flag251Action-22
:parameters ()
:precondition
(and
(Flag134-)
(Flag134prime-)
)
:effect
(and
(Flag251-)
(Flag251prime-)
)

)
(:action Flag251Action-23
:parameters ()
:precondition
(and
(Flag160-)
(Flag160prime-)
)
:effect
(and
(Flag251-)
(Flag251prime-)
)

)
(:action Flag251Action-24
:parameters ()
:precondition
(and
(Flag53-)
(Flag53prime-)
)
:effect
(and
(Flag251-)
(Flag251prime-)
)

)
(:action Flag251Action-25
:parameters ()
:precondition
(and
(Flag157-)
(Flag157prime-)
)
:effect
(and
(Flag251-)
(Flag251prime-)
)

)
(:action Flag251Action-26
:parameters ()
:precondition
(and
(Flag57-)
(Flag57prime-)
)
:effect
(and
(Flag251-)
(Flag251prime-)
)

)
(:action Flag251Action-27
:parameters ()
:precondition
(and
(Flag87-)
(Flag87prime-)
)
:effect
(and
(Flag251-)
(Flag251prime-)
)

)
(:action Flag251Action-28
:parameters ()
:precondition
(and
(Flag219-)
(Flag219prime-)
)
:effect
(and
(Flag251-)
(Flag251prime-)
)

)
(:action Flag251Action-29
:parameters ()
:precondition
(and
(Flag41-)
(Flag41prime-)
)
:effect
(and
(Flag251-)
(Flag251prime-)
)

)
(:action Flag251Action-30
:parameters ()
:precondition
(and
(Flag140-)
(Flag140prime-)
)
:effect
(and
(Flag251-)
(Flag251prime-)
)

)
(:action Flag251Action-31
:parameters ()
:precondition
(and
(Flag241-)
(Flag241prime-)
)
:effect
(and
(Flag251-)
(Flag251prime-)
)

)
(:action Flag251Action-32
:parameters ()
:precondition
(and
(Flag216-)
(Flag216prime-)
)
:effect
(and
(Flag251-)
(Flag251prime-)
)

)
(:action Flag251Action-33
:parameters ()
:precondition
(and
(Flag81-)
(Flag81prime-)
)
:effect
(and
(Flag251-)
(Flag251prime-)
)

)
(:action Flag251Action-34
:parameters ()
:precondition
(and
(Flag242-)
(Flag242prime-)
)
:effect
(and
(Flag251-)
(Flag251prime-)
)

)
(:action Flag251Action-35
:parameters ()
:precondition
(and
(Flag123-)
(Flag123prime-)
)
:effect
(and
(Flag251-)
(Flag251prime-)
)

)
(:action Flag251Action-36
:parameters ()
:precondition
(and
(Flag161-)
(Flag161prime-)
)
:effect
(and
(Flag251-)
(Flag251prime-)
)

)
(:action Flag251Action-37
:parameters ()
:precondition
(and
(Flag193-)
(Flag193prime-)
)
:effect
(and
(Flag251-)
(Flag251prime-)
)

)
(:action Flag251Action-38
:parameters ()
:precondition
(and
(Flag235-)
(Flag235prime-)
)
:effect
(and
(Flag251-)
(Flag251prime-)
)

)
(:action Flag251Action-39
:parameters ()
:precondition
(and
(Flag74-)
(Flag74prime-)
)
:effect
(and
(Flag251-)
(Flag251prime-)
)

)
(:action Flag251Action-40
:parameters ()
:precondition
(and
(Flag236-)
(Flag236prime-)
)
:effect
(and
(Flag251-)
(Flag251prime-)
)

)
(:action Flag251Action-41
:parameters ()
:precondition
(and
(Flag166-)
(Flag166prime-)
)
:effect
(and
(Flag251-)
(Flag251prime-)
)

)
(:action Flag251Action-42
:parameters ()
:precondition
(and
(Flag92-)
(Flag92prime-)
)
:effect
(and
(Flag251-)
(Flag251prime-)
)

)
(:action Flag251Action-43
:parameters ()
:precondition
(and
(Flag66-)
(Flag66prime-)
)
:effect
(and
(Flag251-)
(Flag251prime-)
)

)
(:action Flag251Action-44
:parameters ()
:precondition
(and
(Flag189-)
(Flag189prime-)
)
:effect
(and
(Flag251-)
(Flag251prime-)
)

)
(:action Flag251Action-45
:parameters ()
:precondition
(and
(Flag240-)
(Flag240prime-)
)
:effect
(and
(Flag251-)
(Flag251prime-)
)

)
(:action Flag251Action-46
:parameters ()
:precondition
(and
(Flag90-)
(Flag90prime-)
)
:effect
(and
(Flag251-)
(Flag251prime-)
)

)
(:action Flag251Action-47
:parameters ()
:precondition
(and
(Flag101-)
(Flag101prime-)
)
:effect
(and
(Flag251-)
(Flag251prime-)
)

)
(:action Flag251Action-48
:parameters ()
:precondition
(and
(Flag249-)
(Flag249prime-)
)
:effect
(and
(Flag251-)
(Flag251prime-)
)

)
(:action Flag251Action-49
:parameters ()
:precondition
(and
(Flag200-)
(Flag200prime-)
)
:effect
(and
(Flag251-)
(Flag251prime-)
)

)
(:action Flag251Action-50
:parameters ()
:precondition
(and
(Flag187-)
(Flag187prime-)
)
:effect
(and
(Flag251-)
(Flag251prime-)
)

)
(:action Flag251Action-51
:parameters ()
:precondition
(and
(Flag194-)
(Flag194prime-)
)
:effect
(and
(Flag251-)
(Flag251prime-)
)

)
(:action Flag251Action-52
:parameters ()
:precondition
(and
(Flag237-)
(Flag237prime-)
)
:effect
(and
(Flag251-)
(Flag251prime-)
)

)
(:action Flag251Action-53
:parameters ()
:precondition
(and
(Flag234-)
(Flag234prime-)
)
:effect
(and
(Flag251-)
(Flag251prime-)
)

)
(:action Flag251Action-54
:parameters ()
:precondition
(and
(Flag201-)
(Flag201prime-)
)
:effect
(and
(Flag251-)
(Flag251prime-)
)

)
(:action Flag251Action-55
:parameters ()
:precondition
(and
(Flag223-)
(Flag223prime-)
)
:effect
(and
(Flag251-)
(Flag251prime-)
)

)
(:action Flag251Action-56
:parameters ()
:precondition
(and
(Flag211-)
(Flag211prime-)
)
:effect
(and
(Flag251-)
(Flag251prime-)
)

)
(:action Flag251Action-57
:parameters ()
:precondition
(and
(Flag188-)
(Flag188prime-)
)
:effect
(and
(Flag251-)
(Flag251prime-)
)

)
(:action Flag251Action-58
:parameters ()
:precondition
(and
(Flag230-)
(Flag230prime-)
)
:effect
(and
(Flag251-)
(Flag251prime-)
)

)
(:action Flag251Action-59
:parameters ()
:precondition
(and
(Flag169-)
(Flag169prime-)
)
:effect
(and
(Flag251-)
(Flag251prime-)
)

)
(:action Flag251Action-60
:parameters ()
:precondition
(and
(Flag153-)
(Flag153prime-)
)
:effect
(and
(Flag251-)
(Flag251prime-)
)

)
(:action Flag251Action-61
:parameters ()
:precondition
(and
(Flag176-)
(Flag176prime-)
)
:effect
(and
(Flag251-)
(Flag251prime-)
)

)
(:action Flag251Action-62
:parameters ()
:precondition
(and
(Flag143-)
(Flag143prime-)
)
:effect
(and
(Flag251-)
(Flag251prime-)
)

)
(:action Flag251Action-63
:parameters ()
:precondition
(and
(Flag98-)
(Flag98prime-)
)
:effect
(and
(Flag251-)
(Flag251prime-)
)

)
(:action Flag251Action-64
:parameters ()
:precondition
(and
(Flag250-)
(Flag250prime-)
)
:effect
(and
(Flag251-)
(Flag251prime-)
)

)
(:action Flag251Action-65
:parameters ()
:precondition
(and
(Flag228-)
(Flag228prime-)
)
:effect
(and
(Flag251-)
(Flag251prime-)
)

)
(:action Prim251Action
:parameters ()
:precondition
(and
(not (Flag40-))
(Flag40prime-)
(not (Flag96-))
(Flag96prime-)
(not (Flag116-))
(Flag116prime-)
(not (Flag132-))
(Flag132prime-)
(not (Flag146-))
(Flag146prime-)
(not (Flag47-))
(Flag47prime-)
(not (Flag46-))
(Flag46prime-)
(not (Flag246-))
(Flag246prime-)
(not (Flag203-))
(Flag203prime-)
(not (Flag185-))
(Flag185prime-)
(not (Flag247-))
(Flag247prime-)
(not (Flag118-))
(Flag118prime-)
(not (Flag172-))
(Flag172prime-)
(not (Flag239-))
(Flag239prime-)
(not (Flag248-))
(Flag248prime-)
(not (Flag225-))
(Flag225prime-)
(not (Flag220-))
(Flag220prime-)
(not (Flag204-))
(Flag204prime-)
(not (Flag119-))
(Flag119prime-)
(not (Flag218-))
(Flag218prime-)
(not (Flag210-))
(Flag210prime-)
(not (Flag108-))
(Flag108prime-)
(not (Flag134-))
(Flag134prime-)
(not (Flag160-))
(Flag160prime-)
(not (Flag53-))
(Flag53prime-)
(not (Flag157-))
(Flag157prime-)
(not (Flag57-))
(Flag57prime-)
(not (Flag87-))
(Flag87prime-)
(not (Flag219-))
(Flag219prime-)
(not (Flag41-))
(Flag41prime-)
(not (Flag140-))
(Flag140prime-)
(not (Flag241-))
(Flag241prime-)
(not (Flag216-))
(Flag216prime-)
(not (Flag81-))
(Flag81prime-)
(not (Flag242-))
(Flag242prime-)
(not (Flag123-))
(Flag123prime-)
(not (Flag161-))
(Flag161prime-)
(not (Flag193-))
(Flag193prime-)
(not (Flag235-))
(Flag235prime-)
(not (Flag74-))
(Flag74prime-)
(not (Flag236-))
(Flag236prime-)
(not (Flag166-))
(Flag166prime-)
(not (Flag92-))
(Flag92prime-)
(not (Flag66-))
(Flag66prime-)
(not (Flag189-))
(Flag189prime-)
(not (Flag240-))
(Flag240prime-)
(not (Flag90-))
(Flag90prime-)
(not (Flag101-))
(Flag101prime-)
(not (Flag249-))
(Flag249prime-)
(not (Flag200-))
(Flag200prime-)
(not (Flag187-))
(Flag187prime-)
(not (Flag194-))
(Flag194prime-)
(not (Flag237-))
(Flag237prime-)
(not (Flag234-))
(Flag234prime-)
(not (Flag201-))
(Flag201prime-)
(not (Flag223-))
(Flag223prime-)
(not (Flag211-))
(Flag211prime-)
(not (Flag188-))
(Flag188prime-)
(not (Flag230-))
(Flag230prime-)
(not (Flag169-))
(Flag169prime-)
(not (Flag153-))
(Flag153prime-)
(not (Flag176-))
(Flag176prime-)
(not (Flag143-))
(Flag143prime-)
(not (Flag98-))
(Flag98prime-)
(not (Flag250-))
(Flag250prime-)
(not (Flag228-))
(Flag228prime-)
)
:effect
(and
(Flag251prime-)
(not (Flag251-))
)

)
(:action Flag255Action-0
:parameters ()
:precondition
(and
(Flag96-)
(Flag96prime-)
)
:effect
(and
(Flag255-)
(Flag255prime-)
)

)
(:action Flag255Action-1
:parameters ()
:precondition
(and
(Flag41-)
(Flag41prime-)
)
:effect
(and
(Flag255-)
(Flag255prime-)
)

)
(:action Flag255Action-2
:parameters ()
:precondition
(and
(Flag132-)
(Flag132prime-)
)
:effect
(and
(Flag255-)
(Flag255prime-)
)

)
(:action Flag255Action-3
:parameters ()
:precondition
(and
(Flag146-)
(Flag146prime-)
)
:effect
(and
(Flag255-)
(Flag255prime-)
)

)
(:action Flag255Action-4
:parameters ()
:precondition
(and
(Flag46-)
(Flag46prime-)
)
:effect
(and
(Flag255-)
(Flag255prime-)
)

)
(:action Flag255Action-5
:parameters ()
:precondition
(and
(Flag246-)
(Flag246prime-)
)
:effect
(and
(Flag255-)
(Flag255prime-)
)

)
(:action Flag255Action-6
:parameters ()
:precondition
(and
(Flag247-)
(Flag247prime-)
)
:effect
(and
(Flag255-)
(Flag255prime-)
)

)
(:action Flag255Action-7
:parameters ()
:precondition
(and
(Flag118-)
(Flag118prime-)
)
:effect
(and
(Flag255-)
(Flag255prime-)
)

)
(:action Flag255Action-8
:parameters ()
:precondition
(and
(Flag172-)
(Flag172prime-)
)
:effect
(and
(Flag255-)
(Flag255prime-)
)

)
(:action Flag255Action-9
:parameters ()
:precondition
(and
(Flag239-)
(Flag239prime-)
)
:effect
(and
(Flag255-)
(Flag255prime-)
)

)
(:action Flag255Action-10
:parameters ()
:precondition
(and
(Flag223-)
(Flag223prime-)
)
:effect
(and
(Flag255-)
(Flag255prime-)
)

)
(:action Flag255Action-11
:parameters ()
:precondition
(and
(Flag240-)
(Flag240prime-)
)
:effect
(and
(Flag255-)
(Flag255prime-)
)

)
(:action Flag255Action-12
:parameters ()
:precondition
(and
(Flag204-)
(Flag204prime-)
)
:effect
(and
(Flag255-)
(Flag255prime-)
)

)
(:action Flag255Action-13
:parameters ()
:precondition
(and
(Flag119-)
(Flag119prime-)
)
:effect
(and
(Flag255-)
(Flag255prime-)
)

)
(:action Flag255Action-14
:parameters ()
:precondition
(and
(Flag218-)
(Flag218prime-)
)
:effect
(and
(Flag255-)
(Flag255prime-)
)

)
(:action Flag255Action-15
:parameters ()
:precondition
(and
(Flag210-)
(Flag210prime-)
)
:effect
(and
(Flag255-)
(Flag255prime-)
)

)
(:action Flag255Action-16
:parameters ()
:precondition
(and
(Flag92-)
(Flag92prime-)
)
:effect
(and
(Flag255-)
(Flag255prime-)
)

)
(:action Flag255Action-17
:parameters ()
:precondition
(and
(Flag108-)
(Flag108prime-)
)
:effect
(and
(Flag255-)
(Flag255prime-)
)

)
(:action Flag255Action-18
:parameters ()
:precondition
(and
(Flag252-)
(Flag252prime-)
)
:effect
(and
(Flag255-)
(Flag255prime-)
)

)
(:action Flag255Action-19
:parameters ()
:precondition
(and
(Flag160-)
(Flag160prime-)
)
:effect
(and
(Flag255-)
(Flag255prime-)
)

)
(:action Flag255Action-20
:parameters ()
:precondition
(and
(Flag53-)
(Flag53prime-)
)
:effect
(and
(Flag255-)
(Flag255prime-)
)

)
(:action Flag255Action-21
:parameters ()
:precondition
(and
(Flag57-)
(Flag57prime-)
)
:effect
(and
(Flag255-)
(Flag255prime-)
)

)
(:action Flag255Action-22
:parameters ()
:precondition
(and
(Flag166-)
(Flag166prime-)
)
:effect
(and
(Flag255-)
(Flag255prime-)
)

)
(:action Flag255Action-23
:parameters ()
:precondition
(and
(Flag219-)
(Flag219prime-)
)
:effect
(and
(Flag255-)
(Flag255prime-)
)

)
(:action Flag255Action-24
:parameters ()
:precondition
(and
(Flag140-)
(Flag140prime-)
)
:effect
(and
(Flag255-)
(Flag255prime-)
)

)
(:action Flag255Action-25
:parameters ()
:precondition
(and
(Flag241-)
(Flag241prime-)
)
:effect
(and
(Flag255-)
(Flag255prime-)
)

)
(:action Flag255Action-26
:parameters ()
:precondition
(and
(Flag81-)
(Flag81prime-)
)
:effect
(and
(Flag255-)
(Flag255prime-)
)

)
(:action Flag255Action-27
:parameters ()
:precondition
(and
(Flag242-)
(Flag242prime-)
)
:effect
(and
(Flag255-)
(Flag255prime-)
)

)
(:action Flag255Action-28
:parameters ()
:precondition
(and
(Flag123-)
(Flag123prime-)
)
:effect
(and
(Flag255-)
(Flag255prime-)
)

)
(:action Flag255Action-29
:parameters ()
:precondition
(and
(Flag161-)
(Flag161prime-)
)
:effect
(and
(Flag255-)
(Flag255prime-)
)

)
(:action Flag255Action-30
:parameters ()
:precondition
(and
(Flag253-)
(Flag253prime-)
)
:effect
(and
(Flag255-)
(Flag255prime-)
)

)
(:action Flag255Action-31
:parameters ()
:precondition
(and
(Flag193-)
(Flag193prime-)
)
:effect
(and
(Flag255-)
(Flag255prime-)
)

)
(:action Flag255Action-32
:parameters ()
:precondition
(and
(Flag200-)
(Flag200prime-)
)
:effect
(and
(Flag255-)
(Flag255prime-)
)

)
(:action Flag255Action-33
:parameters ()
:precondition
(and
(Flag236-)
(Flag236prime-)
)
:effect
(and
(Flag255-)
(Flag255prime-)
)

)
(:action Flag255Action-34
:parameters ()
:precondition
(and
(Flag254-)
(Flag254prime-)
)
:effect
(and
(Flag255-)
(Flag255prime-)
)

)
(:action Flag255Action-35
:parameters ()
:precondition
(and
(Flag66-)
(Flag66prime-)
)
:effect
(and
(Flag255-)
(Flag255prime-)
)

)
(:action Flag255Action-36
:parameters ()
:precondition
(and
(Flag189-)
(Flag189prime-)
)
:effect
(and
(Flag255-)
(Flag255prime-)
)

)
(:action Flag255Action-37
:parameters ()
:precondition
(and
(Flag74-)
(Flag74prime-)
)
:effect
(and
(Flag255-)
(Flag255prime-)
)

)
(:action Flag255Action-38
:parameters ()
:precondition
(and
(Flag90-)
(Flag90prime-)
)
:effect
(and
(Flag255-)
(Flag255prime-)
)

)
(:action Flag255Action-39
:parameters ()
:precondition
(and
(Flag249-)
(Flag249prime-)
)
:effect
(and
(Flag255-)
(Flag255prime-)
)

)
(:action Flag255Action-40
:parameters ()
:precondition
(and
(Flag235-)
(Flag235prime-)
)
:effect
(and
(Flag255-)
(Flag255prime-)
)

)
(:action Flag255Action-41
:parameters ()
:precondition
(and
(Flag187-)
(Flag187prime-)
)
:effect
(and
(Flag255-)
(Flag255prime-)
)

)
(:action Flag255Action-42
:parameters ()
:precondition
(and
(Flag194-)
(Flag194prime-)
)
:effect
(and
(Flag255-)
(Flag255prime-)
)

)
(:action Flag255Action-43
:parameters ()
:precondition
(and
(Flag234-)
(Flag234prime-)
)
:effect
(and
(Flag255-)
(Flag255prime-)
)

)
(:action Flag255Action-44
:parameters ()
:precondition
(and
(Flag211-)
(Flag211prime-)
)
:effect
(and
(Flag255-)
(Flag255prime-)
)

)
(:action Flag255Action-45
:parameters ()
:precondition
(and
(Flag188-)
(Flag188prime-)
)
:effect
(and
(Flag255-)
(Flag255prime-)
)

)
(:action Flag255Action-46
:parameters ()
:precondition
(and
(Flag230-)
(Flag230prime-)
)
:effect
(and
(Flag255-)
(Flag255prime-)
)

)
(:action Flag255Action-47
:parameters ()
:precondition
(and
(Flag153-)
(Flag153prime-)
)
:effect
(and
(Flag255-)
(Flag255prime-)
)

)
(:action Flag255Action-48
:parameters ()
:precondition
(and
(Flag176-)
(Flag176prime-)
)
:effect
(and
(Flag255-)
(Flag255prime-)
)

)
(:action Flag255Action-49
:parameters ()
:precondition
(and
(Flag143-)
(Flag143prime-)
)
:effect
(and
(Flag255-)
(Flag255prime-)
)

)
(:action Flag255Action-50
:parameters ()
:precondition
(and
(Flag98-)
(Flag98prime-)
)
:effect
(and
(Flag255-)
(Flag255prime-)
)

)
(:action Flag255Action-51
:parameters ()
:precondition
(and
(Flag216-)
(Flag216prime-)
)
:effect
(and
(Flag255-)
(Flag255prime-)
)

)
(:action Flag255Action-52
:parameters ()
:precondition
(and
(Flag228-)
(Flag228prime-)
)
:effect
(and
(Flag255-)
(Flag255prime-)
)

)
(:action Prim255Action
:parameters ()
:precondition
(and
(not (Flag96-))
(Flag96prime-)
(not (Flag41-))
(Flag41prime-)
(not (Flag132-))
(Flag132prime-)
(not (Flag146-))
(Flag146prime-)
(not (Flag46-))
(Flag46prime-)
(not (Flag246-))
(Flag246prime-)
(not (Flag247-))
(Flag247prime-)
(not (Flag118-))
(Flag118prime-)
(not (Flag172-))
(Flag172prime-)
(not (Flag239-))
(Flag239prime-)
(not (Flag223-))
(Flag223prime-)
(not (Flag240-))
(Flag240prime-)
(not (Flag204-))
(Flag204prime-)
(not (Flag119-))
(Flag119prime-)
(not (Flag218-))
(Flag218prime-)
(not (Flag210-))
(Flag210prime-)
(not (Flag92-))
(Flag92prime-)
(not (Flag108-))
(Flag108prime-)
(not (Flag252-))
(Flag252prime-)
(not (Flag160-))
(Flag160prime-)
(not (Flag53-))
(Flag53prime-)
(not (Flag57-))
(Flag57prime-)
(not (Flag166-))
(Flag166prime-)
(not (Flag219-))
(Flag219prime-)
(not (Flag140-))
(Flag140prime-)
(not (Flag241-))
(Flag241prime-)
(not (Flag81-))
(Flag81prime-)
(not (Flag242-))
(Flag242prime-)
(not (Flag123-))
(Flag123prime-)
(not (Flag161-))
(Flag161prime-)
(not (Flag253-))
(Flag253prime-)
(not (Flag193-))
(Flag193prime-)
(not (Flag200-))
(Flag200prime-)
(not (Flag236-))
(Flag236prime-)
(not (Flag254-))
(Flag254prime-)
(not (Flag66-))
(Flag66prime-)
(not (Flag189-))
(Flag189prime-)
(not (Flag74-))
(Flag74prime-)
(not (Flag90-))
(Flag90prime-)
(not (Flag249-))
(Flag249prime-)
(not (Flag235-))
(Flag235prime-)
(not (Flag187-))
(Flag187prime-)
(not (Flag194-))
(Flag194prime-)
(not (Flag234-))
(Flag234prime-)
(not (Flag211-))
(Flag211prime-)
(not (Flag188-))
(Flag188prime-)
(not (Flag230-))
(Flag230prime-)
(not (Flag153-))
(Flag153prime-)
(not (Flag176-))
(Flag176prime-)
(not (Flag143-))
(Flag143prime-)
(not (Flag98-))
(Flag98prime-)
(not (Flag216-))
(Flag216prime-)
(not (Flag228-))
(Flag228prime-)
)
:effect
(and
(Flag255prime-)
(not (Flag255-))
)

)
(:action Flag258Action-0
:parameters ()
:precondition
(and
(Flag96-)
(Flag96prime-)
)
:effect
(and
(Flag258-)
(Flag258prime-)
)

)
(:action Flag258Action-1
:parameters ()
:precondition
(and
(Flag41-)
(Flag41prime-)
)
:effect
(and
(Flag258-)
(Flag258prime-)
)

)
(:action Flag258Action-2
:parameters ()
:precondition
(and
(Flag132-)
(Flag132prime-)
)
:effect
(and
(Flag258-)
(Flag258prime-)
)

)
(:action Flag258Action-3
:parameters ()
:precondition
(and
(Flag46-)
(Flag46prime-)
)
:effect
(and
(Flag258-)
(Flag258prime-)
)

)
(:action Flag258Action-4
:parameters ()
:precondition
(and
(Flag246-)
(Flag246prime-)
)
:effect
(and
(Flag258-)
(Flag258prime-)
)

)
(:action Flag258Action-5
:parameters ()
:precondition
(and
(Flag247-)
(Flag247prime-)
)
:effect
(and
(Flag258-)
(Flag258prime-)
)

)
(:action Flag258Action-6
:parameters ()
:precondition
(and
(Flag118-)
(Flag118prime-)
)
:effect
(and
(Flag258-)
(Flag258prime-)
)

)
(:action Flag258Action-7
:parameters ()
:precondition
(and
(Flag239-)
(Flag239prime-)
)
:effect
(and
(Flag258-)
(Flag258prime-)
)

)
(:action Flag258Action-8
:parameters ()
:precondition
(and
(Flag223-)
(Flag223prime-)
)
:effect
(and
(Flag258-)
(Flag258prime-)
)

)
(:action Flag258Action-9
:parameters ()
:precondition
(and
(Flag153-)
(Flag153prime-)
)
:effect
(and
(Flag258-)
(Flag258prime-)
)

)
(:action Flag258Action-10
:parameters ()
:precondition
(and
(Flag210-)
(Flag210prime-)
)
:effect
(and
(Flag258-)
(Flag258prime-)
)

)
(:action Flag258Action-11
:parameters ()
:precondition
(and
(Flag256-)
(Flag256prime-)
)
:effect
(and
(Flag258-)
(Flag258prime-)
)

)
(:action Flag258Action-12
:parameters ()
:precondition
(and
(Flag172-)
(Flag172prime-)
)
:effect
(and
(Flag258-)
(Flag258prime-)
)

)
(:action Flag258Action-13
:parameters ()
:precondition
(and
(Flag252-)
(Flag252prime-)
)
:effect
(and
(Flag258-)
(Flag258prime-)
)

)
(:action Flag258Action-14
:parameters ()
:precondition
(and
(Flag160-)
(Flag160prime-)
)
:effect
(and
(Flag258-)
(Flag258prime-)
)

)
(:action Flag258Action-15
:parameters ()
:precondition
(and
(Flag57-)
(Flag57prime-)
)
:effect
(and
(Flag258-)
(Flag258prime-)
)

)
(:action Flag258Action-16
:parameters ()
:precondition
(and
(Flag219-)
(Flag219prime-)
)
:effect
(and
(Flag258-)
(Flag258prime-)
)

)
(:action Flag258Action-17
:parameters ()
:precondition
(and
(Flag140-)
(Flag140prime-)
)
:effect
(and
(Flag258-)
(Flag258prime-)
)

)
(:action Flag258Action-18
:parameters ()
:precondition
(and
(Flag241-)
(Flag241prime-)
)
:effect
(and
(Flag258-)
(Flag258prime-)
)

)
(:action Flag258Action-19
:parameters ()
:precondition
(and
(Flag119-)
(Flag119prime-)
)
:effect
(and
(Flag258-)
(Flag258prime-)
)

)
(:action Flag258Action-20
:parameters ()
:precondition
(and
(Flag193-)
(Flag193prime-)
)
:effect
(and
(Flag258-)
(Flag258prime-)
)

)
(:action Flag258Action-21
:parameters ()
:precondition
(and
(Flag236-)
(Flag236prime-)
)
:effect
(and
(Flag258-)
(Flag258prime-)
)

)
(:action Flag258Action-22
:parameters ()
:precondition
(and
(Flag254-)
(Flag254prime-)
)
:effect
(and
(Flag258-)
(Flag258prime-)
)

)
(:action Flag258Action-23
:parameters ()
:precondition
(and
(Flag66-)
(Flag66prime-)
)
:effect
(and
(Flag258-)
(Flag258prime-)
)

)
(:action Flag258Action-24
:parameters ()
:precondition
(and
(Flag189-)
(Flag189prime-)
)
:effect
(and
(Flag258-)
(Flag258prime-)
)

)
(:action Flag258Action-25
:parameters ()
:precondition
(and
(Flag240-)
(Flag240prime-)
)
:effect
(and
(Flag258-)
(Flag258prime-)
)

)
(:action Flag258Action-26
:parameters ()
:precondition
(and
(Flag90-)
(Flag90prime-)
)
:effect
(and
(Flag258-)
(Flag258prime-)
)

)
(:action Flag258Action-27
:parameters ()
:precondition
(and
(Flag92-)
(Flag92prime-)
)
:effect
(and
(Flag258-)
(Flag258prime-)
)

)
(:action Flag258Action-28
:parameters ()
:precondition
(and
(Flag187-)
(Flag187prime-)
)
:effect
(and
(Flag258-)
(Flag258prime-)
)

)
(:action Flag258Action-29
:parameters ()
:precondition
(and
(Flag194-)
(Flag194prime-)
)
:effect
(and
(Flag258-)
(Flag258prime-)
)

)
(:action Flag258Action-30
:parameters ()
:precondition
(and
(Flag234-)
(Flag234prime-)
)
:effect
(and
(Flag258-)
(Flag258prime-)
)

)
(:action Flag258Action-31
:parameters ()
:precondition
(and
(Flag211-)
(Flag211prime-)
)
:effect
(and
(Flag258-)
(Flag258prime-)
)

)
(:action Flag258Action-32
:parameters ()
:precondition
(and
(Flag257-)
(Flag257prime-)
)
:effect
(and
(Flag258-)
(Flag258prime-)
)

)
(:action Flag258Action-33
:parameters ()
:precondition
(and
(Flag176-)
(Flag176prime-)
)
:effect
(and
(Flag258-)
(Flag258prime-)
)

)
(:action Flag258Action-34
:parameters ()
:precondition
(and
(Flag143-)
(Flag143prime-)
)
:effect
(and
(Flag258-)
(Flag258prime-)
)

)
(:action Flag258Action-35
:parameters ()
:precondition
(and
(Flag98-)
(Flag98prime-)
)
:effect
(and
(Flag258-)
(Flag258prime-)
)

)
(:action Flag258Action-36
:parameters ()
:precondition
(and
(Flag216-)
(Flag216prime-)
)
:effect
(and
(Flag258-)
(Flag258prime-)
)

)
(:action Flag258Action-37
:parameters ()
:precondition
(and
(Flag228-)
(Flag228prime-)
)
:effect
(and
(Flag258-)
(Flag258prime-)
)

)
(:action Prim258Action
:parameters ()
:precondition
(and
(not (Flag96-))
(Flag96prime-)
(not (Flag41-))
(Flag41prime-)
(not (Flag132-))
(Flag132prime-)
(not (Flag46-))
(Flag46prime-)
(not (Flag246-))
(Flag246prime-)
(not (Flag247-))
(Flag247prime-)
(not (Flag118-))
(Flag118prime-)
(not (Flag239-))
(Flag239prime-)
(not (Flag223-))
(Flag223prime-)
(not (Flag153-))
(Flag153prime-)
(not (Flag210-))
(Flag210prime-)
(not (Flag256-))
(Flag256prime-)
(not (Flag172-))
(Flag172prime-)
(not (Flag252-))
(Flag252prime-)
(not (Flag160-))
(Flag160prime-)
(not (Flag57-))
(Flag57prime-)
(not (Flag219-))
(Flag219prime-)
(not (Flag140-))
(Flag140prime-)
(not (Flag241-))
(Flag241prime-)
(not (Flag119-))
(Flag119prime-)
(not (Flag193-))
(Flag193prime-)
(not (Flag236-))
(Flag236prime-)
(not (Flag254-))
(Flag254prime-)
(not (Flag66-))
(Flag66prime-)
(not (Flag189-))
(Flag189prime-)
(not (Flag240-))
(Flag240prime-)
(not (Flag90-))
(Flag90prime-)
(not (Flag92-))
(Flag92prime-)
(not (Flag187-))
(Flag187prime-)
(not (Flag194-))
(Flag194prime-)
(not (Flag234-))
(Flag234prime-)
(not (Flag211-))
(Flag211prime-)
(not (Flag257-))
(Flag257prime-)
(not (Flag176-))
(Flag176prime-)
(not (Flag143-))
(Flag143prime-)
(not (Flag98-))
(Flag98prime-)
(not (Flag216-))
(Flag216prime-)
(not (Flag228-))
(Flag228prime-)
)
:effect
(and
(Flag258prime-)
(not (Flag258-))
)

)
(:action Flag260Action-0
:parameters ()
:precondition
(and
(Flag187-)
(Flag187prime-)
)
:effect
(and
(Flag260-)
(Flag260prime-)
)

)
(:action Flag260Action-1
:parameters ()
:precondition
(and
(Flag153-)
(Flag153prime-)
)
:effect
(and
(Flag260-)
(Flag260prime-)
)

)
(:action Flag260Action-2
:parameters ()
:precondition
(and
(Flag210-)
(Flag210prime-)
)
:effect
(and
(Flag260-)
(Flag260prime-)
)

)
(:action Flag260Action-3
:parameters ()
:precondition
(and
(Flag256-)
(Flag256prime-)
)
:effect
(and
(Flag260-)
(Flag260prime-)
)

)
(:action Flag260Action-4
:parameters ()
:precondition
(and
(Flag172-)
(Flag172prime-)
)
:effect
(and
(Flag260-)
(Flag260prime-)
)

)
(:action Flag260Action-5
:parameters ()
:precondition
(and
(Flag46-)
(Flag46prime-)
)
:effect
(and
(Flag260-)
(Flag260prime-)
)

)
(:action Flag260Action-6
:parameters ()
:precondition
(and
(Flag41-)
(Flag41prime-)
)
:effect
(and
(Flag260-)
(Flag260prime-)
)

)
(:action Flag260Action-7
:parameters ()
:precondition
(and
(Flag259-)
(Flag259prime-)
)
:effect
(and
(Flag260-)
(Flag260prime-)
)

)
(:action Flag260Action-8
:parameters ()
:precondition
(and
(Flag193-)
(Flag193prime-)
)
:effect
(and
(Flag260-)
(Flag260prime-)
)

)
(:action Flag260Action-9
:parameters ()
:precondition
(and
(Flag119-)
(Flag119prime-)
)
:effect
(and
(Flag260-)
(Flag260prime-)
)

)
(:action Flag260Action-10
:parameters ()
:precondition
(and
(Flag247-)
(Flag247prime-)
)
:effect
(and
(Flag260-)
(Flag260prime-)
)

)
(:action Flag260Action-11
:parameters ()
:precondition
(and
(Flag118-)
(Flag118prime-)
)
:effect
(and
(Flag260-)
(Flag260prime-)
)

)
(:action Flag260Action-12
:parameters ()
:precondition
(and
(Flag254-)
(Flag254prime-)
)
:effect
(and
(Flag260-)
(Flag260prime-)
)

)
(:action Flag260Action-13
:parameters ()
:precondition
(and
(Flag143-)
(Flag143prime-)
)
:effect
(and
(Flag260-)
(Flag260prime-)
)

)
(:action Flag260Action-14
:parameters ()
:precondition
(and
(Flag240-)
(Flag240prime-)
)
:effect
(and
(Flag260-)
(Flag260prime-)
)

)
(:action Flag260Action-15
:parameters ()
:precondition
(and
(Flag239-)
(Flag239prime-)
)
:effect
(and
(Flag260-)
(Flag260prime-)
)

)
(:action Flag260Action-16
:parameters ()
:precondition
(and
(Flag90-)
(Flag90prime-)
)
:effect
(and
(Flag260-)
(Flag260prime-)
)

)
(:action Flag260Action-17
:parameters ()
:precondition
(and
(Flag216-)
(Flag216prime-)
)
:effect
(and
(Flag260-)
(Flag260prime-)
)

)
(:action Flag260Action-18
:parameters ()
:precondition
(and
(Flag228-)
(Flag228prime-)
)
:effect
(and
(Flag260-)
(Flag260prime-)
)

)
(:action Prim260Action
:parameters ()
:precondition
(and
(not (Flag187-))
(Flag187prime-)
(not (Flag153-))
(Flag153prime-)
(not (Flag210-))
(Flag210prime-)
(not (Flag256-))
(Flag256prime-)
(not (Flag172-))
(Flag172prime-)
(not (Flag46-))
(Flag46prime-)
(not (Flag41-))
(Flag41prime-)
(not (Flag259-))
(Flag259prime-)
(not (Flag193-))
(Flag193prime-)
(not (Flag119-))
(Flag119prime-)
(not (Flag247-))
(Flag247prime-)
(not (Flag118-))
(Flag118prime-)
(not (Flag254-))
(Flag254prime-)
(not (Flag143-))
(Flag143prime-)
(not (Flag240-))
(Flag240prime-)
(not (Flag239-))
(Flag239prime-)
(not (Flag90-))
(Flag90prime-)
(not (Flag216-))
(Flag216prime-)
(not (Flag228-))
(Flag228prime-)
)
:effect
(and
(Flag260prime-)
(not (Flag260-))
)

)
(:action Flag263Action-0
:parameters ()
:precondition
(and
(Flag59-)
(Flag59prime-)
)
:effect
(and
(Flag263-)
(Flag263prime-)
)

)
(:action Flag263Action-1
:parameters ()
:precondition
(and
(Flag47-)
(Flag47prime-)
)
:effect
(and
(Flag263-)
(Flag263prime-)
)

)
(:action Flag263Action-2
:parameters ()
:precondition
(and
(Flag62-)
(Flag62prime-)
)
:effect
(and
(Flag263-)
(Flag263prime-)
)

)
(:action Flag263Action-3
:parameters ()
:precondition
(and
(Flag44-)
(Flag44prime-)
)
:effect
(and
(Flag263-)
(Flag263prime-)
)

)
(:action Flag263Action-4
:parameters ()
:precondition
(and
(Flag67-)
(Flag67prime-)
)
:effect
(and
(Flag263-)
(Flag263prime-)
)

)
(:action Flag263Action-5
:parameters ()
:precondition
(and
(Flag51-)
(Flag51prime-)
)
:effect
(and
(Flag263-)
(Flag263prime-)
)

)
(:action Flag263Action-6
:parameters ()
:precondition
(and
(Flag45-)
(Flag45prime-)
)
:effect
(and
(Flag263-)
(Flag263prime-)
)

)
(:action Flag263Action-7
:parameters ()
:precondition
(and
(Flag46-)
(Flag46prime-)
)
:effect
(and
(Flag263-)
(Flag263prime-)
)

)
(:action Flag263Action-8
:parameters ()
:precondition
(and
(Flag66-)
(Flag66prime-)
)
:effect
(and
(Flag263-)
(Flag263prime-)
)

)
(:action Flag263Action-9
:parameters ()
:precondition
(and
(Flag262-)
(Flag262prime-)
)
:effect
(and
(Flag263-)
(Flag263prime-)
)

)
(:action Flag263Action-10
:parameters ()
:precondition
(and
(Flag64-)
(Flag64prime-)
)
:effect
(and
(Flag263-)
(Flag263prime-)
)

)
(:action Flag263Action-11
:parameters ()
:precondition
(and
(Flag71-)
(Flag71prime-)
)
:effect
(and
(Flag263-)
(Flag263prime-)
)

)
(:action Flag263Action-12
:parameters ()
:precondition
(and
(Flag55-)
(Flag55prime-)
)
:effect
(and
(Flag263-)
(Flag263prime-)
)

)
(:action Flag263Action-13
:parameters ()
:precondition
(and
(Flag65-)
(Flag65prime-)
)
:effect
(and
(Flag263-)
(Flag263prime-)
)

)
(:action Flag263Action-14
:parameters ()
:precondition
(and
(Flag72-)
(Flag72prime-)
)
:effect
(and
(Flag263-)
(Flag263prime-)
)

)
(:action Flag263Action-15
:parameters ()
:precondition
(and
(Flag73-)
(Flag73prime-)
)
:effect
(and
(Flag263-)
(Flag263prime-)
)

)
(:action Flag263Action-16
:parameters ()
:precondition
(and
(Flag48-)
(Flag48prime-)
)
:effect
(and
(Flag263-)
(Flag263prime-)
)

)
(:action Flag263Action-17
:parameters ()
:precondition
(and
(Flag74-)
(Flag74prime-)
)
:effect
(and
(Flag263-)
(Flag263prime-)
)

)
(:action Flag263Action-18
:parameters ()
:precondition
(and
(Flag61-)
(Flag61prime-)
)
:effect
(and
(Flag263-)
(Flag263prime-)
)

)
(:action Prim263Action
:parameters ()
:precondition
(and
(not (Flag59-))
(Flag59prime-)
(not (Flag47-))
(Flag47prime-)
(not (Flag62-))
(Flag62prime-)
(not (Flag44-))
(Flag44prime-)
(not (Flag67-))
(Flag67prime-)
(not (Flag51-))
(Flag51prime-)
(not (Flag45-))
(Flag45prime-)
(not (Flag46-))
(Flag46prime-)
(not (Flag66-))
(Flag66prime-)
(not (Flag262-))
(Flag262prime-)
(not (Flag64-))
(Flag64prime-)
(not (Flag71-))
(Flag71prime-)
(not (Flag55-))
(Flag55prime-)
(not (Flag65-))
(Flag65prime-)
(not (Flag72-))
(Flag72prime-)
(not (Flag73-))
(Flag73prime-)
(not (Flag48-))
(Flag48prime-)
(not (Flag74-))
(Flag74prime-)
(not (Flag61-))
(Flag61prime-)
)
:effect
(and
(Flag263prime-)
(not (Flag263-))
)

)
(:action Flag1Action
:parameters ()
:precondition
(and
(COLUMN2-ROBOT)
(ROW1-ROBOT)
(NOT-ERROR-)
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(Flag1-)
(Flag1prime-)
)

)
(:action Prim1Action-0
:parameters ()
:precondition
(and
(not (COLUMN2-ROBOT))
)
:effect
(and
(Flag1prime-)
(not (Flag1-))
)

)
(:action Flag3Action-0
:parameters ()
:precondition
(and
(LEFTOF3-ROBOT)
)
:effect
(and
(Flag3-)
(Flag3prime-)
)

)
(:action Flag3Action-1
:parameters ()
:precondition
(and
(RIGHTOF7-ROBOT)
)
:effect
(and
(Flag3-)
(Flag3prime-)
)

)
(:action Flag3Action-2
:parameters ()
:precondition
(and
(RIGHTOF8-ROBOT)
)
:effect
(and
(Flag3-)
(Flag3prime-)
)

)
(:action Flag3Action-3
:parameters ()
:precondition
(and
(RIGHTOF15-ROBOT)
)
:effect
(and
(Flag3-)
(Flag3prime-)
)

)
(:action Flag3Action-4
:parameters ()
:precondition
(and
(LEFTOF2-ROBOT)
)
:effect
(and
(Flag3-)
(Flag3prime-)
)

)
(:action Flag3Action-5
:parameters ()
:precondition
(and
(LEFTOF9-ROBOT)
)
:effect
(and
(Flag3-)
(Flag3prime-)
)

)
(:action Flag3Action-7
:parameters ()
:precondition
(and
(RIGHTOF0-ROBOT)
)
:effect
(and
(Flag3-)
(Flag3prime-)
)

)
(:action Flag3Action-8
:parameters ()
:precondition
(and
(LEFTOF8-ROBOT)
)
:effect
(and
(Flag3-)
(Flag3prime-)
)

)
(:action Flag3Action-9
:parameters ()
:precondition
(and
(RIGHTOF12-ROBOT)
)
:effect
(and
(Flag3-)
(Flag3prime-)
)

)
(:action Flag3Action-10
:parameters ()
:precondition
(and
(LEFTOF14-ROBOT)
)
:effect
(and
(Flag3-)
(Flag3prime-)
)

)
(:action Flag3Action-11
:parameters ()
:precondition
(and
(LEFTOF5-ROBOT)
)
:effect
(and
(Flag3-)
(Flag3prime-)
)

)
(:action Flag3Action-12
:parameters ()
:precondition
(and
(RIGHTOF1-ROBOT)
)
:effect
(and
(Flag3-)
(Flag3prime-)
)

)
(:action Flag3Action-13
:parameters ()
:precondition
(and
(LEFTOF12-ROBOT)
)
:effect
(and
(Flag3-)
(Flag3prime-)
)

)
(:action Flag3Action-14
:parameters ()
:precondition
(and
(LEFTOF19-ROBOT)
)
:effect
(and
(Flag3-)
(Flag3prime-)
)

)
(:action Flag3Action-15
:parameters ()
:precondition
(and
(RIGHTOF13-ROBOT)
)
:effect
(and
(Flag3-)
(Flag3prime-)
)

)
(:action Flag3Action-16
:parameters ()
:precondition
(and
(RIGHTOF14-ROBOT)
)
:effect
(and
(Flag3-)
(Flag3prime-)
)

)
(:action Flag3Action-17
:parameters ()
:precondition
(and
(LEFTOF1-ROBOT)
)
:effect
(and
(Flag3-)
(Flag3prime-)
)

)
(:action Flag3Action-18
:parameters ()
:precondition
(and
(RIGHTOF6-ROBOT)
)
:effect
(and
(Flag3-)
(Flag3prime-)
)

)
(:action Flag3Action-19
:parameters ()
:precondition
(and
(RIGHTOF9-ROBOT)
)
:effect
(and
(Flag3-)
(Flag3prime-)
)

)
(:action Flag3Action-20
:parameters ()
:precondition
(and
(LEFTOF6-ROBOT)
)
:effect
(and
(Flag3-)
(Flag3prime-)
)

)
(:action Flag3Action-21
:parameters ()
:precondition
(and
(RIGHTOF11-ROBOT)
)
:effect
(and
(Flag3-)
(Flag3prime-)
)

)
(:action Flag3Action-22
:parameters ()
:precondition
(and
(RIGHTOF17-ROBOT)
)
:effect
(and
(Flag3-)
(Flag3prime-)
)

)
(:action Flag3Action-23
:parameters ()
:precondition
(and
(LEFTOF11-ROBOT)
)
:effect
(and
(Flag3-)
(Flag3prime-)
)

)
(:action Flag3Action-24
:parameters ()
:precondition
(and
(RIGHTOF5-ROBOT)
)
:effect
(and
(Flag3-)
(Flag3prime-)
)

)
(:action Flag3Action-25
:parameters ()
:precondition
(and
(LEFTOF10-ROBOT)
)
:effect
(and
(Flag3-)
(Flag3prime-)
)

)
(:action Flag3Action-26
:parameters ()
:precondition
(and
(LEFTOF15-ROBOT)
)
:effect
(and
(Flag3-)
(Flag3prime-)
)

)
(:action Flag3Action-27
:parameters ()
:precondition
(and
(LEFTOF4-ROBOT)
)
:effect
(and
(Flag3-)
(Flag3prime-)
)

)
(:action Flag3Action-28
:parameters ()
:precondition
(and
(RIGHTOF18-ROBOT)
)
:effect
(and
(Flag3-)
(Flag3prime-)
)

)
(:action Flag3Action-29
:parameters ()
:precondition
(and
(LEFTOF16-ROBOT)
)
:effect
(and
(Flag3-)
(Flag3prime-)
)

)
(:action Flag3Action-30
:parameters ()
:precondition
(and
(RIGHTOF16-ROBOT)
)
:effect
(and
(Flag3-)
(Flag3prime-)
)

)
(:action Flag3Action-31
:parameters ()
:precondition
(and
(RIGHTOF4-ROBOT)
)
:effect
(and
(Flag3-)
(Flag3prime-)
)

)
(:action Flag3Action-32
:parameters ()
:precondition
(and
(RIGHTOF10-ROBOT)
)
:effect
(and
(Flag3-)
(Flag3prime-)
)

)
(:action Flag3Action-33
:parameters ()
:precondition
(and
(LEFTOF7-ROBOT)
)
:effect
(and
(Flag3-)
(Flag3prime-)
)

)
(:action Flag3Action-34
:parameters ()
:precondition
(and
(LEFTOF13-ROBOT)
)
:effect
(and
(Flag3-)
(Flag3prime-)
)

)
(:action Flag3Action-35
:parameters ()
:precondition
(and
(LEFTOF17-ROBOT)
)
:effect
(and
(Flag3-)
(Flag3prime-)
)

)
(:action Flag3Action-37
:parameters ()
:precondition
(and
(RIGHTOF3-ROBOT)
)
:effect
(and
(Flag3-)
(Flag3prime-)
)

)
(:action Prim3Action
:parameters ()
:precondition
(and
(not (LEFTOF3-ROBOT))
(not (RIGHTOF7-ROBOT))
(not (RIGHTOF8-ROBOT))
(not (RIGHTOF15-ROBOT))
(not (LEFTOF2-ROBOT))
(not (LEFTOF9-ROBOT))
(not (RIGHTOF2-ROBOT))
(not (RIGHTOF0-ROBOT))
(not (LEFTOF8-ROBOT))
(not (RIGHTOF12-ROBOT))
(not (LEFTOF14-ROBOT))
(not (LEFTOF5-ROBOT))
(not (RIGHTOF1-ROBOT))
(not (LEFTOF12-ROBOT))
(not (LEFTOF19-ROBOT))
(not (RIGHTOF13-ROBOT))
(not (RIGHTOF14-ROBOT))
(not (LEFTOF1-ROBOT))
(not (RIGHTOF6-ROBOT))
(not (RIGHTOF9-ROBOT))
(not (LEFTOF6-ROBOT))
(not (RIGHTOF11-ROBOT))
(not (RIGHTOF17-ROBOT))
(not (LEFTOF11-ROBOT))
(not (RIGHTOF5-ROBOT))
(not (LEFTOF10-ROBOT))
(not (LEFTOF15-ROBOT))
(not (LEFTOF4-ROBOT))
(not (RIGHTOF18-ROBOT))
(not (LEFTOF16-ROBOT))
(not (RIGHTOF16-ROBOT))
(not (RIGHTOF4-ROBOT))
(not (RIGHTOF10-ROBOT))
(not (LEFTOF7-ROBOT))
(not (LEFTOF13-ROBOT))
(not (LEFTOF17-ROBOT))
(not (LEFTOF18-ROBOT))
(not (RIGHTOF3-ROBOT))
)
:effect
(and
(Flag3prime-)
(not (Flag3-))
)

)
(:action Flag5Action-0
:parameters ()
:precondition
(and
(LEFTOF1-ROBOT)
)
:effect
(and
(Flag5-)
(Flag5prime-)
)

)
(:action Flag5Action-1
:parameters ()
:precondition
(and
(LEFTOF2-ROBOT)
)
:effect
(and
(Flag5-)
(Flag5prime-)
)

)
(:action Prim5Action
:parameters ()
:precondition
(and
(not (LEFTOF1-ROBOT))
(not (LEFTOF2-ROBOT))
)
:effect
(and
(Flag5prime-)
(not (Flag5-))
)

)
(:action Flag6Action-0
:parameters ()
:precondition
(and
(LEFTOF1-ROBOT)
)
:effect
(and
(Flag6-)
(Flag6prime-)
)

)
(:action Flag6Action-1
:parameters ()
:precondition
(and
(LEFTOF3-ROBOT)
)
:effect
(and
(Flag6-)
(Flag6prime-)
)

)
(:action Flag6Action-2
:parameters ()
:precondition
(and
(LEFTOF2-ROBOT)
)
:effect
(and
(Flag6-)
(Flag6prime-)
)

)
(:action Prim6Action
:parameters ()
:precondition
(and
(not (LEFTOF1-ROBOT))
(not (LEFTOF3-ROBOT))
(not (LEFTOF2-ROBOT))
)
:effect
(and
(Flag6prime-)
(not (Flag6-))
)

)
(:action Flag7Action-0
:parameters ()
:precondition
(and
(LEFTOF1-ROBOT)
)
:effect
(and
(Flag7-)
(Flag7prime-)
)

)
(:action Flag7Action-1
:parameters ()
:precondition
(and
(LEFTOF3-ROBOT)
)
:effect
(and
(Flag7-)
(Flag7prime-)
)

)
(:action Flag7Action-2
:parameters ()
:precondition
(and
(LEFTOF4-ROBOT)
)
:effect
(and
(Flag7-)
(Flag7prime-)
)

)
(:action Flag7Action-3
:parameters ()
:precondition
(and
(LEFTOF2-ROBOT)
)
:effect
(and
(Flag7-)
(Flag7prime-)
)

)
(:action Prim7Action
:parameters ()
:precondition
(and
(not (LEFTOF1-ROBOT))
(not (LEFTOF3-ROBOT))
(not (LEFTOF4-ROBOT))
(not (LEFTOF2-ROBOT))
)
:effect
(and
(Flag7prime-)
(not (Flag7-))
)

)
(:action Flag8Action-0
:parameters ()
:precondition
(and
(LEFTOF5-ROBOT)
)
:effect
(and
(Flag8-)
(Flag8prime-)
)

)
(:action Flag8Action-1
:parameters ()
:precondition
(and
(LEFTOF1-ROBOT)
)
:effect
(and
(Flag8-)
(Flag8prime-)
)

)
(:action Flag8Action-2
:parameters ()
:precondition
(and
(LEFTOF3-ROBOT)
)
:effect
(and
(Flag8-)
(Flag8prime-)
)

)
(:action Flag8Action-3
:parameters ()
:precondition
(and
(LEFTOF4-ROBOT)
)
:effect
(and
(Flag8-)
(Flag8prime-)
)

)
(:action Flag8Action-4
:parameters ()
:precondition
(and
(LEFTOF2-ROBOT)
)
:effect
(and
(Flag8-)
(Flag8prime-)
)

)
(:action Prim8Action
:parameters ()
:precondition
(and
(not (LEFTOF5-ROBOT))
(not (LEFTOF1-ROBOT))
(not (LEFTOF3-ROBOT))
(not (LEFTOF4-ROBOT))
(not (LEFTOF2-ROBOT))
)
:effect
(and
(Flag8prime-)
(not (Flag8-))
)

)
(:action Flag9Action-0
:parameters ()
:precondition
(and
(LEFTOF2-ROBOT)
)
:effect
(and
(Flag9-)
(Flag9prime-)
)

)
(:action Flag9Action-1
:parameters ()
:precondition
(and
(LEFTOF1-ROBOT)
)
:effect
(and
(Flag9-)
(Flag9prime-)
)

)
(:action Flag9Action-2
:parameters ()
:precondition
(and
(LEFTOF6-ROBOT)
)
:effect
(and
(Flag9-)
(Flag9prime-)
)

)
(:action Flag9Action-3
:parameters ()
:precondition
(and
(LEFTOF5-ROBOT)
)
:effect
(and
(Flag9-)
(Flag9prime-)
)

)
(:action Flag9Action-4
:parameters ()
:precondition
(and
(LEFTOF3-ROBOT)
)
:effect
(and
(Flag9-)
(Flag9prime-)
)

)
(:action Flag9Action-5
:parameters ()
:precondition
(and
(LEFTOF4-ROBOT)
)
:effect
(and
(Flag9-)
(Flag9prime-)
)

)
(:action Prim9Action
:parameters ()
:precondition
(and
(not (LEFTOF2-ROBOT))
(not (LEFTOF1-ROBOT))
(not (LEFTOF6-ROBOT))
(not (LEFTOF5-ROBOT))
(not (LEFTOF3-ROBOT))
(not (LEFTOF4-ROBOT))
)
:effect
(and
(Flag9prime-)
(not (Flag9-))
)

)
(:action Flag10Action-0
:parameters ()
:precondition
(and
(LEFTOF2-ROBOT)
)
:effect
(and
(Flag10-)
(Flag10prime-)
)

)
(:action Flag10Action-1
:parameters ()
:precondition
(and
(LEFTOF7-ROBOT)
)
:effect
(and
(Flag10-)
(Flag10prime-)
)

)
(:action Flag10Action-2
:parameters ()
:precondition
(and
(LEFTOF1-ROBOT)
)
:effect
(and
(Flag10-)
(Flag10prime-)
)

)
(:action Flag10Action-3
:parameters ()
:precondition
(and
(LEFTOF6-ROBOT)
)
:effect
(and
(Flag10-)
(Flag10prime-)
)

)
(:action Flag10Action-4
:parameters ()
:precondition
(and
(LEFTOF5-ROBOT)
)
:effect
(and
(Flag10-)
(Flag10prime-)
)

)
(:action Flag10Action-5
:parameters ()
:precondition
(and
(LEFTOF3-ROBOT)
)
:effect
(and
(Flag10-)
(Flag10prime-)
)

)
(:action Flag10Action-6
:parameters ()
:precondition
(and
(LEFTOF4-ROBOT)
)
:effect
(and
(Flag10-)
(Flag10prime-)
)

)
(:action Prim10Action
:parameters ()
:precondition
(and
(not (LEFTOF2-ROBOT))
(not (LEFTOF7-ROBOT))
(not (LEFTOF1-ROBOT))
(not (LEFTOF6-ROBOT))
(not (LEFTOF5-ROBOT))
(not (LEFTOF3-ROBOT))
(not (LEFTOF4-ROBOT))
)
:effect
(and
(Flag10prime-)
(not (Flag10-))
)

)
(:action Flag11Action-0
:parameters ()
:precondition
(and
(LEFTOF2-ROBOT)
)
:effect
(and
(Flag11-)
(Flag11prime-)
)

)
(:action Flag11Action-1
:parameters ()
:precondition
(and
(LEFTOF7-ROBOT)
)
:effect
(and
(Flag11-)
(Flag11prime-)
)

)
(:action Flag11Action-2
:parameters ()
:precondition
(and
(LEFTOF1-ROBOT)
)
:effect
(and
(Flag11-)
(Flag11prime-)
)

)
(:action Flag11Action-3
:parameters ()
:precondition
(and
(LEFTOF6-ROBOT)
)
:effect
(and
(Flag11-)
(Flag11prime-)
)

)
(:action Flag11Action-4
:parameters ()
:precondition
(and
(LEFTOF5-ROBOT)
)
:effect
(and
(Flag11-)
(Flag11prime-)
)

)
(:action Flag11Action-5
:parameters ()
:precondition
(and
(LEFTOF3-ROBOT)
)
:effect
(and
(Flag11-)
(Flag11prime-)
)

)
(:action Flag11Action-6
:parameters ()
:precondition
(and
(LEFTOF4-ROBOT)
)
:effect
(and
(Flag11-)
(Flag11prime-)
)

)
(:action Flag11Action-7
:parameters ()
:precondition
(and
(LEFTOF8-ROBOT)
)
:effect
(and
(Flag11-)
(Flag11prime-)
)

)
(:action Prim11Action
:parameters ()
:precondition
(and
(not (LEFTOF2-ROBOT))
(not (LEFTOF7-ROBOT))
(not (LEFTOF1-ROBOT))
(not (LEFTOF6-ROBOT))
(not (LEFTOF5-ROBOT))
(not (LEFTOF3-ROBOT))
(not (LEFTOF4-ROBOT))
(not (LEFTOF8-ROBOT))
)
:effect
(and
(Flag11prime-)
(not (Flag11-))
)

)
(:action Flag12Action-0
:parameters ()
:precondition
(and
(LEFTOF9-ROBOT)
)
:effect
(and
(Flag12-)
(Flag12prime-)
)

)
(:action Flag12Action-1
:parameters ()
:precondition
(and
(LEFTOF2-ROBOT)
)
:effect
(and
(Flag12-)
(Flag12prime-)
)

)
(:action Flag12Action-2
:parameters ()
:precondition
(and
(LEFTOF7-ROBOT)
)
:effect
(and
(Flag12-)
(Flag12prime-)
)

)
(:action Flag12Action-3
:parameters ()
:precondition
(and
(LEFTOF1-ROBOT)
)
:effect
(and
(Flag12-)
(Flag12prime-)
)

)
(:action Flag12Action-4
:parameters ()
:precondition
(and
(LEFTOF6-ROBOT)
)
:effect
(and
(Flag12-)
(Flag12prime-)
)

)
(:action Flag12Action-5
:parameters ()
:precondition
(and
(LEFTOF5-ROBOT)
)
:effect
(and
(Flag12-)
(Flag12prime-)
)

)
(:action Flag12Action-6
:parameters ()
:precondition
(and
(LEFTOF3-ROBOT)
)
:effect
(and
(Flag12-)
(Flag12prime-)
)

)
(:action Flag12Action-7
:parameters ()
:precondition
(and
(LEFTOF4-ROBOT)
)
:effect
(and
(Flag12-)
(Flag12prime-)
)

)
(:action Flag12Action-8
:parameters ()
:precondition
(and
(LEFTOF8-ROBOT)
)
:effect
(and
(Flag12-)
(Flag12prime-)
)

)
(:action Prim12Action
:parameters ()
:precondition
(and
(not (LEFTOF9-ROBOT))
(not (LEFTOF2-ROBOT))
(not (LEFTOF7-ROBOT))
(not (LEFTOF1-ROBOT))
(not (LEFTOF6-ROBOT))
(not (LEFTOF5-ROBOT))
(not (LEFTOF3-ROBOT))
(not (LEFTOF4-ROBOT))
(not (LEFTOF8-ROBOT))
)
:effect
(and
(Flag12prime-)
(not (Flag12-))
)

)
(:action Flag13Action-0
:parameters ()
:precondition
(and
(LEFTOF10-ROBOT)
)
:effect
(and
(Flag13-)
(Flag13prime-)
)

)
(:action Flag13Action-1
:parameters ()
:precondition
(and
(LEFTOF9-ROBOT)
)
:effect
(and
(Flag13-)
(Flag13prime-)
)

)
(:action Flag13Action-2
:parameters ()
:precondition
(and
(LEFTOF2-ROBOT)
)
:effect
(and
(Flag13-)
(Flag13prime-)
)

)
(:action Flag13Action-3
:parameters ()
:precondition
(and
(LEFTOF7-ROBOT)
)
:effect
(and
(Flag13-)
(Flag13prime-)
)

)
(:action Flag13Action-4
:parameters ()
:precondition
(and
(LEFTOF1-ROBOT)
)
:effect
(and
(Flag13-)
(Flag13prime-)
)

)
(:action Flag13Action-5
:parameters ()
:precondition
(and
(LEFTOF6-ROBOT)
)
:effect
(and
(Flag13-)
(Flag13prime-)
)

)
(:action Flag13Action-6
:parameters ()
:precondition
(and
(LEFTOF5-ROBOT)
)
:effect
(and
(Flag13-)
(Flag13prime-)
)

)
(:action Flag13Action-7
:parameters ()
:precondition
(and
(LEFTOF3-ROBOT)
)
:effect
(and
(Flag13-)
(Flag13prime-)
)

)
(:action Flag13Action-8
:parameters ()
:precondition
(and
(LEFTOF4-ROBOT)
)
:effect
(and
(Flag13-)
(Flag13prime-)
)

)
(:action Flag13Action-9
:parameters ()
:precondition
(and
(LEFTOF8-ROBOT)
)
:effect
(and
(Flag13-)
(Flag13prime-)
)

)
(:action Prim13Action
:parameters ()
:precondition
(and
(not (LEFTOF10-ROBOT))
(not (LEFTOF9-ROBOT))
(not (LEFTOF2-ROBOT))
(not (LEFTOF7-ROBOT))
(not (LEFTOF1-ROBOT))
(not (LEFTOF6-ROBOT))
(not (LEFTOF5-ROBOT))
(not (LEFTOF3-ROBOT))
(not (LEFTOF4-ROBOT))
(not (LEFTOF8-ROBOT))
)
:effect
(and
(Flag13prime-)
(not (Flag13-))
)

)
(:action Flag14Action-0
:parameters ()
:precondition
(and
(LEFTOF10-ROBOT)
)
:effect
(and
(Flag14-)
(Flag14prime-)
)

)
(:action Flag14Action-1
:parameters ()
:precondition
(and
(LEFTOF11-ROBOT)
)
:effect
(and
(Flag14-)
(Flag14prime-)
)

)
(:action Flag14Action-2
:parameters ()
:precondition
(and
(LEFTOF9-ROBOT)
)
:effect
(and
(Flag14-)
(Flag14prime-)
)

)
(:action Flag14Action-3
:parameters ()
:precondition
(and
(LEFTOF2-ROBOT)
)
:effect
(and
(Flag14-)
(Flag14prime-)
)

)
(:action Flag14Action-4
:parameters ()
:precondition
(and
(LEFTOF7-ROBOT)
)
:effect
(and
(Flag14-)
(Flag14prime-)
)

)
(:action Flag14Action-5
:parameters ()
:precondition
(and
(LEFTOF1-ROBOT)
)
:effect
(and
(Flag14-)
(Flag14prime-)
)

)
(:action Flag14Action-6
:parameters ()
:precondition
(and
(LEFTOF6-ROBOT)
)
:effect
(and
(Flag14-)
(Flag14prime-)
)

)
(:action Flag14Action-7
:parameters ()
:precondition
(and
(LEFTOF5-ROBOT)
)
:effect
(and
(Flag14-)
(Flag14prime-)
)

)
(:action Flag14Action-8
:parameters ()
:precondition
(and
(LEFTOF3-ROBOT)
)
:effect
(and
(Flag14-)
(Flag14prime-)
)

)
(:action Flag14Action-9
:parameters ()
:precondition
(and
(LEFTOF4-ROBOT)
)
:effect
(and
(Flag14-)
(Flag14prime-)
)

)
(:action Flag14Action-10
:parameters ()
:precondition
(and
(LEFTOF8-ROBOT)
)
:effect
(and
(Flag14-)
(Flag14prime-)
)

)
(:action Prim14Action
:parameters ()
:precondition
(and
(not (LEFTOF10-ROBOT))
(not (LEFTOF11-ROBOT))
(not (LEFTOF9-ROBOT))
(not (LEFTOF2-ROBOT))
(not (LEFTOF7-ROBOT))
(not (LEFTOF1-ROBOT))
(not (LEFTOF6-ROBOT))
(not (LEFTOF5-ROBOT))
(not (LEFTOF3-ROBOT))
(not (LEFTOF4-ROBOT))
(not (LEFTOF8-ROBOT))
)
:effect
(and
(Flag14prime-)
(not (Flag14-))
)

)
(:action Flag15Action-0
:parameters ()
:precondition
(and
(LEFTOF10-ROBOT)
)
:effect
(and
(Flag15-)
(Flag15prime-)
)

)
(:action Flag15Action-1
:parameters ()
:precondition
(and
(LEFTOF11-ROBOT)
)
:effect
(and
(Flag15-)
(Flag15prime-)
)

)
(:action Flag15Action-2
:parameters ()
:precondition
(and
(LEFTOF9-ROBOT)
)
:effect
(and
(Flag15-)
(Flag15prime-)
)

)
(:action Flag15Action-3
:parameters ()
:precondition
(and
(LEFTOF2-ROBOT)
)
:effect
(and
(Flag15-)
(Flag15prime-)
)

)
(:action Flag15Action-4
:parameters ()
:precondition
(and
(LEFTOF7-ROBOT)
)
:effect
(and
(Flag15-)
(Flag15prime-)
)

)
(:action Flag15Action-5
:parameters ()
:precondition
(and
(LEFTOF1-ROBOT)
)
:effect
(and
(Flag15-)
(Flag15prime-)
)

)
(:action Flag15Action-6
:parameters ()
:precondition
(and
(LEFTOF12-ROBOT)
)
:effect
(and
(Flag15-)
(Flag15prime-)
)

)
(:action Flag15Action-7
:parameters ()
:precondition
(and
(LEFTOF6-ROBOT)
)
:effect
(and
(Flag15-)
(Flag15prime-)
)

)
(:action Flag15Action-8
:parameters ()
:precondition
(and
(LEFTOF5-ROBOT)
)
:effect
(and
(Flag15-)
(Flag15prime-)
)

)
(:action Flag15Action-9
:parameters ()
:precondition
(and
(LEFTOF3-ROBOT)
)
:effect
(and
(Flag15-)
(Flag15prime-)
)

)
(:action Flag15Action-10
:parameters ()
:precondition
(and
(LEFTOF4-ROBOT)
)
:effect
(and
(Flag15-)
(Flag15prime-)
)

)
(:action Flag15Action-11
:parameters ()
:precondition
(and
(LEFTOF8-ROBOT)
)
:effect
(and
(Flag15-)
(Flag15prime-)
)

)
(:action Prim15Action
:parameters ()
:precondition
(and
(not (LEFTOF10-ROBOT))
(not (LEFTOF11-ROBOT))
(not (LEFTOF9-ROBOT))
(not (LEFTOF2-ROBOT))
(not (LEFTOF7-ROBOT))
(not (LEFTOF1-ROBOT))
(not (LEFTOF12-ROBOT))
(not (LEFTOF6-ROBOT))
(not (LEFTOF5-ROBOT))
(not (LEFTOF3-ROBOT))
(not (LEFTOF4-ROBOT))
(not (LEFTOF8-ROBOT))
)
:effect
(and
(Flag15prime-)
(not (Flag15-))
)

)
(:action Flag16Action-0
:parameters ()
:precondition
(and
(LEFTOF10-ROBOT)
)
:effect
(and
(Flag16-)
(Flag16prime-)
)

)
(:action Flag16Action-1
:parameters ()
:precondition
(and
(LEFTOF11-ROBOT)
)
:effect
(and
(Flag16-)
(Flag16prime-)
)

)
(:action Flag16Action-2
:parameters ()
:precondition
(and
(LEFTOF9-ROBOT)
)
:effect
(and
(Flag16-)
(Flag16prime-)
)

)
(:action Flag16Action-3
:parameters ()
:precondition
(and
(LEFTOF2-ROBOT)
)
:effect
(and
(Flag16-)
(Flag16prime-)
)

)
(:action Flag16Action-4
:parameters ()
:precondition
(and
(LEFTOF13-ROBOT)
)
:effect
(and
(Flag16-)
(Flag16prime-)
)

)
(:action Flag16Action-5
:parameters ()
:precondition
(and
(LEFTOF1-ROBOT)
)
:effect
(and
(Flag16-)
(Flag16prime-)
)

)
(:action Flag16Action-6
:parameters ()
:precondition
(and
(LEFTOF12-ROBOT)
)
:effect
(and
(Flag16-)
(Flag16prime-)
)

)
(:action Flag16Action-7
:parameters ()
:precondition
(and
(LEFTOF6-ROBOT)
)
:effect
(and
(Flag16-)
(Flag16prime-)
)

)
(:action Flag16Action-8
:parameters ()
:precondition
(and
(LEFTOF5-ROBOT)
)
:effect
(and
(Flag16-)
(Flag16prime-)
)

)
(:action Flag16Action-9
:parameters ()
:precondition
(and
(LEFTOF3-ROBOT)
)
:effect
(and
(Flag16-)
(Flag16prime-)
)

)
(:action Flag16Action-10
:parameters ()
:precondition
(and
(LEFTOF4-ROBOT)
)
:effect
(and
(Flag16-)
(Flag16prime-)
)

)
(:action Flag16Action-11
:parameters ()
:precondition
(and
(LEFTOF7-ROBOT)
)
:effect
(and
(Flag16-)
(Flag16prime-)
)

)
(:action Flag16Action-12
:parameters ()
:precondition
(and
(LEFTOF8-ROBOT)
)
:effect
(and
(Flag16-)
(Flag16prime-)
)

)
(:action Prim16Action
:parameters ()
:precondition
(and
(not (LEFTOF10-ROBOT))
(not (LEFTOF11-ROBOT))
(not (LEFTOF9-ROBOT))
(not (LEFTOF2-ROBOT))
(not (LEFTOF13-ROBOT))
(not (LEFTOF1-ROBOT))
(not (LEFTOF12-ROBOT))
(not (LEFTOF6-ROBOT))
(not (LEFTOF5-ROBOT))
(not (LEFTOF3-ROBOT))
(not (LEFTOF4-ROBOT))
(not (LEFTOF7-ROBOT))
(not (LEFTOF8-ROBOT))
)
:effect
(and
(Flag16prime-)
(not (Flag16-))
)

)
(:action Flag17Action-0
:parameters ()
:precondition
(and
(LEFTOF10-ROBOT)
)
:effect
(and
(Flag17-)
(Flag17prime-)
)

)
(:action Flag17Action-1
:parameters ()
:precondition
(and
(LEFTOF11-ROBOT)
)
:effect
(and
(Flag17-)
(Flag17prime-)
)

)
(:action Flag17Action-2
:parameters ()
:precondition
(and
(LEFTOF9-ROBOT)
)
:effect
(and
(Flag17-)
(Flag17prime-)
)

)
(:action Flag17Action-3
:parameters ()
:precondition
(and
(LEFTOF2-ROBOT)
)
:effect
(and
(Flag17-)
(Flag17prime-)
)

)
(:action Flag17Action-4
:parameters ()
:precondition
(and
(LEFTOF13-ROBOT)
)
:effect
(and
(Flag17-)
(Flag17prime-)
)

)
(:action Flag17Action-5
:parameters ()
:precondition
(and
(LEFTOF1-ROBOT)
)
:effect
(and
(Flag17-)
(Flag17prime-)
)

)
(:action Flag17Action-6
:parameters ()
:precondition
(and
(LEFTOF12-ROBOT)
)
:effect
(and
(Flag17-)
(Flag17prime-)
)

)
(:action Flag17Action-7
:parameters ()
:precondition
(and
(LEFTOF6-ROBOT)
)
:effect
(and
(Flag17-)
(Flag17prime-)
)

)
(:action Flag17Action-8
:parameters ()
:precondition
(and
(LEFTOF5-ROBOT)
)
:effect
(and
(Flag17-)
(Flag17prime-)
)

)
(:action Flag17Action-9
:parameters ()
:precondition
(and
(LEFTOF14-ROBOT)
)
:effect
(and
(Flag17-)
(Flag17prime-)
)

)
(:action Flag17Action-10
:parameters ()
:precondition
(and
(LEFTOF3-ROBOT)
)
:effect
(and
(Flag17-)
(Flag17prime-)
)

)
(:action Flag17Action-11
:parameters ()
:precondition
(and
(LEFTOF4-ROBOT)
)
:effect
(and
(Flag17-)
(Flag17prime-)
)

)
(:action Flag17Action-12
:parameters ()
:precondition
(and
(LEFTOF7-ROBOT)
)
:effect
(and
(Flag17-)
(Flag17prime-)
)

)
(:action Flag17Action-13
:parameters ()
:precondition
(and
(LEFTOF8-ROBOT)
)
:effect
(and
(Flag17-)
(Flag17prime-)
)

)
(:action Prim17Action
:parameters ()
:precondition
(and
(not (LEFTOF10-ROBOT))
(not (LEFTOF11-ROBOT))
(not (LEFTOF9-ROBOT))
(not (LEFTOF2-ROBOT))
(not (LEFTOF13-ROBOT))
(not (LEFTOF1-ROBOT))
(not (LEFTOF12-ROBOT))
(not (LEFTOF6-ROBOT))
(not (LEFTOF5-ROBOT))
(not (LEFTOF14-ROBOT))
(not (LEFTOF3-ROBOT))
(not (LEFTOF4-ROBOT))
(not (LEFTOF7-ROBOT))
(not (LEFTOF8-ROBOT))
)
:effect
(and
(Flag17prime-)
(not (Flag17-))
)

)
(:action Flag18Action-0
:parameters ()
:precondition
(and
(LEFTOF15-ROBOT)
)
:effect
(and
(Flag18-)
(Flag18prime-)
)

)
(:action Flag18Action-1
:parameters ()
:precondition
(and
(LEFTOF10-ROBOT)
)
:effect
(and
(Flag18-)
(Flag18prime-)
)

)
(:action Flag18Action-2
:parameters ()
:precondition
(and
(LEFTOF11-ROBOT)
)
:effect
(and
(Flag18-)
(Flag18prime-)
)

)
(:action Flag18Action-3
:parameters ()
:precondition
(and
(LEFTOF9-ROBOT)
)
:effect
(and
(Flag18-)
(Flag18prime-)
)

)
(:action Flag18Action-4
:parameters ()
:precondition
(and
(LEFTOF2-ROBOT)
)
:effect
(and
(Flag18-)
(Flag18prime-)
)

)
(:action Flag18Action-5
:parameters ()
:precondition
(and
(LEFTOF13-ROBOT)
)
:effect
(and
(Flag18-)
(Flag18prime-)
)

)
(:action Flag18Action-6
:parameters ()
:precondition
(and
(LEFTOF1-ROBOT)
)
:effect
(and
(Flag18-)
(Flag18prime-)
)

)
(:action Flag18Action-7
:parameters ()
:precondition
(and
(LEFTOF12-ROBOT)
)
:effect
(and
(Flag18-)
(Flag18prime-)
)

)
(:action Flag18Action-8
:parameters ()
:precondition
(and
(LEFTOF6-ROBOT)
)
:effect
(and
(Flag18-)
(Flag18prime-)
)

)
(:action Flag18Action-9
:parameters ()
:precondition
(and
(LEFTOF5-ROBOT)
)
:effect
(and
(Flag18-)
(Flag18prime-)
)

)
(:action Flag18Action-10
:parameters ()
:precondition
(and
(LEFTOF14-ROBOT)
)
:effect
(and
(Flag18-)
(Flag18prime-)
)

)
(:action Flag18Action-11
:parameters ()
:precondition
(and
(LEFTOF3-ROBOT)
)
:effect
(and
(Flag18-)
(Flag18prime-)
)

)
(:action Flag18Action-12
:parameters ()
:precondition
(and
(LEFTOF4-ROBOT)
)
:effect
(and
(Flag18-)
(Flag18prime-)
)

)
(:action Flag18Action-13
:parameters ()
:precondition
(and
(LEFTOF7-ROBOT)
)
:effect
(and
(Flag18-)
(Flag18prime-)
)

)
(:action Flag18Action-14
:parameters ()
:precondition
(and
(LEFTOF8-ROBOT)
)
:effect
(and
(Flag18-)
(Flag18prime-)
)

)
(:action Prim18Action
:parameters ()
:precondition
(and
(not (LEFTOF15-ROBOT))
(not (LEFTOF10-ROBOT))
(not (LEFTOF11-ROBOT))
(not (LEFTOF9-ROBOT))
(not (LEFTOF2-ROBOT))
(not (LEFTOF13-ROBOT))
(not (LEFTOF1-ROBOT))
(not (LEFTOF12-ROBOT))
(not (LEFTOF6-ROBOT))
(not (LEFTOF5-ROBOT))
(not (LEFTOF14-ROBOT))
(not (LEFTOF3-ROBOT))
(not (LEFTOF4-ROBOT))
(not (LEFTOF7-ROBOT))
(not (LEFTOF8-ROBOT))
)
:effect
(and
(Flag18prime-)
(not (Flag18-))
)

)
(:action Flag19Action-0
:parameters ()
:precondition
(and
(LEFTOF15-ROBOT)
)
:effect
(and
(Flag19-)
(Flag19prime-)
)

)
(:action Flag19Action-1
:parameters ()
:precondition
(and
(LEFTOF10-ROBOT)
)
:effect
(and
(Flag19-)
(Flag19prime-)
)

)
(:action Flag19Action-2
:parameters ()
:precondition
(and
(LEFTOF11-ROBOT)
)
:effect
(and
(Flag19-)
(Flag19prime-)
)

)
(:action Flag19Action-3
:parameters ()
:precondition
(and
(LEFTOF9-ROBOT)
)
:effect
(and
(Flag19-)
(Flag19prime-)
)

)
(:action Flag19Action-4
:parameters ()
:precondition
(and
(LEFTOF2-ROBOT)
)
:effect
(and
(Flag19-)
(Flag19prime-)
)

)
(:action Flag19Action-5
:parameters ()
:precondition
(and
(LEFTOF13-ROBOT)
)
:effect
(and
(Flag19-)
(Flag19prime-)
)

)
(:action Flag19Action-6
:parameters ()
:precondition
(and
(LEFTOF1-ROBOT)
)
:effect
(and
(Flag19-)
(Flag19prime-)
)

)
(:action Flag19Action-7
:parameters ()
:precondition
(and
(LEFTOF12-ROBOT)
)
:effect
(and
(Flag19-)
(Flag19prime-)
)

)
(:action Flag19Action-8
:parameters ()
:precondition
(and
(LEFTOF6-ROBOT)
)
:effect
(and
(Flag19-)
(Flag19prime-)
)

)
(:action Flag19Action-9
:parameters ()
:precondition
(and
(LEFTOF5-ROBOT)
)
:effect
(and
(Flag19-)
(Flag19prime-)
)

)
(:action Flag19Action-10
:parameters ()
:precondition
(and
(LEFTOF16-ROBOT)
)
:effect
(and
(Flag19-)
(Flag19prime-)
)

)
(:action Flag19Action-11
:parameters ()
:precondition
(and
(LEFTOF14-ROBOT)
)
:effect
(and
(Flag19-)
(Flag19prime-)
)

)
(:action Flag19Action-12
:parameters ()
:precondition
(and
(LEFTOF3-ROBOT)
)
:effect
(and
(Flag19-)
(Flag19prime-)
)

)
(:action Flag19Action-13
:parameters ()
:precondition
(and
(LEFTOF4-ROBOT)
)
:effect
(and
(Flag19-)
(Flag19prime-)
)

)
(:action Flag19Action-14
:parameters ()
:precondition
(and
(LEFTOF7-ROBOT)
)
:effect
(and
(Flag19-)
(Flag19prime-)
)

)
(:action Flag19Action-15
:parameters ()
:precondition
(and
(LEFTOF8-ROBOT)
)
:effect
(and
(Flag19-)
(Flag19prime-)
)

)
(:action Prim19Action
:parameters ()
:precondition
(and
(not (LEFTOF15-ROBOT))
(not (LEFTOF10-ROBOT))
(not (LEFTOF11-ROBOT))
(not (LEFTOF9-ROBOT))
(not (LEFTOF2-ROBOT))
(not (LEFTOF13-ROBOT))
(not (LEFTOF1-ROBOT))
(not (LEFTOF12-ROBOT))
(not (LEFTOF6-ROBOT))
(not (LEFTOF5-ROBOT))
(not (LEFTOF16-ROBOT))
(not (LEFTOF14-ROBOT))
(not (LEFTOF3-ROBOT))
(not (LEFTOF4-ROBOT))
(not (LEFTOF7-ROBOT))
(not (LEFTOF8-ROBOT))
)
:effect
(and
(Flag19prime-)
(not (Flag19-))
)

)
(:action Flag20Action-0
:parameters ()
:precondition
(and
(LEFTOF15-ROBOT)
)
:effect
(and
(Flag20-)
(Flag20prime-)
)

)
(:action Flag20Action-1
:parameters ()
:precondition
(and
(LEFTOF10-ROBOT)
)
:effect
(and
(Flag20-)
(Flag20prime-)
)

)
(:action Flag20Action-2
:parameters ()
:precondition
(and
(LEFTOF17-ROBOT)
)
:effect
(and
(Flag20-)
(Flag20prime-)
)

)
(:action Flag20Action-3
:parameters ()
:precondition
(and
(LEFTOF11-ROBOT)
)
:effect
(and
(Flag20-)
(Flag20prime-)
)

)
(:action Flag20Action-4
:parameters ()
:precondition
(and
(LEFTOF9-ROBOT)
)
:effect
(and
(Flag20-)
(Flag20prime-)
)

)
(:action Flag20Action-5
:parameters ()
:precondition
(and
(LEFTOF2-ROBOT)
)
:effect
(and
(Flag20-)
(Flag20prime-)
)

)
(:action Flag20Action-6
:parameters ()
:precondition
(and
(LEFTOF13-ROBOT)
)
:effect
(and
(Flag20-)
(Flag20prime-)
)

)
(:action Flag20Action-7
:parameters ()
:precondition
(and
(LEFTOF1-ROBOT)
)
:effect
(and
(Flag20-)
(Flag20prime-)
)

)
(:action Flag20Action-8
:parameters ()
:precondition
(and
(LEFTOF12-ROBOT)
)
:effect
(and
(Flag20-)
(Flag20prime-)
)

)
(:action Flag20Action-9
:parameters ()
:precondition
(and
(LEFTOF6-ROBOT)
)
:effect
(and
(Flag20-)
(Flag20prime-)
)

)
(:action Flag20Action-10
:parameters ()
:precondition
(and
(LEFTOF5-ROBOT)
)
:effect
(and
(Flag20-)
(Flag20prime-)
)

)
(:action Flag20Action-11
:parameters ()
:precondition
(and
(LEFTOF16-ROBOT)
)
:effect
(and
(Flag20-)
(Flag20prime-)
)

)
(:action Flag20Action-12
:parameters ()
:precondition
(and
(LEFTOF14-ROBOT)
)
:effect
(and
(Flag20-)
(Flag20prime-)
)

)
(:action Flag20Action-13
:parameters ()
:precondition
(and
(LEFTOF3-ROBOT)
)
:effect
(and
(Flag20-)
(Flag20prime-)
)

)
(:action Flag20Action-14
:parameters ()
:precondition
(and
(LEFTOF4-ROBOT)
)
:effect
(and
(Flag20-)
(Flag20prime-)
)

)
(:action Flag20Action-15
:parameters ()
:precondition
(and
(LEFTOF7-ROBOT)
)
:effect
(and
(Flag20-)
(Flag20prime-)
)

)
(:action Flag20Action-16
:parameters ()
:precondition
(and
(LEFTOF8-ROBOT)
)
:effect
(and
(Flag20-)
(Flag20prime-)
)

)
(:action Prim20Action
:parameters ()
:precondition
(and
(not (LEFTOF15-ROBOT))
(not (LEFTOF10-ROBOT))
(not (LEFTOF17-ROBOT))
(not (LEFTOF11-ROBOT))
(not (LEFTOF9-ROBOT))
(not (LEFTOF2-ROBOT))
(not (LEFTOF13-ROBOT))
(not (LEFTOF1-ROBOT))
(not (LEFTOF12-ROBOT))
(not (LEFTOF6-ROBOT))
(not (LEFTOF5-ROBOT))
(not (LEFTOF16-ROBOT))
(not (LEFTOF14-ROBOT))
(not (LEFTOF3-ROBOT))
(not (LEFTOF4-ROBOT))
(not (LEFTOF7-ROBOT))
(not (LEFTOF8-ROBOT))
)
:effect
(and
(Flag20prime-)
(not (Flag20-))
)

)
(:action Flag21Action-0
:parameters ()
:precondition
(and
(LEFTOF15-ROBOT)
)
:effect
(and
(Flag21-)
(Flag21prime-)
)

)
(:action Flag21Action-1
:parameters ()
:precondition
(and
(LEFTOF10-ROBOT)
)
:effect
(and
(Flag21-)
(Flag21prime-)
)

)
(:action Flag21Action-3
:parameters ()
:precondition
(and
(LEFTOF17-ROBOT)
)
:effect
(and
(Flag21-)
(Flag21prime-)
)

)
(:action Flag21Action-4
:parameters ()
:precondition
(and
(LEFTOF11-ROBOT)
)
:effect
(and
(Flag21-)
(Flag21prime-)
)

)
(:action Flag21Action-5
:parameters ()
:precondition
(and
(LEFTOF9-ROBOT)
)
:effect
(and
(Flag21-)
(Flag21prime-)
)

)
(:action Flag21Action-6
:parameters ()
:precondition
(and
(LEFTOF2-ROBOT)
)
:effect
(and
(Flag21-)
(Flag21prime-)
)

)
(:action Flag21Action-7
:parameters ()
:precondition
(and
(LEFTOF13-ROBOT)
)
:effect
(and
(Flag21-)
(Flag21prime-)
)

)
(:action Flag21Action-8
:parameters ()
:precondition
(and
(LEFTOF1-ROBOT)
)
:effect
(and
(Flag21-)
(Flag21prime-)
)

)
(:action Flag21Action-9
:parameters ()
:precondition
(and
(LEFTOF12-ROBOT)
)
:effect
(and
(Flag21-)
(Flag21prime-)
)

)
(:action Flag21Action-10
:parameters ()
:precondition
(and
(LEFTOF6-ROBOT)
)
:effect
(and
(Flag21-)
(Flag21prime-)
)

)
(:action Flag21Action-11
:parameters ()
:precondition
(and
(LEFTOF5-ROBOT)
)
:effect
(and
(Flag21-)
(Flag21prime-)
)

)
(:action Flag21Action-12
:parameters ()
:precondition
(and
(LEFTOF16-ROBOT)
)
:effect
(and
(Flag21-)
(Flag21prime-)
)

)
(:action Flag21Action-13
:parameters ()
:precondition
(and
(LEFTOF14-ROBOT)
)
:effect
(and
(Flag21-)
(Flag21prime-)
)

)
(:action Flag21Action-14
:parameters ()
:precondition
(and
(LEFTOF3-ROBOT)
)
:effect
(and
(Flag21-)
(Flag21prime-)
)

)
(:action Flag21Action-15
:parameters ()
:precondition
(and
(LEFTOF4-ROBOT)
)
:effect
(and
(Flag21-)
(Flag21prime-)
)

)
(:action Flag21Action-16
:parameters ()
:precondition
(and
(LEFTOF7-ROBOT)
)
:effect
(and
(Flag21-)
(Flag21prime-)
)

)
(:action Flag21Action-17
:parameters ()
:precondition
(and
(LEFTOF8-ROBOT)
)
:effect
(and
(Flag21-)
(Flag21prime-)
)

)
(:action Prim21Action
:parameters ()
:precondition
(and
(not (LEFTOF15-ROBOT))
(not (LEFTOF10-ROBOT))
(not (LEFTOF18-ROBOT))
(not (LEFTOF17-ROBOT))
(not (LEFTOF11-ROBOT))
(not (LEFTOF9-ROBOT))
(not (LEFTOF2-ROBOT))
(not (LEFTOF13-ROBOT))
(not (LEFTOF1-ROBOT))
(not (LEFTOF12-ROBOT))
(not (LEFTOF6-ROBOT))
(not (LEFTOF5-ROBOT))
(not (LEFTOF16-ROBOT))
(not (LEFTOF14-ROBOT))
(not (LEFTOF3-ROBOT))
(not (LEFTOF4-ROBOT))
(not (LEFTOF7-ROBOT))
(not (LEFTOF8-ROBOT))
)
:effect
(and
(Flag21prime-)
(not (Flag21-))
)

)
(:action Flag22Action-0
:parameters ()
:precondition
(and
(LEFTOF15-ROBOT)
)
:effect
(and
(Flag22-)
(Flag22prime-)
)

)
(:action Flag22Action-1
:parameters ()
:precondition
(and
(LEFTOF10-ROBOT)
)
:effect
(and
(Flag22-)
(Flag22prime-)
)

)
(:action Flag22Action-2
:parameters ()
:precondition
(and
(LEFTOF1-ROBOT)
)
:effect
(and
(Flag22-)
(Flag22prime-)
)

)
(:action Flag22Action-4
:parameters ()
:precondition
(and
(LEFTOF17-ROBOT)
)
:effect
(and
(Flag22-)
(Flag22prime-)
)

)
(:action Flag22Action-5
:parameters ()
:precondition
(and
(LEFTOF11-ROBOT)
)
:effect
(and
(Flag22-)
(Flag22prime-)
)

)
(:action Flag22Action-6
:parameters ()
:precondition
(and
(LEFTOF9-ROBOT)
)
:effect
(and
(Flag22-)
(Flag22prime-)
)

)
(:action Flag22Action-7
:parameters ()
:precondition
(and
(LEFTOF2-ROBOT)
)
:effect
(and
(Flag22-)
(Flag22prime-)
)

)
(:action Flag22Action-8
:parameters ()
:precondition
(and
(LEFTOF13-ROBOT)
)
:effect
(and
(Flag22-)
(Flag22prime-)
)

)
(:action Flag22Action-9
:parameters ()
:precondition
(and
(LEFTOF19-ROBOT)
)
:effect
(and
(Flag22-)
(Flag22prime-)
)

)
(:action Flag22Action-10
:parameters ()
:precondition
(and
(LEFTOF12-ROBOT)
)
:effect
(and
(Flag22-)
(Flag22prime-)
)

)
(:action Flag22Action-11
:parameters ()
:precondition
(and
(LEFTOF6-ROBOT)
)
:effect
(and
(Flag22-)
(Flag22prime-)
)

)
(:action Flag22Action-12
:parameters ()
:precondition
(and
(LEFTOF5-ROBOT)
)
:effect
(and
(Flag22-)
(Flag22prime-)
)

)
(:action Flag22Action-13
:parameters ()
:precondition
(and
(LEFTOF16-ROBOT)
)
:effect
(and
(Flag22-)
(Flag22prime-)
)

)
(:action Flag22Action-14
:parameters ()
:precondition
(and
(LEFTOF14-ROBOT)
)
:effect
(and
(Flag22-)
(Flag22prime-)
)

)
(:action Flag22Action-15
:parameters ()
:precondition
(and
(LEFTOF3-ROBOT)
)
:effect
(and
(Flag22-)
(Flag22prime-)
)

)
(:action Flag22Action-16
:parameters ()
:precondition
(and
(LEFTOF4-ROBOT)
)
:effect
(and
(Flag22-)
(Flag22prime-)
)

)
(:action Flag22Action-17
:parameters ()
:precondition
(and
(LEFTOF7-ROBOT)
)
:effect
(and
(Flag22-)
(Flag22prime-)
)

)
(:action Flag22Action-18
:parameters ()
:precondition
(and
(LEFTOF8-ROBOT)
)
:effect
(and
(Flag22-)
(Flag22prime-)
)

)
(:action Prim22Action
:parameters ()
:precondition
(and
(not (LEFTOF15-ROBOT))
(not (LEFTOF10-ROBOT))
(not (LEFTOF1-ROBOT))
(not (LEFTOF18-ROBOT))
(not (LEFTOF17-ROBOT))
(not (LEFTOF11-ROBOT))
(not (LEFTOF9-ROBOT))
(not (LEFTOF2-ROBOT))
(not (LEFTOF13-ROBOT))
(not (LEFTOF19-ROBOT))
(not (LEFTOF12-ROBOT))
(not (LEFTOF6-ROBOT))
(not (LEFTOF5-ROBOT))
(not (LEFTOF16-ROBOT))
(not (LEFTOF14-ROBOT))
(not (LEFTOF3-ROBOT))
(not (LEFTOF4-ROBOT))
(not (LEFTOF7-ROBOT))
(not (LEFTOF8-ROBOT))
)
:effect
(and
(Flag22prime-)
(not (Flag22-))
)

)
(:action Flag23Action-0
:parameters ()
:precondition
(and
(RIGHTOF3-ROBOT)
)
:effect
(and
(Flag23-)
(Flag23prime-)
)

)
(:action Flag23Action-1
:parameters ()
:precondition
(and
(RIGHTOF8-ROBOT)
)
:effect
(and
(Flag23-)
(Flag23prime-)
)

)
(:action Flag23Action-2
:parameters ()
:precondition
(and
(RIGHTOF5-ROBOT)
)
:effect
(and
(Flag23-)
(Flag23prime-)
)

)
(:action Flag23Action-3
:parameters ()
:precondition
(and
(RIGHTOF9-ROBOT)
)
:effect
(and
(Flag23-)
(Flag23prime-)
)

)
(:action Flag23Action-5
:parameters ()
:precondition
(and
(RIGHTOF17-ROBOT)
)
:effect
(and
(Flag23-)
(Flag23prime-)
)

)
(:action Flag23Action-6
:parameters ()
:precondition
(and
(RIGHTOF14-ROBOT)
)
:effect
(and
(Flag23-)
(Flag23prime-)
)

)
(:action Flag23Action-7
:parameters ()
:precondition
(and
(RIGHTOF13-ROBOT)
)
:effect
(and
(Flag23-)
(Flag23prime-)
)

)
(:action Flag23Action-8
:parameters ()
:precondition
(and
(RIGHTOF12-ROBOT)
)
:effect
(and
(Flag23-)
(Flag23prime-)
)

)
(:action Flag23Action-9
:parameters ()
:precondition
(and
(RIGHTOF10-ROBOT)
)
:effect
(and
(Flag23-)
(Flag23prime-)
)

)
(:action Flag23Action-10
:parameters ()
:precondition
(and
(RIGHTOF4-ROBOT)
)
:effect
(and
(Flag23-)
(Flag23prime-)
)

)
(:action Flag23Action-11
:parameters ()
:precondition
(and
(RIGHTOF1-ROBOT)
)
:effect
(and
(Flag23-)
(Flag23prime-)
)

)
(:action Flag23Action-12
:parameters ()
:precondition
(and
(RIGHTOF16-ROBOT)
)
:effect
(and
(Flag23-)
(Flag23prime-)
)

)
(:action Flag23Action-13
:parameters ()
:precondition
(and
(RIGHTOF18-ROBOT)
)
:effect
(and
(Flag23-)
(Flag23prime-)
)

)
(:action Flag23Action-14
:parameters ()
:precondition
(and
(RIGHTOF7-ROBOT)
)
:effect
(and
(Flag23-)
(Flag23prime-)
)

)
(:action Flag23Action-15
:parameters ()
:precondition
(and
(RIGHTOF11-ROBOT)
)
:effect
(and
(Flag23-)
(Flag23prime-)
)

)
(:action Flag23Action-16
:parameters ()
:precondition
(and
(RIGHTOF15-ROBOT)
)
:effect
(and
(Flag23-)
(Flag23prime-)
)

)
(:action Flag23Action-17
:parameters ()
:precondition
(and
(RIGHTOF6-ROBOT)
)
:effect
(and
(Flag23-)
(Flag23prime-)
)

)
(:action Prim23Action
:parameters ()
:precondition
(and
(not (RIGHTOF3-ROBOT))
(not (RIGHTOF8-ROBOT))
(not (RIGHTOF5-ROBOT))
(not (RIGHTOF9-ROBOT))
(not (RIGHTOF2-ROBOT))
(not (RIGHTOF17-ROBOT))
(not (RIGHTOF14-ROBOT))
(not (RIGHTOF13-ROBOT))
(not (RIGHTOF12-ROBOT))
(not (RIGHTOF10-ROBOT))
(not (RIGHTOF4-ROBOT))
(not (RIGHTOF1-ROBOT))
(not (RIGHTOF16-ROBOT))
(not (RIGHTOF18-ROBOT))
(not (RIGHTOF7-ROBOT))
(not (RIGHTOF11-ROBOT))
(not (RIGHTOF15-ROBOT))
(not (RIGHTOF6-ROBOT))
)
:effect
(and
(Flag23prime-)
(not (Flag23-))
)

)
(:action Flag24Action-0
:parameters ()
:precondition
(and
(RIGHTOF3-ROBOT)
)
:effect
(and
(Flag24-)
(Flag24prime-)
)

)
(:action Flag24Action-1
:parameters ()
:precondition
(and
(RIGHTOF5-ROBOT)
)
:effect
(and
(Flag24-)
(Flag24prime-)
)

)
(:action Flag24Action-2
:parameters ()
:precondition
(and
(RIGHTOF9-ROBOT)
)
:effect
(and
(Flag24-)
(Flag24prime-)
)

)
(:action Flag24Action-4
:parameters ()
:precondition
(and
(RIGHTOF17-ROBOT)
)
:effect
(and
(Flag24-)
(Flag24prime-)
)

)
(:action Flag24Action-5
:parameters ()
:precondition
(and
(RIGHTOF14-ROBOT)
)
:effect
(and
(Flag24-)
(Flag24prime-)
)

)
(:action Flag24Action-6
:parameters ()
:precondition
(and
(RIGHTOF13-ROBOT)
)
:effect
(and
(Flag24-)
(Flag24prime-)
)

)
(:action Flag24Action-7
:parameters ()
:precondition
(and
(RIGHTOF12-ROBOT)
)
:effect
(and
(Flag24-)
(Flag24prime-)
)

)
(:action Flag24Action-8
:parameters ()
:precondition
(and
(RIGHTOF10-ROBOT)
)
:effect
(and
(Flag24-)
(Flag24prime-)
)

)
(:action Flag24Action-9
:parameters ()
:precondition
(and
(RIGHTOF4-ROBOT)
)
:effect
(and
(Flag24-)
(Flag24prime-)
)

)
(:action Flag24Action-10
:parameters ()
:precondition
(and
(RIGHTOF8-ROBOT)
)
:effect
(and
(Flag24-)
(Flag24prime-)
)

)
(:action Flag24Action-11
:parameters ()
:precondition
(and
(RIGHTOF16-ROBOT)
)
:effect
(and
(Flag24-)
(Flag24prime-)
)

)
(:action Flag24Action-12
:parameters ()
:precondition
(and
(RIGHTOF18-ROBOT)
)
:effect
(and
(Flag24-)
(Flag24prime-)
)

)
(:action Flag24Action-13
:parameters ()
:precondition
(and
(RIGHTOF7-ROBOT)
)
:effect
(and
(Flag24-)
(Flag24prime-)
)

)
(:action Flag24Action-14
:parameters ()
:precondition
(and
(RIGHTOF11-ROBOT)
)
:effect
(and
(Flag24-)
(Flag24prime-)
)

)
(:action Flag24Action-15
:parameters ()
:precondition
(and
(RIGHTOF15-ROBOT)
)
:effect
(and
(Flag24-)
(Flag24prime-)
)

)
(:action Flag24Action-16
:parameters ()
:precondition
(and
(RIGHTOF6-ROBOT)
)
:effect
(and
(Flag24-)
(Flag24prime-)
)

)
(:action Prim24Action
:parameters ()
:precondition
(and
(not (RIGHTOF3-ROBOT))
(not (RIGHTOF5-ROBOT))
(not (RIGHTOF9-ROBOT))
(not (RIGHTOF2-ROBOT))
(not (RIGHTOF17-ROBOT))
(not (RIGHTOF14-ROBOT))
(not (RIGHTOF13-ROBOT))
(not (RIGHTOF12-ROBOT))
(not (RIGHTOF10-ROBOT))
(not (RIGHTOF4-ROBOT))
(not (RIGHTOF8-ROBOT))
(not (RIGHTOF16-ROBOT))
(not (RIGHTOF18-ROBOT))
(not (RIGHTOF7-ROBOT))
(not (RIGHTOF11-ROBOT))
(not (RIGHTOF15-ROBOT))
(not (RIGHTOF6-ROBOT))
)
:effect
(and
(Flag24prime-)
(not (Flag24-))
)

)
(:action Flag25Action-0
:parameters ()
:precondition
(and
(RIGHTOF7-ROBOT)
)
:effect
(and
(Flag25-)
(Flag25prime-)
)

)
(:action Flag25Action-1
:parameters ()
:precondition
(and
(RIGHTOF3-ROBOT)
)
:effect
(and
(Flag25-)
(Flag25prime-)
)

)
(:action Flag25Action-2
:parameters ()
:precondition
(and
(RIGHTOF5-ROBOT)
)
:effect
(and
(Flag25-)
(Flag25prime-)
)

)
(:action Flag25Action-3
:parameters ()
:precondition
(and
(RIGHTOF9-ROBOT)
)
:effect
(and
(Flag25-)
(Flag25prime-)
)

)
(:action Flag25Action-4
:parameters ()
:precondition
(and
(RIGHTOF17-ROBOT)
)
:effect
(and
(Flag25-)
(Flag25prime-)
)

)
(:action Flag25Action-5
:parameters ()
:precondition
(and
(RIGHTOF14-ROBOT)
)
:effect
(and
(Flag25-)
(Flag25prime-)
)

)
(:action Flag25Action-6
:parameters ()
:precondition
(and
(RIGHTOF13-ROBOT)
)
:effect
(and
(Flag25-)
(Flag25prime-)
)

)
(:action Flag25Action-7
:parameters ()
:precondition
(and
(RIGHTOF12-ROBOT)
)
:effect
(and
(Flag25-)
(Flag25prime-)
)

)
(:action Flag25Action-8
:parameters ()
:precondition
(and
(RIGHTOF10-ROBOT)
)
:effect
(and
(Flag25-)
(Flag25prime-)
)

)
(:action Flag25Action-9
:parameters ()
:precondition
(and
(RIGHTOF4-ROBOT)
)
:effect
(and
(Flag25-)
(Flag25prime-)
)

)
(:action Flag25Action-10
:parameters ()
:precondition
(and
(RIGHTOF8-ROBOT)
)
:effect
(and
(Flag25-)
(Flag25prime-)
)

)
(:action Flag25Action-11
:parameters ()
:precondition
(and
(RIGHTOF16-ROBOT)
)
:effect
(and
(Flag25-)
(Flag25prime-)
)

)
(:action Flag25Action-12
:parameters ()
:precondition
(and
(RIGHTOF18-ROBOT)
)
:effect
(and
(Flag25-)
(Flag25prime-)
)

)
(:action Flag25Action-13
:parameters ()
:precondition
(and
(RIGHTOF11-ROBOT)
)
:effect
(and
(Flag25-)
(Flag25prime-)
)

)
(:action Flag25Action-14
:parameters ()
:precondition
(and
(RIGHTOF15-ROBOT)
)
:effect
(and
(Flag25-)
(Flag25prime-)
)

)
(:action Flag25Action-15
:parameters ()
:precondition
(and
(RIGHTOF6-ROBOT)
)
:effect
(and
(Flag25-)
(Flag25prime-)
)

)
(:action Prim25Action
:parameters ()
:precondition
(and
(not (RIGHTOF7-ROBOT))
(not (RIGHTOF3-ROBOT))
(not (RIGHTOF5-ROBOT))
(not (RIGHTOF9-ROBOT))
(not (RIGHTOF17-ROBOT))
(not (RIGHTOF14-ROBOT))
(not (RIGHTOF13-ROBOT))
(not (RIGHTOF12-ROBOT))
(not (RIGHTOF10-ROBOT))
(not (RIGHTOF4-ROBOT))
(not (RIGHTOF8-ROBOT))
(not (RIGHTOF16-ROBOT))
(not (RIGHTOF18-ROBOT))
(not (RIGHTOF11-ROBOT))
(not (RIGHTOF15-ROBOT))
(not (RIGHTOF6-ROBOT))
)
:effect
(and
(Flag25prime-)
(not (Flag25-))
)

)
(:action Flag26Action-0
:parameters ()
:precondition
(and
(RIGHTOF7-ROBOT)
)
:effect
(and
(Flag26-)
(Flag26prime-)
)

)
(:action Flag26Action-1
:parameters ()
:precondition
(and
(RIGHTOF5-ROBOT)
)
:effect
(and
(Flag26-)
(Flag26prime-)
)

)
(:action Flag26Action-2
:parameters ()
:precondition
(and
(RIGHTOF9-ROBOT)
)
:effect
(and
(Flag26-)
(Flag26prime-)
)

)
(:action Flag26Action-3
:parameters ()
:precondition
(and
(RIGHTOF17-ROBOT)
)
:effect
(and
(Flag26-)
(Flag26prime-)
)

)
(:action Flag26Action-4
:parameters ()
:precondition
(and
(RIGHTOF14-ROBOT)
)
:effect
(and
(Flag26-)
(Flag26prime-)
)

)
(:action Flag26Action-5
:parameters ()
:precondition
(and
(RIGHTOF13-ROBOT)
)
:effect
(and
(Flag26-)
(Flag26prime-)
)

)
(:action Flag26Action-6
:parameters ()
:precondition
(and
(RIGHTOF12-ROBOT)
)
:effect
(and
(Flag26-)
(Flag26prime-)
)

)
(:action Flag26Action-7
:parameters ()
:precondition
(and
(RIGHTOF10-ROBOT)
)
:effect
(and
(Flag26-)
(Flag26prime-)
)

)
(:action Flag26Action-8
:parameters ()
:precondition
(and
(RIGHTOF4-ROBOT)
)
:effect
(and
(Flag26-)
(Flag26prime-)
)

)
(:action Flag26Action-9
:parameters ()
:precondition
(and
(RIGHTOF8-ROBOT)
)
:effect
(and
(Flag26-)
(Flag26prime-)
)

)
(:action Flag26Action-10
:parameters ()
:precondition
(and
(RIGHTOF16-ROBOT)
)
:effect
(and
(Flag26-)
(Flag26prime-)
)

)
(:action Flag26Action-11
:parameters ()
:precondition
(and
(RIGHTOF18-ROBOT)
)
:effect
(and
(Flag26-)
(Flag26prime-)
)

)
(:action Flag26Action-12
:parameters ()
:precondition
(and
(RIGHTOF11-ROBOT)
)
:effect
(and
(Flag26-)
(Flag26prime-)
)

)
(:action Flag26Action-13
:parameters ()
:precondition
(and
(RIGHTOF15-ROBOT)
)
:effect
(and
(Flag26-)
(Flag26prime-)
)

)
(:action Flag26Action-14
:parameters ()
:precondition
(and
(RIGHTOF6-ROBOT)
)
:effect
(and
(Flag26-)
(Flag26prime-)
)

)
(:action Prim26Action
:parameters ()
:precondition
(and
(not (RIGHTOF7-ROBOT))
(not (RIGHTOF5-ROBOT))
(not (RIGHTOF9-ROBOT))
(not (RIGHTOF17-ROBOT))
(not (RIGHTOF14-ROBOT))
(not (RIGHTOF13-ROBOT))
(not (RIGHTOF12-ROBOT))
(not (RIGHTOF10-ROBOT))
(not (RIGHTOF4-ROBOT))
(not (RIGHTOF8-ROBOT))
(not (RIGHTOF16-ROBOT))
(not (RIGHTOF18-ROBOT))
(not (RIGHTOF11-ROBOT))
(not (RIGHTOF15-ROBOT))
(not (RIGHTOF6-ROBOT))
)
:effect
(and
(Flag26prime-)
(not (Flag26-))
)

)
(:action Flag27Action-0
:parameters ()
:precondition
(and
(RIGHTOF7-ROBOT)
)
:effect
(and
(Flag27-)
(Flag27prime-)
)

)
(:action Flag27Action-1
:parameters ()
:precondition
(and
(RIGHTOF5-ROBOT)
)
:effect
(and
(Flag27-)
(Flag27prime-)
)

)
(:action Flag27Action-2
:parameters ()
:precondition
(and
(RIGHTOF9-ROBOT)
)
:effect
(and
(Flag27-)
(Flag27prime-)
)

)
(:action Flag27Action-3
:parameters ()
:precondition
(and
(RIGHTOF17-ROBOT)
)
:effect
(and
(Flag27-)
(Flag27prime-)
)

)
(:action Flag27Action-4
:parameters ()
:precondition
(and
(RIGHTOF14-ROBOT)
)
:effect
(and
(Flag27-)
(Flag27prime-)
)

)
(:action Flag27Action-5
:parameters ()
:precondition
(and
(RIGHTOF13-ROBOT)
)
:effect
(and
(Flag27-)
(Flag27prime-)
)

)
(:action Flag27Action-6
:parameters ()
:precondition
(and
(RIGHTOF12-ROBOT)
)
:effect
(and
(Flag27-)
(Flag27prime-)
)

)
(:action Flag27Action-7
:parameters ()
:precondition
(and
(RIGHTOF10-ROBOT)
)
:effect
(and
(Flag27-)
(Flag27prime-)
)

)
(:action Flag27Action-8
:parameters ()
:precondition
(and
(RIGHTOF8-ROBOT)
)
:effect
(and
(Flag27-)
(Flag27prime-)
)

)
(:action Flag27Action-9
:parameters ()
:precondition
(and
(RIGHTOF16-ROBOT)
)
:effect
(and
(Flag27-)
(Flag27prime-)
)

)
(:action Flag27Action-10
:parameters ()
:precondition
(and
(RIGHTOF18-ROBOT)
)
:effect
(and
(Flag27-)
(Flag27prime-)
)

)
(:action Flag27Action-11
:parameters ()
:precondition
(and
(RIGHTOF11-ROBOT)
)
:effect
(and
(Flag27-)
(Flag27prime-)
)

)
(:action Flag27Action-12
:parameters ()
:precondition
(and
(RIGHTOF15-ROBOT)
)
:effect
(and
(Flag27-)
(Flag27prime-)
)

)
(:action Flag27Action-13
:parameters ()
:precondition
(and
(RIGHTOF6-ROBOT)
)
:effect
(and
(Flag27-)
(Flag27prime-)
)

)
(:action Prim27Action
:parameters ()
:precondition
(and
(not (RIGHTOF7-ROBOT))
(not (RIGHTOF5-ROBOT))
(not (RIGHTOF9-ROBOT))
(not (RIGHTOF17-ROBOT))
(not (RIGHTOF14-ROBOT))
(not (RIGHTOF13-ROBOT))
(not (RIGHTOF12-ROBOT))
(not (RIGHTOF10-ROBOT))
(not (RIGHTOF8-ROBOT))
(not (RIGHTOF16-ROBOT))
(not (RIGHTOF18-ROBOT))
(not (RIGHTOF11-ROBOT))
(not (RIGHTOF15-ROBOT))
(not (RIGHTOF6-ROBOT))
)
:effect
(and
(Flag27prime-)
(not (Flag27-))
)

)
(:action Flag28Action-0
:parameters ()
:precondition
(and
(RIGHTOF7-ROBOT)
)
:effect
(and
(Flag28-)
(Flag28prime-)
)

)
(:action Flag28Action-1
:parameters ()
:precondition
(and
(RIGHTOF9-ROBOT)
)
:effect
(and
(Flag28-)
(Flag28prime-)
)

)
(:action Flag28Action-2
:parameters ()
:precondition
(and
(RIGHTOF17-ROBOT)
)
:effect
(and
(Flag28-)
(Flag28prime-)
)

)
(:action Flag28Action-3
:parameters ()
:precondition
(and
(RIGHTOF14-ROBOT)
)
:effect
(and
(Flag28-)
(Flag28prime-)
)

)
(:action Flag28Action-4
:parameters ()
:precondition
(and
(RIGHTOF13-ROBOT)
)
:effect
(and
(Flag28-)
(Flag28prime-)
)

)
(:action Flag28Action-5
:parameters ()
:precondition
(and
(RIGHTOF12-ROBOT)
)
:effect
(and
(Flag28-)
(Flag28prime-)
)

)
(:action Flag28Action-6
:parameters ()
:precondition
(and
(RIGHTOF10-ROBOT)
)
:effect
(and
(Flag28-)
(Flag28prime-)
)

)
(:action Flag28Action-7
:parameters ()
:precondition
(and
(RIGHTOF8-ROBOT)
)
:effect
(and
(Flag28-)
(Flag28prime-)
)

)
(:action Flag28Action-8
:parameters ()
:precondition
(and
(RIGHTOF16-ROBOT)
)
:effect
(and
(Flag28-)
(Flag28prime-)
)

)
(:action Flag28Action-9
:parameters ()
:precondition
(and
(RIGHTOF18-ROBOT)
)
:effect
(and
(Flag28-)
(Flag28prime-)
)

)
(:action Flag28Action-10
:parameters ()
:precondition
(and
(RIGHTOF11-ROBOT)
)
:effect
(and
(Flag28-)
(Flag28prime-)
)

)
(:action Flag28Action-11
:parameters ()
:precondition
(and
(RIGHTOF15-ROBOT)
)
:effect
(and
(Flag28-)
(Flag28prime-)
)

)
(:action Flag28Action-12
:parameters ()
:precondition
(and
(RIGHTOF6-ROBOT)
)
:effect
(and
(Flag28-)
(Flag28prime-)
)

)
(:action Prim28Action
:parameters ()
:precondition
(and
(not (RIGHTOF7-ROBOT))
(not (RIGHTOF9-ROBOT))
(not (RIGHTOF17-ROBOT))
(not (RIGHTOF14-ROBOT))
(not (RIGHTOF13-ROBOT))
(not (RIGHTOF12-ROBOT))
(not (RIGHTOF10-ROBOT))
(not (RIGHTOF8-ROBOT))
(not (RIGHTOF16-ROBOT))
(not (RIGHTOF18-ROBOT))
(not (RIGHTOF11-ROBOT))
(not (RIGHTOF15-ROBOT))
(not (RIGHTOF6-ROBOT))
)
:effect
(and
(Flag28prime-)
(not (Flag28-))
)

)
(:action Flag29Action-0
:parameters ()
:precondition
(and
(RIGHTOF18-ROBOT)
)
:effect
(and
(Flag29-)
(Flag29prime-)
)

)
(:action Flag29Action-1
:parameters ()
:precondition
(and
(RIGHTOF17-ROBOT)
)
:effect
(and
(Flag29-)
(Flag29prime-)
)

)
(:action Flag29Action-2
:parameters ()
:precondition
(and
(RIGHTOF14-ROBOT)
)
:effect
(and
(Flag29-)
(Flag29prime-)
)

)
(:action Flag29Action-3
:parameters ()
:precondition
(and
(RIGHTOF13-ROBOT)
)
:effect
(and
(Flag29-)
(Flag29prime-)
)

)
(:action Flag29Action-4
:parameters ()
:precondition
(and
(RIGHTOF15-ROBOT)
)
:effect
(and
(Flag29-)
(Flag29prime-)
)

)
(:action Flag29Action-5
:parameters ()
:precondition
(and
(RIGHTOF10-ROBOT)
)
:effect
(and
(Flag29-)
(Flag29prime-)
)

)
(:action Flag29Action-6
:parameters ()
:precondition
(and
(RIGHTOF8-ROBOT)
)
:effect
(and
(Flag29-)
(Flag29prime-)
)

)
(:action Flag29Action-7
:parameters ()
:precondition
(and
(RIGHTOF12-ROBOT)
)
:effect
(and
(Flag29-)
(Flag29prime-)
)

)
(:action Flag29Action-8
:parameters ()
:precondition
(and
(RIGHTOF16-ROBOT)
)
:effect
(and
(Flag29-)
(Flag29prime-)
)

)
(:action Flag29Action-9
:parameters ()
:precondition
(and
(RIGHTOF7-ROBOT)
)
:effect
(and
(Flag29-)
(Flag29prime-)
)

)
(:action Flag29Action-10
:parameters ()
:precondition
(and
(RIGHTOF9-ROBOT)
)
:effect
(and
(Flag29-)
(Flag29prime-)
)

)
(:action Flag29Action-11
:parameters ()
:precondition
(and
(RIGHTOF11-ROBOT)
)
:effect
(and
(Flag29-)
(Flag29prime-)
)

)
(:action Prim29Action
:parameters ()
:precondition
(and
(not (RIGHTOF18-ROBOT))
(not (RIGHTOF17-ROBOT))
(not (RIGHTOF14-ROBOT))
(not (RIGHTOF13-ROBOT))
(not (RIGHTOF15-ROBOT))
(not (RIGHTOF10-ROBOT))
(not (RIGHTOF8-ROBOT))
(not (RIGHTOF12-ROBOT))
(not (RIGHTOF16-ROBOT))
(not (RIGHTOF7-ROBOT))
(not (RIGHTOF9-ROBOT))
(not (RIGHTOF11-ROBOT))
)
:effect
(and
(Flag29prime-)
(not (Flag29-))
)

)
(:action Flag30Action-0
:parameters ()
:precondition
(and
(RIGHTOF18-ROBOT)
)
:effect
(and
(Flag30-)
(Flag30prime-)
)

)
(:action Flag30Action-1
:parameters ()
:precondition
(and
(RIGHTOF17-ROBOT)
)
:effect
(and
(Flag30-)
(Flag30prime-)
)

)
(:action Flag30Action-2
:parameters ()
:precondition
(and
(RIGHTOF14-ROBOT)
)
:effect
(and
(Flag30-)
(Flag30prime-)
)

)
(:action Flag30Action-3
:parameters ()
:precondition
(and
(RIGHTOF13-ROBOT)
)
:effect
(and
(Flag30-)
(Flag30prime-)
)

)
(:action Flag30Action-4
:parameters ()
:precondition
(and
(RIGHTOF15-ROBOT)
)
:effect
(and
(Flag30-)
(Flag30prime-)
)

)
(:action Flag30Action-5
:parameters ()
:precondition
(and
(RIGHTOF10-ROBOT)
)
:effect
(and
(Flag30-)
(Flag30prime-)
)

)
(:action Flag30Action-6
:parameters ()
:precondition
(and
(RIGHTOF8-ROBOT)
)
:effect
(and
(Flag30-)
(Flag30prime-)
)

)
(:action Flag30Action-7
:parameters ()
:precondition
(and
(RIGHTOF16-ROBOT)
)
:effect
(and
(Flag30-)
(Flag30prime-)
)

)
(:action Flag30Action-8
:parameters ()
:precondition
(and
(RIGHTOF12-ROBOT)
)
:effect
(and
(Flag30-)
(Flag30prime-)
)

)
(:action Flag30Action-9
:parameters ()
:precondition
(and
(RIGHTOF9-ROBOT)
)
:effect
(and
(Flag30-)
(Flag30prime-)
)

)
(:action Flag30Action-10
:parameters ()
:precondition
(and
(RIGHTOF11-ROBOT)
)
:effect
(and
(Flag30-)
(Flag30prime-)
)

)
(:action Prim30Action
:parameters ()
:precondition
(and
(not (RIGHTOF18-ROBOT))
(not (RIGHTOF17-ROBOT))
(not (RIGHTOF14-ROBOT))
(not (RIGHTOF13-ROBOT))
(not (RIGHTOF15-ROBOT))
(not (RIGHTOF10-ROBOT))
(not (RIGHTOF8-ROBOT))
(not (RIGHTOF16-ROBOT))
(not (RIGHTOF12-ROBOT))
(not (RIGHTOF9-ROBOT))
(not (RIGHTOF11-ROBOT))
)
:effect
(and
(Flag30prime-)
(not (Flag30-))
)

)
(:action Flag31Action-0
:parameters ()
:precondition
(and
(RIGHTOF9-ROBOT)
)
:effect
(and
(Flag31-)
(Flag31prime-)
)

)
(:action Flag31Action-1
:parameters ()
:precondition
(and
(RIGHTOF17-ROBOT)
)
:effect
(and
(Flag31-)
(Flag31prime-)
)

)
(:action Flag31Action-2
:parameters ()
:precondition
(and
(RIGHTOF14-ROBOT)
)
:effect
(and
(Flag31-)
(Flag31prime-)
)

)
(:action Flag31Action-3
:parameters ()
:precondition
(and
(RIGHTOF13-ROBOT)
)
:effect
(and
(Flag31-)
(Flag31prime-)
)

)
(:action Flag31Action-4
:parameters ()
:precondition
(and
(RIGHTOF15-ROBOT)
)
:effect
(and
(Flag31-)
(Flag31prime-)
)

)
(:action Flag31Action-5
:parameters ()
:precondition
(and
(RIGHTOF10-ROBOT)
)
:effect
(and
(Flag31-)
(Flag31prime-)
)

)
(:action Flag31Action-6
:parameters ()
:precondition
(and
(RIGHTOF16-ROBOT)
)
:effect
(and
(Flag31-)
(Flag31prime-)
)

)
(:action Flag31Action-7
:parameters ()
:precondition
(and
(RIGHTOF12-ROBOT)
)
:effect
(and
(Flag31-)
(Flag31prime-)
)

)
(:action Flag31Action-8
:parameters ()
:precondition
(and
(RIGHTOF18-ROBOT)
)
:effect
(and
(Flag31-)
(Flag31prime-)
)

)
(:action Flag31Action-9
:parameters ()
:precondition
(and
(RIGHTOF11-ROBOT)
)
:effect
(and
(Flag31-)
(Flag31prime-)
)

)
(:action Prim31Action
:parameters ()
:precondition
(and
(not (RIGHTOF9-ROBOT))
(not (RIGHTOF17-ROBOT))
(not (RIGHTOF14-ROBOT))
(not (RIGHTOF13-ROBOT))
(not (RIGHTOF15-ROBOT))
(not (RIGHTOF10-ROBOT))
(not (RIGHTOF16-ROBOT))
(not (RIGHTOF12-ROBOT))
(not (RIGHTOF18-ROBOT))
(not (RIGHTOF11-ROBOT))
)
:effect
(and
(Flag31prime-)
(not (Flag31-))
)

)
(:action Flag32Action-0
:parameters ()
:precondition
(and
(RIGHTOF17-ROBOT)
)
:effect
(and
(Flag32-)
(Flag32prime-)
)

)
(:action Flag32Action-1
:parameters ()
:precondition
(and
(RIGHTOF14-ROBOT)
)
:effect
(and
(Flag32-)
(Flag32prime-)
)

)
(:action Flag32Action-2
:parameters ()
:precondition
(and
(RIGHTOF13-ROBOT)
)
:effect
(and
(Flag32-)
(Flag32prime-)
)

)
(:action Flag32Action-3
:parameters ()
:precondition
(and
(RIGHTOF15-ROBOT)
)
:effect
(and
(Flag32-)
(Flag32prime-)
)

)
(:action Flag32Action-4
:parameters ()
:precondition
(and
(RIGHTOF10-ROBOT)
)
:effect
(and
(Flag32-)
(Flag32prime-)
)

)
(:action Flag32Action-5
:parameters ()
:precondition
(and
(RIGHTOF16-ROBOT)
)
:effect
(and
(Flag32-)
(Flag32prime-)
)

)
(:action Flag32Action-6
:parameters ()
:precondition
(and
(RIGHTOF12-ROBOT)
)
:effect
(and
(Flag32-)
(Flag32prime-)
)

)
(:action Flag32Action-7
:parameters ()
:precondition
(and
(RIGHTOF18-ROBOT)
)
:effect
(and
(Flag32-)
(Flag32prime-)
)

)
(:action Flag32Action-8
:parameters ()
:precondition
(and
(RIGHTOF11-ROBOT)
)
:effect
(and
(Flag32-)
(Flag32prime-)
)

)
(:action Prim32Action
:parameters ()
:precondition
(and
(not (RIGHTOF17-ROBOT))
(not (RIGHTOF14-ROBOT))
(not (RIGHTOF13-ROBOT))
(not (RIGHTOF15-ROBOT))
(not (RIGHTOF10-ROBOT))
(not (RIGHTOF16-ROBOT))
(not (RIGHTOF12-ROBOT))
(not (RIGHTOF18-ROBOT))
(not (RIGHTOF11-ROBOT))
)
:effect
(and
(Flag32prime-)
(not (Flag32-))
)

)
(:action Flag33Action-0
:parameters ()
:precondition
(and
(RIGHTOF17-ROBOT)
)
:effect
(and
(Flag33-)
(Flag33prime-)
)

)
(:action Flag33Action-1
:parameters ()
:precondition
(and
(RIGHTOF14-ROBOT)
)
:effect
(and
(Flag33-)
(Flag33prime-)
)

)
(:action Flag33Action-2
:parameters ()
:precondition
(and
(RIGHTOF13-ROBOT)
)
:effect
(and
(Flag33-)
(Flag33prime-)
)

)
(:action Flag33Action-3
:parameters ()
:precondition
(and
(RIGHTOF15-ROBOT)
)
:effect
(and
(Flag33-)
(Flag33prime-)
)

)
(:action Flag33Action-4
:parameters ()
:precondition
(and
(RIGHTOF16-ROBOT)
)
:effect
(and
(Flag33-)
(Flag33prime-)
)

)
(:action Flag33Action-5
:parameters ()
:precondition
(and
(RIGHTOF12-ROBOT)
)
:effect
(and
(Flag33-)
(Flag33prime-)
)

)
(:action Flag33Action-6
:parameters ()
:precondition
(and
(RIGHTOF18-ROBOT)
)
:effect
(and
(Flag33-)
(Flag33prime-)
)

)
(:action Flag33Action-7
:parameters ()
:precondition
(and
(RIGHTOF11-ROBOT)
)
:effect
(and
(Flag33-)
(Flag33prime-)
)

)
(:action Prim33Action
:parameters ()
:precondition
(and
(not (RIGHTOF17-ROBOT))
(not (RIGHTOF14-ROBOT))
(not (RIGHTOF13-ROBOT))
(not (RIGHTOF15-ROBOT))
(not (RIGHTOF16-ROBOT))
(not (RIGHTOF12-ROBOT))
(not (RIGHTOF18-ROBOT))
(not (RIGHTOF11-ROBOT))
)
:effect
(and
(Flag33prime-)
(not (Flag33-))
)

)
(:action Flag34Action-0
:parameters ()
:precondition
(and
(RIGHTOF17-ROBOT)
)
:effect
(and
(Flag34-)
(Flag34prime-)
)

)
(:action Flag34Action-1
:parameters ()
:precondition
(and
(RIGHTOF14-ROBOT)
)
:effect
(and
(Flag34-)
(Flag34prime-)
)

)
(:action Flag34Action-2
:parameters ()
:precondition
(and
(RIGHTOF13-ROBOT)
)
:effect
(and
(Flag34-)
(Flag34prime-)
)

)
(:action Flag34Action-3
:parameters ()
:precondition
(and
(RIGHTOF12-ROBOT)
)
:effect
(and
(Flag34-)
(Flag34prime-)
)

)
(:action Flag34Action-4
:parameters ()
:precondition
(and
(RIGHTOF16-ROBOT)
)
:effect
(and
(Flag34-)
(Flag34prime-)
)

)
(:action Flag34Action-5
:parameters ()
:precondition
(and
(RIGHTOF18-ROBOT)
)
:effect
(and
(Flag34-)
(Flag34prime-)
)

)
(:action Flag34Action-6
:parameters ()
:precondition
(and
(RIGHTOF15-ROBOT)
)
:effect
(and
(Flag34-)
(Flag34prime-)
)

)
(:action Prim34Action
:parameters ()
:precondition
(and
(not (RIGHTOF17-ROBOT))
(not (RIGHTOF14-ROBOT))
(not (RIGHTOF13-ROBOT))
(not (RIGHTOF12-ROBOT))
(not (RIGHTOF16-ROBOT))
(not (RIGHTOF18-ROBOT))
(not (RIGHTOF15-ROBOT))
)
:effect
(and
(Flag34prime-)
(not (Flag34-))
)

)
(:action Flag35Action-0
:parameters ()
:precondition
(and
(RIGHTOF17-ROBOT)
)
:effect
(and
(Flag35-)
(Flag35prime-)
)

)
(:action Flag35Action-1
:parameters ()
:precondition
(and
(RIGHTOF14-ROBOT)
)
:effect
(and
(Flag35-)
(Flag35prime-)
)

)
(:action Flag35Action-2
:parameters ()
:precondition
(and
(RIGHTOF13-ROBOT)
)
:effect
(and
(Flag35-)
(Flag35prime-)
)

)
(:action Flag35Action-3
:parameters ()
:precondition
(and
(RIGHTOF15-ROBOT)
)
:effect
(and
(Flag35-)
(Flag35prime-)
)

)
(:action Flag35Action-4
:parameters ()
:precondition
(and
(RIGHTOF16-ROBOT)
)
:effect
(and
(Flag35-)
(Flag35prime-)
)

)
(:action Flag35Action-5
:parameters ()
:precondition
(and
(RIGHTOF18-ROBOT)
)
:effect
(and
(Flag35-)
(Flag35prime-)
)

)
(:action Prim35Action
:parameters ()
:precondition
(and
(not (RIGHTOF17-ROBOT))
(not (RIGHTOF14-ROBOT))
(not (RIGHTOF13-ROBOT))
(not (RIGHTOF15-ROBOT))
(not (RIGHTOF16-ROBOT))
(not (RIGHTOF18-ROBOT))
)
:effect
(and
(Flag35prime-)
(not (Flag35-))
)

)
(:action Flag36Action-0
:parameters ()
:precondition
(and
(RIGHTOF16-ROBOT)
)
:effect
(and
(Flag36-)
(Flag36prime-)
)

)
(:action Flag36Action-1
:parameters ()
:precondition
(and
(RIGHTOF17-ROBOT)
)
:effect
(and
(Flag36-)
(Flag36prime-)
)

)
(:action Flag36Action-2
:parameters ()
:precondition
(and
(RIGHTOF14-ROBOT)
)
:effect
(and
(Flag36-)
(Flag36prime-)
)

)
(:action Flag36Action-3
:parameters ()
:precondition
(and
(RIGHTOF18-ROBOT)
)
:effect
(and
(Flag36-)
(Flag36prime-)
)

)
(:action Flag36Action-4
:parameters ()
:precondition
(and
(RIGHTOF15-ROBOT)
)
:effect
(and
(Flag36-)
(Flag36prime-)
)

)
(:action Prim36Action
:parameters ()
:precondition
(and
(not (RIGHTOF16-ROBOT))
(not (RIGHTOF17-ROBOT))
(not (RIGHTOF14-ROBOT))
(not (RIGHTOF18-ROBOT))
(not (RIGHTOF15-ROBOT))
)
:effect
(and
(Flag36prime-)
(not (Flag36-))
)

)
(:action Flag37Action-0
:parameters ()
:precondition
(and
(RIGHTOF16-ROBOT)
)
:effect
(and
(Flag37-)
(Flag37prime-)
)

)
(:action Flag37Action-1
:parameters ()
:precondition
(and
(RIGHTOF17-ROBOT)
)
:effect
(and
(Flag37-)
(Flag37prime-)
)

)
(:action Flag37Action-2
:parameters ()
:precondition
(and
(RIGHTOF18-ROBOT)
)
:effect
(and
(Flag37-)
(Flag37prime-)
)

)
(:action Flag37Action-3
:parameters ()
:precondition
(and
(RIGHTOF15-ROBOT)
)
:effect
(and
(Flag37-)
(Flag37prime-)
)

)
(:action Prim37Action
:parameters ()
:precondition
(and
(not (RIGHTOF16-ROBOT))
(not (RIGHTOF17-ROBOT))
(not (RIGHTOF18-ROBOT))
(not (RIGHTOF15-ROBOT))
)
:effect
(and
(Flag37prime-)
(not (Flag37-))
)

)
(:action Flag38Action-0
:parameters ()
:precondition
(and
(RIGHTOF16-ROBOT)
)
:effect
(and
(Flag38-)
(Flag38prime-)
)

)
(:action Flag38Action-1
:parameters ()
:precondition
(and
(RIGHTOF17-ROBOT)
)
:effect
(and
(Flag38-)
(Flag38prime-)
)

)
(:action Flag38Action-2
:parameters ()
:precondition
(and
(RIGHTOF18-ROBOT)
)
:effect
(and
(Flag38-)
(Flag38prime-)
)

)
(:action Prim38Action
:parameters ()
:precondition
(and
(not (RIGHTOF16-ROBOT))
(not (RIGHTOF17-ROBOT))
(not (RIGHTOF18-ROBOT))
)
:effect
(and
(Flag38prime-)
(not (Flag38-))
)

)
(:action Flag39Action-0
:parameters ()
:precondition
(and
(RIGHTOF17-ROBOT)
)
:effect
(and
(Flag39-)
(Flag39prime-)
)

)
(:action Flag39Action-1
:parameters ()
:precondition
(and
(RIGHTOF18-ROBOT)
)
:effect
(and
(Flag39-)
(Flag39prime-)
)

)
(:action Prim39Action
:parameters ()
:precondition
(and
(not (RIGHTOF17-ROBOT))
(not (RIGHTOF18-ROBOT))
)
:effect
(and
(Flag39prime-)
(not (Flag39-))
)

)
(:action Flag40Action
:parameters ()
:precondition
(and
(LEFTOF2-ROBOT)
(RIGHTOF15-ROBOT)
)
:effect
(and
(Flag40-)
(Flag40prime-)
)

)
(:action Flag41Action
:parameters ()
:precondition
(and
(RIGHTOF18-ROBOT)
(LEFTOF2-ROBOT)
)
:effect
(and
(Flag41-)
(Flag41prime-)
)

)
(:action Flag42Action
:parameters ()
:precondition
(and
(RIGHTOF13-ROBOT)
(LEFTOF2-ROBOT)
)
:effect
(and
(Flag42-)
(Flag42prime-)
)

)
(:action Flag43Action
:parameters ()
:precondition
(and
(LEFTOF2-ROBOT)
(RIGHTOF9-ROBOT)
)
:effect
(and
(Flag43-)
(Flag43prime-)
)

)
(:action Flag44Action
:parameters ()
:precondition
(and
(LEFTOF1-ROBOT)
(RIGHTOF9-ROBOT)
)
:effect
(and
(Flag44-)
(Flag44prime-)
)

)
(:action Flag45Action
:parameters ()
:precondition
(and
(RIGHTOF3-ROBOT)
(LEFTOF1-ROBOT)
)
:effect
(and
(Flag45-)
(Flag45prime-)
)

)
(:action Flag46Action
:parameters ()
:precondition
(and
(RIGHTOF18-ROBOT)
(LEFTOF1-ROBOT)
)
:effect
(and
(Flag46-)
(Flag46prime-)
)

)
(:action Flag47Action
:parameters ()
:precondition
(and
(LEFTOF1-ROBOT)
(RIGHTOF15-ROBOT)
)
:effect
(and
(Flag47-)
(Flag47prime-)
)

)
(:action Flag48Action
:parameters ()
:precondition
(and
(RIGHTOF10-ROBOT)
(LEFTOF1-ROBOT)
)
:effect
(and
(Flag48-)
(Flag48prime-)
)

)
(:action Flag49Action
:parameters ()
:precondition
(and
(LEFTOF2-ROBOT)
(RIGHTOF1-ROBOT)
)
:effect
(and
(Flag49-)
(Flag49prime-)
)

)
(:action Flag50Action
:parameters ()
:precondition
(and
(RIGHTOF5-ROBOT)
(LEFTOF2-ROBOT)
)
:effect
(and
(Flag50-)
(Flag50prime-)
)

)
(:action Flag51Action
:parameters ()
:precondition
(and
(LEFTOF1-ROBOT)
(RIGHTOF1-ROBOT)
)
:effect
(and
(Flag51-)
(Flag51prime-)
)

)
(:action Flag52Action
:parameters ()
:precondition
(and
(LEFTOF2-ROBOT)
(RIGHTOF7-ROBOT)
)
:effect
(and
(Flag52-)
(Flag52prime-)
)

)
(:action Flag53Action
:parameters ()
:precondition
(and
(RIGHTOF16-ROBOT)
(LEFTOF2-ROBOT)
)
:effect
(and
(Flag53-)
(Flag53prime-)
)

)
(:action Flag54Action
:parameters ()
:precondition
(and
(RIGHTOF5-ROBOT)
(LEFTOF1-ROBOT)
)
:effect
(and
(Flag54-)
(Flag54prime-)
)

)
(:action Flag55Action
:parameters ()
:precondition
(and
(RIGHTOF11-ROBOT)
(LEFTOF1-ROBOT)
)
:effect
(and
(Flag55-)
(Flag55prime-)
)

)
(:action Flag56Action
:parameters ()
:precondition
(and
(RIGHTOF3-ROBOT)
(LEFTOF2-ROBOT)
)
:effect
(and
(Flag56-)
(Flag56prime-)
)

)
(:action Flag57Action
:parameters ()
:precondition
(and
(LEFTOF2-ROBOT)
(RIGHTOF17-ROBOT)
)
:effect
(and
(Flag57-)
(Flag57prime-)
)

)
(:action Flag58Action
:parameters ()
:precondition
(and
(LEFTOF2-ROBOT)
(RIGHTOF14-ROBOT)
)
:effect
(and
(Flag58-)
(Flag58prime-)
)

)
(:action Flag59Action
:parameters ()
:precondition
(and
(LEFTOF1-ROBOT)
(RIGHTOF4-ROBOT)
)
:effect
(and
(Flag59-)
(Flag59prime-)
)

)
(:action Flag60Action
:parameters ()
:precondition
(and
(RIGHTOF10-ROBOT)
(LEFTOF2-ROBOT)
)
:effect
(and
(Flag60-)
(Flag60prime-)
)

)
(:action Flag61Action
:parameters ()
:precondition
(and
(LEFTOF1-ROBOT)
(RIGHTOF12-ROBOT)
)
:effect
(and
(Flag61-)
(Flag61prime-)
)

)
(:action Flag62Action
:parameters ()
:precondition
(and
(LEFTOF1-ROBOT)
(RIGHTOF7-ROBOT)
)
:effect
(and
(Flag62-)
(Flag62prime-)
)

)
(:action Flag63Action
:parameters ()
:precondition
(and
(LEFTOF2-ROBOT)
(RIGHTOF12-ROBOT)
)
:effect
(and
(Flag63-)
(Flag63prime-)
)

)
(:action Flag64Action
:parameters ()
:precondition
(and
(LEFTOF1-ROBOT)
(RIGHTOF5-ROBOT)
)
:effect
(and
(Flag64-)
(Flag64prime-)
)

)
(:action Flag65Action
:parameters ()
:precondition
(and
(RIGHTOF8-ROBOT)
(LEFTOF1-ROBOT)
)
:effect
(and
(Flag65-)
(Flag65prime-)
)

)
(:action Flag66Action
:parameters ()
:precondition
(and
(LEFTOF1-ROBOT)
(RIGHTOF17-ROBOT)
)
:effect
(and
(Flag66-)
(Flag66prime-)
)

)
(:action Flag67Action
:parameters ()
:precondition
(and
(RIGHTOF13-ROBOT)
(LEFTOF1-ROBOT)
)
:effect
(and
(Flag67-)
(Flag67prime-)
)

)
(:action Flag68Action
:parameters ()
:precondition
(and
(RIGHTOF11-ROBOT)
(LEFTOF2-ROBOT)
)
:effect
(and
(Flag68-)
(Flag68prime-)
)

)
(:action Flag69Action
:parameters ()
:precondition
(and
(RIGHTOF8-ROBOT)
(LEFTOF2-ROBOT)
)
:effect
(and
(Flag69-)
(Flag69prime-)
)

)
(:action Flag70Action
:parameters ()
:precondition
(and
(RIGHTOF2-ROBOT)
(LEFTOF2-ROBOT)
)
:effect
(and
(Flag70-)
(Flag70prime-)
)

)
(:action Flag71Action
:parameters ()
:precondition
(and
(LEFTOF1-ROBOT)
(RIGHTOF2-ROBOT)
)
:effect
(and
(Flag71-)
(Flag71prime-)
)

)
(:action Flag72Action
:parameters ()
:precondition
(and
(LEFTOF1-ROBOT)
(RIGHTOF6-ROBOT)
)
:effect
(and
(Flag72-)
(Flag72prime-)
)

)
(:action Flag73Action
:parameters ()
:precondition
(and
(LEFTOF1-ROBOT)
(RIGHTOF14-ROBOT)
)
:effect
(and
(Flag73-)
(Flag73prime-)
)

)
(:action Flag74Action
:parameters ()
:precondition
(and
(LEFTOF1-ROBOT)
(RIGHTOF16-ROBOT)
)
:effect
(and
(Flag74-)
(Flag74prime-)
)

)
(:action Flag75Action
:parameters ()
:precondition
(and
(LEFTOF2-ROBOT)
(RIGHTOF6-ROBOT)
)
:effect
(and
(Flag75-)
(Flag75prime-)
)

)
(:action Flag76Action
:parameters ()
:precondition
(and
(LEFTOF2-ROBOT)
(RIGHTOF4-ROBOT)
)
:effect
(and
(Flag76-)
(Flag76prime-)
)

)
(:action Prim40Action-0
:parameters ()
:precondition
(and
(not (LEFTOF2-ROBOT))
)
:effect
(and
(Flag40prime-)
(not (Flag40-))
)

)
(:action Prim40Action-1
:parameters ()
:precondition
(and
(not (RIGHTOF15-ROBOT))
)
:effect
(and
(Flag40prime-)
(not (Flag40-))
)

)
(:action Prim41Action-0
:parameters ()
:precondition
(and
(not (RIGHTOF18-ROBOT))
)
:effect
(and
(Flag41prime-)
(not (Flag41-))
)

)
(:action Prim41Action-1
:parameters ()
:precondition
(and
(not (LEFTOF2-ROBOT))
)
:effect
(and
(Flag41prime-)
(not (Flag41-))
)

)
(:action Prim42Action-0
:parameters ()
:precondition
(and
(not (RIGHTOF13-ROBOT))
)
:effect
(and
(Flag42prime-)
(not (Flag42-))
)

)
(:action Prim42Action-1
:parameters ()
:precondition
(and
(not (LEFTOF2-ROBOT))
)
:effect
(and
(Flag42prime-)
(not (Flag42-))
)

)
(:action Prim43Action-0
:parameters ()
:precondition
(and
(not (LEFTOF2-ROBOT))
)
:effect
(and
(Flag43prime-)
(not (Flag43-))
)

)
(:action Prim43Action-1
:parameters ()
:precondition
(and
(not (RIGHTOF9-ROBOT))
)
:effect
(and
(Flag43prime-)
(not (Flag43-))
)

)
(:action Prim44Action-0
:parameters ()
:precondition
(and
(not (LEFTOF1-ROBOT))
)
:effect
(and
(Flag44prime-)
(not (Flag44-))
)

)
(:action Prim44Action-1
:parameters ()
:precondition
(and
(not (RIGHTOF9-ROBOT))
)
:effect
(and
(Flag44prime-)
(not (Flag44-))
)

)
(:action Prim45Action-0
:parameters ()
:precondition
(and
(not (RIGHTOF3-ROBOT))
)
:effect
(and
(Flag45prime-)
(not (Flag45-))
)

)
(:action Prim45Action-1
:parameters ()
:precondition
(and
(not (LEFTOF1-ROBOT))
)
:effect
(and
(Flag45prime-)
(not (Flag45-))
)

)
(:action Prim46Action-0
:parameters ()
:precondition
(and
(not (RIGHTOF18-ROBOT))
)
:effect
(and
(Flag46prime-)
(not (Flag46-))
)

)
(:action Prim46Action-1
:parameters ()
:precondition
(and
(not (LEFTOF1-ROBOT))
)
:effect
(and
(Flag46prime-)
(not (Flag46-))
)

)
(:action Prim47Action-0
:parameters ()
:precondition
(and
(not (LEFTOF1-ROBOT))
)
:effect
(and
(Flag47prime-)
(not (Flag47-))
)

)
(:action Prim47Action-1
:parameters ()
:precondition
(and
(not (RIGHTOF15-ROBOT))
)
:effect
(and
(Flag47prime-)
(not (Flag47-))
)

)
(:action Prim48Action-0
:parameters ()
:precondition
(and
(not (RIGHTOF10-ROBOT))
)
:effect
(and
(Flag48prime-)
(not (Flag48-))
)

)
(:action Prim48Action-1
:parameters ()
:precondition
(and
(not (LEFTOF1-ROBOT))
)
:effect
(and
(Flag48prime-)
(not (Flag48-))
)

)
(:action Prim49Action-0
:parameters ()
:precondition
(and
(not (LEFTOF2-ROBOT))
)
:effect
(and
(Flag49prime-)
(not (Flag49-))
)

)
(:action Prim49Action-1
:parameters ()
:precondition
(and
(not (RIGHTOF1-ROBOT))
)
:effect
(and
(Flag49prime-)
(not (Flag49-))
)

)
(:action Prim50Action-0
:parameters ()
:precondition
(and
(not (RIGHTOF5-ROBOT))
)
:effect
(and
(Flag50prime-)
(not (Flag50-))
)

)
(:action Prim50Action-1
:parameters ()
:precondition
(and
(not (LEFTOF2-ROBOT))
)
:effect
(and
(Flag50prime-)
(not (Flag50-))
)

)
(:action Prim51Action-0
:parameters ()
:precondition
(and
(not (LEFTOF1-ROBOT))
)
:effect
(and
(Flag51prime-)
(not (Flag51-))
)

)
(:action Prim51Action-1
:parameters ()
:precondition
(and
(not (RIGHTOF1-ROBOT))
)
:effect
(and
(Flag51prime-)
(not (Flag51-))
)

)
(:action Prim52Action-0
:parameters ()
:precondition
(and
(not (LEFTOF2-ROBOT))
)
:effect
(and
(Flag52prime-)
(not (Flag52-))
)

)
(:action Prim52Action-1
:parameters ()
:precondition
(and
(not (RIGHTOF7-ROBOT))
)
:effect
(and
(Flag52prime-)
(not (Flag52-))
)

)
(:action Prim53Action-0
:parameters ()
:precondition
(and
(not (RIGHTOF16-ROBOT))
)
:effect
(and
(Flag53prime-)
(not (Flag53-))
)

)
(:action Prim53Action-1
:parameters ()
:precondition
(and
(not (LEFTOF2-ROBOT))
)
:effect
(and
(Flag53prime-)
(not (Flag53-))
)

)
(:action Prim54Action-0
:parameters ()
:precondition
(and
(not (RIGHTOF5-ROBOT))
)
:effect
(and
(Flag54prime-)
(not (Flag54-))
)

)
(:action Prim54Action-1
:parameters ()
:precondition
(and
(not (LEFTOF1-ROBOT))
)
:effect
(and
(Flag54prime-)
(not (Flag54-))
)

)
(:action Prim55Action-0
:parameters ()
:precondition
(and
(not (RIGHTOF11-ROBOT))
)
:effect
(and
(Flag55prime-)
(not (Flag55-))
)

)
(:action Prim55Action-1
:parameters ()
:precondition
(and
(not (LEFTOF1-ROBOT))
)
:effect
(and
(Flag55prime-)
(not (Flag55-))
)

)
(:action Prim56Action-0
:parameters ()
:precondition
(and
(not (RIGHTOF3-ROBOT))
)
:effect
(and
(Flag56prime-)
(not (Flag56-))
)

)
(:action Prim56Action-1
:parameters ()
:precondition
(and
(not (LEFTOF2-ROBOT))
)
:effect
(and
(Flag56prime-)
(not (Flag56-))
)

)
(:action Prim57Action-0
:parameters ()
:precondition
(and
(not (LEFTOF2-ROBOT))
)
:effect
(and
(Flag57prime-)
(not (Flag57-))
)

)
(:action Prim57Action-1
:parameters ()
:precondition
(and
(not (RIGHTOF17-ROBOT))
)
:effect
(and
(Flag57prime-)
(not (Flag57-))
)

)
(:action Prim58Action-0
:parameters ()
:precondition
(and
(not (LEFTOF2-ROBOT))
)
:effect
(and
(Flag58prime-)
(not (Flag58-))
)

)
(:action Prim58Action-1
:parameters ()
:precondition
(and
(not (RIGHTOF14-ROBOT))
)
:effect
(and
(Flag58prime-)
(not (Flag58-))
)

)
(:action Prim59Action-0
:parameters ()
:precondition
(and
(not (LEFTOF1-ROBOT))
)
:effect
(and
(Flag59prime-)
(not (Flag59-))
)

)
(:action Prim59Action-1
:parameters ()
:precondition
(and
(not (RIGHTOF4-ROBOT))
)
:effect
(and
(Flag59prime-)
(not (Flag59-))
)

)
(:action Prim60Action-0
:parameters ()
:precondition
(and
(not (RIGHTOF10-ROBOT))
)
:effect
(and
(Flag60prime-)
(not (Flag60-))
)

)
(:action Prim60Action-1
:parameters ()
:precondition
(and
(not (LEFTOF2-ROBOT))
)
:effect
(and
(Flag60prime-)
(not (Flag60-))
)

)
(:action Prim61Action-0
:parameters ()
:precondition
(and
(not (LEFTOF1-ROBOT))
)
:effect
(and
(Flag61prime-)
(not (Flag61-))
)

)
(:action Prim61Action-1
:parameters ()
:precondition
(and
(not (RIGHTOF12-ROBOT))
)
:effect
(and
(Flag61prime-)
(not (Flag61-))
)

)
(:action Prim62Action-0
:parameters ()
:precondition
(and
(not (LEFTOF1-ROBOT))
)
:effect
(and
(Flag62prime-)
(not (Flag62-))
)

)
(:action Prim62Action-1
:parameters ()
:precondition
(and
(not (RIGHTOF7-ROBOT))
)
:effect
(and
(Flag62prime-)
(not (Flag62-))
)

)
(:action Prim63Action-0
:parameters ()
:precondition
(and
(not (LEFTOF2-ROBOT))
)
:effect
(and
(Flag63prime-)
(not (Flag63-))
)

)
(:action Prim63Action-1
:parameters ()
:precondition
(and
(not (RIGHTOF12-ROBOT))
)
:effect
(and
(Flag63prime-)
(not (Flag63-))
)

)
(:action Prim64Action-0
:parameters ()
:precondition
(and
(not (LEFTOF1-ROBOT))
)
:effect
(and
(Flag64prime-)
(not (Flag64-))
)

)
(:action Prim64Action-1
:parameters ()
:precondition
(and
(not (RIGHTOF5-ROBOT))
)
:effect
(and
(Flag64prime-)
(not (Flag64-))
)

)
(:action Prim65Action-0
:parameters ()
:precondition
(and
(not (RIGHTOF8-ROBOT))
)
:effect
(and
(Flag65prime-)
(not (Flag65-))
)

)
(:action Prim65Action-1
:parameters ()
:precondition
(and
(not (LEFTOF1-ROBOT))
)
:effect
(and
(Flag65prime-)
(not (Flag65-))
)

)
(:action Prim66Action-0
:parameters ()
:precondition
(and
(not (LEFTOF1-ROBOT))
)
:effect
(and
(Flag66prime-)
(not (Flag66-))
)

)
(:action Prim66Action-1
:parameters ()
:precondition
(and
(not (RIGHTOF17-ROBOT))
)
:effect
(and
(Flag66prime-)
(not (Flag66-))
)

)
(:action Prim67Action-0
:parameters ()
:precondition
(and
(not (RIGHTOF13-ROBOT))
)
:effect
(and
(Flag67prime-)
(not (Flag67-))
)

)
(:action Prim67Action-1
:parameters ()
:precondition
(and
(not (LEFTOF1-ROBOT))
)
:effect
(and
(Flag67prime-)
(not (Flag67-))
)

)
(:action Prim68Action-0
:parameters ()
:precondition
(and
(not (RIGHTOF11-ROBOT))
)
:effect
(and
(Flag68prime-)
(not (Flag68-))
)

)
(:action Prim68Action-1
:parameters ()
:precondition
(and
(not (LEFTOF2-ROBOT))
)
:effect
(and
(Flag68prime-)
(not (Flag68-))
)

)
(:action Prim69Action-0
:parameters ()
:precondition
(and
(not (RIGHTOF8-ROBOT))
)
:effect
(and
(Flag69prime-)
(not (Flag69-))
)

)
(:action Prim69Action-1
:parameters ()
:precondition
(and
(not (LEFTOF2-ROBOT))
)
:effect
(and
(Flag69prime-)
(not (Flag69-))
)

)
(:action Prim70Action-1
:parameters ()
:precondition
(and
(not (LEFTOF2-ROBOT))
)
:effect
(and
(Flag70prime-)
(not (Flag70-))
)

)
(:action Prim71Action-0
:parameters ()
:precondition
(and
(not (LEFTOF1-ROBOT))
)
:effect
(and
(Flag71prime-)
(not (Flag71-))
)

)
(:action Prim72Action-0
:parameters ()
:precondition
(and
(not (LEFTOF1-ROBOT))
)
:effect
(and
(Flag72prime-)
(not (Flag72-))
)

)
(:action Prim72Action-1
:parameters ()
:precondition
(and
(not (RIGHTOF6-ROBOT))
)
:effect
(and
(Flag72prime-)
(not (Flag72-))
)

)
(:action Prim73Action-0
:parameters ()
:precondition
(and
(not (LEFTOF1-ROBOT))
)
:effect
(and
(Flag73prime-)
(not (Flag73-))
)

)
(:action Prim73Action-1
:parameters ()
:precondition
(and
(not (RIGHTOF14-ROBOT))
)
:effect
(and
(Flag73prime-)
(not (Flag73-))
)

)
(:action Prim74Action-0
:parameters ()
:precondition
(and
(not (LEFTOF1-ROBOT))
)
:effect
(and
(Flag74prime-)
(not (Flag74-))
)

)
(:action Prim74Action-1
:parameters ()
:precondition
(and
(not (RIGHTOF16-ROBOT))
)
:effect
(and
(Flag74prime-)
(not (Flag74-))
)

)
(:action Prim75Action-0
:parameters ()
:precondition
(and
(not (LEFTOF2-ROBOT))
)
:effect
(and
(Flag75prime-)
(not (Flag75-))
)

)
(:action Prim75Action-1
:parameters ()
:precondition
(and
(not (RIGHTOF6-ROBOT))
)
:effect
(and
(Flag75prime-)
(not (Flag75-))
)

)
(:action Prim76Action-0
:parameters ()
:precondition
(and
(not (LEFTOF2-ROBOT))
)
:effect
(and
(Flag76prime-)
(not (Flag76-))
)

)
(:action Prim76Action-1
:parameters ()
:precondition
(and
(not (RIGHTOF4-ROBOT))
)
:effect
(and
(Flag76prime-)
(not (Flag76-))
)

)
(:action Flag78Action
:parameters ()
:precondition
(and
(LEFTOF3-ROBOT)
(RIGHTOF6-ROBOT)
)
:effect
(and
(Flag78-)
(Flag78prime-)
)

)
(:action Flag79Action
:parameters ()
:precondition
(and
(LEFTOF3-ROBOT)
(RIGHTOF14-ROBOT)
)
:effect
(and
(Flag79-)
(Flag79prime-)
)

)
(:action Flag80Action
:parameters ()
:precondition
(and
(LEFTOF3-ROBOT)
(RIGHTOF4-ROBOT)
)
:effect
(and
(Flag80-)
(Flag80prime-)
)

)
(:action Flag81Action
:parameters ()
:precondition
(and
(LEFTOF3-ROBOT)
(RIGHTOF16-ROBOT)
)
:effect
(and
(Flag81-)
(Flag81prime-)
)

)
(:action Flag82Action
:parameters ()
:precondition
(and
(LEFTOF3-ROBOT)
(RIGHTOF7-ROBOT)
)
:effect
(and
(Flag82-)
(Flag82prime-)
)

)
(:action Flag83Action
:parameters ()
:precondition
(and
(LEFTOF3-ROBOT)
(RIGHTOF12-ROBOT)
)
:effect
(and
(Flag83-)
(Flag83prime-)
)

)
(:action Flag84Action
:parameters ()
:precondition
(and
(LEFTOF3-ROBOT)
(RIGHTOF10-ROBOT)
)
:effect
(and
(Flag84-)
(Flag84prime-)
)

)
(:action Flag85Action
:parameters ()
:precondition
(and
(LEFTOF3-ROBOT)
(RIGHTOF8-ROBOT)
)
:effect
(and
(Flag85-)
(Flag85prime-)
)

)
(:action Flag86Action
:parameters ()
:precondition
(and
(LEFTOF3-ROBOT)
(RIGHTOF5-ROBOT)
)
:effect
(and
(Flag86-)
(Flag86prime-)
)

)
(:action Flag87Action
:parameters ()
:precondition
(and
(LEFTOF3-ROBOT)
(RIGHTOF15-ROBOT)
)
:effect
(and
(Flag87-)
(Flag87prime-)
)

)
(:action Flag88Action
:parameters ()
:precondition
(and
(RIGHTOF3-ROBOT)
(LEFTOF3-ROBOT)
)
:effect
(and
(Flag88-)
(Flag88prime-)
)

)
(:action Flag89Action
:parameters ()
:precondition
(and
(LEFTOF3-ROBOT)
(RIGHTOF11-ROBOT)
)
:effect
(and
(Flag89-)
(Flag89prime-)
)

)
(:action Flag90Action
:parameters ()
:precondition
(and
(LEFTOF3-ROBOT)
(RIGHTOF18-ROBOT)
)
:effect
(and
(Flag90-)
(Flag90prime-)
)

)
(:action Flag91Action
:parameters ()
:precondition
(and
(RIGHTOF13-ROBOT)
(LEFTOF3-ROBOT)
)
:effect
(and
(Flag91-)
(Flag91prime-)
)

)
(:action Flag92Action
:parameters ()
:precondition
(and
(LEFTOF3-ROBOT)
(RIGHTOF17-ROBOT)
)
:effect
(and
(Flag92-)
(Flag92prime-)
)

)
(:action Flag93Action
:parameters ()
:precondition
(and
(LEFTOF3-ROBOT)
(RIGHTOF9-ROBOT)
)
:effect
(and
(Flag93-)
(Flag93prime-)
)

)
(:action Flag94Action
:parameters ()
:precondition
(and
(LEFTOF3-ROBOT)
(RIGHTOF2-ROBOT)
)
:effect
(and
(Flag94-)
(Flag94prime-)
)

)
(:action Prim78Action-0
:parameters ()
:precondition
(and
(not (LEFTOF3-ROBOT))
)
:effect
(and
(Flag78prime-)
(not (Flag78-))
)

)
(:action Prim78Action-1
:parameters ()
:precondition
(and
(not (RIGHTOF6-ROBOT))
)
:effect
(and
(Flag78prime-)
(not (Flag78-))
)

)
(:action Prim79Action-0
:parameters ()
:precondition
(and
(not (LEFTOF3-ROBOT))
)
:effect
(and
(Flag79prime-)
(not (Flag79-))
)

)
(:action Prim79Action-1
:parameters ()
:precondition
(and
(not (RIGHTOF14-ROBOT))
)
:effect
(and
(Flag79prime-)
(not (Flag79-))
)

)
(:action Prim80Action-0
:parameters ()
:precondition
(and
(not (LEFTOF3-ROBOT))
)
:effect
(and
(Flag80prime-)
(not (Flag80-))
)

)
(:action Prim80Action-1
:parameters ()
:precondition
(and
(not (RIGHTOF4-ROBOT))
)
:effect
(and
(Flag80prime-)
(not (Flag80-))
)

)
(:action Prim81Action-0
:parameters ()
:precondition
(and
(not (LEFTOF3-ROBOT))
)
:effect
(and
(Flag81prime-)
(not (Flag81-))
)

)
(:action Prim81Action-1
:parameters ()
:precondition
(and
(not (RIGHTOF16-ROBOT))
)
:effect
(and
(Flag81prime-)
(not (Flag81-))
)

)
(:action Prim82Action-0
:parameters ()
:precondition
(and
(not (LEFTOF3-ROBOT))
)
:effect
(and
(Flag82prime-)
(not (Flag82-))
)

)
(:action Prim82Action-1
:parameters ()
:precondition
(and
(not (RIGHTOF7-ROBOT))
)
:effect
(and
(Flag82prime-)
(not (Flag82-))
)

)
(:action Prim83Action-0
:parameters ()
:precondition
(and
(not (LEFTOF3-ROBOT))
)
:effect
(and
(Flag83prime-)
(not (Flag83-))
)

)
(:action Prim83Action-1
:parameters ()
:precondition
(and
(not (RIGHTOF12-ROBOT))
)
:effect
(and
(Flag83prime-)
(not (Flag83-))
)

)
(:action Prim84Action-0
:parameters ()
:precondition
(and
(not (LEFTOF3-ROBOT))
)
:effect
(and
(Flag84prime-)
(not (Flag84-))
)

)
(:action Prim84Action-1
:parameters ()
:precondition
(and
(not (RIGHTOF10-ROBOT))
)
:effect
(and
(Flag84prime-)
(not (Flag84-))
)

)
(:action Prim85Action-0
:parameters ()
:precondition
(and
(not (LEFTOF3-ROBOT))
)
:effect
(and
(Flag85prime-)
(not (Flag85-))
)

)
(:action Prim85Action-1
:parameters ()
:precondition
(and
(not (RIGHTOF8-ROBOT))
)
:effect
(and
(Flag85prime-)
(not (Flag85-))
)

)
(:action Prim86Action-0
:parameters ()
:precondition
(and
(not (LEFTOF3-ROBOT))
)
:effect
(and
(Flag86prime-)
(not (Flag86-))
)

)
(:action Prim86Action-1
:parameters ()
:precondition
(and
(not (RIGHTOF5-ROBOT))
)
:effect
(and
(Flag86prime-)
(not (Flag86-))
)

)
(:action Prim87Action-0
:parameters ()
:precondition
(and
(not (LEFTOF3-ROBOT))
)
:effect
(and
(Flag87prime-)
(not (Flag87-))
)

)
(:action Prim87Action-1
:parameters ()
:precondition
(and
(not (RIGHTOF15-ROBOT))
)
:effect
(and
(Flag87prime-)
(not (Flag87-))
)

)
(:action Prim88Action-0
:parameters ()
:precondition
(and
(not (RIGHTOF3-ROBOT))
)
:effect
(and
(Flag88prime-)
(not (Flag88-))
)

)
(:action Prim88Action-1
:parameters ()
:precondition
(and
(not (LEFTOF3-ROBOT))
)
:effect
(and
(Flag88prime-)
(not (Flag88-))
)

)
(:action Prim89Action-0
:parameters ()
:precondition
(and
(not (LEFTOF3-ROBOT))
)
:effect
(and
(Flag89prime-)
(not (Flag89-))
)

)
(:action Prim89Action-1
:parameters ()
:precondition
(and
(not (RIGHTOF11-ROBOT))
)
:effect
(and
(Flag89prime-)
(not (Flag89-))
)

)
(:action Prim90Action-0
:parameters ()
:precondition
(and
(not (LEFTOF3-ROBOT))
)
:effect
(and
(Flag90prime-)
(not (Flag90-))
)

)
(:action Prim90Action-1
:parameters ()
:precondition
(and
(not (RIGHTOF18-ROBOT))
)
:effect
(and
(Flag90prime-)
(not (Flag90-))
)

)
(:action Prim91Action-0
:parameters ()
:precondition
(and
(not (RIGHTOF13-ROBOT))
)
:effect
(and
(Flag91prime-)
(not (Flag91-))
)

)
(:action Prim91Action-1
:parameters ()
:precondition
(and
(not (LEFTOF3-ROBOT))
)
:effect
(and
(Flag91prime-)
(not (Flag91-))
)

)
(:action Prim92Action-0
:parameters ()
:precondition
(and
(not (LEFTOF3-ROBOT))
)
:effect
(and
(Flag92prime-)
(not (Flag92-))
)

)
(:action Prim92Action-1
:parameters ()
:precondition
(and
(not (RIGHTOF17-ROBOT))
)
:effect
(and
(Flag92prime-)
(not (Flag92-))
)

)
(:action Prim93Action-0
:parameters ()
:precondition
(and
(not (LEFTOF3-ROBOT))
)
:effect
(and
(Flag93prime-)
(not (Flag93-))
)

)
(:action Prim93Action-1
:parameters ()
:precondition
(and
(not (RIGHTOF9-ROBOT))
)
:effect
(and
(Flag93prime-)
(not (Flag93-))
)

)
(:action Prim94Action-0
:parameters ()
:precondition
(and
(not (LEFTOF3-ROBOT))
)
:effect
(and
(Flag94prime-)
(not (Flag94-))
)

)
(:action Flag96Action
:parameters ()
:precondition
(and
(RIGHTOF18-ROBOT)
(LEFTOF4-ROBOT)
)
:effect
(and
(Flag96-)
(Flag96prime-)
)

)
(:action Flag97Action
:parameters ()
:precondition
(and
(LEFTOF3-ROBOT)
(RIGHTOF13-ROBOT)
)
:effect
(and
(Flag97-)
(Flag97prime-)
)

)
(:action Flag98Action
:parameters ()
:precondition
(and
(LEFTOF4-ROBOT)
(RIGHTOF17-ROBOT)
)
:effect
(and
(Flag98-)
(Flag98prime-)
)

)
(:action Flag99Action
:parameters ()
:precondition
(and
(LEFTOF4-ROBOT)
(RIGHTOF4-ROBOT)
)
:effect
(and
(Flag99-)
(Flag99prime-)
)

)
(:action Flag100Action
:parameters ()
:precondition
(and
(LEFTOF4-ROBOT)
(RIGHTOF7-ROBOT)
)
:effect
(and
(Flag100-)
(Flag100prime-)
)

)
(:action Flag101Action
:parameters ()
:precondition
(and
(LEFTOF4-ROBOT)
(RIGHTOF15-ROBOT)
)
:effect
(and
(Flag101-)
(Flag101prime-)
)

)
(:action Flag102Action
:parameters ()
:precondition
(and
(LEFTOF4-ROBOT)
(RIGHTOF14-ROBOT)
)
:effect
(and
(Flag102-)
(Flag102prime-)
)

)
(:action Flag103Action
:parameters ()
:precondition
(and
(LEFTOF3-ROBOT)
(RIGHTOF3-ROBOT)
)
:effect
(and
(Flag103-)
(Flag103prime-)
)

)
(:action Flag104Action
:parameters ()
:precondition
(and
(RIGHTOF8-ROBOT)
(LEFTOF4-ROBOT)
)
:effect
(and
(Flag104-)
(Flag104prime-)
)

)
(:action Flag105Action
:parameters ()
:precondition
(and
(LEFTOF4-ROBOT)
(RIGHTOF12-ROBOT)
)
:effect
(and
(Flag105-)
(Flag105prime-)
)

)
(:action Flag106Action
:parameters ()
:precondition
(and
(RIGHTOF10-ROBOT)
(LEFTOF4-ROBOT)
)
:effect
(and
(Flag106-)
(Flag106prime-)
)

)
(:action Flag107Action
:parameters ()
:precondition
(and
(LEFTOF4-ROBOT)
(RIGHTOF9-ROBOT)
)
:effect
(and
(Flag107-)
(Flag107prime-)
)

)
(:action Flag108Action
:parameters ()
:precondition
(and
(LEFTOF4-ROBOT)
(RIGHTOF16-ROBOT)
)
:effect
(and
(Flag108-)
(Flag108prime-)
)

)
(:action Flag109Action
:parameters ()
:precondition
(and
(LEFTOF4-ROBOT)
(RIGHTOF6-ROBOT)
)
:effect
(and
(Flag109-)
(Flag109prime-)
)

)
(:action Flag110Action
:parameters ()
:precondition
(and
(LEFTOF4-ROBOT)
(RIGHTOF5-ROBOT)
)
:effect
(and
(Flag110-)
(Flag110prime-)
)

)
(:action Flag111Action
:parameters ()
:precondition
(and
(RIGHTOF13-ROBOT)
(LEFTOF4-ROBOT)
)
:effect
(and
(Flag111-)
(Flag111prime-)
)

)
(:action Flag112Action
:parameters ()
:precondition
(and
(RIGHTOF3-ROBOT)
(LEFTOF4-ROBOT)
)
:effect
(and
(Flag112-)
(Flag112prime-)
)

)
(:action Flag113Action
:parameters ()
:precondition
(and
(LEFTOF4-ROBOT)
(RIGHTOF11-ROBOT)
)
:effect
(and
(Flag113-)
(Flag113prime-)
)

)
(:action Prim96Action-0
:parameters ()
:precondition
(and
(not (RIGHTOF18-ROBOT))
)
:effect
(and
(Flag96prime-)
(not (Flag96-))
)

)
(:action Prim96Action-1
:parameters ()
:precondition
(and
(not (LEFTOF4-ROBOT))
)
:effect
(and
(Flag96prime-)
(not (Flag96-))
)

)
(:action Prim97Action-0
:parameters ()
:precondition
(and
(not (LEFTOF3-ROBOT))
)
:effect
(and
(Flag97prime-)
(not (Flag97-))
)

)
(:action Prim97Action-1
:parameters ()
:precondition
(and
(not (RIGHTOF13-ROBOT))
)
:effect
(and
(Flag97prime-)
(not (Flag97-))
)

)
(:action Prim98Action-0
:parameters ()
:precondition
(and
(not (LEFTOF4-ROBOT))
)
:effect
(and
(Flag98prime-)
(not (Flag98-))
)

)
(:action Prim98Action-1
:parameters ()
:precondition
(and
(not (RIGHTOF17-ROBOT))
)
:effect
(and
(Flag98prime-)
(not (Flag98-))
)

)
(:action Prim99Action-0
:parameters ()
:precondition
(and
(not (LEFTOF4-ROBOT))
)
:effect
(and
(Flag99prime-)
(not (Flag99-))
)

)
(:action Prim99Action-1
:parameters ()
:precondition
(and
(not (RIGHTOF4-ROBOT))
)
:effect
(and
(Flag99prime-)
(not (Flag99-))
)

)
(:action Prim100Action-0
:parameters ()
:precondition
(and
(not (LEFTOF4-ROBOT))
)
:effect
(and
(Flag100prime-)
(not (Flag100-))
)

)
(:action Prim100Action-1
:parameters ()
:precondition
(and
(not (RIGHTOF7-ROBOT))
)
:effect
(and
(Flag100prime-)
(not (Flag100-))
)

)
(:action Prim101Action-0
:parameters ()
:precondition
(and
(not (LEFTOF4-ROBOT))
)
:effect
(and
(Flag101prime-)
(not (Flag101-))
)

)
(:action Prim101Action-1
:parameters ()
:precondition
(and
(not (RIGHTOF15-ROBOT))
)
:effect
(and
(Flag101prime-)
(not (Flag101-))
)

)
(:action Prim102Action-0
:parameters ()
:precondition
(and
(not (LEFTOF4-ROBOT))
)
:effect
(and
(Flag102prime-)
(not (Flag102-))
)

)
(:action Prim102Action-1
:parameters ()
:precondition
(and
(not (RIGHTOF14-ROBOT))
)
:effect
(and
(Flag102prime-)
(not (Flag102-))
)

)
(:action Prim103Action-0
:parameters ()
:precondition
(and
(not (LEFTOF3-ROBOT))
)
:effect
(and
(Flag103prime-)
(not (Flag103-))
)

)
(:action Prim103Action-1
:parameters ()
:precondition
(and
(not (RIGHTOF3-ROBOT))
)
:effect
(and
(Flag103prime-)
(not (Flag103-))
)

)
(:action Prim104Action-0
:parameters ()
:precondition
(and
(not (RIGHTOF8-ROBOT))
)
:effect
(and
(Flag104prime-)
(not (Flag104-))
)

)
(:action Prim104Action-1
:parameters ()
:precondition
(and
(not (LEFTOF4-ROBOT))
)
:effect
(and
(Flag104prime-)
(not (Flag104-))
)

)
(:action Prim105Action-0
:parameters ()
:precondition
(and
(not (LEFTOF4-ROBOT))
)
:effect
(and
(Flag105prime-)
(not (Flag105-))
)

)
(:action Prim105Action-1
:parameters ()
:precondition
(and
(not (RIGHTOF12-ROBOT))
)
:effect
(and
(Flag105prime-)
(not (Flag105-))
)

)
(:action Prim106Action-0
:parameters ()
:precondition
(and
(not (RIGHTOF10-ROBOT))
)
:effect
(and
(Flag106prime-)
(not (Flag106-))
)

)
(:action Prim106Action-1
:parameters ()
:precondition
(and
(not (LEFTOF4-ROBOT))
)
:effect
(and
(Flag106prime-)
(not (Flag106-))
)

)
(:action Prim107Action-0
:parameters ()
:precondition
(and
(not (LEFTOF4-ROBOT))
)
:effect
(and
(Flag107prime-)
(not (Flag107-))
)

)
(:action Prim107Action-1
:parameters ()
:precondition
(and
(not (RIGHTOF9-ROBOT))
)
:effect
(and
(Flag107prime-)
(not (Flag107-))
)

)
(:action Prim108Action-0
:parameters ()
:precondition
(and
(not (LEFTOF4-ROBOT))
)
:effect
(and
(Flag108prime-)
(not (Flag108-))
)

)
(:action Prim108Action-1
:parameters ()
:precondition
(and
(not (RIGHTOF16-ROBOT))
)
:effect
(and
(Flag108prime-)
(not (Flag108-))
)

)
(:action Prim109Action-0
:parameters ()
:precondition
(and
(not (LEFTOF4-ROBOT))
)
:effect
(and
(Flag109prime-)
(not (Flag109-))
)

)
(:action Prim109Action-1
:parameters ()
:precondition
(and
(not (RIGHTOF6-ROBOT))
)
:effect
(and
(Flag109prime-)
(not (Flag109-))
)

)
(:action Prim110Action-0
:parameters ()
:precondition
(and
(not (LEFTOF4-ROBOT))
)
:effect
(and
(Flag110prime-)
(not (Flag110-))
)

)
(:action Prim110Action-1
:parameters ()
:precondition
(and
(not (RIGHTOF5-ROBOT))
)
:effect
(and
(Flag110prime-)
(not (Flag110-))
)

)
(:action Prim111Action-0
:parameters ()
:precondition
(and
(not (RIGHTOF13-ROBOT))
)
:effect
(and
(Flag111prime-)
(not (Flag111-))
)

)
(:action Prim111Action-1
:parameters ()
:precondition
(and
(not (LEFTOF4-ROBOT))
)
:effect
(and
(Flag111prime-)
(not (Flag111-))
)

)
(:action Prim112Action-0
:parameters ()
:precondition
(and
(not (RIGHTOF3-ROBOT))
)
:effect
(and
(Flag112prime-)
(not (Flag112-))
)

)
(:action Prim112Action-1
:parameters ()
:precondition
(and
(not (LEFTOF4-ROBOT))
)
:effect
(and
(Flag112prime-)
(not (Flag112-))
)

)
(:action Prim113Action-0
:parameters ()
:precondition
(and
(not (LEFTOF4-ROBOT))
)
:effect
(and
(Flag113prime-)
(not (Flag113-))
)

)
(:action Prim113Action-1
:parameters ()
:precondition
(and
(not (RIGHTOF11-ROBOT))
)
:effect
(and
(Flag113prime-)
(not (Flag113-))
)

)
(:action Flag115Action
:parameters ()
:precondition
(and
(RIGHTOF9-ROBOT)
(LEFTOF5-ROBOT)
)
:effect
(and
(Flag115-)
(Flag115prime-)
)

)
(:action Flag116Action
:parameters ()
:precondition
(and
(RIGHTOF15-ROBOT)
(LEFTOF5-ROBOT)
)
:effect
(and
(Flag116-)
(Flag116prime-)
)

)
(:action Flag117Action
:parameters ()
:precondition
(and
(RIGHTOF4-ROBOT)
(LEFTOF5-ROBOT)
)
:effect
(and
(Flag117-)
(Flag117prime-)
)

)
(:action Flag118Action
:parameters ()
:precondition
(and
(RIGHTOF18-ROBOT)
(LEFTOF5-ROBOT)
)
:effect
(and
(Flag118-)
(Flag118prime-)
)

)
(:action Flag119Action
:parameters ()
:precondition
(and
(LEFTOF4-ROBOT)
(RIGHTOF18-ROBOT)
)
:effect
(and
(Flag119-)
(Flag119prime-)
)

)
(:action Flag120Action
:parameters ()
:precondition
(and
(RIGHTOF7-ROBOT)
(LEFTOF5-ROBOT)
)
:effect
(and
(Flag120-)
(Flag120prime-)
)

)
(:action Flag121Action
:parameters ()
:precondition
(and
(RIGHTOF11-ROBOT)
(LEFTOF5-ROBOT)
)
:effect
(and
(Flag121-)
(Flag121prime-)
)

)
(:action Flag122Action
:parameters ()
:precondition
(and
(RIGHTOF13-ROBOT)
(LEFTOF5-ROBOT)
)
:effect
(and
(Flag122-)
(Flag122prime-)
)

)
(:action Flag123Action
:parameters ()
:precondition
(and
(RIGHTOF16-ROBOT)
(LEFTOF5-ROBOT)
)
:effect
(and
(Flag123-)
(Flag123prime-)
)

)
(:action Flag124Action
:parameters ()
:precondition
(and
(LEFTOF4-ROBOT)
(RIGHTOF8-ROBOT)
)
:effect
(and
(Flag124-)
(Flag124prime-)
)

)
(:action Flag125Action
:parameters ()
:precondition
(and
(LEFTOF5-ROBOT)
(RIGHTOF6-ROBOT)
)
:effect
(and
(Flag125-)
(Flag125prime-)
)

)
(:action Flag126Action
:parameters ()
:precondition
(and
(RIGHTOF10-ROBOT)
(LEFTOF5-ROBOT)
)
:effect
(and
(Flag126-)
(Flag126prime-)
)

)
(:action Flag127Action
:parameters ()
:precondition
(and
(LEFTOF4-ROBOT)
(RIGHTOF10-ROBOT)
)
:effect
(and
(Flag127-)
(Flag127prime-)
)

)
(:action Flag128Action
:parameters ()
:precondition
(and
(LEFTOF5-ROBOT)
(RIGHTOF12-ROBOT)
)
:effect
(and
(Flag128-)
(Flag128prime-)
)

)
(:action Flag129Action
:parameters ()
:precondition
(and
(RIGHTOF8-ROBOT)
(LEFTOF5-ROBOT)
)
:effect
(and
(Flag129-)
(Flag129prime-)
)

)
(:action Flag130Action
:parameters ()
:precondition
(and
(RIGHTOF14-ROBOT)
(LEFTOF5-ROBOT)
)
:effect
(and
(Flag130-)
(Flag130prime-)
)

)
(:action Flag131Action
:parameters ()
:precondition
(and
(RIGHTOF5-ROBOT)
(LEFTOF5-ROBOT)
)
:effect
(and
(Flag131-)
(Flag131prime-)
)

)
(:action Flag132Action
:parameters ()
:precondition
(and
(RIGHTOF17-ROBOT)
(LEFTOF5-ROBOT)
)
:effect
(and
(Flag132-)
(Flag132prime-)
)

)
(:action Prim115Action-0
:parameters ()
:precondition
(and
(not (RIGHTOF9-ROBOT))
)
:effect
(and
(Flag115prime-)
(not (Flag115-))
)

)
(:action Prim115Action-1
:parameters ()
:precondition
(and
(not (LEFTOF5-ROBOT))
)
:effect
(and
(Flag115prime-)
(not (Flag115-))
)

)
(:action Prim116Action-0
:parameters ()
:precondition
(and
(not (RIGHTOF15-ROBOT))
)
:effect
(and
(Flag116prime-)
(not (Flag116-))
)

)
(:action Prim116Action-1
:parameters ()
:precondition
(and
(not (LEFTOF5-ROBOT))
)
:effect
(and
(Flag116prime-)
(not (Flag116-))
)

)
(:action Prim117Action-0
:parameters ()
:precondition
(and
(not (RIGHTOF4-ROBOT))
)
:effect
(and
(Flag117prime-)
(not (Flag117-))
)

)
(:action Prim117Action-1
:parameters ()
:precondition
(and
(not (LEFTOF5-ROBOT))
)
:effect
(and
(Flag117prime-)
(not (Flag117-))
)

)
(:action Prim118Action-0
:parameters ()
:precondition
(and
(not (RIGHTOF18-ROBOT))
)
:effect
(and
(Flag118prime-)
(not (Flag118-))
)

)
(:action Prim118Action-1
:parameters ()
:precondition
(and
(not (LEFTOF5-ROBOT))
)
:effect
(and
(Flag118prime-)
(not (Flag118-))
)

)
(:action Prim119Action-0
:parameters ()
:precondition
(and
(not (LEFTOF4-ROBOT))
)
:effect
(and
(Flag119prime-)
(not (Flag119-))
)

)
(:action Prim119Action-1
:parameters ()
:precondition
(and
(not (RIGHTOF18-ROBOT))
)
:effect
(and
(Flag119prime-)
(not (Flag119-))
)

)
(:action Prim120Action-0
:parameters ()
:precondition
(and
(not (RIGHTOF7-ROBOT))
)
:effect
(and
(Flag120prime-)
(not (Flag120-))
)

)
(:action Prim120Action-1
:parameters ()
:precondition
(and
(not (LEFTOF5-ROBOT))
)
:effect
(and
(Flag120prime-)
(not (Flag120-))
)

)
(:action Prim121Action-0
:parameters ()
:precondition
(and
(not (RIGHTOF11-ROBOT))
)
:effect
(and
(Flag121prime-)
(not (Flag121-))
)

)
(:action Prim121Action-1
:parameters ()
:precondition
(and
(not (LEFTOF5-ROBOT))
)
:effect
(and
(Flag121prime-)
(not (Flag121-))
)

)
(:action Prim122Action-0
:parameters ()
:precondition
(and
(not (RIGHTOF13-ROBOT))
)
:effect
(and
(Flag122prime-)
(not (Flag122-))
)

)
(:action Prim122Action-1
:parameters ()
:precondition
(and
(not (LEFTOF5-ROBOT))
)
:effect
(and
(Flag122prime-)
(not (Flag122-))
)

)
(:action Prim123Action-0
:parameters ()
:precondition
(and
(not (RIGHTOF16-ROBOT))
)
:effect
(and
(Flag123prime-)
(not (Flag123-))
)

)
(:action Prim123Action-1
:parameters ()
:precondition
(and
(not (LEFTOF5-ROBOT))
)
:effect
(and
(Flag123prime-)
(not (Flag123-))
)

)
(:action Prim124Action-0
:parameters ()
:precondition
(and
(not (LEFTOF4-ROBOT))
)
:effect
(and
(Flag124prime-)
(not (Flag124-))
)

)
(:action Prim124Action-1
:parameters ()
:precondition
(and
(not (RIGHTOF8-ROBOT))
)
:effect
(and
(Flag124prime-)
(not (Flag124-))
)

)
(:action Prim125Action-0
:parameters ()
:precondition
(and
(not (LEFTOF5-ROBOT))
)
:effect
(and
(Flag125prime-)
(not (Flag125-))
)

)
(:action Prim125Action-1
:parameters ()
:precondition
(and
(not (RIGHTOF6-ROBOT))
)
:effect
(and
(Flag125prime-)
(not (Flag125-))
)

)
(:action Prim126Action-0
:parameters ()
:precondition
(and
(not (RIGHTOF10-ROBOT))
)
:effect
(and
(Flag126prime-)
(not (Flag126-))
)

)
(:action Prim126Action-1
:parameters ()
:precondition
(and
(not (LEFTOF5-ROBOT))
)
:effect
(and
(Flag126prime-)
(not (Flag126-))
)

)
(:action Prim127Action-0
:parameters ()
:precondition
(and
(not (LEFTOF4-ROBOT))
)
:effect
(and
(Flag127prime-)
(not (Flag127-))
)

)
(:action Prim127Action-1
:parameters ()
:precondition
(and
(not (RIGHTOF10-ROBOT))
)
:effect
(and
(Flag127prime-)
(not (Flag127-))
)

)
(:action Prim128Action-0
:parameters ()
:precondition
(and
(not (LEFTOF5-ROBOT))
)
:effect
(and
(Flag128prime-)
(not (Flag128-))
)

)
(:action Prim128Action-1
:parameters ()
:precondition
(and
(not (RIGHTOF12-ROBOT))
)
:effect
(and
(Flag128prime-)
(not (Flag128-))
)

)
(:action Prim129Action-0
:parameters ()
:precondition
(and
(not (RIGHTOF8-ROBOT))
)
:effect
(and
(Flag129prime-)
(not (Flag129-))
)

)
(:action Prim129Action-1
:parameters ()
:precondition
(and
(not (LEFTOF5-ROBOT))
)
:effect
(and
(Flag129prime-)
(not (Flag129-))
)

)
(:action Prim130Action-0
:parameters ()
:precondition
(and
(not (RIGHTOF14-ROBOT))
)
:effect
(and
(Flag130prime-)
(not (Flag130-))
)

)
(:action Prim130Action-1
:parameters ()
:precondition
(and
(not (LEFTOF5-ROBOT))
)
:effect
(and
(Flag130prime-)
(not (Flag130-))
)

)
(:action Prim131Action-0
:parameters ()
:precondition
(and
(not (RIGHTOF5-ROBOT))
)
:effect
(and
(Flag131prime-)
(not (Flag131-))
)

)
(:action Prim131Action-1
:parameters ()
:precondition
(and
(not (LEFTOF5-ROBOT))
)
:effect
(and
(Flag131prime-)
(not (Flag131-))
)

)
(:action Prim132Action-0
:parameters ()
:precondition
(and
(not (RIGHTOF17-ROBOT))
)
:effect
(and
(Flag132prime-)
(not (Flag132-))
)

)
(:action Prim132Action-1
:parameters ()
:precondition
(and
(not (LEFTOF5-ROBOT))
)
:effect
(and
(Flag132prime-)
(not (Flag132-))
)

)
(:action Flag134Action
:parameters ()
:precondition
(and
(RIGHTOF15-ROBOT)
(LEFTOF6-ROBOT)
)
:effect
(and
(Flag134-)
(Flag134prime-)
)

)
(:action Flag135Action
:parameters ()
:precondition
(and
(RIGHTOF11-ROBOT)
(LEFTOF6-ROBOT)
)
:effect
(and
(Flag135-)
(Flag135prime-)
)

)
(:action Flag136Action
:parameters ()
:precondition
(and
(RIGHTOF5-ROBOT)
(LEFTOF6-ROBOT)
)
:effect
(and
(Flag136-)
(Flag136prime-)
)

)
(:action Flag137Action
:parameters ()
:precondition
(and
(RIGHTOF14-ROBOT)
(LEFTOF6-ROBOT)
)
:effect
(and
(Flag137-)
(Flag137prime-)
)

)
(:action Flag138Action
:parameters ()
:precondition
(and
(RIGHTOF7-ROBOT)
(LEFTOF6-ROBOT)
)
:effect
(and
(Flag138-)
(Flag138prime-)
)

)
(:action Flag139Action
:parameters ()
:precondition
(and
(RIGHTOF12-ROBOT)
(LEFTOF6-ROBOT)
)
:effect
(and
(Flag139-)
(Flag139prime-)
)

)
(:action Flag140Action
:parameters ()
:precondition
(and
(RIGHTOF17-ROBOT)
(LEFTOF6-ROBOT)
)
:effect
(and
(Flag140-)
(Flag140prime-)
)

)
(:action Flag141Action
:parameters ()
:precondition
(and
(RIGHTOF10-ROBOT)
(LEFTOF6-ROBOT)
)
:effect
(and
(Flag141-)
(Flag141prime-)
)

)
(:action Flag142Action
:parameters ()
:precondition
(and
(RIGHTOF6-ROBOT)
(LEFTOF6-ROBOT)
)
:effect
(and
(Flag142-)
(Flag142prime-)
)

)
(:action Flag143Action
:parameters ()
:precondition
(and
(RIGHTOF18-ROBOT)
(LEFTOF6-ROBOT)
)
:effect
(and
(Flag143-)
(Flag143prime-)
)

)
(:action Flag144Action
:parameters ()
:precondition
(and
(RIGHTOF13-ROBOT)
(LEFTOF6-ROBOT)
)
:effect
(and
(Flag144-)
(Flag144prime-)
)

)
(:action Flag145Action
:parameters ()
:precondition
(and
(RIGHTOF9-ROBOT)
(LEFTOF6-ROBOT)
)
:effect
(and
(Flag145-)
(Flag145prime-)
)

)
(:action Flag146Action
:parameters ()
:precondition
(and
(RIGHTOF16-ROBOT)
(LEFTOF6-ROBOT)
)
:effect
(and
(Flag146-)
(Flag146prime-)
)

)
(:action Flag147Action
:parameters ()
:precondition
(and
(RIGHTOF8-ROBOT)
(LEFTOF6-ROBOT)
)
:effect
(and
(Flag147-)
(Flag147prime-)
)

)
(:action Prim134Action-0
:parameters ()
:precondition
(and
(not (RIGHTOF15-ROBOT))
)
:effect
(and
(Flag134prime-)
(not (Flag134-))
)

)
(:action Prim134Action-1
:parameters ()
:precondition
(and
(not (LEFTOF6-ROBOT))
)
:effect
(and
(Flag134prime-)
(not (Flag134-))
)

)
(:action Prim135Action-0
:parameters ()
:precondition
(and
(not (RIGHTOF11-ROBOT))
)
:effect
(and
(Flag135prime-)
(not (Flag135-))
)

)
(:action Prim135Action-1
:parameters ()
:precondition
(and
(not (LEFTOF6-ROBOT))
)
:effect
(and
(Flag135prime-)
(not (Flag135-))
)

)
(:action Prim136Action-0
:parameters ()
:precondition
(and
(not (RIGHTOF5-ROBOT))
)
:effect
(and
(Flag136prime-)
(not (Flag136-))
)

)
(:action Prim136Action-1
:parameters ()
:precondition
(and
(not (LEFTOF6-ROBOT))
)
:effect
(and
(Flag136prime-)
(not (Flag136-))
)

)
(:action Prim137Action-0
:parameters ()
:precondition
(and
(not (RIGHTOF14-ROBOT))
)
:effect
(and
(Flag137prime-)
(not (Flag137-))
)

)
(:action Prim137Action-1
:parameters ()
:precondition
(and
(not (LEFTOF6-ROBOT))
)
:effect
(and
(Flag137prime-)
(not (Flag137-))
)

)
(:action Prim138Action-0
:parameters ()
:precondition
(and
(not (RIGHTOF7-ROBOT))
)
:effect
(and
(Flag138prime-)
(not (Flag138-))
)

)
(:action Prim138Action-1
:parameters ()
:precondition
(and
(not (LEFTOF6-ROBOT))
)
:effect
(and
(Flag138prime-)
(not (Flag138-))
)

)
(:action Prim139Action-0
:parameters ()
:precondition
(and
(not (RIGHTOF12-ROBOT))
)
:effect
(and
(Flag139prime-)
(not (Flag139-))
)

)
(:action Prim139Action-1
:parameters ()
:precondition
(and
(not (LEFTOF6-ROBOT))
)
:effect
(and
(Flag139prime-)
(not (Flag139-))
)

)
(:action Prim140Action-0
:parameters ()
:precondition
(and
(not (RIGHTOF17-ROBOT))
)
:effect
(and
(Flag140prime-)
(not (Flag140-))
)

)
(:action Prim140Action-1
:parameters ()
:precondition
(and
(not (LEFTOF6-ROBOT))
)
:effect
(and
(Flag140prime-)
(not (Flag140-))
)

)
(:action Prim141Action-0
:parameters ()
:precondition
(and
(not (RIGHTOF10-ROBOT))
)
:effect
(and
(Flag141prime-)
(not (Flag141-))
)

)
(:action Prim141Action-1
:parameters ()
:precondition
(and
(not (LEFTOF6-ROBOT))
)
:effect
(and
(Flag141prime-)
(not (Flag141-))
)

)
(:action Prim142Action-0
:parameters ()
:precondition
(and
(not (RIGHTOF6-ROBOT))
)
:effect
(and
(Flag142prime-)
(not (Flag142-))
)

)
(:action Prim142Action-1
:parameters ()
:precondition
(and
(not (LEFTOF6-ROBOT))
)
:effect
(and
(Flag142prime-)
(not (Flag142-))
)

)
(:action Prim143Action-0
:parameters ()
:precondition
(and
(not (RIGHTOF18-ROBOT))
)
:effect
(and
(Flag143prime-)
(not (Flag143-))
)

)
(:action Prim143Action-1
:parameters ()
:precondition
(and
(not (LEFTOF6-ROBOT))
)
:effect
(and
(Flag143prime-)
(not (Flag143-))
)

)
(:action Prim144Action-0
:parameters ()
:precondition
(and
(not (RIGHTOF13-ROBOT))
)
:effect
(and
(Flag144prime-)
(not (Flag144-))
)

)
(:action Prim144Action-1
:parameters ()
:precondition
(and
(not (LEFTOF6-ROBOT))
)
:effect
(and
(Flag144prime-)
(not (Flag144-))
)

)
(:action Prim145Action-0
:parameters ()
:precondition
(and
(not (RIGHTOF9-ROBOT))
)
:effect
(and
(Flag145prime-)
(not (Flag145-))
)

)
(:action Prim145Action-1
:parameters ()
:precondition
(and
(not (LEFTOF6-ROBOT))
)
:effect
(and
(Flag145prime-)
(not (Flag145-))
)

)
(:action Prim146Action-0
:parameters ()
:precondition
(and
(not (RIGHTOF16-ROBOT))
)
:effect
(and
(Flag146prime-)
(not (Flag146-))
)

)
(:action Prim146Action-1
:parameters ()
:precondition
(and
(not (LEFTOF6-ROBOT))
)
:effect
(and
(Flag146prime-)
(not (Flag146-))
)

)
(:action Prim147Action-0
:parameters ()
:precondition
(and
(not (RIGHTOF8-ROBOT))
)
:effect
(and
(Flag147prime-)
(not (Flag147-))
)

)
(:action Prim147Action-1
:parameters ()
:precondition
(and
(not (LEFTOF6-ROBOT))
)
:effect
(and
(Flag147prime-)
(not (Flag147-))
)

)
(:action Flag149Action
:parameters ()
:precondition
(and
(LEFTOF7-ROBOT)
(RIGHTOF7-ROBOT)
)
:effect
(and
(Flag149-)
(Flag149prime-)
)

)
(:action Flag150Action
:parameters ()
:precondition
(and
(RIGHTOF13-ROBOT)
(LEFTOF7-ROBOT)
)
:effect
(and
(Flag150-)
(Flag150prime-)
)

)
(:action Flag151Action
:parameters ()
:precondition
(and
(LEFTOF7-ROBOT)
(RIGHTOF9-ROBOT)
)
:effect
(and
(Flag151-)
(Flag151prime-)
)

)
(:action Flag152Action
:parameters ()
:precondition
(and
(LEFTOF6-ROBOT)
(RIGHTOF6-ROBOT)
)
:effect
(and
(Flag152-)
(Flag152prime-)
)

)
(:action Flag153Action
:parameters ()
:precondition
(and
(RIGHTOF18-ROBOT)
(LEFTOF7-ROBOT)
)
:effect
(and
(Flag153-)
(Flag153prime-)
)

)
(:action Flag154Action
:parameters ()
:precondition
(and
(LEFTOF7-ROBOT)
(RIGHTOF12-ROBOT)
)
:effect
(and
(Flag154-)
(Flag154prime-)
)

)
(:action Flag155Action
:parameters ()
:precondition
(and
(LEFTOF7-ROBOT)
(RIGHTOF6-ROBOT)
)
:effect
(and
(Flag155-)
(Flag155prime-)
)

)
(:action Flag156Action
:parameters ()
:precondition
(and
(LEFTOF6-ROBOT)
(RIGHTOF12-ROBOT)
)
:effect
(and
(Flag156-)
(Flag156prime-)
)

)
(:action Flag157Action
:parameters ()
:precondition
(and
(LEFTOF7-ROBOT)
(RIGHTOF15-ROBOT)
)
:effect
(and
(Flag157-)
(Flag157prime-)
)

)
(:action Flag158Action
:parameters ()
:precondition
(and
(RIGHTOF11-ROBOT)
(LEFTOF7-ROBOT)
)
:effect
(and
(Flag158-)
(Flag158prime-)
)

)
(:action Flag159Action
:parameters ()
:precondition
(and
(RIGHTOF8-ROBOT)
(LEFTOF7-ROBOT)
)
:effect
(and
(Flag159-)
(Flag159prime-)
)

)
(:action Flag160Action
:parameters ()
:precondition
(and
(RIGHTOF17-ROBOT)
(LEFTOF7-ROBOT)
)
:effect
(and
(Flag160-)
(Flag160prime-)
)

)
(:action Flag161Action
:parameters ()
:precondition
(and
(RIGHTOF16-ROBOT)
(LEFTOF7-ROBOT)
)
:effect
(and
(Flag161-)
(Flag161prime-)
)

)
(:action Flag162Action
:parameters ()
:precondition
(and
(LEFTOF7-ROBOT)
(RIGHTOF14-ROBOT)
)
:effect
(and
(Flag162-)
(Flag162prime-)
)

)
(:action Flag163Action
:parameters ()
:precondition
(and
(RIGHTOF10-ROBOT)
(LEFTOF7-ROBOT)
)
:effect
(and
(Flag163-)
(Flag163prime-)
)

)
(:action Prim149Action-0
:parameters ()
:precondition
(and
(not (LEFTOF7-ROBOT))
)
:effect
(and
(Flag149prime-)
(not (Flag149-))
)

)
(:action Prim149Action-1
:parameters ()
:precondition
(and
(not (RIGHTOF7-ROBOT))
)
:effect
(and
(Flag149prime-)
(not (Flag149-))
)

)
(:action Prim150Action-0
:parameters ()
:precondition
(and
(not (RIGHTOF13-ROBOT))
)
:effect
(and
(Flag150prime-)
(not (Flag150-))
)

)
(:action Prim150Action-1
:parameters ()
:precondition
(and
(not (LEFTOF7-ROBOT))
)
:effect
(and
(Flag150prime-)
(not (Flag150-))
)

)
(:action Prim151Action-0
:parameters ()
:precondition
(and
(not (LEFTOF7-ROBOT))
)
:effect
(and
(Flag151prime-)
(not (Flag151-))
)

)
(:action Prim151Action-1
:parameters ()
:precondition
(and
(not (RIGHTOF9-ROBOT))
)
:effect
(and
(Flag151prime-)
(not (Flag151-))
)

)
(:action Prim152Action-0
:parameters ()
:precondition
(and
(not (LEFTOF6-ROBOT))
)
:effect
(and
(Flag152prime-)
(not (Flag152-))
)

)
(:action Prim152Action-1
:parameters ()
:precondition
(and
(not (RIGHTOF6-ROBOT))
)
:effect
(and
(Flag152prime-)
(not (Flag152-))
)

)
(:action Prim153Action-0
:parameters ()
:precondition
(and
(not (RIGHTOF18-ROBOT))
)
:effect
(and
(Flag153prime-)
(not (Flag153-))
)

)
(:action Prim153Action-1
:parameters ()
:precondition
(and
(not (LEFTOF7-ROBOT))
)
:effect
(and
(Flag153prime-)
(not (Flag153-))
)

)
(:action Prim154Action-0
:parameters ()
:precondition
(and
(not (LEFTOF7-ROBOT))
)
:effect
(and
(Flag154prime-)
(not (Flag154-))
)

)
(:action Prim154Action-1
:parameters ()
:precondition
(and
(not (RIGHTOF12-ROBOT))
)
:effect
(and
(Flag154prime-)
(not (Flag154-))
)

)
(:action Prim155Action-0
:parameters ()
:precondition
(and
(not (LEFTOF7-ROBOT))
)
:effect
(and
(Flag155prime-)
(not (Flag155-))
)

)
(:action Prim155Action-1
:parameters ()
:precondition
(and
(not (RIGHTOF6-ROBOT))
)
:effect
(and
(Flag155prime-)
(not (Flag155-))
)

)
(:action Prim156Action-0
:parameters ()
:precondition
(and
(not (LEFTOF6-ROBOT))
)
:effect
(and
(Flag156prime-)
(not (Flag156-))
)

)
(:action Prim156Action-1
:parameters ()
:precondition
(and
(not (RIGHTOF12-ROBOT))
)
:effect
(and
(Flag156prime-)
(not (Flag156-))
)

)
(:action Prim157Action-0
:parameters ()
:precondition
(and
(not (LEFTOF7-ROBOT))
)
:effect
(and
(Flag157prime-)
(not (Flag157-))
)

)
(:action Prim157Action-1
:parameters ()
:precondition
(and
(not (RIGHTOF15-ROBOT))
)
:effect
(and
(Flag157prime-)
(not (Flag157-))
)

)
(:action Prim158Action-0
:parameters ()
:precondition
(and
(not (RIGHTOF11-ROBOT))
)
:effect
(and
(Flag158prime-)
(not (Flag158-))
)

)
(:action Prim158Action-1
:parameters ()
:precondition
(and
(not (LEFTOF7-ROBOT))
)
:effect
(and
(Flag158prime-)
(not (Flag158-))
)

)
(:action Prim159Action-0
:parameters ()
:precondition
(and
(not (RIGHTOF8-ROBOT))
)
:effect
(and
(Flag159prime-)
(not (Flag159-))
)

)
(:action Prim159Action-1
:parameters ()
:precondition
(and
(not (LEFTOF7-ROBOT))
)
:effect
(and
(Flag159prime-)
(not (Flag159-))
)

)
(:action Prim160Action-0
:parameters ()
:precondition
(and
(not (RIGHTOF17-ROBOT))
)
:effect
(and
(Flag160prime-)
(not (Flag160-))
)

)
(:action Prim160Action-1
:parameters ()
:precondition
(and
(not (LEFTOF7-ROBOT))
)
:effect
(and
(Flag160prime-)
(not (Flag160-))
)

)
(:action Prim161Action-0
:parameters ()
:precondition
(and
(not (RIGHTOF16-ROBOT))
)
:effect
(and
(Flag161prime-)
(not (Flag161-))
)

)
(:action Prim161Action-1
:parameters ()
:precondition
(and
(not (LEFTOF7-ROBOT))
)
:effect
(and
(Flag161prime-)
(not (Flag161-))
)

)
(:action Prim162Action-0
:parameters ()
:precondition
(and
(not (LEFTOF7-ROBOT))
)
:effect
(and
(Flag162prime-)
(not (Flag162-))
)

)
(:action Prim162Action-1
:parameters ()
:precondition
(and
(not (RIGHTOF14-ROBOT))
)
:effect
(and
(Flag162prime-)
(not (Flag162-))
)

)
(:action Prim163Action-0
:parameters ()
:precondition
(and
(not (RIGHTOF10-ROBOT))
)
:effect
(and
(Flag163prime-)
(not (Flag163-))
)

)
(:action Prim163Action-1
:parameters ()
:precondition
(and
(not (LEFTOF7-ROBOT))
)
:effect
(and
(Flag163prime-)
(not (Flag163-))
)

)
(:action Flag165Action
:parameters ()
:precondition
(and
(RIGHTOF10-ROBOT)
(LEFTOF8-ROBOT)
)
:effect
(and
(Flag165-)
(Flag165prime-)
)

)
(:action Flag166Action
:parameters ()
:precondition
(and
(RIGHTOF16-ROBOT)
(LEFTOF8-ROBOT)
)
:effect
(and
(Flag166-)
(Flag166prime-)
)

)
(:action Flag167Action
:parameters ()
:precondition
(and
(RIGHTOF13-ROBOT)
(LEFTOF8-ROBOT)
)
:effect
(and
(Flag167-)
(Flag167prime-)
)

)
(:action Flag168Action
:parameters ()
:precondition
(and
(RIGHTOF7-ROBOT)
(LEFTOF7-ROBOT)
)
:effect
(and
(Flag168-)
(Flag168prime-)
)

)
(:action Flag169Action
:parameters ()
:precondition
(and
(LEFTOF8-ROBOT)
(RIGHTOF15-ROBOT)
)
:effect
(and
(Flag169-)
(Flag169prime-)
)

)
(:action Flag170Action
:parameters ()
:precondition
(and
(LEFTOF8-ROBOT)
(RIGHTOF9-ROBOT)
)
:effect
(and
(Flag170-)
(Flag170prime-)
)

)
(:action Flag171Action
:parameters ()
:precondition
(and
(RIGHTOF7-ROBOT)
(LEFTOF8-ROBOT)
)
:effect
(and
(Flag171-)
(Flag171prime-)
)

)
(:action Flag172Action
:parameters ()
:precondition
(and
(RIGHTOF18-ROBOT)
(LEFTOF8-ROBOT)
)
:effect
(and
(Flag172-)
(Flag172prime-)
)

)
(:action Flag173Action
:parameters ()
:precondition
(and
(RIGHTOF14-ROBOT)
(LEFTOF8-ROBOT)
)
:effect
(and
(Flag173-)
(Flag173prime-)
)

)
(:action Flag174Action
:parameters ()
:precondition
(and
(LEFTOF8-ROBOT)
(RIGHTOF12-ROBOT)
)
:effect
(and
(Flag174-)
(Flag174prime-)
)

)
(:action Flag175Action
:parameters ()
:precondition
(and
(RIGHTOF11-ROBOT)
(LEFTOF8-ROBOT)
)
:effect
(and
(Flag175-)
(Flag175prime-)
)

)
(:action Flag176Action
:parameters ()
:precondition
(and
(RIGHTOF17-ROBOT)
(LEFTOF8-ROBOT)
)
:effect
(and
(Flag176-)
(Flag176prime-)
)

)
(:action Flag177Action
:parameters ()
:precondition
(and
(RIGHTOF8-ROBOT)
(LEFTOF8-ROBOT)
)
:effect
(and
(Flag177-)
(Flag177prime-)
)

)
(:action Prim165Action-0
:parameters ()
:precondition
(and
(not (RIGHTOF10-ROBOT))
)
:effect
(and
(Flag165prime-)
(not (Flag165-))
)

)
(:action Prim165Action-1
:parameters ()
:precondition
(and
(not (LEFTOF8-ROBOT))
)
:effect
(and
(Flag165prime-)
(not (Flag165-))
)

)
(:action Prim166Action-0
:parameters ()
:precondition
(and
(not (RIGHTOF16-ROBOT))
)
:effect
(and
(Flag166prime-)
(not (Flag166-))
)

)
(:action Prim166Action-1
:parameters ()
:precondition
(and
(not (LEFTOF8-ROBOT))
)
:effect
(and
(Flag166prime-)
(not (Flag166-))
)

)
(:action Prim167Action-0
:parameters ()
:precondition
(and
(not (RIGHTOF13-ROBOT))
)
:effect
(and
(Flag167prime-)
(not (Flag167-))
)

)
(:action Prim167Action-1
:parameters ()
:precondition
(and
(not (LEFTOF8-ROBOT))
)
:effect
(and
(Flag167prime-)
(not (Flag167-))
)

)
(:action Prim168Action-0
:parameters ()
:precondition
(and
(not (RIGHTOF7-ROBOT))
)
:effect
(and
(Flag168prime-)
(not (Flag168-))
)

)
(:action Prim168Action-1
:parameters ()
:precondition
(and
(not (LEFTOF7-ROBOT))
)
:effect
(and
(Flag168prime-)
(not (Flag168-))
)

)
(:action Prim169Action-0
:parameters ()
:precondition
(and
(not (LEFTOF8-ROBOT))
)
:effect
(and
(Flag169prime-)
(not (Flag169-))
)

)
(:action Prim169Action-1
:parameters ()
:precondition
(and
(not (RIGHTOF15-ROBOT))
)
:effect
(and
(Flag169prime-)
(not (Flag169-))
)

)
(:action Prim170Action-0
:parameters ()
:precondition
(and
(not (LEFTOF8-ROBOT))
)
:effect
(and
(Flag170prime-)
(not (Flag170-))
)

)
(:action Prim170Action-1
:parameters ()
:precondition
(and
(not (RIGHTOF9-ROBOT))
)
:effect
(and
(Flag170prime-)
(not (Flag170-))
)

)
(:action Prim171Action-0
:parameters ()
:precondition
(and
(not (RIGHTOF7-ROBOT))
)
:effect
(and
(Flag171prime-)
(not (Flag171-))
)

)
(:action Prim171Action-1
:parameters ()
:precondition
(and
(not (LEFTOF8-ROBOT))
)
:effect
(and
(Flag171prime-)
(not (Flag171-))
)

)
(:action Prim172Action-0
:parameters ()
:precondition
(and
(not (RIGHTOF18-ROBOT))
)
:effect
(and
(Flag172prime-)
(not (Flag172-))
)

)
(:action Prim172Action-1
:parameters ()
:precondition
(and
(not (LEFTOF8-ROBOT))
)
:effect
(and
(Flag172prime-)
(not (Flag172-))
)

)
(:action Prim173Action-0
:parameters ()
:precondition
(and
(not (RIGHTOF14-ROBOT))
)
:effect
(and
(Flag173prime-)
(not (Flag173-))
)

)
(:action Prim173Action-1
:parameters ()
:precondition
(and
(not (LEFTOF8-ROBOT))
)
:effect
(and
(Flag173prime-)
(not (Flag173-))
)

)
(:action Prim174Action-0
:parameters ()
:precondition
(and
(not (LEFTOF8-ROBOT))
)
:effect
(and
(Flag174prime-)
(not (Flag174-))
)

)
(:action Prim174Action-1
:parameters ()
:precondition
(and
(not (RIGHTOF12-ROBOT))
)
:effect
(and
(Flag174prime-)
(not (Flag174-))
)

)
(:action Prim175Action-0
:parameters ()
:precondition
(and
(not (RIGHTOF11-ROBOT))
)
:effect
(and
(Flag175prime-)
(not (Flag175-))
)

)
(:action Prim175Action-1
:parameters ()
:precondition
(and
(not (LEFTOF8-ROBOT))
)
:effect
(and
(Flag175prime-)
(not (Flag175-))
)

)
(:action Prim176Action-0
:parameters ()
:precondition
(and
(not (RIGHTOF17-ROBOT))
)
:effect
(and
(Flag176prime-)
(not (Flag176-))
)

)
(:action Prim176Action-1
:parameters ()
:precondition
(and
(not (LEFTOF8-ROBOT))
)
:effect
(and
(Flag176prime-)
(not (Flag176-))
)

)
(:action Prim177Action-0
:parameters ()
:precondition
(and
(not (RIGHTOF8-ROBOT))
)
:effect
(and
(Flag177prime-)
(not (Flag177-))
)

)
(:action Prim177Action-1
:parameters ()
:precondition
(and
(not (LEFTOF8-ROBOT))
)
:effect
(and
(Flag177prime-)
(not (Flag177-))
)

)
(:action Flag179Action
:parameters ()
:precondition
(and
(RIGHTOF13-ROBOT)
(LEFTOF9-ROBOT)
)
:effect
(and
(Flag179-)
(Flag179prime-)
)

)
(:action Flag180Action
:parameters ()
:precondition
(and
(RIGHTOF8-ROBOT)
(LEFTOF9-ROBOT)
)
:effect
(and
(Flag180-)
(Flag180prime-)
)

)
(:action Flag181Action
:parameters ()
:precondition
(and
(LEFTOF9-ROBOT)
(RIGHTOF12-ROBOT)
)
:effect
(and
(Flag181-)
(Flag181prime-)
)

)
(:action Flag182Action
:parameters ()
:precondition
(and
(LEFTOF9-ROBOT)
(RIGHTOF9-ROBOT)
)
:effect
(and
(Flag182-)
(Flag182prime-)
)

)
(:action Flag183Action
:parameters ()
:precondition
(and
(LEFTOF8-ROBOT)
(RIGHTOF14-ROBOT)
)
:effect
(and
(Flag183-)
(Flag183prime-)
)

)
(:action Flag184Action
:parameters ()
:precondition
(and
(RIGHTOF11-ROBOT)
(LEFTOF9-ROBOT)
)
:effect
(and
(Flag184-)
(Flag184prime-)
)

)
(:action Flag185Action
:parameters ()
:precondition
(and
(LEFTOF9-ROBOT)
(RIGHTOF15-ROBOT)
)
:effect
(and
(Flag185-)
(Flag185prime-)
)

)
(:action Flag186Action
:parameters ()
:precondition
(and
(RIGHTOF10-ROBOT)
(LEFTOF9-ROBOT)
)
:effect
(and
(Flag186-)
(Flag186prime-)
)

)
(:action Flag187Action
:parameters ()
:precondition
(and
(RIGHTOF18-ROBOT)
(LEFTOF9-ROBOT)
)
:effect
(and
(Flag187-)
(Flag187prime-)
)

)
(:action Flag188Action
:parameters ()
:precondition
(and
(LEFTOF9-ROBOT)
(RIGHTOF16-ROBOT)
)
:effect
(and
(Flag188-)
(Flag188prime-)
)

)
(:action Flag189Action
:parameters ()
:precondition
(and
(LEFTOF9-ROBOT)
(RIGHTOF17-ROBOT)
)
:effect
(and
(Flag189-)
(Flag189prime-)
)

)
(:action Flag190Action
:parameters ()
:precondition
(and
(LEFTOF9-ROBOT)
(RIGHTOF14-ROBOT)
)
:effect
(and
(Flag190-)
(Flag190prime-)
)

)
(:action Prim179Action-0
:parameters ()
:precondition
(and
(not (RIGHTOF13-ROBOT))
)
:effect
(and
(Flag179prime-)
(not (Flag179-))
)

)
(:action Prim179Action-1
:parameters ()
:precondition
(and
(not (LEFTOF9-ROBOT))
)
:effect
(and
(Flag179prime-)
(not (Flag179-))
)

)
(:action Prim180Action-0
:parameters ()
:precondition
(and
(not (RIGHTOF8-ROBOT))
)
:effect
(and
(Flag180prime-)
(not (Flag180-))
)

)
(:action Prim180Action-1
:parameters ()
:precondition
(and
(not (LEFTOF9-ROBOT))
)
:effect
(and
(Flag180prime-)
(not (Flag180-))
)

)
(:action Prim181Action-0
:parameters ()
:precondition
(and
(not (LEFTOF9-ROBOT))
)
:effect
(and
(Flag181prime-)
(not (Flag181-))
)

)
(:action Prim181Action-1
:parameters ()
:precondition
(and
(not (RIGHTOF12-ROBOT))
)
:effect
(and
(Flag181prime-)
(not (Flag181-))
)

)
(:action Prim182Action-0
:parameters ()
:precondition
(and
(not (LEFTOF9-ROBOT))
)
:effect
(and
(Flag182prime-)
(not (Flag182-))
)

)
(:action Prim182Action-1
:parameters ()
:precondition
(and
(not (RIGHTOF9-ROBOT))
)
:effect
(and
(Flag182prime-)
(not (Flag182-))
)

)
(:action Prim183Action-0
:parameters ()
:precondition
(and
(not (LEFTOF8-ROBOT))
)
:effect
(and
(Flag183prime-)
(not (Flag183-))
)

)
(:action Prim183Action-1
:parameters ()
:precondition
(and
(not (RIGHTOF14-ROBOT))
)
:effect
(and
(Flag183prime-)
(not (Flag183-))
)

)
(:action Prim184Action-0
:parameters ()
:precondition
(and
(not (RIGHTOF11-ROBOT))
)
:effect
(and
(Flag184prime-)
(not (Flag184-))
)

)
(:action Prim184Action-1
:parameters ()
:precondition
(and
(not (LEFTOF9-ROBOT))
)
:effect
(and
(Flag184prime-)
(not (Flag184-))
)

)
(:action Prim185Action-0
:parameters ()
:precondition
(and
(not (LEFTOF9-ROBOT))
)
:effect
(and
(Flag185prime-)
(not (Flag185-))
)

)
(:action Prim185Action-1
:parameters ()
:precondition
(and
(not (RIGHTOF15-ROBOT))
)
:effect
(and
(Flag185prime-)
(not (Flag185-))
)

)
(:action Prim186Action-0
:parameters ()
:precondition
(and
(not (RIGHTOF10-ROBOT))
)
:effect
(and
(Flag186prime-)
(not (Flag186-))
)

)
(:action Prim186Action-1
:parameters ()
:precondition
(and
(not (LEFTOF9-ROBOT))
)
:effect
(and
(Flag186prime-)
(not (Flag186-))
)

)
(:action Prim187Action-0
:parameters ()
:precondition
(and
(not (RIGHTOF18-ROBOT))
)
:effect
(and
(Flag187prime-)
(not (Flag187-))
)

)
(:action Prim187Action-1
:parameters ()
:precondition
(and
(not (LEFTOF9-ROBOT))
)
:effect
(and
(Flag187prime-)
(not (Flag187-))
)

)
(:action Prim188Action-0
:parameters ()
:precondition
(and
(not (LEFTOF9-ROBOT))
)
:effect
(and
(Flag188prime-)
(not (Flag188-))
)

)
(:action Prim188Action-1
:parameters ()
:precondition
(and
(not (RIGHTOF16-ROBOT))
)
:effect
(and
(Flag188prime-)
(not (Flag188-))
)

)
(:action Prim189Action-0
:parameters ()
:precondition
(and
(not (LEFTOF9-ROBOT))
)
:effect
(and
(Flag189prime-)
(not (Flag189-))
)

)
(:action Prim189Action-1
:parameters ()
:precondition
(and
(not (RIGHTOF17-ROBOT))
)
:effect
(and
(Flag189prime-)
(not (Flag189-))
)

)
(:action Prim190Action-0
:parameters ()
:precondition
(and
(not (LEFTOF9-ROBOT))
)
:effect
(and
(Flag190prime-)
(not (Flag190-))
)

)
(:action Prim190Action-1
:parameters ()
:precondition
(and
(not (RIGHTOF14-ROBOT))
)
:effect
(and
(Flag190prime-)
(not (Flag190-))
)

)
(:action Flag192Action
:parameters ()
:precondition
(and
(LEFTOF10-ROBOT)
(RIGHTOF12-ROBOT)
)
:effect
(and
(Flag192-)
(Flag192prime-)
)

)
(:action Flag193Action
:parameters ()
:precondition
(and
(RIGHTOF18-ROBOT)
(LEFTOF10-ROBOT)
)
:effect
(and
(Flag193-)
(Flag193prime-)
)

)
(:action Flag194Action
:parameters ()
:precondition
(and
(RIGHTOF17-ROBOT)
(LEFTOF10-ROBOT)
)
:effect
(and
(Flag194-)
(Flag194prime-)
)

)
(:action Flag195Action
:parameters ()
:precondition
(and
(RIGHTOF14-ROBOT)
(LEFTOF10-ROBOT)
)
:effect
(and
(Flag195-)
(Flag195prime-)
)

)
(:action Flag196Action
:parameters ()
:precondition
(and
(RIGHTOF13-ROBOT)
(LEFTOF10-ROBOT)
)
:effect
(and
(Flag196-)
(Flag196prime-)
)

)
(:action Flag197Action
:parameters ()
:precondition
(and
(LEFTOF10-ROBOT)
(RIGHTOF9-ROBOT)
)
:effect
(and
(Flag197-)
(Flag197prime-)
)

)
(:action Flag198Action
:parameters ()
:precondition
(and
(RIGHTOF11-ROBOT)
(LEFTOF10-ROBOT)
)
:effect
(and
(Flag198-)
(Flag198prime-)
)

)
(:action Flag199Action
:parameters ()
:precondition
(and
(RIGHTOF10-ROBOT)
(LEFTOF10-ROBOT)
)
:effect
(and
(Flag199-)
(Flag199prime-)
)

)
(:action Flag200Action
:parameters ()
:precondition
(and
(RIGHTOF16-ROBOT)
(LEFTOF10-ROBOT)
)
:effect
(and
(Flag200-)
(Flag200prime-)
)

)
(:action Flag201Action
:parameters ()
:precondition
(and
(LEFTOF10-ROBOT)
(RIGHTOF15-ROBOT)
)
:effect
(and
(Flag201-)
(Flag201prime-)
)

)
(:action Prim192Action-0
:parameters ()
:precondition
(and
(not (LEFTOF10-ROBOT))
)
:effect
(and
(Flag192prime-)
(not (Flag192-))
)

)
(:action Prim192Action-1
:parameters ()
:precondition
(and
(not (RIGHTOF12-ROBOT))
)
:effect
(and
(Flag192prime-)
(not (Flag192-))
)

)
(:action Prim193Action-0
:parameters ()
:precondition
(and
(not (RIGHTOF18-ROBOT))
)
:effect
(and
(Flag193prime-)
(not (Flag193-))
)

)
(:action Prim193Action-1
:parameters ()
:precondition
(and
(not (LEFTOF10-ROBOT))
)
:effect
(and
(Flag193prime-)
(not (Flag193-))
)

)
(:action Prim194Action-0
:parameters ()
:precondition
(and
(not (RIGHTOF17-ROBOT))
)
:effect
(and
(Flag194prime-)
(not (Flag194-))
)

)
(:action Prim194Action-1
:parameters ()
:precondition
(and
(not (LEFTOF10-ROBOT))
)
:effect
(and
(Flag194prime-)
(not (Flag194-))
)

)
(:action Prim195Action-0
:parameters ()
:precondition
(and
(not (RIGHTOF14-ROBOT))
)
:effect
(and
(Flag195prime-)
(not (Flag195-))
)

)
(:action Prim195Action-1
:parameters ()
:precondition
(and
(not (LEFTOF10-ROBOT))
)
:effect
(and
(Flag195prime-)
(not (Flag195-))
)

)
(:action Prim196Action-0
:parameters ()
:precondition
(and
(not (RIGHTOF13-ROBOT))
)
:effect
(and
(Flag196prime-)
(not (Flag196-))
)

)
(:action Prim196Action-1
:parameters ()
:precondition
(and
(not (LEFTOF10-ROBOT))
)
:effect
(and
(Flag196prime-)
(not (Flag196-))
)

)
(:action Prim197Action-0
:parameters ()
:precondition
(and
(not (LEFTOF10-ROBOT))
)
:effect
(and
(Flag197prime-)
(not (Flag197-))
)

)
(:action Prim197Action-1
:parameters ()
:precondition
(and
(not (RIGHTOF9-ROBOT))
)
:effect
(and
(Flag197prime-)
(not (Flag197-))
)

)
(:action Prim198Action-0
:parameters ()
:precondition
(and
(not (RIGHTOF11-ROBOT))
)
:effect
(and
(Flag198prime-)
(not (Flag198-))
)

)
(:action Prim198Action-1
:parameters ()
:precondition
(and
(not (LEFTOF10-ROBOT))
)
:effect
(and
(Flag198prime-)
(not (Flag198-))
)

)
(:action Prim199Action-0
:parameters ()
:precondition
(and
(not (RIGHTOF10-ROBOT))
)
:effect
(and
(Flag199prime-)
(not (Flag199-))
)

)
(:action Prim199Action-1
:parameters ()
:precondition
(and
(not (LEFTOF10-ROBOT))
)
:effect
(and
(Flag199prime-)
(not (Flag199-))
)

)
(:action Prim200Action-0
:parameters ()
:precondition
(and
(not (RIGHTOF16-ROBOT))
)
:effect
(and
(Flag200prime-)
(not (Flag200-))
)

)
(:action Prim200Action-1
:parameters ()
:precondition
(and
(not (LEFTOF10-ROBOT))
)
:effect
(and
(Flag200prime-)
(not (Flag200-))
)

)
(:action Prim201Action-0
:parameters ()
:precondition
(and
(not (LEFTOF10-ROBOT))
)
:effect
(and
(Flag201prime-)
(not (Flag201-))
)

)
(:action Prim201Action-1
:parameters ()
:precondition
(and
(not (RIGHTOF15-ROBOT))
)
:effect
(and
(Flag201prime-)
(not (Flag201-))
)

)
(:action Flag203Action
:parameters ()
:precondition
(and
(LEFTOF11-ROBOT)
(RIGHTOF15-ROBOT)
)
:effect
(and
(Flag203-)
(Flag203prime-)
)

)
(:action Flag204Action
:parameters ()
:precondition
(and
(LEFTOF11-ROBOT)
(RIGHTOF16-ROBOT)
)
:effect
(and
(Flag204-)
(Flag204prime-)
)

)
(:action Flag205Action
:parameters ()
:precondition
(and
(RIGHTOF11-ROBOT)
(LEFTOF11-ROBOT)
)
:effect
(and
(Flag205-)
(Flag205prime-)
)

)
(:action Flag206Action
:parameters ()
:precondition
(and
(LEFTOF10-ROBOT)
(RIGHTOF14-ROBOT)
)
:effect
(and
(Flag206-)
(Flag206prime-)
)

)
(:action Flag207Action
:parameters ()
:precondition
(and
(RIGHTOF13-ROBOT)
(LEFTOF11-ROBOT)
)
:effect
(and
(Flag207-)
(Flag207prime-)
)

)
(:action Flag208Action
:parameters ()
:precondition
(and
(LEFTOF11-ROBOT)
(RIGHTOF14-ROBOT)
)
:effect
(and
(Flag208-)
(Flag208prime-)
)

)
(:action Flag209Action
:parameters ()
:precondition
(and
(LEFTOF11-ROBOT)
(RIGHTOF12-ROBOT)
)
:effect
(and
(Flag209-)
(Flag209prime-)
)

)
(:action Flag210Action
:parameters ()
:precondition
(and
(RIGHTOF18-ROBOT)
(LEFTOF11-ROBOT)
)
:effect
(and
(Flag210-)
(Flag210prime-)
)

)
(:action Flag211Action
:parameters ()
:precondition
(and
(LEFTOF11-ROBOT)
(RIGHTOF17-ROBOT)
)
:effect
(and
(Flag211-)
(Flag211prime-)
)

)
(:action Flag212Action
:parameters ()
:precondition
(and
(RIGHTOF10-ROBOT)
(LEFTOF11-ROBOT)
)
:effect
(and
(Flag212-)
(Flag212prime-)
)

)
(:action Prim203Action-0
:parameters ()
:precondition
(and
(not (LEFTOF11-ROBOT))
)
:effect
(and
(Flag203prime-)
(not (Flag203-))
)

)
(:action Prim203Action-1
:parameters ()
:precondition
(and
(not (RIGHTOF15-ROBOT))
)
:effect
(and
(Flag203prime-)
(not (Flag203-))
)

)
(:action Prim204Action-0
:parameters ()
:precondition
(and
(not (LEFTOF11-ROBOT))
)
:effect
(and
(Flag204prime-)
(not (Flag204-))
)

)
(:action Prim204Action-1
:parameters ()
:precondition
(and
(not (RIGHTOF16-ROBOT))
)
:effect
(and
(Flag204prime-)
(not (Flag204-))
)

)
(:action Prim205Action-0
:parameters ()
:precondition
(and
(not (RIGHTOF11-ROBOT))
)
:effect
(and
(Flag205prime-)
(not (Flag205-))
)

)
(:action Prim205Action-1
:parameters ()
:precondition
(and
(not (LEFTOF11-ROBOT))
)
:effect
(and
(Flag205prime-)
(not (Flag205-))
)

)
(:action Prim206Action-0
:parameters ()
:precondition
(and
(not (LEFTOF10-ROBOT))
)
:effect
(and
(Flag206prime-)
(not (Flag206-))
)

)
(:action Prim206Action-1
:parameters ()
:precondition
(and
(not (RIGHTOF14-ROBOT))
)
:effect
(and
(Flag206prime-)
(not (Flag206-))
)

)
(:action Prim207Action-0
:parameters ()
:precondition
(and
(not (RIGHTOF13-ROBOT))
)
:effect
(and
(Flag207prime-)
(not (Flag207-))
)

)
(:action Prim207Action-1
:parameters ()
:precondition
(and
(not (LEFTOF11-ROBOT))
)
:effect
(and
(Flag207prime-)
(not (Flag207-))
)

)
(:action Prim208Action-0
:parameters ()
:precondition
(and
(not (LEFTOF11-ROBOT))
)
:effect
(and
(Flag208prime-)
(not (Flag208-))
)

)
(:action Prim208Action-1
:parameters ()
:precondition
(and
(not (RIGHTOF14-ROBOT))
)
:effect
(and
(Flag208prime-)
(not (Flag208-))
)

)
(:action Prim209Action-0
:parameters ()
:precondition
(and
(not (LEFTOF11-ROBOT))
)
:effect
(and
(Flag209prime-)
(not (Flag209-))
)

)
(:action Prim209Action-1
:parameters ()
:precondition
(and
(not (RIGHTOF12-ROBOT))
)
:effect
(and
(Flag209prime-)
(not (Flag209-))
)

)
(:action Prim210Action-0
:parameters ()
:precondition
(and
(not (RIGHTOF18-ROBOT))
)
:effect
(and
(Flag210prime-)
(not (Flag210-))
)

)
(:action Prim210Action-1
:parameters ()
:precondition
(and
(not (LEFTOF11-ROBOT))
)
:effect
(and
(Flag210prime-)
(not (Flag210-))
)

)
(:action Prim211Action-0
:parameters ()
:precondition
(and
(not (LEFTOF11-ROBOT))
)
:effect
(and
(Flag211prime-)
(not (Flag211-))
)

)
(:action Prim211Action-1
:parameters ()
:precondition
(and
(not (RIGHTOF17-ROBOT))
)
:effect
(and
(Flag211prime-)
(not (Flag211-))
)

)
(:action Prim212Action-0
:parameters ()
:precondition
(and
(not (RIGHTOF10-ROBOT))
)
:effect
(and
(Flag212prime-)
(not (Flag212-))
)

)
(:action Prim212Action-1
:parameters ()
:precondition
(and
(not (LEFTOF11-ROBOT))
)
:effect
(and
(Flag212prime-)
(not (Flag212-))
)

)
(:action Flag214Action
:parameters ()
:precondition
(and
(RIGHTOF14-ROBOT)
(LEFTOF12-ROBOT)
)
:effect
(and
(Flag214-)
(Flag214prime-)
)

)
(:action Flag215Action
:parameters ()
:precondition
(and
(RIGHTOF11-ROBOT)
(LEFTOF12-ROBOT)
)
:effect
(and
(Flag215-)
(Flag215prime-)
)

)
(:action Flag216Action
:parameters ()
:precondition
(and
(RIGHTOF18-ROBOT)
(LEFTOF12-ROBOT)
)
:effect
(and
(Flag216-)
(Flag216prime-)
)

)
(:action Flag217Action
:parameters ()
:precondition
(and
(RIGHTOF12-ROBOT)
(LEFTOF12-ROBOT)
)
:effect
(and
(Flag217-)
(Flag217prime-)
)

)
(:action Flag218Action
:parameters ()
:precondition
(and
(RIGHTOF16-ROBOT)
(LEFTOF12-ROBOT)
)
:effect
(and
(Flag218-)
(Flag218prime-)
)

)
(:action Flag219Action
:parameters ()
:precondition
(and
(RIGHTOF17-ROBOT)
(LEFTOF12-ROBOT)
)
:effect
(and
(Flag219-)
(Flag219prime-)
)

)
(:action Flag220Action
:parameters ()
:precondition
(and
(RIGHTOF15-ROBOT)
(LEFTOF12-ROBOT)
)
:effect
(and
(Flag220-)
(Flag220prime-)
)

)
(:action Flag221Action
:parameters ()
:precondition
(and
(RIGHTOF13-ROBOT)
(LEFTOF12-ROBOT)
)
:effect
(and
(Flag221-)
(Flag221prime-)
)

)
(:action Prim214Action-0
:parameters ()
:precondition
(and
(not (RIGHTOF14-ROBOT))
)
:effect
(and
(Flag214prime-)
(not (Flag214-))
)

)
(:action Prim214Action-1
:parameters ()
:precondition
(and
(not (LEFTOF12-ROBOT))
)
:effect
(and
(Flag214prime-)
(not (Flag214-))
)

)
(:action Prim215Action-0
:parameters ()
:precondition
(and
(not (RIGHTOF11-ROBOT))
)
:effect
(and
(Flag215prime-)
(not (Flag215-))
)

)
(:action Prim215Action-1
:parameters ()
:precondition
(and
(not (LEFTOF12-ROBOT))
)
:effect
(and
(Flag215prime-)
(not (Flag215-))
)

)
(:action Prim216Action-0
:parameters ()
:precondition
(and
(not (RIGHTOF18-ROBOT))
)
:effect
(and
(Flag216prime-)
(not (Flag216-))
)

)
(:action Prim216Action-1
:parameters ()
:precondition
(and
(not (LEFTOF12-ROBOT))
)
:effect
(and
(Flag216prime-)
(not (Flag216-))
)

)
(:action Prim217Action-0
:parameters ()
:precondition
(and
(not (RIGHTOF12-ROBOT))
)
:effect
(and
(Flag217prime-)
(not (Flag217-))
)

)
(:action Prim217Action-1
:parameters ()
:precondition
(and
(not (LEFTOF12-ROBOT))
)
:effect
(and
(Flag217prime-)
(not (Flag217-))
)

)
(:action Prim218Action-0
:parameters ()
:precondition
(and
(not (RIGHTOF16-ROBOT))
)
:effect
(and
(Flag218prime-)
(not (Flag218-))
)

)
(:action Prim218Action-1
:parameters ()
:precondition
(and
(not (LEFTOF12-ROBOT))
)
:effect
(and
(Flag218prime-)
(not (Flag218-))
)

)
(:action Prim219Action-0
:parameters ()
:precondition
(and
(not (RIGHTOF17-ROBOT))
)
:effect
(and
(Flag219prime-)
(not (Flag219-))
)

)
(:action Prim219Action-1
:parameters ()
:precondition
(and
(not (LEFTOF12-ROBOT))
)
:effect
(and
(Flag219prime-)
(not (Flag219-))
)

)
(:action Prim220Action-0
:parameters ()
:precondition
(and
(not (RIGHTOF15-ROBOT))
)
:effect
(and
(Flag220prime-)
(not (Flag220-))
)

)
(:action Prim220Action-1
:parameters ()
:precondition
(and
(not (LEFTOF12-ROBOT))
)
:effect
(and
(Flag220prime-)
(not (Flag220-))
)

)
(:action Prim221Action-0
:parameters ()
:precondition
(and
(not (RIGHTOF13-ROBOT))
)
:effect
(and
(Flag221prime-)
(not (Flag221-))
)

)
(:action Prim221Action-1
:parameters ()
:precondition
(and
(not (LEFTOF12-ROBOT))
)
:effect
(and
(Flag221prime-)
(not (Flag221-))
)

)
(:action Flag223Action
:parameters ()
:precondition
(and
(RIGHTOF17-ROBOT)
(LEFTOF13-ROBOT)
)
:effect
(and
(Flag223-)
(Flag223prime-)
)

)
(:action Flag224Action
:parameters ()
:precondition
(and
(RIGHTOF13-ROBOT)
(LEFTOF13-ROBOT)
)
:effect
(and
(Flag224-)
(Flag224prime-)
)

)
(:action Flag225Action
:parameters ()
:precondition
(and
(LEFTOF13-ROBOT)
(RIGHTOF15-ROBOT)
)
:effect
(and
(Flag225-)
(Flag225prime-)
)

)
(:action Flag226Action
:parameters ()
:precondition
(and
(LEFTOF13-ROBOT)
(RIGHTOF14-ROBOT)
)
:effect
(and
(Flag226-)
(Flag226prime-)
)

)
(:action Flag227Action
:parameters ()
:precondition
(and
(LEFTOF12-ROBOT)
(RIGHTOF12-ROBOT)
)
:effect
(and
(Flag227-)
(Flag227prime-)
)

)
(:action Flag228Action
:parameters ()
:precondition
(and
(RIGHTOF18-ROBOT)
(LEFTOF13-ROBOT)
)
:effect
(and
(Flag228-)
(Flag228prime-)
)

)
(:action Flag229Action
:parameters ()
:precondition
(and
(LEFTOF13-ROBOT)
(RIGHTOF12-ROBOT)
)
:effect
(and
(Flag229-)
(Flag229prime-)
)

)
(:action Flag230Action
:parameters ()
:precondition
(and
(RIGHTOF16-ROBOT)
(LEFTOF13-ROBOT)
)
:effect
(and
(Flag230-)
(Flag230prime-)
)

)
(:action Prim223Action-0
:parameters ()
:precondition
(and
(not (RIGHTOF17-ROBOT))
)
:effect
(and
(Flag223prime-)
(not (Flag223-))
)

)
(:action Prim223Action-1
:parameters ()
:precondition
(and
(not (LEFTOF13-ROBOT))
)
:effect
(and
(Flag223prime-)
(not (Flag223-))
)

)
(:action Prim224Action-0
:parameters ()
:precondition
(and
(not (RIGHTOF13-ROBOT))
)
:effect
(and
(Flag224prime-)
(not (Flag224-))
)

)
(:action Prim224Action-1
:parameters ()
:precondition
(and
(not (LEFTOF13-ROBOT))
)
:effect
(and
(Flag224prime-)
(not (Flag224-))
)

)
(:action Prim225Action-0
:parameters ()
:precondition
(and
(not (LEFTOF13-ROBOT))
)
:effect
(and
(Flag225prime-)
(not (Flag225-))
)

)
(:action Prim225Action-1
:parameters ()
:precondition
(and
(not (RIGHTOF15-ROBOT))
)
:effect
(and
(Flag225prime-)
(not (Flag225-))
)

)
(:action Prim226Action-0
:parameters ()
:precondition
(and
(not (LEFTOF13-ROBOT))
)
:effect
(and
(Flag226prime-)
(not (Flag226-))
)

)
(:action Prim226Action-1
:parameters ()
:precondition
(and
(not (RIGHTOF14-ROBOT))
)
:effect
(and
(Flag226prime-)
(not (Flag226-))
)

)
(:action Prim227Action-0
:parameters ()
:precondition
(and
(not (LEFTOF12-ROBOT))
)
:effect
(and
(Flag227prime-)
(not (Flag227-))
)

)
(:action Prim227Action-1
:parameters ()
:precondition
(and
(not (RIGHTOF12-ROBOT))
)
:effect
(and
(Flag227prime-)
(not (Flag227-))
)

)
(:action Prim228Action-0
:parameters ()
:precondition
(and
(not (RIGHTOF18-ROBOT))
)
:effect
(and
(Flag228prime-)
(not (Flag228-))
)

)
(:action Prim228Action-1
:parameters ()
:precondition
(and
(not (LEFTOF13-ROBOT))
)
:effect
(and
(Flag228prime-)
(not (Flag228-))
)

)
(:action Prim229Action-0
:parameters ()
:precondition
(and
(not (LEFTOF13-ROBOT))
)
:effect
(and
(Flag229prime-)
(not (Flag229-))
)

)
(:action Prim229Action-1
:parameters ()
:precondition
(and
(not (RIGHTOF12-ROBOT))
)
:effect
(and
(Flag229prime-)
(not (Flag229-))
)

)
(:action Prim230Action-0
:parameters ()
:precondition
(and
(not (RIGHTOF16-ROBOT))
)
:effect
(and
(Flag230prime-)
(not (Flag230-))
)

)
(:action Prim230Action-1
:parameters ()
:precondition
(and
(not (LEFTOF13-ROBOT))
)
:effect
(and
(Flag230prime-)
(not (Flag230-))
)

)
(:action Flag232Action
:parameters ()
:precondition
(and
(LEFTOF14-ROBOT)
(RIGHTOF14-ROBOT)
)
:effect
(and
(Flag232-)
(Flag232prime-)
)

)
(:action Flag233Action
:parameters ()
:precondition
(and
(RIGHTOF13-ROBOT)
(LEFTOF14-ROBOT)
)
:effect
(and
(Flag233-)
(Flag233prime-)
)

)
(:action Flag234Action
:parameters ()
:precondition
(and
(LEFTOF14-ROBOT)
(RIGHTOF17-ROBOT)
)
:effect
(and
(Flag234-)
(Flag234prime-)
)

)
(:action Flag235Action
:parameters ()
:precondition
(and
(LEFTOF14-ROBOT)
(RIGHTOF16-ROBOT)
)
:effect
(and
(Flag235-)
(Flag235prime-)
)

)
(:action Flag236Action
:parameters ()
:precondition
(and
(RIGHTOF18-ROBOT)
(LEFTOF14-ROBOT)
)
:effect
(and
(Flag236-)
(Flag236prime-)
)

)
(:action Flag237Action
:parameters ()
:precondition
(and
(LEFTOF14-ROBOT)
(RIGHTOF15-ROBOT)
)
:effect
(and
(Flag237-)
(Flag237prime-)
)

)
(:action Prim232Action-0
:parameters ()
:precondition
(and
(not (LEFTOF14-ROBOT))
)
:effect
(and
(Flag232prime-)
(not (Flag232-))
)

)
(:action Prim232Action-1
:parameters ()
:precondition
(and
(not (RIGHTOF14-ROBOT))
)
:effect
(and
(Flag232prime-)
(not (Flag232-))
)

)
(:action Prim233Action-0
:parameters ()
:precondition
(and
(not (RIGHTOF13-ROBOT))
)
:effect
(and
(Flag233prime-)
(not (Flag233-))
)

)
(:action Prim233Action-1
:parameters ()
:precondition
(and
(not (LEFTOF14-ROBOT))
)
:effect
(and
(Flag233prime-)
(not (Flag233-))
)

)
(:action Prim234Action-0
:parameters ()
:precondition
(and
(not (LEFTOF14-ROBOT))
)
:effect
(and
(Flag234prime-)
(not (Flag234-))
)

)
(:action Prim234Action-1
:parameters ()
:precondition
(and
(not (RIGHTOF17-ROBOT))
)
:effect
(and
(Flag234prime-)
(not (Flag234-))
)

)
(:action Prim235Action-0
:parameters ()
:precondition
(and
(not (LEFTOF14-ROBOT))
)
:effect
(and
(Flag235prime-)
(not (Flag235-))
)

)
(:action Prim235Action-1
:parameters ()
:precondition
(and
(not (RIGHTOF16-ROBOT))
)
:effect
(and
(Flag235prime-)
(not (Flag235-))
)

)
(:action Prim236Action-0
:parameters ()
:precondition
(and
(not (RIGHTOF18-ROBOT))
)
:effect
(and
(Flag236prime-)
(not (Flag236-))
)

)
(:action Prim236Action-1
:parameters ()
:precondition
(and
(not (LEFTOF14-ROBOT))
)
:effect
(and
(Flag236prime-)
(not (Flag236-))
)

)
(:action Prim237Action-0
:parameters ()
:precondition
(and
(not (LEFTOF14-ROBOT))
)
:effect
(and
(Flag237prime-)
(not (Flag237-))
)

)
(:action Prim237Action-1
:parameters ()
:precondition
(and
(not (RIGHTOF15-ROBOT))
)
:effect
(and
(Flag237prime-)
(not (Flag237-))
)

)
(:action Flag239Action
:parameters ()
:precondition
(and
(LEFTOF14-ROBOT)
(RIGHTOF18-ROBOT)
)
:effect
(and
(Flag239-)
(Flag239prime-)
)

)
(:action Flag240Action
:parameters ()
:precondition
(and
(RIGHTOF18-ROBOT)
(LEFTOF15-ROBOT)
)
:effect
(and
(Flag240-)
(Flag240prime-)
)

)
(:action Flag241Action
:parameters ()
:precondition
(and
(RIGHTOF17-ROBOT)
(LEFTOF15-ROBOT)
)
:effect
(and
(Flag241-)
(Flag241prime-)
)

)
(:action Flag242Action
:parameters ()
:precondition
(and
(RIGHTOF16-ROBOT)
(LEFTOF15-ROBOT)
)
:effect
(and
(Flag242-)
(Flag242prime-)
)

)
(:action Flag243Action
:parameters ()
:precondition
(and
(RIGHTOF15-ROBOT)
(LEFTOF15-ROBOT)
)
:effect
(and
(Flag243-)
(Flag243prime-)
)

)
(:action Flag244Action
:parameters ()
:precondition
(and
(RIGHTOF14-ROBOT)
(LEFTOF15-ROBOT)
)
:effect
(and
(Flag244-)
(Flag244prime-)
)

)
(:action Prim239Action-0
:parameters ()
:precondition
(and
(not (LEFTOF14-ROBOT))
)
:effect
(and
(Flag239prime-)
(not (Flag239-))
)

)
(:action Prim239Action-1
:parameters ()
:precondition
(and
(not (RIGHTOF18-ROBOT))
)
:effect
(and
(Flag239prime-)
(not (Flag239-))
)

)
(:action Prim240Action-0
:parameters ()
:precondition
(and
(not (RIGHTOF18-ROBOT))
)
:effect
(and
(Flag240prime-)
(not (Flag240-))
)

)
(:action Prim240Action-1
:parameters ()
:precondition
(and
(not (LEFTOF15-ROBOT))
)
:effect
(and
(Flag240prime-)
(not (Flag240-))
)

)
(:action Prim241Action-0
:parameters ()
:precondition
(and
(not (RIGHTOF17-ROBOT))
)
:effect
(and
(Flag241prime-)
(not (Flag241-))
)

)
(:action Prim241Action-1
:parameters ()
:precondition
(and
(not (LEFTOF15-ROBOT))
)
:effect
(and
(Flag241prime-)
(not (Flag241-))
)

)
(:action Prim242Action-0
:parameters ()
:precondition
(and
(not (RIGHTOF16-ROBOT))
)
:effect
(and
(Flag242prime-)
(not (Flag242-))
)

)
(:action Prim242Action-1
:parameters ()
:precondition
(and
(not (LEFTOF15-ROBOT))
)
:effect
(and
(Flag242prime-)
(not (Flag242-))
)

)
(:action Prim243Action-0
:parameters ()
:precondition
(and
(not (RIGHTOF15-ROBOT))
)
:effect
(and
(Flag243prime-)
(not (Flag243-))
)

)
(:action Prim243Action-1
:parameters ()
:precondition
(and
(not (LEFTOF15-ROBOT))
)
:effect
(and
(Flag243prime-)
(not (Flag243-))
)

)
(:action Prim244Action-0
:parameters ()
:precondition
(and
(not (RIGHTOF14-ROBOT))
)
:effect
(and
(Flag244prime-)
(not (Flag244-))
)

)
(:action Prim244Action-1
:parameters ()
:precondition
(and
(not (LEFTOF15-ROBOT))
)
:effect
(and
(Flag244prime-)
(not (Flag244-))
)

)
(:action Flag246Action
:parameters ()
:precondition
(and
(LEFTOF16-ROBOT)
(RIGHTOF17-ROBOT)
)
:effect
(and
(Flag246-)
(Flag246prime-)
)

)
(:action Flag247Action
:parameters ()
:precondition
(and
(RIGHTOF18-ROBOT)
(LEFTOF16-ROBOT)
)
:effect
(and
(Flag247-)
(Flag247prime-)
)

)
(:action Flag248Action
:parameters ()
:precondition
(and
(LEFTOF15-ROBOT)
(RIGHTOF15-ROBOT)
)
:effect
(and
(Flag248-)
(Flag248prime-)
)

)
(:action Flag249Action
:parameters ()
:precondition
(and
(RIGHTOF16-ROBOT)
(LEFTOF16-ROBOT)
)
:effect
(and
(Flag249-)
(Flag249prime-)
)

)
(:action Flag250Action
:parameters ()
:precondition
(and
(LEFTOF16-ROBOT)
(RIGHTOF15-ROBOT)
)
:effect
(and
(Flag250-)
(Flag250prime-)
)

)
(:action Prim246Action-0
:parameters ()
:precondition
(and
(not (LEFTOF16-ROBOT))
)
:effect
(and
(Flag246prime-)
(not (Flag246-))
)

)
(:action Prim246Action-1
:parameters ()
:precondition
(and
(not (RIGHTOF17-ROBOT))
)
:effect
(and
(Flag246prime-)
(not (Flag246-))
)

)
(:action Prim247Action-0
:parameters ()
:precondition
(and
(not (RIGHTOF18-ROBOT))
)
:effect
(and
(Flag247prime-)
(not (Flag247-))
)

)
(:action Prim247Action-1
:parameters ()
:precondition
(and
(not (LEFTOF16-ROBOT))
)
:effect
(and
(Flag247prime-)
(not (Flag247-))
)

)
(:action Prim248Action-0
:parameters ()
:precondition
(and
(not (LEFTOF15-ROBOT))
)
:effect
(and
(Flag248prime-)
(not (Flag248-))
)

)
(:action Prim248Action-1
:parameters ()
:precondition
(and
(not (RIGHTOF15-ROBOT))
)
:effect
(and
(Flag248prime-)
(not (Flag248-))
)

)
(:action Prim249Action-0
:parameters ()
:precondition
(and
(not (RIGHTOF16-ROBOT))
)
:effect
(and
(Flag249prime-)
(not (Flag249-))
)

)
(:action Prim249Action-1
:parameters ()
:precondition
(and
(not (LEFTOF16-ROBOT))
)
:effect
(and
(Flag249prime-)
(not (Flag249-))
)

)
(:action Prim250Action-0
:parameters ()
:precondition
(and
(not (LEFTOF16-ROBOT))
)
:effect
(and
(Flag250prime-)
(not (Flag250-))
)

)
(:action Prim250Action-1
:parameters ()
:precondition
(and
(not (RIGHTOF15-ROBOT))
)
:effect
(and
(Flag250prime-)
(not (Flag250-))
)

)
(:action Flag252Action
:parameters ()
:precondition
(and
(LEFTOF17-ROBOT)
(RIGHTOF17-ROBOT)
)
:effect
(and
(Flag252-)
(Flag252prime-)
)

)
(:action Flag253Action
:parameters ()
:precondition
(and
(LEFTOF17-ROBOT)
(RIGHTOF16-ROBOT)
)
:effect
(and
(Flag253-)
(Flag253prime-)
)

)
(:action Flag254Action
:parameters ()
:precondition
(and
(LEFTOF17-ROBOT)
(RIGHTOF18-ROBOT)
)
:effect
(and
(Flag254-)
(Flag254prime-)
)

)
(:action Prim252Action-0
:parameters ()
:precondition
(and
(not (LEFTOF17-ROBOT))
)
:effect
(and
(Flag252prime-)
(not (Flag252-))
)

)
(:action Prim252Action-1
:parameters ()
:precondition
(and
(not (RIGHTOF17-ROBOT))
)
:effect
(and
(Flag252prime-)
(not (Flag252-))
)

)
(:action Prim253Action-0
:parameters ()
:precondition
(and
(not (LEFTOF17-ROBOT))
)
:effect
(and
(Flag253prime-)
(not (Flag253-))
)

)
(:action Prim253Action-1
:parameters ()
:precondition
(and
(not (RIGHTOF16-ROBOT))
)
:effect
(and
(Flag253prime-)
(not (Flag253-))
)

)
(:action Prim254Action-0
:parameters ()
:precondition
(and
(not (LEFTOF17-ROBOT))
)
:effect
(and
(Flag254prime-)
(not (Flag254-))
)

)
(:action Prim254Action-1
:parameters ()
:precondition
(and
(not (RIGHTOF18-ROBOT))
)
:effect
(and
(Flag254prime-)
(not (Flag254-))
)

)
(:action Flag256Action
:parameters ()
:precondition
(and
(RIGHTOF18-ROBOT)
(LEFTOF18-ROBOT)
)
:effect
(and
(Flag256-)
(Flag256prime-)
)

)
(:action Flag257Action
:parameters ()
:precondition
(and
(RIGHTOF17-ROBOT)
(LEFTOF18-ROBOT)
)
:effect
(and
(Flag257-)
(Flag257prime-)
)

)
(:action Prim256Action-0
:parameters ()
:precondition
(and
(not (RIGHTOF18-ROBOT))
)
:effect
(and
(Flag256prime-)
(not (Flag256-))
)

)
(:action Prim257Action-0
:parameters ()
:precondition
(and
(not (RIGHTOF17-ROBOT))
)
:effect
(and
(Flag257prime-)
(not (Flag257-))
)

)
(:action Flag259Action
:parameters ()
:precondition
(and
(RIGHTOF18-ROBOT)
(LEFTOF19-ROBOT)
)
:effect
(and
(Flag259-)
(Flag259prime-)
)

)
(:action Prim259Action-0
:parameters ()
:precondition
(and
(not (RIGHTOF18-ROBOT))
)
:effect
(and
(Flag259prime-)
(not (Flag259-))
)

)
(:action Prim259Action-1
:parameters ()
:precondition
(and
(not (LEFTOF19-ROBOT))
)
:effect
(and
(Flag259prime-)
(not (Flag259-))
)

)
(:action Flag261Action-0
:parameters ()
:precondition
(and
(RIGHTOF3-ROBOT)
)
:effect
(and
(Flag261-)
(Flag261prime-)
)

)
(:action Flag261Action-1
:parameters ()
:precondition
(and
(RIGHTOF8-ROBOT)
)
:effect
(and
(Flag261-)
(Flag261prime-)
)

)
(:action Flag261Action-2
:parameters ()
:precondition
(and
(RIGHTOF10-ROBOT)
)
:effect
(and
(Flag261-)
(Flag261prime-)
)

)
(:action Flag261Action-3
:parameters ()
:precondition
(and
(RIGHTOF5-ROBOT)
)
:effect
(and
(Flag261-)
(Flag261prime-)
)

)
(:action Flag261Action-4
:parameters ()
:precondition
(and
(RIGHTOF9-ROBOT)
)
:effect
(and
(Flag261-)
(Flag261prime-)
)

)
(:action Flag261Action-6
:parameters ()
:precondition
(and
(RIGHTOF17-ROBOT)
)
:effect
(and
(Flag261-)
(Flag261prime-)
)

)
(:action Flag261Action-7
:parameters ()
:precondition
(and
(RIGHTOF14-ROBOT)
)
:effect
(and
(Flag261-)
(Flag261prime-)
)

)
(:action Flag261Action-8
:parameters ()
:precondition
(and
(RIGHTOF13-ROBOT)
)
:effect
(and
(Flag261-)
(Flag261prime-)
)

)
(:action Flag261Action-9
:parameters ()
:precondition
(and
(RIGHTOF12-ROBOT)
)
:effect
(and
(Flag261-)
(Flag261prime-)
)

)
(:action Flag261Action-10
:parameters ()
:precondition
(and
(RIGHTOF0-ROBOT)
)
:effect
(and
(Flag261-)
(Flag261prime-)
)

)
(:action Flag261Action-11
:parameters ()
:precondition
(and
(RIGHTOF4-ROBOT)
)
:effect
(and
(Flag261-)
(Flag261prime-)
)

)
(:action Flag261Action-12
:parameters ()
:precondition
(and
(RIGHTOF1-ROBOT)
)
:effect
(and
(Flag261-)
(Flag261prime-)
)

)
(:action Flag261Action-13
:parameters ()
:precondition
(and
(RIGHTOF16-ROBOT)
)
:effect
(and
(Flag261-)
(Flag261prime-)
)

)
(:action Flag261Action-14
:parameters ()
:precondition
(and
(RIGHTOF18-ROBOT)
)
:effect
(and
(Flag261-)
(Flag261prime-)
)

)
(:action Flag261Action-15
:parameters ()
:precondition
(and
(RIGHTOF7-ROBOT)
)
:effect
(and
(Flag261-)
(Flag261prime-)
)

)
(:action Flag261Action-16
:parameters ()
:precondition
(and
(RIGHTOF11-ROBOT)
)
:effect
(and
(Flag261-)
(Flag261prime-)
)

)
(:action Flag261Action-17
:parameters ()
:precondition
(and
(RIGHTOF15-ROBOT)
)
:effect
(and
(Flag261-)
(Flag261prime-)
)

)
(:action Flag261Action-18
:parameters ()
:precondition
(and
(RIGHTOF6-ROBOT)
)
:effect
(and
(Flag261-)
(Flag261prime-)
)

)
(:action Prim261Action
:parameters ()
:precondition
(and
(not (RIGHTOF3-ROBOT))
(not (RIGHTOF8-ROBOT))
(not (RIGHTOF10-ROBOT))
(not (RIGHTOF5-ROBOT))
(not (RIGHTOF9-ROBOT))
(not (RIGHTOF2-ROBOT))
(not (RIGHTOF17-ROBOT))
(not (RIGHTOF14-ROBOT))
(not (RIGHTOF13-ROBOT))
(not (RIGHTOF12-ROBOT))
(not (RIGHTOF0-ROBOT))
(not (RIGHTOF4-ROBOT))
(not (RIGHTOF1-ROBOT))
(not (RIGHTOF16-ROBOT))
(not (RIGHTOF18-ROBOT))
(not (RIGHTOF7-ROBOT))
(not (RIGHTOF11-ROBOT))
(not (RIGHTOF15-ROBOT))
(not (RIGHTOF6-ROBOT))
)
:effect
(and
(Flag261prime-)
(not (Flag261-))
)

)
(:action Flag262Action
:parameters ()
:precondition
(and
(RIGHTOF0-ROBOT)
(LEFTOF1-ROBOT)
)
:effect
(and
(Flag262-)
(Flag262prime-)
)

)
(:action Prim262Action-0
:parameters ()
:precondition
(and
(not (RIGHTOF0-ROBOT))
)
:effect
(and
(Flag262prime-)
(not (Flag262-))
)

)
(:action Prim262Action-1
:parameters ()
:precondition
(and
(not (LEFTOF1-ROBOT))
)
:effect
(and
(Flag262prime-)
(not (Flag262-))
)

)
(:action Flag336Action-0
:parameters ()
:precondition
(and
(Flag299-)
(Flag299prime-)
)
:effect
(and
(Flag336-)
(Flag336prime-)
)

)
(:action Flag336Action-1
:parameters ()
:precondition
(and
(Flag300-)
(Flag300prime-)
)
:effect
(and
(Flag336-)
(Flag336prime-)
)

)
(:action Flag336Action-2
:parameters ()
:precondition
(and
(Flag301-)
(Flag301prime-)
)
:effect
(and
(Flag336-)
(Flag336prime-)
)

)
(:action Flag336Action-3
:parameters ()
:precondition
(and
(Flag302-)
(Flag302prime-)
)
:effect
(and
(Flag336-)
(Flag336prime-)
)

)
(:action Flag336Action-4
:parameters ()
:precondition
(and
(Flag303-)
(Flag303prime-)
)
:effect
(and
(Flag336-)
(Flag336prime-)
)

)
(:action Flag336Action-5
:parameters ()
:precondition
(and
(Flag304-)
(Flag304prime-)
)
:effect
(and
(Flag336-)
(Flag336prime-)
)

)
(:action Flag336Action-6
:parameters ()
:precondition
(and
(Flag305-)
(Flag305prime-)
)
:effect
(and
(Flag336-)
(Flag336prime-)
)

)
(:action Flag336Action-7
:parameters ()
:precondition
(and
(Flag306-)
(Flag306prime-)
)
:effect
(and
(Flag336-)
(Flag336prime-)
)

)
(:action Flag336Action-8
:parameters ()
:precondition
(and
(Flag307-)
(Flag307prime-)
)
:effect
(and
(Flag336-)
(Flag336prime-)
)

)
(:action Flag336Action-9
:parameters ()
:precondition
(and
(Flag308-)
(Flag308prime-)
)
:effect
(and
(Flag336-)
(Flag336prime-)
)

)
(:action Flag336Action-10
:parameters ()
:precondition
(and
(Flag309-)
(Flag309prime-)
)
:effect
(and
(Flag336-)
(Flag336prime-)
)

)
(:action Flag336Action-11
:parameters ()
:precondition
(and
(Flag310-)
(Flag310prime-)
)
:effect
(and
(Flag336-)
(Flag336prime-)
)

)
(:action Flag336Action-12
:parameters ()
:precondition
(and
(Flag311-)
(Flag311prime-)
)
:effect
(and
(Flag336-)
(Flag336prime-)
)

)
(:action Flag336Action-13
:parameters ()
:precondition
(and
(Flag312-)
(Flag312prime-)
)
:effect
(and
(Flag336-)
(Flag336prime-)
)

)
(:action Flag336Action-14
:parameters ()
:precondition
(and
(Flag313-)
(Flag313prime-)
)
:effect
(and
(Flag336-)
(Flag336prime-)
)

)
(:action Flag336Action-15
:parameters ()
:precondition
(and
(Flag314-)
(Flag314prime-)
)
:effect
(and
(Flag336-)
(Flag336prime-)
)

)
(:action Flag336Action-16
:parameters ()
:precondition
(and
(Flag315-)
(Flag315prime-)
)
:effect
(and
(Flag336-)
(Flag336prime-)
)

)
(:action Flag336Action-17
:parameters ()
:precondition
(and
(Flag316-)
(Flag316prime-)
)
:effect
(and
(Flag336-)
(Flag336prime-)
)

)
(:action Flag336Action-18
:parameters ()
:precondition
(and
(Flag317-)
(Flag317prime-)
)
:effect
(and
(Flag336-)
(Flag336prime-)
)

)
(:action Flag336Action-19
:parameters ()
:precondition
(and
(Flag318-)
(Flag318prime-)
)
:effect
(and
(Flag336-)
(Flag336prime-)
)

)
(:action Flag336Action-20
:parameters ()
:precondition
(and
(Flag319-)
(Flag319prime-)
)
:effect
(and
(Flag336-)
(Flag336prime-)
)

)
(:action Flag336Action-21
:parameters ()
:precondition
(and
(Flag320-)
(Flag320prime-)
)
:effect
(and
(Flag336-)
(Flag336prime-)
)

)
(:action Flag336Action-22
:parameters ()
:precondition
(and
(Flag321-)
(Flag321prime-)
)
:effect
(and
(Flag336-)
(Flag336prime-)
)

)
(:action Flag336Action-23
:parameters ()
:precondition
(and
(Flag322-)
(Flag322prime-)
)
:effect
(and
(Flag336-)
(Flag336prime-)
)

)
(:action Flag336Action-24
:parameters ()
:precondition
(and
(Flag323-)
(Flag323prime-)
)
:effect
(and
(Flag336-)
(Flag336prime-)
)

)
(:action Flag336Action-25
:parameters ()
:precondition
(and
(Flag324-)
(Flag324prime-)
)
:effect
(and
(Flag336-)
(Flag336prime-)
)

)
(:action Flag336Action-26
:parameters ()
:precondition
(and
(Flag325-)
(Flag325prime-)
)
:effect
(and
(Flag336-)
(Flag336prime-)
)

)
(:action Flag336Action-27
:parameters ()
:precondition
(and
(Flag326-)
(Flag326prime-)
)
:effect
(and
(Flag336-)
(Flag336prime-)
)

)
(:action Flag336Action-28
:parameters ()
:precondition
(and
(Flag327-)
(Flag327prime-)
)
:effect
(and
(Flag336-)
(Flag336prime-)
)

)
(:action Flag336Action-29
:parameters ()
:precondition
(and
(Flag328-)
(Flag328prime-)
)
:effect
(and
(Flag336-)
(Flag336prime-)
)

)
(:action Flag336Action-30
:parameters ()
:precondition
(and
(Flag329-)
(Flag329prime-)
)
:effect
(and
(Flag336-)
(Flag336prime-)
)

)
(:action Flag336Action-31
:parameters ()
:precondition
(and
(Flag330-)
(Flag330prime-)
)
:effect
(and
(Flag336-)
(Flag336prime-)
)

)
(:action Flag336Action-32
:parameters ()
:precondition
(and
(Flag331-)
(Flag331prime-)
)
:effect
(and
(Flag336-)
(Flag336prime-)
)

)
(:action Flag336Action-33
:parameters ()
:precondition
(and
(Flag332-)
(Flag332prime-)
)
:effect
(and
(Flag336-)
(Flag336prime-)
)

)
(:action Flag336Action-34
:parameters ()
:precondition
(and
(Flag333-)
(Flag333prime-)
)
:effect
(and
(Flag336-)
(Flag336prime-)
)

)
(:action Flag336Action-35
:parameters ()
:precondition
(and
(Flag334-)
(Flag334prime-)
)
:effect
(and
(Flag336-)
(Flag336prime-)
)

)
(:action Flag336Action-36
:parameters ()
:precondition
(and
(Flag335-)
(Flag335prime-)
)
:effect
(and
(Flag336-)
(Flag336prime-)
)

)
(:action Prim336Action
:parameters ()
:precondition
(and
(not (Flag299-))
(Flag299prime-)
(not (Flag300-))
(Flag300prime-)
(not (Flag301-))
(Flag301prime-)
(not (Flag302-))
(Flag302prime-)
(not (Flag303-))
(Flag303prime-)
(not (Flag304-))
(Flag304prime-)
(not (Flag305-))
(Flag305prime-)
(not (Flag306-))
(Flag306prime-)
(not (Flag307-))
(Flag307prime-)
(not (Flag308-))
(Flag308prime-)
(not (Flag309-))
(Flag309prime-)
(not (Flag310-))
(Flag310prime-)
(not (Flag311-))
(Flag311prime-)
(not (Flag312-))
(Flag312prime-)
(not (Flag313-))
(Flag313prime-)
(not (Flag314-))
(Flag314prime-)
(not (Flag315-))
(Flag315prime-)
(not (Flag316-))
(Flag316prime-)
(not (Flag317-))
(Flag317prime-)
(not (Flag318-))
(Flag318prime-)
(not (Flag319-))
(Flag319prime-)
(not (Flag320-))
(Flag320prime-)
(not (Flag321-))
(Flag321prime-)
(not (Flag322-))
(Flag322prime-)
(not (Flag323-))
(Flag323prime-)
(not (Flag324-))
(Flag324prime-)
(not (Flag325-))
(Flag325prime-)
(not (Flag326-))
(Flag326prime-)
(not (Flag327-))
(Flag327prime-)
(not (Flag328-))
(Flag328prime-)
(not (Flag329-))
(Flag329prime-)
(not (Flag330-))
(Flag330prime-)
(not (Flag331-))
(Flag331prime-)
(not (Flag332-))
(Flag332prime-)
(not (Flag333-))
(Flag333prime-)
(not (Flag334-))
(Flag334prime-)
(not (Flag335-))
(Flag335prime-)
)
:effect
(and
(Flag336prime-)
(not (Flag336-))
)

)
(:action Flag355Action-0
:parameters ()
:precondition
(and
(Flag299-)
(Flag299prime-)
)
:effect
(and
(Flag355-)
(Flag355prime-)
)

)
(:action Flag355Action-1
:parameters ()
:precondition
(and
(Flag300-)
(Flag300prime-)
)
:effect
(and
(Flag355-)
(Flag355prime-)
)

)
(:action Flag355Action-2
:parameters ()
:precondition
(and
(Flag337-)
(Flag337prime-)
)
:effect
(and
(Flag355-)
(Flag355prime-)
)

)
(:action Flag355Action-3
:parameters ()
:precondition
(and
(Flag308-)
(Flag308prime-)
)
:effect
(and
(Flag355-)
(Flag355prime-)
)

)
(:action Flag355Action-4
:parameters ()
:precondition
(and
(Flag302-)
(Flag302prime-)
)
:effect
(and
(Flag355-)
(Flag355prime-)
)

)
(:action Flag355Action-5
:parameters ()
:precondition
(and
(Flag324-)
(Flag324prime-)
)
:effect
(and
(Flag355-)
(Flag355prime-)
)

)
(:action Flag355Action-6
:parameters ()
:precondition
(and
(Flag338-)
(Flag338prime-)
)
:effect
(and
(Flag355-)
(Flag355prime-)
)

)
(:action Flag355Action-7
:parameters ()
:precondition
(and
(Flag317-)
(Flag317prime-)
)
:effect
(and
(Flag355-)
(Flag355prime-)
)

)
(:action Flag355Action-8
:parameters ()
:precondition
(and
(Flag339-)
(Flag339prime-)
)
:effect
(and
(Flag355-)
(Flag355prime-)
)

)
(:action Flag355Action-9
:parameters ()
:precondition
(and
(Flag303-)
(Flag303prime-)
)
:effect
(and
(Flag355-)
(Flag355prime-)
)

)
(:action Flag355Action-10
:parameters ()
:precondition
(and
(Flag314-)
(Flag314prime-)
)
:effect
(and
(Flag355-)
(Flag355prime-)
)

)
(:action Flag355Action-11
:parameters ()
:precondition
(and
(Flag305-)
(Flag305prime-)
)
:effect
(and
(Flag355-)
(Flag355prime-)
)

)
(:action Flag355Action-12
:parameters ()
:precondition
(and
(Flag306-)
(Flag306prime-)
)
:effect
(and
(Flag355-)
(Flag355prime-)
)

)
(:action Flag355Action-13
:parameters ()
:precondition
(and
(Flag340-)
(Flag340prime-)
)
:effect
(and
(Flag355-)
(Flag355prime-)
)

)
(:action Flag355Action-14
:parameters ()
:precondition
(and
(Flag341-)
(Flag341prime-)
)
:effect
(and
(Flag355-)
(Flag355prime-)
)

)
(:action Flag355Action-15
:parameters ()
:precondition
(and
(Flag307-)
(Flag307prime-)
)
:effect
(and
(Flag355-)
(Flag355prime-)
)

)
(:action Flag355Action-16
:parameters ()
:precondition
(and
(Flag309-)
(Flag309prime-)
)
:effect
(and
(Flag355-)
(Flag355prime-)
)

)
(:action Flag355Action-17
:parameters ()
:precondition
(and
(Flag342-)
(Flag342prime-)
)
:effect
(and
(Flag355-)
(Flag355prime-)
)

)
(:action Flag355Action-18
:parameters ()
:precondition
(and
(Flag311-)
(Flag311prime-)
)
:effect
(and
(Flag355-)
(Flag355prime-)
)

)
(:action Flag355Action-19
:parameters ()
:precondition
(and
(Flag343-)
(Flag343prime-)
)
:effect
(and
(Flag355-)
(Flag355prime-)
)

)
(:action Flag355Action-20
:parameters ()
:precondition
(and
(Flag344-)
(Flag344prime-)
)
:effect
(and
(Flag355-)
(Flag355prime-)
)

)
(:action Flag355Action-21
:parameters ()
:precondition
(and
(Flag312-)
(Flag312prime-)
)
:effect
(and
(Flag355-)
(Flag355prime-)
)

)
(:action Flag355Action-22
:parameters ()
:precondition
(and
(Flag313-)
(Flag313prime-)
)
:effect
(and
(Flag355-)
(Flag355prime-)
)

)
(:action Flag355Action-23
:parameters ()
:precondition
(and
(Flag315-)
(Flag315prime-)
)
:effect
(and
(Flag355-)
(Flag355prime-)
)

)
(:action Flag355Action-24
:parameters ()
:precondition
(and
(Flag345-)
(Flag345prime-)
)
:effect
(and
(Flag355-)
(Flag355prime-)
)

)
(:action Flag355Action-25
:parameters ()
:precondition
(and
(Flag316-)
(Flag316prime-)
)
:effect
(and
(Flag355-)
(Flag355prime-)
)

)
(:action Flag355Action-26
:parameters ()
:precondition
(and
(Flag346-)
(Flag346prime-)
)
:effect
(and
(Flag355-)
(Flag355prime-)
)

)
(:action Flag355Action-27
:parameters ()
:precondition
(and
(Flag318-)
(Flag318prime-)
)
:effect
(and
(Flag355-)
(Flag355prime-)
)

)
(:action Flag355Action-28
:parameters ()
:precondition
(and
(Flag319-)
(Flag319prime-)
)
:effect
(and
(Flag355-)
(Flag355prime-)
)

)
(:action Flag355Action-29
:parameters ()
:precondition
(and
(Flag347-)
(Flag347prime-)
)
:effect
(and
(Flag355-)
(Flag355prime-)
)

)
(:action Flag355Action-30
:parameters ()
:precondition
(and
(Flag320-)
(Flag320prime-)
)
:effect
(and
(Flag355-)
(Flag355prime-)
)

)
(:action Flag355Action-31
:parameters ()
:precondition
(and
(Flag348-)
(Flag348prime-)
)
:effect
(and
(Flag355-)
(Flag355prime-)
)

)
(:action Flag355Action-32
:parameters ()
:precondition
(and
(Flag322-)
(Flag322prime-)
)
:effect
(and
(Flag355-)
(Flag355prime-)
)

)
(:action Flag355Action-33
:parameters ()
:precondition
(and
(Flag323-)
(Flag323prime-)
)
:effect
(and
(Flag355-)
(Flag355prime-)
)

)
(:action Flag355Action-34
:parameters ()
:precondition
(and
(Flag321-)
(Flag321prime-)
)
:effect
(and
(Flag355-)
(Flag355prime-)
)

)
(:action Flag355Action-35
:parameters ()
:precondition
(and
(Flag349-)
(Flag349prime-)
)
:effect
(and
(Flag355-)
(Flag355prime-)
)

)
(:action Flag355Action-36
:parameters ()
:precondition
(and
(Flag350-)
(Flag350prime-)
)
:effect
(and
(Flag355-)
(Flag355prime-)
)

)
(:action Flag355Action-37
:parameters ()
:precondition
(and
(Flag325-)
(Flag325prime-)
)
:effect
(and
(Flag355-)
(Flag355prime-)
)

)
(:action Flag355Action-38
:parameters ()
:precondition
(and
(Flag326-)
(Flag326prime-)
)
:effect
(and
(Flag355-)
(Flag355prime-)
)

)
(:action Flag355Action-39
:parameters ()
:precondition
(and
(Flag327-)
(Flag327prime-)
)
:effect
(and
(Flag355-)
(Flag355prime-)
)

)
(:action Flag355Action-40
:parameters ()
:precondition
(and
(Flag328-)
(Flag328prime-)
)
:effect
(and
(Flag355-)
(Flag355prime-)
)

)
(:action Flag355Action-41
:parameters ()
:precondition
(and
(Flag329-)
(Flag329prime-)
)
:effect
(and
(Flag355-)
(Flag355prime-)
)

)
(:action Flag355Action-42
:parameters ()
:precondition
(and
(Flag330-)
(Flag330prime-)
)
:effect
(and
(Flag355-)
(Flag355prime-)
)

)
(:action Flag355Action-43
:parameters ()
:precondition
(and
(Flag351-)
(Flag351prime-)
)
:effect
(and
(Flag355-)
(Flag355prime-)
)

)
(:action Flag355Action-44
:parameters ()
:precondition
(and
(Flag331-)
(Flag331prime-)
)
:effect
(and
(Flag355-)
(Flag355prime-)
)

)
(:action Flag355Action-45
:parameters ()
:precondition
(and
(Flag352-)
(Flag352prime-)
)
:effect
(and
(Flag355-)
(Flag355prime-)
)

)
(:action Flag355Action-46
:parameters ()
:precondition
(and
(Flag353-)
(Flag353prime-)
)
:effect
(and
(Flag355-)
(Flag355prime-)
)

)
(:action Flag355Action-47
:parameters ()
:precondition
(and
(Flag332-)
(Flag332prime-)
)
:effect
(and
(Flag355-)
(Flag355prime-)
)

)
(:action Flag355Action-48
:parameters ()
:precondition
(and
(Flag333-)
(Flag333prime-)
)
:effect
(and
(Flag355-)
(Flag355prime-)
)

)
(:action Flag355Action-49
:parameters ()
:precondition
(and
(Flag334-)
(Flag334prime-)
)
:effect
(and
(Flag355-)
(Flag355prime-)
)

)
(:action Flag355Action-50
:parameters ()
:precondition
(and
(Flag354-)
(Flag354prime-)
)
:effect
(and
(Flag355-)
(Flag355prime-)
)

)
(:action Flag355Action-51
:parameters ()
:precondition
(and
(Flag335-)
(Flag335prime-)
)
:effect
(and
(Flag355-)
(Flag355prime-)
)

)
(:action Prim355Action
:parameters ()
:precondition
(and
(not (Flag299-))
(Flag299prime-)
(not (Flag300-))
(Flag300prime-)
(not (Flag337-))
(Flag337prime-)
(not (Flag308-))
(Flag308prime-)
(not (Flag302-))
(Flag302prime-)
(not (Flag324-))
(Flag324prime-)
(not (Flag338-))
(Flag338prime-)
(not (Flag317-))
(Flag317prime-)
(not (Flag339-))
(Flag339prime-)
(not (Flag303-))
(Flag303prime-)
(not (Flag314-))
(Flag314prime-)
(not (Flag305-))
(Flag305prime-)
(not (Flag306-))
(Flag306prime-)
(not (Flag340-))
(Flag340prime-)
(not (Flag341-))
(Flag341prime-)
(not (Flag307-))
(Flag307prime-)
(not (Flag309-))
(Flag309prime-)
(not (Flag342-))
(Flag342prime-)
(not (Flag311-))
(Flag311prime-)
(not (Flag343-))
(Flag343prime-)
(not (Flag344-))
(Flag344prime-)
(not (Flag312-))
(Flag312prime-)
(not (Flag313-))
(Flag313prime-)
(not (Flag315-))
(Flag315prime-)
(not (Flag345-))
(Flag345prime-)
(not (Flag316-))
(Flag316prime-)
(not (Flag346-))
(Flag346prime-)
(not (Flag318-))
(Flag318prime-)
(not (Flag319-))
(Flag319prime-)
(not (Flag347-))
(Flag347prime-)
(not (Flag320-))
(Flag320prime-)
(not (Flag348-))
(Flag348prime-)
(not (Flag322-))
(Flag322prime-)
(not (Flag323-))
(Flag323prime-)
(not (Flag321-))
(Flag321prime-)
(not (Flag349-))
(Flag349prime-)
(not (Flag350-))
(Flag350prime-)
(not (Flag325-))
(Flag325prime-)
(not (Flag326-))
(Flag326prime-)
(not (Flag327-))
(Flag327prime-)
(not (Flag328-))
(Flag328prime-)
(not (Flag329-))
(Flag329prime-)
(not (Flag330-))
(Flag330prime-)
(not (Flag351-))
(Flag351prime-)
(not (Flag331-))
(Flag331prime-)
(not (Flag352-))
(Flag352prime-)
(not (Flag353-))
(Flag353prime-)
(not (Flag332-))
(Flag332prime-)
(not (Flag333-))
(Flag333prime-)
(not (Flag334-))
(Flag334prime-)
(not (Flag354-))
(Flag354prime-)
(not (Flag335-))
(Flag335prime-)
)
:effect
(and
(Flag355prime-)
(not (Flag355-))
)

)
(:action Flag374Action-0
:parameters ()
:precondition
(and
(Flag299-)
(Flag299prime-)
)
:effect
(and
(Flag374-)
(Flag374prime-)
)

)
(:action Flag374Action-1
:parameters ()
:precondition
(and
(Flag302-)
(Flag302prime-)
)
:effect
(and
(Flag374-)
(Flag374prime-)
)

)
(:action Flag374Action-2
:parameters ()
:precondition
(and
(Flag341-)
(Flag341prime-)
)
:effect
(and
(Flag374-)
(Flag374prime-)
)

)
(:action Flag374Action-3
:parameters ()
:precondition
(and
(Flag337-)
(Flag337prime-)
)
:effect
(and
(Flag374-)
(Flag374prime-)
)

)
(:action Flag374Action-4
:parameters ()
:precondition
(and
(Flag356-)
(Flag356prime-)
)
:effect
(and
(Flag374-)
(Flag374prime-)
)

)
(:action Flag374Action-5
:parameters ()
:precondition
(and
(Flag357-)
(Flag357prime-)
)
:effect
(and
(Flag374-)
(Flag374prime-)
)

)
(:action Flag374Action-6
:parameters ()
:precondition
(and
(Flag358-)
(Flag358prime-)
)
:effect
(and
(Flag374-)
(Flag374prime-)
)

)
(:action Flag374Action-7
:parameters ()
:precondition
(and
(Flag359-)
(Flag359prime-)
)
:effect
(and
(Flag374-)
(Flag374prime-)
)

)
(:action Flag374Action-8
:parameters ()
:precondition
(and
(Flag360-)
(Flag360prime-)
)
:effect
(and
(Flag374-)
(Flag374prime-)
)

)
(:action Flag374Action-9
:parameters ()
:precondition
(and
(Flag324-)
(Flag324prime-)
)
:effect
(and
(Flag374-)
(Flag374prime-)
)

)
(:action Flag374Action-10
:parameters ()
:precondition
(and
(Flag338-)
(Flag338prime-)
)
:effect
(and
(Flag374-)
(Flag374prime-)
)

)
(:action Flag374Action-11
:parameters ()
:precondition
(and
(Flag339-)
(Flag339prime-)
)
:effect
(and
(Flag374-)
(Flag374prime-)
)

)
(:action Flag374Action-12
:parameters ()
:precondition
(and
(Flag308-)
(Flag308prime-)
)
:effect
(and
(Flag374-)
(Flag374prime-)
)

)
(:action Flag374Action-13
:parameters ()
:precondition
(and
(Flag303-)
(Flag303prime-)
)
:effect
(and
(Flag374-)
(Flag374prime-)
)

)
(:action Flag374Action-14
:parameters ()
:precondition
(and
(Flag314-)
(Flag314prime-)
)
:effect
(and
(Flag374-)
(Flag374prime-)
)

)
(:action Flag374Action-15
:parameters ()
:precondition
(and
(Flag305-)
(Flag305prime-)
)
:effect
(and
(Flag374-)
(Flag374prime-)
)

)
(:action Flag374Action-16
:parameters ()
:precondition
(and
(Flag361-)
(Flag361prime-)
)
:effect
(and
(Flag374-)
(Flag374prime-)
)

)
(:action Flag374Action-17
:parameters ()
:precondition
(and
(Flag306-)
(Flag306prime-)
)
:effect
(and
(Flag374-)
(Flag374prime-)
)

)
(:action Flag374Action-18
:parameters ()
:precondition
(and
(Flag340-)
(Flag340prime-)
)
:effect
(and
(Flag374-)
(Flag374prime-)
)

)
(:action Flag374Action-19
:parameters ()
:precondition
(and
(Flag362-)
(Flag362prime-)
)
:effect
(and
(Flag374-)
(Flag374prime-)
)

)
(:action Flag374Action-20
:parameters ()
:precondition
(and
(Flag307-)
(Flag307prime-)
)
:effect
(and
(Flag374-)
(Flag374prime-)
)

)
(:action Flag374Action-21
:parameters ()
:precondition
(and
(Flag363-)
(Flag363prime-)
)
:effect
(and
(Flag374-)
(Flag374prime-)
)

)
(:action Flag374Action-22
:parameters ()
:precondition
(and
(Flag309-)
(Flag309prime-)
)
:effect
(and
(Flag374-)
(Flag374prime-)
)

)
(:action Flag374Action-23
:parameters ()
:precondition
(and
(Flag342-)
(Flag342prime-)
)
:effect
(and
(Flag374-)
(Flag374prime-)
)

)
(:action Flag374Action-24
:parameters ()
:precondition
(and
(Flag311-)
(Flag311prime-)
)
:effect
(and
(Flag374-)
(Flag374prime-)
)

)
(:action Flag374Action-25
:parameters ()
:precondition
(and
(Flag364-)
(Flag364prime-)
)
:effect
(and
(Flag374-)
(Flag374prime-)
)

)
(:action Flag374Action-26
:parameters ()
:precondition
(and
(Flag343-)
(Flag343prime-)
)
:effect
(and
(Flag374-)
(Flag374prime-)
)

)
(:action Flag374Action-27
:parameters ()
:precondition
(and
(Flag344-)
(Flag344prime-)
)
:effect
(and
(Flag374-)
(Flag374prime-)
)

)
(:action Flag374Action-28
:parameters ()
:precondition
(and
(Flag312-)
(Flag312prime-)
)
:effect
(and
(Flag374-)
(Flag374prime-)
)

)
(:action Flag374Action-29
:parameters ()
:precondition
(and
(Flag313-)
(Flag313prime-)
)
:effect
(and
(Flag374-)
(Flag374prime-)
)

)
(:action Flag374Action-30
:parameters ()
:precondition
(and
(Flag365-)
(Flag365prime-)
)
:effect
(and
(Flag374-)
(Flag374prime-)
)

)
(:action Flag374Action-31
:parameters ()
:precondition
(and
(Flag366-)
(Flag366prime-)
)
:effect
(and
(Flag374-)
(Flag374prime-)
)

)
(:action Flag374Action-32
:parameters ()
:precondition
(and
(Flag367-)
(Flag367prime-)
)
:effect
(and
(Flag374-)
(Flag374prime-)
)

)
(:action Flag374Action-33
:parameters ()
:precondition
(and
(Flag316-)
(Flag316prime-)
)
:effect
(and
(Flag374-)
(Flag374prime-)
)

)
(:action Flag374Action-34
:parameters ()
:precondition
(and
(Flag300-)
(Flag300prime-)
)
:effect
(and
(Flag374-)
(Flag374prime-)
)

)
(:action Flag374Action-35
:parameters ()
:precondition
(and
(Flag318-)
(Flag318prime-)
)
:effect
(and
(Flag374-)
(Flag374prime-)
)

)
(:action Flag374Action-36
:parameters ()
:precondition
(and
(Flag319-)
(Flag319prime-)
)
:effect
(and
(Flag374-)
(Flag374prime-)
)

)
(:action Flag374Action-37
:parameters ()
:precondition
(and
(Flag347-)
(Flag347prime-)
)
:effect
(and
(Flag374-)
(Flag374prime-)
)

)
(:action Flag374Action-38
:parameters ()
:precondition
(and
(Flag320-)
(Flag320prime-)
)
:effect
(and
(Flag374-)
(Flag374prime-)
)

)
(:action Flag374Action-39
:parameters ()
:precondition
(and
(Flag348-)
(Flag348prime-)
)
:effect
(and
(Flag374-)
(Flag374prime-)
)

)
(:action Flag374Action-40
:parameters ()
:precondition
(and
(Flag322-)
(Flag322prime-)
)
:effect
(and
(Flag374-)
(Flag374prime-)
)

)
(:action Flag374Action-41
:parameters ()
:precondition
(and
(Flag330-)
(Flag330prime-)
)
:effect
(and
(Flag374-)
(Flag374prime-)
)

)
(:action Flag374Action-42
:parameters ()
:precondition
(and
(Flag323-)
(Flag323prime-)
)
:effect
(and
(Flag374-)
(Flag374prime-)
)

)
(:action Flag374Action-43
:parameters ()
:precondition
(and
(Flag321-)
(Flag321prime-)
)
:effect
(and
(Flag374-)
(Flag374prime-)
)

)
(:action Flag374Action-44
:parameters ()
:precondition
(and
(Flag349-)
(Flag349prime-)
)
:effect
(and
(Flag374-)
(Flag374prime-)
)

)
(:action Flag374Action-45
:parameters ()
:precondition
(and
(Flag368-)
(Flag368prime-)
)
:effect
(and
(Flag374-)
(Flag374prime-)
)

)
(:action Flag374Action-46
:parameters ()
:precondition
(and
(Flag325-)
(Flag325prime-)
)
:effect
(and
(Flag374-)
(Flag374prime-)
)

)
(:action Flag374Action-47
:parameters ()
:precondition
(and
(Flag326-)
(Flag326prime-)
)
:effect
(and
(Flag374-)
(Flag374prime-)
)

)
(:action Flag374Action-48
:parameters ()
:precondition
(and
(Flag327-)
(Flag327prime-)
)
:effect
(and
(Flag374-)
(Flag374prime-)
)

)
(:action Flag374Action-49
:parameters ()
:precondition
(and
(Flag369-)
(Flag369prime-)
)
:effect
(and
(Flag374-)
(Flag374prime-)
)

)
(:action Flag374Action-50
:parameters ()
:precondition
(and
(Flag329-)
(Flag329prime-)
)
:effect
(and
(Flag374-)
(Flag374prime-)
)

)
(:action Flag374Action-51
:parameters ()
:precondition
(and
(Flag370-)
(Flag370prime-)
)
:effect
(and
(Flag374-)
(Flag374prime-)
)

)
(:action Flag374Action-52
:parameters ()
:precondition
(and
(Flag371-)
(Flag371prime-)
)
:effect
(and
(Flag374-)
(Flag374prime-)
)

)
(:action Flag374Action-53
:parameters ()
:precondition
(and
(Flag351-)
(Flag351prime-)
)
:effect
(and
(Flag374-)
(Flag374prime-)
)

)
(:action Flag374Action-54
:parameters ()
:precondition
(and
(Flag372-)
(Flag372prime-)
)
:effect
(and
(Flag374-)
(Flag374prime-)
)

)
(:action Flag374Action-55
:parameters ()
:precondition
(and
(Flag373-)
(Flag373prime-)
)
:effect
(and
(Flag374-)
(Flag374prime-)
)

)
(:action Flag374Action-56
:parameters ()
:precondition
(and
(Flag352-)
(Flag352prime-)
)
:effect
(and
(Flag374-)
(Flag374prime-)
)

)
(:action Flag374Action-57
:parameters ()
:precondition
(and
(Flag315-)
(Flag315prime-)
)
:effect
(and
(Flag374-)
(Flag374prime-)
)

)
(:action Flag374Action-58
:parameters ()
:precondition
(and
(Flag331-)
(Flag331prime-)
)
:effect
(and
(Flag374-)
(Flag374prime-)
)

)
(:action Flag374Action-59
:parameters ()
:precondition
(and
(Flag353-)
(Flag353prime-)
)
:effect
(and
(Flag374-)
(Flag374prime-)
)

)
(:action Flag374Action-60
:parameters ()
:precondition
(and
(Flag332-)
(Flag332prime-)
)
:effect
(and
(Flag374-)
(Flag374prime-)
)

)
(:action Flag374Action-61
:parameters ()
:precondition
(and
(Flag333-)
(Flag333prime-)
)
:effect
(and
(Flag374-)
(Flag374prime-)
)

)
(:action Flag374Action-62
:parameters ()
:precondition
(and
(Flag334-)
(Flag334prime-)
)
:effect
(and
(Flag374-)
(Flag374prime-)
)

)
(:action Flag374Action-63
:parameters ()
:precondition
(and
(Flag354-)
(Flag354prime-)
)
:effect
(and
(Flag374-)
(Flag374prime-)
)

)
(:action Flag374Action-64
:parameters ()
:precondition
(and
(Flag328-)
(Flag328prime-)
)
:effect
(and
(Flag374-)
(Flag374prime-)
)

)
(:action Flag374Action-65
:parameters ()
:precondition
(and
(Flag335-)
(Flag335prime-)
)
:effect
(and
(Flag374-)
(Flag374prime-)
)

)
(:action Prim374Action
:parameters ()
:precondition
(and
(not (Flag299-))
(Flag299prime-)
(not (Flag302-))
(Flag302prime-)
(not (Flag341-))
(Flag341prime-)
(not (Flag337-))
(Flag337prime-)
(not (Flag356-))
(Flag356prime-)
(not (Flag357-))
(Flag357prime-)
(not (Flag358-))
(Flag358prime-)
(not (Flag359-))
(Flag359prime-)
(not (Flag360-))
(Flag360prime-)
(not (Flag324-))
(Flag324prime-)
(not (Flag338-))
(Flag338prime-)
(not (Flag339-))
(Flag339prime-)
(not (Flag308-))
(Flag308prime-)
(not (Flag303-))
(Flag303prime-)
(not (Flag314-))
(Flag314prime-)
(not (Flag305-))
(Flag305prime-)
(not (Flag361-))
(Flag361prime-)
(not (Flag306-))
(Flag306prime-)
(not (Flag340-))
(Flag340prime-)
(not (Flag362-))
(Flag362prime-)
(not (Flag307-))
(Flag307prime-)
(not (Flag363-))
(Flag363prime-)
(not (Flag309-))
(Flag309prime-)
(not (Flag342-))
(Flag342prime-)
(not (Flag311-))
(Flag311prime-)
(not (Flag364-))
(Flag364prime-)
(not (Flag343-))
(Flag343prime-)
(not (Flag344-))
(Flag344prime-)
(not (Flag312-))
(Flag312prime-)
(not (Flag313-))
(Flag313prime-)
(not (Flag365-))
(Flag365prime-)
(not (Flag366-))
(Flag366prime-)
(not (Flag367-))
(Flag367prime-)
(not (Flag316-))
(Flag316prime-)
(not (Flag300-))
(Flag300prime-)
(not (Flag318-))
(Flag318prime-)
(not (Flag319-))
(Flag319prime-)
(not (Flag347-))
(Flag347prime-)
(not (Flag320-))
(Flag320prime-)
(not (Flag348-))
(Flag348prime-)
(not (Flag322-))
(Flag322prime-)
(not (Flag330-))
(Flag330prime-)
(not (Flag323-))
(Flag323prime-)
(not (Flag321-))
(Flag321prime-)
(not (Flag349-))
(Flag349prime-)
(not (Flag368-))
(Flag368prime-)
(not (Flag325-))
(Flag325prime-)
(not (Flag326-))
(Flag326prime-)
(not (Flag327-))
(Flag327prime-)
(not (Flag369-))
(Flag369prime-)
(not (Flag329-))
(Flag329prime-)
(not (Flag370-))
(Flag370prime-)
(not (Flag371-))
(Flag371prime-)
(not (Flag351-))
(Flag351prime-)
(not (Flag372-))
(Flag372prime-)
(not (Flag373-))
(Flag373prime-)
(not (Flag352-))
(Flag352prime-)
(not (Flag315-))
(Flag315prime-)
(not (Flag331-))
(Flag331prime-)
(not (Flag353-))
(Flag353prime-)
(not (Flag332-))
(Flag332prime-)
(not (Flag333-))
(Flag333prime-)
(not (Flag334-))
(Flag334prime-)
(not (Flag354-))
(Flag354prime-)
(not (Flag328-))
(Flag328prime-)
(not (Flag335-))
(Flag335prime-)
)
:effect
(and
(Flag374prime-)
(not (Flag374-))
)

)
(:action Flag391Action-0
:parameters ()
:precondition
(and
(Flag299-)
(Flag299prime-)
)
:effect
(and
(Flag391-)
(Flag391prime-)
)

)
(:action Flag391Action-1
:parameters ()
:precondition
(and
(Flag300-)
(Flag300prime-)
)
:effect
(and
(Flag391-)
(Flag391prime-)
)

)
(:action Flag391Action-2
:parameters ()
:precondition
(and
(Flag375-)
(Flag375prime-)
)
:effect
(and
(Flag391-)
(Flag391prime-)
)

)
(:action Flag391Action-3
:parameters ()
:precondition
(and
(Flag341-)
(Flag341prime-)
)
:effect
(and
(Flag391-)
(Flag391prime-)
)

)
(:action Flag391Action-4
:parameters ()
:precondition
(and
(Flag376-)
(Flag376prime-)
)
:effect
(and
(Flag391-)
(Flag391prime-)
)

)
(:action Flag391Action-5
:parameters ()
:precondition
(and
(Flag337-)
(Flag337prime-)
)
:effect
(and
(Flag391-)
(Flag391prime-)
)

)
(:action Flag391Action-6
:parameters ()
:precondition
(and
(Flag356-)
(Flag356prime-)
)
:effect
(and
(Flag391-)
(Flag391prime-)
)

)
(:action Flag391Action-7
:parameters ()
:precondition
(and
(Flag359-)
(Flag359prime-)
)
:effect
(and
(Flag391-)
(Flag391prime-)
)

)
(:action Flag391Action-8
:parameters ()
:precondition
(and
(Flag357-)
(Flag357prime-)
)
:effect
(and
(Flag391-)
(Flag391prime-)
)

)
(:action Flag391Action-9
:parameters ()
:precondition
(and
(Flag302-)
(Flag302prime-)
)
:effect
(and
(Flag391-)
(Flag391prime-)
)

)
(:action Flag391Action-10
:parameters ()
:precondition
(and
(Flag371-)
(Flag371prime-)
)
:effect
(and
(Flag391-)
(Flag391prime-)
)

)
(:action Flag391Action-11
:parameters ()
:precondition
(and
(Flag377-)
(Flag377prime-)
)
:effect
(and
(Flag391-)
(Flag391prime-)
)

)
(:action Flag391Action-12
:parameters ()
:precondition
(and
(Flag338-)
(Flag338prime-)
)
:effect
(and
(Flag391-)
(Flag391prime-)
)

)
(:action Flag391Action-13
:parameters ()
:precondition
(and
(Flag378-)
(Flag378prime-)
)
:effect
(and
(Flag391-)
(Flag391prime-)
)

)
(:action Flag391Action-14
:parameters ()
:precondition
(and
(Flag339-)
(Flag339prime-)
)
:effect
(and
(Flag391-)
(Flag391prime-)
)

)
(:action Flag391Action-15
:parameters ()
:precondition
(and
(Flag308-)
(Flag308prime-)
)
:effect
(and
(Flag391-)
(Flag391prime-)
)

)
(:action Flag391Action-16
:parameters ()
:precondition
(and
(Flag379-)
(Flag379prime-)
)
:effect
(and
(Flag391-)
(Flag391prime-)
)

)
(:action Flag391Action-17
:parameters ()
:precondition
(and
(Flag303-)
(Flag303prime-)
)
:effect
(and
(Flag391-)
(Flag391prime-)
)

)
(:action Flag391Action-18
:parameters ()
:precondition
(and
(Flag314-)
(Flag314prime-)
)
:effect
(and
(Flag391-)
(Flag391prime-)
)

)
(:action Flag391Action-19
:parameters ()
:precondition
(and
(Flag305-)
(Flag305prime-)
)
:effect
(and
(Flag391-)
(Flag391prime-)
)

)
(:action Flag391Action-20
:parameters ()
:precondition
(and
(Flag361-)
(Flag361prime-)
)
:effect
(and
(Flag391-)
(Flag391prime-)
)

)
(:action Flag391Action-21
:parameters ()
:precondition
(and
(Flag306-)
(Flag306prime-)
)
:effect
(and
(Flag391-)
(Flag391prime-)
)

)
(:action Flag391Action-22
:parameters ()
:precondition
(and
(Flag340-)
(Flag340prime-)
)
:effect
(and
(Flag391-)
(Flag391prime-)
)

)
(:action Flag391Action-23
:parameters ()
:precondition
(and
(Flag369-)
(Flag369prime-)
)
:effect
(and
(Flag391-)
(Flag391prime-)
)

)
(:action Flag391Action-24
:parameters ()
:precondition
(and
(Flag307-)
(Flag307prime-)
)
:effect
(and
(Flag391-)
(Flag391prime-)
)

)
(:action Flag391Action-25
:parameters ()
:precondition
(and
(Flag363-)
(Flag363prime-)
)
:effect
(and
(Flag391-)
(Flag391prime-)
)

)
(:action Flag391Action-26
:parameters ()
:precondition
(and
(Flag309-)
(Flag309prime-)
)
:effect
(and
(Flag391-)
(Flag391prime-)
)

)
(:action Flag391Action-27
:parameters ()
:precondition
(and
(Flag332-)
(Flag332prime-)
)
:effect
(and
(Flag391-)
(Flag391prime-)
)

)
(:action Flag391Action-28
:parameters ()
:precondition
(and
(Flag342-)
(Flag342prime-)
)
:effect
(and
(Flag391-)
(Flag391prime-)
)

)
(:action Flag391Action-29
:parameters ()
:precondition
(and
(Flag311-)
(Flag311prime-)
)
:effect
(and
(Flag391-)
(Flag391prime-)
)

)
(:action Flag391Action-30
:parameters ()
:precondition
(and
(Flag364-)
(Flag364prime-)
)
:effect
(and
(Flag391-)
(Flag391prime-)
)

)
(:action Flag391Action-31
:parameters ()
:precondition
(and
(Flag343-)
(Flag343prime-)
)
:effect
(and
(Flag391-)
(Flag391prime-)
)

)
(:action Flag391Action-32
:parameters ()
:precondition
(and
(Flag380-)
(Flag380prime-)
)
:effect
(and
(Flag391-)
(Flag391prime-)
)

)
(:action Flag391Action-33
:parameters ()
:precondition
(and
(Flag344-)
(Flag344prime-)
)
:effect
(and
(Flag391-)
(Flag391prime-)
)

)
(:action Flag391Action-34
:parameters ()
:precondition
(and
(Flag312-)
(Flag312prime-)
)
:effect
(and
(Flag391-)
(Flag391prime-)
)

)
(:action Flag391Action-35
:parameters ()
:precondition
(and
(Flag313-)
(Flag313prime-)
)
:effect
(and
(Flag391-)
(Flag391prime-)
)

)
(:action Flag391Action-36
:parameters ()
:precondition
(and
(Flag365-)
(Flag365prime-)
)
:effect
(and
(Flag391-)
(Flag391prime-)
)

)
(:action Flag391Action-37
:parameters ()
:precondition
(and
(Flag366-)
(Flag366prime-)
)
:effect
(and
(Flag391-)
(Flag391prime-)
)

)
(:action Flag391Action-38
:parameters ()
:precondition
(and
(Flag381-)
(Flag381prime-)
)
:effect
(and
(Flag391-)
(Flag391prime-)
)

)
(:action Flag391Action-39
:parameters ()
:precondition
(and
(Flag316-)
(Flag316prime-)
)
:effect
(and
(Flag391-)
(Flag391prime-)
)

)
(:action Flag391Action-40
:parameters ()
:precondition
(and
(Flag382-)
(Flag382prime-)
)
:effect
(and
(Flag391-)
(Flag391prime-)
)

)
(:action Flag391Action-41
:parameters ()
:precondition
(and
(Flag318-)
(Flag318prime-)
)
:effect
(and
(Flag391-)
(Flag391prime-)
)

)
(:action Flag391Action-42
:parameters ()
:precondition
(and
(Flag319-)
(Flag319prime-)
)
:effect
(and
(Flag391-)
(Flag391prime-)
)

)
(:action Flag391Action-43
:parameters ()
:precondition
(and
(Flag347-)
(Flag347prime-)
)
:effect
(and
(Flag391-)
(Flag391prime-)
)

)
(:action Flag391Action-44
:parameters ()
:precondition
(and
(Flag320-)
(Flag320prime-)
)
:effect
(and
(Flag391-)
(Flag391prime-)
)

)
(:action Flag391Action-45
:parameters ()
:precondition
(and
(Flag383-)
(Flag383prime-)
)
:effect
(and
(Flag391-)
(Flag391prime-)
)

)
(:action Flag391Action-46
:parameters ()
:precondition
(and
(Flag348-)
(Flag348prime-)
)
:effect
(and
(Flag391-)
(Flag391prime-)
)

)
(:action Flag391Action-47
:parameters ()
:precondition
(and
(Flag322-)
(Flag322prime-)
)
:effect
(and
(Flag391-)
(Flag391prime-)
)

)
(:action Flag391Action-48
:parameters ()
:precondition
(and
(Flag384-)
(Flag384prime-)
)
:effect
(and
(Flag391-)
(Flag391prime-)
)

)
(:action Flag391Action-49
:parameters ()
:precondition
(and
(Flag330-)
(Flag330prime-)
)
:effect
(and
(Flag391-)
(Flag391prime-)
)

)
(:action Flag391Action-50
:parameters ()
:precondition
(and
(Flag323-)
(Flag323prime-)
)
:effect
(and
(Flag391-)
(Flag391prime-)
)

)
(:action Flag391Action-51
:parameters ()
:precondition
(and
(Flag321-)
(Flag321prime-)
)
:effect
(and
(Flag391-)
(Flag391prime-)
)

)
(:action Flag391Action-52
:parameters ()
:precondition
(and
(Flag349-)
(Flag349prime-)
)
:effect
(and
(Flag391-)
(Flag391prime-)
)

)
(:action Flag391Action-53
:parameters ()
:precondition
(and
(Flag368-)
(Flag368prime-)
)
:effect
(and
(Flag391-)
(Flag391prime-)
)

)
(:action Flag391Action-54
:parameters ()
:precondition
(and
(Flag360-)
(Flag360prime-)
)
:effect
(and
(Flag391-)
(Flag391prime-)
)

)
(:action Flag391Action-55
:parameters ()
:precondition
(and
(Flag326-)
(Flag326prime-)
)
:effect
(and
(Flag391-)
(Flag391prime-)
)

)
(:action Flag391Action-56
:parameters ()
:precondition
(and
(Flag327-)
(Flag327prime-)
)
:effect
(and
(Flag391-)
(Flag391prime-)
)

)
(:action Flag391Action-57
:parameters ()
:precondition
(and
(Flag328-)
(Flag328prime-)
)
:effect
(and
(Flag391-)
(Flag391prime-)
)

)
(:action Flag391Action-58
:parameters ()
:precondition
(and
(Flag329-)
(Flag329prime-)
)
:effect
(and
(Flag391-)
(Flag391prime-)
)

)
(:action Flag391Action-59
:parameters ()
:precondition
(and
(Flag385-)
(Flag385prime-)
)
:effect
(and
(Flag391-)
(Flag391prime-)
)

)
(:action Flag391Action-60
:parameters ()
:precondition
(and
(Flag370-)
(Flag370prime-)
)
:effect
(and
(Flag391-)
(Flag391prime-)
)

)
(:action Flag391Action-61
:parameters ()
:precondition
(and
(Flag386-)
(Flag386prime-)
)
:effect
(and
(Flag391-)
(Flag391prime-)
)

)
(:action Flag391Action-62
:parameters ()
:precondition
(and
(Flag351-)
(Flag351prime-)
)
:effect
(and
(Flag391-)
(Flag391prime-)
)

)
(:action Flag391Action-63
:parameters ()
:precondition
(and
(Flag372-)
(Flag372prime-)
)
:effect
(and
(Flag391-)
(Flag391prime-)
)

)
(:action Flag391Action-64
:parameters ()
:precondition
(and
(Flag387-)
(Flag387prime-)
)
:effect
(and
(Flag391-)
(Flag391prime-)
)

)
(:action Flag391Action-65
:parameters ()
:precondition
(and
(Flag373-)
(Flag373prime-)
)
:effect
(and
(Flag391-)
(Flag391prime-)
)

)
(:action Flag391Action-66
:parameters ()
:precondition
(and
(Flag352-)
(Flag352prime-)
)
:effect
(and
(Flag391-)
(Flag391prime-)
)

)
(:action Flag391Action-67
:parameters ()
:precondition
(and
(Flag315-)
(Flag315prime-)
)
:effect
(and
(Flag391-)
(Flag391prime-)
)

)
(:action Flag391Action-68
:parameters ()
:precondition
(and
(Flag331-)
(Flag331prime-)
)
:effect
(and
(Flag391-)
(Flag391prime-)
)

)
(:action Flag391Action-69
:parameters ()
:precondition
(and
(Flag388-)
(Flag388prime-)
)
:effect
(and
(Flag391-)
(Flag391prime-)
)

)
(:action Flag391Action-70
:parameters ()
:precondition
(and
(Flag353-)
(Flag353prime-)
)
:effect
(and
(Flag391-)
(Flag391prime-)
)

)
(:action Flag391Action-71
:parameters ()
:precondition
(and
(Flag389-)
(Flag389prime-)
)
:effect
(and
(Flag391-)
(Flag391prime-)
)

)
(:action Flag391Action-72
:parameters ()
:precondition
(and
(Flag333-)
(Flag333prime-)
)
:effect
(and
(Flag391-)
(Flag391prime-)
)

)
(:action Flag391Action-73
:parameters ()
:precondition
(and
(Flag334-)
(Flag334prime-)
)
:effect
(and
(Flag391-)
(Flag391prime-)
)

)
(:action Flag391Action-74
:parameters ()
:precondition
(and
(Flag354-)
(Flag354prime-)
)
:effect
(and
(Flag391-)
(Flag391prime-)
)

)
(:action Flag391Action-75
:parameters ()
:precondition
(and
(Flag362-)
(Flag362prime-)
)
:effect
(and
(Flag391-)
(Flag391prime-)
)

)
(:action Flag391Action-76
:parameters ()
:precondition
(and
(Flag324-)
(Flag324prime-)
)
:effect
(and
(Flag391-)
(Flag391prime-)
)

)
(:action Flag391Action-77
:parameters ()
:precondition
(and
(Flag390-)
(Flag390prime-)
)
:effect
(and
(Flag391-)
(Flag391prime-)
)

)
(:action Prim391Action
:parameters ()
:precondition
(and
(not (Flag299-))
(Flag299prime-)
(not (Flag300-))
(Flag300prime-)
(not (Flag375-))
(Flag375prime-)
(not (Flag341-))
(Flag341prime-)
(not (Flag376-))
(Flag376prime-)
(not (Flag337-))
(Flag337prime-)
(not (Flag356-))
(Flag356prime-)
(not (Flag359-))
(Flag359prime-)
(not (Flag357-))
(Flag357prime-)
(not (Flag302-))
(Flag302prime-)
(not (Flag371-))
(Flag371prime-)
(not (Flag377-))
(Flag377prime-)
(not (Flag338-))
(Flag338prime-)
(not (Flag378-))
(Flag378prime-)
(not (Flag339-))
(Flag339prime-)
(not (Flag308-))
(Flag308prime-)
(not (Flag379-))
(Flag379prime-)
(not (Flag303-))
(Flag303prime-)
(not (Flag314-))
(Flag314prime-)
(not (Flag305-))
(Flag305prime-)
(not (Flag361-))
(Flag361prime-)
(not (Flag306-))
(Flag306prime-)
(not (Flag340-))
(Flag340prime-)
(not (Flag369-))
(Flag369prime-)
(not (Flag307-))
(Flag307prime-)
(not (Flag363-))
(Flag363prime-)
(not (Flag309-))
(Flag309prime-)
(not (Flag332-))
(Flag332prime-)
(not (Flag342-))
(Flag342prime-)
(not (Flag311-))
(Flag311prime-)
(not (Flag364-))
(Flag364prime-)
(not (Flag343-))
(Flag343prime-)
(not (Flag380-))
(Flag380prime-)
(not (Flag344-))
(Flag344prime-)
(not (Flag312-))
(Flag312prime-)
(not (Flag313-))
(Flag313prime-)
(not (Flag365-))
(Flag365prime-)
(not (Flag366-))
(Flag366prime-)
(not (Flag381-))
(Flag381prime-)
(not (Flag316-))
(Flag316prime-)
(not (Flag382-))
(Flag382prime-)
(not (Flag318-))
(Flag318prime-)
(not (Flag319-))
(Flag319prime-)
(not (Flag347-))
(Flag347prime-)
(not (Flag320-))
(Flag320prime-)
(not (Flag383-))
(Flag383prime-)
(not (Flag348-))
(Flag348prime-)
(not (Flag322-))
(Flag322prime-)
(not (Flag384-))
(Flag384prime-)
(not (Flag330-))
(Flag330prime-)
(not (Flag323-))
(Flag323prime-)
(not (Flag321-))
(Flag321prime-)
(not (Flag349-))
(Flag349prime-)
(not (Flag368-))
(Flag368prime-)
(not (Flag360-))
(Flag360prime-)
(not (Flag326-))
(Flag326prime-)
(not (Flag327-))
(Flag327prime-)
(not (Flag328-))
(Flag328prime-)
(not (Flag329-))
(Flag329prime-)
(not (Flag385-))
(Flag385prime-)
(not (Flag370-))
(Flag370prime-)
(not (Flag386-))
(Flag386prime-)
(not (Flag351-))
(Flag351prime-)
(not (Flag372-))
(Flag372prime-)
(not (Flag387-))
(Flag387prime-)
(not (Flag373-))
(Flag373prime-)
(not (Flag352-))
(Flag352prime-)
(not (Flag315-))
(Flag315prime-)
(not (Flag331-))
(Flag331prime-)
(not (Flag388-))
(Flag388prime-)
(not (Flag353-))
(Flag353prime-)
(not (Flag389-))
(Flag389prime-)
(not (Flag333-))
(Flag333prime-)
(not (Flag334-))
(Flag334prime-)
(not (Flag354-))
(Flag354prime-)
(not (Flag362-))
(Flag362prime-)
(not (Flag324-))
(Flag324prime-)
(not (Flag390-))
(Flag390prime-)
)
:effect
(and
(Flag391prime-)
(not (Flag391-))
)

)
(:action Flag408Action-0
:parameters ()
:precondition
(and
(Flag386-)
(Flag386prime-)
)
:effect
(and
(Flag408-)
(Flag408prime-)
)

)
(:action Flag408Action-1
:parameters ()
:precondition
(and
(Flag302-)
(Flag302prime-)
)
:effect
(and
(Flag408-)
(Flag408prime-)
)

)
(:action Flag408Action-2
:parameters ()
:precondition
(and
(Flag337-)
(Flag337prime-)
)
:effect
(and
(Flag408-)
(Flag408prime-)
)

)
(:action Flag408Action-3
:parameters ()
:precondition
(and
(Flag305-)
(Flag305prime-)
)
:effect
(and
(Flag408-)
(Flag408prime-)
)

)
(:action Flag408Action-4
:parameters ()
:precondition
(and
(Flag378-)
(Flag378prime-)
)
:effect
(and
(Flag408-)
(Flag408prime-)
)

)
(:action Flag408Action-5
:parameters ()
:precondition
(and
(Flag307-)
(Flag307prime-)
)
:effect
(and
(Flag408-)
(Flag408prime-)
)

)
(:action Flag408Action-6
:parameters ()
:precondition
(and
(Flag309-)
(Flag309prime-)
)
:effect
(and
(Flag408-)
(Flag408prime-)
)

)
(:action Flag408Action-7
:parameters ()
:precondition
(and
(Flag342-)
(Flag342prime-)
)
:effect
(and
(Flag408-)
(Flag408prime-)
)

)
(:action Flag408Action-8
:parameters ()
:precondition
(and
(Flag359-)
(Flag359prime-)
)
:effect
(and
(Flag408-)
(Flag408prime-)
)

)
(:action Flag408Action-9
:parameters ()
:precondition
(and
(Flag392-)
(Flag392prime-)
)
:effect
(and
(Flag408-)
(Flag408prime-)
)

)
(:action Flag408Action-10
:parameters ()
:precondition
(and
(Flag343-)
(Flag343prime-)
)
:effect
(and
(Flag408-)
(Flag408prime-)
)

)
(:action Flag408Action-11
:parameters ()
:precondition
(and
(Flag380-)
(Flag380prime-)
)
:effect
(and
(Flag408-)
(Flag408prime-)
)

)
(:action Flag408Action-12
:parameters ()
:precondition
(and
(Flag388-)
(Flag388prime-)
)
:effect
(and
(Flag408-)
(Flag408prime-)
)

)
(:action Flag408Action-13
:parameters ()
:precondition
(and
(Flag322-)
(Flag322prime-)
)
:effect
(and
(Flag408-)
(Flag408prime-)
)

)
(:action Flag408Action-14
:parameters ()
:precondition
(and
(Flag393-)
(Flag393prime-)
)
:effect
(and
(Flag408-)
(Flag408prime-)
)

)
(:action Flag408Action-15
:parameters ()
:precondition
(and
(Flag318-)
(Flag318prime-)
)
:effect
(and
(Flag408-)
(Flag408prime-)
)

)
(:action Flag408Action-16
:parameters ()
:precondition
(and
(Flag344-)
(Flag344prime-)
)
:effect
(and
(Flag408-)
(Flag408prime-)
)

)
(:action Flag408Action-17
:parameters ()
:precondition
(and
(Flag394-)
(Flag394prime-)
)
:effect
(and
(Flag408-)
(Flag408prime-)
)

)
(:action Flag408Action-18
:parameters ()
:precondition
(and
(Flag349-)
(Flag349prime-)
)
:effect
(and
(Flag408-)
(Flag408prime-)
)

)
(:action Flag408Action-19
:parameters ()
:precondition
(and
(Flag368-)
(Flag368prime-)
)
:effect
(and
(Flag408-)
(Flag408prime-)
)

)
(:action Flag408Action-20
:parameters ()
:precondition
(and
(Flag377-)
(Flag377prime-)
)
:effect
(and
(Flag408-)
(Flag408prime-)
)

)
(:action Flag408Action-21
:parameters ()
:precondition
(and
(Flag326-)
(Flag326prime-)
)
:effect
(and
(Flag408-)
(Flag408prime-)
)

)
(:action Flag408Action-22
:parameters ()
:precondition
(and
(Flag300-)
(Flag300prime-)
)
:effect
(and
(Flag408-)
(Flag408prime-)
)

)
(:action Flag408Action-23
:parameters ()
:precondition
(and
(Flag385-)
(Flag385prime-)
)
:effect
(and
(Flag408-)
(Flag408prime-)
)

)
(:action Flag408Action-24
:parameters ()
:precondition
(and
(Flag387-)
(Flag387prime-)
)
:effect
(and
(Flag408-)
(Flag408prime-)
)

)
(:action Flag408Action-25
:parameters ()
:precondition
(and
(Flag319-)
(Flag319prime-)
)
:effect
(and
(Flag408-)
(Flag408prime-)
)

)
(:action Flag408Action-26
:parameters ()
:precondition
(and
(Flag354-)
(Flag354prime-)
)
:effect
(and
(Flag408-)
(Flag408prime-)
)

)
(:action Flag408Action-27
:parameters ()
:precondition
(and
(Flag308-)
(Flag308prime-)
)
:effect
(and
(Flag408-)
(Flag408prime-)
)

)
(:action Flag408Action-28
:parameters ()
:precondition
(and
(Flag357-)
(Flag357prime-)
)
:effect
(and
(Flag408-)
(Flag408prime-)
)

)
(:action Flag408Action-29
:parameters ()
:precondition
(and
(Flag395-)
(Flag395prime-)
)
:effect
(and
(Flag408-)
(Flag408prime-)
)

)
(:action Flag408Action-30
:parameters ()
:precondition
(and
(Flag339-)
(Flag339prime-)
)
:effect
(and
(Flag408-)
(Flag408prime-)
)

)
(:action Flag408Action-31
:parameters ()
:precondition
(and
(Flag396-)
(Flag396prime-)
)
:effect
(and
(Flag408-)
(Flag408prime-)
)

)
(:action Flag408Action-32
:parameters ()
:precondition
(and
(Flag397-)
(Flag397prime-)
)
:effect
(and
(Flag408-)
(Flag408prime-)
)

)
(:action Flag408Action-33
:parameters ()
:precondition
(and
(Flag341-)
(Flag341prime-)
)
:effect
(and
(Flag408-)
(Flag408prime-)
)

)
(:action Flag408Action-34
:parameters ()
:precondition
(and
(Flag311-)
(Flag311prime-)
)
:effect
(and
(Flag408-)
(Flag408prime-)
)

)
(:action Flag408Action-35
:parameters ()
:precondition
(and
(Flag381-)
(Flag381prime-)
)
:effect
(and
(Flag408-)
(Flag408prime-)
)

)
(:action Flag408Action-36
:parameters ()
:precondition
(and
(Flag312-)
(Flag312prime-)
)
:effect
(and
(Flag408-)
(Flag408prime-)
)

)
(:action Flag408Action-37
:parameters ()
:precondition
(and
(Flag363-)
(Flag363prime-)
)
:effect
(and
(Flag408-)
(Flag408prime-)
)

)
(:action Flag408Action-38
:parameters ()
:precondition
(and
(Flag321-)
(Flag321prime-)
)
:effect
(and
(Flag408-)
(Flag408prime-)
)

)
(:action Flag408Action-39
:parameters ()
:precondition
(and
(Flag323-)
(Flag323prime-)
)
:effect
(and
(Flag408-)
(Flag408prime-)
)

)
(:action Flag408Action-40
:parameters ()
:precondition
(and
(Flag398-)
(Flag398prime-)
)
:effect
(and
(Flag408-)
(Flag408prime-)
)

)
(:action Flag408Action-41
:parameters ()
:precondition
(and
(Flag360-)
(Flag360prime-)
)
:effect
(and
(Flag408-)
(Flag408prime-)
)

)
(:action Flag408Action-42
:parameters ()
:precondition
(and
(Flag327-)
(Flag327prime-)
)
:effect
(and
(Flag408-)
(Flag408prime-)
)

)
(:action Flag408Action-43
:parameters ()
:precondition
(and
(Flag369-)
(Flag369prime-)
)
:effect
(and
(Flag408-)
(Flag408prime-)
)

)
(:action Flag408Action-44
:parameters ()
:precondition
(and
(Flag370-)
(Flag370prime-)
)
:effect
(and
(Flag408-)
(Flag408prime-)
)

)
(:action Flag408Action-45
:parameters ()
:precondition
(and
(Flag331-)
(Flag331prime-)
)
:effect
(and
(Flag408-)
(Flag408prime-)
)

)
(:action Flag408Action-46
:parameters ()
:precondition
(and
(Flag352-)
(Flag352prime-)
)
:effect
(and
(Flag408-)
(Flag408prime-)
)

)
(:action Flag408Action-47
:parameters ()
:precondition
(and
(Flag399-)
(Flag399prime-)
)
:effect
(and
(Flag408-)
(Flag408prime-)
)

)
(:action Flag408Action-48
:parameters ()
:precondition
(and
(Flag389-)
(Flag389prime-)
)
:effect
(and
(Flag408-)
(Flag408prime-)
)

)
(:action Flag408Action-49
:parameters ()
:precondition
(and
(Flag333-)
(Flag333prime-)
)
:effect
(and
(Flag408-)
(Flag408prime-)
)

)
(:action Flag408Action-50
:parameters ()
:precondition
(and
(Flag334-)
(Flag334prime-)
)
:effect
(and
(Flag408-)
(Flag408prime-)
)

)
(:action Flag408Action-51
:parameters ()
:precondition
(and
(Flag299-)
(Flag299prime-)
)
:effect
(and
(Flag408-)
(Flag408prime-)
)

)
(:action Flag408Action-52
:parameters ()
:precondition
(and
(Flag375-)
(Flag375prime-)
)
:effect
(and
(Flag408-)
(Flag408prime-)
)

)
(:action Flag408Action-53
:parameters ()
:precondition
(and
(Flag400-)
(Flag400prime-)
)
:effect
(and
(Flag408-)
(Flag408prime-)
)

)
(:action Flag408Action-54
:parameters ()
:precondition
(and
(Flag401-)
(Flag401prime-)
)
:effect
(and
(Flag408-)
(Flag408prime-)
)

)
(:action Flag408Action-55
:parameters ()
:precondition
(and
(Flag379-)
(Flag379prime-)
)
:effect
(and
(Flag408-)
(Flag408prime-)
)

)
(:action Flag408Action-56
:parameters ()
:precondition
(and
(Flag303-)
(Flag303prime-)
)
:effect
(and
(Flag408-)
(Flag408prime-)
)

)
(:action Flag408Action-57
:parameters ()
:precondition
(and
(Flag402-)
(Flag402prime-)
)
:effect
(and
(Flag408-)
(Flag408prime-)
)

)
(:action Flag408Action-58
:parameters ()
:precondition
(and
(Flag340-)
(Flag340prime-)
)
:effect
(and
(Flag408-)
(Flag408prime-)
)

)
(:action Flag408Action-59
:parameters ()
:precondition
(and
(Flag356-)
(Flag356prime-)
)
:effect
(and
(Flag408-)
(Flag408prime-)
)

)
(:action Flag408Action-60
:parameters ()
:precondition
(and
(Flag371-)
(Flag371prime-)
)
:effect
(and
(Flag408-)
(Flag408prime-)
)

)
(:action Flag408Action-61
:parameters ()
:precondition
(and
(Flag365-)
(Flag365prime-)
)
:effect
(and
(Flag408-)
(Flag408prime-)
)

)
(:action Flag408Action-62
:parameters ()
:precondition
(and
(Flag315-)
(Flag315prime-)
)
:effect
(and
(Flag408-)
(Flag408prime-)
)

)
(:action Flag408Action-63
:parameters ()
:precondition
(and
(Flag316-)
(Flag316prime-)
)
:effect
(and
(Flag408-)
(Flag408prime-)
)

)
(:action Flag408Action-64
:parameters ()
:precondition
(and
(Flag347-)
(Flag347prime-)
)
:effect
(and
(Flag408-)
(Flag408prime-)
)

)
(:action Flag408Action-65
:parameters ()
:precondition
(and
(Flag320-)
(Flag320prime-)
)
:effect
(and
(Flag408-)
(Flag408prime-)
)

)
(:action Flag408Action-66
:parameters ()
:precondition
(and
(Flag384-)
(Flag384prime-)
)
:effect
(and
(Flag408-)
(Flag408prime-)
)

)
(:action Flag408Action-67
:parameters ()
:precondition
(and
(Flag330-)
(Flag330prime-)
)
:effect
(and
(Flag408-)
(Flag408prime-)
)

)
(:action Flag408Action-68
:parameters ()
:precondition
(and
(Flag403-)
(Flag403prime-)
)
:effect
(and
(Flag408-)
(Flag408prime-)
)

)
(:action Flag408Action-69
:parameters ()
:precondition
(and
(Flag404-)
(Flag404prime-)
)
:effect
(and
(Flag408-)
(Flag408prime-)
)

)
(:action Flag408Action-70
:parameters ()
:precondition
(and
(Flag328-)
(Flag328prime-)
)
:effect
(and
(Flag408-)
(Flag408prime-)
)

)
(:action Flag408Action-71
:parameters ()
:precondition
(and
(Flag382-)
(Flag382prime-)
)
:effect
(and
(Flag408-)
(Flag408prime-)
)

)
(:action Flag408Action-72
:parameters ()
:precondition
(and
(Flag372-)
(Flag372prime-)
)
:effect
(and
(Flag408-)
(Flag408prime-)
)

)
(:action Flag408Action-73
:parameters ()
:precondition
(and
(Flag353-)
(Flag353prime-)
)
:effect
(and
(Flag408-)
(Flag408prime-)
)

)
(:action Flag408Action-74
:parameters ()
:precondition
(and
(Flag405-)
(Flag405prime-)
)
:effect
(and
(Flag408-)
(Flag408prime-)
)

)
(:action Flag408Action-75
:parameters ()
:precondition
(and
(Flag373-)
(Flag373prime-)
)
:effect
(and
(Flag408-)
(Flag408prime-)
)

)
(:action Flag408Action-76
:parameters ()
:precondition
(and
(Flag406-)
(Flag406prime-)
)
:effect
(and
(Flag408-)
(Flag408prime-)
)

)
(:action Flag408Action-77
:parameters ()
:precondition
(and
(Flag314-)
(Flag314prime-)
)
:effect
(and
(Flag408-)
(Flag408prime-)
)

)
(:action Flag408Action-78
:parameters ()
:precondition
(and
(Flag306-)
(Flag306prime-)
)
:effect
(and
(Flag408-)
(Flag408prime-)
)

)
(:action Flag408Action-79
:parameters ()
:precondition
(and
(Flag362-)
(Flag362prime-)
)
:effect
(and
(Flag408-)
(Flag408prime-)
)

)
(:action Flag408Action-80
:parameters ()
:precondition
(and
(Flag332-)
(Flag332prime-)
)
:effect
(and
(Flag408-)
(Flag408prime-)
)

)
(:action Flag408Action-81
:parameters ()
:precondition
(and
(Flag407-)
(Flag407prime-)
)
:effect
(and
(Flag408-)
(Flag408prime-)
)

)
(:action Flag408Action-82
:parameters ()
:precondition
(and
(Flag364-)
(Flag364prime-)
)
:effect
(and
(Flag408-)
(Flag408prime-)
)

)
(:action Flag408Action-83
:parameters ()
:precondition
(and
(Flag390-)
(Flag390prime-)
)
:effect
(and
(Flag408-)
(Flag408prime-)
)

)
(:action Flag408Action-84
:parameters ()
:precondition
(and
(Flag366-)
(Flag366prime-)
)
:effect
(and
(Flag408-)
(Flag408prime-)
)

)
(:action Flag408Action-85
:parameters ()
:precondition
(and
(Flag338-)
(Flag338prime-)
)
:effect
(and
(Flag408-)
(Flag408prime-)
)

)
(:action Flag408Action-86
:parameters ()
:precondition
(and
(Flag324-)
(Flag324prime-)
)
:effect
(and
(Flag408-)
(Flag408prime-)
)

)
(:action Flag408Action-87
:parameters ()
:precondition
(and
(Flag351-)
(Flag351prime-)
)
:effect
(and
(Flag408-)
(Flag408prime-)
)

)
(:action Prim408Action
:parameters ()
:precondition
(and
(not (Flag386-))
(Flag386prime-)
(not (Flag302-))
(Flag302prime-)
(not (Flag337-))
(Flag337prime-)
(not (Flag305-))
(Flag305prime-)
(not (Flag378-))
(Flag378prime-)
(not (Flag307-))
(Flag307prime-)
(not (Flag309-))
(Flag309prime-)
(not (Flag342-))
(Flag342prime-)
(not (Flag359-))
(Flag359prime-)
(not (Flag392-))
(Flag392prime-)
(not (Flag343-))
(Flag343prime-)
(not (Flag380-))
(Flag380prime-)
(not (Flag388-))
(Flag388prime-)
(not (Flag322-))
(Flag322prime-)
(not (Flag393-))
(Flag393prime-)
(not (Flag318-))
(Flag318prime-)
(not (Flag344-))
(Flag344prime-)
(not (Flag394-))
(Flag394prime-)
(not (Flag349-))
(Flag349prime-)
(not (Flag368-))
(Flag368prime-)
(not (Flag377-))
(Flag377prime-)
(not (Flag326-))
(Flag326prime-)
(not (Flag300-))
(Flag300prime-)
(not (Flag385-))
(Flag385prime-)
(not (Flag387-))
(Flag387prime-)
(not (Flag319-))
(Flag319prime-)
(not (Flag354-))
(Flag354prime-)
(not (Flag308-))
(Flag308prime-)
(not (Flag357-))
(Flag357prime-)
(not (Flag395-))
(Flag395prime-)
(not (Flag339-))
(Flag339prime-)
(not (Flag396-))
(Flag396prime-)
(not (Flag397-))
(Flag397prime-)
(not (Flag341-))
(Flag341prime-)
(not (Flag311-))
(Flag311prime-)
(not (Flag381-))
(Flag381prime-)
(not (Flag312-))
(Flag312prime-)
(not (Flag363-))
(Flag363prime-)
(not (Flag321-))
(Flag321prime-)
(not (Flag323-))
(Flag323prime-)
(not (Flag398-))
(Flag398prime-)
(not (Flag360-))
(Flag360prime-)
(not (Flag327-))
(Flag327prime-)
(not (Flag369-))
(Flag369prime-)
(not (Flag370-))
(Flag370prime-)
(not (Flag331-))
(Flag331prime-)
(not (Flag352-))
(Flag352prime-)
(not (Flag399-))
(Flag399prime-)
(not (Flag389-))
(Flag389prime-)
(not (Flag333-))
(Flag333prime-)
(not (Flag334-))
(Flag334prime-)
(not (Flag299-))
(Flag299prime-)
(not (Flag375-))
(Flag375prime-)
(not (Flag400-))
(Flag400prime-)
(not (Flag401-))
(Flag401prime-)
(not (Flag379-))
(Flag379prime-)
(not (Flag303-))
(Flag303prime-)
(not (Flag402-))
(Flag402prime-)
(not (Flag340-))
(Flag340prime-)
(not (Flag356-))
(Flag356prime-)
(not (Flag371-))
(Flag371prime-)
(not (Flag365-))
(Flag365prime-)
(not (Flag315-))
(Flag315prime-)
(not (Flag316-))
(Flag316prime-)
(not (Flag347-))
(Flag347prime-)
(not (Flag320-))
(Flag320prime-)
(not (Flag384-))
(Flag384prime-)
(not (Flag330-))
(Flag330prime-)
(not (Flag403-))
(Flag403prime-)
(not (Flag404-))
(Flag404prime-)
(not (Flag328-))
(Flag328prime-)
(not (Flag382-))
(Flag382prime-)
(not (Flag372-))
(Flag372prime-)
(not (Flag353-))
(Flag353prime-)
(not (Flag405-))
(Flag405prime-)
(not (Flag373-))
(Flag373prime-)
(not (Flag406-))
(Flag406prime-)
(not (Flag314-))
(Flag314prime-)
(not (Flag306-))
(Flag306prime-)
(not (Flag362-))
(Flag362prime-)
(not (Flag332-))
(Flag332prime-)
(not (Flag407-))
(Flag407prime-)
(not (Flag364-))
(Flag364prime-)
(not (Flag390-))
(Flag390prime-)
(not (Flag366-))
(Flag366prime-)
(not (Flag338-))
(Flag338prime-)
(not (Flag324-))
(Flag324prime-)
(not (Flag351-))
(Flag351prime-)
)
:effect
(and
(Flag408prime-)
(not (Flag408-))
)

)
(:action Flag425Action-0
:parameters ()
:precondition
(and
(Flag409-)
(Flag409prime-)
)
:effect
(and
(Flag425-)
(Flag425prime-)
)

)
(:action Flag425Action-1
:parameters ()
:precondition
(and
(Flag386-)
(Flag386prime-)
)
:effect
(and
(Flag425-)
(Flag425prime-)
)

)
(:action Flag425Action-2
:parameters ()
:precondition
(and
(Flag302-)
(Flag302prime-)
)
:effect
(and
(Flag425-)
(Flag425prime-)
)

)
(:action Flag425Action-3
:parameters ()
:precondition
(and
(Flag305-)
(Flag305prime-)
)
:effect
(and
(Flag425-)
(Flag425prime-)
)

)
(:action Flag425Action-4
:parameters ()
:precondition
(and
(Flag378-)
(Flag378prime-)
)
:effect
(and
(Flag425-)
(Flag425prime-)
)

)
(:action Flag425Action-5
:parameters ()
:precondition
(and
(Flag307-)
(Flag307prime-)
)
:effect
(and
(Flag425-)
(Flag425prime-)
)

)
(:action Flag425Action-6
:parameters ()
:precondition
(and
(Flag309-)
(Flag309prime-)
)
:effect
(and
(Flag425-)
(Flag425prime-)
)

)
(:action Flag425Action-7
:parameters ()
:precondition
(and
(Flag342-)
(Flag342prime-)
)
:effect
(and
(Flag425-)
(Flag425prime-)
)

)
(:action Flag425Action-8
:parameters ()
:precondition
(and
(Flag359-)
(Flag359prime-)
)
:effect
(and
(Flag425-)
(Flag425prime-)
)

)
(:action Flag425Action-9
:parameters ()
:precondition
(and
(Flag343-)
(Flag343prime-)
)
:effect
(and
(Flag425-)
(Flag425prime-)
)

)
(:action Flag425Action-10
:parameters ()
:precondition
(and
(Flag380-)
(Flag380prime-)
)
:effect
(and
(Flag425-)
(Flag425prime-)
)

)
(:action Flag425Action-11
:parameters ()
:precondition
(and
(Flag388-)
(Flag388prime-)
)
:effect
(and
(Flag425-)
(Flag425prime-)
)

)
(:action Flag425Action-12
:parameters ()
:precondition
(and
(Flag322-)
(Flag322prime-)
)
:effect
(and
(Flag425-)
(Flag425prime-)
)

)
(:action Flag425Action-13
:parameters ()
:precondition
(and
(Flag393-)
(Flag393prime-)
)
:effect
(and
(Flag425-)
(Flag425prime-)
)

)
(:action Flag425Action-14
:parameters ()
:precondition
(and
(Flag410-)
(Flag410prime-)
)
:effect
(and
(Flag425-)
(Flag425prime-)
)

)
(:action Flag425Action-15
:parameters ()
:precondition
(and
(Flag411-)
(Flag411prime-)
)
:effect
(and
(Flag425-)
(Flag425prime-)
)

)
(:action Flag425Action-16
:parameters ()
:precondition
(and
(Flag318-)
(Flag318prime-)
)
:effect
(and
(Flag425-)
(Flag425prime-)
)

)
(:action Flag425Action-17
:parameters ()
:precondition
(and
(Flag344-)
(Flag344prime-)
)
:effect
(and
(Flag425-)
(Flag425prime-)
)

)
(:action Flag425Action-18
:parameters ()
:precondition
(and
(Flag349-)
(Flag349prime-)
)
:effect
(and
(Flag425-)
(Flag425prime-)
)

)
(:action Flag425Action-19
:parameters ()
:precondition
(and
(Flag368-)
(Flag368prime-)
)
:effect
(and
(Flag425-)
(Flag425prime-)
)

)
(:action Flag425Action-20
:parameters ()
:precondition
(and
(Flag377-)
(Flag377prime-)
)
:effect
(and
(Flag425-)
(Flag425prime-)
)

)
(:action Flag425Action-21
:parameters ()
:precondition
(and
(Flag326-)
(Flag326prime-)
)
:effect
(and
(Flag425-)
(Flag425prime-)
)

)
(:action Flag425Action-22
:parameters ()
:precondition
(and
(Flag300-)
(Flag300prime-)
)
:effect
(and
(Flag425-)
(Flag425prime-)
)

)
(:action Flag425Action-23
:parameters ()
:precondition
(and
(Flag385-)
(Flag385prime-)
)
:effect
(and
(Flag425-)
(Flag425prime-)
)

)
(:action Flag425Action-24
:parameters ()
:precondition
(and
(Flag412-)
(Flag412prime-)
)
:effect
(and
(Flag425-)
(Flag425prime-)
)

)
(:action Flag425Action-25
:parameters ()
:precondition
(and
(Flag387-)
(Flag387prime-)
)
:effect
(and
(Flag425-)
(Flag425prime-)
)

)
(:action Flag425Action-26
:parameters ()
:precondition
(and
(Flag319-)
(Flag319prime-)
)
:effect
(and
(Flag425-)
(Flag425prime-)
)

)
(:action Flag425Action-27
:parameters ()
:precondition
(and
(Flag354-)
(Flag354prime-)
)
:effect
(and
(Flag425-)
(Flag425prime-)
)

)
(:action Flag425Action-28
:parameters ()
:precondition
(and
(Flag308-)
(Flag308prime-)
)
:effect
(and
(Flag425-)
(Flag425prime-)
)

)
(:action Flag425Action-29
:parameters ()
:precondition
(and
(Flag357-)
(Flag357prime-)
)
:effect
(and
(Flag425-)
(Flag425prime-)
)

)
(:action Flag425Action-30
:parameters ()
:precondition
(and
(Flag395-)
(Flag395prime-)
)
:effect
(and
(Flag425-)
(Flag425prime-)
)

)
(:action Flag425Action-31
:parameters ()
:precondition
(and
(Flag413-)
(Flag413prime-)
)
:effect
(and
(Flag425-)
(Flag425prime-)
)

)
(:action Flag425Action-32
:parameters ()
:precondition
(and
(Flag339-)
(Flag339prime-)
)
:effect
(and
(Flag425-)
(Flag425prime-)
)

)
(:action Flag425Action-33
:parameters ()
:precondition
(and
(Flag396-)
(Flag396prime-)
)
:effect
(and
(Flag425-)
(Flag425prime-)
)

)
(:action Flag425Action-34
:parameters ()
:precondition
(and
(Flag397-)
(Flag397prime-)
)
:effect
(and
(Flag425-)
(Flag425prime-)
)

)
(:action Flag425Action-35
:parameters ()
:precondition
(and
(Flag414-)
(Flag414prime-)
)
:effect
(and
(Flag425-)
(Flag425prime-)
)

)
(:action Flag425Action-36
:parameters ()
:precondition
(and
(Flag341-)
(Flag341prime-)
)
:effect
(and
(Flag425-)
(Flag425prime-)
)

)
(:action Flag425Action-37
:parameters ()
:precondition
(and
(Flag381-)
(Flag381prime-)
)
:effect
(and
(Flag425-)
(Flag425prime-)
)

)
(:action Flag425Action-38
:parameters ()
:precondition
(and
(Flag312-)
(Flag312prime-)
)
:effect
(and
(Flag425-)
(Flag425prime-)
)

)
(:action Flag425Action-39
:parameters ()
:precondition
(and
(Flag415-)
(Flag415prime-)
)
:effect
(and
(Flag425-)
(Flag425prime-)
)

)
(:action Flag425Action-40
:parameters ()
:precondition
(and
(Flag363-)
(Flag363prime-)
)
:effect
(and
(Flag425-)
(Flag425prime-)
)

)
(:action Flag425Action-41
:parameters ()
:precondition
(and
(Flag321-)
(Flag321prime-)
)
:effect
(and
(Flag425-)
(Flag425prime-)
)

)
(:action Flag425Action-42
:parameters ()
:precondition
(and
(Flag323-)
(Flag323prime-)
)
:effect
(and
(Flag425-)
(Flag425prime-)
)

)
(:action Flag425Action-43
:parameters ()
:precondition
(and
(Flag398-)
(Flag398prime-)
)
:effect
(and
(Flag425-)
(Flag425prime-)
)

)
(:action Flag425Action-44
:parameters ()
:precondition
(and
(Flag360-)
(Flag360prime-)
)
:effect
(and
(Flag425-)
(Flag425prime-)
)

)
(:action Flag425Action-45
:parameters ()
:precondition
(and
(Flag327-)
(Flag327prime-)
)
:effect
(and
(Flag425-)
(Flag425prime-)
)

)
(:action Flag425Action-46
:parameters ()
:precondition
(and
(Flag369-)
(Flag369prime-)
)
:effect
(and
(Flag425-)
(Flag425prime-)
)

)
(:action Flag425Action-47
:parameters ()
:precondition
(and
(Flag370-)
(Flag370prime-)
)
:effect
(and
(Flag425-)
(Flag425prime-)
)

)
(:action Flag425Action-48
:parameters ()
:precondition
(and
(Flag331-)
(Flag331prime-)
)
:effect
(and
(Flag425-)
(Flag425prime-)
)

)
(:action Flag425Action-49
:parameters ()
:precondition
(and
(Flag352-)
(Flag352prime-)
)
:effect
(and
(Flag425-)
(Flag425prime-)
)

)
(:action Flag425Action-50
:parameters ()
:precondition
(and
(Flag399-)
(Flag399prime-)
)
:effect
(and
(Flag425-)
(Flag425prime-)
)

)
(:action Flag425Action-51
:parameters ()
:precondition
(and
(Flag416-)
(Flag416prime-)
)
:effect
(and
(Flag425-)
(Flag425prime-)
)

)
(:action Flag425Action-52
:parameters ()
:precondition
(and
(Flag389-)
(Flag389prime-)
)
:effect
(and
(Flag425-)
(Flag425prime-)
)

)
(:action Flag425Action-53
:parameters ()
:precondition
(and
(Flag333-)
(Flag333prime-)
)
:effect
(and
(Flag425-)
(Flag425prime-)
)

)
(:action Flag425Action-54
:parameters ()
:precondition
(and
(Flag334-)
(Flag334prime-)
)
:effect
(and
(Flag425-)
(Flag425prime-)
)

)
(:action Flag425Action-55
:parameters ()
:precondition
(and
(Flag299-)
(Flag299prime-)
)
:effect
(and
(Flag425-)
(Flag425prime-)
)

)
(:action Flag425Action-56
:parameters ()
:precondition
(and
(Flag375-)
(Flag375prime-)
)
:effect
(and
(Flag425-)
(Flag425prime-)
)

)
(:action Flag425Action-57
:parameters ()
:precondition
(and
(Flag417-)
(Flag417prime-)
)
:effect
(and
(Flag425-)
(Flag425prime-)
)

)
(:action Flag425Action-58
:parameters ()
:precondition
(and
(Flag400-)
(Flag400prime-)
)
:effect
(and
(Flag425-)
(Flag425prime-)
)

)
(:action Flag425Action-59
:parameters ()
:precondition
(and
(Flag418-)
(Flag418prime-)
)
:effect
(and
(Flag425-)
(Flag425prime-)
)

)
(:action Flag425Action-60
:parameters ()
:precondition
(and
(Flag401-)
(Flag401prime-)
)
:effect
(and
(Flag425-)
(Flag425prime-)
)

)
(:action Flag425Action-61
:parameters ()
:precondition
(and
(Flag379-)
(Flag379prime-)
)
:effect
(and
(Flag425-)
(Flag425prime-)
)

)
(:action Flag425Action-62
:parameters ()
:precondition
(and
(Flag303-)
(Flag303prime-)
)
:effect
(and
(Flag425-)
(Flag425prime-)
)

)
(:action Flag425Action-63
:parameters ()
:precondition
(and
(Flag340-)
(Flag340prime-)
)
:effect
(and
(Flag425-)
(Flag425prime-)
)

)
(:action Flag425Action-64
:parameters ()
:precondition
(and
(Flag356-)
(Flag356prime-)
)
:effect
(and
(Flag425-)
(Flag425prime-)
)

)
(:action Flag425Action-65
:parameters ()
:precondition
(and
(Flag371-)
(Flag371prime-)
)
:effect
(and
(Flag425-)
(Flag425prime-)
)

)
(:action Flag425Action-66
:parameters ()
:precondition
(and
(Flag365-)
(Flag365prime-)
)
:effect
(and
(Flag425-)
(Flag425prime-)
)

)
(:action Flag425Action-67
:parameters ()
:precondition
(and
(Flag315-)
(Flag315prime-)
)
:effect
(and
(Flag425-)
(Flag425prime-)
)

)
(:action Flag425Action-68
:parameters ()
:precondition
(and
(Flag316-)
(Flag316prime-)
)
:effect
(and
(Flag425-)
(Flag425prime-)
)

)
(:action Flag425Action-69
:parameters ()
:precondition
(and
(Flag347-)
(Flag347prime-)
)
:effect
(and
(Flag425-)
(Flag425prime-)
)

)
(:action Flag425Action-70
:parameters ()
:precondition
(and
(Flag320-)
(Flag320prime-)
)
:effect
(and
(Flag425-)
(Flag425prime-)
)

)
(:action Flag425Action-71
:parameters ()
:precondition
(and
(Flag384-)
(Flag384prime-)
)
:effect
(and
(Flag425-)
(Flag425prime-)
)

)
(:action Flag425Action-72
:parameters ()
:precondition
(and
(Flag330-)
(Flag330prime-)
)
:effect
(and
(Flag425-)
(Flag425prime-)
)

)
(:action Flag425Action-73
:parameters ()
:precondition
(and
(Flag403-)
(Flag403prime-)
)
:effect
(and
(Flag425-)
(Flag425prime-)
)

)
(:action Flag425Action-74
:parameters ()
:precondition
(and
(Flag404-)
(Flag404prime-)
)
:effect
(and
(Flag425-)
(Flag425prime-)
)

)
(:action Flag425Action-75
:parameters ()
:precondition
(and
(Flag328-)
(Flag328prime-)
)
:effect
(and
(Flag425-)
(Flag425prime-)
)

)
(:action Flag425Action-76
:parameters ()
:precondition
(and
(Flag382-)
(Flag382prime-)
)
:effect
(and
(Flag425-)
(Flag425prime-)
)

)
(:action Flag425Action-77
:parameters ()
:precondition
(and
(Flag372-)
(Flag372prime-)
)
:effect
(and
(Flag425-)
(Flag425prime-)
)

)
(:action Flag425Action-78
:parameters ()
:precondition
(and
(Flag419-)
(Flag419prime-)
)
:effect
(and
(Flag425-)
(Flag425prime-)
)

)
(:action Flag425Action-79
:parameters ()
:precondition
(and
(Flag420-)
(Flag420prime-)
)
:effect
(and
(Flag425-)
(Flag425prime-)
)

)
(:action Flag425Action-80
:parameters ()
:precondition
(and
(Flag421-)
(Flag421prime-)
)
:effect
(and
(Flag425-)
(Flag425prime-)
)

)
(:action Flag425Action-81
:parameters ()
:precondition
(and
(Flag353-)
(Flag353prime-)
)
:effect
(and
(Flag425-)
(Flag425prime-)
)

)
(:action Flag425Action-82
:parameters ()
:precondition
(and
(Flag405-)
(Flag405prime-)
)
:effect
(and
(Flag425-)
(Flag425prime-)
)

)
(:action Flag425Action-83
:parameters ()
:precondition
(and
(Flag373-)
(Flag373prime-)
)
:effect
(and
(Flag425-)
(Flag425prime-)
)

)
(:action Flag425Action-84
:parameters ()
:precondition
(and
(Flag422-)
(Flag422prime-)
)
:effect
(and
(Flag425-)
(Flag425prime-)
)

)
(:action Flag425Action-85
:parameters ()
:precondition
(and
(Flag406-)
(Flag406prime-)
)
:effect
(and
(Flag425-)
(Flag425prime-)
)

)
(:action Flag425Action-86
:parameters ()
:precondition
(and
(Flag314-)
(Flag314prime-)
)
:effect
(and
(Flag425-)
(Flag425prime-)
)

)
(:action Flag425Action-87
:parameters ()
:precondition
(and
(Flag423-)
(Flag423prime-)
)
:effect
(and
(Flag425-)
(Flag425prime-)
)

)
(:action Flag425Action-88
:parameters ()
:precondition
(and
(Flag306-)
(Flag306prime-)
)
:effect
(and
(Flag425-)
(Flag425prime-)
)

)
(:action Flag425Action-89
:parameters ()
:precondition
(and
(Flag362-)
(Flag362prime-)
)
:effect
(and
(Flag425-)
(Flag425prime-)
)

)
(:action Flag425Action-90
:parameters ()
:precondition
(and
(Flag407-)
(Flag407prime-)
)
:effect
(and
(Flag425-)
(Flag425prime-)
)

)
(:action Flag425Action-91
:parameters ()
:precondition
(and
(Flag364-)
(Flag364prime-)
)
:effect
(and
(Flag425-)
(Flag425prime-)
)

)
(:action Flag425Action-92
:parameters ()
:precondition
(and
(Flag390-)
(Flag390prime-)
)
:effect
(and
(Flag425-)
(Flag425prime-)
)

)
(:action Flag425Action-93
:parameters ()
:precondition
(and
(Flag338-)
(Flag338prime-)
)
:effect
(and
(Flag425-)
(Flag425prime-)
)

)
(:action Flag425Action-94
:parameters ()
:precondition
(and
(Flag324-)
(Flag324prime-)
)
:effect
(and
(Flag425-)
(Flag425prime-)
)

)
(:action Flag425Action-95
:parameters ()
:precondition
(and
(Flag424-)
(Flag424prime-)
)
:effect
(and
(Flag425-)
(Flag425prime-)
)

)
(:action Flag425Action-96
:parameters ()
:precondition
(and
(Flag351-)
(Flag351prime-)
)
:effect
(and
(Flag425-)
(Flag425prime-)
)

)
(:action Prim425Action
:parameters ()
:precondition
(and
(not (Flag409-))
(Flag409prime-)
(not (Flag386-))
(Flag386prime-)
(not (Flag302-))
(Flag302prime-)
(not (Flag305-))
(Flag305prime-)
(not (Flag378-))
(Flag378prime-)
(not (Flag307-))
(Flag307prime-)
(not (Flag309-))
(Flag309prime-)
(not (Flag342-))
(Flag342prime-)
(not (Flag359-))
(Flag359prime-)
(not (Flag343-))
(Flag343prime-)
(not (Flag380-))
(Flag380prime-)
(not (Flag388-))
(Flag388prime-)
(not (Flag322-))
(Flag322prime-)
(not (Flag393-))
(Flag393prime-)
(not (Flag410-))
(Flag410prime-)
(not (Flag411-))
(Flag411prime-)
(not (Flag318-))
(Flag318prime-)
(not (Flag344-))
(Flag344prime-)
(not (Flag349-))
(Flag349prime-)
(not (Flag368-))
(Flag368prime-)
(not (Flag377-))
(Flag377prime-)
(not (Flag326-))
(Flag326prime-)
(not (Flag300-))
(Flag300prime-)
(not (Flag385-))
(Flag385prime-)
(not (Flag412-))
(Flag412prime-)
(not (Flag387-))
(Flag387prime-)
(not (Flag319-))
(Flag319prime-)
(not (Flag354-))
(Flag354prime-)
(not (Flag308-))
(Flag308prime-)
(not (Flag357-))
(Flag357prime-)
(not (Flag395-))
(Flag395prime-)
(not (Flag413-))
(Flag413prime-)
(not (Flag339-))
(Flag339prime-)
(not (Flag396-))
(Flag396prime-)
(not (Flag397-))
(Flag397prime-)
(not (Flag414-))
(Flag414prime-)
(not (Flag341-))
(Flag341prime-)
(not (Flag381-))
(Flag381prime-)
(not (Flag312-))
(Flag312prime-)
(not (Flag415-))
(Flag415prime-)
(not (Flag363-))
(Flag363prime-)
(not (Flag321-))
(Flag321prime-)
(not (Flag323-))
(Flag323prime-)
(not (Flag398-))
(Flag398prime-)
(not (Flag360-))
(Flag360prime-)
(not (Flag327-))
(Flag327prime-)
(not (Flag369-))
(Flag369prime-)
(not (Flag370-))
(Flag370prime-)
(not (Flag331-))
(Flag331prime-)
(not (Flag352-))
(Flag352prime-)
(not (Flag399-))
(Flag399prime-)
(not (Flag416-))
(Flag416prime-)
(not (Flag389-))
(Flag389prime-)
(not (Flag333-))
(Flag333prime-)
(not (Flag334-))
(Flag334prime-)
(not (Flag299-))
(Flag299prime-)
(not (Flag375-))
(Flag375prime-)
(not (Flag417-))
(Flag417prime-)
(not (Flag400-))
(Flag400prime-)
(not (Flag418-))
(Flag418prime-)
(not (Flag401-))
(Flag401prime-)
(not (Flag379-))
(Flag379prime-)
(not (Flag303-))
(Flag303prime-)
(not (Flag340-))
(Flag340prime-)
(not (Flag356-))
(Flag356prime-)
(not (Flag371-))
(Flag371prime-)
(not (Flag365-))
(Flag365prime-)
(not (Flag315-))
(Flag315prime-)
(not (Flag316-))
(Flag316prime-)
(not (Flag347-))
(Flag347prime-)
(not (Flag320-))
(Flag320prime-)
(not (Flag384-))
(Flag384prime-)
(not (Flag330-))
(Flag330prime-)
(not (Flag403-))
(Flag403prime-)
(not (Flag404-))
(Flag404prime-)
(not (Flag328-))
(Flag328prime-)
(not (Flag382-))
(Flag382prime-)
(not (Flag372-))
(Flag372prime-)
(not (Flag419-))
(Flag419prime-)
(not (Flag420-))
(Flag420prime-)
(not (Flag421-))
(Flag421prime-)
(not (Flag353-))
(Flag353prime-)
(not (Flag405-))
(Flag405prime-)
(not (Flag373-))
(Flag373prime-)
(not (Flag422-))
(Flag422prime-)
(not (Flag406-))
(Flag406prime-)
(not (Flag314-))
(Flag314prime-)
(not (Flag423-))
(Flag423prime-)
(not (Flag306-))
(Flag306prime-)
(not (Flag362-))
(Flag362prime-)
(not (Flag407-))
(Flag407prime-)
(not (Flag364-))
(Flag364prime-)
(not (Flag390-))
(Flag390prime-)
(not (Flag338-))
(Flag338prime-)
(not (Flag324-))
(Flag324prime-)
(not (Flag424-))
(Flag424prime-)
(not (Flag351-))
(Flag351prime-)
)
:effect
(and
(Flag425prime-)
(not (Flag425-))
)

)
(:action Flag439Action-0
:parameters ()
:precondition
(and
(Flag386-)
(Flag386prime-)
)
:effect
(and
(Flag439-)
(Flag439prime-)
)

)
(:action Flag439Action-1
:parameters ()
:precondition
(and
(Flag302-)
(Flag302prime-)
)
:effect
(and
(Flag439-)
(Flag439prime-)
)

)
(:action Flag439Action-2
:parameters ()
:precondition
(and
(Flag305-)
(Flag305prime-)
)
:effect
(and
(Flag439-)
(Flag439prime-)
)

)
(:action Flag439Action-3
:parameters ()
:precondition
(and
(Flag378-)
(Flag378prime-)
)
:effect
(and
(Flag439-)
(Flag439prime-)
)

)
(:action Flag439Action-4
:parameters ()
:precondition
(and
(Flag307-)
(Flag307prime-)
)
:effect
(and
(Flag439-)
(Flag439prime-)
)

)
(:action Flag439Action-5
:parameters ()
:precondition
(and
(Flag309-)
(Flag309prime-)
)
:effect
(and
(Flag439-)
(Flag439prime-)
)

)
(:action Flag439Action-6
:parameters ()
:precondition
(and
(Flag417-)
(Flag417prime-)
)
:effect
(and
(Flag439-)
(Flag439prime-)
)

)
(:action Flag439Action-7
:parameters ()
:precondition
(and
(Flag359-)
(Flag359prime-)
)
:effect
(and
(Flag439-)
(Flag439prime-)
)

)
(:action Flag439Action-8
:parameters ()
:precondition
(and
(Flag343-)
(Flag343prime-)
)
:effect
(and
(Flag439-)
(Flag439prime-)
)

)
(:action Flag439Action-9
:parameters ()
:precondition
(and
(Flag380-)
(Flag380prime-)
)
:effect
(and
(Flag439-)
(Flag439prime-)
)

)
(:action Flag439Action-10
:parameters ()
:precondition
(and
(Flag388-)
(Flag388prime-)
)
:effect
(and
(Flag439-)
(Flag439prime-)
)

)
(:action Flag439Action-11
:parameters ()
:precondition
(and
(Flag322-)
(Flag322prime-)
)
:effect
(and
(Flag439-)
(Flag439prime-)
)

)
(:action Flag439Action-12
:parameters ()
:precondition
(and
(Flag393-)
(Flag393prime-)
)
:effect
(and
(Flag439-)
(Flag439prime-)
)

)
(:action Flag439Action-13
:parameters ()
:precondition
(and
(Flag410-)
(Flag410prime-)
)
:effect
(and
(Flag439-)
(Flag439prime-)
)

)
(:action Flag439Action-14
:parameters ()
:precondition
(and
(Flag411-)
(Flag411prime-)
)
:effect
(and
(Flag439-)
(Flag439prime-)
)

)
(:action Flag439Action-15
:parameters ()
:precondition
(and
(Flag318-)
(Flag318prime-)
)
:effect
(and
(Flag439-)
(Flag439prime-)
)

)
(:action Flag439Action-16
:parameters ()
:precondition
(and
(Flag344-)
(Flag344prime-)
)
:effect
(and
(Flag439-)
(Flag439prime-)
)

)
(:action Flag439Action-17
:parameters ()
:precondition
(and
(Flag349-)
(Flag349prime-)
)
:effect
(and
(Flag439-)
(Flag439prime-)
)

)
(:action Flag439Action-18
:parameters ()
:precondition
(and
(Flag368-)
(Flag368prime-)
)
:effect
(and
(Flag439-)
(Flag439prime-)
)

)
(:action Flag439Action-19
:parameters ()
:precondition
(and
(Flag377-)
(Flag377prime-)
)
:effect
(and
(Flag439-)
(Flag439prime-)
)

)
(:action Flag439Action-20
:parameters ()
:precondition
(and
(Flag326-)
(Flag326prime-)
)
:effect
(and
(Flag439-)
(Flag439prime-)
)

)
(:action Flag439Action-21
:parameters ()
:precondition
(and
(Flag300-)
(Flag300prime-)
)
:effect
(and
(Flag439-)
(Flag439prime-)
)

)
(:action Flag439Action-22
:parameters ()
:precondition
(and
(Flag385-)
(Flag385prime-)
)
:effect
(and
(Flag439-)
(Flag439prime-)
)

)
(:action Flag439Action-23
:parameters ()
:precondition
(and
(Flag387-)
(Flag387prime-)
)
:effect
(and
(Flag439-)
(Flag439prime-)
)

)
(:action Flag439Action-24
:parameters ()
:precondition
(and
(Flag319-)
(Flag319prime-)
)
:effect
(and
(Flag439-)
(Flag439prime-)
)

)
(:action Flag439Action-25
:parameters ()
:precondition
(and
(Flag354-)
(Flag354prime-)
)
:effect
(and
(Flag439-)
(Flag439prime-)
)

)
(:action Flag439Action-26
:parameters ()
:precondition
(and
(Flag426-)
(Flag426prime-)
)
:effect
(and
(Flag439-)
(Flag439prime-)
)

)
(:action Flag439Action-27
:parameters ()
:precondition
(and
(Flag427-)
(Flag427prime-)
)
:effect
(and
(Flag439-)
(Flag439prime-)
)

)
(:action Flag439Action-28
:parameters ()
:precondition
(and
(Flag308-)
(Flag308prime-)
)
:effect
(and
(Flag439-)
(Flag439prime-)
)

)
(:action Flag439Action-29
:parameters ()
:precondition
(and
(Flag428-)
(Flag428prime-)
)
:effect
(and
(Flag439-)
(Flag439prime-)
)

)
(:action Flag439Action-30
:parameters ()
:precondition
(and
(Flag357-)
(Flag357prime-)
)
:effect
(and
(Flag439-)
(Flag439prime-)
)

)
(:action Flag439Action-31
:parameters ()
:precondition
(and
(Flag395-)
(Flag395prime-)
)
:effect
(and
(Flag439-)
(Flag439prime-)
)

)
(:action Flag439Action-32
:parameters ()
:precondition
(and
(Flag429-)
(Flag429prime-)
)
:effect
(and
(Flag439-)
(Flag439prime-)
)

)
(:action Flag439Action-33
:parameters ()
:precondition
(and
(Flag413-)
(Flag413prime-)
)
:effect
(and
(Flag439-)
(Flag439prime-)
)

)
(:action Flag439Action-34
:parameters ()
:precondition
(and
(Flag339-)
(Flag339prime-)
)
:effect
(and
(Flag439-)
(Flag439prime-)
)

)
(:action Flag439Action-35
:parameters ()
:precondition
(and
(Flag396-)
(Flag396prime-)
)
:effect
(and
(Flag439-)
(Flag439prime-)
)

)
(:action Flag439Action-36
:parameters ()
:precondition
(and
(Flag397-)
(Flag397prime-)
)
:effect
(and
(Flag439-)
(Flag439prime-)
)

)
(:action Flag439Action-37
:parameters ()
:precondition
(and
(Flag414-)
(Flag414prime-)
)
:effect
(and
(Flag439-)
(Flag439prime-)
)

)
(:action Flag439Action-38
:parameters ()
:precondition
(and
(Flag341-)
(Flag341prime-)
)
:effect
(and
(Flag439-)
(Flag439prime-)
)

)
(:action Flag439Action-39
:parameters ()
:precondition
(and
(Flag430-)
(Flag430prime-)
)
:effect
(and
(Flag439-)
(Flag439prime-)
)

)
(:action Flag439Action-40
:parameters ()
:precondition
(and
(Flag381-)
(Flag381prime-)
)
:effect
(and
(Flag439-)
(Flag439prime-)
)

)
(:action Flag439Action-41
:parameters ()
:precondition
(and
(Flag431-)
(Flag431prime-)
)
:effect
(and
(Flag439-)
(Flag439prime-)
)

)
(:action Flag439Action-42
:parameters ()
:precondition
(and
(Flag415-)
(Flag415prime-)
)
:effect
(and
(Flag439-)
(Flag439prime-)
)

)
(:action Flag439Action-43
:parameters ()
:precondition
(and
(Flag363-)
(Flag363prime-)
)
:effect
(and
(Flag439-)
(Flag439prime-)
)

)
(:action Flag439Action-44
:parameters ()
:precondition
(and
(Flag321-)
(Flag321prime-)
)
:effect
(and
(Flag439-)
(Flag439prime-)
)

)
(:action Flag439Action-45
:parameters ()
:precondition
(and
(Flag432-)
(Flag432prime-)
)
:effect
(and
(Flag439-)
(Flag439prime-)
)

)
(:action Flag439Action-46
:parameters ()
:precondition
(and
(Flag323-)
(Flag323prime-)
)
:effect
(and
(Flag439-)
(Flag439prime-)
)

)
(:action Flag439Action-47
:parameters ()
:precondition
(and
(Flag398-)
(Flag398prime-)
)
:effect
(and
(Flag439-)
(Flag439prime-)
)

)
(:action Flag439Action-48
:parameters ()
:precondition
(and
(Flag327-)
(Flag327prime-)
)
:effect
(and
(Flag439-)
(Flag439prime-)
)

)
(:action Flag439Action-49
:parameters ()
:precondition
(and
(Flag369-)
(Flag369prime-)
)
:effect
(and
(Flag439-)
(Flag439prime-)
)

)
(:action Flag439Action-50
:parameters ()
:precondition
(and
(Flag370-)
(Flag370prime-)
)
:effect
(and
(Flag439-)
(Flag439prime-)
)

)
(:action Flag439Action-51
:parameters ()
:precondition
(and
(Flag331-)
(Flag331prime-)
)
:effect
(and
(Flag439-)
(Flag439prime-)
)

)
(:action Flag439Action-52
:parameters ()
:precondition
(and
(Flag352-)
(Flag352prime-)
)
:effect
(and
(Flag439-)
(Flag439prime-)
)

)
(:action Flag439Action-53
:parameters ()
:precondition
(and
(Flag399-)
(Flag399prime-)
)
:effect
(and
(Flag439-)
(Flag439prime-)
)

)
(:action Flag439Action-54
:parameters ()
:precondition
(and
(Flag433-)
(Flag433prime-)
)
:effect
(and
(Flag439-)
(Flag439prime-)
)

)
(:action Flag439Action-55
:parameters ()
:precondition
(and
(Flag416-)
(Flag416prime-)
)
:effect
(and
(Flag439-)
(Flag439prime-)
)

)
(:action Flag439Action-56
:parameters ()
:precondition
(and
(Flag333-)
(Flag333prime-)
)
:effect
(and
(Flag439-)
(Flag439prime-)
)

)
(:action Flag439Action-57
:parameters ()
:precondition
(and
(Flag334-)
(Flag334prime-)
)
:effect
(and
(Flag439-)
(Flag439prime-)
)

)
(:action Flag439Action-58
:parameters ()
:precondition
(and
(Flag299-)
(Flag299prime-)
)
:effect
(and
(Flag439-)
(Flag439prime-)
)

)
(:action Flag439Action-59
:parameters ()
:precondition
(and
(Flag375-)
(Flag375prime-)
)
:effect
(and
(Flag439-)
(Flag439prime-)
)

)
(:action Flag439Action-60
:parameters ()
:precondition
(and
(Flag342-)
(Flag342prime-)
)
:effect
(and
(Flag439-)
(Flag439prime-)
)

)
(:action Flag439Action-61
:parameters ()
:precondition
(and
(Flag400-)
(Flag400prime-)
)
:effect
(and
(Flag439-)
(Flag439prime-)
)

)
(:action Flag439Action-62
:parameters ()
:precondition
(and
(Flag418-)
(Flag418prime-)
)
:effect
(and
(Flag439-)
(Flag439prime-)
)

)
(:action Flag439Action-63
:parameters ()
:precondition
(and
(Flag401-)
(Flag401prime-)
)
:effect
(and
(Flag439-)
(Flag439prime-)
)

)
(:action Flag439Action-64
:parameters ()
:precondition
(and
(Flag379-)
(Flag379prime-)
)
:effect
(and
(Flag439-)
(Flag439prime-)
)

)
(:action Flag439Action-65
:parameters ()
:precondition
(and
(Flag303-)
(Flag303prime-)
)
:effect
(and
(Flag439-)
(Flag439prime-)
)

)
(:action Flag439Action-66
:parameters ()
:precondition
(and
(Flag340-)
(Flag340prime-)
)
:effect
(and
(Flag439-)
(Flag439prime-)
)

)
(:action Flag439Action-67
:parameters ()
:precondition
(and
(Flag356-)
(Flag356prime-)
)
:effect
(and
(Flag439-)
(Flag439prime-)
)

)
(:action Flag439Action-68
:parameters ()
:precondition
(and
(Flag371-)
(Flag371prime-)
)
:effect
(and
(Flag439-)
(Flag439prime-)
)

)
(:action Flag439Action-69
:parameters ()
:precondition
(and
(Flag365-)
(Flag365prime-)
)
:effect
(and
(Flag439-)
(Flag439prime-)
)

)
(:action Flag439Action-70
:parameters ()
:precondition
(and
(Flag315-)
(Flag315prime-)
)
:effect
(and
(Flag439-)
(Flag439prime-)
)

)
(:action Flag439Action-71
:parameters ()
:precondition
(and
(Flag312-)
(Flag312prime-)
)
:effect
(and
(Flag439-)
(Flag439prime-)
)

)
(:action Flag439Action-72
:parameters ()
:precondition
(and
(Flag434-)
(Flag434prime-)
)
:effect
(and
(Flag439-)
(Flag439prime-)
)

)
(:action Flag439Action-73
:parameters ()
:precondition
(and
(Flag347-)
(Flag347prime-)
)
:effect
(and
(Flag439-)
(Flag439prime-)
)

)
(:action Flag439Action-74
:parameters ()
:precondition
(and
(Flag435-)
(Flag435prime-)
)
:effect
(and
(Flag439-)
(Flag439prime-)
)

)
(:action Flag439Action-75
:parameters ()
:precondition
(and
(Flag330-)
(Flag330prime-)
)
:effect
(and
(Flag439-)
(Flag439prime-)
)

)
(:action Flag439Action-76
:parameters ()
:precondition
(and
(Flag403-)
(Flag403prime-)
)
:effect
(and
(Flag439-)
(Flag439prime-)
)

)
(:action Flag439Action-77
:parameters ()
:precondition
(and
(Flag404-)
(Flag404prime-)
)
:effect
(and
(Flag439-)
(Flag439prime-)
)

)
(:action Flag439Action-78
:parameters ()
:precondition
(and
(Flag328-)
(Flag328prime-)
)
:effect
(and
(Flag439-)
(Flag439prime-)
)

)
(:action Flag439Action-79
:parameters ()
:precondition
(and
(Flag382-)
(Flag382prime-)
)
:effect
(and
(Flag439-)
(Flag439prime-)
)

)
(:action Flag439Action-80
:parameters ()
:precondition
(and
(Flag372-)
(Flag372prime-)
)
:effect
(and
(Flag439-)
(Flag439prime-)
)

)
(:action Flag439Action-81
:parameters ()
:precondition
(and
(Flag419-)
(Flag419prime-)
)
:effect
(and
(Flag439-)
(Flag439prime-)
)

)
(:action Flag439Action-82
:parameters ()
:precondition
(and
(Flag420-)
(Flag420prime-)
)
:effect
(and
(Flag439-)
(Flag439prime-)
)

)
(:action Flag439Action-83
:parameters ()
:precondition
(and
(Flag421-)
(Flag421prime-)
)
:effect
(and
(Flag439-)
(Flag439prime-)
)

)
(:action Flag439Action-84
:parameters ()
:precondition
(and
(Flag353-)
(Flag353prime-)
)
:effect
(and
(Flag439-)
(Flag439prime-)
)

)
(:action Flag439Action-85
:parameters ()
:precondition
(and
(Flag405-)
(Flag405prime-)
)
:effect
(and
(Flag439-)
(Flag439prime-)
)

)
(:action Flag439Action-86
:parameters ()
:precondition
(and
(Flag436-)
(Flag436prime-)
)
:effect
(and
(Flag439-)
(Flag439prime-)
)

)
(:action Flag439Action-87
:parameters ()
:precondition
(and
(Flag373-)
(Flag373prime-)
)
:effect
(and
(Flag439-)
(Flag439prime-)
)

)
(:action Flag439Action-88
:parameters ()
:precondition
(and
(Flag422-)
(Flag422prime-)
)
:effect
(and
(Flag439-)
(Flag439prime-)
)

)
(:action Flag439Action-89
:parameters ()
:precondition
(and
(Flag406-)
(Flag406prime-)
)
:effect
(and
(Flag439-)
(Flag439prime-)
)

)
(:action Flag439Action-90
:parameters ()
:precondition
(and
(Flag314-)
(Flag314prime-)
)
:effect
(and
(Flag439-)
(Flag439prime-)
)

)
(:action Flag439Action-91
:parameters ()
:precondition
(and
(Flag306-)
(Flag306prime-)
)
:effect
(and
(Flag439-)
(Flag439prime-)
)

)
(:action Flag439Action-92
:parameters ()
:precondition
(and
(Flag362-)
(Flag362prime-)
)
:effect
(and
(Flag439-)
(Flag439prime-)
)

)
(:action Flag439Action-93
:parameters ()
:precondition
(and
(Flag407-)
(Flag407prime-)
)
:effect
(and
(Flag439-)
(Flag439prime-)
)

)
(:action Flag439Action-94
:parameters ()
:precondition
(and
(Flag364-)
(Flag364prime-)
)
:effect
(and
(Flag439-)
(Flag439prime-)
)

)
(:action Flag439Action-95
:parameters ()
:precondition
(and
(Flag437-)
(Flag437prime-)
)
:effect
(and
(Flag439-)
(Flag439prime-)
)

)
(:action Flag439Action-96
:parameters ()
:precondition
(and
(Flag390-)
(Flag390prime-)
)
:effect
(and
(Flag439-)
(Flag439prime-)
)

)
(:action Flag439Action-97
:parameters ()
:precondition
(and
(Flag324-)
(Flag324prime-)
)
:effect
(and
(Flag439-)
(Flag439prime-)
)

)
(:action Flag439Action-98
:parameters ()
:precondition
(and
(Flag438-)
(Flag438prime-)
)
:effect
(and
(Flag439-)
(Flag439prime-)
)

)
(:action Flag439Action-99
:parameters ()
:precondition
(and
(Flag424-)
(Flag424prime-)
)
:effect
(and
(Flag439-)
(Flag439prime-)
)

)
(:action Flag439Action-100
:parameters ()
:precondition
(and
(Flag384-)
(Flag384prime-)
)
:effect
(and
(Flag439-)
(Flag439prime-)
)

)
(:action Flag439Action-101
:parameters ()
:precondition
(and
(Flag351-)
(Flag351prime-)
)
:effect
(and
(Flag439-)
(Flag439prime-)
)

)
(:action Prim439Action
:parameters ()
:precondition
(and
(not (Flag386-))
(Flag386prime-)
(not (Flag302-))
(Flag302prime-)
(not (Flag305-))
(Flag305prime-)
(not (Flag378-))
(Flag378prime-)
(not (Flag307-))
(Flag307prime-)
(not (Flag309-))
(Flag309prime-)
(not (Flag417-))
(Flag417prime-)
(not (Flag359-))
(Flag359prime-)
(not (Flag343-))
(Flag343prime-)
(not (Flag380-))
(Flag380prime-)
(not (Flag388-))
(Flag388prime-)
(not (Flag322-))
(Flag322prime-)
(not (Flag393-))
(Flag393prime-)
(not (Flag410-))
(Flag410prime-)
(not (Flag411-))
(Flag411prime-)
(not (Flag318-))
(Flag318prime-)
(not (Flag344-))
(Flag344prime-)
(not (Flag349-))
(Flag349prime-)
(not (Flag368-))
(Flag368prime-)
(not (Flag377-))
(Flag377prime-)
(not (Flag326-))
(Flag326prime-)
(not (Flag300-))
(Flag300prime-)
(not (Flag385-))
(Flag385prime-)
(not (Flag387-))
(Flag387prime-)
(not (Flag319-))
(Flag319prime-)
(not (Flag354-))
(Flag354prime-)
(not (Flag426-))
(Flag426prime-)
(not (Flag427-))
(Flag427prime-)
(not (Flag308-))
(Flag308prime-)
(not (Flag428-))
(Flag428prime-)
(not (Flag357-))
(Flag357prime-)
(not (Flag395-))
(Flag395prime-)
(not (Flag429-))
(Flag429prime-)
(not (Flag413-))
(Flag413prime-)
(not (Flag339-))
(Flag339prime-)
(not (Flag396-))
(Flag396prime-)
(not (Flag397-))
(Flag397prime-)
(not (Flag414-))
(Flag414prime-)
(not (Flag341-))
(Flag341prime-)
(not (Flag430-))
(Flag430prime-)
(not (Flag381-))
(Flag381prime-)
(not (Flag431-))
(Flag431prime-)
(not (Flag415-))
(Flag415prime-)
(not (Flag363-))
(Flag363prime-)
(not (Flag321-))
(Flag321prime-)
(not (Flag432-))
(Flag432prime-)
(not (Flag323-))
(Flag323prime-)
(not (Flag398-))
(Flag398prime-)
(not (Flag327-))
(Flag327prime-)
(not (Flag369-))
(Flag369prime-)
(not (Flag370-))
(Flag370prime-)
(not (Flag331-))
(Flag331prime-)
(not (Flag352-))
(Flag352prime-)
(not (Flag399-))
(Flag399prime-)
(not (Flag433-))
(Flag433prime-)
(not (Flag416-))
(Flag416prime-)
(not (Flag333-))
(Flag333prime-)
(not (Flag334-))
(Flag334prime-)
(not (Flag299-))
(Flag299prime-)
(not (Flag375-))
(Flag375prime-)
(not (Flag342-))
(Flag342prime-)
(not (Flag400-))
(Flag400prime-)
(not (Flag418-))
(Flag418prime-)
(not (Flag401-))
(Flag401prime-)
(not (Flag379-))
(Flag379prime-)
(not (Flag303-))
(Flag303prime-)
(not (Flag340-))
(Flag340prime-)
(not (Flag356-))
(Flag356prime-)
(not (Flag371-))
(Flag371prime-)
(not (Flag365-))
(Flag365prime-)
(not (Flag315-))
(Flag315prime-)
(not (Flag312-))
(Flag312prime-)
(not (Flag434-))
(Flag434prime-)
(not (Flag347-))
(Flag347prime-)
(not (Flag435-))
(Flag435prime-)
(not (Flag330-))
(Flag330prime-)
(not (Flag403-))
(Flag403prime-)
(not (Flag404-))
(Flag404prime-)
(not (Flag328-))
(Flag328prime-)
(not (Flag382-))
(Flag382prime-)
(not (Flag372-))
(Flag372prime-)
(not (Flag419-))
(Flag419prime-)
(not (Flag420-))
(Flag420prime-)
(not (Flag421-))
(Flag421prime-)
(not (Flag353-))
(Flag353prime-)
(not (Flag405-))
(Flag405prime-)
(not (Flag436-))
(Flag436prime-)
(not (Flag373-))
(Flag373prime-)
(not (Flag422-))
(Flag422prime-)
(not (Flag406-))
(Flag406prime-)
(not (Flag314-))
(Flag314prime-)
(not (Flag306-))
(Flag306prime-)
(not (Flag362-))
(Flag362prime-)
(not (Flag407-))
(Flag407prime-)
(not (Flag364-))
(Flag364prime-)
(not (Flag437-))
(Flag437prime-)
(not (Flag390-))
(Flag390prime-)
(not (Flag324-))
(Flag324prime-)
(not (Flag438-))
(Flag438prime-)
(not (Flag424-))
(Flag424prime-)
(not (Flag384-))
(Flag384prime-)
(not (Flag351-))
(Flag351prime-)
)
:effect
(and
(Flag439prime-)
(not (Flag439-))
)

)
(:action Flag453Action-0
:parameters ()
:precondition
(and
(Flag386-)
(Flag386prime-)
)
:effect
(and
(Flag453-)
(Flag453prime-)
)

)
(:action Flag453Action-1
:parameters ()
:precondition
(and
(Flag302-)
(Flag302prime-)
)
:effect
(and
(Flag453-)
(Flag453prime-)
)

)
(:action Flag453Action-2
:parameters ()
:precondition
(and
(Flag378-)
(Flag378prime-)
)
:effect
(and
(Flag453-)
(Flag453prime-)
)

)
(:action Flag453Action-3
:parameters ()
:precondition
(and
(Flag307-)
(Flag307prime-)
)
:effect
(and
(Flag453-)
(Flag453prime-)
)

)
(:action Flag453Action-4
:parameters ()
:precondition
(and
(Flag417-)
(Flag417prime-)
)
:effect
(and
(Flag453-)
(Flag453prime-)
)

)
(:action Flag453Action-5
:parameters ()
:precondition
(and
(Flag359-)
(Flag359prime-)
)
:effect
(and
(Flag453-)
(Flag453prime-)
)

)
(:action Flag453Action-6
:parameters ()
:precondition
(and
(Flag343-)
(Flag343prime-)
)
:effect
(and
(Flag453-)
(Flag453prime-)
)

)
(:action Flag453Action-7
:parameters ()
:precondition
(and
(Flag380-)
(Flag380prime-)
)
:effect
(and
(Flag453-)
(Flag453prime-)
)

)
(:action Flag453Action-8
:parameters ()
:precondition
(and
(Flag388-)
(Flag388prime-)
)
:effect
(and
(Flag453-)
(Flag453prime-)
)

)
(:action Flag453Action-9
:parameters ()
:precondition
(and
(Flag322-)
(Flag322prime-)
)
:effect
(and
(Flag453-)
(Flag453prime-)
)

)
(:action Flag453Action-10
:parameters ()
:precondition
(and
(Flag393-)
(Flag393prime-)
)
:effect
(and
(Flag453-)
(Flag453prime-)
)

)
(:action Flag453Action-11
:parameters ()
:precondition
(and
(Flag410-)
(Flag410prime-)
)
:effect
(and
(Flag453-)
(Flag453prime-)
)

)
(:action Flag453Action-12
:parameters ()
:precondition
(and
(Flag440-)
(Flag440prime-)
)
:effect
(and
(Flag453-)
(Flag453prime-)
)

)
(:action Flag453Action-13
:parameters ()
:precondition
(and
(Flag411-)
(Flag411prime-)
)
:effect
(and
(Flag453-)
(Flag453prime-)
)

)
(:action Flag453Action-14
:parameters ()
:precondition
(and
(Flag318-)
(Flag318prime-)
)
:effect
(and
(Flag453-)
(Flag453prime-)
)

)
(:action Flag453Action-15
:parameters ()
:precondition
(and
(Flag344-)
(Flag344prime-)
)
:effect
(and
(Flag453-)
(Flag453prime-)
)

)
(:action Flag453Action-16
:parameters ()
:precondition
(and
(Flag349-)
(Flag349prime-)
)
:effect
(and
(Flag453-)
(Flag453prime-)
)

)
(:action Flag453Action-17
:parameters ()
:precondition
(and
(Flag368-)
(Flag368prime-)
)
:effect
(and
(Flag453-)
(Flag453prime-)
)

)
(:action Flag453Action-18
:parameters ()
:precondition
(and
(Flag377-)
(Flag377prime-)
)
:effect
(and
(Flag453-)
(Flag453prime-)
)

)
(:action Flag453Action-19
:parameters ()
:precondition
(and
(Flag326-)
(Flag326prime-)
)
:effect
(and
(Flag453-)
(Flag453prime-)
)

)
(:action Flag453Action-20
:parameters ()
:precondition
(and
(Flag441-)
(Flag441prime-)
)
:effect
(and
(Flag453-)
(Flag453prime-)
)

)
(:action Flag453Action-21
:parameters ()
:precondition
(and
(Flag300-)
(Flag300prime-)
)
:effect
(and
(Flag453-)
(Flag453prime-)
)

)
(:action Flag453Action-22
:parameters ()
:precondition
(and
(Flag385-)
(Flag385prime-)
)
:effect
(and
(Flag453-)
(Flag453prime-)
)

)
(:action Flag453Action-23
:parameters ()
:precondition
(and
(Flag442-)
(Flag442prime-)
)
:effect
(and
(Flag453-)
(Flag453prime-)
)

)
(:action Flag453Action-24
:parameters ()
:precondition
(and
(Flag387-)
(Flag387prime-)
)
:effect
(and
(Flag453-)
(Flag453prime-)
)

)
(:action Flag453Action-25
:parameters ()
:precondition
(and
(Flag319-)
(Flag319prime-)
)
:effect
(and
(Flag453-)
(Flag453prime-)
)

)
(:action Flag453Action-26
:parameters ()
:precondition
(and
(Flag354-)
(Flag354prime-)
)
:effect
(and
(Flag453-)
(Flag453prime-)
)

)
(:action Flag453Action-27
:parameters ()
:precondition
(and
(Flag426-)
(Flag426prime-)
)
:effect
(and
(Flag453-)
(Flag453prime-)
)

)
(:action Flag453Action-28
:parameters ()
:precondition
(and
(Flag427-)
(Flag427prime-)
)
:effect
(and
(Flag453-)
(Flag453prime-)
)

)
(:action Flag453Action-29
:parameters ()
:precondition
(and
(Flag443-)
(Flag443prime-)
)
:effect
(and
(Flag453-)
(Flag453prime-)
)

)
(:action Flag453Action-30
:parameters ()
:precondition
(and
(Flag308-)
(Flag308prime-)
)
:effect
(and
(Flag453-)
(Flag453prime-)
)

)
(:action Flag453Action-31
:parameters ()
:precondition
(and
(Flag428-)
(Flag428prime-)
)
:effect
(and
(Flag453-)
(Flag453prime-)
)

)
(:action Flag453Action-32
:parameters ()
:precondition
(and
(Flag357-)
(Flag357prime-)
)
:effect
(and
(Flag453-)
(Flag453prime-)
)

)
(:action Flag453Action-33
:parameters ()
:precondition
(and
(Flag395-)
(Flag395prime-)
)
:effect
(and
(Flag453-)
(Flag453prime-)
)

)
(:action Flag453Action-34
:parameters ()
:precondition
(and
(Flag429-)
(Flag429prime-)
)
:effect
(and
(Flag453-)
(Flag453prime-)
)

)
(:action Flag453Action-35
:parameters ()
:precondition
(and
(Flag413-)
(Flag413prime-)
)
:effect
(and
(Flag453-)
(Flag453prime-)
)

)
(:action Flag453Action-36
:parameters ()
:precondition
(and
(Flag444-)
(Flag444prime-)
)
:effect
(and
(Flag453-)
(Flag453prime-)
)

)
(:action Flag453Action-37
:parameters ()
:precondition
(and
(Flag396-)
(Flag396prime-)
)
:effect
(and
(Flag453-)
(Flag453prime-)
)

)
(:action Flag453Action-38
:parameters ()
:precondition
(and
(Flag397-)
(Flag397prime-)
)
:effect
(and
(Flag453-)
(Flag453prime-)
)

)
(:action Flag453Action-39
:parameters ()
:precondition
(and
(Flag414-)
(Flag414prime-)
)
:effect
(and
(Flag453-)
(Flag453prime-)
)

)
(:action Flag453Action-40
:parameters ()
:precondition
(and
(Flag341-)
(Flag341prime-)
)
:effect
(and
(Flag453-)
(Flag453prime-)
)

)
(:action Flag453Action-41
:parameters ()
:precondition
(and
(Flag445-)
(Flag445prime-)
)
:effect
(and
(Flag453-)
(Flag453prime-)
)

)
(:action Flag453Action-42
:parameters ()
:precondition
(and
(Flag430-)
(Flag430prime-)
)
:effect
(and
(Flag453-)
(Flag453prime-)
)

)
(:action Flag453Action-43
:parameters ()
:precondition
(and
(Flag381-)
(Flag381prime-)
)
:effect
(and
(Flag453-)
(Flag453prime-)
)

)
(:action Flag453Action-44
:parameters ()
:precondition
(and
(Flag431-)
(Flag431prime-)
)
:effect
(and
(Flag453-)
(Flag453prime-)
)

)
(:action Flag453Action-45
:parameters ()
:precondition
(and
(Flag405-)
(Flag405prime-)
)
:effect
(and
(Flag453-)
(Flag453prime-)
)

)
(:action Flag453Action-46
:parameters ()
:precondition
(and
(Flag415-)
(Flag415prime-)
)
:effect
(and
(Flag453-)
(Flag453prime-)
)

)
(:action Flag453Action-47
:parameters ()
:precondition
(and
(Flag363-)
(Flag363prime-)
)
:effect
(and
(Flag453-)
(Flag453prime-)
)

)
(:action Flag453Action-48
:parameters ()
:precondition
(and
(Flag321-)
(Flag321prime-)
)
:effect
(and
(Flag453-)
(Flag453prime-)
)

)
(:action Flag453Action-49
:parameters ()
:precondition
(and
(Flag432-)
(Flag432prime-)
)
:effect
(and
(Flag453-)
(Flag453prime-)
)

)
(:action Flag453Action-50
:parameters ()
:precondition
(and
(Flag323-)
(Flag323prime-)
)
:effect
(and
(Flag453-)
(Flag453prime-)
)

)
(:action Flag453Action-51
:parameters ()
:precondition
(and
(Flag398-)
(Flag398prime-)
)
:effect
(and
(Flag453-)
(Flag453prime-)
)

)
(:action Flag453Action-52
:parameters ()
:precondition
(and
(Flag340-)
(Flag340prime-)
)
:effect
(and
(Flag453-)
(Flag453prime-)
)

)
(:action Flag453Action-53
:parameters ()
:precondition
(and
(Flag327-)
(Flag327prime-)
)
:effect
(and
(Flag453-)
(Flag453prime-)
)

)
(:action Flag453Action-54
:parameters ()
:precondition
(and
(Flag369-)
(Flag369prime-)
)
:effect
(and
(Flag453-)
(Flag453prime-)
)

)
(:action Flag453Action-55
:parameters ()
:precondition
(and
(Flag370-)
(Flag370prime-)
)
:effect
(and
(Flag453-)
(Flag453prime-)
)

)
(:action Flag453Action-56
:parameters ()
:precondition
(and
(Flag331-)
(Flag331prime-)
)
:effect
(and
(Flag453-)
(Flag453prime-)
)

)
(:action Flag453Action-57
:parameters ()
:precondition
(and
(Flag352-)
(Flag352prime-)
)
:effect
(and
(Flag453-)
(Flag453prime-)
)

)
(:action Flag453Action-58
:parameters ()
:precondition
(and
(Flag399-)
(Flag399prime-)
)
:effect
(and
(Flag453-)
(Flag453prime-)
)

)
(:action Flag453Action-59
:parameters ()
:precondition
(and
(Flag416-)
(Flag416prime-)
)
:effect
(and
(Flag453-)
(Flag453prime-)
)

)
(:action Flag453Action-60
:parameters ()
:precondition
(and
(Flag446-)
(Flag446prime-)
)
:effect
(and
(Flag453-)
(Flag453prime-)
)

)
(:action Flag453Action-61
:parameters ()
:precondition
(and
(Flag333-)
(Flag333prime-)
)
:effect
(and
(Flag453-)
(Flag453prime-)
)

)
(:action Flag453Action-62
:parameters ()
:precondition
(and
(Flag334-)
(Flag334prime-)
)
:effect
(and
(Flag453-)
(Flag453prime-)
)

)
(:action Flag453Action-63
:parameters ()
:precondition
(and
(Flag299-)
(Flag299prime-)
)
:effect
(and
(Flag453-)
(Flag453prime-)
)

)
(:action Flag453Action-64
:parameters ()
:precondition
(and
(Flag375-)
(Flag375prime-)
)
:effect
(and
(Flag453-)
(Flag453prime-)
)

)
(:action Flag453Action-65
:parameters ()
:precondition
(and
(Flag342-)
(Flag342prime-)
)
:effect
(and
(Flag453-)
(Flag453prime-)
)

)
(:action Flag453Action-66
:parameters ()
:precondition
(and
(Flag400-)
(Flag400prime-)
)
:effect
(and
(Flag453-)
(Flag453prime-)
)

)
(:action Flag453Action-67
:parameters ()
:precondition
(and
(Flag418-)
(Flag418prime-)
)
:effect
(and
(Flag453-)
(Flag453prime-)
)

)
(:action Flag453Action-68
:parameters ()
:precondition
(and
(Flag401-)
(Flag401prime-)
)
:effect
(and
(Flag453-)
(Flag453prime-)
)

)
(:action Flag453Action-69
:parameters ()
:precondition
(and
(Flag447-)
(Flag447prime-)
)
:effect
(and
(Flag453-)
(Flag453prime-)
)

)
(:action Flag453Action-70
:parameters ()
:precondition
(and
(Flag379-)
(Flag379prime-)
)
:effect
(and
(Flag453-)
(Flag453prime-)
)

)
(:action Flag453Action-71
:parameters ()
:precondition
(and
(Flag303-)
(Flag303prime-)
)
:effect
(and
(Flag453-)
(Flag453prime-)
)

)
(:action Flag453Action-72
:parameters ()
:precondition
(and
(Flag448-)
(Flag448prime-)
)
:effect
(and
(Flag453-)
(Flag453prime-)
)

)
(:action Flag453Action-73
:parameters ()
:precondition
(and
(Flag356-)
(Flag356prime-)
)
:effect
(and
(Flag453-)
(Flag453prime-)
)

)
(:action Flag453Action-74
:parameters ()
:precondition
(and
(Flag365-)
(Flag365prime-)
)
:effect
(and
(Flag453-)
(Flag453prime-)
)

)
(:action Flag453Action-75
:parameters ()
:precondition
(and
(Flag315-)
(Flag315prime-)
)
:effect
(and
(Flag453-)
(Flag453prime-)
)

)
(:action Flag453Action-76
:parameters ()
:precondition
(and
(Flag312-)
(Flag312prime-)
)
:effect
(and
(Flag453-)
(Flag453prime-)
)

)
(:action Flag453Action-77
:parameters ()
:precondition
(and
(Flag347-)
(Flag347prime-)
)
:effect
(and
(Flag453-)
(Flag453prime-)
)

)
(:action Flag453Action-78
:parameters ()
:precondition
(and
(Flag330-)
(Flag330prime-)
)
:effect
(and
(Flag453-)
(Flag453prime-)
)

)
(:action Flag453Action-79
:parameters ()
:precondition
(and
(Flag403-)
(Flag403prime-)
)
:effect
(and
(Flag453-)
(Flag453prime-)
)

)
(:action Flag453Action-80
:parameters ()
:precondition
(and
(Flag404-)
(Flag404prime-)
)
:effect
(and
(Flag453-)
(Flag453prime-)
)

)
(:action Flag453Action-81
:parameters ()
:precondition
(and
(Flag328-)
(Flag328prime-)
)
:effect
(and
(Flag453-)
(Flag453prime-)
)

)
(:action Flag453Action-82
:parameters ()
:precondition
(and
(Flag382-)
(Flag382prime-)
)
:effect
(and
(Flag453-)
(Flag453prime-)
)

)
(:action Flag453Action-83
:parameters ()
:precondition
(and
(Flag372-)
(Flag372prime-)
)
:effect
(and
(Flag453-)
(Flag453prime-)
)

)
(:action Flag453Action-84
:parameters ()
:precondition
(and
(Flag419-)
(Flag419prime-)
)
:effect
(and
(Flag453-)
(Flag453prime-)
)

)
(:action Flag453Action-85
:parameters ()
:precondition
(and
(Flag420-)
(Flag420prime-)
)
:effect
(and
(Flag453-)
(Flag453prime-)
)

)
(:action Flag453Action-86
:parameters ()
:precondition
(and
(Flag421-)
(Flag421prime-)
)
:effect
(and
(Flag453-)
(Flag453prime-)
)

)
(:action Flag453Action-87
:parameters ()
:precondition
(and
(Flag353-)
(Flag353prime-)
)
:effect
(and
(Flag453-)
(Flag453prime-)
)

)
(:action Flag453Action-88
:parameters ()
:precondition
(and
(Flag449-)
(Flag449prime-)
)
:effect
(and
(Flag453-)
(Flag453prime-)
)

)
(:action Flag453Action-89
:parameters ()
:precondition
(and
(Flag436-)
(Flag436prime-)
)
:effect
(and
(Flag453-)
(Flag453prime-)
)

)
(:action Flag453Action-90
:parameters ()
:precondition
(and
(Flag373-)
(Flag373prime-)
)
:effect
(and
(Flag453-)
(Flag453prime-)
)

)
(:action Flag453Action-91
:parameters ()
:precondition
(and
(Flag422-)
(Flag422prime-)
)
:effect
(and
(Flag453-)
(Flag453prime-)
)

)
(:action Flag453Action-92
:parameters ()
:precondition
(and
(Flag406-)
(Flag406prime-)
)
:effect
(and
(Flag453-)
(Flag453prime-)
)

)
(:action Flag453Action-93
:parameters ()
:precondition
(and
(Flag314-)
(Flag314prime-)
)
:effect
(and
(Flag453-)
(Flag453prime-)
)

)
(:action Flag453Action-94
:parameters ()
:precondition
(and
(Flag306-)
(Flag306prime-)
)
:effect
(and
(Flag453-)
(Flag453prime-)
)

)
(:action Flag453Action-95
:parameters ()
:precondition
(and
(Flag362-)
(Flag362prime-)
)
:effect
(and
(Flag453-)
(Flag453prime-)
)

)
(:action Flag453Action-96
:parameters ()
:precondition
(and
(Flag364-)
(Flag364prime-)
)
:effect
(and
(Flag453-)
(Flag453prime-)
)

)
(:action Flag453Action-97
:parameters ()
:precondition
(and
(Flag437-)
(Flag437prime-)
)
:effect
(and
(Flag453-)
(Flag453prime-)
)

)
(:action Flag453Action-98
:parameters ()
:precondition
(and
(Flag390-)
(Flag390prime-)
)
:effect
(and
(Flag453-)
(Flag453prime-)
)

)
(:action Flag453Action-99
:parameters ()
:precondition
(and
(Flag324-)
(Flag324prime-)
)
:effect
(and
(Flag453-)
(Flag453prime-)
)

)
(:action Flag453Action-100
:parameters ()
:precondition
(and
(Flag438-)
(Flag438prime-)
)
:effect
(and
(Flag453-)
(Flag453prime-)
)

)
(:action Flag453Action-101
:parameters ()
:precondition
(and
(Flag424-)
(Flag424prime-)
)
:effect
(and
(Flag453-)
(Flag453prime-)
)

)
(:action Flag453Action-102
:parameters ()
:precondition
(and
(Flag450-)
(Flag450prime-)
)
:effect
(and
(Flag453-)
(Flag453prime-)
)

)
(:action Flag453Action-103
:parameters ()
:precondition
(and
(Flag451-)
(Flag451prime-)
)
:effect
(and
(Flag453-)
(Flag453prime-)
)

)
(:action Flag453Action-104
:parameters ()
:precondition
(and
(Flag351-)
(Flag351prime-)
)
:effect
(and
(Flag453-)
(Flag453prime-)
)

)
(:action Flag453Action-105
:parameters ()
:precondition
(and
(Flag452-)
(Flag452prime-)
)
:effect
(and
(Flag453-)
(Flag453prime-)
)

)
(:action Prim453Action
:parameters ()
:precondition
(and
(not (Flag386-))
(Flag386prime-)
(not (Flag302-))
(Flag302prime-)
(not (Flag378-))
(Flag378prime-)
(not (Flag307-))
(Flag307prime-)
(not (Flag417-))
(Flag417prime-)
(not (Flag359-))
(Flag359prime-)
(not (Flag343-))
(Flag343prime-)
(not (Flag380-))
(Flag380prime-)
(not (Flag388-))
(Flag388prime-)
(not (Flag322-))
(Flag322prime-)
(not (Flag393-))
(Flag393prime-)
(not (Flag410-))
(Flag410prime-)
(not (Flag440-))
(Flag440prime-)
(not (Flag411-))
(Flag411prime-)
(not (Flag318-))
(Flag318prime-)
(not (Flag344-))
(Flag344prime-)
(not (Flag349-))
(Flag349prime-)
(not (Flag368-))
(Flag368prime-)
(not (Flag377-))
(Flag377prime-)
(not (Flag326-))
(Flag326prime-)
(not (Flag441-))
(Flag441prime-)
(not (Flag300-))
(Flag300prime-)
(not (Flag385-))
(Flag385prime-)
(not (Flag442-))
(Flag442prime-)
(not (Flag387-))
(Flag387prime-)
(not (Flag319-))
(Flag319prime-)
(not (Flag354-))
(Flag354prime-)
(not (Flag426-))
(Flag426prime-)
(not (Flag427-))
(Flag427prime-)
(not (Flag443-))
(Flag443prime-)
(not (Flag308-))
(Flag308prime-)
(not (Flag428-))
(Flag428prime-)
(not (Flag357-))
(Flag357prime-)
(not (Flag395-))
(Flag395prime-)
(not (Flag429-))
(Flag429prime-)
(not (Flag413-))
(Flag413prime-)
(not (Flag444-))
(Flag444prime-)
(not (Flag396-))
(Flag396prime-)
(not (Flag397-))
(Flag397prime-)
(not (Flag414-))
(Flag414prime-)
(not (Flag341-))
(Flag341prime-)
(not (Flag445-))
(Flag445prime-)
(not (Flag430-))
(Flag430prime-)
(not (Flag381-))
(Flag381prime-)
(not (Flag431-))
(Flag431prime-)
(not (Flag405-))
(Flag405prime-)
(not (Flag415-))
(Flag415prime-)
(not (Flag363-))
(Flag363prime-)
(not (Flag321-))
(Flag321prime-)
(not (Flag432-))
(Flag432prime-)
(not (Flag323-))
(Flag323prime-)
(not (Flag398-))
(Flag398prime-)
(not (Flag340-))
(Flag340prime-)
(not (Flag327-))
(Flag327prime-)
(not (Flag369-))
(Flag369prime-)
(not (Flag370-))
(Flag370prime-)
(not (Flag331-))
(Flag331prime-)
(not (Flag352-))
(Flag352prime-)
(not (Flag399-))
(Flag399prime-)
(not (Flag416-))
(Flag416prime-)
(not (Flag446-))
(Flag446prime-)
(not (Flag333-))
(Flag333prime-)
(not (Flag334-))
(Flag334prime-)
(not (Flag299-))
(Flag299prime-)
(not (Flag375-))
(Flag375prime-)
(not (Flag342-))
(Flag342prime-)
(not (Flag400-))
(Flag400prime-)
(not (Flag418-))
(Flag418prime-)
(not (Flag401-))
(Flag401prime-)
(not (Flag447-))
(Flag447prime-)
(not (Flag379-))
(Flag379prime-)
(not (Flag303-))
(Flag303prime-)
(not (Flag448-))
(Flag448prime-)
(not (Flag356-))
(Flag356prime-)
(not (Flag365-))
(Flag365prime-)
(not (Flag315-))
(Flag315prime-)
(not (Flag312-))
(Flag312prime-)
(not (Flag347-))
(Flag347prime-)
(not (Flag330-))
(Flag330prime-)
(not (Flag403-))
(Flag403prime-)
(not (Flag404-))
(Flag404prime-)
(not (Flag328-))
(Flag328prime-)
(not (Flag382-))
(Flag382prime-)
(not (Flag372-))
(Flag372prime-)
(not (Flag419-))
(Flag419prime-)
(not (Flag420-))
(Flag420prime-)
(not (Flag421-))
(Flag421prime-)
(not (Flag353-))
(Flag353prime-)
(not (Flag449-))
(Flag449prime-)
(not (Flag436-))
(Flag436prime-)
(not (Flag373-))
(Flag373prime-)
(not (Flag422-))
(Flag422prime-)
(not (Flag406-))
(Flag406prime-)
(not (Flag314-))
(Flag314prime-)
(not (Flag306-))
(Flag306prime-)
(not (Flag362-))
(Flag362prime-)
(not (Flag364-))
(Flag364prime-)
(not (Flag437-))
(Flag437prime-)
(not (Flag390-))
(Flag390prime-)
(not (Flag324-))
(Flag324prime-)
(not (Flag438-))
(Flag438prime-)
(not (Flag424-))
(Flag424prime-)
(not (Flag450-))
(Flag450prime-)
(not (Flag451-))
(Flag451prime-)
(not (Flag351-))
(Flag351prime-)
(not (Flag452-))
(Flag452prime-)
)
:effect
(and
(Flag453prime-)
(not (Flag453-))
)

)
(:action Flag465Action-0
:parameters ()
:precondition
(and
(Flag341-)
(Flag341prime-)
)
:effect
(and
(Flag465-)
(Flag465prime-)
)

)
(:action Flag465Action-1
:parameters ()
:precondition
(and
(Flag386-)
(Flag386prime-)
)
:effect
(and
(Flag465-)
(Flag465prime-)
)

)
(:action Flag465Action-2
:parameters ()
:precondition
(and
(Flag302-)
(Flag302prime-)
)
:effect
(and
(Flag465-)
(Flag465prime-)
)

)
(:action Flag465Action-3
:parameters ()
:precondition
(and
(Flag307-)
(Flag307prime-)
)
:effect
(and
(Flag465-)
(Flag465prime-)
)

)
(:action Flag465Action-4
:parameters ()
:precondition
(and
(Flag417-)
(Flag417prime-)
)
:effect
(and
(Flag465-)
(Flag465prime-)
)

)
(:action Flag465Action-5
:parameters ()
:precondition
(and
(Flag359-)
(Flag359prime-)
)
:effect
(and
(Flag465-)
(Flag465prime-)
)

)
(:action Flag465Action-6
:parameters ()
:precondition
(and
(Flag380-)
(Flag380prime-)
)
:effect
(and
(Flag465-)
(Flag465prime-)
)

)
(:action Flag465Action-7
:parameters ()
:precondition
(and
(Flag388-)
(Flag388prime-)
)
:effect
(and
(Flag465-)
(Flag465prime-)
)

)
(:action Flag465Action-8
:parameters ()
:precondition
(and
(Flag322-)
(Flag322prime-)
)
:effect
(and
(Flag465-)
(Flag465prime-)
)

)
(:action Flag465Action-9
:parameters ()
:precondition
(and
(Flag393-)
(Flag393prime-)
)
:effect
(and
(Flag465-)
(Flag465prime-)
)

)
(:action Flag465Action-10
:parameters ()
:precondition
(and
(Flag410-)
(Flag410prime-)
)
:effect
(and
(Flag465-)
(Flag465prime-)
)

)
(:action Flag465Action-11
:parameters ()
:precondition
(and
(Flag440-)
(Flag440prime-)
)
:effect
(and
(Flag465-)
(Flag465prime-)
)

)
(:action Flag465Action-12
:parameters ()
:precondition
(and
(Flag411-)
(Flag411prime-)
)
:effect
(and
(Flag465-)
(Flag465prime-)
)

)
(:action Flag465Action-13
:parameters ()
:precondition
(and
(Flag454-)
(Flag454prime-)
)
:effect
(and
(Flag465-)
(Flag465prime-)
)

)
(:action Flag465Action-14
:parameters ()
:precondition
(and
(Flag318-)
(Flag318prime-)
)
:effect
(and
(Flag465-)
(Flag465prime-)
)

)
(:action Flag465Action-15
:parameters ()
:precondition
(and
(Flag344-)
(Flag344prime-)
)
:effect
(and
(Flag465-)
(Flag465prime-)
)

)
(:action Flag465Action-16
:parameters ()
:precondition
(and
(Flag455-)
(Flag455prime-)
)
:effect
(and
(Flag465-)
(Flag465prime-)
)

)
(:action Flag465Action-17
:parameters ()
:precondition
(and
(Flag456-)
(Flag456prime-)
)
:effect
(and
(Flag465-)
(Flag465prime-)
)

)
(:action Flag465Action-18
:parameters ()
:precondition
(and
(Flag349-)
(Flag349prime-)
)
:effect
(and
(Flag465-)
(Flag465prime-)
)

)
(:action Flag465Action-19
:parameters ()
:precondition
(and
(Flag368-)
(Flag368prime-)
)
:effect
(and
(Flag465-)
(Flag465prime-)
)

)
(:action Flag465Action-20
:parameters ()
:precondition
(and
(Flag377-)
(Flag377prime-)
)
:effect
(and
(Flag465-)
(Flag465prime-)
)

)
(:action Flag465Action-21
:parameters ()
:precondition
(and
(Flag326-)
(Flag326prime-)
)
:effect
(and
(Flag465-)
(Flag465prime-)
)

)
(:action Flag465Action-22
:parameters ()
:precondition
(and
(Flag441-)
(Flag441prime-)
)
:effect
(and
(Flag465-)
(Flag465prime-)
)

)
(:action Flag465Action-23
:parameters ()
:precondition
(and
(Flag300-)
(Flag300prime-)
)
:effect
(and
(Flag465-)
(Flag465prime-)
)

)
(:action Flag465Action-24
:parameters ()
:precondition
(and
(Flag385-)
(Flag385prime-)
)
:effect
(and
(Flag465-)
(Flag465prime-)
)

)
(:action Flag465Action-25
:parameters ()
:precondition
(and
(Flag442-)
(Flag442prime-)
)
:effect
(and
(Flag465-)
(Flag465prime-)
)

)
(:action Flag465Action-26
:parameters ()
:precondition
(and
(Flag387-)
(Flag387prime-)
)
:effect
(and
(Flag465-)
(Flag465prime-)
)

)
(:action Flag465Action-27
:parameters ()
:precondition
(and
(Flag319-)
(Flag319prime-)
)
:effect
(and
(Flag465-)
(Flag465prime-)
)

)
(:action Flag465Action-28
:parameters ()
:precondition
(and
(Flag354-)
(Flag354prime-)
)
:effect
(and
(Flag465-)
(Flag465prime-)
)

)
(:action Flag465Action-29
:parameters ()
:precondition
(and
(Flag426-)
(Flag426prime-)
)
:effect
(and
(Flag465-)
(Flag465prime-)
)

)
(:action Flag465Action-30
:parameters ()
:precondition
(and
(Flag427-)
(Flag427prime-)
)
:effect
(and
(Flag465-)
(Flag465prime-)
)

)
(:action Flag465Action-31
:parameters ()
:precondition
(and
(Flag443-)
(Flag443prime-)
)
:effect
(and
(Flag465-)
(Flag465prime-)
)

)
(:action Flag465Action-32
:parameters ()
:precondition
(and
(Flag308-)
(Flag308prime-)
)
:effect
(and
(Flag465-)
(Flag465prime-)
)

)
(:action Flag465Action-33
:parameters ()
:precondition
(and
(Flag428-)
(Flag428prime-)
)
:effect
(and
(Flag465-)
(Flag465prime-)
)

)
(:action Flag465Action-34
:parameters ()
:precondition
(and
(Flag357-)
(Flag357prime-)
)
:effect
(and
(Flag465-)
(Flag465prime-)
)

)
(:action Flag465Action-35
:parameters ()
:precondition
(and
(Flag395-)
(Flag395prime-)
)
:effect
(and
(Flag465-)
(Flag465prime-)
)

)
(:action Flag465Action-36
:parameters ()
:precondition
(and
(Flag429-)
(Flag429prime-)
)
:effect
(and
(Flag465-)
(Flag465prime-)
)

)
(:action Flag465Action-37
:parameters ()
:precondition
(and
(Flag413-)
(Flag413prime-)
)
:effect
(and
(Flag465-)
(Flag465prime-)
)

)
(:action Flag465Action-38
:parameters ()
:precondition
(and
(Flag444-)
(Flag444prime-)
)
:effect
(and
(Flag465-)
(Flag465prime-)
)

)
(:action Flag465Action-39
:parameters ()
:precondition
(and
(Flag396-)
(Flag396prime-)
)
:effect
(and
(Flag465-)
(Flag465prime-)
)

)
(:action Flag465Action-40
:parameters ()
:precondition
(and
(Flag397-)
(Flag397prime-)
)
:effect
(and
(Flag465-)
(Flag465prime-)
)

)
(:action Flag465Action-41
:parameters ()
:precondition
(and
(Flag414-)
(Flag414prime-)
)
:effect
(and
(Flag465-)
(Flag465prime-)
)

)
(:action Flag465Action-42
:parameters ()
:precondition
(and
(Flag457-)
(Flag457prime-)
)
:effect
(and
(Flag465-)
(Flag465prime-)
)

)
(:action Flag465Action-43
:parameters ()
:precondition
(and
(Flag445-)
(Flag445prime-)
)
:effect
(and
(Flag465-)
(Flag465prime-)
)

)
(:action Flag465Action-44
:parameters ()
:precondition
(and
(Flag430-)
(Flag430prime-)
)
:effect
(and
(Flag465-)
(Flag465prime-)
)

)
(:action Flag465Action-45
:parameters ()
:precondition
(and
(Flag381-)
(Flag381prime-)
)
:effect
(and
(Flag465-)
(Flag465prime-)
)

)
(:action Flag465Action-46
:parameters ()
:precondition
(and
(Flag431-)
(Flag431prime-)
)
:effect
(and
(Flag465-)
(Flag465prime-)
)

)
(:action Flag465Action-47
:parameters ()
:precondition
(and
(Flag405-)
(Flag405prime-)
)
:effect
(and
(Flag465-)
(Flag465prime-)
)

)
(:action Flag465Action-48
:parameters ()
:precondition
(and
(Flag415-)
(Flag415prime-)
)
:effect
(and
(Flag465-)
(Flag465prime-)
)

)
(:action Flag465Action-49
:parameters ()
:precondition
(and
(Flag363-)
(Flag363prime-)
)
:effect
(and
(Flag465-)
(Flag465prime-)
)

)
(:action Flag465Action-50
:parameters ()
:precondition
(and
(Flag321-)
(Flag321prime-)
)
:effect
(and
(Flag465-)
(Flag465prime-)
)

)
(:action Flag465Action-51
:parameters ()
:precondition
(and
(Flag432-)
(Flag432prime-)
)
:effect
(and
(Flag465-)
(Flag465prime-)
)

)
(:action Flag465Action-52
:parameters ()
:precondition
(and
(Flag323-)
(Flag323prime-)
)
:effect
(and
(Flag465-)
(Flag465prime-)
)

)
(:action Flag465Action-53
:parameters ()
:precondition
(and
(Flag398-)
(Flag398prime-)
)
:effect
(and
(Flag465-)
(Flag465prime-)
)

)
(:action Flag465Action-54
:parameters ()
:precondition
(and
(Flag327-)
(Flag327prime-)
)
:effect
(and
(Flag465-)
(Flag465prime-)
)

)
(:action Flag465Action-55
:parameters ()
:precondition
(and
(Flag369-)
(Flag369prime-)
)
:effect
(and
(Flag465-)
(Flag465prime-)
)

)
(:action Flag465Action-56
:parameters ()
:precondition
(and
(Flag458-)
(Flag458prime-)
)
:effect
(and
(Flag465-)
(Flag465prime-)
)

)
(:action Flag465Action-57
:parameters ()
:precondition
(and
(Flag370-)
(Flag370prime-)
)
:effect
(and
(Flag465-)
(Flag465prime-)
)

)
(:action Flag465Action-58
:parameters ()
:precondition
(and
(Flag331-)
(Flag331prime-)
)
:effect
(and
(Flag465-)
(Flag465prime-)
)

)
(:action Flag465Action-59
:parameters ()
:precondition
(and
(Flag352-)
(Flag352prime-)
)
:effect
(and
(Flag465-)
(Flag465prime-)
)

)
(:action Flag465Action-60
:parameters ()
:precondition
(and
(Flag399-)
(Flag399prime-)
)
:effect
(and
(Flag465-)
(Flag465prime-)
)

)
(:action Flag465Action-61
:parameters ()
:precondition
(and
(Flag416-)
(Flag416prime-)
)
:effect
(and
(Flag465-)
(Flag465prime-)
)

)
(:action Flag465Action-62
:parameters ()
:precondition
(and
(Flag459-)
(Flag459prime-)
)
:effect
(and
(Flag465-)
(Flag465prime-)
)

)
(:action Flag465Action-63
:parameters ()
:precondition
(and
(Flag299-)
(Flag299prime-)
)
:effect
(and
(Flag465-)
(Flag465prime-)
)

)
(:action Flag465Action-64
:parameters ()
:precondition
(and
(Flag375-)
(Flag375prime-)
)
:effect
(and
(Flag465-)
(Flag465prime-)
)

)
(:action Flag465Action-65
:parameters ()
:precondition
(and
(Flag342-)
(Flag342prime-)
)
:effect
(and
(Flag465-)
(Flag465prime-)
)

)
(:action Flag465Action-66
:parameters ()
:precondition
(and
(Flag400-)
(Flag400prime-)
)
:effect
(and
(Flag465-)
(Flag465prime-)
)

)
(:action Flag465Action-67
:parameters ()
:precondition
(and
(Flag418-)
(Flag418prime-)
)
:effect
(and
(Flag465-)
(Flag465prime-)
)

)
(:action Flag465Action-68
:parameters ()
:precondition
(and
(Flag401-)
(Flag401prime-)
)
:effect
(and
(Flag465-)
(Flag465prime-)
)

)
(:action Flag465Action-69
:parameters ()
:precondition
(and
(Flag447-)
(Flag447prime-)
)
:effect
(and
(Flag465-)
(Flag465prime-)
)

)
(:action Flag465Action-70
:parameters ()
:precondition
(and
(Flag379-)
(Flag379prime-)
)
:effect
(and
(Flag465-)
(Flag465prime-)
)

)
(:action Flag465Action-71
:parameters ()
:precondition
(and
(Flag303-)
(Flag303prime-)
)
:effect
(and
(Flag465-)
(Flag465prime-)
)

)
(:action Flag465Action-72
:parameters ()
:precondition
(and
(Flag340-)
(Flag340prime-)
)
:effect
(and
(Flag465-)
(Flag465prime-)
)

)
(:action Flag465Action-73
:parameters ()
:precondition
(and
(Flag460-)
(Flag460prime-)
)
:effect
(and
(Flag465-)
(Flag465prime-)
)

)
(:action Flag465Action-74
:parameters ()
:precondition
(and
(Flag356-)
(Flag356prime-)
)
:effect
(and
(Flag465-)
(Flag465prime-)
)

)
(:action Flag465Action-75
:parameters ()
:precondition
(and
(Flag365-)
(Flag365prime-)
)
:effect
(and
(Flag465-)
(Flag465prime-)
)

)
(:action Flag465Action-76
:parameters ()
:precondition
(and
(Flag315-)
(Flag315prime-)
)
:effect
(and
(Flag465-)
(Flag465prime-)
)

)
(:action Flag465Action-77
:parameters ()
:precondition
(and
(Flag312-)
(Flag312prime-)
)
:effect
(and
(Flag465-)
(Flag465prime-)
)

)
(:action Flag465Action-78
:parameters ()
:precondition
(and
(Flag330-)
(Flag330prime-)
)
:effect
(and
(Flag465-)
(Flag465prime-)
)

)
(:action Flag465Action-79
:parameters ()
:precondition
(and
(Flag404-)
(Flag404prime-)
)
:effect
(and
(Flag465-)
(Flag465prime-)
)

)
(:action Flag465Action-80
:parameters ()
:precondition
(and
(Flag382-)
(Flag382prime-)
)
:effect
(and
(Flag465-)
(Flag465prime-)
)

)
(:action Flag465Action-81
:parameters ()
:precondition
(and
(Flag372-)
(Flag372prime-)
)
:effect
(and
(Flag465-)
(Flag465prime-)
)

)
(:action Flag465Action-82
:parameters ()
:precondition
(and
(Flag419-)
(Flag419prime-)
)
:effect
(and
(Flag465-)
(Flag465prime-)
)

)
(:action Flag465Action-83
:parameters ()
:precondition
(and
(Flag421-)
(Flag421prime-)
)
:effect
(and
(Flag465-)
(Flag465prime-)
)

)
(:action Flag465Action-84
:parameters ()
:precondition
(and
(Flag353-)
(Flag353prime-)
)
:effect
(and
(Flag465-)
(Flag465prime-)
)

)
(:action Flag465Action-85
:parameters ()
:precondition
(and
(Flag449-)
(Flag449prime-)
)
:effect
(and
(Flag465-)
(Flag465prime-)
)

)
(:action Flag465Action-86
:parameters ()
:precondition
(and
(Flag436-)
(Flag436prime-)
)
:effect
(and
(Flag465-)
(Flag465prime-)
)

)
(:action Flag465Action-87
:parameters ()
:precondition
(and
(Flag373-)
(Flag373prime-)
)
:effect
(and
(Flag465-)
(Flag465prime-)
)

)
(:action Flag465Action-88
:parameters ()
:precondition
(and
(Flag422-)
(Flag422prime-)
)
:effect
(and
(Flag465-)
(Flag465prime-)
)

)
(:action Flag465Action-89
:parameters ()
:precondition
(and
(Flag406-)
(Flag406prime-)
)
:effect
(and
(Flag465-)
(Flag465prime-)
)

)
(:action Flag465Action-90
:parameters ()
:precondition
(and
(Flag461-)
(Flag461prime-)
)
:effect
(and
(Flag465-)
(Flag465prime-)
)

)
(:action Flag465Action-91
:parameters ()
:precondition
(and
(Flag306-)
(Flag306prime-)
)
:effect
(and
(Flag465-)
(Flag465prime-)
)

)
(:action Flag465Action-92
:parameters ()
:precondition
(and
(Flag362-)
(Flag362prime-)
)
:effect
(and
(Flag465-)
(Flag465prime-)
)

)
(:action Flag465Action-93
:parameters ()
:precondition
(and
(Flag462-)
(Flag462prime-)
)
:effect
(and
(Flag465-)
(Flag465prime-)
)

)
(:action Flag465Action-94
:parameters ()
:precondition
(and
(Flag463-)
(Flag463prime-)
)
:effect
(and
(Flag465-)
(Flag465prime-)
)

)
(:action Flag465Action-95
:parameters ()
:precondition
(and
(Flag343-)
(Flag343prime-)
)
:effect
(and
(Flag465-)
(Flag465prime-)
)

)
(:action Flag465Action-96
:parameters ()
:precondition
(and
(Flag437-)
(Flag437prime-)
)
:effect
(and
(Flag465-)
(Flag465prime-)
)

)
(:action Flag465Action-97
:parameters ()
:precondition
(and
(Flag314-)
(Flag314prime-)
)
:effect
(and
(Flag465-)
(Flag465prime-)
)

)
(:action Flag465Action-98
:parameters ()
:precondition
(and
(Flag390-)
(Flag390prime-)
)
:effect
(and
(Flag465-)
(Flag465prime-)
)

)
(:action Flag465Action-99
:parameters ()
:precondition
(and
(Flag333-)
(Flag333prime-)
)
:effect
(and
(Flag465-)
(Flag465prime-)
)

)
(:action Flag465Action-100
:parameters ()
:precondition
(and
(Flag324-)
(Flag324prime-)
)
:effect
(and
(Flag465-)
(Flag465prime-)
)

)
(:action Flag465Action-101
:parameters ()
:precondition
(and
(Flag438-)
(Flag438prime-)
)
:effect
(and
(Flag465-)
(Flag465prime-)
)

)
(:action Flag465Action-102
:parameters ()
:precondition
(and
(Flag424-)
(Flag424prime-)
)
:effect
(and
(Flag465-)
(Flag465prime-)
)

)
(:action Flag465Action-103
:parameters ()
:precondition
(and
(Flag464-)
(Flag464prime-)
)
:effect
(and
(Flag465-)
(Flag465prime-)
)

)
(:action Flag465Action-104
:parameters ()
:precondition
(and
(Flag450-)
(Flag450prime-)
)
:effect
(and
(Flag465-)
(Flag465prime-)
)

)
(:action Flag465Action-105
:parameters ()
:precondition
(and
(Flag351-)
(Flag351prime-)
)
:effect
(and
(Flag465-)
(Flag465prime-)
)

)
(:action Flag465Action-106
:parameters ()
:precondition
(and
(Flag452-)
(Flag452prime-)
)
:effect
(and
(Flag465-)
(Flag465prime-)
)

)
(:action Prim465Action
:parameters ()
:precondition
(and
(not (Flag341-))
(Flag341prime-)
(not (Flag386-))
(Flag386prime-)
(not (Flag302-))
(Flag302prime-)
(not (Flag307-))
(Flag307prime-)
(not (Flag417-))
(Flag417prime-)
(not (Flag359-))
(Flag359prime-)
(not (Flag380-))
(Flag380prime-)
(not (Flag388-))
(Flag388prime-)
(not (Flag322-))
(Flag322prime-)
(not (Flag393-))
(Flag393prime-)
(not (Flag410-))
(Flag410prime-)
(not (Flag440-))
(Flag440prime-)
(not (Flag411-))
(Flag411prime-)
(not (Flag454-))
(Flag454prime-)
(not (Flag318-))
(Flag318prime-)
(not (Flag344-))
(Flag344prime-)
(not (Flag455-))
(Flag455prime-)
(not (Flag456-))
(Flag456prime-)
(not (Flag349-))
(Flag349prime-)
(not (Flag368-))
(Flag368prime-)
(not (Flag377-))
(Flag377prime-)
(not (Flag326-))
(Flag326prime-)
(not (Flag441-))
(Flag441prime-)
(not (Flag300-))
(Flag300prime-)
(not (Flag385-))
(Flag385prime-)
(not (Flag442-))
(Flag442prime-)
(not (Flag387-))
(Flag387prime-)
(not (Flag319-))
(Flag319prime-)
(not (Flag354-))
(Flag354prime-)
(not (Flag426-))
(Flag426prime-)
(not (Flag427-))
(Flag427prime-)
(not (Flag443-))
(Flag443prime-)
(not (Flag308-))
(Flag308prime-)
(not (Flag428-))
(Flag428prime-)
(not (Flag357-))
(Flag357prime-)
(not (Flag395-))
(Flag395prime-)
(not (Flag429-))
(Flag429prime-)
(not (Flag413-))
(Flag413prime-)
(not (Flag444-))
(Flag444prime-)
(not (Flag396-))
(Flag396prime-)
(not (Flag397-))
(Flag397prime-)
(not (Flag414-))
(Flag414prime-)
(not (Flag457-))
(Flag457prime-)
(not (Flag445-))
(Flag445prime-)
(not (Flag430-))
(Flag430prime-)
(not (Flag381-))
(Flag381prime-)
(not (Flag431-))
(Flag431prime-)
(not (Flag405-))
(Flag405prime-)
(not (Flag415-))
(Flag415prime-)
(not (Flag363-))
(Flag363prime-)
(not (Flag321-))
(Flag321prime-)
(not (Flag432-))
(Flag432prime-)
(not (Flag323-))
(Flag323prime-)
(not (Flag398-))
(Flag398prime-)
(not (Flag327-))
(Flag327prime-)
(not (Flag369-))
(Flag369prime-)
(not (Flag458-))
(Flag458prime-)
(not (Flag370-))
(Flag370prime-)
(not (Flag331-))
(Flag331prime-)
(not (Flag352-))
(Flag352prime-)
(not (Flag399-))
(Flag399prime-)
(not (Flag416-))
(Flag416prime-)
(not (Flag459-))
(Flag459prime-)
(not (Flag299-))
(Flag299prime-)
(not (Flag375-))
(Flag375prime-)
(not (Flag342-))
(Flag342prime-)
(not (Flag400-))
(Flag400prime-)
(not (Flag418-))
(Flag418prime-)
(not (Flag401-))
(Flag401prime-)
(not (Flag447-))
(Flag447prime-)
(not (Flag379-))
(Flag379prime-)
(not (Flag303-))
(Flag303prime-)
(not (Flag340-))
(Flag340prime-)
(not (Flag460-))
(Flag460prime-)
(not (Flag356-))
(Flag356prime-)
(not (Flag365-))
(Flag365prime-)
(not (Flag315-))
(Flag315prime-)
(not (Flag312-))
(Flag312prime-)
(not (Flag330-))
(Flag330prime-)
(not (Flag404-))
(Flag404prime-)
(not (Flag382-))
(Flag382prime-)
(not (Flag372-))
(Flag372prime-)
(not (Flag419-))
(Flag419prime-)
(not (Flag421-))
(Flag421prime-)
(not (Flag353-))
(Flag353prime-)
(not (Flag449-))
(Flag449prime-)
(not (Flag436-))
(Flag436prime-)
(not (Flag373-))
(Flag373prime-)
(not (Flag422-))
(Flag422prime-)
(not (Flag406-))
(Flag406prime-)
(not (Flag461-))
(Flag461prime-)
(not (Flag306-))
(Flag306prime-)
(not (Flag362-))
(Flag362prime-)
(not (Flag462-))
(Flag462prime-)
(not (Flag463-))
(Flag463prime-)
(not (Flag343-))
(Flag343prime-)
(not (Flag437-))
(Flag437prime-)
(not (Flag314-))
(Flag314prime-)
(not (Flag390-))
(Flag390prime-)
(not (Flag333-))
(Flag333prime-)
(not (Flag324-))
(Flag324prime-)
(not (Flag438-))
(Flag438prime-)
(not (Flag424-))
(Flag424prime-)
(not (Flag464-))
(Flag464prime-)
(not (Flag450-))
(Flag450prime-)
(not (Flag351-))
(Flag351prime-)
(not (Flag452-))
(Flag452prime-)
)
:effect
(and
(Flag465prime-)
(not (Flag465-))
)

)
(:action Flag475Action-0
:parameters ()
:precondition
(and
(Flag341-)
(Flag341prime-)
)
:effect
(and
(Flag475-)
(Flag475prime-)
)

)
(:action Flag475Action-1
:parameters ()
:precondition
(and
(Flag386-)
(Flag386prime-)
)
:effect
(and
(Flag475-)
(Flag475prime-)
)

)
(:action Flag475Action-2
:parameters ()
:precondition
(and
(Flag302-)
(Flag302prime-)
)
:effect
(and
(Flag475-)
(Flag475prime-)
)

)
(:action Flag475Action-3
:parameters ()
:precondition
(and
(Flag307-)
(Flag307prime-)
)
:effect
(and
(Flag475-)
(Flag475prime-)
)

)
(:action Flag475Action-4
:parameters ()
:precondition
(and
(Flag417-)
(Flag417prime-)
)
:effect
(and
(Flag475-)
(Flag475prime-)
)

)
(:action Flag475Action-5
:parameters ()
:precondition
(and
(Flag359-)
(Flag359prime-)
)
:effect
(and
(Flag475-)
(Flag475prime-)
)

)
(:action Flag475Action-6
:parameters ()
:precondition
(and
(Flag380-)
(Flag380prime-)
)
:effect
(and
(Flag475-)
(Flag475prime-)
)

)
(:action Flag475Action-7
:parameters ()
:precondition
(and
(Flag388-)
(Flag388prime-)
)
:effect
(and
(Flag475-)
(Flag475prime-)
)

)
(:action Flag475Action-8
:parameters ()
:precondition
(and
(Flag322-)
(Flag322prime-)
)
:effect
(and
(Flag475-)
(Flag475prime-)
)

)
(:action Flag475Action-9
:parameters ()
:precondition
(and
(Flag393-)
(Flag393prime-)
)
:effect
(and
(Flag475-)
(Flag475prime-)
)

)
(:action Flag475Action-10
:parameters ()
:precondition
(and
(Flag410-)
(Flag410prime-)
)
:effect
(and
(Flag475-)
(Flag475prime-)
)

)
(:action Flag475Action-11
:parameters ()
:precondition
(and
(Flag440-)
(Flag440prime-)
)
:effect
(and
(Flag475-)
(Flag475prime-)
)

)
(:action Flag475Action-12
:parameters ()
:precondition
(and
(Flag411-)
(Flag411prime-)
)
:effect
(and
(Flag475-)
(Flag475prime-)
)

)
(:action Flag475Action-13
:parameters ()
:precondition
(and
(Flag454-)
(Flag454prime-)
)
:effect
(and
(Flag475-)
(Flag475prime-)
)

)
(:action Flag475Action-14
:parameters ()
:precondition
(and
(Flag344-)
(Flag344prime-)
)
:effect
(and
(Flag475-)
(Flag475prime-)
)

)
(:action Flag475Action-15
:parameters ()
:precondition
(and
(Flag456-)
(Flag456prime-)
)
:effect
(and
(Flag475-)
(Flag475prime-)
)

)
(:action Flag475Action-16
:parameters ()
:precondition
(and
(Flag349-)
(Flag349prime-)
)
:effect
(and
(Flag475-)
(Flag475prime-)
)

)
(:action Flag475Action-17
:parameters ()
:precondition
(and
(Flag368-)
(Flag368prime-)
)
:effect
(and
(Flag475-)
(Flag475prime-)
)

)
(:action Flag475Action-18
:parameters ()
:precondition
(and
(Flag377-)
(Flag377prime-)
)
:effect
(and
(Flag475-)
(Flag475prime-)
)

)
(:action Flag475Action-19
:parameters ()
:precondition
(and
(Flag441-)
(Flag441prime-)
)
:effect
(and
(Flag475-)
(Flag475prime-)
)

)
(:action Flag475Action-20
:parameters ()
:precondition
(and
(Flag300-)
(Flag300prime-)
)
:effect
(and
(Flag475-)
(Flag475prime-)
)

)
(:action Flag475Action-21
:parameters ()
:precondition
(and
(Flag455-)
(Flag455prime-)
)
:effect
(and
(Flag475-)
(Flag475prime-)
)

)
(:action Flag475Action-22
:parameters ()
:precondition
(and
(Flag442-)
(Flag442prime-)
)
:effect
(and
(Flag475-)
(Flag475prime-)
)

)
(:action Flag475Action-23
:parameters ()
:precondition
(and
(Flag387-)
(Flag387prime-)
)
:effect
(and
(Flag475-)
(Flag475prime-)
)

)
(:action Flag475Action-24
:parameters ()
:precondition
(and
(Flag319-)
(Flag319prime-)
)
:effect
(and
(Flag475-)
(Flag475prime-)
)

)
(:action Flag475Action-25
:parameters ()
:precondition
(and
(Flag354-)
(Flag354prime-)
)
:effect
(and
(Flag475-)
(Flag475prime-)
)

)
(:action Flag475Action-26
:parameters ()
:precondition
(and
(Flag426-)
(Flag426prime-)
)
:effect
(and
(Flag475-)
(Flag475prime-)
)

)
(:action Flag475Action-27
:parameters ()
:precondition
(and
(Flag427-)
(Flag427prime-)
)
:effect
(and
(Flag475-)
(Flag475prime-)
)

)
(:action Flag475Action-28
:parameters ()
:precondition
(and
(Flag443-)
(Flag443prime-)
)
:effect
(and
(Flag475-)
(Flag475prime-)
)

)
(:action Flag475Action-29
:parameters ()
:precondition
(and
(Flag308-)
(Flag308prime-)
)
:effect
(and
(Flag475-)
(Flag475prime-)
)

)
(:action Flag475Action-30
:parameters ()
:precondition
(and
(Flag466-)
(Flag466prime-)
)
:effect
(and
(Flag475-)
(Flag475prime-)
)

)
(:action Flag475Action-31
:parameters ()
:precondition
(and
(Flag428-)
(Flag428prime-)
)
:effect
(and
(Flag475-)
(Flag475prime-)
)

)
(:action Flag475Action-32
:parameters ()
:precondition
(and
(Flag357-)
(Flag357prime-)
)
:effect
(and
(Flag475-)
(Flag475prime-)
)

)
(:action Flag475Action-33
:parameters ()
:precondition
(and
(Flag467-)
(Flag467prime-)
)
:effect
(and
(Flag475-)
(Flag475prime-)
)

)
(:action Flag475Action-34
:parameters ()
:precondition
(and
(Flag395-)
(Flag395prime-)
)
:effect
(and
(Flag475-)
(Flag475prime-)
)

)
(:action Flag475Action-35
:parameters ()
:precondition
(and
(Flag429-)
(Flag429prime-)
)
:effect
(and
(Flag475-)
(Flag475prime-)
)

)
(:action Flag475Action-36
:parameters ()
:precondition
(and
(Flag413-)
(Flag413prime-)
)
:effect
(and
(Flag475-)
(Flag475prime-)
)

)
(:action Flag475Action-37
:parameters ()
:precondition
(and
(Flag444-)
(Flag444prime-)
)
:effect
(and
(Flag475-)
(Flag475prime-)
)

)
(:action Flag475Action-38
:parameters ()
:precondition
(and
(Flag396-)
(Flag396prime-)
)
:effect
(and
(Flag475-)
(Flag475prime-)
)

)
(:action Flag475Action-39
:parameters ()
:precondition
(and
(Flag397-)
(Flag397prime-)
)
:effect
(and
(Flag475-)
(Flag475prime-)
)

)
(:action Flag475Action-40
:parameters ()
:precondition
(and
(Flag414-)
(Flag414prime-)
)
:effect
(and
(Flag475-)
(Flag475prime-)
)

)
(:action Flag475Action-41
:parameters ()
:precondition
(and
(Flag457-)
(Flag457prime-)
)
:effect
(and
(Flag475-)
(Flag475prime-)
)

)
(:action Flag475Action-42
:parameters ()
:precondition
(and
(Flag445-)
(Flag445prime-)
)
:effect
(and
(Flag475-)
(Flag475prime-)
)

)
(:action Flag475Action-43
:parameters ()
:precondition
(and
(Flag430-)
(Flag430prime-)
)
:effect
(and
(Flag475-)
(Flag475prime-)
)

)
(:action Flag475Action-44
:parameters ()
:precondition
(and
(Flag381-)
(Flag381prime-)
)
:effect
(and
(Flag475-)
(Flag475prime-)
)

)
(:action Flag475Action-45
:parameters ()
:precondition
(and
(Flag431-)
(Flag431prime-)
)
:effect
(and
(Flag475-)
(Flag475prime-)
)

)
(:action Flag475Action-46
:parameters ()
:precondition
(and
(Flag405-)
(Flag405prime-)
)
:effect
(and
(Flag475-)
(Flag475prime-)
)

)
(:action Flag475Action-47
:parameters ()
:precondition
(and
(Flag415-)
(Flag415prime-)
)
:effect
(and
(Flag475-)
(Flag475prime-)
)

)
(:action Flag475Action-48
:parameters ()
:precondition
(and
(Flag468-)
(Flag468prime-)
)
:effect
(and
(Flag475-)
(Flag475prime-)
)

)
(:action Flag475Action-49
:parameters ()
:precondition
(and
(Flag363-)
(Flag363prime-)
)
:effect
(and
(Flag475-)
(Flag475prime-)
)

)
(:action Flag475Action-50
:parameters ()
:precondition
(and
(Flag321-)
(Flag321prime-)
)
:effect
(and
(Flag475-)
(Flag475prime-)
)

)
(:action Flag475Action-51
:parameters ()
:precondition
(and
(Flag469-)
(Flag469prime-)
)
:effect
(and
(Flag475-)
(Flag475prime-)
)

)
(:action Flag475Action-52
:parameters ()
:precondition
(and
(Flag323-)
(Flag323prime-)
)
:effect
(and
(Flag475-)
(Flag475prime-)
)

)
(:action Flag475Action-53
:parameters ()
:precondition
(and
(Flag398-)
(Flag398prime-)
)
:effect
(and
(Flag475-)
(Flag475prime-)
)

)
(:action Flag475Action-54
:parameters ()
:precondition
(and
(Flag327-)
(Flag327prime-)
)
:effect
(and
(Flag475-)
(Flag475prime-)
)

)
(:action Flag475Action-55
:parameters ()
:precondition
(and
(Flag369-)
(Flag369prime-)
)
:effect
(and
(Flag475-)
(Flag475prime-)
)

)
(:action Flag475Action-56
:parameters ()
:precondition
(and
(Flag370-)
(Flag370prime-)
)
:effect
(and
(Flag475-)
(Flag475prime-)
)

)
(:action Flag475Action-57
:parameters ()
:precondition
(and
(Flag331-)
(Flag331prime-)
)
:effect
(and
(Flag475-)
(Flag475prime-)
)

)
(:action Flag475Action-58
:parameters ()
:precondition
(and
(Flag352-)
(Flag352prime-)
)
:effect
(and
(Flag475-)
(Flag475prime-)
)

)
(:action Flag475Action-59
:parameters ()
:precondition
(and
(Flag416-)
(Flag416prime-)
)
:effect
(and
(Flag475-)
(Flag475prime-)
)

)
(:action Flag475Action-60
:parameters ()
:precondition
(and
(Flag459-)
(Flag459prime-)
)
:effect
(and
(Flag475-)
(Flag475prime-)
)

)
(:action Flag475Action-61
:parameters ()
:precondition
(and
(Flag299-)
(Flag299prime-)
)
:effect
(and
(Flag475-)
(Flag475prime-)
)

)
(:action Flag475Action-62
:parameters ()
:precondition
(and
(Flag375-)
(Flag375prime-)
)
:effect
(and
(Flag475-)
(Flag475prime-)
)

)
(:action Flag475Action-63
:parameters ()
:precondition
(and
(Flag342-)
(Flag342prime-)
)
:effect
(and
(Flag475-)
(Flag475prime-)
)

)
(:action Flag475Action-64
:parameters ()
:precondition
(and
(Flag400-)
(Flag400prime-)
)
:effect
(and
(Flag475-)
(Flag475prime-)
)

)
(:action Flag475Action-65
:parameters ()
:precondition
(and
(Flag418-)
(Flag418prime-)
)
:effect
(and
(Flag475-)
(Flag475prime-)
)

)
(:action Flag475Action-66
:parameters ()
:precondition
(and
(Flag401-)
(Flag401prime-)
)
:effect
(and
(Flag475-)
(Flag475prime-)
)

)
(:action Flag475Action-67
:parameters ()
:precondition
(and
(Flag447-)
(Flag447prime-)
)
:effect
(and
(Flag475-)
(Flag475prime-)
)

)
(:action Flag475Action-68
:parameters ()
:precondition
(and
(Flag379-)
(Flag379prime-)
)
:effect
(and
(Flag475-)
(Flag475prime-)
)

)
(:action Flag475Action-69
:parameters ()
:precondition
(and
(Flag303-)
(Flag303prime-)
)
:effect
(and
(Flag475-)
(Flag475prime-)
)

)
(:action Flag475Action-70
:parameters ()
:precondition
(and
(Flag340-)
(Flag340prime-)
)
:effect
(and
(Flag475-)
(Flag475prime-)
)

)
(:action Flag475Action-71
:parameters ()
:precondition
(and
(Flag460-)
(Flag460prime-)
)
:effect
(and
(Flag475-)
(Flag475prime-)
)

)
(:action Flag475Action-72
:parameters ()
:precondition
(and
(Flag470-)
(Flag470prime-)
)
:effect
(and
(Flag475-)
(Flag475prime-)
)

)
(:action Flag475Action-73
:parameters ()
:precondition
(and
(Flag471-)
(Flag471prime-)
)
:effect
(and
(Flag475-)
(Flag475prime-)
)

)
(:action Flag475Action-74
:parameters ()
:precondition
(and
(Flag315-)
(Flag315prime-)
)
:effect
(and
(Flag475-)
(Flag475prime-)
)

)
(:action Flag475Action-75
:parameters ()
:precondition
(and
(Flag312-)
(Flag312prime-)
)
:effect
(and
(Flag475-)
(Flag475prime-)
)

)
(:action Flag475Action-76
:parameters ()
:precondition
(and
(Flag330-)
(Flag330prime-)
)
:effect
(and
(Flag475-)
(Flag475prime-)
)

)
(:action Flag475Action-77
:parameters ()
:precondition
(and
(Flag382-)
(Flag382prime-)
)
:effect
(and
(Flag475-)
(Flag475prime-)
)

)
(:action Flag475Action-78
:parameters ()
:precondition
(and
(Flag365-)
(Flag365prime-)
)
:effect
(and
(Flag475-)
(Flag475prime-)
)

)
(:action Flag475Action-79
:parameters ()
:precondition
(and
(Flag372-)
(Flag372prime-)
)
:effect
(and
(Flag475-)
(Flag475prime-)
)

)
(:action Flag475Action-80
:parameters ()
:precondition
(and
(Flag419-)
(Flag419prime-)
)
:effect
(and
(Flag475-)
(Flag475prime-)
)

)
(:action Flag475Action-81
:parameters ()
:precondition
(and
(Flag421-)
(Flag421prime-)
)
:effect
(and
(Flag475-)
(Flag475prime-)
)

)
(:action Flag475Action-82
:parameters ()
:precondition
(and
(Flag353-)
(Flag353prime-)
)
:effect
(and
(Flag475-)
(Flag475prime-)
)

)
(:action Flag475Action-83
:parameters ()
:precondition
(and
(Flag449-)
(Flag449prime-)
)
:effect
(and
(Flag475-)
(Flag475prime-)
)

)
(:action Flag475Action-84
:parameters ()
:precondition
(and
(Flag436-)
(Flag436prime-)
)
:effect
(and
(Flag475-)
(Flag475prime-)
)

)
(:action Flag475Action-85
:parameters ()
:precondition
(and
(Flag373-)
(Flag373prime-)
)
:effect
(and
(Flag475-)
(Flag475prime-)
)

)
(:action Flag475Action-86
:parameters ()
:precondition
(and
(Flag472-)
(Flag472prime-)
)
:effect
(and
(Flag475-)
(Flag475prime-)
)

)
(:action Flag475Action-87
:parameters ()
:precondition
(and
(Flag406-)
(Flag406prime-)
)
:effect
(and
(Flag475-)
(Flag475prime-)
)

)
(:action Flag475Action-88
:parameters ()
:precondition
(and
(Flag473-)
(Flag473prime-)
)
:effect
(and
(Flag475-)
(Flag475prime-)
)

)
(:action Flag475Action-89
:parameters ()
:precondition
(and
(Flag314-)
(Flag314prime-)
)
:effect
(and
(Flag475-)
(Flag475prime-)
)

)
(:action Flag475Action-90
:parameters ()
:precondition
(and
(Flag306-)
(Flag306prime-)
)
:effect
(and
(Flag475-)
(Flag475prime-)
)

)
(:action Flag475Action-91
:parameters ()
:precondition
(and
(Flag362-)
(Flag362prime-)
)
:effect
(and
(Flag475-)
(Flag475prime-)
)

)
(:action Flag475Action-92
:parameters ()
:precondition
(and
(Flag462-)
(Flag462prime-)
)
:effect
(and
(Flag475-)
(Flag475prime-)
)

)
(:action Flag475Action-93
:parameters ()
:precondition
(and
(Flag463-)
(Flag463prime-)
)
:effect
(and
(Flag475-)
(Flag475prime-)
)

)
(:action Flag475Action-94
:parameters ()
:precondition
(and
(Flag437-)
(Flag437prime-)
)
:effect
(and
(Flag475-)
(Flag475prime-)
)

)
(:action Flag475Action-95
:parameters ()
:precondition
(and
(Flag390-)
(Flag390prime-)
)
:effect
(and
(Flag475-)
(Flag475prime-)
)

)
(:action Flag475Action-96
:parameters ()
:precondition
(and
(Flag333-)
(Flag333prime-)
)
:effect
(and
(Flag475-)
(Flag475prime-)
)

)
(:action Flag475Action-97
:parameters ()
:precondition
(and
(Flag324-)
(Flag324prime-)
)
:effect
(and
(Flag475-)
(Flag475prime-)
)

)
(:action Flag475Action-98
:parameters ()
:precondition
(and
(Flag438-)
(Flag438prime-)
)
:effect
(and
(Flag475-)
(Flag475prime-)
)

)
(:action Flag475Action-99
:parameters ()
:precondition
(and
(Flag424-)
(Flag424prime-)
)
:effect
(and
(Flag475-)
(Flag475prime-)
)

)
(:action Flag475Action-100
:parameters ()
:precondition
(and
(Flag464-)
(Flag464prime-)
)
:effect
(and
(Flag475-)
(Flag475prime-)
)

)
(:action Flag475Action-101
:parameters ()
:precondition
(and
(Flag450-)
(Flag450prime-)
)
:effect
(and
(Flag475-)
(Flag475prime-)
)

)
(:action Flag475Action-102
:parameters ()
:precondition
(and
(Flag351-)
(Flag351prime-)
)
:effect
(and
(Flag475-)
(Flag475prime-)
)

)
(:action Flag475Action-103
:parameters ()
:precondition
(and
(Flag474-)
(Flag474prime-)
)
:effect
(and
(Flag475-)
(Flag475prime-)
)

)
(:action Flag475Action-104
:parameters ()
:precondition
(and
(Flag452-)
(Flag452prime-)
)
:effect
(and
(Flag475-)
(Flag475prime-)
)

)
(:action Prim475Action
:parameters ()
:precondition
(and
(not (Flag341-))
(Flag341prime-)
(not (Flag386-))
(Flag386prime-)
(not (Flag302-))
(Flag302prime-)
(not (Flag307-))
(Flag307prime-)
(not (Flag417-))
(Flag417prime-)
(not (Flag359-))
(Flag359prime-)
(not (Flag380-))
(Flag380prime-)
(not (Flag388-))
(Flag388prime-)
(not (Flag322-))
(Flag322prime-)
(not (Flag393-))
(Flag393prime-)
(not (Flag410-))
(Flag410prime-)
(not (Flag440-))
(Flag440prime-)
(not (Flag411-))
(Flag411prime-)
(not (Flag454-))
(Flag454prime-)
(not (Flag344-))
(Flag344prime-)
(not (Flag456-))
(Flag456prime-)
(not (Flag349-))
(Flag349prime-)
(not (Flag368-))
(Flag368prime-)
(not (Flag377-))
(Flag377prime-)
(not (Flag441-))
(Flag441prime-)
(not (Flag300-))
(Flag300prime-)
(not (Flag455-))
(Flag455prime-)
(not (Flag442-))
(Flag442prime-)
(not (Flag387-))
(Flag387prime-)
(not (Flag319-))
(Flag319prime-)
(not (Flag354-))
(Flag354prime-)
(not (Flag426-))
(Flag426prime-)
(not (Flag427-))
(Flag427prime-)
(not (Flag443-))
(Flag443prime-)
(not (Flag308-))
(Flag308prime-)
(not (Flag466-))
(Flag466prime-)
(not (Flag428-))
(Flag428prime-)
(not (Flag357-))
(Flag357prime-)
(not (Flag467-))
(Flag467prime-)
(not (Flag395-))
(Flag395prime-)
(not (Flag429-))
(Flag429prime-)
(not (Flag413-))
(Flag413prime-)
(not (Flag444-))
(Flag444prime-)
(not (Flag396-))
(Flag396prime-)
(not (Flag397-))
(Flag397prime-)
(not (Flag414-))
(Flag414prime-)
(not (Flag457-))
(Flag457prime-)
(not (Flag445-))
(Flag445prime-)
(not (Flag430-))
(Flag430prime-)
(not (Flag381-))
(Flag381prime-)
(not (Flag431-))
(Flag431prime-)
(not (Flag405-))
(Flag405prime-)
(not (Flag415-))
(Flag415prime-)
(not (Flag468-))
(Flag468prime-)
(not (Flag363-))
(Flag363prime-)
(not (Flag321-))
(Flag321prime-)
(not (Flag469-))
(Flag469prime-)
(not (Flag323-))
(Flag323prime-)
(not (Flag398-))
(Flag398prime-)
(not (Flag327-))
(Flag327prime-)
(not (Flag369-))
(Flag369prime-)
(not (Flag370-))
(Flag370prime-)
(not (Flag331-))
(Flag331prime-)
(not (Flag352-))
(Flag352prime-)
(not (Flag416-))
(Flag416prime-)
(not (Flag459-))
(Flag459prime-)
(not (Flag299-))
(Flag299prime-)
(not (Flag375-))
(Flag375prime-)
(not (Flag342-))
(Flag342prime-)
(not (Flag400-))
(Flag400prime-)
(not (Flag418-))
(Flag418prime-)
(not (Flag401-))
(Flag401prime-)
(not (Flag447-))
(Flag447prime-)
(not (Flag379-))
(Flag379prime-)
(not (Flag303-))
(Flag303prime-)
(not (Flag340-))
(Flag340prime-)
(not (Flag460-))
(Flag460prime-)
(not (Flag470-))
(Flag470prime-)
(not (Flag471-))
(Flag471prime-)
(not (Flag315-))
(Flag315prime-)
(not (Flag312-))
(Flag312prime-)
(not (Flag330-))
(Flag330prime-)
(not (Flag382-))
(Flag382prime-)
(not (Flag365-))
(Flag365prime-)
(not (Flag372-))
(Flag372prime-)
(not (Flag419-))
(Flag419prime-)
(not (Flag421-))
(Flag421prime-)
(not (Flag353-))
(Flag353prime-)
(not (Flag449-))
(Flag449prime-)
(not (Flag436-))
(Flag436prime-)
(not (Flag373-))
(Flag373prime-)
(not (Flag472-))
(Flag472prime-)
(not (Flag406-))
(Flag406prime-)
(not (Flag473-))
(Flag473prime-)
(not (Flag314-))
(Flag314prime-)
(not (Flag306-))
(Flag306prime-)
(not (Flag362-))
(Flag362prime-)
(not (Flag462-))
(Flag462prime-)
(not (Flag463-))
(Flag463prime-)
(not (Flag437-))
(Flag437prime-)
(not (Flag390-))
(Flag390prime-)
(not (Flag333-))
(Flag333prime-)
(not (Flag324-))
(Flag324prime-)
(not (Flag438-))
(Flag438prime-)
(not (Flag424-))
(Flag424prime-)
(not (Flag464-))
(Flag464prime-)
(not (Flag450-))
(Flag450prime-)
(not (Flag351-))
(Flag351prime-)
(not (Flag474-))
(Flag474prime-)
(not (Flag452-))
(Flag452prime-)
)
:effect
(and
(Flag475prime-)
(not (Flag475-))
)

)
(:action Flag484Action-0
:parameters ()
:precondition
(and
(Flag476-)
(Flag476prime-)
)
:effect
(and
(Flag484-)
(Flag484prime-)
)

)
(:action Flag484Action-1
:parameters ()
:precondition
(and
(Flag386-)
(Flag386prime-)
)
:effect
(and
(Flag484-)
(Flag484prime-)
)

)
(:action Flag484Action-2
:parameters ()
:precondition
(and
(Flag302-)
(Flag302prime-)
)
:effect
(and
(Flag484-)
(Flag484prime-)
)

)
(:action Flag484Action-3
:parameters ()
:precondition
(and
(Flag307-)
(Flag307prime-)
)
:effect
(and
(Flag484-)
(Flag484prime-)
)

)
(:action Flag484Action-4
:parameters ()
:precondition
(and
(Flag342-)
(Flag342prime-)
)
:effect
(and
(Flag484-)
(Flag484prime-)
)

)
(:action Flag484Action-5
:parameters ()
:precondition
(and
(Flag359-)
(Flag359prime-)
)
:effect
(and
(Flag484-)
(Flag484prime-)
)

)
(:action Flag484Action-6
:parameters ()
:precondition
(and
(Flag388-)
(Flag388prime-)
)
:effect
(and
(Flag484-)
(Flag484prime-)
)

)
(:action Flag484Action-7
:parameters ()
:precondition
(and
(Flag322-)
(Flag322prime-)
)
:effect
(and
(Flag484-)
(Flag484prime-)
)

)
(:action Flag484Action-8
:parameters ()
:precondition
(and
(Flag393-)
(Flag393prime-)
)
:effect
(and
(Flag484-)
(Flag484prime-)
)

)
(:action Flag484Action-9
:parameters ()
:precondition
(and
(Flag410-)
(Flag410prime-)
)
:effect
(and
(Flag484-)
(Flag484prime-)
)

)
(:action Flag484Action-10
:parameters ()
:precondition
(and
(Flag477-)
(Flag477prime-)
)
:effect
(and
(Flag484-)
(Flag484prime-)
)

)
(:action Flag484Action-11
:parameters ()
:precondition
(and
(Flag440-)
(Flag440prime-)
)
:effect
(and
(Flag484-)
(Flag484prime-)
)

)
(:action Flag484Action-12
:parameters ()
:precondition
(and
(Flag411-)
(Flag411prime-)
)
:effect
(and
(Flag484-)
(Flag484prime-)
)

)
(:action Flag484Action-13
:parameters ()
:precondition
(and
(Flag344-)
(Flag344prime-)
)
:effect
(and
(Flag484-)
(Flag484prime-)
)

)
(:action Flag484Action-14
:parameters ()
:precondition
(and
(Flag456-)
(Flag456prime-)
)
:effect
(and
(Flag484-)
(Flag484prime-)
)

)
(:action Flag484Action-15
:parameters ()
:precondition
(and
(Flag377-)
(Flag377prime-)
)
:effect
(and
(Flag484-)
(Flag484prime-)
)

)
(:action Flag484Action-16
:parameters ()
:precondition
(and
(Flag441-)
(Flag441prime-)
)
:effect
(and
(Flag484-)
(Flag484prime-)
)

)
(:action Flag484Action-17
:parameters ()
:precondition
(and
(Flag300-)
(Flag300prime-)
)
:effect
(and
(Flag484-)
(Flag484prime-)
)

)
(:action Flag484Action-18
:parameters ()
:precondition
(and
(Flag455-)
(Flag455prime-)
)
:effect
(and
(Flag484-)
(Flag484prime-)
)

)
(:action Flag484Action-19
:parameters ()
:precondition
(and
(Flag442-)
(Flag442prime-)
)
:effect
(and
(Flag484-)
(Flag484prime-)
)

)
(:action Flag484Action-20
:parameters ()
:precondition
(and
(Flag387-)
(Flag387prime-)
)
:effect
(and
(Flag484-)
(Flag484prime-)
)

)
(:action Flag484Action-21
:parameters ()
:precondition
(and
(Flag319-)
(Flag319prime-)
)
:effect
(and
(Flag484-)
(Flag484prime-)
)

)
(:action Flag484Action-22
:parameters ()
:precondition
(and
(Flag354-)
(Flag354prime-)
)
:effect
(and
(Flag484-)
(Flag484prime-)
)

)
(:action Flag484Action-23
:parameters ()
:precondition
(and
(Flag426-)
(Flag426prime-)
)
:effect
(and
(Flag484-)
(Flag484prime-)
)

)
(:action Flag484Action-24
:parameters ()
:precondition
(and
(Flag427-)
(Flag427prime-)
)
:effect
(and
(Flag484-)
(Flag484prime-)
)

)
(:action Flag484Action-25
:parameters ()
:precondition
(and
(Flag443-)
(Flag443prime-)
)
:effect
(and
(Flag484-)
(Flag484prime-)
)

)
(:action Flag484Action-26
:parameters ()
:precondition
(and
(Flag308-)
(Flag308prime-)
)
:effect
(and
(Flag484-)
(Flag484prime-)
)

)
(:action Flag484Action-27
:parameters ()
:precondition
(and
(Flag466-)
(Flag466prime-)
)
:effect
(and
(Flag484-)
(Flag484prime-)
)

)
(:action Flag484Action-28
:parameters ()
:precondition
(and
(Flag428-)
(Flag428prime-)
)
:effect
(and
(Flag484-)
(Flag484prime-)
)

)
(:action Flag484Action-29
:parameters ()
:precondition
(and
(Flag357-)
(Flag357prime-)
)
:effect
(and
(Flag484-)
(Flag484prime-)
)

)
(:action Flag484Action-30
:parameters ()
:precondition
(and
(Flag467-)
(Flag467prime-)
)
:effect
(and
(Flag484-)
(Flag484prime-)
)

)
(:action Flag484Action-31
:parameters ()
:precondition
(and
(Flag395-)
(Flag395prime-)
)
:effect
(and
(Flag484-)
(Flag484prime-)
)

)
(:action Flag484Action-32
:parameters ()
:precondition
(and
(Flag429-)
(Flag429prime-)
)
:effect
(and
(Flag484-)
(Flag484prime-)
)

)
(:action Flag484Action-33
:parameters ()
:precondition
(and
(Flag413-)
(Flag413prime-)
)
:effect
(and
(Flag484-)
(Flag484prime-)
)

)
(:action Flag484Action-34
:parameters ()
:precondition
(and
(Flag396-)
(Flag396prime-)
)
:effect
(and
(Flag484-)
(Flag484prime-)
)

)
(:action Flag484Action-35
:parameters ()
:precondition
(and
(Flag397-)
(Flag397prime-)
)
:effect
(and
(Flag484-)
(Flag484prime-)
)

)
(:action Flag484Action-36
:parameters ()
:precondition
(and
(Flag478-)
(Flag478prime-)
)
:effect
(and
(Flag484-)
(Flag484prime-)
)

)
(:action Flag484Action-37
:parameters ()
:precondition
(and
(Flag414-)
(Flag414prime-)
)
:effect
(and
(Flag484-)
(Flag484prime-)
)

)
(:action Flag484Action-38
:parameters ()
:precondition
(and
(Flag457-)
(Flag457prime-)
)
:effect
(and
(Flag484-)
(Flag484prime-)
)

)
(:action Flag484Action-39
:parameters ()
:precondition
(and
(Flag445-)
(Flag445prime-)
)
:effect
(and
(Flag484-)
(Flag484prime-)
)

)
(:action Flag484Action-40
:parameters ()
:precondition
(and
(Flag381-)
(Flag381prime-)
)
:effect
(and
(Flag484-)
(Flag484prime-)
)

)
(:action Flag484Action-41
:parameters ()
:precondition
(and
(Flag431-)
(Flag431prime-)
)
:effect
(and
(Flag484-)
(Flag484prime-)
)

)
(:action Flag484Action-42
:parameters ()
:precondition
(and
(Flag405-)
(Flag405prime-)
)
:effect
(and
(Flag484-)
(Flag484prime-)
)

)
(:action Flag484Action-43
:parameters ()
:precondition
(and
(Flag468-)
(Flag468prime-)
)
:effect
(and
(Flag484-)
(Flag484prime-)
)

)
(:action Flag484Action-44
:parameters ()
:precondition
(and
(Flag363-)
(Flag363prime-)
)
:effect
(and
(Flag484-)
(Flag484prime-)
)

)
(:action Flag484Action-45
:parameters ()
:precondition
(and
(Flag321-)
(Flag321prime-)
)
:effect
(and
(Flag484-)
(Flag484prime-)
)

)
(:action Flag484Action-46
:parameters ()
:precondition
(and
(Flag469-)
(Flag469prime-)
)
:effect
(and
(Flag484-)
(Flag484prime-)
)

)
(:action Flag484Action-47
:parameters ()
:precondition
(and
(Flag398-)
(Flag398prime-)
)
:effect
(and
(Flag484-)
(Flag484prime-)
)

)
(:action Flag484Action-48
:parameters ()
:precondition
(and
(Flag369-)
(Flag369prime-)
)
:effect
(and
(Flag484-)
(Flag484prime-)
)

)
(:action Flag484Action-49
:parameters ()
:precondition
(and
(Flag370-)
(Flag370prime-)
)
:effect
(and
(Flag484-)
(Flag484prime-)
)

)
(:action Flag484Action-50
:parameters ()
:precondition
(and
(Flag479-)
(Flag479prime-)
)
:effect
(and
(Flag484-)
(Flag484prime-)
)

)
(:action Flag484Action-51
:parameters ()
:precondition
(and
(Flag331-)
(Flag331prime-)
)
:effect
(and
(Flag484-)
(Flag484prime-)
)

)
(:action Flag484Action-52
:parameters ()
:precondition
(and
(Flag352-)
(Flag352prime-)
)
:effect
(and
(Flag484-)
(Flag484prime-)
)

)
(:action Flag484Action-53
:parameters ()
:precondition
(and
(Flag416-)
(Flag416prime-)
)
:effect
(and
(Flag484-)
(Flag484prime-)
)

)
(:action Flag484Action-54
:parameters ()
:precondition
(and
(Flag459-)
(Flag459prime-)
)
:effect
(and
(Flag484-)
(Flag484prime-)
)

)
(:action Flag484Action-55
:parameters ()
:precondition
(and
(Flag299-)
(Flag299prime-)
)
:effect
(and
(Flag484-)
(Flag484prime-)
)

)
(:action Flag484Action-56
:parameters ()
:precondition
(and
(Flag375-)
(Flag375prime-)
)
:effect
(and
(Flag484-)
(Flag484prime-)
)

)
(:action Flag484Action-57
:parameters ()
:precondition
(and
(Flag400-)
(Flag400prime-)
)
:effect
(and
(Flag484-)
(Flag484prime-)
)

)
(:action Flag484Action-58
:parameters ()
:precondition
(and
(Flag418-)
(Flag418prime-)
)
:effect
(and
(Flag484-)
(Flag484prime-)
)

)
(:action Flag484Action-59
:parameters ()
:precondition
(and
(Flag401-)
(Flag401prime-)
)
:effect
(and
(Flag484-)
(Flag484prime-)
)

)
(:action Flag484Action-60
:parameters ()
:precondition
(and
(Flag447-)
(Flag447prime-)
)
:effect
(and
(Flag484-)
(Flag484prime-)
)

)
(:action Flag484Action-61
:parameters ()
:precondition
(and
(Flag379-)
(Flag379prime-)
)
:effect
(and
(Flag484-)
(Flag484prime-)
)

)
(:action Flag484Action-62
:parameters ()
:precondition
(and
(Flag303-)
(Flag303prime-)
)
:effect
(and
(Flag484-)
(Flag484prime-)
)

)
(:action Flag484Action-63
:parameters ()
:precondition
(and
(Flag340-)
(Flag340prime-)
)
:effect
(and
(Flag484-)
(Flag484prime-)
)

)
(:action Flag484Action-64
:parameters ()
:precondition
(and
(Flag460-)
(Flag460prime-)
)
:effect
(and
(Flag484-)
(Flag484prime-)
)

)
(:action Flag484Action-65
:parameters ()
:precondition
(and
(Flag470-)
(Flag470prime-)
)
:effect
(and
(Flag484-)
(Flag484prime-)
)

)
(:action Flag484Action-66
:parameters ()
:precondition
(and
(Flag471-)
(Flag471prime-)
)
:effect
(and
(Flag484-)
(Flag484prime-)
)

)
(:action Flag484Action-67
:parameters ()
:precondition
(and
(Flag315-)
(Flag315prime-)
)
:effect
(and
(Flag484-)
(Flag484prime-)
)

)
(:action Flag484Action-68
:parameters ()
:precondition
(and
(Flag312-)
(Flag312prime-)
)
:effect
(and
(Flag484-)
(Flag484prime-)
)

)
(:action Flag484Action-69
:parameters ()
:precondition
(and
(Flag480-)
(Flag480prime-)
)
:effect
(and
(Flag484-)
(Flag484prime-)
)

)
(:action Flag484Action-70
:parameters ()
:precondition
(and
(Flag330-)
(Flag330prime-)
)
:effect
(and
(Flag484-)
(Flag484prime-)
)

)
(:action Flag484Action-71
:parameters ()
:precondition
(and
(Flag382-)
(Flag382prime-)
)
:effect
(and
(Flag484-)
(Flag484prime-)
)

)
(:action Flag484Action-72
:parameters ()
:precondition
(and
(Flag365-)
(Flag365prime-)
)
:effect
(and
(Flag484-)
(Flag484prime-)
)

)
(:action Flag484Action-73
:parameters ()
:precondition
(and
(Flag372-)
(Flag372prime-)
)
:effect
(and
(Flag484-)
(Flag484prime-)
)

)
(:action Flag484Action-74
:parameters ()
:precondition
(and
(Flag419-)
(Flag419prime-)
)
:effect
(and
(Flag484-)
(Flag484prime-)
)

)
(:action Flag484Action-75
:parameters ()
:precondition
(and
(Flag421-)
(Flag421prime-)
)
:effect
(and
(Flag484-)
(Flag484prime-)
)

)
(:action Flag484Action-76
:parameters ()
:precondition
(and
(Flag341-)
(Flag341prime-)
)
:effect
(and
(Flag484-)
(Flag484prime-)
)

)
(:action Flag484Action-77
:parameters ()
:precondition
(and
(Flag481-)
(Flag481prime-)
)
:effect
(and
(Flag484-)
(Flag484prime-)
)

)
(:action Flag484Action-78
:parameters ()
:precondition
(and
(Flag449-)
(Flag449prime-)
)
:effect
(and
(Flag484-)
(Flag484prime-)
)

)
(:action Flag484Action-79
:parameters ()
:precondition
(and
(Flag436-)
(Flag436prime-)
)
:effect
(and
(Flag484-)
(Flag484prime-)
)

)
(:action Flag484Action-80
:parameters ()
:precondition
(and
(Flag373-)
(Flag373prime-)
)
:effect
(and
(Flag484-)
(Flag484prime-)
)

)
(:action Flag484Action-81
:parameters ()
:precondition
(and
(Flag406-)
(Flag406prime-)
)
:effect
(and
(Flag484-)
(Flag484prime-)
)

)
(:action Flag484Action-82
:parameters ()
:precondition
(and
(Flag473-)
(Flag473prime-)
)
:effect
(and
(Flag484-)
(Flag484prime-)
)

)
(:action Flag484Action-83
:parameters ()
:precondition
(and
(Flag314-)
(Flag314prime-)
)
:effect
(and
(Flag484-)
(Flag484prime-)
)

)
(:action Flag484Action-84
:parameters ()
:precondition
(and
(Flag482-)
(Flag482prime-)
)
:effect
(and
(Flag484-)
(Flag484prime-)
)

)
(:action Flag484Action-85
:parameters ()
:precondition
(and
(Flag306-)
(Flag306prime-)
)
:effect
(and
(Flag484-)
(Flag484prime-)
)

)
(:action Flag484Action-86
:parameters ()
:precondition
(and
(Flag362-)
(Flag362prime-)
)
:effect
(and
(Flag484-)
(Flag484prime-)
)

)
(:action Flag484Action-87
:parameters ()
:precondition
(and
(Flag462-)
(Flag462prime-)
)
:effect
(and
(Flag484-)
(Flag484prime-)
)

)
(:action Flag484Action-88
:parameters ()
:precondition
(and
(Flag463-)
(Flag463prime-)
)
:effect
(and
(Flag484-)
(Flag484prime-)
)

)
(:action Flag484Action-89
:parameters ()
:precondition
(and
(Flag437-)
(Flag437prime-)
)
:effect
(and
(Flag484-)
(Flag484prime-)
)

)
(:action Flag484Action-90
:parameters ()
:precondition
(and
(Flag390-)
(Flag390prime-)
)
:effect
(and
(Flag484-)
(Flag484prime-)
)

)
(:action Flag484Action-91
:parameters ()
:precondition
(and
(Flag333-)
(Flag333prime-)
)
:effect
(and
(Flag484-)
(Flag484prime-)
)

)
(:action Flag484Action-92
:parameters ()
:precondition
(and
(Flag483-)
(Flag483prime-)
)
:effect
(and
(Flag484-)
(Flag484prime-)
)

)
(:action Flag484Action-93
:parameters ()
:precondition
(and
(Flag324-)
(Flag324prime-)
)
:effect
(and
(Flag484-)
(Flag484prime-)
)

)
(:action Flag484Action-94
:parameters ()
:precondition
(and
(Flag438-)
(Flag438prime-)
)
:effect
(and
(Flag484-)
(Flag484prime-)
)

)
(:action Flag484Action-95
:parameters ()
:precondition
(and
(Flag424-)
(Flag424prime-)
)
:effect
(and
(Flag484-)
(Flag484prime-)
)

)
(:action Flag484Action-96
:parameters ()
:precondition
(and
(Flag464-)
(Flag464prime-)
)
:effect
(and
(Flag484-)
(Flag484prime-)
)

)
(:action Flag484Action-97
:parameters ()
:precondition
(and
(Flag450-)
(Flag450prime-)
)
:effect
(and
(Flag484-)
(Flag484prime-)
)

)
(:action Flag484Action-98
:parameters ()
:precondition
(and
(Flag351-)
(Flag351prime-)
)
:effect
(and
(Flag484-)
(Flag484prime-)
)

)
(:action Flag484Action-99
:parameters ()
:precondition
(and
(Flag474-)
(Flag474prime-)
)
:effect
(and
(Flag484-)
(Flag484prime-)
)

)
(:action Flag484Action-100
:parameters ()
:precondition
(and
(Flag452-)
(Flag452prime-)
)
:effect
(and
(Flag484-)
(Flag484prime-)
)

)
(:action Prim484Action
:parameters ()
:precondition
(and
(not (Flag476-))
(Flag476prime-)
(not (Flag386-))
(Flag386prime-)
(not (Flag302-))
(Flag302prime-)
(not (Flag307-))
(Flag307prime-)
(not (Flag342-))
(Flag342prime-)
(not (Flag359-))
(Flag359prime-)
(not (Flag388-))
(Flag388prime-)
(not (Flag322-))
(Flag322prime-)
(not (Flag393-))
(Flag393prime-)
(not (Flag410-))
(Flag410prime-)
(not (Flag477-))
(Flag477prime-)
(not (Flag440-))
(Flag440prime-)
(not (Flag411-))
(Flag411prime-)
(not (Flag344-))
(Flag344prime-)
(not (Flag456-))
(Flag456prime-)
(not (Flag377-))
(Flag377prime-)
(not (Flag441-))
(Flag441prime-)
(not (Flag300-))
(Flag300prime-)
(not (Flag455-))
(Flag455prime-)
(not (Flag442-))
(Flag442prime-)
(not (Flag387-))
(Flag387prime-)
(not (Flag319-))
(Flag319prime-)
(not (Flag354-))
(Flag354prime-)
(not (Flag426-))
(Flag426prime-)
(not (Flag427-))
(Flag427prime-)
(not (Flag443-))
(Flag443prime-)
(not (Flag308-))
(Flag308prime-)
(not (Flag466-))
(Flag466prime-)
(not (Flag428-))
(Flag428prime-)
(not (Flag357-))
(Flag357prime-)
(not (Flag467-))
(Flag467prime-)
(not (Flag395-))
(Flag395prime-)
(not (Flag429-))
(Flag429prime-)
(not (Flag413-))
(Flag413prime-)
(not (Flag396-))
(Flag396prime-)
(not (Flag397-))
(Flag397prime-)
(not (Flag478-))
(Flag478prime-)
(not (Flag414-))
(Flag414prime-)
(not (Flag457-))
(Flag457prime-)
(not (Flag445-))
(Flag445prime-)
(not (Flag381-))
(Flag381prime-)
(not (Flag431-))
(Flag431prime-)
(not (Flag405-))
(Flag405prime-)
(not (Flag468-))
(Flag468prime-)
(not (Flag363-))
(Flag363prime-)
(not (Flag321-))
(Flag321prime-)
(not (Flag469-))
(Flag469prime-)
(not (Flag398-))
(Flag398prime-)
(not (Flag369-))
(Flag369prime-)
(not (Flag370-))
(Flag370prime-)
(not (Flag479-))
(Flag479prime-)
(not (Flag331-))
(Flag331prime-)
(not (Flag352-))
(Flag352prime-)
(not (Flag416-))
(Flag416prime-)
(not (Flag459-))
(Flag459prime-)
(not (Flag299-))
(Flag299prime-)
(not (Flag375-))
(Flag375prime-)
(not (Flag400-))
(Flag400prime-)
(not (Flag418-))
(Flag418prime-)
(not (Flag401-))
(Flag401prime-)
(not (Flag447-))
(Flag447prime-)
(not (Flag379-))
(Flag379prime-)
(not (Flag303-))
(Flag303prime-)
(not (Flag340-))
(Flag340prime-)
(not (Flag460-))
(Flag460prime-)
(not (Flag470-))
(Flag470prime-)
(not (Flag471-))
(Flag471prime-)
(not (Flag315-))
(Flag315prime-)
(not (Flag312-))
(Flag312prime-)
(not (Flag480-))
(Flag480prime-)
(not (Flag330-))
(Flag330prime-)
(not (Flag382-))
(Flag382prime-)
(not (Flag365-))
(Flag365prime-)
(not (Flag372-))
(Flag372prime-)
(not (Flag419-))
(Flag419prime-)
(not (Flag421-))
(Flag421prime-)
(not (Flag341-))
(Flag341prime-)
(not (Flag481-))
(Flag481prime-)
(not (Flag449-))
(Flag449prime-)
(not (Flag436-))
(Flag436prime-)
(not (Flag373-))
(Flag373prime-)
(not (Flag406-))
(Flag406prime-)
(not (Flag473-))
(Flag473prime-)
(not (Flag314-))
(Flag314prime-)
(not (Flag482-))
(Flag482prime-)
(not (Flag306-))
(Flag306prime-)
(not (Flag362-))
(Flag362prime-)
(not (Flag462-))
(Flag462prime-)
(not (Flag463-))
(Flag463prime-)
(not (Flag437-))
(Flag437prime-)
(not (Flag390-))
(Flag390prime-)
(not (Flag333-))
(Flag333prime-)
(not (Flag483-))
(Flag483prime-)
(not (Flag324-))
(Flag324prime-)
(not (Flag438-))
(Flag438prime-)
(not (Flag424-))
(Flag424prime-)
(not (Flag464-))
(Flag464prime-)
(not (Flag450-))
(Flag450prime-)
(not (Flag351-))
(Flag351prime-)
(not (Flag474-))
(Flag474prime-)
(not (Flag452-))
(Flag452prime-)
)
:effect
(and
(Flag484prime-)
(not (Flag484-))
)

)
(:action Flag493Action-0
:parameters ()
:precondition
(and
(Flag476-)
(Flag476prime-)
)
:effect
(and
(Flag493-)
(Flag493prime-)
)

)
(:action Flag493Action-1
:parameters ()
:precondition
(and
(Flag386-)
(Flag386prime-)
)
:effect
(and
(Flag493-)
(Flag493prime-)
)

)
(:action Flag493Action-2
:parameters ()
:precondition
(and
(Flag302-)
(Flag302prime-)
)
:effect
(and
(Flag493-)
(Flag493prime-)
)

)
(:action Flag493Action-3
:parameters ()
:precondition
(and
(Flag307-)
(Flag307prime-)
)
:effect
(and
(Flag493-)
(Flag493prime-)
)

)
(:action Flag493Action-4
:parameters ()
:precondition
(and
(Flag342-)
(Flag342prime-)
)
:effect
(and
(Flag493-)
(Flag493prime-)
)

)
(:action Flag493Action-5
:parameters ()
:precondition
(and
(Flag359-)
(Flag359prime-)
)
:effect
(and
(Flag493-)
(Flag493prime-)
)

)
(:action Flag493Action-6
:parameters ()
:precondition
(and
(Flag312-)
(Flag312prime-)
)
:effect
(and
(Flag493-)
(Flag493prime-)
)

)
(:action Flag493Action-7
:parameters ()
:precondition
(and
(Flag322-)
(Flag322prime-)
)
:effect
(and
(Flag493-)
(Flag493prime-)
)

)
(:action Flag493Action-8
:parameters ()
:precondition
(and
(Flag393-)
(Flag393prime-)
)
:effect
(and
(Flag493-)
(Flag493prime-)
)

)
(:action Flag493Action-9
:parameters ()
:precondition
(and
(Flag410-)
(Flag410prime-)
)
:effect
(and
(Flag493-)
(Flag493prime-)
)

)
(:action Flag493Action-10
:parameters ()
:precondition
(and
(Flag477-)
(Flag477prime-)
)
:effect
(and
(Flag493-)
(Flag493prime-)
)

)
(:action Flag493Action-11
:parameters ()
:precondition
(and
(Flag440-)
(Flag440prime-)
)
:effect
(and
(Flag493-)
(Flag493prime-)
)

)
(:action Flag493Action-12
:parameters ()
:precondition
(and
(Flag485-)
(Flag485prime-)
)
:effect
(and
(Flag493-)
(Flag493prime-)
)

)
(:action Flag493Action-13
:parameters ()
:precondition
(and
(Flag411-)
(Flag411prime-)
)
:effect
(and
(Flag493-)
(Flag493prime-)
)

)
(:action Flag493Action-14
:parameters ()
:precondition
(and
(Flag344-)
(Flag344prime-)
)
:effect
(and
(Flag493-)
(Flag493prime-)
)

)
(:action Flag493Action-15
:parameters ()
:precondition
(and
(Flag456-)
(Flag456prime-)
)
:effect
(and
(Flag493-)
(Flag493prime-)
)

)
(:action Flag493Action-16
:parameters ()
:precondition
(and
(Flag377-)
(Flag377prime-)
)
:effect
(and
(Flag493-)
(Flag493prime-)
)

)
(:action Flag493Action-17
:parameters ()
:precondition
(and
(Flag486-)
(Flag486prime-)
)
:effect
(and
(Flag493-)
(Flag493prime-)
)

)
(:action Flag493Action-18
:parameters ()
:precondition
(and
(Flag441-)
(Flag441prime-)
)
:effect
(and
(Flag493-)
(Flag493prime-)
)

)
(:action Flag493Action-19
:parameters ()
:precondition
(and
(Flag300-)
(Flag300prime-)
)
:effect
(and
(Flag493-)
(Flag493prime-)
)

)
(:action Flag493Action-20
:parameters ()
:precondition
(and
(Flag455-)
(Flag455prime-)
)
:effect
(and
(Flag493-)
(Flag493prime-)
)

)
(:action Flag493Action-21
:parameters ()
:precondition
(and
(Flag467-)
(Flag467prime-)
)
:effect
(and
(Flag493-)
(Flag493prime-)
)

)
(:action Flag493Action-22
:parameters ()
:precondition
(and
(Flag387-)
(Flag387prime-)
)
:effect
(and
(Flag493-)
(Flag493prime-)
)

)
(:action Flag493Action-23
:parameters ()
:precondition
(and
(Flag319-)
(Flag319prime-)
)
:effect
(and
(Flag493-)
(Flag493prime-)
)

)
(:action Flag493Action-24
:parameters ()
:precondition
(and
(Flag354-)
(Flag354prime-)
)
:effect
(and
(Flag493-)
(Flag493prime-)
)

)
(:action Flag493Action-25
:parameters ()
:precondition
(and
(Flag427-)
(Flag427prime-)
)
:effect
(and
(Flag493-)
(Flag493prime-)
)

)
(:action Flag493Action-26
:parameters ()
:precondition
(and
(Flag443-)
(Flag443prime-)
)
:effect
(and
(Flag493-)
(Flag493prime-)
)

)
(:action Flag493Action-27
:parameters ()
:precondition
(and
(Flag308-)
(Flag308prime-)
)
:effect
(and
(Flag493-)
(Flag493prime-)
)

)
(:action Flag493Action-28
:parameters ()
:precondition
(and
(Flag466-)
(Flag466prime-)
)
:effect
(and
(Flag493-)
(Flag493prime-)
)

)
(:action Flag493Action-29
:parameters ()
:precondition
(and
(Flag428-)
(Flag428prime-)
)
:effect
(and
(Flag493-)
(Flag493prime-)
)

)
(:action Flag493Action-30
:parameters ()
:precondition
(and
(Flag357-)
(Flag357prime-)
)
:effect
(and
(Flag493-)
(Flag493prime-)
)

)
(:action Flag493Action-31
:parameters ()
:precondition
(and
(Flag429-)
(Flag429prime-)
)
:effect
(and
(Flag493-)
(Flag493prime-)
)

)
(:action Flag493Action-32
:parameters ()
:precondition
(and
(Flag413-)
(Flag413prime-)
)
:effect
(and
(Flag493-)
(Flag493prime-)
)

)
(:action Flag493Action-33
:parameters ()
:precondition
(and
(Flag396-)
(Flag396prime-)
)
:effect
(and
(Flag493-)
(Flag493prime-)
)

)
(:action Flag493Action-34
:parameters ()
:precondition
(and
(Flag397-)
(Flag397prime-)
)
:effect
(and
(Flag493-)
(Flag493prime-)
)

)
(:action Flag493Action-35
:parameters ()
:precondition
(and
(Flag478-)
(Flag478prime-)
)
:effect
(and
(Flag493-)
(Flag493prime-)
)

)
(:action Flag493Action-36
:parameters ()
:precondition
(and
(Flag414-)
(Flag414prime-)
)
:effect
(and
(Flag493-)
(Flag493prime-)
)

)
(:action Flag493Action-37
:parameters ()
:precondition
(and
(Flag457-)
(Flag457prime-)
)
:effect
(and
(Flag493-)
(Flag493prime-)
)

)
(:action Flag493Action-38
:parameters ()
:precondition
(and
(Flag445-)
(Flag445prime-)
)
:effect
(and
(Flag493-)
(Flag493prime-)
)

)
(:action Flag493Action-39
:parameters ()
:precondition
(and
(Flag381-)
(Flag381prime-)
)
:effect
(and
(Flag493-)
(Flag493prime-)
)

)
(:action Flag493Action-40
:parameters ()
:precondition
(and
(Flag431-)
(Flag431prime-)
)
:effect
(and
(Flag493-)
(Flag493prime-)
)

)
(:action Flag493Action-41
:parameters ()
:precondition
(and
(Flag405-)
(Flag405prime-)
)
:effect
(and
(Flag493-)
(Flag493prime-)
)

)
(:action Flag493Action-42
:parameters ()
:precondition
(and
(Flag468-)
(Flag468prime-)
)
:effect
(and
(Flag493-)
(Flag493prime-)
)

)
(:action Flag493Action-43
:parameters ()
:precondition
(and
(Flag363-)
(Flag363prime-)
)
:effect
(and
(Flag493-)
(Flag493prime-)
)

)
(:action Flag493Action-44
:parameters ()
:precondition
(and
(Flag487-)
(Flag487prime-)
)
:effect
(and
(Flag493-)
(Flag493prime-)
)

)
(:action Flag493Action-45
:parameters ()
:precondition
(and
(Flag321-)
(Flag321prime-)
)
:effect
(and
(Flag493-)
(Flag493prime-)
)

)
(:action Flag493Action-46
:parameters ()
:precondition
(and
(Flag469-)
(Flag469prime-)
)
:effect
(and
(Flag493-)
(Flag493prime-)
)

)
(:action Flag493Action-47
:parameters ()
:precondition
(and
(Flag398-)
(Flag398prime-)
)
:effect
(and
(Flag493-)
(Flag493prime-)
)

)
(:action Flag493Action-48
:parameters ()
:precondition
(and
(Flag369-)
(Flag369prime-)
)
:effect
(and
(Flag493-)
(Flag493prime-)
)

)
(:action Flag493Action-49
:parameters ()
:precondition
(and
(Flag330-)
(Flag330prime-)
)
:effect
(and
(Flag493-)
(Flag493prime-)
)

)
(:action Flag493Action-50
:parameters ()
:precondition
(and
(Flag479-)
(Flag479prime-)
)
:effect
(and
(Flag493-)
(Flag493prime-)
)

)
(:action Flag493Action-51
:parameters ()
:precondition
(and
(Flag331-)
(Flag331prime-)
)
:effect
(and
(Flag493-)
(Flag493prime-)
)

)
(:action Flag493Action-52
:parameters ()
:precondition
(and
(Flag352-)
(Flag352prime-)
)
:effect
(and
(Flag493-)
(Flag493prime-)
)

)
(:action Flag493Action-53
:parameters ()
:precondition
(and
(Flag416-)
(Flag416prime-)
)
:effect
(and
(Flag493-)
(Flag493prime-)
)

)
(:action Flag493Action-54
:parameters ()
:precondition
(and
(Flag459-)
(Flag459prime-)
)
:effect
(and
(Flag493-)
(Flag493prime-)
)

)
(:action Flag493Action-55
:parameters ()
:precondition
(and
(Flag299-)
(Flag299prime-)
)
:effect
(and
(Flag493-)
(Flag493prime-)
)

)
(:action Flag493Action-56
:parameters ()
:precondition
(and
(Flag375-)
(Flag375prime-)
)
:effect
(and
(Flag493-)
(Flag493prime-)
)

)
(:action Flag493Action-57
:parameters ()
:precondition
(and
(Flag400-)
(Flag400prime-)
)
:effect
(and
(Flag493-)
(Flag493prime-)
)

)
(:action Flag493Action-58
:parameters ()
:precondition
(and
(Flag418-)
(Flag418prime-)
)
:effect
(and
(Flag493-)
(Flag493prime-)
)

)
(:action Flag493Action-59
:parameters ()
:precondition
(and
(Flag401-)
(Flag401prime-)
)
:effect
(and
(Flag493-)
(Flag493prime-)
)

)
(:action Flag493Action-60
:parameters ()
:precondition
(and
(Flag488-)
(Flag488prime-)
)
:effect
(and
(Flag493-)
(Flag493prime-)
)

)
(:action Flag493Action-61
:parameters ()
:precondition
(and
(Flag447-)
(Flag447prime-)
)
:effect
(and
(Flag493-)
(Flag493prime-)
)

)
(:action Flag493Action-62
:parameters ()
:precondition
(and
(Flag379-)
(Flag379prime-)
)
:effect
(and
(Flag493-)
(Flag493prime-)
)

)
(:action Flag493Action-63
:parameters ()
:precondition
(and
(Flag489-)
(Flag489prime-)
)
:effect
(and
(Flag493-)
(Flag493prime-)
)

)
(:action Flag493Action-64
:parameters ()
:precondition
(and
(Flag340-)
(Flag340prime-)
)
:effect
(and
(Flag493-)
(Flag493prime-)
)

)
(:action Flag493Action-65
:parameters ()
:precondition
(and
(Flag460-)
(Flag460prime-)
)
:effect
(and
(Flag493-)
(Flag493prime-)
)

)
(:action Flag493Action-66
:parameters ()
:precondition
(and
(Flag470-)
(Flag470prime-)
)
:effect
(and
(Flag493-)
(Flag493prime-)
)

)
(:action Flag493Action-67
:parameters ()
:precondition
(and
(Flag471-)
(Flag471prime-)
)
:effect
(and
(Flag493-)
(Flag493prime-)
)

)
(:action Flag493Action-68
:parameters ()
:precondition
(and
(Flag315-)
(Flag315prime-)
)
:effect
(and
(Flag493-)
(Flag493prime-)
)

)
(:action Flag493Action-69
:parameters ()
:precondition
(and
(Flag382-)
(Flag382prime-)
)
:effect
(and
(Flag493-)
(Flag493prime-)
)

)
(:action Flag493Action-70
:parameters ()
:precondition
(and
(Flag365-)
(Flag365prime-)
)
:effect
(and
(Flag493-)
(Flag493prime-)
)

)
(:action Flag493Action-71
:parameters ()
:precondition
(and
(Flag419-)
(Flag419prime-)
)
:effect
(and
(Flag493-)
(Flag493prime-)
)

)
(:action Flag493Action-72
:parameters ()
:precondition
(and
(Flag341-)
(Flag341prime-)
)
:effect
(and
(Flag493-)
(Flag493prime-)
)

)
(:action Flag493Action-73
:parameters ()
:precondition
(and
(Flag481-)
(Flag481prime-)
)
:effect
(and
(Flag493-)
(Flag493prime-)
)

)
(:action Flag493Action-74
:parameters ()
:precondition
(and
(Flag449-)
(Flag449prime-)
)
:effect
(and
(Flag493-)
(Flag493prime-)
)

)
(:action Flag493Action-75
:parameters ()
:precondition
(and
(Flag436-)
(Flag436prime-)
)
:effect
(and
(Flag493-)
(Flag493prime-)
)

)
(:action Flag493Action-76
:parameters ()
:precondition
(and
(Flag373-)
(Flag373prime-)
)
:effect
(and
(Flag493-)
(Flag493prime-)
)

)
(:action Flag493Action-77
:parameters ()
:precondition
(and
(Flag406-)
(Flag406prime-)
)
:effect
(and
(Flag493-)
(Flag493prime-)
)

)
(:action Flag493Action-78
:parameters ()
:precondition
(and
(Flag314-)
(Flag314prime-)
)
:effect
(and
(Flag493-)
(Flag493prime-)
)

)
(:action Flag493Action-79
:parameters ()
:precondition
(and
(Flag306-)
(Flag306prime-)
)
:effect
(and
(Flag493-)
(Flag493prime-)
)

)
(:action Flag493Action-80
:parameters ()
:precondition
(and
(Flag362-)
(Flag362prime-)
)
:effect
(and
(Flag493-)
(Flag493prime-)
)

)
(:action Flag493Action-81
:parameters ()
:precondition
(and
(Flag462-)
(Flag462prime-)
)
:effect
(and
(Flag493-)
(Flag493prime-)
)

)
(:action Flag493Action-82
:parameters ()
:precondition
(and
(Flag463-)
(Flag463prime-)
)
:effect
(and
(Flag493-)
(Flag493prime-)
)

)
(:action Flag493Action-83
:parameters ()
:precondition
(and
(Flag490-)
(Flag490prime-)
)
:effect
(and
(Flag493-)
(Flag493prime-)
)

)
(:action Flag493Action-84
:parameters ()
:precondition
(and
(Flag390-)
(Flag390prime-)
)
:effect
(and
(Flag493-)
(Flag493prime-)
)

)
(:action Flag493Action-85
:parameters ()
:precondition
(and
(Flag333-)
(Flag333prime-)
)
:effect
(and
(Flag493-)
(Flag493prime-)
)

)
(:action Flag493Action-86
:parameters ()
:precondition
(and
(Flag491-)
(Flag491prime-)
)
:effect
(and
(Flag493-)
(Flag493prime-)
)

)
(:action Flag493Action-87
:parameters ()
:precondition
(and
(Flag483-)
(Flag483prime-)
)
:effect
(and
(Flag493-)
(Flag493prime-)
)

)
(:action Flag493Action-88
:parameters ()
:precondition
(and
(Flag438-)
(Flag438prime-)
)
:effect
(and
(Flag493-)
(Flag493prime-)
)

)
(:action Flag493Action-89
:parameters ()
:precondition
(and
(Flag424-)
(Flag424prime-)
)
:effect
(and
(Flag493-)
(Flag493prime-)
)

)
(:action Flag493Action-90
:parameters ()
:precondition
(and
(Flag450-)
(Flag450prime-)
)
:effect
(and
(Flag493-)
(Flag493prime-)
)

)
(:action Flag493Action-91
:parameters ()
:precondition
(and
(Flag351-)
(Flag351prime-)
)
:effect
(and
(Flag493-)
(Flag493prime-)
)

)
(:action Flag493Action-92
:parameters ()
:precondition
(and
(Flag474-)
(Flag474prime-)
)
:effect
(and
(Flag493-)
(Flag493prime-)
)

)
(:action Flag493Action-93
:parameters ()
:precondition
(and
(Flag452-)
(Flag452prime-)
)
:effect
(and
(Flag493-)
(Flag493prime-)
)

)
(:action Flag493Action-94
:parameters ()
:precondition
(and
(Flag492-)
(Flag492prime-)
)
:effect
(and
(Flag493-)
(Flag493prime-)
)

)
(:action Prim493Action
:parameters ()
:precondition
(and
(not (Flag476-))
(Flag476prime-)
(not (Flag386-))
(Flag386prime-)
(not (Flag302-))
(Flag302prime-)
(not (Flag307-))
(Flag307prime-)
(not (Flag342-))
(Flag342prime-)
(not (Flag359-))
(Flag359prime-)
(not (Flag312-))
(Flag312prime-)
(not (Flag322-))
(Flag322prime-)
(not (Flag393-))
(Flag393prime-)
(not (Flag410-))
(Flag410prime-)
(not (Flag477-))
(Flag477prime-)
(not (Flag440-))
(Flag440prime-)
(not (Flag485-))
(Flag485prime-)
(not (Flag411-))
(Flag411prime-)
(not (Flag344-))
(Flag344prime-)
(not (Flag456-))
(Flag456prime-)
(not (Flag377-))
(Flag377prime-)
(not (Flag486-))
(Flag486prime-)
(not (Flag441-))
(Flag441prime-)
(not (Flag300-))
(Flag300prime-)
(not (Flag455-))
(Flag455prime-)
(not (Flag467-))
(Flag467prime-)
(not (Flag387-))
(Flag387prime-)
(not (Flag319-))
(Flag319prime-)
(not (Flag354-))
(Flag354prime-)
(not (Flag427-))
(Flag427prime-)
(not (Flag443-))
(Flag443prime-)
(not (Flag308-))
(Flag308prime-)
(not (Flag466-))
(Flag466prime-)
(not (Flag428-))
(Flag428prime-)
(not (Flag357-))
(Flag357prime-)
(not (Flag429-))
(Flag429prime-)
(not (Flag413-))
(Flag413prime-)
(not (Flag396-))
(Flag396prime-)
(not (Flag397-))
(Flag397prime-)
(not (Flag478-))
(Flag478prime-)
(not (Flag414-))
(Flag414prime-)
(not (Flag457-))
(Flag457prime-)
(not (Flag445-))
(Flag445prime-)
(not (Flag381-))
(Flag381prime-)
(not (Flag431-))
(Flag431prime-)
(not (Flag405-))
(Flag405prime-)
(not (Flag468-))
(Flag468prime-)
(not (Flag363-))
(Flag363prime-)
(not (Flag487-))
(Flag487prime-)
(not (Flag321-))
(Flag321prime-)
(not (Flag469-))
(Flag469prime-)
(not (Flag398-))
(Flag398prime-)
(not (Flag369-))
(Flag369prime-)
(not (Flag330-))
(Flag330prime-)
(not (Flag479-))
(Flag479prime-)
(not (Flag331-))
(Flag331prime-)
(not (Flag352-))
(Flag352prime-)
(not (Flag416-))
(Flag416prime-)
(not (Flag459-))
(Flag459prime-)
(not (Flag299-))
(Flag299prime-)
(not (Flag375-))
(Flag375prime-)
(not (Flag400-))
(Flag400prime-)
(not (Flag418-))
(Flag418prime-)
(not (Flag401-))
(Flag401prime-)
(not (Flag488-))
(Flag488prime-)
(not (Flag447-))
(Flag447prime-)
(not (Flag379-))
(Flag379prime-)
(not (Flag489-))
(Flag489prime-)
(not (Flag340-))
(Flag340prime-)
(not (Flag460-))
(Flag460prime-)
(not (Flag470-))
(Flag470prime-)
(not (Flag471-))
(Flag471prime-)
(not (Flag315-))
(Flag315prime-)
(not (Flag382-))
(Flag382prime-)
(not (Flag365-))
(Flag365prime-)
(not (Flag419-))
(Flag419prime-)
(not (Flag341-))
(Flag341prime-)
(not (Flag481-))
(Flag481prime-)
(not (Flag449-))
(Flag449prime-)
(not (Flag436-))
(Flag436prime-)
(not (Flag373-))
(Flag373prime-)
(not (Flag406-))
(Flag406prime-)
(not (Flag314-))
(Flag314prime-)
(not (Flag306-))
(Flag306prime-)
(not (Flag362-))
(Flag362prime-)
(not (Flag462-))
(Flag462prime-)
(not (Flag463-))
(Flag463prime-)
(not (Flag490-))
(Flag490prime-)
(not (Flag390-))
(Flag390prime-)
(not (Flag333-))
(Flag333prime-)
(not (Flag491-))
(Flag491prime-)
(not (Flag483-))
(Flag483prime-)
(not (Flag438-))
(Flag438prime-)
(not (Flag424-))
(Flag424prime-)
(not (Flag450-))
(Flag450prime-)
(not (Flag351-))
(Flag351prime-)
(not (Flag474-))
(Flag474prime-)
(not (Flag452-))
(Flag452prime-)
(not (Flag492-))
(Flag492prime-)
)
:effect
(and
(Flag493prime-)
(not (Flag493-))
)

)
(:action Flag501Action-0
:parameters ()
:precondition
(and
(Flag476-)
(Flag476prime-)
)
:effect
(and
(Flag501-)
(Flag501prime-)
)

)
(:action Flag501Action-1
:parameters ()
:precondition
(and
(Flag386-)
(Flag386prime-)
)
:effect
(and
(Flag501-)
(Flag501prime-)
)

)
(:action Flag501Action-2
:parameters ()
:precondition
(and
(Flag302-)
(Flag302prime-)
)
:effect
(and
(Flag501-)
(Flag501prime-)
)

)
(:action Flag501Action-3
:parameters ()
:precondition
(and
(Flag494-)
(Flag494prime-)
)
:effect
(and
(Flag501-)
(Flag501prime-)
)

)
(:action Flag501Action-4
:parameters ()
:precondition
(and
(Flag307-)
(Flag307prime-)
)
:effect
(and
(Flag501-)
(Flag501prime-)
)

)
(:action Flag501Action-5
:parameters ()
:precondition
(and
(Flag342-)
(Flag342prime-)
)
:effect
(and
(Flag501-)
(Flag501prime-)
)

)
(:action Flag501Action-6
:parameters ()
:precondition
(and
(Flag359-)
(Flag359prime-)
)
:effect
(and
(Flag501-)
(Flag501prime-)
)

)
(:action Flag501Action-7
:parameters ()
:precondition
(and
(Flag312-)
(Flag312prime-)
)
:effect
(and
(Flag501-)
(Flag501prime-)
)

)
(:action Flag501Action-8
:parameters ()
:precondition
(and
(Flag322-)
(Flag322prime-)
)
:effect
(and
(Flag501-)
(Flag501prime-)
)

)
(:action Flag501Action-9
:parameters ()
:precondition
(and
(Flag393-)
(Flag393prime-)
)
:effect
(and
(Flag501-)
(Flag501prime-)
)

)
(:action Flag501Action-10
:parameters ()
:precondition
(and
(Flag410-)
(Flag410prime-)
)
:effect
(and
(Flag501-)
(Flag501prime-)
)

)
(:action Flag501Action-11
:parameters ()
:precondition
(and
(Flag477-)
(Flag477prime-)
)
:effect
(and
(Flag501-)
(Flag501prime-)
)

)
(:action Flag501Action-12
:parameters ()
:precondition
(and
(Flag440-)
(Flag440prime-)
)
:effect
(and
(Flag501-)
(Flag501prime-)
)

)
(:action Flag501Action-13
:parameters ()
:precondition
(and
(Flag485-)
(Flag485prime-)
)
:effect
(and
(Flag501-)
(Flag501prime-)
)

)
(:action Flag501Action-14
:parameters ()
:precondition
(and
(Flag411-)
(Flag411prime-)
)
:effect
(and
(Flag501-)
(Flag501prime-)
)

)
(:action Flag501Action-15
:parameters ()
:precondition
(and
(Flag344-)
(Flag344prime-)
)
:effect
(and
(Flag501-)
(Flag501prime-)
)

)
(:action Flag501Action-16
:parameters ()
:precondition
(and
(Flag377-)
(Flag377prime-)
)
:effect
(and
(Flag501-)
(Flag501prime-)
)

)
(:action Flag501Action-17
:parameters ()
:precondition
(and
(Flag486-)
(Flag486prime-)
)
:effect
(and
(Flag501-)
(Flag501prime-)
)

)
(:action Flag501Action-18
:parameters ()
:precondition
(and
(Flag441-)
(Flag441prime-)
)
:effect
(and
(Flag501-)
(Flag501prime-)
)

)
(:action Flag501Action-19
:parameters ()
:precondition
(and
(Flag300-)
(Flag300prime-)
)
:effect
(and
(Flag501-)
(Flag501prime-)
)

)
(:action Flag501Action-20
:parameters ()
:precondition
(and
(Flag455-)
(Flag455prime-)
)
:effect
(and
(Flag501-)
(Flag501prime-)
)

)
(:action Flag501Action-21
:parameters ()
:precondition
(and
(Flag467-)
(Flag467prime-)
)
:effect
(and
(Flag501-)
(Flag501prime-)
)

)
(:action Flag501Action-22
:parameters ()
:precondition
(and
(Flag319-)
(Flag319prime-)
)
:effect
(and
(Flag501-)
(Flag501prime-)
)

)
(:action Flag501Action-23
:parameters ()
:precondition
(and
(Flag354-)
(Flag354prime-)
)
:effect
(and
(Flag501-)
(Flag501prime-)
)

)
(:action Flag501Action-24
:parameters ()
:precondition
(and
(Flag427-)
(Flag427prime-)
)
:effect
(and
(Flag501-)
(Flag501prime-)
)

)
(:action Flag501Action-25
:parameters ()
:precondition
(and
(Flag308-)
(Flag308prime-)
)
:effect
(and
(Flag501-)
(Flag501prime-)
)

)
(:action Flag501Action-26
:parameters ()
:precondition
(and
(Flag466-)
(Flag466prime-)
)
:effect
(and
(Flag501-)
(Flag501prime-)
)

)
(:action Flag501Action-27
:parameters ()
:precondition
(and
(Flag428-)
(Flag428prime-)
)
:effect
(and
(Flag501-)
(Flag501prime-)
)

)
(:action Flag501Action-28
:parameters ()
:precondition
(and
(Flag357-)
(Flag357prime-)
)
:effect
(and
(Flag501-)
(Flag501prime-)
)

)
(:action Flag501Action-29
:parameters ()
:precondition
(and
(Flag429-)
(Flag429prime-)
)
:effect
(and
(Flag501-)
(Flag501prime-)
)

)
(:action Flag501Action-30
:parameters ()
:precondition
(and
(Flag413-)
(Flag413prime-)
)
:effect
(and
(Flag501-)
(Flag501prime-)
)

)
(:action Flag501Action-31
:parameters ()
:precondition
(and
(Flag397-)
(Flag397prime-)
)
:effect
(and
(Flag501-)
(Flag501prime-)
)

)
(:action Flag501Action-32
:parameters ()
:precondition
(and
(Flag478-)
(Flag478prime-)
)
:effect
(and
(Flag501-)
(Flag501prime-)
)

)
(:action Flag501Action-33
:parameters ()
:precondition
(and
(Flag414-)
(Flag414prime-)
)
:effect
(and
(Flag501-)
(Flag501prime-)
)

)
(:action Flag501Action-34
:parameters ()
:precondition
(and
(Flag457-)
(Flag457prime-)
)
:effect
(and
(Flag501-)
(Flag501prime-)
)

)
(:action Flag501Action-35
:parameters ()
:precondition
(and
(Flag445-)
(Flag445prime-)
)
:effect
(and
(Flag501-)
(Flag501prime-)
)

)
(:action Flag501Action-36
:parameters ()
:precondition
(and
(Flag431-)
(Flag431prime-)
)
:effect
(and
(Flag501-)
(Flag501prime-)
)

)
(:action Flag501Action-37
:parameters ()
:precondition
(and
(Flag405-)
(Flag405prime-)
)
:effect
(and
(Flag501-)
(Flag501prime-)
)

)
(:action Flag501Action-38
:parameters ()
:precondition
(and
(Flag468-)
(Flag468prime-)
)
:effect
(and
(Flag501-)
(Flag501prime-)
)

)
(:action Flag501Action-39
:parameters ()
:precondition
(and
(Flag363-)
(Flag363prime-)
)
:effect
(and
(Flag501-)
(Flag501prime-)
)

)
(:action Flag501Action-40
:parameters ()
:precondition
(and
(Flag487-)
(Flag487prime-)
)
:effect
(and
(Flag501-)
(Flag501prime-)
)

)
(:action Flag501Action-41
:parameters ()
:precondition
(and
(Flag321-)
(Flag321prime-)
)
:effect
(and
(Flag501-)
(Flag501prime-)
)

)
(:action Flag501Action-42
:parameters ()
:precondition
(and
(Flag469-)
(Flag469prime-)
)
:effect
(and
(Flag501-)
(Flag501prime-)
)

)
(:action Flag501Action-43
:parameters ()
:precondition
(and
(Flag398-)
(Flag398prime-)
)
:effect
(and
(Flag501-)
(Flag501prime-)
)

)
(:action Flag501Action-44
:parameters ()
:precondition
(and
(Flag369-)
(Flag369prime-)
)
:effect
(and
(Flag501-)
(Flag501prime-)
)

)
(:action Flag501Action-45
:parameters ()
:precondition
(and
(Flag495-)
(Flag495prime-)
)
:effect
(and
(Flag501-)
(Flag501prime-)
)

)
(:action Flag501Action-46
:parameters ()
:precondition
(and
(Flag479-)
(Flag479prime-)
)
:effect
(and
(Flag501-)
(Flag501prime-)
)

)
(:action Flag501Action-47
:parameters ()
:precondition
(and
(Flag352-)
(Flag352prime-)
)
:effect
(and
(Flag501-)
(Flag501prime-)
)

)
(:action Flag501Action-48
:parameters ()
:precondition
(and
(Flag416-)
(Flag416prime-)
)
:effect
(and
(Flag501-)
(Flag501prime-)
)

)
(:action Flag501Action-49
:parameters ()
:precondition
(and
(Flag459-)
(Flag459prime-)
)
:effect
(and
(Flag501-)
(Flag501prime-)
)

)
(:action Flag501Action-50
:parameters ()
:precondition
(and
(Flag299-)
(Flag299prime-)
)
:effect
(and
(Flag501-)
(Flag501prime-)
)

)
(:action Flag501Action-51
:parameters ()
:precondition
(and
(Flag375-)
(Flag375prime-)
)
:effect
(and
(Flag501-)
(Flag501prime-)
)

)
(:action Flag501Action-52
:parameters ()
:precondition
(and
(Flag400-)
(Flag400prime-)
)
:effect
(and
(Flag501-)
(Flag501prime-)
)

)
(:action Flag501Action-53
:parameters ()
:precondition
(and
(Flag418-)
(Flag418prime-)
)
:effect
(and
(Flag501-)
(Flag501prime-)
)

)
(:action Flag501Action-54
:parameters ()
:precondition
(and
(Flag401-)
(Flag401prime-)
)
:effect
(and
(Flag501-)
(Flag501prime-)
)

)
(:action Flag501Action-55
:parameters ()
:precondition
(and
(Flag447-)
(Flag447prime-)
)
:effect
(and
(Flag501-)
(Flag501prime-)
)

)
(:action Flag501Action-56
:parameters ()
:precondition
(and
(Flag379-)
(Flag379prime-)
)
:effect
(and
(Flag501-)
(Flag501prime-)
)

)
(:action Flag501Action-57
:parameters ()
:precondition
(and
(Flag496-)
(Flag496prime-)
)
:effect
(and
(Flag501-)
(Flag501prime-)
)

)
(:action Flag501Action-58
:parameters ()
:precondition
(and
(Flag489-)
(Flag489prime-)
)
:effect
(and
(Flag501-)
(Flag501prime-)
)

)
(:action Flag501Action-59
:parameters ()
:precondition
(and
(Flag340-)
(Flag340prime-)
)
:effect
(and
(Flag501-)
(Flag501prime-)
)

)
(:action Flag501Action-60
:parameters ()
:precondition
(and
(Flag470-)
(Flag470prime-)
)
:effect
(and
(Flag501-)
(Flag501prime-)
)

)
(:action Flag501Action-61
:parameters ()
:precondition
(and
(Flag497-)
(Flag497prime-)
)
:effect
(and
(Flag501-)
(Flag501prime-)
)

)
(:action Flag501Action-62
:parameters ()
:precondition
(and
(Flag471-)
(Flag471prime-)
)
:effect
(and
(Flag501-)
(Flag501prime-)
)

)
(:action Flag501Action-63
:parameters ()
:precondition
(and
(Flag315-)
(Flag315prime-)
)
:effect
(and
(Flag501-)
(Flag501prime-)
)

)
(:action Flag501Action-64
:parameters ()
:precondition
(and
(Flag498-)
(Flag498prime-)
)
:effect
(and
(Flag501-)
(Flag501prime-)
)

)
(:action Flag501Action-65
:parameters ()
:precondition
(and
(Flag382-)
(Flag382prime-)
)
:effect
(and
(Flag501-)
(Flag501prime-)
)

)
(:action Flag501Action-66
:parameters ()
:precondition
(and
(Flag499-)
(Flag499prime-)
)
:effect
(and
(Flag501-)
(Flag501prime-)
)

)
(:action Flag501Action-67
:parameters ()
:precondition
(and
(Flag419-)
(Flag419prime-)
)
:effect
(and
(Flag501-)
(Flag501prime-)
)

)
(:action Flag501Action-68
:parameters ()
:precondition
(and
(Flag341-)
(Flag341prime-)
)
:effect
(and
(Flag501-)
(Flag501prime-)
)

)
(:action Flag501Action-69
:parameters ()
:precondition
(and
(Flag481-)
(Flag481prime-)
)
:effect
(and
(Flag501-)
(Flag501prime-)
)

)
(:action Flag501Action-70
:parameters ()
:precondition
(and
(Flag449-)
(Flag449prime-)
)
:effect
(and
(Flag501-)
(Flag501prime-)
)

)
(:action Flag501Action-71
:parameters ()
:precondition
(and
(Flag436-)
(Flag436prime-)
)
:effect
(and
(Flag501-)
(Flag501prime-)
)

)
(:action Flag501Action-72
:parameters ()
:precondition
(and
(Flag373-)
(Flag373prime-)
)
:effect
(and
(Flag501-)
(Flag501prime-)
)

)
(:action Flag501Action-73
:parameters ()
:precondition
(and
(Flag406-)
(Flag406prime-)
)
:effect
(and
(Flag501-)
(Flag501prime-)
)

)
(:action Flag501Action-74
:parameters ()
:precondition
(and
(Flag314-)
(Flag314prime-)
)
:effect
(and
(Flag501-)
(Flag501prime-)
)

)
(:action Flag501Action-75
:parameters ()
:precondition
(and
(Flag306-)
(Flag306prime-)
)
:effect
(and
(Flag501-)
(Flag501prime-)
)

)
(:action Flag501Action-76
:parameters ()
:precondition
(and
(Flag362-)
(Flag362prime-)
)
:effect
(and
(Flag501-)
(Flag501prime-)
)

)
(:action Flag501Action-77
:parameters ()
:precondition
(and
(Flag462-)
(Flag462prime-)
)
:effect
(and
(Flag501-)
(Flag501prime-)
)

)
(:action Flag501Action-78
:parameters ()
:precondition
(and
(Flag463-)
(Flag463prime-)
)
:effect
(and
(Flag501-)
(Flag501prime-)
)

)
(:action Flag501Action-79
:parameters ()
:precondition
(and
(Flag490-)
(Flag490prime-)
)
:effect
(and
(Flag501-)
(Flag501prime-)
)

)
(:action Flag501Action-80
:parameters ()
:precondition
(and
(Flag390-)
(Flag390prime-)
)
:effect
(and
(Flag501-)
(Flag501prime-)
)

)
(:action Flag501Action-81
:parameters ()
:precondition
(and
(Flag500-)
(Flag500prime-)
)
:effect
(and
(Flag501-)
(Flag501prime-)
)

)
(:action Flag501Action-82
:parameters ()
:precondition
(and
(Flag333-)
(Flag333prime-)
)
:effect
(and
(Flag501-)
(Flag501prime-)
)

)
(:action Flag501Action-83
:parameters ()
:precondition
(and
(Flag483-)
(Flag483prime-)
)
:effect
(and
(Flag501-)
(Flag501prime-)
)

)
(:action Flag501Action-84
:parameters ()
:precondition
(and
(Flag438-)
(Flag438prime-)
)
:effect
(and
(Flag501-)
(Flag501prime-)
)

)
(:action Flag501Action-85
:parameters ()
:precondition
(and
(Flag460-)
(Flag460prime-)
)
:effect
(and
(Flag501-)
(Flag501prime-)
)

)
(:action Flag501Action-86
:parameters ()
:precondition
(and
(Flag450-)
(Flag450prime-)
)
:effect
(and
(Flag501-)
(Flag501prime-)
)

)
(:action Prim501Action
:parameters ()
:precondition
(and
(not (Flag476-))
(Flag476prime-)
(not (Flag386-))
(Flag386prime-)
(not (Flag302-))
(Flag302prime-)
(not (Flag494-))
(Flag494prime-)
(not (Flag307-))
(Flag307prime-)
(not (Flag342-))
(Flag342prime-)
(not (Flag359-))
(Flag359prime-)
(not (Flag312-))
(Flag312prime-)
(not (Flag322-))
(Flag322prime-)
(not (Flag393-))
(Flag393prime-)
(not (Flag410-))
(Flag410prime-)
(not (Flag477-))
(Flag477prime-)
(not (Flag440-))
(Flag440prime-)
(not (Flag485-))
(Flag485prime-)
(not (Flag411-))
(Flag411prime-)
(not (Flag344-))
(Flag344prime-)
(not (Flag377-))
(Flag377prime-)
(not (Flag486-))
(Flag486prime-)
(not (Flag441-))
(Flag441prime-)
(not (Flag300-))
(Flag300prime-)
(not (Flag455-))
(Flag455prime-)
(not (Flag467-))
(Flag467prime-)
(not (Flag319-))
(Flag319prime-)
(not (Flag354-))
(Flag354prime-)
(not (Flag427-))
(Flag427prime-)
(not (Flag308-))
(Flag308prime-)
(not (Flag466-))
(Flag466prime-)
(not (Flag428-))
(Flag428prime-)
(not (Flag357-))
(Flag357prime-)
(not (Flag429-))
(Flag429prime-)
(not (Flag413-))
(Flag413prime-)
(not (Flag397-))
(Flag397prime-)
(not (Flag478-))
(Flag478prime-)
(not (Flag414-))
(Flag414prime-)
(not (Flag457-))
(Flag457prime-)
(not (Flag445-))
(Flag445prime-)
(not (Flag431-))
(Flag431prime-)
(not (Flag405-))
(Flag405prime-)
(not (Flag468-))
(Flag468prime-)
(not (Flag363-))
(Flag363prime-)
(not (Flag487-))
(Flag487prime-)
(not (Flag321-))
(Flag321prime-)
(not (Flag469-))
(Flag469prime-)
(not (Flag398-))
(Flag398prime-)
(not (Flag369-))
(Flag369prime-)
(not (Flag495-))
(Flag495prime-)
(not (Flag479-))
(Flag479prime-)
(not (Flag352-))
(Flag352prime-)
(not (Flag416-))
(Flag416prime-)
(not (Flag459-))
(Flag459prime-)
(not (Flag299-))
(Flag299prime-)
(not (Flag375-))
(Flag375prime-)
(not (Flag400-))
(Flag400prime-)
(not (Flag418-))
(Flag418prime-)
(not (Flag401-))
(Flag401prime-)
(not (Flag447-))
(Flag447prime-)
(not (Flag379-))
(Flag379prime-)
(not (Flag496-))
(Flag496prime-)
(not (Flag489-))
(Flag489prime-)
(not (Flag340-))
(Flag340prime-)
(not (Flag470-))
(Flag470prime-)
(not (Flag497-))
(Flag497prime-)
(not (Flag471-))
(Flag471prime-)
(not (Flag315-))
(Flag315prime-)
(not (Flag498-))
(Flag498prime-)
(not (Flag382-))
(Flag382prime-)
(not (Flag499-))
(Flag499prime-)
(not (Flag419-))
(Flag419prime-)
(not (Flag341-))
(Flag341prime-)
(not (Flag481-))
(Flag481prime-)
(not (Flag449-))
(Flag449prime-)
(not (Flag436-))
(Flag436prime-)
(not (Flag373-))
(Flag373prime-)
(not (Flag406-))
(Flag406prime-)
(not (Flag314-))
(Flag314prime-)
(not (Flag306-))
(Flag306prime-)
(not (Flag362-))
(Flag362prime-)
(not (Flag462-))
(Flag462prime-)
(not (Flag463-))
(Flag463prime-)
(not (Flag490-))
(Flag490prime-)
(not (Flag390-))
(Flag390prime-)
(not (Flag500-))
(Flag500prime-)
(not (Flag333-))
(Flag333prime-)
(not (Flag483-))
(Flag483prime-)
(not (Flag438-))
(Flag438prime-)
(not (Flag460-))
(Flag460prime-)
(not (Flag450-))
(Flag450prime-)
)
:effect
(and
(Flag501prime-)
(not (Flag501-))
)

)
(:action Flag507Action-0
:parameters ()
:precondition
(and
(Flag476-)
(Flag476prime-)
)
:effect
(and
(Flag507-)
(Flag507prime-)
)

)
(:action Flag507Action-1
:parameters ()
:precondition
(and
(Flag375-)
(Flag375prime-)
)
:effect
(and
(Flag507-)
(Flag507prime-)
)

)
(:action Flag507Action-2
:parameters ()
:precondition
(and
(Flag299-)
(Flag299prime-)
)
:effect
(and
(Flag507-)
(Flag507prime-)
)

)
(:action Flag507Action-3
:parameters ()
:precondition
(and
(Flag469-)
(Flag469prime-)
)
:effect
(and
(Flag507-)
(Flag507prime-)
)

)
(:action Flag507Action-4
:parameters ()
:precondition
(and
(Flag308-)
(Flag308prime-)
)
:effect
(and
(Flag507-)
(Flag507prime-)
)

)
(:action Flag507Action-5
:parameters ()
:precondition
(and
(Flag466-)
(Flag466prime-)
)
:effect
(and
(Flag507-)
(Flag507prime-)
)

)
(:action Flag507Action-6
:parameters ()
:precondition
(and
(Flag428-)
(Flag428prime-)
)
:effect
(and
(Flag507-)
(Flag507prime-)
)

)
(:action Flag507Action-7
:parameters ()
:precondition
(and
(Flag386-)
(Flag386prime-)
)
:effect
(and
(Flag507-)
(Flag507prime-)
)

)
(:action Flag507Action-8
:parameters ()
:precondition
(and
(Flag302-)
(Flag302prime-)
)
:effect
(and
(Flag507-)
(Flag507prime-)
)

)
(:action Flag507Action-9
:parameters ()
:precondition
(and
(Flag359-)
(Flag359prime-)
)
:effect
(and
(Flag507-)
(Flag507prime-)
)

)
(:action Flag507Action-10
:parameters ()
:precondition
(and
(Flag416-)
(Flag416prime-)
)
:effect
(and
(Flag507-)
(Flag507prime-)
)

)
(:action Flag507Action-11
:parameters ()
:precondition
(and
(Flag401-)
(Flag401prime-)
)
:effect
(and
(Flag507-)
(Flag507prime-)
)

)
(:action Flag507Action-12
:parameters ()
:precondition
(and
(Flag413-)
(Flag413prime-)
)
:effect
(and
(Flag507-)
(Flag507prime-)
)

)
(:action Flag507Action-13
:parameters ()
:precondition
(and
(Flag478-)
(Flag478prime-)
)
:effect
(and
(Flag507-)
(Flag507prime-)
)

)
(:action Flag507Action-14
:parameters ()
:precondition
(and
(Flag411-)
(Flag411prime-)
)
:effect
(and
(Flag507-)
(Flag507prime-)
)

)
(:action Flag507Action-15
:parameters ()
:precondition
(and
(Flag447-)
(Flag447prime-)
)
:effect
(and
(Flag507-)
(Flag507prime-)
)

)
(:action Flag507Action-16
:parameters ()
:precondition
(and
(Flag502-)
(Flag502prime-)
)
:effect
(and
(Flag507-)
(Flag507prime-)
)

)
(:action Flag507Action-17
:parameters ()
:precondition
(and
(Flag414-)
(Flag414prime-)
)
:effect
(and
(Flag507-)
(Flag507prime-)
)

)
(:action Flag507Action-18
:parameters ()
:precondition
(and
(Flag379-)
(Flag379prime-)
)
:effect
(and
(Flag507-)
(Flag507prime-)
)

)
(:action Flag507Action-19
:parameters ()
:precondition
(and
(Flag494-)
(Flag494prime-)
)
:effect
(and
(Flag507-)
(Flag507prime-)
)

)
(:action Flag507Action-20
:parameters ()
:precondition
(and
(Flag462-)
(Flag462prime-)
)
:effect
(and
(Flag507-)
(Flag507prime-)
)

)
(:action Flag507Action-21
:parameters ()
:precondition
(and
(Flag314-)
(Flag314prime-)
)
:effect
(and
(Flag507-)
(Flag507prime-)
)

)
(:action Flag507Action-22
:parameters ()
:precondition
(and
(Flag503-)
(Flag503prime-)
)
:effect
(and
(Flag507-)
(Flag507prime-)
)

)
(:action Flag507Action-23
:parameters ()
:precondition
(and
(Flag306-)
(Flag306prime-)
)
:effect
(and
(Flag507-)
(Flag507prime-)
)

)
(:action Flag507Action-24
:parameters ()
:precondition
(and
(Flag340-)
(Flag340prime-)
)
:effect
(and
(Flag507-)
(Flag507prime-)
)

)
(:action Flag507Action-25
:parameters ()
:precondition
(and
(Flag457-)
(Flag457prime-)
)
:effect
(and
(Flag507-)
(Flag507prime-)
)

)
(:action Flag507Action-26
:parameters ()
:precondition
(and
(Flag496-)
(Flag496prime-)
)
:effect
(and
(Flag507-)
(Flag507prime-)
)

)
(:action Flag507Action-27
:parameters ()
:precondition
(and
(Flag363-)
(Flag363prime-)
)
:effect
(and
(Flag507-)
(Flag507prime-)
)

)
(:action Flag507Action-28
:parameters ()
:precondition
(and
(Flag315-)
(Flag315prime-)
)
:effect
(and
(Flag507-)
(Flag507prime-)
)

)
(:action Flag507Action-29
:parameters ()
:precondition
(and
(Flag489-)
(Flag489prime-)
)
:effect
(and
(Flag507-)
(Flag507prime-)
)

)
(:action Flag507Action-30
:parameters ()
:precondition
(and
(Flag342-)
(Flag342prime-)
)
:effect
(and
(Flag507-)
(Flag507prime-)
)

)
(:action Flag507Action-31
:parameters ()
:precondition
(and
(Flag445-)
(Flag445prime-)
)
:effect
(and
(Flag507-)
(Flag507prime-)
)

)
(:action Flag507Action-32
:parameters ()
:precondition
(and
(Flag463-)
(Flag463prime-)
)
:effect
(and
(Flag507-)
(Flag507prime-)
)

)
(:action Flag507Action-33
:parameters ()
:precondition
(and
(Flag312-)
(Flag312prime-)
)
:effect
(and
(Flag507-)
(Flag507prime-)
)

)
(:action Flag507Action-34
:parameters ()
:precondition
(and
(Flag322-)
(Flag322prime-)
)
:effect
(and
(Flag507-)
(Flag507prime-)
)

)
(:action Flag507Action-35
:parameters ()
:precondition
(and
(Flag393-)
(Flag393prime-)
)
:effect
(and
(Flag507-)
(Flag507prime-)
)

)
(:action Flag507Action-36
:parameters ()
:precondition
(and
(Flag490-)
(Flag490prime-)
)
:effect
(and
(Flag507-)
(Flag507prime-)
)

)
(:action Flag507Action-37
:parameters ()
:precondition
(and
(Flag400-)
(Flag400prime-)
)
:effect
(and
(Flag507-)
(Flag507prime-)
)

)
(:action Flag507Action-38
:parameters ()
:precondition
(and
(Flag390-)
(Flag390prime-)
)
:effect
(and
(Flag507-)
(Flag507prime-)
)

)
(:action Flag507Action-39
:parameters ()
:precondition
(and
(Flag405-)
(Flag405prime-)
)
:effect
(and
(Flag507-)
(Flag507prime-)
)

)
(:action Flag507Action-40
:parameters ()
:precondition
(and
(Flag500-)
(Flag500prime-)
)
:effect
(and
(Flag507-)
(Flag507prime-)
)

)
(:action Flag507Action-41
:parameters ()
:precondition
(and
(Flag440-)
(Flag440prime-)
)
:effect
(and
(Flag507-)
(Flag507prime-)
)

)
(:action Flag507Action-42
:parameters ()
:precondition
(and
(Flag485-)
(Flag485prime-)
)
:effect
(and
(Flag507-)
(Flag507prime-)
)

)
(:action Flag507Action-43
:parameters ()
:precondition
(and
(Flag431-)
(Flag431prime-)
)
:effect
(and
(Flag507-)
(Flag507prime-)
)

)
(:action Flag507Action-44
:parameters ()
:precondition
(and
(Flag468-)
(Flag468prime-)
)
:effect
(and
(Flag507-)
(Flag507prime-)
)

)
(:action Flag507Action-45
:parameters ()
:precondition
(and
(Flag300-)
(Flag300prime-)
)
:effect
(and
(Flag507-)
(Flag507prime-)
)

)
(:action Flag507Action-46
:parameters ()
:precondition
(and
(Flag487-)
(Flag487prime-)
)
:effect
(and
(Flag507-)
(Flag507prime-)
)

)
(:action Flag507Action-47
:parameters ()
:precondition
(and
(Flag344-)
(Flag344prime-)
)
:effect
(and
(Flag507-)
(Flag507prime-)
)

)
(:action Flag507Action-48
:parameters ()
:precondition
(and
(Flag483-)
(Flag483prime-)
)
:effect
(and
(Flag507-)
(Flag507prime-)
)

)
(:action Flag507Action-49
:parameters ()
:precondition
(and
(Flag321-)
(Flag321prime-)
)
:effect
(and
(Flag507-)
(Flag507prime-)
)

)
(:action Flag507Action-50
:parameters ()
:precondition
(and
(Flag499-)
(Flag499prime-)
)
:effect
(and
(Flag507-)
(Flag507prime-)
)

)
(:action Flag507Action-51
:parameters ()
:precondition
(and
(Flag373-)
(Flag373prime-)
)
:effect
(and
(Flag507-)
(Flag507prime-)
)

)
(:action Flag507Action-52
:parameters ()
:precondition
(and
(Flag398-)
(Flag398prime-)
)
:effect
(and
(Flag507-)
(Flag507prime-)
)

)
(:action Flag507Action-53
:parameters ()
:precondition
(and
(Flag438-)
(Flag438prime-)
)
:effect
(and
(Flag507-)
(Flag507prime-)
)

)
(:action Flag507Action-54
:parameters ()
:precondition
(and
(Flag377-)
(Flag377prime-)
)
:effect
(and
(Flag507-)
(Flag507prime-)
)

)
(:action Flag507Action-55
:parameters ()
:precondition
(and
(Flag486-)
(Flag486prime-)
)
:effect
(and
(Flag507-)
(Flag507prime-)
)

)
(:action Flag507Action-56
:parameters ()
:precondition
(and
(Flag460-)
(Flag460prime-)
)
:effect
(and
(Flag507-)
(Flag507prime-)
)

)
(:action Flag507Action-57
:parameters ()
:precondition
(and
(Flag471-)
(Flag471prime-)
)
:effect
(and
(Flag507-)
(Flag507prime-)
)

)
(:action Flag507Action-58
:parameters ()
:precondition
(and
(Flag362-)
(Flag362prime-)
)
:effect
(and
(Flag507-)
(Flag507prime-)
)

)
(:action Flag507Action-59
:parameters ()
:precondition
(and
(Flag369-)
(Flag369prime-)
)
:effect
(and
(Flag507-)
(Flag507prime-)
)

)
(:action Flag507Action-60
:parameters ()
:precondition
(and
(Flag382-)
(Flag382prime-)
)
:effect
(and
(Flag507-)
(Flag507prime-)
)

)
(:action Flag507Action-61
:parameters ()
:precondition
(and
(Flag455-)
(Flag455prime-)
)
:effect
(and
(Flag507-)
(Flag507prime-)
)

)
(:action Flag507Action-62
:parameters ()
:precondition
(and
(Flag495-)
(Flag495prime-)
)
:effect
(and
(Flag507-)
(Flag507prime-)
)

)
(:action Flag507Action-63
:parameters ()
:precondition
(and
(Flag450-)
(Flag450prime-)
)
:effect
(and
(Flag507-)
(Flag507prime-)
)

)
(:action Flag507Action-64
:parameters ()
:precondition
(and
(Flag479-)
(Flag479prime-)
)
:effect
(and
(Flag507-)
(Flag507prime-)
)

)
(:action Flag507Action-65
:parameters ()
:precondition
(and
(Flag410-)
(Flag410prime-)
)
:effect
(and
(Flag507-)
(Flag507prime-)
)

)
(:action Flag507Action-66
:parameters ()
:precondition
(and
(Flag419-)
(Flag419prime-)
)
:effect
(and
(Flag507-)
(Flag507prime-)
)

)
(:action Flag507Action-67
:parameters ()
:precondition
(and
(Flag467-)
(Flag467prime-)
)
:effect
(and
(Flag507-)
(Flag507prime-)
)

)
(:action Flag507Action-68
:parameters ()
:precondition
(and
(Flag341-)
(Flag341prime-)
)
:effect
(and
(Flag507-)
(Flag507prime-)
)

)
(:action Flag507Action-69
:parameters ()
:precondition
(and
(Flag481-)
(Flag481prime-)
)
:effect
(and
(Flag507-)
(Flag507prime-)
)

)
(:action Flag507Action-70
:parameters ()
:precondition
(and
(Flag504-)
(Flag504prime-)
)
:effect
(and
(Flag507-)
(Flag507prime-)
)

)
(:action Flag507Action-71
:parameters ()
:precondition
(and
(Flag449-)
(Flag449prime-)
)
:effect
(and
(Flag507-)
(Flag507prime-)
)

)
(:action Flag507Action-72
:parameters ()
:precondition
(and
(Flag333-)
(Flag333prime-)
)
:effect
(and
(Flag507-)
(Flag507prime-)
)

)
(:action Flag507Action-73
:parameters ()
:precondition
(and
(Flag436-)
(Flag436prime-)
)
:effect
(and
(Flag507-)
(Flag507prime-)
)

)
(:action Flag507Action-74
:parameters ()
:precondition
(and
(Flag354-)
(Flag354prime-)
)
:effect
(and
(Flag507-)
(Flag507prime-)
)

)
(:action Flag507Action-75
:parameters ()
:precondition
(and
(Flag505-)
(Flag505prime-)
)
:effect
(and
(Flag507-)
(Flag507prime-)
)

)
(:action Flag507Action-76
:parameters ()
:precondition
(and
(Flag427-)
(Flag427prime-)
)
:effect
(and
(Flag507-)
(Flag507prime-)
)

)
(:action Flag507Action-77
:parameters ()
:precondition
(and
(Flag506-)
(Flag506prime-)
)
:effect
(and
(Flag507-)
(Flag507prime-)
)

)
(:action Prim507Action
:parameters ()
:precondition
(and
(not (Flag476-))
(Flag476prime-)
(not (Flag375-))
(Flag375prime-)
(not (Flag299-))
(Flag299prime-)
(not (Flag469-))
(Flag469prime-)
(not (Flag308-))
(Flag308prime-)
(not (Flag466-))
(Flag466prime-)
(not (Flag428-))
(Flag428prime-)
(not (Flag386-))
(Flag386prime-)
(not (Flag302-))
(Flag302prime-)
(not (Flag359-))
(Flag359prime-)
(not (Flag416-))
(Flag416prime-)
(not (Flag401-))
(Flag401prime-)
(not (Flag413-))
(Flag413prime-)
(not (Flag478-))
(Flag478prime-)
(not (Flag411-))
(Flag411prime-)
(not (Flag447-))
(Flag447prime-)
(not (Flag502-))
(Flag502prime-)
(not (Flag414-))
(Flag414prime-)
(not (Flag379-))
(Flag379prime-)
(not (Flag494-))
(Flag494prime-)
(not (Flag462-))
(Flag462prime-)
(not (Flag314-))
(Flag314prime-)
(not (Flag503-))
(Flag503prime-)
(not (Flag306-))
(Flag306prime-)
(not (Flag340-))
(Flag340prime-)
(not (Flag457-))
(Flag457prime-)
(not (Flag496-))
(Flag496prime-)
(not (Flag363-))
(Flag363prime-)
(not (Flag315-))
(Flag315prime-)
(not (Flag489-))
(Flag489prime-)
(not (Flag342-))
(Flag342prime-)
(not (Flag445-))
(Flag445prime-)
(not (Flag463-))
(Flag463prime-)
(not (Flag312-))
(Flag312prime-)
(not (Flag322-))
(Flag322prime-)
(not (Flag393-))
(Flag393prime-)
(not (Flag490-))
(Flag490prime-)
(not (Flag400-))
(Flag400prime-)
(not (Flag390-))
(Flag390prime-)
(not (Flag405-))
(Flag405prime-)
(not (Flag500-))
(Flag500prime-)
(not (Flag440-))
(Flag440prime-)
(not (Flag485-))
(Flag485prime-)
(not (Flag431-))
(Flag431prime-)
(not (Flag468-))
(Flag468prime-)
(not (Flag300-))
(Flag300prime-)
(not (Flag487-))
(Flag487prime-)
(not (Flag344-))
(Flag344prime-)
(not (Flag483-))
(Flag483prime-)
(not (Flag321-))
(Flag321prime-)
(not (Flag499-))
(Flag499prime-)
(not (Flag373-))
(Flag373prime-)
(not (Flag398-))
(Flag398prime-)
(not (Flag438-))
(Flag438prime-)
(not (Flag377-))
(Flag377prime-)
(not (Flag486-))
(Flag486prime-)
(not (Flag460-))
(Flag460prime-)
(not (Flag471-))
(Flag471prime-)
(not (Flag362-))
(Flag362prime-)
(not (Flag369-))
(Flag369prime-)
(not (Flag382-))
(Flag382prime-)
(not (Flag455-))
(Flag455prime-)
(not (Flag495-))
(Flag495prime-)
(not (Flag450-))
(Flag450prime-)
(not (Flag479-))
(Flag479prime-)
(not (Flag410-))
(Flag410prime-)
(not (Flag419-))
(Flag419prime-)
(not (Flag467-))
(Flag467prime-)
(not (Flag341-))
(Flag341prime-)
(not (Flag481-))
(Flag481prime-)
(not (Flag504-))
(Flag504prime-)
(not (Flag449-))
(Flag449prime-)
(not (Flag333-))
(Flag333prime-)
(not (Flag436-))
(Flag436prime-)
(not (Flag354-))
(Flag354prime-)
(not (Flag505-))
(Flag505prime-)
(not (Flag427-))
(Flag427prime-)
(not (Flag506-))
(Flag506prime-)
)
:effect
(and
(Flag507prime-)
(not (Flag507-))
)

)
(:action Flag512Action-0
:parameters ()
:precondition
(and
(Flag476-)
(Flag476prime-)
)
:effect
(and
(Flag512-)
(Flag512prime-)
)

)
(:action Flag512Action-1
:parameters ()
:precondition
(and
(Flag375-)
(Flag375prime-)
)
:effect
(and
(Flag512-)
(Flag512prime-)
)

)
(:action Flag512Action-2
:parameters ()
:precondition
(and
(Flag312-)
(Flag312prime-)
)
:effect
(and
(Flag512-)
(Flag512prime-)
)

)
(:action Flag512Action-3
:parameters ()
:precondition
(and
(Flag308-)
(Flag308prime-)
)
:effect
(and
(Flag512-)
(Flag512prime-)
)

)
(:action Flag512Action-4
:parameters ()
:precondition
(and
(Flag466-)
(Flag466prime-)
)
:effect
(and
(Flag512-)
(Flag512prime-)
)

)
(:action Flag512Action-5
:parameters ()
:precondition
(and
(Flag428-)
(Flag428prime-)
)
:effect
(and
(Flag512-)
(Flag512prime-)
)

)
(:action Flag512Action-6
:parameters ()
:precondition
(and
(Flag386-)
(Flag386prime-)
)
:effect
(and
(Flag512-)
(Flag512prime-)
)

)
(:action Flag512Action-7
:parameters ()
:precondition
(and
(Flag302-)
(Flag302prime-)
)
:effect
(and
(Flag512-)
(Flag512prime-)
)

)
(:action Flag512Action-8
:parameters ()
:precondition
(and
(Flag413-)
(Flag413prime-)
)
:effect
(and
(Flag512-)
(Flag512prime-)
)

)
(:action Flag512Action-9
:parameters ()
:precondition
(and
(Flag478-)
(Flag478prime-)
)
:effect
(and
(Flag512-)
(Flag512prime-)
)

)
(:action Flag512Action-10
:parameters ()
:precondition
(and
(Flag411-)
(Flag411prime-)
)
:effect
(and
(Flag512-)
(Flag512prime-)
)

)
(:action Flag512Action-11
:parameters ()
:precondition
(and
(Flag447-)
(Flag447prime-)
)
:effect
(and
(Flag512-)
(Flag512prime-)
)

)
(:action Flag512Action-12
:parameters ()
:precondition
(and
(Flag414-)
(Flag414prime-)
)
:effect
(and
(Flag512-)
(Flag512prime-)
)

)
(:action Flag512Action-13
:parameters ()
:precondition
(and
(Flag379-)
(Flag379prime-)
)
:effect
(and
(Flag512-)
(Flag512prime-)
)

)
(:action Flag512Action-14
:parameters ()
:precondition
(and
(Flag494-)
(Flag494prime-)
)
:effect
(and
(Flag512-)
(Flag512prime-)
)

)
(:action Flag512Action-15
:parameters ()
:precondition
(and
(Flag462-)
(Flag462prime-)
)
:effect
(and
(Flag512-)
(Flag512prime-)
)

)
(:action Flag512Action-16
:parameters ()
:precondition
(and
(Flag314-)
(Flag314prime-)
)
:effect
(and
(Flag512-)
(Flag512prime-)
)

)
(:action Flag512Action-17
:parameters ()
:precondition
(and
(Flag503-)
(Flag503prime-)
)
:effect
(and
(Flag512-)
(Flag512prime-)
)

)
(:action Flag512Action-18
:parameters ()
:precondition
(and
(Flag306-)
(Flag306prime-)
)
:effect
(and
(Flag512-)
(Flag512prime-)
)

)
(:action Flag512Action-19
:parameters ()
:precondition
(and
(Flag340-)
(Flag340prime-)
)
:effect
(and
(Flag512-)
(Flag512prime-)
)

)
(:action Flag512Action-20
:parameters ()
:precondition
(and
(Flag362-)
(Flag362prime-)
)
:effect
(and
(Flag512-)
(Flag512prime-)
)

)
(:action Flag512Action-21
:parameters ()
:precondition
(and
(Flag363-)
(Flag363prime-)
)
:effect
(and
(Flag512-)
(Flag512prime-)
)

)
(:action Flag512Action-22
:parameters ()
:precondition
(and
(Flag505-)
(Flag505prime-)
)
:effect
(and
(Flag512-)
(Flag512prime-)
)

)
(:action Flag512Action-23
:parameters ()
:precondition
(and
(Flag342-)
(Flag342prime-)
)
:effect
(and
(Flag512-)
(Flag512prime-)
)

)
(:action Flag512Action-24
:parameters ()
:precondition
(and
(Flag359-)
(Flag359prime-)
)
:effect
(and
(Flag512-)
(Flag512prime-)
)

)
(:action Flag512Action-25
:parameters ()
:precondition
(and
(Flag463-)
(Flag463prime-)
)
:effect
(and
(Flag512-)
(Flag512prime-)
)

)
(:action Flag512Action-26
:parameters ()
:precondition
(and
(Flag499-)
(Flag499prime-)
)
:effect
(and
(Flag512-)
(Flag512prime-)
)

)
(:action Flag512Action-27
:parameters ()
:precondition
(and
(Flag431-)
(Flag431prime-)
)
:effect
(and
(Flag512-)
(Flag512prime-)
)

)
(:action Flag512Action-28
:parameters ()
:precondition
(and
(Flag322-)
(Flag322prime-)
)
:effect
(and
(Flag512-)
(Flag512prime-)
)

)
(:action Flag512Action-29
:parameters ()
:precondition
(and
(Flag393-)
(Flag393prime-)
)
:effect
(and
(Flag512-)
(Flag512prime-)
)

)
(:action Flag512Action-30
:parameters ()
:precondition
(and
(Flag490-)
(Flag490prime-)
)
:effect
(and
(Flag512-)
(Flag512prime-)
)

)
(:action Flag512Action-31
:parameters ()
:precondition
(and
(Flag508-)
(Flag508prime-)
)
:effect
(and
(Flag512-)
(Flag512prime-)
)

)
(:action Flag512Action-32
:parameters ()
:precondition
(and
(Flag405-)
(Flag405prime-)
)
:effect
(and
(Flag512-)
(Flag512prime-)
)

)
(:action Flag512Action-33
:parameters ()
:precondition
(and
(Flag500-)
(Flag500prime-)
)
:effect
(and
(Flag512-)
(Flag512prime-)
)

)
(:action Flag512Action-34
:parameters ()
:precondition
(and
(Flag440-)
(Flag440prime-)
)
:effect
(and
(Flag512-)
(Flag512prime-)
)

)
(:action Flag512Action-35
:parameters ()
:precondition
(and
(Flag485-)
(Flag485prime-)
)
:effect
(and
(Flag512-)
(Flag512prime-)
)

)
(:action Flag512Action-36
:parameters ()
:precondition
(and
(Flag509-)
(Flag509prime-)
)
:effect
(and
(Flag512-)
(Flag512prime-)
)

)
(:action Flag512Action-37
:parameters ()
:precondition
(and
(Flag468-)
(Flag468prime-)
)
:effect
(and
(Flag512-)
(Flag512prime-)
)

)
(:action Flag512Action-38
:parameters ()
:precondition
(and
(Flag300-)
(Flag300prime-)
)
:effect
(and
(Flag512-)
(Flag512prime-)
)

)
(:action Flag512Action-39
:parameters ()
:precondition
(and
(Flag487-)
(Flag487prime-)
)
:effect
(and
(Flag512-)
(Flag512prime-)
)

)
(:action Flag512Action-40
:parameters ()
:precondition
(and
(Flag344-)
(Flag344prime-)
)
:effect
(and
(Flag512-)
(Flag512prime-)
)

)
(:action Flag512Action-41
:parameters ()
:precondition
(and
(Flag483-)
(Flag483prime-)
)
:effect
(and
(Flag512-)
(Flag512prime-)
)

)
(:action Flag512Action-42
:parameters ()
:precondition
(and
(Flag469-)
(Flag469prime-)
)
:effect
(and
(Flag512-)
(Flag512prime-)
)

)
(:action Flag512Action-43
:parameters ()
:precondition
(and
(Flag398-)
(Flag398prime-)
)
:effect
(and
(Flag512-)
(Flag512prime-)
)

)
(:action Flag512Action-44
:parameters ()
:precondition
(and
(Flag438-)
(Flag438prime-)
)
:effect
(and
(Flag512-)
(Flag512prime-)
)

)
(:action Flag512Action-45
:parameters ()
:precondition
(and
(Flag510-)
(Flag510prime-)
)
:effect
(and
(Flag512-)
(Flag512prime-)
)

)
(:action Flag512Action-46
:parameters ()
:precondition
(and
(Flag377-)
(Flag377prime-)
)
:effect
(and
(Flag512-)
(Flag512prime-)
)

)
(:action Flag512Action-47
:parameters ()
:precondition
(and
(Flag486-)
(Flag486prime-)
)
:effect
(and
(Flag512-)
(Flag512prime-)
)

)
(:action Flag512Action-48
:parameters ()
:precondition
(and
(Flag460-)
(Flag460prime-)
)
:effect
(and
(Flag512-)
(Flag512prime-)
)

)
(:action Flag512Action-49
:parameters ()
:precondition
(and
(Flag369-)
(Flag369prime-)
)
:effect
(and
(Flag512-)
(Flag512prime-)
)

)
(:action Flag512Action-50
:parameters ()
:precondition
(and
(Flag299-)
(Flag299prime-)
)
:effect
(and
(Flag512-)
(Flag512prime-)
)

)
(:action Flag512Action-51
:parameters ()
:precondition
(and
(Flag382-)
(Flag382prime-)
)
:effect
(and
(Flag512-)
(Flag512prime-)
)

)
(:action Flag512Action-52
:parameters ()
:precondition
(and
(Flag455-)
(Flag455prime-)
)
:effect
(and
(Flag512-)
(Flag512prime-)
)

)
(:action Flag512Action-53
:parameters ()
:precondition
(and
(Flag495-)
(Flag495prime-)
)
:effect
(and
(Flag512-)
(Flag512prime-)
)

)
(:action Flag512Action-54
:parameters ()
:precondition
(and
(Flag450-)
(Flag450prime-)
)
:effect
(and
(Flag512-)
(Flag512prime-)
)

)
(:action Flag512Action-55
:parameters ()
:precondition
(and
(Flag479-)
(Flag479prime-)
)
:effect
(and
(Flag512-)
(Flag512prime-)
)

)
(:action Flag512Action-56
:parameters ()
:precondition
(and
(Flag419-)
(Flag419prime-)
)
:effect
(and
(Flag512-)
(Flag512prime-)
)

)
(:action Flag512Action-57
:parameters ()
:precondition
(and
(Flag467-)
(Flag467prime-)
)
:effect
(and
(Flag512-)
(Flag512prime-)
)

)
(:action Flag512Action-58
:parameters ()
:precondition
(and
(Flag400-)
(Flag400prime-)
)
:effect
(and
(Flag512-)
(Flag512prime-)
)

)
(:action Flag512Action-59
:parameters ()
:precondition
(and
(Flag416-)
(Flag416prime-)
)
:effect
(and
(Flag512-)
(Flag512prime-)
)

)
(:action Flag512Action-60
:parameters ()
:precondition
(and
(Flag511-)
(Flag511prime-)
)
:effect
(and
(Flag512-)
(Flag512prime-)
)

)
(:action Flag512Action-61
:parameters ()
:precondition
(and
(Flag504-)
(Flag504prime-)
)
:effect
(and
(Flag512-)
(Flag512prime-)
)

)
(:action Flag512Action-62
:parameters ()
:precondition
(and
(Flag449-)
(Flag449prime-)
)
:effect
(and
(Flag512-)
(Flag512prime-)
)

)
(:action Flag512Action-63
:parameters ()
:precondition
(and
(Flag333-)
(Flag333prime-)
)
:effect
(and
(Flag512-)
(Flag512prime-)
)

)
(:action Flag512Action-64
:parameters ()
:precondition
(and
(Flag436-)
(Flag436prime-)
)
:effect
(and
(Flag512-)
(Flag512prime-)
)

)
(:action Flag512Action-65
:parameters ()
:precondition
(and
(Flag354-)
(Flag354prime-)
)
:effect
(and
(Flag512-)
(Flag512prime-)
)

)
(:action Flag512Action-66
:parameters ()
:precondition
(and
(Flag506-)
(Flag506prime-)
)
:effect
(and
(Flag512-)
(Flag512prime-)
)

)
(:action Prim512Action
:parameters ()
:precondition
(and
(not (Flag476-))
(Flag476prime-)
(not (Flag375-))
(Flag375prime-)
(not (Flag312-))
(Flag312prime-)
(not (Flag308-))
(Flag308prime-)
(not (Flag466-))
(Flag466prime-)
(not (Flag428-))
(Flag428prime-)
(not (Flag386-))
(Flag386prime-)
(not (Flag302-))
(Flag302prime-)
(not (Flag413-))
(Flag413prime-)
(not (Flag478-))
(Flag478prime-)
(not (Flag411-))
(Flag411prime-)
(not (Flag447-))
(Flag447prime-)
(not (Flag414-))
(Flag414prime-)
(not (Flag379-))
(Flag379prime-)
(not (Flag494-))
(Flag494prime-)
(not (Flag462-))
(Flag462prime-)
(not (Flag314-))
(Flag314prime-)
(not (Flag503-))
(Flag503prime-)
(not (Flag306-))
(Flag306prime-)
(not (Flag340-))
(Flag340prime-)
(not (Flag362-))
(Flag362prime-)
(not (Flag363-))
(Flag363prime-)
(not (Flag505-))
(Flag505prime-)
(not (Flag342-))
(Flag342prime-)
(not (Flag359-))
(Flag359prime-)
(not (Flag463-))
(Flag463prime-)
(not (Flag499-))
(Flag499prime-)
(not (Flag431-))
(Flag431prime-)
(not (Flag322-))
(Flag322prime-)
(not (Flag393-))
(Flag393prime-)
(not (Flag490-))
(Flag490prime-)
(not (Flag508-))
(Flag508prime-)
(not (Flag405-))
(Flag405prime-)
(not (Flag500-))
(Flag500prime-)
(not (Flag440-))
(Flag440prime-)
(not (Flag485-))
(Flag485prime-)
(not (Flag509-))
(Flag509prime-)
(not (Flag468-))
(Flag468prime-)
(not (Flag300-))
(Flag300prime-)
(not (Flag487-))
(Flag487prime-)
(not (Flag344-))
(Flag344prime-)
(not (Flag483-))
(Flag483prime-)
(not (Flag469-))
(Flag469prime-)
(not (Flag398-))
(Flag398prime-)
(not (Flag438-))
(Flag438prime-)
(not (Flag510-))
(Flag510prime-)
(not (Flag377-))
(Flag377prime-)
(not (Flag486-))
(Flag486prime-)
(not (Flag460-))
(Flag460prime-)
(not (Flag369-))
(Flag369prime-)
(not (Flag299-))
(Flag299prime-)
(not (Flag382-))
(Flag382prime-)
(not (Flag455-))
(Flag455prime-)
(not (Flag495-))
(Flag495prime-)
(not (Flag450-))
(Flag450prime-)
(not (Flag479-))
(Flag479prime-)
(not (Flag419-))
(Flag419prime-)
(not (Flag467-))
(Flag467prime-)
(not (Flag400-))
(Flag400prime-)
(not (Flag416-))
(Flag416prime-)
(not (Flag511-))
(Flag511prime-)
(not (Flag504-))
(Flag504prime-)
(not (Flag449-))
(Flag449prime-)
(not (Flag333-))
(Flag333prime-)
(not (Flag436-))
(Flag436prime-)
(not (Flag354-))
(Flag354prime-)
(not (Flag506-))
(Flag506prime-)
)
:effect
(and
(Flag512prime-)
(not (Flag512-))
)

)
(:action Flag516Action-0
:parameters ()
:precondition
(and
(Flag476-)
(Flag476prime-)
)
:effect
(and
(Flag516-)
(Flag516prime-)
)

)
(:action Flag516Action-1
:parameters ()
:precondition
(and
(Flag375-)
(Flag375prime-)
)
:effect
(and
(Flag516-)
(Flag516prime-)
)

)
(:action Flag516Action-2
:parameters ()
:precondition
(and
(Flag312-)
(Flag312prime-)
)
:effect
(and
(Flag516-)
(Flag516prime-)
)

)
(:action Flag516Action-3
:parameters ()
:precondition
(and
(Flag308-)
(Flag308prime-)
)
:effect
(and
(Flag516-)
(Flag516prime-)
)

)
(:action Flag516Action-4
:parameters ()
:precondition
(and
(Flag466-)
(Flag466prime-)
)
:effect
(and
(Flag516-)
(Flag516prime-)
)

)
(:action Flag516Action-5
:parameters ()
:precondition
(and
(Flag428-)
(Flag428prime-)
)
:effect
(and
(Flag516-)
(Flag516prime-)
)

)
(:action Flag516Action-6
:parameters ()
:precondition
(and
(Flag386-)
(Flag386prime-)
)
:effect
(and
(Flag516-)
(Flag516prime-)
)

)
(:action Flag516Action-7
:parameters ()
:precondition
(and
(Flag302-)
(Flag302prime-)
)
:effect
(and
(Flag516-)
(Flag516prime-)
)

)
(:action Flag516Action-8
:parameters ()
:precondition
(and
(Flag460-)
(Flag460prime-)
)
:effect
(and
(Flag516-)
(Flag516prime-)
)

)
(:action Flag516Action-9
:parameters ()
:precondition
(and
(Flag413-)
(Flag413prime-)
)
:effect
(and
(Flag516-)
(Flag516prime-)
)

)
(:action Flag516Action-10
:parameters ()
:precondition
(and
(Flag513-)
(Flag513prime-)
)
:effect
(and
(Flag516-)
(Flag516prime-)
)

)
(:action Flag516Action-11
:parameters ()
:precondition
(and
(Flag447-)
(Flag447prime-)
)
:effect
(and
(Flag516-)
(Flag516prime-)
)

)
(:action Flag516Action-12
:parameters ()
:precondition
(and
(Flag379-)
(Flag379prime-)
)
:effect
(and
(Flag516-)
(Flag516prime-)
)

)
(:action Flag516Action-13
:parameters ()
:precondition
(and
(Flag514-)
(Flag514prime-)
)
:effect
(and
(Flag516-)
(Flag516prime-)
)

)
(:action Flag516Action-14
:parameters ()
:precondition
(and
(Flag314-)
(Flag314prime-)
)
:effect
(and
(Flag516-)
(Flag516prime-)
)

)
(:action Flag516Action-15
:parameters ()
:precondition
(and
(Flag414-)
(Flag414prime-)
)
:effect
(and
(Flag516-)
(Flag516prime-)
)

)
(:action Flag516Action-16
:parameters ()
:precondition
(and
(Flag340-)
(Flag340prime-)
)
:effect
(and
(Flag516-)
(Flag516prime-)
)

)
(:action Flag516Action-17
:parameters ()
:precondition
(and
(Flag362-)
(Flag362prime-)
)
:effect
(and
(Flag516-)
(Flag516prime-)
)

)
(:action Flag516Action-18
:parameters ()
:precondition
(and
(Flag505-)
(Flag505prime-)
)
:effect
(and
(Flag516-)
(Flag516prime-)
)

)
(:action Flag516Action-19
:parameters ()
:precondition
(and
(Flag342-)
(Flag342prime-)
)
:effect
(and
(Flag516-)
(Flag516prime-)
)

)
(:action Flag516Action-20
:parameters ()
:precondition
(and
(Flag359-)
(Flag359prime-)
)
:effect
(and
(Flag516-)
(Flag516prime-)
)

)
(:action Flag516Action-21
:parameters ()
:precondition
(and
(Flag463-)
(Flag463prime-)
)
:effect
(and
(Flag516-)
(Flag516prime-)
)

)
(:action Flag516Action-22
:parameters ()
:precondition
(and
(Flag499-)
(Flag499prime-)
)
:effect
(and
(Flag516-)
(Flag516prime-)
)

)
(:action Flag516Action-23
:parameters ()
:precondition
(and
(Flag431-)
(Flag431prime-)
)
:effect
(and
(Flag516-)
(Flag516prime-)
)

)
(:action Flag516Action-24
:parameters ()
:precondition
(and
(Flag322-)
(Flag322prime-)
)
:effect
(and
(Flag516-)
(Flag516prime-)
)

)
(:action Flag516Action-25
:parameters ()
:precondition
(and
(Flag393-)
(Flag393prime-)
)
:effect
(and
(Flag516-)
(Flag516prime-)
)

)
(:action Flag516Action-26
:parameters ()
:precondition
(and
(Flag508-)
(Flag508prime-)
)
:effect
(and
(Flag516-)
(Flag516prime-)
)

)
(:action Flag516Action-27
:parameters ()
:precondition
(and
(Flag500-)
(Flag500prime-)
)
:effect
(and
(Flag516-)
(Flag516prime-)
)

)
(:action Flag516Action-28
:parameters ()
:precondition
(and
(Flag440-)
(Flag440prime-)
)
:effect
(and
(Flag516-)
(Flag516prime-)
)

)
(:action Flag516Action-29
:parameters ()
:precondition
(and
(Flag485-)
(Flag485prime-)
)
:effect
(and
(Flag516-)
(Flag516prime-)
)

)
(:action Flag516Action-30
:parameters ()
:precondition
(and
(Flag509-)
(Flag509prime-)
)
:effect
(and
(Flag516-)
(Flag516prime-)
)

)
(:action Flag516Action-31
:parameters ()
:precondition
(and
(Flag300-)
(Flag300prime-)
)
:effect
(and
(Flag516-)
(Flag516prime-)
)

)
(:action Flag516Action-32
:parameters ()
:precondition
(and
(Flag487-)
(Flag487prime-)
)
:effect
(and
(Flag516-)
(Flag516prime-)
)

)
(:action Flag516Action-33
:parameters ()
:precondition
(and
(Flag483-)
(Flag483prime-)
)
:effect
(and
(Flag516-)
(Flag516prime-)
)

)
(:action Flag516Action-34
:parameters ()
:precondition
(and
(Flag469-)
(Flag469prime-)
)
:effect
(and
(Flag516-)
(Flag516prime-)
)

)
(:action Flag516Action-35
:parameters ()
:precondition
(and
(Flag398-)
(Flag398prime-)
)
:effect
(and
(Flag516-)
(Flag516prime-)
)

)
(:action Flag516Action-36
:parameters ()
:precondition
(and
(Flag438-)
(Flag438prime-)
)
:effect
(and
(Flag516-)
(Flag516prime-)
)

)
(:action Flag516Action-37
:parameters ()
:precondition
(and
(Flag486-)
(Flag486prime-)
)
:effect
(and
(Flag516-)
(Flag516prime-)
)

)
(:action Flag516Action-38
:parameters ()
:precondition
(and
(Flag515-)
(Flag515prime-)
)
:effect
(and
(Flag516-)
(Flag516prime-)
)

)
(:action Flag516Action-39
:parameters ()
:precondition
(and
(Flag299-)
(Flag299prime-)
)
:effect
(and
(Flag516-)
(Flag516prime-)
)

)
(:action Flag516Action-40
:parameters ()
:precondition
(and
(Flag382-)
(Flag382prime-)
)
:effect
(and
(Flag516-)
(Flag516prime-)
)

)
(:action Flag516Action-41
:parameters ()
:precondition
(and
(Flag455-)
(Flag455prime-)
)
:effect
(and
(Flag516-)
(Flag516prime-)
)

)
(:action Flag516Action-42
:parameters ()
:precondition
(and
(Flag495-)
(Flag495prime-)
)
:effect
(and
(Flag516-)
(Flag516prime-)
)

)
(:action Flag516Action-43
:parameters ()
:precondition
(and
(Flag450-)
(Flag450prime-)
)
:effect
(and
(Flag516-)
(Flag516prime-)
)

)
(:action Flag516Action-44
:parameters ()
:precondition
(and
(Flag479-)
(Flag479prime-)
)
:effect
(and
(Flag516-)
(Flag516prime-)
)

)
(:action Flag516Action-45
:parameters ()
:precondition
(and
(Flag419-)
(Flag419prime-)
)
:effect
(and
(Flag516-)
(Flag516prime-)
)

)
(:action Flag516Action-46
:parameters ()
:precondition
(and
(Flag467-)
(Flag467prime-)
)
:effect
(and
(Flag516-)
(Flag516prime-)
)

)
(:action Flag516Action-47
:parameters ()
:precondition
(and
(Flag416-)
(Flag416prime-)
)
:effect
(and
(Flag516-)
(Flag516prime-)
)

)
(:action Flag516Action-48
:parameters ()
:precondition
(and
(Flag511-)
(Flag511prime-)
)
:effect
(and
(Flag516-)
(Flag516prime-)
)

)
(:action Flag516Action-49
:parameters ()
:precondition
(and
(Flag504-)
(Flag504prime-)
)
:effect
(and
(Flag516-)
(Flag516prime-)
)

)
(:action Flag516Action-50
:parameters ()
:precondition
(and
(Flag405-)
(Flag405prime-)
)
:effect
(and
(Flag516-)
(Flag516prime-)
)

)
(:action Flag516Action-51
:parameters ()
:precondition
(and
(Flag354-)
(Flag354prime-)
)
:effect
(and
(Flag516-)
(Flag516prime-)
)

)
(:action Flag516Action-52
:parameters ()
:precondition
(and
(Flag506-)
(Flag506prime-)
)
:effect
(and
(Flag516-)
(Flag516prime-)
)

)
(:action Prim516Action
:parameters ()
:precondition
(and
(not (Flag476-))
(Flag476prime-)
(not (Flag375-))
(Flag375prime-)
(not (Flag312-))
(Flag312prime-)
(not (Flag308-))
(Flag308prime-)
(not (Flag466-))
(Flag466prime-)
(not (Flag428-))
(Flag428prime-)
(not (Flag386-))
(Flag386prime-)
(not (Flag302-))
(Flag302prime-)
(not (Flag460-))
(Flag460prime-)
(not (Flag413-))
(Flag413prime-)
(not (Flag513-))
(Flag513prime-)
(not (Flag447-))
(Flag447prime-)
(not (Flag379-))
(Flag379prime-)
(not (Flag514-))
(Flag514prime-)
(not (Flag314-))
(Flag314prime-)
(not (Flag414-))
(Flag414prime-)
(not (Flag340-))
(Flag340prime-)
(not (Flag362-))
(Flag362prime-)
(not (Flag505-))
(Flag505prime-)
(not (Flag342-))
(Flag342prime-)
(not (Flag359-))
(Flag359prime-)
(not (Flag463-))
(Flag463prime-)
(not (Flag499-))
(Flag499prime-)
(not (Flag431-))
(Flag431prime-)
(not (Flag322-))
(Flag322prime-)
(not (Flag393-))
(Flag393prime-)
(not (Flag508-))
(Flag508prime-)
(not (Flag500-))
(Flag500prime-)
(not (Flag440-))
(Flag440prime-)
(not (Flag485-))
(Flag485prime-)
(not (Flag509-))
(Flag509prime-)
(not (Flag300-))
(Flag300prime-)
(not (Flag487-))
(Flag487prime-)
(not (Flag483-))
(Flag483prime-)
(not (Flag469-))
(Flag469prime-)
(not (Flag398-))
(Flag398prime-)
(not (Flag438-))
(Flag438prime-)
(not (Flag486-))
(Flag486prime-)
(not (Flag515-))
(Flag515prime-)
(not (Flag299-))
(Flag299prime-)
(not (Flag382-))
(Flag382prime-)
(not (Flag455-))
(Flag455prime-)
(not (Flag495-))
(Flag495prime-)
(not (Flag450-))
(Flag450prime-)
(not (Flag479-))
(Flag479prime-)
(not (Flag419-))
(Flag419prime-)
(not (Flag467-))
(Flag467prime-)
(not (Flag416-))
(Flag416prime-)
(not (Flag511-))
(Flag511prime-)
(not (Flag504-))
(Flag504prime-)
(not (Flag405-))
(Flag405prime-)
(not (Flag354-))
(Flag354prime-)
(not (Flag506-))
(Flag506prime-)
)
:effect
(and
(Flag516prime-)
(not (Flag516-))
)

)
(:action Flag519Action-0
:parameters ()
:precondition
(and
(Flag476-)
(Flag476prime-)
)
:effect
(and
(Flag519-)
(Flag519prime-)
)

)
(:action Flag519Action-1
:parameters ()
:precondition
(and
(Flag375-)
(Flag375prime-)
)
:effect
(and
(Flag519-)
(Flag519prime-)
)

)
(:action Flag519Action-2
:parameters ()
:precondition
(and
(Flag428-)
(Flag428prime-)
)
:effect
(and
(Flag519-)
(Flag519prime-)
)

)
(:action Flag519Action-3
:parameters ()
:precondition
(and
(Flag386-)
(Flag386prime-)
)
:effect
(and
(Flag519-)
(Flag519prime-)
)

)
(:action Flag519Action-4
:parameters ()
:precondition
(and
(Flag302-)
(Flag302prime-)
)
:effect
(and
(Flag519-)
(Flag519prime-)
)

)
(:action Flag519Action-5
:parameters ()
:precondition
(and
(Flag513-)
(Flag513prime-)
)
:effect
(and
(Flag519-)
(Flag519prime-)
)

)
(:action Flag519Action-6
:parameters ()
:precondition
(and
(Flag314-)
(Flag314prime-)
)
:effect
(and
(Flag519-)
(Flag519prime-)
)

)
(:action Flag519Action-7
:parameters ()
:precondition
(and
(Flag414-)
(Flag414prime-)
)
:effect
(and
(Flag519-)
(Flag519prime-)
)

)
(:action Flag519Action-8
:parameters ()
:precondition
(and
(Flag340-)
(Flag340prime-)
)
:effect
(and
(Flag519-)
(Flag519prime-)
)

)
(:action Flag519Action-9
:parameters ()
:precondition
(and
(Flag362-)
(Flag362prime-)
)
:effect
(and
(Flag519-)
(Flag519prime-)
)

)
(:action Flag519Action-10
:parameters ()
:precondition
(and
(Flag359-)
(Flag359prime-)
)
:effect
(and
(Flag519-)
(Flag519prime-)
)

)
(:action Flag519Action-11
:parameters ()
:precondition
(and
(Flag463-)
(Flag463prime-)
)
:effect
(and
(Flag519-)
(Flag519prime-)
)

)
(:action Flag519Action-12
:parameters ()
:precondition
(and
(Flag517-)
(Flag517prime-)
)
:effect
(and
(Flag519-)
(Flag519prime-)
)

)
(:action Flag519Action-13
:parameters ()
:precondition
(and
(Flag431-)
(Flag431prime-)
)
:effect
(and
(Flag519-)
(Flag519prime-)
)

)
(:action Flag519Action-14
:parameters ()
:precondition
(and
(Flag322-)
(Flag322prime-)
)
:effect
(and
(Flag519-)
(Flag519prime-)
)

)
(:action Flag519Action-15
:parameters ()
:precondition
(and
(Flag393-)
(Flag393prime-)
)
:effect
(and
(Flag519-)
(Flag519prime-)
)

)
(:action Flag519Action-16
:parameters ()
:precondition
(and
(Flag508-)
(Flag508prime-)
)
:effect
(and
(Flag519-)
(Flag519prime-)
)

)
(:action Flag519Action-17
:parameters ()
:precondition
(and
(Flag500-)
(Flag500prime-)
)
:effect
(and
(Flag519-)
(Flag519prime-)
)

)
(:action Flag519Action-18
:parameters ()
:precondition
(and
(Flag518-)
(Flag518prime-)
)
:effect
(and
(Flag519-)
(Flag519prime-)
)

)
(:action Flag519Action-19
:parameters ()
:precondition
(and
(Flag485-)
(Flag485prime-)
)
:effect
(and
(Flag519-)
(Flag519prime-)
)

)
(:action Flag519Action-20
:parameters ()
:precondition
(and
(Flag487-)
(Flag487prime-)
)
:effect
(and
(Flag519-)
(Flag519prime-)
)

)
(:action Flag519Action-21
:parameters ()
:precondition
(and
(Flag469-)
(Flag469prime-)
)
:effect
(and
(Flag519-)
(Flag519prime-)
)

)
(:action Flag519Action-22
:parameters ()
:precondition
(and
(Flag398-)
(Flag398prime-)
)
:effect
(and
(Flag519-)
(Flag519prime-)
)

)
(:action Flag519Action-23
:parameters ()
:precondition
(and
(Flag515-)
(Flag515prime-)
)
:effect
(and
(Flag519-)
(Flag519prime-)
)

)
(:action Flag519Action-24
:parameters ()
:precondition
(and
(Flag300-)
(Flag300prime-)
)
:effect
(and
(Flag519-)
(Flag519prime-)
)

)
(:action Flag519Action-25
:parameters ()
:precondition
(and
(Flag455-)
(Flag455prime-)
)
:effect
(and
(Flag519-)
(Flag519prime-)
)

)
(:action Flag519Action-26
:parameters ()
:precondition
(and
(Flag499-)
(Flag499prime-)
)
:effect
(and
(Flag519-)
(Flag519prime-)
)

)
(:action Flag519Action-27
:parameters ()
:precondition
(and
(Flag450-)
(Flag450prime-)
)
:effect
(and
(Flag519-)
(Flag519prime-)
)

)
(:action Flag519Action-28
:parameters ()
:precondition
(and
(Flag479-)
(Flag479prime-)
)
:effect
(and
(Flag519-)
(Flag519prime-)
)

)
(:action Flag519Action-29
:parameters ()
:precondition
(and
(Flag419-)
(Flag419prime-)
)
:effect
(and
(Flag519-)
(Flag519prime-)
)

)
(:action Flag519Action-30
:parameters ()
:precondition
(and
(Flag467-)
(Flag467prime-)
)
:effect
(and
(Flag519-)
(Flag519prime-)
)

)
(:action Flag519Action-31
:parameters ()
:precondition
(and
(Flag416-)
(Flag416prime-)
)
:effect
(and
(Flag519-)
(Flag519prime-)
)

)
(:action Flag519Action-32
:parameters ()
:precondition
(and
(Flag511-)
(Flag511prime-)
)
:effect
(and
(Flag519-)
(Flag519prime-)
)

)
(:action Flag519Action-33
:parameters ()
:precondition
(and
(Flag504-)
(Flag504prime-)
)
:effect
(and
(Flag519-)
(Flag519prime-)
)

)
(:action Flag519Action-34
:parameters ()
:precondition
(and
(Flag354-)
(Flag354prime-)
)
:effect
(and
(Flag519-)
(Flag519prime-)
)

)
(:action Flag519Action-35
:parameters ()
:precondition
(and
(Flag440-)
(Flag440prime-)
)
:effect
(and
(Flag519-)
(Flag519prime-)
)

)
(:action Flag519Action-36
:parameters ()
:precondition
(and
(Flag506-)
(Flag506prime-)
)
:effect
(and
(Flag519-)
(Flag519prime-)
)

)
(:action Prim519Action
:parameters ()
:precondition
(and
(not (Flag476-))
(Flag476prime-)
(not (Flag375-))
(Flag375prime-)
(not (Flag428-))
(Flag428prime-)
(not (Flag386-))
(Flag386prime-)
(not (Flag302-))
(Flag302prime-)
(not (Flag513-))
(Flag513prime-)
(not (Flag314-))
(Flag314prime-)
(not (Flag414-))
(Flag414prime-)
(not (Flag340-))
(Flag340prime-)
(not (Flag362-))
(Flag362prime-)
(not (Flag359-))
(Flag359prime-)
(not (Flag463-))
(Flag463prime-)
(not (Flag517-))
(Flag517prime-)
(not (Flag431-))
(Flag431prime-)
(not (Flag322-))
(Flag322prime-)
(not (Flag393-))
(Flag393prime-)
(not (Flag508-))
(Flag508prime-)
(not (Flag500-))
(Flag500prime-)
(not (Flag518-))
(Flag518prime-)
(not (Flag485-))
(Flag485prime-)
(not (Flag487-))
(Flag487prime-)
(not (Flag469-))
(Flag469prime-)
(not (Flag398-))
(Flag398prime-)
(not (Flag515-))
(Flag515prime-)
(not (Flag300-))
(Flag300prime-)
(not (Flag455-))
(Flag455prime-)
(not (Flag499-))
(Flag499prime-)
(not (Flag450-))
(Flag450prime-)
(not (Flag479-))
(Flag479prime-)
(not (Flag419-))
(Flag419prime-)
(not (Flag467-))
(Flag467prime-)
(not (Flag416-))
(Flag416prime-)
(not (Flag511-))
(Flag511prime-)
(not (Flag504-))
(Flag504prime-)
(not (Flag354-))
(Flag354prime-)
(not (Flag440-))
(Flag440prime-)
(not (Flag506-))
(Flag506prime-)
)
:effect
(and
(Flag519prime-)
(not (Flag519-))
)

)
(:action Flag521Action-0
:parameters ()
:precondition
(and
(Flag485-)
(Flag485prime-)
)
:effect
(and
(Flag521-)
(Flag521prime-)
)

)
(:action Flag521Action-1
:parameters ()
:precondition
(and
(Flag455-)
(Flag455prime-)
)
:effect
(and
(Flag521-)
(Flag521prime-)
)

)
(:action Flag521Action-2
:parameters ()
:precondition
(and
(Flag499-)
(Flag499prime-)
)
:effect
(and
(Flag521-)
(Flag521prime-)
)

)
(:action Flag521Action-3
:parameters ()
:precondition
(and
(Flag450-)
(Flag450prime-)
)
:effect
(and
(Flag521-)
(Flag521prime-)
)

)
(:action Flag521Action-4
:parameters ()
:precondition
(and
(Flag416-)
(Flag416prime-)
)
:effect
(and
(Flag521-)
(Flag521prime-)
)

)
(:action Flag521Action-5
:parameters ()
:precondition
(and
(Flag513-)
(Flag513prime-)
)
:effect
(and
(Flag521-)
(Flag521prime-)
)

)
(:action Flag521Action-6
:parameters ()
:precondition
(and
(Flag476-)
(Flag476prime-)
)
:effect
(and
(Flag521-)
(Flag521prime-)
)

)
(:action Flag521Action-7
:parameters ()
:precondition
(and
(Flag386-)
(Flag386prime-)
)
:effect
(and
(Flag521-)
(Flag521prime-)
)

)
(:action Flag521Action-8
:parameters ()
:precondition
(and
(Flag302-)
(Flag302prime-)
)
:effect
(and
(Flag521-)
(Flag521prime-)
)

)
(:action Flag521Action-9
:parameters ()
:precondition
(and
(Flag467-)
(Flag467prime-)
)
:effect
(and
(Flag521-)
(Flag521prime-)
)

)
(:action Flag521Action-10
:parameters ()
:precondition
(and
(Flag300-)
(Flag300prime-)
)
:effect
(and
(Flag521-)
(Flag521prime-)
)

)
(:action Flag521Action-11
:parameters ()
:precondition
(and
(Flag414-)
(Flag414prime-)
)
:effect
(and
(Flag521-)
(Flag521prime-)
)

)
(:action Flag521Action-12
:parameters ()
:precondition
(and
(Flag520-)
(Flag520prime-)
)
:effect
(and
(Flag521-)
(Flag521prime-)
)

)
(:action Flag521Action-13
:parameters ()
:precondition
(and
(Flag511-)
(Flag511prime-)
)
:effect
(and
(Flag521-)
(Flag521prime-)
)

)
(:action Flag521Action-14
:parameters ()
:precondition
(and
(Flag431-)
(Flag431prime-)
)
:effect
(and
(Flag521-)
(Flag521prime-)
)

)
(:action Flag521Action-15
:parameters ()
:precondition
(and
(Flag354-)
(Flag354prime-)
)
:effect
(and
(Flag521-)
(Flag521prime-)
)

)
(:action Flag521Action-16
:parameters ()
:precondition
(and
(Flag362-)
(Flag362prime-)
)
:effect
(and
(Flag521-)
(Flag521prime-)
)

)
(:action Flag521Action-17
:parameters ()
:precondition
(and
(Flag506-)
(Flag506prime-)
)
:effect
(and
(Flag521-)
(Flag521prime-)
)

)
(:action Flag521Action-18
:parameters ()
:precondition
(and
(Flag518-)
(Flag518prime-)
)
:effect
(and
(Flag521-)
(Flag521prime-)
)

)
(:action Prim521Action
:parameters ()
:precondition
(and
(not (Flag485-))
(Flag485prime-)
(not (Flag455-))
(Flag455prime-)
(not (Flag499-))
(Flag499prime-)
(not (Flag450-))
(Flag450prime-)
(not (Flag416-))
(Flag416prime-)
(not (Flag513-))
(Flag513prime-)
(not (Flag476-))
(Flag476prime-)
(not (Flag386-))
(Flag386prime-)
(not (Flag302-))
(Flag302prime-)
(not (Flag467-))
(Flag467prime-)
(not (Flag300-))
(Flag300prime-)
(not (Flag414-))
(Flag414prime-)
(not (Flag520-))
(Flag520prime-)
(not (Flag511-))
(Flag511prime-)
(not (Flag431-))
(Flag431prime-)
(not (Flag354-))
(Flag354prime-)
(not (Flag362-))
(Flag362prime-)
(not (Flag506-))
(Flag506prime-)
(not (Flag518-))
(Flag518prime-)
)
:effect
(and
(Flag521prime-)
(not (Flag521-))
)

)
(:action Flag522Action-0
:parameters ()
:precondition
(and
(Flag329-)
(Flag329prime-)
)
:effect
(and
(Flag522-)
(Flag522prime-)
)

)
(:action Flag522Action-1
:parameters ()
:precondition
(and
(Flag300-)
(Flag300prime-)
)
:effect
(and
(Flag522-)
(Flag522prime-)
)

)
(:action Flag522Action-2
:parameters ()
:precondition
(and
(Flag317-)
(Flag317prime-)
)
:effect
(and
(Flag522-)
(Flag522prime-)
)

)
(:action Flag522Action-3
:parameters ()
:precondition
(and
(Flag301-)
(Flag301prime-)
)
:effect
(and
(Flag522-)
(Flag522prime-)
)

)
(:action Flag522Action-4
:parameters ()
:precondition
(and
(Flag331-)
(Flag331prime-)
)
:effect
(and
(Flag522-)
(Flag522prime-)
)

)
(:action Flag522Action-5
:parameters ()
:precondition
(and
(Flag319-)
(Flag319prime-)
)
:effect
(and
(Flag522-)
(Flag522prime-)
)

)
(:action Flag522Action-6
:parameters ()
:precondition
(and
(Flag309-)
(Flag309prime-)
)
:effect
(and
(Flag522-)
(Flag522prime-)
)

)
(:action Flag522Action-7
:parameters ()
:precondition
(and
(Flag303-)
(Flag303prime-)
)
:effect
(and
(Flag522-)
(Flag522prime-)
)

)
(:action Flag522Action-8
:parameters ()
:precondition
(and
(Flag320-)
(Flag320prime-)
)
:effect
(and
(Flag522-)
(Flag522prime-)
)

)
(:action Flag522Action-9
:parameters ()
:precondition
(and
(Flag311-)
(Flag311prime-)
)
:effect
(and
(Flag522-)
(Flag522prime-)
)

)
(:action Flag522Action-10
:parameters ()
:precondition
(and
(Flag321-)
(Flag321prime-)
)
:effect
(and
(Flag522-)
(Flag522prime-)
)

)
(:action Flag522Action-11
:parameters ()
:precondition
(and
(Flag308-)
(Flag308prime-)
)
:effect
(and
(Flag522-)
(Flag522prime-)
)

)
(:action Flag522Action-12
:parameters ()
:precondition
(and
(Flag323-)
(Flag323prime-)
)
:effect
(and
(Flag522-)
(Flag522prime-)
)

)
(:action Flag522Action-14
:parameters ()
:precondition
(and
(Flag333-)
(Flag333prime-)
)
:effect
(and
(Flag522-)
(Flag522prime-)
)

)
(:action Flag522Action-15
:parameters ()
:precondition
(and
(Flag334-)
(Flag334prime-)
)
:effect
(and
(Flag522-)
(Flag522prime-)
)

)
(:action Flag522Action-16
:parameters ()
:precondition
(and
(Flag325-)
(Flag325prime-)
)
:effect
(and
(Flag522-)
(Flag522prime-)
)

)
(:action Flag522Action-17
:parameters ()
:precondition
(and
(Flag326-)
(Flag326prime-)
)
:effect
(and
(Flag522-)
(Flag522prime-)
)

)
(:action Flag522Action-18
:parameters ()
:precondition
(and
(Flag299-)
(Flag299prime-)
)
:effect
(and
(Flag522-)
(Flag522prime-)
)

)
(:action Prim522Action
:parameters ()
:precondition
(and
(not (Flag329-))
(Flag329prime-)
(not (Flag300-))
(Flag300prime-)
(not (Flag317-))
(Flag317prime-)
(not (Flag301-))
(Flag301prime-)
(not (Flag331-))
(Flag331prime-)
(not (Flag319-))
(Flag319prime-)
(not (Flag309-))
(Flag309prime-)
(not (Flag303-))
(Flag303prime-)
(not (Flag320-))
(Flag320prime-)
(not (Flag311-))
(Flag311prime-)
(not (Flag321-))
(Flag321prime-)
(not (Flag308-))
(Flag308prime-)
(not (Flag323-))
(Flag323prime-)
(not (BELOWOF1-ROBOT))
(not (Flag333-))
(Flag333prime-)
(not (Flag334-))
(Flag334prime-)
(not (Flag325-))
(Flag325prime-)
(not (Flag326-))
(Flag326prime-)
(not (Flag299-))
(Flag299prime-)
)
:effect
(and
(Flag522prime-)
(not (Flag522-))
)

)
(:action MOVERIGHT-ROBOT
:parameters ()
:precondition
(and
(Flag4-)
(Flag4prime-)
(Flag258prime-)
(Flag255prime-)
(Flag251prime-)
(Flag245prime-)
(Flag238prime-)
(Flag231prime-)
(Flag222prime-)
(Flag213prime-)
(Flag202prime-)
(Flag191prime-)
(Flag178prime-)
(Flag164prime-)
(Flag148prime-)
(Flag133prime-)
(Flag114prime-)
(Flag95prime-)
(Flag77prime-)
(Flag263prime-)
(Flag261prime-)
)
:effect
(and
(when
(and
(Flag258-)
)
(and
(COLUMN18-ROBOT)
(not (NOT-COLUMN18-ROBOT))
)
)
(when
(and
(Flag255-)
)
(and
(COLUMN17-ROBOT)
(not (NOT-COLUMN17-ROBOT))
)
)
(when
(and
(Flag251-)
)
(and
(COLUMN16-ROBOT)
(not (NOT-COLUMN16-ROBOT))
)
)
(when
(and
(Flag245-)
)
(and
(COLUMN15-ROBOT)
(not (NOT-COLUMN15-ROBOT))
)
)
(when
(and
(Flag238-)
)
(and
(COLUMN14-ROBOT)
(not (NOT-COLUMN14-ROBOT))
)
)
(when
(and
(Flag231-)
)
(and
(COLUMN13-ROBOT)
(not (NOT-COLUMN13-ROBOT))
)
)
(when
(and
(Flag222-)
)
(and
(COLUMN12-ROBOT)
(not (NOT-COLUMN12-ROBOT))
)
)
(when
(and
(Flag213-)
)
(and
(COLUMN11-ROBOT)
(not (NOT-COLUMN11-ROBOT))
)
)
(when
(and
(Flag202-)
)
(and
(COLUMN10-ROBOT)
(not (NOT-COLUMN10-ROBOT))
)
)
(when
(and
(Flag191-)
)
(and
(COLUMN9-ROBOT)
(not (NOT-COLUMN9-ROBOT))
)
)
(when
(and
(Flag178-)
)
(and
(COLUMN8-ROBOT)
(not (NOT-COLUMN8-ROBOT))
)
)
(when
(and
(Flag164-)
)
(and
(COLUMN7-ROBOT)
(not (NOT-COLUMN7-ROBOT))
)
)
(when
(and
(Flag148-)
)
(and
(COLUMN6-ROBOT)
(not (NOT-COLUMN6-ROBOT))
)
)
(when
(and
(Flag133-)
)
(and
(COLUMN5-ROBOT)
(not (NOT-COLUMN5-ROBOT))
)
)
(when
(and
(Flag114-)
)
(and
(COLUMN4-ROBOT)
(not (NOT-COLUMN4-ROBOT))
)
)
(when
(and
(Flag95-)
)
(and
(COLUMN3-ROBOT)
(not (NOT-COLUMN3-ROBOT))
)
)
(when
(and
(Flag77-)
)
(and
(COLUMN2-ROBOT)
(not (NOT-COLUMN2-ROBOT))
)
)
(when
(and
(Flag263-)
)
(and
(COLUMN1-ROBOT)
(not (NOT-COLUMN1-ROBOT))
)
)
(when
(and
(COLUMN17-ROBOT)
)
(and
(COLUMN18-ROBOT)
(NOT-COLUMN17-ROBOT)
(not (NOT-COLUMN18-ROBOT))
(not (COLUMN17-ROBOT))
)
)
(when
(and
(COLUMN16-ROBOT)
)
(and
(COLUMN17-ROBOT)
(NOT-COLUMN16-ROBOT)
(not (NOT-COLUMN17-ROBOT))
(not (COLUMN16-ROBOT))
)
)
(when
(and
(COLUMN15-ROBOT)
)
(and
(COLUMN16-ROBOT)
(NOT-COLUMN15-ROBOT)
(not (NOT-COLUMN16-ROBOT))
(not (COLUMN15-ROBOT))
)
)
(when
(and
(COLUMN14-ROBOT)
)
(and
(COLUMN15-ROBOT)
(NOT-COLUMN14-ROBOT)
(not (NOT-COLUMN15-ROBOT))
(not (COLUMN14-ROBOT))
)
)
(when
(and
(COLUMN13-ROBOT)
)
(and
(COLUMN14-ROBOT)
(NOT-COLUMN13-ROBOT)
(not (NOT-COLUMN14-ROBOT))
(not (COLUMN13-ROBOT))
)
)
(when
(and
(COLUMN12-ROBOT)
)
(and
(COLUMN13-ROBOT)
(NOT-COLUMN12-ROBOT)
(not (NOT-COLUMN13-ROBOT))
(not (COLUMN12-ROBOT))
)
)
(when
(and
(COLUMN11-ROBOT)
)
(and
(COLUMN12-ROBOT)
(NOT-COLUMN11-ROBOT)
(not (NOT-COLUMN12-ROBOT))
(not (COLUMN11-ROBOT))
)
)
(when
(and
(COLUMN10-ROBOT)
)
(and
(COLUMN11-ROBOT)
(NOT-COLUMN10-ROBOT)
(not (NOT-COLUMN11-ROBOT))
(not (COLUMN10-ROBOT))
)
)
(when
(and
(COLUMN9-ROBOT)
)
(and
(COLUMN10-ROBOT)
(NOT-COLUMN9-ROBOT)
(not (NOT-COLUMN10-ROBOT))
(not (COLUMN9-ROBOT))
)
)
(when
(and
(COLUMN8-ROBOT)
)
(and
(COLUMN9-ROBOT)
(NOT-COLUMN8-ROBOT)
(not (NOT-COLUMN9-ROBOT))
(not (COLUMN8-ROBOT))
)
)
(when
(and
(COLUMN7-ROBOT)
)
(and
(COLUMN8-ROBOT)
(NOT-COLUMN7-ROBOT)
(not (NOT-COLUMN8-ROBOT))
(not (COLUMN7-ROBOT))
)
)
(when
(and
(COLUMN6-ROBOT)
)
(and
(COLUMN7-ROBOT)
(NOT-COLUMN6-ROBOT)
(not (NOT-COLUMN7-ROBOT))
(not (COLUMN6-ROBOT))
)
)
(when
(and
(COLUMN5-ROBOT)
)
(and
(COLUMN6-ROBOT)
(NOT-COLUMN5-ROBOT)
(not (NOT-COLUMN6-ROBOT))
(not (COLUMN5-ROBOT))
)
)
(when
(and
(COLUMN4-ROBOT)
)
(and
(COLUMN5-ROBOT)
(NOT-COLUMN4-ROBOT)
(not (NOT-COLUMN5-ROBOT))
(not (COLUMN4-ROBOT))
)
)
(when
(and
(COLUMN3-ROBOT)
)
(and
(COLUMN4-ROBOT)
(NOT-COLUMN3-ROBOT)
(not (NOT-COLUMN4-ROBOT))
(not (COLUMN3-ROBOT))
)
)
(when
(and
(COLUMN2-ROBOT)
)
(and
(COLUMN3-ROBOT)
(NOT-COLUMN2-ROBOT)
(not (NOT-COLUMN3-ROBOT))
(not (COLUMN2-ROBOT))
)
)
(when
(and
(COLUMN1-ROBOT)
)
(and
(COLUMN2-ROBOT)
(NOT-COLUMN1-ROBOT)
(not (NOT-COLUMN2-ROBOT))
(not (COLUMN1-ROBOT))
)
)
(when
(and
(COLUMN0-ROBOT)
)
(and
(COLUMN1-ROBOT)
(NOT-COLUMN0-ROBOT)
(not (NOT-COLUMN1-ROBOT))
(not (COLUMN0-ROBOT))
)
)
(when
(and
(LEFTOF8-ROBOT)
)
(and
(LEFTOF19-ROBOT)
(NOT-LEFTOF18-ROBOT)
(LEFTOF18-ROBOT)
(NOT-LEFTOF17-ROBOT)
(LEFTOF17-ROBOT)
(NOT-LEFTOF16-ROBOT)
(LEFTOF16-ROBOT)
(NOT-LEFTOF15-ROBOT)
(LEFTOF15-ROBOT)
(NOT-LEFTOF14-ROBOT)
(LEFTOF14-ROBOT)
(NOT-LEFTOF13-ROBOT)
(LEFTOF13-ROBOT)
(NOT-LEFTOF12-ROBOT)
(LEFTOF12-ROBOT)
(NOT-LEFTOF11-ROBOT)
(LEFTOF11-ROBOT)
(NOT-LEFTOF10-ROBOT)
(LEFTOF10-ROBOT)
(NOT-LEFTOF9-ROBOT)
(LEFTOF9-ROBOT)
(NOT-LEFTOF8-ROBOT)
(not (NOT-LEFTOF19-ROBOT))
(not (LEFTOF8-ROBOT))
)
)
(when
(and
(LEFTOF7-ROBOT)
)
(and
(LEFTOF19-ROBOT)
(NOT-LEFTOF18-ROBOT)
(LEFTOF18-ROBOT)
(NOT-LEFTOF17-ROBOT)
(LEFTOF17-ROBOT)
(NOT-LEFTOF16-ROBOT)
(LEFTOF16-ROBOT)
(NOT-LEFTOF15-ROBOT)
(LEFTOF15-ROBOT)
(NOT-LEFTOF14-ROBOT)
(LEFTOF14-ROBOT)
(NOT-LEFTOF13-ROBOT)
(LEFTOF13-ROBOT)
(NOT-LEFTOF12-ROBOT)
(LEFTOF12-ROBOT)
(NOT-LEFTOF11-ROBOT)
(LEFTOF11-ROBOT)
(NOT-LEFTOF10-ROBOT)
(LEFTOF10-ROBOT)
(NOT-LEFTOF9-ROBOT)
(LEFTOF9-ROBOT)
(NOT-LEFTOF8-ROBOT)
(LEFTOF8-ROBOT)
(NOT-LEFTOF7-ROBOT)
(not (NOT-LEFTOF19-ROBOT))
(not (LEFTOF7-ROBOT))
)
)
(when
(and
(LEFTOF4-ROBOT)
)
(and
(LEFTOF19-ROBOT)
(NOT-LEFTOF18-ROBOT)
(LEFTOF18-ROBOT)
(NOT-LEFTOF17-ROBOT)
(LEFTOF17-ROBOT)
(NOT-LEFTOF16-ROBOT)
(LEFTOF16-ROBOT)
(NOT-LEFTOF15-ROBOT)
(LEFTOF15-ROBOT)
(NOT-LEFTOF14-ROBOT)
(LEFTOF14-ROBOT)
(NOT-LEFTOF13-ROBOT)
(LEFTOF13-ROBOT)
(NOT-LEFTOF12-ROBOT)
(LEFTOF12-ROBOT)
(NOT-LEFTOF11-ROBOT)
(LEFTOF11-ROBOT)
(NOT-LEFTOF10-ROBOT)
(LEFTOF10-ROBOT)
(NOT-LEFTOF9-ROBOT)
(LEFTOF9-ROBOT)
(NOT-LEFTOF8-ROBOT)
(LEFTOF8-ROBOT)
(NOT-LEFTOF7-ROBOT)
(LEFTOF7-ROBOT)
(NOT-LEFTOF6-ROBOT)
(LEFTOF6-ROBOT)
(NOT-LEFTOF5-ROBOT)
(LEFTOF5-ROBOT)
(NOT-LEFTOF4-ROBOT)
(not (NOT-LEFTOF19-ROBOT))
(not (LEFTOF4-ROBOT))
)
)
(when
(and
(LEFTOF3-ROBOT)
)
(and
(LEFTOF19-ROBOT)
(NOT-LEFTOF18-ROBOT)
(LEFTOF18-ROBOT)
(NOT-LEFTOF17-ROBOT)
(LEFTOF17-ROBOT)
(NOT-LEFTOF16-ROBOT)
(LEFTOF16-ROBOT)
(NOT-LEFTOF15-ROBOT)
(LEFTOF15-ROBOT)
(NOT-LEFTOF14-ROBOT)
(LEFTOF14-ROBOT)
(NOT-LEFTOF13-ROBOT)
(LEFTOF13-ROBOT)
(NOT-LEFTOF12-ROBOT)
(LEFTOF12-ROBOT)
(NOT-LEFTOF11-ROBOT)
(LEFTOF11-ROBOT)
(NOT-LEFTOF10-ROBOT)
(LEFTOF10-ROBOT)
(NOT-LEFTOF9-ROBOT)
(LEFTOF9-ROBOT)
(NOT-LEFTOF8-ROBOT)
(LEFTOF8-ROBOT)
(NOT-LEFTOF7-ROBOT)
(LEFTOF7-ROBOT)
(NOT-LEFTOF6-ROBOT)
(LEFTOF6-ROBOT)
(NOT-LEFTOF5-ROBOT)
(LEFTOF5-ROBOT)
(NOT-LEFTOF4-ROBOT)
(LEFTOF4-ROBOT)
(NOT-LEFTOF3-ROBOT)
(not (NOT-LEFTOF19-ROBOT))
(not (LEFTOF3-ROBOT))
)
)
(when
(and
(LEFTOF14-ROBOT)
)
(and
(LEFTOF19-ROBOT)
(NOT-LEFTOF18-ROBOT)
(LEFTOF18-ROBOT)
(NOT-LEFTOF17-ROBOT)
(LEFTOF17-ROBOT)
(NOT-LEFTOF16-ROBOT)
(LEFTOF16-ROBOT)
(NOT-LEFTOF15-ROBOT)
(LEFTOF15-ROBOT)
(NOT-LEFTOF14-ROBOT)
(not (NOT-LEFTOF19-ROBOT))
(not (LEFTOF14-ROBOT))
)
)
(when
(and
(LEFTOF16-ROBOT)
)
(and
(LEFTOF19-ROBOT)
(NOT-LEFTOF18-ROBOT)
(LEFTOF18-ROBOT)
(NOT-LEFTOF17-ROBOT)
(LEFTOF17-ROBOT)
(NOT-LEFTOF16-ROBOT)
(not (NOT-LEFTOF19-ROBOT))
(not (LEFTOF16-ROBOT))
)
)
(when
(and
(LEFTOF5-ROBOT)
)
(and
(LEFTOF19-ROBOT)
(NOT-LEFTOF18-ROBOT)
(LEFTOF18-ROBOT)
(NOT-LEFTOF17-ROBOT)
(LEFTOF17-ROBOT)
(NOT-LEFTOF16-ROBOT)
(LEFTOF16-ROBOT)
(NOT-LEFTOF15-ROBOT)
(LEFTOF15-ROBOT)
(NOT-LEFTOF14-ROBOT)
(LEFTOF14-ROBOT)
(NOT-LEFTOF13-ROBOT)
(LEFTOF13-ROBOT)
(NOT-LEFTOF12-ROBOT)
(LEFTOF12-ROBOT)
(NOT-LEFTOF11-ROBOT)
(LEFTOF11-ROBOT)
(NOT-LEFTOF10-ROBOT)
(LEFTOF10-ROBOT)
(NOT-LEFTOF9-ROBOT)
(LEFTOF9-ROBOT)
(NOT-LEFTOF8-ROBOT)
(LEFTOF8-ROBOT)
(NOT-LEFTOF7-ROBOT)
(LEFTOF7-ROBOT)
(NOT-LEFTOF6-ROBOT)
(LEFTOF6-ROBOT)
(NOT-LEFTOF5-ROBOT)
(not (NOT-LEFTOF19-ROBOT))
(not (LEFTOF5-ROBOT))
)
)
(when
(and
(LEFTOF6-ROBOT)
)
(and
(LEFTOF19-ROBOT)
(NOT-LEFTOF18-ROBOT)
(LEFTOF18-ROBOT)
(NOT-LEFTOF17-ROBOT)
(LEFTOF17-ROBOT)
(NOT-LEFTOF16-ROBOT)
(LEFTOF16-ROBOT)
(NOT-LEFTOF15-ROBOT)
(LEFTOF15-ROBOT)
(NOT-LEFTOF14-ROBOT)
(LEFTOF14-ROBOT)
(NOT-LEFTOF13-ROBOT)
(LEFTOF13-ROBOT)
(NOT-LEFTOF12-ROBOT)
(LEFTOF12-ROBOT)
(NOT-LEFTOF11-ROBOT)
(LEFTOF11-ROBOT)
(NOT-LEFTOF10-ROBOT)
(LEFTOF10-ROBOT)
(NOT-LEFTOF9-ROBOT)
(LEFTOF9-ROBOT)
(NOT-LEFTOF8-ROBOT)
(LEFTOF8-ROBOT)
(NOT-LEFTOF7-ROBOT)
(LEFTOF7-ROBOT)
(NOT-LEFTOF6-ROBOT)
(not (NOT-LEFTOF19-ROBOT))
(not (LEFTOF6-ROBOT))
)
)
(when
(and
(LEFTOF12-ROBOT)
)
(and
(LEFTOF19-ROBOT)
(NOT-LEFTOF18-ROBOT)
(LEFTOF18-ROBOT)
(NOT-LEFTOF17-ROBOT)
(LEFTOF17-ROBOT)
(NOT-LEFTOF16-ROBOT)
(LEFTOF16-ROBOT)
(NOT-LEFTOF15-ROBOT)
(LEFTOF15-ROBOT)
(NOT-LEFTOF14-ROBOT)
(LEFTOF14-ROBOT)
(NOT-LEFTOF13-ROBOT)
(LEFTOF13-ROBOT)
(NOT-LEFTOF12-ROBOT)
(not (NOT-LEFTOF19-ROBOT))
(not (LEFTOF12-ROBOT))
)
)
(when
(and
(LEFTOF1-ROBOT)
)
(and
(LEFTOF19-ROBOT)
(NOT-LEFTOF18-ROBOT)
(LEFTOF18-ROBOT)
(NOT-LEFTOF17-ROBOT)
(LEFTOF17-ROBOT)
(NOT-LEFTOF16-ROBOT)
(LEFTOF16-ROBOT)
(NOT-LEFTOF15-ROBOT)
(LEFTOF15-ROBOT)
(NOT-LEFTOF14-ROBOT)
(LEFTOF14-ROBOT)
(NOT-LEFTOF13-ROBOT)
(LEFTOF13-ROBOT)
(NOT-LEFTOF12-ROBOT)
(LEFTOF12-ROBOT)
(NOT-LEFTOF11-ROBOT)
(LEFTOF11-ROBOT)
(NOT-LEFTOF10-ROBOT)
(LEFTOF10-ROBOT)
(NOT-LEFTOF9-ROBOT)
(LEFTOF9-ROBOT)
(NOT-LEFTOF8-ROBOT)
(LEFTOF8-ROBOT)
(NOT-LEFTOF7-ROBOT)
(LEFTOF7-ROBOT)
(NOT-LEFTOF6-ROBOT)
(LEFTOF6-ROBOT)
(NOT-LEFTOF5-ROBOT)
(LEFTOF5-ROBOT)
(NOT-LEFTOF4-ROBOT)
(LEFTOF4-ROBOT)
(NOT-LEFTOF3-ROBOT)
(LEFTOF3-ROBOT)
(NOT-LEFTOF2-ROBOT)
(LEFTOF2-ROBOT)
(NOT-LEFTOF1-ROBOT)
(not (NOT-LEFTOF19-ROBOT))
(not (LEFTOF1-ROBOT))
)
)
(when
(and
(LEFTOF13-ROBOT)
)
(and
(LEFTOF19-ROBOT)
(NOT-LEFTOF18-ROBOT)
(LEFTOF18-ROBOT)
(NOT-LEFTOF17-ROBOT)
(LEFTOF17-ROBOT)
(NOT-LEFTOF16-ROBOT)
(LEFTOF16-ROBOT)
(NOT-LEFTOF15-ROBOT)
(LEFTOF15-ROBOT)
(NOT-LEFTOF14-ROBOT)
(LEFTOF14-ROBOT)
(NOT-LEFTOF13-ROBOT)
(not (NOT-LEFTOF19-ROBOT))
(not (LEFTOF13-ROBOT))
)
)
(when
(and
(LEFTOF2-ROBOT)
)
(and
(LEFTOF19-ROBOT)
(NOT-LEFTOF18-ROBOT)
(LEFTOF18-ROBOT)
(NOT-LEFTOF17-ROBOT)
(LEFTOF17-ROBOT)
(NOT-LEFTOF16-ROBOT)
(LEFTOF16-ROBOT)
(NOT-LEFTOF15-ROBOT)
(LEFTOF15-ROBOT)
(NOT-LEFTOF14-ROBOT)
(LEFTOF14-ROBOT)
(NOT-LEFTOF13-ROBOT)
(LEFTOF13-ROBOT)
(NOT-LEFTOF12-ROBOT)
(LEFTOF12-ROBOT)
(NOT-LEFTOF11-ROBOT)
(LEFTOF11-ROBOT)
(NOT-LEFTOF10-ROBOT)
(LEFTOF10-ROBOT)
(NOT-LEFTOF9-ROBOT)
(LEFTOF9-ROBOT)
(NOT-LEFTOF8-ROBOT)
(LEFTOF8-ROBOT)
(NOT-LEFTOF7-ROBOT)
(LEFTOF7-ROBOT)
(NOT-LEFTOF6-ROBOT)
(LEFTOF6-ROBOT)
(NOT-LEFTOF5-ROBOT)
(LEFTOF5-ROBOT)
(NOT-LEFTOF4-ROBOT)
(LEFTOF4-ROBOT)
(NOT-LEFTOF3-ROBOT)
(LEFTOF3-ROBOT)
(NOT-LEFTOF2-ROBOT)
(not (NOT-LEFTOF19-ROBOT))
(not (LEFTOF2-ROBOT))
)
)
(when
(and
(LEFTOF9-ROBOT)
)
(and
(LEFTOF19-ROBOT)
(NOT-LEFTOF18-ROBOT)
(LEFTOF18-ROBOT)
(NOT-LEFTOF17-ROBOT)
(LEFTOF17-ROBOT)
(NOT-LEFTOF16-ROBOT)
(LEFTOF16-ROBOT)
(NOT-LEFTOF15-ROBOT)
(LEFTOF15-ROBOT)
(NOT-LEFTOF14-ROBOT)
(LEFTOF14-ROBOT)
(NOT-LEFTOF13-ROBOT)
(LEFTOF13-ROBOT)
(NOT-LEFTOF12-ROBOT)
(LEFTOF12-ROBOT)
(NOT-LEFTOF11-ROBOT)
(LEFTOF11-ROBOT)
(NOT-LEFTOF10-ROBOT)
(LEFTOF10-ROBOT)
(NOT-LEFTOF9-ROBOT)
(not (NOT-LEFTOF19-ROBOT))
(not (LEFTOF9-ROBOT))
)
)
(when
(and
(LEFTOF11-ROBOT)
)
(and
(LEFTOF19-ROBOT)
(NOT-LEFTOF18-ROBOT)
(LEFTOF18-ROBOT)
(NOT-LEFTOF17-ROBOT)
(LEFTOF17-ROBOT)
(NOT-LEFTOF16-ROBOT)
(LEFTOF16-ROBOT)
(NOT-LEFTOF15-ROBOT)
(LEFTOF15-ROBOT)
(NOT-LEFTOF14-ROBOT)
(LEFTOF14-ROBOT)
(NOT-LEFTOF13-ROBOT)
(LEFTOF13-ROBOT)
(NOT-LEFTOF12-ROBOT)
(LEFTOF12-ROBOT)
(NOT-LEFTOF11-ROBOT)
(not (NOT-LEFTOF19-ROBOT))
(not (LEFTOF11-ROBOT))
)
)
(when
(and
(LEFTOF17-ROBOT)
)
(and
(LEFTOF19-ROBOT)
(NOT-LEFTOF18-ROBOT)
(LEFTOF18-ROBOT)
(NOT-LEFTOF17-ROBOT)
(not (NOT-LEFTOF19-ROBOT))
(not (LEFTOF17-ROBOT))
)
)
(when
(and
(LEFTOF18-ROBOT)
)
(and
(LEFTOF19-ROBOT)
(NOT-LEFTOF18-ROBOT)
(not (NOT-LEFTOF19-ROBOT))
(not (LEFTOF18-ROBOT))
)
)
(when
(and
(LEFTOF10-ROBOT)
)
(and
(LEFTOF19-ROBOT)
(NOT-LEFTOF18-ROBOT)
(LEFTOF18-ROBOT)
(NOT-LEFTOF17-ROBOT)
(LEFTOF17-ROBOT)
(NOT-LEFTOF16-ROBOT)
(LEFTOF16-ROBOT)
(NOT-LEFTOF15-ROBOT)
(LEFTOF15-ROBOT)
(NOT-LEFTOF14-ROBOT)
(LEFTOF14-ROBOT)
(NOT-LEFTOF13-ROBOT)
(LEFTOF13-ROBOT)
(NOT-LEFTOF12-ROBOT)
(LEFTOF12-ROBOT)
(NOT-LEFTOF11-ROBOT)
(LEFTOF11-ROBOT)
(NOT-LEFTOF10-ROBOT)
(not (NOT-LEFTOF19-ROBOT))
(not (LEFTOF10-ROBOT))
)
)
(when
(and
(LEFTOF15-ROBOT)
)
(and
(LEFTOF19-ROBOT)
(NOT-LEFTOF18-ROBOT)
(LEFTOF18-ROBOT)
(NOT-LEFTOF17-ROBOT)
(LEFTOF17-ROBOT)
(NOT-LEFTOF16-ROBOT)
(LEFTOF16-ROBOT)
(NOT-LEFTOF15-ROBOT)
(not (NOT-LEFTOF19-ROBOT))
(not (LEFTOF15-ROBOT))
)
)
(when
(and
(RIGHTOF18-ROBOT)
)
(and
(RIGHTOF17-ROBOT)
(RIGHTOF16-ROBOT)
(RIGHTOF15-ROBOT)
(RIGHTOF14-ROBOT)
(RIGHTOF13-ROBOT)
(RIGHTOF12-ROBOT)
(RIGHTOF11-ROBOT)
(RIGHTOF10-ROBOT)
(RIGHTOF9-ROBOT)
(RIGHTOF8-ROBOT)
(RIGHTOF7-ROBOT)
(RIGHTOF6-ROBOT)
(RIGHTOF5-ROBOT)
(RIGHTOF4-ROBOT)
(RIGHTOF3-ROBOT)
(RIGHTOF2-ROBOT)
(not (NOT-RIGHTOF18-ROBOT))
(not (NOT-RIGHTOF17-ROBOT))
(not (NOT-RIGHTOF16-ROBOT))
(not (NOT-RIGHTOF15-ROBOT))
(not (NOT-RIGHTOF14-ROBOT))
(not (NOT-RIGHTOF13-ROBOT))
(not (NOT-RIGHTOF12-ROBOT))
(not (NOT-RIGHTOF11-ROBOT))
(not (NOT-RIGHTOF10-ROBOT))
(not (NOT-RIGHTOF9-ROBOT))
(not (NOT-RIGHTOF8-ROBOT))
(not (NOT-RIGHTOF7-ROBOT))
(not (NOT-RIGHTOF6-ROBOT))
(not (NOT-RIGHTOF5-ROBOT))
(not (NOT-RIGHTOF4-ROBOT))
(not (NOT-RIGHTOF3-ROBOT))
(not (NOT-RIGHTOF2-ROBOT))
)
)
(when
(and
(RIGHTOF17-ROBOT)
)
(and
(RIGHTOF18-ROBOT)
(RIGHTOF16-ROBOT)
(RIGHTOF15-ROBOT)
(RIGHTOF14-ROBOT)
(RIGHTOF13-ROBOT)
(RIGHTOF12-ROBOT)
(RIGHTOF11-ROBOT)
(RIGHTOF10-ROBOT)
(RIGHTOF9-ROBOT)
(RIGHTOF8-ROBOT)
(RIGHTOF7-ROBOT)
(RIGHTOF6-ROBOT)
(RIGHTOF5-ROBOT)
(RIGHTOF4-ROBOT)
(RIGHTOF3-ROBOT)
(RIGHTOF2-ROBOT)
(not (NOT-RIGHTOF18-ROBOT))
(not (NOT-RIGHTOF17-ROBOT))
(not (NOT-RIGHTOF16-ROBOT))
(not (NOT-RIGHTOF15-ROBOT))
(not (NOT-RIGHTOF14-ROBOT))
(not (NOT-RIGHTOF13-ROBOT))
(not (NOT-RIGHTOF12-ROBOT))
(not (NOT-RIGHTOF11-ROBOT))
(not (NOT-RIGHTOF10-ROBOT))
(not (NOT-RIGHTOF9-ROBOT))
(not (NOT-RIGHTOF8-ROBOT))
(not (NOT-RIGHTOF7-ROBOT))
(not (NOT-RIGHTOF6-ROBOT))
(not (NOT-RIGHTOF5-ROBOT))
(not (NOT-RIGHTOF4-ROBOT))
(not (NOT-RIGHTOF3-ROBOT))
(not (NOT-RIGHTOF2-ROBOT))
)
)
(when
(and
(RIGHTOF16-ROBOT)
)
(and
(RIGHTOF17-ROBOT)
(RIGHTOF15-ROBOT)
(RIGHTOF14-ROBOT)
(RIGHTOF13-ROBOT)
(RIGHTOF12-ROBOT)
(RIGHTOF11-ROBOT)
(RIGHTOF10-ROBOT)
(RIGHTOF9-ROBOT)
(RIGHTOF8-ROBOT)
(RIGHTOF7-ROBOT)
(RIGHTOF6-ROBOT)
(RIGHTOF5-ROBOT)
(RIGHTOF4-ROBOT)
(RIGHTOF3-ROBOT)
(RIGHTOF2-ROBOT)
(not (NOT-RIGHTOF17-ROBOT))
(not (NOT-RIGHTOF16-ROBOT))
(not (NOT-RIGHTOF15-ROBOT))
(not (NOT-RIGHTOF14-ROBOT))
(not (NOT-RIGHTOF13-ROBOT))
(not (NOT-RIGHTOF12-ROBOT))
(not (NOT-RIGHTOF11-ROBOT))
(not (NOT-RIGHTOF10-ROBOT))
(not (NOT-RIGHTOF9-ROBOT))
(not (NOT-RIGHTOF8-ROBOT))
(not (NOT-RIGHTOF7-ROBOT))
(not (NOT-RIGHTOF6-ROBOT))
(not (NOT-RIGHTOF5-ROBOT))
(not (NOT-RIGHTOF4-ROBOT))
(not (NOT-RIGHTOF3-ROBOT))
(not (NOT-RIGHTOF2-ROBOT))
)
)
(when
(and
(RIGHTOF15-ROBOT)
)
(and
(RIGHTOF16-ROBOT)
(RIGHTOF14-ROBOT)
(RIGHTOF13-ROBOT)
(RIGHTOF12-ROBOT)
(RIGHTOF11-ROBOT)
(RIGHTOF10-ROBOT)
(RIGHTOF9-ROBOT)
(RIGHTOF8-ROBOT)
(RIGHTOF7-ROBOT)
(RIGHTOF6-ROBOT)
(RIGHTOF5-ROBOT)
(RIGHTOF4-ROBOT)
(RIGHTOF3-ROBOT)
(RIGHTOF2-ROBOT)
(not (NOT-RIGHTOF16-ROBOT))
(not (NOT-RIGHTOF15-ROBOT))
(not (NOT-RIGHTOF14-ROBOT))
(not (NOT-RIGHTOF13-ROBOT))
(not (NOT-RIGHTOF12-ROBOT))
(not (NOT-RIGHTOF11-ROBOT))
(not (NOT-RIGHTOF10-ROBOT))
(not (NOT-RIGHTOF9-ROBOT))
(not (NOT-RIGHTOF8-ROBOT))
(not (NOT-RIGHTOF7-ROBOT))
(not (NOT-RIGHTOF6-ROBOT))
(not (NOT-RIGHTOF5-ROBOT))
(not (NOT-RIGHTOF4-ROBOT))
(not (NOT-RIGHTOF3-ROBOT))
(not (NOT-RIGHTOF2-ROBOT))
)
)
(when
(and
(RIGHTOF14-ROBOT)
)
(and
(RIGHTOF15-ROBOT)
(RIGHTOF13-ROBOT)
(RIGHTOF12-ROBOT)
(RIGHTOF11-ROBOT)
(RIGHTOF10-ROBOT)
(RIGHTOF9-ROBOT)
(RIGHTOF8-ROBOT)
(RIGHTOF7-ROBOT)
(RIGHTOF6-ROBOT)
(RIGHTOF5-ROBOT)
(RIGHTOF4-ROBOT)
(RIGHTOF3-ROBOT)
(RIGHTOF2-ROBOT)
(not (NOT-RIGHTOF15-ROBOT))
(not (NOT-RIGHTOF14-ROBOT))
(not (NOT-RIGHTOF13-ROBOT))
(not (NOT-RIGHTOF12-ROBOT))
(not (NOT-RIGHTOF11-ROBOT))
(not (NOT-RIGHTOF10-ROBOT))
(not (NOT-RIGHTOF9-ROBOT))
(not (NOT-RIGHTOF8-ROBOT))
(not (NOT-RIGHTOF7-ROBOT))
(not (NOT-RIGHTOF6-ROBOT))
(not (NOT-RIGHTOF5-ROBOT))
(not (NOT-RIGHTOF4-ROBOT))
(not (NOT-RIGHTOF3-ROBOT))
(not (NOT-RIGHTOF2-ROBOT))
)
)
(when
(and
(RIGHTOF13-ROBOT)
)
(and
(RIGHTOF14-ROBOT)
(RIGHTOF12-ROBOT)
(RIGHTOF11-ROBOT)
(RIGHTOF10-ROBOT)
(RIGHTOF9-ROBOT)
(RIGHTOF8-ROBOT)
(RIGHTOF7-ROBOT)
(RIGHTOF6-ROBOT)
(RIGHTOF5-ROBOT)
(RIGHTOF4-ROBOT)
(RIGHTOF3-ROBOT)
(RIGHTOF2-ROBOT)
(not (NOT-RIGHTOF14-ROBOT))
(not (NOT-RIGHTOF13-ROBOT))
(not (NOT-RIGHTOF12-ROBOT))
(not (NOT-RIGHTOF11-ROBOT))
(not (NOT-RIGHTOF10-ROBOT))
(not (NOT-RIGHTOF9-ROBOT))
(not (NOT-RIGHTOF8-ROBOT))
(not (NOT-RIGHTOF7-ROBOT))
(not (NOT-RIGHTOF6-ROBOT))
(not (NOT-RIGHTOF5-ROBOT))
(not (NOT-RIGHTOF4-ROBOT))
(not (NOT-RIGHTOF3-ROBOT))
(not (NOT-RIGHTOF2-ROBOT))
)
)
(when
(and
(RIGHTOF12-ROBOT)
)
(and
(RIGHTOF13-ROBOT)
(RIGHTOF11-ROBOT)
(RIGHTOF10-ROBOT)
(RIGHTOF9-ROBOT)
(RIGHTOF8-ROBOT)
(RIGHTOF7-ROBOT)
(RIGHTOF6-ROBOT)
(RIGHTOF5-ROBOT)
(RIGHTOF4-ROBOT)
(RIGHTOF3-ROBOT)
(RIGHTOF2-ROBOT)
(not (NOT-RIGHTOF13-ROBOT))
(not (NOT-RIGHTOF12-ROBOT))
(not (NOT-RIGHTOF11-ROBOT))
(not (NOT-RIGHTOF10-ROBOT))
(not (NOT-RIGHTOF9-ROBOT))
(not (NOT-RIGHTOF8-ROBOT))
(not (NOT-RIGHTOF7-ROBOT))
(not (NOT-RIGHTOF6-ROBOT))
(not (NOT-RIGHTOF5-ROBOT))
(not (NOT-RIGHTOF4-ROBOT))
(not (NOT-RIGHTOF3-ROBOT))
(not (NOT-RIGHTOF2-ROBOT))
)
)
(when
(and
(RIGHTOF11-ROBOT)
)
(and
(RIGHTOF12-ROBOT)
(RIGHTOF10-ROBOT)
(RIGHTOF9-ROBOT)
(RIGHTOF8-ROBOT)
(RIGHTOF7-ROBOT)
(RIGHTOF6-ROBOT)
(RIGHTOF5-ROBOT)
(RIGHTOF4-ROBOT)
(RIGHTOF3-ROBOT)
(RIGHTOF2-ROBOT)
(not (NOT-RIGHTOF12-ROBOT))
(not (NOT-RIGHTOF11-ROBOT))
(not (NOT-RIGHTOF10-ROBOT))
(not (NOT-RIGHTOF9-ROBOT))
(not (NOT-RIGHTOF8-ROBOT))
(not (NOT-RIGHTOF7-ROBOT))
(not (NOT-RIGHTOF6-ROBOT))
(not (NOT-RIGHTOF5-ROBOT))
(not (NOT-RIGHTOF4-ROBOT))
(not (NOT-RIGHTOF3-ROBOT))
(not (NOT-RIGHTOF2-ROBOT))
)
)
(when
(and
(RIGHTOF10-ROBOT)
)
(and
(RIGHTOF11-ROBOT)
(RIGHTOF9-ROBOT)
(RIGHTOF8-ROBOT)
(RIGHTOF7-ROBOT)
(RIGHTOF6-ROBOT)
(RIGHTOF5-ROBOT)
(RIGHTOF4-ROBOT)
(RIGHTOF3-ROBOT)
(RIGHTOF2-ROBOT)
(not (NOT-RIGHTOF11-ROBOT))
(not (NOT-RIGHTOF10-ROBOT))
(not (NOT-RIGHTOF9-ROBOT))
(not (NOT-RIGHTOF8-ROBOT))
(not (NOT-RIGHTOF7-ROBOT))
(not (NOT-RIGHTOF6-ROBOT))
(not (NOT-RIGHTOF5-ROBOT))
(not (NOT-RIGHTOF4-ROBOT))
(not (NOT-RIGHTOF3-ROBOT))
(not (NOT-RIGHTOF2-ROBOT))
)
)
(when
(and
(RIGHTOF9-ROBOT)
)
(and
(RIGHTOF10-ROBOT)
(RIGHTOF8-ROBOT)
(RIGHTOF7-ROBOT)
(RIGHTOF6-ROBOT)
(RIGHTOF5-ROBOT)
(RIGHTOF4-ROBOT)
(RIGHTOF3-ROBOT)
(RIGHTOF2-ROBOT)
(not (NOT-RIGHTOF10-ROBOT))
(not (NOT-RIGHTOF9-ROBOT))
(not (NOT-RIGHTOF8-ROBOT))
(not (NOT-RIGHTOF7-ROBOT))
(not (NOT-RIGHTOF6-ROBOT))
(not (NOT-RIGHTOF5-ROBOT))
(not (NOT-RIGHTOF4-ROBOT))
(not (NOT-RIGHTOF3-ROBOT))
(not (NOT-RIGHTOF2-ROBOT))
)
)
(when
(and
(RIGHTOF8-ROBOT)
)
(and
(RIGHTOF9-ROBOT)
(RIGHTOF7-ROBOT)
(RIGHTOF6-ROBOT)
(RIGHTOF5-ROBOT)
(RIGHTOF4-ROBOT)
(RIGHTOF3-ROBOT)
(RIGHTOF2-ROBOT)
(not (NOT-RIGHTOF9-ROBOT))
(not (NOT-RIGHTOF8-ROBOT))
(not (NOT-RIGHTOF7-ROBOT))
(not (NOT-RIGHTOF6-ROBOT))
(not (NOT-RIGHTOF5-ROBOT))
(not (NOT-RIGHTOF4-ROBOT))
(not (NOT-RIGHTOF3-ROBOT))
(not (NOT-RIGHTOF2-ROBOT))
)
)
(when
(and
(RIGHTOF7-ROBOT)
)
(and
(RIGHTOF8-ROBOT)
(RIGHTOF6-ROBOT)
(RIGHTOF5-ROBOT)
(RIGHTOF4-ROBOT)
(RIGHTOF3-ROBOT)
(RIGHTOF2-ROBOT)
(not (NOT-RIGHTOF8-ROBOT))
(not (NOT-RIGHTOF7-ROBOT))
(not (NOT-RIGHTOF6-ROBOT))
(not (NOT-RIGHTOF5-ROBOT))
(not (NOT-RIGHTOF4-ROBOT))
(not (NOT-RIGHTOF3-ROBOT))
(not (NOT-RIGHTOF2-ROBOT))
)
)
(when
(and
(RIGHTOF6-ROBOT)
)
(and
(RIGHTOF7-ROBOT)
(RIGHTOF5-ROBOT)
(RIGHTOF4-ROBOT)
(RIGHTOF3-ROBOT)
(RIGHTOF2-ROBOT)
(not (NOT-RIGHTOF7-ROBOT))
(not (NOT-RIGHTOF6-ROBOT))
(not (NOT-RIGHTOF5-ROBOT))
(not (NOT-RIGHTOF4-ROBOT))
(not (NOT-RIGHTOF3-ROBOT))
(not (NOT-RIGHTOF2-ROBOT))
)
)
(when
(and
(RIGHTOF5-ROBOT)
)
(and
(RIGHTOF6-ROBOT)
(RIGHTOF4-ROBOT)
(RIGHTOF3-ROBOT)
(RIGHTOF2-ROBOT)
(not (NOT-RIGHTOF6-ROBOT))
(not (NOT-RIGHTOF5-ROBOT))
(not (NOT-RIGHTOF4-ROBOT))
(not (NOT-RIGHTOF3-ROBOT))
(not (NOT-RIGHTOF2-ROBOT))
)
)
(when
(and
(RIGHTOF4-ROBOT)
)
(and
(RIGHTOF5-ROBOT)
(RIGHTOF3-ROBOT)
(RIGHTOF2-ROBOT)
(not (NOT-RIGHTOF5-ROBOT))
(not (NOT-RIGHTOF4-ROBOT))
(not (NOT-RIGHTOF3-ROBOT))
(not (NOT-RIGHTOF2-ROBOT))
)
)
(when
(and
(RIGHTOF3-ROBOT)
)
(and
(RIGHTOF4-ROBOT)
(RIGHTOF2-ROBOT)
(not (NOT-RIGHTOF4-ROBOT))
(not (NOT-RIGHTOF3-ROBOT))
(not (NOT-RIGHTOF2-ROBOT))
)
)
(when
(and
(RIGHTOF2-ROBOT)
)
(and
(RIGHTOF3-ROBOT)
(not (NOT-RIGHTOF3-ROBOT))
(not (NOT-RIGHTOF2-ROBOT))
)
)
(when
(and
(RIGHTOF1-ROBOT)
)
(and
(RIGHTOF2-ROBOT)
(not (NOT-RIGHTOF2-ROBOT))
)
)
(when
(and
(Flag261-)
)
(and
(RIGHTOF1-ROBOT)
(not (NOT-RIGHTOF1-ROBOT))
)
)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag281prime-))
(not (Flag280prime-))
(not (Flag261prime-))
(not (Flag24prime-))
(not (Flag23prime-))
(not (Flag22prime-))
(not (Flag21prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag522prime-))
(not (Flag520prime-))
(not (Flag518prime-))
(not (Flag517prime-))
(not (Flag515prime-))
(not (Flag514prime-))
(not (Flag513prime-))
(not (Flag511prime-))
(not (Flag510prime-))
(not (Flag509prime-))
(not (Flag508prime-))
(not (Flag506prime-))
(not (Flag505prime-))
(not (Flag504prime-))
(not (Flag503prime-))
(not (Flag502prime-))
(not (Flag500prime-))
(not (Flag499prime-))
(not (Flag498prime-))
(not (Flag497prime-))
(not (Flag496prime-))
(not (Flag495prime-))
(not (Flag494prime-))
(not (Flag492prime-))
(not (Flag491prime-))
(not (Flag490prime-))
(not (Flag489prime-))
(not (Flag488prime-))
(not (Flag487prime-))
(not (Flag486prime-))
(not (Flag485prime-))
(not (Flag483prime-))
(not (Flag482prime-))
(not (Flag481prime-))
(not (Flag480prime-))
(not (Flag479prime-))
(not (Flag478prime-))
(not (Flag477prime-))
(not (Flag476prime-))
(not (Flag474prime-))
(not (Flag473prime-))
(not (Flag472prime-))
(not (Flag471prime-))
(not (Flag470prime-))
(not (Flag469prime-))
(not (Flag468prime-))
(not (Flag467prime-))
(not (Flag466prime-))
(not (Flag464prime-))
(not (Flag463prime-))
(not (Flag462prime-))
(not (Flag461prime-))
(not (Flag460prime-))
(not (Flag459prime-))
(not (Flag458prime-))
(not (Flag457prime-))
(not (Flag456prime-))
(not (Flag455prime-))
(not (Flag454prime-))
(not (Flag452prime-))
(not (Flag451prime-))
(not (Flag450prime-))
(not (Flag449prime-))
(not (Flag448prime-))
(not (Flag447prime-))
(not (Flag446prime-))
(not (Flag445prime-))
(not (Flag444prime-))
(not (Flag443prime-))
(not (Flag442prime-))
(not (Flag441prime-))
(not (Flag440prime-))
(not (Flag438prime-))
(not (Flag437prime-))
(not (Flag436prime-))
(not (Flag435prime-))
(not (Flag434prime-))
(not (Flag433prime-))
(not (Flag432prime-))
(not (Flag431prime-))
(not (Flag430prime-))
(not (Flag429prime-))
(not (Flag428prime-))
(not (Flag427prime-))
(not (Flag426prime-))
(not (Flag424prime-))
(not (Flag423prime-))
(not (Flag422prime-))
(not (Flag421prime-))
(not (Flag420prime-))
(not (Flag419prime-))
(not (Flag418prime-))
(not (Flag417prime-))
(not (Flag416prime-))
(not (Flag415prime-))
(not (Flag414prime-))
(not (Flag413prime-))
(not (Flag412prime-))
(not (Flag411prime-))
(not (Flag410prime-))
(not (Flag409prime-))
(not (Flag407prime-))
(not (Flag406prime-))
(not (Flag405prime-))
(not (Flag404prime-))
(not (Flag403prime-))
(not (Flag402prime-))
(not (Flag401prime-))
(not (Flag400prime-))
(not (Flag399prime-))
(not (Flag398prime-))
(not (Flag397prime-))
(not (Flag396prime-))
(not (Flag395prime-))
(not (Flag394prime-))
(not (Flag393prime-))
(not (Flag392prime-))
(not (Flag390prime-))
(not (Flag389prime-))
(not (Flag388prime-))
(not (Flag387prime-))
(not (Flag386prime-))
(not (Flag385prime-))
(not (Flag384prime-))
(not (Flag383prime-))
(not (Flag382prime-))
(not (Flag381prime-))
(not (Flag380prime-))
(not (Flag379prime-))
(not (Flag378prime-))
(not (Flag377prime-))
(not (Flag376prime-))
(not (Flag375prime-))
(not (Flag373prime-))
(not (Flag372prime-))
(not (Flag371prime-))
(not (Flag370prime-))
(not (Flag369prime-))
(not (Flag368prime-))
(not (Flag367prime-))
(not (Flag366prime-))
(not (Flag365prime-))
(not (Flag364prime-))
(not (Flag363prime-))
(not (Flag362prime-))
(not (Flag361prime-))
(not (Flag360prime-))
(not (Flag359prime-))
(not (Flag358prime-))
(not (Flag357prime-))
(not (Flag356prime-))
(not (Flag354prime-))
(not (Flag353prime-))
(not (Flag352prime-))
(not (Flag351prime-))
(not (Flag350prime-))
(not (Flag349prime-))
(not (Flag348prime-))
(not (Flag347prime-))
(not (Flag346prime-))
(not (Flag345prime-))
(not (Flag344prime-))
(not (Flag343prime-))
(not (Flag342prime-))
(not (Flag341prime-))
(not (Flag340prime-))
(not (Flag339prime-))
(not (Flag338prime-))
(not (Flag337prime-))
(not (Flag335prime-))
(not (Flag334prime-))
(not (Flag333prime-))
(not (Flag332prime-))
(not (Flag331prime-))
(not (Flag330prime-))
(not (Flag329prime-))
(not (Flag328prime-))
(not (Flag327prime-))
(not (Flag326prime-))
(not (Flag325prime-))
(not (Flag324prime-))
(not (Flag323prime-))
(not (Flag322prime-))
(not (Flag321prime-))
(not (Flag320prime-))
(not (Flag319prime-))
(not (Flag318prime-))
(not (Flag317prime-))
(not (Flag316prime-))
(not (Flag315prime-))
(not (Flag314prime-))
(not (Flag313prime-))
(not (Flag312prime-))
(not (Flag311prime-))
(not (Flag310prime-))
(not (Flag309prime-))
(not (Flag308prime-))
(not (Flag307prime-))
(not (Flag306prime-))
(not (Flag305prime-))
(not (Flag304prime-))
(not (Flag303prime-))
(not (Flag302prime-))
(not (Flag301prime-))
(not (Flag300prime-))
(not (Flag299prime-))
(not (Flag298prime-))
(not (Flag297prime-))
(not (Flag296prime-))
(not (Flag295prime-))
(not (Flag294prime-))
(not (Flag293prime-))
(not (Flag292prime-))
(not (Flag291prime-))
(not (Flag290prime-))
(not (Flag289prime-))
(not (Flag288prime-))
(not (Flag287prime-))
(not (Flag286prime-))
(not (Flag285prime-))
(not (Flag284prime-))
(not (Flag283prime-))
(not (Flag282prime-))
(not (Flag279prime-))
(not (Flag278prime-))
(not (Flag277prime-))
(not (Flag276prime-))
(not (Flag275prime-))
(not (Flag274prime-))
(not (Flag273prime-))
(not (Flag272prime-))
(not (Flag271prime-))
(not (Flag270prime-))
(not (Flag269prime-))
(not (Flag268prime-))
(not (Flag267prime-))
(not (Flag266prime-))
(not (Flag265prime-))
(not (Flag264prime-))
(not (Flag4prime-))
(not (Flag521prime-))
(not (Flag519prime-))
(not (Flag516prime-))
(not (Flag512prime-))
(not (Flag507prime-))
(not (Flag501prime-))
(not (Flag493prime-))
(not (Flag484prime-))
(not (Flag475prime-))
(not (Flag465prime-))
(not (Flag453prime-))
(not (Flag439prime-))
(not (Flag425prime-))
(not (Flag408prime-))
(not (Flag391prime-))
(not (Flag374prime-))
(not (Flag355prime-))
(not (Flag336prime-))
(not (Flag262prime-))
(not (Flag259prime-))
(not (Flag257prime-))
(not (Flag256prime-))
(not (Flag254prime-))
(not (Flag253prime-))
(not (Flag252prime-))
(not (Flag250prime-))
(not (Flag249prime-))
(not (Flag248prime-))
(not (Flag247prime-))
(not (Flag246prime-))
(not (Flag244prime-))
(not (Flag243prime-))
(not (Flag242prime-))
(not (Flag241prime-))
(not (Flag240prime-))
(not (Flag239prime-))
(not (Flag237prime-))
(not (Flag236prime-))
(not (Flag235prime-))
(not (Flag234prime-))
(not (Flag233prime-))
(not (Flag232prime-))
(not (Flag230prime-))
(not (Flag229prime-))
(not (Flag228prime-))
(not (Flag227prime-))
(not (Flag226prime-))
(not (Flag225prime-))
(not (Flag224prime-))
(not (Flag223prime-))
(not (Flag221prime-))
(not (Flag220prime-))
(not (Flag219prime-))
(not (Flag218prime-))
(not (Flag217prime-))
(not (Flag216prime-))
(not (Flag215prime-))
(not (Flag214prime-))
(not (Flag212prime-))
(not (Flag211prime-))
(not (Flag210prime-))
(not (Flag209prime-))
(not (Flag208prime-))
(not (Flag207prime-))
(not (Flag206prime-))
(not (Flag205prime-))
(not (Flag204prime-))
(not (Flag203prime-))
(not (Flag201prime-))
(not (Flag200prime-))
(not (Flag199prime-))
(not (Flag198prime-))
(not (Flag197prime-))
(not (Flag196prime-))
(not (Flag195prime-))
(not (Flag194prime-))
(not (Flag193prime-))
(not (Flag192prime-))
(not (Flag190prime-))
(not (Flag189prime-))
(not (Flag188prime-))
(not (Flag187prime-))
(not (Flag186prime-))
(not (Flag185prime-))
(not (Flag184prime-))
(not (Flag183prime-))
(not (Flag182prime-))
(not (Flag181prime-))
(not (Flag180prime-))
(not (Flag179prime-))
(not (Flag177prime-))
(not (Flag176prime-))
(not (Flag175prime-))
(not (Flag174prime-))
(not (Flag173prime-))
(not (Flag172prime-))
(not (Flag171prime-))
(not (Flag170prime-))
(not (Flag169prime-))
(not (Flag168prime-))
(not (Flag167prime-))
(not (Flag166prime-))
(not (Flag165prime-))
(not (Flag163prime-))
(not (Flag162prime-))
(not (Flag161prime-))
(not (Flag160prime-))
(not (Flag159prime-))
(not (Flag158prime-))
(not (Flag157prime-))
(not (Flag156prime-))
(not (Flag155prime-))
(not (Flag154prime-))
(not (Flag153prime-))
(not (Flag152prime-))
(not (Flag151prime-))
(not (Flag150prime-))
(not (Flag149prime-))
(not (Flag147prime-))
(not (Flag146prime-))
(not (Flag145prime-))
(not (Flag144prime-))
(not (Flag143prime-))
(not (Flag142prime-))
(not (Flag141prime-))
(not (Flag140prime-))
(not (Flag139prime-))
(not (Flag138prime-))
(not (Flag137prime-))
(not (Flag136prime-))
(not (Flag135prime-))
(not (Flag134prime-))
(not (Flag132prime-))
(not (Flag131prime-))
(not (Flag130prime-))
(not (Flag129prime-))
(not (Flag128prime-))
(not (Flag127prime-))
(not (Flag126prime-))
(not (Flag125prime-))
(not (Flag124prime-))
(not (Flag123prime-))
(not (Flag122prime-))
(not (Flag121prime-))
(not (Flag120prime-))
(not (Flag119prime-))
(not (Flag118prime-))
(not (Flag117prime-))
(not (Flag116prime-))
(not (Flag115prime-))
(not (Flag113prime-))
(not (Flag112prime-))
(not (Flag111prime-))
(not (Flag110prime-))
(not (Flag109prime-))
(not (Flag108prime-))
(not (Flag107prime-))
(not (Flag106prime-))
(not (Flag105prime-))
(not (Flag104prime-))
(not (Flag103prime-))
(not (Flag102prime-))
(not (Flag101prime-))
(not (Flag100prime-))
(not (Flag99prime-))
(not (Flag98prime-))
(not (Flag97prime-))
(not (Flag96prime-))
(not (Flag94prime-))
(not (Flag93prime-))
(not (Flag92prime-))
(not (Flag91prime-))
(not (Flag90prime-))
(not (Flag89prime-))
(not (Flag88prime-))
(not (Flag87prime-))
(not (Flag86prime-))
(not (Flag85prime-))
(not (Flag84prime-))
(not (Flag83prime-))
(not (Flag82prime-))
(not (Flag81prime-))
(not (Flag80prime-))
(not (Flag79prime-))
(not (Flag78prime-))
(not (Flag76prime-))
(not (Flag75prime-))
(not (Flag74prime-))
(not (Flag73prime-))
(not (Flag72prime-))
(not (Flag71prime-))
(not (Flag70prime-))
(not (Flag69prime-))
(not (Flag68prime-))
(not (Flag67prime-))
(not (Flag66prime-))
(not (Flag65prime-))
(not (Flag64prime-))
(not (Flag63prime-))
(not (Flag62prime-))
(not (Flag61prime-))
(not (Flag60prime-))
(not (Flag59prime-))
(not (Flag58prime-))
(not (Flag57prime-))
(not (Flag56prime-))
(not (Flag55prime-))
(not (Flag54prime-))
(not (Flag53prime-))
(not (Flag52prime-))
(not (Flag51prime-))
(not (Flag50prime-))
(not (Flag49prime-))
(not (Flag48prime-))
(not (Flag47prime-))
(not (Flag46prime-))
(not (Flag45prime-))
(not (Flag44prime-))
(not (Flag43prime-))
(not (Flag42prime-))
(not (Flag41prime-))
(not (Flag40prime-))
(not (Flag39prime-))
(not (Flag38prime-))
(not (Flag37prime-))
(not (Flag36prime-))
(not (Flag35prime-))
(not (Flag34prime-))
(not (Flag33prime-))
(not (Flag32prime-))
(not (Flag31prime-))
(not (Flag30prime-))
(not (Flag29prime-))
(not (Flag28prime-))
(not (Flag27prime-))
(not (Flag26prime-))
(not (Flag25prime-))
(not (Flag20prime-))
(not (Flag19prime-))
(not (Flag18prime-))
(not (Flag17prime-))
(not (Flag16prime-))
(not (Flag15prime-))
(not (Flag14prime-))
(not (Flag13prime-))
(not (Flag12prime-))
(not (Flag11prime-))
(not (Flag10prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag1prime-))
(not (Flag263prime-))
(not (Flag260prime-))
(not (Flag258prime-))
(not (Flag255prime-))
(not (Flag251prime-))
(not (Flag245prime-))
(not (Flag238prime-))
(not (Flag231prime-))
(not (Flag222prime-))
(not (Flag213prime-))
(not (Flag202prime-))
(not (Flag191prime-))
(not (Flag178prime-))
(not (Flag164prime-))
(not (Flag148prime-))
(not (Flag133prime-))
(not (Flag114prime-))
(not (Flag95prime-))
(not (Flag77prime-))
)
)
(:action MOVELEFT-ROBOT
:parameters ()
:precondition
(and
(Flag4-)
(Flag4prime-)
(Flag260prime-)
(Flag258prime-)
(Flag255prime-)
(Flag251prime-)
(Flag245prime-)
(Flag238prime-)
(Flag231prime-)
(Flag222prime-)
(Flag213prime-)
(Flag202prime-)
(Flag191prime-)
(Flag178prime-)
(Flag164prime-)
(Flag148prime-)
(Flag133prime-)
(Flag114prime-)
(Flag95prime-)
(Flag77prime-)
(Flag39prime-)
(Flag38prime-)
(Flag37prime-)
(Flag36prime-)
(Flag35prime-)
(Flag34prime-)
(Flag33prime-)
(Flag32prime-)
(Flag31prime-)
(Flag30prime-)
(Flag29prime-)
(Flag28prime-)
(Flag27prime-)
(Flag26prime-)
(Flag25prime-)
(Flag24prime-)
(Flag23prime-)
(Flag22prime-)
(Flag21prime-)
(Flag20prime-)
(Flag19prime-)
(Flag18prime-)
(Flag17prime-)
(Flag16prime-)
(Flag15prime-)
(Flag14prime-)
(Flag13prime-)
(Flag12prime-)
(Flag11prime-)
(Flag10prime-)
(Flag9prime-)
(Flag8prime-)
(Flag7prime-)
(Flag6prime-)
(Flag5prime-)
)
:effect
(and
(when
(and
(Flag260-)
)
(and
(COLUMN17-ROBOT)
(not (NOT-COLUMN17-ROBOT))
)
)
(when
(and
(Flag258-)
)
(and
(COLUMN16-ROBOT)
(not (NOT-COLUMN16-ROBOT))
)
)
(when
(and
(Flag255-)
)
(and
(COLUMN15-ROBOT)
(not (NOT-COLUMN15-ROBOT))
)
)
(when
(and
(Flag251-)
)
(and
(COLUMN14-ROBOT)
(not (NOT-COLUMN14-ROBOT))
)
)
(when
(and
(Flag245-)
)
(and
(COLUMN13-ROBOT)
(not (NOT-COLUMN13-ROBOT))
)
)
(when
(and
(Flag238-)
)
(and
(COLUMN12-ROBOT)
(not (NOT-COLUMN12-ROBOT))
)
)
(when
(and
(Flag231-)
)
(and
(COLUMN11-ROBOT)
(not (NOT-COLUMN11-ROBOT))
)
)
(when
(and
(Flag222-)
)
(and
(COLUMN10-ROBOT)
(not (NOT-COLUMN10-ROBOT))
)
)
(when
(and
(Flag213-)
)
(and
(COLUMN9-ROBOT)
(not (NOT-COLUMN9-ROBOT))
)
)
(when
(and
(Flag202-)
)
(and
(COLUMN8-ROBOT)
(not (NOT-COLUMN8-ROBOT))
)
)
(when
(and
(Flag191-)
)
(and
(COLUMN7-ROBOT)
(not (NOT-COLUMN7-ROBOT))
)
)
(when
(and
(Flag178-)
)
(and
(COLUMN6-ROBOT)
(not (NOT-COLUMN6-ROBOT))
)
)
(when
(and
(Flag164-)
)
(and
(COLUMN5-ROBOT)
(not (NOT-COLUMN5-ROBOT))
)
)
(when
(and
(Flag148-)
)
(and
(COLUMN4-ROBOT)
(not (NOT-COLUMN4-ROBOT))
)
)
(when
(and
(Flag133-)
)
(and
(COLUMN3-ROBOT)
(not (NOT-COLUMN3-ROBOT))
)
)
(when
(and
(Flag114-)
)
(and
(COLUMN2-ROBOT)
(not (NOT-COLUMN2-ROBOT))
)
)
(when
(and
(Flag95-)
)
(and
(COLUMN1-ROBOT)
(not (NOT-COLUMN1-ROBOT))
)
)
(when
(and
(Flag77-)
)
(and
(COLUMN0-ROBOT)
(not (NOT-COLUMN0-ROBOT))
)
)
(when
(and
(COLUMN18-ROBOT)
)
(and
(COLUMN17-ROBOT)
(NOT-COLUMN18-ROBOT)
(not (NOT-COLUMN17-ROBOT))
(not (COLUMN18-ROBOT))
)
)
(when
(and
(COLUMN17-ROBOT)
)
(and
(COLUMN16-ROBOT)
(NOT-COLUMN17-ROBOT)
(not (NOT-COLUMN16-ROBOT))
(not (COLUMN17-ROBOT))
)
)
(when
(and
(COLUMN16-ROBOT)
)
(and
(COLUMN15-ROBOT)
(NOT-COLUMN16-ROBOT)
(not (NOT-COLUMN15-ROBOT))
(not (COLUMN16-ROBOT))
)
)
(when
(and
(COLUMN15-ROBOT)
)
(and
(COLUMN14-ROBOT)
(NOT-COLUMN15-ROBOT)
(not (NOT-COLUMN14-ROBOT))
(not (COLUMN15-ROBOT))
)
)
(when
(and
(COLUMN14-ROBOT)
)
(and
(COLUMN13-ROBOT)
(NOT-COLUMN14-ROBOT)
(not (NOT-COLUMN13-ROBOT))
(not (COLUMN14-ROBOT))
)
)
(when
(and
(COLUMN13-ROBOT)
)
(and
(COLUMN12-ROBOT)
(NOT-COLUMN13-ROBOT)
(not (NOT-COLUMN12-ROBOT))
(not (COLUMN13-ROBOT))
)
)
(when
(and
(COLUMN12-ROBOT)
)
(and
(COLUMN11-ROBOT)
(NOT-COLUMN12-ROBOT)
(not (NOT-COLUMN11-ROBOT))
(not (COLUMN12-ROBOT))
)
)
(when
(and
(COLUMN11-ROBOT)
)
(and
(COLUMN10-ROBOT)
(NOT-COLUMN11-ROBOT)
(not (NOT-COLUMN10-ROBOT))
(not (COLUMN11-ROBOT))
)
)
(when
(and
(COLUMN10-ROBOT)
)
(and
(COLUMN9-ROBOT)
(NOT-COLUMN10-ROBOT)
(not (NOT-COLUMN9-ROBOT))
(not (COLUMN10-ROBOT))
)
)
(when
(and
(COLUMN9-ROBOT)
)
(and
(COLUMN8-ROBOT)
(NOT-COLUMN9-ROBOT)
(not (NOT-COLUMN8-ROBOT))
(not (COLUMN9-ROBOT))
)
)
(when
(and
(COLUMN8-ROBOT)
)
(and
(COLUMN7-ROBOT)
(NOT-COLUMN8-ROBOT)
(not (NOT-COLUMN7-ROBOT))
(not (COLUMN8-ROBOT))
)
)
(when
(and
(COLUMN7-ROBOT)
)
(and
(COLUMN6-ROBOT)
(NOT-COLUMN7-ROBOT)
(not (NOT-COLUMN6-ROBOT))
(not (COLUMN7-ROBOT))
)
)
(when
(and
(COLUMN6-ROBOT)
)
(and
(COLUMN5-ROBOT)
(NOT-COLUMN6-ROBOT)
(not (NOT-COLUMN5-ROBOT))
(not (COLUMN6-ROBOT))
)
)
(when
(and
(COLUMN5-ROBOT)
)
(and
(COLUMN4-ROBOT)
(NOT-COLUMN5-ROBOT)
(not (NOT-COLUMN4-ROBOT))
(not (COLUMN5-ROBOT))
)
)
(when
(and
(COLUMN4-ROBOT)
)
(and
(COLUMN3-ROBOT)
(NOT-COLUMN4-ROBOT)
(not (NOT-COLUMN3-ROBOT))
(not (COLUMN4-ROBOT))
)
)
(when
(and
(COLUMN3-ROBOT)
)
(and
(COLUMN2-ROBOT)
(NOT-COLUMN3-ROBOT)
(not (NOT-COLUMN2-ROBOT))
(not (COLUMN3-ROBOT))
)
)
(when
(and
(COLUMN2-ROBOT)
)
(and
(COLUMN1-ROBOT)
(NOT-COLUMN2-ROBOT)
(not (NOT-COLUMN1-ROBOT))
(not (COLUMN2-ROBOT))
)
)
(when
(and
(COLUMN1-ROBOT)
)
(and
(COLUMN0-ROBOT)
(NOT-COLUMN1-ROBOT)
(not (NOT-COLUMN0-ROBOT))
(not (COLUMN1-ROBOT))
)
)
(when
(and
(RIGHTOF18-ROBOT)
)
(and
(RIGHTOF17-ROBOT)
(NOT-RIGHTOF18-ROBOT)
(not (NOT-RIGHTOF17-ROBOT))
(not (RIGHTOF18-ROBOT))
)
)
(when
(and
(Flag39-)
)
(and
(RIGHTOF16-ROBOT)
(NOT-RIGHTOF17-ROBOT)
(not (NOT-RIGHTOF16-ROBOT))
(not (RIGHTOF17-ROBOT))
)
)
(when
(and
(Flag38-)
)
(and
(RIGHTOF15-ROBOT)
(NOT-RIGHTOF16-ROBOT)
(not (NOT-RIGHTOF15-ROBOT))
(not (RIGHTOF16-ROBOT))
)
)
(when
(and
(Flag37-)
)
(and
(RIGHTOF14-ROBOT)
(NOT-RIGHTOF15-ROBOT)
(not (NOT-RIGHTOF14-ROBOT))
(not (RIGHTOF15-ROBOT))
)
)
(when
(and
(Flag36-)
)
(and
(RIGHTOF13-ROBOT)
(NOT-RIGHTOF14-ROBOT)
(not (NOT-RIGHTOF13-ROBOT))
(not (RIGHTOF14-ROBOT))
)
)
(when
(and
(Flag35-)
)
(and
(RIGHTOF12-ROBOT)
(NOT-RIGHTOF13-ROBOT)
(not (NOT-RIGHTOF12-ROBOT))
(not (RIGHTOF13-ROBOT))
)
)
(when
(and
(Flag34-)
)
(and
(RIGHTOF11-ROBOT)
(NOT-RIGHTOF12-ROBOT)
(not (NOT-RIGHTOF11-ROBOT))
(not (RIGHTOF12-ROBOT))
)
)
(when
(and
(Flag33-)
)
(and
(RIGHTOF10-ROBOT)
(NOT-RIGHTOF11-ROBOT)
(not (NOT-RIGHTOF10-ROBOT))
(not (RIGHTOF11-ROBOT))
)
)
(when
(and
(Flag32-)
)
(and
(RIGHTOF9-ROBOT)
(NOT-RIGHTOF10-ROBOT)
(not (NOT-RIGHTOF9-ROBOT))
(not (RIGHTOF10-ROBOT))
)
)
(when
(and
(Flag31-)
)
(and
(RIGHTOF8-ROBOT)
(NOT-RIGHTOF9-ROBOT)
(not (NOT-RIGHTOF8-ROBOT))
(not (RIGHTOF9-ROBOT))
)
)
(when
(and
(Flag30-)
)
(and
(RIGHTOF7-ROBOT)
(NOT-RIGHTOF8-ROBOT)
(not (NOT-RIGHTOF7-ROBOT))
(not (RIGHTOF8-ROBOT))
)
)
(when
(and
(Flag29-)
)
(and
(RIGHTOF6-ROBOT)
(NOT-RIGHTOF7-ROBOT)
(not (NOT-RIGHTOF6-ROBOT))
(not (RIGHTOF7-ROBOT))
)
)
(when
(and
(Flag28-)
)
(and
(RIGHTOF5-ROBOT)
(NOT-RIGHTOF6-ROBOT)
(not (NOT-RIGHTOF5-ROBOT))
(not (RIGHTOF6-ROBOT))
)
)
(when
(and
(Flag27-)
)
(and
(RIGHTOF4-ROBOT)
(NOT-RIGHTOF5-ROBOT)
(not (NOT-RIGHTOF4-ROBOT))
(not (RIGHTOF5-ROBOT))
)
)
(when
(and
(Flag26-)
)
(and
(RIGHTOF3-ROBOT)
(NOT-RIGHTOF4-ROBOT)
(not (NOT-RIGHTOF3-ROBOT))
(not (RIGHTOF4-ROBOT))
)
)
(when
(and
(Flag25-)
)
(and
(RIGHTOF2-ROBOT)
(NOT-RIGHTOF3-ROBOT)
(not (NOT-RIGHTOF2-ROBOT))
(not (RIGHTOF3-ROBOT))
)
)
(when
(and
(Flag24-)
)
(and
(RIGHTOF1-ROBOT)
(NOT-RIGHTOF2-ROBOT)
(not (NOT-RIGHTOF1-ROBOT))
(not (RIGHTOF2-ROBOT))
)
)
(when
(and
(Flag23-)
)
(and
(RIGHTOF0-ROBOT)
(NOT-RIGHTOF1-ROBOT)
(not (NOT-RIGHTOF0-ROBOT))
(not (RIGHTOF1-ROBOT))
)
)
(when
(and
(Flag22-)
)
(and
(LEFTOF18-ROBOT)
(not (NOT-LEFTOF18-ROBOT))
)
)
(when
(and
(Flag21-)
)
(and
(LEFTOF17-ROBOT)
(not (NOT-LEFTOF17-ROBOT))
)
)
(when
(and
(Flag20-)
)
(and
(LEFTOF16-ROBOT)
(not (NOT-LEFTOF16-ROBOT))
)
)
(when
(and
(Flag19-)
)
(and
(LEFTOF15-ROBOT)
(not (NOT-LEFTOF15-ROBOT))
)
)
(when
(and
(Flag18-)
)
(and
(LEFTOF14-ROBOT)
(not (NOT-LEFTOF14-ROBOT))
)
)
(when
(and
(Flag17-)
)
(and
(LEFTOF13-ROBOT)
(not (NOT-LEFTOF13-ROBOT))
)
)
(when
(and
(Flag16-)
)
(and
(LEFTOF12-ROBOT)
(not (NOT-LEFTOF12-ROBOT))
)
)
(when
(and
(Flag15-)
)
(and
(LEFTOF11-ROBOT)
(not (NOT-LEFTOF11-ROBOT))
)
)
(when
(and
(Flag14-)
)
(and
(LEFTOF10-ROBOT)
(not (NOT-LEFTOF10-ROBOT))
)
)
(when
(and
(Flag13-)
)
(and
(LEFTOF9-ROBOT)
(not (NOT-LEFTOF9-ROBOT))
)
)
(when
(and
(Flag12-)
)
(and
(LEFTOF8-ROBOT)
(not (NOT-LEFTOF8-ROBOT))
)
)
(when
(and
(Flag11-)
)
(and
(LEFTOF7-ROBOT)
(not (NOT-LEFTOF7-ROBOT))
)
)
(when
(and
(Flag10-)
)
(and
(LEFTOF6-ROBOT)
(not (NOT-LEFTOF6-ROBOT))
)
)
(when
(and
(Flag9-)
)
(and
(LEFTOF5-ROBOT)
(not (NOT-LEFTOF5-ROBOT))
)
)
(when
(and
(Flag8-)
)
(and
(LEFTOF4-ROBOT)
(not (NOT-LEFTOF4-ROBOT))
)
)
(when
(and
(Flag7-)
)
(and
(LEFTOF3-ROBOT)
(not (NOT-LEFTOF3-ROBOT))
)
)
(when
(and
(Flag6-)
)
(and
(LEFTOF2-ROBOT)
(not (NOT-LEFTOF2-ROBOT))
)
)
(when
(and
(Flag5-)
)
(and
(LEFTOF1-ROBOT)
(not (NOT-LEFTOF1-ROBOT))
)
)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag281prime-))
(not (Flag280prime-))
(not (Flag261prime-))
(not (Flag24prime-))
(not (Flag23prime-))
(not (Flag22prime-))
(not (Flag21prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag522prime-))
(not (Flag520prime-))
(not (Flag518prime-))
(not (Flag517prime-))
(not (Flag515prime-))
(not (Flag514prime-))
(not (Flag513prime-))
(not (Flag511prime-))
(not (Flag510prime-))
(not (Flag509prime-))
(not (Flag508prime-))
(not (Flag506prime-))
(not (Flag505prime-))
(not (Flag504prime-))
(not (Flag503prime-))
(not (Flag502prime-))
(not (Flag500prime-))
(not (Flag499prime-))
(not (Flag498prime-))
(not (Flag497prime-))
(not (Flag496prime-))
(not (Flag495prime-))
(not (Flag494prime-))
(not (Flag492prime-))
(not (Flag491prime-))
(not (Flag490prime-))
(not (Flag489prime-))
(not (Flag488prime-))
(not (Flag487prime-))
(not (Flag486prime-))
(not (Flag485prime-))
(not (Flag483prime-))
(not (Flag482prime-))
(not (Flag481prime-))
(not (Flag480prime-))
(not (Flag479prime-))
(not (Flag478prime-))
(not (Flag477prime-))
(not (Flag476prime-))
(not (Flag474prime-))
(not (Flag473prime-))
(not (Flag472prime-))
(not (Flag471prime-))
(not (Flag470prime-))
(not (Flag469prime-))
(not (Flag468prime-))
(not (Flag467prime-))
(not (Flag466prime-))
(not (Flag464prime-))
(not (Flag463prime-))
(not (Flag462prime-))
(not (Flag461prime-))
(not (Flag460prime-))
(not (Flag459prime-))
(not (Flag458prime-))
(not (Flag457prime-))
(not (Flag456prime-))
(not (Flag455prime-))
(not (Flag454prime-))
(not (Flag452prime-))
(not (Flag451prime-))
(not (Flag450prime-))
(not (Flag449prime-))
(not (Flag448prime-))
(not (Flag447prime-))
(not (Flag446prime-))
(not (Flag445prime-))
(not (Flag444prime-))
(not (Flag443prime-))
(not (Flag442prime-))
(not (Flag441prime-))
(not (Flag440prime-))
(not (Flag438prime-))
(not (Flag437prime-))
(not (Flag436prime-))
(not (Flag435prime-))
(not (Flag434prime-))
(not (Flag433prime-))
(not (Flag432prime-))
(not (Flag431prime-))
(not (Flag430prime-))
(not (Flag429prime-))
(not (Flag428prime-))
(not (Flag427prime-))
(not (Flag426prime-))
(not (Flag424prime-))
(not (Flag423prime-))
(not (Flag422prime-))
(not (Flag421prime-))
(not (Flag420prime-))
(not (Flag419prime-))
(not (Flag418prime-))
(not (Flag417prime-))
(not (Flag416prime-))
(not (Flag415prime-))
(not (Flag414prime-))
(not (Flag413prime-))
(not (Flag412prime-))
(not (Flag411prime-))
(not (Flag410prime-))
(not (Flag409prime-))
(not (Flag407prime-))
(not (Flag406prime-))
(not (Flag405prime-))
(not (Flag404prime-))
(not (Flag403prime-))
(not (Flag402prime-))
(not (Flag401prime-))
(not (Flag400prime-))
(not (Flag399prime-))
(not (Flag398prime-))
(not (Flag397prime-))
(not (Flag396prime-))
(not (Flag395prime-))
(not (Flag394prime-))
(not (Flag393prime-))
(not (Flag392prime-))
(not (Flag390prime-))
(not (Flag389prime-))
(not (Flag388prime-))
(not (Flag387prime-))
(not (Flag386prime-))
(not (Flag385prime-))
(not (Flag384prime-))
(not (Flag383prime-))
(not (Flag382prime-))
(not (Flag381prime-))
(not (Flag380prime-))
(not (Flag379prime-))
(not (Flag378prime-))
(not (Flag377prime-))
(not (Flag376prime-))
(not (Flag375prime-))
(not (Flag373prime-))
(not (Flag372prime-))
(not (Flag371prime-))
(not (Flag370prime-))
(not (Flag369prime-))
(not (Flag368prime-))
(not (Flag367prime-))
(not (Flag366prime-))
(not (Flag365prime-))
(not (Flag364prime-))
(not (Flag363prime-))
(not (Flag362prime-))
(not (Flag361prime-))
(not (Flag360prime-))
(not (Flag359prime-))
(not (Flag358prime-))
(not (Flag357prime-))
(not (Flag356prime-))
(not (Flag354prime-))
(not (Flag353prime-))
(not (Flag352prime-))
(not (Flag351prime-))
(not (Flag350prime-))
(not (Flag349prime-))
(not (Flag348prime-))
(not (Flag347prime-))
(not (Flag346prime-))
(not (Flag345prime-))
(not (Flag344prime-))
(not (Flag343prime-))
(not (Flag342prime-))
(not (Flag341prime-))
(not (Flag340prime-))
(not (Flag339prime-))
(not (Flag338prime-))
(not (Flag337prime-))
(not (Flag335prime-))
(not (Flag334prime-))
(not (Flag333prime-))
(not (Flag332prime-))
(not (Flag331prime-))
(not (Flag330prime-))
(not (Flag329prime-))
(not (Flag328prime-))
(not (Flag327prime-))
(not (Flag326prime-))
(not (Flag325prime-))
(not (Flag324prime-))
(not (Flag323prime-))
(not (Flag322prime-))
(not (Flag321prime-))
(not (Flag320prime-))
(not (Flag319prime-))
(not (Flag318prime-))
(not (Flag317prime-))
(not (Flag316prime-))
(not (Flag315prime-))
(not (Flag314prime-))
(not (Flag313prime-))
(not (Flag312prime-))
(not (Flag311prime-))
(not (Flag310prime-))
(not (Flag309prime-))
(not (Flag308prime-))
(not (Flag307prime-))
(not (Flag306prime-))
(not (Flag305prime-))
(not (Flag304prime-))
(not (Flag303prime-))
(not (Flag302prime-))
(not (Flag301prime-))
(not (Flag300prime-))
(not (Flag299prime-))
(not (Flag298prime-))
(not (Flag297prime-))
(not (Flag296prime-))
(not (Flag295prime-))
(not (Flag294prime-))
(not (Flag293prime-))
(not (Flag292prime-))
(not (Flag291prime-))
(not (Flag290prime-))
(not (Flag289prime-))
(not (Flag288prime-))
(not (Flag287prime-))
(not (Flag286prime-))
(not (Flag285prime-))
(not (Flag284prime-))
(not (Flag283prime-))
(not (Flag282prime-))
(not (Flag279prime-))
(not (Flag278prime-))
(not (Flag277prime-))
(not (Flag276prime-))
(not (Flag275prime-))
(not (Flag274prime-))
(not (Flag273prime-))
(not (Flag272prime-))
(not (Flag271prime-))
(not (Flag270prime-))
(not (Flag269prime-))
(not (Flag268prime-))
(not (Flag267prime-))
(not (Flag266prime-))
(not (Flag265prime-))
(not (Flag264prime-))
(not (Flag4prime-))
(not (Flag521prime-))
(not (Flag519prime-))
(not (Flag516prime-))
(not (Flag512prime-))
(not (Flag507prime-))
(not (Flag501prime-))
(not (Flag493prime-))
(not (Flag484prime-))
(not (Flag475prime-))
(not (Flag465prime-))
(not (Flag453prime-))
(not (Flag439prime-))
(not (Flag425prime-))
(not (Flag408prime-))
(not (Flag391prime-))
(not (Flag374prime-))
(not (Flag355prime-))
(not (Flag336prime-))
(not (Flag262prime-))
(not (Flag259prime-))
(not (Flag257prime-))
(not (Flag256prime-))
(not (Flag254prime-))
(not (Flag253prime-))
(not (Flag252prime-))
(not (Flag250prime-))
(not (Flag249prime-))
(not (Flag248prime-))
(not (Flag247prime-))
(not (Flag246prime-))
(not (Flag244prime-))
(not (Flag243prime-))
(not (Flag242prime-))
(not (Flag241prime-))
(not (Flag240prime-))
(not (Flag239prime-))
(not (Flag237prime-))
(not (Flag236prime-))
(not (Flag235prime-))
(not (Flag234prime-))
(not (Flag233prime-))
(not (Flag232prime-))
(not (Flag230prime-))
(not (Flag229prime-))
(not (Flag228prime-))
(not (Flag227prime-))
(not (Flag226prime-))
(not (Flag225prime-))
(not (Flag224prime-))
(not (Flag223prime-))
(not (Flag221prime-))
(not (Flag220prime-))
(not (Flag219prime-))
(not (Flag218prime-))
(not (Flag217prime-))
(not (Flag216prime-))
(not (Flag215prime-))
(not (Flag214prime-))
(not (Flag212prime-))
(not (Flag211prime-))
(not (Flag210prime-))
(not (Flag209prime-))
(not (Flag208prime-))
(not (Flag207prime-))
(not (Flag206prime-))
(not (Flag205prime-))
(not (Flag204prime-))
(not (Flag203prime-))
(not (Flag201prime-))
(not (Flag200prime-))
(not (Flag199prime-))
(not (Flag198prime-))
(not (Flag197prime-))
(not (Flag196prime-))
(not (Flag195prime-))
(not (Flag194prime-))
(not (Flag193prime-))
(not (Flag192prime-))
(not (Flag190prime-))
(not (Flag189prime-))
(not (Flag188prime-))
(not (Flag187prime-))
(not (Flag186prime-))
(not (Flag185prime-))
(not (Flag184prime-))
(not (Flag183prime-))
(not (Flag182prime-))
(not (Flag181prime-))
(not (Flag180prime-))
(not (Flag179prime-))
(not (Flag177prime-))
(not (Flag176prime-))
(not (Flag175prime-))
(not (Flag174prime-))
(not (Flag173prime-))
(not (Flag172prime-))
(not (Flag171prime-))
(not (Flag170prime-))
(not (Flag169prime-))
(not (Flag168prime-))
(not (Flag167prime-))
(not (Flag166prime-))
(not (Flag165prime-))
(not (Flag163prime-))
(not (Flag162prime-))
(not (Flag161prime-))
(not (Flag160prime-))
(not (Flag159prime-))
(not (Flag158prime-))
(not (Flag157prime-))
(not (Flag156prime-))
(not (Flag155prime-))
(not (Flag154prime-))
(not (Flag153prime-))
(not (Flag152prime-))
(not (Flag151prime-))
(not (Flag150prime-))
(not (Flag149prime-))
(not (Flag147prime-))
(not (Flag146prime-))
(not (Flag145prime-))
(not (Flag144prime-))
(not (Flag143prime-))
(not (Flag142prime-))
(not (Flag141prime-))
(not (Flag140prime-))
(not (Flag139prime-))
(not (Flag138prime-))
(not (Flag137prime-))
(not (Flag136prime-))
(not (Flag135prime-))
(not (Flag134prime-))
(not (Flag132prime-))
(not (Flag131prime-))
(not (Flag130prime-))
(not (Flag129prime-))
(not (Flag128prime-))
(not (Flag127prime-))
(not (Flag126prime-))
(not (Flag125prime-))
(not (Flag124prime-))
(not (Flag123prime-))
(not (Flag122prime-))
(not (Flag121prime-))
(not (Flag120prime-))
(not (Flag119prime-))
(not (Flag118prime-))
(not (Flag117prime-))
(not (Flag116prime-))
(not (Flag115prime-))
(not (Flag113prime-))
(not (Flag112prime-))
(not (Flag111prime-))
(not (Flag110prime-))
(not (Flag109prime-))
(not (Flag108prime-))
(not (Flag107prime-))
(not (Flag106prime-))
(not (Flag105prime-))
(not (Flag104prime-))
(not (Flag103prime-))
(not (Flag102prime-))
(not (Flag101prime-))
(not (Flag100prime-))
(not (Flag99prime-))
(not (Flag98prime-))
(not (Flag97prime-))
(not (Flag96prime-))
(not (Flag94prime-))
(not (Flag93prime-))
(not (Flag92prime-))
(not (Flag91prime-))
(not (Flag90prime-))
(not (Flag89prime-))
(not (Flag88prime-))
(not (Flag87prime-))
(not (Flag86prime-))
(not (Flag85prime-))
(not (Flag84prime-))
(not (Flag83prime-))
(not (Flag82prime-))
(not (Flag81prime-))
(not (Flag80prime-))
(not (Flag79prime-))
(not (Flag78prime-))
(not (Flag76prime-))
(not (Flag75prime-))
(not (Flag74prime-))
(not (Flag73prime-))
(not (Flag72prime-))
(not (Flag71prime-))
(not (Flag70prime-))
(not (Flag69prime-))
(not (Flag68prime-))
(not (Flag67prime-))
(not (Flag66prime-))
(not (Flag65prime-))
(not (Flag64prime-))
(not (Flag63prime-))
(not (Flag62prime-))
(not (Flag61prime-))
(not (Flag60prime-))
(not (Flag59prime-))
(not (Flag58prime-))
(not (Flag57prime-))
(not (Flag56prime-))
(not (Flag55prime-))
(not (Flag54prime-))
(not (Flag53prime-))
(not (Flag52prime-))
(not (Flag51prime-))
(not (Flag50prime-))
(not (Flag49prime-))
(not (Flag48prime-))
(not (Flag47prime-))
(not (Flag46prime-))
(not (Flag45prime-))
(not (Flag44prime-))
(not (Flag43prime-))
(not (Flag42prime-))
(not (Flag41prime-))
(not (Flag40prime-))
(not (Flag39prime-))
(not (Flag38prime-))
(not (Flag37prime-))
(not (Flag36prime-))
(not (Flag35prime-))
(not (Flag34prime-))
(not (Flag33prime-))
(not (Flag32prime-))
(not (Flag31prime-))
(not (Flag30prime-))
(not (Flag29prime-))
(not (Flag28prime-))
(not (Flag27prime-))
(not (Flag26prime-))
(not (Flag25prime-))
(not (Flag20prime-))
(not (Flag19prime-))
(not (Flag18prime-))
(not (Flag17prime-))
(not (Flag16prime-))
(not (Flag15prime-))
(not (Flag14prime-))
(not (Flag13prime-))
(not (Flag12prime-))
(not (Flag11prime-))
(not (Flag10prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag1prime-))
(not (Flag263prime-))
(not (Flag260prime-))
(not (Flag258prime-))
(not (Flag255prime-))
(not (Flag251prime-))
(not (Flag245prime-))
(not (Flag238prime-))
(not (Flag231prime-))
(not (Flag222prime-))
(not (Flag213prime-))
(not (Flag202prime-))
(not (Flag191prime-))
(not (Flag178prime-))
(not (Flag164prime-))
(not (Flag148prime-))
(not (Flag133prime-))
(not (Flag114prime-))
(not (Flag95prime-))
(not (Flag77prime-))
)
)
(:action Prim1Action-1
:parameters ()
:precondition
(and
(not (ROW1-ROBOT))
)
:effect
(and
(Flag1prime-)
(not (Flag1-))
)

)
(:action CHECKCONSISTENCYACTION
:parameters ()
:precondition
(and
(NOT-ERROR-)
(CHECKCONSISTENCY-)
)
:effect
(and
(when
(and
(LEFTOF5-ROBOT)
(RIGHTOF17-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF17-ROBOT)
(LEFTOF3-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF8-ROBOT)
(ABOVEOF16-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF11-ROBOT)
(LEFTOF4-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF17-ROBOT)
(BELOWOF3-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF4-ROBOT)
(RIGHTOF18-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF4-ROBOT)
(RIGHTOF15-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF17-ROBOT)
(BELOWOF6-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF1-ROBOT)
(RIGHTOF13-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF10-ROBOT)
(BELOWOF2-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF5-ROBOT)
(RIGHTOF15-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF5-ROBOT)
(ABOVEOF12-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF8-ROBOT)
(ABOVEOF15-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF16-ROBOT)
(BELOWOF1-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF4-ROBOT)
(ABOVEOF15-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF17-ROBOT)
(BELOWOF10-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF2-ROBOT)
(ABOVEOF8-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF14-ROBOT)
(ABOVEOF15-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF5-ROBOT)
(RIGHTOF12-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF1-ROBOT)
(RIGHTOF7-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF13-ROBOT)
(LEFTOF2-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF15-ROBOT)
(BELOWOF1-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF2-ROBOT)
(ABOVEOF18-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF9-ROBOT)
(RIGHTOF12-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF15-ROBOT)
(LEFTOF11-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF18-ROBOT)
(BELOWOF11-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF7-ROBOT)
(LEFTOF4-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF13-ROBOT)
(RIGHTOF18-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF2-ROBOT)
(RIGHTOF5-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF18-ROBOT)
(BELOWOF6-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF11-ROBOT)
(LEFTOF2-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF6-ROBOT)
(RIGHTOF17-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF9-ROBOT)
(LEFTOF2-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF1-ROBOT)
(ABOVEOF10-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF6-ROBOT)
(BELOWOF3-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF11-ROBOT)
(BELOWOF8-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF8-ROBOT)
(RIGHTOF14-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF15-ROBOT)
(LEFTOF2-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF3-ROBOT)
(ABOVEOF12-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF16-ROBOT)
(LEFTOF8-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF3-ROBOT)
(RIGHTOF14-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF17-ROBOT)
(LEFTOF13-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF3-ROBOT)
(RIGHTOF5-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF5-ROBOT)
(ABOVEOF16-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF3-ROBOT)
(RIGHTOF9-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF14-ROBOT)
(BELOWOF1-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF3-ROBOT)
(ABOVEOF14-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF4-ROBOT)
(RIGHTOF10-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF12-ROBOT)
(BELOWOF10-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF16-ROBOT)
(BELOWOF2-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF15-ROBOT)
(LEFTOF6-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF16-ROBOT)
(BELOWOF11-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF11-ROBOT)
(BELOWOF1-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF4-ROBOT)
(RIGHTOF12-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF16-ROBOT)
(LEFTOF7-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF5-ROBOT)
(RIGHTOF9-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF18-ROBOT)
(BELOWOF7-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF5-ROBOT)
(ABOVEOF17-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF14-ROBOT)
(LEFTOF5-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF6-ROBOT)
(ABOVEOF10-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF17-ROBOT)
(BELOWOF1-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF6-ROBOT)
(ABOVEOF14-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF8-ROBOT)
(BELOWOF1-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF14-ROBOT)
(BELOWOF7-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF1-ROBOT)
(ABOVEOF18-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF13-ROBOT)
(BELOWOF1-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF16-ROBOT)
(LEFTOF12-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF8-ROBOT)
(ABOVEOF18-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF3-ROBOT)
(ABOVEOF9-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF10-ROBOT)
(RIGHTOF16-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF11-ROBOT)
(LEFTOF3-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF13-ROBOT)
(BELOWOF3-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF5-ROBOT)
(RIGHTOF18-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF6-ROBOT)
(ABOVEOF16-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF18-ROBOT)
(LEFTOF8-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF3-ROBOT)
(RIGHTOF13-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF7-ROBOT)
(RIGHTOF8-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF14-ROBOT)
(LEFTOF1-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF9-ROBOT)
(ABOVEOF12-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF10-ROBOT)
(BELOWOF3-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF13-ROBOT)
(BELOWOF7-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF3-ROBOT)
(RIGHTOF16-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF15-ROBOT)
(LEFTOF8-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF14-ROBOT)
(LEFTOF2-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF7-ROBOT)
(LEFTOF5-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF4-ROBOT)
(ABOVEOF16-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF17-ROBOT)
(LEFTOF7-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF8-ROBOT)
(LEFTOF3-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF2-ROBOT)
(RIGHTOF16-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF12-ROBOT)
(BELOWOF4-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF18-ROBOT)
(LEFTOF9-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF15-ROBOT)
(BELOWOF11-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF12-ROBOT)
(LEFTOF2-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF18-ROBOT)
(LEFTOF1-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF15-ROBOT)
(LEFTOF3-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF15-ROBOT)
(LEFTOF9-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF14-ROBOT)
(LEFTOF9-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF15-ROBOT)
(BELOWOF6-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF18-ROBOT)
(BELOWOF5-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF6-ROBOT)
(ABOVEOF12-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF17-ROBOT)
(LEFTOF2-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF17-ROBOT)
(BELOWOF12-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF5-ROBOT)
(ABOVEOF10-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF18-ROBOT)
(LEFTOF12-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF13-ROBOT)
(RIGHTOF14-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF12-ROBOT)
(BELOWOF1-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF1-ROBOT)
(RIGHTOF15-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF10-ROBOT)
(ABOVEOF14-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF2-ROBOT)
(ABOVEOF12-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF5-ROBOT)
(RIGHTOF10-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF6-ROBOT)
(RIGHTOF11-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF18-ROBOT)
(LEFTOF14-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF9-ROBOT)
(BELOWOF1-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF7-ROBOT)
(RIGHTOF14-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF13-ROBOT)
(BELOWOF6-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF7-ROBOT)
(ABOVEOF12-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF1-ROBOT)
(RIGHTOF5-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF14-ROBOT)
(BELOWOF9-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF8-ROBOT)
(LEFTOF1-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF11-ROBOT)
(BELOWOF10-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF1-ROBOT)
(ABOVEOF4-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF6-ROBOT)
(RIGHTOF14-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF17-ROBOT)
(BELOWOF4-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF4-ROBOT)
(ABOVEOF9-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF7-ROBOT)
(BELOWOF2-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF11-ROBOT)
(ABOVEOF12-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF11-ROBOT)
(ABOVEOF14-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF6-ROBOT)
(RIGHTOF16-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF6-ROBOT)
(ABOVEOF11-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF8-ROBOT)
(ABOVEOF9-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF4-ROBOT)
(RIGHTOF13-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF11-ROBOT)
(BELOWOF3-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF18-ROBOT)
(LEFTOF3-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF16-ROBOT)
(BELOWOF12-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF10-ROBOT)
(RIGHTOF13-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF4-ROBOT)
(ABOVEOF18-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF7-ROBOT)
(ABOVEOF16-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF3-ROBOT)
(ABOVEOF8-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF14-ROBOT)
(BELOWOF2-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF17-ROBOT)
(LEFTOF11-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF3-ROBOT)
(ABOVEOF5-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF7-ROBOT)
(RIGHTOF11-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF10-ROBOT)
(RIGHTOF18-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF13-ROBOT)
(BELOWOF11-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF16-ROBOT)
(BELOWOF3-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF6-ROBOT)
(LEFTOF2-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF11-ROBOT)
(RIGHTOF18-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF1-ROBOT)
(ABOVEOF1-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF4-ROBOT)
(LEFTOF2-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF18-ROBOT)
(BELOWOF10-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF15-ROBOT)
(LEFTOF7-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF17-ROBOT)
(LEFTOF9-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF6-ROBOT)
(RIGHTOF10-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF13-ROBOT)
(LEFTOF5-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF16-ROBOT)
(LEFTOF9-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF8-ROBOT)
(BELOWOF4-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF13-ROBOT)
(BELOWOF2-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF17-ROBOT)
(LEFTOF8-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF7-ROBOT)
(RIGHTOF13-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF15-ROBOT)
(BELOWOF7-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF10-ROBOT)
(ABOVEOF10-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF18-ROBOT)
(LEFTOF17-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF18-ROBOT)
(LEFTOF7-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF4-ROBOT)
(RIGHTOF8-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF17-ROBOT)
(BELOWOF8-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF13-ROBOT)
(BELOWOF8-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF11-ROBOT)
(BELOWOF2-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF3-ROBOT)
(ABOVEOF15-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF6-ROBOT)
(RIGHTOF13-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF10-ROBOT)
(LEFTOF3-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF3-ROBOT)
(ABOVEOF18-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF10-ROBOT)
(LEFTOF1-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF1-ROBOT)
(RIGHTOF4-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF16-ROBOT)
(BELOWOF13-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF2-ROBOT)
(ABOVEOF9-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF18-ROBOT)
(BELOWOF13-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF6-ROBOT)
(RIGHTOF12-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF5-ROBOT)
(BELOWOF4-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF5-ROBOT)
(RIGHTOF6-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF15-ROBOT)
(BELOWOF2-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF4-ROBOT)
(ABOVEOF6-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF4-ROBOT)
(ABOVEOF10-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF14-ROBOT)
(ABOVEOF17-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF11-ROBOT)
(RIGHTOF14-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF9-ROBOT)
(ABOVEOF16-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF10-ROBOT)
(RIGHTOF12-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF12-ROBOT)
(ABOVEOF12-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF17-ROBOT)
(LEFTOF4-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF18-ROBOT)
(BELOWOF16-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF11-ROBOT)
(BELOWOF5-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF17-ROBOT)
(BELOWOF15-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF7-ROBOT)
(LEFTOF2-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF15-ROBOT)
(BELOWOF10-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF12-ROBOT)
(RIGHTOF15-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF15-ROBOT)
(RIGHTOF18-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF12-ROBOT)
(LEFTOF1-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF1-ROBOT)
(RIGHTOF17-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF4-ROBOT)
(ABOVEOF13-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF13-ROBOT)
(BELOWOF12-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF11-ROBOT)
(BELOWOF4-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF17-ROBOT)
(BELOWOF7-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF18-ROBOT)
(LEFTOF2-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF2-ROBOT)
(ABOVEOF17-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF4-ROBOT)
(RIGHTOF14-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF12-ROBOT)
(RIGHTOF17-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF11-ROBOT)
(LEFTOF11-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF7-ROBOT)
(BELOWOF1-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF7-ROBOT)
(ABOVEOF10-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF6-ROBOT)
(LEFTOF4-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF6-ROBOT)
(RIGHTOF9-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF11-ROBOT)
(RIGHTOF13-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF7-ROBOT)
(BELOWOF6-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF18-ROBOT)
(BELOWOF12-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF15-ROBOT)
(BELOWOF9-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF16-ROBOT)
(LEFTOF5-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF5-ROBOT)
(RIGHTOF8-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF8-ROBOT)
(BELOWOF5-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF7-ROBOT)
(RIGHTOF12-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF16-ROBOT)
(BELOWOF10-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF2-ROBOT)
(RIGHTOF10-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF10-ROBOT)
(RIGHTOF17-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF9-ROBOT)
(LEFTOF4-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF16-ROBOT)
(LEFTOF1-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF10-ROBOT)
(RIGHTOF14-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF5-ROBOT)
(RIGHTOF11-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF9-ROBOT)
(RIGHTOF13-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF6-ROBOT)
(BELOWOF2-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF7-ROBOT)
(BELOWOF4-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF15-ROBOT)
(BELOWOF5-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF15-ROBOT)
(RIGHTOF16-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF11-ROBOT)
(LEFTOF8-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF2-ROBOT)
(RIGHTOF8-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF7-ROBOT)
(RIGHTOF10-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF8-ROBOT)
(ABOVEOF14-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF15-ROBOT)
(ABOVEOF16-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF6-ROBOT)
(RIGHTOF18-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF13-ROBOT)
(BELOWOF10-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF4-ROBOT)
(ABOVEOF14-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF10-ROBOT)
(BELOWOF8-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF7-ROBOT)
(BELOWOF5-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF3-ROBOT)
(RIGHTOF7-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF1-ROBOT)
(ABOVEOF3-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF9-ROBOT)
(LEFTOF7-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF1-ROBOT)
(RIGHTOF11-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF5-ROBOT)
(ABOVEOF13-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF5-ROBOT)
(ABOVEOF14-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF7-ROBOT)
(ABOVEOF11-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF6-ROBOT)
(BELOWOF6-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF6-ROBOT)
(RIGHTOF8-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF4-ROBOT)
(RIGHTOF16-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF13-ROBOT)
(RIGHTOF15-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF9-ROBOT)
(ABOVEOF18-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF6-ROBOT)
(BELOWOF5-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF15-ROBOT)
(ABOVEOF18-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF6-ROBOT)
(BELOWOF1-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF3-ROBOT)
(ABOVEOF3-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF15-ROBOT)
(RIGHTOF17-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF18-ROBOT)
(BELOWOF17-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF17-ROBOT)
(BELOWOF11-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF5-ROBOT)
(BELOWOF1-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF6-ROBOT)
(ABOVEOF9-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF9-ROBOT)
(LEFTOF1-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF3-ROBOT)
(RIGHTOF6-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF4-ROBOT)
(BELOWOF2-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF11-ROBOT)
(BELOWOF11-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF7-ROBOT)
(BELOWOF7-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF15-ROBOT)
(BELOWOF12-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF17-ROBOT)
(LEFTOF14-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF17-ROBOT)
(BELOWOF13-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF9-ROBOT)
(RIGHTOF10-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF11-ROBOT)
(RIGHTOF16-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF9-ROBOT)
(BELOWOF5-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF9-ROBOT)
(RIGHTOF9-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF12-ROBOT)
(LEFTOF3-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF16-ROBOT)
(RIGHTOF18-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF10-ROBOT)
(LEFTOF8-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF9-ROBOT)
(BELOWOF7-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF4-ROBOT)
(ABOVEOF4-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF18-ROBOT)
(BELOWOF14-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF7-ROBOT)
(ABOVEOF8-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF14-ROBOT)
(LEFTOF12-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF13-ROBOT)
(RIGHTOF16-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF2-ROBOT)
(ABOVEOF2-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF3-ROBOT)
(ABOVEOF7-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF9-ROBOT)
(ABOVEOF13-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF12-ROBOT)
(ABOVEOF14-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF13-ROBOT)
(BELOWOF13-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF13-ROBOT)
(LEFTOF8-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF1-ROBOT)
(RIGHTOF6-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF1-ROBOT)
(RIGHTOF3-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF10-ROBOT)
(RIGHTOF15-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF14-ROBOT)
(RIGHTOF16-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF14-ROBOT)
(BELOWOF14-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF6-ROBOT)
(RIGHTOF6-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF3-ROBOT)
(BELOWOF2-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF17-ROBOT)
(BELOWOF17-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF14-ROBOT)
(RIGHTOF15-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF9-ROBOT)
(ABOVEOF10-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF5-ROBOT)
(BELOWOF2-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF1-ROBOT)
(RIGHTOF2-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF9-ROBOT)
(ABOVEOF17-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF12-ROBOT)
(RIGHTOF12-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF4-ROBOT)
(BELOWOF3-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF8-ROBOT)
(BELOWOF6-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF2-ROBOT)
(BELOWOF1-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF16-ROBOT)
(RIGHTOF17-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF4-ROBOT)
(RIGHTOF4-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF3-ROBOT)
(LEFTOF2-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF13-ROBOT)
(RIGHTOF13-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF15-ROBOT)
(ABOVEOF15-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF18-ROBOT)
(BELOWOF18-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF17-ROBOT)
(LEFTOF17-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF8-ROBOT)
(RIGHTOF12-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF11-ROBOT)
(RIGHTOF12-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF8-ROBOT)
(LEFTOF8-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF14-ROBOT)
(BELOWOF13-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF11-ROBOT)
(BELOWOF9-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF8-ROBOT)
(ABOVEOF12-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF13-ROBOT)
(ABOVEOF15-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF14-ROBOT)
(RIGHTOF14-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF16-ROBOT)
(BELOWOF14-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF10-ROBOT)
(RIGHTOF11-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF16-ROBOT)
(ABOVEOF16-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF10-ROBOT)
(LEFTOF10-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF9-ROBOT)
(LEFTOF8-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF17-ROBOT)
(BELOWOF16-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF9-ROBOT)
(RIGHTOF11-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF4-ROBOT)
(RIGHTOF5-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF5-ROBOT)
(ABOVEOF5-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF18-ROBOT)
(RIGHTOF18-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF7-ROBOT)
(LEFTOF6-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF3-ROBOT)
(RIGHTOF4-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF9-ROBOT)
(BELOWOF9-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF7-ROBOT)
(LEFTOF7-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF5-ROBOT)
(RIGHTOF5-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF16-ROBOT)
(RIGHTOF16-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF3-ROBOT)
(LEFTOF3-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF12-ROBOT)
(RIGHTOF13-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF2-ROBOT)
(RIGHTOF2-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF8-ROBOT)
(ABOVEOF8-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF15-ROBOT)
(RIGHTOF15-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF1-ROBOT)
(RIGHTOF1-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(NOT-CHECKCONSISTENCY-)
(not (CHECKCONSISTENCY-))
(not (Flag281prime-))
(not (Flag280prime-))
(not (Flag261prime-))
(not (Flag24prime-))
(not (Flag23prime-))
(not (Flag22prime-))
(not (Flag21prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag522prime-))
(not (Flag520prime-))
(not (Flag518prime-))
(not (Flag517prime-))
(not (Flag515prime-))
(not (Flag514prime-))
(not (Flag513prime-))
(not (Flag511prime-))
(not (Flag510prime-))
(not (Flag509prime-))
(not (Flag508prime-))
(not (Flag506prime-))
(not (Flag505prime-))
(not (Flag504prime-))
(not (Flag503prime-))
(not (Flag502prime-))
(not (Flag500prime-))
(not (Flag499prime-))
(not (Flag498prime-))
(not (Flag497prime-))
(not (Flag496prime-))
(not (Flag495prime-))
(not (Flag494prime-))
(not (Flag492prime-))
(not (Flag491prime-))
(not (Flag490prime-))
(not (Flag489prime-))
(not (Flag488prime-))
(not (Flag487prime-))
(not (Flag486prime-))
(not (Flag485prime-))
(not (Flag483prime-))
(not (Flag482prime-))
(not (Flag481prime-))
(not (Flag480prime-))
(not (Flag479prime-))
(not (Flag478prime-))
(not (Flag477prime-))
(not (Flag476prime-))
(not (Flag474prime-))
(not (Flag473prime-))
(not (Flag472prime-))
(not (Flag471prime-))
(not (Flag470prime-))
(not (Flag469prime-))
(not (Flag468prime-))
(not (Flag467prime-))
(not (Flag466prime-))
(not (Flag464prime-))
(not (Flag463prime-))
(not (Flag462prime-))
(not (Flag461prime-))
(not (Flag460prime-))
(not (Flag459prime-))
(not (Flag458prime-))
(not (Flag457prime-))
(not (Flag456prime-))
(not (Flag455prime-))
(not (Flag454prime-))
(not (Flag452prime-))
(not (Flag451prime-))
(not (Flag450prime-))
(not (Flag449prime-))
(not (Flag448prime-))
(not (Flag447prime-))
(not (Flag446prime-))
(not (Flag445prime-))
(not (Flag444prime-))
(not (Flag443prime-))
(not (Flag442prime-))
(not (Flag441prime-))
(not (Flag440prime-))
(not (Flag438prime-))
(not (Flag437prime-))
(not (Flag436prime-))
(not (Flag435prime-))
(not (Flag434prime-))
(not (Flag433prime-))
(not (Flag432prime-))
(not (Flag431prime-))
(not (Flag430prime-))
(not (Flag429prime-))
(not (Flag428prime-))
(not (Flag427prime-))
(not (Flag426prime-))
(not (Flag424prime-))
(not (Flag423prime-))
(not (Flag422prime-))
(not (Flag421prime-))
(not (Flag420prime-))
(not (Flag419prime-))
(not (Flag418prime-))
(not (Flag417prime-))
(not (Flag416prime-))
(not (Flag415prime-))
(not (Flag414prime-))
(not (Flag413prime-))
(not (Flag412prime-))
(not (Flag411prime-))
(not (Flag410prime-))
(not (Flag409prime-))
(not (Flag407prime-))
(not (Flag406prime-))
(not (Flag405prime-))
(not (Flag404prime-))
(not (Flag403prime-))
(not (Flag402prime-))
(not (Flag401prime-))
(not (Flag400prime-))
(not (Flag399prime-))
(not (Flag398prime-))
(not (Flag397prime-))
(not (Flag396prime-))
(not (Flag395prime-))
(not (Flag394prime-))
(not (Flag393prime-))
(not (Flag392prime-))
(not (Flag390prime-))
(not (Flag389prime-))
(not (Flag388prime-))
(not (Flag387prime-))
(not (Flag386prime-))
(not (Flag385prime-))
(not (Flag384prime-))
(not (Flag383prime-))
(not (Flag382prime-))
(not (Flag381prime-))
(not (Flag380prime-))
(not (Flag379prime-))
(not (Flag378prime-))
(not (Flag377prime-))
(not (Flag376prime-))
(not (Flag375prime-))
(not (Flag373prime-))
(not (Flag372prime-))
(not (Flag371prime-))
(not (Flag370prime-))
(not (Flag369prime-))
(not (Flag368prime-))
(not (Flag367prime-))
(not (Flag366prime-))
(not (Flag365prime-))
(not (Flag364prime-))
(not (Flag363prime-))
(not (Flag362prime-))
(not (Flag361prime-))
(not (Flag360prime-))
(not (Flag359prime-))
(not (Flag358prime-))
(not (Flag357prime-))
(not (Flag356prime-))
(not (Flag354prime-))
(not (Flag353prime-))
(not (Flag352prime-))
(not (Flag351prime-))
(not (Flag350prime-))
(not (Flag349prime-))
(not (Flag348prime-))
(not (Flag347prime-))
(not (Flag346prime-))
(not (Flag345prime-))
(not (Flag344prime-))
(not (Flag343prime-))
(not (Flag342prime-))
(not (Flag341prime-))
(not (Flag340prime-))
(not (Flag339prime-))
(not (Flag338prime-))
(not (Flag337prime-))
(not (Flag335prime-))
(not (Flag334prime-))
(not (Flag333prime-))
(not (Flag332prime-))
(not (Flag331prime-))
(not (Flag330prime-))
(not (Flag329prime-))
(not (Flag328prime-))
(not (Flag327prime-))
(not (Flag326prime-))
(not (Flag325prime-))
(not (Flag324prime-))
(not (Flag323prime-))
(not (Flag322prime-))
(not (Flag321prime-))
(not (Flag320prime-))
(not (Flag319prime-))
(not (Flag318prime-))
(not (Flag317prime-))
(not (Flag316prime-))
(not (Flag315prime-))
(not (Flag314prime-))
(not (Flag313prime-))
(not (Flag312prime-))
(not (Flag311prime-))
(not (Flag310prime-))
(not (Flag309prime-))
(not (Flag308prime-))
(not (Flag307prime-))
(not (Flag306prime-))
(not (Flag305prime-))
(not (Flag304prime-))
(not (Flag303prime-))
(not (Flag302prime-))
(not (Flag301prime-))
(not (Flag300prime-))
(not (Flag299prime-))
(not (Flag298prime-))
(not (Flag297prime-))
(not (Flag296prime-))
(not (Flag295prime-))
(not (Flag294prime-))
(not (Flag293prime-))
(not (Flag292prime-))
(not (Flag291prime-))
(not (Flag290prime-))
(not (Flag289prime-))
(not (Flag288prime-))
(not (Flag287prime-))
(not (Flag286prime-))
(not (Flag285prime-))
(not (Flag284prime-))
(not (Flag283prime-))
(not (Flag282prime-))
(not (Flag279prime-))
(not (Flag278prime-))
(not (Flag277prime-))
(not (Flag276prime-))
(not (Flag275prime-))
(not (Flag274prime-))
(not (Flag273prime-))
(not (Flag272prime-))
(not (Flag271prime-))
(not (Flag270prime-))
(not (Flag269prime-))
(not (Flag268prime-))
(not (Flag267prime-))
(not (Flag266prime-))
(not (Flag265prime-))
(not (Flag264prime-))
(not (Flag4prime-))
(not (Flag521prime-))
(not (Flag519prime-))
(not (Flag516prime-))
(not (Flag512prime-))
(not (Flag507prime-))
(not (Flag501prime-))
(not (Flag493prime-))
(not (Flag484prime-))
(not (Flag475prime-))
(not (Flag465prime-))
(not (Flag453prime-))
(not (Flag439prime-))
(not (Flag425prime-))
(not (Flag408prime-))
(not (Flag391prime-))
(not (Flag374prime-))
(not (Flag355prime-))
(not (Flag336prime-))
(not (Flag262prime-))
(not (Flag259prime-))
(not (Flag257prime-))
(not (Flag256prime-))
(not (Flag254prime-))
(not (Flag253prime-))
(not (Flag252prime-))
(not (Flag250prime-))
(not (Flag249prime-))
(not (Flag248prime-))
(not (Flag247prime-))
(not (Flag246prime-))
(not (Flag244prime-))
(not (Flag243prime-))
(not (Flag242prime-))
(not (Flag241prime-))
(not (Flag240prime-))
(not (Flag239prime-))
(not (Flag237prime-))
(not (Flag236prime-))
(not (Flag235prime-))
(not (Flag234prime-))
(not (Flag233prime-))
(not (Flag232prime-))
(not (Flag230prime-))
(not (Flag229prime-))
(not (Flag228prime-))
(not (Flag227prime-))
(not (Flag226prime-))
(not (Flag225prime-))
(not (Flag224prime-))
(not (Flag223prime-))
(not (Flag221prime-))
(not (Flag220prime-))
(not (Flag219prime-))
(not (Flag218prime-))
(not (Flag217prime-))
(not (Flag216prime-))
(not (Flag215prime-))
(not (Flag214prime-))
(not (Flag212prime-))
(not (Flag211prime-))
(not (Flag210prime-))
(not (Flag209prime-))
(not (Flag208prime-))
(not (Flag207prime-))
(not (Flag206prime-))
(not (Flag205prime-))
(not (Flag204prime-))
(not (Flag203prime-))
(not (Flag201prime-))
(not (Flag200prime-))
(not (Flag199prime-))
(not (Flag198prime-))
(not (Flag197prime-))
(not (Flag196prime-))
(not (Flag195prime-))
(not (Flag194prime-))
(not (Flag193prime-))
(not (Flag192prime-))
(not (Flag190prime-))
(not (Flag189prime-))
(not (Flag188prime-))
(not (Flag187prime-))
(not (Flag186prime-))
(not (Flag185prime-))
(not (Flag184prime-))
(not (Flag183prime-))
(not (Flag182prime-))
(not (Flag181prime-))
(not (Flag180prime-))
(not (Flag179prime-))
(not (Flag177prime-))
(not (Flag176prime-))
(not (Flag175prime-))
(not (Flag174prime-))
(not (Flag173prime-))
(not (Flag172prime-))
(not (Flag171prime-))
(not (Flag170prime-))
(not (Flag169prime-))
(not (Flag168prime-))
(not (Flag167prime-))
(not (Flag166prime-))
(not (Flag165prime-))
(not (Flag163prime-))
(not (Flag162prime-))
(not (Flag161prime-))
(not (Flag160prime-))
(not (Flag159prime-))
(not (Flag158prime-))
(not (Flag157prime-))
(not (Flag156prime-))
(not (Flag155prime-))
(not (Flag154prime-))
(not (Flag153prime-))
(not (Flag152prime-))
(not (Flag151prime-))
(not (Flag150prime-))
(not (Flag149prime-))
(not (Flag147prime-))
(not (Flag146prime-))
(not (Flag145prime-))
(not (Flag144prime-))
(not (Flag143prime-))
(not (Flag142prime-))
(not (Flag141prime-))
(not (Flag140prime-))
(not (Flag139prime-))
(not (Flag138prime-))
(not (Flag137prime-))
(not (Flag136prime-))
(not (Flag135prime-))
(not (Flag134prime-))
(not (Flag132prime-))
(not (Flag131prime-))
(not (Flag130prime-))
(not (Flag129prime-))
(not (Flag128prime-))
(not (Flag127prime-))
(not (Flag126prime-))
(not (Flag125prime-))
(not (Flag124prime-))
(not (Flag123prime-))
(not (Flag122prime-))
(not (Flag121prime-))
(not (Flag120prime-))
(not (Flag119prime-))
(not (Flag118prime-))
(not (Flag117prime-))
(not (Flag116prime-))
(not (Flag115prime-))
(not (Flag113prime-))
(not (Flag112prime-))
(not (Flag111prime-))
(not (Flag110prime-))
(not (Flag109prime-))
(not (Flag108prime-))
(not (Flag107prime-))
(not (Flag106prime-))
(not (Flag105prime-))
(not (Flag104prime-))
(not (Flag103prime-))
(not (Flag102prime-))
(not (Flag101prime-))
(not (Flag100prime-))
(not (Flag99prime-))
(not (Flag98prime-))
(not (Flag97prime-))
(not (Flag96prime-))
(not (Flag94prime-))
(not (Flag93prime-))
(not (Flag92prime-))
(not (Flag91prime-))
(not (Flag90prime-))
(not (Flag89prime-))
(not (Flag88prime-))
(not (Flag87prime-))
(not (Flag86prime-))
(not (Flag85prime-))
(not (Flag84prime-))
(not (Flag83prime-))
(not (Flag82prime-))
(not (Flag81prime-))
(not (Flag80prime-))
(not (Flag79prime-))
(not (Flag78prime-))
(not (Flag76prime-))
(not (Flag75prime-))
(not (Flag74prime-))
(not (Flag73prime-))
(not (Flag72prime-))
(not (Flag71prime-))
(not (Flag70prime-))
(not (Flag69prime-))
(not (Flag68prime-))
(not (Flag67prime-))
(not (Flag66prime-))
(not (Flag65prime-))
(not (Flag64prime-))
(not (Flag63prime-))
(not (Flag62prime-))
(not (Flag61prime-))
(not (Flag60prime-))
(not (Flag59prime-))
(not (Flag58prime-))
(not (Flag57prime-))
(not (Flag56prime-))
(not (Flag55prime-))
(not (Flag54prime-))
(not (Flag53prime-))
(not (Flag52prime-))
(not (Flag51prime-))
(not (Flag50prime-))
(not (Flag49prime-))
(not (Flag48prime-))
(not (Flag47prime-))
(not (Flag46prime-))
(not (Flag45prime-))
(not (Flag44prime-))
(not (Flag43prime-))
(not (Flag42prime-))
(not (Flag41prime-))
(not (Flag40prime-))
(not (Flag39prime-))
(not (Flag38prime-))
(not (Flag37prime-))
(not (Flag36prime-))
(not (Flag35prime-))
(not (Flag34prime-))
(not (Flag33prime-))
(not (Flag32prime-))
(not (Flag31prime-))
(not (Flag30prime-))
(not (Flag29prime-))
(not (Flag28prime-))
(not (Flag27prime-))
(not (Flag26prime-))
(not (Flag25prime-))
(not (Flag20prime-))
(not (Flag19prime-))
(not (Flag18prime-))
(not (Flag17prime-))
(not (Flag16prime-))
(not (Flag15prime-))
(not (Flag14prime-))
(not (Flag13prime-))
(not (Flag12prime-))
(not (Flag11prime-))
(not (Flag10prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag1prime-))
(not (Flag263prime-))
(not (Flag260prime-))
(not (Flag258prime-))
(not (Flag255prime-))
(not (Flag251prime-))
(not (Flag245prime-))
(not (Flag238prime-))
(not (Flag231prime-))
(not (Flag222prime-))
(not (Flag213prime-))
(not (Flag202prime-))
(not (Flag191prime-))
(not (Flag178prime-))
(not (Flag164prime-))
(not (Flag148prime-))
(not (Flag133prime-))
(not (Flag114prime-))
(not (Flag95prime-))
(not (Flag77prime-))
)
)
(:action Flag4Action
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(Flag3-)
(Flag3prime-)
)
:effect
(and
(Flag4-)
(Flag4prime-)
)

)
(:action Prim4Action-2
:parameters ()
:precondition
(and
(not (Flag3-))
(Flag3prime-)
)
:effect
(and
(Flag4prime-)
(not (Flag4-))
)

)
(:action Flag264Action-0
:parameters ()
:precondition
(and
(BELOWOF2-ROBOT)
)
:effect
(and
(Flag264-)
(Flag264prime-)
)

)
(:action Flag264Action-1
:parameters ()
:precondition
(and
(BELOWOF1-ROBOT)
)
:effect
(and
(Flag264-)
(Flag264prime-)
)

)
(:action Prim264Action
:parameters ()
:precondition
(and
(not (BELOWOF2-ROBOT))
(not (BELOWOF1-ROBOT))
)
:effect
(and
(Flag264prime-)
(not (Flag264-))
)

)
(:action Flag265Action-0
:parameters ()
:precondition
(and
(BELOWOF2-ROBOT)
)
:effect
(and
(Flag265-)
(Flag265prime-)
)

)
(:action Flag265Action-1
:parameters ()
:precondition
(and
(BELOWOF1-ROBOT)
)
:effect
(and
(Flag265-)
(Flag265prime-)
)

)
(:action Flag265Action-2
:parameters ()
:precondition
(and
(BELOWOF3-ROBOT)
)
:effect
(and
(Flag265-)
(Flag265prime-)
)

)
(:action Prim265Action
:parameters ()
:precondition
(and
(not (BELOWOF2-ROBOT))
(not (BELOWOF1-ROBOT))
(not (BELOWOF3-ROBOT))
)
:effect
(and
(Flag265prime-)
(not (Flag265-))
)

)
(:action Flag266Action-0
:parameters ()
:precondition
(and
(BELOWOF2-ROBOT)
)
:effect
(and
(Flag266-)
(Flag266prime-)
)

)
(:action Flag266Action-1
:parameters ()
:precondition
(and
(BELOWOF1-ROBOT)
)
:effect
(and
(Flag266-)
(Flag266prime-)
)

)
(:action Flag266Action-2
:parameters ()
:precondition
(and
(BELOWOF3-ROBOT)
)
:effect
(and
(Flag266-)
(Flag266prime-)
)

)
(:action Flag266Action-3
:parameters ()
:precondition
(and
(BELOWOF4-ROBOT)
)
:effect
(and
(Flag266-)
(Flag266prime-)
)

)
(:action Prim266Action
:parameters ()
:precondition
(and
(not (BELOWOF2-ROBOT))
(not (BELOWOF1-ROBOT))
(not (BELOWOF3-ROBOT))
(not (BELOWOF4-ROBOT))
)
:effect
(and
(Flag266prime-)
(not (Flag266-))
)

)
(:action Flag267Action-0
:parameters ()
:precondition
(and
(BELOWOF2-ROBOT)
)
:effect
(and
(Flag267-)
(Flag267prime-)
)

)
(:action Flag267Action-1
:parameters ()
:precondition
(and
(BELOWOF1-ROBOT)
)
:effect
(and
(Flag267-)
(Flag267prime-)
)

)
(:action Flag267Action-2
:parameters ()
:precondition
(and
(BELOWOF3-ROBOT)
)
:effect
(and
(Flag267-)
(Flag267prime-)
)

)
(:action Flag267Action-3
:parameters ()
:precondition
(and
(BELOWOF4-ROBOT)
)
:effect
(and
(Flag267-)
(Flag267prime-)
)

)
(:action Flag267Action-4
:parameters ()
:precondition
(and
(BELOWOF5-ROBOT)
)
:effect
(and
(Flag267-)
(Flag267prime-)
)

)
(:action Prim267Action
:parameters ()
:precondition
(and
(not (BELOWOF2-ROBOT))
(not (BELOWOF1-ROBOT))
(not (BELOWOF3-ROBOT))
(not (BELOWOF4-ROBOT))
(not (BELOWOF5-ROBOT))
)
:effect
(and
(Flag267prime-)
(not (Flag267-))
)

)
(:action Flag268Action-0
:parameters ()
:precondition
(and
(BELOWOF6-ROBOT)
)
:effect
(and
(Flag268-)
(Flag268prime-)
)

)
(:action Flag268Action-1
:parameters ()
:precondition
(and
(BELOWOF5-ROBOT)
)
:effect
(and
(Flag268-)
(Flag268prime-)
)

)
(:action Flag268Action-2
:parameters ()
:precondition
(and
(BELOWOF2-ROBOT)
)
:effect
(and
(Flag268-)
(Flag268prime-)
)

)
(:action Flag268Action-3
:parameters ()
:precondition
(and
(BELOWOF1-ROBOT)
)
:effect
(and
(Flag268-)
(Flag268prime-)
)

)
(:action Flag268Action-4
:parameters ()
:precondition
(and
(BELOWOF4-ROBOT)
)
:effect
(and
(Flag268-)
(Flag268prime-)
)

)
(:action Flag268Action-5
:parameters ()
:precondition
(and
(BELOWOF3-ROBOT)
)
:effect
(and
(Flag268-)
(Flag268prime-)
)

)
(:action Prim268Action
:parameters ()
:precondition
(and
(not (BELOWOF6-ROBOT))
(not (BELOWOF5-ROBOT))
(not (BELOWOF2-ROBOT))
(not (BELOWOF1-ROBOT))
(not (BELOWOF4-ROBOT))
(not (BELOWOF3-ROBOT))
)
:effect
(and
(Flag268prime-)
(not (Flag268-))
)

)
(:action Flag269Action-0
:parameters ()
:precondition
(and
(BELOWOF6-ROBOT)
)
:effect
(and
(Flag269-)
(Flag269prime-)
)

)
(:action Flag269Action-1
:parameters ()
:precondition
(and
(BELOWOF5-ROBOT)
)
:effect
(and
(Flag269-)
(Flag269prime-)
)

)
(:action Flag269Action-2
:parameters ()
:precondition
(and
(BELOWOF2-ROBOT)
)
:effect
(and
(Flag269-)
(Flag269prime-)
)

)
(:action Flag269Action-3
:parameters ()
:precondition
(and
(BELOWOF1-ROBOT)
)
:effect
(and
(Flag269-)
(Flag269prime-)
)

)
(:action Flag269Action-4
:parameters ()
:precondition
(and
(BELOWOF4-ROBOT)
)
:effect
(and
(Flag269-)
(Flag269prime-)
)

)
(:action Flag269Action-5
:parameters ()
:precondition
(and
(BELOWOF7-ROBOT)
)
:effect
(and
(Flag269-)
(Flag269prime-)
)

)
(:action Flag269Action-6
:parameters ()
:precondition
(and
(BELOWOF3-ROBOT)
)
:effect
(and
(Flag269-)
(Flag269prime-)
)

)
(:action Prim269Action
:parameters ()
:precondition
(and
(not (BELOWOF6-ROBOT))
(not (BELOWOF5-ROBOT))
(not (BELOWOF2-ROBOT))
(not (BELOWOF1-ROBOT))
(not (BELOWOF4-ROBOT))
(not (BELOWOF7-ROBOT))
(not (BELOWOF3-ROBOT))
)
:effect
(and
(Flag269prime-)
(not (Flag269-))
)

)
(:action Flag270Action-0
:parameters ()
:precondition
(and
(BELOWOF6-ROBOT)
)
:effect
(and
(Flag270-)
(Flag270prime-)
)

)
(:action Flag270Action-1
:parameters ()
:precondition
(and
(BELOWOF5-ROBOT)
)
:effect
(and
(Flag270-)
(Flag270prime-)
)

)
(:action Flag270Action-2
:parameters ()
:precondition
(and
(BELOWOF2-ROBOT)
)
:effect
(and
(Flag270-)
(Flag270prime-)
)

)
(:action Flag270Action-3
:parameters ()
:precondition
(and
(BELOWOF1-ROBOT)
)
:effect
(and
(Flag270-)
(Flag270prime-)
)

)
(:action Flag270Action-4
:parameters ()
:precondition
(and
(BELOWOF4-ROBOT)
)
:effect
(and
(Flag270-)
(Flag270prime-)
)

)
(:action Flag270Action-5
:parameters ()
:precondition
(and
(BELOWOF7-ROBOT)
)
:effect
(and
(Flag270-)
(Flag270prime-)
)

)
(:action Flag270Action-6
:parameters ()
:precondition
(and
(BELOWOF3-ROBOT)
)
:effect
(and
(Flag270-)
(Flag270prime-)
)

)
(:action Flag270Action-7
:parameters ()
:precondition
(and
(BELOWOF8-ROBOT)
)
:effect
(and
(Flag270-)
(Flag270prime-)
)

)
(:action Prim270Action
:parameters ()
:precondition
(and
(not (BELOWOF6-ROBOT))
(not (BELOWOF5-ROBOT))
(not (BELOWOF2-ROBOT))
(not (BELOWOF1-ROBOT))
(not (BELOWOF4-ROBOT))
(not (BELOWOF7-ROBOT))
(not (BELOWOF3-ROBOT))
(not (BELOWOF8-ROBOT))
)
:effect
(and
(Flag270prime-)
(not (Flag270-))
)

)
(:action Flag271Action-0
:parameters ()
:precondition
(and
(BELOWOF6-ROBOT)
)
:effect
(and
(Flag271-)
(Flag271prime-)
)

)
(:action Flag271Action-1
:parameters ()
:precondition
(and
(BELOWOF5-ROBOT)
)
:effect
(and
(Flag271-)
(Flag271prime-)
)

)
(:action Flag271Action-2
:parameters ()
:precondition
(and
(BELOWOF2-ROBOT)
)
:effect
(and
(Flag271-)
(Flag271prime-)
)

)
(:action Flag271Action-3
:parameters ()
:precondition
(and
(BELOWOF1-ROBOT)
)
:effect
(and
(Flag271-)
(Flag271prime-)
)

)
(:action Flag271Action-4
:parameters ()
:precondition
(and
(BELOWOF4-ROBOT)
)
:effect
(and
(Flag271-)
(Flag271prime-)
)

)
(:action Flag271Action-5
:parameters ()
:precondition
(and
(BELOWOF7-ROBOT)
)
:effect
(and
(Flag271-)
(Flag271prime-)
)

)
(:action Flag271Action-6
:parameters ()
:precondition
(and
(BELOWOF9-ROBOT)
)
:effect
(and
(Flag271-)
(Flag271prime-)
)

)
(:action Flag271Action-7
:parameters ()
:precondition
(and
(BELOWOF3-ROBOT)
)
:effect
(and
(Flag271-)
(Flag271prime-)
)

)
(:action Flag271Action-8
:parameters ()
:precondition
(and
(BELOWOF8-ROBOT)
)
:effect
(and
(Flag271-)
(Flag271prime-)
)

)
(:action Prim271Action
:parameters ()
:precondition
(and
(not (BELOWOF6-ROBOT))
(not (BELOWOF5-ROBOT))
(not (BELOWOF2-ROBOT))
(not (BELOWOF1-ROBOT))
(not (BELOWOF4-ROBOT))
(not (BELOWOF7-ROBOT))
(not (BELOWOF9-ROBOT))
(not (BELOWOF3-ROBOT))
(not (BELOWOF8-ROBOT))
)
:effect
(and
(Flag271prime-)
(not (Flag271-))
)

)
(:action Flag272Action-0
:parameters ()
:precondition
(and
(BELOWOF10-ROBOT)
)
:effect
(and
(Flag272-)
(Flag272prime-)
)

)
(:action Flag272Action-1
:parameters ()
:precondition
(and
(BELOWOF6-ROBOT)
)
:effect
(and
(Flag272-)
(Flag272prime-)
)

)
(:action Flag272Action-2
:parameters ()
:precondition
(and
(BELOWOF5-ROBOT)
)
:effect
(and
(Flag272-)
(Flag272prime-)
)

)
(:action Flag272Action-3
:parameters ()
:precondition
(and
(BELOWOF2-ROBOT)
)
:effect
(and
(Flag272-)
(Flag272prime-)
)

)
(:action Flag272Action-4
:parameters ()
:precondition
(and
(BELOWOF1-ROBOT)
)
:effect
(and
(Flag272-)
(Flag272prime-)
)

)
(:action Flag272Action-5
:parameters ()
:precondition
(and
(BELOWOF4-ROBOT)
)
:effect
(and
(Flag272-)
(Flag272prime-)
)

)
(:action Flag272Action-6
:parameters ()
:precondition
(and
(BELOWOF7-ROBOT)
)
:effect
(and
(Flag272-)
(Flag272prime-)
)

)
(:action Flag272Action-7
:parameters ()
:precondition
(and
(BELOWOF9-ROBOT)
)
:effect
(and
(Flag272-)
(Flag272prime-)
)

)
(:action Flag272Action-8
:parameters ()
:precondition
(and
(BELOWOF3-ROBOT)
)
:effect
(and
(Flag272-)
(Flag272prime-)
)

)
(:action Flag272Action-9
:parameters ()
:precondition
(and
(BELOWOF8-ROBOT)
)
:effect
(and
(Flag272-)
(Flag272prime-)
)

)
(:action Prim272Action
:parameters ()
:precondition
(and
(not (BELOWOF10-ROBOT))
(not (BELOWOF6-ROBOT))
(not (BELOWOF5-ROBOT))
(not (BELOWOF2-ROBOT))
(not (BELOWOF1-ROBOT))
(not (BELOWOF4-ROBOT))
(not (BELOWOF7-ROBOT))
(not (BELOWOF9-ROBOT))
(not (BELOWOF3-ROBOT))
(not (BELOWOF8-ROBOT))
)
:effect
(and
(Flag272prime-)
(not (Flag272-))
)

)
(:action Flag273Action-0
:parameters ()
:precondition
(and
(BELOWOF10-ROBOT)
)
:effect
(and
(Flag273-)
(Flag273prime-)
)

)
(:action Flag273Action-1
:parameters ()
:precondition
(and
(BELOWOF11-ROBOT)
)
:effect
(and
(Flag273-)
(Flag273prime-)
)

)
(:action Flag273Action-2
:parameters ()
:precondition
(and
(BELOWOF6-ROBOT)
)
:effect
(and
(Flag273-)
(Flag273prime-)
)

)
(:action Flag273Action-3
:parameters ()
:precondition
(and
(BELOWOF5-ROBOT)
)
:effect
(and
(Flag273-)
(Flag273prime-)
)

)
(:action Flag273Action-4
:parameters ()
:precondition
(and
(BELOWOF2-ROBOT)
)
:effect
(and
(Flag273-)
(Flag273prime-)
)

)
(:action Flag273Action-5
:parameters ()
:precondition
(and
(BELOWOF1-ROBOT)
)
:effect
(and
(Flag273-)
(Flag273prime-)
)

)
(:action Flag273Action-6
:parameters ()
:precondition
(and
(BELOWOF4-ROBOT)
)
:effect
(and
(Flag273-)
(Flag273prime-)
)

)
(:action Flag273Action-7
:parameters ()
:precondition
(and
(BELOWOF7-ROBOT)
)
:effect
(and
(Flag273-)
(Flag273prime-)
)

)
(:action Flag273Action-8
:parameters ()
:precondition
(and
(BELOWOF9-ROBOT)
)
:effect
(and
(Flag273-)
(Flag273prime-)
)

)
(:action Flag273Action-9
:parameters ()
:precondition
(and
(BELOWOF3-ROBOT)
)
:effect
(and
(Flag273-)
(Flag273prime-)
)

)
(:action Flag273Action-10
:parameters ()
:precondition
(and
(BELOWOF8-ROBOT)
)
:effect
(and
(Flag273-)
(Flag273prime-)
)

)
(:action Prim273Action
:parameters ()
:precondition
(and
(not (BELOWOF10-ROBOT))
(not (BELOWOF11-ROBOT))
(not (BELOWOF6-ROBOT))
(not (BELOWOF5-ROBOT))
(not (BELOWOF2-ROBOT))
(not (BELOWOF1-ROBOT))
(not (BELOWOF4-ROBOT))
(not (BELOWOF7-ROBOT))
(not (BELOWOF9-ROBOT))
(not (BELOWOF3-ROBOT))
(not (BELOWOF8-ROBOT))
)
:effect
(and
(Flag273prime-)
(not (Flag273-))
)

)
(:action Flag274Action-0
:parameters ()
:precondition
(and
(BELOWOF10-ROBOT)
)
:effect
(and
(Flag274-)
(Flag274prime-)
)

)
(:action Flag274Action-1
:parameters ()
:precondition
(and
(BELOWOF11-ROBOT)
)
:effect
(and
(Flag274-)
(Flag274prime-)
)

)
(:action Flag274Action-2
:parameters ()
:precondition
(and
(BELOWOF6-ROBOT)
)
:effect
(and
(Flag274-)
(Flag274prime-)
)

)
(:action Flag274Action-3
:parameters ()
:precondition
(and
(BELOWOF5-ROBOT)
)
:effect
(and
(Flag274-)
(Flag274prime-)
)

)
(:action Flag274Action-4
:parameters ()
:precondition
(and
(BELOWOF2-ROBOT)
)
:effect
(and
(Flag274-)
(Flag274prime-)
)

)
(:action Flag274Action-5
:parameters ()
:precondition
(and
(BELOWOF1-ROBOT)
)
:effect
(and
(Flag274-)
(Flag274prime-)
)

)
(:action Flag274Action-6
:parameters ()
:precondition
(and
(BELOWOF4-ROBOT)
)
:effect
(and
(Flag274-)
(Flag274prime-)
)

)
(:action Flag274Action-7
:parameters ()
:precondition
(and
(BELOWOF12-ROBOT)
)
:effect
(and
(Flag274-)
(Flag274prime-)
)

)
(:action Flag274Action-8
:parameters ()
:precondition
(and
(BELOWOF7-ROBOT)
)
:effect
(and
(Flag274-)
(Flag274prime-)
)

)
(:action Flag274Action-9
:parameters ()
:precondition
(and
(BELOWOF9-ROBOT)
)
:effect
(and
(Flag274-)
(Flag274prime-)
)

)
(:action Flag274Action-10
:parameters ()
:precondition
(and
(BELOWOF3-ROBOT)
)
:effect
(and
(Flag274-)
(Flag274prime-)
)

)
(:action Flag274Action-11
:parameters ()
:precondition
(and
(BELOWOF8-ROBOT)
)
:effect
(and
(Flag274-)
(Flag274prime-)
)

)
(:action Prim274Action
:parameters ()
:precondition
(and
(not (BELOWOF10-ROBOT))
(not (BELOWOF11-ROBOT))
(not (BELOWOF6-ROBOT))
(not (BELOWOF5-ROBOT))
(not (BELOWOF2-ROBOT))
(not (BELOWOF1-ROBOT))
(not (BELOWOF4-ROBOT))
(not (BELOWOF12-ROBOT))
(not (BELOWOF7-ROBOT))
(not (BELOWOF9-ROBOT))
(not (BELOWOF3-ROBOT))
(not (BELOWOF8-ROBOT))
)
:effect
(and
(Flag274prime-)
(not (Flag274-))
)

)
(:action Flag275Action-0
:parameters ()
:precondition
(and
(BELOWOF10-ROBOT)
)
:effect
(and
(Flag275-)
(Flag275prime-)
)

)
(:action Flag275Action-1
:parameters ()
:precondition
(and
(BELOWOF11-ROBOT)
)
:effect
(and
(Flag275-)
(Flag275prime-)
)

)
(:action Flag275Action-2
:parameters ()
:precondition
(and
(BELOWOF6-ROBOT)
)
:effect
(and
(Flag275-)
(Flag275prime-)
)

)
(:action Flag275Action-3
:parameters ()
:precondition
(and
(BELOWOF5-ROBOT)
)
:effect
(and
(Flag275-)
(Flag275prime-)
)

)
(:action Flag275Action-4
:parameters ()
:precondition
(and
(BELOWOF2-ROBOT)
)
:effect
(and
(Flag275-)
(Flag275prime-)
)

)
(:action Flag275Action-5
:parameters ()
:precondition
(and
(BELOWOF1-ROBOT)
)
:effect
(and
(Flag275-)
(Flag275prime-)
)

)
(:action Flag275Action-6
:parameters ()
:precondition
(and
(BELOWOF4-ROBOT)
)
:effect
(and
(Flag275-)
(Flag275prime-)
)

)
(:action Flag275Action-7
:parameters ()
:precondition
(and
(BELOWOF12-ROBOT)
)
:effect
(and
(Flag275-)
(Flag275prime-)
)

)
(:action Flag275Action-8
:parameters ()
:precondition
(and
(BELOWOF7-ROBOT)
)
:effect
(and
(Flag275-)
(Flag275prime-)
)

)
(:action Flag275Action-9
:parameters ()
:precondition
(and
(BELOWOF9-ROBOT)
)
:effect
(and
(Flag275-)
(Flag275prime-)
)

)
(:action Flag275Action-10
:parameters ()
:precondition
(and
(BELOWOF3-ROBOT)
)
:effect
(and
(Flag275-)
(Flag275prime-)
)

)
(:action Flag275Action-11
:parameters ()
:precondition
(and
(BELOWOF13-ROBOT)
)
:effect
(and
(Flag275-)
(Flag275prime-)
)

)
(:action Flag275Action-12
:parameters ()
:precondition
(and
(BELOWOF8-ROBOT)
)
:effect
(and
(Flag275-)
(Flag275prime-)
)

)
(:action Prim275Action
:parameters ()
:precondition
(and
(not (BELOWOF10-ROBOT))
(not (BELOWOF11-ROBOT))
(not (BELOWOF6-ROBOT))
(not (BELOWOF5-ROBOT))
(not (BELOWOF2-ROBOT))
(not (BELOWOF1-ROBOT))
(not (BELOWOF4-ROBOT))
(not (BELOWOF12-ROBOT))
(not (BELOWOF7-ROBOT))
(not (BELOWOF9-ROBOT))
(not (BELOWOF3-ROBOT))
(not (BELOWOF13-ROBOT))
(not (BELOWOF8-ROBOT))
)
:effect
(and
(Flag275prime-)
(not (Flag275-))
)

)
(:action Flag276Action-0
:parameters ()
:precondition
(and
(BELOWOF10-ROBOT)
)
:effect
(and
(Flag276-)
(Flag276prime-)
)

)
(:action Flag276Action-1
:parameters ()
:precondition
(and
(BELOWOF11-ROBOT)
)
:effect
(and
(Flag276-)
(Flag276prime-)
)

)
(:action Flag276Action-2
:parameters ()
:precondition
(and
(BELOWOF6-ROBOT)
)
:effect
(and
(Flag276-)
(Flag276prime-)
)

)
(:action Flag276Action-3
:parameters ()
:precondition
(and
(BELOWOF5-ROBOT)
)
:effect
(and
(Flag276-)
(Flag276prime-)
)

)
(:action Flag276Action-4
:parameters ()
:precondition
(and
(BELOWOF2-ROBOT)
)
:effect
(and
(Flag276-)
(Flag276prime-)
)

)
(:action Flag276Action-5
:parameters ()
:precondition
(and
(BELOWOF1-ROBOT)
)
:effect
(and
(Flag276-)
(Flag276prime-)
)

)
(:action Flag276Action-6
:parameters ()
:precondition
(and
(BELOWOF4-ROBOT)
)
:effect
(and
(Flag276-)
(Flag276prime-)
)

)
(:action Flag276Action-7
:parameters ()
:precondition
(and
(BELOWOF12-ROBOT)
)
:effect
(and
(Flag276-)
(Flag276prime-)
)

)
(:action Flag276Action-8
:parameters ()
:precondition
(and
(BELOWOF7-ROBOT)
)
:effect
(and
(Flag276-)
(Flag276prime-)
)

)
(:action Flag276Action-9
:parameters ()
:precondition
(and
(BELOWOF9-ROBOT)
)
:effect
(and
(Flag276-)
(Flag276prime-)
)

)
(:action Flag276Action-10
:parameters ()
:precondition
(and
(BELOWOF3-ROBOT)
)
:effect
(and
(Flag276-)
(Flag276prime-)
)

)
(:action Flag276Action-11
:parameters ()
:precondition
(and
(BELOWOF13-ROBOT)
)
:effect
(and
(Flag276-)
(Flag276prime-)
)

)
(:action Flag276Action-12
:parameters ()
:precondition
(and
(BELOWOF8-ROBOT)
)
:effect
(and
(Flag276-)
(Flag276prime-)
)

)
(:action Flag276Action-13
:parameters ()
:precondition
(and
(BELOWOF14-ROBOT)
)
:effect
(and
(Flag276-)
(Flag276prime-)
)

)
(:action Prim276Action
:parameters ()
:precondition
(and
(not (BELOWOF10-ROBOT))
(not (BELOWOF11-ROBOT))
(not (BELOWOF6-ROBOT))
(not (BELOWOF5-ROBOT))
(not (BELOWOF2-ROBOT))
(not (BELOWOF1-ROBOT))
(not (BELOWOF4-ROBOT))
(not (BELOWOF12-ROBOT))
(not (BELOWOF7-ROBOT))
(not (BELOWOF9-ROBOT))
(not (BELOWOF3-ROBOT))
(not (BELOWOF13-ROBOT))
(not (BELOWOF8-ROBOT))
(not (BELOWOF14-ROBOT))
)
:effect
(and
(Flag276prime-)
(not (Flag276-))
)

)
(:action Flag277Action-0
:parameters ()
:precondition
(and
(BELOWOF10-ROBOT)
)
:effect
(and
(Flag277-)
(Flag277prime-)
)

)
(:action Flag277Action-1
:parameters ()
:precondition
(and
(BELOWOF11-ROBOT)
)
:effect
(and
(Flag277-)
(Flag277prime-)
)

)
(:action Flag277Action-2
:parameters ()
:precondition
(and
(BELOWOF15-ROBOT)
)
:effect
(and
(Flag277-)
(Flag277prime-)
)

)
(:action Flag277Action-3
:parameters ()
:precondition
(and
(BELOWOF6-ROBOT)
)
:effect
(and
(Flag277-)
(Flag277prime-)
)

)
(:action Flag277Action-4
:parameters ()
:precondition
(and
(BELOWOF5-ROBOT)
)
:effect
(and
(Flag277-)
(Flag277prime-)
)

)
(:action Flag277Action-5
:parameters ()
:precondition
(and
(BELOWOF2-ROBOT)
)
:effect
(and
(Flag277-)
(Flag277prime-)
)

)
(:action Flag277Action-6
:parameters ()
:precondition
(and
(BELOWOF1-ROBOT)
)
:effect
(and
(Flag277-)
(Flag277prime-)
)

)
(:action Flag277Action-7
:parameters ()
:precondition
(and
(BELOWOF4-ROBOT)
)
:effect
(and
(Flag277-)
(Flag277prime-)
)

)
(:action Flag277Action-8
:parameters ()
:precondition
(and
(BELOWOF12-ROBOT)
)
:effect
(and
(Flag277-)
(Flag277prime-)
)

)
(:action Flag277Action-9
:parameters ()
:precondition
(and
(BELOWOF7-ROBOT)
)
:effect
(and
(Flag277-)
(Flag277prime-)
)

)
(:action Flag277Action-10
:parameters ()
:precondition
(and
(BELOWOF9-ROBOT)
)
:effect
(and
(Flag277-)
(Flag277prime-)
)

)
(:action Flag277Action-11
:parameters ()
:precondition
(and
(BELOWOF3-ROBOT)
)
:effect
(and
(Flag277-)
(Flag277prime-)
)

)
(:action Flag277Action-12
:parameters ()
:precondition
(and
(BELOWOF13-ROBOT)
)
:effect
(and
(Flag277-)
(Flag277prime-)
)

)
(:action Flag277Action-13
:parameters ()
:precondition
(and
(BELOWOF8-ROBOT)
)
:effect
(and
(Flag277-)
(Flag277prime-)
)

)
(:action Flag277Action-14
:parameters ()
:precondition
(and
(BELOWOF14-ROBOT)
)
:effect
(and
(Flag277-)
(Flag277prime-)
)

)
(:action Prim277Action
:parameters ()
:precondition
(and
(not (BELOWOF10-ROBOT))
(not (BELOWOF11-ROBOT))
(not (BELOWOF15-ROBOT))
(not (BELOWOF6-ROBOT))
(not (BELOWOF5-ROBOT))
(not (BELOWOF2-ROBOT))
(not (BELOWOF1-ROBOT))
(not (BELOWOF4-ROBOT))
(not (BELOWOF12-ROBOT))
(not (BELOWOF7-ROBOT))
(not (BELOWOF9-ROBOT))
(not (BELOWOF3-ROBOT))
(not (BELOWOF13-ROBOT))
(not (BELOWOF8-ROBOT))
(not (BELOWOF14-ROBOT))
)
:effect
(and
(Flag277prime-)
(not (Flag277-))
)

)
(:action Flag278Action-0
:parameters ()
:precondition
(and
(BELOWOF10-ROBOT)
)
:effect
(and
(Flag278-)
(Flag278prime-)
)

)
(:action Flag278Action-1
:parameters ()
:precondition
(and
(BELOWOF16-ROBOT)
)
:effect
(and
(Flag278-)
(Flag278prime-)
)

)
(:action Flag278Action-2
:parameters ()
:precondition
(and
(BELOWOF15-ROBOT)
)
:effect
(and
(Flag278-)
(Flag278prime-)
)

)
(:action Flag278Action-3
:parameters ()
:precondition
(and
(BELOWOF6-ROBOT)
)
:effect
(and
(Flag278-)
(Flag278prime-)
)

)
(:action Flag278Action-4
:parameters ()
:precondition
(and
(BELOWOF11-ROBOT)
)
:effect
(and
(Flag278-)
(Flag278prime-)
)

)
(:action Flag278Action-5
:parameters ()
:precondition
(and
(BELOWOF5-ROBOT)
)
:effect
(and
(Flag278-)
(Flag278prime-)
)

)
(:action Flag278Action-6
:parameters ()
:precondition
(and
(BELOWOF2-ROBOT)
)
:effect
(and
(Flag278-)
(Flag278prime-)
)

)
(:action Flag278Action-7
:parameters ()
:precondition
(and
(BELOWOF1-ROBOT)
)
:effect
(and
(Flag278-)
(Flag278prime-)
)

)
(:action Flag278Action-8
:parameters ()
:precondition
(and
(BELOWOF4-ROBOT)
)
:effect
(and
(Flag278-)
(Flag278prime-)
)

)
(:action Flag278Action-9
:parameters ()
:precondition
(and
(BELOWOF12-ROBOT)
)
:effect
(and
(Flag278-)
(Flag278prime-)
)

)
(:action Flag278Action-10
:parameters ()
:precondition
(and
(BELOWOF7-ROBOT)
)
:effect
(and
(Flag278-)
(Flag278prime-)
)

)
(:action Flag278Action-11
:parameters ()
:precondition
(and
(BELOWOF9-ROBOT)
)
:effect
(and
(Flag278-)
(Flag278prime-)
)

)
(:action Flag278Action-12
:parameters ()
:precondition
(and
(BELOWOF3-ROBOT)
)
:effect
(and
(Flag278-)
(Flag278prime-)
)

)
(:action Flag278Action-13
:parameters ()
:precondition
(and
(BELOWOF13-ROBOT)
)
:effect
(and
(Flag278-)
(Flag278prime-)
)

)
(:action Flag278Action-14
:parameters ()
:precondition
(and
(BELOWOF8-ROBOT)
)
:effect
(and
(Flag278-)
(Flag278prime-)
)

)
(:action Flag278Action-15
:parameters ()
:precondition
(and
(BELOWOF14-ROBOT)
)
:effect
(and
(Flag278-)
(Flag278prime-)
)

)
(:action Prim278Action
:parameters ()
:precondition
(and
(not (BELOWOF10-ROBOT))
(not (BELOWOF16-ROBOT))
(not (BELOWOF15-ROBOT))
(not (BELOWOF6-ROBOT))
(not (BELOWOF11-ROBOT))
(not (BELOWOF5-ROBOT))
(not (BELOWOF2-ROBOT))
(not (BELOWOF1-ROBOT))
(not (BELOWOF4-ROBOT))
(not (BELOWOF12-ROBOT))
(not (BELOWOF7-ROBOT))
(not (BELOWOF9-ROBOT))
(not (BELOWOF3-ROBOT))
(not (BELOWOF13-ROBOT))
(not (BELOWOF8-ROBOT))
(not (BELOWOF14-ROBOT))
)
:effect
(and
(Flag278prime-)
(not (Flag278-))
)

)
(:action Flag279Action-0
:parameters ()
:precondition
(and
(BELOWOF10-ROBOT)
)
:effect
(and
(Flag279-)
(Flag279prime-)
)

)
(:action Flag279Action-1
:parameters ()
:precondition
(and
(BELOWOF16-ROBOT)
)
:effect
(and
(Flag279-)
(Flag279prime-)
)

)
(:action Flag279Action-2
:parameters ()
:precondition
(and
(BELOWOF15-ROBOT)
)
:effect
(and
(Flag279-)
(Flag279prime-)
)

)
(:action Flag279Action-3
:parameters ()
:precondition
(and
(BELOWOF6-ROBOT)
)
:effect
(and
(Flag279-)
(Flag279prime-)
)

)
(:action Flag279Action-4
:parameters ()
:precondition
(and
(BELOWOF17-ROBOT)
)
:effect
(and
(Flag279-)
(Flag279prime-)
)

)
(:action Flag279Action-5
:parameters ()
:precondition
(and
(BELOWOF5-ROBOT)
)
:effect
(and
(Flag279-)
(Flag279prime-)
)

)
(:action Flag279Action-6
:parameters ()
:precondition
(and
(BELOWOF2-ROBOT)
)
:effect
(and
(Flag279-)
(Flag279prime-)
)

)
(:action Flag279Action-7
:parameters ()
:precondition
(and
(BELOWOF1-ROBOT)
)
:effect
(and
(Flag279-)
(Flag279prime-)
)

)
(:action Flag279Action-8
:parameters ()
:precondition
(and
(BELOWOF4-ROBOT)
)
:effect
(and
(Flag279-)
(Flag279prime-)
)

)
(:action Flag279Action-9
:parameters ()
:precondition
(and
(BELOWOF11-ROBOT)
)
:effect
(and
(Flag279-)
(Flag279prime-)
)

)
(:action Flag279Action-10
:parameters ()
:precondition
(and
(BELOWOF12-ROBOT)
)
:effect
(and
(Flag279-)
(Flag279prime-)
)

)
(:action Flag279Action-11
:parameters ()
:precondition
(and
(BELOWOF7-ROBOT)
)
:effect
(and
(Flag279-)
(Flag279prime-)
)

)
(:action Flag279Action-12
:parameters ()
:precondition
(and
(BELOWOF9-ROBOT)
)
:effect
(and
(Flag279-)
(Flag279prime-)
)

)
(:action Flag279Action-13
:parameters ()
:precondition
(and
(BELOWOF3-ROBOT)
)
:effect
(and
(Flag279-)
(Flag279prime-)
)

)
(:action Flag279Action-14
:parameters ()
:precondition
(and
(BELOWOF13-ROBOT)
)
:effect
(and
(Flag279-)
(Flag279prime-)
)

)
(:action Flag279Action-15
:parameters ()
:precondition
(and
(BELOWOF8-ROBOT)
)
:effect
(and
(Flag279-)
(Flag279prime-)
)

)
(:action Flag279Action-16
:parameters ()
:precondition
(and
(BELOWOF14-ROBOT)
)
:effect
(and
(Flag279-)
(Flag279prime-)
)

)
(:action Prim279Action
:parameters ()
:precondition
(and
(not (BELOWOF10-ROBOT))
(not (BELOWOF16-ROBOT))
(not (BELOWOF15-ROBOT))
(not (BELOWOF6-ROBOT))
(not (BELOWOF17-ROBOT))
(not (BELOWOF5-ROBOT))
(not (BELOWOF2-ROBOT))
(not (BELOWOF1-ROBOT))
(not (BELOWOF4-ROBOT))
(not (BELOWOF11-ROBOT))
(not (BELOWOF12-ROBOT))
(not (BELOWOF7-ROBOT))
(not (BELOWOF9-ROBOT))
(not (BELOWOF3-ROBOT))
(not (BELOWOF13-ROBOT))
(not (BELOWOF8-ROBOT))
(not (BELOWOF14-ROBOT))
)
:effect
(and
(Flag279prime-)
(not (Flag279-))
)

)
(:action Flag280Action-0
:parameters ()
:precondition
(and
(BELOWOF10-ROBOT)
)
:effect
(and
(Flag280-)
(Flag280prime-)
)

)
(:action Flag280Action-1
:parameters ()
:precondition
(and
(BELOWOF16-ROBOT)
)
:effect
(and
(Flag280-)
(Flag280prime-)
)

)
(:action Flag280Action-2
:parameters ()
:precondition
(and
(BELOWOF15-ROBOT)
)
:effect
(and
(Flag280-)
(Flag280prime-)
)

)
(:action Flag280Action-3
:parameters ()
:precondition
(and
(BELOWOF6-ROBOT)
)
:effect
(and
(Flag280-)
(Flag280prime-)
)

)
(:action Flag280Action-4
:parameters ()
:precondition
(and
(BELOWOF17-ROBOT)
)
:effect
(and
(Flag280-)
(Flag280prime-)
)

)
(:action Flag280Action-5
:parameters ()
:precondition
(and
(BELOWOF5-ROBOT)
)
:effect
(and
(Flag280-)
(Flag280prime-)
)

)
(:action Flag280Action-6
:parameters ()
:precondition
(and
(BELOWOF2-ROBOT)
)
:effect
(and
(Flag280-)
(Flag280prime-)
)

)
(:action Flag280Action-7
:parameters ()
:precondition
(and
(BELOWOF1-ROBOT)
)
:effect
(and
(Flag280-)
(Flag280prime-)
)

)
(:action Flag280Action-8
:parameters ()
:precondition
(and
(BELOWOF4-ROBOT)
)
:effect
(and
(Flag280-)
(Flag280prime-)
)

)
(:action Flag280Action-9
:parameters ()
:precondition
(and
(BELOWOF11-ROBOT)
)
:effect
(and
(Flag280-)
(Flag280prime-)
)

)
(:action Flag280Action-11
:parameters ()
:precondition
(and
(BELOWOF12-ROBOT)
)
:effect
(and
(Flag280-)
(Flag280prime-)
)

)
(:action Flag280Action-12
:parameters ()
:precondition
(and
(BELOWOF7-ROBOT)
)
:effect
(and
(Flag280-)
(Flag280prime-)
)

)
(:action Flag280Action-13
:parameters ()
:precondition
(and
(BELOWOF9-ROBOT)
)
:effect
(and
(Flag280-)
(Flag280prime-)
)

)
(:action Flag280Action-14
:parameters ()
:precondition
(and
(BELOWOF3-ROBOT)
)
:effect
(and
(Flag280-)
(Flag280prime-)
)

)
(:action Flag280Action-15
:parameters ()
:precondition
(and
(BELOWOF13-ROBOT)
)
:effect
(and
(Flag280-)
(Flag280prime-)
)

)
(:action Flag280Action-16
:parameters ()
:precondition
(and
(BELOWOF8-ROBOT)
)
:effect
(and
(Flag280-)
(Flag280prime-)
)

)
(:action Flag280Action-17
:parameters ()
:precondition
(and
(BELOWOF14-ROBOT)
)
:effect
(and
(Flag280-)
(Flag280prime-)
)

)
(:action Prim280Action
:parameters ()
:precondition
(and
(not (BELOWOF10-ROBOT))
(not (BELOWOF16-ROBOT))
(not (BELOWOF15-ROBOT))
(not (BELOWOF6-ROBOT))
(not (BELOWOF17-ROBOT))
(not (BELOWOF5-ROBOT))
(not (BELOWOF2-ROBOT))
(not (BELOWOF1-ROBOT))
(not (BELOWOF4-ROBOT))
(not (BELOWOF11-ROBOT))
(not (BELOWOF18-ROBOT))
(not (BELOWOF12-ROBOT))
(not (BELOWOF7-ROBOT))
(not (BELOWOF9-ROBOT))
(not (BELOWOF3-ROBOT))
(not (BELOWOF13-ROBOT))
(not (BELOWOF8-ROBOT))
(not (BELOWOF14-ROBOT))
)
:effect
(and
(Flag280prime-)
(not (Flag280-))
)

)
(:action Flag281Action-0
:parameters ()
:precondition
(and
(BELOWOF3-ROBOT)
)
:effect
(and
(Flag281-)
(Flag281prime-)
)

)
(:action Flag281Action-1
:parameters ()
:precondition
(and
(BELOWOF19-ROBOT)
)
:effect
(and
(Flag281-)
(Flag281prime-)
)

)
(:action Flag281Action-2
:parameters ()
:precondition
(and
(BELOWOF16-ROBOT)
)
:effect
(and
(Flag281-)
(Flag281prime-)
)

)
(:action Flag281Action-3
:parameters ()
:precondition
(and
(BELOWOF15-ROBOT)
)
:effect
(and
(Flag281-)
(Flag281prime-)
)

)
(:action Flag281Action-4
:parameters ()
:precondition
(and
(BELOWOF6-ROBOT)
)
:effect
(and
(Flag281-)
(Flag281prime-)
)

)
(:action Flag281Action-5
:parameters ()
:precondition
(and
(BELOWOF17-ROBOT)
)
:effect
(and
(Flag281-)
(Flag281prime-)
)

)
(:action Flag281Action-6
:parameters ()
:precondition
(and
(BELOWOF5-ROBOT)
)
:effect
(and
(Flag281-)
(Flag281prime-)
)

)
(:action Flag281Action-7
:parameters ()
:precondition
(and
(BELOWOF2-ROBOT)
)
:effect
(and
(Flag281-)
(Flag281prime-)
)

)
(:action Flag281Action-8
:parameters ()
:precondition
(and
(BELOWOF1-ROBOT)
)
:effect
(and
(Flag281-)
(Flag281prime-)
)

)
(:action Flag281Action-9
:parameters ()
:precondition
(and
(BELOWOF4-ROBOT)
)
:effect
(and
(Flag281-)
(Flag281prime-)
)

)
(:action Flag281Action-10
:parameters ()
:precondition
(and
(BELOWOF11-ROBOT)
)
:effect
(and
(Flag281-)
(Flag281prime-)
)

)
(:action Flag281Action-12
:parameters ()
:precondition
(and
(BELOWOF12-ROBOT)
)
:effect
(and
(Flag281-)
(Flag281prime-)
)

)
(:action Flag281Action-13
:parameters ()
:precondition
(and
(BELOWOF7-ROBOT)
)
:effect
(and
(Flag281-)
(Flag281prime-)
)

)
(:action Flag281Action-14
:parameters ()
:precondition
(and
(BELOWOF9-ROBOT)
)
:effect
(and
(Flag281-)
(Flag281prime-)
)

)
(:action Flag281Action-15
:parameters ()
:precondition
(and
(BELOWOF10-ROBOT)
)
:effect
(and
(Flag281-)
(Flag281prime-)
)

)
(:action Flag281Action-16
:parameters ()
:precondition
(and
(BELOWOF13-ROBOT)
)
:effect
(and
(Flag281-)
(Flag281prime-)
)

)
(:action Flag281Action-17
:parameters ()
:precondition
(and
(BELOWOF8-ROBOT)
)
:effect
(and
(Flag281-)
(Flag281prime-)
)

)
(:action Flag281Action-18
:parameters ()
:precondition
(and
(BELOWOF14-ROBOT)
)
:effect
(and
(Flag281-)
(Flag281prime-)
)

)
(:action Prim281Action
:parameters ()
:precondition
(and
(not (BELOWOF3-ROBOT))
(not (BELOWOF19-ROBOT))
(not (BELOWOF16-ROBOT))
(not (BELOWOF15-ROBOT))
(not (BELOWOF6-ROBOT))
(not (BELOWOF17-ROBOT))
(not (BELOWOF5-ROBOT))
(not (BELOWOF2-ROBOT))
(not (BELOWOF1-ROBOT))
(not (BELOWOF4-ROBOT))
(not (BELOWOF11-ROBOT))
(not (BELOWOF18-ROBOT))
(not (BELOWOF12-ROBOT))
(not (BELOWOF7-ROBOT))
(not (BELOWOF9-ROBOT))
(not (BELOWOF10-ROBOT))
(not (BELOWOF13-ROBOT))
(not (BELOWOF8-ROBOT))
(not (BELOWOF14-ROBOT))
)
:effect
(and
(Flag281prime-)
(not (Flag281-))
)

)
(:action Flag282Action-0
:parameters ()
:precondition
(and
(ABOVEOF6-ROBOT)
)
:effect
(and
(Flag282-)
(Flag282prime-)
)

)
(:action Flag282Action-1
:parameters ()
:precondition
(and
(ABOVEOF11-ROBOT)
)
:effect
(and
(Flag282-)
(Flag282prime-)
)

)
(:action Flag282Action-2
:parameters ()
:precondition
(and
(ABOVEOF10-ROBOT)
)
:effect
(and
(Flag282-)
(Flag282prime-)
)

)
(:action Flag282Action-3
:parameters ()
:precondition
(and
(ABOVEOF2-ROBOT)
)
:effect
(and
(Flag282-)
(Flag282prime-)
)

)
(:action Flag282Action-4
:parameters ()
:precondition
(and
(ABOVEOF7-ROBOT)
)
:effect
(and
(Flag282-)
(Flag282prime-)
)

)
(:action Flag282Action-5
:parameters ()
:precondition
(and
(ABOVEOF12-ROBOT)
)
:effect
(and
(Flag282-)
(Flag282prime-)
)

)
(:action Flag282Action-6
:parameters ()
:precondition
(and
(ABOVEOF17-ROBOT)
)
:effect
(and
(Flag282-)
(Flag282prime-)
)

)
(:action Flag282Action-7
:parameters ()
:precondition
(and
(ABOVEOF15-ROBOT)
)
:effect
(and
(Flag282-)
(Flag282prime-)
)

)
(:action Flag282Action-8
:parameters ()
:precondition
(and
(ABOVEOF8-ROBOT)
)
:effect
(and
(Flag282-)
(Flag282prime-)
)

)
(:action Flag282Action-9
:parameters ()
:precondition
(and
(ABOVEOF9-ROBOT)
)
:effect
(and
(Flag282-)
(Flag282prime-)
)

)
(:action Flag282Action-10
:parameters ()
:precondition
(and
(ABOVEOF16-ROBOT)
)
:effect
(and
(Flag282-)
(Flag282prime-)
)

)
(:action Flag282Action-11
:parameters ()
:precondition
(and
(ABOVEOF4-ROBOT)
)
:effect
(and
(Flag282-)
(Flag282prime-)
)

)
(:action Flag282Action-12
:parameters ()
:precondition
(and
(ABOVEOF3-ROBOT)
)
:effect
(and
(Flag282-)
(Flag282prime-)
)

)
(:action Flag282Action-13
:parameters ()
:precondition
(and
(ABOVEOF13-ROBOT)
)
:effect
(and
(Flag282-)
(Flag282prime-)
)

)
(:action Flag282Action-14
:parameters ()
:precondition
(and
(ABOVEOF14-ROBOT)
)
:effect
(and
(Flag282-)
(Flag282prime-)
)

)
(:action Flag282Action-15
:parameters ()
:precondition
(and
(ABOVEOF5-ROBOT)
)
:effect
(and
(Flag282-)
(Flag282prime-)
)

)
(:action Flag282Action-16
:parameters ()
:precondition
(and
(ABOVEOF1-ROBOT)
)
:effect
(and
(Flag282-)
(Flag282prime-)
)

)
(:action Flag282Action-17
:parameters ()
:precondition
(and
(ABOVEOF18-ROBOT)
)
:effect
(and
(Flag282-)
(Flag282prime-)
)

)
(:action Prim282Action
:parameters ()
:precondition
(and
(not (ABOVEOF6-ROBOT))
(not (ABOVEOF11-ROBOT))
(not (ABOVEOF10-ROBOT))
(not (ABOVEOF2-ROBOT))
(not (ABOVEOF7-ROBOT))
(not (ABOVEOF12-ROBOT))
(not (ABOVEOF17-ROBOT))
(not (ABOVEOF15-ROBOT))
(not (ABOVEOF8-ROBOT))
(not (ABOVEOF9-ROBOT))
(not (ABOVEOF16-ROBOT))
(not (ABOVEOF4-ROBOT))
(not (ABOVEOF3-ROBOT))
(not (ABOVEOF13-ROBOT))
(not (ABOVEOF14-ROBOT))
(not (ABOVEOF5-ROBOT))
(not (ABOVEOF1-ROBOT))
(not (ABOVEOF18-ROBOT))
)
:effect
(and
(Flag282prime-)
(not (Flag282-))
)

)
(:action Flag283Action-0
:parameters ()
:precondition
(and
(ABOVEOF11-ROBOT)
)
:effect
(and
(Flag283-)
(Flag283prime-)
)

)
(:action Flag283Action-1
:parameters ()
:precondition
(and
(ABOVEOF6-ROBOT)
)
:effect
(and
(Flag283-)
(Flag283prime-)
)

)
(:action Flag283Action-2
:parameters ()
:precondition
(and
(ABOVEOF10-ROBOT)
)
:effect
(and
(Flag283-)
(Flag283prime-)
)

)
(:action Flag283Action-3
:parameters ()
:precondition
(and
(ABOVEOF2-ROBOT)
)
:effect
(and
(Flag283-)
(Flag283prime-)
)

)
(:action Flag283Action-4
:parameters ()
:precondition
(and
(ABOVEOF7-ROBOT)
)
:effect
(and
(Flag283-)
(Flag283prime-)
)

)
(:action Flag283Action-5
:parameters ()
:precondition
(and
(ABOVEOF12-ROBOT)
)
:effect
(and
(Flag283-)
(Flag283prime-)
)

)
(:action Flag283Action-6
:parameters ()
:precondition
(and
(ABOVEOF17-ROBOT)
)
:effect
(and
(Flag283-)
(Flag283prime-)
)

)
(:action Flag283Action-7
:parameters ()
:precondition
(and
(ABOVEOF15-ROBOT)
)
:effect
(and
(Flag283-)
(Flag283prime-)
)

)
(:action Flag283Action-8
:parameters ()
:precondition
(and
(ABOVEOF8-ROBOT)
)
:effect
(and
(Flag283-)
(Flag283prime-)
)

)
(:action Flag283Action-9
:parameters ()
:precondition
(and
(ABOVEOF9-ROBOT)
)
:effect
(and
(Flag283-)
(Flag283prime-)
)

)
(:action Flag283Action-10
:parameters ()
:precondition
(and
(ABOVEOF16-ROBOT)
)
:effect
(and
(Flag283-)
(Flag283prime-)
)

)
(:action Flag283Action-11
:parameters ()
:precondition
(and
(ABOVEOF4-ROBOT)
)
:effect
(and
(Flag283-)
(Flag283prime-)
)

)
(:action Flag283Action-12
:parameters ()
:precondition
(and
(ABOVEOF3-ROBOT)
)
:effect
(and
(Flag283-)
(Flag283prime-)
)

)
(:action Flag283Action-13
:parameters ()
:precondition
(and
(ABOVEOF13-ROBOT)
)
:effect
(and
(Flag283-)
(Flag283prime-)
)

)
(:action Flag283Action-14
:parameters ()
:precondition
(and
(ABOVEOF14-ROBOT)
)
:effect
(and
(Flag283-)
(Flag283prime-)
)

)
(:action Flag283Action-15
:parameters ()
:precondition
(and
(ABOVEOF5-ROBOT)
)
:effect
(and
(Flag283-)
(Flag283prime-)
)

)
(:action Flag283Action-16
:parameters ()
:precondition
(and
(ABOVEOF18-ROBOT)
)
:effect
(and
(Flag283-)
(Flag283prime-)
)

)
(:action Prim283Action
:parameters ()
:precondition
(and
(not (ABOVEOF11-ROBOT))
(not (ABOVEOF6-ROBOT))
(not (ABOVEOF10-ROBOT))
(not (ABOVEOF2-ROBOT))
(not (ABOVEOF7-ROBOT))
(not (ABOVEOF12-ROBOT))
(not (ABOVEOF17-ROBOT))
(not (ABOVEOF15-ROBOT))
(not (ABOVEOF8-ROBOT))
(not (ABOVEOF9-ROBOT))
(not (ABOVEOF16-ROBOT))
(not (ABOVEOF4-ROBOT))
(not (ABOVEOF3-ROBOT))
(not (ABOVEOF13-ROBOT))
(not (ABOVEOF14-ROBOT))
(not (ABOVEOF5-ROBOT))
(not (ABOVEOF18-ROBOT))
)
:effect
(and
(Flag283prime-)
(not (Flag283-))
)

)
(:action Flag284Action-0
:parameters ()
:precondition
(and
(ABOVEOF11-ROBOT)
)
:effect
(and
(Flag284-)
(Flag284prime-)
)

)
(:action Flag284Action-1
:parameters ()
:precondition
(and
(ABOVEOF6-ROBOT)
)
:effect
(and
(Flag284-)
(Flag284prime-)
)

)
(:action Flag284Action-2
:parameters ()
:precondition
(and
(ABOVEOF10-ROBOT)
)
:effect
(and
(Flag284-)
(Flag284prime-)
)

)
(:action Flag284Action-3
:parameters ()
:precondition
(and
(ABOVEOF8-ROBOT)
)
:effect
(and
(Flag284-)
(Flag284prime-)
)

)
(:action Flag284Action-4
:parameters ()
:precondition
(and
(ABOVEOF7-ROBOT)
)
:effect
(and
(Flag284-)
(Flag284prime-)
)

)
(:action Flag284Action-5
:parameters ()
:precondition
(and
(ABOVEOF12-ROBOT)
)
:effect
(and
(Flag284-)
(Flag284prime-)
)

)
(:action Flag284Action-6
:parameters ()
:precondition
(and
(ABOVEOF17-ROBOT)
)
:effect
(and
(Flag284-)
(Flag284prime-)
)

)
(:action Flag284Action-7
:parameters ()
:precondition
(and
(ABOVEOF15-ROBOT)
)
:effect
(and
(Flag284-)
(Flag284prime-)
)

)
(:action Flag284Action-8
:parameters ()
:precondition
(and
(ABOVEOF9-ROBOT)
)
:effect
(and
(Flag284-)
(Flag284prime-)
)

)
(:action Flag284Action-9
:parameters ()
:precondition
(and
(ABOVEOF16-ROBOT)
)
:effect
(and
(Flag284-)
(Flag284prime-)
)

)
(:action Flag284Action-10
:parameters ()
:precondition
(and
(ABOVEOF4-ROBOT)
)
:effect
(and
(Flag284-)
(Flag284prime-)
)

)
(:action Flag284Action-11
:parameters ()
:precondition
(and
(ABOVEOF3-ROBOT)
)
:effect
(and
(Flag284-)
(Flag284prime-)
)

)
(:action Flag284Action-12
:parameters ()
:precondition
(and
(ABOVEOF13-ROBOT)
)
:effect
(and
(Flag284-)
(Flag284prime-)
)

)
(:action Flag284Action-13
:parameters ()
:precondition
(and
(ABOVEOF14-ROBOT)
)
:effect
(and
(Flag284-)
(Flag284prime-)
)

)
(:action Flag284Action-14
:parameters ()
:precondition
(and
(ABOVEOF5-ROBOT)
)
:effect
(and
(Flag284-)
(Flag284prime-)
)

)
(:action Flag284Action-15
:parameters ()
:precondition
(and
(ABOVEOF18-ROBOT)
)
:effect
(and
(Flag284-)
(Flag284prime-)
)

)
(:action Prim284Action
:parameters ()
:precondition
(and
(not (ABOVEOF11-ROBOT))
(not (ABOVEOF6-ROBOT))
(not (ABOVEOF10-ROBOT))
(not (ABOVEOF8-ROBOT))
(not (ABOVEOF7-ROBOT))
(not (ABOVEOF12-ROBOT))
(not (ABOVEOF17-ROBOT))
(not (ABOVEOF15-ROBOT))
(not (ABOVEOF9-ROBOT))
(not (ABOVEOF16-ROBOT))
(not (ABOVEOF4-ROBOT))
(not (ABOVEOF3-ROBOT))
(not (ABOVEOF13-ROBOT))
(not (ABOVEOF14-ROBOT))
(not (ABOVEOF5-ROBOT))
(not (ABOVEOF18-ROBOT))
)
:effect
(and
(Flag284prime-)
(not (Flag284-))
)

)
(:action Flag285Action-0
:parameters ()
:precondition
(and
(ABOVEOF11-ROBOT)
)
:effect
(and
(Flag285-)
(Flag285prime-)
)

)
(:action Flag285Action-1
:parameters ()
:precondition
(and
(ABOVEOF6-ROBOT)
)
:effect
(and
(Flag285-)
(Flag285prime-)
)

)
(:action Flag285Action-2
:parameters ()
:precondition
(and
(ABOVEOF10-ROBOT)
)
:effect
(and
(Flag285-)
(Flag285prime-)
)

)
(:action Flag285Action-3
:parameters ()
:precondition
(and
(ABOVEOF8-ROBOT)
)
:effect
(and
(Flag285-)
(Flag285prime-)
)

)
(:action Flag285Action-4
:parameters ()
:precondition
(and
(ABOVEOF7-ROBOT)
)
:effect
(and
(Flag285-)
(Flag285prime-)
)

)
(:action Flag285Action-5
:parameters ()
:precondition
(and
(ABOVEOF12-ROBOT)
)
:effect
(and
(Flag285-)
(Flag285prime-)
)

)
(:action Flag285Action-6
:parameters ()
:precondition
(and
(ABOVEOF17-ROBOT)
)
:effect
(and
(Flag285-)
(Flag285prime-)
)

)
(:action Flag285Action-7
:parameters ()
:precondition
(and
(ABOVEOF15-ROBOT)
)
:effect
(and
(Flag285-)
(Flag285prime-)
)

)
(:action Flag285Action-8
:parameters ()
:precondition
(and
(ABOVEOF9-ROBOT)
)
:effect
(and
(Flag285-)
(Flag285prime-)
)

)
(:action Flag285Action-9
:parameters ()
:precondition
(and
(ABOVEOF16-ROBOT)
)
:effect
(and
(Flag285-)
(Flag285prime-)
)

)
(:action Flag285Action-10
:parameters ()
:precondition
(and
(ABOVEOF4-ROBOT)
)
:effect
(and
(Flag285-)
(Flag285prime-)
)

)
(:action Flag285Action-11
:parameters ()
:precondition
(and
(ABOVEOF13-ROBOT)
)
:effect
(and
(Flag285-)
(Flag285prime-)
)

)
(:action Flag285Action-12
:parameters ()
:precondition
(and
(ABOVEOF14-ROBOT)
)
:effect
(and
(Flag285-)
(Flag285prime-)
)

)
(:action Flag285Action-13
:parameters ()
:precondition
(and
(ABOVEOF5-ROBOT)
)
:effect
(and
(Flag285-)
(Flag285prime-)
)

)
(:action Flag285Action-14
:parameters ()
:precondition
(and
(ABOVEOF18-ROBOT)
)
:effect
(and
(Flag285-)
(Flag285prime-)
)

)
(:action Prim285Action
:parameters ()
:precondition
(and
(not (ABOVEOF11-ROBOT))
(not (ABOVEOF6-ROBOT))
(not (ABOVEOF10-ROBOT))
(not (ABOVEOF8-ROBOT))
(not (ABOVEOF7-ROBOT))
(not (ABOVEOF12-ROBOT))
(not (ABOVEOF17-ROBOT))
(not (ABOVEOF15-ROBOT))
(not (ABOVEOF9-ROBOT))
(not (ABOVEOF16-ROBOT))
(not (ABOVEOF4-ROBOT))
(not (ABOVEOF13-ROBOT))
(not (ABOVEOF14-ROBOT))
(not (ABOVEOF5-ROBOT))
(not (ABOVEOF18-ROBOT))
)
:effect
(and
(Flag285prime-)
(not (Flag285-))
)

)
(:action Flag286Action-0
:parameters ()
:precondition
(and
(ABOVEOF11-ROBOT)
)
:effect
(and
(Flag286-)
(Flag286prime-)
)

)
(:action Flag286Action-1
:parameters ()
:precondition
(and
(ABOVEOF6-ROBOT)
)
:effect
(and
(Flag286-)
(Flag286prime-)
)

)
(:action Flag286Action-2
:parameters ()
:precondition
(and
(ABOVEOF7-ROBOT)
)
:effect
(and
(Flag286-)
(Flag286prime-)
)

)
(:action Flag286Action-3
:parameters ()
:precondition
(and
(ABOVEOF8-ROBOT)
)
:effect
(and
(Flag286-)
(Flag286prime-)
)

)
(:action Flag286Action-4
:parameters ()
:precondition
(and
(ABOVEOF10-ROBOT)
)
:effect
(and
(Flag286-)
(Flag286prime-)
)

)
(:action Flag286Action-5
:parameters ()
:precondition
(and
(ABOVEOF12-ROBOT)
)
:effect
(and
(Flag286-)
(Flag286prime-)
)

)
(:action Flag286Action-6
:parameters ()
:precondition
(and
(ABOVEOF17-ROBOT)
)
:effect
(and
(Flag286-)
(Flag286prime-)
)

)
(:action Flag286Action-7
:parameters ()
:precondition
(and
(ABOVEOF15-ROBOT)
)
:effect
(and
(Flag286-)
(Flag286prime-)
)

)
(:action Flag286Action-8
:parameters ()
:precondition
(and
(ABOVEOF9-ROBOT)
)
:effect
(and
(Flag286-)
(Flag286prime-)
)

)
(:action Flag286Action-9
:parameters ()
:precondition
(and
(ABOVEOF16-ROBOT)
)
:effect
(and
(Flag286-)
(Flag286prime-)
)

)
(:action Flag286Action-10
:parameters ()
:precondition
(and
(ABOVEOF13-ROBOT)
)
:effect
(and
(Flag286-)
(Flag286prime-)
)

)
(:action Flag286Action-11
:parameters ()
:precondition
(and
(ABOVEOF14-ROBOT)
)
:effect
(and
(Flag286-)
(Flag286prime-)
)

)
(:action Flag286Action-12
:parameters ()
:precondition
(and
(ABOVEOF5-ROBOT)
)
:effect
(and
(Flag286-)
(Flag286prime-)
)

)
(:action Flag286Action-13
:parameters ()
:precondition
(and
(ABOVEOF18-ROBOT)
)
:effect
(and
(Flag286-)
(Flag286prime-)
)

)
(:action Prim286Action
:parameters ()
:precondition
(and
(not (ABOVEOF11-ROBOT))
(not (ABOVEOF6-ROBOT))
(not (ABOVEOF7-ROBOT))
(not (ABOVEOF8-ROBOT))
(not (ABOVEOF10-ROBOT))
(not (ABOVEOF12-ROBOT))
(not (ABOVEOF17-ROBOT))
(not (ABOVEOF15-ROBOT))
(not (ABOVEOF9-ROBOT))
(not (ABOVEOF16-ROBOT))
(not (ABOVEOF13-ROBOT))
(not (ABOVEOF14-ROBOT))
(not (ABOVEOF5-ROBOT))
(not (ABOVEOF18-ROBOT))
)
:effect
(and
(Flag286prime-)
(not (Flag286-))
)

)
(:action Flag287Action-0
:parameters ()
:precondition
(and
(ABOVEOF11-ROBOT)
)
:effect
(and
(Flag287-)
(Flag287prime-)
)

)
(:action Flag287Action-1
:parameters ()
:precondition
(and
(ABOVEOF6-ROBOT)
)
:effect
(and
(Flag287-)
(Flag287prime-)
)

)
(:action Flag287Action-2
:parameters ()
:precondition
(and
(ABOVEOF7-ROBOT)
)
:effect
(and
(Flag287-)
(Flag287prime-)
)

)
(:action Flag287Action-3
:parameters ()
:precondition
(and
(ABOVEOF8-ROBOT)
)
:effect
(and
(Flag287-)
(Flag287prime-)
)

)
(:action Flag287Action-4
:parameters ()
:precondition
(and
(ABOVEOF10-ROBOT)
)
:effect
(and
(Flag287-)
(Flag287prime-)
)

)
(:action Flag287Action-5
:parameters ()
:precondition
(and
(ABOVEOF12-ROBOT)
)
:effect
(and
(Flag287-)
(Flag287prime-)
)

)
(:action Flag287Action-6
:parameters ()
:precondition
(and
(ABOVEOF17-ROBOT)
)
:effect
(and
(Flag287-)
(Flag287prime-)
)

)
(:action Flag287Action-7
:parameters ()
:precondition
(and
(ABOVEOF15-ROBOT)
)
:effect
(and
(Flag287-)
(Flag287prime-)
)

)
(:action Flag287Action-8
:parameters ()
:precondition
(and
(ABOVEOF9-ROBOT)
)
:effect
(and
(Flag287-)
(Flag287prime-)
)

)
(:action Flag287Action-9
:parameters ()
:precondition
(and
(ABOVEOF16-ROBOT)
)
:effect
(and
(Flag287-)
(Flag287prime-)
)

)
(:action Flag287Action-10
:parameters ()
:precondition
(and
(ABOVEOF13-ROBOT)
)
:effect
(and
(Flag287-)
(Flag287prime-)
)

)
(:action Flag287Action-11
:parameters ()
:precondition
(and
(ABOVEOF14-ROBOT)
)
:effect
(and
(Flag287-)
(Flag287prime-)
)

)
(:action Flag287Action-12
:parameters ()
:precondition
(and
(ABOVEOF18-ROBOT)
)
:effect
(and
(Flag287-)
(Flag287prime-)
)

)
(:action Prim287Action
:parameters ()
:precondition
(and
(not (ABOVEOF11-ROBOT))
(not (ABOVEOF6-ROBOT))
(not (ABOVEOF7-ROBOT))
(not (ABOVEOF8-ROBOT))
(not (ABOVEOF10-ROBOT))
(not (ABOVEOF12-ROBOT))
(not (ABOVEOF17-ROBOT))
(not (ABOVEOF15-ROBOT))
(not (ABOVEOF9-ROBOT))
(not (ABOVEOF16-ROBOT))
(not (ABOVEOF13-ROBOT))
(not (ABOVEOF14-ROBOT))
(not (ABOVEOF18-ROBOT))
)
:effect
(and
(Flag287prime-)
(not (Flag287-))
)

)
(:action Flag288Action-0
:parameters ()
:precondition
(and
(ABOVEOF11-ROBOT)
)
:effect
(and
(Flag288-)
(Flag288prime-)
)

)
(:action Flag288Action-1
:parameters ()
:precondition
(and
(ABOVEOF7-ROBOT)
)
:effect
(and
(Flag288-)
(Flag288prime-)
)

)
(:action Flag288Action-2
:parameters ()
:precondition
(and
(ABOVEOF8-ROBOT)
)
:effect
(and
(Flag288-)
(Flag288prime-)
)

)
(:action Flag288Action-3
:parameters ()
:precondition
(and
(ABOVEOF10-ROBOT)
)
:effect
(and
(Flag288-)
(Flag288prime-)
)

)
(:action Flag288Action-4
:parameters ()
:precondition
(and
(ABOVEOF12-ROBOT)
)
:effect
(and
(Flag288-)
(Flag288prime-)
)

)
(:action Flag288Action-5
:parameters ()
:precondition
(and
(ABOVEOF17-ROBOT)
)
:effect
(and
(Flag288-)
(Flag288prime-)
)

)
(:action Flag288Action-6
:parameters ()
:precondition
(and
(ABOVEOF15-ROBOT)
)
:effect
(and
(Flag288-)
(Flag288prime-)
)

)
(:action Flag288Action-7
:parameters ()
:precondition
(and
(ABOVEOF9-ROBOT)
)
:effect
(and
(Flag288-)
(Flag288prime-)
)

)
(:action Flag288Action-8
:parameters ()
:precondition
(and
(ABOVEOF16-ROBOT)
)
:effect
(and
(Flag288-)
(Flag288prime-)
)

)
(:action Flag288Action-9
:parameters ()
:precondition
(and
(ABOVEOF13-ROBOT)
)
:effect
(and
(Flag288-)
(Flag288prime-)
)

)
(:action Flag288Action-10
:parameters ()
:precondition
(and
(ABOVEOF14-ROBOT)
)
:effect
(and
(Flag288-)
(Flag288prime-)
)

)
(:action Flag288Action-11
:parameters ()
:precondition
(and
(ABOVEOF18-ROBOT)
)
:effect
(and
(Flag288-)
(Flag288prime-)
)

)
(:action Prim288Action
:parameters ()
:precondition
(and
(not (ABOVEOF11-ROBOT))
(not (ABOVEOF7-ROBOT))
(not (ABOVEOF8-ROBOT))
(not (ABOVEOF10-ROBOT))
(not (ABOVEOF12-ROBOT))
(not (ABOVEOF17-ROBOT))
(not (ABOVEOF15-ROBOT))
(not (ABOVEOF9-ROBOT))
(not (ABOVEOF16-ROBOT))
(not (ABOVEOF13-ROBOT))
(not (ABOVEOF14-ROBOT))
(not (ABOVEOF18-ROBOT))
)
:effect
(and
(Flag288prime-)
(not (Flag288-))
)

)
(:action Flag289Action-0
:parameters ()
:precondition
(and
(ABOVEOF11-ROBOT)
)
:effect
(and
(Flag289-)
(Flag289prime-)
)

)
(:action Flag289Action-1
:parameters ()
:precondition
(and
(ABOVEOF8-ROBOT)
)
:effect
(and
(Flag289-)
(Flag289prime-)
)

)
(:action Flag289Action-2
:parameters ()
:precondition
(and
(ABOVEOF10-ROBOT)
)
:effect
(and
(Flag289-)
(Flag289prime-)
)

)
(:action Flag289Action-3
:parameters ()
:precondition
(and
(ABOVEOF12-ROBOT)
)
:effect
(and
(Flag289-)
(Flag289prime-)
)

)
(:action Flag289Action-4
:parameters ()
:precondition
(and
(ABOVEOF17-ROBOT)
)
:effect
(and
(Flag289-)
(Flag289prime-)
)

)
(:action Flag289Action-5
:parameters ()
:precondition
(and
(ABOVEOF15-ROBOT)
)
:effect
(and
(Flag289-)
(Flag289prime-)
)

)
(:action Flag289Action-6
:parameters ()
:precondition
(and
(ABOVEOF9-ROBOT)
)
:effect
(and
(Flag289-)
(Flag289prime-)
)

)
(:action Flag289Action-7
:parameters ()
:precondition
(and
(ABOVEOF16-ROBOT)
)
:effect
(and
(Flag289-)
(Flag289prime-)
)

)
(:action Flag289Action-8
:parameters ()
:precondition
(and
(ABOVEOF13-ROBOT)
)
:effect
(and
(Flag289-)
(Flag289prime-)
)

)
(:action Flag289Action-9
:parameters ()
:precondition
(and
(ABOVEOF14-ROBOT)
)
:effect
(and
(Flag289-)
(Flag289prime-)
)

)
(:action Flag289Action-10
:parameters ()
:precondition
(and
(ABOVEOF18-ROBOT)
)
:effect
(and
(Flag289-)
(Flag289prime-)
)

)
(:action Prim289Action
:parameters ()
:precondition
(and
(not (ABOVEOF11-ROBOT))
(not (ABOVEOF8-ROBOT))
(not (ABOVEOF10-ROBOT))
(not (ABOVEOF12-ROBOT))
(not (ABOVEOF17-ROBOT))
(not (ABOVEOF15-ROBOT))
(not (ABOVEOF9-ROBOT))
(not (ABOVEOF16-ROBOT))
(not (ABOVEOF13-ROBOT))
(not (ABOVEOF14-ROBOT))
(not (ABOVEOF18-ROBOT))
)
:effect
(and
(Flag289prime-)
(not (Flag289-))
)

)
(:action Flag290Action-0
:parameters ()
:precondition
(and
(ABOVEOF11-ROBOT)
)
:effect
(and
(Flag290-)
(Flag290prime-)
)

)
(:action Flag290Action-1
:parameters ()
:precondition
(and
(ABOVEOF10-ROBOT)
)
:effect
(and
(Flag290-)
(Flag290prime-)
)

)
(:action Flag290Action-2
:parameters ()
:precondition
(and
(ABOVEOF12-ROBOT)
)
:effect
(and
(Flag290-)
(Flag290prime-)
)

)
(:action Flag290Action-3
:parameters ()
:precondition
(and
(ABOVEOF17-ROBOT)
)
:effect
(and
(Flag290-)
(Flag290prime-)
)

)
(:action Flag290Action-4
:parameters ()
:precondition
(and
(ABOVEOF15-ROBOT)
)
:effect
(and
(Flag290-)
(Flag290prime-)
)

)
(:action Flag290Action-5
:parameters ()
:precondition
(and
(ABOVEOF9-ROBOT)
)
:effect
(and
(Flag290-)
(Flag290prime-)
)

)
(:action Flag290Action-6
:parameters ()
:precondition
(and
(ABOVEOF16-ROBOT)
)
:effect
(and
(Flag290-)
(Flag290prime-)
)

)
(:action Flag290Action-7
:parameters ()
:precondition
(and
(ABOVEOF13-ROBOT)
)
:effect
(and
(Flag290-)
(Flag290prime-)
)

)
(:action Flag290Action-8
:parameters ()
:precondition
(and
(ABOVEOF14-ROBOT)
)
:effect
(and
(Flag290-)
(Flag290prime-)
)

)
(:action Flag290Action-9
:parameters ()
:precondition
(and
(ABOVEOF18-ROBOT)
)
:effect
(and
(Flag290-)
(Flag290prime-)
)

)
(:action Prim290Action
:parameters ()
:precondition
(and
(not (ABOVEOF11-ROBOT))
(not (ABOVEOF10-ROBOT))
(not (ABOVEOF12-ROBOT))
(not (ABOVEOF17-ROBOT))
(not (ABOVEOF15-ROBOT))
(not (ABOVEOF9-ROBOT))
(not (ABOVEOF16-ROBOT))
(not (ABOVEOF13-ROBOT))
(not (ABOVEOF14-ROBOT))
(not (ABOVEOF18-ROBOT))
)
:effect
(and
(Flag290prime-)
(not (Flag290-))
)

)
(:action Flag291Action-0
:parameters ()
:precondition
(and
(ABOVEOF11-ROBOT)
)
:effect
(and
(Flag291-)
(Flag291prime-)
)

)
(:action Flag291Action-1
:parameters ()
:precondition
(and
(ABOVEOF10-ROBOT)
)
:effect
(and
(Flag291-)
(Flag291prime-)
)

)
(:action Flag291Action-2
:parameters ()
:precondition
(and
(ABOVEOF12-ROBOT)
)
:effect
(and
(Flag291-)
(Flag291prime-)
)

)
(:action Flag291Action-3
:parameters ()
:precondition
(and
(ABOVEOF17-ROBOT)
)
:effect
(and
(Flag291-)
(Flag291prime-)
)

)
(:action Flag291Action-4
:parameters ()
:precondition
(and
(ABOVEOF15-ROBOT)
)
:effect
(and
(Flag291-)
(Flag291prime-)
)

)
(:action Flag291Action-5
:parameters ()
:precondition
(and
(ABOVEOF16-ROBOT)
)
:effect
(and
(Flag291-)
(Flag291prime-)
)

)
(:action Flag291Action-6
:parameters ()
:precondition
(and
(ABOVEOF13-ROBOT)
)
:effect
(and
(Flag291-)
(Flag291prime-)
)

)
(:action Flag291Action-7
:parameters ()
:precondition
(and
(ABOVEOF14-ROBOT)
)
:effect
(and
(Flag291-)
(Flag291prime-)
)

)
(:action Flag291Action-8
:parameters ()
:precondition
(and
(ABOVEOF18-ROBOT)
)
:effect
(and
(Flag291-)
(Flag291prime-)
)

)
(:action Prim291Action
:parameters ()
:precondition
(and
(not (ABOVEOF11-ROBOT))
(not (ABOVEOF10-ROBOT))
(not (ABOVEOF12-ROBOT))
(not (ABOVEOF17-ROBOT))
(not (ABOVEOF15-ROBOT))
(not (ABOVEOF16-ROBOT))
(not (ABOVEOF13-ROBOT))
(not (ABOVEOF14-ROBOT))
(not (ABOVEOF18-ROBOT))
)
:effect
(and
(Flag291prime-)
(not (Flag291-))
)

)
(:action Flag292Action-0
:parameters ()
:precondition
(and
(ABOVEOF11-ROBOT)
)
:effect
(and
(Flag292-)
(Flag292prime-)
)

)
(:action Flag292Action-1
:parameters ()
:precondition
(and
(ABOVEOF12-ROBOT)
)
:effect
(and
(Flag292-)
(Flag292prime-)
)

)
(:action Flag292Action-2
:parameters ()
:precondition
(and
(ABOVEOF17-ROBOT)
)
:effect
(and
(Flag292-)
(Flag292prime-)
)

)
(:action Flag292Action-3
:parameters ()
:precondition
(and
(ABOVEOF15-ROBOT)
)
:effect
(and
(Flag292-)
(Flag292prime-)
)

)
(:action Flag292Action-4
:parameters ()
:precondition
(and
(ABOVEOF16-ROBOT)
)
:effect
(and
(Flag292-)
(Flag292prime-)
)

)
(:action Flag292Action-5
:parameters ()
:precondition
(and
(ABOVEOF13-ROBOT)
)
:effect
(and
(Flag292-)
(Flag292prime-)
)

)
(:action Flag292Action-6
:parameters ()
:precondition
(and
(ABOVEOF14-ROBOT)
)
:effect
(and
(Flag292-)
(Flag292prime-)
)

)
(:action Flag292Action-7
:parameters ()
:precondition
(and
(ABOVEOF18-ROBOT)
)
:effect
(and
(Flag292-)
(Flag292prime-)
)

)
(:action Prim292Action
:parameters ()
:precondition
(and
(not (ABOVEOF11-ROBOT))
(not (ABOVEOF12-ROBOT))
(not (ABOVEOF17-ROBOT))
(not (ABOVEOF15-ROBOT))
(not (ABOVEOF16-ROBOT))
(not (ABOVEOF13-ROBOT))
(not (ABOVEOF14-ROBOT))
(not (ABOVEOF18-ROBOT))
)
:effect
(and
(Flag292prime-)
(not (Flag292-))
)

)
(:action Flag293Action-0
:parameters ()
:precondition
(and
(ABOVEOF12-ROBOT)
)
:effect
(and
(Flag293-)
(Flag293prime-)
)

)
(:action Flag293Action-1
:parameters ()
:precondition
(and
(ABOVEOF17-ROBOT)
)
:effect
(and
(Flag293-)
(Flag293prime-)
)

)
(:action Flag293Action-2
:parameters ()
:precondition
(and
(ABOVEOF15-ROBOT)
)
:effect
(and
(Flag293-)
(Flag293prime-)
)

)
(:action Flag293Action-3
:parameters ()
:precondition
(and
(ABOVEOF16-ROBOT)
)
:effect
(and
(Flag293-)
(Flag293prime-)
)

)
(:action Flag293Action-4
:parameters ()
:precondition
(and
(ABOVEOF13-ROBOT)
)
:effect
(and
(Flag293-)
(Flag293prime-)
)

)
(:action Flag293Action-5
:parameters ()
:precondition
(and
(ABOVEOF14-ROBOT)
)
:effect
(and
(Flag293-)
(Flag293prime-)
)

)
(:action Flag293Action-6
:parameters ()
:precondition
(and
(ABOVEOF18-ROBOT)
)
:effect
(and
(Flag293-)
(Flag293prime-)
)

)
(:action Prim293Action
:parameters ()
:precondition
(and
(not (ABOVEOF12-ROBOT))
(not (ABOVEOF17-ROBOT))
(not (ABOVEOF15-ROBOT))
(not (ABOVEOF16-ROBOT))
(not (ABOVEOF13-ROBOT))
(not (ABOVEOF14-ROBOT))
(not (ABOVEOF18-ROBOT))
)
:effect
(and
(Flag293prime-)
(not (Flag293-))
)

)
(:action Flag294Action-0
:parameters ()
:precondition
(and
(ABOVEOF17-ROBOT)
)
:effect
(and
(Flag294-)
(Flag294prime-)
)

)
(:action Flag294Action-1
:parameters ()
:precondition
(and
(ABOVEOF15-ROBOT)
)
:effect
(and
(Flag294-)
(Flag294prime-)
)

)
(:action Flag294Action-2
:parameters ()
:precondition
(and
(ABOVEOF16-ROBOT)
)
:effect
(and
(Flag294-)
(Flag294prime-)
)

)
(:action Flag294Action-3
:parameters ()
:precondition
(and
(ABOVEOF13-ROBOT)
)
:effect
(and
(Flag294-)
(Flag294prime-)
)

)
(:action Flag294Action-4
:parameters ()
:precondition
(and
(ABOVEOF14-ROBOT)
)
:effect
(and
(Flag294-)
(Flag294prime-)
)

)
(:action Flag294Action-5
:parameters ()
:precondition
(and
(ABOVEOF18-ROBOT)
)
:effect
(and
(Flag294-)
(Flag294prime-)
)

)
(:action Prim294Action
:parameters ()
:precondition
(and
(not (ABOVEOF17-ROBOT))
(not (ABOVEOF15-ROBOT))
(not (ABOVEOF16-ROBOT))
(not (ABOVEOF13-ROBOT))
(not (ABOVEOF14-ROBOT))
(not (ABOVEOF18-ROBOT))
)
:effect
(and
(Flag294prime-)
(not (Flag294-))
)

)
(:action Flag295Action-0
:parameters ()
:precondition
(and
(ABOVEOF16-ROBOT)
)
:effect
(and
(Flag295-)
(Flag295prime-)
)

)
(:action Flag295Action-1
:parameters ()
:precondition
(and
(ABOVEOF14-ROBOT)
)
:effect
(and
(Flag295-)
(Flag295prime-)
)

)
(:action Flag295Action-2
:parameters ()
:precondition
(and
(ABOVEOF17-ROBOT)
)
:effect
(and
(Flag295-)
(Flag295prime-)
)

)
(:action Flag295Action-3
:parameters ()
:precondition
(and
(ABOVEOF15-ROBOT)
)
:effect
(and
(Flag295-)
(Flag295prime-)
)

)
(:action Flag295Action-4
:parameters ()
:precondition
(and
(ABOVEOF18-ROBOT)
)
:effect
(and
(Flag295-)
(Flag295prime-)
)

)
(:action Prim295Action
:parameters ()
:precondition
(and
(not (ABOVEOF16-ROBOT))
(not (ABOVEOF14-ROBOT))
(not (ABOVEOF17-ROBOT))
(not (ABOVEOF15-ROBOT))
(not (ABOVEOF18-ROBOT))
)
:effect
(and
(Flag295prime-)
(not (Flag295-))
)

)
(:action Flag296Action-0
:parameters ()
:precondition
(and
(ABOVEOF18-ROBOT)
)
:effect
(and
(Flag296-)
(Flag296prime-)
)

)
(:action Flag296Action-1
:parameters ()
:precondition
(and
(ABOVEOF17-ROBOT)
)
:effect
(and
(Flag296-)
(Flag296prime-)
)

)
(:action Flag296Action-2
:parameters ()
:precondition
(and
(ABOVEOF15-ROBOT)
)
:effect
(and
(Flag296-)
(Flag296prime-)
)

)
(:action Flag296Action-3
:parameters ()
:precondition
(and
(ABOVEOF16-ROBOT)
)
:effect
(and
(Flag296-)
(Flag296prime-)
)

)
(:action Prim296Action
:parameters ()
:precondition
(and
(not (ABOVEOF18-ROBOT))
(not (ABOVEOF17-ROBOT))
(not (ABOVEOF15-ROBOT))
(not (ABOVEOF16-ROBOT))
)
:effect
(and
(Flag296prime-)
(not (Flag296-))
)

)
(:action Flag297Action-0
:parameters ()
:precondition
(and
(ABOVEOF18-ROBOT)
)
:effect
(and
(Flag297-)
(Flag297prime-)
)

)
(:action Flag297Action-1
:parameters ()
:precondition
(and
(ABOVEOF17-ROBOT)
)
:effect
(and
(Flag297-)
(Flag297prime-)
)

)
(:action Flag297Action-2
:parameters ()
:precondition
(and
(ABOVEOF16-ROBOT)
)
:effect
(and
(Flag297-)
(Flag297prime-)
)

)
(:action Prim297Action
:parameters ()
:precondition
(and
(not (ABOVEOF18-ROBOT))
(not (ABOVEOF17-ROBOT))
(not (ABOVEOF16-ROBOT))
)
:effect
(and
(Flag297prime-)
(not (Flag297-))
)

)
(:action Flag298Action-0
:parameters ()
:precondition
(and
(ABOVEOF17-ROBOT)
)
:effect
(and
(Flag298-)
(Flag298prime-)
)

)
(:action Flag298Action-1
:parameters ()
:precondition
(and
(ABOVEOF18-ROBOT)
)
:effect
(and
(Flag298-)
(Flag298prime-)
)

)
(:action Prim298Action
:parameters ()
:precondition
(and
(not (ABOVEOF17-ROBOT))
(not (ABOVEOF18-ROBOT))
)
:effect
(and
(Flag298prime-)
(not (Flag298-))
)

)
(:action Flag299Action
:parameters ()
:precondition
(and
(ABOVEOF17-ROBOT)
(BELOWOF1-ROBOT)
)
:effect
(and
(Flag299-)
(Flag299prime-)
)

)
(:action Flag300Action
:parameters ()
:precondition
(and
(ABOVEOF18-ROBOT)
(BELOWOF1-ROBOT)
)
:effect
(and
(Flag300-)
(Flag300prime-)
)

)
(:action Flag301Action
:parameters ()
:precondition
(and
(BELOWOF1-ROBOT)
(ABOVEOF1-ROBOT)
)
:effect
(and
(Flag301-)
(Flag301prime-)
)

)
(:action Flag302Action
:parameters ()
:precondition
(and
(ABOVEOF18-ROBOT)
(BELOWOF2-ROBOT)
)
:effect
(and
(Flag302-)
(Flag302prime-)
)

)
(:action Flag303Action
:parameters ()
:precondition
(and
(ABOVEOF11-ROBOT)
(BELOWOF1-ROBOT)
)
:effect
(and
(Flag303-)
(Flag303prime-)
)

)
(:action Flag304Action
:parameters ()
:precondition
(and
(ABOVEOF1-ROBOT)
(BELOWOF2-ROBOT)
)
:effect
(and
(Flag304-)
(Flag304prime-)
)

)
(:action Flag305Action
:parameters ()
:precondition
(and
(ABOVEOF7-ROBOT)
(BELOWOF2-ROBOT)
)
:effect
(and
(Flag305-)
(Flag305prime-)
)

)
(:action Flag306Action
:parameters ()
:precondition
(and
(ABOVEOF15-ROBOT)
(BELOWOF2-ROBOT)
)
:effect
(and
(Flag306-)
(Flag306prime-)
)

)
(:action Flag307Action
:parameters ()
:precondition
(and
(BELOWOF2-ROBOT)
(ABOVEOF13-ROBOT)
)
:effect
(and
(Flag307-)
(Flag307prime-)
)

)
(:action Flag308Action
:parameters ()
:precondition
(and
(BELOWOF1-ROBOT)
(ABOVEOF16-ROBOT)
)
:effect
(and
(Flag308-)
(Flag308prime-)
)

)
(:action Flag309Action
:parameters ()
:precondition
(and
(BELOWOF1-ROBOT)
(ABOVEOF7-ROBOT)
)
:effect
(and
(Flag309-)
(Flag309prime-)
)

)
(:action Flag310Action
:parameters ()
:precondition
(and
(ABOVEOF2-ROBOT)
(BELOWOF2-ROBOT)
)
:effect
(and
(Flag310-)
(Flag310prime-)
)

)
(:action Flag311Action
:parameters ()
:precondition
(and
(BELOWOF1-ROBOT)
(ABOVEOF5-ROBOT)
)
:effect
(and
(Flag311-)
(Flag311prime-)
)

)
(:action Flag312Action
:parameters ()
:precondition
(and
(ABOVEOF16-ROBOT)
(BELOWOF2-ROBOT)
)
:effect
(and
(Flag312-)
(Flag312prime-)
)

)
(:action Flag313Action
:parameters ()
:precondition
(and
(ABOVEOF4-ROBOT)
(BELOWOF2-ROBOT)
)
:effect
(and
(Flag313-)
(Flag313prime-)
)

)
(:action Flag314Action
:parameters ()
:precondition
(and
(BELOWOF1-ROBOT)
(ABOVEOF17-ROBOT)
)
:effect
(and
(Flag314-)
(Flag314prime-)
)

)
(:action Flag315Action
:parameters ()
:precondition
(and
(BELOWOF2-ROBOT)
(ABOVEOF14-ROBOT)
)
:effect
(and
(Flag315-)
(Flag315prime-)
)

)
(:action Flag316Action
:parameters ()
:precondition
(and
(ABOVEOF6-ROBOT)
(BELOWOF2-ROBOT)
)
:effect
(and
(Flag316-)
(Flag316prime-)
)

)
(:action Flag317Action
:parameters ()
:precondition
(and
(BELOWOF1-ROBOT)
(ABOVEOF2-ROBOT)
)
:effect
(and
(Flag317-)
(Flag317prime-)
)

)
(:action Flag318Action
:parameters ()
:precondition
(and
(ABOVEOF9-ROBOT)
(BELOWOF2-ROBOT)
)
:effect
(and
(Flag318-)
(Flag318prime-)
)

)
(:action Flag319Action
:parameters ()
:precondition
(and
(BELOWOF1-ROBOT)
(ABOVEOF13-ROBOT)
)
:effect
(and
(Flag319-)
(Flag319prime-)
)

)
(:action Flag320Action
:parameters ()
:precondition
(and
(ABOVEOF6-ROBOT)
(BELOWOF1-ROBOT)
)
:effect
(and
(Flag320-)
(Flag320prime-)
)

)
(:action Flag321Action
:parameters ()
:precondition
(and
(BELOWOF1-ROBOT)
(ABOVEOF14-ROBOT)
)
:effect
(and
(Flag321-)
(Flag321prime-)
)

)
(:action Flag322Action
:parameters ()
:precondition
(and
(ABOVEOF17-ROBOT)
(BELOWOF2-ROBOT)
)
:effect
(and
(Flag322-)
(Flag322prime-)
)

)
(:action Flag323Action
:parameters ()
:precondition
(and
(ABOVEOF10-ROBOT)
(BELOWOF1-ROBOT)
)
:effect
(and
(Flag323-)
(Flag323prime-)
)

)
(:action Flag324Action
:parameters ()
:precondition
(and
(ABOVEOF11-ROBOT)
(BELOWOF2-ROBOT)
)
:effect
(and
(Flag324-)
(Flag324prime-)
)

)
(:action Flag325Action
:parameters ()
:precondition
(and
(ABOVEOF3-ROBOT)
(BELOWOF1-ROBOT)
)
:effect
(and
(Flag325-)
(Flag325prime-)
)

)
(:action Flag326Action
:parameters ()
:precondition
(and
(BELOWOF1-ROBOT)
(ABOVEOF9-ROBOT)
)
:effect
(and
(Flag326-)
(Flag326prime-)
)

)
(:action Flag327Action
:parameters ()
:precondition
(and
(ABOVEOF10-ROBOT)
(BELOWOF2-ROBOT)
)
:effect
(and
(Flag327-)
(Flag327prime-)
)

)
(:action Flag328Action
:parameters ()
:precondition
(and
(ABOVEOF8-ROBOT)
(BELOWOF2-ROBOT)
)
:effect
(and
(Flag328-)
(Flag328prime-)
)

)
(:action Flag329Action
:parameters ()
:precondition
(and
(BELOWOF1-ROBOT)
(ABOVEOF4-ROBOT)
)
:effect
(and
(Flag329-)
(Flag329prime-)
)

)
(:action Flag330Action
:parameters ()
:precondition
(and
(ABOVEOF12-ROBOT)
(BELOWOF2-ROBOT)
)
:effect
(and
(Flag330-)
(Flag330prime-)
)

)
(:action Flag331Action
:parameters ()
:precondition
(and
(ABOVEOF12-ROBOT)
(BELOWOF1-ROBOT)
)
:effect
(and
(Flag331-)
(Flag331prime-)
)

)
(:action Flag332Action
:parameters ()
:precondition
(and
(BELOWOF2-ROBOT)
(ABOVEOF5-ROBOT)
)
:effect
(and
(Flag332-)
(Flag332prime-)
)

)
(:action Flag333Action
:parameters ()
:precondition
(and
(BELOWOF1-ROBOT)
(ABOVEOF15-ROBOT)
)
:effect
(and
(Flag333-)
(Flag333prime-)
)

)
(:action Flag334Action
:parameters ()
:precondition
(and
(ABOVEOF8-ROBOT)
(BELOWOF1-ROBOT)
)
:effect
(and
(Flag334-)
(Flag334prime-)
)

)
(:action Flag335Action
:parameters ()
:precondition
(and
(ABOVEOF3-ROBOT)
(BELOWOF2-ROBOT)
)
:effect
(and
(Flag335-)
(Flag335prime-)
)

)
(:action Prim299Action-0
:parameters ()
:precondition
(and
(not (ABOVEOF17-ROBOT))
)
:effect
(and
(Flag299prime-)
(not (Flag299-))
)

)
(:action Prim299Action-1
:parameters ()
:precondition
(and
(not (BELOWOF1-ROBOT))
)
:effect
(and
(Flag299prime-)
(not (Flag299-))
)

)
(:action Prim300Action-0
:parameters ()
:precondition
(and
(not (ABOVEOF18-ROBOT))
)
:effect
(and
(Flag300prime-)
(not (Flag300-))
)

)
(:action Prim300Action-1
:parameters ()
:precondition
(and
(not (BELOWOF1-ROBOT))
)
:effect
(and
(Flag300prime-)
(not (Flag300-))
)

)
(:action Prim301Action-0
:parameters ()
:precondition
(and
(not (BELOWOF1-ROBOT))
)
:effect
(and
(Flag301prime-)
(not (Flag301-))
)

)
(:action Prim301Action-1
:parameters ()
:precondition
(and
(not (ABOVEOF1-ROBOT))
)
:effect
(and
(Flag301prime-)
(not (Flag301-))
)

)
(:action Prim302Action-0
:parameters ()
:precondition
(and
(not (ABOVEOF18-ROBOT))
)
:effect
(and
(Flag302prime-)
(not (Flag302-))
)

)
(:action Prim302Action-1
:parameters ()
:precondition
(and
(not (BELOWOF2-ROBOT))
)
:effect
(and
(Flag302prime-)
(not (Flag302-))
)

)
(:action Prim303Action-0
:parameters ()
:precondition
(and
(not (ABOVEOF11-ROBOT))
)
:effect
(and
(Flag303prime-)
(not (Flag303-))
)

)
(:action Prim303Action-1
:parameters ()
:precondition
(and
(not (BELOWOF1-ROBOT))
)
:effect
(and
(Flag303prime-)
(not (Flag303-))
)

)
(:action Prim304Action-0
:parameters ()
:precondition
(and
(not (ABOVEOF1-ROBOT))
)
:effect
(and
(Flag304prime-)
(not (Flag304-))
)

)
(:action Prim304Action-1
:parameters ()
:precondition
(and
(not (BELOWOF2-ROBOT))
)
:effect
(and
(Flag304prime-)
(not (Flag304-))
)

)
(:action Prim305Action-0
:parameters ()
:precondition
(and
(not (ABOVEOF7-ROBOT))
)
:effect
(and
(Flag305prime-)
(not (Flag305-))
)

)
(:action Prim305Action-1
:parameters ()
:precondition
(and
(not (BELOWOF2-ROBOT))
)
:effect
(and
(Flag305prime-)
(not (Flag305-))
)

)
(:action Prim306Action-0
:parameters ()
:precondition
(and
(not (ABOVEOF15-ROBOT))
)
:effect
(and
(Flag306prime-)
(not (Flag306-))
)

)
(:action Prim306Action-1
:parameters ()
:precondition
(and
(not (BELOWOF2-ROBOT))
)
:effect
(and
(Flag306prime-)
(not (Flag306-))
)

)
(:action Prim307Action-0
:parameters ()
:precondition
(and
(not (BELOWOF2-ROBOT))
)
:effect
(and
(Flag307prime-)
(not (Flag307-))
)

)
(:action Prim307Action-1
:parameters ()
:precondition
(and
(not (ABOVEOF13-ROBOT))
)
:effect
(and
(Flag307prime-)
(not (Flag307-))
)

)
(:action Prim308Action-0
:parameters ()
:precondition
(and
(not (BELOWOF1-ROBOT))
)
:effect
(and
(Flag308prime-)
(not (Flag308-))
)

)
(:action Prim308Action-1
:parameters ()
:precondition
(and
(not (ABOVEOF16-ROBOT))
)
:effect
(and
(Flag308prime-)
(not (Flag308-))
)

)
(:action Prim309Action-0
:parameters ()
:precondition
(and
(not (BELOWOF1-ROBOT))
)
:effect
(and
(Flag309prime-)
(not (Flag309-))
)

)
(:action Prim309Action-1
:parameters ()
:precondition
(and
(not (ABOVEOF7-ROBOT))
)
:effect
(and
(Flag309prime-)
(not (Flag309-))
)

)
(:action Prim310Action-0
:parameters ()
:precondition
(and
(not (ABOVEOF2-ROBOT))
)
:effect
(and
(Flag310prime-)
(not (Flag310-))
)

)
(:action Prim310Action-1
:parameters ()
:precondition
(and
(not (BELOWOF2-ROBOT))
)
:effect
(and
(Flag310prime-)
(not (Flag310-))
)

)
(:action Prim311Action-0
:parameters ()
:precondition
(and
(not (BELOWOF1-ROBOT))
)
:effect
(and
(Flag311prime-)
(not (Flag311-))
)

)
(:action Prim311Action-1
:parameters ()
:precondition
(and
(not (ABOVEOF5-ROBOT))
)
:effect
(and
(Flag311prime-)
(not (Flag311-))
)

)
(:action Prim312Action-0
:parameters ()
:precondition
(and
(not (ABOVEOF16-ROBOT))
)
:effect
(and
(Flag312prime-)
(not (Flag312-))
)

)
(:action Prim312Action-1
:parameters ()
:precondition
(and
(not (BELOWOF2-ROBOT))
)
:effect
(and
(Flag312prime-)
(not (Flag312-))
)

)
(:action Prim313Action-0
:parameters ()
:precondition
(and
(not (ABOVEOF4-ROBOT))
)
:effect
(and
(Flag313prime-)
(not (Flag313-))
)

)
(:action Prim313Action-1
:parameters ()
:precondition
(and
(not (BELOWOF2-ROBOT))
)
:effect
(and
(Flag313prime-)
(not (Flag313-))
)

)
(:action Prim314Action-0
:parameters ()
:precondition
(and
(not (BELOWOF1-ROBOT))
)
:effect
(and
(Flag314prime-)
(not (Flag314-))
)

)
(:action Prim314Action-1
:parameters ()
:precondition
(and
(not (ABOVEOF17-ROBOT))
)
:effect
(and
(Flag314prime-)
(not (Flag314-))
)

)
(:action Prim315Action-0
:parameters ()
:precondition
(and
(not (BELOWOF2-ROBOT))
)
:effect
(and
(Flag315prime-)
(not (Flag315-))
)

)
(:action Prim315Action-1
:parameters ()
:precondition
(and
(not (ABOVEOF14-ROBOT))
)
:effect
(and
(Flag315prime-)
(not (Flag315-))
)

)
(:action Prim316Action-0
:parameters ()
:precondition
(and
(not (ABOVEOF6-ROBOT))
)
:effect
(and
(Flag316prime-)
(not (Flag316-))
)

)
(:action Prim316Action-1
:parameters ()
:precondition
(and
(not (BELOWOF2-ROBOT))
)
:effect
(and
(Flag316prime-)
(not (Flag316-))
)

)
(:action Prim317Action-0
:parameters ()
:precondition
(and
(not (BELOWOF1-ROBOT))
)
:effect
(and
(Flag317prime-)
(not (Flag317-))
)

)
(:action Prim317Action-1
:parameters ()
:precondition
(and
(not (ABOVEOF2-ROBOT))
)
:effect
(and
(Flag317prime-)
(not (Flag317-))
)

)
(:action Prim318Action-0
:parameters ()
:precondition
(and
(not (ABOVEOF9-ROBOT))
)
:effect
(and
(Flag318prime-)
(not (Flag318-))
)

)
(:action Prim318Action-1
:parameters ()
:precondition
(and
(not (BELOWOF2-ROBOT))
)
:effect
(and
(Flag318prime-)
(not (Flag318-))
)

)
(:action Prim319Action-0
:parameters ()
:precondition
(and
(not (BELOWOF1-ROBOT))
)
:effect
(and
(Flag319prime-)
(not (Flag319-))
)

)
(:action Prim319Action-1
:parameters ()
:precondition
(and
(not (ABOVEOF13-ROBOT))
)
:effect
(and
(Flag319prime-)
(not (Flag319-))
)

)
(:action Prim320Action-0
:parameters ()
:precondition
(and
(not (ABOVEOF6-ROBOT))
)
:effect
(and
(Flag320prime-)
(not (Flag320-))
)

)
(:action Prim320Action-1
:parameters ()
:precondition
(and
(not (BELOWOF1-ROBOT))
)
:effect
(and
(Flag320prime-)
(not (Flag320-))
)

)
(:action Prim321Action-0
:parameters ()
:precondition
(and
(not (BELOWOF1-ROBOT))
)
:effect
(and
(Flag321prime-)
(not (Flag321-))
)

)
(:action Prim321Action-1
:parameters ()
:precondition
(and
(not (ABOVEOF14-ROBOT))
)
:effect
(and
(Flag321prime-)
(not (Flag321-))
)

)
(:action Prim322Action-0
:parameters ()
:precondition
(and
(not (ABOVEOF17-ROBOT))
)
:effect
(and
(Flag322prime-)
(not (Flag322-))
)

)
(:action Prim322Action-1
:parameters ()
:precondition
(and
(not (BELOWOF2-ROBOT))
)
:effect
(and
(Flag322prime-)
(not (Flag322-))
)

)
(:action Prim323Action-0
:parameters ()
:precondition
(and
(not (ABOVEOF10-ROBOT))
)
:effect
(and
(Flag323prime-)
(not (Flag323-))
)

)
(:action Prim323Action-1
:parameters ()
:precondition
(and
(not (BELOWOF1-ROBOT))
)
:effect
(and
(Flag323prime-)
(not (Flag323-))
)

)
(:action Prim324Action-0
:parameters ()
:precondition
(and
(not (ABOVEOF11-ROBOT))
)
:effect
(and
(Flag324prime-)
(not (Flag324-))
)

)
(:action Prim324Action-1
:parameters ()
:precondition
(and
(not (BELOWOF2-ROBOT))
)
:effect
(and
(Flag324prime-)
(not (Flag324-))
)

)
(:action Prim325Action-0
:parameters ()
:precondition
(and
(not (ABOVEOF3-ROBOT))
)
:effect
(and
(Flag325prime-)
(not (Flag325-))
)

)
(:action Prim325Action-1
:parameters ()
:precondition
(and
(not (BELOWOF1-ROBOT))
)
:effect
(and
(Flag325prime-)
(not (Flag325-))
)

)
(:action Prim326Action-0
:parameters ()
:precondition
(and
(not (BELOWOF1-ROBOT))
)
:effect
(and
(Flag326prime-)
(not (Flag326-))
)

)
(:action Prim326Action-1
:parameters ()
:precondition
(and
(not (ABOVEOF9-ROBOT))
)
:effect
(and
(Flag326prime-)
(not (Flag326-))
)

)
(:action Prim327Action-0
:parameters ()
:precondition
(and
(not (ABOVEOF10-ROBOT))
)
:effect
(and
(Flag327prime-)
(not (Flag327-))
)

)
(:action Prim327Action-1
:parameters ()
:precondition
(and
(not (BELOWOF2-ROBOT))
)
:effect
(and
(Flag327prime-)
(not (Flag327-))
)

)
(:action Prim328Action-0
:parameters ()
:precondition
(and
(not (ABOVEOF8-ROBOT))
)
:effect
(and
(Flag328prime-)
(not (Flag328-))
)

)
(:action Prim328Action-1
:parameters ()
:precondition
(and
(not (BELOWOF2-ROBOT))
)
:effect
(and
(Flag328prime-)
(not (Flag328-))
)

)
(:action Prim329Action-0
:parameters ()
:precondition
(and
(not (BELOWOF1-ROBOT))
)
:effect
(and
(Flag329prime-)
(not (Flag329-))
)

)
(:action Prim329Action-1
:parameters ()
:precondition
(and
(not (ABOVEOF4-ROBOT))
)
:effect
(and
(Flag329prime-)
(not (Flag329-))
)

)
(:action Prim330Action-0
:parameters ()
:precondition
(and
(not (ABOVEOF12-ROBOT))
)
:effect
(and
(Flag330prime-)
(not (Flag330-))
)

)
(:action Prim330Action-1
:parameters ()
:precondition
(and
(not (BELOWOF2-ROBOT))
)
:effect
(and
(Flag330prime-)
(not (Flag330-))
)

)
(:action Prim331Action-0
:parameters ()
:precondition
(and
(not (ABOVEOF12-ROBOT))
)
:effect
(and
(Flag331prime-)
(not (Flag331-))
)

)
(:action Prim331Action-1
:parameters ()
:precondition
(and
(not (BELOWOF1-ROBOT))
)
:effect
(and
(Flag331prime-)
(not (Flag331-))
)

)
(:action Prim332Action-0
:parameters ()
:precondition
(and
(not (BELOWOF2-ROBOT))
)
:effect
(and
(Flag332prime-)
(not (Flag332-))
)

)
(:action Prim332Action-1
:parameters ()
:precondition
(and
(not (ABOVEOF5-ROBOT))
)
:effect
(and
(Flag332prime-)
(not (Flag332-))
)

)
(:action Prim333Action-0
:parameters ()
:precondition
(and
(not (BELOWOF1-ROBOT))
)
:effect
(and
(Flag333prime-)
(not (Flag333-))
)

)
(:action Prim333Action-1
:parameters ()
:precondition
(and
(not (ABOVEOF15-ROBOT))
)
:effect
(and
(Flag333prime-)
(not (Flag333-))
)

)
(:action Prim334Action-0
:parameters ()
:precondition
(and
(not (ABOVEOF8-ROBOT))
)
:effect
(and
(Flag334prime-)
(not (Flag334-))
)

)
(:action Prim334Action-1
:parameters ()
:precondition
(and
(not (BELOWOF1-ROBOT))
)
:effect
(and
(Flag334prime-)
(not (Flag334-))
)

)
(:action Prim335Action-0
:parameters ()
:precondition
(and
(not (ABOVEOF3-ROBOT))
)
:effect
(and
(Flag335prime-)
(not (Flag335-))
)

)
(:action Prim335Action-1
:parameters ()
:precondition
(and
(not (BELOWOF2-ROBOT))
)
:effect
(and
(Flag335prime-)
(not (Flag335-))
)

)
(:action Flag337Action
:parameters ()
:precondition
(and
(BELOWOF3-ROBOT)
(ABOVEOF5-ROBOT)
)
:effect
(and
(Flag337-)
(Flag337prime-)
)

)
(:action Flag338Action
:parameters ()
:precondition
(and
(BELOWOF3-ROBOT)
(ABOVEOF6-ROBOT)
)
:effect
(and
(Flag338-)
(Flag338prime-)
)

)
(:action Flag339Action
:parameters ()
:precondition
(and
(BELOWOF3-ROBOT)
(ABOVEOF7-ROBOT)
)
:effect
(and
(Flag339-)
(Flag339prime-)
)

)
(:action Flag340Action
:parameters ()
:precondition
(and
(BELOWOF3-ROBOT)
(ABOVEOF17-ROBOT)
)
:effect
(and
(Flag340-)
(Flag340prime-)
)

)
(:action Flag341Action
:parameters ()
:precondition
(and
(BELOWOF3-ROBOT)
(ABOVEOF14-ROBOT)
)
:effect
(and
(Flag341-)
(Flag341prime-)
)

)
(:action Flag342Action
:parameters ()
:precondition
(and
(BELOWOF3-ROBOT)
(ABOVEOF16-ROBOT)
)
:effect
(and
(Flag342-)
(Flag342prime-)
)

)
(:action Flag343Action
:parameters ()
:precondition
(and
(BELOWOF3-ROBOT)
(ABOVEOF9-ROBOT)
)
:effect
(and
(Flag343-)
(Flag343prime-)
)

)
(:action Flag344Action
:parameters ()
:precondition
(and
(BELOWOF3-ROBOT)
(ABOVEOF15-ROBOT)
)
:effect
(and
(Flag344-)
(Flag344prime-)
)

)
(:action Flag345Action
:parameters ()
:precondition
(and
(BELOWOF3-ROBOT)
(ABOVEOF2-ROBOT)
)
:effect
(and
(Flag345-)
(Flag345prime-)
)

)
(:action Flag346Action
:parameters ()
:precondition
(and
(ABOVEOF3-ROBOT)
(BELOWOF3-ROBOT)
)
:effect
(and
(Flag346-)
(Flag346prime-)
)

)
(:action Flag347Action
:parameters ()
:precondition
(and
(ABOVEOF8-ROBOT)
(BELOWOF3-ROBOT)
)
:effect
(and
(Flag347-)
(Flag347prime-)
)

)
(:action Flag348Action
:parameters ()
:precondition
(and
(BELOWOF3-ROBOT)
(ABOVEOF4-ROBOT)
)
:effect
(and
(Flag348-)
(Flag348prime-)
)

)
(:action Flag349Action
:parameters ()
:precondition
(and
(BELOWOF3-ROBOT)
(ABOVEOF10-ROBOT)
)
:effect
(and
(Flag349-)
(Flag349prime-)
)

)
(:action Flag350Action
:parameters ()
:precondition
(and
(BELOWOF2-ROBOT)
(ABOVEOF2-ROBOT)
)
:effect
(and
(Flag350-)
(Flag350prime-)
)

)
(:action Flag351Action
:parameters ()
:precondition
(and
(ABOVEOF12-ROBOT)
(BELOWOF3-ROBOT)
)
:effect
(and
(Flag351-)
(Flag351prime-)
)

)
(:action Flag352Action
:parameters ()
:precondition
(and
(BELOWOF3-ROBOT)
(ABOVEOF13-ROBOT)
)
:effect
(and
(Flag352-)
(Flag352prime-)
)

)
(:action Flag353Action
:parameters ()
:precondition
(and
(ABOVEOF11-ROBOT)
(BELOWOF3-ROBOT)
)
:effect
(and
(Flag353-)
(Flag353prime-)
)

)
(:action Flag354Action
:parameters ()
:precondition
(and
(BELOWOF3-ROBOT)
(ABOVEOF18-ROBOT)
)
:effect
(and
(Flag354-)
(Flag354prime-)
)

)
(:action Prim337Action-0
:parameters ()
:precondition
(and
(not (BELOWOF3-ROBOT))
)
:effect
(and
(Flag337prime-)
(not (Flag337-))
)

)
(:action Prim337Action-1
:parameters ()
:precondition
(and
(not (ABOVEOF5-ROBOT))
)
:effect
(and
(Flag337prime-)
(not (Flag337-))
)

)
(:action Prim338Action-0
:parameters ()
:precondition
(and
(not (BELOWOF3-ROBOT))
)
:effect
(and
(Flag338prime-)
(not (Flag338-))
)

)
(:action Prim338Action-1
:parameters ()
:precondition
(and
(not (ABOVEOF6-ROBOT))
)
:effect
(and
(Flag338prime-)
(not (Flag338-))
)

)
(:action Prim339Action-0
:parameters ()
:precondition
(and
(not (BELOWOF3-ROBOT))
)
:effect
(and
(Flag339prime-)
(not (Flag339-))
)

)
(:action Prim339Action-1
:parameters ()
:precondition
(and
(not (ABOVEOF7-ROBOT))
)
:effect
(and
(Flag339prime-)
(not (Flag339-))
)

)
(:action Prim340Action-0
:parameters ()
:precondition
(and
(not (BELOWOF3-ROBOT))
)
:effect
(and
(Flag340prime-)
(not (Flag340-))
)

)
(:action Prim340Action-1
:parameters ()
:precondition
(and
(not (ABOVEOF17-ROBOT))
)
:effect
(and
(Flag340prime-)
(not (Flag340-))
)

)
(:action Prim341Action-0
:parameters ()
:precondition
(and
(not (BELOWOF3-ROBOT))
)
:effect
(and
(Flag341prime-)
(not (Flag341-))
)

)
(:action Prim341Action-1
:parameters ()
:precondition
(and
(not (ABOVEOF14-ROBOT))
)
:effect
(and
(Flag341prime-)
(not (Flag341-))
)

)
(:action Prim342Action-0
:parameters ()
:precondition
(and
(not (BELOWOF3-ROBOT))
)
:effect
(and
(Flag342prime-)
(not (Flag342-))
)

)
(:action Prim342Action-1
:parameters ()
:precondition
(and
(not (ABOVEOF16-ROBOT))
)
:effect
(and
(Flag342prime-)
(not (Flag342-))
)

)
(:action Prim343Action-0
:parameters ()
:precondition
(and
(not (BELOWOF3-ROBOT))
)
:effect
(and
(Flag343prime-)
(not (Flag343-))
)

)
(:action Prim343Action-1
:parameters ()
:precondition
(and
(not (ABOVEOF9-ROBOT))
)
:effect
(and
(Flag343prime-)
(not (Flag343-))
)

)
(:action Prim344Action-0
:parameters ()
:precondition
(and
(not (BELOWOF3-ROBOT))
)
:effect
(and
(Flag344prime-)
(not (Flag344-))
)

)
(:action Prim344Action-1
:parameters ()
:precondition
(and
(not (ABOVEOF15-ROBOT))
)
:effect
(and
(Flag344prime-)
(not (Flag344-))
)

)
(:action Prim345Action-0
:parameters ()
:precondition
(and
(not (BELOWOF3-ROBOT))
)
:effect
(and
(Flag345prime-)
(not (Flag345-))
)

)
(:action Prim345Action-1
:parameters ()
:precondition
(and
(not (ABOVEOF2-ROBOT))
)
:effect
(and
(Flag345prime-)
(not (Flag345-))
)

)
(:action Prim346Action-0
:parameters ()
:precondition
(and
(not (ABOVEOF3-ROBOT))
)
:effect
(and
(Flag346prime-)
(not (Flag346-))
)

)
(:action Prim346Action-1
:parameters ()
:precondition
(and
(not (BELOWOF3-ROBOT))
)
:effect
(and
(Flag346prime-)
(not (Flag346-))
)

)
(:action Prim347Action-0
:parameters ()
:precondition
(and
(not (ABOVEOF8-ROBOT))
)
:effect
(and
(Flag347prime-)
(not (Flag347-))
)

)
(:action Prim347Action-1
:parameters ()
:precondition
(and
(not (BELOWOF3-ROBOT))
)
:effect
(and
(Flag347prime-)
(not (Flag347-))
)

)
(:action Prim348Action-0
:parameters ()
:precondition
(and
(not (BELOWOF3-ROBOT))
)
:effect
(and
(Flag348prime-)
(not (Flag348-))
)

)
(:action Prim348Action-1
:parameters ()
:precondition
(and
(not (ABOVEOF4-ROBOT))
)
:effect
(and
(Flag348prime-)
(not (Flag348-))
)

)
(:action Prim349Action-0
:parameters ()
:precondition
(and
(not (BELOWOF3-ROBOT))
)
:effect
(and
(Flag349prime-)
(not (Flag349-))
)

)
(:action Prim349Action-1
:parameters ()
:precondition
(and
(not (ABOVEOF10-ROBOT))
)
:effect
(and
(Flag349prime-)
(not (Flag349-))
)

)
(:action Prim350Action-0
:parameters ()
:precondition
(and
(not (BELOWOF2-ROBOT))
)
:effect
(and
(Flag350prime-)
(not (Flag350-))
)

)
(:action Prim350Action-1
:parameters ()
:precondition
(and
(not (ABOVEOF2-ROBOT))
)
:effect
(and
(Flag350prime-)
(not (Flag350-))
)

)
(:action Prim351Action-0
:parameters ()
:precondition
(and
(not (ABOVEOF12-ROBOT))
)
:effect
(and
(Flag351prime-)
(not (Flag351-))
)

)
(:action Prim351Action-1
:parameters ()
:precondition
(and
(not (BELOWOF3-ROBOT))
)
:effect
(and
(Flag351prime-)
(not (Flag351-))
)

)
(:action Prim352Action-0
:parameters ()
:precondition
(and
(not (BELOWOF3-ROBOT))
)
:effect
(and
(Flag352prime-)
(not (Flag352-))
)

)
(:action Prim352Action-1
:parameters ()
:precondition
(and
(not (ABOVEOF13-ROBOT))
)
:effect
(and
(Flag352prime-)
(not (Flag352-))
)

)
(:action Prim353Action-0
:parameters ()
:precondition
(and
(not (ABOVEOF11-ROBOT))
)
:effect
(and
(Flag353prime-)
(not (Flag353-))
)

)
(:action Prim353Action-1
:parameters ()
:precondition
(and
(not (BELOWOF3-ROBOT))
)
:effect
(and
(Flag353prime-)
(not (Flag353-))
)

)
(:action Prim354Action-0
:parameters ()
:precondition
(and
(not (BELOWOF3-ROBOT))
)
:effect
(and
(Flag354prime-)
(not (Flag354-))
)

)
(:action Prim354Action-1
:parameters ()
:precondition
(and
(not (ABOVEOF18-ROBOT))
)
:effect
(and
(Flag354prime-)
(not (Flag354-))
)

)
(:action Flag356Action
:parameters ()
:precondition
(and
(ABOVEOF9-ROBOT)
(BELOWOF4-ROBOT)
)
:effect
(and
(Flag356-)
(Flag356prime-)
)

)
(:action Flag357Action
:parameters ()
:precondition
(and
(BELOWOF4-ROBOT)
(ABOVEOF13-ROBOT)
)
:effect
(and
(Flag357-)
(Flag357prime-)
)

)
(:action Flag358Action
:parameters ()
:precondition
(and
(ABOVEOF3-ROBOT)
(BELOWOF4-ROBOT)
)
:effect
(and
(Flag358-)
(Flag358prime-)
)

)
(:action Flag359Action
:parameters ()
:precondition
(and
(ABOVEOF17-ROBOT)
(BELOWOF4-ROBOT)
)
:effect
(and
(Flag359-)
(Flag359prime-)
)

)
(:action Flag360Action
:parameters ()
:precondition
(and
(ABOVEOF6-ROBOT)
(BELOWOF4-ROBOT)
)
:effect
(and
(Flag360-)
(Flag360prime-)
)

)
(:action Flag361Action
:parameters ()
:precondition
(and
(BELOWOF4-ROBOT)
(ABOVEOF4-ROBOT)
)
:effect
(and
(Flag361-)
(Flag361prime-)
)

)
(:action Flag362Action
:parameters ()
:precondition
(and
(ABOVEOF18-ROBOT)
(BELOWOF4-ROBOT)
)
:effect
(and
(Flag362-)
(Flag362prime-)
)

)
(:action Flag363Action
:parameters ()
:precondition
(and
(BELOWOF4-ROBOT)
(ABOVEOF16-ROBOT)
)
:effect
(and
(Flag363-)
(Flag363prime-)
)

)
(:action Flag364Action
:parameters ()
:precondition
(and
(ABOVEOF8-ROBOT)
(BELOWOF4-ROBOT)
)
:effect
(and
(Flag364-)
(Flag364prime-)
)

)
(:action Flag365Action
:parameters ()
:precondition
(and
(ABOVEOF12-ROBOT)
(BELOWOF4-ROBOT)
)
:effect
(and
(Flag365-)
(Flag365prime-)
)

)
(:action Flag366Action
:parameters ()
:precondition
(and
(BELOWOF4-ROBOT)
(ABOVEOF5-ROBOT)
)
:effect
(and
(Flag366-)
(Flag366prime-)
)

)
(:action Flag367Action
:parameters ()
:precondition
(and
(BELOWOF3-ROBOT)
(ABOVEOF3-ROBOT)
)
:effect
(and
(Flag367-)
(Flag367prime-)
)

)
(:action Flag368Action
:parameters ()
:precondition
(and
(ABOVEOF10-ROBOT)
(BELOWOF4-ROBOT)
)
:effect
(and
(Flag368-)
(Flag368prime-)
)

)
(:action Flag369Action
:parameters ()
:precondition
(and
(BELOWOF4-ROBOT)
(ABOVEOF15-ROBOT)
)
:effect
(and
(Flag369-)
(Flag369prime-)
)

)
(:action Flag370Action
:parameters ()
:precondition
(and
(BELOWOF3-ROBOT)
(ABOVEOF11-ROBOT)
)
:effect
(and
(Flag370-)
(Flag370prime-)
)

)
(:action Flag371Action
:parameters ()
:precondition
(and
(BELOWOF4-ROBOT)
(ABOVEOF7-ROBOT)
)
:effect
(and
(Flag371-)
(Flag371prime-)
)

)
(:action Flag372Action
:parameters ()
:precondition
(and
(ABOVEOF11-ROBOT)
(BELOWOF4-ROBOT)
)
:effect
(and
(Flag372-)
(Flag372prime-)
)

)
(:action Flag373Action
:parameters ()
:precondition
(and
(BELOWOF4-ROBOT)
(ABOVEOF14-ROBOT)
)
:effect
(and
(Flag373-)
(Flag373prime-)
)

)
(:action Prim356Action-0
:parameters ()
:precondition
(and
(not (ABOVEOF9-ROBOT))
)
:effect
(and
(Flag356prime-)
(not (Flag356-))
)

)
(:action Prim356Action-1
:parameters ()
:precondition
(and
(not (BELOWOF4-ROBOT))
)
:effect
(and
(Flag356prime-)
(not (Flag356-))
)

)
(:action Prim357Action-0
:parameters ()
:precondition
(and
(not (BELOWOF4-ROBOT))
)
:effect
(and
(Flag357prime-)
(not (Flag357-))
)

)
(:action Prim357Action-1
:parameters ()
:precondition
(and
(not (ABOVEOF13-ROBOT))
)
:effect
(and
(Flag357prime-)
(not (Flag357-))
)

)
(:action Prim358Action-0
:parameters ()
:precondition
(and
(not (ABOVEOF3-ROBOT))
)
:effect
(and
(Flag358prime-)
(not (Flag358-))
)

)
(:action Prim358Action-1
:parameters ()
:precondition
(and
(not (BELOWOF4-ROBOT))
)
:effect
(and
(Flag358prime-)
(not (Flag358-))
)

)
(:action Prim359Action-0
:parameters ()
:precondition
(and
(not (ABOVEOF17-ROBOT))
)
:effect
(and
(Flag359prime-)
(not (Flag359-))
)

)
(:action Prim359Action-1
:parameters ()
:precondition
(and
(not (BELOWOF4-ROBOT))
)
:effect
(and
(Flag359prime-)
(not (Flag359-))
)

)
(:action Prim360Action-0
:parameters ()
:precondition
(and
(not (ABOVEOF6-ROBOT))
)
:effect
(and
(Flag360prime-)
(not (Flag360-))
)

)
(:action Prim360Action-1
:parameters ()
:precondition
(and
(not (BELOWOF4-ROBOT))
)
:effect
(and
(Flag360prime-)
(not (Flag360-))
)

)
(:action Prim361Action-0
:parameters ()
:precondition
(and
(not (BELOWOF4-ROBOT))
)
:effect
(and
(Flag361prime-)
(not (Flag361-))
)

)
(:action Prim361Action-1
:parameters ()
:precondition
(and
(not (ABOVEOF4-ROBOT))
)
:effect
(and
(Flag361prime-)
(not (Flag361-))
)

)
(:action Prim362Action-0
:parameters ()
:precondition
(and
(not (ABOVEOF18-ROBOT))
)
:effect
(and
(Flag362prime-)
(not (Flag362-))
)

)
(:action Prim362Action-1
:parameters ()
:precondition
(and
(not (BELOWOF4-ROBOT))
)
:effect
(and
(Flag362prime-)
(not (Flag362-))
)

)
(:action Prim363Action-0
:parameters ()
:precondition
(and
(not (BELOWOF4-ROBOT))
)
:effect
(and
(Flag363prime-)
(not (Flag363-))
)

)
(:action Prim363Action-1
:parameters ()
:precondition
(and
(not (ABOVEOF16-ROBOT))
)
:effect
(and
(Flag363prime-)
(not (Flag363-))
)

)
(:action Prim364Action-0
:parameters ()
:precondition
(and
(not (ABOVEOF8-ROBOT))
)
:effect
(and
(Flag364prime-)
(not (Flag364-))
)

)
(:action Prim364Action-1
:parameters ()
:precondition
(and
(not (BELOWOF4-ROBOT))
)
:effect
(and
(Flag364prime-)
(not (Flag364-))
)

)
(:action Prim365Action-0
:parameters ()
:precondition
(and
(not (ABOVEOF12-ROBOT))
)
:effect
(and
(Flag365prime-)
(not (Flag365-))
)

)
(:action Prim365Action-1
:parameters ()
:precondition
(and
(not (BELOWOF4-ROBOT))
)
:effect
(and
(Flag365prime-)
(not (Flag365-))
)

)
(:action Prim366Action-0
:parameters ()
:precondition
(and
(not (BELOWOF4-ROBOT))
)
:effect
(and
(Flag366prime-)
(not (Flag366-))
)

)
(:action Prim366Action-1
:parameters ()
:precondition
(and
(not (ABOVEOF5-ROBOT))
)
:effect
(and
(Flag366prime-)
(not (Flag366-))
)

)
(:action Prim367Action-0
:parameters ()
:precondition
(and
(not (BELOWOF3-ROBOT))
)
:effect
(and
(Flag367prime-)
(not (Flag367-))
)

)
(:action Prim367Action-1
:parameters ()
:precondition
(and
(not (ABOVEOF3-ROBOT))
)
:effect
(and
(Flag367prime-)
(not (Flag367-))
)

)
(:action Prim368Action-0
:parameters ()
:precondition
(and
(not (ABOVEOF10-ROBOT))
)
:effect
(and
(Flag368prime-)
(not (Flag368-))
)

)
(:action Prim368Action-1
:parameters ()
:precondition
(and
(not (BELOWOF4-ROBOT))
)
:effect
(and
(Flag368prime-)
(not (Flag368-))
)

)
(:action Prim369Action-0
:parameters ()
:precondition
(and
(not (BELOWOF4-ROBOT))
)
:effect
(and
(Flag369prime-)
(not (Flag369-))
)

)
(:action Prim369Action-1
:parameters ()
:precondition
(and
(not (ABOVEOF15-ROBOT))
)
:effect
(and
(Flag369prime-)
(not (Flag369-))
)

)
(:action Prim370Action-0
:parameters ()
:precondition
(and
(not (BELOWOF3-ROBOT))
)
:effect
(and
(Flag370prime-)
(not (Flag370-))
)

)
(:action Prim370Action-1
:parameters ()
:precondition
(and
(not (ABOVEOF11-ROBOT))
)
:effect
(and
(Flag370prime-)
(not (Flag370-))
)

)
(:action Prim371Action-0
:parameters ()
:precondition
(and
(not (BELOWOF4-ROBOT))
)
:effect
(and
(Flag371prime-)
(not (Flag371-))
)

)
(:action Prim371Action-1
:parameters ()
:precondition
(and
(not (ABOVEOF7-ROBOT))
)
:effect
(and
(Flag371prime-)
(not (Flag371-))
)

)
(:action Prim372Action-0
:parameters ()
:precondition
(and
(not (ABOVEOF11-ROBOT))
)
:effect
(and
(Flag372prime-)
(not (Flag372-))
)

)
(:action Prim372Action-1
:parameters ()
:precondition
(and
(not (BELOWOF4-ROBOT))
)
:effect
(and
(Flag372prime-)
(not (Flag372-))
)

)
(:action Prim373Action-0
:parameters ()
:precondition
(and
(not (BELOWOF4-ROBOT))
)
:effect
(and
(Flag373prime-)
(not (Flag373-))
)

)
(:action Prim373Action-1
:parameters ()
:precondition
(and
(not (ABOVEOF14-ROBOT))
)
:effect
(and
(Flag373prime-)
(not (Flag373-))
)

)
(:action Flag375Action
:parameters ()
:precondition
(and
(ABOVEOF17-ROBOT)
(BELOWOF5-ROBOT)
)
:effect
(and
(Flag375-)
(Flag375prime-)
)

)
(:action Flag376Action
:parameters ()
:precondition
(and
(ABOVEOF4-ROBOT)
(BELOWOF5-ROBOT)
)
:effect
(and
(Flag376-)
(Flag376prime-)
)

)
(:action Flag377Action
:parameters ()
:precondition
(and
(ABOVEOF15-ROBOT)
(BELOWOF5-ROBOT)
)
:effect
(and
(Flag377-)
(Flag377prime-)
)

)
(:action Flag378Action
:parameters ()
:precondition
(and
(ABOVEOF8-ROBOT)
(BELOWOF5-ROBOT)
)
:effect
(and
(Flag378-)
(Flag378prime-)
)

)
(:action Flag379Action
:parameters ()
:precondition
(and
(ABOVEOF16-ROBOT)
(BELOWOF5-ROBOT)
)
:effect
(and
(Flag379-)
(Flag379prime-)
)

)
(:action Flag380Action
:parameters ()
:precondition
(and
(ABOVEOF10-ROBOT)
(BELOWOF5-ROBOT)
)
:effect
(and
(Flag380-)
(Flag380prime-)
)

)
(:action Flag381Action
:parameters ()
:precondition
(and
(ABOVEOF13-ROBOT)
(BELOWOF5-ROBOT)
)
:effect
(and
(Flag381-)
(Flag381prime-)
)

)
(:action Flag382Action
:parameters ()
:precondition
(and
(ABOVEOF16-ROBOT)
(BELOWOF4-ROBOT)
)
:effect
(and
(Flag382-)
(Flag382prime-)
)

)
(:action Flag383Action
:parameters ()
:precondition
(and
(ABOVEOF5-ROBOT)
(BELOWOF5-ROBOT)
)
:effect
(and
(Flag383-)
(Flag383prime-)
)

)
(:action Flag384Action
:parameters ()
:precondition
(and
(ABOVEOF7-ROBOT)
(BELOWOF5-ROBOT)
)
:effect
(and
(Flag384-)
(Flag384prime-)
)

)
(:action Flag385Action
:parameters ()
:precondition
(and
(ABOVEOF9-ROBOT)
(BELOWOF5-ROBOT)
)
:effect
(and
(Flag385-)
(Flag385prime-)
)

)
(:action Flag386Action
:parameters ()
:precondition
(and
(ABOVEOF18-ROBOT)
(BELOWOF5-ROBOT)
)
:effect
(and
(Flag386-)
(Flag386prime-)
)

)
(:action Flag387Action
:parameters ()
:precondition
(and
(ABOVEOF12-ROBOT)
(BELOWOF5-ROBOT)
)
:effect
(and
(Flag387-)
(Flag387prime-)
)

)
(:action Flag388Action
:parameters ()
:precondition
(and
(ABOVEOF11-ROBOT)
(BELOWOF5-ROBOT)
)
:effect
(and
(Flag388-)
(Flag388prime-)
)

)
(:action Flag389Action
:parameters ()
:precondition
(and
(ABOVEOF6-ROBOT)
(BELOWOF5-ROBOT)
)
:effect
(and
(Flag389-)
(Flag389prime-)
)

)
(:action Flag390Action
:parameters ()
:precondition
(and
(ABOVEOF14-ROBOT)
(BELOWOF5-ROBOT)
)
:effect
(and
(Flag390-)
(Flag390prime-)
)

)
(:action Prim375Action-0
:parameters ()
:precondition
(and
(not (ABOVEOF17-ROBOT))
)
:effect
(and
(Flag375prime-)
(not (Flag375-))
)

)
(:action Prim375Action-1
:parameters ()
:precondition
(and
(not (BELOWOF5-ROBOT))
)
:effect
(and
(Flag375prime-)
(not (Flag375-))
)

)
(:action Prim376Action-0
:parameters ()
:precondition
(and
(not (ABOVEOF4-ROBOT))
)
:effect
(and
(Flag376prime-)
(not (Flag376-))
)

)
(:action Prim376Action-1
:parameters ()
:precondition
(and
(not (BELOWOF5-ROBOT))
)
:effect
(and
(Flag376prime-)
(not (Flag376-))
)

)
(:action Prim377Action-0
:parameters ()
:precondition
(and
(not (ABOVEOF15-ROBOT))
)
:effect
(and
(Flag377prime-)
(not (Flag377-))
)

)
(:action Prim377Action-1
:parameters ()
:precondition
(and
(not (BELOWOF5-ROBOT))
)
:effect
(and
(Flag377prime-)
(not (Flag377-))
)

)
(:action Prim378Action-0
:parameters ()
:precondition
(and
(not (ABOVEOF8-ROBOT))
)
:effect
(and
(Flag378prime-)
(not (Flag378-))
)

)
(:action Prim378Action-1
:parameters ()
:precondition
(and
(not (BELOWOF5-ROBOT))
)
:effect
(and
(Flag378prime-)
(not (Flag378-))
)

)
(:action Prim379Action-0
:parameters ()
:precondition
(and
(not (ABOVEOF16-ROBOT))
)
:effect
(and
(Flag379prime-)
(not (Flag379-))
)

)
(:action Prim379Action-1
:parameters ()
:precondition
(and
(not (BELOWOF5-ROBOT))
)
:effect
(and
(Flag379prime-)
(not (Flag379-))
)

)
(:action Prim380Action-0
:parameters ()
:precondition
(and
(not (ABOVEOF10-ROBOT))
)
:effect
(and
(Flag380prime-)
(not (Flag380-))
)

)
(:action Prim380Action-1
:parameters ()
:precondition
(and
(not (BELOWOF5-ROBOT))
)
:effect
(and
(Flag380prime-)
(not (Flag380-))
)

)
(:action Prim381Action-0
:parameters ()
:precondition
(and
(not (ABOVEOF13-ROBOT))
)
:effect
(and
(Flag381prime-)
(not (Flag381-))
)

)
(:action Prim381Action-1
:parameters ()
:precondition
(and
(not (BELOWOF5-ROBOT))
)
:effect
(and
(Flag381prime-)
(not (Flag381-))
)

)
(:action Prim382Action-0
:parameters ()
:precondition
(and
(not (ABOVEOF16-ROBOT))
)
:effect
(and
(Flag382prime-)
(not (Flag382-))
)

)
(:action Prim382Action-1
:parameters ()
:precondition
(and
(not (BELOWOF4-ROBOT))
)
:effect
(and
(Flag382prime-)
(not (Flag382-))
)

)
(:action Prim383Action-0
:parameters ()
:precondition
(and
(not (ABOVEOF5-ROBOT))
)
:effect
(and
(Flag383prime-)
(not (Flag383-))
)

)
(:action Prim383Action-1
:parameters ()
:precondition
(and
(not (BELOWOF5-ROBOT))
)
:effect
(and
(Flag383prime-)
(not (Flag383-))
)

)
(:action Prim384Action-0
:parameters ()
:precondition
(and
(not (ABOVEOF7-ROBOT))
)
:effect
(and
(Flag384prime-)
(not (Flag384-))
)

)
(:action Prim384Action-1
:parameters ()
:precondition
(and
(not (BELOWOF5-ROBOT))
)
:effect
(and
(Flag384prime-)
(not (Flag384-))
)

)
(:action Prim385Action-0
:parameters ()
:precondition
(and
(not (ABOVEOF9-ROBOT))
)
:effect
(and
(Flag385prime-)
(not (Flag385-))
)

)
(:action Prim385Action-1
:parameters ()
:precondition
(and
(not (BELOWOF5-ROBOT))
)
:effect
(and
(Flag385prime-)
(not (Flag385-))
)

)
(:action Prim386Action-0
:parameters ()
:precondition
(and
(not (ABOVEOF18-ROBOT))
)
:effect
(and
(Flag386prime-)
(not (Flag386-))
)

)
(:action Prim386Action-1
:parameters ()
:precondition
(and
(not (BELOWOF5-ROBOT))
)
:effect
(and
(Flag386prime-)
(not (Flag386-))
)

)
(:action Prim387Action-0
:parameters ()
:precondition
(and
(not (ABOVEOF12-ROBOT))
)
:effect
(and
(Flag387prime-)
(not (Flag387-))
)

)
(:action Prim387Action-1
:parameters ()
:precondition
(and
(not (BELOWOF5-ROBOT))
)
:effect
(and
(Flag387prime-)
(not (Flag387-))
)

)
(:action Prim388Action-0
:parameters ()
:precondition
(and
(not (ABOVEOF11-ROBOT))
)
:effect
(and
(Flag388prime-)
(not (Flag388-))
)

)
(:action Prim388Action-1
:parameters ()
:precondition
(and
(not (BELOWOF5-ROBOT))
)
:effect
(and
(Flag388prime-)
(not (Flag388-))
)

)
(:action Prim389Action-0
:parameters ()
:precondition
(and
(not (ABOVEOF6-ROBOT))
)
:effect
(and
(Flag389prime-)
(not (Flag389-))
)

)
(:action Prim389Action-1
:parameters ()
:precondition
(and
(not (BELOWOF5-ROBOT))
)
:effect
(and
(Flag389prime-)
(not (Flag389-))
)

)
(:action Prim390Action-0
:parameters ()
:precondition
(and
(not (ABOVEOF14-ROBOT))
)
:effect
(and
(Flag390prime-)
(not (Flag390-))
)

)
(:action Prim390Action-1
:parameters ()
:precondition
(and
(not (BELOWOF5-ROBOT))
)
:effect
(and
(Flag390prime-)
(not (Flag390-))
)

)
(:action Flag392Action
:parameters ()
:precondition
(and
(BELOWOF5-ROBOT)
(ABOVEOF5-ROBOT)
)
:effect
(and
(Flag392-)
(Flag392prime-)
)

)
(:action Flag393Action
:parameters ()
:precondition
(and
(BELOWOF6-ROBOT)
(ABOVEOF18-ROBOT)
)
:effect
(and
(Flag393-)
(Flag393prime-)
)

)
(:action Flag394Action
:parameters ()
:precondition
(and
(BELOWOF6-ROBOT)
(ABOVEOF6-ROBOT)
)
:effect
(and
(Flag394-)
(Flag394prime-)
)

)
(:action Flag395Action
:parameters ()
:precondition
(and
(ABOVEOF11-ROBOT)
(BELOWOF6-ROBOT)
)
:effect
(and
(Flag395-)
(Flag395prime-)
)

)
(:action Flag396Action
:parameters ()
:precondition
(and
(ABOVEOF12-ROBOT)
(BELOWOF6-ROBOT)
)
:effect
(and
(Flag396-)
(Flag396prime-)
)

)
(:action Flag397Action
:parameters ()
:precondition
(and
(BELOWOF5-ROBOT)
(ABOVEOF13-ROBOT)
)
:effect
(and
(Flag397-)
(Flag397prime-)
)

)
(:action Flag398Action
:parameters ()
:precondition
(and
(BELOWOF6-ROBOT)
(ABOVEOF17-ROBOT)
)
:effect
(and
(Flag398-)
(Flag398prime-)
)

)
(:action Flag399Action
:parameters ()
:precondition
(and
(BELOWOF6-ROBOT)
(ABOVEOF10-ROBOT)
)
:effect
(and
(Flag399-)
(Flag399prime-)
)

)
(:action Flag400Action
:parameters ()
:precondition
(and
(BELOWOF6-ROBOT)
(ABOVEOF15-ROBOT)
)
:effect
(and
(Flag400-)
(Flag400prime-)
)

)
(:action Flag401Action
:parameters ()
:precondition
(and
(BELOWOF6-ROBOT)
(ABOVEOF14-ROBOT)
)
:effect
(and
(Flag401-)
(Flag401prime-)
)

)
(:action Flag402Action
:parameters ()
:precondition
(and
(BELOWOF6-ROBOT)
(ABOVEOF5-ROBOT)
)
:effect
(and
(Flag402-)
(Flag402prime-)
)

)
(:action Flag403Action
:parameters ()
:precondition
(and
(ABOVEOF8-ROBOT)
(BELOWOF6-ROBOT)
)
:effect
(and
(Flag403-)
(Flag403prime-)
)

)
(:action Flag404Action
:parameters ()
:precondition
(and
(BELOWOF6-ROBOT)
(ABOVEOF9-ROBOT)
)
:effect
(and
(Flag404-)
(Flag404prime-)
)

)
(:action Flag405Action
:parameters ()
:precondition
(and
(BELOWOF6-ROBOT)
(ABOVEOF16-ROBOT)
)
:effect
(and
(Flag405-)
(Flag405prime-)
)

)
(:action Flag406Action
:parameters ()
:precondition
(and
(BELOWOF6-ROBOT)
(ABOVEOF13-ROBOT)
)
:effect
(and
(Flag406-)
(Flag406prime-)
)

)
(:action Flag407Action
:parameters ()
:precondition
(and
(BELOWOF6-ROBOT)
(ABOVEOF7-ROBOT)
)
:effect
(and
(Flag407-)
(Flag407prime-)
)

)
(:action Prim392Action-0
:parameters ()
:precondition
(and
(not (BELOWOF5-ROBOT))
)
:effect
(and
(Flag392prime-)
(not (Flag392-))
)

)
(:action Prim392Action-1
:parameters ()
:precondition
(and
(not (ABOVEOF5-ROBOT))
)
:effect
(and
(Flag392prime-)
(not (Flag392-))
)

)
(:action Prim393Action-0
:parameters ()
:precondition
(and
(not (BELOWOF6-ROBOT))
)
:effect
(and
(Flag393prime-)
(not (Flag393-))
)

)
(:action Prim393Action-1
:parameters ()
:precondition
(and
(not (ABOVEOF18-ROBOT))
)
:effect
(and
(Flag393prime-)
(not (Flag393-))
)

)
(:action Prim394Action-0
:parameters ()
:precondition
(and
(not (BELOWOF6-ROBOT))
)
:effect
(and
(Flag394prime-)
(not (Flag394-))
)

)
(:action Prim394Action-1
:parameters ()
:precondition
(and
(not (ABOVEOF6-ROBOT))
)
:effect
(and
(Flag394prime-)
(not (Flag394-))
)

)
(:action Prim395Action-0
:parameters ()
:precondition
(and
(not (ABOVEOF11-ROBOT))
)
:effect
(and
(Flag395prime-)
(not (Flag395-))
)

)
(:action Prim395Action-1
:parameters ()
:precondition
(and
(not (BELOWOF6-ROBOT))
)
:effect
(and
(Flag395prime-)
(not (Flag395-))
)

)
(:action Prim396Action-0
:parameters ()
:precondition
(and
(not (ABOVEOF12-ROBOT))
)
:effect
(and
(Flag396prime-)
(not (Flag396-))
)

)
(:action Prim396Action-1
:parameters ()
:precondition
(and
(not (BELOWOF6-ROBOT))
)
:effect
(and
(Flag396prime-)
(not (Flag396-))
)

)
(:action Prim397Action-0
:parameters ()
:precondition
(and
(not (BELOWOF5-ROBOT))
)
:effect
(and
(Flag397prime-)
(not (Flag397-))
)

)
(:action Prim397Action-1
:parameters ()
:precondition
(and
(not (ABOVEOF13-ROBOT))
)
:effect
(and
(Flag397prime-)
(not (Flag397-))
)

)
(:action Prim398Action-0
:parameters ()
:precondition
(and
(not (BELOWOF6-ROBOT))
)
:effect
(and
(Flag398prime-)
(not (Flag398-))
)

)
(:action Prim398Action-1
:parameters ()
:precondition
(and
(not (ABOVEOF17-ROBOT))
)
:effect
(and
(Flag398prime-)
(not (Flag398-))
)

)
(:action Prim399Action-0
:parameters ()
:precondition
(and
(not (BELOWOF6-ROBOT))
)
:effect
(and
(Flag399prime-)
(not (Flag399-))
)

)
(:action Prim399Action-1
:parameters ()
:precondition
(and
(not (ABOVEOF10-ROBOT))
)
:effect
(and
(Flag399prime-)
(not (Flag399-))
)

)
(:action Prim400Action-0
:parameters ()
:precondition
(and
(not (BELOWOF6-ROBOT))
)
:effect
(and
(Flag400prime-)
(not (Flag400-))
)

)
(:action Prim400Action-1
:parameters ()
:precondition
(and
(not (ABOVEOF15-ROBOT))
)
:effect
(and
(Flag400prime-)
(not (Flag400-))
)

)
(:action Prim401Action-0
:parameters ()
:precondition
(and
(not (BELOWOF6-ROBOT))
)
:effect
(and
(Flag401prime-)
(not (Flag401-))
)

)
(:action Prim401Action-1
:parameters ()
:precondition
(and
(not (ABOVEOF14-ROBOT))
)
:effect
(and
(Flag401prime-)
(not (Flag401-))
)

)
(:action Prim402Action-0
:parameters ()
:precondition
(and
(not (BELOWOF6-ROBOT))
)
:effect
(and
(Flag402prime-)
(not (Flag402-))
)

)
(:action Prim402Action-1
:parameters ()
:precondition
(and
(not (ABOVEOF5-ROBOT))
)
:effect
(and
(Flag402prime-)
(not (Flag402-))
)

)
(:action Prim403Action-0
:parameters ()
:precondition
(and
(not (ABOVEOF8-ROBOT))
)
:effect
(and
(Flag403prime-)
(not (Flag403-))
)

)
(:action Prim403Action-1
:parameters ()
:precondition
(and
(not (BELOWOF6-ROBOT))
)
:effect
(and
(Flag403prime-)
(not (Flag403-))
)

)
(:action Prim404Action-0
:parameters ()
:precondition
(and
(not (BELOWOF6-ROBOT))
)
:effect
(and
(Flag404prime-)
(not (Flag404-))
)

)
(:action Prim404Action-1
:parameters ()
:precondition
(and
(not (ABOVEOF9-ROBOT))
)
:effect
(and
(Flag404prime-)
(not (Flag404-))
)

)
(:action Prim405Action-0
:parameters ()
:precondition
(and
(not (BELOWOF6-ROBOT))
)
:effect
(and
(Flag405prime-)
(not (Flag405-))
)

)
(:action Prim405Action-1
:parameters ()
:precondition
(and
(not (ABOVEOF16-ROBOT))
)
:effect
(and
(Flag405prime-)
(not (Flag405-))
)

)
(:action Prim406Action-0
:parameters ()
:precondition
(and
(not (BELOWOF6-ROBOT))
)
:effect
(and
(Flag406prime-)
(not (Flag406-))
)

)
(:action Prim406Action-1
:parameters ()
:precondition
(and
(not (ABOVEOF13-ROBOT))
)
:effect
(and
(Flag406prime-)
(not (Flag406-))
)

)
(:action Prim407Action-0
:parameters ()
:precondition
(and
(not (BELOWOF6-ROBOT))
)
:effect
(and
(Flag407prime-)
(not (Flag407-))
)

)
(:action Prim407Action-1
:parameters ()
:precondition
(and
(not (ABOVEOF7-ROBOT))
)
:effect
(and
(Flag407prime-)
(not (Flag407-))
)

)
(:action Flag409Action
:parameters ()
:precondition
(and
(ABOVEOF6-ROBOT)
(BELOWOF7-ROBOT)
)
:effect
(and
(Flag409-)
(Flag409prime-)
)

)
(:action Flag410Action
:parameters ()
:precondition
(and
(BELOWOF7-ROBOT)
(ABOVEOF14-ROBOT)
)
:effect
(and
(Flag410-)
(Flag410prime-)
)

)
(:action Flag411Action
:parameters ()
:precondition
(and
(BELOWOF7-ROBOT)
(ABOVEOF15-ROBOT)
)
:effect
(and
(Flag411-)
(Flag411prime-)
)

)
(:action Flag412Action
:parameters ()
:precondition
(and
(ABOVEOF7-ROBOT)
(BELOWOF7-ROBOT)
)
:effect
(and
(Flag412-)
(Flag412prime-)
)

)
(:action Flag413Action
:parameters ()
:precondition
(and
(ABOVEOF16-ROBOT)
(BELOWOF7-ROBOT)
)
:effect
(and
(Flag413-)
(Flag413prime-)
)

)
(:action Flag414Action
:parameters ()
:precondition
(and
(ABOVEOF18-ROBOT)
(BELOWOF7-ROBOT)
)
:effect
(and
(Flag414-)
(Flag414prime-)
)

)
(:action Flag415Action
:parameters ()
:precondition
(and
(ABOVEOF10-ROBOT)
(BELOWOF6-ROBOT)
)
:effect
(and
(Flag415-)
(Flag415prime-)
)

)
(:action Flag416Action
:parameters ()
:precondition
(and
(ABOVEOF18-ROBOT)
(BELOWOF6-ROBOT)
)
:effect
(and
(Flag416-)
(Flag416prime-)
)

)
(:action Flag417Action
:parameters ()
:precondition
(and
(ABOVEOF10-ROBOT)
(BELOWOF7-ROBOT)
)
:effect
(and
(Flag417-)
(Flag417prime-)
)

)
(:action Flag418Action
:parameters ()
:precondition
(and
(BELOWOF7-ROBOT)
(ABOVEOF13-ROBOT)
)
:effect
(and
(Flag418-)
(Flag418prime-)
)

)
(:action Flag419Action
:parameters ()
:precondition
(and
(ABOVEOF17-ROBOT)
(BELOWOF7-ROBOT)
)
:effect
(and
(Flag419-)
(Flag419prime-)
)

)
(:action Flag420Action
:parameters ()
:precondition
(and
(ABOVEOF8-ROBOT)
(BELOWOF7-ROBOT)
)
:effect
(and
(Flag420-)
(Flag420prime-)
)

)
(:action Flag421Action
:parameters ()
:precondition
(and
(ABOVEOF11-ROBOT)
(BELOWOF7-ROBOT)
)
:effect
(and
(Flag421-)
(Flag421prime-)
)

)
(:action Flag422Action
:parameters ()
:precondition
(and
(ABOVEOF9-ROBOT)
(BELOWOF7-ROBOT)
)
:effect
(and
(Flag422-)
(Flag422prime-)
)

)
(:action Flag423Action
:parameters ()
:precondition
(and
(ABOVEOF6-ROBOT)
(BELOWOF6-ROBOT)
)
:effect
(and
(Flag423-)
(Flag423prime-)
)

)
(:action Flag424Action
:parameters ()
:precondition
(and
(ABOVEOF12-ROBOT)
(BELOWOF7-ROBOT)
)
:effect
(and
(Flag424-)
(Flag424prime-)
)

)
(:action Prim409Action-0
:parameters ()
:precondition
(and
(not (ABOVEOF6-ROBOT))
)
:effect
(and
(Flag409prime-)
(not (Flag409-))
)

)
(:action Prim409Action-1
:parameters ()
:precondition
(and
(not (BELOWOF7-ROBOT))
)
:effect
(and
(Flag409prime-)
(not (Flag409-))
)

)
(:action Prim410Action-0
:parameters ()
:precondition
(and
(not (BELOWOF7-ROBOT))
)
:effect
(and
(Flag410prime-)
(not (Flag410-))
)

)
(:action Prim410Action-1
:parameters ()
:precondition
(and
(not (ABOVEOF14-ROBOT))
)
:effect
(and
(Flag410prime-)
(not (Flag410-))
)

)
(:action Prim411Action-0
:parameters ()
:precondition
(and
(not (BELOWOF7-ROBOT))
)
:effect
(and
(Flag411prime-)
(not (Flag411-))
)

)
(:action Prim411Action-1
:parameters ()
:precondition
(and
(not (ABOVEOF15-ROBOT))
)
:effect
(and
(Flag411prime-)
(not (Flag411-))
)

)
(:action Prim412Action-0
:parameters ()
:precondition
(and
(not (ABOVEOF7-ROBOT))
)
:effect
(and
(Flag412prime-)
(not (Flag412-))
)

)
(:action Prim412Action-1
:parameters ()
:precondition
(and
(not (BELOWOF7-ROBOT))
)
:effect
(and
(Flag412prime-)
(not (Flag412-))
)

)
(:action Prim413Action-0
:parameters ()
:precondition
(and
(not (ABOVEOF16-ROBOT))
)
:effect
(and
(Flag413prime-)
(not (Flag413-))
)

)
(:action Prim413Action-1
:parameters ()
:precondition
(and
(not (BELOWOF7-ROBOT))
)
:effect
(and
(Flag413prime-)
(not (Flag413-))
)

)
(:action Prim414Action-0
:parameters ()
:precondition
(and
(not (ABOVEOF18-ROBOT))
)
:effect
(and
(Flag414prime-)
(not (Flag414-))
)

)
(:action Prim414Action-1
:parameters ()
:precondition
(and
(not (BELOWOF7-ROBOT))
)
:effect
(and
(Flag414prime-)
(not (Flag414-))
)

)
(:action Prim415Action-0
:parameters ()
:precondition
(and
(not (ABOVEOF10-ROBOT))
)
:effect
(and
(Flag415prime-)
(not (Flag415-))
)

)
(:action Prim415Action-1
:parameters ()
:precondition
(and
(not (BELOWOF6-ROBOT))
)
:effect
(and
(Flag415prime-)
(not (Flag415-))
)

)
(:action Prim416Action-0
:parameters ()
:precondition
(and
(not (ABOVEOF18-ROBOT))
)
:effect
(and
(Flag416prime-)
(not (Flag416-))
)

)
(:action Prim416Action-1
:parameters ()
:precondition
(and
(not (BELOWOF6-ROBOT))
)
:effect
(and
(Flag416prime-)
(not (Flag416-))
)

)
(:action Prim417Action-0
:parameters ()
:precondition
(and
(not (ABOVEOF10-ROBOT))
)
:effect
(and
(Flag417prime-)
(not (Flag417-))
)

)
(:action Prim417Action-1
:parameters ()
:precondition
(and
(not (BELOWOF7-ROBOT))
)
:effect
(and
(Flag417prime-)
(not (Flag417-))
)

)
(:action Prim418Action-0
:parameters ()
:precondition
(and
(not (BELOWOF7-ROBOT))
)
:effect
(and
(Flag418prime-)
(not (Flag418-))
)

)
(:action Prim418Action-1
:parameters ()
:precondition
(and
(not (ABOVEOF13-ROBOT))
)
:effect
(and
(Flag418prime-)
(not (Flag418-))
)

)
(:action Prim419Action-0
:parameters ()
:precondition
(and
(not (ABOVEOF17-ROBOT))
)
:effect
(and
(Flag419prime-)
(not (Flag419-))
)

)
(:action Prim419Action-1
:parameters ()
:precondition
(and
(not (BELOWOF7-ROBOT))
)
:effect
(and
(Flag419prime-)
(not (Flag419-))
)

)
(:action Prim420Action-0
:parameters ()
:precondition
(and
(not (ABOVEOF8-ROBOT))
)
:effect
(and
(Flag420prime-)
(not (Flag420-))
)

)
(:action Prim420Action-1
:parameters ()
:precondition
(and
(not (BELOWOF7-ROBOT))
)
:effect
(and
(Flag420prime-)
(not (Flag420-))
)

)
(:action Prim421Action-0
:parameters ()
:precondition
(and
(not (ABOVEOF11-ROBOT))
)
:effect
(and
(Flag421prime-)
(not (Flag421-))
)

)
(:action Prim421Action-1
:parameters ()
:precondition
(and
(not (BELOWOF7-ROBOT))
)
:effect
(and
(Flag421prime-)
(not (Flag421-))
)

)
(:action Prim422Action-0
:parameters ()
:precondition
(and
(not (ABOVEOF9-ROBOT))
)
:effect
(and
(Flag422prime-)
(not (Flag422-))
)

)
(:action Prim422Action-1
:parameters ()
:precondition
(and
(not (BELOWOF7-ROBOT))
)
:effect
(and
(Flag422prime-)
(not (Flag422-))
)

)
(:action Prim423Action-0
:parameters ()
:precondition
(and
(not (ABOVEOF6-ROBOT))
)
:effect
(and
(Flag423prime-)
(not (Flag423-))
)

)
(:action Prim423Action-1
:parameters ()
:precondition
(and
(not (BELOWOF6-ROBOT))
)
:effect
(and
(Flag423prime-)
(not (Flag423-))
)

)
(:action Prim424Action-0
:parameters ()
:precondition
(and
(not (ABOVEOF12-ROBOT))
)
:effect
(and
(Flag424prime-)
(not (Flag424-))
)

)
(:action Prim424Action-1
:parameters ()
:precondition
(and
(not (BELOWOF7-ROBOT))
)
:effect
(and
(Flag424prime-)
(not (Flag424-))
)

)
(:action Flag426Action
:parameters ()
:precondition
(and
(BELOWOF8-ROBOT)
(ABOVEOF11-ROBOT)
)
:effect
(and
(Flag426-)
(Flag426prime-)
)

)
(:action Flag427Action
:parameters ()
:precondition
(and
(BELOWOF8-ROBOT)
(ABOVEOF14-ROBOT)
)
:effect
(and
(Flag427-)
(Flag427prime-)
)

)
(:action Flag428Action
:parameters ()
:precondition
(and
(BELOWOF8-ROBOT)
(ABOVEOF17-ROBOT)
)
:effect
(and
(Flag428-)
(Flag428prime-)
)

)
(:action Flag429Action
:parameters ()
:precondition
(and
(BELOWOF8-ROBOT)
(ABOVEOF13-ROBOT)
)
:effect
(and
(Flag429-)
(Flag429prime-)
)

)
(:action Flag430Action
:parameters ()
:precondition
(and
(BELOWOF8-ROBOT)
(ABOVEOF10-ROBOT)
)
:effect
(and
(Flag430-)
(Flag430prime-)
)

)
(:action Flag431Action
:parameters ()
:precondition
(and
(BELOWOF8-ROBOT)
(ABOVEOF18-ROBOT)
)
:effect
(and
(Flag431-)
(Flag431prime-)
)

)
(:action Flag432Action
:parameters ()
:precondition
(and
(BELOWOF8-ROBOT)
(ABOVEOF9-ROBOT)
)
:effect
(and
(Flag432-)
(Flag432prime-)
)

)
(:action Flag433Action
:parameters ()
:precondition
(and
(BELOWOF8-ROBOT)
(ABOVEOF7-ROBOT)
)
:effect
(and
(Flag433-)
(Flag433prime-)
)

)
(:action Flag434Action
:parameters ()
:precondition
(and
(ABOVEOF8-ROBOT)
(BELOWOF8-ROBOT)
)
:effect
(and
(Flag434-)
(Flag434prime-)
)

)
(:action Flag435Action
:parameters ()
:precondition
(and
(BELOWOF7-ROBOT)
(ABOVEOF7-ROBOT)
)
:effect
(and
(Flag435-)
(Flag435prime-)
)

)
(:action Flag436Action
:parameters ()
:precondition
(and
(BELOWOF8-ROBOT)
(ABOVEOF15-ROBOT)
)
:effect
(and
(Flag436-)
(Flag436prime-)
)

)
(:action Flag437Action
:parameters ()
:precondition
(and
(ABOVEOF12-ROBOT)
(BELOWOF8-ROBOT)
)
:effect
(and
(Flag437-)
(Flag437prime-)
)

)
(:action Flag438Action
:parameters ()
:precondition
(and
(BELOWOF8-ROBOT)
(ABOVEOF16-ROBOT)
)
:effect
(and
(Flag438-)
(Flag438prime-)
)

)
(:action Prim426Action-0
:parameters ()
:precondition
(and
(not (BELOWOF8-ROBOT))
)
:effect
(and
(Flag426prime-)
(not (Flag426-))
)

)
(:action Prim426Action-1
:parameters ()
:precondition
(and
(not (ABOVEOF11-ROBOT))
)
:effect
(and
(Flag426prime-)
(not (Flag426-))
)

)
(:action Prim427Action-0
:parameters ()
:precondition
(and
(not (BELOWOF8-ROBOT))
)
:effect
(and
(Flag427prime-)
(not (Flag427-))
)

)
(:action Prim427Action-1
:parameters ()
:precondition
(and
(not (ABOVEOF14-ROBOT))
)
:effect
(and
(Flag427prime-)
(not (Flag427-))
)

)
(:action Prim428Action-0
:parameters ()
:precondition
(and
(not (BELOWOF8-ROBOT))
)
:effect
(and
(Flag428prime-)
(not (Flag428-))
)

)
(:action Prim428Action-1
:parameters ()
:precondition
(and
(not (ABOVEOF17-ROBOT))
)
:effect
(and
(Flag428prime-)
(not (Flag428-))
)

)
(:action Prim429Action-0
:parameters ()
:precondition
(and
(not (BELOWOF8-ROBOT))
)
:effect
(and
(Flag429prime-)
(not (Flag429-))
)

)
(:action Prim429Action-1
:parameters ()
:precondition
(and
(not (ABOVEOF13-ROBOT))
)
:effect
(and
(Flag429prime-)
(not (Flag429-))
)

)
(:action Prim430Action-0
:parameters ()
:precondition
(and
(not (BELOWOF8-ROBOT))
)
:effect
(and
(Flag430prime-)
(not (Flag430-))
)

)
(:action Prim430Action-1
:parameters ()
:precondition
(and
(not (ABOVEOF10-ROBOT))
)
:effect
(and
(Flag430prime-)
(not (Flag430-))
)

)
(:action Prim431Action-0
:parameters ()
:precondition
(and
(not (BELOWOF8-ROBOT))
)
:effect
(and
(Flag431prime-)
(not (Flag431-))
)

)
(:action Prim431Action-1
:parameters ()
:precondition
(and
(not (ABOVEOF18-ROBOT))
)
:effect
(and
(Flag431prime-)
(not (Flag431-))
)

)
(:action Prim432Action-0
:parameters ()
:precondition
(and
(not (BELOWOF8-ROBOT))
)
:effect
(and
(Flag432prime-)
(not (Flag432-))
)

)
(:action Prim432Action-1
:parameters ()
:precondition
(and
(not (ABOVEOF9-ROBOT))
)
:effect
(and
(Flag432prime-)
(not (Flag432-))
)

)
(:action Prim433Action-0
:parameters ()
:precondition
(and
(not (BELOWOF8-ROBOT))
)
:effect
(and
(Flag433prime-)
(not (Flag433-))
)

)
(:action Prim433Action-1
:parameters ()
:precondition
(and
(not (ABOVEOF7-ROBOT))
)
:effect
(and
(Flag433prime-)
(not (Flag433-))
)

)
(:action Prim434Action-0
:parameters ()
:precondition
(and
(not (ABOVEOF8-ROBOT))
)
:effect
(and
(Flag434prime-)
(not (Flag434-))
)

)
(:action Prim434Action-1
:parameters ()
:precondition
(and
(not (BELOWOF8-ROBOT))
)
:effect
(and
(Flag434prime-)
(not (Flag434-))
)

)
(:action Prim435Action-0
:parameters ()
:precondition
(and
(not (BELOWOF7-ROBOT))
)
:effect
(and
(Flag435prime-)
(not (Flag435-))
)

)
(:action Prim435Action-1
:parameters ()
:precondition
(and
(not (ABOVEOF7-ROBOT))
)
:effect
(and
(Flag435prime-)
(not (Flag435-))
)

)
(:action Prim436Action-0
:parameters ()
:precondition
(and
(not (BELOWOF8-ROBOT))
)
:effect
(and
(Flag436prime-)
(not (Flag436-))
)

)
(:action Prim436Action-1
:parameters ()
:precondition
(and
(not (ABOVEOF15-ROBOT))
)
:effect
(and
(Flag436prime-)
(not (Flag436-))
)

)
(:action Prim437Action-0
:parameters ()
:precondition
(and
(not (ABOVEOF12-ROBOT))
)
:effect
(and
(Flag437prime-)
(not (Flag437-))
)

)
(:action Prim437Action-1
:parameters ()
:precondition
(and
(not (BELOWOF8-ROBOT))
)
:effect
(and
(Flag437prime-)
(not (Flag437-))
)

)
(:action Prim438Action-0
:parameters ()
:precondition
(and
(not (BELOWOF8-ROBOT))
)
:effect
(and
(Flag438prime-)
(not (Flag438-))
)

)
(:action Prim438Action-1
:parameters ()
:precondition
(and
(not (ABOVEOF16-ROBOT))
)
:effect
(and
(Flag438prime-)
(not (Flag438-))
)

)
(:action Flag440Action
:parameters ()
:precondition
(and
(ABOVEOF17-ROBOT)
(BELOWOF9-ROBOT)
)
:effect
(and
(Flag440-)
(Flag440prime-)
)

)
(:action Flag441Action
:parameters ()
:precondition
(and
(BELOWOF9-ROBOT)
(ABOVEOF13-ROBOT)
)
:effect
(and
(Flag441-)
(Flag441prime-)
)

)
(:action Flag442Action
:parameters ()
:precondition
(and
(ABOVEOF11-ROBOT)
(BELOWOF9-ROBOT)
)
:effect
(and
(Flag442-)
(Flag442prime-)
)

)
(:action Flag443Action
:parameters ()
:precondition
(and
(BELOWOF8-ROBOT)
(ABOVEOF12-ROBOT)
)
:effect
(and
(Flag443-)
(Flag443prime-)
)

)
(:action Flag444Action
:parameters ()
:precondition
(and
(ABOVEOF10-ROBOT)
(BELOWOF9-ROBOT)
)
:effect
(and
(Flag444-)
(Flag444prime-)
)

)
(:action Flag445Action
:parameters ()
:precondition
(and
(BELOWOF9-ROBOT)
(ABOVEOF14-ROBOT)
)
:effect
(and
(Flag445-)
(Flag445prime-)
)

)
(:action Flag446Action
:parameters ()
:precondition
(and
(BELOWOF9-ROBOT)
(ABOVEOF9-ROBOT)
)
:effect
(and
(Flag446-)
(Flag446prime-)
)

)
(:action Flag447Action
:parameters ()
:precondition
(and
(BELOWOF9-ROBOT)
(ABOVEOF16-ROBOT)
)
:effect
(and
(Flag447-)
(Flag447prime-)
)

)
(:action Flag448Action
:parameters ()
:precondition
(and
(BELOWOF8-ROBOT)
(ABOVEOF8-ROBOT)
)
:effect
(and
(Flag448-)
(Flag448prime-)
)

)
(:action Flag449Action
:parameters ()
:precondition
(and
(BELOWOF9-ROBOT)
(ABOVEOF15-ROBOT)
)
:effect
(and
(Flag449-)
(Flag449prime-)
)

)
(:action Flag450Action
:parameters ()
:precondition
(and
(ABOVEOF18-ROBOT)
(BELOWOF9-ROBOT)
)
:effect
(and
(Flag450-)
(Flag450prime-)
)

)
(:action Flag451Action
:parameters ()
:precondition
(and
(ABOVEOF8-ROBOT)
(BELOWOF9-ROBOT)
)
:effect
(and
(Flag451-)
(Flag451prime-)
)

)
(:action Flag452Action
:parameters ()
:precondition
(and
(ABOVEOF12-ROBOT)
(BELOWOF9-ROBOT)
)
:effect
(and
(Flag452-)
(Flag452prime-)
)

)
(:action Prim440Action-0
:parameters ()
:precondition
(and
(not (ABOVEOF17-ROBOT))
)
:effect
(and
(Flag440prime-)
(not (Flag440-))
)

)
(:action Prim440Action-1
:parameters ()
:precondition
(and
(not (BELOWOF9-ROBOT))
)
:effect
(and
(Flag440prime-)
(not (Flag440-))
)

)
(:action Prim441Action-0
:parameters ()
:precondition
(and
(not (BELOWOF9-ROBOT))
)
:effect
(and
(Flag441prime-)
(not (Flag441-))
)

)
(:action Prim441Action-1
:parameters ()
:precondition
(and
(not (ABOVEOF13-ROBOT))
)
:effect
(and
(Flag441prime-)
(not (Flag441-))
)

)
(:action Prim442Action-0
:parameters ()
:precondition
(and
(not (ABOVEOF11-ROBOT))
)
:effect
(and
(Flag442prime-)
(not (Flag442-))
)

)
(:action Prim442Action-1
:parameters ()
:precondition
(and
(not (BELOWOF9-ROBOT))
)
:effect
(and
(Flag442prime-)
(not (Flag442-))
)

)
(:action Prim443Action-0
:parameters ()
:precondition
(and
(not (BELOWOF8-ROBOT))
)
:effect
(and
(Flag443prime-)
(not (Flag443-))
)

)
(:action Prim443Action-1
:parameters ()
:precondition
(and
(not (ABOVEOF12-ROBOT))
)
:effect
(and
(Flag443prime-)
(not (Flag443-))
)

)
(:action Prim444Action-0
:parameters ()
:precondition
(and
(not (ABOVEOF10-ROBOT))
)
:effect
(and
(Flag444prime-)
(not (Flag444-))
)

)
(:action Prim444Action-1
:parameters ()
:precondition
(and
(not (BELOWOF9-ROBOT))
)
:effect
(and
(Flag444prime-)
(not (Flag444-))
)

)
(:action Prim445Action-0
:parameters ()
:precondition
(and
(not (BELOWOF9-ROBOT))
)
:effect
(and
(Flag445prime-)
(not (Flag445-))
)

)
(:action Prim445Action-1
:parameters ()
:precondition
(and
(not (ABOVEOF14-ROBOT))
)
:effect
(and
(Flag445prime-)
(not (Flag445-))
)

)
(:action Prim446Action-0
:parameters ()
:precondition
(and
(not (BELOWOF9-ROBOT))
)
:effect
(and
(Flag446prime-)
(not (Flag446-))
)

)
(:action Prim446Action-1
:parameters ()
:precondition
(and
(not (ABOVEOF9-ROBOT))
)
:effect
(and
(Flag446prime-)
(not (Flag446-))
)

)
(:action Prim447Action-0
:parameters ()
:precondition
(and
(not (BELOWOF9-ROBOT))
)
:effect
(and
(Flag447prime-)
(not (Flag447-))
)

)
(:action Prim447Action-1
:parameters ()
:precondition
(and
(not (ABOVEOF16-ROBOT))
)
:effect
(and
(Flag447prime-)
(not (Flag447-))
)

)
(:action Prim448Action-0
:parameters ()
:precondition
(and
(not (BELOWOF8-ROBOT))
)
:effect
(and
(Flag448prime-)
(not (Flag448-))
)

)
(:action Prim448Action-1
:parameters ()
:precondition
(and
(not (ABOVEOF8-ROBOT))
)
:effect
(and
(Flag448prime-)
(not (Flag448-))
)

)
(:action Prim449Action-0
:parameters ()
:precondition
(and
(not (BELOWOF9-ROBOT))
)
:effect
(and
(Flag449prime-)
(not (Flag449-))
)

)
(:action Prim449Action-1
:parameters ()
:precondition
(and
(not (ABOVEOF15-ROBOT))
)
:effect
(and
(Flag449prime-)
(not (Flag449-))
)

)
(:action Prim450Action-0
:parameters ()
:precondition
(and
(not (ABOVEOF18-ROBOT))
)
:effect
(and
(Flag450prime-)
(not (Flag450-))
)

)
(:action Prim450Action-1
:parameters ()
:precondition
(and
(not (BELOWOF9-ROBOT))
)
:effect
(and
(Flag450prime-)
(not (Flag450-))
)

)
(:action Prim451Action-0
:parameters ()
:precondition
(and
(not (ABOVEOF8-ROBOT))
)
:effect
(and
(Flag451prime-)
(not (Flag451-))
)

)
(:action Prim451Action-1
:parameters ()
:precondition
(and
(not (BELOWOF9-ROBOT))
)
:effect
(and
(Flag451prime-)
(not (Flag451-))
)

)
(:action Prim452Action-0
:parameters ()
:precondition
(and
(not (ABOVEOF12-ROBOT))
)
:effect
(and
(Flag452prime-)
(not (Flag452-))
)

)
(:action Prim452Action-1
:parameters ()
:precondition
(and
(not (BELOWOF9-ROBOT))
)
:effect
(and
(Flag452prime-)
(not (Flag452-))
)

)
(:action Flag454Action
:parameters ()
:precondition
(and
(BELOWOF10-ROBOT)
(ABOVEOF10-ROBOT)
)
:effect
(and
(Flag454-)
(Flag454prime-)
)

)
(:action Flag455Action
:parameters ()
:precondition
(and
(BELOWOF10-ROBOT)
(ABOVEOF18-ROBOT)
)
:effect
(and
(Flag455-)
(Flag455prime-)
)

)
(:action Flag456Action
:parameters ()
:precondition
(and
(ABOVEOF12-ROBOT)
(BELOWOF10-ROBOT)
)
:effect
(and
(Flag456-)
(Flag456prime-)
)

)
(:action Flag457Action
:parameters ()
:precondition
(and
(BELOWOF10-ROBOT)
(ABOVEOF14-ROBOT)
)
:effect
(and
(Flag457-)
(Flag457prime-)
)

)
(:action Flag458Action
:parameters ()
:precondition
(and
(ABOVEOF9-ROBOT)
(BELOWOF9-ROBOT)
)
:effect
(and
(Flag458-)
(Flag458prime-)
)

)
(:action Flag459Action
:parameters ()
:precondition
(and
(BELOWOF10-ROBOT)
(ABOVEOF13-ROBOT)
)
:effect
(and
(Flag459-)
(Flag459prime-)
)

)
(:action Flag460Action
:parameters ()
:precondition
(and
(BELOWOF10-ROBOT)
(ABOVEOF16-ROBOT)
)
:effect
(and
(Flag460-)
(Flag460prime-)
)

)
(:action Flag461Action
:parameters ()
:precondition
(and
(BELOWOF10-ROBOT)
(ABOVEOF9-ROBOT)
)
:effect
(and
(Flag461-)
(Flag461prime-)
)

)
(:action Flag462Action
:parameters ()
:precondition
(and
(BELOWOF10-ROBOT)
(ABOVEOF15-ROBOT)
)
:effect
(and
(Flag462-)
(Flag462prime-)
)

)
(:action Flag463Action
:parameters ()
:precondition
(and
(BELOWOF10-ROBOT)
(ABOVEOF17-ROBOT)
)
:effect
(and
(Flag463-)
(Flag463prime-)
)

)
(:action Flag464Action
:parameters ()
:precondition
(and
(ABOVEOF11-ROBOT)
(BELOWOF10-ROBOT)
)
:effect
(and
(Flag464-)
(Flag464prime-)
)

)
(:action Prim454Action-0
:parameters ()
:precondition
(and
(not (BELOWOF10-ROBOT))
)
:effect
(and
(Flag454prime-)
(not (Flag454-))
)

)
(:action Prim454Action-1
:parameters ()
:precondition
(and
(not (ABOVEOF10-ROBOT))
)
:effect
(and
(Flag454prime-)
(not (Flag454-))
)

)
(:action Prim455Action-0
:parameters ()
:precondition
(and
(not (BELOWOF10-ROBOT))
)
:effect
(and
(Flag455prime-)
(not (Flag455-))
)

)
(:action Prim455Action-1
:parameters ()
:precondition
(and
(not (ABOVEOF18-ROBOT))
)
:effect
(and
(Flag455prime-)
(not (Flag455-))
)

)
(:action Prim456Action-0
:parameters ()
:precondition
(and
(not (ABOVEOF12-ROBOT))
)
:effect
(and
(Flag456prime-)
(not (Flag456-))
)

)
(:action Prim456Action-1
:parameters ()
:precondition
(and
(not (BELOWOF10-ROBOT))
)
:effect
(and
(Flag456prime-)
(not (Flag456-))
)

)
(:action Prim457Action-0
:parameters ()
:precondition
(and
(not (BELOWOF10-ROBOT))
)
:effect
(and
(Flag457prime-)
(not (Flag457-))
)

)
(:action Prim457Action-1
:parameters ()
:precondition
(and
(not (ABOVEOF14-ROBOT))
)
:effect
(and
(Flag457prime-)
(not (Flag457-))
)

)
(:action Prim458Action-0
:parameters ()
:precondition
(and
(not (ABOVEOF9-ROBOT))
)
:effect
(and
(Flag458prime-)
(not (Flag458-))
)

)
(:action Prim458Action-1
:parameters ()
:precondition
(and
(not (BELOWOF9-ROBOT))
)
:effect
(and
(Flag458prime-)
(not (Flag458-))
)

)
(:action Prim459Action-0
:parameters ()
:precondition
(and
(not (BELOWOF10-ROBOT))
)
:effect
(and
(Flag459prime-)
(not (Flag459-))
)

)
(:action Prim459Action-1
:parameters ()
:precondition
(and
(not (ABOVEOF13-ROBOT))
)
:effect
(and
(Flag459prime-)
(not (Flag459-))
)

)
(:action Prim460Action-0
:parameters ()
:precondition
(and
(not (BELOWOF10-ROBOT))
)
:effect
(and
(Flag460prime-)
(not (Flag460-))
)

)
(:action Prim460Action-1
:parameters ()
:precondition
(and
(not (ABOVEOF16-ROBOT))
)
:effect
(and
(Flag460prime-)
(not (Flag460-))
)

)
(:action Prim461Action-0
:parameters ()
:precondition
(and
(not (BELOWOF10-ROBOT))
)
:effect
(and
(Flag461prime-)
(not (Flag461-))
)

)
(:action Prim461Action-1
:parameters ()
:precondition
(and
(not (ABOVEOF9-ROBOT))
)
:effect
(and
(Flag461prime-)
(not (Flag461-))
)

)
(:action Prim462Action-0
:parameters ()
:precondition
(and
(not (BELOWOF10-ROBOT))
)
:effect
(and
(Flag462prime-)
(not (Flag462-))
)

)
(:action Prim462Action-1
:parameters ()
:precondition
(and
(not (ABOVEOF15-ROBOT))
)
:effect
(and
(Flag462prime-)
(not (Flag462-))
)

)
(:action Prim463Action-0
:parameters ()
:precondition
(and
(not (BELOWOF10-ROBOT))
)
:effect
(and
(Flag463prime-)
(not (Flag463-))
)

)
(:action Prim463Action-1
:parameters ()
:precondition
(and
(not (ABOVEOF17-ROBOT))
)
:effect
(and
(Flag463prime-)
(not (Flag463-))
)

)
(:action Prim464Action-0
:parameters ()
:precondition
(and
(not (ABOVEOF11-ROBOT))
)
:effect
(and
(Flag464prime-)
(not (Flag464-))
)

)
(:action Prim464Action-1
:parameters ()
:precondition
(and
(not (BELOWOF10-ROBOT))
)
:effect
(and
(Flag464prime-)
(not (Flag464-))
)

)
(:action Flag466Action
:parameters ()
:precondition
(and
(BELOWOF11-ROBOT)
(ABOVEOF16-ROBOT)
)
:effect
(and
(Flag466-)
(Flag466prime-)
)

)
(:action Flag467Action
:parameters ()
:precondition
(and
(BELOWOF11-ROBOT)
(ABOVEOF18-ROBOT)
)
:effect
(and
(Flag467-)
(Flag467prime-)
)

)
(:action Flag468Action
:parameters ()
:precondition
(and
(BELOWOF11-ROBOT)
(ABOVEOF15-ROBOT)
)
:effect
(and
(Flag468-)
(Flag468prime-)
)

)
(:action Flag469Action
:parameters ()
:precondition
(and
(BELOWOF11-ROBOT)
(ABOVEOF17-ROBOT)
)
:effect
(and
(Flag469-)
(Flag469prime-)
)

)
(:action Flag470Action
:parameters ()
:precondition
(and
(BELOWOF11-ROBOT)
(ABOVEOF13-ROBOT)
)
:effect
(and
(Flag470-)
(Flag470prime-)
)

)
(:action Flag471Action
:parameters ()
:precondition
(and
(BELOWOF11-ROBOT)
(ABOVEOF14-ROBOT)
)
:effect
(and
(Flag471-)
(Flag471prime-)
)

)
(:action Flag472Action
:parameters ()
:precondition
(and
(BELOWOF11-ROBOT)
(ABOVEOF10-ROBOT)
)
:effect
(and
(Flag472-)
(Flag472prime-)
)

)
(:action Flag473Action
:parameters ()
:precondition
(and
(ABOVEOF11-ROBOT)
(BELOWOF11-ROBOT)
)
:effect
(and
(Flag473-)
(Flag473prime-)
)

)
(:action Flag474Action
:parameters ()
:precondition
(and
(ABOVEOF12-ROBOT)
(BELOWOF11-ROBOT)
)
:effect
(and
(Flag474-)
(Flag474prime-)
)

)
(:action Prim466Action-0
:parameters ()
:precondition
(and
(not (BELOWOF11-ROBOT))
)
:effect
(and
(Flag466prime-)
(not (Flag466-))
)

)
(:action Prim466Action-1
:parameters ()
:precondition
(and
(not (ABOVEOF16-ROBOT))
)
:effect
(and
(Flag466prime-)
(not (Flag466-))
)

)
(:action Prim467Action-0
:parameters ()
:precondition
(and
(not (BELOWOF11-ROBOT))
)
:effect
(and
(Flag467prime-)
(not (Flag467-))
)

)
(:action Prim467Action-1
:parameters ()
:precondition
(and
(not (ABOVEOF18-ROBOT))
)
:effect
(and
(Flag467prime-)
(not (Flag467-))
)

)
(:action Prim468Action-0
:parameters ()
:precondition
(and
(not (BELOWOF11-ROBOT))
)
:effect
(and
(Flag468prime-)
(not (Flag468-))
)

)
(:action Prim468Action-1
:parameters ()
:precondition
(and
(not (ABOVEOF15-ROBOT))
)
:effect
(and
(Flag468prime-)
(not (Flag468-))
)

)
(:action Prim469Action-0
:parameters ()
:precondition
(and
(not (BELOWOF11-ROBOT))
)
:effect
(and
(Flag469prime-)
(not (Flag469-))
)

)
(:action Prim469Action-1
:parameters ()
:precondition
(and
(not (ABOVEOF17-ROBOT))
)
:effect
(and
(Flag469prime-)
(not (Flag469-))
)

)
(:action Prim470Action-0
:parameters ()
:precondition
(and
(not (BELOWOF11-ROBOT))
)
:effect
(and
(Flag470prime-)
(not (Flag470-))
)

)
(:action Prim470Action-1
:parameters ()
:precondition
(and
(not (ABOVEOF13-ROBOT))
)
:effect
(and
(Flag470prime-)
(not (Flag470-))
)

)
(:action Prim471Action-0
:parameters ()
:precondition
(and
(not (BELOWOF11-ROBOT))
)
:effect
(and
(Flag471prime-)
(not (Flag471-))
)

)
(:action Prim471Action-1
:parameters ()
:precondition
(and
(not (ABOVEOF14-ROBOT))
)
:effect
(and
(Flag471prime-)
(not (Flag471-))
)

)
(:action Prim472Action-0
:parameters ()
:precondition
(and
(not (BELOWOF11-ROBOT))
)
:effect
(and
(Flag472prime-)
(not (Flag472-))
)

)
(:action Prim472Action-1
:parameters ()
:precondition
(and
(not (ABOVEOF10-ROBOT))
)
:effect
(and
(Flag472prime-)
(not (Flag472-))
)

)
(:action Prim473Action-0
:parameters ()
:precondition
(and
(not (ABOVEOF11-ROBOT))
)
:effect
(and
(Flag473prime-)
(not (Flag473-))
)

)
(:action Prim473Action-1
:parameters ()
:precondition
(and
(not (BELOWOF11-ROBOT))
)
:effect
(and
(Flag473prime-)
(not (Flag473-))
)

)
(:action Prim474Action-0
:parameters ()
:precondition
(and
(not (ABOVEOF12-ROBOT))
)
:effect
(and
(Flag474prime-)
(not (Flag474-))
)

)
(:action Prim474Action-1
:parameters ()
:precondition
(and
(not (BELOWOF11-ROBOT))
)
:effect
(and
(Flag474prime-)
(not (Flag474-))
)

)
(:action Flag476Action
:parameters ()
:precondition
(and
(BELOWOF12-ROBOT)
(ABOVEOF18-ROBOT)
)
:effect
(and
(Flag476-)
(Flag476prime-)
)

)
(:action Flag477Action
:parameters ()
:precondition
(and
(BELOWOF12-ROBOT)
(ABOVEOF13-ROBOT)
)
:effect
(and
(Flag477-)
(Flag477prime-)
)

)
(:action Flag478Action
:parameters ()
:precondition
(and
(BELOWOF12-ROBOT)
(ABOVEOF15-ROBOT)
)
:effect
(and
(Flag478-)
(Flag478prime-)
)

)
(:action Flag479Action
:parameters ()
:precondition
(and
(BELOWOF12-ROBOT)
(ABOVEOF17-ROBOT)
)
:effect
(and
(Flag479-)
(Flag479prime-)
)

)
(:action Flag480Action
:parameters ()
:precondition
(and
(BELOWOF12-ROBOT)
(ABOVEOF11-ROBOT)
)
:effect
(and
(Flag480-)
(Flag480prime-)
)

)
(:action Flag481Action
:parameters ()
:precondition
(and
(BELOWOF12-ROBOT)
(ABOVEOF14-ROBOT)
)
:effect
(and
(Flag481-)
(Flag481prime-)
)

)
(:action Flag482Action
:parameters ()
:precondition
(and
(ABOVEOF12-ROBOT)
(BELOWOF12-ROBOT)
)
:effect
(and
(Flag482-)
(Flag482prime-)
)

)
(:action Flag483Action
:parameters ()
:precondition
(and
(BELOWOF12-ROBOT)
(ABOVEOF16-ROBOT)
)
:effect
(and
(Flag483-)
(Flag483prime-)
)

)
(:action Prim476Action-0
:parameters ()
:precondition
(and
(not (BELOWOF12-ROBOT))
)
:effect
(and
(Flag476prime-)
(not (Flag476-))
)

)
(:action Prim476Action-1
:parameters ()
:precondition
(and
(not (ABOVEOF18-ROBOT))
)
:effect
(and
(Flag476prime-)
(not (Flag476-))
)

)
(:action Prim477Action-0
:parameters ()
:precondition
(and
(not (BELOWOF12-ROBOT))
)
:effect
(and
(Flag477prime-)
(not (Flag477-))
)

)
(:action Prim477Action-1
:parameters ()
:precondition
(and
(not (ABOVEOF13-ROBOT))
)
:effect
(and
(Flag477prime-)
(not (Flag477-))
)

)
(:action Prim478Action-0
:parameters ()
:precondition
(and
(not (BELOWOF12-ROBOT))
)
:effect
(and
(Flag478prime-)
(not (Flag478-))
)

)
(:action Prim478Action-1
:parameters ()
:precondition
(and
(not (ABOVEOF15-ROBOT))
)
:effect
(and
(Flag478prime-)
(not (Flag478-))
)

)
(:action Prim479Action-0
:parameters ()
:precondition
(and
(not (BELOWOF12-ROBOT))
)
:effect
(and
(Flag479prime-)
(not (Flag479-))
)

)
(:action Prim479Action-1
:parameters ()
:precondition
(and
(not (ABOVEOF17-ROBOT))
)
:effect
(and
(Flag479prime-)
(not (Flag479-))
)

)
(:action Prim480Action-0
:parameters ()
:precondition
(and
(not (BELOWOF12-ROBOT))
)
:effect
(and
(Flag480prime-)
(not (Flag480-))
)

)
(:action Prim480Action-1
:parameters ()
:precondition
(and
(not (ABOVEOF11-ROBOT))
)
:effect
(and
(Flag480prime-)
(not (Flag480-))
)

)
(:action Prim481Action-0
:parameters ()
:precondition
(and
(not (BELOWOF12-ROBOT))
)
:effect
(and
(Flag481prime-)
(not (Flag481-))
)

)
(:action Prim481Action-1
:parameters ()
:precondition
(and
(not (ABOVEOF14-ROBOT))
)
:effect
(and
(Flag481prime-)
(not (Flag481-))
)

)
(:action Prim482Action-0
:parameters ()
:precondition
(and
(not (ABOVEOF12-ROBOT))
)
:effect
(and
(Flag482prime-)
(not (Flag482-))
)

)
(:action Prim482Action-1
:parameters ()
:precondition
(and
(not (BELOWOF12-ROBOT))
)
:effect
(and
(Flag482prime-)
(not (Flag482-))
)

)
(:action Prim483Action-0
:parameters ()
:precondition
(and
(not (BELOWOF12-ROBOT))
)
:effect
(and
(Flag483prime-)
(not (Flag483-))
)

)
(:action Prim483Action-1
:parameters ()
:precondition
(and
(not (ABOVEOF16-ROBOT))
)
:effect
(and
(Flag483prime-)
(not (Flag483-))
)

)
(:action Flag485Action
:parameters ()
:precondition
(and
(ABOVEOF18-ROBOT)
(BELOWOF13-ROBOT)
)
:effect
(and
(Flag485-)
(Flag485prime-)
)

)
(:action Flag486Action
:parameters ()
:precondition
(and
(ABOVEOF16-ROBOT)
(BELOWOF13-ROBOT)
)
:effect
(and
(Flag486-)
(Flag486prime-)
)

)
(:action Flag487Action
:parameters ()
:precondition
(and
(ABOVEOF17-ROBOT)
(BELOWOF13-ROBOT)
)
:effect
(and
(Flag487-)
(Flag487prime-)
)

)
(:action Flag488Action
:parameters ()
:precondition
(and
(ABOVEOF13-ROBOT)
(BELOWOF13-ROBOT)
)
:effect
(and
(Flag488-)
(Flag488prime-)
)

)
(:action Flag489Action
:parameters ()
:precondition
(and
(ABOVEOF14-ROBOT)
(BELOWOF13-ROBOT)
)
:effect
(and
(Flag489-)
(Flag489prime-)
)

)
(:action Flag490Action
:parameters ()
:precondition
(and
(ABOVEOF15-ROBOT)
(BELOWOF13-ROBOT)
)
:effect
(and
(Flag490-)
(Flag490prime-)
)

)
(:action Flag491Action
:parameters ()
:precondition
(and
(ABOVEOF12-ROBOT)
(BELOWOF13-ROBOT)
)
:effect
(and
(Flag491-)
(Flag491prime-)
)

)
(:action Flag492Action
:parameters ()
:precondition
(and
(BELOWOF12-ROBOT)
(ABOVEOF12-ROBOT)
)
:effect
(and
(Flag492-)
(Flag492prime-)
)

)
(:action Prim485Action-0
:parameters ()
:precondition
(and
(not (ABOVEOF18-ROBOT))
)
:effect
(and
(Flag485prime-)
(not (Flag485-))
)

)
(:action Prim485Action-1
:parameters ()
:precondition
(and
(not (BELOWOF13-ROBOT))
)
:effect
(and
(Flag485prime-)
(not (Flag485-))
)

)
(:action Prim486Action-0
:parameters ()
:precondition
(and
(not (ABOVEOF16-ROBOT))
)
:effect
(and
(Flag486prime-)
(not (Flag486-))
)

)
(:action Prim486Action-1
:parameters ()
:precondition
(and
(not (BELOWOF13-ROBOT))
)
:effect
(and
(Flag486prime-)
(not (Flag486-))
)

)
(:action Prim487Action-0
:parameters ()
:precondition
(and
(not (ABOVEOF17-ROBOT))
)
:effect
(and
(Flag487prime-)
(not (Flag487-))
)

)
(:action Prim487Action-1
:parameters ()
:precondition
(and
(not (BELOWOF13-ROBOT))
)
:effect
(and
(Flag487prime-)
(not (Flag487-))
)

)
(:action Prim488Action-0
:parameters ()
:precondition
(and
(not (ABOVEOF13-ROBOT))
)
:effect
(and
(Flag488prime-)
(not (Flag488-))
)

)
(:action Prim488Action-1
:parameters ()
:precondition
(and
(not (BELOWOF13-ROBOT))
)
:effect
(and
(Flag488prime-)
(not (Flag488-))
)

)
(:action Prim489Action-0
:parameters ()
:precondition
(and
(not (ABOVEOF14-ROBOT))
)
:effect
(and
(Flag489prime-)
(not (Flag489-))
)

)
(:action Prim489Action-1
:parameters ()
:precondition
(and
(not (BELOWOF13-ROBOT))
)
:effect
(and
(Flag489prime-)
(not (Flag489-))
)

)
(:action Prim490Action-0
:parameters ()
:precondition
(and
(not (ABOVEOF15-ROBOT))
)
:effect
(and
(Flag490prime-)
(not (Flag490-))
)

)
(:action Prim490Action-1
:parameters ()
:precondition
(and
(not (BELOWOF13-ROBOT))
)
:effect
(and
(Flag490prime-)
(not (Flag490-))
)

)
(:action Prim491Action-0
:parameters ()
:precondition
(and
(not (ABOVEOF12-ROBOT))
)
:effect
(and
(Flag491prime-)
(not (Flag491-))
)

)
(:action Prim491Action-1
:parameters ()
:precondition
(and
(not (BELOWOF13-ROBOT))
)
:effect
(and
(Flag491prime-)
(not (Flag491-))
)

)
(:action Prim492Action-0
:parameters ()
:precondition
(and
(not (BELOWOF12-ROBOT))
)
:effect
(and
(Flag492prime-)
(not (Flag492-))
)

)
(:action Prim492Action-1
:parameters ()
:precondition
(and
(not (ABOVEOF12-ROBOT))
)
:effect
(and
(Flag492prime-)
(not (Flag492-))
)

)
(:action Flag494Action
:parameters ()
:precondition
(and
(ABOVEOF15-ROBOT)
(BELOWOF14-ROBOT)
)
:effect
(and
(Flag494-)
(Flag494prime-)
)

)
(:action Flag495Action
:parameters ()
:precondition
(and
(ABOVEOF16-ROBOT)
(BELOWOF14-ROBOT)
)
:effect
(and
(Flag495-)
(Flag495prime-)
)

)
(:action Flag496Action
:parameters ()
:precondition
(and
(BELOWOF14-ROBOT)
(ABOVEOF14-ROBOT)
)
:effect
(and
(Flag496-)
(Flag496prime-)
)

)
(:action Flag497Action
:parameters ()
:precondition
(and
(BELOWOF13-ROBOT)
(ABOVEOF13-ROBOT)
)
:effect
(and
(Flag497-)
(Flag497prime-)
)

)
(:action Flag498Action
:parameters ()
:precondition
(and
(BELOWOF14-ROBOT)
(ABOVEOF13-ROBOT)
)
:effect
(and
(Flag498-)
(Flag498prime-)
)

)
(:action Flag499Action
:parameters ()
:precondition
(and
(ABOVEOF18-ROBOT)
(BELOWOF14-ROBOT)
)
:effect
(and
(Flag499-)
(Flag499prime-)
)

)
(:action Flag500Action
:parameters ()
:precondition
(and
(ABOVEOF17-ROBOT)
(BELOWOF14-ROBOT)
)
:effect
(and
(Flag500-)
(Flag500prime-)
)

)
(:action Prim494Action-0
:parameters ()
:precondition
(and
(not (ABOVEOF15-ROBOT))
)
:effect
(and
(Flag494prime-)
(not (Flag494-))
)

)
(:action Prim494Action-1
:parameters ()
:precondition
(and
(not (BELOWOF14-ROBOT))
)
:effect
(and
(Flag494prime-)
(not (Flag494-))
)

)
(:action Prim495Action-0
:parameters ()
:precondition
(and
(not (ABOVEOF16-ROBOT))
)
:effect
(and
(Flag495prime-)
(not (Flag495-))
)

)
(:action Prim495Action-1
:parameters ()
:precondition
(and
(not (BELOWOF14-ROBOT))
)
:effect
(and
(Flag495prime-)
(not (Flag495-))
)

)
(:action Prim496Action-0
:parameters ()
:precondition
(and
(not (BELOWOF14-ROBOT))
)
:effect
(and
(Flag496prime-)
(not (Flag496-))
)

)
(:action Prim496Action-1
:parameters ()
:precondition
(and
(not (ABOVEOF14-ROBOT))
)
:effect
(and
(Flag496prime-)
(not (Flag496-))
)

)
(:action Prim497Action-0
:parameters ()
:precondition
(and
(not (BELOWOF13-ROBOT))
)
:effect
(and
(Flag497prime-)
(not (Flag497-))
)

)
(:action Prim497Action-1
:parameters ()
:precondition
(and
(not (ABOVEOF13-ROBOT))
)
:effect
(and
(Flag497prime-)
(not (Flag497-))
)

)
(:action Prim498Action-0
:parameters ()
:precondition
(and
(not (BELOWOF14-ROBOT))
)
:effect
(and
(Flag498prime-)
(not (Flag498-))
)

)
(:action Prim498Action-1
:parameters ()
:precondition
(and
(not (ABOVEOF13-ROBOT))
)
:effect
(and
(Flag498prime-)
(not (Flag498-))
)

)
(:action Prim499Action-0
:parameters ()
:precondition
(and
(not (ABOVEOF18-ROBOT))
)
:effect
(and
(Flag499prime-)
(not (Flag499-))
)

)
(:action Prim499Action-1
:parameters ()
:precondition
(and
(not (BELOWOF14-ROBOT))
)
:effect
(and
(Flag499prime-)
(not (Flag499-))
)

)
(:action Prim500Action-0
:parameters ()
:precondition
(and
(not (ABOVEOF17-ROBOT))
)
:effect
(and
(Flag500prime-)
(not (Flag500-))
)

)
(:action Prim500Action-1
:parameters ()
:precondition
(and
(not (BELOWOF14-ROBOT))
)
:effect
(and
(Flag500prime-)
(not (Flag500-))
)

)
(:action Flag502Action
:parameters ()
:precondition
(and
(BELOWOF15-ROBOT)
(ABOVEOF14-ROBOT)
)
:effect
(and
(Flag502-)
(Flag502prime-)
)

)
(:action Flag503Action
:parameters ()
:precondition
(and
(BELOWOF15-ROBOT)
(ABOVEOF15-ROBOT)
)
:effect
(and
(Flag503-)
(Flag503prime-)
)

)
(:action Flag504Action
:parameters ()
:precondition
(and
(ABOVEOF17-ROBOT)
(BELOWOF15-ROBOT)
)
:effect
(and
(Flag504-)
(Flag504prime-)
)

)
(:action Flag505Action
:parameters ()
:precondition
(and
(ABOVEOF16-ROBOT)
(BELOWOF15-ROBOT)
)
:effect
(and
(Flag505-)
(Flag505prime-)
)

)
(:action Flag506Action
:parameters ()
:precondition
(and
(ABOVEOF18-ROBOT)
(BELOWOF15-ROBOT)
)
:effect
(and
(Flag506-)
(Flag506prime-)
)

)
(:action Prim502Action-0
:parameters ()
:precondition
(and
(not (BELOWOF15-ROBOT))
)
:effect
(and
(Flag502prime-)
(not (Flag502-))
)

)
(:action Prim502Action-1
:parameters ()
:precondition
(and
(not (ABOVEOF14-ROBOT))
)
:effect
(and
(Flag502prime-)
(not (Flag502-))
)

)
(:action Prim503Action-0
:parameters ()
:precondition
(and
(not (BELOWOF15-ROBOT))
)
:effect
(and
(Flag503prime-)
(not (Flag503-))
)

)
(:action Prim503Action-1
:parameters ()
:precondition
(and
(not (ABOVEOF15-ROBOT))
)
:effect
(and
(Flag503prime-)
(not (Flag503-))
)

)
(:action Prim504Action-0
:parameters ()
:precondition
(and
(not (ABOVEOF17-ROBOT))
)
:effect
(and
(Flag504prime-)
(not (Flag504-))
)

)
(:action Prim504Action-1
:parameters ()
:precondition
(and
(not (BELOWOF15-ROBOT))
)
:effect
(and
(Flag504prime-)
(not (Flag504-))
)

)
(:action Prim505Action-0
:parameters ()
:precondition
(and
(not (ABOVEOF16-ROBOT))
)
:effect
(and
(Flag505prime-)
(not (Flag505-))
)

)
(:action Prim505Action-1
:parameters ()
:precondition
(and
(not (BELOWOF15-ROBOT))
)
:effect
(and
(Flag505prime-)
(not (Flag505-))
)

)
(:action Prim506Action-0
:parameters ()
:precondition
(and
(not (ABOVEOF18-ROBOT))
)
:effect
(and
(Flag506prime-)
(not (Flag506-))
)

)
(:action Prim506Action-1
:parameters ()
:precondition
(and
(not (BELOWOF15-ROBOT))
)
:effect
(and
(Flag506prime-)
(not (Flag506-))
)

)
(:action Flag508Action
:parameters ()
:precondition
(and
(ABOVEOF17-ROBOT)
(BELOWOF16-ROBOT)
)
:effect
(and
(Flag508-)
(Flag508prime-)
)

)
(:action Flag509Action
:parameters ()
:precondition
(and
(ABOVEOF16-ROBOT)
(BELOWOF16-ROBOT)
)
:effect
(and
(Flag509-)
(Flag509prime-)
)

)
(:action Flag510Action
:parameters ()
:precondition
(and
(BELOWOF16-ROBOT)
(ABOVEOF15-ROBOT)
)
:effect
(and
(Flag510-)
(Flag510prime-)
)

)
(:action Flag511Action
:parameters ()
:precondition
(and
(ABOVEOF18-ROBOT)
(BELOWOF16-ROBOT)
)
:effect
(and
(Flag511-)
(Flag511prime-)
)

)
(:action Prim508Action-0
:parameters ()
:precondition
(and
(not (ABOVEOF17-ROBOT))
)
:effect
(and
(Flag508prime-)
(not (Flag508-))
)

)
(:action Prim508Action-1
:parameters ()
:precondition
(and
(not (BELOWOF16-ROBOT))
)
:effect
(and
(Flag508prime-)
(not (Flag508-))
)

)
(:action Prim509Action-0
:parameters ()
:precondition
(and
(not (ABOVEOF16-ROBOT))
)
:effect
(and
(Flag509prime-)
(not (Flag509-))
)

)
(:action Prim509Action-1
:parameters ()
:precondition
(and
(not (BELOWOF16-ROBOT))
)
:effect
(and
(Flag509prime-)
(not (Flag509-))
)

)
(:action Prim510Action-0
:parameters ()
:precondition
(and
(not (BELOWOF16-ROBOT))
)
:effect
(and
(Flag510prime-)
(not (Flag510-))
)

)
(:action Prim510Action-1
:parameters ()
:precondition
(and
(not (ABOVEOF15-ROBOT))
)
:effect
(and
(Flag510prime-)
(not (Flag510-))
)

)
(:action Prim511Action-0
:parameters ()
:precondition
(and
(not (ABOVEOF18-ROBOT))
)
:effect
(and
(Flag511prime-)
(not (Flag511-))
)

)
(:action Prim511Action-1
:parameters ()
:precondition
(and
(not (BELOWOF16-ROBOT))
)
:effect
(and
(Flag511prime-)
(not (Flag511-))
)

)
(:action Flag513Action
:parameters ()
:precondition
(and
(ABOVEOF18-ROBOT)
(BELOWOF17-ROBOT)
)
:effect
(and
(Flag513-)
(Flag513prime-)
)

)
(:action Flag514Action
:parameters ()
:precondition
(and
(BELOWOF17-ROBOT)
(ABOVEOF16-ROBOT)
)
:effect
(and
(Flag514-)
(Flag514prime-)
)

)
(:action Flag515Action
:parameters ()
:precondition
(and
(ABOVEOF17-ROBOT)
(BELOWOF17-ROBOT)
)
:effect
(and
(Flag515-)
(Flag515prime-)
)

)
(:action Prim513Action-0
:parameters ()
:precondition
(and
(not (ABOVEOF18-ROBOT))
)
:effect
(and
(Flag513prime-)
(not (Flag513-))
)

)
(:action Prim513Action-1
:parameters ()
:precondition
(and
(not (BELOWOF17-ROBOT))
)
:effect
(and
(Flag513prime-)
(not (Flag513-))
)

)
(:action Prim514Action-0
:parameters ()
:precondition
(and
(not (BELOWOF17-ROBOT))
)
:effect
(and
(Flag514prime-)
(not (Flag514-))
)

)
(:action Prim514Action-1
:parameters ()
:precondition
(and
(not (ABOVEOF16-ROBOT))
)
:effect
(and
(Flag514prime-)
(not (Flag514-))
)

)
(:action Prim515Action-0
:parameters ()
:precondition
(and
(not (ABOVEOF17-ROBOT))
)
:effect
(and
(Flag515prime-)
(not (Flag515-))
)

)
(:action Prim515Action-1
:parameters ()
:precondition
(and
(not (BELOWOF17-ROBOT))
)
:effect
(and
(Flag515prime-)
(not (Flag515-))
)

)
(:action Flag517Action
:parameters ()
:precondition
(and
(BELOWOF18-ROBOT)
(ABOVEOF17-ROBOT)
)
:effect
(and
(Flag517-)
(Flag517prime-)
)

)
(:action Flag518Action
:parameters ()
:precondition
(and
(BELOWOF18-ROBOT)
(ABOVEOF18-ROBOT)
)
:effect
(and
(Flag518-)
(Flag518prime-)
)

)
(:action Prim517Action-1
:parameters ()
:precondition
(and
(not (ABOVEOF17-ROBOT))
)
:effect
(and
(Flag517prime-)
(not (Flag517-))
)

)
(:action Prim518Action-1
:parameters ()
:precondition
(and
(not (ABOVEOF18-ROBOT))
)
:effect
(and
(Flag518prime-)
(not (Flag518-))
)

)
(:action Flag520Action
:parameters ()
:precondition
(and
(BELOWOF19-ROBOT)
(ABOVEOF18-ROBOT)
)
:effect
(and
(Flag520-)
(Flag520prime-)
)

)
(:action Prim520Action-0
:parameters ()
:precondition
(and
(not (BELOWOF19-ROBOT))
)
:effect
(and
(Flag520prime-)
(not (Flag520-))
)

)
(:action Prim520Action-1
:parameters ()
:precondition
(and
(not (ABOVEOF18-ROBOT))
)
:effect
(and
(Flag520prime-)
(not (Flag520-))
)

)
(:action Flag522Action-13
:parameters ()
:precondition
(and
(BELOWOF1-ROBOT)
)
:effect
(and
(Flag522-)
(Flag522prime-)
)

)
(:action MOVEUP-ROBOT
:parameters ()
:precondition
(and
(Flag2-)
(Flag2prime-)
(Flag519prime-)
(Flag516prime-)
(Flag512prime-)
(Flag507prime-)
(Flag501prime-)
(Flag493prime-)
(Flag484prime-)
(Flag475prime-)
(Flag465prime-)
(Flag453prime-)
(Flag439prime-)
(Flag425prime-)
(Flag408prime-)
(Flag391prime-)
(Flag374prime-)
(Flag355prime-)
(Flag336prime-)
(Flag522prime-)
)
:effect
(and
(when
(and
(Flag519-)
)
(and
(ROW18-ROBOT)
(not (NOT-ROW18-ROBOT))
)
)
(when
(and
(Flag516-)
)
(and
(ROW17-ROBOT)
(not (NOT-ROW17-ROBOT))
)
)
(when
(and
(Flag512-)
)
(and
(ROW16-ROBOT)
(not (NOT-ROW16-ROBOT))
)
)
(when
(and
(Flag507-)
)
(and
(ROW15-ROBOT)
(not (NOT-ROW15-ROBOT))
)
)
(when
(and
(Flag501-)
)
(and
(ROW14-ROBOT)
(not (NOT-ROW14-ROBOT))
)
)
(when
(and
(Flag493-)
)
(and
(ROW13-ROBOT)
(not (NOT-ROW13-ROBOT))
)
)
(when
(and
(Flag484-)
)
(and
(ROW12-ROBOT)
(not (NOT-ROW12-ROBOT))
)
)
(when
(and
(Flag475-)
)
(and
(ROW11-ROBOT)
(not (NOT-ROW11-ROBOT))
)
)
(when
(and
(Flag465-)
)
(and
(ROW10-ROBOT)
(not (NOT-ROW10-ROBOT))
)
)
(when
(and
(Flag453-)
)
(and
(ROW9-ROBOT)
(not (NOT-ROW9-ROBOT))
)
)
(when
(and
(Flag439-)
)
(and
(ROW8-ROBOT)
(not (NOT-ROW8-ROBOT))
)
)
(when
(and
(Flag425-)
)
(and
(ROW7-ROBOT)
(not (NOT-ROW7-ROBOT))
)
)
(when
(and
(Flag408-)
)
(and
(ROW6-ROBOT)
(not (NOT-ROW6-ROBOT))
)
)
(when
(and
(Flag391-)
)
(and
(ROW5-ROBOT)
(not (NOT-ROW5-ROBOT))
)
)
(when
(and
(Flag374-)
)
(and
(ROW4-ROBOT)
(not (NOT-ROW4-ROBOT))
)
)
(when
(and
(Flag355-)
)
(and
(ROW3-ROBOT)
(not (NOT-ROW3-ROBOT))
)
)
(when
(and
(Flag336-)
)
(and
(ROW2-ROBOT)
(not (NOT-ROW2-ROBOT))
)
)
(when
(and
(Flag522-)
)
(and
(ROW1-ROBOT)
(not (NOT-ROW1-ROBOT))
)
)
(when
(and
(ROW17-ROBOT)
)
(and
(ROW18-ROBOT)
(NOT-ROW17-ROBOT)
(not (NOT-ROW18-ROBOT))
(not (ROW17-ROBOT))
)
)
(when
(and
(ROW16-ROBOT)
)
(and
(ROW17-ROBOT)
(NOT-ROW16-ROBOT)
(not (NOT-ROW17-ROBOT))
(not (ROW16-ROBOT))
)
)
(when
(and
(ROW15-ROBOT)
)
(and
(ROW16-ROBOT)
(NOT-ROW15-ROBOT)
(not (NOT-ROW16-ROBOT))
(not (ROW15-ROBOT))
)
)
(when
(and
(ROW14-ROBOT)
)
(and
(ROW15-ROBOT)
(NOT-ROW14-ROBOT)
(not (NOT-ROW15-ROBOT))
(not (ROW14-ROBOT))
)
)
(when
(and
(ROW13-ROBOT)
)
(and
(ROW14-ROBOT)
(NOT-ROW13-ROBOT)
(not (NOT-ROW14-ROBOT))
(not (ROW13-ROBOT))
)
)
(when
(and
(ROW12-ROBOT)
)
(and
(ROW13-ROBOT)
(NOT-ROW12-ROBOT)
(not (NOT-ROW13-ROBOT))
(not (ROW12-ROBOT))
)
)
(when
(and
(ROW11-ROBOT)
)
(and
(ROW12-ROBOT)
(NOT-ROW11-ROBOT)
(not (NOT-ROW12-ROBOT))
(not (ROW11-ROBOT))
)
)
(when
(and
(ROW10-ROBOT)
)
(and
(ROW11-ROBOT)
(NOT-ROW10-ROBOT)
(not (NOT-ROW11-ROBOT))
(not (ROW10-ROBOT))
)
)
(when
(and
(ROW9-ROBOT)
)
(and
(ROW10-ROBOT)
(NOT-ROW9-ROBOT)
(not (NOT-ROW10-ROBOT))
(not (ROW9-ROBOT))
)
)
(when
(and
(ROW8-ROBOT)
)
(and
(ROW9-ROBOT)
(NOT-ROW8-ROBOT)
(not (NOT-ROW9-ROBOT))
(not (ROW8-ROBOT))
)
)
(when
(and
(ROW7-ROBOT)
)
(and
(ROW8-ROBOT)
(NOT-ROW7-ROBOT)
(not (NOT-ROW8-ROBOT))
(not (ROW7-ROBOT))
)
)
(when
(and
(ROW6-ROBOT)
)
(and
(ROW7-ROBOT)
(NOT-ROW6-ROBOT)
(not (NOT-ROW7-ROBOT))
(not (ROW6-ROBOT))
)
)
(when
(and
(ROW5-ROBOT)
)
(and
(ROW6-ROBOT)
(NOT-ROW5-ROBOT)
(not (NOT-ROW6-ROBOT))
(not (ROW5-ROBOT))
)
)
(when
(and
(ROW4-ROBOT)
)
(and
(ROW5-ROBOT)
(NOT-ROW4-ROBOT)
(not (NOT-ROW5-ROBOT))
(not (ROW4-ROBOT))
)
)
(when
(and
(ROW3-ROBOT)
)
(and
(ROW4-ROBOT)
(NOT-ROW3-ROBOT)
(not (NOT-ROW4-ROBOT))
(not (ROW3-ROBOT))
)
)
(when
(and
(ROW2-ROBOT)
)
(and
(ROW3-ROBOT)
(NOT-ROW2-ROBOT)
(not (NOT-ROW3-ROBOT))
(not (ROW2-ROBOT))
)
)
(when
(and
(ROW1-ROBOT)
)
(and
(ROW2-ROBOT)
(NOT-ROW1-ROBOT)
(not (NOT-ROW2-ROBOT))
(not (ROW1-ROBOT))
)
)
(when
(and
(ROW0-ROBOT)
)
(and
(ROW1-ROBOT)
(NOT-ROW0-ROBOT)
(not (NOT-ROW1-ROBOT))
(not (ROW0-ROBOT))
)
)
(when
(and
(BELOWOF14-ROBOT)
)
(and
(BELOWOF19-ROBOT)
(NOT-BELOWOF18-ROBOT)
(BELOWOF18-ROBOT)
(NOT-BELOWOF17-ROBOT)
(BELOWOF17-ROBOT)
(NOT-BELOWOF16-ROBOT)
(BELOWOF16-ROBOT)
(NOT-BELOWOF15-ROBOT)
(BELOWOF15-ROBOT)
(NOT-BELOWOF14-ROBOT)
(not (NOT-BELOWOF19-ROBOT))
(not (BELOWOF14-ROBOT))
)
)
(when
(and
(BELOWOF8-ROBOT)
)
(and
(BELOWOF19-ROBOT)
(NOT-BELOWOF18-ROBOT)
(BELOWOF18-ROBOT)
(NOT-BELOWOF17-ROBOT)
(BELOWOF17-ROBOT)
(NOT-BELOWOF16-ROBOT)
(BELOWOF16-ROBOT)
(NOT-BELOWOF15-ROBOT)
(BELOWOF15-ROBOT)
(NOT-BELOWOF14-ROBOT)
(BELOWOF14-ROBOT)
(NOT-BELOWOF13-ROBOT)
(BELOWOF13-ROBOT)
(NOT-BELOWOF12-ROBOT)
(BELOWOF12-ROBOT)
(NOT-BELOWOF11-ROBOT)
(BELOWOF11-ROBOT)
(NOT-BELOWOF10-ROBOT)
(BELOWOF10-ROBOT)
(NOT-BELOWOF9-ROBOT)
(BELOWOF9-ROBOT)
(NOT-BELOWOF8-ROBOT)
(not (NOT-BELOWOF19-ROBOT))
(not (BELOWOF8-ROBOT))
)
)
(when
(and
(BELOWOF13-ROBOT)
)
(and
(BELOWOF19-ROBOT)
(NOT-BELOWOF18-ROBOT)
(BELOWOF18-ROBOT)
(NOT-BELOWOF17-ROBOT)
(BELOWOF17-ROBOT)
(NOT-BELOWOF16-ROBOT)
(BELOWOF16-ROBOT)
(NOT-BELOWOF15-ROBOT)
(BELOWOF15-ROBOT)
(NOT-BELOWOF14-ROBOT)
(BELOWOF14-ROBOT)
(NOT-BELOWOF13-ROBOT)
(not (NOT-BELOWOF19-ROBOT))
(not (BELOWOF13-ROBOT))
)
)
(when
(and
(BELOWOF3-ROBOT)
)
(and
(BELOWOF19-ROBOT)
(NOT-BELOWOF18-ROBOT)
(BELOWOF18-ROBOT)
(NOT-BELOWOF17-ROBOT)
(BELOWOF17-ROBOT)
(NOT-BELOWOF16-ROBOT)
(BELOWOF16-ROBOT)
(NOT-BELOWOF15-ROBOT)
(BELOWOF15-ROBOT)
(NOT-BELOWOF14-ROBOT)
(BELOWOF14-ROBOT)
(NOT-BELOWOF13-ROBOT)
(BELOWOF13-ROBOT)
(NOT-BELOWOF12-ROBOT)
(BELOWOF12-ROBOT)
(NOT-BELOWOF11-ROBOT)
(BELOWOF11-ROBOT)
(NOT-BELOWOF10-ROBOT)
(BELOWOF10-ROBOT)
(NOT-BELOWOF9-ROBOT)
(BELOWOF9-ROBOT)
(NOT-BELOWOF8-ROBOT)
(BELOWOF8-ROBOT)
(NOT-BELOWOF7-ROBOT)
(BELOWOF7-ROBOT)
(NOT-BELOWOF6-ROBOT)
(BELOWOF6-ROBOT)
(NOT-BELOWOF5-ROBOT)
(BELOWOF5-ROBOT)
(NOT-BELOWOF4-ROBOT)
(BELOWOF4-ROBOT)
(NOT-BELOWOF3-ROBOT)
(not (NOT-BELOWOF19-ROBOT))
(not (BELOWOF3-ROBOT))
)
)
(when
(and
(BELOWOF9-ROBOT)
)
(and
(BELOWOF19-ROBOT)
(NOT-BELOWOF18-ROBOT)
(BELOWOF18-ROBOT)
(NOT-BELOWOF17-ROBOT)
(BELOWOF17-ROBOT)
(NOT-BELOWOF16-ROBOT)
(BELOWOF16-ROBOT)
(NOT-BELOWOF15-ROBOT)
(BELOWOF15-ROBOT)
(NOT-BELOWOF14-ROBOT)
(BELOWOF14-ROBOT)
(NOT-BELOWOF13-ROBOT)
(BELOWOF13-ROBOT)
(NOT-BELOWOF12-ROBOT)
(BELOWOF12-ROBOT)
(NOT-BELOWOF11-ROBOT)
(BELOWOF11-ROBOT)
(NOT-BELOWOF10-ROBOT)
(BELOWOF10-ROBOT)
(NOT-BELOWOF9-ROBOT)
(not (NOT-BELOWOF19-ROBOT))
(not (BELOWOF9-ROBOT))
)
)
(when
(and
(BELOWOF7-ROBOT)
)
(and
(BELOWOF19-ROBOT)
(NOT-BELOWOF18-ROBOT)
(BELOWOF18-ROBOT)
(NOT-BELOWOF17-ROBOT)
(BELOWOF17-ROBOT)
(NOT-BELOWOF16-ROBOT)
(BELOWOF16-ROBOT)
(NOT-BELOWOF15-ROBOT)
(BELOWOF15-ROBOT)
(NOT-BELOWOF14-ROBOT)
(BELOWOF14-ROBOT)
(NOT-BELOWOF13-ROBOT)
(BELOWOF13-ROBOT)
(NOT-BELOWOF12-ROBOT)
(BELOWOF12-ROBOT)
(NOT-BELOWOF11-ROBOT)
(BELOWOF11-ROBOT)
(NOT-BELOWOF10-ROBOT)
(BELOWOF10-ROBOT)
(NOT-BELOWOF9-ROBOT)
(BELOWOF9-ROBOT)
(NOT-BELOWOF8-ROBOT)
(BELOWOF8-ROBOT)
(NOT-BELOWOF7-ROBOT)
(not (NOT-BELOWOF19-ROBOT))
(not (BELOWOF7-ROBOT))
)
)
(when
(and
(BELOWOF12-ROBOT)
)
(and
(BELOWOF19-ROBOT)
(NOT-BELOWOF18-ROBOT)
(BELOWOF18-ROBOT)
(NOT-BELOWOF17-ROBOT)
(BELOWOF17-ROBOT)
(NOT-BELOWOF16-ROBOT)
(BELOWOF16-ROBOT)
(NOT-BELOWOF15-ROBOT)
(BELOWOF15-ROBOT)
(NOT-BELOWOF14-ROBOT)
(BELOWOF14-ROBOT)
(NOT-BELOWOF13-ROBOT)
(BELOWOF13-ROBOT)
(NOT-BELOWOF12-ROBOT)
(not (NOT-BELOWOF19-ROBOT))
(not (BELOWOF12-ROBOT))
)
)
(when
(and
(BELOWOF18-ROBOT)
)
(and
(BELOWOF19-ROBOT)
(NOT-BELOWOF18-ROBOT)
(not (NOT-BELOWOF19-ROBOT))
(not (BELOWOF18-ROBOT))
)
)
(when
(and
(BELOWOF11-ROBOT)
)
(and
(BELOWOF19-ROBOT)
(NOT-BELOWOF18-ROBOT)
(BELOWOF18-ROBOT)
(NOT-BELOWOF17-ROBOT)
(BELOWOF17-ROBOT)
(NOT-BELOWOF16-ROBOT)
(BELOWOF16-ROBOT)
(NOT-BELOWOF15-ROBOT)
(BELOWOF15-ROBOT)
(NOT-BELOWOF14-ROBOT)
(BELOWOF14-ROBOT)
(NOT-BELOWOF13-ROBOT)
(BELOWOF13-ROBOT)
(NOT-BELOWOF12-ROBOT)
(BELOWOF12-ROBOT)
(NOT-BELOWOF11-ROBOT)
(not (NOT-BELOWOF19-ROBOT))
(not (BELOWOF11-ROBOT))
)
)
(when
(and
(BELOWOF4-ROBOT)
)
(and
(BELOWOF19-ROBOT)
(NOT-BELOWOF18-ROBOT)
(BELOWOF18-ROBOT)
(NOT-BELOWOF17-ROBOT)
(BELOWOF17-ROBOT)
(NOT-BELOWOF16-ROBOT)
(BELOWOF16-ROBOT)
(NOT-BELOWOF15-ROBOT)
(BELOWOF15-ROBOT)
(NOT-BELOWOF14-ROBOT)
(BELOWOF14-ROBOT)
(NOT-BELOWOF13-ROBOT)
(BELOWOF13-ROBOT)
(NOT-BELOWOF12-ROBOT)
(BELOWOF12-ROBOT)
(NOT-BELOWOF11-ROBOT)
(BELOWOF11-ROBOT)
(NOT-BELOWOF10-ROBOT)
(BELOWOF10-ROBOT)
(NOT-BELOWOF9-ROBOT)
(BELOWOF9-ROBOT)
(NOT-BELOWOF8-ROBOT)
(BELOWOF8-ROBOT)
(NOT-BELOWOF7-ROBOT)
(BELOWOF7-ROBOT)
(NOT-BELOWOF6-ROBOT)
(BELOWOF6-ROBOT)
(NOT-BELOWOF5-ROBOT)
(BELOWOF5-ROBOT)
(NOT-BELOWOF4-ROBOT)
(not (NOT-BELOWOF19-ROBOT))
(not (BELOWOF4-ROBOT))
)
)
(when
(and
(BELOWOF1-ROBOT)
)
(and
(BELOWOF19-ROBOT)
(NOT-BELOWOF18-ROBOT)
(BELOWOF18-ROBOT)
(NOT-BELOWOF17-ROBOT)
(BELOWOF17-ROBOT)
(NOT-BELOWOF16-ROBOT)
(BELOWOF16-ROBOT)
(NOT-BELOWOF15-ROBOT)
(BELOWOF15-ROBOT)
(NOT-BELOWOF14-ROBOT)
(BELOWOF14-ROBOT)
(NOT-BELOWOF13-ROBOT)
(BELOWOF13-ROBOT)
(NOT-BELOWOF12-ROBOT)
(BELOWOF12-ROBOT)
(NOT-BELOWOF11-ROBOT)
(BELOWOF11-ROBOT)
(NOT-BELOWOF10-ROBOT)
(BELOWOF10-ROBOT)
(NOT-BELOWOF9-ROBOT)
(BELOWOF9-ROBOT)
(NOT-BELOWOF8-ROBOT)
(BELOWOF8-ROBOT)
(NOT-BELOWOF7-ROBOT)
(BELOWOF7-ROBOT)
(NOT-BELOWOF6-ROBOT)
(BELOWOF6-ROBOT)
(NOT-BELOWOF5-ROBOT)
(BELOWOF5-ROBOT)
(NOT-BELOWOF4-ROBOT)
(BELOWOF4-ROBOT)
(NOT-BELOWOF3-ROBOT)
(BELOWOF3-ROBOT)
(NOT-BELOWOF2-ROBOT)
(BELOWOF2-ROBOT)
(NOT-BELOWOF1-ROBOT)
(not (NOT-BELOWOF19-ROBOT))
(not (BELOWOF1-ROBOT))
)
)
(when
(and
(BELOWOF2-ROBOT)
)
(and
(BELOWOF19-ROBOT)
(NOT-BELOWOF18-ROBOT)
(BELOWOF18-ROBOT)
(NOT-BELOWOF17-ROBOT)
(BELOWOF17-ROBOT)
(NOT-BELOWOF16-ROBOT)
(BELOWOF16-ROBOT)
(NOT-BELOWOF15-ROBOT)
(BELOWOF15-ROBOT)
(NOT-BELOWOF14-ROBOT)
(BELOWOF14-ROBOT)
(NOT-BELOWOF13-ROBOT)
(BELOWOF13-ROBOT)
(NOT-BELOWOF12-ROBOT)
(BELOWOF12-ROBOT)
(NOT-BELOWOF11-ROBOT)
(BELOWOF11-ROBOT)
(NOT-BELOWOF10-ROBOT)
(BELOWOF10-ROBOT)
(NOT-BELOWOF9-ROBOT)
(BELOWOF9-ROBOT)
(NOT-BELOWOF8-ROBOT)
(BELOWOF8-ROBOT)
(NOT-BELOWOF7-ROBOT)
(BELOWOF7-ROBOT)
(NOT-BELOWOF6-ROBOT)
(BELOWOF6-ROBOT)
(NOT-BELOWOF5-ROBOT)
(BELOWOF5-ROBOT)
(NOT-BELOWOF4-ROBOT)
(BELOWOF4-ROBOT)
(NOT-BELOWOF3-ROBOT)
(BELOWOF3-ROBOT)
(NOT-BELOWOF2-ROBOT)
(not (NOT-BELOWOF19-ROBOT))
(not (BELOWOF2-ROBOT))
)
)
(when
(and
(BELOWOF5-ROBOT)
)
(and
(BELOWOF19-ROBOT)
(NOT-BELOWOF18-ROBOT)
(BELOWOF18-ROBOT)
(NOT-BELOWOF17-ROBOT)
(BELOWOF17-ROBOT)
(NOT-BELOWOF16-ROBOT)
(BELOWOF16-ROBOT)
(NOT-BELOWOF15-ROBOT)
(BELOWOF15-ROBOT)
(NOT-BELOWOF14-ROBOT)
(BELOWOF14-ROBOT)
(NOT-BELOWOF13-ROBOT)
(BELOWOF13-ROBOT)
(NOT-BELOWOF12-ROBOT)
(BELOWOF12-ROBOT)
(NOT-BELOWOF11-ROBOT)
(BELOWOF11-ROBOT)
(NOT-BELOWOF10-ROBOT)
(BELOWOF10-ROBOT)
(NOT-BELOWOF9-ROBOT)
(BELOWOF9-ROBOT)
(NOT-BELOWOF8-ROBOT)
(BELOWOF8-ROBOT)
(NOT-BELOWOF7-ROBOT)
(BELOWOF7-ROBOT)
(NOT-BELOWOF6-ROBOT)
(BELOWOF6-ROBOT)
(NOT-BELOWOF5-ROBOT)
(not (NOT-BELOWOF19-ROBOT))
(not (BELOWOF5-ROBOT))
)
)
(when
(and
(BELOWOF17-ROBOT)
)
(and
(BELOWOF19-ROBOT)
(NOT-BELOWOF18-ROBOT)
(BELOWOF18-ROBOT)
(NOT-BELOWOF17-ROBOT)
(not (NOT-BELOWOF19-ROBOT))
(not (BELOWOF17-ROBOT))
)
)
(when
(and
(BELOWOF6-ROBOT)
)
(and
(BELOWOF19-ROBOT)
(NOT-BELOWOF18-ROBOT)
(BELOWOF18-ROBOT)
(NOT-BELOWOF17-ROBOT)
(BELOWOF17-ROBOT)
(NOT-BELOWOF16-ROBOT)
(BELOWOF16-ROBOT)
(NOT-BELOWOF15-ROBOT)
(BELOWOF15-ROBOT)
(NOT-BELOWOF14-ROBOT)
(BELOWOF14-ROBOT)
(NOT-BELOWOF13-ROBOT)
(BELOWOF13-ROBOT)
(NOT-BELOWOF12-ROBOT)
(BELOWOF12-ROBOT)
(NOT-BELOWOF11-ROBOT)
(BELOWOF11-ROBOT)
(NOT-BELOWOF10-ROBOT)
(BELOWOF10-ROBOT)
(NOT-BELOWOF9-ROBOT)
(BELOWOF9-ROBOT)
(NOT-BELOWOF8-ROBOT)
(BELOWOF8-ROBOT)
(NOT-BELOWOF7-ROBOT)
(BELOWOF7-ROBOT)
(NOT-BELOWOF6-ROBOT)
(not (NOT-BELOWOF19-ROBOT))
(not (BELOWOF6-ROBOT))
)
)
(when
(and
(BELOWOF15-ROBOT)
)
(and
(BELOWOF19-ROBOT)
(NOT-BELOWOF18-ROBOT)
(BELOWOF18-ROBOT)
(NOT-BELOWOF17-ROBOT)
(BELOWOF17-ROBOT)
(NOT-BELOWOF16-ROBOT)
(BELOWOF16-ROBOT)
(NOT-BELOWOF15-ROBOT)
(not (NOT-BELOWOF19-ROBOT))
(not (BELOWOF15-ROBOT))
)
)
(when
(and
(BELOWOF16-ROBOT)
)
(and
(BELOWOF19-ROBOT)
(NOT-BELOWOF18-ROBOT)
(BELOWOF18-ROBOT)
(NOT-BELOWOF17-ROBOT)
(BELOWOF17-ROBOT)
(NOT-BELOWOF16-ROBOT)
(not (NOT-BELOWOF19-ROBOT))
(not (BELOWOF16-ROBOT))
)
)
(when
(and
(BELOWOF10-ROBOT)
)
(and
(BELOWOF19-ROBOT)
(NOT-BELOWOF18-ROBOT)
(BELOWOF18-ROBOT)
(NOT-BELOWOF17-ROBOT)
(BELOWOF17-ROBOT)
(NOT-BELOWOF16-ROBOT)
(BELOWOF16-ROBOT)
(NOT-BELOWOF15-ROBOT)
(BELOWOF15-ROBOT)
(NOT-BELOWOF14-ROBOT)
(BELOWOF14-ROBOT)
(NOT-BELOWOF13-ROBOT)
(BELOWOF13-ROBOT)
(NOT-BELOWOF12-ROBOT)
(BELOWOF12-ROBOT)
(NOT-BELOWOF11-ROBOT)
(BELOWOF11-ROBOT)
(NOT-BELOWOF10-ROBOT)
(not (NOT-BELOWOF19-ROBOT))
(not (BELOWOF10-ROBOT))
)
)
(when
(and
(ABOVEOF18-ROBOT)
)
(and
(ABOVEOF17-ROBOT)
(ABOVEOF16-ROBOT)
(ABOVEOF15-ROBOT)
(ABOVEOF14-ROBOT)
(ABOVEOF13-ROBOT)
(ABOVEOF12-ROBOT)
(ABOVEOF11-ROBOT)
(ABOVEOF10-ROBOT)
(ABOVEOF9-ROBOT)
(ABOVEOF8-ROBOT)
(ABOVEOF7-ROBOT)
(ABOVEOF6-ROBOT)
(ABOVEOF5-ROBOT)
(ABOVEOF4-ROBOT)
(ABOVEOF3-ROBOT)
(ABOVEOF2-ROBOT)
(not (NOT-ABOVEOF18-ROBOT))
(not (NOT-ABOVEOF17-ROBOT))
(not (NOT-ABOVEOF16-ROBOT))
(not (NOT-ABOVEOF15-ROBOT))
(not (NOT-ABOVEOF14-ROBOT))
(not (NOT-ABOVEOF13-ROBOT))
(not (NOT-ABOVEOF12-ROBOT))
(not (NOT-ABOVEOF11-ROBOT))
(not (NOT-ABOVEOF10-ROBOT))
(not (NOT-ABOVEOF9-ROBOT))
(not (NOT-ABOVEOF8-ROBOT))
(not (NOT-ABOVEOF7-ROBOT))
(not (NOT-ABOVEOF6-ROBOT))
(not (NOT-ABOVEOF5-ROBOT))
(not (NOT-ABOVEOF4-ROBOT))
(not (NOT-ABOVEOF3-ROBOT))
(not (NOT-ABOVEOF2-ROBOT))
)
)
(when
(and
(ABOVEOF17-ROBOT)
)
(and
(ABOVEOF18-ROBOT)
(ABOVEOF16-ROBOT)
(ABOVEOF15-ROBOT)
(ABOVEOF14-ROBOT)
(ABOVEOF13-ROBOT)
(ABOVEOF12-ROBOT)
(ABOVEOF11-ROBOT)
(ABOVEOF10-ROBOT)
(ABOVEOF9-ROBOT)
(ABOVEOF8-ROBOT)
(ABOVEOF7-ROBOT)
(ABOVEOF6-ROBOT)
(ABOVEOF5-ROBOT)
(ABOVEOF4-ROBOT)
(ABOVEOF3-ROBOT)
(ABOVEOF2-ROBOT)
(not (NOT-ABOVEOF18-ROBOT))
(not (NOT-ABOVEOF17-ROBOT))
(not (NOT-ABOVEOF16-ROBOT))
(not (NOT-ABOVEOF15-ROBOT))
(not (NOT-ABOVEOF14-ROBOT))
(not (NOT-ABOVEOF13-ROBOT))
(not (NOT-ABOVEOF12-ROBOT))
(not (NOT-ABOVEOF11-ROBOT))
(not (NOT-ABOVEOF10-ROBOT))
(not (NOT-ABOVEOF9-ROBOT))
(not (NOT-ABOVEOF8-ROBOT))
(not (NOT-ABOVEOF7-ROBOT))
(not (NOT-ABOVEOF6-ROBOT))
(not (NOT-ABOVEOF5-ROBOT))
(not (NOT-ABOVEOF4-ROBOT))
(not (NOT-ABOVEOF3-ROBOT))
(not (NOT-ABOVEOF2-ROBOT))
)
)
(when
(and
(ABOVEOF16-ROBOT)
)
(and
(ABOVEOF17-ROBOT)
(ABOVEOF15-ROBOT)
(ABOVEOF14-ROBOT)
(ABOVEOF13-ROBOT)
(ABOVEOF12-ROBOT)
(ABOVEOF11-ROBOT)
(ABOVEOF10-ROBOT)
(ABOVEOF9-ROBOT)
(ABOVEOF8-ROBOT)
(ABOVEOF7-ROBOT)
(ABOVEOF6-ROBOT)
(ABOVEOF5-ROBOT)
(ABOVEOF4-ROBOT)
(ABOVEOF3-ROBOT)
(ABOVEOF2-ROBOT)
(not (NOT-ABOVEOF17-ROBOT))
(not (NOT-ABOVEOF16-ROBOT))
(not (NOT-ABOVEOF15-ROBOT))
(not (NOT-ABOVEOF14-ROBOT))
(not (NOT-ABOVEOF13-ROBOT))
(not (NOT-ABOVEOF12-ROBOT))
(not (NOT-ABOVEOF11-ROBOT))
(not (NOT-ABOVEOF10-ROBOT))
(not (NOT-ABOVEOF9-ROBOT))
(not (NOT-ABOVEOF8-ROBOT))
(not (NOT-ABOVEOF7-ROBOT))
(not (NOT-ABOVEOF6-ROBOT))
(not (NOT-ABOVEOF5-ROBOT))
(not (NOT-ABOVEOF4-ROBOT))
(not (NOT-ABOVEOF3-ROBOT))
(not (NOT-ABOVEOF2-ROBOT))
)
)
(when
(and
(ABOVEOF15-ROBOT)
)
(and
(ABOVEOF16-ROBOT)
(ABOVEOF14-ROBOT)
(ABOVEOF13-ROBOT)
(ABOVEOF12-ROBOT)
(ABOVEOF11-ROBOT)
(ABOVEOF10-ROBOT)
(ABOVEOF9-ROBOT)
(ABOVEOF8-ROBOT)
(ABOVEOF7-ROBOT)
(ABOVEOF6-ROBOT)
(ABOVEOF5-ROBOT)
(ABOVEOF4-ROBOT)
(ABOVEOF3-ROBOT)
(ABOVEOF2-ROBOT)
(not (NOT-ABOVEOF16-ROBOT))
(not (NOT-ABOVEOF15-ROBOT))
(not (NOT-ABOVEOF14-ROBOT))
(not (NOT-ABOVEOF13-ROBOT))
(not (NOT-ABOVEOF12-ROBOT))
(not (NOT-ABOVEOF11-ROBOT))
(not (NOT-ABOVEOF10-ROBOT))
(not (NOT-ABOVEOF9-ROBOT))
(not (NOT-ABOVEOF8-ROBOT))
(not (NOT-ABOVEOF7-ROBOT))
(not (NOT-ABOVEOF6-ROBOT))
(not (NOT-ABOVEOF5-ROBOT))
(not (NOT-ABOVEOF4-ROBOT))
(not (NOT-ABOVEOF3-ROBOT))
(not (NOT-ABOVEOF2-ROBOT))
)
)
(when
(and
(ABOVEOF14-ROBOT)
)
(and
(ABOVEOF15-ROBOT)
(ABOVEOF13-ROBOT)
(ABOVEOF12-ROBOT)
(ABOVEOF11-ROBOT)
(ABOVEOF10-ROBOT)
(ABOVEOF9-ROBOT)
(ABOVEOF8-ROBOT)
(ABOVEOF7-ROBOT)
(ABOVEOF6-ROBOT)
(ABOVEOF5-ROBOT)
(ABOVEOF4-ROBOT)
(ABOVEOF3-ROBOT)
(ABOVEOF2-ROBOT)
(not (NOT-ABOVEOF15-ROBOT))
(not (NOT-ABOVEOF14-ROBOT))
(not (NOT-ABOVEOF13-ROBOT))
(not (NOT-ABOVEOF12-ROBOT))
(not (NOT-ABOVEOF11-ROBOT))
(not (NOT-ABOVEOF10-ROBOT))
(not (NOT-ABOVEOF9-ROBOT))
(not (NOT-ABOVEOF8-ROBOT))
(not (NOT-ABOVEOF7-ROBOT))
(not (NOT-ABOVEOF6-ROBOT))
(not (NOT-ABOVEOF5-ROBOT))
(not (NOT-ABOVEOF4-ROBOT))
(not (NOT-ABOVEOF3-ROBOT))
(not (NOT-ABOVEOF2-ROBOT))
)
)
(when
(and
(ABOVEOF13-ROBOT)
)
(and
(ABOVEOF14-ROBOT)
(ABOVEOF12-ROBOT)
(ABOVEOF11-ROBOT)
(ABOVEOF10-ROBOT)
(ABOVEOF9-ROBOT)
(ABOVEOF8-ROBOT)
(ABOVEOF7-ROBOT)
(ABOVEOF6-ROBOT)
(ABOVEOF5-ROBOT)
(ABOVEOF4-ROBOT)
(ABOVEOF3-ROBOT)
(ABOVEOF2-ROBOT)
(not (NOT-ABOVEOF14-ROBOT))
(not (NOT-ABOVEOF13-ROBOT))
(not (NOT-ABOVEOF12-ROBOT))
(not (NOT-ABOVEOF11-ROBOT))
(not (NOT-ABOVEOF10-ROBOT))
(not (NOT-ABOVEOF9-ROBOT))
(not (NOT-ABOVEOF8-ROBOT))
(not (NOT-ABOVEOF7-ROBOT))
(not (NOT-ABOVEOF6-ROBOT))
(not (NOT-ABOVEOF5-ROBOT))
(not (NOT-ABOVEOF4-ROBOT))
(not (NOT-ABOVEOF3-ROBOT))
(not (NOT-ABOVEOF2-ROBOT))
)
)
(when
(and
(ABOVEOF12-ROBOT)
)
(and
(ABOVEOF13-ROBOT)
(ABOVEOF11-ROBOT)
(ABOVEOF10-ROBOT)
(ABOVEOF9-ROBOT)
(ABOVEOF8-ROBOT)
(ABOVEOF7-ROBOT)
(ABOVEOF6-ROBOT)
(ABOVEOF5-ROBOT)
(ABOVEOF4-ROBOT)
(ABOVEOF3-ROBOT)
(ABOVEOF2-ROBOT)
(not (NOT-ABOVEOF13-ROBOT))
(not (NOT-ABOVEOF12-ROBOT))
(not (NOT-ABOVEOF11-ROBOT))
(not (NOT-ABOVEOF10-ROBOT))
(not (NOT-ABOVEOF9-ROBOT))
(not (NOT-ABOVEOF8-ROBOT))
(not (NOT-ABOVEOF7-ROBOT))
(not (NOT-ABOVEOF6-ROBOT))
(not (NOT-ABOVEOF5-ROBOT))
(not (NOT-ABOVEOF4-ROBOT))
(not (NOT-ABOVEOF3-ROBOT))
(not (NOT-ABOVEOF2-ROBOT))
)
)
(when
(and
(ABOVEOF11-ROBOT)
)
(and
(ABOVEOF12-ROBOT)
(ABOVEOF10-ROBOT)
(ABOVEOF9-ROBOT)
(ABOVEOF8-ROBOT)
(ABOVEOF7-ROBOT)
(ABOVEOF6-ROBOT)
(ABOVEOF5-ROBOT)
(ABOVEOF4-ROBOT)
(ABOVEOF3-ROBOT)
(ABOVEOF2-ROBOT)
(not (NOT-ABOVEOF12-ROBOT))
(not (NOT-ABOVEOF11-ROBOT))
(not (NOT-ABOVEOF10-ROBOT))
(not (NOT-ABOVEOF9-ROBOT))
(not (NOT-ABOVEOF8-ROBOT))
(not (NOT-ABOVEOF7-ROBOT))
(not (NOT-ABOVEOF6-ROBOT))
(not (NOT-ABOVEOF5-ROBOT))
(not (NOT-ABOVEOF4-ROBOT))
(not (NOT-ABOVEOF3-ROBOT))
(not (NOT-ABOVEOF2-ROBOT))
)
)
(when
(and
(ABOVEOF10-ROBOT)
)
(and
(ABOVEOF11-ROBOT)
(ABOVEOF9-ROBOT)
(ABOVEOF8-ROBOT)
(ABOVEOF7-ROBOT)
(ABOVEOF6-ROBOT)
(ABOVEOF5-ROBOT)
(ABOVEOF4-ROBOT)
(ABOVEOF3-ROBOT)
(ABOVEOF2-ROBOT)
(not (NOT-ABOVEOF11-ROBOT))
(not (NOT-ABOVEOF10-ROBOT))
(not (NOT-ABOVEOF9-ROBOT))
(not (NOT-ABOVEOF8-ROBOT))
(not (NOT-ABOVEOF7-ROBOT))
(not (NOT-ABOVEOF6-ROBOT))
(not (NOT-ABOVEOF5-ROBOT))
(not (NOT-ABOVEOF4-ROBOT))
(not (NOT-ABOVEOF3-ROBOT))
(not (NOT-ABOVEOF2-ROBOT))
)
)
(when
(and
(ABOVEOF9-ROBOT)
)
(and
(ABOVEOF10-ROBOT)
(ABOVEOF8-ROBOT)
(ABOVEOF7-ROBOT)
(ABOVEOF6-ROBOT)
(ABOVEOF5-ROBOT)
(ABOVEOF4-ROBOT)
(ABOVEOF3-ROBOT)
(ABOVEOF2-ROBOT)
(not (NOT-ABOVEOF10-ROBOT))
(not (NOT-ABOVEOF9-ROBOT))
(not (NOT-ABOVEOF8-ROBOT))
(not (NOT-ABOVEOF7-ROBOT))
(not (NOT-ABOVEOF6-ROBOT))
(not (NOT-ABOVEOF5-ROBOT))
(not (NOT-ABOVEOF4-ROBOT))
(not (NOT-ABOVEOF3-ROBOT))
(not (NOT-ABOVEOF2-ROBOT))
)
)
(when
(and
(ABOVEOF8-ROBOT)
)
(and
(ABOVEOF9-ROBOT)
(ABOVEOF7-ROBOT)
(ABOVEOF6-ROBOT)
(ABOVEOF5-ROBOT)
(ABOVEOF4-ROBOT)
(ABOVEOF3-ROBOT)
(ABOVEOF2-ROBOT)
(not (NOT-ABOVEOF9-ROBOT))
(not (NOT-ABOVEOF8-ROBOT))
(not (NOT-ABOVEOF7-ROBOT))
(not (NOT-ABOVEOF6-ROBOT))
(not (NOT-ABOVEOF5-ROBOT))
(not (NOT-ABOVEOF4-ROBOT))
(not (NOT-ABOVEOF3-ROBOT))
(not (NOT-ABOVEOF2-ROBOT))
)
)
(when
(and
(ABOVEOF7-ROBOT)
)
(and
(ABOVEOF8-ROBOT)
(ABOVEOF6-ROBOT)
(ABOVEOF5-ROBOT)
(ABOVEOF4-ROBOT)
(ABOVEOF3-ROBOT)
(ABOVEOF2-ROBOT)
(not (NOT-ABOVEOF8-ROBOT))
(not (NOT-ABOVEOF7-ROBOT))
(not (NOT-ABOVEOF6-ROBOT))
(not (NOT-ABOVEOF5-ROBOT))
(not (NOT-ABOVEOF4-ROBOT))
(not (NOT-ABOVEOF3-ROBOT))
(not (NOT-ABOVEOF2-ROBOT))
)
)
(when
(and
(ABOVEOF6-ROBOT)
)
(and
(ABOVEOF7-ROBOT)
(ABOVEOF5-ROBOT)
(ABOVEOF4-ROBOT)
(ABOVEOF3-ROBOT)
(ABOVEOF2-ROBOT)
(not (NOT-ABOVEOF7-ROBOT))
(not (NOT-ABOVEOF6-ROBOT))
(not (NOT-ABOVEOF5-ROBOT))
(not (NOT-ABOVEOF4-ROBOT))
(not (NOT-ABOVEOF3-ROBOT))
(not (NOT-ABOVEOF2-ROBOT))
)
)
(when
(and
(ABOVEOF5-ROBOT)
)
(and
(ABOVEOF6-ROBOT)
(ABOVEOF4-ROBOT)
(ABOVEOF3-ROBOT)
(ABOVEOF2-ROBOT)
(not (NOT-ABOVEOF6-ROBOT))
(not (NOT-ABOVEOF5-ROBOT))
(not (NOT-ABOVEOF4-ROBOT))
(not (NOT-ABOVEOF3-ROBOT))
(not (NOT-ABOVEOF2-ROBOT))
)
)
(when
(and
(ABOVEOF4-ROBOT)
)
(and
(ABOVEOF5-ROBOT)
(ABOVEOF3-ROBOT)
(ABOVEOF2-ROBOT)
(not (NOT-ABOVEOF5-ROBOT))
(not (NOT-ABOVEOF4-ROBOT))
(not (NOT-ABOVEOF3-ROBOT))
(not (NOT-ABOVEOF2-ROBOT))
)
)
(when
(and
(ABOVEOF3-ROBOT)
)
(and
(ABOVEOF4-ROBOT)
(ABOVEOF2-ROBOT)
(not (NOT-ABOVEOF4-ROBOT))
(not (NOT-ABOVEOF3-ROBOT))
(not (NOT-ABOVEOF2-ROBOT))
)
)
(when
(and
(ABOVEOF2-ROBOT)
)
(and
(ABOVEOF3-ROBOT)
(not (NOT-ABOVEOF3-ROBOT))
(not (NOT-ABOVEOF2-ROBOT))
)
)
(when
(and
(ABOVEOF1-ROBOT)
)
(and
(ABOVEOF2-ROBOT)
(not (NOT-ABOVEOF2-ROBOT))
)
)
(ABOVEOF1-ROBOT)
(CHECKCONSISTENCY-)
(not (NOT-ABOVEOF1-ROBOT))
(not (NOT-CHECKCONSISTENCY-))
(not (Flag281prime-))
(not (Flag280prime-))
(not (Flag261prime-))
(not (Flag24prime-))
(not (Flag23prime-))
(not (Flag22prime-))
(not (Flag21prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag522prime-))
(not (Flag520prime-))
(not (Flag518prime-))
(not (Flag517prime-))
(not (Flag515prime-))
(not (Flag514prime-))
(not (Flag513prime-))
(not (Flag511prime-))
(not (Flag510prime-))
(not (Flag509prime-))
(not (Flag508prime-))
(not (Flag506prime-))
(not (Flag505prime-))
(not (Flag504prime-))
(not (Flag503prime-))
(not (Flag502prime-))
(not (Flag500prime-))
(not (Flag499prime-))
(not (Flag498prime-))
(not (Flag497prime-))
(not (Flag496prime-))
(not (Flag495prime-))
(not (Flag494prime-))
(not (Flag492prime-))
(not (Flag491prime-))
(not (Flag490prime-))
(not (Flag489prime-))
(not (Flag488prime-))
(not (Flag487prime-))
(not (Flag486prime-))
(not (Flag485prime-))
(not (Flag483prime-))
(not (Flag482prime-))
(not (Flag481prime-))
(not (Flag480prime-))
(not (Flag479prime-))
(not (Flag478prime-))
(not (Flag477prime-))
(not (Flag476prime-))
(not (Flag474prime-))
(not (Flag473prime-))
(not (Flag472prime-))
(not (Flag471prime-))
(not (Flag470prime-))
(not (Flag469prime-))
(not (Flag468prime-))
(not (Flag467prime-))
(not (Flag466prime-))
(not (Flag464prime-))
(not (Flag463prime-))
(not (Flag462prime-))
(not (Flag461prime-))
(not (Flag460prime-))
(not (Flag459prime-))
(not (Flag458prime-))
(not (Flag457prime-))
(not (Flag456prime-))
(not (Flag455prime-))
(not (Flag454prime-))
(not (Flag452prime-))
(not (Flag451prime-))
(not (Flag450prime-))
(not (Flag449prime-))
(not (Flag448prime-))
(not (Flag447prime-))
(not (Flag446prime-))
(not (Flag445prime-))
(not (Flag444prime-))
(not (Flag443prime-))
(not (Flag442prime-))
(not (Flag441prime-))
(not (Flag440prime-))
(not (Flag438prime-))
(not (Flag437prime-))
(not (Flag436prime-))
(not (Flag435prime-))
(not (Flag434prime-))
(not (Flag433prime-))
(not (Flag432prime-))
(not (Flag431prime-))
(not (Flag430prime-))
(not (Flag429prime-))
(not (Flag428prime-))
(not (Flag427prime-))
(not (Flag426prime-))
(not (Flag424prime-))
(not (Flag423prime-))
(not (Flag422prime-))
(not (Flag421prime-))
(not (Flag420prime-))
(not (Flag419prime-))
(not (Flag418prime-))
(not (Flag417prime-))
(not (Flag416prime-))
(not (Flag415prime-))
(not (Flag414prime-))
(not (Flag413prime-))
(not (Flag412prime-))
(not (Flag411prime-))
(not (Flag410prime-))
(not (Flag409prime-))
(not (Flag407prime-))
(not (Flag406prime-))
(not (Flag405prime-))
(not (Flag404prime-))
(not (Flag403prime-))
(not (Flag402prime-))
(not (Flag401prime-))
(not (Flag400prime-))
(not (Flag399prime-))
(not (Flag398prime-))
(not (Flag397prime-))
(not (Flag396prime-))
(not (Flag395prime-))
(not (Flag394prime-))
(not (Flag393prime-))
(not (Flag392prime-))
(not (Flag390prime-))
(not (Flag389prime-))
(not (Flag388prime-))
(not (Flag387prime-))
(not (Flag386prime-))
(not (Flag385prime-))
(not (Flag384prime-))
(not (Flag383prime-))
(not (Flag382prime-))
(not (Flag381prime-))
(not (Flag380prime-))
(not (Flag379prime-))
(not (Flag378prime-))
(not (Flag377prime-))
(not (Flag376prime-))
(not (Flag375prime-))
(not (Flag373prime-))
(not (Flag372prime-))
(not (Flag371prime-))
(not (Flag370prime-))
(not (Flag369prime-))
(not (Flag368prime-))
(not (Flag367prime-))
(not (Flag366prime-))
(not (Flag365prime-))
(not (Flag364prime-))
(not (Flag363prime-))
(not (Flag362prime-))
(not (Flag361prime-))
(not (Flag360prime-))
(not (Flag359prime-))
(not (Flag358prime-))
(not (Flag357prime-))
(not (Flag356prime-))
(not (Flag354prime-))
(not (Flag353prime-))
(not (Flag352prime-))
(not (Flag351prime-))
(not (Flag350prime-))
(not (Flag349prime-))
(not (Flag348prime-))
(not (Flag347prime-))
(not (Flag346prime-))
(not (Flag345prime-))
(not (Flag344prime-))
(not (Flag343prime-))
(not (Flag342prime-))
(not (Flag341prime-))
(not (Flag340prime-))
(not (Flag339prime-))
(not (Flag338prime-))
(not (Flag337prime-))
(not (Flag335prime-))
(not (Flag334prime-))
(not (Flag333prime-))
(not (Flag332prime-))
(not (Flag331prime-))
(not (Flag330prime-))
(not (Flag329prime-))
(not (Flag328prime-))
(not (Flag327prime-))
(not (Flag326prime-))
(not (Flag325prime-))
(not (Flag324prime-))
(not (Flag323prime-))
(not (Flag322prime-))
(not (Flag321prime-))
(not (Flag320prime-))
(not (Flag319prime-))
(not (Flag318prime-))
(not (Flag317prime-))
(not (Flag316prime-))
(not (Flag315prime-))
(not (Flag314prime-))
(not (Flag313prime-))
(not (Flag312prime-))
(not (Flag311prime-))
(not (Flag310prime-))
(not (Flag309prime-))
(not (Flag308prime-))
(not (Flag307prime-))
(not (Flag306prime-))
(not (Flag305prime-))
(not (Flag304prime-))
(not (Flag303prime-))
(not (Flag302prime-))
(not (Flag301prime-))
(not (Flag300prime-))
(not (Flag299prime-))
(not (Flag298prime-))
(not (Flag297prime-))
(not (Flag296prime-))
(not (Flag295prime-))
(not (Flag294prime-))
(not (Flag293prime-))
(not (Flag292prime-))
(not (Flag291prime-))
(not (Flag290prime-))
(not (Flag289prime-))
(not (Flag288prime-))
(not (Flag287prime-))
(not (Flag286prime-))
(not (Flag285prime-))
(not (Flag284prime-))
(not (Flag283prime-))
(not (Flag282prime-))
(not (Flag279prime-))
(not (Flag278prime-))
(not (Flag277prime-))
(not (Flag276prime-))
(not (Flag275prime-))
(not (Flag274prime-))
(not (Flag273prime-))
(not (Flag272prime-))
(not (Flag271prime-))
(not (Flag270prime-))
(not (Flag269prime-))
(not (Flag268prime-))
(not (Flag267prime-))
(not (Flag266prime-))
(not (Flag265prime-))
(not (Flag264prime-))
(not (Flag4prime-))
(not (Flag521prime-))
(not (Flag519prime-))
(not (Flag516prime-))
(not (Flag512prime-))
(not (Flag507prime-))
(not (Flag501prime-))
(not (Flag493prime-))
(not (Flag484prime-))
(not (Flag475prime-))
(not (Flag465prime-))
(not (Flag453prime-))
(not (Flag439prime-))
(not (Flag425prime-))
(not (Flag408prime-))
(not (Flag391prime-))
(not (Flag374prime-))
(not (Flag355prime-))
(not (Flag336prime-))
(not (Flag262prime-))
(not (Flag259prime-))
(not (Flag257prime-))
(not (Flag256prime-))
(not (Flag254prime-))
(not (Flag253prime-))
(not (Flag252prime-))
(not (Flag250prime-))
(not (Flag249prime-))
(not (Flag248prime-))
(not (Flag247prime-))
(not (Flag246prime-))
(not (Flag244prime-))
(not (Flag243prime-))
(not (Flag242prime-))
(not (Flag241prime-))
(not (Flag240prime-))
(not (Flag239prime-))
(not (Flag237prime-))
(not (Flag236prime-))
(not (Flag235prime-))
(not (Flag234prime-))
(not (Flag233prime-))
(not (Flag232prime-))
(not (Flag230prime-))
(not (Flag229prime-))
(not (Flag228prime-))
(not (Flag227prime-))
(not (Flag226prime-))
(not (Flag225prime-))
(not (Flag224prime-))
(not (Flag223prime-))
(not (Flag221prime-))
(not (Flag220prime-))
(not (Flag219prime-))
(not (Flag218prime-))
(not (Flag217prime-))
(not (Flag216prime-))
(not (Flag215prime-))
(not (Flag214prime-))
(not (Flag212prime-))
(not (Flag211prime-))
(not (Flag210prime-))
(not (Flag209prime-))
(not (Flag208prime-))
(not (Flag207prime-))
(not (Flag206prime-))
(not (Flag205prime-))
(not (Flag204prime-))
(not (Flag203prime-))
(not (Flag201prime-))
(not (Flag200prime-))
(not (Flag199prime-))
(not (Flag198prime-))
(not (Flag197prime-))
(not (Flag196prime-))
(not (Flag195prime-))
(not (Flag194prime-))
(not (Flag193prime-))
(not (Flag192prime-))
(not (Flag190prime-))
(not (Flag189prime-))
(not (Flag188prime-))
(not (Flag187prime-))
(not (Flag186prime-))
(not (Flag185prime-))
(not (Flag184prime-))
(not (Flag183prime-))
(not (Flag182prime-))
(not (Flag181prime-))
(not (Flag180prime-))
(not (Flag179prime-))
(not (Flag177prime-))
(not (Flag176prime-))
(not (Flag175prime-))
(not (Flag174prime-))
(not (Flag173prime-))
(not (Flag172prime-))
(not (Flag171prime-))
(not (Flag170prime-))
(not (Flag169prime-))
(not (Flag168prime-))
(not (Flag167prime-))
(not (Flag166prime-))
(not (Flag165prime-))
(not (Flag163prime-))
(not (Flag162prime-))
(not (Flag161prime-))
(not (Flag160prime-))
(not (Flag159prime-))
(not (Flag158prime-))
(not (Flag157prime-))
(not (Flag156prime-))
(not (Flag155prime-))
(not (Flag154prime-))
(not (Flag153prime-))
(not (Flag152prime-))
(not (Flag151prime-))
(not (Flag150prime-))
(not (Flag149prime-))
(not (Flag147prime-))
(not (Flag146prime-))
(not (Flag145prime-))
(not (Flag144prime-))
(not (Flag143prime-))
(not (Flag142prime-))
(not (Flag141prime-))
(not (Flag140prime-))
(not (Flag139prime-))
(not (Flag138prime-))
(not (Flag137prime-))
(not (Flag136prime-))
(not (Flag135prime-))
(not (Flag134prime-))
(not (Flag132prime-))
(not (Flag131prime-))
(not (Flag130prime-))
(not (Flag129prime-))
(not (Flag128prime-))
(not (Flag127prime-))
(not (Flag126prime-))
(not (Flag125prime-))
(not (Flag124prime-))
(not (Flag123prime-))
(not (Flag122prime-))
(not (Flag121prime-))
(not (Flag120prime-))
(not (Flag119prime-))
(not (Flag118prime-))
(not (Flag117prime-))
(not (Flag116prime-))
(not (Flag115prime-))
(not (Flag113prime-))
(not (Flag112prime-))
(not (Flag111prime-))
(not (Flag110prime-))
(not (Flag109prime-))
(not (Flag108prime-))
(not (Flag107prime-))
(not (Flag106prime-))
(not (Flag105prime-))
(not (Flag104prime-))
(not (Flag103prime-))
(not (Flag102prime-))
(not (Flag101prime-))
(not (Flag100prime-))
(not (Flag99prime-))
(not (Flag98prime-))
(not (Flag97prime-))
(not (Flag96prime-))
(not (Flag94prime-))
(not (Flag93prime-))
(not (Flag92prime-))
(not (Flag91prime-))
(not (Flag90prime-))
(not (Flag89prime-))
(not (Flag88prime-))
(not (Flag87prime-))
(not (Flag86prime-))
(not (Flag85prime-))
(not (Flag84prime-))
(not (Flag83prime-))
(not (Flag82prime-))
(not (Flag81prime-))
(not (Flag80prime-))
(not (Flag79prime-))
(not (Flag78prime-))
(not (Flag76prime-))
(not (Flag75prime-))
(not (Flag74prime-))
(not (Flag73prime-))
(not (Flag72prime-))
(not (Flag71prime-))
(not (Flag70prime-))
(not (Flag69prime-))
(not (Flag68prime-))
(not (Flag67prime-))
(not (Flag66prime-))
(not (Flag65prime-))
(not (Flag64prime-))
(not (Flag63prime-))
(not (Flag62prime-))
(not (Flag61prime-))
(not (Flag60prime-))
(not (Flag59prime-))
(not (Flag58prime-))
(not (Flag57prime-))
(not (Flag56prime-))
(not (Flag55prime-))
(not (Flag54prime-))
(not (Flag53prime-))
(not (Flag52prime-))
(not (Flag51prime-))
(not (Flag50prime-))
(not (Flag49prime-))
(not (Flag48prime-))
(not (Flag47prime-))
(not (Flag46prime-))
(not (Flag45prime-))
(not (Flag44prime-))
(not (Flag43prime-))
(not (Flag42prime-))
(not (Flag41prime-))
(not (Flag40prime-))
(not (Flag39prime-))
(not (Flag38prime-))
(not (Flag37prime-))
(not (Flag36prime-))
(not (Flag35prime-))
(not (Flag34prime-))
(not (Flag33prime-))
(not (Flag32prime-))
(not (Flag31prime-))
(not (Flag30prime-))
(not (Flag29prime-))
(not (Flag28prime-))
(not (Flag27prime-))
(not (Flag26prime-))
(not (Flag25prime-))
(not (Flag20prime-))
(not (Flag19prime-))
(not (Flag18prime-))
(not (Flag17prime-))
(not (Flag16prime-))
(not (Flag15prime-))
(not (Flag14prime-))
(not (Flag13prime-))
(not (Flag12prime-))
(not (Flag11prime-))
(not (Flag10prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag1prime-))
(not (Flag263prime-))
(not (Flag260prime-))
(not (Flag258prime-))
(not (Flag255prime-))
(not (Flag251prime-))
(not (Flag245prime-))
(not (Flag238prime-))
(not (Flag231prime-))
(not (Flag222prime-))
(not (Flag213prime-))
(not (Flag202prime-))
(not (Flag191prime-))
(not (Flag178prime-))
(not (Flag164prime-))
(not (Flag148prime-))
(not (Flag133prime-))
(not (Flag114prime-))
(not (Flag95prime-))
(not (Flag77prime-))
)
)
(:action MOVEDOWN-ROBOT
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(Flag521prime-)
(Flag519prime-)
(Flag516prime-)
(Flag512prime-)
(Flag507prime-)
(Flag501prime-)
(Flag493prime-)
(Flag484prime-)
(Flag475prime-)
(Flag465prime-)
(Flag453prime-)
(Flag439prime-)
(Flag425prime-)
(Flag408prime-)
(Flag391prime-)
(Flag374prime-)
(Flag355prime-)
(Flag336prime-)
(Flag298prime-)
(Flag297prime-)
(Flag296prime-)
(Flag295prime-)
(Flag294prime-)
(Flag293prime-)
(Flag292prime-)
(Flag291prime-)
(Flag290prime-)
(Flag289prime-)
(Flag288prime-)
(Flag287prime-)
(Flag286prime-)
(Flag285prime-)
(Flag284prime-)
(Flag283prime-)
(Flag282prime-)
(Flag281prime-)
(Flag280prime-)
(Flag279prime-)
(Flag278prime-)
(Flag277prime-)
(Flag276prime-)
(Flag275prime-)
(Flag274prime-)
(Flag273prime-)
(Flag272prime-)
(Flag271prime-)
(Flag270prime-)
(Flag269prime-)
(Flag268prime-)
(Flag267prime-)
(Flag266prime-)
(Flag265prime-)
(Flag264prime-)
)
:effect
(and
(when
(and
(Flag521-)
)
(and
(ROW17-ROBOT)
(not (NOT-ROW17-ROBOT))
)
)
(when
(and
(Flag519-)
)
(and
(ROW16-ROBOT)
(not (NOT-ROW16-ROBOT))
)
)
(when
(and
(Flag516-)
)
(and
(ROW15-ROBOT)
(not (NOT-ROW15-ROBOT))
)
)
(when
(and
(Flag512-)
)
(and
(ROW14-ROBOT)
(not (NOT-ROW14-ROBOT))
)
)
(when
(and
(Flag507-)
)
(and
(ROW13-ROBOT)
(not (NOT-ROW13-ROBOT))
)
)
(when
(and
(Flag501-)
)
(and
(ROW12-ROBOT)
(not (NOT-ROW12-ROBOT))
)
)
(when
(and
(Flag493-)
)
(and
(ROW11-ROBOT)
(not (NOT-ROW11-ROBOT))
)
)
(when
(and
(Flag484-)
)
(and
(ROW10-ROBOT)
(not (NOT-ROW10-ROBOT))
)
)
(when
(and
(Flag475-)
)
(and
(ROW9-ROBOT)
(not (NOT-ROW9-ROBOT))
)
)
(when
(and
(Flag465-)
)
(and
(ROW8-ROBOT)
(not (NOT-ROW8-ROBOT))
)
)
(when
(and
(Flag453-)
)
(and
(ROW7-ROBOT)
(not (NOT-ROW7-ROBOT))
)
)
(when
(and
(Flag439-)
)
(and
(ROW6-ROBOT)
(not (NOT-ROW6-ROBOT))
)
)
(when
(and
(Flag425-)
)
(and
(ROW5-ROBOT)
(not (NOT-ROW5-ROBOT))
)
)
(when
(and
(Flag408-)
)
(and
(ROW4-ROBOT)
(not (NOT-ROW4-ROBOT))
)
)
(when
(and
(Flag391-)
)
(and
(ROW3-ROBOT)
(not (NOT-ROW3-ROBOT))
)
)
(when
(and
(Flag374-)
)
(and
(ROW2-ROBOT)
(not (NOT-ROW2-ROBOT))
)
)
(when
(and
(Flag355-)
)
(and
(ROW1-ROBOT)
(not (NOT-ROW1-ROBOT))
)
)
(when
(and
(Flag336-)
)
(and
(ROW0-ROBOT)
(not (NOT-ROW0-ROBOT))
)
)
(when
(and
(ROW18-ROBOT)
)
(and
(ROW17-ROBOT)
(NOT-ROW18-ROBOT)
(not (NOT-ROW17-ROBOT))
(not (ROW18-ROBOT))
)
)
(when
(and
(ROW17-ROBOT)
)
(and
(ROW16-ROBOT)
(NOT-ROW17-ROBOT)
(not (NOT-ROW16-ROBOT))
(not (ROW17-ROBOT))
)
)
(when
(and
(ROW16-ROBOT)
)
(and
(ROW15-ROBOT)
(NOT-ROW16-ROBOT)
(not (NOT-ROW15-ROBOT))
(not (ROW16-ROBOT))
)
)
(when
(and
(ROW15-ROBOT)
)
(and
(ROW14-ROBOT)
(NOT-ROW15-ROBOT)
(not (NOT-ROW14-ROBOT))
(not (ROW15-ROBOT))
)
)
(when
(and
(ROW14-ROBOT)
)
(and
(ROW13-ROBOT)
(NOT-ROW14-ROBOT)
(not (NOT-ROW13-ROBOT))
(not (ROW14-ROBOT))
)
)
(when
(and
(ROW13-ROBOT)
)
(and
(ROW12-ROBOT)
(NOT-ROW13-ROBOT)
(not (NOT-ROW12-ROBOT))
(not (ROW13-ROBOT))
)
)
(when
(and
(ROW12-ROBOT)
)
(and
(ROW11-ROBOT)
(NOT-ROW12-ROBOT)
(not (NOT-ROW11-ROBOT))
(not (ROW12-ROBOT))
)
)
(when
(and
(ROW11-ROBOT)
)
(and
(ROW10-ROBOT)
(NOT-ROW11-ROBOT)
(not (NOT-ROW10-ROBOT))
(not (ROW11-ROBOT))
)
)
(when
(and
(ROW10-ROBOT)
)
(and
(ROW9-ROBOT)
(NOT-ROW10-ROBOT)
(not (NOT-ROW9-ROBOT))
(not (ROW10-ROBOT))
)
)
(when
(and
(ROW9-ROBOT)
)
(and
(ROW8-ROBOT)
(NOT-ROW9-ROBOT)
(not (NOT-ROW8-ROBOT))
(not (ROW9-ROBOT))
)
)
(when
(and
(ROW8-ROBOT)
)
(and
(ROW7-ROBOT)
(NOT-ROW8-ROBOT)
(not (NOT-ROW7-ROBOT))
(not (ROW8-ROBOT))
)
)
(when
(and
(ROW7-ROBOT)
)
(and
(ROW6-ROBOT)
(NOT-ROW7-ROBOT)
(not (NOT-ROW6-ROBOT))
(not (ROW7-ROBOT))
)
)
(when
(and
(ROW6-ROBOT)
)
(and
(ROW5-ROBOT)
(NOT-ROW6-ROBOT)
(not (NOT-ROW5-ROBOT))
(not (ROW6-ROBOT))
)
)
(when
(and
(ROW5-ROBOT)
)
(and
(ROW4-ROBOT)
(NOT-ROW5-ROBOT)
(not (NOT-ROW4-ROBOT))
(not (ROW5-ROBOT))
)
)
(when
(and
(ROW4-ROBOT)
)
(and
(ROW3-ROBOT)
(NOT-ROW4-ROBOT)
(not (NOT-ROW3-ROBOT))
(not (ROW4-ROBOT))
)
)
(when
(and
(ROW3-ROBOT)
)
(and
(ROW2-ROBOT)
(NOT-ROW3-ROBOT)
(not (NOT-ROW2-ROBOT))
(not (ROW3-ROBOT))
)
)
(when
(and
(ROW2-ROBOT)
)
(and
(ROW1-ROBOT)
(NOT-ROW2-ROBOT)
(not (NOT-ROW1-ROBOT))
(not (ROW2-ROBOT))
)
)
(when
(and
(ROW1-ROBOT)
)
(and
(ROW0-ROBOT)
(NOT-ROW1-ROBOT)
(not (NOT-ROW0-ROBOT))
(not (ROW1-ROBOT))
)
)
(when
(and
(ABOVEOF18-ROBOT)
)
(and
(ABOVEOF17-ROBOT)
(NOT-ABOVEOF18-ROBOT)
(not (NOT-ABOVEOF17-ROBOT))
(not (ABOVEOF18-ROBOT))
)
)
(when
(and
(Flag298-)
)
(and
(ABOVEOF16-ROBOT)
(NOT-ABOVEOF17-ROBOT)
(not (NOT-ABOVEOF16-ROBOT))
(not (ABOVEOF17-ROBOT))
)
)
(when
(and
(Flag297-)
)
(and
(ABOVEOF15-ROBOT)
(NOT-ABOVEOF16-ROBOT)
(not (NOT-ABOVEOF15-ROBOT))
(not (ABOVEOF16-ROBOT))
)
)
(when
(and
(Flag296-)
)
(and
(ABOVEOF14-ROBOT)
(NOT-ABOVEOF15-ROBOT)
(not (NOT-ABOVEOF14-ROBOT))
(not (ABOVEOF15-ROBOT))
)
)
(when
(and
(Flag295-)
)
(and
(ABOVEOF13-ROBOT)
(NOT-ABOVEOF14-ROBOT)
(not (NOT-ABOVEOF13-ROBOT))
(not (ABOVEOF14-ROBOT))
)
)
(when
(and
(Flag294-)
)
(and
(ABOVEOF12-ROBOT)
(NOT-ABOVEOF13-ROBOT)
(not (NOT-ABOVEOF12-ROBOT))
(not (ABOVEOF13-ROBOT))
)
)
(when
(and
(Flag293-)
)
(and
(ABOVEOF11-ROBOT)
(NOT-ABOVEOF12-ROBOT)
(not (NOT-ABOVEOF11-ROBOT))
(not (ABOVEOF12-ROBOT))
)
)
(when
(and
(Flag292-)
)
(and
(ABOVEOF10-ROBOT)
(NOT-ABOVEOF11-ROBOT)
(not (NOT-ABOVEOF10-ROBOT))
(not (ABOVEOF11-ROBOT))
)
)
(when
(and
(Flag291-)
)
(and
(ABOVEOF9-ROBOT)
(NOT-ABOVEOF10-ROBOT)
(not (NOT-ABOVEOF9-ROBOT))
(not (ABOVEOF10-ROBOT))
)
)
(when
(and
(Flag290-)
)
(and
(ABOVEOF8-ROBOT)
(NOT-ABOVEOF9-ROBOT)
(not (NOT-ABOVEOF8-ROBOT))
(not (ABOVEOF9-ROBOT))
)
)
(when
(and
(Flag289-)
)
(and
(ABOVEOF7-ROBOT)
(NOT-ABOVEOF8-ROBOT)
(not (NOT-ABOVEOF7-ROBOT))
(not (ABOVEOF8-ROBOT))
)
)
(when
(and
(Flag288-)
)
(and
(ABOVEOF6-ROBOT)
(NOT-ABOVEOF7-ROBOT)
(not (NOT-ABOVEOF6-ROBOT))
(not (ABOVEOF7-ROBOT))
)
)
(when
(and
(Flag287-)
)
(and
(ABOVEOF5-ROBOT)
(NOT-ABOVEOF6-ROBOT)
(not (NOT-ABOVEOF5-ROBOT))
(not (ABOVEOF6-ROBOT))
)
)
(when
(and
(Flag286-)
)
(and
(ABOVEOF4-ROBOT)
(NOT-ABOVEOF5-ROBOT)
(not (NOT-ABOVEOF4-ROBOT))
(not (ABOVEOF5-ROBOT))
)
)
(when
(and
(Flag285-)
)
(and
(ABOVEOF3-ROBOT)
(NOT-ABOVEOF4-ROBOT)
(not (NOT-ABOVEOF3-ROBOT))
(not (ABOVEOF4-ROBOT))
)
)
(when
(and
(Flag284-)
)
(and
(ABOVEOF2-ROBOT)
(NOT-ABOVEOF3-ROBOT)
(not (NOT-ABOVEOF2-ROBOT))
(not (ABOVEOF3-ROBOT))
)
)
(when
(and
(Flag283-)
)
(and
(ABOVEOF1-ROBOT)
(NOT-ABOVEOF2-ROBOT)
(not (NOT-ABOVEOF1-ROBOT))
(not (ABOVEOF2-ROBOT))
)
)
(when
(and
(Flag282-)
)
(and
(NOT-ABOVEOF1-ROBOT)
(not (ABOVEOF1-ROBOT))
)
)
(when
(and
(Flag281-)
)
(and
(BELOWOF18-ROBOT)
(not (NOT-BELOWOF18-ROBOT))
)
)
(when
(and
(Flag280-)
)
(and
(BELOWOF17-ROBOT)
(not (NOT-BELOWOF17-ROBOT))
)
)
(when
(and
(Flag279-)
)
(and
(BELOWOF16-ROBOT)
(not (NOT-BELOWOF16-ROBOT))
)
)
(when
(and
(Flag278-)
)
(and
(BELOWOF15-ROBOT)
(not (NOT-BELOWOF15-ROBOT))
)
)
(when
(and
(Flag277-)
)
(and
(BELOWOF14-ROBOT)
(not (NOT-BELOWOF14-ROBOT))
)
)
(when
(and
(Flag276-)
)
(and
(BELOWOF13-ROBOT)
(not (NOT-BELOWOF13-ROBOT))
)
)
(when
(and
(Flag275-)
)
(and
(BELOWOF12-ROBOT)
(not (NOT-BELOWOF12-ROBOT))
)
)
(when
(and
(Flag274-)
)
(and
(BELOWOF11-ROBOT)
(not (NOT-BELOWOF11-ROBOT))
)
)
(when
(and
(Flag273-)
)
(and
(BELOWOF10-ROBOT)
(not (NOT-BELOWOF10-ROBOT))
)
)
(when
(and
(Flag272-)
)
(and
(BELOWOF9-ROBOT)
(not (NOT-BELOWOF9-ROBOT))
)
)
(when
(and
(Flag271-)
)
(and
(BELOWOF8-ROBOT)
(not (NOT-BELOWOF8-ROBOT))
)
)
(when
(and
(Flag270-)
)
(and
(BELOWOF7-ROBOT)
(not (NOT-BELOWOF7-ROBOT))
)
)
(when
(and
(Flag269-)
)
(and
(BELOWOF6-ROBOT)
(not (NOT-BELOWOF6-ROBOT))
)
)
(when
(and
(Flag268-)
)
(and
(BELOWOF5-ROBOT)
(not (NOT-BELOWOF5-ROBOT))
)
)
(when
(and
(Flag267-)
)
(and
(BELOWOF4-ROBOT)
(not (NOT-BELOWOF4-ROBOT))
)
)
(when
(and
(Flag266-)
)
(and
(BELOWOF3-ROBOT)
(not (NOT-BELOWOF3-ROBOT))
)
)
(when
(and
(Flag265-)
)
(and
(BELOWOF2-ROBOT)
(not (NOT-BELOWOF2-ROBOT))
)
)
(when
(and
(Flag264-)
)
(and
(BELOWOF1-ROBOT)
(not (NOT-BELOWOF1-ROBOT))
)
)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag281prime-))
(not (Flag280prime-))
(not (Flag261prime-))
(not (Flag24prime-))
(not (Flag23prime-))
(not (Flag22prime-))
(not (Flag21prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag522prime-))
(not (Flag520prime-))
(not (Flag518prime-))
(not (Flag517prime-))
(not (Flag515prime-))
(not (Flag514prime-))
(not (Flag513prime-))
(not (Flag511prime-))
(not (Flag510prime-))
(not (Flag509prime-))
(not (Flag508prime-))
(not (Flag506prime-))
(not (Flag505prime-))
(not (Flag504prime-))
(not (Flag503prime-))
(not (Flag502prime-))
(not (Flag500prime-))
(not (Flag499prime-))
(not (Flag498prime-))
(not (Flag497prime-))
(not (Flag496prime-))
(not (Flag495prime-))
(not (Flag494prime-))
(not (Flag492prime-))
(not (Flag491prime-))
(not (Flag490prime-))
(not (Flag489prime-))
(not (Flag488prime-))
(not (Flag487prime-))
(not (Flag486prime-))
(not (Flag485prime-))
(not (Flag483prime-))
(not (Flag482prime-))
(not (Flag481prime-))
(not (Flag480prime-))
(not (Flag479prime-))
(not (Flag478prime-))
(not (Flag477prime-))
(not (Flag476prime-))
(not (Flag474prime-))
(not (Flag473prime-))
(not (Flag472prime-))
(not (Flag471prime-))
(not (Flag470prime-))
(not (Flag469prime-))
(not (Flag468prime-))
(not (Flag467prime-))
(not (Flag466prime-))
(not (Flag464prime-))
(not (Flag463prime-))
(not (Flag462prime-))
(not (Flag461prime-))
(not (Flag460prime-))
(not (Flag459prime-))
(not (Flag458prime-))
(not (Flag457prime-))
(not (Flag456prime-))
(not (Flag455prime-))
(not (Flag454prime-))
(not (Flag452prime-))
(not (Flag451prime-))
(not (Flag450prime-))
(not (Flag449prime-))
(not (Flag448prime-))
(not (Flag447prime-))
(not (Flag446prime-))
(not (Flag445prime-))
(not (Flag444prime-))
(not (Flag443prime-))
(not (Flag442prime-))
(not (Flag441prime-))
(not (Flag440prime-))
(not (Flag438prime-))
(not (Flag437prime-))
(not (Flag436prime-))
(not (Flag435prime-))
(not (Flag434prime-))
(not (Flag433prime-))
(not (Flag432prime-))
(not (Flag431prime-))
(not (Flag430prime-))
(not (Flag429prime-))
(not (Flag428prime-))
(not (Flag427prime-))
(not (Flag426prime-))
(not (Flag424prime-))
(not (Flag423prime-))
(not (Flag422prime-))
(not (Flag421prime-))
(not (Flag420prime-))
(not (Flag419prime-))
(not (Flag418prime-))
(not (Flag417prime-))
(not (Flag416prime-))
(not (Flag415prime-))
(not (Flag414prime-))
(not (Flag413prime-))
(not (Flag412prime-))
(not (Flag411prime-))
(not (Flag410prime-))
(not (Flag409prime-))
(not (Flag407prime-))
(not (Flag406prime-))
(not (Flag405prime-))
(not (Flag404prime-))
(not (Flag403prime-))
(not (Flag402prime-))
(not (Flag401prime-))
(not (Flag400prime-))
(not (Flag399prime-))
(not (Flag398prime-))
(not (Flag397prime-))
(not (Flag396prime-))
(not (Flag395prime-))
(not (Flag394prime-))
(not (Flag393prime-))
(not (Flag392prime-))
(not (Flag390prime-))
(not (Flag389prime-))
(not (Flag388prime-))
(not (Flag387prime-))
(not (Flag386prime-))
(not (Flag385prime-))
(not (Flag384prime-))
(not (Flag383prime-))
(not (Flag382prime-))
(not (Flag381prime-))
(not (Flag380prime-))
(not (Flag379prime-))
(not (Flag378prime-))
(not (Flag377prime-))
(not (Flag376prime-))
(not (Flag375prime-))
(not (Flag373prime-))
(not (Flag372prime-))
(not (Flag371prime-))
(not (Flag370prime-))
(not (Flag369prime-))
(not (Flag368prime-))
(not (Flag367prime-))
(not (Flag366prime-))
(not (Flag365prime-))
(not (Flag364prime-))
(not (Flag363prime-))
(not (Flag362prime-))
(not (Flag361prime-))
(not (Flag360prime-))
(not (Flag359prime-))
(not (Flag358prime-))
(not (Flag357prime-))
(not (Flag356prime-))
(not (Flag354prime-))
(not (Flag353prime-))
(not (Flag352prime-))
(not (Flag351prime-))
(not (Flag350prime-))
(not (Flag349prime-))
(not (Flag348prime-))
(not (Flag347prime-))
(not (Flag346prime-))
(not (Flag345prime-))
(not (Flag344prime-))
(not (Flag343prime-))
(not (Flag342prime-))
(not (Flag341prime-))
(not (Flag340prime-))
(not (Flag339prime-))
(not (Flag338prime-))
(not (Flag337prime-))
(not (Flag335prime-))
(not (Flag334prime-))
(not (Flag333prime-))
(not (Flag332prime-))
(not (Flag331prime-))
(not (Flag330prime-))
(not (Flag329prime-))
(not (Flag328prime-))
(not (Flag327prime-))
(not (Flag326prime-))
(not (Flag325prime-))
(not (Flag324prime-))
(not (Flag323prime-))
(not (Flag322prime-))
(not (Flag321prime-))
(not (Flag320prime-))
(not (Flag319prime-))
(not (Flag318prime-))
(not (Flag317prime-))
(not (Flag316prime-))
(not (Flag315prime-))
(not (Flag314prime-))
(not (Flag313prime-))
(not (Flag312prime-))
(not (Flag311prime-))
(not (Flag310prime-))
(not (Flag309prime-))
(not (Flag308prime-))
(not (Flag307prime-))
(not (Flag306prime-))
(not (Flag305prime-))
(not (Flag304prime-))
(not (Flag303prime-))
(not (Flag302prime-))
(not (Flag301prime-))
(not (Flag300prime-))
(not (Flag299prime-))
(not (Flag298prime-))
(not (Flag297prime-))
(not (Flag296prime-))
(not (Flag295prime-))
(not (Flag294prime-))
(not (Flag293prime-))
(not (Flag292prime-))
(not (Flag291prime-))
(not (Flag290prime-))
(not (Flag289prime-))
(not (Flag288prime-))
(not (Flag287prime-))
(not (Flag286prime-))
(not (Flag285prime-))
(not (Flag284prime-))
(not (Flag283prime-))
(not (Flag282prime-))
(not (Flag279prime-))
(not (Flag278prime-))
(not (Flag277prime-))
(not (Flag276prime-))
(not (Flag275prime-))
(not (Flag274prime-))
(not (Flag273prime-))
(not (Flag272prime-))
(not (Flag271prime-))
(not (Flag270prime-))
(not (Flag269prime-))
(not (Flag268prime-))
(not (Flag267prime-))
(not (Flag266prime-))
(not (Flag265prime-))
(not (Flag264prime-))
(not (Flag4prime-))
(not (Flag521prime-))
(not (Flag519prime-))
(not (Flag516prime-))
(not (Flag512prime-))
(not (Flag507prime-))
(not (Flag501prime-))
(not (Flag493prime-))
(not (Flag484prime-))
(not (Flag475prime-))
(not (Flag465prime-))
(not (Flag453prime-))
(not (Flag439prime-))
(not (Flag425prime-))
(not (Flag408prime-))
(not (Flag391prime-))
(not (Flag374prime-))
(not (Flag355prime-))
(not (Flag336prime-))
(not (Flag262prime-))
(not (Flag259prime-))
(not (Flag257prime-))
(not (Flag256prime-))
(not (Flag254prime-))
(not (Flag253prime-))
(not (Flag252prime-))
(not (Flag250prime-))
(not (Flag249prime-))
(not (Flag248prime-))
(not (Flag247prime-))
(not (Flag246prime-))
(not (Flag244prime-))
(not (Flag243prime-))
(not (Flag242prime-))
(not (Flag241prime-))
(not (Flag240prime-))
(not (Flag239prime-))
(not (Flag237prime-))
(not (Flag236prime-))
(not (Flag235prime-))
(not (Flag234prime-))
(not (Flag233prime-))
(not (Flag232prime-))
(not (Flag230prime-))
(not (Flag229prime-))
(not (Flag228prime-))
(not (Flag227prime-))
(not (Flag226prime-))
(not (Flag225prime-))
(not (Flag224prime-))
(not (Flag223prime-))
(not (Flag221prime-))
(not (Flag220prime-))
(not (Flag219prime-))
(not (Flag218prime-))
(not (Flag217prime-))
(not (Flag216prime-))
(not (Flag215prime-))
(not (Flag214prime-))
(not (Flag212prime-))
(not (Flag211prime-))
(not (Flag210prime-))
(not (Flag209prime-))
(not (Flag208prime-))
(not (Flag207prime-))
(not (Flag206prime-))
(not (Flag205prime-))
(not (Flag204prime-))
(not (Flag203prime-))
(not (Flag201prime-))
(not (Flag200prime-))
(not (Flag199prime-))
(not (Flag198prime-))
(not (Flag197prime-))
(not (Flag196prime-))
(not (Flag195prime-))
(not (Flag194prime-))
(not (Flag193prime-))
(not (Flag192prime-))
(not (Flag190prime-))
(not (Flag189prime-))
(not (Flag188prime-))
(not (Flag187prime-))
(not (Flag186prime-))
(not (Flag185prime-))
(not (Flag184prime-))
(not (Flag183prime-))
(not (Flag182prime-))
(not (Flag181prime-))
(not (Flag180prime-))
(not (Flag179prime-))
(not (Flag177prime-))
(not (Flag176prime-))
(not (Flag175prime-))
(not (Flag174prime-))
(not (Flag173prime-))
(not (Flag172prime-))
(not (Flag171prime-))
(not (Flag170prime-))
(not (Flag169prime-))
(not (Flag168prime-))
(not (Flag167prime-))
(not (Flag166prime-))
(not (Flag165prime-))
(not (Flag163prime-))
(not (Flag162prime-))
(not (Flag161prime-))
(not (Flag160prime-))
(not (Flag159prime-))
(not (Flag158prime-))
(not (Flag157prime-))
(not (Flag156prime-))
(not (Flag155prime-))
(not (Flag154prime-))
(not (Flag153prime-))
(not (Flag152prime-))
(not (Flag151prime-))
(not (Flag150prime-))
(not (Flag149prime-))
(not (Flag147prime-))
(not (Flag146prime-))
(not (Flag145prime-))
(not (Flag144prime-))
(not (Flag143prime-))
(not (Flag142prime-))
(not (Flag141prime-))
(not (Flag140prime-))
(not (Flag139prime-))
(not (Flag138prime-))
(not (Flag137prime-))
(not (Flag136prime-))
(not (Flag135prime-))
(not (Flag134prime-))
(not (Flag132prime-))
(not (Flag131prime-))
(not (Flag130prime-))
(not (Flag129prime-))
(not (Flag128prime-))
(not (Flag127prime-))
(not (Flag126prime-))
(not (Flag125prime-))
(not (Flag124prime-))
(not (Flag123prime-))
(not (Flag122prime-))
(not (Flag121prime-))
(not (Flag120prime-))
(not (Flag119prime-))
(not (Flag118prime-))
(not (Flag117prime-))
(not (Flag116prime-))
(not (Flag115prime-))
(not (Flag113prime-))
(not (Flag112prime-))
(not (Flag111prime-))
(not (Flag110prime-))
(not (Flag109prime-))
(not (Flag108prime-))
(not (Flag107prime-))
(not (Flag106prime-))
(not (Flag105prime-))
(not (Flag104prime-))
(not (Flag103prime-))
(not (Flag102prime-))
(not (Flag101prime-))
(not (Flag100prime-))
(not (Flag99prime-))
(not (Flag98prime-))
(not (Flag97prime-))
(not (Flag96prime-))
(not (Flag94prime-))
(not (Flag93prime-))
(not (Flag92prime-))
(not (Flag91prime-))
(not (Flag90prime-))
(not (Flag89prime-))
(not (Flag88prime-))
(not (Flag87prime-))
(not (Flag86prime-))
(not (Flag85prime-))
(not (Flag84prime-))
(not (Flag83prime-))
(not (Flag82prime-))
(not (Flag81prime-))
(not (Flag80prime-))
(not (Flag79prime-))
(not (Flag78prime-))
(not (Flag76prime-))
(not (Flag75prime-))
(not (Flag74prime-))
(not (Flag73prime-))
(not (Flag72prime-))
(not (Flag71prime-))
(not (Flag70prime-))
(not (Flag69prime-))
(not (Flag68prime-))
(not (Flag67prime-))
(not (Flag66prime-))
(not (Flag65prime-))
(not (Flag64prime-))
(not (Flag63prime-))
(not (Flag62prime-))
(not (Flag61prime-))
(not (Flag60prime-))
(not (Flag59prime-))
(not (Flag58prime-))
(not (Flag57prime-))
(not (Flag56prime-))
(not (Flag55prime-))
(not (Flag54prime-))
(not (Flag53prime-))
(not (Flag52prime-))
(not (Flag51prime-))
(not (Flag50prime-))
(not (Flag49prime-))
(not (Flag48prime-))
(not (Flag47prime-))
(not (Flag46prime-))
(not (Flag45prime-))
(not (Flag44prime-))
(not (Flag43prime-))
(not (Flag42prime-))
(not (Flag41prime-))
(not (Flag40prime-))
(not (Flag39prime-))
(not (Flag38prime-))
(not (Flag37prime-))
(not (Flag36prime-))
(not (Flag35prime-))
(not (Flag34prime-))
(not (Flag33prime-))
(not (Flag32prime-))
(not (Flag31prime-))
(not (Flag30prime-))
(not (Flag29prime-))
(not (Flag28prime-))
(not (Flag27prime-))
(not (Flag26prime-))
(not (Flag25prime-))
(not (Flag20prime-))
(not (Flag19prime-))
(not (Flag18prime-))
(not (Flag17prime-))
(not (Flag16prime-))
(not (Flag15prime-))
(not (Flag14prime-))
(not (Flag13prime-))
(not (Flag12prime-))
(not (Flag11prime-))
(not (Flag10prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag1prime-))
(not (Flag263prime-))
(not (Flag260prime-))
(not (Flag258prime-))
(not (Flag255prime-))
(not (Flag251prime-))
(not (Flag245prime-))
(not (Flag238prime-))
(not (Flag231prime-))
(not (Flag222prime-))
(not (Flag213prime-))
(not (Flag202prime-))
(not (Flag191prime-))
(not (Flag178prime-))
(not (Flag164prime-))
(not (Flag148prime-))
(not (Flag133prime-))
(not (Flag114prime-))
(not (Flag95prime-))
(not (Flag77prime-))
)
)
(:action Prim1Action-2
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
(Flag1prime-)
(not (Flag1-))
)

)
(:action Prim1Action-3
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
(Flag1prime-)
(not (Flag1-))
)

)
(:action Flag2Action
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
)
:effect
(and
(Flag2-)
(Flag2prime-)
)

)
(:action Prim2Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
(Flag2prime-)
(not (Flag2-))
)

)
(:action Prim2Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
(Flag2prime-)
(not (Flag2-))
)

)
(:action Flag3Action-6
:parameters ()
:precondition
(and
(RIGHTOF2-ROBOT)
)
:effect
(and
(Flag3-)
(Flag3prime-)
)

)
(:action Flag3Action-36
:parameters ()
:precondition
(and
(LEFTOF18-ROBOT)
)
:effect
(and
(Flag3-)
(Flag3prime-)
)

)
(:action Prim4Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
(Flag4prime-)
(not (Flag4-))
)

)
(:action Prim4Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
(Flag4prime-)
(not (Flag4-))
)

)
(:action Flag21Action-2
:parameters ()
:precondition
(and
(LEFTOF18-ROBOT)
)
:effect
(and
(Flag21-)
(Flag21prime-)
)

)
(:action Flag22Action-3
:parameters ()
:precondition
(and
(LEFTOF18-ROBOT)
)
:effect
(and
(Flag22-)
(Flag22prime-)
)

)
(:action Flag23Action-4
:parameters ()
:precondition
(and
(RIGHTOF2-ROBOT)
)
:effect
(and
(Flag23-)
(Flag23prime-)
)

)
(:action Flag24Action-3
:parameters ()
:precondition
(and
(RIGHTOF2-ROBOT)
)
:effect
(and
(Flag24-)
(Flag24prime-)
)

)
(:action Prim70Action-0
:parameters ()
:precondition
(and
(not (RIGHTOF2-ROBOT))
)
:effect
(and
(Flag70prime-)
(not (Flag70-))
)

)
(:action Prim71Action-1
:parameters ()
:precondition
(and
(not (RIGHTOF2-ROBOT))
)
:effect
(and
(Flag71prime-)
(not (Flag71-))
)

)
(:action Prim94Action-1
:parameters ()
:precondition
(and
(not (RIGHTOF2-ROBOT))
)
:effect
(and
(Flag94prime-)
(not (Flag94-))
)

)
(:action Prim256Action-1
:parameters ()
:precondition
(and
(not (LEFTOF18-ROBOT))
)
:effect
(and
(Flag256prime-)
(not (Flag256-))
)

)
(:action Prim257Action-1
:parameters ()
:precondition
(and
(not (LEFTOF18-ROBOT))
)
:effect
(and
(Flag257prime-)
(not (Flag257-))
)

)
(:action Flag261Action-5
:parameters ()
:precondition
(and
(RIGHTOF2-ROBOT)
)
:effect
(and
(Flag261-)
(Flag261prime-)
)

)
(:action Flag280Action-10
:parameters ()
:precondition
(and
(BELOWOF18-ROBOT)
)
:effect
(and
(Flag280-)
(Flag280prime-)
)

)
(:action Flag281Action-11
:parameters ()
:precondition
(and
(BELOWOF18-ROBOT)
)
:effect
(and
(Flag281-)
(Flag281prime-)
)

)
(:action Prim517Action-0
:parameters ()
:precondition
(and
(not (BELOWOF18-ROBOT))
)
:effect
(and
(Flag517prime-)
(not (Flag517-))
)

)
(:action Prim518Action-0
:parameters ()
:precondition
(and
(not (BELOWOF18-ROBOT))
)
:effect
(and
(Flag518prime-)
(not (Flag518-))
)

)
)
