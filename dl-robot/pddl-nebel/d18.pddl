(define (domain grounded-SIMPLE-ADL-ROBOT)
(:requirements
:strips
)
(:predicates
(Flag473-)
(Flag258-)
(Flag257-)
(Flag256-)
(Flag255-)
(Flag21-)
(Flag20-)
(Flag4-)
(Flag2-)
(ROW16-ROBOT)
(ROW15-ROBOT)
(ROW14-ROBOT)
(ROW13-ROBOT)
(ROW12-ROBOT)
(ROW11-ROBOT)
(ROW10-ROBOT)
(ROW9-ROBOT)
(ROW8-ROBOT)
(ROW7-ROBOT)
(ROW6-ROBOT)
(ROW5-ROBOT)
(ROW4-ROBOT)
(ROW3-ROBOT)
(ROW2-ROBOT)
(ROW1-ROBOT)
(ROW0-ROBOT)
(ABOVEOF16-ROBOT)
(ABOVEOF15-ROBOT)
(ABOVEOF14-ROBOT)
(ABOVEOF13-ROBOT)
(ABOVEOF12-ROBOT)
(ABOVEOF11-ROBOT)
(ABOVEOF10-ROBOT)
(ABOVEOF9-ROBOT)
(ABOVEOF8-ROBOT)
(ABOVEOF7-ROBOT)
(ABOVEOF6-ROBOT)
(ABOVEOF5-ROBOT)
(ABOVEOF4-ROBOT)
(ABOVEOF3-ROBOT)
(ABOVEOF2-ROBOT)
(ABOVEOF1-ROBOT)
(BELOWOF16-ROBOT)
(BELOWOF15-ROBOT)
(BELOWOF14-ROBOT)
(BELOWOF13-ROBOT)
(BELOWOF12-ROBOT)
(BELOWOF11-ROBOT)
(BELOWOF10-ROBOT)
(BELOWOF9-ROBOT)
(BELOWOF8-ROBOT)
(BELOWOF7-ROBOT)
(BELOWOF6-ROBOT)
(BELOWOF5-ROBOT)
(BELOWOF4-ROBOT)
(BELOWOF3-ROBOT)
(BELOWOF2-ROBOT)
(BELOWOF1-ROBOT)
(CHECKCONSISTENCY-)
(ROW17-ROBOT)
(BELOWOF18-ROBOT)
(NOT-BELOWOF17-ROBOT)
(ABOVEOF17-ROBOT)
(Flag472-)
(Flag238-)
(Flag236-)
(Flag235-)
(Flag233-)
(Flag232-)
(Flag231-)
(Flag229-)
(Flag228-)
(Flag227-)
(Flag226-)
(Flag224-)
(Flag223-)
(Flag222-)
(Flag221-)
(Flag220-)
(Flag219-)
(Flag217-)
(Flag216-)
(Flag215-)
(Flag214-)
(Flag213-)
(Flag212-)
(Flag211-)
(Flag209-)
(Flag208-)
(Flag207-)
(Flag206-)
(Flag205-)
(Flag204-)
(Flag203-)
(Flag201-)
(Flag200-)
(Flag199-)
(Flag198-)
(Flag197-)
(Flag196-)
(Flag195-)
(Flag194-)
(Flag192-)
(Flag191-)
(Flag190-)
(Flag189-)
(Flag188-)
(Flag187-)
(Flag186-)
(Flag185-)
(Flag184-)
(Flag183-)
(Flag181-)
(Flag180-)
(Flag179-)
(Flag178-)
(Flag177-)
(Flag176-)
(Flag175-)
(Flag174-)
(Flag173-)
(Flag172-)
(Flag171-)
(Flag170-)
(Flag168-)
(Flag167-)
(Flag166-)
(Flag165-)
(Flag164-)
(Flag163-)
(Flag162-)
(Flag161-)
(Flag160-)
(Flag159-)
(Flag158-)
(Flag157-)
(Flag155-)
(Flag154-)
(Flag153-)
(Flag152-)
(Flag151-)
(Flag150-)
(Flag149-)
(Flag148-)
(Flag147-)
(Flag146-)
(Flag145-)
(Flag144-)
(Flag143-)
(Flag142-)
(Flag140-)
(Flag139-)
(Flag138-)
(Flag137-)
(Flag136-)
(Flag135-)
(Flag134-)
(Flag133-)
(Flag132-)
(Flag131-)
(Flag130-)
(Flag129-)
(Flag128-)
(Flag127-)
(Flag126-)
(Flag124-)
(Flag123-)
(Flag122-)
(Flag121-)
(Flag120-)
(Flag119-)
(Flag118-)
(Flag117-)
(Flag116-)
(Flag115-)
(Flag114-)
(Flag113-)
(Flag112-)
(Flag111-)
(Flag110-)
(Flag108-)
(Flag107-)
(Flag106-)
(Flag105-)
(Flag104-)
(Flag103-)
(Flag102-)
(Flag101-)
(Flag100-)
(Flag99-)
(Flag98-)
(Flag97-)
(Flag96-)
(Flag95-)
(Flag94-)
(Flag93-)
(Flag92-)
(Flag90-)
(Flag89-)
(Flag88-)
(Flag87-)
(Flag86-)
(Flag85-)
(Flag84-)
(Flag83-)
(Flag82-)
(Flag81-)
(Flag80-)
(Flag79-)
(Flag78-)
(Flag77-)
(Flag76-)
(Flag75-)
(Flag74-)
(Flag72-)
(Flag71-)
(Flag70-)
(Flag69-)
(Flag68-)
(Flag67-)
(Flag66-)
(Flag65-)
(Flag64-)
(Flag63-)
(Flag62-)
(Flag61-)
(Flag60-)
(Flag59-)
(Flag58-)
(Flag57-)
(Flag56-)
(Flag55-)
(Flag54-)
(Flag53-)
(Flag52-)
(Flag51-)
(Flag50-)
(Flag49-)
(Flag48-)
(Flag47-)
(Flag46-)
(Flag45-)
(Flag44-)
(Flag43-)
(Flag42-)
(Flag41-)
(Flag40-)
(Flag39-)
(Flag38-)
(Flag37-)
(Flag36-)
(Flag35-)
(Flag34-)
(Flag33-)
(Flag32-)
(Flag31-)
(Flag30-)
(Flag29-)
(Flag28-)
(Flag27-)
(Flag26-)
(Flag25-)
(Flag24-)
(Flag23-)
(Flag22-)
(Flag19-)
(Flag18-)
(Flag17-)
(Flag16-)
(Flag15-)
(Flag14-)
(Flag13-)
(Flag12-)
(Flag11-)
(Flag10-)
(Flag9-)
(Flag8-)
(Flag7-)
(Flag6-)
(Flag5-)
(Flag3-)
(ERROR-)
(COLUMN16-ROBOT)
(COLUMN15-ROBOT)
(COLUMN14-ROBOT)
(COLUMN13-ROBOT)
(COLUMN12-ROBOT)
(COLUMN11-ROBOT)
(COLUMN10-ROBOT)
(COLUMN9-ROBOT)
(COLUMN8-ROBOT)
(COLUMN7-ROBOT)
(COLUMN6-ROBOT)
(COLUMN5-ROBOT)
(COLUMN4-ROBOT)
(COLUMN3-ROBOT)
(COLUMN2-ROBOT)
(COLUMN1-ROBOT)
(COLUMN0-ROBOT)
(RIGHTOF16-ROBOT)
(RIGHTOF15-ROBOT)
(RIGHTOF14-ROBOT)
(RIGHTOF13-ROBOT)
(RIGHTOF12-ROBOT)
(RIGHTOF11-ROBOT)
(RIGHTOF10-ROBOT)
(RIGHTOF9-ROBOT)
(RIGHTOF8-ROBOT)
(RIGHTOF7-ROBOT)
(RIGHTOF6-ROBOT)
(RIGHTOF5-ROBOT)
(RIGHTOF4-ROBOT)
(RIGHTOF3-ROBOT)
(RIGHTOF1-ROBOT)
(NOT-RIGHTOF2-ROBOT)
(RIGHTOF0-ROBOT)
(LEFTOF16-ROBOT)
(LEFTOF15-ROBOT)
(LEFTOF14-ROBOT)
(LEFTOF13-ROBOT)
(LEFTOF12-ROBOT)
(LEFTOF11-ROBOT)
(LEFTOF10-ROBOT)
(LEFTOF9-ROBOT)
(LEFTOF8-ROBOT)
(LEFTOF7-ROBOT)
(LEFTOF6-ROBOT)
(LEFTOF5-ROBOT)
(LEFTOF4-ROBOT)
(LEFTOF3-ROBOT)
(LEFTOF2-ROBOT)
(LEFTOF1-ROBOT)
(COLUMN17-ROBOT)
(LEFTOF18-ROBOT)
(NOT-LEFTOF17-ROBOT)
(RIGHTOF17-ROBOT)
(Flag474-)
(Flag470-)
(Flag468-)
(Flag467-)
(Flag465-)
(Flag464-)
(Flag463-)
(Flag462-)
(Flag460-)
(Flag459-)
(Flag458-)
(Flag457-)
(Flag455-)
(Flag454-)
(Flag453-)
(Flag452-)
(Flag451-)
(Flag449-)
(Flag448-)
(Flag447-)
(Flag446-)
(Flag445-)
(Flag444-)
(Flag443-)
(Flag441-)
(Flag440-)
(Flag439-)
(Flag438-)
(Flag437-)
(Flag436-)
(Flag435-)
(Flag433-)
(Flag432-)
(Flag431-)
(Flag430-)
(Flag429-)
(Flag428-)
(Flag427-)
(Flag426-)
(Flag425-)
(Flag423-)
(Flag422-)
(Flag421-)
(Flag420-)
(Flag419-)
(Flag418-)
(Flag417-)
(Flag416-)
(Flag415-)
(Flag413-)
(Flag412-)
(Flag411-)
(Flag410-)
(Flag409-)
(Flag408-)
(Flag407-)
(Flag406-)
(Flag405-)
(Flag404-)
(Flag403-)
(Flag401-)
(Flag400-)
(Flag399-)
(Flag398-)
(Flag397-)
(Flag396-)
(Flag395-)
(Flag394-)
(Flag393-)
(Flag392-)
(Flag391-)
(Flag390-)
(Flag388-)
(Flag387-)
(Flag386-)
(Flag385-)
(Flag384-)
(Flag383-)
(Flag382-)
(Flag381-)
(Flag380-)
(Flag379-)
(Flag378-)
(Flag377-)
(Flag376-)
(Flag375-)
(Flag373-)
(Flag372-)
(Flag371-)
(Flag370-)
(Flag369-)
(Flag368-)
(Flag367-)
(Flag366-)
(Flag365-)
(Flag364-)
(Flag363-)
(Flag362-)
(Flag361-)
(Flag359-)
(Flag358-)
(Flag357-)
(Flag356-)
(Flag355-)
(Flag354-)
(Flag353-)
(Flag352-)
(Flag351-)
(Flag350-)
(Flag349-)
(Flag348-)
(Flag347-)
(Flag346-)
(Flag345-)
(Flag344-)
(Flag342-)
(Flag341-)
(Flag340-)
(Flag339-)
(Flag338-)
(Flag337-)
(Flag336-)
(Flag335-)
(Flag334-)
(Flag333-)
(Flag332-)
(Flag331-)
(Flag330-)
(Flag329-)
(Flag328-)
(Flag327-)
(Flag326-)
(Flag324-)
(Flag323-)
(Flag322-)
(Flag321-)
(Flag320-)
(Flag319-)
(Flag318-)
(Flag317-)
(Flag316-)
(Flag315-)
(Flag314-)
(Flag313-)
(Flag312-)
(Flag311-)
(Flag310-)
(Flag309-)
(Flag307-)
(Flag306-)
(Flag305-)
(Flag304-)
(Flag303-)
(Flag302-)
(Flag301-)
(Flag300-)
(Flag299-)
(Flag298-)
(Flag297-)
(Flag296-)
(Flag295-)
(Flag294-)
(Flag293-)
(Flag292-)
(Flag291-)
(Flag290-)
(Flag289-)
(Flag288-)
(Flag287-)
(Flag286-)
(Flag285-)
(Flag284-)
(Flag283-)
(Flag282-)
(Flag281-)
(Flag280-)
(Flag279-)
(Flag278-)
(Flag277-)
(Flag276-)
(Flag275-)
(Flag274-)
(Flag273-)
(Flag272-)
(Flag271-)
(Flag270-)
(Flag269-)
(Flag268-)
(Flag267-)
(Flag266-)
(Flag265-)
(Flag264-)
(Flag263-)
(Flag262-)
(Flag261-)
(Flag260-)
(Flag259-)
(Flag254-)
(Flag253-)
(Flag252-)
(Flag251-)
(Flag250-)
(Flag249-)
(Flag248-)
(Flag247-)
(Flag246-)
(Flag245-)
(Flag244-)
(Flag243-)
(Flag242-)
(Flag241-)
(Flag240-)
(Flag239-)
(Flag237-)
(Flag234-)
(Flag230-)
(Flag225-)
(Flag218-)
(Flag210-)
(Flag202-)
(Flag193-)
(Flag182-)
(Flag169-)
(Flag156-)
(Flag141-)
(Flag125-)
(Flag109-)
(Flag91-)
(Flag73-)
(Flag1-)
(Flag475-)
(Flag471-)
(Flag469-)
(Flag466-)
(Flag461-)
(Flag456-)
(Flag450-)
(Flag442-)
(Flag434-)
(Flag424-)
(Flag414-)
(Flag402-)
(Flag389-)
(Flag374-)
(Flag360-)
(Flag343-)
(Flag325-)
(Flag308-)
(NOT-COLUMN17-ROBOT)
(NOT-COLUMN16-ROBOT)
(NOT-COLUMN15-ROBOT)
(NOT-COLUMN14-ROBOT)
(NOT-COLUMN13-ROBOT)
(NOT-COLUMN12-ROBOT)
(NOT-COLUMN11-ROBOT)
(NOT-COLUMN10-ROBOT)
(NOT-COLUMN9-ROBOT)
(NOT-COLUMN8-ROBOT)
(NOT-COLUMN7-ROBOT)
(NOT-COLUMN6-ROBOT)
(NOT-COLUMN5-ROBOT)
(NOT-COLUMN4-ROBOT)
(NOT-COLUMN3-ROBOT)
(NOT-COLUMN2-ROBOT)
(NOT-COLUMN1-ROBOT)
(NOT-LEFTOF18-ROBOT)
(LEFTOF17-ROBOT)
(NOT-LEFTOF16-ROBOT)
(NOT-LEFTOF15-ROBOT)
(NOT-LEFTOF14-ROBOT)
(NOT-LEFTOF13-ROBOT)
(NOT-LEFTOF12-ROBOT)
(NOT-LEFTOF11-ROBOT)
(NOT-LEFTOF10-ROBOT)
(NOT-LEFTOF9-ROBOT)
(NOT-LEFTOF8-ROBOT)
(NOT-LEFTOF7-ROBOT)
(NOT-LEFTOF6-ROBOT)
(NOT-LEFTOF5-ROBOT)
(NOT-LEFTOF4-ROBOT)
(NOT-LEFTOF3-ROBOT)
(NOT-LEFTOF2-ROBOT)
(NOT-RIGHTOF17-ROBOT)
(NOT-RIGHTOF16-ROBOT)
(NOT-RIGHTOF15-ROBOT)
(NOT-RIGHTOF14-ROBOT)
(NOT-RIGHTOF13-ROBOT)
(NOT-RIGHTOF12-ROBOT)
(NOT-RIGHTOF11-ROBOT)
(NOT-RIGHTOF10-ROBOT)
(NOT-RIGHTOF9-ROBOT)
(NOT-RIGHTOF8-ROBOT)
(NOT-RIGHTOF7-ROBOT)
(NOT-RIGHTOF6-ROBOT)
(NOT-RIGHTOF5-ROBOT)
(NOT-RIGHTOF4-ROBOT)
(NOT-RIGHTOF3-ROBOT)
(NOT-RIGHTOF1-ROBOT)
(NOT-CHECKCONSISTENCY-)
(NOT-COLUMN0-ROBOT)
(RIGHTOF2-ROBOT)
(NOT-RIGHTOF0-ROBOT)
(NOT-LEFTOF1-ROBOT)
(NOT-ERROR-)
(NOT-ROW17-ROBOT)
(NOT-ROW16-ROBOT)
(NOT-ROW15-ROBOT)
(NOT-ROW14-ROBOT)
(NOT-ROW13-ROBOT)
(NOT-ROW12-ROBOT)
(NOT-ROW11-ROBOT)
(NOT-ROW10-ROBOT)
(NOT-ROW9-ROBOT)
(NOT-ROW8-ROBOT)
(NOT-ROW7-ROBOT)
(NOT-ROW6-ROBOT)
(NOT-ROW5-ROBOT)
(NOT-ROW4-ROBOT)
(NOT-ROW3-ROBOT)
(NOT-ROW2-ROBOT)
(NOT-ROW1-ROBOT)
(NOT-BELOWOF18-ROBOT)
(BELOWOF17-ROBOT)
(NOT-BELOWOF16-ROBOT)
(NOT-BELOWOF15-ROBOT)
(NOT-BELOWOF14-ROBOT)
(NOT-BELOWOF13-ROBOT)
(NOT-BELOWOF12-ROBOT)
(NOT-BELOWOF11-ROBOT)
(NOT-BELOWOF10-ROBOT)
(NOT-BELOWOF9-ROBOT)
(NOT-BELOWOF8-ROBOT)
(NOT-BELOWOF7-ROBOT)
(NOT-BELOWOF6-ROBOT)
(NOT-BELOWOF5-ROBOT)
(NOT-BELOWOF4-ROBOT)
(NOT-BELOWOF3-ROBOT)
(NOT-BELOWOF2-ROBOT)
(NOT-ABOVEOF17-ROBOT)
(NOT-ABOVEOF16-ROBOT)
(NOT-ABOVEOF15-ROBOT)
(NOT-ABOVEOF14-ROBOT)
(NOT-ABOVEOF13-ROBOT)
(NOT-ABOVEOF12-ROBOT)
(NOT-ABOVEOF11-ROBOT)
(NOT-ABOVEOF10-ROBOT)
(NOT-ABOVEOF9-ROBOT)
(NOT-ABOVEOF8-ROBOT)
(NOT-ABOVEOF7-ROBOT)
(NOT-ABOVEOF6-ROBOT)
(NOT-ABOVEOF5-ROBOT)
(NOT-ABOVEOF4-ROBOT)
(NOT-ABOVEOF3-ROBOT)
(NOT-ABOVEOF2-ROBOT)
(NOT-ABOVEOF1-ROBOT)
(NOT-ROW0-ROBOT)
(NOT-BELOWOF1-ROBOT)
(Flag473prime-)
(Flag258prime-)
(Flag257prime-)
(Flag256prime-)
(Flag255prime-)
(Flag21prime-)
(Flag20prime-)
(Flag4prime-)
(Flag2prime-)
(Flag472prime-)
(Flag238prime-)
(Flag236prime-)
(Flag235prime-)
(Flag233prime-)
(Flag232prime-)
(Flag231prime-)
(Flag229prime-)
(Flag228prime-)
(Flag227prime-)
(Flag226prime-)
(Flag224prime-)
(Flag223prime-)
(Flag222prime-)
(Flag221prime-)
(Flag220prime-)
(Flag219prime-)
(Flag217prime-)
(Flag216prime-)
(Flag215prime-)
(Flag214prime-)
(Flag213prime-)
(Flag212prime-)
(Flag211prime-)
(Flag209prime-)
(Flag208prime-)
(Flag207prime-)
(Flag206prime-)
(Flag205prime-)
(Flag204prime-)
(Flag203prime-)
(Flag201prime-)
(Flag200prime-)
(Flag199prime-)
(Flag198prime-)
(Flag197prime-)
(Flag196prime-)
(Flag195prime-)
(Flag194prime-)
(Flag192prime-)
(Flag191prime-)
(Flag190prime-)
(Flag189prime-)
(Flag188prime-)
(Flag187prime-)
(Flag186prime-)
(Flag185prime-)
(Flag184prime-)
(Flag183prime-)
(Flag181prime-)
(Flag180prime-)
(Flag179prime-)
(Flag178prime-)
(Flag177prime-)
(Flag176prime-)
(Flag175prime-)
(Flag174prime-)
(Flag173prime-)
(Flag172prime-)
(Flag171prime-)
(Flag170prime-)
(Flag168prime-)
(Flag167prime-)
(Flag166prime-)
(Flag165prime-)
(Flag164prime-)
(Flag163prime-)
(Flag162prime-)
(Flag161prime-)
(Flag160prime-)
(Flag159prime-)
(Flag158prime-)
(Flag157prime-)
(Flag155prime-)
(Flag154prime-)
(Flag153prime-)
(Flag152prime-)
(Flag151prime-)
(Flag150prime-)
(Flag149prime-)
(Flag148prime-)
(Flag147prime-)
(Flag146prime-)
(Flag145prime-)
(Flag144prime-)
(Flag143prime-)
(Flag142prime-)
(Flag140prime-)
(Flag139prime-)
(Flag138prime-)
(Flag137prime-)
(Flag136prime-)
(Flag135prime-)
(Flag134prime-)
(Flag133prime-)
(Flag132prime-)
(Flag131prime-)
(Flag130prime-)
(Flag129prime-)
(Flag128prime-)
(Flag127prime-)
(Flag126prime-)
(Flag124prime-)
(Flag123prime-)
(Flag122prime-)
(Flag121prime-)
(Flag120prime-)
(Flag119prime-)
(Flag118prime-)
(Flag117prime-)
(Flag116prime-)
(Flag115prime-)
(Flag114prime-)
(Flag113prime-)
(Flag112prime-)
(Flag111prime-)
(Flag110prime-)
(Flag108prime-)
(Flag107prime-)
(Flag106prime-)
(Flag105prime-)
(Flag104prime-)
(Flag103prime-)
(Flag102prime-)
(Flag101prime-)
(Flag100prime-)
(Flag99prime-)
(Flag98prime-)
(Flag97prime-)
(Flag96prime-)
(Flag95prime-)
(Flag94prime-)
(Flag93prime-)
(Flag92prime-)
(Flag90prime-)
(Flag89prime-)
(Flag88prime-)
(Flag87prime-)
(Flag86prime-)
(Flag85prime-)
(Flag84prime-)
(Flag83prime-)
(Flag82prime-)
(Flag81prime-)
(Flag80prime-)
(Flag79prime-)
(Flag78prime-)
(Flag77prime-)
(Flag76prime-)
(Flag75prime-)
(Flag74prime-)
(Flag72prime-)
(Flag71prime-)
(Flag70prime-)
(Flag69prime-)
(Flag68prime-)
(Flag67prime-)
(Flag66prime-)
(Flag65prime-)
(Flag64prime-)
(Flag63prime-)
(Flag62prime-)
(Flag61prime-)
(Flag60prime-)
(Flag59prime-)
(Flag58prime-)
(Flag57prime-)
(Flag56prime-)
(Flag55prime-)
(Flag54prime-)
(Flag53prime-)
(Flag52prime-)
(Flag51prime-)
(Flag50prime-)
(Flag49prime-)
(Flag48prime-)
(Flag47prime-)
(Flag46prime-)
(Flag45prime-)
(Flag44prime-)
(Flag43prime-)
(Flag42prime-)
(Flag41prime-)
(Flag40prime-)
(Flag39prime-)
(Flag38prime-)
(Flag37prime-)
(Flag36prime-)
(Flag35prime-)
(Flag34prime-)
(Flag33prime-)
(Flag32prime-)
(Flag31prime-)
(Flag30prime-)
(Flag29prime-)
(Flag28prime-)
(Flag27prime-)
(Flag26prime-)
(Flag25prime-)
(Flag24prime-)
(Flag23prime-)
(Flag22prime-)
(Flag19prime-)
(Flag18prime-)
(Flag17prime-)
(Flag16prime-)
(Flag15prime-)
(Flag14prime-)
(Flag13prime-)
(Flag12prime-)
(Flag11prime-)
(Flag10prime-)
(Flag9prime-)
(Flag8prime-)
(Flag7prime-)
(Flag6prime-)
(Flag5prime-)
(Flag3prime-)
(Flag474prime-)
(Flag470prime-)
(Flag468prime-)
(Flag467prime-)
(Flag465prime-)
(Flag464prime-)
(Flag463prime-)
(Flag462prime-)
(Flag460prime-)
(Flag459prime-)
(Flag458prime-)
(Flag457prime-)
(Flag455prime-)
(Flag454prime-)
(Flag453prime-)
(Flag452prime-)
(Flag451prime-)
(Flag449prime-)
(Flag448prime-)
(Flag447prime-)
(Flag446prime-)
(Flag445prime-)
(Flag444prime-)
(Flag443prime-)
(Flag441prime-)
(Flag440prime-)
(Flag439prime-)
(Flag438prime-)
(Flag437prime-)
(Flag436prime-)
(Flag435prime-)
(Flag433prime-)
(Flag432prime-)
(Flag431prime-)
(Flag430prime-)
(Flag429prime-)
(Flag428prime-)
(Flag427prime-)
(Flag426prime-)
(Flag425prime-)
(Flag423prime-)
(Flag422prime-)
(Flag421prime-)
(Flag420prime-)
(Flag419prime-)
(Flag418prime-)
(Flag417prime-)
(Flag416prime-)
(Flag415prime-)
(Flag413prime-)
(Flag412prime-)
(Flag411prime-)
(Flag410prime-)
(Flag409prime-)
(Flag408prime-)
(Flag407prime-)
(Flag406prime-)
(Flag405prime-)
(Flag404prime-)
(Flag403prime-)
(Flag401prime-)
(Flag400prime-)
(Flag399prime-)
(Flag398prime-)
(Flag397prime-)
(Flag396prime-)
(Flag395prime-)
(Flag394prime-)
(Flag393prime-)
(Flag392prime-)
(Flag391prime-)
(Flag390prime-)
(Flag388prime-)
(Flag387prime-)
(Flag386prime-)
(Flag385prime-)
(Flag384prime-)
(Flag383prime-)
(Flag382prime-)
(Flag381prime-)
(Flag380prime-)
(Flag379prime-)
(Flag378prime-)
(Flag377prime-)
(Flag376prime-)
(Flag375prime-)
(Flag373prime-)
(Flag372prime-)
(Flag371prime-)
(Flag370prime-)
(Flag369prime-)
(Flag368prime-)
(Flag367prime-)
(Flag366prime-)
(Flag365prime-)
(Flag364prime-)
(Flag363prime-)
(Flag362prime-)
(Flag361prime-)
(Flag359prime-)
(Flag358prime-)
(Flag357prime-)
(Flag356prime-)
(Flag355prime-)
(Flag354prime-)
(Flag353prime-)
(Flag352prime-)
(Flag351prime-)
(Flag350prime-)
(Flag349prime-)
(Flag348prime-)
(Flag347prime-)
(Flag346prime-)
(Flag345prime-)
(Flag344prime-)
(Flag342prime-)
(Flag341prime-)
(Flag340prime-)
(Flag339prime-)
(Flag338prime-)
(Flag337prime-)
(Flag336prime-)
(Flag335prime-)
(Flag334prime-)
(Flag333prime-)
(Flag332prime-)
(Flag331prime-)
(Flag330prime-)
(Flag329prime-)
(Flag328prime-)
(Flag327prime-)
(Flag326prime-)
(Flag324prime-)
(Flag323prime-)
(Flag322prime-)
(Flag321prime-)
(Flag320prime-)
(Flag319prime-)
(Flag318prime-)
(Flag317prime-)
(Flag316prime-)
(Flag315prime-)
(Flag314prime-)
(Flag313prime-)
(Flag312prime-)
(Flag311prime-)
(Flag310prime-)
(Flag309prime-)
(Flag307prime-)
(Flag306prime-)
(Flag305prime-)
(Flag304prime-)
(Flag303prime-)
(Flag302prime-)
(Flag301prime-)
(Flag300prime-)
(Flag299prime-)
(Flag298prime-)
(Flag297prime-)
(Flag296prime-)
(Flag295prime-)
(Flag294prime-)
(Flag293prime-)
(Flag292prime-)
(Flag291prime-)
(Flag290prime-)
(Flag289prime-)
(Flag288prime-)
(Flag287prime-)
(Flag286prime-)
(Flag285prime-)
(Flag284prime-)
(Flag283prime-)
(Flag282prime-)
(Flag281prime-)
(Flag280prime-)
(Flag279prime-)
(Flag278prime-)
(Flag277prime-)
(Flag276prime-)
(Flag275prime-)
(Flag274prime-)
(Flag273prime-)
(Flag272prime-)
(Flag271prime-)
(Flag270prime-)
(Flag269prime-)
(Flag268prime-)
(Flag267prime-)
(Flag266prime-)
(Flag265prime-)
(Flag264prime-)
(Flag263prime-)
(Flag262prime-)
(Flag261prime-)
(Flag260prime-)
(Flag259prime-)
(Flag254prime-)
(Flag253prime-)
(Flag252prime-)
(Flag251prime-)
(Flag250prime-)
(Flag249prime-)
(Flag248prime-)
(Flag247prime-)
(Flag246prime-)
(Flag245prime-)
(Flag244prime-)
(Flag243prime-)
(Flag242prime-)
(Flag241prime-)
(Flag240prime-)
(Flag239prime-)
(Flag237prime-)
(Flag234prime-)
(Flag230prime-)
(Flag225prime-)
(Flag218prime-)
(Flag210prime-)
(Flag202prime-)
(Flag193prime-)
(Flag182prime-)
(Flag169prime-)
(Flag156prime-)
(Flag141prime-)
(Flag125prime-)
(Flag109prime-)
(Flag91prime-)
(Flag73prime-)
(Flag1prime-)
(Flag475prime-)
(Flag471prime-)
(Flag469prime-)
(Flag466prime-)
(Flag461prime-)
(Flag456prime-)
(Flag450prime-)
(Flag442prime-)
(Flag434prime-)
(Flag424prime-)
(Flag414prime-)
(Flag402prime-)
(Flag389prime-)
(Flag374prime-)
(Flag360prime-)
(Flag343prime-)
(Flag325prime-)
(Flag308prime-)
)
(:action Flag308Action-0
:parameters ()
:precondition
(and
(Flag273-)
(Flag273prime-)
)
:effect
(and
(Flag308-)
(Flag308prime-)
)

)
(:action Flag308Action-1
:parameters ()
:precondition
(and
(Flag274-)
(Flag274prime-)
)
:effect
(and
(Flag308-)
(Flag308prime-)
)

)
(:action Flag308Action-2
:parameters ()
:precondition
(and
(Flag275-)
(Flag275prime-)
)
:effect
(and
(Flag308-)
(Flag308prime-)
)

)
(:action Flag308Action-3
:parameters ()
:precondition
(and
(Flag276-)
(Flag276prime-)
)
:effect
(and
(Flag308-)
(Flag308prime-)
)

)
(:action Flag308Action-4
:parameters ()
:precondition
(and
(Flag277-)
(Flag277prime-)
)
:effect
(and
(Flag308-)
(Flag308prime-)
)

)
(:action Flag308Action-5
:parameters ()
:precondition
(and
(Flag278-)
(Flag278prime-)
)
:effect
(and
(Flag308-)
(Flag308prime-)
)

)
(:action Flag308Action-6
:parameters ()
:precondition
(and
(Flag279-)
(Flag279prime-)
)
:effect
(and
(Flag308-)
(Flag308prime-)
)

)
(:action Flag308Action-7
:parameters ()
:precondition
(and
(Flag280-)
(Flag280prime-)
)
:effect
(and
(Flag308-)
(Flag308prime-)
)

)
(:action Flag308Action-8
:parameters ()
:precondition
(and
(Flag281-)
(Flag281prime-)
)
:effect
(and
(Flag308-)
(Flag308prime-)
)

)
(:action Flag308Action-9
:parameters ()
:precondition
(and
(Flag282-)
(Flag282prime-)
)
:effect
(and
(Flag308-)
(Flag308prime-)
)

)
(:action Flag308Action-10
:parameters ()
:precondition
(and
(Flag283-)
(Flag283prime-)
)
:effect
(and
(Flag308-)
(Flag308prime-)
)

)
(:action Flag308Action-11
:parameters ()
:precondition
(and
(Flag284-)
(Flag284prime-)
)
:effect
(and
(Flag308-)
(Flag308prime-)
)

)
(:action Flag308Action-12
:parameters ()
:precondition
(and
(Flag285-)
(Flag285prime-)
)
:effect
(and
(Flag308-)
(Flag308prime-)
)

)
(:action Flag308Action-13
:parameters ()
:precondition
(and
(Flag286-)
(Flag286prime-)
)
:effect
(and
(Flag308-)
(Flag308prime-)
)

)
(:action Flag308Action-14
:parameters ()
:precondition
(and
(Flag287-)
(Flag287prime-)
)
:effect
(and
(Flag308-)
(Flag308prime-)
)

)
(:action Flag308Action-15
:parameters ()
:precondition
(and
(Flag288-)
(Flag288prime-)
)
:effect
(and
(Flag308-)
(Flag308prime-)
)

)
(:action Flag308Action-16
:parameters ()
:precondition
(and
(Flag289-)
(Flag289prime-)
)
:effect
(and
(Flag308-)
(Flag308prime-)
)

)
(:action Flag308Action-17
:parameters ()
:precondition
(and
(Flag290-)
(Flag290prime-)
)
:effect
(and
(Flag308-)
(Flag308prime-)
)

)
(:action Flag308Action-18
:parameters ()
:precondition
(and
(Flag291-)
(Flag291prime-)
)
:effect
(and
(Flag308-)
(Flag308prime-)
)

)
(:action Flag308Action-19
:parameters ()
:precondition
(and
(Flag292-)
(Flag292prime-)
)
:effect
(and
(Flag308-)
(Flag308prime-)
)

)
(:action Flag308Action-20
:parameters ()
:precondition
(and
(Flag293-)
(Flag293prime-)
)
:effect
(and
(Flag308-)
(Flag308prime-)
)

)
(:action Flag308Action-21
:parameters ()
:precondition
(and
(Flag294-)
(Flag294prime-)
)
:effect
(and
(Flag308-)
(Flag308prime-)
)

)
(:action Flag308Action-22
:parameters ()
:precondition
(and
(Flag295-)
(Flag295prime-)
)
:effect
(and
(Flag308-)
(Flag308prime-)
)

)
(:action Flag308Action-23
:parameters ()
:precondition
(and
(Flag296-)
(Flag296prime-)
)
:effect
(and
(Flag308-)
(Flag308prime-)
)

)
(:action Flag308Action-24
:parameters ()
:precondition
(and
(Flag297-)
(Flag297prime-)
)
:effect
(and
(Flag308-)
(Flag308prime-)
)

)
(:action Flag308Action-25
:parameters ()
:precondition
(and
(Flag298-)
(Flag298prime-)
)
:effect
(and
(Flag308-)
(Flag308prime-)
)

)
(:action Flag308Action-26
:parameters ()
:precondition
(and
(Flag299-)
(Flag299prime-)
)
:effect
(and
(Flag308-)
(Flag308prime-)
)

)
(:action Flag308Action-27
:parameters ()
:precondition
(and
(Flag300-)
(Flag300prime-)
)
:effect
(and
(Flag308-)
(Flag308prime-)
)

)
(:action Flag308Action-28
:parameters ()
:precondition
(and
(Flag301-)
(Flag301prime-)
)
:effect
(and
(Flag308-)
(Flag308prime-)
)

)
(:action Flag308Action-29
:parameters ()
:precondition
(and
(Flag302-)
(Flag302prime-)
)
:effect
(and
(Flag308-)
(Flag308prime-)
)

)
(:action Flag308Action-30
:parameters ()
:precondition
(and
(Flag303-)
(Flag303prime-)
)
:effect
(and
(Flag308-)
(Flag308prime-)
)

)
(:action Flag308Action-31
:parameters ()
:precondition
(and
(Flag304-)
(Flag304prime-)
)
:effect
(and
(Flag308-)
(Flag308prime-)
)

)
(:action Flag308Action-32
:parameters ()
:precondition
(and
(Flag305-)
(Flag305prime-)
)
:effect
(and
(Flag308-)
(Flag308prime-)
)

)
(:action Flag308Action-33
:parameters ()
:precondition
(and
(Flag306-)
(Flag306prime-)
)
:effect
(and
(Flag308-)
(Flag308prime-)
)

)
(:action Flag308Action-34
:parameters ()
:precondition
(and
(Flag307-)
(Flag307prime-)
)
:effect
(and
(Flag308-)
(Flag308prime-)
)

)
(:action Prim308Action
:parameters ()
:precondition
(and
(not (Flag273-))
(Flag273prime-)
(not (Flag274-))
(Flag274prime-)
(not (Flag275-))
(Flag275prime-)
(not (Flag276-))
(Flag276prime-)
(not (Flag277-))
(Flag277prime-)
(not (Flag278-))
(Flag278prime-)
(not (Flag279-))
(Flag279prime-)
(not (Flag280-))
(Flag280prime-)
(not (Flag281-))
(Flag281prime-)
(not (Flag282-))
(Flag282prime-)
(not (Flag283-))
(Flag283prime-)
(not (Flag284-))
(Flag284prime-)
(not (Flag285-))
(Flag285prime-)
(not (Flag286-))
(Flag286prime-)
(not (Flag287-))
(Flag287prime-)
(not (Flag288-))
(Flag288prime-)
(not (Flag289-))
(Flag289prime-)
(not (Flag290-))
(Flag290prime-)
(not (Flag291-))
(Flag291prime-)
(not (Flag292-))
(Flag292prime-)
(not (Flag293-))
(Flag293prime-)
(not (Flag294-))
(Flag294prime-)
(not (Flag295-))
(Flag295prime-)
(not (Flag296-))
(Flag296prime-)
(not (Flag297-))
(Flag297prime-)
(not (Flag298-))
(Flag298prime-)
(not (Flag299-))
(Flag299prime-)
(not (Flag300-))
(Flag300prime-)
(not (Flag301-))
(Flag301prime-)
(not (Flag302-))
(Flag302prime-)
(not (Flag303-))
(Flag303prime-)
(not (Flag304-))
(Flag304prime-)
(not (Flag305-))
(Flag305prime-)
(not (Flag306-))
(Flag306prime-)
(not (Flag307-))
(Flag307prime-)
)
:effect
(and
(Flag308prime-)
(not (Flag308-))
)

)
(:action Flag325Action-0
:parameters ()
:precondition
(and
(Flag273-)
(Flag273prime-)
)
:effect
(and
(Flag325-)
(Flag325prime-)
)

)
(:action Flag325Action-1
:parameters ()
:precondition
(and
(Flag274-)
(Flag274prime-)
)
:effect
(and
(Flag325-)
(Flag325prime-)
)

)
(:action Flag325Action-2
:parameters ()
:precondition
(and
(Flag275-)
(Flag275prime-)
)
:effect
(and
(Flag325-)
(Flag325prime-)
)

)
(:action Flag325Action-3
:parameters ()
:precondition
(and
(Flag309-)
(Flag309prime-)
)
:effect
(and
(Flag325-)
(Flag325prime-)
)

)
(:action Flag325Action-4
:parameters ()
:precondition
(and
(Flag276-)
(Flag276prime-)
)
:effect
(and
(Flag325-)
(Flag325prime-)
)

)
(:action Flag325Action-5
:parameters ()
:precondition
(and
(Flag277-)
(Flag277prime-)
)
:effect
(and
(Flag325-)
(Flag325prime-)
)

)
(:action Flag325Action-6
:parameters ()
:precondition
(and
(Flag310-)
(Flag310prime-)
)
:effect
(and
(Flag325-)
(Flag325prime-)
)

)
(:action Flag325Action-7
:parameters ()
:precondition
(and
(Flag297-)
(Flag297prime-)
)
:effect
(and
(Flag325-)
(Flag325prime-)
)

)
(:action Flag325Action-8
:parameters ()
:precondition
(and
(Flag311-)
(Flag311prime-)
)
:effect
(and
(Flag325-)
(Flag325prime-)
)

)
(:action Flag325Action-9
:parameters ()
:precondition
(and
(Flag278-)
(Flag278prime-)
)
:effect
(and
(Flag325-)
(Flag325prime-)
)

)
(:action Flag325Action-10
:parameters ()
:precondition
(and
(Flag279-)
(Flag279prime-)
)
:effect
(and
(Flag325-)
(Flag325prime-)
)

)
(:action Flag325Action-11
:parameters ()
:precondition
(and
(Flag312-)
(Flag312prime-)
)
:effect
(and
(Flag325-)
(Flag325prime-)
)

)
(:action Flag325Action-12
:parameters ()
:precondition
(and
(Flag281-)
(Flag281prime-)
)
:effect
(and
(Flag325-)
(Flag325prime-)
)

)
(:action Flag325Action-13
:parameters ()
:precondition
(and
(Flag293-)
(Flag293prime-)
)
:effect
(and
(Flag325-)
(Flag325prime-)
)

)
(:action Flag325Action-14
:parameters ()
:precondition
(and
(Flag284-)
(Flag284prime-)
)
:effect
(and
(Flag325-)
(Flag325prime-)
)

)
(:action Flag325Action-15
:parameters ()
:precondition
(and
(Flag285-)
(Flag285prime-)
)
:effect
(and
(Flag325-)
(Flag325prime-)
)

)
(:action Flag325Action-16
:parameters ()
:precondition
(and
(Flag313-)
(Flag313prime-)
)
:effect
(and
(Flag325-)
(Flag325prime-)
)

)
(:action Flag325Action-17
:parameters ()
:precondition
(and
(Flag286-)
(Flag286prime-)
)
:effect
(and
(Flag325-)
(Flag325prime-)
)

)
(:action Flag325Action-18
:parameters ()
:precondition
(and
(Flag287-)
(Flag287prime-)
)
:effect
(and
(Flag325-)
(Flag325prime-)
)

)
(:action Flag325Action-19
:parameters ()
:precondition
(and
(Flag295-)
(Flag295prime-)
)
:effect
(and
(Flag325-)
(Flag325prime-)
)

)
(:action Flag325Action-20
:parameters ()
:precondition
(and
(Flag288-)
(Flag288prime-)
)
:effect
(and
(Flag325-)
(Flag325prime-)
)

)
(:action Flag325Action-21
:parameters ()
:precondition
(and
(Flag314-)
(Flag314prime-)
)
:effect
(and
(Flag325-)
(Flag325prime-)
)

)
(:action Flag325Action-22
:parameters ()
:precondition
(and
(Flag289-)
(Flag289prime-)
)
:effect
(and
(Flag325-)
(Flag325prime-)
)

)
(:action Flag325Action-23
:parameters ()
:precondition
(and
(Flag290-)
(Flag290prime-)
)
:effect
(and
(Flag325-)
(Flag325prime-)
)

)
(:action Flag325Action-24
:parameters ()
:precondition
(and
(Flag291-)
(Flag291prime-)
)
:effect
(and
(Flag325-)
(Flag325prime-)
)

)
(:action Flag325Action-25
:parameters ()
:precondition
(and
(Flag292-)
(Flag292prime-)
)
:effect
(and
(Flag325-)
(Flag325prime-)
)

)
(:action Flag325Action-26
:parameters ()
:precondition
(and
(Flag315-)
(Flag315prime-)
)
:effect
(and
(Flag325-)
(Flag325prime-)
)

)
(:action Flag325Action-27
:parameters ()
:precondition
(and
(Flag316-)
(Flag316prime-)
)
:effect
(and
(Flag325-)
(Flag325prime-)
)

)
(:action Flag325Action-28
:parameters ()
:precondition
(and
(Flag283-)
(Flag283prime-)
)
:effect
(and
(Flag325-)
(Flag325prime-)
)

)
(:action Flag325Action-29
:parameters ()
:precondition
(and
(Flag294-)
(Flag294prime-)
)
:effect
(and
(Flag325-)
(Flag325prime-)
)

)
(:action Flag325Action-30
:parameters ()
:precondition
(and
(Flag317-)
(Flag317prime-)
)
:effect
(and
(Flag325-)
(Flag325prime-)
)

)
(:action Flag325Action-31
:parameters ()
:precondition
(and
(Flag318-)
(Flag318prime-)
)
:effect
(and
(Flag325-)
(Flag325prime-)
)

)
(:action Flag325Action-32
:parameters ()
:precondition
(and
(Flag296-)
(Flag296prime-)
)
:effect
(and
(Flag325-)
(Flag325prime-)
)

)
(:action Flag325Action-33
:parameters ()
:precondition
(and
(Flag319-)
(Flag319prime-)
)
:effect
(and
(Flag325-)
(Flag325prime-)
)

)
(:action Flag325Action-34
:parameters ()
:precondition
(and
(Flag320-)
(Flag320prime-)
)
:effect
(and
(Flag325-)
(Flag325prime-)
)

)
(:action Flag325Action-35
:parameters ()
:precondition
(and
(Flag321-)
(Flag321prime-)
)
:effect
(and
(Flag325-)
(Flag325prime-)
)

)
(:action Flag325Action-36
:parameters ()
:precondition
(and
(Flag322-)
(Flag322prime-)
)
:effect
(and
(Flag325-)
(Flag325prime-)
)

)
(:action Flag325Action-37
:parameters ()
:precondition
(and
(Flag298-)
(Flag298prime-)
)
:effect
(and
(Flag325-)
(Flag325prime-)
)

)
(:action Flag325Action-38
:parameters ()
:precondition
(and
(Flag299-)
(Flag299prime-)
)
:effect
(and
(Flag325-)
(Flag325prime-)
)

)
(:action Flag325Action-39
:parameters ()
:precondition
(and
(Flag300-)
(Flag300prime-)
)
:effect
(and
(Flag325-)
(Flag325prime-)
)

)
(:action Flag325Action-40
:parameters ()
:precondition
(and
(Flag301-)
(Flag301prime-)
)
:effect
(and
(Flag325-)
(Flag325prime-)
)

)
(:action Flag325Action-41
:parameters ()
:precondition
(and
(Flag302-)
(Flag302prime-)
)
:effect
(and
(Flag325-)
(Flag325prime-)
)

)
(:action Flag325Action-42
:parameters ()
:precondition
(and
(Flag303-)
(Flag303prime-)
)
:effect
(and
(Flag325-)
(Flag325prime-)
)

)
(:action Flag325Action-43
:parameters ()
:precondition
(and
(Flag304-)
(Flag304prime-)
)
:effect
(and
(Flag325-)
(Flag325prime-)
)

)
(:action Flag325Action-44
:parameters ()
:precondition
(and
(Flag305-)
(Flag305prime-)
)
:effect
(and
(Flag325-)
(Flag325prime-)
)

)
(:action Flag325Action-45
:parameters ()
:precondition
(and
(Flag306-)
(Flag306prime-)
)
:effect
(and
(Flag325-)
(Flag325prime-)
)

)
(:action Flag325Action-46
:parameters ()
:precondition
(and
(Flag323-)
(Flag323prime-)
)
:effect
(and
(Flag325-)
(Flag325prime-)
)

)
(:action Flag325Action-47
:parameters ()
:precondition
(and
(Flag307-)
(Flag307prime-)
)
:effect
(and
(Flag325-)
(Flag325prime-)
)

)
(:action Flag325Action-48
:parameters ()
:precondition
(and
(Flag324-)
(Flag324prime-)
)
:effect
(and
(Flag325-)
(Flag325prime-)
)

)
(:action Prim325Action
:parameters ()
:precondition
(and
(not (Flag273-))
(Flag273prime-)
(not (Flag274-))
(Flag274prime-)
(not (Flag275-))
(Flag275prime-)
(not (Flag309-))
(Flag309prime-)
(not (Flag276-))
(Flag276prime-)
(not (Flag277-))
(Flag277prime-)
(not (Flag310-))
(Flag310prime-)
(not (Flag297-))
(Flag297prime-)
(not (Flag311-))
(Flag311prime-)
(not (Flag278-))
(Flag278prime-)
(not (Flag279-))
(Flag279prime-)
(not (Flag312-))
(Flag312prime-)
(not (Flag281-))
(Flag281prime-)
(not (Flag293-))
(Flag293prime-)
(not (Flag284-))
(Flag284prime-)
(not (Flag285-))
(Flag285prime-)
(not (Flag313-))
(Flag313prime-)
(not (Flag286-))
(Flag286prime-)
(not (Flag287-))
(Flag287prime-)
(not (Flag295-))
(Flag295prime-)
(not (Flag288-))
(Flag288prime-)
(not (Flag314-))
(Flag314prime-)
(not (Flag289-))
(Flag289prime-)
(not (Flag290-))
(Flag290prime-)
(not (Flag291-))
(Flag291prime-)
(not (Flag292-))
(Flag292prime-)
(not (Flag315-))
(Flag315prime-)
(not (Flag316-))
(Flag316prime-)
(not (Flag283-))
(Flag283prime-)
(not (Flag294-))
(Flag294prime-)
(not (Flag317-))
(Flag317prime-)
(not (Flag318-))
(Flag318prime-)
(not (Flag296-))
(Flag296prime-)
(not (Flag319-))
(Flag319prime-)
(not (Flag320-))
(Flag320prime-)
(not (Flag321-))
(Flag321prime-)
(not (Flag322-))
(Flag322prime-)
(not (Flag298-))
(Flag298prime-)
(not (Flag299-))
(Flag299prime-)
(not (Flag300-))
(Flag300prime-)
(not (Flag301-))
(Flag301prime-)
(not (Flag302-))
(Flag302prime-)
(not (Flag303-))
(Flag303prime-)
(not (Flag304-))
(Flag304prime-)
(not (Flag305-))
(Flag305prime-)
(not (Flag306-))
(Flag306prime-)
(not (Flag323-))
(Flag323prime-)
(not (Flag307-))
(Flag307prime-)
(not (Flag324-))
(Flag324prime-)
)
:effect
(and
(Flag325prime-)
(not (Flag325-))
)

)
(:action Flag343Action-0
:parameters ()
:precondition
(and
(Flag273-)
(Flag273prime-)
)
:effect
(and
(Flag343-)
(Flag343prime-)
)

)
(:action Flag343Action-1
:parameters ()
:precondition
(and
(Flag274-)
(Flag274prime-)
)
:effect
(and
(Flag343-)
(Flag343prime-)
)

)
(:action Flag343Action-2
:parameters ()
:precondition
(and
(Flag275-)
(Flag275prime-)
)
:effect
(and
(Flag343-)
(Flag343prime-)
)

)
(:action Flag343Action-3
:parameters ()
:precondition
(and
(Flag309-)
(Flag309prime-)
)
:effect
(and
(Flag343-)
(Flag343prime-)
)

)
(:action Flag343Action-4
:parameters ()
:precondition
(and
(Flag276-)
(Flag276prime-)
)
:effect
(and
(Flag343-)
(Flag343prime-)
)

)
(:action Flag343Action-5
:parameters ()
:precondition
(and
(Flag277-)
(Flag277prime-)
)
:effect
(and
(Flag343-)
(Flag343prime-)
)

)
(:action Flag343Action-6
:parameters ()
:precondition
(and
(Flag310-)
(Flag310prime-)
)
:effect
(and
(Flag343-)
(Flag343prime-)
)

)
(:action Flag343Action-7
:parameters ()
:precondition
(and
(Flag304-)
(Flag304prime-)
)
:effect
(and
(Flag343-)
(Flag343prime-)
)

)
(:action Flag343Action-8
:parameters ()
:precondition
(and
(Flag326-)
(Flag326prime-)
)
:effect
(and
(Flag343-)
(Flag343prime-)
)

)
(:action Flag343Action-9
:parameters ()
:precondition
(and
(Flag311-)
(Flag311prime-)
)
:effect
(and
(Flag343-)
(Flag343prime-)
)

)
(:action Flag343Action-10
:parameters ()
:precondition
(and
(Flag278-)
(Flag278prime-)
)
:effect
(and
(Flag343-)
(Flag343prime-)
)

)
(:action Flag343Action-11
:parameters ()
:precondition
(and
(Flag279-)
(Flag279prime-)
)
:effect
(and
(Flag343-)
(Flag343prime-)
)

)
(:action Flag343Action-12
:parameters ()
:precondition
(and
(Flag327-)
(Flag327prime-)
)
:effect
(and
(Flag343-)
(Flag343prime-)
)

)
(:action Flag343Action-13
:parameters ()
:precondition
(and
(Flag328-)
(Flag328prime-)
)
:effect
(and
(Flag343-)
(Flag343prime-)
)

)
(:action Flag343Action-14
:parameters ()
:precondition
(and
(Flag312-)
(Flag312prime-)
)
:effect
(and
(Flag343-)
(Flag343prime-)
)

)
(:action Flag343Action-15
:parameters ()
:precondition
(and
(Flag329-)
(Flag329prime-)
)
:effect
(and
(Flag343-)
(Flag343prime-)
)

)
(:action Flag343Action-16
:parameters ()
:precondition
(and
(Flag281-)
(Flag281prime-)
)
:effect
(and
(Flag343-)
(Flag343prime-)
)

)
(:action Flag343Action-17
:parameters ()
:precondition
(and
(Flag316-)
(Flag316prime-)
)
:effect
(and
(Flag343-)
(Flag343prime-)
)

)
(:action Flag343Action-18
:parameters ()
:precondition
(and
(Flag293-)
(Flag293prime-)
)
:effect
(and
(Flag343-)
(Flag343prime-)
)

)
(:action Flag343Action-19
:parameters ()
:precondition
(and
(Flag284-)
(Flag284prime-)
)
:effect
(and
(Flag343-)
(Flag343prime-)
)

)
(:action Flag343Action-20
:parameters ()
:precondition
(and
(Flag285-)
(Flag285prime-)
)
:effect
(and
(Flag343-)
(Flag343prime-)
)

)
(:action Flag343Action-21
:parameters ()
:precondition
(and
(Flag330-)
(Flag330prime-)
)
:effect
(and
(Flag343-)
(Flag343prime-)
)

)
(:action Flag343Action-22
:parameters ()
:precondition
(and
(Flag313-)
(Flag313prime-)
)
:effect
(and
(Flag343-)
(Flag343prime-)
)

)
(:action Flag343Action-23
:parameters ()
:precondition
(and
(Flag286-)
(Flag286prime-)
)
:effect
(and
(Flag343-)
(Flag343prime-)
)

)
(:action Flag343Action-24
:parameters ()
:precondition
(and
(Flag287-)
(Flag287prime-)
)
:effect
(and
(Flag343-)
(Flag343prime-)
)

)
(:action Flag343Action-25
:parameters ()
:precondition
(and
(Flag295-)
(Flag295prime-)
)
:effect
(and
(Flag343-)
(Flag343prime-)
)

)
(:action Flag343Action-26
:parameters ()
:precondition
(and
(Flag288-)
(Flag288prime-)
)
:effect
(and
(Flag343-)
(Flag343prime-)
)

)
(:action Flag343Action-27
:parameters ()
:precondition
(and
(Flag314-)
(Flag314prime-)
)
:effect
(and
(Flag343-)
(Flag343prime-)
)

)
(:action Flag343Action-28
:parameters ()
:precondition
(and
(Flag289-)
(Flag289prime-)
)
:effect
(and
(Flag343-)
(Flag343prime-)
)

)
(:action Flag343Action-29
:parameters ()
:precondition
(and
(Flag290-)
(Flag290prime-)
)
:effect
(and
(Flag343-)
(Flag343prime-)
)

)
(:action Flag343Action-30
:parameters ()
:precondition
(and
(Flag331-)
(Flag331prime-)
)
:effect
(and
(Flag343-)
(Flag343prime-)
)

)
(:action Flag343Action-31
:parameters ()
:precondition
(and
(Flag291-)
(Flag291prime-)
)
:effect
(and
(Flag343-)
(Flag343prime-)
)

)
(:action Flag343Action-32
:parameters ()
:precondition
(and
(Flag292-)
(Flag292prime-)
)
:effect
(and
(Flag343-)
(Flag343prime-)
)

)
(:action Flag343Action-33
:parameters ()
:precondition
(and
(Flag315-)
(Flag315prime-)
)
:effect
(and
(Flag343-)
(Flag343prime-)
)

)
(:action Flag343Action-34
:parameters ()
:precondition
(and
(Flag332-)
(Flag332prime-)
)
:effect
(and
(Flag343-)
(Flag343prime-)
)

)
(:action Flag343Action-35
:parameters ()
:precondition
(and
(Flag283-)
(Flag283prime-)
)
:effect
(and
(Flag343-)
(Flag343prime-)
)

)
(:action Flag343Action-36
:parameters ()
:precondition
(and
(Flag294-)
(Flag294prime-)
)
:effect
(and
(Flag343-)
(Flag343prime-)
)

)
(:action Flag343Action-37
:parameters ()
:precondition
(and
(Flag333-)
(Flag333prime-)
)
:effect
(and
(Flag343-)
(Flag343prime-)
)

)
(:action Flag343Action-38
:parameters ()
:precondition
(and
(Flag305-)
(Flag305prime-)
)
:effect
(and
(Flag343-)
(Flag343prime-)
)

)
(:action Flag343Action-39
:parameters ()
:precondition
(and
(Flag317-)
(Flag317prime-)
)
:effect
(and
(Flag343-)
(Flag343prime-)
)

)
(:action Flag343Action-40
:parameters ()
:precondition
(and
(Flag318-)
(Flag318prime-)
)
:effect
(and
(Flag343-)
(Flag343prime-)
)

)
(:action Flag343Action-41
:parameters ()
:precondition
(and
(Flag296-)
(Flag296prime-)
)
:effect
(and
(Flag343-)
(Flag343prime-)
)

)
(:action Flag343Action-42
:parameters ()
:precondition
(and
(Flag297-)
(Flag297prime-)
)
:effect
(and
(Flag343-)
(Flag343prime-)
)

)
(:action Flag343Action-43
:parameters ()
:precondition
(and
(Flag320-)
(Flag320prime-)
)
:effect
(and
(Flag343-)
(Flag343prime-)
)

)
(:action Flag343Action-44
:parameters ()
:precondition
(and
(Flag334-)
(Flag334prime-)
)
:effect
(and
(Flag343-)
(Flag343prime-)
)

)
(:action Flag343Action-45
:parameters ()
:precondition
(and
(Flag335-)
(Flag335prime-)
)
:effect
(and
(Flag343-)
(Flag343prime-)
)

)
(:action Flag343Action-46
:parameters ()
:precondition
(and
(Flag321-)
(Flag321prime-)
)
:effect
(and
(Flag343-)
(Flag343prime-)
)

)
(:action Flag343Action-47
:parameters ()
:precondition
(and
(Flag322-)
(Flag322prime-)
)
:effect
(and
(Flag343-)
(Flag343prime-)
)

)
(:action Flag343Action-48
:parameters ()
:precondition
(and
(Flag336-)
(Flag336prime-)
)
:effect
(and
(Flag343-)
(Flag343prime-)
)

)
(:action Flag343Action-49
:parameters ()
:precondition
(and
(Flag337-)
(Flag337prime-)
)
:effect
(and
(Flag343-)
(Flag343prime-)
)

)
(:action Flag343Action-50
:parameters ()
:precondition
(and
(Flag298-)
(Flag298prime-)
)
:effect
(and
(Flag343-)
(Flag343prime-)
)

)
(:action Flag343Action-51
:parameters ()
:precondition
(and
(Flag299-)
(Flag299prime-)
)
:effect
(and
(Flag343-)
(Flag343prime-)
)

)
(:action Flag343Action-52
:parameters ()
:precondition
(and
(Flag338-)
(Flag338prime-)
)
:effect
(and
(Flag343-)
(Flag343prime-)
)

)
(:action Flag343Action-53
:parameters ()
:precondition
(and
(Flag339-)
(Flag339prime-)
)
:effect
(and
(Flag343-)
(Flag343prime-)
)

)
(:action Flag343Action-54
:parameters ()
:precondition
(and
(Flag300-)
(Flag300prime-)
)
:effect
(and
(Flag343-)
(Flag343prime-)
)

)
(:action Flag343Action-55
:parameters ()
:precondition
(and
(Flag340-)
(Flag340prime-)
)
:effect
(and
(Flag343-)
(Flag343prime-)
)

)
(:action Flag343Action-56
:parameters ()
:precondition
(and
(Flag303-)
(Flag303prime-)
)
:effect
(and
(Flag343-)
(Flag343prime-)
)

)
(:action Flag343Action-57
:parameters ()
:precondition
(and
(Flag341-)
(Flag341prime-)
)
:effect
(and
(Flag343-)
(Flag343prime-)
)

)
(:action Flag343Action-58
:parameters ()
:precondition
(and
(Flag342-)
(Flag342prime-)
)
:effect
(and
(Flag343-)
(Flag343prime-)
)

)
(:action Flag343Action-59
:parameters ()
:precondition
(and
(Flag306-)
(Flag306prime-)
)
:effect
(and
(Flag343-)
(Flag343prime-)
)

)
(:action Flag343Action-60
:parameters ()
:precondition
(and
(Flag323-)
(Flag323prime-)
)
:effect
(and
(Flag343-)
(Flag343prime-)
)

)
(:action Flag343Action-61
:parameters ()
:precondition
(and
(Flag307-)
(Flag307prime-)
)
:effect
(and
(Flag343-)
(Flag343prime-)
)

)
(:action Prim343Action
:parameters ()
:precondition
(and
(not (Flag273-))
(Flag273prime-)
(not (Flag274-))
(Flag274prime-)
(not (Flag275-))
(Flag275prime-)
(not (Flag309-))
(Flag309prime-)
(not (Flag276-))
(Flag276prime-)
(not (Flag277-))
(Flag277prime-)
(not (Flag310-))
(Flag310prime-)
(not (Flag304-))
(Flag304prime-)
(not (Flag326-))
(Flag326prime-)
(not (Flag311-))
(Flag311prime-)
(not (Flag278-))
(Flag278prime-)
(not (Flag279-))
(Flag279prime-)
(not (Flag327-))
(Flag327prime-)
(not (Flag328-))
(Flag328prime-)
(not (Flag312-))
(Flag312prime-)
(not (Flag329-))
(Flag329prime-)
(not (Flag281-))
(Flag281prime-)
(not (Flag316-))
(Flag316prime-)
(not (Flag293-))
(Flag293prime-)
(not (Flag284-))
(Flag284prime-)
(not (Flag285-))
(Flag285prime-)
(not (Flag330-))
(Flag330prime-)
(not (Flag313-))
(Flag313prime-)
(not (Flag286-))
(Flag286prime-)
(not (Flag287-))
(Flag287prime-)
(not (Flag295-))
(Flag295prime-)
(not (Flag288-))
(Flag288prime-)
(not (Flag314-))
(Flag314prime-)
(not (Flag289-))
(Flag289prime-)
(not (Flag290-))
(Flag290prime-)
(not (Flag331-))
(Flag331prime-)
(not (Flag291-))
(Flag291prime-)
(not (Flag292-))
(Flag292prime-)
(not (Flag315-))
(Flag315prime-)
(not (Flag332-))
(Flag332prime-)
(not (Flag283-))
(Flag283prime-)
(not (Flag294-))
(Flag294prime-)
(not (Flag333-))
(Flag333prime-)
(not (Flag305-))
(Flag305prime-)
(not (Flag317-))
(Flag317prime-)
(not (Flag318-))
(Flag318prime-)
(not (Flag296-))
(Flag296prime-)
(not (Flag297-))
(Flag297prime-)
(not (Flag320-))
(Flag320prime-)
(not (Flag334-))
(Flag334prime-)
(not (Flag335-))
(Flag335prime-)
(not (Flag321-))
(Flag321prime-)
(not (Flag322-))
(Flag322prime-)
(not (Flag336-))
(Flag336prime-)
(not (Flag337-))
(Flag337prime-)
(not (Flag298-))
(Flag298prime-)
(not (Flag299-))
(Flag299prime-)
(not (Flag338-))
(Flag338prime-)
(not (Flag339-))
(Flag339prime-)
(not (Flag300-))
(Flag300prime-)
(not (Flag340-))
(Flag340prime-)
(not (Flag303-))
(Flag303prime-)
(not (Flag341-))
(Flag341prime-)
(not (Flag342-))
(Flag342prime-)
(not (Flag306-))
(Flag306prime-)
(not (Flag323-))
(Flag323prime-)
(not (Flag307-))
(Flag307prime-)
)
:effect
(and
(Flag343prime-)
(not (Flag343-))
)

)
(:action Flag360Action-0
:parameters ()
:precondition
(and
(Flag344-)
(Flag344prime-)
)
:effect
(and
(Flag360-)
(Flag360prime-)
)

)
(:action Flag360Action-1
:parameters ()
:precondition
(and
(Flag345-)
(Flag345prime-)
)
:effect
(and
(Flag360-)
(Flag360prime-)
)

)
(:action Flag360Action-2
:parameters ()
:precondition
(and
(Flag274-)
(Flag274prime-)
)
:effect
(and
(Flag360-)
(Flag360prime-)
)

)
(:action Flag360Action-3
:parameters ()
:precondition
(and
(Flag346-)
(Flag346prime-)
)
:effect
(and
(Flag360-)
(Flag360prime-)
)

)
(:action Flag360Action-4
:parameters ()
:precondition
(and
(Flag275-)
(Flag275prime-)
)
:effect
(and
(Flag360-)
(Flag360prime-)
)

)
(:action Flag360Action-5
:parameters ()
:precondition
(and
(Flag309-)
(Flag309prime-)
)
:effect
(and
(Flag360-)
(Flag360prime-)
)

)
(:action Flag360Action-6
:parameters ()
:precondition
(and
(Flag276-)
(Flag276prime-)
)
:effect
(and
(Flag360-)
(Flag360prime-)
)

)
(:action Flag360Action-7
:parameters ()
:precondition
(and
(Flag310-)
(Flag310prime-)
)
:effect
(and
(Flag360-)
(Flag360prime-)
)

)
(:action Flag360Action-8
:parameters ()
:precondition
(and
(Flag304-)
(Flag304prime-)
)
:effect
(and
(Flag360-)
(Flag360prime-)
)

)
(:action Flag360Action-9
:parameters ()
:precondition
(and
(Flag326-)
(Flag326prime-)
)
:effect
(and
(Flag360-)
(Flag360prime-)
)

)
(:action Flag360Action-10
:parameters ()
:precondition
(and
(Flag347-)
(Flag347prime-)
)
:effect
(and
(Flag360-)
(Flag360prime-)
)

)
(:action Flag360Action-11
:parameters ()
:precondition
(and
(Flag311-)
(Flag311prime-)
)
:effect
(and
(Flag360-)
(Flag360prime-)
)

)
(:action Flag360Action-12
:parameters ()
:precondition
(and
(Flag278-)
(Flag278prime-)
)
:effect
(and
(Flag360-)
(Flag360prime-)
)

)
(:action Flag360Action-13
:parameters ()
:precondition
(and
(Flag279-)
(Flag279prime-)
)
:effect
(and
(Flag360-)
(Flag360prime-)
)

)
(:action Flag360Action-14
:parameters ()
:precondition
(and
(Flag296-)
(Flag296prime-)
)
:effect
(and
(Flag360-)
(Flag360prime-)
)

)
(:action Flag360Action-15
:parameters ()
:precondition
(and
(Flag348-)
(Flag348prime-)
)
:effect
(and
(Flag360-)
(Flag360prime-)
)

)
(:action Flag360Action-16
:parameters ()
:precondition
(and
(Flag327-)
(Flag327prime-)
)
:effect
(and
(Flag360-)
(Flag360prime-)
)

)
(:action Flag360Action-17
:parameters ()
:precondition
(and
(Flag328-)
(Flag328prime-)
)
:effect
(and
(Flag360-)
(Flag360prime-)
)

)
(:action Flag360Action-18
:parameters ()
:precondition
(and
(Flag349-)
(Flag349prime-)
)
:effect
(and
(Flag360-)
(Flag360prime-)
)

)
(:action Flag360Action-19
:parameters ()
:precondition
(and
(Flag312-)
(Flag312prime-)
)
:effect
(and
(Flag360-)
(Flag360prime-)
)

)
(:action Flag360Action-20
:parameters ()
:precondition
(and
(Flag273-)
(Flag273prime-)
)
:effect
(and
(Flag360-)
(Flag360prime-)
)

)
(:action Flag360Action-21
:parameters ()
:precondition
(and
(Flag329-)
(Flag329prime-)
)
:effect
(and
(Flag360-)
(Flag360prime-)
)

)
(:action Flag360Action-22
:parameters ()
:precondition
(and
(Flag281-)
(Flag281prime-)
)
:effect
(and
(Flag360-)
(Flag360prime-)
)

)
(:action Flag360Action-23
:parameters ()
:precondition
(and
(Flag316-)
(Flag316prime-)
)
:effect
(and
(Flag360-)
(Flag360prime-)
)

)
(:action Flag360Action-24
:parameters ()
:precondition
(and
(Flag293-)
(Flag293prime-)
)
:effect
(and
(Flag360-)
(Flag360prime-)
)

)
(:action Flag360Action-25
:parameters ()
:precondition
(and
(Flag284-)
(Flag284prime-)
)
:effect
(and
(Flag360-)
(Flag360prime-)
)

)
(:action Flag360Action-26
:parameters ()
:precondition
(and
(Flag285-)
(Flag285prime-)
)
:effect
(and
(Flag360-)
(Flag360prime-)
)

)
(:action Flag360Action-27
:parameters ()
:precondition
(and
(Flag330-)
(Flag330prime-)
)
:effect
(and
(Flag360-)
(Flag360prime-)
)

)
(:action Flag360Action-28
:parameters ()
:precondition
(and
(Flag313-)
(Flag313prime-)
)
:effect
(and
(Flag360-)
(Flag360prime-)
)

)
(:action Flag360Action-29
:parameters ()
:precondition
(and
(Flag286-)
(Flag286prime-)
)
:effect
(and
(Flag360-)
(Flag360prime-)
)

)
(:action Flag360Action-30
:parameters ()
:precondition
(and
(Flag331-)
(Flag331prime-)
)
:effect
(and
(Flag360-)
(Flag360prime-)
)

)
(:action Flag360Action-31
:parameters ()
:precondition
(and
(Flag288-)
(Flag288prime-)
)
:effect
(and
(Flag360-)
(Flag360prime-)
)

)
(:action Flag360Action-32
:parameters ()
:precondition
(and
(Flag318-)
(Flag318prime-)
)
:effect
(and
(Flag360-)
(Flag360prime-)
)

)
(:action Flag360Action-33
:parameters ()
:precondition
(and
(Flag350-)
(Flag350prime-)
)
:effect
(and
(Flag360-)
(Flag360prime-)
)

)
(:action Flag360Action-34
:parameters ()
:precondition
(and
(Flag351-)
(Flag351prime-)
)
:effect
(and
(Flag360-)
(Flag360prime-)
)

)
(:action Flag360Action-35
:parameters ()
:precondition
(and
(Flag352-)
(Flag352prime-)
)
:effect
(and
(Flag360-)
(Flag360prime-)
)

)
(:action Flag360Action-36
:parameters ()
:precondition
(and
(Flag290-)
(Flag290prime-)
)
:effect
(and
(Flag360-)
(Flag360prime-)
)

)
(:action Flag360Action-37
:parameters ()
:precondition
(and
(Flag353-)
(Flag353prime-)
)
:effect
(and
(Flag360-)
(Flag360prime-)
)

)
(:action Flag360Action-38
:parameters ()
:precondition
(and
(Flag291-)
(Flag291prime-)
)
:effect
(and
(Flag360-)
(Flag360prime-)
)

)
(:action Flag360Action-39
:parameters ()
:precondition
(and
(Flag292-)
(Flag292prime-)
)
:effect
(and
(Flag360-)
(Flag360prime-)
)

)
(:action Flag360Action-40
:parameters ()
:precondition
(and
(Flag315-)
(Flag315prime-)
)
:effect
(and
(Flag360-)
(Flag360prime-)
)

)
(:action Flag360Action-41
:parameters ()
:precondition
(and
(Flag354-)
(Flag354prime-)
)
:effect
(and
(Flag360-)
(Flag360prime-)
)

)
(:action Flag360Action-42
:parameters ()
:precondition
(and
(Flag283-)
(Flag283prime-)
)
:effect
(and
(Flag360-)
(Flag360prime-)
)

)
(:action Flag360Action-43
:parameters ()
:precondition
(and
(Flag294-)
(Flag294prime-)
)
:effect
(and
(Flag360-)
(Flag360prime-)
)

)
(:action Flag360Action-44
:parameters ()
:precondition
(and
(Flag333-)
(Flag333prime-)
)
:effect
(and
(Flag360-)
(Flag360prime-)
)

)
(:action Flag360Action-45
:parameters ()
:precondition
(and
(Flag355-)
(Flag355prime-)
)
:effect
(and
(Flag360-)
(Flag360prime-)
)

)
(:action Flag360Action-46
:parameters ()
:precondition
(and
(Flag356-)
(Flag356prime-)
)
:effect
(and
(Flag360-)
(Flag360prime-)
)

)
(:action Flag360Action-47
:parameters ()
:precondition
(and
(Flag305-)
(Flag305prime-)
)
:effect
(and
(Flag360-)
(Flag360prime-)
)

)
(:action Flag360Action-48
:parameters ()
:precondition
(and
(Flag295-)
(Flag295prime-)
)
:effect
(and
(Flag360-)
(Flag360prime-)
)

)
(:action Flag360Action-49
:parameters ()
:precondition
(and
(Flag314-)
(Flag314prime-)
)
:effect
(and
(Flag360-)
(Flag360prime-)
)

)
(:action Flag360Action-50
:parameters ()
:precondition
(and
(Flag289-)
(Flag289prime-)
)
:effect
(and
(Flag360-)
(Flag360prime-)
)

)
(:action Flag360Action-51
:parameters ()
:precondition
(and
(Flag297-)
(Flag297prime-)
)
:effect
(and
(Flag360-)
(Flag360prime-)
)

)
(:action Flag360Action-52
:parameters ()
:precondition
(and
(Flag320-)
(Flag320prime-)
)
:effect
(and
(Flag360-)
(Flag360prime-)
)

)
(:action Flag360Action-53
:parameters ()
:precondition
(and
(Flag334-)
(Flag334prime-)
)
:effect
(and
(Flag360-)
(Flag360prime-)
)

)
(:action Flag360Action-54
:parameters ()
:precondition
(and
(Flag335-)
(Flag335prime-)
)
:effect
(and
(Flag360-)
(Flag360prime-)
)

)
(:action Flag360Action-55
:parameters ()
:precondition
(and
(Flag321-)
(Flag321prime-)
)
:effect
(and
(Flag360-)
(Flag360prime-)
)

)
(:action Flag360Action-56
:parameters ()
:precondition
(and
(Flag322-)
(Flag322prime-)
)
:effect
(and
(Flag360-)
(Flag360prime-)
)

)
(:action Flag360Action-57
:parameters ()
:precondition
(and
(Flag336-)
(Flag336prime-)
)
:effect
(and
(Flag360-)
(Flag360prime-)
)

)
(:action Flag360Action-58
:parameters ()
:precondition
(and
(Flag337-)
(Flag337prime-)
)
:effect
(and
(Flag360-)
(Flag360prime-)
)

)
(:action Flag360Action-59
:parameters ()
:precondition
(and
(Flag298-)
(Flag298prime-)
)
:effect
(and
(Flag360-)
(Flag360prime-)
)

)
(:action Flag360Action-60
:parameters ()
:precondition
(and
(Flag299-)
(Flag299prime-)
)
:effect
(and
(Flag360-)
(Flag360prime-)
)

)
(:action Flag360Action-61
:parameters ()
:precondition
(and
(Flag338-)
(Flag338prime-)
)
:effect
(and
(Flag360-)
(Flag360prime-)
)

)
(:action Flag360Action-62
:parameters ()
:precondition
(and
(Flag339-)
(Flag339prime-)
)
:effect
(and
(Flag360-)
(Flag360prime-)
)

)
(:action Flag360Action-63
:parameters ()
:precondition
(and
(Flag300-)
(Flag300prime-)
)
:effect
(and
(Flag360-)
(Flag360prime-)
)

)
(:action Flag360Action-64
:parameters ()
:precondition
(and
(Flag340-)
(Flag340prime-)
)
:effect
(and
(Flag360-)
(Flag360prime-)
)

)
(:action Flag360Action-65
:parameters ()
:precondition
(and
(Flag357-)
(Flag357prime-)
)
:effect
(and
(Flag360-)
(Flag360prime-)
)

)
(:action Flag360Action-66
:parameters ()
:precondition
(and
(Flag317-)
(Flag317prime-)
)
:effect
(and
(Flag360-)
(Flag360prime-)
)

)
(:action Flag360Action-67
:parameters ()
:precondition
(and
(Flag303-)
(Flag303prime-)
)
:effect
(and
(Flag360-)
(Flag360prime-)
)

)
(:action Flag360Action-68
:parameters ()
:precondition
(and
(Flag358-)
(Flag358prime-)
)
:effect
(and
(Flag360-)
(Flag360prime-)
)

)
(:action Flag360Action-69
:parameters ()
:precondition
(and
(Flag342-)
(Flag342prime-)
)
:effect
(and
(Flag360-)
(Flag360prime-)
)

)
(:action Flag360Action-70
:parameters ()
:precondition
(and
(Flag306-)
(Flag306prime-)
)
:effect
(and
(Flag360-)
(Flag360prime-)
)

)
(:action Flag360Action-71
:parameters ()
:precondition
(and
(Flag323-)
(Flag323prime-)
)
:effect
(and
(Flag360-)
(Flag360prime-)
)

)
(:action Flag360Action-72
:parameters ()
:precondition
(and
(Flag307-)
(Flag307prime-)
)
:effect
(and
(Flag360-)
(Flag360prime-)
)

)
(:action Flag360Action-73
:parameters ()
:precondition
(and
(Flag359-)
(Flag359prime-)
)
:effect
(and
(Flag360-)
(Flag360prime-)
)

)
(:action Prim360Action
:parameters ()
:precondition
(and
(not (Flag344-))
(Flag344prime-)
(not (Flag345-))
(Flag345prime-)
(not (Flag274-))
(Flag274prime-)
(not (Flag346-))
(Flag346prime-)
(not (Flag275-))
(Flag275prime-)
(not (Flag309-))
(Flag309prime-)
(not (Flag276-))
(Flag276prime-)
(not (Flag310-))
(Flag310prime-)
(not (Flag304-))
(Flag304prime-)
(not (Flag326-))
(Flag326prime-)
(not (Flag347-))
(Flag347prime-)
(not (Flag311-))
(Flag311prime-)
(not (Flag278-))
(Flag278prime-)
(not (Flag279-))
(Flag279prime-)
(not (Flag296-))
(Flag296prime-)
(not (Flag348-))
(Flag348prime-)
(not (Flag327-))
(Flag327prime-)
(not (Flag328-))
(Flag328prime-)
(not (Flag349-))
(Flag349prime-)
(not (Flag312-))
(Flag312prime-)
(not (Flag273-))
(Flag273prime-)
(not (Flag329-))
(Flag329prime-)
(not (Flag281-))
(Flag281prime-)
(not (Flag316-))
(Flag316prime-)
(not (Flag293-))
(Flag293prime-)
(not (Flag284-))
(Flag284prime-)
(not (Flag285-))
(Flag285prime-)
(not (Flag330-))
(Flag330prime-)
(not (Flag313-))
(Flag313prime-)
(not (Flag286-))
(Flag286prime-)
(not (Flag331-))
(Flag331prime-)
(not (Flag288-))
(Flag288prime-)
(not (Flag318-))
(Flag318prime-)
(not (Flag350-))
(Flag350prime-)
(not (Flag351-))
(Flag351prime-)
(not (Flag352-))
(Flag352prime-)
(not (Flag290-))
(Flag290prime-)
(not (Flag353-))
(Flag353prime-)
(not (Flag291-))
(Flag291prime-)
(not (Flag292-))
(Flag292prime-)
(not (Flag315-))
(Flag315prime-)
(not (Flag354-))
(Flag354prime-)
(not (Flag283-))
(Flag283prime-)
(not (Flag294-))
(Flag294prime-)
(not (Flag333-))
(Flag333prime-)
(not (Flag355-))
(Flag355prime-)
(not (Flag356-))
(Flag356prime-)
(not (Flag305-))
(Flag305prime-)
(not (Flag295-))
(Flag295prime-)
(not (Flag314-))
(Flag314prime-)
(not (Flag289-))
(Flag289prime-)
(not (Flag297-))
(Flag297prime-)
(not (Flag320-))
(Flag320prime-)
(not (Flag334-))
(Flag334prime-)
(not (Flag335-))
(Flag335prime-)
(not (Flag321-))
(Flag321prime-)
(not (Flag322-))
(Flag322prime-)
(not (Flag336-))
(Flag336prime-)
(not (Flag337-))
(Flag337prime-)
(not (Flag298-))
(Flag298prime-)
(not (Flag299-))
(Flag299prime-)
(not (Flag338-))
(Flag338prime-)
(not (Flag339-))
(Flag339prime-)
(not (Flag300-))
(Flag300prime-)
(not (Flag340-))
(Flag340prime-)
(not (Flag357-))
(Flag357prime-)
(not (Flag317-))
(Flag317prime-)
(not (Flag303-))
(Flag303prime-)
(not (Flag358-))
(Flag358prime-)
(not (Flag342-))
(Flag342prime-)
(not (Flag306-))
(Flag306prime-)
(not (Flag323-))
(Flag323prime-)
(not (Flag307-))
(Flag307prime-)
(not (Flag359-))
(Flag359prime-)
)
:effect
(and
(Flag360prime-)
(not (Flag360-))
)

)
(:action Flag374Action-0
:parameters ()
:precondition
(and
(Flag299-)
(Flag299prime-)
)
:effect
(and
(Flag374-)
(Flag374prime-)
)

)
(:action Flag374Action-1
:parameters ()
:precondition
(and
(Flag345-)
(Flag345prime-)
)
:effect
(and
(Flag374-)
(Flag374prime-)
)

)
(:action Flag374Action-2
:parameters ()
:precondition
(and
(Flag274-)
(Flag274prime-)
)
:effect
(and
(Flag374-)
(Flag374prime-)
)

)
(:action Flag374Action-3
:parameters ()
:precondition
(and
(Flag344-)
(Flag344prime-)
)
:effect
(and
(Flag374-)
(Flag374prime-)
)

)
(:action Flag374Action-4
:parameters ()
:precondition
(and
(Flag275-)
(Flag275prime-)
)
:effect
(and
(Flag374-)
(Flag374prime-)
)

)
(:action Flag374Action-5
:parameters ()
:precondition
(and
(Flag347-)
(Flag347prime-)
)
:effect
(and
(Flag374-)
(Flag374prime-)
)

)
(:action Flag374Action-6
:parameters ()
:precondition
(and
(Flag276-)
(Flag276prime-)
)
:effect
(and
(Flag374-)
(Flag374prime-)
)

)
(:action Flag374Action-7
:parameters ()
:precondition
(and
(Flag310-)
(Flag310prime-)
)
:effect
(and
(Flag374-)
(Flag374prime-)
)

)
(:action Flag374Action-8
:parameters ()
:precondition
(and
(Flag304-)
(Flag304prime-)
)
:effect
(and
(Flag374-)
(Flag374prime-)
)

)
(:action Flag374Action-9
:parameters ()
:precondition
(and
(Flag361-)
(Flag361prime-)
)
:effect
(and
(Flag374-)
(Flag374prime-)
)

)
(:action Flag374Action-10
:parameters ()
:precondition
(and
(Flag326-)
(Flag326prime-)
)
:effect
(and
(Flag374-)
(Flag374prime-)
)

)
(:action Flag374Action-11
:parameters ()
:precondition
(and
(Flag327-)
(Flag327prime-)
)
:effect
(and
(Flag374-)
(Flag374prime-)
)

)
(:action Flag374Action-12
:parameters ()
:precondition
(and
(Flag323-)
(Flag323prime-)
)
:effect
(and
(Flag374-)
(Flag374prime-)
)

)
(:action Flag374Action-13
:parameters ()
:precondition
(and
(Flag278-)
(Flag278prime-)
)
:effect
(and
(Flag374-)
(Flag374prime-)
)

)
(:action Flag374Action-14
:parameters ()
:precondition
(and
(Flag279-)
(Flag279prime-)
)
:effect
(and
(Flag374-)
(Flag374prime-)
)

)
(:action Flag374Action-15
:parameters ()
:precondition
(and
(Flag349-)
(Flag349prime-)
)
:effect
(and
(Flag374-)
(Flag374prime-)
)

)
(:action Flag374Action-16
:parameters ()
:precondition
(and
(Flag312-)
(Flag312prime-)
)
:effect
(and
(Flag374-)
(Flag374prime-)
)

)
(:action Flag374Action-17
:parameters ()
:precondition
(and
(Flag273-)
(Flag273prime-)
)
:effect
(and
(Flag374-)
(Flag374prime-)
)

)
(:action Flag374Action-18
:parameters ()
:precondition
(and
(Flag329-)
(Flag329prime-)
)
:effect
(and
(Flag374-)
(Flag374prime-)
)

)
(:action Flag374Action-19
:parameters ()
:precondition
(and
(Flag281-)
(Flag281prime-)
)
:effect
(and
(Flag374-)
(Flag374prime-)
)

)
(:action Flag374Action-20
:parameters ()
:precondition
(and
(Flag350-)
(Flag350prime-)
)
:effect
(and
(Flag374-)
(Flag374prime-)
)

)
(:action Flag374Action-21
:parameters ()
:precondition
(and
(Flag316-)
(Flag316prime-)
)
:effect
(and
(Flag374-)
(Flag374prime-)
)

)
(:action Flag374Action-22
:parameters ()
:precondition
(and
(Flag362-)
(Flag362prime-)
)
:effect
(and
(Flag374-)
(Flag374prime-)
)

)
(:action Flag374Action-23
:parameters ()
:precondition
(and
(Flag283-)
(Flag283prime-)
)
:effect
(and
(Flag374-)
(Flag374prime-)
)

)
(:action Flag374Action-24
:parameters ()
:precondition
(and
(Flag284-)
(Flag284prime-)
)
:effect
(and
(Flag374-)
(Flag374prime-)
)

)
(:action Flag374Action-25
:parameters ()
:precondition
(and
(Flag340-)
(Flag340prime-)
)
:effect
(and
(Flag374-)
(Flag374prime-)
)

)
(:action Flag374Action-26
:parameters ()
:precondition
(and
(Flag363-)
(Flag363prime-)
)
:effect
(and
(Flag374-)
(Flag374prime-)
)

)
(:action Flag374Action-27
:parameters ()
:precondition
(and
(Flag330-)
(Flag330prime-)
)
:effect
(and
(Flag374-)
(Flag374prime-)
)

)
(:action Flag374Action-28
:parameters ()
:precondition
(and
(Flag313-)
(Flag313prime-)
)
:effect
(and
(Flag374-)
(Flag374prime-)
)

)
(:action Flag374Action-29
:parameters ()
:precondition
(and
(Flag286-)
(Flag286prime-)
)
:effect
(and
(Flag374-)
(Flag374prime-)
)

)
(:action Flag374Action-30
:parameters ()
:precondition
(and
(Flag331-)
(Flag331prime-)
)
:effect
(and
(Flag374-)
(Flag374prime-)
)

)
(:action Flag374Action-31
:parameters ()
:precondition
(and
(Flag364-)
(Flag364prime-)
)
:effect
(and
(Flag374-)
(Flag374prime-)
)

)
(:action Flag374Action-32
:parameters ()
:precondition
(and
(Flag288-)
(Flag288prime-)
)
:effect
(and
(Flag374-)
(Flag374prime-)
)

)
(:action Flag374Action-33
:parameters ()
:precondition
(and
(Flag314-)
(Flag314prime-)
)
:effect
(and
(Flag374-)
(Flag374prime-)
)

)
(:action Flag374Action-34
:parameters ()
:precondition
(and
(Flag365-)
(Flag365prime-)
)
:effect
(and
(Flag374-)
(Flag374prime-)
)

)
(:action Flag374Action-35
:parameters ()
:precondition
(and
(Flag351-)
(Flag351prime-)
)
:effect
(and
(Flag374-)
(Flag374prime-)
)

)
(:action Flag374Action-36
:parameters ()
:precondition
(and
(Flag366-)
(Flag366prime-)
)
:effect
(and
(Flag374-)
(Flag374prime-)
)

)
(:action Flag374Action-37
:parameters ()
:precondition
(and
(Flag309-)
(Flag309prime-)
)
:effect
(and
(Flag374-)
(Flag374prime-)
)

)
(:action Flag374Action-38
:parameters ()
:precondition
(and
(Flag285-)
(Flag285prime-)
)
:effect
(and
(Flag374-)
(Flag374prime-)
)

)
(:action Flag374Action-39
:parameters ()
:precondition
(and
(Flag352-)
(Flag352prime-)
)
:effect
(and
(Flag374-)
(Flag374prime-)
)

)
(:action Flag374Action-40
:parameters ()
:precondition
(and
(Flag353-)
(Flag353prime-)
)
:effect
(and
(Flag374-)
(Flag374prime-)
)

)
(:action Flag374Action-41
:parameters ()
:precondition
(and
(Flag291-)
(Flag291prime-)
)
:effect
(and
(Flag374-)
(Flag374prime-)
)

)
(:action Flag374Action-42
:parameters ()
:precondition
(and
(Flag292-)
(Flag292prime-)
)
:effect
(and
(Flag374-)
(Flag374prime-)
)

)
(:action Flag374Action-43
:parameters ()
:precondition
(and
(Flag315-)
(Flag315prime-)
)
:effect
(and
(Flag374-)
(Flag374prime-)
)

)
(:action Flag374Action-44
:parameters ()
:precondition
(and
(Flag354-)
(Flag354prime-)
)
:effect
(and
(Flag374-)
(Flag374prime-)
)

)
(:action Flag374Action-45
:parameters ()
:precondition
(and
(Flag293-)
(Flag293prime-)
)
:effect
(and
(Flag374-)
(Flag374prime-)
)

)
(:action Flag374Action-46
:parameters ()
:precondition
(and
(Flag294-)
(Flag294prime-)
)
:effect
(and
(Flag374-)
(Flag374prime-)
)

)
(:action Flag374Action-47
:parameters ()
:precondition
(and
(Flag333-)
(Flag333prime-)
)
:effect
(and
(Flag374-)
(Flag374prime-)
)

)
(:action Flag374Action-48
:parameters ()
:precondition
(and
(Flag355-)
(Flag355prime-)
)
:effect
(and
(Flag374-)
(Flag374prime-)
)

)
(:action Flag374Action-49
:parameters ()
:precondition
(and
(Flag356-)
(Flag356prime-)
)
:effect
(and
(Flag374-)
(Flag374prime-)
)

)
(:action Flag374Action-50
:parameters ()
:precondition
(and
(Flag289-)
(Flag289prime-)
)
:effect
(and
(Flag374-)
(Flag374prime-)
)

)
(:action Flag374Action-51
:parameters ()
:precondition
(and
(Flag305-)
(Flag305prime-)
)
:effect
(and
(Flag374-)
(Flag374prime-)
)

)
(:action Flag374Action-52
:parameters ()
:precondition
(and
(Flag317-)
(Flag317prime-)
)
:effect
(and
(Flag374-)
(Flag374prime-)
)

)
(:action Flag374Action-53
:parameters ()
:precondition
(and
(Flag367-)
(Flag367prime-)
)
:effect
(and
(Flag374-)
(Flag374prime-)
)

)
(:action Flag374Action-54
:parameters ()
:precondition
(and
(Flag318-)
(Flag318prime-)
)
:effect
(and
(Flag374-)
(Flag374prime-)
)

)
(:action Flag374Action-55
:parameters ()
:precondition
(and
(Flag296-)
(Flag296prime-)
)
:effect
(and
(Flag374-)
(Flag374prime-)
)

)
(:action Flag374Action-56
:parameters ()
:precondition
(and
(Flag297-)
(Flag297prime-)
)
:effect
(and
(Flag374-)
(Flag374prime-)
)

)
(:action Flag374Action-57
:parameters ()
:precondition
(and
(Flag320-)
(Flag320prime-)
)
:effect
(and
(Flag374-)
(Flag374prime-)
)

)
(:action Flag374Action-58
:parameters ()
:precondition
(and
(Flag334-)
(Flag334prime-)
)
:effect
(and
(Flag374-)
(Flag374prime-)
)

)
(:action Flag374Action-59
:parameters ()
:precondition
(and
(Flag335-)
(Flag335prime-)
)
:effect
(and
(Flag374-)
(Flag374prime-)
)

)
(:action Flag374Action-60
:parameters ()
:precondition
(and
(Flag368-)
(Flag368prime-)
)
:effect
(and
(Flag374-)
(Flag374prime-)
)

)
(:action Flag374Action-61
:parameters ()
:precondition
(and
(Flag321-)
(Flag321prime-)
)
:effect
(and
(Flag374-)
(Flag374prime-)
)

)
(:action Flag374Action-62
:parameters ()
:precondition
(and
(Flag369-)
(Flag369prime-)
)
:effect
(and
(Flag374-)
(Flag374prime-)
)

)
(:action Flag374Action-63
:parameters ()
:precondition
(and
(Flag322-)
(Flag322prime-)
)
:effect
(and
(Flag374-)
(Flag374prime-)
)

)
(:action Flag374Action-64
:parameters ()
:precondition
(and
(Flag370-)
(Flag370prime-)
)
:effect
(and
(Flag374-)
(Flag374prime-)
)

)
(:action Flag374Action-65
:parameters ()
:precondition
(and
(Flag336-)
(Flag336prime-)
)
:effect
(and
(Flag374-)
(Flag374prime-)
)

)
(:action Flag374Action-66
:parameters ()
:precondition
(and
(Flag337-)
(Flag337prime-)
)
:effect
(and
(Flag374-)
(Flag374prime-)
)

)
(:action Flag374Action-67
:parameters ()
:precondition
(and
(Flag298-)
(Flag298prime-)
)
:effect
(and
(Flag374-)
(Flag374prime-)
)

)
(:action Flag374Action-68
:parameters ()
:precondition
(and
(Flag371-)
(Flag371prime-)
)
:effect
(and
(Flag374-)
(Flag374prime-)
)

)
(:action Flag374Action-69
:parameters ()
:precondition
(and
(Flag338-)
(Flag338prime-)
)
:effect
(and
(Flag374-)
(Flag374prime-)
)

)
(:action Flag374Action-70
:parameters ()
:precondition
(and
(Flag339-)
(Flag339prime-)
)
:effect
(and
(Flag374-)
(Flag374prime-)
)

)
(:action Flag374Action-71
:parameters ()
:precondition
(and
(Flag300-)
(Flag300prime-)
)
:effect
(and
(Flag374-)
(Flag374prime-)
)

)
(:action Flag374Action-72
:parameters ()
:precondition
(and
(Flag372-)
(Flag372prime-)
)
:effect
(and
(Flag374-)
(Flag374prime-)
)

)
(:action Flag374Action-73
:parameters ()
:precondition
(and
(Flag357-)
(Flag357prime-)
)
:effect
(and
(Flag374-)
(Flag374prime-)
)

)
(:action Flag374Action-74
:parameters ()
:precondition
(and
(Flag303-)
(Flag303prime-)
)
:effect
(and
(Flag374-)
(Flag374prime-)
)

)
(:action Flag374Action-75
:parameters ()
:precondition
(and
(Flag358-)
(Flag358prime-)
)
:effect
(and
(Flag374-)
(Flag374prime-)
)

)
(:action Flag374Action-76
:parameters ()
:precondition
(and
(Flag342-)
(Flag342prime-)
)
:effect
(and
(Flag374-)
(Flag374prime-)
)

)
(:action Flag374Action-77
:parameters ()
:precondition
(and
(Flag306-)
(Flag306prime-)
)
:effect
(and
(Flag374-)
(Flag374prime-)
)

)
(:action Flag374Action-78
:parameters ()
:precondition
(and
(Flag348-)
(Flag348prime-)
)
:effect
(and
(Flag374-)
(Flag374prime-)
)

)
(:action Flag374Action-79
:parameters ()
:precondition
(and
(Flag373-)
(Flag373prime-)
)
:effect
(and
(Flag374-)
(Flag374prime-)
)

)
(:action Flag374Action-80
:parameters ()
:precondition
(and
(Flag359-)
(Flag359prime-)
)
:effect
(and
(Flag374-)
(Flag374prime-)
)

)
(:action Prim374Action
:parameters ()
:precondition
(and
(not (Flag299-))
(Flag299prime-)
(not (Flag345-))
(Flag345prime-)
(not (Flag274-))
(Flag274prime-)
(not (Flag344-))
(Flag344prime-)
(not (Flag275-))
(Flag275prime-)
(not (Flag347-))
(Flag347prime-)
(not (Flag276-))
(Flag276prime-)
(not (Flag310-))
(Flag310prime-)
(not (Flag304-))
(Flag304prime-)
(not (Flag361-))
(Flag361prime-)
(not (Flag326-))
(Flag326prime-)
(not (Flag327-))
(Flag327prime-)
(not (Flag323-))
(Flag323prime-)
(not (Flag278-))
(Flag278prime-)
(not (Flag279-))
(Flag279prime-)
(not (Flag349-))
(Flag349prime-)
(not (Flag312-))
(Flag312prime-)
(not (Flag273-))
(Flag273prime-)
(not (Flag329-))
(Flag329prime-)
(not (Flag281-))
(Flag281prime-)
(not (Flag350-))
(Flag350prime-)
(not (Flag316-))
(Flag316prime-)
(not (Flag362-))
(Flag362prime-)
(not (Flag283-))
(Flag283prime-)
(not (Flag284-))
(Flag284prime-)
(not (Flag340-))
(Flag340prime-)
(not (Flag363-))
(Flag363prime-)
(not (Flag330-))
(Flag330prime-)
(not (Flag313-))
(Flag313prime-)
(not (Flag286-))
(Flag286prime-)
(not (Flag331-))
(Flag331prime-)
(not (Flag364-))
(Flag364prime-)
(not (Flag288-))
(Flag288prime-)
(not (Flag314-))
(Flag314prime-)
(not (Flag365-))
(Flag365prime-)
(not (Flag351-))
(Flag351prime-)
(not (Flag366-))
(Flag366prime-)
(not (Flag309-))
(Flag309prime-)
(not (Flag285-))
(Flag285prime-)
(not (Flag352-))
(Flag352prime-)
(not (Flag353-))
(Flag353prime-)
(not (Flag291-))
(Flag291prime-)
(not (Flag292-))
(Flag292prime-)
(not (Flag315-))
(Flag315prime-)
(not (Flag354-))
(Flag354prime-)
(not (Flag293-))
(Flag293prime-)
(not (Flag294-))
(Flag294prime-)
(not (Flag333-))
(Flag333prime-)
(not (Flag355-))
(Flag355prime-)
(not (Flag356-))
(Flag356prime-)
(not (Flag289-))
(Flag289prime-)
(not (Flag305-))
(Flag305prime-)
(not (Flag317-))
(Flag317prime-)
(not (Flag367-))
(Flag367prime-)
(not (Flag318-))
(Flag318prime-)
(not (Flag296-))
(Flag296prime-)
(not (Flag297-))
(Flag297prime-)
(not (Flag320-))
(Flag320prime-)
(not (Flag334-))
(Flag334prime-)
(not (Flag335-))
(Flag335prime-)
(not (Flag368-))
(Flag368prime-)
(not (Flag321-))
(Flag321prime-)
(not (Flag369-))
(Flag369prime-)
(not (Flag322-))
(Flag322prime-)
(not (Flag370-))
(Flag370prime-)
(not (Flag336-))
(Flag336prime-)
(not (Flag337-))
(Flag337prime-)
(not (Flag298-))
(Flag298prime-)
(not (Flag371-))
(Flag371prime-)
(not (Flag338-))
(Flag338prime-)
(not (Flag339-))
(Flag339prime-)
(not (Flag300-))
(Flag300prime-)
(not (Flag372-))
(Flag372prime-)
(not (Flag357-))
(Flag357prime-)
(not (Flag303-))
(Flag303prime-)
(not (Flag358-))
(Flag358prime-)
(not (Flag342-))
(Flag342prime-)
(not (Flag306-))
(Flag306prime-)
(not (Flag348-))
(Flag348prime-)
(not (Flag373-))
(Flag373prime-)
(not (Flag359-))
(Flag359prime-)
)
:effect
(and
(Flag374prime-)
(not (Flag374-))
)

)
(:action Flag389Action-0
:parameters ()
:precondition
(and
(Flag278-)
(Flag278prime-)
)
:effect
(and
(Flag389-)
(Flag389prime-)
)

)
(:action Flag389Action-1
:parameters ()
:precondition
(and
(Flag375-)
(Flag375prime-)
)
:effect
(and
(Flag389-)
(Flag389prime-)
)

)
(:action Flag389Action-2
:parameters ()
:precondition
(and
(Flag362-)
(Flag362prime-)
)
:effect
(and
(Flag389-)
(Flag389prime-)
)

)
(:action Flag389Action-3
:parameters ()
:precondition
(and
(Flag376-)
(Flag376prime-)
)
:effect
(and
(Flag389-)
(Flag389prime-)
)

)
(:action Flag389Action-4
:parameters ()
:precondition
(and
(Flag365-)
(Flag365prime-)
)
:effect
(and
(Flag389-)
(Flag389prime-)
)

)
(:action Flag389Action-5
:parameters ()
:precondition
(and
(Flag351-)
(Flag351prime-)
)
:effect
(and
(Flag389-)
(Flag389prime-)
)

)
(:action Flag389Action-6
:parameters ()
:precondition
(and
(Flag352-)
(Flag352prime-)
)
:effect
(and
(Flag389-)
(Flag389prime-)
)

)
(:action Flag389Action-7
:parameters ()
:precondition
(and
(Flag353-)
(Flag353prime-)
)
:effect
(and
(Flag389-)
(Flag389prime-)
)

)
(:action Flag389Action-8
:parameters ()
:precondition
(and
(Flag315-)
(Flag315prime-)
)
:effect
(and
(Flag389-)
(Flag389prime-)
)

)
(:action Flag389Action-9
:parameters ()
:precondition
(and
(Flag316-)
(Flag316prime-)
)
:effect
(and
(Flag389-)
(Flag389prime-)
)

)
(:action Flag389Action-10
:parameters ()
:precondition
(and
(Flag294-)
(Flag294prime-)
)
:effect
(and
(Flag389-)
(Flag389prime-)
)

)
(:action Flag389Action-11
:parameters ()
:precondition
(and
(Flag275-)
(Flag275prime-)
)
:effect
(and
(Flag389-)
(Flag389prime-)
)

)
(:action Flag389Action-12
:parameters ()
:precondition
(and
(Flag377-)
(Flag377prime-)
)
:effect
(and
(Flag389-)
(Flag389prime-)
)

)
(:action Flag389Action-13
:parameters ()
:precondition
(and
(Flag378-)
(Flag378prime-)
)
:effect
(and
(Flag389-)
(Flag389prime-)
)

)
(:action Flag389Action-14
:parameters ()
:precondition
(and
(Flag322-)
(Flag322prime-)
)
:effect
(and
(Flag389-)
(Flag389prime-)
)

)
(:action Flag389Action-15
:parameters ()
:precondition
(and
(Flag300-)
(Flag300prime-)
)
:effect
(and
(Flag389-)
(Flag389prime-)
)

)
(:action Flag389Action-16
:parameters ()
:precondition
(and
(Flag303-)
(Flag303prime-)
)
:effect
(and
(Flag389-)
(Flag389prime-)
)

)
(:action Flag389Action-17
:parameters ()
:precondition
(and
(Flag372-)
(Flag372prime-)
)
:effect
(and
(Flag389-)
(Flag389prime-)
)

)
(:action Flag389Action-18
:parameters ()
:precondition
(and
(Flag274-)
(Flag274prime-)
)
:effect
(and
(Flag389-)
(Flag389prime-)
)

)
(:action Flag389Action-19
:parameters ()
:precondition
(and
(Flag326-)
(Flag326prime-)
)
:effect
(and
(Flag389-)
(Flag389prime-)
)

)
(:action Flag389Action-20
:parameters ()
:precondition
(and
(Flag279-)
(Flag279prime-)
)
:effect
(and
(Flag389-)
(Flag389prime-)
)

)
(:action Flag389Action-21
:parameters ()
:precondition
(and
(Flag349-)
(Flag349prime-)
)
:effect
(and
(Flag389-)
(Flag389prime-)
)

)
(:action Flag389Action-22
:parameters ()
:precondition
(and
(Flag368-)
(Flag368prime-)
)
:effect
(and
(Flag389-)
(Flag389prime-)
)

)
(:action Flag389Action-23
:parameters ()
:precondition
(and
(Flag283-)
(Flag283prime-)
)
:effect
(and
(Flag389-)
(Flag389prime-)
)

)
(:action Flag389Action-24
:parameters ()
:precondition
(and
(Flag363-)
(Flag363prime-)
)
:effect
(and
(Flag389-)
(Flag389prime-)
)

)
(:action Flag389Action-25
:parameters ()
:precondition
(and
(Flag313-)
(Flag313prime-)
)
:effect
(and
(Flag389-)
(Flag389prime-)
)

)
(:action Flag389Action-26
:parameters ()
:precondition
(and
(Flag331-)
(Flag331prime-)
)
:effect
(and
(Flag389-)
(Flag389prime-)
)

)
(:action Flag389Action-27
:parameters ()
:precondition
(and
(Flag288-)
(Flag288prime-)
)
:effect
(and
(Flag389-)
(Flag389prime-)
)

)
(:action Flag389Action-28
:parameters ()
:precondition
(and
(Flag318-)
(Flag318prime-)
)
:effect
(and
(Flag389-)
(Flag389prime-)
)

)
(:action Flag389Action-29
:parameters ()
:precondition
(and
(Flag350-)
(Flag350prime-)
)
:effect
(and
(Flag389-)
(Flag389prime-)
)

)
(:action Flag389Action-30
:parameters ()
:precondition
(and
(Flag366-)
(Flag366prime-)
)
:effect
(and
(Flag389-)
(Flag389prime-)
)

)
(:action Flag389Action-31
:parameters ()
:precondition
(and
(Flag292-)
(Flag292prime-)
)
:effect
(and
(Flag389-)
(Flag389prime-)
)

)
(:action Flag389Action-32
:parameters ()
:precondition
(and
(Flag293-)
(Flag293prime-)
)
:effect
(and
(Flag389-)
(Flag389prime-)
)

)
(:action Flag389Action-33
:parameters ()
:precondition
(and
(Flag379-)
(Flag379prime-)
)
:effect
(and
(Flag389-)
(Flag389prime-)
)

)
(:action Flag389Action-34
:parameters ()
:precondition
(and
(Flag355-)
(Flag355prime-)
)
:effect
(and
(Flag389-)
(Flag389prime-)
)

)
(:action Flag389Action-35
:parameters ()
:precondition
(and
(Flag330-)
(Flag330prime-)
)
:effect
(and
(Flag389-)
(Flag389prime-)
)

)
(:action Flag389Action-36
:parameters ()
:precondition
(and
(Flag380-)
(Flag380prime-)
)
:effect
(and
(Flag389-)
(Flag389prime-)
)

)
(:action Flag389Action-37
:parameters ()
:precondition
(and
(Flag381-)
(Flag381prime-)
)
:effect
(and
(Flag389-)
(Flag389prime-)
)

)
(:action Flag389Action-38
:parameters ()
:precondition
(and
(Flag369-)
(Flag369prime-)
)
:effect
(and
(Flag389-)
(Flag389prime-)
)

)
(:action Flag389Action-39
:parameters ()
:precondition
(and
(Flag340-)
(Flag340prime-)
)
:effect
(and
(Flag389-)
(Flag389prime-)
)

)
(:action Flag389Action-40
:parameters ()
:precondition
(and
(Flag304-)
(Flag304prime-)
)
:effect
(and
(Flag389-)
(Flag389prime-)
)

)
(:action Flag389Action-41
:parameters ()
:precondition
(and
(Flag305-)
(Flag305prime-)
)
:effect
(and
(Flag389-)
(Flag389prime-)
)

)
(:action Flag389Action-42
:parameters ()
:precondition
(and
(Flag323-)
(Flag323prime-)
)
:effect
(and
(Flag389-)
(Flag389prime-)
)

)
(:action Flag389Action-43
:parameters ()
:precondition
(and
(Flag382-)
(Flag382prime-)
)
:effect
(and
(Flag389-)
(Flag389prime-)
)

)
(:action Flag389Action-44
:parameters ()
:precondition
(and
(Flag344-)
(Flag344prime-)
)
:effect
(and
(Flag389-)
(Flag389prime-)
)

)
(:action Flag389Action-45
:parameters ()
:precondition
(and
(Flag345-)
(Flag345prime-)
)
:effect
(and
(Flag389-)
(Flag389prime-)
)

)
(:action Flag389Action-46
:parameters ()
:precondition
(and
(Flag276-)
(Flag276prime-)
)
:effect
(and
(Flag389-)
(Flag389prime-)
)

)
(:action Flag389Action-47
:parameters ()
:precondition
(and
(Flag383-)
(Flag383prime-)
)
:effect
(and
(Flag389-)
(Flag389prime-)
)

)
(:action Flag389Action-48
:parameters ()
:precondition
(and
(Flag310-)
(Flag310prime-)
)
:effect
(and
(Flag389-)
(Flag389prime-)
)

)
(:action Flag389Action-49
:parameters ()
:precondition
(and
(Flag384-)
(Flag384prime-)
)
:effect
(and
(Flag389-)
(Flag389prime-)
)

)
(:action Flag389Action-50
:parameters ()
:precondition
(and
(Flag329-)
(Flag329prime-)
)
:effect
(and
(Flag389-)
(Flag389prime-)
)

)
(:action Flag389Action-51
:parameters ()
:precondition
(and
(Flag337-)
(Flag337prime-)
)
:effect
(and
(Flag389-)
(Flag389prime-)
)

)
(:action Flag389Action-52
:parameters ()
:precondition
(and
(Flag357-)
(Flag357prime-)
)
:effect
(and
(Flag389-)
(Flag389prime-)
)

)
(:action Flag389Action-53
:parameters ()
:precondition
(and
(Flag385-)
(Flag385prime-)
)
:effect
(and
(Flag389-)
(Flag389prime-)
)

)
(:action Flag389Action-54
:parameters ()
:precondition
(and
(Flag347-)
(Flag347prime-)
)
:effect
(and
(Flag389-)
(Flag389prime-)
)

)
(:action Flag389Action-55
:parameters ()
:precondition
(and
(Flag314-)
(Flag314prime-)
)
:effect
(and
(Flag389-)
(Flag389prime-)
)

)
(:action Flag389Action-56
:parameters ()
:precondition
(and
(Flag291-)
(Flag291prime-)
)
:effect
(and
(Flag389-)
(Flag389prime-)
)

)
(:action Flag389Action-57
:parameters ()
:precondition
(and
(Flag289-)
(Flag289prime-)
)
:effect
(and
(Flag389-)
(Flag389prime-)
)

)
(:action Flag389Action-58
:parameters ()
:precondition
(and
(Flag297-)
(Flag297prime-)
)
:effect
(and
(Flag389-)
(Flag389prime-)
)

)
(:action Flag389Action-59
:parameters ()
:precondition
(and
(Flag335-)
(Flag335prime-)
)
:effect
(and
(Flag389-)
(Flag389prime-)
)

)
(:action Flag389Action-60
:parameters ()
:precondition
(and
(Flag298-)
(Flag298prime-)
)
:effect
(and
(Flag389-)
(Flag389prime-)
)

)
(:action Flag389Action-61
:parameters ()
:precondition
(and
(Flag371-)
(Flag371prime-)
)
:effect
(and
(Flag389-)
(Flag389prime-)
)

)
(:action Flag389Action-62
:parameters ()
:precondition
(and
(Flag338-)
(Flag338prime-)
)
:effect
(and
(Flag389-)
(Flag389prime-)
)

)
(:action Flag389Action-63
:parameters ()
:precondition
(and
(Flag306-)
(Flag306prime-)
)
:effect
(and
(Flag389-)
(Flag389prime-)
)

)
(:action Flag389Action-64
:parameters ()
:precondition
(and
(Flag348-)
(Flag348prime-)
)
:effect
(and
(Flag389-)
(Flag389prime-)
)

)
(:action Flag389Action-65
:parameters ()
:precondition
(and
(Flag373-)
(Flag373prime-)
)
:effect
(and
(Flag389-)
(Flag389prime-)
)

)
(:action Flag389Action-66
:parameters ()
:precondition
(and
(Flag359-)
(Flag359prime-)
)
:effect
(and
(Flag389-)
(Flag389prime-)
)

)
(:action Flag389Action-67
:parameters ()
:precondition
(and
(Flag299-)
(Flag299prime-)
)
:effect
(and
(Flag389-)
(Flag389prime-)
)

)
(:action Flag389Action-68
:parameters ()
:precondition
(and
(Flag361-)
(Flag361prime-)
)
:effect
(and
(Flag389-)
(Flag389prime-)
)

)
(:action Flag389Action-69
:parameters ()
:precondition
(and
(Flag309-)
(Flag309prime-)
)
:effect
(and
(Flag389-)
(Flag389prime-)
)

)
(:action Flag389Action-70
:parameters ()
:precondition
(and
(Flag327-)
(Flag327prime-)
)
:effect
(and
(Flag389-)
(Flag389prime-)
)

)
(:action Flag389Action-71
:parameters ()
:precondition
(and
(Flag354-)
(Flag354prime-)
)
:effect
(and
(Flag389-)
(Flag389prime-)
)

)
(:action Flag389Action-72
:parameters ()
:precondition
(and
(Flag284-)
(Flag284prime-)
)
:effect
(and
(Flag389-)
(Flag389prime-)
)

)
(:action Flag389Action-73
:parameters ()
:precondition
(and
(Flag364-)
(Flag364prime-)
)
:effect
(and
(Flag389-)
(Flag389prime-)
)

)
(:action Flag389Action-74
:parameters ()
:precondition
(and
(Flag286-)
(Flag286prime-)
)
:effect
(and
(Flag389-)
(Flag389prime-)
)

)
(:action Flag389Action-75
:parameters ()
:precondition
(and
(Flag312-)
(Flag312prime-)
)
:effect
(and
(Flag389-)
(Flag389prime-)
)

)
(:action Flag389Action-76
:parameters ()
:precondition
(and
(Flag333-)
(Flag333prime-)
)
:effect
(and
(Flag389-)
(Flag389prime-)
)

)
(:action Flag389Action-77
:parameters ()
:precondition
(and
(Flag386-)
(Flag386prime-)
)
:effect
(and
(Flag389-)
(Flag389prime-)
)

)
(:action Flag389Action-78
:parameters ()
:precondition
(and
(Flag356-)
(Flag356prime-)
)
:effect
(and
(Flag389-)
(Flag389prime-)
)

)
(:action Flag389Action-79
:parameters ()
:precondition
(and
(Flag387-)
(Flag387prime-)
)
:effect
(and
(Flag389-)
(Flag389prime-)
)

)
(:action Flag389Action-80
:parameters ()
:precondition
(and
(Flag296-)
(Flag296prime-)
)
:effect
(and
(Flag389-)
(Flag389prime-)
)

)
(:action Flag389Action-81
:parameters ()
:precondition
(and
(Flag320-)
(Flag320prime-)
)
:effect
(and
(Flag389-)
(Flag389prime-)
)

)
(:action Flag389Action-82
:parameters ()
:precondition
(and
(Flag334-)
(Flag334prime-)
)
:effect
(and
(Flag389-)
(Flag389prime-)
)

)
(:action Flag389Action-83
:parameters ()
:precondition
(and
(Flag321-)
(Flag321prime-)
)
:effect
(and
(Flag389-)
(Flag389prime-)
)

)
(:action Flag389Action-84
:parameters ()
:precondition
(and
(Flag336-)
(Flag336prime-)
)
:effect
(and
(Flag389-)
(Flag389prime-)
)

)
(:action Flag389Action-85
:parameters ()
:precondition
(and
(Flag273-)
(Flag273prime-)
)
:effect
(and
(Flag389-)
(Flag389prime-)
)

)
(:action Flag389Action-86
:parameters ()
:precondition
(and
(Flag388-)
(Flag388prime-)
)
:effect
(and
(Flag389-)
(Flag389prime-)
)

)
(:action Flag389Action-87
:parameters ()
:precondition
(and
(Flag342-)
(Flag342prime-)
)
:effect
(and
(Flag389-)
(Flag389prime-)
)

)
(:action Prim389Action
:parameters ()
:precondition
(and
(not (Flag278-))
(Flag278prime-)
(not (Flag375-))
(Flag375prime-)
(not (Flag362-))
(Flag362prime-)
(not (Flag376-))
(Flag376prime-)
(not (Flag365-))
(Flag365prime-)
(not (Flag351-))
(Flag351prime-)
(not (Flag352-))
(Flag352prime-)
(not (Flag353-))
(Flag353prime-)
(not (Flag315-))
(Flag315prime-)
(not (Flag316-))
(Flag316prime-)
(not (Flag294-))
(Flag294prime-)
(not (Flag275-))
(Flag275prime-)
(not (Flag377-))
(Flag377prime-)
(not (Flag378-))
(Flag378prime-)
(not (Flag322-))
(Flag322prime-)
(not (Flag300-))
(Flag300prime-)
(not (Flag303-))
(Flag303prime-)
(not (Flag372-))
(Flag372prime-)
(not (Flag274-))
(Flag274prime-)
(not (Flag326-))
(Flag326prime-)
(not (Flag279-))
(Flag279prime-)
(not (Flag349-))
(Flag349prime-)
(not (Flag368-))
(Flag368prime-)
(not (Flag283-))
(Flag283prime-)
(not (Flag363-))
(Flag363prime-)
(not (Flag313-))
(Flag313prime-)
(not (Flag331-))
(Flag331prime-)
(not (Flag288-))
(Flag288prime-)
(not (Flag318-))
(Flag318prime-)
(not (Flag350-))
(Flag350prime-)
(not (Flag366-))
(Flag366prime-)
(not (Flag292-))
(Flag292prime-)
(not (Flag293-))
(Flag293prime-)
(not (Flag379-))
(Flag379prime-)
(not (Flag355-))
(Flag355prime-)
(not (Flag330-))
(Flag330prime-)
(not (Flag380-))
(Flag380prime-)
(not (Flag381-))
(Flag381prime-)
(not (Flag369-))
(Flag369prime-)
(not (Flag340-))
(Flag340prime-)
(not (Flag304-))
(Flag304prime-)
(not (Flag305-))
(Flag305prime-)
(not (Flag323-))
(Flag323prime-)
(not (Flag382-))
(Flag382prime-)
(not (Flag344-))
(Flag344prime-)
(not (Flag345-))
(Flag345prime-)
(not (Flag276-))
(Flag276prime-)
(not (Flag383-))
(Flag383prime-)
(not (Flag310-))
(Flag310prime-)
(not (Flag384-))
(Flag384prime-)
(not (Flag329-))
(Flag329prime-)
(not (Flag337-))
(Flag337prime-)
(not (Flag357-))
(Flag357prime-)
(not (Flag385-))
(Flag385prime-)
(not (Flag347-))
(Flag347prime-)
(not (Flag314-))
(Flag314prime-)
(not (Flag291-))
(Flag291prime-)
(not (Flag289-))
(Flag289prime-)
(not (Flag297-))
(Flag297prime-)
(not (Flag335-))
(Flag335prime-)
(not (Flag298-))
(Flag298prime-)
(not (Flag371-))
(Flag371prime-)
(not (Flag338-))
(Flag338prime-)
(not (Flag306-))
(Flag306prime-)
(not (Flag348-))
(Flag348prime-)
(not (Flag373-))
(Flag373prime-)
(not (Flag359-))
(Flag359prime-)
(not (Flag299-))
(Flag299prime-)
(not (Flag361-))
(Flag361prime-)
(not (Flag309-))
(Flag309prime-)
(not (Flag327-))
(Flag327prime-)
(not (Flag354-))
(Flag354prime-)
(not (Flag284-))
(Flag284prime-)
(not (Flag364-))
(Flag364prime-)
(not (Flag286-))
(Flag286prime-)
(not (Flag312-))
(Flag312prime-)
(not (Flag333-))
(Flag333prime-)
(not (Flag386-))
(Flag386prime-)
(not (Flag356-))
(Flag356prime-)
(not (Flag387-))
(Flag387prime-)
(not (Flag296-))
(Flag296prime-)
(not (Flag320-))
(Flag320prime-)
(not (Flag334-))
(Flag334prime-)
(not (Flag321-))
(Flag321prime-)
(not (Flag336-))
(Flag336prime-)
(not (Flag273-))
(Flag273prime-)
(not (Flag388-))
(Flag388prime-)
(not (Flag342-))
(Flag342prime-)
)
:effect
(and
(Flag389prime-)
(not (Flag389-))
)

)
(:action Flag402Action-0
:parameters ()
:precondition
(and
(Flag278-)
(Flag278prime-)
)
:effect
(and
(Flag402-)
(Flag402prime-)
)

)
(:action Flag402Action-1
:parameters ()
:precondition
(and
(Flag362-)
(Flag362prime-)
)
:effect
(and
(Flag402-)
(Flag402prime-)
)

)
(:action Flag402Action-2
:parameters ()
:precondition
(and
(Flag376-)
(Flag376prime-)
)
:effect
(and
(Flag402-)
(Flag402prime-)
)

)
(:action Flag402Action-3
:parameters ()
:precondition
(and
(Flag276-)
(Flag276prime-)
)
:effect
(and
(Flag402-)
(Flag402prime-)
)

)
(:action Flag402Action-4
:parameters ()
:precondition
(and
(Flag365-)
(Flag365prime-)
)
:effect
(and
(Flag402-)
(Flag402prime-)
)

)
(:action Flag402Action-5
:parameters ()
:precondition
(and
(Flag351-)
(Flag351prime-)
)
:effect
(and
(Flag402-)
(Flag402prime-)
)

)
(:action Flag402Action-6
:parameters ()
:precondition
(and
(Flag390-)
(Flag390prime-)
)
:effect
(and
(Flag402-)
(Flag402prime-)
)

)
(:action Flag402Action-7
:parameters ()
:precondition
(and
(Flag353-)
(Flag353prime-)
)
:effect
(and
(Flag402-)
(Flag402prime-)
)

)
(:action Flag402Action-8
:parameters ()
:precondition
(and
(Flag315-)
(Flag315prime-)
)
:effect
(and
(Flag402-)
(Flag402prime-)
)

)
(:action Flag402Action-9
:parameters ()
:precondition
(and
(Flag316-)
(Flag316prime-)
)
:effect
(and
(Flag402-)
(Flag402prime-)
)

)
(:action Flag402Action-10
:parameters ()
:precondition
(and
(Flag294-)
(Flag294prime-)
)
:effect
(and
(Flag402-)
(Flag402prime-)
)

)
(:action Flag402Action-11
:parameters ()
:precondition
(and
(Flag391-)
(Flag391prime-)
)
:effect
(and
(Flag402-)
(Flag402prime-)
)

)
(:action Flag402Action-12
:parameters ()
:precondition
(and
(Flag275-)
(Flag275prime-)
)
:effect
(and
(Flag402-)
(Flag402prime-)
)

)
(:action Flag402Action-13
:parameters ()
:precondition
(and
(Flag377-)
(Flag377prime-)
)
:effect
(and
(Flag402-)
(Flag402prime-)
)

)
(:action Flag402Action-14
:parameters ()
:precondition
(and
(Flag322-)
(Flag322prime-)
)
:effect
(and
(Flag402-)
(Flag402prime-)
)

)
(:action Flag402Action-15
:parameters ()
:precondition
(and
(Flag300-)
(Flag300prime-)
)
:effect
(and
(Flag402-)
(Flag402prime-)
)

)
(:action Flag402Action-16
:parameters ()
:precondition
(and
(Flag372-)
(Flag372prime-)
)
:effect
(and
(Flag402-)
(Flag402prime-)
)

)
(:action Flag402Action-17
:parameters ()
:precondition
(and
(Flag274-)
(Flag274prime-)
)
:effect
(and
(Flag402-)
(Flag402prime-)
)

)
(:action Flag402Action-18
:parameters ()
:precondition
(and
(Flag326-)
(Flag326prime-)
)
:effect
(and
(Flag402-)
(Flag402prime-)
)

)
(:action Flag402Action-19
:parameters ()
:precondition
(and
(Flag279-)
(Flag279prime-)
)
:effect
(and
(Flag402-)
(Flag402prime-)
)

)
(:action Flag402Action-20
:parameters ()
:precondition
(and
(Flag392-)
(Flag392prime-)
)
:effect
(and
(Flag402-)
(Flag402prime-)
)

)
(:action Flag402Action-21
:parameters ()
:precondition
(and
(Flag349-)
(Flag349prime-)
)
:effect
(and
(Flag402-)
(Flag402prime-)
)

)
(:action Flag402Action-22
:parameters ()
:precondition
(and
(Flag368-)
(Flag368prime-)
)
:effect
(and
(Flag402-)
(Flag402prime-)
)

)
(:action Flag402Action-23
:parameters ()
:precondition
(and
(Flag293-)
(Flag293prime-)
)
:effect
(and
(Flag402-)
(Flag402prime-)
)

)
(:action Flag402Action-24
:parameters ()
:precondition
(and
(Flag363-)
(Flag363prime-)
)
:effect
(and
(Flag402-)
(Flag402prime-)
)

)
(:action Flag402Action-25
:parameters ()
:precondition
(and
(Flag313-)
(Flag313prime-)
)
:effect
(and
(Flag402-)
(Flag402prime-)
)

)
(:action Flag402Action-26
:parameters ()
:precondition
(and
(Flag331-)
(Flag331prime-)
)
:effect
(and
(Flag402-)
(Flag402prime-)
)

)
(:action Flag402Action-27
:parameters ()
:precondition
(and
(Flag288-)
(Flag288prime-)
)
:effect
(and
(Flag402-)
(Flag402prime-)
)

)
(:action Flag402Action-28
:parameters ()
:precondition
(and
(Flag318-)
(Flag318prime-)
)
:effect
(and
(Flag402-)
(Flag402prime-)
)

)
(:action Flag402Action-29
:parameters ()
:precondition
(and
(Flag350-)
(Flag350prime-)
)
:effect
(and
(Flag402-)
(Flag402prime-)
)

)
(:action Flag402Action-30
:parameters ()
:precondition
(and
(Flag366-)
(Flag366prime-)
)
:effect
(and
(Flag402-)
(Flag402prime-)
)

)
(:action Flag402Action-31
:parameters ()
:precondition
(and
(Flag393-)
(Flag393prime-)
)
:effect
(and
(Flag402-)
(Flag402prime-)
)

)
(:action Flag402Action-32
:parameters ()
:precondition
(and
(Flag292-)
(Flag292prime-)
)
:effect
(and
(Flag402-)
(Flag402prime-)
)

)
(:action Flag402Action-33
:parameters ()
:precondition
(and
(Flag283-)
(Flag283prime-)
)
:effect
(and
(Flag402-)
(Flag402prime-)
)

)
(:action Flag402Action-34
:parameters ()
:precondition
(and
(Flag379-)
(Flag379prime-)
)
:effect
(and
(Flag402-)
(Flag402prime-)
)

)
(:action Flag402Action-35
:parameters ()
:precondition
(and
(Flag355-)
(Flag355prime-)
)
:effect
(and
(Flag402-)
(Flag402prime-)
)

)
(:action Flag402Action-36
:parameters ()
:precondition
(and
(Flag394-)
(Flag394prime-)
)
:effect
(and
(Flag402-)
(Flag402prime-)
)

)
(:action Flag402Action-37
:parameters ()
:precondition
(and
(Flag330-)
(Flag330prime-)
)
:effect
(and
(Flag402-)
(Flag402prime-)
)

)
(:action Flag402Action-38
:parameters ()
:precondition
(and
(Flag381-)
(Flag381prime-)
)
:effect
(and
(Flag402-)
(Flag402prime-)
)

)
(:action Flag402Action-39
:parameters ()
:precondition
(and
(Flag369-)
(Flag369prime-)
)
:effect
(and
(Flag402-)
(Flag402prime-)
)

)
(:action Flag402Action-40
:parameters ()
:precondition
(and
(Flag340-)
(Flag340prime-)
)
:effect
(and
(Flag402-)
(Flag402prime-)
)

)
(:action Flag402Action-41
:parameters ()
:precondition
(and
(Flag304-)
(Flag304prime-)
)
:effect
(and
(Flag402-)
(Flag402prime-)
)

)
(:action Flag402Action-42
:parameters ()
:precondition
(and
(Flag305-)
(Flag305prime-)
)
:effect
(and
(Flag402-)
(Flag402prime-)
)

)
(:action Flag402Action-43
:parameters ()
:precondition
(and
(Flag323-)
(Flag323prime-)
)
:effect
(and
(Flag402-)
(Flag402prime-)
)

)
(:action Flag402Action-44
:parameters ()
:precondition
(and
(Flag382-)
(Flag382prime-)
)
:effect
(and
(Flag402-)
(Flag402prime-)
)

)
(:action Flag402Action-45
:parameters ()
:precondition
(and
(Flag344-)
(Flag344prime-)
)
:effect
(and
(Flag402-)
(Flag402prime-)
)

)
(:action Flag402Action-46
:parameters ()
:precondition
(and
(Flag345-)
(Flag345prime-)
)
:effect
(and
(Flag402-)
(Flag402prime-)
)

)
(:action Flag402Action-47
:parameters ()
:precondition
(and
(Flag289-)
(Flag289prime-)
)
:effect
(and
(Flag402-)
(Flag402prime-)
)

)
(:action Flag402Action-48
:parameters ()
:precondition
(and
(Flag383-)
(Flag383prime-)
)
:effect
(and
(Flag402-)
(Flag402prime-)
)

)
(:action Flag402Action-49
:parameters ()
:precondition
(and
(Flag310-)
(Flag310prime-)
)
:effect
(and
(Flag402-)
(Flag402prime-)
)

)
(:action Flag402Action-50
:parameters ()
:precondition
(and
(Flag384-)
(Flag384prime-)
)
:effect
(and
(Flag402-)
(Flag402prime-)
)

)
(:action Flag402Action-51
:parameters ()
:precondition
(and
(Flag395-)
(Flag395prime-)
)
:effect
(and
(Flag402-)
(Flag402prime-)
)

)
(:action Flag402Action-52
:parameters ()
:precondition
(and
(Flag396-)
(Flag396prime-)
)
:effect
(and
(Flag402-)
(Flag402prime-)
)

)
(:action Flag402Action-53
:parameters ()
:precondition
(and
(Flag329-)
(Flag329prime-)
)
:effect
(and
(Flag402-)
(Flag402prime-)
)

)
(:action Flag402Action-54
:parameters ()
:precondition
(and
(Flag337-)
(Flag337prime-)
)
:effect
(and
(Flag402-)
(Flag402prime-)
)

)
(:action Flag402Action-55
:parameters ()
:precondition
(and
(Flag397-)
(Flag397prime-)
)
:effect
(and
(Flag402-)
(Flag402prime-)
)

)
(:action Flag402Action-56
:parameters ()
:precondition
(and
(Flag357-)
(Flag357prime-)
)
:effect
(and
(Flag402-)
(Flag402prime-)
)

)
(:action Flag402Action-57
:parameters ()
:precondition
(and
(Flag385-)
(Flag385prime-)
)
:effect
(and
(Flag402-)
(Flag402prime-)
)

)
(:action Flag402Action-58
:parameters ()
:precondition
(and
(Flag314-)
(Flag314prime-)
)
:effect
(and
(Flag402-)
(Flag402prime-)
)

)
(:action Flag402Action-59
:parameters ()
:precondition
(and
(Flag398-)
(Flag398prime-)
)
:effect
(and
(Flag402-)
(Flag402prime-)
)

)
(:action Flag402Action-60
:parameters ()
:precondition
(and
(Flag297-)
(Flag297prime-)
)
:effect
(and
(Flag402-)
(Flag402prime-)
)

)
(:action Flag402Action-61
:parameters ()
:precondition
(and
(Flag335-)
(Flag335prime-)
)
:effect
(and
(Flag402-)
(Flag402prime-)
)

)
(:action Flag402Action-62
:parameters ()
:precondition
(and
(Flag298-)
(Flag298prime-)
)
:effect
(and
(Flag402-)
(Flag402prime-)
)

)
(:action Flag402Action-63
:parameters ()
:precondition
(and
(Flag371-)
(Flag371prime-)
)
:effect
(and
(Flag402-)
(Flag402prime-)
)

)
(:action Flag402Action-64
:parameters ()
:precondition
(and
(Flag399-)
(Flag399prime-)
)
:effect
(and
(Flag402-)
(Flag402prime-)
)

)
(:action Flag402Action-65
:parameters ()
:precondition
(and
(Flag348-)
(Flag348prime-)
)
:effect
(and
(Flag402-)
(Flag402prime-)
)

)
(:action Flag402Action-66
:parameters ()
:precondition
(and
(Flag373-)
(Flag373prime-)
)
:effect
(and
(Flag402-)
(Flag402prime-)
)

)
(:action Flag402Action-67
:parameters ()
:precondition
(and
(Flag400-)
(Flag400prime-)
)
:effect
(and
(Flag402-)
(Flag402prime-)
)

)
(:action Flag402Action-68
:parameters ()
:precondition
(and
(Flag359-)
(Flag359prime-)
)
:effect
(and
(Flag402-)
(Flag402prime-)
)

)
(:action Flag402Action-69
:parameters ()
:precondition
(and
(Flag299-)
(Flag299prime-)
)
:effect
(and
(Flag402-)
(Flag402prime-)
)

)
(:action Flag402Action-70
:parameters ()
:precondition
(and
(Flag361-)
(Flag361prime-)
)
:effect
(and
(Flag402-)
(Flag402prime-)
)

)
(:action Flag402Action-71
:parameters ()
:precondition
(and
(Flag347-)
(Flag347prime-)
)
:effect
(and
(Flag402-)
(Flag402prime-)
)

)
(:action Flag402Action-72
:parameters ()
:precondition
(and
(Flag327-)
(Flag327prime-)
)
:effect
(and
(Flag402-)
(Flag402prime-)
)

)
(:action Flag402Action-73
:parameters ()
:precondition
(and
(Flag291-)
(Flag291prime-)
)
:effect
(and
(Flag402-)
(Flag402prime-)
)

)
(:action Flag402Action-74
:parameters ()
:precondition
(and
(Flag354-)
(Flag354prime-)
)
:effect
(and
(Flag402-)
(Flag402prime-)
)

)
(:action Flag402Action-75
:parameters ()
:precondition
(and
(Flag284-)
(Flag284prime-)
)
:effect
(and
(Flag402-)
(Flag402prime-)
)

)
(:action Flag402Action-76
:parameters ()
:precondition
(and
(Flag364-)
(Flag364prime-)
)
:effect
(and
(Flag402-)
(Flag402prime-)
)

)
(:action Flag402Action-77
:parameters ()
:precondition
(and
(Flag286-)
(Flag286prime-)
)
:effect
(and
(Flag402-)
(Flag402prime-)
)

)
(:action Flag402Action-78
:parameters ()
:precondition
(and
(Flag401-)
(Flag401prime-)
)
:effect
(and
(Flag402-)
(Flag402prime-)
)

)
(:action Flag402Action-79
:parameters ()
:precondition
(and
(Flag312-)
(Flag312prime-)
)
:effect
(and
(Flag402-)
(Flag402prime-)
)

)
(:action Flag402Action-80
:parameters ()
:precondition
(and
(Flag333-)
(Flag333prime-)
)
:effect
(and
(Flag402-)
(Flag402prime-)
)

)
(:action Flag402Action-81
:parameters ()
:precondition
(and
(Flag386-)
(Flag386prime-)
)
:effect
(and
(Flag402-)
(Flag402prime-)
)

)
(:action Flag402Action-82
:parameters ()
:precondition
(and
(Flag356-)
(Flag356prime-)
)
:effect
(and
(Flag402-)
(Flag402prime-)
)

)
(:action Flag402Action-83
:parameters ()
:precondition
(and
(Flag387-)
(Flag387prime-)
)
:effect
(and
(Flag402-)
(Flag402prime-)
)

)
(:action Flag402Action-84
:parameters ()
:precondition
(and
(Flag296-)
(Flag296prime-)
)
:effect
(and
(Flag402-)
(Flag402prime-)
)

)
(:action Flag402Action-85
:parameters ()
:precondition
(and
(Flag320-)
(Flag320prime-)
)
:effect
(and
(Flag402-)
(Flag402prime-)
)

)
(:action Flag402Action-86
:parameters ()
:precondition
(and
(Flag334-)
(Flag334prime-)
)
:effect
(and
(Flag402-)
(Flag402prime-)
)

)
(:action Flag402Action-87
:parameters ()
:precondition
(and
(Flag321-)
(Flag321prime-)
)
:effect
(and
(Flag402-)
(Flag402prime-)
)

)
(:action Flag402Action-88
:parameters ()
:precondition
(and
(Flag336-)
(Flag336prime-)
)
:effect
(and
(Flag402-)
(Flag402prime-)
)

)
(:action Flag402Action-89
:parameters ()
:precondition
(and
(Flag273-)
(Flag273prime-)
)
:effect
(and
(Flag402-)
(Flag402prime-)
)

)
(:action Flag402Action-90
:parameters ()
:precondition
(and
(Flag388-)
(Flag388prime-)
)
:effect
(and
(Flag402-)
(Flag402prime-)
)

)
(:action Flag402Action-91
:parameters ()
:precondition
(and
(Flag342-)
(Flag342prime-)
)
:effect
(and
(Flag402-)
(Flag402prime-)
)

)
(:action Prim402Action
:parameters ()
:precondition
(and
(not (Flag278-))
(Flag278prime-)
(not (Flag362-))
(Flag362prime-)
(not (Flag376-))
(Flag376prime-)
(not (Flag276-))
(Flag276prime-)
(not (Flag365-))
(Flag365prime-)
(not (Flag351-))
(Flag351prime-)
(not (Flag390-))
(Flag390prime-)
(not (Flag353-))
(Flag353prime-)
(not (Flag315-))
(Flag315prime-)
(not (Flag316-))
(Flag316prime-)
(not (Flag294-))
(Flag294prime-)
(not (Flag391-))
(Flag391prime-)
(not (Flag275-))
(Flag275prime-)
(not (Flag377-))
(Flag377prime-)
(not (Flag322-))
(Flag322prime-)
(not (Flag300-))
(Flag300prime-)
(not (Flag372-))
(Flag372prime-)
(not (Flag274-))
(Flag274prime-)
(not (Flag326-))
(Flag326prime-)
(not (Flag279-))
(Flag279prime-)
(not (Flag392-))
(Flag392prime-)
(not (Flag349-))
(Flag349prime-)
(not (Flag368-))
(Flag368prime-)
(not (Flag293-))
(Flag293prime-)
(not (Flag363-))
(Flag363prime-)
(not (Flag313-))
(Flag313prime-)
(not (Flag331-))
(Flag331prime-)
(not (Flag288-))
(Flag288prime-)
(not (Flag318-))
(Flag318prime-)
(not (Flag350-))
(Flag350prime-)
(not (Flag366-))
(Flag366prime-)
(not (Flag393-))
(Flag393prime-)
(not (Flag292-))
(Flag292prime-)
(not (Flag283-))
(Flag283prime-)
(not (Flag379-))
(Flag379prime-)
(not (Flag355-))
(Flag355prime-)
(not (Flag394-))
(Flag394prime-)
(not (Flag330-))
(Flag330prime-)
(not (Flag381-))
(Flag381prime-)
(not (Flag369-))
(Flag369prime-)
(not (Flag340-))
(Flag340prime-)
(not (Flag304-))
(Flag304prime-)
(not (Flag305-))
(Flag305prime-)
(not (Flag323-))
(Flag323prime-)
(not (Flag382-))
(Flag382prime-)
(not (Flag344-))
(Flag344prime-)
(not (Flag345-))
(Flag345prime-)
(not (Flag289-))
(Flag289prime-)
(not (Flag383-))
(Flag383prime-)
(not (Flag310-))
(Flag310prime-)
(not (Flag384-))
(Flag384prime-)
(not (Flag395-))
(Flag395prime-)
(not (Flag396-))
(Flag396prime-)
(not (Flag329-))
(Flag329prime-)
(not (Flag337-))
(Flag337prime-)
(not (Flag397-))
(Flag397prime-)
(not (Flag357-))
(Flag357prime-)
(not (Flag385-))
(Flag385prime-)
(not (Flag314-))
(Flag314prime-)
(not (Flag398-))
(Flag398prime-)
(not (Flag297-))
(Flag297prime-)
(not (Flag335-))
(Flag335prime-)
(not (Flag298-))
(Flag298prime-)
(not (Flag371-))
(Flag371prime-)
(not (Flag399-))
(Flag399prime-)
(not (Flag348-))
(Flag348prime-)
(not (Flag373-))
(Flag373prime-)
(not (Flag400-))
(Flag400prime-)
(not (Flag359-))
(Flag359prime-)
(not (Flag299-))
(Flag299prime-)
(not (Flag361-))
(Flag361prime-)
(not (Flag347-))
(Flag347prime-)
(not (Flag327-))
(Flag327prime-)
(not (Flag291-))
(Flag291prime-)
(not (Flag354-))
(Flag354prime-)
(not (Flag284-))
(Flag284prime-)
(not (Flag364-))
(Flag364prime-)
(not (Flag286-))
(Flag286prime-)
(not (Flag401-))
(Flag401prime-)
(not (Flag312-))
(Flag312prime-)
(not (Flag333-))
(Flag333prime-)
(not (Flag386-))
(Flag386prime-)
(not (Flag356-))
(Flag356prime-)
(not (Flag387-))
(Flag387prime-)
(not (Flag296-))
(Flag296prime-)
(not (Flag320-))
(Flag320prime-)
(not (Flag334-))
(Flag334prime-)
(not (Flag321-))
(Flag321prime-)
(not (Flag336-))
(Flag336prime-)
(not (Flag273-))
(Flag273prime-)
(not (Flag388-))
(Flag388prime-)
(not (Flag342-))
(Flag342prime-)
)
:effect
(and
(Flag402prime-)
(not (Flag402-))
)

)
(:action Flag414Action-0
:parameters ()
:precondition
(and
(Flag278-)
(Flag278prime-)
)
:effect
(and
(Flag414-)
(Flag414prime-)
)

)
(:action Flag414Action-1
:parameters ()
:precondition
(and
(Flag403-)
(Flag403prime-)
)
:effect
(and
(Flag414-)
(Flag414prime-)
)

)
(:action Flag414Action-2
:parameters ()
:precondition
(and
(Flag404-)
(Flag404prime-)
)
:effect
(and
(Flag414-)
(Flag414prime-)
)

)
(:action Flag414Action-3
:parameters ()
:precondition
(and
(Flag362-)
(Flag362prime-)
)
:effect
(and
(Flag414-)
(Flag414prime-)
)

)
(:action Flag414Action-4
:parameters ()
:precondition
(and
(Flag376-)
(Flag376prime-)
)
:effect
(and
(Flag414-)
(Flag414prime-)
)

)
(:action Flag414Action-5
:parameters ()
:precondition
(and
(Flag276-)
(Flag276prime-)
)
:effect
(and
(Flag414-)
(Flag414prime-)
)

)
(:action Flag414Action-6
:parameters ()
:precondition
(and
(Flag365-)
(Flag365prime-)
)
:effect
(and
(Flag414-)
(Flag414prime-)
)

)
(:action Flag414Action-7
:parameters ()
:precondition
(and
(Flag351-)
(Flag351prime-)
)
:effect
(and
(Flag414-)
(Flag414prime-)
)

)
(:action Flag414Action-8
:parameters ()
:precondition
(and
(Flag390-)
(Flag390prime-)
)
:effect
(and
(Flag414-)
(Flag414prime-)
)

)
(:action Flag414Action-9
:parameters ()
:precondition
(and
(Flag353-)
(Flag353prime-)
)
:effect
(and
(Flag414-)
(Flag414prime-)
)

)
(:action Flag414Action-10
:parameters ()
:precondition
(and
(Flag315-)
(Flag315prime-)
)
:effect
(and
(Flag414-)
(Flag414prime-)
)

)
(:action Flag414Action-11
:parameters ()
:precondition
(and
(Flag316-)
(Flag316prime-)
)
:effect
(and
(Flag414-)
(Flag414prime-)
)

)
(:action Flag414Action-12
:parameters ()
:precondition
(and
(Flag294-)
(Flag294prime-)
)
:effect
(and
(Flag414-)
(Flag414prime-)
)

)
(:action Flag414Action-13
:parameters ()
:precondition
(and
(Flag405-)
(Flag405prime-)
)
:effect
(and
(Flag414-)
(Flag414prime-)
)

)
(:action Flag414Action-14
:parameters ()
:precondition
(and
(Flag391-)
(Flag391prime-)
)
:effect
(and
(Flag414-)
(Flag414prime-)
)

)
(:action Flag414Action-15
:parameters ()
:precondition
(and
(Flag275-)
(Flag275prime-)
)
:effect
(and
(Flag414-)
(Flag414prime-)
)

)
(:action Flag414Action-16
:parameters ()
:precondition
(and
(Flag377-)
(Flag377prime-)
)
:effect
(and
(Flag414-)
(Flag414prime-)
)

)
(:action Flag414Action-17
:parameters ()
:precondition
(and
(Flag322-)
(Flag322prime-)
)
:effect
(and
(Flag414-)
(Flag414prime-)
)

)
(:action Flag414Action-18
:parameters ()
:precondition
(and
(Flag384-)
(Flag384prime-)
)
:effect
(and
(Flag414-)
(Flag414prime-)
)

)
(:action Flag414Action-19
:parameters ()
:precondition
(and
(Flag300-)
(Flag300prime-)
)
:effect
(and
(Flag414-)
(Flag414prime-)
)

)
(:action Flag414Action-20
:parameters ()
:precondition
(and
(Flag372-)
(Flag372prime-)
)
:effect
(and
(Flag414-)
(Flag414prime-)
)

)
(:action Flag414Action-21
:parameters ()
:precondition
(and
(Flag274-)
(Flag274prime-)
)
:effect
(and
(Flag414-)
(Flag414prime-)
)

)
(:action Flag414Action-22
:parameters ()
:precondition
(and
(Flag326-)
(Flag326prime-)
)
:effect
(and
(Flag414-)
(Flag414prime-)
)

)
(:action Flag414Action-23
:parameters ()
:precondition
(and
(Flag279-)
(Flag279prime-)
)
:effect
(and
(Flag414-)
(Flag414prime-)
)

)
(:action Flag414Action-24
:parameters ()
:precondition
(and
(Flag392-)
(Flag392prime-)
)
:effect
(and
(Flag414-)
(Flag414prime-)
)

)
(:action Flag414Action-25
:parameters ()
:precondition
(and
(Flag349-)
(Flag349prime-)
)
:effect
(and
(Flag414-)
(Flag414prime-)
)

)
(:action Flag414Action-26
:parameters ()
:precondition
(and
(Flag406-)
(Flag406prime-)
)
:effect
(and
(Flag414-)
(Flag414prime-)
)

)
(:action Flag414Action-27
:parameters ()
:precondition
(and
(Flag363-)
(Flag363prime-)
)
:effect
(and
(Flag414-)
(Flag414prime-)
)

)
(:action Flag414Action-28
:parameters ()
:precondition
(and
(Flag331-)
(Flag331prime-)
)
:effect
(and
(Flag414-)
(Flag414prime-)
)

)
(:action Flag414Action-29
:parameters ()
:precondition
(and
(Flag288-)
(Flag288prime-)
)
:effect
(and
(Flag414-)
(Flag414prime-)
)

)
(:action Flag414Action-30
:parameters ()
:precondition
(and
(Flag318-)
(Flag318prime-)
)
:effect
(and
(Flag414-)
(Flag414prime-)
)

)
(:action Flag414Action-31
:parameters ()
:precondition
(and
(Flag350-)
(Flag350prime-)
)
:effect
(and
(Flag414-)
(Flag414prime-)
)

)
(:action Flag414Action-32
:parameters ()
:precondition
(and
(Flag366-)
(Flag366prime-)
)
:effect
(and
(Flag414-)
(Flag414prime-)
)

)
(:action Flag414Action-33
:parameters ()
:precondition
(and
(Flag292-)
(Flag292prime-)
)
:effect
(and
(Flag414-)
(Flag414prime-)
)

)
(:action Flag414Action-34
:parameters ()
:precondition
(and
(Flag407-)
(Flag407prime-)
)
:effect
(and
(Flag414-)
(Flag414prime-)
)

)
(:action Flag414Action-35
:parameters ()
:precondition
(and
(Flag379-)
(Flag379prime-)
)
:effect
(and
(Flag414-)
(Flag414prime-)
)

)
(:action Flag414Action-36
:parameters ()
:precondition
(and
(Flag355-)
(Flag355prime-)
)
:effect
(and
(Flag414-)
(Flag414prime-)
)

)
(:action Flag414Action-37
:parameters ()
:precondition
(and
(Flag408-)
(Flag408prime-)
)
:effect
(and
(Flag414-)
(Flag414prime-)
)

)
(:action Flag414Action-38
:parameters ()
:precondition
(and
(Flag394-)
(Flag394prime-)
)
:effect
(and
(Flag414-)
(Flag414prime-)
)

)
(:action Flag414Action-39
:parameters ()
:precondition
(and
(Flag330-)
(Flag330prime-)
)
:effect
(and
(Flag414-)
(Flag414prime-)
)

)
(:action Flag414Action-40
:parameters ()
:precondition
(and
(Flag381-)
(Flag381prime-)
)
:effect
(and
(Flag414-)
(Flag414prime-)
)

)
(:action Flag414Action-41
:parameters ()
:precondition
(and
(Flag369-)
(Flag369prime-)
)
:effect
(and
(Flag414-)
(Flag414prime-)
)

)
(:action Flag414Action-42
:parameters ()
:precondition
(and
(Flag340-)
(Flag340prime-)
)
:effect
(and
(Flag414-)
(Flag414prime-)
)

)
(:action Flag414Action-43
:parameters ()
:precondition
(and
(Flag304-)
(Flag304prime-)
)
:effect
(and
(Flag414-)
(Flag414prime-)
)

)
(:action Flag414Action-44
:parameters ()
:precondition
(and
(Flag305-)
(Flag305prime-)
)
:effect
(and
(Flag414-)
(Flag414prime-)
)

)
(:action Flag414Action-45
:parameters ()
:precondition
(and
(Flag323-)
(Flag323prime-)
)
:effect
(and
(Flag414-)
(Flag414prime-)
)

)
(:action Flag414Action-46
:parameters ()
:precondition
(and
(Flag382-)
(Flag382prime-)
)
:effect
(and
(Flag414-)
(Flag414prime-)
)

)
(:action Flag414Action-47
:parameters ()
:precondition
(and
(Flag344-)
(Flag344prime-)
)
:effect
(and
(Flag414-)
(Flag414prime-)
)

)
(:action Flag414Action-48
:parameters ()
:precondition
(and
(Flag345-)
(Flag345prime-)
)
:effect
(and
(Flag414-)
(Flag414prime-)
)

)
(:action Flag414Action-49
:parameters ()
:precondition
(and
(Flag289-)
(Flag289prime-)
)
:effect
(and
(Flag414-)
(Flag414prime-)
)

)
(:action Flag414Action-50
:parameters ()
:precondition
(and
(Flag383-)
(Flag383prime-)
)
:effect
(and
(Flag414-)
(Flag414prime-)
)

)
(:action Flag414Action-51
:parameters ()
:precondition
(and
(Flag310-)
(Flag310prime-)
)
:effect
(and
(Flag414-)
(Flag414prime-)
)

)
(:action Flag414Action-52
:parameters ()
:precondition
(and
(Flag409-)
(Flag409prime-)
)
:effect
(and
(Flag414-)
(Flag414prime-)
)

)
(:action Flag414Action-53
:parameters ()
:precondition
(and
(Flag320-)
(Flag320prime-)
)
:effect
(and
(Flag414-)
(Flag414prime-)
)

)
(:action Flag414Action-54
:parameters ()
:precondition
(and
(Flag395-)
(Flag395prime-)
)
:effect
(and
(Flag414-)
(Flag414prime-)
)

)
(:action Flag414Action-55
:parameters ()
:precondition
(and
(Flag337-)
(Flag337prime-)
)
:effect
(and
(Flag414-)
(Flag414prime-)
)

)
(:action Flag414Action-56
:parameters ()
:precondition
(and
(Flag397-)
(Flag397prime-)
)
:effect
(and
(Flag414-)
(Flag414prime-)
)

)
(:action Flag414Action-57
:parameters ()
:precondition
(and
(Flag357-)
(Flag357prime-)
)
:effect
(and
(Flag414-)
(Flag414prime-)
)

)
(:action Flag414Action-58
:parameters ()
:precondition
(and
(Flag385-)
(Flag385prime-)
)
:effect
(and
(Flag414-)
(Flag414prime-)
)

)
(:action Flag414Action-59
:parameters ()
:precondition
(and
(Flag314-)
(Flag314prime-)
)
:effect
(and
(Flag414-)
(Flag414prime-)
)

)
(:action Flag414Action-60
:parameters ()
:precondition
(and
(Flag410-)
(Flag410prime-)
)
:effect
(and
(Flag414-)
(Flag414prime-)
)

)
(:action Flag414Action-61
:parameters ()
:precondition
(and
(Flag398-)
(Flag398prime-)
)
:effect
(and
(Flag414-)
(Flag414prime-)
)

)
(:action Flag414Action-62
:parameters ()
:precondition
(and
(Flag297-)
(Flag297prime-)
)
:effect
(and
(Flag414-)
(Flag414prime-)
)

)
(:action Flag414Action-63
:parameters ()
:precondition
(and
(Flag335-)
(Flag335prime-)
)
:effect
(and
(Flag414-)
(Flag414prime-)
)

)
(:action Flag414Action-64
:parameters ()
:precondition
(and
(Flag298-)
(Flag298prime-)
)
:effect
(and
(Flag414-)
(Flag414prime-)
)

)
(:action Flag414Action-65
:parameters ()
:precondition
(and
(Flag371-)
(Flag371prime-)
)
:effect
(and
(Flag414-)
(Flag414prime-)
)

)
(:action Flag414Action-66
:parameters ()
:precondition
(and
(Flag399-)
(Flag399prime-)
)
:effect
(and
(Flag414-)
(Flag414prime-)
)

)
(:action Flag414Action-67
:parameters ()
:precondition
(and
(Flag348-)
(Flag348prime-)
)
:effect
(and
(Flag414-)
(Flag414prime-)
)

)
(:action Flag414Action-68
:parameters ()
:precondition
(and
(Flag373-)
(Flag373prime-)
)
:effect
(and
(Flag414-)
(Flag414prime-)
)

)
(:action Flag414Action-69
:parameters ()
:precondition
(and
(Flag400-)
(Flag400prime-)
)
:effect
(and
(Flag414-)
(Flag414prime-)
)

)
(:action Flag414Action-70
:parameters ()
:precondition
(and
(Flag359-)
(Flag359prime-)
)
:effect
(and
(Flag414-)
(Flag414prime-)
)

)
(:action Flag414Action-71
:parameters ()
:precondition
(and
(Flag299-)
(Flag299prime-)
)
:effect
(and
(Flag414-)
(Flag414prime-)
)

)
(:action Flag414Action-72
:parameters ()
:precondition
(and
(Flag361-)
(Flag361prime-)
)
:effect
(and
(Flag414-)
(Flag414prime-)
)

)
(:action Flag414Action-73
:parameters ()
:precondition
(and
(Flag411-)
(Flag411prime-)
)
:effect
(and
(Flag414-)
(Flag414prime-)
)

)
(:action Flag414Action-74
:parameters ()
:precondition
(and
(Flag327-)
(Flag327prime-)
)
:effect
(and
(Flag414-)
(Flag414prime-)
)

)
(:action Flag414Action-75
:parameters ()
:precondition
(and
(Flag291-)
(Flag291prime-)
)
:effect
(and
(Flag414-)
(Flag414prime-)
)

)
(:action Flag414Action-76
:parameters ()
:precondition
(and
(Flag354-)
(Flag354prime-)
)
:effect
(and
(Flag414-)
(Flag414prime-)
)

)
(:action Flag414Action-77
:parameters ()
:precondition
(and
(Flag284-)
(Flag284prime-)
)
:effect
(and
(Flag414-)
(Flag414prime-)
)

)
(:action Flag414Action-78
:parameters ()
:precondition
(and
(Flag364-)
(Flag364prime-)
)
:effect
(and
(Flag414-)
(Flag414prime-)
)

)
(:action Flag414Action-79
:parameters ()
:precondition
(and
(Flag286-)
(Flag286prime-)
)
:effect
(and
(Flag414-)
(Flag414prime-)
)

)
(:action Flag414Action-80
:parameters ()
:precondition
(and
(Flag401-)
(Flag401prime-)
)
:effect
(and
(Flag414-)
(Flag414prime-)
)

)
(:action Flag414Action-81
:parameters ()
:precondition
(and
(Flag312-)
(Flag312prime-)
)
:effect
(and
(Flag414-)
(Flag414prime-)
)

)
(:action Flag414Action-82
:parameters ()
:precondition
(and
(Flag386-)
(Flag386prime-)
)
:effect
(and
(Flag414-)
(Flag414prime-)
)

)
(:action Flag414Action-83
:parameters ()
:precondition
(and
(Flag356-)
(Flag356prime-)
)
:effect
(and
(Flag414-)
(Flag414prime-)
)

)
(:action Flag414Action-84
:parameters ()
:precondition
(and
(Flag387-)
(Flag387prime-)
)
:effect
(and
(Flag414-)
(Flag414prime-)
)

)
(:action Flag414Action-85
:parameters ()
:precondition
(and
(Flag296-)
(Flag296prime-)
)
:effect
(and
(Flag414-)
(Flag414prime-)
)

)
(:action Flag414Action-86
:parameters ()
:precondition
(and
(Flag412-)
(Flag412prime-)
)
:effect
(and
(Flag414-)
(Flag414prime-)
)

)
(:action Flag414Action-87
:parameters ()
:precondition
(and
(Flag334-)
(Flag334prime-)
)
:effect
(and
(Flag414-)
(Flag414prime-)
)

)
(:action Flag414Action-88
:parameters ()
:precondition
(and
(Flag342-)
(Flag342prime-)
)
:effect
(and
(Flag414-)
(Flag414prime-)
)

)
(:action Flag414Action-89
:parameters ()
:precondition
(and
(Flag321-)
(Flag321prime-)
)
:effect
(and
(Flag414-)
(Flag414prime-)
)

)
(:action Flag414Action-90
:parameters ()
:precondition
(and
(Flag336-)
(Flag336prime-)
)
:effect
(and
(Flag414-)
(Flag414prime-)
)

)
(:action Flag414Action-91
:parameters ()
:precondition
(and
(Flag273-)
(Flag273prime-)
)
:effect
(and
(Flag414-)
(Flag414prime-)
)

)
(:action Flag414Action-92
:parameters ()
:precondition
(and
(Flag388-)
(Flag388prime-)
)
:effect
(and
(Flag414-)
(Flag414prime-)
)

)
(:action Flag414Action-93
:parameters ()
:precondition
(and
(Flag413-)
(Flag413prime-)
)
:effect
(and
(Flag414-)
(Flag414prime-)
)

)
(:action Prim414Action
:parameters ()
:precondition
(and
(not (Flag278-))
(Flag278prime-)
(not (Flag403-))
(Flag403prime-)
(not (Flag404-))
(Flag404prime-)
(not (Flag362-))
(Flag362prime-)
(not (Flag376-))
(Flag376prime-)
(not (Flag276-))
(Flag276prime-)
(not (Flag365-))
(Flag365prime-)
(not (Flag351-))
(Flag351prime-)
(not (Flag390-))
(Flag390prime-)
(not (Flag353-))
(Flag353prime-)
(not (Flag315-))
(Flag315prime-)
(not (Flag316-))
(Flag316prime-)
(not (Flag294-))
(Flag294prime-)
(not (Flag405-))
(Flag405prime-)
(not (Flag391-))
(Flag391prime-)
(not (Flag275-))
(Flag275prime-)
(not (Flag377-))
(Flag377prime-)
(not (Flag322-))
(Flag322prime-)
(not (Flag384-))
(Flag384prime-)
(not (Flag300-))
(Flag300prime-)
(not (Flag372-))
(Flag372prime-)
(not (Flag274-))
(Flag274prime-)
(not (Flag326-))
(Flag326prime-)
(not (Flag279-))
(Flag279prime-)
(not (Flag392-))
(Flag392prime-)
(not (Flag349-))
(Flag349prime-)
(not (Flag406-))
(Flag406prime-)
(not (Flag363-))
(Flag363prime-)
(not (Flag331-))
(Flag331prime-)
(not (Flag288-))
(Flag288prime-)
(not (Flag318-))
(Flag318prime-)
(not (Flag350-))
(Flag350prime-)
(not (Flag366-))
(Flag366prime-)
(not (Flag292-))
(Flag292prime-)
(not (Flag407-))
(Flag407prime-)
(not (Flag379-))
(Flag379prime-)
(not (Flag355-))
(Flag355prime-)
(not (Flag408-))
(Flag408prime-)
(not (Flag394-))
(Flag394prime-)
(not (Flag330-))
(Flag330prime-)
(not (Flag381-))
(Flag381prime-)
(not (Flag369-))
(Flag369prime-)
(not (Flag340-))
(Flag340prime-)
(not (Flag304-))
(Flag304prime-)
(not (Flag305-))
(Flag305prime-)
(not (Flag323-))
(Flag323prime-)
(not (Flag382-))
(Flag382prime-)
(not (Flag344-))
(Flag344prime-)
(not (Flag345-))
(Flag345prime-)
(not (Flag289-))
(Flag289prime-)
(not (Flag383-))
(Flag383prime-)
(not (Flag310-))
(Flag310prime-)
(not (Flag409-))
(Flag409prime-)
(not (Flag320-))
(Flag320prime-)
(not (Flag395-))
(Flag395prime-)
(not (Flag337-))
(Flag337prime-)
(not (Flag397-))
(Flag397prime-)
(not (Flag357-))
(Flag357prime-)
(not (Flag385-))
(Flag385prime-)
(not (Flag314-))
(Flag314prime-)
(not (Flag410-))
(Flag410prime-)
(not (Flag398-))
(Flag398prime-)
(not (Flag297-))
(Flag297prime-)
(not (Flag335-))
(Flag335prime-)
(not (Flag298-))
(Flag298prime-)
(not (Flag371-))
(Flag371prime-)
(not (Flag399-))
(Flag399prime-)
(not (Flag348-))
(Flag348prime-)
(not (Flag373-))
(Flag373prime-)
(not (Flag400-))
(Flag400prime-)
(not (Flag359-))
(Flag359prime-)
(not (Flag299-))
(Flag299prime-)
(not (Flag361-))
(Flag361prime-)
(not (Flag411-))
(Flag411prime-)
(not (Flag327-))
(Flag327prime-)
(not (Flag291-))
(Flag291prime-)
(not (Flag354-))
(Flag354prime-)
(not (Flag284-))
(Flag284prime-)
(not (Flag364-))
(Flag364prime-)
(not (Flag286-))
(Flag286prime-)
(not (Flag401-))
(Flag401prime-)
(not (Flag312-))
(Flag312prime-)
(not (Flag386-))
(Flag386prime-)
(not (Flag356-))
(Flag356prime-)
(not (Flag387-))
(Flag387prime-)
(not (Flag296-))
(Flag296prime-)
(not (Flag412-))
(Flag412prime-)
(not (Flag334-))
(Flag334prime-)
(not (Flag342-))
(Flag342prime-)
(not (Flag321-))
(Flag321prime-)
(not (Flag336-))
(Flag336prime-)
(not (Flag273-))
(Flag273prime-)
(not (Flag388-))
(Flag388prime-)
(not (Flag413-))
(Flag413prime-)
)
:effect
(and
(Flag414prime-)
(not (Flag414-))
)

)
(:action Flag424Action-0
:parameters ()
:precondition
(and
(Flag278-)
(Flag278prime-)
)
:effect
(and
(Flag424-)
(Flag424prime-)
)

)
(:action Flag424Action-1
:parameters ()
:precondition
(and
(Flag415-)
(Flag415prime-)
)
:effect
(and
(Flag424-)
(Flag424prime-)
)

)
(:action Flag424Action-2
:parameters ()
:precondition
(and
(Flag403-)
(Flag403prime-)
)
:effect
(and
(Flag424-)
(Flag424prime-)
)

)
(:action Flag424Action-3
:parameters ()
:precondition
(and
(Flag362-)
(Flag362prime-)
)
:effect
(and
(Flag424-)
(Flag424prime-)
)

)
(:action Flag424Action-4
:parameters ()
:precondition
(and
(Flag376-)
(Flag376prime-)
)
:effect
(and
(Flag424-)
(Flag424prime-)
)

)
(:action Flag424Action-5
:parameters ()
:precondition
(and
(Flag365-)
(Flag365prime-)
)
:effect
(and
(Flag424-)
(Flag424prime-)
)

)
(:action Flag424Action-6
:parameters ()
:precondition
(and
(Flag390-)
(Flag390prime-)
)
:effect
(and
(Flag424-)
(Flag424prime-)
)

)
(:action Flag424Action-7
:parameters ()
:precondition
(and
(Flag353-)
(Flag353prime-)
)
:effect
(and
(Flag424-)
(Flag424prime-)
)

)
(:action Flag424Action-8
:parameters ()
:precondition
(and
(Flag315-)
(Flag315prime-)
)
:effect
(and
(Flag424-)
(Flag424prime-)
)

)
(:action Flag424Action-9
:parameters ()
:precondition
(and
(Flag354-)
(Flag354prime-)
)
:effect
(and
(Flag424-)
(Flag424prime-)
)

)
(:action Flag424Action-10
:parameters ()
:precondition
(and
(Flag294-)
(Flag294prime-)
)
:effect
(and
(Flag424-)
(Flag424prime-)
)

)
(:action Flag424Action-11
:parameters ()
:precondition
(and
(Flag405-)
(Flag405prime-)
)
:effect
(and
(Flag424-)
(Flag424prime-)
)

)
(:action Flag424Action-12
:parameters ()
:precondition
(and
(Flag391-)
(Flag391prime-)
)
:effect
(and
(Flag424-)
(Flag424prime-)
)

)
(:action Flag424Action-13
:parameters ()
:precondition
(and
(Flag275-)
(Flag275prime-)
)
:effect
(and
(Flag424-)
(Flag424prime-)
)

)
(:action Flag424Action-14
:parameters ()
:precondition
(and
(Flag377-)
(Flag377prime-)
)
:effect
(and
(Flag424-)
(Flag424prime-)
)

)
(:action Flag424Action-15
:parameters ()
:precondition
(and
(Flag322-)
(Flag322prime-)
)
:effect
(and
(Flag424-)
(Flag424prime-)
)

)
(:action Flag424Action-16
:parameters ()
:precondition
(and
(Flag372-)
(Flag372prime-)
)
:effect
(and
(Flag424-)
(Flag424prime-)
)

)
(:action Flag424Action-17
:parameters ()
:precondition
(and
(Flag274-)
(Flag274prime-)
)
:effect
(and
(Flag424-)
(Flag424prime-)
)

)
(:action Flag424Action-18
:parameters ()
:precondition
(and
(Flag326-)
(Flag326prime-)
)
:effect
(and
(Flag424-)
(Flag424prime-)
)

)
(:action Flag424Action-19
:parameters ()
:precondition
(and
(Flag279-)
(Flag279prime-)
)
:effect
(and
(Flag424-)
(Flag424prime-)
)

)
(:action Flag424Action-20
:parameters ()
:precondition
(and
(Flag392-)
(Flag392prime-)
)
:effect
(and
(Flag424-)
(Flag424prime-)
)

)
(:action Flag424Action-21
:parameters ()
:precondition
(and
(Flag349-)
(Flag349prime-)
)
:effect
(and
(Flag424-)
(Flag424prime-)
)

)
(:action Flag424Action-22
:parameters ()
:precondition
(and
(Flag406-)
(Flag406prime-)
)
:effect
(and
(Flag424-)
(Flag424prime-)
)

)
(:action Flag424Action-23
:parameters ()
:precondition
(and
(Flag363-)
(Flag363prime-)
)
:effect
(and
(Flag424-)
(Flag424prime-)
)

)
(:action Flag424Action-24
:parameters ()
:precondition
(and
(Flag331-)
(Flag331prime-)
)
:effect
(and
(Flag424-)
(Flag424prime-)
)

)
(:action Flag424Action-25
:parameters ()
:precondition
(and
(Flag288-)
(Flag288prime-)
)
:effect
(and
(Flag424-)
(Flag424prime-)
)

)
(:action Flag424Action-26
:parameters ()
:precondition
(and
(Flag318-)
(Flag318prime-)
)
:effect
(and
(Flag424-)
(Flag424prime-)
)

)
(:action Flag424Action-27
:parameters ()
:precondition
(and
(Flag350-)
(Flag350prime-)
)
:effect
(and
(Flag424-)
(Flag424prime-)
)

)
(:action Flag424Action-28
:parameters ()
:precondition
(and
(Flag366-)
(Flag366prime-)
)
:effect
(and
(Flag424-)
(Flag424prime-)
)

)
(:action Flag424Action-29
:parameters ()
:precondition
(and
(Flag292-)
(Flag292prime-)
)
:effect
(and
(Flag424-)
(Flag424prime-)
)

)
(:action Flag424Action-30
:parameters ()
:precondition
(and
(Flag407-)
(Flag407prime-)
)
:effect
(and
(Flag424-)
(Flag424prime-)
)

)
(:action Flag424Action-31
:parameters ()
:precondition
(and
(Flag379-)
(Flag379prime-)
)
:effect
(and
(Flag424-)
(Flag424prime-)
)

)
(:action Flag424Action-32
:parameters ()
:precondition
(and
(Flag355-)
(Flag355prime-)
)
:effect
(and
(Flag424-)
(Flag424prime-)
)

)
(:action Flag424Action-33
:parameters ()
:precondition
(and
(Flag408-)
(Flag408prime-)
)
:effect
(and
(Flag424-)
(Flag424prime-)
)

)
(:action Flag424Action-34
:parameters ()
:precondition
(and
(Flag394-)
(Flag394prime-)
)
:effect
(and
(Flag424-)
(Flag424prime-)
)

)
(:action Flag424Action-35
:parameters ()
:precondition
(and
(Flag330-)
(Flag330prime-)
)
:effect
(and
(Flag424-)
(Flag424prime-)
)

)
(:action Flag424Action-36
:parameters ()
:precondition
(and
(Flag381-)
(Flag381prime-)
)
:effect
(and
(Flag424-)
(Flag424prime-)
)

)
(:action Flag424Action-37
:parameters ()
:precondition
(and
(Flag369-)
(Flag369prime-)
)
:effect
(and
(Flag424-)
(Flag424prime-)
)

)
(:action Flag424Action-38
:parameters ()
:precondition
(and
(Flag416-)
(Flag416prime-)
)
:effect
(and
(Flag424-)
(Flag424prime-)
)

)
(:action Flag424Action-39
:parameters ()
:precondition
(and
(Flag340-)
(Flag340prime-)
)
:effect
(and
(Flag424-)
(Flag424prime-)
)

)
(:action Flag424Action-40
:parameters ()
:precondition
(and
(Flag304-)
(Flag304prime-)
)
:effect
(and
(Flag424-)
(Flag424prime-)
)

)
(:action Flag424Action-41
:parameters ()
:precondition
(and
(Flag305-)
(Flag305prime-)
)
:effect
(and
(Flag424-)
(Flag424prime-)
)

)
(:action Flag424Action-42
:parameters ()
:precondition
(and
(Flag323-)
(Flag323prime-)
)
:effect
(and
(Flag424-)
(Flag424prime-)
)

)
(:action Flag424Action-43
:parameters ()
:precondition
(and
(Flag382-)
(Flag382prime-)
)
:effect
(and
(Flag424-)
(Flag424prime-)
)

)
(:action Flag424Action-44
:parameters ()
:precondition
(and
(Flag344-)
(Flag344prime-)
)
:effect
(and
(Flag424-)
(Flag424prime-)
)

)
(:action Flag424Action-45
:parameters ()
:precondition
(and
(Flag345-)
(Flag345prime-)
)
:effect
(and
(Flag424-)
(Flag424prime-)
)

)
(:action Flag424Action-46
:parameters ()
:precondition
(and
(Flag276-)
(Flag276prime-)
)
:effect
(and
(Flag424-)
(Flag424prime-)
)

)
(:action Flag424Action-47
:parameters ()
:precondition
(and
(Flag417-)
(Flag417prime-)
)
:effect
(and
(Flag424-)
(Flag424prime-)
)

)
(:action Flag424Action-48
:parameters ()
:precondition
(and
(Flag310-)
(Flag310prime-)
)
:effect
(and
(Flag424-)
(Flag424prime-)
)

)
(:action Flag424Action-49
:parameters ()
:precondition
(and
(Flag409-)
(Flag409prime-)
)
:effect
(and
(Flag424-)
(Flag424prime-)
)

)
(:action Flag424Action-50
:parameters ()
:precondition
(and
(Flag320-)
(Flag320prime-)
)
:effect
(and
(Flag424-)
(Flag424prime-)
)

)
(:action Flag424Action-51
:parameters ()
:precondition
(and
(Flag395-)
(Flag395prime-)
)
:effect
(and
(Flag424-)
(Flag424prime-)
)

)
(:action Flag424Action-52
:parameters ()
:precondition
(and
(Flag418-)
(Flag418prime-)
)
:effect
(and
(Flag424-)
(Flag424prime-)
)

)
(:action Flag424Action-53
:parameters ()
:precondition
(and
(Flag337-)
(Flag337prime-)
)
:effect
(and
(Flag424-)
(Flag424prime-)
)

)
(:action Flag424Action-54
:parameters ()
:precondition
(and
(Flag397-)
(Flag397prime-)
)
:effect
(and
(Flag424-)
(Flag424prime-)
)

)
(:action Flag424Action-55
:parameters ()
:precondition
(and
(Flag357-)
(Flag357prime-)
)
:effect
(and
(Flag424-)
(Flag424prime-)
)

)
(:action Flag424Action-56
:parameters ()
:precondition
(and
(Flag385-)
(Flag385prime-)
)
:effect
(and
(Flag424-)
(Flag424prime-)
)

)
(:action Flag424Action-57
:parameters ()
:precondition
(and
(Flag314-)
(Flag314prime-)
)
:effect
(and
(Flag424-)
(Flag424prime-)
)

)
(:action Flag424Action-58
:parameters ()
:precondition
(and
(Flag410-)
(Flag410prime-)
)
:effect
(and
(Flag424-)
(Flag424prime-)
)

)
(:action Flag424Action-59
:parameters ()
:precondition
(and
(Flag398-)
(Flag398prime-)
)
:effect
(and
(Flag424-)
(Flag424prime-)
)

)
(:action Flag424Action-60
:parameters ()
:precondition
(and
(Flag419-)
(Flag419prime-)
)
:effect
(and
(Flag424-)
(Flag424prime-)
)

)
(:action Flag424Action-61
:parameters ()
:precondition
(and
(Flag297-)
(Flag297prime-)
)
:effect
(and
(Flag424-)
(Flag424prime-)
)

)
(:action Flag424Action-62
:parameters ()
:precondition
(and
(Flag335-)
(Flag335prime-)
)
:effect
(and
(Flag424-)
(Flag424prime-)
)

)
(:action Flag424Action-63
:parameters ()
:precondition
(and
(Flag298-)
(Flag298prime-)
)
:effect
(and
(Flag424-)
(Flag424prime-)
)

)
(:action Flag424Action-64
:parameters ()
:precondition
(and
(Flag371-)
(Flag371prime-)
)
:effect
(and
(Flag424-)
(Flag424prime-)
)

)
(:action Flag424Action-65
:parameters ()
:precondition
(and
(Flag399-)
(Flag399prime-)
)
:effect
(and
(Flag424-)
(Flag424prime-)
)

)
(:action Flag424Action-66
:parameters ()
:precondition
(and
(Flag348-)
(Flag348prime-)
)
:effect
(and
(Flag424-)
(Flag424prime-)
)

)
(:action Flag424Action-67
:parameters ()
:precondition
(and
(Flag373-)
(Flag373prime-)
)
:effect
(and
(Flag424-)
(Flag424prime-)
)

)
(:action Flag424Action-68
:parameters ()
:precondition
(and
(Flag400-)
(Flag400prime-)
)
:effect
(and
(Flag424-)
(Flag424prime-)
)

)
(:action Flag424Action-69
:parameters ()
:precondition
(and
(Flag359-)
(Flag359prime-)
)
:effect
(and
(Flag424-)
(Flag424prime-)
)

)
(:action Flag424Action-70
:parameters ()
:precondition
(and
(Flag299-)
(Flag299prime-)
)
:effect
(and
(Flag424-)
(Flag424prime-)
)

)
(:action Flag424Action-71
:parameters ()
:precondition
(and
(Flag383-)
(Flag383prime-)
)
:effect
(and
(Flag424-)
(Flag424prime-)
)

)
(:action Flag424Action-72
:parameters ()
:precondition
(and
(Flag361-)
(Flag361prime-)
)
:effect
(and
(Flag424-)
(Flag424prime-)
)

)
(:action Flag424Action-73
:parameters ()
:precondition
(and
(Flag420-)
(Flag420prime-)
)
:effect
(and
(Flag424-)
(Flag424prime-)
)

)
(:action Flag424Action-74
:parameters ()
:precondition
(and
(Flag421-)
(Flag421prime-)
)
:effect
(and
(Flag424-)
(Flag424prime-)
)

)
(:action Flag424Action-75
:parameters ()
:precondition
(and
(Flag411-)
(Flag411prime-)
)
:effect
(and
(Flag424-)
(Flag424prime-)
)

)
(:action Flag424Action-76
:parameters ()
:precondition
(and
(Flag327-)
(Flag327prime-)
)
:effect
(and
(Flag424-)
(Flag424prime-)
)

)
(:action Flag424Action-77
:parameters ()
:precondition
(and
(Flag291-)
(Flag291prime-)
)
:effect
(and
(Flag424-)
(Flag424prime-)
)

)
(:action Flag424Action-78
:parameters ()
:precondition
(and
(Flag284-)
(Flag284prime-)
)
:effect
(and
(Flag424-)
(Flag424prime-)
)

)
(:action Flag424Action-79
:parameters ()
:precondition
(and
(Flag286-)
(Flag286prime-)
)
:effect
(and
(Flag424-)
(Flag424prime-)
)

)
(:action Flag424Action-80
:parameters ()
:precondition
(and
(Flag312-)
(Flag312prime-)
)
:effect
(and
(Flag424-)
(Flag424prime-)
)

)
(:action Flag424Action-81
:parameters ()
:precondition
(and
(Flag386-)
(Flag386prime-)
)
:effect
(and
(Flag424-)
(Flag424prime-)
)

)
(:action Flag424Action-82
:parameters ()
:precondition
(and
(Flag387-)
(Flag387prime-)
)
:effect
(and
(Flag424-)
(Flag424prime-)
)

)
(:action Flag424Action-83
:parameters ()
:precondition
(and
(Flag422-)
(Flag422prime-)
)
:effect
(and
(Flag424-)
(Flag424prime-)
)

)
(:action Flag424Action-84
:parameters ()
:precondition
(and
(Flag289-)
(Flag289prime-)
)
:effect
(and
(Flag424-)
(Flag424prime-)
)

)
(:action Flag424Action-85
:parameters ()
:precondition
(and
(Flag412-)
(Flag412prime-)
)
:effect
(and
(Flag424-)
(Flag424prime-)
)

)
(:action Flag424Action-86
:parameters ()
:precondition
(and
(Flag334-)
(Flag334prime-)
)
:effect
(and
(Flag424-)
(Flag424prime-)
)

)
(:action Flag424Action-87
:parameters ()
:precondition
(and
(Flag342-)
(Flag342prime-)
)
:effect
(and
(Flag424-)
(Flag424prime-)
)

)
(:action Flag424Action-88
:parameters ()
:precondition
(and
(Flag321-)
(Flag321prime-)
)
:effect
(and
(Flag424-)
(Flag424prime-)
)

)
(:action Flag424Action-89
:parameters ()
:precondition
(and
(Flag336-)
(Flag336prime-)
)
:effect
(and
(Flag424-)
(Flag424prime-)
)

)
(:action Flag424Action-90
:parameters ()
:precondition
(and
(Flag273-)
(Flag273prime-)
)
:effect
(and
(Flag424-)
(Flag424prime-)
)

)
(:action Flag424Action-91
:parameters ()
:precondition
(and
(Flag423-)
(Flag423prime-)
)
:effect
(and
(Flag424-)
(Flag424prime-)
)

)
(:action Flag424Action-92
:parameters ()
:precondition
(and
(Flag388-)
(Flag388prime-)
)
:effect
(and
(Flag424-)
(Flag424prime-)
)

)
(:action Flag424Action-93
:parameters ()
:precondition
(and
(Flag413-)
(Flag413prime-)
)
:effect
(and
(Flag424-)
(Flag424prime-)
)

)
(:action Prim424Action
:parameters ()
:precondition
(and
(not (Flag278-))
(Flag278prime-)
(not (Flag415-))
(Flag415prime-)
(not (Flag403-))
(Flag403prime-)
(not (Flag362-))
(Flag362prime-)
(not (Flag376-))
(Flag376prime-)
(not (Flag365-))
(Flag365prime-)
(not (Flag390-))
(Flag390prime-)
(not (Flag353-))
(Flag353prime-)
(not (Flag315-))
(Flag315prime-)
(not (Flag354-))
(Flag354prime-)
(not (Flag294-))
(Flag294prime-)
(not (Flag405-))
(Flag405prime-)
(not (Flag391-))
(Flag391prime-)
(not (Flag275-))
(Flag275prime-)
(not (Flag377-))
(Flag377prime-)
(not (Flag322-))
(Flag322prime-)
(not (Flag372-))
(Flag372prime-)
(not (Flag274-))
(Flag274prime-)
(not (Flag326-))
(Flag326prime-)
(not (Flag279-))
(Flag279prime-)
(not (Flag392-))
(Flag392prime-)
(not (Flag349-))
(Flag349prime-)
(not (Flag406-))
(Flag406prime-)
(not (Flag363-))
(Flag363prime-)
(not (Flag331-))
(Flag331prime-)
(not (Flag288-))
(Flag288prime-)
(not (Flag318-))
(Flag318prime-)
(not (Flag350-))
(Flag350prime-)
(not (Flag366-))
(Flag366prime-)
(not (Flag292-))
(Flag292prime-)
(not (Flag407-))
(Flag407prime-)
(not (Flag379-))
(Flag379prime-)
(not (Flag355-))
(Flag355prime-)
(not (Flag408-))
(Flag408prime-)
(not (Flag394-))
(Flag394prime-)
(not (Flag330-))
(Flag330prime-)
(not (Flag381-))
(Flag381prime-)
(not (Flag369-))
(Flag369prime-)
(not (Flag416-))
(Flag416prime-)
(not (Flag340-))
(Flag340prime-)
(not (Flag304-))
(Flag304prime-)
(not (Flag305-))
(Flag305prime-)
(not (Flag323-))
(Flag323prime-)
(not (Flag382-))
(Flag382prime-)
(not (Flag344-))
(Flag344prime-)
(not (Flag345-))
(Flag345prime-)
(not (Flag276-))
(Flag276prime-)
(not (Flag417-))
(Flag417prime-)
(not (Flag310-))
(Flag310prime-)
(not (Flag409-))
(Flag409prime-)
(not (Flag320-))
(Flag320prime-)
(not (Flag395-))
(Flag395prime-)
(not (Flag418-))
(Flag418prime-)
(not (Flag337-))
(Flag337prime-)
(not (Flag397-))
(Flag397prime-)
(not (Flag357-))
(Flag357prime-)
(not (Flag385-))
(Flag385prime-)
(not (Flag314-))
(Flag314prime-)
(not (Flag410-))
(Flag410prime-)
(not (Flag398-))
(Flag398prime-)
(not (Flag419-))
(Flag419prime-)
(not (Flag297-))
(Flag297prime-)
(not (Flag335-))
(Flag335prime-)
(not (Flag298-))
(Flag298prime-)
(not (Flag371-))
(Flag371prime-)
(not (Flag399-))
(Flag399prime-)
(not (Flag348-))
(Flag348prime-)
(not (Flag373-))
(Flag373prime-)
(not (Flag400-))
(Flag400prime-)
(not (Flag359-))
(Flag359prime-)
(not (Flag299-))
(Flag299prime-)
(not (Flag383-))
(Flag383prime-)
(not (Flag361-))
(Flag361prime-)
(not (Flag420-))
(Flag420prime-)
(not (Flag421-))
(Flag421prime-)
(not (Flag411-))
(Flag411prime-)
(not (Flag327-))
(Flag327prime-)
(not (Flag291-))
(Flag291prime-)
(not (Flag284-))
(Flag284prime-)
(not (Flag286-))
(Flag286prime-)
(not (Flag312-))
(Flag312prime-)
(not (Flag386-))
(Flag386prime-)
(not (Flag387-))
(Flag387prime-)
(not (Flag422-))
(Flag422prime-)
(not (Flag289-))
(Flag289prime-)
(not (Flag412-))
(Flag412prime-)
(not (Flag334-))
(Flag334prime-)
(not (Flag342-))
(Flag342prime-)
(not (Flag321-))
(Flag321prime-)
(not (Flag336-))
(Flag336prime-)
(not (Flag273-))
(Flag273prime-)
(not (Flag423-))
(Flag423prime-)
(not (Flag388-))
(Flag388prime-)
(not (Flag413-))
(Flag413prime-)
)
:effect
(and
(Flag424prime-)
(not (Flag424-))
)

)
(:action Flag434Action-0
:parameters ()
:precondition
(and
(Flag278-)
(Flag278prime-)
)
:effect
(and
(Flag434-)
(Flag434prime-)
)

)
(:action Flag434Action-1
:parameters ()
:precondition
(and
(Flag425-)
(Flag425prime-)
)
:effect
(and
(Flag434-)
(Flag434prime-)
)

)
(:action Flag434Action-2
:parameters ()
:precondition
(and
(Flag415-)
(Flag415prime-)
)
:effect
(and
(Flag434-)
(Flag434prime-)
)

)
(:action Flag434Action-3
:parameters ()
:precondition
(and
(Flag403-)
(Flag403prime-)
)
:effect
(and
(Flag434-)
(Flag434prime-)
)

)
(:action Flag434Action-4
:parameters ()
:precondition
(and
(Flag426-)
(Flag426prime-)
)
:effect
(and
(Flag434-)
(Flag434prime-)
)

)
(:action Flag434Action-5
:parameters ()
:precondition
(and
(Flag362-)
(Flag362prime-)
)
:effect
(and
(Flag434-)
(Flag434prime-)
)

)
(:action Flag434Action-6
:parameters ()
:precondition
(and
(Flag376-)
(Flag376prime-)
)
:effect
(and
(Flag434-)
(Flag434prime-)
)

)
(:action Flag434Action-7
:parameters ()
:precondition
(and
(Flag365-)
(Flag365prime-)
)
:effect
(and
(Flag434-)
(Flag434prime-)
)

)
(:action Flag434Action-8
:parameters ()
:precondition
(and
(Flag390-)
(Flag390prime-)
)
:effect
(and
(Flag434-)
(Flag434prime-)
)

)
(:action Flag434Action-9
:parameters ()
:precondition
(and
(Flag353-)
(Flag353prime-)
)
:effect
(and
(Flag434-)
(Flag434prime-)
)

)
(:action Flag434Action-10
:parameters ()
:precondition
(and
(Flag315-)
(Flag315prime-)
)
:effect
(and
(Flag434-)
(Flag434prime-)
)

)
(:action Flag434Action-11
:parameters ()
:precondition
(and
(Flag354-)
(Flag354prime-)
)
:effect
(and
(Flag434-)
(Flag434prime-)
)

)
(:action Flag434Action-12
:parameters ()
:precondition
(and
(Flag294-)
(Flag294prime-)
)
:effect
(and
(Flag434-)
(Flag434prime-)
)

)
(:action Flag434Action-13
:parameters ()
:precondition
(and
(Flag427-)
(Flag427prime-)
)
:effect
(and
(Flag434-)
(Flag434prime-)
)

)
(:action Flag434Action-14
:parameters ()
:precondition
(and
(Flag405-)
(Flag405prime-)
)
:effect
(and
(Flag434-)
(Flag434prime-)
)

)
(:action Flag434Action-15
:parameters ()
:precondition
(and
(Flag391-)
(Flag391prime-)
)
:effect
(and
(Flag434-)
(Flag434prime-)
)

)
(:action Flag434Action-16
:parameters ()
:precondition
(and
(Flag322-)
(Flag322prime-)
)
:effect
(and
(Flag434-)
(Flag434prime-)
)

)
(:action Flag434Action-17
:parameters ()
:precondition
(and
(Flag372-)
(Flag372prime-)
)
:effect
(and
(Flag434-)
(Flag434prime-)
)

)
(:action Flag434Action-18
:parameters ()
:precondition
(and
(Flag274-)
(Flag274prime-)
)
:effect
(and
(Flag434-)
(Flag434prime-)
)

)
(:action Flag434Action-19
:parameters ()
:precondition
(and
(Flag326-)
(Flag326prime-)
)
:effect
(and
(Flag434-)
(Flag434prime-)
)

)
(:action Flag434Action-20
:parameters ()
:precondition
(and
(Flag279-)
(Flag279prime-)
)
:effect
(and
(Flag434-)
(Flag434prime-)
)

)
(:action Flag434Action-21
:parameters ()
:precondition
(and
(Flag392-)
(Flag392prime-)
)
:effect
(and
(Flag434-)
(Flag434prime-)
)

)
(:action Flag434Action-22
:parameters ()
:precondition
(and
(Flag349-)
(Flag349prime-)
)
:effect
(and
(Flag434-)
(Flag434prime-)
)

)
(:action Flag434Action-23
:parameters ()
:precondition
(and
(Flag363-)
(Flag363prime-)
)
:effect
(and
(Flag434-)
(Flag434prime-)
)

)
(:action Flag434Action-24
:parameters ()
:precondition
(and
(Flag331-)
(Flag331prime-)
)
:effect
(and
(Flag434-)
(Flag434prime-)
)

)
(:action Flag434Action-25
:parameters ()
:precondition
(and
(Flag288-)
(Flag288prime-)
)
:effect
(and
(Flag434-)
(Flag434prime-)
)

)
(:action Flag434Action-26
:parameters ()
:precondition
(and
(Flag318-)
(Flag318prime-)
)
:effect
(and
(Flag434-)
(Flag434prime-)
)

)
(:action Flag434Action-27
:parameters ()
:precondition
(and
(Flag350-)
(Flag350prime-)
)
:effect
(and
(Flag434-)
(Flag434prime-)
)

)
(:action Flag434Action-28
:parameters ()
:precondition
(and
(Flag428-)
(Flag428prime-)
)
:effect
(and
(Flag434-)
(Flag434prime-)
)

)
(:action Flag434Action-29
:parameters ()
:precondition
(and
(Flag366-)
(Flag366prime-)
)
:effect
(and
(Flag434-)
(Flag434prime-)
)

)
(:action Flag434Action-30
:parameters ()
:precondition
(and
(Flag292-)
(Flag292prime-)
)
:effect
(and
(Flag434-)
(Flag434prime-)
)

)
(:action Flag434Action-31
:parameters ()
:precondition
(and
(Flag407-)
(Flag407prime-)
)
:effect
(and
(Flag434-)
(Flag434prime-)
)

)
(:action Flag434Action-32
:parameters ()
:precondition
(and
(Flag379-)
(Flag379prime-)
)
:effect
(and
(Flag434-)
(Flag434prime-)
)

)
(:action Flag434Action-33
:parameters ()
:precondition
(and
(Flag355-)
(Flag355prime-)
)
:effect
(and
(Flag434-)
(Flag434prime-)
)

)
(:action Flag434Action-34
:parameters ()
:precondition
(and
(Flag408-)
(Flag408prime-)
)
:effect
(and
(Flag434-)
(Flag434prime-)
)

)
(:action Flag434Action-35
:parameters ()
:precondition
(and
(Flag429-)
(Flag429prime-)
)
:effect
(and
(Flag434-)
(Flag434prime-)
)

)
(:action Flag434Action-36
:parameters ()
:precondition
(and
(Flag394-)
(Flag394prime-)
)
:effect
(and
(Flag434-)
(Flag434prime-)
)

)
(:action Flag434Action-37
:parameters ()
:precondition
(and
(Flag330-)
(Flag330prime-)
)
:effect
(and
(Flag434-)
(Flag434prime-)
)

)
(:action Flag434Action-38
:parameters ()
:precondition
(and
(Flag381-)
(Flag381prime-)
)
:effect
(and
(Flag434-)
(Flag434prime-)
)

)
(:action Flag434Action-39
:parameters ()
:precondition
(and
(Flag369-)
(Flag369prime-)
)
:effect
(and
(Flag434-)
(Flag434prime-)
)

)
(:action Flag434Action-40
:parameters ()
:precondition
(and
(Flag416-)
(Flag416prime-)
)
:effect
(and
(Flag434-)
(Flag434prime-)
)

)
(:action Flag434Action-41
:parameters ()
:precondition
(and
(Flag348-)
(Flag348prime-)
)
:effect
(and
(Flag434-)
(Flag434prime-)
)

)
(:action Flag434Action-42
:parameters ()
:precondition
(and
(Flag340-)
(Flag340prime-)
)
:effect
(and
(Flag434-)
(Flag434prime-)
)

)
(:action Flag434Action-43
:parameters ()
:precondition
(and
(Flag304-)
(Flag304prime-)
)
:effect
(and
(Flag434-)
(Flag434prime-)
)

)
(:action Flag434Action-44
:parameters ()
:precondition
(and
(Flag305-)
(Flag305prime-)
)
:effect
(and
(Flag434-)
(Flag434prime-)
)

)
(:action Flag434Action-45
:parameters ()
:precondition
(and
(Flag382-)
(Flag382prime-)
)
:effect
(and
(Flag434-)
(Flag434prime-)
)

)
(:action Flag434Action-46
:parameters ()
:precondition
(and
(Flag430-)
(Flag430prime-)
)
:effect
(and
(Flag434-)
(Flag434prime-)
)

)
(:action Flag434Action-47
:parameters ()
:precondition
(and
(Flag345-)
(Flag345prime-)
)
:effect
(and
(Flag434-)
(Flag434prime-)
)

)
(:action Flag434Action-48
:parameters ()
:precondition
(and
(Flag417-)
(Flag417prime-)
)
:effect
(and
(Flag434-)
(Flag434prime-)
)

)
(:action Flag434Action-49
:parameters ()
:precondition
(and
(Flag310-)
(Flag310prime-)
)
:effect
(and
(Flag434-)
(Flag434prime-)
)

)
(:action Flag434Action-50
:parameters ()
:precondition
(and
(Flag409-)
(Flag409prime-)
)
:effect
(and
(Flag434-)
(Flag434prime-)
)

)
(:action Flag434Action-51
:parameters ()
:precondition
(and
(Flag320-)
(Flag320prime-)
)
:effect
(and
(Flag434-)
(Flag434prime-)
)

)
(:action Flag434Action-52
:parameters ()
:precondition
(and
(Flag418-)
(Flag418prime-)
)
:effect
(and
(Flag434-)
(Flag434prime-)
)

)
(:action Flag434Action-53
:parameters ()
:precondition
(and
(Flag337-)
(Flag337prime-)
)
:effect
(and
(Flag434-)
(Flag434prime-)
)

)
(:action Flag434Action-54
:parameters ()
:precondition
(and
(Flag397-)
(Flag397prime-)
)
:effect
(and
(Flag434-)
(Flag434prime-)
)

)
(:action Flag434Action-55
:parameters ()
:precondition
(and
(Flag357-)
(Flag357prime-)
)
:effect
(and
(Flag434-)
(Flag434prime-)
)

)
(:action Flag434Action-56
:parameters ()
:precondition
(and
(Flag385-)
(Flag385prime-)
)
:effect
(and
(Flag434-)
(Flag434prime-)
)

)
(:action Flag434Action-57
:parameters ()
:precondition
(and
(Flag314-)
(Flag314prime-)
)
:effect
(and
(Flag434-)
(Flag434prime-)
)

)
(:action Flag434Action-58
:parameters ()
:precondition
(and
(Flag410-)
(Flag410prime-)
)
:effect
(and
(Flag434-)
(Flag434prime-)
)

)
(:action Flag434Action-59
:parameters ()
:precondition
(and
(Flag398-)
(Flag398prime-)
)
:effect
(and
(Flag434-)
(Flag434prime-)
)

)
(:action Flag434Action-60
:parameters ()
:precondition
(and
(Flag297-)
(Flag297prime-)
)
:effect
(and
(Flag434-)
(Flag434prime-)
)

)
(:action Flag434Action-61
:parameters ()
:precondition
(and
(Flag383-)
(Flag383prime-)
)
:effect
(and
(Flag434-)
(Flag434prime-)
)

)
(:action Flag434Action-62
:parameters ()
:precondition
(and
(Flag298-)
(Flag298prime-)
)
:effect
(and
(Flag434-)
(Flag434prime-)
)

)
(:action Flag434Action-63
:parameters ()
:precondition
(and
(Flag371-)
(Flag371prime-)
)
:effect
(and
(Flag434-)
(Flag434prime-)
)

)
(:action Flag434Action-64
:parameters ()
:precondition
(and
(Flag399-)
(Flag399prime-)
)
:effect
(and
(Flag434-)
(Flag434prime-)
)

)
(:action Flag434Action-65
:parameters ()
:precondition
(and
(Flag431-)
(Flag431prime-)
)
:effect
(and
(Flag434-)
(Flag434prime-)
)

)
(:action Flag434Action-66
:parameters ()
:precondition
(and
(Flag400-)
(Flag400prime-)
)
:effect
(and
(Flag434-)
(Flag434prime-)
)

)
(:action Flag434Action-67
:parameters ()
:precondition
(and
(Flag359-)
(Flag359prime-)
)
:effect
(and
(Flag434-)
(Flag434prime-)
)

)
(:action Flag434Action-68
:parameters ()
:precondition
(and
(Flag299-)
(Flag299prime-)
)
:effect
(and
(Flag434-)
(Flag434prime-)
)

)
(:action Flag434Action-69
:parameters ()
:precondition
(and
(Flag361-)
(Flag361prime-)
)
:effect
(and
(Flag434-)
(Flag434prime-)
)

)
(:action Flag434Action-70
:parameters ()
:precondition
(and
(Flag420-)
(Flag420prime-)
)
:effect
(and
(Flag434-)
(Flag434prime-)
)

)
(:action Flag434Action-71
:parameters ()
:precondition
(and
(Flag421-)
(Flag421prime-)
)
:effect
(and
(Flag434-)
(Flag434prime-)
)

)
(:action Flag434Action-72
:parameters ()
:precondition
(and
(Flag411-)
(Flag411prime-)
)
:effect
(and
(Flag434-)
(Flag434prime-)
)

)
(:action Flag434Action-73
:parameters ()
:precondition
(and
(Flag327-)
(Flag327prime-)
)
:effect
(and
(Flag434-)
(Flag434prime-)
)

)
(:action Flag434Action-74
:parameters ()
:precondition
(and
(Flag291-)
(Flag291prime-)
)
:effect
(and
(Flag434-)
(Flag434prime-)
)

)
(:action Flag434Action-75
:parameters ()
:precondition
(and
(Flag284-)
(Flag284prime-)
)
:effect
(and
(Flag434-)
(Flag434prime-)
)

)
(:action Flag434Action-76
:parameters ()
:precondition
(and
(Flag286-)
(Flag286prime-)
)
:effect
(and
(Flag434-)
(Flag434prime-)
)

)
(:action Flag434Action-77
:parameters ()
:precondition
(and
(Flag312-)
(Flag312prime-)
)
:effect
(and
(Flag434-)
(Flag434prime-)
)

)
(:action Flag434Action-78
:parameters ()
:precondition
(and
(Flag386-)
(Flag386prime-)
)
:effect
(and
(Flag434-)
(Flag434prime-)
)

)
(:action Flag434Action-79
:parameters ()
:precondition
(and
(Flag387-)
(Flag387prime-)
)
:effect
(and
(Flag434-)
(Flag434prime-)
)

)
(:action Flag434Action-80
:parameters ()
:precondition
(and
(Flag422-)
(Flag422prime-)
)
:effect
(and
(Flag434-)
(Flag434prime-)
)

)
(:action Flag434Action-81
:parameters ()
:precondition
(and
(Flag289-)
(Flag289prime-)
)
:effect
(and
(Flag434-)
(Flag434prime-)
)

)
(:action Flag434Action-82
:parameters ()
:precondition
(and
(Flag412-)
(Flag412prime-)
)
:effect
(and
(Flag434-)
(Flag434prime-)
)

)
(:action Flag434Action-83
:parameters ()
:precondition
(and
(Flag334-)
(Flag334prime-)
)
:effect
(and
(Flag434-)
(Flag434prime-)
)

)
(:action Flag434Action-84
:parameters ()
:precondition
(and
(Flag342-)
(Flag342prime-)
)
:effect
(and
(Flag434-)
(Flag434prime-)
)

)
(:action Flag434Action-85
:parameters ()
:precondition
(and
(Flag321-)
(Flag321prime-)
)
:effect
(and
(Flag434-)
(Flag434prime-)
)

)
(:action Flag434Action-86
:parameters ()
:precondition
(and
(Flag273-)
(Flag273prime-)
)
:effect
(and
(Flag434-)
(Flag434prime-)
)

)
(:action Flag434Action-87
:parameters ()
:precondition
(and
(Flag423-)
(Flag423prime-)
)
:effect
(and
(Flag434-)
(Flag434prime-)
)

)
(:action Flag434Action-88
:parameters ()
:precondition
(and
(Flag388-)
(Flag388prime-)
)
:effect
(and
(Flag434-)
(Flag434prime-)
)

)
(:action Flag434Action-89
:parameters ()
:precondition
(and
(Flag432-)
(Flag432prime-)
)
:effect
(and
(Flag434-)
(Flag434prime-)
)

)
(:action Flag434Action-90
:parameters ()
:precondition
(and
(Flag433-)
(Flag433prime-)
)
:effect
(and
(Flag434-)
(Flag434prime-)
)

)
(:action Flag434Action-91
:parameters ()
:precondition
(and
(Flag413-)
(Flag413prime-)
)
:effect
(and
(Flag434-)
(Flag434prime-)
)

)
(:action Prim434Action
:parameters ()
:precondition
(and
(not (Flag278-))
(Flag278prime-)
(not (Flag425-))
(Flag425prime-)
(not (Flag415-))
(Flag415prime-)
(not (Flag403-))
(Flag403prime-)
(not (Flag426-))
(Flag426prime-)
(not (Flag362-))
(Flag362prime-)
(not (Flag376-))
(Flag376prime-)
(not (Flag365-))
(Flag365prime-)
(not (Flag390-))
(Flag390prime-)
(not (Flag353-))
(Flag353prime-)
(not (Flag315-))
(Flag315prime-)
(not (Flag354-))
(Flag354prime-)
(not (Flag294-))
(Flag294prime-)
(not (Flag427-))
(Flag427prime-)
(not (Flag405-))
(Flag405prime-)
(not (Flag391-))
(Flag391prime-)
(not (Flag322-))
(Flag322prime-)
(not (Flag372-))
(Flag372prime-)
(not (Flag274-))
(Flag274prime-)
(not (Flag326-))
(Flag326prime-)
(not (Flag279-))
(Flag279prime-)
(not (Flag392-))
(Flag392prime-)
(not (Flag349-))
(Flag349prime-)
(not (Flag363-))
(Flag363prime-)
(not (Flag331-))
(Flag331prime-)
(not (Flag288-))
(Flag288prime-)
(not (Flag318-))
(Flag318prime-)
(not (Flag350-))
(Flag350prime-)
(not (Flag428-))
(Flag428prime-)
(not (Flag366-))
(Flag366prime-)
(not (Flag292-))
(Flag292prime-)
(not (Flag407-))
(Flag407prime-)
(not (Flag379-))
(Flag379prime-)
(not (Flag355-))
(Flag355prime-)
(not (Flag408-))
(Flag408prime-)
(not (Flag429-))
(Flag429prime-)
(not (Flag394-))
(Flag394prime-)
(not (Flag330-))
(Flag330prime-)
(not (Flag381-))
(Flag381prime-)
(not (Flag369-))
(Flag369prime-)
(not (Flag416-))
(Flag416prime-)
(not (Flag348-))
(Flag348prime-)
(not (Flag340-))
(Flag340prime-)
(not (Flag304-))
(Flag304prime-)
(not (Flag305-))
(Flag305prime-)
(not (Flag382-))
(Flag382prime-)
(not (Flag430-))
(Flag430prime-)
(not (Flag345-))
(Flag345prime-)
(not (Flag417-))
(Flag417prime-)
(not (Flag310-))
(Flag310prime-)
(not (Flag409-))
(Flag409prime-)
(not (Flag320-))
(Flag320prime-)
(not (Flag418-))
(Flag418prime-)
(not (Flag337-))
(Flag337prime-)
(not (Flag397-))
(Flag397prime-)
(not (Flag357-))
(Flag357prime-)
(not (Flag385-))
(Flag385prime-)
(not (Flag314-))
(Flag314prime-)
(not (Flag410-))
(Flag410prime-)
(not (Flag398-))
(Flag398prime-)
(not (Flag297-))
(Flag297prime-)
(not (Flag383-))
(Flag383prime-)
(not (Flag298-))
(Flag298prime-)
(not (Flag371-))
(Flag371prime-)
(not (Flag399-))
(Flag399prime-)
(not (Flag431-))
(Flag431prime-)
(not (Flag400-))
(Flag400prime-)
(not (Flag359-))
(Flag359prime-)
(not (Flag299-))
(Flag299prime-)
(not (Flag361-))
(Flag361prime-)
(not (Flag420-))
(Flag420prime-)
(not (Flag421-))
(Flag421prime-)
(not (Flag411-))
(Flag411prime-)
(not (Flag327-))
(Flag327prime-)
(not (Flag291-))
(Flag291prime-)
(not (Flag284-))
(Flag284prime-)
(not (Flag286-))
(Flag286prime-)
(not (Flag312-))
(Flag312prime-)
(not (Flag386-))
(Flag386prime-)
(not (Flag387-))
(Flag387prime-)
(not (Flag422-))
(Flag422prime-)
(not (Flag289-))
(Flag289prime-)
(not (Flag412-))
(Flag412prime-)
(not (Flag334-))
(Flag334prime-)
(not (Flag342-))
(Flag342prime-)
(not (Flag321-))
(Flag321prime-)
(not (Flag273-))
(Flag273prime-)
(not (Flag423-))
(Flag423prime-)
(not (Flag388-))
(Flag388prime-)
(not (Flag432-))
(Flag432prime-)
(not (Flag433-))
(Flag433prime-)
(not (Flag413-))
(Flag413prime-)
)
:effect
(and
(Flag434prime-)
(not (Flag434-))
)

)
(:action Flag442Action-0
:parameters ()
:precondition
(and
(Flag278-)
(Flag278prime-)
)
:effect
(and
(Flag442-)
(Flag442prime-)
)

)
(:action Flag442Action-1
:parameters ()
:precondition
(and
(Flag425-)
(Flag425prime-)
)
:effect
(and
(Flag442-)
(Flag442prime-)
)

)
(:action Flag442Action-2
:parameters ()
:precondition
(and
(Flag415-)
(Flag415prime-)
)
:effect
(and
(Flag442-)
(Flag442prime-)
)

)
(:action Flag442Action-3
:parameters ()
:precondition
(and
(Flag403-)
(Flag403prime-)
)
:effect
(and
(Flag442-)
(Flag442prime-)
)

)
(:action Flag442Action-4
:parameters ()
:precondition
(and
(Flag426-)
(Flag426prime-)
)
:effect
(and
(Flag442-)
(Flag442prime-)
)

)
(:action Flag442Action-5
:parameters ()
:precondition
(and
(Flag362-)
(Flag362prime-)
)
:effect
(and
(Flag442-)
(Flag442prime-)
)

)
(:action Flag442Action-6
:parameters ()
:precondition
(and
(Flag376-)
(Flag376prime-)
)
:effect
(and
(Flag442-)
(Flag442prime-)
)

)
(:action Flag442Action-7
:parameters ()
:precondition
(and
(Flag365-)
(Flag365prime-)
)
:effect
(and
(Flag442-)
(Flag442prime-)
)

)
(:action Flag442Action-8
:parameters ()
:precondition
(and
(Flag435-)
(Flag435prime-)
)
:effect
(and
(Flag442-)
(Flag442prime-)
)

)
(:action Flag442Action-9
:parameters ()
:precondition
(and
(Flag436-)
(Flag436prime-)
)
:effect
(and
(Flag442-)
(Flag442prime-)
)

)
(:action Flag442Action-10
:parameters ()
:precondition
(and
(Flag417-)
(Flag417prime-)
)
:effect
(and
(Flag442-)
(Flag442prime-)
)

)
(:action Flag442Action-11
:parameters ()
:precondition
(and
(Flag294-)
(Flag294prime-)
)
:effect
(and
(Flag442-)
(Flag442prime-)
)

)
(:action Flag442Action-12
:parameters ()
:precondition
(and
(Flag427-)
(Flag427prime-)
)
:effect
(and
(Flag442-)
(Flag442prime-)
)

)
(:action Flag442Action-13
:parameters ()
:precondition
(and
(Flag405-)
(Flag405prime-)
)
:effect
(and
(Flag442-)
(Flag442prime-)
)

)
(:action Flag442Action-14
:parameters ()
:precondition
(and
(Flag391-)
(Flag391prime-)
)
:effect
(and
(Flag442-)
(Flag442prime-)
)

)
(:action Flag442Action-15
:parameters ()
:precondition
(and
(Flag322-)
(Flag322prime-)
)
:effect
(and
(Flag442-)
(Flag442prime-)
)

)
(:action Flag442Action-16
:parameters ()
:precondition
(and
(Flag372-)
(Flag372prime-)
)
:effect
(and
(Flag442-)
(Flag442prime-)
)

)
(:action Flag442Action-17
:parameters ()
:precondition
(and
(Flag274-)
(Flag274prime-)
)
:effect
(and
(Flag442-)
(Flag442prime-)
)

)
(:action Flag442Action-18
:parameters ()
:precondition
(and
(Flag326-)
(Flag326prime-)
)
:effect
(and
(Flag442-)
(Flag442prime-)
)

)
(:action Flag442Action-19
:parameters ()
:precondition
(and
(Flag392-)
(Flag392prime-)
)
:effect
(and
(Flag442-)
(Flag442prime-)
)

)
(:action Flag442Action-20
:parameters ()
:precondition
(and
(Flag349-)
(Flag349prime-)
)
:effect
(and
(Flag442-)
(Flag442prime-)
)

)
(:action Flag442Action-21
:parameters ()
:precondition
(and
(Flag363-)
(Flag363prime-)
)
:effect
(and
(Flag442-)
(Flag442prime-)
)

)
(:action Flag442Action-22
:parameters ()
:precondition
(and
(Flag331-)
(Flag331prime-)
)
:effect
(and
(Flag442-)
(Flag442prime-)
)

)
(:action Flag442Action-23
:parameters ()
:precondition
(and
(Flag288-)
(Flag288prime-)
)
:effect
(and
(Flag442-)
(Flag442prime-)
)

)
(:action Flag442Action-24
:parameters ()
:precondition
(and
(Flag318-)
(Flag318prime-)
)
:effect
(and
(Flag442-)
(Flag442prime-)
)

)
(:action Flag442Action-25
:parameters ()
:precondition
(and
(Flag350-)
(Flag350prime-)
)
:effect
(and
(Flag442-)
(Flag442prime-)
)

)
(:action Flag442Action-26
:parameters ()
:precondition
(and
(Flag428-)
(Flag428prime-)
)
:effect
(and
(Flag442-)
(Flag442prime-)
)

)
(:action Flag442Action-27
:parameters ()
:precondition
(and
(Flag366-)
(Flag366prime-)
)
:effect
(and
(Flag442-)
(Flag442prime-)
)

)
(:action Flag442Action-28
:parameters ()
:precondition
(and
(Flag292-)
(Flag292prime-)
)
:effect
(and
(Flag442-)
(Flag442prime-)
)

)
(:action Flag442Action-29
:parameters ()
:precondition
(and
(Flag407-)
(Flag407prime-)
)
:effect
(and
(Flag442-)
(Flag442prime-)
)

)
(:action Flag442Action-30
:parameters ()
:precondition
(and
(Flag379-)
(Flag379prime-)
)
:effect
(and
(Flag442-)
(Flag442prime-)
)

)
(:action Flag442Action-31
:parameters ()
:precondition
(and
(Flag355-)
(Flag355prime-)
)
:effect
(and
(Flag442-)
(Flag442prime-)
)

)
(:action Flag442Action-32
:parameters ()
:precondition
(and
(Flag408-)
(Flag408prime-)
)
:effect
(and
(Flag442-)
(Flag442prime-)
)

)
(:action Flag442Action-33
:parameters ()
:precondition
(and
(Flag429-)
(Flag429prime-)
)
:effect
(and
(Flag442-)
(Flag442prime-)
)

)
(:action Flag442Action-34
:parameters ()
:precondition
(and
(Flag394-)
(Flag394prime-)
)
:effect
(and
(Flag442-)
(Flag442prime-)
)

)
(:action Flag442Action-35
:parameters ()
:precondition
(and
(Flag330-)
(Flag330prime-)
)
:effect
(and
(Flag442-)
(Flag442prime-)
)

)
(:action Flag442Action-36
:parameters ()
:precondition
(and
(Flag381-)
(Flag381prime-)
)
:effect
(and
(Flag442-)
(Flag442prime-)
)

)
(:action Flag442Action-37
:parameters ()
:precondition
(and
(Flag416-)
(Flag416prime-)
)
:effect
(and
(Flag442-)
(Flag442prime-)
)

)
(:action Flag442Action-38
:parameters ()
:precondition
(and
(Flag348-)
(Flag348prime-)
)
:effect
(and
(Flag442-)
(Flag442prime-)
)

)
(:action Flag442Action-39
:parameters ()
:precondition
(and
(Flag340-)
(Flag340prime-)
)
:effect
(and
(Flag442-)
(Flag442prime-)
)

)
(:action Flag442Action-40
:parameters ()
:precondition
(and
(Flag304-)
(Flag304prime-)
)
:effect
(and
(Flag442-)
(Flag442prime-)
)

)
(:action Flag442Action-41
:parameters ()
:precondition
(and
(Flag305-)
(Flag305prime-)
)
:effect
(and
(Flag442-)
(Flag442prime-)
)

)
(:action Flag442Action-42
:parameters ()
:precondition
(and
(Flag382-)
(Flag382prime-)
)
:effect
(and
(Flag442-)
(Flag442prime-)
)

)
(:action Flag442Action-43
:parameters ()
:precondition
(and
(Flag430-)
(Flag430prime-)
)
:effect
(and
(Flag442-)
(Flag442prime-)
)

)
(:action Flag442Action-44
:parameters ()
:precondition
(and
(Flag345-)
(Flag345prime-)
)
:effect
(and
(Flag442-)
(Flag442prime-)
)

)
(:action Flag442Action-45
:parameters ()
:precondition
(and
(Flag383-)
(Flag383prime-)
)
:effect
(and
(Flag442-)
(Flag442prime-)
)

)
(:action Flag442Action-46
:parameters ()
:precondition
(and
(Flag310-)
(Flag310prime-)
)
:effect
(and
(Flag442-)
(Flag442prime-)
)

)
(:action Flag442Action-47
:parameters ()
:precondition
(and
(Flag409-)
(Flag409prime-)
)
:effect
(and
(Flag442-)
(Flag442prime-)
)

)
(:action Flag442Action-48
:parameters ()
:precondition
(and
(Flag437-)
(Flag437prime-)
)
:effect
(and
(Flag442-)
(Flag442prime-)
)

)
(:action Flag442Action-49
:parameters ()
:precondition
(and
(Flag320-)
(Flag320prime-)
)
:effect
(and
(Flag442-)
(Flag442prime-)
)

)
(:action Flag442Action-50
:parameters ()
:precondition
(and
(Flag418-)
(Flag418prime-)
)
:effect
(and
(Flag442-)
(Flag442prime-)
)

)
(:action Flag442Action-51
:parameters ()
:precondition
(and
(Flag438-)
(Flag438prime-)
)
:effect
(and
(Flag442-)
(Flag442prime-)
)

)
(:action Flag442Action-52
:parameters ()
:precondition
(and
(Flag337-)
(Flag337prime-)
)
:effect
(and
(Flag442-)
(Flag442prime-)
)

)
(:action Flag442Action-53
:parameters ()
:precondition
(and
(Flag397-)
(Flag397prime-)
)
:effect
(and
(Flag442-)
(Flag442prime-)
)

)
(:action Flag442Action-54
:parameters ()
:precondition
(and
(Flag357-)
(Flag357prime-)
)
:effect
(and
(Flag442-)
(Flag442prime-)
)

)
(:action Flag442Action-55
:parameters ()
:precondition
(and
(Flag385-)
(Flag385prime-)
)
:effect
(and
(Flag442-)
(Flag442prime-)
)

)
(:action Flag442Action-56
:parameters ()
:precondition
(and
(Flag314-)
(Flag314prime-)
)
:effect
(and
(Flag442-)
(Flag442prime-)
)

)
(:action Flag442Action-57
:parameters ()
:precondition
(and
(Flag439-)
(Flag439prime-)
)
:effect
(and
(Flag442-)
(Flag442prime-)
)

)
(:action Flag442Action-58
:parameters ()
:precondition
(and
(Flag398-)
(Flag398prime-)
)
:effect
(and
(Flag442-)
(Flag442prime-)
)

)
(:action Flag442Action-59
:parameters ()
:precondition
(and
(Flag297-)
(Flag297prime-)
)
:effect
(and
(Flag442-)
(Flag442prime-)
)

)
(:action Flag442Action-60
:parameters ()
:precondition
(and
(Flag298-)
(Flag298prime-)
)
:effect
(and
(Flag442-)
(Flag442prime-)
)

)
(:action Flag442Action-61
:parameters ()
:precondition
(and
(Flag371-)
(Flag371prime-)
)
:effect
(and
(Flag442-)
(Flag442prime-)
)

)
(:action Flag442Action-62
:parameters ()
:precondition
(and
(Flag399-)
(Flag399prime-)
)
:effect
(and
(Flag442-)
(Flag442prime-)
)

)
(:action Flag442Action-63
:parameters ()
:precondition
(and
(Flag431-)
(Flag431prime-)
)
:effect
(and
(Flag442-)
(Flag442prime-)
)

)
(:action Flag442Action-64
:parameters ()
:precondition
(and
(Flag400-)
(Flag400prime-)
)
:effect
(and
(Flag442-)
(Flag442prime-)
)

)
(:action Flag442Action-65
:parameters ()
:precondition
(and
(Flag359-)
(Flag359prime-)
)
:effect
(and
(Flag442-)
(Flag442prime-)
)

)
(:action Flag442Action-66
:parameters ()
:precondition
(and
(Flag299-)
(Flag299prime-)
)
:effect
(and
(Flag442-)
(Flag442prime-)
)

)
(:action Flag442Action-67
:parameters ()
:precondition
(and
(Flag361-)
(Flag361prime-)
)
:effect
(and
(Flag442-)
(Flag442prime-)
)

)
(:action Flag442Action-68
:parameters ()
:precondition
(and
(Flag420-)
(Flag420prime-)
)
:effect
(and
(Flag442-)
(Flag442prime-)
)

)
(:action Flag442Action-69
:parameters ()
:precondition
(and
(Flag411-)
(Flag411prime-)
)
:effect
(and
(Flag442-)
(Flag442prime-)
)

)
(:action Flag442Action-70
:parameters ()
:precondition
(and
(Flag327-)
(Flag327prime-)
)
:effect
(and
(Flag442-)
(Flag442prime-)
)

)
(:action Flag442Action-71
:parameters ()
:precondition
(and
(Flag284-)
(Flag284prime-)
)
:effect
(and
(Flag442-)
(Flag442prime-)
)

)
(:action Flag442Action-72
:parameters ()
:precondition
(and
(Flag286-)
(Flag286prime-)
)
:effect
(and
(Flag442-)
(Flag442prime-)
)

)
(:action Flag442Action-73
:parameters ()
:precondition
(and
(Flag440-)
(Flag440prime-)
)
:effect
(and
(Flag442-)
(Flag442prime-)
)

)
(:action Flag442Action-74
:parameters ()
:precondition
(and
(Flag312-)
(Flag312prime-)
)
:effect
(and
(Flag442-)
(Flag442prime-)
)

)
(:action Flag442Action-75
:parameters ()
:precondition
(and
(Flag386-)
(Flag386prime-)
)
:effect
(and
(Flag442-)
(Flag442prime-)
)

)
(:action Flag442Action-76
:parameters ()
:precondition
(and
(Flag387-)
(Flag387prime-)
)
:effect
(and
(Flag442-)
(Flag442prime-)
)

)
(:action Flag442Action-77
:parameters ()
:precondition
(and
(Flag422-)
(Flag422prime-)
)
:effect
(and
(Flag442-)
(Flag442prime-)
)

)
(:action Flag442Action-78
:parameters ()
:precondition
(and
(Flag289-)
(Flag289prime-)
)
:effect
(and
(Flag442-)
(Flag442prime-)
)

)
(:action Flag442Action-79
:parameters ()
:precondition
(and
(Flag412-)
(Flag412prime-)
)
:effect
(and
(Flag442-)
(Flag442prime-)
)

)
(:action Flag442Action-80
:parameters ()
:precondition
(and
(Flag334-)
(Flag334prime-)
)
:effect
(and
(Flag442-)
(Flag442prime-)
)

)
(:action Flag442Action-81
:parameters ()
:precondition
(and
(Flag342-)
(Flag342prime-)
)
:effect
(and
(Flag442-)
(Flag442prime-)
)

)
(:action Flag442Action-82
:parameters ()
:precondition
(and
(Flag321-)
(Flag321prime-)
)
:effect
(and
(Flag442-)
(Flag442prime-)
)

)
(:action Flag442Action-83
:parameters ()
:precondition
(and
(Flag441-)
(Flag441prime-)
)
:effect
(and
(Flag442-)
(Flag442prime-)
)

)
(:action Flag442Action-84
:parameters ()
:precondition
(and
(Flag273-)
(Flag273prime-)
)
:effect
(and
(Flag442-)
(Flag442prime-)
)

)
(:action Flag442Action-85
:parameters ()
:precondition
(and
(Flag423-)
(Flag423prime-)
)
:effect
(and
(Flag442-)
(Flag442prime-)
)

)
(:action Flag442Action-86
:parameters ()
:precondition
(and
(Flag432-)
(Flag432prime-)
)
:effect
(and
(Flag442-)
(Flag442prime-)
)

)
(:action Flag442Action-87
:parameters ()
:precondition
(and
(Flag413-)
(Flag413prime-)
)
:effect
(and
(Flag442-)
(Flag442prime-)
)

)
(:action Prim442Action
:parameters ()
:precondition
(and
(not (Flag278-))
(Flag278prime-)
(not (Flag425-))
(Flag425prime-)
(not (Flag415-))
(Flag415prime-)
(not (Flag403-))
(Flag403prime-)
(not (Flag426-))
(Flag426prime-)
(not (Flag362-))
(Flag362prime-)
(not (Flag376-))
(Flag376prime-)
(not (Flag365-))
(Flag365prime-)
(not (Flag435-))
(Flag435prime-)
(not (Flag436-))
(Flag436prime-)
(not (Flag417-))
(Flag417prime-)
(not (Flag294-))
(Flag294prime-)
(not (Flag427-))
(Flag427prime-)
(not (Flag405-))
(Flag405prime-)
(not (Flag391-))
(Flag391prime-)
(not (Flag322-))
(Flag322prime-)
(not (Flag372-))
(Flag372prime-)
(not (Flag274-))
(Flag274prime-)
(not (Flag326-))
(Flag326prime-)
(not (Flag392-))
(Flag392prime-)
(not (Flag349-))
(Flag349prime-)
(not (Flag363-))
(Flag363prime-)
(not (Flag331-))
(Flag331prime-)
(not (Flag288-))
(Flag288prime-)
(not (Flag318-))
(Flag318prime-)
(not (Flag350-))
(Flag350prime-)
(not (Flag428-))
(Flag428prime-)
(not (Flag366-))
(Flag366prime-)
(not (Flag292-))
(Flag292prime-)
(not (Flag407-))
(Flag407prime-)
(not (Flag379-))
(Flag379prime-)
(not (Flag355-))
(Flag355prime-)
(not (Flag408-))
(Flag408prime-)
(not (Flag429-))
(Flag429prime-)
(not (Flag394-))
(Flag394prime-)
(not (Flag330-))
(Flag330prime-)
(not (Flag381-))
(Flag381prime-)
(not (Flag416-))
(Flag416prime-)
(not (Flag348-))
(Flag348prime-)
(not (Flag340-))
(Flag340prime-)
(not (Flag304-))
(Flag304prime-)
(not (Flag305-))
(Flag305prime-)
(not (Flag382-))
(Flag382prime-)
(not (Flag430-))
(Flag430prime-)
(not (Flag345-))
(Flag345prime-)
(not (Flag383-))
(Flag383prime-)
(not (Flag310-))
(Flag310prime-)
(not (Flag409-))
(Flag409prime-)
(not (Flag437-))
(Flag437prime-)
(not (Flag320-))
(Flag320prime-)
(not (Flag418-))
(Flag418prime-)
(not (Flag438-))
(Flag438prime-)
(not (Flag337-))
(Flag337prime-)
(not (Flag397-))
(Flag397prime-)
(not (Flag357-))
(Flag357prime-)
(not (Flag385-))
(Flag385prime-)
(not (Flag314-))
(Flag314prime-)
(not (Flag439-))
(Flag439prime-)
(not (Flag398-))
(Flag398prime-)
(not (Flag297-))
(Flag297prime-)
(not (Flag298-))
(Flag298prime-)
(not (Flag371-))
(Flag371prime-)
(not (Flag399-))
(Flag399prime-)
(not (Flag431-))
(Flag431prime-)
(not (Flag400-))
(Flag400prime-)
(not (Flag359-))
(Flag359prime-)
(not (Flag299-))
(Flag299prime-)
(not (Flag361-))
(Flag361prime-)
(not (Flag420-))
(Flag420prime-)
(not (Flag411-))
(Flag411prime-)
(not (Flag327-))
(Flag327prime-)
(not (Flag284-))
(Flag284prime-)
(not (Flag286-))
(Flag286prime-)
(not (Flag440-))
(Flag440prime-)
(not (Flag312-))
(Flag312prime-)
(not (Flag386-))
(Flag386prime-)
(not (Flag387-))
(Flag387prime-)
(not (Flag422-))
(Flag422prime-)
(not (Flag289-))
(Flag289prime-)
(not (Flag412-))
(Flag412prime-)
(not (Flag334-))
(Flag334prime-)
(not (Flag342-))
(Flag342prime-)
(not (Flag321-))
(Flag321prime-)
(not (Flag441-))
(Flag441prime-)
(not (Flag273-))
(Flag273prime-)
(not (Flag423-))
(Flag423prime-)
(not (Flag432-))
(Flag432prime-)
(not (Flag413-))
(Flag413prime-)
)
:effect
(and
(Flag442prime-)
(not (Flag442-))
)

)
(:action Flag450Action-0
:parameters ()
:precondition
(and
(Flag430-)
(Flag430prime-)
)
:effect
(and
(Flag450-)
(Flag450prime-)
)

)
(:action Flag450Action-1
:parameters ()
:precondition
(and
(Flag345-)
(Flag345prime-)
)
:effect
(and
(Flag450-)
(Flag450prime-)
)

)
(:action Flag450Action-2
:parameters ()
:precondition
(and
(Flag274-)
(Flag274prime-)
)
:effect
(and
(Flag450-)
(Flag450prime-)
)

)
(:action Flag450Action-3
:parameters ()
:precondition
(and
(Flag359-)
(Flag359prime-)
)
:effect
(and
(Flag450-)
(Flag450prime-)
)

)
(:action Flag450Action-4
:parameters ()
:precondition
(and
(Flag361-)
(Flag361prime-)
)
:effect
(and
(Flag450-)
(Flag450prime-)
)

)
(:action Flag450Action-5
:parameters ()
:precondition
(and
(Flag278-)
(Flag278prime-)
)
:effect
(and
(Flag450-)
(Flag450prime-)
)

)
(:action Flag450Action-6
:parameters ()
:precondition
(and
(Flag417-)
(Flag417prime-)
)
:effect
(and
(Flag450-)
(Flag450prime-)
)

)
(:action Flag450Action-7
:parameters ()
:precondition
(and
(Flag310-)
(Flag310prime-)
)
:effect
(and
(Flag450-)
(Flag450prime-)
)

)
(:action Flag450Action-8
:parameters ()
:precondition
(and
(Flag425-)
(Flag425prime-)
)
:effect
(and
(Flag450-)
(Flag450prime-)
)

)
(:action Flag450Action-9
:parameters ()
:precondition
(and
(Flag409-)
(Flag409prime-)
)
:effect
(and
(Flag450-)
(Flag450prime-)
)

)
(:action Flag450Action-10
:parameters ()
:precondition
(and
(Flag411-)
(Flag411prime-)
)
:effect
(and
(Flag450-)
(Flag450prime-)
)

)
(:action Flag450Action-11
:parameters ()
:precondition
(and
(Flag327-)
(Flag327prime-)
)
:effect
(and
(Flag450-)
(Flag450prime-)
)

)
(:action Flag450Action-12
:parameters ()
:precondition
(and
(Flag415-)
(Flag415prime-)
)
:effect
(and
(Flag450-)
(Flag450prime-)
)

)
(:action Flag450Action-13
:parameters ()
:precondition
(and
(Flag418-)
(Flag418prime-)
)
:effect
(and
(Flag450-)
(Flag450prime-)
)

)
(:action Flag450Action-14
:parameters ()
:precondition
(and
(Flag292-)
(Flag292prime-)
)
:effect
(and
(Flag450-)
(Flag450prime-)
)

)
(:action Flag450Action-15
:parameters ()
:precondition
(and
(Flag403-)
(Flag403prime-)
)
:effect
(and
(Flag450-)
(Flag450prime-)
)

)
(:action Flag450Action-16
:parameters ()
:precondition
(and
(Flag443-)
(Flag443prime-)
)
:effect
(and
(Flag450-)
(Flag450prime-)
)

)
(:action Flag450Action-17
:parameters ()
:precondition
(and
(Flag440-)
(Flag440prime-)
)
:effect
(and
(Flag450-)
(Flag450prime-)
)

)
(:action Flag450Action-18
:parameters ()
:precondition
(and
(Flag426-)
(Flag426prime-)
)
:effect
(and
(Flag450-)
(Flag450prime-)
)

)
(:action Flag450Action-19
:parameters ()
:precondition
(and
(Flag438-)
(Flag438prime-)
)
:effect
(and
(Flag450-)
(Flag450prime-)
)

)
(:action Flag450Action-20
:parameters ()
:precondition
(and
(Flag337-)
(Flag337prime-)
)
:effect
(and
(Flag450-)
(Flag450prime-)
)

)
(:action Flag450Action-21
:parameters ()
:precondition
(and
(Flag397-)
(Flag397prime-)
)
:effect
(and
(Flag450-)
(Flag450prime-)
)

)
(:action Flag450Action-22
:parameters ()
:precondition
(and
(Flag357-)
(Flag357prime-)
)
:effect
(and
(Flag450-)
(Flag450prime-)
)

)
(:action Flag450Action-23
:parameters ()
:precondition
(and
(Flag362-)
(Flag362prime-)
)
:effect
(and
(Flag450-)
(Flag450prime-)
)

)
(:action Flag450Action-24
:parameters ()
:precondition
(and
(Flag385-)
(Flag385prime-)
)
:effect
(and
(Flag450-)
(Flag450prime-)
)

)
(:action Flag450Action-25
:parameters ()
:precondition
(and
(Flag376-)
(Flag376prime-)
)
:effect
(and
(Flag450-)
(Flag450prime-)
)

)
(:action Flag450Action-26
:parameters ()
:precondition
(and
(Flag386-)
(Flag386prime-)
)
:effect
(and
(Flag450-)
(Flag450prime-)
)

)
(:action Flag450Action-27
:parameters ()
:precondition
(and
(Flag284-)
(Flag284prime-)
)
:effect
(and
(Flag450-)
(Flag450prime-)
)

)
(:action Flag450Action-28
:parameters ()
:precondition
(and
(Flag330-)
(Flag330prime-)
)
:effect
(and
(Flag450-)
(Flag450prime-)
)

)
(:action Flag450Action-29
:parameters ()
:precondition
(and
(Flag305-)
(Flag305prime-)
)
:effect
(and
(Flag450-)
(Flag450prime-)
)

)
(:action Flag450Action-30
:parameters ()
:precondition
(and
(Flag331-)
(Flag331prime-)
)
:effect
(and
(Flag450-)
(Flag450prime-)
)

)
(:action Flag450Action-31
:parameters ()
:precondition
(and
(Flag288-)
(Flag288prime-)
)
:effect
(and
(Flag450-)
(Flag450prime-)
)

)
(:action Flag450Action-32
:parameters ()
:precondition
(and
(Flag318-)
(Flag318prime-)
)
:effect
(and
(Flag450-)
(Flag450prime-)
)

)
(:action Flag450Action-33
:parameters ()
:precondition
(and
(Flag439-)
(Flag439prime-)
)
:effect
(and
(Flag450-)
(Flag450prime-)
)

)
(:action Flag450Action-34
:parameters ()
:precondition
(and
(Flag435-)
(Flag435prime-)
)
:effect
(and
(Flag450-)
(Flag450prime-)
)

)
(:action Flag450Action-35
:parameters ()
:precondition
(and
(Flag428-)
(Flag428prime-)
)
:effect
(and
(Flag450-)
(Flag450prime-)
)

)
(:action Flag450Action-36
:parameters ()
:precondition
(and
(Flag366-)
(Flag366prime-)
)
:effect
(and
(Flag450-)
(Flag450prime-)
)

)
(:action Flag450Action-37
:parameters ()
:precondition
(and
(Flag391-)
(Flag391prime-)
)
:effect
(and
(Flag450-)
(Flag450prime-)
)

)
(:action Flag450Action-38
:parameters ()
:precondition
(and
(Flag398-)
(Flag398prime-)
)
:effect
(and
(Flag450-)
(Flag450prime-)
)

)
(:action Flag450Action-39
:parameters ()
:precondition
(and
(Flag392-)
(Flag392prime-)
)
:effect
(and
(Flag450-)
(Flag450prime-)
)

)
(:action Flag450Action-40
:parameters ()
:precondition
(and
(Flag312-)
(Flag312prime-)
)
:effect
(and
(Flag450-)
(Flag450prime-)
)

)
(:action Flag450Action-41
:parameters ()
:precondition
(and
(Flag379-)
(Flag379prime-)
)
:effect
(and
(Flag450-)
(Flag450prime-)
)

)
(:action Flag450Action-42
:parameters ()
:precondition
(and
(Flag407-)
(Flag407prime-)
)
:effect
(and
(Flag450-)
(Flag450prime-)
)

)
(:action Flag450Action-43
:parameters ()
:precondition
(and
(Flag294-)
(Flag294prime-)
)
:effect
(and
(Flag450-)
(Flag450prime-)
)

)
(:action Flag450Action-44
:parameters ()
:precondition
(and
(Flag444-)
(Flag444prime-)
)
:effect
(and
(Flag450-)
(Flag450prime-)
)

)
(:action Flag450Action-45
:parameters ()
:precondition
(and
(Flag355-)
(Flag355prime-)
)
:effect
(and
(Flag450-)
(Flag450prime-)
)

)
(:action Flag450Action-46
:parameters ()
:precondition
(and
(Flag405-)
(Flag405prime-)
)
:effect
(and
(Flag450-)
(Flag450prime-)
)

)
(:action Flag450Action-47
:parameters ()
:precondition
(and
(Flag445-)
(Flag445prime-)
)
:effect
(and
(Flag450-)
(Flag450prime-)
)

)
(:action Flag450Action-48
:parameters ()
:precondition
(and
(Flag422-)
(Flag422prime-)
)
:effect
(and
(Flag450-)
(Flag450prime-)
)

)
(:action Flag450Action-49
:parameters ()
:precondition
(and
(Flag429-)
(Flag429prime-)
)
:effect
(and
(Flag450-)
(Flag450prime-)
)

)
(:action Flag450Action-50
:parameters ()
:precondition
(and
(Flag394-)
(Flag394prime-)
)
:effect
(and
(Flag450-)
(Flag450prime-)
)

)
(:action Flag450Action-51
:parameters ()
:precondition
(and
(Flag314-)
(Flag314prime-)
)
:effect
(and
(Flag450-)
(Flag450prime-)
)

)
(:action Flag450Action-52
:parameters ()
:precondition
(and
(Flag289-)
(Flag289prime-)
)
:effect
(and
(Flag450-)
(Flag450prime-)
)

)
(:action Flag450Action-53
:parameters ()
:precondition
(and
(Flag297-)
(Flag297prime-)
)
:effect
(and
(Flag450-)
(Flag450prime-)
)

)
(:action Flag450Action-54
:parameters ()
:precondition
(and
(Flag412-)
(Flag412prime-)
)
:effect
(and
(Flag450-)
(Flag450prime-)
)

)
(:action Flag450Action-55
:parameters ()
:precondition
(and
(Flag334-)
(Flag334prime-)
)
:effect
(and
(Flag450-)
(Flag450prime-)
)

)
(:action Flag450Action-56
:parameters ()
:precondition
(and
(Flag350-)
(Flag350prime-)
)
:effect
(and
(Flag450-)
(Flag450prime-)
)

)
(:action Flag450Action-57
:parameters ()
:precondition
(and
(Flag321-)
(Flag321prime-)
)
:effect
(and
(Flag450-)
(Flag450prime-)
)

)
(:action Flag450Action-58
:parameters ()
:precondition
(and
(Flag387-)
(Flag387prime-)
)
:effect
(and
(Flag450-)
(Flag450prime-)
)

)
(:action Flag450Action-59
:parameters ()
:precondition
(and
(Flag381-)
(Flag381prime-)
)
:effect
(and
(Flag450-)
(Flag450prime-)
)

)
(:action Flag450Action-60
:parameters ()
:precondition
(and
(Flag322-)
(Flag322prime-)
)
:effect
(and
(Flag450-)
(Flag450prime-)
)

)
(:action Flag450Action-61
:parameters ()
:precondition
(and
(Flag273-)
(Flag273prime-)
)
:effect
(and
(Flag450-)
(Flag450prime-)
)

)
(:action Flag450Action-62
:parameters ()
:precondition
(and
(Flag416-)
(Flag416prime-)
)
:effect
(and
(Flag450-)
(Flag450prime-)
)

)
(:action Flag450Action-63
:parameters ()
:precondition
(and
(Flag441-)
(Flag441prime-)
)
:effect
(and
(Flag450-)
(Flag450prime-)
)

)
(:action Flag450Action-64
:parameters ()
:precondition
(and
(Flag446-)
(Flag446prime-)
)
:effect
(and
(Flag450-)
(Flag450prime-)
)

)
(:action Flag450Action-65
:parameters ()
:precondition
(and
(Flag298-)
(Flag298prime-)
)
:effect
(and
(Flag450-)
(Flag450prime-)
)

)
(:action Flag450Action-66
:parameters ()
:precondition
(and
(Flag371-)
(Flag371prime-)
)
:effect
(and
(Flag450-)
(Flag450prime-)
)

)
(:action Flag450Action-67
:parameters ()
:precondition
(and
(Flag423-)
(Flag423prime-)
)
:effect
(and
(Flag450-)
(Flag450prime-)
)

)
(:action Flag450Action-68
:parameters ()
:precondition
(and
(Flag447-)
(Flag447prime-)
)
:effect
(and
(Flag450-)
(Flag450prime-)
)

)
(:action Flag450Action-69
:parameters ()
:precondition
(and
(Flag432-)
(Flag432prime-)
)
:effect
(and
(Flag450-)
(Flag450prime-)
)

)
(:action Flag450Action-70
:parameters ()
:precondition
(and
(Flag340-)
(Flag340prime-)
)
:effect
(and
(Flag450-)
(Flag450prime-)
)

)
(:action Flag450Action-71
:parameters ()
:precondition
(and
(Flag448-)
(Flag448prime-)
)
:effect
(and
(Flag450-)
(Flag450prime-)
)

)
(:action Flag450Action-72
:parameters ()
:precondition
(and
(Flag326-)
(Flag326prime-)
)
:effect
(and
(Flag450-)
(Flag450prime-)
)

)
(:action Flag450Action-73
:parameters ()
:precondition
(and
(Flag449-)
(Flag449prime-)
)
:effect
(and
(Flag450-)
(Flag450prime-)
)

)
(:action Flag450Action-74
:parameters ()
:precondition
(and
(Flag304-)
(Flag304prime-)
)
:effect
(and
(Flag450-)
(Flag450prime-)
)

)
(:action Flag450Action-75
:parameters ()
:precondition
(and
(Flag413-)
(Flag413prime-)
)
:effect
(and
(Flag450-)
(Flag450prime-)
)

)
(:action Flag450Action-76
:parameters ()
:precondition
(and
(Flag349-)
(Flag349prime-)
)
:effect
(and
(Flag450-)
(Flag450prime-)
)

)
(:action Flag450Action-77
:parameters ()
:precondition
(and
(Flag431-)
(Flag431prime-)
)
:effect
(and
(Flag450-)
(Flag450prime-)
)

)
(:action Flag450Action-78
:parameters ()
:precondition
(and
(Flag372-)
(Flag372prime-)
)
:effect
(and
(Flag450-)
(Flag450prime-)
)

)
(:action Flag450Action-79
:parameters ()
:precondition
(and
(Flag400-)
(Flag400prime-)
)
:effect
(and
(Flag450-)
(Flag450prime-)
)

)
(:action Flag450Action-80
:parameters ()
:precondition
(and
(Flag382-)
(Flag382prime-)
)
:effect
(and
(Flag450-)
(Flag450prime-)
)

)
(:action Prim450Action
:parameters ()
:precondition
(and
(not (Flag430-))
(Flag430prime-)
(not (Flag345-))
(Flag345prime-)
(not (Flag274-))
(Flag274prime-)
(not (Flag359-))
(Flag359prime-)
(not (Flag361-))
(Flag361prime-)
(not (Flag278-))
(Flag278prime-)
(not (Flag417-))
(Flag417prime-)
(not (Flag310-))
(Flag310prime-)
(not (Flag425-))
(Flag425prime-)
(not (Flag409-))
(Flag409prime-)
(not (Flag411-))
(Flag411prime-)
(not (Flag327-))
(Flag327prime-)
(not (Flag415-))
(Flag415prime-)
(not (Flag418-))
(Flag418prime-)
(not (Flag292-))
(Flag292prime-)
(not (Flag403-))
(Flag403prime-)
(not (Flag443-))
(Flag443prime-)
(not (Flag440-))
(Flag440prime-)
(not (Flag426-))
(Flag426prime-)
(not (Flag438-))
(Flag438prime-)
(not (Flag337-))
(Flag337prime-)
(not (Flag397-))
(Flag397prime-)
(not (Flag357-))
(Flag357prime-)
(not (Flag362-))
(Flag362prime-)
(not (Flag385-))
(Flag385prime-)
(not (Flag376-))
(Flag376prime-)
(not (Flag386-))
(Flag386prime-)
(not (Flag284-))
(Flag284prime-)
(not (Flag330-))
(Flag330prime-)
(not (Flag305-))
(Flag305prime-)
(not (Flag331-))
(Flag331prime-)
(not (Flag288-))
(Flag288prime-)
(not (Flag318-))
(Flag318prime-)
(not (Flag439-))
(Flag439prime-)
(not (Flag435-))
(Flag435prime-)
(not (Flag428-))
(Flag428prime-)
(not (Flag366-))
(Flag366prime-)
(not (Flag391-))
(Flag391prime-)
(not (Flag398-))
(Flag398prime-)
(not (Flag392-))
(Flag392prime-)
(not (Flag312-))
(Flag312prime-)
(not (Flag379-))
(Flag379prime-)
(not (Flag407-))
(Flag407prime-)
(not (Flag294-))
(Flag294prime-)
(not (Flag444-))
(Flag444prime-)
(not (Flag355-))
(Flag355prime-)
(not (Flag405-))
(Flag405prime-)
(not (Flag445-))
(Flag445prime-)
(not (Flag422-))
(Flag422prime-)
(not (Flag429-))
(Flag429prime-)
(not (Flag394-))
(Flag394prime-)
(not (Flag314-))
(Flag314prime-)
(not (Flag289-))
(Flag289prime-)
(not (Flag297-))
(Flag297prime-)
(not (Flag412-))
(Flag412prime-)
(not (Flag334-))
(Flag334prime-)
(not (Flag350-))
(Flag350prime-)
(not (Flag321-))
(Flag321prime-)
(not (Flag387-))
(Flag387prime-)
(not (Flag381-))
(Flag381prime-)
(not (Flag322-))
(Flag322prime-)
(not (Flag273-))
(Flag273prime-)
(not (Flag416-))
(Flag416prime-)
(not (Flag441-))
(Flag441prime-)
(not (Flag446-))
(Flag446prime-)
(not (Flag298-))
(Flag298prime-)
(not (Flag371-))
(Flag371prime-)
(not (Flag423-))
(Flag423prime-)
(not (Flag447-))
(Flag447prime-)
(not (Flag432-))
(Flag432prime-)
(not (Flag340-))
(Flag340prime-)
(not (Flag448-))
(Flag448prime-)
(not (Flag326-))
(Flag326prime-)
(not (Flag449-))
(Flag449prime-)
(not (Flag304-))
(Flag304prime-)
(not (Flag413-))
(Flag413prime-)
(not (Flag349-))
(Flag349prime-)
(not (Flag431-))
(Flag431prime-)
(not (Flag372-))
(Flag372prime-)
(not (Flag400-))
(Flag400prime-)
(not (Flag382-))
(Flag382prime-)
)
:effect
(and
(Flag450prime-)
(not (Flag450-))
)

)
(:action Flag456Action-0
:parameters ()
:precondition
(and
(Flag273-)
(Flag273prime-)
)
:effect
(and
(Flag456-)
(Flag456prime-)
)

)
(:action Flag456Action-1
:parameters ()
:precondition
(and
(Flag345-)
(Flag345prime-)
)
:effect
(and
(Flag456-)
(Flag456prime-)
)

)
(:action Flag456Action-2
:parameters ()
:precondition
(and
(Flag274-)
(Flag274prime-)
)
:effect
(and
(Flag456-)
(Flag456prime-)
)

)
(:action Flag456Action-3
:parameters ()
:precondition
(and
(Flag430-)
(Flag430prime-)
)
:effect
(and
(Flag456-)
(Flag456prime-)
)

)
(:action Flag456Action-4
:parameters ()
:precondition
(and
(Flag359-)
(Flag359prime-)
)
:effect
(and
(Flag456-)
(Flag456prime-)
)

)
(:action Flag456Action-5
:parameters ()
:precondition
(and
(Flag361-)
(Flag361prime-)
)
:effect
(and
(Flag456-)
(Flag456prime-)
)

)
(:action Flag456Action-6
:parameters ()
:precondition
(and
(Flag278-)
(Flag278prime-)
)
:effect
(and
(Flag456-)
(Flag456prime-)
)

)
(:action Flag456Action-7
:parameters ()
:precondition
(and
(Flag451-)
(Flag451prime-)
)
:effect
(and
(Flag456-)
(Flag456prime-)
)

)
(:action Flag456Action-8
:parameters ()
:precondition
(and
(Flag310-)
(Flag310prime-)
)
:effect
(and
(Flag456-)
(Flag456prime-)
)

)
(:action Flag456Action-9
:parameters ()
:precondition
(and
(Flag425-)
(Flag425prime-)
)
:effect
(and
(Flag456-)
(Flag456prime-)
)

)
(:action Flag456Action-10
:parameters ()
:precondition
(and
(Flag409-)
(Flag409prime-)
)
:effect
(and
(Flag456-)
(Flag456prime-)
)

)
(:action Flag456Action-11
:parameters ()
:precondition
(and
(Flag411-)
(Flag411prime-)
)
:effect
(and
(Flag456-)
(Flag456prime-)
)

)
(:action Flag456Action-12
:parameters ()
:precondition
(and
(Flag327-)
(Flag327prime-)
)
:effect
(and
(Flag456-)
(Flag456prime-)
)

)
(:action Flag456Action-13
:parameters ()
:precondition
(and
(Flag418-)
(Flag418prime-)
)
:effect
(and
(Flag456-)
(Flag456prime-)
)

)
(:action Flag456Action-14
:parameters ()
:precondition
(and
(Flag403-)
(Flag403prime-)
)
:effect
(and
(Flag456-)
(Flag456prime-)
)

)
(:action Flag456Action-15
:parameters ()
:precondition
(and
(Flag443-)
(Flag443prime-)
)
:effect
(and
(Flag456-)
(Flag456prime-)
)

)
(:action Flag456Action-16
:parameters ()
:precondition
(and
(Flag440-)
(Flag440prime-)
)
:effect
(and
(Flag456-)
(Flag456prime-)
)

)
(:action Flag456Action-17
:parameters ()
:precondition
(and
(Flag426-)
(Flag426prime-)
)
:effect
(and
(Flag456-)
(Flag456prime-)
)

)
(:action Flag456Action-18
:parameters ()
:precondition
(and
(Flag438-)
(Flag438prime-)
)
:effect
(and
(Flag456-)
(Flag456prime-)
)

)
(:action Flag456Action-19
:parameters ()
:precondition
(and
(Flag337-)
(Flag337prime-)
)
:effect
(and
(Flag456-)
(Flag456prime-)
)

)
(:action Flag456Action-20
:parameters ()
:precondition
(and
(Flag397-)
(Flag397prime-)
)
:effect
(and
(Flag456-)
(Flag456prime-)
)

)
(:action Flag456Action-21
:parameters ()
:precondition
(and
(Flag357-)
(Flag357prime-)
)
:effect
(and
(Flag456-)
(Flag456prime-)
)

)
(:action Flag456Action-22
:parameters ()
:precondition
(and
(Flag362-)
(Flag362prime-)
)
:effect
(and
(Flag456-)
(Flag456prime-)
)

)
(:action Flag456Action-23
:parameters ()
:precondition
(and
(Flag385-)
(Flag385prime-)
)
:effect
(and
(Flag456-)
(Flag456prime-)
)

)
(:action Flag456Action-24
:parameters ()
:precondition
(and
(Flag376-)
(Flag376prime-)
)
:effect
(and
(Flag456-)
(Flag456prime-)
)

)
(:action Flag456Action-25
:parameters ()
:precondition
(and
(Flag284-)
(Flag284prime-)
)
:effect
(and
(Flag456-)
(Flag456prime-)
)

)
(:action Flag456Action-26
:parameters ()
:precondition
(and
(Flag305-)
(Flag305prime-)
)
:effect
(and
(Flag456-)
(Flag456prime-)
)

)
(:action Flag456Action-27
:parameters ()
:precondition
(and
(Flag331-)
(Flag331prime-)
)
:effect
(and
(Flag456-)
(Flag456prime-)
)

)
(:action Flag456Action-28
:parameters ()
:precondition
(and
(Flag288-)
(Flag288prime-)
)
:effect
(and
(Flag456-)
(Flag456prime-)
)

)
(:action Flag456Action-29
:parameters ()
:precondition
(and
(Flag318-)
(Flag318prime-)
)
:effect
(and
(Flag456-)
(Flag456prime-)
)

)
(:action Flag456Action-30
:parameters ()
:precondition
(and
(Flag439-)
(Flag439prime-)
)
:effect
(and
(Flag456-)
(Flag456prime-)
)

)
(:action Flag456Action-31
:parameters ()
:precondition
(and
(Flag435-)
(Flag435prime-)
)
:effect
(and
(Flag456-)
(Flag456prime-)
)

)
(:action Flag456Action-32
:parameters ()
:precondition
(and
(Flag428-)
(Flag428prime-)
)
:effect
(and
(Flag456-)
(Flag456prime-)
)

)
(:action Flag456Action-33
:parameters ()
:precondition
(and
(Flag366-)
(Flag366prime-)
)
:effect
(and
(Flag456-)
(Flag456prime-)
)

)
(:action Flag456Action-34
:parameters ()
:precondition
(and
(Flag452-)
(Flag452prime-)
)
:effect
(and
(Flag456-)
(Flag456prime-)
)

)
(:action Flag456Action-35
:parameters ()
:precondition
(and
(Flag312-)
(Flag312prime-)
)
:effect
(and
(Flag456-)
(Flag456prime-)
)

)
(:action Flag456Action-36
:parameters ()
:precondition
(and
(Flag417-)
(Flag417prime-)
)
:effect
(and
(Flag456-)
(Flag456prime-)
)

)
(:action Flag456Action-37
:parameters ()
:precondition
(and
(Flag407-)
(Flag407prime-)
)
:effect
(and
(Flag456-)
(Flag456prime-)
)

)
(:action Flag456Action-38
:parameters ()
:precondition
(and
(Flag349-)
(Flag349prime-)
)
:effect
(and
(Flag456-)
(Flag456prime-)
)

)
(:action Flag456Action-39
:parameters ()
:precondition
(and
(Flag386-)
(Flag386prime-)
)
:effect
(and
(Flag456-)
(Flag456prime-)
)

)
(:action Flag456Action-40
:parameters ()
:precondition
(and
(Flag445-)
(Flag445prime-)
)
:effect
(and
(Flag456-)
(Flag456prime-)
)

)
(:action Flag456Action-41
:parameters ()
:precondition
(and
(Flag453-)
(Flag453prime-)
)
:effect
(and
(Flag456-)
(Flag456prime-)
)

)
(:action Flag456Action-42
:parameters ()
:precondition
(and
(Flag429-)
(Flag429prime-)
)
:effect
(and
(Flag456-)
(Flag456prime-)
)

)
(:action Flag456Action-43
:parameters ()
:precondition
(and
(Flag394-)
(Flag394prime-)
)
:effect
(and
(Flag456-)
(Flag456prime-)
)

)
(:action Flag456Action-44
:parameters ()
:precondition
(and
(Flag391-)
(Flag391prime-)
)
:effect
(and
(Flag456-)
(Flag456prime-)
)

)
(:action Flag456Action-45
:parameters ()
:precondition
(and
(Flag289-)
(Flag289prime-)
)
:effect
(and
(Flag456-)
(Flag456prime-)
)

)
(:action Flag456Action-46
:parameters ()
:precondition
(and
(Flag297-)
(Flag297prime-)
)
:effect
(and
(Flag456-)
(Flag456prime-)
)

)
(:action Flag456Action-47
:parameters ()
:precondition
(and
(Flag412-)
(Flag412prime-)
)
:effect
(and
(Flag456-)
(Flag456prime-)
)

)
(:action Flag456Action-48
:parameters ()
:precondition
(and
(Flag350-)
(Flag350prime-)
)
:effect
(and
(Flag456-)
(Flag456prime-)
)

)
(:action Flag456Action-49
:parameters ()
:precondition
(and
(Flag330-)
(Flag330prime-)
)
:effect
(and
(Flag456-)
(Flag456prime-)
)

)
(:action Flag456Action-50
:parameters ()
:precondition
(and
(Flag387-)
(Flag387prime-)
)
:effect
(and
(Flag456-)
(Flag456prime-)
)

)
(:action Flag456Action-51
:parameters ()
:precondition
(and
(Flag371-)
(Flag371prime-)
)
:effect
(and
(Flag456-)
(Flag456prime-)
)

)
(:action Flag456Action-52
:parameters ()
:precondition
(and
(Flag322-)
(Flag322prime-)
)
:effect
(and
(Flag456-)
(Flag456prime-)
)

)
(:action Flag456Action-53
:parameters ()
:precondition
(and
(Flag422-)
(Flag422prime-)
)
:effect
(and
(Flag456-)
(Flag456prime-)
)

)
(:action Flag456Action-54
:parameters ()
:precondition
(and
(Flag416-)
(Flag416prime-)
)
:effect
(and
(Flag456-)
(Flag456prime-)
)

)
(:action Flag456Action-55
:parameters ()
:precondition
(and
(Flag441-)
(Flag441prime-)
)
:effect
(and
(Flag456-)
(Flag456prime-)
)

)
(:action Flag456Action-56
:parameters ()
:precondition
(and
(Flag446-)
(Flag446prime-)
)
:effect
(and
(Flag456-)
(Flag456prime-)
)

)
(:action Flag456Action-57
:parameters ()
:precondition
(and
(Flag298-)
(Flag298prime-)
)
:effect
(and
(Flag456-)
(Flag456prime-)
)

)
(:action Flag456Action-58
:parameters ()
:precondition
(and
(Flag454-)
(Flag454prime-)
)
:effect
(and
(Flag456-)
(Flag456prime-)
)

)
(:action Flag456Action-59
:parameters ()
:precondition
(and
(Flag423-)
(Flag423prime-)
)
:effect
(and
(Flag456-)
(Flag456prime-)
)

)
(:action Flag456Action-60
:parameters ()
:precondition
(and
(Flag447-)
(Flag447prime-)
)
:effect
(and
(Flag456-)
(Flag456prime-)
)

)
(:action Flag456Action-61
:parameters ()
:precondition
(and
(Flag392-)
(Flag392prime-)
)
:effect
(and
(Flag456-)
(Flag456prime-)
)

)
(:action Flag456Action-62
:parameters ()
:precondition
(and
(Flag455-)
(Flag455prime-)
)
:effect
(and
(Flag456-)
(Flag456prime-)
)

)
(:action Flag456Action-63
:parameters ()
:precondition
(and
(Flag432-)
(Flag432prime-)
)
:effect
(and
(Flag456-)
(Flag456prime-)
)

)
(:action Flag456Action-64
:parameters ()
:precondition
(and
(Flag340-)
(Flag340prime-)
)
:effect
(and
(Flag456-)
(Flag456prime-)
)

)
(:action Flag456Action-65
:parameters ()
:precondition
(and
(Flag448-)
(Flag448prime-)
)
:effect
(and
(Flag456-)
(Flag456prime-)
)

)
(:action Flag456Action-66
:parameters ()
:precondition
(and
(Flag326-)
(Flag326prime-)
)
:effect
(and
(Flag456-)
(Flag456prime-)
)

)
(:action Flag456Action-67
:parameters ()
:precondition
(and
(Flag304-)
(Flag304prime-)
)
:effect
(and
(Flag456-)
(Flag456prime-)
)

)
(:action Flag456Action-68
:parameters ()
:precondition
(and
(Flag413-)
(Flag413prime-)
)
:effect
(and
(Flag456-)
(Flag456prime-)
)

)
(:action Flag456Action-69
:parameters ()
:precondition
(and
(Flag372-)
(Flag372prime-)
)
:effect
(and
(Flag456-)
(Flag456prime-)
)

)
(:action Flag456Action-70
:parameters ()
:precondition
(and
(Flag400-)
(Flag400prime-)
)
:effect
(and
(Flag456-)
(Flag456prime-)
)

)
(:action Flag456Action-71
:parameters ()
:precondition
(and
(Flag382-)
(Flag382prime-)
)
:effect
(and
(Flag456-)
(Flag456prime-)
)

)
(:action Prim456Action
:parameters ()
:precondition
(and
(not (Flag273-))
(Flag273prime-)
(not (Flag345-))
(Flag345prime-)
(not (Flag274-))
(Flag274prime-)
(not (Flag430-))
(Flag430prime-)
(not (Flag359-))
(Flag359prime-)
(not (Flag361-))
(Flag361prime-)
(not (Flag278-))
(Flag278prime-)
(not (Flag451-))
(Flag451prime-)
(not (Flag310-))
(Flag310prime-)
(not (Flag425-))
(Flag425prime-)
(not (Flag409-))
(Flag409prime-)
(not (Flag411-))
(Flag411prime-)
(not (Flag327-))
(Flag327prime-)
(not (Flag418-))
(Flag418prime-)
(not (Flag403-))
(Flag403prime-)
(not (Flag443-))
(Flag443prime-)
(not (Flag440-))
(Flag440prime-)
(not (Flag426-))
(Flag426prime-)
(not (Flag438-))
(Flag438prime-)
(not (Flag337-))
(Flag337prime-)
(not (Flag397-))
(Flag397prime-)
(not (Flag357-))
(Flag357prime-)
(not (Flag362-))
(Flag362prime-)
(not (Flag385-))
(Flag385prime-)
(not (Flag376-))
(Flag376prime-)
(not (Flag284-))
(Flag284prime-)
(not (Flag305-))
(Flag305prime-)
(not (Flag331-))
(Flag331prime-)
(not (Flag288-))
(Flag288prime-)
(not (Flag318-))
(Flag318prime-)
(not (Flag439-))
(Flag439prime-)
(not (Flag435-))
(Flag435prime-)
(not (Flag428-))
(Flag428prime-)
(not (Flag366-))
(Flag366prime-)
(not (Flag452-))
(Flag452prime-)
(not (Flag312-))
(Flag312prime-)
(not (Flag417-))
(Flag417prime-)
(not (Flag407-))
(Flag407prime-)
(not (Flag349-))
(Flag349prime-)
(not (Flag386-))
(Flag386prime-)
(not (Flag445-))
(Flag445prime-)
(not (Flag453-))
(Flag453prime-)
(not (Flag429-))
(Flag429prime-)
(not (Flag394-))
(Flag394prime-)
(not (Flag391-))
(Flag391prime-)
(not (Flag289-))
(Flag289prime-)
(not (Flag297-))
(Flag297prime-)
(not (Flag412-))
(Flag412prime-)
(not (Flag350-))
(Flag350prime-)
(not (Flag330-))
(Flag330prime-)
(not (Flag387-))
(Flag387prime-)
(not (Flag371-))
(Flag371prime-)
(not (Flag322-))
(Flag322prime-)
(not (Flag422-))
(Flag422prime-)
(not (Flag416-))
(Flag416prime-)
(not (Flag441-))
(Flag441prime-)
(not (Flag446-))
(Flag446prime-)
(not (Flag298-))
(Flag298prime-)
(not (Flag454-))
(Flag454prime-)
(not (Flag423-))
(Flag423prime-)
(not (Flag447-))
(Flag447prime-)
(not (Flag392-))
(Flag392prime-)
(not (Flag455-))
(Flag455prime-)
(not (Flag432-))
(Flag432prime-)
(not (Flag340-))
(Flag340prime-)
(not (Flag448-))
(Flag448prime-)
(not (Flag326-))
(Flag326prime-)
(not (Flag304-))
(Flag304prime-)
(not (Flag413-))
(Flag413prime-)
(not (Flag372-))
(Flag372prime-)
(not (Flag400-))
(Flag400prime-)
(not (Flag382-))
(Flag382prime-)
)
:effect
(and
(Flag456prime-)
(not (Flag456-))
)

)
(:action Flag461Action-0
:parameters ()
:precondition
(and
(Flag430-)
(Flag430prime-)
)
:effect
(and
(Flag461-)
(Flag461prime-)
)

)
(:action Flag461Action-1
:parameters ()
:precondition
(and
(Flag345-)
(Flag345prime-)
)
:effect
(and
(Flag461-)
(Flag461prime-)
)

)
(:action Flag461Action-2
:parameters ()
:precondition
(and
(Flag359-)
(Flag359prime-)
)
:effect
(and
(Flag461-)
(Flag461prime-)
)

)
(:action Flag461Action-3
:parameters ()
:precondition
(and
(Flag361-)
(Flag361prime-)
)
:effect
(and
(Flag461-)
(Flag461prime-)
)

)
(:action Flag461Action-4
:parameters ()
:precondition
(and
(Flag278-)
(Flag278prime-)
)
:effect
(and
(Flag461-)
(Flag461prime-)
)

)
(:action Flag461Action-5
:parameters ()
:precondition
(and
(Flag451-)
(Flag451prime-)
)
:effect
(and
(Flag461-)
(Flag461prime-)
)

)
(:action Flag461Action-6
:parameters ()
:precondition
(and
(Flag310-)
(Flag310prime-)
)
:effect
(and
(Flag461-)
(Flag461prime-)
)

)
(:action Flag461Action-7
:parameters ()
:precondition
(and
(Flag425-)
(Flag425prime-)
)
:effect
(and
(Flag461-)
(Flag461prime-)
)

)
(:action Flag461Action-8
:parameters ()
:precondition
(and
(Flag409-)
(Flag409prime-)
)
:effect
(and
(Flag461-)
(Flag461prime-)
)

)
(:action Flag461Action-9
:parameters ()
:precondition
(and
(Flag411-)
(Flag411prime-)
)
:effect
(and
(Flag461-)
(Flag461prime-)
)

)
(:action Flag461Action-10
:parameters ()
:precondition
(and
(Flag327-)
(Flag327prime-)
)
:effect
(and
(Flag461-)
(Flag461prime-)
)

)
(:action Flag461Action-11
:parameters ()
:precondition
(and
(Flag457-)
(Flag457prime-)
)
:effect
(and
(Flag461-)
(Flag461prime-)
)

)
(:action Flag461Action-12
:parameters ()
:precondition
(and
(Flag273-)
(Flag273prime-)
)
:effect
(and
(Flag461-)
(Flag461prime-)
)

)
(:action Flag461Action-13
:parameters ()
:precondition
(and
(Flag447-)
(Flag447prime-)
)
:effect
(and
(Flag461-)
(Flag461prime-)
)

)
(:action Flag461Action-14
:parameters ()
:precondition
(and
(Flag443-)
(Flag443prime-)
)
:effect
(and
(Flag461-)
(Flag461prime-)
)

)
(:action Flag461Action-15
:parameters ()
:precondition
(and
(Flag440-)
(Flag440prime-)
)
:effect
(and
(Flag461-)
(Flag461prime-)
)

)
(:action Flag461Action-16
:parameters ()
:precondition
(and
(Flag426-)
(Flag426prime-)
)
:effect
(and
(Flag461-)
(Flag461prime-)
)

)
(:action Flag461Action-17
:parameters ()
:precondition
(and
(Flag438-)
(Flag438prime-)
)
:effect
(and
(Flag461-)
(Flag461prime-)
)

)
(:action Flag461Action-18
:parameters ()
:precondition
(and
(Flag337-)
(Flag337prime-)
)
:effect
(and
(Flag461-)
(Flag461prime-)
)

)
(:action Flag461Action-19
:parameters ()
:precondition
(and
(Flag357-)
(Flag357prime-)
)
:effect
(and
(Flag461-)
(Flag461prime-)
)

)
(:action Flag461Action-20
:parameters ()
:precondition
(and
(Flag362-)
(Flag362prime-)
)
:effect
(and
(Flag461-)
(Flag461prime-)
)

)
(:action Flag461Action-21
:parameters ()
:precondition
(and
(Flag385-)
(Flag385prime-)
)
:effect
(and
(Flag461-)
(Flag461prime-)
)

)
(:action Flag461Action-22
:parameters ()
:precondition
(and
(Flag284-)
(Flag284prime-)
)
:effect
(and
(Flag461-)
(Flag461prime-)
)

)
(:action Flag461Action-23
:parameters ()
:precondition
(and
(Flag331-)
(Flag331prime-)
)
:effect
(and
(Flag461-)
(Flag461prime-)
)

)
(:action Flag461Action-24
:parameters ()
:precondition
(and
(Flag288-)
(Flag288prime-)
)
:effect
(and
(Flag461-)
(Flag461prime-)
)

)
(:action Flag461Action-25
:parameters ()
:precondition
(and
(Flag391-)
(Flag391prime-)
)
:effect
(and
(Flag461-)
(Flag461prime-)
)

)
(:action Flag461Action-26
:parameters ()
:precondition
(and
(Flag439-)
(Flag439prime-)
)
:effect
(and
(Flag461-)
(Flag461prime-)
)

)
(:action Flag461Action-27
:parameters ()
:precondition
(and
(Flag435-)
(Flag435prime-)
)
:effect
(and
(Flag461-)
(Flag461prime-)
)

)
(:action Flag461Action-28
:parameters ()
:precondition
(and
(Flag428-)
(Flag428prime-)
)
:effect
(and
(Flag461-)
(Flag461prime-)
)

)
(:action Flag461Action-29
:parameters ()
:precondition
(and
(Flag366-)
(Flag366prime-)
)
:effect
(and
(Flag461-)
(Flag461prime-)
)

)
(:action Flag461Action-30
:parameters ()
:precondition
(and
(Flag458-)
(Flag458prime-)
)
:effect
(and
(Flag461-)
(Flag461prime-)
)

)
(:action Flag461Action-31
:parameters ()
:precondition
(and
(Flag312-)
(Flag312prime-)
)
:effect
(and
(Flag461-)
(Flag461prime-)
)

)
(:action Flag461Action-32
:parameters ()
:precondition
(and
(Flag407-)
(Flag407prime-)
)
:effect
(and
(Flag461-)
(Flag461prime-)
)

)
(:action Flag461Action-33
:parameters ()
:precondition
(and
(Flag350-)
(Flag350prime-)
)
:effect
(and
(Flag461-)
(Flag461prime-)
)

)
(:action Flag461Action-34
:parameters ()
:precondition
(and
(Flag386-)
(Flag386prime-)
)
:effect
(and
(Flag461-)
(Flag461prime-)
)

)
(:action Flag461Action-35
:parameters ()
:precondition
(and
(Flag445-)
(Flag445prime-)
)
:effect
(and
(Flag461-)
(Flag461prime-)
)

)
(:action Flag461Action-36
:parameters ()
:precondition
(and
(Flag453-)
(Flag453prime-)
)
:effect
(and
(Flag461-)
(Flag461prime-)
)

)
(:action Flag461Action-37
:parameters ()
:precondition
(and
(Flag394-)
(Flag394prime-)
)
:effect
(and
(Flag461-)
(Flag461prime-)
)

)
(:action Flag461Action-38
:parameters ()
:precondition
(and
(Flag318-)
(Flag318prime-)
)
:effect
(and
(Flag461-)
(Flag461prime-)
)

)
(:action Flag461Action-39
:parameters ()
:precondition
(and
(Flag289-)
(Flag289prime-)
)
:effect
(and
(Flag461-)
(Flag461prime-)
)

)
(:action Flag461Action-40
:parameters ()
:precondition
(and
(Flag297-)
(Flag297prime-)
)
:effect
(and
(Flag461-)
(Flag461prime-)
)

)
(:action Flag461Action-41
:parameters ()
:precondition
(and
(Flag412-)
(Flag412prime-)
)
:effect
(and
(Flag461-)
(Flag461prime-)
)

)
(:action Flag461Action-42
:parameters ()
:precondition
(and
(Flag305-)
(Flag305prime-)
)
:effect
(and
(Flag461-)
(Flag461prime-)
)

)
(:action Flag461Action-43
:parameters ()
:precondition
(and
(Flag330-)
(Flag330prime-)
)
:effect
(and
(Flag461-)
(Flag461prime-)
)

)
(:action Flag461Action-44
:parameters ()
:precondition
(and
(Flag387-)
(Flag387prime-)
)
:effect
(and
(Flag461-)
(Flag461prime-)
)

)
(:action Flag461Action-45
:parameters ()
:precondition
(and
(Flag459-)
(Flag459prime-)
)
:effect
(and
(Flag461-)
(Flag461prime-)
)

)
(:action Flag461Action-46
:parameters ()
:precondition
(and
(Flag322-)
(Flag322prime-)
)
:effect
(and
(Flag461-)
(Flag461prime-)
)

)
(:action Flag461Action-47
:parameters ()
:precondition
(and
(Flag422-)
(Flag422prime-)
)
:effect
(and
(Flag461-)
(Flag461prime-)
)

)
(:action Flag461Action-48
:parameters ()
:precondition
(and
(Flag416-)
(Flag416prime-)
)
:effect
(and
(Flag461-)
(Flag461prime-)
)

)
(:action Flag461Action-49
:parameters ()
:precondition
(and
(Flag455-)
(Flag455prime-)
)
:effect
(and
(Flag461-)
(Flag461prime-)
)

)
(:action Flag461Action-50
:parameters ()
:precondition
(and
(Flag454-)
(Flag454prime-)
)
:effect
(and
(Flag461-)
(Flag461prime-)
)

)
(:action Flag461Action-51
:parameters ()
:precondition
(and
(Flag423-)
(Flag423prime-)
)
:effect
(and
(Flag461-)
(Flag461prime-)
)

)
(:action Flag461Action-52
:parameters ()
:precondition
(and
(Flag432-)
(Flag432prime-)
)
:effect
(and
(Flag461-)
(Flag461prime-)
)

)
(:action Flag461Action-53
:parameters ()
:precondition
(and
(Flag448-)
(Flag448prime-)
)
:effect
(and
(Flag461-)
(Flag461prime-)
)

)
(:action Flag461Action-54
:parameters ()
:precondition
(and
(Flag304-)
(Flag304prime-)
)
:effect
(and
(Flag461-)
(Flag461prime-)
)

)
(:action Flag461Action-55
:parameters ()
:precondition
(and
(Flag413-)
(Flag413prime-)
)
:effect
(and
(Flag461-)
(Flag461prime-)
)

)
(:action Flag461Action-56
:parameters ()
:precondition
(and
(Flag460-)
(Flag460prime-)
)
:effect
(and
(Flag461-)
(Flag461prime-)
)

)
(:action Flag461Action-57
:parameters ()
:precondition
(and
(Flag372-)
(Flag372prime-)
)
:effect
(and
(Flag461-)
(Flag461prime-)
)

)
(:action Flag461Action-58
:parameters ()
:precondition
(and
(Flag400-)
(Flag400prime-)
)
:effect
(and
(Flag461-)
(Flag461prime-)
)

)
(:action Flag461Action-59
:parameters ()
:precondition
(and
(Flag382-)
(Flag382prime-)
)
:effect
(and
(Flag461-)
(Flag461prime-)
)

)
(:action Prim461Action
:parameters ()
:precondition
(and
(not (Flag430-))
(Flag430prime-)
(not (Flag345-))
(Flag345prime-)
(not (Flag359-))
(Flag359prime-)
(not (Flag361-))
(Flag361prime-)
(not (Flag278-))
(Flag278prime-)
(not (Flag451-))
(Flag451prime-)
(not (Flag310-))
(Flag310prime-)
(not (Flag425-))
(Flag425prime-)
(not (Flag409-))
(Flag409prime-)
(not (Flag411-))
(Flag411prime-)
(not (Flag327-))
(Flag327prime-)
(not (Flag457-))
(Flag457prime-)
(not (Flag273-))
(Flag273prime-)
(not (Flag447-))
(Flag447prime-)
(not (Flag443-))
(Flag443prime-)
(not (Flag440-))
(Flag440prime-)
(not (Flag426-))
(Flag426prime-)
(not (Flag438-))
(Flag438prime-)
(not (Flag337-))
(Flag337prime-)
(not (Flag357-))
(Flag357prime-)
(not (Flag362-))
(Flag362prime-)
(not (Flag385-))
(Flag385prime-)
(not (Flag284-))
(Flag284prime-)
(not (Flag331-))
(Flag331prime-)
(not (Flag288-))
(Flag288prime-)
(not (Flag391-))
(Flag391prime-)
(not (Flag439-))
(Flag439prime-)
(not (Flag435-))
(Flag435prime-)
(not (Flag428-))
(Flag428prime-)
(not (Flag366-))
(Flag366prime-)
(not (Flag458-))
(Flag458prime-)
(not (Flag312-))
(Flag312prime-)
(not (Flag407-))
(Flag407prime-)
(not (Flag350-))
(Flag350prime-)
(not (Flag386-))
(Flag386prime-)
(not (Flag445-))
(Flag445prime-)
(not (Flag453-))
(Flag453prime-)
(not (Flag394-))
(Flag394prime-)
(not (Flag318-))
(Flag318prime-)
(not (Flag289-))
(Flag289prime-)
(not (Flag297-))
(Flag297prime-)
(not (Flag412-))
(Flag412prime-)
(not (Flag305-))
(Flag305prime-)
(not (Flag330-))
(Flag330prime-)
(not (Flag387-))
(Flag387prime-)
(not (Flag459-))
(Flag459prime-)
(not (Flag322-))
(Flag322prime-)
(not (Flag422-))
(Flag422prime-)
(not (Flag416-))
(Flag416prime-)
(not (Flag455-))
(Flag455prime-)
(not (Flag454-))
(Flag454prime-)
(not (Flag423-))
(Flag423prime-)
(not (Flag432-))
(Flag432prime-)
(not (Flag448-))
(Flag448prime-)
(not (Flag304-))
(Flag304prime-)
(not (Flag413-))
(Flag413prime-)
(not (Flag460-))
(Flag460prime-)
(not (Flag372-))
(Flag372prime-)
(not (Flag400-))
(Flag400prime-)
(not (Flag382-))
(Flag382prime-)
)
:effect
(and
(Flag461prime-)
(not (Flag461-))
)

)
(:action Flag466Action-0
:parameters ()
:precondition
(and
(Flag273-)
(Flag273prime-)
)
:effect
(and
(Flag466-)
(Flag466prime-)
)

)
(:action Flag466Action-1
:parameters ()
:precondition
(and
(Flag345-)
(Flag345prime-)
)
:effect
(and
(Flag466-)
(Flag466prime-)
)

)
(:action Flag466Action-2
:parameters ()
:precondition
(and
(Flag359-)
(Flag359prime-)
)
:effect
(and
(Flag466-)
(Flag466prime-)
)

)
(:action Flag466Action-3
:parameters ()
:precondition
(and
(Flag361-)
(Flag361prime-)
)
:effect
(and
(Flag466-)
(Flag466prime-)
)

)
(:action Flag466Action-4
:parameters ()
:precondition
(and
(Flag278-)
(Flag278prime-)
)
:effect
(and
(Flag466-)
(Flag466prime-)
)

)
(:action Flag466Action-5
:parameters ()
:precondition
(and
(Flag462-)
(Flag462prime-)
)
:effect
(and
(Flag466-)
(Flag466prime-)
)

)
(:action Flag466Action-6
:parameters ()
:precondition
(and
(Flag425-)
(Flag425prime-)
)
:effect
(and
(Flag466-)
(Flag466prime-)
)

)
(:action Flag466Action-7
:parameters ()
:precondition
(and
(Flag409-)
(Flag409prime-)
)
:effect
(and
(Flag466-)
(Flag466prime-)
)

)
(:action Flag466Action-8
:parameters ()
:precondition
(and
(Flag411-)
(Flag411prime-)
)
:effect
(and
(Flag466-)
(Flag466prime-)
)

)
(:action Flag466Action-9
:parameters ()
:precondition
(and
(Flag327-)
(Flag327prime-)
)
:effect
(and
(Flag466-)
(Flag466prime-)
)

)
(:action Flag466Action-10
:parameters ()
:precondition
(and
(Flag457-)
(Flag457prime-)
)
:effect
(and
(Flag466-)
(Flag466prime-)
)

)
(:action Flag466Action-11
:parameters ()
:precondition
(and
(Flag463-)
(Flag463prime-)
)
:effect
(and
(Flag466-)
(Flag466prime-)
)

)
(:action Flag466Action-12
:parameters ()
:precondition
(and
(Flag443-)
(Flag443prime-)
)
:effect
(and
(Flag466-)
(Flag466prime-)
)

)
(:action Flag466Action-13
:parameters ()
:precondition
(and
(Flag440-)
(Flag440prime-)
)
:effect
(and
(Flag466-)
(Flag466prime-)
)

)
(:action Flag466Action-14
:parameters ()
:precondition
(and
(Flag426-)
(Flag426prime-)
)
:effect
(and
(Flag466-)
(Flag466prime-)
)

)
(:action Flag466Action-15
:parameters ()
:precondition
(and
(Flag438-)
(Flag438prime-)
)
:effect
(and
(Flag466-)
(Flag466prime-)
)

)
(:action Flag466Action-16
:parameters ()
:precondition
(and
(Flag337-)
(Flag337prime-)
)
:effect
(and
(Flag466-)
(Flag466prime-)
)

)
(:action Flag466Action-17
:parameters ()
:precondition
(and
(Flag362-)
(Flag362prime-)
)
:effect
(and
(Flag466-)
(Flag466prime-)
)

)
(:action Flag466Action-18
:parameters ()
:precondition
(and
(Flag385-)
(Flag385prime-)
)
:effect
(and
(Flag466-)
(Flag466prime-)
)

)
(:action Flag466Action-19
:parameters ()
:precondition
(and
(Flag284-)
(Flag284prime-)
)
:effect
(and
(Flag466-)
(Flag466prime-)
)

)
(:action Flag466Action-20
:parameters ()
:precondition
(and
(Flag288-)
(Flag288prime-)
)
:effect
(and
(Flag466-)
(Flag466prime-)
)

)
(:action Flag466Action-21
:parameters ()
:precondition
(and
(Flag318-)
(Flag318prime-)
)
:effect
(and
(Flag466-)
(Flag466prime-)
)

)
(:action Flag466Action-22
:parameters ()
:precondition
(and
(Flag439-)
(Flag439prime-)
)
:effect
(and
(Flag466-)
(Flag466prime-)
)

)
(:action Flag466Action-23
:parameters ()
:precondition
(and
(Flag366-)
(Flag366prime-)
)
:effect
(and
(Flag466-)
(Flag466prime-)
)

)
(:action Flag466Action-24
:parameters ()
:precondition
(and
(Flag458-)
(Flag458prime-)
)
:effect
(and
(Flag466-)
(Flag466prime-)
)

)
(:action Flag466Action-25
:parameters ()
:precondition
(and
(Flag312-)
(Flag312prime-)
)
:effect
(and
(Flag466-)
(Flag466prime-)
)

)
(:action Flag466Action-26
:parameters ()
:precondition
(and
(Flag386-)
(Flag386prime-)
)
:effect
(and
(Flag466-)
(Flag466prime-)
)

)
(:action Flag466Action-27
:parameters ()
:precondition
(and
(Flag422-)
(Flag422prime-)
)
:effect
(and
(Flag466-)
(Flag466prime-)
)

)
(:action Flag466Action-28
:parameters ()
:precondition
(and
(Flag394-)
(Flag394prime-)
)
:effect
(and
(Flag466-)
(Flag466prime-)
)

)
(:action Flag466Action-29
:parameters ()
:precondition
(and
(Flag391-)
(Flag391prime-)
)
:effect
(and
(Flag466-)
(Flag466prime-)
)

)
(:action Flag466Action-30
:parameters ()
:precondition
(and
(Flag322-)
(Flag322prime-)
)
:effect
(and
(Flag466-)
(Flag466prime-)
)

)
(:action Flag466Action-31
:parameters ()
:precondition
(and
(Flag297-)
(Flag297prime-)
)
:effect
(and
(Flag466-)
(Flag466prime-)
)

)
(:action Flag466Action-32
:parameters ()
:precondition
(and
(Flag412-)
(Flag412prime-)
)
:effect
(and
(Flag466-)
(Flag466prime-)
)

)
(:action Flag466Action-33
:parameters ()
:precondition
(and
(Flag350-)
(Flag350prime-)
)
:effect
(and
(Flag466-)
(Flag466prime-)
)

)
(:action Flag466Action-34
:parameters ()
:precondition
(and
(Flag330-)
(Flag330prime-)
)
:effect
(and
(Flag466-)
(Flag466prime-)
)

)
(:action Flag466Action-35
:parameters ()
:precondition
(and
(Flag464-)
(Flag464prime-)
)
:effect
(and
(Flag466-)
(Flag466prime-)
)

)
(:action Flag466Action-36
:parameters ()
:precondition
(and
(Flag453-)
(Flag453prime-)
)
:effect
(and
(Flag466-)
(Flag466prime-)
)

)
(:action Flag466Action-37
:parameters ()
:precondition
(and
(Flag416-)
(Flag416prime-)
)
:effect
(and
(Flag466-)
(Flag466prime-)
)

)
(:action Flag466Action-38
:parameters ()
:precondition
(and
(Flag455-)
(Flag455prime-)
)
:effect
(and
(Flag466-)
(Flag466prime-)
)

)
(:action Flag466Action-39
:parameters ()
:precondition
(and
(Flag454-)
(Flag454prime-)
)
:effect
(and
(Flag466-)
(Flag466prime-)
)

)
(:action Flag466Action-40
:parameters ()
:precondition
(and
(Flag423-)
(Flag423prime-)
)
:effect
(and
(Flag466-)
(Flag466prime-)
)

)
(:action Flag466Action-41
:parameters ()
:precondition
(and
(Flag447-)
(Flag447prime-)
)
:effect
(and
(Flag466-)
(Flag466prime-)
)

)
(:action Flag466Action-42
:parameters ()
:precondition
(and
(Flag432-)
(Flag432prime-)
)
:effect
(and
(Flag466-)
(Flag466prime-)
)

)
(:action Flag466Action-43
:parameters ()
:precondition
(and
(Flag448-)
(Flag448prime-)
)
:effect
(and
(Flag466-)
(Flag466prime-)
)

)
(:action Flag466Action-44
:parameters ()
:precondition
(and
(Flag400-)
(Flag400prime-)
)
:effect
(and
(Flag466-)
(Flag466prime-)
)

)
(:action Flag466Action-45
:parameters ()
:precondition
(and
(Flag305-)
(Flag305prime-)
)
:effect
(and
(Flag466-)
(Flag466prime-)
)

)
(:action Flag466Action-46
:parameters ()
:precondition
(and
(Flag465-)
(Flag465prime-)
)
:effect
(and
(Flag466-)
(Flag466prime-)
)

)
(:action Flag466Action-47
:parameters ()
:precondition
(and
(Flag382-)
(Flag382prime-)
)
:effect
(and
(Flag466-)
(Flag466prime-)
)

)
(:action Prim466Action
:parameters ()
:precondition
(and
(not (Flag273-))
(Flag273prime-)
(not (Flag345-))
(Flag345prime-)
(not (Flag359-))
(Flag359prime-)
(not (Flag361-))
(Flag361prime-)
(not (Flag278-))
(Flag278prime-)
(not (Flag462-))
(Flag462prime-)
(not (Flag425-))
(Flag425prime-)
(not (Flag409-))
(Flag409prime-)
(not (Flag411-))
(Flag411prime-)
(not (Flag327-))
(Flag327prime-)
(not (Flag457-))
(Flag457prime-)
(not (Flag463-))
(Flag463prime-)
(not (Flag443-))
(Flag443prime-)
(not (Flag440-))
(Flag440prime-)
(not (Flag426-))
(Flag426prime-)
(not (Flag438-))
(Flag438prime-)
(not (Flag337-))
(Flag337prime-)
(not (Flag362-))
(Flag362prime-)
(not (Flag385-))
(Flag385prime-)
(not (Flag284-))
(Flag284prime-)
(not (Flag288-))
(Flag288prime-)
(not (Flag318-))
(Flag318prime-)
(not (Flag439-))
(Flag439prime-)
(not (Flag366-))
(Flag366prime-)
(not (Flag458-))
(Flag458prime-)
(not (Flag312-))
(Flag312prime-)
(not (Flag386-))
(Flag386prime-)
(not (Flag422-))
(Flag422prime-)
(not (Flag394-))
(Flag394prime-)
(not (Flag391-))
(Flag391prime-)
(not (Flag322-))
(Flag322prime-)
(not (Flag297-))
(Flag297prime-)
(not (Flag412-))
(Flag412prime-)
(not (Flag350-))
(Flag350prime-)
(not (Flag330-))
(Flag330prime-)
(not (Flag464-))
(Flag464prime-)
(not (Flag453-))
(Flag453prime-)
(not (Flag416-))
(Flag416prime-)
(not (Flag455-))
(Flag455prime-)
(not (Flag454-))
(Flag454prime-)
(not (Flag423-))
(Flag423prime-)
(not (Flag447-))
(Flag447prime-)
(not (Flag432-))
(Flag432prime-)
(not (Flag448-))
(Flag448prime-)
(not (Flag400-))
(Flag400prime-)
(not (Flag305-))
(Flag305prime-)
(not (Flag465-))
(Flag465prime-)
(not (Flag382-))
(Flag382prime-)
)
:effect
(and
(Flag466prime-)
(not (Flag466-))
)

)
(:action Flag469Action-0
:parameters ()
:precondition
(and
(Flag361-)
(Flag361prime-)
)
:effect
(and
(Flag469-)
(Flag469prime-)
)

)
(:action Flag469Action-1
:parameters ()
:precondition
(and
(Flag462-)
(Flag462prime-)
)
:effect
(and
(Flag469-)
(Flag469prime-)
)

)
(:action Flag469Action-2
:parameters ()
:precondition
(and
(Flag411-)
(Flag411prime-)
)
:effect
(and
(Flag469-)
(Flag469prime-)
)

)
(:action Flag469Action-3
:parameters ()
:precondition
(and
(Flag327-)
(Flag327prime-)
)
:effect
(and
(Flag469-)
(Flag469prime-)
)

)
(:action Flag469Action-4
:parameters ()
:precondition
(and
(Flag457-)
(Flag457prime-)
)
:effect
(and
(Flag469-)
(Flag469prime-)
)

)
(:action Flag469Action-5
:parameters ()
:precondition
(and
(Flag447-)
(Flag447prime-)
)
:effect
(and
(Flag469-)
(Flag469prime-)
)

)
(:action Flag469Action-6
:parameters ()
:precondition
(and
(Flag426-)
(Flag426prime-)
)
:effect
(and
(Flag469-)
(Flag469prime-)
)

)
(:action Flag469Action-7
:parameters ()
:precondition
(and
(Flag438-)
(Flag438prime-)
)
:effect
(and
(Flag469-)
(Flag469prime-)
)

)
(:action Flag469Action-8
:parameters ()
:precondition
(and
(Flag337-)
(Flag337prime-)
)
:effect
(and
(Flag469-)
(Flag469prime-)
)

)
(:action Flag469Action-9
:parameters ()
:precondition
(and
(Flag467-)
(Flag467prime-)
)
:effect
(and
(Flag469-)
(Flag469prime-)
)

)
(:action Flag469Action-10
:parameters ()
:precondition
(and
(Flag385-)
(Flag385prime-)
)
:effect
(and
(Flag469-)
(Flag469prime-)
)

)
(:action Flag469Action-11
:parameters ()
:precondition
(and
(Flag284-)
(Flag284prime-)
)
:effect
(and
(Flag469-)
(Flag469prime-)
)

)
(:action Flag469Action-12
:parameters ()
:precondition
(and
(Flag288-)
(Flag288prime-)
)
:effect
(and
(Flag469-)
(Flag469prime-)
)

)
(:action Flag469Action-13
:parameters ()
:precondition
(and
(Flag391-)
(Flag391prime-)
)
:effect
(and
(Flag469-)
(Flag469prime-)
)

)
(:action Flag469Action-14
:parameters ()
:precondition
(and
(Flag439-)
(Flag439prime-)
)
:effect
(and
(Flag469-)
(Flag469prime-)
)

)
(:action Flag469Action-15
:parameters ()
:precondition
(and
(Flag366-)
(Flag366prime-)
)
:effect
(and
(Flag469-)
(Flag469prime-)
)

)
(:action Flag469Action-16
:parameters ()
:precondition
(and
(Flag458-)
(Flag458prime-)
)
:effect
(and
(Flag469-)
(Flag469prime-)
)

)
(:action Flag469Action-17
:parameters ()
:precondition
(and
(Flag312-)
(Flag312prime-)
)
:effect
(and
(Flag469-)
(Flag469prime-)
)

)
(:action Flag469Action-18
:parameters ()
:precondition
(and
(Flag386-)
(Flag386prime-)
)
:effect
(and
(Flag469-)
(Flag469prime-)
)

)
(:action Flag469Action-19
:parameters ()
:precondition
(and
(Flag468-)
(Flag468prime-)
)
:effect
(and
(Flag469-)
(Flag469prime-)
)

)
(:action Flag469Action-20
:parameters ()
:precondition
(and
(Flag453-)
(Flag453prime-)
)
:effect
(and
(Flag469-)
(Flag469prime-)
)

)
(:action Flag469Action-21
:parameters ()
:precondition
(and
(Flag322-)
(Flag322prime-)
)
:effect
(and
(Flag469-)
(Flag469prime-)
)

)
(:action Flag469Action-22
:parameters ()
:precondition
(and
(Flag297-)
(Flag297prime-)
)
:effect
(and
(Flag469-)
(Flag469prime-)
)

)
(:action Flag469Action-23
:parameters ()
:precondition
(and
(Flag412-)
(Flag412prime-)
)
:effect
(and
(Flag469-)
(Flag469prime-)
)

)
(:action Flag469Action-24
:parameters ()
:precondition
(and
(Flag350-)
(Flag350prime-)
)
:effect
(and
(Flag469-)
(Flag469prime-)
)

)
(:action Flag469Action-25
:parameters ()
:precondition
(and
(Flag464-)
(Flag464prime-)
)
:effect
(and
(Flag469-)
(Flag469prime-)
)

)
(:action Flag469Action-26
:parameters ()
:precondition
(and
(Flag422-)
(Flag422prime-)
)
:effect
(and
(Flag469-)
(Flag469prime-)
)

)
(:action Flag469Action-27
:parameters ()
:precondition
(and
(Flag416-)
(Flag416prime-)
)
:effect
(and
(Flag469-)
(Flag469prime-)
)

)
(:action Flag469Action-28
:parameters ()
:precondition
(and
(Flag454-)
(Flag454prime-)
)
:effect
(and
(Flag469-)
(Flag469prime-)
)

)
(:action Flag469Action-29
:parameters ()
:precondition
(and
(Flag432-)
(Flag432prime-)
)
:effect
(and
(Flag469-)
(Flag469prime-)
)

)
(:action Flag469Action-30
:parameters ()
:precondition
(and
(Flag448-)
(Flag448prime-)
)
:effect
(and
(Flag469-)
(Flag469prime-)
)

)
(:action Flag469Action-31
:parameters ()
:precondition
(and
(Flag305-)
(Flag305prime-)
)
:effect
(and
(Flag469-)
(Flag469prime-)
)

)
(:action Flag469Action-32
:parameters ()
:precondition
(and
(Flag400-)
(Flag400prime-)
)
:effect
(and
(Flag469-)
(Flag469prime-)
)

)
(:action Flag469Action-33
:parameters ()
:precondition
(and
(Flag359-)
(Flag359prime-)
)
:effect
(and
(Flag469-)
(Flag469prime-)
)

)
(:action Prim469Action
:parameters ()
:precondition
(and
(not (Flag361-))
(Flag361prime-)
(not (Flag462-))
(Flag462prime-)
(not (Flag411-))
(Flag411prime-)
(not (Flag327-))
(Flag327prime-)
(not (Flag457-))
(Flag457prime-)
(not (Flag447-))
(Flag447prime-)
(not (Flag426-))
(Flag426prime-)
(not (Flag438-))
(Flag438prime-)
(not (Flag337-))
(Flag337prime-)
(not (Flag467-))
(Flag467prime-)
(not (Flag385-))
(Flag385prime-)
(not (Flag284-))
(Flag284prime-)
(not (Flag288-))
(Flag288prime-)
(not (Flag391-))
(Flag391prime-)
(not (Flag439-))
(Flag439prime-)
(not (Flag366-))
(Flag366prime-)
(not (Flag458-))
(Flag458prime-)
(not (Flag312-))
(Flag312prime-)
(not (Flag386-))
(Flag386prime-)
(not (Flag468-))
(Flag468prime-)
(not (Flag453-))
(Flag453prime-)
(not (Flag322-))
(Flag322prime-)
(not (Flag297-))
(Flag297prime-)
(not (Flag412-))
(Flag412prime-)
(not (Flag350-))
(Flag350prime-)
(not (Flag464-))
(Flag464prime-)
(not (Flag422-))
(Flag422prime-)
(not (Flag416-))
(Flag416prime-)
(not (Flag454-))
(Flag454prime-)
(not (Flag432-))
(Flag432prime-)
(not (Flag448-))
(Flag448prime-)
(not (Flag305-))
(Flag305prime-)
(not (Flag400-))
(Flag400prime-)
(not (Flag359-))
(Flag359prime-)
)
:effect
(and
(Flag469prime-)
(not (Flag469-))
)

)
(:action Flag471Action-0
:parameters ()
:precondition
(and
(Flag385-)
(Flag385prime-)
)
:effect
(and
(Flag471-)
(Flag471prime-)
)

)
(:action Flag471Action-1
:parameters ()
:precondition
(and
(Flag322-)
(Flag322prime-)
)
:effect
(and
(Flag471-)
(Flag471prime-)
)

)
(:action Flag471Action-2
:parameters ()
:precondition
(and
(Flag416-)
(Flag416prime-)
)
:effect
(and
(Flag471-)
(Flag471prime-)
)

)
(:action Flag471Action-3
:parameters ()
:precondition
(and
(Flag467-)
(Flag467prime-)
)
:effect
(and
(Flag471-)
(Flag471prime-)
)

)
(:action Flag471Action-4
:parameters ()
:precondition
(and
(Flag327-)
(Flag327prime-)
)
:effect
(and
(Flag471-)
(Flag471prime-)
)

)
(:action Flag471Action-5
:parameters ()
:precondition
(and
(Flag454-)
(Flag454prime-)
)
:effect
(and
(Flag471-)
(Flag471prime-)
)

)
(:action Flag471Action-6
:parameters ()
:precondition
(and
(Flag297-)
(Flag297prime-)
)
:effect
(and
(Flag471-)
(Flag471prime-)
)

)
(:action Flag471Action-7
:parameters ()
:precondition
(and
(Flag462-)
(Flag462prime-)
)
:effect
(and
(Flag471-)
(Flag471prime-)
)

)
(:action Flag471Action-8
:parameters ()
:precondition
(and
(Flag288-)
(Flag288prime-)
)
:effect
(and
(Flag471-)
(Flag471prime-)
)

)
(:action Flag471Action-9
:parameters ()
:precondition
(and
(Flag432-)
(Flag432prime-)
)
:effect
(and
(Flag471-)
(Flag471prime-)
)

)
(:action Flag471Action-10
:parameters ()
:precondition
(and
(Flag470-)
(Flag470prime-)
)
:effect
(and
(Flag471-)
(Flag471prime-)
)

)
(:action Flag471Action-11
:parameters ()
:precondition
(and
(Flag400-)
(Flag400prime-)
)
:effect
(and
(Flag471-)
(Flag471prime-)
)

)
(:action Flag471Action-12
:parameters ()
:precondition
(and
(Flag412-)
(Flag412prime-)
)
:effect
(and
(Flag471-)
(Flag471prime-)
)

)
(:action Flag471Action-13
:parameters ()
:precondition
(and
(Flag439-)
(Flag439prime-)
)
:effect
(and
(Flag471-)
(Flag471prime-)
)

)
(:action Flag471Action-14
:parameters ()
:precondition
(and
(Flag366-)
(Flag366prime-)
)
:effect
(and
(Flag471-)
(Flag471prime-)
)

)
(:action Flag471Action-15
:parameters ()
:precondition
(and
(Flag458-)
(Flag458prime-)
)
:effect
(and
(Flag471-)
(Flag471prime-)
)

)
(:action Flag471Action-16
:parameters ()
:precondition
(and
(Flag447-)
(Flag447prime-)
)
:effect
(and
(Flag471-)
(Flag471prime-)
)

)
(:action Flag471Action-17
:parameters ()
:precondition
(and
(Flag359-)
(Flag359prime-)
)
:effect
(and
(Flag471-)
(Flag471prime-)
)

)
(:action Prim471Action
:parameters ()
:precondition
(and
(not (Flag385-))
(Flag385prime-)
(not (Flag322-))
(Flag322prime-)
(not (Flag416-))
(Flag416prime-)
(not (Flag467-))
(Flag467prime-)
(not (Flag327-))
(Flag327prime-)
(not (Flag454-))
(Flag454prime-)
(not (Flag297-))
(Flag297prime-)
(not (Flag462-))
(Flag462prime-)
(not (Flag288-))
(Flag288prime-)
(not (Flag432-))
(Flag432prime-)
(not (Flag470-))
(Flag470prime-)
(not (Flag400-))
(Flag400prime-)
(not (Flag412-))
(Flag412prime-)
(not (Flag439-))
(Flag439prime-)
(not (Flag366-))
(Flag366prime-)
(not (Flag458-))
(Flag458prime-)
(not (Flag447-))
(Flag447prime-)
(not (Flag359-))
(Flag359prime-)
)
:effect
(and
(Flag471prime-)
(not (Flag471-))
)

)
(:action Flag475Action-0
:parameters ()
:precondition
(and
(Flag290-)
(Flag290prime-)
)
:effect
(and
(Flag475-)
(Flag475prime-)
)

)
(:action Flag475Action-1
:parameters ()
:precondition
(and
(Flag292-)
(Flag292prime-)
)
:effect
(and
(Flag475-)
(Flag475prime-)
)

)
(:action Flag475Action-2
:parameters ()
:precondition
(and
(Flag293-)
(Flag293prime-)
)
:effect
(and
(Flag475-)
(Flag475prime-)
)

)
(:action Flag475Action-3
:parameters ()
:precondition
(and
(Flag276-)
(Flag276prime-)
)
:effect
(and
(Flag475-)
(Flag475prime-)
)

)
(:action Flag475Action-4
:parameters ()
:precondition
(and
(Flag298-)
(Flag298prime-)
)
:effect
(and
(Flag475-)
(Flag475prime-)
)

)
(:action Flag475Action-5
:parameters ()
:precondition
(and
(Flag282-)
(Flag282prime-)
)
:effect
(and
(Flag475-)
(Flag475prime-)
)

)
(:action Flag475Action-6
:parameters ()
:precondition
(and
(Flag277-)
(Flag277prime-)
)
:effect
(and
(Flag475-)
(Flag475prime-)
)

)
(:action Flag475Action-7
:parameters ()
:precondition
(and
(Flag278-)
(Flag278prime-)
)
:effect
(and
(Flag475-)
(Flag475prime-)
)

)
(:action Flag475Action-8
:parameters ()
:precondition
(and
(Flag297-)
(Flag297prime-)
)
:effect
(and
(Flag475-)
(Flag475prime-)
)

)
(:action Flag475Action-9
:parameters ()
:precondition
(and
(Flag474-)
(Flag474prime-)
)
:effect
(and
(Flag475-)
(Flag475prime-)
)

)
(:action Flag475Action-10
:parameters ()
:precondition
(and
(Flag295-)
(Flag295prime-)
)
:effect
(and
(Flag475-)
(Flag475prime-)
)

)
(:action Flag475Action-11
:parameters ()
:precondition
(and
(Flag302-)
(Flag302prime-)
)
:effect
(and
(Flag475-)
(Flag475prime-)
)

)
(:action Flag475Action-12
:parameters ()
:precondition
(and
(Flag286-)
(Flag286prime-)
)
:effect
(and
(Flag475-)
(Flag475prime-)
)

)
(:action Flag475Action-13
:parameters ()
:precondition
(and
(Flag296-)
(Flag296prime-)
)
:effect
(and
(Flag475-)
(Flag475prime-)
)

)
(:action Flag475Action-14
:parameters ()
:precondition
(and
(Flag303-)
(Flag303prime-)
)
:effect
(and
(Flag475-)
(Flag475prime-)
)

)
(:action Flag475Action-15
:parameters ()
:precondition
(and
(Flag304-)
(Flag304prime-)
)
:effect
(and
(Flag475-)
(Flag475prime-)
)

)
(:action Flag475Action-16
:parameters ()
:precondition
(and
(Flag279-)
(Flag279prime-)
)
:effect
(and
(Flag475-)
(Flag475prime-)
)

)
(:action Flag475Action-17
:parameters ()
:precondition
(and
(Flag305-)
(Flag305prime-)
)
:effect
(and
(Flag475-)
(Flag475prime-)
)

)
(:action Prim475Action
:parameters ()
:precondition
(and
(not (Flag290-))
(Flag290prime-)
(not (Flag292-))
(Flag292prime-)
(not (Flag293-))
(Flag293prime-)
(not (Flag276-))
(Flag276prime-)
(not (Flag298-))
(Flag298prime-)
(not (Flag282-))
(Flag282prime-)
(not (Flag277-))
(Flag277prime-)
(not (Flag278-))
(Flag278prime-)
(not (Flag297-))
(Flag297prime-)
(not (Flag474-))
(Flag474prime-)
(not (Flag295-))
(Flag295prime-)
(not (Flag302-))
(Flag302prime-)
(not (Flag286-))
(Flag286prime-)
(not (Flag296-))
(Flag296prime-)
(not (Flag303-))
(Flag303prime-)
(not (Flag304-))
(Flag304prime-)
(not (Flag279-))
(Flag279prime-)
(not (Flag305-))
(Flag305prime-)
)
:effect
(and
(Flag475prime-)
(not (Flag475-))
)

)
(:action Flag1Action
:parameters ()
:precondition
(and
(COLUMN2-ROBOT)
(ROW1-ROBOT)
(NOT-ERROR-)
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(Flag1-)
(Flag1prime-)
)

)
(:action Prim1Action-0
:parameters ()
:precondition
(and
(not (COLUMN2-ROBOT))
)
:effect
(and
(Flag1prime-)
(not (Flag1-))
)

)
(:action Flag2Action-0
:parameters ()
:precondition
(and
(LEFTOF3-ROBOT)
)
:effect
(and
(Flag2-)
(Flag2prime-)
)

)
(:action Flag2Action-1
:parameters ()
:precondition
(and
(RIGHTOF7-ROBOT)
)
:effect
(and
(Flag2-)
(Flag2prime-)
)

)
(:action Flag2Action-2
:parameters ()
:precondition
(and
(RIGHTOF8-ROBOT)
)
:effect
(and
(Flag2-)
(Flag2prime-)
)

)
(:action Flag2Action-3
:parameters ()
:precondition
(and
(RIGHTOF15-ROBOT)
)
:effect
(and
(Flag2-)
(Flag2prime-)
)

)
(:action Flag2Action-4
:parameters ()
:precondition
(and
(LEFTOF2-ROBOT)
)
:effect
(and
(Flag2-)
(Flag2prime-)
)

)
(:action Flag2Action-5
:parameters ()
:precondition
(and
(LEFTOF9-ROBOT)
)
:effect
(and
(Flag2-)
(Flag2prime-)
)

)
(:action Flag2Action-7
:parameters ()
:precondition
(and
(RIGHTOF0-ROBOT)
)
:effect
(and
(Flag2-)
(Flag2prime-)
)

)
(:action Flag2Action-8
:parameters ()
:precondition
(and
(LEFTOF8-ROBOT)
)
:effect
(and
(Flag2-)
(Flag2prime-)
)

)
(:action Flag2Action-9
:parameters ()
:precondition
(and
(LEFTOF7-ROBOT)
)
:effect
(and
(Flag2-)
(Flag2prime-)
)

)
(:action Flag2Action-10
:parameters ()
:precondition
(and
(LEFTOF14-ROBOT)
)
:effect
(and
(Flag2-)
(Flag2prime-)
)

)
(:action Flag2Action-11
:parameters ()
:precondition
(and
(LEFTOF5-ROBOT)
)
:effect
(and
(Flag2-)
(Flag2prime-)
)

)
(:action Flag2Action-12
:parameters ()
:precondition
(and
(RIGHTOF1-ROBOT)
)
:effect
(and
(Flag2-)
(Flag2prime-)
)

)
(:action Flag2Action-13
:parameters ()
:precondition
(and
(LEFTOF12-ROBOT)
)
:effect
(and
(Flag2-)
(Flag2prime-)
)

)
(:action Flag2Action-14
:parameters ()
:precondition
(and
(RIGHTOF13-ROBOT)
)
:effect
(and
(Flag2-)
(Flag2prime-)
)

)
(:action Flag2Action-15
:parameters ()
:precondition
(and
(RIGHTOF14-ROBOT)
)
:effect
(and
(Flag2-)
(Flag2prime-)
)

)
(:action Flag2Action-16
:parameters ()
:precondition
(and
(RIGHTOF6-ROBOT)
)
:effect
(and
(Flag2-)
(Flag2prime-)
)

)
(:action Flag2Action-17
:parameters ()
:precondition
(and
(RIGHTOF9-ROBOT)
)
:effect
(and
(Flag2-)
(Flag2prime-)
)

)
(:action Flag2Action-18
:parameters ()
:precondition
(and
(LEFTOF6-ROBOT)
)
:effect
(and
(Flag2-)
(Flag2prime-)
)

)
(:action Flag2Action-19
:parameters ()
:precondition
(and
(RIGHTOF10-ROBOT)
)
:effect
(and
(Flag2-)
(Flag2prime-)
)

)
(:action Flag2Action-20
:parameters ()
:precondition
(and
(RIGHTOF11-ROBOT)
)
:effect
(and
(Flag2-)
(Flag2prime-)
)

)
(:action Flag2Action-21
:parameters ()
:precondition
(and
(RIGHTOF17-ROBOT)
)
:effect
(and
(Flag2-)
(Flag2prime-)
)

)
(:action Flag2Action-22
:parameters ()
:precondition
(and
(LEFTOF11-ROBOT)
)
:effect
(and
(Flag2-)
(Flag2prime-)
)

)
(:action Flag2Action-23
:parameters ()
:precondition
(and
(RIGHTOF5-ROBOT)
)
:effect
(and
(Flag2-)
(Flag2prime-)
)

)
(:action Flag2Action-24
:parameters ()
:precondition
(and
(LEFTOF10-ROBOT)
)
:effect
(and
(Flag2-)
(Flag2prime-)
)

)
(:action Flag2Action-25
:parameters ()
:precondition
(and
(LEFTOF15-ROBOT)
)
:effect
(and
(Flag2-)
(Flag2prime-)
)

)
(:action Flag2Action-26
:parameters ()
:precondition
(and
(LEFTOF4-ROBOT)
)
:effect
(and
(Flag2-)
(Flag2prime-)
)

)
(:action Flag2Action-27
:parameters ()
:precondition
(and
(LEFTOF16-ROBOT)
)
:effect
(and
(Flag2-)
(Flag2prime-)
)

)
(:action Flag2Action-28
:parameters ()
:precondition
(and
(RIGHTOF16-ROBOT)
)
:effect
(and
(Flag2-)
(Flag2prime-)
)

)
(:action Flag2Action-29
:parameters ()
:precondition
(and
(RIGHTOF4-ROBOT)
)
:effect
(and
(Flag2-)
(Flag2prime-)
)

)
(:action Flag2Action-30
:parameters ()
:precondition
(and
(LEFTOF1-ROBOT)
)
:effect
(and
(Flag2-)
(Flag2prime-)
)

)
(:action Flag2Action-31
:parameters ()
:precondition
(and
(RIGHTOF12-ROBOT)
)
:effect
(and
(Flag2-)
(Flag2prime-)
)

)
(:action Flag2Action-32
:parameters ()
:precondition
(and
(LEFTOF13-ROBOT)
)
:effect
(and
(Flag2-)
(Flag2prime-)
)

)
(:action Flag2Action-34
:parameters ()
:precondition
(and
(LEFTOF18-ROBOT)
)
:effect
(and
(Flag2-)
(Flag2prime-)
)

)
(:action Flag2Action-35
:parameters ()
:precondition
(and
(RIGHTOF3-ROBOT)
)
:effect
(and
(Flag2-)
(Flag2prime-)
)

)
(:action Prim2Action
:parameters ()
:precondition
(and
(not (LEFTOF3-ROBOT))
(not (RIGHTOF7-ROBOT))
(not (RIGHTOF8-ROBOT))
(not (RIGHTOF15-ROBOT))
(not (LEFTOF2-ROBOT))
(not (LEFTOF9-ROBOT))
(not (RIGHTOF2-ROBOT))
(not (RIGHTOF0-ROBOT))
(not (LEFTOF8-ROBOT))
(not (LEFTOF7-ROBOT))
(not (LEFTOF14-ROBOT))
(not (LEFTOF5-ROBOT))
(not (RIGHTOF1-ROBOT))
(not (LEFTOF12-ROBOT))
(not (RIGHTOF13-ROBOT))
(not (RIGHTOF14-ROBOT))
(not (RIGHTOF6-ROBOT))
(not (RIGHTOF9-ROBOT))
(not (LEFTOF6-ROBOT))
(not (RIGHTOF10-ROBOT))
(not (RIGHTOF11-ROBOT))
(not (RIGHTOF17-ROBOT))
(not (LEFTOF11-ROBOT))
(not (RIGHTOF5-ROBOT))
(not (LEFTOF10-ROBOT))
(not (LEFTOF15-ROBOT))
(not (LEFTOF4-ROBOT))
(not (LEFTOF16-ROBOT))
(not (RIGHTOF16-ROBOT))
(not (RIGHTOF4-ROBOT))
(not (LEFTOF1-ROBOT))
(not (RIGHTOF12-ROBOT))
(not (LEFTOF13-ROBOT))
(not (LEFTOF17-ROBOT))
(not (LEFTOF18-ROBOT))
(not (RIGHTOF3-ROBOT))
)
:effect
(and
(Flag2prime-)
(not (Flag2-))
)

)
(:action Flag73Action-0
:parameters ()
:precondition
(and
(Flag38-)
(Flag38prime-)
)
:effect
(and
(Flag73-)
(Flag73prime-)
)

)
(:action Flag73Action-1
:parameters ()
:precondition
(and
(Flag39-)
(Flag39prime-)
)
:effect
(and
(Flag73-)
(Flag73prime-)
)

)
(:action Flag73Action-2
:parameters ()
:precondition
(and
(Flag40-)
(Flag40prime-)
)
:effect
(and
(Flag73-)
(Flag73prime-)
)

)
(:action Flag73Action-3
:parameters ()
:precondition
(and
(Flag41-)
(Flag41prime-)
)
:effect
(and
(Flag73-)
(Flag73prime-)
)

)
(:action Flag73Action-4
:parameters ()
:precondition
(and
(Flag42-)
(Flag42prime-)
)
:effect
(and
(Flag73-)
(Flag73prime-)
)

)
(:action Flag73Action-5
:parameters ()
:precondition
(and
(Flag43-)
(Flag43prime-)
)
:effect
(and
(Flag73-)
(Flag73prime-)
)

)
(:action Flag73Action-6
:parameters ()
:precondition
(and
(Flag44-)
(Flag44prime-)
)
:effect
(and
(Flag73-)
(Flag73prime-)
)

)
(:action Flag73Action-7
:parameters ()
:precondition
(and
(Flag45-)
(Flag45prime-)
)
:effect
(and
(Flag73-)
(Flag73prime-)
)

)
(:action Flag73Action-8
:parameters ()
:precondition
(and
(Flag46-)
(Flag46prime-)
)
:effect
(and
(Flag73-)
(Flag73prime-)
)

)
(:action Flag73Action-9
:parameters ()
:precondition
(and
(Flag47-)
(Flag47prime-)
)
:effect
(and
(Flag73-)
(Flag73prime-)
)

)
(:action Flag73Action-10
:parameters ()
:precondition
(and
(Flag48-)
(Flag48prime-)
)
:effect
(and
(Flag73-)
(Flag73prime-)
)

)
(:action Flag73Action-11
:parameters ()
:precondition
(and
(Flag49-)
(Flag49prime-)
)
:effect
(and
(Flag73-)
(Flag73prime-)
)

)
(:action Flag73Action-12
:parameters ()
:precondition
(and
(Flag50-)
(Flag50prime-)
)
:effect
(and
(Flag73-)
(Flag73prime-)
)

)
(:action Flag73Action-13
:parameters ()
:precondition
(and
(Flag51-)
(Flag51prime-)
)
:effect
(and
(Flag73-)
(Flag73prime-)
)

)
(:action Flag73Action-14
:parameters ()
:precondition
(and
(Flag52-)
(Flag52prime-)
)
:effect
(and
(Flag73-)
(Flag73prime-)
)

)
(:action Flag73Action-15
:parameters ()
:precondition
(and
(Flag53-)
(Flag53prime-)
)
:effect
(and
(Flag73-)
(Flag73prime-)
)

)
(:action Flag73Action-16
:parameters ()
:precondition
(and
(Flag54-)
(Flag54prime-)
)
:effect
(and
(Flag73-)
(Flag73prime-)
)

)
(:action Flag73Action-17
:parameters ()
:precondition
(and
(Flag55-)
(Flag55prime-)
)
:effect
(and
(Flag73-)
(Flag73prime-)
)

)
(:action Flag73Action-18
:parameters ()
:precondition
(and
(Flag56-)
(Flag56prime-)
)
:effect
(and
(Flag73-)
(Flag73prime-)
)

)
(:action Flag73Action-19
:parameters ()
:precondition
(and
(Flag57-)
(Flag57prime-)
)
:effect
(and
(Flag73-)
(Flag73prime-)
)

)
(:action Flag73Action-20
:parameters ()
:precondition
(and
(Flag58-)
(Flag58prime-)
)
:effect
(and
(Flag73-)
(Flag73prime-)
)

)
(:action Flag73Action-21
:parameters ()
:precondition
(and
(Flag59-)
(Flag59prime-)
)
:effect
(and
(Flag73-)
(Flag73prime-)
)

)
(:action Flag73Action-22
:parameters ()
:precondition
(and
(Flag60-)
(Flag60prime-)
)
:effect
(and
(Flag73-)
(Flag73prime-)
)

)
(:action Flag73Action-23
:parameters ()
:precondition
(and
(Flag61-)
(Flag61prime-)
)
:effect
(and
(Flag73-)
(Flag73prime-)
)

)
(:action Flag73Action-24
:parameters ()
:precondition
(and
(Flag62-)
(Flag62prime-)
)
:effect
(and
(Flag73-)
(Flag73prime-)
)

)
(:action Flag73Action-25
:parameters ()
:precondition
(and
(Flag63-)
(Flag63prime-)
)
:effect
(and
(Flag73-)
(Flag73prime-)
)

)
(:action Flag73Action-26
:parameters ()
:precondition
(and
(Flag64-)
(Flag64prime-)
)
:effect
(and
(Flag73-)
(Flag73prime-)
)

)
(:action Flag73Action-27
:parameters ()
:precondition
(and
(Flag65-)
(Flag65prime-)
)
:effect
(and
(Flag73-)
(Flag73prime-)
)

)
(:action Flag73Action-28
:parameters ()
:precondition
(and
(Flag66-)
(Flag66prime-)
)
:effect
(and
(Flag73-)
(Flag73prime-)
)

)
(:action Flag73Action-29
:parameters ()
:precondition
(and
(Flag67-)
(Flag67prime-)
)
:effect
(and
(Flag73-)
(Flag73prime-)
)

)
(:action Flag73Action-30
:parameters ()
:precondition
(and
(Flag68-)
(Flag68prime-)
)
:effect
(and
(Flag73-)
(Flag73prime-)
)

)
(:action Flag73Action-31
:parameters ()
:precondition
(and
(Flag69-)
(Flag69prime-)
)
:effect
(and
(Flag73-)
(Flag73prime-)
)

)
(:action Flag73Action-32
:parameters ()
:precondition
(and
(Flag70-)
(Flag70prime-)
)
:effect
(and
(Flag73-)
(Flag73prime-)
)

)
(:action Flag73Action-33
:parameters ()
:precondition
(and
(Flag71-)
(Flag71prime-)
)
:effect
(and
(Flag73-)
(Flag73prime-)
)

)
(:action Flag73Action-34
:parameters ()
:precondition
(and
(Flag72-)
(Flag72prime-)
)
:effect
(and
(Flag73-)
(Flag73prime-)
)

)
(:action Prim73Action
:parameters ()
:precondition
(and
(not (Flag38-))
(Flag38prime-)
(not (Flag39-))
(Flag39prime-)
(not (Flag40-))
(Flag40prime-)
(not (Flag41-))
(Flag41prime-)
(not (Flag42-))
(Flag42prime-)
(not (Flag43-))
(Flag43prime-)
(not (Flag44-))
(Flag44prime-)
(not (Flag45-))
(Flag45prime-)
(not (Flag46-))
(Flag46prime-)
(not (Flag47-))
(Flag47prime-)
(not (Flag48-))
(Flag48prime-)
(not (Flag49-))
(Flag49prime-)
(not (Flag50-))
(Flag50prime-)
(not (Flag51-))
(Flag51prime-)
(not (Flag52-))
(Flag52prime-)
(not (Flag53-))
(Flag53prime-)
(not (Flag54-))
(Flag54prime-)
(not (Flag55-))
(Flag55prime-)
(not (Flag56-))
(Flag56prime-)
(not (Flag57-))
(Flag57prime-)
(not (Flag58-))
(Flag58prime-)
(not (Flag59-))
(Flag59prime-)
(not (Flag60-))
(Flag60prime-)
(not (Flag61-))
(Flag61prime-)
(not (Flag62-))
(Flag62prime-)
(not (Flag63-))
(Flag63prime-)
(not (Flag64-))
(Flag64prime-)
(not (Flag65-))
(Flag65prime-)
(not (Flag66-))
(Flag66prime-)
(not (Flag67-))
(Flag67prime-)
(not (Flag68-))
(Flag68prime-)
(not (Flag69-))
(Flag69prime-)
(not (Flag70-))
(Flag70prime-)
(not (Flag71-))
(Flag71prime-)
(not (Flag72-))
(Flag72prime-)
)
:effect
(and
(Flag73prime-)
(not (Flag73-))
)

)
(:action Flag91Action-0
:parameters ()
:precondition
(and
(Flag38-)
(Flag38prime-)
)
:effect
(and
(Flag91-)
(Flag91prime-)
)

)
(:action Flag91Action-1
:parameters ()
:precondition
(and
(Flag74-)
(Flag74prime-)
)
:effect
(and
(Flag91-)
(Flag91prime-)
)

)
(:action Flag91Action-2
:parameters ()
:precondition
(and
(Flag45-)
(Flag45prime-)
)
:effect
(and
(Flag91-)
(Flag91prime-)
)

)
(:action Flag91Action-3
:parameters ()
:precondition
(and
(Flag61-)
(Flag61prime-)
)
:effect
(and
(Flag91-)
(Flag91prime-)
)

)
(:action Flag91Action-4
:parameters ()
:precondition
(and
(Flag75-)
(Flag75prime-)
)
:effect
(and
(Flag91-)
(Flag91prime-)
)

)
(:action Flag91Action-5
:parameters ()
:precondition
(and
(Flag54-)
(Flag54prime-)
)
:effect
(and
(Flag91-)
(Flag91prime-)
)

)
(:action Flag91Action-6
:parameters ()
:precondition
(and
(Flag76-)
(Flag76prime-)
)
:effect
(and
(Flag91-)
(Flag91prime-)
)

)
(:action Flag91Action-7
:parameters ()
:precondition
(and
(Flag40-)
(Flag40prime-)
)
:effect
(and
(Flag91-)
(Flag91prime-)
)

)
(:action Flag91Action-8
:parameters ()
:precondition
(and
(Flag51-)
(Flag51prime-)
)
:effect
(and
(Flag91-)
(Flag91prime-)
)

)
(:action Flag91Action-9
:parameters ()
:precondition
(and
(Flag42-)
(Flag42prime-)
)
:effect
(and
(Flag91-)
(Flag91prime-)
)

)
(:action Flag91Action-10
:parameters ()
:precondition
(and
(Flag43-)
(Flag43prime-)
)
:effect
(and
(Flag91-)
(Flag91prime-)
)

)
(:action Flag91Action-11
:parameters ()
:precondition
(and
(Flag77-)
(Flag77prime-)
)
:effect
(and
(Flag91-)
(Flag91prime-)
)

)
(:action Flag91Action-12
:parameters ()
:precondition
(and
(Flag78-)
(Flag78prime-)
)
:effect
(and
(Flag91-)
(Flag91prime-)
)

)
(:action Flag91Action-13
:parameters ()
:precondition
(and
(Flag44-)
(Flag44prime-)
)
:effect
(and
(Flag91-)
(Flag91prime-)
)

)
(:action Flag91Action-14
:parameters ()
:precondition
(and
(Flag46-)
(Flag46prime-)
)
:effect
(and
(Flag91-)
(Flag91prime-)
)

)
(:action Flag91Action-15
:parameters ()
:precondition
(and
(Flag79-)
(Flag79prime-)
)
:effect
(and
(Flag91-)
(Flag91prime-)
)

)
(:action Flag91Action-16
:parameters ()
:precondition
(and
(Flag48-)
(Flag48prime-)
)
:effect
(and
(Flag91-)
(Flag91prime-)
)

)
(:action Flag91Action-17
:parameters ()
:precondition
(and
(Flag80-)
(Flag80prime-)
)
:effect
(and
(Flag91-)
(Flag91prime-)
)

)
(:action Flag91Action-18
:parameters ()
:precondition
(and
(Flag81-)
(Flag81prime-)
)
:effect
(and
(Flag91-)
(Flag91prime-)
)

)
(:action Flag91Action-19
:parameters ()
:precondition
(and
(Flag49-)
(Flag49prime-)
)
:effect
(and
(Flag91-)
(Flag91prime-)
)

)
(:action Flag91Action-20
:parameters ()
:precondition
(and
(Flag50-)
(Flag50prime-)
)
:effect
(and
(Flag91-)
(Flag91prime-)
)

)
(:action Flag91Action-21
:parameters ()
:precondition
(and
(Flag52-)
(Flag52prime-)
)
:effect
(and
(Flag91-)
(Flag91prime-)
)

)
(:action Flag91Action-22
:parameters ()
:precondition
(and
(Flag82-)
(Flag82prime-)
)
:effect
(and
(Flag91-)
(Flag91prime-)
)

)
(:action Flag91Action-23
:parameters ()
:precondition
(and
(Flag53-)
(Flag53prime-)
)
:effect
(and
(Flag91-)
(Flag91prime-)
)

)
(:action Flag91Action-24
:parameters ()
:precondition
(and
(Flag83-)
(Flag83prime-)
)
:effect
(and
(Flag91-)
(Flag91prime-)
)

)
(:action Flag91Action-25
:parameters ()
:precondition
(and
(Flag55-)
(Flag55prime-)
)
:effect
(and
(Flag91-)
(Flag91prime-)
)

)
(:action Flag91Action-26
:parameters ()
:precondition
(and
(Flag56-)
(Flag56prime-)
)
:effect
(and
(Flag91-)
(Flag91prime-)
)

)
(:action Flag91Action-27
:parameters ()
:precondition
(and
(Flag84-)
(Flag84prime-)
)
:effect
(and
(Flag91-)
(Flag91prime-)
)

)
(:action Flag91Action-28
:parameters ()
:precondition
(and
(Flag57-)
(Flag57prime-)
)
:effect
(and
(Flag91-)
(Flag91prime-)
)

)
(:action Flag91Action-29
:parameters ()
:precondition
(and
(Flag85-)
(Flag85prime-)
)
:effect
(and
(Flag91-)
(Flag91prime-)
)

)
(:action Flag91Action-30
:parameters ()
:precondition
(and
(Flag59-)
(Flag59prime-)
)
:effect
(and
(Flag91-)
(Flag91prime-)
)

)
(:action Flag91Action-31
:parameters ()
:precondition
(and
(Flag60-)
(Flag60prime-)
)
:effect
(and
(Flag91-)
(Flag91prime-)
)

)
(:action Flag91Action-32
:parameters ()
:precondition
(and
(Flag58-)
(Flag58prime-)
)
:effect
(and
(Flag91-)
(Flag91prime-)
)

)
(:action Flag91Action-33
:parameters ()
:precondition
(and
(Flag86-)
(Flag86prime-)
)
:effect
(and
(Flag91-)
(Flag91prime-)
)

)
(:action Flag91Action-34
:parameters ()
:precondition
(and
(Flag87-)
(Flag87prime-)
)
:effect
(and
(Flag91-)
(Flag91prime-)
)

)
(:action Flag91Action-35
:parameters ()
:precondition
(and
(Flag62-)
(Flag62prime-)
)
:effect
(and
(Flag91-)
(Flag91prime-)
)

)
(:action Flag91Action-36
:parameters ()
:precondition
(and
(Flag63-)
(Flag63prime-)
)
:effect
(and
(Flag91-)
(Flag91prime-)
)

)
(:action Flag91Action-37
:parameters ()
:precondition
(and
(Flag64-)
(Flag64prime-)
)
:effect
(and
(Flag91-)
(Flag91prime-)
)

)
(:action Flag91Action-38
:parameters ()
:precondition
(and
(Flag65-)
(Flag65prime-)
)
:effect
(and
(Flag91-)
(Flag91prime-)
)

)
(:action Flag91Action-39
:parameters ()
:precondition
(and
(Flag66-)
(Flag66prime-)
)
:effect
(and
(Flag91-)
(Flag91prime-)
)

)
(:action Flag91Action-40
:parameters ()
:precondition
(and
(Flag67-)
(Flag67prime-)
)
:effect
(and
(Flag91-)
(Flag91prime-)
)

)
(:action Flag91Action-41
:parameters ()
:precondition
(and
(Flag88-)
(Flag88prime-)
)
:effect
(and
(Flag91-)
(Flag91prime-)
)

)
(:action Flag91Action-42
:parameters ()
:precondition
(and
(Flag68-)
(Flag68prime-)
)
:effect
(and
(Flag91-)
(Flag91prime-)
)

)
(:action Flag91Action-43
:parameters ()
:precondition
(and
(Flag89-)
(Flag89prime-)
)
:effect
(and
(Flag91-)
(Flag91prime-)
)

)
(:action Flag91Action-44
:parameters ()
:precondition
(and
(Flag90-)
(Flag90prime-)
)
:effect
(and
(Flag91-)
(Flag91prime-)
)

)
(:action Flag91Action-45
:parameters ()
:precondition
(and
(Flag69-)
(Flag69prime-)
)
:effect
(and
(Flag91-)
(Flag91prime-)
)

)
(:action Flag91Action-46
:parameters ()
:precondition
(and
(Flag70-)
(Flag70prime-)
)
:effect
(and
(Flag91-)
(Flag91prime-)
)

)
(:action Flag91Action-47
:parameters ()
:precondition
(and
(Flag71-)
(Flag71prime-)
)
:effect
(and
(Flag91-)
(Flag91prime-)
)

)
(:action Flag91Action-48
:parameters ()
:precondition
(and
(Flag72-)
(Flag72prime-)
)
:effect
(and
(Flag91-)
(Flag91prime-)
)

)
(:action Prim91Action
:parameters ()
:precondition
(and
(not (Flag38-))
(Flag38prime-)
(not (Flag74-))
(Flag74prime-)
(not (Flag45-))
(Flag45prime-)
(not (Flag61-))
(Flag61prime-)
(not (Flag75-))
(Flag75prime-)
(not (Flag54-))
(Flag54prime-)
(not (Flag76-))
(Flag76prime-)
(not (Flag40-))
(Flag40prime-)
(not (Flag51-))
(Flag51prime-)
(not (Flag42-))
(Flag42prime-)
(not (Flag43-))
(Flag43prime-)
(not (Flag77-))
(Flag77prime-)
(not (Flag78-))
(Flag78prime-)
(not (Flag44-))
(Flag44prime-)
(not (Flag46-))
(Flag46prime-)
(not (Flag79-))
(Flag79prime-)
(not (Flag48-))
(Flag48prime-)
(not (Flag80-))
(Flag80prime-)
(not (Flag81-))
(Flag81prime-)
(not (Flag49-))
(Flag49prime-)
(not (Flag50-))
(Flag50prime-)
(not (Flag52-))
(Flag52prime-)
(not (Flag82-))
(Flag82prime-)
(not (Flag53-))
(Flag53prime-)
(not (Flag83-))
(Flag83prime-)
(not (Flag55-))
(Flag55prime-)
(not (Flag56-))
(Flag56prime-)
(not (Flag84-))
(Flag84prime-)
(not (Flag57-))
(Flag57prime-)
(not (Flag85-))
(Flag85prime-)
(not (Flag59-))
(Flag59prime-)
(not (Flag60-))
(Flag60prime-)
(not (Flag58-))
(Flag58prime-)
(not (Flag86-))
(Flag86prime-)
(not (Flag87-))
(Flag87prime-)
(not (Flag62-))
(Flag62prime-)
(not (Flag63-))
(Flag63prime-)
(not (Flag64-))
(Flag64prime-)
(not (Flag65-))
(Flag65prime-)
(not (Flag66-))
(Flag66prime-)
(not (Flag67-))
(Flag67prime-)
(not (Flag88-))
(Flag88prime-)
(not (Flag68-))
(Flag68prime-)
(not (Flag89-))
(Flag89prime-)
(not (Flag90-))
(Flag90prime-)
(not (Flag69-))
(Flag69prime-)
(not (Flag70-))
(Flag70prime-)
(not (Flag71-))
(Flag71prime-)
(not (Flag72-))
(Flag72prime-)
)
:effect
(and
(Flag91prime-)
(not (Flag91-))
)

)
(:action Flag109Action-0
:parameters ()
:precondition
(and
(Flag38-)
(Flag38prime-)
)
:effect
(and
(Flag109-)
(Flag109prime-)
)

)
(:action Flag109Action-1
:parameters ()
:precondition
(and
(Flag78-)
(Flag78prime-)
)
:effect
(and
(Flag109-)
(Flag109prime-)
)

)
(:action Flag109Action-2
:parameters ()
:precondition
(and
(Flag74-)
(Flag74prime-)
)
:effect
(and
(Flag109-)
(Flag109prime-)
)

)
(:action Flag109Action-3
:parameters ()
:precondition
(and
(Flag92-)
(Flag92prime-)
)
:effect
(and
(Flag109-)
(Flag109prime-)
)

)
(:action Flag109Action-4
:parameters ()
:precondition
(and
(Flag93-)
(Flag93prime-)
)
:effect
(and
(Flag109-)
(Flag109prime-)
)

)
(:action Flag109Action-5
:parameters ()
:precondition
(and
(Flag94-)
(Flag94prime-)
)
:effect
(and
(Flag109-)
(Flag109prime-)
)

)
(:action Flag109Action-6
:parameters ()
:precondition
(and
(Flag95-)
(Flag95prime-)
)
:effect
(and
(Flag109-)
(Flag109prime-)
)

)
(:action Flag109Action-7
:parameters ()
:precondition
(and
(Flag96-)
(Flag96prime-)
)
:effect
(and
(Flag109-)
(Flag109prime-)
)

)
(:action Flag109Action-8
:parameters ()
:precondition
(and
(Flag61-)
(Flag61prime-)
)
:effect
(and
(Flag109-)
(Flag109prime-)
)

)
(:action Flag109Action-9
:parameters ()
:precondition
(and
(Flag75-)
(Flag75prime-)
)
:effect
(and
(Flag109-)
(Flag109prime-)
)

)
(:action Flag109Action-10
:parameters ()
:precondition
(and
(Flag76-)
(Flag76prime-)
)
:effect
(and
(Flag109-)
(Flag109prime-)
)

)
(:action Flag109Action-11
:parameters ()
:precondition
(and
(Flag45-)
(Flag45prime-)
)
:effect
(and
(Flag109-)
(Flag109prime-)
)

)
(:action Flag109Action-12
:parameters ()
:precondition
(and
(Flag40-)
(Flag40prime-)
)
:effect
(and
(Flag109-)
(Flag109prime-)
)

)
(:action Flag109Action-13
:parameters ()
:precondition
(and
(Flag51-)
(Flag51prime-)
)
:effect
(and
(Flag109-)
(Flag109prime-)
)

)
(:action Flag109Action-14
:parameters ()
:precondition
(and
(Flag42-)
(Flag42prime-)
)
:effect
(and
(Flag109-)
(Flag109prime-)
)

)
(:action Flag109Action-15
:parameters ()
:precondition
(and
(Flag80-)
(Flag80prime-)
)
:effect
(and
(Flag109-)
(Flag109prime-)
)

)
(:action Flag109Action-16
:parameters ()
:precondition
(and
(Flag43-)
(Flag43prime-)
)
:effect
(and
(Flag109-)
(Flag109prime-)
)

)
(:action Flag109Action-17
:parameters ()
:precondition
(and
(Flag77-)
(Flag77prime-)
)
:effect
(and
(Flag109-)
(Flag109prime-)
)

)
(:action Flag109Action-18
:parameters ()
:precondition
(and
(Flag97-)
(Flag97prime-)
)
:effect
(and
(Flag109-)
(Flag109prime-)
)

)
(:action Flag109Action-19
:parameters ()
:precondition
(and
(Flag44-)
(Flag44prime-)
)
:effect
(and
(Flag109-)
(Flag109prime-)
)

)
(:action Flag109Action-20
:parameters ()
:precondition
(and
(Flag98-)
(Flag98prime-)
)
:effect
(and
(Flag109-)
(Flag109prime-)
)

)
(:action Flag109Action-21
:parameters ()
:precondition
(and
(Flag46-)
(Flag46prime-)
)
:effect
(and
(Flag109-)
(Flag109prime-)
)

)
(:action Flag109Action-22
:parameters ()
:precondition
(and
(Flag79-)
(Flag79prime-)
)
:effect
(and
(Flag109-)
(Flag109prime-)
)

)
(:action Flag109Action-23
:parameters ()
:precondition
(and
(Flag48-)
(Flag48prime-)
)
:effect
(and
(Flag109-)
(Flag109prime-)
)

)
(:action Flag109Action-24
:parameters ()
:precondition
(and
(Flag99-)
(Flag99prime-)
)
:effect
(and
(Flag109-)
(Flag109prime-)
)

)
(:action Flag109Action-25
:parameters ()
:precondition
(and
(Flag65-)
(Flag65prime-)
)
:effect
(and
(Flag109-)
(Flag109prime-)
)

)
(:action Flag109Action-26
:parameters ()
:precondition
(and
(Flag81-)
(Flag81prime-)
)
:effect
(and
(Flag109-)
(Flag109prime-)
)

)
(:action Flag109Action-27
:parameters ()
:precondition
(and
(Flag49-)
(Flag49prime-)
)
:effect
(and
(Flag109-)
(Flag109prime-)
)

)
(:action Flag109Action-28
:parameters ()
:precondition
(and
(Flag50-)
(Flag50prime-)
)
:effect
(and
(Flag109-)
(Flag109prime-)
)

)
(:action Flag109Action-29
:parameters ()
:precondition
(and
(Flag100-)
(Flag100prime-)
)
:effect
(and
(Flag109-)
(Flag109prime-)
)

)
(:action Flag109Action-30
:parameters ()
:precondition
(and
(Flag101-)
(Flag101prime-)
)
:effect
(and
(Flag109-)
(Flag109prime-)
)

)
(:action Flag109Action-31
:parameters ()
:precondition
(and
(Flag102-)
(Flag102prime-)
)
:effect
(and
(Flag109-)
(Flag109prime-)
)

)
(:action Flag109Action-32
:parameters ()
:precondition
(and
(Flag53-)
(Flag53prime-)
)
:effect
(and
(Flag109-)
(Flag109prime-)
)

)
(:action Flag109Action-33
:parameters ()
:precondition
(and
(Flag55-)
(Flag55prime-)
)
:effect
(and
(Flag109-)
(Flag109prime-)
)

)
(:action Flag109Action-34
:parameters ()
:precondition
(and
(Flag56-)
(Flag56prime-)
)
:effect
(and
(Flag109-)
(Flag109prime-)
)

)
(:action Flag109Action-35
:parameters ()
:precondition
(and
(Flag84-)
(Flag84prime-)
)
:effect
(and
(Flag109-)
(Flag109prime-)
)

)
(:action Flag109Action-36
:parameters ()
:precondition
(and
(Flag57-)
(Flag57prime-)
)
:effect
(and
(Flag109-)
(Flag109prime-)
)

)
(:action Flag109Action-37
:parameters ()
:precondition
(and
(Flag85-)
(Flag85prime-)
)
:effect
(and
(Flag109-)
(Flag109prime-)
)

)
(:action Flag109Action-38
:parameters ()
:precondition
(and
(Flag59-)
(Flag59prime-)
)
:effect
(and
(Flag109-)
(Flag109prime-)
)

)
(:action Flag109Action-39
:parameters ()
:precondition
(and
(Flag67-)
(Flag67prime-)
)
:effect
(and
(Flag109-)
(Flag109prime-)
)

)
(:action Flag109Action-40
:parameters ()
:precondition
(and
(Flag60-)
(Flag60prime-)
)
:effect
(and
(Flag109-)
(Flag109prime-)
)

)
(:action Flag109Action-41
:parameters ()
:precondition
(and
(Flag58-)
(Flag58prime-)
)
:effect
(and
(Flag109-)
(Flag109prime-)
)

)
(:action Flag109Action-42
:parameters ()
:precondition
(and
(Flag86-)
(Flag86prime-)
)
:effect
(and
(Flag109-)
(Flag109prime-)
)

)
(:action Flag109Action-43
:parameters ()
:precondition
(and
(Flag103-)
(Flag103prime-)
)
:effect
(and
(Flag109-)
(Flag109prime-)
)

)
(:action Flag109Action-44
:parameters ()
:precondition
(and
(Flag62-)
(Flag62prime-)
)
:effect
(and
(Flag109-)
(Flag109prime-)
)

)
(:action Flag109Action-45
:parameters ()
:precondition
(and
(Flag63-)
(Flag63prime-)
)
:effect
(and
(Flag109-)
(Flag109prime-)
)

)
(:action Flag109Action-46
:parameters ()
:precondition
(and
(Flag64-)
(Flag64prime-)
)
:effect
(and
(Flag109-)
(Flag109prime-)
)

)
(:action Flag109Action-47
:parameters ()
:precondition
(and
(Flag104-)
(Flag104prime-)
)
:effect
(and
(Flag109-)
(Flag109prime-)
)

)
(:action Flag109Action-48
:parameters ()
:precondition
(and
(Flag66-)
(Flag66prime-)
)
:effect
(and
(Flag109-)
(Flag109prime-)
)

)
(:action Flag109Action-49
:parameters ()
:precondition
(and
(Flag105-)
(Flag105prime-)
)
:effect
(and
(Flag109-)
(Flag109prime-)
)

)
(:action Flag109Action-50
:parameters ()
:precondition
(and
(Flag106-)
(Flag106prime-)
)
:effect
(and
(Flag109-)
(Flag109prime-)
)

)
(:action Flag109Action-51
:parameters ()
:precondition
(and
(Flag88-)
(Flag88prime-)
)
:effect
(and
(Flag109-)
(Flag109prime-)
)

)
(:action Flag109Action-52
:parameters ()
:precondition
(and
(Flag107-)
(Flag107prime-)
)
:effect
(and
(Flag109-)
(Flag109prime-)
)

)
(:action Flag109Action-53
:parameters ()
:precondition
(and
(Flag108-)
(Flag108prime-)
)
:effect
(and
(Flag109-)
(Flag109prime-)
)

)
(:action Flag109Action-54
:parameters ()
:precondition
(and
(Flag89-)
(Flag89prime-)
)
:effect
(and
(Flag109-)
(Flag109prime-)
)

)
(:action Flag109Action-55
:parameters ()
:precondition
(and
(Flag52-)
(Flag52prime-)
)
:effect
(and
(Flag109-)
(Flag109prime-)
)

)
(:action Flag109Action-56
:parameters ()
:precondition
(and
(Flag68-)
(Flag68prime-)
)
:effect
(and
(Flag109-)
(Flag109prime-)
)

)
(:action Flag109Action-57
:parameters ()
:precondition
(and
(Flag90-)
(Flag90prime-)
)
:effect
(and
(Flag109-)
(Flag109prime-)
)

)
(:action Flag109Action-58
:parameters ()
:precondition
(and
(Flag69-)
(Flag69prime-)
)
:effect
(and
(Flag109-)
(Flag109prime-)
)

)
(:action Flag109Action-59
:parameters ()
:precondition
(and
(Flag70-)
(Flag70prime-)
)
:effect
(and
(Flag109-)
(Flag109prime-)
)

)
(:action Flag109Action-60
:parameters ()
:precondition
(and
(Flag71-)
(Flag71prime-)
)
:effect
(and
(Flag109-)
(Flag109prime-)
)

)
(:action Flag109Action-61
:parameters ()
:precondition
(and
(Flag72-)
(Flag72prime-)
)
:effect
(and
(Flag109-)
(Flag109prime-)
)

)
(:action Prim109Action
:parameters ()
:precondition
(and
(not (Flag38-))
(Flag38prime-)
(not (Flag78-))
(Flag78prime-)
(not (Flag74-))
(Flag74prime-)
(not (Flag92-))
(Flag92prime-)
(not (Flag93-))
(Flag93prime-)
(not (Flag94-))
(Flag94prime-)
(not (Flag95-))
(Flag95prime-)
(not (Flag96-))
(Flag96prime-)
(not (Flag61-))
(Flag61prime-)
(not (Flag75-))
(Flag75prime-)
(not (Flag76-))
(Flag76prime-)
(not (Flag45-))
(Flag45prime-)
(not (Flag40-))
(Flag40prime-)
(not (Flag51-))
(Flag51prime-)
(not (Flag42-))
(Flag42prime-)
(not (Flag80-))
(Flag80prime-)
(not (Flag43-))
(Flag43prime-)
(not (Flag77-))
(Flag77prime-)
(not (Flag97-))
(Flag97prime-)
(not (Flag44-))
(Flag44prime-)
(not (Flag98-))
(Flag98prime-)
(not (Flag46-))
(Flag46prime-)
(not (Flag79-))
(Flag79prime-)
(not (Flag48-))
(Flag48prime-)
(not (Flag99-))
(Flag99prime-)
(not (Flag65-))
(Flag65prime-)
(not (Flag81-))
(Flag81prime-)
(not (Flag49-))
(Flag49prime-)
(not (Flag50-))
(Flag50prime-)
(not (Flag100-))
(Flag100prime-)
(not (Flag101-))
(Flag101prime-)
(not (Flag102-))
(Flag102prime-)
(not (Flag53-))
(Flag53prime-)
(not (Flag55-))
(Flag55prime-)
(not (Flag56-))
(Flag56prime-)
(not (Flag84-))
(Flag84prime-)
(not (Flag57-))
(Flag57prime-)
(not (Flag85-))
(Flag85prime-)
(not (Flag59-))
(Flag59prime-)
(not (Flag67-))
(Flag67prime-)
(not (Flag60-))
(Flag60prime-)
(not (Flag58-))
(Flag58prime-)
(not (Flag86-))
(Flag86prime-)
(not (Flag103-))
(Flag103prime-)
(not (Flag62-))
(Flag62prime-)
(not (Flag63-))
(Flag63prime-)
(not (Flag64-))
(Flag64prime-)
(not (Flag104-))
(Flag104prime-)
(not (Flag66-))
(Flag66prime-)
(not (Flag105-))
(Flag105prime-)
(not (Flag106-))
(Flag106prime-)
(not (Flag88-))
(Flag88prime-)
(not (Flag107-))
(Flag107prime-)
(not (Flag108-))
(Flag108prime-)
(not (Flag89-))
(Flag89prime-)
(not (Flag52-))
(Flag52prime-)
(not (Flag68-))
(Flag68prime-)
(not (Flag90-))
(Flag90prime-)
(not (Flag69-))
(Flag69prime-)
(not (Flag70-))
(Flag70prime-)
(not (Flag71-))
(Flag71prime-)
(not (Flag72-))
(Flag72prime-)
)
:effect
(and
(Flag109prime-)
(not (Flag109-))
)

)
(:action Flag125Action-0
:parameters ()
:precondition
(and
(Flag38-)
(Flag38prime-)
)
:effect
(and
(Flag125-)
(Flag125prime-)
)

)
(:action Flag125Action-1
:parameters ()
:precondition
(and
(Flag110-)
(Flag110prime-)
)
:effect
(and
(Flag125-)
(Flag125prime-)
)

)
(:action Flag125Action-2
:parameters ()
:precondition
(and
(Flag78-)
(Flag78prime-)
)
:effect
(and
(Flag125-)
(Flag125prime-)
)

)
(:action Flag125Action-3
:parameters ()
:precondition
(and
(Flag111-)
(Flag111prime-)
)
:effect
(and
(Flag125-)
(Flag125prime-)
)

)
(:action Flag125Action-4
:parameters ()
:precondition
(and
(Flag74-)
(Flag74prime-)
)
:effect
(and
(Flag125-)
(Flag125prime-)
)

)
(:action Flag125Action-5
:parameters ()
:precondition
(and
(Flag92-)
(Flag92prime-)
)
:effect
(and
(Flag125-)
(Flag125prime-)
)

)
(:action Flag125Action-6
:parameters ()
:precondition
(and
(Flag95-)
(Flag95prime-)
)
:effect
(and
(Flag125-)
(Flag125prime-)
)

)
(:action Flag125Action-7
:parameters ()
:precondition
(and
(Flag93-)
(Flag93prime-)
)
:effect
(and
(Flag125-)
(Flag125prime-)
)

)
(:action Flag125Action-8
:parameters ()
:precondition
(and
(Flag106-)
(Flag106prime-)
)
:effect
(and
(Flag125-)
(Flag125prime-)
)

)
(:action Flag125Action-9
:parameters ()
:precondition
(and
(Flag112-)
(Flag112prime-)
)
:effect
(and
(Flag125-)
(Flag125prime-)
)

)
(:action Flag125Action-10
:parameters ()
:precondition
(and
(Flag75-)
(Flag75prime-)
)
:effect
(and
(Flag125-)
(Flag125prime-)
)

)
(:action Flag125Action-11
:parameters ()
:precondition
(and
(Flag113-)
(Flag113prime-)
)
:effect
(and
(Flag125-)
(Flag125prime-)
)

)
(:action Flag125Action-12
:parameters ()
:precondition
(and
(Flag76-)
(Flag76prime-)
)
:effect
(and
(Flag125-)
(Flag125prime-)
)

)
(:action Flag125Action-13
:parameters ()
:precondition
(and
(Flag45-)
(Flag45prime-)
)
:effect
(and
(Flag125-)
(Flag125prime-)
)

)
(:action Flag125Action-14
:parameters ()
:precondition
(and
(Flag114-)
(Flag114prime-)
)
:effect
(and
(Flag125-)
(Flag125prime-)
)

)
(:action Flag125Action-15
:parameters ()
:precondition
(and
(Flag40-)
(Flag40prime-)
)
:effect
(and
(Flag125-)
(Flag125prime-)
)

)
(:action Flag125Action-16
:parameters ()
:precondition
(and
(Flag51-)
(Flag51prime-)
)
:effect
(and
(Flag125-)
(Flag125prime-)
)

)
(:action Flag125Action-17
:parameters ()
:precondition
(and
(Flag42-)
(Flag42prime-)
)
:effect
(and
(Flag125-)
(Flag125prime-)
)

)
(:action Flag125Action-18
:parameters ()
:precondition
(and
(Flag97-)
(Flag97prime-)
)
:effect
(and
(Flag125-)
(Flag125prime-)
)

)
(:action Flag125Action-19
:parameters ()
:precondition
(and
(Flag43-)
(Flag43prime-)
)
:effect
(and
(Flag125-)
(Flag125prime-)
)

)
(:action Flag125Action-20
:parameters ()
:precondition
(and
(Flag77-)
(Flag77prime-)
)
:effect
(and
(Flag125-)
(Flag125prime-)
)

)
(:action Flag125Action-21
:parameters ()
:precondition
(and
(Flag104-)
(Flag104prime-)
)
:effect
(and
(Flag125-)
(Flag125prime-)
)

)
(:action Flag125Action-22
:parameters ()
:precondition
(and
(Flag44-)
(Flag44prime-)
)
:effect
(and
(Flag125-)
(Flag125prime-)
)

)
(:action Flag125Action-23
:parameters ()
:precondition
(and
(Flag98-)
(Flag98prime-)
)
:effect
(and
(Flag125-)
(Flag125prime-)
)

)
(:action Flag125Action-24
:parameters ()
:precondition
(and
(Flag46-)
(Flag46prime-)
)
:effect
(and
(Flag125-)
(Flag125prime-)
)

)
(:action Flag125Action-25
:parameters ()
:precondition
(and
(Flag69-)
(Flag69prime-)
)
:effect
(and
(Flag125-)
(Flag125prime-)
)

)
(:action Flag125Action-26
:parameters ()
:precondition
(and
(Flag79-)
(Flag79prime-)
)
:effect
(and
(Flag125-)
(Flag125prime-)
)

)
(:action Flag125Action-27
:parameters ()
:precondition
(and
(Flag48-)
(Flag48prime-)
)
:effect
(and
(Flag125-)
(Flag125prime-)
)

)
(:action Flag125Action-28
:parameters ()
:precondition
(and
(Flag115-)
(Flag115prime-)
)
:effect
(and
(Flag125-)
(Flag125prime-)
)

)
(:action Flag125Action-29
:parameters ()
:precondition
(and
(Flag99-)
(Flag99prime-)
)
:effect
(and
(Flag125-)
(Flag125prime-)
)

)
(:action Flag125Action-30
:parameters ()
:precondition
(and
(Flag116-)
(Flag116prime-)
)
:effect
(and
(Flag125-)
(Flag125prime-)
)

)
(:action Flag125Action-31
:parameters ()
:precondition
(and
(Flag81-)
(Flag81prime-)
)
:effect
(and
(Flag125-)
(Flag125prime-)
)

)
(:action Flag125Action-32
:parameters ()
:precondition
(and
(Flag49-)
(Flag49prime-)
)
:effect
(and
(Flag125-)
(Flag125prime-)
)

)
(:action Flag125Action-33
:parameters ()
:precondition
(and
(Flag50-)
(Flag50prime-)
)
:effect
(and
(Flag125-)
(Flag125prime-)
)

)
(:action Flag125Action-34
:parameters ()
:precondition
(and
(Flag100-)
(Flag100prime-)
)
:effect
(and
(Flag125-)
(Flag125prime-)
)

)
(:action Flag125Action-35
:parameters ()
:precondition
(and
(Flag101-)
(Flag101prime-)
)
:effect
(and
(Flag125-)
(Flag125prime-)
)

)
(:action Flag125Action-36
:parameters ()
:precondition
(and
(Flag80-)
(Flag80prime-)
)
:effect
(and
(Flag125-)
(Flag125prime-)
)

)
(:action Flag125Action-37
:parameters ()
:precondition
(and
(Flag53-)
(Flag53prime-)
)
:effect
(and
(Flag125-)
(Flag125prime-)
)

)
(:action Flag125Action-38
:parameters ()
:precondition
(and
(Flag117-)
(Flag117prime-)
)
:effect
(and
(Flag125-)
(Flag125prime-)
)

)
(:action Flag125Action-39
:parameters ()
:precondition
(and
(Flag55-)
(Flag55prime-)
)
:effect
(and
(Flag125-)
(Flag125prime-)
)

)
(:action Flag125Action-40
:parameters ()
:precondition
(and
(Flag56-)
(Flag56prime-)
)
:effect
(and
(Flag125-)
(Flag125prime-)
)

)
(:action Flag125Action-41
:parameters ()
:precondition
(and
(Flag84-)
(Flag84prime-)
)
:effect
(and
(Flag125-)
(Flag125prime-)
)

)
(:action Flag125Action-42
:parameters ()
:precondition
(and
(Flag57-)
(Flag57prime-)
)
:effect
(and
(Flag125-)
(Flag125prime-)
)

)
(:action Flag125Action-43
:parameters ()
:precondition
(and
(Flag118-)
(Flag118prime-)
)
:effect
(and
(Flag125-)
(Flag125prime-)
)

)
(:action Flag125Action-44
:parameters ()
:precondition
(and
(Flag85-)
(Flag85prime-)
)
:effect
(and
(Flag125-)
(Flag125prime-)
)

)
(:action Flag125Action-45
:parameters ()
:precondition
(and
(Flag59-)
(Flag59prime-)
)
:effect
(and
(Flag125-)
(Flag125prime-)
)

)
(:action Flag125Action-46
:parameters ()
:precondition
(and
(Flag119-)
(Flag119prime-)
)
:effect
(and
(Flag125-)
(Flag125prime-)
)

)
(:action Flag125Action-47
:parameters ()
:precondition
(and
(Flag67-)
(Flag67prime-)
)
:effect
(and
(Flag125-)
(Flag125prime-)
)

)
(:action Flag125Action-48
:parameters ()
:precondition
(and
(Flag60-)
(Flag60prime-)
)
:effect
(and
(Flag125-)
(Flag125prime-)
)

)
(:action Flag125Action-49
:parameters ()
:precondition
(and
(Flag58-)
(Flag58prime-)
)
:effect
(and
(Flag125-)
(Flag125prime-)
)

)
(:action Flag125Action-50
:parameters ()
:precondition
(and
(Flag61-)
(Flag61prime-)
)
:effect
(and
(Flag125-)
(Flag125prime-)
)

)
(:action Flag125Action-51
:parameters ()
:precondition
(and
(Flag103-)
(Flag103prime-)
)
:effect
(and
(Flag125-)
(Flag125prime-)
)

)
(:action Flag125Action-52
:parameters ()
:precondition
(and
(Flag96-)
(Flag96prime-)
)
:effect
(and
(Flag125-)
(Flag125prime-)
)

)
(:action Flag125Action-53
:parameters ()
:precondition
(and
(Flag63-)
(Flag63prime-)
)
:effect
(and
(Flag125-)
(Flag125prime-)
)

)
(:action Flag125Action-54
:parameters ()
:precondition
(and
(Flag64-)
(Flag64prime-)
)
:effect
(and
(Flag125-)
(Flag125prime-)
)

)
(:action Flag125Action-55
:parameters ()
:precondition
(and
(Flag65-)
(Flag65prime-)
)
:effect
(and
(Flag125-)
(Flag125prime-)
)

)
(:action Flag125Action-56
:parameters ()
:precondition
(and
(Flag66-)
(Flag66prime-)
)
:effect
(and
(Flag125-)
(Flag125prime-)
)

)
(:action Flag125Action-57
:parameters ()
:precondition
(and
(Flag120-)
(Flag120prime-)
)
:effect
(and
(Flag125-)
(Flag125prime-)
)

)
(:action Flag125Action-58
:parameters ()
:precondition
(and
(Flag105-)
(Flag105prime-)
)
:effect
(and
(Flag125-)
(Flag125prime-)
)

)
(:action Flag125Action-59
:parameters ()
:precondition
(and
(Flag88-)
(Flag88prime-)
)
:effect
(and
(Flag125-)
(Flag125prime-)
)

)
(:action Flag125Action-60
:parameters ()
:precondition
(and
(Flag107-)
(Flag107prime-)
)
:effect
(and
(Flag125-)
(Flag125prime-)
)

)
(:action Flag125Action-61
:parameters ()
:precondition
(and
(Flag121-)
(Flag121prime-)
)
:effect
(and
(Flag125-)
(Flag125prime-)
)

)
(:action Flag125Action-62
:parameters ()
:precondition
(and
(Flag108-)
(Flag108prime-)
)
:effect
(and
(Flag125-)
(Flag125prime-)
)

)
(:action Flag125Action-63
:parameters ()
:precondition
(and
(Flag89-)
(Flag89prime-)
)
:effect
(and
(Flag125-)
(Flag125prime-)
)

)
(:action Flag125Action-64
:parameters ()
:precondition
(and
(Flag52-)
(Flag52prime-)
)
:effect
(and
(Flag125-)
(Flag125prime-)
)

)
(:action Flag125Action-65
:parameters ()
:precondition
(and
(Flag68-)
(Flag68prime-)
)
:effect
(and
(Flag125-)
(Flag125prime-)
)

)
(:action Flag125Action-66
:parameters ()
:precondition
(and
(Flag122-)
(Flag122prime-)
)
:effect
(and
(Flag125-)
(Flag125prime-)
)

)
(:action Flag125Action-67
:parameters ()
:precondition
(and
(Flag90-)
(Flag90prime-)
)
:effect
(and
(Flag125-)
(Flag125prime-)
)

)
(:action Flag125Action-68
:parameters ()
:precondition
(and
(Flag123-)
(Flag123prime-)
)
:effect
(and
(Flag125-)
(Flag125prime-)
)

)
(:action Flag125Action-69
:parameters ()
:precondition
(and
(Flag70-)
(Flag70prime-)
)
:effect
(and
(Flag125-)
(Flag125prime-)
)

)
(:action Flag125Action-70
:parameters ()
:precondition
(and
(Flag71-)
(Flag71prime-)
)
:effect
(and
(Flag125-)
(Flag125prime-)
)

)
(:action Flag125Action-71
:parameters ()
:precondition
(and
(Flag86-)
(Flag86prime-)
)
:effect
(and
(Flag125-)
(Flag125prime-)
)

)
(:action Flag125Action-72
:parameters ()
:precondition
(and
(Flag124-)
(Flag124prime-)
)
:effect
(and
(Flag125-)
(Flag125prime-)
)

)
(:action Prim125Action
:parameters ()
:precondition
(and
(not (Flag38-))
(Flag38prime-)
(not (Flag110-))
(Flag110prime-)
(not (Flag78-))
(Flag78prime-)
(not (Flag111-))
(Flag111prime-)
(not (Flag74-))
(Flag74prime-)
(not (Flag92-))
(Flag92prime-)
(not (Flag95-))
(Flag95prime-)
(not (Flag93-))
(Flag93prime-)
(not (Flag106-))
(Flag106prime-)
(not (Flag112-))
(Flag112prime-)
(not (Flag75-))
(Flag75prime-)
(not (Flag113-))
(Flag113prime-)
(not (Flag76-))
(Flag76prime-)
(not (Flag45-))
(Flag45prime-)
(not (Flag114-))
(Flag114prime-)
(not (Flag40-))
(Flag40prime-)
(not (Flag51-))
(Flag51prime-)
(not (Flag42-))
(Flag42prime-)
(not (Flag97-))
(Flag97prime-)
(not (Flag43-))
(Flag43prime-)
(not (Flag77-))
(Flag77prime-)
(not (Flag104-))
(Flag104prime-)
(not (Flag44-))
(Flag44prime-)
(not (Flag98-))
(Flag98prime-)
(not (Flag46-))
(Flag46prime-)
(not (Flag69-))
(Flag69prime-)
(not (Flag79-))
(Flag79prime-)
(not (Flag48-))
(Flag48prime-)
(not (Flag115-))
(Flag115prime-)
(not (Flag99-))
(Flag99prime-)
(not (Flag116-))
(Flag116prime-)
(not (Flag81-))
(Flag81prime-)
(not (Flag49-))
(Flag49prime-)
(not (Flag50-))
(Flag50prime-)
(not (Flag100-))
(Flag100prime-)
(not (Flag101-))
(Flag101prime-)
(not (Flag80-))
(Flag80prime-)
(not (Flag53-))
(Flag53prime-)
(not (Flag117-))
(Flag117prime-)
(not (Flag55-))
(Flag55prime-)
(not (Flag56-))
(Flag56prime-)
(not (Flag84-))
(Flag84prime-)
(not (Flag57-))
(Flag57prime-)
(not (Flag118-))
(Flag118prime-)
(not (Flag85-))
(Flag85prime-)
(not (Flag59-))
(Flag59prime-)
(not (Flag119-))
(Flag119prime-)
(not (Flag67-))
(Flag67prime-)
(not (Flag60-))
(Flag60prime-)
(not (Flag58-))
(Flag58prime-)
(not (Flag61-))
(Flag61prime-)
(not (Flag103-))
(Flag103prime-)
(not (Flag96-))
(Flag96prime-)
(not (Flag63-))
(Flag63prime-)
(not (Flag64-))
(Flag64prime-)
(not (Flag65-))
(Flag65prime-)
(not (Flag66-))
(Flag66prime-)
(not (Flag120-))
(Flag120prime-)
(not (Flag105-))
(Flag105prime-)
(not (Flag88-))
(Flag88prime-)
(not (Flag107-))
(Flag107prime-)
(not (Flag121-))
(Flag121prime-)
(not (Flag108-))
(Flag108prime-)
(not (Flag89-))
(Flag89prime-)
(not (Flag52-))
(Flag52prime-)
(not (Flag68-))
(Flag68prime-)
(not (Flag122-))
(Flag122prime-)
(not (Flag90-))
(Flag90prime-)
(not (Flag123-))
(Flag123prime-)
(not (Flag70-))
(Flag70prime-)
(not (Flag71-))
(Flag71prime-)
(not (Flag86-))
(Flag86prime-)
(not (Flag124-))
(Flag124prime-)
)
:effect
(and
(Flag125prime-)
(not (Flag125-))
)

)
(:action Flag141Action-0
:parameters ()
:precondition
(and
(Flag38-)
(Flag38prime-)
)
:effect
(and
(Flag141-)
(Flag141prime-)
)

)
(:action Flag141Action-1
:parameters ()
:precondition
(and
(Flag108-)
(Flag108prime-)
)
:effect
(and
(Flag141-)
(Flag141prime-)
)

)
(:action Flag141Action-2
:parameters ()
:precondition
(and
(Flag110-)
(Flag110prime-)
)
:effect
(and
(Flag141-)
(Flag141prime-)
)

)
(:action Flag141Action-3
:parameters ()
:precondition
(and
(Flag78-)
(Flag78prime-)
)
:effect
(and
(Flag141-)
(Flag141prime-)
)

)
(:action Flag141Action-4
:parameters ()
:precondition
(and
(Flag74-)
(Flag74prime-)
)
:effect
(and
(Flag141-)
(Flag141prime-)
)

)
(:action Flag141Action-5
:parameters ()
:precondition
(and
(Flag92-)
(Flag92prime-)
)
:effect
(and
(Flag141-)
(Flag141prime-)
)

)
(:action Flag141Action-6
:parameters ()
:precondition
(and
(Flag69-)
(Flag69prime-)
)
:effect
(and
(Flag141-)
(Flag141prime-)
)

)
(:action Flag141Action-7
:parameters ()
:precondition
(and
(Flag126-)
(Flag126prime-)
)
:effect
(and
(Flag141-)
(Flag141prime-)
)

)
(:action Flag141Action-8
:parameters ()
:precondition
(and
(Flag93-)
(Flag93prime-)
)
:effect
(and
(Flag141-)
(Flag141prime-)
)

)
(:action Flag141Action-9
:parameters ()
:precondition
(and
(Flag77-)
(Flag77prime-)
)
:effect
(and
(Flag141-)
(Flag141prime-)
)

)
(:action Flag141Action-10
:parameters ()
:precondition
(and
(Flag48-)
(Flag48prime-)
)
:effect
(and
(Flag141-)
(Flag141prime-)
)

)
(:action Flag141Action-11
:parameters ()
:precondition
(and
(Flag127-)
(Flag127prime-)
)
:effect
(and
(Flag141-)
(Flag141prime-)
)

)
(:action Flag141Action-12
:parameters ()
:precondition
(and
(Flag112-)
(Flag112prime-)
)
:effect
(and
(Flag141-)
(Flag141prime-)
)

)
(:action Flag141Action-13
:parameters ()
:precondition
(and
(Flag128-)
(Flag128prime-)
)
:effect
(and
(Flag141-)
(Flag141prime-)
)

)
(:action Flag141Action-14
:parameters ()
:precondition
(and
(Flag75-)
(Flag75prime-)
)
:effect
(and
(Flag141-)
(Flag141prime-)
)

)
(:action Flag141Action-15
:parameters ()
:precondition
(and
(Flag113-)
(Flag113prime-)
)
:effect
(and
(Flag141-)
(Flag141prime-)
)

)
(:action Flag141Action-16
:parameters ()
:precondition
(and
(Flag76-)
(Flag76prime-)
)
:effect
(and
(Flag141-)
(Flag141prime-)
)

)
(:action Flag141Action-17
:parameters ()
:precondition
(and
(Flag45-)
(Flag45prime-)
)
:effect
(and
(Flag141-)
(Flag141prime-)
)

)
(:action Flag141Action-18
:parameters ()
:precondition
(and
(Flag57-)
(Flag57prime-)
)
:effect
(and
(Flag141-)
(Flag141prime-)
)

)
(:action Flag141Action-19
:parameters ()
:precondition
(and
(Flag129-)
(Flag129prime-)
)
:effect
(and
(Flag141-)
(Flag141prime-)
)

)
(:action Flag141Action-20
:parameters ()
:precondition
(and
(Flag130-)
(Flag130prime-)
)
:effect
(and
(Flag141-)
(Flag141prime-)
)

)
(:action Flag141Action-21
:parameters ()
:precondition
(and
(Flag114-)
(Flag114prime-)
)
:effect
(and
(Flag141-)
(Flag141prime-)
)

)
(:action Flag141Action-22
:parameters ()
:precondition
(and
(Flag40-)
(Flag40prime-)
)
:effect
(and
(Flag141-)
(Flag141prime-)
)

)
(:action Flag141Action-23
:parameters ()
:precondition
(and
(Flag131-)
(Flag131prime-)
)
:effect
(and
(Flag141-)
(Flag141prime-)
)

)
(:action Flag141Action-24
:parameters ()
:precondition
(and
(Flag51-)
(Flag51prime-)
)
:effect
(and
(Flag141-)
(Flag141prime-)
)

)
(:action Flag141Action-25
:parameters ()
:precondition
(and
(Flag42-)
(Flag42prime-)
)
:effect
(and
(Flag141-)
(Flag141prime-)
)

)
(:action Flag141Action-26
:parameters ()
:precondition
(and
(Flag80-)
(Flag80prime-)
)
:effect
(and
(Flag141-)
(Flag141prime-)
)

)
(:action Flag141Action-27
:parameters ()
:precondition
(and
(Flag43-)
(Flag43prime-)
)
:effect
(and
(Flag141-)
(Flag141prime-)
)

)
(:action Flag141Action-28
:parameters ()
:precondition
(and
(Flag86-)
(Flag86prime-)
)
:effect
(and
(Flag141-)
(Flag141prime-)
)

)
(:action Flag141Action-29
:parameters ()
:precondition
(and
(Flag104-)
(Flag104prime-)
)
:effect
(and
(Flag141-)
(Flag141prime-)
)

)
(:action Flag141Action-30
:parameters ()
:precondition
(and
(Flag63-)
(Flag63prime-)
)
:effect
(and
(Flag141-)
(Flag141prime-)
)

)
(:action Flag141Action-31
:parameters ()
:precondition
(and
(Flag44-)
(Flag44prime-)
)
:effect
(and
(Flag141-)
(Flag141prime-)
)

)
(:action Flag141Action-32
:parameters ()
:precondition
(and
(Flag55-)
(Flag55prime-)
)
:effect
(and
(Flag141-)
(Flag141prime-)
)

)
(:action Flag141Action-33
:parameters ()
:precondition
(and
(Flag46-)
(Flag46prime-)
)
:effect
(and
(Flag141-)
(Flag141prime-)
)

)
(:action Flag141Action-34
:parameters ()
:precondition
(and
(Flag121-)
(Flag121prime-)
)
:effect
(and
(Flag141-)
(Flag141prime-)
)

)
(:action Flag141Action-35
:parameters ()
:precondition
(and
(Flag79-)
(Flag79prime-)
)
:effect
(and
(Flag141-)
(Flag141prime-)
)

)
(:action Flag141Action-36
:parameters ()
:precondition
(and
(Flag95-)
(Flag95prime-)
)
:effect
(and
(Flag141-)
(Flag141prime-)
)

)
(:action Flag141Action-37
:parameters ()
:precondition
(and
(Flag132-)
(Flag132prime-)
)
:effect
(and
(Flag141-)
(Flag141prime-)
)

)
(:action Flag141Action-38
:parameters ()
:precondition
(and
(Flag133-)
(Flag133prime-)
)
:effect
(and
(Flag141-)
(Flag141prime-)
)

)
(:action Flag141Action-39
:parameters ()
:precondition
(and
(Flag115-)
(Flag115prime-)
)
:effect
(and
(Flag141-)
(Flag141prime-)
)

)
(:action Flag141Action-40
:parameters ()
:precondition
(and
(Flag99-)
(Flag99prime-)
)
:effect
(and
(Flag141-)
(Flag141prime-)
)

)
(:action Flag141Action-41
:parameters ()
:precondition
(and
(Flag116-)
(Flag116prime-)
)
:effect
(and
(Flag141-)
(Flag141prime-)
)

)
(:action Flag141Action-42
:parameters ()
:precondition
(and
(Flag81-)
(Flag81prime-)
)
:effect
(and
(Flag141-)
(Flag141prime-)
)

)
(:action Flag141Action-43
:parameters ()
:precondition
(and
(Flag49-)
(Flag49prime-)
)
:effect
(and
(Flag141-)
(Flag141prime-)
)

)
(:action Flag141Action-44
:parameters ()
:precondition
(and
(Flag59-)
(Flag59prime-)
)
:effect
(and
(Flag141-)
(Flag141prime-)
)

)
(:action Flag141Action-45
:parameters ()
:precondition
(and
(Flag100-)
(Flag100prime-)
)
:effect
(and
(Flag141-)
(Flag141prime-)
)

)
(:action Flag141Action-46
:parameters ()
:precondition
(and
(Flag101-)
(Flag101prime-)
)
:effect
(and
(Flag141-)
(Flag141prime-)
)

)
(:action Flag141Action-47
:parameters ()
:precondition
(and
(Flag53-)
(Flag53prime-)
)
:effect
(and
(Flag141-)
(Flag141prime-)
)

)
(:action Flag141Action-48
:parameters ()
:precondition
(and
(Flag98-)
(Flag98prime-)
)
:effect
(and
(Flag141-)
(Flag141prime-)
)

)
(:action Flag141Action-49
:parameters ()
:precondition
(and
(Flag56-)
(Flag56prime-)
)
:effect
(and
(Flag141-)
(Flag141prime-)
)

)
(:action Flag141Action-50
:parameters ()
:precondition
(and
(Flag84-)
(Flag84prime-)
)
:effect
(and
(Flag141-)
(Flag141prime-)
)

)
(:action Flag141Action-51
:parameters ()
:precondition
(and
(Flag134-)
(Flag134prime-)
)
:effect
(and
(Flag141-)
(Flag141prime-)
)

)
(:action Flag141Action-52
:parameters ()
:precondition
(and
(Flag58-)
(Flag58prime-)
)
:effect
(and
(Flag141-)
(Flag141prime-)
)

)
(:action Flag141Action-53
:parameters ()
:precondition
(and
(Flag119-)
(Flag119prime-)
)
:effect
(and
(Flag141-)
(Flag141prime-)
)

)
(:action Flag141Action-54
:parameters ()
:precondition
(and
(Flag67-)
(Flag67prime-)
)
:effect
(and
(Flag141-)
(Flag141prime-)
)

)
(:action Flag141Action-55
:parameters ()
:precondition
(and
(Flag60-)
(Flag60prime-)
)
:effect
(and
(Flag141-)
(Flag141prime-)
)

)
(:action Flag141Action-56
:parameters ()
:precondition
(and
(Flag135-)
(Flag135prime-)
)
:effect
(and
(Flag141-)
(Flag141prime-)
)

)
(:action Flag141Action-57
:parameters ()
:precondition
(and
(Flag103-)
(Flag103prime-)
)
:effect
(and
(Flag141-)
(Flag141prime-)
)

)
(:action Flag141Action-58
:parameters ()
:precondition
(and
(Flag136-)
(Flag136prime-)
)
:effect
(and
(Flag141-)
(Flag141prime-)
)

)
(:action Flag141Action-59
:parameters ()
:precondition
(and
(Flag96-)
(Flag96prime-)
)
:effect
(and
(Flag141-)
(Flag141prime-)
)

)
(:action Flag141Action-60
:parameters ()
:precondition
(and
(Flag137-)
(Flag137prime-)
)
:effect
(and
(Flag141-)
(Flag141prime-)
)

)
(:action Flag141Action-61
:parameters ()
:precondition
(and
(Flag64-)
(Flag64prime-)
)
:effect
(and
(Flag141-)
(Flag141prime-)
)

)
(:action Flag141Action-62
:parameters ()
:precondition
(and
(Flag65-)
(Flag65prime-)
)
:effect
(and
(Flag141-)
(Flag141prime-)
)

)
(:action Flag141Action-63
:parameters ()
:precondition
(and
(Flag117-)
(Flag117prime-)
)
:effect
(and
(Flag141-)
(Flag141prime-)
)

)
(:action Flag141Action-64
:parameters ()
:precondition
(and
(Flag120-)
(Flag120prime-)
)
:effect
(and
(Flag141-)
(Flag141prime-)
)

)
(:action Flag141Action-65
:parameters ()
:precondition
(and
(Flag105-)
(Flag105prime-)
)
:effect
(and
(Flag141-)
(Flag141prime-)
)

)
(:action Flag141Action-66
:parameters ()
:precondition
(and
(Flag106-)
(Flag106prime-)
)
:effect
(and
(Flag141-)
(Flag141prime-)
)

)
(:action Flag141Action-67
:parameters ()
:precondition
(and
(Flag88-)
(Flag88prime-)
)
:effect
(and
(Flag141-)
(Flag141prime-)
)

)
(:action Flag141Action-68
:parameters ()
:precondition
(and
(Flag107-)
(Flag107prime-)
)
:effect
(and
(Flag141-)
(Flag141prime-)
)

)
(:action Flag141Action-69
:parameters ()
:precondition
(and
(Flag138-)
(Flag138prime-)
)
:effect
(and
(Flag141-)
(Flag141prime-)
)

)
(:action Flag141Action-70
:parameters ()
:precondition
(and
(Flag89-)
(Flag89prime-)
)
:effect
(and
(Flag141-)
(Flag141prime-)
)

)
(:action Flag141Action-71
:parameters ()
:precondition
(and
(Flag52-)
(Flag52prime-)
)
:effect
(and
(Flag141-)
(Flag141prime-)
)

)
(:action Flag141Action-72
:parameters ()
:precondition
(and
(Flag139-)
(Flag139prime-)
)
:effect
(and
(Flag141-)
(Flag141prime-)
)

)
(:action Flag141Action-73
:parameters ()
:precondition
(and
(Flag68-)
(Flag68prime-)
)
:effect
(and
(Flag141-)
(Flag141prime-)
)

)
(:action Flag141Action-74
:parameters ()
:precondition
(and
(Flag122-)
(Flag122prime-)
)
:effect
(and
(Flag141-)
(Flag141prime-)
)

)
(:action Flag141Action-75
:parameters ()
:precondition
(and
(Flag90-)
(Flag90prime-)
)
:effect
(and
(Flag141-)
(Flag141prime-)
)

)
(:action Flag141Action-76
:parameters ()
:precondition
(and
(Flag123-)
(Flag123prime-)
)
:effect
(and
(Flag141-)
(Flag141prime-)
)

)
(:action Flag141Action-77
:parameters ()
:precondition
(and
(Flag140-)
(Flag140prime-)
)
:effect
(and
(Flag141-)
(Flag141prime-)
)

)
(:action Flag141Action-78
:parameters ()
:precondition
(and
(Flag70-)
(Flag70prime-)
)
:effect
(and
(Flag141-)
(Flag141prime-)
)

)
(:action Flag141Action-79
:parameters ()
:precondition
(and
(Flag71-)
(Flag71prime-)
)
:effect
(and
(Flag141-)
(Flag141prime-)
)

)
(:action Flag141Action-80
:parameters ()
:precondition
(and
(Flag61-)
(Flag61prime-)
)
:effect
(and
(Flag141-)
(Flag141prime-)
)

)
(:action Flag141Action-81
:parameters ()
:precondition
(and
(Flag124-)
(Flag124prime-)
)
:effect
(and
(Flag141-)
(Flag141prime-)
)

)
(:action Prim141Action
:parameters ()
:precondition
(and
(not (Flag38-))
(Flag38prime-)
(not (Flag108-))
(Flag108prime-)
(not (Flag110-))
(Flag110prime-)
(not (Flag78-))
(Flag78prime-)
(not (Flag74-))
(Flag74prime-)
(not (Flag92-))
(Flag92prime-)
(not (Flag69-))
(Flag69prime-)
(not (Flag126-))
(Flag126prime-)
(not (Flag93-))
(Flag93prime-)
(not (Flag77-))
(Flag77prime-)
(not (Flag48-))
(Flag48prime-)
(not (Flag127-))
(Flag127prime-)
(not (Flag112-))
(Flag112prime-)
(not (Flag128-))
(Flag128prime-)
(not (Flag75-))
(Flag75prime-)
(not (Flag113-))
(Flag113prime-)
(not (Flag76-))
(Flag76prime-)
(not (Flag45-))
(Flag45prime-)
(not (Flag57-))
(Flag57prime-)
(not (Flag129-))
(Flag129prime-)
(not (Flag130-))
(Flag130prime-)
(not (Flag114-))
(Flag114prime-)
(not (Flag40-))
(Flag40prime-)
(not (Flag131-))
(Flag131prime-)
(not (Flag51-))
(Flag51prime-)
(not (Flag42-))
(Flag42prime-)
(not (Flag80-))
(Flag80prime-)
(not (Flag43-))
(Flag43prime-)
(not (Flag86-))
(Flag86prime-)
(not (Flag104-))
(Flag104prime-)
(not (Flag63-))
(Flag63prime-)
(not (Flag44-))
(Flag44prime-)
(not (Flag55-))
(Flag55prime-)
(not (Flag46-))
(Flag46prime-)
(not (Flag121-))
(Flag121prime-)
(not (Flag79-))
(Flag79prime-)
(not (Flag95-))
(Flag95prime-)
(not (Flag132-))
(Flag132prime-)
(not (Flag133-))
(Flag133prime-)
(not (Flag115-))
(Flag115prime-)
(not (Flag99-))
(Flag99prime-)
(not (Flag116-))
(Flag116prime-)
(not (Flag81-))
(Flag81prime-)
(not (Flag49-))
(Flag49prime-)
(not (Flag59-))
(Flag59prime-)
(not (Flag100-))
(Flag100prime-)
(not (Flag101-))
(Flag101prime-)
(not (Flag53-))
(Flag53prime-)
(not (Flag98-))
(Flag98prime-)
(not (Flag56-))
(Flag56prime-)
(not (Flag84-))
(Flag84prime-)
(not (Flag134-))
(Flag134prime-)
(not (Flag58-))
(Flag58prime-)
(not (Flag119-))
(Flag119prime-)
(not (Flag67-))
(Flag67prime-)
(not (Flag60-))
(Flag60prime-)
(not (Flag135-))
(Flag135prime-)
(not (Flag103-))
(Flag103prime-)
(not (Flag136-))
(Flag136prime-)
(not (Flag96-))
(Flag96prime-)
(not (Flag137-))
(Flag137prime-)
(not (Flag64-))
(Flag64prime-)
(not (Flag65-))
(Flag65prime-)
(not (Flag117-))
(Flag117prime-)
(not (Flag120-))
(Flag120prime-)
(not (Flag105-))
(Flag105prime-)
(not (Flag106-))
(Flag106prime-)
(not (Flag88-))
(Flag88prime-)
(not (Flag107-))
(Flag107prime-)
(not (Flag138-))
(Flag138prime-)
(not (Flag89-))
(Flag89prime-)
(not (Flag52-))
(Flag52prime-)
(not (Flag139-))
(Flag139prime-)
(not (Flag68-))
(Flag68prime-)
(not (Flag122-))
(Flag122prime-)
(not (Flag90-))
(Flag90prime-)
(not (Flag123-))
(Flag123prime-)
(not (Flag140-))
(Flag140prime-)
(not (Flag70-))
(Flag70prime-)
(not (Flag71-))
(Flag71prime-)
(not (Flag61-))
(Flag61prime-)
(not (Flag124-))
(Flag124prime-)
)
:effect
(and
(Flag141prime-)
(not (Flag141-))
)

)
(:action Flag156Action-0
:parameters ()
:precondition
(and
(Flag142-)
(Flag142prime-)
)
:effect
(and
(Flag156-)
(Flag156prime-)
)

)
(:action Flag156Action-1
:parameters ()
:precondition
(and
(Flag42-)
(Flag42prime-)
)
:effect
(and
(Flag156-)
(Flag156prime-)
)

)
(:action Flag156Action-2
:parameters ()
:precondition
(and
(Flag113-)
(Flag113prime-)
)
:effect
(and
(Flag156-)
(Flag156prime-)
)

)
(:action Flag156Action-3
:parameters ()
:precondition
(and
(Flag44-)
(Flag44prime-)
)
:effect
(and
(Flag156-)
(Flag156prime-)
)

)
(:action Flag156Action-4
:parameters ()
:precondition
(and
(Flag46-)
(Flag46prime-)
)
:effect
(and
(Flag156-)
(Flag156prime-)
)

)
(:action Flag156Action-5
:parameters ()
:precondition
(and
(Flag79-)
(Flag79prime-)
)
:effect
(and
(Flag156-)
(Flag156prime-)
)

)
(:action Flag156Action-6
:parameters ()
:precondition
(and
(Flag95-)
(Flag95prime-)
)
:effect
(and
(Flag156-)
(Flag156prime-)
)

)
(:action Flag156Action-7
:parameters ()
:precondition
(and
(Flag80-)
(Flag80prime-)
)
:effect
(and
(Flag156-)
(Flag156prime-)
)

)
(:action Flag156Action-8
:parameters ()
:precondition
(and
(Flag116-)
(Flag116prime-)
)
:effect
(and
(Flag156-)
(Flag156prime-)
)

)
(:action Flag156Action-9
:parameters ()
:precondition
(and
(Flag122-)
(Flag122prime-)
)
:effect
(and
(Flag156-)
(Flag156prime-)
)

)
(:action Flag156Action-10
:parameters ()
:precondition
(and
(Flag59-)
(Flag59prime-)
)
:effect
(and
(Flag156-)
(Flag156prime-)
)

)
(:action Flag156Action-11
:parameters ()
:precondition
(and
(Flag143-)
(Flag143prime-)
)
:effect
(and
(Flag156-)
(Flag156prime-)
)

)
(:action Flag156Action-12
:parameters ()
:precondition
(and
(Flag144-)
(Flag144prime-)
)
:effect
(and
(Flag156-)
(Flag156prime-)
)

)
(:action Flag156Action-13
:parameters ()
:precondition
(and
(Flag55-)
(Flag55prime-)
)
:effect
(and
(Flag156-)
(Flag156prime-)
)

)
(:action Flag156Action-14
:parameters ()
:precondition
(and
(Flag81-)
(Flag81prime-)
)
:effect
(and
(Flag156-)
(Flag156prime-)
)

)
(:action Flag156Action-15
:parameters ()
:precondition
(and
(Flag86-)
(Flag86prime-)
)
:effect
(and
(Flag156-)
(Flag156prime-)
)

)
(:action Flag156Action-16
:parameters ()
:precondition
(and
(Flag103-)
(Flag103prime-)
)
:effect
(and
(Flag156-)
(Flag156prime-)
)

)
(:action Flag156Action-17
:parameters ()
:precondition
(and
(Flag112-)
(Flag112prime-)
)
:effect
(and
(Flag156-)
(Flag156prime-)
)

)
(:action Flag156Action-18
:parameters ()
:precondition
(and
(Flag63-)
(Flag63prime-)
)
:effect
(and
(Flag156-)
(Flag156prime-)
)

)
(:action Flag156Action-19
:parameters ()
:precondition
(and
(Flag120-)
(Flag120prime-)
)
:effect
(and
(Flag156-)
(Flag156prime-)
)

)
(:action Flag156Action-20
:parameters ()
:precondition
(and
(Flag145-)
(Flag145prime-)
)
:effect
(and
(Flag156-)
(Flag156prime-)
)

)
(:action Flag156Action-21
:parameters ()
:precondition
(and
(Flag121-)
(Flag121prime-)
)
:effect
(and
(Flag156-)
(Flag156prime-)
)

)
(:action Flag156Action-22
:parameters ()
:precondition
(and
(Flag56-)
(Flag56prime-)
)
:effect
(and
(Flag156-)
(Flag156prime-)
)

)
(:action Flag156Action-23
:parameters ()
:precondition
(and
(Flag45-)
(Flag45prime-)
)
:effect
(and
(Flag156-)
(Flag156prime-)
)

)
(:action Flag156Action-24
:parameters ()
:precondition
(and
(Flag93-)
(Flag93prime-)
)
:effect
(and
(Flag156-)
(Flag156prime-)
)

)
(:action Flag156Action-25
:parameters ()
:precondition
(and
(Flag128-)
(Flag128prime-)
)
:effect
(and
(Flag156-)
(Flag156prime-)
)

)
(:action Flag156Action-26
:parameters ()
:precondition
(and
(Flag146-)
(Flag146prime-)
)
:effect
(and
(Flag156-)
(Flag156prime-)
)

)
(:action Flag156Action-27
:parameters ()
:precondition
(and
(Flag76-)
(Flag76prime-)
)
:effect
(and
(Flag156-)
(Flag156prime-)
)

)
(:action Flag156Action-28
:parameters ()
:precondition
(and
(Flag129-)
(Flag129prime-)
)
:effect
(and
(Flag156-)
(Flag156prime-)
)

)
(:action Flag156Action-29
:parameters ()
:precondition
(and
(Flag130-)
(Flag130prime-)
)
:effect
(and
(Flag156-)
(Flag156prime-)
)

)
(:action Flag156Action-30
:parameters ()
:precondition
(and
(Flag78-)
(Flag78prime-)
)
:effect
(and
(Flag156-)
(Flag156prime-)
)

)
(:action Flag156Action-31
:parameters ()
:precondition
(and
(Flag115-)
(Flag115prime-)
)
:effect
(and
(Flag156-)
(Flag156prime-)
)

)
(:action Flag156Action-32
:parameters ()
:precondition
(and
(Flag49-)
(Flag49prime-)
)
:effect
(and
(Flag156-)
(Flag156prime-)
)

)
(:action Flag156Action-33
:parameters ()
:precondition
(and
(Flag147-)
(Flag147prime-)
)
:effect
(and
(Flag156-)
(Flag156prime-)
)

)
(:action Flag156Action-34
:parameters ()
:precondition
(and
(Flag98-)
(Flag98prime-)
)
:effect
(and
(Flag156-)
(Flag156prime-)
)

)
(:action Flag156Action-35
:parameters ()
:precondition
(and
(Flag58-)
(Flag58prime-)
)
:effect
(and
(Flag156-)
(Flag156prime-)
)

)
(:action Flag156Action-36
:parameters ()
:precondition
(and
(Flag60-)
(Flag60prime-)
)
:effect
(and
(Flag156-)
(Flag156prime-)
)

)
(:action Flag156Action-37
:parameters ()
:precondition
(and
(Flag135-)
(Flag135prime-)
)
:effect
(and
(Flag156-)
(Flag156prime-)
)

)
(:action Flag156Action-38
:parameters ()
:precondition
(and
(Flag96-)
(Flag96prime-)
)
:effect
(and
(Flag156-)
(Flag156prime-)
)

)
(:action Flag156Action-39
:parameters ()
:precondition
(and
(Flag64-)
(Flag64prime-)
)
:effect
(and
(Flag156-)
(Flag156prime-)
)

)
(:action Flag156Action-40
:parameters ()
:precondition
(and
(Flag104-)
(Flag104prime-)
)
:effect
(and
(Flag156-)
(Flag156prime-)
)

)
(:action Flag156Action-41
:parameters ()
:precondition
(and
(Flag105-)
(Flag105prime-)
)
:effect
(and
(Flag156-)
(Flag156prime-)
)

)
(:action Flag156Action-42
:parameters ()
:precondition
(and
(Flag68-)
(Flag68prime-)
)
:effect
(and
(Flag156-)
(Flag156prime-)
)

)
(:action Flag156Action-43
:parameters ()
:precondition
(and
(Flag89-)
(Flag89prime-)
)
:effect
(and
(Flag156-)
(Flag156prime-)
)

)
(:action Flag156Action-44
:parameters ()
:precondition
(and
(Flag139-)
(Flag139prime-)
)
:effect
(and
(Flag156-)
(Flag156prime-)
)

)
(:action Flag156Action-45
:parameters ()
:precondition
(and
(Flag123-)
(Flag123prime-)
)
:effect
(and
(Flag156-)
(Flag156prime-)
)

)
(:action Flag156Action-46
:parameters ()
:precondition
(and
(Flag70-)
(Flag70prime-)
)
:effect
(and
(Flag156-)
(Flag156prime-)
)

)
(:action Flag156Action-47
:parameters ()
:precondition
(and
(Flag71-)
(Flag71prime-)
)
:effect
(and
(Flag156-)
(Flag156prime-)
)

)
(:action Flag156Action-48
:parameters ()
:precondition
(and
(Flag38-)
(Flag38prime-)
)
:effect
(and
(Flag156-)
(Flag156prime-)
)

)
(:action Flag156Action-49
:parameters ()
:precondition
(and
(Flag110-)
(Flag110prime-)
)
:effect
(and
(Flag156-)
(Flag156prime-)
)

)
(:action Flag156Action-50
:parameters ()
:precondition
(and
(Flag148-)
(Flag148prime-)
)
:effect
(and
(Flag156-)
(Flag156prime-)
)

)
(:action Flag156Action-51
:parameters ()
:precondition
(and
(Flag126-)
(Flag126prime-)
)
:effect
(and
(Flag156-)
(Flag156prime-)
)

)
(:action Flag156Action-52
:parameters ()
:precondition
(and
(Flag149-)
(Flag149prime-)
)
:effect
(and
(Flag156-)
(Flag156prime-)
)

)
(:action Flag156Action-53
:parameters ()
:precondition
(and
(Flag138-)
(Flag138prime-)
)
:effect
(and
(Flag156-)
(Flag156prime-)
)

)
(:action Flag156Action-54
:parameters ()
:precondition
(and
(Flag114-)
(Flag114prime-)
)
:effect
(and
(Flag156-)
(Flag156prime-)
)

)
(:action Flag156Action-55
:parameters ()
:precondition
(and
(Flag40-)
(Flag40prime-)
)
:effect
(and
(Flag156-)
(Flag156prime-)
)

)
(:action Flag156Action-56
:parameters ()
:precondition
(and
(Flag77-)
(Flag77prime-)
)
:effect
(and
(Flag156-)
(Flag156prime-)
)

)
(:action Flag156Action-57
:parameters ()
:precondition
(and
(Flag92-)
(Flag92prime-)
)
:effect
(and
(Flag156-)
(Flag156prime-)
)

)
(:action Flag156Action-58
:parameters ()
:precondition
(and
(Flag106-)
(Flag106prime-)
)
:effect
(and
(Flag156-)
(Flag156prime-)
)

)
(:action Flag156Action-59
:parameters ()
:precondition
(and
(Flag100-)
(Flag100prime-)
)
:effect
(and
(Flag156-)
(Flag156prime-)
)

)
(:action Flag156Action-60
:parameters ()
:precondition
(and
(Flag52-)
(Flag52prime-)
)
:effect
(and
(Flag156-)
(Flag156prime-)
)

)
(:action Flag156Action-61
:parameters ()
:precondition
(and
(Flag53-)
(Flag53prime-)
)
:effect
(and
(Flag156-)
(Flag156prime-)
)

)
(:action Flag156Action-62
:parameters ()
:precondition
(and
(Flag84-)
(Flag84prime-)
)
:effect
(and
(Flag156-)
(Flag156prime-)
)

)
(:action Flag156Action-63
:parameters ()
:precondition
(and
(Flag57-)
(Flag57prime-)
)
:effect
(and
(Flag156-)
(Flag156prime-)
)

)
(:action Flag156Action-64
:parameters ()
:precondition
(and
(Flag119-)
(Flag119prime-)
)
:effect
(and
(Flag156-)
(Flag156prime-)
)

)
(:action Flag156Action-65
:parameters ()
:precondition
(and
(Flag67-)
(Flag67prime-)
)
:effect
(and
(Flag156-)
(Flag156prime-)
)

)
(:action Flag156Action-66
:parameters ()
:precondition
(and
(Flag136-)
(Flag136prime-)
)
:effect
(and
(Flag156-)
(Flag156prime-)
)

)
(:action Flag156Action-67
:parameters ()
:precondition
(and
(Flag137-)
(Flag137prime-)
)
:effect
(and
(Flag156-)
(Flag156prime-)
)

)
(:action Flag156Action-68
:parameters ()
:precondition
(and
(Flag65-)
(Flag65prime-)
)
:effect
(and
(Flag156-)
(Flag156prime-)
)

)
(:action Flag156Action-69
:parameters ()
:precondition
(and
(Flag117-)
(Flag117prime-)
)
:effect
(and
(Flag156-)
(Flag156prime-)
)

)
(:action Flag156Action-70
:parameters ()
:precondition
(and
(Flag107-)
(Flag107prime-)
)
:effect
(and
(Flag156-)
(Flag156prime-)
)

)
(:action Flag156Action-71
:parameters ()
:precondition
(and
(Flag150-)
(Flag150prime-)
)
:effect
(and
(Flag156-)
(Flag156prime-)
)

)
(:action Flag156Action-72
:parameters ()
:precondition
(and
(Flag151-)
(Flag151prime-)
)
:effect
(and
(Flag156-)
(Flag156prime-)
)

)
(:action Flag156Action-73
:parameters ()
:precondition
(and
(Flag152-)
(Flag152prime-)
)
:effect
(and
(Flag156-)
(Flag156prime-)
)

)
(:action Flag156Action-74
:parameters ()
:precondition
(and
(Flag90-)
(Flag90prime-)
)
:effect
(and
(Flag156-)
(Flag156prime-)
)

)
(:action Flag156Action-75
:parameters ()
:precondition
(and
(Flag140-)
(Flag140prime-)
)
:effect
(and
(Flag156-)
(Flag156prime-)
)

)
(:action Flag156Action-76
:parameters ()
:precondition
(and
(Flag108-)
(Flag108prime-)
)
:effect
(and
(Flag156-)
(Flag156prime-)
)

)
(:action Flag156Action-77
:parameters ()
:precondition
(and
(Flag153-)
(Flag153prime-)
)
:effect
(and
(Flag156-)
(Flag156prime-)
)

)
(:action Flag156Action-78
:parameters ()
:precondition
(and
(Flag131-)
(Flag131prime-)
)
:effect
(and
(Flag156-)
(Flag156prime-)
)

)
(:action Flag156Action-79
:parameters ()
:precondition
(and
(Flag51-)
(Flag51prime-)
)
:effect
(and
(Flag156-)
(Flag156prime-)
)

)
(:action Flag156Action-80
:parameters ()
:precondition
(and
(Flag154-)
(Flag154prime-)
)
:effect
(and
(Flag156-)
(Flag156prime-)
)

)
(:action Flag156Action-81
:parameters ()
:precondition
(and
(Flag43-)
(Flag43prime-)
)
:effect
(and
(Flag156-)
(Flag156prime-)
)

)
(:action Flag156Action-82
:parameters ()
:precondition
(and
(Flag132-)
(Flag132prime-)
)
:effect
(and
(Flag156-)
(Flag156prime-)
)

)
(:action Flag156Action-83
:parameters ()
:precondition
(and
(Flag99-)
(Flag99prime-)
)
:effect
(and
(Flag156-)
(Flag156prime-)
)

)
(:action Flag156Action-84
:parameters ()
:precondition
(and
(Flag124-)
(Flag124prime-)
)
:effect
(and
(Flag156-)
(Flag156prime-)
)

)
(:action Flag156Action-85
:parameters ()
:precondition
(and
(Flag75-)
(Flag75prime-)
)
:effect
(and
(Flag156-)
(Flag156prime-)
)

)
(:action Flag156Action-86
:parameters ()
:precondition
(and
(Flag61-)
(Flag61prime-)
)
:effect
(and
(Flag156-)
(Flag156prime-)
)

)
(:action Flag156Action-87
:parameters ()
:precondition
(and
(Flag155-)
(Flag155prime-)
)
:effect
(and
(Flag156-)
(Flag156prime-)
)

)
(:action Flag156Action-88
:parameters ()
:precondition
(and
(Flag88-)
(Flag88prime-)
)
:effect
(and
(Flag156-)
(Flag156prime-)
)

)
(:action Prim156Action
:parameters ()
:precondition
(and
(not (Flag142-))
(Flag142prime-)
(not (Flag42-))
(Flag42prime-)
(not (Flag113-))
(Flag113prime-)
(not (Flag44-))
(Flag44prime-)
(not (Flag46-))
(Flag46prime-)
(not (Flag79-))
(Flag79prime-)
(not (Flag95-))
(Flag95prime-)
(not (Flag80-))
(Flag80prime-)
(not (Flag116-))
(Flag116prime-)
(not (Flag122-))
(Flag122prime-)
(not (Flag59-))
(Flag59prime-)
(not (Flag143-))
(Flag143prime-)
(not (Flag144-))
(Flag144prime-)
(not (Flag55-))
(Flag55prime-)
(not (Flag81-))
(Flag81prime-)
(not (Flag86-))
(Flag86prime-)
(not (Flag103-))
(Flag103prime-)
(not (Flag112-))
(Flag112prime-)
(not (Flag63-))
(Flag63prime-)
(not (Flag120-))
(Flag120prime-)
(not (Flag145-))
(Flag145prime-)
(not (Flag121-))
(Flag121prime-)
(not (Flag56-))
(Flag56prime-)
(not (Flag45-))
(Flag45prime-)
(not (Flag93-))
(Flag93prime-)
(not (Flag128-))
(Flag128prime-)
(not (Flag146-))
(Flag146prime-)
(not (Flag76-))
(Flag76prime-)
(not (Flag129-))
(Flag129prime-)
(not (Flag130-))
(Flag130prime-)
(not (Flag78-))
(Flag78prime-)
(not (Flag115-))
(Flag115prime-)
(not (Flag49-))
(Flag49prime-)
(not (Flag147-))
(Flag147prime-)
(not (Flag98-))
(Flag98prime-)
(not (Flag58-))
(Flag58prime-)
(not (Flag60-))
(Flag60prime-)
(not (Flag135-))
(Flag135prime-)
(not (Flag96-))
(Flag96prime-)
(not (Flag64-))
(Flag64prime-)
(not (Flag104-))
(Flag104prime-)
(not (Flag105-))
(Flag105prime-)
(not (Flag68-))
(Flag68prime-)
(not (Flag89-))
(Flag89prime-)
(not (Flag139-))
(Flag139prime-)
(not (Flag123-))
(Flag123prime-)
(not (Flag70-))
(Flag70prime-)
(not (Flag71-))
(Flag71prime-)
(not (Flag38-))
(Flag38prime-)
(not (Flag110-))
(Flag110prime-)
(not (Flag148-))
(Flag148prime-)
(not (Flag126-))
(Flag126prime-)
(not (Flag149-))
(Flag149prime-)
(not (Flag138-))
(Flag138prime-)
(not (Flag114-))
(Flag114prime-)
(not (Flag40-))
(Flag40prime-)
(not (Flag77-))
(Flag77prime-)
(not (Flag92-))
(Flag92prime-)
(not (Flag106-))
(Flag106prime-)
(not (Flag100-))
(Flag100prime-)
(not (Flag52-))
(Flag52prime-)
(not (Flag53-))
(Flag53prime-)
(not (Flag84-))
(Flag84prime-)
(not (Flag57-))
(Flag57prime-)
(not (Flag119-))
(Flag119prime-)
(not (Flag67-))
(Flag67prime-)
(not (Flag136-))
(Flag136prime-)
(not (Flag137-))
(Flag137prime-)
(not (Flag65-))
(Flag65prime-)
(not (Flag117-))
(Flag117prime-)
(not (Flag107-))
(Flag107prime-)
(not (Flag150-))
(Flag150prime-)
(not (Flag151-))
(Flag151prime-)
(not (Flag152-))
(Flag152prime-)
(not (Flag90-))
(Flag90prime-)
(not (Flag140-))
(Flag140prime-)
(not (Flag108-))
(Flag108prime-)
(not (Flag153-))
(Flag153prime-)
(not (Flag131-))
(Flag131prime-)
(not (Flag51-))
(Flag51prime-)
(not (Flag154-))
(Flag154prime-)
(not (Flag43-))
(Flag43prime-)
(not (Flag132-))
(Flag132prime-)
(not (Flag99-))
(Flag99prime-)
(not (Flag124-))
(Flag124prime-)
(not (Flag75-))
(Flag75prime-)
(not (Flag61-))
(Flag61prime-)
(not (Flag155-))
(Flag155prime-)
(not (Flag88-))
(Flag88prime-)
)
:effect
(and
(Flag156prime-)
(not (Flag156-))
)

)
(:action Flag169Action-0
:parameters ()
:precondition
(and
(Flag42-)
(Flag42prime-)
)
:effect
(and
(Flag169-)
(Flag169prime-)
)

)
(:action Flag169Action-1
:parameters ()
:precondition
(and
(Flag113-)
(Flag113prime-)
)
:effect
(and
(Flag169-)
(Flag169prime-)
)

)
(:action Flag169Action-2
:parameters ()
:precondition
(and
(Flag44-)
(Flag44prime-)
)
:effect
(and
(Flag169-)
(Flag169prime-)
)

)
(:action Flag169Action-3
:parameters ()
:precondition
(and
(Flag46-)
(Flag46prime-)
)
:effect
(and
(Flag169-)
(Flag169prime-)
)

)
(:action Flag169Action-4
:parameters ()
:precondition
(and
(Flag79-)
(Flag79prime-)
)
:effect
(and
(Flag169-)
(Flag169prime-)
)

)
(:action Flag169Action-5
:parameters ()
:precondition
(and
(Flag95-)
(Flag95prime-)
)
:effect
(and
(Flag169-)
(Flag169prime-)
)

)
(:action Flag169Action-6
:parameters ()
:precondition
(and
(Flag80-)
(Flag80prime-)
)
:effect
(and
(Flag169-)
(Flag169prime-)
)

)
(:action Flag169Action-7
:parameters ()
:precondition
(and
(Flag116-)
(Flag116prime-)
)
:effect
(and
(Flag169-)
(Flag169prime-)
)

)
(:action Flag169Action-8
:parameters ()
:precondition
(and
(Flag122-)
(Flag122prime-)
)
:effect
(and
(Flag169-)
(Flag169prime-)
)

)
(:action Flag169Action-9
:parameters ()
:precondition
(and
(Flag59-)
(Flag59prime-)
)
:effect
(and
(Flag169-)
(Flag169prime-)
)

)
(:action Flag169Action-10
:parameters ()
:precondition
(and
(Flag143-)
(Flag143prime-)
)
:effect
(and
(Flag169-)
(Flag169prime-)
)

)
(:action Flag169Action-11
:parameters ()
:precondition
(and
(Flag144-)
(Flag144prime-)
)
:effect
(and
(Flag169-)
(Flag169prime-)
)

)
(:action Flag169Action-12
:parameters ()
:precondition
(and
(Flag55-)
(Flag55prime-)
)
:effect
(and
(Flag169-)
(Flag169prime-)
)

)
(:action Flag169Action-13
:parameters ()
:precondition
(and
(Flag81-)
(Flag81prime-)
)
:effect
(and
(Flag169-)
(Flag169prime-)
)

)
(:action Flag169Action-14
:parameters ()
:precondition
(and
(Flag86-)
(Flag86prime-)
)
:effect
(and
(Flag169-)
(Flag169prime-)
)

)
(:action Flag169Action-15
:parameters ()
:precondition
(and
(Flag103-)
(Flag103prime-)
)
:effect
(and
(Flag169-)
(Flag169prime-)
)

)
(:action Flag169Action-16
:parameters ()
:precondition
(and
(Flag112-)
(Flag112prime-)
)
:effect
(and
(Flag169-)
(Flag169prime-)
)

)
(:action Flag169Action-17
:parameters ()
:precondition
(and
(Flag63-)
(Flag63prime-)
)
:effect
(and
(Flag169-)
(Flag169prime-)
)

)
(:action Flag169Action-18
:parameters ()
:precondition
(and
(Flag120-)
(Flag120prime-)
)
:effect
(and
(Flag169-)
(Flag169prime-)
)

)
(:action Flag169Action-19
:parameters ()
:precondition
(and
(Flag121-)
(Flag121prime-)
)
:effect
(and
(Flag169-)
(Flag169prime-)
)

)
(:action Flag169Action-20
:parameters ()
:precondition
(and
(Flag56-)
(Flag56prime-)
)
:effect
(and
(Flag169-)
(Flag169prime-)
)

)
(:action Flag169Action-21
:parameters ()
:precondition
(and
(Flag157-)
(Flag157prime-)
)
:effect
(and
(Flag169-)
(Flag169prime-)
)

)
(:action Flag169Action-22
:parameters ()
:precondition
(and
(Flag158-)
(Flag158prime-)
)
:effect
(and
(Flag169-)
(Flag169prime-)
)

)
(:action Flag169Action-23
:parameters ()
:precondition
(and
(Flag45-)
(Flag45prime-)
)
:effect
(and
(Flag169-)
(Flag169prime-)
)

)
(:action Flag169Action-24
:parameters ()
:precondition
(and
(Flag159-)
(Flag159prime-)
)
:effect
(and
(Flag169-)
(Flag169prime-)
)

)
(:action Flag169Action-25
:parameters ()
:precondition
(and
(Flag93-)
(Flag93prime-)
)
:effect
(and
(Flag169-)
(Flag169prime-)
)

)
(:action Flag169Action-26
:parameters ()
:precondition
(and
(Flag128-)
(Flag128prime-)
)
:effect
(and
(Flag169-)
(Flag169prime-)
)

)
(:action Flag169Action-27
:parameters ()
:precondition
(and
(Flag160-)
(Flag160prime-)
)
:effect
(and
(Flag169-)
(Flag169prime-)
)

)
(:action Flag169Action-28
:parameters ()
:precondition
(and
(Flag146-)
(Flag146prime-)
)
:effect
(and
(Flag169-)
(Flag169prime-)
)

)
(:action Flag169Action-29
:parameters ()
:precondition
(and
(Flag76-)
(Flag76prime-)
)
:effect
(and
(Flag169-)
(Flag169prime-)
)

)
(:action Flag169Action-30
:parameters ()
:precondition
(and
(Flag129-)
(Flag129prime-)
)
:effect
(and
(Flag169-)
(Flag169prime-)
)

)
(:action Flag169Action-31
:parameters ()
:precondition
(and
(Flag130-)
(Flag130prime-)
)
:effect
(and
(Flag169-)
(Flag169prime-)
)

)
(:action Flag169Action-32
:parameters ()
:precondition
(and
(Flag78-)
(Flag78prime-)
)
:effect
(and
(Flag169-)
(Flag169prime-)
)

)
(:action Flag169Action-33
:parameters ()
:precondition
(and
(Flag161-)
(Flag161prime-)
)
:effect
(and
(Flag169-)
(Flag169prime-)
)

)
(:action Flag169Action-34
:parameters ()
:precondition
(and
(Flag115-)
(Flag115prime-)
)
:effect
(and
(Flag169-)
(Flag169prime-)
)

)
(:action Flag169Action-35
:parameters ()
:precondition
(and
(Flag147-)
(Flag147prime-)
)
:effect
(and
(Flag169-)
(Flag169prime-)
)

)
(:action Flag169Action-36
:parameters ()
:precondition
(and
(Flag98-)
(Flag98prime-)
)
:effect
(and
(Flag169-)
(Flag169prime-)
)

)
(:action Flag169Action-37
:parameters ()
:precondition
(and
(Flag58-)
(Flag58prime-)
)
:effect
(and
(Flag169-)
(Flag169prime-)
)

)
(:action Flag169Action-38
:parameters ()
:precondition
(and
(Flag162-)
(Flag162prime-)
)
:effect
(and
(Flag169-)
(Flag169prime-)
)

)
(:action Flag169Action-39
:parameters ()
:precondition
(and
(Flag60-)
(Flag60prime-)
)
:effect
(and
(Flag169-)
(Flag169prime-)
)

)
(:action Flag169Action-40
:parameters ()
:precondition
(and
(Flag135-)
(Flag135prime-)
)
:effect
(and
(Flag169-)
(Flag169prime-)
)

)
(:action Flag169Action-41
:parameters ()
:precondition
(and
(Flag64-)
(Flag64prime-)
)
:effect
(and
(Flag169-)
(Flag169prime-)
)

)
(:action Flag169Action-42
:parameters ()
:precondition
(and
(Flag104-)
(Flag104prime-)
)
:effect
(and
(Flag169-)
(Flag169prime-)
)

)
(:action Flag169Action-43
:parameters ()
:precondition
(and
(Flag105-)
(Flag105prime-)
)
:effect
(and
(Flag169-)
(Flag169prime-)
)

)
(:action Flag169Action-44
:parameters ()
:precondition
(and
(Flag68-)
(Flag68prime-)
)
:effect
(and
(Flag169-)
(Flag169prime-)
)

)
(:action Flag169Action-45
:parameters ()
:precondition
(and
(Flag89-)
(Flag89prime-)
)
:effect
(and
(Flag169-)
(Flag169prime-)
)

)
(:action Flag169Action-46
:parameters ()
:precondition
(and
(Flag139-)
(Flag139prime-)
)
:effect
(and
(Flag169-)
(Flag169prime-)
)

)
(:action Flag169Action-47
:parameters ()
:precondition
(and
(Flag163-)
(Flag163prime-)
)
:effect
(and
(Flag169-)
(Flag169prime-)
)

)
(:action Flag169Action-48
:parameters ()
:precondition
(and
(Flag70-)
(Flag70prime-)
)
:effect
(and
(Flag169-)
(Flag169prime-)
)

)
(:action Flag169Action-49
:parameters ()
:precondition
(and
(Flag71-)
(Flag71prime-)
)
:effect
(and
(Flag169-)
(Flag169prime-)
)

)
(:action Flag169Action-50
:parameters ()
:precondition
(and
(Flag38-)
(Flag38prime-)
)
:effect
(and
(Flag169-)
(Flag169prime-)
)

)
(:action Flag169Action-51
:parameters ()
:precondition
(and
(Flag110-)
(Flag110prime-)
)
:effect
(and
(Flag169-)
(Flag169prime-)
)

)
(:action Flag169Action-52
:parameters ()
:precondition
(and
(Flag148-)
(Flag148prime-)
)
:effect
(and
(Flag169-)
(Flag169prime-)
)

)
(:action Flag169Action-53
:parameters ()
:precondition
(and
(Flag126-)
(Flag126prime-)
)
:effect
(and
(Flag169-)
(Flag169prime-)
)

)
(:action Flag169Action-54
:parameters ()
:precondition
(and
(Flag149-)
(Flag149prime-)
)
:effect
(and
(Flag169-)
(Flag169prime-)
)

)
(:action Flag169Action-55
:parameters ()
:precondition
(and
(Flag138-)
(Flag138prime-)
)
:effect
(and
(Flag169-)
(Flag169prime-)
)

)
(:action Flag169Action-56
:parameters ()
:precondition
(and
(Flag114-)
(Flag114prime-)
)
:effect
(and
(Flag169-)
(Flag169prime-)
)

)
(:action Flag169Action-57
:parameters ()
:precondition
(and
(Flag40-)
(Flag40prime-)
)
:effect
(and
(Flag169-)
(Flag169prime-)
)

)
(:action Flag169Action-58
:parameters ()
:precondition
(and
(Flag77-)
(Flag77prime-)
)
:effect
(and
(Flag169-)
(Flag169prime-)
)

)
(:action Flag169Action-59
:parameters ()
:precondition
(and
(Flag92-)
(Flag92prime-)
)
:effect
(and
(Flag169-)
(Flag169prime-)
)

)
(:action Flag169Action-60
:parameters ()
:precondition
(and
(Flag106-)
(Flag106prime-)
)
:effect
(and
(Flag169-)
(Flag169prime-)
)

)
(:action Flag169Action-61
:parameters ()
:precondition
(and
(Flag100-)
(Flag100prime-)
)
:effect
(and
(Flag169-)
(Flag169prime-)
)

)
(:action Flag169Action-62
:parameters ()
:precondition
(and
(Flag52-)
(Flag52prime-)
)
:effect
(and
(Flag169-)
(Flag169prime-)
)

)
(:action Flag169Action-63
:parameters ()
:precondition
(and
(Flag49-)
(Flag49prime-)
)
:effect
(and
(Flag169-)
(Flag169prime-)
)

)
(:action Flag169Action-64
:parameters ()
:precondition
(and
(Flag164-)
(Flag164prime-)
)
:effect
(and
(Flag169-)
(Flag169prime-)
)

)
(:action Flag169Action-65
:parameters ()
:precondition
(and
(Flag84-)
(Flag84prime-)
)
:effect
(and
(Flag169-)
(Flag169prime-)
)

)
(:action Flag169Action-66
:parameters ()
:precondition
(and
(Flag165-)
(Flag165prime-)
)
:effect
(and
(Flag169-)
(Flag169prime-)
)

)
(:action Flag169Action-67
:parameters ()
:precondition
(and
(Flag67-)
(Flag67prime-)
)
:effect
(and
(Flag169-)
(Flag169prime-)
)

)
(:action Flag169Action-68
:parameters ()
:precondition
(and
(Flag136-)
(Flag136prime-)
)
:effect
(and
(Flag169-)
(Flag169prime-)
)

)
(:action Flag169Action-69
:parameters ()
:precondition
(and
(Flag137-)
(Flag137prime-)
)
:effect
(and
(Flag169-)
(Flag169prime-)
)

)
(:action Flag169Action-70
:parameters ()
:precondition
(and
(Flag65-)
(Flag65prime-)
)
:effect
(and
(Flag169-)
(Flag169prime-)
)

)
(:action Flag169Action-71
:parameters ()
:precondition
(and
(Flag117-)
(Flag117prime-)
)
:effect
(and
(Flag169-)
(Flag169prime-)
)

)
(:action Flag169Action-72
:parameters ()
:precondition
(and
(Flag107-)
(Flag107prime-)
)
:effect
(and
(Flag169-)
(Flag169prime-)
)

)
(:action Flag169Action-73
:parameters ()
:precondition
(and
(Flag150-)
(Flag150prime-)
)
:effect
(and
(Flag169-)
(Flag169prime-)
)

)
(:action Flag169Action-74
:parameters ()
:precondition
(and
(Flag151-)
(Flag151prime-)
)
:effect
(and
(Flag169-)
(Flag169prime-)
)

)
(:action Flag169Action-75
:parameters ()
:precondition
(and
(Flag152-)
(Flag152prime-)
)
:effect
(and
(Flag169-)
(Flag169prime-)
)

)
(:action Flag169Action-76
:parameters ()
:precondition
(and
(Flag90-)
(Flag90prime-)
)
:effect
(and
(Flag169-)
(Flag169prime-)
)

)
(:action Flag169Action-77
:parameters ()
:precondition
(and
(Flag140-)
(Flag140prime-)
)
:effect
(and
(Flag169-)
(Flag169prime-)
)

)
(:action Flag169Action-78
:parameters ()
:precondition
(and
(Flag166-)
(Flag166prime-)
)
:effect
(and
(Flag169-)
(Flag169prime-)
)

)
(:action Flag169Action-79
:parameters ()
:precondition
(and
(Flag108-)
(Flag108prime-)
)
:effect
(and
(Flag169-)
(Flag169prime-)
)

)
(:action Flag169Action-80
:parameters ()
:precondition
(and
(Flag153-)
(Flag153prime-)
)
:effect
(and
(Flag169-)
(Flag169prime-)
)

)
(:action Flag169Action-81
:parameters ()
:precondition
(and
(Flag131-)
(Flag131prime-)
)
:effect
(and
(Flag169-)
(Flag169prime-)
)

)
(:action Flag169Action-82
:parameters ()
:precondition
(and
(Flag51-)
(Flag51prime-)
)
:effect
(and
(Flag169-)
(Flag169prime-)
)

)
(:action Flag169Action-83
:parameters ()
:precondition
(and
(Flag43-)
(Flag43prime-)
)
:effect
(and
(Flag169-)
(Flag169prime-)
)

)
(:action Flag169Action-84
:parameters ()
:precondition
(and
(Flag132-)
(Flag132prime-)
)
:effect
(and
(Flag169-)
(Flag169prime-)
)

)
(:action Flag169Action-85
:parameters ()
:precondition
(and
(Flag99-)
(Flag99prime-)
)
:effect
(and
(Flag169-)
(Flag169prime-)
)

)
(:action Flag169Action-86
:parameters ()
:precondition
(and
(Flag167-)
(Flag167prime-)
)
:effect
(and
(Flag169-)
(Flag169prime-)
)

)
(:action Flag169Action-87
:parameters ()
:precondition
(and
(Flag124-)
(Flag124prime-)
)
:effect
(and
(Flag169-)
(Flag169prime-)
)

)
(:action Flag169Action-88
:parameters ()
:precondition
(and
(Flag61-)
(Flag61prime-)
)
:effect
(and
(Flag169-)
(Flag169prime-)
)

)
(:action Flag169Action-89
:parameters ()
:precondition
(and
(Flag168-)
(Flag168prime-)
)
:effect
(and
(Flag169-)
(Flag169prime-)
)

)
(:action Flag169Action-90
:parameters ()
:precondition
(and
(Flag155-)
(Flag155prime-)
)
:effect
(and
(Flag169-)
(Flag169prime-)
)

)
(:action Flag169Action-91
:parameters ()
:precondition
(and
(Flag119-)
(Flag119prime-)
)
:effect
(and
(Flag169-)
(Flag169prime-)
)

)
(:action Flag169Action-92
:parameters ()
:precondition
(and
(Flag88-)
(Flag88prime-)
)
:effect
(and
(Flag169-)
(Flag169prime-)
)

)
(:action Prim169Action
:parameters ()
:precondition
(and
(not (Flag42-))
(Flag42prime-)
(not (Flag113-))
(Flag113prime-)
(not (Flag44-))
(Flag44prime-)
(not (Flag46-))
(Flag46prime-)
(not (Flag79-))
(Flag79prime-)
(not (Flag95-))
(Flag95prime-)
(not (Flag80-))
(Flag80prime-)
(not (Flag116-))
(Flag116prime-)
(not (Flag122-))
(Flag122prime-)
(not (Flag59-))
(Flag59prime-)
(not (Flag143-))
(Flag143prime-)
(not (Flag144-))
(Flag144prime-)
(not (Flag55-))
(Flag55prime-)
(not (Flag81-))
(Flag81prime-)
(not (Flag86-))
(Flag86prime-)
(not (Flag103-))
(Flag103prime-)
(not (Flag112-))
(Flag112prime-)
(not (Flag63-))
(Flag63prime-)
(not (Flag120-))
(Flag120prime-)
(not (Flag121-))
(Flag121prime-)
(not (Flag56-))
(Flag56prime-)
(not (Flag157-))
(Flag157prime-)
(not (Flag158-))
(Flag158prime-)
(not (Flag45-))
(Flag45prime-)
(not (Flag159-))
(Flag159prime-)
(not (Flag93-))
(Flag93prime-)
(not (Flag128-))
(Flag128prime-)
(not (Flag160-))
(Flag160prime-)
(not (Flag146-))
(Flag146prime-)
(not (Flag76-))
(Flag76prime-)
(not (Flag129-))
(Flag129prime-)
(not (Flag130-))
(Flag130prime-)
(not (Flag78-))
(Flag78prime-)
(not (Flag161-))
(Flag161prime-)
(not (Flag115-))
(Flag115prime-)
(not (Flag147-))
(Flag147prime-)
(not (Flag98-))
(Flag98prime-)
(not (Flag58-))
(Flag58prime-)
(not (Flag162-))
(Flag162prime-)
(not (Flag60-))
(Flag60prime-)
(not (Flag135-))
(Flag135prime-)
(not (Flag64-))
(Flag64prime-)
(not (Flag104-))
(Flag104prime-)
(not (Flag105-))
(Flag105prime-)
(not (Flag68-))
(Flag68prime-)
(not (Flag89-))
(Flag89prime-)
(not (Flag139-))
(Flag139prime-)
(not (Flag163-))
(Flag163prime-)
(not (Flag70-))
(Flag70prime-)
(not (Flag71-))
(Flag71prime-)
(not (Flag38-))
(Flag38prime-)
(not (Flag110-))
(Flag110prime-)
(not (Flag148-))
(Flag148prime-)
(not (Flag126-))
(Flag126prime-)
(not (Flag149-))
(Flag149prime-)
(not (Flag138-))
(Flag138prime-)
(not (Flag114-))
(Flag114prime-)
(not (Flag40-))
(Flag40prime-)
(not (Flag77-))
(Flag77prime-)
(not (Flag92-))
(Flag92prime-)
(not (Flag106-))
(Flag106prime-)
(not (Flag100-))
(Flag100prime-)
(not (Flag52-))
(Flag52prime-)
(not (Flag49-))
(Flag49prime-)
(not (Flag164-))
(Flag164prime-)
(not (Flag84-))
(Flag84prime-)
(not (Flag165-))
(Flag165prime-)
(not (Flag67-))
(Flag67prime-)
(not (Flag136-))
(Flag136prime-)
(not (Flag137-))
(Flag137prime-)
(not (Flag65-))
(Flag65prime-)
(not (Flag117-))
(Flag117prime-)
(not (Flag107-))
(Flag107prime-)
(not (Flag150-))
(Flag150prime-)
(not (Flag151-))
(Flag151prime-)
(not (Flag152-))
(Flag152prime-)
(not (Flag90-))
(Flag90prime-)
(not (Flag140-))
(Flag140prime-)
(not (Flag166-))
(Flag166prime-)
(not (Flag108-))
(Flag108prime-)
(not (Flag153-))
(Flag153prime-)
(not (Flag131-))
(Flag131prime-)
(not (Flag51-))
(Flag51prime-)
(not (Flag43-))
(Flag43prime-)
(not (Flag132-))
(Flag132prime-)
(not (Flag99-))
(Flag99prime-)
(not (Flag167-))
(Flag167prime-)
(not (Flag124-))
(Flag124prime-)
(not (Flag61-))
(Flag61prime-)
(not (Flag168-))
(Flag168prime-)
(not (Flag155-))
(Flag155prime-)
(not (Flag119-))
(Flag119prime-)
(not (Flag88-))
(Flag88prime-)
)
:effect
(and
(Flag169prime-)
(not (Flag169-))
)

)
(:action Flag182Action-0
:parameters ()
:precondition
(and
(Flag113-)
(Flag113prime-)
)
:effect
(and
(Flag182-)
(Flag182prime-)
)

)
(:action Flag182Action-1
:parameters ()
:precondition
(and
(Flag44-)
(Flag44prime-)
)
:effect
(and
(Flag182-)
(Flag182prime-)
)

)
(:action Flag182Action-2
:parameters ()
:precondition
(and
(Flag148-)
(Flag148prime-)
)
:effect
(and
(Flag182-)
(Flag182prime-)
)

)
(:action Flag182Action-3
:parameters ()
:precondition
(and
(Flag95-)
(Flag95prime-)
)
:effect
(and
(Flag182-)
(Flag182prime-)
)

)
(:action Flag182Action-4
:parameters ()
:precondition
(and
(Flag80-)
(Flag80prime-)
)
:effect
(and
(Flag182-)
(Flag182prime-)
)

)
(:action Flag182Action-5
:parameters ()
:precondition
(and
(Flag116-)
(Flag116prime-)
)
:effect
(and
(Flag182-)
(Flag182prime-)
)

)
(:action Flag182Action-6
:parameters ()
:precondition
(and
(Flag122-)
(Flag122prime-)
)
:effect
(and
(Flag182-)
(Flag182prime-)
)

)
(:action Flag182Action-7
:parameters ()
:precondition
(and
(Flag59-)
(Flag59prime-)
)
:effect
(and
(Flag182-)
(Flag182prime-)
)

)
(:action Flag182Action-8
:parameters ()
:precondition
(and
(Flag143-)
(Flag143prime-)
)
:effect
(and
(Flag182-)
(Flag182prime-)
)

)
(:action Flag182Action-9
:parameters ()
:precondition
(and
(Flag170-)
(Flag170prime-)
)
:effect
(and
(Flag182-)
(Flag182prime-)
)

)
(:action Flag182Action-10
:parameters ()
:precondition
(and
(Flag144-)
(Flag144prime-)
)
:effect
(and
(Flag182-)
(Flag182prime-)
)

)
(:action Flag182Action-11
:parameters ()
:precondition
(and
(Flag55-)
(Flag55prime-)
)
:effect
(and
(Flag182-)
(Flag182prime-)
)

)
(:action Flag182Action-12
:parameters ()
:precondition
(and
(Flag81-)
(Flag81prime-)
)
:effect
(and
(Flag182-)
(Flag182prime-)
)

)
(:action Flag182Action-13
:parameters ()
:precondition
(and
(Flag86-)
(Flag86prime-)
)
:effect
(and
(Flag182-)
(Flag182prime-)
)

)
(:action Flag182Action-14
:parameters ()
:precondition
(and
(Flag103-)
(Flag103prime-)
)
:effect
(and
(Flag182-)
(Flag182prime-)
)

)
(:action Flag182Action-15
:parameters ()
:precondition
(and
(Flag112-)
(Flag112prime-)
)
:effect
(and
(Flag182-)
(Flag182prime-)
)

)
(:action Flag182Action-16
:parameters ()
:precondition
(and
(Flag63-)
(Flag63prime-)
)
:effect
(and
(Flag182-)
(Flag182prime-)
)

)
(:action Flag182Action-17
:parameters ()
:precondition
(and
(Flag171-)
(Flag171prime-)
)
:effect
(and
(Flag182-)
(Flag182prime-)
)

)
(:action Flag182Action-18
:parameters ()
:precondition
(and
(Flag120-)
(Flag120prime-)
)
:effect
(and
(Flag182-)
(Flag182prime-)
)

)
(:action Flag182Action-19
:parameters ()
:precondition
(and
(Flag172-)
(Flag172prime-)
)
:effect
(and
(Flag182-)
(Flag182prime-)
)

)
(:action Flag182Action-20
:parameters ()
:precondition
(and
(Flag121-)
(Flag121prime-)
)
:effect
(and
(Flag182-)
(Flag182prime-)
)

)
(:action Flag182Action-21
:parameters ()
:precondition
(and
(Flag56-)
(Flag56prime-)
)
:effect
(and
(Flag182-)
(Flag182prime-)
)

)
(:action Flag182Action-22
:parameters ()
:precondition
(and
(Flag157-)
(Flag157prime-)
)
:effect
(and
(Flag182-)
(Flag182prime-)
)

)
(:action Flag182Action-23
:parameters ()
:precondition
(and
(Flag158-)
(Flag158prime-)
)
:effect
(and
(Flag182-)
(Flag182prime-)
)

)
(:action Flag182Action-24
:parameters ()
:precondition
(and
(Flag173-)
(Flag173prime-)
)
:effect
(and
(Flag182-)
(Flag182prime-)
)

)
(:action Flag182Action-25
:parameters ()
:precondition
(and
(Flag45-)
(Flag45prime-)
)
:effect
(and
(Flag182-)
(Flag182prime-)
)

)
(:action Flag182Action-26
:parameters ()
:precondition
(and
(Flag159-)
(Flag159prime-)
)
:effect
(and
(Flag182-)
(Flag182prime-)
)

)
(:action Flag182Action-27
:parameters ()
:precondition
(and
(Flag93-)
(Flag93prime-)
)
:effect
(and
(Flag182-)
(Flag182prime-)
)

)
(:action Flag182Action-28
:parameters ()
:precondition
(and
(Flag128-)
(Flag128prime-)
)
:effect
(and
(Flag182-)
(Flag182prime-)
)

)
(:action Flag182Action-29
:parameters ()
:precondition
(and
(Flag160-)
(Flag160prime-)
)
:effect
(and
(Flag182-)
(Flag182prime-)
)

)
(:action Flag182Action-30
:parameters ()
:precondition
(and
(Flag146-)
(Flag146prime-)
)
:effect
(and
(Flag182-)
(Flag182prime-)
)

)
(:action Flag182Action-31
:parameters ()
:precondition
(and
(Flag174-)
(Flag174prime-)
)
:effect
(and
(Flag182-)
(Flag182prime-)
)

)
(:action Flag182Action-32
:parameters ()
:precondition
(and
(Flag129-)
(Flag129prime-)
)
:effect
(and
(Flag182-)
(Flag182prime-)
)

)
(:action Flag182Action-33
:parameters ()
:precondition
(and
(Flag130-)
(Flag130prime-)
)
:effect
(and
(Flag182-)
(Flag182prime-)
)

)
(:action Flag182Action-34
:parameters ()
:precondition
(and
(Flag78-)
(Flag78prime-)
)
:effect
(and
(Flag182-)
(Flag182prime-)
)

)
(:action Flag182Action-35
:parameters ()
:precondition
(and
(Flag175-)
(Flag175prime-)
)
:effect
(and
(Flag182-)
(Flag182prime-)
)

)
(:action Flag182Action-36
:parameters ()
:precondition
(and
(Flag161-)
(Flag161prime-)
)
:effect
(and
(Flag182-)
(Flag182prime-)
)

)
(:action Flag182Action-37
:parameters ()
:precondition
(and
(Flag115-)
(Flag115prime-)
)
:effect
(and
(Flag182-)
(Flag182prime-)
)

)
(:action Flag182Action-38
:parameters ()
:precondition
(and
(Flag140-)
(Flag140prime-)
)
:effect
(and
(Flag182-)
(Flag182prime-)
)

)
(:action Flag182Action-39
:parameters ()
:precondition
(and
(Flag147-)
(Flag147prime-)
)
:effect
(and
(Flag182-)
(Flag182prime-)
)

)
(:action Flag182Action-40
:parameters ()
:precondition
(and
(Flag98-)
(Flag98prime-)
)
:effect
(and
(Flag182-)
(Flag182prime-)
)

)
(:action Flag182Action-41
:parameters ()
:precondition
(and
(Flag58-)
(Flag58prime-)
)
:effect
(and
(Flag182-)
(Flag182prime-)
)

)
(:action Flag182Action-42
:parameters ()
:precondition
(and
(Flag162-)
(Flag162prime-)
)
:effect
(and
(Flag182-)
(Flag182prime-)
)

)
(:action Flag182Action-43
:parameters ()
:precondition
(and
(Flag60-)
(Flag60prime-)
)
:effect
(and
(Flag182-)
(Flag182prime-)
)

)
(:action Flag182Action-44
:parameters ()
:precondition
(and
(Flag135-)
(Flag135prime-)
)
:effect
(and
(Flag182-)
(Flag182prime-)
)

)
(:action Flag182Action-45
:parameters ()
:precondition
(and
(Flag77-)
(Flag77prime-)
)
:effect
(and
(Flag182-)
(Flag182prime-)
)

)
(:action Flag182Action-46
:parameters ()
:precondition
(and
(Flag64-)
(Flag64prime-)
)
:effect
(and
(Flag182-)
(Flag182prime-)
)

)
(:action Flag182Action-47
:parameters ()
:precondition
(and
(Flag104-)
(Flag104prime-)
)
:effect
(and
(Flag182-)
(Flag182prime-)
)

)
(:action Flag182Action-48
:parameters ()
:precondition
(and
(Flag105-)
(Flag105prime-)
)
:effect
(and
(Flag182-)
(Flag182prime-)
)

)
(:action Flag182Action-49
:parameters ()
:precondition
(and
(Flag68-)
(Flag68prime-)
)
:effect
(and
(Flag182-)
(Flag182prime-)
)

)
(:action Flag182Action-50
:parameters ()
:precondition
(and
(Flag89-)
(Flag89prime-)
)
:effect
(and
(Flag182-)
(Flag182prime-)
)

)
(:action Flag182Action-51
:parameters ()
:precondition
(and
(Flag139-)
(Flag139prime-)
)
:effect
(and
(Flag182-)
(Flag182prime-)
)

)
(:action Flag182Action-52
:parameters ()
:precondition
(and
(Flag176-)
(Flag176prime-)
)
:effect
(and
(Flag182-)
(Flag182prime-)
)

)
(:action Flag182Action-53
:parameters ()
:precondition
(and
(Flag70-)
(Flag70prime-)
)
:effect
(and
(Flag182-)
(Flag182prime-)
)

)
(:action Flag182Action-54
:parameters ()
:precondition
(and
(Flag71-)
(Flag71prime-)
)
:effect
(and
(Flag182-)
(Flag182prime-)
)

)
(:action Flag182Action-55
:parameters ()
:precondition
(and
(Flag38-)
(Flag38prime-)
)
:effect
(and
(Flag182-)
(Flag182prime-)
)

)
(:action Flag182Action-56
:parameters ()
:precondition
(and
(Flag110-)
(Flag110prime-)
)
:effect
(and
(Flag182-)
(Flag182prime-)
)

)
(:action Flag182Action-57
:parameters ()
:precondition
(and
(Flag79-)
(Flag79prime-)
)
:effect
(and
(Flag182-)
(Flag182prime-)
)

)
(:action Flag182Action-58
:parameters ()
:precondition
(and
(Flag126-)
(Flag126prime-)
)
:effect
(and
(Flag182-)
(Flag182prime-)
)

)
(:action Flag182Action-59
:parameters ()
:precondition
(and
(Flag149-)
(Flag149prime-)
)
:effect
(and
(Flag182-)
(Flag182prime-)
)

)
(:action Flag182Action-60
:parameters ()
:precondition
(and
(Flag138-)
(Flag138prime-)
)
:effect
(and
(Flag182-)
(Flag182prime-)
)

)
(:action Flag182Action-61
:parameters ()
:precondition
(and
(Flag177-)
(Flag177prime-)
)
:effect
(and
(Flag182-)
(Flag182prime-)
)

)
(:action Flag182Action-62
:parameters ()
:precondition
(and
(Flag114-)
(Flag114prime-)
)
:effect
(and
(Flag182-)
(Flag182prime-)
)

)
(:action Flag182Action-63
:parameters ()
:precondition
(and
(Flag40-)
(Flag40prime-)
)
:effect
(and
(Flag182-)
(Flag182prime-)
)

)
(:action Flag182Action-64
:parameters ()
:precondition
(and
(Flag178-)
(Flag178prime-)
)
:effect
(and
(Flag182-)
(Flag182prime-)
)

)
(:action Flag182Action-65
:parameters ()
:precondition
(and
(Flag92-)
(Flag92prime-)
)
:effect
(and
(Flag182-)
(Flag182prime-)
)

)
(:action Flag182Action-66
:parameters ()
:precondition
(and
(Flag100-)
(Flag100prime-)
)
:effect
(and
(Flag182-)
(Flag182prime-)
)

)
(:action Flag182Action-67
:parameters ()
:precondition
(and
(Flag52-)
(Flag52prime-)
)
:effect
(and
(Flag182-)
(Flag182prime-)
)

)
(:action Flag182Action-68
:parameters ()
:precondition
(and
(Flag49-)
(Flag49prime-)
)
:effect
(and
(Flag182-)
(Flag182prime-)
)

)
(:action Flag182Action-69
:parameters ()
:precondition
(and
(Flag84-)
(Flag84prime-)
)
:effect
(and
(Flag182-)
(Flag182prime-)
)

)
(:action Flag182Action-70
:parameters ()
:precondition
(and
(Flag67-)
(Flag67prime-)
)
:effect
(and
(Flag182-)
(Flag182prime-)
)

)
(:action Flag182Action-71
:parameters ()
:precondition
(and
(Flag136-)
(Flag136prime-)
)
:effect
(and
(Flag182-)
(Flag182prime-)
)

)
(:action Flag182Action-72
:parameters ()
:precondition
(and
(Flag137-)
(Flag137prime-)
)
:effect
(and
(Flag182-)
(Flag182prime-)
)

)
(:action Flag182Action-73
:parameters ()
:precondition
(and
(Flag65-)
(Flag65prime-)
)
:effect
(and
(Flag182-)
(Flag182prime-)
)

)
(:action Flag182Action-74
:parameters ()
:precondition
(and
(Flag117-)
(Flag117prime-)
)
:effect
(and
(Flag182-)
(Flag182prime-)
)

)
(:action Flag182Action-75
:parameters ()
:precondition
(and
(Flag107-)
(Flag107prime-)
)
:effect
(and
(Flag182-)
(Flag182prime-)
)

)
(:action Flag182Action-76
:parameters ()
:precondition
(and
(Flag150-)
(Flag150prime-)
)
:effect
(and
(Flag182-)
(Flag182prime-)
)

)
(:action Flag182Action-77
:parameters ()
:precondition
(and
(Flag151-)
(Flag151prime-)
)
:effect
(and
(Flag182-)
(Flag182prime-)
)

)
(:action Flag182Action-78
:parameters ()
:precondition
(and
(Flag152-)
(Flag152prime-)
)
:effect
(and
(Flag182-)
(Flag182prime-)
)

)
(:action Flag182Action-79
:parameters ()
:precondition
(and
(Flag90-)
(Flag90prime-)
)
:effect
(and
(Flag182-)
(Flag182prime-)
)

)
(:action Flag182Action-80
:parameters ()
:precondition
(and
(Flag179-)
(Flag179prime-)
)
:effect
(and
(Flag182-)
(Flag182prime-)
)

)
(:action Flag182Action-81
:parameters ()
:precondition
(and
(Flag166-)
(Flag166prime-)
)
:effect
(and
(Flag182-)
(Flag182prime-)
)

)
(:action Flag182Action-82
:parameters ()
:precondition
(and
(Flag108-)
(Flag108prime-)
)
:effect
(and
(Flag182-)
(Flag182prime-)
)

)
(:action Flag182Action-83
:parameters ()
:precondition
(and
(Flag153-)
(Flag153prime-)
)
:effect
(and
(Flag182-)
(Flag182prime-)
)

)
(:action Flag182Action-84
:parameters ()
:precondition
(and
(Flag131-)
(Flag131prime-)
)
:effect
(and
(Flag182-)
(Flag182prime-)
)

)
(:action Flag182Action-85
:parameters ()
:precondition
(and
(Flag51-)
(Flag51prime-)
)
:effect
(and
(Flag182-)
(Flag182prime-)
)

)
(:action Flag182Action-86
:parameters ()
:precondition
(and
(Flag43-)
(Flag43prime-)
)
:effect
(and
(Flag182-)
(Flag182prime-)
)

)
(:action Flag182Action-87
:parameters ()
:precondition
(and
(Flag99-)
(Flag99prime-)
)
:effect
(and
(Flag182-)
(Flag182prime-)
)

)
(:action Flag182Action-88
:parameters ()
:precondition
(and
(Flag167-)
(Flag167prime-)
)
:effect
(and
(Flag182-)
(Flag182prime-)
)

)
(:action Flag182Action-89
:parameters ()
:precondition
(and
(Flag124-)
(Flag124prime-)
)
:effect
(and
(Flag182-)
(Flag182prime-)
)

)
(:action Flag182Action-90
:parameters ()
:precondition
(and
(Flag61-)
(Flag61prime-)
)
:effect
(and
(Flag182-)
(Flag182prime-)
)

)
(:action Flag182Action-91
:parameters ()
:precondition
(and
(Flag168-)
(Flag168prime-)
)
:effect
(and
(Flag182-)
(Flag182prime-)
)

)
(:action Flag182Action-92
:parameters ()
:precondition
(and
(Flag155-)
(Flag155prime-)
)
:effect
(and
(Flag182-)
(Flag182prime-)
)

)
(:action Flag182Action-93
:parameters ()
:precondition
(and
(Flag180-)
(Flag180prime-)
)
:effect
(and
(Flag182-)
(Flag182prime-)
)

)
(:action Flag182Action-94
:parameters ()
:precondition
(and
(Flag88-)
(Flag88prime-)
)
:effect
(and
(Flag182-)
(Flag182prime-)
)

)
(:action Flag182Action-95
:parameters ()
:precondition
(and
(Flag181-)
(Flag181prime-)
)
:effect
(and
(Flag182-)
(Flag182prime-)
)

)
(:action Prim182Action
:parameters ()
:precondition
(and
(not (Flag113-))
(Flag113prime-)
(not (Flag44-))
(Flag44prime-)
(not (Flag148-))
(Flag148prime-)
(not (Flag95-))
(Flag95prime-)
(not (Flag80-))
(Flag80prime-)
(not (Flag116-))
(Flag116prime-)
(not (Flag122-))
(Flag122prime-)
(not (Flag59-))
(Flag59prime-)
(not (Flag143-))
(Flag143prime-)
(not (Flag170-))
(Flag170prime-)
(not (Flag144-))
(Flag144prime-)
(not (Flag55-))
(Flag55prime-)
(not (Flag81-))
(Flag81prime-)
(not (Flag86-))
(Flag86prime-)
(not (Flag103-))
(Flag103prime-)
(not (Flag112-))
(Flag112prime-)
(not (Flag63-))
(Flag63prime-)
(not (Flag171-))
(Flag171prime-)
(not (Flag120-))
(Flag120prime-)
(not (Flag172-))
(Flag172prime-)
(not (Flag121-))
(Flag121prime-)
(not (Flag56-))
(Flag56prime-)
(not (Flag157-))
(Flag157prime-)
(not (Flag158-))
(Flag158prime-)
(not (Flag173-))
(Flag173prime-)
(not (Flag45-))
(Flag45prime-)
(not (Flag159-))
(Flag159prime-)
(not (Flag93-))
(Flag93prime-)
(not (Flag128-))
(Flag128prime-)
(not (Flag160-))
(Flag160prime-)
(not (Flag146-))
(Flag146prime-)
(not (Flag174-))
(Flag174prime-)
(not (Flag129-))
(Flag129prime-)
(not (Flag130-))
(Flag130prime-)
(not (Flag78-))
(Flag78prime-)
(not (Flag175-))
(Flag175prime-)
(not (Flag161-))
(Flag161prime-)
(not (Flag115-))
(Flag115prime-)
(not (Flag140-))
(Flag140prime-)
(not (Flag147-))
(Flag147prime-)
(not (Flag98-))
(Flag98prime-)
(not (Flag58-))
(Flag58prime-)
(not (Flag162-))
(Flag162prime-)
(not (Flag60-))
(Flag60prime-)
(not (Flag135-))
(Flag135prime-)
(not (Flag77-))
(Flag77prime-)
(not (Flag64-))
(Flag64prime-)
(not (Flag104-))
(Flag104prime-)
(not (Flag105-))
(Flag105prime-)
(not (Flag68-))
(Flag68prime-)
(not (Flag89-))
(Flag89prime-)
(not (Flag139-))
(Flag139prime-)
(not (Flag176-))
(Flag176prime-)
(not (Flag70-))
(Flag70prime-)
(not (Flag71-))
(Flag71prime-)
(not (Flag38-))
(Flag38prime-)
(not (Flag110-))
(Flag110prime-)
(not (Flag79-))
(Flag79prime-)
(not (Flag126-))
(Flag126prime-)
(not (Flag149-))
(Flag149prime-)
(not (Flag138-))
(Flag138prime-)
(not (Flag177-))
(Flag177prime-)
(not (Flag114-))
(Flag114prime-)
(not (Flag40-))
(Flag40prime-)
(not (Flag178-))
(Flag178prime-)
(not (Flag92-))
(Flag92prime-)
(not (Flag100-))
(Flag100prime-)
(not (Flag52-))
(Flag52prime-)
(not (Flag49-))
(Flag49prime-)
(not (Flag84-))
(Flag84prime-)
(not (Flag67-))
(Flag67prime-)
(not (Flag136-))
(Flag136prime-)
(not (Flag137-))
(Flag137prime-)
(not (Flag65-))
(Flag65prime-)
(not (Flag117-))
(Flag117prime-)
(not (Flag107-))
(Flag107prime-)
(not (Flag150-))
(Flag150prime-)
(not (Flag151-))
(Flag151prime-)
(not (Flag152-))
(Flag152prime-)
(not (Flag90-))
(Flag90prime-)
(not (Flag179-))
(Flag179prime-)
(not (Flag166-))
(Flag166prime-)
(not (Flag108-))
(Flag108prime-)
(not (Flag153-))
(Flag153prime-)
(not (Flag131-))
(Flag131prime-)
(not (Flag51-))
(Flag51prime-)
(not (Flag43-))
(Flag43prime-)
(not (Flag99-))
(Flag99prime-)
(not (Flag167-))
(Flag167prime-)
(not (Flag124-))
(Flag124prime-)
(not (Flag61-))
(Flag61prime-)
(not (Flag168-))
(Flag168prime-)
(not (Flag155-))
(Flag155prime-)
(not (Flag180-))
(Flag180prime-)
(not (Flag88-))
(Flag88prime-)
(not (Flag181-))
(Flag181prime-)
)
:effect
(and
(Flag182prime-)
(not (Flag182-))
)

)
(:action Flag193Action-0
:parameters ()
:precondition
(and
(Flag78-)
(Flag78prime-)
)
:effect
(and
(Flag193-)
(Flag193prime-)
)

)
(:action Flag193Action-1
:parameters ()
:precondition
(and
(Flag44-)
(Flag44prime-)
)
:effect
(and
(Flag193-)
(Flag193prime-)
)

)
(:action Flag193Action-2
:parameters ()
:precondition
(and
(Flag148-)
(Flag148prime-)
)
:effect
(and
(Flag193-)
(Flag193prime-)
)

)
(:action Flag193Action-3
:parameters ()
:precondition
(and
(Flag95-)
(Flag95prime-)
)
:effect
(and
(Flag193-)
(Flag193prime-)
)

)
(:action Flag193Action-4
:parameters ()
:precondition
(and
(Flag116-)
(Flag116prime-)
)
:effect
(and
(Flag193-)
(Flag193prime-)
)

)
(:action Flag193Action-5
:parameters ()
:precondition
(and
(Flag122-)
(Flag122prime-)
)
:effect
(and
(Flag193-)
(Flag193prime-)
)

)
(:action Flag193Action-6
:parameters ()
:precondition
(and
(Flag59-)
(Flag59prime-)
)
:effect
(and
(Flag193-)
(Flag193prime-)
)

)
(:action Flag193Action-7
:parameters ()
:precondition
(and
(Flag143-)
(Flag143prime-)
)
:effect
(and
(Flag193-)
(Flag193prime-)
)

)
(:action Flag193Action-8
:parameters ()
:precondition
(and
(Flag170-)
(Flag170prime-)
)
:effect
(and
(Flag193-)
(Flag193prime-)
)

)
(:action Flag193Action-9
:parameters ()
:precondition
(and
(Flag144-)
(Flag144prime-)
)
:effect
(and
(Flag193-)
(Flag193prime-)
)

)
(:action Flag193Action-10
:parameters ()
:precondition
(and
(Flag183-)
(Flag183prime-)
)
:effect
(and
(Flag193-)
(Flag193prime-)
)

)
(:action Flag193Action-11
:parameters ()
:precondition
(and
(Flag55-)
(Flag55prime-)
)
:effect
(and
(Flag193-)
(Flag193prime-)
)

)
(:action Flag193Action-12
:parameters ()
:precondition
(and
(Flag81-)
(Flag81prime-)
)
:effect
(and
(Flag193-)
(Flag193prime-)
)

)
(:action Flag193Action-13
:parameters ()
:precondition
(and
(Flag184-)
(Flag184prime-)
)
:effect
(and
(Flag193-)
(Flag193prime-)
)

)
(:action Flag193Action-14
:parameters ()
:precondition
(and
(Flag86-)
(Flag86prime-)
)
:effect
(and
(Flag193-)
(Flag193prime-)
)

)
(:action Flag193Action-15
:parameters ()
:precondition
(and
(Flag103-)
(Flag103prime-)
)
:effect
(and
(Flag193-)
(Flag193prime-)
)

)
(:action Flag193Action-16
:parameters ()
:precondition
(and
(Flag112-)
(Flag112prime-)
)
:effect
(and
(Flag193-)
(Flag193prime-)
)

)
(:action Flag193Action-17
:parameters ()
:precondition
(and
(Flag63-)
(Flag63prime-)
)
:effect
(and
(Flag193-)
(Flag193prime-)
)

)
(:action Flag193Action-18
:parameters ()
:precondition
(and
(Flag171-)
(Flag171prime-)
)
:effect
(and
(Flag193-)
(Flag193prime-)
)

)
(:action Flag193Action-19
:parameters ()
:precondition
(and
(Flag120-)
(Flag120prime-)
)
:effect
(and
(Flag193-)
(Flag193prime-)
)

)
(:action Flag193Action-20
:parameters ()
:precondition
(and
(Flag172-)
(Flag172prime-)
)
:effect
(and
(Flag193-)
(Flag193prime-)
)

)
(:action Flag193Action-21
:parameters ()
:precondition
(and
(Flag121-)
(Flag121prime-)
)
:effect
(and
(Flag193-)
(Flag193prime-)
)

)
(:action Flag193Action-22
:parameters ()
:precondition
(and
(Flag56-)
(Flag56prime-)
)
:effect
(and
(Flag193-)
(Flag193prime-)
)

)
(:action Flag193Action-23
:parameters ()
:precondition
(and
(Flag157-)
(Flag157prime-)
)
:effect
(and
(Flag193-)
(Flag193prime-)
)

)
(:action Flag193Action-24
:parameters ()
:precondition
(and
(Flag158-)
(Flag158prime-)
)
:effect
(and
(Flag193-)
(Flag193prime-)
)

)
(:action Flag193Action-25
:parameters ()
:precondition
(and
(Flag173-)
(Flag173prime-)
)
:effect
(and
(Flag193-)
(Flag193prime-)
)

)
(:action Flag193Action-26
:parameters ()
:precondition
(and
(Flag45-)
(Flag45prime-)
)
:effect
(and
(Flag193-)
(Flag193prime-)
)

)
(:action Flag193Action-27
:parameters ()
:precondition
(and
(Flag159-)
(Flag159prime-)
)
:effect
(and
(Flag193-)
(Flag193prime-)
)

)
(:action Flag193Action-28
:parameters ()
:precondition
(and
(Flag93-)
(Flag93prime-)
)
:effect
(and
(Flag193-)
(Flag193prime-)
)

)
(:action Flag193Action-29
:parameters ()
:precondition
(and
(Flag128-)
(Flag128prime-)
)
:effect
(and
(Flag193-)
(Flag193prime-)
)

)
(:action Flag193Action-30
:parameters ()
:precondition
(and
(Flag160-)
(Flag160prime-)
)
:effect
(and
(Flag193-)
(Flag193prime-)
)

)
(:action Flag193Action-31
:parameters ()
:precondition
(and
(Flag146-)
(Flag146prime-)
)
:effect
(and
(Flag193-)
(Flag193prime-)
)

)
(:action Flag193Action-32
:parameters ()
:precondition
(and
(Flag174-)
(Flag174prime-)
)
:effect
(and
(Flag193-)
(Flag193prime-)
)

)
(:action Flag193Action-33
:parameters ()
:precondition
(and
(Flag129-)
(Flag129prime-)
)
:effect
(and
(Flag193-)
(Flag193prime-)
)

)
(:action Flag193Action-34
:parameters ()
:precondition
(and
(Flag130-)
(Flag130prime-)
)
:effect
(and
(Flag193-)
(Flag193prime-)
)

)
(:action Flag193Action-35
:parameters ()
:precondition
(and
(Flag185-)
(Flag185prime-)
)
:effect
(and
(Flag193-)
(Flag193prime-)
)

)
(:action Flag193Action-36
:parameters ()
:precondition
(and
(Flag175-)
(Flag175prime-)
)
:effect
(and
(Flag193-)
(Flag193prime-)
)

)
(:action Flag193Action-37
:parameters ()
:precondition
(and
(Flag161-)
(Flag161prime-)
)
:effect
(and
(Flag193-)
(Flag193prime-)
)

)
(:action Flag193Action-38
:parameters ()
:precondition
(and
(Flag115-)
(Flag115prime-)
)
:effect
(and
(Flag193-)
(Flag193prime-)
)

)
(:action Flag193Action-39
:parameters ()
:precondition
(and
(Flag140-)
(Flag140prime-)
)
:effect
(and
(Flag193-)
(Flag193prime-)
)

)
(:action Flag193Action-40
:parameters ()
:precondition
(and
(Flag147-)
(Flag147prime-)
)
:effect
(and
(Flag193-)
(Flag193prime-)
)

)
(:action Flag193Action-41
:parameters ()
:precondition
(and
(Flag98-)
(Flag98prime-)
)
:effect
(and
(Flag193-)
(Flag193prime-)
)

)
(:action Flag193Action-42
:parameters ()
:precondition
(and
(Flag58-)
(Flag58prime-)
)
:effect
(and
(Flag193-)
(Flag193prime-)
)

)
(:action Flag193Action-43
:parameters ()
:precondition
(and
(Flag162-)
(Flag162prime-)
)
:effect
(and
(Flag193-)
(Flag193prime-)
)

)
(:action Flag193Action-44
:parameters ()
:precondition
(and
(Flag60-)
(Flag60prime-)
)
:effect
(and
(Flag193-)
(Flag193prime-)
)

)
(:action Flag193Action-45
:parameters ()
:precondition
(and
(Flag135-)
(Flag135prime-)
)
:effect
(and
(Flag193-)
(Flag193prime-)
)

)
(:action Flag193Action-46
:parameters ()
:precondition
(and
(Flag64-)
(Flag64prime-)
)
:effect
(and
(Flag193-)
(Flag193prime-)
)

)
(:action Flag193Action-47
:parameters ()
:precondition
(and
(Flag104-)
(Flag104prime-)
)
:effect
(and
(Flag193-)
(Flag193prime-)
)

)
(:action Flag193Action-48
:parameters ()
:precondition
(and
(Flag186-)
(Flag186prime-)
)
:effect
(and
(Flag193-)
(Flag193prime-)
)

)
(:action Flag193Action-49
:parameters ()
:precondition
(and
(Flag105-)
(Flag105prime-)
)
:effect
(and
(Flag193-)
(Flag193prime-)
)

)
(:action Flag193Action-50
:parameters ()
:precondition
(and
(Flag68-)
(Flag68prime-)
)
:effect
(and
(Flag193-)
(Flag193prime-)
)

)
(:action Flag193Action-51
:parameters ()
:precondition
(and
(Flag89-)
(Flag89prime-)
)
:effect
(and
(Flag193-)
(Flag193prime-)
)

)
(:action Flag193Action-52
:parameters ()
:precondition
(and
(Flag139-)
(Flag139prime-)
)
:effect
(and
(Flag193-)
(Flag193prime-)
)

)
(:action Flag193Action-53
:parameters ()
:precondition
(and
(Flag187-)
(Flag187prime-)
)
:effect
(and
(Flag193-)
(Flag193prime-)
)

)
(:action Flag193Action-54
:parameters ()
:precondition
(and
(Flag38-)
(Flag38prime-)
)
:effect
(and
(Flag193-)
(Flag193prime-)
)

)
(:action Flag193Action-55
:parameters ()
:precondition
(and
(Flag110-)
(Flag110prime-)
)
:effect
(and
(Flag193-)
(Flag193prime-)
)

)
(:action Flag193Action-56
:parameters ()
:precondition
(and
(Flag79-)
(Flag79prime-)
)
:effect
(and
(Flag193-)
(Flag193prime-)
)

)
(:action Flag193Action-57
:parameters ()
:precondition
(and
(Flag126-)
(Flag126prime-)
)
:effect
(and
(Flag193-)
(Flag193prime-)
)

)
(:action Flag193Action-58
:parameters ()
:precondition
(and
(Flag149-)
(Flag149prime-)
)
:effect
(and
(Flag193-)
(Flag193prime-)
)

)
(:action Flag193Action-59
:parameters ()
:precondition
(and
(Flag138-)
(Flag138prime-)
)
:effect
(and
(Flag193-)
(Flag193prime-)
)

)
(:action Flag193Action-60
:parameters ()
:precondition
(and
(Flag177-)
(Flag177prime-)
)
:effect
(and
(Flag193-)
(Flag193prime-)
)

)
(:action Flag193Action-61
:parameters ()
:precondition
(and
(Flag114-)
(Flag114prime-)
)
:effect
(and
(Flag193-)
(Flag193prime-)
)

)
(:action Flag193Action-62
:parameters ()
:precondition
(and
(Flag40-)
(Flag40prime-)
)
:effect
(and
(Flag193-)
(Flag193prime-)
)

)
(:action Flag193Action-63
:parameters ()
:precondition
(and
(Flag77-)
(Flag77prime-)
)
:effect
(and
(Flag193-)
(Flag193prime-)
)

)
(:action Flag193Action-64
:parameters ()
:precondition
(and
(Flag188-)
(Flag188prime-)
)
:effect
(and
(Flag193-)
(Flag193prime-)
)

)
(:action Flag193Action-65
:parameters ()
:precondition
(and
(Flag92-)
(Flag92prime-)
)
:effect
(and
(Flag193-)
(Flag193prime-)
)

)
(:action Flag193Action-66
:parameters ()
:precondition
(and
(Flag100-)
(Flag100prime-)
)
:effect
(and
(Flag193-)
(Flag193prime-)
)

)
(:action Flag193Action-67
:parameters ()
:precondition
(and
(Flag52-)
(Flag52prime-)
)
:effect
(and
(Flag193-)
(Flag193prime-)
)

)
(:action Flag193Action-68
:parameters ()
:precondition
(and
(Flag49-)
(Flag49prime-)
)
:effect
(and
(Flag193-)
(Flag193prime-)
)

)
(:action Flag193Action-69
:parameters ()
:precondition
(and
(Flag67-)
(Flag67prime-)
)
:effect
(and
(Flag193-)
(Flag193prime-)
)

)
(:action Flag193Action-70
:parameters ()
:precondition
(and
(Flag137-)
(Flag137prime-)
)
:effect
(and
(Flag193-)
(Flag193prime-)
)

)
(:action Flag193Action-71
:parameters ()
:precondition
(and
(Flag117-)
(Flag117prime-)
)
:effect
(and
(Flag193-)
(Flag193prime-)
)

)
(:action Flag193Action-72
:parameters ()
:precondition
(and
(Flag107-)
(Flag107prime-)
)
:effect
(and
(Flag193-)
(Flag193prime-)
)

)
(:action Flag193Action-73
:parameters ()
:precondition
(and
(Flag150-)
(Flag150prime-)
)
:effect
(and
(Flag193-)
(Flag193prime-)
)

)
(:action Flag193Action-74
:parameters ()
:precondition
(and
(Flag152-)
(Flag152prime-)
)
:effect
(and
(Flag193-)
(Flag193prime-)
)

)
(:action Flag193Action-75
:parameters ()
:precondition
(and
(Flag90-)
(Flag90prime-)
)
:effect
(and
(Flag193-)
(Flag193prime-)
)

)
(:action Flag193Action-76
:parameters ()
:precondition
(and
(Flag179-)
(Flag179prime-)
)
:effect
(and
(Flag193-)
(Flag193prime-)
)

)
(:action Flag193Action-77
:parameters ()
:precondition
(and
(Flag166-)
(Flag166prime-)
)
:effect
(and
(Flag193-)
(Flag193prime-)
)

)
(:action Flag193Action-78
:parameters ()
:precondition
(and
(Flag108-)
(Flag108prime-)
)
:effect
(and
(Flag193-)
(Flag193prime-)
)

)
(:action Flag193Action-79
:parameters ()
:precondition
(and
(Flag153-)
(Flag153prime-)
)
:effect
(and
(Flag193-)
(Flag193prime-)
)

)
(:action Flag193Action-80
:parameters ()
:precondition
(and
(Flag131-)
(Flag131prime-)
)
:effect
(and
(Flag193-)
(Flag193prime-)
)

)
(:action Flag193Action-81
:parameters ()
:precondition
(and
(Flag189-)
(Flag189prime-)
)
:effect
(and
(Flag193-)
(Flag193prime-)
)

)
(:action Flag193Action-82
:parameters ()
:precondition
(and
(Flag43-)
(Flag43prime-)
)
:effect
(and
(Flag193-)
(Flag193prime-)
)

)
(:action Flag193Action-83
:parameters ()
:precondition
(and
(Flag190-)
(Flag190prime-)
)
:effect
(and
(Flag193-)
(Flag193prime-)
)

)
(:action Flag193Action-84
:parameters ()
:precondition
(and
(Flag191-)
(Flag191prime-)
)
:effect
(and
(Flag193-)
(Flag193prime-)
)

)
(:action Flag193Action-85
:parameters ()
:precondition
(and
(Flag80-)
(Flag80prime-)
)
:effect
(and
(Flag193-)
(Flag193prime-)
)

)
(:action Flag193Action-86
:parameters ()
:precondition
(and
(Flag167-)
(Flag167prime-)
)
:effect
(and
(Flag193-)
(Flag193prime-)
)

)
(:action Flag193Action-87
:parameters ()
:precondition
(and
(Flag51-)
(Flag51prime-)
)
:effect
(and
(Flag193-)
(Flag193prime-)
)

)
(:action Flag193Action-88
:parameters ()
:precondition
(and
(Flag124-)
(Flag124prime-)
)
:effect
(and
(Flag193-)
(Flag193prime-)
)

)
(:action Flag193Action-89
:parameters ()
:precondition
(and
(Flag70-)
(Flag70prime-)
)
:effect
(and
(Flag193-)
(Flag193prime-)
)

)
(:action Flag193Action-90
:parameters ()
:precondition
(and
(Flag61-)
(Flag61prime-)
)
:effect
(and
(Flag193-)
(Flag193prime-)
)

)
(:action Flag193Action-91
:parameters ()
:precondition
(and
(Flag168-)
(Flag168prime-)
)
:effect
(and
(Flag193-)
(Flag193prime-)
)

)
(:action Flag193Action-92
:parameters ()
:precondition
(and
(Flag155-)
(Flag155prime-)
)
:effect
(and
(Flag193-)
(Flag193prime-)
)

)
(:action Flag193Action-93
:parameters ()
:precondition
(and
(Flag192-)
(Flag192prime-)
)
:effect
(and
(Flag193-)
(Flag193prime-)
)

)
(:action Flag193Action-94
:parameters ()
:precondition
(and
(Flag88-)
(Flag88prime-)
)
:effect
(and
(Flag193-)
(Flag193prime-)
)

)
(:action Flag193Action-95
:parameters ()
:precondition
(and
(Flag181-)
(Flag181prime-)
)
:effect
(and
(Flag193-)
(Flag193prime-)
)

)
(:action Prim193Action
:parameters ()
:precondition
(and
(not (Flag78-))
(Flag78prime-)
(not (Flag44-))
(Flag44prime-)
(not (Flag148-))
(Flag148prime-)
(not (Flag95-))
(Flag95prime-)
(not (Flag116-))
(Flag116prime-)
(not (Flag122-))
(Flag122prime-)
(not (Flag59-))
(Flag59prime-)
(not (Flag143-))
(Flag143prime-)
(not (Flag170-))
(Flag170prime-)
(not (Flag144-))
(Flag144prime-)
(not (Flag183-))
(Flag183prime-)
(not (Flag55-))
(Flag55prime-)
(not (Flag81-))
(Flag81prime-)
(not (Flag184-))
(Flag184prime-)
(not (Flag86-))
(Flag86prime-)
(not (Flag103-))
(Flag103prime-)
(not (Flag112-))
(Flag112prime-)
(not (Flag63-))
(Flag63prime-)
(not (Flag171-))
(Flag171prime-)
(not (Flag120-))
(Flag120prime-)
(not (Flag172-))
(Flag172prime-)
(not (Flag121-))
(Flag121prime-)
(not (Flag56-))
(Flag56prime-)
(not (Flag157-))
(Flag157prime-)
(not (Flag158-))
(Flag158prime-)
(not (Flag173-))
(Flag173prime-)
(not (Flag45-))
(Flag45prime-)
(not (Flag159-))
(Flag159prime-)
(not (Flag93-))
(Flag93prime-)
(not (Flag128-))
(Flag128prime-)
(not (Flag160-))
(Flag160prime-)
(not (Flag146-))
(Flag146prime-)
(not (Flag174-))
(Flag174prime-)
(not (Flag129-))
(Flag129prime-)
(not (Flag130-))
(Flag130prime-)
(not (Flag185-))
(Flag185prime-)
(not (Flag175-))
(Flag175prime-)
(not (Flag161-))
(Flag161prime-)
(not (Flag115-))
(Flag115prime-)
(not (Flag140-))
(Flag140prime-)
(not (Flag147-))
(Flag147prime-)
(not (Flag98-))
(Flag98prime-)
(not (Flag58-))
(Flag58prime-)
(not (Flag162-))
(Flag162prime-)
(not (Flag60-))
(Flag60prime-)
(not (Flag135-))
(Flag135prime-)
(not (Flag64-))
(Flag64prime-)
(not (Flag104-))
(Flag104prime-)
(not (Flag186-))
(Flag186prime-)
(not (Flag105-))
(Flag105prime-)
(not (Flag68-))
(Flag68prime-)
(not (Flag89-))
(Flag89prime-)
(not (Flag139-))
(Flag139prime-)
(not (Flag187-))
(Flag187prime-)
(not (Flag38-))
(Flag38prime-)
(not (Flag110-))
(Flag110prime-)
(not (Flag79-))
(Flag79prime-)
(not (Flag126-))
(Flag126prime-)
(not (Flag149-))
(Flag149prime-)
(not (Flag138-))
(Flag138prime-)
(not (Flag177-))
(Flag177prime-)
(not (Flag114-))
(Flag114prime-)
(not (Flag40-))
(Flag40prime-)
(not (Flag77-))
(Flag77prime-)
(not (Flag188-))
(Flag188prime-)
(not (Flag92-))
(Flag92prime-)
(not (Flag100-))
(Flag100prime-)
(not (Flag52-))
(Flag52prime-)
(not (Flag49-))
(Flag49prime-)
(not (Flag67-))
(Flag67prime-)
(not (Flag137-))
(Flag137prime-)
(not (Flag117-))
(Flag117prime-)
(not (Flag107-))
(Flag107prime-)
(not (Flag150-))
(Flag150prime-)
(not (Flag152-))
(Flag152prime-)
(not (Flag90-))
(Flag90prime-)
(not (Flag179-))
(Flag179prime-)
(not (Flag166-))
(Flag166prime-)
(not (Flag108-))
(Flag108prime-)
(not (Flag153-))
(Flag153prime-)
(not (Flag131-))
(Flag131prime-)
(not (Flag189-))
(Flag189prime-)
(not (Flag43-))
(Flag43prime-)
(not (Flag190-))
(Flag190prime-)
(not (Flag191-))
(Flag191prime-)
(not (Flag80-))
(Flag80prime-)
(not (Flag167-))
(Flag167prime-)
(not (Flag51-))
(Flag51prime-)
(not (Flag124-))
(Flag124prime-)
(not (Flag70-))
(Flag70prime-)
(not (Flag61-))
(Flag61prime-)
(not (Flag168-))
(Flag168prime-)
(not (Flag155-))
(Flag155prime-)
(not (Flag192-))
(Flag192prime-)
(not (Flag88-))
(Flag88prime-)
(not (Flag181-))
(Flag181prime-)
)
:effect
(and
(Flag193prime-)
(not (Flag193-))
)

)
(:action Flag202Action-0
:parameters ()
:precondition
(and
(Flag78-)
(Flag78prime-)
)
:effect
(and
(Flag202-)
(Flag202prime-)
)

)
(:action Flag202Action-1
:parameters ()
:precondition
(and
(Flag44-)
(Flag44prime-)
)
:effect
(and
(Flag202-)
(Flag202prime-)
)

)
(:action Flag202Action-2
:parameters ()
:precondition
(and
(Flag148-)
(Flag148prime-)
)
:effect
(and
(Flag202-)
(Flag202prime-)
)

)
(:action Flag202Action-3
:parameters ()
:precondition
(and
(Flag95-)
(Flag95prime-)
)
:effect
(and
(Flag202-)
(Flag202prime-)
)

)
(:action Flag202Action-4
:parameters ()
:precondition
(and
(Flag116-)
(Flag116prime-)
)
:effect
(and
(Flag202-)
(Flag202prime-)
)

)
(:action Flag202Action-5
:parameters ()
:precondition
(and
(Flag122-)
(Flag122prime-)
)
:effect
(and
(Flag202-)
(Flag202prime-)
)

)
(:action Flag202Action-6
:parameters ()
:precondition
(and
(Flag59-)
(Flag59prime-)
)
:effect
(and
(Flag202-)
(Flag202prime-)
)

)
(:action Flag202Action-7
:parameters ()
:precondition
(and
(Flag143-)
(Flag143prime-)
)
:effect
(and
(Flag202-)
(Flag202prime-)
)

)
(:action Flag202Action-8
:parameters ()
:precondition
(and
(Flag170-)
(Flag170prime-)
)
:effect
(and
(Flag202-)
(Flag202prime-)
)

)
(:action Flag202Action-9
:parameters ()
:precondition
(and
(Flag144-)
(Flag144prime-)
)
:effect
(and
(Flag202-)
(Flag202prime-)
)

)
(:action Flag202Action-10
:parameters ()
:precondition
(and
(Flag183-)
(Flag183prime-)
)
:effect
(and
(Flag202-)
(Flag202prime-)
)

)
(:action Flag202Action-11
:parameters ()
:precondition
(and
(Flag81-)
(Flag81prime-)
)
:effect
(and
(Flag202-)
(Flag202prime-)
)

)
(:action Flag202Action-12
:parameters ()
:precondition
(and
(Flag184-)
(Flag184prime-)
)
:effect
(and
(Flag202-)
(Flag202prime-)
)

)
(:action Flag202Action-13
:parameters ()
:precondition
(and
(Flag86-)
(Flag86prime-)
)
:effect
(and
(Flag202-)
(Flag202prime-)
)

)
(:action Flag202Action-14
:parameters ()
:precondition
(and
(Flag103-)
(Flag103prime-)
)
:effect
(and
(Flag202-)
(Flag202prime-)
)

)
(:action Flag202Action-15
:parameters ()
:precondition
(and
(Flag112-)
(Flag112prime-)
)
:effect
(and
(Flag202-)
(Flag202prime-)
)

)
(:action Flag202Action-16
:parameters ()
:precondition
(and
(Flag171-)
(Flag171prime-)
)
:effect
(and
(Flag202-)
(Flag202prime-)
)

)
(:action Flag202Action-17
:parameters ()
:precondition
(and
(Flag172-)
(Flag172prime-)
)
:effect
(and
(Flag202-)
(Flag202prime-)
)

)
(:action Flag202Action-18
:parameters ()
:precondition
(and
(Flag121-)
(Flag121prime-)
)
:effect
(and
(Flag202-)
(Flag202prime-)
)

)
(:action Flag202Action-19
:parameters ()
:precondition
(and
(Flag56-)
(Flag56prime-)
)
:effect
(and
(Flag202-)
(Flag202prime-)
)

)
(:action Flag202Action-20
:parameters ()
:precondition
(and
(Flag157-)
(Flag157prime-)
)
:effect
(and
(Flag202-)
(Flag202prime-)
)

)
(:action Flag202Action-21
:parameters ()
:precondition
(and
(Flag158-)
(Flag158prime-)
)
:effect
(and
(Flag202-)
(Flag202prime-)
)

)
(:action Flag202Action-22
:parameters ()
:precondition
(and
(Flag173-)
(Flag173prime-)
)
:effect
(and
(Flag202-)
(Flag202prime-)
)

)
(:action Flag202Action-23
:parameters ()
:precondition
(and
(Flag45-)
(Flag45prime-)
)
:effect
(and
(Flag202-)
(Flag202prime-)
)

)
(:action Flag202Action-24
:parameters ()
:precondition
(and
(Flag194-)
(Flag194prime-)
)
:effect
(and
(Flag202-)
(Flag202prime-)
)

)
(:action Flag202Action-25
:parameters ()
:precondition
(and
(Flag159-)
(Flag159prime-)
)
:effect
(and
(Flag202-)
(Flag202prime-)
)

)
(:action Flag202Action-26
:parameters ()
:precondition
(and
(Flag93-)
(Flag93prime-)
)
:effect
(and
(Flag202-)
(Flag202prime-)
)

)
(:action Flag202Action-27
:parameters ()
:precondition
(and
(Flag128-)
(Flag128prime-)
)
:effect
(and
(Flag202-)
(Flag202prime-)
)

)
(:action Flag202Action-28
:parameters ()
:precondition
(and
(Flag160-)
(Flag160prime-)
)
:effect
(and
(Flag202-)
(Flag202prime-)
)

)
(:action Flag202Action-29
:parameters ()
:precondition
(and
(Flag146-)
(Flag146prime-)
)
:effect
(and
(Flag202-)
(Flag202prime-)
)

)
(:action Flag202Action-30
:parameters ()
:precondition
(and
(Flag174-)
(Flag174prime-)
)
:effect
(and
(Flag202-)
(Flag202prime-)
)

)
(:action Flag202Action-31
:parameters ()
:precondition
(and
(Flag129-)
(Flag129prime-)
)
:effect
(and
(Flag202-)
(Flag202prime-)
)

)
(:action Flag202Action-32
:parameters ()
:precondition
(and
(Flag130-)
(Flag130prime-)
)
:effect
(and
(Flag202-)
(Flag202prime-)
)

)
(:action Flag202Action-33
:parameters ()
:precondition
(and
(Flag185-)
(Flag185prime-)
)
:effect
(and
(Flag202-)
(Flag202prime-)
)

)
(:action Flag202Action-34
:parameters ()
:precondition
(and
(Flag175-)
(Flag175prime-)
)
:effect
(and
(Flag202-)
(Flag202prime-)
)

)
(:action Flag202Action-35
:parameters ()
:precondition
(and
(Flag161-)
(Flag161prime-)
)
:effect
(and
(Flag202-)
(Flag202prime-)
)

)
(:action Flag202Action-36
:parameters ()
:precondition
(and
(Flag115-)
(Flag115prime-)
)
:effect
(and
(Flag202-)
(Flag202prime-)
)

)
(:action Flag202Action-37
:parameters ()
:precondition
(and
(Flag140-)
(Flag140prime-)
)
:effect
(and
(Flag202-)
(Flag202prime-)
)

)
(:action Flag202Action-38
:parameters ()
:precondition
(and
(Flag147-)
(Flag147prime-)
)
:effect
(and
(Flag202-)
(Flag202prime-)
)

)
(:action Flag202Action-39
:parameters ()
:precondition
(and
(Flag195-)
(Flag195prime-)
)
:effect
(and
(Flag202-)
(Flag202prime-)
)

)
(:action Flag202Action-40
:parameters ()
:precondition
(and
(Flag98-)
(Flag98prime-)
)
:effect
(and
(Flag202-)
(Flag202prime-)
)

)
(:action Flag202Action-41
:parameters ()
:precondition
(and
(Flag58-)
(Flag58prime-)
)
:effect
(and
(Flag202-)
(Flag202prime-)
)

)
(:action Flag202Action-42
:parameters ()
:precondition
(and
(Flag196-)
(Flag196prime-)
)
:effect
(and
(Flag202-)
(Flag202prime-)
)

)
(:action Flag202Action-43
:parameters ()
:precondition
(and
(Flag60-)
(Flag60prime-)
)
:effect
(and
(Flag202-)
(Flag202prime-)
)

)
(:action Flag202Action-44
:parameters ()
:precondition
(and
(Flag135-)
(Flag135prime-)
)
:effect
(and
(Flag202-)
(Flag202prime-)
)

)
(:action Flag202Action-45
:parameters ()
:precondition
(and
(Flag64-)
(Flag64prime-)
)
:effect
(and
(Flag202-)
(Flag202prime-)
)

)
(:action Flag202Action-46
:parameters ()
:precondition
(and
(Flag104-)
(Flag104prime-)
)
:effect
(and
(Flag202-)
(Flag202prime-)
)

)
(:action Flag202Action-47
:parameters ()
:precondition
(and
(Flag105-)
(Flag105prime-)
)
:effect
(and
(Flag202-)
(Flag202prime-)
)

)
(:action Flag202Action-48
:parameters ()
:precondition
(and
(Flag68-)
(Flag68prime-)
)
:effect
(and
(Flag202-)
(Flag202prime-)
)

)
(:action Flag202Action-49
:parameters ()
:precondition
(and
(Flag89-)
(Flag89prime-)
)
:effect
(and
(Flag202-)
(Flag202prime-)
)

)
(:action Flag202Action-50
:parameters ()
:precondition
(and
(Flag187-)
(Flag187prime-)
)
:effect
(and
(Flag202-)
(Flag202prime-)
)

)
(:action Flag202Action-51
:parameters ()
:precondition
(and
(Flag38-)
(Flag38prime-)
)
:effect
(and
(Flag202-)
(Flag202prime-)
)

)
(:action Flag202Action-52
:parameters ()
:precondition
(and
(Flag110-)
(Flag110prime-)
)
:effect
(and
(Flag202-)
(Flag202prime-)
)

)
(:action Flag202Action-53
:parameters ()
:precondition
(and
(Flag79-)
(Flag79prime-)
)
:effect
(and
(Flag202-)
(Flag202prime-)
)

)
(:action Flag202Action-54
:parameters ()
:precondition
(and
(Flag126-)
(Flag126prime-)
)
:effect
(and
(Flag202-)
(Flag202prime-)
)

)
(:action Flag202Action-55
:parameters ()
:precondition
(and
(Flag149-)
(Flag149prime-)
)
:effect
(and
(Flag202-)
(Flag202prime-)
)

)
(:action Flag202Action-56
:parameters ()
:precondition
(and
(Flag138-)
(Flag138prime-)
)
:effect
(and
(Flag202-)
(Flag202prime-)
)

)
(:action Flag202Action-57
:parameters ()
:precondition
(and
(Flag177-)
(Flag177prime-)
)
:effect
(and
(Flag202-)
(Flag202prime-)
)

)
(:action Flag202Action-58
:parameters ()
:precondition
(and
(Flag114-)
(Flag114prime-)
)
:effect
(and
(Flag202-)
(Flag202prime-)
)

)
(:action Flag202Action-59
:parameters ()
:precondition
(and
(Flag40-)
(Flag40prime-)
)
:effect
(and
(Flag202-)
(Flag202prime-)
)

)
(:action Flag202Action-60
:parameters ()
:precondition
(and
(Flag77-)
(Flag77prime-)
)
:effect
(and
(Flag202-)
(Flag202prime-)
)

)
(:action Flag202Action-61
:parameters ()
:precondition
(and
(Flag188-)
(Flag188prime-)
)
:effect
(and
(Flag202-)
(Flag202prime-)
)

)
(:action Flag202Action-62
:parameters ()
:precondition
(and
(Flag197-)
(Flag197prime-)
)
:effect
(and
(Flag202-)
(Flag202prime-)
)

)
(:action Flag202Action-63
:parameters ()
:precondition
(and
(Flag198-)
(Flag198prime-)
)
:effect
(and
(Flag202-)
(Flag202prime-)
)

)
(:action Flag202Action-64
:parameters ()
:precondition
(and
(Flag52-)
(Flag52prime-)
)
:effect
(and
(Flag202-)
(Flag202prime-)
)

)
(:action Flag202Action-65
:parameters ()
:precondition
(and
(Flag49-)
(Flag49prime-)
)
:effect
(and
(Flag202-)
(Flag202prime-)
)

)
(:action Flag202Action-66
:parameters ()
:precondition
(and
(Flag67-)
(Flag67prime-)
)
:effect
(and
(Flag202-)
(Flag202prime-)
)

)
(:action Flag202Action-67
:parameters ()
:precondition
(and
(Flag117-)
(Flag117prime-)
)
:effect
(and
(Flag202-)
(Flag202prime-)
)

)
(:action Flag202Action-68
:parameters ()
:precondition
(and
(Flag100-)
(Flag100prime-)
)
:effect
(and
(Flag202-)
(Flag202prime-)
)

)
(:action Flag202Action-69
:parameters ()
:precondition
(and
(Flag107-)
(Flag107prime-)
)
:effect
(and
(Flag202-)
(Flag202prime-)
)

)
(:action Flag202Action-70
:parameters ()
:precondition
(and
(Flag150-)
(Flag150prime-)
)
:effect
(and
(Flag202-)
(Flag202prime-)
)

)
(:action Flag202Action-71
:parameters ()
:precondition
(and
(Flag152-)
(Flag152prime-)
)
:effect
(and
(Flag202-)
(Flag202prime-)
)

)
(:action Flag202Action-72
:parameters ()
:precondition
(and
(Flag90-)
(Flag90prime-)
)
:effect
(and
(Flag202-)
(Flag202prime-)
)

)
(:action Flag202Action-73
:parameters ()
:precondition
(and
(Flag179-)
(Flag179prime-)
)
:effect
(and
(Flag202-)
(Flag202prime-)
)

)
(:action Flag202Action-74
:parameters ()
:precondition
(and
(Flag166-)
(Flag166prime-)
)
:effect
(and
(Flag202-)
(Flag202prime-)
)

)
(:action Flag202Action-75
:parameters ()
:precondition
(and
(Flag108-)
(Flag108prime-)
)
:effect
(and
(Flag202-)
(Flag202prime-)
)

)
(:action Flag202Action-76
:parameters ()
:precondition
(and
(Flag199-)
(Flag199prime-)
)
:effect
(and
(Flag202-)
(Flag202prime-)
)

)
(:action Flag202Action-77
:parameters ()
:precondition
(and
(Flag131-)
(Flag131prime-)
)
:effect
(and
(Flag202-)
(Flag202prime-)
)

)
(:action Flag202Action-78
:parameters ()
:precondition
(and
(Flag200-)
(Flag200prime-)
)
:effect
(and
(Flag202-)
(Flag202prime-)
)

)
(:action Flag202Action-79
:parameters ()
:precondition
(and
(Flag51-)
(Flag51prime-)
)
:effect
(and
(Flag202-)
(Flag202prime-)
)

)
(:action Flag202Action-80
:parameters ()
:precondition
(and
(Flag43-)
(Flag43prime-)
)
:effect
(and
(Flag202-)
(Flag202prime-)
)

)
(:action Flag202Action-81
:parameters ()
:precondition
(and
(Flag190-)
(Flag190prime-)
)
:effect
(and
(Flag202-)
(Flag202prime-)
)

)
(:action Flag202Action-82
:parameters ()
:precondition
(and
(Flag191-)
(Flag191prime-)
)
:effect
(and
(Flag202-)
(Flag202prime-)
)

)
(:action Flag202Action-83
:parameters ()
:precondition
(and
(Flag167-)
(Flag167prime-)
)
:effect
(and
(Flag202-)
(Flag202prime-)
)

)
(:action Flag202Action-84
:parameters ()
:precondition
(and
(Flag124-)
(Flag124prime-)
)
:effect
(and
(Flag202-)
(Flag202prime-)
)

)
(:action Flag202Action-85
:parameters ()
:precondition
(and
(Flag70-)
(Flag70prime-)
)
:effect
(and
(Flag202-)
(Flag202prime-)
)

)
(:action Flag202Action-86
:parameters ()
:precondition
(and
(Flag61-)
(Flag61prime-)
)
:effect
(and
(Flag202-)
(Flag202prime-)
)

)
(:action Flag202Action-87
:parameters ()
:precondition
(and
(Flag168-)
(Flag168prime-)
)
:effect
(and
(Flag202-)
(Flag202prime-)
)

)
(:action Flag202Action-88
:parameters ()
:precondition
(and
(Flag155-)
(Flag155prime-)
)
:effect
(and
(Flag202-)
(Flag202prime-)
)

)
(:action Flag202Action-89
:parameters ()
:precondition
(and
(Flag192-)
(Flag192prime-)
)
:effect
(and
(Flag202-)
(Flag202prime-)
)

)
(:action Flag202Action-90
:parameters ()
:precondition
(and
(Flag88-)
(Flag88prime-)
)
:effect
(and
(Flag202-)
(Flag202prime-)
)

)
(:action Flag202Action-91
:parameters ()
:precondition
(and
(Flag201-)
(Flag201prime-)
)
:effect
(and
(Flag202-)
(Flag202prime-)
)

)
(:action Flag202Action-92
:parameters ()
:precondition
(and
(Flag181-)
(Flag181prime-)
)
:effect
(and
(Flag202-)
(Flag202prime-)
)

)
(:action Prim202Action
:parameters ()
:precondition
(and
(not (Flag78-))
(Flag78prime-)
(not (Flag44-))
(Flag44prime-)
(not (Flag148-))
(Flag148prime-)
(not (Flag95-))
(Flag95prime-)
(not (Flag116-))
(Flag116prime-)
(not (Flag122-))
(Flag122prime-)
(not (Flag59-))
(Flag59prime-)
(not (Flag143-))
(Flag143prime-)
(not (Flag170-))
(Flag170prime-)
(not (Flag144-))
(Flag144prime-)
(not (Flag183-))
(Flag183prime-)
(not (Flag81-))
(Flag81prime-)
(not (Flag184-))
(Flag184prime-)
(not (Flag86-))
(Flag86prime-)
(not (Flag103-))
(Flag103prime-)
(not (Flag112-))
(Flag112prime-)
(not (Flag171-))
(Flag171prime-)
(not (Flag172-))
(Flag172prime-)
(not (Flag121-))
(Flag121prime-)
(not (Flag56-))
(Flag56prime-)
(not (Flag157-))
(Flag157prime-)
(not (Flag158-))
(Flag158prime-)
(not (Flag173-))
(Flag173prime-)
(not (Flag45-))
(Flag45prime-)
(not (Flag194-))
(Flag194prime-)
(not (Flag159-))
(Flag159prime-)
(not (Flag93-))
(Flag93prime-)
(not (Flag128-))
(Flag128prime-)
(not (Flag160-))
(Flag160prime-)
(not (Flag146-))
(Flag146prime-)
(not (Flag174-))
(Flag174prime-)
(not (Flag129-))
(Flag129prime-)
(not (Flag130-))
(Flag130prime-)
(not (Flag185-))
(Flag185prime-)
(not (Flag175-))
(Flag175prime-)
(not (Flag161-))
(Flag161prime-)
(not (Flag115-))
(Flag115prime-)
(not (Flag140-))
(Flag140prime-)
(not (Flag147-))
(Flag147prime-)
(not (Flag195-))
(Flag195prime-)
(not (Flag98-))
(Flag98prime-)
(not (Flag58-))
(Flag58prime-)
(not (Flag196-))
(Flag196prime-)
(not (Flag60-))
(Flag60prime-)
(not (Flag135-))
(Flag135prime-)
(not (Flag64-))
(Flag64prime-)
(not (Flag104-))
(Flag104prime-)
(not (Flag105-))
(Flag105prime-)
(not (Flag68-))
(Flag68prime-)
(not (Flag89-))
(Flag89prime-)
(not (Flag187-))
(Flag187prime-)
(not (Flag38-))
(Flag38prime-)
(not (Flag110-))
(Flag110prime-)
(not (Flag79-))
(Flag79prime-)
(not (Flag126-))
(Flag126prime-)
(not (Flag149-))
(Flag149prime-)
(not (Flag138-))
(Flag138prime-)
(not (Flag177-))
(Flag177prime-)
(not (Flag114-))
(Flag114prime-)
(not (Flag40-))
(Flag40prime-)
(not (Flag77-))
(Flag77prime-)
(not (Flag188-))
(Flag188prime-)
(not (Flag197-))
(Flag197prime-)
(not (Flag198-))
(Flag198prime-)
(not (Flag52-))
(Flag52prime-)
(not (Flag49-))
(Flag49prime-)
(not (Flag67-))
(Flag67prime-)
(not (Flag117-))
(Flag117prime-)
(not (Flag100-))
(Flag100prime-)
(not (Flag107-))
(Flag107prime-)
(not (Flag150-))
(Flag150prime-)
(not (Flag152-))
(Flag152prime-)
(not (Flag90-))
(Flag90prime-)
(not (Flag179-))
(Flag179prime-)
(not (Flag166-))
(Flag166prime-)
(not (Flag108-))
(Flag108prime-)
(not (Flag199-))
(Flag199prime-)
(not (Flag131-))
(Flag131prime-)
(not (Flag200-))
(Flag200prime-)
(not (Flag51-))
(Flag51prime-)
(not (Flag43-))
(Flag43prime-)
(not (Flag190-))
(Flag190prime-)
(not (Flag191-))
(Flag191prime-)
(not (Flag167-))
(Flag167prime-)
(not (Flag124-))
(Flag124prime-)
(not (Flag70-))
(Flag70prime-)
(not (Flag61-))
(Flag61prime-)
(not (Flag168-))
(Flag168prime-)
(not (Flag155-))
(Flag155prime-)
(not (Flag192-))
(Flag192prime-)
(not (Flag88-))
(Flag88prime-)
(not (Flag201-))
(Flag201prime-)
(not (Flag181-))
(Flag181prime-)
)
:effect
(and
(Flag202prime-)
(not (Flag202-))
)

)
(:action Flag210Action-0
:parameters ()
:precondition
(and
(Flag44-)
(Flag44prime-)
)
:effect
(and
(Flag210-)
(Flag210prime-)
)

)
(:action Flag210Action-1
:parameters ()
:precondition
(and
(Flag79-)
(Flag79prime-)
)
:effect
(and
(Flag210-)
(Flag210prime-)
)

)
(:action Flag210Action-2
:parameters ()
:precondition
(and
(Flag95-)
(Flag95prime-)
)
:effect
(and
(Flag210-)
(Flag210prime-)
)

)
(:action Flag210Action-3
:parameters ()
:precondition
(and
(Flag49-)
(Flag49prime-)
)
:effect
(and
(Flag210-)
(Flag210prime-)
)

)
(:action Flag210Action-4
:parameters ()
:precondition
(and
(Flag59-)
(Flag59prime-)
)
:effect
(and
(Flag210-)
(Flag210prime-)
)

)
(:action Flag210Action-5
:parameters ()
:precondition
(and
(Flag143-)
(Flag143prime-)
)
:effect
(and
(Flag210-)
(Flag210prime-)
)

)
(:action Flag210Action-6
:parameters ()
:precondition
(and
(Flag203-)
(Flag203prime-)
)
:effect
(and
(Flag210-)
(Flag210prime-)
)

)
(:action Flag210Action-7
:parameters ()
:precondition
(and
(Flag170-)
(Flag170prime-)
)
:effect
(and
(Flag210-)
(Flag210prime-)
)

)
(:action Flag210Action-8
:parameters ()
:precondition
(and
(Flag144-)
(Flag144prime-)
)
:effect
(and
(Flag210-)
(Flag210prime-)
)

)
(:action Flag210Action-9
:parameters ()
:precondition
(and
(Flag81-)
(Flag81prime-)
)
:effect
(and
(Flag210-)
(Flag210prime-)
)

)
(:action Flag210Action-10
:parameters ()
:precondition
(and
(Flag184-)
(Flag184prime-)
)
:effect
(and
(Flag210-)
(Flag210prime-)
)

)
(:action Flag210Action-11
:parameters ()
:precondition
(and
(Flag112-)
(Flag112prime-)
)
:effect
(and
(Flag210-)
(Flag210prime-)
)

)
(:action Flag210Action-12
:parameters ()
:precondition
(and
(Flag171-)
(Flag171prime-)
)
:effect
(and
(Flag210-)
(Flag210prime-)
)

)
(:action Flag210Action-13
:parameters ()
:precondition
(and
(Flag172-)
(Flag172prime-)
)
:effect
(and
(Flag210-)
(Flag210prime-)
)

)
(:action Flag210Action-14
:parameters ()
:precondition
(and
(Flag121-)
(Flag121prime-)
)
:effect
(and
(Flag210-)
(Flag210prime-)
)

)
(:action Flag210Action-15
:parameters ()
:precondition
(and
(Flag56-)
(Flag56prime-)
)
:effect
(and
(Flag210-)
(Flag210prime-)
)

)
(:action Flag210Action-16
:parameters ()
:precondition
(and
(Flag157-)
(Flag157prime-)
)
:effect
(and
(Flag210-)
(Flag210prime-)
)

)
(:action Flag210Action-17
:parameters ()
:precondition
(and
(Flag158-)
(Flag158prime-)
)
:effect
(and
(Flag210-)
(Flag210prime-)
)

)
(:action Flag210Action-18
:parameters ()
:precondition
(and
(Flag173-)
(Flag173prime-)
)
:effect
(and
(Flag210-)
(Flag210prime-)
)

)
(:action Flag210Action-19
:parameters ()
:precondition
(and
(Flag45-)
(Flag45prime-)
)
:effect
(and
(Flag210-)
(Flag210prime-)
)

)
(:action Flag210Action-20
:parameters ()
:precondition
(and
(Flag194-)
(Flag194prime-)
)
:effect
(and
(Flag210-)
(Flag210prime-)
)

)
(:action Flag210Action-21
:parameters ()
:precondition
(and
(Flag159-)
(Flag159prime-)
)
:effect
(and
(Flag210-)
(Flag210prime-)
)

)
(:action Flag210Action-22
:parameters ()
:precondition
(and
(Flag93-)
(Flag93prime-)
)
:effect
(and
(Flag210-)
(Flag210prime-)
)

)
(:action Flag210Action-23
:parameters ()
:precondition
(and
(Flag128-)
(Flag128prime-)
)
:effect
(and
(Flag210-)
(Flag210prime-)
)

)
(:action Flag210Action-24
:parameters ()
:precondition
(and
(Flag160-)
(Flag160prime-)
)
:effect
(and
(Flag210-)
(Flag210prime-)
)

)
(:action Flag210Action-25
:parameters ()
:precondition
(and
(Flag146-)
(Flag146prime-)
)
:effect
(and
(Flag210-)
(Flag210prime-)
)

)
(:action Flag210Action-26
:parameters ()
:precondition
(and
(Flag129-)
(Flag129prime-)
)
:effect
(and
(Flag210-)
(Flag210prime-)
)

)
(:action Flag210Action-27
:parameters ()
:precondition
(and
(Flag130-)
(Flag130prime-)
)
:effect
(and
(Flag210-)
(Flag210prime-)
)

)
(:action Flag210Action-28
:parameters ()
:precondition
(and
(Flag204-)
(Flag204prime-)
)
:effect
(and
(Flag210-)
(Flag210prime-)
)

)
(:action Flag210Action-29
:parameters ()
:precondition
(and
(Flag185-)
(Flag185prime-)
)
:effect
(and
(Flag210-)
(Flag210prime-)
)

)
(:action Flag210Action-30
:parameters ()
:precondition
(and
(Flag175-)
(Flag175prime-)
)
:effect
(and
(Flag210-)
(Flag210prime-)
)

)
(:action Flag210Action-31
:parameters ()
:precondition
(and
(Flag115-)
(Flag115prime-)
)
:effect
(and
(Flag210-)
(Flag210prime-)
)

)
(:action Flag210Action-32
:parameters ()
:precondition
(and
(Flag140-)
(Flag140prime-)
)
:effect
(and
(Flag210-)
(Flag210prime-)
)

)
(:action Flag210Action-33
:parameters ()
:precondition
(and
(Flag195-)
(Flag195prime-)
)
:effect
(and
(Flag210-)
(Flag210prime-)
)

)
(:action Flag210Action-34
:parameters ()
:precondition
(and
(Flag98-)
(Flag98prime-)
)
:effect
(and
(Flag210-)
(Flag210prime-)
)

)
(:action Flag210Action-35
:parameters ()
:precondition
(and
(Flag58-)
(Flag58prime-)
)
:effect
(and
(Flag210-)
(Flag210prime-)
)

)
(:action Flag210Action-36
:parameters ()
:precondition
(and
(Flag196-)
(Flag196prime-)
)
:effect
(and
(Flag210-)
(Flag210prime-)
)

)
(:action Flag210Action-37
:parameters ()
:precondition
(and
(Flag135-)
(Flag135prime-)
)
:effect
(and
(Flag210-)
(Flag210prime-)
)

)
(:action Flag210Action-38
:parameters ()
:precondition
(and
(Flag104-)
(Flag104prime-)
)
:effect
(and
(Flag210-)
(Flag210prime-)
)

)
(:action Flag210Action-39
:parameters ()
:precondition
(and
(Flag105-)
(Flag105prime-)
)
:effect
(and
(Flag210-)
(Flag210prime-)
)

)
(:action Flag210Action-40
:parameters ()
:precondition
(and
(Flag205-)
(Flag205prime-)
)
:effect
(and
(Flag210-)
(Flag210prime-)
)

)
(:action Flag210Action-41
:parameters ()
:precondition
(and
(Flag68-)
(Flag68prime-)
)
:effect
(and
(Flag210-)
(Flag210prime-)
)

)
(:action Flag210Action-42
:parameters ()
:precondition
(and
(Flag89-)
(Flag89prime-)
)
:effect
(and
(Flag210-)
(Flag210prime-)
)

)
(:action Flag210Action-43
:parameters ()
:precondition
(and
(Flag187-)
(Flag187prime-)
)
:effect
(and
(Flag210-)
(Flag210prime-)
)

)
(:action Flag210Action-44
:parameters ()
:precondition
(and
(Flag38-)
(Flag38prime-)
)
:effect
(and
(Flag210-)
(Flag210prime-)
)

)
(:action Flag210Action-45
:parameters ()
:precondition
(and
(Flag110-)
(Flag110prime-)
)
:effect
(and
(Flag210-)
(Flag210prime-)
)

)
(:action Flag210Action-46
:parameters ()
:precondition
(and
(Flag126-)
(Flag126prime-)
)
:effect
(and
(Flag210-)
(Flag210prime-)
)

)
(:action Flag210Action-47
:parameters ()
:precondition
(and
(Flag149-)
(Flag149prime-)
)
:effect
(and
(Flag210-)
(Flag210prime-)
)

)
(:action Flag210Action-48
:parameters ()
:precondition
(and
(Flag138-)
(Flag138prime-)
)
:effect
(and
(Flag210-)
(Flag210prime-)
)

)
(:action Flag210Action-49
:parameters ()
:precondition
(and
(Flag177-)
(Flag177prime-)
)
:effect
(and
(Flag210-)
(Flag210prime-)
)

)
(:action Flag210Action-50
:parameters ()
:precondition
(and
(Flag114-)
(Flag114prime-)
)
:effect
(and
(Flag210-)
(Flag210prime-)
)

)
(:action Flag210Action-51
:parameters ()
:precondition
(and
(Flag40-)
(Flag40prime-)
)
:effect
(and
(Flag210-)
(Flag210prime-)
)

)
(:action Flag210Action-52
:parameters ()
:precondition
(and
(Flag77-)
(Flag77prime-)
)
:effect
(and
(Flag210-)
(Flag210prime-)
)

)
(:action Flag210Action-53
:parameters ()
:precondition
(and
(Flag188-)
(Flag188prime-)
)
:effect
(and
(Flag210-)
(Flag210prime-)
)

)
(:action Flag210Action-54
:parameters ()
:precondition
(and
(Flag197-)
(Flag197prime-)
)
:effect
(and
(Flag210-)
(Flag210prime-)
)

)
(:action Flag210Action-55
:parameters ()
:precondition
(and
(Flag198-)
(Flag198prime-)
)
:effect
(and
(Flag210-)
(Flag210prime-)
)

)
(:action Flag210Action-56
:parameters ()
:precondition
(and
(Flag52-)
(Flag52prime-)
)
:effect
(and
(Flag210-)
(Flag210prime-)
)

)
(:action Flag210Action-57
:parameters ()
:precondition
(and
(Flag122-)
(Flag122prime-)
)
:effect
(and
(Flag210-)
(Flag210prime-)
)

)
(:action Flag210Action-58
:parameters ()
:precondition
(and
(Flag206-)
(Flag206prime-)
)
:effect
(and
(Flag210-)
(Flag210prime-)
)

)
(:action Flag210Action-59
:parameters ()
:precondition
(and
(Flag67-)
(Flag67prime-)
)
:effect
(and
(Flag210-)
(Flag210prime-)
)

)
(:action Flag210Action-60
:parameters ()
:precondition
(and
(Flag117-)
(Flag117prime-)
)
:effect
(and
(Flag210-)
(Flag210prime-)
)

)
(:action Flag210Action-61
:parameters ()
:precondition
(and
(Flag100-)
(Flag100prime-)
)
:effect
(and
(Flag210-)
(Flag210prime-)
)

)
(:action Flag210Action-62
:parameters ()
:precondition
(and
(Flag107-)
(Flag107prime-)
)
:effect
(and
(Flag210-)
(Flag210prime-)
)

)
(:action Flag210Action-63
:parameters ()
:precondition
(and
(Flag150-)
(Flag150prime-)
)
:effect
(and
(Flag210-)
(Flag210prime-)
)

)
(:action Flag210Action-64
:parameters ()
:precondition
(and
(Flag152-)
(Flag152prime-)
)
:effect
(and
(Flag210-)
(Flag210prime-)
)

)
(:action Flag210Action-65
:parameters ()
:precondition
(and
(Flag78-)
(Flag78prime-)
)
:effect
(and
(Flag210-)
(Flag210prime-)
)

)
(:action Flag210Action-66
:parameters ()
:precondition
(and
(Flag207-)
(Flag207prime-)
)
:effect
(and
(Flag210-)
(Flag210prime-)
)

)
(:action Flag210Action-67
:parameters ()
:precondition
(and
(Flag179-)
(Flag179prime-)
)
:effect
(and
(Flag210-)
(Flag210prime-)
)

)
(:action Flag210Action-68
:parameters ()
:precondition
(and
(Flag166-)
(Flag166prime-)
)
:effect
(and
(Flag210-)
(Flag210prime-)
)

)
(:action Flag210Action-69
:parameters ()
:precondition
(and
(Flag108-)
(Flag108prime-)
)
:effect
(and
(Flag210-)
(Flag210prime-)
)

)
(:action Flag210Action-70
:parameters ()
:precondition
(and
(Flag131-)
(Flag131prime-)
)
:effect
(and
(Flag210-)
(Flag210prime-)
)

)
(:action Flag210Action-71
:parameters ()
:precondition
(and
(Flag200-)
(Flag200prime-)
)
:effect
(and
(Flag210-)
(Flag210prime-)
)

)
(:action Flag210Action-72
:parameters ()
:precondition
(and
(Flag51-)
(Flag51prime-)
)
:effect
(and
(Flag210-)
(Flag210prime-)
)

)
(:action Flag210Action-73
:parameters ()
:precondition
(and
(Flag208-)
(Flag208prime-)
)
:effect
(and
(Flag210-)
(Flag210prime-)
)

)
(:action Flag210Action-74
:parameters ()
:precondition
(and
(Flag43-)
(Flag43prime-)
)
:effect
(and
(Flag210-)
(Flag210prime-)
)

)
(:action Flag210Action-75
:parameters ()
:precondition
(and
(Flag190-)
(Flag190prime-)
)
:effect
(and
(Flag210-)
(Flag210prime-)
)

)
(:action Flag210Action-76
:parameters ()
:precondition
(and
(Flag191-)
(Flag191prime-)
)
:effect
(and
(Flag210-)
(Flag210prime-)
)

)
(:action Flag210Action-77
:parameters ()
:precondition
(and
(Flag167-)
(Flag167prime-)
)
:effect
(and
(Flag210-)
(Flag210prime-)
)

)
(:action Flag210Action-78
:parameters ()
:precondition
(and
(Flag124-)
(Flag124prime-)
)
:effect
(and
(Flag210-)
(Flag210prime-)
)

)
(:action Flag210Action-79
:parameters ()
:precondition
(and
(Flag70-)
(Flag70prime-)
)
:effect
(and
(Flag210-)
(Flag210prime-)
)

)
(:action Flag210Action-80
:parameters ()
:precondition
(and
(Flag209-)
(Flag209prime-)
)
:effect
(and
(Flag210-)
(Flag210prime-)
)

)
(:action Flag210Action-81
:parameters ()
:precondition
(and
(Flag61-)
(Flag61prime-)
)
:effect
(and
(Flag210-)
(Flag210prime-)
)

)
(:action Flag210Action-82
:parameters ()
:precondition
(and
(Flag168-)
(Flag168prime-)
)
:effect
(and
(Flag210-)
(Flag210prime-)
)

)
(:action Flag210Action-83
:parameters ()
:precondition
(and
(Flag155-)
(Flag155prime-)
)
:effect
(and
(Flag210-)
(Flag210prime-)
)

)
(:action Flag210Action-84
:parameters ()
:precondition
(and
(Flag192-)
(Flag192prime-)
)
:effect
(and
(Flag210-)
(Flag210prime-)
)

)
(:action Flag210Action-85
:parameters ()
:precondition
(and
(Flag88-)
(Flag88prime-)
)
:effect
(and
(Flag210-)
(Flag210prime-)
)

)
(:action Flag210Action-86
:parameters ()
:precondition
(and
(Flag201-)
(Flag201prime-)
)
:effect
(and
(Flag210-)
(Flag210prime-)
)

)
(:action Flag210Action-87
:parameters ()
:precondition
(and
(Flag181-)
(Flag181prime-)
)
:effect
(and
(Flag210-)
(Flag210prime-)
)

)
(:action Prim210Action
:parameters ()
:precondition
(and
(not (Flag44-))
(Flag44prime-)
(not (Flag79-))
(Flag79prime-)
(not (Flag95-))
(Flag95prime-)
(not (Flag49-))
(Flag49prime-)
(not (Flag59-))
(Flag59prime-)
(not (Flag143-))
(Flag143prime-)
(not (Flag203-))
(Flag203prime-)
(not (Flag170-))
(Flag170prime-)
(not (Flag144-))
(Flag144prime-)
(not (Flag81-))
(Flag81prime-)
(not (Flag184-))
(Flag184prime-)
(not (Flag112-))
(Flag112prime-)
(not (Flag171-))
(Flag171prime-)
(not (Flag172-))
(Flag172prime-)
(not (Flag121-))
(Flag121prime-)
(not (Flag56-))
(Flag56prime-)
(not (Flag157-))
(Flag157prime-)
(not (Flag158-))
(Flag158prime-)
(not (Flag173-))
(Flag173prime-)
(not (Flag45-))
(Flag45prime-)
(not (Flag194-))
(Flag194prime-)
(not (Flag159-))
(Flag159prime-)
(not (Flag93-))
(Flag93prime-)
(not (Flag128-))
(Flag128prime-)
(not (Flag160-))
(Flag160prime-)
(not (Flag146-))
(Flag146prime-)
(not (Flag129-))
(Flag129prime-)
(not (Flag130-))
(Flag130prime-)
(not (Flag204-))
(Flag204prime-)
(not (Flag185-))
(Flag185prime-)
(not (Flag175-))
(Flag175prime-)
(not (Flag115-))
(Flag115prime-)
(not (Flag140-))
(Flag140prime-)
(not (Flag195-))
(Flag195prime-)
(not (Flag98-))
(Flag98prime-)
(not (Flag58-))
(Flag58prime-)
(not (Flag196-))
(Flag196prime-)
(not (Flag135-))
(Flag135prime-)
(not (Flag104-))
(Flag104prime-)
(not (Flag105-))
(Flag105prime-)
(not (Flag205-))
(Flag205prime-)
(not (Flag68-))
(Flag68prime-)
(not (Flag89-))
(Flag89prime-)
(not (Flag187-))
(Flag187prime-)
(not (Flag38-))
(Flag38prime-)
(not (Flag110-))
(Flag110prime-)
(not (Flag126-))
(Flag126prime-)
(not (Flag149-))
(Flag149prime-)
(not (Flag138-))
(Flag138prime-)
(not (Flag177-))
(Flag177prime-)
(not (Flag114-))
(Flag114prime-)
(not (Flag40-))
(Flag40prime-)
(not (Flag77-))
(Flag77prime-)
(not (Flag188-))
(Flag188prime-)
(not (Flag197-))
(Flag197prime-)
(not (Flag198-))
(Flag198prime-)
(not (Flag52-))
(Flag52prime-)
(not (Flag122-))
(Flag122prime-)
(not (Flag206-))
(Flag206prime-)
(not (Flag67-))
(Flag67prime-)
(not (Flag117-))
(Flag117prime-)
(not (Flag100-))
(Flag100prime-)
(not (Flag107-))
(Flag107prime-)
(not (Flag150-))
(Flag150prime-)
(not (Flag152-))
(Flag152prime-)
(not (Flag78-))
(Flag78prime-)
(not (Flag207-))
(Flag207prime-)
(not (Flag179-))
(Flag179prime-)
(not (Flag166-))
(Flag166prime-)
(not (Flag108-))
(Flag108prime-)
(not (Flag131-))
(Flag131prime-)
(not (Flag200-))
(Flag200prime-)
(not (Flag51-))
(Flag51prime-)
(not (Flag208-))
(Flag208prime-)
(not (Flag43-))
(Flag43prime-)
(not (Flag190-))
(Flag190prime-)
(not (Flag191-))
(Flag191prime-)
(not (Flag167-))
(Flag167prime-)
(not (Flag124-))
(Flag124prime-)
(not (Flag70-))
(Flag70prime-)
(not (Flag209-))
(Flag209prime-)
(not (Flag61-))
(Flag61prime-)
(not (Flag168-))
(Flag168prime-)
(not (Flag155-))
(Flag155prime-)
(not (Flag192-))
(Flag192prime-)
(not (Flag88-))
(Flag88prime-)
(not (Flag201-))
(Flag201prime-)
(not (Flag181-))
(Flag181prime-)
)
:effect
(and
(Flag210prime-)
(not (Flag210-))
)

)
(:action Flag218Action-0
:parameters ()
:precondition
(and
(Flag150-)
(Flag150prime-)
)
:effect
(and
(Flag218-)
(Flag218prime-)
)

)
(:action Flag218Action-1
:parameters ()
:precondition
(and
(Flag110-)
(Flag110prime-)
)
:effect
(and
(Flag218-)
(Flag218prime-)
)

)
(:action Flag218Action-2
:parameters ()
:precondition
(and
(Flag173-)
(Flag173prime-)
)
:effect
(and
(Flag218-)
(Flag218prime-)
)

)
(:action Flag218Action-3
:parameters ()
:precondition
(and
(Flag211-)
(Flag211prime-)
)
:effect
(and
(Flag218-)
(Flag218prime-)
)

)
(:action Flag218Action-4
:parameters ()
:precondition
(and
(Flag212-)
(Flag212prime-)
)
:effect
(and
(Flag218-)
(Flag218prime-)
)

)
(:action Flag218Action-5
:parameters ()
:precondition
(and
(Flag194-)
(Flag194prime-)
)
:effect
(and
(Flag218-)
(Flag218prime-)
)

)
(:action Flag218Action-6
:parameters ()
:precondition
(and
(Flag159-)
(Flag159prime-)
)
:effect
(and
(Flag218-)
(Flag218prime-)
)

)
(:action Flag218Action-7
:parameters ()
:precondition
(and
(Flag93-)
(Flag93prime-)
)
:effect
(and
(Flag218-)
(Flag218prime-)
)

)
(:action Flag218Action-8
:parameters ()
:precondition
(and
(Flag95-)
(Flag95prime-)
)
:effect
(and
(Flag218-)
(Flag218prime-)
)

)
(:action Flag218Action-9
:parameters ()
:precondition
(and
(Flag138-)
(Flag138prime-)
)
:effect
(and
(Flag218-)
(Flag218prime-)
)

)
(:action Flag218Action-10
:parameters ()
:precondition
(and
(Flag149-)
(Flag149prime-)
)
:effect
(and
(Flag218-)
(Flag218prime-)
)

)
(:action Flag218Action-11
:parameters ()
:precondition
(and
(Flag160-)
(Flag160prime-)
)
:effect
(and
(Flag218-)
(Flag218prime-)
)

)
(:action Flag218Action-12
:parameters ()
:precondition
(and
(Flag146-)
(Flag146prime-)
)
:effect
(and
(Flag218-)
(Flag218prime-)
)

)
(:action Flag218Action-13
:parameters ()
:precondition
(and
(Flag213-)
(Flag213prime-)
)
:effect
(and
(Flag218-)
(Flag218prime-)
)

)
(:action Flag218Action-14
:parameters ()
:precondition
(and
(Flag45-)
(Flag45prime-)
)
:effect
(and
(Flag218-)
(Flag218prime-)
)

)
(:action Flag218Action-15
:parameters ()
:precondition
(and
(Flag144-)
(Flag144prime-)
)
:effect
(and
(Flag218-)
(Flag218prime-)
)

)
(:action Flag218Action-16
:parameters ()
:precondition
(and
(Flag177-)
(Flag177prime-)
)
:effect
(and
(Flag218-)
(Flag218prime-)
)

)
(:action Flag218Action-17
:parameters ()
:precondition
(and
(Flag130-)
(Flag130prime-)
)
:effect
(and
(Flag218-)
(Flag218prime-)
)

)
(:action Flag218Action-18
:parameters ()
:precondition
(and
(Flag204-)
(Flag204prime-)
)
:effect
(and
(Flag218-)
(Flag218prime-)
)

)
(:action Flag218Action-19
:parameters ()
:precondition
(and
(Flag114-)
(Flag114prime-)
)
:effect
(and
(Flag218-)
(Flag218prime-)
)

)
(:action Flag218Action-20
:parameters ()
:precondition
(and
(Flag190-)
(Flag190prime-)
)
:effect
(and
(Flag218-)
(Flag218prime-)
)

)
(:action Flag218Action-21
:parameters ()
:precondition
(and
(Flag51-)
(Flag51prime-)
)
:effect
(and
(Flag218-)
(Flag218prime-)
)

)
(:action Flag218Action-22
:parameters ()
:precondition
(and
(Flag140-)
(Flag140prime-)
)
:effect
(and
(Flag218-)
(Flag218prime-)
)

)
(:action Flag218Action-23
:parameters ()
:precondition
(and
(Flag43-)
(Flag43prime-)
)
:effect
(and
(Flag218-)
(Flag218prime-)
)

)
(:action Flag218Action-24
:parameters ()
:precondition
(and
(Flag77-)
(Flag77prime-)
)
:effect
(and
(Flag218-)
(Flag218prime-)
)

)
(:action Flag218Action-25
:parameters ()
:precondition
(and
(Flag185-)
(Flag185prime-)
)
:effect
(and
(Flag218-)
(Flag218prime-)
)

)
(:action Flag218Action-26
:parameters ()
:precondition
(and
(Flag44-)
(Flag44prime-)
)
:effect
(and
(Flag218-)
(Flag218prime-)
)

)
(:action Flag218Action-27
:parameters ()
:precondition
(and
(Flag197-)
(Flag197prime-)
)
:effect
(and
(Flag218-)
(Flag218prime-)
)

)
(:action Flag218Action-28
:parameters ()
:precondition
(and
(Flag121-)
(Flag121prime-)
)
:effect
(and
(Flag218-)
(Flag218prime-)
)

)
(:action Flag218Action-29
:parameters ()
:precondition
(and
(Flag79-)
(Flag79prime-)
)
:effect
(and
(Flag218-)
(Flag218prime-)
)

)
(:action Flag218Action-30
:parameters ()
:precondition
(and
(Flag175-)
(Flag175prime-)
)
:effect
(and
(Flag218-)
(Flag218prime-)
)

)
(:action Flag218Action-31
:parameters ()
:precondition
(and
(Flag191-)
(Flag191prime-)
)
:effect
(and
(Flag218-)
(Flag218prime-)
)

)
(:action Flag218Action-32
:parameters ()
:precondition
(and
(Flag129-)
(Flag129prime-)
)
:effect
(and
(Flag218-)
(Flag218prime-)
)

)
(:action Flag218Action-33
:parameters ()
:precondition
(and
(Flag115-)
(Flag115prime-)
)
:effect
(and
(Flag218-)
(Flag218prime-)
)

)
(:action Flag218Action-34
:parameters ()
:precondition
(and
(Flag81-)
(Flag81prime-)
)
:effect
(and
(Flag218-)
(Flag218prime-)
)

)
(:action Flag218Action-35
:parameters ()
:precondition
(and
(Flag49-)
(Flag49prime-)
)
:effect
(and
(Flag218-)
(Flag218prime-)
)

)
(:action Flag218Action-36
:parameters ()
:precondition
(and
(Flag59-)
(Flag59prime-)
)
:effect
(and
(Flag218-)
(Flag218prime-)
)

)
(:action Flag218Action-37
:parameters ()
:precondition
(and
(Flag214-)
(Flag214prime-)
)
:effect
(and
(Flag218-)
(Flag218prime-)
)

)
(:action Flag218Action-38
:parameters ()
:precondition
(and
(Flag126-)
(Flag126prime-)
)
:effect
(and
(Flag218-)
(Flag218prime-)
)

)
(:action Flag218Action-39
:parameters ()
:precondition
(and
(Flag143-)
(Flag143prime-)
)
:effect
(and
(Flag218-)
(Flag218prime-)
)

)
(:action Flag218Action-40
:parameters ()
:precondition
(and
(Flag203-)
(Flag203prime-)
)
:effect
(and
(Flag218-)
(Flag218prime-)
)

)
(:action Flag218Action-41
:parameters ()
:precondition
(and
(Flag38-)
(Flag38prime-)
)
:effect
(and
(Flag218-)
(Flag218prime-)
)

)
(:action Flag218Action-42
:parameters ()
:precondition
(and
(Flag170-)
(Flag170prime-)
)
:effect
(and
(Flag218-)
(Flag218prime-)
)

)
(:action Flag218Action-43
:parameters ()
:precondition
(and
(Flag70-)
(Flag70prime-)
)
:effect
(and
(Flag218-)
(Flag218prime-)
)

)
(:action Flag218Action-44
:parameters ()
:precondition
(and
(Flag195-)
(Flag195prime-)
)
:effect
(and
(Flag218-)
(Flag218prime-)
)

)
(:action Flag218Action-45
:parameters ()
:precondition
(and
(Flag98-)
(Flag98prime-)
)
:effect
(and
(Flag218-)
(Flag218prime-)
)

)
(:action Flag218Action-46
:parameters ()
:precondition
(and
(Flag215-)
(Flag215prime-)
)
:effect
(and
(Flag218-)
(Flag218prime-)
)

)
(:action Flag218Action-47
:parameters ()
:precondition
(and
(Flag56-)
(Flag56prime-)
)
:effect
(and
(Flag218-)
(Flag218prime-)
)

)
(:action Flag218Action-48
:parameters ()
:precondition
(and
(Flag100-)
(Flag100prime-)
)
:effect
(and
(Flag218-)
(Flag218prime-)
)

)
(:action Flag218Action-49
:parameters ()
:precondition
(and
(Flag216-)
(Flag216prime-)
)
:effect
(and
(Flag218-)
(Flag218prime-)
)

)
(:action Flag218Action-50
:parameters ()
:precondition
(and
(Flag58-)
(Flag58prime-)
)
:effect
(and
(Flag218-)
(Flag218prime-)
)

)
(:action Flag218Action-51
:parameters ()
:precondition
(and
(Flag196-)
(Flag196prime-)
)
:effect
(and
(Flag218-)
(Flag218prime-)
)

)
(:action Flag218Action-52
:parameters ()
:precondition
(and
(Flag108-)
(Flag108prime-)
)
:effect
(and
(Flag218-)
(Flag218prime-)
)

)
(:action Flag218Action-53
:parameters ()
:precondition
(and
(Flag184-)
(Flag184prime-)
)
:effect
(and
(Flag218-)
(Flag218prime-)
)

)
(:action Flag218Action-54
:parameters ()
:precondition
(and
(Flag188-)
(Flag188prime-)
)
:effect
(and
(Flag218-)
(Flag218prime-)
)

)
(:action Flag218Action-55
:parameters ()
:precondition
(and
(Flag135-)
(Flag135prime-)
)
:effect
(and
(Flag218-)
(Flag218prime-)
)

)
(:action Flag218Action-56
:parameters ()
:precondition
(and
(Flag168-)
(Flag168prime-)
)
:effect
(and
(Flag218-)
(Flag218prime-)
)

)
(:action Flag218Action-57
:parameters ()
:precondition
(and
(Flag112-)
(Flag112prime-)
)
:effect
(and
(Flag218-)
(Flag218prime-)
)

)
(:action Flag218Action-58
:parameters ()
:precondition
(and
(Flag217-)
(Flag217prime-)
)
:effect
(and
(Flag218-)
(Flag218prime-)
)

)
(:action Flag218Action-59
:parameters ()
:precondition
(and
(Flag155-)
(Flag155prime-)
)
:effect
(and
(Flag218-)
(Flag218prime-)
)

)
(:action Flag218Action-60
:parameters ()
:precondition
(and
(Flag198-)
(Flag198prime-)
)
:effect
(and
(Flag218-)
(Flag218prime-)
)

)
(:action Flag218Action-61
:parameters ()
:precondition
(and
(Flag131-)
(Flag131prime-)
)
:effect
(and
(Flag218-)
(Flag218prime-)
)

)
(:action Flag218Action-62
:parameters ()
:precondition
(and
(Flag104-)
(Flag104prime-)
)
:effect
(and
(Flag218-)
(Flag218prime-)
)

)
(:action Flag218Action-63
:parameters ()
:precondition
(and
(Flag171-)
(Flag171prime-)
)
:effect
(and
(Flag218-)
(Flag218prime-)
)

)
(:action Flag218Action-64
:parameters ()
:precondition
(and
(Flag117-)
(Flag117prime-)
)
:effect
(and
(Flag218-)
(Flag218prime-)
)

)
(:action Flag218Action-65
:parameters ()
:precondition
(and
(Flag67-)
(Flag67prime-)
)
:effect
(and
(Flag218-)
(Flag218prime-)
)

)
(:action Flag218Action-66
:parameters ()
:precondition
(and
(Flag205-)
(Flag205prime-)
)
:effect
(and
(Flag218-)
(Flag218prime-)
)

)
(:action Flag218Action-67
:parameters ()
:precondition
(and
(Flag88-)
(Flag88prime-)
)
:effect
(and
(Flag218-)
(Flag218prime-)
)

)
(:action Flag218Action-68
:parameters ()
:precondition
(and
(Flag68-)
(Flag68prime-)
)
:effect
(and
(Flag218-)
(Flag218prime-)
)

)
(:action Flag218Action-69
:parameters ()
:precondition
(and
(Flag201-)
(Flag201prime-)
)
:effect
(and
(Flag218-)
(Flag218prime-)
)

)
(:action Flag218Action-70
:parameters ()
:precondition
(and
(Flag181-)
(Flag181prime-)
)
:effect
(and
(Flag218-)
(Flag218prime-)
)

)
(:action Flag218Action-71
:parameters ()
:precondition
(and
(Flag89-)
(Flag89prime-)
)
:effect
(and
(Flag218-)
(Flag218prime-)
)

)
(:action Flag218Action-72
:parameters ()
:precondition
(and
(Flag52-)
(Flag52prime-)
)
:effect
(and
(Flag218-)
(Flag218prime-)
)

)
(:action Flag218Action-73
:parameters ()
:precondition
(and
(Flag209-)
(Flag209prime-)
)
:effect
(and
(Flag218-)
(Flag218prime-)
)

)
(:action Flag218Action-74
:parameters ()
:precondition
(and
(Flag78-)
(Flag78prime-)
)
:effect
(and
(Flag218-)
(Flag218prime-)
)

)
(:action Flag218Action-75
:parameters ()
:precondition
(and
(Flag207-)
(Flag207prime-)
)
:effect
(and
(Flag218-)
(Flag218prime-)
)

)
(:action Flag218Action-76
:parameters ()
:precondition
(and
(Flag179-)
(Flag179prime-)
)
:effect
(and
(Flag218-)
(Flag218prime-)
)

)
(:action Flag218Action-77
:parameters ()
:precondition
(and
(Flag187-)
(Flag187prime-)
)
:effect
(and
(Flag218-)
(Flag218prime-)
)

)
(:action Flag218Action-78
:parameters ()
:precondition
(and
(Flag166-)
(Flag166prime-)
)
:effect
(and
(Flag218-)
(Flag218prime-)
)

)
(:action Flag218Action-79
:parameters ()
:precondition
(and
(Flag158-)
(Flag158prime-)
)
:effect
(and
(Flag218-)
(Flag218prime-)
)

)
(:action Flag218Action-80
:parameters ()
:precondition
(and
(Flag124-)
(Flag124prime-)
)
:effect
(and
(Flag218-)
(Flag218prime-)
)

)
(:action Prim218Action
:parameters ()
:precondition
(and
(not (Flag150-))
(Flag150prime-)
(not (Flag110-))
(Flag110prime-)
(not (Flag173-))
(Flag173prime-)
(not (Flag211-))
(Flag211prime-)
(not (Flag212-))
(Flag212prime-)
(not (Flag194-))
(Flag194prime-)
(not (Flag159-))
(Flag159prime-)
(not (Flag93-))
(Flag93prime-)
(not (Flag95-))
(Flag95prime-)
(not (Flag138-))
(Flag138prime-)
(not (Flag149-))
(Flag149prime-)
(not (Flag160-))
(Flag160prime-)
(not (Flag146-))
(Flag146prime-)
(not (Flag213-))
(Flag213prime-)
(not (Flag45-))
(Flag45prime-)
(not (Flag144-))
(Flag144prime-)
(not (Flag177-))
(Flag177prime-)
(not (Flag130-))
(Flag130prime-)
(not (Flag204-))
(Flag204prime-)
(not (Flag114-))
(Flag114prime-)
(not (Flag190-))
(Flag190prime-)
(not (Flag51-))
(Flag51prime-)
(not (Flag140-))
(Flag140prime-)
(not (Flag43-))
(Flag43prime-)
(not (Flag77-))
(Flag77prime-)
(not (Flag185-))
(Flag185prime-)
(not (Flag44-))
(Flag44prime-)
(not (Flag197-))
(Flag197prime-)
(not (Flag121-))
(Flag121prime-)
(not (Flag79-))
(Flag79prime-)
(not (Flag175-))
(Flag175prime-)
(not (Flag191-))
(Flag191prime-)
(not (Flag129-))
(Flag129prime-)
(not (Flag115-))
(Flag115prime-)
(not (Flag81-))
(Flag81prime-)
(not (Flag49-))
(Flag49prime-)
(not (Flag59-))
(Flag59prime-)
(not (Flag214-))
(Flag214prime-)
(not (Flag126-))
(Flag126prime-)
(not (Flag143-))
(Flag143prime-)
(not (Flag203-))
(Flag203prime-)
(not (Flag38-))
(Flag38prime-)
(not (Flag170-))
(Flag170prime-)
(not (Flag70-))
(Flag70prime-)
(not (Flag195-))
(Flag195prime-)
(not (Flag98-))
(Flag98prime-)
(not (Flag215-))
(Flag215prime-)
(not (Flag56-))
(Flag56prime-)
(not (Flag100-))
(Flag100prime-)
(not (Flag216-))
(Flag216prime-)
(not (Flag58-))
(Flag58prime-)
(not (Flag196-))
(Flag196prime-)
(not (Flag108-))
(Flag108prime-)
(not (Flag184-))
(Flag184prime-)
(not (Flag188-))
(Flag188prime-)
(not (Flag135-))
(Flag135prime-)
(not (Flag168-))
(Flag168prime-)
(not (Flag112-))
(Flag112prime-)
(not (Flag217-))
(Flag217prime-)
(not (Flag155-))
(Flag155prime-)
(not (Flag198-))
(Flag198prime-)
(not (Flag131-))
(Flag131prime-)
(not (Flag104-))
(Flag104prime-)
(not (Flag171-))
(Flag171prime-)
(not (Flag117-))
(Flag117prime-)
(not (Flag67-))
(Flag67prime-)
(not (Flag205-))
(Flag205prime-)
(not (Flag88-))
(Flag88prime-)
(not (Flag68-))
(Flag68prime-)
(not (Flag201-))
(Flag201prime-)
(not (Flag181-))
(Flag181prime-)
(not (Flag89-))
(Flag89prime-)
(not (Flag52-))
(Flag52prime-)
(not (Flag209-))
(Flag209prime-)
(not (Flag78-))
(Flag78prime-)
(not (Flag207-))
(Flag207prime-)
(not (Flag179-))
(Flag179prime-)
(not (Flag187-))
(Flag187prime-)
(not (Flag166-))
(Flag166prime-)
(not (Flag158-))
(Flag158prime-)
(not (Flag124-))
(Flag124prime-)
)
:effect
(and
(Flag218prime-)
(not (Flag218-))
)

)
(:action Flag225Action-0
:parameters ()
:precondition
(and
(Flag38-)
(Flag38prime-)
)
:effect
(and
(Flag225-)
(Flag225prime-)
)

)
(:action Flag225Action-1
:parameters ()
:precondition
(and
(Flag110-)
(Flag110prime-)
)
:effect
(and
(Flag225-)
(Flag225prime-)
)

)
(:action Flag225Action-2
:parameters ()
:precondition
(and
(Flag211-)
(Flag211prime-)
)
:effect
(and
(Flag225-)
(Flag225prime-)
)

)
(:action Flag225Action-3
:parameters ()
:precondition
(and
(Flag45-)
(Flag45prime-)
)
:effect
(and
(Flag225-)
(Flag225prime-)
)

)
(:action Flag225Action-4
:parameters ()
:precondition
(and
(Flag194-)
(Flag194prime-)
)
:effect
(and
(Flag225-)
(Flag225prime-)
)

)
(:action Flag225Action-5
:parameters ()
:precondition
(and
(Flag159-)
(Flag159prime-)
)
:effect
(and
(Flag225-)
(Flag225prime-)
)

)
(:action Flag225Action-6
:parameters ()
:precondition
(and
(Flag93-)
(Flag93prime-)
)
:effect
(and
(Flag225-)
(Flag225prime-)
)

)
(:action Flag225Action-7
:parameters ()
:precondition
(and
(Flag95-)
(Flag95prime-)
)
:effect
(and
(Flag225-)
(Flag225prime-)
)

)
(:action Flag225Action-8
:parameters ()
:precondition
(and
(Flag138-)
(Flag138prime-)
)
:effect
(and
(Flag225-)
(Flag225prime-)
)

)
(:action Flag225Action-9
:parameters ()
:precondition
(and
(Flag149-)
(Flag149prime-)
)
:effect
(and
(Flag225-)
(Flag225prime-)
)

)
(:action Flag225Action-10
:parameters ()
:precondition
(and
(Flag160-)
(Flag160prime-)
)
:effect
(and
(Flag225-)
(Flag225prime-)
)

)
(:action Flag225Action-11
:parameters ()
:precondition
(and
(Flag146-)
(Flag146prime-)
)
:effect
(and
(Flag225-)
(Flag225prime-)
)

)
(:action Flag225Action-12
:parameters ()
:precondition
(and
(Flag144-)
(Flag144prime-)
)
:effect
(and
(Flag225-)
(Flag225prime-)
)

)
(:action Flag225Action-13
:parameters ()
:precondition
(and
(Flag177-)
(Flag177prime-)
)
:effect
(and
(Flag225-)
(Flag225prime-)
)

)
(:action Flag225Action-14
:parameters ()
:precondition
(and
(Flag130-)
(Flag130prime-)
)
:effect
(and
(Flag225-)
(Flag225prime-)
)

)
(:action Flag225Action-15
:parameters ()
:precondition
(and
(Flag204-)
(Flag204prime-)
)
:effect
(and
(Flag225-)
(Flag225prime-)
)

)
(:action Flag225Action-16
:parameters ()
:precondition
(and
(Flag114-)
(Flag114prime-)
)
:effect
(and
(Flag225-)
(Flag225prime-)
)

)
(:action Flag225Action-17
:parameters ()
:precondition
(and
(Flag219-)
(Flag219prime-)
)
:effect
(and
(Flag225-)
(Flag225prime-)
)

)
(:action Flag225Action-18
:parameters ()
:precondition
(and
(Flag190-)
(Flag190prime-)
)
:effect
(and
(Flag225-)
(Flag225prime-)
)

)
(:action Flag225Action-19
:parameters ()
:precondition
(and
(Flag51-)
(Flag51prime-)
)
:effect
(and
(Flag225-)
(Flag225prime-)
)

)
(:action Flag225Action-20
:parameters ()
:precondition
(and
(Flag43-)
(Flag43prime-)
)
:effect
(and
(Flag225-)
(Flag225prime-)
)

)
(:action Flag225Action-21
:parameters ()
:precondition
(and
(Flag77-)
(Flag77prime-)
)
:effect
(and
(Flag225-)
(Flag225prime-)
)

)
(:action Flag225Action-22
:parameters ()
:precondition
(and
(Flag185-)
(Flag185prime-)
)
:effect
(and
(Flag225-)
(Flag225prime-)
)

)
(:action Flag225Action-23
:parameters ()
:precondition
(and
(Flag220-)
(Flag220prime-)
)
:effect
(and
(Flag225-)
(Flag225prime-)
)

)
(:action Flag225Action-24
:parameters ()
:precondition
(and
(Flag44-)
(Flag44prime-)
)
:effect
(and
(Flag225-)
(Flag225prime-)
)

)
(:action Flag225Action-25
:parameters ()
:precondition
(and
(Flag197-)
(Flag197prime-)
)
:effect
(and
(Flag225-)
(Flag225prime-)
)

)
(:action Flag225Action-26
:parameters ()
:precondition
(and
(Flag79-)
(Flag79prime-)
)
:effect
(and
(Flag225-)
(Flag225prime-)
)

)
(:action Flag225Action-27
:parameters ()
:precondition
(and
(Flag175-)
(Flag175prime-)
)
:effect
(and
(Flag225-)
(Flag225prime-)
)

)
(:action Flag225Action-28
:parameters ()
:precondition
(and
(Flag191-)
(Flag191prime-)
)
:effect
(and
(Flag225-)
(Flag225prime-)
)

)
(:action Flag225Action-29
:parameters ()
:precondition
(and
(Flag56-)
(Flag56prime-)
)
:effect
(and
(Flag225-)
(Flag225prime-)
)

)
(:action Flag225Action-30
:parameters ()
:precondition
(and
(Flag221-)
(Flag221prime-)
)
:effect
(and
(Flag225-)
(Flag225prime-)
)

)
(:action Flag225Action-31
:parameters ()
:precondition
(and
(Flag49-)
(Flag49prime-)
)
:effect
(and
(Flag225-)
(Flag225prime-)
)

)
(:action Flag225Action-32
:parameters ()
:precondition
(and
(Flag59-)
(Flag59prime-)
)
:effect
(and
(Flag225-)
(Flag225prime-)
)

)
(:action Flag225Action-33
:parameters ()
:precondition
(and
(Flag214-)
(Flag214prime-)
)
:effect
(and
(Flag225-)
(Flag225prime-)
)

)
(:action Flag225Action-34
:parameters ()
:precondition
(and
(Flag126-)
(Flag126prime-)
)
:effect
(and
(Flag225-)
(Flag225prime-)
)

)
(:action Flag225Action-35
:parameters ()
:precondition
(and
(Flag143-)
(Flag143prime-)
)
:effect
(and
(Flag225-)
(Flag225prime-)
)

)
(:action Flag225Action-36
:parameters ()
:precondition
(and
(Flag203-)
(Flag203prime-)
)
:effect
(and
(Flag225-)
(Flag225prime-)
)

)
(:action Flag225Action-37
:parameters ()
:precondition
(and
(Flag222-)
(Flag222prime-)
)
:effect
(and
(Flag225-)
(Flag225prime-)
)

)
(:action Flag225Action-38
:parameters ()
:precondition
(and
(Flag170-)
(Flag170prime-)
)
:effect
(and
(Flag225-)
(Flag225prime-)
)

)
(:action Flag225Action-39
:parameters ()
:precondition
(and
(Flag70-)
(Flag70prime-)
)
:effect
(and
(Flag225-)
(Flag225prime-)
)

)
(:action Flag225Action-40
:parameters ()
:precondition
(and
(Flag195-)
(Flag195prime-)
)
:effect
(and
(Flag225-)
(Flag225prime-)
)

)
(:action Flag225Action-41
:parameters ()
:precondition
(and
(Flag216-)
(Flag216prime-)
)
:effect
(and
(Flag225-)
(Flag225prime-)
)

)
(:action Flag225Action-42
:parameters ()
:precondition
(and
(Flag81-)
(Flag81prime-)
)
:effect
(and
(Flag225-)
(Flag225prime-)
)

)
(:action Flag225Action-43
:parameters ()
:precondition
(and
(Flag140-)
(Flag140prime-)
)
:effect
(and
(Flag225-)
(Flag225prime-)
)

)
(:action Flag225Action-44
:parameters ()
:precondition
(and
(Flag209-)
(Flag209prime-)
)
:effect
(and
(Flag225-)
(Flag225prime-)
)

)
(:action Flag225Action-45
:parameters ()
:precondition
(and
(Flag58-)
(Flag58prime-)
)
:effect
(and
(Flag225-)
(Flag225prime-)
)

)
(:action Flag225Action-46
:parameters ()
:precondition
(and
(Flag223-)
(Flag223prime-)
)
:effect
(and
(Flag225-)
(Flag225prime-)
)

)
(:action Flag225Action-47
:parameters ()
:precondition
(and
(Flag196-)
(Flag196prime-)
)
:effect
(and
(Flag225-)
(Flag225prime-)
)

)
(:action Flag225Action-48
:parameters ()
:precondition
(and
(Flag108-)
(Flag108prime-)
)
:effect
(and
(Flag225-)
(Flag225prime-)
)

)
(:action Flag225Action-49
:parameters ()
:precondition
(and
(Flag135-)
(Flag135prime-)
)
:effect
(and
(Flag225-)
(Flag225prime-)
)

)
(:action Flag225Action-50
:parameters ()
:precondition
(and
(Flag168-)
(Flag168prime-)
)
:effect
(and
(Flag225-)
(Flag225prime-)
)

)
(:action Flag225Action-51
:parameters ()
:precondition
(and
(Flag112-)
(Flag112prime-)
)
:effect
(and
(Flag225-)
(Flag225prime-)
)

)
(:action Flag225Action-52
:parameters ()
:precondition
(and
(Flag217-)
(Flag217prime-)
)
:effect
(and
(Flag225-)
(Flag225prime-)
)

)
(:action Flag225Action-53
:parameters ()
:precondition
(and
(Flag188-)
(Flag188prime-)
)
:effect
(and
(Flag225-)
(Flag225prime-)
)

)
(:action Flag225Action-54
:parameters ()
:precondition
(and
(Flag198-)
(Flag198prime-)
)
:effect
(and
(Flag225-)
(Flag225prime-)
)

)
(:action Flag225Action-55
:parameters ()
:precondition
(and
(Flag131-)
(Flag131prime-)
)
:effect
(and
(Flag225-)
(Flag225prime-)
)

)
(:action Flag225Action-56
:parameters ()
:precondition
(and
(Flag104-)
(Flag104prime-)
)
:effect
(and
(Flag225-)
(Flag225prime-)
)

)
(:action Flag225Action-57
:parameters ()
:precondition
(and
(Flag171-)
(Flag171prime-)
)
:effect
(and
(Flag225-)
(Flag225prime-)
)

)
(:action Flag225Action-58
:parameters ()
:precondition
(and
(Flag117-)
(Flag117prime-)
)
:effect
(and
(Flag225-)
(Flag225prime-)
)

)
(:action Flag225Action-59
:parameters ()
:precondition
(and
(Flag224-)
(Flag224prime-)
)
:effect
(and
(Flag225-)
(Flag225prime-)
)

)
(:action Flag225Action-60
:parameters ()
:precondition
(and
(Flag205-)
(Flag205prime-)
)
:effect
(and
(Flag225-)
(Flag225prime-)
)

)
(:action Flag225Action-61
:parameters ()
:precondition
(and
(Flag124-)
(Flag124prime-)
)
:effect
(and
(Flag225-)
(Flag225prime-)
)

)
(:action Flag225Action-62
:parameters ()
:precondition
(and
(Flag150-)
(Flag150prime-)
)
:effect
(and
(Flag225-)
(Flag225prime-)
)

)
(:action Flag225Action-63
:parameters ()
:precondition
(and
(Flag89-)
(Flag89prime-)
)
:effect
(and
(Flag225-)
(Flag225prime-)
)

)
(:action Flag225Action-64
:parameters ()
:precondition
(and
(Flag52-)
(Flag52prime-)
)
:effect
(and
(Flag225-)
(Flag225prime-)
)

)
(:action Flag225Action-65
:parameters ()
:precondition
(and
(Flag78-)
(Flag78prime-)
)
:effect
(and
(Flag225-)
(Flag225prime-)
)

)
(:action Flag225Action-66
:parameters ()
:precondition
(and
(Flag207-)
(Flag207prime-)
)
:effect
(and
(Flag225-)
(Flag225prime-)
)

)
(:action Flag225Action-67
:parameters ()
:precondition
(and
(Flag98-)
(Flag98prime-)
)
:effect
(and
(Flag225-)
(Flag225prime-)
)

)
(:action Flag225Action-68
:parameters ()
:precondition
(and
(Flag179-)
(Flag179prime-)
)
:effect
(and
(Flag225-)
(Flag225prime-)
)

)
(:action Flag225Action-69
:parameters ()
:precondition
(and
(Flag187-)
(Flag187prime-)
)
:effect
(and
(Flag225-)
(Flag225prime-)
)

)
(:action Flag225Action-70
:parameters ()
:precondition
(and
(Flag166-)
(Flag166prime-)
)
:effect
(and
(Flag225-)
(Flag225prime-)
)

)
(:action Flag225Action-71
:parameters ()
:precondition
(and
(Flag158-)
(Flag158prime-)
)
:effect
(and
(Flag225-)
(Flag225prime-)
)

)
(:action Prim225Action
:parameters ()
:precondition
(and
(not (Flag38-))
(Flag38prime-)
(not (Flag110-))
(Flag110prime-)
(not (Flag211-))
(Flag211prime-)
(not (Flag45-))
(Flag45prime-)
(not (Flag194-))
(Flag194prime-)
(not (Flag159-))
(Flag159prime-)
(not (Flag93-))
(Flag93prime-)
(not (Flag95-))
(Flag95prime-)
(not (Flag138-))
(Flag138prime-)
(not (Flag149-))
(Flag149prime-)
(not (Flag160-))
(Flag160prime-)
(not (Flag146-))
(Flag146prime-)
(not (Flag144-))
(Flag144prime-)
(not (Flag177-))
(Flag177prime-)
(not (Flag130-))
(Flag130prime-)
(not (Flag204-))
(Flag204prime-)
(not (Flag114-))
(Flag114prime-)
(not (Flag219-))
(Flag219prime-)
(not (Flag190-))
(Flag190prime-)
(not (Flag51-))
(Flag51prime-)
(not (Flag43-))
(Flag43prime-)
(not (Flag77-))
(Flag77prime-)
(not (Flag185-))
(Flag185prime-)
(not (Flag220-))
(Flag220prime-)
(not (Flag44-))
(Flag44prime-)
(not (Flag197-))
(Flag197prime-)
(not (Flag79-))
(Flag79prime-)
(not (Flag175-))
(Flag175prime-)
(not (Flag191-))
(Flag191prime-)
(not (Flag56-))
(Flag56prime-)
(not (Flag221-))
(Flag221prime-)
(not (Flag49-))
(Flag49prime-)
(not (Flag59-))
(Flag59prime-)
(not (Flag214-))
(Flag214prime-)
(not (Flag126-))
(Flag126prime-)
(not (Flag143-))
(Flag143prime-)
(not (Flag203-))
(Flag203prime-)
(not (Flag222-))
(Flag222prime-)
(not (Flag170-))
(Flag170prime-)
(not (Flag70-))
(Flag70prime-)
(not (Flag195-))
(Flag195prime-)
(not (Flag216-))
(Flag216prime-)
(not (Flag81-))
(Flag81prime-)
(not (Flag140-))
(Flag140prime-)
(not (Flag209-))
(Flag209prime-)
(not (Flag58-))
(Flag58prime-)
(not (Flag223-))
(Flag223prime-)
(not (Flag196-))
(Flag196prime-)
(not (Flag108-))
(Flag108prime-)
(not (Flag135-))
(Flag135prime-)
(not (Flag168-))
(Flag168prime-)
(not (Flag112-))
(Flag112prime-)
(not (Flag217-))
(Flag217prime-)
(not (Flag188-))
(Flag188prime-)
(not (Flag198-))
(Flag198prime-)
(not (Flag131-))
(Flag131prime-)
(not (Flag104-))
(Flag104prime-)
(not (Flag171-))
(Flag171prime-)
(not (Flag117-))
(Flag117prime-)
(not (Flag224-))
(Flag224prime-)
(not (Flag205-))
(Flag205prime-)
(not (Flag124-))
(Flag124prime-)
(not (Flag150-))
(Flag150prime-)
(not (Flag89-))
(Flag89prime-)
(not (Flag52-))
(Flag52prime-)
(not (Flag78-))
(Flag78prime-)
(not (Flag207-))
(Flag207prime-)
(not (Flag98-))
(Flag98prime-)
(not (Flag179-))
(Flag179prime-)
(not (Flag187-))
(Flag187prime-)
(not (Flag166-))
(Flag166prime-)
(not (Flag158-))
(Flag158prime-)
)
:effect
(and
(Flag225prime-)
(not (Flag225-))
)

)
(:action Flag230Action-0
:parameters ()
:precondition
(and
(Flag38-)
(Flag38prime-)
)
:effect
(and
(Flag230-)
(Flag230prime-)
)

)
(:action Flag230Action-1
:parameters ()
:precondition
(and
(Flag110-)
(Flag110prime-)
)
:effect
(and
(Flag230-)
(Flag230prime-)
)

)
(:action Flag230Action-2
:parameters ()
:precondition
(and
(Flag211-)
(Flag211prime-)
)
:effect
(and
(Flag230-)
(Flag230prime-)
)

)
(:action Flag230Action-3
:parameters ()
:precondition
(and
(Flag45-)
(Flag45prime-)
)
:effect
(and
(Flag230-)
(Flag230prime-)
)

)
(:action Flag230Action-4
:parameters ()
:precondition
(and
(Flag194-)
(Flag194prime-)
)
:effect
(and
(Flag230-)
(Flag230prime-)
)

)
(:action Flag230Action-5
:parameters ()
:precondition
(and
(Flag159-)
(Flag159prime-)
)
:effect
(and
(Flag230-)
(Flag230prime-)
)

)
(:action Flag230Action-6
:parameters ()
:precondition
(and
(Flag95-)
(Flag95prime-)
)
:effect
(and
(Flag230-)
(Flag230prime-)
)

)
(:action Flag230Action-7
:parameters ()
:precondition
(and
(Flag138-)
(Flag138prime-)
)
:effect
(and
(Flag230-)
(Flag230prime-)
)

)
(:action Flag230Action-8
:parameters ()
:precondition
(and
(Flag146-)
(Flag146prime-)
)
:effect
(and
(Flag230-)
(Flag230prime-)
)

)
(:action Flag230Action-9
:parameters ()
:precondition
(and
(Flag144-)
(Flag144prime-)
)
:effect
(and
(Flag230-)
(Flag230prime-)
)

)
(:action Flag230Action-10
:parameters ()
:precondition
(and
(Flag177-)
(Flag177prime-)
)
:effect
(and
(Flag230-)
(Flag230prime-)
)

)
(:action Flag230Action-11
:parameters ()
:precondition
(and
(Flag226-)
(Flag226prime-)
)
:effect
(and
(Flag230-)
(Flag230prime-)
)

)
(:action Flag230Action-12
:parameters ()
:precondition
(and
(Flag204-)
(Flag204prime-)
)
:effect
(and
(Flag230-)
(Flag230prime-)
)

)
(:action Flag230Action-13
:parameters ()
:precondition
(and
(Flag114-)
(Flag114prime-)
)
:effect
(and
(Flag230-)
(Flag230prime-)
)

)
(:action Flag230Action-14
:parameters ()
:precondition
(and
(Flag220-)
(Flag220prime-)
)
:effect
(and
(Flag230-)
(Flag230prime-)
)

)
(:action Flag230Action-15
:parameters ()
:precondition
(and
(Flag190-)
(Flag190prime-)
)
:effect
(and
(Flag230-)
(Flag230prime-)
)

)
(:action Flag230Action-16
:parameters ()
:precondition
(and
(Flag51-)
(Flag51prime-)
)
:effect
(and
(Flag230-)
(Flag230prime-)
)

)
(:action Flag230Action-17
:parameters ()
:precondition
(and
(Flag227-)
(Flag227prime-)
)
:effect
(and
(Flag230-)
(Flag230prime-)
)

)
(:action Flag230Action-18
:parameters ()
:precondition
(and
(Flag43-)
(Flag43prime-)
)
:effect
(and
(Flag230-)
(Flag230prime-)
)

)
(:action Flag230Action-19
:parameters ()
:precondition
(and
(Flag77-)
(Flag77prime-)
)
:effect
(and
(Flag230-)
(Flag230prime-)
)

)
(:action Flag230Action-20
:parameters ()
:precondition
(and
(Flag185-)
(Flag185prime-)
)
:effect
(and
(Flag230-)
(Flag230prime-)
)

)
(:action Flag230Action-21
:parameters ()
:precondition
(and
(Flag219-)
(Flag219prime-)
)
:effect
(and
(Flag230-)
(Flag230prime-)
)

)
(:action Flag230Action-22
:parameters ()
:precondition
(and
(Flag98-)
(Flag98prime-)
)
:effect
(and
(Flag230-)
(Flag230prime-)
)

)
(:action Flag230Action-23
:parameters ()
:precondition
(and
(Flag228-)
(Flag228prime-)
)
:effect
(and
(Flag230-)
(Flag230prime-)
)

)
(:action Flag230Action-24
:parameters ()
:precondition
(and
(Flag79-)
(Flag79prime-)
)
:effect
(and
(Flag230-)
(Flag230prime-)
)

)
(:action Flag230Action-25
:parameters ()
:precondition
(and
(Flag175-)
(Flag175prime-)
)
:effect
(and
(Flag230-)
(Flag230prime-)
)

)
(:action Flag230Action-26
:parameters ()
:precondition
(and
(Flag191-)
(Flag191prime-)
)
:effect
(and
(Flag230-)
(Flag230prime-)
)

)
(:action Flag230Action-27
:parameters ()
:precondition
(and
(Flag49-)
(Flag49prime-)
)
:effect
(and
(Flag230-)
(Flag230prime-)
)

)
(:action Flag230Action-28
:parameters ()
:precondition
(and
(Flag59-)
(Flag59prime-)
)
:effect
(and
(Flag230-)
(Flag230prime-)
)

)
(:action Flag230Action-29
:parameters ()
:precondition
(and
(Flag214-)
(Flag214prime-)
)
:effect
(and
(Flag230-)
(Flag230prime-)
)

)
(:action Flag230Action-30
:parameters ()
:precondition
(and
(Flag126-)
(Flag126prime-)
)
:effect
(and
(Flag230-)
(Flag230prime-)
)

)
(:action Flag230Action-31
:parameters ()
:precondition
(and
(Flag143-)
(Flag143prime-)
)
:effect
(and
(Flag230-)
(Flag230prime-)
)

)
(:action Flag230Action-32
:parameters ()
:precondition
(and
(Flag140-)
(Flag140prime-)
)
:effect
(and
(Flag230-)
(Flag230prime-)
)

)
(:action Flag230Action-33
:parameters ()
:precondition
(and
(Flag222-)
(Flag222prime-)
)
:effect
(and
(Flag230-)
(Flag230prime-)
)

)
(:action Flag230Action-34
:parameters ()
:precondition
(and
(Flag170-)
(Flag170prime-)
)
:effect
(and
(Flag230-)
(Flag230prime-)
)

)
(:action Flag230Action-35
:parameters ()
:precondition
(and
(Flag195-)
(Flag195prime-)
)
:effect
(and
(Flag230-)
(Flag230prime-)
)

)
(:action Flag230Action-36
:parameters ()
:precondition
(and
(Flag216-)
(Flag216prime-)
)
:effect
(and
(Flag230-)
(Flag230prime-)
)

)
(:action Flag230Action-37
:parameters ()
:precondition
(and
(Flag81-)
(Flag81prime-)
)
:effect
(and
(Flag230-)
(Flag230prime-)
)

)
(:action Flag230Action-38
:parameters ()
:precondition
(and
(Flag209-)
(Flag209prime-)
)
:effect
(and
(Flag230-)
(Flag230prime-)
)

)
(:action Flag230Action-39
:parameters ()
:precondition
(and
(Flag58-)
(Flag58prime-)
)
:effect
(and
(Flag230-)
(Flag230prime-)
)

)
(:action Flag230Action-40
:parameters ()
:precondition
(and
(Flag196-)
(Flag196prime-)
)
:effect
(and
(Flag230-)
(Flag230prime-)
)

)
(:action Flag230Action-41
:parameters ()
:precondition
(and
(Flag108-)
(Flag108prime-)
)
:effect
(and
(Flag230-)
(Flag230prime-)
)

)
(:action Flag230Action-42
:parameters ()
:precondition
(and
(Flag135-)
(Flag135prime-)
)
:effect
(and
(Flag230-)
(Flag230prime-)
)

)
(:action Flag230Action-43
:parameters ()
:precondition
(and
(Flag168-)
(Flag168prime-)
)
:effect
(and
(Flag230-)
(Flag230prime-)
)

)
(:action Flag230Action-44
:parameters ()
:precondition
(and
(Flag112-)
(Flag112prime-)
)
:effect
(and
(Flag230-)
(Flag230prime-)
)

)
(:action Flag230Action-45
:parameters ()
:precondition
(and
(Flag217-)
(Flag217prime-)
)
:effect
(and
(Flag230-)
(Flag230prime-)
)

)
(:action Flag230Action-46
:parameters ()
:precondition
(and
(Flag188-)
(Flag188prime-)
)
:effect
(and
(Flag230-)
(Flag230prime-)
)

)
(:action Flag230Action-47
:parameters ()
:precondition
(and
(Flag104-)
(Flag104prime-)
)
:effect
(and
(Flag230-)
(Flag230prime-)
)

)
(:action Flag230Action-48
:parameters ()
:precondition
(and
(Flag117-)
(Flag117prime-)
)
:effect
(and
(Flag230-)
(Flag230prime-)
)

)
(:action Flag230Action-49
:parameters ()
:precondition
(and
(Flag224-)
(Flag224prime-)
)
:effect
(and
(Flag230-)
(Flag230prime-)
)

)
(:action Flag230Action-50
:parameters ()
:precondition
(and
(Flag205-)
(Flag205prime-)
)
:effect
(and
(Flag230-)
(Flag230prime-)
)

)
(:action Flag230Action-51
:parameters ()
:precondition
(and
(Flag198-)
(Flag198prime-)
)
:effect
(and
(Flag230-)
(Flag230prime-)
)

)
(:action Flag230Action-52
:parameters ()
:precondition
(and
(Flag150-)
(Flag150prime-)
)
:effect
(and
(Flag230-)
(Flag230prime-)
)

)
(:action Flag230Action-53
:parameters ()
:precondition
(and
(Flag52-)
(Flag52prime-)
)
:effect
(and
(Flag230-)
(Flag230prime-)
)

)
(:action Flag230Action-54
:parameters ()
:precondition
(and
(Flag78-)
(Flag78prime-)
)
:effect
(and
(Flag230-)
(Flag230prime-)
)

)
(:action Flag230Action-55
:parameters ()
:precondition
(and
(Flag207-)
(Flag207prime-)
)
:effect
(and
(Flag230-)
(Flag230prime-)
)

)
(:action Flag230Action-56
:parameters ()
:precondition
(and
(Flag229-)
(Flag229prime-)
)
:effect
(and
(Flag230-)
(Flag230prime-)
)

)
(:action Flag230Action-57
:parameters ()
:precondition
(and
(Flag179-)
(Flag179prime-)
)
:effect
(and
(Flag230-)
(Flag230prime-)
)

)
(:action Flag230Action-58
:parameters ()
:precondition
(and
(Flag70-)
(Flag70prime-)
)
:effect
(and
(Flag230-)
(Flag230prime-)
)

)
(:action Flag230Action-59
:parameters ()
:precondition
(and
(Flag166-)
(Flag166prime-)
)
:effect
(and
(Flag230-)
(Flag230prime-)
)

)
(:action Flag230Action-60
:parameters ()
:precondition
(and
(Flag158-)
(Flag158prime-)
)
:effect
(and
(Flag230-)
(Flag230prime-)
)

)
(:action Flag230Action-61
:parameters ()
:precondition
(and
(Flag124-)
(Flag124prime-)
)
:effect
(and
(Flag230-)
(Flag230prime-)
)

)
(:action Prim230Action
:parameters ()
:precondition
(and
(not (Flag38-))
(Flag38prime-)
(not (Flag110-))
(Flag110prime-)
(not (Flag211-))
(Flag211prime-)
(not (Flag45-))
(Flag45prime-)
(not (Flag194-))
(Flag194prime-)
(not (Flag159-))
(Flag159prime-)
(not (Flag95-))
(Flag95prime-)
(not (Flag138-))
(Flag138prime-)
(not (Flag146-))
(Flag146prime-)
(not (Flag144-))
(Flag144prime-)
(not (Flag177-))
(Flag177prime-)
(not (Flag226-))
(Flag226prime-)
(not (Flag204-))
(Flag204prime-)
(not (Flag114-))
(Flag114prime-)
(not (Flag220-))
(Flag220prime-)
(not (Flag190-))
(Flag190prime-)
(not (Flag51-))
(Flag51prime-)
(not (Flag227-))
(Flag227prime-)
(not (Flag43-))
(Flag43prime-)
(not (Flag77-))
(Flag77prime-)
(not (Flag185-))
(Flag185prime-)
(not (Flag219-))
(Flag219prime-)
(not (Flag98-))
(Flag98prime-)
(not (Flag228-))
(Flag228prime-)
(not (Flag79-))
(Flag79prime-)
(not (Flag175-))
(Flag175prime-)
(not (Flag191-))
(Flag191prime-)
(not (Flag49-))
(Flag49prime-)
(not (Flag59-))
(Flag59prime-)
(not (Flag214-))
(Flag214prime-)
(not (Flag126-))
(Flag126prime-)
(not (Flag143-))
(Flag143prime-)
(not (Flag140-))
(Flag140prime-)
(not (Flag222-))
(Flag222prime-)
(not (Flag170-))
(Flag170prime-)
(not (Flag195-))
(Flag195prime-)
(not (Flag216-))
(Flag216prime-)
(not (Flag81-))
(Flag81prime-)
(not (Flag209-))
(Flag209prime-)
(not (Flag58-))
(Flag58prime-)
(not (Flag196-))
(Flag196prime-)
(not (Flag108-))
(Flag108prime-)
(not (Flag135-))
(Flag135prime-)
(not (Flag168-))
(Flag168prime-)
(not (Flag112-))
(Flag112prime-)
(not (Flag217-))
(Flag217prime-)
(not (Flag188-))
(Flag188prime-)
(not (Flag104-))
(Flag104prime-)
(not (Flag117-))
(Flag117prime-)
(not (Flag224-))
(Flag224prime-)
(not (Flag205-))
(Flag205prime-)
(not (Flag198-))
(Flag198prime-)
(not (Flag150-))
(Flag150prime-)
(not (Flag52-))
(Flag52prime-)
(not (Flag78-))
(Flag78prime-)
(not (Flag207-))
(Flag207prime-)
(not (Flag229-))
(Flag229prime-)
(not (Flag179-))
(Flag179prime-)
(not (Flag70-))
(Flag70prime-)
(not (Flag166-))
(Flag166prime-)
(not (Flag158-))
(Flag158prime-)
(not (Flag124-))
(Flag124prime-)
)
:effect
(and
(Flag230prime-)
(not (Flag230-))
)

)
(:action Flag234Action-0
:parameters ()
:precondition
(and
(Flag38-)
(Flag38prime-)
)
:effect
(and
(Flag234-)
(Flag234prime-)
)

)
(:action Flag234Action-1
:parameters ()
:precondition
(and
(Flag110-)
(Flag110prime-)
)
:effect
(and
(Flag234-)
(Flag234prime-)
)

)
(:action Flag234Action-2
:parameters ()
:precondition
(and
(Flag45-)
(Flag45prime-)
)
:effect
(and
(Flag234-)
(Flag234prime-)
)

)
(:action Flag234Action-3
:parameters ()
:precondition
(and
(Flag194-)
(Flag194prime-)
)
:effect
(and
(Flag234-)
(Flag234prime-)
)

)
(:action Flag234Action-4
:parameters ()
:precondition
(and
(Flag159-)
(Flag159prime-)
)
:effect
(and
(Flag234-)
(Flag234prime-)
)

)
(:action Flag234Action-5
:parameters ()
:precondition
(and
(Flag146-)
(Flag146prime-)
)
:effect
(and
(Flag234-)
(Flag234prime-)
)

)
(:action Flag234Action-6
:parameters ()
:precondition
(and
(Flag144-)
(Flag144prime-)
)
:effect
(and
(Flag234-)
(Flag234prime-)
)

)
(:action Flag234Action-7
:parameters ()
:precondition
(and
(Flag177-)
(Flag177prime-)
)
:effect
(and
(Flag234-)
(Flag234prime-)
)

)
(:action Flag234Action-8
:parameters ()
:precondition
(and
(Flag204-)
(Flag204prime-)
)
:effect
(and
(Flag234-)
(Flag234prime-)
)

)
(:action Flag234Action-9
:parameters ()
:precondition
(and
(Flag114-)
(Flag114prime-)
)
:effect
(and
(Flag234-)
(Flag234prime-)
)

)
(:action Flag234Action-10
:parameters ()
:precondition
(and
(Flag220-)
(Flag220prime-)
)
:effect
(and
(Flag234-)
(Flag234prime-)
)

)
(:action Flag234Action-11
:parameters ()
:precondition
(and
(Flag190-)
(Flag190prime-)
)
:effect
(and
(Flag234-)
(Flag234prime-)
)

)
(:action Flag234Action-12
:parameters ()
:precondition
(and
(Flag51-)
(Flag51prime-)
)
:effect
(and
(Flag234-)
(Flag234prime-)
)

)
(:action Flag234Action-13
:parameters ()
:precondition
(and
(Flag227-)
(Flag227prime-)
)
:effect
(and
(Flag234-)
(Flag234prime-)
)

)
(:action Flag234Action-14
:parameters ()
:precondition
(and
(Flag43-)
(Flag43prime-)
)
:effect
(and
(Flag234-)
(Flag234prime-)
)

)
(:action Flag234Action-15
:parameters ()
:precondition
(and
(Flag77-)
(Flag77prime-)
)
:effect
(and
(Flag234-)
(Flag234prime-)
)

)
(:action Flag234Action-16
:parameters ()
:precondition
(and
(Flag98-)
(Flag98prime-)
)
:effect
(and
(Flag234-)
(Flag234prime-)
)

)
(:action Flag234Action-17
:parameters ()
:precondition
(and
(Flag228-)
(Flag228prime-)
)
:effect
(and
(Flag234-)
(Flag234prime-)
)

)
(:action Flag234Action-18
:parameters ()
:precondition
(and
(Flag79-)
(Flag79prime-)
)
:effect
(and
(Flag234-)
(Flag234prime-)
)

)
(:action Flag234Action-19
:parameters ()
:precondition
(and
(Flag95-)
(Flag95prime-)
)
:effect
(and
(Flag234-)
(Flag234prime-)
)

)
(:action Flag234Action-20
:parameters ()
:precondition
(and
(Flag191-)
(Flag191prime-)
)
:effect
(and
(Flag234-)
(Flag234prime-)
)

)
(:action Flag234Action-21
:parameters ()
:precondition
(and
(Flag49-)
(Flag49prime-)
)
:effect
(and
(Flag234-)
(Flag234prime-)
)

)
(:action Flag234Action-22
:parameters ()
:precondition
(and
(Flag59-)
(Flag59prime-)
)
:effect
(and
(Flag234-)
(Flag234prime-)
)

)
(:action Flag234Action-23
:parameters ()
:precondition
(and
(Flag214-)
(Flag214prime-)
)
:effect
(and
(Flag234-)
(Flag234prime-)
)

)
(:action Flag234Action-24
:parameters ()
:precondition
(and
(Flag231-)
(Flag231prime-)
)
:effect
(and
(Flag234-)
(Flag234prime-)
)

)
(:action Flag234Action-25
:parameters ()
:precondition
(and
(Flag140-)
(Flag140prime-)
)
:effect
(and
(Flag234-)
(Flag234prime-)
)

)
(:action Flag234Action-26
:parameters ()
:precondition
(and
(Flag222-)
(Flag222prime-)
)
:effect
(and
(Flag234-)
(Flag234prime-)
)

)
(:action Flag234Action-27
:parameters ()
:precondition
(and
(Flag170-)
(Flag170prime-)
)
:effect
(and
(Flag234-)
(Flag234prime-)
)

)
(:action Flag234Action-28
:parameters ()
:precondition
(and
(Flag232-)
(Flag232prime-)
)
:effect
(and
(Flag234-)
(Flag234prime-)
)

)
(:action Flag234Action-29
:parameters ()
:precondition
(and
(Flag195-)
(Flag195prime-)
)
:effect
(and
(Flag234-)
(Flag234prime-)
)

)
(:action Flag234Action-30
:parameters ()
:precondition
(and
(Flag216-)
(Flag216prime-)
)
:effect
(and
(Flag234-)
(Flag234prime-)
)

)
(:action Flag234Action-31
:parameters ()
:precondition
(and
(Flag81-)
(Flag81prime-)
)
:effect
(and
(Flag234-)
(Flag234prime-)
)

)
(:action Flag234Action-32
:parameters ()
:precondition
(and
(Flag209-)
(Flag209prime-)
)
:effect
(and
(Flag234-)
(Flag234prime-)
)

)
(:action Flag234Action-33
:parameters ()
:precondition
(and
(Flag196-)
(Flag196prime-)
)
:effect
(and
(Flag234-)
(Flag234prime-)
)

)
(:action Flag234Action-34
:parameters ()
:precondition
(and
(Flag135-)
(Flag135prime-)
)
:effect
(and
(Flag234-)
(Flag234prime-)
)

)
(:action Flag234Action-35
:parameters ()
:precondition
(and
(Flag168-)
(Flag168prime-)
)
:effect
(and
(Flag234-)
(Flag234prime-)
)

)
(:action Flag234Action-36
:parameters ()
:precondition
(and
(Flag112-)
(Flag112prime-)
)
:effect
(and
(Flag234-)
(Flag234prime-)
)

)
(:action Flag234Action-37
:parameters ()
:precondition
(and
(Flag217-)
(Flag217prime-)
)
:effect
(and
(Flag234-)
(Flag234prime-)
)

)
(:action Flag234Action-38
:parameters ()
:precondition
(and
(Flag188-)
(Flag188prime-)
)
:effect
(and
(Flag234-)
(Flag234prime-)
)

)
(:action Flag234Action-39
:parameters ()
:precondition
(and
(Flag104-)
(Flag104prime-)
)
:effect
(and
(Flag234-)
(Flag234prime-)
)

)
(:action Flag234Action-40
:parameters ()
:precondition
(and
(Flag117-)
(Flag117prime-)
)
:effect
(and
(Flag234-)
(Flag234prime-)
)

)
(:action Flag234Action-41
:parameters ()
:precondition
(and
(Flag224-)
(Flag224prime-)
)
:effect
(and
(Flag234-)
(Flag234prime-)
)

)
(:action Flag234Action-42
:parameters ()
:precondition
(and
(Flag233-)
(Flag233prime-)
)
:effect
(and
(Flag234-)
(Flag234prime-)
)

)
(:action Flag234Action-43
:parameters ()
:precondition
(and
(Flag205-)
(Flag205prime-)
)
:effect
(and
(Flag234-)
(Flag234prime-)
)

)
(:action Flag234Action-44
:parameters ()
:precondition
(and
(Flag150-)
(Flag150prime-)
)
:effect
(and
(Flag234-)
(Flag234prime-)
)

)
(:action Flag234Action-45
:parameters ()
:precondition
(and
(Flag126-)
(Flag126prime-)
)
:effect
(and
(Flag234-)
(Flag234prime-)
)

)
(:action Flag234Action-46
:parameters ()
:precondition
(and
(Flag229-)
(Flag229prime-)
)
:effect
(and
(Flag234-)
(Flag234prime-)
)

)
(:action Flag234Action-47
:parameters ()
:precondition
(and
(Flag179-)
(Flag179prime-)
)
:effect
(and
(Flag234-)
(Flag234prime-)
)

)
(:action Flag234Action-48
:parameters ()
:precondition
(and
(Flag70-)
(Flag70prime-)
)
:effect
(and
(Flag234-)
(Flag234prime-)
)

)
(:action Flag234Action-49
:parameters ()
:precondition
(and
(Flag166-)
(Flag166prime-)
)
:effect
(and
(Flag234-)
(Flag234prime-)
)

)
(:action Prim234Action
:parameters ()
:precondition
(and
(not (Flag38-))
(Flag38prime-)
(not (Flag110-))
(Flag110prime-)
(not (Flag45-))
(Flag45prime-)
(not (Flag194-))
(Flag194prime-)
(not (Flag159-))
(Flag159prime-)
(not (Flag146-))
(Flag146prime-)
(not (Flag144-))
(Flag144prime-)
(not (Flag177-))
(Flag177prime-)
(not (Flag204-))
(Flag204prime-)
(not (Flag114-))
(Flag114prime-)
(not (Flag220-))
(Flag220prime-)
(not (Flag190-))
(Flag190prime-)
(not (Flag51-))
(Flag51prime-)
(not (Flag227-))
(Flag227prime-)
(not (Flag43-))
(Flag43prime-)
(not (Flag77-))
(Flag77prime-)
(not (Flag98-))
(Flag98prime-)
(not (Flag228-))
(Flag228prime-)
(not (Flag79-))
(Flag79prime-)
(not (Flag95-))
(Flag95prime-)
(not (Flag191-))
(Flag191prime-)
(not (Flag49-))
(Flag49prime-)
(not (Flag59-))
(Flag59prime-)
(not (Flag214-))
(Flag214prime-)
(not (Flag231-))
(Flag231prime-)
(not (Flag140-))
(Flag140prime-)
(not (Flag222-))
(Flag222prime-)
(not (Flag170-))
(Flag170prime-)
(not (Flag232-))
(Flag232prime-)
(not (Flag195-))
(Flag195prime-)
(not (Flag216-))
(Flag216prime-)
(not (Flag81-))
(Flag81prime-)
(not (Flag209-))
(Flag209prime-)
(not (Flag196-))
(Flag196prime-)
(not (Flag135-))
(Flag135prime-)
(not (Flag168-))
(Flag168prime-)
(not (Flag112-))
(Flag112prime-)
(not (Flag217-))
(Flag217prime-)
(not (Flag188-))
(Flag188prime-)
(not (Flag104-))
(Flag104prime-)
(not (Flag117-))
(Flag117prime-)
(not (Flag224-))
(Flag224prime-)
(not (Flag233-))
(Flag233prime-)
(not (Flag205-))
(Flag205prime-)
(not (Flag150-))
(Flag150prime-)
(not (Flag126-))
(Flag126prime-)
(not (Flag229-))
(Flag229prime-)
(not (Flag179-))
(Flag179prime-)
(not (Flag70-))
(Flag70prime-)
(not (Flag166-))
(Flag166prime-)
)
:effect
(and
(Flag234prime-)
(not (Flag234-))
)

)
(:action Flag237Action-0
:parameters ()
:precondition
(and
(Flag38-)
(Flag38prime-)
)
:effect
(and
(Flag237-)
(Flag237prime-)
)

)
(:action Flag237Action-1
:parameters ()
:precondition
(and
(Flag110-)
(Flag110prime-)
)
:effect
(and
(Flag237-)
(Flag237prime-)
)

)
(:action Flag237Action-2
:parameters ()
:precondition
(and
(Flag45-)
(Flag45prime-)
)
:effect
(and
(Flag237-)
(Flag237prime-)
)

)
(:action Flag237Action-3
:parameters ()
:precondition
(and
(Flag194-)
(Flag194prime-)
)
:effect
(and
(Flag237-)
(Flag237prime-)
)

)
(:action Flag237Action-4
:parameters ()
:precondition
(and
(Flag159-)
(Flag159prime-)
)
:effect
(and
(Flag237-)
(Flag237prime-)
)

)
(:action Flag237Action-5
:parameters ()
:precondition
(and
(Flag146-)
(Flag146prime-)
)
:effect
(and
(Flag237-)
(Flag237prime-)
)

)
(:action Flag237Action-6
:parameters ()
:precondition
(and
(Flag177-)
(Flag177prime-)
)
:effect
(and
(Flag237-)
(Flag237prime-)
)

)
(:action Flag237Action-7
:parameters ()
:precondition
(and
(Flag188-)
(Flag188prime-)
)
:effect
(and
(Flag237-)
(Flag237prime-)
)

)
(:action Flag237Action-8
:parameters ()
:precondition
(and
(Flag235-)
(Flag235prime-)
)
:effect
(and
(Flag237-)
(Flag237prime-)
)

)
(:action Flag237Action-9
:parameters ()
:precondition
(and
(Flag51-)
(Flag51prime-)
)
:effect
(and
(Flag237-)
(Flag237prime-)
)

)
(:action Flag237Action-10
:parameters ()
:precondition
(and
(Flag114-)
(Flag114prime-)
)
:effect
(and
(Flag237-)
(Flag237prime-)
)

)
(:action Flag237Action-11
:parameters ()
:precondition
(and
(Flag77-)
(Flag77prime-)
)
:effect
(and
(Flag237-)
(Flag237prime-)
)

)
(:action Flag237Action-12
:parameters ()
:precondition
(and
(Flag228-)
(Flag228prime-)
)
:effect
(and
(Flag237-)
(Flag237prime-)
)

)
(:action Flag237Action-13
:parameters ()
:precondition
(and
(Flag79-)
(Flag79prime-)
)
:effect
(and
(Flag237-)
(Flag237prime-)
)

)
(:action Flag237Action-14
:parameters ()
:precondition
(and
(Flag95-)
(Flag95prime-)
)
:effect
(and
(Flag237-)
(Flag237prime-)
)

)
(:action Flag237Action-15
:parameters ()
:precondition
(and
(Flag191-)
(Flag191prime-)
)
:effect
(and
(Flag237-)
(Flag237prime-)
)

)
(:action Flag237Action-16
:parameters ()
:precondition
(and
(Flag49-)
(Flag49prime-)
)
:effect
(and
(Flag237-)
(Flag237prime-)
)

)
(:action Flag237Action-17
:parameters ()
:precondition
(and
(Flag59-)
(Flag59prime-)
)
:effect
(and
(Flag237-)
(Flag237prime-)
)

)
(:action Flag237Action-18
:parameters ()
:precondition
(and
(Flag231-)
(Flag231prime-)
)
:effect
(and
(Flag237-)
(Flag237prime-)
)

)
(:action Flag237Action-19
:parameters ()
:precondition
(and
(Flag222-)
(Flag222prime-)
)
:effect
(and
(Flag237-)
(Flag237prime-)
)

)
(:action Flag237Action-20
:parameters ()
:precondition
(and
(Flag170-)
(Flag170prime-)
)
:effect
(and
(Flag237-)
(Flag237prime-)
)

)
(:action Flag237Action-21
:parameters ()
:precondition
(and
(Flag232-)
(Flag232prime-)
)
:effect
(and
(Flag237-)
(Flag237prime-)
)

)
(:action Flag237Action-22
:parameters ()
:precondition
(and
(Flag216-)
(Flag216prime-)
)
:effect
(and
(Flag237-)
(Flag237prime-)
)

)
(:action Flag237Action-23
:parameters ()
:precondition
(and
(Flag209-)
(Flag209prime-)
)
:effect
(and
(Flag237-)
(Flag237prime-)
)

)
(:action Flag237Action-24
:parameters ()
:precondition
(and
(Flag196-)
(Flag196prime-)
)
:effect
(and
(Flag237-)
(Flag237prime-)
)

)
(:action Flag237Action-25
:parameters ()
:precondition
(and
(Flag135-)
(Flag135prime-)
)
:effect
(and
(Flag237-)
(Flag237prime-)
)

)
(:action Flag237Action-26
:parameters ()
:precondition
(and
(Flag168-)
(Flag168prime-)
)
:effect
(and
(Flag237-)
(Flag237prime-)
)

)
(:action Flag237Action-27
:parameters ()
:precondition
(and
(Flag217-)
(Flag217prime-)
)
:effect
(and
(Flag237-)
(Flag237prime-)
)

)
(:action Flag237Action-28
:parameters ()
:precondition
(and
(Flag236-)
(Flag236prime-)
)
:effect
(and
(Flag237-)
(Flag237prime-)
)

)
(:action Flag237Action-29
:parameters ()
:precondition
(and
(Flag117-)
(Flag117prime-)
)
:effect
(and
(Flag237-)
(Flag237prime-)
)

)
(:action Flag237Action-30
:parameters ()
:precondition
(and
(Flag224-)
(Flag224prime-)
)
:effect
(and
(Flag237-)
(Flag237prime-)
)

)
(:action Flag237Action-31
:parameters ()
:precondition
(and
(Flag205-)
(Flag205prime-)
)
:effect
(and
(Flag237-)
(Flag237prime-)
)

)
(:action Flag237Action-32
:parameters ()
:precondition
(and
(Flag150-)
(Flag150prime-)
)
:effect
(and
(Flag237-)
(Flag237prime-)
)

)
(:action Flag237Action-33
:parameters ()
:precondition
(and
(Flag229-)
(Flag229prime-)
)
:effect
(and
(Flag237-)
(Flag237prime-)
)

)
(:action Flag237Action-34
:parameters ()
:precondition
(and
(Flag140-)
(Flag140prime-)
)
:effect
(and
(Flag237-)
(Flag237prime-)
)

)
(:action Prim237Action
:parameters ()
:precondition
(and
(not (Flag38-))
(Flag38prime-)
(not (Flag110-))
(Flag110prime-)
(not (Flag45-))
(Flag45prime-)
(not (Flag194-))
(Flag194prime-)
(not (Flag159-))
(Flag159prime-)
(not (Flag146-))
(Flag146prime-)
(not (Flag177-))
(Flag177prime-)
(not (Flag188-))
(Flag188prime-)
(not (Flag235-))
(Flag235prime-)
(not (Flag51-))
(Flag51prime-)
(not (Flag114-))
(Flag114prime-)
(not (Flag77-))
(Flag77prime-)
(not (Flag228-))
(Flag228prime-)
(not (Flag79-))
(Flag79prime-)
(not (Flag95-))
(Flag95prime-)
(not (Flag191-))
(Flag191prime-)
(not (Flag49-))
(Flag49prime-)
(not (Flag59-))
(Flag59prime-)
(not (Flag231-))
(Flag231prime-)
(not (Flag222-))
(Flag222prime-)
(not (Flag170-))
(Flag170prime-)
(not (Flag232-))
(Flag232prime-)
(not (Flag216-))
(Flag216prime-)
(not (Flag209-))
(Flag209prime-)
(not (Flag196-))
(Flag196prime-)
(not (Flag135-))
(Flag135prime-)
(not (Flag168-))
(Flag168prime-)
(not (Flag217-))
(Flag217prime-)
(not (Flag236-))
(Flag236prime-)
(not (Flag117-))
(Flag117prime-)
(not (Flag224-))
(Flag224prime-)
(not (Flag205-))
(Flag205prime-)
(not (Flag150-))
(Flag150prime-)
(not (Flag229-))
(Flag229prime-)
(not (Flag140-))
(Flag140prime-)
)
:effect
(and
(Flag237prime-)
(not (Flag237-))
)

)
(:action Flag239Action-0
:parameters ()
:precondition
(and
(Flag110-)
(Flag110prime-)
)
:effect
(and
(Flag239-)
(Flag239prime-)
)

)
(:action Flag239Action-1
:parameters ()
:precondition
(and
(Flag77-)
(Flag77prime-)
)
:effect
(and
(Flag239-)
(Flag239prime-)
)

)
(:action Flag239Action-2
:parameters ()
:precondition
(and
(Flag205-)
(Flag205prime-)
)
:effect
(and
(Flag239-)
(Flag239prime-)
)

)
(:action Flag239Action-3
:parameters ()
:precondition
(and
(Flag216-)
(Flag216prime-)
)
:effect
(and
(Flag239-)
(Flag239prime-)
)

)
(:action Flag239Action-4
:parameters ()
:precondition
(and
(Flag159-)
(Flag159prime-)
)
:effect
(and
(Flag239-)
(Flag239prime-)
)

)
(:action Flag239Action-5
:parameters ()
:precondition
(and
(Flag150-)
(Flag150prime-)
)
:effect
(and
(Flag239-)
(Flag239prime-)
)

)
(:action Flag239Action-6
:parameters ()
:precondition
(and
(Flag95-)
(Flag95prime-)
)
:effect
(and
(Flag239-)
(Flag239prime-)
)

)
(:action Flag239Action-7
:parameters ()
:precondition
(and
(Flag191-)
(Flag191prime-)
)
:effect
(and
(Flag239-)
(Flag239prime-)
)

)
(:action Flag239Action-8
:parameters ()
:precondition
(and
(Flag196-)
(Flag196prime-)
)
:effect
(and
(Flag239-)
(Flag239prime-)
)

)
(:action Flag239Action-9
:parameters ()
:precondition
(and
(Flag229-)
(Flag229prime-)
)
:effect
(and
(Flag239-)
(Flag239prime-)
)

)
(:action Flag239Action-10
:parameters ()
:precondition
(and
(Flag135-)
(Flag135prime-)
)
:effect
(and
(Flag239-)
(Flag239prime-)
)

)
(:action Flag239Action-11
:parameters ()
:precondition
(and
(Flag238-)
(Flag238prime-)
)
:effect
(and
(Flag239-)
(Flag239prime-)
)

)
(:action Flag239Action-12
:parameters ()
:precondition
(and
(Flag59-)
(Flag59prime-)
)
:effect
(and
(Flag239-)
(Flag239prime-)
)

)
(:action Flag239Action-13
:parameters ()
:precondition
(and
(Flag51-)
(Flag51prime-)
)
:effect
(and
(Flag239-)
(Flag239prime-)
)

)
(:action Flag239Action-14
:parameters ()
:precondition
(and
(Flag236-)
(Flag236prime-)
)
:effect
(and
(Flag239-)
(Flag239prime-)
)

)
(:action Flag239Action-15
:parameters ()
:precondition
(and
(Flag231-)
(Flag231prime-)
)
:effect
(and
(Flag239-)
(Flag239prime-)
)

)
(:action Flag239Action-16
:parameters ()
:precondition
(and
(Flag222-)
(Flag222prime-)
)
:effect
(and
(Flag239-)
(Flag239prime-)
)

)
(:action Flag239Action-17
:parameters ()
:precondition
(and
(Flag170-)
(Flag170prime-)
)
:effect
(and
(Flag239-)
(Flag239prime-)
)

)
(:action Prim239Action
:parameters ()
:precondition
(and
(not (Flag110-))
(Flag110prime-)
(not (Flag77-))
(Flag77prime-)
(not (Flag205-))
(Flag205prime-)
(not (Flag216-))
(Flag216prime-)
(not (Flag159-))
(Flag159prime-)
(not (Flag150-))
(Flag150prime-)
(not (Flag95-))
(Flag95prime-)
(not (Flag191-))
(Flag191prime-)
(not (Flag196-))
(Flag196prime-)
(not (Flag229-))
(Flag229prime-)
(not (Flag135-))
(Flag135prime-)
(not (Flag238-))
(Flag238prime-)
(not (Flag59-))
(Flag59prime-)
(not (Flag51-))
(Flag51prime-)
(not (Flag236-))
(Flag236prime-)
(not (Flag231-))
(Flag231prime-)
(not (Flag222-))
(Flag222prime-)
(not (Flag170-))
(Flag170prime-)
)
:effect
(and
(Flag239prime-)
(not (Flag239-))
)

)
(:action Flag240Action-0
:parameters ()
:precondition
(and
(LEFTOF1-ROBOT)
)
:effect
(and
(Flag240-)
(Flag240prime-)
)

)
(:action Flag240Action-1
:parameters ()
:precondition
(and
(LEFTOF2-ROBOT)
)
:effect
(and
(Flag240-)
(Flag240prime-)
)

)
(:action Prim240Action
:parameters ()
:precondition
(and
(not (LEFTOF1-ROBOT))
(not (LEFTOF2-ROBOT))
)
:effect
(and
(Flag240prime-)
(not (Flag240-))
)

)
(:action Flag241Action-0
:parameters ()
:precondition
(and
(LEFTOF1-ROBOT)
)
:effect
(and
(Flag241-)
(Flag241prime-)
)

)
(:action Flag241Action-1
:parameters ()
:precondition
(and
(LEFTOF3-ROBOT)
)
:effect
(and
(Flag241-)
(Flag241prime-)
)

)
(:action Flag241Action-2
:parameters ()
:precondition
(and
(LEFTOF2-ROBOT)
)
:effect
(and
(Flag241-)
(Flag241prime-)
)

)
(:action Prim241Action
:parameters ()
:precondition
(and
(not (LEFTOF1-ROBOT))
(not (LEFTOF3-ROBOT))
(not (LEFTOF2-ROBOT))
)
:effect
(and
(Flag241prime-)
(not (Flag241-))
)

)
(:action Flag242Action-0
:parameters ()
:precondition
(and
(LEFTOF1-ROBOT)
)
:effect
(and
(Flag242-)
(Flag242prime-)
)

)
(:action Flag242Action-1
:parameters ()
:precondition
(and
(LEFTOF3-ROBOT)
)
:effect
(and
(Flag242-)
(Flag242prime-)
)

)
(:action Flag242Action-2
:parameters ()
:precondition
(and
(LEFTOF4-ROBOT)
)
:effect
(and
(Flag242-)
(Flag242prime-)
)

)
(:action Flag242Action-3
:parameters ()
:precondition
(and
(LEFTOF2-ROBOT)
)
:effect
(and
(Flag242-)
(Flag242prime-)
)

)
(:action Prim242Action
:parameters ()
:precondition
(and
(not (LEFTOF1-ROBOT))
(not (LEFTOF3-ROBOT))
(not (LEFTOF4-ROBOT))
(not (LEFTOF2-ROBOT))
)
:effect
(and
(Flag242prime-)
(not (Flag242-))
)

)
(:action Flag243Action-0
:parameters ()
:precondition
(and
(LEFTOF5-ROBOT)
)
:effect
(and
(Flag243-)
(Flag243prime-)
)

)
(:action Flag243Action-1
:parameters ()
:precondition
(and
(LEFTOF1-ROBOT)
)
:effect
(and
(Flag243-)
(Flag243prime-)
)

)
(:action Flag243Action-2
:parameters ()
:precondition
(and
(LEFTOF3-ROBOT)
)
:effect
(and
(Flag243-)
(Flag243prime-)
)

)
(:action Flag243Action-3
:parameters ()
:precondition
(and
(LEFTOF4-ROBOT)
)
:effect
(and
(Flag243-)
(Flag243prime-)
)

)
(:action Flag243Action-4
:parameters ()
:precondition
(and
(LEFTOF2-ROBOT)
)
:effect
(and
(Flag243-)
(Flag243prime-)
)

)
(:action Prim243Action
:parameters ()
:precondition
(and
(not (LEFTOF5-ROBOT))
(not (LEFTOF1-ROBOT))
(not (LEFTOF3-ROBOT))
(not (LEFTOF4-ROBOT))
(not (LEFTOF2-ROBOT))
)
:effect
(and
(Flag243prime-)
(not (Flag243-))
)

)
(:action Flag244Action-0
:parameters ()
:precondition
(and
(LEFTOF2-ROBOT)
)
:effect
(and
(Flag244-)
(Flag244prime-)
)

)
(:action Flag244Action-1
:parameters ()
:precondition
(and
(LEFTOF1-ROBOT)
)
:effect
(and
(Flag244-)
(Flag244prime-)
)

)
(:action Flag244Action-2
:parameters ()
:precondition
(and
(LEFTOF6-ROBOT)
)
:effect
(and
(Flag244-)
(Flag244prime-)
)

)
(:action Flag244Action-3
:parameters ()
:precondition
(and
(LEFTOF5-ROBOT)
)
:effect
(and
(Flag244-)
(Flag244prime-)
)

)
(:action Flag244Action-4
:parameters ()
:precondition
(and
(LEFTOF3-ROBOT)
)
:effect
(and
(Flag244-)
(Flag244prime-)
)

)
(:action Flag244Action-5
:parameters ()
:precondition
(and
(LEFTOF4-ROBOT)
)
:effect
(and
(Flag244-)
(Flag244prime-)
)

)
(:action Prim244Action
:parameters ()
:precondition
(and
(not (LEFTOF2-ROBOT))
(not (LEFTOF1-ROBOT))
(not (LEFTOF6-ROBOT))
(not (LEFTOF5-ROBOT))
(not (LEFTOF3-ROBOT))
(not (LEFTOF4-ROBOT))
)
:effect
(and
(Flag244prime-)
(not (Flag244-))
)

)
(:action Flag245Action-0
:parameters ()
:precondition
(and
(LEFTOF2-ROBOT)
)
:effect
(and
(Flag245-)
(Flag245prime-)
)

)
(:action Flag245Action-1
:parameters ()
:precondition
(and
(LEFTOF7-ROBOT)
)
:effect
(and
(Flag245-)
(Flag245prime-)
)

)
(:action Flag245Action-2
:parameters ()
:precondition
(and
(LEFTOF1-ROBOT)
)
:effect
(and
(Flag245-)
(Flag245prime-)
)

)
(:action Flag245Action-3
:parameters ()
:precondition
(and
(LEFTOF6-ROBOT)
)
:effect
(and
(Flag245-)
(Flag245prime-)
)

)
(:action Flag245Action-4
:parameters ()
:precondition
(and
(LEFTOF5-ROBOT)
)
:effect
(and
(Flag245-)
(Flag245prime-)
)

)
(:action Flag245Action-5
:parameters ()
:precondition
(and
(LEFTOF3-ROBOT)
)
:effect
(and
(Flag245-)
(Flag245prime-)
)

)
(:action Flag245Action-6
:parameters ()
:precondition
(and
(LEFTOF4-ROBOT)
)
:effect
(and
(Flag245-)
(Flag245prime-)
)

)
(:action Prim245Action
:parameters ()
:precondition
(and
(not (LEFTOF2-ROBOT))
(not (LEFTOF7-ROBOT))
(not (LEFTOF1-ROBOT))
(not (LEFTOF6-ROBOT))
(not (LEFTOF5-ROBOT))
(not (LEFTOF3-ROBOT))
(not (LEFTOF4-ROBOT))
)
:effect
(and
(Flag245prime-)
(not (Flag245-))
)

)
(:action Flag246Action-0
:parameters ()
:precondition
(and
(LEFTOF2-ROBOT)
)
:effect
(and
(Flag246-)
(Flag246prime-)
)

)
(:action Flag246Action-1
:parameters ()
:precondition
(and
(LEFTOF7-ROBOT)
)
:effect
(and
(Flag246-)
(Flag246prime-)
)

)
(:action Flag246Action-2
:parameters ()
:precondition
(and
(LEFTOF1-ROBOT)
)
:effect
(and
(Flag246-)
(Flag246prime-)
)

)
(:action Flag246Action-3
:parameters ()
:precondition
(and
(LEFTOF6-ROBOT)
)
:effect
(and
(Flag246-)
(Flag246prime-)
)

)
(:action Flag246Action-4
:parameters ()
:precondition
(and
(LEFTOF5-ROBOT)
)
:effect
(and
(Flag246-)
(Flag246prime-)
)

)
(:action Flag246Action-5
:parameters ()
:precondition
(and
(LEFTOF3-ROBOT)
)
:effect
(and
(Flag246-)
(Flag246prime-)
)

)
(:action Flag246Action-6
:parameters ()
:precondition
(and
(LEFTOF4-ROBOT)
)
:effect
(and
(Flag246-)
(Flag246prime-)
)

)
(:action Flag246Action-7
:parameters ()
:precondition
(and
(LEFTOF8-ROBOT)
)
:effect
(and
(Flag246-)
(Flag246prime-)
)

)
(:action Prim246Action
:parameters ()
:precondition
(and
(not (LEFTOF2-ROBOT))
(not (LEFTOF7-ROBOT))
(not (LEFTOF1-ROBOT))
(not (LEFTOF6-ROBOT))
(not (LEFTOF5-ROBOT))
(not (LEFTOF3-ROBOT))
(not (LEFTOF4-ROBOT))
(not (LEFTOF8-ROBOT))
)
:effect
(and
(Flag246prime-)
(not (Flag246-))
)

)
(:action Flag247Action-0
:parameters ()
:precondition
(and
(LEFTOF9-ROBOT)
)
:effect
(and
(Flag247-)
(Flag247prime-)
)

)
(:action Flag247Action-1
:parameters ()
:precondition
(and
(LEFTOF2-ROBOT)
)
:effect
(and
(Flag247-)
(Flag247prime-)
)

)
(:action Flag247Action-2
:parameters ()
:precondition
(and
(LEFTOF7-ROBOT)
)
:effect
(and
(Flag247-)
(Flag247prime-)
)

)
(:action Flag247Action-3
:parameters ()
:precondition
(and
(LEFTOF1-ROBOT)
)
:effect
(and
(Flag247-)
(Flag247prime-)
)

)
(:action Flag247Action-4
:parameters ()
:precondition
(and
(LEFTOF6-ROBOT)
)
:effect
(and
(Flag247-)
(Flag247prime-)
)

)
(:action Flag247Action-5
:parameters ()
:precondition
(and
(LEFTOF5-ROBOT)
)
:effect
(and
(Flag247-)
(Flag247prime-)
)

)
(:action Flag247Action-6
:parameters ()
:precondition
(and
(LEFTOF3-ROBOT)
)
:effect
(and
(Flag247-)
(Flag247prime-)
)

)
(:action Flag247Action-7
:parameters ()
:precondition
(and
(LEFTOF4-ROBOT)
)
:effect
(and
(Flag247-)
(Flag247prime-)
)

)
(:action Flag247Action-8
:parameters ()
:precondition
(and
(LEFTOF8-ROBOT)
)
:effect
(and
(Flag247-)
(Flag247prime-)
)

)
(:action Prim247Action
:parameters ()
:precondition
(and
(not (LEFTOF9-ROBOT))
(not (LEFTOF2-ROBOT))
(not (LEFTOF7-ROBOT))
(not (LEFTOF1-ROBOT))
(not (LEFTOF6-ROBOT))
(not (LEFTOF5-ROBOT))
(not (LEFTOF3-ROBOT))
(not (LEFTOF4-ROBOT))
(not (LEFTOF8-ROBOT))
)
:effect
(and
(Flag247prime-)
(not (Flag247-))
)

)
(:action Flag248Action-0
:parameters ()
:precondition
(and
(LEFTOF10-ROBOT)
)
:effect
(and
(Flag248-)
(Flag248prime-)
)

)
(:action Flag248Action-1
:parameters ()
:precondition
(and
(LEFTOF9-ROBOT)
)
:effect
(and
(Flag248-)
(Flag248prime-)
)

)
(:action Flag248Action-2
:parameters ()
:precondition
(and
(LEFTOF2-ROBOT)
)
:effect
(and
(Flag248-)
(Flag248prime-)
)

)
(:action Flag248Action-3
:parameters ()
:precondition
(and
(LEFTOF7-ROBOT)
)
:effect
(and
(Flag248-)
(Flag248prime-)
)

)
(:action Flag248Action-4
:parameters ()
:precondition
(and
(LEFTOF1-ROBOT)
)
:effect
(and
(Flag248-)
(Flag248prime-)
)

)
(:action Flag248Action-5
:parameters ()
:precondition
(and
(LEFTOF6-ROBOT)
)
:effect
(and
(Flag248-)
(Flag248prime-)
)

)
(:action Flag248Action-6
:parameters ()
:precondition
(and
(LEFTOF5-ROBOT)
)
:effect
(and
(Flag248-)
(Flag248prime-)
)

)
(:action Flag248Action-7
:parameters ()
:precondition
(and
(LEFTOF3-ROBOT)
)
:effect
(and
(Flag248-)
(Flag248prime-)
)

)
(:action Flag248Action-8
:parameters ()
:precondition
(and
(LEFTOF4-ROBOT)
)
:effect
(and
(Flag248-)
(Flag248prime-)
)

)
(:action Flag248Action-9
:parameters ()
:precondition
(and
(LEFTOF8-ROBOT)
)
:effect
(and
(Flag248-)
(Flag248prime-)
)

)
(:action Prim248Action
:parameters ()
:precondition
(and
(not (LEFTOF10-ROBOT))
(not (LEFTOF9-ROBOT))
(not (LEFTOF2-ROBOT))
(not (LEFTOF7-ROBOT))
(not (LEFTOF1-ROBOT))
(not (LEFTOF6-ROBOT))
(not (LEFTOF5-ROBOT))
(not (LEFTOF3-ROBOT))
(not (LEFTOF4-ROBOT))
(not (LEFTOF8-ROBOT))
)
:effect
(and
(Flag248prime-)
(not (Flag248-))
)

)
(:action Flag249Action-0
:parameters ()
:precondition
(and
(LEFTOF10-ROBOT)
)
:effect
(and
(Flag249-)
(Flag249prime-)
)

)
(:action Flag249Action-1
:parameters ()
:precondition
(and
(LEFTOF11-ROBOT)
)
:effect
(and
(Flag249-)
(Flag249prime-)
)

)
(:action Flag249Action-2
:parameters ()
:precondition
(and
(LEFTOF9-ROBOT)
)
:effect
(and
(Flag249-)
(Flag249prime-)
)

)
(:action Flag249Action-3
:parameters ()
:precondition
(and
(LEFTOF2-ROBOT)
)
:effect
(and
(Flag249-)
(Flag249prime-)
)

)
(:action Flag249Action-4
:parameters ()
:precondition
(and
(LEFTOF7-ROBOT)
)
:effect
(and
(Flag249-)
(Flag249prime-)
)

)
(:action Flag249Action-5
:parameters ()
:precondition
(and
(LEFTOF1-ROBOT)
)
:effect
(and
(Flag249-)
(Flag249prime-)
)

)
(:action Flag249Action-6
:parameters ()
:precondition
(and
(LEFTOF6-ROBOT)
)
:effect
(and
(Flag249-)
(Flag249prime-)
)

)
(:action Flag249Action-7
:parameters ()
:precondition
(and
(LEFTOF5-ROBOT)
)
:effect
(and
(Flag249-)
(Flag249prime-)
)

)
(:action Flag249Action-8
:parameters ()
:precondition
(and
(LEFTOF3-ROBOT)
)
:effect
(and
(Flag249-)
(Flag249prime-)
)

)
(:action Flag249Action-9
:parameters ()
:precondition
(and
(LEFTOF4-ROBOT)
)
:effect
(and
(Flag249-)
(Flag249prime-)
)

)
(:action Flag249Action-10
:parameters ()
:precondition
(and
(LEFTOF8-ROBOT)
)
:effect
(and
(Flag249-)
(Flag249prime-)
)

)
(:action Prim249Action
:parameters ()
:precondition
(and
(not (LEFTOF10-ROBOT))
(not (LEFTOF11-ROBOT))
(not (LEFTOF9-ROBOT))
(not (LEFTOF2-ROBOT))
(not (LEFTOF7-ROBOT))
(not (LEFTOF1-ROBOT))
(not (LEFTOF6-ROBOT))
(not (LEFTOF5-ROBOT))
(not (LEFTOF3-ROBOT))
(not (LEFTOF4-ROBOT))
(not (LEFTOF8-ROBOT))
)
:effect
(and
(Flag249prime-)
(not (Flag249-))
)

)
(:action Flag250Action-0
:parameters ()
:precondition
(and
(LEFTOF10-ROBOT)
)
:effect
(and
(Flag250-)
(Flag250prime-)
)

)
(:action Flag250Action-1
:parameters ()
:precondition
(and
(LEFTOF11-ROBOT)
)
:effect
(and
(Flag250-)
(Flag250prime-)
)

)
(:action Flag250Action-2
:parameters ()
:precondition
(and
(LEFTOF9-ROBOT)
)
:effect
(and
(Flag250-)
(Flag250prime-)
)

)
(:action Flag250Action-3
:parameters ()
:precondition
(and
(LEFTOF2-ROBOT)
)
:effect
(and
(Flag250-)
(Flag250prime-)
)

)
(:action Flag250Action-4
:parameters ()
:precondition
(and
(LEFTOF7-ROBOT)
)
:effect
(and
(Flag250-)
(Flag250prime-)
)

)
(:action Flag250Action-5
:parameters ()
:precondition
(and
(LEFTOF1-ROBOT)
)
:effect
(and
(Flag250-)
(Flag250prime-)
)

)
(:action Flag250Action-6
:parameters ()
:precondition
(and
(LEFTOF12-ROBOT)
)
:effect
(and
(Flag250-)
(Flag250prime-)
)

)
(:action Flag250Action-7
:parameters ()
:precondition
(and
(LEFTOF6-ROBOT)
)
:effect
(and
(Flag250-)
(Flag250prime-)
)

)
(:action Flag250Action-8
:parameters ()
:precondition
(and
(LEFTOF5-ROBOT)
)
:effect
(and
(Flag250-)
(Flag250prime-)
)

)
(:action Flag250Action-9
:parameters ()
:precondition
(and
(LEFTOF3-ROBOT)
)
:effect
(and
(Flag250-)
(Flag250prime-)
)

)
(:action Flag250Action-10
:parameters ()
:precondition
(and
(LEFTOF4-ROBOT)
)
:effect
(and
(Flag250-)
(Flag250prime-)
)

)
(:action Flag250Action-11
:parameters ()
:precondition
(and
(LEFTOF8-ROBOT)
)
:effect
(and
(Flag250-)
(Flag250prime-)
)

)
(:action Prim250Action
:parameters ()
:precondition
(and
(not (LEFTOF10-ROBOT))
(not (LEFTOF11-ROBOT))
(not (LEFTOF9-ROBOT))
(not (LEFTOF2-ROBOT))
(not (LEFTOF7-ROBOT))
(not (LEFTOF1-ROBOT))
(not (LEFTOF12-ROBOT))
(not (LEFTOF6-ROBOT))
(not (LEFTOF5-ROBOT))
(not (LEFTOF3-ROBOT))
(not (LEFTOF4-ROBOT))
(not (LEFTOF8-ROBOT))
)
:effect
(and
(Flag250prime-)
(not (Flag250-))
)

)
(:action Flag251Action-0
:parameters ()
:precondition
(and
(LEFTOF10-ROBOT)
)
:effect
(and
(Flag251-)
(Flag251prime-)
)

)
(:action Flag251Action-1
:parameters ()
:precondition
(and
(LEFTOF11-ROBOT)
)
:effect
(and
(Flag251-)
(Flag251prime-)
)

)
(:action Flag251Action-2
:parameters ()
:precondition
(and
(LEFTOF9-ROBOT)
)
:effect
(and
(Flag251-)
(Flag251prime-)
)

)
(:action Flag251Action-3
:parameters ()
:precondition
(and
(LEFTOF2-ROBOT)
)
:effect
(and
(Flag251-)
(Flag251prime-)
)

)
(:action Flag251Action-4
:parameters ()
:precondition
(and
(LEFTOF13-ROBOT)
)
:effect
(and
(Flag251-)
(Flag251prime-)
)

)
(:action Flag251Action-5
:parameters ()
:precondition
(and
(LEFTOF1-ROBOT)
)
:effect
(and
(Flag251-)
(Flag251prime-)
)

)
(:action Flag251Action-6
:parameters ()
:precondition
(and
(LEFTOF12-ROBOT)
)
:effect
(and
(Flag251-)
(Flag251prime-)
)

)
(:action Flag251Action-7
:parameters ()
:precondition
(and
(LEFTOF6-ROBOT)
)
:effect
(and
(Flag251-)
(Flag251prime-)
)

)
(:action Flag251Action-8
:parameters ()
:precondition
(and
(LEFTOF5-ROBOT)
)
:effect
(and
(Flag251-)
(Flag251prime-)
)

)
(:action Flag251Action-9
:parameters ()
:precondition
(and
(LEFTOF3-ROBOT)
)
:effect
(and
(Flag251-)
(Flag251prime-)
)

)
(:action Flag251Action-10
:parameters ()
:precondition
(and
(LEFTOF4-ROBOT)
)
:effect
(and
(Flag251-)
(Flag251prime-)
)

)
(:action Flag251Action-11
:parameters ()
:precondition
(and
(LEFTOF7-ROBOT)
)
:effect
(and
(Flag251-)
(Flag251prime-)
)

)
(:action Flag251Action-12
:parameters ()
:precondition
(and
(LEFTOF8-ROBOT)
)
:effect
(and
(Flag251-)
(Flag251prime-)
)

)
(:action Prim251Action
:parameters ()
:precondition
(and
(not (LEFTOF10-ROBOT))
(not (LEFTOF11-ROBOT))
(not (LEFTOF9-ROBOT))
(not (LEFTOF2-ROBOT))
(not (LEFTOF13-ROBOT))
(not (LEFTOF1-ROBOT))
(not (LEFTOF12-ROBOT))
(not (LEFTOF6-ROBOT))
(not (LEFTOF5-ROBOT))
(not (LEFTOF3-ROBOT))
(not (LEFTOF4-ROBOT))
(not (LEFTOF7-ROBOT))
(not (LEFTOF8-ROBOT))
)
:effect
(and
(Flag251prime-)
(not (Flag251-))
)

)
(:action Flag252Action-0
:parameters ()
:precondition
(and
(LEFTOF10-ROBOT)
)
:effect
(and
(Flag252-)
(Flag252prime-)
)

)
(:action Flag252Action-1
:parameters ()
:precondition
(and
(LEFTOF11-ROBOT)
)
:effect
(and
(Flag252-)
(Flag252prime-)
)

)
(:action Flag252Action-2
:parameters ()
:precondition
(and
(LEFTOF9-ROBOT)
)
:effect
(and
(Flag252-)
(Flag252prime-)
)

)
(:action Flag252Action-3
:parameters ()
:precondition
(and
(LEFTOF2-ROBOT)
)
:effect
(and
(Flag252-)
(Flag252prime-)
)

)
(:action Flag252Action-4
:parameters ()
:precondition
(and
(LEFTOF13-ROBOT)
)
:effect
(and
(Flag252-)
(Flag252prime-)
)

)
(:action Flag252Action-5
:parameters ()
:precondition
(and
(LEFTOF1-ROBOT)
)
:effect
(and
(Flag252-)
(Flag252prime-)
)

)
(:action Flag252Action-6
:parameters ()
:precondition
(and
(LEFTOF12-ROBOT)
)
:effect
(and
(Flag252-)
(Flag252prime-)
)

)
(:action Flag252Action-7
:parameters ()
:precondition
(and
(LEFTOF6-ROBOT)
)
:effect
(and
(Flag252-)
(Flag252prime-)
)

)
(:action Flag252Action-8
:parameters ()
:precondition
(and
(LEFTOF5-ROBOT)
)
:effect
(and
(Flag252-)
(Flag252prime-)
)

)
(:action Flag252Action-9
:parameters ()
:precondition
(and
(LEFTOF14-ROBOT)
)
:effect
(and
(Flag252-)
(Flag252prime-)
)

)
(:action Flag252Action-10
:parameters ()
:precondition
(and
(LEFTOF3-ROBOT)
)
:effect
(and
(Flag252-)
(Flag252prime-)
)

)
(:action Flag252Action-11
:parameters ()
:precondition
(and
(LEFTOF4-ROBOT)
)
:effect
(and
(Flag252-)
(Flag252prime-)
)

)
(:action Flag252Action-12
:parameters ()
:precondition
(and
(LEFTOF7-ROBOT)
)
:effect
(and
(Flag252-)
(Flag252prime-)
)

)
(:action Flag252Action-13
:parameters ()
:precondition
(and
(LEFTOF8-ROBOT)
)
:effect
(and
(Flag252-)
(Flag252prime-)
)

)
(:action Prim252Action
:parameters ()
:precondition
(and
(not (LEFTOF10-ROBOT))
(not (LEFTOF11-ROBOT))
(not (LEFTOF9-ROBOT))
(not (LEFTOF2-ROBOT))
(not (LEFTOF13-ROBOT))
(not (LEFTOF1-ROBOT))
(not (LEFTOF12-ROBOT))
(not (LEFTOF6-ROBOT))
(not (LEFTOF5-ROBOT))
(not (LEFTOF14-ROBOT))
(not (LEFTOF3-ROBOT))
(not (LEFTOF4-ROBOT))
(not (LEFTOF7-ROBOT))
(not (LEFTOF8-ROBOT))
)
:effect
(and
(Flag252prime-)
(not (Flag252-))
)

)
(:action Flag253Action-0
:parameters ()
:precondition
(and
(LEFTOF15-ROBOT)
)
:effect
(and
(Flag253-)
(Flag253prime-)
)

)
(:action Flag253Action-1
:parameters ()
:precondition
(and
(LEFTOF10-ROBOT)
)
:effect
(and
(Flag253-)
(Flag253prime-)
)

)
(:action Flag253Action-2
:parameters ()
:precondition
(and
(LEFTOF11-ROBOT)
)
:effect
(and
(Flag253-)
(Flag253prime-)
)

)
(:action Flag253Action-3
:parameters ()
:precondition
(and
(LEFTOF9-ROBOT)
)
:effect
(and
(Flag253-)
(Flag253prime-)
)

)
(:action Flag253Action-4
:parameters ()
:precondition
(and
(LEFTOF2-ROBOT)
)
:effect
(and
(Flag253-)
(Flag253prime-)
)

)
(:action Flag253Action-5
:parameters ()
:precondition
(and
(LEFTOF13-ROBOT)
)
:effect
(and
(Flag253-)
(Flag253prime-)
)

)
(:action Flag253Action-6
:parameters ()
:precondition
(and
(LEFTOF1-ROBOT)
)
:effect
(and
(Flag253-)
(Flag253prime-)
)

)
(:action Flag253Action-7
:parameters ()
:precondition
(and
(LEFTOF12-ROBOT)
)
:effect
(and
(Flag253-)
(Flag253prime-)
)

)
(:action Flag253Action-8
:parameters ()
:precondition
(and
(LEFTOF6-ROBOT)
)
:effect
(and
(Flag253-)
(Flag253prime-)
)

)
(:action Flag253Action-9
:parameters ()
:precondition
(and
(LEFTOF5-ROBOT)
)
:effect
(and
(Flag253-)
(Flag253prime-)
)

)
(:action Flag253Action-10
:parameters ()
:precondition
(and
(LEFTOF14-ROBOT)
)
:effect
(and
(Flag253-)
(Flag253prime-)
)

)
(:action Flag253Action-11
:parameters ()
:precondition
(and
(LEFTOF3-ROBOT)
)
:effect
(and
(Flag253-)
(Flag253prime-)
)

)
(:action Flag253Action-12
:parameters ()
:precondition
(and
(LEFTOF4-ROBOT)
)
:effect
(and
(Flag253-)
(Flag253prime-)
)

)
(:action Flag253Action-13
:parameters ()
:precondition
(and
(LEFTOF7-ROBOT)
)
:effect
(and
(Flag253-)
(Flag253prime-)
)

)
(:action Flag253Action-14
:parameters ()
:precondition
(and
(LEFTOF8-ROBOT)
)
:effect
(and
(Flag253-)
(Flag253prime-)
)

)
(:action Prim253Action
:parameters ()
:precondition
(and
(not (LEFTOF15-ROBOT))
(not (LEFTOF10-ROBOT))
(not (LEFTOF11-ROBOT))
(not (LEFTOF9-ROBOT))
(not (LEFTOF2-ROBOT))
(not (LEFTOF13-ROBOT))
(not (LEFTOF1-ROBOT))
(not (LEFTOF12-ROBOT))
(not (LEFTOF6-ROBOT))
(not (LEFTOF5-ROBOT))
(not (LEFTOF14-ROBOT))
(not (LEFTOF3-ROBOT))
(not (LEFTOF4-ROBOT))
(not (LEFTOF7-ROBOT))
(not (LEFTOF8-ROBOT))
)
:effect
(and
(Flag253prime-)
(not (Flag253-))
)

)
(:action Flag254Action-0
:parameters ()
:precondition
(and
(LEFTOF15-ROBOT)
)
:effect
(and
(Flag254-)
(Flag254prime-)
)

)
(:action Flag254Action-1
:parameters ()
:precondition
(and
(LEFTOF10-ROBOT)
)
:effect
(and
(Flag254-)
(Flag254prime-)
)

)
(:action Flag254Action-2
:parameters ()
:precondition
(and
(LEFTOF11-ROBOT)
)
:effect
(and
(Flag254-)
(Flag254prime-)
)

)
(:action Flag254Action-3
:parameters ()
:precondition
(and
(LEFTOF9-ROBOT)
)
:effect
(and
(Flag254-)
(Flag254prime-)
)

)
(:action Flag254Action-4
:parameters ()
:precondition
(and
(LEFTOF2-ROBOT)
)
:effect
(and
(Flag254-)
(Flag254prime-)
)

)
(:action Flag254Action-5
:parameters ()
:precondition
(and
(LEFTOF13-ROBOT)
)
:effect
(and
(Flag254-)
(Flag254prime-)
)

)
(:action Flag254Action-6
:parameters ()
:precondition
(and
(LEFTOF1-ROBOT)
)
:effect
(and
(Flag254-)
(Flag254prime-)
)

)
(:action Flag254Action-7
:parameters ()
:precondition
(and
(LEFTOF12-ROBOT)
)
:effect
(and
(Flag254-)
(Flag254prime-)
)

)
(:action Flag254Action-8
:parameters ()
:precondition
(and
(LEFTOF6-ROBOT)
)
:effect
(and
(Flag254-)
(Flag254prime-)
)

)
(:action Flag254Action-9
:parameters ()
:precondition
(and
(LEFTOF5-ROBOT)
)
:effect
(and
(Flag254-)
(Flag254prime-)
)

)
(:action Flag254Action-10
:parameters ()
:precondition
(and
(LEFTOF16-ROBOT)
)
:effect
(and
(Flag254-)
(Flag254prime-)
)

)
(:action Flag254Action-11
:parameters ()
:precondition
(and
(LEFTOF14-ROBOT)
)
:effect
(and
(Flag254-)
(Flag254prime-)
)

)
(:action Flag254Action-12
:parameters ()
:precondition
(and
(LEFTOF3-ROBOT)
)
:effect
(and
(Flag254-)
(Flag254prime-)
)

)
(:action Flag254Action-13
:parameters ()
:precondition
(and
(LEFTOF4-ROBOT)
)
:effect
(and
(Flag254-)
(Flag254prime-)
)

)
(:action Flag254Action-14
:parameters ()
:precondition
(and
(LEFTOF7-ROBOT)
)
:effect
(and
(Flag254-)
(Flag254prime-)
)

)
(:action Flag254Action-15
:parameters ()
:precondition
(and
(LEFTOF8-ROBOT)
)
:effect
(and
(Flag254-)
(Flag254prime-)
)

)
(:action Prim254Action
:parameters ()
:precondition
(and
(not (LEFTOF15-ROBOT))
(not (LEFTOF10-ROBOT))
(not (LEFTOF11-ROBOT))
(not (LEFTOF9-ROBOT))
(not (LEFTOF2-ROBOT))
(not (LEFTOF13-ROBOT))
(not (LEFTOF1-ROBOT))
(not (LEFTOF12-ROBOT))
(not (LEFTOF6-ROBOT))
(not (LEFTOF5-ROBOT))
(not (LEFTOF16-ROBOT))
(not (LEFTOF14-ROBOT))
(not (LEFTOF3-ROBOT))
(not (LEFTOF4-ROBOT))
(not (LEFTOF7-ROBOT))
(not (LEFTOF8-ROBOT))
)
:effect
(and
(Flag254prime-)
(not (Flag254-))
)

)
(:action Flag255Action-0
:parameters ()
:precondition
(and
(LEFTOF15-ROBOT)
)
:effect
(and
(Flag255-)
(Flag255prime-)
)

)
(:action Flag255Action-1
:parameters ()
:precondition
(and
(LEFTOF10-ROBOT)
)
:effect
(and
(Flag255-)
(Flag255prime-)
)

)
(:action Flag255Action-3
:parameters ()
:precondition
(and
(LEFTOF11-ROBOT)
)
:effect
(and
(Flag255-)
(Flag255prime-)
)

)
(:action Flag255Action-4
:parameters ()
:precondition
(and
(LEFTOF9-ROBOT)
)
:effect
(and
(Flag255-)
(Flag255prime-)
)

)
(:action Flag255Action-5
:parameters ()
:precondition
(and
(LEFTOF2-ROBOT)
)
:effect
(and
(Flag255-)
(Flag255prime-)
)

)
(:action Flag255Action-6
:parameters ()
:precondition
(and
(LEFTOF13-ROBOT)
)
:effect
(and
(Flag255-)
(Flag255prime-)
)

)
(:action Flag255Action-7
:parameters ()
:precondition
(and
(LEFTOF1-ROBOT)
)
:effect
(and
(Flag255-)
(Flag255prime-)
)

)
(:action Flag255Action-8
:parameters ()
:precondition
(and
(LEFTOF12-ROBOT)
)
:effect
(and
(Flag255-)
(Flag255prime-)
)

)
(:action Flag255Action-9
:parameters ()
:precondition
(and
(LEFTOF6-ROBOT)
)
:effect
(and
(Flag255-)
(Flag255prime-)
)

)
(:action Flag255Action-10
:parameters ()
:precondition
(and
(LEFTOF5-ROBOT)
)
:effect
(and
(Flag255-)
(Flag255prime-)
)

)
(:action Flag255Action-11
:parameters ()
:precondition
(and
(LEFTOF16-ROBOT)
)
:effect
(and
(Flag255-)
(Flag255prime-)
)

)
(:action Flag255Action-12
:parameters ()
:precondition
(and
(LEFTOF14-ROBOT)
)
:effect
(and
(Flag255-)
(Flag255prime-)
)

)
(:action Flag255Action-13
:parameters ()
:precondition
(and
(LEFTOF3-ROBOT)
)
:effect
(and
(Flag255-)
(Flag255prime-)
)

)
(:action Flag255Action-14
:parameters ()
:precondition
(and
(LEFTOF4-ROBOT)
)
:effect
(and
(Flag255-)
(Flag255prime-)
)

)
(:action Flag255Action-15
:parameters ()
:precondition
(and
(LEFTOF7-ROBOT)
)
:effect
(and
(Flag255-)
(Flag255prime-)
)

)
(:action Flag255Action-16
:parameters ()
:precondition
(and
(LEFTOF8-ROBOT)
)
:effect
(and
(Flag255-)
(Flag255prime-)
)

)
(:action Prim255Action
:parameters ()
:precondition
(and
(not (LEFTOF15-ROBOT))
(not (LEFTOF10-ROBOT))
(not (LEFTOF17-ROBOT))
(not (LEFTOF11-ROBOT))
(not (LEFTOF9-ROBOT))
(not (LEFTOF2-ROBOT))
(not (LEFTOF13-ROBOT))
(not (LEFTOF1-ROBOT))
(not (LEFTOF12-ROBOT))
(not (LEFTOF6-ROBOT))
(not (LEFTOF5-ROBOT))
(not (LEFTOF16-ROBOT))
(not (LEFTOF14-ROBOT))
(not (LEFTOF3-ROBOT))
(not (LEFTOF4-ROBOT))
(not (LEFTOF7-ROBOT))
(not (LEFTOF8-ROBOT))
)
:effect
(and
(Flag255prime-)
(not (Flag255-))
)

)
(:action Flag256Action-0
:parameters ()
:precondition
(and
(LEFTOF15-ROBOT)
)
:effect
(and
(Flag256-)
(Flag256prime-)
)

)
(:action Flag256Action-1
:parameters ()
:precondition
(and
(LEFTOF10-ROBOT)
)
:effect
(and
(Flag256-)
(Flag256prime-)
)

)
(:action Flag256Action-2
:parameters ()
:precondition
(and
(LEFTOF18-ROBOT)
)
:effect
(and
(Flag256-)
(Flag256prime-)
)

)
(:action Flag256Action-4
:parameters ()
:precondition
(and
(LEFTOF11-ROBOT)
)
:effect
(and
(Flag256-)
(Flag256prime-)
)

)
(:action Flag256Action-5
:parameters ()
:precondition
(and
(LEFTOF9-ROBOT)
)
:effect
(and
(Flag256-)
(Flag256prime-)
)

)
(:action Flag256Action-6
:parameters ()
:precondition
(and
(LEFTOF2-ROBOT)
)
:effect
(and
(Flag256-)
(Flag256prime-)
)

)
(:action Flag256Action-7
:parameters ()
:precondition
(and
(LEFTOF13-ROBOT)
)
:effect
(and
(Flag256-)
(Flag256prime-)
)

)
(:action Flag256Action-8
:parameters ()
:precondition
(and
(LEFTOF1-ROBOT)
)
:effect
(and
(Flag256-)
(Flag256prime-)
)

)
(:action Flag256Action-9
:parameters ()
:precondition
(and
(LEFTOF12-ROBOT)
)
:effect
(and
(Flag256-)
(Flag256prime-)
)

)
(:action Flag256Action-10
:parameters ()
:precondition
(and
(LEFTOF6-ROBOT)
)
:effect
(and
(Flag256-)
(Flag256prime-)
)

)
(:action Flag256Action-11
:parameters ()
:precondition
(and
(LEFTOF5-ROBOT)
)
:effect
(and
(Flag256-)
(Flag256prime-)
)

)
(:action Flag256Action-12
:parameters ()
:precondition
(and
(LEFTOF16-ROBOT)
)
:effect
(and
(Flag256-)
(Flag256prime-)
)

)
(:action Flag256Action-13
:parameters ()
:precondition
(and
(LEFTOF14-ROBOT)
)
:effect
(and
(Flag256-)
(Flag256prime-)
)

)
(:action Flag256Action-14
:parameters ()
:precondition
(and
(LEFTOF3-ROBOT)
)
:effect
(and
(Flag256-)
(Flag256prime-)
)

)
(:action Flag256Action-15
:parameters ()
:precondition
(and
(LEFTOF4-ROBOT)
)
:effect
(and
(Flag256-)
(Flag256prime-)
)

)
(:action Flag256Action-16
:parameters ()
:precondition
(and
(LEFTOF7-ROBOT)
)
:effect
(and
(Flag256-)
(Flag256prime-)
)

)
(:action Flag256Action-17
:parameters ()
:precondition
(and
(LEFTOF8-ROBOT)
)
:effect
(and
(Flag256-)
(Flag256prime-)
)

)
(:action Prim256Action
:parameters ()
:precondition
(and
(not (LEFTOF15-ROBOT))
(not (LEFTOF10-ROBOT))
(not (LEFTOF18-ROBOT))
(not (LEFTOF17-ROBOT))
(not (LEFTOF11-ROBOT))
(not (LEFTOF9-ROBOT))
(not (LEFTOF2-ROBOT))
(not (LEFTOF13-ROBOT))
(not (LEFTOF1-ROBOT))
(not (LEFTOF12-ROBOT))
(not (LEFTOF6-ROBOT))
(not (LEFTOF5-ROBOT))
(not (LEFTOF16-ROBOT))
(not (LEFTOF14-ROBOT))
(not (LEFTOF3-ROBOT))
(not (LEFTOF4-ROBOT))
(not (LEFTOF7-ROBOT))
(not (LEFTOF8-ROBOT))
)
:effect
(and
(Flag256prime-)
(not (Flag256-))
)

)
(:action Flag257Action-0
:parameters ()
:precondition
(and
(RIGHTOF3-ROBOT)
)
:effect
(and
(Flag257-)
(Flag257prime-)
)

)
(:action Flag257Action-1
:parameters ()
:precondition
(and
(RIGHTOF5-ROBOT)
)
:effect
(and
(Flag257-)
(Flag257prime-)
)

)
(:action Flag257Action-2
:parameters ()
:precondition
(and
(RIGHTOF9-ROBOT)
)
:effect
(and
(Flag257-)
(Flag257prime-)
)

)
(:action Flag257Action-4
:parameters ()
:precondition
(and
(RIGHTOF17-ROBOT)
)
:effect
(and
(Flag257-)
(Flag257prime-)
)

)
(:action Flag257Action-5
:parameters ()
:precondition
(and
(RIGHTOF14-ROBOT)
)
:effect
(and
(Flag257-)
(Flag257prime-)
)

)
(:action Flag257Action-6
:parameters ()
:precondition
(and
(RIGHTOF13-ROBOT)
)
:effect
(and
(Flag257-)
(Flag257prime-)
)

)
(:action Flag257Action-7
:parameters ()
:precondition
(and
(RIGHTOF12-ROBOT)
)
:effect
(and
(Flag257-)
(Flag257prime-)
)

)
(:action Flag257Action-8
:parameters ()
:precondition
(and
(RIGHTOF10-ROBOT)
)
:effect
(and
(Flag257-)
(Flag257prime-)
)

)
(:action Flag257Action-9
:parameters ()
:precondition
(and
(RIGHTOF8-ROBOT)
)
:effect
(and
(Flag257-)
(Flag257prime-)
)

)
(:action Flag257Action-10
:parameters ()
:precondition
(and
(RIGHTOF4-ROBOT)
)
:effect
(and
(Flag257-)
(Flag257prime-)
)

)
(:action Flag257Action-11
:parameters ()
:precondition
(and
(RIGHTOF1-ROBOT)
)
:effect
(and
(Flag257-)
(Flag257prime-)
)

)
(:action Flag257Action-12
:parameters ()
:precondition
(and
(RIGHTOF16-ROBOT)
)
:effect
(and
(Flag257-)
(Flag257prime-)
)

)
(:action Flag257Action-13
:parameters ()
:precondition
(and
(RIGHTOF7-ROBOT)
)
:effect
(and
(Flag257-)
(Flag257prime-)
)

)
(:action Flag257Action-14
:parameters ()
:precondition
(and
(RIGHTOF11-ROBOT)
)
:effect
(and
(Flag257-)
(Flag257prime-)
)

)
(:action Flag257Action-15
:parameters ()
:precondition
(and
(RIGHTOF15-ROBOT)
)
:effect
(and
(Flag257-)
(Flag257prime-)
)

)
(:action Flag257Action-16
:parameters ()
:precondition
(and
(RIGHTOF6-ROBOT)
)
:effect
(and
(Flag257-)
(Flag257prime-)
)

)
(:action Prim257Action
:parameters ()
:precondition
(and
(not (RIGHTOF3-ROBOT))
(not (RIGHTOF5-ROBOT))
(not (RIGHTOF9-ROBOT))
(not (RIGHTOF2-ROBOT))
(not (RIGHTOF17-ROBOT))
(not (RIGHTOF14-ROBOT))
(not (RIGHTOF13-ROBOT))
(not (RIGHTOF12-ROBOT))
(not (RIGHTOF10-ROBOT))
(not (RIGHTOF8-ROBOT))
(not (RIGHTOF4-ROBOT))
(not (RIGHTOF1-ROBOT))
(not (RIGHTOF16-ROBOT))
(not (RIGHTOF7-ROBOT))
(not (RIGHTOF11-ROBOT))
(not (RIGHTOF15-ROBOT))
(not (RIGHTOF6-ROBOT))
)
:effect
(and
(Flag257prime-)
(not (Flag257-))
)

)
(:action Flag258Action-0
:parameters ()
:precondition
(and
(RIGHTOF3-ROBOT)
)
:effect
(and
(Flag258-)
(Flag258prime-)
)

)
(:action Flag258Action-1
:parameters ()
:precondition
(and
(RIGHTOF5-ROBOT)
)
:effect
(and
(Flag258-)
(Flag258prime-)
)

)
(:action Flag258Action-2
:parameters ()
:precondition
(and
(RIGHTOF9-ROBOT)
)
:effect
(and
(Flag258-)
(Flag258prime-)
)

)
(:action Flag258Action-4
:parameters ()
:precondition
(and
(RIGHTOF17-ROBOT)
)
:effect
(and
(Flag258-)
(Flag258prime-)
)

)
(:action Flag258Action-5
:parameters ()
:precondition
(and
(RIGHTOF14-ROBOT)
)
:effect
(and
(Flag258-)
(Flag258prime-)
)

)
(:action Flag258Action-6
:parameters ()
:precondition
(and
(RIGHTOF13-ROBOT)
)
:effect
(and
(Flag258-)
(Flag258prime-)
)

)
(:action Flag258Action-7
:parameters ()
:precondition
(and
(RIGHTOF12-ROBOT)
)
:effect
(and
(Flag258-)
(Flag258prime-)
)

)
(:action Flag258Action-8
:parameters ()
:precondition
(and
(RIGHTOF10-ROBOT)
)
:effect
(and
(Flag258-)
(Flag258prime-)
)

)
(:action Flag258Action-9
:parameters ()
:precondition
(and
(RIGHTOF4-ROBOT)
)
:effect
(and
(Flag258-)
(Flag258prime-)
)

)
(:action Flag258Action-10
:parameters ()
:precondition
(and
(RIGHTOF8-ROBOT)
)
:effect
(and
(Flag258-)
(Flag258prime-)
)

)
(:action Flag258Action-11
:parameters ()
:precondition
(and
(RIGHTOF16-ROBOT)
)
:effect
(and
(Flag258-)
(Flag258prime-)
)

)
(:action Flag258Action-12
:parameters ()
:precondition
(and
(RIGHTOF7-ROBOT)
)
:effect
(and
(Flag258-)
(Flag258prime-)
)

)
(:action Flag258Action-13
:parameters ()
:precondition
(and
(RIGHTOF11-ROBOT)
)
:effect
(and
(Flag258-)
(Flag258prime-)
)

)
(:action Flag258Action-14
:parameters ()
:precondition
(and
(RIGHTOF15-ROBOT)
)
:effect
(and
(Flag258-)
(Flag258prime-)
)

)
(:action Flag258Action-15
:parameters ()
:precondition
(and
(RIGHTOF6-ROBOT)
)
:effect
(and
(Flag258-)
(Flag258prime-)
)

)
(:action Prim258Action
:parameters ()
:precondition
(and
(not (RIGHTOF3-ROBOT))
(not (RIGHTOF5-ROBOT))
(not (RIGHTOF9-ROBOT))
(not (RIGHTOF2-ROBOT))
(not (RIGHTOF17-ROBOT))
(not (RIGHTOF14-ROBOT))
(not (RIGHTOF13-ROBOT))
(not (RIGHTOF12-ROBOT))
(not (RIGHTOF10-ROBOT))
(not (RIGHTOF4-ROBOT))
(not (RIGHTOF8-ROBOT))
(not (RIGHTOF16-ROBOT))
(not (RIGHTOF7-ROBOT))
(not (RIGHTOF11-ROBOT))
(not (RIGHTOF15-ROBOT))
(not (RIGHTOF6-ROBOT))
)
:effect
(and
(Flag258prime-)
(not (Flag258-))
)

)
(:action Flag259Action-0
:parameters ()
:precondition
(and
(RIGHTOF3-ROBOT)
)
:effect
(and
(Flag259-)
(Flag259prime-)
)

)
(:action Flag259Action-1
:parameters ()
:precondition
(and
(RIGHTOF5-ROBOT)
)
:effect
(and
(Flag259-)
(Flag259prime-)
)

)
(:action Flag259Action-2
:parameters ()
:precondition
(and
(RIGHTOF9-ROBOT)
)
:effect
(and
(Flag259-)
(Flag259prime-)
)

)
(:action Flag259Action-3
:parameters ()
:precondition
(and
(RIGHTOF17-ROBOT)
)
:effect
(and
(Flag259-)
(Flag259prime-)
)

)
(:action Flag259Action-4
:parameters ()
:precondition
(and
(RIGHTOF14-ROBOT)
)
:effect
(and
(Flag259-)
(Flag259prime-)
)

)
(:action Flag259Action-5
:parameters ()
:precondition
(and
(RIGHTOF13-ROBOT)
)
:effect
(and
(Flag259-)
(Flag259prime-)
)

)
(:action Flag259Action-6
:parameters ()
:precondition
(and
(RIGHTOF12-ROBOT)
)
:effect
(and
(Flag259-)
(Flag259prime-)
)

)
(:action Flag259Action-7
:parameters ()
:precondition
(and
(RIGHTOF10-ROBOT)
)
:effect
(and
(Flag259-)
(Flag259prime-)
)

)
(:action Flag259Action-8
:parameters ()
:precondition
(and
(RIGHTOF4-ROBOT)
)
:effect
(and
(Flag259-)
(Flag259prime-)
)

)
(:action Flag259Action-9
:parameters ()
:precondition
(and
(RIGHTOF8-ROBOT)
)
:effect
(and
(Flag259-)
(Flag259prime-)
)

)
(:action Flag259Action-10
:parameters ()
:precondition
(and
(RIGHTOF16-ROBOT)
)
:effect
(and
(Flag259-)
(Flag259prime-)
)

)
(:action Flag259Action-11
:parameters ()
:precondition
(and
(RIGHTOF7-ROBOT)
)
:effect
(and
(Flag259-)
(Flag259prime-)
)

)
(:action Flag259Action-12
:parameters ()
:precondition
(and
(RIGHTOF11-ROBOT)
)
:effect
(and
(Flag259-)
(Flag259prime-)
)

)
(:action Flag259Action-13
:parameters ()
:precondition
(and
(RIGHTOF15-ROBOT)
)
:effect
(and
(Flag259-)
(Flag259prime-)
)

)
(:action Flag259Action-14
:parameters ()
:precondition
(and
(RIGHTOF6-ROBOT)
)
:effect
(and
(Flag259-)
(Flag259prime-)
)

)
(:action Prim259Action
:parameters ()
:precondition
(and
(not (RIGHTOF3-ROBOT))
(not (RIGHTOF5-ROBOT))
(not (RIGHTOF9-ROBOT))
(not (RIGHTOF17-ROBOT))
(not (RIGHTOF14-ROBOT))
(not (RIGHTOF13-ROBOT))
(not (RIGHTOF12-ROBOT))
(not (RIGHTOF10-ROBOT))
(not (RIGHTOF4-ROBOT))
(not (RIGHTOF8-ROBOT))
(not (RIGHTOF16-ROBOT))
(not (RIGHTOF7-ROBOT))
(not (RIGHTOF11-ROBOT))
(not (RIGHTOF15-ROBOT))
(not (RIGHTOF6-ROBOT))
)
:effect
(and
(Flag259prime-)
(not (Flag259-))
)

)
(:action Flag260Action-0
:parameters ()
:precondition
(and
(RIGHTOF5-ROBOT)
)
:effect
(and
(Flag260-)
(Flag260prime-)
)

)
(:action Flag260Action-1
:parameters ()
:precondition
(and
(RIGHTOF9-ROBOT)
)
:effect
(and
(Flag260-)
(Flag260prime-)
)

)
(:action Flag260Action-2
:parameters ()
:precondition
(and
(RIGHTOF17-ROBOT)
)
:effect
(and
(Flag260-)
(Flag260prime-)
)

)
(:action Flag260Action-3
:parameters ()
:precondition
(and
(RIGHTOF14-ROBOT)
)
:effect
(and
(Flag260-)
(Flag260prime-)
)

)
(:action Flag260Action-4
:parameters ()
:precondition
(and
(RIGHTOF13-ROBOT)
)
:effect
(and
(Flag260-)
(Flag260prime-)
)

)
(:action Flag260Action-5
:parameters ()
:precondition
(and
(RIGHTOF12-ROBOT)
)
:effect
(and
(Flag260-)
(Flag260prime-)
)

)
(:action Flag260Action-6
:parameters ()
:precondition
(and
(RIGHTOF10-ROBOT)
)
:effect
(and
(Flag260-)
(Flag260prime-)
)

)
(:action Flag260Action-7
:parameters ()
:precondition
(and
(RIGHTOF4-ROBOT)
)
:effect
(and
(Flag260-)
(Flag260prime-)
)

)
(:action Flag260Action-8
:parameters ()
:precondition
(and
(RIGHTOF8-ROBOT)
)
:effect
(and
(Flag260-)
(Flag260prime-)
)

)
(:action Flag260Action-9
:parameters ()
:precondition
(and
(RIGHTOF16-ROBOT)
)
:effect
(and
(Flag260-)
(Flag260prime-)
)

)
(:action Flag260Action-10
:parameters ()
:precondition
(and
(RIGHTOF7-ROBOT)
)
:effect
(and
(Flag260-)
(Flag260prime-)
)

)
(:action Flag260Action-11
:parameters ()
:precondition
(and
(RIGHTOF11-ROBOT)
)
:effect
(and
(Flag260-)
(Flag260prime-)
)

)
(:action Flag260Action-12
:parameters ()
:precondition
(and
(RIGHTOF15-ROBOT)
)
:effect
(and
(Flag260-)
(Flag260prime-)
)

)
(:action Flag260Action-13
:parameters ()
:precondition
(and
(RIGHTOF6-ROBOT)
)
:effect
(and
(Flag260-)
(Flag260prime-)
)

)
(:action Prim260Action
:parameters ()
:precondition
(and
(not (RIGHTOF5-ROBOT))
(not (RIGHTOF9-ROBOT))
(not (RIGHTOF17-ROBOT))
(not (RIGHTOF14-ROBOT))
(not (RIGHTOF13-ROBOT))
(not (RIGHTOF12-ROBOT))
(not (RIGHTOF10-ROBOT))
(not (RIGHTOF4-ROBOT))
(not (RIGHTOF8-ROBOT))
(not (RIGHTOF16-ROBOT))
(not (RIGHTOF7-ROBOT))
(not (RIGHTOF11-ROBOT))
(not (RIGHTOF15-ROBOT))
(not (RIGHTOF6-ROBOT))
)
:effect
(and
(Flag260prime-)
(not (Flag260-))
)

)
(:action Flag261Action-0
:parameters ()
:precondition
(and
(RIGHTOF5-ROBOT)
)
:effect
(and
(Flag261-)
(Flag261prime-)
)

)
(:action Flag261Action-1
:parameters ()
:precondition
(and
(RIGHTOF9-ROBOT)
)
:effect
(and
(Flag261-)
(Flag261prime-)
)

)
(:action Flag261Action-2
:parameters ()
:precondition
(and
(RIGHTOF17-ROBOT)
)
:effect
(and
(Flag261-)
(Flag261prime-)
)

)
(:action Flag261Action-3
:parameters ()
:precondition
(and
(RIGHTOF14-ROBOT)
)
:effect
(and
(Flag261-)
(Flag261prime-)
)

)
(:action Flag261Action-4
:parameters ()
:precondition
(and
(RIGHTOF13-ROBOT)
)
:effect
(and
(Flag261-)
(Flag261prime-)
)

)
(:action Flag261Action-5
:parameters ()
:precondition
(and
(RIGHTOF12-ROBOT)
)
:effect
(and
(Flag261-)
(Flag261prime-)
)

)
(:action Flag261Action-6
:parameters ()
:precondition
(and
(RIGHTOF10-ROBOT)
)
:effect
(and
(Flag261-)
(Flag261prime-)
)

)
(:action Flag261Action-7
:parameters ()
:precondition
(and
(RIGHTOF8-ROBOT)
)
:effect
(and
(Flag261-)
(Flag261prime-)
)

)
(:action Flag261Action-8
:parameters ()
:precondition
(and
(RIGHTOF16-ROBOT)
)
:effect
(and
(Flag261-)
(Flag261prime-)
)

)
(:action Flag261Action-9
:parameters ()
:precondition
(and
(RIGHTOF7-ROBOT)
)
:effect
(and
(Flag261-)
(Flag261prime-)
)

)
(:action Flag261Action-10
:parameters ()
:precondition
(and
(RIGHTOF11-ROBOT)
)
:effect
(and
(Flag261-)
(Flag261prime-)
)

)
(:action Flag261Action-11
:parameters ()
:precondition
(and
(RIGHTOF15-ROBOT)
)
:effect
(and
(Flag261-)
(Flag261prime-)
)

)
(:action Flag261Action-12
:parameters ()
:precondition
(and
(RIGHTOF6-ROBOT)
)
:effect
(and
(Flag261-)
(Flag261prime-)
)

)
(:action Prim261Action
:parameters ()
:precondition
(and
(not (RIGHTOF5-ROBOT))
(not (RIGHTOF9-ROBOT))
(not (RIGHTOF17-ROBOT))
(not (RIGHTOF14-ROBOT))
(not (RIGHTOF13-ROBOT))
(not (RIGHTOF12-ROBOT))
(not (RIGHTOF10-ROBOT))
(not (RIGHTOF8-ROBOT))
(not (RIGHTOF16-ROBOT))
(not (RIGHTOF7-ROBOT))
(not (RIGHTOF11-ROBOT))
(not (RIGHTOF15-ROBOT))
(not (RIGHTOF6-ROBOT))
)
:effect
(and
(Flag261prime-)
(not (Flag261-))
)

)
(:action Flag262Action-0
:parameters ()
:precondition
(and
(RIGHTOF9-ROBOT)
)
:effect
(and
(Flag262-)
(Flag262prime-)
)

)
(:action Flag262Action-1
:parameters ()
:precondition
(and
(RIGHTOF17-ROBOT)
)
:effect
(and
(Flag262-)
(Flag262prime-)
)

)
(:action Flag262Action-2
:parameters ()
:precondition
(and
(RIGHTOF14-ROBOT)
)
:effect
(and
(Flag262-)
(Flag262prime-)
)

)
(:action Flag262Action-3
:parameters ()
:precondition
(and
(RIGHTOF13-ROBOT)
)
:effect
(and
(Flag262-)
(Flag262prime-)
)

)
(:action Flag262Action-4
:parameters ()
:precondition
(and
(RIGHTOF12-ROBOT)
)
:effect
(and
(Flag262-)
(Flag262prime-)
)

)
(:action Flag262Action-5
:parameters ()
:precondition
(and
(RIGHTOF10-ROBOT)
)
:effect
(and
(Flag262-)
(Flag262prime-)
)

)
(:action Flag262Action-6
:parameters ()
:precondition
(and
(RIGHTOF8-ROBOT)
)
:effect
(and
(Flag262-)
(Flag262prime-)
)

)
(:action Flag262Action-7
:parameters ()
:precondition
(and
(RIGHTOF16-ROBOT)
)
:effect
(and
(Flag262-)
(Flag262prime-)
)

)
(:action Flag262Action-8
:parameters ()
:precondition
(and
(RIGHTOF7-ROBOT)
)
:effect
(and
(Flag262-)
(Flag262prime-)
)

)
(:action Flag262Action-9
:parameters ()
:precondition
(and
(RIGHTOF11-ROBOT)
)
:effect
(and
(Flag262-)
(Flag262prime-)
)

)
(:action Flag262Action-10
:parameters ()
:precondition
(and
(RIGHTOF15-ROBOT)
)
:effect
(and
(Flag262-)
(Flag262prime-)
)

)
(:action Flag262Action-11
:parameters ()
:precondition
(and
(RIGHTOF6-ROBOT)
)
:effect
(and
(Flag262-)
(Flag262prime-)
)

)
(:action Prim262Action
:parameters ()
:precondition
(and
(not (RIGHTOF9-ROBOT))
(not (RIGHTOF17-ROBOT))
(not (RIGHTOF14-ROBOT))
(not (RIGHTOF13-ROBOT))
(not (RIGHTOF12-ROBOT))
(not (RIGHTOF10-ROBOT))
(not (RIGHTOF8-ROBOT))
(not (RIGHTOF16-ROBOT))
(not (RIGHTOF7-ROBOT))
(not (RIGHTOF11-ROBOT))
(not (RIGHTOF15-ROBOT))
(not (RIGHTOF6-ROBOT))
)
:effect
(and
(Flag262prime-)
(not (Flag262-))
)

)
(:action Flag263Action-0
:parameters ()
:precondition
(and
(RIGHTOF17-ROBOT)
)
:effect
(and
(Flag263-)
(Flag263prime-)
)

)
(:action Flag263Action-1
:parameters ()
:precondition
(and
(RIGHTOF14-ROBOT)
)
:effect
(and
(Flag263-)
(Flag263prime-)
)

)
(:action Flag263Action-2
:parameters ()
:precondition
(and
(RIGHTOF13-ROBOT)
)
:effect
(and
(Flag263-)
(Flag263prime-)
)

)
(:action Flag263Action-3
:parameters ()
:precondition
(and
(RIGHTOF15-ROBOT)
)
:effect
(and
(Flag263-)
(Flag263prime-)
)

)
(:action Flag263Action-4
:parameters ()
:precondition
(and
(RIGHTOF10-ROBOT)
)
:effect
(and
(Flag263-)
(Flag263prime-)
)

)
(:action Flag263Action-5
:parameters ()
:precondition
(and
(RIGHTOF8-ROBOT)
)
:effect
(and
(Flag263-)
(Flag263prime-)
)

)
(:action Flag263Action-6
:parameters ()
:precondition
(and
(RIGHTOF12-ROBOT)
)
:effect
(and
(Flag263-)
(Flag263prime-)
)

)
(:action Flag263Action-7
:parameters ()
:precondition
(and
(RIGHTOF16-ROBOT)
)
:effect
(and
(Flag263-)
(Flag263prime-)
)

)
(:action Flag263Action-8
:parameters ()
:precondition
(and
(RIGHTOF7-ROBOT)
)
:effect
(and
(Flag263-)
(Flag263prime-)
)

)
(:action Flag263Action-9
:parameters ()
:precondition
(and
(RIGHTOF9-ROBOT)
)
:effect
(and
(Flag263-)
(Flag263prime-)
)

)
(:action Flag263Action-10
:parameters ()
:precondition
(and
(RIGHTOF11-ROBOT)
)
:effect
(and
(Flag263-)
(Flag263prime-)
)

)
(:action Prim263Action
:parameters ()
:precondition
(and
(not (RIGHTOF17-ROBOT))
(not (RIGHTOF14-ROBOT))
(not (RIGHTOF13-ROBOT))
(not (RIGHTOF15-ROBOT))
(not (RIGHTOF10-ROBOT))
(not (RIGHTOF8-ROBOT))
(not (RIGHTOF12-ROBOT))
(not (RIGHTOF16-ROBOT))
(not (RIGHTOF7-ROBOT))
(not (RIGHTOF9-ROBOT))
(not (RIGHTOF11-ROBOT))
)
:effect
(and
(Flag263prime-)
(not (Flag263-))
)

)
(:action Flag264Action-0
:parameters ()
:precondition
(and
(RIGHTOF17-ROBOT)
)
:effect
(and
(Flag264-)
(Flag264prime-)
)

)
(:action Flag264Action-1
:parameters ()
:precondition
(and
(RIGHTOF14-ROBOT)
)
:effect
(and
(Flag264-)
(Flag264prime-)
)

)
(:action Flag264Action-2
:parameters ()
:precondition
(and
(RIGHTOF13-ROBOT)
)
:effect
(and
(Flag264-)
(Flag264prime-)
)

)
(:action Flag264Action-3
:parameters ()
:precondition
(and
(RIGHTOF15-ROBOT)
)
:effect
(and
(Flag264-)
(Flag264prime-)
)

)
(:action Flag264Action-4
:parameters ()
:precondition
(and
(RIGHTOF10-ROBOT)
)
:effect
(and
(Flag264-)
(Flag264prime-)
)

)
(:action Flag264Action-5
:parameters ()
:precondition
(and
(RIGHTOF8-ROBOT)
)
:effect
(and
(Flag264-)
(Flag264prime-)
)

)
(:action Flag264Action-6
:parameters ()
:precondition
(and
(RIGHTOF16-ROBOT)
)
:effect
(and
(Flag264-)
(Flag264prime-)
)

)
(:action Flag264Action-7
:parameters ()
:precondition
(and
(RIGHTOF12-ROBOT)
)
:effect
(and
(Flag264-)
(Flag264prime-)
)

)
(:action Flag264Action-8
:parameters ()
:precondition
(and
(RIGHTOF9-ROBOT)
)
:effect
(and
(Flag264-)
(Flag264prime-)
)

)
(:action Flag264Action-9
:parameters ()
:precondition
(and
(RIGHTOF11-ROBOT)
)
:effect
(and
(Flag264-)
(Flag264prime-)
)

)
(:action Prim264Action
:parameters ()
:precondition
(and
(not (RIGHTOF17-ROBOT))
(not (RIGHTOF14-ROBOT))
(not (RIGHTOF13-ROBOT))
(not (RIGHTOF15-ROBOT))
(not (RIGHTOF10-ROBOT))
(not (RIGHTOF8-ROBOT))
(not (RIGHTOF16-ROBOT))
(not (RIGHTOF12-ROBOT))
(not (RIGHTOF9-ROBOT))
(not (RIGHTOF11-ROBOT))
)
:effect
(and
(Flag264prime-)
(not (Flag264-))
)

)
(:action Flag265Action-0
:parameters ()
:precondition
(and
(RIGHTOF17-ROBOT)
)
:effect
(and
(Flag265-)
(Flag265prime-)
)

)
(:action Flag265Action-1
:parameters ()
:precondition
(and
(RIGHTOF14-ROBOT)
)
:effect
(and
(Flag265-)
(Flag265prime-)
)

)
(:action Flag265Action-2
:parameters ()
:precondition
(and
(RIGHTOF13-ROBOT)
)
:effect
(and
(Flag265-)
(Flag265prime-)
)

)
(:action Flag265Action-3
:parameters ()
:precondition
(and
(RIGHTOF15-ROBOT)
)
:effect
(and
(Flag265-)
(Flag265prime-)
)

)
(:action Flag265Action-4
:parameters ()
:precondition
(and
(RIGHTOF10-ROBOT)
)
:effect
(and
(Flag265-)
(Flag265prime-)
)

)
(:action Flag265Action-5
:parameters ()
:precondition
(and
(RIGHTOF16-ROBOT)
)
:effect
(and
(Flag265-)
(Flag265prime-)
)

)
(:action Flag265Action-6
:parameters ()
:precondition
(and
(RIGHTOF12-ROBOT)
)
:effect
(and
(Flag265-)
(Flag265prime-)
)

)
(:action Flag265Action-7
:parameters ()
:precondition
(and
(RIGHTOF9-ROBOT)
)
:effect
(and
(Flag265-)
(Flag265prime-)
)

)
(:action Flag265Action-8
:parameters ()
:precondition
(and
(RIGHTOF11-ROBOT)
)
:effect
(and
(Flag265-)
(Flag265prime-)
)

)
(:action Prim265Action
:parameters ()
:precondition
(and
(not (RIGHTOF17-ROBOT))
(not (RIGHTOF14-ROBOT))
(not (RIGHTOF13-ROBOT))
(not (RIGHTOF15-ROBOT))
(not (RIGHTOF10-ROBOT))
(not (RIGHTOF16-ROBOT))
(not (RIGHTOF12-ROBOT))
(not (RIGHTOF9-ROBOT))
(not (RIGHTOF11-ROBOT))
)
:effect
(and
(Flag265prime-)
(not (Flag265-))
)

)
(:action Flag266Action-0
:parameters ()
:precondition
(and
(RIGHTOF17-ROBOT)
)
:effect
(and
(Flag266-)
(Flag266prime-)
)

)
(:action Flag266Action-1
:parameters ()
:precondition
(and
(RIGHTOF14-ROBOT)
)
:effect
(and
(Flag266-)
(Flag266prime-)
)

)
(:action Flag266Action-2
:parameters ()
:precondition
(and
(RIGHTOF13-ROBOT)
)
:effect
(and
(Flag266-)
(Flag266prime-)
)

)
(:action Flag266Action-3
:parameters ()
:precondition
(and
(RIGHTOF15-ROBOT)
)
:effect
(and
(Flag266-)
(Flag266prime-)
)

)
(:action Flag266Action-4
:parameters ()
:precondition
(and
(RIGHTOF10-ROBOT)
)
:effect
(and
(Flag266-)
(Flag266prime-)
)

)
(:action Flag266Action-5
:parameters ()
:precondition
(and
(RIGHTOF16-ROBOT)
)
:effect
(and
(Flag266-)
(Flag266prime-)
)

)
(:action Flag266Action-6
:parameters ()
:precondition
(and
(RIGHTOF12-ROBOT)
)
:effect
(and
(Flag266-)
(Flag266prime-)
)

)
(:action Flag266Action-7
:parameters ()
:precondition
(and
(RIGHTOF11-ROBOT)
)
:effect
(and
(Flag266-)
(Flag266prime-)
)

)
(:action Prim266Action
:parameters ()
:precondition
(and
(not (RIGHTOF17-ROBOT))
(not (RIGHTOF14-ROBOT))
(not (RIGHTOF13-ROBOT))
(not (RIGHTOF15-ROBOT))
(not (RIGHTOF10-ROBOT))
(not (RIGHTOF16-ROBOT))
(not (RIGHTOF12-ROBOT))
(not (RIGHTOF11-ROBOT))
)
:effect
(and
(Flag266prime-)
(not (Flag266-))
)

)
(:action Flag267Action-0
:parameters ()
:precondition
(and
(RIGHTOF17-ROBOT)
)
:effect
(and
(Flag267-)
(Flag267prime-)
)

)
(:action Flag267Action-1
:parameters ()
:precondition
(and
(RIGHTOF14-ROBOT)
)
:effect
(and
(Flag267-)
(Flag267prime-)
)

)
(:action Flag267Action-2
:parameters ()
:precondition
(and
(RIGHTOF13-ROBOT)
)
:effect
(and
(Flag267-)
(Flag267prime-)
)

)
(:action Flag267Action-3
:parameters ()
:precondition
(and
(RIGHTOF15-ROBOT)
)
:effect
(and
(Flag267-)
(Flag267prime-)
)

)
(:action Flag267Action-4
:parameters ()
:precondition
(and
(RIGHTOF16-ROBOT)
)
:effect
(and
(Flag267-)
(Flag267prime-)
)

)
(:action Flag267Action-5
:parameters ()
:precondition
(and
(RIGHTOF12-ROBOT)
)
:effect
(and
(Flag267-)
(Flag267prime-)
)

)
(:action Flag267Action-6
:parameters ()
:precondition
(and
(RIGHTOF11-ROBOT)
)
:effect
(and
(Flag267-)
(Flag267prime-)
)

)
(:action Prim267Action
:parameters ()
:precondition
(and
(not (RIGHTOF17-ROBOT))
(not (RIGHTOF14-ROBOT))
(not (RIGHTOF13-ROBOT))
(not (RIGHTOF15-ROBOT))
(not (RIGHTOF16-ROBOT))
(not (RIGHTOF12-ROBOT))
(not (RIGHTOF11-ROBOT))
)
:effect
(and
(Flag267prime-)
(not (Flag267-))
)

)
(:action Flag268Action-0
:parameters ()
:precondition
(and
(RIGHTOF17-ROBOT)
)
:effect
(and
(Flag268-)
(Flag268prime-)
)

)
(:action Flag268Action-1
:parameters ()
:precondition
(and
(RIGHTOF14-ROBOT)
)
:effect
(and
(Flag268-)
(Flag268prime-)
)

)
(:action Flag268Action-2
:parameters ()
:precondition
(and
(RIGHTOF13-ROBOT)
)
:effect
(and
(Flag268-)
(Flag268prime-)
)

)
(:action Flag268Action-3
:parameters ()
:precondition
(and
(RIGHTOF12-ROBOT)
)
:effect
(and
(Flag268-)
(Flag268prime-)
)

)
(:action Flag268Action-4
:parameters ()
:precondition
(and
(RIGHTOF16-ROBOT)
)
:effect
(and
(Flag268-)
(Flag268prime-)
)

)
(:action Flag268Action-5
:parameters ()
:precondition
(and
(RIGHTOF15-ROBOT)
)
:effect
(and
(Flag268-)
(Flag268prime-)
)

)
(:action Prim268Action
:parameters ()
:precondition
(and
(not (RIGHTOF17-ROBOT))
(not (RIGHTOF14-ROBOT))
(not (RIGHTOF13-ROBOT))
(not (RIGHTOF12-ROBOT))
(not (RIGHTOF16-ROBOT))
(not (RIGHTOF15-ROBOT))
)
:effect
(and
(Flag268prime-)
(not (Flag268-))
)

)
(:action Flag269Action-0
:parameters ()
:precondition
(and
(RIGHTOF16-ROBOT)
)
:effect
(and
(Flag269-)
(Flag269prime-)
)

)
(:action Flag269Action-1
:parameters ()
:precondition
(and
(RIGHTOF17-ROBOT)
)
:effect
(and
(Flag269-)
(Flag269prime-)
)

)
(:action Flag269Action-2
:parameters ()
:precondition
(and
(RIGHTOF14-ROBOT)
)
:effect
(and
(Flag269-)
(Flag269prime-)
)

)
(:action Flag269Action-3
:parameters ()
:precondition
(and
(RIGHTOF13-ROBOT)
)
:effect
(and
(Flag269-)
(Flag269prime-)
)

)
(:action Flag269Action-4
:parameters ()
:precondition
(and
(RIGHTOF15-ROBOT)
)
:effect
(and
(Flag269-)
(Flag269prime-)
)

)
(:action Prim269Action
:parameters ()
:precondition
(and
(not (RIGHTOF16-ROBOT))
(not (RIGHTOF17-ROBOT))
(not (RIGHTOF14-ROBOT))
(not (RIGHTOF13-ROBOT))
(not (RIGHTOF15-ROBOT))
)
:effect
(and
(Flag269prime-)
(not (Flag269-))
)

)
(:action Flag270Action-0
:parameters ()
:precondition
(and
(RIGHTOF16-ROBOT)
)
:effect
(and
(Flag270-)
(Flag270prime-)
)

)
(:action Flag270Action-1
:parameters ()
:precondition
(and
(RIGHTOF17-ROBOT)
)
:effect
(and
(Flag270-)
(Flag270prime-)
)

)
(:action Flag270Action-2
:parameters ()
:precondition
(and
(RIGHTOF14-ROBOT)
)
:effect
(and
(Flag270-)
(Flag270prime-)
)

)
(:action Flag270Action-3
:parameters ()
:precondition
(and
(RIGHTOF15-ROBOT)
)
:effect
(and
(Flag270-)
(Flag270prime-)
)

)
(:action Prim270Action
:parameters ()
:precondition
(and
(not (RIGHTOF16-ROBOT))
(not (RIGHTOF17-ROBOT))
(not (RIGHTOF14-ROBOT))
(not (RIGHTOF15-ROBOT))
)
:effect
(and
(Flag270prime-)
(not (Flag270-))
)

)
(:action Flag271Action-0
:parameters ()
:precondition
(and
(RIGHTOF16-ROBOT)
)
:effect
(and
(Flag271-)
(Flag271prime-)
)

)
(:action Flag271Action-1
:parameters ()
:precondition
(and
(RIGHTOF17-ROBOT)
)
:effect
(and
(Flag271-)
(Flag271prime-)
)

)
(:action Flag271Action-2
:parameters ()
:precondition
(and
(RIGHTOF15-ROBOT)
)
:effect
(and
(Flag271-)
(Flag271prime-)
)

)
(:action Prim271Action
:parameters ()
:precondition
(and
(not (RIGHTOF16-ROBOT))
(not (RIGHTOF17-ROBOT))
(not (RIGHTOF15-ROBOT))
)
:effect
(and
(Flag271prime-)
(not (Flag271-))
)

)
(:action Flag272Action-0
:parameters ()
:precondition
(and
(RIGHTOF16-ROBOT)
)
:effect
(and
(Flag272-)
(Flag272prime-)
)

)
(:action Flag272Action-1
:parameters ()
:precondition
(and
(RIGHTOF17-ROBOT)
)
:effect
(and
(Flag272-)
(Flag272prime-)
)

)
(:action Prim272Action
:parameters ()
:precondition
(and
(not (RIGHTOF16-ROBOT))
(not (RIGHTOF17-ROBOT))
)
:effect
(and
(Flag272prime-)
(not (Flag272-))
)

)
(:action Flag273Action
:parameters ()
:precondition
(and
(LEFTOF2-ROBOT)
(RIGHTOF15-ROBOT)
)
:effect
(and
(Flag273-)
(Flag273prime-)
)

)
(:action Flag274Action
:parameters ()
:precondition
(and
(RIGHTOF13-ROBOT)
(LEFTOF2-ROBOT)
)
:effect
(and
(Flag274-)
(Flag274prime-)
)

)
(:action Flag275Action
:parameters ()
:precondition
(and
(LEFTOF2-ROBOT)
(RIGHTOF9-ROBOT)
)
:effect
(and
(Flag275-)
(Flag275prime-)
)

)
(:action Flag276Action
:parameters ()
:precondition
(and
(LEFTOF1-ROBOT)
(RIGHTOF9-ROBOT)
)
:effect
(and
(Flag276-)
(Flag276prime-)
)

)
(:action Flag277Action
:parameters ()
:precondition
(and
(RIGHTOF3-ROBOT)
(LEFTOF1-ROBOT)
)
:effect
(and
(Flag277-)
(Flag277prime-)
)

)
(:action Flag278Action
:parameters ()
:precondition
(and
(LEFTOF1-ROBOT)
(RIGHTOF15-ROBOT)
)
:effect
(and
(Flag278-)
(Flag278prime-)
)

)
(:action Flag279Action
:parameters ()
:precondition
(and
(RIGHTOF10-ROBOT)
(LEFTOF1-ROBOT)
)
:effect
(and
(Flag279-)
(Flag279prime-)
)

)
(:action Flag280Action
:parameters ()
:precondition
(and
(LEFTOF2-ROBOT)
(RIGHTOF1-ROBOT)
)
:effect
(and
(Flag280-)
(Flag280prime-)
)

)
(:action Flag281Action
:parameters ()
:precondition
(and
(RIGHTOF5-ROBOT)
(LEFTOF2-ROBOT)
)
:effect
(and
(Flag281-)
(Flag281prime-)
)

)
(:action Flag282Action
:parameters ()
:precondition
(and
(LEFTOF1-ROBOT)
(RIGHTOF1-ROBOT)
)
:effect
(and
(Flag282-)
(Flag282prime-)
)

)
(:action Flag283Action
:parameters ()
:precondition
(and
(LEFTOF2-ROBOT)
(RIGHTOF7-ROBOT)
)
:effect
(and
(Flag283-)
(Flag283prime-)
)

)
(:action Flag284Action
:parameters ()
:precondition
(and
(RIGHTOF16-ROBOT)
(LEFTOF2-ROBOT)
)
:effect
(and
(Flag284-)
(Flag284prime-)
)

)
(:action Flag285Action
:parameters ()
:precondition
(and
(RIGHTOF5-ROBOT)
(LEFTOF1-ROBOT)
)
:effect
(and
(Flag285-)
(Flag285prime-)
)

)
(:action Flag286Action
:parameters ()
:precondition
(and
(RIGHTOF11-ROBOT)
(LEFTOF1-ROBOT)
)
:effect
(and
(Flag286-)
(Flag286prime-)
)

)
(:action Flag287Action
:parameters ()
:precondition
(and
(RIGHTOF3-ROBOT)
(LEFTOF2-ROBOT)
)
:effect
(and
(Flag287-)
(Flag287prime-)
)

)
(:action Flag288Action
:parameters ()
:precondition
(and
(LEFTOF2-ROBOT)
(RIGHTOF17-ROBOT)
)
:effect
(and
(Flag288-)
(Flag288prime-)
)

)
(:action Flag289Action
:parameters ()
:precondition
(and
(LEFTOF2-ROBOT)
(RIGHTOF14-ROBOT)
)
:effect
(and
(Flag289-)
(Flag289prime-)
)

)
(:action Flag290Action
:parameters ()
:precondition
(and
(LEFTOF1-ROBOT)
(RIGHTOF4-ROBOT)
)
:effect
(and
(Flag290-)
(Flag290prime-)
)

)
(:action Flag291Action
:parameters ()
:precondition
(and
(RIGHTOF10-ROBOT)
(LEFTOF2-ROBOT)
)
:effect
(and
(Flag291-)
(Flag291prime-)
)

)
(:action Flag292Action
:parameters ()
:precondition
(and
(LEFTOF1-ROBOT)
(RIGHTOF12-ROBOT)
)
:effect
(and
(Flag292-)
(Flag292prime-)
)

)
(:action Flag293Action
:parameters ()
:precondition
(and
(LEFTOF1-ROBOT)
(RIGHTOF7-ROBOT)
)
:effect
(and
(Flag293-)
(Flag293prime-)
)

)
(:action Flag294Action
:parameters ()
:precondition
(and
(LEFTOF2-ROBOT)
(RIGHTOF12-ROBOT)
)
:effect
(and
(Flag294-)
(Flag294prime-)
)

)
(:action Flag295Action
:parameters ()
:precondition
(and
(LEFTOF1-ROBOT)
(RIGHTOF5-ROBOT)
)
:effect
(and
(Flag295-)
(Flag295prime-)
)

)
(:action Flag296Action
:parameters ()
:precondition
(and
(RIGHTOF8-ROBOT)
(LEFTOF1-ROBOT)
)
:effect
(and
(Flag296-)
(Flag296prime-)
)

)
(:action Flag297Action
:parameters ()
:precondition
(and
(LEFTOF1-ROBOT)
(RIGHTOF17-ROBOT)
)
:effect
(and
(Flag297-)
(Flag297prime-)
)

)
(:action Flag298Action
:parameters ()
:precondition
(and
(RIGHTOF13-ROBOT)
(LEFTOF1-ROBOT)
)
:effect
(and
(Flag298-)
(Flag298prime-)
)

)
(:action Flag299Action
:parameters ()
:precondition
(and
(RIGHTOF11-ROBOT)
(LEFTOF2-ROBOT)
)
:effect
(and
(Flag299-)
(Flag299prime-)
)

)
(:action Flag300Action
:parameters ()
:precondition
(and
(RIGHTOF8-ROBOT)
(LEFTOF2-ROBOT)
)
:effect
(and
(Flag300-)
(Flag300prime-)
)

)
(:action Flag301Action
:parameters ()
:precondition
(and
(RIGHTOF2-ROBOT)
(LEFTOF2-ROBOT)
)
:effect
(and
(Flag301-)
(Flag301prime-)
)

)
(:action Flag302Action
:parameters ()
:precondition
(and
(LEFTOF1-ROBOT)
(RIGHTOF2-ROBOT)
)
:effect
(and
(Flag302-)
(Flag302prime-)
)

)
(:action Flag303Action
:parameters ()
:precondition
(and
(LEFTOF1-ROBOT)
(RIGHTOF6-ROBOT)
)
:effect
(and
(Flag303-)
(Flag303prime-)
)

)
(:action Flag304Action
:parameters ()
:precondition
(and
(LEFTOF1-ROBOT)
(RIGHTOF14-ROBOT)
)
:effect
(and
(Flag304-)
(Flag304prime-)
)

)
(:action Flag305Action
:parameters ()
:precondition
(and
(LEFTOF1-ROBOT)
(RIGHTOF16-ROBOT)
)
:effect
(and
(Flag305-)
(Flag305prime-)
)

)
(:action Flag306Action
:parameters ()
:precondition
(and
(LEFTOF2-ROBOT)
(RIGHTOF6-ROBOT)
)
:effect
(and
(Flag306-)
(Flag306prime-)
)

)
(:action Flag307Action
:parameters ()
:precondition
(and
(LEFTOF2-ROBOT)
(RIGHTOF4-ROBOT)
)
:effect
(and
(Flag307-)
(Flag307prime-)
)

)
(:action Prim273Action-0
:parameters ()
:precondition
(and
(not (LEFTOF2-ROBOT))
)
:effect
(and
(Flag273prime-)
(not (Flag273-))
)

)
(:action Prim273Action-1
:parameters ()
:precondition
(and
(not (RIGHTOF15-ROBOT))
)
:effect
(and
(Flag273prime-)
(not (Flag273-))
)

)
(:action Prim274Action-0
:parameters ()
:precondition
(and
(not (RIGHTOF13-ROBOT))
)
:effect
(and
(Flag274prime-)
(not (Flag274-))
)

)
(:action Prim274Action-1
:parameters ()
:precondition
(and
(not (LEFTOF2-ROBOT))
)
:effect
(and
(Flag274prime-)
(not (Flag274-))
)

)
(:action Prim275Action-0
:parameters ()
:precondition
(and
(not (LEFTOF2-ROBOT))
)
:effect
(and
(Flag275prime-)
(not (Flag275-))
)

)
(:action Prim275Action-1
:parameters ()
:precondition
(and
(not (RIGHTOF9-ROBOT))
)
:effect
(and
(Flag275prime-)
(not (Flag275-))
)

)
(:action Prim276Action-0
:parameters ()
:precondition
(and
(not (LEFTOF1-ROBOT))
)
:effect
(and
(Flag276prime-)
(not (Flag276-))
)

)
(:action Prim276Action-1
:parameters ()
:precondition
(and
(not (RIGHTOF9-ROBOT))
)
:effect
(and
(Flag276prime-)
(not (Flag276-))
)

)
(:action Prim277Action-0
:parameters ()
:precondition
(and
(not (RIGHTOF3-ROBOT))
)
:effect
(and
(Flag277prime-)
(not (Flag277-))
)

)
(:action Prim277Action-1
:parameters ()
:precondition
(and
(not (LEFTOF1-ROBOT))
)
:effect
(and
(Flag277prime-)
(not (Flag277-))
)

)
(:action Prim278Action-0
:parameters ()
:precondition
(and
(not (LEFTOF1-ROBOT))
)
:effect
(and
(Flag278prime-)
(not (Flag278-))
)

)
(:action Prim278Action-1
:parameters ()
:precondition
(and
(not (RIGHTOF15-ROBOT))
)
:effect
(and
(Flag278prime-)
(not (Flag278-))
)

)
(:action Prim279Action-0
:parameters ()
:precondition
(and
(not (RIGHTOF10-ROBOT))
)
:effect
(and
(Flag279prime-)
(not (Flag279-))
)

)
(:action Prim279Action-1
:parameters ()
:precondition
(and
(not (LEFTOF1-ROBOT))
)
:effect
(and
(Flag279prime-)
(not (Flag279-))
)

)
(:action Prim280Action-0
:parameters ()
:precondition
(and
(not (LEFTOF2-ROBOT))
)
:effect
(and
(Flag280prime-)
(not (Flag280-))
)

)
(:action Prim280Action-1
:parameters ()
:precondition
(and
(not (RIGHTOF1-ROBOT))
)
:effect
(and
(Flag280prime-)
(not (Flag280-))
)

)
(:action Prim281Action-0
:parameters ()
:precondition
(and
(not (RIGHTOF5-ROBOT))
)
:effect
(and
(Flag281prime-)
(not (Flag281-))
)

)
(:action Prim281Action-1
:parameters ()
:precondition
(and
(not (LEFTOF2-ROBOT))
)
:effect
(and
(Flag281prime-)
(not (Flag281-))
)

)
(:action Prim282Action-0
:parameters ()
:precondition
(and
(not (LEFTOF1-ROBOT))
)
:effect
(and
(Flag282prime-)
(not (Flag282-))
)

)
(:action Prim282Action-1
:parameters ()
:precondition
(and
(not (RIGHTOF1-ROBOT))
)
:effect
(and
(Flag282prime-)
(not (Flag282-))
)

)
(:action Prim283Action-0
:parameters ()
:precondition
(and
(not (LEFTOF2-ROBOT))
)
:effect
(and
(Flag283prime-)
(not (Flag283-))
)

)
(:action Prim283Action-1
:parameters ()
:precondition
(and
(not (RIGHTOF7-ROBOT))
)
:effect
(and
(Flag283prime-)
(not (Flag283-))
)

)
(:action Prim284Action-0
:parameters ()
:precondition
(and
(not (RIGHTOF16-ROBOT))
)
:effect
(and
(Flag284prime-)
(not (Flag284-))
)

)
(:action Prim284Action-1
:parameters ()
:precondition
(and
(not (LEFTOF2-ROBOT))
)
:effect
(and
(Flag284prime-)
(not (Flag284-))
)

)
(:action Prim285Action-0
:parameters ()
:precondition
(and
(not (RIGHTOF5-ROBOT))
)
:effect
(and
(Flag285prime-)
(not (Flag285-))
)

)
(:action Prim285Action-1
:parameters ()
:precondition
(and
(not (LEFTOF1-ROBOT))
)
:effect
(and
(Flag285prime-)
(not (Flag285-))
)

)
(:action Prim286Action-0
:parameters ()
:precondition
(and
(not (RIGHTOF11-ROBOT))
)
:effect
(and
(Flag286prime-)
(not (Flag286-))
)

)
(:action Prim286Action-1
:parameters ()
:precondition
(and
(not (LEFTOF1-ROBOT))
)
:effect
(and
(Flag286prime-)
(not (Flag286-))
)

)
(:action Prim287Action-0
:parameters ()
:precondition
(and
(not (RIGHTOF3-ROBOT))
)
:effect
(and
(Flag287prime-)
(not (Flag287-))
)

)
(:action Prim287Action-1
:parameters ()
:precondition
(and
(not (LEFTOF2-ROBOT))
)
:effect
(and
(Flag287prime-)
(not (Flag287-))
)

)
(:action Prim288Action-0
:parameters ()
:precondition
(and
(not (LEFTOF2-ROBOT))
)
:effect
(and
(Flag288prime-)
(not (Flag288-))
)

)
(:action Prim288Action-1
:parameters ()
:precondition
(and
(not (RIGHTOF17-ROBOT))
)
:effect
(and
(Flag288prime-)
(not (Flag288-))
)

)
(:action Prim289Action-0
:parameters ()
:precondition
(and
(not (LEFTOF2-ROBOT))
)
:effect
(and
(Flag289prime-)
(not (Flag289-))
)

)
(:action Prim289Action-1
:parameters ()
:precondition
(and
(not (RIGHTOF14-ROBOT))
)
:effect
(and
(Flag289prime-)
(not (Flag289-))
)

)
(:action Prim290Action-0
:parameters ()
:precondition
(and
(not (LEFTOF1-ROBOT))
)
:effect
(and
(Flag290prime-)
(not (Flag290-))
)

)
(:action Prim290Action-1
:parameters ()
:precondition
(and
(not (RIGHTOF4-ROBOT))
)
:effect
(and
(Flag290prime-)
(not (Flag290-))
)

)
(:action Prim291Action-0
:parameters ()
:precondition
(and
(not (RIGHTOF10-ROBOT))
)
:effect
(and
(Flag291prime-)
(not (Flag291-))
)

)
(:action Prim291Action-1
:parameters ()
:precondition
(and
(not (LEFTOF2-ROBOT))
)
:effect
(and
(Flag291prime-)
(not (Flag291-))
)

)
(:action Prim292Action-0
:parameters ()
:precondition
(and
(not (LEFTOF1-ROBOT))
)
:effect
(and
(Flag292prime-)
(not (Flag292-))
)

)
(:action Prim292Action-1
:parameters ()
:precondition
(and
(not (RIGHTOF12-ROBOT))
)
:effect
(and
(Flag292prime-)
(not (Flag292-))
)

)
(:action Prim293Action-0
:parameters ()
:precondition
(and
(not (LEFTOF1-ROBOT))
)
:effect
(and
(Flag293prime-)
(not (Flag293-))
)

)
(:action Prim293Action-1
:parameters ()
:precondition
(and
(not (RIGHTOF7-ROBOT))
)
:effect
(and
(Flag293prime-)
(not (Flag293-))
)

)
(:action Prim294Action-0
:parameters ()
:precondition
(and
(not (LEFTOF2-ROBOT))
)
:effect
(and
(Flag294prime-)
(not (Flag294-))
)

)
(:action Prim294Action-1
:parameters ()
:precondition
(and
(not (RIGHTOF12-ROBOT))
)
:effect
(and
(Flag294prime-)
(not (Flag294-))
)

)
(:action Prim295Action-0
:parameters ()
:precondition
(and
(not (LEFTOF1-ROBOT))
)
:effect
(and
(Flag295prime-)
(not (Flag295-))
)

)
(:action Prim295Action-1
:parameters ()
:precondition
(and
(not (RIGHTOF5-ROBOT))
)
:effect
(and
(Flag295prime-)
(not (Flag295-))
)

)
(:action Prim296Action-0
:parameters ()
:precondition
(and
(not (RIGHTOF8-ROBOT))
)
:effect
(and
(Flag296prime-)
(not (Flag296-))
)

)
(:action Prim296Action-1
:parameters ()
:precondition
(and
(not (LEFTOF1-ROBOT))
)
:effect
(and
(Flag296prime-)
(not (Flag296-))
)

)
(:action Prim297Action-0
:parameters ()
:precondition
(and
(not (LEFTOF1-ROBOT))
)
:effect
(and
(Flag297prime-)
(not (Flag297-))
)

)
(:action Prim297Action-1
:parameters ()
:precondition
(and
(not (RIGHTOF17-ROBOT))
)
:effect
(and
(Flag297prime-)
(not (Flag297-))
)

)
(:action Prim298Action-0
:parameters ()
:precondition
(and
(not (RIGHTOF13-ROBOT))
)
:effect
(and
(Flag298prime-)
(not (Flag298-))
)

)
(:action Prim298Action-1
:parameters ()
:precondition
(and
(not (LEFTOF1-ROBOT))
)
:effect
(and
(Flag298prime-)
(not (Flag298-))
)

)
(:action Prim299Action-0
:parameters ()
:precondition
(and
(not (RIGHTOF11-ROBOT))
)
:effect
(and
(Flag299prime-)
(not (Flag299-))
)

)
(:action Prim299Action-1
:parameters ()
:precondition
(and
(not (LEFTOF2-ROBOT))
)
:effect
(and
(Flag299prime-)
(not (Flag299-))
)

)
(:action Prim300Action-0
:parameters ()
:precondition
(and
(not (RIGHTOF8-ROBOT))
)
:effect
(and
(Flag300prime-)
(not (Flag300-))
)

)
(:action Prim300Action-1
:parameters ()
:precondition
(and
(not (LEFTOF2-ROBOT))
)
:effect
(and
(Flag300prime-)
(not (Flag300-))
)

)
(:action Prim301Action-1
:parameters ()
:precondition
(and
(not (LEFTOF2-ROBOT))
)
:effect
(and
(Flag301prime-)
(not (Flag301-))
)

)
(:action Prim302Action-0
:parameters ()
:precondition
(and
(not (LEFTOF1-ROBOT))
)
:effect
(and
(Flag302prime-)
(not (Flag302-))
)

)
(:action Prim303Action-0
:parameters ()
:precondition
(and
(not (LEFTOF1-ROBOT))
)
:effect
(and
(Flag303prime-)
(not (Flag303-))
)

)
(:action Prim303Action-1
:parameters ()
:precondition
(and
(not (RIGHTOF6-ROBOT))
)
:effect
(and
(Flag303prime-)
(not (Flag303-))
)

)
(:action Prim304Action-0
:parameters ()
:precondition
(and
(not (LEFTOF1-ROBOT))
)
:effect
(and
(Flag304prime-)
(not (Flag304-))
)

)
(:action Prim304Action-1
:parameters ()
:precondition
(and
(not (RIGHTOF14-ROBOT))
)
:effect
(and
(Flag304prime-)
(not (Flag304-))
)

)
(:action Prim305Action-0
:parameters ()
:precondition
(and
(not (LEFTOF1-ROBOT))
)
:effect
(and
(Flag305prime-)
(not (Flag305-))
)

)
(:action Prim305Action-1
:parameters ()
:precondition
(and
(not (RIGHTOF16-ROBOT))
)
:effect
(and
(Flag305prime-)
(not (Flag305-))
)

)
(:action Prim306Action-0
:parameters ()
:precondition
(and
(not (LEFTOF2-ROBOT))
)
:effect
(and
(Flag306prime-)
(not (Flag306-))
)

)
(:action Prim306Action-1
:parameters ()
:precondition
(and
(not (RIGHTOF6-ROBOT))
)
:effect
(and
(Flag306prime-)
(not (Flag306-))
)

)
(:action Prim307Action-0
:parameters ()
:precondition
(and
(not (LEFTOF2-ROBOT))
)
:effect
(and
(Flag307prime-)
(not (Flag307-))
)

)
(:action Prim307Action-1
:parameters ()
:precondition
(and
(not (RIGHTOF4-ROBOT))
)
:effect
(and
(Flag307prime-)
(not (Flag307-))
)

)
(:action Flag309Action
:parameters ()
:precondition
(and
(LEFTOF3-ROBOT)
(RIGHTOF6-ROBOT)
)
:effect
(and
(Flag309-)
(Flag309prime-)
)

)
(:action Flag310Action
:parameters ()
:precondition
(and
(LEFTOF3-ROBOT)
(RIGHTOF14-ROBOT)
)
:effect
(and
(Flag310-)
(Flag310prime-)
)

)
(:action Flag311Action
:parameters ()
:precondition
(and
(LEFTOF3-ROBOT)
(RIGHTOF4-ROBOT)
)
:effect
(and
(Flag311-)
(Flag311prime-)
)

)
(:action Flag312Action
:parameters ()
:precondition
(and
(LEFTOF3-ROBOT)
(RIGHTOF16-ROBOT)
)
:effect
(and
(Flag312-)
(Flag312prime-)
)

)
(:action Flag313Action
:parameters ()
:precondition
(and
(LEFTOF3-ROBOT)
(RIGHTOF7-ROBOT)
)
:effect
(and
(Flag313-)
(Flag313prime-)
)

)
(:action Flag314Action
:parameters ()
:precondition
(and
(LEFTOF3-ROBOT)
(RIGHTOF12-ROBOT)
)
:effect
(and
(Flag314-)
(Flag314prime-)
)

)
(:action Flag315Action
:parameters ()
:precondition
(and
(LEFTOF3-ROBOT)
(RIGHTOF10-ROBOT)
)
:effect
(and
(Flag315-)
(Flag315prime-)
)

)
(:action Flag316Action
:parameters ()
:precondition
(and
(LEFTOF3-ROBOT)
(RIGHTOF8-ROBOT)
)
:effect
(and
(Flag316-)
(Flag316prime-)
)

)
(:action Flag317Action
:parameters ()
:precondition
(and
(LEFTOF3-ROBOT)
(RIGHTOF5-ROBOT)
)
:effect
(and
(Flag317-)
(Flag317prime-)
)

)
(:action Flag318Action
:parameters ()
:precondition
(and
(LEFTOF3-ROBOT)
(RIGHTOF15-ROBOT)
)
:effect
(and
(Flag318-)
(Flag318prime-)
)

)
(:action Flag319Action
:parameters ()
:precondition
(and
(RIGHTOF3-ROBOT)
(LEFTOF3-ROBOT)
)
:effect
(and
(Flag319-)
(Flag319prime-)
)

)
(:action Flag320Action
:parameters ()
:precondition
(and
(LEFTOF3-ROBOT)
(RIGHTOF11-ROBOT)
)
:effect
(and
(Flag320-)
(Flag320prime-)
)

)
(:action Flag321Action
:parameters ()
:precondition
(and
(RIGHTOF13-ROBOT)
(LEFTOF3-ROBOT)
)
:effect
(and
(Flag321-)
(Flag321prime-)
)

)
(:action Flag322Action
:parameters ()
:precondition
(and
(LEFTOF3-ROBOT)
(RIGHTOF17-ROBOT)
)
:effect
(and
(Flag322-)
(Flag322prime-)
)

)
(:action Flag323Action
:parameters ()
:precondition
(and
(LEFTOF3-ROBOT)
(RIGHTOF9-ROBOT)
)
:effect
(and
(Flag323-)
(Flag323prime-)
)

)
(:action Flag324Action
:parameters ()
:precondition
(and
(LEFTOF3-ROBOT)
(RIGHTOF2-ROBOT)
)
:effect
(and
(Flag324-)
(Flag324prime-)
)

)
(:action Prim309Action-0
:parameters ()
:precondition
(and
(not (LEFTOF3-ROBOT))
)
:effect
(and
(Flag309prime-)
(not (Flag309-))
)

)
(:action Prim309Action-1
:parameters ()
:precondition
(and
(not (RIGHTOF6-ROBOT))
)
:effect
(and
(Flag309prime-)
(not (Flag309-))
)

)
(:action Prim310Action-0
:parameters ()
:precondition
(and
(not (LEFTOF3-ROBOT))
)
:effect
(and
(Flag310prime-)
(not (Flag310-))
)

)
(:action Prim310Action-1
:parameters ()
:precondition
(and
(not (RIGHTOF14-ROBOT))
)
:effect
(and
(Flag310prime-)
(not (Flag310-))
)

)
(:action Prim311Action-0
:parameters ()
:precondition
(and
(not (LEFTOF3-ROBOT))
)
:effect
(and
(Flag311prime-)
(not (Flag311-))
)

)
(:action Prim311Action-1
:parameters ()
:precondition
(and
(not (RIGHTOF4-ROBOT))
)
:effect
(and
(Flag311prime-)
(not (Flag311-))
)

)
(:action Prim312Action-0
:parameters ()
:precondition
(and
(not (LEFTOF3-ROBOT))
)
:effect
(and
(Flag312prime-)
(not (Flag312-))
)

)
(:action Prim312Action-1
:parameters ()
:precondition
(and
(not (RIGHTOF16-ROBOT))
)
:effect
(and
(Flag312prime-)
(not (Flag312-))
)

)
(:action Prim313Action-0
:parameters ()
:precondition
(and
(not (LEFTOF3-ROBOT))
)
:effect
(and
(Flag313prime-)
(not (Flag313-))
)

)
(:action Prim313Action-1
:parameters ()
:precondition
(and
(not (RIGHTOF7-ROBOT))
)
:effect
(and
(Flag313prime-)
(not (Flag313-))
)

)
(:action Prim314Action-0
:parameters ()
:precondition
(and
(not (LEFTOF3-ROBOT))
)
:effect
(and
(Flag314prime-)
(not (Flag314-))
)

)
(:action Prim314Action-1
:parameters ()
:precondition
(and
(not (RIGHTOF12-ROBOT))
)
:effect
(and
(Flag314prime-)
(not (Flag314-))
)

)
(:action Prim315Action-0
:parameters ()
:precondition
(and
(not (LEFTOF3-ROBOT))
)
:effect
(and
(Flag315prime-)
(not (Flag315-))
)

)
(:action Prim315Action-1
:parameters ()
:precondition
(and
(not (RIGHTOF10-ROBOT))
)
:effect
(and
(Flag315prime-)
(not (Flag315-))
)

)
(:action Prim316Action-0
:parameters ()
:precondition
(and
(not (LEFTOF3-ROBOT))
)
:effect
(and
(Flag316prime-)
(not (Flag316-))
)

)
(:action Prim316Action-1
:parameters ()
:precondition
(and
(not (RIGHTOF8-ROBOT))
)
:effect
(and
(Flag316prime-)
(not (Flag316-))
)

)
(:action Prim317Action-0
:parameters ()
:precondition
(and
(not (LEFTOF3-ROBOT))
)
:effect
(and
(Flag317prime-)
(not (Flag317-))
)

)
(:action Prim317Action-1
:parameters ()
:precondition
(and
(not (RIGHTOF5-ROBOT))
)
:effect
(and
(Flag317prime-)
(not (Flag317-))
)

)
(:action Prim318Action-0
:parameters ()
:precondition
(and
(not (LEFTOF3-ROBOT))
)
:effect
(and
(Flag318prime-)
(not (Flag318-))
)

)
(:action Prim318Action-1
:parameters ()
:precondition
(and
(not (RIGHTOF15-ROBOT))
)
:effect
(and
(Flag318prime-)
(not (Flag318-))
)

)
(:action Prim319Action-0
:parameters ()
:precondition
(and
(not (RIGHTOF3-ROBOT))
)
:effect
(and
(Flag319prime-)
(not (Flag319-))
)

)
(:action Prim319Action-1
:parameters ()
:precondition
(and
(not (LEFTOF3-ROBOT))
)
:effect
(and
(Flag319prime-)
(not (Flag319-))
)

)
(:action Prim320Action-0
:parameters ()
:precondition
(and
(not (LEFTOF3-ROBOT))
)
:effect
(and
(Flag320prime-)
(not (Flag320-))
)

)
(:action Prim320Action-1
:parameters ()
:precondition
(and
(not (RIGHTOF11-ROBOT))
)
:effect
(and
(Flag320prime-)
(not (Flag320-))
)

)
(:action Prim321Action-0
:parameters ()
:precondition
(and
(not (RIGHTOF13-ROBOT))
)
:effect
(and
(Flag321prime-)
(not (Flag321-))
)

)
(:action Prim321Action-1
:parameters ()
:precondition
(and
(not (LEFTOF3-ROBOT))
)
:effect
(and
(Flag321prime-)
(not (Flag321-))
)

)
(:action Prim322Action-0
:parameters ()
:precondition
(and
(not (LEFTOF3-ROBOT))
)
:effect
(and
(Flag322prime-)
(not (Flag322-))
)

)
(:action Prim322Action-1
:parameters ()
:precondition
(and
(not (RIGHTOF17-ROBOT))
)
:effect
(and
(Flag322prime-)
(not (Flag322-))
)

)
(:action Prim323Action-0
:parameters ()
:precondition
(and
(not (LEFTOF3-ROBOT))
)
:effect
(and
(Flag323prime-)
(not (Flag323-))
)

)
(:action Prim323Action-1
:parameters ()
:precondition
(and
(not (RIGHTOF9-ROBOT))
)
:effect
(and
(Flag323prime-)
(not (Flag323-))
)

)
(:action Prim324Action-0
:parameters ()
:precondition
(and
(not (LEFTOF3-ROBOT))
)
:effect
(and
(Flag324prime-)
(not (Flag324-))
)

)
(:action Flag326Action
:parameters ()
:precondition
(and
(LEFTOF3-ROBOT)
(RIGHTOF13-ROBOT)
)
:effect
(and
(Flag326-)
(Flag326prime-)
)

)
(:action Flag327Action
:parameters ()
:precondition
(and
(LEFTOF4-ROBOT)
(RIGHTOF17-ROBOT)
)
:effect
(and
(Flag327-)
(Flag327prime-)
)

)
(:action Flag328Action
:parameters ()
:precondition
(and
(LEFTOF4-ROBOT)
(RIGHTOF4-ROBOT)
)
:effect
(and
(Flag328-)
(Flag328prime-)
)

)
(:action Flag329Action
:parameters ()
:precondition
(and
(LEFTOF4-ROBOT)
(RIGHTOF7-ROBOT)
)
:effect
(and
(Flag329-)
(Flag329prime-)
)

)
(:action Flag330Action
:parameters ()
:precondition
(and
(LEFTOF4-ROBOT)
(RIGHTOF15-ROBOT)
)
:effect
(and
(Flag330-)
(Flag330prime-)
)

)
(:action Flag331Action
:parameters ()
:precondition
(and
(LEFTOF4-ROBOT)
(RIGHTOF14-ROBOT)
)
:effect
(and
(Flag331-)
(Flag331prime-)
)

)
(:action Flag332Action
:parameters ()
:precondition
(and
(LEFTOF3-ROBOT)
(RIGHTOF3-ROBOT)
)
:effect
(and
(Flag332-)
(Flag332prime-)
)

)
(:action Flag333Action
:parameters ()
:precondition
(and
(RIGHTOF8-ROBOT)
(LEFTOF4-ROBOT)
)
:effect
(and
(Flag333-)
(Flag333prime-)
)

)
(:action Flag334Action
:parameters ()
:precondition
(and
(LEFTOF4-ROBOT)
(RIGHTOF12-ROBOT)
)
:effect
(and
(Flag334-)
(Flag334prime-)
)

)
(:action Flag335Action
:parameters ()
:precondition
(and
(RIGHTOF10-ROBOT)
(LEFTOF4-ROBOT)
)
:effect
(and
(Flag335-)
(Flag335prime-)
)

)
(:action Flag336Action
:parameters ()
:precondition
(and
(LEFTOF4-ROBOT)
(RIGHTOF9-ROBOT)
)
:effect
(and
(Flag336-)
(Flag336prime-)
)

)
(:action Flag337Action
:parameters ()
:precondition
(and
(LEFTOF4-ROBOT)
(RIGHTOF16-ROBOT)
)
:effect
(and
(Flag337-)
(Flag337prime-)
)

)
(:action Flag338Action
:parameters ()
:precondition
(and
(LEFTOF4-ROBOT)
(RIGHTOF6-ROBOT)
)
:effect
(and
(Flag338-)
(Flag338prime-)
)

)
(:action Flag339Action
:parameters ()
:precondition
(and
(LEFTOF4-ROBOT)
(RIGHTOF5-ROBOT)
)
:effect
(and
(Flag339-)
(Flag339prime-)
)

)
(:action Flag340Action
:parameters ()
:precondition
(and
(RIGHTOF13-ROBOT)
(LEFTOF4-ROBOT)
)
:effect
(and
(Flag340-)
(Flag340prime-)
)

)
(:action Flag341Action
:parameters ()
:precondition
(and
(RIGHTOF3-ROBOT)
(LEFTOF4-ROBOT)
)
:effect
(and
(Flag341-)
(Flag341prime-)
)

)
(:action Flag342Action
:parameters ()
:precondition
(and
(LEFTOF4-ROBOT)
(RIGHTOF11-ROBOT)
)
:effect
(and
(Flag342-)
(Flag342prime-)
)

)
(:action Prim326Action-0
:parameters ()
:precondition
(and
(not (LEFTOF3-ROBOT))
)
:effect
(and
(Flag326prime-)
(not (Flag326-))
)

)
(:action Prim326Action-1
:parameters ()
:precondition
(and
(not (RIGHTOF13-ROBOT))
)
:effect
(and
(Flag326prime-)
(not (Flag326-))
)

)
(:action Prim327Action-0
:parameters ()
:precondition
(and
(not (LEFTOF4-ROBOT))
)
:effect
(and
(Flag327prime-)
(not (Flag327-))
)

)
(:action Prim327Action-1
:parameters ()
:precondition
(and
(not (RIGHTOF17-ROBOT))
)
:effect
(and
(Flag327prime-)
(not (Flag327-))
)

)
(:action Prim328Action-0
:parameters ()
:precondition
(and
(not (LEFTOF4-ROBOT))
)
:effect
(and
(Flag328prime-)
(not (Flag328-))
)

)
(:action Prim328Action-1
:parameters ()
:precondition
(and
(not (RIGHTOF4-ROBOT))
)
:effect
(and
(Flag328prime-)
(not (Flag328-))
)

)
(:action Prim329Action-0
:parameters ()
:precondition
(and
(not (LEFTOF4-ROBOT))
)
:effect
(and
(Flag329prime-)
(not (Flag329-))
)

)
(:action Prim329Action-1
:parameters ()
:precondition
(and
(not (RIGHTOF7-ROBOT))
)
:effect
(and
(Flag329prime-)
(not (Flag329-))
)

)
(:action Prim330Action-0
:parameters ()
:precondition
(and
(not (LEFTOF4-ROBOT))
)
:effect
(and
(Flag330prime-)
(not (Flag330-))
)

)
(:action Prim330Action-1
:parameters ()
:precondition
(and
(not (RIGHTOF15-ROBOT))
)
:effect
(and
(Flag330prime-)
(not (Flag330-))
)

)
(:action Prim331Action-0
:parameters ()
:precondition
(and
(not (LEFTOF4-ROBOT))
)
:effect
(and
(Flag331prime-)
(not (Flag331-))
)

)
(:action Prim331Action-1
:parameters ()
:precondition
(and
(not (RIGHTOF14-ROBOT))
)
:effect
(and
(Flag331prime-)
(not (Flag331-))
)

)
(:action Prim332Action-0
:parameters ()
:precondition
(and
(not (LEFTOF3-ROBOT))
)
:effect
(and
(Flag332prime-)
(not (Flag332-))
)

)
(:action Prim332Action-1
:parameters ()
:precondition
(and
(not (RIGHTOF3-ROBOT))
)
:effect
(and
(Flag332prime-)
(not (Flag332-))
)

)
(:action Prim333Action-0
:parameters ()
:precondition
(and
(not (RIGHTOF8-ROBOT))
)
:effect
(and
(Flag333prime-)
(not (Flag333-))
)

)
(:action Prim333Action-1
:parameters ()
:precondition
(and
(not (LEFTOF4-ROBOT))
)
:effect
(and
(Flag333prime-)
(not (Flag333-))
)

)
(:action Prim334Action-0
:parameters ()
:precondition
(and
(not (LEFTOF4-ROBOT))
)
:effect
(and
(Flag334prime-)
(not (Flag334-))
)

)
(:action Prim334Action-1
:parameters ()
:precondition
(and
(not (RIGHTOF12-ROBOT))
)
:effect
(and
(Flag334prime-)
(not (Flag334-))
)

)
(:action Prim335Action-0
:parameters ()
:precondition
(and
(not (RIGHTOF10-ROBOT))
)
:effect
(and
(Flag335prime-)
(not (Flag335-))
)

)
(:action Prim335Action-1
:parameters ()
:precondition
(and
(not (LEFTOF4-ROBOT))
)
:effect
(and
(Flag335prime-)
(not (Flag335-))
)

)
(:action Prim336Action-0
:parameters ()
:precondition
(and
(not (LEFTOF4-ROBOT))
)
:effect
(and
(Flag336prime-)
(not (Flag336-))
)

)
(:action Prim336Action-1
:parameters ()
:precondition
(and
(not (RIGHTOF9-ROBOT))
)
:effect
(and
(Flag336prime-)
(not (Flag336-))
)

)
(:action Prim337Action-0
:parameters ()
:precondition
(and
(not (LEFTOF4-ROBOT))
)
:effect
(and
(Flag337prime-)
(not (Flag337-))
)

)
(:action Prim337Action-1
:parameters ()
:precondition
(and
(not (RIGHTOF16-ROBOT))
)
:effect
(and
(Flag337prime-)
(not (Flag337-))
)

)
(:action Prim338Action-0
:parameters ()
:precondition
(and
(not (LEFTOF4-ROBOT))
)
:effect
(and
(Flag338prime-)
(not (Flag338-))
)

)
(:action Prim338Action-1
:parameters ()
:precondition
(and
(not (RIGHTOF6-ROBOT))
)
:effect
(and
(Flag338prime-)
(not (Flag338-))
)

)
(:action Prim339Action-0
:parameters ()
:precondition
(and
(not (LEFTOF4-ROBOT))
)
:effect
(and
(Flag339prime-)
(not (Flag339-))
)

)
(:action Prim339Action-1
:parameters ()
:precondition
(and
(not (RIGHTOF5-ROBOT))
)
:effect
(and
(Flag339prime-)
(not (Flag339-))
)

)
(:action Prim340Action-0
:parameters ()
:precondition
(and
(not (RIGHTOF13-ROBOT))
)
:effect
(and
(Flag340prime-)
(not (Flag340-))
)

)
(:action Prim340Action-1
:parameters ()
:precondition
(and
(not (LEFTOF4-ROBOT))
)
:effect
(and
(Flag340prime-)
(not (Flag340-))
)

)
(:action Prim341Action-0
:parameters ()
:precondition
(and
(not (RIGHTOF3-ROBOT))
)
:effect
(and
(Flag341prime-)
(not (Flag341-))
)

)
(:action Prim341Action-1
:parameters ()
:precondition
(and
(not (LEFTOF4-ROBOT))
)
:effect
(and
(Flag341prime-)
(not (Flag341-))
)

)
(:action Prim342Action-0
:parameters ()
:precondition
(and
(not (LEFTOF4-ROBOT))
)
:effect
(and
(Flag342prime-)
(not (Flag342-))
)

)
(:action Prim342Action-1
:parameters ()
:precondition
(and
(not (RIGHTOF11-ROBOT))
)
:effect
(and
(Flag342prime-)
(not (Flag342-))
)

)
(:action Flag344Action
:parameters ()
:precondition
(and
(RIGHTOF9-ROBOT)
(LEFTOF5-ROBOT)
)
:effect
(and
(Flag344-)
(Flag344prime-)
)

)
(:action Flag345Action
:parameters ()
:precondition
(and
(RIGHTOF15-ROBOT)
(LEFTOF5-ROBOT)
)
:effect
(and
(Flag345-)
(Flag345prime-)
)

)
(:action Flag346Action
:parameters ()
:precondition
(and
(RIGHTOF4-ROBOT)
(LEFTOF5-ROBOT)
)
:effect
(and
(Flag346-)
(Flag346prime-)
)

)
(:action Flag347Action
:parameters ()
:precondition
(and
(RIGHTOF7-ROBOT)
(LEFTOF5-ROBOT)
)
:effect
(and
(Flag347-)
(Flag347prime-)
)

)
(:action Flag348Action
:parameters ()
:precondition
(and
(RIGHTOF11-ROBOT)
(LEFTOF5-ROBOT)
)
:effect
(and
(Flag348-)
(Flag348prime-)
)

)
(:action Flag349Action
:parameters ()
:precondition
(and
(RIGHTOF13-ROBOT)
(LEFTOF5-ROBOT)
)
:effect
(and
(Flag349-)
(Flag349prime-)
)

)
(:action Flag350Action
:parameters ()
:precondition
(and
(RIGHTOF16-ROBOT)
(LEFTOF5-ROBOT)
)
:effect
(and
(Flag350-)
(Flag350prime-)
)

)
(:action Flag351Action
:parameters ()
:precondition
(and
(LEFTOF4-ROBOT)
(RIGHTOF8-ROBOT)
)
:effect
(and
(Flag351-)
(Flag351prime-)
)

)
(:action Flag352Action
:parameters ()
:precondition
(and
(LEFTOF5-ROBOT)
(RIGHTOF6-ROBOT)
)
:effect
(and
(Flag352-)
(Flag352prime-)
)

)
(:action Flag353Action
:parameters ()
:precondition
(and
(RIGHTOF10-ROBOT)
(LEFTOF5-ROBOT)
)
:effect
(and
(Flag353-)
(Flag353prime-)
)

)
(:action Flag354Action
:parameters ()
:precondition
(and
(LEFTOF4-ROBOT)
(RIGHTOF10-ROBOT)
)
:effect
(and
(Flag354-)
(Flag354prime-)
)

)
(:action Flag355Action
:parameters ()
:precondition
(and
(LEFTOF5-ROBOT)
(RIGHTOF12-ROBOT)
)
:effect
(and
(Flag355-)
(Flag355prime-)
)

)
(:action Flag356Action
:parameters ()
:precondition
(and
(RIGHTOF8-ROBOT)
(LEFTOF5-ROBOT)
)
:effect
(and
(Flag356-)
(Flag356prime-)
)

)
(:action Flag357Action
:parameters ()
:precondition
(and
(RIGHTOF14-ROBOT)
(LEFTOF5-ROBOT)
)
:effect
(and
(Flag357-)
(Flag357prime-)
)

)
(:action Flag358Action
:parameters ()
:precondition
(and
(RIGHTOF5-ROBOT)
(LEFTOF5-ROBOT)
)
:effect
(and
(Flag358-)
(Flag358prime-)
)

)
(:action Flag359Action
:parameters ()
:precondition
(and
(RIGHTOF17-ROBOT)
(LEFTOF5-ROBOT)
)
:effect
(and
(Flag359-)
(Flag359prime-)
)

)
(:action Prim344Action-0
:parameters ()
:precondition
(and
(not (RIGHTOF9-ROBOT))
)
:effect
(and
(Flag344prime-)
(not (Flag344-))
)

)
(:action Prim344Action-1
:parameters ()
:precondition
(and
(not (LEFTOF5-ROBOT))
)
:effect
(and
(Flag344prime-)
(not (Flag344-))
)

)
(:action Prim345Action-0
:parameters ()
:precondition
(and
(not (RIGHTOF15-ROBOT))
)
:effect
(and
(Flag345prime-)
(not (Flag345-))
)

)
(:action Prim345Action-1
:parameters ()
:precondition
(and
(not (LEFTOF5-ROBOT))
)
:effect
(and
(Flag345prime-)
(not (Flag345-))
)

)
(:action Prim346Action-0
:parameters ()
:precondition
(and
(not (RIGHTOF4-ROBOT))
)
:effect
(and
(Flag346prime-)
(not (Flag346-))
)

)
(:action Prim346Action-1
:parameters ()
:precondition
(and
(not (LEFTOF5-ROBOT))
)
:effect
(and
(Flag346prime-)
(not (Flag346-))
)

)
(:action Prim347Action-0
:parameters ()
:precondition
(and
(not (RIGHTOF7-ROBOT))
)
:effect
(and
(Flag347prime-)
(not (Flag347-))
)

)
(:action Prim347Action-1
:parameters ()
:precondition
(and
(not (LEFTOF5-ROBOT))
)
:effect
(and
(Flag347prime-)
(not (Flag347-))
)

)
(:action Prim348Action-0
:parameters ()
:precondition
(and
(not (RIGHTOF11-ROBOT))
)
:effect
(and
(Flag348prime-)
(not (Flag348-))
)

)
(:action Prim348Action-1
:parameters ()
:precondition
(and
(not (LEFTOF5-ROBOT))
)
:effect
(and
(Flag348prime-)
(not (Flag348-))
)

)
(:action Prim349Action-0
:parameters ()
:precondition
(and
(not (RIGHTOF13-ROBOT))
)
:effect
(and
(Flag349prime-)
(not (Flag349-))
)

)
(:action Prim349Action-1
:parameters ()
:precondition
(and
(not (LEFTOF5-ROBOT))
)
:effect
(and
(Flag349prime-)
(not (Flag349-))
)

)
(:action Prim350Action-0
:parameters ()
:precondition
(and
(not (RIGHTOF16-ROBOT))
)
:effect
(and
(Flag350prime-)
(not (Flag350-))
)

)
(:action Prim350Action-1
:parameters ()
:precondition
(and
(not (LEFTOF5-ROBOT))
)
:effect
(and
(Flag350prime-)
(not (Flag350-))
)

)
(:action Prim351Action-0
:parameters ()
:precondition
(and
(not (LEFTOF4-ROBOT))
)
:effect
(and
(Flag351prime-)
(not (Flag351-))
)

)
(:action Prim351Action-1
:parameters ()
:precondition
(and
(not (RIGHTOF8-ROBOT))
)
:effect
(and
(Flag351prime-)
(not (Flag351-))
)

)
(:action Prim352Action-0
:parameters ()
:precondition
(and
(not (LEFTOF5-ROBOT))
)
:effect
(and
(Flag352prime-)
(not (Flag352-))
)

)
(:action Prim352Action-1
:parameters ()
:precondition
(and
(not (RIGHTOF6-ROBOT))
)
:effect
(and
(Flag352prime-)
(not (Flag352-))
)

)
(:action Prim353Action-0
:parameters ()
:precondition
(and
(not (RIGHTOF10-ROBOT))
)
:effect
(and
(Flag353prime-)
(not (Flag353-))
)

)
(:action Prim353Action-1
:parameters ()
:precondition
(and
(not (LEFTOF5-ROBOT))
)
:effect
(and
(Flag353prime-)
(not (Flag353-))
)

)
(:action Prim354Action-0
:parameters ()
:precondition
(and
(not (LEFTOF4-ROBOT))
)
:effect
(and
(Flag354prime-)
(not (Flag354-))
)

)
(:action Prim354Action-1
:parameters ()
:precondition
(and
(not (RIGHTOF10-ROBOT))
)
:effect
(and
(Flag354prime-)
(not (Flag354-))
)

)
(:action Prim355Action-0
:parameters ()
:precondition
(and
(not (LEFTOF5-ROBOT))
)
:effect
(and
(Flag355prime-)
(not (Flag355-))
)

)
(:action Prim355Action-1
:parameters ()
:precondition
(and
(not (RIGHTOF12-ROBOT))
)
:effect
(and
(Flag355prime-)
(not (Flag355-))
)

)
(:action Prim356Action-0
:parameters ()
:precondition
(and
(not (RIGHTOF8-ROBOT))
)
:effect
(and
(Flag356prime-)
(not (Flag356-))
)

)
(:action Prim356Action-1
:parameters ()
:precondition
(and
(not (LEFTOF5-ROBOT))
)
:effect
(and
(Flag356prime-)
(not (Flag356-))
)

)
(:action Prim357Action-0
:parameters ()
:precondition
(and
(not (RIGHTOF14-ROBOT))
)
:effect
(and
(Flag357prime-)
(not (Flag357-))
)

)
(:action Prim357Action-1
:parameters ()
:precondition
(and
(not (LEFTOF5-ROBOT))
)
:effect
(and
(Flag357prime-)
(not (Flag357-))
)

)
(:action Prim358Action-0
:parameters ()
:precondition
(and
(not (RIGHTOF5-ROBOT))
)
:effect
(and
(Flag358prime-)
(not (Flag358-))
)

)
(:action Prim358Action-1
:parameters ()
:precondition
(and
(not (LEFTOF5-ROBOT))
)
:effect
(and
(Flag358prime-)
(not (Flag358-))
)

)
(:action Prim359Action-0
:parameters ()
:precondition
(and
(not (RIGHTOF17-ROBOT))
)
:effect
(and
(Flag359prime-)
(not (Flag359-))
)

)
(:action Prim359Action-1
:parameters ()
:precondition
(and
(not (LEFTOF5-ROBOT))
)
:effect
(and
(Flag359prime-)
(not (Flag359-))
)

)
(:action Flag361Action
:parameters ()
:precondition
(and
(RIGHTOF16-ROBOT)
(LEFTOF6-ROBOT)
)
:effect
(and
(Flag361-)
(Flag361prime-)
)

)
(:action Flag362Action
:parameters ()
:precondition
(and
(RIGHTOF15-ROBOT)
(LEFTOF6-ROBOT)
)
:effect
(and
(Flag362-)
(Flag362prime-)
)

)
(:action Flag363Action
:parameters ()
:precondition
(and
(RIGHTOF12-ROBOT)
(LEFTOF6-ROBOT)
)
:effect
(and
(Flag363-)
(Flag363prime-)
)

)
(:action Flag364Action
:parameters ()
:precondition
(and
(RIGHTOF8-ROBOT)
(LEFTOF6-ROBOT)
)
:effect
(and
(Flag364-)
(Flag364prime-)
)

)
(:action Flag365Action
:parameters ()
:precondition
(and
(RIGHTOF11-ROBOT)
(LEFTOF6-ROBOT)
)
:effect
(and
(Flag365-)
(Flag365prime-)
)

)
(:action Flag366Action
:parameters ()
:precondition
(and
(RIGHTOF17-ROBOT)
(LEFTOF6-ROBOT)
)
:effect
(and
(Flag366-)
(Flag366prime-)
)

)
(:action Flag367Action
:parameters ()
:precondition
(and
(RIGHTOF5-ROBOT)
(LEFTOF6-ROBOT)
)
:effect
(and
(Flag367-)
(Flag367prime-)
)

)
(:action Flag368Action
:parameters ()
:precondition
(and
(RIGHTOF7-ROBOT)
(LEFTOF6-ROBOT)
)
:effect
(and
(Flag368-)
(Flag368prime-)
)

)
(:action Flag369Action
:parameters ()
:precondition
(and
(RIGHTOF10-ROBOT)
(LEFTOF6-ROBOT)
)
:effect
(and
(Flag369-)
(Flag369prime-)
)

)
(:action Flag370Action
:parameters ()
:precondition
(and
(RIGHTOF6-ROBOT)
(LEFTOF6-ROBOT)
)
:effect
(and
(Flag370-)
(Flag370prime-)
)

)
(:action Flag371Action
:parameters ()
:precondition
(and
(RIGHTOF13-ROBOT)
(LEFTOF6-ROBOT)
)
:effect
(and
(Flag371-)
(Flag371prime-)
)

)
(:action Flag372Action
:parameters ()
:precondition
(and
(RIGHTOF14-ROBOT)
(LEFTOF6-ROBOT)
)
:effect
(and
(Flag372-)
(Flag372prime-)
)

)
(:action Flag373Action
:parameters ()
:precondition
(and
(RIGHTOF9-ROBOT)
(LEFTOF6-ROBOT)
)
:effect
(and
(Flag373-)
(Flag373prime-)
)

)
(:action Prim361Action-0
:parameters ()
:precondition
(and
(not (RIGHTOF16-ROBOT))
)
:effect
(and
(Flag361prime-)
(not (Flag361-))
)

)
(:action Prim361Action-1
:parameters ()
:precondition
(and
(not (LEFTOF6-ROBOT))
)
:effect
(and
(Flag361prime-)
(not (Flag361-))
)

)
(:action Prim362Action-0
:parameters ()
:precondition
(and
(not (RIGHTOF15-ROBOT))
)
:effect
(and
(Flag362prime-)
(not (Flag362-))
)

)
(:action Prim362Action-1
:parameters ()
:precondition
(and
(not (LEFTOF6-ROBOT))
)
:effect
(and
(Flag362prime-)
(not (Flag362-))
)

)
(:action Prim363Action-0
:parameters ()
:precondition
(and
(not (RIGHTOF12-ROBOT))
)
:effect
(and
(Flag363prime-)
(not (Flag363-))
)

)
(:action Prim363Action-1
:parameters ()
:precondition
(and
(not (LEFTOF6-ROBOT))
)
:effect
(and
(Flag363prime-)
(not (Flag363-))
)

)
(:action Prim364Action-0
:parameters ()
:precondition
(and
(not (RIGHTOF8-ROBOT))
)
:effect
(and
(Flag364prime-)
(not (Flag364-))
)

)
(:action Prim364Action-1
:parameters ()
:precondition
(and
(not (LEFTOF6-ROBOT))
)
:effect
(and
(Flag364prime-)
(not (Flag364-))
)

)
(:action Prim365Action-0
:parameters ()
:precondition
(and
(not (RIGHTOF11-ROBOT))
)
:effect
(and
(Flag365prime-)
(not (Flag365-))
)

)
(:action Prim365Action-1
:parameters ()
:precondition
(and
(not (LEFTOF6-ROBOT))
)
:effect
(and
(Flag365prime-)
(not (Flag365-))
)

)
(:action Prim366Action-0
:parameters ()
:precondition
(and
(not (RIGHTOF17-ROBOT))
)
:effect
(and
(Flag366prime-)
(not (Flag366-))
)

)
(:action Prim366Action-1
:parameters ()
:precondition
(and
(not (LEFTOF6-ROBOT))
)
:effect
(and
(Flag366prime-)
(not (Flag366-))
)

)
(:action Prim367Action-0
:parameters ()
:precondition
(and
(not (RIGHTOF5-ROBOT))
)
:effect
(and
(Flag367prime-)
(not (Flag367-))
)

)
(:action Prim367Action-1
:parameters ()
:precondition
(and
(not (LEFTOF6-ROBOT))
)
:effect
(and
(Flag367prime-)
(not (Flag367-))
)

)
(:action Prim368Action-0
:parameters ()
:precondition
(and
(not (RIGHTOF7-ROBOT))
)
:effect
(and
(Flag368prime-)
(not (Flag368-))
)

)
(:action Prim368Action-1
:parameters ()
:precondition
(and
(not (LEFTOF6-ROBOT))
)
:effect
(and
(Flag368prime-)
(not (Flag368-))
)

)
(:action Prim369Action-0
:parameters ()
:precondition
(and
(not (RIGHTOF10-ROBOT))
)
:effect
(and
(Flag369prime-)
(not (Flag369-))
)

)
(:action Prim369Action-1
:parameters ()
:precondition
(and
(not (LEFTOF6-ROBOT))
)
:effect
(and
(Flag369prime-)
(not (Flag369-))
)

)
(:action Prim370Action-0
:parameters ()
:precondition
(and
(not (RIGHTOF6-ROBOT))
)
:effect
(and
(Flag370prime-)
(not (Flag370-))
)

)
(:action Prim370Action-1
:parameters ()
:precondition
(and
(not (LEFTOF6-ROBOT))
)
:effect
(and
(Flag370prime-)
(not (Flag370-))
)

)
(:action Prim371Action-0
:parameters ()
:precondition
(and
(not (RIGHTOF13-ROBOT))
)
:effect
(and
(Flag371prime-)
(not (Flag371-))
)

)
(:action Prim371Action-1
:parameters ()
:precondition
(and
(not (LEFTOF6-ROBOT))
)
:effect
(and
(Flag371prime-)
(not (Flag371-))
)

)
(:action Prim372Action-0
:parameters ()
:precondition
(and
(not (RIGHTOF14-ROBOT))
)
:effect
(and
(Flag372prime-)
(not (Flag372-))
)

)
(:action Prim372Action-1
:parameters ()
:precondition
(and
(not (LEFTOF6-ROBOT))
)
:effect
(and
(Flag372prime-)
(not (Flag372-))
)

)
(:action Prim373Action-0
:parameters ()
:precondition
(and
(not (RIGHTOF9-ROBOT))
)
:effect
(and
(Flag373prime-)
(not (Flag373-))
)

)
(:action Prim373Action-1
:parameters ()
:precondition
(and
(not (LEFTOF6-ROBOT))
)
:effect
(and
(Flag373prime-)
(not (Flag373-))
)

)
(:action Flag375Action
:parameters ()
:precondition
(and
(LEFTOF7-ROBOT)
(RIGHTOF7-ROBOT)
)
:effect
(and
(Flag375-)
(Flag375prime-)
)

)
(:action Flag376Action
:parameters ()
:precondition
(and
(RIGHTOF13-ROBOT)
(LEFTOF7-ROBOT)
)
:effect
(and
(Flag376-)
(Flag376prime-)
)

)
(:action Flag377Action
:parameters ()
:precondition
(and
(LEFTOF7-ROBOT)
(RIGHTOF9-ROBOT)
)
:effect
(and
(Flag377-)
(Flag377prime-)
)

)
(:action Flag378Action
:parameters ()
:precondition
(and
(LEFTOF6-ROBOT)
(RIGHTOF6-ROBOT)
)
:effect
(and
(Flag378-)
(Flag378prime-)
)

)
(:action Flag379Action
:parameters ()
:precondition
(and
(LEFTOF7-ROBOT)
(RIGHTOF12-ROBOT)
)
:effect
(and
(Flag379-)
(Flag379prime-)
)

)
(:action Flag380Action
:parameters ()
:precondition
(and
(LEFTOF7-ROBOT)
(RIGHTOF6-ROBOT)
)
:effect
(and
(Flag380-)
(Flag380prime-)
)

)
(:action Flag381Action
:parameters ()
:precondition
(and
(LEFTOF6-ROBOT)
(RIGHTOF12-ROBOT)
)
:effect
(and
(Flag381-)
(Flag381prime-)
)

)
(:action Flag382Action
:parameters ()
:precondition
(and
(LEFTOF7-ROBOT)
(RIGHTOF15-ROBOT)
)
:effect
(and
(Flag382-)
(Flag382prime-)
)

)
(:action Flag383Action
:parameters ()
:precondition
(and
(RIGHTOF11-ROBOT)
(LEFTOF7-ROBOT)
)
:effect
(and
(Flag383-)
(Flag383prime-)
)

)
(:action Flag384Action
:parameters ()
:precondition
(and
(RIGHTOF8-ROBOT)
(LEFTOF7-ROBOT)
)
:effect
(and
(Flag384-)
(Flag384prime-)
)

)
(:action Flag385Action
:parameters ()
:precondition
(and
(RIGHTOF17-ROBOT)
(LEFTOF7-ROBOT)
)
:effect
(and
(Flag385-)
(Flag385prime-)
)

)
(:action Flag386Action
:parameters ()
:precondition
(and
(RIGHTOF16-ROBOT)
(LEFTOF7-ROBOT)
)
:effect
(and
(Flag386-)
(Flag386prime-)
)

)
(:action Flag387Action
:parameters ()
:precondition
(and
(LEFTOF7-ROBOT)
(RIGHTOF14-ROBOT)
)
:effect
(and
(Flag387-)
(Flag387prime-)
)

)
(:action Flag388Action
:parameters ()
:precondition
(and
(RIGHTOF10-ROBOT)
(LEFTOF7-ROBOT)
)
:effect
(and
(Flag388-)
(Flag388prime-)
)

)
(:action Prim375Action-0
:parameters ()
:precondition
(and
(not (LEFTOF7-ROBOT))
)
:effect
(and
(Flag375prime-)
(not (Flag375-))
)

)
(:action Prim375Action-1
:parameters ()
:precondition
(and
(not (RIGHTOF7-ROBOT))
)
:effect
(and
(Flag375prime-)
(not (Flag375-))
)

)
(:action Prim376Action-0
:parameters ()
:precondition
(and
(not (RIGHTOF13-ROBOT))
)
:effect
(and
(Flag376prime-)
(not (Flag376-))
)

)
(:action Prim376Action-1
:parameters ()
:precondition
(and
(not (LEFTOF7-ROBOT))
)
:effect
(and
(Flag376prime-)
(not (Flag376-))
)

)
(:action Prim377Action-0
:parameters ()
:precondition
(and
(not (LEFTOF7-ROBOT))
)
:effect
(and
(Flag377prime-)
(not (Flag377-))
)

)
(:action Prim377Action-1
:parameters ()
:precondition
(and
(not (RIGHTOF9-ROBOT))
)
:effect
(and
(Flag377prime-)
(not (Flag377-))
)

)
(:action Prim378Action-0
:parameters ()
:precondition
(and
(not (LEFTOF6-ROBOT))
)
:effect
(and
(Flag378prime-)
(not (Flag378-))
)

)
(:action Prim378Action-1
:parameters ()
:precondition
(and
(not (RIGHTOF6-ROBOT))
)
:effect
(and
(Flag378prime-)
(not (Flag378-))
)

)
(:action Prim379Action-0
:parameters ()
:precondition
(and
(not (LEFTOF7-ROBOT))
)
:effect
(and
(Flag379prime-)
(not (Flag379-))
)

)
(:action Prim379Action-1
:parameters ()
:precondition
(and
(not (RIGHTOF12-ROBOT))
)
:effect
(and
(Flag379prime-)
(not (Flag379-))
)

)
(:action Prim380Action-0
:parameters ()
:precondition
(and
(not (LEFTOF7-ROBOT))
)
:effect
(and
(Flag380prime-)
(not (Flag380-))
)

)
(:action Prim380Action-1
:parameters ()
:precondition
(and
(not (RIGHTOF6-ROBOT))
)
:effect
(and
(Flag380prime-)
(not (Flag380-))
)

)
(:action Prim381Action-0
:parameters ()
:precondition
(and
(not (LEFTOF6-ROBOT))
)
:effect
(and
(Flag381prime-)
(not (Flag381-))
)

)
(:action Prim381Action-1
:parameters ()
:precondition
(and
(not (RIGHTOF12-ROBOT))
)
:effect
(and
(Flag381prime-)
(not (Flag381-))
)

)
(:action Prim382Action-0
:parameters ()
:precondition
(and
(not (LEFTOF7-ROBOT))
)
:effect
(and
(Flag382prime-)
(not (Flag382-))
)

)
(:action Prim382Action-1
:parameters ()
:precondition
(and
(not (RIGHTOF15-ROBOT))
)
:effect
(and
(Flag382prime-)
(not (Flag382-))
)

)
(:action Prim383Action-0
:parameters ()
:precondition
(and
(not (RIGHTOF11-ROBOT))
)
:effect
(and
(Flag383prime-)
(not (Flag383-))
)

)
(:action Prim383Action-1
:parameters ()
:precondition
(and
(not (LEFTOF7-ROBOT))
)
:effect
(and
(Flag383prime-)
(not (Flag383-))
)

)
(:action Prim384Action-0
:parameters ()
:precondition
(and
(not (RIGHTOF8-ROBOT))
)
:effect
(and
(Flag384prime-)
(not (Flag384-))
)

)
(:action Prim384Action-1
:parameters ()
:precondition
(and
(not (LEFTOF7-ROBOT))
)
:effect
(and
(Flag384prime-)
(not (Flag384-))
)

)
(:action Prim385Action-0
:parameters ()
:precondition
(and
(not (RIGHTOF17-ROBOT))
)
:effect
(and
(Flag385prime-)
(not (Flag385-))
)

)
(:action Prim385Action-1
:parameters ()
:precondition
(and
(not (LEFTOF7-ROBOT))
)
:effect
(and
(Flag385prime-)
(not (Flag385-))
)

)
(:action Prim386Action-0
:parameters ()
:precondition
(and
(not (RIGHTOF16-ROBOT))
)
:effect
(and
(Flag386prime-)
(not (Flag386-))
)

)
(:action Prim386Action-1
:parameters ()
:precondition
(and
(not (LEFTOF7-ROBOT))
)
:effect
(and
(Flag386prime-)
(not (Flag386-))
)

)
(:action Prim387Action-0
:parameters ()
:precondition
(and
(not (LEFTOF7-ROBOT))
)
:effect
(and
(Flag387prime-)
(not (Flag387-))
)

)
(:action Prim387Action-1
:parameters ()
:precondition
(and
(not (RIGHTOF14-ROBOT))
)
:effect
(and
(Flag387prime-)
(not (Flag387-))
)

)
(:action Prim388Action-0
:parameters ()
:precondition
(and
(not (RIGHTOF10-ROBOT))
)
:effect
(and
(Flag388prime-)
(not (Flag388-))
)

)
(:action Prim388Action-1
:parameters ()
:precondition
(and
(not (LEFTOF7-ROBOT))
)
:effect
(and
(Flag388prime-)
(not (Flag388-))
)

)
(:action Flag390Action
:parameters ()
:precondition
(and
(RIGHTOF10-ROBOT)
(LEFTOF8-ROBOT)
)
:effect
(and
(Flag390-)
(Flag390prime-)
)

)
(:action Flag391Action
:parameters ()
:precondition
(and
(RIGHTOF16-ROBOT)
(LEFTOF8-ROBOT)
)
:effect
(and
(Flag391-)
(Flag391prime-)
)

)
(:action Flag392Action
:parameters ()
:precondition
(and
(RIGHTOF13-ROBOT)
(LEFTOF8-ROBOT)
)
:effect
(and
(Flag392-)
(Flag392prime-)
)

)
(:action Flag393Action
:parameters ()
:precondition
(and
(RIGHTOF7-ROBOT)
(LEFTOF7-ROBOT)
)
:effect
(and
(Flag393-)
(Flag393prime-)
)

)
(:action Flag394Action
:parameters ()
:precondition
(and
(LEFTOF8-ROBOT)
(RIGHTOF15-ROBOT)
)
:effect
(and
(Flag394-)
(Flag394prime-)
)

)
(:action Flag395Action
:parameters ()
:precondition
(and
(LEFTOF8-ROBOT)
(RIGHTOF9-ROBOT)
)
:effect
(and
(Flag395-)
(Flag395prime-)
)

)
(:action Flag396Action
:parameters ()
:precondition
(and
(RIGHTOF7-ROBOT)
(LEFTOF8-ROBOT)
)
:effect
(and
(Flag396-)
(Flag396prime-)
)

)
(:action Flag397Action
:parameters ()
:precondition
(and
(RIGHTOF14-ROBOT)
(LEFTOF8-ROBOT)
)
:effect
(and
(Flag397-)
(Flag397prime-)
)

)
(:action Flag398Action
:parameters ()
:precondition
(and
(LEFTOF8-ROBOT)
(RIGHTOF12-ROBOT)
)
:effect
(and
(Flag398-)
(Flag398prime-)
)

)
(:action Flag399Action
:parameters ()
:precondition
(and
(RIGHTOF11-ROBOT)
(LEFTOF8-ROBOT)
)
:effect
(and
(Flag399-)
(Flag399prime-)
)

)
(:action Flag400Action
:parameters ()
:precondition
(and
(RIGHTOF17-ROBOT)
(LEFTOF8-ROBOT)
)
:effect
(and
(Flag400-)
(Flag400prime-)
)

)
(:action Flag401Action
:parameters ()
:precondition
(and
(RIGHTOF8-ROBOT)
(LEFTOF8-ROBOT)
)
:effect
(and
(Flag401-)
(Flag401prime-)
)

)
(:action Prim390Action-0
:parameters ()
:precondition
(and
(not (RIGHTOF10-ROBOT))
)
:effect
(and
(Flag390prime-)
(not (Flag390-))
)

)
(:action Prim390Action-1
:parameters ()
:precondition
(and
(not (LEFTOF8-ROBOT))
)
:effect
(and
(Flag390prime-)
(not (Flag390-))
)

)
(:action Prim391Action-0
:parameters ()
:precondition
(and
(not (RIGHTOF16-ROBOT))
)
:effect
(and
(Flag391prime-)
(not (Flag391-))
)

)
(:action Prim391Action-1
:parameters ()
:precondition
(and
(not (LEFTOF8-ROBOT))
)
:effect
(and
(Flag391prime-)
(not (Flag391-))
)

)
(:action Prim392Action-0
:parameters ()
:precondition
(and
(not (RIGHTOF13-ROBOT))
)
:effect
(and
(Flag392prime-)
(not (Flag392-))
)

)
(:action Prim392Action-1
:parameters ()
:precondition
(and
(not (LEFTOF8-ROBOT))
)
:effect
(and
(Flag392prime-)
(not (Flag392-))
)

)
(:action Prim393Action-0
:parameters ()
:precondition
(and
(not (RIGHTOF7-ROBOT))
)
:effect
(and
(Flag393prime-)
(not (Flag393-))
)

)
(:action Prim393Action-1
:parameters ()
:precondition
(and
(not (LEFTOF7-ROBOT))
)
:effect
(and
(Flag393prime-)
(not (Flag393-))
)

)
(:action Prim394Action-0
:parameters ()
:precondition
(and
(not (LEFTOF8-ROBOT))
)
:effect
(and
(Flag394prime-)
(not (Flag394-))
)

)
(:action Prim394Action-1
:parameters ()
:precondition
(and
(not (RIGHTOF15-ROBOT))
)
:effect
(and
(Flag394prime-)
(not (Flag394-))
)

)
(:action Prim395Action-0
:parameters ()
:precondition
(and
(not (LEFTOF8-ROBOT))
)
:effect
(and
(Flag395prime-)
(not (Flag395-))
)

)
(:action Prim395Action-1
:parameters ()
:precondition
(and
(not (RIGHTOF9-ROBOT))
)
:effect
(and
(Flag395prime-)
(not (Flag395-))
)

)
(:action Prim396Action-0
:parameters ()
:precondition
(and
(not (RIGHTOF7-ROBOT))
)
:effect
(and
(Flag396prime-)
(not (Flag396-))
)

)
(:action Prim396Action-1
:parameters ()
:precondition
(and
(not (LEFTOF8-ROBOT))
)
:effect
(and
(Flag396prime-)
(not (Flag396-))
)

)
(:action Prim397Action-0
:parameters ()
:precondition
(and
(not (RIGHTOF14-ROBOT))
)
:effect
(and
(Flag397prime-)
(not (Flag397-))
)

)
(:action Prim397Action-1
:parameters ()
:precondition
(and
(not (LEFTOF8-ROBOT))
)
:effect
(and
(Flag397prime-)
(not (Flag397-))
)

)
(:action Prim398Action-0
:parameters ()
:precondition
(and
(not (LEFTOF8-ROBOT))
)
:effect
(and
(Flag398prime-)
(not (Flag398-))
)

)
(:action Prim398Action-1
:parameters ()
:precondition
(and
(not (RIGHTOF12-ROBOT))
)
:effect
(and
(Flag398prime-)
(not (Flag398-))
)

)
(:action Prim399Action-0
:parameters ()
:precondition
(and
(not (RIGHTOF11-ROBOT))
)
:effect
(and
(Flag399prime-)
(not (Flag399-))
)

)
(:action Prim399Action-1
:parameters ()
:precondition
(and
(not (LEFTOF8-ROBOT))
)
:effect
(and
(Flag399prime-)
(not (Flag399-))
)

)
(:action Prim400Action-0
:parameters ()
:precondition
(and
(not (RIGHTOF17-ROBOT))
)
:effect
(and
(Flag400prime-)
(not (Flag400-))
)

)
(:action Prim400Action-1
:parameters ()
:precondition
(and
(not (LEFTOF8-ROBOT))
)
:effect
(and
(Flag400prime-)
(not (Flag400-))
)

)
(:action Prim401Action-0
:parameters ()
:precondition
(and
(not (RIGHTOF8-ROBOT))
)
:effect
(and
(Flag401prime-)
(not (Flag401-))
)

)
(:action Prim401Action-1
:parameters ()
:precondition
(and
(not (LEFTOF8-ROBOT))
)
:effect
(and
(Flag401prime-)
(not (Flag401-))
)

)
(:action Flag403Action
:parameters ()
:precondition
(and
(RIGHTOF13-ROBOT)
(LEFTOF9-ROBOT)
)
:effect
(and
(Flag403-)
(Flag403prime-)
)

)
(:action Flag404Action
:parameters ()
:precondition
(and
(RIGHTOF8-ROBOT)
(LEFTOF9-ROBOT)
)
:effect
(and
(Flag404-)
(Flag404prime-)
)

)
(:action Flag405Action
:parameters ()
:precondition
(and
(LEFTOF9-ROBOT)
(RIGHTOF12-ROBOT)
)
:effect
(and
(Flag405-)
(Flag405prime-)
)

)
(:action Flag406Action
:parameters ()
:precondition
(and
(LEFTOF9-ROBOT)
(RIGHTOF9-ROBOT)
)
:effect
(and
(Flag406-)
(Flag406prime-)
)

)
(:action Flag407Action
:parameters ()
:precondition
(and
(LEFTOF8-ROBOT)
(RIGHTOF14-ROBOT)
)
:effect
(and
(Flag407-)
(Flag407prime-)
)

)
(:action Flag408Action
:parameters ()
:precondition
(and
(RIGHTOF11-ROBOT)
(LEFTOF9-ROBOT)
)
:effect
(and
(Flag408-)
(Flag408prime-)
)

)
(:action Flag409Action
:parameters ()
:precondition
(and
(LEFTOF9-ROBOT)
(RIGHTOF15-ROBOT)
)
:effect
(and
(Flag409-)
(Flag409prime-)
)

)
(:action Flag410Action
:parameters ()
:precondition
(and
(RIGHTOF10-ROBOT)
(LEFTOF9-ROBOT)
)
:effect
(and
(Flag410-)
(Flag410prime-)
)

)
(:action Flag411Action
:parameters ()
:precondition
(and
(LEFTOF9-ROBOT)
(RIGHTOF16-ROBOT)
)
:effect
(and
(Flag411-)
(Flag411prime-)
)

)
(:action Flag412Action
:parameters ()
:precondition
(and
(LEFTOF9-ROBOT)
(RIGHTOF17-ROBOT)
)
:effect
(and
(Flag412-)
(Flag412prime-)
)

)
(:action Flag413Action
:parameters ()
:precondition
(and
(LEFTOF9-ROBOT)
(RIGHTOF14-ROBOT)
)
:effect
(and
(Flag413-)
(Flag413prime-)
)

)
(:action Prim403Action-0
:parameters ()
:precondition
(and
(not (RIGHTOF13-ROBOT))
)
:effect
(and
(Flag403prime-)
(not (Flag403-))
)

)
(:action Prim403Action-1
:parameters ()
:precondition
(and
(not (LEFTOF9-ROBOT))
)
:effect
(and
(Flag403prime-)
(not (Flag403-))
)

)
(:action Prim404Action-0
:parameters ()
:precondition
(and
(not (RIGHTOF8-ROBOT))
)
:effect
(and
(Flag404prime-)
(not (Flag404-))
)

)
(:action Prim404Action-1
:parameters ()
:precondition
(and
(not (LEFTOF9-ROBOT))
)
:effect
(and
(Flag404prime-)
(not (Flag404-))
)

)
(:action Prim405Action-0
:parameters ()
:precondition
(and
(not (LEFTOF9-ROBOT))
)
:effect
(and
(Flag405prime-)
(not (Flag405-))
)

)
(:action Prim405Action-1
:parameters ()
:precondition
(and
(not (RIGHTOF12-ROBOT))
)
:effect
(and
(Flag405prime-)
(not (Flag405-))
)

)
(:action Prim406Action-0
:parameters ()
:precondition
(and
(not (LEFTOF9-ROBOT))
)
:effect
(and
(Flag406prime-)
(not (Flag406-))
)

)
(:action Prim406Action-1
:parameters ()
:precondition
(and
(not (RIGHTOF9-ROBOT))
)
:effect
(and
(Flag406prime-)
(not (Flag406-))
)

)
(:action Prim407Action-0
:parameters ()
:precondition
(and
(not (LEFTOF8-ROBOT))
)
:effect
(and
(Flag407prime-)
(not (Flag407-))
)

)
(:action Prim407Action-1
:parameters ()
:precondition
(and
(not (RIGHTOF14-ROBOT))
)
:effect
(and
(Flag407prime-)
(not (Flag407-))
)

)
(:action Prim408Action-0
:parameters ()
:precondition
(and
(not (RIGHTOF11-ROBOT))
)
:effect
(and
(Flag408prime-)
(not (Flag408-))
)

)
(:action Prim408Action-1
:parameters ()
:precondition
(and
(not (LEFTOF9-ROBOT))
)
:effect
(and
(Flag408prime-)
(not (Flag408-))
)

)
(:action Prim409Action-0
:parameters ()
:precondition
(and
(not (LEFTOF9-ROBOT))
)
:effect
(and
(Flag409prime-)
(not (Flag409-))
)

)
(:action Prim409Action-1
:parameters ()
:precondition
(and
(not (RIGHTOF15-ROBOT))
)
:effect
(and
(Flag409prime-)
(not (Flag409-))
)

)
(:action Prim410Action-0
:parameters ()
:precondition
(and
(not (RIGHTOF10-ROBOT))
)
:effect
(and
(Flag410prime-)
(not (Flag410-))
)

)
(:action Prim410Action-1
:parameters ()
:precondition
(and
(not (LEFTOF9-ROBOT))
)
:effect
(and
(Flag410prime-)
(not (Flag410-))
)

)
(:action Prim411Action-0
:parameters ()
:precondition
(and
(not (LEFTOF9-ROBOT))
)
:effect
(and
(Flag411prime-)
(not (Flag411-))
)

)
(:action Prim411Action-1
:parameters ()
:precondition
(and
(not (RIGHTOF16-ROBOT))
)
:effect
(and
(Flag411prime-)
(not (Flag411-))
)

)
(:action Prim412Action-0
:parameters ()
:precondition
(and
(not (LEFTOF9-ROBOT))
)
:effect
(and
(Flag412prime-)
(not (Flag412-))
)

)
(:action Prim412Action-1
:parameters ()
:precondition
(and
(not (RIGHTOF17-ROBOT))
)
:effect
(and
(Flag412prime-)
(not (Flag412-))
)

)
(:action Prim413Action-0
:parameters ()
:precondition
(and
(not (LEFTOF9-ROBOT))
)
:effect
(and
(Flag413prime-)
(not (Flag413-))
)

)
(:action Prim413Action-1
:parameters ()
:precondition
(and
(not (RIGHTOF14-ROBOT))
)
:effect
(and
(Flag413prime-)
(not (Flag413-))
)

)
(:action Flag415Action
:parameters ()
:precondition
(and
(LEFTOF10-ROBOT)
(RIGHTOF12-ROBOT)
)
:effect
(and
(Flag415-)
(Flag415prime-)
)

)
(:action Flag416Action
:parameters ()
:precondition
(and
(RIGHTOF17-ROBOT)
(LEFTOF10-ROBOT)
)
:effect
(and
(Flag416-)
(Flag416prime-)
)

)
(:action Flag417Action
:parameters ()
:precondition
(and
(RIGHTOF14-ROBOT)
(LEFTOF10-ROBOT)
)
:effect
(and
(Flag417-)
(Flag417prime-)
)

)
(:action Flag418Action
:parameters ()
:precondition
(and
(RIGHTOF13-ROBOT)
(LEFTOF10-ROBOT)
)
:effect
(and
(Flag418-)
(Flag418prime-)
)

)
(:action Flag419Action
:parameters ()
:precondition
(and
(LEFTOF10-ROBOT)
(RIGHTOF9-ROBOT)
)
:effect
(and
(Flag419-)
(Flag419prime-)
)

)
(:action Flag420Action
:parameters ()
:precondition
(and
(RIGHTOF11-ROBOT)
(LEFTOF10-ROBOT)
)
:effect
(and
(Flag420-)
(Flag420prime-)
)

)
(:action Flag421Action
:parameters ()
:precondition
(and
(RIGHTOF10-ROBOT)
(LEFTOF10-ROBOT)
)
:effect
(and
(Flag421-)
(Flag421prime-)
)

)
(:action Flag422Action
:parameters ()
:precondition
(and
(RIGHTOF16-ROBOT)
(LEFTOF10-ROBOT)
)
:effect
(and
(Flag422-)
(Flag422prime-)
)

)
(:action Flag423Action
:parameters ()
:precondition
(and
(LEFTOF10-ROBOT)
(RIGHTOF15-ROBOT)
)
:effect
(and
(Flag423-)
(Flag423prime-)
)

)
(:action Prim415Action-0
:parameters ()
:precondition
(and
(not (LEFTOF10-ROBOT))
)
:effect
(and
(Flag415prime-)
(not (Flag415-))
)

)
(:action Prim415Action-1
:parameters ()
:precondition
(and
(not (RIGHTOF12-ROBOT))
)
:effect
(and
(Flag415prime-)
(not (Flag415-))
)

)
(:action Prim416Action-0
:parameters ()
:precondition
(and
(not (RIGHTOF17-ROBOT))
)
:effect
(and
(Flag416prime-)
(not (Flag416-))
)

)
(:action Prim416Action-1
:parameters ()
:precondition
(and
(not (LEFTOF10-ROBOT))
)
:effect
(and
(Flag416prime-)
(not (Flag416-))
)

)
(:action Prim417Action-0
:parameters ()
:precondition
(and
(not (RIGHTOF14-ROBOT))
)
:effect
(and
(Flag417prime-)
(not (Flag417-))
)

)
(:action Prim417Action-1
:parameters ()
:precondition
(and
(not (LEFTOF10-ROBOT))
)
:effect
(and
(Flag417prime-)
(not (Flag417-))
)

)
(:action Prim418Action-0
:parameters ()
:precondition
(and
(not (RIGHTOF13-ROBOT))
)
:effect
(and
(Flag418prime-)
(not (Flag418-))
)

)
(:action Prim418Action-1
:parameters ()
:precondition
(and
(not (LEFTOF10-ROBOT))
)
:effect
(and
(Flag418prime-)
(not (Flag418-))
)

)
(:action Prim419Action-0
:parameters ()
:precondition
(and
(not (LEFTOF10-ROBOT))
)
:effect
(and
(Flag419prime-)
(not (Flag419-))
)

)
(:action Prim419Action-1
:parameters ()
:precondition
(and
(not (RIGHTOF9-ROBOT))
)
:effect
(and
(Flag419prime-)
(not (Flag419-))
)

)
(:action Prim420Action-0
:parameters ()
:precondition
(and
(not (RIGHTOF11-ROBOT))
)
:effect
(and
(Flag420prime-)
(not (Flag420-))
)

)
(:action Prim420Action-1
:parameters ()
:precondition
(and
(not (LEFTOF10-ROBOT))
)
:effect
(and
(Flag420prime-)
(not (Flag420-))
)

)
(:action Prim421Action-0
:parameters ()
:precondition
(and
(not (RIGHTOF10-ROBOT))
)
:effect
(and
(Flag421prime-)
(not (Flag421-))
)

)
(:action Prim421Action-1
:parameters ()
:precondition
(and
(not (LEFTOF10-ROBOT))
)
:effect
(and
(Flag421prime-)
(not (Flag421-))
)

)
(:action Prim422Action-0
:parameters ()
:precondition
(and
(not (RIGHTOF16-ROBOT))
)
:effect
(and
(Flag422prime-)
(not (Flag422-))
)

)
(:action Prim422Action-1
:parameters ()
:precondition
(and
(not (LEFTOF10-ROBOT))
)
:effect
(and
(Flag422prime-)
(not (Flag422-))
)

)
(:action Prim423Action-0
:parameters ()
:precondition
(and
(not (LEFTOF10-ROBOT))
)
:effect
(and
(Flag423prime-)
(not (Flag423-))
)

)
(:action Prim423Action-1
:parameters ()
:precondition
(and
(not (RIGHTOF15-ROBOT))
)
:effect
(and
(Flag423prime-)
(not (Flag423-))
)

)
(:action Flag425Action
:parameters ()
:precondition
(and
(LEFTOF11-ROBOT)
(RIGHTOF15-ROBOT)
)
:effect
(and
(Flag425-)
(Flag425prime-)
)

)
(:action Flag426Action
:parameters ()
:precondition
(and
(LEFTOF11-ROBOT)
(RIGHTOF16-ROBOT)
)
:effect
(and
(Flag426-)
(Flag426prime-)
)

)
(:action Flag427Action
:parameters ()
:precondition
(and
(RIGHTOF11-ROBOT)
(LEFTOF11-ROBOT)
)
:effect
(and
(Flag427-)
(Flag427prime-)
)

)
(:action Flag428Action
:parameters ()
:precondition
(and
(LEFTOF10-ROBOT)
(RIGHTOF14-ROBOT)
)
:effect
(and
(Flag428-)
(Flag428prime-)
)

)
(:action Flag429Action
:parameters ()
:precondition
(and
(RIGHTOF13-ROBOT)
(LEFTOF11-ROBOT)
)
:effect
(and
(Flag429-)
(Flag429prime-)
)

)
(:action Flag430Action
:parameters ()
:precondition
(and
(LEFTOF11-ROBOT)
(RIGHTOF14-ROBOT)
)
:effect
(and
(Flag430-)
(Flag430prime-)
)

)
(:action Flag431Action
:parameters ()
:precondition
(and
(LEFTOF11-ROBOT)
(RIGHTOF12-ROBOT)
)
:effect
(and
(Flag431-)
(Flag431prime-)
)

)
(:action Flag432Action
:parameters ()
:precondition
(and
(LEFTOF11-ROBOT)
(RIGHTOF17-ROBOT)
)
:effect
(and
(Flag432-)
(Flag432prime-)
)

)
(:action Flag433Action
:parameters ()
:precondition
(and
(RIGHTOF10-ROBOT)
(LEFTOF11-ROBOT)
)
:effect
(and
(Flag433-)
(Flag433prime-)
)

)
(:action Prim425Action-0
:parameters ()
:precondition
(and
(not (LEFTOF11-ROBOT))
)
:effect
(and
(Flag425prime-)
(not (Flag425-))
)

)
(:action Prim425Action-1
:parameters ()
:precondition
(and
(not (RIGHTOF15-ROBOT))
)
:effect
(and
(Flag425prime-)
(not (Flag425-))
)

)
(:action Prim426Action-0
:parameters ()
:precondition
(and
(not (LEFTOF11-ROBOT))
)
:effect
(and
(Flag426prime-)
(not (Flag426-))
)

)
(:action Prim426Action-1
:parameters ()
:precondition
(and
(not (RIGHTOF16-ROBOT))
)
:effect
(and
(Flag426prime-)
(not (Flag426-))
)

)
(:action Prim427Action-0
:parameters ()
:precondition
(and
(not (RIGHTOF11-ROBOT))
)
:effect
(and
(Flag427prime-)
(not (Flag427-))
)

)
(:action Prim427Action-1
:parameters ()
:precondition
(and
(not (LEFTOF11-ROBOT))
)
:effect
(and
(Flag427prime-)
(not (Flag427-))
)

)
(:action Prim428Action-0
:parameters ()
:precondition
(and
(not (LEFTOF10-ROBOT))
)
:effect
(and
(Flag428prime-)
(not (Flag428-))
)

)
(:action Prim428Action-1
:parameters ()
:precondition
(and
(not (RIGHTOF14-ROBOT))
)
:effect
(and
(Flag428prime-)
(not (Flag428-))
)

)
(:action Prim429Action-0
:parameters ()
:precondition
(and
(not (RIGHTOF13-ROBOT))
)
:effect
(and
(Flag429prime-)
(not (Flag429-))
)

)
(:action Prim429Action-1
:parameters ()
:precondition
(and
(not (LEFTOF11-ROBOT))
)
:effect
(and
(Flag429prime-)
(not (Flag429-))
)

)
(:action Prim430Action-0
:parameters ()
:precondition
(and
(not (LEFTOF11-ROBOT))
)
:effect
(and
(Flag430prime-)
(not (Flag430-))
)

)
(:action Prim430Action-1
:parameters ()
:precondition
(and
(not (RIGHTOF14-ROBOT))
)
:effect
(and
(Flag430prime-)
(not (Flag430-))
)

)
(:action Prim431Action-0
:parameters ()
:precondition
(and
(not (LEFTOF11-ROBOT))
)
:effect
(and
(Flag431prime-)
(not (Flag431-))
)

)
(:action Prim431Action-1
:parameters ()
:precondition
(and
(not (RIGHTOF12-ROBOT))
)
:effect
(and
(Flag431prime-)
(not (Flag431-))
)

)
(:action Prim432Action-0
:parameters ()
:precondition
(and
(not (LEFTOF11-ROBOT))
)
:effect
(and
(Flag432prime-)
(not (Flag432-))
)

)
(:action Prim432Action-1
:parameters ()
:precondition
(and
(not (RIGHTOF17-ROBOT))
)
:effect
(and
(Flag432prime-)
(not (Flag432-))
)

)
(:action Prim433Action-0
:parameters ()
:precondition
(and
(not (RIGHTOF10-ROBOT))
)
:effect
(and
(Flag433prime-)
(not (Flag433-))
)

)
(:action Prim433Action-1
:parameters ()
:precondition
(and
(not (LEFTOF11-ROBOT))
)
:effect
(and
(Flag433prime-)
(not (Flag433-))
)

)
(:action Flag435Action
:parameters ()
:precondition
(and
(RIGHTOF14-ROBOT)
(LEFTOF12-ROBOT)
)
:effect
(and
(Flag435-)
(Flag435prime-)
)

)
(:action Flag436Action
:parameters ()
:precondition
(and
(RIGHTOF11-ROBOT)
(LEFTOF12-ROBOT)
)
:effect
(and
(Flag436-)
(Flag436prime-)
)

)
(:action Flag437Action
:parameters ()
:precondition
(and
(RIGHTOF12-ROBOT)
(LEFTOF12-ROBOT)
)
:effect
(and
(Flag437-)
(Flag437prime-)
)

)
(:action Flag438Action
:parameters ()
:precondition
(and
(RIGHTOF16-ROBOT)
(LEFTOF12-ROBOT)
)
:effect
(and
(Flag438-)
(Flag438prime-)
)

)
(:action Flag439Action
:parameters ()
:precondition
(and
(RIGHTOF17-ROBOT)
(LEFTOF12-ROBOT)
)
:effect
(and
(Flag439-)
(Flag439prime-)
)

)
(:action Flag440Action
:parameters ()
:precondition
(and
(RIGHTOF15-ROBOT)
(LEFTOF12-ROBOT)
)
:effect
(and
(Flag440-)
(Flag440prime-)
)

)
(:action Flag441Action
:parameters ()
:precondition
(and
(RIGHTOF13-ROBOT)
(LEFTOF12-ROBOT)
)
:effect
(and
(Flag441-)
(Flag441prime-)
)

)
(:action Prim435Action-0
:parameters ()
:precondition
(and
(not (RIGHTOF14-ROBOT))
)
:effect
(and
(Flag435prime-)
(not (Flag435-))
)

)
(:action Prim435Action-1
:parameters ()
:precondition
(and
(not (LEFTOF12-ROBOT))
)
:effect
(and
(Flag435prime-)
(not (Flag435-))
)

)
(:action Prim436Action-0
:parameters ()
:precondition
(and
(not (RIGHTOF11-ROBOT))
)
:effect
(and
(Flag436prime-)
(not (Flag436-))
)

)
(:action Prim436Action-1
:parameters ()
:precondition
(and
(not (LEFTOF12-ROBOT))
)
:effect
(and
(Flag436prime-)
(not (Flag436-))
)

)
(:action Prim437Action-0
:parameters ()
:precondition
(and
(not (RIGHTOF12-ROBOT))
)
:effect
(and
(Flag437prime-)
(not (Flag437-))
)

)
(:action Prim437Action-1
:parameters ()
:precondition
(and
(not (LEFTOF12-ROBOT))
)
:effect
(and
(Flag437prime-)
(not (Flag437-))
)

)
(:action Prim438Action-0
:parameters ()
:precondition
(and
(not (RIGHTOF16-ROBOT))
)
:effect
(and
(Flag438prime-)
(not (Flag438-))
)

)
(:action Prim438Action-1
:parameters ()
:precondition
(and
(not (LEFTOF12-ROBOT))
)
:effect
(and
(Flag438prime-)
(not (Flag438-))
)

)
(:action Prim439Action-0
:parameters ()
:precondition
(and
(not (RIGHTOF17-ROBOT))
)
:effect
(and
(Flag439prime-)
(not (Flag439-))
)

)
(:action Prim439Action-1
:parameters ()
:precondition
(and
(not (LEFTOF12-ROBOT))
)
:effect
(and
(Flag439prime-)
(not (Flag439-))
)

)
(:action Prim440Action-0
:parameters ()
:precondition
(and
(not (RIGHTOF15-ROBOT))
)
:effect
(and
(Flag440prime-)
(not (Flag440-))
)

)
(:action Prim440Action-1
:parameters ()
:precondition
(and
(not (LEFTOF12-ROBOT))
)
:effect
(and
(Flag440prime-)
(not (Flag440-))
)

)
(:action Prim441Action-0
:parameters ()
:precondition
(and
(not (RIGHTOF13-ROBOT))
)
:effect
(and
(Flag441prime-)
(not (Flag441-))
)

)
(:action Prim441Action-1
:parameters ()
:precondition
(and
(not (LEFTOF12-ROBOT))
)
:effect
(and
(Flag441prime-)
(not (Flag441-))
)

)
(:action Flag443Action
:parameters ()
:precondition
(and
(LEFTOF13-ROBOT)
(RIGHTOF15-ROBOT)
)
:effect
(and
(Flag443-)
(Flag443prime-)
)

)
(:action Flag444Action
:parameters ()
:precondition
(and
(LEFTOF13-ROBOT)
(RIGHTOF12-ROBOT)
)
:effect
(and
(Flag444-)
(Flag444prime-)
)

)
(:action Flag445Action
:parameters ()
:precondition
(and
(LEFTOF13-ROBOT)
(RIGHTOF14-ROBOT)
)
:effect
(and
(Flag445-)
(Flag445prime-)
)

)
(:action Flag446Action
:parameters ()
:precondition
(and
(RIGHTOF13-ROBOT)
(LEFTOF13-ROBOT)
)
:effect
(and
(Flag446-)
(Flag446prime-)
)

)
(:action Flag447Action
:parameters ()
:precondition
(and
(RIGHTOF17-ROBOT)
(LEFTOF13-ROBOT)
)
:effect
(and
(Flag447-)
(Flag447prime-)
)

)
(:action Flag448Action
:parameters ()
:precondition
(and
(RIGHTOF16-ROBOT)
(LEFTOF13-ROBOT)
)
:effect
(and
(Flag448-)
(Flag448prime-)
)

)
(:action Flag449Action
:parameters ()
:precondition
(and
(LEFTOF12-ROBOT)
(RIGHTOF12-ROBOT)
)
:effect
(and
(Flag449-)
(Flag449prime-)
)

)
(:action Prim443Action-0
:parameters ()
:precondition
(and
(not (LEFTOF13-ROBOT))
)
:effect
(and
(Flag443prime-)
(not (Flag443-))
)

)
(:action Prim443Action-1
:parameters ()
:precondition
(and
(not (RIGHTOF15-ROBOT))
)
:effect
(and
(Flag443prime-)
(not (Flag443-))
)

)
(:action Prim444Action-0
:parameters ()
:precondition
(and
(not (LEFTOF13-ROBOT))
)
:effect
(and
(Flag444prime-)
(not (Flag444-))
)

)
(:action Prim444Action-1
:parameters ()
:precondition
(and
(not (RIGHTOF12-ROBOT))
)
:effect
(and
(Flag444prime-)
(not (Flag444-))
)

)
(:action Prim445Action-0
:parameters ()
:precondition
(and
(not (LEFTOF13-ROBOT))
)
:effect
(and
(Flag445prime-)
(not (Flag445-))
)

)
(:action Prim445Action-1
:parameters ()
:precondition
(and
(not (RIGHTOF14-ROBOT))
)
:effect
(and
(Flag445prime-)
(not (Flag445-))
)

)
(:action Prim446Action-0
:parameters ()
:precondition
(and
(not (RIGHTOF13-ROBOT))
)
:effect
(and
(Flag446prime-)
(not (Flag446-))
)

)
(:action Prim446Action-1
:parameters ()
:precondition
(and
(not (LEFTOF13-ROBOT))
)
:effect
(and
(Flag446prime-)
(not (Flag446-))
)

)
(:action Prim447Action-0
:parameters ()
:precondition
(and
(not (RIGHTOF17-ROBOT))
)
:effect
(and
(Flag447prime-)
(not (Flag447-))
)

)
(:action Prim447Action-1
:parameters ()
:precondition
(and
(not (LEFTOF13-ROBOT))
)
:effect
(and
(Flag447prime-)
(not (Flag447-))
)

)
(:action Prim448Action-0
:parameters ()
:precondition
(and
(not (RIGHTOF16-ROBOT))
)
:effect
(and
(Flag448prime-)
(not (Flag448-))
)

)
(:action Prim448Action-1
:parameters ()
:precondition
(and
(not (LEFTOF13-ROBOT))
)
:effect
(and
(Flag448prime-)
(not (Flag448-))
)

)
(:action Prim449Action-0
:parameters ()
:precondition
(and
(not (LEFTOF12-ROBOT))
)
:effect
(and
(Flag449prime-)
(not (Flag449-))
)

)
(:action Prim449Action-1
:parameters ()
:precondition
(and
(not (RIGHTOF12-ROBOT))
)
:effect
(and
(Flag449prime-)
(not (Flag449-))
)

)
(:action Flag451Action
:parameters ()
:precondition
(and
(LEFTOF14-ROBOT)
(RIGHTOF14-ROBOT)
)
:effect
(and
(Flag451-)
(Flag451prime-)
)

)
(:action Flag452Action
:parameters ()
:precondition
(and
(RIGHTOF13-ROBOT)
(LEFTOF14-ROBOT)
)
:effect
(and
(Flag452-)
(Flag452prime-)
)

)
(:action Flag453Action
:parameters ()
:precondition
(and
(LEFTOF14-ROBOT)
(RIGHTOF16-ROBOT)
)
:effect
(and
(Flag453-)
(Flag453prime-)
)

)
(:action Flag454Action
:parameters ()
:precondition
(and
(LEFTOF14-ROBOT)
(RIGHTOF17-ROBOT)
)
:effect
(and
(Flag454-)
(Flag454prime-)
)

)
(:action Flag455Action
:parameters ()
:precondition
(and
(LEFTOF14-ROBOT)
(RIGHTOF15-ROBOT)
)
:effect
(and
(Flag455-)
(Flag455prime-)
)

)
(:action Prim451Action-0
:parameters ()
:precondition
(and
(not (LEFTOF14-ROBOT))
)
:effect
(and
(Flag451prime-)
(not (Flag451-))
)

)
(:action Prim451Action-1
:parameters ()
:precondition
(and
(not (RIGHTOF14-ROBOT))
)
:effect
(and
(Flag451prime-)
(not (Flag451-))
)

)
(:action Prim452Action-0
:parameters ()
:precondition
(and
(not (RIGHTOF13-ROBOT))
)
:effect
(and
(Flag452prime-)
(not (Flag452-))
)

)
(:action Prim452Action-1
:parameters ()
:precondition
(and
(not (LEFTOF14-ROBOT))
)
:effect
(and
(Flag452prime-)
(not (Flag452-))
)

)
(:action Prim453Action-0
:parameters ()
:precondition
(and
(not (LEFTOF14-ROBOT))
)
:effect
(and
(Flag453prime-)
(not (Flag453-))
)

)
(:action Prim453Action-1
:parameters ()
:precondition
(and
(not (RIGHTOF16-ROBOT))
)
:effect
(and
(Flag453prime-)
(not (Flag453-))
)

)
(:action Prim454Action-0
:parameters ()
:precondition
(and
(not (LEFTOF14-ROBOT))
)
:effect
(and
(Flag454prime-)
(not (Flag454-))
)

)
(:action Prim454Action-1
:parameters ()
:precondition
(and
(not (RIGHTOF17-ROBOT))
)
:effect
(and
(Flag454prime-)
(not (Flag454-))
)

)
(:action Prim455Action-0
:parameters ()
:precondition
(and
(not (LEFTOF14-ROBOT))
)
:effect
(and
(Flag455prime-)
(not (Flag455-))
)

)
(:action Prim455Action-1
:parameters ()
:precondition
(and
(not (RIGHTOF15-ROBOT))
)
:effect
(and
(Flag455prime-)
(not (Flag455-))
)

)
(:action Flag457Action
:parameters ()
:precondition
(and
(RIGHTOF16-ROBOT)
(LEFTOF15-ROBOT)
)
:effect
(and
(Flag457-)
(Flag457prime-)
)

)
(:action Flag458Action
:parameters ()
:precondition
(and
(RIGHTOF17-ROBOT)
(LEFTOF15-ROBOT)
)
:effect
(and
(Flag458-)
(Flag458prime-)
)

)
(:action Flag459Action
:parameters ()
:precondition
(and
(RIGHTOF15-ROBOT)
(LEFTOF15-ROBOT)
)
:effect
(and
(Flag459-)
(Flag459prime-)
)

)
(:action Flag460Action
:parameters ()
:precondition
(and
(RIGHTOF14-ROBOT)
(LEFTOF15-ROBOT)
)
:effect
(and
(Flag460-)
(Flag460prime-)
)

)
(:action Prim457Action-0
:parameters ()
:precondition
(and
(not (RIGHTOF16-ROBOT))
)
:effect
(and
(Flag457prime-)
(not (Flag457-))
)

)
(:action Prim457Action-1
:parameters ()
:precondition
(and
(not (LEFTOF15-ROBOT))
)
:effect
(and
(Flag457prime-)
(not (Flag457-))
)

)
(:action Prim458Action-0
:parameters ()
:precondition
(and
(not (RIGHTOF17-ROBOT))
)
:effect
(and
(Flag458prime-)
(not (Flag458-))
)

)
(:action Prim458Action-1
:parameters ()
:precondition
(and
(not (LEFTOF15-ROBOT))
)
:effect
(and
(Flag458prime-)
(not (Flag458-))
)

)
(:action Prim459Action-0
:parameters ()
:precondition
(and
(not (RIGHTOF15-ROBOT))
)
:effect
(and
(Flag459prime-)
(not (Flag459-))
)

)
(:action Prim459Action-1
:parameters ()
:precondition
(and
(not (LEFTOF15-ROBOT))
)
:effect
(and
(Flag459prime-)
(not (Flag459-))
)

)
(:action Prim460Action-0
:parameters ()
:precondition
(and
(not (RIGHTOF14-ROBOT))
)
:effect
(and
(Flag460prime-)
(not (Flag460-))
)

)
(:action Prim460Action-1
:parameters ()
:precondition
(and
(not (LEFTOF15-ROBOT))
)
:effect
(and
(Flag460prime-)
(not (Flag460-))
)

)
(:action Flag462Action
:parameters ()
:precondition
(and
(LEFTOF16-ROBOT)
(RIGHTOF17-ROBOT)
)
:effect
(and
(Flag462-)
(Flag462prime-)
)

)
(:action Flag463Action
:parameters ()
:precondition
(and
(LEFTOF15-ROBOT)
(RIGHTOF15-ROBOT)
)
:effect
(and
(Flag463-)
(Flag463prime-)
)

)
(:action Flag464Action
:parameters ()
:precondition
(and
(RIGHTOF16-ROBOT)
(LEFTOF16-ROBOT)
)
:effect
(and
(Flag464-)
(Flag464prime-)
)

)
(:action Flag465Action
:parameters ()
:precondition
(and
(LEFTOF16-ROBOT)
(RIGHTOF15-ROBOT)
)
:effect
(and
(Flag465-)
(Flag465prime-)
)

)
(:action Prim462Action-0
:parameters ()
:precondition
(and
(not (LEFTOF16-ROBOT))
)
:effect
(and
(Flag462prime-)
(not (Flag462-))
)

)
(:action Prim462Action-1
:parameters ()
:precondition
(and
(not (RIGHTOF17-ROBOT))
)
:effect
(and
(Flag462prime-)
(not (Flag462-))
)

)
(:action Prim463Action-0
:parameters ()
:precondition
(and
(not (LEFTOF15-ROBOT))
)
:effect
(and
(Flag463prime-)
(not (Flag463-))
)

)
(:action Prim463Action-1
:parameters ()
:precondition
(and
(not (RIGHTOF15-ROBOT))
)
:effect
(and
(Flag463prime-)
(not (Flag463-))
)

)
(:action Prim464Action-0
:parameters ()
:precondition
(and
(not (RIGHTOF16-ROBOT))
)
:effect
(and
(Flag464prime-)
(not (Flag464-))
)

)
(:action Prim464Action-1
:parameters ()
:precondition
(and
(not (LEFTOF16-ROBOT))
)
:effect
(and
(Flag464prime-)
(not (Flag464-))
)

)
(:action Prim465Action-0
:parameters ()
:precondition
(and
(not (LEFTOF16-ROBOT))
)
:effect
(and
(Flag465prime-)
(not (Flag465-))
)

)
(:action Prim465Action-1
:parameters ()
:precondition
(and
(not (RIGHTOF15-ROBOT))
)
:effect
(and
(Flag465prime-)
(not (Flag465-))
)

)
(:action Flag467Action
:parameters ()
:precondition
(and
(LEFTOF17-ROBOT)
(RIGHTOF17-ROBOT)
)
:effect
(and
(Flag467-)
(Flag467prime-)
)

)
(:action Flag468Action
:parameters ()
:precondition
(and
(LEFTOF17-ROBOT)
(RIGHTOF16-ROBOT)
)
:effect
(and
(Flag468-)
(Flag468prime-)
)

)
(:action Prim467Action-1
:parameters ()
:precondition
(and
(not (RIGHTOF17-ROBOT))
)
:effect
(and
(Flag467prime-)
(not (Flag467-))
)

)
(:action Prim468Action-1
:parameters ()
:precondition
(and
(not (RIGHTOF16-ROBOT))
)
:effect
(and
(Flag468prime-)
(not (Flag468-))
)

)
(:action Flag470Action
:parameters ()
:precondition
(and
(RIGHTOF17-ROBOT)
(LEFTOF18-ROBOT)
)
:effect
(and
(Flag470-)
(Flag470prime-)
)

)
(:action Prim470Action-0
:parameters ()
:precondition
(and
(not (RIGHTOF17-ROBOT))
)
:effect
(and
(Flag470prime-)
(not (Flag470-))
)

)
(:action Prim470Action-1
:parameters ()
:precondition
(and
(not (LEFTOF18-ROBOT))
)
:effect
(and
(Flag470prime-)
(not (Flag470-))
)

)
(:action Flag472Action-0
:parameters ()
:precondition
(and
(Flag66-)
(Flag66prime-)
)
:effect
(and
(Flag472-)
(Flag472prime-)
)

)
(:action Flag472Action-1
:parameters ()
:precondition
(and
(Flag54-)
(Flag54prime-)
)
:effect
(and
(Flag472-)
(Flag472prime-)
)

)
(:action Flag472Action-2
:parameters ()
:precondition
(and
(Flag39-)
(Flag39prime-)
)
:effect
(and
(Flag472-)
(Flag472prime-)
)

)
(:action Flag472Action-3
:parameters ()
:precondition
(and
(Flag68-)
(Flag68prime-)
)
:effect
(and
(Flag472-)
(Flag472prime-)
)

)
(:action Flag472Action-4
:parameters ()
:precondition
(and
(Flag56-)
(Flag56prime-)
)
:effect
(and
(Flag472-)
(Flag472prime-)
)

)
(:action Flag472Action-5
:parameters ()
:precondition
(and
(Flag46-)
(Flag46prime-)
)
:effect
(and
(Flag472-)
(Flag472prime-)
)

)
(:action Flag472Action-6
:parameters ()
:precondition
(and
(Flag40-)
(Flag40prime-)
)
:effect
(and
(Flag472-)
(Flag472prime-)
)

)
(:action Flag472Action-7
:parameters ()
:precondition
(and
(Flag57-)
(Flag57prime-)
)
:effect
(and
(Flag472-)
(Flag472prime-)
)

)
(:action Flag472Action-8
:parameters ()
:precondition
(and
(Flag48-)
(Flag48prime-)
)
:effect
(and
(Flag472-)
(Flag472prime-)
)

)
(:action Flag472Action-9
:parameters ()
:precondition
(and
(Flag58-)
(Flag58prime-)
)
:effect
(and
(Flag472-)
(Flag472prime-)
)

)
(:action Flag472Action-10
:parameters ()
:precondition
(and
(Flag45-)
(Flag45prime-)
)
:effect
(and
(Flag472-)
(Flag472prime-)
)

)
(:action Flag472Action-11
:parameters ()
:precondition
(and
(Flag60-)
(Flag60prime-)
)
:effect
(and
(Flag472-)
(Flag472prime-)
)

)
(:action Flag472Action-13
:parameters ()
:precondition
(and
(Flag70-)
(Flag70prime-)
)
:effect
(and
(Flag472-)
(Flag472prime-)
)

)
(:action Flag472Action-14
:parameters ()
:precondition
(and
(Flag71-)
(Flag71prime-)
)
:effect
(and
(Flag472-)
(Flag472prime-)
)

)
(:action Flag472Action-15
:parameters ()
:precondition
(and
(Flag62-)
(Flag62prime-)
)
:effect
(and
(Flag472-)
(Flag472prime-)
)

)
(:action Flag472Action-16
:parameters ()
:precondition
(and
(Flag63-)
(Flag63prime-)
)
:effect
(and
(Flag472-)
(Flag472prime-)
)

)
(:action Flag472Action-17
:parameters ()
:precondition
(and
(Flag38-)
(Flag38prime-)
)
:effect
(and
(Flag472-)
(Flag472prime-)
)

)
(:action Prim472Action
:parameters ()
:precondition
(and
(not (Flag66-))
(Flag66prime-)
(not (Flag54-))
(Flag54prime-)
(not (Flag39-))
(Flag39prime-)
(not (Flag68-))
(Flag68prime-)
(not (Flag56-))
(Flag56prime-)
(not (Flag46-))
(Flag46prime-)
(not (Flag40-))
(Flag40prime-)
(not (Flag57-))
(Flag57prime-)
(not (Flag48-))
(Flag48prime-)
(not (Flag58-))
(Flag58prime-)
(not (Flag45-))
(Flag45prime-)
(not (Flag60-))
(Flag60prime-)
(not (BELOWOF1-ROBOT))
(not (Flag70-))
(Flag70prime-)
(not (Flag71-))
(Flag71prime-)
(not (Flag62-))
(Flag62prime-)
(not (Flag63-))
(Flag63prime-)
(not (Flag38-))
(Flag38prime-)
)
:effect
(and
(Flag472prime-)
(not (Flag472-))
)

)
(:action Flag473Action-0
:parameters ()
:precondition
(and
(RIGHTOF3-ROBOT)
)
:effect
(and
(Flag473-)
(Flag473prime-)
)

)
(:action Flag473Action-1
:parameters ()
:precondition
(and
(RIGHTOF10-ROBOT)
)
:effect
(and
(Flag473-)
(Flag473prime-)
)

)
(:action Flag473Action-2
:parameters ()
:precondition
(and
(RIGHTOF5-ROBOT)
)
:effect
(and
(Flag473-)
(Flag473prime-)
)

)
(:action Flag473Action-3
:parameters ()
:precondition
(and
(RIGHTOF9-ROBOT)
)
:effect
(and
(Flag473-)
(Flag473prime-)
)

)
(:action Flag473Action-5
:parameters ()
:precondition
(and
(RIGHTOF17-ROBOT)
)
:effect
(and
(Flag473-)
(Flag473prime-)
)

)
(:action Flag473Action-6
:parameters ()
:precondition
(and
(RIGHTOF14-ROBOT)
)
:effect
(and
(Flag473-)
(Flag473prime-)
)

)
(:action Flag473Action-7
:parameters ()
:precondition
(and
(RIGHTOF13-ROBOT)
)
:effect
(and
(Flag473-)
(Flag473prime-)
)

)
(:action Flag473Action-8
:parameters ()
:precondition
(and
(RIGHTOF12-ROBOT)
)
:effect
(and
(Flag473-)
(Flag473prime-)
)

)
(:action Flag473Action-9
:parameters ()
:precondition
(and
(RIGHTOF0-ROBOT)
)
:effect
(and
(Flag473-)
(Flag473prime-)
)

)
(:action Flag473Action-10
:parameters ()
:precondition
(and
(RIGHTOF8-ROBOT)
)
:effect
(and
(Flag473-)
(Flag473prime-)
)

)
(:action Flag473Action-11
:parameters ()
:precondition
(and
(RIGHTOF4-ROBOT)
)
:effect
(and
(Flag473-)
(Flag473prime-)
)

)
(:action Flag473Action-12
:parameters ()
:precondition
(and
(RIGHTOF1-ROBOT)
)
:effect
(and
(Flag473-)
(Flag473prime-)
)

)
(:action Flag473Action-13
:parameters ()
:precondition
(and
(RIGHTOF16-ROBOT)
)
:effect
(and
(Flag473-)
(Flag473prime-)
)

)
(:action Flag473Action-14
:parameters ()
:precondition
(and
(RIGHTOF7-ROBOT)
)
:effect
(and
(Flag473-)
(Flag473prime-)
)

)
(:action Flag473Action-15
:parameters ()
:precondition
(and
(RIGHTOF11-ROBOT)
)
:effect
(and
(Flag473-)
(Flag473prime-)
)

)
(:action Flag473Action-16
:parameters ()
:precondition
(and
(RIGHTOF15-ROBOT)
)
:effect
(and
(Flag473-)
(Flag473prime-)
)

)
(:action Flag473Action-17
:parameters ()
:precondition
(and
(RIGHTOF6-ROBOT)
)
:effect
(and
(Flag473-)
(Flag473prime-)
)

)
(:action Prim473Action
:parameters ()
:precondition
(and
(not (RIGHTOF3-ROBOT))
(not (RIGHTOF10-ROBOT))
(not (RIGHTOF5-ROBOT))
(not (RIGHTOF9-ROBOT))
(not (RIGHTOF2-ROBOT))
(not (RIGHTOF17-ROBOT))
(not (RIGHTOF14-ROBOT))
(not (RIGHTOF13-ROBOT))
(not (RIGHTOF12-ROBOT))
(not (RIGHTOF0-ROBOT))
(not (RIGHTOF8-ROBOT))
(not (RIGHTOF4-ROBOT))
(not (RIGHTOF1-ROBOT))
(not (RIGHTOF16-ROBOT))
(not (RIGHTOF7-ROBOT))
(not (RIGHTOF11-ROBOT))
(not (RIGHTOF15-ROBOT))
(not (RIGHTOF6-ROBOT))
)
:effect
(and
(Flag473prime-)
(not (Flag473-))
)

)
(:action Flag474Action
:parameters ()
:precondition
(and
(RIGHTOF0-ROBOT)
(LEFTOF1-ROBOT)
)
:effect
(and
(Flag474-)
(Flag474prime-)
)

)
(:action Prim474Action-0
:parameters ()
:precondition
(and
(not (RIGHTOF0-ROBOT))
)
:effect
(and
(Flag474prime-)
(not (Flag474-))
)

)
(:action Prim474Action-1
:parameters ()
:precondition
(and
(not (LEFTOF1-ROBOT))
)
:effect
(and
(Flag474prime-)
(not (Flag474-))
)

)
(:action MOVERIGHT-ROBOT
:parameters ()
:precondition
(and
(Flag3-)
(Flag3prime-)
(Flag469prime-)
(Flag466prime-)
(Flag461prime-)
(Flag456prime-)
(Flag450prime-)
(Flag442prime-)
(Flag434prime-)
(Flag424prime-)
(Flag414prime-)
(Flag402prime-)
(Flag389prime-)
(Flag374prime-)
(Flag360prime-)
(Flag343prime-)
(Flag325prime-)
(Flag308prime-)
(Flag475prime-)
(Flag473prime-)
)
:effect
(and
(when
(and
(Flag469-)
)
(and
(COLUMN17-ROBOT)
(not (NOT-COLUMN17-ROBOT))
)
)
(when
(and
(Flag466-)
)
(and
(COLUMN16-ROBOT)
(not (NOT-COLUMN16-ROBOT))
)
)
(when
(and
(Flag461-)
)
(and
(COLUMN15-ROBOT)
(not (NOT-COLUMN15-ROBOT))
)
)
(when
(and
(Flag456-)
)
(and
(COLUMN14-ROBOT)
(not (NOT-COLUMN14-ROBOT))
)
)
(when
(and
(Flag450-)
)
(and
(COLUMN13-ROBOT)
(not (NOT-COLUMN13-ROBOT))
)
)
(when
(and
(Flag442-)
)
(and
(COLUMN12-ROBOT)
(not (NOT-COLUMN12-ROBOT))
)
)
(when
(and
(Flag434-)
)
(and
(COLUMN11-ROBOT)
(not (NOT-COLUMN11-ROBOT))
)
)
(when
(and
(Flag424-)
)
(and
(COLUMN10-ROBOT)
(not (NOT-COLUMN10-ROBOT))
)
)
(when
(and
(Flag414-)
)
(and
(COLUMN9-ROBOT)
(not (NOT-COLUMN9-ROBOT))
)
)
(when
(and
(Flag402-)
)
(and
(COLUMN8-ROBOT)
(not (NOT-COLUMN8-ROBOT))
)
)
(when
(and
(Flag389-)
)
(and
(COLUMN7-ROBOT)
(not (NOT-COLUMN7-ROBOT))
)
)
(when
(and
(Flag374-)
)
(and
(COLUMN6-ROBOT)
(not (NOT-COLUMN6-ROBOT))
)
)
(when
(and
(Flag360-)
)
(and
(COLUMN5-ROBOT)
(not (NOT-COLUMN5-ROBOT))
)
)
(when
(and
(Flag343-)
)
(and
(COLUMN4-ROBOT)
(not (NOT-COLUMN4-ROBOT))
)
)
(when
(and
(Flag325-)
)
(and
(COLUMN3-ROBOT)
(not (NOT-COLUMN3-ROBOT))
)
)
(when
(and
(Flag308-)
)
(and
(COLUMN2-ROBOT)
(not (NOT-COLUMN2-ROBOT))
)
)
(when
(and
(Flag475-)
)
(and
(COLUMN1-ROBOT)
(not (NOT-COLUMN1-ROBOT))
)
)
(when
(and
(COLUMN16-ROBOT)
)
(and
(COLUMN17-ROBOT)
(NOT-COLUMN16-ROBOT)
(not (NOT-COLUMN17-ROBOT))
(not (COLUMN16-ROBOT))
)
)
(when
(and
(COLUMN15-ROBOT)
)
(and
(COLUMN16-ROBOT)
(NOT-COLUMN15-ROBOT)
(not (NOT-COLUMN16-ROBOT))
(not (COLUMN15-ROBOT))
)
)
(when
(and
(COLUMN14-ROBOT)
)
(and
(COLUMN15-ROBOT)
(NOT-COLUMN14-ROBOT)
(not (NOT-COLUMN15-ROBOT))
(not (COLUMN14-ROBOT))
)
)
(when
(and
(COLUMN13-ROBOT)
)
(and
(COLUMN14-ROBOT)
(NOT-COLUMN13-ROBOT)
(not (NOT-COLUMN14-ROBOT))
(not (COLUMN13-ROBOT))
)
)
(when
(and
(COLUMN12-ROBOT)
)
(and
(COLUMN13-ROBOT)
(NOT-COLUMN12-ROBOT)
(not (NOT-COLUMN13-ROBOT))
(not (COLUMN12-ROBOT))
)
)
(when
(and
(COLUMN11-ROBOT)
)
(and
(COLUMN12-ROBOT)
(NOT-COLUMN11-ROBOT)
(not (NOT-COLUMN12-ROBOT))
(not (COLUMN11-ROBOT))
)
)
(when
(and
(COLUMN10-ROBOT)
)
(and
(COLUMN11-ROBOT)
(NOT-COLUMN10-ROBOT)
(not (NOT-COLUMN11-ROBOT))
(not (COLUMN10-ROBOT))
)
)
(when
(and
(COLUMN9-ROBOT)
)
(and
(COLUMN10-ROBOT)
(NOT-COLUMN9-ROBOT)
(not (NOT-COLUMN10-ROBOT))
(not (COLUMN9-ROBOT))
)
)
(when
(and
(COLUMN8-ROBOT)
)
(and
(COLUMN9-ROBOT)
(NOT-COLUMN8-ROBOT)
(not (NOT-COLUMN9-ROBOT))
(not (COLUMN8-ROBOT))
)
)
(when
(and
(COLUMN7-ROBOT)
)
(and
(COLUMN8-ROBOT)
(NOT-COLUMN7-ROBOT)
(not (NOT-COLUMN8-ROBOT))
(not (COLUMN7-ROBOT))
)
)
(when
(and
(COLUMN6-ROBOT)
)
(and
(COLUMN7-ROBOT)
(NOT-COLUMN6-ROBOT)
(not (NOT-COLUMN7-ROBOT))
(not (COLUMN6-ROBOT))
)
)
(when
(and
(COLUMN5-ROBOT)
)
(and
(COLUMN6-ROBOT)
(NOT-COLUMN5-ROBOT)
(not (NOT-COLUMN6-ROBOT))
(not (COLUMN5-ROBOT))
)
)
(when
(and
(COLUMN4-ROBOT)
)
(and
(COLUMN5-ROBOT)
(NOT-COLUMN4-ROBOT)
(not (NOT-COLUMN5-ROBOT))
(not (COLUMN4-ROBOT))
)
)
(when
(and
(COLUMN3-ROBOT)
)
(and
(COLUMN4-ROBOT)
(NOT-COLUMN3-ROBOT)
(not (NOT-COLUMN4-ROBOT))
(not (COLUMN3-ROBOT))
)
)
(when
(and
(COLUMN2-ROBOT)
)
(and
(COLUMN3-ROBOT)
(NOT-COLUMN2-ROBOT)
(not (NOT-COLUMN3-ROBOT))
(not (COLUMN2-ROBOT))
)
)
(when
(and
(COLUMN1-ROBOT)
)
(and
(COLUMN2-ROBOT)
(NOT-COLUMN1-ROBOT)
(not (NOT-COLUMN2-ROBOT))
(not (COLUMN1-ROBOT))
)
)
(when
(and
(COLUMN0-ROBOT)
)
(and
(COLUMN1-ROBOT)
(NOT-COLUMN0-ROBOT)
(not (NOT-COLUMN1-ROBOT))
(not (COLUMN0-ROBOT))
)
)
(when
(and
(LEFTOF8-ROBOT)
)
(and
(LEFTOF18-ROBOT)
(NOT-LEFTOF17-ROBOT)
(LEFTOF17-ROBOT)
(NOT-LEFTOF16-ROBOT)
(LEFTOF16-ROBOT)
(NOT-LEFTOF15-ROBOT)
(LEFTOF15-ROBOT)
(NOT-LEFTOF14-ROBOT)
(LEFTOF14-ROBOT)
(NOT-LEFTOF13-ROBOT)
(LEFTOF13-ROBOT)
(NOT-LEFTOF12-ROBOT)
(LEFTOF12-ROBOT)
(NOT-LEFTOF11-ROBOT)
(LEFTOF11-ROBOT)
(NOT-LEFTOF10-ROBOT)
(LEFTOF10-ROBOT)
(NOT-LEFTOF9-ROBOT)
(LEFTOF9-ROBOT)
(NOT-LEFTOF8-ROBOT)
(not (NOT-LEFTOF18-ROBOT))
(not (LEFTOF8-ROBOT))
)
)
(when
(and
(LEFTOF7-ROBOT)
)
(and
(LEFTOF18-ROBOT)
(NOT-LEFTOF17-ROBOT)
(LEFTOF17-ROBOT)
(NOT-LEFTOF16-ROBOT)
(LEFTOF16-ROBOT)
(NOT-LEFTOF15-ROBOT)
(LEFTOF15-ROBOT)
(NOT-LEFTOF14-ROBOT)
(LEFTOF14-ROBOT)
(NOT-LEFTOF13-ROBOT)
(LEFTOF13-ROBOT)
(NOT-LEFTOF12-ROBOT)
(LEFTOF12-ROBOT)
(NOT-LEFTOF11-ROBOT)
(LEFTOF11-ROBOT)
(NOT-LEFTOF10-ROBOT)
(LEFTOF10-ROBOT)
(NOT-LEFTOF9-ROBOT)
(LEFTOF9-ROBOT)
(NOT-LEFTOF8-ROBOT)
(LEFTOF8-ROBOT)
(NOT-LEFTOF7-ROBOT)
(not (NOT-LEFTOF18-ROBOT))
(not (LEFTOF7-ROBOT))
)
)
(when
(and
(LEFTOF4-ROBOT)
)
(and
(LEFTOF18-ROBOT)
(NOT-LEFTOF17-ROBOT)
(LEFTOF17-ROBOT)
(NOT-LEFTOF16-ROBOT)
(LEFTOF16-ROBOT)
(NOT-LEFTOF15-ROBOT)
(LEFTOF15-ROBOT)
(NOT-LEFTOF14-ROBOT)
(LEFTOF14-ROBOT)
(NOT-LEFTOF13-ROBOT)
(LEFTOF13-ROBOT)
(NOT-LEFTOF12-ROBOT)
(LEFTOF12-ROBOT)
(NOT-LEFTOF11-ROBOT)
(LEFTOF11-ROBOT)
(NOT-LEFTOF10-ROBOT)
(LEFTOF10-ROBOT)
(NOT-LEFTOF9-ROBOT)
(LEFTOF9-ROBOT)
(NOT-LEFTOF8-ROBOT)
(LEFTOF8-ROBOT)
(NOT-LEFTOF7-ROBOT)
(LEFTOF7-ROBOT)
(NOT-LEFTOF6-ROBOT)
(LEFTOF6-ROBOT)
(NOT-LEFTOF5-ROBOT)
(LEFTOF5-ROBOT)
(NOT-LEFTOF4-ROBOT)
(not (NOT-LEFTOF18-ROBOT))
(not (LEFTOF4-ROBOT))
)
)
(when
(and
(LEFTOF3-ROBOT)
)
(and
(LEFTOF18-ROBOT)
(NOT-LEFTOF17-ROBOT)
(LEFTOF17-ROBOT)
(NOT-LEFTOF16-ROBOT)
(LEFTOF16-ROBOT)
(NOT-LEFTOF15-ROBOT)
(LEFTOF15-ROBOT)
(NOT-LEFTOF14-ROBOT)
(LEFTOF14-ROBOT)
(NOT-LEFTOF13-ROBOT)
(LEFTOF13-ROBOT)
(NOT-LEFTOF12-ROBOT)
(LEFTOF12-ROBOT)
(NOT-LEFTOF11-ROBOT)
(LEFTOF11-ROBOT)
(NOT-LEFTOF10-ROBOT)
(LEFTOF10-ROBOT)
(NOT-LEFTOF9-ROBOT)
(LEFTOF9-ROBOT)
(NOT-LEFTOF8-ROBOT)
(LEFTOF8-ROBOT)
(NOT-LEFTOF7-ROBOT)
(LEFTOF7-ROBOT)
(NOT-LEFTOF6-ROBOT)
(LEFTOF6-ROBOT)
(NOT-LEFTOF5-ROBOT)
(LEFTOF5-ROBOT)
(NOT-LEFTOF4-ROBOT)
(LEFTOF4-ROBOT)
(NOT-LEFTOF3-ROBOT)
(not (NOT-LEFTOF18-ROBOT))
(not (LEFTOF3-ROBOT))
)
)
(when
(and
(LEFTOF14-ROBOT)
)
(and
(LEFTOF18-ROBOT)
(NOT-LEFTOF17-ROBOT)
(LEFTOF17-ROBOT)
(NOT-LEFTOF16-ROBOT)
(LEFTOF16-ROBOT)
(NOT-LEFTOF15-ROBOT)
(LEFTOF15-ROBOT)
(NOT-LEFTOF14-ROBOT)
(not (NOT-LEFTOF18-ROBOT))
(not (LEFTOF14-ROBOT))
)
)
(when
(and
(LEFTOF16-ROBOT)
)
(and
(LEFTOF18-ROBOT)
(NOT-LEFTOF17-ROBOT)
(LEFTOF17-ROBOT)
(NOT-LEFTOF16-ROBOT)
(not (NOT-LEFTOF18-ROBOT))
(not (LEFTOF16-ROBOT))
)
)
(when
(and
(LEFTOF5-ROBOT)
)
(and
(LEFTOF18-ROBOT)
(NOT-LEFTOF17-ROBOT)
(LEFTOF17-ROBOT)
(NOT-LEFTOF16-ROBOT)
(LEFTOF16-ROBOT)
(NOT-LEFTOF15-ROBOT)
(LEFTOF15-ROBOT)
(NOT-LEFTOF14-ROBOT)
(LEFTOF14-ROBOT)
(NOT-LEFTOF13-ROBOT)
(LEFTOF13-ROBOT)
(NOT-LEFTOF12-ROBOT)
(LEFTOF12-ROBOT)
(NOT-LEFTOF11-ROBOT)
(LEFTOF11-ROBOT)
(NOT-LEFTOF10-ROBOT)
(LEFTOF10-ROBOT)
(NOT-LEFTOF9-ROBOT)
(LEFTOF9-ROBOT)
(NOT-LEFTOF8-ROBOT)
(LEFTOF8-ROBOT)
(NOT-LEFTOF7-ROBOT)
(LEFTOF7-ROBOT)
(NOT-LEFTOF6-ROBOT)
(LEFTOF6-ROBOT)
(NOT-LEFTOF5-ROBOT)
(not (NOT-LEFTOF18-ROBOT))
(not (LEFTOF5-ROBOT))
)
)
(when
(and
(LEFTOF6-ROBOT)
)
(and
(LEFTOF18-ROBOT)
(NOT-LEFTOF17-ROBOT)
(LEFTOF17-ROBOT)
(NOT-LEFTOF16-ROBOT)
(LEFTOF16-ROBOT)
(NOT-LEFTOF15-ROBOT)
(LEFTOF15-ROBOT)
(NOT-LEFTOF14-ROBOT)
(LEFTOF14-ROBOT)
(NOT-LEFTOF13-ROBOT)
(LEFTOF13-ROBOT)
(NOT-LEFTOF12-ROBOT)
(LEFTOF12-ROBOT)
(NOT-LEFTOF11-ROBOT)
(LEFTOF11-ROBOT)
(NOT-LEFTOF10-ROBOT)
(LEFTOF10-ROBOT)
(NOT-LEFTOF9-ROBOT)
(LEFTOF9-ROBOT)
(NOT-LEFTOF8-ROBOT)
(LEFTOF8-ROBOT)
(NOT-LEFTOF7-ROBOT)
(LEFTOF7-ROBOT)
(NOT-LEFTOF6-ROBOT)
(not (NOT-LEFTOF18-ROBOT))
(not (LEFTOF6-ROBOT))
)
)
(when
(and
(LEFTOF12-ROBOT)
)
(and
(LEFTOF18-ROBOT)
(NOT-LEFTOF17-ROBOT)
(LEFTOF17-ROBOT)
(NOT-LEFTOF16-ROBOT)
(LEFTOF16-ROBOT)
(NOT-LEFTOF15-ROBOT)
(LEFTOF15-ROBOT)
(NOT-LEFTOF14-ROBOT)
(LEFTOF14-ROBOT)
(NOT-LEFTOF13-ROBOT)
(LEFTOF13-ROBOT)
(NOT-LEFTOF12-ROBOT)
(not (NOT-LEFTOF18-ROBOT))
(not (LEFTOF12-ROBOT))
)
)
(when
(and
(LEFTOF1-ROBOT)
)
(and
(LEFTOF18-ROBOT)
(NOT-LEFTOF17-ROBOT)
(LEFTOF17-ROBOT)
(NOT-LEFTOF16-ROBOT)
(LEFTOF16-ROBOT)
(NOT-LEFTOF15-ROBOT)
(LEFTOF15-ROBOT)
(NOT-LEFTOF14-ROBOT)
(LEFTOF14-ROBOT)
(NOT-LEFTOF13-ROBOT)
(LEFTOF13-ROBOT)
(NOT-LEFTOF12-ROBOT)
(LEFTOF12-ROBOT)
(NOT-LEFTOF11-ROBOT)
(LEFTOF11-ROBOT)
(NOT-LEFTOF10-ROBOT)
(LEFTOF10-ROBOT)
(NOT-LEFTOF9-ROBOT)
(LEFTOF9-ROBOT)
(NOT-LEFTOF8-ROBOT)
(LEFTOF8-ROBOT)
(NOT-LEFTOF7-ROBOT)
(LEFTOF7-ROBOT)
(NOT-LEFTOF6-ROBOT)
(LEFTOF6-ROBOT)
(NOT-LEFTOF5-ROBOT)
(LEFTOF5-ROBOT)
(NOT-LEFTOF4-ROBOT)
(LEFTOF4-ROBOT)
(NOT-LEFTOF3-ROBOT)
(LEFTOF3-ROBOT)
(NOT-LEFTOF2-ROBOT)
(LEFTOF2-ROBOT)
(NOT-LEFTOF1-ROBOT)
(not (NOT-LEFTOF18-ROBOT))
(not (LEFTOF1-ROBOT))
)
)
(when
(and
(LEFTOF13-ROBOT)
)
(and
(LEFTOF18-ROBOT)
(NOT-LEFTOF17-ROBOT)
(LEFTOF17-ROBOT)
(NOT-LEFTOF16-ROBOT)
(LEFTOF16-ROBOT)
(NOT-LEFTOF15-ROBOT)
(LEFTOF15-ROBOT)
(NOT-LEFTOF14-ROBOT)
(LEFTOF14-ROBOT)
(NOT-LEFTOF13-ROBOT)
(not (NOT-LEFTOF18-ROBOT))
(not (LEFTOF13-ROBOT))
)
)
(when
(and
(LEFTOF2-ROBOT)
)
(and
(LEFTOF18-ROBOT)
(NOT-LEFTOF17-ROBOT)
(LEFTOF17-ROBOT)
(NOT-LEFTOF16-ROBOT)
(LEFTOF16-ROBOT)
(NOT-LEFTOF15-ROBOT)
(LEFTOF15-ROBOT)
(NOT-LEFTOF14-ROBOT)
(LEFTOF14-ROBOT)
(NOT-LEFTOF13-ROBOT)
(LEFTOF13-ROBOT)
(NOT-LEFTOF12-ROBOT)
(LEFTOF12-ROBOT)
(NOT-LEFTOF11-ROBOT)
(LEFTOF11-ROBOT)
(NOT-LEFTOF10-ROBOT)
(LEFTOF10-ROBOT)
(NOT-LEFTOF9-ROBOT)
(LEFTOF9-ROBOT)
(NOT-LEFTOF8-ROBOT)
(LEFTOF8-ROBOT)
(NOT-LEFTOF7-ROBOT)
(LEFTOF7-ROBOT)
(NOT-LEFTOF6-ROBOT)
(LEFTOF6-ROBOT)
(NOT-LEFTOF5-ROBOT)
(LEFTOF5-ROBOT)
(NOT-LEFTOF4-ROBOT)
(LEFTOF4-ROBOT)
(NOT-LEFTOF3-ROBOT)
(LEFTOF3-ROBOT)
(NOT-LEFTOF2-ROBOT)
(not (NOT-LEFTOF18-ROBOT))
(not (LEFTOF2-ROBOT))
)
)
(when
(and
(LEFTOF9-ROBOT)
)
(and
(LEFTOF18-ROBOT)
(NOT-LEFTOF17-ROBOT)
(LEFTOF17-ROBOT)
(NOT-LEFTOF16-ROBOT)
(LEFTOF16-ROBOT)
(NOT-LEFTOF15-ROBOT)
(LEFTOF15-ROBOT)
(NOT-LEFTOF14-ROBOT)
(LEFTOF14-ROBOT)
(NOT-LEFTOF13-ROBOT)
(LEFTOF13-ROBOT)
(NOT-LEFTOF12-ROBOT)
(LEFTOF12-ROBOT)
(NOT-LEFTOF11-ROBOT)
(LEFTOF11-ROBOT)
(NOT-LEFTOF10-ROBOT)
(LEFTOF10-ROBOT)
(NOT-LEFTOF9-ROBOT)
(not (NOT-LEFTOF18-ROBOT))
(not (LEFTOF9-ROBOT))
)
)
(when
(and
(LEFTOF11-ROBOT)
)
(and
(LEFTOF18-ROBOT)
(NOT-LEFTOF17-ROBOT)
(LEFTOF17-ROBOT)
(NOT-LEFTOF16-ROBOT)
(LEFTOF16-ROBOT)
(NOT-LEFTOF15-ROBOT)
(LEFTOF15-ROBOT)
(NOT-LEFTOF14-ROBOT)
(LEFTOF14-ROBOT)
(NOT-LEFTOF13-ROBOT)
(LEFTOF13-ROBOT)
(NOT-LEFTOF12-ROBOT)
(LEFTOF12-ROBOT)
(NOT-LEFTOF11-ROBOT)
(not (NOT-LEFTOF18-ROBOT))
(not (LEFTOF11-ROBOT))
)
)
(when
(and
(LEFTOF17-ROBOT)
)
(and
(LEFTOF18-ROBOT)
(NOT-LEFTOF17-ROBOT)
(not (NOT-LEFTOF18-ROBOT))
(not (LEFTOF17-ROBOT))
)
)
(when
(and
(LEFTOF10-ROBOT)
)
(and
(LEFTOF18-ROBOT)
(NOT-LEFTOF17-ROBOT)
(LEFTOF17-ROBOT)
(NOT-LEFTOF16-ROBOT)
(LEFTOF16-ROBOT)
(NOT-LEFTOF15-ROBOT)
(LEFTOF15-ROBOT)
(NOT-LEFTOF14-ROBOT)
(LEFTOF14-ROBOT)
(NOT-LEFTOF13-ROBOT)
(LEFTOF13-ROBOT)
(NOT-LEFTOF12-ROBOT)
(LEFTOF12-ROBOT)
(NOT-LEFTOF11-ROBOT)
(LEFTOF11-ROBOT)
(NOT-LEFTOF10-ROBOT)
(not (NOT-LEFTOF18-ROBOT))
(not (LEFTOF10-ROBOT))
)
)
(when
(and
(LEFTOF15-ROBOT)
)
(and
(LEFTOF18-ROBOT)
(NOT-LEFTOF17-ROBOT)
(LEFTOF17-ROBOT)
(NOT-LEFTOF16-ROBOT)
(LEFTOF16-ROBOT)
(NOT-LEFTOF15-ROBOT)
(not (NOT-LEFTOF18-ROBOT))
(not (LEFTOF15-ROBOT))
)
)
(when
(and
(RIGHTOF17-ROBOT)
)
(and
(RIGHTOF16-ROBOT)
(RIGHTOF15-ROBOT)
(RIGHTOF14-ROBOT)
(RIGHTOF13-ROBOT)
(RIGHTOF12-ROBOT)
(RIGHTOF11-ROBOT)
(RIGHTOF10-ROBOT)
(RIGHTOF9-ROBOT)
(RIGHTOF8-ROBOT)
(RIGHTOF7-ROBOT)
(RIGHTOF6-ROBOT)
(RIGHTOF5-ROBOT)
(RIGHTOF4-ROBOT)
(RIGHTOF3-ROBOT)
(RIGHTOF2-ROBOT)
(not (NOT-RIGHTOF17-ROBOT))
(not (NOT-RIGHTOF16-ROBOT))
(not (NOT-RIGHTOF15-ROBOT))
(not (NOT-RIGHTOF14-ROBOT))
(not (NOT-RIGHTOF13-ROBOT))
(not (NOT-RIGHTOF12-ROBOT))
(not (NOT-RIGHTOF11-ROBOT))
(not (NOT-RIGHTOF10-ROBOT))
(not (NOT-RIGHTOF9-ROBOT))
(not (NOT-RIGHTOF8-ROBOT))
(not (NOT-RIGHTOF7-ROBOT))
(not (NOT-RIGHTOF6-ROBOT))
(not (NOT-RIGHTOF5-ROBOT))
(not (NOT-RIGHTOF4-ROBOT))
(not (NOT-RIGHTOF3-ROBOT))
(not (NOT-RIGHTOF2-ROBOT))
)
)
(when
(and
(RIGHTOF16-ROBOT)
)
(and
(RIGHTOF17-ROBOT)
(RIGHTOF15-ROBOT)
(RIGHTOF14-ROBOT)
(RIGHTOF13-ROBOT)
(RIGHTOF12-ROBOT)
(RIGHTOF11-ROBOT)
(RIGHTOF10-ROBOT)
(RIGHTOF9-ROBOT)
(RIGHTOF8-ROBOT)
(RIGHTOF7-ROBOT)
(RIGHTOF6-ROBOT)
(RIGHTOF5-ROBOT)
(RIGHTOF4-ROBOT)
(RIGHTOF3-ROBOT)
(RIGHTOF2-ROBOT)
(not (NOT-RIGHTOF17-ROBOT))
(not (NOT-RIGHTOF16-ROBOT))
(not (NOT-RIGHTOF15-ROBOT))
(not (NOT-RIGHTOF14-ROBOT))
(not (NOT-RIGHTOF13-ROBOT))
(not (NOT-RIGHTOF12-ROBOT))
(not (NOT-RIGHTOF11-ROBOT))
(not (NOT-RIGHTOF10-ROBOT))
(not (NOT-RIGHTOF9-ROBOT))
(not (NOT-RIGHTOF8-ROBOT))
(not (NOT-RIGHTOF7-ROBOT))
(not (NOT-RIGHTOF6-ROBOT))
(not (NOT-RIGHTOF5-ROBOT))
(not (NOT-RIGHTOF4-ROBOT))
(not (NOT-RIGHTOF3-ROBOT))
(not (NOT-RIGHTOF2-ROBOT))
)
)
(when
(and
(RIGHTOF15-ROBOT)
)
(and
(RIGHTOF16-ROBOT)
(RIGHTOF14-ROBOT)
(RIGHTOF13-ROBOT)
(RIGHTOF12-ROBOT)
(RIGHTOF11-ROBOT)
(RIGHTOF10-ROBOT)
(RIGHTOF9-ROBOT)
(RIGHTOF8-ROBOT)
(RIGHTOF7-ROBOT)
(RIGHTOF6-ROBOT)
(RIGHTOF5-ROBOT)
(RIGHTOF4-ROBOT)
(RIGHTOF3-ROBOT)
(RIGHTOF2-ROBOT)
(not (NOT-RIGHTOF16-ROBOT))
(not (NOT-RIGHTOF15-ROBOT))
(not (NOT-RIGHTOF14-ROBOT))
(not (NOT-RIGHTOF13-ROBOT))
(not (NOT-RIGHTOF12-ROBOT))
(not (NOT-RIGHTOF11-ROBOT))
(not (NOT-RIGHTOF10-ROBOT))
(not (NOT-RIGHTOF9-ROBOT))
(not (NOT-RIGHTOF8-ROBOT))
(not (NOT-RIGHTOF7-ROBOT))
(not (NOT-RIGHTOF6-ROBOT))
(not (NOT-RIGHTOF5-ROBOT))
(not (NOT-RIGHTOF4-ROBOT))
(not (NOT-RIGHTOF3-ROBOT))
(not (NOT-RIGHTOF2-ROBOT))
)
)
(when
(and
(RIGHTOF14-ROBOT)
)
(and
(RIGHTOF15-ROBOT)
(RIGHTOF13-ROBOT)
(RIGHTOF12-ROBOT)
(RIGHTOF11-ROBOT)
(RIGHTOF10-ROBOT)
(RIGHTOF9-ROBOT)
(RIGHTOF8-ROBOT)
(RIGHTOF7-ROBOT)
(RIGHTOF6-ROBOT)
(RIGHTOF5-ROBOT)
(RIGHTOF4-ROBOT)
(RIGHTOF3-ROBOT)
(RIGHTOF2-ROBOT)
(not (NOT-RIGHTOF15-ROBOT))
(not (NOT-RIGHTOF14-ROBOT))
(not (NOT-RIGHTOF13-ROBOT))
(not (NOT-RIGHTOF12-ROBOT))
(not (NOT-RIGHTOF11-ROBOT))
(not (NOT-RIGHTOF10-ROBOT))
(not (NOT-RIGHTOF9-ROBOT))
(not (NOT-RIGHTOF8-ROBOT))
(not (NOT-RIGHTOF7-ROBOT))
(not (NOT-RIGHTOF6-ROBOT))
(not (NOT-RIGHTOF5-ROBOT))
(not (NOT-RIGHTOF4-ROBOT))
(not (NOT-RIGHTOF3-ROBOT))
(not (NOT-RIGHTOF2-ROBOT))
)
)
(when
(and
(RIGHTOF13-ROBOT)
)
(and
(RIGHTOF14-ROBOT)
(RIGHTOF12-ROBOT)
(RIGHTOF11-ROBOT)
(RIGHTOF10-ROBOT)
(RIGHTOF9-ROBOT)
(RIGHTOF8-ROBOT)
(RIGHTOF7-ROBOT)
(RIGHTOF6-ROBOT)
(RIGHTOF5-ROBOT)
(RIGHTOF4-ROBOT)
(RIGHTOF3-ROBOT)
(RIGHTOF2-ROBOT)
(not (NOT-RIGHTOF14-ROBOT))
(not (NOT-RIGHTOF13-ROBOT))
(not (NOT-RIGHTOF12-ROBOT))
(not (NOT-RIGHTOF11-ROBOT))
(not (NOT-RIGHTOF10-ROBOT))
(not (NOT-RIGHTOF9-ROBOT))
(not (NOT-RIGHTOF8-ROBOT))
(not (NOT-RIGHTOF7-ROBOT))
(not (NOT-RIGHTOF6-ROBOT))
(not (NOT-RIGHTOF5-ROBOT))
(not (NOT-RIGHTOF4-ROBOT))
(not (NOT-RIGHTOF3-ROBOT))
(not (NOT-RIGHTOF2-ROBOT))
)
)
(when
(and
(RIGHTOF12-ROBOT)
)
(and
(RIGHTOF13-ROBOT)
(RIGHTOF11-ROBOT)
(RIGHTOF10-ROBOT)
(RIGHTOF9-ROBOT)
(RIGHTOF8-ROBOT)
(RIGHTOF7-ROBOT)
(RIGHTOF6-ROBOT)
(RIGHTOF5-ROBOT)
(RIGHTOF4-ROBOT)
(RIGHTOF3-ROBOT)
(RIGHTOF2-ROBOT)
(not (NOT-RIGHTOF13-ROBOT))
(not (NOT-RIGHTOF12-ROBOT))
(not (NOT-RIGHTOF11-ROBOT))
(not (NOT-RIGHTOF10-ROBOT))
(not (NOT-RIGHTOF9-ROBOT))
(not (NOT-RIGHTOF8-ROBOT))
(not (NOT-RIGHTOF7-ROBOT))
(not (NOT-RIGHTOF6-ROBOT))
(not (NOT-RIGHTOF5-ROBOT))
(not (NOT-RIGHTOF4-ROBOT))
(not (NOT-RIGHTOF3-ROBOT))
(not (NOT-RIGHTOF2-ROBOT))
)
)
(when
(and
(RIGHTOF11-ROBOT)
)
(and
(RIGHTOF12-ROBOT)
(RIGHTOF10-ROBOT)
(RIGHTOF9-ROBOT)
(RIGHTOF8-ROBOT)
(RIGHTOF7-ROBOT)
(RIGHTOF6-ROBOT)
(RIGHTOF5-ROBOT)
(RIGHTOF4-ROBOT)
(RIGHTOF3-ROBOT)
(RIGHTOF2-ROBOT)
(not (NOT-RIGHTOF12-ROBOT))
(not (NOT-RIGHTOF11-ROBOT))
(not (NOT-RIGHTOF10-ROBOT))
(not (NOT-RIGHTOF9-ROBOT))
(not (NOT-RIGHTOF8-ROBOT))
(not (NOT-RIGHTOF7-ROBOT))
(not (NOT-RIGHTOF6-ROBOT))
(not (NOT-RIGHTOF5-ROBOT))
(not (NOT-RIGHTOF4-ROBOT))
(not (NOT-RIGHTOF3-ROBOT))
(not (NOT-RIGHTOF2-ROBOT))
)
)
(when
(and
(RIGHTOF10-ROBOT)
)
(and
(RIGHTOF11-ROBOT)
(RIGHTOF9-ROBOT)
(RIGHTOF8-ROBOT)
(RIGHTOF7-ROBOT)
(RIGHTOF6-ROBOT)
(RIGHTOF5-ROBOT)
(RIGHTOF4-ROBOT)
(RIGHTOF3-ROBOT)
(RIGHTOF2-ROBOT)
(not (NOT-RIGHTOF11-ROBOT))
(not (NOT-RIGHTOF10-ROBOT))
(not (NOT-RIGHTOF9-ROBOT))
(not (NOT-RIGHTOF8-ROBOT))
(not (NOT-RIGHTOF7-ROBOT))
(not (NOT-RIGHTOF6-ROBOT))
(not (NOT-RIGHTOF5-ROBOT))
(not (NOT-RIGHTOF4-ROBOT))
(not (NOT-RIGHTOF3-ROBOT))
(not (NOT-RIGHTOF2-ROBOT))
)
)
(when
(and
(RIGHTOF9-ROBOT)
)
(and
(RIGHTOF10-ROBOT)
(RIGHTOF8-ROBOT)
(RIGHTOF7-ROBOT)
(RIGHTOF6-ROBOT)
(RIGHTOF5-ROBOT)
(RIGHTOF4-ROBOT)
(RIGHTOF3-ROBOT)
(RIGHTOF2-ROBOT)
(not (NOT-RIGHTOF10-ROBOT))
(not (NOT-RIGHTOF9-ROBOT))
(not (NOT-RIGHTOF8-ROBOT))
(not (NOT-RIGHTOF7-ROBOT))
(not (NOT-RIGHTOF6-ROBOT))
(not (NOT-RIGHTOF5-ROBOT))
(not (NOT-RIGHTOF4-ROBOT))
(not (NOT-RIGHTOF3-ROBOT))
(not (NOT-RIGHTOF2-ROBOT))
)
)
(when
(and
(RIGHTOF8-ROBOT)
)
(and
(RIGHTOF9-ROBOT)
(RIGHTOF7-ROBOT)
(RIGHTOF6-ROBOT)
(RIGHTOF5-ROBOT)
(RIGHTOF4-ROBOT)
(RIGHTOF3-ROBOT)
(RIGHTOF2-ROBOT)
(not (NOT-RIGHTOF9-ROBOT))
(not (NOT-RIGHTOF8-ROBOT))
(not (NOT-RIGHTOF7-ROBOT))
(not (NOT-RIGHTOF6-ROBOT))
(not (NOT-RIGHTOF5-ROBOT))
(not (NOT-RIGHTOF4-ROBOT))
(not (NOT-RIGHTOF3-ROBOT))
(not (NOT-RIGHTOF2-ROBOT))
)
)
(when
(and
(RIGHTOF7-ROBOT)
)
(and
(RIGHTOF8-ROBOT)
(RIGHTOF6-ROBOT)
(RIGHTOF5-ROBOT)
(RIGHTOF4-ROBOT)
(RIGHTOF3-ROBOT)
(RIGHTOF2-ROBOT)
(not (NOT-RIGHTOF8-ROBOT))
(not (NOT-RIGHTOF7-ROBOT))
(not (NOT-RIGHTOF6-ROBOT))
(not (NOT-RIGHTOF5-ROBOT))
(not (NOT-RIGHTOF4-ROBOT))
(not (NOT-RIGHTOF3-ROBOT))
(not (NOT-RIGHTOF2-ROBOT))
)
)
(when
(and
(RIGHTOF6-ROBOT)
)
(and
(RIGHTOF7-ROBOT)
(RIGHTOF5-ROBOT)
(RIGHTOF4-ROBOT)
(RIGHTOF3-ROBOT)
(RIGHTOF2-ROBOT)
(not (NOT-RIGHTOF7-ROBOT))
(not (NOT-RIGHTOF6-ROBOT))
(not (NOT-RIGHTOF5-ROBOT))
(not (NOT-RIGHTOF4-ROBOT))
(not (NOT-RIGHTOF3-ROBOT))
(not (NOT-RIGHTOF2-ROBOT))
)
)
(when
(and
(RIGHTOF5-ROBOT)
)
(and
(RIGHTOF6-ROBOT)
(RIGHTOF4-ROBOT)
(RIGHTOF3-ROBOT)
(RIGHTOF2-ROBOT)
(not (NOT-RIGHTOF6-ROBOT))
(not (NOT-RIGHTOF5-ROBOT))
(not (NOT-RIGHTOF4-ROBOT))
(not (NOT-RIGHTOF3-ROBOT))
(not (NOT-RIGHTOF2-ROBOT))
)
)
(when
(and
(RIGHTOF4-ROBOT)
)
(and
(RIGHTOF5-ROBOT)
(RIGHTOF3-ROBOT)
(RIGHTOF2-ROBOT)
(not (NOT-RIGHTOF5-ROBOT))
(not (NOT-RIGHTOF4-ROBOT))
(not (NOT-RIGHTOF3-ROBOT))
(not (NOT-RIGHTOF2-ROBOT))
)
)
(when
(and
(RIGHTOF3-ROBOT)
)
(and
(RIGHTOF4-ROBOT)
(RIGHTOF2-ROBOT)
(not (NOT-RIGHTOF4-ROBOT))
(not (NOT-RIGHTOF3-ROBOT))
(not (NOT-RIGHTOF2-ROBOT))
)
)
(when
(and
(RIGHTOF2-ROBOT)
)
(and
(RIGHTOF3-ROBOT)
(not (NOT-RIGHTOF3-ROBOT))
(not (NOT-RIGHTOF2-ROBOT))
)
)
(when
(and
(RIGHTOF1-ROBOT)
)
(and
(RIGHTOF2-ROBOT)
(not (NOT-RIGHTOF2-ROBOT))
)
)
(when
(and
(Flag473-)
)
(and
(RIGHTOF1-ROBOT)
(not (NOT-RIGHTOF1-ROBOT))
)
)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag473prime-))
(not (Flag258prime-))
(not (Flag257prime-))
(not (Flag256prime-))
(not (Flag255prime-))
(not (Flag21prime-))
(not (Flag20prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag472prime-))
(not (Flag238prime-))
(not (Flag236prime-))
(not (Flag235prime-))
(not (Flag233prime-))
(not (Flag232prime-))
(not (Flag231prime-))
(not (Flag229prime-))
(not (Flag228prime-))
(not (Flag227prime-))
(not (Flag226prime-))
(not (Flag224prime-))
(not (Flag223prime-))
(not (Flag222prime-))
(not (Flag221prime-))
(not (Flag220prime-))
(not (Flag219prime-))
(not (Flag217prime-))
(not (Flag216prime-))
(not (Flag215prime-))
(not (Flag214prime-))
(not (Flag213prime-))
(not (Flag212prime-))
(not (Flag211prime-))
(not (Flag209prime-))
(not (Flag208prime-))
(not (Flag207prime-))
(not (Flag206prime-))
(not (Flag205prime-))
(not (Flag204prime-))
(not (Flag203prime-))
(not (Flag201prime-))
(not (Flag200prime-))
(not (Flag199prime-))
(not (Flag198prime-))
(not (Flag197prime-))
(not (Flag196prime-))
(not (Flag195prime-))
(not (Flag194prime-))
(not (Flag192prime-))
(not (Flag191prime-))
(not (Flag190prime-))
(not (Flag189prime-))
(not (Flag188prime-))
(not (Flag187prime-))
(not (Flag186prime-))
(not (Flag185prime-))
(not (Flag184prime-))
(not (Flag183prime-))
(not (Flag181prime-))
(not (Flag180prime-))
(not (Flag179prime-))
(not (Flag178prime-))
(not (Flag177prime-))
(not (Flag176prime-))
(not (Flag175prime-))
(not (Flag174prime-))
(not (Flag173prime-))
(not (Flag172prime-))
(not (Flag171prime-))
(not (Flag170prime-))
(not (Flag168prime-))
(not (Flag167prime-))
(not (Flag166prime-))
(not (Flag165prime-))
(not (Flag164prime-))
(not (Flag163prime-))
(not (Flag162prime-))
(not (Flag161prime-))
(not (Flag160prime-))
(not (Flag159prime-))
(not (Flag158prime-))
(not (Flag157prime-))
(not (Flag155prime-))
(not (Flag154prime-))
(not (Flag153prime-))
(not (Flag152prime-))
(not (Flag151prime-))
(not (Flag150prime-))
(not (Flag149prime-))
(not (Flag148prime-))
(not (Flag147prime-))
(not (Flag146prime-))
(not (Flag145prime-))
(not (Flag144prime-))
(not (Flag143prime-))
(not (Flag142prime-))
(not (Flag140prime-))
(not (Flag139prime-))
(not (Flag138prime-))
(not (Flag137prime-))
(not (Flag136prime-))
(not (Flag135prime-))
(not (Flag134prime-))
(not (Flag133prime-))
(not (Flag132prime-))
(not (Flag131prime-))
(not (Flag130prime-))
(not (Flag129prime-))
(not (Flag128prime-))
(not (Flag127prime-))
(not (Flag126prime-))
(not (Flag124prime-))
(not (Flag123prime-))
(not (Flag122prime-))
(not (Flag121prime-))
(not (Flag120prime-))
(not (Flag119prime-))
(not (Flag118prime-))
(not (Flag117prime-))
(not (Flag116prime-))
(not (Flag115prime-))
(not (Flag114prime-))
(not (Flag113prime-))
(not (Flag112prime-))
(not (Flag111prime-))
(not (Flag110prime-))
(not (Flag108prime-))
(not (Flag107prime-))
(not (Flag106prime-))
(not (Flag105prime-))
(not (Flag104prime-))
(not (Flag103prime-))
(not (Flag102prime-))
(not (Flag101prime-))
(not (Flag100prime-))
(not (Flag99prime-))
(not (Flag98prime-))
(not (Flag97prime-))
(not (Flag96prime-))
(not (Flag95prime-))
(not (Flag94prime-))
(not (Flag93prime-))
(not (Flag92prime-))
(not (Flag90prime-))
(not (Flag89prime-))
(not (Flag88prime-))
(not (Flag87prime-))
(not (Flag86prime-))
(not (Flag85prime-))
(not (Flag84prime-))
(not (Flag83prime-))
(not (Flag82prime-))
(not (Flag81prime-))
(not (Flag80prime-))
(not (Flag79prime-))
(not (Flag78prime-))
(not (Flag77prime-))
(not (Flag76prime-))
(not (Flag75prime-))
(not (Flag74prime-))
(not (Flag72prime-))
(not (Flag71prime-))
(not (Flag70prime-))
(not (Flag69prime-))
(not (Flag68prime-))
(not (Flag67prime-))
(not (Flag66prime-))
(not (Flag65prime-))
(not (Flag64prime-))
(not (Flag63prime-))
(not (Flag62prime-))
(not (Flag61prime-))
(not (Flag60prime-))
(not (Flag59prime-))
(not (Flag58prime-))
(not (Flag57prime-))
(not (Flag56prime-))
(not (Flag55prime-))
(not (Flag54prime-))
(not (Flag53prime-))
(not (Flag52prime-))
(not (Flag51prime-))
(not (Flag50prime-))
(not (Flag49prime-))
(not (Flag48prime-))
(not (Flag47prime-))
(not (Flag46prime-))
(not (Flag45prime-))
(not (Flag44prime-))
(not (Flag43prime-))
(not (Flag42prime-))
(not (Flag41prime-))
(not (Flag40prime-))
(not (Flag39prime-))
(not (Flag38prime-))
(not (Flag37prime-))
(not (Flag36prime-))
(not (Flag35prime-))
(not (Flag34prime-))
(not (Flag33prime-))
(not (Flag32prime-))
(not (Flag31prime-))
(not (Flag30prime-))
(not (Flag29prime-))
(not (Flag28prime-))
(not (Flag27prime-))
(not (Flag26prime-))
(not (Flag25prime-))
(not (Flag24prime-))
(not (Flag23prime-))
(not (Flag22prime-))
(not (Flag19prime-))
(not (Flag18prime-))
(not (Flag17prime-))
(not (Flag16prime-))
(not (Flag15prime-))
(not (Flag14prime-))
(not (Flag13prime-))
(not (Flag12prime-))
(not (Flag11prime-))
(not (Flag10prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag474prime-))
(not (Flag470prime-))
(not (Flag468prime-))
(not (Flag467prime-))
(not (Flag465prime-))
(not (Flag464prime-))
(not (Flag463prime-))
(not (Flag462prime-))
(not (Flag460prime-))
(not (Flag459prime-))
(not (Flag458prime-))
(not (Flag457prime-))
(not (Flag455prime-))
(not (Flag454prime-))
(not (Flag453prime-))
(not (Flag452prime-))
(not (Flag451prime-))
(not (Flag449prime-))
(not (Flag448prime-))
(not (Flag447prime-))
(not (Flag446prime-))
(not (Flag445prime-))
(not (Flag444prime-))
(not (Flag443prime-))
(not (Flag441prime-))
(not (Flag440prime-))
(not (Flag439prime-))
(not (Flag438prime-))
(not (Flag437prime-))
(not (Flag436prime-))
(not (Flag435prime-))
(not (Flag433prime-))
(not (Flag432prime-))
(not (Flag431prime-))
(not (Flag430prime-))
(not (Flag429prime-))
(not (Flag428prime-))
(not (Flag427prime-))
(not (Flag426prime-))
(not (Flag425prime-))
(not (Flag423prime-))
(not (Flag422prime-))
(not (Flag421prime-))
(not (Flag420prime-))
(not (Flag419prime-))
(not (Flag418prime-))
(not (Flag417prime-))
(not (Flag416prime-))
(not (Flag415prime-))
(not (Flag413prime-))
(not (Flag412prime-))
(not (Flag411prime-))
(not (Flag410prime-))
(not (Flag409prime-))
(not (Flag408prime-))
(not (Flag407prime-))
(not (Flag406prime-))
(not (Flag405prime-))
(not (Flag404prime-))
(not (Flag403prime-))
(not (Flag401prime-))
(not (Flag400prime-))
(not (Flag399prime-))
(not (Flag398prime-))
(not (Flag397prime-))
(not (Flag396prime-))
(not (Flag395prime-))
(not (Flag394prime-))
(not (Flag393prime-))
(not (Flag392prime-))
(not (Flag391prime-))
(not (Flag390prime-))
(not (Flag388prime-))
(not (Flag387prime-))
(not (Flag386prime-))
(not (Flag385prime-))
(not (Flag384prime-))
(not (Flag383prime-))
(not (Flag382prime-))
(not (Flag381prime-))
(not (Flag380prime-))
(not (Flag379prime-))
(not (Flag378prime-))
(not (Flag377prime-))
(not (Flag376prime-))
(not (Flag375prime-))
(not (Flag373prime-))
(not (Flag372prime-))
(not (Flag371prime-))
(not (Flag370prime-))
(not (Flag369prime-))
(not (Flag368prime-))
(not (Flag367prime-))
(not (Flag366prime-))
(not (Flag365prime-))
(not (Flag364prime-))
(not (Flag363prime-))
(not (Flag362prime-))
(not (Flag361prime-))
(not (Flag359prime-))
(not (Flag358prime-))
(not (Flag357prime-))
(not (Flag356prime-))
(not (Flag355prime-))
(not (Flag354prime-))
(not (Flag353prime-))
(not (Flag352prime-))
(not (Flag351prime-))
(not (Flag350prime-))
(not (Flag349prime-))
(not (Flag348prime-))
(not (Flag347prime-))
(not (Flag346prime-))
(not (Flag345prime-))
(not (Flag344prime-))
(not (Flag342prime-))
(not (Flag341prime-))
(not (Flag340prime-))
(not (Flag339prime-))
(not (Flag338prime-))
(not (Flag337prime-))
(not (Flag336prime-))
(not (Flag335prime-))
(not (Flag334prime-))
(not (Flag333prime-))
(not (Flag332prime-))
(not (Flag331prime-))
(not (Flag330prime-))
(not (Flag329prime-))
(not (Flag328prime-))
(not (Flag327prime-))
(not (Flag326prime-))
(not (Flag324prime-))
(not (Flag323prime-))
(not (Flag322prime-))
(not (Flag321prime-))
(not (Flag320prime-))
(not (Flag319prime-))
(not (Flag318prime-))
(not (Flag317prime-))
(not (Flag316prime-))
(not (Flag315prime-))
(not (Flag314prime-))
(not (Flag313prime-))
(not (Flag312prime-))
(not (Flag311prime-))
(not (Flag310prime-))
(not (Flag309prime-))
(not (Flag307prime-))
(not (Flag306prime-))
(not (Flag305prime-))
(not (Flag304prime-))
(not (Flag303prime-))
(not (Flag302prime-))
(not (Flag301prime-))
(not (Flag300prime-))
(not (Flag299prime-))
(not (Flag298prime-))
(not (Flag297prime-))
(not (Flag296prime-))
(not (Flag295prime-))
(not (Flag294prime-))
(not (Flag293prime-))
(not (Flag292prime-))
(not (Flag291prime-))
(not (Flag290prime-))
(not (Flag289prime-))
(not (Flag288prime-))
(not (Flag287prime-))
(not (Flag286prime-))
(not (Flag285prime-))
(not (Flag284prime-))
(not (Flag283prime-))
(not (Flag282prime-))
(not (Flag281prime-))
(not (Flag280prime-))
(not (Flag279prime-))
(not (Flag278prime-))
(not (Flag277prime-))
(not (Flag276prime-))
(not (Flag275prime-))
(not (Flag274prime-))
(not (Flag273prime-))
(not (Flag272prime-))
(not (Flag271prime-))
(not (Flag270prime-))
(not (Flag269prime-))
(not (Flag268prime-))
(not (Flag267prime-))
(not (Flag266prime-))
(not (Flag265prime-))
(not (Flag264prime-))
(not (Flag263prime-))
(not (Flag262prime-))
(not (Flag261prime-))
(not (Flag260prime-))
(not (Flag259prime-))
(not (Flag254prime-))
(not (Flag253prime-))
(not (Flag252prime-))
(not (Flag251prime-))
(not (Flag250prime-))
(not (Flag249prime-))
(not (Flag248prime-))
(not (Flag247prime-))
(not (Flag246prime-))
(not (Flag245prime-))
(not (Flag244prime-))
(not (Flag243prime-))
(not (Flag242prime-))
(not (Flag241prime-))
(not (Flag240prime-))
(not (Flag239prime-))
(not (Flag237prime-))
(not (Flag234prime-))
(not (Flag230prime-))
(not (Flag225prime-))
(not (Flag218prime-))
(not (Flag210prime-))
(not (Flag202prime-))
(not (Flag193prime-))
(not (Flag182prime-))
(not (Flag169prime-))
(not (Flag156prime-))
(not (Flag141prime-))
(not (Flag125prime-))
(not (Flag109prime-))
(not (Flag91prime-))
(not (Flag73prime-))
(not (Flag1prime-))
(not (Flag475prime-))
(not (Flag471prime-))
(not (Flag469prime-))
(not (Flag466prime-))
(not (Flag461prime-))
(not (Flag456prime-))
(not (Flag450prime-))
(not (Flag442prime-))
(not (Flag434prime-))
(not (Flag424prime-))
(not (Flag414prime-))
(not (Flag402prime-))
(not (Flag389prime-))
(not (Flag374prime-))
(not (Flag360prime-))
(not (Flag343prime-))
(not (Flag325prime-))
(not (Flag308prime-))
)
)
(:action MOVELEFT-ROBOT
:parameters ()
:precondition
(and
(Flag3-)
(Flag3prime-)
(Flag471prime-)
(Flag469prime-)
(Flag466prime-)
(Flag461prime-)
(Flag456prime-)
(Flag450prime-)
(Flag442prime-)
(Flag434prime-)
(Flag424prime-)
(Flag414prime-)
(Flag402prime-)
(Flag389prime-)
(Flag374prime-)
(Flag360prime-)
(Flag343prime-)
(Flag325prime-)
(Flag308prime-)
(Flag272prime-)
(Flag271prime-)
(Flag270prime-)
(Flag269prime-)
(Flag268prime-)
(Flag267prime-)
(Flag266prime-)
(Flag265prime-)
(Flag264prime-)
(Flag263prime-)
(Flag262prime-)
(Flag261prime-)
(Flag260prime-)
(Flag259prime-)
(Flag258prime-)
(Flag257prime-)
(Flag256prime-)
(Flag255prime-)
(Flag254prime-)
(Flag253prime-)
(Flag252prime-)
(Flag251prime-)
(Flag250prime-)
(Flag249prime-)
(Flag248prime-)
(Flag247prime-)
(Flag246prime-)
(Flag245prime-)
(Flag244prime-)
(Flag243prime-)
(Flag242prime-)
(Flag241prime-)
(Flag240prime-)
)
:effect
(and
(when
(and
(Flag471-)
)
(and
(COLUMN16-ROBOT)
(not (NOT-COLUMN16-ROBOT))
)
)
(when
(and
(Flag469-)
)
(and
(COLUMN15-ROBOT)
(not (NOT-COLUMN15-ROBOT))
)
)
(when
(and
(Flag466-)
)
(and
(COLUMN14-ROBOT)
(not (NOT-COLUMN14-ROBOT))
)
)
(when
(and
(Flag461-)
)
(and
(COLUMN13-ROBOT)
(not (NOT-COLUMN13-ROBOT))
)
)
(when
(and
(Flag456-)
)
(and
(COLUMN12-ROBOT)
(not (NOT-COLUMN12-ROBOT))
)
)
(when
(and
(Flag450-)
)
(and
(COLUMN11-ROBOT)
(not (NOT-COLUMN11-ROBOT))
)
)
(when
(and
(Flag442-)
)
(and
(COLUMN10-ROBOT)
(not (NOT-COLUMN10-ROBOT))
)
)
(when
(and
(Flag434-)
)
(and
(COLUMN9-ROBOT)
(not (NOT-COLUMN9-ROBOT))
)
)
(when
(and
(Flag424-)
)
(and
(COLUMN8-ROBOT)
(not (NOT-COLUMN8-ROBOT))
)
)
(when
(and
(Flag414-)
)
(and
(COLUMN7-ROBOT)
(not (NOT-COLUMN7-ROBOT))
)
)
(when
(and
(Flag402-)
)
(and
(COLUMN6-ROBOT)
(not (NOT-COLUMN6-ROBOT))
)
)
(when
(and
(Flag389-)
)
(and
(COLUMN5-ROBOT)
(not (NOT-COLUMN5-ROBOT))
)
)
(when
(and
(Flag374-)
)
(and
(COLUMN4-ROBOT)
(not (NOT-COLUMN4-ROBOT))
)
)
(when
(and
(Flag360-)
)
(and
(COLUMN3-ROBOT)
(not (NOT-COLUMN3-ROBOT))
)
)
(when
(and
(Flag343-)
)
(and
(COLUMN2-ROBOT)
(not (NOT-COLUMN2-ROBOT))
)
)
(when
(and
(Flag325-)
)
(and
(COLUMN1-ROBOT)
(not (NOT-COLUMN1-ROBOT))
)
)
(when
(and
(Flag308-)
)
(and
(COLUMN0-ROBOT)
(not (NOT-COLUMN0-ROBOT))
)
)
(when
(and
(COLUMN17-ROBOT)
)
(and
(COLUMN16-ROBOT)
(NOT-COLUMN17-ROBOT)
(not (NOT-COLUMN16-ROBOT))
(not (COLUMN17-ROBOT))
)
)
(when
(and
(COLUMN16-ROBOT)
)
(and
(COLUMN15-ROBOT)
(NOT-COLUMN16-ROBOT)
(not (NOT-COLUMN15-ROBOT))
(not (COLUMN16-ROBOT))
)
)
(when
(and
(COLUMN15-ROBOT)
)
(and
(COLUMN14-ROBOT)
(NOT-COLUMN15-ROBOT)
(not (NOT-COLUMN14-ROBOT))
(not (COLUMN15-ROBOT))
)
)
(when
(and
(COLUMN14-ROBOT)
)
(and
(COLUMN13-ROBOT)
(NOT-COLUMN14-ROBOT)
(not (NOT-COLUMN13-ROBOT))
(not (COLUMN14-ROBOT))
)
)
(when
(and
(COLUMN13-ROBOT)
)
(and
(COLUMN12-ROBOT)
(NOT-COLUMN13-ROBOT)
(not (NOT-COLUMN12-ROBOT))
(not (COLUMN13-ROBOT))
)
)
(when
(and
(COLUMN12-ROBOT)
)
(and
(COLUMN11-ROBOT)
(NOT-COLUMN12-ROBOT)
(not (NOT-COLUMN11-ROBOT))
(not (COLUMN12-ROBOT))
)
)
(when
(and
(COLUMN11-ROBOT)
)
(and
(COLUMN10-ROBOT)
(NOT-COLUMN11-ROBOT)
(not (NOT-COLUMN10-ROBOT))
(not (COLUMN11-ROBOT))
)
)
(when
(and
(COLUMN10-ROBOT)
)
(and
(COLUMN9-ROBOT)
(NOT-COLUMN10-ROBOT)
(not (NOT-COLUMN9-ROBOT))
(not (COLUMN10-ROBOT))
)
)
(when
(and
(COLUMN9-ROBOT)
)
(and
(COLUMN8-ROBOT)
(NOT-COLUMN9-ROBOT)
(not (NOT-COLUMN8-ROBOT))
(not (COLUMN9-ROBOT))
)
)
(when
(and
(COLUMN8-ROBOT)
)
(and
(COLUMN7-ROBOT)
(NOT-COLUMN8-ROBOT)
(not (NOT-COLUMN7-ROBOT))
(not (COLUMN8-ROBOT))
)
)
(when
(and
(COLUMN7-ROBOT)
)
(and
(COLUMN6-ROBOT)
(NOT-COLUMN7-ROBOT)
(not (NOT-COLUMN6-ROBOT))
(not (COLUMN7-ROBOT))
)
)
(when
(and
(COLUMN6-ROBOT)
)
(and
(COLUMN5-ROBOT)
(NOT-COLUMN6-ROBOT)
(not (NOT-COLUMN5-ROBOT))
(not (COLUMN6-ROBOT))
)
)
(when
(and
(COLUMN5-ROBOT)
)
(and
(COLUMN4-ROBOT)
(NOT-COLUMN5-ROBOT)
(not (NOT-COLUMN4-ROBOT))
(not (COLUMN5-ROBOT))
)
)
(when
(and
(COLUMN4-ROBOT)
)
(and
(COLUMN3-ROBOT)
(NOT-COLUMN4-ROBOT)
(not (NOT-COLUMN3-ROBOT))
(not (COLUMN4-ROBOT))
)
)
(when
(and
(COLUMN3-ROBOT)
)
(and
(COLUMN2-ROBOT)
(NOT-COLUMN3-ROBOT)
(not (NOT-COLUMN2-ROBOT))
(not (COLUMN3-ROBOT))
)
)
(when
(and
(COLUMN2-ROBOT)
)
(and
(COLUMN1-ROBOT)
(NOT-COLUMN2-ROBOT)
(not (NOT-COLUMN1-ROBOT))
(not (COLUMN2-ROBOT))
)
)
(when
(and
(COLUMN1-ROBOT)
)
(and
(COLUMN0-ROBOT)
(NOT-COLUMN1-ROBOT)
(not (NOT-COLUMN0-ROBOT))
(not (COLUMN1-ROBOT))
)
)
(when
(and
(RIGHTOF17-ROBOT)
)
(and
(RIGHTOF16-ROBOT)
(NOT-RIGHTOF17-ROBOT)
(not (NOT-RIGHTOF16-ROBOT))
(not (RIGHTOF17-ROBOT))
)
)
(when
(and
(Flag272-)
)
(and
(RIGHTOF15-ROBOT)
(NOT-RIGHTOF16-ROBOT)
(not (NOT-RIGHTOF15-ROBOT))
(not (RIGHTOF16-ROBOT))
)
)
(when
(and
(Flag271-)
)
(and
(RIGHTOF14-ROBOT)
(NOT-RIGHTOF15-ROBOT)
(not (NOT-RIGHTOF14-ROBOT))
(not (RIGHTOF15-ROBOT))
)
)
(when
(and
(Flag270-)
)
(and
(RIGHTOF13-ROBOT)
(NOT-RIGHTOF14-ROBOT)
(not (NOT-RIGHTOF13-ROBOT))
(not (RIGHTOF14-ROBOT))
)
)
(when
(and
(Flag269-)
)
(and
(RIGHTOF12-ROBOT)
(NOT-RIGHTOF13-ROBOT)
(not (NOT-RIGHTOF12-ROBOT))
(not (RIGHTOF13-ROBOT))
)
)
(when
(and
(Flag268-)
)
(and
(RIGHTOF11-ROBOT)
(NOT-RIGHTOF12-ROBOT)
(not (NOT-RIGHTOF11-ROBOT))
(not (RIGHTOF12-ROBOT))
)
)
(when
(and
(Flag267-)
)
(and
(RIGHTOF10-ROBOT)
(NOT-RIGHTOF11-ROBOT)
(not (NOT-RIGHTOF10-ROBOT))
(not (RIGHTOF11-ROBOT))
)
)
(when
(and
(Flag266-)
)
(and
(RIGHTOF9-ROBOT)
(NOT-RIGHTOF10-ROBOT)
(not (NOT-RIGHTOF9-ROBOT))
(not (RIGHTOF10-ROBOT))
)
)
(when
(and
(Flag265-)
)
(and
(RIGHTOF8-ROBOT)
(NOT-RIGHTOF9-ROBOT)
(not (NOT-RIGHTOF8-ROBOT))
(not (RIGHTOF9-ROBOT))
)
)
(when
(and
(Flag264-)
)
(and
(RIGHTOF7-ROBOT)
(NOT-RIGHTOF8-ROBOT)
(not (NOT-RIGHTOF7-ROBOT))
(not (RIGHTOF8-ROBOT))
)
)
(when
(and
(Flag263-)
)
(and
(RIGHTOF6-ROBOT)
(NOT-RIGHTOF7-ROBOT)
(not (NOT-RIGHTOF6-ROBOT))
(not (RIGHTOF7-ROBOT))
)
)
(when
(and
(Flag262-)
)
(and
(RIGHTOF5-ROBOT)
(NOT-RIGHTOF6-ROBOT)
(not (NOT-RIGHTOF5-ROBOT))
(not (RIGHTOF6-ROBOT))
)
)
(when
(and
(Flag261-)
)
(and
(RIGHTOF4-ROBOT)
(NOT-RIGHTOF5-ROBOT)
(not (NOT-RIGHTOF4-ROBOT))
(not (RIGHTOF5-ROBOT))
)
)
(when
(and
(Flag260-)
)
(and
(RIGHTOF3-ROBOT)
(NOT-RIGHTOF4-ROBOT)
(not (NOT-RIGHTOF3-ROBOT))
(not (RIGHTOF4-ROBOT))
)
)
(when
(and
(Flag259-)
)
(and
(RIGHTOF2-ROBOT)
(NOT-RIGHTOF3-ROBOT)
(not (NOT-RIGHTOF2-ROBOT))
(not (RIGHTOF3-ROBOT))
)
)
(when
(and
(Flag258-)
)
(and
(RIGHTOF1-ROBOT)
(NOT-RIGHTOF2-ROBOT)
(not (NOT-RIGHTOF1-ROBOT))
(not (RIGHTOF2-ROBOT))
)
)
(when
(and
(Flag257-)
)
(and
(RIGHTOF0-ROBOT)
(NOT-RIGHTOF1-ROBOT)
(not (NOT-RIGHTOF0-ROBOT))
(not (RIGHTOF1-ROBOT))
)
)
(when
(and
(Flag256-)
)
(and
(LEFTOF17-ROBOT)
(not (NOT-LEFTOF17-ROBOT))
)
)
(when
(and
(Flag255-)
)
(and
(LEFTOF16-ROBOT)
(not (NOT-LEFTOF16-ROBOT))
)
)
(when
(and
(Flag254-)
)
(and
(LEFTOF15-ROBOT)
(not (NOT-LEFTOF15-ROBOT))
)
)
(when
(and
(Flag253-)
)
(and
(LEFTOF14-ROBOT)
(not (NOT-LEFTOF14-ROBOT))
)
)
(when
(and
(Flag252-)
)
(and
(LEFTOF13-ROBOT)
(not (NOT-LEFTOF13-ROBOT))
)
)
(when
(and
(Flag251-)
)
(and
(LEFTOF12-ROBOT)
(not (NOT-LEFTOF12-ROBOT))
)
)
(when
(and
(Flag250-)
)
(and
(LEFTOF11-ROBOT)
(not (NOT-LEFTOF11-ROBOT))
)
)
(when
(and
(Flag249-)
)
(and
(LEFTOF10-ROBOT)
(not (NOT-LEFTOF10-ROBOT))
)
)
(when
(and
(Flag248-)
)
(and
(LEFTOF9-ROBOT)
(not (NOT-LEFTOF9-ROBOT))
)
)
(when
(and
(Flag247-)
)
(and
(LEFTOF8-ROBOT)
(not (NOT-LEFTOF8-ROBOT))
)
)
(when
(and
(Flag246-)
)
(and
(LEFTOF7-ROBOT)
(not (NOT-LEFTOF7-ROBOT))
)
)
(when
(and
(Flag245-)
)
(and
(LEFTOF6-ROBOT)
(not (NOT-LEFTOF6-ROBOT))
)
)
(when
(and
(Flag244-)
)
(and
(LEFTOF5-ROBOT)
(not (NOT-LEFTOF5-ROBOT))
)
)
(when
(and
(Flag243-)
)
(and
(LEFTOF4-ROBOT)
(not (NOT-LEFTOF4-ROBOT))
)
)
(when
(and
(Flag242-)
)
(and
(LEFTOF3-ROBOT)
(not (NOT-LEFTOF3-ROBOT))
)
)
(when
(and
(Flag241-)
)
(and
(LEFTOF2-ROBOT)
(not (NOT-LEFTOF2-ROBOT))
)
)
(when
(and
(Flag240-)
)
(and
(LEFTOF1-ROBOT)
(not (NOT-LEFTOF1-ROBOT))
)
)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag473prime-))
(not (Flag258prime-))
(not (Flag257prime-))
(not (Flag256prime-))
(not (Flag255prime-))
(not (Flag21prime-))
(not (Flag20prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag472prime-))
(not (Flag238prime-))
(not (Flag236prime-))
(not (Flag235prime-))
(not (Flag233prime-))
(not (Flag232prime-))
(not (Flag231prime-))
(not (Flag229prime-))
(not (Flag228prime-))
(not (Flag227prime-))
(not (Flag226prime-))
(not (Flag224prime-))
(not (Flag223prime-))
(not (Flag222prime-))
(not (Flag221prime-))
(not (Flag220prime-))
(not (Flag219prime-))
(not (Flag217prime-))
(not (Flag216prime-))
(not (Flag215prime-))
(not (Flag214prime-))
(not (Flag213prime-))
(not (Flag212prime-))
(not (Flag211prime-))
(not (Flag209prime-))
(not (Flag208prime-))
(not (Flag207prime-))
(not (Flag206prime-))
(not (Flag205prime-))
(not (Flag204prime-))
(not (Flag203prime-))
(not (Flag201prime-))
(not (Flag200prime-))
(not (Flag199prime-))
(not (Flag198prime-))
(not (Flag197prime-))
(not (Flag196prime-))
(not (Flag195prime-))
(not (Flag194prime-))
(not (Flag192prime-))
(not (Flag191prime-))
(not (Flag190prime-))
(not (Flag189prime-))
(not (Flag188prime-))
(not (Flag187prime-))
(not (Flag186prime-))
(not (Flag185prime-))
(not (Flag184prime-))
(not (Flag183prime-))
(not (Flag181prime-))
(not (Flag180prime-))
(not (Flag179prime-))
(not (Flag178prime-))
(not (Flag177prime-))
(not (Flag176prime-))
(not (Flag175prime-))
(not (Flag174prime-))
(not (Flag173prime-))
(not (Flag172prime-))
(not (Flag171prime-))
(not (Flag170prime-))
(not (Flag168prime-))
(not (Flag167prime-))
(not (Flag166prime-))
(not (Flag165prime-))
(not (Flag164prime-))
(not (Flag163prime-))
(not (Flag162prime-))
(not (Flag161prime-))
(not (Flag160prime-))
(not (Flag159prime-))
(not (Flag158prime-))
(not (Flag157prime-))
(not (Flag155prime-))
(not (Flag154prime-))
(not (Flag153prime-))
(not (Flag152prime-))
(not (Flag151prime-))
(not (Flag150prime-))
(not (Flag149prime-))
(not (Flag148prime-))
(not (Flag147prime-))
(not (Flag146prime-))
(not (Flag145prime-))
(not (Flag144prime-))
(not (Flag143prime-))
(not (Flag142prime-))
(not (Flag140prime-))
(not (Flag139prime-))
(not (Flag138prime-))
(not (Flag137prime-))
(not (Flag136prime-))
(not (Flag135prime-))
(not (Flag134prime-))
(not (Flag133prime-))
(not (Flag132prime-))
(not (Flag131prime-))
(not (Flag130prime-))
(not (Flag129prime-))
(not (Flag128prime-))
(not (Flag127prime-))
(not (Flag126prime-))
(not (Flag124prime-))
(not (Flag123prime-))
(not (Flag122prime-))
(not (Flag121prime-))
(not (Flag120prime-))
(not (Flag119prime-))
(not (Flag118prime-))
(not (Flag117prime-))
(not (Flag116prime-))
(not (Flag115prime-))
(not (Flag114prime-))
(not (Flag113prime-))
(not (Flag112prime-))
(not (Flag111prime-))
(not (Flag110prime-))
(not (Flag108prime-))
(not (Flag107prime-))
(not (Flag106prime-))
(not (Flag105prime-))
(not (Flag104prime-))
(not (Flag103prime-))
(not (Flag102prime-))
(not (Flag101prime-))
(not (Flag100prime-))
(not (Flag99prime-))
(not (Flag98prime-))
(not (Flag97prime-))
(not (Flag96prime-))
(not (Flag95prime-))
(not (Flag94prime-))
(not (Flag93prime-))
(not (Flag92prime-))
(not (Flag90prime-))
(not (Flag89prime-))
(not (Flag88prime-))
(not (Flag87prime-))
(not (Flag86prime-))
(not (Flag85prime-))
(not (Flag84prime-))
(not (Flag83prime-))
(not (Flag82prime-))
(not (Flag81prime-))
(not (Flag80prime-))
(not (Flag79prime-))
(not (Flag78prime-))
(not (Flag77prime-))
(not (Flag76prime-))
(not (Flag75prime-))
(not (Flag74prime-))
(not (Flag72prime-))
(not (Flag71prime-))
(not (Flag70prime-))
(not (Flag69prime-))
(not (Flag68prime-))
(not (Flag67prime-))
(not (Flag66prime-))
(not (Flag65prime-))
(not (Flag64prime-))
(not (Flag63prime-))
(not (Flag62prime-))
(not (Flag61prime-))
(not (Flag60prime-))
(not (Flag59prime-))
(not (Flag58prime-))
(not (Flag57prime-))
(not (Flag56prime-))
(not (Flag55prime-))
(not (Flag54prime-))
(not (Flag53prime-))
(not (Flag52prime-))
(not (Flag51prime-))
(not (Flag50prime-))
(not (Flag49prime-))
(not (Flag48prime-))
(not (Flag47prime-))
(not (Flag46prime-))
(not (Flag45prime-))
(not (Flag44prime-))
(not (Flag43prime-))
(not (Flag42prime-))
(not (Flag41prime-))
(not (Flag40prime-))
(not (Flag39prime-))
(not (Flag38prime-))
(not (Flag37prime-))
(not (Flag36prime-))
(not (Flag35prime-))
(not (Flag34prime-))
(not (Flag33prime-))
(not (Flag32prime-))
(not (Flag31prime-))
(not (Flag30prime-))
(not (Flag29prime-))
(not (Flag28prime-))
(not (Flag27prime-))
(not (Flag26prime-))
(not (Flag25prime-))
(not (Flag24prime-))
(not (Flag23prime-))
(not (Flag22prime-))
(not (Flag19prime-))
(not (Flag18prime-))
(not (Flag17prime-))
(not (Flag16prime-))
(not (Flag15prime-))
(not (Flag14prime-))
(not (Flag13prime-))
(not (Flag12prime-))
(not (Flag11prime-))
(not (Flag10prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag474prime-))
(not (Flag470prime-))
(not (Flag468prime-))
(not (Flag467prime-))
(not (Flag465prime-))
(not (Flag464prime-))
(not (Flag463prime-))
(not (Flag462prime-))
(not (Flag460prime-))
(not (Flag459prime-))
(not (Flag458prime-))
(not (Flag457prime-))
(not (Flag455prime-))
(not (Flag454prime-))
(not (Flag453prime-))
(not (Flag452prime-))
(not (Flag451prime-))
(not (Flag449prime-))
(not (Flag448prime-))
(not (Flag447prime-))
(not (Flag446prime-))
(not (Flag445prime-))
(not (Flag444prime-))
(not (Flag443prime-))
(not (Flag441prime-))
(not (Flag440prime-))
(not (Flag439prime-))
(not (Flag438prime-))
(not (Flag437prime-))
(not (Flag436prime-))
(not (Flag435prime-))
(not (Flag433prime-))
(not (Flag432prime-))
(not (Flag431prime-))
(not (Flag430prime-))
(not (Flag429prime-))
(not (Flag428prime-))
(not (Flag427prime-))
(not (Flag426prime-))
(not (Flag425prime-))
(not (Flag423prime-))
(not (Flag422prime-))
(not (Flag421prime-))
(not (Flag420prime-))
(not (Flag419prime-))
(not (Flag418prime-))
(not (Flag417prime-))
(not (Flag416prime-))
(not (Flag415prime-))
(not (Flag413prime-))
(not (Flag412prime-))
(not (Flag411prime-))
(not (Flag410prime-))
(not (Flag409prime-))
(not (Flag408prime-))
(not (Flag407prime-))
(not (Flag406prime-))
(not (Flag405prime-))
(not (Flag404prime-))
(not (Flag403prime-))
(not (Flag401prime-))
(not (Flag400prime-))
(not (Flag399prime-))
(not (Flag398prime-))
(not (Flag397prime-))
(not (Flag396prime-))
(not (Flag395prime-))
(not (Flag394prime-))
(not (Flag393prime-))
(not (Flag392prime-))
(not (Flag391prime-))
(not (Flag390prime-))
(not (Flag388prime-))
(not (Flag387prime-))
(not (Flag386prime-))
(not (Flag385prime-))
(not (Flag384prime-))
(not (Flag383prime-))
(not (Flag382prime-))
(not (Flag381prime-))
(not (Flag380prime-))
(not (Flag379prime-))
(not (Flag378prime-))
(not (Flag377prime-))
(not (Flag376prime-))
(not (Flag375prime-))
(not (Flag373prime-))
(not (Flag372prime-))
(not (Flag371prime-))
(not (Flag370prime-))
(not (Flag369prime-))
(not (Flag368prime-))
(not (Flag367prime-))
(not (Flag366prime-))
(not (Flag365prime-))
(not (Flag364prime-))
(not (Flag363prime-))
(not (Flag362prime-))
(not (Flag361prime-))
(not (Flag359prime-))
(not (Flag358prime-))
(not (Flag357prime-))
(not (Flag356prime-))
(not (Flag355prime-))
(not (Flag354prime-))
(not (Flag353prime-))
(not (Flag352prime-))
(not (Flag351prime-))
(not (Flag350prime-))
(not (Flag349prime-))
(not (Flag348prime-))
(not (Flag347prime-))
(not (Flag346prime-))
(not (Flag345prime-))
(not (Flag344prime-))
(not (Flag342prime-))
(not (Flag341prime-))
(not (Flag340prime-))
(not (Flag339prime-))
(not (Flag338prime-))
(not (Flag337prime-))
(not (Flag336prime-))
(not (Flag335prime-))
(not (Flag334prime-))
(not (Flag333prime-))
(not (Flag332prime-))
(not (Flag331prime-))
(not (Flag330prime-))
(not (Flag329prime-))
(not (Flag328prime-))
(not (Flag327prime-))
(not (Flag326prime-))
(not (Flag324prime-))
(not (Flag323prime-))
(not (Flag322prime-))
(not (Flag321prime-))
(not (Flag320prime-))
(not (Flag319prime-))
(not (Flag318prime-))
(not (Flag317prime-))
(not (Flag316prime-))
(not (Flag315prime-))
(not (Flag314prime-))
(not (Flag313prime-))
(not (Flag312prime-))
(not (Flag311prime-))
(not (Flag310prime-))
(not (Flag309prime-))
(not (Flag307prime-))
(not (Flag306prime-))
(not (Flag305prime-))
(not (Flag304prime-))
(not (Flag303prime-))
(not (Flag302prime-))
(not (Flag301prime-))
(not (Flag300prime-))
(not (Flag299prime-))
(not (Flag298prime-))
(not (Flag297prime-))
(not (Flag296prime-))
(not (Flag295prime-))
(not (Flag294prime-))
(not (Flag293prime-))
(not (Flag292prime-))
(not (Flag291prime-))
(not (Flag290prime-))
(not (Flag289prime-))
(not (Flag288prime-))
(not (Flag287prime-))
(not (Flag286prime-))
(not (Flag285prime-))
(not (Flag284prime-))
(not (Flag283prime-))
(not (Flag282prime-))
(not (Flag281prime-))
(not (Flag280prime-))
(not (Flag279prime-))
(not (Flag278prime-))
(not (Flag277prime-))
(not (Flag276prime-))
(not (Flag275prime-))
(not (Flag274prime-))
(not (Flag273prime-))
(not (Flag272prime-))
(not (Flag271prime-))
(not (Flag270prime-))
(not (Flag269prime-))
(not (Flag268prime-))
(not (Flag267prime-))
(not (Flag266prime-))
(not (Flag265prime-))
(not (Flag264prime-))
(not (Flag263prime-))
(not (Flag262prime-))
(not (Flag261prime-))
(not (Flag260prime-))
(not (Flag259prime-))
(not (Flag254prime-))
(not (Flag253prime-))
(not (Flag252prime-))
(not (Flag251prime-))
(not (Flag250prime-))
(not (Flag249prime-))
(not (Flag248prime-))
(not (Flag247prime-))
(not (Flag246prime-))
(not (Flag245prime-))
(not (Flag244prime-))
(not (Flag243prime-))
(not (Flag242prime-))
(not (Flag241prime-))
(not (Flag240prime-))
(not (Flag239prime-))
(not (Flag237prime-))
(not (Flag234prime-))
(not (Flag230prime-))
(not (Flag225prime-))
(not (Flag218prime-))
(not (Flag210prime-))
(not (Flag202prime-))
(not (Flag193prime-))
(not (Flag182prime-))
(not (Flag169prime-))
(not (Flag156prime-))
(not (Flag141prime-))
(not (Flag125prime-))
(not (Flag109prime-))
(not (Flag91prime-))
(not (Flag73prime-))
(not (Flag1prime-))
(not (Flag475prime-))
(not (Flag471prime-))
(not (Flag469prime-))
(not (Flag466prime-))
(not (Flag461prime-))
(not (Flag456prime-))
(not (Flag450prime-))
(not (Flag442prime-))
(not (Flag434prime-))
(not (Flag424prime-))
(not (Flag414prime-))
(not (Flag402prime-))
(not (Flag389prime-))
(not (Flag374prime-))
(not (Flag360prime-))
(not (Flag343prime-))
(not (Flag325prime-))
(not (Flag308prime-))
)
)
(:action Prim1Action-1
:parameters ()
:precondition
(and
(not (ROW1-ROBOT))
)
:effect
(and
(Flag1prime-)
(not (Flag1-))
)

)
(:action CHECKCONSISTENCYACTION
:parameters ()
:precondition
(and
(NOT-ERROR-)
(CHECKCONSISTENCY-)
)
:effect
(and
(when
(and
(RIGHTOF14-ROBOT)
(LEFTOF8-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF6-ROBOT)
(LEFTOF2-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF1-ROBOT)
(ABOVEOF7-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF17-ROBOT)
(BELOWOF3-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF10-ROBOT)
(LEFTOF4-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF4-ROBOT)
(RIGHTOF15-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF6-ROBOT)
(ABOVEOF11-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF15-ROBOT)
(LEFTOF7-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF4-ROBOT)
(RIGHTOF16-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF15-ROBOT)
(RIGHTOF17-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF2-ROBOT)
(ABOVEOF2-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF14-ROBOT)
(LEFTOF6-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF8-ROBOT)
(RIGHTOF11-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF17-ROBOT)
(LEFTOF5-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF12-ROBOT)
(BELOWOF4-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF2-ROBOT)
(ABOVEOF8-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF8-ROBOT)
(LEFTOF2-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF16-ROBOT)
(LEFTOF5-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF16-ROBOT)
(BELOWOF7-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF2-ROBOT)
(ABOVEOF17-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF7-ROBOT)
(LEFTOF1-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF8-ROBOT)
(RIGHTOF12-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF1-ROBOT)
(RIGHTOF14-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF15-ROBOT)
(ABOVEOF16-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF15-ROBOT)
(BELOWOF14-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF12-ROBOT)
(BELOWOF2-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF7-ROBOT)
(RIGHTOF17-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF13-ROBOT)
(LEFTOF6-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF9-ROBOT)
(ABOVEOF17-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF10-ROBOT)
(ABOVEOF17-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF15-ROBOT)
(BELOWOF6-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF1-ROBOT)
(ABOVEOF10-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF9-ROBOT)
(LEFTOF1-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF6-ROBOT)
(RIGHTOF16-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF14-ROBOT)
(LEFTOF3-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF5-ROBOT)
(RIGHTOF8-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF4-ROBOT)
(ABOVEOF11-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF4-ROBOT)
(ABOVEOF10-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF7-ROBOT)
(RIGHTOF13-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF8-ROBOT)
(RIGHTOF17-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF2-ROBOT)
(ABOVEOF15-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF14-ROBOT)
(BELOWOF7-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF15-ROBOT)
(LEFTOF5-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF14-ROBOT)
(BELOWOF14-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF14-ROBOT)
(RIGHTOF16-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF11-ROBOT)
(BELOWOF1-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF2-ROBOT)
(RIGHTOF14-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF5-ROBOT)
(ABOVEOF11-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF17-ROBOT)
(LEFTOF3-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF5-ROBOT)
(ABOVEOF17-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF3-ROBOT)
(RIGHTOF9-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF15-ROBOT)
(LEFTOF1-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF17-ROBOT)
(BELOWOF1-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF6-ROBOT)
(LEFTOF4-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF6-ROBOT)
(ABOVEOF13-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF8-ROBOT)
(BELOWOF1-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF7-ROBOT)
(BELOWOF2-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF14-ROBOT)
(BELOWOF4-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF6-ROBOT)
(LEFTOF5-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF14-ROBOT)
(BELOWOF1-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF13-ROBOT)
(RIGHTOF17-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF5-ROBOT)
(RIGHTOF13-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF9-ROBOT)
(RIGHTOF16-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF15-ROBOT)
(BELOWOF4-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF10-ROBOT)
(BELOWOF3-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF13-ROBOT)
(BELOWOF1-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF1-ROBOT)
(RIGHTOF16-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF13-ROBOT)
(BELOWOF7-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF7-ROBOT)
(ABOVEOF11-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF15-ROBOT)
(BELOWOF1-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF14-ROBOT)
(BELOWOF10-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF16-ROBOT)
(BELOWOF8-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF16-ROBOT)
(BELOWOF5-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF16-ROBOT)
(LEFTOF8-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF17-ROBOT)
(LEFTOF6-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF13-ROBOT)
(BELOWOF12-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF16-ROBOT)
(LEFTOF3-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF16-ROBOT)
(BELOWOF4-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF8-ROBOT)
(BELOWOF5-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF9-ROBOT)
(BELOWOF7-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF17-ROBOT)
(LEFTOF2-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF17-ROBOT)
(BELOWOF12-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF16-ROBOT)
(LEFTOF10-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF17-ROBOT)
(LEFTOF10-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF6-ROBOT)
(ABOVEOF16-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF16-ROBOT)
(LEFTOF11-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF17-ROBOT)
(BELOWOF8-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF14-ROBOT)
(BELOWOF8-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF5-ROBOT)
(RIGHTOF10-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF10-ROBOT)
(RIGHTOF12-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF13-ROBOT)
(LEFTOF11-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF11-ROBOT)
(BELOWOF3-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF1-ROBOT)
(RIGHTOF17-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF5-ROBOT)
(RIGHTOF12-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF4-ROBOT)
(RIGHTOF14-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF8-ROBOT)
(RIGHTOF13-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF3-ROBOT)
(RIGHTOF11-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF7-ROBOT)
(RIGHTOF9-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF4-ROBOT)
(LEFTOF3-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF6-ROBOT)
(ABOVEOF12-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF8-ROBOT)
(LEFTOF1-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF9-ROBOT)
(LEFTOF4-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF2-ROBOT)
(RIGHTOF5-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF10-ROBOT)
(BELOWOF5-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF15-ROBOT)
(LEFTOF9-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF16-ROBOT)
(LEFTOF2-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF3-ROBOT)
(ABOVEOF14-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF16-ROBOT)
(BELOWOF14-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF9-ROBOT)
(BELOWOF1-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF9-ROBOT)
(LEFTOF2-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF10-ROBOT)
(ABOVEOF13-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF15-ROBOT)
(LEFTOF2-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF1-ROBOT)
(ABOVEOF16-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF13-ROBOT)
(BELOWOF3-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF11-ROBOT)
(ABOVEOF17-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF2-ROBOT)
(RIGHTOF10-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF7-ROBOT)
(ABOVEOF17-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF7-ROBOT)
(RIGHTOF14-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF3-ROBOT)
(ABOVEOF15-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF16-ROBOT)
(LEFTOF7-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF10-ROBOT)
(LEFTOF3-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF6-ROBOT)
(LEFTOF1-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF4-ROBOT)
(RIGHTOF12-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF2-ROBOT)
(RIGHTOF3-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF6-ROBOT)
(ABOVEOF17-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF7-ROBOT)
(ABOVEOF15-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF8-ROBOT)
(ABOVEOF9-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF8-ROBOT)
(ABOVEOF15-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF10-ROBOT)
(LEFTOF10-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF9-ROBOT)
(LEFTOF8-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF15-ROBOT)
(BELOWOF9-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF14-ROBOT)
(BELOWOF2-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF14-ROBOT)
(LEFTOF5-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF17-ROBOT)
(ABOVEOF17-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF11-ROBOT)
(RIGHTOF17-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF8-ROBOT)
(ABOVEOF12-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF6-ROBOT)
(BELOWOF3-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF17-ROBOT)
(LEFTOF9-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF9-ROBOT)
(RIGHTOF11-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF14-ROBOT)
(LEFTOF9-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF15-ROBOT)
(LEFTOF8-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF4-ROBOT)
(RIGHTOF13-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF7-ROBOT)
(ABOVEOF12-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF5-ROBOT)
(BELOWOF3-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF12-ROBOT)
(RIGHTOF16-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF12-ROBOT)
(LEFTOF3-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF13-ROBOT)
(LEFTOF2-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF8-ROBOT)
(BELOWOF3-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF10-ROBOT)
(LEFTOF1-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF15-ROBOT)
(LEFTOF3-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF4-ROBOT)
(ABOVEOF13-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF15-ROBOT)
(LEFTOF13-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF11-ROBOT)
(LEFTOF5-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF11-ROBOT)
(BELOWOF2-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF4-ROBOT)
(RIGHTOF8-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF15-ROBOT)
(LEFTOF12-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF13-ROBOT)
(BELOWOF5-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF6-ROBOT)
(BELOWOF4-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF11-ROBOT)
(ABOVEOF13-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF11-ROBOT)
(ABOVEOF12-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF6-ROBOT)
(RIGHTOF10-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF10-ROBOT)
(LEFTOF7-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF13-ROBOT)
(LEFTOF3-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF11-ROBOT)
(LEFTOF1-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF17-ROBOT)
(LEFTOF4-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF2-ROBOT)
(RIGHTOF7-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF11-ROBOT)
(ABOVEOF14-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF1-ROBOT)
(ABOVEOF4-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF15-ROBOT)
(BELOWOF12-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF4-ROBOT)
(LEFTOF2-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF5-ROBOT)
(ABOVEOF6-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF14-ROBOT)
(BELOWOF9-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF7-ROBOT)
(BELOWOF5-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF16-ROBOT)
(BELOWOF9-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF4-ROBOT)
(ABOVEOF9-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF6-ROBOT)
(LEFTOF6-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF12-ROBOT)
(LEFTOF1-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF12-ROBOT)
(LEFTOF2-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF16-ROBOT)
(BELOWOF13-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF10-ROBOT)
(BELOWOF6-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF17-ROBOT)
(LEFTOF12-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF9-ROBOT)
(RIGHTOF13-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF4-ROBOT)
(RIGHTOF11-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF1-ROBOT)
(RIGHTOF4-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF7-ROBOT)
(BELOWOF6-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF7-ROBOT)
(LEFTOF3-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF11-ROBOT)
(ABOVEOF15-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF1-ROBOT)
(RIGHTOF2-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF4-ROBOT)
(ABOVEOF5-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF12-ROBOT)
(LEFTOF7-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF5-ROBOT)
(ABOVEOF14-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF2-ROBOT)
(ABOVEOF16-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF10-ROBOT)
(ABOVEOF12-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF9-ROBOT)
(ABOVEOF13-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF10-ROBOT)
(ABOVEOF15-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF2-ROBOT)
(ABOVEOF9-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF2-ROBOT)
(ABOVEOF6-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF9-ROBOT)
(LEFTOF5-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF5-ROBOT)
(BELOWOF5-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF4-ROBOT)
(ABOVEOF17-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF11-ROBOT)
(RIGHTOF15-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF1-ROBOT)
(ABOVEOF5-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF3-ROBOT)
(RIGHTOF8-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF6-ROBOT)
(BELOWOF1-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF16-ROBOT)
(LEFTOF15-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF7-ROBOT)
(BELOWOF3-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF16-ROBOT)
(BELOWOF3-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF5-ROBOT)
(LEFTOF1-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF7-ROBOT)
(BELOWOF7-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF5-ROBOT)
(RIGHTOF7-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF8-ROBOT)
(BELOWOF4-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF12-ROBOT)
(LEFTOF6-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF2-ROBOT)
(ABOVEOF5-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF11-ROBOT)
(ABOVEOF16-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF1-ROBOT)
(ABOVEOF12-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF15-ROBOT)
(LEFTOF6-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF14-ROBOT)
(LEFTOF11-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF6-ROBOT)
(BELOWOF6-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF1-ROBOT)
(BELOWOF1-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF11-ROBOT)
(BELOWOF9-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF8-ROBOT)
(ABOVEOF13-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF2-ROBOT)
(ABOVEOF13-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF5-ROBOT)
(ABOVEOF15-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF10-ROBOT)
(LEFTOF8-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF9-ROBOT)
(BELOWOF3-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF6-ROBOT)
(ABOVEOF14-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF13-ROBOT)
(LEFTOF1-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF3-ROBOT)
(RIGHTOF6-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF10-ROBOT)
(ABOVEOF16-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF14-ROBOT)
(RIGHTOF15-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF10-ROBOT)
(RIGHTOF15-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF9-ROBOT)
(BELOWOF5-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF2-ROBOT)
(ABOVEOF10-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF1-ROBOT)
(ABOVEOF2-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF15-ROBOT)
(BELOWOF13-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF6-ROBOT)
(RIGHTOF9-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF14-ROBOT)
(RIGHTOF14-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF12-ROBOT)
(BELOWOF5-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF10-ROBOT)
(BELOWOF7-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF12-ROBOT)
(RIGHTOF14-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF6-ROBOT)
(ABOVEOF9-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF7-ROBOT)
(RIGHTOF11-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF14-ROBOT)
(BELOWOF12-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF8-ROBOT)
(ABOVEOF10-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF6-ROBOT)
(RIGHTOF8-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF12-ROBOT)
(LEFTOF12-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF9-ROBOT)
(ABOVEOF12-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF17-ROBOT)
(LEFTOF14-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF8-ROBOT)
(ABOVEOF11-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF10-ROBOT)
(BELOWOF9-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF10-ROBOT)
(RIGHTOF11-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF17-ROBOT)
(RIGHTOF17-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF12-ROBOT)
(BELOWOF3-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF5-ROBOT)
(LEFTOF3-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF17-ROBOT)
(BELOWOF14-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF4-ROBOT)
(ABOVEOF7-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF4-ROBOT)
(BELOWOF3-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF1-ROBOT)
(RIGHTOF3-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF4-ROBOT)
(BELOWOF4-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF11-ROBOT)
(RIGHTOF11-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF17-ROBOT)
(LEFTOF16-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF7-ROBOT)
(RIGHTOF8-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF10-ROBOT)
(RIGHTOF14-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF12-ROBOT)
(LEFTOF9-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF5-ROBOT)
(RIGHTOF5-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF10-ROBOT)
(ABOVEOF11-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF14-ROBOT)
(BELOWOF13-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF13-ROBOT)
(ABOVEOF17-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF11-ROBOT)
(RIGHTOF12-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF6-ROBOT)
(RIGHTOF11-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF10-ROBOT)
(RIGHTOF13-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF2-ROBOT)
(RIGHTOF11-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF4-ROBOT)
(RIGHTOF7-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF10-ROBOT)
(BELOWOF10-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF5-ROBOT)
(LEFTOF4-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF12-ROBOT)
(ABOVEOF16-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF15-ROBOT)
(ABOVEOF17-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF10-ROBOT)
(LEFTOF9-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF3-ROBOT)
(RIGHTOF3-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF1-ROBOT)
(ABOVEOF3-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF3-ROBOT)
(BELOWOF2-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF14-ROBOT)
(LEFTOF13-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF7-ROBOT)
(LEFTOF6-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF4-ROBOT)
(BELOWOF2-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF8-ROBOT)
(BELOWOF6-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF16-ROBOT)
(LEFTOF13-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF7-ROBOT)
(ABOVEOF8-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF3-ROBOT)
(ABOVEOF3-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF15-ROBOT)
(BELOWOF15-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF15-ROBOT)
(RIGHTOF15-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF13-ROBOT)
(RIGHTOF13-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF4-ROBOT)
(RIGHTOF4-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF16-ROBOT)
(BELOWOF16-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF7-ROBOT)
(RIGHTOF7-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF8-ROBOT)
(BELOWOF8-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF8-ROBOT)
(LEFTOF8-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF16-ROBOT)
(LEFTOF16-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF16-ROBOT)
(ABOVEOF17-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF13-ROBOT)
(ABOVEOF13-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF9-ROBOT)
(BELOWOF9-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF11-ROBOT)
(BELOWOF11-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF13-ROBOT)
(LEFTOF12-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF12-ROBOT)
(ABOVEOF12-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF9-ROBOT)
(RIGHTOF9-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF2-ROBOT)
(LEFTOF2-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF1-ROBOT)
(RIGHTOF1-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(NOT-CHECKCONSISTENCY-)
(not (CHECKCONSISTENCY-))
(not (Flag473prime-))
(not (Flag258prime-))
(not (Flag257prime-))
(not (Flag256prime-))
(not (Flag255prime-))
(not (Flag21prime-))
(not (Flag20prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag472prime-))
(not (Flag238prime-))
(not (Flag236prime-))
(not (Flag235prime-))
(not (Flag233prime-))
(not (Flag232prime-))
(not (Flag231prime-))
(not (Flag229prime-))
(not (Flag228prime-))
(not (Flag227prime-))
(not (Flag226prime-))
(not (Flag224prime-))
(not (Flag223prime-))
(not (Flag222prime-))
(not (Flag221prime-))
(not (Flag220prime-))
(not (Flag219prime-))
(not (Flag217prime-))
(not (Flag216prime-))
(not (Flag215prime-))
(not (Flag214prime-))
(not (Flag213prime-))
(not (Flag212prime-))
(not (Flag211prime-))
(not (Flag209prime-))
(not (Flag208prime-))
(not (Flag207prime-))
(not (Flag206prime-))
(not (Flag205prime-))
(not (Flag204prime-))
(not (Flag203prime-))
(not (Flag201prime-))
(not (Flag200prime-))
(not (Flag199prime-))
(not (Flag198prime-))
(not (Flag197prime-))
(not (Flag196prime-))
(not (Flag195prime-))
(not (Flag194prime-))
(not (Flag192prime-))
(not (Flag191prime-))
(not (Flag190prime-))
(not (Flag189prime-))
(not (Flag188prime-))
(not (Flag187prime-))
(not (Flag186prime-))
(not (Flag185prime-))
(not (Flag184prime-))
(not (Flag183prime-))
(not (Flag181prime-))
(not (Flag180prime-))
(not (Flag179prime-))
(not (Flag178prime-))
(not (Flag177prime-))
(not (Flag176prime-))
(not (Flag175prime-))
(not (Flag174prime-))
(not (Flag173prime-))
(not (Flag172prime-))
(not (Flag171prime-))
(not (Flag170prime-))
(not (Flag168prime-))
(not (Flag167prime-))
(not (Flag166prime-))
(not (Flag165prime-))
(not (Flag164prime-))
(not (Flag163prime-))
(not (Flag162prime-))
(not (Flag161prime-))
(not (Flag160prime-))
(not (Flag159prime-))
(not (Flag158prime-))
(not (Flag157prime-))
(not (Flag155prime-))
(not (Flag154prime-))
(not (Flag153prime-))
(not (Flag152prime-))
(not (Flag151prime-))
(not (Flag150prime-))
(not (Flag149prime-))
(not (Flag148prime-))
(not (Flag147prime-))
(not (Flag146prime-))
(not (Flag145prime-))
(not (Flag144prime-))
(not (Flag143prime-))
(not (Flag142prime-))
(not (Flag140prime-))
(not (Flag139prime-))
(not (Flag138prime-))
(not (Flag137prime-))
(not (Flag136prime-))
(not (Flag135prime-))
(not (Flag134prime-))
(not (Flag133prime-))
(not (Flag132prime-))
(not (Flag131prime-))
(not (Flag130prime-))
(not (Flag129prime-))
(not (Flag128prime-))
(not (Flag127prime-))
(not (Flag126prime-))
(not (Flag124prime-))
(not (Flag123prime-))
(not (Flag122prime-))
(not (Flag121prime-))
(not (Flag120prime-))
(not (Flag119prime-))
(not (Flag118prime-))
(not (Flag117prime-))
(not (Flag116prime-))
(not (Flag115prime-))
(not (Flag114prime-))
(not (Flag113prime-))
(not (Flag112prime-))
(not (Flag111prime-))
(not (Flag110prime-))
(not (Flag108prime-))
(not (Flag107prime-))
(not (Flag106prime-))
(not (Flag105prime-))
(not (Flag104prime-))
(not (Flag103prime-))
(not (Flag102prime-))
(not (Flag101prime-))
(not (Flag100prime-))
(not (Flag99prime-))
(not (Flag98prime-))
(not (Flag97prime-))
(not (Flag96prime-))
(not (Flag95prime-))
(not (Flag94prime-))
(not (Flag93prime-))
(not (Flag92prime-))
(not (Flag90prime-))
(not (Flag89prime-))
(not (Flag88prime-))
(not (Flag87prime-))
(not (Flag86prime-))
(not (Flag85prime-))
(not (Flag84prime-))
(not (Flag83prime-))
(not (Flag82prime-))
(not (Flag81prime-))
(not (Flag80prime-))
(not (Flag79prime-))
(not (Flag78prime-))
(not (Flag77prime-))
(not (Flag76prime-))
(not (Flag75prime-))
(not (Flag74prime-))
(not (Flag72prime-))
(not (Flag71prime-))
(not (Flag70prime-))
(not (Flag69prime-))
(not (Flag68prime-))
(not (Flag67prime-))
(not (Flag66prime-))
(not (Flag65prime-))
(not (Flag64prime-))
(not (Flag63prime-))
(not (Flag62prime-))
(not (Flag61prime-))
(not (Flag60prime-))
(not (Flag59prime-))
(not (Flag58prime-))
(not (Flag57prime-))
(not (Flag56prime-))
(not (Flag55prime-))
(not (Flag54prime-))
(not (Flag53prime-))
(not (Flag52prime-))
(not (Flag51prime-))
(not (Flag50prime-))
(not (Flag49prime-))
(not (Flag48prime-))
(not (Flag47prime-))
(not (Flag46prime-))
(not (Flag45prime-))
(not (Flag44prime-))
(not (Flag43prime-))
(not (Flag42prime-))
(not (Flag41prime-))
(not (Flag40prime-))
(not (Flag39prime-))
(not (Flag38prime-))
(not (Flag37prime-))
(not (Flag36prime-))
(not (Flag35prime-))
(not (Flag34prime-))
(not (Flag33prime-))
(not (Flag32prime-))
(not (Flag31prime-))
(not (Flag30prime-))
(not (Flag29prime-))
(not (Flag28prime-))
(not (Flag27prime-))
(not (Flag26prime-))
(not (Flag25prime-))
(not (Flag24prime-))
(not (Flag23prime-))
(not (Flag22prime-))
(not (Flag19prime-))
(not (Flag18prime-))
(not (Flag17prime-))
(not (Flag16prime-))
(not (Flag15prime-))
(not (Flag14prime-))
(not (Flag13prime-))
(not (Flag12prime-))
(not (Flag11prime-))
(not (Flag10prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag474prime-))
(not (Flag470prime-))
(not (Flag468prime-))
(not (Flag467prime-))
(not (Flag465prime-))
(not (Flag464prime-))
(not (Flag463prime-))
(not (Flag462prime-))
(not (Flag460prime-))
(not (Flag459prime-))
(not (Flag458prime-))
(not (Flag457prime-))
(not (Flag455prime-))
(not (Flag454prime-))
(not (Flag453prime-))
(not (Flag452prime-))
(not (Flag451prime-))
(not (Flag449prime-))
(not (Flag448prime-))
(not (Flag447prime-))
(not (Flag446prime-))
(not (Flag445prime-))
(not (Flag444prime-))
(not (Flag443prime-))
(not (Flag441prime-))
(not (Flag440prime-))
(not (Flag439prime-))
(not (Flag438prime-))
(not (Flag437prime-))
(not (Flag436prime-))
(not (Flag435prime-))
(not (Flag433prime-))
(not (Flag432prime-))
(not (Flag431prime-))
(not (Flag430prime-))
(not (Flag429prime-))
(not (Flag428prime-))
(not (Flag427prime-))
(not (Flag426prime-))
(not (Flag425prime-))
(not (Flag423prime-))
(not (Flag422prime-))
(not (Flag421prime-))
(not (Flag420prime-))
(not (Flag419prime-))
(not (Flag418prime-))
(not (Flag417prime-))
(not (Flag416prime-))
(not (Flag415prime-))
(not (Flag413prime-))
(not (Flag412prime-))
(not (Flag411prime-))
(not (Flag410prime-))
(not (Flag409prime-))
(not (Flag408prime-))
(not (Flag407prime-))
(not (Flag406prime-))
(not (Flag405prime-))
(not (Flag404prime-))
(not (Flag403prime-))
(not (Flag401prime-))
(not (Flag400prime-))
(not (Flag399prime-))
(not (Flag398prime-))
(not (Flag397prime-))
(not (Flag396prime-))
(not (Flag395prime-))
(not (Flag394prime-))
(not (Flag393prime-))
(not (Flag392prime-))
(not (Flag391prime-))
(not (Flag390prime-))
(not (Flag388prime-))
(not (Flag387prime-))
(not (Flag386prime-))
(not (Flag385prime-))
(not (Flag384prime-))
(not (Flag383prime-))
(not (Flag382prime-))
(not (Flag381prime-))
(not (Flag380prime-))
(not (Flag379prime-))
(not (Flag378prime-))
(not (Flag377prime-))
(not (Flag376prime-))
(not (Flag375prime-))
(not (Flag373prime-))
(not (Flag372prime-))
(not (Flag371prime-))
(not (Flag370prime-))
(not (Flag369prime-))
(not (Flag368prime-))
(not (Flag367prime-))
(not (Flag366prime-))
(not (Flag365prime-))
(not (Flag364prime-))
(not (Flag363prime-))
(not (Flag362prime-))
(not (Flag361prime-))
(not (Flag359prime-))
(not (Flag358prime-))
(not (Flag357prime-))
(not (Flag356prime-))
(not (Flag355prime-))
(not (Flag354prime-))
(not (Flag353prime-))
(not (Flag352prime-))
(not (Flag351prime-))
(not (Flag350prime-))
(not (Flag349prime-))
(not (Flag348prime-))
(not (Flag347prime-))
(not (Flag346prime-))
(not (Flag345prime-))
(not (Flag344prime-))
(not (Flag342prime-))
(not (Flag341prime-))
(not (Flag340prime-))
(not (Flag339prime-))
(not (Flag338prime-))
(not (Flag337prime-))
(not (Flag336prime-))
(not (Flag335prime-))
(not (Flag334prime-))
(not (Flag333prime-))
(not (Flag332prime-))
(not (Flag331prime-))
(not (Flag330prime-))
(not (Flag329prime-))
(not (Flag328prime-))
(not (Flag327prime-))
(not (Flag326prime-))
(not (Flag324prime-))
(not (Flag323prime-))
(not (Flag322prime-))
(not (Flag321prime-))
(not (Flag320prime-))
(not (Flag319prime-))
(not (Flag318prime-))
(not (Flag317prime-))
(not (Flag316prime-))
(not (Flag315prime-))
(not (Flag314prime-))
(not (Flag313prime-))
(not (Flag312prime-))
(not (Flag311prime-))
(not (Flag310prime-))
(not (Flag309prime-))
(not (Flag307prime-))
(not (Flag306prime-))
(not (Flag305prime-))
(not (Flag304prime-))
(not (Flag303prime-))
(not (Flag302prime-))
(not (Flag301prime-))
(not (Flag300prime-))
(not (Flag299prime-))
(not (Flag298prime-))
(not (Flag297prime-))
(not (Flag296prime-))
(not (Flag295prime-))
(not (Flag294prime-))
(not (Flag293prime-))
(not (Flag292prime-))
(not (Flag291prime-))
(not (Flag290prime-))
(not (Flag289prime-))
(not (Flag288prime-))
(not (Flag287prime-))
(not (Flag286prime-))
(not (Flag285prime-))
(not (Flag284prime-))
(not (Flag283prime-))
(not (Flag282prime-))
(not (Flag281prime-))
(not (Flag280prime-))
(not (Flag279prime-))
(not (Flag278prime-))
(not (Flag277prime-))
(not (Flag276prime-))
(not (Flag275prime-))
(not (Flag274prime-))
(not (Flag273prime-))
(not (Flag272prime-))
(not (Flag271prime-))
(not (Flag270prime-))
(not (Flag269prime-))
(not (Flag268prime-))
(not (Flag267prime-))
(not (Flag266prime-))
(not (Flag265prime-))
(not (Flag264prime-))
(not (Flag263prime-))
(not (Flag262prime-))
(not (Flag261prime-))
(not (Flag260prime-))
(not (Flag259prime-))
(not (Flag254prime-))
(not (Flag253prime-))
(not (Flag252prime-))
(not (Flag251prime-))
(not (Flag250prime-))
(not (Flag249prime-))
(not (Flag248prime-))
(not (Flag247prime-))
(not (Flag246prime-))
(not (Flag245prime-))
(not (Flag244prime-))
(not (Flag243prime-))
(not (Flag242prime-))
(not (Flag241prime-))
(not (Flag240prime-))
(not (Flag239prime-))
(not (Flag237prime-))
(not (Flag234prime-))
(not (Flag230prime-))
(not (Flag225prime-))
(not (Flag218prime-))
(not (Flag210prime-))
(not (Flag202prime-))
(not (Flag193prime-))
(not (Flag182prime-))
(not (Flag169prime-))
(not (Flag156prime-))
(not (Flag141prime-))
(not (Flag125prime-))
(not (Flag109prime-))
(not (Flag91prime-))
(not (Flag73prime-))
(not (Flag1prime-))
(not (Flag475prime-))
(not (Flag471prime-))
(not (Flag469prime-))
(not (Flag466prime-))
(not (Flag461prime-))
(not (Flag456prime-))
(not (Flag450prime-))
(not (Flag442prime-))
(not (Flag434prime-))
(not (Flag424prime-))
(not (Flag414prime-))
(not (Flag402prime-))
(not (Flag389prime-))
(not (Flag374prime-))
(not (Flag360prime-))
(not (Flag343prime-))
(not (Flag325prime-))
(not (Flag308prime-))
)
)
(:action Flag3Action
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(Flag2-)
(Flag2prime-)
)
:effect
(and
(Flag3-)
(Flag3prime-)
)

)
(:action Prim3Action-2
:parameters ()
:precondition
(and
(not (Flag2-))
(Flag2prime-)
)
:effect
(and
(Flag3prime-)
(not (Flag3-))
)

)
(:action Flag5Action-0
:parameters ()
:precondition
(and
(BELOWOF2-ROBOT)
)
:effect
(and
(Flag5-)
(Flag5prime-)
)

)
(:action Flag5Action-1
:parameters ()
:precondition
(and
(BELOWOF1-ROBOT)
)
:effect
(and
(Flag5-)
(Flag5prime-)
)

)
(:action Prim5Action
:parameters ()
:precondition
(and
(not (BELOWOF2-ROBOT))
(not (BELOWOF1-ROBOT))
)
:effect
(and
(Flag5prime-)
(not (Flag5-))
)

)
(:action Flag6Action-0
:parameters ()
:precondition
(and
(BELOWOF2-ROBOT)
)
:effect
(and
(Flag6-)
(Flag6prime-)
)

)
(:action Flag6Action-1
:parameters ()
:precondition
(and
(BELOWOF1-ROBOT)
)
:effect
(and
(Flag6-)
(Flag6prime-)
)

)
(:action Flag6Action-2
:parameters ()
:precondition
(and
(BELOWOF3-ROBOT)
)
:effect
(and
(Flag6-)
(Flag6prime-)
)

)
(:action Prim6Action
:parameters ()
:precondition
(and
(not (BELOWOF2-ROBOT))
(not (BELOWOF1-ROBOT))
(not (BELOWOF3-ROBOT))
)
:effect
(and
(Flag6prime-)
(not (Flag6-))
)

)
(:action Flag7Action-0
:parameters ()
:precondition
(and
(BELOWOF2-ROBOT)
)
:effect
(and
(Flag7-)
(Flag7prime-)
)

)
(:action Flag7Action-1
:parameters ()
:precondition
(and
(BELOWOF1-ROBOT)
)
:effect
(and
(Flag7-)
(Flag7prime-)
)

)
(:action Flag7Action-2
:parameters ()
:precondition
(and
(BELOWOF3-ROBOT)
)
:effect
(and
(Flag7-)
(Flag7prime-)
)

)
(:action Flag7Action-3
:parameters ()
:precondition
(and
(BELOWOF4-ROBOT)
)
:effect
(and
(Flag7-)
(Flag7prime-)
)

)
(:action Prim7Action
:parameters ()
:precondition
(and
(not (BELOWOF2-ROBOT))
(not (BELOWOF1-ROBOT))
(not (BELOWOF3-ROBOT))
(not (BELOWOF4-ROBOT))
)
:effect
(and
(Flag7prime-)
(not (Flag7-))
)

)
(:action Flag8Action-0
:parameters ()
:precondition
(and
(BELOWOF2-ROBOT)
)
:effect
(and
(Flag8-)
(Flag8prime-)
)

)
(:action Flag8Action-1
:parameters ()
:precondition
(and
(BELOWOF1-ROBOT)
)
:effect
(and
(Flag8-)
(Flag8prime-)
)

)
(:action Flag8Action-2
:parameters ()
:precondition
(and
(BELOWOF3-ROBOT)
)
:effect
(and
(Flag8-)
(Flag8prime-)
)

)
(:action Flag8Action-3
:parameters ()
:precondition
(and
(BELOWOF4-ROBOT)
)
:effect
(and
(Flag8-)
(Flag8prime-)
)

)
(:action Flag8Action-4
:parameters ()
:precondition
(and
(BELOWOF5-ROBOT)
)
:effect
(and
(Flag8-)
(Flag8prime-)
)

)
(:action Prim8Action
:parameters ()
:precondition
(and
(not (BELOWOF2-ROBOT))
(not (BELOWOF1-ROBOT))
(not (BELOWOF3-ROBOT))
(not (BELOWOF4-ROBOT))
(not (BELOWOF5-ROBOT))
)
:effect
(and
(Flag8prime-)
(not (Flag8-))
)

)
(:action Flag9Action-0
:parameters ()
:precondition
(and
(BELOWOF6-ROBOT)
)
:effect
(and
(Flag9-)
(Flag9prime-)
)

)
(:action Flag9Action-1
:parameters ()
:precondition
(and
(BELOWOF5-ROBOT)
)
:effect
(and
(Flag9-)
(Flag9prime-)
)

)
(:action Flag9Action-2
:parameters ()
:precondition
(and
(BELOWOF2-ROBOT)
)
:effect
(and
(Flag9-)
(Flag9prime-)
)

)
(:action Flag9Action-3
:parameters ()
:precondition
(and
(BELOWOF1-ROBOT)
)
:effect
(and
(Flag9-)
(Flag9prime-)
)

)
(:action Flag9Action-4
:parameters ()
:precondition
(and
(BELOWOF4-ROBOT)
)
:effect
(and
(Flag9-)
(Flag9prime-)
)

)
(:action Flag9Action-5
:parameters ()
:precondition
(and
(BELOWOF3-ROBOT)
)
:effect
(and
(Flag9-)
(Flag9prime-)
)

)
(:action Prim9Action
:parameters ()
:precondition
(and
(not (BELOWOF6-ROBOT))
(not (BELOWOF5-ROBOT))
(not (BELOWOF2-ROBOT))
(not (BELOWOF1-ROBOT))
(not (BELOWOF4-ROBOT))
(not (BELOWOF3-ROBOT))
)
:effect
(and
(Flag9prime-)
(not (Flag9-))
)

)
(:action Flag10Action-0
:parameters ()
:precondition
(and
(BELOWOF6-ROBOT)
)
:effect
(and
(Flag10-)
(Flag10prime-)
)

)
(:action Flag10Action-1
:parameters ()
:precondition
(and
(BELOWOF5-ROBOT)
)
:effect
(and
(Flag10-)
(Flag10prime-)
)

)
(:action Flag10Action-2
:parameters ()
:precondition
(and
(BELOWOF2-ROBOT)
)
:effect
(and
(Flag10-)
(Flag10prime-)
)

)
(:action Flag10Action-3
:parameters ()
:precondition
(and
(BELOWOF1-ROBOT)
)
:effect
(and
(Flag10-)
(Flag10prime-)
)

)
(:action Flag10Action-4
:parameters ()
:precondition
(and
(BELOWOF4-ROBOT)
)
:effect
(and
(Flag10-)
(Flag10prime-)
)

)
(:action Flag10Action-5
:parameters ()
:precondition
(and
(BELOWOF7-ROBOT)
)
:effect
(and
(Flag10-)
(Flag10prime-)
)

)
(:action Flag10Action-6
:parameters ()
:precondition
(and
(BELOWOF3-ROBOT)
)
:effect
(and
(Flag10-)
(Flag10prime-)
)

)
(:action Prim10Action
:parameters ()
:precondition
(and
(not (BELOWOF6-ROBOT))
(not (BELOWOF5-ROBOT))
(not (BELOWOF2-ROBOT))
(not (BELOWOF1-ROBOT))
(not (BELOWOF4-ROBOT))
(not (BELOWOF7-ROBOT))
(not (BELOWOF3-ROBOT))
)
:effect
(and
(Flag10prime-)
(not (Flag10-))
)

)
(:action Flag11Action-0
:parameters ()
:precondition
(and
(BELOWOF6-ROBOT)
)
:effect
(and
(Flag11-)
(Flag11prime-)
)

)
(:action Flag11Action-1
:parameters ()
:precondition
(and
(BELOWOF5-ROBOT)
)
:effect
(and
(Flag11-)
(Flag11prime-)
)

)
(:action Flag11Action-2
:parameters ()
:precondition
(and
(BELOWOF2-ROBOT)
)
:effect
(and
(Flag11-)
(Flag11prime-)
)

)
(:action Flag11Action-3
:parameters ()
:precondition
(and
(BELOWOF1-ROBOT)
)
:effect
(and
(Flag11-)
(Flag11prime-)
)

)
(:action Flag11Action-4
:parameters ()
:precondition
(and
(BELOWOF4-ROBOT)
)
:effect
(and
(Flag11-)
(Flag11prime-)
)

)
(:action Flag11Action-5
:parameters ()
:precondition
(and
(BELOWOF7-ROBOT)
)
:effect
(and
(Flag11-)
(Flag11prime-)
)

)
(:action Flag11Action-6
:parameters ()
:precondition
(and
(BELOWOF3-ROBOT)
)
:effect
(and
(Flag11-)
(Flag11prime-)
)

)
(:action Flag11Action-7
:parameters ()
:precondition
(and
(BELOWOF8-ROBOT)
)
:effect
(and
(Flag11-)
(Flag11prime-)
)

)
(:action Prim11Action
:parameters ()
:precondition
(and
(not (BELOWOF6-ROBOT))
(not (BELOWOF5-ROBOT))
(not (BELOWOF2-ROBOT))
(not (BELOWOF1-ROBOT))
(not (BELOWOF4-ROBOT))
(not (BELOWOF7-ROBOT))
(not (BELOWOF3-ROBOT))
(not (BELOWOF8-ROBOT))
)
:effect
(and
(Flag11prime-)
(not (Flag11-))
)

)
(:action Flag12Action-0
:parameters ()
:precondition
(and
(BELOWOF6-ROBOT)
)
:effect
(and
(Flag12-)
(Flag12prime-)
)

)
(:action Flag12Action-1
:parameters ()
:precondition
(and
(BELOWOF5-ROBOT)
)
:effect
(and
(Flag12-)
(Flag12prime-)
)

)
(:action Flag12Action-2
:parameters ()
:precondition
(and
(BELOWOF2-ROBOT)
)
:effect
(and
(Flag12-)
(Flag12prime-)
)

)
(:action Flag12Action-3
:parameters ()
:precondition
(and
(BELOWOF1-ROBOT)
)
:effect
(and
(Flag12-)
(Flag12prime-)
)

)
(:action Flag12Action-4
:parameters ()
:precondition
(and
(BELOWOF4-ROBOT)
)
:effect
(and
(Flag12-)
(Flag12prime-)
)

)
(:action Flag12Action-5
:parameters ()
:precondition
(and
(BELOWOF7-ROBOT)
)
:effect
(and
(Flag12-)
(Flag12prime-)
)

)
(:action Flag12Action-6
:parameters ()
:precondition
(and
(BELOWOF9-ROBOT)
)
:effect
(and
(Flag12-)
(Flag12prime-)
)

)
(:action Flag12Action-7
:parameters ()
:precondition
(and
(BELOWOF3-ROBOT)
)
:effect
(and
(Flag12-)
(Flag12prime-)
)

)
(:action Flag12Action-8
:parameters ()
:precondition
(and
(BELOWOF8-ROBOT)
)
:effect
(and
(Flag12-)
(Flag12prime-)
)

)
(:action Prim12Action
:parameters ()
:precondition
(and
(not (BELOWOF6-ROBOT))
(not (BELOWOF5-ROBOT))
(not (BELOWOF2-ROBOT))
(not (BELOWOF1-ROBOT))
(not (BELOWOF4-ROBOT))
(not (BELOWOF7-ROBOT))
(not (BELOWOF9-ROBOT))
(not (BELOWOF3-ROBOT))
(not (BELOWOF8-ROBOT))
)
:effect
(and
(Flag12prime-)
(not (Flag12-))
)

)
(:action Flag13Action-0
:parameters ()
:precondition
(and
(BELOWOF10-ROBOT)
)
:effect
(and
(Flag13-)
(Flag13prime-)
)

)
(:action Flag13Action-1
:parameters ()
:precondition
(and
(BELOWOF6-ROBOT)
)
:effect
(and
(Flag13-)
(Flag13prime-)
)

)
(:action Flag13Action-2
:parameters ()
:precondition
(and
(BELOWOF5-ROBOT)
)
:effect
(and
(Flag13-)
(Flag13prime-)
)

)
(:action Flag13Action-3
:parameters ()
:precondition
(and
(BELOWOF2-ROBOT)
)
:effect
(and
(Flag13-)
(Flag13prime-)
)

)
(:action Flag13Action-4
:parameters ()
:precondition
(and
(BELOWOF1-ROBOT)
)
:effect
(and
(Flag13-)
(Flag13prime-)
)

)
(:action Flag13Action-5
:parameters ()
:precondition
(and
(BELOWOF4-ROBOT)
)
:effect
(and
(Flag13-)
(Flag13prime-)
)

)
(:action Flag13Action-6
:parameters ()
:precondition
(and
(BELOWOF7-ROBOT)
)
:effect
(and
(Flag13-)
(Flag13prime-)
)

)
(:action Flag13Action-7
:parameters ()
:precondition
(and
(BELOWOF9-ROBOT)
)
:effect
(and
(Flag13-)
(Flag13prime-)
)

)
(:action Flag13Action-8
:parameters ()
:precondition
(and
(BELOWOF3-ROBOT)
)
:effect
(and
(Flag13-)
(Flag13prime-)
)

)
(:action Flag13Action-9
:parameters ()
:precondition
(and
(BELOWOF8-ROBOT)
)
:effect
(and
(Flag13-)
(Flag13prime-)
)

)
(:action Prim13Action
:parameters ()
:precondition
(and
(not (BELOWOF10-ROBOT))
(not (BELOWOF6-ROBOT))
(not (BELOWOF5-ROBOT))
(not (BELOWOF2-ROBOT))
(not (BELOWOF1-ROBOT))
(not (BELOWOF4-ROBOT))
(not (BELOWOF7-ROBOT))
(not (BELOWOF9-ROBOT))
(not (BELOWOF3-ROBOT))
(not (BELOWOF8-ROBOT))
)
:effect
(and
(Flag13prime-)
(not (Flag13-))
)

)
(:action Flag14Action-0
:parameters ()
:precondition
(and
(BELOWOF10-ROBOT)
)
:effect
(and
(Flag14-)
(Flag14prime-)
)

)
(:action Flag14Action-1
:parameters ()
:precondition
(and
(BELOWOF11-ROBOT)
)
:effect
(and
(Flag14-)
(Flag14prime-)
)

)
(:action Flag14Action-2
:parameters ()
:precondition
(and
(BELOWOF6-ROBOT)
)
:effect
(and
(Flag14-)
(Flag14prime-)
)

)
(:action Flag14Action-3
:parameters ()
:precondition
(and
(BELOWOF5-ROBOT)
)
:effect
(and
(Flag14-)
(Flag14prime-)
)

)
(:action Flag14Action-4
:parameters ()
:precondition
(and
(BELOWOF2-ROBOT)
)
:effect
(and
(Flag14-)
(Flag14prime-)
)

)
(:action Flag14Action-5
:parameters ()
:precondition
(and
(BELOWOF1-ROBOT)
)
:effect
(and
(Flag14-)
(Flag14prime-)
)

)
(:action Flag14Action-6
:parameters ()
:precondition
(and
(BELOWOF4-ROBOT)
)
:effect
(and
(Flag14-)
(Flag14prime-)
)

)
(:action Flag14Action-7
:parameters ()
:precondition
(and
(BELOWOF7-ROBOT)
)
:effect
(and
(Flag14-)
(Flag14prime-)
)

)
(:action Flag14Action-8
:parameters ()
:precondition
(and
(BELOWOF9-ROBOT)
)
:effect
(and
(Flag14-)
(Flag14prime-)
)

)
(:action Flag14Action-9
:parameters ()
:precondition
(and
(BELOWOF3-ROBOT)
)
:effect
(and
(Flag14-)
(Flag14prime-)
)

)
(:action Flag14Action-10
:parameters ()
:precondition
(and
(BELOWOF8-ROBOT)
)
:effect
(and
(Flag14-)
(Flag14prime-)
)

)
(:action Prim14Action
:parameters ()
:precondition
(and
(not (BELOWOF10-ROBOT))
(not (BELOWOF11-ROBOT))
(not (BELOWOF6-ROBOT))
(not (BELOWOF5-ROBOT))
(not (BELOWOF2-ROBOT))
(not (BELOWOF1-ROBOT))
(not (BELOWOF4-ROBOT))
(not (BELOWOF7-ROBOT))
(not (BELOWOF9-ROBOT))
(not (BELOWOF3-ROBOT))
(not (BELOWOF8-ROBOT))
)
:effect
(and
(Flag14prime-)
(not (Flag14-))
)

)
(:action Flag15Action-0
:parameters ()
:precondition
(and
(BELOWOF10-ROBOT)
)
:effect
(and
(Flag15-)
(Flag15prime-)
)

)
(:action Flag15Action-1
:parameters ()
:precondition
(and
(BELOWOF11-ROBOT)
)
:effect
(and
(Flag15-)
(Flag15prime-)
)

)
(:action Flag15Action-2
:parameters ()
:precondition
(and
(BELOWOF6-ROBOT)
)
:effect
(and
(Flag15-)
(Flag15prime-)
)

)
(:action Flag15Action-3
:parameters ()
:precondition
(and
(BELOWOF5-ROBOT)
)
:effect
(and
(Flag15-)
(Flag15prime-)
)

)
(:action Flag15Action-4
:parameters ()
:precondition
(and
(BELOWOF2-ROBOT)
)
:effect
(and
(Flag15-)
(Flag15prime-)
)

)
(:action Flag15Action-5
:parameters ()
:precondition
(and
(BELOWOF1-ROBOT)
)
:effect
(and
(Flag15-)
(Flag15prime-)
)

)
(:action Flag15Action-6
:parameters ()
:precondition
(and
(BELOWOF4-ROBOT)
)
:effect
(and
(Flag15-)
(Flag15prime-)
)

)
(:action Flag15Action-7
:parameters ()
:precondition
(and
(BELOWOF12-ROBOT)
)
:effect
(and
(Flag15-)
(Flag15prime-)
)

)
(:action Flag15Action-8
:parameters ()
:precondition
(and
(BELOWOF7-ROBOT)
)
:effect
(and
(Flag15-)
(Flag15prime-)
)

)
(:action Flag15Action-9
:parameters ()
:precondition
(and
(BELOWOF9-ROBOT)
)
:effect
(and
(Flag15-)
(Flag15prime-)
)

)
(:action Flag15Action-10
:parameters ()
:precondition
(and
(BELOWOF3-ROBOT)
)
:effect
(and
(Flag15-)
(Flag15prime-)
)

)
(:action Flag15Action-11
:parameters ()
:precondition
(and
(BELOWOF8-ROBOT)
)
:effect
(and
(Flag15-)
(Flag15prime-)
)

)
(:action Prim15Action
:parameters ()
:precondition
(and
(not (BELOWOF10-ROBOT))
(not (BELOWOF11-ROBOT))
(not (BELOWOF6-ROBOT))
(not (BELOWOF5-ROBOT))
(not (BELOWOF2-ROBOT))
(not (BELOWOF1-ROBOT))
(not (BELOWOF4-ROBOT))
(not (BELOWOF12-ROBOT))
(not (BELOWOF7-ROBOT))
(not (BELOWOF9-ROBOT))
(not (BELOWOF3-ROBOT))
(not (BELOWOF8-ROBOT))
)
:effect
(and
(Flag15prime-)
(not (Flag15-))
)

)
(:action Flag16Action-0
:parameters ()
:precondition
(and
(BELOWOF10-ROBOT)
)
:effect
(and
(Flag16-)
(Flag16prime-)
)

)
(:action Flag16Action-1
:parameters ()
:precondition
(and
(BELOWOF11-ROBOT)
)
:effect
(and
(Flag16-)
(Flag16prime-)
)

)
(:action Flag16Action-2
:parameters ()
:precondition
(and
(BELOWOF6-ROBOT)
)
:effect
(and
(Flag16-)
(Flag16prime-)
)

)
(:action Flag16Action-3
:parameters ()
:precondition
(and
(BELOWOF5-ROBOT)
)
:effect
(and
(Flag16-)
(Flag16prime-)
)

)
(:action Flag16Action-4
:parameters ()
:precondition
(and
(BELOWOF2-ROBOT)
)
:effect
(and
(Flag16-)
(Flag16prime-)
)

)
(:action Flag16Action-5
:parameters ()
:precondition
(and
(BELOWOF1-ROBOT)
)
:effect
(and
(Flag16-)
(Flag16prime-)
)

)
(:action Flag16Action-6
:parameters ()
:precondition
(and
(BELOWOF4-ROBOT)
)
:effect
(and
(Flag16-)
(Flag16prime-)
)

)
(:action Flag16Action-7
:parameters ()
:precondition
(and
(BELOWOF12-ROBOT)
)
:effect
(and
(Flag16-)
(Flag16prime-)
)

)
(:action Flag16Action-8
:parameters ()
:precondition
(and
(BELOWOF7-ROBOT)
)
:effect
(and
(Flag16-)
(Flag16prime-)
)

)
(:action Flag16Action-9
:parameters ()
:precondition
(and
(BELOWOF9-ROBOT)
)
:effect
(and
(Flag16-)
(Flag16prime-)
)

)
(:action Flag16Action-10
:parameters ()
:precondition
(and
(BELOWOF3-ROBOT)
)
:effect
(and
(Flag16-)
(Flag16prime-)
)

)
(:action Flag16Action-11
:parameters ()
:precondition
(and
(BELOWOF13-ROBOT)
)
:effect
(and
(Flag16-)
(Flag16prime-)
)

)
(:action Flag16Action-12
:parameters ()
:precondition
(and
(BELOWOF8-ROBOT)
)
:effect
(and
(Flag16-)
(Flag16prime-)
)

)
(:action Prim16Action
:parameters ()
:precondition
(and
(not (BELOWOF10-ROBOT))
(not (BELOWOF11-ROBOT))
(not (BELOWOF6-ROBOT))
(not (BELOWOF5-ROBOT))
(not (BELOWOF2-ROBOT))
(not (BELOWOF1-ROBOT))
(not (BELOWOF4-ROBOT))
(not (BELOWOF12-ROBOT))
(not (BELOWOF7-ROBOT))
(not (BELOWOF9-ROBOT))
(not (BELOWOF3-ROBOT))
(not (BELOWOF13-ROBOT))
(not (BELOWOF8-ROBOT))
)
:effect
(and
(Flag16prime-)
(not (Flag16-))
)

)
(:action Flag17Action-0
:parameters ()
:precondition
(and
(BELOWOF10-ROBOT)
)
:effect
(and
(Flag17-)
(Flag17prime-)
)

)
(:action Flag17Action-1
:parameters ()
:precondition
(and
(BELOWOF11-ROBOT)
)
:effect
(and
(Flag17-)
(Flag17prime-)
)

)
(:action Flag17Action-2
:parameters ()
:precondition
(and
(BELOWOF6-ROBOT)
)
:effect
(and
(Flag17-)
(Flag17prime-)
)

)
(:action Flag17Action-3
:parameters ()
:precondition
(and
(BELOWOF5-ROBOT)
)
:effect
(and
(Flag17-)
(Flag17prime-)
)

)
(:action Flag17Action-4
:parameters ()
:precondition
(and
(BELOWOF2-ROBOT)
)
:effect
(and
(Flag17-)
(Flag17prime-)
)

)
(:action Flag17Action-5
:parameters ()
:precondition
(and
(BELOWOF1-ROBOT)
)
:effect
(and
(Flag17-)
(Flag17prime-)
)

)
(:action Flag17Action-6
:parameters ()
:precondition
(and
(BELOWOF4-ROBOT)
)
:effect
(and
(Flag17-)
(Flag17prime-)
)

)
(:action Flag17Action-7
:parameters ()
:precondition
(and
(BELOWOF12-ROBOT)
)
:effect
(and
(Flag17-)
(Flag17prime-)
)

)
(:action Flag17Action-8
:parameters ()
:precondition
(and
(BELOWOF7-ROBOT)
)
:effect
(and
(Flag17-)
(Flag17prime-)
)

)
(:action Flag17Action-9
:parameters ()
:precondition
(and
(BELOWOF9-ROBOT)
)
:effect
(and
(Flag17-)
(Flag17prime-)
)

)
(:action Flag17Action-10
:parameters ()
:precondition
(and
(BELOWOF3-ROBOT)
)
:effect
(and
(Flag17-)
(Flag17prime-)
)

)
(:action Flag17Action-11
:parameters ()
:precondition
(and
(BELOWOF13-ROBOT)
)
:effect
(and
(Flag17-)
(Flag17prime-)
)

)
(:action Flag17Action-12
:parameters ()
:precondition
(and
(BELOWOF8-ROBOT)
)
:effect
(and
(Flag17-)
(Flag17prime-)
)

)
(:action Flag17Action-13
:parameters ()
:precondition
(and
(BELOWOF14-ROBOT)
)
:effect
(and
(Flag17-)
(Flag17prime-)
)

)
(:action Prim17Action
:parameters ()
:precondition
(and
(not (BELOWOF10-ROBOT))
(not (BELOWOF11-ROBOT))
(not (BELOWOF6-ROBOT))
(not (BELOWOF5-ROBOT))
(not (BELOWOF2-ROBOT))
(not (BELOWOF1-ROBOT))
(not (BELOWOF4-ROBOT))
(not (BELOWOF12-ROBOT))
(not (BELOWOF7-ROBOT))
(not (BELOWOF9-ROBOT))
(not (BELOWOF3-ROBOT))
(not (BELOWOF13-ROBOT))
(not (BELOWOF8-ROBOT))
(not (BELOWOF14-ROBOT))
)
:effect
(and
(Flag17prime-)
(not (Flag17-))
)

)
(:action Flag18Action-0
:parameters ()
:precondition
(and
(BELOWOF10-ROBOT)
)
:effect
(and
(Flag18-)
(Flag18prime-)
)

)
(:action Flag18Action-1
:parameters ()
:precondition
(and
(BELOWOF11-ROBOT)
)
:effect
(and
(Flag18-)
(Flag18prime-)
)

)
(:action Flag18Action-2
:parameters ()
:precondition
(and
(BELOWOF15-ROBOT)
)
:effect
(and
(Flag18-)
(Flag18prime-)
)

)
(:action Flag18Action-3
:parameters ()
:precondition
(and
(BELOWOF6-ROBOT)
)
:effect
(and
(Flag18-)
(Flag18prime-)
)

)
(:action Flag18Action-4
:parameters ()
:precondition
(and
(BELOWOF5-ROBOT)
)
:effect
(and
(Flag18-)
(Flag18prime-)
)

)
(:action Flag18Action-5
:parameters ()
:precondition
(and
(BELOWOF2-ROBOT)
)
:effect
(and
(Flag18-)
(Flag18prime-)
)

)
(:action Flag18Action-6
:parameters ()
:precondition
(and
(BELOWOF1-ROBOT)
)
:effect
(and
(Flag18-)
(Flag18prime-)
)

)
(:action Flag18Action-7
:parameters ()
:precondition
(and
(BELOWOF4-ROBOT)
)
:effect
(and
(Flag18-)
(Flag18prime-)
)

)
(:action Flag18Action-8
:parameters ()
:precondition
(and
(BELOWOF12-ROBOT)
)
:effect
(and
(Flag18-)
(Flag18prime-)
)

)
(:action Flag18Action-9
:parameters ()
:precondition
(and
(BELOWOF7-ROBOT)
)
:effect
(and
(Flag18-)
(Flag18prime-)
)

)
(:action Flag18Action-10
:parameters ()
:precondition
(and
(BELOWOF9-ROBOT)
)
:effect
(and
(Flag18-)
(Flag18prime-)
)

)
(:action Flag18Action-11
:parameters ()
:precondition
(and
(BELOWOF3-ROBOT)
)
:effect
(and
(Flag18-)
(Flag18prime-)
)

)
(:action Flag18Action-12
:parameters ()
:precondition
(and
(BELOWOF13-ROBOT)
)
:effect
(and
(Flag18-)
(Flag18prime-)
)

)
(:action Flag18Action-13
:parameters ()
:precondition
(and
(BELOWOF8-ROBOT)
)
:effect
(and
(Flag18-)
(Flag18prime-)
)

)
(:action Flag18Action-14
:parameters ()
:precondition
(and
(BELOWOF14-ROBOT)
)
:effect
(and
(Flag18-)
(Flag18prime-)
)

)
(:action Prim18Action
:parameters ()
:precondition
(and
(not (BELOWOF10-ROBOT))
(not (BELOWOF11-ROBOT))
(not (BELOWOF15-ROBOT))
(not (BELOWOF6-ROBOT))
(not (BELOWOF5-ROBOT))
(not (BELOWOF2-ROBOT))
(not (BELOWOF1-ROBOT))
(not (BELOWOF4-ROBOT))
(not (BELOWOF12-ROBOT))
(not (BELOWOF7-ROBOT))
(not (BELOWOF9-ROBOT))
(not (BELOWOF3-ROBOT))
(not (BELOWOF13-ROBOT))
(not (BELOWOF8-ROBOT))
(not (BELOWOF14-ROBOT))
)
:effect
(and
(Flag18prime-)
(not (Flag18-))
)

)
(:action Flag19Action-0
:parameters ()
:precondition
(and
(BELOWOF10-ROBOT)
)
:effect
(and
(Flag19-)
(Flag19prime-)
)

)
(:action Flag19Action-1
:parameters ()
:precondition
(and
(BELOWOF16-ROBOT)
)
:effect
(and
(Flag19-)
(Flag19prime-)
)

)
(:action Flag19Action-2
:parameters ()
:precondition
(and
(BELOWOF15-ROBOT)
)
:effect
(and
(Flag19-)
(Flag19prime-)
)

)
(:action Flag19Action-3
:parameters ()
:precondition
(and
(BELOWOF6-ROBOT)
)
:effect
(and
(Flag19-)
(Flag19prime-)
)

)
(:action Flag19Action-4
:parameters ()
:precondition
(and
(BELOWOF11-ROBOT)
)
:effect
(and
(Flag19-)
(Flag19prime-)
)

)
(:action Flag19Action-5
:parameters ()
:precondition
(and
(BELOWOF5-ROBOT)
)
:effect
(and
(Flag19-)
(Flag19prime-)
)

)
(:action Flag19Action-6
:parameters ()
:precondition
(and
(BELOWOF2-ROBOT)
)
:effect
(and
(Flag19-)
(Flag19prime-)
)

)
(:action Flag19Action-7
:parameters ()
:precondition
(and
(BELOWOF1-ROBOT)
)
:effect
(and
(Flag19-)
(Flag19prime-)
)

)
(:action Flag19Action-8
:parameters ()
:precondition
(and
(BELOWOF4-ROBOT)
)
:effect
(and
(Flag19-)
(Flag19prime-)
)

)
(:action Flag19Action-9
:parameters ()
:precondition
(and
(BELOWOF12-ROBOT)
)
:effect
(and
(Flag19-)
(Flag19prime-)
)

)
(:action Flag19Action-10
:parameters ()
:precondition
(and
(BELOWOF7-ROBOT)
)
:effect
(and
(Flag19-)
(Flag19prime-)
)

)
(:action Flag19Action-11
:parameters ()
:precondition
(and
(BELOWOF9-ROBOT)
)
:effect
(and
(Flag19-)
(Flag19prime-)
)

)
(:action Flag19Action-12
:parameters ()
:precondition
(and
(BELOWOF3-ROBOT)
)
:effect
(and
(Flag19-)
(Flag19prime-)
)

)
(:action Flag19Action-13
:parameters ()
:precondition
(and
(BELOWOF13-ROBOT)
)
:effect
(and
(Flag19-)
(Flag19prime-)
)

)
(:action Flag19Action-14
:parameters ()
:precondition
(and
(BELOWOF8-ROBOT)
)
:effect
(and
(Flag19-)
(Flag19prime-)
)

)
(:action Flag19Action-15
:parameters ()
:precondition
(and
(BELOWOF14-ROBOT)
)
:effect
(and
(Flag19-)
(Flag19prime-)
)

)
(:action Prim19Action
:parameters ()
:precondition
(and
(not (BELOWOF10-ROBOT))
(not (BELOWOF16-ROBOT))
(not (BELOWOF15-ROBOT))
(not (BELOWOF6-ROBOT))
(not (BELOWOF11-ROBOT))
(not (BELOWOF5-ROBOT))
(not (BELOWOF2-ROBOT))
(not (BELOWOF1-ROBOT))
(not (BELOWOF4-ROBOT))
(not (BELOWOF12-ROBOT))
(not (BELOWOF7-ROBOT))
(not (BELOWOF9-ROBOT))
(not (BELOWOF3-ROBOT))
(not (BELOWOF13-ROBOT))
(not (BELOWOF8-ROBOT))
(not (BELOWOF14-ROBOT))
)
:effect
(and
(Flag19prime-)
(not (Flag19-))
)

)
(:action Flag20Action-0
:parameters ()
:precondition
(and
(BELOWOF10-ROBOT)
)
:effect
(and
(Flag20-)
(Flag20prime-)
)

)
(:action Flag20Action-1
:parameters ()
:precondition
(and
(BELOWOF16-ROBOT)
)
:effect
(and
(Flag20-)
(Flag20prime-)
)

)
(:action Flag20Action-2
:parameters ()
:precondition
(and
(BELOWOF15-ROBOT)
)
:effect
(and
(Flag20-)
(Flag20prime-)
)

)
(:action Flag20Action-3
:parameters ()
:precondition
(and
(BELOWOF6-ROBOT)
)
:effect
(and
(Flag20-)
(Flag20prime-)
)

)
(:action Flag20Action-5
:parameters ()
:precondition
(and
(BELOWOF5-ROBOT)
)
:effect
(and
(Flag20-)
(Flag20prime-)
)

)
(:action Flag20Action-6
:parameters ()
:precondition
(and
(BELOWOF2-ROBOT)
)
:effect
(and
(Flag20-)
(Flag20prime-)
)

)
(:action Flag20Action-7
:parameters ()
:precondition
(and
(BELOWOF1-ROBOT)
)
:effect
(and
(Flag20-)
(Flag20prime-)
)

)
(:action Flag20Action-8
:parameters ()
:precondition
(and
(BELOWOF4-ROBOT)
)
:effect
(and
(Flag20-)
(Flag20prime-)
)

)
(:action Flag20Action-9
:parameters ()
:precondition
(and
(BELOWOF11-ROBOT)
)
:effect
(and
(Flag20-)
(Flag20prime-)
)

)
(:action Flag20Action-10
:parameters ()
:precondition
(and
(BELOWOF12-ROBOT)
)
:effect
(and
(Flag20-)
(Flag20prime-)
)

)
(:action Flag20Action-11
:parameters ()
:precondition
(and
(BELOWOF7-ROBOT)
)
:effect
(and
(Flag20-)
(Flag20prime-)
)

)
(:action Flag20Action-12
:parameters ()
:precondition
(and
(BELOWOF9-ROBOT)
)
:effect
(and
(Flag20-)
(Flag20prime-)
)

)
(:action Flag20Action-13
:parameters ()
:precondition
(and
(BELOWOF3-ROBOT)
)
:effect
(and
(Flag20-)
(Flag20prime-)
)

)
(:action Flag20Action-14
:parameters ()
:precondition
(and
(BELOWOF13-ROBOT)
)
:effect
(and
(Flag20-)
(Flag20prime-)
)

)
(:action Flag20Action-15
:parameters ()
:precondition
(and
(BELOWOF8-ROBOT)
)
:effect
(and
(Flag20-)
(Flag20prime-)
)

)
(:action Flag20Action-16
:parameters ()
:precondition
(and
(BELOWOF14-ROBOT)
)
:effect
(and
(Flag20-)
(Flag20prime-)
)

)
(:action Prim20Action
:parameters ()
:precondition
(and
(not (BELOWOF10-ROBOT))
(not (BELOWOF16-ROBOT))
(not (BELOWOF15-ROBOT))
(not (BELOWOF6-ROBOT))
(not (BELOWOF17-ROBOT))
(not (BELOWOF5-ROBOT))
(not (BELOWOF2-ROBOT))
(not (BELOWOF1-ROBOT))
(not (BELOWOF4-ROBOT))
(not (BELOWOF11-ROBOT))
(not (BELOWOF12-ROBOT))
(not (BELOWOF7-ROBOT))
(not (BELOWOF9-ROBOT))
(not (BELOWOF3-ROBOT))
(not (BELOWOF13-ROBOT))
(not (BELOWOF8-ROBOT))
(not (BELOWOF14-ROBOT))
)
:effect
(and
(Flag20prime-)
(not (Flag20-))
)

)
(:action Flag21Action-0
:parameters ()
:precondition
(and
(BELOWOF10-ROBOT)
)
:effect
(and
(Flag21-)
(Flag21prime-)
)

)
(:action Flag21Action-1
:parameters ()
:precondition
(and
(BELOWOF16-ROBOT)
)
:effect
(and
(Flag21-)
(Flag21prime-)
)

)
(:action Flag21Action-2
:parameters ()
:precondition
(and
(BELOWOF15-ROBOT)
)
:effect
(and
(Flag21-)
(Flag21prime-)
)

)
(:action Flag21Action-3
:parameters ()
:precondition
(and
(BELOWOF6-ROBOT)
)
:effect
(and
(Flag21-)
(Flag21prime-)
)

)
(:action Flag21Action-5
:parameters ()
:precondition
(and
(BELOWOF5-ROBOT)
)
:effect
(and
(Flag21-)
(Flag21prime-)
)

)
(:action Flag21Action-6
:parameters ()
:precondition
(and
(BELOWOF2-ROBOT)
)
:effect
(and
(Flag21-)
(Flag21prime-)
)

)
(:action Flag21Action-7
:parameters ()
:precondition
(and
(BELOWOF1-ROBOT)
)
:effect
(and
(Flag21-)
(Flag21prime-)
)

)
(:action Flag21Action-8
:parameters ()
:precondition
(and
(BELOWOF4-ROBOT)
)
:effect
(and
(Flag21-)
(Flag21prime-)
)

)
(:action Flag21Action-9
:parameters ()
:precondition
(and
(BELOWOF11-ROBOT)
)
:effect
(and
(Flag21-)
(Flag21prime-)
)

)
(:action Flag21Action-10
:parameters ()
:precondition
(and
(BELOWOF18-ROBOT)
)
:effect
(and
(Flag21-)
(Flag21prime-)
)

)
(:action Flag21Action-11
:parameters ()
:precondition
(and
(BELOWOF12-ROBOT)
)
:effect
(and
(Flag21-)
(Flag21prime-)
)

)
(:action Flag21Action-12
:parameters ()
:precondition
(and
(BELOWOF7-ROBOT)
)
:effect
(and
(Flag21-)
(Flag21prime-)
)

)
(:action Flag21Action-13
:parameters ()
:precondition
(and
(BELOWOF9-ROBOT)
)
:effect
(and
(Flag21-)
(Flag21prime-)
)

)
(:action Flag21Action-14
:parameters ()
:precondition
(and
(BELOWOF3-ROBOT)
)
:effect
(and
(Flag21-)
(Flag21prime-)
)

)
(:action Flag21Action-15
:parameters ()
:precondition
(and
(BELOWOF13-ROBOT)
)
:effect
(and
(Flag21-)
(Flag21prime-)
)

)
(:action Flag21Action-16
:parameters ()
:precondition
(and
(BELOWOF8-ROBOT)
)
:effect
(and
(Flag21-)
(Flag21prime-)
)

)
(:action Flag21Action-17
:parameters ()
:precondition
(and
(BELOWOF14-ROBOT)
)
:effect
(and
(Flag21-)
(Flag21prime-)
)

)
(:action Prim21Action
:parameters ()
:precondition
(and
(not (BELOWOF10-ROBOT))
(not (BELOWOF16-ROBOT))
(not (BELOWOF15-ROBOT))
(not (BELOWOF6-ROBOT))
(not (BELOWOF17-ROBOT))
(not (BELOWOF5-ROBOT))
(not (BELOWOF2-ROBOT))
(not (BELOWOF1-ROBOT))
(not (BELOWOF4-ROBOT))
(not (BELOWOF11-ROBOT))
(not (BELOWOF18-ROBOT))
(not (BELOWOF12-ROBOT))
(not (BELOWOF7-ROBOT))
(not (BELOWOF9-ROBOT))
(not (BELOWOF3-ROBOT))
(not (BELOWOF13-ROBOT))
(not (BELOWOF8-ROBOT))
(not (BELOWOF14-ROBOT))
)
:effect
(and
(Flag21prime-)
(not (Flag21-))
)

)
(:action Flag22Action-0
:parameters ()
:precondition
(and
(ABOVEOF6-ROBOT)
)
:effect
(and
(Flag22-)
(Flag22prime-)
)

)
(:action Flag22Action-1
:parameters ()
:precondition
(and
(ABOVEOF11-ROBOT)
)
:effect
(and
(Flag22-)
(Flag22prime-)
)

)
(:action Flag22Action-2
:parameters ()
:precondition
(and
(ABOVEOF10-ROBOT)
)
:effect
(and
(Flag22-)
(Flag22prime-)
)

)
(:action Flag22Action-3
:parameters ()
:precondition
(and
(ABOVEOF2-ROBOT)
)
:effect
(and
(Flag22-)
(Flag22prime-)
)

)
(:action Flag22Action-4
:parameters ()
:precondition
(and
(ABOVEOF7-ROBOT)
)
:effect
(and
(Flag22-)
(Flag22prime-)
)

)
(:action Flag22Action-5
:parameters ()
:precondition
(and
(ABOVEOF12-ROBOT)
)
:effect
(and
(Flag22-)
(Flag22prime-)
)

)
(:action Flag22Action-6
:parameters ()
:precondition
(and
(ABOVEOF17-ROBOT)
)
:effect
(and
(Flag22-)
(Flag22prime-)
)

)
(:action Flag22Action-7
:parameters ()
:precondition
(and
(ABOVEOF15-ROBOT)
)
:effect
(and
(Flag22-)
(Flag22prime-)
)

)
(:action Flag22Action-8
:parameters ()
:precondition
(and
(ABOVEOF8-ROBOT)
)
:effect
(and
(Flag22-)
(Flag22prime-)
)

)
(:action Flag22Action-9
:parameters ()
:precondition
(and
(ABOVEOF9-ROBOT)
)
:effect
(and
(Flag22-)
(Flag22prime-)
)

)
(:action Flag22Action-10
:parameters ()
:precondition
(and
(ABOVEOF16-ROBOT)
)
:effect
(and
(Flag22-)
(Flag22prime-)
)

)
(:action Flag22Action-11
:parameters ()
:precondition
(and
(ABOVEOF4-ROBOT)
)
:effect
(and
(Flag22-)
(Flag22prime-)
)

)
(:action Flag22Action-12
:parameters ()
:precondition
(and
(ABOVEOF3-ROBOT)
)
:effect
(and
(Flag22-)
(Flag22prime-)
)

)
(:action Flag22Action-13
:parameters ()
:precondition
(and
(ABOVEOF13-ROBOT)
)
:effect
(and
(Flag22-)
(Flag22prime-)
)

)
(:action Flag22Action-14
:parameters ()
:precondition
(and
(ABOVEOF14-ROBOT)
)
:effect
(and
(Flag22-)
(Flag22prime-)
)

)
(:action Flag22Action-15
:parameters ()
:precondition
(and
(ABOVEOF5-ROBOT)
)
:effect
(and
(Flag22-)
(Flag22prime-)
)

)
(:action Flag22Action-16
:parameters ()
:precondition
(and
(ABOVEOF1-ROBOT)
)
:effect
(and
(Flag22-)
(Flag22prime-)
)

)
(:action Prim22Action
:parameters ()
:precondition
(and
(not (ABOVEOF6-ROBOT))
(not (ABOVEOF11-ROBOT))
(not (ABOVEOF10-ROBOT))
(not (ABOVEOF2-ROBOT))
(not (ABOVEOF7-ROBOT))
(not (ABOVEOF12-ROBOT))
(not (ABOVEOF17-ROBOT))
(not (ABOVEOF15-ROBOT))
(not (ABOVEOF8-ROBOT))
(not (ABOVEOF9-ROBOT))
(not (ABOVEOF16-ROBOT))
(not (ABOVEOF4-ROBOT))
(not (ABOVEOF3-ROBOT))
(not (ABOVEOF13-ROBOT))
(not (ABOVEOF14-ROBOT))
(not (ABOVEOF5-ROBOT))
(not (ABOVEOF1-ROBOT))
)
:effect
(and
(Flag22prime-)
(not (Flag22-))
)

)
(:action Flag23Action-0
:parameters ()
:precondition
(and
(ABOVEOF11-ROBOT)
)
:effect
(and
(Flag23-)
(Flag23prime-)
)

)
(:action Flag23Action-1
:parameters ()
:precondition
(and
(ABOVEOF6-ROBOT)
)
:effect
(and
(Flag23-)
(Flag23prime-)
)

)
(:action Flag23Action-2
:parameters ()
:precondition
(and
(ABOVEOF10-ROBOT)
)
:effect
(and
(Flag23-)
(Flag23prime-)
)

)
(:action Flag23Action-3
:parameters ()
:precondition
(and
(ABOVEOF2-ROBOT)
)
:effect
(and
(Flag23-)
(Flag23prime-)
)

)
(:action Flag23Action-4
:parameters ()
:precondition
(and
(ABOVEOF7-ROBOT)
)
:effect
(and
(Flag23-)
(Flag23prime-)
)

)
(:action Flag23Action-5
:parameters ()
:precondition
(and
(ABOVEOF12-ROBOT)
)
:effect
(and
(Flag23-)
(Flag23prime-)
)

)
(:action Flag23Action-6
:parameters ()
:precondition
(and
(ABOVEOF17-ROBOT)
)
:effect
(and
(Flag23-)
(Flag23prime-)
)

)
(:action Flag23Action-7
:parameters ()
:precondition
(and
(ABOVEOF15-ROBOT)
)
:effect
(and
(Flag23-)
(Flag23prime-)
)

)
(:action Flag23Action-8
:parameters ()
:precondition
(and
(ABOVEOF8-ROBOT)
)
:effect
(and
(Flag23-)
(Flag23prime-)
)

)
(:action Flag23Action-9
:parameters ()
:precondition
(and
(ABOVEOF9-ROBOT)
)
:effect
(and
(Flag23-)
(Flag23prime-)
)

)
(:action Flag23Action-10
:parameters ()
:precondition
(and
(ABOVEOF16-ROBOT)
)
:effect
(and
(Flag23-)
(Flag23prime-)
)

)
(:action Flag23Action-11
:parameters ()
:precondition
(and
(ABOVEOF4-ROBOT)
)
:effect
(and
(Flag23-)
(Flag23prime-)
)

)
(:action Flag23Action-12
:parameters ()
:precondition
(and
(ABOVEOF3-ROBOT)
)
:effect
(and
(Flag23-)
(Flag23prime-)
)

)
(:action Flag23Action-13
:parameters ()
:precondition
(and
(ABOVEOF13-ROBOT)
)
:effect
(and
(Flag23-)
(Flag23prime-)
)

)
(:action Flag23Action-14
:parameters ()
:precondition
(and
(ABOVEOF14-ROBOT)
)
:effect
(and
(Flag23-)
(Flag23prime-)
)

)
(:action Flag23Action-15
:parameters ()
:precondition
(and
(ABOVEOF5-ROBOT)
)
:effect
(and
(Flag23-)
(Flag23prime-)
)

)
(:action Prim23Action
:parameters ()
:precondition
(and
(not (ABOVEOF11-ROBOT))
(not (ABOVEOF6-ROBOT))
(not (ABOVEOF10-ROBOT))
(not (ABOVEOF2-ROBOT))
(not (ABOVEOF7-ROBOT))
(not (ABOVEOF12-ROBOT))
(not (ABOVEOF17-ROBOT))
(not (ABOVEOF15-ROBOT))
(not (ABOVEOF8-ROBOT))
(not (ABOVEOF9-ROBOT))
(not (ABOVEOF16-ROBOT))
(not (ABOVEOF4-ROBOT))
(not (ABOVEOF3-ROBOT))
(not (ABOVEOF13-ROBOT))
(not (ABOVEOF14-ROBOT))
(not (ABOVEOF5-ROBOT))
)
:effect
(and
(Flag23prime-)
(not (Flag23-))
)

)
(:action Flag24Action-0
:parameters ()
:precondition
(and
(ABOVEOF11-ROBOT)
)
:effect
(and
(Flag24-)
(Flag24prime-)
)

)
(:action Flag24Action-1
:parameters ()
:precondition
(and
(ABOVEOF6-ROBOT)
)
:effect
(and
(Flag24-)
(Flag24prime-)
)

)
(:action Flag24Action-2
:parameters ()
:precondition
(and
(ABOVEOF10-ROBOT)
)
:effect
(and
(Flag24-)
(Flag24prime-)
)

)
(:action Flag24Action-3
:parameters ()
:precondition
(and
(ABOVEOF8-ROBOT)
)
:effect
(and
(Flag24-)
(Flag24prime-)
)

)
(:action Flag24Action-4
:parameters ()
:precondition
(and
(ABOVEOF7-ROBOT)
)
:effect
(and
(Flag24-)
(Flag24prime-)
)

)
(:action Flag24Action-5
:parameters ()
:precondition
(and
(ABOVEOF12-ROBOT)
)
:effect
(and
(Flag24-)
(Flag24prime-)
)

)
(:action Flag24Action-6
:parameters ()
:precondition
(and
(ABOVEOF17-ROBOT)
)
:effect
(and
(Flag24-)
(Flag24prime-)
)

)
(:action Flag24Action-7
:parameters ()
:precondition
(and
(ABOVEOF15-ROBOT)
)
:effect
(and
(Flag24-)
(Flag24prime-)
)

)
(:action Flag24Action-8
:parameters ()
:precondition
(and
(ABOVEOF9-ROBOT)
)
:effect
(and
(Flag24-)
(Flag24prime-)
)

)
(:action Flag24Action-9
:parameters ()
:precondition
(and
(ABOVEOF16-ROBOT)
)
:effect
(and
(Flag24-)
(Flag24prime-)
)

)
(:action Flag24Action-10
:parameters ()
:precondition
(and
(ABOVEOF4-ROBOT)
)
:effect
(and
(Flag24-)
(Flag24prime-)
)

)
(:action Flag24Action-11
:parameters ()
:precondition
(and
(ABOVEOF3-ROBOT)
)
:effect
(and
(Flag24-)
(Flag24prime-)
)

)
(:action Flag24Action-12
:parameters ()
:precondition
(and
(ABOVEOF13-ROBOT)
)
:effect
(and
(Flag24-)
(Flag24prime-)
)

)
(:action Flag24Action-13
:parameters ()
:precondition
(and
(ABOVEOF14-ROBOT)
)
:effect
(and
(Flag24-)
(Flag24prime-)
)

)
(:action Flag24Action-14
:parameters ()
:precondition
(and
(ABOVEOF5-ROBOT)
)
:effect
(and
(Flag24-)
(Flag24prime-)
)

)
(:action Prim24Action
:parameters ()
:precondition
(and
(not (ABOVEOF11-ROBOT))
(not (ABOVEOF6-ROBOT))
(not (ABOVEOF10-ROBOT))
(not (ABOVEOF8-ROBOT))
(not (ABOVEOF7-ROBOT))
(not (ABOVEOF12-ROBOT))
(not (ABOVEOF17-ROBOT))
(not (ABOVEOF15-ROBOT))
(not (ABOVEOF9-ROBOT))
(not (ABOVEOF16-ROBOT))
(not (ABOVEOF4-ROBOT))
(not (ABOVEOF3-ROBOT))
(not (ABOVEOF13-ROBOT))
(not (ABOVEOF14-ROBOT))
(not (ABOVEOF5-ROBOT))
)
:effect
(and
(Flag24prime-)
(not (Flag24-))
)

)
(:action Flag25Action-0
:parameters ()
:precondition
(and
(ABOVEOF11-ROBOT)
)
:effect
(and
(Flag25-)
(Flag25prime-)
)

)
(:action Flag25Action-1
:parameters ()
:precondition
(and
(ABOVEOF6-ROBOT)
)
:effect
(and
(Flag25-)
(Flag25prime-)
)

)
(:action Flag25Action-2
:parameters ()
:precondition
(and
(ABOVEOF10-ROBOT)
)
:effect
(and
(Flag25-)
(Flag25prime-)
)

)
(:action Flag25Action-3
:parameters ()
:precondition
(and
(ABOVEOF8-ROBOT)
)
:effect
(and
(Flag25-)
(Flag25prime-)
)

)
(:action Flag25Action-4
:parameters ()
:precondition
(and
(ABOVEOF7-ROBOT)
)
:effect
(and
(Flag25-)
(Flag25prime-)
)

)
(:action Flag25Action-5
:parameters ()
:precondition
(and
(ABOVEOF12-ROBOT)
)
:effect
(and
(Flag25-)
(Flag25prime-)
)

)
(:action Flag25Action-6
:parameters ()
:precondition
(and
(ABOVEOF17-ROBOT)
)
:effect
(and
(Flag25-)
(Flag25prime-)
)

)
(:action Flag25Action-7
:parameters ()
:precondition
(and
(ABOVEOF15-ROBOT)
)
:effect
(and
(Flag25-)
(Flag25prime-)
)

)
(:action Flag25Action-8
:parameters ()
:precondition
(and
(ABOVEOF9-ROBOT)
)
:effect
(and
(Flag25-)
(Flag25prime-)
)

)
(:action Flag25Action-9
:parameters ()
:precondition
(and
(ABOVEOF16-ROBOT)
)
:effect
(and
(Flag25-)
(Flag25prime-)
)

)
(:action Flag25Action-10
:parameters ()
:precondition
(and
(ABOVEOF4-ROBOT)
)
:effect
(and
(Flag25-)
(Flag25prime-)
)

)
(:action Flag25Action-11
:parameters ()
:precondition
(and
(ABOVEOF13-ROBOT)
)
:effect
(and
(Flag25-)
(Flag25prime-)
)

)
(:action Flag25Action-12
:parameters ()
:precondition
(and
(ABOVEOF14-ROBOT)
)
:effect
(and
(Flag25-)
(Flag25prime-)
)

)
(:action Flag25Action-13
:parameters ()
:precondition
(and
(ABOVEOF5-ROBOT)
)
:effect
(and
(Flag25-)
(Flag25prime-)
)

)
(:action Prim25Action
:parameters ()
:precondition
(and
(not (ABOVEOF11-ROBOT))
(not (ABOVEOF6-ROBOT))
(not (ABOVEOF10-ROBOT))
(not (ABOVEOF8-ROBOT))
(not (ABOVEOF7-ROBOT))
(not (ABOVEOF12-ROBOT))
(not (ABOVEOF17-ROBOT))
(not (ABOVEOF15-ROBOT))
(not (ABOVEOF9-ROBOT))
(not (ABOVEOF16-ROBOT))
(not (ABOVEOF4-ROBOT))
(not (ABOVEOF13-ROBOT))
(not (ABOVEOF14-ROBOT))
(not (ABOVEOF5-ROBOT))
)
:effect
(and
(Flag25prime-)
(not (Flag25-))
)

)
(:action Flag26Action-0
:parameters ()
:precondition
(and
(ABOVEOF11-ROBOT)
)
:effect
(and
(Flag26-)
(Flag26prime-)
)

)
(:action Flag26Action-1
:parameters ()
:precondition
(and
(ABOVEOF6-ROBOT)
)
:effect
(and
(Flag26-)
(Flag26prime-)
)

)
(:action Flag26Action-2
:parameters ()
:precondition
(and
(ABOVEOF7-ROBOT)
)
:effect
(and
(Flag26-)
(Flag26prime-)
)

)
(:action Flag26Action-3
:parameters ()
:precondition
(and
(ABOVEOF8-ROBOT)
)
:effect
(and
(Flag26-)
(Flag26prime-)
)

)
(:action Flag26Action-4
:parameters ()
:precondition
(and
(ABOVEOF10-ROBOT)
)
:effect
(and
(Flag26-)
(Flag26prime-)
)

)
(:action Flag26Action-5
:parameters ()
:precondition
(and
(ABOVEOF12-ROBOT)
)
:effect
(and
(Flag26-)
(Flag26prime-)
)

)
(:action Flag26Action-6
:parameters ()
:precondition
(and
(ABOVEOF17-ROBOT)
)
:effect
(and
(Flag26-)
(Flag26prime-)
)

)
(:action Flag26Action-7
:parameters ()
:precondition
(and
(ABOVEOF15-ROBOT)
)
:effect
(and
(Flag26-)
(Flag26prime-)
)

)
(:action Flag26Action-8
:parameters ()
:precondition
(and
(ABOVEOF9-ROBOT)
)
:effect
(and
(Flag26-)
(Flag26prime-)
)

)
(:action Flag26Action-9
:parameters ()
:precondition
(and
(ABOVEOF16-ROBOT)
)
:effect
(and
(Flag26-)
(Flag26prime-)
)

)
(:action Flag26Action-10
:parameters ()
:precondition
(and
(ABOVEOF13-ROBOT)
)
:effect
(and
(Flag26-)
(Flag26prime-)
)

)
(:action Flag26Action-11
:parameters ()
:precondition
(and
(ABOVEOF14-ROBOT)
)
:effect
(and
(Flag26-)
(Flag26prime-)
)

)
(:action Flag26Action-12
:parameters ()
:precondition
(and
(ABOVEOF5-ROBOT)
)
:effect
(and
(Flag26-)
(Flag26prime-)
)

)
(:action Prim26Action
:parameters ()
:precondition
(and
(not (ABOVEOF11-ROBOT))
(not (ABOVEOF6-ROBOT))
(not (ABOVEOF7-ROBOT))
(not (ABOVEOF8-ROBOT))
(not (ABOVEOF10-ROBOT))
(not (ABOVEOF12-ROBOT))
(not (ABOVEOF17-ROBOT))
(not (ABOVEOF15-ROBOT))
(not (ABOVEOF9-ROBOT))
(not (ABOVEOF16-ROBOT))
(not (ABOVEOF13-ROBOT))
(not (ABOVEOF14-ROBOT))
(not (ABOVEOF5-ROBOT))
)
:effect
(and
(Flag26prime-)
(not (Flag26-))
)

)
(:action Flag27Action-0
:parameters ()
:precondition
(and
(ABOVEOF11-ROBOT)
)
:effect
(and
(Flag27-)
(Flag27prime-)
)

)
(:action Flag27Action-1
:parameters ()
:precondition
(and
(ABOVEOF6-ROBOT)
)
:effect
(and
(Flag27-)
(Flag27prime-)
)

)
(:action Flag27Action-2
:parameters ()
:precondition
(and
(ABOVEOF7-ROBOT)
)
:effect
(and
(Flag27-)
(Flag27prime-)
)

)
(:action Flag27Action-3
:parameters ()
:precondition
(and
(ABOVEOF8-ROBOT)
)
:effect
(and
(Flag27-)
(Flag27prime-)
)

)
(:action Flag27Action-4
:parameters ()
:precondition
(and
(ABOVEOF10-ROBOT)
)
:effect
(and
(Flag27-)
(Flag27prime-)
)

)
(:action Flag27Action-5
:parameters ()
:precondition
(and
(ABOVEOF12-ROBOT)
)
:effect
(and
(Flag27-)
(Flag27prime-)
)

)
(:action Flag27Action-6
:parameters ()
:precondition
(and
(ABOVEOF17-ROBOT)
)
:effect
(and
(Flag27-)
(Flag27prime-)
)

)
(:action Flag27Action-7
:parameters ()
:precondition
(and
(ABOVEOF15-ROBOT)
)
:effect
(and
(Flag27-)
(Flag27prime-)
)

)
(:action Flag27Action-8
:parameters ()
:precondition
(and
(ABOVEOF9-ROBOT)
)
:effect
(and
(Flag27-)
(Flag27prime-)
)

)
(:action Flag27Action-9
:parameters ()
:precondition
(and
(ABOVEOF16-ROBOT)
)
:effect
(and
(Flag27-)
(Flag27prime-)
)

)
(:action Flag27Action-10
:parameters ()
:precondition
(and
(ABOVEOF13-ROBOT)
)
:effect
(and
(Flag27-)
(Flag27prime-)
)

)
(:action Flag27Action-11
:parameters ()
:precondition
(and
(ABOVEOF14-ROBOT)
)
:effect
(and
(Flag27-)
(Flag27prime-)
)

)
(:action Prim27Action
:parameters ()
:precondition
(and
(not (ABOVEOF11-ROBOT))
(not (ABOVEOF6-ROBOT))
(not (ABOVEOF7-ROBOT))
(not (ABOVEOF8-ROBOT))
(not (ABOVEOF10-ROBOT))
(not (ABOVEOF12-ROBOT))
(not (ABOVEOF17-ROBOT))
(not (ABOVEOF15-ROBOT))
(not (ABOVEOF9-ROBOT))
(not (ABOVEOF16-ROBOT))
(not (ABOVEOF13-ROBOT))
(not (ABOVEOF14-ROBOT))
)
:effect
(and
(Flag27prime-)
(not (Flag27-))
)

)
(:action Flag28Action-0
:parameters ()
:precondition
(and
(ABOVEOF11-ROBOT)
)
:effect
(and
(Flag28-)
(Flag28prime-)
)

)
(:action Flag28Action-1
:parameters ()
:precondition
(and
(ABOVEOF7-ROBOT)
)
:effect
(and
(Flag28-)
(Flag28prime-)
)

)
(:action Flag28Action-2
:parameters ()
:precondition
(and
(ABOVEOF8-ROBOT)
)
:effect
(and
(Flag28-)
(Flag28prime-)
)

)
(:action Flag28Action-3
:parameters ()
:precondition
(and
(ABOVEOF10-ROBOT)
)
:effect
(and
(Flag28-)
(Flag28prime-)
)

)
(:action Flag28Action-4
:parameters ()
:precondition
(and
(ABOVEOF12-ROBOT)
)
:effect
(and
(Flag28-)
(Flag28prime-)
)

)
(:action Flag28Action-5
:parameters ()
:precondition
(and
(ABOVEOF17-ROBOT)
)
:effect
(and
(Flag28-)
(Flag28prime-)
)

)
(:action Flag28Action-6
:parameters ()
:precondition
(and
(ABOVEOF15-ROBOT)
)
:effect
(and
(Flag28-)
(Flag28prime-)
)

)
(:action Flag28Action-7
:parameters ()
:precondition
(and
(ABOVEOF9-ROBOT)
)
:effect
(and
(Flag28-)
(Flag28prime-)
)

)
(:action Flag28Action-8
:parameters ()
:precondition
(and
(ABOVEOF16-ROBOT)
)
:effect
(and
(Flag28-)
(Flag28prime-)
)

)
(:action Flag28Action-9
:parameters ()
:precondition
(and
(ABOVEOF13-ROBOT)
)
:effect
(and
(Flag28-)
(Flag28prime-)
)

)
(:action Flag28Action-10
:parameters ()
:precondition
(and
(ABOVEOF14-ROBOT)
)
:effect
(and
(Flag28-)
(Flag28prime-)
)

)
(:action Prim28Action
:parameters ()
:precondition
(and
(not (ABOVEOF11-ROBOT))
(not (ABOVEOF7-ROBOT))
(not (ABOVEOF8-ROBOT))
(not (ABOVEOF10-ROBOT))
(not (ABOVEOF12-ROBOT))
(not (ABOVEOF17-ROBOT))
(not (ABOVEOF15-ROBOT))
(not (ABOVEOF9-ROBOT))
(not (ABOVEOF16-ROBOT))
(not (ABOVEOF13-ROBOT))
(not (ABOVEOF14-ROBOT))
)
:effect
(and
(Flag28prime-)
(not (Flag28-))
)

)
(:action Flag29Action-0
:parameters ()
:precondition
(and
(ABOVEOF11-ROBOT)
)
:effect
(and
(Flag29-)
(Flag29prime-)
)

)
(:action Flag29Action-1
:parameters ()
:precondition
(and
(ABOVEOF8-ROBOT)
)
:effect
(and
(Flag29-)
(Flag29prime-)
)

)
(:action Flag29Action-2
:parameters ()
:precondition
(and
(ABOVEOF10-ROBOT)
)
:effect
(and
(Flag29-)
(Flag29prime-)
)

)
(:action Flag29Action-3
:parameters ()
:precondition
(and
(ABOVEOF12-ROBOT)
)
:effect
(and
(Flag29-)
(Flag29prime-)
)

)
(:action Flag29Action-4
:parameters ()
:precondition
(and
(ABOVEOF17-ROBOT)
)
:effect
(and
(Flag29-)
(Flag29prime-)
)

)
(:action Flag29Action-5
:parameters ()
:precondition
(and
(ABOVEOF15-ROBOT)
)
:effect
(and
(Flag29-)
(Flag29prime-)
)

)
(:action Flag29Action-6
:parameters ()
:precondition
(and
(ABOVEOF9-ROBOT)
)
:effect
(and
(Flag29-)
(Flag29prime-)
)

)
(:action Flag29Action-7
:parameters ()
:precondition
(and
(ABOVEOF16-ROBOT)
)
:effect
(and
(Flag29-)
(Flag29prime-)
)

)
(:action Flag29Action-8
:parameters ()
:precondition
(and
(ABOVEOF13-ROBOT)
)
:effect
(and
(Flag29-)
(Flag29prime-)
)

)
(:action Flag29Action-9
:parameters ()
:precondition
(and
(ABOVEOF14-ROBOT)
)
:effect
(and
(Flag29-)
(Flag29prime-)
)

)
(:action Prim29Action
:parameters ()
:precondition
(and
(not (ABOVEOF11-ROBOT))
(not (ABOVEOF8-ROBOT))
(not (ABOVEOF10-ROBOT))
(not (ABOVEOF12-ROBOT))
(not (ABOVEOF17-ROBOT))
(not (ABOVEOF15-ROBOT))
(not (ABOVEOF9-ROBOT))
(not (ABOVEOF16-ROBOT))
(not (ABOVEOF13-ROBOT))
(not (ABOVEOF14-ROBOT))
)
:effect
(and
(Flag29prime-)
(not (Flag29-))
)

)
(:action Flag30Action-0
:parameters ()
:precondition
(and
(ABOVEOF11-ROBOT)
)
:effect
(and
(Flag30-)
(Flag30prime-)
)

)
(:action Flag30Action-1
:parameters ()
:precondition
(and
(ABOVEOF10-ROBOT)
)
:effect
(and
(Flag30-)
(Flag30prime-)
)

)
(:action Flag30Action-2
:parameters ()
:precondition
(and
(ABOVEOF12-ROBOT)
)
:effect
(and
(Flag30-)
(Flag30prime-)
)

)
(:action Flag30Action-3
:parameters ()
:precondition
(and
(ABOVEOF17-ROBOT)
)
:effect
(and
(Flag30-)
(Flag30prime-)
)

)
(:action Flag30Action-4
:parameters ()
:precondition
(and
(ABOVEOF15-ROBOT)
)
:effect
(and
(Flag30-)
(Flag30prime-)
)

)
(:action Flag30Action-5
:parameters ()
:precondition
(and
(ABOVEOF9-ROBOT)
)
:effect
(and
(Flag30-)
(Flag30prime-)
)

)
(:action Flag30Action-6
:parameters ()
:precondition
(and
(ABOVEOF16-ROBOT)
)
:effect
(and
(Flag30-)
(Flag30prime-)
)

)
(:action Flag30Action-7
:parameters ()
:precondition
(and
(ABOVEOF13-ROBOT)
)
:effect
(and
(Flag30-)
(Flag30prime-)
)

)
(:action Flag30Action-8
:parameters ()
:precondition
(and
(ABOVEOF14-ROBOT)
)
:effect
(and
(Flag30-)
(Flag30prime-)
)

)
(:action Prim30Action
:parameters ()
:precondition
(and
(not (ABOVEOF11-ROBOT))
(not (ABOVEOF10-ROBOT))
(not (ABOVEOF12-ROBOT))
(not (ABOVEOF17-ROBOT))
(not (ABOVEOF15-ROBOT))
(not (ABOVEOF9-ROBOT))
(not (ABOVEOF16-ROBOT))
(not (ABOVEOF13-ROBOT))
(not (ABOVEOF14-ROBOT))
)
:effect
(and
(Flag30prime-)
(not (Flag30-))
)

)
(:action Flag31Action-0
:parameters ()
:precondition
(and
(ABOVEOF11-ROBOT)
)
:effect
(and
(Flag31-)
(Flag31prime-)
)

)
(:action Flag31Action-1
:parameters ()
:precondition
(and
(ABOVEOF10-ROBOT)
)
:effect
(and
(Flag31-)
(Flag31prime-)
)

)
(:action Flag31Action-2
:parameters ()
:precondition
(and
(ABOVEOF12-ROBOT)
)
:effect
(and
(Flag31-)
(Flag31prime-)
)

)
(:action Flag31Action-3
:parameters ()
:precondition
(and
(ABOVEOF17-ROBOT)
)
:effect
(and
(Flag31-)
(Flag31prime-)
)

)
(:action Flag31Action-4
:parameters ()
:precondition
(and
(ABOVEOF15-ROBOT)
)
:effect
(and
(Flag31-)
(Flag31prime-)
)

)
(:action Flag31Action-5
:parameters ()
:precondition
(and
(ABOVEOF16-ROBOT)
)
:effect
(and
(Flag31-)
(Flag31prime-)
)

)
(:action Flag31Action-6
:parameters ()
:precondition
(and
(ABOVEOF13-ROBOT)
)
:effect
(and
(Flag31-)
(Flag31prime-)
)

)
(:action Flag31Action-7
:parameters ()
:precondition
(and
(ABOVEOF14-ROBOT)
)
:effect
(and
(Flag31-)
(Flag31prime-)
)

)
(:action Prim31Action
:parameters ()
:precondition
(and
(not (ABOVEOF11-ROBOT))
(not (ABOVEOF10-ROBOT))
(not (ABOVEOF12-ROBOT))
(not (ABOVEOF17-ROBOT))
(not (ABOVEOF15-ROBOT))
(not (ABOVEOF16-ROBOT))
(not (ABOVEOF13-ROBOT))
(not (ABOVEOF14-ROBOT))
)
:effect
(and
(Flag31prime-)
(not (Flag31-))
)

)
(:action Flag32Action-0
:parameters ()
:precondition
(and
(ABOVEOF11-ROBOT)
)
:effect
(and
(Flag32-)
(Flag32prime-)
)

)
(:action Flag32Action-1
:parameters ()
:precondition
(and
(ABOVEOF12-ROBOT)
)
:effect
(and
(Flag32-)
(Flag32prime-)
)

)
(:action Flag32Action-2
:parameters ()
:precondition
(and
(ABOVEOF17-ROBOT)
)
:effect
(and
(Flag32-)
(Flag32prime-)
)

)
(:action Flag32Action-3
:parameters ()
:precondition
(and
(ABOVEOF15-ROBOT)
)
:effect
(and
(Flag32-)
(Flag32prime-)
)

)
(:action Flag32Action-4
:parameters ()
:precondition
(and
(ABOVEOF16-ROBOT)
)
:effect
(and
(Flag32-)
(Flag32prime-)
)

)
(:action Flag32Action-5
:parameters ()
:precondition
(and
(ABOVEOF13-ROBOT)
)
:effect
(and
(Flag32-)
(Flag32prime-)
)

)
(:action Flag32Action-6
:parameters ()
:precondition
(and
(ABOVEOF14-ROBOT)
)
:effect
(and
(Flag32-)
(Flag32prime-)
)

)
(:action Prim32Action
:parameters ()
:precondition
(and
(not (ABOVEOF11-ROBOT))
(not (ABOVEOF12-ROBOT))
(not (ABOVEOF17-ROBOT))
(not (ABOVEOF15-ROBOT))
(not (ABOVEOF16-ROBOT))
(not (ABOVEOF13-ROBOT))
(not (ABOVEOF14-ROBOT))
)
:effect
(and
(Flag32prime-)
(not (Flag32-))
)

)
(:action Flag33Action-0
:parameters ()
:precondition
(and
(ABOVEOF12-ROBOT)
)
:effect
(and
(Flag33-)
(Flag33prime-)
)

)
(:action Flag33Action-1
:parameters ()
:precondition
(and
(ABOVEOF17-ROBOT)
)
:effect
(and
(Flag33-)
(Flag33prime-)
)

)
(:action Flag33Action-2
:parameters ()
:precondition
(and
(ABOVEOF15-ROBOT)
)
:effect
(and
(Flag33-)
(Flag33prime-)
)

)
(:action Flag33Action-3
:parameters ()
:precondition
(and
(ABOVEOF16-ROBOT)
)
:effect
(and
(Flag33-)
(Flag33prime-)
)

)
(:action Flag33Action-4
:parameters ()
:precondition
(and
(ABOVEOF13-ROBOT)
)
:effect
(and
(Flag33-)
(Flag33prime-)
)

)
(:action Flag33Action-5
:parameters ()
:precondition
(and
(ABOVEOF14-ROBOT)
)
:effect
(and
(Flag33-)
(Flag33prime-)
)

)
(:action Prim33Action
:parameters ()
:precondition
(and
(not (ABOVEOF12-ROBOT))
(not (ABOVEOF17-ROBOT))
(not (ABOVEOF15-ROBOT))
(not (ABOVEOF16-ROBOT))
(not (ABOVEOF13-ROBOT))
(not (ABOVEOF14-ROBOT))
)
:effect
(and
(Flag33prime-)
(not (Flag33-))
)

)
(:action Flag34Action-0
:parameters ()
:precondition
(and
(ABOVEOF15-ROBOT)
)
:effect
(and
(Flag34-)
(Flag34prime-)
)

)
(:action Flag34Action-1
:parameters ()
:precondition
(and
(ABOVEOF17-ROBOT)
)
:effect
(and
(Flag34-)
(Flag34prime-)
)

)
(:action Flag34Action-2
:parameters ()
:precondition
(and
(ABOVEOF13-ROBOT)
)
:effect
(and
(Flag34-)
(Flag34prime-)
)

)
(:action Flag34Action-3
:parameters ()
:precondition
(and
(ABOVEOF14-ROBOT)
)
:effect
(and
(Flag34-)
(Flag34prime-)
)

)
(:action Flag34Action-4
:parameters ()
:precondition
(and
(ABOVEOF16-ROBOT)
)
:effect
(and
(Flag34-)
(Flag34prime-)
)

)
(:action Prim34Action
:parameters ()
:precondition
(and
(not (ABOVEOF15-ROBOT))
(not (ABOVEOF17-ROBOT))
(not (ABOVEOF13-ROBOT))
(not (ABOVEOF14-ROBOT))
(not (ABOVEOF16-ROBOT))
)
:effect
(and
(Flag34prime-)
(not (Flag34-))
)

)
(:action Flag35Action-0
:parameters ()
:precondition
(and
(ABOVEOF16-ROBOT)
)
:effect
(and
(Flag35-)
(Flag35prime-)
)

)
(:action Flag35Action-1
:parameters ()
:precondition
(and
(ABOVEOF17-ROBOT)
)
:effect
(and
(Flag35-)
(Flag35prime-)
)

)
(:action Flag35Action-2
:parameters ()
:precondition
(and
(ABOVEOF14-ROBOT)
)
:effect
(and
(Flag35-)
(Flag35prime-)
)

)
(:action Flag35Action-3
:parameters ()
:precondition
(and
(ABOVEOF15-ROBOT)
)
:effect
(and
(Flag35-)
(Flag35prime-)
)

)
(:action Prim35Action
:parameters ()
:precondition
(and
(not (ABOVEOF16-ROBOT))
(not (ABOVEOF17-ROBOT))
(not (ABOVEOF14-ROBOT))
(not (ABOVEOF15-ROBOT))
)
:effect
(and
(Flag35prime-)
(not (Flag35-))
)

)
(:action Flag36Action-0
:parameters ()
:precondition
(and
(ABOVEOF17-ROBOT)
)
:effect
(and
(Flag36-)
(Flag36prime-)
)

)
(:action Flag36Action-1
:parameters ()
:precondition
(and
(ABOVEOF15-ROBOT)
)
:effect
(and
(Flag36-)
(Flag36prime-)
)

)
(:action Flag36Action-2
:parameters ()
:precondition
(and
(ABOVEOF16-ROBOT)
)
:effect
(and
(Flag36-)
(Flag36prime-)
)

)
(:action Prim36Action
:parameters ()
:precondition
(and
(not (ABOVEOF17-ROBOT))
(not (ABOVEOF15-ROBOT))
(not (ABOVEOF16-ROBOT))
)
:effect
(and
(Flag36prime-)
(not (Flag36-))
)

)
(:action Flag37Action-0
:parameters ()
:precondition
(and
(ABOVEOF17-ROBOT)
)
:effect
(and
(Flag37-)
(Flag37prime-)
)

)
(:action Flag37Action-1
:parameters ()
:precondition
(and
(ABOVEOF16-ROBOT)
)
:effect
(and
(Flag37-)
(Flag37prime-)
)

)
(:action Prim37Action
:parameters ()
:precondition
(and
(not (ABOVEOF17-ROBOT))
(not (ABOVEOF16-ROBOT))
)
:effect
(and
(Flag37prime-)
(not (Flag37-))
)

)
(:action Flag38Action
:parameters ()
:precondition
(and
(ABOVEOF17-ROBOT)
(BELOWOF1-ROBOT)
)
:effect
(and
(Flag38-)
(Flag38prime-)
)

)
(:action Flag39Action
:parameters ()
:precondition
(and
(BELOWOF1-ROBOT)
(ABOVEOF1-ROBOT)
)
:effect
(and
(Flag39-)
(Flag39prime-)
)

)
(:action Flag40Action
:parameters ()
:precondition
(and
(ABOVEOF11-ROBOT)
(BELOWOF1-ROBOT)
)
:effect
(and
(Flag40-)
(Flag40prime-)
)

)
(:action Flag41Action
:parameters ()
:precondition
(and
(ABOVEOF1-ROBOT)
(BELOWOF2-ROBOT)
)
:effect
(and
(Flag41-)
(Flag41prime-)
)

)
(:action Flag42Action
:parameters ()
:precondition
(and
(ABOVEOF7-ROBOT)
(BELOWOF2-ROBOT)
)
:effect
(and
(Flag42-)
(Flag42prime-)
)

)
(:action Flag43Action
:parameters ()
:precondition
(and
(ABOVEOF15-ROBOT)
(BELOWOF2-ROBOT)
)
:effect
(and
(Flag43-)
(Flag43prime-)
)

)
(:action Flag44Action
:parameters ()
:precondition
(and
(BELOWOF2-ROBOT)
(ABOVEOF13-ROBOT)
)
:effect
(and
(Flag44-)
(Flag44prime-)
)

)
(:action Flag45Action
:parameters ()
:precondition
(and
(BELOWOF1-ROBOT)
(ABOVEOF16-ROBOT)
)
:effect
(and
(Flag45-)
(Flag45prime-)
)

)
(:action Flag46Action
:parameters ()
:precondition
(and
(BELOWOF1-ROBOT)
(ABOVEOF7-ROBOT)
)
:effect
(and
(Flag46-)
(Flag46prime-)
)

)
(:action Flag47Action
:parameters ()
:precondition
(and
(ABOVEOF2-ROBOT)
(BELOWOF2-ROBOT)
)
:effect
(and
(Flag47-)
(Flag47prime-)
)

)
(:action Flag48Action
:parameters ()
:precondition
(and
(BELOWOF1-ROBOT)
(ABOVEOF5-ROBOT)
)
:effect
(and
(Flag48-)
(Flag48prime-)
)

)
(:action Flag49Action
:parameters ()
:precondition
(and
(ABOVEOF16-ROBOT)
(BELOWOF2-ROBOT)
)
:effect
(and
(Flag49-)
(Flag49prime-)
)

)
(:action Flag50Action
:parameters ()
:precondition
(and
(ABOVEOF4-ROBOT)
(BELOWOF2-ROBOT)
)
:effect
(and
(Flag50-)
(Flag50prime-)
)

)
(:action Flag51Action
:parameters ()
:precondition
(and
(BELOWOF1-ROBOT)
(ABOVEOF17-ROBOT)
)
:effect
(and
(Flag51-)
(Flag51prime-)
)

)
(:action Flag52Action
:parameters ()
:precondition
(and
(BELOWOF2-ROBOT)
(ABOVEOF14-ROBOT)
)
:effect
(and
(Flag52-)
(Flag52prime-)
)

)
(:action Flag53Action
:parameters ()
:precondition
(and
(ABOVEOF6-ROBOT)
(BELOWOF2-ROBOT)
)
:effect
(and
(Flag53-)
(Flag53prime-)
)

)
(:action Flag54Action
:parameters ()
:precondition
(and
(BELOWOF1-ROBOT)
(ABOVEOF2-ROBOT)
)
:effect
(and
(Flag54-)
(Flag54prime-)
)

)
(:action Flag55Action
:parameters ()
:precondition
(and
(ABOVEOF9-ROBOT)
(BELOWOF2-ROBOT)
)
:effect
(and
(Flag55-)
(Flag55prime-)
)

)
(:action Flag56Action
:parameters ()
:precondition
(and
(BELOWOF1-ROBOT)
(ABOVEOF13-ROBOT)
)
:effect
(and
(Flag56-)
(Flag56prime-)
)

)
(:action Flag57Action
:parameters ()
:precondition
(and
(ABOVEOF6-ROBOT)
(BELOWOF1-ROBOT)
)
:effect
(and
(Flag57-)
(Flag57prime-)
)

)
(:action Flag58Action
:parameters ()
:precondition
(and
(BELOWOF1-ROBOT)
(ABOVEOF14-ROBOT)
)
:effect
(and
(Flag58-)
(Flag58prime-)
)

)
(:action Flag59Action
:parameters ()
:precondition
(and
(ABOVEOF17-ROBOT)
(BELOWOF2-ROBOT)
)
:effect
(and
(Flag59-)
(Flag59prime-)
)

)
(:action Flag60Action
:parameters ()
:precondition
(and
(ABOVEOF10-ROBOT)
(BELOWOF1-ROBOT)
)
:effect
(and
(Flag60-)
(Flag60prime-)
)

)
(:action Flag61Action
:parameters ()
:precondition
(and
(ABOVEOF11-ROBOT)
(BELOWOF2-ROBOT)
)
:effect
(and
(Flag61-)
(Flag61prime-)
)

)
(:action Flag62Action
:parameters ()
:precondition
(and
(ABOVEOF3-ROBOT)
(BELOWOF1-ROBOT)
)
:effect
(and
(Flag62-)
(Flag62prime-)
)

)
(:action Flag63Action
:parameters ()
:precondition
(and
(BELOWOF1-ROBOT)
(ABOVEOF9-ROBOT)
)
:effect
(and
(Flag63-)
(Flag63prime-)
)

)
(:action Flag64Action
:parameters ()
:precondition
(and
(ABOVEOF10-ROBOT)
(BELOWOF2-ROBOT)
)
:effect
(and
(Flag64-)
(Flag64prime-)
)

)
(:action Flag65Action
:parameters ()
:precondition
(and
(ABOVEOF8-ROBOT)
(BELOWOF2-ROBOT)
)
:effect
(and
(Flag65-)
(Flag65prime-)
)

)
(:action Flag66Action
:parameters ()
:precondition
(and
(BELOWOF1-ROBOT)
(ABOVEOF4-ROBOT)
)
:effect
(and
(Flag66-)
(Flag66prime-)
)

)
(:action Flag67Action
:parameters ()
:precondition
(and
(ABOVEOF12-ROBOT)
(BELOWOF2-ROBOT)
)
:effect
(and
(Flag67-)
(Flag67prime-)
)

)
(:action Flag68Action
:parameters ()
:precondition
(and
(ABOVEOF12-ROBOT)
(BELOWOF1-ROBOT)
)
:effect
(and
(Flag68-)
(Flag68prime-)
)

)
(:action Flag69Action
:parameters ()
:precondition
(and
(BELOWOF2-ROBOT)
(ABOVEOF5-ROBOT)
)
:effect
(and
(Flag69-)
(Flag69prime-)
)

)
(:action Flag70Action
:parameters ()
:precondition
(and
(BELOWOF1-ROBOT)
(ABOVEOF15-ROBOT)
)
:effect
(and
(Flag70-)
(Flag70prime-)
)

)
(:action Flag71Action
:parameters ()
:precondition
(and
(ABOVEOF8-ROBOT)
(BELOWOF1-ROBOT)
)
:effect
(and
(Flag71-)
(Flag71prime-)
)

)
(:action Flag72Action
:parameters ()
:precondition
(and
(ABOVEOF3-ROBOT)
(BELOWOF2-ROBOT)
)
:effect
(and
(Flag72-)
(Flag72prime-)
)

)
(:action Prim38Action-0
:parameters ()
:precondition
(and
(not (ABOVEOF17-ROBOT))
)
:effect
(and
(Flag38prime-)
(not (Flag38-))
)

)
(:action Prim38Action-1
:parameters ()
:precondition
(and
(not (BELOWOF1-ROBOT))
)
:effect
(and
(Flag38prime-)
(not (Flag38-))
)

)
(:action Prim39Action-0
:parameters ()
:precondition
(and
(not (BELOWOF1-ROBOT))
)
:effect
(and
(Flag39prime-)
(not (Flag39-))
)

)
(:action Prim39Action-1
:parameters ()
:precondition
(and
(not (ABOVEOF1-ROBOT))
)
:effect
(and
(Flag39prime-)
(not (Flag39-))
)

)
(:action Prim40Action-0
:parameters ()
:precondition
(and
(not (ABOVEOF11-ROBOT))
)
:effect
(and
(Flag40prime-)
(not (Flag40-))
)

)
(:action Prim40Action-1
:parameters ()
:precondition
(and
(not (BELOWOF1-ROBOT))
)
:effect
(and
(Flag40prime-)
(not (Flag40-))
)

)
(:action Prim41Action-0
:parameters ()
:precondition
(and
(not (ABOVEOF1-ROBOT))
)
:effect
(and
(Flag41prime-)
(not (Flag41-))
)

)
(:action Prim41Action-1
:parameters ()
:precondition
(and
(not (BELOWOF2-ROBOT))
)
:effect
(and
(Flag41prime-)
(not (Flag41-))
)

)
(:action Prim42Action-0
:parameters ()
:precondition
(and
(not (ABOVEOF7-ROBOT))
)
:effect
(and
(Flag42prime-)
(not (Flag42-))
)

)
(:action Prim42Action-1
:parameters ()
:precondition
(and
(not (BELOWOF2-ROBOT))
)
:effect
(and
(Flag42prime-)
(not (Flag42-))
)

)
(:action Prim43Action-0
:parameters ()
:precondition
(and
(not (ABOVEOF15-ROBOT))
)
:effect
(and
(Flag43prime-)
(not (Flag43-))
)

)
(:action Prim43Action-1
:parameters ()
:precondition
(and
(not (BELOWOF2-ROBOT))
)
:effect
(and
(Flag43prime-)
(not (Flag43-))
)

)
(:action Prim44Action-0
:parameters ()
:precondition
(and
(not (BELOWOF2-ROBOT))
)
:effect
(and
(Flag44prime-)
(not (Flag44-))
)

)
(:action Prim44Action-1
:parameters ()
:precondition
(and
(not (ABOVEOF13-ROBOT))
)
:effect
(and
(Flag44prime-)
(not (Flag44-))
)

)
(:action Prim45Action-0
:parameters ()
:precondition
(and
(not (BELOWOF1-ROBOT))
)
:effect
(and
(Flag45prime-)
(not (Flag45-))
)

)
(:action Prim45Action-1
:parameters ()
:precondition
(and
(not (ABOVEOF16-ROBOT))
)
:effect
(and
(Flag45prime-)
(not (Flag45-))
)

)
(:action Prim46Action-0
:parameters ()
:precondition
(and
(not (BELOWOF1-ROBOT))
)
:effect
(and
(Flag46prime-)
(not (Flag46-))
)

)
(:action Prim46Action-1
:parameters ()
:precondition
(and
(not (ABOVEOF7-ROBOT))
)
:effect
(and
(Flag46prime-)
(not (Flag46-))
)

)
(:action Prim47Action-0
:parameters ()
:precondition
(and
(not (ABOVEOF2-ROBOT))
)
:effect
(and
(Flag47prime-)
(not (Flag47-))
)

)
(:action Prim47Action-1
:parameters ()
:precondition
(and
(not (BELOWOF2-ROBOT))
)
:effect
(and
(Flag47prime-)
(not (Flag47-))
)

)
(:action Prim48Action-0
:parameters ()
:precondition
(and
(not (BELOWOF1-ROBOT))
)
:effect
(and
(Flag48prime-)
(not (Flag48-))
)

)
(:action Prim48Action-1
:parameters ()
:precondition
(and
(not (ABOVEOF5-ROBOT))
)
:effect
(and
(Flag48prime-)
(not (Flag48-))
)

)
(:action Prim49Action-0
:parameters ()
:precondition
(and
(not (ABOVEOF16-ROBOT))
)
:effect
(and
(Flag49prime-)
(not (Flag49-))
)

)
(:action Prim49Action-1
:parameters ()
:precondition
(and
(not (BELOWOF2-ROBOT))
)
:effect
(and
(Flag49prime-)
(not (Flag49-))
)

)
(:action Prim50Action-0
:parameters ()
:precondition
(and
(not (ABOVEOF4-ROBOT))
)
:effect
(and
(Flag50prime-)
(not (Flag50-))
)

)
(:action Prim50Action-1
:parameters ()
:precondition
(and
(not (BELOWOF2-ROBOT))
)
:effect
(and
(Flag50prime-)
(not (Flag50-))
)

)
(:action Prim51Action-0
:parameters ()
:precondition
(and
(not (BELOWOF1-ROBOT))
)
:effect
(and
(Flag51prime-)
(not (Flag51-))
)

)
(:action Prim51Action-1
:parameters ()
:precondition
(and
(not (ABOVEOF17-ROBOT))
)
:effect
(and
(Flag51prime-)
(not (Flag51-))
)

)
(:action Prim52Action-0
:parameters ()
:precondition
(and
(not (BELOWOF2-ROBOT))
)
:effect
(and
(Flag52prime-)
(not (Flag52-))
)

)
(:action Prim52Action-1
:parameters ()
:precondition
(and
(not (ABOVEOF14-ROBOT))
)
:effect
(and
(Flag52prime-)
(not (Flag52-))
)

)
(:action Prim53Action-0
:parameters ()
:precondition
(and
(not (ABOVEOF6-ROBOT))
)
:effect
(and
(Flag53prime-)
(not (Flag53-))
)

)
(:action Prim53Action-1
:parameters ()
:precondition
(and
(not (BELOWOF2-ROBOT))
)
:effect
(and
(Flag53prime-)
(not (Flag53-))
)

)
(:action Prim54Action-0
:parameters ()
:precondition
(and
(not (BELOWOF1-ROBOT))
)
:effect
(and
(Flag54prime-)
(not (Flag54-))
)

)
(:action Prim54Action-1
:parameters ()
:precondition
(and
(not (ABOVEOF2-ROBOT))
)
:effect
(and
(Flag54prime-)
(not (Flag54-))
)

)
(:action Prim55Action-0
:parameters ()
:precondition
(and
(not (ABOVEOF9-ROBOT))
)
:effect
(and
(Flag55prime-)
(not (Flag55-))
)

)
(:action Prim55Action-1
:parameters ()
:precondition
(and
(not (BELOWOF2-ROBOT))
)
:effect
(and
(Flag55prime-)
(not (Flag55-))
)

)
(:action Prim56Action-0
:parameters ()
:precondition
(and
(not (BELOWOF1-ROBOT))
)
:effect
(and
(Flag56prime-)
(not (Flag56-))
)

)
(:action Prim56Action-1
:parameters ()
:precondition
(and
(not (ABOVEOF13-ROBOT))
)
:effect
(and
(Flag56prime-)
(not (Flag56-))
)

)
(:action Prim57Action-0
:parameters ()
:precondition
(and
(not (ABOVEOF6-ROBOT))
)
:effect
(and
(Flag57prime-)
(not (Flag57-))
)

)
(:action Prim57Action-1
:parameters ()
:precondition
(and
(not (BELOWOF1-ROBOT))
)
:effect
(and
(Flag57prime-)
(not (Flag57-))
)

)
(:action Prim58Action-0
:parameters ()
:precondition
(and
(not (BELOWOF1-ROBOT))
)
:effect
(and
(Flag58prime-)
(not (Flag58-))
)

)
(:action Prim58Action-1
:parameters ()
:precondition
(and
(not (ABOVEOF14-ROBOT))
)
:effect
(and
(Flag58prime-)
(not (Flag58-))
)

)
(:action Prim59Action-0
:parameters ()
:precondition
(and
(not (ABOVEOF17-ROBOT))
)
:effect
(and
(Flag59prime-)
(not (Flag59-))
)

)
(:action Prim59Action-1
:parameters ()
:precondition
(and
(not (BELOWOF2-ROBOT))
)
:effect
(and
(Flag59prime-)
(not (Flag59-))
)

)
(:action Prim60Action-0
:parameters ()
:precondition
(and
(not (ABOVEOF10-ROBOT))
)
:effect
(and
(Flag60prime-)
(not (Flag60-))
)

)
(:action Prim60Action-1
:parameters ()
:precondition
(and
(not (BELOWOF1-ROBOT))
)
:effect
(and
(Flag60prime-)
(not (Flag60-))
)

)
(:action Prim61Action-0
:parameters ()
:precondition
(and
(not (ABOVEOF11-ROBOT))
)
:effect
(and
(Flag61prime-)
(not (Flag61-))
)

)
(:action Prim61Action-1
:parameters ()
:precondition
(and
(not (BELOWOF2-ROBOT))
)
:effect
(and
(Flag61prime-)
(not (Flag61-))
)

)
(:action Prim62Action-0
:parameters ()
:precondition
(and
(not (ABOVEOF3-ROBOT))
)
:effect
(and
(Flag62prime-)
(not (Flag62-))
)

)
(:action Prim62Action-1
:parameters ()
:precondition
(and
(not (BELOWOF1-ROBOT))
)
:effect
(and
(Flag62prime-)
(not (Flag62-))
)

)
(:action Prim63Action-0
:parameters ()
:precondition
(and
(not (BELOWOF1-ROBOT))
)
:effect
(and
(Flag63prime-)
(not (Flag63-))
)

)
(:action Prim63Action-1
:parameters ()
:precondition
(and
(not (ABOVEOF9-ROBOT))
)
:effect
(and
(Flag63prime-)
(not (Flag63-))
)

)
(:action Prim64Action-0
:parameters ()
:precondition
(and
(not (ABOVEOF10-ROBOT))
)
:effect
(and
(Flag64prime-)
(not (Flag64-))
)

)
(:action Prim64Action-1
:parameters ()
:precondition
(and
(not (BELOWOF2-ROBOT))
)
:effect
(and
(Flag64prime-)
(not (Flag64-))
)

)
(:action Prim65Action-0
:parameters ()
:precondition
(and
(not (ABOVEOF8-ROBOT))
)
:effect
(and
(Flag65prime-)
(not (Flag65-))
)

)
(:action Prim65Action-1
:parameters ()
:precondition
(and
(not (BELOWOF2-ROBOT))
)
:effect
(and
(Flag65prime-)
(not (Flag65-))
)

)
(:action Prim66Action-0
:parameters ()
:precondition
(and
(not (BELOWOF1-ROBOT))
)
:effect
(and
(Flag66prime-)
(not (Flag66-))
)

)
(:action Prim66Action-1
:parameters ()
:precondition
(and
(not (ABOVEOF4-ROBOT))
)
:effect
(and
(Flag66prime-)
(not (Flag66-))
)

)
(:action Prim67Action-0
:parameters ()
:precondition
(and
(not (ABOVEOF12-ROBOT))
)
:effect
(and
(Flag67prime-)
(not (Flag67-))
)

)
(:action Prim67Action-1
:parameters ()
:precondition
(and
(not (BELOWOF2-ROBOT))
)
:effect
(and
(Flag67prime-)
(not (Flag67-))
)

)
(:action Prim68Action-0
:parameters ()
:precondition
(and
(not (ABOVEOF12-ROBOT))
)
:effect
(and
(Flag68prime-)
(not (Flag68-))
)

)
(:action Prim68Action-1
:parameters ()
:precondition
(and
(not (BELOWOF1-ROBOT))
)
:effect
(and
(Flag68prime-)
(not (Flag68-))
)

)
(:action Prim69Action-0
:parameters ()
:precondition
(and
(not (BELOWOF2-ROBOT))
)
:effect
(and
(Flag69prime-)
(not (Flag69-))
)

)
(:action Prim69Action-1
:parameters ()
:precondition
(and
(not (ABOVEOF5-ROBOT))
)
:effect
(and
(Flag69prime-)
(not (Flag69-))
)

)
(:action Prim70Action-0
:parameters ()
:precondition
(and
(not (BELOWOF1-ROBOT))
)
:effect
(and
(Flag70prime-)
(not (Flag70-))
)

)
(:action Prim70Action-1
:parameters ()
:precondition
(and
(not (ABOVEOF15-ROBOT))
)
:effect
(and
(Flag70prime-)
(not (Flag70-))
)

)
(:action Prim71Action-0
:parameters ()
:precondition
(and
(not (ABOVEOF8-ROBOT))
)
:effect
(and
(Flag71prime-)
(not (Flag71-))
)

)
(:action Prim71Action-1
:parameters ()
:precondition
(and
(not (BELOWOF1-ROBOT))
)
:effect
(and
(Flag71prime-)
(not (Flag71-))
)

)
(:action Prim72Action-0
:parameters ()
:precondition
(and
(not (ABOVEOF3-ROBOT))
)
:effect
(and
(Flag72prime-)
(not (Flag72-))
)

)
(:action Prim72Action-1
:parameters ()
:precondition
(and
(not (BELOWOF2-ROBOT))
)
:effect
(and
(Flag72prime-)
(not (Flag72-))
)

)
(:action Flag74Action
:parameters ()
:precondition
(and
(BELOWOF3-ROBOT)
(ABOVEOF5-ROBOT)
)
:effect
(and
(Flag74-)
(Flag74prime-)
)

)
(:action Flag75Action
:parameters ()
:precondition
(and
(BELOWOF3-ROBOT)
(ABOVEOF6-ROBOT)
)
:effect
(and
(Flag75-)
(Flag75prime-)
)

)
(:action Flag76Action
:parameters ()
:precondition
(and
(BELOWOF3-ROBOT)
(ABOVEOF7-ROBOT)
)
:effect
(and
(Flag76-)
(Flag76prime-)
)

)
(:action Flag77Action
:parameters ()
:precondition
(and
(BELOWOF3-ROBOT)
(ABOVEOF17-ROBOT)
)
:effect
(and
(Flag77-)
(Flag77prime-)
)

)
(:action Flag78Action
:parameters ()
:precondition
(and
(BELOWOF3-ROBOT)
(ABOVEOF14-ROBOT)
)
:effect
(and
(Flag78-)
(Flag78prime-)
)

)
(:action Flag79Action
:parameters ()
:precondition
(and
(BELOWOF3-ROBOT)
(ABOVEOF16-ROBOT)
)
:effect
(and
(Flag79-)
(Flag79prime-)
)

)
(:action Flag80Action
:parameters ()
:precondition
(and
(BELOWOF3-ROBOT)
(ABOVEOF9-ROBOT)
)
:effect
(and
(Flag80-)
(Flag80prime-)
)

)
(:action Flag81Action
:parameters ()
:precondition
(and
(BELOWOF3-ROBOT)
(ABOVEOF15-ROBOT)
)
:effect
(and
(Flag81-)
(Flag81prime-)
)

)
(:action Flag82Action
:parameters ()
:precondition
(and
(BELOWOF3-ROBOT)
(ABOVEOF2-ROBOT)
)
:effect
(and
(Flag82-)
(Flag82prime-)
)

)
(:action Flag83Action
:parameters ()
:precondition
(and
(ABOVEOF3-ROBOT)
(BELOWOF3-ROBOT)
)
:effect
(and
(Flag83-)
(Flag83prime-)
)

)
(:action Flag84Action
:parameters ()
:precondition
(and
(ABOVEOF8-ROBOT)
(BELOWOF3-ROBOT)
)
:effect
(and
(Flag84-)
(Flag84prime-)
)

)
(:action Flag85Action
:parameters ()
:precondition
(and
(BELOWOF3-ROBOT)
(ABOVEOF4-ROBOT)
)
:effect
(and
(Flag85-)
(Flag85prime-)
)

)
(:action Flag86Action
:parameters ()
:precondition
(and
(BELOWOF3-ROBOT)
(ABOVEOF10-ROBOT)
)
:effect
(and
(Flag86-)
(Flag86prime-)
)

)
(:action Flag87Action
:parameters ()
:precondition
(and
(BELOWOF2-ROBOT)
(ABOVEOF2-ROBOT)
)
:effect
(and
(Flag87-)
(Flag87prime-)
)

)
(:action Flag88Action
:parameters ()
:precondition
(and
(ABOVEOF12-ROBOT)
(BELOWOF3-ROBOT)
)
:effect
(and
(Flag88-)
(Flag88prime-)
)

)
(:action Flag89Action
:parameters ()
:precondition
(and
(BELOWOF3-ROBOT)
(ABOVEOF13-ROBOT)
)
:effect
(and
(Flag89-)
(Flag89prime-)
)

)
(:action Flag90Action
:parameters ()
:precondition
(and
(ABOVEOF11-ROBOT)
(BELOWOF3-ROBOT)
)
:effect
(and
(Flag90-)
(Flag90prime-)
)

)
(:action Prim74Action-0
:parameters ()
:precondition
(and
(not (BELOWOF3-ROBOT))
)
:effect
(and
(Flag74prime-)
(not (Flag74-))
)

)
(:action Prim74Action-1
:parameters ()
:precondition
(and
(not (ABOVEOF5-ROBOT))
)
:effect
(and
(Flag74prime-)
(not (Flag74-))
)

)
(:action Prim75Action-0
:parameters ()
:precondition
(and
(not (BELOWOF3-ROBOT))
)
:effect
(and
(Flag75prime-)
(not (Flag75-))
)

)
(:action Prim75Action-1
:parameters ()
:precondition
(and
(not (ABOVEOF6-ROBOT))
)
:effect
(and
(Flag75prime-)
(not (Flag75-))
)

)
(:action Prim76Action-0
:parameters ()
:precondition
(and
(not (BELOWOF3-ROBOT))
)
:effect
(and
(Flag76prime-)
(not (Flag76-))
)

)
(:action Prim76Action-1
:parameters ()
:precondition
(and
(not (ABOVEOF7-ROBOT))
)
:effect
(and
(Flag76prime-)
(not (Flag76-))
)

)
(:action Prim77Action-0
:parameters ()
:precondition
(and
(not (BELOWOF3-ROBOT))
)
:effect
(and
(Flag77prime-)
(not (Flag77-))
)

)
(:action Prim77Action-1
:parameters ()
:precondition
(and
(not (ABOVEOF17-ROBOT))
)
:effect
(and
(Flag77prime-)
(not (Flag77-))
)

)
(:action Prim78Action-0
:parameters ()
:precondition
(and
(not (BELOWOF3-ROBOT))
)
:effect
(and
(Flag78prime-)
(not (Flag78-))
)

)
(:action Prim78Action-1
:parameters ()
:precondition
(and
(not (ABOVEOF14-ROBOT))
)
:effect
(and
(Flag78prime-)
(not (Flag78-))
)

)
(:action Prim79Action-0
:parameters ()
:precondition
(and
(not (BELOWOF3-ROBOT))
)
:effect
(and
(Flag79prime-)
(not (Flag79-))
)

)
(:action Prim79Action-1
:parameters ()
:precondition
(and
(not (ABOVEOF16-ROBOT))
)
:effect
(and
(Flag79prime-)
(not (Flag79-))
)

)
(:action Prim80Action-0
:parameters ()
:precondition
(and
(not (BELOWOF3-ROBOT))
)
:effect
(and
(Flag80prime-)
(not (Flag80-))
)

)
(:action Prim80Action-1
:parameters ()
:precondition
(and
(not (ABOVEOF9-ROBOT))
)
:effect
(and
(Flag80prime-)
(not (Flag80-))
)

)
(:action Prim81Action-0
:parameters ()
:precondition
(and
(not (BELOWOF3-ROBOT))
)
:effect
(and
(Flag81prime-)
(not (Flag81-))
)

)
(:action Prim81Action-1
:parameters ()
:precondition
(and
(not (ABOVEOF15-ROBOT))
)
:effect
(and
(Flag81prime-)
(not (Flag81-))
)

)
(:action Prim82Action-0
:parameters ()
:precondition
(and
(not (BELOWOF3-ROBOT))
)
:effect
(and
(Flag82prime-)
(not (Flag82-))
)

)
(:action Prim82Action-1
:parameters ()
:precondition
(and
(not (ABOVEOF2-ROBOT))
)
:effect
(and
(Flag82prime-)
(not (Flag82-))
)

)
(:action Prim83Action-0
:parameters ()
:precondition
(and
(not (ABOVEOF3-ROBOT))
)
:effect
(and
(Flag83prime-)
(not (Flag83-))
)

)
(:action Prim83Action-1
:parameters ()
:precondition
(and
(not (BELOWOF3-ROBOT))
)
:effect
(and
(Flag83prime-)
(not (Flag83-))
)

)
(:action Prim84Action-0
:parameters ()
:precondition
(and
(not (ABOVEOF8-ROBOT))
)
:effect
(and
(Flag84prime-)
(not (Flag84-))
)

)
(:action Prim84Action-1
:parameters ()
:precondition
(and
(not (BELOWOF3-ROBOT))
)
:effect
(and
(Flag84prime-)
(not (Flag84-))
)

)
(:action Prim85Action-0
:parameters ()
:precondition
(and
(not (BELOWOF3-ROBOT))
)
:effect
(and
(Flag85prime-)
(not (Flag85-))
)

)
(:action Prim85Action-1
:parameters ()
:precondition
(and
(not (ABOVEOF4-ROBOT))
)
:effect
(and
(Flag85prime-)
(not (Flag85-))
)

)
(:action Prim86Action-0
:parameters ()
:precondition
(and
(not (BELOWOF3-ROBOT))
)
:effect
(and
(Flag86prime-)
(not (Flag86-))
)

)
(:action Prim86Action-1
:parameters ()
:precondition
(and
(not (ABOVEOF10-ROBOT))
)
:effect
(and
(Flag86prime-)
(not (Flag86-))
)

)
(:action Prim87Action-0
:parameters ()
:precondition
(and
(not (BELOWOF2-ROBOT))
)
:effect
(and
(Flag87prime-)
(not (Flag87-))
)

)
(:action Prim87Action-1
:parameters ()
:precondition
(and
(not (ABOVEOF2-ROBOT))
)
:effect
(and
(Flag87prime-)
(not (Flag87-))
)

)
(:action Prim88Action-0
:parameters ()
:precondition
(and
(not (ABOVEOF12-ROBOT))
)
:effect
(and
(Flag88prime-)
(not (Flag88-))
)

)
(:action Prim88Action-1
:parameters ()
:precondition
(and
(not (BELOWOF3-ROBOT))
)
:effect
(and
(Flag88prime-)
(not (Flag88-))
)

)
(:action Prim89Action-0
:parameters ()
:precondition
(and
(not (BELOWOF3-ROBOT))
)
:effect
(and
(Flag89prime-)
(not (Flag89-))
)

)
(:action Prim89Action-1
:parameters ()
:precondition
(and
(not (ABOVEOF13-ROBOT))
)
:effect
(and
(Flag89prime-)
(not (Flag89-))
)

)
(:action Prim90Action-0
:parameters ()
:precondition
(and
(not (ABOVEOF11-ROBOT))
)
:effect
(and
(Flag90prime-)
(not (Flag90-))
)

)
(:action Prim90Action-1
:parameters ()
:precondition
(and
(not (BELOWOF3-ROBOT))
)
:effect
(and
(Flag90prime-)
(not (Flag90-))
)

)
(:action Flag92Action
:parameters ()
:precondition
(and
(ABOVEOF9-ROBOT)
(BELOWOF4-ROBOT)
)
:effect
(and
(Flag92-)
(Flag92prime-)
)

)
(:action Flag93Action
:parameters ()
:precondition
(and
(BELOWOF4-ROBOT)
(ABOVEOF13-ROBOT)
)
:effect
(and
(Flag93-)
(Flag93prime-)
)

)
(:action Flag94Action
:parameters ()
:precondition
(and
(ABOVEOF3-ROBOT)
(BELOWOF4-ROBOT)
)
:effect
(and
(Flag94-)
(Flag94prime-)
)

)
(:action Flag95Action
:parameters ()
:precondition
(and
(ABOVEOF17-ROBOT)
(BELOWOF4-ROBOT)
)
:effect
(and
(Flag95-)
(Flag95prime-)
)

)
(:action Flag96Action
:parameters ()
:precondition
(and
(ABOVEOF6-ROBOT)
(BELOWOF4-ROBOT)
)
:effect
(and
(Flag96-)
(Flag96prime-)
)

)
(:action Flag97Action
:parameters ()
:precondition
(and
(BELOWOF4-ROBOT)
(ABOVEOF4-ROBOT)
)
:effect
(and
(Flag97-)
(Flag97prime-)
)

)
(:action Flag98Action
:parameters ()
:precondition
(and
(BELOWOF4-ROBOT)
(ABOVEOF16-ROBOT)
)
:effect
(and
(Flag98-)
(Flag98prime-)
)

)
(:action Flag99Action
:parameters ()
:precondition
(and
(ABOVEOF8-ROBOT)
(BELOWOF4-ROBOT)
)
:effect
(and
(Flag99-)
(Flag99prime-)
)

)
(:action Flag100Action
:parameters ()
:precondition
(and
(ABOVEOF12-ROBOT)
(BELOWOF4-ROBOT)
)
:effect
(and
(Flag100-)
(Flag100prime-)
)

)
(:action Flag101Action
:parameters ()
:precondition
(and
(BELOWOF4-ROBOT)
(ABOVEOF5-ROBOT)
)
:effect
(and
(Flag101-)
(Flag101prime-)
)

)
(:action Flag102Action
:parameters ()
:precondition
(and
(BELOWOF3-ROBOT)
(ABOVEOF3-ROBOT)
)
:effect
(and
(Flag102-)
(Flag102prime-)
)

)
(:action Flag103Action
:parameters ()
:precondition
(and
(ABOVEOF10-ROBOT)
(BELOWOF4-ROBOT)
)
:effect
(and
(Flag103-)
(Flag103prime-)
)

)
(:action Flag104Action
:parameters ()
:precondition
(and
(BELOWOF4-ROBOT)
(ABOVEOF15-ROBOT)
)
:effect
(and
(Flag104-)
(Flag104prime-)
)

)
(:action Flag105Action
:parameters ()
:precondition
(and
(BELOWOF3-ROBOT)
(ABOVEOF11-ROBOT)
)
:effect
(and
(Flag105-)
(Flag105prime-)
)

)
(:action Flag106Action
:parameters ()
:precondition
(and
(BELOWOF4-ROBOT)
(ABOVEOF7-ROBOT)
)
:effect
(and
(Flag106-)
(Flag106prime-)
)

)
(:action Flag107Action
:parameters ()
:precondition
(and
(ABOVEOF11-ROBOT)
(BELOWOF4-ROBOT)
)
:effect
(and
(Flag107-)
(Flag107prime-)
)

)
(:action Flag108Action
:parameters ()
:precondition
(and
(BELOWOF4-ROBOT)
(ABOVEOF14-ROBOT)
)
:effect
(and
(Flag108-)
(Flag108prime-)
)

)
(:action Prim92Action-0
:parameters ()
:precondition
(and
(not (ABOVEOF9-ROBOT))
)
:effect
(and
(Flag92prime-)
(not (Flag92-))
)

)
(:action Prim92Action-1
:parameters ()
:precondition
(and
(not (BELOWOF4-ROBOT))
)
:effect
(and
(Flag92prime-)
(not (Flag92-))
)

)
(:action Prim93Action-0
:parameters ()
:precondition
(and
(not (BELOWOF4-ROBOT))
)
:effect
(and
(Flag93prime-)
(not (Flag93-))
)

)
(:action Prim93Action-1
:parameters ()
:precondition
(and
(not (ABOVEOF13-ROBOT))
)
:effect
(and
(Flag93prime-)
(not (Flag93-))
)

)
(:action Prim94Action-0
:parameters ()
:precondition
(and
(not (ABOVEOF3-ROBOT))
)
:effect
(and
(Flag94prime-)
(not (Flag94-))
)

)
(:action Prim94Action-1
:parameters ()
:precondition
(and
(not (BELOWOF4-ROBOT))
)
:effect
(and
(Flag94prime-)
(not (Flag94-))
)

)
(:action Prim95Action-0
:parameters ()
:precondition
(and
(not (ABOVEOF17-ROBOT))
)
:effect
(and
(Flag95prime-)
(not (Flag95-))
)

)
(:action Prim95Action-1
:parameters ()
:precondition
(and
(not (BELOWOF4-ROBOT))
)
:effect
(and
(Flag95prime-)
(not (Flag95-))
)

)
(:action Prim96Action-0
:parameters ()
:precondition
(and
(not (ABOVEOF6-ROBOT))
)
:effect
(and
(Flag96prime-)
(not (Flag96-))
)

)
(:action Prim96Action-1
:parameters ()
:precondition
(and
(not (BELOWOF4-ROBOT))
)
:effect
(and
(Flag96prime-)
(not (Flag96-))
)

)
(:action Prim97Action-0
:parameters ()
:precondition
(and
(not (BELOWOF4-ROBOT))
)
:effect
(and
(Flag97prime-)
(not (Flag97-))
)

)
(:action Prim97Action-1
:parameters ()
:precondition
(and
(not (ABOVEOF4-ROBOT))
)
:effect
(and
(Flag97prime-)
(not (Flag97-))
)

)
(:action Prim98Action-0
:parameters ()
:precondition
(and
(not (BELOWOF4-ROBOT))
)
:effect
(and
(Flag98prime-)
(not (Flag98-))
)

)
(:action Prim98Action-1
:parameters ()
:precondition
(and
(not (ABOVEOF16-ROBOT))
)
:effect
(and
(Flag98prime-)
(not (Flag98-))
)

)
(:action Prim99Action-0
:parameters ()
:precondition
(and
(not (ABOVEOF8-ROBOT))
)
:effect
(and
(Flag99prime-)
(not (Flag99-))
)

)
(:action Prim99Action-1
:parameters ()
:precondition
(and
(not (BELOWOF4-ROBOT))
)
:effect
(and
(Flag99prime-)
(not (Flag99-))
)

)
(:action Prim100Action-0
:parameters ()
:precondition
(and
(not (ABOVEOF12-ROBOT))
)
:effect
(and
(Flag100prime-)
(not (Flag100-))
)

)
(:action Prim100Action-1
:parameters ()
:precondition
(and
(not (BELOWOF4-ROBOT))
)
:effect
(and
(Flag100prime-)
(not (Flag100-))
)

)
(:action Prim101Action-0
:parameters ()
:precondition
(and
(not (BELOWOF4-ROBOT))
)
:effect
(and
(Flag101prime-)
(not (Flag101-))
)

)
(:action Prim101Action-1
:parameters ()
:precondition
(and
(not (ABOVEOF5-ROBOT))
)
:effect
(and
(Flag101prime-)
(not (Flag101-))
)

)
(:action Prim102Action-0
:parameters ()
:precondition
(and
(not (BELOWOF3-ROBOT))
)
:effect
(and
(Flag102prime-)
(not (Flag102-))
)

)
(:action Prim102Action-1
:parameters ()
:precondition
(and
(not (ABOVEOF3-ROBOT))
)
:effect
(and
(Flag102prime-)
(not (Flag102-))
)

)
(:action Prim103Action-0
:parameters ()
:precondition
(and
(not (ABOVEOF10-ROBOT))
)
:effect
(and
(Flag103prime-)
(not (Flag103-))
)

)
(:action Prim103Action-1
:parameters ()
:precondition
(and
(not (BELOWOF4-ROBOT))
)
:effect
(and
(Flag103prime-)
(not (Flag103-))
)

)
(:action Prim104Action-0
:parameters ()
:precondition
(and
(not (BELOWOF4-ROBOT))
)
:effect
(and
(Flag104prime-)
(not (Flag104-))
)

)
(:action Prim104Action-1
:parameters ()
:precondition
(and
(not (ABOVEOF15-ROBOT))
)
:effect
(and
(Flag104prime-)
(not (Flag104-))
)

)
(:action Prim105Action-0
:parameters ()
:precondition
(and
(not (BELOWOF3-ROBOT))
)
:effect
(and
(Flag105prime-)
(not (Flag105-))
)

)
(:action Prim105Action-1
:parameters ()
:precondition
(and
(not (ABOVEOF11-ROBOT))
)
:effect
(and
(Flag105prime-)
(not (Flag105-))
)

)
(:action Prim106Action-0
:parameters ()
:precondition
(and
(not (BELOWOF4-ROBOT))
)
:effect
(and
(Flag106prime-)
(not (Flag106-))
)

)
(:action Prim106Action-1
:parameters ()
:precondition
(and
(not (ABOVEOF7-ROBOT))
)
:effect
(and
(Flag106prime-)
(not (Flag106-))
)

)
(:action Prim107Action-0
:parameters ()
:precondition
(and
(not (ABOVEOF11-ROBOT))
)
:effect
(and
(Flag107prime-)
(not (Flag107-))
)

)
(:action Prim107Action-1
:parameters ()
:precondition
(and
(not (BELOWOF4-ROBOT))
)
:effect
(and
(Flag107prime-)
(not (Flag107-))
)

)
(:action Prim108Action-0
:parameters ()
:precondition
(and
(not (BELOWOF4-ROBOT))
)
:effect
(and
(Flag108prime-)
(not (Flag108-))
)

)
(:action Prim108Action-1
:parameters ()
:precondition
(and
(not (ABOVEOF14-ROBOT))
)
:effect
(and
(Flag108prime-)
(not (Flag108-))
)

)
(:action Flag110Action
:parameters ()
:precondition
(and
(ABOVEOF17-ROBOT)
(BELOWOF5-ROBOT)
)
:effect
(and
(Flag110-)
(Flag110prime-)
)

)
(:action Flag111Action
:parameters ()
:precondition
(and
(ABOVEOF4-ROBOT)
(BELOWOF5-ROBOT)
)
:effect
(and
(Flag111-)
(Flag111prime-)
)

)
(:action Flag112Action
:parameters ()
:precondition
(and
(ABOVEOF15-ROBOT)
(BELOWOF5-ROBOT)
)
:effect
(and
(Flag112-)
(Flag112prime-)
)

)
(:action Flag113Action
:parameters ()
:precondition
(and
(ABOVEOF8-ROBOT)
(BELOWOF5-ROBOT)
)
:effect
(and
(Flag113-)
(Flag113prime-)
)

)
(:action Flag114Action
:parameters ()
:precondition
(and
(ABOVEOF16-ROBOT)
(BELOWOF5-ROBOT)
)
:effect
(and
(Flag114-)
(Flag114prime-)
)

)
(:action Flag115Action
:parameters ()
:precondition
(and
(ABOVEOF13-ROBOT)
(BELOWOF5-ROBOT)
)
:effect
(and
(Flag115-)
(Flag115prime-)
)

)
(:action Flag116Action
:parameters ()
:precondition
(and
(ABOVEOF10-ROBOT)
(BELOWOF5-ROBOT)
)
:effect
(and
(Flag116-)
(Flag116prime-)
)

)
(:action Flag117Action
:parameters ()
:precondition
(and
(ABOVEOF16-ROBOT)
(BELOWOF4-ROBOT)
)
:effect
(and
(Flag117-)
(Flag117prime-)
)

)
(:action Flag118Action
:parameters ()
:precondition
(and
(ABOVEOF5-ROBOT)
(BELOWOF5-ROBOT)
)
:effect
(and
(Flag118-)
(Flag118prime-)
)

)
(:action Flag119Action
:parameters ()
:precondition
(and
(ABOVEOF7-ROBOT)
(BELOWOF5-ROBOT)
)
:effect
(and
(Flag119-)
(Flag119prime-)
)

)
(:action Flag120Action
:parameters ()
:precondition
(and
(ABOVEOF9-ROBOT)
(BELOWOF5-ROBOT)
)
:effect
(and
(Flag120-)
(Flag120prime-)
)

)
(:action Flag121Action
:parameters ()
:precondition
(and
(ABOVEOF12-ROBOT)
(BELOWOF5-ROBOT)
)
:effect
(and
(Flag121-)
(Flag121prime-)
)

)
(:action Flag122Action
:parameters ()
:precondition
(and
(ABOVEOF11-ROBOT)
(BELOWOF5-ROBOT)
)
:effect
(and
(Flag122-)
(Flag122prime-)
)

)
(:action Flag123Action
:parameters ()
:precondition
(and
(ABOVEOF6-ROBOT)
(BELOWOF5-ROBOT)
)
:effect
(and
(Flag123-)
(Flag123prime-)
)

)
(:action Flag124Action
:parameters ()
:precondition
(and
(ABOVEOF14-ROBOT)
(BELOWOF5-ROBOT)
)
:effect
(and
(Flag124-)
(Flag124prime-)
)

)
(:action Prim110Action-0
:parameters ()
:precondition
(and
(not (ABOVEOF17-ROBOT))
)
:effect
(and
(Flag110prime-)
(not (Flag110-))
)

)
(:action Prim110Action-1
:parameters ()
:precondition
(and
(not (BELOWOF5-ROBOT))
)
:effect
(and
(Flag110prime-)
(not (Flag110-))
)

)
(:action Prim111Action-0
:parameters ()
:precondition
(and
(not (ABOVEOF4-ROBOT))
)
:effect
(and
(Flag111prime-)
(not (Flag111-))
)

)
(:action Prim111Action-1
:parameters ()
:precondition
(and
(not (BELOWOF5-ROBOT))
)
:effect
(and
(Flag111prime-)
(not (Flag111-))
)

)
(:action Prim112Action-0
:parameters ()
:precondition
(and
(not (ABOVEOF15-ROBOT))
)
:effect
(and
(Flag112prime-)
(not (Flag112-))
)

)
(:action Prim112Action-1
:parameters ()
:precondition
(and
(not (BELOWOF5-ROBOT))
)
:effect
(and
(Flag112prime-)
(not (Flag112-))
)

)
(:action Prim113Action-0
:parameters ()
:precondition
(and
(not (ABOVEOF8-ROBOT))
)
:effect
(and
(Flag113prime-)
(not (Flag113-))
)

)
(:action Prim113Action-1
:parameters ()
:precondition
(and
(not (BELOWOF5-ROBOT))
)
:effect
(and
(Flag113prime-)
(not (Flag113-))
)

)
(:action Prim114Action-0
:parameters ()
:precondition
(and
(not (ABOVEOF16-ROBOT))
)
:effect
(and
(Flag114prime-)
(not (Flag114-))
)

)
(:action Prim114Action-1
:parameters ()
:precondition
(and
(not (BELOWOF5-ROBOT))
)
:effect
(and
(Flag114prime-)
(not (Flag114-))
)

)
(:action Prim115Action-0
:parameters ()
:precondition
(and
(not (ABOVEOF13-ROBOT))
)
:effect
(and
(Flag115prime-)
(not (Flag115-))
)

)
(:action Prim115Action-1
:parameters ()
:precondition
(and
(not (BELOWOF5-ROBOT))
)
:effect
(and
(Flag115prime-)
(not (Flag115-))
)

)
(:action Prim116Action-0
:parameters ()
:precondition
(and
(not (ABOVEOF10-ROBOT))
)
:effect
(and
(Flag116prime-)
(not (Flag116-))
)

)
(:action Prim116Action-1
:parameters ()
:precondition
(and
(not (BELOWOF5-ROBOT))
)
:effect
(and
(Flag116prime-)
(not (Flag116-))
)

)
(:action Prim117Action-0
:parameters ()
:precondition
(and
(not (ABOVEOF16-ROBOT))
)
:effect
(and
(Flag117prime-)
(not (Flag117-))
)

)
(:action Prim117Action-1
:parameters ()
:precondition
(and
(not (BELOWOF4-ROBOT))
)
:effect
(and
(Flag117prime-)
(not (Flag117-))
)

)
(:action Prim118Action-0
:parameters ()
:precondition
(and
(not (ABOVEOF5-ROBOT))
)
:effect
(and
(Flag118prime-)
(not (Flag118-))
)

)
(:action Prim118Action-1
:parameters ()
:precondition
(and
(not (BELOWOF5-ROBOT))
)
:effect
(and
(Flag118prime-)
(not (Flag118-))
)

)
(:action Prim119Action-0
:parameters ()
:precondition
(and
(not (ABOVEOF7-ROBOT))
)
:effect
(and
(Flag119prime-)
(not (Flag119-))
)

)
(:action Prim119Action-1
:parameters ()
:precondition
(and
(not (BELOWOF5-ROBOT))
)
:effect
(and
(Flag119prime-)
(not (Flag119-))
)

)
(:action Prim120Action-0
:parameters ()
:precondition
(and
(not (ABOVEOF9-ROBOT))
)
:effect
(and
(Flag120prime-)
(not (Flag120-))
)

)
(:action Prim120Action-1
:parameters ()
:precondition
(and
(not (BELOWOF5-ROBOT))
)
:effect
(and
(Flag120prime-)
(not (Flag120-))
)

)
(:action Prim121Action-0
:parameters ()
:precondition
(and
(not (ABOVEOF12-ROBOT))
)
:effect
(and
(Flag121prime-)
(not (Flag121-))
)

)
(:action Prim121Action-1
:parameters ()
:precondition
(and
(not (BELOWOF5-ROBOT))
)
:effect
(and
(Flag121prime-)
(not (Flag121-))
)

)
(:action Prim122Action-0
:parameters ()
:precondition
(and
(not (ABOVEOF11-ROBOT))
)
:effect
(and
(Flag122prime-)
(not (Flag122-))
)

)
(:action Prim122Action-1
:parameters ()
:precondition
(and
(not (BELOWOF5-ROBOT))
)
:effect
(and
(Flag122prime-)
(not (Flag122-))
)

)
(:action Prim123Action-0
:parameters ()
:precondition
(and
(not (ABOVEOF6-ROBOT))
)
:effect
(and
(Flag123prime-)
(not (Flag123-))
)

)
(:action Prim123Action-1
:parameters ()
:precondition
(and
(not (BELOWOF5-ROBOT))
)
:effect
(and
(Flag123prime-)
(not (Flag123-))
)

)
(:action Prim124Action-0
:parameters ()
:precondition
(and
(not (ABOVEOF14-ROBOT))
)
:effect
(and
(Flag124prime-)
(not (Flag124-))
)

)
(:action Prim124Action-1
:parameters ()
:precondition
(and
(not (BELOWOF5-ROBOT))
)
:effect
(and
(Flag124prime-)
(not (Flag124-))
)

)
(:action Flag126Action
:parameters ()
:precondition
(and
(BELOWOF6-ROBOT)
(ABOVEOF15-ROBOT)
)
:effect
(and
(Flag126-)
(Flag126prime-)
)

)
(:action Flag127Action
:parameters ()
:precondition
(and
(BELOWOF6-ROBOT)
(ABOVEOF5-ROBOT)
)
:effect
(and
(Flag127-)
(Flag127prime-)
)

)
(:action Flag128Action
:parameters ()
:precondition
(and
(ABOVEOF11-ROBOT)
(BELOWOF6-ROBOT)
)
:effect
(and
(Flag128-)
(Flag128prime-)
)

)
(:action Flag129Action
:parameters ()
:precondition
(and
(ABOVEOF12-ROBOT)
(BELOWOF6-ROBOT)
)
:effect
(and
(Flag129-)
(Flag129prime-)
)

)
(:action Flag130Action
:parameters ()
:precondition
(and
(BELOWOF5-ROBOT)
(ABOVEOF13-ROBOT)
)
:effect
(and
(Flag130-)
(Flag130prime-)
)

)
(:action Flag131Action
:parameters ()
:precondition
(and
(BELOWOF6-ROBOT)
(ABOVEOF13-ROBOT)
)
:effect
(and
(Flag131-)
(Flag131prime-)
)

)
(:action Flag132Action
:parameters ()
:precondition
(and
(BELOWOF6-ROBOT)
(ABOVEOF7-ROBOT)
)
:effect
(and
(Flag132-)
(Flag132prime-)
)

)
(:action Flag133Action
:parameters ()
:precondition
(and
(BELOWOF5-ROBOT)
(ABOVEOF5-ROBOT)
)
:effect
(and
(Flag133-)
(Flag133prime-)
)

)
(:action Flag134Action
:parameters ()
:precondition
(and
(BELOWOF6-ROBOT)
(ABOVEOF6-ROBOT)
)
:effect
(and
(Flag134-)
(Flag134prime-)
)

)
(:action Flag135Action
:parameters ()
:precondition
(and
(BELOWOF6-ROBOT)
(ABOVEOF17-ROBOT)
)
:effect
(and
(Flag135-)
(Flag135prime-)
)

)
(:action Flag136Action
:parameters ()
:precondition
(and
(ABOVEOF8-ROBOT)
(BELOWOF6-ROBOT)
)
:effect
(and
(Flag136-)
(Flag136prime-)
)

)
(:action Flag137Action
:parameters ()
:precondition
(and
(BELOWOF6-ROBOT)
(ABOVEOF9-ROBOT)
)
:effect
(and
(Flag137-)
(Flag137prime-)
)

)
(:action Flag138Action
:parameters ()
:precondition
(and
(BELOWOF6-ROBOT)
(ABOVEOF14-ROBOT)
)
:effect
(and
(Flag138-)
(Flag138prime-)
)

)
(:action Flag139Action
:parameters ()
:precondition
(and
(BELOWOF6-ROBOT)
(ABOVEOF10-ROBOT)
)
:effect
(and
(Flag139-)
(Flag139prime-)
)

)
(:action Flag140Action
:parameters ()
:precondition
(and
(BELOWOF6-ROBOT)
(ABOVEOF16-ROBOT)
)
:effect
(and
(Flag140-)
(Flag140prime-)
)

)
(:action Prim126Action-0
:parameters ()
:precondition
(and
(not (BELOWOF6-ROBOT))
)
:effect
(and
(Flag126prime-)
(not (Flag126-))
)

)
(:action Prim126Action-1
:parameters ()
:precondition
(and
(not (ABOVEOF15-ROBOT))
)
:effect
(and
(Flag126prime-)
(not (Flag126-))
)

)
(:action Prim127Action-0
:parameters ()
:precondition
(and
(not (BELOWOF6-ROBOT))
)
:effect
(and
(Flag127prime-)
(not (Flag127-))
)

)
(:action Prim127Action-1
:parameters ()
:precondition
(and
(not (ABOVEOF5-ROBOT))
)
:effect
(and
(Flag127prime-)
(not (Flag127-))
)

)
(:action Prim128Action-0
:parameters ()
:precondition
(and
(not (ABOVEOF11-ROBOT))
)
:effect
(and
(Flag128prime-)
(not (Flag128-))
)

)
(:action Prim128Action-1
:parameters ()
:precondition
(and
(not (BELOWOF6-ROBOT))
)
:effect
(and
(Flag128prime-)
(not (Flag128-))
)

)
(:action Prim129Action-0
:parameters ()
:precondition
(and
(not (ABOVEOF12-ROBOT))
)
:effect
(and
(Flag129prime-)
(not (Flag129-))
)

)
(:action Prim129Action-1
:parameters ()
:precondition
(and
(not (BELOWOF6-ROBOT))
)
:effect
(and
(Flag129prime-)
(not (Flag129-))
)

)
(:action Prim130Action-0
:parameters ()
:precondition
(and
(not (BELOWOF5-ROBOT))
)
:effect
(and
(Flag130prime-)
(not (Flag130-))
)

)
(:action Prim130Action-1
:parameters ()
:precondition
(and
(not (ABOVEOF13-ROBOT))
)
:effect
(and
(Flag130prime-)
(not (Flag130-))
)

)
(:action Prim131Action-0
:parameters ()
:precondition
(and
(not (BELOWOF6-ROBOT))
)
:effect
(and
(Flag131prime-)
(not (Flag131-))
)

)
(:action Prim131Action-1
:parameters ()
:precondition
(and
(not (ABOVEOF13-ROBOT))
)
:effect
(and
(Flag131prime-)
(not (Flag131-))
)

)
(:action Prim132Action-0
:parameters ()
:precondition
(and
(not (BELOWOF6-ROBOT))
)
:effect
(and
(Flag132prime-)
(not (Flag132-))
)

)
(:action Prim132Action-1
:parameters ()
:precondition
(and
(not (ABOVEOF7-ROBOT))
)
:effect
(and
(Flag132prime-)
(not (Flag132-))
)

)
(:action Prim133Action-0
:parameters ()
:precondition
(and
(not (BELOWOF5-ROBOT))
)
:effect
(and
(Flag133prime-)
(not (Flag133-))
)

)
(:action Prim133Action-1
:parameters ()
:precondition
(and
(not (ABOVEOF5-ROBOT))
)
:effect
(and
(Flag133prime-)
(not (Flag133-))
)

)
(:action Prim134Action-0
:parameters ()
:precondition
(and
(not (BELOWOF6-ROBOT))
)
:effect
(and
(Flag134prime-)
(not (Flag134-))
)

)
(:action Prim134Action-1
:parameters ()
:precondition
(and
(not (ABOVEOF6-ROBOT))
)
:effect
(and
(Flag134prime-)
(not (Flag134-))
)

)
(:action Prim135Action-0
:parameters ()
:precondition
(and
(not (BELOWOF6-ROBOT))
)
:effect
(and
(Flag135prime-)
(not (Flag135-))
)

)
(:action Prim135Action-1
:parameters ()
:precondition
(and
(not (ABOVEOF17-ROBOT))
)
:effect
(and
(Flag135prime-)
(not (Flag135-))
)

)
(:action Prim136Action-0
:parameters ()
:precondition
(and
(not (ABOVEOF8-ROBOT))
)
:effect
(and
(Flag136prime-)
(not (Flag136-))
)

)
(:action Prim136Action-1
:parameters ()
:precondition
(and
(not (BELOWOF6-ROBOT))
)
:effect
(and
(Flag136prime-)
(not (Flag136-))
)

)
(:action Prim137Action-0
:parameters ()
:precondition
(and
(not (BELOWOF6-ROBOT))
)
:effect
(and
(Flag137prime-)
(not (Flag137-))
)

)
(:action Prim137Action-1
:parameters ()
:precondition
(and
(not (ABOVEOF9-ROBOT))
)
:effect
(and
(Flag137prime-)
(not (Flag137-))
)

)
(:action Prim138Action-0
:parameters ()
:precondition
(and
(not (BELOWOF6-ROBOT))
)
:effect
(and
(Flag138prime-)
(not (Flag138-))
)

)
(:action Prim138Action-1
:parameters ()
:precondition
(and
(not (ABOVEOF14-ROBOT))
)
:effect
(and
(Flag138prime-)
(not (Flag138-))
)

)
(:action Prim139Action-0
:parameters ()
:precondition
(and
(not (BELOWOF6-ROBOT))
)
:effect
(and
(Flag139prime-)
(not (Flag139-))
)

)
(:action Prim139Action-1
:parameters ()
:precondition
(and
(not (ABOVEOF10-ROBOT))
)
:effect
(and
(Flag139prime-)
(not (Flag139-))
)

)
(:action Prim140Action-0
:parameters ()
:precondition
(and
(not (BELOWOF6-ROBOT))
)
:effect
(and
(Flag140prime-)
(not (Flag140-))
)

)
(:action Prim140Action-1
:parameters ()
:precondition
(and
(not (ABOVEOF16-ROBOT))
)
:effect
(and
(Flag140prime-)
(not (Flag140-))
)

)
(:action Flag142Action
:parameters ()
:precondition
(and
(ABOVEOF6-ROBOT)
(BELOWOF7-ROBOT)
)
:effect
(and
(Flag142-)
(Flag142prime-)
)

)
(:action Flag143Action
:parameters ()
:precondition
(and
(BELOWOF7-ROBOT)
(ABOVEOF14-ROBOT)
)
:effect
(and
(Flag143-)
(Flag143prime-)
)

)
(:action Flag144Action
:parameters ()
:precondition
(and
(BELOWOF7-ROBOT)
(ABOVEOF15-ROBOT)
)
:effect
(and
(Flag144-)
(Flag144prime-)
)

)
(:action Flag145Action
:parameters ()
:precondition
(and
(ABOVEOF7-ROBOT)
(BELOWOF7-ROBOT)
)
:effect
(and
(Flag145-)
(Flag145prime-)
)

)
(:action Flag146Action
:parameters ()
:precondition
(and
(ABOVEOF16-ROBOT)
(BELOWOF7-ROBOT)
)
:effect
(and
(Flag146-)
(Flag146prime-)
)

)
(:action Flag147Action
:parameters ()
:precondition
(and
(ABOVEOF10-ROBOT)
(BELOWOF6-ROBOT)
)
:effect
(and
(Flag147-)
(Flag147prime-)
)

)
(:action Flag148Action
:parameters ()
:precondition
(and
(ABOVEOF10-ROBOT)
(BELOWOF7-ROBOT)
)
:effect
(and
(Flag148-)
(Flag148prime-)
)

)
(:action Flag149Action
:parameters ()
:precondition
(and
(BELOWOF7-ROBOT)
(ABOVEOF13-ROBOT)
)
:effect
(and
(Flag149-)
(Flag149prime-)
)

)
(:action Flag150Action
:parameters ()
:precondition
(and
(ABOVEOF17-ROBOT)
(BELOWOF7-ROBOT)
)
:effect
(and
(Flag150-)
(Flag150prime-)
)

)
(:action Flag151Action
:parameters ()
:precondition
(and
(ABOVEOF8-ROBOT)
(BELOWOF7-ROBOT)
)
:effect
(and
(Flag151-)
(Flag151prime-)
)

)
(:action Flag152Action
:parameters ()
:precondition
(and
(ABOVEOF11-ROBOT)
(BELOWOF7-ROBOT)
)
:effect
(and
(Flag152-)
(Flag152prime-)
)

)
(:action Flag153Action
:parameters ()
:precondition
(and
(ABOVEOF9-ROBOT)
(BELOWOF7-ROBOT)
)
:effect
(and
(Flag153-)
(Flag153prime-)
)

)
(:action Flag154Action
:parameters ()
:precondition
(and
(ABOVEOF6-ROBOT)
(BELOWOF6-ROBOT)
)
:effect
(and
(Flag154-)
(Flag154prime-)
)

)
(:action Flag155Action
:parameters ()
:precondition
(and
(ABOVEOF12-ROBOT)
(BELOWOF7-ROBOT)
)
:effect
(and
(Flag155-)
(Flag155prime-)
)

)
(:action Prim142Action-0
:parameters ()
:precondition
(and
(not (ABOVEOF6-ROBOT))
)
:effect
(and
(Flag142prime-)
(not (Flag142-))
)

)
(:action Prim142Action-1
:parameters ()
:precondition
(and
(not (BELOWOF7-ROBOT))
)
:effect
(and
(Flag142prime-)
(not (Flag142-))
)

)
(:action Prim143Action-0
:parameters ()
:precondition
(and
(not (BELOWOF7-ROBOT))
)
:effect
(and
(Flag143prime-)
(not (Flag143-))
)

)
(:action Prim143Action-1
:parameters ()
:precondition
(and
(not (ABOVEOF14-ROBOT))
)
:effect
(and
(Flag143prime-)
(not (Flag143-))
)

)
(:action Prim144Action-0
:parameters ()
:precondition
(and
(not (BELOWOF7-ROBOT))
)
:effect
(and
(Flag144prime-)
(not (Flag144-))
)

)
(:action Prim144Action-1
:parameters ()
:precondition
(and
(not (ABOVEOF15-ROBOT))
)
:effect
(and
(Flag144prime-)
(not (Flag144-))
)

)
(:action Prim145Action-0
:parameters ()
:precondition
(and
(not (ABOVEOF7-ROBOT))
)
:effect
(and
(Flag145prime-)
(not (Flag145-))
)

)
(:action Prim145Action-1
:parameters ()
:precondition
(and
(not (BELOWOF7-ROBOT))
)
:effect
(and
(Flag145prime-)
(not (Flag145-))
)

)
(:action Prim146Action-0
:parameters ()
:precondition
(and
(not (ABOVEOF16-ROBOT))
)
:effect
(and
(Flag146prime-)
(not (Flag146-))
)

)
(:action Prim146Action-1
:parameters ()
:precondition
(and
(not (BELOWOF7-ROBOT))
)
:effect
(and
(Flag146prime-)
(not (Flag146-))
)

)
(:action Prim147Action-0
:parameters ()
:precondition
(and
(not (ABOVEOF10-ROBOT))
)
:effect
(and
(Flag147prime-)
(not (Flag147-))
)

)
(:action Prim147Action-1
:parameters ()
:precondition
(and
(not (BELOWOF6-ROBOT))
)
:effect
(and
(Flag147prime-)
(not (Flag147-))
)

)
(:action Prim148Action-0
:parameters ()
:precondition
(and
(not (ABOVEOF10-ROBOT))
)
:effect
(and
(Flag148prime-)
(not (Flag148-))
)

)
(:action Prim148Action-1
:parameters ()
:precondition
(and
(not (BELOWOF7-ROBOT))
)
:effect
(and
(Flag148prime-)
(not (Flag148-))
)

)
(:action Prim149Action-0
:parameters ()
:precondition
(and
(not (BELOWOF7-ROBOT))
)
:effect
(and
(Flag149prime-)
(not (Flag149-))
)

)
(:action Prim149Action-1
:parameters ()
:precondition
(and
(not (ABOVEOF13-ROBOT))
)
:effect
(and
(Flag149prime-)
(not (Flag149-))
)

)
(:action Prim150Action-0
:parameters ()
:precondition
(and
(not (ABOVEOF17-ROBOT))
)
:effect
(and
(Flag150prime-)
(not (Flag150-))
)

)
(:action Prim150Action-1
:parameters ()
:precondition
(and
(not (BELOWOF7-ROBOT))
)
:effect
(and
(Flag150prime-)
(not (Flag150-))
)

)
(:action Prim151Action-0
:parameters ()
:precondition
(and
(not (ABOVEOF8-ROBOT))
)
:effect
(and
(Flag151prime-)
(not (Flag151-))
)

)
(:action Prim151Action-1
:parameters ()
:precondition
(and
(not (BELOWOF7-ROBOT))
)
:effect
(and
(Flag151prime-)
(not (Flag151-))
)

)
(:action Prim152Action-0
:parameters ()
:precondition
(and
(not (ABOVEOF11-ROBOT))
)
:effect
(and
(Flag152prime-)
(not (Flag152-))
)

)
(:action Prim152Action-1
:parameters ()
:precondition
(and
(not (BELOWOF7-ROBOT))
)
:effect
(and
(Flag152prime-)
(not (Flag152-))
)

)
(:action Prim153Action-0
:parameters ()
:precondition
(and
(not (ABOVEOF9-ROBOT))
)
:effect
(and
(Flag153prime-)
(not (Flag153-))
)

)
(:action Prim153Action-1
:parameters ()
:precondition
(and
(not (BELOWOF7-ROBOT))
)
:effect
(and
(Flag153prime-)
(not (Flag153-))
)

)
(:action Prim154Action-0
:parameters ()
:precondition
(and
(not (ABOVEOF6-ROBOT))
)
:effect
(and
(Flag154prime-)
(not (Flag154-))
)

)
(:action Prim154Action-1
:parameters ()
:precondition
(and
(not (BELOWOF6-ROBOT))
)
:effect
(and
(Flag154prime-)
(not (Flag154-))
)

)
(:action Prim155Action-0
:parameters ()
:precondition
(and
(not (ABOVEOF12-ROBOT))
)
:effect
(and
(Flag155prime-)
(not (Flag155-))
)

)
(:action Prim155Action-1
:parameters ()
:precondition
(and
(not (BELOWOF7-ROBOT))
)
:effect
(and
(Flag155prime-)
(not (Flag155-))
)

)
(:action Flag157Action
:parameters ()
:precondition
(and
(BELOWOF8-ROBOT)
(ABOVEOF11-ROBOT)
)
:effect
(and
(Flag157-)
(Flag157prime-)
)

)
(:action Flag158Action
:parameters ()
:precondition
(and
(BELOWOF8-ROBOT)
(ABOVEOF14-ROBOT)
)
:effect
(and
(Flag158-)
(Flag158prime-)
)

)
(:action Flag159Action
:parameters ()
:precondition
(and
(BELOWOF8-ROBOT)
(ABOVEOF17-ROBOT)
)
:effect
(and
(Flag159-)
(Flag159prime-)
)

)
(:action Flag160Action
:parameters ()
:precondition
(and
(BELOWOF8-ROBOT)
(ABOVEOF13-ROBOT)
)
:effect
(and
(Flag160-)
(Flag160prime-)
)

)
(:action Flag161Action
:parameters ()
:precondition
(and
(BELOWOF8-ROBOT)
(ABOVEOF10-ROBOT)
)
:effect
(and
(Flag161-)
(Flag161prime-)
)

)
(:action Flag162Action
:parameters ()
:precondition
(and
(BELOWOF8-ROBOT)
(ABOVEOF9-ROBOT)
)
:effect
(and
(Flag162-)
(Flag162prime-)
)

)
(:action Flag163Action
:parameters ()
:precondition
(and
(BELOWOF8-ROBOT)
(ABOVEOF7-ROBOT)
)
:effect
(and
(Flag163-)
(Flag163prime-)
)

)
(:action Flag164Action
:parameters ()
:precondition
(and
(ABOVEOF8-ROBOT)
(BELOWOF8-ROBOT)
)
:effect
(and
(Flag164-)
(Flag164prime-)
)

)
(:action Flag165Action
:parameters ()
:precondition
(and
(BELOWOF7-ROBOT)
(ABOVEOF7-ROBOT)
)
:effect
(and
(Flag165-)
(Flag165prime-)
)

)
(:action Flag166Action
:parameters ()
:precondition
(and
(BELOWOF8-ROBOT)
(ABOVEOF15-ROBOT)
)
:effect
(and
(Flag166-)
(Flag166prime-)
)

)
(:action Flag167Action
:parameters ()
:precondition
(and
(ABOVEOF12-ROBOT)
(BELOWOF8-ROBOT)
)
:effect
(and
(Flag167-)
(Flag167prime-)
)

)
(:action Flag168Action
:parameters ()
:precondition
(and
(BELOWOF8-ROBOT)
(ABOVEOF16-ROBOT)
)
:effect
(and
(Flag168-)
(Flag168prime-)
)

)
(:action Prim157Action-0
:parameters ()
:precondition
(and
(not (BELOWOF8-ROBOT))
)
:effect
(and
(Flag157prime-)
(not (Flag157-))
)

)
(:action Prim157Action-1
:parameters ()
:precondition
(and
(not (ABOVEOF11-ROBOT))
)
:effect
(and
(Flag157prime-)
(not (Flag157-))
)

)
(:action Prim158Action-0
:parameters ()
:precondition
(and
(not (BELOWOF8-ROBOT))
)
:effect
(and
(Flag158prime-)
(not (Flag158-))
)

)
(:action Prim158Action-1
:parameters ()
:precondition
(and
(not (ABOVEOF14-ROBOT))
)
:effect
(and
(Flag158prime-)
(not (Flag158-))
)

)
(:action Prim159Action-0
:parameters ()
:precondition
(and
(not (BELOWOF8-ROBOT))
)
:effect
(and
(Flag159prime-)
(not (Flag159-))
)

)
(:action Prim159Action-1
:parameters ()
:precondition
(and
(not (ABOVEOF17-ROBOT))
)
:effect
(and
(Flag159prime-)
(not (Flag159-))
)

)
(:action Prim160Action-0
:parameters ()
:precondition
(and
(not (BELOWOF8-ROBOT))
)
:effect
(and
(Flag160prime-)
(not (Flag160-))
)

)
(:action Prim160Action-1
:parameters ()
:precondition
(and
(not (ABOVEOF13-ROBOT))
)
:effect
(and
(Flag160prime-)
(not (Flag160-))
)

)
(:action Prim161Action-0
:parameters ()
:precondition
(and
(not (BELOWOF8-ROBOT))
)
:effect
(and
(Flag161prime-)
(not (Flag161-))
)

)
(:action Prim161Action-1
:parameters ()
:precondition
(and
(not (ABOVEOF10-ROBOT))
)
:effect
(and
(Flag161prime-)
(not (Flag161-))
)

)
(:action Prim162Action-0
:parameters ()
:precondition
(and
(not (BELOWOF8-ROBOT))
)
:effect
(and
(Flag162prime-)
(not (Flag162-))
)

)
(:action Prim162Action-1
:parameters ()
:precondition
(and
(not (ABOVEOF9-ROBOT))
)
:effect
(and
(Flag162prime-)
(not (Flag162-))
)

)
(:action Prim163Action-0
:parameters ()
:precondition
(and
(not (BELOWOF8-ROBOT))
)
:effect
(and
(Flag163prime-)
(not (Flag163-))
)

)
(:action Prim163Action-1
:parameters ()
:precondition
(and
(not (ABOVEOF7-ROBOT))
)
:effect
(and
(Flag163prime-)
(not (Flag163-))
)

)
(:action Prim164Action-0
:parameters ()
:precondition
(and
(not (ABOVEOF8-ROBOT))
)
:effect
(and
(Flag164prime-)
(not (Flag164-))
)

)
(:action Prim164Action-1
:parameters ()
:precondition
(and
(not (BELOWOF8-ROBOT))
)
:effect
(and
(Flag164prime-)
(not (Flag164-))
)

)
(:action Prim165Action-0
:parameters ()
:precondition
(and
(not (BELOWOF7-ROBOT))
)
:effect
(and
(Flag165prime-)
(not (Flag165-))
)

)
(:action Prim165Action-1
:parameters ()
:precondition
(and
(not (ABOVEOF7-ROBOT))
)
:effect
(and
(Flag165prime-)
(not (Flag165-))
)

)
(:action Prim166Action-0
:parameters ()
:precondition
(and
(not (BELOWOF8-ROBOT))
)
:effect
(and
(Flag166prime-)
(not (Flag166-))
)

)
(:action Prim166Action-1
:parameters ()
:precondition
(and
(not (ABOVEOF15-ROBOT))
)
:effect
(and
(Flag166prime-)
(not (Flag166-))
)

)
(:action Prim167Action-0
:parameters ()
:precondition
(and
(not (ABOVEOF12-ROBOT))
)
:effect
(and
(Flag167prime-)
(not (Flag167-))
)

)
(:action Prim167Action-1
:parameters ()
:precondition
(and
(not (BELOWOF8-ROBOT))
)
:effect
(and
(Flag167prime-)
(not (Flag167-))
)

)
(:action Prim168Action-0
:parameters ()
:precondition
(and
(not (BELOWOF8-ROBOT))
)
:effect
(and
(Flag168prime-)
(not (Flag168-))
)

)
(:action Prim168Action-1
:parameters ()
:precondition
(and
(not (ABOVEOF16-ROBOT))
)
:effect
(and
(Flag168prime-)
(not (Flag168-))
)

)
(:action Flag170Action
:parameters ()
:precondition
(and
(ABOVEOF17-ROBOT)
(BELOWOF9-ROBOT)
)
:effect
(and
(Flag170-)
(Flag170prime-)
)

)
(:action Flag171Action
:parameters ()
:precondition
(and
(BELOWOF9-ROBOT)
(ABOVEOF13-ROBOT)
)
:effect
(and
(Flag171-)
(Flag171prime-)
)

)
(:action Flag172Action
:parameters ()
:precondition
(and
(ABOVEOF11-ROBOT)
(BELOWOF9-ROBOT)
)
:effect
(and
(Flag172-)
(Flag172prime-)
)

)
(:action Flag173Action
:parameters ()
:precondition
(and
(BELOWOF8-ROBOT)
(ABOVEOF12-ROBOT)
)
:effect
(and
(Flag173-)
(Flag173prime-)
)

)
(:action Flag174Action
:parameters ()
:precondition
(and
(ABOVEOF10-ROBOT)
(BELOWOF9-ROBOT)
)
:effect
(and
(Flag174-)
(Flag174prime-)
)

)
(:action Flag175Action
:parameters ()
:precondition
(and
(BELOWOF9-ROBOT)
(ABOVEOF14-ROBOT)
)
:effect
(and
(Flag175-)
(Flag175prime-)
)

)
(:action Flag176Action
:parameters ()
:precondition
(and
(BELOWOF9-ROBOT)
(ABOVEOF9-ROBOT)
)
:effect
(and
(Flag176-)
(Flag176prime-)
)

)
(:action Flag177Action
:parameters ()
:precondition
(and
(BELOWOF9-ROBOT)
(ABOVEOF16-ROBOT)
)
:effect
(and
(Flag177-)
(Flag177prime-)
)

)
(:action Flag178Action
:parameters ()
:precondition
(and
(BELOWOF8-ROBOT)
(ABOVEOF8-ROBOT)
)
:effect
(and
(Flag178-)
(Flag178prime-)
)

)
(:action Flag179Action
:parameters ()
:precondition
(and
(BELOWOF9-ROBOT)
(ABOVEOF15-ROBOT)
)
:effect
(and
(Flag179-)
(Flag179prime-)
)

)
(:action Flag180Action
:parameters ()
:precondition
(and
(ABOVEOF8-ROBOT)
(BELOWOF9-ROBOT)
)
:effect
(and
(Flag180-)
(Flag180prime-)
)

)
(:action Flag181Action
:parameters ()
:precondition
(and
(ABOVEOF12-ROBOT)
(BELOWOF9-ROBOT)
)
:effect
(and
(Flag181-)
(Flag181prime-)
)

)
(:action Prim170Action-0
:parameters ()
:precondition
(and
(not (ABOVEOF17-ROBOT))
)
:effect
(and
(Flag170prime-)
(not (Flag170-))
)

)
(:action Prim170Action-1
:parameters ()
:precondition
(and
(not (BELOWOF9-ROBOT))
)
:effect
(and
(Flag170prime-)
(not (Flag170-))
)

)
(:action Prim171Action-0
:parameters ()
:precondition
(and
(not (BELOWOF9-ROBOT))
)
:effect
(and
(Flag171prime-)
(not (Flag171-))
)

)
(:action Prim171Action-1
:parameters ()
:precondition
(and
(not (ABOVEOF13-ROBOT))
)
:effect
(and
(Flag171prime-)
(not (Flag171-))
)

)
(:action Prim172Action-0
:parameters ()
:precondition
(and
(not (ABOVEOF11-ROBOT))
)
:effect
(and
(Flag172prime-)
(not (Flag172-))
)

)
(:action Prim172Action-1
:parameters ()
:precondition
(and
(not (BELOWOF9-ROBOT))
)
:effect
(and
(Flag172prime-)
(not (Flag172-))
)

)
(:action Prim173Action-0
:parameters ()
:precondition
(and
(not (BELOWOF8-ROBOT))
)
:effect
(and
(Flag173prime-)
(not (Flag173-))
)

)
(:action Prim173Action-1
:parameters ()
:precondition
(and
(not (ABOVEOF12-ROBOT))
)
:effect
(and
(Flag173prime-)
(not (Flag173-))
)

)
(:action Prim174Action-0
:parameters ()
:precondition
(and
(not (ABOVEOF10-ROBOT))
)
:effect
(and
(Flag174prime-)
(not (Flag174-))
)

)
(:action Prim174Action-1
:parameters ()
:precondition
(and
(not (BELOWOF9-ROBOT))
)
:effect
(and
(Flag174prime-)
(not (Flag174-))
)

)
(:action Prim175Action-0
:parameters ()
:precondition
(and
(not (BELOWOF9-ROBOT))
)
:effect
(and
(Flag175prime-)
(not (Flag175-))
)

)
(:action Prim175Action-1
:parameters ()
:precondition
(and
(not (ABOVEOF14-ROBOT))
)
:effect
(and
(Flag175prime-)
(not (Flag175-))
)

)
(:action Prim176Action-0
:parameters ()
:precondition
(and
(not (BELOWOF9-ROBOT))
)
:effect
(and
(Flag176prime-)
(not (Flag176-))
)

)
(:action Prim176Action-1
:parameters ()
:precondition
(and
(not (ABOVEOF9-ROBOT))
)
:effect
(and
(Flag176prime-)
(not (Flag176-))
)

)
(:action Prim177Action-0
:parameters ()
:precondition
(and
(not (BELOWOF9-ROBOT))
)
:effect
(and
(Flag177prime-)
(not (Flag177-))
)

)
(:action Prim177Action-1
:parameters ()
:precondition
(and
(not (ABOVEOF16-ROBOT))
)
:effect
(and
(Flag177prime-)
(not (Flag177-))
)

)
(:action Prim178Action-0
:parameters ()
:precondition
(and
(not (BELOWOF8-ROBOT))
)
:effect
(and
(Flag178prime-)
(not (Flag178-))
)

)
(:action Prim178Action-1
:parameters ()
:precondition
(and
(not (ABOVEOF8-ROBOT))
)
:effect
(and
(Flag178prime-)
(not (Flag178-))
)

)
(:action Prim179Action-0
:parameters ()
:precondition
(and
(not (BELOWOF9-ROBOT))
)
:effect
(and
(Flag179prime-)
(not (Flag179-))
)

)
(:action Prim179Action-1
:parameters ()
:precondition
(and
(not (ABOVEOF15-ROBOT))
)
:effect
(and
(Flag179prime-)
(not (Flag179-))
)

)
(:action Prim180Action-0
:parameters ()
:precondition
(and
(not (ABOVEOF8-ROBOT))
)
:effect
(and
(Flag180prime-)
(not (Flag180-))
)

)
(:action Prim180Action-1
:parameters ()
:precondition
(and
(not (BELOWOF9-ROBOT))
)
:effect
(and
(Flag180prime-)
(not (Flag180-))
)

)
(:action Prim181Action-0
:parameters ()
:precondition
(and
(not (ABOVEOF12-ROBOT))
)
:effect
(and
(Flag181prime-)
(not (Flag181-))
)

)
(:action Prim181Action-1
:parameters ()
:precondition
(and
(not (BELOWOF9-ROBOT))
)
:effect
(and
(Flag181prime-)
(not (Flag181-))
)

)
(:action Flag183Action
:parameters ()
:precondition
(and
(BELOWOF10-ROBOT)
(ABOVEOF10-ROBOT)
)
:effect
(and
(Flag183-)
(Flag183prime-)
)

)
(:action Flag184Action
:parameters ()
:precondition
(and
(ABOVEOF12-ROBOT)
(BELOWOF10-ROBOT)
)
:effect
(and
(Flag184-)
(Flag184prime-)
)

)
(:action Flag185Action
:parameters ()
:precondition
(and
(BELOWOF10-ROBOT)
(ABOVEOF14-ROBOT)
)
:effect
(and
(Flag185-)
(Flag185prime-)
)

)
(:action Flag186Action
:parameters ()
:precondition
(and
(ABOVEOF9-ROBOT)
(BELOWOF9-ROBOT)
)
:effect
(and
(Flag186-)
(Flag186prime-)
)

)
(:action Flag187Action
:parameters ()
:precondition
(and
(BELOWOF10-ROBOT)
(ABOVEOF13-ROBOT)
)
:effect
(and
(Flag187-)
(Flag187prime-)
)

)
(:action Flag188Action
:parameters ()
:precondition
(and
(BELOWOF10-ROBOT)
(ABOVEOF16-ROBOT)
)
:effect
(and
(Flag188-)
(Flag188prime-)
)

)
(:action Flag189Action
:parameters ()
:precondition
(and
(BELOWOF10-ROBOT)
(ABOVEOF9-ROBOT)
)
:effect
(and
(Flag189-)
(Flag189prime-)
)

)
(:action Flag190Action
:parameters ()
:precondition
(and
(BELOWOF10-ROBOT)
(ABOVEOF15-ROBOT)
)
:effect
(and
(Flag190-)
(Flag190prime-)
)

)
(:action Flag191Action
:parameters ()
:precondition
(and
(BELOWOF10-ROBOT)
(ABOVEOF17-ROBOT)
)
:effect
(and
(Flag191-)
(Flag191prime-)
)

)
(:action Flag192Action
:parameters ()
:precondition
(and
(ABOVEOF11-ROBOT)
(BELOWOF10-ROBOT)
)
:effect
(and
(Flag192-)
(Flag192prime-)
)

)
(:action Prim183Action-0
:parameters ()
:precondition
(and
(not (BELOWOF10-ROBOT))
)
:effect
(and
(Flag183prime-)
(not (Flag183-))
)

)
(:action Prim183Action-1
:parameters ()
:precondition
(and
(not (ABOVEOF10-ROBOT))
)
:effect
(and
(Flag183prime-)
(not (Flag183-))
)

)
(:action Prim184Action-0
:parameters ()
:precondition
(and
(not (ABOVEOF12-ROBOT))
)
:effect
(and
(Flag184prime-)
(not (Flag184-))
)

)
(:action Prim184Action-1
:parameters ()
:precondition
(and
(not (BELOWOF10-ROBOT))
)
:effect
(and
(Flag184prime-)
(not (Flag184-))
)

)
(:action Prim185Action-0
:parameters ()
:precondition
(and
(not (BELOWOF10-ROBOT))
)
:effect
(and
(Flag185prime-)
(not (Flag185-))
)

)
(:action Prim185Action-1
:parameters ()
:precondition
(and
(not (ABOVEOF14-ROBOT))
)
:effect
(and
(Flag185prime-)
(not (Flag185-))
)

)
(:action Prim186Action-0
:parameters ()
:precondition
(and
(not (ABOVEOF9-ROBOT))
)
:effect
(and
(Flag186prime-)
(not (Flag186-))
)

)
(:action Prim186Action-1
:parameters ()
:precondition
(and
(not (BELOWOF9-ROBOT))
)
:effect
(and
(Flag186prime-)
(not (Flag186-))
)

)
(:action Prim187Action-0
:parameters ()
:precondition
(and
(not (BELOWOF10-ROBOT))
)
:effect
(and
(Flag187prime-)
(not (Flag187-))
)

)
(:action Prim187Action-1
:parameters ()
:precondition
(and
(not (ABOVEOF13-ROBOT))
)
:effect
(and
(Flag187prime-)
(not (Flag187-))
)

)
(:action Prim188Action-0
:parameters ()
:precondition
(and
(not (BELOWOF10-ROBOT))
)
:effect
(and
(Flag188prime-)
(not (Flag188-))
)

)
(:action Prim188Action-1
:parameters ()
:precondition
(and
(not (ABOVEOF16-ROBOT))
)
:effect
(and
(Flag188prime-)
(not (Flag188-))
)

)
(:action Prim189Action-0
:parameters ()
:precondition
(and
(not (BELOWOF10-ROBOT))
)
:effect
(and
(Flag189prime-)
(not (Flag189-))
)

)
(:action Prim189Action-1
:parameters ()
:precondition
(and
(not (ABOVEOF9-ROBOT))
)
:effect
(and
(Flag189prime-)
(not (Flag189-))
)

)
(:action Prim190Action-0
:parameters ()
:precondition
(and
(not (BELOWOF10-ROBOT))
)
:effect
(and
(Flag190prime-)
(not (Flag190-))
)

)
(:action Prim190Action-1
:parameters ()
:precondition
(and
(not (ABOVEOF15-ROBOT))
)
:effect
(and
(Flag190prime-)
(not (Flag190-))
)

)
(:action Prim191Action-0
:parameters ()
:precondition
(and
(not (BELOWOF10-ROBOT))
)
:effect
(and
(Flag191prime-)
(not (Flag191-))
)

)
(:action Prim191Action-1
:parameters ()
:precondition
(and
(not (ABOVEOF17-ROBOT))
)
:effect
(and
(Flag191prime-)
(not (Flag191-))
)

)
(:action Prim192Action-0
:parameters ()
:precondition
(and
(not (ABOVEOF11-ROBOT))
)
:effect
(and
(Flag192prime-)
(not (Flag192-))
)

)
(:action Prim192Action-1
:parameters ()
:precondition
(and
(not (BELOWOF10-ROBOT))
)
:effect
(and
(Flag192prime-)
(not (Flag192-))
)

)
(:action Flag194Action
:parameters ()
:precondition
(and
(BELOWOF11-ROBOT)
(ABOVEOF16-ROBOT)
)
:effect
(and
(Flag194-)
(Flag194prime-)
)

)
(:action Flag195Action
:parameters ()
:precondition
(and
(BELOWOF11-ROBOT)
(ABOVEOF15-ROBOT)
)
:effect
(and
(Flag195-)
(Flag195prime-)
)

)
(:action Flag196Action
:parameters ()
:precondition
(and
(BELOWOF11-ROBOT)
(ABOVEOF17-ROBOT)
)
:effect
(and
(Flag196-)
(Flag196prime-)
)

)
(:action Flag197Action
:parameters ()
:precondition
(and
(BELOWOF11-ROBOT)
(ABOVEOF13-ROBOT)
)
:effect
(and
(Flag197-)
(Flag197prime-)
)

)
(:action Flag198Action
:parameters ()
:precondition
(and
(BELOWOF11-ROBOT)
(ABOVEOF14-ROBOT)
)
:effect
(and
(Flag198-)
(Flag198prime-)
)

)
(:action Flag199Action
:parameters ()
:precondition
(and
(BELOWOF11-ROBOT)
(ABOVEOF10-ROBOT)
)
:effect
(and
(Flag199-)
(Flag199prime-)
)

)
(:action Flag200Action
:parameters ()
:precondition
(and
(ABOVEOF11-ROBOT)
(BELOWOF11-ROBOT)
)
:effect
(and
(Flag200-)
(Flag200prime-)
)

)
(:action Flag201Action
:parameters ()
:precondition
(and
(ABOVEOF12-ROBOT)
(BELOWOF11-ROBOT)
)
:effect
(and
(Flag201-)
(Flag201prime-)
)

)
(:action Prim194Action-0
:parameters ()
:precondition
(and
(not (BELOWOF11-ROBOT))
)
:effect
(and
(Flag194prime-)
(not (Flag194-))
)

)
(:action Prim194Action-1
:parameters ()
:precondition
(and
(not (ABOVEOF16-ROBOT))
)
:effect
(and
(Flag194prime-)
(not (Flag194-))
)

)
(:action Prim195Action-0
:parameters ()
:precondition
(and
(not (BELOWOF11-ROBOT))
)
:effect
(and
(Flag195prime-)
(not (Flag195-))
)

)
(:action Prim195Action-1
:parameters ()
:precondition
(and
(not (ABOVEOF15-ROBOT))
)
:effect
(and
(Flag195prime-)
(not (Flag195-))
)

)
(:action Prim196Action-0
:parameters ()
:precondition
(and
(not (BELOWOF11-ROBOT))
)
:effect
(and
(Flag196prime-)
(not (Flag196-))
)

)
(:action Prim196Action-1
:parameters ()
:precondition
(and
(not (ABOVEOF17-ROBOT))
)
:effect
(and
(Flag196prime-)
(not (Flag196-))
)

)
(:action Prim197Action-0
:parameters ()
:precondition
(and
(not (BELOWOF11-ROBOT))
)
:effect
(and
(Flag197prime-)
(not (Flag197-))
)

)
(:action Prim197Action-1
:parameters ()
:precondition
(and
(not (ABOVEOF13-ROBOT))
)
:effect
(and
(Flag197prime-)
(not (Flag197-))
)

)
(:action Prim198Action-0
:parameters ()
:precondition
(and
(not (BELOWOF11-ROBOT))
)
:effect
(and
(Flag198prime-)
(not (Flag198-))
)

)
(:action Prim198Action-1
:parameters ()
:precondition
(and
(not (ABOVEOF14-ROBOT))
)
:effect
(and
(Flag198prime-)
(not (Flag198-))
)

)
(:action Prim199Action-0
:parameters ()
:precondition
(and
(not (BELOWOF11-ROBOT))
)
:effect
(and
(Flag199prime-)
(not (Flag199-))
)

)
(:action Prim199Action-1
:parameters ()
:precondition
(and
(not (ABOVEOF10-ROBOT))
)
:effect
(and
(Flag199prime-)
(not (Flag199-))
)

)
(:action Prim200Action-0
:parameters ()
:precondition
(and
(not (ABOVEOF11-ROBOT))
)
:effect
(and
(Flag200prime-)
(not (Flag200-))
)

)
(:action Prim200Action-1
:parameters ()
:precondition
(and
(not (BELOWOF11-ROBOT))
)
:effect
(and
(Flag200prime-)
(not (Flag200-))
)

)
(:action Prim201Action-0
:parameters ()
:precondition
(and
(not (ABOVEOF12-ROBOT))
)
:effect
(and
(Flag201prime-)
(not (Flag201-))
)

)
(:action Prim201Action-1
:parameters ()
:precondition
(and
(not (BELOWOF11-ROBOT))
)
:effect
(and
(Flag201prime-)
(not (Flag201-))
)

)
(:action Flag203Action
:parameters ()
:precondition
(and
(BELOWOF12-ROBOT)
(ABOVEOF13-ROBOT)
)
:effect
(and
(Flag203-)
(Flag203prime-)
)

)
(:action Flag204Action
:parameters ()
:precondition
(and
(BELOWOF12-ROBOT)
(ABOVEOF15-ROBOT)
)
:effect
(and
(Flag204-)
(Flag204prime-)
)

)
(:action Flag205Action
:parameters ()
:precondition
(and
(BELOWOF12-ROBOT)
(ABOVEOF17-ROBOT)
)
:effect
(and
(Flag205-)
(Flag205prime-)
)

)
(:action Flag206Action
:parameters ()
:precondition
(and
(BELOWOF12-ROBOT)
(ABOVEOF11-ROBOT)
)
:effect
(and
(Flag206-)
(Flag206prime-)
)

)
(:action Flag207Action
:parameters ()
:precondition
(and
(BELOWOF12-ROBOT)
(ABOVEOF14-ROBOT)
)
:effect
(and
(Flag207-)
(Flag207prime-)
)

)
(:action Flag208Action
:parameters ()
:precondition
(and
(ABOVEOF12-ROBOT)
(BELOWOF12-ROBOT)
)
:effect
(and
(Flag208-)
(Flag208prime-)
)

)
(:action Flag209Action
:parameters ()
:precondition
(and
(BELOWOF12-ROBOT)
(ABOVEOF16-ROBOT)
)
:effect
(and
(Flag209-)
(Flag209prime-)
)

)
(:action Prim203Action-0
:parameters ()
:precondition
(and
(not (BELOWOF12-ROBOT))
)
:effect
(and
(Flag203prime-)
(not (Flag203-))
)

)
(:action Prim203Action-1
:parameters ()
:precondition
(and
(not (ABOVEOF13-ROBOT))
)
:effect
(and
(Flag203prime-)
(not (Flag203-))
)

)
(:action Prim204Action-0
:parameters ()
:precondition
(and
(not (BELOWOF12-ROBOT))
)
:effect
(and
(Flag204prime-)
(not (Flag204-))
)

)
(:action Prim204Action-1
:parameters ()
:precondition
(and
(not (ABOVEOF15-ROBOT))
)
:effect
(and
(Flag204prime-)
(not (Flag204-))
)

)
(:action Prim205Action-0
:parameters ()
:precondition
(and
(not (BELOWOF12-ROBOT))
)
:effect
(and
(Flag205prime-)
(not (Flag205-))
)

)
(:action Prim205Action-1
:parameters ()
:precondition
(and
(not (ABOVEOF17-ROBOT))
)
:effect
(and
(Flag205prime-)
(not (Flag205-))
)

)
(:action Prim206Action-0
:parameters ()
:precondition
(and
(not (BELOWOF12-ROBOT))
)
:effect
(and
(Flag206prime-)
(not (Flag206-))
)

)
(:action Prim206Action-1
:parameters ()
:precondition
(and
(not (ABOVEOF11-ROBOT))
)
:effect
(and
(Flag206prime-)
(not (Flag206-))
)

)
(:action Prim207Action-0
:parameters ()
:precondition
(and
(not (BELOWOF12-ROBOT))
)
:effect
(and
(Flag207prime-)
(not (Flag207-))
)

)
(:action Prim207Action-1
:parameters ()
:precondition
(and
(not (ABOVEOF14-ROBOT))
)
:effect
(and
(Flag207prime-)
(not (Flag207-))
)

)
(:action Prim208Action-0
:parameters ()
:precondition
(and
(not (ABOVEOF12-ROBOT))
)
:effect
(and
(Flag208prime-)
(not (Flag208-))
)

)
(:action Prim208Action-1
:parameters ()
:precondition
(and
(not (BELOWOF12-ROBOT))
)
:effect
(and
(Flag208prime-)
(not (Flag208-))
)

)
(:action Prim209Action-0
:parameters ()
:precondition
(and
(not (BELOWOF12-ROBOT))
)
:effect
(and
(Flag209prime-)
(not (Flag209-))
)

)
(:action Prim209Action-1
:parameters ()
:precondition
(and
(not (ABOVEOF16-ROBOT))
)
:effect
(and
(Flag209prime-)
(not (Flag209-))
)

)
(:action Flag211Action
:parameters ()
:precondition
(and
(ABOVEOF14-ROBOT)
(BELOWOF13-ROBOT)
)
:effect
(and
(Flag211-)
(Flag211prime-)
)

)
(:action Flag212Action
:parameters ()
:precondition
(and
(BELOWOF12-ROBOT)
(ABOVEOF12-ROBOT)
)
:effect
(and
(Flag212-)
(Flag212prime-)
)

)
(:action Flag213Action
:parameters ()
:precondition
(and
(ABOVEOF13-ROBOT)
(BELOWOF13-ROBOT)
)
:effect
(and
(Flag213-)
(Flag213prime-)
)

)
(:action Flag214Action
:parameters ()
:precondition
(and
(ABOVEOF15-ROBOT)
(BELOWOF13-ROBOT)
)
:effect
(and
(Flag214-)
(Flag214prime-)
)

)
(:action Flag215Action
:parameters ()
:precondition
(and
(ABOVEOF12-ROBOT)
(BELOWOF13-ROBOT)
)
:effect
(and
(Flag215-)
(Flag215prime-)
)

)
(:action Flag216Action
:parameters ()
:precondition
(and
(ABOVEOF17-ROBOT)
(BELOWOF13-ROBOT)
)
:effect
(and
(Flag216-)
(Flag216prime-)
)

)
(:action Flag217Action
:parameters ()
:precondition
(and
(ABOVEOF16-ROBOT)
(BELOWOF13-ROBOT)
)
:effect
(and
(Flag217-)
(Flag217prime-)
)

)
(:action Prim211Action-0
:parameters ()
:precondition
(and
(not (ABOVEOF14-ROBOT))
)
:effect
(and
(Flag211prime-)
(not (Flag211-))
)

)
(:action Prim211Action-1
:parameters ()
:precondition
(and
(not (BELOWOF13-ROBOT))
)
:effect
(and
(Flag211prime-)
(not (Flag211-))
)

)
(:action Prim212Action-0
:parameters ()
:precondition
(and
(not (BELOWOF12-ROBOT))
)
:effect
(and
(Flag212prime-)
(not (Flag212-))
)

)
(:action Prim212Action-1
:parameters ()
:precondition
(and
(not (ABOVEOF12-ROBOT))
)
:effect
(and
(Flag212prime-)
(not (Flag212-))
)

)
(:action Prim213Action-0
:parameters ()
:precondition
(and
(not (ABOVEOF13-ROBOT))
)
:effect
(and
(Flag213prime-)
(not (Flag213-))
)

)
(:action Prim213Action-1
:parameters ()
:precondition
(and
(not (BELOWOF13-ROBOT))
)
:effect
(and
(Flag213prime-)
(not (Flag213-))
)

)
(:action Prim214Action-0
:parameters ()
:precondition
(and
(not (ABOVEOF15-ROBOT))
)
:effect
(and
(Flag214prime-)
(not (Flag214-))
)

)
(:action Prim214Action-1
:parameters ()
:precondition
(and
(not (BELOWOF13-ROBOT))
)
:effect
(and
(Flag214prime-)
(not (Flag214-))
)

)
(:action Prim215Action-0
:parameters ()
:precondition
(and
(not (ABOVEOF12-ROBOT))
)
:effect
(and
(Flag215prime-)
(not (Flag215-))
)

)
(:action Prim215Action-1
:parameters ()
:precondition
(and
(not (BELOWOF13-ROBOT))
)
:effect
(and
(Flag215prime-)
(not (Flag215-))
)

)
(:action Prim216Action-0
:parameters ()
:precondition
(and
(not (ABOVEOF17-ROBOT))
)
:effect
(and
(Flag216prime-)
(not (Flag216-))
)

)
(:action Prim216Action-1
:parameters ()
:precondition
(and
(not (BELOWOF13-ROBOT))
)
:effect
(and
(Flag216prime-)
(not (Flag216-))
)

)
(:action Prim217Action-0
:parameters ()
:precondition
(and
(not (ABOVEOF16-ROBOT))
)
:effect
(and
(Flag217prime-)
(not (Flag217-))
)

)
(:action Prim217Action-1
:parameters ()
:precondition
(and
(not (BELOWOF13-ROBOT))
)
:effect
(and
(Flag217prime-)
(not (Flag217-))
)

)
(:action Flag219Action
:parameters ()
:precondition
(and
(BELOWOF14-ROBOT)
(ABOVEOF14-ROBOT)
)
:effect
(and
(Flag219-)
(Flag219prime-)
)

)
(:action Flag220Action
:parameters ()
:precondition
(and
(ABOVEOF15-ROBOT)
(BELOWOF14-ROBOT)
)
:effect
(and
(Flag220-)
(Flag220prime-)
)

)
(:action Flag221Action
:parameters ()
:precondition
(and
(BELOWOF13-ROBOT)
(ABOVEOF13-ROBOT)
)
:effect
(and
(Flag221-)
(Flag221prime-)
)

)
(:action Flag222Action
:parameters ()
:precondition
(and
(ABOVEOF17-ROBOT)
(BELOWOF14-ROBOT)
)
:effect
(and
(Flag222-)
(Flag222prime-)
)

)
(:action Flag223Action
:parameters ()
:precondition
(and
(BELOWOF14-ROBOT)
(ABOVEOF13-ROBOT)
)
:effect
(and
(Flag223-)
(Flag223prime-)
)

)
(:action Flag224Action
:parameters ()
:precondition
(and
(ABOVEOF16-ROBOT)
(BELOWOF14-ROBOT)
)
:effect
(and
(Flag224-)
(Flag224prime-)
)

)
(:action Prim219Action-0
:parameters ()
:precondition
(and
(not (BELOWOF14-ROBOT))
)
:effect
(and
(Flag219prime-)
(not (Flag219-))
)

)
(:action Prim219Action-1
:parameters ()
:precondition
(and
(not (ABOVEOF14-ROBOT))
)
:effect
(and
(Flag219prime-)
(not (Flag219-))
)

)
(:action Prim220Action-0
:parameters ()
:precondition
(and
(not (ABOVEOF15-ROBOT))
)
:effect
(and
(Flag220prime-)
(not (Flag220-))
)

)
(:action Prim220Action-1
:parameters ()
:precondition
(and
(not (BELOWOF14-ROBOT))
)
:effect
(and
(Flag220prime-)
(not (Flag220-))
)

)
(:action Prim221Action-0
:parameters ()
:precondition
(and
(not (BELOWOF13-ROBOT))
)
:effect
(and
(Flag221prime-)
(not (Flag221-))
)

)
(:action Prim221Action-1
:parameters ()
:precondition
(and
(not (ABOVEOF13-ROBOT))
)
:effect
(and
(Flag221prime-)
(not (Flag221-))
)

)
(:action Prim222Action-0
:parameters ()
:precondition
(and
(not (ABOVEOF17-ROBOT))
)
:effect
(and
(Flag222prime-)
(not (Flag222-))
)

)
(:action Prim222Action-1
:parameters ()
:precondition
(and
(not (BELOWOF14-ROBOT))
)
:effect
(and
(Flag222prime-)
(not (Flag222-))
)

)
(:action Prim223Action-0
:parameters ()
:precondition
(and
(not (BELOWOF14-ROBOT))
)
:effect
(and
(Flag223prime-)
(not (Flag223-))
)

)
(:action Prim223Action-1
:parameters ()
:precondition
(and
(not (ABOVEOF13-ROBOT))
)
:effect
(and
(Flag223prime-)
(not (Flag223-))
)

)
(:action Prim224Action-0
:parameters ()
:precondition
(and
(not (ABOVEOF16-ROBOT))
)
:effect
(and
(Flag224prime-)
(not (Flag224-))
)

)
(:action Prim224Action-1
:parameters ()
:precondition
(and
(not (BELOWOF14-ROBOT))
)
:effect
(and
(Flag224prime-)
(not (Flag224-))
)

)
(:action Flag226Action
:parameters ()
:precondition
(and
(BELOWOF15-ROBOT)
(ABOVEOF14-ROBOT)
)
:effect
(and
(Flag226-)
(Flag226prime-)
)

)
(:action Flag227Action
:parameters ()
:precondition
(and
(BELOWOF15-ROBOT)
(ABOVEOF15-ROBOT)
)
:effect
(and
(Flag227-)
(Flag227prime-)
)

)
(:action Flag228Action
:parameters ()
:precondition
(and
(ABOVEOF16-ROBOT)
(BELOWOF15-ROBOT)
)
:effect
(and
(Flag228-)
(Flag228prime-)
)

)
(:action Flag229Action
:parameters ()
:precondition
(and
(ABOVEOF17-ROBOT)
(BELOWOF15-ROBOT)
)
:effect
(and
(Flag229-)
(Flag229prime-)
)

)
(:action Prim226Action-0
:parameters ()
:precondition
(and
(not (BELOWOF15-ROBOT))
)
:effect
(and
(Flag226prime-)
(not (Flag226-))
)

)
(:action Prim226Action-1
:parameters ()
:precondition
(and
(not (ABOVEOF14-ROBOT))
)
:effect
(and
(Flag226prime-)
(not (Flag226-))
)

)
(:action Prim227Action-0
:parameters ()
:precondition
(and
(not (BELOWOF15-ROBOT))
)
:effect
(and
(Flag227prime-)
(not (Flag227-))
)

)
(:action Prim227Action-1
:parameters ()
:precondition
(and
(not (ABOVEOF15-ROBOT))
)
:effect
(and
(Flag227prime-)
(not (Flag227-))
)

)
(:action Prim228Action-0
:parameters ()
:precondition
(and
(not (ABOVEOF16-ROBOT))
)
:effect
(and
(Flag228prime-)
(not (Flag228-))
)

)
(:action Prim228Action-1
:parameters ()
:precondition
(and
(not (BELOWOF15-ROBOT))
)
:effect
(and
(Flag228prime-)
(not (Flag228-))
)

)
(:action Prim229Action-0
:parameters ()
:precondition
(and
(not (ABOVEOF17-ROBOT))
)
:effect
(and
(Flag229prime-)
(not (Flag229-))
)

)
(:action Prim229Action-1
:parameters ()
:precondition
(and
(not (BELOWOF15-ROBOT))
)
:effect
(and
(Flag229prime-)
(not (Flag229-))
)

)
(:action Flag231Action
:parameters ()
:precondition
(and
(ABOVEOF17-ROBOT)
(BELOWOF16-ROBOT)
)
:effect
(and
(Flag231-)
(Flag231prime-)
)

)
(:action Flag232Action
:parameters ()
:precondition
(and
(ABOVEOF16-ROBOT)
(BELOWOF16-ROBOT)
)
:effect
(and
(Flag232-)
(Flag232prime-)
)

)
(:action Flag233Action
:parameters ()
:precondition
(and
(BELOWOF16-ROBOT)
(ABOVEOF15-ROBOT)
)
:effect
(and
(Flag233-)
(Flag233prime-)
)

)
(:action Prim231Action-0
:parameters ()
:precondition
(and
(not (ABOVEOF17-ROBOT))
)
:effect
(and
(Flag231prime-)
(not (Flag231-))
)

)
(:action Prim231Action-1
:parameters ()
:precondition
(and
(not (BELOWOF16-ROBOT))
)
:effect
(and
(Flag231prime-)
(not (Flag231-))
)

)
(:action Prim232Action-0
:parameters ()
:precondition
(and
(not (ABOVEOF16-ROBOT))
)
:effect
(and
(Flag232prime-)
(not (Flag232-))
)

)
(:action Prim232Action-1
:parameters ()
:precondition
(and
(not (BELOWOF16-ROBOT))
)
:effect
(and
(Flag232prime-)
(not (Flag232-))
)

)
(:action Prim233Action-0
:parameters ()
:precondition
(and
(not (BELOWOF16-ROBOT))
)
:effect
(and
(Flag233prime-)
(not (Flag233-))
)

)
(:action Prim233Action-1
:parameters ()
:precondition
(and
(not (ABOVEOF15-ROBOT))
)
:effect
(and
(Flag233prime-)
(not (Flag233-))
)

)
(:action Flag235Action
:parameters ()
:precondition
(and
(BELOWOF17-ROBOT)
(ABOVEOF16-ROBOT)
)
:effect
(and
(Flag235-)
(Flag235prime-)
)

)
(:action Flag236Action
:parameters ()
:precondition
(and
(ABOVEOF17-ROBOT)
(BELOWOF17-ROBOT)
)
:effect
(and
(Flag236-)
(Flag236prime-)
)

)
(:action Prim235Action-1
:parameters ()
:precondition
(and
(not (ABOVEOF16-ROBOT))
)
:effect
(and
(Flag235prime-)
(not (Flag235-))
)

)
(:action Prim236Action-0
:parameters ()
:precondition
(and
(not (ABOVEOF17-ROBOT))
)
:effect
(and
(Flag236prime-)
(not (Flag236-))
)

)
(:action Flag238Action
:parameters ()
:precondition
(and
(BELOWOF18-ROBOT)
(ABOVEOF17-ROBOT)
)
:effect
(and
(Flag238-)
(Flag238prime-)
)

)
(:action Prim238Action-0
:parameters ()
:precondition
(and
(not (BELOWOF18-ROBOT))
)
:effect
(and
(Flag238prime-)
(not (Flag238-))
)

)
(:action Prim238Action-1
:parameters ()
:precondition
(and
(not (ABOVEOF17-ROBOT))
)
:effect
(and
(Flag238prime-)
(not (Flag238-))
)

)
(:action Flag472Action-12
:parameters ()
:precondition
(and
(BELOWOF1-ROBOT)
)
:effect
(and
(Flag472-)
(Flag472prime-)
)

)
(:action MOVEUP-ROBOT
:parameters ()
:precondition
(and
(Flag4-)
(Flag4prime-)
(Flag237prime-)
(Flag234prime-)
(Flag230prime-)
(Flag225prime-)
(Flag218prime-)
(Flag210prime-)
(Flag202prime-)
(Flag193prime-)
(Flag182prime-)
(Flag169prime-)
(Flag156prime-)
(Flag141prime-)
(Flag125prime-)
(Flag109prime-)
(Flag91prime-)
(Flag73prime-)
(Flag472prime-)
)
:effect
(and
(when
(and
(Flag237-)
)
(and
(ROW17-ROBOT)
(not (NOT-ROW17-ROBOT))
)
)
(when
(and
(Flag234-)
)
(and
(ROW16-ROBOT)
(not (NOT-ROW16-ROBOT))
)
)
(when
(and
(Flag230-)
)
(and
(ROW15-ROBOT)
(not (NOT-ROW15-ROBOT))
)
)
(when
(and
(Flag225-)
)
(and
(ROW14-ROBOT)
(not (NOT-ROW14-ROBOT))
)
)
(when
(and
(Flag218-)
)
(and
(ROW13-ROBOT)
(not (NOT-ROW13-ROBOT))
)
)
(when
(and
(Flag210-)
)
(and
(ROW12-ROBOT)
(not (NOT-ROW12-ROBOT))
)
)
(when
(and
(Flag202-)
)
(and
(ROW11-ROBOT)
(not (NOT-ROW11-ROBOT))
)
)
(when
(and
(Flag193-)
)
(and
(ROW10-ROBOT)
(not (NOT-ROW10-ROBOT))
)
)
(when
(and
(Flag182-)
)
(and
(ROW9-ROBOT)
(not (NOT-ROW9-ROBOT))
)
)
(when
(and
(Flag169-)
)
(and
(ROW8-ROBOT)
(not (NOT-ROW8-ROBOT))
)
)
(when
(and
(Flag156-)
)
(and
(ROW7-ROBOT)
(not (NOT-ROW7-ROBOT))
)
)
(when
(and
(Flag141-)
)
(and
(ROW6-ROBOT)
(not (NOT-ROW6-ROBOT))
)
)
(when
(and
(Flag125-)
)
(and
(ROW5-ROBOT)
(not (NOT-ROW5-ROBOT))
)
)
(when
(and
(Flag109-)
)
(and
(ROW4-ROBOT)
(not (NOT-ROW4-ROBOT))
)
)
(when
(and
(Flag91-)
)
(and
(ROW3-ROBOT)
(not (NOT-ROW3-ROBOT))
)
)
(when
(and
(Flag73-)
)
(and
(ROW2-ROBOT)
(not (NOT-ROW2-ROBOT))
)
)
(when
(and
(Flag472-)
)
(and
(ROW1-ROBOT)
(not (NOT-ROW1-ROBOT))
)
)
(when
(and
(ROW16-ROBOT)
)
(and
(ROW17-ROBOT)
(NOT-ROW16-ROBOT)
(not (NOT-ROW17-ROBOT))
(not (ROW16-ROBOT))
)
)
(when
(and
(ROW15-ROBOT)
)
(and
(ROW16-ROBOT)
(NOT-ROW15-ROBOT)
(not (NOT-ROW16-ROBOT))
(not (ROW15-ROBOT))
)
)
(when
(and
(ROW14-ROBOT)
)
(and
(ROW15-ROBOT)
(NOT-ROW14-ROBOT)
(not (NOT-ROW15-ROBOT))
(not (ROW14-ROBOT))
)
)
(when
(and
(ROW13-ROBOT)
)
(and
(ROW14-ROBOT)
(NOT-ROW13-ROBOT)
(not (NOT-ROW14-ROBOT))
(not (ROW13-ROBOT))
)
)
(when
(and
(ROW12-ROBOT)
)
(and
(ROW13-ROBOT)
(NOT-ROW12-ROBOT)
(not (NOT-ROW13-ROBOT))
(not (ROW12-ROBOT))
)
)
(when
(and
(ROW11-ROBOT)
)
(and
(ROW12-ROBOT)
(NOT-ROW11-ROBOT)
(not (NOT-ROW12-ROBOT))
(not (ROW11-ROBOT))
)
)
(when
(and
(ROW10-ROBOT)
)
(and
(ROW11-ROBOT)
(NOT-ROW10-ROBOT)
(not (NOT-ROW11-ROBOT))
(not (ROW10-ROBOT))
)
)
(when
(and
(ROW9-ROBOT)
)
(and
(ROW10-ROBOT)
(NOT-ROW9-ROBOT)
(not (NOT-ROW10-ROBOT))
(not (ROW9-ROBOT))
)
)
(when
(and
(ROW8-ROBOT)
)
(and
(ROW9-ROBOT)
(NOT-ROW8-ROBOT)
(not (NOT-ROW9-ROBOT))
(not (ROW8-ROBOT))
)
)
(when
(and
(ROW7-ROBOT)
)
(and
(ROW8-ROBOT)
(NOT-ROW7-ROBOT)
(not (NOT-ROW8-ROBOT))
(not (ROW7-ROBOT))
)
)
(when
(and
(ROW6-ROBOT)
)
(and
(ROW7-ROBOT)
(NOT-ROW6-ROBOT)
(not (NOT-ROW7-ROBOT))
(not (ROW6-ROBOT))
)
)
(when
(and
(ROW5-ROBOT)
)
(and
(ROW6-ROBOT)
(NOT-ROW5-ROBOT)
(not (NOT-ROW6-ROBOT))
(not (ROW5-ROBOT))
)
)
(when
(and
(ROW4-ROBOT)
)
(and
(ROW5-ROBOT)
(NOT-ROW4-ROBOT)
(not (NOT-ROW5-ROBOT))
(not (ROW4-ROBOT))
)
)
(when
(and
(ROW3-ROBOT)
)
(and
(ROW4-ROBOT)
(NOT-ROW3-ROBOT)
(not (NOT-ROW4-ROBOT))
(not (ROW3-ROBOT))
)
)
(when
(and
(ROW2-ROBOT)
)
(and
(ROW3-ROBOT)
(NOT-ROW2-ROBOT)
(not (NOT-ROW3-ROBOT))
(not (ROW2-ROBOT))
)
)
(when
(and
(ROW1-ROBOT)
)
(and
(ROW2-ROBOT)
(NOT-ROW1-ROBOT)
(not (NOT-ROW2-ROBOT))
(not (ROW1-ROBOT))
)
)
(when
(and
(ROW0-ROBOT)
)
(and
(ROW1-ROBOT)
(NOT-ROW0-ROBOT)
(not (NOT-ROW1-ROBOT))
(not (ROW0-ROBOT))
)
)
(when
(and
(BELOWOF14-ROBOT)
)
(and
(BELOWOF18-ROBOT)
(NOT-BELOWOF17-ROBOT)
(BELOWOF17-ROBOT)
(NOT-BELOWOF16-ROBOT)
(BELOWOF16-ROBOT)
(NOT-BELOWOF15-ROBOT)
(BELOWOF15-ROBOT)
(NOT-BELOWOF14-ROBOT)
(not (NOT-BELOWOF18-ROBOT))
(not (BELOWOF14-ROBOT))
)
)
(when
(and
(BELOWOF8-ROBOT)
)
(and
(BELOWOF18-ROBOT)
(NOT-BELOWOF17-ROBOT)
(BELOWOF17-ROBOT)
(NOT-BELOWOF16-ROBOT)
(BELOWOF16-ROBOT)
(NOT-BELOWOF15-ROBOT)
(BELOWOF15-ROBOT)
(NOT-BELOWOF14-ROBOT)
(BELOWOF14-ROBOT)
(NOT-BELOWOF13-ROBOT)
(BELOWOF13-ROBOT)
(NOT-BELOWOF12-ROBOT)
(BELOWOF12-ROBOT)
(NOT-BELOWOF11-ROBOT)
(BELOWOF11-ROBOT)
(NOT-BELOWOF10-ROBOT)
(BELOWOF10-ROBOT)
(NOT-BELOWOF9-ROBOT)
(BELOWOF9-ROBOT)
(NOT-BELOWOF8-ROBOT)
(not (NOT-BELOWOF18-ROBOT))
(not (BELOWOF8-ROBOT))
)
)
(when
(and
(BELOWOF13-ROBOT)
)
(and
(BELOWOF18-ROBOT)
(NOT-BELOWOF17-ROBOT)
(BELOWOF17-ROBOT)
(NOT-BELOWOF16-ROBOT)
(BELOWOF16-ROBOT)
(NOT-BELOWOF15-ROBOT)
(BELOWOF15-ROBOT)
(NOT-BELOWOF14-ROBOT)
(BELOWOF14-ROBOT)
(NOT-BELOWOF13-ROBOT)
(not (NOT-BELOWOF18-ROBOT))
(not (BELOWOF13-ROBOT))
)
)
(when
(and
(BELOWOF3-ROBOT)
)
(and
(BELOWOF18-ROBOT)
(NOT-BELOWOF17-ROBOT)
(BELOWOF17-ROBOT)
(NOT-BELOWOF16-ROBOT)
(BELOWOF16-ROBOT)
(NOT-BELOWOF15-ROBOT)
(BELOWOF15-ROBOT)
(NOT-BELOWOF14-ROBOT)
(BELOWOF14-ROBOT)
(NOT-BELOWOF13-ROBOT)
(BELOWOF13-ROBOT)
(NOT-BELOWOF12-ROBOT)
(BELOWOF12-ROBOT)
(NOT-BELOWOF11-ROBOT)
(BELOWOF11-ROBOT)
(NOT-BELOWOF10-ROBOT)
(BELOWOF10-ROBOT)
(NOT-BELOWOF9-ROBOT)
(BELOWOF9-ROBOT)
(NOT-BELOWOF8-ROBOT)
(BELOWOF8-ROBOT)
(NOT-BELOWOF7-ROBOT)
(BELOWOF7-ROBOT)
(NOT-BELOWOF6-ROBOT)
(BELOWOF6-ROBOT)
(NOT-BELOWOF5-ROBOT)
(BELOWOF5-ROBOT)
(NOT-BELOWOF4-ROBOT)
(BELOWOF4-ROBOT)
(NOT-BELOWOF3-ROBOT)
(not (NOT-BELOWOF18-ROBOT))
(not (BELOWOF3-ROBOT))
)
)
(when
(and
(BELOWOF9-ROBOT)
)
(and
(BELOWOF18-ROBOT)
(NOT-BELOWOF17-ROBOT)
(BELOWOF17-ROBOT)
(NOT-BELOWOF16-ROBOT)
(BELOWOF16-ROBOT)
(NOT-BELOWOF15-ROBOT)
(BELOWOF15-ROBOT)
(NOT-BELOWOF14-ROBOT)
(BELOWOF14-ROBOT)
(NOT-BELOWOF13-ROBOT)
(BELOWOF13-ROBOT)
(NOT-BELOWOF12-ROBOT)
(BELOWOF12-ROBOT)
(NOT-BELOWOF11-ROBOT)
(BELOWOF11-ROBOT)
(NOT-BELOWOF10-ROBOT)
(BELOWOF10-ROBOT)
(NOT-BELOWOF9-ROBOT)
(not (NOT-BELOWOF18-ROBOT))
(not (BELOWOF9-ROBOT))
)
)
(when
(and
(BELOWOF7-ROBOT)
)
(and
(BELOWOF18-ROBOT)
(NOT-BELOWOF17-ROBOT)
(BELOWOF17-ROBOT)
(NOT-BELOWOF16-ROBOT)
(BELOWOF16-ROBOT)
(NOT-BELOWOF15-ROBOT)
(BELOWOF15-ROBOT)
(NOT-BELOWOF14-ROBOT)
(BELOWOF14-ROBOT)
(NOT-BELOWOF13-ROBOT)
(BELOWOF13-ROBOT)
(NOT-BELOWOF12-ROBOT)
(BELOWOF12-ROBOT)
(NOT-BELOWOF11-ROBOT)
(BELOWOF11-ROBOT)
(NOT-BELOWOF10-ROBOT)
(BELOWOF10-ROBOT)
(NOT-BELOWOF9-ROBOT)
(BELOWOF9-ROBOT)
(NOT-BELOWOF8-ROBOT)
(BELOWOF8-ROBOT)
(NOT-BELOWOF7-ROBOT)
(not (NOT-BELOWOF18-ROBOT))
(not (BELOWOF7-ROBOT))
)
)
(when
(and
(BELOWOF12-ROBOT)
)
(and
(BELOWOF18-ROBOT)
(NOT-BELOWOF17-ROBOT)
(BELOWOF17-ROBOT)
(NOT-BELOWOF16-ROBOT)
(BELOWOF16-ROBOT)
(NOT-BELOWOF15-ROBOT)
(BELOWOF15-ROBOT)
(NOT-BELOWOF14-ROBOT)
(BELOWOF14-ROBOT)
(NOT-BELOWOF13-ROBOT)
(BELOWOF13-ROBOT)
(NOT-BELOWOF12-ROBOT)
(not (NOT-BELOWOF18-ROBOT))
(not (BELOWOF12-ROBOT))
)
)
(when
(and
(BELOWOF11-ROBOT)
)
(and
(BELOWOF18-ROBOT)
(NOT-BELOWOF17-ROBOT)
(BELOWOF17-ROBOT)
(NOT-BELOWOF16-ROBOT)
(BELOWOF16-ROBOT)
(NOT-BELOWOF15-ROBOT)
(BELOWOF15-ROBOT)
(NOT-BELOWOF14-ROBOT)
(BELOWOF14-ROBOT)
(NOT-BELOWOF13-ROBOT)
(BELOWOF13-ROBOT)
(NOT-BELOWOF12-ROBOT)
(BELOWOF12-ROBOT)
(NOT-BELOWOF11-ROBOT)
(not (NOT-BELOWOF18-ROBOT))
(not (BELOWOF11-ROBOT))
)
)
(when
(and
(BELOWOF4-ROBOT)
)
(and
(BELOWOF18-ROBOT)
(NOT-BELOWOF17-ROBOT)
(BELOWOF17-ROBOT)
(NOT-BELOWOF16-ROBOT)
(BELOWOF16-ROBOT)
(NOT-BELOWOF15-ROBOT)
(BELOWOF15-ROBOT)
(NOT-BELOWOF14-ROBOT)
(BELOWOF14-ROBOT)
(NOT-BELOWOF13-ROBOT)
(BELOWOF13-ROBOT)
(NOT-BELOWOF12-ROBOT)
(BELOWOF12-ROBOT)
(NOT-BELOWOF11-ROBOT)
(BELOWOF11-ROBOT)
(NOT-BELOWOF10-ROBOT)
(BELOWOF10-ROBOT)
(NOT-BELOWOF9-ROBOT)
(BELOWOF9-ROBOT)
(NOT-BELOWOF8-ROBOT)
(BELOWOF8-ROBOT)
(NOT-BELOWOF7-ROBOT)
(BELOWOF7-ROBOT)
(NOT-BELOWOF6-ROBOT)
(BELOWOF6-ROBOT)
(NOT-BELOWOF5-ROBOT)
(BELOWOF5-ROBOT)
(NOT-BELOWOF4-ROBOT)
(not (NOT-BELOWOF18-ROBOT))
(not (BELOWOF4-ROBOT))
)
)
(when
(and
(BELOWOF1-ROBOT)
)
(and
(BELOWOF18-ROBOT)
(NOT-BELOWOF17-ROBOT)
(BELOWOF17-ROBOT)
(NOT-BELOWOF16-ROBOT)
(BELOWOF16-ROBOT)
(NOT-BELOWOF15-ROBOT)
(BELOWOF15-ROBOT)
(NOT-BELOWOF14-ROBOT)
(BELOWOF14-ROBOT)
(NOT-BELOWOF13-ROBOT)
(BELOWOF13-ROBOT)
(NOT-BELOWOF12-ROBOT)
(BELOWOF12-ROBOT)
(NOT-BELOWOF11-ROBOT)
(BELOWOF11-ROBOT)
(NOT-BELOWOF10-ROBOT)
(BELOWOF10-ROBOT)
(NOT-BELOWOF9-ROBOT)
(BELOWOF9-ROBOT)
(NOT-BELOWOF8-ROBOT)
(BELOWOF8-ROBOT)
(NOT-BELOWOF7-ROBOT)
(BELOWOF7-ROBOT)
(NOT-BELOWOF6-ROBOT)
(BELOWOF6-ROBOT)
(NOT-BELOWOF5-ROBOT)
(BELOWOF5-ROBOT)
(NOT-BELOWOF4-ROBOT)
(BELOWOF4-ROBOT)
(NOT-BELOWOF3-ROBOT)
(BELOWOF3-ROBOT)
(NOT-BELOWOF2-ROBOT)
(BELOWOF2-ROBOT)
(NOT-BELOWOF1-ROBOT)
(not (NOT-BELOWOF18-ROBOT))
(not (BELOWOF1-ROBOT))
)
)
(when
(and
(BELOWOF2-ROBOT)
)
(and
(BELOWOF18-ROBOT)
(NOT-BELOWOF17-ROBOT)
(BELOWOF17-ROBOT)
(NOT-BELOWOF16-ROBOT)
(BELOWOF16-ROBOT)
(NOT-BELOWOF15-ROBOT)
(BELOWOF15-ROBOT)
(NOT-BELOWOF14-ROBOT)
(BELOWOF14-ROBOT)
(NOT-BELOWOF13-ROBOT)
(BELOWOF13-ROBOT)
(NOT-BELOWOF12-ROBOT)
(BELOWOF12-ROBOT)
(NOT-BELOWOF11-ROBOT)
(BELOWOF11-ROBOT)
(NOT-BELOWOF10-ROBOT)
(BELOWOF10-ROBOT)
(NOT-BELOWOF9-ROBOT)
(BELOWOF9-ROBOT)
(NOT-BELOWOF8-ROBOT)
(BELOWOF8-ROBOT)
(NOT-BELOWOF7-ROBOT)
(BELOWOF7-ROBOT)
(NOT-BELOWOF6-ROBOT)
(BELOWOF6-ROBOT)
(NOT-BELOWOF5-ROBOT)
(BELOWOF5-ROBOT)
(NOT-BELOWOF4-ROBOT)
(BELOWOF4-ROBOT)
(NOT-BELOWOF3-ROBOT)
(BELOWOF3-ROBOT)
(NOT-BELOWOF2-ROBOT)
(not (NOT-BELOWOF18-ROBOT))
(not (BELOWOF2-ROBOT))
)
)
(when
(and
(BELOWOF5-ROBOT)
)
(and
(BELOWOF18-ROBOT)
(NOT-BELOWOF17-ROBOT)
(BELOWOF17-ROBOT)
(NOT-BELOWOF16-ROBOT)
(BELOWOF16-ROBOT)
(NOT-BELOWOF15-ROBOT)
(BELOWOF15-ROBOT)
(NOT-BELOWOF14-ROBOT)
(BELOWOF14-ROBOT)
(NOT-BELOWOF13-ROBOT)
(BELOWOF13-ROBOT)
(NOT-BELOWOF12-ROBOT)
(BELOWOF12-ROBOT)
(NOT-BELOWOF11-ROBOT)
(BELOWOF11-ROBOT)
(NOT-BELOWOF10-ROBOT)
(BELOWOF10-ROBOT)
(NOT-BELOWOF9-ROBOT)
(BELOWOF9-ROBOT)
(NOT-BELOWOF8-ROBOT)
(BELOWOF8-ROBOT)
(NOT-BELOWOF7-ROBOT)
(BELOWOF7-ROBOT)
(NOT-BELOWOF6-ROBOT)
(BELOWOF6-ROBOT)
(NOT-BELOWOF5-ROBOT)
(not (NOT-BELOWOF18-ROBOT))
(not (BELOWOF5-ROBOT))
)
)
(when
(and
(BELOWOF17-ROBOT)
)
(and
(BELOWOF18-ROBOT)
(NOT-BELOWOF17-ROBOT)
(not (NOT-BELOWOF18-ROBOT))
(not (BELOWOF17-ROBOT))
)
)
(when
(and
(BELOWOF6-ROBOT)
)
(and
(BELOWOF18-ROBOT)
(NOT-BELOWOF17-ROBOT)
(BELOWOF17-ROBOT)
(NOT-BELOWOF16-ROBOT)
(BELOWOF16-ROBOT)
(NOT-BELOWOF15-ROBOT)
(BELOWOF15-ROBOT)
(NOT-BELOWOF14-ROBOT)
(BELOWOF14-ROBOT)
(NOT-BELOWOF13-ROBOT)
(BELOWOF13-ROBOT)
(NOT-BELOWOF12-ROBOT)
(BELOWOF12-ROBOT)
(NOT-BELOWOF11-ROBOT)
(BELOWOF11-ROBOT)
(NOT-BELOWOF10-ROBOT)
(BELOWOF10-ROBOT)
(NOT-BELOWOF9-ROBOT)
(BELOWOF9-ROBOT)
(NOT-BELOWOF8-ROBOT)
(BELOWOF8-ROBOT)
(NOT-BELOWOF7-ROBOT)
(BELOWOF7-ROBOT)
(NOT-BELOWOF6-ROBOT)
(not (NOT-BELOWOF18-ROBOT))
(not (BELOWOF6-ROBOT))
)
)
(when
(and
(BELOWOF15-ROBOT)
)
(and
(BELOWOF18-ROBOT)
(NOT-BELOWOF17-ROBOT)
(BELOWOF17-ROBOT)
(NOT-BELOWOF16-ROBOT)
(BELOWOF16-ROBOT)
(NOT-BELOWOF15-ROBOT)
(not (NOT-BELOWOF18-ROBOT))
(not (BELOWOF15-ROBOT))
)
)
(when
(and
(BELOWOF16-ROBOT)
)
(and
(BELOWOF18-ROBOT)
(NOT-BELOWOF17-ROBOT)
(BELOWOF17-ROBOT)
(NOT-BELOWOF16-ROBOT)
(not (NOT-BELOWOF18-ROBOT))
(not (BELOWOF16-ROBOT))
)
)
(when
(and
(BELOWOF10-ROBOT)
)
(and
(BELOWOF18-ROBOT)
(NOT-BELOWOF17-ROBOT)
(BELOWOF17-ROBOT)
(NOT-BELOWOF16-ROBOT)
(BELOWOF16-ROBOT)
(NOT-BELOWOF15-ROBOT)
(BELOWOF15-ROBOT)
(NOT-BELOWOF14-ROBOT)
(BELOWOF14-ROBOT)
(NOT-BELOWOF13-ROBOT)
(BELOWOF13-ROBOT)
(NOT-BELOWOF12-ROBOT)
(BELOWOF12-ROBOT)
(NOT-BELOWOF11-ROBOT)
(BELOWOF11-ROBOT)
(NOT-BELOWOF10-ROBOT)
(not (NOT-BELOWOF18-ROBOT))
(not (BELOWOF10-ROBOT))
)
)
(when
(and
(ABOVEOF16-ROBOT)
)
(and
(ABOVEOF17-ROBOT)
(ABOVEOF15-ROBOT)
(ABOVEOF14-ROBOT)
(ABOVEOF13-ROBOT)
(ABOVEOF12-ROBOT)
(ABOVEOF11-ROBOT)
(ABOVEOF10-ROBOT)
(ABOVEOF9-ROBOT)
(ABOVEOF8-ROBOT)
(ABOVEOF7-ROBOT)
(ABOVEOF6-ROBOT)
(ABOVEOF5-ROBOT)
(ABOVEOF4-ROBOT)
(ABOVEOF3-ROBOT)
(ABOVEOF2-ROBOT)
(not (NOT-ABOVEOF17-ROBOT))
(not (NOT-ABOVEOF16-ROBOT))
(not (NOT-ABOVEOF15-ROBOT))
(not (NOT-ABOVEOF14-ROBOT))
(not (NOT-ABOVEOF13-ROBOT))
(not (NOT-ABOVEOF12-ROBOT))
(not (NOT-ABOVEOF11-ROBOT))
(not (NOT-ABOVEOF10-ROBOT))
(not (NOT-ABOVEOF9-ROBOT))
(not (NOT-ABOVEOF8-ROBOT))
(not (NOT-ABOVEOF7-ROBOT))
(not (NOT-ABOVEOF6-ROBOT))
(not (NOT-ABOVEOF5-ROBOT))
(not (NOT-ABOVEOF4-ROBOT))
(not (NOT-ABOVEOF3-ROBOT))
(not (NOT-ABOVEOF2-ROBOT))
)
)
(when
(and
(ABOVEOF17-ROBOT)
)
(and
(ABOVEOF16-ROBOT)
(ABOVEOF15-ROBOT)
(ABOVEOF14-ROBOT)
(ABOVEOF13-ROBOT)
(ABOVEOF12-ROBOT)
(ABOVEOF11-ROBOT)
(ABOVEOF10-ROBOT)
(ABOVEOF9-ROBOT)
(ABOVEOF8-ROBOT)
(ABOVEOF7-ROBOT)
(ABOVEOF6-ROBOT)
(ABOVEOF5-ROBOT)
(ABOVEOF4-ROBOT)
(ABOVEOF3-ROBOT)
(ABOVEOF2-ROBOT)
(not (NOT-ABOVEOF17-ROBOT))
(not (NOT-ABOVEOF16-ROBOT))
(not (NOT-ABOVEOF15-ROBOT))
(not (NOT-ABOVEOF14-ROBOT))
(not (NOT-ABOVEOF13-ROBOT))
(not (NOT-ABOVEOF12-ROBOT))
(not (NOT-ABOVEOF11-ROBOT))
(not (NOT-ABOVEOF10-ROBOT))
(not (NOT-ABOVEOF9-ROBOT))
(not (NOT-ABOVEOF8-ROBOT))
(not (NOT-ABOVEOF7-ROBOT))
(not (NOT-ABOVEOF6-ROBOT))
(not (NOT-ABOVEOF5-ROBOT))
(not (NOT-ABOVEOF4-ROBOT))
(not (NOT-ABOVEOF3-ROBOT))
(not (NOT-ABOVEOF2-ROBOT))
)
)
(when
(and
(ABOVEOF15-ROBOT)
)
(and
(ABOVEOF16-ROBOT)
(ABOVEOF14-ROBOT)
(ABOVEOF13-ROBOT)
(ABOVEOF12-ROBOT)
(ABOVEOF11-ROBOT)
(ABOVEOF10-ROBOT)
(ABOVEOF9-ROBOT)
(ABOVEOF8-ROBOT)
(ABOVEOF7-ROBOT)
(ABOVEOF6-ROBOT)
(ABOVEOF5-ROBOT)
(ABOVEOF4-ROBOT)
(ABOVEOF3-ROBOT)
(ABOVEOF2-ROBOT)
(not (NOT-ABOVEOF16-ROBOT))
(not (NOT-ABOVEOF15-ROBOT))
(not (NOT-ABOVEOF14-ROBOT))
(not (NOT-ABOVEOF13-ROBOT))
(not (NOT-ABOVEOF12-ROBOT))
(not (NOT-ABOVEOF11-ROBOT))
(not (NOT-ABOVEOF10-ROBOT))
(not (NOT-ABOVEOF9-ROBOT))
(not (NOT-ABOVEOF8-ROBOT))
(not (NOT-ABOVEOF7-ROBOT))
(not (NOT-ABOVEOF6-ROBOT))
(not (NOT-ABOVEOF5-ROBOT))
(not (NOT-ABOVEOF4-ROBOT))
(not (NOT-ABOVEOF3-ROBOT))
(not (NOT-ABOVEOF2-ROBOT))
)
)
(when
(and
(ABOVEOF14-ROBOT)
)
(and
(ABOVEOF15-ROBOT)
(ABOVEOF13-ROBOT)
(ABOVEOF12-ROBOT)
(ABOVEOF11-ROBOT)
(ABOVEOF10-ROBOT)
(ABOVEOF9-ROBOT)
(ABOVEOF8-ROBOT)
(ABOVEOF7-ROBOT)
(ABOVEOF6-ROBOT)
(ABOVEOF5-ROBOT)
(ABOVEOF4-ROBOT)
(ABOVEOF3-ROBOT)
(ABOVEOF2-ROBOT)
(not (NOT-ABOVEOF15-ROBOT))
(not (NOT-ABOVEOF14-ROBOT))
(not (NOT-ABOVEOF13-ROBOT))
(not (NOT-ABOVEOF12-ROBOT))
(not (NOT-ABOVEOF11-ROBOT))
(not (NOT-ABOVEOF10-ROBOT))
(not (NOT-ABOVEOF9-ROBOT))
(not (NOT-ABOVEOF8-ROBOT))
(not (NOT-ABOVEOF7-ROBOT))
(not (NOT-ABOVEOF6-ROBOT))
(not (NOT-ABOVEOF5-ROBOT))
(not (NOT-ABOVEOF4-ROBOT))
(not (NOT-ABOVEOF3-ROBOT))
(not (NOT-ABOVEOF2-ROBOT))
)
)
(when
(and
(ABOVEOF13-ROBOT)
)
(and
(ABOVEOF14-ROBOT)
(ABOVEOF12-ROBOT)
(ABOVEOF11-ROBOT)
(ABOVEOF10-ROBOT)
(ABOVEOF9-ROBOT)
(ABOVEOF8-ROBOT)
(ABOVEOF7-ROBOT)
(ABOVEOF6-ROBOT)
(ABOVEOF5-ROBOT)
(ABOVEOF4-ROBOT)
(ABOVEOF3-ROBOT)
(ABOVEOF2-ROBOT)
(not (NOT-ABOVEOF14-ROBOT))
(not (NOT-ABOVEOF13-ROBOT))
(not (NOT-ABOVEOF12-ROBOT))
(not (NOT-ABOVEOF11-ROBOT))
(not (NOT-ABOVEOF10-ROBOT))
(not (NOT-ABOVEOF9-ROBOT))
(not (NOT-ABOVEOF8-ROBOT))
(not (NOT-ABOVEOF7-ROBOT))
(not (NOT-ABOVEOF6-ROBOT))
(not (NOT-ABOVEOF5-ROBOT))
(not (NOT-ABOVEOF4-ROBOT))
(not (NOT-ABOVEOF3-ROBOT))
(not (NOT-ABOVEOF2-ROBOT))
)
)
(when
(and
(ABOVEOF12-ROBOT)
)
(and
(ABOVEOF13-ROBOT)
(ABOVEOF11-ROBOT)
(ABOVEOF10-ROBOT)
(ABOVEOF9-ROBOT)
(ABOVEOF8-ROBOT)
(ABOVEOF7-ROBOT)
(ABOVEOF6-ROBOT)
(ABOVEOF5-ROBOT)
(ABOVEOF4-ROBOT)
(ABOVEOF3-ROBOT)
(ABOVEOF2-ROBOT)
(not (NOT-ABOVEOF13-ROBOT))
(not (NOT-ABOVEOF12-ROBOT))
(not (NOT-ABOVEOF11-ROBOT))
(not (NOT-ABOVEOF10-ROBOT))
(not (NOT-ABOVEOF9-ROBOT))
(not (NOT-ABOVEOF8-ROBOT))
(not (NOT-ABOVEOF7-ROBOT))
(not (NOT-ABOVEOF6-ROBOT))
(not (NOT-ABOVEOF5-ROBOT))
(not (NOT-ABOVEOF4-ROBOT))
(not (NOT-ABOVEOF3-ROBOT))
(not (NOT-ABOVEOF2-ROBOT))
)
)
(when
(and
(ABOVEOF11-ROBOT)
)
(and
(ABOVEOF12-ROBOT)
(ABOVEOF10-ROBOT)
(ABOVEOF9-ROBOT)
(ABOVEOF8-ROBOT)
(ABOVEOF7-ROBOT)
(ABOVEOF6-ROBOT)
(ABOVEOF5-ROBOT)
(ABOVEOF4-ROBOT)
(ABOVEOF3-ROBOT)
(ABOVEOF2-ROBOT)
(not (NOT-ABOVEOF12-ROBOT))
(not (NOT-ABOVEOF11-ROBOT))
(not (NOT-ABOVEOF10-ROBOT))
(not (NOT-ABOVEOF9-ROBOT))
(not (NOT-ABOVEOF8-ROBOT))
(not (NOT-ABOVEOF7-ROBOT))
(not (NOT-ABOVEOF6-ROBOT))
(not (NOT-ABOVEOF5-ROBOT))
(not (NOT-ABOVEOF4-ROBOT))
(not (NOT-ABOVEOF3-ROBOT))
(not (NOT-ABOVEOF2-ROBOT))
)
)
(when
(and
(ABOVEOF10-ROBOT)
)
(and
(ABOVEOF11-ROBOT)
(ABOVEOF9-ROBOT)
(ABOVEOF8-ROBOT)
(ABOVEOF7-ROBOT)
(ABOVEOF6-ROBOT)
(ABOVEOF5-ROBOT)
(ABOVEOF4-ROBOT)
(ABOVEOF3-ROBOT)
(ABOVEOF2-ROBOT)
(not (NOT-ABOVEOF11-ROBOT))
(not (NOT-ABOVEOF10-ROBOT))
(not (NOT-ABOVEOF9-ROBOT))
(not (NOT-ABOVEOF8-ROBOT))
(not (NOT-ABOVEOF7-ROBOT))
(not (NOT-ABOVEOF6-ROBOT))
(not (NOT-ABOVEOF5-ROBOT))
(not (NOT-ABOVEOF4-ROBOT))
(not (NOT-ABOVEOF3-ROBOT))
(not (NOT-ABOVEOF2-ROBOT))
)
)
(when
(and
(ABOVEOF9-ROBOT)
)
(and
(ABOVEOF10-ROBOT)
(ABOVEOF8-ROBOT)
(ABOVEOF7-ROBOT)
(ABOVEOF6-ROBOT)
(ABOVEOF5-ROBOT)
(ABOVEOF4-ROBOT)
(ABOVEOF3-ROBOT)
(ABOVEOF2-ROBOT)
(not (NOT-ABOVEOF10-ROBOT))
(not (NOT-ABOVEOF9-ROBOT))
(not (NOT-ABOVEOF8-ROBOT))
(not (NOT-ABOVEOF7-ROBOT))
(not (NOT-ABOVEOF6-ROBOT))
(not (NOT-ABOVEOF5-ROBOT))
(not (NOT-ABOVEOF4-ROBOT))
(not (NOT-ABOVEOF3-ROBOT))
(not (NOT-ABOVEOF2-ROBOT))
)
)
(when
(and
(ABOVEOF8-ROBOT)
)
(and
(ABOVEOF9-ROBOT)
(ABOVEOF7-ROBOT)
(ABOVEOF6-ROBOT)
(ABOVEOF5-ROBOT)
(ABOVEOF4-ROBOT)
(ABOVEOF3-ROBOT)
(ABOVEOF2-ROBOT)
(not (NOT-ABOVEOF9-ROBOT))
(not (NOT-ABOVEOF8-ROBOT))
(not (NOT-ABOVEOF7-ROBOT))
(not (NOT-ABOVEOF6-ROBOT))
(not (NOT-ABOVEOF5-ROBOT))
(not (NOT-ABOVEOF4-ROBOT))
(not (NOT-ABOVEOF3-ROBOT))
(not (NOT-ABOVEOF2-ROBOT))
)
)
(when
(and
(ABOVEOF7-ROBOT)
)
(and
(ABOVEOF8-ROBOT)
(ABOVEOF6-ROBOT)
(ABOVEOF5-ROBOT)
(ABOVEOF4-ROBOT)
(ABOVEOF3-ROBOT)
(ABOVEOF2-ROBOT)
(not (NOT-ABOVEOF8-ROBOT))
(not (NOT-ABOVEOF7-ROBOT))
(not (NOT-ABOVEOF6-ROBOT))
(not (NOT-ABOVEOF5-ROBOT))
(not (NOT-ABOVEOF4-ROBOT))
(not (NOT-ABOVEOF3-ROBOT))
(not (NOT-ABOVEOF2-ROBOT))
)
)
(when
(and
(ABOVEOF6-ROBOT)
)
(and
(ABOVEOF7-ROBOT)
(ABOVEOF5-ROBOT)
(ABOVEOF4-ROBOT)
(ABOVEOF3-ROBOT)
(ABOVEOF2-ROBOT)
(not (NOT-ABOVEOF7-ROBOT))
(not (NOT-ABOVEOF6-ROBOT))
(not (NOT-ABOVEOF5-ROBOT))
(not (NOT-ABOVEOF4-ROBOT))
(not (NOT-ABOVEOF3-ROBOT))
(not (NOT-ABOVEOF2-ROBOT))
)
)
(when
(and
(ABOVEOF5-ROBOT)
)
(and
(ABOVEOF6-ROBOT)
(ABOVEOF4-ROBOT)
(ABOVEOF3-ROBOT)
(ABOVEOF2-ROBOT)
(not (NOT-ABOVEOF6-ROBOT))
(not (NOT-ABOVEOF5-ROBOT))
(not (NOT-ABOVEOF4-ROBOT))
(not (NOT-ABOVEOF3-ROBOT))
(not (NOT-ABOVEOF2-ROBOT))
)
)
(when
(and
(ABOVEOF4-ROBOT)
)
(and
(ABOVEOF5-ROBOT)
(ABOVEOF3-ROBOT)
(ABOVEOF2-ROBOT)
(not (NOT-ABOVEOF5-ROBOT))
(not (NOT-ABOVEOF4-ROBOT))
(not (NOT-ABOVEOF3-ROBOT))
(not (NOT-ABOVEOF2-ROBOT))
)
)
(when
(and
(ABOVEOF3-ROBOT)
)
(and
(ABOVEOF4-ROBOT)
(ABOVEOF2-ROBOT)
(not (NOT-ABOVEOF4-ROBOT))
(not (NOT-ABOVEOF3-ROBOT))
(not (NOT-ABOVEOF2-ROBOT))
)
)
(when
(and
(ABOVEOF2-ROBOT)
)
(and
(ABOVEOF3-ROBOT)
(not (NOT-ABOVEOF3-ROBOT))
(not (NOT-ABOVEOF2-ROBOT))
)
)
(when
(and
(ABOVEOF1-ROBOT)
)
(and
(ABOVEOF2-ROBOT)
(not (NOT-ABOVEOF2-ROBOT))
)
)
(ABOVEOF1-ROBOT)
(CHECKCONSISTENCY-)
(not (NOT-ABOVEOF1-ROBOT))
(not (NOT-CHECKCONSISTENCY-))
(not (Flag473prime-))
(not (Flag258prime-))
(not (Flag257prime-))
(not (Flag256prime-))
(not (Flag255prime-))
(not (Flag21prime-))
(not (Flag20prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag472prime-))
(not (Flag238prime-))
(not (Flag236prime-))
(not (Flag235prime-))
(not (Flag233prime-))
(not (Flag232prime-))
(not (Flag231prime-))
(not (Flag229prime-))
(not (Flag228prime-))
(not (Flag227prime-))
(not (Flag226prime-))
(not (Flag224prime-))
(not (Flag223prime-))
(not (Flag222prime-))
(not (Flag221prime-))
(not (Flag220prime-))
(not (Flag219prime-))
(not (Flag217prime-))
(not (Flag216prime-))
(not (Flag215prime-))
(not (Flag214prime-))
(not (Flag213prime-))
(not (Flag212prime-))
(not (Flag211prime-))
(not (Flag209prime-))
(not (Flag208prime-))
(not (Flag207prime-))
(not (Flag206prime-))
(not (Flag205prime-))
(not (Flag204prime-))
(not (Flag203prime-))
(not (Flag201prime-))
(not (Flag200prime-))
(not (Flag199prime-))
(not (Flag198prime-))
(not (Flag197prime-))
(not (Flag196prime-))
(not (Flag195prime-))
(not (Flag194prime-))
(not (Flag192prime-))
(not (Flag191prime-))
(not (Flag190prime-))
(not (Flag189prime-))
(not (Flag188prime-))
(not (Flag187prime-))
(not (Flag186prime-))
(not (Flag185prime-))
(not (Flag184prime-))
(not (Flag183prime-))
(not (Flag181prime-))
(not (Flag180prime-))
(not (Flag179prime-))
(not (Flag178prime-))
(not (Flag177prime-))
(not (Flag176prime-))
(not (Flag175prime-))
(not (Flag174prime-))
(not (Flag173prime-))
(not (Flag172prime-))
(not (Flag171prime-))
(not (Flag170prime-))
(not (Flag168prime-))
(not (Flag167prime-))
(not (Flag166prime-))
(not (Flag165prime-))
(not (Flag164prime-))
(not (Flag163prime-))
(not (Flag162prime-))
(not (Flag161prime-))
(not (Flag160prime-))
(not (Flag159prime-))
(not (Flag158prime-))
(not (Flag157prime-))
(not (Flag155prime-))
(not (Flag154prime-))
(not (Flag153prime-))
(not (Flag152prime-))
(not (Flag151prime-))
(not (Flag150prime-))
(not (Flag149prime-))
(not (Flag148prime-))
(not (Flag147prime-))
(not (Flag146prime-))
(not (Flag145prime-))
(not (Flag144prime-))
(not (Flag143prime-))
(not (Flag142prime-))
(not (Flag140prime-))
(not (Flag139prime-))
(not (Flag138prime-))
(not (Flag137prime-))
(not (Flag136prime-))
(not (Flag135prime-))
(not (Flag134prime-))
(not (Flag133prime-))
(not (Flag132prime-))
(not (Flag131prime-))
(not (Flag130prime-))
(not (Flag129prime-))
(not (Flag128prime-))
(not (Flag127prime-))
(not (Flag126prime-))
(not (Flag124prime-))
(not (Flag123prime-))
(not (Flag122prime-))
(not (Flag121prime-))
(not (Flag120prime-))
(not (Flag119prime-))
(not (Flag118prime-))
(not (Flag117prime-))
(not (Flag116prime-))
(not (Flag115prime-))
(not (Flag114prime-))
(not (Flag113prime-))
(not (Flag112prime-))
(not (Flag111prime-))
(not (Flag110prime-))
(not (Flag108prime-))
(not (Flag107prime-))
(not (Flag106prime-))
(not (Flag105prime-))
(not (Flag104prime-))
(not (Flag103prime-))
(not (Flag102prime-))
(not (Flag101prime-))
(not (Flag100prime-))
(not (Flag99prime-))
(not (Flag98prime-))
(not (Flag97prime-))
(not (Flag96prime-))
(not (Flag95prime-))
(not (Flag94prime-))
(not (Flag93prime-))
(not (Flag92prime-))
(not (Flag90prime-))
(not (Flag89prime-))
(not (Flag88prime-))
(not (Flag87prime-))
(not (Flag86prime-))
(not (Flag85prime-))
(not (Flag84prime-))
(not (Flag83prime-))
(not (Flag82prime-))
(not (Flag81prime-))
(not (Flag80prime-))
(not (Flag79prime-))
(not (Flag78prime-))
(not (Flag77prime-))
(not (Flag76prime-))
(not (Flag75prime-))
(not (Flag74prime-))
(not (Flag72prime-))
(not (Flag71prime-))
(not (Flag70prime-))
(not (Flag69prime-))
(not (Flag68prime-))
(not (Flag67prime-))
(not (Flag66prime-))
(not (Flag65prime-))
(not (Flag64prime-))
(not (Flag63prime-))
(not (Flag62prime-))
(not (Flag61prime-))
(not (Flag60prime-))
(not (Flag59prime-))
(not (Flag58prime-))
(not (Flag57prime-))
(not (Flag56prime-))
(not (Flag55prime-))
(not (Flag54prime-))
(not (Flag53prime-))
(not (Flag52prime-))
(not (Flag51prime-))
(not (Flag50prime-))
(not (Flag49prime-))
(not (Flag48prime-))
(not (Flag47prime-))
(not (Flag46prime-))
(not (Flag45prime-))
(not (Flag44prime-))
(not (Flag43prime-))
(not (Flag42prime-))
(not (Flag41prime-))
(not (Flag40prime-))
(not (Flag39prime-))
(not (Flag38prime-))
(not (Flag37prime-))
(not (Flag36prime-))
(not (Flag35prime-))
(not (Flag34prime-))
(not (Flag33prime-))
(not (Flag32prime-))
(not (Flag31prime-))
(not (Flag30prime-))
(not (Flag29prime-))
(not (Flag28prime-))
(not (Flag27prime-))
(not (Flag26prime-))
(not (Flag25prime-))
(not (Flag24prime-))
(not (Flag23prime-))
(not (Flag22prime-))
(not (Flag19prime-))
(not (Flag18prime-))
(not (Flag17prime-))
(not (Flag16prime-))
(not (Flag15prime-))
(not (Flag14prime-))
(not (Flag13prime-))
(not (Flag12prime-))
(not (Flag11prime-))
(not (Flag10prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag474prime-))
(not (Flag470prime-))
(not (Flag468prime-))
(not (Flag467prime-))
(not (Flag465prime-))
(not (Flag464prime-))
(not (Flag463prime-))
(not (Flag462prime-))
(not (Flag460prime-))
(not (Flag459prime-))
(not (Flag458prime-))
(not (Flag457prime-))
(not (Flag455prime-))
(not (Flag454prime-))
(not (Flag453prime-))
(not (Flag452prime-))
(not (Flag451prime-))
(not (Flag449prime-))
(not (Flag448prime-))
(not (Flag447prime-))
(not (Flag446prime-))
(not (Flag445prime-))
(not (Flag444prime-))
(not (Flag443prime-))
(not (Flag441prime-))
(not (Flag440prime-))
(not (Flag439prime-))
(not (Flag438prime-))
(not (Flag437prime-))
(not (Flag436prime-))
(not (Flag435prime-))
(not (Flag433prime-))
(not (Flag432prime-))
(not (Flag431prime-))
(not (Flag430prime-))
(not (Flag429prime-))
(not (Flag428prime-))
(not (Flag427prime-))
(not (Flag426prime-))
(not (Flag425prime-))
(not (Flag423prime-))
(not (Flag422prime-))
(not (Flag421prime-))
(not (Flag420prime-))
(not (Flag419prime-))
(not (Flag418prime-))
(not (Flag417prime-))
(not (Flag416prime-))
(not (Flag415prime-))
(not (Flag413prime-))
(not (Flag412prime-))
(not (Flag411prime-))
(not (Flag410prime-))
(not (Flag409prime-))
(not (Flag408prime-))
(not (Flag407prime-))
(not (Flag406prime-))
(not (Flag405prime-))
(not (Flag404prime-))
(not (Flag403prime-))
(not (Flag401prime-))
(not (Flag400prime-))
(not (Flag399prime-))
(not (Flag398prime-))
(not (Flag397prime-))
(not (Flag396prime-))
(not (Flag395prime-))
(not (Flag394prime-))
(not (Flag393prime-))
(not (Flag392prime-))
(not (Flag391prime-))
(not (Flag390prime-))
(not (Flag388prime-))
(not (Flag387prime-))
(not (Flag386prime-))
(not (Flag385prime-))
(not (Flag384prime-))
(not (Flag383prime-))
(not (Flag382prime-))
(not (Flag381prime-))
(not (Flag380prime-))
(not (Flag379prime-))
(not (Flag378prime-))
(not (Flag377prime-))
(not (Flag376prime-))
(not (Flag375prime-))
(not (Flag373prime-))
(not (Flag372prime-))
(not (Flag371prime-))
(not (Flag370prime-))
(not (Flag369prime-))
(not (Flag368prime-))
(not (Flag367prime-))
(not (Flag366prime-))
(not (Flag365prime-))
(not (Flag364prime-))
(not (Flag363prime-))
(not (Flag362prime-))
(not (Flag361prime-))
(not (Flag359prime-))
(not (Flag358prime-))
(not (Flag357prime-))
(not (Flag356prime-))
(not (Flag355prime-))
(not (Flag354prime-))
(not (Flag353prime-))
(not (Flag352prime-))
(not (Flag351prime-))
(not (Flag350prime-))
(not (Flag349prime-))
(not (Flag348prime-))
(not (Flag347prime-))
(not (Flag346prime-))
(not (Flag345prime-))
(not (Flag344prime-))
(not (Flag342prime-))
(not (Flag341prime-))
(not (Flag340prime-))
(not (Flag339prime-))
(not (Flag338prime-))
(not (Flag337prime-))
(not (Flag336prime-))
(not (Flag335prime-))
(not (Flag334prime-))
(not (Flag333prime-))
(not (Flag332prime-))
(not (Flag331prime-))
(not (Flag330prime-))
(not (Flag329prime-))
(not (Flag328prime-))
(not (Flag327prime-))
(not (Flag326prime-))
(not (Flag324prime-))
(not (Flag323prime-))
(not (Flag322prime-))
(not (Flag321prime-))
(not (Flag320prime-))
(not (Flag319prime-))
(not (Flag318prime-))
(not (Flag317prime-))
(not (Flag316prime-))
(not (Flag315prime-))
(not (Flag314prime-))
(not (Flag313prime-))
(not (Flag312prime-))
(not (Flag311prime-))
(not (Flag310prime-))
(not (Flag309prime-))
(not (Flag307prime-))
(not (Flag306prime-))
(not (Flag305prime-))
(not (Flag304prime-))
(not (Flag303prime-))
(not (Flag302prime-))
(not (Flag301prime-))
(not (Flag300prime-))
(not (Flag299prime-))
(not (Flag298prime-))
(not (Flag297prime-))
(not (Flag296prime-))
(not (Flag295prime-))
(not (Flag294prime-))
(not (Flag293prime-))
(not (Flag292prime-))
(not (Flag291prime-))
(not (Flag290prime-))
(not (Flag289prime-))
(not (Flag288prime-))
(not (Flag287prime-))
(not (Flag286prime-))
(not (Flag285prime-))
(not (Flag284prime-))
(not (Flag283prime-))
(not (Flag282prime-))
(not (Flag281prime-))
(not (Flag280prime-))
(not (Flag279prime-))
(not (Flag278prime-))
(not (Flag277prime-))
(not (Flag276prime-))
(not (Flag275prime-))
(not (Flag274prime-))
(not (Flag273prime-))
(not (Flag272prime-))
(not (Flag271prime-))
(not (Flag270prime-))
(not (Flag269prime-))
(not (Flag268prime-))
(not (Flag267prime-))
(not (Flag266prime-))
(not (Flag265prime-))
(not (Flag264prime-))
(not (Flag263prime-))
(not (Flag262prime-))
(not (Flag261prime-))
(not (Flag260prime-))
(not (Flag259prime-))
(not (Flag254prime-))
(not (Flag253prime-))
(not (Flag252prime-))
(not (Flag251prime-))
(not (Flag250prime-))
(not (Flag249prime-))
(not (Flag248prime-))
(not (Flag247prime-))
(not (Flag246prime-))
(not (Flag245prime-))
(not (Flag244prime-))
(not (Flag243prime-))
(not (Flag242prime-))
(not (Flag241prime-))
(not (Flag240prime-))
(not (Flag239prime-))
(not (Flag237prime-))
(not (Flag234prime-))
(not (Flag230prime-))
(not (Flag225prime-))
(not (Flag218prime-))
(not (Flag210prime-))
(not (Flag202prime-))
(not (Flag193prime-))
(not (Flag182prime-))
(not (Flag169prime-))
(not (Flag156prime-))
(not (Flag141prime-))
(not (Flag125prime-))
(not (Flag109prime-))
(not (Flag91prime-))
(not (Flag73prime-))
(not (Flag1prime-))
(not (Flag475prime-))
(not (Flag471prime-))
(not (Flag469prime-))
(not (Flag466prime-))
(not (Flag461prime-))
(not (Flag456prime-))
(not (Flag450prime-))
(not (Flag442prime-))
(not (Flag434prime-))
(not (Flag424prime-))
(not (Flag414prime-))
(not (Flag402prime-))
(not (Flag389prime-))
(not (Flag374prime-))
(not (Flag360prime-))
(not (Flag343prime-))
(not (Flag325prime-))
(not (Flag308prime-))
)
)
(:action MOVEDOWN-ROBOT
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(Flag239prime-)
(Flag237prime-)
(Flag234prime-)
(Flag230prime-)
(Flag225prime-)
(Flag218prime-)
(Flag210prime-)
(Flag202prime-)
(Flag193prime-)
(Flag182prime-)
(Flag169prime-)
(Flag156prime-)
(Flag141prime-)
(Flag125prime-)
(Flag109prime-)
(Flag91prime-)
(Flag73prime-)
(Flag37prime-)
(Flag36prime-)
(Flag35prime-)
(Flag34prime-)
(Flag33prime-)
(Flag32prime-)
(Flag31prime-)
(Flag30prime-)
(Flag29prime-)
(Flag28prime-)
(Flag27prime-)
(Flag26prime-)
(Flag25prime-)
(Flag24prime-)
(Flag23prime-)
(Flag22prime-)
(Flag21prime-)
(Flag20prime-)
(Flag19prime-)
(Flag18prime-)
(Flag17prime-)
(Flag16prime-)
(Flag15prime-)
(Flag14prime-)
(Flag13prime-)
(Flag12prime-)
(Flag11prime-)
(Flag10prime-)
(Flag9prime-)
(Flag8prime-)
(Flag7prime-)
(Flag6prime-)
(Flag5prime-)
)
:effect
(and
(when
(and
(Flag239-)
)
(and
(ROW16-ROBOT)
(not (NOT-ROW16-ROBOT))
)
)
(when
(and
(Flag237-)
)
(and
(ROW15-ROBOT)
(not (NOT-ROW15-ROBOT))
)
)
(when
(and
(Flag234-)
)
(and
(ROW14-ROBOT)
(not (NOT-ROW14-ROBOT))
)
)
(when
(and
(Flag230-)
)
(and
(ROW13-ROBOT)
(not (NOT-ROW13-ROBOT))
)
)
(when
(and
(Flag225-)
)
(and
(ROW12-ROBOT)
(not (NOT-ROW12-ROBOT))
)
)
(when
(and
(Flag218-)
)
(and
(ROW11-ROBOT)
(not (NOT-ROW11-ROBOT))
)
)
(when
(and
(Flag210-)
)
(and
(ROW10-ROBOT)
(not (NOT-ROW10-ROBOT))
)
)
(when
(and
(Flag202-)
)
(and
(ROW9-ROBOT)
(not (NOT-ROW9-ROBOT))
)
)
(when
(and
(Flag193-)
)
(and
(ROW8-ROBOT)
(not (NOT-ROW8-ROBOT))
)
)
(when
(and
(Flag182-)
)
(and
(ROW7-ROBOT)
(not (NOT-ROW7-ROBOT))
)
)
(when
(and
(Flag169-)
)
(and
(ROW6-ROBOT)
(not (NOT-ROW6-ROBOT))
)
)
(when
(and
(Flag156-)
)
(and
(ROW5-ROBOT)
(not (NOT-ROW5-ROBOT))
)
)
(when
(and
(Flag141-)
)
(and
(ROW4-ROBOT)
(not (NOT-ROW4-ROBOT))
)
)
(when
(and
(Flag125-)
)
(and
(ROW3-ROBOT)
(not (NOT-ROW3-ROBOT))
)
)
(when
(and
(Flag109-)
)
(and
(ROW2-ROBOT)
(not (NOT-ROW2-ROBOT))
)
)
(when
(and
(Flag91-)
)
(and
(ROW1-ROBOT)
(not (NOT-ROW1-ROBOT))
)
)
(when
(and
(Flag73-)
)
(and
(ROW0-ROBOT)
(not (NOT-ROW0-ROBOT))
)
)
(when
(and
(ROW17-ROBOT)
)
(and
(ROW16-ROBOT)
(NOT-ROW17-ROBOT)
(not (NOT-ROW16-ROBOT))
(not (ROW17-ROBOT))
)
)
(when
(and
(ROW16-ROBOT)
)
(and
(ROW15-ROBOT)
(NOT-ROW16-ROBOT)
(not (NOT-ROW15-ROBOT))
(not (ROW16-ROBOT))
)
)
(when
(and
(ROW15-ROBOT)
)
(and
(ROW14-ROBOT)
(NOT-ROW15-ROBOT)
(not (NOT-ROW14-ROBOT))
(not (ROW15-ROBOT))
)
)
(when
(and
(ROW14-ROBOT)
)
(and
(ROW13-ROBOT)
(NOT-ROW14-ROBOT)
(not (NOT-ROW13-ROBOT))
(not (ROW14-ROBOT))
)
)
(when
(and
(ROW13-ROBOT)
)
(and
(ROW12-ROBOT)
(NOT-ROW13-ROBOT)
(not (NOT-ROW12-ROBOT))
(not (ROW13-ROBOT))
)
)
(when
(and
(ROW12-ROBOT)
)
(and
(ROW11-ROBOT)
(NOT-ROW12-ROBOT)
(not (NOT-ROW11-ROBOT))
(not (ROW12-ROBOT))
)
)
(when
(and
(ROW11-ROBOT)
)
(and
(ROW10-ROBOT)
(NOT-ROW11-ROBOT)
(not (NOT-ROW10-ROBOT))
(not (ROW11-ROBOT))
)
)
(when
(and
(ROW10-ROBOT)
)
(and
(ROW9-ROBOT)
(NOT-ROW10-ROBOT)
(not (NOT-ROW9-ROBOT))
(not (ROW10-ROBOT))
)
)
(when
(and
(ROW9-ROBOT)
)
(and
(ROW8-ROBOT)
(NOT-ROW9-ROBOT)
(not (NOT-ROW8-ROBOT))
(not (ROW9-ROBOT))
)
)
(when
(and
(ROW8-ROBOT)
)
(and
(ROW7-ROBOT)
(NOT-ROW8-ROBOT)
(not (NOT-ROW7-ROBOT))
(not (ROW8-ROBOT))
)
)
(when
(and
(ROW7-ROBOT)
)
(and
(ROW6-ROBOT)
(NOT-ROW7-ROBOT)
(not (NOT-ROW6-ROBOT))
(not (ROW7-ROBOT))
)
)
(when
(and
(ROW6-ROBOT)
)
(and
(ROW5-ROBOT)
(NOT-ROW6-ROBOT)
(not (NOT-ROW5-ROBOT))
(not (ROW6-ROBOT))
)
)
(when
(and
(ROW5-ROBOT)
)
(and
(ROW4-ROBOT)
(NOT-ROW5-ROBOT)
(not (NOT-ROW4-ROBOT))
(not (ROW5-ROBOT))
)
)
(when
(and
(ROW4-ROBOT)
)
(and
(ROW3-ROBOT)
(NOT-ROW4-ROBOT)
(not (NOT-ROW3-ROBOT))
(not (ROW4-ROBOT))
)
)
(when
(and
(ROW3-ROBOT)
)
(and
(ROW2-ROBOT)
(NOT-ROW3-ROBOT)
(not (NOT-ROW2-ROBOT))
(not (ROW3-ROBOT))
)
)
(when
(and
(ROW2-ROBOT)
)
(and
(ROW1-ROBOT)
(NOT-ROW2-ROBOT)
(not (NOT-ROW1-ROBOT))
(not (ROW2-ROBOT))
)
)
(when
(and
(ROW1-ROBOT)
)
(and
(ROW0-ROBOT)
(NOT-ROW1-ROBOT)
(not (NOT-ROW0-ROBOT))
(not (ROW1-ROBOT))
)
)
(when
(and
(ABOVEOF17-ROBOT)
)
(and
(ABOVEOF16-ROBOT)
(NOT-ABOVEOF17-ROBOT)
(not (NOT-ABOVEOF16-ROBOT))
(not (ABOVEOF17-ROBOT))
)
)
(when
(and
(Flag37-)
)
(and
(ABOVEOF15-ROBOT)
(NOT-ABOVEOF16-ROBOT)
(not (NOT-ABOVEOF15-ROBOT))
(not (ABOVEOF16-ROBOT))
)
)
(when
(and
(Flag36-)
)
(and
(ABOVEOF14-ROBOT)
(NOT-ABOVEOF15-ROBOT)
(not (NOT-ABOVEOF14-ROBOT))
(not (ABOVEOF15-ROBOT))
)
)
(when
(and
(Flag35-)
)
(and
(ABOVEOF13-ROBOT)
(NOT-ABOVEOF14-ROBOT)
(not (NOT-ABOVEOF13-ROBOT))
(not (ABOVEOF14-ROBOT))
)
)
(when
(and
(Flag34-)
)
(and
(ABOVEOF12-ROBOT)
(NOT-ABOVEOF13-ROBOT)
(not (NOT-ABOVEOF12-ROBOT))
(not (ABOVEOF13-ROBOT))
)
)
(when
(and
(Flag33-)
)
(and
(ABOVEOF11-ROBOT)
(NOT-ABOVEOF12-ROBOT)
(not (NOT-ABOVEOF11-ROBOT))
(not (ABOVEOF12-ROBOT))
)
)
(when
(and
(Flag32-)
)
(and
(ABOVEOF10-ROBOT)
(NOT-ABOVEOF11-ROBOT)
(not (NOT-ABOVEOF10-ROBOT))
(not (ABOVEOF11-ROBOT))
)
)
(when
(and
(Flag31-)
)
(and
(ABOVEOF9-ROBOT)
(NOT-ABOVEOF10-ROBOT)
(not (NOT-ABOVEOF9-ROBOT))
(not (ABOVEOF10-ROBOT))
)
)
(when
(and
(Flag30-)
)
(and
(ABOVEOF8-ROBOT)
(NOT-ABOVEOF9-ROBOT)
(not (NOT-ABOVEOF8-ROBOT))
(not (ABOVEOF9-ROBOT))
)
)
(when
(and
(Flag29-)
)
(and
(ABOVEOF7-ROBOT)
(NOT-ABOVEOF8-ROBOT)
(not (NOT-ABOVEOF7-ROBOT))
(not (ABOVEOF8-ROBOT))
)
)
(when
(and
(Flag28-)
)
(and
(ABOVEOF6-ROBOT)
(NOT-ABOVEOF7-ROBOT)
(not (NOT-ABOVEOF6-ROBOT))
(not (ABOVEOF7-ROBOT))
)
)
(when
(and
(Flag27-)
)
(and
(ABOVEOF5-ROBOT)
(NOT-ABOVEOF6-ROBOT)
(not (NOT-ABOVEOF5-ROBOT))
(not (ABOVEOF6-ROBOT))
)
)
(when
(and
(Flag26-)
)
(and
(ABOVEOF4-ROBOT)
(NOT-ABOVEOF5-ROBOT)
(not (NOT-ABOVEOF4-ROBOT))
(not (ABOVEOF5-ROBOT))
)
)
(when
(and
(Flag25-)
)
(and
(ABOVEOF3-ROBOT)
(NOT-ABOVEOF4-ROBOT)
(not (NOT-ABOVEOF3-ROBOT))
(not (ABOVEOF4-ROBOT))
)
)
(when
(and
(Flag24-)
)
(and
(ABOVEOF2-ROBOT)
(NOT-ABOVEOF3-ROBOT)
(not (NOT-ABOVEOF2-ROBOT))
(not (ABOVEOF3-ROBOT))
)
)
(when
(and
(Flag23-)
)
(and
(ABOVEOF1-ROBOT)
(NOT-ABOVEOF2-ROBOT)
(not (NOT-ABOVEOF1-ROBOT))
(not (ABOVEOF2-ROBOT))
)
)
(when
(and
(Flag22-)
)
(and
(NOT-ABOVEOF1-ROBOT)
(not (ABOVEOF1-ROBOT))
)
)
(when
(and
(Flag21-)
)
(and
(BELOWOF17-ROBOT)
(not (NOT-BELOWOF17-ROBOT))
)
)
(when
(and
(Flag20-)
)
(and
(BELOWOF16-ROBOT)
(not (NOT-BELOWOF16-ROBOT))
)
)
(when
(and
(Flag19-)
)
(and
(BELOWOF15-ROBOT)
(not (NOT-BELOWOF15-ROBOT))
)
)
(when
(and
(Flag18-)
)
(and
(BELOWOF14-ROBOT)
(not (NOT-BELOWOF14-ROBOT))
)
)
(when
(and
(Flag17-)
)
(and
(BELOWOF13-ROBOT)
(not (NOT-BELOWOF13-ROBOT))
)
)
(when
(and
(Flag16-)
)
(and
(BELOWOF12-ROBOT)
(not (NOT-BELOWOF12-ROBOT))
)
)
(when
(and
(Flag15-)
)
(and
(BELOWOF11-ROBOT)
(not (NOT-BELOWOF11-ROBOT))
)
)
(when
(and
(Flag14-)
)
(and
(BELOWOF10-ROBOT)
(not (NOT-BELOWOF10-ROBOT))
)
)
(when
(and
(Flag13-)
)
(and
(BELOWOF9-ROBOT)
(not (NOT-BELOWOF9-ROBOT))
)
)
(when
(and
(Flag12-)
)
(and
(BELOWOF8-ROBOT)
(not (NOT-BELOWOF8-ROBOT))
)
)
(when
(and
(Flag11-)
)
(and
(BELOWOF7-ROBOT)
(not (NOT-BELOWOF7-ROBOT))
)
)
(when
(and
(Flag10-)
)
(and
(BELOWOF6-ROBOT)
(not (NOT-BELOWOF6-ROBOT))
)
)
(when
(and
(Flag9-)
)
(and
(BELOWOF5-ROBOT)
(not (NOT-BELOWOF5-ROBOT))
)
)
(when
(and
(Flag8-)
)
(and
(BELOWOF4-ROBOT)
(not (NOT-BELOWOF4-ROBOT))
)
)
(when
(and
(Flag7-)
)
(and
(BELOWOF3-ROBOT)
(not (NOT-BELOWOF3-ROBOT))
)
)
(when
(and
(Flag6-)
)
(and
(BELOWOF2-ROBOT)
(not (NOT-BELOWOF2-ROBOT))
)
)
(when
(and
(Flag5-)
)
(and
(BELOWOF1-ROBOT)
(not (NOT-BELOWOF1-ROBOT))
)
)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag473prime-))
(not (Flag258prime-))
(not (Flag257prime-))
(not (Flag256prime-))
(not (Flag255prime-))
(not (Flag21prime-))
(not (Flag20prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag472prime-))
(not (Flag238prime-))
(not (Flag236prime-))
(not (Flag235prime-))
(not (Flag233prime-))
(not (Flag232prime-))
(not (Flag231prime-))
(not (Flag229prime-))
(not (Flag228prime-))
(not (Flag227prime-))
(not (Flag226prime-))
(not (Flag224prime-))
(not (Flag223prime-))
(not (Flag222prime-))
(not (Flag221prime-))
(not (Flag220prime-))
(not (Flag219prime-))
(not (Flag217prime-))
(not (Flag216prime-))
(not (Flag215prime-))
(not (Flag214prime-))
(not (Flag213prime-))
(not (Flag212prime-))
(not (Flag211prime-))
(not (Flag209prime-))
(not (Flag208prime-))
(not (Flag207prime-))
(not (Flag206prime-))
(not (Flag205prime-))
(not (Flag204prime-))
(not (Flag203prime-))
(not (Flag201prime-))
(not (Flag200prime-))
(not (Flag199prime-))
(not (Flag198prime-))
(not (Flag197prime-))
(not (Flag196prime-))
(not (Flag195prime-))
(not (Flag194prime-))
(not (Flag192prime-))
(not (Flag191prime-))
(not (Flag190prime-))
(not (Flag189prime-))
(not (Flag188prime-))
(not (Flag187prime-))
(not (Flag186prime-))
(not (Flag185prime-))
(not (Flag184prime-))
(not (Flag183prime-))
(not (Flag181prime-))
(not (Flag180prime-))
(not (Flag179prime-))
(not (Flag178prime-))
(not (Flag177prime-))
(not (Flag176prime-))
(not (Flag175prime-))
(not (Flag174prime-))
(not (Flag173prime-))
(not (Flag172prime-))
(not (Flag171prime-))
(not (Flag170prime-))
(not (Flag168prime-))
(not (Flag167prime-))
(not (Flag166prime-))
(not (Flag165prime-))
(not (Flag164prime-))
(not (Flag163prime-))
(not (Flag162prime-))
(not (Flag161prime-))
(not (Flag160prime-))
(not (Flag159prime-))
(not (Flag158prime-))
(not (Flag157prime-))
(not (Flag155prime-))
(not (Flag154prime-))
(not (Flag153prime-))
(not (Flag152prime-))
(not (Flag151prime-))
(not (Flag150prime-))
(not (Flag149prime-))
(not (Flag148prime-))
(not (Flag147prime-))
(not (Flag146prime-))
(not (Flag145prime-))
(not (Flag144prime-))
(not (Flag143prime-))
(not (Flag142prime-))
(not (Flag140prime-))
(not (Flag139prime-))
(not (Flag138prime-))
(not (Flag137prime-))
(not (Flag136prime-))
(not (Flag135prime-))
(not (Flag134prime-))
(not (Flag133prime-))
(not (Flag132prime-))
(not (Flag131prime-))
(not (Flag130prime-))
(not (Flag129prime-))
(not (Flag128prime-))
(not (Flag127prime-))
(not (Flag126prime-))
(not (Flag124prime-))
(not (Flag123prime-))
(not (Flag122prime-))
(not (Flag121prime-))
(not (Flag120prime-))
(not (Flag119prime-))
(not (Flag118prime-))
(not (Flag117prime-))
(not (Flag116prime-))
(not (Flag115prime-))
(not (Flag114prime-))
(not (Flag113prime-))
(not (Flag112prime-))
(not (Flag111prime-))
(not (Flag110prime-))
(not (Flag108prime-))
(not (Flag107prime-))
(not (Flag106prime-))
(not (Flag105prime-))
(not (Flag104prime-))
(not (Flag103prime-))
(not (Flag102prime-))
(not (Flag101prime-))
(not (Flag100prime-))
(not (Flag99prime-))
(not (Flag98prime-))
(not (Flag97prime-))
(not (Flag96prime-))
(not (Flag95prime-))
(not (Flag94prime-))
(not (Flag93prime-))
(not (Flag92prime-))
(not (Flag90prime-))
(not (Flag89prime-))
(not (Flag88prime-))
(not (Flag87prime-))
(not (Flag86prime-))
(not (Flag85prime-))
(not (Flag84prime-))
(not (Flag83prime-))
(not (Flag82prime-))
(not (Flag81prime-))
(not (Flag80prime-))
(not (Flag79prime-))
(not (Flag78prime-))
(not (Flag77prime-))
(not (Flag76prime-))
(not (Flag75prime-))
(not (Flag74prime-))
(not (Flag72prime-))
(not (Flag71prime-))
(not (Flag70prime-))
(not (Flag69prime-))
(not (Flag68prime-))
(not (Flag67prime-))
(not (Flag66prime-))
(not (Flag65prime-))
(not (Flag64prime-))
(not (Flag63prime-))
(not (Flag62prime-))
(not (Flag61prime-))
(not (Flag60prime-))
(not (Flag59prime-))
(not (Flag58prime-))
(not (Flag57prime-))
(not (Flag56prime-))
(not (Flag55prime-))
(not (Flag54prime-))
(not (Flag53prime-))
(not (Flag52prime-))
(not (Flag51prime-))
(not (Flag50prime-))
(not (Flag49prime-))
(not (Flag48prime-))
(not (Flag47prime-))
(not (Flag46prime-))
(not (Flag45prime-))
(not (Flag44prime-))
(not (Flag43prime-))
(not (Flag42prime-))
(not (Flag41prime-))
(not (Flag40prime-))
(not (Flag39prime-))
(not (Flag38prime-))
(not (Flag37prime-))
(not (Flag36prime-))
(not (Flag35prime-))
(not (Flag34prime-))
(not (Flag33prime-))
(not (Flag32prime-))
(not (Flag31prime-))
(not (Flag30prime-))
(not (Flag29prime-))
(not (Flag28prime-))
(not (Flag27prime-))
(not (Flag26prime-))
(not (Flag25prime-))
(not (Flag24prime-))
(not (Flag23prime-))
(not (Flag22prime-))
(not (Flag19prime-))
(not (Flag18prime-))
(not (Flag17prime-))
(not (Flag16prime-))
(not (Flag15prime-))
(not (Flag14prime-))
(not (Flag13prime-))
(not (Flag12prime-))
(not (Flag11prime-))
(not (Flag10prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag474prime-))
(not (Flag470prime-))
(not (Flag468prime-))
(not (Flag467prime-))
(not (Flag465prime-))
(not (Flag464prime-))
(not (Flag463prime-))
(not (Flag462prime-))
(not (Flag460prime-))
(not (Flag459prime-))
(not (Flag458prime-))
(not (Flag457prime-))
(not (Flag455prime-))
(not (Flag454prime-))
(not (Flag453prime-))
(not (Flag452prime-))
(not (Flag451prime-))
(not (Flag449prime-))
(not (Flag448prime-))
(not (Flag447prime-))
(not (Flag446prime-))
(not (Flag445prime-))
(not (Flag444prime-))
(not (Flag443prime-))
(not (Flag441prime-))
(not (Flag440prime-))
(not (Flag439prime-))
(not (Flag438prime-))
(not (Flag437prime-))
(not (Flag436prime-))
(not (Flag435prime-))
(not (Flag433prime-))
(not (Flag432prime-))
(not (Flag431prime-))
(not (Flag430prime-))
(not (Flag429prime-))
(not (Flag428prime-))
(not (Flag427prime-))
(not (Flag426prime-))
(not (Flag425prime-))
(not (Flag423prime-))
(not (Flag422prime-))
(not (Flag421prime-))
(not (Flag420prime-))
(not (Flag419prime-))
(not (Flag418prime-))
(not (Flag417prime-))
(not (Flag416prime-))
(not (Flag415prime-))
(not (Flag413prime-))
(not (Flag412prime-))
(not (Flag411prime-))
(not (Flag410prime-))
(not (Flag409prime-))
(not (Flag408prime-))
(not (Flag407prime-))
(not (Flag406prime-))
(not (Flag405prime-))
(not (Flag404prime-))
(not (Flag403prime-))
(not (Flag401prime-))
(not (Flag400prime-))
(not (Flag399prime-))
(not (Flag398prime-))
(not (Flag397prime-))
(not (Flag396prime-))
(not (Flag395prime-))
(not (Flag394prime-))
(not (Flag393prime-))
(not (Flag392prime-))
(not (Flag391prime-))
(not (Flag390prime-))
(not (Flag388prime-))
(not (Flag387prime-))
(not (Flag386prime-))
(not (Flag385prime-))
(not (Flag384prime-))
(not (Flag383prime-))
(not (Flag382prime-))
(not (Flag381prime-))
(not (Flag380prime-))
(not (Flag379prime-))
(not (Flag378prime-))
(not (Flag377prime-))
(not (Flag376prime-))
(not (Flag375prime-))
(not (Flag373prime-))
(not (Flag372prime-))
(not (Flag371prime-))
(not (Flag370prime-))
(not (Flag369prime-))
(not (Flag368prime-))
(not (Flag367prime-))
(not (Flag366prime-))
(not (Flag365prime-))
(not (Flag364prime-))
(not (Flag363prime-))
(not (Flag362prime-))
(not (Flag361prime-))
(not (Flag359prime-))
(not (Flag358prime-))
(not (Flag357prime-))
(not (Flag356prime-))
(not (Flag355prime-))
(not (Flag354prime-))
(not (Flag353prime-))
(not (Flag352prime-))
(not (Flag351prime-))
(not (Flag350prime-))
(not (Flag349prime-))
(not (Flag348prime-))
(not (Flag347prime-))
(not (Flag346prime-))
(not (Flag345prime-))
(not (Flag344prime-))
(not (Flag342prime-))
(not (Flag341prime-))
(not (Flag340prime-))
(not (Flag339prime-))
(not (Flag338prime-))
(not (Flag337prime-))
(not (Flag336prime-))
(not (Flag335prime-))
(not (Flag334prime-))
(not (Flag333prime-))
(not (Flag332prime-))
(not (Flag331prime-))
(not (Flag330prime-))
(not (Flag329prime-))
(not (Flag328prime-))
(not (Flag327prime-))
(not (Flag326prime-))
(not (Flag324prime-))
(not (Flag323prime-))
(not (Flag322prime-))
(not (Flag321prime-))
(not (Flag320prime-))
(not (Flag319prime-))
(not (Flag318prime-))
(not (Flag317prime-))
(not (Flag316prime-))
(not (Flag315prime-))
(not (Flag314prime-))
(not (Flag313prime-))
(not (Flag312prime-))
(not (Flag311prime-))
(not (Flag310prime-))
(not (Flag309prime-))
(not (Flag307prime-))
(not (Flag306prime-))
(not (Flag305prime-))
(not (Flag304prime-))
(not (Flag303prime-))
(not (Flag302prime-))
(not (Flag301prime-))
(not (Flag300prime-))
(not (Flag299prime-))
(not (Flag298prime-))
(not (Flag297prime-))
(not (Flag296prime-))
(not (Flag295prime-))
(not (Flag294prime-))
(not (Flag293prime-))
(not (Flag292prime-))
(not (Flag291prime-))
(not (Flag290prime-))
(not (Flag289prime-))
(not (Flag288prime-))
(not (Flag287prime-))
(not (Flag286prime-))
(not (Flag285prime-))
(not (Flag284prime-))
(not (Flag283prime-))
(not (Flag282prime-))
(not (Flag281prime-))
(not (Flag280prime-))
(not (Flag279prime-))
(not (Flag278prime-))
(not (Flag277prime-))
(not (Flag276prime-))
(not (Flag275prime-))
(not (Flag274prime-))
(not (Flag273prime-))
(not (Flag272prime-))
(not (Flag271prime-))
(not (Flag270prime-))
(not (Flag269prime-))
(not (Flag268prime-))
(not (Flag267prime-))
(not (Flag266prime-))
(not (Flag265prime-))
(not (Flag264prime-))
(not (Flag263prime-))
(not (Flag262prime-))
(not (Flag261prime-))
(not (Flag260prime-))
(not (Flag259prime-))
(not (Flag254prime-))
(not (Flag253prime-))
(not (Flag252prime-))
(not (Flag251prime-))
(not (Flag250prime-))
(not (Flag249prime-))
(not (Flag248prime-))
(not (Flag247prime-))
(not (Flag246prime-))
(not (Flag245prime-))
(not (Flag244prime-))
(not (Flag243prime-))
(not (Flag242prime-))
(not (Flag241prime-))
(not (Flag240prime-))
(not (Flag239prime-))
(not (Flag237prime-))
(not (Flag234prime-))
(not (Flag230prime-))
(not (Flag225prime-))
(not (Flag218prime-))
(not (Flag210prime-))
(not (Flag202prime-))
(not (Flag193prime-))
(not (Flag182prime-))
(not (Flag169prime-))
(not (Flag156prime-))
(not (Flag141prime-))
(not (Flag125prime-))
(not (Flag109prime-))
(not (Flag91prime-))
(not (Flag73prime-))
(not (Flag1prime-))
(not (Flag475prime-))
(not (Flag471prime-))
(not (Flag469prime-))
(not (Flag466prime-))
(not (Flag461prime-))
(not (Flag456prime-))
(not (Flag450prime-))
(not (Flag442prime-))
(not (Flag434prime-))
(not (Flag424prime-))
(not (Flag414prime-))
(not (Flag402prime-))
(not (Flag389prime-))
(not (Flag374prime-))
(not (Flag360prime-))
(not (Flag343prime-))
(not (Flag325prime-))
(not (Flag308prime-))
)
)
(:action Prim1Action-2
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
(Flag1prime-)
(not (Flag1-))
)

)
(:action Prim1Action-3
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
(Flag1prime-)
(not (Flag1-))
)

)
(:action Flag2Action-6
:parameters ()
:precondition
(and
(RIGHTOF2-ROBOT)
)
:effect
(and
(Flag2-)
(Flag2prime-)
)

)
(:action Flag2Action-33
:parameters ()
:precondition
(and
(LEFTOF17-ROBOT)
)
:effect
(and
(Flag2-)
(Flag2prime-)
)

)
(:action Prim3Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
(Flag3prime-)
(not (Flag3-))
)

)
(:action Prim3Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
(Flag3prime-)
(not (Flag3-))
)

)
(:action Flag4Action
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
)
:effect
(and
(Flag4-)
(Flag4prime-)
)

)
(:action Prim4Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
(Flag4prime-)
(not (Flag4-))
)

)
(:action Prim4Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
(Flag4prime-)
(not (Flag4-))
)

)
(:action Flag20Action-4
:parameters ()
:precondition
(and
(BELOWOF17-ROBOT)
)
:effect
(and
(Flag20-)
(Flag20prime-)
)

)
(:action Flag21Action-4
:parameters ()
:precondition
(and
(BELOWOF17-ROBOT)
)
:effect
(and
(Flag21-)
(Flag21prime-)
)

)
(:action Prim235Action-0
:parameters ()
:precondition
(and
(not (BELOWOF17-ROBOT))
)
:effect
(and
(Flag235prime-)
(not (Flag235-))
)

)
(:action Prim236Action-1
:parameters ()
:precondition
(and
(not (BELOWOF17-ROBOT))
)
:effect
(and
(Flag236prime-)
(not (Flag236-))
)

)
(:action Flag255Action-2
:parameters ()
:precondition
(and
(LEFTOF17-ROBOT)
)
:effect
(and
(Flag255-)
(Flag255prime-)
)

)
(:action Flag256Action-3
:parameters ()
:precondition
(and
(LEFTOF17-ROBOT)
)
:effect
(and
(Flag256-)
(Flag256prime-)
)

)
(:action Flag257Action-3
:parameters ()
:precondition
(and
(RIGHTOF2-ROBOT)
)
:effect
(and
(Flag257-)
(Flag257prime-)
)

)
(:action Flag258Action-3
:parameters ()
:precondition
(and
(RIGHTOF2-ROBOT)
)
:effect
(and
(Flag258-)
(Flag258prime-)
)

)
(:action Prim301Action-0
:parameters ()
:precondition
(and
(not (RIGHTOF2-ROBOT))
)
:effect
(and
(Flag301prime-)
(not (Flag301-))
)

)
(:action Prim302Action-1
:parameters ()
:precondition
(and
(not (RIGHTOF2-ROBOT))
)
:effect
(and
(Flag302prime-)
(not (Flag302-))
)

)
(:action Prim324Action-1
:parameters ()
:precondition
(and
(not (RIGHTOF2-ROBOT))
)
:effect
(and
(Flag324prime-)
(not (Flag324-))
)

)
(:action Prim467Action-0
:parameters ()
:precondition
(and
(not (LEFTOF17-ROBOT))
)
:effect
(and
(Flag467prime-)
(not (Flag467-))
)

)
(:action Prim468Action-0
:parameters ()
:precondition
(and
(not (LEFTOF17-ROBOT))
)
:effect
(and
(Flag468prime-)
(not (Flag468-))
)

)
(:action Flag473Action-4
:parameters ()
:precondition
(and
(RIGHTOF2-ROBOT)
)
:effect
(and
(Flag473-)
(Flag473prime-)
)

)
)
