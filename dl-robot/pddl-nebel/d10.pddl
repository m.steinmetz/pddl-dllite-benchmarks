(define (domain grounded-SIMPLE-ADL-ROBOT)
(:requirements
:strips
)
(:predicates
(Flag177-)
(Flag98-)
(Flag97-)
(Flag21-)
(Flag7-)
(Flag6-)
(Flag5-)
(Flag3-)
(Flag2-)
(ROW8-ROBOT)
(ROW7-ROBOT)
(ROW6-ROBOT)
(ROW5-ROBOT)
(ROW4-ROBOT)
(ROW3-ROBOT)
(ROW2-ROBOT)
(ROW1-ROBOT)
(ROW0-ROBOT)
(ABOVEOF8-ROBOT)
(ABOVEOF7-ROBOT)
(ABOVEOF6-ROBOT)
(ABOVEOF5-ROBOT)
(ABOVEOF4-ROBOT)
(ABOVEOF3-ROBOT)
(ABOVEOF2-ROBOT)
(ABOVEOF1-ROBOT)
(BELOWOF8-ROBOT)
(BELOWOF7-ROBOT)
(BELOWOF6-ROBOT)
(BELOWOF5-ROBOT)
(BELOWOF4-ROBOT)
(BELOWOF3-ROBOT)
(BELOWOF2-ROBOT)
(BELOWOF1-ROBOT)
(CHECKCONSISTENCY-)
(ROW9-ROBOT)
(BELOWOF10-ROBOT)
(NOT-BELOWOF9-ROBOT)
(ABOVEOF9-ROBOT)
(Flag180-)
(Flag175-)
(Flag174-)
(Flag172-)
(Flag171-)
(Flag170-)
(Flag168-)
(Flag167-)
(Flag166-)
(Flag165-)
(Flag163-)
(Flag162-)
(Flag161-)
(Flag160-)
(Flag159-)
(Flag157-)
(Flag156-)
(Flag155-)
(Flag154-)
(Flag153-)
(Flag152-)
(Flag150-)
(Flag149-)
(Flag148-)
(Flag147-)
(Flag146-)
(Flag145-)
(Flag143-)
(Flag142-)
(Flag141-)
(Flag140-)
(Flag139-)
(Flag138-)
(Flag137-)
(Flag136-)
(Flag134-)
(Flag133-)
(Flag132-)
(Flag131-)
(Flag130-)
(Flag129-)
(Flag128-)
(Flag127-)
(Flag126-)
(Flag124-)
(Flag123-)
(Flag122-)
(Flag121-)
(Flag120-)
(Flag119-)
(Flag118-)
(Flag117-)
(Flag116-)
(Flag115-)
(Flag114-)
(Flag113-)
(Flag112-)
(Flag111-)
(Flag110-)
(Flag109-)
(Flag108-)
(Flag107-)
(Flag106-)
(Flag105-)
(Flag104-)
(Flag103-)
(Flag102-)
(Flag101-)
(Flag100-)
(Flag99-)
(Flag96-)
(Flag95-)
(Flag94-)
(Flag93-)
(Flag92-)
(Flag91-)
(Flag90-)
(Flag4-)
(ERROR-)
(COLUMN9-ROBOT)
(COLUMN8-ROBOT)
(COLUMN7-ROBOT)
(COLUMN6-ROBOT)
(COLUMN5-ROBOT)
(COLUMN4-ROBOT)
(COLUMN3-ROBOT)
(COLUMN2-ROBOT)
(COLUMN1-ROBOT)
(LEFTOF10-ROBOT)
(NOT-LEFTOF9-ROBOT)
(LEFTOF8-ROBOT)
(LEFTOF7-ROBOT)
(LEFTOF6-ROBOT)
(LEFTOF5-ROBOT)
(LEFTOF4-ROBOT)
(LEFTOF3-ROBOT)
(LEFTOF2-ROBOT)
(RIGHTOF9-ROBOT)
(RIGHTOF8-ROBOT)
(RIGHTOF7-ROBOT)
(RIGHTOF6-ROBOT)
(RIGHTOF5-ROBOT)
(RIGHTOF4-ROBOT)
(RIGHTOF3-ROBOT)
(RIGHTOF1-ROBOT)
(COLUMN0-ROBOT)
(NOT-RIGHTOF2-ROBOT)
(RIGHTOF0-ROBOT)
(LEFTOF1-ROBOT)
(Flag178-)
(Flag176-)
(Flag173-)
(Flag169-)
(Flag164-)
(Flag158-)
(Flag151-)
(Flag144-)
(Flag135-)
(Flag125-)
(Flag88-)
(Flag87-)
(Flag85-)
(Flag84-)
(Flag83-)
(Flag82-)
(Flag80-)
(Flag79-)
(Flag78-)
(Flag77-)
(Flag76-)
(Flag74-)
(Flag73-)
(Flag72-)
(Flag71-)
(Flag70-)
(Flag68-)
(Flag67-)
(Flag66-)
(Flag65-)
(Flag64-)
(Flag63-)
(Flag62-)
(Flag60-)
(Flag59-)
(Flag58-)
(Flag57-)
(Flag56-)
(Flag55-)
(Flag54-)
(Flag53-)
(Flag51-)
(Flag50-)
(Flag49-)
(Flag48-)
(Flag47-)
(Flag46-)
(Flag45-)
(Flag44-)
(Flag42-)
(Flag41-)
(Flag40-)
(Flag39-)
(Flag38-)
(Flag37-)
(Flag36-)
(Flag35-)
(Flag34-)
(Flag33-)
(Flag31-)
(Flag30-)
(Flag29-)
(Flag28-)
(Flag27-)
(Flag26-)
(Flag25-)
(Flag24-)
(Flag23-)
(Flag22-)
(Flag20-)
(Flag19-)
(Flag18-)
(Flag17-)
(Flag16-)
(Flag15-)
(Flag14-)
(Flag13-)
(Flag12-)
(Flag11-)
(Flag10-)
(Flag9-)
(Flag8-)
(Flag1-)
(Flag179-)
(Flag89-)
(Flag86-)
(Flag81-)
(Flag75-)
(Flag69-)
(Flag61-)
(Flag52-)
(Flag43-)
(Flag32-)
(NOT-COLUMN8-ROBOT)
(NOT-COLUMN7-ROBOT)
(NOT-COLUMN6-ROBOT)
(NOT-COLUMN5-ROBOT)
(NOT-COLUMN4-ROBOT)
(NOT-COLUMN3-ROBOT)
(NOT-COLUMN2-ROBOT)
(NOT-COLUMN1-ROBOT)
(NOT-COLUMN0-ROBOT)
(NOT-RIGHTOF8-ROBOT)
(NOT-RIGHTOF7-ROBOT)
(NOT-RIGHTOF6-ROBOT)
(NOT-RIGHTOF5-ROBOT)
(NOT-RIGHTOF4-ROBOT)
(NOT-RIGHTOF3-ROBOT)
(NOT-RIGHTOF1-ROBOT)
(RIGHTOF2-ROBOT)
(NOT-RIGHTOF0-ROBOT)
(NOT-LEFTOF8-ROBOT)
(NOT-LEFTOF7-ROBOT)
(NOT-LEFTOF6-ROBOT)
(NOT-LEFTOF5-ROBOT)
(NOT-LEFTOF4-ROBOT)
(NOT-LEFTOF3-ROBOT)
(NOT-LEFTOF2-ROBOT)
(NOT-LEFTOF1-ROBOT)
(NOT-CHECKCONSISTENCY-)
(NOT-COLUMN9-ROBOT)
(NOT-LEFTOF10-ROBOT)
(LEFTOF9-ROBOT)
(NOT-RIGHTOF9-ROBOT)
(NOT-ERROR-)
(NOT-ROW9-ROBOT)
(NOT-ROW8-ROBOT)
(NOT-ROW7-ROBOT)
(NOT-ROW6-ROBOT)
(NOT-ROW5-ROBOT)
(NOT-ROW4-ROBOT)
(NOT-ROW3-ROBOT)
(NOT-ROW2-ROBOT)
(NOT-ROW1-ROBOT)
(NOT-BELOWOF10-ROBOT)
(BELOWOF9-ROBOT)
(NOT-BELOWOF8-ROBOT)
(NOT-BELOWOF7-ROBOT)
(NOT-BELOWOF6-ROBOT)
(NOT-BELOWOF5-ROBOT)
(NOT-BELOWOF4-ROBOT)
(NOT-BELOWOF3-ROBOT)
(NOT-BELOWOF2-ROBOT)
(NOT-ABOVEOF9-ROBOT)
(NOT-ABOVEOF8-ROBOT)
(NOT-ABOVEOF7-ROBOT)
(NOT-ABOVEOF6-ROBOT)
(NOT-ABOVEOF5-ROBOT)
(NOT-ABOVEOF4-ROBOT)
(NOT-ABOVEOF3-ROBOT)
(NOT-ABOVEOF2-ROBOT)
(NOT-ABOVEOF1-ROBOT)
(NOT-ROW0-ROBOT)
(NOT-BELOWOF1-ROBOT)
(Flag177prime-)
(Flag98prime-)
(Flag97prime-)
(Flag21prime-)
(Flag7prime-)
(Flag6prime-)
(Flag5prime-)
(Flag3prime-)
(Flag2prime-)
(Flag180prime-)
(Flag175prime-)
(Flag174prime-)
(Flag172prime-)
(Flag171prime-)
(Flag170prime-)
(Flag168prime-)
(Flag167prime-)
(Flag166prime-)
(Flag165prime-)
(Flag163prime-)
(Flag162prime-)
(Flag161prime-)
(Flag160prime-)
(Flag159prime-)
(Flag157prime-)
(Flag156prime-)
(Flag155prime-)
(Flag154prime-)
(Flag153prime-)
(Flag152prime-)
(Flag150prime-)
(Flag149prime-)
(Flag148prime-)
(Flag147prime-)
(Flag146prime-)
(Flag145prime-)
(Flag143prime-)
(Flag142prime-)
(Flag141prime-)
(Flag140prime-)
(Flag139prime-)
(Flag138prime-)
(Flag137prime-)
(Flag136prime-)
(Flag134prime-)
(Flag133prime-)
(Flag132prime-)
(Flag131prime-)
(Flag130prime-)
(Flag129prime-)
(Flag128prime-)
(Flag127prime-)
(Flag126prime-)
(Flag124prime-)
(Flag123prime-)
(Flag122prime-)
(Flag121prime-)
(Flag120prime-)
(Flag119prime-)
(Flag118prime-)
(Flag117prime-)
(Flag116prime-)
(Flag115prime-)
(Flag114prime-)
(Flag113prime-)
(Flag112prime-)
(Flag111prime-)
(Flag110prime-)
(Flag109prime-)
(Flag108prime-)
(Flag107prime-)
(Flag106prime-)
(Flag105prime-)
(Flag104prime-)
(Flag103prime-)
(Flag102prime-)
(Flag101prime-)
(Flag100prime-)
(Flag99prime-)
(Flag96prime-)
(Flag95prime-)
(Flag94prime-)
(Flag93prime-)
(Flag92prime-)
(Flag91prime-)
(Flag90prime-)
(Flag4prime-)
(Flag178prime-)
(Flag176prime-)
(Flag173prime-)
(Flag169prime-)
(Flag164prime-)
(Flag158prime-)
(Flag151prime-)
(Flag144prime-)
(Flag135prime-)
(Flag125prime-)
(Flag88prime-)
(Flag87prime-)
(Flag85prime-)
(Flag84prime-)
(Flag83prime-)
(Flag82prime-)
(Flag80prime-)
(Flag79prime-)
(Flag78prime-)
(Flag77prime-)
(Flag76prime-)
(Flag74prime-)
(Flag73prime-)
(Flag72prime-)
(Flag71prime-)
(Flag70prime-)
(Flag68prime-)
(Flag67prime-)
(Flag66prime-)
(Flag65prime-)
(Flag64prime-)
(Flag63prime-)
(Flag62prime-)
(Flag60prime-)
(Flag59prime-)
(Flag58prime-)
(Flag57prime-)
(Flag56prime-)
(Flag55prime-)
(Flag54prime-)
(Flag53prime-)
(Flag51prime-)
(Flag50prime-)
(Flag49prime-)
(Flag48prime-)
(Flag47prime-)
(Flag46prime-)
(Flag45prime-)
(Flag44prime-)
(Flag42prime-)
(Flag41prime-)
(Flag40prime-)
(Flag39prime-)
(Flag38prime-)
(Flag37prime-)
(Flag36prime-)
(Flag35prime-)
(Flag34prime-)
(Flag33prime-)
(Flag31prime-)
(Flag30prime-)
(Flag29prime-)
(Flag28prime-)
(Flag27prime-)
(Flag26prime-)
(Flag25prime-)
(Flag24prime-)
(Flag23prime-)
(Flag22prime-)
(Flag20prime-)
(Flag19prime-)
(Flag18prime-)
(Flag17prime-)
(Flag16prime-)
(Flag15prime-)
(Flag14prime-)
(Flag13prime-)
(Flag12prime-)
(Flag11prime-)
(Flag10prime-)
(Flag9prime-)
(Flag8prime-)
(Flag1prime-)
(Flag179prime-)
(Flag89prime-)
(Flag86prime-)
(Flag81prime-)
(Flag75prime-)
(Flag69prime-)
(Flag61prime-)
(Flag52prime-)
(Flag43prime-)
(Flag32prime-)
)
(:action Flag32Action-0
:parameters ()
:precondition
(and
(Flag22-)
(Flag22prime-)
)
:effect
(and
(Flag32-)
(Flag32prime-)
)

)
(:action Flag32Action-1
:parameters ()
:precondition
(and
(Flag23-)
(Flag23prime-)
)
:effect
(and
(Flag32-)
(Flag32prime-)
)

)
(:action Flag32Action-2
:parameters ()
:precondition
(and
(Flag24-)
(Flag24prime-)
)
:effect
(and
(Flag32-)
(Flag32prime-)
)

)
(:action Flag32Action-3
:parameters ()
:precondition
(and
(Flag25-)
(Flag25prime-)
)
:effect
(and
(Flag32-)
(Flag32prime-)
)

)
(:action Flag32Action-4
:parameters ()
:precondition
(and
(Flag26-)
(Flag26prime-)
)
:effect
(and
(Flag32-)
(Flag32prime-)
)

)
(:action Flag32Action-5
:parameters ()
:precondition
(and
(Flag27-)
(Flag27prime-)
)
:effect
(and
(Flag32-)
(Flag32prime-)
)

)
(:action Flag32Action-6
:parameters ()
:precondition
(and
(Flag28-)
(Flag28prime-)
)
:effect
(and
(Flag32-)
(Flag32prime-)
)

)
(:action Flag32Action-7
:parameters ()
:precondition
(and
(Flag29-)
(Flag29prime-)
)
:effect
(and
(Flag32-)
(Flag32prime-)
)

)
(:action Flag32Action-8
:parameters ()
:precondition
(and
(Flag30-)
(Flag30prime-)
)
:effect
(and
(Flag32-)
(Flag32prime-)
)

)
(:action Flag32Action-9
:parameters ()
:precondition
(and
(Flag31-)
(Flag31prime-)
)
:effect
(and
(Flag32-)
(Flag32prime-)
)

)
(:action Prim32Action
:parameters ()
:precondition
(and
(not (Flag22-))
(Flag22prime-)
(not (Flag23-))
(Flag23prime-)
(not (Flag24-))
(Flag24prime-)
(not (Flag25-))
(Flag25prime-)
(not (Flag26-))
(Flag26prime-)
(not (Flag27-))
(Flag27prime-)
(not (Flag28-))
(Flag28prime-)
(not (Flag29-))
(Flag29prime-)
(not (Flag30-))
(Flag30prime-)
(not (Flag31-))
(Flag31prime-)
)
:effect
(and
(Flag32prime-)
(not (Flag32-))
)

)
(:action Flag43Action-0
:parameters ()
:precondition
(and
(Flag22-)
(Flag22prime-)
)
:effect
(and
(Flag43-)
(Flag43prime-)
)

)
(:action Flag43Action-1
:parameters ()
:precondition
(and
(Flag33-)
(Flag33prime-)
)
:effect
(and
(Flag43-)
(Flag43prime-)
)

)
(:action Flag43Action-2
:parameters ()
:precondition
(and
(Flag34-)
(Flag34prime-)
)
:effect
(and
(Flag43-)
(Flag43prime-)
)

)
(:action Flag43Action-3
:parameters ()
:precondition
(and
(Flag35-)
(Flag35prime-)
)
:effect
(and
(Flag43-)
(Flag43prime-)
)

)
(:action Flag43Action-4
:parameters ()
:precondition
(and
(Flag36-)
(Flag36prime-)
)
:effect
(and
(Flag43-)
(Flag43prime-)
)

)
(:action Flag43Action-5
:parameters ()
:precondition
(and
(Flag37-)
(Flag37prime-)
)
:effect
(and
(Flag43-)
(Flag43prime-)
)

)
(:action Flag43Action-6
:parameters ()
:precondition
(and
(Flag24-)
(Flag24prime-)
)
:effect
(and
(Flag43-)
(Flag43prime-)
)

)
(:action Flag43Action-7
:parameters ()
:precondition
(and
(Flag25-)
(Flag25prime-)
)
:effect
(and
(Flag43-)
(Flag43prime-)
)

)
(:action Flag43Action-8
:parameters ()
:precondition
(and
(Flag26-)
(Flag26prime-)
)
:effect
(and
(Flag43-)
(Flag43prime-)
)

)
(:action Flag43Action-9
:parameters ()
:precondition
(and
(Flag23-)
(Flag23prime-)
)
:effect
(and
(Flag43-)
(Flag43prime-)
)

)
(:action Flag43Action-10
:parameters ()
:precondition
(and
(Flag38-)
(Flag38prime-)
)
:effect
(and
(Flag43-)
(Flag43prime-)
)

)
(:action Flag43Action-11
:parameters ()
:precondition
(and
(Flag39-)
(Flag39prime-)
)
:effect
(and
(Flag43-)
(Flag43prime-)
)

)
(:action Flag43Action-12
:parameters ()
:precondition
(and
(Flag28-)
(Flag28prime-)
)
:effect
(and
(Flag43-)
(Flag43prime-)
)

)
(:action Flag43Action-13
:parameters ()
:precondition
(and
(Flag29-)
(Flag29prime-)
)
:effect
(and
(Flag43-)
(Flag43prime-)
)

)
(:action Flag43Action-14
:parameters ()
:precondition
(and
(Flag40-)
(Flag40prime-)
)
:effect
(and
(Flag43-)
(Flag43prime-)
)

)
(:action Flag43Action-15
:parameters ()
:precondition
(and
(Flag30-)
(Flag30prime-)
)
:effect
(and
(Flag43-)
(Flag43prime-)
)

)
(:action Flag43Action-16
:parameters ()
:precondition
(and
(Flag31-)
(Flag31prime-)
)
:effect
(and
(Flag43-)
(Flag43prime-)
)

)
(:action Flag43Action-17
:parameters ()
:precondition
(and
(Flag41-)
(Flag41prime-)
)
:effect
(and
(Flag43-)
(Flag43prime-)
)

)
(:action Flag43Action-18
:parameters ()
:precondition
(and
(Flag42-)
(Flag42prime-)
)
:effect
(and
(Flag43-)
(Flag43prime-)
)

)
(:action Prim43Action
:parameters ()
:precondition
(and
(not (Flag22-))
(Flag22prime-)
(not (Flag33-))
(Flag33prime-)
(not (Flag34-))
(Flag34prime-)
(not (Flag35-))
(Flag35prime-)
(not (Flag36-))
(Flag36prime-)
(not (Flag37-))
(Flag37prime-)
(not (Flag24-))
(Flag24prime-)
(not (Flag25-))
(Flag25prime-)
(not (Flag26-))
(Flag26prime-)
(not (Flag23-))
(Flag23prime-)
(not (Flag38-))
(Flag38prime-)
(not (Flag39-))
(Flag39prime-)
(not (Flag28-))
(Flag28prime-)
(not (Flag29-))
(Flag29prime-)
(not (Flag40-))
(Flag40prime-)
(not (Flag30-))
(Flag30prime-)
(not (Flag31-))
(Flag31prime-)
(not (Flag41-))
(Flag41prime-)
(not (Flag42-))
(Flag42prime-)
)
:effect
(and
(Flag43prime-)
(not (Flag43-))
)

)
(:action Flag52Action-0
:parameters ()
:precondition
(and
(Flag36-)
(Flag36prime-)
)
:effect
(and
(Flag52-)
(Flag52prime-)
)

)
(:action Flag52Action-1
:parameters ()
:precondition
(and
(Flag44-)
(Flag44prime-)
)
:effect
(and
(Flag52-)
(Flag52prime-)
)

)
(:action Flag52Action-2
:parameters ()
:precondition
(and
(Flag24-)
(Flag24prime-)
)
:effect
(and
(Flag52-)
(Flag52prime-)
)

)
(:action Flag52Action-3
:parameters ()
:precondition
(and
(Flag26-)
(Flag26prime-)
)
:effect
(and
(Flag52-)
(Flag52prime-)
)

)
(:action Flag52Action-4
:parameters ()
:precondition
(and
(Flag45-)
(Flag45prime-)
)
:effect
(and
(Flag52-)
(Flag52prime-)
)

)
(:action Flag52Action-5
:parameters ()
:precondition
(and
(Flag33-)
(Flag33prime-)
)
:effect
(and
(Flag52-)
(Flag52prime-)
)

)
(:action Flag52Action-6
:parameters ()
:precondition
(and
(Flag23-)
(Flag23prime-)
)
:effect
(and
(Flag52-)
(Flag52prime-)
)

)
(:action Flag52Action-7
:parameters ()
:precondition
(and
(Flag38-)
(Flag38prime-)
)
:effect
(and
(Flag52-)
(Flag52prime-)
)

)
(:action Flag52Action-8
:parameters ()
:precondition
(and
(Flag46-)
(Flag46prime-)
)
:effect
(and
(Flag52-)
(Flag52prime-)
)

)
(:action Flag52Action-9
:parameters ()
:precondition
(and
(Flag40-)
(Flag40prime-)
)
:effect
(and
(Flag52-)
(Flag52prime-)
)

)
(:action Flag52Action-10
:parameters ()
:precondition
(and
(Flag28-)
(Flag28prime-)
)
:effect
(and
(Flag52-)
(Flag52prime-)
)

)
(:action Flag52Action-11
:parameters ()
:precondition
(and
(Flag22-)
(Flag22prime-)
)
:effect
(and
(Flag52-)
(Flag52prime-)
)

)
(:action Flag52Action-12
:parameters ()
:precondition
(and
(Flag47-)
(Flag47prime-)
)
:effect
(and
(Flag52-)
(Flag52prime-)
)

)
(:action Flag52Action-13
:parameters ()
:precondition
(and
(Flag37-)
(Flag37prime-)
)
:effect
(and
(Flag52-)
(Flag52prime-)
)

)
(:action Flag52Action-14
:parameters ()
:precondition
(and
(Flag48-)
(Flag48prime-)
)
:effect
(and
(Flag52-)
(Flag52prime-)
)

)
(:action Flag52Action-15
:parameters ()
:precondition
(and
(Flag30-)
(Flag30prime-)
)
:effect
(and
(Flag52-)
(Flag52prime-)
)

)
(:action Flag52Action-16
:parameters ()
:precondition
(and
(Flag49-)
(Flag49prime-)
)
:effect
(and
(Flag52-)
(Flag52prime-)
)

)
(:action Flag52Action-17
:parameters ()
:precondition
(and
(Flag34-)
(Flag34prime-)
)
:effect
(and
(Flag52-)
(Flag52prime-)
)

)
(:action Flag52Action-18
:parameters ()
:precondition
(and
(Flag39-)
(Flag39prime-)
)
:effect
(and
(Flag52-)
(Flag52prime-)
)

)
(:action Flag52Action-19
:parameters ()
:precondition
(and
(Flag29-)
(Flag29prime-)
)
:effect
(and
(Flag52-)
(Flag52prime-)
)

)
(:action Flag52Action-20
:parameters ()
:precondition
(and
(Flag31-)
(Flag31prime-)
)
:effect
(and
(Flag52-)
(Flag52prime-)
)

)
(:action Flag52Action-21
:parameters ()
:precondition
(and
(Flag41-)
(Flag41prime-)
)
:effect
(and
(Flag52-)
(Flag52prime-)
)

)
(:action Flag52Action-22
:parameters ()
:precondition
(and
(Flag50-)
(Flag50prime-)
)
:effect
(and
(Flag52-)
(Flag52prime-)
)

)
(:action Flag52Action-23
:parameters ()
:precondition
(and
(Flag42-)
(Flag42prime-)
)
:effect
(and
(Flag52-)
(Flag52prime-)
)

)
(:action Flag52Action-24
:parameters ()
:precondition
(and
(Flag51-)
(Flag51prime-)
)
:effect
(and
(Flag52-)
(Flag52prime-)
)

)
(:action Prim52Action
:parameters ()
:precondition
(and
(not (Flag36-))
(Flag36prime-)
(not (Flag44-))
(Flag44prime-)
(not (Flag24-))
(Flag24prime-)
(not (Flag26-))
(Flag26prime-)
(not (Flag45-))
(Flag45prime-)
(not (Flag33-))
(Flag33prime-)
(not (Flag23-))
(Flag23prime-)
(not (Flag38-))
(Flag38prime-)
(not (Flag46-))
(Flag46prime-)
(not (Flag40-))
(Flag40prime-)
(not (Flag28-))
(Flag28prime-)
(not (Flag22-))
(Flag22prime-)
(not (Flag47-))
(Flag47prime-)
(not (Flag37-))
(Flag37prime-)
(not (Flag48-))
(Flag48prime-)
(not (Flag30-))
(Flag30prime-)
(not (Flag49-))
(Flag49prime-)
(not (Flag34-))
(Flag34prime-)
(not (Flag39-))
(Flag39prime-)
(not (Flag29-))
(Flag29prime-)
(not (Flag31-))
(Flag31prime-)
(not (Flag41-))
(Flag41prime-)
(not (Flag50-))
(Flag50prime-)
(not (Flag42-))
(Flag42prime-)
(not (Flag51-))
(Flag51prime-)
)
:effect
(and
(Flag52prime-)
(not (Flag52-))
)

)
(:action Flag61Action-0
:parameters ()
:precondition
(and
(Flag36-)
(Flag36prime-)
)
:effect
(and
(Flag61-)
(Flag61prime-)
)

)
(:action Flag61Action-1
:parameters ()
:precondition
(and
(Flag44-)
(Flag44prime-)
)
:effect
(and
(Flag61-)
(Flag61prime-)
)

)
(:action Flag61Action-2
:parameters ()
:precondition
(and
(Flag24-)
(Flag24prime-)
)
:effect
(and
(Flag61-)
(Flag61prime-)
)

)
(:action Flag61Action-3
:parameters ()
:precondition
(and
(Flag26-)
(Flag26prime-)
)
:effect
(and
(Flag61-)
(Flag61prime-)
)

)
(:action Flag61Action-4
:parameters ()
:precondition
(and
(Flag45-)
(Flag45prime-)
)
:effect
(and
(Flag61-)
(Flag61prime-)
)

)
(:action Flag61Action-5
:parameters ()
:precondition
(and
(Flag53-)
(Flag53prime-)
)
:effect
(and
(Flag61-)
(Flag61prime-)
)

)
(:action Flag61Action-6
:parameters ()
:precondition
(and
(Flag54-)
(Flag54prime-)
)
:effect
(and
(Flag61-)
(Flag61prime-)
)

)
(:action Flag61Action-7
:parameters ()
:precondition
(and
(Flag33-)
(Flag33prime-)
)
:effect
(and
(Flag61-)
(Flag61prime-)
)

)
(:action Flag61Action-8
:parameters ()
:precondition
(and
(Flag23-)
(Flag23prime-)
)
:effect
(and
(Flag61-)
(Flag61prime-)
)

)
(:action Flag61Action-9
:parameters ()
:precondition
(and
(Flag38-)
(Flag38prime-)
)
:effect
(and
(Flag61-)
(Flag61prime-)
)

)
(:action Flag61Action-10
:parameters ()
:precondition
(and
(Flag46-)
(Flag46prime-)
)
:effect
(and
(Flag61-)
(Flag61prime-)
)

)
(:action Flag61Action-11
:parameters ()
:precondition
(and
(Flag40-)
(Flag40prime-)
)
:effect
(and
(Flag61-)
(Flag61prime-)
)

)
(:action Flag61Action-12
:parameters ()
:precondition
(and
(Flag28-)
(Flag28prime-)
)
:effect
(and
(Flag61-)
(Flag61prime-)
)

)
(:action Flag61Action-13
:parameters ()
:precondition
(and
(Flag22-)
(Flag22prime-)
)
:effect
(and
(Flag61-)
(Flag61prime-)
)

)
(:action Flag61Action-14
:parameters ()
:precondition
(and
(Flag47-)
(Flag47prime-)
)
:effect
(and
(Flag61-)
(Flag61prime-)
)

)
(:action Flag61Action-15
:parameters ()
:precondition
(and
(Flag37-)
(Flag37prime-)
)
:effect
(and
(Flag61-)
(Flag61prime-)
)

)
(:action Flag61Action-16
:parameters ()
:precondition
(and
(Flag55-)
(Flag55prime-)
)
:effect
(and
(Flag61-)
(Flag61prime-)
)

)
(:action Flag61Action-17
:parameters ()
:precondition
(and
(Flag48-)
(Flag48prime-)
)
:effect
(and
(Flag61-)
(Flag61prime-)
)

)
(:action Flag61Action-18
:parameters ()
:precondition
(and
(Flag56-)
(Flag56prime-)
)
:effect
(and
(Flag61-)
(Flag61prime-)
)

)
(:action Flag61Action-19
:parameters ()
:precondition
(and
(Flag30-)
(Flag30prime-)
)
:effect
(and
(Flag61-)
(Flag61prime-)
)

)
(:action Flag61Action-20
:parameters ()
:precondition
(and
(Flag57-)
(Flag57prime-)
)
:effect
(and
(Flag61-)
(Flag61prime-)
)

)
(:action Flag61Action-21
:parameters ()
:precondition
(and
(Flag58-)
(Flag58prime-)
)
:effect
(and
(Flag61-)
(Flag61prime-)
)

)
(:action Flag61Action-22
:parameters ()
:precondition
(and
(Flag59-)
(Flag59prime-)
)
:effect
(and
(Flag61-)
(Flag61prime-)
)

)
(:action Flag61Action-23
:parameters ()
:precondition
(and
(Flag34-)
(Flag34prime-)
)
:effect
(and
(Flag61-)
(Flag61prime-)
)

)
(:action Flag61Action-24
:parameters ()
:precondition
(and
(Flag31-)
(Flag31prime-)
)
:effect
(and
(Flag61-)
(Flag61prime-)
)

)
(:action Flag61Action-25
:parameters ()
:precondition
(and
(Flag60-)
(Flag60prime-)
)
:effect
(and
(Flag61-)
(Flag61prime-)
)

)
(:action Flag61Action-26
:parameters ()
:precondition
(and
(Flag41-)
(Flag41prime-)
)
:effect
(and
(Flag61-)
(Flag61prime-)
)

)
(:action Flag61Action-27
:parameters ()
:precondition
(and
(Flag50-)
(Flag50prime-)
)
:effect
(and
(Flag61-)
(Flag61prime-)
)

)
(:action Flag61Action-28
:parameters ()
:precondition
(and
(Flag42-)
(Flag42prime-)
)
:effect
(and
(Flag61-)
(Flag61prime-)
)

)
(:action Prim61Action
:parameters ()
:precondition
(and
(not (Flag36-))
(Flag36prime-)
(not (Flag44-))
(Flag44prime-)
(not (Flag24-))
(Flag24prime-)
(not (Flag26-))
(Flag26prime-)
(not (Flag45-))
(Flag45prime-)
(not (Flag53-))
(Flag53prime-)
(not (Flag54-))
(Flag54prime-)
(not (Flag33-))
(Flag33prime-)
(not (Flag23-))
(Flag23prime-)
(not (Flag38-))
(Flag38prime-)
(not (Flag46-))
(Flag46prime-)
(not (Flag40-))
(Flag40prime-)
(not (Flag28-))
(Flag28prime-)
(not (Flag22-))
(Flag22prime-)
(not (Flag47-))
(Flag47prime-)
(not (Flag37-))
(Flag37prime-)
(not (Flag55-))
(Flag55prime-)
(not (Flag48-))
(Flag48prime-)
(not (Flag56-))
(Flag56prime-)
(not (Flag30-))
(Flag30prime-)
(not (Flag57-))
(Flag57prime-)
(not (Flag58-))
(Flag58prime-)
(not (Flag59-))
(Flag59prime-)
(not (Flag34-))
(Flag34prime-)
(not (Flag31-))
(Flag31prime-)
(not (Flag60-))
(Flag60prime-)
(not (Flag41-))
(Flag41prime-)
(not (Flag50-))
(Flag50prime-)
(not (Flag42-))
(Flag42prime-)
)
:effect
(and
(Flag61prime-)
(not (Flag61-))
)

)
(:action Flag69Action-0
:parameters ()
:precondition
(and
(Flag62-)
(Flag62prime-)
)
:effect
(and
(Flag69-)
(Flag69prime-)
)

)
(:action Flag69Action-1
:parameters ()
:precondition
(and
(Flag36-)
(Flag36prime-)
)
:effect
(and
(Flag69-)
(Flag69prime-)
)

)
(:action Flag69Action-2
:parameters ()
:precondition
(and
(Flag44-)
(Flag44prime-)
)
:effect
(and
(Flag69-)
(Flag69prime-)
)

)
(:action Flag69Action-3
:parameters ()
:precondition
(and
(Flag24-)
(Flag24prime-)
)
:effect
(and
(Flag69-)
(Flag69prime-)
)

)
(:action Flag69Action-4
:parameters ()
:precondition
(and
(Flag63-)
(Flag63prime-)
)
:effect
(and
(Flag69-)
(Flag69prime-)
)

)
(:action Flag69Action-5
:parameters ()
:precondition
(and
(Flag45-)
(Flag45prime-)
)
:effect
(and
(Flag69-)
(Flag69prime-)
)

)
(:action Flag69Action-6
:parameters ()
:precondition
(and
(Flag53-)
(Flag53prime-)
)
:effect
(and
(Flag69-)
(Flag69prime-)
)

)
(:action Flag69Action-7
:parameters ()
:precondition
(and
(Flag54-)
(Flag54prime-)
)
:effect
(and
(Flag69-)
(Flag69prime-)
)

)
(:action Flag69Action-8
:parameters ()
:precondition
(and
(Flag33-)
(Flag33prime-)
)
:effect
(and
(Flag69-)
(Flag69prime-)
)

)
(:action Flag69Action-9
:parameters ()
:precondition
(and
(Flag23-)
(Flag23prime-)
)
:effect
(and
(Flag69-)
(Flag69prime-)
)

)
(:action Flag69Action-10
:parameters ()
:precondition
(and
(Flag38-)
(Flag38prime-)
)
:effect
(and
(Flag69-)
(Flag69prime-)
)

)
(:action Flag69Action-11
:parameters ()
:precondition
(and
(Flag46-)
(Flag46prime-)
)
:effect
(and
(Flag69-)
(Flag69prime-)
)

)
(:action Flag69Action-12
:parameters ()
:precondition
(and
(Flag64-)
(Flag64prime-)
)
:effect
(and
(Flag69-)
(Flag69prime-)
)

)
(:action Flag69Action-13
:parameters ()
:precondition
(and
(Flag65-)
(Flag65prime-)
)
:effect
(and
(Flag69-)
(Flag69prime-)
)

)
(:action Flag69Action-14
:parameters ()
:precondition
(and
(Flag66-)
(Flag66prime-)
)
:effect
(and
(Flag69-)
(Flag69prime-)
)

)
(:action Flag69Action-15
:parameters ()
:precondition
(and
(Flag22-)
(Flag22prime-)
)
:effect
(and
(Flag69-)
(Flag69prime-)
)

)
(:action Flag69Action-16
:parameters ()
:precondition
(and
(Flag47-)
(Flag47prime-)
)
:effect
(and
(Flag69-)
(Flag69prime-)
)

)
(:action Flag69Action-17
:parameters ()
:precondition
(and
(Flag37-)
(Flag37prime-)
)
:effect
(and
(Flag69-)
(Flag69prime-)
)

)
(:action Flag69Action-18
:parameters ()
:precondition
(and
(Flag56-)
(Flag56prime-)
)
:effect
(and
(Flag69-)
(Flag69prime-)
)

)
(:action Flag69Action-19
:parameters ()
:precondition
(and
(Flag67-)
(Flag67prime-)
)
:effect
(and
(Flag69-)
(Flag69prime-)
)

)
(:action Flag69Action-20
:parameters ()
:precondition
(and
(Flag28-)
(Flag28prime-)
)
:effect
(and
(Flag69-)
(Flag69prime-)
)

)
(:action Flag69Action-21
:parameters ()
:precondition
(and
(Flag30-)
(Flag30prime-)
)
:effect
(and
(Flag69-)
(Flag69prime-)
)

)
(:action Flag69Action-22
:parameters ()
:precondition
(and
(Flag57-)
(Flag57prime-)
)
:effect
(and
(Flag69-)
(Flag69prime-)
)

)
(:action Flag69Action-23
:parameters ()
:precondition
(and
(Flag58-)
(Flag58prime-)
)
:effect
(and
(Flag69-)
(Flag69prime-)
)

)
(:action Flag69Action-24
:parameters ()
:precondition
(and
(Flag59-)
(Flag59prime-)
)
:effect
(and
(Flag69-)
(Flag69prime-)
)

)
(:action Flag69Action-25
:parameters ()
:precondition
(and
(Flag34-)
(Flag34prime-)
)
:effect
(and
(Flag69-)
(Flag69prime-)
)

)
(:action Flag69Action-26
:parameters ()
:precondition
(and
(Flag48-)
(Flag48prime-)
)
:effect
(and
(Flag69-)
(Flag69prime-)
)

)
(:action Flag69Action-27
:parameters ()
:precondition
(and
(Flag31-)
(Flag31prime-)
)
:effect
(and
(Flag69-)
(Flag69prime-)
)

)
(:action Flag69Action-28
:parameters ()
:precondition
(and
(Flag68-)
(Flag68prime-)
)
:effect
(and
(Flag69-)
(Flag69prime-)
)

)
(:action Flag69Action-29
:parameters ()
:precondition
(and
(Flag41-)
(Flag41prime-)
)
:effect
(and
(Flag69-)
(Flag69prime-)
)

)
(:action Flag69Action-30
:parameters ()
:precondition
(and
(Flag50-)
(Flag50prime-)
)
:effect
(and
(Flag69-)
(Flag69prime-)
)

)
(:action Flag69Action-31
:parameters ()
:precondition
(and
(Flag42-)
(Flag42prime-)
)
:effect
(and
(Flag69-)
(Flag69prime-)
)

)
(:action Prim69Action
:parameters ()
:precondition
(and
(not (Flag62-))
(Flag62prime-)
(not (Flag36-))
(Flag36prime-)
(not (Flag44-))
(Flag44prime-)
(not (Flag24-))
(Flag24prime-)
(not (Flag63-))
(Flag63prime-)
(not (Flag45-))
(Flag45prime-)
(not (Flag53-))
(Flag53prime-)
(not (Flag54-))
(Flag54prime-)
(not (Flag33-))
(Flag33prime-)
(not (Flag23-))
(Flag23prime-)
(not (Flag38-))
(Flag38prime-)
(not (Flag46-))
(Flag46prime-)
(not (Flag64-))
(Flag64prime-)
(not (Flag65-))
(Flag65prime-)
(not (Flag66-))
(Flag66prime-)
(not (Flag22-))
(Flag22prime-)
(not (Flag47-))
(Flag47prime-)
(not (Flag37-))
(Flag37prime-)
(not (Flag56-))
(Flag56prime-)
(not (Flag67-))
(Flag67prime-)
(not (Flag28-))
(Flag28prime-)
(not (Flag30-))
(Flag30prime-)
(not (Flag57-))
(Flag57prime-)
(not (Flag58-))
(Flag58prime-)
(not (Flag59-))
(Flag59prime-)
(not (Flag34-))
(Flag34prime-)
(not (Flag48-))
(Flag48prime-)
(not (Flag31-))
(Flag31prime-)
(not (Flag68-))
(Flag68prime-)
(not (Flag41-))
(Flag41prime-)
(not (Flag50-))
(Flag50prime-)
(not (Flag42-))
(Flag42prime-)
)
:effect
(and
(Flag69prime-)
(not (Flag69-))
)

)
(:action Flag75Action-0
:parameters ()
:precondition
(and
(Flag62-)
(Flag62prime-)
)
:effect
(and
(Flag75-)
(Flag75prime-)
)

)
(:action Flag75Action-1
:parameters ()
:precondition
(and
(Flag36-)
(Flag36prime-)
)
:effect
(and
(Flag75-)
(Flag75prime-)
)

)
(:action Flag75Action-2
:parameters ()
:precondition
(and
(Flag63-)
(Flag63prime-)
)
:effect
(and
(Flag75-)
(Flag75prime-)
)

)
(:action Flag75Action-3
:parameters ()
:precondition
(and
(Flag24-)
(Flag24prime-)
)
:effect
(and
(Flag75-)
(Flag75prime-)
)

)
(:action Flag75Action-4
:parameters ()
:precondition
(and
(Flag54-)
(Flag54prime-)
)
:effect
(and
(Flag75-)
(Flag75prime-)
)

)
(:action Flag75Action-5
:parameters ()
:precondition
(and
(Flag33-)
(Flag33prime-)
)
:effect
(and
(Flag75-)
(Flag75prime-)
)

)
(:action Flag75Action-6
:parameters ()
:precondition
(and
(Flag23-)
(Flag23prime-)
)
:effect
(and
(Flag75-)
(Flag75prime-)
)

)
(:action Flag75Action-7
:parameters ()
:precondition
(and
(Flag38-)
(Flag38prime-)
)
:effect
(and
(Flag75-)
(Flag75prime-)
)

)
(:action Flag75Action-8
:parameters ()
:precondition
(and
(Flag70-)
(Flag70prime-)
)
:effect
(and
(Flag75-)
(Flag75prime-)
)

)
(:action Flag75Action-9
:parameters ()
:precondition
(and
(Flag46-)
(Flag46prime-)
)
:effect
(and
(Flag75-)
(Flag75prime-)
)

)
(:action Flag75Action-10
:parameters ()
:precondition
(and
(Flag65-)
(Flag65prime-)
)
:effect
(and
(Flag75-)
(Flag75prime-)
)

)
(:action Flag75Action-11
:parameters ()
:precondition
(and
(Flag66-)
(Flag66prime-)
)
:effect
(and
(Flag75-)
(Flag75prime-)
)

)
(:action Flag75Action-12
:parameters ()
:precondition
(and
(Flag44-)
(Flag44prime-)
)
:effect
(and
(Flag75-)
(Flag75prime-)
)

)
(:action Flag75Action-13
:parameters ()
:precondition
(and
(Flag47-)
(Flag47prime-)
)
:effect
(and
(Flag75-)
(Flag75prime-)
)

)
(:action Flag75Action-14
:parameters ()
:precondition
(and
(Flag37-)
(Flag37prime-)
)
:effect
(and
(Flag75-)
(Flag75prime-)
)

)
(:action Flag75Action-15
:parameters ()
:precondition
(and
(Flag56-)
(Flag56prime-)
)
:effect
(and
(Flag75-)
(Flag75prime-)
)

)
(:action Flag75Action-16
:parameters ()
:precondition
(and
(Flag67-)
(Flag67prime-)
)
:effect
(and
(Flag75-)
(Flag75prime-)
)

)
(:action Flag75Action-17
:parameters ()
:precondition
(and
(Flag48-)
(Flag48prime-)
)
:effect
(and
(Flag75-)
(Flag75prime-)
)

)
(:action Flag75Action-18
:parameters ()
:precondition
(and
(Flag71-)
(Flag71prime-)
)
:effect
(and
(Flag75-)
(Flag75prime-)
)

)
(:action Flag75Action-19
:parameters ()
:precondition
(and
(Flag30-)
(Flag30prime-)
)
:effect
(and
(Flag75-)
(Flag75prime-)
)

)
(:action Flag75Action-20
:parameters ()
:precondition
(and
(Flag72-)
(Flag72prime-)
)
:effect
(and
(Flag75-)
(Flag75prime-)
)

)
(:action Flag75Action-21
:parameters ()
:precondition
(and
(Flag73-)
(Flag73prime-)
)
:effect
(and
(Flag75-)
(Flag75prime-)
)

)
(:action Flag75Action-22
:parameters ()
:precondition
(and
(Flag57-)
(Flag57prime-)
)
:effect
(and
(Flag75-)
(Flag75prime-)
)

)
(:action Flag75Action-23
:parameters ()
:precondition
(and
(Flag58-)
(Flag58prime-)
)
:effect
(and
(Flag75-)
(Flag75prime-)
)

)
(:action Flag75Action-24
:parameters ()
:precondition
(and
(Flag59-)
(Flag59prime-)
)
:effect
(and
(Flag75-)
(Flag75prime-)
)

)
(:action Flag75Action-25
:parameters ()
:precondition
(and
(Flag34-)
(Flag34prime-)
)
:effect
(and
(Flag75-)
(Flag75prime-)
)

)
(:action Flag75Action-26
:parameters ()
:precondition
(and
(Flag31-)
(Flag31prime-)
)
:effect
(and
(Flag75-)
(Flag75prime-)
)

)
(:action Flag75Action-27
:parameters ()
:precondition
(and
(Flag68-)
(Flag68prime-)
)
:effect
(and
(Flag75-)
(Flag75prime-)
)

)
(:action Flag75Action-28
:parameters ()
:precondition
(and
(Flag41-)
(Flag41prime-)
)
:effect
(and
(Flag75-)
(Flag75prime-)
)

)
(:action Flag75Action-29
:parameters ()
:precondition
(and
(Flag50-)
(Flag50prime-)
)
:effect
(and
(Flag75-)
(Flag75prime-)
)

)
(:action Flag75Action-30
:parameters ()
:precondition
(and
(Flag74-)
(Flag74prime-)
)
:effect
(and
(Flag75-)
(Flag75prime-)
)

)
(:action Prim75Action
:parameters ()
:precondition
(and
(not (Flag62-))
(Flag62prime-)
(not (Flag36-))
(Flag36prime-)
(not (Flag63-))
(Flag63prime-)
(not (Flag24-))
(Flag24prime-)
(not (Flag54-))
(Flag54prime-)
(not (Flag33-))
(Flag33prime-)
(not (Flag23-))
(Flag23prime-)
(not (Flag38-))
(Flag38prime-)
(not (Flag70-))
(Flag70prime-)
(not (Flag46-))
(Flag46prime-)
(not (Flag65-))
(Flag65prime-)
(not (Flag66-))
(Flag66prime-)
(not (Flag44-))
(Flag44prime-)
(not (Flag47-))
(Flag47prime-)
(not (Flag37-))
(Flag37prime-)
(not (Flag56-))
(Flag56prime-)
(not (Flag67-))
(Flag67prime-)
(not (Flag48-))
(Flag48prime-)
(not (Flag71-))
(Flag71prime-)
(not (Flag30-))
(Flag30prime-)
(not (Flag72-))
(Flag72prime-)
(not (Flag73-))
(Flag73prime-)
(not (Flag57-))
(Flag57prime-)
(not (Flag58-))
(Flag58prime-)
(not (Flag59-))
(Flag59prime-)
(not (Flag34-))
(Flag34prime-)
(not (Flag31-))
(Flag31prime-)
(not (Flag68-))
(Flag68prime-)
(not (Flag41-))
(Flag41prime-)
(not (Flag50-))
(Flag50prime-)
(not (Flag74-))
(Flag74prime-)
)
:effect
(and
(Flag75prime-)
(not (Flag75-))
)

)
(:action Flag81Action-0
:parameters ()
:precondition
(and
(Flag62-)
(Flag62prime-)
)
:effect
(and
(Flag81-)
(Flag81prime-)
)

)
(:action Flag81Action-1
:parameters ()
:precondition
(and
(Flag36-)
(Flag36prime-)
)
:effect
(and
(Flag81-)
(Flag81prime-)
)

)
(:action Flag81Action-2
:parameters ()
:precondition
(and
(Flag63-)
(Flag63prime-)
)
:effect
(and
(Flag81-)
(Flag81prime-)
)

)
(:action Flag81Action-3
:parameters ()
:precondition
(and
(Flag24-)
(Flag24prime-)
)
:effect
(and
(Flag81-)
(Flag81prime-)
)

)
(:action Flag81Action-4
:parameters ()
:precondition
(and
(Flag76-)
(Flag76prime-)
)
:effect
(and
(Flag81-)
(Flag81prime-)
)

)
(:action Flag81Action-5
:parameters ()
:precondition
(and
(Flag77-)
(Flag77prime-)
)
:effect
(and
(Flag81-)
(Flag81prime-)
)

)
(:action Flag81Action-6
:parameters ()
:precondition
(and
(Flag44-)
(Flag44prime-)
)
:effect
(and
(Flag81-)
(Flag81prime-)
)

)
(:action Flag81Action-7
:parameters ()
:precondition
(and
(Flag54-)
(Flag54prime-)
)
:effect
(and
(Flag81-)
(Flag81prime-)
)

)
(:action Flag81Action-8
:parameters ()
:precondition
(and
(Flag37-)
(Flag37prime-)
)
:effect
(and
(Flag81-)
(Flag81prime-)
)

)
(:action Flag81Action-9
:parameters ()
:precondition
(and
(Flag46-)
(Flag46prime-)
)
:effect
(and
(Flag81-)
(Flag81prime-)
)

)
(:action Flag81Action-10
:parameters ()
:precondition
(and
(Flag70-)
(Flag70prime-)
)
:effect
(and
(Flag81-)
(Flag81prime-)
)

)
(:action Flag81Action-11
:parameters ()
:precondition
(and
(Flag65-)
(Flag65prime-)
)
:effect
(and
(Flag81-)
(Flag81prime-)
)

)
(:action Flag81Action-12
:parameters ()
:precondition
(and
(Flag66-)
(Flag66prime-)
)
:effect
(and
(Flag81-)
(Flag81prime-)
)

)
(:action Flag81Action-13
:parameters ()
:precondition
(and
(Flag78-)
(Flag78prime-)
)
:effect
(and
(Flag81-)
(Flag81prime-)
)

)
(:action Flag81Action-14
:parameters ()
:precondition
(and
(Flag47-)
(Flag47prime-)
)
:effect
(and
(Flag81-)
(Flag81prime-)
)

)
(:action Flag81Action-15
:parameters ()
:precondition
(and
(Flag23-)
(Flag23prime-)
)
:effect
(and
(Flag81-)
(Flag81prime-)
)

)
(:action Flag81Action-16
:parameters ()
:precondition
(and
(Flag56-)
(Flag56prime-)
)
:effect
(and
(Flag81-)
(Flag81prime-)
)

)
(:action Flag81Action-17
:parameters ()
:precondition
(and
(Flag67-)
(Flag67prime-)
)
:effect
(and
(Flag81-)
(Flag81prime-)
)

)
(:action Flag81Action-18
:parameters ()
:precondition
(and
(Flag30-)
(Flag30prime-)
)
:effect
(and
(Flag81-)
(Flag81prime-)
)

)
(:action Flag81Action-19
:parameters ()
:precondition
(and
(Flag72-)
(Flag72prime-)
)
:effect
(and
(Flag81-)
(Flag81prime-)
)

)
(:action Flag81Action-20
:parameters ()
:precondition
(and
(Flag79-)
(Flag79prime-)
)
:effect
(and
(Flag81-)
(Flag81prime-)
)

)
(:action Flag81Action-21
:parameters ()
:precondition
(and
(Flag80-)
(Flag80prime-)
)
:effect
(and
(Flag81-)
(Flag81prime-)
)

)
(:action Flag81Action-22
:parameters ()
:precondition
(and
(Flag57-)
(Flag57prime-)
)
:effect
(and
(Flag81-)
(Flag81prime-)
)

)
(:action Flag81Action-23
:parameters ()
:precondition
(and
(Flag58-)
(Flag58prime-)
)
:effect
(and
(Flag81-)
(Flag81prime-)
)

)
(:action Flag81Action-24
:parameters ()
:precondition
(and
(Flag34-)
(Flag34prime-)
)
:effect
(and
(Flag81-)
(Flag81prime-)
)

)
(:action Flag81Action-25
:parameters ()
:precondition
(and
(Flag31-)
(Flag31prime-)
)
:effect
(and
(Flag81-)
(Flag81prime-)
)

)
(:action Flag81Action-26
:parameters ()
:precondition
(and
(Flag41-)
(Flag41prime-)
)
:effect
(and
(Flag81-)
(Flag81prime-)
)

)
(:action Flag81Action-27
:parameters ()
:precondition
(and
(Flag50-)
(Flag50prime-)
)
:effect
(and
(Flag81-)
(Flag81prime-)
)

)
(:action Flag81Action-28
:parameters ()
:precondition
(and
(Flag74-)
(Flag74prime-)
)
:effect
(and
(Flag81-)
(Flag81prime-)
)

)
(:action Prim81Action
:parameters ()
:precondition
(and
(not (Flag62-))
(Flag62prime-)
(not (Flag36-))
(Flag36prime-)
(not (Flag63-))
(Flag63prime-)
(not (Flag24-))
(Flag24prime-)
(not (Flag76-))
(Flag76prime-)
(not (Flag77-))
(Flag77prime-)
(not (Flag44-))
(Flag44prime-)
(not (Flag54-))
(Flag54prime-)
(not (Flag37-))
(Flag37prime-)
(not (Flag46-))
(Flag46prime-)
(not (Flag70-))
(Flag70prime-)
(not (Flag65-))
(Flag65prime-)
(not (Flag66-))
(Flag66prime-)
(not (Flag78-))
(Flag78prime-)
(not (Flag47-))
(Flag47prime-)
(not (Flag23-))
(Flag23prime-)
(not (Flag56-))
(Flag56prime-)
(not (Flag67-))
(Flag67prime-)
(not (Flag30-))
(Flag30prime-)
(not (Flag72-))
(Flag72prime-)
(not (Flag79-))
(Flag79prime-)
(not (Flag80-))
(Flag80prime-)
(not (Flag57-))
(Flag57prime-)
(not (Flag58-))
(Flag58prime-)
(not (Flag34-))
(Flag34prime-)
(not (Flag31-))
(Flag31prime-)
(not (Flag41-))
(Flag41prime-)
(not (Flag50-))
(Flag50prime-)
(not (Flag74-))
(Flag74prime-)
)
:effect
(and
(Flag81prime-)
(not (Flag81-))
)

)
(:action Flag86Action-0
:parameters ()
:precondition
(and
(Flag62-)
(Flag62prime-)
)
:effect
(and
(Flag86-)
(Flag86prime-)
)

)
(:action Flag86Action-1
:parameters ()
:precondition
(and
(Flag82-)
(Flag82prime-)
)
:effect
(and
(Flag86-)
(Flag86prime-)
)

)
(:action Flag86Action-2
:parameters ()
:precondition
(and
(Flag63-)
(Flag63prime-)
)
:effect
(and
(Flag86-)
(Flag86prime-)
)

)
(:action Flag86Action-3
:parameters ()
:precondition
(and
(Flag24-)
(Flag24prime-)
)
:effect
(and
(Flag86-)
(Flag86prime-)
)

)
(:action Flag86Action-4
:parameters ()
:precondition
(and
(Flag77-)
(Flag77prime-)
)
:effect
(and
(Flag86-)
(Flag86prime-)
)

)
(:action Flag86Action-5
:parameters ()
:precondition
(and
(Flag83-)
(Flag83prime-)
)
:effect
(and
(Flag86-)
(Flag86prime-)
)

)
(:action Flag86Action-6
:parameters ()
:precondition
(and
(Flag54-)
(Flag54prime-)
)
:effect
(and
(Flag86-)
(Flag86prime-)
)

)
(:action Flag86Action-7
:parameters ()
:precondition
(and
(Flag36-)
(Flag36prime-)
)
:effect
(and
(Flag86-)
(Flag86prime-)
)

)
(:action Flag86Action-8
:parameters ()
:precondition
(and
(Flag37-)
(Flag37prime-)
)
:effect
(and
(Flag86-)
(Flag86prime-)
)

)
(:action Flag86Action-9
:parameters ()
:precondition
(and
(Flag46-)
(Flag46prime-)
)
:effect
(and
(Flag86-)
(Flag86prime-)
)

)
(:action Flag86Action-10
:parameters ()
:precondition
(and
(Flag70-)
(Flag70prime-)
)
:effect
(and
(Flag86-)
(Flag86prime-)
)

)
(:action Flag86Action-11
:parameters ()
:precondition
(and
(Flag65-)
(Flag65prime-)
)
:effect
(and
(Flag86-)
(Flag86prime-)
)

)
(:action Flag86Action-12
:parameters ()
:precondition
(and
(Flag84-)
(Flag84prime-)
)
:effect
(and
(Flag86-)
(Flag86prime-)
)

)
(:action Flag86Action-13
:parameters ()
:precondition
(and
(Flag85-)
(Flag85prime-)
)
:effect
(and
(Flag86-)
(Flag86prime-)
)

)
(:action Flag86Action-14
:parameters ()
:precondition
(and
(Flag47-)
(Flag47prime-)
)
:effect
(and
(Flag86-)
(Flag86prime-)
)

)
(:action Flag86Action-15
:parameters ()
:precondition
(and
(Flag23-)
(Flag23prime-)
)
:effect
(and
(Flag86-)
(Flag86prime-)
)

)
(:action Flag86Action-16
:parameters ()
:precondition
(and
(Flag56-)
(Flag56prime-)
)
:effect
(and
(Flag86-)
(Flag86prime-)
)

)
(:action Flag86Action-17
:parameters ()
:precondition
(and
(Flag67-)
(Flag67prime-)
)
:effect
(and
(Flag86-)
(Flag86prime-)
)

)
(:action Flag86Action-18
:parameters ()
:precondition
(and
(Flag30-)
(Flag30prime-)
)
:effect
(and
(Flag86-)
(Flag86prime-)
)

)
(:action Flag86Action-19
:parameters ()
:precondition
(and
(Flag72-)
(Flag72prime-)
)
:effect
(and
(Flag86-)
(Flag86prime-)
)

)
(:action Flag86Action-20
:parameters ()
:precondition
(and
(Flag79-)
(Flag79prime-)
)
:effect
(and
(Flag86-)
(Flag86prime-)
)

)
(:action Flag86Action-21
:parameters ()
:precondition
(and
(Flag57-)
(Flag57prime-)
)
:effect
(and
(Flag86-)
(Flag86prime-)
)

)
(:action Flag86Action-22
:parameters ()
:precondition
(and
(Flag34-)
(Flag34prime-)
)
:effect
(and
(Flag86-)
(Flag86prime-)
)

)
(:action Flag86Action-23
:parameters ()
:precondition
(and
(Flag50-)
(Flag50prime-)
)
:effect
(and
(Flag86-)
(Flag86prime-)
)

)
(:action Flag86Action-24
:parameters ()
:precondition
(and
(Flag74-)
(Flag74prime-)
)
:effect
(and
(Flag86-)
(Flag86prime-)
)

)
(:action Prim86Action
:parameters ()
:precondition
(and
(not (Flag62-))
(Flag62prime-)
(not (Flag82-))
(Flag82prime-)
(not (Flag63-))
(Flag63prime-)
(not (Flag24-))
(Flag24prime-)
(not (Flag77-))
(Flag77prime-)
(not (Flag83-))
(Flag83prime-)
(not (Flag54-))
(Flag54prime-)
(not (Flag36-))
(Flag36prime-)
(not (Flag37-))
(Flag37prime-)
(not (Flag46-))
(Flag46prime-)
(not (Flag70-))
(Flag70prime-)
(not (Flag65-))
(Flag65prime-)
(not (Flag84-))
(Flag84prime-)
(not (Flag85-))
(Flag85prime-)
(not (Flag47-))
(Flag47prime-)
(not (Flag23-))
(Flag23prime-)
(not (Flag56-))
(Flag56prime-)
(not (Flag67-))
(Flag67prime-)
(not (Flag30-))
(Flag30prime-)
(not (Flag72-))
(Flag72prime-)
(not (Flag79-))
(Flag79prime-)
(not (Flag57-))
(Flag57prime-)
(not (Flag34-))
(Flag34prime-)
(not (Flag50-))
(Flag50prime-)
(not (Flag74-))
(Flag74prime-)
)
:effect
(and
(Flag86prime-)
(not (Flag86-))
)

)
(:action Flag89Action-0
:parameters ()
:precondition
(and
(Flag62-)
(Flag62prime-)
)
:effect
(and
(Flag89-)
(Flag89prime-)
)

)
(:action Flag89Action-1
:parameters ()
:precondition
(and
(Flag84-)
(Flag84prime-)
)
:effect
(and
(Flag89-)
(Flag89prime-)
)

)
(:action Flag89Action-2
:parameters ()
:precondition
(and
(Flag57-)
(Flag57prime-)
)
:effect
(and
(Flag89-)
(Flag89prime-)
)

)
(:action Flag89Action-3
:parameters ()
:precondition
(and
(Flag47-)
(Flag47prime-)
)
:effect
(and
(Flag89-)
(Flag89prime-)
)

)
(:action Flag89Action-4
:parameters ()
:precondition
(and
(Flag24-)
(Flag24prime-)
)
:effect
(and
(Flag89-)
(Flag89prime-)
)

)
(:action Flag89Action-5
:parameters ()
:precondition
(and
(Flag87-)
(Flag87prime-)
)
:effect
(and
(Flag89-)
(Flag89prime-)
)

)
(:action Flag89Action-6
:parameters ()
:precondition
(and
(Flag67-)
(Flag67prime-)
)
:effect
(and
(Flag89-)
(Flag89prime-)
)

)
(:action Flag89Action-7
:parameters ()
:precondition
(and
(Flag34-)
(Flag34prime-)
)
:effect
(and
(Flag89-)
(Flag89prime-)
)

)
(:action Flag89Action-8
:parameters ()
:precondition
(and
(Flag77-)
(Flag77prime-)
)
:effect
(and
(Flag89-)
(Flag89prime-)
)

)
(:action Flag89Action-9
:parameters ()
:precondition
(and
(Flag70-)
(Flag70prime-)
)
:effect
(and
(Flag89-)
(Flag89prime-)
)

)
(:action Flag89Action-10
:parameters ()
:precondition
(and
(Flag30-)
(Flag30prime-)
)
:effect
(and
(Flag89-)
(Flag89prime-)
)

)
(:action Flag89Action-11
:parameters ()
:precondition
(and
(Flag83-)
(Flag83prime-)
)
:effect
(and
(Flag89-)
(Flag89prime-)
)

)
(:action Flag89Action-12
:parameters ()
:precondition
(and
(Flag36-)
(Flag36prime-)
)
:effect
(and
(Flag89-)
(Flag89prime-)
)

)
(:action Flag89Action-13
:parameters ()
:precondition
(and
(Flag65-)
(Flag65prime-)
)
:effect
(and
(Flag89-)
(Flag89prime-)
)

)
(:action Flag89Action-14
:parameters ()
:precondition
(and
(Flag50-)
(Flag50prime-)
)
:effect
(and
(Flag89-)
(Flag89prime-)
)

)
(:action Flag89Action-15
:parameters ()
:precondition
(and
(Flag74-)
(Flag74prime-)
)
:effect
(and
(Flag89-)
(Flag89prime-)
)

)
(:action Flag89Action-16
:parameters ()
:precondition
(and
(Flag79-)
(Flag79prime-)
)
:effect
(and
(Flag89-)
(Flag89prime-)
)

)
(:action Flag89Action-17
:parameters ()
:precondition
(and
(Flag88-)
(Flag88prime-)
)
:effect
(and
(Flag89-)
(Flag89prime-)
)

)
(:action Prim89Action
:parameters ()
:precondition
(and
(not (Flag62-))
(Flag62prime-)
(not (Flag84-))
(Flag84prime-)
(not (Flag57-))
(Flag57prime-)
(not (Flag47-))
(Flag47prime-)
(not (Flag24-))
(Flag24prime-)
(not (Flag87-))
(Flag87prime-)
(not (Flag67-))
(Flag67prime-)
(not (Flag34-))
(Flag34prime-)
(not (Flag77-))
(Flag77prime-)
(not (Flag70-))
(Flag70prime-)
(not (Flag30-))
(Flag30prime-)
(not (Flag83-))
(Flag83prime-)
(not (Flag36-))
(Flag36prime-)
(not (Flag65-))
(Flag65prime-)
(not (Flag50-))
(Flag50prime-)
(not (Flag74-))
(Flag74prime-)
(not (Flag79-))
(Flag79prime-)
(not (Flag88-))
(Flag88prime-)
)
:effect
(and
(Flag89prime-)
(not (Flag89-))
)

)
(:action Flag179Action-0
:parameters ()
:precondition
(and
(Flag62-)
(Flag62prime-)
)
:effect
(and
(Flag179-)
(Flag179prime-)
)

)
(:action Flag179Action-1
:parameters ()
:precondition
(and
(Flag57-)
(Flag57prime-)
)
:effect
(and
(Flag179-)
(Flag179prime-)
)

)
(:action Flag179Action-2
:parameters ()
:precondition
(and
(Flag36-)
(Flag36prime-)
)
:effect
(and
(Flag179-)
(Flag179prime-)
)

)
(:action Flag179Action-3
:parameters ()
:precondition
(and
(Flag24-)
(Flag24prime-)
)
:effect
(and
(Flag179-)
(Flag179prime-)
)

)
(:action Flag179Action-4
:parameters ()
:precondition
(and
(Flag87-)
(Flag87prime-)
)
:effect
(and
(Flag179-)
(Flag179prime-)
)

)
(:action Flag179Action-5
:parameters ()
:precondition
(and
(Flag178-)
(Flag178prime-)
)
:effect
(and
(Flag179-)
(Flag179prime-)
)

)
(:action Flag179Action-6
:parameters ()
:precondition
(and
(Flag83-)
(Flag83prime-)
)
:effect
(and
(Flag179-)
(Flag179prime-)
)

)
(:action Flag179Action-7
:parameters ()
:precondition
(and
(Flag50-)
(Flag50prime-)
)
:effect
(and
(Flag179-)
(Flag179prime-)
)

)
(:action Flag179Action-8
:parameters ()
:precondition
(and
(Flag74-)
(Flag74prime-)
)
:effect
(and
(Flag179-)
(Flag179prime-)
)

)
(:action Flag179Action-9
:parameters ()
:precondition
(and
(Flag79-)
(Flag79prime-)
)
:effect
(and
(Flag179-)
(Flag179prime-)
)

)
(:action Prim179Action
:parameters ()
:precondition
(and
(not (Flag62-))
(Flag62prime-)
(not (Flag57-))
(Flag57prime-)
(not (Flag36-))
(Flag36prime-)
(not (Flag24-))
(Flag24prime-)
(not (Flag87-))
(Flag87prime-)
(not (Flag178-))
(Flag178prime-)
(not (Flag83-))
(Flag83prime-)
(not (Flag50-))
(Flag50prime-)
(not (Flag74-))
(Flag74prime-)
(not (Flag79-))
(Flag79prime-)
)
:effect
(and
(Flag179prime-)
(not (Flag179-))
)

)
(:action Flag1Action
:parameters ()
:precondition
(and
(COLUMN2-ROBOT)
(ROW1-ROBOT)
(NOT-ERROR-)
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(Flag1-)
(Flag1prime-)
)

)
(:action Prim1Action-0
:parameters ()
:precondition
(and
(not (COLUMN2-ROBOT))
)
:effect
(and
(Flag1prime-)
(not (Flag1-))
)

)
(:action Flag3Action-0
:parameters ()
:precondition
(and
(LEFTOF8-ROBOT)
)
:effect
(and
(Flag3-)
(Flag3prime-)
)

)
(:action Flag3Action-1
:parameters ()
:precondition
(and
(RIGHTOF6-ROBOT)
)
:effect
(and
(Flag3-)
(Flag3prime-)
)

)
(:action Flag3Action-2
:parameters ()
:precondition
(and
(LEFTOF4-ROBOT)
)
:effect
(and
(Flag3-)
(Flag3prime-)
)

)
(:action Flag3Action-3
:parameters ()
:precondition
(and
(LEFTOF3-ROBOT)
)
:effect
(and
(Flag3-)
(Flag3prime-)
)

)
(:action Flag3Action-4
:parameters ()
:precondition
(and
(RIGHTOF9-ROBOT)
)
:effect
(and
(Flag3-)
(Flag3prime-)
)

)
(:action Flag3Action-5
:parameters ()
:precondition
(and
(RIGHTOF7-ROBOT)
)
:effect
(and
(Flag3-)
(Flag3prime-)
)

)
(:action Flag3Action-6
:parameters ()
:precondition
(and
(LEFTOF5-ROBOT)
)
:effect
(and
(Flag3-)
(Flag3prime-)
)

)
(:action Flag3Action-7
:parameters ()
:precondition
(and
(RIGHTOF1-ROBOT)
)
:effect
(and
(Flag3-)
(Flag3prime-)
)

)
(:action Flag3Action-8
:parameters ()
:precondition
(and
(RIGHTOF4-ROBOT)
)
:effect
(and
(Flag3-)
(Flag3prime-)
)

)
(:action Flag3Action-9
:parameters ()
:precondition
(and
(RIGHTOF0-ROBOT)
)
:effect
(and
(Flag3-)
(Flag3prime-)
)

)
(:action Flag3Action-10
:parameters ()
:precondition
(and
(LEFTOF7-ROBOT)
)
:effect
(and
(Flag3-)
(Flag3prime-)
)

)
(:action Flag3Action-11
:parameters ()
:precondition
(and
(LEFTOF2-ROBOT)
)
:effect
(and
(Flag3-)
(Flag3prime-)
)

)
(:action Flag3Action-12
:parameters ()
:precondition
(and
(RIGHTOF8-ROBOT)
)
:effect
(and
(Flag3-)
(Flag3prime-)
)

)
(:action Flag3Action-15
:parameters ()
:precondition
(and
(RIGHTOF5-ROBOT)
)
:effect
(and
(Flag3-)
(Flag3prime-)
)

)
(:action Flag3Action-16
:parameters ()
:precondition
(and
(LEFTOF1-ROBOT)
)
:effect
(and
(Flag3-)
(Flag3prime-)
)

)
(:action Flag3Action-17
:parameters ()
:precondition
(and
(LEFTOF10-ROBOT)
)
:effect
(and
(Flag3-)
(Flag3prime-)
)

)
(:action Flag3Action-18
:parameters ()
:precondition
(and
(LEFTOF6-ROBOT)
)
:effect
(and
(Flag3-)
(Flag3prime-)
)

)
(:action Flag3Action-19
:parameters ()
:precondition
(and
(RIGHTOF3-ROBOT)
)
:effect
(and
(Flag3-)
(Flag3prime-)
)

)
(:action Prim3Action
:parameters ()
:precondition
(and
(not (LEFTOF8-ROBOT))
(not (RIGHTOF6-ROBOT))
(not (LEFTOF4-ROBOT))
(not (LEFTOF3-ROBOT))
(not (RIGHTOF9-ROBOT))
(not (RIGHTOF7-ROBOT))
(not (LEFTOF5-ROBOT))
(not (RIGHTOF1-ROBOT))
(not (RIGHTOF4-ROBOT))
(not (RIGHTOF0-ROBOT))
(not (LEFTOF7-ROBOT))
(not (LEFTOF2-ROBOT))
(not (RIGHTOF8-ROBOT))
(not (LEFTOF9-ROBOT))
(not (RIGHTOF2-ROBOT))
(not (RIGHTOF5-ROBOT))
(not (LEFTOF1-ROBOT))
(not (LEFTOF10-ROBOT))
(not (LEFTOF6-ROBOT))
(not (RIGHTOF3-ROBOT))
)
:effect
(and
(Flag3prime-)
(not (Flag3-))
)

)
(:action Flag5Action-0
:parameters ()
:precondition
(and
(RIGHTOF3-ROBOT)
)
:effect
(and
(Flag5-)
(Flag5prime-)
)

)
(:action Flag5Action-1
:parameters ()
:precondition
(and
(RIGHTOF8-ROBOT)
)
:effect
(and
(Flag5-)
(Flag5prime-)
)

)
(:action Flag5Action-2
:parameters ()
:precondition
(and
(RIGHTOF5-ROBOT)
)
:effect
(and
(Flag5-)
(Flag5prime-)
)

)
(:action Flag5Action-4
:parameters ()
:precondition
(and
(RIGHTOF0-ROBOT)
)
:effect
(and
(Flag5-)
(Flag5prime-)
)

)
(:action Flag5Action-5
:parameters ()
:precondition
(and
(RIGHTOF4-ROBOT)
)
:effect
(and
(Flag5-)
(Flag5prime-)
)

)
(:action Flag5Action-6
:parameters ()
:precondition
(and
(RIGHTOF1-ROBOT)
)
:effect
(and
(Flag5-)
(Flag5prime-)
)

)
(:action Flag5Action-7
:parameters ()
:precondition
(and
(RIGHTOF7-ROBOT)
)
:effect
(and
(Flag5-)
(Flag5prime-)
)

)
(:action Flag5Action-8
:parameters ()
:precondition
(and
(RIGHTOF9-ROBOT)
)
:effect
(and
(Flag5-)
(Flag5prime-)
)

)
(:action Flag5Action-9
:parameters ()
:precondition
(and
(RIGHTOF6-ROBOT)
)
:effect
(and
(Flag5-)
(Flag5prime-)
)

)
(:action Prim5Action
:parameters ()
:precondition
(and
(not (RIGHTOF3-ROBOT))
(not (RIGHTOF8-ROBOT))
(not (RIGHTOF5-ROBOT))
(not (RIGHTOF2-ROBOT))
(not (RIGHTOF0-ROBOT))
(not (RIGHTOF4-ROBOT))
(not (RIGHTOF1-ROBOT))
(not (RIGHTOF7-ROBOT))
(not (RIGHTOF9-ROBOT))
(not (RIGHTOF6-ROBOT))
)
:effect
(and
(Flag5prime-)
(not (Flag5-))
)

)
(:action Flag6Action-0
:parameters ()
:precondition
(and
(RIGHTOF3-ROBOT)
)
:effect
(and
(Flag6-)
(Flag6prime-)
)

)
(:action Flag6Action-1
:parameters ()
:precondition
(and
(RIGHTOF5-ROBOT)
)
:effect
(and
(Flag6-)
(Flag6prime-)
)

)
(:action Flag6Action-3
:parameters ()
:precondition
(and
(RIGHTOF8-ROBOT)
)
:effect
(and
(Flag6-)
(Flag6prime-)
)

)
(:action Flag6Action-4
:parameters ()
:precondition
(and
(RIGHTOF4-ROBOT)
)
:effect
(and
(Flag6-)
(Flag6prime-)
)

)
(:action Flag6Action-5
:parameters ()
:precondition
(and
(RIGHTOF1-ROBOT)
)
:effect
(and
(Flag6-)
(Flag6prime-)
)

)
(:action Flag6Action-6
:parameters ()
:precondition
(and
(RIGHTOF7-ROBOT)
)
:effect
(and
(Flag6-)
(Flag6prime-)
)

)
(:action Flag6Action-7
:parameters ()
:precondition
(and
(RIGHTOF9-ROBOT)
)
:effect
(and
(Flag6-)
(Flag6prime-)
)

)
(:action Flag6Action-8
:parameters ()
:precondition
(and
(RIGHTOF6-ROBOT)
)
:effect
(and
(Flag6-)
(Flag6prime-)
)

)
(:action Prim6Action
:parameters ()
:precondition
(and
(not (RIGHTOF3-ROBOT))
(not (RIGHTOF5-ROBOT))
(not (RIGHTOF2-ROBOT))
(not (RIGHTOF8-ROBOT))
(not (RIGHTOF4-ROBOT))
(not (RIGHTOF1-ROBOT))
(not (RIGHTOF7-ROBOT))
(not (RIGHTOF9-ROBOT))
(not (RIGHTOF6-ROBOT))
)
:effect
(and
(Flag6prime-)
(not (Flag6-))
)

)
(:action Flag7Action-0
:parameters ()
:precondition
(and
(RIGHTOF3-ROBOT)
)
:effect
(and
(Flag7-)
(Flag7prime-)
)

)
(:action Flag7Action-1
:parameters ()
:precondition
(and
(RIGHTOF5-ROBOT)
)
:effect
(and
(Flag7-)
(Flag7prime-)
)

)
(:action Flag7Action-3
:parameters ()
:precondition
(and
(RIGHTOF4-ROBOT)
)
:effect
(and
(Flag7-)
(Flag7prime-)
)

)
(:action Flag7Action-4
:parameters ()
:precondition
(and
(RIGHTOF8-ROBOT)
)
:effect
(and
(Flag7-)
(Flag7prime-)
)

)
(:action Flag7Action-5
:parameters ()
:precondition
(and
(RIGHTOF7-ROBOT)
)
:effect
(and
(Flag7-)
(Flag7prime-)
)

)
(:action Flag7Action-6
:parameters ()
:precondition
(and
(RIGHTOF9-ROBOT)
)
:effect
(and
(Flag7-)
(Flag7prime-)
)

)
(:action Flag7Action-7
:parameters ()
:precondition
(and
(RIGHTOF6-ROBOT)
)
:effect
(and
(Flag7-)
(Flag7prime-)
)

)
(:action Prim7Action
:parameters ()
:precondition
(and
(not (RIGHTOF3-ROBOT))
(not (RIGHTOF5-ROBOT))
(not (RIGHTOF2-ROBOT))
(not (RIGHTOF4-ROBOT))
(not (RIGHTOF8-ROBOT))
(not (RIGHTOF7-ROBOT))
(not (RIGHTOF9-ROBOT))
(not (RIGHTOF6-ROBOT))
)
:effect
(and
(Flag7prime-)
(not (Flag7-))
)

)
(:action Flag8Action-0
:parameters ()
:precondition
(and
(RIGHTOF3-ROBOT)
)
:effect
(and
(Flag8-)
(Flag8prime-)
)

)
(:action Flag8Action-1
:parameters ()
:precondition
(and
(RIGHTOF5-ROBOT)
)
:effect
(and
(Flag8-)
(Flag8prime-)
)

)
(:action Flag8Action-2
:parameters ()
:precondition
(and
(RIGHTOF4-ROBOT)
)
:effect
(and
(Flag8-)
(Flag8prime-)
)

)
(:action Flag8Action-3
:parameters ()
:precondition
(and
(RIGHTOF8-ROBOT)
)
:effect
(and
(Flag8-)
(Flag8prime-)
)

)
(:action Flag8Action-4
:parameters ()
:precondition
(and
(RIGHTOF7-ROBOT)
)
:effect
(and
(Flag8-)
(Flag8prime-)
)

)
(:action Flag8Action-5
:parameters ()
:precondition
(and
(RIGHTOF9-ROBOT)
)
:effect
(and
(Flag8-)
(Flag8prime-)
)

)
(:action Flag8Action-6
:parameters ()
:precondition
(and
(RIGHTOF6-ROBOT)
)
:effect
(and
(Flag8-)
(Flag8prime-)
)

)
(:action Prim8Action
:parameters ()
:precondition
(and
(not (RIGHTOF3-ROBOT))
(not (RIGHTOF5-ROBOT))
(not (RIGHTOF4-ROBOT))
(not (RIGHTOF8-ROBOT))
(not (RIGHTOF7-ROBOT))
(not (RIGHTOF9-ROBOT))
(not (RIGHTOF6-ROBOT))
)
:effect
(and
(Flag8prime-)
(not (Flag8-))
)

)
(:action Flag9Action-0
:parameters ()
:precondition
(and
(RIGHTOF5-ROBOT)
)
:effect
(and
(Flag9-)
(Flag9prime-)
)

)
(:action Flag9Action-1
:parameters ()
:precondition
(and
(RIGHTOF4-ROBOT)
)
:effect
(and
(Flag9-)
(Flag9prime-)
)

)
(:action Flag9Action-2
:parameters ()
:precondition
(and
(RIGHTOF8-ROBOT)
)
:effect
(and
(Flag9-)
(Flag9prime-)
)

)
(:action Flag9Action-3
:parameters ()
:precondition
(and
(RIGHTOF7-ROBOT)
)
:effect
(and
(Flag9-)
(Flag9prime-)
)

)
(:action Flag9Action-4
:parameters ()
:precondition
(and
(RIGHTOF9-ROBOT)
)
:effect
(and
(Flag9-)
(Flag9prime-)
)

)
(:action Flag9Action-5
:parameters ()
:precondition
(and
(RIGHTOF6-ROBOT)
)
:effect
(and
(Flag9-)
(Flag9prime-)
)

)
(:action Prim9Action
:parameters ()
:precondition
(and
(not (RIGHTOF5-ROBOT))
(not (RIGHTOF4-ROBOT))
(not (RIGHTOF8-ROBOT))
(not (RIGHTOF7-ROBOT))
(not (RIGHTOF9-ROBOT))
(not (RIGHTOF6-ROBOT))
)
:effect
(and
(Flag9prime-)
(not (Flag9-))
)

)
(:action Flag10Action-0
:parameters ()
:precondition
(and
(RIGHTOF8-ROBOT)
)
:effect
(and
(Flag10-)
(Flag10prime-)
)

)
(:action Flag10Action-1
:parameters ()
:precondition
(and
(RIGHTOF7-ROBOT)
)
:effect
(and
(Flag10-)
(Flag10prime-)
)

)
(:action Flag10Action-2
:parameters ()
:precondition
(and
(RIGHTOF9-ROBOT)
)
:effect
(and
(Flag10-)
(Flag10prime-)
)

)
(:action Flag10Action-3
:parameters ()
:precondition
(and
(RIGHTOF5-ROBOT)
)
:effect
(and
(Flag10-)
(Flag10prime-)
)

)
(:action Flag10Action-4
:parameters ()
:precondition
(and
(RIGHTOF6-ROBOT)
)
:effect
(and
(Flag10-)
(Flag10prime-)
)

)
(:action Prim10Action
:parameters ()
:precondition
(and
(not (RIGHTOF8-ROBOT))
(not (RIGHTOF7-ROBOT))
(not (RIGHTOF9-ROBOT))
(not (RIGHTOF5-ROBOT))
(not (RIGHTOF6-ROBOT))
)
:effect
(and
(Flag10prime-)
(not (Flag10-))
)

)
(:action Flag11Action-0
:parameters ()
:precondition
(and
(RIGHTOF7-ROBOT)
)
:effect
(and
(Flag11-)
(Flag11prime-)
)

)
(:action Flag11Action-1
:parameters ()
:precondition
(and
(RIGHTOF9-ROBOT)
)
:effect
(and
(Flag11-)
(Flag11prime-)
)

)
(:action Flag11Action-2
:parameters ()
:precondition
(and
(RIGHTOF8-ROBOT)
)
:effect
(and
(Flag11-)
(Flag11prime-)
)

)
(:action Flag11Action-3
:parameters ()
:precondition
(and
(RIGHTOF6-ROBOT)
)
:effect
(and
(Flag11-)
(Flag11prime-)
)

)
(:action Prim11Action
:parameters ()
:precondition
(and
(not (RIGHTOF7-ROBOT))
(not (RIGHTOF9-ROBOT))
(not (RIGHTOF8-ROBOT))
(not (RIGHTOF6-ROBOT))
)
:effect
(and
(Flag11prime-)
(not (Flag11-))
)

)
(:action Flag12Action-0
:parameters ()
:precondition
(and
(RIGHTOF7-ROBOT)
)
:effect
(and
(Flag12-)
(Flag12prime-)
)

)
(:action Flag12Action-1
:parameters ()
:precondition
(and
(RIGHTOF9-ROBOT)
)
:effect
(and
(Flag12-)
(Flag12prime-)
)

)
(:action Flag12Action-2
:parameters ()
:precondition
(and
(RIGHTOF8-ROBOT)
)
:effect
(and
(Flag12-)
(Flag12prime-)
)

)
(:action Prim12Action
:parameters ()
:precondition
(and
(not (RIGHTOF7-ROBOT))
(not (RIGHTOF9-ROBOT))
(not (RIGHTOF8-ROBOT))
)
:effect
(and
(Flag12prime-)
(not (Flag12-))
)

)
(:action Flag13Action-0
:parameters ()
:precondition
(and
(RIGHTOF9-ROBOT)
)
:effect
(and
(Flag13-)
(Flag13prime-)
)

)
(:action Flag13Action-1
:parameters ()
:precondition
(and
(RIGHTOF8-ROBOT)
)
:effect
(and
(Flag13-)
(Flag13prime-)
)

)
(:action Prim13Action
:parameters ()
:precondition
(and
(not (RIGHTOF9-ROBOT))
(not (RIGHTOF8-ROBOT))
)
:effect
(and
(Flag13prime-)
(not (Flag13-))
)

)
(:action Flag14Action-0
:parameters ()
:precondition
(and
(LEFTOF1-ROBOT)
)
:effect
(and
(Flag14-)
(Flag14prime-)
)

)
(:action Flag14Action-1
:parameters ()
:precondition
(and
(LEFTOF2-ROBOT)
)
:effect
(and
(Flag14-)
(Flag14prime-)
)

)
(:action Prim14Action
:parameters ()
:precondition
(and
(not (LEFTOF1-ROBOT))
(not (LEFTOF2-ROBOT))
)
:effect
(and
(Flag14prime-)
(not (Flag14-))
)

)
(:action Flag15Action-0
:parameters ()
:precondition
(and
(LEFTOF1-ROBOT)
)
:effect
(and
(Flag15-)
(Flag15prime-)
)

)
(:action Flag15Action-1
:parameters ()
:precondition
(and
(LEFTOF3-ROBOT)
)
:effect
(and
(Flag15-)
(Flag15prime-)
)

)
(:action Flag15Action-2
:parameters ()
:precondition
(and
(LEFTOF2-ROBOT)
)
:effect
(and
(Flag15-)
(Flag15prime-)
)

)
(:action Prim15Action
:parameters ()
:precondition
(and
(not (LEFTOF1-ROBOT))
(not (LEFTOF3-ROBOT))
(not (LEFTOF2-ROBOT))
)
:effect
(and
(Flag15prime-)
(not (Flag15-))
)

)
(:action Flag16Action-0
:parameters ()
:precondition
(and
(LEFTOF1-ROBOT)
)
:effect
(and
(Flag16-)
(Flag16prime-)
)

)
(:action Flag16Action-1
:parameters ()
:precondition
(and
(LEFTOF3-ROBOT)
)
:effect
(and
(Flag16-)
(Flag16prime-)
)

)
(:action Flag16Action-2
:parameters ()
:precondition
(and
(LEFTOF4-ROBOT)
)
:effect
(and
(Flag16-)
(Flag16prime-)
)

)
(:action Flag16Action-3
:parameters ()
:precondition
(and
(LEFTOF2-ROBOT)
)
:effect
(and
(Flag16-)
(Flag16prime-)
)

)
(:action Prim16Action
:parameters ()
:precondition
(and
(not (LEFTOF1-ROBOT))
(not (LEFTOF3-ROBOT))
(not (LEFTOF4-ROBOT))
(not (LEFTOF2-ROBOT))
)
:effect
(and
(Flag16prime-)
(not (Flag16-))
)

)
(:action Flag17Action-0
:parameters ()
:precondition
(and
(LEFTOF5-ROBOT)
)
:effect
(and
(Flag17-)
(Flag17prime-)
)

)
(:action Flag17Action-1
:parameters ()
:precondition
(and
(LEFTOF1-ROBOT)
)
:effect
(and
(Flag17-)
(Flag17prime-)
)

)
(:action Flag17Action-2
:parameters ()
:precondition
(and
(LEFTOF3-ROBOT)
)
:effect
(and
(Flag17-)
(Flag17prime-)
)

)
(:action Flag17Action-3
:parameters ()
:precondition
(and
(LEFTOF4-ROBOT)
)
:effect
(and
(Flag17-)
(Flag17prime-)
)

)
(:action Flag17Action-4
:parameters ()
:precondition
(and
(LEFTOF2-ROBOT)
)
:effect
(and
(Flag17-)
(Flag17prime-)
)

)
(:action Prim17Action
:parameters ()
:precondition
(and
(not (LEFTOF5-ROBOT))
(not (LEFTOF1-ROBOT))
(not (LEFTOF3-ROBOT))
(not (LEFTOF4-ROBOT))
(not (LEFTOF2-ROBOT))
)
:effect
(and
(Flag17prime-)
(not (Flag17-))
)

)
(:action Flag18Action-0
:parameters ()
:precondition
(and
(LEFTOF2-ROBOT)
)
:effect
(and
(Flag18-)
(Flag18prime-)
)

)
(:action Flag18Action-1
:parameters ()
:precondition
(and
(LEFTOF1-ROBOT)
)
:effect
(and
(Flag18-)
(Flag18prime-)
)

)
(:action Flag18Action-2
:parameters ()
:precondition
(and
(LEFTOF6-ROBOT)
)
:effect
(and
(Flag18-)
(Flag18prime-)
)

)
(:action Flag18Action-3
:parameters ()
:precondition
(and
(LEFTOF5-ROBOT)
)
:effect
(and
(Flag18-)
(Flag18prime-)
)

)
(:action Flag18Action-4
:parameters ()
:precondition
(and
(LEFTOF3-ROBOT)
)
:effect
(and
(Flag18-)
(Flag18prime-)
)

)
(:action Flag18Action-5
:parameters ()
:precondition
(and
(LEFTOF4-ROBOT)
)
:effect
(and
(Flag18-)
(Flag18prime-)
)

)
(:action Prim18Action
:parameters ()
:precondition
(and
(not (LEFTOF2-ROBOT))
(not (LEFTOF1-ROBOT))
(not (LEFTOF6-ROBOT))
(not (LEFTOF5-ROBOT))
(not (LEFTOF3-ROBOT))
(not (LEFTOF4-ROBOT))
)
:effect
(and
(Flag18prime-)
(not (Flag18-))
)

)
(:action Flag19Action-0
:parameters ()
:precondition
(and
(LEFTOF2-ROBOT)
)
:effect
(and
(Flag19-)
(Flag19prime-)
)

)
(:action Flag19Action-1
:parameters ()
:precondition
(and
(LEFTOF7-ROBOT)
)
:effect
(and
(Flag19-)
(Flag19prime-)
)

)
(:action Flag19Action-2
:parameters ()
:precondition
(and
(LEFTOF1-ROBOT)
)
:effect
(and
(Flag19-)
(Flag19prime-)
)

)
(:action Flag19Action-3
:parameters ()
:precondition
(and
(LEFTOF6-ROBOT)
)
:effect
(and
(Flag19-)
(Flag19prime-)
)

)
(:action Flag19Action-4
:parameters ()
:precondition
(and
(LEFTOF5-ROBOT)
)
:effect
(and
(Flag19-)
(Flag19prime-)
)

)
(:action Flag19Action-5
:parameters ()
:precondition
(and
(LEFTOF3-ROBOT)
)
:effect
(and
(Flag19-)
(Flag19prime-)
)

)
(:action Flag19Action-6
:parameters ()
:precondition
(and
(LEFTOF4-ROBOT)
)
:effect
(and
(Flag19-)
(Flag19prime-)
)

)
(:action Prim19Action
:parameters ()
:precondition
(and
(not (LEFTOF2-ROBOT))
(not (LEFTOF7-ROBOT))
(not (LEFTOF1-ROBOT))
(not (LEFTOF6-ROBOT))
(not (LEFTOF5-ROBOT))
(not (LEFTOF3-ROBOT))
(not (LEFTOF4-ROBOT))
)
:effect
(and
(Flag19prime-)
(not (Flag19-))
)

)
(:action Flag20Action-0
:parameters ()
:precondition
(and
(LEFTOF2-ROBOT)
)
:effect
(and
(Flag20-)
(Flag20prime-)
)

)
(:action Flag20Action-1
:parameters ()
:precondition
(and
(LEFTOF7-ROBOT)
)
:effect
(and
(Flag20-)
(Flag20prime-)
)

)
(:action Flag20Action-2
:parameters ()
:precondition
(and
(LEFTOF1-ROBOT)
)
:effect
(and
(Flag20-)
(Flag20prime-)
)

)
(:action Flag20Action-3
:parameters ()
:precondition
(and
(LEFTOF6-ROBOT)
)
:effect
(and
(Flag20-)
(Flag20prime-)
)

)
(:action Flag20Action-4
:parameters ()
:precondition
(and
(LEFTOF5-ROBOT)
)
:effect
(and
(Flag20-)
(Flag20prime-)
)

)
(:action Flag20Action-5
:parameters ()
:precondition
(and
(LEFTOF3-ROBOT)
)
:effect
(and
(Flag20-)
(Flag20prime-)
)

)
(:action Flag20Action-6
:parameters ()
:precondition
(and
(LEFTOF4-ROBOT)
)
:effect
(and
(Flag20-)
(Flag20prime-)
)

)
(:action Flag20Action-7
:parameters ()
:precondition
(and
(LEFTOF8-ROBOT)
)
:effect
(and
(Flag20-)
(Flag20prime-)
)

)
(:action Prim20Action
:parameters ()
:precondition
(and
(not (LEFTOF2-ROBOT))
(not (LEFTOF7-ROBOT))
(not (LEFTOF1-ROBOT))
(not (LEFTOF6-ROBOT))
(not (LEFTOF5-ROBOT))
(not (LEFTOF3-ROBOT))
(not (LEFTOF4-ROBOT))
(not (LEFTOF8-ROBOT))
)
:effect
(and
(Flag20prime-)
(not (Flag20-))
)

)
(:action Flag21Action-1
:parameters ()
:precondition
(and
(LEFTOF2-ROBOT)
)
:effect
(and
(Flag21-)
(Flag21prime-)
)

)
(:action Flag21Action-2
:parameters ()
:precondition
(and
(LEFTOF7-ROBOT)
)
:effect
(and
(Flag21-)
(Flag21prime-)
)

)
(:action Flag21Action-3
:parameters ()
:precondition
(and
(LEFTOF1-ROBOT)
)
:effect
(and
(Flag21-)
(Flag21prime-)
)

)
(:action Flag21Action-4
:parameters ()
:precondition
(and
(LEFTOF6-ROBOT)
)
:effect
(and
(Flag21-)
(Flag21prime-)
)

)
(:action Flag21Action-5
:parameters ()
:precondition
(and
(LEFTOF5-ROBOT)
)
:effect
(and
(Flag21-)
(Flag21prime-)
)

)
(:action Flag21Action-6
:parameters ()
:precondition
(and
(LEFTOF3-ROBOT)
)
:effect
(and
(Flag21-)
(Flag21prime-)
)

)
(:action Flag21Action-7
:parameters ()
:precondition
(and
(LEFTOF4-ROBOT)
)
:effect
(and
(Flag21-)
(Flag21prime-)
)

)
(:action Flag21Action-8
:parameters ()
:precondition
(and
(LEFTOF8-ROBOT)
)
:effect
(and
(Flag21-)
(Flag21prime-)
)

)
(:action Prim21Action
:parameters ()
:precondition
(and
(not (LEFTOF9-ROBOT))
(not (LEFTOF2-ROBOT))
(not (LEFTOF7-ROBOT))
(not (LEFTOF1-ROBOT))
(not (LEFTOF6-ROBOT))
(not (LEFTOF5-ROBOT))
(not (LEFTOF3-ROBOT))
(not (LEFTOF4-ROBOT))
(not (LEFTOF8-ROBOT))
)
:effect
(and
(Flag21prime-)
(not (Flag21-))
)

)
(:action Flag22Action
:parameters ()
:precondition
(and
(LEFTOF1-ROBOT)
(RIGHTOF4-ROBOT)
)
:effect
(and
(Flag22-)
(Flag22prime-)
)

)
(:action Flag23Action
:parameters ()
:precondition
(and
(LEFTOF1-ROBOT)
(RIGHTOF7-ROBOT)
)
:effect
(and
(Flag23-)
(Flag23prime-)
)

)
(:action Flag24Action
:parameters ()
:precondition
(and
(LEFTOF1-ROBOT)
(RIGHTOF9-ROBOT)
)
:effect
(and
(Flag24-)
(Flag24prime-)
)

)
(:action Flag25Action
:parameters ()
:precondition
(and
(LEFTOF1-ROBOT)
(RIGHTOF1-ROBOT)
)
:effect
(and
(Flag25-)
(Flag25prime-)
)

)
(:action Flag26Action
:parameters ()
:precondition
(and
(RIGHTOF3-ROBOT)
(LEFTOF1-ROBOT)
)
:effect
(and
(Flag26-)
(Flag26prime-)
)

)
(:action Flag27Action
:parameters ()
:precondition
(and
(RIGHTOF0-ROBOT)
(LEFTOF1-ROBOT)
)
:effect
(and
(Flag27-)
(Flag27prime-)
)

)
(:action Flag28Action
:parameters ()
:precondition
(and
(LEFTOF1-ROBOT)
(RIGHTOF5-ROBOT)
)
:effect
(and
(Flag28-)
(Flag28prime-)
)

)
(:action Flag29Action
:parameters ()
:precondition
(and
(LEFTOF1-ROBOT)
(RIGHTOF2-ROBOT)
)
:effect
(and
(Flag29-)
(Flag29prime-)
)

)
(:action Flag30Action
:parameters ()
:precondition
(and
(RIGHTOF8-ROBOT)
(LEFTOF1-ROBOT)
)
:effect
(and
(Flag30-)
(Flag30prime-)
)

)
(:action Flag31Action
:parameters ()
:precondition
(and
(LEFTOF1-ROBOT)
(RIGHTOF6-ROBOT)
)
:effect
(and
(Flag31-)
(Flag31prime-)
)

)
(:action Prim22Action-0
:parameters ()
:precondition
(and
(not (LEFTOF1-ROBOT))
)
:effect
(and
(Flag22prime-)
(not (Flag22-))
)

)
(:action Prim22Action-1
:parameters ()
:precondition
(and
(not (RIGHTOF4-ROBOT))
)
:effect
(and
(Flag22prime-)
(not (Flag22-))
)

)
(:action Prim23Action-0
:parameters ()
:precondition
(and
(not (LEFTOF1-ROBOT))
)
:effect
(and
(Flag23prime-)
(not (Flag23-))
)

)
(:action Prim23Action-1
:parameters ()
:precondition
(and
(not (RIGHTOF7-ROBOT))
)
:effect
(and
(Flag23prime-)
(not (Flag23-))
)

)
(:action Prim24Action-0
:parameters ()
:precondition
(and
(not (LEFTOF1-ROBOT))
)
:effect
(and
(Flag24prime-)
(not (Flag24-))
)

)
(:action Prim24Action-1
:parameters ()
:precondition
(and
(not (RIGHTOF9-ROBOT))
)
:effect
(and
(Flag24prime-)
(not (Flag24-))
)

)
(:action Prim25Action-0
:parameters ()
:precondition
(and
(not (LEFTOF1-ROBOT))
)
:effect
(and
(Flag25prime-)
(not (Flag25-))
)

)
(:action Prim25Action-1
:parameters ()
:precondition
(and
(not (RIGHTOF1-ROBOT))
)
:effect
(and
(Flag25prime-)
(not (Flag25-))
)

)
(:action Prim26Action-0
:parameters ()
:precondition
(and
(not (RIGHTOF3-ROBOT))
)
:effect
(and
(Flag26prime-)
(not (Flag26-))
)

)
(:action Prim26Action-1
:parameters ()
:precondition
(and
(not (LEFTOF1-ROBOT))
)
:effect
(and
(Flag26prime-)
(not (Flag26-))
)

)
(:action Prim27Action-0
:parameters ()
:precondition
(and
(not (RIGHTOF0-ROBOT))
)
:effect
(and
(Flag27prime-)
(not (Flag27-))
)

)
(:action Prim27Action-1
:parameters ()
:precondition
(and
(not (LEFTOF1-ROBOT))
)
:effect
(and
(Flag27prime-)
(not (Flag27-))
)

)
(:action Prim28Action-0
:parameters ()
:precondition
(and
(not (LEFTOF1-ROBOT))
)
:effect
(and
(Flag28prime-)
(not (Flag28-))
)

)
(:action Prim28Action-1
:parameters ()
:precondition
(and
(not (RIGHTOF5-ROBOT))
)
:effect
(and
(Flag28prime-)
(not (Flag28-))
)

)
(:action Prim29Action-0
:parameters ()
:precondition
(and
(not (LEFTOF1-ROBOT))
)
:effect
(and
(Flag29prime-)
(not (Flag29-))
)

)
(:action Prim30Action-0
:parameters ()
:precondition
(and
(not (RIGHTOF8-ROBOT))
)
:effect
(and
(Flag30prime-)
(not (Flag30-))
)

)
(:action Prim30Action-1
:parameters ()
:precondition
(and
(not (LEFTOF1-ROBOT))
)
:effect
(and
(Flag30prime-)
(not (Flag30-))
)

)
(:action Prim31Action-0
:parameters ()
:precondition
(and
(not (LEFTOF1-ROBOT))
)
:effect
(and
(Flag31prime-)
(not (Flag31-))
)

)
(:action Prim31Action-1
:parameters ()
:precondition
(and
(not (RIGHTOF6-ROBOT))
)
:effect
(and
(Flag31prime-)
(not (Flag31-))
)

)
(:action Flag33Action
:parameters ()
:precondition
(and
(RIGHTOF5-ROBOT)
(LEFTOF2-ROBOT)
)
:effect
(and
(Flag33-)
(Flag33prime-)
)

)
(:action Flag34Action
:parameters ()
:precondition
(and
(RIGHTOF8-ROBOT)
(LEFTOF2-ROBOT)
)
:effect
(and
(Flag34-)
(Flag34prime-)
)

)
(:action Flag35Action
:parameters ()
:precondition
(and
(LEFTOF2-ROBOT)
(RIGHTOF1-ROBOT)
)
:effect
(and
(Flag35-)
(Flag35prime-)
)

)
(:action Flag36Action
:parameters ()
:precondition
(and
(LEFTOF2-ROBOT)
(RIGHTOF9-ROBOT)
)
:effect
(and
(Flag36-)
(Flag36prime-)
)

)
(:action Flag37Action
:parameters ()
:precondition
(and
(LEFTOF2-ROBOT)
(RIGHTOF7-ROBOT)
)
:effect
(and
(Flag37-)
(Flag37prime-)
)

)
(:action Flag38Action
:parameters ()
:precondition
(and
(RIGHTOF5-ROBOT)
(LEFTOF1-ROBOT)
)
:effect
(and
(Flag38-)
(Flag38prime-)
)

)
(:action Flag39Action
:parameters ()
:precondition
(and
(RIGHTOF2-ROBOT)
(LEFTOF2-ROBOT)
)
:effect
(and
(Flag39-)
(Flag39prime-)
)

)
(:action Flag40Action
:parameters ()
:precondition
(and
(RIGHTOF3-ROBOT)
(LEFTOF2-ROBOT)
)
:effect
(and
(Flag40-)
(Flag40prime-)
)

)
(:action Flag41Action
:parameters ()
:precondition
(and
(LEFTOF2-ROBOT)
(RIGHTOF6-ROBOT)
)
:effect
(and
(Flag41-)
(Flag41prime-)
)

)
(:action Flag42Action
:parameters ()
:precondition
(and
(LEFTOF2-ROBOT)
(RIGHTOF4-ROBOT)
)
:effect
(and
(Flag42-)
(Flag42prime-)
)

)
(:action Prim33Action-0
:parameters ()
:precondition
(and
(not (RIGHTOF5-ROBOT))
)
:effect
(and
(Flag33prime-)
(not (Flag33-))
)

)
(:action Prim33Action-1
:parameters ()
:precondition
(and
(not (LEFTOF2-ROBOT))
)
:effect
(and
(Flag33prime-)
(not (Flag33-))
)

)
(:action Prim34Action-0
:parameters ()
:precondition
(and
(not (RIGHTOF8-ROBOT))
)
:effect
(and
(Flag34prime-)
(not (Flag34-))
)

)
(:action Prim34Action-1
:parameters ()
:precondition
(and
(not (LEFTOF2-ROBOT))
)
:effect
(and
(Flag34prime-)
(not (Flag34-))
)

)
(:action Prim35Action-0
:parameters ()
:precondition
(and
(not (LEFTOF2-ROBOT))
)
:effect
(and
(Flag35prime-)
(not (Flag35-))
)

)
(:action Prim35Action-1
:parameters ()
:precondition
(and
(not (RIGHTOF1-ROBOT))
)
:effect
(and
(Flag35prime-)
(not (Flag35-))
)

)
(:action Prim36Action-0
:parameters ()
:precondition
(and
(not (LEFTOF2-ROBOT))
)
:effect
(and
(Flag36prime-)
(not (Flag36-))
)

)
(:action Prim36Action-1
:parameters ()
:precondition
(and
(not (RIGHTOF9-ROBOT))
)
:effect
(and
(Flag36prime-)
(not (Flag36-))
)

)
(:action Prim37Action-0
:parameters ()
:precondition
(and
(not (LEFTOF2-ROBOT))
)
:effect
(and
(Flag37prime-)
(not (Flag37-))
)

)
(:action Prim37Action-1
:parameters ()
:precondition
(and
(not (RIGHTOF7-ROBOT))
)
:effect
(and
(Flag37prime-)
(not (Flag37-))
)

)
(:action Prim38Action-0
:parameters ()
:precondition
(and
(not (RIGHTOF5-ROBOT))
)
:effect
(and
(Flag38prime-)
(not (Flag38-))
)

)
(:action Prim38Action-1
:parameters ()
:precondition
(and
(not (LEFTOF1-ROBOT))
)
:effect
(and
(Flag38prime-)
(not (Flag38-))
)

)
(:action Prim39Action-1
:parameters ()
:precondition
(and
(not (LEFTOF2-ROBOT))
)
:effect
(and
(Flag39prime-)
(not (Flag39-))
)

)
(:action Prim40Action-0
:parameters ()
:precondition
(and
(not (RIGHTOF3-ROBOT))
)
:effect
(and
(Flag40prime-)
(not (Flag40-))
)

)
(:action Prim40Action-1
:parameters ()
:precondition
(and
(not (LEFTOF2-ROBOT))
)
:effect
(and
(Flag40prime-)
(not (Flag40-))
)

)
(:action Prim41Action-0
:parameters ()
:precondition
(and
(not (LEFTOF2-ROBOT))
)
:effect
(and
(Flag41prime-)
(not (Flag41-))
)

)
(:action Prim41Action-1
:parameters ()
:precondition
(and
(not (RIGHTOF6-ROBOT))
)
:effect
(and
(Flag41prime-)
(not (Flag41-))
)

)
(:action Prim42Action-0
:parameters ()
:precondition
(and
(not (LEFTOF2-ROBOT))
)
:effect
(and
(Flag42prime-)
(not (Flag42-))
)

)
(:action Prim42Action-1
:parameters ()
:precondition
(and
(not (RIGHTOF4-ROBOT))
)
:effect
(and
(Flag42prime-)
(not (Flag42-))
)

)
(:action Flag44Action
:parameters ()
:precondition
(and
(LEFTOF3-ROBOT)
(RIGHTOF6-ROBOT)
)
:effect
(and
(Flag44-)
(Flag44prime-)
)

)
(:action Flag45Action
:parameters ()
:precondition
(and
(LEFTOF3-ROBOT)
(RIGHTOF4-ROBOT)
)
:effect
(and
(Flag45-)
(Flag45prime-)
)

)
(:action Flag46Action
:parameters ()
:precondition
(and
(LEFTOF3-ROBOT)
(RIGHTOF7-ROBOT)
)
:effect
(and
(Flag46-)
(Flag46prime-)
)

)
(:action Flag47Action
:parameters ()
:precondition
(and
(LEFTOF3-ROBOT)
(RIGHTOF8-ROBOT)
)
:effect
(and
(Flag47-)
(Flag47prime-)
)

)
(:action Flag48Action
:parameters ()
:precondition
(and
(LEFTOF3-ROBOT)
(RIGHTOF5-ROBOT)
)
:effect
(and
(Flag48-)
(Flag48prime-)
)

)
(:action Flag49Action
:parameters ()
:precondition
(and
(RIGHTOF3-ROBOT)
(LEFTOF3-ROBOT)
)
:effect
(and
(Flag49-)
(Flag49prime-)
)

)
(:action Flag50Action
:parameters ()
:precondition
(and
(LEFTOF3-ROBOT)
(RIGHTOF9-ROBOT)
)
:effect
(and
(Flag50-)
(Flag50prime-)
)

)
(:action Flag51Action
:parameters ()
:precondition
(and
(LEFTOF3-ROBOT)
(RIGHTOF2-ROBOT)
)
:effect
(and
(Flag51-)
(Flag51prime-)
)

)
(:action Prim44Action-0
:parameters ()
:precondition
(and
(not (LEFTOF3-ROBOT))
)
:effect
(and
(Flag44prime-)
(not (Flag44-))
)

)
(:action Prim44Action-1
:parameters ()
:precondition
(and
(not (RIGHTOF6-ROBOT))
)
:effect
(and
(Flag44prime-)
(not (Flag44-))
)

)
(:action Prim45Action-0
:parameters ()
:precondition
(and
(not (LEFTOF3-ROBOT))
)
:effect
(and
(Flag45prime-)
(not (Flag45-))
)

)
(:action Prim45Action-1
:parameters ()
:precondition
(and
(not (RIGHTOF4-ROBOT))
)
:effect
(and
(Flag45prime-)
(not (Flag45-))
)

)
(:action Prim46Action-0
:parameters ()
:precondition
(and
(not (LEFTOF3-ROBOT))
)
:effect
(and
(Flag46prime-)
(not (Flag46-))
)

)
(:action Prim46Action-1
:parameters ()
:precondition
(and
(not (RIGHTOF7-ROBOT))
)
:effect
(and
(Flag46prime-)
(not (Flag46-))
)

)
(:action Prim47Action-0
:parameters ()
:precondition
(and
(not (LEFTOF3-ROBOT))
)
:effect
(and
(Flag47prime-)
(not (Flag47-))
)

)
(:action Prim47Action-1
:parameters ()
:precondition
(and
(not (RIGHTOF8-ROBOT))
)
:effect
(and
(Flag47prime-)
(not (Flag47-))
)

)
(:action Prim48Action-0
:parameters ()
:precondition
(and
(not (LEFTOF3-ROBOT))
)
:effect
(and
(Flag48prime-)
(not (Flag48-))
)

)
(:action Prim48Action-1
:parameters ()
:precondition
(and
(not (RIGHTOF5-ROBOT))
)
:effect
(and
(Flag48prime-)
(not (Flag48-))
)

)
(:action Prim49Action-0
:parameters ()
:precondition
(and
(not (RIGHTOF3-ROBOT))
)
:effect
(and
(Flag49prime-)
(not (Flag49-))
)

)
(:action Prim49Action-1
:parameters ()
:precondition
(and
(not (LEFTOF3-ROBOT))
)
:effect
(and
(Flag49prime-)
(not (Flag49-))
)

)
(:action Prim50Action-0
:parameters ()
:precondition
(and
(not (LEFTOF3-ROBOT))
)
:effect
(and
(Flag50prime-)
(not (Flag50-))
)

)
(:action Prim50Action-1
:parameters ()
:precondition
(and
(not (RIGHTOF9-ROBOT))
)
:effect
(and
(Flag50prime-)
(not (Flag50-))
)

)
(:action Prim51Action-0
:parameters ()
:precondition
(and
(not (LEFTOF3-ROBOT))
)
:effect
(and
(Flag51prime-)
(not (Flag51-))
)

)
(:action Flag53Action
:parameters ()
:precondition
(and
(LEFTOF4-ROBOT)
(RIGHTOF4-ROBOT)
)
:effect
(and
(Flag53-)
(Flag53prime-)
)

)
(:action Flag54Action
:parameters ()
:precondition
(and
(LEFTOF4-ROBOT)
(RIGHTOF7-ROBOT)
)
:effect
(and
(Flag54-)
(Flag54prime-)
)

)
(:action Flag55Action
:parameters ()
:precondition
(and
(LEFTOF3-ROBOT)
(RIGHTOF3-ROBOT)
)
:effect
(and
(Flag55-)
(Flag55prime-)
)

)
(:action Flag56Action
:parameters ()
:precondition
(and
(RIGHTOF8-ROBOT)
(LEFTOF4-ROBOT)
)
:effect
(and
(Flag56-)
(Flag56prime-)
)

)
(:action Flag57Action
:parameters ()
:precondition
(and
(LEFTOF4-ROBOT)
(RIGHTOF9-ROBOT)
)
:effect
(and
(Flag57-)
(Flag57prime-)
)

)
(:action Flag58Action
:parameters ()
:precondition
(and
(LEFTOF4-ROBOT)
(RIGHTOF6-ROBOT)
)
:effect
(and
(Flag58-)
(Flag58prime-)
)

)
(:action Flag59Action
:parameters ()
:precondition
(and
(LEFTOF4-ROBOT)
(RIGHTOF5-ROBOT)
)
:effect
(and
(Flag59-)
(Flag59prime-)
)

)
(:action Flag60Action
:parameters ()
:precondition
(and
(RIGHTOF3-ROBOT)
(LEFTOF4-ROBOT)
)
:effect
(and
(Flag60-)
(Flag60prime-)
)

)
(:action Prim53Action-0
:parameters ()
:precondition
(and
(not (LEFTOF4-ROBOT))
)
:effect
(and
(Flag53prime-)
(not (Flag53-))
)

)
(:action Prim53Action-1
:parameters ()
:precondition
(and
(not (RIGHTOF4-ROBOT))
)
:effect
(and
(Flag53prime-)
(not (Flag53-))
)

)
(:action Prim54Action-0
:parameters ()
:precondition
(and
(not (LEFTOF4-ROBOT))
)
:effect
(and
(Flag54prime-)
(not (Flag54-))
)

)
(:action Prim54Action-1
:parameters ()
:precondition
(and
(not (RIGHTOF7-ROBOT))
)
:effect
(and
(Flag54prime-)
(not (Flag54-))
)

)
(:action Prim55Action-0
:parameters ()
:precondition
(and
(not (LEFTOF3-ROBOT))
)
:effect
(and
(Flag55prime-)
(not (Flag55-))
)

)
(:action Prim55Action-1
:parameters ()
:precondition
(and
(not (RIGHTOF3-ROBOT))
)
:effect
(and
(Flag55prime-)
(not (Flag55-))
)

)
(:action Prim56Action-0
:parameters ()
:precondition
(and
(not (RIGHTOF8-ROBOT))
)
:effect
(and
(Flag56prime-)
(not (Flag56-))
)

)
(:action Prim56Action-1
:parameters ()
:precondition
(and
(not (LEFTOF4-ROBOT))
)
:effect
(and
(Flag56prime-)
(not (Flag56-))
)

)
(:action Prim57Action-0
:parameters ()
:precondition
(and
(not (LEFTOF4-ROBOT))
)
:effect
(and
(Flag57prime-)
(not (Flag57-))
)

)
(:action Prim57Action-1
:parameters ()
:precondition
(and
(not (RIGHTOF9-ROBOT))
)
:effect
(and
(Flag57prime-)
(not (Flag57-))
)

)
(:action Prim58Action-0
:parameters ()
:precondition
(and
(not (LEFTOF4-ROBOT))
)
:effect
(and
(Flag58prime-)
(not (Flag58-))
)

)
(:action Prim58Action-1
:parameters ()
:precondition
(and
(not (RIGHTOF6-ROBOT))
)
:effect
(and
(Flag58prime-)
(not (Flag58-))
)

)
(:action Prim59Action-0
:parameters ()
:precondition
(and
(not (LEFTOF4-ROBOT))
)
:effect
(and
(Flag59prime-)
(not (Flag59-))
)

)
(:action Prim59Action-1
:parameters ()
:precondition
(and
(not (RIGHTOF5-ROBOT))
)
:effect
(and
(Flag59prime-)
(not (Flag59-))
)

)
(:action Prim60Action-0
:parameters ()
:precondition
(and
(not (RIGHTOF3-ROBOT))
)
:effect
(and
(Flag60prime-)
(not (Flag60-))
)

)
(:action Prim60Action-1
:parameters ()
:precondition
(and
(not (LEFTOF4-ROBOT))
)
:effect
(and
(Flag60prime-)
(not (Flag60-))
)

)
(:action Flag62Action
:parameters ()
:precondition
(and
(RIGHTOF9-ROBOT)
(LEFTOF5-ROBOT)
)
:effect
(and
(Flag62-)
(Flag62prime-)
)

)
(:action Flag63Action
:parameters ()
:precondition
(and
(RIGHTOF7-ROBOT)
(LEFTOF5-ROBOT)
)
:effect
(and
(Flag63-)
(Flag63prime-)
)

)
(:action Flag64Action
:parameters ()
:precondition
(and
(RIGHTOF4-ROBOT)
(LEFTOF5-ROBOT)
)
:effect
(and
(Flag64-)
(Flag64prime-)
)

)
(:action Flag65Action
:parameters ()
:precondition
(and
(LEFTOF4-ROBOT)
(RIGHTOF8-ROBOT)
)
:effect
(and
(Flag65-)
(Flag65prime-)
)

)
(:action Flag66Action
:parameters ()
:precondition
(and
(LEFTOF5-ROBOT)
(RIGHTOF6-ROBOT)
)
:effect
(and
(Flag66-)
(Flag66prime-)
)

)
(:action Flag67Action
:parameters ()
:precondition
(and
(RIGHTOF8-ROBOT)
(LEFTOF5-ROBOT)
)
:effect
(and
(Flag67-)
(Flag67prime-)
)

)
(:action Flag68Action
:parameters ()
:precondition
(and
(RIGHTOF5-ROBOT)
(LEFTOF5-ROBOT)
)
:effect
(and
(Flag68-)
(Flag68prime-)
)

)
(:action Prim62Action-0
:parameters ()
:precondition
(and
(not (RIGHTOF9-ROBOT))
)
:effect
(and
(Flag62prime-)
(not (Flag62-))
)

)
(:action Prim62Action-1
:parameters ()
:precondition
(and
(not (LEFTOF5-ROBOT))
)
:effect
(and
(Flag62prime-)
(not (Flag62-))
)

)
(:action Prim63Action-0
:parameters ()
:precondition
(and
(not (RIGHTOF7-ROBOT))
)
:effect
(and
(Flag63prime-)
(not (Flag63-))
)

)
(:action Prim63Action-1
:parameters ()
:precondition
(and
(not (LEFTOF5-ROBOT))
)
:effect
(and
(Flag63prime-)
(not (Flag63-))
)

)
(:action Prim64Action-0
:parameters ()
:precondition
(and
(not (RIGHTOF4-ROBOT))
)
:effect
(and
(Flag64prime-)
(not (Flag64-))
)

)
(:action Prim64Action-1
:parameters ()
:precondition
(and
(not (LEFTOF5-ROBOT))
)
:effect
(and
(Flag64prime-)
(not (Flag64-))
)

)
(:action Prim65Action-0
:parameters ()
:precondition
(and
(not (LEFTOF4-ROBOT))
)
:effect
(and
(Flag65prime-)
(not (Flag65-))
)

)
(:action Prim65Action-1
:parameters ()
:precondition
(and
(not (RIGHTOF8-ROBOT))
)
:effect
(and
(Flag65prime-)
(not (Flag65-))
)

)
(:action Prim66Action-0
:parameters ()
:precondition
(and
(not (LEFTOF5-ROBOT))
)
:effect
(and
(Flag66prime-)
(not (Flag66-))
)

)
(:action Prim66Action-1
:parameters ()
:precondition
(and
(not (RIGHTOF6-ROBOT))
)
:effect
(and
(Flag66prime-)
(not (Flag66-))
)

)
(:action Prim67Action-0
:parameters ()
:precondition
(and
(not (RIGHTOF8-ROBOT))
)
:effect
(and
(Flag67prime-)
(not (Flag67-))
)

)
(:action Prim67Action-1
:parameters ()
:precondition
(and
(not (LEFTOF5-ROBOT))
)
:effect
(and
(Flag67prime-)
(not (Flag67-))
)

)
(:action Prim68Action-0
:parameters ()
:precondition
(and
(not (RIGHTOF5-ROBOT))
)
:effect
(and
(Flag68prime-)
(not (Flag68-))
)

)
(:action Prim68Action-1
:parameters ()
:precondition
(and
(not (LEFTOF5-ROBOT))
)
:effect
(and
(Flag68prime-)
(not (Flag68-))
)

)
(:action Flag70Action
:parameters ()
:precondition
(and
(RIGHTOF8-ROBOT)
(LEFTOF6-ROBOT)
)
:effect
(and
(Flag70-)
(Flag70prime-)
)

)
(:action Flag71Action
:parameters ()
:precondition
(and
(RIGHTOF5-ROBOT)
(LEFTOF6-ROBOT)
)
:effect
(and
(Flag71-)
(Flag71prime-)
)

)
(:action Flag72Action
:parameters ()
:precondition
(and
(RIGHTOF7-ROBOT)
(LEFTOF6-ROBOT)
)
:effect
(and
(Flag72-)
(Flag72prime-)
)

)
(:action Flag73Action
:parameters ()
:precondition
(and
(RIGHTOF6-ROBOT)
(LEFTOF6-ROBOT)
)
:effect
(and
(Flag73-)
(Flag73prime-)
)

)
(:action Flag74Action
:parameters ()
:precondition
(and
(RIGHTOF9-ROBOT)
(LEFTOF6-ROBOT)
)
:effect
(and
(Flag74-)
(Flag74prime-)
)

)
(:action Prim70Action-0
:parameters ()
:precondition
(and
(not (RIGHTOF8-ROBOT))
)
:effect
(and
(Flag70prime-)
(not (Flag70-))
)

)
(:action Prim70Action-1
:parameters ()
:precondition
(and
(not (LEFTOF6-ROBOT))
)
:effect
(and
(Flag70prime-)
(not (Flag70-))
)

)
(:action Prim71Action-0
:parameters ()
:precondition
(and
(not (RIGHTOF5-ROBOT))
)
:effect
(and
(Flag71prime-)
(not (Flag71-))
)

)
(:action Prim71Action-1
:parameters ()
:precondition
(and
(not (LEFTOF6-ROBOT))
)
:effect
(and
(Flag71prime-)
(not (Flag71-))
)

)
(:action Prim72Action-0
:parameters ()
:precondition
(and
(not (RIGHTOF7-ROBOT))
)
:effect
(and
(Flag72prime-)
(not (Flag72-))
)

)
(:action Prim72Action-1
:parameters ()
:precondition
(and
(not (LEFTOF6-ROBOT))
)
:effect
(and
(Flag72prime-)
(not (Flag72-))
)

)
(:action Prim73Action-0
:parameters ()
:precondition
(and
(not (RIGHTOF6-ROBOT))
)
:effect
(and
(Flag73prime-)
(not (Flag73-))
)

)
(:action Prim73Action-1
:parameters ()
:precondition
(and
(not (LEFTOF6-ROBOT))
)
:effect
(and
(Flag73prime-)
(not (Flag73-))
)

)
(:action Prim74Action-0
:parameters ()
:precondition
(and
(not (RIGHTOF9-ROBOT))
)
:effect
(and
(Flag74prime-)
(not (Flag74-))
)

)
(:action Prim74Action-1
:parameters ()
:precondition
(and
(not (LEFTOF6-ROBOT))
)
:effect
(and
(Flag74prime-)
(not (Flag74-))
)

)
(:action Flag76Action
:parameters ()
:precondition
(and
(LEFTOF7-ROBOT)
(RIGHTOF7-ROBOT)
)
:effect
(and
(Flag76-)
(Flag76prime-)
)

)
(:action Flag77Action
:parameters ()
:precondition
(and
(RIGHTOF8-ROBOT)
(LEFTOF7-ROBOT)
)
:effect
(and
(Flag77-)
(Flag77prime-)
)

)
(:action Flag78Action
:parameters ()
:precondition
(and
(LEFTOF6-ROBOT)
(RIGHTOF6-ROBOT)
)
:effect
(and
(Flag78-)
(Flag78prime-)
)

)
(:action Flag79Action
:parameters ()
:precondition
(and
(LEFTOF7-ROBOT)
(RIGHTOF9-ROBOT)
)
:effect
(and
(Flag79-)
(Flag79prime-)
)

)
(:action Flag80Action
:parameters ()
:precondition
(and
(LEFTOF7-ROBOT)
(RIGHTOF6-ROBOT)
)
:effect
(and
(Flag80-)
(Flag80prime-)
)

)
(:action Prim76Action-0
:parameters ()
:precondition
(and
(not (LEFTOF7-ROBOT))
)
:effect
(and
(Flag76prime-)
(not (Flag76-))
)

)
(:action Prim76Action-1
:parameters ()
:precondition
(and
(not (RIGHTOF7-ROBOT))
)
:effect
(and
(Flag76prime-)
(not (Flag76-))
)

)
(:action Prim77Action-0
:parameters ()
:precondition
(and
(not (RIGHTOF8-ROBOT))
)
:effect
(and
(Flag77prime-)
(not (Flag77-))
)

)
(:action Prim77Action-1
:parameters ()
:precondition
(and
(not (LEFTOF7-ROBOT))
)
:effect
(and
(Flag77prime-)
(not (Flag77-))
)

)
(:action Prim78Action-0
:parameters ()
:precondition
(and
(not (LEFTOF6-ROBOT))
)
:effect
(and
(Flag78prime-)
(not (Flag78-))
)

)
(:action Prim78Action-1
:parameters ()
:precondition
(and
(not (RIGHTOF6-ROBOT))
)
:effect
(and
(Flag78prime-)
(not (Flag78-))
)

)
(:action Prim79Action-0
:parameters ()
:precondition
(and
(not (LEFTOF7-ROBOT))
)
:effect
(and
(Flag79prime-)
(not (Flag79-))
)

)
(:action Prim79Action-1
:parameters ()
:precondition
(and
(not (RIGHTOF9-ROBOT))
)
:effect
(and
(Flag79prime-)
(not (Flag79-))
)

)
(:action Prim80Action-0
:parameters ()
:precondition
(and
(not (LEFTOF7-ROBOT))
)
:effect
(and
(Flag80prime-)
(not (Flag80-))
)

)
(:action Prim80Action-1
:parameters ()
:precondition
(and
(not (RIGHTOF6-ROBOT))
)
:effect
(and
(Flag80prime-)
(not (Flag80-))
)

)
(:action Flag82Action
:parameters ()
:precondition
(and
(RIGHTOF7-ROBOT)
(LEFTOF8-ROBOT)
)
:effect
(and
(Flag82-)
(Flag82prime-)
)

)
(:action Flag83Action
:parameters ()
:precondition
(and
(LEFTOF8-ROBOT)
(RIGHTOF9-ROBOT)
)
:effect
(and
(Flag83-)
(Flag83prime-)
)

)
(:action Flag84Action
:parameters ()
:precondition
(and
(RIGHTOF8-ROBOT)
(LEFTOF8-ROBOT)
)
:effect
(and
(Flag84-)
(Flag84prime-)
)

)
(:action Flag85Action
:parameters ()
:precondition
(and
(RIGHTOF7-ROBOT)
(LEFTOF7-ROBOT)
)
:effect
(and
(Flag85-)
(Flag85prime-)
)

)
(:action Prim82Action-0
:parameters ()
:precondition
(and
(not (RIGHTOF7-ROBOT))
)
:effect
(and
(Flag82prime-)
(not (Flag82-))
)

)
(:action Prim82Action-1
:parameters ()
:precondition
(and
(not (LEFTOF8-ROBOT))
)
:effect
(and
(Flag82prime-)
(not (Flag82-))
)

)
(:action Prim83Action-0
:parameters ()
:precondition
(and
(not (LEFTOF8-ROBOT))
)
:effect
(and
(Flag83prime-)
(not (Flag83-))
)

)
(:action Prim83Action-1
:parameters ()
:precondition
(and
(not (RIGHTOF9-ROBOT))
)
:effect
(and
(Flag83prime-)
(not (Flag83-))
)

)
(:action Prim84Action-0
:parameters ()
:precondition
(and
(not (RIGHTOF8-ROBOT))
)
:effect
(and
(Flag84prime-)
(not (Flag84-))
)

)
(:action Prim84Action-1
:parameters ()
:precondition
(and
(not (LEFTOF8-ROBOT))
)
:effect
(and
(Flag84prime-)
(not (Flag84-))
)

)
(:action Prim85Action-0
:parameters ()
:precondition
(and
(not (RIGHTOF7-ROBOT))
)
:effect
(and
(Flag85prime-)
(not (Flag85-))
)

)
(:action Prim85Action-1
:parameters ()
:precondition
(and
(not (LEFTOF7-ROBOT))
)
:effect
(and
(Flag85prime-)
(not (Flag85-))
)

)
(:action Flag87Action
:parameters ()
:precondition
(and
(LEFTOF9-ROBOT)
(RIGHTOF9-ROBOT)
)
:effect
(and
(Flag87-)
(Flag87prime-)
)

)
(:action Flag88Action
:parameters ()
:precondition
(and
(RIGHTOF8-ROBOT)
(LEFTOF9-ROBOT)
)
:effect
(and
(Flag88-)
(Flag88prime-)
)

)
(:action Prim87Action-1
:parameters ()
:precondition
(and
(not (RIGHTOF9-ROBOT))
)
:effect
(and
(Flag87prime-)
(not (Flag87-))
)

)
(:action Prim88Action-0
:parameters ()
:precondition
(and
(not (RIGHTOF8-ROBOT))
)
:effect
(and
(Flag88prime-)
(not (Flag88-))
)

)
(:action Flag125Action-0
:parameters ()
:precondition
(and
(Flag107-)
(Flag107prime-)
)
:effect
(and
(Flag125-)
(Flag125prime-)
)

)
(:action Flag125Action-1
:parameters ()
:precondition
(and
(Flag108-)
(Flag108prime-)
)
:effect
(and
(Flag125-)
(Flag125prime-)
)

)
(:action Flag125Action-2
:parameters ()
:precondition
(and
(Flag109-)
(Flag109prime-)
)
:effect
(and
(Flag125-)
(Flag125prime-)
)

)
(:action Flag125Action-3
:parameters ()
:precondition
(and
(Flag110-)
(Flag110prime-)
)
:effect
(and
(Flag125-)
(Flag125prime-)
)

)
(:action Flag125Action-4
:parameters ()
:precondition
(and
(Flag111-)
(Flag111prime-)
)
:effect
(and
(Flag125-)
(Flag125prime-)
)

)
(:action Flag125Action-5
:parameters ()
:precondition
(and
(Flag112-)
(Flag112prime-)
)
:effect
(and
(Flag125-)
(Flag125prime-)
)

)
(:action Flag125Action-6
:parameters ()
:precondition
(and
(Flag113-)
(Flag113prime-)
)
:effect
(and
(Flag125-)
(Flag125prime-)
)

)
(:action Flag125Action-7
:parameters ()
:precondition
(and
(Flag114-)
(Flag114prime-)
)
:effect
(and
(Flag125-)
(Flag125prime-)
)

)
(:action Flag125Action-8
:parameters ()
:precondition
(and
(Flag115-)
(Flag115prime-)
)
:effect
(and
(Flag125-)
(Flag125prime-)
)

)
(:action Flag125Action-9
:parameters ()
:precondition
(and
(Flag116-)
(Flag116prime-)
)
:effect
(and
(Flag125-)
(Flag125prime-)
)

)
(:action Flag125Action-10
:parameters ()
:precondition
(and
(Flag117-)
(Flag117prime-)
)
:effect
(and
(Flag125-)
(Flag125prime-)
)

)
(:action Flag125Action-11
:parameters ()
:precondition
(and
(Flag118-)
(Flag118prime-)
)
:effect
(and
(Flag125-)
(Flag125prime-)
)

)
(:action Flag125Action-12
:parameters ()
:precondition
(and
(Flag119-)
(Flag119prime-)
)
:effect
(and
(Flag125-)
(Flag125prime-)
)

)
(:action Flag125Action-13
:parameters ()
:precondition
(and
(Flag120-)
(Flag120prime-)
)
:effect
(and
(Flag125-)
(Flag125prime-)
)

)
(:action Flag125Action-14
:parameters ()
:precondition
(and
(Flag121-)
(Flag121prime-)
)
:effect
(and
(Flag125-)
(Flag125prime-)
)

)
(:action Flag125Action-15
:parameters ()
:precondition
(and
(Flag122-)
(Flag122prime-)
)
:effect
(and
(Flag125-)
(Flag125prime-)
)

)
(:action Flag125Action-16
:parameters ()
:precondition
(and
(Flag123-)
(Flag123prime-)
)
:effect
(and
(Flag125-)
(Flag125prime-)
)

)
(:action Flag125Action-17
:parameters ()
:precondition
(and
(Flag124-)
(Flag124prime-)
)
:effect
(and
(Flag125-)
(Flag125prime-)
)

)
(:action Prim125Action
:parameters ()
:precondition
(and
(not (Flag107-))
(Flag107prime-)
(not (Flag108-))
(Flag108prime-)
(not (Flag109-))
(Flag109prime-)
(not (Flag110-))
(Flag110prime-)
(not (Flag111-))
(Flag111prime-)
(not (Flag112-))
(Flag112prime-)
(not (Flag113-))
(Flag113prime-)
(not (Flag114-))
(Flag114prime-)
(not (Flag115-))
(Flag115prime-)
(not (Flag116-))
(Flag116prime-)
(not (Flag117-))
(Flag117prime-)
(not (Flag118-))
(Flag118prime-)
(not (Flag119-))
(Flag119prime-)
(not (Flag120-))
(Flag120prime-)
(not (Flag121-))
(Flag121prime-)
(not (Flag122-))
(Flag122prime-)
(not (Flag123-))
(Flag123prime-)
(not (Flag124-))
(Flag124prime-)
)
:effect
(and
(Flag125prime-)
(not (Flag125-))
)

)
(:action Flag135Action-0
:parameters ()
:precondition
(and
(Flag126-)
(Flag126prime-)
)
:effect
(and
(Flag135-)
(Flag135prime-)
)

)
(:action Flag135Action-1
:parameters ()
:precondition
(and
(Flag127-)
(Flag127prime-)
)
:effect
(and
(Flag135-)
(Flag135prime-)
)

)
(:action Flag135Action-2
:parameters ()
:precondition
(and
(Flag107-)
(Flag107prime-)
)
:effect
(and
(Flag135-)
(Flag135prime-)
)

)
(:action Flag135Action-3
:parameters ()
:precondition
(and
(Flag128-)
(Flag128prime-)
)
:effect
(and
(Flag135-)
(Flag135prime-)
)

)
(:action Flag135Action-4
:parameters ()
:precondition
(and
(Flag120-)
(Flag120prime-)
)
:effect
(and
(Flag135-)
(Flag135prime-)
)

)
(:action Flag135Action-5
:parameters ()
:precondition
(and
(Flag112-)
(Flag112prime-)
)
:effect
(and
(Flag135-)
(Flag135prime-)
)

)
(:action Flag135Action-6
:parameters ()
:precondition
(and
(Flag114-)
(Flag114prime-)
)
:effect
(and
(Flag135-)
(Flag135prime-)
)

)
(:action Flag135Action-7
:parameters ()
:precondition
(and
(Flag129-)
(Flag129prime-)
)
:effect
(and
(Flag135-)
(Flag135prime-)
)

)
(:action Flag135Action-8
:parameters ()
:precondition
(and
(Flag115-)
(Flag115prime-)
)
:effect
(and
(Flag135-)
(Flag135prime-)
)

)
(:action Flag135Action-9
:parameters ()
:precondition
(and
(Flag130-)
(Flag130prime-)
)
:effect
(and
(Flag135-)
(Flag135prime-)
)

)
(:action Flag135Action-10
:parameters ()
:precondition
(and
(Flag108-)
(Flag108prime-)
)
:effect
(and
(Flag135-)
(Flag135prime-)
)

)
(:action Flag135Action-11
:parameters ()
:precondition
(and
(Flag110-)
(Flag110prime-)
)
:effect
(and
(Flag135-)
(Flag135prime-)
)

)
(:action Flag135Action-12
:parameters ()
:precondition
(and
(Flag111-)
(Flag111prime-)
)
:effect
(and
(Flag135-)
(Flag135prime-)
)

)
(:action Flag135Action-13
:parameters ()
:precondition
(and
(Flag131-)
(Flag131prime-)
)
:effect
(and
(Flag135-)
(Flag135prime-)
)

)
(:action Flag135Action-14
:parameters ()
:precondition
(and
(Flag113-)
(Flag113prime-)
)
:effect
(and
(Flag135-)
(Flag135prime-)
)

)
(:action Flag135Action-15
:parameters ()
:precondition
(and
(Flag132-)
(Flag132prime-)
)
:effect
(and
(Flag135-)
(Flag135prime-)
)

)
(:action Flag135Action-16
:parameters ()
:precondition
(and
(Flag133-)
(Flag133prime-)
)
:effect
(and
(Flag135-)
(Flag135prime-)
)

)
(:action Flag135Action-17
:parameters ()
:precondition
(and
(Flag119-)
(Flag119prime-)
)
:effect
(and
(Flag135-)
(Flag135prime-)
)

)
(:action Flag135Action-18
:parameters ()
:precondition
(and
(Flag121-)
(Flag121prime-)
)
:effect
(and
(Flag135-)
(Flag135prime-)
)

)
(:action Flag135Action-19
:parameters ()
:precondition
(and
(Flag123-)
(Flag123prime-)
)
:effect
(and
(Flag135-)
(Flag135prime-)
)

)
(:action Flag135Action-20
:parameters ()
:precondition
(and
(Flag134-)
(Flag134prime-)
)
:effect
(and
(Flag135-)
(Flag135prime-)
)

)
(:action Flag135Action-21
:parameters ()
:precondition
(and
(Flag116-)
(Flag116prime-)
)
:effect
(and
(Flag135-)
(Flag135prime-)
)

)
(:action Flag135Action-22
:parameters ()
:precondition
(and
(Flag118-)
(Flag118prime-)
)
:effect
(and
(Flag135-)
(Flag135prime-)
)

)
(:action Flag135Action-23
:parameters ()
:precondition
(and
(Flag122-)
(Flag122prime-)
)
:effect
(and
(Flag135-)
(Flag135prime-)
)

)
(:action Prim135Action
:parameters ()
:precondition
(and
(not (Flag126-))
(Flag126prime-)
(not (Flag127-))
(Flag127prime-)
(not (Flag107-))
(Flag107prime-)
(not (Flag128-))
(Flag128prime-)
(not (Flag120-))
(Flag120prime-)
(not (Flag112-))
(Flag112prime-)
(not (Flag114-))
(Flag114prime-)
(not (Flag129-))
(Flag129prime-)
(not (Flag115-))
(Flag115prime-)
(not (Flag130-))
(Flag130prime-)
(not (Flag108-))
(Flag108prime-)
(not (Flag110-))
(Flag110prime-)
(not (Flag111-))
(Flag111prime-)
(not (Flag131-))
(Flag131prime-)
(not (Flag113-))
(Flag113prime-)
(not (Flag132-))
(Flag132prime-)
(not (Flag133-))
(Flag133prime-)
(not (Flag119-))
(Flag119prime-)
(not (Flag121-))
(Flag121prime-)
(not (Flag123-))
(Flag123prime-)
(not (Flag134-))
(Flag134prime-)
(not (Flag116-))
(Flag116prime-)
(not (Flag118-))
(Flag118prime-)
(not (Flag122-))
(Flag122prime-)
)
:effect
(and
(Flag135prime-)
(not (Flag135-))
)

)
(:action Flag144Action-0
:parameters ()
:precondition
(and
(Flag126-)
(Flag126prime-)
)
:effect
(and
(Flag144-)
(Flag144prime-)
)

)
(:action Flag144Action-1
:parameters ()
:precondition
(and
(Flag136-)
(Flag136prime-)
)
:effect
(and
(Flag144-)
(Flag144prime-)
)

)
(:action Flag144Action-2
:parameters ()
:precondition
(and
(Flag137-)
(Flag137prime-)
)
:effect
(and
(Flag144-)
(Flag144prime-)
)

)
(:action Flag144Action-3
:parameters ()
:precondition
(and
(Flag127-)
(Flag127prime-)
)
:effect
(and
(Flag144-)
(Flag144prime-)
)

)
(:action Flag144Action-4
:parameters ()
:precondition
(and
(Flag128-)
(Flag128prime-)
)
:effect
(and
(Flag144-)
(Flag144prime-)
)

)
(:action Flag144Action-5
:parameters ()
:precondition
(and
(Flag120-)
(Flag120prime-)
)
:effect
(and
(Flag144-)
(Flag144prime-)
)

)
(:action Flag144Action-6
:parameters ()
:precondition
(and
(Flag129-)
(Flag129prime-)
)
:effect
(and
(Flag144-)
(Flag144prime-)
)

)
(:action Flag144Action-7
:parameters ()
:precondition
(and
(Flag112-)
(Flag112prime-)
)
:effect
(and
(Flag144-)
(Flag144prime-)
)

)
(:action Flag144Action-8
:parameters ()
:precondition
(and
(Flag138-)
(Flag138prime-)
)
:effect
(and
(Flag144-)
(Flag144prime-)
)

)
(:action Flag144Action-9
:parameters ()
:precondition
(and
(Flag114-)
(Flag114prime-)
)
:effect
(and
(Flag144-)
(Flag144prime-)
)

)
(:action Flag144Action-10
:parameters ()
:precondition
(and
(Flag139-)
(Flag139prime-)
)
:effect
(and
(Flag144-)
(Flag144prime-)
)

)
(:action Flag144Action-11
:parameters ()
:precondition
(and
(Flag140-)
(Flag140prime-)
)
:effect
(and
(Flag144-)
(Flag144prime-)
)

)
(:action Flag144Action-12
:parameters ()
:precondition
(and
(Flag141-)
(Flag141prime-)
)
:effect
(and
(Flag144-)
(Flag144prime-)
)

)
(:action Flag144Action-13
:parameters ()
:precondition
(and
(Flag115-)
(Flag115prime-)
)
:effect
(and
(Flag144-)
(Flag144prime-)
)

)
(:action Flag144Action-14
:parameters ()
:precondition
(and
(Flag142-)
(Flag142prime-)
)
:effect
(and
(Flag144-)
(Flag144prime-)
)

)
(:action Flag144Action-15
:parameters ()
:precondition
(and
(Flag143-)
(Flag143prime-)
)
:effect
(and
(Flag144-)
(Flag144prime-)
)

)
(:action Flag144Action-16
:parameters ()
:precondition
(and
(Flag108-)
(Flag108prime-)
)
:effect
(and
(Flag144-)
(Flag144prime-)
)

)
(:action Flag144Action-17
:parameters ()
:precondition
(and
(Flag111-)
(Flag111prime-)
)
:effect
(and
(Flag144-)
(Flag144prime-)
)

)
(:action Flag144Action-18
:parameters ()
:precondition
(and
(Flag131-)
(Flag131prime-)
)
:effect
(and
(Flag144-)
(Flag144prime-)
)

)
(:action Flag144Action-19
:parameters ()
:precondition
(and
(Flag113-)
(Flag113prime-)
)
:effect
(and
(Flag144-)
(Flag144prime-)
)

)
(:action Flag144Action-20
:parameters ()
:precondition
(and
(Flag132-)
(Flag132prime-)
)
:effect
(and
(Flag144-)
(Flag144prime-)
)

)
(:action Flag144Action-21
:parameters ()
:precondition
(and
(Flag119-)
(Flag119prime-)
)
:effect
(and
(Flag144-)
(Flag144prime-)
)

)
(:action Flag144Action-22
:parameters ()
:precondition
(and
(Flag121-)
(Flag121prime-)
)
:effect
(and
(Flag144-)
(Flag144prime-)
)

)
(:action Flag144Action-23
:parameters ()
:precondition
(and
(Flag123-)
(Flag123prime-)
)
:effect
(and
(Flag144-)
(Flag144prime-)
)

)
(:action Flag144Action-24
:parameters ()
:precondition
(and
(Flag110-)
(Flag110prime-)
)
:effect
(and
(Flag144-)
(Flag144prime-)
)

)
(:action Flag144Action-25
:parameters ()
:precondition
(and
(Flag116-)
(Flag116prime-)
)
:effect
(and
(Flag144-)
(Flag144prime-)
)

)
(:action Flag144Action-26
:parameters ()
:precondition
(and
(Flag118-)
(Flag118prime-)
)
:effect
(and
(Flag144-)
(Flag144prime-)
)

)
(:action Flag144Action-27
:parameters ()
:precondition
(and
(Flag122-)
(Flag122prime-)
)
:effect
(and
(Flag144-)
(Flag144prime-)
)

)
(:action Prim144Action
:parameters ()
:precondition
(and
(not (Flag126-))
(Flag126prime-)
(not (Flag136-))
(Flag136prime-)
(not (Flag137-))
(Flag137prime-)
(not (Flag127-))
(Flag127prime-)
(not (Flag128-))
(Flag128prime-)
(not (Flag120-))
(Flag120prime-)
(not (Flag129-))
(Flag129prime-)
(not (Flag112-))
(Flag112prime-)
(not (Flag138-))
(Flag138prime-)
(not (Flag114-))
(Flag114prime-)
(not (Flag139-))
(Flag139prime-)
(not (Flag140-))
(Flag140prime-)
(not (Flag141-))
(Flag141prime-)
(not (Flag115-))
(Flag115prime-)
(not (Flag142-))
(Flag142prime-)
(not (Flag143-))
(Flag143prime-)
(not (Flag108-))
(Flag108prime-)
(not (Flag111-))
(Flag111prime-)
(not (Flag131-))
(Flag131prime-)
(not (Flag113-))
(Flag113prime-)
(not (Flag132-))
(Flag132prime-)
(not (Flag119-))
(Flag119prime-)
(not (Flag121-))
(Flag121prime-)
(not (Flag123-))
(Flag123prime-)
(not (Flag110-))
(Flag110prime-)
(not (Flag116-))
(Flag116prime-)
(not (Flag118-))
(Flag118prime-)
(not (Flag122-))
(Flag122prime-)
)
:effect
(and
(Flag144prime-)
(not (Flag144-))
)

)
(:action Flag151Action-0
:parameters ()
:precondition
(and
(Flag145-)
(Flag145prime-)
)
:effect
(and
(Flag151-)
(Flag151prime-)
)

)
(:action Flag151Action-1
:parameters ()
:precondition
(and
(Flag126-)
(Flag126prime-)
)
:effect
(and
(Flag151-)
(Flag151prime-)
)

)
(:action Flag151Action-2
:parameters ()
:precondition
(and
(Flag127-)
(Flag127prime-)
)
:effect
(and
(Flag151-)
(Flag151prime-)
)

)
(:action Flag151Action-3
:parameters ()
:precondition
(and
(Flag128-)
(Flag128prime-)
)
:effect
(and
(Flag151-)
(Flag151prime-)
)

)
(:action Flag151Action-4
:parameters ()
:precondition
(and
(Flag120-)
(Flag120prime-)
)
:effect
(and
(Flag151-)
(Flag151prime-)
)

)
(:action Flag151Action-5
:parameters ()
:precondition
(and
(Flag129-)
(Flag129prime-)
)
:effect
(and
(Flag151-)
(Flag151prime-)
)

)
(:action Flag151Action-6
:parameters ()
:precondition
(and
(Flag146-)
(Flag146prime-)
)
:effect
(and
(Flag151-)
(Flag151prime-)
)

)
(:action Flag151Action-7
:parameters ()
:precondition
(and
(Flag112-)
(Flag112prime-)
)
:effect
(and
(Flag151-)
(Flag151prime-)
)

)
(:action Flag151Action-8
:parameters ()
:precondition
(and
(Flag116-)
(Flag116prime-)
)
:effect
(and
(Flag151-)
(Flag151prime-)
)

)
(:action Flag151Action-9
:parameters ()
:precondition
(and
(Flag138-)
(Flag138prime-)
)
:effect
(and
(Flag151-)
(Flag151prime-)
)

)
(:action Flag151Action-10
:parameters ()
:precondition
(and
(Flag114-)
(Flag114prime-)
)
:effect
(and
(Flag151-)
(Flag151prime-)
)

)
(:action Flag151Action-11
:parameters ()
:precondition
(and
(Flag139-)
(Flag139prime-)
)
:effect
(and
(Flag151-)
(Flag151prime-)
)

)
(:action Flag151Action-12
:parameters ()
:precondition
(and
(Flag140-)
(Flag140prime-)
)
:effect
(and
(Flag151-)
(Flag151prime-)
)

)
(:action Flag151Action-13
:parameters ()
:precondition
(and
(Flag141-)
(Flag141prime-)
)
:effect
(and
(Flag151-)
(Flag151prime-)
)

)
(:action Flag151Action-14
:parameters ()
:precondition
(and
(Flag115-)
(Flag115prime-)
)
:effect
(and
(Flag151-)
(Flag151prime-)
)

)
(:action Flag151Action-15
:parameters ()
:precondition
(and
(Flag142-)
(Flag142prime-)
)
:effect
(and
(Flag151-)
(Flag151prime-)
)

)
(:action Flag151Action-16
:parameters ()
:precondition
(and
(Flag108-)
(Flag108prime-)
)
:effect
(and
(Flag151-)
(Flag151prime-)
)

)
(:action Flag151Action-17
:parameters ()
:precondition
(and
(Flag111-)
(Flag111prime-)
)
:effect
(and
(Flag151-)
(Flag151prime-)
)

)
(:action Flag151Action-18
:parameters ()
:precondition
(and
(Flag131-)
(Flag131prime-)
)
:effect
(and
(Flag151-)
(Flag151prime-)
)

)
(:action Flag151Action-19
:parameters ()
:precondition
(and
(Flag113-)
(Flag113prime-)
)
:effect
(and
(Flag151-)
(Flag151prime-)
)

)
(:action Flag151Action-20
:parameters ()
:precondition
(and
(Flag147-)
(Flag147prime-)
)
:effect
(and
(Flag151-)
(Flag151prime-)
)

)
(:action Flag151Action-21
:parameters ()
:precondition
(and
(Flag132-)
(Flag132prime-)
)
:effect
(and
(Flag151-)
(Flag151prime-)
)

)
(:action Flag151Action-22
:parameters ()
:precondition
(and
(Flag148-)
(Flag148prime-)
)
:effect
(and
(Flag151-)
(Flag151prime-)
)

)
(:action Flag151Action-23
:parameters ()
:precondition
(and
(Flag137-)
(Flag137prime-)
)
:effect
(and
(Flag151-)
(Flag151prime-)
)

)
(:action Flag151Action-24
:parameters ()
:precondition
(and
(Flag121-)
(Flag121prime-)
)
:effect
(and
(Flag151-)
(Flag151prime-)
)

)
(:action Flag151Action-25
:parameters ()
:precondition
(and
(Flag123-)
(Flag123prime-)
)
:effect
(and
(Flag151-)
(Flag151prime-)
)

)
(:action Flag151Action-26
:parameters ()
:precondition
(and
(Flag110-)
(Flag110prime-)
)
:effect
(and
(Flag151-)
(Flag151prime-)
)

)
(:action Flag151Action-27
:parameters ()
:precondition
(and
(Flag149-)
(Flag149prime-)
)
:effect
(and
(Flag151-)
(Flag151prime-)
)

)
(:action Flag151Action-28
:parameters ()
:precondition
(and
(Flag150-)
(Flag150prime-)
)
:effect
(and
(Flag151-)
(Flag151prime-)
)

)
(:action Flag151Action-29
:parameters ()
:precondition
(and
(Flag118-)
(Flag118prime-)
)
:effect
(and
(Flag151-)
(Flag151prime-)
)

)
(:action Prim151Action
:parameters ()
:precondition
(and
(not (Flag145-))
(Flag145prime-)
(not (Flag126-))
(Flag126prime-)
(not (Flag127-))
(Flag127prime-)
(not (Flag128-))
(Flag128prime-)
(not (Flag120-))
(Flag120prime-)
(not (Flag129-))
(Flag129prime-)
(not (Flag146-))
(Flag146prime-)
(not (Flag112-))
(Flag112prime-)
(not (Flag116-))
(Flag116prime-)
(not (Flag138-))
(Flag138prime-)
(not (Flag114-))
(Flag114prime-)
(not (Flag139-))
(Flag139prime-)
(not (Flag140-))
(Flag140prime-)
(not (Flag141-))
(Flag141prime-)
(not (Flag115-))
(Flag115prime-)
(not (Flag142-))
(Flag142prime-)
(not (Flag108-))
(Flag108prime-)
(not (Flag111-))
(Flag111prime-)
(not (Flag131-))
(Flag131prime-)
(not (Flag113-))
(Flag113prime-)
(not (Flag147-))
(Flag147prime-)
(not (Flag132-))
(Flag132prime-)
(not (Flag148-))
(Flag148prime-)
(not (Flag137-))
(Flag137prime-)
(not (Flag121-))
(Flag121prime-)
(not (Flag123-))
(Flag123prime-)
(not (Flag110-))
(Flag110prime-)
(not (Flag149-))
(Flag149prime-)
(not (Flag150-))
(Flag150prime-)
(not (Flag118-))
(Flag118prime-)
)
:effect
(and
(Flag151prime-)
(not (Flag151-))
)

)
(:action Flag158Action-0
:parameters ()
:precondition
(and
(Flag126-)
(Flag126prime-)
)
:effect
(and
(Flag158-)
(Flag158prime-)
)

)
(:action Flag158Action-1
:parameters ()
:precondition
(and
(Flag127-)
(Flag127prime-)
)
:effect
(and
(Flag158-)
(Flag158prime-)
)

)
(:action Flag158Action-2
:parameters ()
:precondition
(and
(Flag128-)
(Flag128prime-)
)
:effect
(and
(Flag158-)
(Flag158prime-)
)

)
(:action Flag158Action-3
:parameters ()
:precondition
(and
(Flag120-)
(Flag120prime-)
)
:effect
(and
(Flag158-)
(Flag158prime-)
)

)
(:action Flag158Action-4
:parameters ()
:precondition
(and
(Flag152-)
(Flag152prime-)
)
:effect
(and
(Flag158-)
(Flag158prime-)
)

)
(:action Flag158Action-5
:parameters ()
:precondition
(and
(Flag146-)
(Flag146prime-)
)
:effect
(and
(Flag158-)
(Flag158prime-)
)

)
(:action Flag158Action-6
:parameters ()
:precondition
(and
(Flag121-)
(Flag121prime-)
)
:effect
(and
(Flag158-)
(Flag158prime-)
)

)
(:action Flag158Action-7
:parameters ()
:precondition
(and
(Flag111-)
(Flag111prime-)
)
:effect
(and
(Flag158-)
(Flag158prime-)
)

)
(:action Flag158Action-8
:parameters ()
:precondition
(and
(Flag112-)
(Flag112prime-)
)
:effect
(and
(Flag158-)
(Flag158prime-)
)

)
(:action Flag158Action-9
:parameters ()
:precondition
(and
(Flag116-)
(Flag116prime-)
)
:effect
(and
(Flag158-)
(Flag158prime-)
)

)
(:action Flag158Action-10
:parameters ()
:precondition
(and
(Flag138-)
(Flag138prime-)
)
:effect
(and
(Flag158-)
(Flag158prime-)
)

)
(:action Flag158Action-11
:parameters ()
:precondition
(and
(Flag114-)
(Flag114prime-)
)
:effect
(and
(Flag158-)
(Flag158prime-)
)

)
(:action Flag158Action-12
:parameters ()
:precondition
(and
(Flag153-)
(Flag153prime-)
)
:effect
(and
(Flag158-)
(Flag158prime-)
)

)
(:action Flag158Action-13
:parameters ()
:precondition
(and
(Flag154-)
(Flag154prime-)
)
:effect
(and
(Flag158-)
(Flag158prime-)
)

)
(:action Flag158Action-14
:parameters ()
:precondition
(and
(Flag129-)
(Flag129prime-)
)
:effect
(and
(Flag158-)
(Flag158prime-)
)

)
(:action Flag158Action-15
:parameters ()
:precondition
(and
(Flag140-)
(Flag140prime-)
)
:effect
(and
(Flag158-)
(Flag158prime-)
)

)
(:action Flag158Action-16
:parameters ()
:precondition
(and
(Flag141-)
(Flag141prime-)
)
:effect
(and
(Flag158-)
(Flag158prime-)
)

)
(:action Flag158Action-17
:parameters ()
:precondition
(and
(Flag142-)
(Flag142prime-)
)
:effect
(and
(Flag158-)
(Flag158prime-)
)

)
(:action Flag158Action-18
:parameters ()
:precondition
(and
(Flag108-)
(Flag108prime-)
)
:effect
(and
(Flag158-)
(Flag158prime-)
)

)
(:action Flag158Action-19
:parameters ()
:precondition
(and
(Flag113-)
(Flag113prime-)
)
:effect
(and
(Flag158-)
(Flag158prime-)
)

)
(:action Flag158Action-20
:parameters ()
:precondition
(and
(Flag131-)
(Flag131prime-)
)
:effect
(and
(Flag158-)
(Flag158prime-)
)

)
(:action Flag158Action-21
:parameters ()
:precondition
(and
(Flag155-)
(Flag155prime-)
)
:effect
(and
(Flag158-)
(Flag158prime-)
)

)
(:action Flag158Action-22
:parameters ()
:precondition
(and
(Flag148-)
(Flag148prime-)
)
:effect
(and
(Flag158-)
(Flag158prime-)
)

)
(:action Flag158Action-23
:parameters ()
:precondition
(and
(Flag156-)
(Flag156prime-)
)
:effect
(and
(Flag158-)
(Flag158prime-)
)

)
(:action Flag158Action-24
:parameters ()
:precondition
(and
(Flag137-)
(Flag137prime-)
)
:effect
(and
(Flag158-)
(Flag158prime-)
)

)
(:action Flag158Action-25
:parameters ()
:precondition
(and
(Flag157-)
(Flag157prime-)
)
:effect
(and
(Flag158-)
(Flag158prime-)
)

)
(:action Flag158Action-26
:parameters ()
:precondition
(and
(Flag123-)
(Flag123prime-)
)
:effect
(and
(Flag158-)
(Flag158prime-)
)

)
(:action Flag158Action-27
:parameters ()
:precondition
(and
(Flag149-)
(Flag149prime-)
)
:effect
(and
(Flag158-)
(Flag158prime-)
)

)
(:action Flag158Action-28
:parameters ()
:precondition
(and
(Flag150-)
(Flag150prime-)
)
:effect
(and
(Flag158-)
(Flag158prime-)
)

)
(:action Flag158Action-29
:parameters ()
:precondition
(and
(Flag118-)
(Flag118prime-)
)
:effect
(and
(Flag158-)
(Flag158prime-)
)

)
(:action Prim158Action
:parameters ()
:precondition
(and
(not (Flag126-))
(Flag126prime-)
(not (Flag127-))
(Flag127prime-)
(not (Flag128-))
(Flag128prime-)
(not (Flag120-))
(Flag120prime-)
(not (Flag152-))
(Flag152prime-)
(not (Flag146-))
(Flag146prime-)
(not (Flag121-))
(Flag121prime-)
(not (Flag111-))
(Flag111prime-)
(not (Flag112-))
(Flag112prime-)
(not (Flag116-))
(Flag116prime-)
(not (Flag138-))
(Flag138prime-)
(not (Flag114-))
(Flag114prime-)
(not (Flag153-))
(Flag153prime-)
(not (Flag154-))
(Flag154prime-)
(not (Flag129-))
(Flag129prime-)
(not (Flag140-))
(Flag140prime-)
(not (Flag141-))
(Flag141prime-)
(not (Flag142-))
(Flag142prime-)
(not (Flag108-))
(Flag108prime-)
(not (Flag113-))
(Flag113prime-)
(not (Flag131-))
(Flag131prime-)
(not (Flag155-))
(Flag155prime-)
(not (Flag148-))
(Flag148prime-)
(not (Flag156-))
(Flag156prime-)
(not (Flag137-))
(Flag137prime-)
(not (Flag157-))
(Flag157prime-)
(not (Flag123-))
(Flag123prime-)
(not (Flag149-))
(Flag149prime-)
(not (Flag150-))
(Flag150prime-)
(not (Flag118-))
(Flag118prime-)
)
:effect
(and
(Flag158prime-)
(not (Flag158-))
)

)
(:action Flag164Action-0
:parameters ()
:precondition
(and
(Flag159-)
(Flag159prime-)
)
:effect
(and
(Flag164-)
(Flag164prime-)
)

)
(:action Flag164Action-1
:parameters ()
:precondition
(and
(Flag127-)
(Flag127prime-)
)
:effect
(and
(Flag164-)
(Flag164prime-)
)

)
(:action Flag164Action-2
:parameters ()
:precondition
(and
(Flag128-)
(Flag128prime-)
)
:effect
(and
(Flag164-)
(Flag164prime-)
)

)
(:action Flag164Action-3
:parameters ()
:precondition
(and
(Flag160-)
(Flag160prime-)
)
:effect
(and
(Flag164-)
(Flag164prime-)
)

)
(:action Flag164Action-4
:parameters ()
:precondition
(and
(Flag161-)
(Flag161prime-)
)
:effect
(and
(Flag164-)
(Flag164prime-)
)

)
(:action Flag164Action-5
:parameters ()
:precondition
(and
(Flag120-)
(Flag120prime-)
)
:effect
(and
(Flag164-)
(Flag164prime-)
)

)
(:action Flag164Action-6
:parameters ()
:precondition
(and
(Flag146-)
(Flag146prime-)
)
:effect
(and
(Flag164-)
(Flag164prime-)
)

)
(:action Flag164Action-7
:parameters ()
:precondition
(and
(Flag121-)
(Flag121prime-)
)
:effect
(and
(Flag164-)
(Flag164prime-)
)

)
(:action Flag164Action-8
:parameters ()
:precondition
(and
(Flag112-)
(Flag112prime-)
)
:effect
(and
(Flag164-)
(Flag164prime-)
)

)
(:action Flag164Action-9
:parameters ()
:precondition
(and
(Flag138-)
(Flag138prime-)
)
:effect
(and
(Flag164-)
(Flag164prime-)
)

)
(:action Flag164Action-10
:parameters ()
:precondition
(and
(Flag153-)
(Flag153prime-)
)
:effect
(and
(Flag164-)
(Flag164prime-)
)

)
(:action Flag164Action-11
:parameters ()
:precondition
(and
(Flag129-)
(Flag129prime-)
)
:effect
(and
(Flag164-)
(Flag164prime-)
)

)
(:action Flag164Action-12
:parameters ()
:precondition
(and
(Flag140-)
(Flag140prime-)
)
:effect
(and
(Flag164-)
(Flag164prime-)
)

)
(:action Flag164Action-13
:parameters ()
:precondition
(and
(Flag141-)
(Flag141prime-)
)
:effect
(and
(Flag164-)
(Flag164prime-)
)

)
(:action Flag164Action-14
:parameters ()
:precondition
(and
(Flag108-)
(Flag108prime-)
)
:effect
(and
(Flag164-)
(Flag164prime-)
)

)
(:action Flag164Action-15
:parameters ()
:precondition
(and
(Flag111-)
(Flag111prime-)
)
:effect
(and
(Flag164-)
(Flag164prime-)
)

)
(:action Flag164Action-16
:parameters ()
:precondition
(and
(Flag131-)
(Flag131prime-)
)
:effect
(and
(Flag164-)
(Flag164prime-)
)

)
(:action Flag164Action-17
:parameters ()
:precondition
(and
(Flag113-)
(Flag113prime-)
)
:effect
(and
(Flag164-)
(Flag164prime-)
)

)
(:action Flag164Action-18
:parameters ()
:precondition
(and
(Flag148-)
(Flag148prime-)
)
:effect
(and
(Flag164-)
(Flag164prime-)
)

)
(:action Flag164Action-19
:parameters ()
:precondition
(and
(Flag156-)
(Flag156prime-)
)
:effect
(and
(Flag164-)
(Flag164prime-)
)

)
(:action Flag164Action-20
:parameters ()
:precondition
(and
(Flag137-)
(Flag137prime-)
)
:effect
(and
(Flag164-)
(Flag164prime-)
)

)
(:action Flag164Action-21
:parameters ()
:precondition
(and
(Flag157-)
(Flag157prime-)
)
:effect
(and
(Flag164-)
(Flag164prime-)
)

)
(:action Flag164Action-22
:parameters ()
:precondition
(and
(Flag123-)
(Flag123prime-)
)
:effect
(and
(Flag164-)
(Flag164prime-)
)

)
(:action Flag164Action-23
:parameters ()
:precondition
(and
(Flag149-)
(Flag149prime-)
)
:effect
(and
(Flag164-)
(Flag164prime-)
)

)
(:action Flag164Action-24
:parameters ()
:precondition
(and
(Flag162-)
(Flag162prime-)
)
:effect
(and
(Flag164-)
(Flag164prime-)
)

)
(:action Flag164Action-25
:parameters ()
:precondition
(and
(Flag163-)
(Flag163prime-)
)
:effect
(and
(Flag164-)
(Flag164prime-)
)

)
(:action Flag164Action-26
:parameters ()
:precondition
(and
(Flag150-)
(Flag150prime-)
)
:effect
(and
(Flag164-)
(Flag164prime-)
)

)
(:action Flag164Action-27
:parameters ()
:precondition
(and
(Flag118-)
(Flag118prime-)
)
:effect
(and
(Flag164-)
(Flag164prime-)
)

)
(:action Prim164Action
:parameters ()
:precondition
(and
(not (Flag159-))
(Flag159prime-)
(not (Flag127-))
(Flag127prime-)
(not (Flag128-))
(Flag128prime-)
(not (Flag160-))
(Flag160prime-)
(not (Flag161-))
(Flag161prime-)
(not (Flag120-))
(Flag120prime-)
(not (Flag146-))
(Flag146prime-)
(not (Flag121-))
(Flag121prime-)
(not (Flag112-))
(Flag112prime-)
(not (Flag138-))
(Flag138prime-)
(not (Flag153-))
(Flag153prime-)
(not (Flag129-))
(Flag129prime-)
(not (Flag140-))
(Flag140prime-)
(not (Flag141-))
(Flag141prime-)
(not (Flag108-))
(Flag108prime-)
(not (Flag111-))
(Flag111prime-)
(not (Flag131-))
(Flag131prime-)
(not (Flag113-))
(Flag113prime-)
(not (Flag148-))
(Flag148prime-)
(not (Flag156-))
(Flag156prime-)
(not (Flag137-))
(Flag137prime-)
(not (Flag157-))
(Flag157prime-)
(not (Flag123-))
(Flag123prime-)
(not (Flag149-))
(Flag149prime-)
(not (Flag162-))
(Flag162prime-)
(not (Flag163-))
(Flag163prime-)
(not (Flag150-))
(Flag150prime-)
(not (Flag118-))
(Flag118prime-)
)
:effect
(and
(Flag164prime-)
(not (Flag164-))
)

)
(:action Flag169Action-0
:parameters ()
:precondition
(and
(Flag128-)
(Flag128prime-)
)
:effect
(and
(Flag169-)
(Flag169prime-)
)

)
(:action Flag169Action-1
:parameters ()
:precondition
(and
(Flag160-)
(Flag160prime-)
)
:effect
(and
(Flag169-)
(Flag169prime-)
)

)
(:action Flag169Action-2
:parameters ()
:precondition
(and
(Flag120-)
(Flag120prime-)
)
:effect
(and
(Flag169-)
(Flag169prime-)
)

)
(:action Flag169Action-3
:parameters ()
:precondition
(and
(Flag146-)
(Flag146prime-)
)
:effect
(and
(Flag169-)
(Flag169prime-)
)

)
(:action Flag169Action-4
:parameters ()
:precondition
(and
(Flag121-)
(Flag121prime-)
)
:effect
(and
(Flag169-)
(Flag169prime-)
)

)
(:action Flag169Action-5
:parameters ()
:precondition
(and
(Flag112-)
(Flag112prime-)
)
:effect
(and
(Flag169-)
(Flag169prime-)
)

)
(:action Flag169Action-6
:parameters ()
:precondition
(and
(Flag138-)
(Flag138prime-)
)
:effect
(and
(Flag169-)
(Flag169prime-)
)

)
(:action Flag169Action-7
:parameters ()
:precondition
(and
(Flag153-)
(Flag153prime-)
)
:effect
(and
(Flag169-)
(Flag169prime-)
)

)
(:action Flag169Action-8
:parameters ()
:precondition
(and
(Flag140-)
(Flag140prime-)
)
:effect
(and
(Flag169-)
(Flag169prime-)
)

)
(:action Flag169Action-9
:parameters ()
:precondition
(and
(Flag129-)
(Flag129prime-)
)
:effect
(and
(Flag169-)
(Flag169prime-)
)

)
(:action Flag169Action-10
:parameters ()
:precondition
(and
(Flag141-)
(Flag141prime-)
)
:effect
(and
(Flag169-)
(Flag169prime-)
)

)
(:action Flag169Action-11
:parameters ()
:precondition
(and
(Flag165-)
(Flag165prime-)
)
:effect
(and
(Flag169-)
(Flag169prime-)
)

)
(:action Flag169Action-12
:parameters ()
:precondition
(and
(Flag111-)
(Flag111prime-)
)
:effect
(and
(Flag169-)
(Flag169prime-)
)

)
(:action Flag169Action-13
:parameters ()
:precondition
(and
(Flag131-)
(Flag131prime-)
)
:effect
(and
(Flag169-)
(Flag169prime-)
)

)
(:action Flag169Action-14
:parameters ()
:precondition
(and
(Flag148-)
(Flag148prime-)
)
:effect
(and
(Flag169-)
(Flag169prime-)
)

)
(:action Flag169Action-15
:parameters ()
:precondition
(and
(Flag166-)
(Flag166prime-)
)
:effect
(and
(Flag169-)
(Flag169prime-)
)

)
(:action Flag169Action-16
:parameters ()
:precondition
(and
(Flag156-)
(Flag156prime-)
)
:effect
(and
(Flag169-)
(Flag169prime-)
)

)
(:action Flag169Action-17
:parameters ()
:precondition
(and
(Flag157-)
(Flag157prime-)
)
:effect
(and
(Flag169-)
(Flag169prime-)
)

)
(:action Flag169Action-18
:parameters ()
:precondition
(and
(Flag123-)
(Flag123prime-)
)
:effect
(and
(Flag169-)
(Flag169prime-)
)

)
(:action Flag169Action-19
:parameters ()
:precondition
(and
(Flag167-)
(Flag167prime-)
)
:effect
(and
(Flag169-)
(Flag169prime-)
)

)
(:action Flag169Action-20
:parameters ()
:precondition
(and
(Flag149-)
(Flag149prime-)
)
:effect
(and
(Flag169-)
(Flag169prime-)
)

)
(:action Flag169Action-21
:parameters ()
:precondition
(and
(Flag163-)
(Flag163prime-)
)
:effect
(and
(Flag169-)
(Flag169prime-)
)

)
(:action Flag169Action-22
:parameters ()
:precondition
(and
(Flag168-)
(Flag168prime-)
)
:effect
(and
(Flag169-)
(Flag169prime-)
)

)
(:action Flag169Action-23
:parameters ()
:precondition
(and
(Flag118-)
(Flag118prime-)
)
:effect
(and
(Flag169-)
(Flag169prime-)
)

)
(:action Prim169Action
:parameters ()
:precondition
(and
(not (Flag128-))
(Flag128prime-)
(not (Flag160-))
(Flag160prime-)
(not (Flag120-))
(Flag120prime-)
(not (Flag146-))
(Flag146prime-)
(not (Flag121-))
(Flag121prime-)
(not (Flag112-))
(Flag112prime-)
(not (Flag138-))
(Flag138prime-)
(not (Flag153-))
(Flag153prime-)
(not (Flag140-))
(Flag140prime-)
(not (Flag129-))
(Flag129prime-)
(not (Flag141-))
(Flag141prime-)
(not (Flag165-))
(Flag165prime-)
(not (Flag111-))
(Flag111prime-)
(not (Flag131-))
(Flag131prime-)
(not (Flag148-))
(Flag148prime-)
(not (Flag166-))
(Flag166prime-)
(not (Flag156-))
(Flag156prime-)
(not (Flag157-))
(Flag157prime-)
(not (Flag123-))
(Flag123prime-)
(not (Flag167-))
(Flag167prime-)
(not (Flag149-))
(Flag149prime-)
(not (Flag163-))
(Flag163prime-)
(not (Flag168-))
(Flag168prime-)
(not (Flag118-))
(Flag118prime-)
)
:effect
(and
(Flag169prime-)
(not (Flag169-))
)

)
(:action Flag173Action-0
:parameters ()
:precondition
(and
(Flag149-)
(Flag149prime-)
)
:effect
(and
(Flag173-)
(Flag173prime-)
)

)
(:action Flag173Action-1
:parameters ()
:precondition
(and
(Flag170-)
(Flag170prime-)
)
:effect
(and
(Flag173-)
(Flag173prime-)
)

)
(:action Flag173Action-2
:parameters ()
:precondition
(and
(Flag146-)
(Flag146prime-)
)
:effect
(and
(Flag173-)
(Flag173prime-)
)

)
(:action Flag173Action-3
:parameters ()
:precondition
(and
(Flag171-)
(Flag171prime-)
)
:effect
(and
(Flag173-)
(Flag173prime-)
)

)
(:action Flag173Action-4
:parameters ()
:precondition
(and
(Flag111-)
(Flag111prime-)
)
:effect
(and
(Flag173-)
(Flag173prime-)
)

)
(:action Flag173Action-5
:parameters ()
:precondition
(and
(Flag118-)
(Flag118prime-)
)
:effect
(and
(Flag173-)
(Flag173prime-)
)

)
(:action Flag173Action-6
:parameters ()
:precondition
(and
(Flag131-)
(Flag131prime-)
)
:effect
(and
(Flag173-)
(Flag173prime-)
)

)
(:action Flag173Action-7
:parameters ()
:precondition
(and
(Flag138-)
(Flag138prime-)
)
:effect
(and
(Flag173-)
(Flag173prime-)
)

)
(:action Flag173Action-8
:parameters ()
:precondition
(and
(Flag163-)
(Flag163prime-)
)
:effect
(and
(Flag173-)
(Flag173prime-)
)

)
(:action Flag173Action-9
:parameters ()
:precondition
(and
(Flag166-)
(Flag166prime-)
)
:effect
(and
(Flag173-)
(Flag173prime-)
)

)
(:action Flag173Action-10
:parameters ()
:precondition
(and
(Flag140-)
(Flag140prime-)
)
:effect
(and
(Flag173-)
(Flag173prime-)
)

)
(:action Flag173Action-11
:parameters ()
:precondition
(and
(Flag129-)
(Flag129prime-)
)
:effect
(and
(Flag173-)
(Flag173prime-)
)

)
(:action Flag173Action-12
:parameters ()
:precondition
(and
(Flag156-)
(Flag156prime-)
)
:effect
(and
(Flag173-)
(Flag173prime-)
)

)
(:action Flag173Action-13
:parameters ()
:precondition
(and
(Flag172-)
(Flag172prime-)
)
:effect
(and
(Flag173-)
(Flag173prime-)
)

)
(:action Flag173Action-14
:parameters ()
:precondition
(and
(Flag160-)
(Flag160prime-)
)
:effect
(and
(Flag173-)
(Flag173prime-)
)

)
(:action Flag173Action-15
:parameters ()
:precondition
(and
(Flag121-)
(Flag121prime-)
)
:effect
(and
(Flag173-)
(Flag173prime-)
)

)
(:action Flag173Action-16
:parameters ()
:precondition
(and
(Flag157-)
(Flag157prime-)
)
:effect
(and
(Flag173-)
(Flag173prime-)
)

)
(:action Flag173Action-17
:parameters ()
:precondition
(and
(Flag123-)
(Flag123prime-)
)
:effect
(and
(Flag173-)
(Flag173prime-)
)

)
(:action Prim173Action
:parameters ()
:precondition
(and
(not (Flag149-))
(Flag149prime-)
(not (Flag170-))
(Flag170prime-)
(not (Flag146-))
(Flag146prime-)
(not (Flag171-))
(Flag171prime-)
(not (Flag111-))
(Flag111prime-)
(not (Flag118-))
(Flag118prime-)
(not (Flag131-))
(Flag131prime-)
(not (Flag138-))
(Flag138prime-)
(not (Flag163-))
(Flag163prime-)
(not (Flag166-))
(Flag166prime-)
(not (Flag140-))
(Flag140prime-)
(not (Flag129-))
(Flag129prime-)
(not (Flag156-))
(Flag156prime-)
(not (Flag172-))
(Flag172prime-)
(not (Flag160-))
(Flag160prime-)
(not (Flag121-))
(Flag121prime-)
(not (Flag157-))
(Flag157prime-)
(not (Flag123-))
(Flag123prime-)
)
:effect
(and
(Flag173prime-)
(not (Flag173-))
)

)
(:action Flag176Action-0
:parameters ()
:precondition
(and
(Flag149-)
(Flag149prime-)
)
:effect
(and
(Flag176-)
(Flag176prime-)
)

)
(:action Flag176Action-1
:parameters ()
:precondition
(and
(Flag111-)
(Flag111prime-)
)
:effect
(and
(Flag176-)
(Flag176prime-)
)

)
(:action Flag176Action-2
:parameters ()
:precondition
(and
(Flag138-)
(Flag138prime-)
)
:effect
(and
(Flag176-)
(Flag176prime-)
)

)
(:action Flag176Action-3
:parameters ()
:precondition
(and
(Flag157-)
(Flag157prime-)
)
:effect
(and
(Flag176-)
(Flag176prime-)
)

)
(:action Flag176Action-4
:parameters ()
:precondition
(and
(Flag166-)
(Flag166prime-)
)
:effect
(and
(Flag176-)
(Flag176prime-)
)

)
(:action Flag176Action-5
:parameters ()
:precondition
(and
(Flag129-)
(Flag129prime-)
)
:effect
(and
(Flag176-)
(Flag176prime-)
)

)
(:action Flag176Action-6
:parameters ()
:precondition
(and
(Flag174-)
(Flag174prime-)
)
:effect
(and
(Flag176-)
(Flag176prime-)
)

)
(:action Flag176Action-7
:parameters ()
:precondition
(and
(Flag160-)
(Flag160prime-)
)
:effect
(and
(Flag176-)
(Flag176prime-)
)

)
(:action Flag176Action-8
:parameters ()
:precondition
(and
(Flag121-)
(Flag121prime-)
)
:effect
(and
(Flag176-)
(Flag176prime-)
)

)
(:action Flag176Action-9
:parameters ()
:precondition
(and
(Flag175-)
(Flag175prime-)
)
:effect
(and
(Flag176-)
(Flag176prime-)
)

)
(:action Prim176Action
:parameters ()
:precondition
(and
(not (Flag149-))
(Flag149prime-)
(not (Flag111-))
(Flag111prime-)
(not (Flag138-))
(Flag138prime-)
(not (Flag157-))
(Flag157prime-)
(not (Flag166-))
(Flag166prime-)
(not (Flag129-))
(Flag129prime-)
(not (Flag174-))
(Flag174prime-)
(not (Flag160-))
(Flag160prime-)
(not (Flag121-))
(Flag121prime-)
(not (Flag175-))
(Flag175prime-)
)
:effect
(and
(Flag176prime-)
(not (Flag176-))
)

)
(:action Flag177Action-0
:parameters ()
:precondition
(and
(LEFTOF10-ROBOT)
)
:effect
(and
(Flag177-)
(Flag177prime-)
)

)
(:action Flag177Action-2
:parameters ()
:precondition
(and
(LEFTOF2-ROBOT)
)
:effect
(and
(Flag177-)
(Flag177prime-)
)

)
(:action Flag177Action-3
:parameters ()
:precondition
(and
(LEFTOF7-ROBOT)
)
:effect
(and
(Flag177-)
(Flag177prime-)
)

)
(:action Flag177Action-4
:parameters ()
:precondition
(and
(LEFTOF1-ROBOT)
)
:effect
(and
(Flag177-)
(Flag177prime-)
)

)
(:action Flag177Action-5
:parameters ()
:precondition
(and
(LEFTOF6-ROBOT)
)
:effect
(and
(Flag177-)
(Flag177prime-)
)

)
(:action Flag177Action-6
:parameters ()
:precondition
(and
(LEFTOF5-ROBOT)
)
:effect
(and
(Flag177-)
(Flag177prime-)
)

)
(:action Flag177Action-7
:parameters ()
:precondition
(and
(LEFTOF3-ROBOT)
)
:effect
(and
(Flag177-)
(Flag177prime-)
)

)
(:action Flag177Action-8
:parameters ()
:precondition
(and
(LEFTOF4-ROBOT)
)
:effect
(and
(Flag177-)
(Flag177prime-)
)

)
(:action Flag177Action-9
:parameters ()
:precondition
(and
(LEFTOF8-ROBOT)
)
:effect
(and
(Flag177-)
(Flag177prime-)
)

)
(:action Prim177Action
:parameters ()
:precondition
(and
(not (LEFTOF10-ROBOT))
(not (LEFTOF9-ROBOT))
(not (LEFTOF2-ROBOT))
(not (LEFTOF7-ROBOT))
(not (LEFTOF1-ROBOT))
(not (LEFTOF6-ROBOT))
(not (LEFTOF5-ROBOT))
(not (LEFTOF3-ROBOT))
(not (LEFTOF4-ROBOT))
(not (LEFTOF8-ROBOT))
)
:effect
(and
(Flag177prime-)
(not (Flag177-))
)

)
(:action Flag178Action
:parameters ()
:precondition
(and
(LEFTOF10-ROBOT)
(RIGHTOF9-ROBOT)
)
:effect
(and
(Flag178-)
(Flag178prime-)
)

)
(:action Prim178Action-0
:parameters ()
:precondition
(and
(not (LEFTOF10-ROBOT))
)
:effect
(and
(Flag178prime-)
(not (Flag178-))
)

)
(:action Prim178Action-1
:parameters ()
:precondition
(and
(not (RIGHTOF9-ROBOT))
)
:effect
(and
(Flag178prime-)
(not (Flag178-))
)

)
(:action Flag180Action-0
:parameters ()
:precondition
(and
(Flag110-)
(Flag110prime-)
)
:effect
(and
(Flag180-)
(Flag180prime-)
)

)
(:action Flag180Action-1
:parameters ()
:precondition
(and
(Flag107-)
(Flag107prime-)
)
:effect
(and
(Flag180-)
(Flag180prime-)
)

)
(:action Flag180Action-2
:parameters ()
:precondition
(and
(Flag113-)
(Flag113prime-)
)
:effect
(and
(Flag180-)
(Flag180prime-)
)

)
(:action Flag180Action-3
:parameters ()
:precondition
(and
(Flag112-)
(Flag112prime-)
)
:effect
(and
(Flag180-)
(Flag180prime-)
)

)
(:action Flag180Action-4
:parameters ()
:precondition
(and
(Flag117-)
(Flag117prime-)
)
:effect
(and
(Flag180-)
(Flag180prime-)
)

)
(:action Flag180Action-5
:parameters ()
:precondition
(and
(Flag114-)
(Flag114prime-)
)
:effect
(and
(Flag180-)
(Flag180prime-)
)

)
(:action Flag180Action-7
:parameters ()
:precondition
(and
(Flag118-)
(Flag118prime-)
)
:effect
(and
(Flag180-)
(Flag180prime-)
)

)
(:action Flag180Action-8
:parameters ()
:precondition
(and
(Flag119-)
(Flag119prime-)
)
:effect
(and
(Flag180-)
(Flag180prime-)
)

)
(:action Flag180Action-9
:parameters ()
:precondition
(and
(Flag121-)
(Flag121prime-)
)
:effect
(and
(Flag180-)
(Flag180prime-)
)

)
(:action Prim180Action
:parameters ()
:precondition
(and
(not (Flag110-))
(Flag110prime-)
(not (Flag107-))
(Flag107prime-)
(not (Flag113-))
(Flag113prime-)
(not (Flag112-))
(Flag112prime-)
(not (Flag117-))
(Flag117prime-)
(not (Flag114-))
(Flag114prime-)
(not (BELOWOF1-ROBOT))
(not (Flag118-))
(Flag118prime-)
(not (Flag119-))
(Flag119prime-)
(not (Flag121-))
(Flag121prime-)
)
:effect
(and
(Flag180prime-)
(not (Flag180-))
)

)
(:action MOVELEFT-ROBOT
:parameters ()
:precondition
(and
(Flag4-)
(Flag4prime-)
(Flag179prime-)
(Flag89prime-)
(Flag86prime-)
(Flag81prime-)
(Flag75prime-)
(Flag69prime-)
(Flag61prime-)
(Flag52prime-)
(Flag43prime-)
(Flag177prime-)
)
:effect
(and
(when
(and
(Flag179-)
)
(and
(COLUMN8-ROBOT)
(not (NOT-COLUMN8-ROBOT))
)
)
(when
(and
(Flag89-)
)
(and
(COLUMN7-ROBOT)
(not (NOT-COLUMN7-ROBOT))
)
)
(when
(and
(Flag86-)
)
(and
(COLUMN6-ROBOT)
(not (NOT-COLUMN6-ROBOT))
)
)
(when
(and
(Flag81-)
)
(and
(COLUMN5-ROBOT)
(not (NOT-COLUMN5-ROBOT))
)
)
(when
(and
(Flag75-)
)
(and
(COLUMN4-ROBOT)
(not (NOT-COLUMN4-ROBOT))
)
)
(when
(and
(Flag69-)
)
(and
(COLUMN3-ROBOT)
(not (NOT-COLUMN3-ROBOT))
)
)
(when
(and
(Flag61-)
)
(and
(COLUMN2-ROBOT)
(not (NOT-COLUMN2-ROBOT))
)
)
(when
(and
(Flag52-)
)
(and
(COLUMN1-ROBOT)
(not (NOT-COLUMN1-ROBOT))
)
)
(when
(and
(Flag43-)
)
(and
(COLUMN0-ROBOT)
(not (NOT-COLUMN0-ROBOT))
)
)
(when
(and
(COLUMN9-ROBOT)
)
(and
(COLUMN8-ROBOT)
(NOT-COLUMN9-ROBOT)
(not (NOT-COLUMN8-ROBOT))
(not (COLUMN9-ROBOT))
)
)
(when
(and
(COLUMN8-ROBOT)
)
(and
(COLUMN7-ROBOT)
(NOT-COLUMN8-ROBOT)
(not (NOT-COLUMN7-ROBOT))
(not (COLUMN8-ROBOT))
)
)
(when
(and
(COLUMN7-ROBOT)
)
(and
(COLUMN6-ROBOT)
(NOT-COLUMN7-ROBOT)
(not (NOT-COLUMN6-ROBOT))
(not (COLUMN7-ROBOT))
)
)
(when
(and
(COLUMN6-ROBOT)
)
(and
(COLUMN5-ROBOT)
(NOT-COLUMN6-ROBOT)
(not (NOT-COLUMN5-ROBOT))
(not (COLUMN6-ROBOT))
)
)
(when
(and
(COLUMN5-ROBOT)
)
(and
(COLUMN4-ROBOT)
(NOT-COLUMN5-ROBOT)
(not (NOT-COLUMN4-ROBOT))
(not (COLUMN5-ROBOT))
)
)
(when
(and
(COLUMN4-ROBOT)
)
(and
(COLUMN3-ROBOT)
(NOT-COLUMN4-ROBOT)
(not (NOT-COLUMN3-ROBOT))
(not (COLUMN4-ROBOT))
)
)
(when
(and
(COLUMN3-ROBOT)
)
(and
(COLUMN2-ROBOT)
(NOT-COLUMN3-ROBOT)
(not (NOT-COLUMN2-ROBOT))
(not (COLUMN3-ROBOT))
)
)
(when
(and
(COLUMN2-ROBOT)
)
(and
(COLUMN1-ROBOT)
(NOT-COLUMN2-ROBOT)
(not (NOT-COLUMN1-ROBOT))
(not (COLUMN2-ROBOT))
)
)
(when
(and
(COLUMN1-ROBOT)
)
(and
(COLUMN0-ROBOT)
(NOT-COLUMN1-ROBOT)
(not (NOT-COLUMN0-ROBOT))
(not (COLUMN1-ROBOT))
)
)
(when
(and
(RIGHTOF9-ROBOT)
)
(and
(RIGHTOF8-ROBOT)
(NOT-RIGHTOF9-ROBOT)
(RIGHTOF7-ROBOT)
(NOT-RIGHTOF8-ROBOT)
(RIGHTOF6-ROBOT)
(NOT-RIGHTOF7-ROBOT)
(RIGHTOF5-ROBOT)
(NOT-RIGHTOF6-ROBOT)
(RIGHTOF4-ROBOT)
(NOT-RIGHTOF5-ROBOT)
(RIGHTOF3-ROBOT)
(NOT-RIGHTOF4-ROBOT)
(RIGHTOF2-ROBOT)
(NOT-RIGHTOF3-ROBOT)
(RIGHTOF1-ROBOT)
(NOT-RIGHTOF2-ROBOT)
(RIGHTOF0-ROBOT)
(NOT-RIGHTOF1-ROBOT)
(not (RIGHTOF9-ROBOT))
(not (NOT-RIGHTOF0-ROBOT))
)
)
(when
(and
(RIGHTOF8-ROBOT)
)
(and
(RIGHTOF7-ROBOT)
(NOT-RIGHTOF8-ROBOT)
(RIGHTOF6-ROBOT)
(NOT-RIGHTOF7-ROBOT)
(RIGHTOF5-ROBOT)
(NOT-RIGHTOF6-ROBOT)
(RIGHTOF4-ROBOT)
(NOT-RIGHTOF5-ROBOT)
(RIGHTOF3-ROBOT)
(NOT-RIGHTOF4-ROBOT)
(RIGHTOF2-ROBOT)
(NOT-RIGHTOF3-ROBOT)
(RIGHTOF1-ROBOT)
(NOT-RIGHTOF2-ROBOT)
(RIGHTOF0-ROBOT)
(NOT-RIGHTOF1-ROBOT)
(not (RIGHTOF8-ROBOT))
(not (NOT-RIGHTOF0-ROBOT))
)
)
(when
(and
(RIGHTOF7-ROBOT)
)
(and
(RIGHTOF6-ROBOT)
(NOT-RIGHTOF7-ROBOT)
(RIGHTOF5-ROBOT)
(NOT-RIGHTOF6-ROBOT)
(RIGHTOF4-ROBOT)
(NOT-RIGHTOF5-ROBOT)
(RIGHTOF3-ROBOT)
(NOT-RIGHTOF4-ROBOT)
(RIGHTOF2-ROBOT)
(NOT-RIGHTOF3-ROBOT)
(RIGHTOF1-ROBOT)
(NOT-RIGHTOF2-ROBOT)
(RIGHTOF0-ROBOT)
(NOT-RIGHTOF1-ROBOT)
(not (RIGHTOF7-ROBOT))
(not (NOT-RIGHTOF0-ROBOT))
)
)
(when
(and
(RIGHTOF6-ROBOT)
)
(and
(RIGHTOF5-ROBOT)
(NOT-RIGHTOF6-ROBOT)
(RIGHTOF4-ROBOT)
(NOT-RIGHTOF5-ROBOT)
(RIGHTOF3-ROBOT)
(NOT-RIGHTOF4-ROBOT)
(RIGHTOF2-ROBOT)
(NOT-RIGHTOF3-ROBOT)
(RIGHTOF1-ROBOT)
(NOT-RIGHTOF2-ROBOT)
(RIGHTOF0-ROBOT)
(NOT-RIGHTOF1-ROBOT)
(not (RIGHTOF6-ROBOT))
(not (NOT-RIGHTOF0-ROBOT))
)
)
(when
(and
(RIGHTOF5-ROBOT)
)
(and
(RIGHTOF4-ROBOT)
(NOT-RIGHTOF5-ROBOT)
(RIGHTOF3-ROBOT)
(NOT-RIGHTOF4-ROBOT)
(RIGHTOF2-ROBOT)
(NOT-RIGHTOF3-ROBOT)
(RIGHTOF1-ROBOT)
(NOT-RIGHTOF2-ROBOT)
(RIGHTOF0-ROBOT)
(NOT-RIGHTOF1-ROBOT)
(not (RIGHTOF5-ROBOT))
(not (NOT-RIGHTOF0-ROBOT))
)
)
(when
(and
(RIGHTOF4-ROBOT)
)
(and
(RIGHTOF3-ROBOT)
(NOT-RIGHTOF4-ROBOT)
(RIGHTOF2-ROBOT)
(NOT-RIGHTOF3-ROBOT)
(RIGHTOF1-ROBOT)
(NOT-RIGHTOF2-ROBOT)
(RIGHTOF0-ROBOT)
(NOT-RIGHTOF1-ROBOT)
(not (RIGHTOF4-ROBOT))
(not (NOT-RIGHTOF0-ROBOT))
)
)
(when
(and
(RIGHTOF3-ROBOT)
)
(and
(RIGHTOF2-ROBOT)
(NOT-RIGHTOF3-ROBOT)
(RIGHTOF1-ROBOT)
(NOT-RIGHTOF2-ROBOT)
(RIGHTOF0-ROBOT)
(NOT-RIGHTOF1-ROBOT)
(not (RIGHTOF3-ROBOT))
(not (NOT-RIGHTOF0-ROBOT))
)
)
(when
(and
(RIGHTOF2-ROBOT)
)
(and
(RIGHTOF1-ROBOT)
(NOT-RIGHTOF2-ROBOT)
(RIGHTOF0-ROBOT)
(NOT-RIGHTOF1-ROBOT)
(not (RIGHTOF2-ROBOT))
(not (NOT-RIGHTOF0-ROBOT))
)
)
(when
(and
(RIGHTOF1-ROBOT)
)
(and
(RIGHTOF0-ROBOT)
(NOT-RIGHTOF1-ROBOT)
(not (NOT-RIGHTOF0-ROBOT))
(not (RIGHTOF1-ROBOT))
)
)
(when
(and
(Flag177-)
)
(and
(LEFTOF9-ROBOT)
(not (NOT-LEFTOF9-ROBOT))
)
)
(when
(and
(LEFTOF8-ROBOT)
)
(and
(LEFTOF7-ROBOT)
(not (NOT-LEFTOF8-ROBOT))
(not (NOT-LEFTOF7-ROBOT))
)
)
(when
(and
(LEFTOF4-ROBOT)
)
(and
(LEFTOF8-ROBOT)
(LEFTOF7-ROBOT)
(LEFTOF6-ROBOT)
(LEFTOF5-ROBOT)
(LEFTOF3-ROBOT)
(not (NOT-LEFTOF8-ROBOT))
(not (NOT-LEFTOF7-ROBOT))
(not (NOT-LEFTOF6-ROBOT))
(not (NOT-LEFTOF5-ROBOT))
(not (NOT-LEFTOF4-ROBOT))
(not (NOT-LEFTOF3-ROBOT))
)
)
(when
(and
(LEFTOF3-ROBOT)
)
(and
(LEFTOF8-ROBOT)
(LEFTOF7-ROBOT)
(LEFTOF6-ROBOT)
(LEFTOF5-ROBOT)
(LEFTOF4-ROBOT)
(LEFTOF2-ROBOT)
(not (NOT-LEFTOF8-ROBOT))
(not (NOT-LEFTOF7-ROBOT))
(not (NOT-LEFTOF6-ROBOT))
(not (NOT-LEFTOF5-ROBOT))
(not (NOT-LEFTOF4-ROBOT))
(not (NOT-LEFTOF3-ROBOT))
(not (NOT-LEFTOF2-ROBOT))
)
)
(when
(and
(LEFTOF5-ROBOT)
)
(and
(LEFTOF8-ROBOT)
(LEFTOF7-ROBOT)
(LEFTOF6-ROBOT)
(LEFTOF4-ROBOT)
(not (NOT-LEFTOF8-ROBOT))
(not (NOT-LEFTOF7-ROBOT))
(not (NOT-LEFTOF6-ROBOT))
(not (NOT-LEFTOF5-ROBOT))
(not (NOT-LEFTOF4-ROBOT))
)
)
(when
(and
(LEFTOF6-ROBOT)
)
(and
(LEFTOF8-ROBOT)
(LEFTOF7-ROBOT)
(LEFTOF5-ROBOT)
(not (NOT-LEFTOF8-ROBOT))
(not (NOT-LEFTOF7-ROBOT))
(not (NOT-LEFTOF6-ROBOT))
(not (NOT-LEFTOF5-ROBOT))
)
)
(when
(and
(LEFTOF1-ROBOT)
)
(and
(LEFTOF8-ROBOT)
(LEFTOF7-ROBOT)
(LEFTOF6-ROBOT)
(LEFTOF5-ROBOT)
(LEFTOF4-ROBOT)
(LEFTOF3-ROBOT)
(LEFTOF2-ROBOT)
(not (NOT-LEFTOF8-ROBOT))
(not (NOT-LEFTOF7-ROBOT))
(not (NOT-LEFTOF6-ROBOT))
(not (NOT-LEFTOF5-ROBOT))
(not (NOT-LEFTOF4-ROBOT))
(not (NOT-LEFTOF3-ROBOT))
(not (NOT-LEFTOF2-ROBOT))
(not (NOT-LEFTOF1-ROBOT))
)
)
(when
(and
(LEFTOF7-ROBOT)
)
(and
(LEFTOF8-ROBOT)
(LEFTOF6-ROBOT)
(not (NOT-LEFTOF8-ROBOT))
(not (NOT-LEFTOF7-ROBOT))
(not (NOT-LEFTOF6-ROBOT))
)
)
(when
(and
(LEFTOF2-ROBOT)
)
(and
(LEFTOF8-ROBOT)
(LEFTOF7-ROBOT)
(LEFTOF6-ROBOT)
(LEFTOF5-ROBOT)
(LEFTOF4-ROBOT)
(LEFTOF3-ROBOT)
(LEFTOF1-ROBOT)
(not (NOT-LEFTOF8-ROBOT))
(not (NOT-LEFTOF7-ROBOT))
(not (NOT-LEFTOF6-ROBOT))
(not (NOT-LEFTOF5-ROBOT))
(not (NOT-LEFTOF4-ROBOT))
(not (NOT-LEFTOF3-ROBOT))
(not (NOT-LEFTOF2-ROBOT))
(not (NOT-LEFTOF1-ROBOT))
)
)
(when
(and
(LEFTOF9-ROBOT)
)
(and
(LEFTOF8-ROBOT)
(not (NOT-LEFTOF8-ROBOT))
)
)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag177prime-))
(not (Flag98prime-))
(not (Flag97prime-))
(not (Flag21prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag180prime-))
(not (Flag175prime-))
(not (Flag174prime-))
(not (Flag172prime-))
(not (Flag171prime-))
(not (Flag170prime-))
(not (Flag168prime-))
(not (Flag167prime-))
(not (Flag166prime-))
(not (Flag165prime-))
(not (Flag163prime-))
(not (Flag162prime-))
(not (Flag161prime-))
(not (Flag160prime-))
(not (Flag159prime-))
(not (Flag157prime-))
(not (Flag156prime-))
(not (Flag155prime-))
(not (Flag154prime-))
(not (Flag153prime-))
(not (Flag152prime-))
(not (Flag150prime-))
(not (Flag149prime-))
(not (Flag148prime-))
(not (Flag147prime-))
(not (Flag146prime-))
(not (Flag145prime-))
(not (Flag143prime-))
(not (Flag142prime-))
(not (Flag141prime-))
(not (Flag140prime-))
(not (Flag139prime-))
(not (Flag138prime-))
(not (Flag137prime-))
(not (Flag136prime-))
(not (Flag134prime-))
(not (Flag133prime-))
(not (Flag132prime-))
(not (Flag131prime-))
(not (Flag130prime-))
(not (Flag129prime-))
(not (Flag128prime-))
(not (Flag127prime-))
(not (Flag126prime-))
(not (Flag124prime-))
(not (Flag123prime-))
(not (Flag122prime-))
(not (Flag121prime-))
(not (Flag120prime-))
(not (Flag119prime-))
(not (Flag118prime-))
(not (Flag117prime-))
(not (Flag116prime-))
(not (Flag115prime-))
(not (Flag114prime-))
(not (Flag113prime-))
(not (Flag112prime-))
(not (Flag111prime-))
(not (Flag110prime-))
(not (Flag109prime-))
(not (Flag108prime-))
(not (Flag107prime-))
(not (Flag106prime-))
(not (Flag105prime-))
(not (Flag104prime-))
(not (Flag103prime-))
(not (Flag102prime-))
(not (Flag101prime-))
(not (Flag100prime-))
(not (Flag99prime-))
(not (Flag96prime-))
(not (Flag95prime-))
(not (Flag94prime-))
(not (Flag93prime-))
(not (Flag92prime-))
(not (Flag91prime-))
(not (Flag90prime-))
(not (Flag4prime-))
(not (Flag178prime-))
(not (Flag176prime-))
(not (Flag173prime-))
(not (Flag169prime-))
(not (Flag164prime-))
(not (Flag158prime-))
(not (Flag151prime-))
(not (Flag144prime-))
(not (Flag135prime-))
(not (Flag125prime-))
(not (Flag88prime-))
(not (Flag87prime-))
(not (Flag85prime-))
(not (Flag84prime-))
(not (Flag83prime-))
(not (Flag82prime-))
(not (Flag80prime-))
(not (Flag79prime-))
(not (Flag78prime-))
(not (Flag77prime-))
(not (Flag76prime-))
(not (Flag74prime-))
(not (Flag73prime-))
(not (Flag72prime-))
(not (Flag71prime-))
(not (Flag70prime-))
(not (Flag68prime-))
(not (Flag67prime-))
(not (Flag66prime-))
(not (Flag65prime-))
(not (Flag64prime-))
(not (Flag63prime-))
(not (Flag62prime-))
(not (Flag60prime-))
(not (Flag59prime-))
(not (Flag58prime-))
(not (Flag57prime-))
(not (Flag56prime-))
(not (Flag55prime-))
(not (Flag54prime-))
(not (Flag53prime-))
(not (Flag51prime-))
(not (Flag50prime-))
(not (Flag49prime-))
(not (Flag48prime-))
(not (Flag47prime-))
(not (Flag46prime-))
(not (Flag45prime-))
(not (Flag44prime-))
(not (Flag42prime-))
(not (Flag41prime-))
(not (Flag40prime-))
(not (Flag39prime-))
(not (Flag38prime-))
(not (Flag37prime-))
(not (Flag36prime-))
(not (Flag35prime-))
(not (Flag34prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag30prime-))
(not (Flag29prime-))
(not (Flag28prime-))
(not (Flag27prime-))
(not (Flag26prime-))
(not (Flag25prime-))
(not (Flag24prime-))
(not (Flag23prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag19prime-))
(not (Flag18prime-))
(not (Flag17prime-))
(not (Flag16prime-))
(not (Flag15prime-))
(not (Flag14prime-))
(not (Flag13prime-))
(not (Flag12prime-))
(not (Flag11prime-))
(not (Flag10prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag1prime-))
(not (Flag179prime-))
(not (Flag89prime-))
(not (Flag86prime-))
(not (Flag81prime-))
(not (Flag75prime-))
(not (Flag69prime-))
(not (Flag61prime-))
(not (Flag52prime-))
(not (Flag43prime-))
(not (Flag32prime-))
)
)
(:action MOVERIGHT-ROBOT
:parameters ()
:precondition
(and
(Flag4-)
(Flag4prime-)
(Flag89prime-)
(Flag86prime-)
(Flag81prime-)
(Flag75prime-)
(Flag69prime-)
(Flag61prime-)
(Flag52prime-)
(Flag43prime-)
(Flag32prime-)
(Flag21prime-)
(Flag20prime-)
(Flag19prime-)
(Flag18prime-)
(Flag17prime-)
(Flag16prime-)
(Flag15prime-)
(Flag14prime-)
(Flag13prime-)
(Flag12prime-)
(Flag11prime-)
(Flag10prime-)
(Flag9prime-)
(Flag8prime-)
(Flag7prime-)
(Flag6prime-)
(Flag5prime-)
)
:effect
(and
(when
(and
(Flag89-)
)
(and
(COLUMN9-ROBOT)
(not (NOT-COLUMN9-ROBOT))
)
)
(when
(and
(Flag86-)
)
(and
(COLUMN8-ROBOT)
(not (NOT-COLUMN8-ROBOT))
)
)
(when
(and
(Flag81-)
)
(and
(COLUMN7-ROBOT)
(not (NOT-COLUMN7-ROBOT))
)
)
(when
(and
(Flag75-)
)
(and
(COLUMN6-ROBOT)
(not (NOT-COLUMN6-ROBOT))
)
)
(when
(and
(Flag69-)
)
(and
(COLUMN5-ROBOT)
(not (NOT-COLUMN5-ROBOT))
)
)
(when
(and
(Flag61-)
)
(and
(COLUMN4-ROBOT)
(not (NOT-COLUMN4-ROBOT))
)
)
(when
(and
(Flag52-)
)
(and
(COLUMN3-ROBOT)
(not (NOT-COLUMN3-ROBOT))
)
)
(when
(and
(Flag43-)
)
(and
(COLUMN2-ROBOT)
(not (NOT-COLUMN2-ROBOT))
)
)
(when
(and
(Flag32-)
)
(and
(COLUMN1-ROBOT)
(not (NOT-COLUMN1-ROBOT))
)
)
(when
(and
(COLUMN8-ROBOT)
)
(and
(COLUMN9-ROBOT)
(NOT-COLUMN8-ROBOT)
(not (NOT-COLUMN9-ROBOT))
(not (COLUMN8-ROBOT))
)
)
(when
(and
(COLUMN7-ROBOT)
)
(and
(COLUMN8-ROBOT)
(NOT-COLUMN7-ROBOT)
(not (NOT-COLUMN8-ROBOT))
(not (COLUMN7-ROBOT))
)
)
(when
(and
(COLUMN6-ROBOT)
)
(and
(COLUMN7-ROBOT)
(NOT-COLUMN6-ROBOT)
(not (NOT-COLUMN7-ROBOT))
(not (COLUMN6-ROBOT))
)
)
(when
(and
(COLUMN5-ROBOT)
)
(and
(COLUMN6-ROBOT)
(NOT-COLUMN5-ROBOT)
(not (NOT-COLUMN6-ROBOT))
(not (COLUMN5-ROBOT))
)
)
(when
(and
(COLUMN4-ROBOT)
)
(and
(COLUMN5-ROBOT)
(NOT-COLUMN4-ROBOT)
(not (NOT-COLUMN5-ROBOT))
(not (COLUMN4-ROBOT))
)
)
(when
(and
(COLUMN3-ROBOT)
)
(and
(COLUMN4-ROBOT)
(NOT-COLUMN3-ROBOT)
(not (NOT-COLUMN4-ROBOT))
(not (COLUMN3-ROBOT))
)
)
(when
(and
(COLUMN2-ROBOT)
)
(and
(COLUMN3-ROBOT)
(NOT-COLUMN2-ROBOT)
(not (NOT-COLUMN3-ROBOT))
(not (COLUMN2-ROBOT))
)
)
(when
(and
(COLUMN1-ROBOT)
)
(and
(COLUMN2-ROBOT)
(NOT-COLUMN1-ROBOT)
(not (NOT-COLUMN2-ROBOT))
(not (COLUMN1-ROBOT))
)
)
(when
(and
(COLUMN0-ROBOT)
)
(and
(COLUMN1-ROBOT)
(NOT-COLUMN0-ROBOT)
(not (NOT-COLUMN1-ROBOT))
(not (COLUMN0-ROBOT))
)
)
(when
(and
(Flag21-)
)
(and
(LEFTOF10-ROBOT)
(NOT-LEFTOF9-ROBOT)
(not (NOT-LEFTOF10-ROBOT))
(not (LEFTOF9-ROBOT))
)
)
(when
(and
(Flag20-)
)
(and
(LEFTOF9-ROBOT)
(NOT-LEFTOF8-ROBOT)
(not (NOT-LEFTOF9-ROBOT))
(not (LEFTOF8-ROBOT))
)
)
(when
(and
(Flag19-)
)
(and
(LEFTOF8-ROBOT)
(NOT-LEFTOF7-ROBOT)
(not (NOT-LEFTOF8-ROBOT))
(not (LEFTOF7-ROBOT))
)
)
(when
(and
(Flag18-)
)
(and
(LEFTOF7-ROBOT)
(NOT-LEFTOF6-ROBOT)
(not (NOT-LEFTOF7-ROBOT))
(not (LEFTOF6-ROBOT))
)
)
(when
(and
(Flag17-)
)
(and
(LEFTOF6-ROBOT)
(NOT-LEFTOF5-ROBOT)
(not (NOT-LEFTOF6-ROBOT))
(not (LEFTOF5-ROBOT))
)
)
(when
(and
(Flag16-)
)
(and
(LEFTOF5-ROBOT)
(NOT-LEFTOF4-ROBOT)
(not (NOT-LEFTOF5-ROBOT))
(not (LEFTOF4-ROBOT))
)
)
(when
(and
(Flag15-)
)
(and
(LEFTOF4-ROBOT)
(NOT-LEFTOF3-ROBOT)
(not (NOT-LEFTOF4-ROBOT))
(not (LEFTOF3-ROBOT))
)
)
(when
(and
(Flag14-)
)
(and
(LEFTOF3-ROBOT)
(NOT-LEFTOF2-ROBOT)
(not (NOT-LEFTOF3-ROBOT))
(not (LEFTOF2-ROBOT))
)
)
(when
(and
(LEFTOF1-ROBOT)
)
(and
(LEFTOF2-ROBOT)
(NOT-LEFTOF1-ROBOT)
(not (NOT-LEFTOF2-ROBOT))
(not (LEFTOF1-ROBOT))
)
)
(when
(and
(Flag13-)
)
(and
(RIGHTOF9-ROBOT)
(not (NOT-RIGHTOF9-ROBOT))
)
)
(when
(and
(Flag12-)
)
(and
(RIGHTOF8-ROBOT)
(not (NOT-RIGHTOF8-ROBOT))
)
)
(when
(and
(Flag11-)
)
(and
(RIGHTOF7-ROBOT)
(not (NOT-RIGHTOF7-ROBOT))
)
)
(when
(and
(Flag10-)
)
(and
(RIGHTOF6-ROBOT)
(not (NOT-RIGHTOF6-ROBOT))
)
)
(when
(and
(Flag9-)
)
(and
(RIGHTOF5-ROBOT)
(not (NOT-RIGHTOF5-ROBOT))
)
)
(when
(and
(Flag8-)
)
(and
(RIGHTOF4-ROBOT)
(not (NOT-RIGHTOF4-ROBOT))
)
)
(when
(and
(Flag7-)
)
(and
(RIGHTOF3-ROBOT)
(not (NOT-RIGHTOF3-ROBOT))
)
)
(when
(and
(Flag6-)
)
(and
(RIGHTOF2-ROBOT)
(not (NOT-RIGHTOF2-ROBOT))
)
)
(when
(and
(Flag5-)
)
(and
(RIGHTOF1-ROBOT)
(not (NOT-RIGHTOF1-ROBOT))
)
)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag177prime-))
(not (Flag98prime-))
(not (Flag97prime-))
(not (Flag21prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag180prime-))
(not (Flag175prime-))
(not (Flag174prime-))
(not (Flag172prime-))
(not (Flag171prime-))
(not (Flag170prime-))
(not (Flag168prime-))
(not (Flag167prime-))
(not (Flag166prime-))
(not (Flag165prime-))
(not (Flag163prime-))
(not (Flag162prime-))
(not (Flag161prime-))
(not (Flag160prime-))
(not (Flag159prime-))
(not (Flag157prime-))
(not (Flag156prime-))
(not (Flag155prime-))
(not (Flag154prime-))
(not (Flag153prime-))
(not (Flag152prime-))
(not (Flag150prime-))
(not (Flag149prime-))
(not (Flag148prime-))
(not (Flag147prime-))
(not (Flag146prime-))
(not (Flag145prime-))
(not (Flag143prime-))
(not (Flag142prime-))
(not (Flag141prime-))
(not (Flag140prime-))
(not (Flag139prime-))
(not (Flag138prime-))
(not (Flag137prime-))
(not (Flag136prime-))
(not (Flag134prime-))
(not (Flag133prime-))
(not (Flag132prime-))
(not (Flag131prime-))
(not (Flag130prime-))
(not (Flag129prime-))
(not (Flag128prime-))
(not (Flag127prime-))
(not (Flag126prime-))
(not (Flag124prime-))
(not (Flag123prime-))
(not (Flag122prime-))
(not (Flag121prime-))
(not (Flag120prime-))
(not (Flag119prime-))
(not (Flag118prime-))
(not (Flag117prime-))
(not (Flag116prime-))
(not (Flag115prime-))
(not (Flag114prime-))
(not (Flag113prime-))
(not (Flag112prime-))
(not (Flag111prime-))
(not (Flag110prime-))
(not (Flag109prime-))
(not (Flag108prime-))
(not (Flag107prime-))
(not (Flag106prime-))
(not (Flag105prime-))
(not (Flag104prime-))
(not (Flag103prime-))
(not (Flag102prime-))
(not (Flag101prime-))
(not (Flag100prime-))
(not (Flag99prime-))
(not (Flag96prime-))
(not (Flag95prime-))
(not (Flag94prime-))
(not (Flag93prime-))
(not (Flag92prime-))
(not (Flag91prime-))
(not (Flag90prime-))
(not (Flag4prime-))
(not (Flag178prime-))
(not (Flag176prime-))
(not (Flag173prime-))
(not (Flag169prime-))
(not (Flag164prime-))
(not (Flag158prime-))
(not (Flag151prime-))
(not (Flag144prime-))
(not (Flag135prime-))
(not (Flag125prime-))
(not (Flag88prime-))
(not (Flag87prime-))
(not (Flag85prime-))
(not (Flag84prime-))
(not (Flag83prime-))
(not (Flag82prime-))
(not (Flag80prime-))
(not (Flag79prime-))
(not (Flag78prime-))
(not (Flag77prime-))
(not (Flag76prime-))
(not (Flag74prime-))
(not (Flag73prime-))
(not (Flag72prime-))
(not (Flag71prime-))
(not (Flag70prime-))
(not (Flag68prime-))
(not (Flag67prime-))
(not (Flag66prime-))
(not (Flag65prime-))
(not (Flag64prime-))
(not (Flag63prime-))
(not (Flag62prime-))
(not (Flag60prime-))
(not (Flag59prime-))
(not (Flag58prime-))
(not (Flag57prime-))
(not (Flag56prime-))
(not (Flag55prime-))
(not (Flag54prime-))
(not (Flag53prime-))
(not (Flag51prime-))
(not (Flag50prime-))
(not (Flag49prime-))
(not (Flag48prime-))
(not (Flag47prime-))
(not (Flag46prime-))
(not (Flag45prime-))
(not (Flag44prime-))
(not (Flag42prime-))
(not (Flag41prime-))
(not (Flag40prime-))
(not (Flag39prime-))
(not (Flag38prime-))
(not (Flag37prime-))
(not (Flag36prime-))
(not (Flag35prime-))
(not (Flag34prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag30prime-))
(not (Flag29prime-))
(not (Flag28prime-))
(not (Flag27prime-))
(not (Flag26prime-))
(not (Flag25prime-))
(not (Flag24prime-))
(not (Flag23prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag19prime-))
(not (Flag18prime-))
(not (Flag17prime-))
(not (Flag16prime-))
(not (Flag15prime-))
(not (Flag14prime-))
(not (Flag13prime-))
(not (Flag12prime-))
(not (Flag11prime-))
(not (Flag10prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag1prime-))
(not (Flag179prime-))
(not (Flag89prime-))
(not (Flag86prime-))
(not (Flag81prime-))
(not (Flag75prime-))
(not (Flag69prime-))
(not (Flag61prime-))
(not (Flag52prime-))
(not (Flag43prime-))
(not (Flag32prime-))
)
)
(:action Prim1Action-1
:parameters ()
:precondition
(and
(not (ROW1-ROBOT))
)
:effect
(and
(Flag1prime-)
(not (Flag1-))
)

)
(:action CHECKCONSISTENCYACTION
:parameters ()
:precondition
(and
(NOT-ERROR-)
(CHECKCONSISTENCY-)
)
:effect
(and
(when
(and
(RIGHTOF9-ROBOT)
(LEFTOF1-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF3-ROBOT)
(RIGHTOF5-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF9-ROBOT)
(BELOWOF6-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF5-ROBOT)
(ABOVEOF9-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF8-ROBOT)
(BELOWOF1-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF5-ROBOT)
(RIGHTOF9-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF9-ROBOT)
(RIGHTOF9-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF1-ROBOT)
(RIGHTOF5-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF7-ROBOT)
(BELOWOF4-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF1-ROBOT)
(ABOVEOF9-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF1-ROBOT)
(ABOVEOF2-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF4-ROBOT)
(BELOWOF2-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF9-ROBOT)
(LEFTOF7-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF7-ROBOT)
(BELOWOF1-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF1-ROBOT)
(RIGHTOF8-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF1-ROBOT)
(RIGHTOF7-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF8-ROBOT)
(LEFTOF4-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF9-ROBOT)
(LEFTOF3-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF8-ROBOT)
(LEFTOF2-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF1-ROBOT)
(RIGHTOF6-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF9-ROBOT)
(LEFTOF8-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF8-ROBOT)
(LEFTOF3-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF4-ROBOT)
(BELOWOF1-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF3-ROBOT)
(ABOVEOF9-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF3-ROBOT)
(BELOWOF2-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF8-ROBOT)
(BELOWOF6-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF8-ROBOT)
(ABOVEOF9-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF3-ROBOT)
(BELOWOF1-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF6-ROBOT)
(BELOWOF2-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF7-ROBOT)
(LEFTOF5-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF9-ROBOT)
(LEFTOF6-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF7-ROBOT)
(BELOWOF2-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF7-ROBOT)
(LEFTOF3-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF5-ROBOT)
(BELOWOF3-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF7-ROBOT)
(BELOWOF3-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF9-ROBOT)
(BELOWOF2-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF4-ROBOT)
(RIGHTOF5-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF3-ROBOT)
(LEFTOF3-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF4-ROBOT)
(LEFTOF1-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF6-ROBOT)
(LEFTOF3-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF5-ROBOT)
(BELOWOF1-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF7-ROBOT)
(BELOWOF5-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF6-ROBOT)
(LEFTOF2-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF7-ROBOT)
(LEFTOF4-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF6-ROBOT)
(BELOWOF6-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF9-ROBOT)
(LEFTOF4-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF4-ROBOT)
(RIGHTOF6-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF8-ROBOT)
(BELOWOF2-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF6-ROBOT)
(BELOWOF5-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF6-ROBOT)
(LEFTOF6-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF8-ROBOT)
(BELOWOF4-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF5-ROBOT)
(LEFTOF2-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF7-ROBOT)
(ABOVEOF9-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF4-ROBOT)
(ABOVEOF9-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF5-ROBOT)
(BELOWOF2-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF9-ROBOT)
(LEFTOF2-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF7-ROBOT)
(LEFTOF2-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF4-ROBOT)
(LEFTOF3-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF8-ROBOT)
(BELOWOF5-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF6-ROBOT)
(ABOVEOF7-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF1-ROBOT)
(RIGHTOF2-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF2-ROBOT)
(RIGHTOF4-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF8-ROBOT)
(BELOWOF3-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF6-ROBOT)
(RIGHTOF8-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF6-ROBOT)
(BELOWOF3-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF2-ROBOT)
(RIGHTOF2-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF5-ROBOT)
(BELOWOF4-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF1-ROBOT)
(RIGHTOF1-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF8-ROBOT)
(LEFTOF5-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF8-ROBOT)
(RIGHTOF8-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF5-ROBOT)
(BELOWOF5-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF3-ROBOT)
(BELOWOF3-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF7-ROBOT)
(LEFTOF6-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF6-ROBOT)
(BELOWOF1-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF8-ROBOT)
(LEFTOF7-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF2-ROBOT)
(ABOVEOF2-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF3-ROBOT)
(ABOVEOF4-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF8-ROBOT)
(BELOWOF8-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF6-ROBOT)
(LEFTOF5-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF8-ROBOT)
(BELOWOF7-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF4-ROBOT)
(RIGHTOF4-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF7-ROBOT)
(RIGHTOF7-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF6-ROBOT)
(BELOWOF4-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF1-ROBOT)
(BELOWOF1-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF2-ROBOT)
(RIGHTOF3-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF5-ROBOT)
(LEFTOF5-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF7-ROBOT)
(BELOWOF7-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF1-ROBOT)
(RIGHTOF3-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF4-ROBOT)
(BELOWOF4-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF9-ROBOT)
(BELOWOF9-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(NOT-CHECKCONSISTENCY-)
(not (CHECKCONSISTENCY-))
(not (Flag177prime-))
(not (Flag98prime-))
(not (Flag97prime-))
(not (Flag21prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag180prime-))
(not (Flag175prime-))
(not (Flag174prime-))
(not (Flag172prime-))
(not (Flag171prime-))
(not (Flag170prime-))
(not (Flag168prime-))
(not (Flag167prime-))
(not (Flag166prime-))
(not (Flag165prime-))
(not (Flag163prime-))
(not (Flag162prime-))
(not (Flag161prime-))
(not (Flag160prime-))
(not (Flag159prime-))
(not (Flag157prime-))
(not (Flag156prime-))
(not (Flag155prime-))
(not (Flag154prime-))
(not (Flag153prime-))
(not (Flag152prime-))
(not (Flag150prime-))
(not (Flag149prime-))
(not (Flag148prime-))
(not (Flag147prime-))
(not (Flag146prime-))
(not (Flag145prime-))
(not (Flag143prime-))
(not (Flag142prime-))
(not (Flag141prime-))
(not (Flag140prime-))
(not (Flag139prime-))
(not (Flag138prime-))
(not (Flag137prime-))
(not (Flag136prime-))
(not (Flag134prime-))
(not (Flag133prime-))
(not (Flag132prime-))
(not (Flag131prime-))
(not (Flag130prime-))
(not (Flag129prime-))
(not (Flag128prime-))
(not (Flag127prime-))
(not (Flag126prime-))
(not (Flag124prime-))
(not (Flag123prime-))
(not (Flag122prime-))
(not (Flag121prime-))
(not (Flag120prime-))
(not (Flag119prime-))
(not (Flag118prime-))
(not (Flag117prime-))
(not (Flag116prime-))
(not (Flag115prime-))
(not (Flag114prime-))
(not (Flag113prime-))
(not (Flag112prime-))
(not (Flag111prime-))
(not (Flag110prime-))
(not (Flag109prime-))
(not (Flag108prime-))
(not (Flag107prime-))
(not (Flag106prime-))
(not (Flag105prime-))
(not (Flag104prime-))
(not (Flag103prime-))
(not (Flag102prime-))
(not (Flag101prime-))
(not (Flag100prime-))
(not (Flag99prime-))
(not (Flag96prime-))
(not (Flag95prime-))
(not (Flag94prime-))
(not (Flag93prime-))
(not (Flag92prime-))
(not (Flag91prime-))
(not (Flag90prime-))
(not (Flag4prime-))
(not (Flag178prime-))
(not (Flag176prime-))
(not (Flag173prime-))
(not (Flag169prime-))
(not (Flag164prime-))
(not (Flag158prime-))
(not (Flag151prime-))
(not (Flag144prime-))
(not (Flag135prime-))
(not (Flag125prime-))
(not (Flag88prime-))
(not (Flag87prime-))
(not (Flag85prime-))
(not (Flag84prime-))
(not (Flag83prime-))
(not (Flag82prime-))
(not (Flag80prime-))
(not (Flag79prime-))
(not (Flag78prime-))
(not (Flag77prime-))
(not (Flag76prime-))
(not (Flag74prime-))
(not (Flag73prime-))
(not (Flag72prime-))
(not (Flag71prime-))
(not (Flag70prime-))
(not (Flag68prime-))
(not (Flag67prime-))
(not (Flag66prime-))
(not (Flag65prime-))
(not (Flag64prime-))
(not (Flag63prime-))
(not (Flag62prime-))
(not (Flag60prime-))
(not (Flag59prime-))
(not (Flag58prime-))
(not (Flag57prime-))
(not (Flag56prime-))
(not (Flag55prime-))
(not (Flag54prime-))
(not (Flag53prime-))
(not (Flag51prime-))
(not (Flag50prime-))
(not (Flag49prime-))
(not (Flag48prime-))
(not (Flag47prime-))
(not (Flag46prime-))
(not (Flag45prime-))
(not (Flag44prime-))
(not (Flag42prime-))
(not (Flag41prime-))
(not (Flag40prime-))
(not (Flag39prime-))
(not (Flag38prime-))
(not (Flag37prime-))
(not (Flag36prime-))
(not (Flag35prime-))
(not (Flag34prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag30prime-))
(not (Flag29prime-))
(not (Flag28prime-))
(not (Flag27prime-))
(not (Flag26prime-))
(not (Flag25prime-))
(not (Flag24prime-))
(not (Flag23prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag19prime-))
(not (Flag18prime-))
(not (Flag17prime-))
(not (Flag16prime-))
(not (Flag15prime-))
(not (Flag14prime-))
(not (Flag13prime-))
(not (Flag12prime-))
(not (Flag11prime-))
(not (Flag10prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag1prime-))
(not (Flag179prime-))
(not (Flag89prime-))
(not (Flag86prime-))
(not (Flag81prime-))
(not (Flag75prime-))
(not (Flag69prime-))
(not (Flag61prime-))
(not (Flag52prime-))
(not (Flag43prime-))
(not (Flag32prime-))
)
)
(:action Flag4Action
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(Flag3-)
(Flag3prime-)
)
:effect
(and
(Flag4-)
(Flag4prime-)
)

)
(:action Prim4Action-2
:parameters ()
:precondition
(and
(not (Flag3-))
(Flag3prime-)
)
:effect
(and
(Flag4prime-)
(not (Flag4-))
)

)
(:action Flag90Action-0
:parameters ()
:precondition
(and
(BELOWOF2-ROBOT)
)
:effect
(and
(Flag90-)
(Flag90prime-)
)

)
(:action Flag90Action-1
:parameters ()
:precondition
(and
(BELOWOF1-ROBOT)
)
:effect
(and
(Flag90-)
(Flag90prime-)
)

)
(:action Prim90Action
:parameters ()
:precondition
(and
(not (BELOWOF2-ROBOT))
(not (BELOWOF1-ROBOT))
)
:effect
(and
(Flag90prime-)
(not (Flag90-))
)

)
(:action Flag91Action-0
:parameters ()
:precondition
(and
(BELOWOF2-ROBOT)
)
:effect
(and
(Flag91-)
(Flag91prime-)
)

)
(:action Flag91Action-1
:parameters ()
:precondition
(and
(BELOWOF1-ROBOT)
)
:effect
(and
(Flag91-)
(Flag91prime-)
)

)
(:action Flag91Action-2
:parameters ()
:precondition
(and
(BELOWOF3-ROBOT)
)
:effect
(and
(Flag91-)
(Flag91prime-)
)

)
(:action Prim91Action
:parameters ()
:precondition
(and
(not (BELOWOF2-ROBOT))
(not (BELOWOF1-ROBOT))
(not (BELOWOF3-ROBOT))
)
:effect
(and
(Flag91prime-)
(not (Flag91-))
)

)
(:action Flag92Action-0
:parameters ()
:precondition
(and
(BELOWOF2-ROBOT)
)
:effect
(and
(Flag92-)
(Flag92prime-)
)

)
(:action Flag92Action-1
:parameters ()
:precondition
(and
(BELOWOF1-ROBOT)
)
:effect
(and
(Flag92-)
(Flag92prime-)
)

)
(:action Flag92Action-2
:parameters ()
:precondition
(and
(BELOWOF3-ROBOT)
)
:effect
(and
(Flag92-)
(Flag92prime-)
)

)
(:action Flag92Action-3
:parameters ()
:precondition
(and
(BELOWOF4-ROBOT)
)
:effect
(and
(Flag92-)
(Flag92prime-)
)

)
(:action Prim92Action
:parameters ()
:precondition
(and
(not (BELOWOF2-ROBOT))
(not (BELOWOF1-ROBOT))
(not (BELOWOF3-ROBOT))
(not (BELOWOF4-ROBOT))
)
:effect
(and
(Flag92prime-)
(not (Flag92-))
)

)
(:action Flag93Action-0
:parameters ()
:precondition
(and
(BELOWOF2-ROBOT)
)
:effect
(and
(Flag93-)
(Flag93prime-)
)

)
(:action Flag93Action-1
:parameters ()
:precondition
(and
(BELOWOF1-ROBOT)
)
:effect
(and
(Flag93-)
(Flag93prime-)
)

)
(:action Flag93Action-2
:parameters ()
:precondition
(and
(BELOWOF3-ROBOT)
)
:effect
(and
(Flag93-)
(Flag93prime-)
)

)
(:action Flag93Action-3
:parameters ()
:precondition
(and
(BELOWOF4-ROBOT)
)
:effect
(and
(Flag93-)
(Flag93prime-)
)

)
(:action Flag93Action-4
:parameters ()
:precondition
(and
(BELOWOF5-ROBOT)
)
:effect
(and
(Flag93-)
(Flag93prime-)
)

)
(:action Prim93Action
:parameters ()
:precondition
(and
(not (BELOWOF2-ROBOT))
(not (BELOWOF1-ROBOT))
(not (BELOWOF3-ROBOT))
(not (BELOWOF4-ROBOT))
(not (BELOWOF5-ROBOT))
)
:effect
(and
(Flag93prime-)
(not (Flag93-))
)

)
(:action Flag94Action-0
:parameters ()
:precondition
(and
(BELOWOF6-ROBOT)
)
:effect
(and
(Flag94-)
(Flag94prime-)
)

)
(:action Flag94Action-1
:parameters ()
:precondition
(and
(BELOWOF5-ROBOT)
)
:effect
(and
(Flag94-)
(Flag94prime-)
)

)
(:action Flag94Action-2
:parameters ()
:precondition
(and
(BELOWOF2-ROBOT)
)
:effect
(and
(Flag94-)
(Flag94prime-)
)

)
(:action Flag94Action-3
:parameters ()
:precondition
(and
(BELOWOF1-ROBOT)
)
:effect
(and
(Flag94-)
(Flag94prime-)
)

)
(:action Flag94Action-4
:parameters ()
:precondition
(and
(BELOWOF4-ROBOT)
)
:effect
(and
(Flag94-)
(Flag94prime-)
)

)
(:action Flag94Action-5
:parameters ()
:precondition
(and
(BELOWOF3-ROBOT)
)
:effect
(and
(Flag94-)
(Flag94prime-)
)

)
(:action Prim94Action
:parameters ()
:precondition
(and
(not (BELOWOF6-ROBOT))
(not (BELOWOF5-ROBOT))
(not (BELOWOF2-ROBOT))
(not (BELOWOF1-ROBOT))
(not (BELOWOF4-ROBOT))
(not (BELOWOF3-ROBOT))
)
:effect
(and
(Flag94prime-)
(not (Flag94-))
)

)
(:action Flag95Action-0
:parameters ()
:precondition
(and
(BELOWOF6-ROBOT)
)
:effect
(and
(Flag95-)
(Flag95prime-)
)

)
(:action Flag95Action-1
:parameters ()
:precondition
(and
(BELOWOF5-ROBOT)
)
:effect
(and
(Flag95-)
(Flag95prime-)
)

)
(:action Flag95Action-2
:parameters ()
:precondition
(and
(BELOWOF2-ROBOT)
)
:effect
(and
(Flag95-)
(Flag95prime-)
)

)
(:action Flag95Action-3
:parameters ()
:precondition
(and
(BELOWOF1-ROBOT)
)
:effect
(and
(Flag95-)
(Flag95prime-)
)

)
(:action Flag95Action-4
:parameters ()
:precondition
(and
(BELOWOF4-ROBOT)
)
:effect
(and
(Flag95-)
(Flag95prime-)
)

)
(:action Flag95Action-5
:parameters ()
:precondition
(and
(BELOWOF7-ROBOT)
)
:effect
(and
(Flag95-)
(Flag95prime-)
)

)
(:action Flag95Action-6
:parameters ()
:precondition
(and
(BELOWOF3-ROBOT)
)
:effect
(and
(Flag95-)
(Flag95prime-)
)

)
(:action Prim95Action
:parameters ()
:precondition
(and
(not (BELOWOF6-ROBOT))
(not (BELOWOF5-ROBOT))
(not (BELOWOF2-ROBOT))
(not (BELOWOF1-ROBOT))
(not (BELOWOF4-ROBOT))
(not (BELOWOF7-ROBOT))
(not (BELOWOF3-ROBOT))
)
:effect
(and
(Flag95prime-)
(not (Flag95-))
)

)
(:action Flag96Action-0
:parameters ()
:precondition
(and
(BELOWOF6-ROBOT)
)
:effect
(and
(Flag96-)
(Flag96prime-)
)

)
(:action Flag96Action-1
:parameters ()
:precondition
(and
(BELOWOF5-ROBOT)
)
:effect
(and
(Flag96-)
(Flag96prime-)
)

)
(:action Flag96Action-2
:parameters ()
:precondition
(and
(BELOWOF2-ROBOT)
)
:effect
(and
(Flag96-)
(Flag96prime-)
)

)
(:action Flag96Action-3
:parameters ()
:precondition
(and
(BELOWOF1-ROBOT)
)
:effect
(and
(Flag96-)
(Flag96prime-)
)

)
(:action Flag96Action-4
:parameters ()
:precondition
(and
(BELOWOF4-ROBOT)
)
:effect
(and
(Flag96-)
(Flag96prime-)
)

)
(:action Flag96Action-5
:parameters ()
:precondition
(and
(BELOWOF7-ROBOT)
)
:effect
(and
(Flag96-)
(Flag96prime-)
)

)
(:action Flag96Action-6
:parameters ()
:precondition
(and
(BELOWOF3-ROBOT)
)
:effect
(and
(Flag96-)
(Flag96prime-)
)

)
(:action Flag96Action-7
:parameters ()
:precondition
(and
(BELOWOF8-ROBOT)
)
:effect
(and
(Flag96-)
(Flag96prime-)
)

)
(:action Prim96Action
:parameters ()
:precondition
(and
(not (BELOWOF6-ROBOT))
(not (BELOWOF5-ROBOT))
(not (BELOWOF2-ROBOT))
(not (BELOWOF1-ROBOT))
(not (BELOWOF4-ROBOT))
(not (BELOWOF7-ROBOT))
(not (BELOWOF3-ROBOT))
(not (BELOWOF8-ROBOT))
)
:effect
(and
(Flag96prime-)
(not (Flag96-))
)

)
(:action Flag97Action-0
:parameters ()
:precondition
(and
(BELOWOF6-ROBOT)
)
:effect
(and
(Flag97-)
(Flag97prime-)
)

)
(:action Flag97Action-1
:parameters ()
:precondition
(and
(BELOWOF5-ROBOT)
)
:effect
(and
(Flag97-)
(Flag97prime-)
)

)
(:action Flag97Action-2
:parameters ()
:precondition
(and
(BELOWOF2-ROBOT)
)
:effect
(and
(Flag97-)
(Flag97prime-)
)

)
(:action Flag97Action-3
:parameters ()
:precondition
(and
(BELOWOF1-ROBOT)
)
:effect
(and
(Flag97-)
(Flag97prime-)
)

)
(:action Flag97Action-4
:parameters ()
:precondition
(and
(BELOWOF4-ROBOT)
)
:effect
(and
(Flag97-)
(Flag97prime-)
)

)
(:action Flag97Action-5
:parameters ()
:precondition
(and
(BELOWOF7-ROBOT)
)
:effect
(and
(Flag97-)
(Flag97prime-)
)

)
(:action Flag97Action-7
:parameters ()
:precondition
(and
(BELOWOF3-ROBOT)
)
:effect
(and
(Flag97-)
(Flag97prime-)
)

)
(:action Flag97Action-8
:parameters ()
:precondition
(and
(BELOWOF8-ROBOT)
)
:effect
(and
(Flag97-)
(Flag97prime-)
)

)
(:action Prim97Action
:parameters ()
:precondition
(and
(not (BELOWOF6-ROBOT))
(not (BELOWOF5-ROBOT))
(not (BELOWOF2-ROBOT))
(not (BELOWOF1-ROBOT))
(not (BELOWOF4-ROBOT))
(not (BELOWOF7-ROBOT))
(not (BELOWOF9-ROBOT))
(not (BELOWOF3-ROBOT))
(not (BELOWOF8-ROBOT))
)
:effect
(and
(Flag97prime-)
(not (Flag97-))
)

)
(:action Flag98Action-0
:parameters ()
:precondition
(and
(BELOWOF10-ROBOT)
)
:effect
(and
(Flag98-)
(Flag98prime-)
)

)
(:action Flag98Action-1
:parameters ()
:precondition
(and
(BELOWOF6-ROBOT)
)
:effect
(and
(Flag98-)
(Flag98prime-)
)

)
(:action Flag98Action-2
:parameters ()
:precondition
(and
(BELOWOF5-ROBOT)
)
:effect
(and
(Flag98-)
(Flag98prime-)
)

)
(:action Flag98Action-3
:parameters ()
:precondition
(and
(BELOWOF2-ROBOT)
)
:effect
(and
(Flag98-)
(Flag98prime-)
)

)
(:action Flag98Action-4
:parameters ()
:precondition
(and
(BELOWOF1-ROBOT)
)
:effect
(and
(Flag98-)
(Flag98prime-)
)

)
(:action Flag98Action-5
:parameters ()
:precondition
(and
(BELOWOF4-ROBOT)
)
:effect
(and
(Flag98-)
(Flag98prime-)
)

)
(:action Flag98Action-6
:parameters ()
:precondition
(and
(BELOWOF7-ROBOT)
)
:effect
(and
(Flag98-)
(Flag98prime-)
)

)
(:action Flag98Action-8
:parameters ()
:precondition
(and
(BELOWOF3-ROBOT)
)
:effect
(and
(Flag98-)
(Flag98prime-)
)

)
(:action Flag98Action-9
:parameters ()
:precondition
(and
(BELOWOF8-ROBOT)
)
:effect
(and
(Flag98-)
(Flag98prime-)
)

)
(:action Prim98Action
:parameters ()
:precondition
(and
(not (BELOWOF10-ROBOT))
(not (BELOWOF6-ROBOT))
(not (BELOWOF5-ROBOT))
(not (BELOWOF2-ROBOT))
(not (BELOWOF1-ROBOT))
(not (BELOWOF4-ROBOT))
(not (BELOWOF7-ROBOT))
(not (BELOWOF9-ROBOT))
(not (BELOWOF3-ROBOT))
(not (BELOWOF8-ROBOT))
)
:effect
(and
(Flag98prime-)
(not (Flag98-))
)

)
(:action Flag99Action-0
:parameters ()
:precondition
(and
(ABOVEOF6-ROBOT)
)
:effect
(and
(Flag99-)
(Flag99prime-)
)

)
(:action Flag99Action-1
:parameters ()
:precondition
(and
(ABOVEOF1-ROBOT)
)
:effect
(and
(Flag99-)
(Flag99prime-)
)

)
(:action Flag99Action-2
:parameters ()
:precondition
(and
(ABOVEOF2-ROBOT)
)
:effect
(and
(Flag99-)
(Flag99prime-)
)

)
(:action Flag99Action-3
:parameters ()
:precondition
(and
(ABOVEOF7-ROBOT)
)
:effect
(and
(Flag99-)
(Flag99prime-)
)

)
(:action Flag99Action-4
:parameters ()
:precondition
(and
(ABOVEOF8-ROBOT)
)
:effect
(and
(Flag99-)
(Flag99prime-)
)

)
(:action Flag99Action-5
:parameters ()
:precondition
(and
(ABOVEOF9-ROBOT)
)
:effect
(and
(Flag99-)
(Flag99prime-)
)

)
(:action Flag99Action-6
:parameters ()
:precondition
(and
(ABOVEOF4-ROBOT)
)
:effect
(and
(Flag99-)
(Flag99prime-)
)

)
(:action Flag99Action-7
:parameters ()
:precondition
(and
(ABOVEOF3-ROBOT)
)
:effect
(and
(Flag99-)
(Flag99prime-)
)

)
(:action Flag99Action-8
:parameters ()
:precondition
(and
(ABOVEOF5-ROBOT)
)
:effect
(and
(Flag99-)
(Flag99prime-)
)

)
(:action Prim99Action
:parameters ()
:precondition
(and
(not (ABOVEOF6-ROBOT))
(not (ABOVEOF1-ROBOT))
(not (ABOVEOF2-ROBOT))
(not (ABOVEOF7-ROBOT))
(not (ABOVEOF8-ROBOT))
(not (ABOVEOF9-ROBOT))
(not (ABOVEOF4-ROBOT))
(not (ABOVEOF3-ROBOT))
(not (ABOVEOF5-ROBOT))
)
:effect
(and
(Flag99prime-)
(not (Flag99-))
)

)
(:action Flag100Action-0
:parameters ()
:precondition
(and
(ABOVEOF6-ROBOT)
)
:effect
(and
(Flag100-)
(Flag100prime-)
)

)
(:action Flag100Action-1
:parameters ()
:precondition
(and
(ABOVEOF2-ROBOT)
)
:effect
(and
(Flag100-)
(Flag100prime-)
)

)
(:action Flag100Action-2
:parameters ()
:precondition
(and
(ABOVEOF7-ROBOT)
)
:effect
(and
(Flag100-)
(Flag100prime-)
)

)
(:action Flag100Action-3
:parameters ()
:precondition
(and
(ABOVEOF8-ROBOT)
)
:effect
(and
(Flag100-)
(Flag100prime-)
)

)
(:action Flag100Action-4
:parameters ()
:precondition
(and
(ABOVEOF9-ROBOT)
)
:effect
(and
(Flag100-)
(Flag100prime-)
)

)
(:action Flag100Action-5
:parameters ()
:precondition
(and
(ABOVEOF4-ROBOT)
)
:effect
(and
(Flag100-)
(Flag100prime-)
)

)
(:action Flag100Action-6
:parameters ()
:precondition
(and
(ABOVEOF3-ROBOT)
)
:effect
(and
(Flag100-)
(Flag100prime-)
)

)
(:action Flag100Action-7
:parameters ()
:precondition
(and
(ABOVEOF5-ROBOT)
)
:effect
(and
(Flag100-)
(Flag100prime-)
)

)
(:action Prim100Action
:parameters ()
:precondition
(and
(not (ABOVEOF6-ROBOT))
(not (ABOVEOF2-ROBOT))
(not (ABOVEOF7-ROBOT))
(not (ABOVEOF8-ROBOT))
(not (ABOVEOF9-ROBOT))
(not (ABOVEOF4-ROBOT))
(not (ABOVEOF3-ROBOT))
(not (ABOVEOF5-ROBOT))
)
:effect
(and
(Flag100prime-)
(not (Flag100-))
)

)
(:action Flag101Action-0
:parameters ()
:precondition
(and
(ABOVEOF6-ROBOT)
)
:effect
(and
(Flag101-)
(Flag101prime-)
)

)
(:action Flag101Action-1
:parameters ()
:precondition
(and
(ABOVEOF8-ROBOT)
)
:effect
(and
(Flag101-)
(Flag101prime-)
)

)
(:action Flag101Action-2
:parameters ()
:precondition
(and
(ABOVEOF7-ROBOT)
)
:effect
(and
(Flag101-)
(Flag101prime-)
)

)
(:action Flag101Action-3
:parameters ()
:precondition
(and
(ABOVEOF9-ROBOT)
)
:effect
(and
(Flag101-)
(Flag101prime-)
)

)
(:action Flag101Action-4
:parameters ()
:precondition
(and
(ABOVEOF4-ROBOT)
)
:effect
(and
(Flag101-)
(Flag101prime-)
)

)
(:action Flag101Action-5
:parameters ()
:precondition
(and
(ABOVEOF3-ROBOT)
)
:effect
(and
(Flag101-)
(Flag101prime-)
)

)
(:action Flag101Action-6
:parameters ()
:precondition
(and
(ABOVEOF5-ROBOT)
)
:effect
(and
(Flag101-)
(Flag101prime-)
)

)
(:action Prim101Action
:parameters ()
:precondition
(and
(not (ABOVEOF6-ROBOT))
(not (ABOVEOF8-ROBOT))
(not (ABOVEOF7-ROBOT))
(not (ABOVEOF9-ROBOT))
(not (ABOVEOF4-ROBOT))
(not (ABOVEOF3-ROBOT))
(not (ABOVEOF5-ROBOT))
)
:effect
(and
(Flag101prime-)
(not (Flag101-))
)

)
(:action Flag102Action-0
:parameters ()
:precondition
(and
(ABOVEOF6-ROBOT)
)
:effect
(and
(Flag102-)
(Flag102prime-)
)

)
(:action Flag102Action-1
:parameters ()
:precondition
(and
(ABOVEOF8-ROBOT)
)
:effect
(and
(Flag102-)
(Flag102prime-)
)

)
(:action Flag102Action-2
:parameters ()
:precondition
(and
(ABOVEOF7-ROBOT)
)
:effect
(and
(Flag102-)
(Flag102prime-)
)

)
(:action Flag102Action-3
:parameters ()
:precondition
(and
(ABOVEOF9-ROBOT)
)
:effect
(and
(Flag102-)
(Flag102prime-)
)

)
(:action Flag102Action-4
:parameters ()
:precondition
(and
(ABOVEOF4-ROBOT)
)
:effect
(and
(Flag102-)
(Flag102prime-)
)

)
(:action Flag102Action-5
:parameters ()
:precondition
(and
(ABOVEOF5-ROBOT)
)
:effect
(and
(Flag102-)
(Flag102prime-)
)

)
(:action Prim102Action
:parameters ()
:precondition
(and
(not (ABOVEOF6-ROBOT))
(not (ABOVEOF8-ROBOT))
(not (ABOVEOF7-ROBOT))
(not (ABOVEOF9-ROBOT))
(not (ABOVEOF4-ROBOT))
(not (ABOVEOF5-ROBOT))
)
:effect
(and
(Flag102prime-)
(not (Flag102-))
)

)
(:action Flag103Action-0
:parameters ()
:precondition
(and
(ABOVEOF8-ROBOT)
)
:effect
(and
(Flag103-)
(Flag103prime-)
)

)
(:action Flag103Action-1
:parameters ()
:precondition
(and
(ABOVEOF9-ROBOT)
)
:effect
(and
(Flag103-)
(Flag103prime-)
)

)
(:action Flag103Action-2
:parameters ()
:precondition
(and
(ABOVEOF7-ROBOT)
)
:effect
(and
(Flag103-)
(Flag103prime-)
)

)
(:action Flag103Action-3
:parameters ()
:precondition
(and
(ABOVEOF5-ROBOT)
)
:effect
(and
(Flag103-)
(Flag103prime-)
)

)
(:action Flag103Action-4
:parameters ()
:precondition
(and
(ABOVEOF6-ROBOT)
)
:effect
(and
(Flag103-)
(Flag103prime-)
)

)
(:action Prim103Action
:parameters ()
:precondition
(and
(not (ABOVEOF8-ROBOT))
(not (ABOVEOF9-ROBOT))
(not (ABOVEOF7-ROBOT))
(not (ABOVEOF5-ROBOT))
(not (ABOVEOF6-ROBOT))
)
:effect
(and
(Flag103prime-)
(not (Flag103-))
)

)
(:action Flag104Action-0
:parameters ()
:precondition
(and
(ABOVEOF8-ROBOT)
)
:effect
(and
(Flag104-)
(Flag104prime-)
)

)
(:action Flag104Action-1
:parameters ()
:precondition
(and
(ABOVEOF9-ROBOT)
)
:effect
(and
(Flag104-)
(Flag104prime-)
)

)
(:action Flag104Action-2
:parameters ()
:precondition
(and
(ABOVEOF7-ROBOT)
)
:effect
(and
(Flag104-)
(Flag104prime-)
)

)
(:action Flag104Action-3
:parameters ()
:precondition
(and
(ABOVEOF6-ROBOT)
)
:effect
(and
(Flag104-)
(Flag104prime-)
)

)
(:action Prim104Action
:parameters ()
:precondition
(and
(not (ABOVEOF8-ROBOT))
(not (ABOVEOF9-ROBOT))
(not (ABOVEOF7-ROBOT))
(not (ABOVEOF6-ROBOT))
)
:effect
(and
(Flag104prime-)
(not (Flag104-))
)

)
(:action Flag105Action-0
:parameters ()
:precondition
(and
(ABOVEOF8-ROBOT)
)
:effect
(and
(Flag105-)
(Flag105prime-)
)

)
(:action Flag105Action-1
:parameters ()
:precondition
(and
(ABOVEOF7-ROBOT)
)
:effect
(and
(Flag105-)
(Flag105prime-)
)

)
(:action Flag105Action-2
:parameters ()
:precondition
(and
(ABOVEOF9-ROBOT)
)
:effect
(and
(Flag105-)
(Flag105prime-)
)

)
(:action Prim105Action
:parameters ()
:precondition
(and
(not (ABOVEOF8-ROBOT))
(not (ABOVEOF7-ROBOT))
(not (ABOVEOF9-ROBOT))
)
:effect
(and
(Flag105prime-)
(not (Flag105-))
)

)
(:action Flag106Action-0
:parameters ()
:precondition
(and
(ABOVEOF8-ROBOT)
)
:effect
(and
(Flag106-)
(Flag106prime-)
)

)
(:action Flag106Action-1
:parameters ()
:precondition
(and
(ABOVEOF9-ROBOT)
)
:effect
(and
(Flag106-)
(Flag106prime-)
)

)
(:action Prim106Action
:parameters ()
:precondition
(and
(not (ABOVEOF8-ROBOT))
(not (ABOVEOF9-ROBOT))
)
:effect
(and
(Flag106prime-)
(not (Flag106-))
)

)
(:action Flag107Action
:parameters ()
:precondition
(and
(BELOWOF1-ROBOT)
(ABOVEOF2-ROBOT)
)
:effect
(and
(Flag107-)
(Flag107prime-)
)

)
(:action Flag108Action
:parameters ()
:precondition
(and
(ABOVEOF6-ROBOT)
(BELOWOF2-ROBOT)
)
:effect
(and
(Flag108-)
(Flag108prime-)
)

)
(:action Flag109Action
:parameters ()
:precondition
(and
(ABOVEOF2-ROBOT)
(BELOWOF2-ROBOT)
)
:effect
(and
(Flag109-)
(Flag109prime-)
)

)
(:action Flag110Action
:parameters ()
:precondition
(and
(BELOWOF1-ROBOT)
(ABOVEOF4-ROBOT)
)
:effect
(and
(Flag110-)
(Flag110prime-)
)

)
(:action Flag111Action
:parameters ()
:precondition
(and
(ABOVEOF9-ROBOT)
(BELOWOF2-ROBOT)
)
:effect
(and
(Flag111-)
(Flag111prime-)
)

)
(:action Flag112Action
:parameters ()
:precondition
(and
(BELOWOF1-ROBOT)
(ABOVEOF7-ROBOT)
)
:effect
(and
(Flag112-)
(Flag112prime-)
)

)
(:action Flag113Action
:parameters ()
:precondition
(and
(ABOVEOF6-ROBOT)
(BELOWOF1-ROBOT)
)
:effect
(and
(Flag113-)
(Flag113prime-)
)

)
(:action Flag114Action
:parameters ()
:precondition
(and
(BELOWOF1-ROBOT)
(ABOVEOF5-ROBOT)
)
:effect
(and
(Flag114-)
(Flag114prime-)
)

)
(:action Flag115Action
:parameters ()
:precondition
(and
(ABOVEOF4-ROBOT)
(BELOWOF2-ROBOT)
)
:effect
(and
(Flag115-)
(Flag115prime-)
)

)
(:action Flag116Action
:parameters ()
:precondition
(and
(BELOWOF2-ROBOT)
(ABOVEOF5-ROBOT)
)
:effect
(and
(Flag116-)
(Flag116prime-)
)

)
(:action Flag117Action
:parameters ()
:precondition
(and
(BELOWOF1-ROBOT)
(ABOVEOF1-ROBOT)
)
:effect
(and
(Flag117-)
(Flag117prime-)
)

)
(:action Flag118Action
:parameters ()
:precondition
(and
(ABOVEOF8-ROBOT)
(BELOWOF1-ROBOT)
)
:effect
(and
(Flag118-)
(Flag118prime-)
)

)
(:action Flag119Action
:parameters ()
:precondition
(and
(ABOVEOF3-ROBOT)
(BELOWOF1-ROBOT)
)
:effect
(and
(Flag119-)
(Flag119prime-)
)

)
(:action Flag120Action
:parameters ()
:precondition
(and
(ABOVEOF7-ROBOT)
(BELOWOF2-ROBOT)
)
:effect
(and
(Flag120-)
(Flag120prime-)
)

)
(:action Flag121Action
:parameters ()
:precondition
(and
(BELOWOF1-ROBOT)
(ABOVEOF9-ROBOT)
)
:effect
(and
(Flag121-)
(Flag121prime-)
)

)
(:action Flag122Action
:parameters ()
:precondition
(and
(ABOVEOF3-ROBOT)
(BELOWOF2-ROBOT)
)
:effect
(and
(Flag122-)
(Flag122prime-)
)

)
(:action Flag123Action
:parameters ()
:precondition
(and
(ABOVEOF8-ROBOT)
(BELOWOF2-ROBOT)
)
:effect
(and
(Flag123-)
(Flag123prime-)
)

)
(:action Flag124Action
:parameters ()
:precondition
(and
(ABOVEOF1-ROBOT)
(BELOWOF2-ROBOT)
)
:effect
(and
(Flag124-)
(Flag124prime-)
)

)
(:action Prim107Action-0
:parameters ()
:precondition
(and
(not (BELOWOF1-ROBOT))
)
:effect
(and
(Flag107prime-)
(not (Flag107-))
)

)
(:action Prim107Action-1
:parameters ()
:precondition
(and
(not (ABOVEOF2-ROBOT))
)
:effect
(and
(Flag107prime-)
(not (Flag107-))
)

)
(:action Prim108Action-0
:parameters ()
:precondition
(and
(not (ABOVEOF6-ROBOT))
)
:effect
(and
(Flag108prime-)
(not (Flag108-))
)

)
(:action Prim108Action-1
:parameters ()
:precondition
(and
(not (BELOWOF2-ROBOT))
)
:effect
(and
(Flag108prime-)
(not (Flag108-))
)

)
(:action Prim109Action-0
:parameters ()
:precondition
(and
(not (ABOVEOF2-ROBOT))
)
:effect
(and
(Flag109prime-)
(not (Flag109-))
)

)
(:action Prim109Action-1
:parameters ()
:precondition
(and
(not (BELOWOF2-ROBOT))
)
:effect
(and
(Flag109prime-)
(not (Flag109-))
)

)
(:action Prim110Action-0
:parameters ()
:precondition
(and
(not (BELOWOF1-ROBOT))
)
:effect
(and
(Flag110prime-)
(not (Flag110-))
)

)
(:action Prim110Action-1
:parameters ()
:precondition
(and
(not (ABOVEOF4-ROBOT))
)
:effect
(and
(Flag110prime-)
(not (Flag110-))
)

)
(:action Prim111Action-0
:parameters ()
:precondition
(and
(not (ABOVEOF9-ROBOT))
)
:effect
(and
(Flag111prime-)
(not (Flag111-))
)

)
(:action Prim111Action-1
:parameters ()
:precondition
(and
(not (BELOWOF2-ROBOT))
)
:effect
(and
(Flag111prime-)
(not (Flag111-))
)

)
(:action Prim112Action-0
:parameters ()
:precondition
(and
(not (BELOWOF1-ROBOT))
)
:effect
(and
(Flag112prime-)
(not (Flag112-))
)

)
(:action Prim112Action-1
:parameters ()
:precondition
(and
(not (ABOVEOF7-ROBOT))
)
:effect
(and
(Flag112prime-)
(not (Flag112-))
)

)
(:action Prim113Action-0
:parameters ()
:precondition
(and
(not (ABOVEOF6-ROBOT))
)
:effect
(and
(Flag113prime-)
(not (Flag113-))
)

)
(:action Prim113Action-1
:parameters ()
:precondition
(and
(not (BELOWOF1-ROBOT))
)
:effect
(and
(Flag113prime-)
(not (Flag113-))
)

)
(:action Prim114Action-0
:parameters ()
:precondition
(and
(not (BELOWOF1-ROBOT))
)
:effect
(and
(Flag114prime-)
(not (Flag114-))
)

)
(:action Prim114Action-1
:parameters ()
:precondition
(and
(not (ABOVEOF5-ROBOT))
)
:effect
(and
(Flag114prime-)
(not (Flag114-))
)

)
(:action Prim115Action-0
:parameters ()
:precondition
(and
(not (ABOVEOF4-ROBOT))
)
:effect
(and
(Flag115prime-)
(not (Flag115-))
)

)
(:action Prim115Action-1
:parameters ()
:precondition
(and
(not (BELOWOF2-ROBOT))
)
:effect
(and
(Flag115prime-)
(not (Flag115-))
)

)
(:action Prim116Action-0
:parameters ()
:precondition
(and
(not (BELOWOF2-ROBOT))
)
:effect
(and
(Flag116prime-)
(not (Flag116-))
)

)
(:action Prim116Action-1
:parameters ()
:precondition
(and
(not (ABOVEOF5-ROBOT))
)
:effect
(and
(Flag116prime-)
(not (Flag116-))
)

)
(:action Prim117Action-0
:parameters ()
:precondition
(and
(not (BELOWOF1-ROBOT))
)
:effect
(and
(Flag117prime-)
(not (Flag117-))
)

)
(:action Prim117Action-1
:parameters ()
:precondition
(and
(not (ABOVEOF1-ROBOT))
)
:effect
(and
(Flag117prime-)
(not (Flag117-))
)

)
(:action Prim118Action-0
:parameters ()
:precondition
(and
(not (ABOVEOF8-ROBOT))
)
:effect
(and
(Flag118prime-)
(not (Flag118-))
)

)
(:action Prim118Action-1
:parameters ()
:precondition
(and
(not (BELOWOF1-ROBOT))
)
:effect
(and
(Flag118prime-)
(not (Flag118-))
)

)
(:action Prim119Action-0
:parameters ()
:precondition
(and
(not (ABOVEOF3-ROBOT))
)
:effect
(and
(Flag119prime-)
(not (Flag119-))
)

)
(:action Prim119Action-1
:parameters ()
:precondition
(and
(not (BELOWOF1-ROBOT))
)
:effect
(and
(Flag119prime-)
(not (Flag119-))
)

)
(:action Prim120Action-0
:parameters ()
:precondition
(and
(not (ABOVEOF7-ROBOT))
)
:effect
(and
(Flag120prime-)
(not (Flag120-))
)

)
(:action Prim120Action-1
:parameters ()
:precondition
(and
(not (BELOWOF2-ROBOT))
)
:effect
(and
(Flag120prime-)
(not (Flag120-))
)

)
(:action Prim121Action-0
:parameters ()
:precondition
(and
(not (BELOWOF1-ROBOT))
)
:effect
(and
(Flag121prime-)
(not (Flag121-))
)

)
(:action Prim121Action-1
:parameters ()
:precondition
(and
(not (ABOVEOF9-ROBOT))
)
:effect
(and
(Flag121prime-)
(not (Flag121-))
)

)
(:action Prim122Action-0
:parameters ()
:precondition
(and
(not (ABOVEOF3-ROBOT))
)
:effect
(and
(Flag122prime-)
(not (Flag122-))
)

)
(:action Prim122Action-1
:parameters ()
:precondition
(and
(not (BELOWOF2-ROBOT))
)
:effect
(and
(Flag122prime-)
(not (Flag122-))
)

)
(:action Prim123Action-0
:parameters ()
:precondition
(and
(not (ABOVEOF8-ROBOT))
)
:effect
(and
(Flag123prime-)
(not (Flag123-))
)

)
(:action Prim123Action-1
:parameters ()
:precondition
(and
(not (BELOWOF2-ROBOT))
)
:effect
(and
(Flag123prime-)
(not (Flag123-))
)

)
(:action Prim124Action-0
:parameters ()
:precondition
(and
(not (ABOVEOF1-ROBOT))
)
:effect
(and
(Flag124prime-)
(not (Flag124-))
)

)
(:action Prim124Action-1
:parameters ()
:precondition
(and
(not (BELOWOF2-ROBOT))
)
:effect
(and
(Flag124prime-)
(not (Flag124-))
)

)
(:action Flag126Action
:parameters ()
:precondition
(and
(BELOWOF3-ROBOT)
(ABOVEOF5-ROBOT)
)
:effect
(and
(Flag126-)
(Flag126prime-)
)

)
(:action Flag127Action
:parameters ()
:precondition
(and
(BELOWOF3-ROBOT)
(ABOVEOF6-ROBOT)
)
:effect
(and
(Flag127-)
(Flag127prime-)
)

)
(:action Flag128Action
:parameters ()
:precondition
(and
(BELOWOF3-ROBOT)
(ABOVEOF7-ROBOT)
)
:effect
(and
(Flag128-)
(Flag128prime-)
)

)
(:action Flag129Action
:parameters ()
:precondition
(and
(BELOWOF3-ROBOT)
(ABOVEOF9-ROBOT)
)
:effect
(and
(Flag129-)
(Flag129prime-)
)

)
(:action Flag130Action
:parameters ()
:precondition
(and
(BELOWOF3-ROBOT)
(ABOVEOF2-ROBOT)
)
:effect
(and
(Flag130-)
(Flag130prime-)
)

)
(:action Flag131Action
:parameters ()
:precondition
(and
(ABOVEOF8-ROBOT)
(BELOWOF3-ROBOT)
)
:effect
(and
(Flag131-)
(Flag131prime-)
)

)
(:action Flag132Action
:parameters ()
:precondition
(and
(BELOWOF3-ROBOT)
(ABOVEOF4-ROBOT)
)
:effect
(and
(Flag132-)
(Flag132prime-)
)

)
(:action Flag133Action
:parameters ()
:precondition
(and
(BELOWOF2-ROBOT)
(ABOVEOF2-ROBOT)
)
:effect
(and
(Flag133-)
(Flag133prime-)
)

)
(:action Flag134Action
:parameters ()
:precondition
(and
(ABOVEOF3-ROBOT)
(BELOWOF3-ROBOT)
)
:effect
(and
(Flag134-)
(Flag134prime-)
)

)
(:action Prim126Action-0
:parameters ()
:precondition
(and
(not (BELOWOF3-ROBOT))
)
:effect
(and
(Flag126prime-)
(not (Flag126-))
)

)
(:action Prim126Action-1
:parameters ()
:precondition
(and
(not (ABOVEOF5-ROBOT))
)
:effect
(and
(Flag126prime-)
(not (Flag126-))
)

)
(:action Prim127Action-0
:parameters ()
:precondition
(and
(not (BELOWOF3-ROBOT))
)
:effect
(and
(Flag127prime-)
(not (Flag127-))
)

)
(:action Prim127Action-1
:parameters ()
:precondition
(and
(not (ABOVEOF6-ROBOT))
)
:effect
(and
(Flag127prime-)
(not (Flag127-))
)

)
(:action Prim128Action-0
:parameters ()
:precondition
(and
(not (BELOWOF3-ROBOT))
)
:effect
(and
(Flag128prime-)
(not (Flag128-))
)

)
(:action Prim128Action-1
:parameters ()
:precondition
(and
(not (ABOVEOF7-ROBOT))
)
:effect
(and
(Flag128prime-)
(not (Flag128-))
)

)
(:action Prim129Action-0
:parameters ()
:precondition
(and
(not (BELOWOF3-ROBOT))
)
:effect
(and
(Flag129prime-)
(not (Flag129-))
)

)
(:action Prim129Action-1
:parameters ()
:precondition
(and
(not (ABOVEOF9-ROBOT))
)
:effect
(and
(Flag129prime-)
(not (Flag129-))
)

)
(:action Prim130Action-0
:parameters ()
:precondition
(and
(not (BELOWOF3-ROBOT))
)
:effect
(and
(Flag130prime-)
(not (Flag130-))
)

)
(:action Prim130Action-1
:parameters ()
:precondition
(and
(not (ABOVEOF2-ROBOT))
)
:effect
(and
(Flag130prime-)
(not (Flag130-))
)

)
(:action Prim131Action-0
:parameters ()
:precondition
(and
(not (ABOVEOF8-ROBOT))
)
:effect
(and
(Flag131prime-)
(not (Flag131-))
)

)
(:action Prim131Action-1
:parameters ()
:precondition
(and
(not (BELOWOF3-ROBOT))
)
:effect
(and
(Flag131prime-)
(not (Flag131-))
)

)
(:action Prim132Action-0
:parameters ()
:precondition
(and
(not (BELOWOF3-ROBOT))
)
:effect
(and
(Flag132prime-)
(not (Flag132-))
)

)
(:action Prim132Action-1
:parameters ()
:precondition
(and
(not (ABOVEOF4-ROBOT))
)
:effect
(and
(Flag132prime-)
(not (Flag132-))
)

)
(:action Prim133Action-0
:parameters ()
:precondition
(and
(not (BELOWOF2-ROBOT))
)
:effect
(and
(Flag133prime-)
(not (Flag133-))
)

)
(:action Prim133Action-1
:parameters ()
:precondition
(and
(not (ABOVEOF2-ROBOT))
)
:effect
(and
(Flag133prime-)
(not (Flag133-))
)

)
(:action Prim134Action-0
:parameters ()
:precondition
(and
(not (ABOVEOF3-ROBOT))
)
:effect
(and
(Flag134prime-)
(not (Flag134-))
)

)
(:action Prim134Action-1
:parameters ()
:precondition
(and
(not (BELOWOF3-ROBOT))
)
:effect
(and
(Flag134prime-)
(not (Flag134-))
)

)
(:action Flag136Action
:parameters ()
:precondition
(and
(ABOVEOF3-ROBOT)
(BELOWOF4-ROBOT)
)
:effect
(and
(Flag136-)
(Flag136prime-)
)

)
(:action Flag137Action
:parameters ()
:precondition
(and
(ABOVEOF6-ROBOT)
(BELOWOF4-ROBOT)
)
:effect
(and
(Flag137-)
(Flag137prime-)
)

)
(:action Flag138Action
:parameters ()
:precondition
(and
(ABOVEOF9-ROBOT)
(BELOWOF4-ROBOT)
)
:effect
(and
(Flag138-)
(Flag138prime-)
)

)
(:action Flag139Action
:parameters ()
:precondition
(and
(BELOWOF4-ROBOT)
(ABOVEOF4-ROBOT)
)
:effect
(and
(Flag139-)
(Flag139prime-)
)

)
(:action Flag140Action
:parameters ()
:precondition
(and
(ABOVEOF8-ROBOT)
(BELOWOF4-ROBOT)
)
:effect
(and
(Flag140-)
(Flag140prime-)
)

)
(:action Flag141Action
:parameters ()
:precondition
(and
(BELOWOF4-ROBOT)
(ABOVEOF7-ROBOT)
)
:effect
(and
(Flag141-)
(Flag141prime-)
)

)
(:action Flag142Action
:parameters ()
:precondition
(and
(BELOWOF4-ROBOT)
(ABOVEOF5-ROBOT)
)
:effect
(and
(Flag142-)
(Flag142prime-)
)

)
(:action Flag143Action
:parameters ()
:precondition
(and
(BELOWOF3-ROBOT)
(ABOVEOF3-ROBOT)
)
:effect
(and
(Flag143-)
(Flag143prime-)
)

)
(:action Prim136Action-0
:parameters ()
:precondition
(and
(not (ABOVEOF3-ROBOT))
)
:effect
(and
(Flag136prime-)
(not (Flag136-))
)

)
(:action Prim136Action-1
:parameters ()
:precondition
(and
(not (BELOWOF4-ROBOT))
)
:effect
(and
(Flag136prime-)
(not (Flag136-))
)

)
(:action Prim137Action-0
:parameters ()
:precondition
(and
(not (ABOVEOF6-ROBOT))
)
:effect
(and
(Flag137prime-)
(not (Flag137-))
)

)
(:action Prim137Action-1
:parameters ()
:precondition
(and
(not (BELOWOF4-ROBOT))
)
:effect
(and
(Flag137prime-)
(not (Flag137-))
)

)
(:action Prim138Action-0
:parameters ()
:precondition
(and
(not (ABOVEOF9-ROBOT))
)
:effect
(and
(Flag138prime-)
(not (Flag138-))
)

)
(:action Prim138Action-1
:parameters ()
:precondition
(and
(not (BELOWOF4-ROBOT))
)
:effect
(and
(Flag138prime-)
(not (Flag138-))
)

)
(:action Prim139Action-0
:parameters ()
:precondition
(and
(not (BELOWOF4-ROBOT))
)
:effect
(and
(Flag139prime-)
(not (Flag139-))
)

)
(:action Prim139Action-1
:parameters ()
:precondition
(and
(not (ABOVEOF4-ROBOT))
)
:effect
(and
(Flag139prime-)
(not (Flag139-))
)

)
(:action Prim140Action-0
:parameters ()
:precondition
(and
(not (ABOVEOF8-ROBOT))
)
:effect
(and
(Flag140prime-)
(not (Flag140-))
)

)
(:action Prim140Action-1
:parameters ()
:precondition
(and
(not (BELOWOF4-ROBOT))
)
:effect
(and
(Flag140prime-)
(not (Flag140-))
)

)
(:action Prim141Action-0
:parameters ()
:precondition
(and
(not (BELOWOF4-ROBOT))
)
:effect
(and
(Flag141prime-)
(not (Flag141-))
)

)
(:action Prim141Action-1
:parameters ()
:precondition
(and
(not (ABOVEOF7-ROBOT))
)
:effect
(and
(Flag141prime-)
(not (Flag141-))
)

)
(:action Prim142Action-0
:parameters ()
:precondition
(and
(not (BELOWOF4-ROBOT))
)
:effect
(and
(Flag142prime-)
(not (Flag142-))
)

)
(:action Prim142Action-1
:parameters ()
:precondition
(and
(not (ABOVEOF5-ROBOT))
)
:effect
(and
(Flag142prime-)
(not (Flag142-))
)

)
(:action Prim143Action-0
:parameters ()
:precondition
(and
(not (BELOWOF3-ROBOT))
)
:effect
(and
(Flag143prime-)
(not (Flag143-))
)

)
(:action Prim143Action-1
:parameters ()
:precondition
(and
(not (ABOVEOF3-ROBOT))
)
:effect
(and
(Flag143prime-)
(not (Flag143-))
)

)
(:action Flag145Action
:parameters ()
:precondition
(and
(ABOVEOF4-ROBOT)
(BELOWOF5-ROBOT)
)
:effect
(and
(Flag145-)
(Flag145prime-)
)

)
(:action Flag146Action
:parameters ()
:precondition
(and
(ABOVEOF8-ROBOT)
(BELOWOF5-ROBOT)
)
:effect
(and
(Flag146-)
(Flag146prime-)
)

)
(:action Flag147Action
:parameters ()
:precondition
(and
(ABOVEOF5-ROBOT)
(BELOWOF5-ROBOT)
)
:effect
(and
(Flag147-)
(Flag147prime-)
)

)
(:action Flag148Action
:parameters ()
:precondition
(and
(ABOVEOF7-ROBOT)
(BELOWOF5-ROBOT)
)
:effect
(and
(Flag148-)
(Flag148prime-)
)

)
(:action Flag149Action
:parameters ()
:precondition
(and
(ABOVEOF9-ROBOT)
(BELOWOF5-ROBOT)
)
:effect
(and
(Flag149-)
(Flag149prime-)
)

)
(:action Flag150Action
:parameters ()
:precondition
(and
(ABOVEOF6-ROBOT)
(BELOWOF5-ROBOT)
)
:effect
(and
(Flag150-)
(Flag150prime-)
)

)
(:action Prim145Action-0
:parameters ()
:precondition
(and
(not (ABOVEOF4-ROBOT))
)
:effect
(and
(Flag145prime-)
(not (Flag145-))
)

)
(:action Prim145Action-1
:parameters ()
:precondition
(and
(not (BELOWOF5-ROBOT))
)
:effect
(and
(Flag145prime-)
(not (Flag145-))
)

)
(:action Prim146Action-0
:parameters ()
:precondition
(and
(not (ABOVEOF8-ROBOT))
)
:effect
(and
(Flag146prime-)
(not (Flag146-))
)

)
(:action Prim146Action-1
:parameters ()
:precondition
(and
(not (BELOWOF5-ROBOT))
)
:effect
(and
(Flag146prime-)
(not (Flag146-))
)

)
(:action Prim147Action-0
:parameters ()
:precondition
(and
(not (ABOVEOF5-ROBOT))
)
:effect
(and
(Flag147prime-)
(not (Flag147-))
)

)
(:action Prim147Action-1
:parameters ()
:precondition
(and
(not (BELOWOF5-ROBOT))
)
:effect
(and
(Flag147prime-)
(not (Flag147-))
)

)
(:action Prim148Action-0
:parameters ()
:precondition
(and
(not (ABOVEOF7-ROBOT))
)
:effect
(and
(Flag148prime-)
(not (Flag148-))
)

)
(:action Prim148Action-1
:parameters ()
:precondition
(and
(not (BELOWOF5-ROBOT))
)
:effect
(and
(Flag148prime-)
(not (Flag148-))
)

)
(:action Prim149Action-0
:parameters ()
:precondition
(and
(not (ABOVEOF9-ROBOT))
)
:effect
(and
(Flag149prime-)
(not (Flag149-))
)

)
(:action Prim149Action-1
:parameters ()
:precondition
(and
(not (BELOWOF5-ROBOT))
)
:effect
(and
(Flag149prime-)
(not (Flag149-))
)

)
(:action Prim150Action-0
:parameters ()
:precondition
(and
(not (ABOVEOF6-ROBOT))
)
:effect
(and
(Flag150prime-)
(not (Flag150-))
)

)
(:action Prim150Action-1
:parameters ()
:precondition
(and
(not (BELOWOF5-ROBOT))
)
:effect
(and
(Flag150prime-)
(not (Flag150-))
)

)
(:action Flag152Action
:parameters ()
:precondition
(and
(BELOWOF6-ROBOT)
(ABOVEOF5-ROBOT)
)
:effect
(and
(Flag152-)
(Flag152prime-)
)

)
(:action Flag153Action
:parameters ()
:precondition
(and
(BELOWOF6-ROBOT)
(ABOVEOF7-ROBOT)
)
:effect
(and
(Flag153-)
(Flag153prime-)
)

)
(:action Flag154Action
:parameters ()
:precondition
(and
(BELOWOF5-ROBOT)
(ABOVEOF5-ROBOT)
)
:effect
(and
(Flag154-)
(Flag154prime-)
)

)
(:action Flag155Action
:parameters ()
:precondition
(and
(BELOWOF6-ROBOT)
(ABOVEOF6-ROBOT)
)
:effect
(and
(Flag155-)
(Flag155prime-)
)

)
(:action Flag156Action
:parameters ()
:precondition
(and
(ABOVEOF8-ROBOT)
(BELOWOF6-ROBOT)
)
:effect
(and
(Flag156-)
(Flag156prime-)
)

)
(:action Flag157Action
:parameters ()
:precondition
(and
(BELOWOF6-ROBOT)
(ABOVEOF9-ROBOT)
)
:effect
(and
(Flag157-)
(Flag157prime-)
)

)
(:action Prim152Action-0
:parameters ()
:precondition
(and
(not (BELOWOF6-ROBOT))
)
:effect
(and
(Flag152prime-)
(not (Flag152-))
)

)
(:action Prim152Action-1
:parameters ()
:precondition
(and
(not (ABOVEOF5-ROBOT))
)
:effect
(and
(Flag152prime-)
(not (Flag152-))
)

)
(:action Prim153Action-0
:parameters ()
:precondition
(and
(not (BELOWOF6-ROBOT))
)
:effect
(and
(Flag153prime-)
(not (Flag153-))
)

)
(:action Prim153Action-1
:parameters ()
:precondition
(and
(not (ABOVEOF7-ROBOT))
)
:effect
(and
(Flag153prime-)
(not (Flag153-))
)

)
(:action Prim154Action-0
:parameters ()
:precondition
(and
(not (BELOWOF5-ROBOT))
)
:effect
(and
(Flag154prime-)
(not (Flag154-))
)

)
(:action Prim154Action-1
:parameters ()
:precondition
(and
(not (ABOVEOF5-ROBOT))
)
:effect
(and
(Flag154prime-)
(not (Flag154-))
)

)
(:action Prim155Action-0
:parameters ()
:precondition
(and
(not (BELOWOF6-ROBOT))
)
:effect
(and
(Flag155prime-)
(not (Flag155-))
)

)
(:action Prim155Action-1
:parameters ()
:precondition
(and
(not (ABOVEOF6-ROBOT))
)
:effect
(and
(Flag155prime-)
(not (Flag155-))
)

)
(:action Prim156Action-0
:parameters ()
:precondition
(and
(not (ABOVEOF8-ROBOT))
)
:effect
(and
(Flag156prime-)
(not (Flag156-))
)

)
(:action Prim156Action-1
:parameters ()
:precondition
(and
(not (BELOWOF6-ROBOT))
)
:effect
(and
(Flag156prime-)
(not (Flag156-))
)

)
(:action Prim157Action-0
:parameters ()
:precondition
(and
(not (BELOWOF6-ROBOT))
)
:effect
(and
(Flag157prime-)
(not (Flag157-))
)

)
(:action Prim157Action-1
:parameters ()
:precondition
(and
(not (ABOVEOF9-ROBOT))
)
:effect
(and
(Flag157prime-)
(not (Flag157-))
)

)
(:action Flag159Action
:parameters ()
:precondition
(and
(ABOVEOF6-ROBOT)
(BELOWOF7-ROBOT)
)
:effect
(and
(Flag159-)
(Flag159prime-)
)

)
(:action Flag160Action
:parameters ()
:precondition
(and
(ABOVEOF9-ROBOT)
(BELOWOF7-ROBOT)
)
:effect
(and
(Flag160-)
(Flag160prime-)
)

)
(:action Flag161Action
:parameters ()
:precondition
(and
(ABOVEOF6-ROBOT)
(BELOWOF6-ROBOT)
)
:effect
(and
(Flag161-)
(Flag161prime-)
)

)
(:action Flag162Action
:parameters ()
:precondition
(and
(ABOVEOF7-ROBOT)
(BELOWOF7-ROBOT)
)
:effect
(and
(Flag162-)
(Flag162prime-)
)

)
(:action Flag163Action
:parameters ()
:precondition
(and
(ABOVEOF8-ROBOT)
(BELOWOF7-ROBOT)
)
:effect
(and
(Flag163-)
(Flag163prime-)
)

)
(:action Prim159Action-0
:parameters ()
:precondition
(and
(not (ABOVEOF6-ROBOT))
)
:effect
(and
(Flag159prime-)
(not (Flag159-))
)

)
(:action Prim159Action-1
:parameters ()
:precondition
(and
(not (BELOWOF7-ROBOT))
)
:effect
(and
(Flag159prime-)
(not (Flag159-))
)

)
(:action Prim160Action-0
:parameters ()
:precondition
(and
(not (ABOVEOF9-ROBOT))
)
:effect
(and
(Flag160prime-)
(not (Flag160-))
)

)
(:action Prim160Action-1
:parameters ()
:precondition
(and
(not (BELOWOF7-ROBOT))
)
:effect
(and
(Flag160prime-)
(not (Flag160-))
)

)
(:action Prim161Action-0
:parameters ()
:precondition
(and
(not (ABOVEOF6-ROBOT))
)
:effect
(and
(Flag161prime-)
(not (Flag161-))
)

)
(:action Prim161Action-1
:parameters ()
:precondition
(and
(not (BELOWOF6-ROBOT))
)
:effect
(and
(Flag161prime-)
(not (Flag161-))
)

)
(:action Prim162Action-0
:parameters ()
:precondition
(and
(not (ABOVEOF7-ROBOT))
)
:effect
(and
(Flag162prime-)
(not (Flag162-))
)

)
(:action Prim162Action-1
:parameters ()
:precondition
(and
(not (BELOWOF7-ROBOT))
)
:effect
(and
(Flag162prime-)
(not (Flag162-))
)

)
(:action Prim163Action-0
:parameters ()
:precondition
(and
(not (ABOVEOF8-ROBOT))
)
:effect
(and
(Flag163prime-)
(not (Flag163-))
)

)
(:action Prim163Action-1
:parameters ()
:precondition
(and
(not (BELOWOF7-ROBOT))
)
:effect
(and
(Flag163prime-)
(not (Flag163-))
)

)
(:action Flag165Action
:parameters ()
:precondition
(and
(ABOVEOF8-ROBOT)
(BELOWOF8-ROBOT)
)
:effect
(and
(Flag165-)
(Flag165prime-)
)

)
(:action Flag166Action
:parameters ()
:precondition
(and
(BELOWOF8-ROBOT)
(ABOVEOF9-ROBOT)
)
:effect
(and
(Flag166-)
(Flag166prime-)
)

)
(:action Flag167Action
:parameters ()
:precondition
(and
(BELOWOF7-ROBOT)
(ABOVEOF7-ROBOT)
)
:effect
(and
(Flag167-)
(Flag167prime-)
)

)
(:action Flag168Action
:parameters ()
:precondition
(and
(BELOWOF8-ROBOT)
(ABOVEOF7-ROBOT)
)
:effect
(and
(Flag168-)
(Flag168prime-)
)

)
(:action Prim165Action-0
:parameters ()
:precondition
(and
(not (ABOVEOF8-ROBOT))
)
:effect
(and
(Flag165prime-)
(not (Flag165-))
)

)
(:action Prim165Action-1
:parameters ()
:precondition
(and
(not (BELOWOF8-ROBOT))
)
:effect
(and
(Flag165prime-)
(not (Flag165-))
)

)
(:action Prim166Action-0
:parameters ()
:precondition
(and
(not (BELOWOF8-ROBOT))
)
:effect
(and
(Flag166prime-)
(not (Flag166-))
)

)
(:action Prim166Action-1
:parameters ()
:precondition
(and
(not (ABOVEOF9-ROBOT))
)
:effect
(and
(Flag166prime-)
(not (Flag166-))
)

)
(:action Prim167Action-0
:parameters ()
:precondition
(and
(not (BELOWOF7-ROBOT))
)
:effect
(and
(Flag167prime-)
(not (Flag167-))
)

)
(:action Prim167Action-1
:parameters ()
:precondition
(and
(not (ABOVEOF7-ROBOT))
)
:effect
(and
(Flag167prime-)
(not (Flag167-))
)

)
(:action Prim168Action-0
:parameters ()
:precondition
(and
(not (BELOWOF8-ROBOT))
)
:effect
(and
(Flag168prime-)
(not (Flag168-))
)

)
(:action Prim168Action-1
:parameters ()
:precondition
(and
(not (ABOVEOF7-ROBOT))
)
:effect
(and
(Flag168prime-)
(not (Flag168-))
)

)
(:action Flag170Action
:parameters ()
:precondition
(and
(BELOWOF8-ROBOT)
(ABOVEOF8-ROBOT)
)
:effect
(and
(Flag170-)
(Flag170prime-)
)

)
(:action Flag171Action
:parameters ()
:precondition
(and
(ABOVEOF8-ROBOT)
(BELOWOF9-ROBOT)
)
:effect
(and
(Flag171-)
(Flag171prime-)
)

)
(:action Flag172Action
:parameters ()
:precondition
(and
(BELOWOF9-ROBOT)
(ABOVEOF9-ROBOT)
)
:effect
(and
(Flag172-)
(Flag172prime-)
)

)
(:action Prim170Action-0
:parameters ()
:precondition
(and
(not (BELOWOF8-ROBOT))
)
:effect
(and
(Flag170prime-)
(not (Flag170-))
)

)
(:action Prim170Action-1
:parameters ()
:precondition
(and
(not (ABOVEOF8-ROBOT))
)
:effect
(and
(Flag170prime-)
(not (Flag170-))
)

)
(:action Prim171Action-0
:parameters ()
:precondition
(and
(not (ABOVEOF8-ROBOT))
)
:effect
(and
(Flag171prime-)
(not (Flag171-))
)

)
(:action Prim172Action-1
:parameters ()
:precondition
(and
(not (ABOVEOF9-ROBOT))
)
:effect
(and
(Flag172prime-)
(not (Flag172-))
)

)
(:action Flag174Action
:parameters ()
:precondition
(and
(ABOVEOF9-ROBOT)
(BELOWOF9-ROBOT)
)
:effect
(and
(Flag174-)
(Flag174prime-)
)

)
(:action Flag175Action
:parameters ()
:precondition
(and
(BELOWOF10-ROBOT)
(ABOVEOF9-ROBOT)
)
:effect
(and
(Flag175-)
(Flag175prime-)
)

)
(:action Prim174Action-0
:parameters ()
:precondition
(and
(not (ABOVEOF9-ROBOT))
)
:effect
(and
(Flag174prime-)
(not (Flag174-))
)

)
(:action Prim175Action-0
:parameters ()
:precondition
(and
(not (BELOWOF10-ROBOT))
)
:effect
(and
(Flag175prime-)
(not (Flag175-))
)

)
(:action Prim175Action-1
:parameters ()
:precondition
(and
(not (ABOVEOF9-ROBOT))
)
:effect
(and
(Flag175prime-)
(not (Flag175-))
)

)
(:action Flag180Action-6
:parameters ()
:precondition
(and
(BELOWOF1-ROBOT)
)
:effect
(and
(Flag180-)
(Flag180prime-)
)

)
(:action MOVEUP-ROBOT
:parameters ()
:precondition
(and
(Flag2-)
(Flag2prime-)
(Flag173prime-)
(Flag169prime-)
(Flag164prime-)
(Flag158prime-)
(Flag151prime-)
(Flag144prime-)
(Flag135prime-)
(Flag125prime-)
(Flag180prime-)
)
:effect
(and
(when
(and
(Flag173-)
)
(and
(ROW9-ROBOT)
(not (NOT-ROW9-ROBOT))
)
)
(when
(and
(Flag169-)
)
(and
(ROW8-ROBOT)
(not (NOT-ROW8-ROBOT))
)
)
(when
(and
(Flag164-)
)
(and
(ROW7-ROBOT)
(not (NOT-ROW7-ROBOT))
)
)
(when
(and
(Flag158-)
)
(and
(ROW6-ROBOT)
(not (NOT-ROW6-ROBOT))
)
)
(when
(and
(Flag151-)
)
(and
(ROW5-ROBOT)
(not (NOT-ROW5-ROBOT))
)
)
(when
(and
(Flag144-)
)
(and
(ROW4-ROBOT)
(not (NOT-ROW4-ROBOT))
)
)
(when
(and
(Flag135-)
)
(and
(ROW3-ROBOT)
(not (NOT-ROW3-ROBOT))
)
)
(when
(and
(Flag125-)
)
(and
(ROW2-ROBOT)
(not (NOT-ROW2-ROBOT))
)
)
(when
(and
(Flag180-)
)
(and
(ROW1-ROBOT)
(not (NOT-ROW1-ROBOT))
)
)
(when
(and
(ROW8-ROBOT)
)
(and
(ROW9-ROBOT)
(NOT-ROW8-ROBOT)
(not (NOT-ROW9-ROBOT))
(not (ROW8-ROBOT))
)
)
(when
(and
(ROW7-ROBOT)
)
(and
(ROW8-ROBOT)
(NOT-ROW7-ROBOT)
(not (NOT-ROW8-ROBOT))
(not (ROW7-ROBOT))
)
)
(when
(and
(ROW6-ROBOT)
)
(and
(ROW7-ROBOT)
(NOT-ROW6-ROBOT)
(not (NOT-ROW7-ROBOT))
(not (ROW6-ROBOT))
)
)
(when
(and
(ROW5-ROBOT)
)
(and
(ROW6-ROBOT)
(NOT-ROW5-ROBOT)
(not (NOT-ROW6-ROBOT))
(not (ROW5-ROBOT))
)
)
(when
(and
(ROW4-ROBOT)
)
(and
(ROW5-ROBOT)
(NOT-ROW4-ROBOT)
(not (NOT-ROW5-ROBOT))
(not (ROW4-ROBOT))
)
)
(when
(and
(ROW3-ROBOT)
)
(and
(ROW4-ROBOT)
(NOT-ROW3-ROBOT)
(not (NOT-ROW4-ROBOT))
(not (ROW3-ROBOT))
)
)
(when
(and
(ROW2-ROBOT)
)
(and
(ROW3-ROBOT)
(NOT-ROW2-ROBOT)
(not (NOT-ROW3-ROBOT))
(not (ROW2-ROBOT))
)
)
(when
(and
(ROW1-ROBOT)
)
(and
(ROW2-ROBOT)
(NOT-ROW1-ROBOT)
(not (NOT-ROW2-ROBOT))
(not (ROW1-ROBOT))
)
)
(when
(and
(ROW0-ROBOT)
)
(and
(ROW1-ROBOT)
(NOT-ROW0-ROBOT)
(not (NOT-ROW1-ROBOT))
(not (ROW0-ROBOT))
)
)
(when
(and
(BELOWOF8-ROBOT)
)
(and
(BELOWOF10-ROBOT)
(NOT-BELOWOF9-ROBOT)
(BELOWOF9-ROBOT)
(NOT-BELOWOF8-ROBOT)
(not (NOT-BELOWOF10-ROBOT))
(not (BELOWOF8-ROBOT))
)
)
(when
(and
(BELOWOF3-ROBOT)
)
(and
(BELOWOF10-ROBOT)
(NOT-BELOWOF9-ROBOT)
(BELOWOF9-ROBOT)
(NOT-BELOWOF8-ROBOT)
(BELOWOF8-ROBOT)
(NOT-BELOWOF7-ROBOT)
(BELOWOF7-ROBOT)
(NOT-BELOWOF6-ROBOT)
(BELOWOF6-ROBOT)
(NOT-BELOWOF5-ROBOT)
(BELOWOF5-ROBOT)
(NOT-BELOWOF4-ROBOT)
(BELOWOF4-ROBOT)
(NOT-BELOWOF3-ROBOT)
(not (NOT-BELOWOF10-ROBOT))
(not (BELOWOF3-ROBOT))
)
)
(when
(and
(BELOWOF9-ROBOT)
)
(and
(BELOWOF10-ROBOT)
(NOT-BELOWOF9-ROBOT)
(not (NOT-BELOWOF10-ROBOT))
(not (BELOWOF9-ROBOT))
)
)
(when
(and
(BELOWOF7-ROBOT)
)
(and
(BELOWOF10-ROBOT)
(NOT-BELOWOF9-ROBOT)
(BELOWOF9-ROBOT)
(NOT-BELOWOF8-ROBOT)
(BELOWOF8-ROBOT)
(NOT-BELOWOF7-ROBOT)
(not (NOT-BELOWOF10-ROBOT))
(not (BELOWOF7-ROBOT))
)
)
(when
(and
(BELOWOF4-ROBOT)
)
(and
(BELOWOF10-ROBOT)
(NOT-BELOWOF9-ROBOT)
(BELOWOF9-ROBOT)
(NOT-BELOWOF8-ROBOT)
(BELOWOF8-ROBOT)
(NOT-BELOWOF7-ROBOT)
(BELOWOF7-ROBOT)
(NOT-BELOWOF6-ROBOT)
(BELOWOF6-ROBOT)
(NOT-BELOWOF5-ROBOT)
(BELOWOF5-ROBOT)
(NOT-BELOWOF4-ROBOT)
(not (NOT-BELOWOF10-ROBOT))
(not (BELOWOF4-ROBOT))
)
)
(when
(and
(BELOWOF1-ROBOT)
)
(and
(BELOWOF10-ROBOT)
(NOT-BELOWOF9-ROBOT)
(BELOWOF9-ROBOT)
(NOT-BELOWOF8-ROBOT)
(BELOWOF8-ROBOT)
(NOT-BELOWOF7-ROBOT)
(BELOWOF7-ROBOT)
(NOT-BELOWOF6-ROBOT)
(BELOWOF6-ROBOT)
(NOT-BELOWOF5-ROBOT)
(BELOWOF5-ROBOT)
(NOT-BELOWOF4-ROBOT)
(BELOWOF4-ROBOT)
(NOT-BELOWOF3-ROBOT)
(BELOWOF3-ROBOT)
(NOT-BELOWOF2-ROBOT)
(BELOWOF2-ROBOT)
(NOT-BELOWOF1-ROBOT)
(not (NOT-BELOWOF10-ROBOT))
(not (BELOWOF1-ROBOT))
)
)
(when
(and
(BELOWOF2-ROBOT)
)
(and
(BELOWOF10-ROBOT)
(NOT-BELOWOF9-ROBOT)
(BELOWOF9-ROBOT)
(NOT-BELOWOF8-ROBOT)
(BELOWOF8-ROBOT)
(NOT-BELOWOF7-ROBOT)
(BELOWOF7-ROBOT)
(NOT-BELOWOF6-ROBOT)
(BELOWOF6-ROBOT)
(NOT-BELOWOF5-ROBOT)
(BELOWOF5-ROBOT)
(NOT-BELOWOF4-ROBOT)
(BELOWOF4-ROBOT)
(NOT-BELOWOF3-ROBOT)
(BELOWOF3-ROBOT)
(NOT-BELOWOF2-ROBOT)
(not (NOT-BELOWOF10-ROBOT))
(not (BELOWOF2-ROBOT))
)
)
(when
(and
(BELOWOF5-ROBOT)
)
(and
(BELOWOF10-ROBOT)
(NOT-BELOWOF9-ROBOT)
(BELOWOF9-ROBOT)
(NOT-BELOWOF8-ROBOT)
(BELOWOF8-ROBOT)
(NOT-BELOWOF7-ROBOT)
(BELOWOF7-ROBOT)
(NOT-BELOWOF6-ROBOT)
(BELOWOF6-ROBOT)
(NOT-BELOWOF5-ROBOT)
(not (NOT-BELOWOF10-ROBOT))
(not (BELOWOF5-ROBOT))
)
)
(when
(and
(BELOWOF6-ROBOT)
)
(and
(BELOWOF10-ROBOT)
(NOT-BELOWOF9-ROBOT)
(BELOWOF9-ROBOT)
(NOT-BELOWOF8-ROBOT)
(BELOWOF8-ROBOT)
(NOT-BELOWOF7-ROBOT)
(BELOWOF7-ROBOT)
(NOT-BELOWOF6-ROBOT)
(not (NOT-BELOWOF10-ROBOT))
(not (BELOWOF6-ROBOT))
)
)
(when
(and
(ABOVEOF9-ROBOT)
)
(and
(ABOVEOF8-ROBOT)
(ABOVEOF7-ROBOT)
(ABOVEOF6-ROBOT)
(ABOVEOF5-ROBOT)
(ABOVEOF4-ROBOT)
(ABOVEOF3-ROBOT)
(ABOVEOF2-ROBOT)
(not (NOT-ABOVEOF9-ROBOT))
(not (NOT-ABOVEOF8-ROBOT))
(not (NOT-ABOVEOF7-ROBOT))
(not (NOT-ABOVEOF6-ROBOT))
(not (NOT-ABOVEOF5-ROBOT))
(not (NOT-ABOVEOF4-ROBOT))
(not (NOT-ABOVEOF3-ROBOT))
(not (NOT-ABOVEOF2-ROBOT))
)
)
(when
(and
(ABOVEOF8-ROBOT)
)
(and
(ABOVEOF9-ROBOT)
(ABOVEOF7-ROBOT)
(ABOVEOF6-ROBOT)
(ABOVEOF5-ROBOT)
(ABOVEOF4-ROBOT)
(ABOVEOF3-ROBOT)
(ABOVEOF2-ROBOT)
(not (NOT-ABOVEOF9-ROBOT))
(not (NOT-ABOVEOF8-ROBOT))
(not (NOT-ABOVEOF7-ROBOT))
(not (NOT-ABOVEOF6-ROBOT))
(not (NOT-ABOVEOF5-ROBOT))
(not (NOT-ABOVEOF4-ROBOT))
(not (NOT-ABOVEOF3-ROBOT))
(not (NOT-ABOVEOF2-ROBOT))
)
)
(when
(and
(ABOVEOF7-ROBOT)
)
(and
(ABOVEOF8-ROBOT)
(ABOVEOF6-ROBOT)
(ABOVEOF5-ROBOT)
(ABOVEOF4-ROBOT)
(ABOVEOF3-ROBOT)
(ABOVEOF2-ROBOT)
(not (NOT-ABOVEOF8-ROBOT))
(not (NOT-ABOVEOF7-ROBOT))
(not (NOT-ABOVEOF6-ROBOT))
(not (NOT-ABOVEOF5-ROBOT))
(not (NOT-ABOVEOF4-ROBOT))
(not (NOT-ABOVEOF3-ROBOT))
(not (NOT-ABOVEOF2-ROBOT))
)
)
(when
(and
(ABOVEOF6-ROBOT)
)
(and
(ABOVEOF7-ROBOT)
(ABOVEOF5-ROBOT)
(ABOVEOF4-ROBOT)
(ABOVEOF3-ROBOT)
(ABOVEOF2-ROBOT)
(not (NOT-ABOVEOF7-ROBOT))
(not (NOT-ABOVEOF6-ROBOT))
(not (NOT-ABOVEOF5-ROBOT))
(not (NOT-ABOVEOF4-ROBOT))
(not (NOT-ABOVEOF3-ROBOT))
(not (NOT-ABOVEOF2-ROBOT))
)
)
(when
(and
(ABOVEOF5-ROBOT)
)
(and
(ABOVEOF6-ROBOT)
(ABOVEOF4-ROBOT)
(ABOVEOF3-ROBOT)
(ABOVEOF2-ROBOT)
(not (NOT-ABOVEOF6-ROBOT))
(not (NOT-ABOVEOF5-ROBOT))
(not (NOT-ABOVEOF4-ROBOT))
(not (NOT-ABOVEOF3-ROBOT))
(not (NOT-ABOVEOF2-ROBOT))
)
)
(when
(and
(ABOVEOF4-ROBOT)
)
(and
(ABOVEOF5-ROBOT)
(ABOVEOF3-ROBOT)
(ABOVEOF2-ROBOT)
(not (NOT-ABOVEOF5-ROBOT))
(not (NOT-ABOVEOF4-ROBOT))
(not (NOT-ABOVEOF3-ROBOT))
(not (NOT-ABOVEOF2-ROBOT))
)
)
(when
(and
(ABOVEOF3-ROBOT)
)
(and
(ABOVEOF4-ROBOT)
(ABOVEOF2-ROBOT)
(not (NOT-ABOVEOF4-ROBOT))
(not (NOT-ABOVEOF3-ROBOT))
(not (NOT-ABOVEOF2-ROBOT))
)
)
(when
(and
(ABOVEOF2-ROBOT)
)
(and
(ABOVEOF3-ROBOT)
(not (NOT-ABOVEOF3-ROBOT))
(not (NOT-ABOVEOF2-ROBOT))
)
)
(when
(and
(ABOVEOF1-ROBOT)
)
(and
(ABOVEOF2-ROBOT)
(not (NOT-ABOVEOF2-ROBOT))
)
)
(ABOVEOF1-ROBOT)
(CHECKCONSISTENCY-)
(not (NOT-ABOVEOF1-ROBOT))
(not (NOT-CHECKCONSISTENCY-))
(not (Flag177prime-))
(not (Flag98prime-))
(not (Flag97prime-))
(not (Flag21prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag180prime-))
(not (Flag175prime-))
(not (Flag174prime-))
(not (Flag172prime-))
(not (Flag171prime-))
(not (Flag170prime-))
(not (Flag168prime-))
(not (Flag167prime-))
(not (Flag166prime-))
(not (Flag165prime-))
(not (Flag163prime-))
(not (Flag162prime-))
(not (Flag161prime-))
(not (Flag160prime-))
(not (Flag159prime-))
(not (Flag157prime-))
(not (Flag156prime-))
(not (Flag155prime-))
(not (Flag154prime-))
(not (Flag153prime-))
(not (Flag152prime-))
(not (Flag150prime-))
(not (Flag149prime-))
(not (Flag148prime-))
(not (Flag147prime-))
(not (Flag146prime-))
(not (Flag145prime-))
(not (Flag143prime-))
(not (Flag142prime-))
(not (Flag141prime-))
(not (Flag140prime-))
(not (Flag139prime-))
(not (Flag138prime-))
(not (Flag137prime-))
(not (Flag136prime-))
(not (Flag134prime-))
(not (Flag133prime-))
(not (Flag132prime-))
(not (Flag131prime-))
(not (Flag130prime-))
(not (Flag129prime-))
(not (Flag128prime-))
(not (Flag127prime-))
(not (Flag126prime-))
(not (Flag124prime-))
(not (Flag123prime-))
(not (Flag122prime-))
(not (Flag121prime-))
(not (Flag120prime-))
(not (Flag119prime-))
(not (Flag118prime-))
(not (Flag117prime-))
(not (Flag116prime-))
(not (Flag115prime-))
(not (Flag114prime-))
(not (Flag113prime-))
(not (Flag112prime-))
(not (Flag111prime-))
(not (Flag110prime-))
(not (Flag109prime-))
(not (Flag108prime-))
(not (Flag107prime-))
(not (Flag106prime-))
(not (Flag105prime-))
(not (Flag104prime-))
(not (Flag103prime-))
(not (Flag102prime-))
(not (Flag101prime-))
(not (Flag100prime-))
(not (Flag99prime-))
(not (Flag96prime-))
(not (Flag95prime-))
(not (Flag94prime-))
(not (Flag93prime-))
(not (Flag92prime-))
(not (Flag91prime-))
(not (Flag90prime-))
(not (Flag4prime-))
(not (Flag178prime-))
(not (Flag176prime-))
(not (Flag173prime-))
(not (Flag169prime-))
(not (Flag164prime-))
(not (Flag158prime-))
(not (Flag151prime-))
(not (Flag144prime-))
(not (Flag135prime-))
(not (Flag125prime-))
(not (Flag88prime-))
(not (Flag87prime-))
(not (Flag85prime-))
(not (Flag84prime-))
(not (Flag83prime-))
(not (Flag82prime-))
(not (Flag80prime-))
(not (Flag79prime-))
(not (Flag78prime-))
(not (Flag77prime-))
(not (Flag76prime-))
(not (Flag74prime-))
(not (Flag73prime-))
(not (Flag72prime-))
(not (Flag71prime-))
(not (Flag70prime-))
(not (Flag68prime-))
(not (Flag67prime-))
(not (Flag66prime-))
(not (Flag65prime-))
(not (Flag64prime-))
(not (Flag63prime-))
(not (Flag62prime-))
(not (Flag60prime-))
(not (Flag59prime-))
(not (Flag58prime-))
(not (Flag57prime-))
(not (Flag56prime-))
(not (Flag55prime-))
(not (Flag54prime-))
(not (Flag53prime-))
(not (Flag51prime-))
(not (Flag50prime-))
(not (Flag49prime-))
(not (Flag48prime-))
(not (Flag47prime-))
(not (Flag46prime-))
(not (Flag45prime-))
(not (Flag44prime-))
(not (Flag42prime-))
(not (Flag41prime-))
(not (Flag40prime-))
(not (Flag39prime-))
(not (Flag38prime-))
(not (Flag37prime-))
(not (Flag36prime-))
(not (Flag35prime-))
(not (Flag34prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag30prime-))
(not (Flag29prime-))
(not (Flag28prime-))
(not (Flag27prime-))
(not (Flag26prime-))
(not (Flag25prime-))
(not (Flag24prime-))
(not (Flag23prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag19prime-))
(not (Flag18prime-))
(not (Flag17prime-))
(not (Flag16prime-))
(not (Flag15prime-))
(not (Flag14prime-))
(not (Flag13prime-))
(not (Flag12prime-))
(not (Flag11prime-))
(not (Flag10prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag1prime-))
(not (Flag179prime-))
(not (Flag89prime-))
(not (Flag86prime-))
(not (Flag81prime-))
(not (Flag75prime-))
(not (Flag69prime-))
(not (Flag61prime-))
(not (Flag52prime-))
(not (Flag43prime-))
(not (Flag32prime-))
)
)
(:action MOVEDOWN-ROBOT
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(Flag176prime-)
(Flag173prime-)
(Flag169prime-)
(Flag164prime-)
(Flag158prime-)
(Flag151prime-)
(Flag144prime-)
(Flag135prime-)
(Flag125prime-)
(Flag106prime-)
(Flag105prime-)
(Flag104prime-)
(Flag103prime-)
(Flag102prime-)
(Flag101prime-)
(Flag100prime-)
(Flag99prime-)
(Flag98prime-)
(Flag97prime-)
(Flag96prime-)
(Flag95prime-)
(Flag94prime-)
(Flag93prime-)
(Flag92prime-)
(Flag91prime-)
(Flag90prime-)
)
:effect
(and
(when
(and
(Flag176-)
)
(and
(ROW8-ROBOT)
(not (NOT-ROW8-ROBOT))
)
)
(when
(and
(Flag173-)
)
(and
(ROW7-ROBOT)
(not (NOT-ROW7-ROBOT))
)
)
(when
(and
(Flag169-)
)
(and
(ROW6-ROBOT)
(not (NOT-ROW6-ROBOT))
)
)
(when
(and
(Flag164-)
)
(and
(ROW5-ROBOT)
(not (NOT-ROW5-ROBOT))
)
)
(when
(and
(Flag158-)
)
(and
(ROW4-ROBOT)
(not (NOT-ROW4-ROBOT))
)
)
(when
(and
(Flag151-)
)
(and
(ROW3-ROBOT)
(not (NOT-ROW3-ROBOT))
)
)
(when
(and
(Flag144-)
)
(and
(ROW2-ROBOT)
(not (NOT-ROW2-ROBOT))
)
)
(when
(and
(Flag135-)
)
(and
(ROW1-ROBOT)
(not (NOT-ROW1-ROBOT))
)
)
(when
(and
(Flag125-)
)
(and
(ROW0-ROBOT)
(not (NOT-ROW0-ROBOT))
)
)
(when
(and
(ROW9-ROBOT)
)
(and
(ROW8-ROBOT)
(NOT-ROW9-ROBOT)
(not (NOT-ROW8-ROBOT))
(not (ROW9-ROBOT))
)
)
(when
(and
(ROW8-ROBOT)
)
(and
(ROW7-ROBOT)
(NOT-ROW8-ROBOT)
(not (NOT-ROW7-ROBOT))
(not (ROW8-ROBOT))
)
)
(when
(and
(ROW7-ROBOT)
)
(and
(ROW6-ROBOT)
(NOT-ROW7-ROBOT)
(not (NOT-ROW6-ROBOT))
(not (ROW7-ROBOT))
)
)
(when
(and
(ROW6-ROBOT)
)
(and
(ROW5-ROBOT)
(NOT-ROW6-ROBOT)
(not (NOT-ROW5-ROBOT))
(not (ROW6-ROBOT))
)
)
(when
(and
(ROW5-ROBOT)
)
(and
(ROW4-ROBOT)
(NOT-ROW5-ROBOT)
(not (NOT-ROW4-ROBOT))
(not (ROW5-ROBOT))
)
)
(when
(and
(ROW4-ROBOT)
)
(and
(ROW3-ROBOT)
(NOT-ROW4-ROBOT)
(not (NOT-ROW3-ROBOT))
(not (ROW4-ROBOT))
)
)
(when
(and
(ROW3-ROBOT)
)
(and
(ROW2-ROBOT)
(NOT-ROW3-ROBOT)
(not (NOT-ROW2-ROBOT))
(not (ROW3-ROBOT))
)
)
(when
(and
(ROW2-ROBOT)
)
(and
(ROW1-ROBOT)
(NOT-ROW2-ROBOT)
(not (NOT-ROW1-ROBOT))
(not (ROW2-ROBOT))
)
)
(when
(and
(ROW1-ROBOT)
)
(and
(ROW0-ROBOT)
(NOT-ROW1-ROBOT)
(not (NOT-ROW0-ROBOT))
(not (ROW1-ROBOT))
)
)
(when
(and
(ABOVEOF9-ROBOT)
)
(and
(ABOVEOF8-ROBOT)
(NOT-ABOVEOF9-ROBOT)
(not (NOT-ABOVEOF8-ROBOT))
(not (ABOVEOF9-ROBOT))
)
)
(when
(and
(Flag106-)
)
(and
(ABOVEOF7-ROBOT)
(NOT-ABOVEOF8-ROBOT)
(not (NOT-ABOVEOF7-ROBOT))
(not (ABOVEOF8-ROBOT))
)
)
(when
(and
(Flag105-)
)
(and
(ABOVEOF6-ROBOT)
(NOT-ABOVEOF7-ROBOT)
(not (NOT-ABOVEOF6-ROBOT))
(not (ABOVEOF7-ROBOT))
)
)
(when
(and
(Flag104-)
)
(and
(ABOVEOF5-ROBOT)
(NOT-ABOVEOF6-ROBOT)
(not (NOT-ABOVEOF5-ROBOT))
(not (ABOVEOF6-ROBOT))
)
)
(when
(and
(Flag103-)
)
(and
(ABOVEOF4-ROBOT)
(NOT-ABOVEOF5-ROBOT)
(not (NOT-ABOVEOF4-ROBOT))
(not (ABOVEOF5-ROBOT))
)
)
(when
(and
(Flag102-)
)
(and
(ABOVEOF3-ROBOT)
(NOT-ABOVEOF4-ROBOT)
(not (NOT-ABOVEOF3-ROBOT))
(not (ABOVEOF4-ROBOT))
)
)
(when
(and
(Flag101-)
)
(and
(ABOVEOF2-ROBOT)
(NOT-ABOVEOF3-ROBOT)
(not (NOT-ABOVEOF2-ROBOT))
(not (ABOVEOF3-ROBOT))
)
)
(when
(and
(Flag100-)
)
(and
(ABOVEOF1-ROBOT)
(NOT-ABOVEOF2-ROBOT)
(not (NOT-ABOVEOF1-ROBOT))
(not (ABOVEOF2-ROBOT))
)
)
(when
(and
(Flag99-)
)
(and
(NOT-ABOVEOF1-ROBOT)
(not (ABOVEOF1-ROBOT))
)
)
(when
(and
(Flag98-)
)
(and
(BELOWOF9-ROBOT)
(not (NOT-BELOWOF9-ROBOT))
)
)
(when
(and
(Flag97-)
)
(and
(BELOWOF8-ROBOT)
(not (NOT-BELOWOF8-ROBOT))
)
)
(when
(and
(Flag96-)
)
(and
(BELOWOF7-ROBOT)
(not (NOT-BELOWOF7-ROBOT))
)
)
(when
(and
(Flag95-)
)
(and
(BELOWOF6-ROBOT)
(not (NOT-BELOWOF6-ROBOT))
)
)
(when
(and
(Flag94-)
)
(and
(BELOWOF5-ROBOT)
(not (NOT-BELOWOF5-ROBOT))
)
)
(when
(and
(Flag93-)
)
(and
(BELOWOF4-ROBOT)
(not (NOT-BELOWOF4-ROBOT))
)
)
(when
(and
(Flag92-)
)
(and
(BELOWOF3-ROBOT)
(not (NOT-BELOWOF3-ROBOT))
)
)
(when
(and
(Flag91-)
)
(and
(BELOWOF2-ROBOT)
(not (NOT-BELOWOF2-ROBOT))
)
)
(when
(and
(Flag90-)
)
(and
(BELOWOF1-ROBOT)
(not (NOT-BELOWOF1-ROBOT))
)
)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag177prime-))
(not (Flag98prime-))
(not (Flag97prime-))
(not (Flag21prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag180prime-))
(not (Flag175prime-))
(not (Flag174prime-))
(not (Flag172prime-))
(not (Flag171prime-))
(not (Flag170prime-))
(not (Flag168prime-))
(not (Flag167prime-))
(not (Flag166prime-))
(not (Flag165prime-))
(not (Flag163prime-))
(not (Flag162prime-))
(not (Flag161prime-))
(not (Flag160prime-))
(not (Flag159prime-))
(not (Flag157prime-))
(not (Flag156prime-))
(not (Flag155prime-))
(not (Flag154prime-))
(not (Flag153prime-))
(not (Flag152prime-))
(not (Flag150prime-))
(not (Flag149prime-))
(not (Flag148prime-))
(not (Flag147prime-))
(not (Flag146prime-))
(not (Flag145prime-))
(not (Flag143prime-))
(not (Flag142prime-))
(not (Flag141prime-))
(not (Flag140prime-))
(not (Flag139prime-))
(not (Flag138prime-))
(not (Flag137prime-))
(not (Flag136prime-))
(not (Flag134prime-))
(not (Flag133prime-))
(not (Flag132prime-))
(not (Flag131prime-))
(not (Flag130prime-))
(not (Flag129prime-))
(not (Flag128prime-))
(not (Flag127prime-))
(not (Flag126prime-))
(not (Flag124prime-))
(not (Flag123prime-))
(not (Flag122prime-))
(not (Flag121prime-))
(not (Flag120prime-))
(not (Flag119prime-))
(not (Flag118prime-))
(not (Flag117prime-))
(not (Flag116prime-))
(not (Flag115prime-))
(not (Flag114prime-))
(not (Flag113prime-))
(not (Flag112prime-))
(not (Flag111prime-))
(not (Flag110prime-))
(not (Flag109prime-))
(not (Flag108prime-))
(not (Flag107prime-))
(not (Flag106prime-))
(not (Flag105prime-))
(not (Flag104prime-))
(not (Flag103prime-))
(not (Flag102prime-))
(not (Flag101prime-))
(not (Flag100prime-))
(not (Flag99prime-))
(not (Flag96prime-))
(not (Flag95prime-))
(not (Flag94prime-))
(not (Flag93prime-))
(not (Flag92prime-))
(not (Flag91prime-))
(not (Flag90prime-))
(not (Flag4prime-))
(not (Flag178prime-))
(not (Flag176prime-))
(not (Flag173prime-))
(not (Flag169prime-))
(not (Flag164prime-))
(not (Flag158prime-))
(not (Flag151prime-))
(not (Flag144prime-))
(not (Flag135prime-))
(not (Flag125prime-))
(not (Flag88prime-))
(not (Flag87prime-))
(not (Flag85prime-))
(not (Flag84prime-))
(not (Flag83prime-))
(not (Flag82prime-))
(not (Flag80prime-))
(not (Flag79prime-))
(not (Flag78prime-))
(not (Flag77prime-))
(not (Flag76prime-))
(not (Flag74prime-))
(not (Flag73prime-))
(not (Flag72prime-))
(not (Flag71prime-))
(not (Flag70prime-))
(not (Flag68prime-))
(not (Flag67prime-))
(not (Flag66prime-))
(not (Flag65prime-))
(not (Flag64prime-))
(not (Flag63prime-))
(not (Flag62prime-))
(not (Flag60prime-))
(not (Flag59prime-))
(not (Flag58prime-))
(not (Flag57prime-))
(not (Flag56prime-))
(not (Flag55prime-))
(not (Flag54prime-))
(not (Flag53prime-))
(not (Flag51prime-))
(not (Flag50prime-))
(not (Flag49prime-))
(not (Flag48prime-))
(not (Flag47prime-))
(not (Flag46prime-))
(not (Flag45prime-))
(not (Flag44prime-))
(not (Flag42prime-))
(not (Flag41prime-))
(not (Flag40prime-))
(not (Flag39prime-))
(not (Flag38prime-))
(not (Flag37prime-))
(not (Flag36prime-))
(not (Flag35prime-))
(not (Flag34prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag30prime-))
(not (Flag29prime-))
(not (Flag28prime-))
(not (Flag27prime-))
(not (Flag26prime-))
(not (Flag25prime-))
(not (Flag24prime-))
(not (Flag23prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag19prime-))
(not (Flag18prime-))
(not (Flag17prime-))
(not (Flag16prime-))
(not (Flag15prime-))
(not (Flag14prime-))
(not (Flag13prime-))
(not (Flag12prime-))
(not (Flag11prime-))
(not (Flag10prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag1prime-))
(not (Flag179prime-))
(not (Flag89prime-))
(not (Flag86prime-))
(not (Flag81prime-))
(not (Flag75prime-))
(not (Flag69prime-))
(not (Flag61prime-))
(not (Flag52prime-))
(not (Flag43prime-))
(not (Flag32prime-))
)
)
(:action Prim1Action-2
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
(Flag1prime-)
(not (Flag1-))
)

)
(:action Prim1Action-3
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
(Flag1prime-)
(not (Flag1-))
)

)
(:action Flag2Action
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
)
:effect
(and
(Flag2-)
(Flag2prime-)
)

)
(:action Prim2Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
(Flag2prime-)
(not (Flag2-))
)

)
(:action Prim2Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
(Flag2prime-)
(not (Flag2-))
)

)
(:action Flag3Action-13
:parameters ()
:precondition
(and
(LEFTOF9-ROBOT)
)
:effect
(and
(Flag3-)
(Flag3prime-)
)

)
(:action Flag3Action-14
:parameters ()
:precondition
(and
(RIGHTOF2-ROBOT)
)
:effect
(and
(Flag3-)
(Flag3prime-)
)

)
(:action Prim4Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
(Flag4prime-)
(not (Flag4-))
)

)
(:action Prim4Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
(Flag4prime-)
(not (Flag4-))
)

)
(:action Flag5Action-3
:parameters ()
:precondition
(and
(RIGHTOF2-ROBOT)
)
:effect
(and
(Flag5-)
(Flag5prime-)
)

)
(:action Flag6Action-2
:parameters ()
:precondition
(and
(RIGHTOF2-ROBOT)
)
:effect
(and
(Flag6-)
(Flag6prime-)
)

)
(:action Flag7Action-2
:parameters ()
:precondition
(and
(RIGHTOF2-ROBOT)
)
:effect
(and
(Flag7-)
(Flag7prime-)
)

)
(:action Flag21Action-0
:parameters ()
:precondition
(and
(LEFTOF9-ROBOT)
)
:effect
(and
(Flag21-)
(Flag21prime-)
)

)
(:action Prim29Action-1
:parameters ()
:precondition
(and
(not (RIGHTOF2-ROBOT))
)
:effect
(and
(Flag29prime-)
(not (Flag29-))
)

)
(:action Prim39Action-0
:parameters ()
:precondition
(and
(not (RIGHTOF2-ROBOT))
)
:effect
(and
(Flag39prime-)
(not (Flag39-))
)

)
(:action Prim51Action-1
:parameters ()
:precondition
(and
(not (RIGHTOF2-ROBOT))
)
:effect
(and
(Flag51prime-)
(not (Flag51-))
)

)
(:action Prim87Action-0
:parameters ()
:precondition
(and
(not (LEFTOF9-ROBOT))
)
:effect
(and
(Flag87prime-)
(not (Flag87-))
)

)
(:action Prim88Action-1
:parameters ()
:precondition
(and
(not (LEFTOF9-ROBOT))
)
:effect
(and
(Flag88prime-)
(not (Flag88-))
)

)
(:action Flag97Action-6
:parameters ()
:precondition
(and
(BELOWOF9-ROBOT)
)
:effect
(and
(Flag97-)
(Flag97prime-)
)

)
(:action Flag98Action-7
:parameters ()
:precondition
(and
(BELOWOF9-ROBOT)
)
:effect
(and
(Flag98-)
(Flag98prime-)
)

)
(:action Prim171Action-1
:parameters ()
:precondition
(and
(not (BELOWOF9-ROBOT))
)
:effect
(and
(Flag171prime-)
(not (Flag171-))
)

)
(:action Prim172Action-0
:parameters ()
:precondition
(and
(not (BELOWOF9-ROBOT))
)
:effect
(and
(Flag172prime-)
(not (Flag172-))
)

)
(:action Prim174Action-1
:parameters ()
:precondition
(and
(not (BELOWOF9-ROBOT))
)
:effect
(and
(Flag174prime-)
(not (Flag174-))
)

)
(:action Flag177Action-1
:parameters ()
:precondition
(and
(LEFTOF9-ROBOT)
)
:effect
(and
(Flag177-)
(Flag177prime-)
)

)
)
