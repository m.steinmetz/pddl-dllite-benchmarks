(define (domain grounded-SIMPLE-ADL-ROBOT)
(:requirements
:strips
)
(:predicates
(Flag213-)
(Flag212-)
(Flag195-)
(Flag33-)
(Flag7-)
(Flag6-)
(Flag5-)
(Flag3-)
(Flag2-)
(ROW14-ROBOT)
(ROW13-ROBOT)
(ROW12-ROBOT)
(ROW11-ROBOT)
(ROW10-ROBOT)
(ROW9-ROBOT)
(ROW8-ROBOT)
(ROW7-ROBOT)
(ROW6-ROBOT)
(ROW5-ROBOT)
(ROW4-ROBOT)
(ROW3-ROBOT)
(ROW2-ROBOT)
(ROW1-ROBOT)
(ROW0-ROBOT)
(ABOVEOF14-ROBOT)
(ABOVEOF13-ROBOT)
(ABOVEOF12-ROBOT)
(ABOVEOF11-ROBOT)
(ABOVEOF10-ROBOT)
(ABOVEOF9-ROBOT)
(ABOVEOF8-ROBOT)
(ABOVEOF7-ROBOT)
(ABOVEOF6-ROBOT)
(ABOVEOF5-ROBOT)
(ABOVEOF4-ROBOT)
(ABOVEOF3-ROBOT)
(ABOVEOF2-ROBOT)
(ABOVEOF1-ROBOT)
(BELOWOF14-ROBOT)
(BELOWOF13-ROBOT)
(BELOWOF12-ROBOT)
(BELOWOF11-ROBOT)
(BELOWOF10-ROBOT)
(BELOWOF9-ROBOT)
(BELOWOF8-ROBOT)
(BELOWOF7-ROBOT)
(BELOWOF6-ROBOT)
(BELOWOF5-ROBOT)
(BELOWOF4-ROBOT)
(BELOWOF3-ROBOT)
(BELOWOF2-ROBOT)
(BELOWOF1-ROBOT)
(CHECKCONSISTENCY-)
(ROW15-ROBOT)
(BELOWOF16-ROBOT)
(NOT-BELOWOF15-ROBOT)
(ABOVEOF15-ROBOT)
(Flag391-)
(Flag389-)
(Flag387-)
(Flag386-)
(Flag384-)
(Flag383-)
(Flag382-)
(Flag381-)
(Flag379-)
(Flag378-)
(Flag377-)
(Flag376-)
(Flag375-)
(Flag373-)
(Flag372-)
(Flag371-)
(Flag370-)
(Flag369-)
(Flag367-)
(Flag366-)
(Flag365-)
(Flag364-)
(Flag363-)
(Flag362-)
(Flag360-)
(Flag359-)
(Flag358-)
(Flag357-)
(Flag356-)
(Flag355-)
(Flag354-)
(Flag353-)
(Flag351-)
(Flag350-)
(Flag349-)
(Flag348-)
(Flag347-)
(Flag346-)
(Flag345-)
(Flag344-)
(Flag343-)
(Flag342-)
(Flag340-)
(Flag339-)
(Flag338-)
(Flag337-)
(Flag336-)
(Flag335-)
(Flag334-)
(Flag333-)
(Flag332-)
(Flag331-)
(Flag329-)
(Flag328-)
(Flag327-)
(Flag326-)
(Flag325-)
(Flag324-)
(Flag323-)
(Flag322-)
(Flag321-)
(Flag320-)
(Flag319-)
(Flag318-)
(Flag316-)
(Flag315-)
(Flag314-)
(Flag313-)
(Flag312-)
(Flag311-)
(Flag310-)
(Flag309-)
(Flag308-)
(Flag307-)
(Flag306-)
(Flag305-)
(Flag304-)
(Flag302-)
(Flag301-)
(Flag300-)
(Flag299-)
(Flag298-)
(Flag297-)
(Flag296-)
(Flag295-)
(Flag294-)
(Flag293-)
(Flag292-)
(Flag291-)
(Flag289-)
(Flag288-)
(Flag287-)
(Flag286-)
(Flag285-)
(Flag284-)
(Flag283-)
(Flag282-)
(Flag281-)
(Flag280-)
(Flag279-)
(Flag278-)
(Flag277-)
(Flag276-)
(Flag275-)
(Flag273-)
(Flag272-)
(Flag271-)
(Flag270-)
(Flag269-)
(Flag268-)
(Flag267-)
(Flag266-)
(Flag265-)
(Flag264-)
(Flag263-)
(Flag262-)
(Flag261-)
(Flag260-)
(Flag259-)
(Flag257-)
(Flag256-)
(Flag255-)
(Flag254-)
(Flag253-)
(Flag252-)
(Flag251-)
(Flag250-)
(Flag249-)
(Flag248-)
(Flag247-)
(Flag246-)
(Flag245-)
(Flag244-)
(Flag243-)
(Flag242-)
(Flag241-)
(Flag240-)
(Flag239-)
(Flag238-)
(Flag237-)
(Flag236-)
(Flag235-)
(Flag234-)
(Flag233-)
(Flag232-)
(Flag231-)
(Flag230-)
(Flag229-)
(Flag228-)
(Flag227-)
(Flag226-)
(Flag225-)
(Flag224-)
(Flag223-)
(Flag222-)
(Flag221-)
(Flag220-)
(Flag219-)
(Flag218-)
(Flag217-)
(Flag216-)
(Flag215-)
(Flag214-)
(Flag211-)
(Flag210-)
(Flag209-)
(Flag208-)
(Flag207-)
(Flag206-)
(Flag205-)
(Flag204-)
(Flag203-)
(Flag202-)
(Flag201-)
(Flag200-)
(Flag199-)
(Flag4-)
(ERROR-)
(COLUMN15-ROBOT)
(COLUMN14-ROBOT)
(COLUMN13-ROBOT)
(COLUMN12-ROBOT)
(COLUMN11-ROBOT)
(COLUMN10-ROBOT)
(COLUMN9-ROBOT)
(COLUMN8-ROBOT)
(COLUMN7-ROBOT)
(COLUMN6-ROBOT)
(COLUMN5-ROBOT)
(COLUMN4-ROBOT)
(COLUMN3-ROBOT)
(COLUMN2-ROBOT)
(COLUMN1-ROBOT)
(LEFTOF16-ROBOT)
(NOT-LEFTOF15-ROBOT)
(LEFTOF14-ROBOT)
(LEFTOF13-ROBOT)
(LEFTOF12-ROBOT)
(LEFTOF11-ROBOT)
(LEFTOF10-ROBOT)
(LEFTOF9-ROBOT)
(LEFTOF8-ROBOT)
(LEFTOF7-ROBOT)
(LEFTOF6-ROBOT)
(LEFTOF5-ROBOT)
(LEFTOF4-ROBOT)
(LEFTOF3-ROBOT)
(LEFTOF2-ROBOT)
(RIGHTOF15-ROBOT)
(RIGHTOF14-ROBOT)
(RIGHTOF13-ROBOT)
(RIGHTOF12-ROBOT)
(RIGHTOF11-ROBOT)
(RIGHTOF10-ROBOT)
(RIGHTOF9-ROBOT)
(RIGHTOF8-ROBOT)
(RIGHTOF7-ROBOT)
(RIGHTOF6-ROBOT)
(RIGHTOF5-ROBOT)
(RIGHTOF4-ROBOT)
(RIGHTOF3-ROBOT)
(RIGHTOF1-ROBOT)
(COLUMN0-ROBOT)
(NOT-RIGHTOF2-ROBOT)
(RIGHTOF0-ROBOT)
(LEFTOF1-ROBOT)
(Flag390-)
(Flag388-)
(Flag385-)
(Flag380-)
(Flag374-)
(Flag368-)
(Flag361-)
(Flag352-)
(Flag341-)
(Flag330-)
(Flag317-)
(Flag303-)
(Flag290-)
(Flag274-)
(Flag258-)
(Flag197-)
(Flag196-)
(Flag193-)
(Flag192-)
(Flag190-)
(Flag189-)
(Flag188-)
(Flag186-)
(Flag185-)
(Flag184-)
(Flag183-)
(Flag182-)
(Flag180-)
(Flag179-)
(Flag178-)
(Flag177-)
(Flag176-)
(Flag174-)
(Flag173-)
(Flag172-)
(Flag171-)
(Flag170-)
(Flag169-)
(Flag168-)
(Flag166-)
(Flag165-)
(Flag164-)
(Flag163-)
(Flag162-)
(Flag161-)
(Flag160-)
(Flag158-)
(Flag157-)
(Flag156-)
(Flag155-)
(Flag154-)
(Flag153-)
(Flag152-)
(Flag151-)
(Flag150-)
(Flag148-)
(Flag147-)
(Flag146-)
(Flag145-)
(Flag144-)
(Flag143-)
(Flag142-)
(Flag141-)
(Flag140-)
(Flag139-)
(Flag137-)
(Flag136-)
(Flag135-)
(Flag134-)
(Flag133-)
(Flag132-)
(Flag131-)
(Flag130-)
(Flag129-)
(Flag128-)
(Flag127-)
(Flag126-)
(Flag124-)
(Flag123-)
(Flag122-)
(Flag121-)
(Flag120-)
(Flag119-)
(Flag118-)
(Flag117-)
(Flag116-)
(Flag115-)
(Flag114-)
(Flag112-)
(Flag111-)
(Flag110-)
(Flag109-)
(Flag108-)
(Flag107-)
(Flag106-)
(Flag105-)
(Flag104-)
(Flag103-)
(Flag102-)
(Flag101-)
(Flag100-)
(Flag99-)
(Flag97-)
(Flag96-)
(Flag95-)
(Flag94-)
(Flag93-)
(Flag92-)
(Flag91-)
(Flag90-)
(Flag89-)
(Flag88-)
(Flag87-)
(Flag86-)
(Flag85-)
(Flag84-)
(Flag83-)
(Flag81-)
(Flag80-)
(Flag79-)
(Flag78-)
(Flag77-)
(Flag76-)
(Flag75-)
(Flag74-)
(Flag73-)
(Flag72-)
(Flag71-)
(Flag70-)
(Flag69-)
(Flag68-)
(Flag66-)
(Flag65-)
(Flag64-)
(Flag63-)
(Flag62-)
(Flag61-)
(Flag60-)
(Flag59-)
(Flag58-)
(Flag57-)
(Flag56-)
(Flag55-)
(Flag54-)
(Flag53-)
(Flag52-)
(Flag51-)
(Flag49-)
(Flag48-)
(Flag47-)
(Flag46-)
(Flag45-)
(Flag44-)
(Flag43-)
(Flag42-)
(Flag41-)
(Flag40-)
(Flag39-)
(Flag38-)
(Flag37-)
(Flag36-)
(Flag35-)
(Flag34-)
(Flag32-)
(Flag31-)
(Flag30-)
(Flag29-)
(Flag28-)
(Flag27-)
(Flag26-)
(Flag25-)
(Flag24-)
(Flag23-)
(Flag22-)
(Flag21-)
(Flag20-)
(Flag19-)
(Flag18-)
(Flag17-)
(Flag16-)
(Flag15-)
(Flag14-)
(Flag13-)
(Flag12-)
(Flag11-)
(Flag10-)
(Flag9-)
(Flag8-)
(Flag1-)
(Flag198-)
(Flag194-)
(Flag191-)
(Flag187-)
(Flag181-)
(Flag175-)
(Flag167-)
(Flag159-)
(Flag149-)
(Flag138-)
(Flag125-)
(Flag113-)
(Flag98-)
(Flag82-)
(Flag67-)
(Flag50-)
(NOT-COLUMN14-ROBOT)
(NOT-COLUMN13-ROBOT)
(NOT-COLUMN12-ROBOT)
(NOT-COLUMN11-ROBOT)
(NOT-COLUMN10-ROBOT)
(NOT-COLUMN9-ROBOT)
(NOT-COLUMN8-ROBOT)
(NOT-COLUMN7-ROBOT)
(NOT-COLUMN6-ROBOT)
(NOT-COLUMN5-ROBOT)
(NOT-COLUMN4-ROBOT)
(NOT-COLUMN3-ROBOT)
(NOT-COLUMN2-ROBOT)
(NOT-COLUMN1-ROBOT)
(NOT-COLUMN0-ROBOT)
(NOT-RIGHTOF14-ROBOT)
(NOT-RIGHTOF13-ROBOT)
(NOT-RIGHTOF12-ROBOT)
(NOT-RIGHTOF11-ROBOT)
(NOT-RIGHTOF10-ROBOT)
(NOT-RIGHTOF9-ROBOT)
(NOT-RIGHTOF8-ROBOT)
(NOT-RIGHTOF7-ROBOT)
(NOT-RIGHTOF6-ROBOT)
(NOT-RIGHTOF5-ROBOT)
(NOT-RIGHTOF4-ROBOT)
(NOT-RIGHTOF3-ROBOT)
(NOT-RIGHTOF1-ROBOT)
(RIGHTOF2-ROBOT)
(NOT-RIGHTOF0-ROBOT)
(NOT-LEFTOF14-ROBOT)
(NOT-LEFTOF13-ROBOT)
(NOT-LEFTOF12-ROBOT)
(NOT-LEFTOF11-ROBOT)
(NOT-LEFTOF10-ROBOT)
(NOT-LEFTOF9-ROBOT)
(NOT-LEFTOF8-ROBOT)
(NOT-LEFTOF7-ROBOT)
(NOT-LEFTOF6-ROBOT)
(NOT-LEFTOF5-ROBOT)
(NOT-LEFTOF4-ROBOT)
(NOT-LEFTOF3-ROBOT)
(NOT-LEFTOF2-ROBOT)
(NOT-LEFTOF1-ROBOT)
(NOT-CHECKCONSISTENCY-)
(NOT-COLUMN15-ROBOT)
(NOT-LEFTOF16-ROBOT)
(LEFTOF15-ROBOT)
(NOT-RIGHTOF15-ROBOT)
(NOT-ERROR-)
(NOT-ROW15-ROBOT)
(NOT-ROW14-ROBOT)
(NOT-ROW13-ROBOT)
(NOT-ROW12-ROBOT)
(NOT-ROW11-ROBOT)
(NOT-ROW10-ROBOT)
(NOT-ROW9-ROBOT)
(NOT-ROW8-ROBOT)
(NOT-ROW7-ROBOT)
(NOT-ROW6-ROBOT)
(NOT-ROW5-ROBOT)
(NOT-ROW4-ROBOT)
(NOT-ROW3-ROBOT)
(NOT-ROW2-ROBOT)
(NOT-ROW1-ROBOT)
(NOT-BELOWOF16-ROBOT)
(BELOWOF15-ROBOT)
(NOT-BELOWOF14-ROBOT)
(NOT-BELOWOF13-ROBOT)
(NOT-BELOWOF12-ROBOT)
(NOT-BELOWOF11-ROBOT)
(NOT-BELOWOF10-ROBOT)
(NOT-BELOWOF9-ROBOT)
(NOT-BELOWOF8-ROBOT)
(NOT-BELOWOF7-ROBOT)
(NOT-BELOWOF6-ROBOT)
(NOT-BELOWOF5-ROBOT)
(NOT-BELOWOF4-ROBOT)
(NOT-BELOWOF3-ROBOT)
(NOT-BELOWOF2-ROBOT)
(NOT-ABOVEOF15-ROBOT)
(NOT-ABOVEOF14-ROBOT)
(NOT-ABOVEOF13-ROBOT)
(NOT-ABOVEOF12-ROBOT)
(NOT-ABOVEOF11-ROBOT)
(NOT-ABOVEOF10-ROBOT)
(NOT-ABOVEOF9-ROBOT)
(NOT-ABOVEOF8-ROBOT)
(NOT-ABOVEOF7-ROBOT)
(NOT-ABOVEOF6-ROBOT)
(NOT-ABOVEOF5-ROBOT)
(NOT-ABOVEOF4-ROBOT)
(NOT-ABOVEOF3-ROBOT)
(NOT-ABOVEOF2-ROBOT)
(NOT-ABOVEOF1-ROBOT)
(NOT-ROW0-ROBOT)
(NOT-BELOWOF1-ROBOT)
(Flag213prime-)
(Flag212prime-)
(Flag195prime-)
(Flag33prime-)
(Flag7prime-)
(Flag6prime-)
(Flag5prime-)
(Flag3prime-)
(Flag2prime-)
(Flag391prime-)
(Flag389prime-)
(Flag387prime-)
(Flag386prime-)
(Flag384prime-)
(Flag383prime-)
(Flag382prime-)
(Flag381prime-)
(Flag379prime-)
(Flag378prime-)
(Flag377prime-)
(Flag376prime-)
(Flag375prime-)
(Flag373prime-)
(Flag372prime-)
(Flag371prime-)
(Flag370prime-)
(Flag369prime-)
(Flag367prime-)
(Flag366prime-)
(Flag365prime-)
(Flag364prime-)
(Flag363prime-)
(Flag362prime-)
(Flag360prime-)
(Flag359prime-)
(Flag358prime-)
(Flag357prime-)
(Flag356prime-)
(Flag355prime-)
(Flag354prime-)
(Flag353prime-)
(Flag351prime-)
(Flag350prime-)
(Flag349prime-)
(Flag348prime-)
(Flag347prime-)
(Flag346prime-)
(Flag345prime-)
(Flag344prime-)
(Flag343prime-)
(Flag342prime-)
(Flag340prime-)
(Flag339prime-)
(Flag338prime-)
(Flag337prime-)
(Flag336prime-)
(Flag335prime-)
(Flag334prime-)
(Flag333prime-)
(Flag332prime-)
(Flag331prime-)
(Flag329prime-)
(Flag328prime-)
(Flag327prime-)
(Flag326prime-)
(Flag325prime-)
(Flag324prime-)
(Flag323prime-)
(Flag322prime-)
(Flag321prime-)
(Flag320prime-)
(Flag319prime-)
(Flag318prime-)
(Flag316prime-)
(Flag315prime-)
(Flag314prime-)
(Flag313prime-)
(Flag312prime-)
(Flag311prime-)
(Flag310prime-)
(Flag309prime-)
(Flag308prime-)
(Flag307prime-)
(Flag306prime-)
(Flag305prime-)
(Flag304prime-)
(Flag302prime-)
(Flag301prime-)
(Flag300prime-)
(Flag299prime-)
(Flag298prime-)
(Flag297prime-)
(Flag296prime-)
(Flag295prime-)
(Flag294prime-)
(Flag293prime-)
(Flag292prime-)
(Flag291prime-)
(Flag289prime-)
(Flag288prime-)
(Flag287prime-)
(Flag286prime-)
(Flag285prime-)
(Flag284prime-)
(Flag283prime-)
(Flag282prime-)
(Flag281prime-)
(Flag280prime-)
(Flag279prime-)
(Flag278prime-)
(Flag277prime-)
(Flag276prime-)
(Flag275prime-)
(Flag273prime-)
(Flag272prime-)
(Flag271prime-)
(Flag270prime-)
(Flag269prime-)
(Flag268prime-)
(Flag267prime-)
(Flag266prime-)
(Flag265prime-)
(Flag264prime-)
(Flag263prime-)
(Flag262prime-)
(Flag261prime-)
(Flag260prime-)
(Flag259prime-)
(Flag257prime-)
(Flag256prime-)
(Flag255prime-)
(Flag254prime-)
(Flag253prime-)
(Flag252prime-)
(Flag251prime-)
(Flag250prime-)
(Flag249prime-)
(Flag248prime-)
(Flag247prime-)
(Flag246prime-)
(Flag245prime-)
(Flag244prime-)
(Flag243prime-)
(Flag242prime-)
(Flag241prime-)
(Flag240prime-)
(Flag239prime-)
(Flag238prime-)
(Flag237prime-)
(Flag236prime-)
(Flag235prime-)
(Flag234prime-)
(Flag233prime-)
(Flag232prime-)
(Flag231prime-)
(Flag230prime-)
(Flag229prime-)
(Flag228prime-)
(Flag227prime-)
(Flag226prime-)
(Flag225prime-)
(Flag224prime-)
(Flag223prime-)
(Flag222prime-)
(Flag221prime-)
(Flag220prime-)
(Flag219prime-)
(Flag218prime-)
(Flag217prime-)
(Flag216prime-)
(Flag215prime-)
(Flag214prime-)
(Flag211prime-)
(Flag210prime-)
(Flag209prime-)
(Flag208prime-)
(Flag207prime-)
(Flag206prime-)
(Flag205prime-)
(Flag204prime-)
(Flag203prime-)
(Flag202prime-)
(Flag201prime-)
(Flag200prime-)
(Flag199prime-)
(Flag4prime-)
(Flag390prime-)
(Flag388prime-)
(Flag385prime-)
(Flag380prime-)
(Flag374prime-)
(Flag368prime-)
(Flag361prime-)
(Flag352prime-)
(Flag341prime-)
(Flag330prime-)
(Flag317prime-)
(Flag303prime-)
(Flag290prime-)
(Flag274prime-)
(Flag258prime-)
(Flag197prime-)
(Flag196prime-)
(Flag193prime-)
(Flag192prime-)
(Flag190prime-)
(Flag189prime-)
(Flag188prime-)
(Flag186prime-)
(Flag185prime-)
(Flag184prime-)
(Flag183prime-)
(Flag182prime-)
(Flag180prime-)
(Flag179prime-)
(Flag178prime-)
(Flag177prime-)
(Flag176prime-)
(Flag174prime-)
(Flag173prime-)
(Flag172prime-)
(Flag171prime-)
(Flag170prime-)
(Flag169prime-)
(Flag168prime-)
(Flag166prime-)
(Flag165prime-)
(Flag164prime-)
(Flag163prime-)
(Flag162prime-)
(Flag161prime-)
(Flag160prime-)
(Flag158prime-)
(Flag157prime-)
(Flag156prime-)
(Flag155prime-)
(Flag154prime-)
(Flag153prime-)
(Flag152prime-)
(Flag151prime-)
(Flag150prime-)
(Flag148prime-)
(Flag147prime-)
(Flag146prime-)
(Flag145prime-)
(Flag144prime-)
(Flag143prime-)
(Flag142prime-)
(Flag141prime-)
(Flag140prime-)
(Flag139prime-)
(Flag137prime-)
(Flag136prime-)
(Flag135prime-)
(Flag134prime-)
(Flag133prime-)
(Flag132prime-)
(Flag131prime-)
(Flag130prime-)
(Flag129prime-)
(Flag128prime-)
(Flag127prime-)
(Flag126prime-)
(Flag124prime-)
(Flag123prime-)
(Flag122prime-)
(Flag121prime-)
(Flag120prime-)
(Flag119prime-)
(Flag118prime-)
(Flag117prime-)
(Flag116prime-)
(Flag115prime-)
(Flag114prime-)
(Flag112prime-)
(Flag111prime-)
(Flag110prime-)
(Flag109prime-)
(Flag108prime-)
(Flag107prime-)
(Flag106prime-)
(Flag105prime-)
(Flag104prime-)
(Flag103prime-)
(Flag102prime-)
(Flag101prime-)
(Flag100prime-)
(Flag99prime-)
(Flag97prime-)
(Flag96prime-)
(Flag95prime-)
(Flag94prime-)
(Flag93prime-)
(Flag92prime-)
(Flag91prime-)
(Flag90prime-)
(Flag89prime-)
(Flag88prime-)
(Flag87prime-)
(Flag86prime-)
(Flag85prime-)
(Flag84prime-)
(Flag83prime-)
(Flag81prime-)
(Flag80prime-)
(Flag79prime-)
(Flag78prime-)
(Flag77prime-)
(Flag76prime-)
(Flag75prime-)
(Flag74prime-)
(Flag73prime-)
(Flag72prime-)
(Flag71prime-)
(Flag70prime-)
(Flag69prime-)
(Flag68prime-)
(Flag66prime-)
(Flag65prime-)
(Flag64prime-)
(Flag63prime-)
(Flag62prime-)
(Flag61prime-)
(Flag60prime-)
(Flag59prime-)
(Flag58prime-)
(Flag57prime-)
(Flag56prime-)
(Flag55prime-)
(Flag54prime-)
(Flag53prime-)
(Flag52prime-)
(Flag51prime-)
(Flag49prime-)
(Flag48prime-)
(Flag47prime-)
(Flag46prime-)
(Flag45prime-)
(Flag44prime-)
(Flag43prime-)
(Flag42prime-)
(Flag41prime-)
(Flag40prime-)
(Flag39prime-)
(Flag38prime-)
(Flag37prime-)
(Flag36prime-)
(Flag35prime-)
(Flag34prime-)
(Flag32prime-)
(Flag31prime-)
(Flag30prime-)
(Flag29prime-)
(Flag28prime-)
(Flag27prime-)
(Flag26prime-)
(Flag25prime-)
(Flag24prime-)
(Flag23prime-)
(Flag22prime-)
(Flag21prime-)
(Flag20prime-)
(Flag19prime-)
(Flag18prime-)
(Flag17prime-)
(Flag16prime-)
(Flag15prime-)
(Flag14prime-)
(Flag13prime-)
(Flag12prime-)
(Flag11prime-)
(Flag10prime-)
(Flag9prime-)
(Flag8prime-)
(Flag1prime-)
(Flag198prime-)
(Flag194prime-)
(Flag191prime-)
(Flag187prime-)
(Flag181prime-)
(Flag175prime-)
(Flag167prime-)
(Flag159prime-)
(Flag149prime-)
(Flag138prime-)
(Flag125prime-)
(Flag113prime-)
(Flag98prime-)
(Flag82prime-)
(Flag67prime-)
(Flag50prime-)
)
(:action Flag50Action-0
:parameters ()
:precondition
(and
(Flag34-)
(Flag34prime-)
)
:effect
(and
(Flag50-)
(Flag50prime-)
)

)
(:action Flag50Action-1
:parameters ()
:precondition
(and
(Flag35-)
(Flag35prime-)
)
:effect
(and
(Flag50-)
(Flag50prime-)
)

)
(:action Flag50Action-2
:parameters ()
:precondition
(and
(Flag36-)
(Flag36prime-)
)
:effect
(and
(Flag50-)
(Flag50prime-)
)

)
(:action Flag50Action-3
:parameters ()
:precondition
(and
(Flag37-)
(Flag37prime-)
)
:effect
(and
(Flag50-)
(Flag50prime-)
)

)
(:action Flag50Action-4
:parameters ()
:precondition
(and
(Flag38-)
(Flag38prime-)
)
:effect
(and
(Flag50-)
(Flag50prime-)
)

)
(:action Flag50Action-5
:parameters ()
:precondition
(and
(Flag39-)
(Flag39prime-)
)
:effect
(and
(Flag50-)
(Flag50prime-)
)

)
(:action Flag50Action-6
:parameters ()
:precondition
(and
(Flag40-)
(Flag40prime-)
)
:effect
(and
(Flag50-)
(Flag50prime-)
)

)
(:action Flag50Action-7
:parameters ()
:precondition
(and
(Flag41-)
(Flag41prime-)
)
:effect
(and
(Flag50-)
(Flag50prime-)
)

)
(:action Flag50Action-8
:parameters ()
:precondition
(and
(Flag42-)
(Flag42prime-)
)
:effect
(and
(Flag50-)
(Flag50prime-)
)

)
(:action Flag50Action-9
:parameters ()
:precondition
(and
(Flag43-)
(Flag43prime-)
)
:effect
(and
(Flag50-)
(Flag50prime-)
)

)
(:action Flag50Action-10
:parameters ()
:precondition
(and
(Flag44-)
(Flag44prime-)
)
:effect
(and
(Flag50-)
(Flag50prime-)
)

)
(:action Flag50Action-11
:parameters ()
:precondition
(and
(Flag45-)
(Flag45prime-)
)
:effect
(and
(Flag50-)
(Flag50prime-)
)

)
(:action Flag50Action-12
:parameters ()
:precondition
(and
(Flag46-)
(Flag46prime-)
)
:effect
(and
(Flag50-)
(Flag50prime-)
)

)
(:action Flag50Action-13
:parameters ()
:precondition
(and
(Flag47-)
(Flag47prime-)
)
:effect
(and
(Flag50-)
(Flag50prime-)
)

)
(:action Flag50Action-14
:parameters ()
:precondition
(and
(Flag48-)
(Flag48prime-)
)
:effect
(and
(Flag50-)
(Flag50prime-)
)

)
(:action Flag50Action-15
:parameters ()
:precondition
(and
(Flag49-)
(Flag49prime-)
)
:effect
(and
(Flag50-)
(Flag50prime-)
)

)
(:action Prim50Action
:parameters ()
:precondition
(and
(not (Flag34-))
(Flag34prime-)
(not (Flag35-))
(Flag35prime-)
(not (Flag36-))
(Flag36prime-)
(not (Flag37-))
(Flag37prime-)
(not (Flag38-))
(Flag38prime-)
(not (Flag39-))
(Flag39prime-)
(not (Flag40-))
(Flag40prime-)
(not (Flag41-))
(Flag41prime-)
(not (Flag42-))
(Flag42prime-)
(not (Flag43-))
(Flag43prime-)
(not (Flag44-))
(Flag44prime-)
(not (Flag45-))
(Flag45prime-)
(not (Flag46-))
(Flag46prime-)
(not (Flag47-))
(Flag47prime-)
(not (Flag48-))
(Flag48prime-)
(not (Flag49-))
(Flag49prime-)
)
:effect
(and
(Flag50prime-)
(not (Flag50-))
)

)
(:action Flag67Action-0
:parameters ()
:precondition
(and
(Flag51-)
(Flag51prime-)
)
:effect
(and
(Flag67-)
(Flag67prime-)
)

)
(:action Flag67Action-1
:parameters ()
:precondition
(and
(Flag52-)
(Flag52prime-)
)
:effect
(and
(Flag67-)
(Flag67prime-)
)

)
(:action Flag67Action-2
:parameters ()
:precondition
(and
(Flag53-)
(Flag53prime-)
)
:effect
(and
(Flag67-)
(Flag67prime-)
)

)
(:action Flag67Action-3
:parameters ()
:precondition
(and
(Flag37-)
(Flag37prime-)
)
:effect
(and
(Flag67-)
(Flag67prime-)
)

)
(:action Flag67Action-4
:parameters ()
:precondition
(and
(Flag40-)
(Flag40prime-)
)
:effect
(and
(Flag67-)
(Flag67prime-)
)

)
(:action Flag67Action-5
:parameters ()
:precondition
(and
(Flag41-)
(Flag41prime-)
)
:effect
(and
(Flag67-)
(Flag67prime-)
)

)
(:action Flag67Action-6
:parameters ()
:precondition
(and
(Flag49-)
(Flag49prime-)
)
:effect
(and
(Flag67-)
(Flag67prime-)
)

)
(:action Flag67Action-7
:parameters ()
:precondition
(and
(Flag54-)
(Flag54prime-)
)
:effect
(and
(Flag67-)
(Flag67prime-)
)

)
(:action Flag67Action-8
:parameters ()
:precondition
(and
(Flag55-)
(Flag55prime-)
)
:effect
(and
(Flag67-)
(Flag67prime-)
)

)
(:action Flag67Action-9
:parameters ()
:precondition
(and
(Flag39-)
(Flag39prime-)
)
:effect
(and
(Flag67-)
(Flag67prime-)
)

)
(:action Flag67Action-10
:parameters ()
:precondition
(and
(Flag56-)
(Flag56prime-)
)
:effect
(and
(Flag67-)
(Flag67prime-)
)

)
(:action Flag67Action-11
:parameters ()
:precondition
(and
(Flag57-)
(Flag57prime-)
)
:effect
(and
(Flag67-)
(Flag67prime-)
)

)
(:action Flag67Action-12
:parameters ()
:precondition
(and
(Flag45-)
(Flag45prime-)
)
:effect
(and
(Flag67-)
(Flag67prime-)
)

)
(:action Flag67Action-13
:parameters ()
:precondition
(and
(Flag58-)
(Flag58prime-)
)
:effect
(and
(Flag67-)
(Flag67prime-)
)

)
(:action Flag67Action-14
:parameters ()
:precondition
(and
(Flag59-)
(Flag59prime-)
)
:effect
(and
(Flag67-)
(Flag67prime-)
)

)
(:action Flag67Action-15
:parameters ()
:precondition
(and
(Flag34-)
(Flag34prime-)
)
:effect
(and
(Flag67-)
(Flag67prime-)
)

)
(:action Flag67Action-16
:parameters ()
:precondition
(and
(Flag60-)
(Flag60prime-)
)
:effect
(and
(Flag67-)
(Flag67prime-)
)

)
(:action Flag67Action-17
:parameters ()
:precondition
(and
(Flag35-)
(Flag35prime-)
)
:effect
(and
(Flag67-)
(Flag67prime-)
)

)
(:action Flag67Action-18
:parameters ()
:precondition
(and
(Flag36-)
(Flag36prime-)
)
:effect
(and
(Flag67-)
(Flag67prime-)
)

)
(:action Flag67Action-19
:parameters ()
:precondition
(and
(Flag61-)
(Flag61prime-)
)
:effect
(and
(Flag67-)
(Flag67prime-)
)

)
(:action Flag67Action-20
:parameters ()
:precondition
(and
(Flag43-)
(Flag43prime-)
)
:effect
(and
(Flag67-)
(Flag67prime-)
)

)
(:action Flag67Action-21
:parameters ()
:precondition
(and
(Flag46-)
(Flag46prime-)
)
:effect
(and
(Flag67-)
(Flag67prime-)
)

)
(:action Flag67Action-22
:parameters ()
:precondition
(and
(Flag38-)
(Flag38prime-)
)
:effect
(and
(Flag67-)
(Flag67prime-)
)

)
(:action Flag67Action-23
:parameters ()
:precondition
(and
(Flag62-)
(Flag62prime-)
)
:effect
(and
(Flag67-)
(Flag67prime-)
)

)
(:action Flag67Action-24
:parameters ()
:precondition
(and
(Flag63-)
(Flag63prime-)
)
:effect
(and
(Flag67-)
(Flag67prime-)
)

)
(:action Flag67Action-25
:parameters ()
:precondition
(and
(Flag64-)
(Flag64prime-)
)
:effect
(and
(Flag67-)
(Flag67prime-)
)

)
(:action Flag67Action-26
:parameters ()
:precondition
(and
(Flag44-)
(Flag44prime-)
)
:effect
(and
(Flag67-)
(Flag67prime-)
)

)
(:action Flag67Action-27
:parameters ()
:precondition
(and
(Flag47-)
(Flag47prime-)
)
:effect
(and
(Flag67-)
(Flag67prime-)
)

)
(:action Flag67Action-28
:parameters ()
:precondition
(and
(Flag48-)
(Flag48prime-)
)
:effect
(and
(Flag67-)
(Flag67prime-)
)

)
(:action Flag67Action-29
:parameters ()
:precondition
(and
(Flag65-)
(Flag65prime-)
)
:effect
(and
(Flag67-)
(Flag67prime-)
)

)
(:action Flag67Action-30
:parameters ()
:precondition
(and
(Flag66-)
(Flag66prime-)
)
:effect
(and
(Flag67-)
(Flag67prime-)
)

)
(:action Prim67Action
:parameters ()
:precondition
(and
(not (Flag51-))
(Flag51prime-)
(not (Flag52-))
(Flag52prime-)
(not (Flag53-))
(Flag53prime-)
(not (Flag37-))
(Flag37prime-)
(not (Flag40-))
(Flag40prime-)
(not (Flag41-))
(Flag41prime-)
(not (Flag49-))
(Flag49prime-)
(not (Flag54-))
(Flag54prime-)
(not (Flag55-))
(Flag55prime-)
(not (Flag39-))
(Flag39prime-)
(not (Flag56-))
(Flag56prime-)
(not (Flag57-))
(Flag57prime-)
(not (Flag45-))
(Flag45prime-)
(not (Flag58-))
(Flag58prime-)
(not (Flag59-))
(Flag59prime-)
(not (Flag34-))
(Flag34prime-)
(not (Flag60-))
(Flag60prime-)
(not (Flag35-))
(Flag35prime-)
(not (Flag36-))
(Flag36prime-)
(not (Flag61-))
(Flag61prime-)
(not (Flag43-))
(Flag43prime-)
(not (Flag46-))
(Flag46prime-)
(not (Flag38-))
(Flag38prime-)
(not (Flag62-))
(Flag62prime-)
(not (Flag63-))
(Flag63prime-)
(not (Flag64-))
(Flag64prime-)
(not (Flag44-))
(Flag44prime-)
(not (Flag47-))
(Flag47prime-)
(not (Flag48-))
(Flag48prime-)
(not (Flag65-))
(Flag65prime-)
(not (Flag66-))
(Flag66prime-)
)
:effect
(and
(Flag67prime-)
(not (Flag67-))
)

)
(:action Flag82Action-0
:parameters ()
:precondition
(and
(Flag51-)
(Flag51prime-)
)
:effect
(and
(Flag82-)
(Flag82prime-)
)

)
(:action Flag82Action-1
:parameters ()
:precondition
(and
(Flag52-)
(Flag52prime-)
)
:effect
(and
(Flag82-)
(Flag82prime-)
)

)
(:action Flag82Action-2
:parameters ()
:precondition
(and
(Flag53-)
(Flag53prime-)
)
:effect
(and
(Flag82-)
(Flag82prime-)
)

)
(:action Flag82Action-3
:parameters ()
:precondition
(and
(Flag68-)
(Flag68prime-)
)
:effect
(and
(Flag82-)
(Flag82prime-)
)

)
(:action Flag82Action-4
:parameters ()
:precondition
(and
(Flag37-)
(Flag37prime-)
)
:effect
(and
(Flag82-)
(Flag82prime-)
)

)
(:action Flag82Action-5
:parameters ()
:precondition
(and
(Flag40-)
(Flag40prime-)
)
:effect
(and
(Flag82-)
(Flag82prime-)
)

)
(:action Flag82Action-6
:parameters ()
:precondition
(and
(Flag69-)
(Flag69prime-)
)
:effect
(and
(Flag82-)
(Flag82prime-)
)

)
(:action Flag82Action-7
:parameters ()
:precondition
(and
(Flag70-)
(Flag70prime-)
)
:effect
(and
(Flag82-)
(Flag82prime-)
)

)
(:action Flag82Action-8
:parameters ()
:precondition
(and
(Flag41-)
(Flag41prime-)
)
:effect
(and
(Flag82-)
(Flag82prime-)
)

)
(:action Flag82Action-9
:parameters ()
:precondition
(and
(Flag49-)
(Flag49prime-)
)
:effect
(and
(Flag82-)
(Flag82prime-)
)

)
(:action Flag82Action-10
:parameters ()
:precondition
(and
(Flag55-)
(Flag55prime-)
)
:effect
(and
(Flag82-)
(Flag82prime-)
)

)
(:action Flag82Action-11
:parameters ()
:precondition
(and
(Flag36-)
(Flag36prime-)
)
:effect
(and
(Flag82-)
(Flag82prime-)
)

)
(:action Flag82Action-12
:parameters ()
:precondition
(and
(Flag57-)
(Flag57prime-)
)
:effect
(and
(Flag82-)
(Flag82prime-)
)

)
(:action Flag82Action-13
:parameters ()
:precondition
(and
(Flag71-)
(Flag71prime-)
)
:effect
(and
(Flag82-)
(Flag82prime-)
)

)
(:action Flag82Action-14
:parameters ()
:precondition
(and
(Flag45-)
(Flag45prime-)
)
:effect
(and
(Flag82-)
(Flag82prime-)
)

)
(:action Flag82Action-15
:parameters ()
:precondition
(and
(Flag58-)
(Flag58prime-)
)
:effect
(and
(Flag82-)
(Flag82prime-)
)

)
(:action Flag82Action-16
:parameters ()
:precondition
(and
(Flag43-)
(Flag43prime-)
)
:effect
(and
(Flag82-)
(Flag82prime-)
)

)
(:action Flag82Action-17
:parameters ()
:precondition
(and
(Flag72-)
(Flag72prime-)
)
:effect
(and
(Flag82-)
(Flag82prime-)
)

)
(:action Flag82Action-18
:parameters ()
:precondition
(and
(Flag59-)
(Flag59prime-)
)
:effect
(and
(Flag82-)
(Flag82prime-)
)

)
(:action Flag82Action-19
:parameters ()
:precondition
(and
(Flag34-)
(Flag34prime-)
)
:effect
(and
(Flag82-)
(Flag82prime-)
)

)
(:action Flag82Action-20
:parameters ()
:precondition
(and
(Flag60-)
(Flag60prime-)
)
:effect
(and
(Flag82-)
(Flag82prime-)
)

)
(:action Flag82Action-21
:parameters ()
:precondition
(and
(Flag35-)
(Flag35prime-)
)
:effect
(and
(Flag82-)
(Flag82prime-)
)

)
(:action Flag82Action-22
:parameters ()
:precondition
(and
(Flag73-)
(Flag73prime-)
)
:effect
(and
(Flag82-)
(Flag82prime-)
)

)
(:action Flag82Action-23
:parameters ()
:precondition
(and
(Flag74-)
(Flag74prime-)
)
:effect
(and
(Flag82-)
(Flag82prime-)
)

)
(:action Flag82Action-24
:parameters ()
:precondition
(and
(Flag56-)
(Flag56prime-)
)
:effect
(and
(Flag82-)
(Flag82prime-)
)

)
(:action Flag82Action-25
:parameters ()
:precondition
(and
(Flag61-)
(Flag61prime-)
)
:effect
(and
(Flag82-)
(Flag82prime-)
)

)
(:action Flag82Action-26
:parameters ()
:precondition
(and
(Flag75-)
(Flag75prime-)
)
:effect
(and
(Flag82-)
(Flag82prime-)
)

)
(:action Flag82Action-27
:parameters ()
:precondition
(and
(Flag76-)
(Flag76prime-)
)
:effect
(and
(Flag82-)
(Flag82prime-)
)

)
(:action Flag82Action-28
:parameters ()
:precondition
(and
(Flag46-)
(Flag46prime-)
)
:effect
(and
(Flag82-)
(Flag82prime-)
)

)
(:action Flag82Action-29
:parameters ()
:precondition
(and
(Flag77-)
(Flag77prime-)
)
:effect
(and
(Flag82-)
(Flag82prime-)
)

)
(:action Flag82Action-30
:parameters ()
:precondition
(and
(Flag78-)
(Flag78prime-)
)
:effect
(and
(Flag82-)
(Flag82prime-)
)

)
(:action Flag82Action-31
:parameters ()
:precondition
(and
(Flag79-)
(Flag79prime-)
)
:effect
(and
(Flag82-)
(Flag82prime-)
)

)
(:action Flag82Action-32
:parameters ()
:precondition
(and
(Flag38-)
(Flag38prime-)
)
:effect
(and
(Flag82-)
(Flag82prime-)
)

)
(:action Flag82Action-33
:parameters ()
:precondition
(and
(Flag62-)
(Flag62prime-)
)
:effect
(and
(Flag82-)
(Flag82prime-)
)

)
(:action Flag82Action-34
:parameters ()
:precondition
(and
(Flag63-)
(Flag63prime-)
)
:effect
(and
(Flag82-)
(Flag82prime-)
)

)
(:action Flag82Action-35
:parameters ()
:precondition
(and
(Flag64-)
(Flag64prime-)
)
:effect
(and
(Flag82-)
(Flag82prime-)
)

)
(:action Flag82Action-36
:parameters ()
:precondition
(and
(Flag44-)
(Flag44prime-)
)
:effect
(and
(Flag82-)
(Flag82prime-)
)

)
(:action Flag82Action-37
:parameters ()
:precondition
(and
(Flag47-)
(Flag47prime-)
)
:effect
(and
(Flag82-)
(Flag82prime-)
)

)
(:action Flag82Action-38
:parameters ()
:precondition
(and
(Flag48-)
(Flag48prime-)
)
:effect
(and
(Flag82-)
(Flag82prime-)
)

)
(:action Flag82Action-39
:parameters ()
:precondition
(and
(Flag65-)
(Flag65prime-)
)
:effect
(and
(Flag82-)
(Flag82prime-)
)

)
(:action Flag82Action-40
:parameters ()
:precondition
(and
(Flag80-)
(Flag80prime-)
)
:effect
(and
(Flag82-)
(Flag82prime-)
)

)
(:action Flag82Action-41
:parameters ()
:precondition
(and
(Flag66-)
(Flag66prime-)
)
:effect
(and
(Flag82-)
(Flag82prime-)
)

)
(:action Flag82Action-42
:parameters ()
:precondition
(and
(Flag81-)
(Flag81prime-)
)
:effect
(and
(Flag82-)
(Flag82prime-)
)

)
(:action Prim82Action
:parameters ()
:precondition
(and
(not (Flag51-))
(Flag51prime-)
(not (Flag52-))
(Flag52prime-)
(not (Flag53-))
(Flag53prime-)
(not (Flag68-))
(Flag68prime-)
(not (Flag37-))
(Flag37prime-)
(not (Flag40-))
(Flag40prime-)
(not (Flag69-))
(Flag69prime-)
(not (Flag70-))
(Flag70prime-)
(not (Flag41-))
(Flag41prime-)
(not (Flag49-))
(Flag49prime-)
(not (Flag55-))
(Flag55prime-)
(not (Flag36-))
(Flag36prime-)
(not (Flag57-))
(Flag57prime-)
(not (Flag71-))
(Flag71prime-)
(not (Flag45-))
(Flag45prime-)
(not (Flag58-))
(Flag58prime-)
(not (Flag43-))
(Flag43prime-)
(not (Flag72-))
(Flag72prime-)
(not (Flag59-))
(Flag59prime-)
(not (Flag34-))
(Flag34prime-)
(not (Flag60-))
(Flag60prime-)
(not (Flag35-))
(Flag35prime-)
(not (Flag73-))
(Flag73prime-)
(not (Flag74-))
(Flag74prime-)
(not (Flag56-))
(Flag56prime-)
(not (Flag61-))
(Flag61prime-)
(not (Flag75-))
(Flag75prime-)
(not (Flag76-))
(Flag76prime-)
(not (Flag46-))
(Flag46prime-)
(not (Flag77-))
(Flag77prime-)
(not (Flag78-))
(Flag78prime-)
(not (Flag79-))
(Flag79prime-)
(not (Flag38-))
(Flag38prime-)
(not (Flag62-))
(Flag62prime-)
(not (Flag63-))
(Flag63prime-)
(not (Flag64-))
(Flag64prime-)
(not (Flag44-))
(Flag44prime-)
(not (Flag47-))
(Flag47prime-)
(not (Flag48-))
(Flag48prime-)
(not (Flag65-))
(Flag65prime-)
(not (Flag80-))
(Flag80prime-)
(not (Flag66-))
(Flag66prime-)
(not (Flag81-))
(Flag81prime-)
)
:effect
(and
(Flag82prime-)
(not (Flag82-))
)

)
(:action Flag98Action-0
:parameters ()
:precondition
(and
(Flag51-)
(Flag51prime-)
)
:effect
(and
(Flag98-)
(Flag98prime-)
)

)
(:action Flag98Action-1
:parameters ()
:precondition
(and
(Flag52-)
(Flag52prime-)
)
:effect
(and
(Flag98-)
(Flag98prime-)
)

)
(:action Flag98Action-2
:parameters ()
:precondition
(and
(Flag53-)
(Flag53prime-)
)
:effect
(and
(Flag98-)
(Flag98prime-)
)

)
(:action Flag98Action-3
:parameters ()
:precondition
(and
(Flag68-)
(Flag68prime-)
)
:effect
(and
(Flag98-)
(Flag98prime-)
)

)
(:action Flag98Action-4
:parameters ()
:precondition
(and
(Flag37-)
(Flag37prime-)
)
:effect
(and
(Flag98-)
(Flag98prime-)
)

)
(:action Flag98Action-5
:parameters ()
:precondition
(and
(Flag40-)
(Flag40prime-)
)
:effect
(and
(Flag98-)
(Flag98prime-)
)

)
(:action Flag98Action-6
:parameters ()
:precondition
(and
(Flag69-)
(Flag69prime-)
)
:effect
(and
(Flag98-)
(Flag98prime-)
)

)
(:action Flag98Action-7
:parameters ()
:precondition
(and
(Flag48-)
(Flag48prime-)
)
:effect
(and
(Flag98-)
(Flag98prime-)
)

)
(:action Flag98Action-8
:parameters ()
:precondition
(and
(Flag83-)
(Flag83prime-)
)
:effect
(and
(Flag98-)
(Flag98prime-)
)

)
(:action Flag98Action-9
:parameters ()
:precondition
(and
(Flag70-)
(Flag70prime-)
)
:effect
(and
(Flag98-)
(Flag98prime-)
)

)
(:action Flag98Action-10
:parameters ()
:precondition
(and
(Flag41-)
(Flag41prime-)
)
:effect
(and
(Flag98-)
(Flag98prime-)
)

)
(:action Flag98Action-11
:parameters ()
:precondition
(and
(Flag49-)
(Flag49prime-)
)
:effect
(and
(Flag98-)
(Flag98prime-)
)

)
(:action Flag98Action-12
:parameters ()
:precondition
(and
(Flag84-)
(Flag84prime-)
)
:effect
(and
(Flag98-)
(Flag98prime-)
)

)
(:action Flag98Action-13
:parameters ()
:precondition
(and
(Flag85-)
(Flag85prime-)
)
:effect
(and
(Flag98-)
(Flag98prime-)
)

)
(:action Flag98Action-14
:parameters ()
:precondition
(and
(Flag55-)
(Flag55prime-)
)
:effect
(and
(Flag98-)
(Flag98prime-)
)

)
(:action Flag98Action-15
:parameters ()
:precondition
(and
(Flag74-)
(Flag74prime-)
)
:effect
(and
(Flag98-)
(Flag98prime-)
)

)
(:action Flag98Action-16
:parameters ()
:precondition
(and
(Flag36-)
(Flag36prime-)
)
:effect
(and
(Flag98-)
(Flag98prime-)
)

)
(:action Flag98Action-17
:parameters ()
:precondition
(and
(Flag57-)
(Flag57prime-)
)
:effect
(and
(Flag98-)
(Flag98prime-)
)

)
(:action Flag98Action-18
:parameters ()
:precondition
(and
(Flag86-)
(Flag86prime-)
)
:effect
(and
(Flag98-)
(Flag98prime-)
)

)
(:action Flag98Action-19
:parameters ()
:precondition
(and
(Flag71-)
(Flag71prime-)
)
:effect
(and
(Flag98-)
(Flag98prime-)
)

)
(:action Flag98Action-20
:parameters ()
:precondition
(and
(Flag45-)
(Flag45prime-)
)
:effect
(and
(Flag98-)
(Flag98prime-)
)

)
(:action Flag98Action-21
:parameters ()
:precondition
(and
(Flag58-)
(Flag58prime-)
)
:effect
(and
(Flag98-)
(Flag98prime-)
)

)
(:action Flag98Action-22
:parameters ()
:precondition
(and
(Flag43-)
(Flag43prime-)
)
:effect
(and
(Flag98-)
(Flag98prime-)
)

)
(:action Flag98Action-23
:parameters ()
:precondition
(and
(Flag72-)
(Flag72prime-)
)
:effect
(and
(Flag98-)
(Flag98prime-)
)

)
(:action Flag98Action-24
:parameters ()
:precondition
(and
(Flag59-)
(Flag59prime-)
)
:effect
(and
(Flag98-)
(Flag98prime-)
)

)
(:action Flag98Action-25
:parameters ()
:precondition
(and
(Flag34-)
(Flag34prime-)
)
:effect
(and
(Flag98-)
(Flag98prime-)
)

)
(:action Flag98Action-26
:parameters ()
:precondition
(and
(Flag87-)
(Flag87prime-)
)
:effect
(and
(Flag98-)
(Flag98prime-)
)

)
(:action Flag98Action-27
:parameters ()
:precondition
(and
(Flag60-)
(Flag60prime-)
)
:effect
(and
(Flag98-)
(Flag98prime-)
)

)
(:action Flag98Action-28
:parameters ()
:precondition
(and
(Flag35-)
(Flag35prime-)
)
:effect
(and
(Flag98-)
(Flag98prime-)
)

)
(:action Flag98Action-29
:parameters ()
:precondition
(and
(Flag73-)
(Flag73prime-)
)
:effect
(and
(Flag98-)
(Flag98prime-)
)

)
(:action Flag98Action-30
:parameters ()
:precondition
(and
(Flag88-)
(Flag88prime-)
)
:effect
(and
(Flag98-)
(Flag98prime-)
)

)
(:action Flag98Action-31
:parameters ()
:precondition
(and
(Flag56-)
(Flag56prime-)
)
:effect
(and
(Flag98-)
(Flag98prime-)
)

)
(:action Flag98Action-32
:parameters ()
:precondition
(and
(Flag61-)
(Flag61prime-)
)
:effect
(and
(Flag98-)
(Flag98prime-)
)

)
(:action Flag98Action-33
:parameters ()
:precondition
(and
(Flag89-)
(Flag89prime-)
)
:effect
(and
(Flag98-)
(Flag98prime-)
)

)
(:action Flag98Action-34
:parameters ()
:precondition
(and
(Flag75-)
(Flag75prime-)
)
:effect
(and
(Flag98-)
(Flag98prime-)
)

)
(:action Flag98Action-35
:parameters ()
:precondition
(and
(Flag76-)
(Flag76prime-)
)
:effect
(and
(Flag98-)
(Flag98prime-)
)

)
(:action Flag98Action-36
:parameters ()
:precondition
(and
(Flag46-)
(Flag46prime-)
)
:effect
(and
(Flag98-)
(Flag98prime-)
)

)
(:action Flag98Action-37
:parameters ()
:precondition
(and
(Flag78-)
(Flag78prime-)
)
:effect
(and
(Flag98-)
(Flag98prime-)
)

)
(:action Flag98Action-38
:parameters ()
:precondition
(and
(Flag90-)
(Flag90prime-)
)
:effect
(and
(Flag98-)
(Flag98prime-)
)

)
(:action Flag98Action-39
:parameters ()
:precondition
(and
(Flag91-)
(Flag91prime-)
)
:effect
(and
(Flag98-)
(Flag98prime-)
)

)
(:action Flag98Action-40
:parameters ()
:precondition
(and
(Flag79-)
(Flag79prime-)
)
:effect
(and
(Flag98-)
(Flag98prime-)
)

)
(:action Flag98Action-41
:parameters ()
:precondition
(and
(Flag92-)
(Flag92prime-)
)
:effect
(and
(Flag98-)
(Flag98prime-)
)

)
(:action Flag98Action-42
:parameters ()
:precondition
(and
(Flag38-)
(Flag38prime-)
)
:effect
(and
(Flag98-)
(Flag98prime-)
)

)
(:action Flag98Action-43
:parameters ()
:precondition
(and
(Flag62-)
(Flag62prime-)
)
:effect
(and
(Flag98-)
(Flag98prime-)
)

)
(:action Flag98Action-44
:parameters ()
:precondition
(and
(Flag93-)
(Flag93prime-)
)
:effect
(and
(Flag98-)
(Flag98prime-)
)

)
(:action Flag98Action-45
:parameters ()
:precondition
(and
(Flag94-)
(Flag94prime-)
)
:effect
(and
(Flag98-)
(Flag98prime-)
)

)
(:action Flag98Action-46
:parameters ()
:precondition
(and
(Flag63-)
(Flag63prime-)
)
:effect
(and
(Flag98-)
(Flag98prime-)
)

)
(:action Flag98Action-47
:parameters ()
:precondition
(and
(Flag95-)
(Flag95prime-)
)
:effect
(and
(Flag98-)
(Flag98prime-)
)

)
(:action Flag98Action-48
:parameters ()
:precondition
(and
(Flag47-)
(Flag47prime-)
)
:effect
(and
(Flag98-)
(Flag98prime-)
)

)
(:action Flag98Action-49
:parameters ()
:precondition
(and
(Flag96-)
(Flag96prime-)
)
:effect
(and
(Flag98-)
(Flag98prime-)
)

)
(:action Flag98Action-50
:parameters ()
:precondition
(and
(Flag97-)
(Flag97prime-)
)
:effect
(and
(Flag98-)
(Flag98prime-)
)

)
(:action Flag98Action-51
:parameters ()
:precondition
(and
(Flag65-)
(Flag65prime-)
)
:effect
(and
(Flag98-)
(Flag98prime-)
)

)
(:action Flag98Action-52
:parameters ()
:precondition
(and
(Flag80-)
(Flag80prime-)
)
:effect
(and
(Flag98-)
(Flag98prime-)
)

)
(:action Flag98Action-53
:parameters ()
:precondition
(and
(Flag66-)
(Flag66prime-)
)
:effect
(and
(Flag98-)
(Flag98prime-)
)

)
(:action Prim98Action
:parameters ()
:precondition
(and
(not (Flag51-))
(Flag51prime-)
(not (Flag52-))
(Flag52prime-)
(not (Flag53-))
(Flag53prime-)
(not (Flag68-))
(Flag68prime-)
(not (Flag37-))
(Flag37prime-)
(not (Flag40-))
(Flag40prime-)
(not (Flag69-))
(Flag69prime-)
(not (Flag48-))
(Flag48prime-)
(not (Flag83-))
(Flag83prime-)
(not (Flag70-))
(Flag70prime-)
(not (Flag41-))
(Flag41prime-)
(not (Flag49-))
(Flag49prime-)
(not (Flag84-))
(Flag84prime-)
(not (Flag85-))
(Flag85prime-)
(not (Flag55-))
(Flag55prime-)
(not (Flag74-))
(Flag74prime-)
(not (Flag36-))
(Flag36prime-)
(not (Flag57-))
(Flag57prime-)
(not (Flag86-))
(Flag86prime-)
(not (Flag71-))
(Flag71prime-)
(not (Flag45-))
(Flag45prime-)
(not (Flag58-))
(Flag58prime-)
(not (Flag43-))
(Flag43prime-)
(not (Flag72-))
(Flag72prime-)
(not (Flag59-))
(Flag59prime-)
(not (Flag34-))
(Flag34prime-)
(not (Flag87-))
(Flag87prime-)
(not (Flag60-))
(Flag60prime-)
(not (Flag35-))
(Flag35prime-)
(not (Flag73-))
(Flag73prime-)
(not (Flag88-))
(Flag88prime-)
(not (Flag56-))
(Flag56prime-)
(not (Flag61-))
(Flag61prime-)
(not (Flag89-))
(Flag89prime-)
(not (Flag75-))
(Flag75prime-)
(not (Flag76-))
(Flag76prime-)
(not (Flag46-))
(Flag46prime-)
(not (Flag78-))
(Flag78prime-)
(not (Flag90-))
(Flag90prime-)
(not (Flag91-))
(Flag91prime-)
(not (Flag79-))
(Flag79prime-)
(not (Flag92-))
(Flag92prime-)
(not (Flag38-))
(Flag38prime-)
(not (Flag62-))
(Flag62prime-)
(not (Flag93-))
(Flag93prime-)
(not (Flag94-))
(Flag94prime-)
(not (Flag63-))
(Flag63prime-)
(not (Flag95-))
(Flag95prime-)
(not (Flag47-))
(Flag47prime-)
(not (Flag96-))
(Flag96prime-)
(not (Flag97-))
(Flag97prime-)
(not (Flag65-))
(Flag65prime-)
(not (Flag80-))
(Flag80prime-)
(not (Flag66-))
(Flag66prime-)
)
:effect
(and
(Flag98prime-)
(not (Flag98-))
)

)
(:action Flag113Action-0
:parameters ()
:precondition
(and
(Flag99-)
(Flag99prime-)
)
:effect
(and
(Flag113-)
(Flag113prime-)
)

)
(:action Flag113Action-1
:parameters ()
:precondition
(and
(Flag100-)
(Flag100prime-)
)
:effect
(and
(Flag113-)
(Flag113prime-)
)

)
(:action Flag113Action-2
:parameters ()
:precondition
(and
(Flag52-)
(Flag52prime-)
)
:effect
(and
(Flag113-)
(Flag113prime-)
)

)
(:action Flag113Action-3
:parameters ()
:precondition
(and
(Flag101-)
(Flag101prime-)
)
:effect
(and
(Flag113-)
(Flag113prime-)
)

)
(:action Flag113Action-4
:parameters ()
:precondition
(and
(Flag53-)
(Flag53prime-)
)
:effect
(and
(Flag113-)
(Flag113prime-)
)

)
(:action Flag113Action-5
:parameters ()
:precondition
(and
(Flag68-)
(Flag68prime-)
)
:effect
(and
(Flag113-)
(Flag113prime-)
)

)
(:action Flag113Action-6
:parameters ()
:precondition
(and
(Flag37-)
(Flag37prime-)
)
:effect
(and
(Flag113-)
(Flag113prime-)
)

)
(:action Flag113Action-7
:parameters ()
:precondition
(and
(Flag69-)
(Flag69prime-)
)
:effect
(and
(Flag113-)
(Flag113prime-)
)

)
(:action Flag113Action-8
:parameters ()
:precondition
(and
(Flag48-)
(Flag48prime-)
)
:effect
(and
(Flag113-)
(Flag113prime-)
)

)
(:action Flag113Action-9
:parameters ()
:precondition
(and
(Flag83-)
(Flag83prime-)
)
:effect
(and
(Flag113-)
(Flag113prime-)
)

)
(:action Flag113Action-10
:parameters ()
:precondition
(and
(Flag102-)
(Flag102prime-)
)
:effect
(and
(Flag113-)
(Flag113prime-)
)

)
(:action Flag113Action-11
:parameters ()
:precondition
(and
(Flag70-)
(Flag70prime-)
)
:effect
(and
(Flag113-)
(Flag113prime-)
)

)
(:action Flag113Action-12
:parameters ()
:precondition
(and
(Flag41-)
(Flag41prime-)
)
:effect
(and
(Flag113-)
(Flag113prime-)
)

)
(:action Flag113Action-13
:parameters ()
:precondition
(and
(Flag49-)
(Flag49prime-)
)
:effect
(and
(Flag113-)
(Flag113prime-)
)

)
(:action Flag113Action-14
:parameters ()
:precondition
(and
(Flag46-)
(Flag46prime-)
)
:effect
(and
(Flag113-)
(Flag113prime-)
)

)
(:action Flag113Action-15
:parameters ()
:precondition
(and
(Flag103-)
(Flag103prime-)
)
:effect
(and
(Flag113-)
(Flag113prime-)
)

)
(:action Flag113Action-16
:parameters ()
:precondition
(and
(Flag84-)
(Flag84prime-)
)
:effect
(and
(Flag113-)
(Flag113prime-)
)

)
(:action Flag113Action-17
:parameters ()
:precondition
(and
(Flag104-)
(Flag104prime-)
)
:effect
(and
(Flag113-)
(Flag113prime-)
)

)
(:action Flag113Action-18
:parameters ()
:precondition
(and
(Flag51-)
(Flag51prime-)
)
:effect
(and
(Flag113-)
(Flag113prime-)
)

)
(:action Flag113Action-19
:parameters ()
:precondition
(and
(Flag85-)
(Flag85prime-)
)
:effect
(and
(Flag113-)
(Flag113prime-)
)

)
(:action Flag113Action-20
:parameters ()
:precondition
(and
(Flag55-)
(Flag55prime-)
)
:effect
(and
(Flag113-)
(Flag113prime-)
)

)
(:action Flag113Action-21
:parameters ()
:precondition
(and
(Flag74-)
(Flag74prime-)
)
:effect
(and
(Flag113-)
(Flag113prime-)
)

)
(:action Flag113Action-22
:parameters ()
:precondition
(and
(Flag36-)
(Flag36prime-)
)
:effect
(and
(Flag113-)
(Flag113prime-)
)

)
(:action Flag113Action-23
:parameters ()
:precondition
(and
(Flag57-)
(Flag57prime-)
)
:effect
(and
(Flag113-)
(Flag113prime-)
)

)
(:action Flag113Action-24
:parameters ()
:precondition
(and
(Flag86-)
(Flag86prime-)
)
:effect
(and
(Flag113-)
(Flag113prime-)
)

)
(:action Flag113Action-25
:parameters ()
:precondition
(and
(Flag71-)
(Flag71prime-)
)
:effect
(and
(Flag113-)
(Flag113prime-)
)

)
(:action Flag113Action-26
:parameters ()
:precondition
(and
(Flag45-)
(Flag45prime-)
)
:effect
(and
(Flag113-)
(Flag113prime-)
)

)
(:action Flag113Action-27
:parameters ()
:precondition
(and
(Flag87-)
(Flag87prime-)
)
:effect
(and
(Flag113-)
(Flag113prime-)
)

)
(:action Flag113Action-28
:parameters ()
:precondition
(and
(Flag76-)
(Flag76prime-)
)
:effect
(and
(Flag113-)
(Flag113prime-)
)

)
(:action Flag113Action-29
:parameters ()
:precondition
(and
(Flag105-)
(Flag105prime-)
)
:effect
(and
(Flag113-)
(Flag113prime-)
)

)
(:action Flag113Action-30
:parameters ()
:precondition
(and
(Flag106-)
(Flag106prime-)
)
:effect
(and
(Flag113-)
(Flag113prime-)
)

)
(:action Flag113Action-31
:parameters ()
:precondition
(and
(Flag34-)
(Flag34prime-)
)
:effect
(and
(Flag113-)
(Flag113prime-)
)

)
(:action Flag113Action-32
:parameters ()
:precondition
(and
(Flag107-)
(Flag107prime-)
)
:effect
(and
(Flag113-)
(Flag113prime-)
)

)
(:action Flag113Action-33
:parameters ()
:precondition
(and
(Flag60-)
(Flag60prime-)
)
:effect
(and
(Flag113-)
(Flag113prime-)
)

)
(:action Flag113Action-34
:parameters ()
:precondition
(and
(Flag35-)
(Flag35prime-)
)
:effect
(and
(Flag113-)
(Flag113prime-)
)

)
(:action Flag113Action-35
:parameters ()
:precondition
(and
(Flag73-)
(Flag73prime-)
)
:effect
(and
(Flag113-)
(Flag113prime-)
)

)
(:action Flag113Action-36
:parameters ()
:precondition
(and
(Flag108-)
(Flag108prime-)
)
:effect
(and
(Flag113-)
(Flag113prime-)
)

)
(:action Flag113Action-37
:parameters ()
:precondition
(and
(Flag56-)
(Flag56prime-)
)
:effect
(and
(Flag113-)
(Flag113prime-)
)

)
(:action Flag113Action-38
:parameters ()
:precondition
(and
(Flag61-)
(Flag61prime-)
)
:effect
(and
(Flag113-)
(Flag113prime-)
)

)
(:action Flag113Action-39
:parameters ()
:precondition
(and
(Flag89-)
(Flag89prime-)
)
:effect
(and
(Flag113-)
(Flag113prime-)
)

)
(:action Flag113Action-40
:parameters ()
:precondition
(and
(Flag109-)
(Flag109prime-)
)
:effect
(and
(Flag113-)
(Flag113prime-)
)

)
(:action Flag113Action-41
:parameters ()
:precondition
(and
(Flag110-)
(Flag110prime-)
)
:effect
(and
(Flag113-)
(Flag113prime-)
)

)
(:action Flag113Action-42
:parameters ()
:precondition
(and
(Flag43-)
(Flag43prime-)
)
:effect
(and
(Flag113-)
(Flag113prime-)
)

)
(:action Flag113Action-43
:parameters ()
:precondition
(and
(Flag72-)
(Flag72prime-)
)
:effect
(and
(Flag113-)
(Flag113prime-)
)

)
(:action Flag113Action-44
:parameters ()
:precondition
(and
(Flag59-)
(Flag59prime-)
)
:effect
(and
(Flag113-)
(Flag113prime-)
)

)
(:action Flag113Action-45
:parameters ()
:precondition
(and
(Flag78-)
(Flag78prime-)
)
:effect
(and
(Flag113-)
(Flag113prime-)
)

)
(:action Flag113Action-46
:parameters ()
:precondition
(and
(Flag90-)
(Flag90prime-)
)
:effect
(and
(Flag113-)
(Flag113prime-)
)

)
(:action Flag113Action-47
:parameters ()
:precondition
(and
(Flag91-)
(Flag91prime-)
)
:effect
(and
(Flag113-)
(Flag113prime-)
)

)
(:action Flag113Action-48
:parameters ()
:precondition
(and
(Flag79-)
(Flag79prime-)
)
:effect
(and
(Flag113-)
(Flag113prime-)
)

)
(:action Flag113Action-49
:parameters ()
:precondition
(and
(Flag92-)
(Flag92prime-)
)
:effect
(and
(Flag113-)
(Flag113prime-)
)

)
(:action Flag113Action-50
:parameters ()
:precondition
(and
(Flag38-)
(Flag38prime-)
)
:effect
(and
(Flag113-)
(Flag113prime-)
)

)
(:action Flag113Action-51
:parameters ()
:precondition
(and
(Flag62-)
(Flag62prime-)
)
:effect
(and
(Flag113-)
(Flag113prime-)
)

)
(:action Flag113Action-52
:parameters ()
:precondition
(and
(Flag93-)
(Flag93prime-)
)
:effect
(and
(Flag113-)
(Flag113prime-)
)

)
(:action Flag113Action-53
:parameters ()
:precondition
(and
(Flag94-)
(Flag94prime-)
)
:effect
(and
(Flag113-)
(Flag113prime-)
)

)
(:action Flag113Action-54
:parameters ()
:precondition
(and
(Flag63-)
(Flag63prime-)
)
:effect
(and
(Flag113-)
(Flag113prime-)
)

)
(:action Flag113Action-55
:parameters ()
:precondition
(and
(Flag95-)
(Flag95prime-)
)
:effect
(and
(Flag113-)
(Flag113prime-)
)

)
(:action Flag113Action-56
:parameters ()
:precondition
(and
(Flag111-)
(Flag111prime-)
)
:effect
(and
(Flag113-)
(Flag113prime-)
)

)
(:action Flag113Action-57
:parameters ()
:precondition
(and
(Flag75-)
(Flag75prime-)
)
:effect
(and
(Flag113-)
(Flag113prime-)
)

)
(:action Flag113Action-58
:parameters ()
:precondition
(and
(Flag47-)
(Flag47prime-)
)
:effect
(and
(Flag113-)
(Flag113prime-)
)

)
(:action Flag113Action-59
:parameters ()
:precondition
(and
(Flag112-)
(Flag112prime-)
)
:effect
(and
(Flag113-)
(Flag113prime-)
)

)
(:action Flag113Action-60
:parameters ()
:precondition
(and
(Flag97-)
(Flag97prime-)
)
:effect
(and
(Flag113-)
(Flag113prime-)
)

)
(:action Flag113Action-61
:parameters ()
:precondition
(and
(Flag65-)
(Flag65prime-)
)
:effect
(and
(Flag113-)
(Flag113prime-)
)

)
(:action Flag113Action-62
:parameters ()
:precondition
(and
(Flag80-)
(Flag80prime-)
)
:effect
(and
(Flag113-)
(Flag113prime-)
)

)
(:action Flag113Action-63
:parameters ()
:precondition
(and
(Flag66-)
(Flag66prime-)
)
:effect
(and
(Flag113-)
(Flag113prime-)
)

)
(:action Prim113Action
:parameters ()
:precondition
(and
(not (Flag99-))
(Flag99prime-)
(not (Flag100-))
(Flag100prime-)
(not (Flag52-))
(Flag52prime-)
(not (Flag101-))
(Flag101prime-)
(not (Flag53-))
(Flag53prime-)
(not (Flag68-))
(Flag68prime-)
(not (Flag37-))
(Flag37prime-)
(not (Flag69-))
(Flag69prime-)
(not (Flag48-))
(Flag48prime-)
(not (Flag83-))
(Flag83prime-)
(not (Flag102-))
(Flag102prime-)
(not (Flag70-))
(Flag70prime-)
(not (Flag41-))
(Flag41prime-)
(not (Flag49-))
(Flag49prime-)
(not (Flag46-))
(Flag46prime-)
(not (Flag103-))
(Flag103prime-)
(not (Flag84-))
(Flag84prime-)
(not (Flag104-))
(Flag104prime-)
(not (Flag51-))
(Flag51prime-)
(not (Flag85-))
(Flag85prime-)
(not (Flag55-))
(Flag55prime-)
(not (Flag74-))
(Flag74prime-)
(not (Flag36-))
(Flag36prime-)
(not (Flag57-))
(Flag57prime-)
(not (Flag86-))
(Flag86prime-)
(not (Flag71-))
(Flag71prime-)
(not (Flag45-))
(Flag45prime-)
(not (Flag87-))
(Flag87prime-)
(not (Flag76-))
(Flag76prime-)
(not (Flag105-))
(Flag105prime-)
(not (Flag106-))
(Flag106prime-)
(not (Flag34-))
(Flag34prime-)
(not (Flag107-))
(Flag107prime-)
(not (Flag60-))
(Flag60prime-)
(not (Flag35-))
(Flag35prime-)
(not (Flag73-))
(Flag73prime-)
(not (Flag108-))
(Flag108prime-)
(not (Flag56-))
(Flag56prime-)
(not (Flag61-))
(Flag61prime-)
(not (Flag89-))
(Flag89prime-)
(not (Flag109-))
(Flag109prime-)
(not (Flag110-))
(Flag110prime-)
(not (Flag43-))
(Flag43prime-)
(not (Flag72-))
(Flag72prime-)
(not (Flag59-))
(Flag59prime-)
(not (Flag78-))
(Flag78prime-)
(not (Flag90-))
(Flag90prime-)
(not (Flag91-))
(Flag91prime-)
(not (Flag79-))
(Flag79prime-)
(not (Flag92-))
(Flag92prime-)
(not (Flag38-))
(Flag38prime-)
(not (Flag62-))
(Flag62prime-)
(not (Flag93-))
(Flag93prime-)
(not (Flag94-))
(Flag94prime-)
(not (Flag63-))
(Flag63prime-)
(not (Flag95-))
(Flag95prime-)
(not (Flag111-))
(Flag111prime-)
(not (Flag75-))
(Flag75prime-)
(not (Flag47-))
(Flag47prime-)
(not (Flag112-))
(Flag112prime-)
(not (Flag97-))
(Flag97prime-)
(not (Flag65-))
(Flag65prime-)
(not (Flag80-))
(Flag80prime-)
(not (Flag66-))
(Flag66prime-)
)
:effect
(and
(Flag113prime-)
(not (Flag113-))
)

)
(:action Flag125Action-0
:parameters ()
:precondition
(and
(Flag62-)
(Flag62prime-)
)
:effect
(and
(Flag125-)
(Flag125prime-)
)

)
(:action Flag125Action-1
:parameters ()
:precondition
(and
(Flag100-)
(Flag100prime-)
)
:effect
(and
(Flag125-)
(Flag125prime-)
)

)
(:action Flag125Action-2
:parameters ()
:precondition
(and
(Flag52-)
(Flag52prime-)
)
:effect
(and
(Flag125-)
(Flag125prime-)
)

)
(:action Flag125Action-3
:parameters ()
:precondition
(and
(Flag99-)
(Flag99prime-)
)
:effect
(and
(Flag125-)
(Flag125prime-)
)

)
(:action Flag125Action-4
:parameters ()
:precondition
(and
(Flag53-)
(Flag53prime-)
)
:effect
(and
(Flag125-)
(Flag125prime-)
)

)
(:action Flag125Action-5
:parameters ()
:precondition
(and
(Flag102-)
(Flag102prime-)
)
:effect
(and
(Flag125-)
(Flag125prime-)
)

)
(:action Flag125Action-6
:parameters ()
:precondition
(and
(Flag37-)
(Flag37prime-)
)
:effect
(and
(Flag125-)
(Flag125prime-)
)

)
(:action Flag125Action-7
:parameters ()
:precondition
(and
(Flag69-)
(Flag69prime-)
)
:effect
(and
(Flag125-)
(Flag125prime-)
)

)
(:action Flag125Action-8
:parameters ()
:precondition
(and
(Flag48-)
(Flag48prime-)
)
:effect
(and
(Flag125-)
(Flag125prime-)
)

)
(:action Flag125Action-9
:parameters ()
:precondition
(and
(Flag83-)
(Flag83prime-)
)
:effect
(and
(Flag125-)
(Flag125prime-)
)

)
(:action Flag125Action-10
:parameters ()
:precondition
(and
(Flag80-)
(Flag80prime-)
)
:effect
(and
(Flag125-)
(Flag125prime-)
)

)
(:action Flag125Action-11
:parameters ()
:precondition
(and
(Flag41-)
(Flag41prime-)
)
:effect
(and
(Flag125-)
(Flag125prime-)
)

)
(:action Flag125Action-12
:parameters ()
:precondition
(and
(Flag49-)
(Flag49prime-)
)
:effect
(and
(Flag125-)
(Flag125prime-)
)

)
(:action Flag125Action-13
:parameters ()
:precondition
(and
(Flag104-)
(Flag104prime-)
)
:effect
(and
(Flag125-)
(Flag125prime-)
)

)
(:action Flag125Action-14
:parameters ()
:precondition
(and
(Flag68-)
(Flag68prime-)
)
:effect
(and
(Flag125-)
(Flag125prime-)
)

)
(:action Flag125Action-15
:parameters ()
:precondition
(and
(Flag51-)
(Flag51prime-)
)
:effect
(and
(Flag125-)
(Flag125prime-)
)

)
(:action Flag125Action-16
:parameters ()
:precondition
(and
(Flag85-)
(Flag85prime-)
)
:effect
(and
(Flag125-)
(Flag125prime-)
)

)
(:action Flag125Action-17
:parameters ()
:precondition
(and
(Flag55-)
(Flag55prime-)
)
:effect
(and
(Flag125-)
(Flag125prime-)
)

)
(:action Flag125Action-18
:parameters ()
:precondition
(and
(Flag74-)
(Flag74prime-)
)
:effect
(and
(Flag125-)
(Flag125prime-)
)

)
(:action Flag125Action-19
:parameters ()
:precondition
(and
(Flag114-)
(Flag114prime-)
)
:effect
(and
(Flag125-)
(Flag125prime-)
)

)
(:action Flag125Action-20
:parameters ()
:precondition
(and
(Flag56-)
(Flag56prime-)
)
:effect
(and
(Flag125-)
(Flag125prime-)
)

)
(:action Flag125Action-21
:parameters ()
:precondition
(and
(Flag95-)
(Flag95prime-)
)
:effect
(and
(Flag125-)
(Flag125prime-)
)

)
(:action Flag125Action-22
:parameters ()
:precondition
(and
(Flag115-)
(Flag115prime-)
)
:effect
(and
(Flag125-)
(Flag125prime-)
)

)
(:action Flag125Action-23
:parameters ()
:precondition
(and
(Flag86-)
(Flag86prime-)
)
:effect
(and
(Flag125-)
(Flag125prime-)
)

)
(:action Flag125Action-24
:parameters ()
:precondition
(and
(Flag71-)
(Flag71prime-)
)
:effect
(and
(Flag125-)
(Flag125prime-)
)

)
(:action Flag125Action-25
:parameters ()
:precondition
(and
(Flag45-)
(Flag45prime-)
)
:effect
(and
(Flag125-)
(Flag125prime-)
)

)
(:action Flag125Action-26
:parameters ()
:precondition
(and
(Flag87-)
(Flag87prime-)
)
:effect
(and
(Flag125-)
(Flag125prime-)
)

)
(:action Flag125Action-27
:parameters ()
:precondition
(and
(Flag116-)
(Flag116prime-)
)
:effect
(and
(Flag125-)
(Flag125prime-)
)

)
(:action Flag125Action-28
:parameters ()
:precondition
(and
(Flag72-)
(Flag72prime-)
)
:effect
(and
(Flag125-)
(Flag125prime-)
)

)
(:action Flag125Action-29
:parameters ()
:precondition
(and
(Flag117-)
(Flag117prime-)
)
:effect
(and
(Flag125-)
(Flag125prime-)
)

)
(:action Flag125Action-30
:parameters ()
:precondition
(and
(Flag105-)
(Flag105prime-)
)
:effect
(and
(Flag125-)
(Flag125prime-)
)

)
(:action Flag125Action-31
:parameters ()
:precondition
(and
(Flag57-)
(Flag57prime-)
)
:effect
(and
(Flag125-)
(Flag125prime-)
)

)
(:action Flag125Action-32
:parameters ()
:precondition
(and
(Flag106-)
(Flag106prime-)
)
:effect
(and
(Flag125-)
(Flag125prime-)
)

)
(:action Flag125Action-33
:parameters ()
:precondition
(and
(Flag107-)
(Flag107prime-)
)
:effect
(and
(Flag125-)
(Flag125prime-)
)

)
(:action Flag125Action-34
:parameters ()
:precondition
(and
(Flag60-)
(Flag60prime-)
)
:effect
(and
(Flag125-)
(Flag125prime-)
)

)
(:action Flag125Action-35
:parameters ()
:precondition
(and
(Flag35-)
(Flag35prime-)
)
:effect
(and
(Flag125-)
(Flag125prime-)
)

)
(:action Flag125Action-36
:parameters ()
:precondition
(and
(Flag73-)
(Flag73prime-)
)
:effect
(and
(Flag125-)
(Flag125prime-)
)

)
(:action Flag125Action-37
:parameters ()
:precondition
(and
(Flag108-)
(Flag108prime-)
)
:effect
(and
(Flag125-)
(Flag125prime-)
)

)
(:action Flag125Action-38
:parameters ()
:precondition
(and
(Flag36-)
(Flag36prime-)
)
:effect
(and
(Flag125-)
(Flag125prime-)
)

)
(:action Flag125Action-39
:parameters ()
:precondition
(and
(Flag61-)
(Flag61prime-)
)
:effect
(and
(Flag125-)
(Flag125prime-)
)

)
(:action Flag125Action-40
:parameters ()
:precondition
(and
(Flag89-)
(Flag89prime-)
)
:effect
(and
(Flag125-)
(Flag125prime-)
)

)
(:action Flag125Action-41
:parameters ()
:precondition
(and
(Flag109-)
(Flag109prime-)
)
:effect
(and
(Flag125-)
(Flag125prime-)
)

)
(:action Flag125Action-42
:parameters ()
:precondition
(and
(Flag110-)
(Flag110prime-)
)
:effect
(and
(Flag125-)
(Flag125prime-)
)

)
(:action Flag125Action-43
:parameters ()
:precondition
(and
(Flag59-)
(Flag59prime-)
)
:effect
(and
(Flag125-)
(Flag125prime-)
)

)
(:action Flag125Action-44
:parameters ()
:precondition
(and
(Flag75-)
(Flag75prime-)
)
:effect
(and
(Flag125-)
(Flag125prime-)
)

)
(:action Flag125Action-45
:parameters ()
:precondition
(and
(Flag118-)
(Flag118prime-)
)
:effect
(and
(Flag125-)
(Flag125prime-)
)

)
(:action Flag125Action-46
:parameters ()
:precondition
(and
(Flag76-)
(Flag76prime-)
)
:effect
(and
(Flag125-)
(Flag125prime-)
)

)
(:action Flag125Action-47
:parameters ()
:precondition
(and
(Flag46-)
(Flag46prime-)
)
:effect
(and
(Flag125-)
(Flag125prime-)
)

)
(:action Flag125Action-48
:parameters ()
:precondition
(and
(Flag78-)
(Flag78prime-)
)
:effect
(and
(Flag125-)
(Flag125prime-)
)

)
(:action Flag125Action-49
:parameters ()
:precondition
(and
(Flag90-)
(Flag90prime-)
)
:effect
(and
(Flag125-)
(Flag125prime-)
)

)
(:action Flag125Action-50
:parameters ()
:precondition
(and
(Flag91-)
(Flag91prime-)
)
:effect
(and
(Flag125-)
(Flag125prime-)
)

)
(:action Flag125Action-51
:parameters ()
:precondition
(and
(Flag119-)
(Flag119prime-)
)
:effect
(and
(Flag125-)
(Flag125prime-)
)

)
(:action Flag125Action-52
:parameters ()
:precondition
(and
(Flag79-)
(Flag79prime-)
)
:effect
(and
(Flag125-)
(Flag125prime-)
)

)
(:action Flag125Action-53
:parameters ()
:precondition
(and
(Flag120-)
(Flag120prime-)
)
:effect
(and
(Flag125-)
(Flag125prime-)
)

)
(:action Flag125Action-54
:parameters ()
:precondition
(and
(Flag121-)
(Flag121prime-)
)
:effect
(and
(Flag125-)
(Flag125prime-)
)

)
(:action Flag125Action-55
:parameters ()
:precondition
(and
(Flag92-)
(Flag92prime-)
)
:effect
(and
(Flag125-)
(Flag125prime-)
)

)
(:action Flag125Action-56
:parameters ()
:precondition
(and
(Flag38-)
(Flag38prime-)
)
:effect
(and
(Flag125-)
(Flag125prime-)
)

)
(:action Flag125Action-57
:parameters ()
:precondition
(and
(Flag122-)
(Flag122prime-)
)
:effect
(and
(Flag125-)
(Flag125prime-)
)

)
(:action Flag125Action-58
:parameters ()
:precondition
(and
(Flag93-)
(Flag93prime-)
)
:effect
(and
(Flag125-)
(Flag125prime-)
)

)
(:action Flag125Action-59
:parameters ()
:precondition
(and
(Flag94-)
(Flag94prime-)
)
:effect
(and
(Flag125-)
(Flag125prime-)
)

)
(:action Flag125Action-60
:parameters ()
:precondition
(and
(Flag63-)
(Flag63prime-)
)
:effect
(and
(Flag125-)
(Flag125prime-)
)

)
(:action Flag125Action-61
:parameters ()
:precondition
(and
(Flag123-)
(Flag123prime-)
)
:effect
(and
(Flag125-)
(Flag125prime-)
)

)
(:action Flag125Action-62
:parameters ()
:precondition
(and
(Flag111-)
(Flag111prime-)
)
:effect
(and
(Flag125-)
(Flag125prime-)
)

)
(:action Flag125Action-63
:parameters ()
:precondition
(and
(Flag47-)
(Flag47prime-)
)
:effect
(and
(Flag125-)
(Flag125prime-)
)

)
(:action Flag125Action-64
:parameters ()
:precondition
(and
(Flag112-)
(Flag112prime-)
)
:effect
(and
(Flag125-)
(Flag125prime-)
)

)
(:action Flag125Action-65
:parameters ()
:precondition
(and
(Flag97-)
(Flag97prime-)
)
:effect
(and
(Flag125-)
(Flag125prime-)
)

)
(:action Flag125Action-66
:parameters ()
:precondition
(and
(Flag65-)
(Flag65prime-)
)
:effect
(and
(Flag125-)
(Flag125prime-)
)

)
(:action Flag125Action-67
:parameters ()
:precondition
(and
(Flag103-)
(Flag103prime-)
)
:effect
(and
(Flag125-)
(Flag125prime-)
)

)
(:action Flag125Action-68
:parameters ()
:precondition
(and
(Flag124-)
(Flag124prime-)
)
:effect
(and
(Flag125-)
(Flag125prime-)
)

)
(:action Prim125Action
:parameters ()
:precondition
(and
(not (Flag62-))
(Flag62prime-)
(not (Flag100-))
(Flag100prime-)
(not (Flag52-))
(Flag52prime-)
(not (Flag99-))
(Flag99prime-)
(not (Flag53-))
(Flag53prime-)
(not (Flag102-))
(Flag102prime-)
(not (Flag37-))
(Flag37prime-)
(not (Flag69-))
(Flag69prime-)
(not (Flag48-))
(Flag48prime-)
(not (Flag83-))
(Flag83prime-)
(not (Flag80-))
(Flag80prime-)
(not (Flag41-))
(Flag41prime-)
(not (Flag49-))
(Flag49prime-)
(not (Flag104-))
(Flag104prime-)
(not (Flag68-))
(Flag68prime-)
(not (Flag51-))
(Flag51prime-)
(not (Flag85-))
(Flag85prime-)
(not (Flag55-))
(Flag55prime-)
(not (Flag74-))
(Flag74prime-)
(not (Flag114-))
(Flag114prime-)
(not (Flag56-))
(Flag56prime-)
(not (Flag95-))
(Flag95prime-)
(not (Flag115-))
(Flag115prime-)
(not (Flag86-))
(Flag86prime-)
(not (Flag71-))
(Flag71prime-)
(not (Flag45-))
(Flag45prime-)
(not (Flag87-))
(Flag87prime-)
(not (Flag116-))
(Flag116prime-)
(not (Flag72-))
(Flag72prime-)
(not (Flag117-))
(Flag117prime-)
(not (Flag105-))
(Flag105prime-)
(not (Flag57-))
(Flag57prime-)
(not (Flag106-))
(Flag106prime-)
(not (Flag107-))
(Flag107prime-)
(not (Flag60-))
(Flag60prime-)
(not (Flag35-))
(Flag35prime-)
(not (Flag73-))
(Flag73prime-)
(not (Flag108-))
(Flag108prime-)
(not (Flag36-))
(Flag36prime-)
(not (Flag61-))
(Flag61prime-)
(not (Flag89-))
(Flag89prime-)
(not (Flag109-))
(Flag109prime-)
(not (Flag110-))
(Flag110prime-)
(not (Flag59-))
(Flag59prime-)
(not (Flag75-))
(Flag75prime-)
(not (Flag118-))
(Flag118prime-)
(not (Flag76-))
(Flag76prime-)
(not (Flag46-))
(Flag46prime-)
(not (Flag78-))
(Flag78prime-)
(not (Flag90-))
(Flag90prime-)
(not (Flag91-))
(Flag91prime-)
(not (Flag119-))
(Flag119prime-)
(not (Flag79-))
(Flag79prime-)
(not (Flag120-))
(Flag120prime-)
(not (Flag121-))
(Flag121prime-)
(not (Flag92-))
(Flag92prime-)
(not (Flag38-))
(Flag38prime-)
(not (Flag122-))
(Flag122prime-)
(not (Flag93-))
(Flag93prime-)
(not (Flag94-))
(Flag94prime-)
(not (Flag63-))
(Flag63prime-)
(not (Flag123-))
(Flag123prime-)
(not (Flag111-))
(Flag111prime-)
(not (Flag47-))
(Flag47prime-)
(not (Flag112-))
(Flag112prime-)
(not (Flag97-))
(Flag97prime-)
(not (Flag65-))
(Flag65prime-)
(not (Flag103-))
(Flag103prime-)
(not (Flag124-))
(Flag124prime-)
)
:effect
(and
(Flag125prime-)
(not (Flag125-))
)

)
(:action Flag138Action-0
:parameters ()
:precondition
(and
(Flag62-)
(Flag62prime-)
)
:effect
(and
(Flag138-)
(Flag138prime-)
)

)
(:action Flag138Action-1
:parameters ()
:precondition
(and
(Flag100-)
(Flag100prime-)
)
:effect
(and
(Flag138-)
(Flag138prime-)
)

)
(:action Flag138Action-2
:parameters ()
:precondition
(and
(Flag52-)
(Flag52prime-)
)
:effect
(and
(Flag138-)
(Flag138prime-)
)

)
(:action Flag138Action-3
:parameters ()
:precondition
(and
(Flag99-)
(Flag99prime-)
)
:effect
(and
(Flag138-)
(Flag138prime-)
)

)
(:action Flag138Action-4
:parameters ()
:precondition
(and
(Flag53-)
(Flag53prime-)
)
:effect
(and
(Flag138-)
(Flag138prime-)
)

)
(:action Flag138Action-5
:parameters ()
:precondition
(and
(Flag102-)
(Flag102prime-)
)
:effect
(and
(Flag138-)
(Flag138prime-)
)

)
(:action Flag138Action-6
:parameters ()
:precondition
(and
(Flag37-)
(Flag37prime-)
)
:effect
(and
(Flag138-)
(Flag138prime-)
)

)
(:action Flag138Action-7
:parameters ()
:precondition
(and
(Flag126-)
(Flag126prime-)
)
:effect
(and
(Flag138-)
(Flag138prime-)
)

)
(:action Flag138Action-8
:parameters ()
:precondition
(and
(Flag93-)
(Flag93prime-)
)
:effect
(and
(Flag138-)
(Flag138prime-)
)

)
(:action Flag138Action-9
:parameters ()
:precondition
(and
(Flag127-)
(Flag127prime-)
)
:effect
(and
(Flag138-)
(Flag138prime-)
)

)
(:action Flag138Action-10
:parameters ()
:precondition
(and
(Flag128-)
(Flag128prime-)
)
:effect
(and
(Flag138-)
(Flag138prime-)
)

)
(:action Flag138Action-11
:parameters ()
:precondition
(and
(Flag83-)
(Flag83prime-)
)
:effect
(and
(Flag138-)
(Flag138prime-)
)

)
(:action Flag138Action-12
:parameters ()
:precondition
(and
(Flag80-)
(Flag80prime-)
)
:effect
(and
(Flag138-)
(Flag138prime-)
)

)
(:action Flag138Action-13
:parameters ()
:precondition
(and
(Flag41-)
(Flag41prime-)
)
:effect
(and
(Flag138-)
(Flag138prime-)
)

)
(:action Flag138Action-14
:parameters ()
:precondition
(and
(Flag49-)
(Flag49prime-)
)
:effect
(and
(Flag138-)
(Flag138prime-)
)

)
(:action Flag138Action-15
:parameters ()
:precondition
(and
(Flag35-)
(Flag35prime-)
)
:effect
(and
(Flag138-)
(Flag138prime-)
)

)
(:action Flag138Action-16
:parameters ()
:precondition
(and
(Flag104-)
(Flag104prime-)
)
:effect
(and
(Flag138-)
(Flag138prime-)
)

)
(:action Flag138Action-17
:parameters ()
:precondition
(and
(Flag68-)
(Flag68prime-)
)
:effect
(and
(Flag138-)
(Flag138prime-)
)

)
(:action Flag138Action-18
:parameters ()
:precondition
(and
(Flag51-)
(Flag51prime-)
)
:effect
(and
(Flag138-)
(Flag138prime-)
)

)
(:action Flag138Action-19
:parameters ()
:precondition
(and
(Flag85-)
(Flag85prime-)
)
:effect
(and
(Flag138-)
(Flag138prime-)
)

)
(:action Flag138Action-20
:parameters ()
:precondition
(and
(Flag108-)
(Flag108prime-)
)
:effect
(and
(Flag138-)
(Flag138prime-)
)

)
(:action Flag138Action-21
:parameters ()
:precondition
(and
(Flag114-)
(Flag114prime-)
)
:effect
(and
(Flag138-)
(Flag138prime-)
)

)
(:action Flag138Action-22
:parameters ()
:precondition
(and
(Flag129-)
(Flag129prime-)
)
:effect
(and
(Flag138-)
(Flag138prime-)
)

)
(:action Flag138Action-23
:parameters ()
:precondition
(and
(Flag56-)
(Flag56prime-)
)
:effect
(and
(Flag138-)
(Flag138prime-)
)

)
(:action Flag138Action-24
:parameters ()
:precondition
(and
(Flag69-)
(Flag69prime-)
)
:effect
(and
(Flag138-)
(Flag138prime-)
)

)
(:action Flag138Action-25
:parameters ()
:precondition
(and
(Flag95-)
(Flag95prime-)
)
:effect
(and
(Flag138-)
(Flag138prime-)
)

)
(:action Flag138Action-26
:parameters ()
:precondition
(and
(Flag115-)
(Flag115prime-)
)
:effect
(and
(Flag138-)
(Flag138prime-)
)

)
(:action Flag138Action-27
:parameters ()
:precondition
(and
(Flag86-)
(Flag86prime-)
)
:effect
(and
(Flag138-)
(Flag138prime-)
)

)
(:action Flag138Action-28
:parameters ()
:precondition
(and
(Flag71-)
(Flag71prime-)
)
:effect
(and
(Flag138-)
(Flag138prime-)
)

)
(:action Flag138Action-29
:parameters ()
:precondition
(and
(Flag45-)
(Flag45prime-)
)
:effect
(and
(Flag138-)
(Flag138prime-)
)

)
(:action Flag138Action-30
:parameters ()
:precondition
(and
(Flag87-)
(Flag87prime-)
)
:effect
(and
(Flag138-)
(Flag138prime-)
)

)
(:action Flag138Action-31
:parameters ()
:precondition
(and
(Flag116-)
(Flag116prime-)
)
:effect
(and
(Flag138-)
(Flag138prime-)
)

)
(:action Flag138Action-32
:parameters ()
:precondition
(and
(Flag72-)
(Flag72prime-)
)
:effect
(and
(Flag138-)
(Flag138prime-)
)

)
(:action Flag138Action-33
:parameters ()
:precondition
(and
(Flag117-)
(Flag117prime-)
)
:effect
(and
(Flag138-)
(Flag138prime-)
)

)
(:action Flag138Action-34
:parameters ()
:precondition
(and
(Flag105-)
(Flag105prime-)
)
:effect
(and
(Flag138-)
(Flag138prime-)
)

)
(:action Flag138Action-35
:parameters ()
:precondition
(and
(Flag106-)
(Flag106prime-)
)
:effect
(and
(Flag138-)
(Flag138prime-)
)

)
(:action Flag138Action-36
:parameters ()
:precondition
(and
(Flag107-)
(Flag107prime-)
)
:effect
(and
(Flag138-)
(Flag138prime-)
)

)
(:action Flag138Action-37
:parameters ()
:precondition
(and
(Flag60-)
(Flag60prime-)
)
:effect
(and
(Flag138-)
(Flag138prime-)
)

)
(:action Flag138Action-38
:parameters ()
:precondition
(and
(Flag130-)
(Flag130prime-)
)
:effect
(and
(Flag138-)
(Flag138prime-)
)

)
(:action Flag138Action-39
:parameters ()
:precondition
(and
(Flag73-)
(Flag73prime-)
)
:effect
(and
(Flag138-)
(Flag138prime-)
)

)
(:action Flag138Action-40
:parameters ()
:precondition
(and
(Flag74-)
(Flag74prime-)
)
:effect
(and
(Flag138-)
(Flag138prime-)
)

)
(:action Flag138Action-41
:parameters ()
:precondition
(and
(Flag36-)
(Flag36prime-)
)
:effect
(and
(Flag138-)
(Flag138prime-)
)

)
(:action Flag138Action-42
:parameters ()
:precondition
(and
(Flag61-)
(Flag61prime-)
)
:effect
(and
(Flag138-)
(Flag138prime-)
)

)
(:action Flag138Action-43
:parameters ()
:precondition
(and
(Flag131-)
(Flag131prime-)
)
:effect
(and
(Flag138-)
(Flag138prime-)
)

)
(:action Flag138Action-44
:parameters ()
:precondition
(and
(Flag109-)
(Flag109prime-)
)
:effect
(and
(Flag138-)
(Flag138prime-)
)

)
(:action Flag138Action-45
:parameters ()
:precondition
(and
(Flag76-)
(Flag76prime-)
)
:effect
(and
(Flag138-)
(Flag138prime-)
)

)
(:action Flag138Action-46
:parameters ()
:precondition
(and
(Flag110-)
(Flag110prime-)
)
:effect
(and
(Flag138-)
(Flag138prime-)
)

)
(:action Flag138Action-47
:parameters ()
:precondition
(and
(Flag132-)
(Flag132prime-)
)
:effect
(and
(Flag138-)
(Flag138prime-)
)

)
(:action Flag138Action-48
:parameters ()
:precondition
(and
(Flag59-)
(Flag59prime-)
)
:effect
(and
(Flag138-)
(Flag138prime-)
)

)
(:action Flag138Action-49
:parameters ()
:precondition
(and
(Flag89-)
(Flag89prime-)
)
:effect
(and
(Flag138-)
(Flag138prime-)
)

)
(:action Flag138Action-50
:parameters ()
:precondition
(and
(Flag46-)
(Flag46prime-)
)
:effect
(and
(Flag138-)
(Flag138prime-)
)

)
(:action Flag138Action-51
:parameters ()
:precondition
(and
(Flag78-)
(Flag78prime-)
)
:effect
(and
(Flag138-)
(Flag138prime-)
)

)
(:action Flag138Action-52
:parameters ()
:precondition
(and
(Flag90-)
(Flag90prime-)
)
:effect
(and
(Flag138-)
(Flag138prime-)
)

)
(:action Flag138Action-53
:parameters ()
:precondition
(and
(Flag91-)
(Flag91prime-)
)
:effect
(and
(Flag138-)
(Flag138prime-)
)

)
(:action Flag138Action-54
:parameters ()
:precondition
(and
(Flag119-)
(Flag119prime-)
)
:effect
(and
(Flag138-)
(Flag138prime-)
)

)
(:action Flag138Action-55
:parameters ()
:precondition
(and
(Flag79-)
(Flag79prime-)
)
:effect
(and
(Flag138-)
(Flag138prime-)
)

)
(:action Flag138Action-56
:parameters ()
:precondition
(and
(Flag133-)
(Flag133prime-)
)
:effect
(and
(Flag138-)
(Flag138prime-)
)

)
(:action Flag138Action-57
:parameters ()
:precondition
(and
(Flag134-)
(Flag134prime-)
)
:effect
(and
(Flag138-)
(Flag138prime-)
)

)
(:action Flag138Action-58
:parameters ()
:precondition
(and
(Flag135-)
(Flag135prime-)
)
:effect
(and
(Flag138-)
(Flag138prime-)
)

)
(:action Flag138Action-59
:parameters ()
:precondition
(and
(Flag120-)
(Flag120prime-)
)
:effect
(and
(Flag138-)
(Flag138prime-)
)

)
(:action Flag138Action-60
:parameters ()
:precondition
(and
(Flag92-)
(Flag92prime-)
)
:effect
(and
(Flag138-)
(Flag138prime-)
)

)
(:action Flag138Action-61
:parameters ()
:precondition
(and
(Flag38-)
(Flag38prime-)
)
:effect
(and
(Flag138-)
(Flag138prime-)
)

)
(:action Flag138Action-62
:parameters ()
:precondition
(and
(Flag122-)
(Flag122prime-)
)
:effect
(and
(Flag138-)
(Flag138prime-)
)

)
(:action Flag138Action-63
:parameters ()
:precondition
(and
(Flag136-)
(Flag136prime-)
)
:effect
(and
(Flag138-)
(Flag138prime-)
)

)
(:action Flag138Action-64
:parameters ()
:precondition
(and
(Flag63-)
(Flag63prime-)
)
:effect
(and
(Flag138-)
(Flag138prime-)
)

)
(:action Flag138Action-65
:parameters ()
:precondition
(and
(Flag123-)
(Flag123prime-)
)
:effect
(and
(Flag138-)
(Flag138prime-)
)

)
(:action Flag138Action-66
:parameters ()
:precondition
(and
(Flag111-)
(Flag111prime-)
)
:effect
(and
(Flag138-)
(Flag138prime-)
)

)
(:action Flag138Action-67
:parameters ()
:precondition
(and
(Flag47-)
(Flag47prime-)
)
:effect
(and
(Flag138-)
(Flag138prime-)
)

)
(:action Flag138Action-68
:parameters ()
:precondition
(and
(Flag48-)
(Flag48prime-)
)
:effect
(and
(Flag138-)
(Flag138prime-)
)

)
(:action Flag138Action-69
:parameters ()
:precondition
(and
(Flag97-)
(Flag97prime-)
)
:effect
(and
(Flag138-)
(Flag138prime-)
)

)
(:action Flag138Action-70
:parameters ()
:precondition
(and
(Flag65-)
(Flag65prime-)
)
:effect
(and
(Flag138-)
(Flag138prime-)
)

)
(:action Flag138Action-71
:parameters ()
:precondition
(and
(Flag103-)
(Flag103prime-)
)
:effect
(and
(Flag138-)
(Flag138prime-)
)

)
(:action Flag138Action-72
:parameters ()
:precondition
(and
(Flag124-)
(Flag124prime-)
)
:effect
(and
(Flag138-)
(Flag138prime-)
)

)
(:action Flag138Action-73
:parameters ()
:precondition
(and
(Flag137-)
(Flag137prime-)
)
:effect
(and
(Flag138-)
(Flag138prime-)
)

)
(:action Prim138Action
:parameters ()
:precondition
(and
(not (Flag62-))
(Flag62prime-)
(not (Flag100-))
(Flag100prime-)
(not (Flag52-))
(Flag52prime-)
(not (Flag99-))
(Flag99prime-)
(not (Flag53-))
(Flag53prime-)
(not (Flag102-))
(Flag102prime-)
(not (Flag37-))
(Flag37prime-)
(not (Flag126-))
(Flag126prime-)
(not (Flag93-))
(Flag93prime-)
(not (Flag127-))
(Flag127prime-)
(not (Flag128-))
(Flag128prime-)
(not (Flag83-))
(Flag83prime-)
(not (Flag80-))
(Flag80prime-)
(not (Flag41-))
(Flag41prime-)
(not (Flag49-))
(Flag49prime-)
(not (Flag35-))
(Flag35prime-)
(not (Flag104-))
(Flag104prime-)
(not (Flag68-))
(Flag68prime-)
(not (Flag51-))
(Flag51prime-)
(not (Flag85-))
(Flag85prime-)
(not (Flag108-))
(Flag108prime-)
(not (Flag114-))
(Flag114prime-)
(not (Flag129-))
(Flag129prime-)
(not (Flag56-))
(Flag56prime-)
(not (Flag69-))
(Flag69prime-)
(not (Flag95-))
(Flag95prime-)
(not (Flag115-))
(Flag115prime-)
(not (Flag86-))
(Flag86prime-)
(not (Flag71-))
(Flag71prime-)
(not (Flag45-))
(Flag45prime-)
(not (Flag87-))
(Flag87prime-)
(not (Flag116-))
(Flag116prime-)
(not (Flag72-))
(Flag72prime-)
(not (Flag117-))
(Flag117prime-)
(not (Flag105-))
(Flag105prime-)
(not (Flag106-))
(Flag106prime-)
(not (Flag107-))
(Flag107prime-)
(not (Flag60-))
(Flag60prime-)
(not (Flag130-))
(Flag130prime-)
(not (Flag73-))
(Flag73prime-)
(not (Flag74-))
(Flag74prime-)
(not (Flag36-))
(Flag36prime-)
(not (Flag61-))
(Flag61prime-)
(not (Flag131-))
(Flag131prime-)
(not (Flag109-))
(Flag109prime-)
(not (Flag76-))
(Flag76prime-)
(not (Flag110-))
(Flag110prime-)
(not (Flag132-))
(Flag132prime-)
(not (Flag59-))
(Flag59prime-)
(not (Flag89-))
(Flag89prime-)
(not (Flag46-))
(Flag46prime-)
(not (Flag78-))
(Flag78prime-)
(not (Flag90-))
(Flag90prime-)
(not (Flag91-))
(Flag91prime-)
(not (Flag119-))
(Flag119prime-)
(not (Flag79-))
(Flag79prime-)
(not (Flag133-))
(Flag133prime-)
(not (Flag134-))
(Flag134prime-)
(not (Flag135-))
(Flag135prime-)
(not (Flag120-))
(Flag120prime-)
(not (Flag92-))
(Flag92prime-)
(not (Flag38-))
(Flag38prime-)
(not (Flag122-))
(Flag122prime-)
(not (Flag136-))
(Flag136prime-)
(not (Flag63-))
(Flag63prime-)
(not (Flag123-))
(Flag123prime-)
(not (Flag111-))
(Flag111prime-)
(not (Flag47-))
(Flag47prime-)
(not (Flag48-))
(Flag48prime-)
(not (Flag97-))
(Flag97prime-)
(not (Flag65-))
(Flag65prime-)
(not (Flag103-))
(Flag103prime-)
(not (Flag124-))
(Flag124prime-)
(not (Flag137-))
(Flag137prime-)
)
:effect
(and
(Flag138prime-)
(not (Flag138-))
)

)
(:action Flag149Action-0
:parameters ()
:precondition
(and
(Flag62-)
(Flag62prime-)
)
:effect
(and
(Flag149-)
(Flag149prime-)
)

)
(:action Flag149Action-1
:parameters ()
:precondition
(and
(Flag100-)
(Flag100prime-)
)
:effect
(and
(Flag149-)
(Flag149prime-)
)

)
(:action Flag149Action-2
:parameters ()
:precondition
(and
(Flag52-)
(Flag52prime-)
)
:effect
(and
(Flag149-)
(Flag149prime-)
)

)
(:action Flag149Action-3
:parameters ()
:precondition
(and
(Flag99-)
(Flag99prime-)
)
:effect
(and
(Flag149-)
(Flag149prime-)
)

)
(:action Flag149Action-4
:parameters ()
:precondition
(and
(Flag139-)
(Flag139prime-)
)
:effect
(and
(Flag149-)
(Flag149prime-)
)

)
(:action Flag149Action-5
:parameters ()
:precondition
(and
(Flag102-)
(Flag102prime-)
)
:effect
(and
(Flag149-)
(Flag149prime-)
)

)
(:action Flag149Action-6
:parameters ()
:precondition
(and
(Flag37-)
(Flag37prime-)
)
:effect
(and
(Flag149-)
(Flag149prime-)
)

)
(:action Flag149Action-7
:parameters ()
:precondition
(and
(Flag107-)
(Flag107prime-)
)
:effect
(and
(Flag149-)
(Flag149prime-)
)

)
(:action Flag149Action-8
:parameters ()
:precondition
(and
(Flag126-)
(Flag126prime-)
)
:effect
(and
(Flag149-)
(Flag149prime-)
)

)
(:action Flag149Action-9
:parameters ()
:precondition
(and
(Flag69-)
(Flag69prime-)
)
:effect
(and
(Flag149-)
(Flag149prime-)
)

)
(:action Flag149Action-10
:parameters ()
:precondition
(and
(Flag127-)
(Flag127prime-)
)
:effect
(and
(Flag149-)
(Flag149prime-)
)

)
(:action Flag149Action-11
:parameters ()
:precondition
(and
(Flag83-)
(Flag83prime-)
)
:effect
(and
(Flag149-)
(Flag149prime-)
)

)
(:action Flag149Action-12
:parameters ()
:precondition
(and
(Flag60-)
(Flag60prime-)
)
:effect
(and
(Flag149-)
(Flag149prime-)
)

)
(:action Flag149Action-13
:parameters ()
:precondition
(and
(Flag140-)
(Flag140prime-)
)
:effect
(and
(Flag149-)
(Flag149prime-)
)

)
(:action Flag149Action-14
:parameters ()
:precondition
(and
(Flag41-)
(Flag41prime-)
)
:effect
(and
(Flag149-)
(Flag149prime-)
)

)
(:action Flag149Action-15
:parameters ()
:precondition
(and
(Flag49-)
(Flag49prime-)
)
:effect
(and
(Flag149-)
(Flag149prime-)
)

)
(:action Flag149Action-16
:parameters ()
:precondition
(and
(Flag103-)
(Flag103prime-)
)
:effect
(and
(Flag149-)
(Flag149prime-)
)

)
(:action Flag149Action-17
:parameters ()
:precondition
(and
(Flag53-)
(Flag53prime-)
)
:effect
(and
(Flag149-)
(Flag149prime-)
)

)
(:action Flag149Action-18
:parameters ()
:precondition
(and
(Flag141-)
(Flag141prime-)
)
:effect
(and
(Flag149-)
(Flag149prime-)
)

)
(:action Flag149Action-19
:parameters ()
:precondition
(and
(Flag104-)
(Flag104prime-)
)
:effect
(and
(Flag149-)
(Flag149prime-)
)

)
(:action Flag149Action-20
:parameters ()
:precondition
(and
(Flag51-)
(Flag51prime-)
)
:effect
(and
(Flag149-)
(Flag149prime-)
)

)
(:action Flag149Action-21
:parameters ()
:precondition
(and
(Flag85-)
(Flag85prime-)
)
:effect
(and
(Flag149-)
(Flag149prime-)
)

)
(:action Flag149Action-22
:parameters ()
:precondition
(and
(Flag142-)
(Flag142prime-)
)
:effect
(and
(Flag149-)
(Flag149prime-)
)

)
(:action Flag149Action-23
:parameters ()
:precondition
(and
(Flag108-)
(Flag108prime-)
)
:effect
(and
(Flag149-)
(Flag149prime-)
)

)
(:action Flag149Action-24
:parameters ()
:precondition
(and
(Flag114-)
(Flag114prime-)
)
:effect
(and
(Flag149-)
(Flag149prime-)
)

)
(:action Flag149Action-25
:parameters ()
:precondition
(and
(Flag129-)
(Flag129prime-)
)
:effect
(and
(Flag149-)
(Flag149prime-)
)

)
(:action Flag149Action-26
:parameters ()
:precondition
(and
(Flag36-)
(Flag36prime-)
)
:effect
(and
(Flag149-)
(Flag149prime-)
)

)
(:action Flag149Action-27
:parameters ()
:precondition
(and
(Flag95-)
(Flag95prime-)
)
:effect
(and
(Flag149-)
(Flag149prime-)
)

)
(:action Flag149Action-28
:parameters ()
:precondition
(and
(Flag115-)
(Flag115prime-)
)
:effect
(and
(Flag149-)
(Flag149prime-)
)

)
(:action Flag149Action-29
:parameters ()
:precondition
(and
(Flag86-)
(Flag86prime-)
)
:effect
(and
(Flag149-)
(Flag149prime-)
)

)
(:action Flag149Action-30
:parameters ()
:precondition
(and
(Flag71-)
(Flag71prime-)
)
:effect
(and
(Flag149-)
(Flag149prime-)
)

)
(:action Flag149Action-31
:parameters ()
:precondition
(and
(Flag45-)
(Flag45prime-)
)
:effect
(and
(Flag149-)
(Flag149prime-)
)

)
(:action Flag149Action-32
:parameters ()
:precondition
(and
(Flag87-)
(Flag87prime-)
)
:effect
(and
(Flag149-)
(Flag149prime-)
)

)
(:action Flag149Action-33
:parameters ()
:precondition
(and
(Flag116-)
(Flag116prime-)
)
:effect
(and
(Flag149-)
(Flag149prime-)
)

)
(:action Flag149Action-34
:parameters ()
:precondition
(and
(Flag72-)
(Flag72prime-)
)
:effect
(and
(Flag149-)
(Flag149prime-)
)

)
(:action Flag149Action-35
:parameters ()
:precondition
(and
(Flag117-)
(Flag117prime-)
)
:effect
(and
(Flag149-)
(Flag149prime-)
)

)
(:action Flag149Action-36
:parameters ()
:precondition
(and
(Flag105-)
(Flag105prime-)
)
:effect
(and
(Flag149-)
(Flag149prime-)
)

)
(:action Flag149Action-37
:parameters ()
:precondition
(and
(Flag143-)
(Flag143prime-)
)
:effect
(and
(Flag149-)
(Flag149prime-)
)

)
(:action Flag149Action-38
:parameters ()
:precondition
(and
(Flag144-)
(Flag144prime-)
)
:effect
(and
(Flag149-)
(Flag149prime-)
)

)
(:action Flag149Action-39
:parameters ()
:precondition
(and
(Flag145-)
(Flag145prime-)
)
:effect
(and
(Flag149-)
(Flag149prime-)
)

)
(:action Flag149Action-40
:parameters ()
:precondition
(and
(Flag146-)
(Flag146prime-)
)
:effect
(and
(Flag149-)
(Flag149prime-)
)

)
(:action Flag149Action-41
:parameters ()
:precondition
(and
(Flag35-)
(Flag35prime-)
)
:effect
(and
(Flag149-)
(Flag149prime-)
)

)
(:action Flag149Action-42
:parameters ()
:precondition
(and
(Flag73-)
(Flag73prime-)
)
:effect
(and
(Flag149-)
(Flag149prime-)
)

)
(:action Flag149Action-43
:parameters ()
:precondition
(and
(Flag74-)
(Flag74prime-)
)
:effect
(and
(Flag149-)
(Flag149prime-)
)

)
(:action Flag149Action-44
:parameters ()
:precondition
(and
(Flag56-)
(Flag56prime-)
)
:effect
(and
(Flag149-)
(Flag149prime-)
)

)
(:action Flag149Action-45
:parameters ()
:precondition
(and
(Flag61-)
(Flag61prime-)
)
:effect
(and
(Flag149-)
(Flag149prime-)
)

)
(:action Flag149Action-46
:parameters ()
:precondition
(and
(Flag131-)
(Flag131prime-)
)
:effect
(and
(Flag149-)
(Flag149prime-)
)

)
(:action Flag149Action-47
:parameters ()
:precondition
(and
(Flag109-)
(Flag109prime-)
)
:effect
(and
(Flag149-)
(Flag149prime-)
)

)
(:action Flag149Action-48
:parameters ()
:precondition
(and
(Flag76-)
(Flag76prime-)
)
:effect
(and
(Flag149-)
(Flag149prime-)
)

)
(:action Flag149Action-49
:parameters ()
:precondition
(and
(Flag110-)
(Flag110prime-)
)
:effect
(and
(Flag149-)
(Flag149prime-)
)

)
(:action Flag149Action-50
:parameters ()
:precondition
(and
(Flag132-)
(Flag132prime-)
)
:effect
(and
(Flag149-)
(Flag149prime-)
)

)
(:action Flag149Action-51
:parameters ()
:precondition
(and
(Flag59-)
(Flag59prime-)
)
:effect
(and
(Flag149-)
(Flag149prime-)
)

)
(:action Flag149Action-52
:parameters ()
:precondition
(and
(Flag147-)
(Flag147prime-)
)
:effect
(and
(Flag149-)
(Flag149prime-)
)

)
(:action Flag149Action-53
:parameters ()
:precondition
(and
(Flag89-)
(Flag89prime-)
)
:effect
(and
(Flag149-)
(Flag149prime-)
)

)
(:action Flag149Action-54
:parameters ()
:precondition
(and
(Flag46-)
(Flag46prime-)
)
:effect
(and
(Flag149-)
(Flag149prime-)
)

)
(:action Flag149Action-55
:parameters ()
:precondition
(and
(Flag78-)
(Flag78prime-)
)
:effect
(and
(Flag149-)
(Flag149prime-)
)

)
(:action Flag149Action-56
:parameters ()
:precondition
(and
(Flag90-)
(Flag90prime-)
)
:effect
(and
(Flag149-)
(Flag149prime-)
)

)
(:action Flag149Action-57
:parameters ()
:precondition
(and
(Flag91-)
(Flag91prime-)
)
:effect
(and
(Flag149-)
(Flag149prime-)
)

)
(:action Flag149Action-58
:parameters ()
:precondition
(and
(Flag119-)
(Flag119prime-)
)
:effect
(and
(Flag149-)
(Flag149prime-)
)

)
(:action Flag149Action-59
:parameters ()
:precondition
(and
(Flag79-)
(Flag79prime-)
)
:effect
(and
(Flag149-)
(Flag149prime-)
)

)
(:action Flag149Action-60
:parameters ()
:precondition
(and
(Flag133-)
(Flag133prime-)
)
:effect
(and
(Flag149-)
(Flag149prime-)
)

)
(:action Flag149Action-61
:parameters ()
:precondition
(and
(Flag135-)
(Flag135prime-)
)
:effect
(and
(Flag149-)
(Flag149prime-)
)

)
(:action Flag149Action-62
:parameters ()
:precondition
(and
(Flag120-)
(Flag120prime-)
)
:effect
(and
(Flag149-)
(Flag149prime-)
)

)
(:action Flag149Action-63
:parameters ()
:precondition
(and
(Flag92-)
(Flag92prime-)
)
:effect
(and
(Flag149-)
(Flag149prime-)
)

)
(:action Flag149Action-64
:parameters ()
:precondition
(and
(Flag38-)
(Flag38prime-)
)
:effect
(and
(Flag149-)
(Flag149prime-)
)

)
(:action Flag149Action-65
:parameters ()
:precondition
(and
(Flag122-)
(Flag122prime-)
)
:effect
(and
(Flag149-)
(Flag149prime-)
)

)
(:action Flag149Action-66
:parameters ()
:precondition
(and
(Flag136-)
(Flag136prime-)
)
:effect
(and
(Flag149-)
(Flag149prime-)
)

)
(:action Flag149Action-67
:parameters ()
:precondition
(and
(Flag63-)
(Flag63prime-)
)
:effect
(and
(Flag149-)
(Flag149prime-)
)

)
(:action Flag149Action-68
:parameters ()
:precondition
(and
(Flag123-)
(Flag123prime-)
)
:effect
(and
(Flag149-)
(Flag149prime-)
)

)
(:action Flag149Action-69
:parameters ()
:precondition
(and
(Flag148-)
(Flag148prime-)
)
:effect
(and
(Flag149-)
(Flag149prime-)
)

)
(:action Flag149Action-70
:parameters ()
:precondition
(and
(Flag111-)
(Flag111prime-)
)
:effect
(and
(Flag149-)
(Flag149prime-)
)

)
(:action Flag149Action-71
:parameters ()
:precondition
(and
(Flag48-)
(Flag48prime-)
)
:effect
(and
(Flag149-)
(Flag149prime-)
)

)
(:action Flag149Action-72
:parameters ()
:precondition
(and
(Flag97-)
(Flag97prime-)
)
:effect
(and
(Flag149-)
(Flag149prime-)
)

)
(:action Flag149Action-73
:parameters ()
:precondition
(and
(Flag80-)
(Flag80prime-)
)
:effect
(and
(Flag149-)
(Flag149prime-)
)

)
(:action Flag149Action-74
:parameters ()
:precondition
(and
(Flag124-)
(Flag124prime-)
)
:effect
(and
(Flag149-)
(Flag149prime-)
)

)
(:action Flag149Action-75
:parameters ()
:precondition
(and
(Flag137-)
(Flag137prime-)
)
:effect
(and
(Flag149-)
(Flag149prime-)
)

)
(:action Prim149Action
:parameters ()
:precondition
(and
(not (Flag62-))
(Flag62prime-)
(not (Flag100-))
(Flag100prime-)
(not (Flag52-))
(Flag52prime-)
(not (Flag99-))
(Flag99prime-)
(not (Flag139-))
(Flag139prime-)
(not (Flag102-))
(Flag102prime-)
(not (Flag37-))
(Flag37prime-)
(not (Flag107-))
(Flag107prime-)
(not (Flag126-))
(Flag126prime-)
(not (Flag69-))
(Flag69prime-)
(not (Flag127-))
(Flag127prime-)
(not (Flag83-))
(Flag83prime-)
(not (Flag60-))
(Flag60prime-)
(not (Flag140-))
(Flag140prime-)
(not (Flag41-))
(Flag41prime-)
(not (Flag49-))
(Flag49prime-)
(not (Flag103-))
(Flag103prime-)
(not (Flag53-))
(Flag53prime-)
(not (Flag141-))
(Flag141prime-)
(not (Flag104-))
(Flag104prime-)
(not (Flag51-))
(Flag51prime-)
(not (Flag85-))
(Flag85prime-)
(not (Flag142-))
(Flag142prime-)
(not (Flag108-))
(Flag108prime-)
(not (Flag114-))
(Flag114prime-)
(not (Flag129-))
(Flag129prime-)
(not (Flag36-))
(Flag36prime-)
(not (Flag95-))
(Flag95prime-)
(not (Flag115-))
(Flag115prime-)
(not (Flag86-))
(Flag86prime-)
(not (Flag71-))
(Flag71prime-)
(not (Flag45-))
(Flag45prime-)
(not (Flag87-))
(Flag87prime-)
(not (Flag116-))
(Flag116prime-)
(not (Flag72-))
(Flag72prime-)
(not (Flag117-))
(Flag117prime-)
(not (Flag105-))
(Flag105prime-)
(not (Flag143-))
(Flag143prime-)
(not (Flag144-))
(Flag144prime-)
(not (Flag145-))
(Flag145prime-)
(not (Flag146-))
(Flag146prime-)
(not (Flag35-))
(Flag35prime-)
(not (Flag73-))
(Flag73prime-)
(not (Flag74-))
(Flag74prime-)
(not (Flag56-))
(Flag56prime-)
(not (Flag61-))
(Flag61prime-)
(not (Flag131-))
(Flag131prime-)
(not (Flag109-))
(Flag109prime-)
(not (Flag76-))
(Flag76prime-)
(not (Flag110-))
(Flag110prime-)
(not (Flag132-))
(Flag132prime-)
(not (Flag59-))
(Flag59prime-)
(not (Flag147-))
(Flag147prime-)
(not (Flag89-))
(Flag89prime-)
(not (Flag46-))
(Flag46prime-)
(not (Flag78-))
(Flag78prime-)
(not (Flag90-))
(Flag90prime-)
(not (Flag91-))
(Flag91prime-)
(not (Flag119-))
(Flag119prime-)
(not (Flag79-))
(Flag79prime-)
(not (Flag133-))
(Flag133prime-)
(not (Flag135-))
(Flag135prime-)
(not (Flag120-))
(Flag120prime-)
(not (Flag92-))
(Flag92prime-)
(not (Flag38-))
(Flag38prime-)
(not (Flag122-))
(Flag122prime-)
(not (Flag136-))
(Flag136prime-)
(not (Flag63-))
(Flag63prime-)
(not (Flag123-))
(Flag123prime-)
(not (Flag148-))
(Flag148prime-)
(not (Flag111-))
(Flag111prime-)
(not (Flag48-))
(Flag48prime-)
(not (Flag97-))
(Flag97prime-)
(not (Flag80-))
(Flag80prime-)
(not (Flag124-))
(Flag124prime-)
(not (Flag137-))
(Flag137prime-)
)
:effect
(and
(Flag149prime-)
(not (Flag149-))
)

)
(:action Flag159Action-0
:parameters ()
:precondition
(and
(Flag62-)
(Flag62prime-)
)
:effect
(and
(Flag159-)
(Flag159prime-)
)

)
(:action Flag159Action-1
:parameters ()
:precondition
(and
(Flag100-)
(Flag100prime-)
)
:effect
(and
(Flag159-)
(Flag159prime-)
)

)
(:action Flag159Action-2
:parameters ()
:precondition
(and
(Flag52-)
(Flag52prime-)
)
:effect
(and
(Flag159-)
(Flag159prime-)
)

)
(:action Flag159Action-3
:parameters ()
:precondition
(and
(Flag99-)
(Flag99prime-)
)
:effect
(and
(Flag159-)
(Flag159prime-)
)

)
(:action Flag159Action-4
:parameters ()
:precondition
(and
(Flag97-)
(Flag97prime-)
)
:effect
(and
(Flag159-)
(Flag159prime-)
)

)
(:action Flag159Action-5
:parameters ()
:precondition
(and
(Flag53-)
(Flag53prime-)
)
:effect
(and
(Flag159-)
(Flag159prime-)
)

)
(:action Flag159Action-6
:parameters ()
:precondition
(and
(Flag37-)
(Flag37prime-)
)
:effect
(and
(Flag159-)
(Flag159prime-)
)

)
(:action Flag159Action-7
:parameters ()
:precondition
(and
(Flag107-)
(Flag107prime-)
)
:effect
(and
(Flag159-)
(Flag159prime-)
)

)
(:action Flag159Action-8
:parameters ()
:precondition
(and
(Flag126-)
(Flag126prime-)
)
:effect
(and
(Flag159-)
(Flag159prime-)
)

)
(:action Flag159Action-9
:parameters ()
:precondition
(and
(Flag69-)
(Flag69prime-)
)
:effect
(and
(Flag159-)
(Flag159prime-)
)

)
(:action Flag159Action-10
:parameters ()
:precondition
(and
(Flag127-)
(Flag127prime-)
)
:effect
(and
(Flag159-)
(Flag159prime-)
)

)
(:action Flag159Action-11
:parameters ()
:precondition
(and
(Flag83-)
(Flag83prime-)
)
:effect
(and
(Flag159-)
(Flag159prime-)
)

)
(:action Flag159Action-12
:parameters ()
:precondition
(and
(Flag60-)
(Flag60prime-)
)
:effect
(and
(Flag159-)
(Flag159prime-)
)

)
(:action Flag159Action-13
:parameters ()
:precondition
(and
(Flag140-)
(Flag140prime-)
)
:effect
(and
(Flag159-)
(Flag159prime-)
)

)
(:action Flag159Action-14
:parameters ()
:precondition
(and
(Flag41-)
(Flag41prime-)
)
:effect
(and
(Flag159-)
(Flag159prime-)
)

)
(:action Flag159Action-15
:parameters ()
:precondition
(and
(Flag49-)
(Flag49prime-)
)
:effect
(and
(Flag159-)
(Flag159prime-)
)

)
(:action Flag159Action-16
:parameters ()
:precondition
(and
(Flag103-)
(Flag103prime-)
)
:effect
(and
(Flag159-)
(Flag159prime-)
)

)
(:action Flag159Action-17
:parameters ()
:precondition
(and
(Flag150-)
(Flag150prime-)
)
:effect
(and
(Flag159-)
(Flag159prime-)
)

)
(:action Flag159Action-18
:parameters ()
:precondition
(and
(Flag151-)
(Flag151prime-)
)
:effect
(and
(Flag159-)
(Flag159prime-)
)

)
(:action Flag159Action-19
:parameters ()
:precondition
(and
(Flag141-)
(Flag141prime-)
)
:effect
(and
(Flag159-)
(Flag159prime-)
)

)
(:action Flag159Action-20
:parameters ()
:precondition
(and
(Flag104-)
(Flag104prime-)
)
:effect
(and
(Flag159-)
(Flag159prime-)
)

)
(:action Flag159Action-21
:parameters ()
:precondition
(and
(Flag51-)
(Flag51prime-)
)
:effect
(and
(Flag159-)
(Flag159prime-)
)

)
(:action Flag159Action-22
:parameters ()
:precondition
(and
(Flag142-)
(Flag142prime-)
)
:effect
(and
(Flag159-)
(Flag159prime-)
)

)
(:action Flag159Action-23
:parameters ()
:precondition
(and
(Flag74-)
(Flag74prime-)
)
:effect
(and
(Flag159-)
(Flag159prime-)
)

)
(:action Flag159Action-24
:parameters ()
:precondition
(and
(Flag152-)
(Flag152prime-)
)
:effect
(and
(Flag159-)
(Flag159prime-)
)

)
(:action Flag159Action-25
:parameters ()
:precondition
(and
(Flag129-)
(Flag129prime-)
)
:effect
(and
(Flag159-)
(Flag159prime-)
)

)
(:action Flag159Action-26
:parameters ()
:precondition
(and
(Flag95-)
(Flag95prime-)
)
:effect
(and
(Flag159-)
(Flag159prime-)
)

)
(:action Flag159Action-27
:parameters ()
:precondition
(and
(Flag115-)
(Flag115prime-)
)
:effect
(and
(Flag159-)
(Flag159prime-)
)

)
(:action Flag159Action-28
:parameters ()
:precondition
(and
(Flag86-)
(Flag86prime-)
)
:effect
(and
(Flag159-)
(Flag159prime-)
)

)
(:action Flag159Action-29
:parameters ()
:precondition
(and
(Flag116-)
(Flag116prime-)
)
:effect
(and
(Flag159-)
(Flag159prime-)
)

)
(:action Flag159Action-30
:parameters ()
:precondition
(and
(Flag45-)
(Flag45prime-)
)
:effect
(and
(Flag159-)
(Flag159prime-)
)

)
(:action Flag159Action-31
:parameters ()
:precondition
(and
(Flag87-)
(Flag87prime-)
)
:effect
(and
(Flag159-)
(Flag159prime-)
)

)
(:action Flag159Action-32
:parameters ()
:precondition
(and
(Flag72-)
(Flag72prime-)
)
:effect
(and
(Flag159-)
(Flag159prime-)
)

)
(:action Flag159Action-33
:parameters ()
:precondition
(and
(Flag117-)
(Flag117prime-)
)
:effect
(and
(Flag159-)
(Flag159prime-)
)

)
(:action Flag159Action-34
:parameters ()
:precondition
(and
(Flag105-)
(Flag105prime-)
)
:effect
(and
(Flag159-)
(Flag159prime-)
)

)
(:action Flag159Action-35
:parameters ()
:precondition
(and
(Flag153-)
(Flag153prime-)
)
:effect
(and
(Flag159-)
(Flag159prime-)
)

)
(:action Flag159Action-36
:parameters ()
:precondition
(and
(Flag143-)
(Flag143prime-)
)
:effect
(and
(Flag159-)
(Flag159prime-)
)

)
(:action Flag159Action-37
:parameters ()
:precondition
(and
(Flag144-)
(Flag144prime-)
)
:effect
(and
(Flag159-)
(Flag159prime-)
)

)
(:action Flag159Action-38
:parameters ()
:precondition
(and
(Flag146-)
(Flag146prime-)
)
:effect
(and
(Flag159-)
(Flag159prime-)
)

)
(:action Flag159Action-39
:parameters ()
:precondition
(and
(Flag35-)
(Flag35prime-)
)
:effect
(and
(Flag159-)
(Flag159prime-)
)

)
(:action Flag159Action-40
:parameters ()
:precondition
(and
(Flag73-)
(Flag73prime-)
)
:effect
(and
(Flag159-)
(Flag159prime-)
)

)
(:action Flag159Action-41
:parameters ()
:precondition
(and
(Flag108-)
(Flag108prime-)
)
:effect
(and
(Flag159-)
(Flag159prime-)
)

)
(:action Flag159Action-42
:parameters ()
:precondition
(and
(Flag154-)
(Flag154prime-)
)
:effect
(and
(Flag159-)
(Flag159prime-)
)

)
(:action Flag159Action-43
:parameters ()
:precondition
(and
(Flag61-)
(Flag61prime-)
)
:effect
(and
(Flag159-)
(Flag159prime-)
)

)
(:action Flag159Action-44
:parameters ()
:precondition
(and
(Flag131-)
(Flag131prime-)
)
:effect
(and
(Flag159-)
(Flag159prime-)
)

)
(:action Flag159Action-45
:parameters ()
:precondition
(and
(Flag109-)
(Flag109prime-)
)
:effect
(and
(Flag159-)
(Flag159prime-)
)

)
(:action Flag159Action-46
:parameters ()
:precondition
(and
(Flag155-)
(Flag155prime-)
)
:effect
(and
(Flag159-)
(Flag159prime-)
)

)
(:action Flag159Action-47
:parameters ()
:precondition
(and
(Flag110-)
(Flag110prime-)
)
:effect
(and
(Flag159-)
(Flag159prime-)
)

)
(:action Flag159Action-48
:parameters ()
:precondition
(and
(Flag132-)
(Flag132prime-)
)
:effect
(and
(Flag159-)
(Flag159prime-)
)

)
(:action Flag159Action-49
:parameters ()
:precondition
(and
(Flag59-)
(Flag59prime-)
)
:effect
(and
(Flag159-)
(Flag159prime-)
)

)
(:action Flag159Action-50
:parameters ()
:precondition
(and
(Flag156-)
(Flag156prime-)
)
:effect
(and
(Flag159-)
(Flag159prime-)
)

)
(:action Flag159Action-51
:parameters ()
:precondition
(and
(Flag147-)
(Flag147prime-)
)
:effect
(and
(Flag159-)
(Flag159prime-)
)

)
(:action Flag159Action-52
:parameters ()
:precondition
(and
(Flag76-)
(Flag76prime-)
)
:effect
(and
(Flag159-)
(Flag159prime-)
)

)
(:action Flag159Action-53
:parameters ()
:precondition
(and
(Flag46-)
(Flag46prime-)
)
:effect
(and
(Flag159-)
(Flag159prime-)
)

)
(:action Flag159Action-54
:parameters ()
:precondition
(and
(Flag78-)
(Flag78prime-)
)
:effect
(and
(Flag159-)
(Flag159prime-)
)

)
(:action Flag159Action-55
:parameters ()
:precondition
(and
(Flag90-)
(Flag90prime-)
)
:effect
(and
(Flag159-)
(Flag159prime-)
)

)
(:action Flag159Action-56
:parameters ()
:precondition
(and
(Flag91-)
(Flag91prime-)
)
:effect
(and
(Flag159-)
(Flag159prime-)
)

)
(:action Flag159Action-57
:parameters ()
:precondition
(and
(Flag79-)
(Flag79prime-)
)
:effect
(and
(Flag159-)
(Flag159prime-)
)

)
(:action Flag159Action-58
:parameters ()
:precondition
(and
(Flag133-)
(Flag133prime-)
)
:effect
(and
(Flag159-)
(Flag159prime-)
)

)
(:action Flag159Action-59
:parameters ()
:precondition
(and
(Flag135-)
(Flag135prime-)
)
:effect
(and
(Flag159-)
(Flag159prime-)
)

)
(:action Flag159Action-60
:parameters ()
:precondition
(and
(Flag120-)
(Flag120prime-)
)
:effect
(and
(Flag159-)
(Flag159prime-)
)

)
(:action Flag159Action-61
:parameters ()
:precondition
(and
(Flag92-)
(Flag92prime-)
)
:effect
(and
(Flag159-)
(Flag159prime-)
)

)
(:action Flag159Action-62
:parameters ()
:precondition
(and
(Flag38-)
(Flag38prime-)
)
:effect
(and
(Flag159-)
(Flag159prime-)
)

)
(:action Flag159Action-63
:parameters ()
:precondition
(and
(Flag122-)
(Flag122prime-)
)
:effect
(and
(Flag159-)
(Flag159prime-)
)

)
(:action Flag159Action-64
:parameters ()
:precondition
(and
(Flag136-)
(Flag136prime-)
)
:effect
(and
(Flag159-)
(Flag159prime-)
)

)
(:action Flag159Action-65
:parameters ()
:precondition
(and
(Flag157-)
(Flag157prime-)
)
:effect
(and
(Flag159-)
(Flag159prime-)
)

)
(:action Flag159Action-66
:parameters ()
:precondition
(and
(Flag63-)
(Flag63prime-)
)
:effect
(and
(Flag159-)
(Flag159prime-)
)

)
(:action Flag159Action-67
:parameters ()
:precondition
(and
(Flag123-)
(Flag123prime-)
)
:effect
(and
(Flag159-)
(Flag159prime-)
)

)
(:action Flag159Action-68
:parameters ()
:precondition
(and
(Flag148-)
(Flag148prime-)
)
:effect
(and
(Flag159-)
(Flag159prime-)
)

)
(:action Flag159Action-69
:parameters ()
:precondition
(and
(Flag111-)
(Flag111prime-)
)
:effect
(and
(Flag159-)
(Flag159prime-)
)

)
(:action Flag159Action-70
:parameters ()
:precondition
(and
(Flag48-)
(Flag48prime-)
)
:effect
(and
(Flag159-)
(Flag159prime-)
)

)
(:action Flag159Action-71
:parameters ()
:precondition
(and
(Flag158-)
(Flag158prime-)
)
:effect
(and
(Flag159-)
(Flag159prime-)
)

)
(:action Flag159Action-72
:parameters ()
:precondition
(and
(Flag114-)
(Flag114prime-)
)
:effect
(and
(Flag159-)
(Flag159prime-)
)

)
(:action Flag159Action-73
:parameters ()
:precondition
(and
(Flag80-)
(Flag80prime-)
)
:effect
(and
(Flag159-)
(Flag159prime-)
)

)
(:action Flag159Action-74
:parameters ()
:precondition
(and
(Flag124-)
(Flag124prime-)
)
:effect
(and
(Flag159-)
(Flag159prime-)
)

)
(:action Flag159Action-75
:parameters ()
:precondition
(and
(Flag137-)
(Flag137prime-)
)
:effect
(and
(Flag159-)
(Flag159prime-)
)

)
(:action Prim159Action
:parameters ()
:precondition
(and
(not (Flag62-))
(Flag62prime-)
(not (Flag100-))
(Flag100prime-)
(not (Flag52-))
(Flag52prime-)
(not (Flag99-))
(Flag99prime-)
(not (Flag97-))
(Flag97prime-)
(not (Flag53-))
(Flag53prime-)
(not (Flag37-))
(Flag37prime-)
(not (Flag107-))
(Flag107prime-)
(not (Flag126-))
(Flag126prime-)
(not (Flag69-))
(Flag69prime-)
(not (Flag127-))
(Flag127prime-)
(not (Flag83-))
(Flag83prime-)
(not (Flag60-))
(Flag60prime-)
(not (Flag140-))
(Flag140prime-)
(not (Flag41-))
(Flag41prime-)
(not (Flag49-))
(Flag49prime-)
(not (Flag103-))
(Flag103prime-)
(not (Flag150-))
(Flag150prime-)
(not (Flag151-))
(Flag151prime-)
(not (Flag141-))
(Flag141prime-)
(not (Flag104-))
(Flag104prime-)
(not (Flag51-))
(Flag51prime-)
(not (Flag142-))
(Flag142prime-)
(not (Flag74-))
(Flag74prime-)
(not (Flag152-))
(Flag152prime-)
(not (Flag129-))
(Flag129prime-)
(not (Flag95-))
(Flag95prime-)
(not (Flag115-))
(Flag115prime-)
(not (Flag86-))
(Flag86prime-)
(not (Flag116-))
(Flag116prime-)
(not (Flag45-))
(Flag45prime-)
(not (Flag87-))
(Flag87prime-)
(not (Flag72-))
(Flag72prime-)
(not (Flag117-))
(Flag117prime-)
(not (Flag105-))
(Flag105prime-)
(not (Flag153-))
(Flag153prime-)
(not (Flag143-))
(Flag143prime-)
(not (Flag144-))
(Flag144prime-)
(not (Flag146-))
(Flag146prime-)
(not (Flag35-))
(Flag35prime-)
(not (Flag73-))
(Flag73prime-)
(not (Flag108-))
(Flag108prime-)
(not (Flag154-))
(Flag154prime-)
(not (Flag61-))
(Flag61prime-)
(not (Flag131-))
(Flag131prime-)
(not (Flag109-))
(Flag109prime-)
(not (Flag155-))
(Flag155prime-)
(not (Flag110-))
(Flag110prime-)
(not (Flag132-))
(Flag132prime-)
(not (Flag59-))
(Flag59prime-)
(not (Flag156-))
(Flag156prime-)
(not (Flag147-))
(Flag147prime-)
(not (Flag76-))
(Flag76prime-)
(not (Flag46-))
(Flag46prime-)
(not (Flag78-))
(Flag78prime-)
(not (Flag90-))
(Flag90prime-)
(not (Flag91-))
(Flag91prime-)
(not (Flag79-))
(Flag79prime-)
(not (Flag133-))
(Flag133prime-)
(not (Flag135-))
(Flag135prime-)
(not (Flag120-))
(Flag120prime-)
(not (Flag92-))
(Flag92prime-)
(not (Flag38-))
(Flag38prime-)
(not (Flag122-))
(Flag122prime-)
(not (Flag136-))
(Flag136prime-)
(not (Flag157-))
(Flag157prime-)
(not (Flag63-))
(Flag63prime-)
(not (Flag123-))
(Flag123prime-)
(not (Flag148-))
(Flag148prime-)
(not (Flag111-))
(Flag111prime-)
(not (Flag48-))
(Flag48prime-)
(not (Flag158-))
(Flag158prime-)
(not (Flag114-))
(Flag114prime-)
(not (Flag80-))
(Flag80prime-)
(not (Flag124-))
(Flag124prime-)
(not (Flag137-))
(Flag137prime-)
)
:effect
(and
(Flag159prime-)
(not (Flag159-))
)

)
(:action Flag167Action-0
:parameters ()
:precondition
(and
(Flag62-)
(Flag62prime-)
)
:effect
(and
(Flag167-)
(Flag167prime-)
)

)
(:action Flag167Action-1
:parameters ()
:precondition
(and
(Flag100-)
(Flag100prime-)
)
:effect
(and
(Flag167-)
(Flag167prime-)
)

)
(:action Flag167Action-2
:parameters ()
:precondition
(and
(Flag52-)
(Flag52prime-)
)
:effect
(and
(Flag167-)
(Flag167prime-)
)

)
(:action Flag167Action-3
:parameters ()
:precondition
(and
(Flag99-)
(Flag99prime-)
)
:effect
(and
(Flag167-)
(Flag167prime-)
)

)
(:action Flag167Action-4
:parameters ()
:precondition
(and
(Flag97-)
(Flag97prime-)
)
:effect
(and
(Flag167-)
(Flag167prime-)
)

)
(:action Flag167Action-5
:parameters ()
:precondition
(and
(Flag53-)
(Flag53prime-)
)
:effect
(and
(Flag167-)
(Flag167prime-)
)

)
(:action Flag167Action-6
:parameters ()
:precondition
(and
(Flag37-)
(Flag37prime-)
)
:effect
(and
(Flag167-)
(Flag167prime-)
)

)
(:action Flag167Action-7
:parameters ()
:precondition
(and
(Flag160-)
(Flag160prime-)
)
:effect
(and
(Flag167-)
(Flag167prime-)
)

)
(:action Flag167Action-8
:parameters ()
:precondition
(and
(Flag129-)
(Flag129prime-)
)
:effect
(and
(Flag167-)
(Flag167prime-)
)

)
(:action Flag167Action-9
:parameters ()
:precondition
(and
(Flag140-)
(Flag140prime-)
)
:effect
(and
(Flag167-)
(Flag167prime-)
)

)
(:action Flag167Action-10
:parameters ()
:precondition
(and
(Flag161-)
(Flag161prime-)
)
:effect
(and
(Flag167-)
(Flag167prime-)
)

)
(:action Flag167Action-11
:parameters ()
:precondition
(and
(Flag162-)
(Flag162prime-)
)
:effect
(and
(Flag167-)
(Flag167prime-)
)

)
(:action Flag167Action-12
:parameters ()
:precondition
(and
(Flag157-)
(Flag157prime-)
)
:effect
(and
(Flag167-)
(Flag167prime-)
)

)
(:action Flag167Action-13
:parameters ()
:precondition
(and
(Flag60-)
(Flag60prime-)
)
:effect
(and
(Flag167-)
(Flag167prime-)
)

)
(:action Flag167Action-14
:parameters ()
:precondition
(and
(Flag163-)
(Flag163prime-)
)
:effect
(and
(Flag167-)
(Flag167prime-)
)

)
(:action Flag167Action-15
:parameters ()
:precondition
(and
(Flag164-)
(Flag164prime-)
)
:effect
(and
(Flag167-)
(Flag167prime-)
)

)
(:action Flag167Action-16
:parameters ()
:precondition
(and
(Flag49-)
(Flag49prime-)
)
:effect
(and
(Flag167-)
(Flag167prime-)
)

)
(:action Flag167Action-17
:parameters ()
:precondition
(and
(Flag150-)
(Flag150prime-)
)
:effect
(and
(Flag167-)
(Flag167prime-)
)

)
(:action Flag167Action-18
:parameters ()
:precondition
(and
(Flag141-)
(Flag141prime-)
)
:effect
(and
(Flag167-)
(Flag167prime-)
)

)
(:action Flag167Action-19
:parameters ()
:precondition
(and
(Flag104-)
(Flag104prime-)
)
:effect
(and
(Flag167-)
(Flag167prime-)
)

)
(:action Flag167Action-20
:parameters ()
:precondition
(and
(Flag51-)
(Flag51prime-)
)
:effect
(and
(Flag167-)
(Flag167prime-)
)

)
(:action Flag167Action-21
:parameters ()
:precondition
(and
(Flag142-)
(Flag142prime-)
)
:effect
(and
(Flag167-)
(Flag167prime-)
)

)
(:action Flag167Action-22
:parameters ()
:precondition
(and
(Flag111-)
(Flag111prime-)
)
:effect
(and
(Flag167-)
(Flag167prime-)
)

)
(:action Flag167Action-23
:parameters ()
:precondition
(and
(Flag152-)
(Flag152prime-)
)
:effect
(and
(Flag167-)
(Flag167prime-)
)

)
(:action Flag167Action-24
:parameters ()
:precondition
(and
(Flag165-)
(Flag165prime-)
)
:effect
(and
(Flag167-)
(Flag167prime-)
)

)
(:action Flag167Action-25
:parameters ()
:precondition
(and
(Flag69-)
(Flag69prime-)
)
:effect
(and
(Flag167-)
(Flag167prime-)
)

)
(:action Flag167Action-26
:parameters ()
:precondition
(and
(Flag95-)
(Flag95prime-)
)
:effect
(and
(Flag167-)
(Flag167prime-)
)

)
(:action Flag167Action-27
:parameters ()
:precondition
(and
(Flag115-)
(Flag115prime-)
)
:effect
(and
(Flag167-)
(Flag167prime-)
)

)
(:action Flag167Action-28
:parameters ()
:precondition
(and
(Flag86-)
(Flag86prime-)
)
:effect
(and
(Flag167-)
(Flag167prime-)
)

)
(:action Flag167Action-29
:parameters ()
:precondition
(and
(Flag45-)
(Flag45prime-)
)
:effect
(and
(Flag167-)
(Flag167prime-)
)

)
(:action Flag167Action-30
:parameters ()
:precondition
(and
(Flag87-)
(Flag87prime-)
)
:effect
(and
(Flag167-)
(Flag167prime-)
)

)
(:action Flag167Action-31
:parameters ()
:precondition
(and
(Flag72-)
(Flag72prime-)
)
:effect
(and
(Flag167-)
(Flag167prime-)
)

)
(:action Flag167Action-32
:parameters ()
:precondition
(and
(Flag117-)
(Flag117prime-)
)
:effect
(and
(Flag167-)
(Flag167prime-)
)

)
(:action Flag167Action-33
:parameters ()
:precondition
(and
(Flag153-)
(Flag153prime-)
)
:effect
(and
(Flag167-)
(Flag167prime-)
)

)
(:action Flag167Action-34
:parameters ()
:precondition
(and
(Flag143-)
(Flag143prime-)
)
:effect
(and
(Flag167-)
(Flag167prime-)
)

)
(:action Flag167Action-35
:parameters ()
:precondition
(and
(Flag107-)
(Flag107prime-)
)
:effect
(and
(Flag167-)
(Flag167prime-)
)

)
(:action Flag167Action-36
:parameters ()
:precondition
(and
(Flag146-)
(Flag146prime-)
)
:effect
(and
(Flag167-)
(Flag167prime-)
)

)
(:action Flag167Action-37
:parameters ()
:precondition
(and
(Flag35-)
(Flag35prime-)
)
:effect
(and
(Flag167-)
(Flag167prime-)
)

)
(:action Flag167Action-38
:parameters ()
:precondition
(and
(Flag73-)
(Flag73prime-)
)
:effect
(and
(Flag167-)
(Flag167prime-)
)

)
(:action Flag167Action-39
:parameters ()
:precondition
(and
(Flag108-)
(Flag108prime-)
)
:effect
(and
(Flag167-)
(Flag167prime-)
)

)
(:action Flag167Action-40
:parameters ()
:precondition
(and
(Flag154-)
(Flag154prime-)
)
:effect
(and
(Flag167-)
(Flag167prime-)
)

)
(:action Flag167Action-41
:parameters ()
:precondition
(and
(Flag61-)
(Flag61prime-)
)
:effect
(and
(Flag167-)
(Flag167prime-)
)

)
(:action Flag167Action-42
:parameters ()
:precondition
(and
(Flag131-)
(Flag131prime-)
)
:effect
(and
(Flag167-)
(Flag167prime-)
)

)
(:action Flag167Action-43
:parameters ()
:precondition
(and
(Flag109-)
(Flag109prime-)
)
:effect
(and
(Flag167-)
(Flag167prime-)
)

)
(:action Flag167Action-44
:parameters ()
:precondition
(and
(Flag155-)
(Flag155prime-)
)
:effect
(and
(Flag167-)
(Flag167prime-)
)

)
(:action Flag167Action-45
:parameters ()
:precondition
(and
(Flag132-)
(Flag132prime-)
)
:effect
(and
(Flag167-)
(Flag167prime-)
)

)
(:action Flag167Action-46
:parameters ()
:precondition
(and
(Flag156-)
(Flag156prime-)
)
:effect
(and
(Flag167-)
(Flag167prime-)
)

)
(:action Flag167Action-47
:parameters ()
:precondition
(and
(Flag147-)
(Flag147prime-)
)
:effect
(and
(Flag167-)
(Flag167prime-)
)

)
(:action Flag167Action-48
:parameters ()
:precondition
(and
(Flag76-)
(Flag76prime-)
)
:effect
(and
(Flag167-)
(Flag167prime-)
)

)
(:action Flag167Action-49
:parameters ()
:precondition
(and
(Flag59-)
(Flag59prime-)
)
:effect
(and
(Flag167-)
(Flag167prime-)
)

)
(:action Flag167Action-50
:parameters ()
:precondition
(and
(Flag78-)
(Flag78prime-)
)
:effect
(and
(Flag167-)
(Flag167prime-)
)

)
(:action Flag167Action-51
:parameters ()
:precondition
(and
(Flag90-)
(Flag90prime-)
)
:effect
(and
(Flag167-)
(Flag167prime-)
)

)
(:action Flag167Action-52
:parameters ()
:precondition
(and
(Flag91-)
(Flag91prime-)
)
:effect
(and
(Flag167-)
(Flag167prime-)
)

)
(:action Flag167Action-53
:parameters ()
:precondition
(and
(Flag79-)
(Flag79prime-)
)
:effect
(and
(Flag167-)
(Flag167prime-)
)

)
(:action Flag167Action-54
:parameters ()
:precondition
(and
(Flag133-)
(Flag133prime-)
)
:effect
(and
(Flag167-)
(Flag167prime-)
)

)
(:action Flag167Action-55
:parameters ()
:precondition
(and
(Flag135-)
(Flag135prime-)
)
:effect
(and
(Flag167-)
(Flag167prime-)
)

)
(:action Flag167Action-56
:parameters ()
:precondition
(and
(Flag120-)
(Flag120prime-)
)
:effect
(and
(Flag167-)
(Flag167prime-)
)

)
(:action Flag167Action-57
:parameters ()
:precondition
(and
(Flag83-)
(Flag83prime-)
)
:effect
(and
(Flag167-)
(Flag167prime-)
)

)
(:action Flag167Action-58
:parameters ()
:precondition
(and
(Flag41-)
(Flag41prime-)
)
:effect
(and
(Flag167-)
(Flag167prime-)
)

)
(:action Flag167Action-59
:parameters ()
:precondition
(and
(Flag92-)
(Flag92prime-)
)
:effect
(and
(Flag167-)
(Flag167prime-)
)

)
(:action Flag167Action-60
:parameters ()
:precondition
(and
(Flag126-)
(Flag126prime-)
)
:effect
(and
(Flag167-)
(Flag167prime-)
)

)
(:action Flag167Action-61
:parameters ()
:precondition
(and
(Flag38-)
(Flag38prime-)
)
:effect
(and
(Flag167-)
(Flag167prime-)
)

)
(:action Flag167Action-62
:parameters ()
:precondition
(and
(Flag122-)
(Flag122prime-)
)
:effect
(and
(Flag167-)
(Flag167prime-)
)

)
(:action Flag167Action-63
:parameters ()
:precondition
(and
(Flag166-)
(Flag166prime-)
)
:effect
(and
(Flag167-)
(Flag167prime-)
)

)
(:action Flag167Action-64
:parameters ()
:precondition
(and
(Flag136-)
(Flag136prime-)
)
:effect
(and
(Flag167-)
(Flag167prime-)
)

)
(:action Flag167Action-65
:parameters ()
:precondition
(and
(Flag123-)
(Flag123prime-)
)
:effect
(and
(Flag167-)
(Flag167prime-)
)

)
(:action Flag167Action-66
:parameters ()
:precondition
(and
(Flag148-)
(Flag148prime-)
)
:effect
(and
(Flag167-)
(Flag167prime-)
)

)
(:action Flag167Action-67
:parameters ()
:precondition
(and
(Flag80-)
(Flag80prime-)
)
:effect
(and
(Flag167-)
(Flag167prime-)
)

)
(:action Flag167Action-68
:parameters ()
:precondition
(and
(Flag48-)
(Flag48prime-)
)
:effect
(and
(Flag167-)
(Flag167prime-)
)

)
(:action Flag167Action-69
:parameters ()
:precondition
(and
(Flag158-)
(Flag158prime-)
)
:effect
(and
(Flag167-)
(Flag167prime-)
)

)
(:action Flag167Action-70
:parameters ()
:precondition
(and
(Flag114-)
(Flag114prime-)
)
:effect
(and
(Flag167-)
(Flag167prime-)
)

)
(:action Flag167Action-71
:parameters ()
:precondition
(and
(Flag103-)
(Flag103prime-)
)
:effect
(and
(Flag167-)
(Flag167prime-)
)

)
(:action Flag167Action-72
:parameters ()
:precondition
(and
(Flag124-)
(Flag124prime-)
)
:effect
(and
(Flag167-)
(Flag167prime-)
)

)
(:action Flag167Action-73
:parameters ()
:precondition
(and
(Flag137-)
(Flag137prime-)
)
:effect
(and
(Flag167-)
(Flag167prime-)
)

)
(:action Prim167Action
:parameters ()
:precondition
(and
(not (Flag62-))
(Flag62prime-)
(not (Flag100-))
(Flag100prime-)
(not (Flag52-))
(Flag52prime-)
(not (Flag99-))
(Flag99prime-)
(not (Flag97-))
(Flag97prime-)
(not (Flag53-))
(Flag53prime-)
(not (Flag37-))
(Flag37prime-)
(not (Flag160-))
(Flag160prime-)
(not (Flag129-))
(Flag129prime-)
(not (Flag140-))
(Flag140prime-)
(not (Flag161-))
(Flag161prime-)
(not (Flag162-))
(Flag162prime-)
(not (Flag157-))
(Flag157prime-)
(not (Flag60-))
(Flag60prime-)
(not (Flag163-))
(Flag163prime-)
(not (Flag164-))
(Flag164prime-)
(not (Flag49-))
(Flag49prime-)
(not (Flag150-))
(Flag150prime-)
(not (Flag141-))
(Flag141prime-)
(not (Flag104-))
(Flag104prime-)
(not (Flag51-))
(Flag51prime-)
(not (Flag142-))
(Flag142prime-)
(not (Flag111-))
(Flag111prime-)
(not (Flag152-))
(Flag152prime-)
(not (Flag165-))
(Flag165prime-)
(not (Flag69-))
(Flag69prime-)
(not (Flag95-))
(Flag95prime-)
(not (Flag115-))
(Flag115prime-)
(not (Flag86-))
(Flag86prime-)
(not (Flag45-))
(Flag45prime-)
(not (Flag87-))
(Flag87prime-)
(not (Flag72-))
(Flag72prime-)
(not (Flag117-))
(Flag117prime-)
(not (Flag153-))
(Flag153prime-)
(not (Flag143-))
(Flag143prime-)
(not (Flag107-))
(Flag107prime-)
(not (Flag146-))
(Flag146prime-)
(not (Flag35-))
(Flag35prime-)
(not (Flag73-))
(Flag73prime-)
(not (Flag108-))
(Flag108prime-)
(not (Flag154-))
(Flag154prime-)
(not (Flag61-))
(Flag61prime-)
(not (Flag131-))
(Flag131prime-)
(not (Flag109-))
(Flag109prime-)
(not (Flag155-))
(Flag155prime-)
(not (Flag132-))
(Flag132prime-)
(not (Flag156-))
(Flag156prime-)
(not (Flag147-))
(Flag147prime-)
(not (Flag76-))
(Flag76prime-)
(not (Flag59-))
(Flag59prime-)
(not (Flag78-))
(Flag78prime-)
(not (Flag90-))
(Flag90prime-)
(not (Flag91-))
(Flag91prime-)
(not (Flag79-))
(Flag79prime-)
(not (Flag133-))
(Flag133prime-)
(not (Flag135-))
(Flag135prime-)
(not (Flag120-))
(Flag120prime-)
(not (Flag83-))
(Flag83prime-)
(not (Flag41-))
(Flag41prime-)
(not (Flag92-))
(Flag92prime-)
(not (Flag126-))
(Flag126prime-)
(not (Flag38-))
(Flag38prime-)
(not (Flag122-))
(Flag122prime-)
(not (Flag166-))
(Flag166prime-)
(not (Flag136-))
(Flag136prime-)
(not (Flag123-))
(Flag123prime-)
(not (Flag148-))
(Flag148prime-)
(not (Flag80-))
(Flag80prime-)
(not (Flag48-))
(Flag48prime-)
(not (Flag158-))
(Flag158prime-)
(not (Flag114-))
(Flag114prime-)
(not (Flag103-))
(Flag103prime-)
(not (Flag124-))
(Flag124prime-)
(not (Flag137-))
(Flag137prime-)
)
:effect
(and
(Flag167prime-)
(not (Flag167-))
)

)
(:action Flag175Action-0
:parameters ()
:precondition
(and
(Flag62-)
(Flag62prime-)
)
:effect
(and
(Flag175-)
(Flag175prime-)
)

)
(:action Flag175Action-1
:parameters ()
:precondition
(and
(Flag100-)
(Flag100prime-)
)
:effect
(and
(Flag175-)
(Flag175prime-)
)

)
(:action Flag175Action-2
:parameters ()
:precondition
(and
(Flag52-)
(Flag52prime-)
)
:effect
(and
(Flag175-)
(Flag175prime-)
)

)
(:action Flag175Action-3
:parameters ()
:precondition
(and
(Flag168-)
(Flag168prime-)
)
:effect
(and
(Flag175-)
(Flag175prime-)
)

)
(:action Flag175Action-4
:parameters ()
:precondition
(and
(Flag126-)
(Flag126prime-)
)
:effect
(and
(Flag175-)
(Flag175prime-)
)

)
(:action Flag175Action-5
:parameters ()
:precondition
(and
(Flag41-)
(Flag41prime-)
)
:effect
(and
(Flag175-)
(Flag175prime-)
)

)
(:action Flag175Action-6
:parameters ()
:precondition
(and
(Flag160-)
(Flag160prime-)
)
:effect
(and
(Flag175-)
(Flag175prime-)
)

)
(:action Flag175Action-7
:parameters ()
:precondition
(and
(Flag169-)
(Flag169prime-)
)
:effect
(and
(Flag175-)
(Flag175prime-)
)

)
(:action Flag175Action-8
:parameters ()
:precondition
(and
(Flag161-)
(Flag161prime-)
)
:effect
(and
(Flag175-)
(Flag175prime-)
)

)
(:action Flag175Action-9
:parameters ()
:precondition
(and
(Flag162-)
(Flag162prime-)
)
:effect
(and
(Flag175-)
(Flag175prime-)
)

)
(:action Flag175Action-10
:parameters ()
:precondition
(and
(Flag157-)
(Flag157prime-)
)
:effect
(and
(Flag175-)
(Flag175prime-)
)

)
(:action Flag175Action-11
:parameters ()
:precondition
(and
(Flag60-)
(Flag60prime-)
)
:effect
(and
(Flag175-)
(Flag175prime-)
)

)
(:action Flag175Action-12
:parameters ()
:precondition
(and
(Flag163-)
(Flag163prime-)
)
:effect
(and
(Flag175-)
(Flag175prime-)
)

)
(:action Flag175Action-13
:parameters ()
:precondition
(and
(Flag164-)
(Flag164prime-)
)
:effect
(and
(Flag175-)
(Flag175prime-)
)

)
(:action Flag175Action-14
:parameters ()
:precondition
(and
(Flag49-)
(Flag49prime-)
)
:effect
(and
(Flag175-)
(Flag175prime-)
)

)
(:action Flag175Action-15
:parameters ()
:precondition
(and
(Flag103-)
(Flag103prime-)
)
:effect
(and
(Flag175-)
(Flag175prime-)
)

)
(:action Flag175Action-16
:parameters ()
:precondition
(and
(Flag150-)
(Flag150prime-)
)
:effect
(and
(Flag175-)
(Flag175prime-)
)

)
(:action Flag175Action-17
:parameters ()
:precondition
(and
(Flag141-)
(Flag141prime-)
)
:effect
(and
(Flag175-)
(Flag175prime-)
)

)
(:action Flag175Action-18
:parameters ()
:precondition
(and
(Flag104-)
(Flag104prime-)
)
:effect
(and
(Flag175-)
(Flag175prime-)
)

)
(:action Flag175Action-19
:parameters ()
:precondition
(and
(Flag51-)
(Flag51prime-)
)
:effect
(and
(Flag175-)
(Flag175prime-)
)

)
(:action Flag175Action-20
:parameters ()
:precondition
(and
(Flag142-)
(Flag142prime-)
)
:effect
(and
(Flag175-)
(Flag175prime-)
)

)
(:action Flag175Action-21
:parameters ()
:precondition
(and
(Flag111-)
(Flag111prime-)
)
:effect
(and
(Flag175-)
(Flag175prime-)
)

)
(:action Flag175Action-22
:parameters ()
:precondition
(and
(Flag114-)
(Flag114prime-)
)
:effect
(and
(Flag175-)
(Flag175prime-)
)

)
(:action Flag175Action-23
:parameters ()
:precondition
(and
(Flag129-)
(Flag129prime-)
)
:effect
(and
(Flag175-)
(Flag175prime-)
)

)
(:action Flag175Action-24
:parameters ()
:precondition
(and
(Flag69-)
(Flag69prime-)
)
:effect
(and
(Flag175-)
(Flag175prime-)
)

)
(:action Flag175Action-25
:parameters ()
:precondition
(and
(Flag115-)
(Flag115prime-)
)
:effect
(and
(Flag175-)
(Flag175prime-)
)

)
(:action Flag175Action-26
:parameters ()
:precondition
(and
(Flag86-)
(Flag86prime-)
)
:effect
(and
(Flag175-)
(Flag175prime-)
)

)
(:action Flag175Action-27
:parameters ()
:precondition
(and
(Flag45-)
(Flag45prime-)
)
:effect
(and
(Flag175-)
(Flag175prime-)
)

)
(:action Flag175Action-28
:parameters ()
:precondition
(and
(Flag87-)
(Flag87prime-)
)
:effect
(and
(Flag175-)
(Flag175prime-)
)

)
(:action Flag175Action-29
:parameters ()
:precondition
(and
(Flag72-)
(Flag72prime-)
)
:effect
(and
(Flag175-)
(Flag175prime-)
)

)
(:action Flag175Action-30
:parameters ()
:precondition
(and
(Flag117-)
(Flag117prime-)
)
:effect
(and
(Flag175-)
(Flag175prime-)
)

)
(:action Flag175Action-31
:parameters ()
:precondition
(and
(Flag170-)
(Flag170prime-)
)
:effect
(and
(Flag175-)
(Flag175prime-)
)

)
(:action Flag175Action-32
:parameters ()
:precondition
(and
(Flag153-)
(Flag153prime-)
)
:effect
(and
(Flag175-)
(Flag175prime-)
)

)
(:action Flag175Action-33
:parameters ()
:precondition
(and
(Flag143-)
(Flag143prime-)
)
:effect
(and
(Flag175-)
(Flag175prime-)
)

)
(:action Flag175Action-34
:parameters ()
:precondition
(and
(Flag107-)
(Flag107prime-)
)
:effect
(and
(Flag175-)
(Flag175prime-)
)

)
(:action Flag175Action-35
:parameters ()
:precondition
(and
(Flag146-)
(Flag146prime-)
)
:effect
(and
(Flag175-)
(Flag175prime-)
)

)
(:action Flag175Action-36
:parameters ()
:precondition
(and
(Flag35-)
(Flag35prime-)
)
:effect
(and
(Flag175-)
(Flag175prime-)
)

)
(:action Flag175Action-37
:parameters ()
:precondition
(and
(Flag73-)
(Flag73prime-)
)
:effect
(and
(Flag175-)
(Flag175prime-)
)

)
(:action Flag175Action-38
:parameters ()
:precondition
(and
(Flag108-)
(Flag108prime-)
)
:effect
(and
(Flag175-)
(Flag175prime-)
)

)
(:action Flag175Action-39
:parameters ()
:precondition
(and
(Flag154-)
(Flag154prime-)
)
:effect
(and
(Flag175-)
(Flag175prime-)
)

)
(:action Flag175Action-40
:parameters ()
:precondition
(and
(Flag61-)
(Flag61prime-)
)
:effect
(and
(Flag175-)
(Flag175prime-)
)

)
(:action Flag175Action-41
:parameters ()
:precondition
(and
(Flag131-)
(Flag131prime-)
)
:effect
(and
(Flag175-)
(Flag175prime-)
)

)
(:action Flag175Action-42
:parameters ()
:precondition
(and
(Flag109-)
(Flag109prime-)
)
:effect
(and
(Flag175-)
(Flag175prime-)
)

)
(:action Flag175Action-43
:parameters ()
:precondition
(and
(Flag155-)
(Flag155prime-)
)
:effect
(and
(Flag175-)
(Flag175prime-)
)

)
(:action Flag175Action-44
:parameters ()
:precondition
(and
(Flag132-)
(Flag132prime-)
)
:effect
(and
(Flag175-)
(Flag175prime-)
)

)
(:action Flag175Action-45
:parameters ()
:precondition
(and
(Flag171-)
(Flag171prime-)
)
:effect
(and
(Flag175-)
(Flag175prime-)
)

)
(:action Flag175Action-46
:parameters ()
:precondition
(and
(Flag156-)
(Flag156prime-)
)
:effect
(and
(Flag175-)
(Flag175prime-)
)

)
(:action Flag175Action-47
:parameters ()
:precondition
(and
(Flag147-)
(Flag147prime-)
)
:effect
(and
(Flag175-)
(Flag175prime-)
)

)
(:action Flag175Action-48
:parameters ()
:precondition
(and
(Flag76-)
(Flag76prime-)
)
:effect
(and
(Flag175-)
(Flag175prime-)
)

)
(:action Flag175Action-49
:parameters ()
:precondition
(and
(Flag59-)
(Flag59prime-)
)
:effect
(and
(Flag175-)
(Flag175prime-)
)

)
(:action Flag175Action-50
:parameters ()
:precondition
(and
(Flag78-)
(Flag78prime-)
)
:effect
(and
(Flag175-)
(Flag175prime-)
)

)
(:action Flag175Action-51
:parameters ()
:precondition
(and
(Flag90-)
(Flag90prime-)
)
:effect
(and
(Flag175-)
(Flag175prime-)
)

)
(:action Flag175Action-52
:parameters ()
:precondition
(and
(Flag97-)
(Flag97prime-)
)
:effect
(and
(Flag175-)
(Flag175prime-)
)

)
(:action Flag175Action-53
:parameters ()
:precondition
(and
(Flag79-)
(Flag79prime-)
)
:effect
(and
(Flag175-)
(Flag175prime-)
)

)
(:action Flag175Action-54
:parameters ()
:precondition
(and
(Flag135-)
(Flag135prime-)
)
:effect
(and
(Flag175-)
(Flag175prime-)
)

)
(:action Flag175Action-55
:parameters ()
:precondition
(and
(Flag120-)
(Flag120prime-)
)
:effect
(and
(Flag175-)
(Flag175prime-)
)

)
(:action Flag175Action-56
:parameters ()
:precondition
(and
(Flag83-)
(Flag83prime-)
)
:effect
(and
(Flag175-)
(Flag175prime-)
)

)
(:action Flag175Action-57
:parameters ()
:precondition
(and
(Flag172-)
(Flag172prime-)
)
:effect
(and
(Flag175-)
(Flag175prime-)
)

)
(:action Flag175Action-58
:parameters ()
:precondition
(and
(Flag38-)
(Flag38prime-)
)
:effect
(and
(Flag175-)
(Flag175prime-)
)

)
(:action Flag175Action-59
:parameters ()
:precondition
(and
(Flag122-)
(Flag122prime-)
)
:effect
(and
(Flag175-)
(Flag175prime-)
)

)
(:action Flag175Action-60
:parameters ()
:precondition
(and
(Flag166-)
(Flag166prime-)
)
:effect
(and
(Flag175-)
(Flag175prime-)
)

)
(:action Flag175Action-61
:parameters ()
:precondition
(and
(Flag136-)
(Flag136prime-)
)
:effect
(and
(Flag175-)
(Flag175prime-)
)

)
(:action Flag175Action-62
:parameters ()
:precondition
(and
(Flag173-)
(Flag173prime-)
)
:effect
(and
(Flag175-)
(Flag175prime-)
)

)
(:action Flag175Action-63
:parameters ()
:precondition
(and
(Flag95-)
(Flag95prime-)
)
:effect
(and
(Flag175-)
(Flag175prime-)
)

)
(:action Flag175Action-64
:parameters ()
:precondition
(and
(Flag148-)
(Flag148prime-)
)
:effect
(and
(Flag175-)
(Flag175prime-)
)

)
(:action Flag175Action-65
:parameters ()
:precondition
(and
(Flag48-)
(Flag48prime-)
)
:effect
(and
(Flag175-)
(Flag175prime-)
)

)
(:action Flag175Action-66
:parameters ()
:precondition
(and
(Flag158-)
(Flag158prime-)
)
:effect
(and
(Flag175-)
(Flag175prime-)
)

)
(:action Flag175Action-67
:parameters ()
:precondition
(and
(Flag174-)
(Flag174prime-)
)
:effect
(and
(Flag175-)
(Flag175prime-)
)

)
(:action Flag175Action-68
:parameters ()
:precondition
(and
(Flag123-)
(Flag123prime-)
)
:effect
(and
(Flag175-)
(Flag175prime-)
)

)
(:action Flag175Action-69
:parameters ()
:precondition
(and
(Flag137-)
(Flag137prime-)
)
:effect
(and
(Flag175-)
(Flag175prime-)
)

)
(:action Prim175Action
:parameters ()
:precondition
(and
(not (Flag62-))
(Flag62prime-)
(not (Flag100-))
(Flag100prime-)
(not (Flag52-))
(Flag52prime-)
(not (Flag168-))
(Flag168prime-)
(not (Flag126-))
(Flag126prime-)
(not (Flag41-))
(Flag41prime-)
(not (Flag160-))
(Flag160prime-)
(not (Flag169-))
(Flag169prime-)
(not (Flag161-))
(Flag161prime-)
(not (Flag162-))
(Flag162prime-)
(not (Flag157-))
(Flag157prime-)
(not (Flag60-))
(Flag60prime-)
(not (Flag163-))
(Flag163prime-)
(not (Flag164-))
(Flag164prime-)
(not (Flag49-))
(Flag49prime-)
(not (Flag103-))
(Flag103prime-)
(not (Flag150-))
(Flag150prime-)
(not (Flag141-))
(Flag141prime-)
(not (Flag104-))
(Flag104prime-)
(not (Flag51-))
(Flag51prime-)
(not (Flag142-))
(Flag142prime-)
(not (Flag111-))
(Flag111prime-)
(not (Flag114-))
(Flag114prime-)
(not (Flag129-))
(Flag129prime-)
(not (Flag69-))
(Flag69prime-)
(not (Flag115-))
(Flag115prime-)
(not (Flag86-))
(Flag86prime-)
(not (Flag45-))
(Flag45prime-)
(not (Flag87-))
(Flag87prime-)
(not (Flag72-))
(Flag72prime-)
(not (Flag117-))
(Flag117prime-)
(not (Flag170-))
(Flag170prime-)
(not (Flag153-))
(Flag153prime-)
(not (Flag143-))
(Flag143prime-)
(not (Flag107-))
(Flag107prime-)
(not (Flag146-))
(Flag146prime-)
(not (Flag35-))
(Flag35prime-)
(not (Flag73-))
(Flag73prime-)
(not (Flag108-))
(Flag108prime-)
(not (Flag154-))
(Flag154prime-)
(not (Flag61-))
(Flag61prime-)
(not (Flag131-))
(Flag131prime-)
(not (Flag109-))
(Flag109prime-)
(not (Flag155-))
(Flag155prime-)
(not (Flag132-))
(Flag132prime-)
(not (Flag171-))
(Flag171prime-)
(not (Flag156-))
(Flag156prime-)
(not (Flag147-))
(Flag147prime-)
(not (Flag76-))
(Flag76prime-)
(not (Flag59-))
(Flag59prime-)
(not (Flag78-))
(Flag78prime-)
(not (Flag90-))
(Flag90prime-)
(not (Flag97-))
(Flag97prime-)
(not (Flag79-))
(Flag79prime-)
(not (Flag135-))
(Flag135prime-)
(not (Flag120-))
(Flag120prime-)
(not (Flag83-))
(Flag83prime-)
(not (Flag172-))
(Flag172prime-)
(not (Flag38-))
(Flag38prime-)
(not (Flag122-))
(Flag122prime-)
(not (Flag166-))
(Flag166prime-)
(not (Flag136-))
(Flag136prime-)
(not (Flag173-))
(Flag173prime-)
(not (Flag95-))
(Flag95prime-)
(not (Flag148-))
(Flag148prime-)
(not (Flag48-))
(Flag48prime-)
(not (Flag158-))
(Flag158prime-)
(not (Flag174-))
(Flag174prime-)
(not (Flag123-))
(Flag123prime-)
(not (Flag137-))
(Flag137prime-)
)
:effect
(and
(Flag175prime-)
(not (Flag175-))
)

)
(:action Flag181Action-0
:parameters ()
:precondition
(and
(Flag62-)
(Flag62prime-)
)
:effect
(and
(Flag181-)
(Flag181prime-)
)

)
(:action Flag181Action-1
:parameters ()
:precondition
(and
(Flag100-)
(Flag100prime-)
)
:effect
(and
(Flag181-)
(Flag181prime-)
)

)
(:action Flag181Action-2
:parameters ()
:precondition
(and
(Flag52-)
(Flag52prime-)
)
:effect
(and
(Flag181-)
(Flag181prime-)
)

)
(:action Flag181Action-3
:parameters ()
:precondition
(and
(Flag168-)
(Flag168prime-)
)
:effect
(and
(Flag181-)
(Flag181prime-)
)

)
(:action Flag181Action-4
:parameters ()
:precondition
(and
(Flag41-)
(Flag41prime-)
)
:effect
(and
(Flag181-)
(Flag181prime-)
)

)
(:action Flag181Action-5
:parameters ()
:precondition
(and
(Flag160-)
(Flag160prime-)
)
:effect
(and
(Flag181-)
(Flag181prime-)
)

)
(:action Flag181Action-6
:parameters ()
:precondition
(and
(Flag169-)
(Flag169prime-)
)
:effect
(and
(Flag181-)
(Flag181prime-)
)

)
(:action Flag181Action-7
:parameters ()
:precondition
(and
(Flag161-)
(Flag161prime-)
)
:effect
(and
(Flag181-)
(Flag181prime-)
)

)
(:action Flag181Action-8
:parameters ()
:precondition
(and
(Flag172-)
(Flag172prime-)
)
:effect
(and
(Flag181-)
(Flag181prime-)
)

)
(:action Flag181Action-9
:parameters ()
:precondition
(and
(Flag157-)
(Flag157prime-)
)
:effect
(and
(Flag181-)
(Flag181prime-)
)

)
(:action Flag181Action-10
:parameters ()
:precondition
(and
(Flag176-)
(Flag176prime-)
)
:effect
(and
(Flag181-)
(Flag181prime-)
)

)
(:action Flag181Action-11
:parameters ()
:precondition
(and
(Flag163-)
(Flag163prime-)
)
:effect
(and
(Flag181-)
(Flag181prime-)
)

)
(:action Flag181Action-12
:parameters ()
:precondition
(and
(Flag164-)
(Flag164prime-)
)
:effect
(and
(Flag181-)
(Flag181prime-)
)

)
(:action Flag181Action-13
:parameters ()
:precondition
(and
(Flag35-)
(Flag35prime-)
)
:effect
(and
(Flag181-)
(Flag181prime-)
)

)
(:action Flag181Action-14
:parameters ()
:precondition
(and
(Flag150-)
(Flag150prime-)
)
:effect
(and
(Flag181-)
(Flag181prime-)
)

)
(:action Flag181Action-15
:parameters ()
:precondition
(and
(Flag177-)
(Flag177prime-)
)
:effect
(and
(Flag181-)
(Flag181prime-)
)

)
(:action Flag181Action-16
:parameters ()
:precondition
(and
(Flag104-)
(Flag104prime-)
)
:effect
(and
(Flag181-)
(Flag181prime-)
)

)
(:action Flag181Action-17
:parameters ()
:precondition
(and
(Flag51-)
(Flag51prime-)
)
:effect
(and
(Flag181-)
(Flag181prime-)
)

)
(:action Flag181Action-18
:parameters ()
:precondition
(and
(Flag142-)
(Flag142prime-)
)
:effect
(and
(Flag181-)
(Flag181prime-)
)

)
(:action Flag181Action-19
:parameters ()
:precondition
(and
(Flag111-)
(Flag111prime-)
)
:effect
(and
(Flag181-)
(Flag181prime-)
)

)
(:action Flag181Action-20
:parameters ()
:precondition
(and
(Flag114-)
(Flag114prime-)
)
:effect
(and
(Flag181-)
(Flag181prime-)
)

)
(:action Flag181Action-21
:parameters ()
:precondition
(and
(Flag129-)
(Flag129prime-)
)
:effect
(and
(Flag181-)
(Flag181prime-)
)

)
(:action Flag181Action-22
:parameters ()
:precondition
(and
(Flag69-)
(Flag69prime-)
)
:effect
(and
(Flag181-)
(Flag181prime-)
)

)
(:action Flag181Action-23
:parameters ()
:precondition
(and
(Flag115-)
(Flag115prime-)
)
:effect
(and
(Flag181-)
(Flag181prime-)
)

)
(:action Flag181Action-24
:parameters ()
:precondition
(and
(Flag86-)
(Flag86prime-)
)
:effect
(and
(Flag181-)
(Flag181prime-)
)

)
(:action Flag181Action-25
:parameters ()
:precondition
(and
(Flag45-)
(Flag45prime-)
)
:effect
(and
(Flag181-)
(Flag181prime-)
)

)
(:action Flag181Action-26
:parameters ()
:precondition
(and
(Flag87-)
(Flag87prime-)
)
:effect
(and
(Flag181-)
(Flag181prime-)
)

)
(:action Flag181Action-27
:parameters ()
:precondition
(and
(Flag72-)
(Flag72prime-)
)
:effect
(and
(Flag181-)
(Flag181prime-)
)

)
(:action Flag181Action-28
:parameters ()
:precondition
(and
(Flag117-)
(Flag117prime-)
)
:effect
(and
(Flag181-)
(Flag181prime-)
)

)
(:action Flag181Action-29
:parameters ()
:precondition
(and
(Flag178-)
(Flag178prime-)
)
:effect
(and
(Flag181-)
(Flag181prime-)
)

)
(:action Flag181Action-30
:parameters ()
:precondition
(and
(Flag170-)
(Flag170prime-)
)
:effect
(and
(Flag181-)
(Flag181prime-)
)

)
(:action Flag181Action-31
:parameters ()
:precondition
(and
(Flag179-)
(Flag179prime-)
)
:effect
(and
(Flag181-)
(Flag181prime-)
)

)
(:action Flag181Action-32
:parameters ()
:precondition
(and
(Flag146-)
(Flag146prime-)
)
:effect
(and
(Flag181-)
(Flag181prime-)
)

)
(:action Flag181Action-33
:parameters ()
:precondition
(and
(Flag141-)
(Flag141prime-)
)
:effect
(and
(Flag181-)
(Flag181prime-)
)

)
(:action Flag181Action-34
:parameters ()
:precondition
(and
(Flag126-)
(Flag126prime-)
)
:effect
(and
(Flag181-)
(Flag181prime-)
)

)
(:action Flag181Action-35
:parameters ()
:precondition
(and
(Flag154-)
(Flag154prime-)
)
:effect
(and
(Flag181-)
(Flag181prime-)
)

)
(:action Flag181Action-36
:parameters ()
:precondition
(and
(Flag61-)
(Flag61prime-)
)
:effect
(and
(Flag181-)
(Flag181prime-)
)

)
(:action Flag181Action-37
:parameters ()
:precondition
(and
(Flag131-)
(Flag131prime-)
)
:effect
(and
(Flag181-)
(Flag181prime-)
)

)
(:action Flag181Action-38
:parameters ()
:precondition
(and
(Flag109-)
(Flag109prime-)
)
:effect
(and
(Flag181-)
(Flag181prime-)
)

)
(:action Flag181Action-39
:parameters ()
:precondition
(and
(Flag155-)
(Flag155prime-)
)
:effect
(and
(Flag181-)
(Flag181prime-)
)

)
(:action Flag181Action-40
:parameters ()
:precondition
(and
(Flag132-)
(Flag132prime-)
)
:effect
(and
(Flag181-)
(Flag181prime-)
)

)
(:action Flag181Action-41
:parameters ()
:precondition
(and
(Flag171-)
(Flag171prime-)
)
:effect
(and
(Flag181-)
(Flag181prime-)
)

)
(:action Flag181Action-42
:parameters ()
:precondition
(and
(Flag156-)
(Flag156prime-)
)
:effect
(and
(Flag181-)
(Flag181prime-)
)

)
(:action Flag181Action-43
:parameters ()
:precondition
(and
(Flag147-)
(Flag147prime-)
)
:effect
(and
(Flag181-)
(Flag181prime-)
)

)
(:action Flag181Action-44
:parameters ()
:precondition
(and
(Flag76-)
(Flag76prime-)
)
:effect
(and
(Flag181-)
(Flag181prime-)
)

)
(:action Flag181Action-45
:parameters ()
:precondition
(and
(Flag59-)
(Flag59prime-)
)
:effect
(and
(Flag181-)
(Flag181prime-)
)

)
(:action Flag181Action-46
:parameters ()
:precondition
(and
(Flag78-)
(Flag78prime-)
)
:effect
(and
(Flag181-)
(Flag181prime-)
)

)
(:action Flag181Action-47
:parameters ()
:precondition
(and
(Flag90-)
(Flag90prime-)
)
:effect
(and
(Flag181-)
(Flag181prime-)
)

)
(:action Flag181Action-48
:parameters ()
:precondition
(and
(Flag97-)
(Flag97prime-)
)
:effect
(and
(Flag181-)
(Flag181prime-)
)

)
(:action Flag181Action-49
:parameters ()
:precondition
(and
(Flag79-)
(Flag79prime-)
)
:effect
(and
(Flag181-)
(Flag181prime-)
)

)
(:action Flag181Action-50
:parameters ()
:precondition
(and
(Flag135-)
(Flag135prime-)
)
:effect
(and
(Flag181-)
(Flag181prime-)
)

)
(:action Flag181Action-51
:parameters ()
:precondition
(and
(Flag83-)
(Flag83prime-)
)
:effect
(and
(Flag181-)
(Flag181prime-)
)

)
(:action Flag181Action-52
:parameters ()
:precondition
(and
(Flag180-)
(Flag180prime-)
)
:effect
(and
(Flag181-)
(Flag181prime-)
)

)
(:action Flag181Action-53
:parameters ()
:precondition
(and
(Flag38-)
(Flag38prime-)
)
:effect
(and
(Flag181-)
(Flag181prime-)
)

)
(:action Flag181Action-54
:parameters ()
:precondition
(and
(Flag122-)
(Flag122prime-)
)
:effect
(and
(Flag181-)
(Flag181prime-)
)

)
(:action Flag181Action-55
:parameters ()
:precondition
(and
(Flag166-)
(Flag166prime-)
)
:effect
(and
(Flag181-)
(Flag181prime-)
)

)
(:action Flag181Action-56
:parameters ()
:precondition
(and
(Flag103-)
(Flag103prime-)
)
:effect
(and
(Flag181-)
(Flag181prime-)
)

)
(:action Flag181Action-57
:parameters ()
:precondition
(and
(Flag95-)
(Flag95prime-)
)
:effect
(and
(Flag181-)
(Flag181prime-)
)

)
(:action Flag181Action-58
:parameters ()
:precondition
(and
(Flag148-)
(Flag148prime-)
)
:effect
(and
(Flag181-)
(Flag181prime-)
)

)
(:action Flag181Action-59
:parameters ()
:precondition
(and
(Flag48-)
(Flag48prime-)
)
:effect
(and
(Flag181-)
(Flag181prime-)
)

)
(:action Flag181Action-60
:parameters ()
:precondition
(and
(Flag158-)
(Flag158prime-)
)
:effect
(and
(Flag181-)
(Flag181prime-)
)

)
(:action Flag181Action-61
:parameters ()
:precondition
(and
(Flag174-)
(Flag174prime-)
)
:effect
(and
(Flag181-)
(Flag181prime-)
)

)
(:action Flag181Action-62
:parameters ()
:precondition
(and
(Flag123-)
(Flag123prime-)
)
:effect
(and
(Flag181-)
(Flag181prime-)
)

)
(:action Flag181Action-63
:parameters ()
:precondition
(and
(Flag137-)
(Flag137prime-)
)
:effect
(and
(Flag181-)
(Flag181prime-)
)

)
(:action Prim181Action
:parameters ()
:precondition
(and
(not (Flag62-))
(Flag62prime-)
(not (Flag100-))
(Flag100prime-)
(not (Flag52-))
(Flag52prime-)
(not (Flag168-))
(Flag168prime-)
(not (Flag41-))
(Flag41prime-)
(not (Flag160-))
(Flag160prime-)
(not (Flag169-))
(Flag169prime-)
(not (Flag161-))
(Flag161prime-)
(not (Flag172-))
(Flag172prime-)
(not (Flag157-))
(Flag157prime-)
(not (Flag176-))
(Flag176prime-)
(not (Flag163-))
(Flag163prime-)
(not (Flag164-))
(Flag164prime-)
(not (Flag35-))
(Flag35prime-)
(not (Flag150-))
(Flag150prime-)
(not (Flag177-))
(Flag177prime-)
(not (Flag104-))
(Flag104prime-)
(not (Flag51-))
(Flag51prime-)
(not (Flag142-))
(Flag142prime-)
(not (Flag111-))
(Flag111prime-)
(not (Flag114-))
(Flag114prime-)
(not (Flag129-))
(Flag129prime-)
(not (Flag69-))
(Flag69prime-)
(not (Flag115-))
(Flag115prime-)
(not (Flag86-))
(Flag86prime-)
(not (Flag45-))
(Flag45prime-)
(not (Flag87-))
(Flag87prime-)
(not (Flag72-))
(Flag72prime-)
(not (Flag117-))
(Flag117prime-)
(not (Flag178-))
(Flag178prime-)
(not (Flag170-))
(Flag170prime-)
(not (Flag179-))
(Flag179prime-)
(not (Flag146-))
(Flag146prime-)
(not (Flag141-))
(Flag141prime-)
(not (Flag126-))
(Flag126prime-)
(not (Flag154-))
(Flag154prime-)
(not (Flag61-))
(Flag61prime-)
(not (Flag131-))
(Flag131prime-)
(not (Flag109-))
(Flag109prime-)
(not (Flag155-))
(Flag155prime-)
(not (Flag132-))
(Flag132prime-)
(not (Flag171-))
(Flag171prime-)
(not (Flag156-))
(Flag156prime-)
(not (Flag147-))
(Flag147prime-)
(not (Flag76-))
(Flag76prime-)
(not (Flag59-))
(Flag59prime-)
(not (Flag78-))
(Flag78prime-)
(not (Flag90-))
(Flag90prime-)
(not (Flag97-))
(Flag97prime-)
(not (Flag79-))
(Flag79prime-)
(not (Flag135-))
(Flag135prime-)
(not (Flag83-))
(Flag83prime-)
(not (Flag180-))
(Flag180prime-)
(not (Flag38-))
(Flag38prime-)
(not (Flag122-))
(Flag122prime-)
(not (Flag166-))
(Flag166prime-)
(not (Flag103-))
(Flag103prime-)
(not (Flag95-))
(Flag95prime-)
(not (Flag148-))
(Flag148prime-)
(not (Flag48-))
(Flag48prime-)
(not (Flag158-))
(Flag158prime-)
(not (Flag174-))
(Flag174prime-)
(not (Flag123-))
(Flag123prime-)
(not (Flag137-))
(Flag137prime-)
)
:effect
(and
(Flag181prime-)
(not (Flag181-))
)

)
(:action Flag187Action-0
:parameters ()
:precondition
(and
(Flag168-)
(Flag168prime-)
)
:effect
(and
(Flag187-)
(Flag187prime-)
)

)
(:action Flag187Action-1
:parameters ()
:precondition
(and
(Flag100-)
(Flag100prime-)
)
:effect
(and
(Flag187-)
(Flag187prime-)
)

)
(:action Flag187Action-2
:parameters ()
:precondition
(and
(Flag52-)
(Flag52prime-)
)
:effect
(and
(Flag187-)
(Flag187prime-)
)

)
(:action Flag187Action-3
:parameters ()
:precondition
(and
(Flag41-)
(Flag41prime-)
)
:effect
(and
(Flag187-)
(Flag187prime-)
)

)
(:action Flag187Action-4
:parameters ()
:precondition
(and
(Flag160-)
(Flag160prime-)
)
:effect
(and
(Flag187-)
(Flag187prime-)
)

)
(:action Flag187Action-5
:parameters ()
:precondition
(and
(Flag69-)
(Flag69prime-)
)
:effect
(and
(Flag187-)
(Flag187prime-)
)

)
(:action Flag187Action-6
:parameters ()
:precondition
(and
(Flag172-)
(Flag172prime-)
)
:effect
(and
(Flag187-)
(Flag187prime-)
)

)
(:action Flag187Action-7
:parameters ()
:precondition
(and
(Flag157-)
(Flag157prime-)
)
:effect
(and
(Flag187-)
(Flag187prime-)
)

)
(:action Flag187Action-8
:parameters ()
:precondition
(and
(Flag163-)
(Flag163prime-)
)
:effect
(and
(Flag187-)
(Flag187prime-)
)

)
(:action Flag187Action-9
:parameters ()
:precondition
(and
(Flag164-)
(Flag164prime-)
)
:effect
(and
(Flag187-)
(Flag187prime-)
)

)
(:action Flag187Action-10
:parameters ()
:precondition
(and
(Flag35-)
(Flag35prime-)
)
:effect
(and
(Flag187-)
(Flag187prime-)
)

)
(:action Flag187Action-11
:parameters ()
:precondition
(and
(Flag150-)
(Flag150prime-)
)
:effect
(and
(Flag187-)
(Flag187prime-)
)

)
(:action Flag187Action-12
:parameters ()
:precondition
(and
(Flag182-)
(Flag182prime-)
)
:effect
(and
(Flag187-)
(Flag187prime-)
)

)
(:action Flag187Action-13
:parameters ()
:precondition
(and
(Flag141-)
(Flag141prime-)
)
:effect
(and
(Flag187-)
(Flag187prime-)
)

)
(:action Flag187Action-14
:parameters ()
:precondition
(and
(Flag104-)
(Flag104prime-)
)
:effect
(and
(Flag187-)
(Flag187prime-)
)

)
(:action Flag187Action-15
:parameters ()
:precondition
(and
(Flag51-)
(Flag51prime-)
)
:effect
(and
(Flag187-)
(Flag187prime-)
)

)
(:action Flag187Action-16
:parameters ()
:precondition
(and
(Flag142-)
(Flag142prime-)
)
:effect
(and
(Flag187-)
(Flag187prime-)
)

)
(:action Flag187Action-17
:parameters ()
:precondition
(and
(Flag111-)
(Flag111prime-)
)
:effect
(and
(Flag187-)
(Flag187prime-)
)

)
(:action Flag187Action-18
:parameters ()
:precondition
(and
(Flag114-)
(Flag114prime-)
)
:effect
(and
(Flag187-)
(Flag187prime-)
)

)
(:action Flag187Action-19
:parameters ()
:precondition
(and
(Flag129-)
(Flag129prime-)
)
:effect
(and
(Flag187-)
(Flag187prime-)
)

)
(:action Flag187Action-20
:parameters ()
:precondition
(and
(Flag86-)
(Flag86prime-)
)
:effect
(and
(Flag187-)
(Flag187prime-)
)

)
(:action Flag187Action-21
:parameters ()
:precondition
(and
(Flag87-)
(Flag87prime-)
)
:effect
(and
(Flag187-)
(Flag187prime-)
)

)
(:action Flag187Action-22
:parameters ()
:precondition
(and
(Flag72-)
(Flag72prime-)
)
:effect
(and
(Flag187-)
(Flag187prime-)
)

)
(:action Flag187Action-23
:parameters ()
:precondition
(and
(Flag178-)
(Flag178prime-)
)
:effect
(and
(Flag187-)
(Flag187prime-)
)

)
(:action Flag187Action-24
:parameters ()
:precondition
(and
(Flag170-)
(Flag170prime-)
)
:effect
(and
(Flag187-)
(Flag187prime-)
)

)
(:action Flag187Action-25
:parameters ()
:precondition
(and
(Flag146-)
(Flag146prime-)
)
:effect
(and
(Flag187-)
(Flag187prime-)
)

)
(:action Flag187Action-26
:parameters ()
:precondition
(and
(Flag177-)
(Flag177prime-)
)
:effect
(and
(Flag187-)
(Flag187prime-)
)

)
(:action Flag187Action-27
:parameters ()
:precondition
(and
(Flag131-)
(Flag131prime-)
)
:effect
(and
(Flag187-)
(Flag187prime-)
)

)
(:action Flag187Action-28
:parameters ()
:precondition
(and
(Flag154-)
(Flag154prime-)
)
:effect
(and
(Flag187-)
(Flag187prime-)
)

)
(:action Flag187Action-29
:parameters ()
:precondition
(and
(Flag61-)
(Flag61prime-)
)
:effect
(and
(Flag187-)
(Flag187prime-)
)

)
(:action Flag187Action-30
:parameters ()
:precondition
(and
(Flag183-)
(Flag183prime-)
)
:effect
(and
(Flag187-)
(Flag187prime-)
)

)
(:action Flag187Action-31
:parameters ()
:precondition
(and
(Flag109-)
(Flag109prime-)
)
:effect
(and
(Flag187-)
(Flag187prime-)
)

)
(:action Flag187Action-32
:parameters ()
:precondition
(and
(Flag155-)
(Flag155prime-)
)
:effect
(and
(Flag187-)
(Flag187prime-)
)

)
(:action Flag187Action-33
:parameters ()
:precondition
(and
(Flag184-)
(Flag184prime-)
)
:effect
(and
(Flag187-)
(Flag187prime-)
)

)
(:action Flag187Action-34
:parameters ()
:precondition
(and
(Flag171-)
(Flag171prime-)
)
:effect
(and
(Flag187-)
(Flag187prime-)
)

)
(:action Flag187Action-35
:parameters ()
:precondition
(and
(Flag147-)
(Flag147prime-)
)
:effect
(and
(Flag187-)
(Flag187prime-)
)

)
(:action Flag187Action-36
:parameters ()
:precondition
(and
(Flag76-)
(Flag76prime-)
)
:effect
(and
(Flag187-)
(Flag187prime-)
)

)
(:action Flag187Action-37
:parameters ()
:precondition
(and
(Flag59-)
(Flag59prime-)
)
:effect
(and
(Flag187-)
(Flag187prime-)
)

)
(:action Flag187Action-38
:parameters ()
:precondition
(and
(Flag90-)
(Flag90prime-)
)
:effect
(and
(Flag187-)
(Flag187prime-)
)

)
(:action Flag187Action-39
:parameters ()
:precondition
(and
(Flag79-)
(Flag79prime-)
)
:effect
(and
(Flag187-)
(Flag187prime-)
)

)
(:action Flag187Action-40
:parameters ()
:precondition
(and
(Flag132-)
(Flag132prime-)
)
:effect
(and
(Flag187-)
(Flag187prime-)
)

)
(:action Flag187Action-41
:parameters ()
:precondition
(and
(Flag135-)
(Flag135prime-)
)
:effect
(and
(Flag187-)
(Flag187prime-)
)

)
(:action Flag187Action-42
:parameters ()
:precondition
(and
(Flag83-)
(Flag83prime-)
)
:effect
(and
(Flag187-)
(Flag187prime-)
)

)
(:action Flag187Action-43
:parameters ()
:precondition
(and
(Flag180-)
(Flag180prime-)
)
:effect
(and
(Flag187-)
(Flag187prime-)
)

)
(:action Flag187Action-44
:parameters ()
:precondition
(and
(Flag185-)
(Flag185prime-)
)
:effect
(and
(Flag187-)
(Flag187prime-)
)

)
(:action Flag187Action-45
:parameters ()
:precondition
(and
(Flag38-)
(Flag38prime-)
)
:effect
(and
(Flag187-)
(Flag187prime-)
)

)
(:action Flag187Action-46
:parameters ()
:precondition
(and
(Flag122-)
(Flag122prime-)
)
:effect
(and
(Flag187-)
(Flag187prime-)
)

)
(:action Flag187Action-47
:parameters ()
:precondition
(and
(Flag166-)
(Flag166prime-)
)
:effect
(and
(Flag187-)
(Flag187prime-)
)

)
(:action Flag187Action-48
:parameters ()
:precondition
(and
(Flag95-)
(Flag95prime-)
)
:effect
(and
(Flag187-)
(Flag187prime-)
)

)
(:action Flag187Action-49
:parameters ()
:precondition
(and
(Flag186-)
(Flag186prime-)
)
:effect
(and
(Flag187-)
(Flag187prime-)
)

)
(:action Flag187Action-50
:parameters ()
:precondition
(and
(Flag48-)
(Flag48prime-)
)
:effect
(and
(Flag187-)
(Flag187prime-)
)

)
(:action Flag187Action-51
:parameters ()
:precondition
(and
(Flag158-)
(Flag158prime-)
)
:effect
(and
(Flag187-)
(Flag187prime-)
)

)
(:action Flag187Action-52
:parameters ()
:precondition
(and
(Flag174-)
(Flag174prime-)
)
:effect
(and
(Flag187-)
(Flag187prime-)
)

)
(:action Flag187Action-53
:parameters ()
:precondition
(and
(Flag123-)
(Flag123prime-)
)
:effect
(and
(Flag187-)
(Flag187prime-)
)

)
(:action Flag187Action-54
:parameters ()
:precondition
(and
(Flag137-)
(Flag137prime-)
)
:effect
(and
(Flag187-)
(Flag187prime-)
)

)
(:action Prim187Action
:parameters ()
:precondition
(and
(not (Flag168-))
(Flag168prime-)
(not (Flag100-))
(Flag100prime-)
(not (Flag52-))
(Flag52prime-)
(not (Flag41-))
(Flag41prime-)
(not (Flag160-))
(Flag160prime-)
(not (Flag69-))
(Flag69prime-)
(not (Flag172-))
(Flag172prime-)
(not (Flag157-))
(Flag157prime-)
(not (Flag163-))
(Flag163prime-)
(not (Flag164-))
(Flag164prime-)
(not (Flag35-))
(Flag35prime-)
(not (Flag150-))
(Flag150prime-)
(not (Flag182-))
(Flag182prime-)
(not (Flag141-))
(Flag141prime-)
(not (Flag104-))
(Flag104prime-)
(not (Flag51-))
(Flag51prime-)
(not (Flag142-))
(Flag142prime-)
(not (Flag111-))
(Flag111prime-)
(not (Flag114-))
(Flag114prime-)
(not (Flag129-))
(Flag129prime-)
(not (Flag86-))
(Flag86prime-)
(not (Flag87-))
(Flag87prime-)
(not (Flag72-))
(Flag72prime-)
(not (Flag178-))
(Flag178prime-)
(not (Flag170-))
(Flag170prime-)
(not (Flag146-))
(Flag146prime-)
(not (Flag177-))
(Flag177prime-)
(not (Flag131-))
(Flag131prime-)
(not (Flag154-))
(Flag154prime-)
(not (Flag61-))
(Flag61prime-)
(not (Flag183-))
(Flag183prime-)
(not (Flag109-))
(Flag109prime-)
(not (Flag155-))
(Flag155prime-)
(not (Flag184-))
(Flag184prime-)
(not (Flag171-))
(Flag171prime-)
(not (Flag147-))
(Flag147prime-)
(not (Flag76-))
(Flag76prime-)
(not (Flag59-))
(Flag59prime-)
(not (Flag90-))
(Flag90prime-)
(not (Flag79-))
(Flag79prime-)
(not (Flag132-))
(Flag132prime-)
(not (Flag135-))
(Flag135prime-)
(not (Flag83-))
(Flag83prime-)
(not (Flag180-))
(Flag180prime-)
(not (Flag185-))
(Flag185prime-)
(not (Flag38-))
(Flag38prime-)
(not (Flag122-))
(Flag122prime-)
(not (Flag166-))
(Flag166prime-)
(not (Flag95-))
(Flag95prime-)
(not (Flag186-))
(Flag186prime-)
(not (Flag48-))
(Flag48prime-)
(not (Flag158-))
(Flag158prime-)
(not (Flag174-))
(Flag174prime-)
(not (Flag123-))
(Flag123prime-)
(not (Flag137-))
(Flag137prime-)
)
:effect
(and
(Flag187prime-)
(not (Flag187-))
)

)
(:action Flag191Action-0
:parameters ()
:precondition
(and
(Flag168-)
(Flag168prime-)
)
:effect
(and
(Flag191-)
(Flag191prime-)
)

)
(:action Flag191Action-1
:parameters ()
:precondition
(and
(Flag100-)
(Flag100prime-)
)
:effect
(and
(Flag191-)
(Flag191prime-)
)

)
(:action Flag191Action-2
:parameters ()
:precondition
(and
(Flag52-)
(Flag52prime-)
)
:effect
(and
(Flag191-)
(Flag191prime-)
)

)
(:action Flag191Action-3
:parameters ()
:precondition
(and
(Flag41-)
(Flag41prime-)
)
:effect
(and
(Flag191-)
(Flag191prime-)
)

)
(:action Flag191Action-4
:parameters ()
:precondition
(and
(Flag188-)
(Flag188prime-)
)
:effect
(and
(Flag191-)
(Flag191prime-)
)

)
(:action Flag191Action-5
:parameters ()
:precondition
(and
(Flag69-)
(Flag69prime-)
)
:effect
(and
(Flag191-)
(Flag191prime-)
)

)
(:action Flag191Action-6
:parameters ()
:precondition
(and
(Flag172-)
(Flag172prime-)
)
:effect
(and
(Flag191-)
(Flag191prime-)
)

)
(:action Flag191Action-7
:parameters ()
:precondition
(and
(Flag157-)
(Flag157prime-)
)
:effect
(and
(Flag191-)
(Flag191prime-)
)

)
(:action Flag191Action-8
:parameters ()
:precondition
(and
(Flag164-)
(Flag164prime-)
)
:effect
(and
(Flag191-)
(Flag191prime-)
)

)
(:action Flag191Action-9
:parameters ()
:precondition
(and
(Flag150-)
(Flag150prime-)
)
:effect
(and
(Flag191-)
(Flag191prime-)
)

)
(:action Flag191Action-10
:parameters ()
:precondition
(and
(Flag182-)
(Flag182prime-)
)
:effect
(and
(Flag191-)
(Flag191prime-)
)

)
(:action Flag191Action-11
:parameters ()
:precondition
(and
(Flag141-)
(Flag141prime-)
)
:effect
(and
(Flag191-)
(Flag191prime-)
)

)
(:action Flag191Action-12
:parameters ()
:precondition
(and
(Flag104-)
(Flag104prime-)
)
:effect
(and
(Flag191-)
(Flag191prime-)
)

)
(:action Flag191Action-13
:parameters ()
:precondition
(and
(Flag51-)
(Flag51prime-)
)
:effect
(and
(Flag191-)
(Flag191prime-)
)

)
(:action Flag191Action-14
:parameters ()
:precondition
(and
(Flag142-)
(Flag142prime-)
)
:effect
(and
(Flag191-)
(Flag191prime-)
)

)
(:action Flag191Action-15
:parameters ()
:precondition
(and
(Flag111-)
(Flag111prime-)
)
:effect
(and
(Flag191-)
(Flag191prime-)
)

)
(:action Flag191Action-16
:parameters ()
:precondition
(and
(Flag114-)
(Flag114prime-)
)
:effect
(and
(Flag191-)
(Flag191prime-)
)

)
(:action Flag191Action-17
:parameters ()
:precondition
(and
(Flag129-)
(Flag129prime-)
)
:effect
(and
(Flag191-)
(Flag191prime-)
)

)
(:action Flag191Action-18
:parameters ()
:precondition
(and
(Flag87-)
(Flag87prime-)
)
:effect
(and
(Flag191-)
(Flag191prime-)
)

)
(:action Flag191Action-19
:parameters ()
:precondition
(and
(Flag76-)
(Flag76prime-)
)
:effect
(and
(Flag191-)
(Flag191prime-)
)

)
(:action Flag191Action-20
:parameters ()
:precondition
(and
(Flag178-)
(Flag178prime-)
)
:effect
(and
(Flag191-)
(Flag191prime-)
)

)
(:action Flag191Action-21
:parameters ()
:precondition
(and
(Flag170-)
(Flag170prime-)
)
:effect
(and
(Flag191-)
(Flag191prime-)
)

)
(:action Flag191Action-22
:parameters ()
:precondition
(and
(Flag177-)
(Flag177prime-)
)
:effect
(and
(Flag191-)
(Flag191prime-)
)

)
(:action Flag191Action-23
:parameters ()
:precondition
(and
(Flag189-)
(Flag189prime-)
)
:effect
(and
(Flag191-)
(Flag191prime-)
)

)
(:action Flag191Action-24
:parameters ()
:precondition
(and
(Flag160-)
(Flag160prime-)
)
:effect
(and
(Flag191-)
(Flag191prime-)
)

)
(:action Flag191Action-25
:parameters ()
:precondition
(and
(Flag154-)
(Flag154prime-)
)
:effect
(and
(Flag191-)
(Flag191prime-)
)

)
(:action Flag191Action-26
:parameters ()
:precondition
(and
(Flag83-)
(Flag83prime-)
)
:effect
(and
(Flag191-)
(Flag191prime-)
)

)
(:action Flag191Action-27
:parameters ()
:precondition
(and
(Flag184-)
(Flag184prime-)
)
:effect
(and
(Flag191-)
(Flag191prime-)
)

)
(:action Flag191Action-28
:parameters ()
:precondition
(and
(Flag171-)
(Flag171prime-)
)
:effect
(and
(Flag191-)
(Flag191prime-)
)

)
(:action Flag191Action-29
:parameters ()
:precondition
(and
(Flag147-)
(Flag147prime-)
)
:effect
(and
(Flag191-)
(Flag191prime-)
)

)
(:action Flag191Action-30
:parameters ()
:precondition
(and
(Flag59-)
(Flag59prime-)
)
:effect
(and
(Flag191-)
(Flag191prime-)
)

)
(:action Flag191Action-31
:parameters ()
:precondition
(and
(Flag86-)
(Flag86prime-)
)
:effect
(and
(Flag191-)
(Flag191prime-)
)

)
(:action Flag191Action-32
:parameters ()
:precondition
(and
(Flag132-)
(Flag132prime-)
)
:effect
(and
(Flag191-)
(Flag191prime-)
)

)
(:action Flag191Action-33
:parameters ()
:precondition
(and
(Flag180-)
(Flag180prime-)
)
:effect
(and
(Flag191-)
(Flag191prime-)
)

)
(:action Flag191Action-34
:parameters ()
:precondition
(and
(Flag185-)
(Flag185prime-)
)
:effect
(and
(Flag191-)
(Flag191prime-)
)

)
(:action Flag191Action-35
:parameters ()
:precondition
(and
(Flag38-)
(Flag38prime-)
)
:effect
(and
(Flag191-)
(Flag191prime-)
)

)
(:action Flag191Action-36
:parameters ()
:precondition
(and
(Flag122-)
(Flag122prime-)
)
:effect
(and
(Flag191-)
(Flag191prime-)
)

)
(:action Flag191Action-37
:parameters ()
:precondition
(and
(Flag166-)
(Flag166prime-)
)
:effect
(and
(Flag191-)
(Flag191prime-)
)

)
(:action Flag191Action-38
:parameters ()
:precondition
(and
(Flag190-)
(Flag190prime-)
)
:effect
(and
(Flag191-)
(Flag191prime-)
)

)
(:action Flag191Action-39
:parameters ()
:precondition
(and
(Flag95-)
(Flag95prime-)
)
:effect
(and
(Flag191-)
(Flag191prime-)
)

)
(:action Flag191Action-40
:parameters ()
:precondition
(and
(Flag48-)
(Flag48prime-)
)
:effect
(and
(Flag191-)
(Flag191prime-)
)

)
(:action Flag191Action-41
:parameters ()
:precondition
(and
(Flag158-)
(Flag158prime-)
)
:effect
(and
(Flag191-)
(Flag191prime-)
)

)
(:action Flag191Action-42
:parameters ()
:precondition
(and
(Flag123-)
(Flag123prime-)
)
:effect
(and
(Flag191-)
(Flag191prime-)
)

)
(:action Flag191Action-43
:parameters ()
:precondition
(and
(Flag137-)
(Flag137prime-)
)
:effect
(and
(Flag191-)
(Flag191prime-)
)

)
(:action Prim191Action
:parameters ()
:precondition
(and
(not (Flag168-))
(Flag168prime-)
(not (Flag100-))
(Flag100prime-)
(not (Flag52-))
(Flag52prime-)
(not (Flag41-))
(Flag41prime-)
(not (Flag188-))
(Flag188prime-)
(not (Flag69-))
(Flag69prime-)
(not (Flag172-))
(Flag172prime-)
(not (Flag157-))
(Flag157prime-)
(not (Flag164-))
(Flag164prime-)
(not (Flag150-))
(Flag150prime-)
(not (Flag182-))
(Flag182prime-)
(not (Flag141-))
(Flag141prime-)
(not (Flag104-))
(Flag104prime-)
(not (Flag51-))
(Flag51prime-)
(not (Flag142-))
(Flag142prime-)
(not (Flag111-))
(Flag111prime-)
(not (Flag114-))
(Flag114prime-)
(not (Flag129-))
(Flag129prime-)
(not (Flag87-))
(Flag87prime-)
(not (Flag76-))
(Flag76prime-)
(not (Flag178-))
(Flag178prime-)
(not (Flag170-))
(Flag170prime-)
(not (Flag177-))
(Flag177prime-)
(not (Flag189-))
(Flag189prime-)
(not (Flag160-))
(Flag160prime-)
(not (Flag154-))
(Flag154prime-)
(not (Flag83-))
(Flag83prime-)
(not (Flag184-))
(Flag184prime-)
(not (Flag171-))
(Flag171prime-)
(not (Flag147-))
(Flag147prime-)
(not (Flag59-))
(Flag59prime-)
(not (Flag86-))
(Flag86prime-)
(not (Flag132-))
(Flag132prime-)
(not (Flag180-))
(Flag180prime-)
(not (Flag185-))
(Flag185prime-)
(not (Flag38-))
(Flag38prime-)
(not (Flag122-))
(Flag122prime-)
(not (Flag166-))
(Flag166prime-)
(not (Flag190-))
(Flag190prime-)
(not (Flag95-))
(Flag95prime-)
(not (Flag48-))
(Flag48prime-)
(not (Flag158-))
(Flag158prime-)
(not (Flag123-))
(Flag123prime-)
(not (Flag137-))
(Flag137prime-)
)
:effect
(and
(Flag191prime-)
(not (Flag191-))
)

)
(:action Flag194Action-0
:parameters ()
:precondition
(and
(Flag168-)
(Flag168prime-)
)
:effect
(and
(Flag194-)
(Flag194prime-)
)

)
(:action Flag194Action-1
:parameters ()
:precondition
(and
(Flag100-)
(Flag100prime-)
)
:effect
(and
(Flag194-)
(Flag194prime-)
)

)
(:action Flag194Action-2
:parameters ()
:precondition
(and
(Flag41-)
(Flag41prime-)
)
:effect
(and
(Flag194-)
(Flag194prime-)
)

)
(:action Flag194Action-3
:parameters ()
:precondition
(and
(Flag188-)
(Flag188prime-)
)
:effect
(and
(Flag194-)
(Flag194prime-)
)

)
(:action Flag194Action-4
:parameters ()
:precondition
(and
(Flag69-)
(Flag69prime-)
)
:effect
(and
(Flag194-)
(Flag194prime-)
)

)
(:action Flag194Action-5
:parameters ()
:precondition
(and
(Flag172-)
(Flag172prime-)
)
:effect
(and
(Flag194-)
(Flag194prime-)
)

)
(:action Flag194Action-6
:parameters ()
:precondition
(and
(Flag157-)
(Flag157prime-)
)
:effect
(and
(Flag194-)
(Flag194prime-)
)

)
(:action Flag194Action-7
:parameters ()
:precondition
(and
(Flag182-)
(Flag182prime-)
)
:effect
(and
(Flag194-)
(Flag194prime-)
)

)
(:action Flag194Action-8
:parameters ()
:precondition
(and
(Flag177-)
(Flag177prime-)
)
:effect
(and
(Flag194-)
(Flag194prime-)
)

)
(:action Flag194Action-9
:parameters ()
:precondition
(and
(Flag111-)
(Flag111prime-)
)
:effect
(and
(Flag194-)
(Flag194prime-)
)

)
(:action Flag194Action-10
:parameters ()
:precondition
(and
(Flag114-)
(Flag114prime-)
)
:effect
(and
(Flag194-)
(Flag194prime-)
)

)
(:action Flag194Action-11
:parameters ()
:precondition
(and
(Flag87-)
(Flag87prime-)
)
:effect
(and
(Flag194-)
(Flag194prime-)
)

)
(:action Flag194Action-12
:parameters ()
:precondition
(and
(Flag76-)
(Flag76prime-)
)
:effect
(and
(Flag194-)
(Flag194prime-)
)

)
(:action Flag194Action-13
:parameters ()
:precondition
(and
(Flag178-)
(Flag178prime-)
)
:effect
(and
(Flag194-)
(Flag194prime-)
)

)
(:action Flag194Action-14
:parameters ()
:precondition
(and
(Flag170-)
(Flag170prime-)
)
:effect
(and
(Flag194-)
(Flag194prime-)
)

)
(:action Flag194Action-15
:parameters ()
:precondition
(and
(Flag154-)
(Flag154prime-)
)
:effect
(and
(Flag194-)
(Flag194prime-)
)

)
(:action Flag194Action-16
:parameters ()
:precondition
(and
(Flag132-)
(Flag132prime-)
)
:effect
(and
(Flag194-)
(Flag194prime-)
)

)
(:action Flag194Action-17
:parameters ()
:precondition
(and
(Flag147-)
(Flag147prime-)
)
:effect
(and
(Flag194-)
(Flag194prime-)
)

)
(:action Flag194Action-18
:parameters ()
:precondition
(and
(Flag59-)
(Flag59prime-)
)
:effect
(and
(Flag194-)
(Flag194prime-)
)

)
(:action Flag194Action-19
:parameters ()
:precondition
(and
(Flag86-)
(Flag86prime-)
)
:effect
(and
(Flag194-)
(Flag194prime-)
)

)
(:action Flag194Action-20
:parameters ()
:precondition
(and
(Flag184-)
(Flag184prime-)
)
:effect
(and
(Flag194-)
(Flag194prime-)
)

)
(:action Flag194Action-21
:parameters ()
:precondition
(and
(Flag192-)
(Flag192prime-)
)
:effect
(and
(Flag194-)
(Flag194prime-)
)

)
(:action Flag194Action-22
:parameters ()
:precondition
(and
(Flag190-)
(Flag190prime-)
)
:effect
(and
(Flag194-)
(Flag194prime-)
)

)
(:action Flag194Action-23
:parameters ()
:precondition
(and
(Flag51-)
(Flag51prime-)
)
:effect
(and
(Flag194-)
(Flag194prime-)
)

)
(:action Flag194Action-24
:parameters ()
:precondition
(and
(Flag166-)
(Flag166prime-)
)
:effect
(and
(Flag194-)
(Flag194prime-)
)

)
(:action Flag194Action-25
:parameters ()
:precondition
(and
(Flag48-)
(Flag48prime-)
)
:effect
(and
(Flag194-)
(Flag194prime-)
)

)
(:action Flag194Action-26
:parameters ()
:precondition
(and
(Flag158-)
(Flag158prime-)
)
:effect
(and
(Flag194-)
(Flag194prime-)
)

)
(:action Flag194Action-27
:parameters ()
:precondition
(and
(Flag193-)
(Flag193prime-)
)
:effect
(and
(Flag194-)
(Flag194prime-)
)

)
(:action Flag194Action-28
:parameters ()
:precondition
(and
(Flag123-)
(Flag123prime-)
)
:effect
(and
(Flag194-)
(Flag194prime-)
)

)
(:action Flag194Action-29
:parameters ()
:precondition
(and
(Flag137-)
(Flag137prime-)
)
:effect
(and
(Flag194-)
(Flag194prime-)
)

)
(:action Prim194Action
:parameters ()
:precondition
(and
(not (Flag168-))
(Flag168prime-)
(not (Flag100-))
(Flag100prime-)
(not (Flag41-))
(Flag41prime-)
(not (Flag188-))
(Flag188prime-)
(not (Flag69-))
(Flag69prime-)
(not (Flag172-))
(Flag172prime-)
(not (Flag157-))
(Flag157prime-)
(not (Flag182-))
(Flag182prime-)
(not (Flag177-))
(Flag177prime-)
(not (Flag111-))
(Flag111prime-)
(not (Flag114-))
(Flag114prime-)
(not (Flag87-))
(Flag87prime-)
(not (Flag76-))
(Flag76prime-)
(not (Flag178-))
(Flag178prime-)
(not (Flag170-))
(Flag170prime-)
(not (Flag154-))
(Flag154prime-)
(not (Flag132-))
(Flag132prime-)
(not (Flag147-))
(Flag147prime-)
(not (Flag59-))
(Flag59prime-)
(not (Flag86-))
(Flag86prime-)
(not (Flag184-))
(Flag184prime-)
(not (Flag192-))
(Flag192prime-)
(not (Flag190-))
(Flag190prime-)
(not (Flag51-))
(Flag51prime-)
(not (Flag166-))
(Flag166prime-)
(not (Flag48-))
(Flag48prime-)
(not (Flag158-))
(Flag158prime-)
(not (Flag193-))
(Flag193prime-)
(not (Flag123-))
(Flag123prime-)
(not (Flag137-))
(Flag137prime-)
)
:effect
(and
(Flag194prime-)
(not (Flag194-))
)

)
(:action Flag198Action-0
:parameters ()
:precondition
(and
(Flag177-)
(Flag177prime-)
)
:effect
(and
(Flag198-)
(Flag198prime-)
)

)
(:action Flag198Action-1
:parameters ()
:precondition
(and
(Flag137-)
(Flag137prime-)
)
:effect
(and
(Flag198-)
(Flag198prime-)
)

)
(:action Flag198Action-2
:parameters ()
:precondition
(and
(Flag100-)
(Flag100prime-)
)
:effect
(and
(Flag198-)
(Flag198prime-)
)

)
(:action Flag198Action-3
:parameters ()
:precondition
(and
(Flag190-)
(Flag190prime-)
)
:effect
(and
(Flag198-)
(Flag198prime-)
)

)
(:action Flag198Action-4
:parameters ()
:precondition
(and
(Flag41-)
(Flag41prime-)
)
:effect
(and
(Flag198-)
(Flag198prime-)
)

)
(:action Flag198Action-5
:parameters ()
:precondition
(and
(Flag114-)
(Flag114prime-)
)
:effect
(and
(Flag198-)
(Flag198prime-)
)

)
(:action Flag198Action-6
:parameters ()
:precondition
(and
(Flag51-)
(Flag51prime-)
)
:effect
(and
(Flag198-)
(Flag198prime-)
)

)
(:action Flag198Action-7
:parameters ()
:precondition
(and
(Flag166-)
(Flag166prime-)
)
:effect
(and
(Flag198-)
(Flag198prime-)
)

)
(:action Flag198Action-8
:parameters ()
:precondition
(and
(Flag172-)
(Flag172prime-)
)
:effect
(and
(Flag198-)
(Flag198prime-)
)

)
(:action Flag198Action-9
:parameters ()
:precondition
(and
(Flag157-)
(Flag157prime-)
)
:effect
(and
(Flag198-)
(Flag198prime-)
)

)
(:action Flag198Action-10
:parameters ()
:precondition
(and
(Flag147-)
(Flag147prime-)
)
:effect
(and
(Flag198-)
(Flag198prime-)
)

)
(:action Flag198Action-11
:parameters ()
:precondition
(and
(Flag76-)
(Flag76prime-)
)
:effect
(and
(Flag198-)
(Flag198prime-)
)

)
(:action Flag198Action-12
:parameters ()
:precondition
(and
(Flag196-)
(Flag196prime-)
)
:effect
(and
(Flag198-)
(Flag198prime-)
)

)
(:action Flag198Action-13
:parameters ()
:precondition
(and
(Flag86-)
(Flag86prime-)
)
:effect
(and
(Flag198-)
(Flag198prime-)
)

)
(:action Flag198Action-14
:parameters ()
:precondition
(and
(Flag197-)
(Flag197prime-)
)
:effect
(and
(Flag198-)
(Flag198prime-)
)

)
(:action Flag198Action-15
:parameters ()
:precondition
(and
(Flag182-)
(Flag182prime-)
)
:effect
(and
(Flag198-)
(Flag198prime-)
)

)
(:action Prim198Action
:parameters ()
:precondition
(and
(not (Flag177-))
(Flag177prime-)
(not (Flag137-))
(Flag137prime-)
(not (Flag100-))
(Flag100prime-)
(not (Flag190-))
(Flag190prime-)
(not (Flag41-))
(Flag41prime-)
(not (Flag114-))
(Flag114prime-)
(not (Flag51-))
(Flag51prime-)
(not (Flag166-))
(Flag166prime-)
(not (Flag172-))
(Flag172prime-)
(not (Flag157-))
(Flag157prime-)
(not (Flag147-))
(Flag147prime-)
(not (Flag76-))
(Flag76prime-)
(not (Flag196-))
(Flag196prime-)
(not (Flag86-))
(Flag86prime-)
(not (Flag197-))
(Flag197prime-)
(not (Flag182-))
(Flag182prime-)
)
:effect
(and
(Flag198prime-)
(not (Flag198-))
)

)
(:action Flag1Action
:parameters ()
:precondition
(and
(COLUMN2-ROBOT)
(ROW1-ROBOT)
(NOT-ERROR-)
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(Flag1-)
(Flag1prime-)
)

)
(:action Prim1Action-0
:parameters ()
:precondition
(and
(not (COLUMN2-ROBOT))
)
:effect
(and
(Flag1prime-)
(not (Flag1-))
)

)
(:action Flag3Action-0
:parameters ()
:precondition
(and
(LEFTOF3-ROBOT)
)
:effect
(and
(Flag3-)
(Flag3prime-)
)

)
(:action Flag3Action-1
:parameters ()
:precondition
(and
(RIGHTOF7-ROBOT)
)
:effect
(and
(Flag3-)
(Flag3prime-)
)

)
(:action Flag3Action-2
:parameters ()
:precondition
(and
(RIGHTOF8-ROBOT)
)
:effect
(and
(Flag3-)
(Flag3prime-)
)

)
(:action Flag3Action-3
:parameters ()
:precondition
(and
(RIGHTOF15-ROBOT)
)
:effect
(and
(Flag3-)
(Flag3prime-)
)

)
(:action Flag3Action-4
:parameters ()
:precondition
(and
(LEFTOF2-ROBOT)
)
:effect
(and
(Flag3-)
(Flag3prime-)
)

)
(:action Flag3Action-5
:parameters ()
:precondition
(and
(LEFTOF9-ROBOT)
)
:effect
(and
(Flag3-)
(Flag3prime-)
)

)
(:action Flag3Action-7
:parameters ()
:precondition
(and
(RIGHTOF0-ROBOT)
)
:effect
(and
(Flag3-)
(Flag3prime-)
)

)
(:action Flag3Action-8
:parameters ()
:precondition
(and
(LEFTOF8-ROBOT)
)
:effect
(and
(Flag3-)
(Flag3prime-)
)

)
(:action Flag3Action-9
:parameters ()
:precondition
(and
(LEFTOF7-ROBOT)
)
:effect
(and
(Flag3-)
(Flag3prime-)
)

)
(:action Flag3Action-10
:parameters ()
:precondition
(and
(LEFTOF14-ROBOT)
)
:effect
(and
(Flag3-)
(Flag3prime-)
)

)
(:action Flag3Action-11
:parameters ()
:precondition
(and
(LEFTOF5-ROBOT)
)
:effect
(and
(Flag3-)
(Flag3prime-)
)

)
(:action Flag3Action-12
:parameters ()
:precondition
(and
(RIGHTOF1-ROBOT)
)
:effect
(and
(Flag3-)
(Flag3prime-)
)

)
(:action Flag3Action-13
:parameters ()
:precondition
(and
(LEFTOF12-ROBOT)
)
:effect
(and
(Flag3-)
(Flag3prime-)
)

)
(:action Flag3Action-14
:parameters ()
:precondition
(and
(RIGHTOF13-ROBOT)
)
:effect
(and
(Flag3-)
(Flag3prime-)
)

)
(:action Flag3Action-15
:parameters ()
:precondition
(and
(RIGHTOF14-ROBOT)
)
:effect
(and
(Flag3-)
(Flag3prime-)
)

)
(:action Flag3Action-16
:parameters ()
:precondition
(and
(RIGHTOF6-ROBOT)
)
:effect
(and
(Flag3-)
(Flag3prime-)
)

)
(:action Flag3Action-17
:parameters ()
:precondition
(and
(RIGHTOF9-ROBOT)
)
:effect
(and
(Flag3-)
(Flag3prime-)
)

)
(:action Flag3Action-18
:parameters ()
:precondition
(and
(LEFTOF6-ROBOT)
)
:effect
(and
(Flag3-)
(Flag3prime-)
)

)
(:action Flag3Action-19
:parameters ()
:precondition
(and
(RIGHTOF10-ROBOT)
)
:effect
(and
(Flag3-)
(Flag3prime-)
)

)
(:action Flag3Action-20
:parameters ()
:precondition
(and
(RIGHTOF11-ROBOT)
)
:effect
(and
(Flag3-)
(Flag3prime-)
)

)
(:action Flag3Action-21
:parameters ()
:precondition
(and
(LEFTOF11-ROBOT)
)
:effect
(and
(Flag3-)
(Flag3prime-)
)

)
(:action Flag3Action-22
:parameters ()
:precondition
(and
(RIGHTOF5-ROBOT)
)
:effect
(and
(Flag3-)
(Flag3prime-)
)

)
(:action Flag3Action-23
:parameters ()
:precondition
(and
(LEFTOF10-ROBOT)
)
:effect
(and
(Flag3-)
(Flag3prime-)
)

)
(:action Flag3Action-25
:parameters ()
:precondition
(and
(LEFTOF4-ROBOT)
)
:effect
(and
(Flag3-)
(Flag3prime-)
)

)
(:action Flag3Action-26
:parameters ()
:precondition
(and
(LEFTOF16-ROBOT)
)
:effect
(and
(Flag3-)
(Flag3prime-)
)

)
(:action Flag3Action-27
:parameters ()
:precondition
(and
(RIGHTOF4-ROBOT)
)
:effect
(and
(Flag3-)
(Flag3prime-)
)

)
(:action Flag3Action-28
:parameters ()
:precondition
(and
(LEFTOF1-ROBOT)
)
:effect
(and
(Flag3-)
(Flag3prime-)
)

)
(:action Flag3Action-29
:parameters ()
:precondition
(and
(RIGHTOF12-ROBOT)
)
:effect
(and
(Flag3-)
(Flag3prime-)
)

)
(:action Flag3Action-30
:parameters ()
:precondition
(and
(LEFTOF13-ROBOT)
)
:effect
(and
(Flag3-)
(Flag3prime-)
)

)
(:action Flag3Action-31
:parameters ()
:precondition
(and
(RIGHTOF3-ROBOT)
)
:effect
(and
(Flag3-)
(Flag3prime-)
)

)
(:action Prim3Action
:parameters ()
:precondition
(and
(not (LEFTOF3-ROBOT))
(not (RIGHTOF7-ROBOT))
(not (RIGHTOF8-ROBOT))
(not (RIGHTOF15-ROBOT))
(not (LEFTOF2-ROBOT))
(not (LEFTOF9-ROBOT))
(not (RIGHTOF2-ROBOT))
(not (RIGHTOF0-ROBOT))
(not (LEFTOF8-ROBOT))
(not (LEFTOF7-ROBOT))
(not (LEFTOF14-ROBOT))
(not (LEFTOF5-ROBOT))
(not (RIGHTOF1-ROBOT))
(not (LEFTOF12-ROBOT))
(not (RIGHTOF13-ROBOT))
(not (RIGHTOF14-ROBOT))
(not (RIGHTOF6-ROBOT))
(not (RIGHTOF9-ROBOT))
(not (LEFTOF6-ROBOT))
(not (RIGHTOF10-ROBOT))
(not (RIGHTOF11-ROBOT))
(not (LEFTOF11-ROBOT))
(not (RIGHTOF5-ROBOT))
(not (LEFTOF10-ROBOT))
(not (LEFTOF15-ROBOT))
(not (LEFTOF4-ROBOT))
(not (LEFTOF16-ROBOT))
(not (RIGHTOF4-ROBOT))
(not (LEFTOF1-ROBOT))
(not (RIGHTOF12-ROBOT))
(not (LEFTOF13-ROBOT))
(not (RIGHTOF3-ROBOT))
)
:effect
(and
(Flag3prime-)
(not (Flag3-))
)

)
(:action Flag5Action-0
:parameters ()
:precondition
(and
(RIGHTOF3-ROBOT)
)
:effect
(and
(Flag5-)
(Flag5prime-)
)

)
(:action Flag5Action-1
:parameters ()
:precondition
(and
(RIGHTOF8-ROBOT)
)
:effect
(and
(Flag5-)
(Flag5prime-)
)

)
(:action Flag5Action-2
:parameters ()
:precondition
(and
(RIGHTOF10-ROBOT)
)
:effect
(and
(Flag5-)
(Flag5prime-)
)

)
(:action Flag5Action-3
:parameters ()
:precondition
(and
(RIGHTOF5-ROBOT)
)
:effect
(and
(Flag5-)
(Flag5prime-)
)

)
(:action Flag5Action-4
:parameters ()
:precondition
(and
(RIGHTOF9-ROBOT)
)
:effect
(and
(Flag5-)
(Flag5prime-)
)

)
(:action Flag5Action-6
:parameters ()
:precondition
(and
(RIGHTOF14-ROBOT)
)
:effect
(and
(Flag5-)
(Flag5prime-)
)

)
(:action Flag5Action-7
:parameters ()
:precondition
(and
(RIGHTOF13-ROBOT)
)
:effect
(and
(Flag5-)
(Flag5prime-)
)

)
(:action Flag5Action-8
:parameters ()
:precondition
(and
(RIGHTOF12-ROBOT)
)
:effect
(and
(Flag5-)
(Flag5prime-)
)

)
(:action Flag5Action-9
:parameters ()
:precondition
(and
(RIGHTOF0-ROBOT)
)
:effect
(and
(Flag5-)
(Flag5prime-)
)

)
(:action Flag5Action-10
:parameters ()
:precondition
(and
(RIGHTOF4-ROBOT)
)
:effect
(and
(Flag5-)
(Flag5prime-)
)

)
(:action Flag5Action-11
:parameters ()
:precondition
(and
(RIGHTOF1-ROBOT)
)
:effect
(and
(Flag5-)
(Flag5prime-)
)

)
(:action Flag5Action-12
:parameters ()
:precondition
(and
(RIGHTOF7-ROBOT)
)
:effect
(and
(Flag5-)
(Flag5prime-)
)

)
(:action Flag5Action-13
:parameters ()
:precondition
(and
(RIGHTOF11-ROBOT)
)
:effect
(and
(Flag5-)
(Flag5prime-)
)

)
(:action Flag5Action-14
:parameters ()
:precondition
(and
(RIGHTOF15-ROBOT)
)
:effect
(and
(Flag5-)
(Flag5prime-)
)

)
(:action Flag5Action-15
:parameters ()
:precondition
(and
(RIGHTOF6-ROBOT)
)
:effect
(and
(Flag5-)
(Flag5prime-)
)

)
(:action Prim5Action
:parameters ()
:precondition
(and
(not (RIGHTOF3-ROBOT))
(not (RIGHTOF8-ROBOT))
(not (RIGHTOF10-ROBOT))
(not (RIGHTOF5-ROBOT))
(not (RIGHTOF9-ROBOT))
(not (RIGHTOF2-ROBOT))
(not (RIGHTOF14-ROBOT))
(not (RIGHTOF13-ROBOT))
(not (RIGHTOF12-ROBOT))
(not (RIGHTOF0-ROBOT))
(not (RIGHTOF4-ROBOT))
(not (RIGHTOF1-ROBOT))
(not (RIGHTOF7-ROBOT))
(not (RIGHTOF11-ROBOT))
(not (RIGHTOF15-ROBOT))
(not (RIGHTOF6-ROBOT))
)
:effect
(and
(Flag5prime-)
(not (Flag5-))
)

)
(:action Flag6Action-0
:parameters ()
:precondition
(and
(RIGHTOF3-ROBOT)
)
:effect
(and
(Flag6-)
(Flag6prime-)
)

)
(:action Flag6Action-1
:parameters ()
:precondition
(and
(RIGHTOF8-ROBOT)
)
:effect
(and
(Flag6-)
(Flag6prime-)
)

)
(:action Flag6Action-2
:parameters ()
:precondition
(and
(RIGHTOF5-ROBOT)
)
:effect
(and
(Flag6-)
(Flag6prime-)
)

)
(:action Flag6Action-3
:parameters ()
:precondition
(and
(RIGHTOF9-ROBOT)
)
:effect
(and
(Flag6-)
(Flag6prime-)
)

)
(:action Flag6Action-5
:parameters ()
:precondition
(and
(RIGHTOF14-ROBOT)
)
:effect
(and
(Flag6-)
(Flag6prime-)
)

)
(:action Flag6Action-6
:parameters ()
:precondition
(and
(RIGHTOF13-ROBOT)
)
:effect
(and
(Flag6-)
(Flag6prime-)
)

)
(:action Flag6Action-7
:parameters ()
:precondition
(and
(RIGHTOF12-ROBOT)
)
:effect
(and
(Flag6-)
(Flag6prime-)
)

)
(:action Flag6Action-8
:parameters ()
:precondition
(and
(RIGHTOF10-ROBOT)
)
:effect
(and
(Flag6-)
(Flag6prime-)
)

)
(:action Flag6Action-9
:parameters ()
:precondition
(and
(RIGHTOF4-ROBOT)
)
:effect
(and
(Flag6-)
(Flag6prime-)
)

)
(:action Flag6Action-10
:parameters ()
:precondition
(and
(RIGHTOF1-ROBOT)
)
:effect
(and
(Flag6-)
(Flag6prime-)
)

)
(:action Flag6Action-11
:parameters ()
:precondition
(and
(RIGHTOF7-ROBOT)
)
:effect
(and
(Flag6-)
(Flag6prime-)
)

)
(:action Flag6Action-12
:parameters ()
:precondition
(and
(RIGHTOF11-ROBOT)
)
:effect
(and
(Flag6-)
(Flag6prime-)
)

)
(:action Flag6Action-13
:parameters ()
:precondition
(and
(RIGHTOF15-ROBOT)
)
:effect
(and
(Flag6-)
(Flag6prime-)
)

)
(:action Flag6Action-14
:parameters ()
:precondition
(and
(RIGHTOF6-ROBOT)
)
:effect
(and
(Flag6-)
(Flag6prime-)
)

)
(:action Prim6Action
:parameters ()
:precondition
(and
(not (RIGHTOF3-ROBOT))
(not (RIGHTOF8-ROBOT))
(not (RIGHTOF5-ROBOT))
(not (RIGHTOF9-ROBOT))
(not (RIGHTOF2-ROBOT))
(not (RIGHTOF14-ROBOT))
(not (RIGHTOF13-ROBOT))
(not (RIGHTOF12-ROBOT))
(not (RIGHTOF10-ROBOT))
(not (RIGHTOF4-ROBOT))
(not (RIGHTOF1-ROBOT))
(not (RIGHTOF7-ROBOT))
(not (RIGHTOF11-ROBOT))
(not (RIGHTOF15-ROBOT))
(not (RIGHTOF6-ROBOT))
)
:effect
(and
(Flag6prime-)
(not (Flag6-))
)

)
(:action Flag7Action-0
:parameters ()
:precondition
(and
(RIGHTOF3-ROBOT)
)
:effect
(and
(Flag7-)
(Flag7prime-)
)

)
(:action Flag7Action-1
:parameters ()
:precondition
(and
(RIGHTOF5-ROBOT)
)
:effect
(and
(Flag7-)
(Flag7prime-)
)

)
(:action Flag7Action-2
:parameters ()
:precondition
(and
(RIGHTOF9-ROBOT)
)
:effect
(and
(Flag7-)
(Flag7prime-)
)

)
(:action Flag7Action-4
:parameters ()
:precondition
(and
(RIGHTOF14-ROBOT)
)
:effect
(and
(Flag7-)
(Flag7prime-)
)

)
(:action Flag7Action-5
:parameters ()
:precondition
(and
(RIGHTOF13-ROBOT)
)
:effect
(and
(Flag7-)
(Flag7prime-)
)

)
(:action Flag7Action-6
:parameters ()
:precondition
(and
(RIGHTOF12-ROBOT)
)
:effect
(and
(Flag7-)
(Flag7prime-)
)

)
(:action Flag7Action-7
:parameters ()
:precondition
(and
(RIGHTOF10-ROBOT)
)
:effect
(and
(Flag7-)
(Flag7prime-)
)

)
(:action Flag7Action-8
:parameters ()
:precondition
(and
(RIGHTOF4-ROBOT)
)
:effect
(and
(Flag7-)
(Flag7prime-)
)

)
(:action Flag7Action-9
:parameters ()
:precondition
(and
(RIGHTOF8-ROBOT)
)
:effect
(and
(Flag7-)
(Flag7prime-)
)

)
(:action Flag7Action-10
:parameters ()
:precondition
(and
(RIGHTOF7-ROBOT)
)
:effect
(and
(Flag7-)
(Flag7prime-)
)

)
(:action Flag7Action-11
:parameters ()
:precondition
(and
(RIGHTOF11-ROBOT)
)
:effect
(and
(Flag7-)
(Flag7prime-)
)

)
(:action Flag7Action-12
:parameters ()
:precondition
(and
(RIGHTOF15-ROBOT)
)
:effect
(and
(Flag7-)
(Flag7prime-)
)

)
(:action Flag7Action-13
:parameters ()
:precondition
(and
(RIGHTOF6-ROBOT)
)
:effect
(and
(Flag7-)
(Flag7prime-)
)

)
(:action Prim7Action
:parameters ()
:precondition
(and
(not (RIGHTOF3-ROBOT))
(not (RIGHTOF5-ROBOT))
(not (RIGHTOF9-ROBOT))
(not (RIGHTOF2-ROBOT))
(not (RIGHTOF14-ROBOT))
(not (RIGHTOF13-ROBOT))
(not (RIGHTOF12-ROBOT))
(not (RIGHTOF10-ROBOT))
(not (RIGHTOF4-ROBOT))
(not (RIGHTOF8-ROBOT))
(not (RIGHTOF7-ROBOT))
(not (RIGHTOF11-ROBOT))
(not (RIGHTOF15-ROBOT))
(not (RIGHTOF6-ROBOT))
)
:effect
(and
(Flag7prime-)
(not (Flag7-))
)

)
(:action Flag8Action-0
:parameters ()
:precondition
(and
(RIGHTOF3-ROBOT)
)
:effect
(and
(Flag8-)
(Flag8prime-)
)

)
(:action Flag8Action-1
:parameters ()
:precondition
(and
(RIGHTOF5-ROBOT)
)
:effect
(and
(Flag8-)
(Flag8prime-)
)

)
(:action Flag8Action-2
:parameters ()
:precondition
(and
(RIGHTOF9-ROBOT)
)
:effect
(and
(Flag8-)
(Flag8prime-)
)

)
(:action Flag8Action-3
:parameters ()
:precondition
(and
(RIGHTOF14-ROBOT)
)
:effect
(and
(Flag8-)
(Flag8prime-)
)

)
(:action Flag8Action-4
:parameters ()
:precondition
(and
(RIGHTOF13-ROBOT)
)
:effect
(and
(Flag8-)
(Flag8prime-)
)

)
(:action Flag8Action-5
:parameters ()
:precondition
(and
(RIGHTOF12-ROBOT)
)
:effect
(and
(Flag8-)
(Flag8prime-)
)

)
(:action Flag8Action-6
:parameters ()
:precondition
(and
(RIGHTOF10-ROBOT)
)
:effect
(and
(Flag8-)
(Flag8prime-)
)

)
(:action Flag8Action-7
:parameters ()
:precondition
(and
(RIGHTOF4-ROBOT)
)
:effect
(and
(Flag8-)
(Flag8prime-)
)

)
(:action Flag8Action-8
:parameters ()
:precondition
(and
(RIGHTOF8-ROBOT)
)
:effect
(and
(Flag8-)
(Flag8prime-)
)

)
(:action Flag8Action-9
:parameters ()
:precondition
(and
(RIGHTOF7-ROBOT)
)
:effect
(and
(Flag8-)
(Flag8prime-)
)

)
(:action Flag8Action-10
:parameters ()
:precondition
(and
(RIGHTOF11-ROBOT)
)
:effect
(and
(Flag8-)
(Flag8prime-)
)

)
(:action Flag8Action-11
:parameters ()
:precondition
(and
(RIGHTOF15-ROBOT)
)
:effect
(and
(Flag8-)
(Flag8prime-)
)

)
(:action Flag8Action-12
:parameters ()
:precondition
(and
(RIGHTOF6-ROBOT)
)
:effect
(and
(Flag8-)
(Flag8prime-)
)

)
(:action Prim8Action
:parameters ()
:precondition
(and
(not (RIGHTOF3-ROBOT))
(not (RIGHTOF5-ROBOT))
(not (RIGHTOF9-ROBOT))
(not (RIGHTOF14-ROBOT))
(not (RIGHTOF13-ROBOT))
(not (RIGHTOF12-ROBOT))
(not (RIGHTOF10-ROBOT))
(not (RIGHTOF4-ROBOT))
(not (RIGHTOF8-ROBOT))
(not (RIGHTOF7-ROBOT))
(not (RIGHTOF11-ROBOT))
(not (RIGHTOF15-ROBOT))
(not (RIGHTOF6-ROBOT))
)
:effect
(and
(Flag8prime-)
(not (Flag8-))
)

)
(:action Flag9Action-0
:parameters ()
:precondition
(and
(RIGHTOF5-ROBOT)
)
:effect
(and
(Flag9-)
(Flag9prime-)
)

)
(:action Flag9Action-1
:parameters ()
:precondition
(and
(RIGHTOF9-ROBOT)
)
:effect
(and
(Flag9-)
(Flag9prime-)
)

)
(:action Flag9Action-2
:parameters ()
:precondition
(and
(RIGHTOF14-ROBOT)
)
:effect
(and
(Flag9-)
(Flag9prime-)
)

)
(:action Flag9Action-3
:parameters ()
:precondition
(and
(RIGHTOF13-ROBOT)
)
:effect
(and
(Flag9-)
(Flag9prime-)
)

)
(:action Flag9Action-4
:parameters ()
:precondition
(and
(RIGHTOF12-ROBOT)
)
:effect
(and
(Flag9-)
(Flag9prime-)
)

)
(:action Flag9Action-5
:parameters ()
:precondition
(and
(RIGHTOF10-ROBOT)
)
:effect
(and
(Flag9-)
(Flag9prime-)
)

)
(:action Flag9Action-6
:parameters ()
:precondition
(and
(RIGHTOF4-ROBOT)
)
:effect
(and
(Flag9-)
(Flag9prime-)
)

)
(:action Flag9Action-7
:parameters ()
:precondition
(and
(RIGHTOF8-ROBOT)
)
:effect
(and
(Flag9-)
(Flag9prime-)
)

)
(:action Flag9Action-8
:parameters ()
:precondition
(and
(RIGHTOF7-ROBOT)
)
:effect
(and
(Flag9-)
(Flag9prime-)
)

)
(:action Flag9Action-9
:parameters ()
:precondition
(and
(RIGHTOF11-ROBOT)
)
:effect
(and
(Flag9-)
(Flag9prime-)
)

)
(:action Flag9Action-10
:parameters ()
:precondition
(and
(RIGHTOF15-ROBOT)
)
:effect
(and
(Flag9-)
(Flag9prime-)
)

)
(:action Flag9Action-11
:parameters ()
:precondition
(and
(RIGHTOF6-ROBOT)
)
:effect
(and
(Flag9-)
(Flag9prime-)
)

)
(:action Prim9Action
:parameters ()
:precondition
(and
(not (RIGHTOF5-ROBOT))
(not (RIGHTOF9-ROBOT))
(not (RIGHTOF14-ROBOT))
(not (RIGHTOF13-ROBOT))
(not (RIGHTOF12-ROBOT))
(not (RIGHTOF10-ROBOT))
(not (RIGHTOF4-ROBOT))
(not (RIGHTOF8-ROBOT))
(not (RIGHTOF7-ROBOT))
(not (RIGHTOF11-ROBOT))
(not (RIGHTOF15-ROBOT))
(not (RIGHTOF6-ROBOT))
)
:effect
(and
(Flag9prime-)
(not (Flag9-))
)

)
(:action Flag10Action-0
:parameters ()
:precondition
(and
(RIGHTOF5-ROBOT)
)
:effect
(and
(Flag10-)
(Flag10prime-)
)

)
(:action Flag10Action-1
:parameters ()
:precondition
(and
(RIGHTOF9-ROBOT)
)
:effect
(and
(Flag10-)
(Flag10prime-)
)

)
(:action Flag10Action-2
:parameters ()
:precondition
(and
(RIGHTOF14-ROBOT)
)
:effect
(and
(Flag10-)
(Flag10prime-)
)

)
(:action Flag10Action-3
:parameters ()
:precondition
(and
(RIGHTOF13-ROBOT)
)
:effect
(and
(Flag10-)
(Flag10prime-)
)

)
(:action Flag10Action-4
:parameters ()
:precondition
(and
(RIGHTOF12-ROBOT)
)
:effect
(and
(Flag10-)
(Flag10prime-)
)

)
(:action Flag10Action-5
:parameters ()
:precondition
(and
(RIGHTOF10-ROBOT)
)
:effect
(and
(Flag10-)
(Flag10prime-)
)

)
(:action Flag10Action-6
:parameters ()
:precondition
(and
(RIGHTOF8-ROBOT)
)
:effect
(and
(Flag10-)
(Flag10prime-)
)

)
(:action Flag10Action-7
:parameters ()
:precondition
(and
(RIGHTOF7-ROBOT)
)
:effect
(and
(Flag10-)
(Flag10prime-)
)

)
(:action Flag10Action-8
:parameters ()
:precondition
(and
(RIGHTOF11-ROBOT)
)
:effect
(and
(Flag10-)
(Flag10prime-)
)

)
(:action Flag10Action-9
:parameters ()
:precondition
(and
(RIGHTOF15-ROBOT)
)
:effect
(and
(Flag10-)
(Flag10prime-)
)

)
(:action Flag10Action-10
:parameters ()
:precondition
(and
(RIGHTOF6-ROBOT)
)
:effect
(and
(Flag10-)
(Flag10prime-)
)

)
(:action Prim10Action
:parameters ()
:precondition
(and
(not (RIGHTOF5-ROBOT))
(not (RIGHTOF9-ROBOT))
(not (RIGHTOF14-ROBOT))
(not (RIGHTOF13-ROBOT))
(not (RIGHTOF12-ROBOT))
(not (RIGHTOF10-ROBOT))
(not (RIGHTOF8-ROBOT))
(not (RIGHTOF7-ROBOT))
(not (RIGHTOF11-ROBOT))
(not (RIGHTOF15-ROBOT))
(not (RIGHTOF6-ROBOT))
)
:effect
(and
(Flag10prime-)
(not (Flag10-))
)

)
(:action Flag11Action-0
:parameters ()
:precondition
(and
(RIGHTOF9-ROBOT)
)
:effect
(and
(Flag11-)
(Flag11prime-)
)

)
(:action Flag11Action-1
:parameters ()
:precondition
(and
(RIGHTOF14-ROBOT)
)
:effect
(and
(Flag11-)
(Flag11prime-)
)

)
(:action Flag11Action-2
:parameters ()
:precondition
(and
(RIGHTOF13-ROBOT)
)
:effect
(and
(Flag11-)
(Flag11prime-)
)

)
(:action Flag11Action-3
:parameters ()
:precondition
(and
(RIGHTOF12-ROBOT)
)
:effect
(and
(Flag11-)
(Flag11prime-)
)

)
(:action Flag11Action-4
:parameters ()
:precondition
(and
(RIGHTOF10-ROBOT)
)
:effect
(and
(Flag11-)
(Flag11prime-)
)

)
(:action Flag11Action-5
:parameters ()
:precondition
(and
(RIGHTOF8-ROBOT)
)
:effect
(and
(Flag11-)
(Flag11prime-)
)

)
(:action Flag11Action-6
:parameters ()
:precondition
(and
(RIGHTOF7-ROBOT)
)
:effect
(and
(Flag11-)
(Flag11prime-)
)

)
(:action Flag11Action-7
:parameters ()
:precondition
(and
(RIGHTOF11-ROBOT)
)
:effect
(and
(Flag11-)
(Flag11prime-)
)

)
(:action Flag11Action-8
:parameters ()
:precondition
(and
(RIGHTOF15-ROBOT)
)
:effect
(and
(Flag11-)
(Flag11prime-)
)

)
(:action Flag11Action-9
:parameters ()
:precondition
(and
(RIGHTOF6-ROBOT)
)
:effect
(and
(Flag11-)
(Flag11prime-)
)

)
(:action Prim11Action
:parameters ()
:precondition
(and
(not (RIGHTOF9-ROBOT))
(not (RIGHTOF14-ROBOT))
(not (RIGHTOF13-ROBOT))
(not (RIGHTOF12-ROBOT))
(not (RIGHTOF10-ROBOT))
(not (RIGHTOF8-ROBOT))
(not (RIGHTOF7-ROBOT))
(not (RIGHTOF11-ROBOT))
(not (RIGHTOF15-ROBOT))
(not (RIGHTOF6-ROBOT))
)
:effect
(and
(Flag11prime-)
(not (Flag11-))
)

)
(:action Flag12Action-0
:parameters ()
:precondition
(and
(RIGHTOF14-ROBOT)
)
:effect
(and
(Flag12-)
(Flag12prime-)
)

)
(:action Flag12Action-1
:parameters ()
:precondition
(and
(RIGHTOF13-ROBOT)
)
:effect
(and
(Flag12-)
(Flag12prime-)
)

)
(:action Flag12Action-2
:parameters ()
:precondition
(and
(RIGHTOF15-ROBOT)
)
:effect
(and
(Flag12-)
(Flag12prime-)
)

)
(:action Flag12Action-3
:parameters ()
:precondition
(and
(RIGHTOF10-ROBOT)
)
:effect
(and
(Flag12-)
(Flag12prime-)
)

)
(:action Flag12Action-4
:parameters ()
:precondition
(and
(RIGHTOF8-ROBOT)
)
:effect
(and
(Flag12-)
(Flag12prime-)
)

)
(:action Flag12Action-5
:parameters ()
:precondition
(and
(RIGHTOF12-ROBOT)
)
:effect
(and
(Flag12-)
(Flag12prime-)
)

)
(:action Flag12Action-6
:parameters ()
:precondition
(and
(RIGHTOF7-ROBOT)
)
:effect
(and
(Flag12-)
(Flag12prime-)
)

)
(:action Flag12Action-7
:parameters ()
:precondition
(and
(RIGHTOF9-ROBOT)
)
:effect
(and
(Flag12-)
(Flag12prime-)
)

)
(:action Flag12Action-8
:parameters ()
:precondition
(and
(RIGHTOF11-ROBOT)
)
:effect
(and
(Flag12-)
(Flag12prime-)
)

)
(:action Prim12Action
:parameters ()
:precondition
(and
(not (RIGHTOF14-ROBOT))
(not (RIGHTOF13-ROBOT))
(not (RIGHTOF15-ROBOT))
(not (RIGHTOF10-ROBOT))
(not (RIGHTOF8-ROBOT))
(not (RIGHTOF12-ROBOT))
(not (RIGHTOF7-ROBOT))
(not (RIGHTOF9-ROBOT))
(not (RIGHTOF11-ROBOT))
)
:effect
(and
(Flag12prime-)
(not (Flag12-))
)

)
(:action Flag13Action-0
:parameters ()
:precondition
(and
(RIGHTOF14-ROBOT)
)
:effect
(and
(Flag13-)
(Flag13prime-)
)

)
(:action Flag13Action-1
:parameters ()
:precondition
(and
(RIGHTOF13-ROBOT)
)
:effect
(and
(Flag13-)
(Flag13prime-)
)

)
(:action Flag13Action-2
:parameters ()
:precondition
(and
(RIGHTOF15-ROBOT)
)
:effect
(and
(Flag13-)
(Flag13prime-)
)

)
(:action Flag13Action-3
:parameters ()
:precondition
(and
(RIGHTOF10-ROBOT)
)
:effect
(and
(Flag13-)
(Flag13prime-)
)

)
(:action Flag13Action-4
:parameters ()
:precondition
(and
(RIGHTOF8-ROBOT)
)
:effect
(and
(Flag13-)
(Flag13prime-)
)

)
(:action Flag13Action-5
:parameters ()
:precondition
(and
(RIGHTOF12-ROBOT)
)
:effect
(and
(Flag13-)
(Flag13prime-)
)

)
(:action Flag13Action-6
:parameters ()
:precondition
(and
(RIGHTOF9-ROBOT)
)
:effect
(and
(Flag13-)
(Flag13prime-)
)

)
(:action Flag13Action-7
:parameters ()
:precondition
(and
(RIGHTOF11-ROBOT)
)
:effect
(and
(Flag13-)
(Flag13prime-)
)

)
(:action Prim13Action
:parameters ()
:precondition
(and
(not (RIGHTOF14-ROBOT))
(not (RIGHTOF13-ROBOT))
(not (RIGHTOF15-ROBOT))
(not (RIGHTOF10-ROBOT))
(not (RIGHTOF8-ROBOT))
(not (RIGHTOF12-ROBOT))
(not (RIGHTOF9-ROBOT))
(not (RIGHTOF11-ROBOT))
)
:effect
(and
(Flag13prime-)
(not (Flag13-))
)

)
(:action Flag14Action-0
:parameters ()
:precondition
(and
(RIGHTOF14-ROBOT)
)
:effect
(and
(Flag14-)
(Flag14prime-)
)

)
(:action Flag14Action-1
:parameters ()
:precondition
(and
(RIGHTOF13-ROBOT)
)
:effect
(and
(Flag14-)
(Flag14prime-)
)

)
(:action Flag14Action-2
:parameters ()
:precondition
(and
(RIGHTOF15-ROBOT)
)
:effect
(and
(Flag14-)
(Flag14prime-)
)

)
(:action Flag14Action-3
:parameters ()
:precondition
(and
(RIGHTOF10-ROBOT)
)
:effect
(and
(Flag14-)
(Flag14prime-)
)

)
(:action Flag14Action-4
:parameters ()
:precondition
(and
(RIGHTOF12-ROBOT)
)
:effect
(and
(Flag14-)
(Flag14prime-)
)

)
(:action Flag14Action-5
:parameters ()
:precondition
(and
(RIGHTOF9-ROBOT)
)
:effect
(and
(Flag14-)
(Flag14prime-)
)

)
(:action Flag14Action-6
:parameters ()
:precondition
(and
(RIGHTOF11-ROBOT)
)
:effect
(and
(Flag14-)
(Flag14prime-)
)

)
(:action Prim14Action
:parameters ()
:precondition
(and
(not (RIGHTOF14-ROBOT))
(not (RIGHTOF13-ROBOT))
(not (RIGHTOF15-ROBOT))
(not (RIGHTOF10-ROBOT))
(not (RIGHTOF12-ROBOT))
(not (RIGHTOF9-ROBOT))
(not (RIGHTOF11-ROBOT))
)
:effect
(and
(Flag14prime-)
(not (Flag14-))
)

)
(:action Flag15Action-0
:parameters ()
:precondition
(and
(RIGHTOF14-ROBOT)
)
:effect
(and
(Flag15-)
(Flag15prime-)
)

)
(:action Flag15Action-1
:parameters ()
:precondition
(and
(RIGHTOF13-ROBOT)
)
:effect
(and
(Flag15-)
(Flag15prime-)
)

)
(:action Flag15Action-2
:parameters ()
:precondition
(and
(RIGHTOF15-ROBOT)
)
:effect
(and
(Flag15-)
(Flag15prime-)
)

)
(:action Flag15Action-3
:parameters ()
:precondition
(and
(RIGHTOF10-ROBOT)
)
:effect
(and
(Flag15-)
(Flag15prime-)
)

)
(:action Flag15Action-4
:parameters ()
:precondition
(and
(RIGHTOF12-ROBOT)
)
:effect
(and
(Flag15-)
(Flag15prime-)
)

)
(:action Flag15Action-5
:parameters ()
:precondition
(and
(RIGHTOF11-ROBOT)
)
:effect
(and
(Flag15-)
(Flag15prime-)
)

)
(:action Prim15Action
:parameters ()
:precondition
(and
(not (RIGHTOF14-ROBOT))
(not (RIGHTOF13-ROBOT))
(not (RIGHTOF15-ROBOT))
(not (RIGHTOF10-ROBOT))
(not (RIGHTOF12-ROBOT))
(not (RIGHTOF11-ROBOT))
)
:effect
(and
(Flag15prime-)
(not (Flag15-))
)

)
(:action Flag16Action-0
:parameters ()
:precondition
(and
(RIGHTOF14-ROBOT)
)
:effect
(and
(Flag16-)
(Flag16prime-)
)

)
(:action Flag16Action-1
:parameters ()
:precondition
(and
(RIGHTOF13-ROBOT)
)
:effect
(and
(Flag16-)
(Flag16prime-)
)

)
(:action Flag16Action-2
:parameters ()
:precondition
(and
(RIGHTOF15-ROBOT)
)
:effect
(and
(Flag16-)
(Flag16prime-)
)

)
(:action Flag16Action-3
:parameters ()
:precondition
(and
(RIGHTOF12-ROBOT)
)
:effect
(and
(Flag16-)
(Flag16prime-)
)

)
(:action Flag16Action-4
:parameters ()
:precondition
(and
(RIGHTOF11-ROBOT)
)
:effect
(and
(Flag16-)
(Flag16prime-)
)

)
(:action Prim16Action
:parameters ()
:precondition
(and
(not (RIGHTOF14-ROBOT))
(not (RIGHTOF13-ROBOT))
(not (RIGHTOF15-ROBOT))
(not (RIGHTOF12-ROBOT))
(not (RIGHTOF11-ROBOT))
)
:effect
(and
(Flag16prime-)
(not (Flag16-))
)

)
(:action Flag17Action-0
:parameters ()
:precondition
(and
(RIGHTOF14-ROBOT)
)
:effect
(and
(Flag17-)
(Flag17prime-)
)

)
(:action Flag17Action-1
:parameters ()
:precondition
(and
(RIGHTOF13-ROBOT)
)
:effect
(and
(Flag17-)
(Flag17prime-)
)

)
(:action Flag17Action-2
:parameters ()
:precondition
(and
(RIGHTOF15-ROBOT)
)
:effect
(and
(Flag17-)
(Flag17prime-)
)

)
(:action Flag17Action-3
:parameters ()
:precondition
(and
(RIGHTOF12-ROBOT)
)
:effect
(and
(Flag17-)
(Flag17prime-)
)

)
(:action Prim17Action
:parameters ()
:precondition
(and
(not (RIGHTOF14-ROBOT))
(not (RIGHTOF13-ROBOT))
(not (RIGHTOF15-ROBOT))
(not (RIGHTOF12-ROBOT))
)
:effect
(and
(Flag17prime-)
(not (Flag17-))
)

)
(:action Flag18Action-0
:parameters ()
:precondition
(and
(RIGHTOF14-ROBOT)
)
:effect
(and
(Flag18-)
(Flag18prime-)
)

)
(:action Flag18Action-1
:parameters ()
:precondition
(and
(RIGHTOF13-ROBOT)
)
:effect
(and
(Flag18-)
(Flag18prime-)
)

)
(:action Flag18Action-2
:parameters ()
:precondition
(and
(RIGHTOF15-ROBOT)
)
:effect
(and
(Flag18-)
(Flag18prime-)
)

)
(:action Prim18Action
:parameters ()
:precondition
(and
(not (RIGHTOF14-ROBOT))
(not (RIGHTOF13-ROBOT))
(not (RIGHTOF15-ROBOT))
)
:effect
(and
(Flag18prime-)
(not (Flag18-))
)

)
(:action Flag19Action-0
:parameters ()
:precondition
(and
(RIGHTOF14-ROBOT)
)
:effect
(and
(Flag19-)
(Flag19prime-)
)

)
(:action Flag19Action-1
:parameters ()
:precondition
(and
(RIGHTOF15-ROBOT)
)
:effect
(and
(Flag19-)
(Flag19prime-)
)

)
(:action Prim19Action
:parameters ()
:precondition
(and
(not (RIGHTOF14-ROBOT))
(not (RIGHTOF15-ROBOT))
)
:effect
(and
(Flag19prime-)
(not (Flag19-))
)

)
(:action Flag20Action-0
:parameters ()
:precondition
(and
(LEFTOF1-ROBOT)
)
:effect
(and
(Flag20-)
(Flag20prime-)
)

)
(:action Flag20Action-1
:parameters ()
:precondition
(and
(LEFTOF2-ROBOT)
)
:effect
(and
(Flag20-)
(Flag20prime-)
)

)
(:action Prim20Action
:parameters ()
:precondition
(and
(not (LEFTOF1-ROBOT))
(not (LEFTOF2-ROBOT))
)
:effect
(and
(Flag20prime-)
(not (Flag20-))
)

)
(:action Flag21Action-0
:parameters ()
:precondition
(and
(LEFTOF1-ROBOT)
)
:effect
(and
(Flag21-)
(Flag21prime-)
)

)
(:action Flag21Action-1
:parameters ()
:precondition
(and
(LEFTOF3-ROBOT)
)
:effect
(and
(Flag21-)
(Flag21prime-)
)

)
(:action Flag21Action-2
:parameters ()
:precondition
(and
(LEFTOF2-ROBOT)
)
:effect
(and
(Flag21-)
(Flag21prime-)
)

)
(:action Prim21Action
:parameters ()
:precondition
(and
(not (LEFTOF1-ROBOT))
(not (LEFTOF3-ROBOT))
(not (LEFTOF2-ROBOT))
)
:effect
(and
(Flag21prime-)
(not (Flag21-))
)

)
(:action Flag22Action-0
:parameters ()
:precondition
(and
(LEFTOF1-ROBOT)
)
:effect
(and
(Flag22-)
(Flag22prime-)
)

)
(:action Flag22Action-1
:parameters ()
:precondition
(and
(LEFTOF3-ROBOT)
)
:effect
(and
(Flag22-)
(Flag22prime-)
)

)
(:action Flag22Action-2
:parameters ()
:precondition
(and
(LEFTOF4-ROBOT)
)
:effect
(and
(Flag22-)
(Flag22prime-)
)

)
(:action Flag22Action-3
:parameters ()
:precondition
(and
(LEFTOF2-ROBOT)
)
:effect
(and
(Flag22-)
(Flag22prime-)
)

)
(:action Prim22Action
:parameters ()
:precondition
(and
(not (LEFTOF1-ROBOT))
(not (LEFTOF3-ROBOT))
(not (LEFTOF4-ROBOT))
(not (LEFTOF2-ROBOT))
)
:effect
(and
(Flag22prime-)
(not (Flag22-))
)

)
(:action Flag23Action-0
:parameters ()
:precondition
(and
(LEFTOF5-ROBOT)
)
:effect
(and
(Flag23-)
(Flag23prime-)
)

)
(:action Flag23Action-1
:parameters ()
:precondition
(and
(LEFTOF1-ROBOT)
)
:effect
(and
(Flag23-)
(Flag23prime-)
)

)
(:action Flag23Action-2
:parameters ()
:precondition
(and
(LEFTOF3-ROBOT)
)
:effect
(and
(Flag23-)
(Flag23prime-)
)

)
(:action Flag23Action-3
:parameters ()
:precondition
(and
(LEFTOF4-ROBOT)
)
:effect
(and
(Flag23-)
(Flag23prime-)
)

)
(:action Flag23Action-4
:parameters ()
:precondition
(and
(LEFTOF2-ROBOT)
)
:effect
(and
(Flag23-)
(Flag23prime-)
)

)
(:action Prim23Action
:parameters ()
:precondition
(and
(not (LEFTOF5-ROBOT))
(not (LEFTOF1-ROBOT))
(not (LEFTOF3-ROBOT))
(not (LEFTOF4-ROBOT))
(not (LEFTOF2-ROBOT))
)
:effect
(and
(Flag23prime-)
(not (Flag23-))
)

)
(:action Flag24Action-0
:parameters ()
:precondition
(and
(LEFTOF2-ROBOT)
)
:effect
(and
(Flag24-)
(Flag24prime-)
)

)
(:action Flag24Action-1
:parameters ()
:precondition
(and
(LEFTOF1-ROBOT)
)
:effect
(and
(Flag24-)
(Flag24prime-)
)

)
(:action Flag24Action-2
:parameters ()
:precondition
(and
(LEFTOF6-ROBOT)
)
:effect
(and
(Flag24-)
(Flag24prime-)
)

)
(:action Flag24Action-3
:parameters ()
:precondition
(and
(LEFTOF5-ROBOT)
)
:effect
(and
(Flag24-)
(Flag24prime-)
)

)
(:action Flag24Action-4
:parameters ()
:precondition
(and
(LEFTOF3-ROBOT)
)
:effect
(and
(Flag24-)
(Flag24prime-)
)

)
(:action Flag24Action-5
:parameters ()
:precondition
(and
(LEFTOF4-ROBOT)
)
:effect
(and
(Flag24-)
(Flag24prime-)
)

)
(:action Prim24Action
:parameters ()
:precondition
(and
(not (LEFTOF2-ROBOT))
(not (LEFTOF1-ROBOT))
(not (LEFTOF6-ROBOT))
(not (LEFTOF5-ROBOT))
(not (LEFTOF3-ROBOT))
(not (LEFTOF4-ROBOT))
)
:effect
(and
(Flag24prime-)
(not (Flag24-))
)

)
(:action Flag25Action-0
:parameters ()
:precondition
(and
(LEFTOF2-ROBOT)
)
:effect
(and
(Flag25-)
(Flag25prime-)
)

)
(:action Flag25Action-1
:parameters ()
:precondition
(and
(LEFTOF7-ROBOT)
)
:effect
(and
(Flag25-)
(Flag25prime-)
)

)
(:action Flag25Action-2
:parameters ()
:precondition
(and
(LEFTOF1-ROBOT)
)
:effect
(and
(Flag25-)
(Flag25prime-)
)

)
(:action Flag25Action-3
:parameters ()
:precondition
(and
(LEFTOF6-ROBOT)
)
:effect
(and
(Flag25-)
(Flag25prime-)
)

)
(:action Flag25Action-4
:parameters ()
:precondition
(and
(LEFTOF5-ROBOT)
)
:effect
(and
(Flag25-)
(Flag25prime-)
)

)
(:action Flag25Action-5
:parameters ()
:precondition
(and
(LEFTOF3-ROBOT)
)
:effect
(and
(Flag25-)
(Flag25prime-)
)

)
(:action Flag25Action-6
:parameters ()
:precondition
(and
(LEFTOF4-ROBOT)
)
:effect
(and
(Flag25-)
(Flag25prime-)
)

)
(:action Prim25Action
:parameters ()
:precondition
(and
(not (LEFTOF2-ROBOT))
(not (LEFTOF7-ROBOT))
(not (LEFTOF1-ROBOT))
(not (LEFTOF6-ROBOT))
(not (LEFTOF5-ROBOT))
(not (LEFTOF3-ROBOT))
(not (LEFTOF4-ROBOT))
)
:effect
(and
(Flag25prime-)
(not (Flag25-))
)

)
(:action Flag26Action-0
:parameters ()
:precondition
(and
(LEFTOF2-ROBOT)
)
:effect
(and
(Flag26-)
(Flag26prime-)
)

)
(:action Flag26Action-1
:parameters ()
:precondition
(and
(LEFTOF7-ROBOT)
)
:effect
(and
(Flag26-)
(Flag26prime-)
)

)
(:action Flag26Action-2
:parameters ()
:precondition
(and
(LEFTOF1-ROBOT)
)
:effect
(and
(Flag26-)
(Flag26prime-)
)

)
(:action Flag26Action-3
:parameters ()
:precondition
(and
(LEFTOF6-ROBOT)
)
:effect
(and
(Flag26-)
(Flag26prime-)
)

)
(:action Flag26Action-4
:parameters ()
:precondition
(and
(LEFTOF5-ROBOT)
)
:effect
(and
(Flag26-)
(Flag26prime-)
)

)
(:action Flag26Action-5
:parameters ()
:precondition
(and
(LEFTOF3-ROBOT)
)
:effect
(and
(Flag26-)
(Flag26prime-)
)

)
(:action Flag26Action-6
:parameters ()
:precondition
(and
(LEFTOF4-ROBOT)
)
:effect
(and
(Flag26-)
(Flag26prime-)
)

)
(:action Flag26Action-7
:parameters ()
:precondition
(and
(LEFTOF8-ROBOT)
)
:effect
(and
(Flag26-)
(Flag26prime-)
)

)
(:action Prim26Action
:parameters ()
:precondition
(and
(not (LEFTOF2-ROBOT))
(not (LEFTOF7-ROBOT))
(not (LEFTOF1-ROBOT))
(not (LEFTOF6-ROBOT))
(not (LEFTOF5-ROBOT))
(not (LEFTOF3-ROBOT))
(not (LEFTOF4-ROBOT))
(not (LEFTOF8-ROBOT))
)
:effect
(and
(Flag26prime-)
(not (Flag26-))
)

)
(:action Flag27Action-0
:parameters ()
:precondition
(and
(LEFTOF9-ROBOT)
)
:effect
(and
(Flag27-)
(Flag27prime-)
)

)
(:action Flag27Action-1
:parameters ()
:precondition
(and
(LEFTOF2-ROBOT)
)
:effect
(and
(Flag27-)
(Flag27prime-)
)

)
(:action Flag27Action-2
:parameters ()
:precondition
(and
(LEFTOF7-ROBOT)
)
:effect
(and
(Flag27-)
(Flag27prime-)
)

)
(:action Flag27Action-3
:parameters ()
:precondition
(and
(LEFTOF1-ROBOT)
)
:effect
(and
(Flag27-)
(Flag27prime-)
)

)
(:action Flag27Action-4
:parameters ()
:precondition
(and
(LEFTOF6-ROBOT)
)
:effect
(and
(Flag27-)
(Flag27prime-)
)

)
(:action Flag27Action-5
:parameters ()
:precondition
(and
(LEFTOF5-ROBOT)
)
:effect
(and
(Flag27-)
(Flag27prime-)
)

)
(:action Flag27Action-6
:parameters ()
:precondition
(and
(LEFTOF3-ROBOT)
)
:effect
(and
(Flag27-)
(Flag27prime-)
)

)
(:action Flag27Action-7
:parameters ()
:precondition
(and
(LEFTOF4-ROBOT)
)
:effect
(and
(Flag27-)
(Flag27prime-)
)

)
(:action Flag27Action-8
:parameters ()
:precondition
(and
(LEFTOF8-ROBOT)
)
:effect
(and
(Flag27-)
(Flag27prime-)
)

)
(:action Prim27Action
:parameters ()
:precondition
(and
(not (LEFTOF9-ROBOT))
(not (LEFTOF2-ROBOT))
(not (LEFTOF7-ROBOT))
(not (LEFTOF1-ROBOT))
(not (LEFTOF6-ROBOT))
(not (LEFTOF5-ROBOT))
(not (LEFTOF3-ROBOT))
(not (LEFTOF4-ROBOT))
(not (LEFTOF8-ROBOT))
)
:effect
(and
(Flag27prime-)
(not (Flag27-))
)

)
(:action Flag28Action-0
:parameters ()
:precondition
(and
(LEFTOF10-ROBOT)
)
:effect
(and
(Flag28-)
(Flag28prime-)
)

)
(:action Flag28Action-1
:parameters ()
:precondition
(and
(LEFTOF9-ROBOT)
)
:effect
(and
(Flag28-)
(Flag28prime-)
)

)
(:action Flag28Action-2
:parameters ()
:precondition
(and
(LEFTOF2-ROBOT)
)
:effect
(and
(Flag28-)
(Flag28prime-)
)

)
(:action Flag28Action-3
:parameters ()
:precondition
(and
(LEFTOF7-ROBOT)
)
:effect
(and
(Flag28-)
(Flag28prime-)
)

)
(:action Flag28Action-4
:parameters ()
:precondition
(and
(LEFTOF1-ROBOT)
)
:effect
(and
(Flag28-)
(Flag28prime-)
)

)
(:action Flag28Action-5
:parameters ()
:precondition
(and
(LEFTOF6-ROBOT)
)
:effect
(and
(Flag28-)
(Flag28prime-)
)

)
(:action Flag28Action-6
:parameters ()
:precondition
(and
(LEFTOF5-ROBOT)
)
:effect
(and
(Flag28-)
(Flag28prime-)
)

)
(:action Flag28Action-7
:parameters ()
:precondition
(and
(LEFTOF3-ROBOT)
)
:effect
(and
(Flag28-)
(Flag28prime-)
)

)
(:action Flag28Action-8
:parameters ()
:precondition
(and
(LEFTOF4-ROBOT)
)
:effect
(and
(Flag28-)
(Flag28prime-)
)

)
(:action Flag28Action-9
:parameters ()
:precondition
(and
(LEFTOF8-ROBOT)
)
:effect
(and
(Flag28-)
(Flag28prime-)
)

)
(:action Prim28Action
:parameters ()
:precondition
(and
(not (LEFTOF10-ROBOT))
(not (LEFTOF9-ROBOT))
(not (LEFTOF2-ROBOT))
(not (LEFTOF7-ROBOT))
(not (LEFTOF1-ROBOT))
(not (LEFTOF6-ROBOT))
(not (LEFTOF5-ROBOT))
(not (LEFTOF3-ROBOT))
(not (LEFTOF4-ROBOT))
(not (LEFTOF8-ROBOT))
)
:effect
(and
(Flag28prime-)
(not (Flag28-))
)

)
(:action Flag29Action-0
:parameters ()
:precondition
(and
(LEFTOF10-ROBOT)
)
:effect
(and
(Flag29-)
(Flag29prime-)
)

)
(:action Flag29Action-1
:parameters ()
:precondition
(and
(LEFTOF11-ROBOT)
)
:effect
(and
(Flag29-)
(Flag29prime-)
)

)
(:action Flag29Action-2
:parameters ()
:precondition
(and
(LEFTOF9-ROBOT)
)
:effect
(and
(Flag29-)
(Flag29prime-)
)

)
(:action Flag29Action-3
:parameters ()
:precondition
(and
(LEFTOF2-ROBOT)
)
:effect
(and
(Flag29-)
(Flag29prime-)
)

)
(:action Flag29Action-4
:parameters ()
:precondition
(and
(LEFTOF7-ROBOT)
)
:effect
(and
(Flag29-)
(Flag29prime-)
)

)
(:action Flag29Action-5
:parameters ()
:precondition
(and
(LEFTOF1-ROBOT)
)
:effect
(and
(Flag29-)
(Flag29prime-)
)

)
(:action Flag29Action-6
:parameters ()
:precondition
(and
(LEFTOF6-ROBOT)
)
:effect
(and
(Flag29-)
(Flag29prime-)
)

)
(:action Flag29Action-7
:parameters ()
:precondition
(and
(LEFTOF5-ROBOT)
)
:effect
(and
(Flag29-)
(Flag29prime-)
)

)
(:action Flag29Action-8
:parameters ()
:precondition
(and
(LEFTOF3-ROBOT)
)
:effect
(and
(Flag29-)
(Flag29prime-)
)

)
(:action Flag29Action-9
:parameters ()
:precondition
(and
(LEFTOF4-ROBOT)
)
:effect
(and
(Flag29-)
(Flag29prime-)
)

)
(:action Flag29Action-10
:parameters ()
:precondition
(and
(LEFTOF8-ROBOT)
)
:effect
(and
(Flag29-)
(Flag29prime-)
)

)
(:action Prim29Action
:parameters ()
:precondition
(and
(not (LEFTOF10-ROBOT))
(not (LEFTOF11-ROBOT))
(not (LEFTOF9-ROBOT))
(not (LEFTOF2-ROBOT))
(not (LEFTOF7-ROBOT))
(not (LEFTOF1-ROBOT))
(not (LEFTOF6-ROBOT))
(not (LEFTOF5-ROBOT))
(not (LEFTOF3-ROBOT))
(not (LEFTOF4-ROBOT))
(not (LEFTOF8-ROBOT))
)
:effect
(and
(Flag29prime-)
(not (Flag29-))
)

)
(:action Flag30Action-0
:parameters ()
:precondition
(and
(LEFTOF10-ROBOT)
)
:effect
(and
(Flag30-)
(Flag30prime-)
)

)
(:action Flag30Action-1
:parameters ()
:precondition
(and
(LEFTOF11-ROBOT)
)
:effect
(and
(Flag30-)
(Flag30prime-)
)

)
(:action Flag30Action-2
:parameters ()
:precondition
(and
(LEFTOF9-ROBOT)
)
:effect
(and
(Flag30-)
(Flag30prime-)
)

)
(:action Flag30Action-3
:parameters ()
:precondition
(and
(LEFTOF2-ROBOT)
)
:effect
(and
(Flag30-)
(Flag30prime-)
)

)
(:action Flag30Action-4
:parameters ()
:precondition
(and
(LEFTOF7-ROBOT)
)
:effect
(and
(Flag30-)
(Flag30prime-)
)

)
(:action Flag30Action-5
:parameters ()
:precondition
(and
(LEFTOF1-ROBOT)
)
:effect
(and
(Flag30-)
(Flag30prime-)
)

)
(:action Flag30Action-6
:parameters ()
:precondition
(and
(LEFTOF12-ROBOT)
)
:effect
(and
(Flag30-)
(Flag30prime-)
)

)
(:action Flag30Action-7
:parameters ()
:precondition
(and
(LEFTOF6-ROBOT)
)
:effect
(and
(Flag30-)
(Flag30prime-)
)

)
(:action Flag30Action-8
:parameters ()
:precondition
(and
(LEFTOF5-ROBOT)
)
:effect
(and
(Flag30-)
(Flag30prime-)
)

)
(:action Flag30Action-9
:parameters ()
:precondition
(and
(LEFTOF3-ROBOT)
)
:effect
(and
(Flag30-)
(Flag30prime-)
)

)
(:action Flag30Action-10
:parameters ()
:precondition
(and
(LEFTOF4-ROBOT)
)
:effect
(and
(Flag30-)
(Flag30prime-)
)

)
(:action Flag30Action-11
:parameters ()
:precondition
(and
(LEFTOF8-ROBOT)
)
:effect
(and
(Flag30-)
(Flag30prime-)
)

)
(:action Prim30Action
:parameters ()
:precondition
(and
(not (LEFTOF10-ROBOT))
(not (LEFTOF11-ROBOT))
(not (LEFTOF9-ROBOT))
(not (LEFTOF2-ROBOT))
(not (LEFTOF7-ROBOT))
(not (LEFTOF1-ROBOT))
(not (LEFTOF12-ROBOT))
(not (LEFTOF6-ROBOT))
(not (LEFTOF5-ROBOT))
(not (LEFTOF3-ROBOT))
(not (LEFTOF4-ROBOT))
(not (LEFTOF8-ROBOT))
)
:effect
(and
(Flag30prime-)
(not (Flag30-))
)

)
(:action Flag31Action-0
:parameters ()
:precondition
(and
(LEFTOF10-ROBOT)
)
:effect
(and
(Flag31-)
(Flag31prime-)
)

)
(:action Flag31Action-1
:parameters ()
:precondition
(and
(LEFTOF11-ROBOT)
)
:effect
(and
(Flag31-)
(Flag31prime-)
)

)
(:action Flag31Action-2
:parameters ()
:precondition
(and
(LEFTOF9-ROBOT)
)
:effect
(and
(Flag31-)
(Flag31prime-)
)

)
(:action Flag31Action-3
:parameters ()
:precondition
(and
(LEFTOF2-ROBOT)
)
:effect
(and
(Flag31-)
(Flag31prime-)
)

)
(:action Flag31Action-4
:parameters ()
:precondition
(and
(LEFTOF13-ROBOT)
)
:effect
(and
(Flag31-)
(Flag31prime-)
)

)
(:action Flag31Action-5
:parameters ()
:precondition
(and
(LEFTOF1-ROBOT)
)
:effect
(and
(Flag31-)
(Flag31prime-)
)

)
(:action Flag31Action-6
:parameters ()
:precondition
(and
(LEFTOF12-ROBOT)
)
:effect
(and
(Flag31-)
(Flag31prime-)
)

)
(:action Flag31Action-7
:parameters ()
:precondition
(and
(LEFTOF6-ROBOT)
)
:effect
(and
(Flag31-)
(Flag31prime-)
)

)
(:action Flag31Action-8
:parameters ()
:precondition
(and
(LEFTOF5-ROBOT)
)
:effect
(and
(Flag31-)
(Flag31prime-)
)

)
(:action Flag31Action-9
:parameters ()
:precondition
(and
(LEFTOF3-ROBOT)
)
:effect
(and
(Flag31-)
(Flag31prime-)
)

)
(:action Flag31Action-10
:parameters ()
:precondition
(and
(LEFTOF4-ROBOT)
)
:effect
(and
(Flag31-)
(Flag31prime-)
)

)
(:action Flag31Action-11
:parameters ()
:precondition
(and
(LEFTOF7-ROBOT)
)
:effect
(and
(Flag31-)
(Flag31prime-)
)

)
(:action Flag31Action-12
:parameters ()
:precondition
(and
(LEFTOF8-ROBOT)
)
:effect
(and
(Flag31-)
(Flag31prime-)
)

)
(:action Prim31Action
:parameters ()
:precondition
(and
(not (LEFTOF10-ROBOT))
(not (LEFTOF11-ROBOT))
(not (LEFTOF9-ROBOT))
(not (LEFTOF2-ROBOT))
(not (LEFTOF13-ROBOT))
(not (LEFTOF1-ROBOT))
(not (LEFTOF12-ROBOT))
(not (LEFTOF6-ROBOT))
(not (LEFTOF5-ROBOT))
(not (LEFTOF3-ROBOT))
(not (LEFTOF4-ROBOT))
(not (LEFTOF7-ROBOT))
(not (LEFTOF8-ROBOT))
)
:effect
(and
(Flag31prime-)
(not (Flag31-))
)

)
(:action Flag32Action-0
:parameters ()
:precondition
(and
(LEFTOF10-ROBOT)
)
:effect
(and
(Flag32-)
(Flag32prime-)
)

)
(:action Flag32Action-1
:parameters ()
:precondition
(and
(LEFTOF11-ROBOT)
)
:effect
(and
(Flag32-)
(Flag32prime-)
)

)
(:action Flag32Action-2
:parameters ()
:precondition
(and
(LEFTOF9-ROBOT)
)
:effect
(and
(Flag32-)
(Flag32prime-)
)

)
(:action Flag32Action-3
:parameters ()
:precondition
(and
(LEFTOF2-ROBOT)
)
:effect
(and
(Flag32-)
(Flag32prime-)
)

)
(:action Flag32Action-4
:parameters ()
:precondition
(and
(LEFTOF13-ROBOT)
)
:effect
(and
(Flag32-)
(Flag32prime-)
)

)
(:action Flag32Action-5
:parameters ()
:precondition
(and
(LEFTOF1-ROBOT)
)
:effect
(and
(Flag32-)
(Flag32prime-)
)

)
(:action Flag32Action-6
:parameters ()
:precondition
(and
(LEFTOF12-ROBOT)
)
:effect
(and
(Flag32-)
(Flag32prime-)
)

)
(:action Flag32Action-7
:parameters ()
:precondition
(and
(LEFTOF6-ROBOT)
)
:effect
(and
(Flag32-)
(Flag32prime-)
)

)
(:action Flag32Action-8
:parameters ()
:precondition
(and
(LEFTOF5-ROBOT)
)
:effect
(and
(Flag32-)
(Flag32prime-)
)

)
(:action Flag32Action-9
:parameters ()
:precondition
(and
(LEFTOF14-ROBOT)
)
:effect
(and
(Flag32-)
(Flag32prime-)
)

)
(:action Flag32Action-10
:parameters ()
:precondition
(and
(LEFTOF3-ROBOT)
)
:effect
(and
(Flag32-)
(Flag32prime-)
)

)
(:action Flag32Action-11
:parameters ()
:precondition
(and
(LEFTOF4-ROBOT)
)
:effect
(and
(Flag32-)
(Flag32prime-)
)

)
(:action Flag32Action-12
:parameters ()
:precondition
(and
(LEFTOF7-ROBOT)
)
:effect
(and
(Flag32-)
(Flag32prime-)
)

)
(:action Flag32Action-13
:parameters ()
:precondition
(and
(LEFTOF8-ROBOT)
)
:effect
(and
(Flag32-)
(Flag32prime-)
)

)
(:action Prim32Action
:parameters ()
:precondition
(and
(not (LEFTOF10-ROBOT))
(not (LEFTOF11-ROBOT))
(not (LEFTOF9-ROBOT))
(not (LEFTOF2-ROBOT))
(not (LEFTOF13-ROBOT))
(not (LEFTOF1-ROBOT))
(not (LEFTOF12-ROBOT))
(not (LEFTOF6-ROBOT))
(not (LEFTOF5-ROBOT))
(not (LEFTOF14-ROBOT))
(not (LEFTOF3-ROBOT))
(not (LEFTOF4-ROBOT))
(not (LEFTOF7-ROBOT))
(not (LEFTOF8-ROBOT))
)
:effect
(and
(Flag32prime-)
(not (Flag32-))
)

)
(:action Flag33Action-1
:parameters ()
:precondition
(and
(LEFTOF10-ROBOT)
)
:effect
(and
(Flag33-)
(Flag33prime-)
)

)
(:action Flag33Action-2
:parameters ()
:precondition
(and
(LEFTOF11-ROBOT)
)
:effect
(and
(Flag33-)
(Flag33prime-)
)

)
(:action Flag33Action-3
:parameters ()
:precondition
(and
(LEFTOF9-ROBOT)
)
:effect
(and
(Flag33-)
(Flag33prime-)
)

)
(:action Flag33Action-4
:parameters ()
:precondition
(and
(LEFTOF2-ROBOT)
)
:effect
(and
(Flag33-)
(Flag33prime-)
)

)
(:action Flag33Action-5
:parameters ()
:precondition
(and
(LEFTOF13-ROBOT)
)
:effect
(and
(Flag33-)
(Flag33prime-)
)

)
(:action Flag33Action-6
:parameters ()
:precondition
(and
(LEFTOF1-ROBOT)
)
:effect
(and
(Flag33-)
(Flag33prime-)
)

)
(:action Flag33Action-7
:parameters ()
:precondition
(and
(LEFTOF12-ROBOT)
)
:effect
(and
(Flag33-)
(Flag33prime-)
)

)
(:action Flag33Action-8
:parameters ()
:precondition
(and
(LEFTOF6-ROBOT)
)
:effect
(and
(Flag33-)
(Flag33prime-)
)

)
(:action Flag33Action-9
:parameters ()
:precondition
(and
(LEFTOF5-ROBOT)
)
:effect
(and
(Flag33-)
(Flag33prime-)
)

)
(:action Flag33Action-10
:parameters ()
:precondition
(and
(LEFTOF14-ROBOT)
)
:effect
(and
(Flag33-)
(Flag33prime-)
)

)
(:action Flag33Action-11
:parameters ()
:precondition
(and
(LEFTOF3-ROBOT)
)
:effect
(and
(Flag33-)
(Flag33prime-)
)

)
(:action Flag33Action-12
:parameters ()
:precondition
(and
(LEFTOF4-ROBOT)
)
:effect
(and
(Flag33-)
(Flag33prime-)
)

)
(:action Flag33Action-13
:parameters ()
:precondition
(and
(LEFTOF7-ROBOT)
)
:effect
(and
(Flag33-)
(Flag33prime-)
)

)
(:action Flag33Action-14
:parameters ()
:precondition
(and
(LEFTOF8-ROBOT)
)
:effect
(and
(Flag33-)
(Flag33prime-)
)

)
(:action Prim33Action
:parameters ()
:precondition
(and
(not (LEFTOF15-ROBOT))
(not (LEFTOF10-ROBOT))
(not (LEFTOF11-ROBOT))
(not (LEFTOF9-ROBOT))
(not (LEFTOF2-ROBOT))
(not (LEFTOF13-ROBOT))
(not (LEFTOF1-ROBOT))
(not (LEFTOF12-ROBOT))
(not (LEFTOF6-ROBOT))
(not (LEFTOF5-ROBOT))
(not (LEFTOF14-ROBOT))
(not (LEFTOF3-ROBOT))
(not (LEFTOF4-ROBOT))
(not (LEFTOF7-ROBOT))
(not (LEFTOF8-ROBOT))
)
:effect
(and
(Flag33prime-)
(not (Flag33-))
)

)
(:action Flag34Action
:parameters ()
:precondition
(and
(LEFTOF1-ROBOT)
(RIGHTOF4-ROBOT)
)
:effect
(and
(Flag34-)
(Flag34prime-)
)

)
(:action Flag35Action
:parameters ()
:precondition
(and
(LEFTOF1-ROBOT)
(RIGHTOF12-ROBOT)
)
:effect
(and
(Flag35-)
(Flag35prime-)
)

)
(:action Flag36Action
:parameters ()
:precondition
(and
(LEFTOF1-ROBOT)
(RIGHTOF7-ROBOT)
)
:effect
(and
(Flag36-)
(Flag36prime-)
)

)
(:action Flag37Action
:parameters ()
:precondition
(and
(LEFTOF1-ROBOT)
(RIGHTOF9-ROBOT)
)
:effect
(and
(Flag37-)
(Flag37prime-)
)

)
(:action Flag38Action
:parameters ()
:precondition
(and
(RIGHTOF13-ROBOT)
(LEFTOF1-ROBOT)
)
:effect
(and
(Flag38-)
(Flag38prime-)
)

)
(:action Flag39Action
:parameters ()
:precondition
(and
(LEFTOF1-ROBOT)
(RIGHTOF1-ROBOT)
)
:effect
(and
(Flag39-)
(Flag39prime-)
)

)
(:action Flag40Action
:parameters ()
:precondition
(and
(RIGHTOF3-ROBOT)
(LEFTOF1-ROBOT)
)
:effect
(and
(Flag40-)
(Flag40prime-)
)

)
(:action Flag41Action
:parameters ()
:precondition
(and
(LEFTOF1-ROBOT)
(RIGHTOF15-ROBOT)
)
:effect
(and
(Flag41-)
(Flag41prime-)
)

)
(:action Flag42Action
:parameters ()
:precondition
(and
(RIGHTOF0-ROBOT)
(LEFTOF1-ROBOT)
)
:effect
(and
(Flag42-)
(Flag42prime-)
)

)
(:action Flag43Action
:parameters ()
:precondition
(and
(LEFTOF1-ROBOT)
(RIGHTOF5-ROBOT)
)
:effect
(and
(Flag43-)
(Flag43prime-)
)

)
(:action Flag44Action
:parameters ()
:precondition
(and
(LEFTOF1-ROBOT)
(RIGHTOF2-ROBOT)
)
:effect
(and
(Flag44-)
(Flag44prime-)
)

)
(:action Flag45Action
:parameters ()
:precondition
(and
(RIGHTOF11-ROBOT)
(LEFTOF1-ROBOT)
)
:effect
(and
(Flag45-)
(Flag45prime-)
)

)
(:action Flag46Action
:parameters ()
:precondition
(and
(RIGHTOF8-ROBOT)
(LEFTOF1-ROBOT)
)
:effect
(and
(Flag46-)
(Flag46prime-)
)

)
(:action Flag47Action
:parameters ()
:precondition
(and
(LEFTOF1-ROBOT)
(RIGHTOF6-ROBOT)
)
:effect
(and
(Flag47-)
(Flag47prime-)
)

)
(:action Flag48Action
:parameters ()
:precondition
(and
(LEFTOF1-ROBOT)
(RIGHTOF14-ROBOT)
)
:effect
(and
(Flag48-)
(Flag48prime-)
)

)
(:action Flag49Action
:parameters ()
:precondition
(and
(RIGHTOF10-ROBOT)
(LEFTOF1-ROBOT)
)
:effect
(and
(Flag49-)
(Flag49prime-)
)

)
(:action Prim34Action-0
:parameters ()
:precondition
(and
(not (LEFTOF1-ROBOT))
)
:effect
(and
(Flag34prime-)
(not (Flag34-))
)

)
(:action Prim34Action-1
:parameters ()
:precondition
(and
(not (RIGHTOF4-ROBOT))
)
:effect
(and
(Flag34prime-)
(not (Flag34-))
)

)
(:action Prim35Action-0
:parameters ()
:precondition
(and
(not (LEFTOF1-ROBOT))
)
:effect
(and
(Flag35prime-)
(not (Flag35-))
)

)
(:action Prim35Action-1
:parameters ()
:precondition
(and
(not (RIGHTOF12-ROBOT))
)
:effect
(and
(Flag35prime-)
(not (Flag35-))
)

)
(:action Prim36Action-0
:parameters ()
:precondition
(and
(not (LEFTOF1-ROBOT))
)
:effect
(and
(Flag36prime-)
(not (Flag36-))
)

)
(:action Prim36Action-1
:parameters ()
:precondition
(and
(not (RIGHTOF7-ROBOT))
)
:effect
(and
(Flag36prime-)
(not (Flag36-))
)

)
(:action Prim37Action-0
:parameters ()
:precondition
(and
(not (LEFTOF1-ROBOT))
)
:effect
(and
(Flag37prime-)
(not (Flag37-))
)

)
(:action Prim37Action-1
:parameters ()
:precondition
(and
(not (RIGHTOF9-ROBOT))
)
:effect
(and
(Flag37prime-)
(not (Flag37-))
)

)
(:action Prim38Action-0
:parameters ()
:precondition
(and
(not (RIGHTOF13-ROBOT))
)
:effect
(and
(Flag38prime-)
(not (Flag38-))
)

)
(:action Prim38Action-1
:parameters ()
:precondition
(and
(not (LEFTOF1-ROBOT))
)
:effect
(and
(Flag38prime-)
(not (Flag38-))
)

)
(:action Prim39Action-0
:parameters ()
:precondition
(and
(not (LEFTOF1-ROBOT))
)
:effect
(and
(Flag39prime-)
(not (Flag39-))
)

)
(:action Prim39Action-1
:parameters ()
:precondition
(and
(not (RIGHTOF1-ROBOT))
)
:effect
(and
(Flag39prime-)
(not (Flag39-))
)

)
(:action Prim40Action-0
:parameters ()
:precondition
(and
(not (RIGHTOF3-ROBOT))
)
:effect
(and
(Flag40prime-)
(not (Flag40-))
)

)
(:action Prim40Action-1
:parameters ()
:precondition
(and
(not (LEFTOF1-ROBOT))
)
:effect
(and
(Flag40prime-)
(not (Flag40-))
)

)
(:action Prim41Action-0
:parameters ()
:precondition
(and
(not (LEFTOF1-ROBOT))
)
:effect
(and
(Flag41prime-)
(not (Flag41-))
)

)
(:action Prim41Action-1
:parameters ()
:precondition
(and
(not (RIGHTOF15-ROBOT))
)
:effect
(and
(Flag41prime-)
(not (Flag41-))
)

)
(:action Prim42Action-0
:parameters ()
:precondition
(and
(not (RIGHTOF0-ROBOT))
)
:effect
(and
(Flag42prime-)
(not (Flag42-))
)

)
(:action Prim42Action-1
:parameters ()
:precondition
(and
(not (LEFTOF1-ROBOT))
)
:effect
(and
(Flag42prime-)
(not (Flag42-))
)

)
(:action Prim43Action-0
:parameters ()
:precondition
(and
(not (LEFTOF1-ROBOT))
)
:effect
(and
(Flag43prime-)
(not (Flag43-))
)

)
(:action Prim43Action-1
:parameters ()
:precondition
(and
(not (RIGHTOF5-ROBOT))
)
:effect
(and
(Flag43prime-)
(not (Flag43-))
)

)
(:action Prim44Action-0
:parameters ()
:precondition
(and
(not (LEFTOF1-ROBOT))
)
:effect
(and
(Flag44prime-)
(not (Flag44-))
)

)
(:action Prim45Action-0
:parameters ()
:precondition
(and
(not (RIGHTOF11-ROBOT))
)
:effect
(and
(Flag45prime-)
(not (Flag45-))
)

)
(:action Prim45Action-1
:parameters ()
:precondition
(and
(not (LEFTOF1-ROBOT))
)
:effect
(and
(Flag45prime-)
(not (Flag45-))
)

)
(:action Prim46Action-0
:parameters ()
:precondition
(and
(not (RIGHTOF8-ROBOT))
)
:effect
(and
(Flag46prime-)
(not (Flag46-))
)

)
(:action Prim46Action-1
:parameters ()
:precondition
(and
(not (LEFTOF1-ROBOT))
)
:effect
(and
(Flag46prime-)
(not (Flag46-))
)

)
(:action Prim47Action-0
:parameters ()
:precondition
(and
(not (LEFTOF1-ROBOT))
)
:effect
(and
(Flag47prime-)
(not (Flag47-))
)

)
(:action Prim47Action-1
:parameters ()
:precondition
(and
(not (RIGHTOF6-ROBOT))
)
:effect
(and
(Flag47prime-)
(not (Flag47-))
)

)
(:action Prim48Action-0
:parameters ()
:precondition
(and
(not (LEFTOF1-ROBOT))
)
:effect
(and
(Flag48prime-)
(not (Flag48-))
)

)
(:action Prim48Action-1
:parameters ()
:precondition
(and
(not (RIGHTOF14-ROBOT))
)
:effect
(and
(Flag48prime-)
(not (Flag48-))
)

)
(:action Prim49Action-0
:parameters ()
:precondition
(and
(not (RIGHTOF10-ROBOT))
)
:effect
(and
(Flag49prime-)
(not (Flag49-))
)

)
(:action Prim49Action-1
:parameters ()
:precondition
(and
(not (LEFTOF1-ROBOT))
)
:effect
(and
(Flag49prime-)
(not (Flag49-))
)

)
(:action Flag51Action
:parameters ()
:precondition
(and
(LEFTOF2-ROBOT)
(RIGHTOF15-ROBOT)
)
:effect
(and
(Flag51-)
(Flag51prime-)
)

)
(:action Flag52Action
:parameters ()
:precondition
(and
(RIGHTOF13-ROBOT)
(LEFTOF2-ROBOT)
)
:effect
(and
(Flag52-)
(Flag52prime-)
)

)
(:action Flag53Action
:parameters ()
:precondition
(and
(LEFTOF2-ROBOT)
(RIGHTOF9-ROBOT)
)
:effect
(and
(Flag53-)
(Flag53prime-)
)

)
(:action Flag54Action
:parameters ()
:precondition
(and
(LEFTOF2-ROBOT)
(RIGHTOF1-ROBOT)
)
:effect
(and
(Flag54-)
(Flag54prime-)
)

)
(:action Flag55Action
:parameters ()
:precondition
(and
(RIGHTOF5-ROBOT)
(LEFTOF2-ROBOT)
)
:effect
(and
(Flag55-)
(Flag55prime-)
)

)
(:action Flag56Action
:parameters ()
:precondition
(and
(LEFTOF2-ROBOT)
(RIGHTOF7-ROBOT)
)
:effect
(and
(Flag56-)
(Flag56prime-)
)

)
(:action Flag57Action
:parameters ()
:precondition
(and
(RIGHTOF5-ROBOT)
(LEFTOF1-ROBOT)
)
:effect
(and
(Flag57-)
(Flag57prime-)
)

)
(:action Flag58Action
:parameters ()
:precondition
(and
(RIGHTOF3-ROBOT)
(LEFTOF2-ROBOT)
)
:effect
(and
(Flag58-)
(Flag58prime-)
)

)
(:action Flag59Action
:parameters ()
:precondition
(and
(LEFTOF2-ROBOT)
(RIGHTOF14-ROBOT)
)
:effect
(and
(Flag59-)
(Flag59prime-)
)

)
(:action Flag60Action
:parameters ()
:precondition
(and
(RIGHTOF10-ROBOT)
(LEFTOF2-ROBOT)
)
:effect
(and
(Flag60-)
(Flag60prime-)
)

)
(:action Flag61Action
:parameters ()
:precondition
(and
(LEFTOF2-ROBOT)
(RIGHTOF12-ROBOT)
)
:effect
(and
(Flag61-)
(Flag61prime-)
)

)
(:action Flag62Action
:parameters ()
:precondition
(and
(RIGHTOF11-ROBOT)
(LEFTOF2-ROBOT)
)
:effect
(and
(Flag62-)
(Flag62prime-)
)

)
(:action Flag63Action
:parameters ()
:precondition
(and
(RIGHTOF8-ROBOT)
(LEFTOF2-ROBOT)
)
:effect
(and
(Flag63-)
(Flag63prime-)
)

)
(:action Flag64Action
:parameters ()
:precondition
(and
(RIGHTOF2-ROBOT)
(LEFTOF2-ROBOT)
)
:effect
(and
(Flag64-)
(Flag64prime-)
)

)
(:action Flag65Action
:parameters ()
:precondition
(and
(LEFTOF2-ROBOT)
(RIGHTOF6-ROBOT)
)
:effect
(and
(Flag65-)
(Flag65prime-)
)

)
(:action Flag66Action
:parameters ()
:precondition
(and
(LEFTOF2-ROBOT)
(RIGHTOF4-ROBOT)
)
:effect
(and
(Flag66-)
(Flag66prime-)
)

)
(:action Prim51Action-0
:parameters ()
:precondition
(and
(not (LEFTOF2-ROBOT))
)
:effect
(and
(Flag51prime-)
(not (Flag51-))
)

)
(:action Prim51Action-1
:parameters ()
:precondition
(and
(not (RIGHTOF15-ROBOT))
)
:effect
(and
(Flag51prime-)
(not (Flag51-))
)

)
(:action Prim52Action-0
:parameters ()
:precondition
(and
(not (RIGHTOF13-ROBOT))
)
:effect
(and
(Flag52prime-)
(not (Flag52-))
)

)
(:action Prim52Action-1
:parameters ()
:precondition
(and
(not (LEFTOF2-ROBOT))
)
:effect
(and
(Flag52prime-)
(not (Flag52-))
)

)
(:action Prim53Action-0
:parameters ()
:precondition
(and
(not (LEFTOF2-ROBOT))
)
:effect
(and
(Flag53prime-)
(not (Flag53-))
)

)
(:action Prim53Action-1
:parameters ()
:precondition
(and
(not (RIGHTOF9-ROBOT))
)
:effect
(and
(Flag53prime-)
(not (Flag53-))
)

)
(:action Prim54Action-0
:parameters ()
:precondition
(and
(not (LEFTOF2-ROBOT))
)
:effect
(and
(Flag54prime-)
(not (Flag54-))
)

)
(:action Prim54Action-1
:parameters ()
:precondition
(and
(not (RIGHTOF1-ROBOT))
)
:effect
(and
(Flag54prime-)
(not (Flag54-))
)

)
(:action Prim55Action-0
:parameters ()
:precondition
(and
(not (RIGHTOF5-ROBOT))
)
:effect
(and
(Flag55prime-)
(not (Flag55-))
)

)
(:action Prim55Action-1
:parameters ()
:precondition
(and
(not (LEFTOF2-ROBOT))
)
:effect
(and
(Flag55prime-)
(not (Flag55-))
)

)
(:action Prim56Action-0
:parameters ()
:precondition
(and
(not (LEFTOF2-ROBOT))
)
:effect
(and
(Flag56prime-)
(not (Flag56-))
)

)
(:action Prim56Action-1
:parameters ()
:precondition
(and
(not (RIGHTOF7-ROBOT))
)
:effect
(and
(Flag56prime-)
(not (Flag56-))
)

)
(:action Prim57Action-0
:parameters ()
:precondition
(and
(not (RIGHTOF5-ROBOT))
)
:effect
(and
(Flag57prime-)
(not (Flag57-))
)

)
(:action Prim57Action-1
:parameters ()
:precondition
(and
(not (LEFTOF1-ROBOT))
)
:effect
(and
(Flag57prime-)
(not (Flag57-))
)

)
(:action Prim58Action-0
:parameters ()
:precondition
(and
(not (RIGHTOF3-ROBOT))
)
:effect
(and
(Flag58prime-)
(not (Flag58-))
)

)
(:action Prim58Action-1
:parameters ()
:precondition
(and
(not (LEFTOF2-ROBOT))
)
:effect
(and
(Flag58prime-)
(not (Flag58-))
)

)
(:action Prim59Action-0
:parameters ()
:precondition
(and
(not (LEFTOF2-ROBOT))
)
:effect
(and
(Flag59prime-)
(not (Flag59-))
)

)
(:action Prim59Action-1
:parameters ()
:precondition
(and
(not (RIGHTOF14-ROBOT))
)
:effect
(and
(Flag59prime-)
(not (Flag59-))
)

)
(:action Prim60Action-0
:parameters ()
:precondition
(and
(not (RIGHTOF10-ROBOT))
)
:effect
(and
(Flag60prime-)
(not (Flag60-))
)

)
(:action Prim60Action-1
:parameters ()
:precondition
(and
(not (LEFTOF2-ROBOT))
)
:effect
(and
(Flag60prime-)
(not (Flag60-))
)

)
(:action Prim61Action-0
:parameters ()
:precondition
(and
(not (LEFTOF2-ROBOT))
)
:effect
(and
(Flag61prime-)
(not (Flag61-))
)

)
(:action Prim61Action-1
:parameters ()
:precondition
(and
(not (RIGHTOF12-ROBOT))
)
:effect
(and
(Flag61prime-)
(not (Flag61-))
)

)
(:action Prim62Action-0
:parameters ()
:precondition
(and
(not (RIGHTOF11-ROBOT))
)
:effect
(and
(Flag62prime-)
(not (Flag62-))
)

)
(:action Prim62Action-1
:parameters ()
:precondition
(and
(not (LEFTOF2-ROBOT))
)
:effect
(and
(Flag62prime-)
(not (Flag62-))
)

)
(:action Prim63Action-0
:parameters ()
:precondition
(and
(not (RIGHTOF8-ROBOT))
)
:effect
(and
(Flag63prime-)
(not (Flag63-))
)

)
(:action Prim63Action-1
:parameters ()
:precondition
(and
(not (LEFTOF2-ROBOT))
)
:effect
(and
(Flag63prime-)
(not (Flag63-))
)

)
(:action Prim64Action-1
:parameters ()
:precondition
(and
(not (LEFTOF2-ROBOT))
)
:effect
(and
(Flag64prime-)
(not (Flag64-))
)

)
(:action Prim65Action-0
:parameters ()
:precondition
(and
(not (LEFTOF2-ROBOT))
)
:effect
(and
(Flag65prime-)
(not (Flag65-))
)

)
(:action Prim65Action-1
:parameters ()
:precondition
(and
(not (RIGHTOF6-ROBOT))
)
:effect
(and
(Flag65prime-)
(not (Flag65-))
)

)
(:action Prim66Action-0
:parameters ()
:precondition
(and
(not (LEFTOF2-ROBOT))
)
:effect
(and
(Flag66prime-)
(not (Flag66-))
)

)
(:action Prim66Action-1
:parameters ()
:precondition
(and
(not (RIGHTOF4-ROBOT))
)
:effect
(and
(Flag66prime-)
(not (Flag66-))
)

)
(:action Flag68Action
:parameters ()
:precondition
(and
(LEFTOF3-ROBOT)
(RIGHTOF6-ROBOT)
)
:effect
(and
(Flag68-)
(Flag68prime-)
)

)
(:action Flag69Action
:parameters ()
:precondition
(and
(LEFTOF3-ROBOT)
(RIGHTOF14-ROBOT)
)
:effect
(and
(Flag69-)
(Flag69prime-)
)

)
(:action Flag70Action
:parameters ()
:precondition
(and
(LEFTOF3-ROBOT)
(RIGHTOF4-ROBOT)
)
:effect
(and
(Flag70-)
(Flag70prime-)
)

)
(:action Flag71Action
:parameters ()
:precondition
(and
(LEFTOF3-ROBOT)
(RIGHTOF7-ROBOT)
)
:effect
(and
(Flag71-)
(Flag71prime-)
)

)
(:action Flag72Action
:parameters ()
:precondition
(and
(LEFTOF3-ROBOT)
(RIGHTOF12-ROBOT)
)
:effect
(and
(Flag72-)
(Flag72prime-)
)

)
(:action Flag73Action
:parameters ()
:precondition
(and
(LEFTOF3-ROBOT)
(RIGHTOF10-ROBOT)
)
:effect
(and
(Flag73-)
(Flag73prime-)
)

)
(:action Flag74Action
:parameters ()
:precondition
(and
(LEFTOF3-ROBOT)
(RIGHTOF8-ROBOT)
)
:effect
(and
(Flag74-)
(Flag74prime-)
)

)
(:action Flag75Action
:parameters ()
:precondition
(and
(LEFTOF3-ROBOT)
(RIGHTOF5-ROBOT)
)
:effect
(and
(Flag75-)
(Flag75prime-)
)

)
(:action Flag76Action
:parameters ()
:precondition
(and
(LEFTOF3-ROBOT)
(RIGHTOF15-ROBOT)
)
:effect
(and
(Flag76-)
(Flag76prime-)
)

)
(:action Flag77Action
:parameters ()
:precondition
(and
(RIGHTOF3-ROBOT)
(LEFTOF3-ROBOT)
)
:effect
(and
(Flag77-)
(Flag77prime-)
)

)
(:action Flag78Action
:parameters ()
:precondition
(and
(LEFTOF3-ROBOT)
(RIGHTOF11-ROBOT)
)
:effect
(and
(Flag78-)
(Flag78prime-)
)

)
(:action Flag79Action
:parameters ()
:precondition
(and
(RIGHTOF13-ROBOT)
(LEFTOF3-ROBOT)
)
:effect
(and
(Flag79-)
(Flag79prime-)
)

)
(:action Flag80Action
:parameters ()
:precondition
(and
(LEFTOF3-ROBOT)
(RIGHTOF9-ROBOT)
)
:effect
(and
(Flag80-)
(Flag80prime-)
)

)
(:action Flag81Action
:parameters ()
:precondition
(and
(LEFTOF3-ROBOT)
(RIGHTOF2-ROBOT)
)
:effect
(and
(Flag81-)
(Flag81prime-)
)

)
(:action Prim68Action-0
:parameters ()
:precondition
(and
(not (LEFTOF3-ROBOT))
)
:effect
(and
(Flag68prime-)
(not (Flag68-))
)

)
(:action Prim68Action-1
:parameters ()
:precondition
(and
(not (RIGHTOF6-ROBOT))
)
:effect
(and
(Flag68prime-)
(not (Flag68-))
)

)
(:action Prim69Action-0
:parameters ()
:precondition
(and
(not (LEFTOF3-ROBOT))
)
:effect
(and
(Flag69prime-)
(not (Flag69-))
)

)
(:action Prim69Action-1
:parameters ()
:precondition
(and
(not (RIGHTOF14-ROBOT))
)
:effect
(and
(Flag69prime-)
(not (Flag69-))
)

)
(:action Prim70Action-0
:parameters ()
:precondition
(and
(not (LEFTOF3-ROBOT))
)
:effect
(and
(Flag70prime-)
(not (Flag70-))
)

)
(:action Prim70Action-1
:parameters ()
:precondition
(and
(not (RIGHTOF4-ROBOT))
)
:effect
(and
(Flag70prime-)
(not (Flag70-))
)

)
(:action Prim71Action-0
:parameters ()
:precondition
(and
(not (LEFTOF3-ROBOT))
)
:effect
(and
(Flag71prime-)
(not (Flag71-))
)

)
(:action Prim71Action-1
:parameters ()
:precondition
(and
(not (RIGHTOF7-ROBOT))
)
:effect
(and
(Flag71prime-)
(not (Flag71-))
)

)
(:action Prim72Action-0
:parameters ()
:precondition
(and
(not (LEFTOF3-ROBOT))
)
:effect
(and
(Flag72prime-)
(not (Flag72-))
)

)
(:action Prim72Action-1
:parameters ()
:precondition
(and
(not (RIGHTOF12-ROBOT))
)
:effect
(and
(Flag72prime-)
(not (Flag72-))
)

)
(:action Prim73Action-0
:parameters ()
:precondition
(and
(not (LEFTOF3-ROBOT))
)
:effect
(and
(Flag73prime-)
(not (Flag73-))
)

)
(:action Prim73Action-1
:parameters ()
:precondition
(and
(not (RIGHTOF10-ROBOT))
)
:effect
(and
(Flag73prime-)
(not (Flag73-))
)

)
(:action Prim74Action-0
:parameters ()
:precondition
(and
(not (LEFTOF3-ROBOT))
)
:effect
(and
(Flag74prime-)
(not (Flag74-))
)

)
(:action Prim74Action-1
:parameters ()
:precondition
(and
(not (RIGHTOF8-ROBOT))
)
:effect
(and
(Flag74prime-)
(not (Flag74-))
)

)
(:action Prim75Action-0
:parameters ()
:precondition
(and
(not (LEFTOF3-ROBOT))
)
:effect
(and
(Flag75prime-)
(not (Flag75-))
)

)
(:action Prim75Action-1
:parameters ()
:precondition
(and
(not (RIGHTOF5-ROBOT))
)
:effect
(and
(Flag75prime-)
(not (Flag75-))
)

)
(:action Prim76Action-0
:parameters ()
:precondition
(and
(not (LEFTOF3-ROBOT))
)
:effect
(and
(Flag76prime-)
(not (Flag76-))
)

)
(:action Prim76Action-1
:parameters ()
:precondition
(and
(not (RIGHTOF15-ROBOT))
)
:effect
(and
(Flag76prime-)
(not (Flag76-))
)

)
(:action Prim77Action-0
:parameters ()
:precondition
(and
(not (RIGHTOF3-ROBOT))
)
:effect
(and
(Flag77prime-)
(not (Flag77-))
)

)
(:action Prim77Action-1
:parameters ()
:precondition
(and
(not (LEFTOF3-ROBOT))
)
:effect
(and
(Flag77prime-)
(not (Flag77-))
)

)
(:action Prim78Action-0
:parameters ()
:precondition
(and
(not (LEFTOF3-ROBOT))
)
:effect
(and
(Flag78prime-)
(not (Flag78-))
)

)
(:action Prim78Action-1
:parameters ()
:precondition
(and
(not (RIGHTOF11-ROBOT))
)
:effect
(and
(Flag78prime-)
(not (Flag78-))
)

)
(:action Prim79Action-0
:parameters ()
:precondition
(and
(not (RIGHTOF13-ROBOT))
)
:effect
(and
(Flag79prime-)
(not (Flag79-))
)

)
(:action Prim79Action-1
:parameters ()
:precondition
(and
(not (LEFTOF3-ROBOT))
)
:effect
(and
(Flag79prime-)
(not (Flag79-))
)

)
(:action Prim80Action-0
:parameters ()
:precondition
(and
(not (LEFTOF3-ROBOT))
)
:effect
(and
(Flag80prime-)
(not (Flag80-))
)

)
(:action Prim80Action-1
:parameters ()
:precondition
(and
(not (RIGHTOF9-ROBOT))
)
:effect
(and
(Flag80prime-)
(not (Flag80-))
)

)
(:action Prim81Action-0
:parameters ()
:precondition
(and
(not (LEFTOF3-ROBOT))
)
:effect
(and
(Flag81prime-)
(not (Flag81-))
)

)
(:action Flag83Action
:parameters ()
:precondition
(and
(LEFTOF3-ROBOT)
(RIGHTOF13-ROBOT)
)
:effect
(and
(Flag83-)
(Flag83prime-)
)

)
(:action Flag84Action
:parameters ()
:precondition
(and
(LEFTOF4-ROBOT)
(RIGHTOF4-ROBOT)
)
:effect
(and
(Flag84-)
(Flag84prime-)
)

)
(:action Flag85Action
:parameters ()
:precondition
(and
(LEFTOF4-ROBOT)
(RIGHTOF7-ROBOT)
)
:effect
(and
(Flag85-)
(Flag85prime-)
)

)
(:action Flag86Action
:parameters ()
:precondition
(and
(LEFTOF4-ROBOT)
(RIGHTOF15-ROBOT)
)
:effect
(and
(Flag86-)
(Flag86prime-)
)

)
(:action Flag87Action
:parameters ()
:precondition
(and
(LEFTOF4-ROBOT)
(RIGHTOF14-ROBOT)
)
:effect
(and
(Flag87-)
(Flag87prime-)
)

)
(:action Flag88Action
:parameters ()
:precondition
(and
(LEFTOF3-ROBOT)
(RIGHTOF3-ROBOT)
)
:effect
(and
(Flag88-)
(Flag88prime-)
)

)
(:action Flag89Action
:parameters ()
:precondition
(and
(RIGHTOF8-ROBOT)
(LEFTOF4-ROBOT)
)
:effect
(and
(Flag89-)
(Flag89prime-)
)

)
(:action Flag90Action
:parameters ()
:precondition
(and
(LEFTOF4-ROBOT)
(RIGHTOF12-ROBOT)
)
:effect
(and
(Flag90-)
(Flag90prime-)
)

)
(:action Flag91Action
:parameters ()
:precondition
(and
(RIGHTOF10-ROBOT)
(LEFTOF4-ROBOT)
)
:effect
(and
(Flag91-)
(Flag91prime-)
)

)
(:action Flag92Action
:parameters ()
:precondition
(and
(LEFTOF4-ROBOT)
(RIGHTOF9-ROBOT)
)
:effect
(and
(Flag92-)
(Flag92prime-)
)

)
(:action Flag93Action
:parameters ()
:precondition
(and
(LEFTOF4-ROBOT)
(RIGHTOF6-ROBOT)
)
:effect
(and
(Flag93-)
(Flag93prime-)
)

)
(:action Flag94Action
:parameters ()
:precondition
(and
(LEFTOF4-ROBOT)
(RIGHTOF5-ROBOT)
)
:effect
(and
(Flag94-)
(Flag94prime-)
)

)
(:action Flag95Action
:parameters ()
:precondition
(and
(RIGHTOF13-ROBOT)
(LEFTOF4-ROBOT)
)
:effect
(and
(Flag95-)
(Flag95prime-)
)

)
(:action Flag96Action
:parameters ()
:precondition
(and
(RIGHTOF3-ROBOT)
(LEFTOF4-ROBOT)
)
:effect
(and
(Flag96-)
(Flag96prime-)
)

)
(:action Flag97Action
:parameters ()
:precondition
(and
(LEFTOF4-ROBOT)
(RIGHTOF11-ROBOT)
)
:effect
(and
(Flag97-)
(Flag97prime-)
)

)
(:action Prim83Action-0
:parameters ()
:precondition
(and
(not (LEFTOF3-ROBOT))
)
:effect
(and
(Flag83prime-)
(not (Flag83-))
)

)
(:action Prim83Action-1
:parameters ()
:precondition
(and
(not (RIGHTOF13-ROBOT))
)
:effect
(and
(Flag83prime-)
(not (Flag83-))
)

)
(:action Prim84Action-0
:parameters ()
:precondition
(and
(not (LEFTOF4-ROBOT))
)
:effect
(and
(Flag84prime-)
(not (Flag84-))
)

)
(:action Prim84Action-1
:parameters ()
:precondition
(and
(not (RIGHTOF4-ROBOT))
)
:effect
(and
(Flag84prime-)
(not (Flag84-))
)

)
(:action Prim85Action-0
:parameters ()
:precondition
(and
(not (LEFTOF4-ROBOT))
)
:effect
(and
(Flag85prime-)
(not (Flag85-))
)

)
(:action Prim85Action-1
:parameters ()
:precondition
(and
(not (RIGHTOF7-ROBOT))
)
:effect
(and
(Flag85prime-)
(not (Flag85-))
)

)
(:action Prim86Action-0
:parameters ()
:precondition
(and
(not (LEFTOF4-ROBOT))
)
:effect
(and
(Flag86prime-)
(not (Flag86-))
)

)
(:action Prim86Action-1
:parameters ()
:precondition
(and
(not (RIGHTOF15-ROBOT))
)
:effect
(and
(Flag86prime-)
(not (Flag86-))
)

)
(:action Prim87Action-0
:parameters ()
:precondition
(and
(not (LEFTOF4-ROBOT))
)
:effect
(and
(Flag87prime-)
(not (Flag87-))
)

)
(:action Prim87Action-1
:parameters ()
:precondition
(and
(not (RIGHTOF14-ROBOT))
)
:effect
(and
(Flag87prime-)
(not (Flag87-))
)

)
(:action Prim88Action-0
:parameters ()
:precondition
(and
(not (LEFTOF3-ROBOT))
)
:effect
(and
(Flag88prime-)
(not (Flag88-))
)

)
(:action Prim88Action-1
:parameters ()
:precondition
(and
(not (RIGHTOF3-ROBOT))
)
:effect
(and
(Flag88prime-)
(not (Flag88-))
)

)
(:action Prim89Action-0
:parameters ()
:precondition
(and
(not (RIGHTOF8-ROBOT))
)
:effect
(and
(Flag89prime-)
(not (Flag89-))
)

)
(:action Prim89Action-1
:parameters ()
:precondition
(and
(not (LEFTOF4-ROBOT))
)
:effect
(and
(Flag89prime-)
(not (Flag89-))
)

)
(:action Prim90Action-0
:parameters ()
:precondition
(and
(not (LEFTOF4-ROBOT))
)
:effect
(and
(Flag90prime-)
(not (Flag90-))
)

)
(:action Prim90Action-1
:parameters ()
:precondition
(and
(not (RIGHTOF12-ROBOT))
)
:effect
(and
(Flag90prime-)
(not (Flag90-))
)

)
(:action Prim91Action-0
:parameters ()
:precondition
(and
(not (RIGHTOF10-ROBOT))
)
:effect
(and
(Flag91prime-)
(not (Flag91-))
)

)
(:action Prim91Action-1
:parameters ()
:precondition
(and
(not (LEFTOF4-ROBOT))
)
:effect
(and
(Flag91prime-)
(not (Flag91-))
)

)
(:action Prim92Action-0
:parameters ()
:precondition
(and
(not (LEFTOF4-ROBOT))
)
:effect
(and
(Flag92prime-)
(not (Flag92-))
)

)
(:action Prim92Action-1
:parameters ()
:precondition
(and
(not (RIGHTOF9-ROBOT))
)
:effect
(and
(Flag92prime-)
(not (Flag92-))
)

)
(:action Prim93Action-0
:parameters ()
:precondition
(and
(not (LEFTOF4-ROBOT))
)
:effect
(and
(Flag93prime-)
(not (Flag93-))
)

)
(:action Prim93Action-1
:parameters ()
:precondition
(and
(not (RIGHTOF6-ROBOT))
)
:effect
(and
(Flag93prime-)
(not (Flag93-))
)

)
(:action Prim94Action-0
:parameters ()
:precondition
(and
(not (LEFTOF4-ROBOT))
)
:effect
(and
(Flag94prime-)
(not (Flag94-))
)

)
(:action Prim94Action-1
:parameters ()
:precondition
(and
(not (RIGHTOF5-ROBOT))
)
:effect
(and
(Flag94prime-)
(not (Flag94-))
)

)
(:action Prim95Action-0
:parameters ()
:precondition
(and
(not (RIGHTOF13-ROBOT))
)
:effect
(and
(Flag95prime-)
(not (Flag95-))
)

)
(:action Prim95Action-1
:parameters ()
:precondition
(and
(not (LEFTOF4-ROBOT))
)
:effect
(and
(Flag95prime-)
(not (Flag95-))
)

)
(:action Prim96Action-0
:parameters ()
:precondition
(and
(not (RIGHTOF3-ROBOT))
)
:effect
(and
(Flag96prime-)
(not (Flag96-))
)

)
(:action Prim96Action-1
:parameters ()
:precondition
(and
(not (LEFTOF4-ROBOT))
)
:effect
(and
(Flag96prime-)
(not (Flag96-))
)

)
(:action Prim97Action-0
:parameters ()
:precondition
(and
(not (LEFTOF4-ROBOT))
)
:effect
(and
(Flag97prime-)
(not (Flag97-))
)

)
(:action Prim97Action-1
:parameters ()
:precondition
(and
(not (RIGHTOF11-ROBOT))
)
:effect
(and
(Flag97prime-)
(not (Flag97-))
)

)
(:action Flag99Action
:parameters ()
:precondition
(and
(RIGHTOF9-ROBOT)
(LEFTOF5-ROBOT)
)
:effect
(and
(Flag99-)
(Flag99prime-)
)

)
(:action Flag100Action
:parameters ()
:precondition
(and
(RIGHTOF15-ROBOT)
(LEFTOF5-ROBOT)
)
:effect
(and
(Flag100-)
(Flag100prime-)
)

)
(:action Flag101Action
:parameters ()
:precondition
(and
(RIGHTOF4-ROBOT)
(LEFTOF5-ROBOT)
)
:effect
(and
(Flag101-)
(Flag101prime-)
)

)
(:action Flag102Action
:parameters ()
:precondition
(and
(RIGHTOF7-ROBOT)
(LEFTOF5-ROBOT)
)
:effect
(and
(Flag102-)
(Flag102prime-)
)

)
(:action Flag103Action
:parameters ()
:precondition
(and
(RIGHTOF11-ROBOT)
(LEFTOF5-ROBOT)
)
:effect
(and
(Flag103-)
(Flag103prime-)
)

)
(:action Flag104Action
:parameters ()
:precondition
(and
(RIGHTOF13-ROBOT)
(LEFTOF5-ROBOT)
)
:effect
(and
(Flag104-)
(Flag104prime-)
)

)
(:action Flag105Action
:parameters ()
:precondition
(and
(LEFTOF4-ROBOT)
(RIGHTOF8-ROBOT)
)
:effect
(and
(Flag105-)
(Flag105prime-)
)

)
(:action Flag106Action
:parameters ()
:precondition
(and
(LEFTOF5-ROBOT)
(RIGHTOF6-ROBOT)
)
:effect
(and
(Flag106-)
(Flag106prime-)
)

)
(:action Flag107Action
:parameters ()
:precondition
(and
(RIGHTOF10-ROBOT)
(LEFTOF5-ROBOT)
)
:effect
(and
(Flag107-)
(Flag107prime-)
)

)
(:action Flag108Action
:parameters ()
:precondition
(and
(LEFTOF4-ROBOT)
(RIGHTOF10-ROBOT)
)
:effect
(and
(Flag108-)
(Flag108prime-)
)

)
(:action Flag109Action
:parameters ()
:precondition
(and
(LEFTOF5-ROBOT)
(RIGHTOF12-ROBOT)
)
:effect
(and
(Flag109-)
(Flag109prime-)
)

)
(:action Flag110Action
:parameters ()
:precondition
(and
(RIGHTOF8-ROBOT)
(LEFTOF5-ROBOT)
)
:effect
(and
(Flag110-)
(Flag110prime-)
)

)
(:action Flag111Action
:parameters ()
:precondition
(and
(RIGHTOF14-ROBOT)
(LEFTOF5-ROBOT)
)
:effect
(and
(Flag111-)
(Flag111prime-)
)

)
(:action Flag112Action
:parameters ()
:precondition
(and
(RIGHTOF5-ROBOT)
(LEFTOF5-ROBOT)
)
:effect
(and
(Flag112-)
(Flag112prime-)
)

)
(:action Prim99Action-0
:parameters ()
:precondition
(and
(not (RIGHTOF9-ROBOT))
)
:effect
(and
(Flag99prime-)
(not (Flag99-))
)

)
(:action Prim99Action-1
:parameters ()
:precondition
(and
(not (LEFTOF5-ROBOT))
)
:effect
(and
(Flag99prime-)
(not (Flag99-))
)

)
(:action Prim100Action-0
:parameters ()
:precondition
(and
(not (RIGHTOF15-ROBOT))
)
:effect
(and
(Flag100prime-)
(not (Flag100-))
)

)
(:action Prim100Action-1
:parameters ()
:precondition
(and
(not (LEFTOF5-ROBOT))
)
:effect
(and
(Flag100prime-)
(not (Flag100-))
)

)
(:action Prim101Action-0
:parameters ()
:precondition
(and
(not (RIGHTOF4-ROBOT))
)
:effect
(and
(Flag101prime-)
(not (Flag101-))
)

)
(:action Prim101Action-1
:parameters ()
:precondition
(and
(not (LEFTOF5-ROBOT))
)
:effect
(and
(Flag101prime-)
(not (Flag101-))
)

)
(:action Prim102Action-0
:parameters ()
:precondition
(and
(not (RIGHTOF7-ROBOT))
)
:effect
(and
(Flag102prime-)
(not (Flag102-))
)

)
(:action Prim102Action-1
:parameters ()
:precondition
(and
(not (LEFTOF5-ROBOT))
)
:effect
(and
(Flag102prime-)
(not (Flag102-))
)

)
(:action Prim103Action-0
:parameters ()
:precondition
(and
(not (RIGHTOF11-ROBOT))
)
:effect
(and
(Flag103prime-)
(not (Flag103-))
)

)
(:action Prim103Action-1
:parameters ()
:precondition
(and
(not (LEFTOF5-ROBOT))
)
:effect
(and
(Flag103prime-)
(not (Flag103-))
)

)
(:action Prim104Action-0
:parameters ()
:precondition
(and
(not (RIGHTOF13-ROBOT))
)
:effect
(and
(Flag104prime-)
(not (Flag104-))
)

)
(:action Prim104Action-1
:parameters ()
:precondition
(and
(not (LEFTOF5-ROBOT))
)
:effect
(and
(Flag104prime-)
(not (Flag104-))
)

)
(:action Prim105Action-0
:parameters ()
:precondition
(and
(not (LEFTOF4-ROBOT))
)
:effect
(and
(Flag105prime-)
(not (Flag105-))
)

)
(:action Prim105Action-1
:parameters ()
:precondition
(and
(not (RIGHTOF8-ROBOT))
)
:effect
(and
(Flag105prime-)
(not (Flag105-))
)

)
(:action Prim106Action-0
:parameters ()
:precondition
(and
(not (LEFTOF5-ROBOT))
)
:effect
(and
(Flag106prime-)
(not (Flag106-))
)

)
(:action Prim106Action-1
:parameters ()
:precondition
(and
(not (RIGHTOF6-ROBOT))
)
:effect
(and
(Flag106prime-)
(not (Flag106-))
)

)
(:action Prim107Action-0
:parameters ()
:precondition
(and
(not (RIGHTOF10-ROBOT))
)
:effect
(and
(Flag107prime-)
(not (Flag107-))
)

)
(:action Prim107Action-1
:parameters ()
:precondition
(and
(not (LEFTOF5-ROBOT))
)
:effect
(and
(Flag107prime-)
(not (Flag107-))
)

)
(:action Prim108Action-0
:parameters ()
:precondition
(and
(not (LEFTOF4-ROBOT))
)
:effect
(and
(Flag108prime-)
(not (Flag108-))
)

)
(:action Prim108Action-1
:parameters ()
:precondition
(and
(not (RIGHTOF10-ROBOT))
)
:effect
(and
(Flag108prime-)
(not (Flag108-))
)

)
(:action Prim109Action-0
:parameters ()
:precondition
(and
(not (LEFTOF5-ROBOT))
)
:effect
(and
(Flag109prime-)
(not (Flag109-))
)

)
(:action Prim109Action-1
:parameters ()
:precondition
(and
(not (RIGHTOF12-ROBOT))
)
:effect
(and
(Flag109prime-)
(not (Flag109-))
)

)
(:action Prim110Action-0
:parameters ()
:precondition
(and
(not (RIGHTOF8-ROBOT))
)
:effect
(and
(Flag110prime-)
(not (Flag110-))
)

)
(:action Prim110Action-1
:parameters ()
:precondition
(and
(not (LEFTOF5-ROBOT))
)
:effect
(and
(Flag110prime-)
(not (Flag110-))
)

)
(:action Prim111Action-0
:parameters ()
:precondition
(and
(not (RIGHTOF14-ROBOT))
)
:effect
(and
(Flag111prime-)
(not (Flag111-))
)

)
(:action Prim111Action-1
:parameters ()
:precondition
(and
(not (LEFTOF5-ROBOT))
)
:effect
(and
(Flag111prime-)
(not (Flag111-))
)

)
(:action Prim112Action-0
:parameters ()
:precondition
(and
(not (RIGHTOF5-ROBOT))
)
:effect
(and
(Flag112prime-)
(not (Flag112-))
)

)
(:action Prim112Action-1
:parameters ()
:precondition
(and
(not (LEFTOF5-ROBOT))
)
:effect
(and
(Flag112prime-)
(not (Flag112-))
)

)
(:action Flag114Action
:parameters ()
:precondition
(and
(RIGHTOF15-ROBOT)
(LEFTOF6-ROBOT)
)
:effect
(and
(Flag114-)
(Flag114prime-)
)

)
(:action Flag115Action
:parameters ()
:precondition
(and
(RIGHTOF12-ROBOT)
(LEFTOF6-ROBOT)
)
:effect
(and
(Flag115-)
(Flag115prime-)
)

)
(:action Flag116Action
:parameters ()
:precondition
(and
(RIGHTOF8-ROBOT)
(LEFTOF6-ROBOT)
)
:effect
(and
(Flag116-)
(Flag116prime-)
)

)
(:action Flag117Action
:parameters ()
:precondition
(and
(RIGHTOF11-ROBOT)
(LEFTOF6-ROBOT)
)
:effect
(and
(Flag117-)
(Flag117prime-)
)

)
(:action Flag118Action
:parameters ()
:precondition
(and
(RIGHTOF5-ROBOT)
(LEFTOF6-ROBOT)
)
:effect
(and
(Flag118-)
(Flag118prime-)
)

)
(:action Flag119Action
:parameters ()
:precondition
(and
(RIGHTOF7-ROBOT)
(LEFTOF6-ROBOT)
)
:effect
(and
(Flag119-)
(Flag119prime-)
)

)
(:action Flag120Action
:parameters ()
:precondition
(and
(RIGHTOF10-ROBOT)
(LEFTOF6-ROBOT)
)
:effect
(and
(Flag120-)
(Flag120prime-)
)

)
(:action Flag121Action
:parameters ()
:precondition
(and
(RIGHTOF6-ROBOT)
(LEFTOF6-ROBOT)
)
:effect
(and
(Flag121-)
(Flag121prime-)
)

)
(:action Flag122Action
:parameters ()
:precondition
(and
(RIGHTOF13-ROBOT)
(LEFTOF6-ROBOT)
)
:effect
(and
(Flag122-)
(Flag122prime-)
)

)
(:action Flag123Action
:parameters ()
:precondition
(and
(RIGHTOF14-ROBOT)
(LEFTOF6-ROBOT)
)
:effect
(and
(Flag123-)
(Flag123prime-)
)

)
(:action Flag124Action
:parameters ()
:precondition
(and
(RIGHTOF9-ROBOT)
(LEFTOF6-ROBOT)
)
:effect
(and
(Flag124-)
(Flag124prime-)
)

)
(:action Prim114Action-0
:parameters ()
:precondition
(and
(not (RIGHTOF15-ROBOT))
)
:effect
(and
(Flag114prime-)
(not (Flag114-))
)

)
(:action Prim114Action-1
:parameters ()
:precondition
(and
(not (LEFTOF6-ROBOT))
)
:effect
(and
(Flag114prime-)
(not (Flag114-))
)

)
(:action Prim115Action-0
:parameters ()
:precondition
(and
(not (RIGHTOF12-ROBOT))
)
:effect
(and
(Flag115prime-)
(not (Flag115-))
)

)
(:action Prim115Action-1
:parameters ()
:precondition
(and
(not (LEFTOF6-ROBOT))
)
:effect
(and
(Flag115prime-)
(not (Flag115-))
)

)
(:action Prim116Action-0
:parameters ()
:precondition
(and
(not (RIGHTOF8-ROBOT))
)
:effect
(and
(Flag116prime-)
(not (Flag116-))
)

)
(:action Prim116Action-1
:parameters ()
:precondition
(and
(not (LEFTOF6-ROBOT))
)
:effect
(and
(Flag116prime-)
(not (Flag116-))
)

)
(:action Prim117Action-0
:parameters ()
:precondition
(and
(not (RIGHTOF11-ROBOT))
)
:effect
(and
(Flag117prime-)
(not (Flag117-))
)

)
(:action Prim117Action-1
:parameters ()
:precondition
(and
(not (LEFTOF6-ROBOT))
)
:effect
(and
(Flag117prime-)
(not (Flag117-))
)

)
(:action Prim118Action-0
:parameters ()
:precondition
(and
(not (RIGHTOF5-ROBOT))
)
:effect
(and
(Flag118prime-)
(not (Flag118-))
)

)
(:action Prim118Action-1
:parameters ()
:precondition
(and
(not (LEFTOF6-ROBOT))
)
:effect
(and
(Flag118prime-)
(not (Flag118-))
)

)
(:action Prim119Action-0
:parameters ()
:precondition
(and
(not (RIGHTOF7-ROBOT))
)
:effect
(and
(Flag119prime-)
(not (Flag119-))
)

)
(:action Prim119Action-1
:parameters ()
:precondition
(and
(not (LEFTOF6-ROBOT))
)
:effect
(and
(Flag119prime-)
(not (Flag119-))
)

)
(:action Prim120Action-0
:parameters ()
:precondition
(and
(not (RIGHTOF10-ROBOT))
)
:effect
(and
(Flag120prime-)
(not (Flag120-))
)

)
(:action Prim120Action-1
:parameters ()
:precondition
(and
(not (LEFTOF6-ROBOT))
)
:effect
(and
(Flag120prime-)
(not (Flag120-))
)

)
(:action Prim121Action-0
:parameters ()
:precondition
(and
(not (RIGHTOF6-ROBOT))
)
:effect
(and
(Flag121prime-)
(not (Flag121-))
)

)
(:action Prim121Action-1
:parameters ()
:precondition
(and
(not (LEFTOF6-ROBOT))
)
:effect
(and
(Flag121prime-)
(not (Flag121-))
)

)
(:action Prim122Action-0
:parameters ()
:precondition
(and
(not (RIGHTOF13-ROBOT))
)
:effect
(and
(Flag122prime-)
(not (Flag122-))
)

)
(:action Prim122Action-1
:parameters ()
:precondition
(and
(not (LEFTOF6-ROBOT))
)
:effect
(and
(Flag122prime-)
(not (Flag122-))
)

)
(:action Prim123Action-0
:parameters ()
:precondition
(and
(not (RIGHTOF14-ROBOT))
)
:effect
(and
(Flag123prime-)
(not (Flag123-))
)

)
(:action Prim123Action-1
:parameters ()
:precondition
(and
(not (LEFTOF6-ROBOT))
)
:effect
(and
(Flag123prime-)
(not (Flag123-))
)

)
(:action Prim124Action-0
:parameters ()
:precondition
(and
(not (RIGHTOF9-ROBOT))
)
:effect
(and
(Flag124prime-)
(not (Flag124-))
)

)
(:action Prim124Action-1
:parameters ()
:precondition
(and
(not (LEFTOF6-ROBOT))
)
:effect
(and
(Flag124prime-)
(not (Flag124-))
)

)
(:action Flag126Action
:parameters ()
:precondition
(and
(RIGHTOF11-ROBOT)
(LEFTOF7-ROBOT)
)
:effect
(and
(Flag126-)
(Flag126prime-)
)

)
(:action Flag127Action
:parameters ()
:precondition
(and
(RIGHTOF8-ROBOT)
(LEFTOF7-ROBOT)
)
:effect
(and
(Flag127-)
(Flag127prime-)
)

)
(:action Flag128Action
:parameters ()
:precondition
(and
(LEFTOF7-ROBOT)
(RIGHTOF7-ROBOT)
)
:effect
(and
(Flag128-)
(Flag128prime-)
)

)
(:action Flag129Action
:parameters ()
:precondition
(and
(RIGHTOF13-ROBOT)
(LEFTOF7-ROBOT)
)
:effect
(and
(Flag129-)
(Flag129prime-)
)

)
(:action Flag130Action
:parameters ()
:precondition
(and
(LEFTOF6-ROBOT)
(RIGHTOF6-ROBOT)
)
:effect
(and
(Flag130-)
(Flag130prime-)
)

)
(:action Flag131Action
:parameters ()
:precondition
(and
(LEFTOF7-ROBOT)
(RIGHTOF12-ROBOT)
)
:effect
(and
(Flag131-)
(Flag131prime-)
)

)
(:action Flag132Action
:parameters ()
:precondition
(and
(LEFTOF7-ROBOT)
(RIGHTOF14-ROBOT)
)
:effect
(and
(Flag132-)
(Flag132prime-)
)

)
(:action Flag133Action
:parameters ()
:precondition
(and
(LEFTOF7-ROBOT)
(RIGHTOF9-ROBOT)
)
:effect
(and
(Flag133-)
(Flag133prime-)
)

)
(:action Flag134Action
:parameters ()
:precondition
(and
(LEFTOF7-ROBOT)
(RIGHTOF6-ROBOT)
)
:effect
(and
(Flag134-)
(Flag134prime-)
)

)
(:action Flag135Action
:parameters ()
:precondition
(and
(LEFTOF6-ROBOT)
(RIGHTOF12-ROBOT)
)
:effect
(and
(Flag135-)
(Flag135prime-)
)

)
(:action Flag136Action
:parameters ()
:precondition
(and
(RIGHTOF10-ROBOT)
(LEFTOF7-ROBOT)
)
:effect
(and
(Flag136-)
(Flag136prime-)
)

)
(:action Flag137Action
:parameters ()
:precondition
(and
(LEFTOF7-ROBOT)
(RIGHTOF15-ROBOT)
)
:effect
(and
(Flag137-)
(Flag137prime-)
)

)
(:action Prim126Action-0
:parameters ()
:precondition
(and
(not (RIGHTOF11-ROBOT))
)
:effect
(and
(Flag126prime-)
(not (Flag126-))
)

)
(:action Prim126Action-1
:parameters ()
:precondition
(and
(not (LEFTOF7-ROBOT))
)
:effect
(and
(Flag126prime-)
(not (Flag126-))
)

)
(:action Prim127Action-0
:parameters ()
:precondition
(and
(not (RIGHTOF8-ROBOT))
)
:effect
(and
(Flag127prime-)
(not (Flag127-))
)

)
(:action Prim127Action-1
:parameters ()
:precondition
(and
(not (LEFTOF7-ROBOT))
)
:effect
(and
(Flag127prime-)
(not (Flag127-))
)

)
(:action Prim128Action-0
:parameters ()
:precondition
(and
(not (LEFTOF7-ROBOT))
)
:effect
(and
(Flag128prime-)
(not (Flag128-))
)

)
(:action Prim128Action-1
:parameters ()
:precondition
(and
(not (RIGHTOF7-ROBOT))
)
:effect
(and
(Flag128prime-)
(not (Flag128-))
)

)
(:action Prim129Action-0
:parameters ()
:precondition
(and
(not (RIGHTOF13-ROBOT))
)
:effect
(and
(Flag129prime-)
(not (Flag129-))
)

)
(:action Prim129Action-1
:parameters ()
:precondition
(and
(not (LEFTOF7-ROBOT))
)
:effect
(and
(Flag129prime-)
(not (Flag129-))
)

)
(:action Prim130Action-0
:parameters ()
:precondition
(and
(not (LEFTOF6-ROBOT))
)
:effect
(and
(Flag130prime-)
(not (Flag130-))
)

)
(:action Prim130Action-1
:parameters ()
:precondition
(and
(not (RIGHTOF6-ROBOT))
)
:effect
(and
(Flag130prime-)
(not (Flag130-))
)

)
(:action Prim131Action-0
:parameters ()
:precondition
(and
(not (LEFTOF7-ROBOT))
)
:effect
(and
(Flag131prime-)
(not (Flag131-))
)

)
(:action Prim131Action-1
:parameters ()
:precondition
(and
(not (RIGHTOF12-ROBOT))
)
:effect
(and
(Flag131prime-)
(not (Flag131-))
)

)
(:action Prim132Action-0
:parameters ()
:precondition
(and
(not (LEFTOF7-ROBOT))
)
:effect
(and
(Flag132prime-)
(not (Flag132-))
)

)
(:action Prim132Action-1
:parameters ()
:precondition
(and
(not (RIGHTOF14-ROBOT))
)
:effect
(and
(Flag132prime-)
(not (Flag132-))
)

)
(:action Prim133Action-0
:parameters ()
:precondition
(and
(not (LEFTOF7-ROBOT))
)
:effect
(and
(Flag133prime-)
(not (Flag133-))
)

)
(:action Prim133Action-1
:parameters ()
:precondition
(and
(not (RIGHTOF9-ROBOT))
)
:effect
(and
(Flag133prime-)
(not (Flag133-))
)

)
(:action Prim134Action-0
:parameters ()
:precondition
(and
(not (LEFTOF7-ROBOT))
)
:effect
(and
(Flag134prime-)
(not (Flag134-))
)

)
(:action Prim134Action-1
:parameters ()
:precondition
(and
(not (RIGHTOF6-ROBOT))
)
:effect
(and
(Flag134prime-)
(not (Flag134-))
)

)
(:action Prim135Action-0
:parameters ()
:precondition
(and
(not (LEFTOF6-ROBOT))
)
:effect
(and
(Flag135prime-)
(not (Flag135-))
)

)
(:action Prim135Action-1
:parameters ()
:precondition
(and
(not (RIGHTOF12-ROBOT))
)
:effect
(and
(Flag135prime-)
(not (Flag135-))
)

)
(:action Prim136Action-0
:parameters ()
:precondition
(and
(not (RIGHTOF10-ROBOT))
)
:effect
(and
(Flag136prime-)
(not (Flag136-))
)

)
(:action Prim136Action-1
:parameters ()
:precondition
(and
(not (LEFTOF7-ROBOT))
)
:effect
(and
(Flag136prime-)
(not (Flag136-))
)

)
(:action Prim137Action-0
:parameters ()
:precondition
(and
(not (LEFTOF7-ROBOT))
)
:effect
(and
(Flag137prime-)
(not (Flag137-))
)

)
(:action Prim137Action-1
:parameters ()
:precondition
(and
(not (RIGHTOF15-ROBOT))
)
:effect
(and
(Flag137prime-)
(not (Flag137-))
)

)
(:action Flag139Action
:parameters ()
:precondition
(and
(RIGHTOF7-ROBOT)
(LEFTOF8-ROBOT)
)
:effect
(and
(Flag139-)
(Flag139prime-)
)

)
(:action Flag140Action
:parameters ()
:precondition
(and
(LEFTOF8-ROBOT)
(RIGHTOF9-ROBOT)
)
:effect
(and
(Flag140-)
(Flag140prime-)
)

)
(:action Flag141Action
:parameters ()
:precondition
(and
(RIGHTOF13-ROBOT)
(LEFTOF8-ROBOT)
)
:effect
(and
(Flag141-)
(Flag141prime-)
)

)
(:action Flag142Action
:parameters ()
:precondition
(and
(RIGHTOF14-ROBOT)
(LEFTOF8-ROBOT)
)
:effect
(and
(Flag142-)
(Flag142prime-)
)

)
(:action Flag143Action
:parameters ()
:precondition
(and
(RIGHTOF10-ROBOT)
(LEFTOF8-ROBOT)
)
:effect
(and
(Flag143-)
(Flag143prime-)
)

)
(:action Flag144Action
:parameters ()
:precondition
(and
(RIGHTOF8-ROBOT)
(LEFTOF8-ROBOT)
)
:effect
(and
(Flag144-)
(Flag144prime-)
)

)
(:action Flag145Action
:parameters ()
:precondition
(and
(RIGHTOF7-ROBOT)
(LEFTOF7-ROBOT)
)
:effect
(and
(Flag145-)
(Flag145prime-)
)

)
(:action Flag146Action
:parameters ()
:precondition
(and
(LEFTOF8-ROBOT)
(RIGHTOF12-ROBOT)
)
:effect
(and
(Flag146-)
(Flag146prime-)
)

)
(:action Flag147Action
:parameters ()
:precondition
(and
(LEFTOF8-ROBOT)
(RIGHTOF15-ROBOT)
)
:effect
(and
(Flag147-)
(Flag147prime-)
)

)
(:action Flag148Action
:parameters ()
:precondition
(and
(RIGHTOF11-ROBOT)
(LEFTOF8-ROBOT)
)
:effect
(and
(Flag148-)
(Flag148prime-)
)

)
(:action Prim139Action-0
:parameters ()
:precondition
(and
(not (RIGHTOF7-ROBOT))
)
:effect
(and
(Flag139prime-)
(not (Flag139-))
)

)
(:action Prim139Action-1
:parameters ()
:precondition
(and
(not (LEFTOF8-ROBOT))
)
:effect
(and
(Flag139prime-)
(not (Flag139-))
)

)
(:action Prim140Action-0
:parameters ()
:precondition
(and
(not (LEFTOF8-ROBOT))
)
:effect
(and
(Flag140prime-)
(not (Flag140-))
)

)
(:action Prim140Action-1
:parameters ()
:precondition
(and
(not (RIGHTOF9-ROBOT))
)
:effect
(and
(Flag140prime-)
(not (Flag140-))
)

)
(:action Prim141Action-0
:parameters ()
:precondition
(and
(not (RIGHTOF13-ROBOT))
)
:effect
(and
(Flag141prime-)
(not (Flag141-))
)

)
(:action Prim141Action-1
:parameters ()
:precondition
(and
(not (LEFTOF8-ROBOT))
)
:effect
(and
(Flag141prime-)
(not (Flag141-))
)

)
(:action Prim142Action-0
:parameters ()
:precondition
(and
(not (RIGHTOF14-ROBOT))
)
:effect
(and
(Flag142prime-)
(not (Flag142-))
)

)
(:action Prim142Action-1
:parameters ()
:precondition
(and
(not (LEFTOF8-ROBOT))
)
:effect
(and
(Flag142prime-)
(not (Flag142-))
)

)
(:action Prim143Action-0
:parameters ()
:precondition
(and
(not (RIGHTOF10-ROBOT))
)
:effect
(and
(Flag143prime-)
(not (Flag143-))
)

)
(:action Prim143Action-1
:parameters ()
:precondition
(and
(not (LEFTOF8-ROBOT))
)
:effect
(and
(Flag143prime-)
(not (Flag143-))
)

)
(:action Prim144Action-0
:parameters ()
:precondition
(and
(not (RIGHTOF8-ROBOT))
)
:effect
(and
(Flag144prime-)
(not (Flag144-))
)

)
(:action Prim144Action-1
:parameters ()
:precondition
(and
(not (LEFTOF8-ROBOT))
)
:effect
(and
(Flag144prime-)
(not (Flag144-))
)

)
(:action Prim145Action-0
:parameters ()
:precondition
(and
(not (RIGHTOF7-ROBOT))
)
:effect
(and
(Flag145prime-)
(not (Flag145-))
)

)
(:action Prim145Action-1
:parameters ()
:precondition
(and
(not (LEFTOF7-ROBOT))
)
:effect
(and
(Flag145prime-)
(not (Flag145-))
)

)
(:action Prim146Action-0
:parameters ()
:precondition
(and
(not (LEFTOF8-ROBOT))
)
:effect
(and
(Flag146prime-)
(not (Flag146-))
)

)
(:action Prim146Action-1
:parameters ()
:precondition
(and
(not (RIGHTOF12-ROBOT))
)
:effect
(and
(Flag146prime-)
(not (Flag146-))
)

)
(:action Prim147Action-0
:parameters ()
:precondition
(and
(not (LEFTOF8-ROBOT))
)
:effect
(and
(Flag147prime-)
(not (Flag147-))
)

)
(:action Prim147Action-1
:parameters ()
:precondition
(and
(not (RIGHTOF15-ROBOT))
)
:effect
(and
(Flag147prime-)
(not (Flag147-))
)

)
(:action Prim148Action-0
:parameters ()
:precondition
(and
(not (RIGHTOF11-ROBOT))
)
:effect
(and
(Flag148prime-)
(not (Flag148-))
)

)
(:action Prim148Action-1
:parameters ()
:precondition
(and
(not (LEFTOF8-ROBOT))
)
:effect
(and
(Flag148prime-)
(not (Flag148-))
)

)
(:action Flag150Action
:parameters ()
:precondition
(and
(RIGHTOF13-ROBOT)
(LEFTOF9-ROBOT)
)
:effect
(and
(Flag150-)
(Flag150prime-)
)

)
(:action Flag151Action
:parameters ()
:precondition
(and
(RIGHTOF8-ROBOT)
(LEFTOF9-ROBOT)
)
:effect
(and
(Flag151-)
(Flag151prime-)
)

)
(:action Flag152Action
:parameters ()
:precondition
(and
(LEFTOF9-ROBOT)
(RIGHTOF9-ROBOT)
)
:effect
(and
(Flag152-)
(Flag152prime-)
)

)
(:action Flag153Action
:parameters ()
:precondition
(and
(RIGHTOF10-ROBOT)
(LEFTOF9-ROBOT)
)
:effect
(and
(Flag153-)
(Flag153prime-)
)

)
(:action Flag154Action
:parameters ()
:precondition
(and
(LEFTOF8-ROBOT)
(RIGHTOF14-ROBOT)
)
:effect
(and
(Flag154-)
(Flag154prime-)
)

)
(:action Flag155Action
:parameters ()
:precondition
(and
(LEFTOF9-ROBOT)
(RIGHTOF12-ROBOT)
)
:effect
(and
(Flag155-)
(Flag155prime-)
)

)
(:action Flag156Action
:parameters ()
:precondition
(and
(RIGHTOF11-ROBOT)
(LEFTOF9-ROBOT)
)
:effect
(and
(Flag156-)
(Flag156prime-)
)

)
(:action Flag157Action
:parameters ()
:precondition
(and
(LEFTOF9-ROBOT)
(RIGHTOF15-ROBOT)
)
:effect
(and
(Flag157-)
(Flag157prime-)
)

)
(:action Flag158Action
:parameters ()
:precondition
(and
(LEFTOF9-ROBOT)
(RIGHTOF14-ROBOT)
)
:effect
(and
(Flag158-)
(Flag158prime-)
)

)
(:action Prim150Action-0
:parameters ()
:precondition
(and
(not (RIGHTOF13-ROBOT))
)
:effect
(and
(Flag150prime-)
(not (Flag150-))
)

)
(:action Prim150Action-1
:parameters ()
:precondition
(and
(not (LEFTOF9-ROBOT))
)
:effect
(and
(Flag150prime-)
(not (Flag150-))
)

)
(:action Prim151Action-0
:parameters ()
:precondition
(and
(not (RIGHTOF8-ROBOT))
)
:effect
(and
(Flag151prime-)
(not (Flag151-))
)

)
(:action Prim151Action-1
:parameters ()
:precondition
(and
(not (LEFTOF9-ROBOT))
)
:effect
(and
(Flag151prime-)
(not (Flag151-))
)

)
(:action Prim152Action-0
:parameters ()
:precondition
(and
(not (LEFTOF9-ROBOT))
)
:effect
(and
(Flag152prime-)
(not (Flag152-))
)

)
(:action Prim152Action-1
:parameters ()
:precondition
(and
(not (RIGHTOF9-ROBOT))
)
:effect
(and
(Flag152prime-)
(not (Flag152-))
)

)
(:action Prim153Action-0
:parameters ()
:precondition
(and
(not (RIGHTOF10-ROBOT))
)
:effect
(and
(Flag153prime-)
(not (Flag153-))
)

)
(:action Prim153Action-1
:parameters ()
:precondition
(and
(not (LEFTOF9-ROBOT))
)
:effect
(and
(Flag153prime-)
(not (Flag153-))
)

)
(:action Prim154Action-0
:parameters ()
:precondition
(and
(not (LEFTOF8-ROBOT))
)
:effect
(and
(Flag154prime-)
(not (Flag154-))
)

)
(:action Prim154Action-1
:parameters ()
:precondition
(and
(not (RIGHTOF14-ROBOT))
)
:effect
(and
(Flag154prime-)
(not (Flag154-))
)

)
(:action Prim155Action-0
:parameters ()
:precondition
(and
(not (LEFTOF9-ROBOT))
)
:effect
(and
(Flag155prime-)
(not (Flag155-))
)

)
(:action Prim155Action-1
:parameters ()
:precondition
(and
(not (RIGHTOF12-ROBOT))
)
:effect
(and
(Flag155prime-)
(not (Flag155-))
)

)
(:action Prim156Action-0
:parameters ()
:precondition
(and
(not (RIGHTOF11-ROBOT))
)
:effect
(and
(Flag156prime-)
(not (Flag156-))
)

)
(:action Prim156Action-1
:parameters ()
:precondition
(and
(not (LEFTOF9-ROBOT))
)
:effect
(and
(Flag156prime-)
(not (Flag156-))
)

)
(:action Prim157Action-0
:parameters ()
:precondition
(and
(not (LEFTOF9-ROBOT))
)
:effect
(and
(Flag157prime-)
(not (Flag157-))
)

)
(:action Prim157Action-1
:parameters ()
:precondition
(and
(not (RIGHTOF15-ROBOT))
)
:effect
(and
(Flag157prime-)
(not (Flag157-))
)

)
(:action Prim158Action-0
:parameters ()
:precondition
(and
(not (LEFTOF9-ROBOT))
)
:effect
(and
(Flag158prime-)
(not (Flag158-))
)

)
(:action Prim158Action-1
:parameters ()
:precondition
(and
(not (RIGHTOF14-ROBOT))
)
:effect
(and
(Flag158prime-)
(not (Flag158-))
)

)
(:action Flag160Action
:parameters ()
:precondition
(and
(RIGHTOF14-ROBOT)
(LEFTOF10-ROBOT)
)
:effect
(and
(Flag160-)
(Flag160prime-)
)

)
(:action Flag161Action
:parameters ()
:precondition
(and
(RIGHTOF11-ROBOT)
(LEFTOF10-ROBOT)
)
:effect
(and
(Flag161-)
(Flag161prime-)
)

)
(:action Flag162Action
:parameters ()
:precondition
(and
(RIGHTOF10-ROBOT)
(LEFTOF10-ROBOT)
)
:effect
(and
(Flag162-)
(Flag162prime-)
)

)
(:action Flag163Action
:parameters ()
:precondition
(and
(LEFTOF10-ROBOT)
(RIGHTOF12-ROBOT)
)
:effect
(and
(Flag163-)
(Flag163prime-)
)

)
(:action Flag164Action
:parameters ()
:precondition
(and
(RIGHTOF13-ROBOT)
(LEFTOF10-ROBOT)
)
:effect
(and
(Flag164-)
(Flag164prime-)
)

)
(:action Flag165Action
:parameters ()
:precondition
(and
(LEFTOF10-ROBOT)
(RIGHTOF9-ROBOT)
)
:effect
(and
(Flag165-)
(Flag165prime-)
)

)
(:action Flag166Action
:parameters ()
:precondition
(and
(LEFTOF10-ROBOT)
(RIGHTOF15-ROBOT)
)
:effect
(and
(Flag166-)
(Flag166prime-)
)

)
(:action Prim160Action-0
:parameters ()
:precondition
(and
(not (RIGHTOF14-ROBOT))
)
:effect
(and
(Flag160prime-)
(not (Flag160-))
)

)
(:action Prim160Action-1
:parameters ()
:precondition
(and
(not (LEFTOF10-ROBOT))
)
:effect
(and
(Flag160prime-)
(not (Flag160-))
)

)
(:action Prim161Action-0
:parameters ()
:precondition
(and
(not (RIGHTOF11-ROBOT))
)
:effect
(and
(Flag161prime-)
(not (Flag161-))
)

)
(:action Prim161Action-1
:parameters ()
:precondition
(and
(not (LEFTOF10-ROBOT))
)
:effect
(and
(Flag161prime-)
(not (Flag161-))
)

)
(:action Prim162Action-0
:parameters ()
:precondition
(and
(not (RIGHTOF10-ROBOT))
)
:effect
(and
(Flag162prime-)
(not (Flag162-))
)

)
(:action Prim162Action-1
:parameters ()
:precondition
(and
(not (LEFTOF10-ROBOT))
)
:effect
(and
(Flag162prime-)
(not (Flag162-))
)

)
(:action Prim163Action-0
:parameters ()
:precondition
(and
(not (LEFTOF10-ROBOT))
)
:effect
(and
(Flag163prime-)
(not (Flag163-))
)

)
(:action Prim163Action-1
:parameters ()
:precondition
(and
(not (RIGHTOF12-ROBOT))
)
:effect
(and
(Flag163prime-)
(not (Flag163-))
)

)
(:action Prim164Action-0
:parameters ()
:precondition
(and
(not (RIGHTOF13-ROBOT))
)
:effect
(and
(Flag164prime-)
(not (Flag164-))
)

)
(:action Prim164Action-1
:parameters ()
:precondition
(and
(not (LEFTOF10-ROBOT))
)
:effect
(and
(Flag164prime-)
(not (Flag164-))
)

)
(:action Prim165Action-0
:parameters ()
:precondition
(and
(not (LEFTOF10-ROBOT))
)
:effect
(and
(Flag165prime-)
(not (Flag165-))
)

)
(:action Prim165Action-1
:parameters ()
:precondition
(and
(not (RIGHTOF9-ROBOT))
)
:effect
(and
(Flag165prime-)
(not (Flag165-))
)

)
(:action Prim166Action-0
:parameters ()
:precondition
(and
(not (LEFTOF10-ROBOT))
)
:effect
(and
(Flag166prime-)
(not (Flag166-))
)

)
(:action Prim166Action-1
:parameters ()
:precondition
(and
(not (RIGHTOF15-ROBOT))
)
:effect
(and
(Flag166prime-)
(not (Flag166-))
)

)
(:action Flag168Action
:parameters ()
:precondition
(and
(LEFTOF11-ROBOT)
(RIGHTOF14-ROBOT)
)
:effect
(and
(Flag168-)
(Flag168prime-)
)

)
(:action Flag169Action
:parameters ()
:precondition
(and
(RIGHTOF11-ROBOT)
(LEFTOF11-ROBOT)
)
:effect
(and
(Flag169-)
(Flag169prime-)
)

)
(:action Flag170Action
:parameters ()
:precondition
(and
(LEFTOF10-ROBOT)
(RIGHTOF14-ROBOT)
)
:effect
(and
(Flag170-)
(Flag170prime-)
)

)
(:action Flag171Action
:parameters ()
:precondition
(and
(RIGHTOF13-ROBOT)
(LEFTOF11-ROBOT)
)
:effect
(and
(Flag171-)
(Flag171prime-)
)

)
(:action Flag172Action
:parameters ()
:precondition
(and
(LEFTOF11-ROBOT)
(RIGHTOF15-ROBOT)
)
:effect
(and
(Flag172-)
(Flag172prime-)
)

)
(:action Flag173Action
:parameters ()
:precondition
(and
(RIGHTOF10-ROBOT)
(LEFTOF11-ROBOT)
)
:effect
(and
(Flag173-)
(Flag173prime-)
)

)
(:action Flag174Action
:parameters ()
:precondition
(and
(LEFTOF11-ROBOT)
(RIGHTOF12-ROBOT)
)
:effect
(and
(Flag174-)
(Flag174prime-)
)

)
(:action Prim168Action-0
:parameters ()
:precondition
(and
(not (LEFTOF11-ROBOT))
)
:effect
(and
(Flag168prime-)
(not (Flag168-))
)

)
(:action Prim168Action-1
:parameters ()
:precondition
(and
(not (RIGHTOF14-ROBOT))
)
:effect
(and
(Flag168prime-)
(not (Flag168-))
)

)
(:action Prim169Action-0
:parameters ()
:precondition
(and
(not (RIGHTOF11-ROBOT))
)
:effect
(and
(Flag169prime-)
(not (Flag169-))
)

)
(:action Prim169Action-1
:parameters ()
:precondition
(and
(not (LEFTOF11-ROBOT))
)
:effect
(and
(Flag169prime-)
(not (Flag169-))
)

)
(:action Prim170Action-0
:parameters ()
:precondition
(and
(not (LEFTOF10-ROBOT))
)
:effect
(and
(Flag170prime-)
(not (Flag170-))
)

)
(:action Prim170Action-1
:parameters ()
:precondition
(and
(not (RIGHTOF14-ROBOT))
)
:effect
(and
(Flag170prime-)
(not (Flag170-))
)

)
(:action Prim171Action-0
:parameters ()
:precondition
(and
(not (RIGHTOF13-ROBOT))
)
:effect
(and
(Flag171prime-)
(not (Flag171-))
)

)
(:action Prim171Action-1
:parameters ()
:precondition
(and
(not (LEFTOF11-ROBOT))
)
:effect
(and
(Flag171prime-)
(not (Flag171-))
)

)
(:action Prim172Action-0
:parameters ()
:precondition
(and
(not (LEFTOF11-ROBOT))
)
:effect
(and
(Flag172prime-)
(not (Flag172-))
)

)
(:action Prim172Action-1
:parameters ()
:precondition
(and
(not (RIGHTOF15-ROBOT))
)
:effect
(and
(Flag172prime-)
(not (Flag172-))
)

)
(:action Prim173Action-0
:parameters ()
:precondition
(and
(not (RIGHTOF10-ROBOT))
)
:effect
(and
(Flag173prime-)
(not (Flag173-))
)

)
(:action Prim173Action-1
:parameters ()
:precondition
(and
(not (LEFTOF11-ROBOT))
)
:effect
(and
(Flag173prime-)
(not (Flag173-))
)

)
(:action Prim174Action-0
:parameters ()
:precondition
(and
(not (LEFTOF11-ROBOT))
)
:effect
(and
(Flag174prime-)
(not (Flag174-))
)

)
(:action Prim174Action-1
:parameters ()
:precondition
(and
(not (RIGHTOF12-ROBOT))
)
:effect
(and
(Flag174prime-)
(not (Flag174-))
)

)
(:action Flag176Action
:parameters ()
:precondition
(and
(RIGHTOF12-ROBOT)
(LEFTOF12-ROBOT)
)
:effect
(and
(Flag176-)
(Flag176prime-)
)

)
(:action Flag177Action
:parameters ()
:precondition
(and
(RIGHTOF15-ROBOT)
(LEFTOF12-ROBOT)
)
:effect
(and
(Flag177-)
(Flag177prime-)
)

)
(:action Flag178Action
:parameters ()
:precondition
(and
(RIGHTOF14-ROBOT)
(LEFTOF12-ROBOT)
)
:effect
(and
(Flag178-)
(Flag178prime-)
)

)
(:action Flag179Action
:parameters ()
:precondition
(and
(RIGHTOF11-ROBOT)
(LEFTOF12-ROBOT)
)
:effect
(and
(Flag179-)
(Flag179prime-)
)

)
(:action Flag180Action
:parameters ()
:precondition
(and
(RIGHTOF13-ROBOT)
(LEFTOF12-ROBOT)
)
:effect
(and
(Flag180-)
(Flag180prime-)
)

)
(:action Prim176Action-0
:parameters ()
:precondition
(and
(not (RIGHTOF12-ROBOT))
)
:effect
(and
(Flag176prime-)
(not (Flag176-))
)

)
(:action Prim176Action-1
:parameters ()
:precondition
(and
(not (LEFTOF12-ROBOT))
)
:effect
(and
(Flag176prime-)
(not (Flag176-))
)

)
(:action Prim177Action-0
:parameters ()
:precondition
(and
(not (RIGHTOF15-ROBOT))
)
:effect
(and
(Flag177prime-)
(not (Flag177-))
)

)
(:action Prim177Action-1
:parameters ()
:precondition
(and
(not (LEFTOF12-ROBOT))
)
:effect
(and
(Flag177prime-)
(not (Flag177-))
)

)
(:action Prim178Action-0
:parameters ()
:precondition
(and
(not (RIGHTOF14-ROBOT))
)
:effect
(and
(Flag178prime-)
(not (Flag178-))
)

)
(:action Prim178Action-1
:parameters ()
:precondition
(and
(not (LEFTOF12-ROBOT))
)
:effect
(and
(Flag178prime-)
(not (Flag178-))
)

)
(:action Prim179Action-0
:parameters ()
:precondition
(and
(not (RIGHTOF11-ROBOT))
)
:effect
(and
(Flag179prime-)
(not (Flag179-))
)

)
(:action Prim179Action-1
:parameters ()
:precondition
(and
(not (LEFTOF12-ROBOT))
)
:effect
(and
(Flag179prime-)
(not (Flag179-))
)

)
(:action Prim180Action-0
:parameters ()
:precondition
(and
(not (RIGHTOF13-ROBOT))
)
:effect
(and
(Flag180prime-)
(not (Flag180-))
)

)
(:action Prim180Action-1
:parameters ()
:precondition
(and
(not (LEFTOF12-ROBOT))
)
:effect
(and
(Flag180prime-)
(not (Flag180-))
)

)
(:action Flag182Action
:parameters ()
:precondition
(and
(LEFTOF13-ROBOT)
(RIGHTOF15-ROBOT)
)
:effect
(and
(Flag182-)
(Flag182prime-)
)

)
(:action Flag183Action
:parameters ()
:precondition
(and
(LEFTOF13-ROBOT)
(RIGHTOF12-ROBOT)
)
:effect
(and
(Flag183-)
(Flag183prime-)
)

)
(:action Flag184Action
:parameters ()
:precondition
(and
(LEFTOF13-ROBOT)
(RIGHTOF14-ROBOT)
)
:effect
(and
(Flag184-)
(Flag184prime-)
)

)
(:action Flag185Action
:parameters ()
:precondition
(and
(RIGHTOF13-ROBOT)
(LEFTOF13-ROBOT)
)
:effect
(and
(Flag185-)
(Flag185prime-)
)

)
(:action Flag186Action
:parameters ()
:precondition
(and
(LEFTOF12-ROBOT)
(RIGHTOF12-ROBOT)
)
:effect
(and
(Flag186-)
(Flag186prime-)
)

)
(:action Prim182Action-0
:parameters ()
:precondition
(and
(not (LEFTOF13-ROBOT))
)
:effect
(and
(Flag182prime-)
(not (Flag182-))
)

)
(:action Prim182Action-1
:parameters ()
:precondition
(and
(not (RIGHTOF15-ROBOT))
)
:effect
(and
(Flag182prime-)
(not (Flag182-))
)

)
(:action Prim183Action-0
:parameters ()
:precondition
(and
(not (LEFTOF13-ROBOT))
)
:effect
(and
(Flag183prime-)
(not (Flag183-))
)

)
(:action Prim183Action-1
:parameters ()
:precondition
(and
(not (RIGHTOF12-ROBOT))
)
:effect
(and
(Flag183prime-)
(not (Flag183-))
)

)
(:action Prim184Action-0
:parameters ()
:precondition
(and
(not (LEFTOF13-ROBOT))
)
:effect
(and
(Flag184prime-)
(not (Flag184-))
)

)
(:action Prim184Action-1
:parameters ()
:precondition
(and
(not (RIGHTOF14-ROBOT))
)
:effect
(and
(Flag184prime-)
(not (Flag184-))
)

)
(:action Prim185Action-0
:parameters ()
:precondition
(and
(not (RIGHTOF13-ROBOT))
)
:effect
(and
(Flag185prime-)
(not (Flag185-))
)

)
(:action Prim185Action-1
:parameters ()
:precondition
(and
(not (LEFTOF13-ROBOT))
)
:effect
(and
(Flag185prime-)
(not (Flag185-))
)

)
(:action Prim186Action-0
:parameters ()
:precondition
(and
(not (LEFTOF12-ROBOT))
)
:effect
(and
(Flag186prime-)
(not (Flag186-))
)

)
(:action Prim186Action-1
:parameters ()
:precondition
(and
(not (RIGHTOF12-ROBOT))
)
:effect
(and
(Flag186prime-)
(not (Flag186-))
)

)
(:action Flag188Action
:parameters ()
:precondition
(and
(LEFTOF14-ROBOT)
(RIGHTOF14-ROBOT)
)
:effect
(and
(Flag188-)
(Flag188prime-)
)

)
(:action Flag189Action
:parameters ()
:precondition
(and
(RIGHTOF13-ROBOT)
(LEFTOF14-ROBOT)
)
:effect
(and
(Flag189-)
(Flag189prime-)
)

)
(:action Flag190Action
:parameters ()
:precondition
(and
(LEFTOF14-ROBOT)
(RIGHTOF15-ROBOT)
)
:effect
(and
(Flag190-)
(Flag190prime-)
)

)
(:action Prim188Action-0
:parameters ()
:precondition
(and
(not (LEFTOF14-ROBOT))
)
:effect
(and
(Flag188prime-)
(not (Flag188-))
)

)
(:action Prim188Action-1
:parameters ()
:precondition
(and
(not (RIGHTOF14-ROBOT))
)
:effect
(and
(Flag188prime-)
(not (Flag188-))
)

)
(:action Prim189Action-0
:parameters ()
:precondition
(and
(not (RIGHTOF13-ROBOT))
)
:effect
(and
(Flag189prime-)
(not (Flag189-))
)

)
(:action Prim189Action-1
:parameters ()
:precondition
(and
(not (LEFTOF14-ROBOT))
)
:effect
(and
(Flag189prime-)
(not (Flag189-))
)

)
(:action Prim190Action-0
:parameters ()
:precondition
(and
(not (LEFTOF14-ROBOT))
)
:effect
(and
(Flag190prime-)
(not (Flag190-))
)

)
(:action Prim190Action-1
:parameters ()
:precondition
(and
(not (RIGHTOF15-ROBOT))
)
:effect
(and
(Flag190prime-)
(not (Flag190-))
)

)
(:action Flag192Action
:parameters ()
:precondition
(and
(RIGHTOF15-ROBOT)
(LEFTOF15-ROBOT)
)
:effect
(and
(Flag192-)
(Flag192prime-)
)

)
(:action Flag193Action
:parameters ()
:precondition
(and
(RIGHTOF14-ROBOT)
(LEFTOF15-ROBOT)
)
:effect
(and
(Flag193-)
(Flag193prime-)
)

)
(:action Prim192Action-0
:parameters ()
:precondition
(and
(not (RIGHTOF15-ROBOT))
)
:effect
(and
(Flag192prime-)
(not (Flag192-))
)

)
(:action Prim193Action-0
:parameters ()
:precondition
(and
(not (RIGHTOF14-ROBOT))
)
:effect
(and
(Flag193prime-)
(not (Flag193-))
)

)
(:action Flag195Action-1
:parameters ()
:precondition
(and
(LEFTOF10-ROBOT)
)
:effect
(and
(Flag195-)
(Flag195prime-)
)

)
(:action Flag195Action-2
:parameters ()
:precondition
(and
(LEFTOF11-ROBOT)
)
:effect
(and
(Flag195-)
(Flag195prime-)
)

)
(:action Flag195Action-3
:parameters ()
:precondition
(and
(LEFTOF9-ROBOT)
)
:effect
(and
(Flag195-)
(Flag195prime-)
)

)
(:action Flag195Action-4
:parameters ()
:precondition
(and
(LEFTOF2-ROBOT)
)
:effect
(and
(Flag195-)
(Flag195prime-)
)

)
(:action Flag195Action-5
:parameters ()
:precondition
(and
(LEFTOF13-ROBOT)
)
:effect
(and
(Flag195-)
(Flag195prime-)
)

)
(:action Flag195Action-6
:parameters ()
:precondition
(and
(LEFTOF1-ROBOT)
)
:effect
(and
(Flag195-)
(Flag195prime-)
)

)
(:action Flag195Action-7
:parameters ()
:precondition
(and
(LEFTOF12-ROBOT)
)
:effect
(and
(Flag195-)
(Flag195prime-)
)

)
(:action Flag195Action-8
:parameters ()
:precondition
(and
(LEFTOF6-ROBOT)
)
:effect
(and
(Flag195-)
(Flag195prime-)
)

)
(:action Flag195Action-9
:parameters ()
:precondition
(and
(LEFTOF5-ROBOT)
)
:effect
(and
(Flag195-)
(Flag195prime-)
)

)
(:action Flag195Action-10
:parameters ()
:precondition
(and
(LEFTOF16-ROBOT)
)
:effect
(and
(Flag195-)
(Flag195prime-)
)

)
(:action Flag195Action-11
:parameters ()
:precondition
(and
(LEFTOF14-ROBOT)
)
:effect
(and
(Flag195-)
(Flag195prime-)
)

)
(:action Flag195Action-12
:parameters ()
:precondition
(and
(LEFTOF3-ROBOT)
)
:effect
(and
(Flag195-)
(Flag195prime-)
)

)
(:action Flag195Action-13
:parameters ()
:precondition
(and
(LEFTOF4-ROBOT)
)
:effect
(and
(Flag195-)
(Flag195prime-)
)

)
(:action Flag195Action-14
:parameters ()
:precondition
(and
(LEFTOF7-ROBOT)
)
:effect
(and
(Flag195-)
(Flag195prime-)
)

)
(:action Flag195Action-15
:parameters ()
:precondition
(and
(LEFTOF8-ROBOT)
)
:effect
(and
(Flag195-)
(Flag195prime-)
)

)
(:action Prim195Action
:parameters ()
:precondition
(and
(not (LEFTOF15-ROBOT))
(not (LEFTOF10-ROBOT))
(not (LEFTOF11-ROBOT))
(not (LEFTOF9-ROBOT))
(not (LEFTOF2-ROBOT))
(not (LEFTOF13-ROBOT))
(not (LEFTOF1-ROBOT))
(not (LEFTOF12-ROBOT))
(not (LEFTOF6-ROBOT))
(not (LEFTOF5-ROBOT))
(not (LEFTOF16-ROBOT))
(not (LEFTOF14-ROBOT))
(not (LEFTOF3-ROBOT))
(not (LEFTOF4-ROBOT))
(not (LEFTOF7-ROBOT))
(not (LEFTOF8-ROBOT))
)
:effect
(and
(Flag195prime-)
(not (Flag195-))
)

)
(:action Flag196Action
:parameters ()
:precondition
(and
(LEFTOF15-ROBOT)
(RIGHTOF15-ROBOT)
)
:effect
(and
(Flag196-)
(Flag196prime-)
)

)
(:action Flag197Action
:parameters ()
:precondition
(and
(LEFTOF16-ROBOT)
(RIGHTOF15-ROBOT)
)
:effect
(and
(Flag197-)
(Flag197prime-)
)

)
(:action Prim196Action-1
:parameters ()
:precondition
(and
(not (RIGHTOF15-ROBOT))
)
:effect
(and
(Flag196prime-)
(not (Flag196-))
)

)
(:action Prim197Action-0
:parameters ()
:precondition
(and
(not (LEFTOF16-ROBOT))
)
:effect
(and
(Flag197prime-)
(not (Flag197-))
)

)
(:action Prim197Action-1
:parameters ()
:precondition
(and
(not (RIGHTOF15-ROBOT))
)
:effect
(and
(Flag197prime-)
(not (Flag197-))
)

)
(:action Flag258Action-0
:parameters ()
:precondition
(and
(Flag228-)
(Flag228prime-)
)
:effect
(and
(Flag258-)
(Flag258prime-)
)

)
(:action Flag258Action-1
:parameters ()
:precondition
(and
(Flag229-)
(Flag229prime-)
)
:effect
(and
(Flag258-)
(Flag258prime-)
)

)
(:action Flag258Action-2
:parameters ()
:precondition
(and
(Flag230-)
(Flag230prime-)
)
:effect
(and
(Flag258-)
(Flag258prime-)
)

)
(:action Flag258Action-3
:parameters ()
:precondition
(and
(Flag231-)
(Flag231prime-)
)
:effect
(and
(Flag258-)
(Flag258prime-)
)

)
(:action Flag258Action-4
:parameters ()
:precondition
(and
(Flag232-)
(Flag232prime-)
)
:effect
(and
(Flag258-)
(Flag258prime-)
)

)
(:action Flag258Action-5
:parameters ()
:precondition
(and
(Flag233-)
(Flag233prime-)
)
:effect
(and
(Flag258-)
(Flag258prime-)
)

)
(:action Flag258Action-6
:parameters ()
:precondition
(and
(Flag234-)
(Flag234prime-)
)
:effect
(and
(Flag258-)
(Flag258prime-)
)

)
(:action Flag258Action-7
:parameters ()
:precondition
(and
(Flag235-)
(Flag235prime-)
)
:effect
(and
(Flag258-)
(Flag258prime-)
)

)
(:action Flag258Action-8
:parameters ()
:precondition
(and
(Flag236-)
(Flag236prime-)
)
:effect
(and
(Flag258-)
(Flag258prime-)
)

)
(:action Flag258Action-9
:parameters ()
:precondition
(and
(Flag237-)
(Flag237prime-)
)
:effect
(and
(Flag258-)
(Flag258prime-)
)

)
(:action Flag258Action-10
:parameters ()
:precondition
(and
(Flag238-)
(Flag238prime-)
)
:effect
(and
(Flag258-)
(Flag258prime-)
)

)
(:action Flag258Action-11
:parameters ()
:precondition
(and
(Flag239-)
(Flag239prime-)
)
:effect
(and
(Flag258-)
(Flag258prime-)
)

)
(:action Flag258Action-12
:parameters ()
:precondition
(and
(Flag240-)
(Flag240prime-)
)
:effect
(and
(Flag258-)
(Flag258prime-)
)

)
(:action Flag258Action-13
:parameters ()
:precondition
(and
(Flag241-)
(Flag241prime-)
)
:effect
(and
(Flag258-)
(Flag258prime-)
)

)
(:action Flag258Action-14
:parameters ()
:precondition
(and
(Flag242-)
(Flag242prime-)
)
:effect
(and
(Flag258-)
(Flag258prime-)
)

)
(:action Flag258Action-15
:parameters ()
:precondition
(and
(Flag243-)
(Flag243prime-)
)
:effect
(and
(Flag258-)
(Flag258prime-)
)

)
(:action Flag258Action-16
:parameters ()
:precondition
(and
(Flag244-)
(Flag244prime-)
)
:effect
(and
(Flag258-)
(Flag258prime-)
)

)
(:action Flag258Action-17
:parameters ()
:precondition
(and
(Flag245-)
(Flag245prime-)
)
:effect
(and
(Flag258-)
(Flag258prime-)
)

)
(:action Flag258Action-18
:parameters ()
:precondition
(and
(Flag246-)
(Flag246prime-)
)
:effect
(and
(Flag258-)
(Flag258prime-)
)

)
(:action Flag258Action-19
:parameters ()
:precondition
(and
(Flag247-)
(Flag247prime-)
)
:effect
(and
(Flag258-)
(Flag258prime-)
)

)
(:action Flag258Action-20
:parameters ()
:precondition
(and
(Flag248-)
(Flag248prime-)
)
:effect
(and
(Flag258-)
(Flag258prime-)
)

)
(:action Flag258Action-21
:parameters ()
:precondition
(and
(Flag249-)
(Flag249prime-)
)
:effect
(and
(Flag258-)
(Flag258prime-)
)

)
(:action Flag258Action-22
:parameters ()
:precondition
(and
(Flag250-)
(Flag250prime-)
)
:effect
(and
(Flag258-)
(Flag258prime-)
)

)
(:action Flag258Action-23
:parameters ()
:precondition
(and
(Flag251-)
(Flag251prime-)
)
:effect
(and
(Flag258-)
(Flag258prime-)
)

)
(:action Flag258Action-24
:parameters ()
:precondition
(and
(Flag252-)
(Flag252prime-)
)
:effect
(and
(Flag258-)
(Flag258prime-)
)

)
(:action Flag258Action-25
:parameters ()
:precondition
(and
(Flag253-)
(Flag253prime-)
)
:effect
(and
(Flag258-)
(Flag258prime-)
)

)
(:action Flag258Action-26
:parameters ()
:precondition
(and
(Flag254-)
(Flag254prime-)
)
:effect
(and
(Flag258-)
(Flag258prime-)
)

)
(:action Flag258Action-27
:parameters ()
:precondition
(and
(Flag255-)
(Flag255prime-)
)
:effect
(and
(Flag258-)
(Flag258prime-)
)

)
(:action Flag258Action-28
:parameters ()
:precondition
(and
(Flag256-)
(Flag256prime-)
)
:effect
(and
(Flag258-)
(Flag258prime-)
)

)
(:action Flag258Action-29
:parameters ()
:precondition
(and
(Flag257-)
(Flag257prime-)
)
:effect
(and
(Flag258-)
(Flag258prime-)
)

)
(:action Prim258Action
:parameters ()
:precondition
(and
(not (Flag228-))
(Flag228prime-)
(not (Flag229-))
(Flag229prime-)
(not (Flag230-))
(Flag230prime-)
(not (Flag231-))
(Flag231prime-)
(not (Flag232-))
(Flag232prime-)
(not (Flag233-))
(Flag233prime-)
(not (Flag234-))
(Flag234prime-)
(not (Flag235-))
(Flag235prime-)
(not (Flag236-))
(Flag236prime-)
(not (Flag237-))
(Flag237prime-)
(not (Flag238-))
(Flag238prime-)
(not (Flag239-))
(Flag239prime-)
(not (Flag240-))
(Flag240prime-)
(not (Flag241-))
(Flag241prime-)
(not (Flag242-))
(Flag242prime-)
(not (Flag243-))
(Flag243prime-)
(not (Flag244-))
(Flag244prime-)
(not (Flag245-))
(Flag245prime-)
(not (Flag246-))
(Flag246prime-)
(not (Flag247-))
(Flag247prime-)
(not (Flag248-))
(Flag248prime-)
(not (Flag249-))
(Flag249prime-)
(not (Flag250-))
(Flag250prime-)
(not (Flag251-))
(Flag251prime-)
(not (Flag252-))
(Flag252prime-)
(not (Flag253-))
(Flag253prime-)
(not (Flag254-))
(Flag254prime-)
(not (Flag255-))
(Flag255prime-)
(not (Flag256-))
(Flag256prime-)
(not (Flag257-))
(Flag257prime-)
)
:effect
(and
(Flag258prime-)
(not (Flag258-))
)

)
(:action Flag274Action-0
:parameters ()
:precondition
(and
(Flag259-)
(Flag259prime-)
)
:effect
(and
(Flag274-)
(Flag274prime-)
)

)
(:action Flag274Action-1
:parameters ()
:precondition
(and
(Flag260-)
(Flag260prime-)
)
:effect
(and
(Flag274-)
(Flag274prime-)
)

)
(:action Flag274Action-2
:parameters ()
:precondition
(and
(Flag240-)
(Flag240prime-)
)
:effect
(and
(Flag274-)
(Flag274prime-)
)

)
(:action Flag274Action-3
:parameters ()
:precondition
(and
(Flag261-)
(Flag261prime-)
)
:effect
(and
(Flag274-)
(Flag274prime-)
)

)
(:action Flag274Action-4
:parameters ()
:precondition
(and
(Flag229-)
(Flag229prime-)
)
:effect
(and
(Flag274-)
(Flag274prime-)
)

)
(:action Flag274Action-5
:parameters ()
:precondition
(and
(Flag246-)
(Flag246prime-)
)
:effect
(and
(Flag274-)
(Flag274prime-)
)

)
(:action Flag274Action-6
:parameters ()
:precondition
(and
(Flag231-)
(Flag231prime-)
)
:effect
(and
(Flag274-)
(Flag274prime-)
)

)
(:action Flag274Action-7
:parameters ()
:precondition
(and
(Flag232-)
(Flag232prime-)
)
:effect
(and
(Flag274-)
(Flag274prime-)
)

)
(:action Flag274Action-8
:parameters ()
:precondition
(and
(Flag262-)
(Flag262prime-)
)
:effect
(and
(Flag274-)
(Flag274prime-)
)

)
(:action Flag274Action-9
:parameters ()
:precondition
(and
(Flag233-)
(Flag233prime-)
)
:effect
(and
(Flag274-)
(Flag274prime-)
)

)
(:action Flag274Action-10
:parameters ()
:precondition
(and
(Flag234-)
(Flag234prime-)
)
:effect
(and
(Flag274-)
(Flag274prime-)
)

)
(:action Flag274Action-11
:parameters ()
:precondition
(and
(Flag236-)
(Flag236prime-)
)
:effect
(and
(Flag274-)
(Flag274prime-)
)

)
(:action Flag274Action-12
:parameters ()
:precondition
(and
(Flag263-)
(Flag263prime-)
)
:effect
(and
(Flag274-)
(Flag274prime-)
)

)
(:action Flag274Action-13
:parameters ()
:precondition
(and
(Flag264-)
(Flag264prime-)
)
:effect
(and
(Flag274-)
(Flag274prime-)
)

)
(:action Flag274Action-14
:parameters ()
:precondition
(and
(Flag237-)
(Flag237prime-)
)
:effect
(and
(Flag274-)
(Flag274prime-)
)

)
(:action Flag274Action-15
:parameters ()
:precondition
(and
(Flag238-)
(Flag238prime-)
)
:effect
(and
(Flag274-)
(Flag274prime-)
)

)
(:action Flag274Action-16
:parameters ()
:precondition
(and
(Flag265-)
(Flag265prime-)
)
:effect
(and
(Flag274-)
(Flag274prime-)
)

)
(:action Flag274Action-17
:parameters ()
:precondition
(and
(Flag239-)
(Flag239prime-)
)
:effect
(and
(Flag274-)
(Flag274prime-)
)

)
(:action Flag274Action-18
:parameters ()
:precondition
(and
(Flag266-)
(Flag266prime-)
)
:effect
(and
(Flag274-)
(Flag274prime-)
)

)
(:action Flag274Action-19
:parameters ()
:precondition
(and
(Flag241-)
(Flag241prime-)
)
:effect
(and
(Flag274-)
(Flag274prime-)
)

)
(:action Flag274Action-20
:parameters ()
:precondition
(and
(Flag242-)
(Flag242prime-)
)
:effect
(and
(Flag274-)
(Flag274prime-)
)

)
(:action Flag274Action-21
:parameters ()
:precondition
(and
(Flag267-)
(Flag267prime-)
)
:effect
(and
(Flag274-)
(Flag274prime-)
)

)
(:action Flag274Action-22
:parameters ()
:precondition
(and
(Flag243-)
(Flag243prime-)
)
:effect
(and
(Flag274-)
(Flag274prime-)
)

)
(:action Flag274Action-23
:parameters ()
:precondition
(and
(Flag268-)
(Flag268prime-)
)
:effect
(and
(Flag274-)
(Flag274prime-)
)

)
(:action Flag274Action-24
:parameters ()
:precondition
(and
(Flag245-)
(Flag245prime-)
)
:effect
(and
(Flag274-)
(Flag274prime-)
)

)
(:action Flag274Action-25
:parameters ()
:precondition
(and
(Flag244-)
(Flag244prime-)
)
:effect
(and
(Flag274-)
(Flag274prime-)
)

)
(:action Flag274Action-26
:parameters ()
:precondition
(and
(Flag269-)
(Flag269prime-)
)
:effect
(and
(Flag274-)
(Flag274prime-)
)

)
(:action Flag274Action-27
:parameters ()
:precondition
(and
(Flag270-)
(Flag270prime-)
)
:effect
(and
(Flag274-)
(Flag274prime-)
)

)
(:action Flag274Action-28
:parameters ()
:precondition
(and
(Flag247-)
(Flag247prime-)
)
:effect
(and
(Flag274-)
(Flag274prime-)
)

)
(:action Flag274Action-29
:parameters ()
:precondition
(and
(Flag248-)
(Flag248prime-)
)
:effect
(and
(Flag274-)
(Flag274prime-)
)

)
(:action Flag274Action-30
:parameters ()
:precondition
(and
(Flag249-)
(Flag249prime-)
)
:effect
(and
(Flag274-)
(Flag274prime-)
)

)
(:action Flag274Action-31
:parameters ()
:precondition
(and
(Flag250-)
(Flag250prime-)
)
:effect
(and
(Flag274-)
(Flag274prime-)
)

)
(:action Flag274Action-32
:parameters ()
:precondition
(and
(Flag251-)
(Flag251prime-)
)
:effect
(and
(Flag274-)
(Flag274prime-)
)

)
(:action Flag274Action-33
:parameters ()
:precondition
(and
(Flag252-)
(Flag252prime-)
)
:effect
(and
(Flag274-)
(Flag274prime-)
)

)
(:action Flag274Action-34
:parameters ()
:precondition
(and
(Flag271-)
(Flag271prime-)
)
:effect
(and
(Flag274-)
(Flag274prime-)
)

)
(:action Flag274Action-35
:parameters ()
:precondition
(and
(Flag253-)
(Flag253prime-)
)
:effect
(and
(Flag274-)
(Flag274prime-)
)

)
(:action Flag274Action-36
:parameters ()
:precondition
(and
(Flag272-)
(Flag272prime-)
)
:effect
(and
(Flag274-)
(Flag274prime-)
)

)
(:action Flag274Action-37
:parameters ()
:precondition
(and
(Flag273-)
(Flag273prime-)
)
:effect
(and
(Flag274-)
(Flag274prime-)
)

)
(:action Flag274Action-38
:parameters ()
:precondition
(and
(Flag254-)
(Flag254prime-)
)
:effect
(and
(Flag274-)
(Flag274prime-)
)

)
(:action Flag274Action-39
:parameters ()
:precondition
(and
(Flag255-)
(Flag255prime-)
)
:effect
(and
(Flag274-)
(Flag274prime-)
)

)
(:action Flag274Action-40
:parameters ()
:precondition
(and
(Flag256-)
(Flag256prime-)
)
:effect
(and
(Flag274-)
(Flag274prime-)
)

)
(:action Flag274Action-41
:parameters ()
:precondition
(and
(Flag257-)
(Flag257prime-)
)
:effect
(and
(Flag274-)
(Flag274prime-)
)

)
(:action Prim274Action
:parameters ()
:precondition
(and
(not (Flag259-))
(Flag259prime-)
(not (Flag260-))
(Flag260prime-)
(not (Flag240-))
(Flag240prime-)
(not (Flag261-))
(Flag261prime-)
(not (Flag229-))
(Flag229prime-)
(not (Flag246-))
(Flag246prime-)
(not (Flag231-))
(Flag231prime-)
(not (Flag232-))
(Flag232prime-)
(not (Flag262-))
(Flag262prime-)
(not (Flag233-))
(Flag233prime-)
(not (Flag234-))
(Flag234prime-)
(not (Flag236-))
(Flag236prime-)
(not (Flag263-))
(Flag263prime-)
(not (Flag264-))
(Flag264prime-)
(not (Flag237-))
(Flag237prime-)
(not (Flag238-))
(Flag238prime-)
(not (Flag265-))
(Flag265prime-)
(not (Flag239-))
(Flag239prime-)
(not (Flag266-))
(Flag266prime-)
(not (Flag241-))
(Flag241prime-)
(not (Flag242-))
(Flag242prime-)
(not (Flag267-))
(Flag267prime-)
(not (Flag243-))
(Flag243prime-)
(not (Flag268-))
(Flag268prime-)
(not (Flag245-))
(Flag245prime-)
(not (Flag244-))
(Flag244prime-)
(not (Flag269-))
(Flag269prime-)
(not (Flag270-))
(Flag270prime-)
(not (Flag247-))
(Flag247prime-)
(not (Flag248-))
(Flag248prime-)
(not (Flag249-))
(Flag249prime-)
(not (Flag250-))
(Flag250prime-)
(not (Flag251-))
(Flag251prime-)
(not (Flag252-))
(Flag252prime-)
(not (Flag271-))
(Flag271prime-)
(not (Flag253-))
(Flag253prime-)
(not (Flag272-))
(Flag272prime-)
(not (Flag273-))
(Flag273prime-)
(not (Flag254-))
(Flag254prime-)
(not (Flag255-))
(Flag255prime-)
(not (Flag256-))
(Flag256prime-)
(not (Flag257-))
(Flag257prime-)
)
:effect
(and
(Flag274prime-)
(not (Flag274-))
)

)
(:action Flag290Action-0
:parameters ()
:precondition
(and
(Flag262-)
(Flag262prime-)
)
:effect
(and
(Flag290-)
(Flag290prime-)
)

)
(:action Flag290Action-1
:parameters ()
:precondition
(and
(Flag259-)
(Flag259prime-)
)
:effect
(and
(Flag290-)
(Flag290prime-)
)

)
(:action Flag290Action-2
:parameters ()
:precondition
(and
(Flag275-)
(Flag275prime-)
)
:effect
(and
(Flag290-)
(Flag290prime-)
)

)
(:action Flag290Action-3
:parameters ()
:precondition
(and
(Flag276-)
(Flag276prime-)
)
:effect
(and
(Flag290-)
(Flag290prime-)
)

)
(:action Flag290Action-4
:parameters ()
:precondition
(and
(Flag277-)
(Flag277prime-)
)
:effect
(and
(Flag290-)
(Flag290prime-)
)

)
(:action Flag290Action-5
:parameters ()
:precondition
(and
(Flag260-)
(Flag260prime-)
)
:effect
(and
(Flag290-)
(Flag290prime-)
)

)
(:action Flag290Action-6
:parameters ()
:precondition
(and
(Flag261-)
(Flag261prime-)
)
:effect
(and
(Flag290-)
(Flag290prime-)
)

)
(:action Flag290Action-7
:parameters ()
:precondition
(and
(Flag229-)
(Flag229prime-)
)
:effect
(and
(Flag290-)
(Flag290prime-)
)

)
(:action Flag290Action-8
:parameters ()
:precondition
(and
(Flag246-)
(Flag246prime-)
)
:effect
(and
(Flag290-)
(Flag290prime-)
)

)
(:action Flag290Action-9
:parameters ()
:precondition
(and
(Flag231-)
(Flag231prime-)
)
:effect
(and
(Flag290-)
(Flag290prime-)
)

)
(:action Flag290Action-10
:parameters ()
:precondition
(and
(Flag263-)
(Flag263prime-)
)
:effect
(and
(Flag290-)
(Flag290prime-)
)

)
(:action Flag290Action-11
:parameters ()
:precondition
(and
(Flag232-)
(Flag232prime-)
)
:effect
(and
(Flag290-)
(Flag290prime-)
)

)
(:action Flag290Action-12
:parameters ()
:precondition
(and
(Flag278-)
(Flag278prime-)
)
:effect
(and
(Flag290-)
(Flag290prime-)
)

)
(:action Flag290Action-13
:parameters ()
:precondition
(and
(Flag233-)
(Flag233prime-)
)
:effect
(and
(Flag290-)
(Flag290prime-)
)

)
(:action Flag290Action-14
:parameters ()
:precondition
(and
(Flag234-)
(Flag234prime-)
)
:effect
(and
(Flag290-)
(Flag290prime-)
)

)
(:action Flag290Action-15
:parameters ()
:precondition
(and
(Flag279-)
(Flag279prime-)
)
:effect
(and
(Flag290-)
(Flag290prime-)
)

)
(:action Flag290Action-16
:parameters ()
:precondition
(and
(Flag236-)
(Flag236prime-)
)
:effect
(and
(Flag290-)
(Flag290prime-)
)

)
(:action Flag290Action-17
:parameters ()
:precondition
(and
(Flag280-)
(Flag280prime-)
)
:effect
(and
(Flag290-)
(Flag290prime-)
)

)
(:action Flag290Action-18
:parameters ()
:precondition
(and
(Flag250-)
(Flag250prime-)
)
:effect
(and
(Flag290-)
(Flag290prime-)
)

)
(:action Flag290Action-19
:parameters ()
:precondition
(and
(Flag264-)
(Flag264prime-)
)
:effect
(and
(Flag290-)
(Flag290prime-)
)

)
(:action Flag290Action-20
:parameters ()
:precondition
(and
(Flag281-)
(Flag281prime-)
)
:effect
(and
(Flag290-)
(Flag290prime-)
)

)
(:action Flag290Action-21
:parameters ()
:precondition
(and
(Flag237-)
(Flag237prime-)
)
:effect
(and
(Flag290-)
(Flag290prime-)
)

)
(:action Flag290Action-22
:parameters ()
:precondition
(and
(Flag282-)
(Flag282prime-)
)
:effect
(and
(Flag290-)
(Flag290prime-)
)

)
(:action Flag290Action-23
:parameters ()
:precondition
(and
(Flag283-)
(Flag283prime-)
)
:effect
(and
(Flag290-)
(Flag290prime-)
)

)
(:action Flag290Action-24
:parameters ()
:precondition
(and
(Flag284-)
(Flag284prime-)
)
:effect
(and
(Flag290-)
(Flag290prime-)
)

)
(:action Flag290Action-25
:parameters ()
:precondition
(and
(Flag239-)
(Flag239prime-)
)
:effect
(and
(Flag290-)
(Flag290prime-)
)

)
(:action Flag290Action-26
:parameters ()
:precondition
(and
(Flag241-)
(Flag241prime-)
)
:effect
(and
(Flag290-)
(Flag290prime-)
)

)
(:action Flag290Action-27
:parameters ()
:precondition
(and
(Flag242-)
(Flag242prime-)
)
:effect
(and
(Flag290-)
(Flag290prime-)
)

)
(:action Flag290Action-28
:parameters ()
:precondition
(and
(Flag267-)
(Flag267prime-)
)
:effect
(and
(Flag290-)
(Flag290prime-)
)

)
(:action Flag290Action-29
:parameters ()
:precondition
(and
(Flag243-)
(Flag243prime-)
)
:effect
(and
(Flag290-)
(Flag290prime-)
)

)
(:action Flag290Action-30
:parameters ()
:precondition
(and
(Flag244-)
(Flag244prime-)
)
:effect
(and
(Flag290-)
(Flag290prime-)
)

)
(:action Flag290Action-31
:parameters ()
:precondition
(and
(Flag252-)
(Flag252prime-)
)
:effect
(and
(Flag290-)
(Flag290prime-)
)

)
(:action Flag290Action-32
:parameters ()
:precondition
(and
(Flag245-)
(Flag245prime-)
)
:effect
(and
(Flag290-)
(Flag290prime-)
)

)
(:action Flag290Action-33
:parameters ()
:precondition
(and
(Flag268-)
(Flag268prime-)
)
:effect
(and
(Flag290-)
(Flag290prime-)
)

)
(:action Flag290Action-34
:parameters ()
:precondition
(and
(Flag269-)
(Flag269prime-)
)
:effect
(and
(Flag290-)
(Flag290prime-)
)

)
(:action Flag290Action-35
:parameters ()
:precondition
(and
(Flag285-)
(Flag285prime-)
)
:effect
(and
(Flag290-)
(Flag290prime-)
)

)
(:action Flag290Action-36
:parameters ()
:precondition
(and
(Flag247-)
(Flag247prime-)
)
:effect
(and
(Flag290-)
(Flag290prime-)
)

)
(:action Flag290Action-37
:parameters ()
:precondition
(and
(Flag248-)
(Flag248prime-)
)
:effect
(and
(Flag290-)
(Flag290prime-)
)

)
(:action Flag290Action-38
:parameters ()
:precondition
(and
(Flag249-)
(Flag249prime-)
)
:effect
(and
(Flag290-)
(Flag290prime-)
)

)
(:action Flag290Action-39
:parameters ()
:precondition
(and
(Flag286-)
(Flag286prime-)
)
:effect
(and
(Flag290-)
(Flag290prime-)
)

)
(:action Flag290Action-40
:parameters ()
:precondition
(and
(Flag251-)
(Flag251prime-)
)
:effect
(and
(Flag290-)
(Flag290prime-)
)

)
(:action Flag290Action-41
:parameters ()
:precondition
(and
(Flag287-)
(Flag287prime-)
)
:effect
(and
(Flag290-)
(Flag290prime-)
)

)
(:action Flag290Action-42
:parameters ()
:precondition
(and
(Flag271-)
(Flag271prime-)
)
:effect
(and
(Flag290-)
(Flag290prime-)
)

)
(:action Flag290Action-43
:parameters ()
:precondition
(and
(Flag288-)
(Flag288prime-)
)
:effect
(and
(Flag290-)
(Flag290prime-)
)

)
(:action Flag290Action-44
:parameters ()
:precondition
(and
(Flag289-)
(Flag289prime-)
)
:effect
(and
(Flag290-)
(Flag290prime-)
)

)
(:action Flag290Action-45
:parameters ()
:precondition
(and
(Flag272-)
(Flag272prime-)
)
:effect
(and
(Flag290-)
(Flag290prime-)
)

)
(:action Flag290Action-46
:parameters ()
:precondition
(and
(Flag238-)
(Flag238prime-)
)
:effect
(and
(Flag290-)
(Flag290prime-)
)

)
(:action Flag290Action-47
:parameters ()
:precondition
(and
(Flag253-)
(Flag253prime-)
)
:effect
(and
(Flag290-)
(Flag290prime-)
)

)
(:action Flag290Action-48
:parameters ()
:precondition
(and
(Flag273-)
(Flag273prime-)
)
:effect
(and
(Flag290-)
(Flag290prime-)
)

)
(:action Flag290Action-49
:parameters ()
:precondition
(and
(Flag254-)
(Flag254prime-)
)
:effect
(and
(Flag290-)
(Flag290prime-)
)

)
(:action Flag290Action-50
:parameters ()
:precondition
(and
(Flag255-)
(Flag255prime-)
)
:effect
(and
(Flag290-)
(Flag290prime-)
)

)
(:action Flag290Action-51
:parameters ()
:precondition
(and
(Flag256-)
(Flag256prime-)
)
:effect
(and
(Flag290-)
(Flag290prime-)
)

)
(:action Flag290Action-52
:parameters ()
:precondition
(and
(Flag257-)
(Flag257prime-)
)
:effect
(and
(Flag290-)
(Flag290prime-)
)

)
(:action Prim290Action
:parameters ()
:precondition
(and
(not (Flag262-))
(Flag262prime-)
(not (Flag259-))
(Flag259prime-)
(not (Flag275-))
(Flag275prime-)
(not (Flag276-))
(Flag276prime-)
(not (Flag277-))
(Flag277prime-)
(not (Flag260-))
(Flag260prime-)
(not (Flag261-))
(Flag261prime-)
(not (Flag229-))
(Flag229prime-)
(not (Flag246-))
(Flag246prime-)
(not (Flag231-))
(Flag231prime-)
(not (Flag263-))
(Flag263prime-)
(not (Flag232-))
(Flag232prime-)
(not (Flag278-))
(Flag278prime-)
(not (Flag233-))
(Flag233prime-)
(not (Flag234-))
(Flag234prime-)
(not (Flag279-))
(Flag279prime-)
(not (Flag236-))
(Flag236prime-)
(not (Flag280-))
(Flag280prime-)
(not (Flag250-))
(Flag250prime-)
(not (Flag264-))
(Flag264prime-)
(not (Flag281-))
(Flag281prime-)
(not (Flag237-))
(Flag237prime-)
(not (Flag282-))
(Flag282prime-)
(not (Flag283-))
(Flag283prime-)
(not (Flag284-))
(Flag284prime-)
(not (Flag239-))
(Flag239prime-)
(not (Flag241-))
(Flag241prime-)
(not (Flag242-))
(Flag242prime-)
(not (Flag267-))
(Flag267prime-)
(not (Flag243-))
(Flag243prime-)
(not (Flag244-))
(Flag244prime-)
(not (Flag252-))
(Flag252prime-)
(not (Flag245-))
(Flag245prime-)
(not (Flag268-))
(Flag268prime-)
(not (Flag269-))
(Flag269prime-)
(not (Flag285-))
(Flag285prime-)
(not (Flag247-))
(Flag247prime-)
(not (Flag248-))
(Flag248prime-)
(not (Flag249-))
(Flag249prime-)
(not (Flag286-))
(Flag286prime-)
(not (Flag251-))
(Flag251prime-)
(not (Flag287-))
(Flag287prime-)
(not (Flag271-))
(Flag271prime-)
(not (Flag288-))
(Flag288prime-)
(not (Flag289-))
(Flag289prime-)
(not (Flag272-))
(Flag272prime-)
(not (Flag238-))
(Flag238prime-)
(not (Flag253-))
(Flag253prime-)
(not (Flag273-))
(Flag273prime-)
(not (Flag254-))
(Flag254prime-)
(not (Flag255-))
(Flag255prime-)
(not (Flag256-))
(Flag256prime-)
(not (Flag257-))
(Flag257prime-)
)
:effect
(and
(Flag290prime-)
(not (Flag290-))
)

)
(:action Flag303Action-0
:parameters ()
:precondition
(and
(Flag262-)
(Flag262prime-)
)
:effect
(and
(Flag303-)
(Flag303prime-)
)

)
(:action Flag303Action-1
:parameters ()
:precondition
(and
(Flag291-)
(Flag291prime-)
)
:effect
(and
(Flag303-)
(Flag303prime-)
)

)
(:action Flag303Action-2
:parameters ()
:precondition
(and
(Flag259-)
(Flag259prime-)
)
:effect
(and
(Flag303-)
(Flag303prime-)
)

)
(:action Flag303Action-3
:parameters ()
:precondition
(and
(Flag275-)
(Flag275prime-)
)
:effect
(and
(Flag303-)
(Flag303prime-)
)

)
(:action Flag303Action-4
:parameters ()
:precondition
(and
(Flag281-)
(Flag281prime-)
)
:effect
(and
(Flag303-)
(Flag303prime-)
)

)
(:action Flag303Action-5
:parameters ()
:precondition
(and
(Flag292-)
(Flag292prime-)
)
:effect
(and
(Flag303-)
(Flag303prime-)
)

)
(:action Flag303Action-6
:parameters ()
:precondition
(and
(Flag260-)
(Flag260prime-)
)
:effect
(and
(Flag303-)
(Flag303prime-)
)

)
(:action Flag303Action-7
:parameters ()
:precondition
(and
(Flag293-)
(Flag293prime-)
)
:effect
(and
(Flag303-)
(Flag303prime-)
)

)
(:action Flag303Action-8
:parameters ()
:precondition
(and
(Flag261-)
(Flag261prime-)
)
:effect
(and
(Flag303-)
(Flag303prime-)
)

)
(:action Flag303Action-9
:parameters ()
:precondition
(and
(Flag229-)
(Flag229prime-)
)
:effect
(and
(Flag303-)
(Flag303prime-)
)

)
(:action Flag303Action-10
:parameters ()
:precondition
(and
(Flag231-)
(Flag231prime-)
)
:effect
(and
(Flag303-)
(Flag303prime-)
)

)
(:action Flag303Action-11
:parameters ()
:precondition
(and
(Flag278-)
(Flag278prime-)
)
:effect
(and
(Flag303-)
(Flag303prime-)
)

)
(:action Flag303Action-12
:parameters ()
:precondition
(and
(Flag232-)
(Flag232prime-)
)
:effect
(and
(Flag303-)
(Flag303prime-)
)

)
(:action Flag303Action-13
:parameters ()
:precondition
(and
(Flag286-)
(Flag286prime-)
)
:effect
(and
(Flag303-)
(Flag303prime-)
)

)
(:action Flag303Action-14
:parameters ()
:precondition
(and
(Flag233-)
(Flag233prime-)
)
:effect
(and
(Flag303-)
(Flag303prime-)
)

)
(:action Flag303Action-15
:parameters ()
:precondition
(and
(Flag234-)
(Flag234prime-)
)
:effect
(and
(Flag303-)
(Flag303prime-)
)

)
(:action Flag303Action-16
:parameters ()
:precondition
(and
(Flag254-)
(Flag254prime-)
)
:effect
(and
(Flag303-)
(Flag303prime-)
)

)
(:action Flag303Action-17
:parameters ()
:precondition
(and
(Flag279-)
(Flag279prime-)
)
:effect
(and
(Flag303-)
(Flag303prime-)
)

)
(:action Flag303Action-18
:parameters ()
:precondition
(and
(Flag236-)
(Flag236prime-)
)
:effect
(and
(Flag303-)
(Flag303prime-)
)

)
(:action Flag303Action-19
:parameters ()
:precondition
(and
(Flag294-)
(Flag294prime-)
)
:effect
(and
(Flag303-)
(Flag303prime-)
)

)
(:action Flag303Action-20
:parameters ()
:precondition
(and
(Flag280-)
(Flag280prime-)
)
:effect
(and
(Flag303-)
(Flag303prime-)
)

)
(:action Flag303Action-21
:parameters ()
:precondition
(and
(Flag295-)
(Flag295prime-)
)
:effect
(and
(Flag303-)
(Flag303prime-)
)

)
(:action Flag303Action-22
:parameters ()
:precondition
(and
(Flag264-)
(Flag264prime-)
)
:effect
(and
(Flag303-)
(Flag303prime-)
)

)
(:action Flag303Action-23
:parameters ()
:precondition
(and
(Flag296-)
(Flag296prime-)
)
:effect
(and
(Flag303-)
(Flag303prime-)
)

)
(:action Flag303Action-24
:parameters ()
:precondition
(and
(Flag237-)
(Flag237prime-)
)
:effect
(and
(Flag303-)
(Flag303prime-)
)

)
(:action Flag303Action-25
:parameters ()
:precondition
(and
(Flag282-)
(Flag282prime-)
)
:effect
(and
(Flag303-)
(Flag303prime-)
)

)
(:action Flag303Action-26
:parameters ()
:precondition
(and
(Flag283-)
(Flag283prime-)
)
:effect
(and
(Flag303-)
(Flag303prime-)
)

)
(:action Flag303Action-27
:parameters ()
:precondition
(and
(Flag263-)
(Flag263prime-)
)
:effect
(and
(Flag303-)
(Flag303prime-)
)

)
(:action Flag303Action-28
:parameters ()
:precondition
(and
(Flag239-)
(Flag239prime-)
)
:effect
(and
(Flag303-)
(Flag303prime-)
)

)
(:action Flag303Action-29
:parameters ()
:precondition
(and
(Flag241-)
(Flag241prime-)
)
:effect
(and
(Flag303-)
(Flag303prime-)
)

)
(:action Flag303Action-30
:parameters ()
:precondition
(and
(Flag242-)
(Flag242prime-)
)
:effect
(and
(Flag303-)
(Flag303prime-)
)

)
(:action Flag303Action-31
:parameters ()
:precondition
(and
(Flag267-)
(Flag267prime-)
)
:effect
(and
(Flag303-)
(Flag303prime-)
)

)
(:action Flag303Action-32
:parameters ()
:precondition
(and
(Flag243-)
(Flag243prime-)
)
:effect
(and
(Flag303-)
(Flag303prime-)
)

)
(:action Flag303Action-33
:parameters ()
:precondition
(and
(Flag297-)
(Flag297prime-)
)
:effect
(and
(Flag303-)
(Flag303prime-)
)

)
(:action Flag303Action-34
:parameters ()
:precondition
(and
(Flag268-)
(Flag268prime-)
)
:effect
(and
(Flag303-)
(Flag303prime-)
)

)
(:action Flag303Action-35
:parameters ()
:precondition
(and
(Flag298-)
(Flag298prime-)
)
:effect
(and
(Flag303-)
(Flag303prime-)
)

)
(:action Flag303Action-36
:parameters ()
:precondition
(and
(Flag252-)
(Flag252prime-)
)
:effect
(and
(Flag303-)
(Flag303prime-)
)

)
(:action Flag303Action-37
:parameters ()
:precondition
(and
(Flag245-)
(Flag245prime-)
)
:effect
(and
(Flag303-)
(Flag303prime-)
)

)
(:action Flag303Action-38
:parameters ()
:precondition
(and
(Flag244-)
(Flag244prime-)
)
:effect
(and
(Flag303-)
(Flag303prime-)
)

)
(:action Flag303Action-39
:parameters ()
:precondition
(and
(Flag246-)
(Flag246prime-)
)
:effect
(and
(Flag303-)
(Flag303prime-)
)

)
(:action Flag303Action-40
:parameters ()
:precondition
(and
(Flag285-)
(Flag285prime-)
)
:effect
(and
(Flag303-)
(Flag303prime-)
)

)
(:action Flag303Action-41
:parameters ()
:precondition
(and
(Flag277-)
(Flag277prime-)
)
:effect
(and
(Flag303-)
(Flag303prime-)
)

)
(:action Flag303Action-42
:parameters ()
:precondition
(and
(Flag248-)
(Flag248prime-)
)
:effect
(and
(Flag303-)
(Flag303prime-)
)

)
(:action Flag303Action-43
:parameters ()
:precondition
(and
(Flag249-)
(Flag249prime-)
)
:effect
(and
(Flag303-)
(Flag303prime-)
)

)
(:action Flag303Action-44
:parameters ()
:precondition
(and
(Flag250-)
(Flag250prime-)
)
:effect
(and
(Flag303-)
(Flag303prime-)
)

)
(:action Flag303Action-45
:parameters ()
:precondition
(and
(Flag251-)
(Flag251prime-)
)
:effect
(and
(Flag303-)
(Flag303prime-)
)

)
(:action Flag303Action-46
:parameters ()
:precondition
(and
(Flag299-)
(Flag299prime-)
)
:effect
(and
(Flag303-)
(Flag303prime-)
)

)
(:action Flag303Action-47
:parameters ()
:precondition
(and
(Flag287-)
(Flag287prime-)
)
:effect
(and
(Flag303-)
(Flag303prime-)
)

)
(:action Flag303Action-48
:parameters ()
:precondition
(and
(Flag271-)
(Flag271prime-)
)
:effect
(and
(Flag303-)
(Flag303prime-)
)

)
(:action Flag303Action-49
:parameters ()
:precondition
(and
(Flag288-)
(Flag288prime-)
)
:effect
(and
(Flag303-)
(Flag303prime-)
)

)
(:action Flag303Action-50
:parameters ()
:precondition
(and
(Flag300-)
(Flag300prime-)
)
:effect
(and
(Flag303-)
(Flag303prime-)
)

)
(:action Flag303Action-51
:parameters ()
:precondition
(and
(Flag289-)
(Flag289prime-)
)
:effect
(and
(Flag303-)
(Flag303prime-)
)

)
(:action Flag303Action-52
:parameters ()
:precondition
(and
(Flag272-)
(Flag272prime-)
)
:effect
(and
(Flag303-)
(Flag303prime-)
)

)
(:action Flag303Action-53
:parameters ()
:precondition
(and
(Flag238-)
(Flag238prime-)
)
:effect
(and
(Flag303-)
(Flag303prime-)
)

)
(:action Flag303Action-54
:parameters ()
:precondition
(and
(Flag253-)
(Flag253prime-)
)
:effect
(and
(Flag303-)
(Flag303prime-)
)

)
(:action Flag303Action-55
:parameters ()
:precondition
(and
(Flag273-)
(Flag273prime-)
)
:effect
(and
(Flag303-)
(Flag303prime-)
)

)
(:action Flag303Action-56
:parameters ()
:precondition
(and
(Flag301-)
(Flag301prime-)
)
:effect
(and
(Flag303-)
(Flag303prime-)
)

)
(:action Flag303Action-57
:parameters ()
:precondition
(and
(Flag255-)
(Flag255prime-)
)
:effect
(and
(Flag303-)
(Flag303prime-)
)

)
(:action Flag303Action-58
:parameters ()
:precondition
(and
(Flag256-)
(Flag256prime-)
)
:effect
(and
(Flag303-)
(Flag303prime-)
)

)
(:action Flag303Action-59
:parameters ()
:precondition
(and
(Flag269-)
(Flag269prime-)
)
:effect
(and
(Flag303-)
(Flag303prime-)
)

)
(:action Flag303Action-60
:parameters ()
:precondition
(and
(Flag302-)
(Flag302prime-)
)
:effect
(and
(Flag303-)
(Flag303prime-)
)

)
(:action Prim303Action
:parameters ()
:precondition
(and
(not (Flag262-))
(Flag262prime-)
(not (Flag291-))
(Flag291prime-)
(not (Flag259-))
(Flag259prime-)
(not (Flag275-))
(Flag275prime-)
(not (Flag281-))
(Flag281prime-)
(not (Flag292-))
(Flag292prime-)
(not (Flag260-))
(Flag260prime-)
(not (Flag293-))
(Flag293prime-)
(not (Flag261-))
(Flag261prime-)
(not (Flag229-))
(Flag229prime-)
(not (Flag231-))
(Flag231prime-)
(not (Flag278-))
(Flag278prime-)
(not (Flag232-))
(Flag232prime-)
(not (Flag286-))
(Flag286prime-)
(not (Flag233-))
(Flag233prime-)
(not (Flag234-))
(Flag234prime-)
(not (Flag254-))
(Flag254prime-)
(not (Flag279-))
(Flag279prime-)
(not (Flag236-))
(Flag236prime-)
(not (Flag294-))
(Flag294prime-)
(not (Flag280-))
(Flag280prime-)
(not (Flag295-))
(Flag295prime-)
(not (Flag264-))
(Flag264prime-)
(not (Flag296-))
(Flag296prime-)
(not (Flag237-))
(Flag237prime-)
(not (Flag282-))
(Flag282prime-)
(not (Flag283-))
(Flag283prime-)
(not (Flag263-))
(Flag263prime-)
(not (Flag239-))
(Flag239prime-)
(not (Flag241-))
(Flag241prime-)
(not (Flag242-))
(Flag242prime-)
(not (Flag267-))
(Flag267prime-)
(not (Flag243-))
(Flag243prime-)
(not (Flag297-))
(Flag297prime-)
(not (Flag268-))
(Flag268prime-)
(not (Flag298-))
(Flag298prime-)
(not (Flag252-))
(Flag252prime-)
(not (Flag245-))
(Flag245prime-)
(not (Flag244-))
(Flag244prime-)
(not (Flag246-))
(Flag246prime-)
(not (Flag285-))
(Flag285prime-)
(not (Flag277-))
(Flag277prime-)
(not (Flag248-))
(Flag248prime-)
(not (Flag249-))
(Flag249prime-)
(not (Flag250-))
(Flag250prime-)
(not (Flag251-))
(Flag251prime-)
(not (Flag299-))
(Flag299prime-)
(not (Flag287-))
(Flag287prime-)
(not (Flag271-))
(Flag271prime-)
(not (Flag288-))
(Flag288prime-)
(not (Flag300-))
(Flag300prime-)
(not (Flag289-))
(Flag289prime-)
(not (Flag272-))
(Flag272prime-)
(not (Flag238-))
(Flag238prime-)
(not (Flag253-))
(Flag253prime-)
(not (Flag273-))
(Flag273prime-)
(not (Flag301-))
(Flag301prime-)
(not (Flag255-))
(Flag255prime-)
(not (Flag256-))
(Flag256prime-)
(not (Flag269-))
(Flag269prime-)
(not (Flag302-))
(Flag302prime-)
)
:effect
(and
(Flag303prime-)
(not (Flag303-))
)

)
(:action Flag317Action-0
:parameters ()
:precondition
(and
(Flag289-)
(Flag289prime-)
)
:effect
(and
(Flag317-)
(Flag317prime-)
)

)
(:action Flag317Action-1
:parameters ()
:precondition
(and
(Flag262-)
(Flag262prime-)
)
:effect
(and
(Flag317-)
(Flag317prime-)
)

)
(:action Flag317Action-2
:parameters ()
:precondition
(and
(Flag259-)
(Flag259prime-)
)
:effect
(and
(Flag317-)
(Flag317prime-)
)

)
(:action Flag317Action-3
:parameters ()
:precondition
(and
(Flag254-)
(Flag254prime-)
)
:effect
(and
(Flag317-)
(Flag317prime-)
)

)
(:action Flag317Action-4
:parameters ()
:precondition
(and
(Flag304-)
(Flag304prime-)
)
:effect
(and
(Flag317-)
(Flag317prime-)
)

)
(:action Flag317Action-5
:parameters ()
:precondition
(and
(Flag275-)
(Flag275prime-)
)
:effect
(and
(Flag317-)
(Flag317prime-)
)

)
(:action Flag317Action-6
:parameters ()
:precondition
(and
(Flag305-)
(Flag305prime-)
)
:effect
(and
(Flag317-)
(Flag317prime-)
)

)
(:action Flag317Action-7
:parameters ()
:precondition
(and
(Flag292-)
(Flag292prime-)
)
:effect
(and
(Flag317-)
(Flag317prime-)
)

)
(:action Flag317Action-8
:parameters ()
:precondition
(and
(Flag306-)
(Flag306prime-)
)
:effect
(and
(Flag317-)
(Flag317prime-)
)

)
(:action Flag317Action-9
:parameters ()
:precondition
(and
(Flag260-)
(Flag260prime-)
)
:effect
(and
(Flag317-)
(Flag317prime-)
)

)
(:action Flag317Action-10
:parameters ()
:precondition
(and
(Flag293-)
(Flag293prime-)
)
:effect
(and
(Flag317-)
(Flag317prime-)
)

)
(:action Flag317Action-11
:parameters ()
:precondition
(and
(Flag261-)
(Flag261prime-)
)
:effect
(and
(Flag317-)
(Flag317prime-)
)

)
(:action Flag317Action-12
:parameters ()
:precondition
(and
(Flag307-)
(Flag307prime-)
)
:effect
(and
(Flag317-)
(Flag317prime-)
)

)
(:action Flag317Action-13
:parameters ()
:precondition
(and
(Flag308-)
(Flag308prime-)
)
:effect
(and
(Flag317-)
(Flag317prime-)
)

)
(:action Flag317Action-14
:parameters ()
:precondition
(and
(Flag229-)
(Flag229prime-)
)
:effect
(and
(Flag317-)
(Flag317prime-)
)

)
(:action Flag317Action-15
:parameters ()
:precondition
(and
(Flag309-)
(Flag309prime-)
)
:effect
(and
(Flag317-)
(Flag317prime-)
)

)
(:action Flag317Action-16
:parameters ()
:precondition
(and
(Flag231-)
(Flag231prime-)
)
:effect
(and
(Flag317-)
(Flag317prime-)
)

)
(:action Flag317Action-17
:parameters ()
:precondition
(and
(Flag263-)
(Flag263prime-)
)
:effect
(and
(Flag317-)
(Flag317prime-)
)

)
(:action Flag317Action-18
:parameters ()
:precondition
(and
(Flag232-)
(Flag232prime-)
)
:effect
(and
(Flag317-)
(Flag317prime-)
)

)
(:action Flag317Action-19
:parameters ()
:precondition
(and
(Flag286-)
(Flag286prime-)
)
:effect
(and
(Flag317-)
(Flag317prime-)
)

)
(:action Flag317Action-20
:parameters ()
:precondition
(and
(Flag248-)
(Flag248prime-)
)
:effect
(and
(Flag317-)
(Flag317prime-)
)

)
(:action Flag317Action-21
:parameters ()
:precondition
(and
(Flag233-)
(Flag233prime-)
)
:effect
(and
(Flag317-)
(Flag317prime-)
)

)
(:action Flag317Action-22
:parameters ()
:precondition
(and
(Flag241-)
(Flag241prime-)
)
:effect
(and
(Flag317-)
(Flag317prime-)
)

)
(:action Flag317Action-23
:parameters ()
:precondition
(and
(Flag234-)
(Flag234prime-)
)
:effect
(and
(Flag317-)
(Flag317prime-)
)

)
(:action Flag317Action-24
:parameters ()
:precondition
(and
(Flag300-)
(Flag300prime-)
)
:effect
(and
(Flag317-)
(Flag317prime-)
)

)
(:action Flag317Action-25
:parameters ()
:precondition
(and
(Flag279-)
(Flag279prime-)
)
:effect
(and
(Flag317-)
(Flag317prime-)
)

)
(:action Flag317Action-26
:parameters ()
:precondition
(and
(Flag236-)
(Flag236prime-)
)
:effect
(and
(Flag317-)
(Flag317prime-)
)

)
(:action Flag317Action-27
:parameters ()
:precondition
(and
(Flag310-)
(Flag310prime-)
)
:effect
(and
(Flag317-)
(Flag317prime-)
)

)
(:action Flag317Action-28
:parameters ()
:precondition
(and
(Flag311-)
(Flag311prime-)
)
:effect
(and
(Flag317-)
(Flag317prime-)
)

)
(:action Flag317Action-29
:parameters ()
:precondition
(and
(Flag294-)
(Flag294prime-)
)
:effect
(and
(Flag317-)
(Flag317prime-)
)

)
(:action Flag317Action-30
:parameters ()
:precondition
(and
(Flag280-)
(Flag280prime-)
)
:effect
(and
(Flag317-)
(Flag317prime-)
)

)
(:action Flag317Action-31
:parameters ()
:precondition
(and
(Flag295-)
(Flag295prime-)
)
:effect
(and
(Flag317-)
(Flag317prime-)
)

)
(:action Flag317Action-32
:parameters ()
:precondition
(and
(Flag264-)
(Flag264prime-)
)
:effect
(and
(Flag317-)
(Flag317prime-)
)

)
(:action Flag317Action-33
:parameters ()
:precondition
(and
(Flag281-)
(Flag281prime-)
)
:effect
(and
(Flag317-)
(Flag317prime-)
)

)
(:action Flag317Action-34
:parameters ()
:precondition
(and
(Flag282-)
(Flag282prime-)
)
:effect
(and
(Flag317-)
(Flag317prime-)
)

)
(:action Flag317Action-35
:parameters ()
:precondition
(and
(Flag283-)
(Flag283prime-)
)
:effect
(and
(Flag317-)
(Flag317prime-)
)

)
(:action Flag317Action-36
:parameters ()
:precondition
(and
(Flag239-)
(Flag239prime-)
)
:effect
(and
(Flag317-)
(Flag317prime-)
)

)
(:action Flag317Action-37
:parameters ()
:precondition
(and
(Flag243-)
(Flag243prime-)
)
:effect
(and
(Flag317-)
(Flag317prime-)
)

)
(:action Flag317Action-38
:parameters ()
:precondition
(and
(Flag242-)
(Flag242prime-)
)
:effect
(and
(Flag317-)
(Flag317prime-)
)

)
(:action Flag317Action-39
:parameters ()
:precondition
(and
(Flag267-)
(Flag267prime-)
)
:effect
(and
(Flag317-)
(Flag317prime-)
)

)
(:action Flag317Action-40
:parameters ()
:precondition
(and
(Flag312-)
(Flag312prime-)
)
:effect
(and
(Flag317-)
(Flag317prime-)
)

)
(:action Flag317Action-41
:parameters ()
:precondition
(and
(Flag244-)
(Flag244prime-)
)
:effect
(and
(Flag317-)
(Flag317prime-)
)

)
(:action Flag317Action-42
:parameters ()
:precondition
(and
(Flag298-)
(Flag298prime-)
)
:effect
(and
(Flag317-)
(Flag317prime-)
)

)
(:action Flag317Action-43
:parameters ()
:precondition
(and
(Flag252-)
(Flag252prime-)
)
:effect
(and
(Flag317-)
(Flag317prime-)
)

)
(:action Flag317Action-44
:parameters ()
:precondition
(and
(Flag245-)
(Flag245prime-)
)
:effect
(and
(Flag317-)
(Flag317prime-)
)

)
(:action Flag317Action-45
:parameters ()
:precondition
(and
(Flag246-)
(Flag246prime-)
)
:effect
(and
(Flag317-)
(Flag317prime-)
)

)
(:action Flag317Action-46
:parameters ()
:precondition
(and
(Flag285-)
(Flag285prime-)
)
:effect
(and
(Flag317-)
(Flag317prime-)
)

)
(:action Flag317Action-47
:parameters ()
:precondition
(and
(Flag313-)
(Flag313prime-)
)
:effect
(and
(Flag317-)
(Flag317prime-)
)

)
(:action Flag317Action-48
:parameters ()
:precondition
(and
(Flag277-)
(Flag277prime-)
)
:effect
(and
(Flag317-)
(Flag317prime-)
)

)
(:action Flag317Action-49
:parameters ()
:precondition
(and
(Flag314-)
(Flag314prime-)
)
:effect
(and
(Flag317-)
(Flag317prime-)
)

)
(:action Flag317Action-50
:parameters ()
:precondition
(and
(Flag249-)
(Flag249prime-)
)
:effect
(and
(Flag317-)
(Flag317prime-)
)

)
(:action Flag317Action-51
:parameters ()
:precondition
(and
(Flag250-)
(Flag250prime-)
)
:effect
(and
(Flag317-)
(Flag317prime-)
)

)
(:action Flag317Action-52
:parameters ()
:precondition
(and
(Flag299-)
(Flag299prime-)
)
:effect
(and
(Flag317-)
(Flag317prime-)
)

)
(:action Flag317Action-53
:parameters ()
:precondition
(and
(Flag287-)
(Flag287prime-)
)
:effect
(and
(Flag317-)
(Flag317prime-)
)

)
(:action Flag317Action-54
:parameters ()
:precondition
(and
(Flag271-)
(Flag271prime-)
)
:effect
(and
(Flag317-)
(Flag317prime-)
)

)
(:action Flag317Action-55
:parameters ()
:precondition
(and
(Flag288-)
(Flag288prime-)
)
:effect
(and
(Flag317-)
(Flag317prime-)
)

)
(:action Flag317Action-56
:parameters ()
:precondition
(and
(Flag315-)
(Flag315prime-)
)
:effect
(and
(Flag317-)
(Flag317prime-)
)

)
(:action Flag317Action-57
:parameters ()
:precondition
(and
(Flag272-)
(Flag272prime-)
)
:effect
(and
(Flag317-)
(Flag317prime-)
)

)
(:action Flag317Action-58
:parameters ()
:precondition
(and
(Flag238-)
(Flag238prime-)
)
:effect
(and
(Flag317-)
(Flag317prime-)
)

)
(:action Flag317Action-59
:parameters ()
:precondition
(and
(Flag316-)
(Flag316prime-)
)
:effect
(and
(Flag317-)
(Flag317prime-)
)

)
(:action Flag317Action-60
:parameters ()
:precondition
(and
(Flag253-)
(Flag253prime-)
)
:effect
(and
(Flag317-)
(Flag317prime-)
)

)
(:action Flag317Action-61
:parameters ()
:precondition
(and
(Flag296-)
(Flag296prime-)
)
:effect
(and
(Flag317-)
(Flag317prime-)
)

)
(:action Flag317Action-62
:parameters ()
:precondition
(and
(Flag273-)
(Flag273prime-)
)
:effect
(and
(Flag317-)
(Flag317prime-)
)

)
(:action Flag317Action-63
:parameters ()
:precondition
(and
(Flag301-)
(Flag301prime-)
)
:effect
(and
(Flag317-)
(Flag317prime-)
)

)
(:action Flag317Action-64
:parameters ()
:precondition
(and
(Flag255-)
(Flag255prime-)
)
:effect
(and
(Flag317-)
(Flag317prime-)
)

)
(:action Flag317Action-65
:parameters ()
:precondition
(and
(Flag256-)
(Flag256prime-)
)
:effect
(and
(Flag317-)
(Flag317prime-)
)

)
(:action Flag317Action-66
:parameters ()
:precondition
(and
(Flag269-)
(Flag269prime-)
)
:effect
(and
(Flag317-)
(Flag317prime-)
)

)
(:action Flag317Action-67
:parameters ()
:precondition
(and
(Flag302-)
(Flag302prime-)
)
:effect
(and
(Flag317-)
(Flag317prime-)
)

)
(:action Prim317Action
:parameters ()
:precondition
(and
(not (Flag289-))
(Flag289prime-)
(not (Flag262-))
(Flag262prime-)
(not (Flag259-))
(Flag259prime-)
(not (Flag254-))
(Flag254prime-)
(not (Flag304-))
(Flag304prime-)
(not (Flag275-))
(Flag275prime-)
(not (Flag305-))
(Flag305prime-)
(not (Flag292-))
(Flag292prime-)
(not (Flag306-))
(Flag306prime-)
(not (Flag260-))
(Flag260prime-)
(not (Flag293-))
(Flag293prime-)
(not (Flag261-))
(Flag261prime-)
(not (Flag307-))
(Flag307prime-)
(not (Flag308-))
(Flag308prime-)
(not (Flag229-))
(Flag229prime-)
(not (Flag309-))
(Flag309prime-)
(not (Flag231-))
(Flag231prime-)
(not (Flag263-))
(Flag263prime-)
(not (Flag232-))
(Flag232prime-)
(not (Flag286-))
(Flag286prime-)
(not (Flag248-))
(Flag248prime-)
(not (Flag233-))
(Flag233prime-)
(not (Flag241-))
(Flag241prime-)
(not (Flag234-))
(Flag234prime-)
(not (Flag300-))
(Flag300prime-)
(not (Flag279-))
(Flag279prime-)
(not (Flag236-))
(Flag236prime-)
(not (Flag310-))
(Flag310prime-)
(not (Flag311-))
(Flag311prime-)
(not (Flag294-))
(Flag294prime-)
(not (Flag280-))
(Flag280prime-)
(not (Flag295-))
(Flag295prime-)
(not (Flag264-))
(Flag264prime-)
(not (Flag281-))
(Flag281prime-)
(not (Flag282-))
(Flag282prime-)
(not (Flag283-))
(Flag283prime-)
(not (Flag239-))
(Flag239prime-)
(not (Flag243-))
(Flag243prime-)
(not (Flag242-))
(Flag242prime-)
(not (Flag267-))
(Flag267prime-)
(not (Flag312-))
(Flag312prime-)
(not (Flag244-))
(Flag244prime-)
(not (Flag298-))
(Flag298prime-)
(not (Flag252-))
(Flag252prime-)
(not (Flag245-))
(Flag245prime-)
(not (Flag246-))
(Flag246prime-)
(not (Flag285-))
(Flag285prime-)
(not (Flag313-))
(Flag313prime-)
(not (Flag277-))
(Flag277prime-)
(not (Flag314-))
(Flag314prime-)
(not (Flag249-))
(Flag249prime-)
(not (Flag250-))
(Flag250prime-)
(not (Flag299-))
(Flag299prime-)
(not (Flag287-))
(Flag287prime-)
(not (Flag271-))
(Flag271prime-)
(not (Flag288-))
(Flag288prime-)
(not (Flag315-))
(Flag315prime-)
(not (Flag272-))
(Flag272prime-)
(not (Flag238-))
(Flag238prime-)
(not (Flag316-))
(Flag316prime-)
(not (Flag253-))
(Flag253prime-)
(not (Flag296-))
(Flag296prime-)
(not (Flag273-))
(Flag273prime-)
(not (Flag301-))
(Flag301prime-)
(not (Flag255-))
(Flag255prime-)
(not (Flag256-))
(Flag256prime-)
(not (Flag269-))
(Flag269prime-)
(not (Flag302-))
(Flag302prime-)
)
:effect
(and
(Flag317prime-)
(not (Flag317-))
)

)
(:action Flag330Action-0
:parameters ()
:precondition
(and
(Flag253-)
(Flag253prime-)
)
:effect
(and
(Flag330-)
(Flag330prime-)
)

)
(:action Flag330Action-1
:parameters ()
:precondition
(and
(Flag262-)
(Flag262prime-)
)
:effect
(and
(Flag330-)
(Flag330prime-)
)

)
(:action Flag330Action-2
:parameters ()
:precondition
(and
(Flag289-)
(Flag289prime-)
)
:effect
(and
(Flag330-)
(Flag330prime-)
)

)
(:action Flag330Action-3
:parameters ()
:precondition
(and
(Flag279-)
(Flag279prime-)
)
:effect
(and
(Flag330-)
(Flag330prime-)
)

)
(:action Flag330Action-4
:parameters ()
:precondition
(and
(Flag318-)
(Flag318prime-)
)
:effect
(and
(Flag330-)
(Flag330prime-)
)

)
(:action Flag330Action-5
:parameters ()
:precondition
(and
(Flag275-)
(Flag275prime-)
)
:effect
(and
(Flag330-)
(Flag330prime-)
)

)
(:action Flag330Action-6
:parameters ()
:precondition
(and
(Flag292-)
(Flag292prime-)
)
:effect
(and
(Flag330-)
(Flag330prime-)
)

)
(:action Flag330Action-7
:parameters ()
:precondition
(and
(Flag306-)
(Flag306prime-)
)
:effect
(and
(Flag330-)
(Flag330prime-)
)

)
(:action Flag330Action-8
:parameters ()
:precondition
(and
(Flag239-)
(Flag239prime-)
)
:effect
(and
(Flag330-)
(Flag330prime-)
)

)
(:action Flag330Action-9
:parameters ()
:precondition
(and
(Flag293-)
(Flag293prime-)
)
:effect
(and
(Flag330-)
(Flag330prime-)
)

)
(:action Flag330Action-10
:parameters ()
:precondition
(and
(Flag316-)
(Flag316prime-)
)
:effect
(and
(Flag330-)
(Flag330prime-)
)

)
(:action Flag330Action-11
:parameters ()
:precondition
(and
(Flag261-)
(Flag261prime-)
)
:effect
(and
(Flag330-)
(Flag330prime-)
)

)
(:action Flag330Action-12
:parameters ()
:precondition
(and
(Flag307-)
(Flag307prime-)
)
:effect
(and
(Flag330-)
(Flag330prime-)
)

)
(:action Flag330Action-13
:parameters ()
:precondition
(and
(Flag308-)
(Flag308prime-)
)
:effect
(and
(Flag330-)
(Flag330prime-)
)

)
(:action Flag330Action-14
:parameters ()
:precondition
(and
(Flag319-)
(Flag319prime-)
)
:effect
(and
(Flag330-)
(Flag330prime-)
)

)
(:action Flag330Action-15
:parameters ()
:precondition
(and
(Flag229-)
(Flag229prime-)
)
:effect
(and
(Flag330-)
(Flag330prime-)
)

)
(:action Flag330Action-16
:parameters ()
:precondition
(and
(Flag309-)
(Flag309prime-)
)
:effect
(and
(Flag330-)
(Flag330prime-)
)

)
(:action Flag330Action-17
:parameters ()
:precondition
(and
(Flag320-)
(Flag320prime-)
)
:effect
(and
(Flag330-)
(Flag330prime-)
)

)
(:action Flag330Action-18
:parameters ()
:precondition
(and
(Flag231-)
(Flag231prime-)
)
:effect
(and
(Flag330-)
(Flag330prime-)
)

)
(:action Flag330Action-19
:parameters ()
:precondition
(and
(Flag263-)
(Flag263prime-)
)
:effect
(and
(Flag330-)
(Flag330prime-)
)

)
(:action Flag330Action-20
:parameters ()
:precondition
(and
(Flag232-)
(Flag232prime-)
)
:effect
(and
(Flag330-)
(Flag330prime-)
)

)
(:action Flag330Action-21
:parameters ()
:precondition
(and
(Flag286-)
(Flag286prime-)
)
:effect
(and
(Flag330-)
(Flag330prime-)
)

)
(:action Flag330Action-22
:parameters ()
:precondition
(and
(Flag248-)
(Flag248prime-)
)
:effect
(and
(Flag330-)
(Flag330prime-)
)

)
(:action Flag330Action-23
:parameters ()
:precondition
(and
(Flag233-)
(Flag233prime-)
)
:effect
(and
(Flag330-)
(Flag330prime-)
)

)
(:action Flag330Action-24
:parameters ()
:precondition
(and
(Flag234-)
(Flag234prime-)
)
:effect
(and
(Flag330-)
(Flag330prime-)
)

)
(:action Flag330Action-25
:parameters ()
:precondition
(and
(Flag300-)
(Flag300prime-)
)
:effect
(and
(Flag330-)
(Flag330prime-)
)

)
(:action Flag330Action-26
:parameters ()
:precondition
(and
(Flag321-)
(Flag321prime-)
)
:effect
(and
(Flag330-)
(Flag330prime-)
)

)
(:action Flag330Action-27
:parameters ()
:precondition
(and
(Flag310-)
(Flag310prime-)
)
:effect
(and
(Flag330-)
(Flag330prime-)
)

)
(:action Flag330Action-28
:parameters ()
:precondition
(and
(Flag294-)
(Flag294prime-)
)
:effect
(and
(Flag330-)
(Flag330prime-)
)

)
(:action Flag330Action-29
:parameters ()
:precondition
(and
(Flag280-)
(Flag280prime-)
)
:effect
(and
(Flag330-)
(Flag330prime-)
)

)
(:action Flag330Action-30
:parameters ()
:precondition
(and
(Flag295-)
(Flag295prime-)
)
:effect
(and
(Flag330-)
(Flag330prime-)
)

)
(:action Flag330Action-31
:parameters ()
:precondition
(and
(Flag264-)
(Flag264prime-)
)
:effect
(and
(Flag330-)
(Flag330prime-)
)

)
(:action Flag330Action-32
:parameters ()
:precondition
(and
(Flag281-)
(Flag281prime-)
)
:effect
(and
(Flag330-)
(Flag330prime-)
)

)
(:action Flag330Action-33
:parameters ()
:precondition
(and
(Flag304-)
(Flag304prime-)
)
:effect
(and
(Flag330-)
(Flag330prime-)
)

)
(:action Flag330Action-34
:parameters ()
:precondition
(and
(Flag282-)
(Flag282prime-)
)
:effect
(and
(Flag330-)
(Flag330prime-)
)

)
(:action Flag330Action-35
:parameters ()
:precondition
(and
(Flag238-)
(Flag238prime-)
)
:effect
(and
(Flag330-)
(Flag330prime-)
)

)
(:action Flag330Action-36
:parameters ()
:precondition
(and
(Flag322-)
(Flag322prime-)
)
:effect
(and
(Flag330-)
(Flag330prime-)
)

)
(:action Flag330Action-37
:parameters ()
:precondition
(and
(Flag260-)
(Flag260prime-)
)
:effect
(and
(Flag330-)
(Flag330prime-)
)

)
(:action Flag330Action-38
:parameters ()
:precondition
(and
(Flag323-)
(Flag323prime-)
)
:effect
(and
(Flag330-)
(Flag330prime-)
)

)
(:action Flag330Action-39
:parameters ()
:precondition
(and
(Flag241-)
(Flag241prime-)
)
:effect
(and
(Flag330-)
(Flag330prime-)
)

)
(:action Flag330Action-40
:parameters ()
:precondition
(and
(Flag242-)
(Flag242prime-)
)
:effect
(and
(Flag330-)
(Flag330prime-)
)

)
(:action Flag330Action-41
:parameters ()
:precondition
(and
(Flag267-)
(Flag267prime-)
)
:effect
(and
(Flag330-)
(Flag330prime-)
)

)
(:action Flag330Action-42
:parameters ()
:precondition
(and
(Flag243-)
(Flag243prime-)
)
:effect
(and
(Flag330-)
(Flag330prime-)
)

)
(:action Flag330Action-43
:parameters ()
:precondition
(and
(Flag244-)
(Flag244prime-)
)
:effect
(and
(Flag330-)
(Flag330prime-)
)

)
(:action Flag330Action-44
:parameters ()
:precondition
(and
(Flag298-)
(Flag298prime-)
)
:effect
(and
(Flag330-)
(Flag330prime-)
)

)
(:action Flag330Action-45
:parameters ()
:precondition
(and
(Flag252-)
(Flag252prime-)
)
:effect
(and
(Flag330-)
(Flag330prime-)
)

)
(:action Flag330Action-46
:parameters ()
:precondition
(and
(Flag245-)
(Flag245prime-)
)
:effect
(and
(Flag330-)
(Flag330prime-)
)

)
(:action Flag330Action-47
:parameters ()
:precondition
(and
(Flag269-)
(Flag269prime-)
)
:effect
(and
(Flag330-)
(Flag330prime-)
)

)
(:action Flag330Action-48
:parameters ()
:precondition
(and
(Flag285-)
(Flag285prime-)
)
:effect
(and
(Flag330-)
(Flag330prime-)
)

)
(:action Flag330Action-49
:parameters ()
:precondition
(and
(Flag313-)
(Flag313prime-)
)
:effect
(and
(Flag330-)
(Flag330prime-)
)

)
(:action Flag330Action-50
:parameters ()
:precondition
(and
(Flag277-)
(Flag277prime-)
)
:effect
(and
(Flag330-)
(Flag330prime-)
)

)
(:action Flag330Action-51
:parameters ()
:precondition
(and
(Flag324-)
(Flag324prime-)
)
:effect
(and
(Flag330-)
(Flag330prime-)
)

)
(:action Flag330Action-52
:parameters ()
:precondition
(and
(Flag314-)
(Flag314prime-)
)
:effect
(and
(Flag330-)
(Flag330prime-)
)

)
(:action Flag330Action-53
:parameters ()
:precondition
(and
(Flag249-)
(Flag249prime-)
)
:effect
(and
(Flag330-)
(Flag330prime-)
)

)
(:action Flag330Action-54
:parameters ()
:precondition
(and
(Flag250-)
(Flag250prime-)
)
:effect
(and
(Flag330-)
(Flag330prime-)
)

)
(:action Flag330Action-55
:parameters ()
:precondition
(and
(Flag299-)
(Flag299prime-)
)
:effect
(and
(Flag330-)
(Flag330prime-)
)

)
(:action Flag330Action-56
:parameters ()
:precondition
(and
(Flag287-)
(Flag287prime-)
)
:effect
(and
(Flag330-)
(Flag330prime-)
)

)
(:action Flag330Action-57
:parameters ()
:precondition
(and
(Flag325-)
(Flag325prime-)
)
:effect
(and
(Flag330-)
(Flag330prime-)
)

)
(:action Flag330Action-58
:parameters ()
:precondition
(and
(Flag326-)
(Flag326prime-)
)
:effect
(and
(Flag330-)
(Flag330prime-)
)

)
(:action Flag330Action-59
:parameters ()
:precondition
(and
(Flag271-)
(Flag271prime-)
)
:effect
(and
(Flag330-)
(Flag330prime-)
)

)
(:action Flag330Action-60
:parameters ()
:precondition
(and
(Flag327-)
(Flag327prime-)
)
:effect
(and
(Flag330-)
(Flag330prime-)
)

)
(:action Flag330Action-61
:parameters ()
:precondition
(and
(Flag315-)
(Flag315prime-)
)
:effect
(and
(Flag330-)
(Flag330prime-)
)

)
(:action Flag330Action-62
:parameters ()
:precondition
(and
(Flag272-)
(Flag272prime-)
)
:effect
(and
(Flag330-)
(Flag330prime-)
)

)
(:action Flag330Action-63
:parameters ()
:precondition
(and
(Flag328-)
(Flag328prime-)
)
:effect
(and
(Flag330-)
(Flag330prime-)
)

)
(:action Flag330Action-64
:parameters ()
:precondition
(and
(Flag329-)
(Flag329prime-)
)
:effect
(and
(Flag330-)
(Flag330prime-)
)

)
(:action Flag330Action-65
:parameters ()
:precondition
(and
(Flag288-)
(Flag288prime-)
)
:effect
(and
(Flag330-)
(Flag330prime-)
)

)
(:action Flag330Action-66
:parameters ()
:precondition
(and
(Flag296-)
(Flag296prime-)
)
:effect
(and
(Flag330-)
(Flag330prime-)
)

)
(:action Flag330Action-67
:parameters ()
:precondition
(and
(Flag273-)
(Flag273prime-)
)
:effect
(and
(Flag330-)
(Flag330prime-)
)

)
(:action Flag330Action-68
:parameters ()
:precondition
(and
(Flag301-)
(Flag301prime-)
)
:effect
(and
(Flag330-)
(Flag330prime-)
)

)
(:action Flag330Action-69
:parameters ()
:precondition
(and
(Flag255-)
(Flag255prime-)
)
:effect
(and
(Flag330-)
(Flag330prime-)
)

)
(:action Flag330Action-70
:parameters ()
:precondition
(and
(Flag256-)
(Flag256prime-)
)
:effect
(and
(Flag330-)
(Flag330prime-)
)

)
(:action Flag330Action-71
:parameters ()
:precondition
(and
(Flag246-)
(Flag246prime-)
)
:effect
(and
(Flag330-)
(Flag330prime-)
)

)
(:action Flag330Action-72
:parameters ()
:precondition
(and
(Flag302-)
(Flag302prime-)
)
:effect
(and
(Flag330-)
(Flag330prime-)
)

)
(:action Prim330Action
:parameters ()
:precondition
(and
(not (Flag253-))
(Flag253prime-)
(not (Flag262-))
(Flag262prime-)
(not (Flag289-))
(Flag289prime-)
(not (Flag279-))
(Flag279prime-)
(not (Flag318-))
(Flag318prime-)
(not (Flag275-))
(Flag275prime-)
(not (Flag292-))
(Flag292prime-)
(not (Flag306-))
(Flag306prime-)
(not (Flag239-))
(Flag239prime-)
(not (Flag293-))
(Flag293prime-)
(not (Flag316-))
(Flag316prime-)
(not (Flag261-))
(Flag261prime-)
(not (Flag307-))
(Flag307prime-)
(not (Flag308-))
(Flag308prime-)
(not (Flag319-))
(Flag319prime-)
(not (Flag229-))
(Flag229prime-)
(not (Flag309-))
(Flag309prime-)
(not (Flag320-))
(Flag320prime-)
(not (Flag231-))
(Flag231prime-)
(not (Flag263-))
(Flag263prime-)
(not (Flag232-))
(Flag232prime-)
(not (Flag286-))
(Flag286prime-)
(not (Flag248-))
(Flag248prime-)
(not (Flag233-))
(Flag233prime-)
(not (Flag234-))
(Flag234prime-)
(not (Flag300-))
(Flag300prime-)
(not (Flag321-))
(Flag321prime-)
(not (Flag310-))
(Flag310prime-)
(not (Flag294-))
(Flag294prime-)
(not (Flag280-))
(Flag280prime-)
(not (Flag295-))
(Flag295prime-)
(not (Flag264-))
(Flag264prime-)
(not (Flag281-))
(Flag281prime-)
(not (Flag304-))
(Flag304prime-)
(not (Flag282-))
(Flag282prime-)
(not (Flag238-))
(Flag238prime-)
(not (Flag322-))
(Flag322prime-)
(not (Flag260-))
(Flag260prime-)
(not (Flag323-))
(Flag323prime-)
(not (Flag241-))
(Flag241prime-)
(not (Flag242-))
(Flag242prime-)
(not (Flag267-))
(Flag267prime-)
(not (Flag243-))
(Flag243prime-)
(not (Flag244-))
(Flag244prime-)
(not (Flag298-))
(Flag298prime-)
(not (Flag252-))
(Flag252prime-)
(not (Flag245-))
(Flag245prime-)
(not (Flag269-))
(Flag269prime-)
(not (Flag285-))
(Flag285prime-)
(not (Flag313-))
(Flag313prime-)
(not (Flag277-))
(Flag277prime-)
(not (Flag324-))
(Flag324prime-)
(not (Flag314-))
(Flag314prime-)
(not (Flag249-))
(Flag249prime-)
(not (Flag250-))
(Flag250prime-)
(not (Flag299-))
(Flag299prime-)
(not (Flag287-))
(Flag287prime-)
(not (Flag325-))
(Flag325prime-)
(not (Flag326-))
(Flag326prime-)
(not (Flag271-))
(Flag271prime-)
(not (Flag327-))
(Flag327prime-)
(not (Flag315-))
(Flag315prime-)
(not (Flag272-))
(Flag272prime-)
(not (Flag328-))
(Flag328prime-)
(not (Flag329-))
(Flag329prime-)
(not (Flag288-))
(Flag288prime-)
(not (Flag296-))
(Flag296prime-)
(not (Flag273-))
(Flag273prime-)
(not (Flag301-))
(Flag301prime-)
(not (Flag255-))
(Flag255prime-)
(not (Flag256-))
(Flag256prime-)
(not (Flag246-))
(Flag246prime-)
(not (Flag302-))
(Flag302prime-)
)
:effect
(and
(Flag330prime-)
(not (Flag330-))
)

)
(:action Flag341Action-0
:parameters ()
:precondition
(and
(Flag253-)
(Flag253prime-)
)
:effect
(and
(Flag341-)
(Flag341prime-)
)

)
(:action Flag341Action-1
:parameters ()
:precondition
(and
(Flag262-)
(Flag262prime-)
)
:effect
(and
(Flag341-)
(Flag341prime-)
)

)
(:action Flag341Action-2
:parameters ()
:precondition
(and
(Flag289-)
(Flag289prime-)
)
:effect
(and
(Flag341-)
(Flag341prime-)
)

)
(:action Flag341Action-3
:parameters ()
:precondition
(and
(Flag279-)
(Flag279prime-)
)
:effect
(and
(Flag341-)
(Flag341prime-)
)

)
(:action Flag341Action-4
:parameters ()
:precondition
(and
(Flag252-)
(Flag252prime-)
)
:effect
(and
(Flag341-)
(Flag341prime-)
)

)
(:action Flag341Action-5
:parameters ()
:precondition
(and
(Flag304-)
(Flag304prime-)
)
:effect
(and
(Flag341-)
(Flag341prime-)
)

)
(:action Flag341Action-6
:parameters ()
:precondition
(and
(Flag275-)
(Flag275prime-)
)
:effect
(and
(Flag341-)
(Flag341prime-)
)

)
(:action Flag341Action-7
:parameters ()
:precondition
(and
(Flag306-)
(Flag306prime-)
)
:effect
(and
(Flag341-)
(Flag341prime-)
)

)
(:action Flag341Action-8
:parameters ()
:precondition
(and
(Flag326-)
(Flag326prime-)
)
:effect
(and
(Flag341-)
(Flag341prime-)
)

)
(:action Flag341Action-9
:parameters ()
:precondition
(and
(Flag331-)
(Flag331prime-)
)
:effect
(and
(Flag341-)
(Flag341prime-)
)

)
(:action Flag341Action-10
:parameters ()
:precondition
(and
(Flag316-)
(Flag316prime-)
)
:effect
(and
(Flag341-)
(Flag341prime-)
)

)
(:action Flag341Action-11
:parameters ()
:precondition
(and
(Flag261-)
(Flag261prime-)
)
:effect
(and
(Flag341-)
(Flag341prime-)
)

)
(:action Flag341Action-12
:parameters ()
:precondition
(and
(Flag323-)
(Flag323prime-)
)
:effect
(and
(Flag341-)
(Flag341prime-)
)

)
(:action Flag341Action-13
:parameters ()
:precondition
(and
(Flag307-)
(Flag307prime-)
)
:effect
(and
(Flag341-)
(Flag341prime-)
)

)
(:action Flag341Action-14
:parameters ()
:precondition
(and
(Flag308-)
(Flag308prime-)
)
:effect
(and
(Flag341-)
(Flag341prime-)
)

)
(:action Flag341Action-15
:parameters ()
:precondition
(and
(Flag319-)
(Flag319prime-)
)
:effect
(and
(Flag341-)
(Flag341prime-)
)

)
(:action Flag341Action-16
:parameters ()
:precondition
(and
(Flag229-)
(Flag229prime-)
)
:effect
(and
(Flag341-)
(Flag341prime-)
)

)
(:action Flag341Action-17
:parameters ()
:precondition
(and
(Flag309-)
(Flag309prime-)
)
:effect
(and
(Flag341-)
(Flag341prime-)
)

)
(:action Flag341Action-18
:parameters ()
:precondition
(and
(Flag246-)
(Flag246prime-)
)
:effect
(and
(Flag341-)
(Flag341prime-)
)

)
(:action Flag341Action-19
:parameters ()
:precondition
(and
(Flag231-)
(Flag231prime-)
)
:effect
(and
(Flag341-)
(Flag341prime-)
)

)
(:action Flag341Action-20
:parameters ()
:precondition
(and
(Flag263-)
(Flag263prime-)
)
:effect
(and
(Flag341-)
(Flag341prime-)
)

)
(:action Flag341Action-21
:parameters ()
:precondition
(and
(Flag232-)
(Flag232prime-)
)
:effect
(and
(Flag341-)
(Flag341prime-)
)

)
(:action Flag341Action-22
:parameters ()
:precondition
(and
(Flag286-)
(Flag286prime-)
)
:effect
(and
(Flag341-)
(Flag341prime-)
)

)
(:action Flag341Action-23
:parameters ()
:precondition
(and
(Flag248-)
(Flag248prime-)
)
:effect
(and
(Flag341-)
(Flag341prime-)
)

)
(:action Flag341Action-24
:parameters ()
:precondition
(and
(Flag233-)
(Flag233prime-)
)
:effect
(and
(Flag341-)
(Flag341prime-)
)

)
(:action Flag341Action-25
:parameters ()
:precondition
(and
(Flag234-)
(Flag234prime-)
)
:effect
(and
(Flag341-)
(Flag341prime-)
)

)
(:action Flag341Action-26
:parameters ()
:precondition
(and
(Flag321-)
(Flag321prime-)
)
:effect
(and
(Flag341-)
(Flag341prime-)
)

)
(:action Flag341Action-27
:parameters ()
:precondition
(and
(Flag310-)
(Flag310prime-)
)
:effect
(and
(Flag341-)
(Flag341prime-)
)

)
(:action Flag341Action-28
:parameters ()
:precondition
(and
(Flag332-)
(Flag332prime-)
)
:effect
(and
(Flag341-)
(Flag341prime-)
)

)
(:action Flag341Action-29
:parameters ()
:precondition
(and
(Flag242-)
(Flag242prime-)
)
:effect
(and
(Flag341-)
(Flag341prime-)
)

)
(:action Flag341Action-30
:parameters ()
:precondition
(and
(Flag294-)
(Flag294prime-)
)
:effect
(and
(Flag341-)
(Flag341prime-)
)

)
(:action Flag341Action-31
:parameters ()
:precondition
(and
(Flag280-)
(Flag280prime-)
)
:effect
(and
(Flag341-)
(Flag341prime-)
)

)
(:action Flag341Action-32
:parameters ()
:precondition
(and
(Flag295-)
(Flag295prime-)
)
:effect
(and
(Flag341-)
(Flag341prime-)
)

)
(:action Flag341Action-33
:parameters ()
:precondition
(and
(Flag333-)
(Flag333prime-)
)
:effect
(and
(Flag341-)
(Flag341prime-)
)

)
(:action Flag341Action-34
:parameters ()
:precondition
(and
(Flag281-)
(Flag281prime-)
)
:effect
(and
(Flag341-)
(Flag341prime-)
)

)
(:action Flag341Action-35
:parameters ()
:precondition
(and
(Flag293-)
(Flag293prime-)
)
:effect
(and
(Flag341-)
(Flag341prime-)
)

)
(:action Flag341Action-36
:parameters ()
:precondition
(and
(Flag282-)
(Flag282prime-)
)
:effect
(and
(Flag341-)
(Flag341prime-)
)

)
(:action Flag341Action-37
:parameters ()
:precondition
(and
(Flag238-)
(Flag238prime-)
)
:effect
(and
(Flag341-)
(Flag341prime-)
)

)
(:action Flag341Action-38
:parameters ()
:precondition
(and
(Flag256-)
(Flag256prime-)
)
:effect
(and
(Flag341-)
(Flag341prime-)
)

)
(:action Flag341Action-39
:parameters ()
:precondition
(and
(Flag322-)
(Flag322prime-)
)
:effect
(and
(Flag341-)
(Flag341prime-)
)

)
(:action Flag341Action-40
:parameters ()
:precondition
(and
(Flag296-)
(Flag296prime-)
)
:effect
(and
(Flag341-)
(Flag341prime-)
)

)
(:action Flag341Action-41
:parameters ()
:precondition
(and
(Flag334-)
(Flag334prime-)
)
:effect
(and
(Flag341-)
(Flag341prime-)
)

)
(:action Flag341Action-42
:parameters ()
:precondition
(and
(Flag241-)
(Flag241prime-)
)
:effect
(and
(Flag341-)
(Flag341prime-)
)

)
(:action Flag341Action-43
:parameters ()
:precondition
(and
(Flag264-)
(Flag264prime-)
)
:effect
(and
(Flag341-)
(Flag341prime-)
)

)
(:action Flag341Action-44
:parameters ()
:precondition
(and
(Flag267-)
(Flag267prime-)
)
:effect
(and
(Flag341-)
(Flag341prime-)
)

)
(:action Flag341Action-45
:parameters ()
:precondition
(and
(Flag244-)
(Flag244prime-)
)
:effect
(and
(Flag341-)
(Flag341prime-)
)

)
(:action Flag341Action-46
:parameters ()
:precondition
(and
(Flag298-)
(Flag298prime-)
)
:effect
(and
(Flag341-)
(Flag341prime-)
)

)
(:action Flag341Action-47
:parameters ()
:precondition
(and
(Flag335-)
(Flag335prime-)
)
:effect
(and
(Flag341-)
(Flag341prime-)
)

)
(:action Flag341Action-48
:parameters ()
:precondition
(and
(Flag245-)
(Flag245prime-)
)
:effect
(and
(Flag341-)
(Flag341prime-)
)

)
(:action Flag341Action-49
:parameters ()
:precondition
(and
(Flag269-)
(Flag269prime-)
)
:effect
(and
(Flag341-)
(Flag341prime-)
)

)
(:action Flag341Action-50
:parameters ()
:precondition
(and
(Flag285-)
(Flag285prime-)
)
:effect
(and
(Flag341-)
(Flag341prime-)
)

)
(:action Flag341Action-51
:parameters ()
:precondition
(and
(Flag313-)
(Flag313prime-)
)
:effect
(and
(Flag341-)
(Flag341prime-)
)

)
(:action Flag341Action-52
:parameters ()
:precondition
(and
(Flag292-)
(Flag292prime-)
)
:effect
(and
(Flag341-)
(Flag341prime-)
)

)
(:action Flag341Action-53
:parameters ()
:precondition
(and
(Flag324-)
(Flag324prime-)
)
:effect
(and
(Flag341-)
(Flag341prime-)
)

)
(:action Flag341Action-54
:parameters ()
:precondition
(and
(Flag314-)
(Flag314prime-)
)
:effect
(and
(Flag341-)
(Flag341prime-)
)

)
(:action Flag341Action-55
:parameters ()
:precondition
(and
(Flag249-)
(Flag249prime-)
)
:effect
(and
(Flag341-)
(Flag341prime-)
)

)
(:action Flag341Action-56
:parameters ()
:precondition
(and
(Flag250-)
(Flag250prime-)
)
:effect
(and
(Flag341-)
(Flag341prime-)
)

)
(:action Flag341Action-57
:parameters ()
:precondition
(and
(Flag336-)
(Flag336prime-)
)
:effect
(and
(Flag341-)
(Flag341prime-)
)

)
(:action Flag341Action-58
:parameters ()
:precondition
(and
(Flag299-)
(Flag299prime-)
)
:effect
(and
(Flag341-)
(Flag341prime-)
)

)
(:action Flag341Action-59
:parameters ()
:precondition
(and
(Flag287-)
(Flag287prime-)
)
:effect
(and
(Flag341-)
(Flag341prime-)
)

)
(:action Flag341Action-60
:parameters ()
:precondition
(and
(Flag325-)
(Flag325prime-)
)
:effect
(and
(Flag341-)
(Flag341prime-)
)

)
(:action Flag341Action-61
:parameters ()
:precondition
(and
(Flag271-)
(Flag271prime-)
)
:effect
(and
(Flag341-)
(Flag341prime-)
)

)
(:action Flag341Action-62
:parameters ()
:precondition
(and
(Flag288-)
(Flag288prime-)
)
:effect
(and
(Flag341-)
(Flag341prime-)
)

)
(:action Flag341Action-63
:parameters ()
:precondition
(and
(Flag315-)
(Flag315prime-)
)
:effect
(and
(Flag341-)
(Flag341prime-)
)

)
(:action Flag341Action-64
:parameters ()
:precondition
(and
(Flag272-)
(Flag272prime-)
)
:effect
(and
(Flag341-)
(Flag341prime-)
)

)
(:action Flag341Action-65
:parameters ()
:precondition
(and
(Flag328-)
(Flag328prime-)
)
:effect
(and
(Flag341-)
(Flag341prime-)
)

)
(:action Flag341Action-66
:parameters ()
:precondition
(and
(Flag329-)
(Flag329prime-)
)
:effect
(and
(Flag341-)
(Flag341prime-)
)

)
(:action Flag341Action-67
:parameters ()
:precondition
(and
(Flag337-)
(Flag337prime-)
)
:effect
(and
(Flag341-)
(Flag341prime-)
)

)
(:action Flag341Action-68
:parameters ()
:precondition
(and
(Flag273-)
(Flag273prime-)
)
:effect
(and
(Flag341-)
(Flag341prime-)
)

)
(:action Flag341Action-69
:parameters ()
:precondition
(and
(Flag300-)
(Flag300prime-)
)
:effect
(and
(Flag341-)
(Flag341prime-)
)

)
(:action Flag341Action-70
:parameters ()
:precondition
(and
(Flag255-)
(Flag255prime-)
)
:effect
(and
(Flag341-)
(Flag341prime-)
)

)
(:action Flag341Action-71
:parameters ()
:precondition
(and
(Flag338-)
(Flag338prime-)
)
:effect
(and
(Flag341-)
(Flag341prime-)
)

)
(:action Flag341Action-72
:parameters ()
:precondition
(and
(Flag339-)
(Flag339prime-)
)
:effect
(and
(Flag341-)
(Flag341prime-)
)

)
(:action Flag341Action-73
:parameters ()
:precondition
(and
(Flag340-)
(Flag340prime-)
)
:effect
(and
(Flag341-)
(Flag341prime-)
)

)
(:action Flag341Action-74
:parameters ()
:precondition
(and
(Flag302-)
(Flag302prime-)
)
:effect
(and
(Flag341-)
(Flag341prime-)
)

)
(:action Prim341Action
:parameters ()
:precondition
(and
(not (Flag253-))
(Flag253prime-)
(not (Flag262-))
(Flag262prime-)
(not (Flag289-))
(Flag289prime-)
(not (Flag279-))
(Flag279prime-)
(not (Flag252-))
(Flag252prime-)
(not (Flag304-))
(Flag304prime-)
(not (Flag275-))
(Flag275prime-)
(not (Flag306-))
(Flag306prime-)
(not (Flag326-))
(Flag326prime-)
(not (Flag331-))
(Flag331prime-)
(not (Flag316-))
(Flag316prime-)
(not (Flag261-))
(Flag261prime-)
(not (Flag323-))
(Flag323prime-)
(not (Flag307-))
(Flag307prime-)
(not (Flag308-))
(Flag308prime-)
(not (Flag319-))
(Flag319prime-)
(not (Flag229-))
(Flag229prime-)
(not (Flag309-))
(Flag309prime-)
(not (Flag246-))
(Flag246prime-)
(not (Flag231-))
(Flag231prime-)
(not (Flag263-))
(Flag263prime-)
(not (Flag232-))
(Flag232prime-)
(not (Flag286-))
(Flag286prime-)
(not (Flag248-))
(Flag248prime-)
(not (Flag233-))
(Flag233prime-)
(not (Flag234-))
(Flag234prime-)
(not (Flag321-))
(Flag321prime-)
(not (Flag310-))
(Flag310prime-)
(not (Flag332-))
(Flag332prime-)
(not (Flag242-))
(Flag242prime-)
(not (Flag294-))
(Flag294prime-)
(not (Flag280-))
(Flag280prime-)
(not (Flag295-))
(Flag295prime-)
(not (Flag333-))
(Flag333prime-)
(not (Flag281-))
(Flag281prime-)
(not (Flag293-))
(Flag293prime-)
(not (Flag282-))
(Flag282prime-)
(not (Flag238-))
(Flag238prime-)
(not (Flag256-))
(Flag256prime-)
(not (Flag322-))
(Flag322prime-)
(not (Flag296-))
(Flag296prime-)
(not (Flag334-))
(Flag334prime-)
(not (Flag241-))
(Flag241prime-)
(not (Flag264-))
(Flag264prime-)
(not (Flag267-))
(Flag267prime-)
(not (Flag244-))
(Flag244prime-)
(not (Flag298-))
(Flag298prime-)
(not (Flag335-))
(Flag335prime-)
(not (Flag245-))
(Flag245prime-)
(not (Flag269-))
(Flag269prime-)
(not (Flag285-))
(Flag285prime-)
(not (Flag313-))
(Flag313prime-)
(not (Flag292-))
(Flag292prime-)
(not (Flag324-))
(Flag324prime-)
(not (Flag314-))
(Flag314prime-)
(not (Flag249-))
(Flag249prime-)
(not (Flag250-))
(Flag250prime-)
(not (Flag336-))
(Flag336prime-)
(not (Flag299-))
(Flag299prime-)
(not (Flag287-))
(Flag287prime-)
(not (Flag325-))
(Flag325prime-)
(not (Flag271-))
(Flag271prime-)
(not (Flag288-))
(Flag288prime-)
(not (Flag315-))
(Flag315prime-)
(not (Flag272-))
(Flag272prime-)
(not (Flag328-))
(Flag328prime-)
(not (Flag329-))
(Flag329prime-)
(not (Flag337-))
(Flag337prime-)
(not (Flag273-))
(Flag273prime-)
(not (Flag300-))
(Flag300prime-)
(not (Flag255-))
(Flag255prime-)
(not (Flag338-))
(Flag338prime-)
(not (Flag339-))
(Flag339prime-)
(not (Flag340-))
(Flag340prime-)
(not (Flag302-))
(Flag302prime-)
)
:effect
(and
(Flag341prime-)
(not (Flag341-))
)

)
(:action Flag352Action-0
:parameters ()
:precondition
(and
(Flag262-)
(Flag262prime-)
)
:effect
(and
(Flag352-)
(Flag352prime-)
)

)
(:action Flag352Action-1
:parameters ()
:precondition
(and
(Flag342-)
(Flag342prime-)
)
:effect
(and
(Flag352-)
(Flag352prime-)
)

)
(:action Flag352Action-2
:parameters ()
:precondition
(and
(Flag289-)
(Flag289prime-)
)
:effect
(and
(Flag352-)
(Flag352prime-)
)

)
(:action Flag352Action-3
:parameters ()
:precondition
(and
(Flag279-)
(Flag279prime-)
)
:effect
(and
(Flag352-)
(Flag352prime-)
)

)
(:action Flag352Action-4
:parameters ()
:precondition
(and
(Flag252-)
(Flag252prime-)
)
:effect
(and
(Flag352-)
(Flag352prime-)
)

)
(:action Flag352Action-5
:parameters ()
:precondition
(and
(Flag304-)
(Flag304prime-)
)
:effect
(and
(Flag352-)
(Flag352prime-)
)

)
(:action Flag352Action-6
:parameters ()
:precondition
(and
(Flag275-)
(Flag275prime-)
)
:effect
(and
(Flag352-)
(Flag352prime-)
)

)
(:action Flag352Action-7
:parameters ()
:precondition
(and
(Flag328-)
(Flag328prime-)
)
:effect
(and
(Flag352-)
(Flag352prime-)
)

)
(:action Flag352Action-8
:parameters ()
:precondition
(and
(Flag315-)
(Flag315prime-)
)
:effect
(and
(Flag352-)
(Flag352prime-)
)

)
(:action Flag352Action-9
:parameters ()
:precondition
(and
(Flag306-)
(Flag306prime-)
)
:effect
(and
(Flag352-)
(Flag352prime-)
)

)
(:action Flag352Action-10
:parameters ()
:precondition
(and
(Flag326-)
(Flag326prime-)
)
:effect
(and
(Flag352-)
(Flag352prime-)
)

)
(:action Flag352Action-11
:parameters ()
:precondition
(and
(Flag293-)
(Flag293prime-)
)
:effect
(and
(Flag352-)
(Flag352prime-)
)

)
(:action Flag352Action-12
:parameters ()
:precondition
(and
(Flag329-)
(Flag329prime-)
)
:effect
(and
(Flag352-)
(Flag352prime-)
)

)
(:action Flag352Action-13
:parameters ()
:precondition
(and
(Flag343-)
(Flag343prime-)
)
:effect
(and
(Flag352-)
(Flag352prime-)
)

)
(:action Flag352Action-14
:parameters ()
:precondition
(and
(Flag307-)
(Flag307prime-)
)
:effect
(and
(Flag352-)
(Flag352prime-)
)

)
(:action Flag352Action-15
:parameters ()
:precondition
(and
(Flag308-)
(Flag308prime-)
)
:effect
(and
(Flag352-)
(Flag352prime-)
)

)
(:action Flag352Action-16
:parameters ()
:precondition
(and
(Flag319-)
(Flag319prime-)
)
:effect
(and
(Flag352-)
(Flag352prime-)
)

)
(:action Flag352Action-17
:parameters ()
:precondition
(and
(Flag229-)
(Flag229prime-)
)
:effect
(and
(Flag352-)
(Flag352prime-)
)

)
(:action Flag352Action-18
:parameters ()
:precondition
(and
(Flag309-)
(Flag309prime-)
)
:effect
(and
(Flag352-)
(Flag352prime-)
)

)
(:action Flag352Action-19
:parameters ()
:precondition
(and
(Flag246-)
(Flag246prime-)
)
:effect
(and
(Flag352-)
(Flag352prime-)
)

)
(:action Flag352Action-20
:parameters ()
:precondition
(and
(Flag263-)
(Flag263prime-)
)
:effect
(and
(Flag352-)
(Flag352prime-)
)

)
(:action Flag352Action-21
:parameters ()
:precondition
(and
(Flag232-)
(Flag232prime-)
)
:effect
(and
(Flag352-)
(Flag352prime-)
)

)
(:action Flag352Action-22
:parameters ()
:precondition
(and
(Flag344-)
(Flag344prime-)
)
:effect
(and
(Flag352-)
(Flag352prime-)
)

)
(:action Flag352Action-23
:parameters ()
:precondition
(and
(Flag286-)
(Flag286prime-)
)
:effect
(and
(Flag352-)
(Flag352prime-)
)

)
(:action Flag352Action-24
:parameters ()
:precondition
(and
(Flag248-)
(Flag248prime-)
)
:effect
(and
(Flag352-)
(Flag352prime-)
)

)
(:action Flag352Action-25
:parameters ()
:precondition
(and
(Flag233-)
(Flag233prime-)
)
:effect
(and
(Flag352-)
(Flag352prime-)
)

)
(:action Flag352Action-26
:parameters ()
:precondition
(and
(Flag321-)
(Flag321prime-)
)
:effect
(and
(Flag352-)
(Flag352prime-)
)

)
(:action Flag352Action-27
:parameters ()
:precondition
(and
(Flag345-)
(Flag345prime-)
)
:effect
(and
(Flag352-)
(Flag352prime-)
)

)
(:action Flag352Action-28
:parameters ()
:precondition
(and
(Flag332-)
(Flag332prime-)
)
:effect
(and
(Flag352-)
(Flag352prime-)
)

)
(:action Flag352Action-29
:parameters ()
:precondition
(and
(Flag242-)
(Flag242prime-)
)
:effect
(and
(Flag352-)
(Flag352prime-)
)

)
(:action Flag352Action-30
:parameters ()
:precondition
(and
(Flag294-)
(Flag294prime-)
)
:effect
(and
(Flag352-)
(Flag352prime-)
)

)
(:action Flag352Action-31
:parameters ()
:precondition
(and
(Flag280-)
(Flag280prime-)
)
:effect
(and
(Flag352-)
(Flag352prime-)
)

)
(:action Flag352Action-32
:parameters ()
:precondition
(and
(Flag295-)
(Flag295prime-)
)
:effect
(and
(Flag352-)
(Flag352prime-)
)

)
(:action Flag352Action-33
:parameters ()
:precondition
(and
(Flag333-)
(Flag333prime-)
)
:effect
(and
(Flag352-)
(Flag352prime-)
)

)
(:action Flag352Action-34
:parameters ()
:precondition
(and
(Flag296-)
(Flag296prime-)
)
:effect
(and
(Flag352-)
(Flag352prime-)
)

)
(:action Flag352Action-35
:parameters ()
:precondition
(and
(Flag331-)
(Flag331prime-)
)
:effect
(and
(Flag352-)
(Flag352prime-)
)

)
(:action Flag352Action-36
:parameters ()
:precondition
(and
(Flag282-)
(Flag282prime-)
)
:effect
(and
(Flag352-)
(Flag352prime-)
)

)
(:action Flag352Action-37
:parameters ()
:precondition
(and
(Flag238-)
(Flag238prime-)
)
:effect
(and
(Flag352-)
(Flag352prime-)
)

)
(:action Flag352Action-38
:parameters ()
:precondition
(and
(Flag256-)
(Flag256prime-)
)
:effect
(and
(Flag352-)
(Flag352prime-)
)

)
(:action Flag352Action-39
:parameters ()
:precondition
(and
(Flag322-)
(Flag322prime-)
)
:effect
(and
(Flag352-)
(Flag352prime-)
)

)
(:action Flag352Action-40
:parameters ()
:precondition
(and
(Flag323-)
(Flag323prime-)
)
:effect
(and
(Flag352-)
(Flag352prime-)
)

)
(:action Flag352Action-41
:parameters ()
:precondition
(and
(Flag241-)
(Flag241prime-)
)
:effect
(and
(Flag352-)
(Flag352prime-)
)

)
(:action Flag352Action-42
:parameters ()
:precondition
(and
(Flag264-)
(Flag264prime-)
)
:effect
(and
(Flag352-)
(Flag352prime-)
)

)
(:action Flag352Action-43
:parameters ()
:precondition
(and
(Flag267-)
(Flag267prime-)
)
:effect
(and
(Flag352-)
(Flag352prime-)
)

)
(:action Flag352Action-44
:parameters ()
:precondition
(and
(Flag244-)
(Flag244prime-)
)
:effect
(and
(Flag352-)
(Flag352prime-)
)

)
(:action Flag352Action-45
:parameters ()
:precondition
(and
(Flag335-)
(Flag335prime-)
)
:effect
(and
(Flag352-)
(Flag352prime-)
)

)
(:action Flag352Action-46
:parameters ()
:precondition
(and
(Flag245-)
(Flag245prime-)
)
:effect
(and
(Flag352-)
(Flag352prime-)
)

)
(:action Flag352Action-47
:parameters ()
:precondition
(and
(Flag269-)
(Flag269prime-)
)
:effect
(and
(Flag352-)
(Flag352prime-)
)

)
(:action Flag352Action-48
:parameters ()
:precondition
(and
(Flag285-)
(Flag285prime-)
)
:effect
(and
(Flag352-)
(Flag352prime-)
)

)
(:action Flag352Action-49
:parameters ()
:precondition
(and
(Flag313-)
(Flag313prime-)
)
:effect
(and
(Flag352-)
(Flag352prime-)
)

)
(:action Flag352Action-50
:parameters ()
:precondition
(and
(Flag292-)
(Flag292prime-)
)
:effect
(and
(Flag352-)
(Flag352prime-)
)

)
(:action Flag352Action-51
:parameters ()
:precondition
(and
(Flag324-)
(Flag324prime-)
)
:effect
(and
(Flag352-)
(Flag352prime-)
)

)
(:action Flag352Action-52
:parameters ()
:precondition
(and
(Flag314-)
(Flag314prime-)
)
:effect
(and
(Flag352-)
(Flag352prime-)
)

)
(:action Flag352Action-53
:parameters ()
:precondition
(and
(Flag249-)
(Flag249prime-)
)
:effect
(and
(Flag352-)
(Flag352prime-)
)

)
(:action Flag352Action-54
:parameters ()
:precondition
(and
(Flag250-)
(Flag250prime-)
)
:effect
(and
(Flag352-)
(Flag352prime-)
)

)
(:action Flag352Action-55
:parameters ()
:precondition
(and
(Flag346-)
(Flag346prime-)
)
:effect
(and
(Flag352-)
(Flag352prime-)
)

)
(:action Flag352Action-56
:parameters ()
:precondition
(and
(Flag299-)
(Flag299prime-)
)
:effect
(and
(Flag352-)
(Flag352prime-)
)

)
(:action Flag352Action-57
:parameters ()
:precondition
(and
(Flag287-)
(Flag287prime-)
)
:effect
(and
(Flag352-)
(Flag352prime-)
)

)
(:action Flag352Action-58
:parameters ()
:precondition
(and
(Flag325-)
(Flag325prime-)
)
:effect
(and
(Flag352-)
(Flag352prime-)
)

)
(:action Flag352Action-59
:parameters ()
:precondition
(and
(Flag347-)
(Flag347prime-)
)
:effect
(and
(Flag352-)
(Flag352prime-)
)

)
(:action Flag352Action-60
:parameters ()
:precondition
(and
(Flag271-)
(Flag271prime-)
)
:effect
(and
(Flag352-)
(Flag352prime-)
)

)
(:action Flag352Action-61
:parameters ()
:precondition
(and
(Flag288-)
(Flag288prime-)
)
:effect
(and
(Flag352-)
(Flag352prime-)
)

)
(:action Flag352Action-62
:parameters ()
:precondition
(and
(Flag348-)
(Flag348prime-)
)
:effect
(and
(Flag352-)
(Flag352prime-)
)

)
(:action Flag352Action-63
:parameters ()
:precondition
(and
(Flag272-)
(Flag272prime-)
)
:effect
(and
(Flag352-)
(Flag352prime-)
)

)
(:action Flag352Action-64
:parameters ()
:precondition
(and
(Flag349-)
(Flag349prime-)
)
:effect
(and
(Flag352-)
(Flag352prime-)
)

)
(:action Flag352Action-65
:parameters ()
:precondition
(and
(Flag316-)
(Flag316prime-)
)
:effect
(and
(Flag352-)
(Flag352prime-)
)

)
(:action Flag352Action-66
:parameters ()
:precondition
(and
(Flag253-)
(Flag253prime-)
)
:effect
(and
(Flag352-)
(Flag352prime-)
)

)
(:action Flag352Action-67
:parameters ()
:precondition
(and
(Flag273-)
(Flag273prime-)
)
:effect
(and
(Flag352-)
(Flag352prime-)
)

)
(:action Flag352Action-68
:parameters ()
:precondition
(and
(Flag300-)
(Flag300prime-)
)
:effect
(and
(Flag352-)
(Flag352prime-)
)

)
(:action Flag352Action-69
:parameters ()
:precondition
(and
(Flag350-)
(Flag350prime-)
)
:effect
(and
(Flag352-)
(Flag352prime-)
)

)
(:action Flag352Action-70
:parameters ()
:precondition
(and
(Flag351-)
(Flag351prime-)
)
:effect
(and
(Flag352-)
(Flag352prime-)
)

)
(:action Flag352Action-71
:parameters ()
:precondition
(and
(Flag255-)
(Flag255prime-)
)
:effect
(and
(Flag352-)
(Flag352prime-)
)

)
(:action Flag352Action-72
:parameters ()
:precondition
(and
(Flag338-)
(Flag338prime-)
)
:effect
(and
(Flag352-)
(Flag352prime-)
)

)
(:action Flag352Action-73
:parameters ()
:precondition
(and
(Flag339-)
(Flag339prime-)
)
:effect
(and
(Flag352-)
(Flag352prime-)
)

)
(:action Flag352Action-74
:parameters ()
:precondition
(and
(Flag340-)
(Flag340prime-)
)
:effect
(and
(Flag352-)
(Flag352prime-)
)

)
(:action Flag352Action-75
:parameters ()
:precondition
(and
(Flag302-)
(Flag302prime-)
)
:effect
(and
(Flag352-)
(Flag352prime-)
)

)
(:action Prim352Action
:parameters ()
:precondition
(and
(not (Flag262-))
(Flag262prime-)
(not (Flag342-))
(Flag342prime-)
(not (Flag289-))
(Flag289prime-)
(not (Flag279-))
(Flag279prime-)
(not (Flag252-))
(Flag252prime-)
(not (Flag304-))
(Flag304prime-)
(not (Flag275-))
(Flag275prime-)
(not (Flag328-))
(Flag328prime-)
(not (Flag315-))
(Flag315prime-)
(not (Flag306-))
(Flag306prime-)
(not (Flag326-))
(Flag326prime-)
(not (Flag293-))
(Flag293prime-)
(not (Flag329-))
(Flag329prime-)
(not (Flag343-))
(Flag343prime-)
(not (Flag307-))
(Flag307prime-)
(not (Flag308-))
(Flag308prime-)
(not (Flag319-))
(Flag319prime-)
(not (Flag229-))
(Flag229prime-)
(not (Flag309-))
(Flag309prime-)
(not (Flag246-))
(Flag246prime-)
(not (Flag263-))
(Flag263prime-)
(not (Flag232-))
(Flag232prime-)
(not (Flag344-))
(Flag344prime-)
(not (Flag286-))
(Flag286prime-)
(not (Flag248-))
(Flag248prime-)
(not (Flag233-))
(Flag233prime-)
(not (Flag321-))
(Flag321prime-)
(not (Flag345-))
(Flag345prime-)
(not (Flag332-))
(Flag332prime-)
(not (Flag242-))
(Flag242prime-)
(not (Flag294-))
(Flag294prime-)
(not (Flag280-))
(Flag280prime-)
(not (Flag295-))
(Flag295prime-)
(not (Flag333-))
(Flag333prime-)
(not (Flag296-))
(Flag296prime-)
(not (Flag331-))
(Flag331prime-)
(not (Flag282-))
(Flag282prime-)
(not (Flag238-))
(Flag238prime-)
(not (Flag256-))
(Flag256prime-)
(not (Flag322-))
(Flag322prime-)
(not (Flag323-))
(Flag323prime-)
(not (Flag241-))
(Flag241prime-)
(not (Flag264-))
(Flag264prime-)
(not (Flag267-))
(Flag267prime-)
(not (Flag244-))
(Flag244prime-)
(not (Flag335-))
(Flag335prime-)
(not (Flag245-))
(Flag245prime-)
(not (Flag269-))
(Flag269prime-)
(not (Flag285-))
(Flag285prime-)
(not (Flag313-))
(Flag313prime-)
(not (Flag292-))
(Flag292prime-)
(not (Flag324-))
(Flag324prime-)
(not (Flag314-))
(Flag314prime-)
(not (Flag249-))
(Flag249prime-)
(not (Flag250-))
(Flag250prime-)
(not (Flag346-))
(Flag346prime-)
(not (Flag299-))
(Flag299prime-)
(not (Flag287-))
(Flag287prime-)
(not (Flag325-))
(Flag325prime-)
(not (Flag347-))
(Flag347prime-)
(not (Flag271-))
(Flag271prime-)
(not (Flag288-))
(Flag288prime-)
(not (Flag348-))
(Flag348prime-)
(not (Flag272-))
(Flag272prime-)
(not (Flag349-))
(Flag349prime-)
(not (Flag316-))
(Flag316prime-)
(not (Flag253-))
(Flag253prime-)
(not (Flag273-))
(Flag273prime-)
(not (Flag300-))
(Flag300prime-)
(not (Flag350-))
(Flag350prime-)
(not (Flag351-))
(Flag351prime-)
(not (Flag255-))
(Flag255prime-)
(not (Flag338-))
(Flag338prime-)
(not (Flag339-))
(Flag339prime-)
(not (Flag340-))
(Flag340prime-)
(not (Flag302-))
(Flag302prime-)
)
:effect
(and
(Flag352prime-)
(not (Flag352-))
)

)
(:action Flag361Action-0
:parameters ()
:precondition
(and
(Flag262-)
(Flag262prime-)
)
:effect
(and
(Flag361-)
(Flag361prime-)
)

)
(:action Flag361Action-1
:parameters ()
:precondition
(and
(Flag342-)
(Flag342prime-)
)
:effect
(and
(Flag361-)
(Flag361prime-)
)

)
(:action Flag361Action-2
:parameters ()
:precondition
(and
(Flag289-)
(Flag289prime-)
)
:effect
(and
(Flag361-)
(Flag361prime-)
)

)
(:action Flag361Action-3
:parameters ()
:precondition
(and
(Flag279-)
(Flag279prime-)
)
:effect
(and
(Flag361-)
(Flag361prime-)
)

)
(:action Flag361Action-4
:parameters ()
:precondition
(and
(Flag252-)
(Flag252prime-)
)
:effect
(and
(Flag361-)
(Flag361prime-)
)

)
(:action Flag361Action-5
:parameters ()
:precondition
(and
(Flag304-)
(Flag304prime-)
)
:effect
(and
(Flag361-)
(Flag361prime-)
)

)
(:action Flag361Action-6
:parameters ()
:precondition
(and
(Flag275-)
(Flag275prime-)
)
:effect
(and
(Flag361-)
(Flag361prime-)
)

)
(:action Flag361Action-7
:parameters ()
:precondition
(and
(Flag315-)
(Flag315prime-)
)
:effect
(and
(Flag361-)
(Flag361prime-)
)

)
(:action Flag361Action-8
:parameters ()
:precondition
(and
(Flag306-)
(Flag306prime-)
)
:effect
(and
(Flag361-)
(Flag361prime-)
)

)
(:action Flag361Action-9
:parameters ()
:precondition
(and
(Flag326-)
(Flag326prime-)
)
:effect
(and
(Flag361-)
(Flag361prime-)
)

)
(:action Flag361Action-10
:parameters ()
:precondition
(and
(Flag331-)
(Flag331prime-)
)
:effect
(and
(Flag361-)
(Flag361prime-)
)

)
(:action Flag361Action-11
:parameters ()
:precondition
(and
(Flag316-)
(Flag316prime-)
)
:effect
(and
(Flag361-)
(Flag361prime-)
)

)
(:action Flag361Action-12
:parameters ()
:precondition
(and
(Flag343-)
(Flag343prime-)
)
:effect
(and
(Flag361-)
(Flag361prime-)
)

)
(:action Flag361Action-13
:parameters ()
:precondition
(and
(Flag307-)
(Flag307prime-)
)
:effect
(and
(Flag361-)
(Flag361prime-)
)

)
(:action Flag361Action-14
:parameters ()
:precondition
(and
(Flag308-)
(Flag308prime-)
)
:effect
(and
(Flag361-)
(Flag361prime-)
)

)
(:action Flag361Action-15
:parameters ()
:precondition
(and
(Flag319-)
(Flag319prime-)
)
:effect
(and
(Flag361-)
(Flag361prime-)
)

)
(:action Flag361Action-16
:parameters ()
:precondition
(and
(Flag229-)
(Flag229prime-)
)
:effect
(and
(Flag361-)
(Flag361prime-)
)

)
(:action Flag361Action-17
:parameters ()
:precondition
(and
(Flag353-)
(Flag353prime-)
)
:effect
(and
(Flag361-)
(Flag361prime-)
)

)
(:action Flag361Action-18
:parameters ()
:precondition
(and
(Flag354-)
(Flag354prime-)
)
:effect
(and
(Flag361-)
(Flag361prime-)
)

)
(:action Flag361Action-19
:parameters ()
:precondition
(and
(Flag232-)
(Flag232prime-)
)
:effect
(and
(Flag361-)
(Flag361prime-)
)

)
(:action Flag361Action-20
:parameters ()
:precondition
(and
(Flag355-)
(Flag355prime-)
)
:effect
(and
(Flag361-)
(Flag361prime-)
)

)
(:action Flag361Action-21
:parameters ()
:precondition
(and
(Flag314-)
(Flag314prime-)
)
:effect
(and
(Flag361-)
(Flag361prime-)
)

)
(:action Flag361Action-22
:parameters ()
:precondition
(and
(Flag233-)
(Flag233prime-)
)
:effect
(and
(Flag361-)
(Flag361prime-)
)

)
(:action Flag361Action-23
:parameters ()
:precondition
(and
(Flag246-)
(Flag246prime-)
)
:effect
(and
(Flag361-)
(Flag361prime-)
)

)
(:action Flag361Action-24
:parameters ()
:precondition
(and
(Flag321-)
(Flag321prime-)
)
:effect
(and
(Flag361-)
(Flag361prime-)
)

)
(:action Flag361Action-25
:parameters ()
:precondition
(and
(Flag345-)
(Flag345prime-)
)
:effect
(and
(Flag361-)
(Flag361prime-)
)

)
(:action Flag361Action-26
:parameters ()
:precondition
(and
(Flag332-)
(Flag332prime-)
)
:effect
(and
(Flag361-)
(Flag361prime-)
)

)
(:action Flag361Action-27
:parameters ()
:precondition
(and
(Flag242-)
(Flag242prime-)
)
:effect
(and
(Flag361-)
(Flag361prime-)
)

)
(:action Flag361Action-28
:parameters ()
:precondition
(and
(Flag294-)
(Flag294prime-)
)
:effect
(and
(Flag361-)
(Flag361prime-)
)

)
(:action Flag361Action-29
:parameters ()
:precondition
(and
(Flag263-)
(Flag263prime-)
)
:effect
(and
(Flag361-)
(Flag361prime-)
)

)
(:action Flag361Action-30
:parameters ()
:precondition
(and
(Flag295-)
(Flag295prime-)
)
:effect
(and
(Flag361-)
(Flag361prime-)
)

)
(:action Flag361Action-31
:parameters ()
:precondition
(and
(Flag333-)
(Flag333prime-)
)
:effect
(and
(Flag361-)
(Flag361prime-)
)

)
(:action Flag361Action-32
:parameters ()
:precondition
(and
(Flag296-)
(Flag296prime-)
)
:effect
(and
(Flag361-)
(Flag361prime-)
)

)
(:action Flag361Action-33
:parameters ()
:precondition
(and
(Flag302-)
(Flag302prime-)
)
:effect
(and
(Flag361-)
(Flag361prime-)
)

)
(:action Flag361Action-34
:parameters ()
:precondition
(and
(Flag238-)
(Flag238prime-)
)
:effect
(and
(Flag361-)
(Flag361prime-)
)

)
(:action Flag361Action-35
:parameters ()
:precondition
(and
(Flag322-)
(Flag322prime-)
)
:effect
(and
(Flag361-)
(Flag361prime-)
)

)
(:action Flag361Action-36
:parameters ()
:precondition
(and
(Flag255-)
(Flag255prime-)
)
:effect
(and
(Flag361-)
(Flag361prime-)
)

)
(:action Flag361Action-37
:parameters ()
:precondition
(and
(Flag323-)
(Flag323prime-)
)
:effect
(and
(Flag361-)
(Flag361prime-)
)

)
(:action Flag361Action-38
:parameters ()
:precondition
(and
(Flag356-)
(Flag356prime-)
)
:effect
(and
(Flag361-)
(Flag361prime-)
)

)
(:action Flag361Action-39
:parameters ()
:precondition
(and
(Flag241-)
(Flag241prime-)
)
:effect
(and
(Flag361-)
(Flag361prime-)
)

)
(:action Flag361Action-40
:parameters ()
:precondition
(and
(Flag264-)
(Flag264prime-)
)
:effect
(and
(Flag361-)
(Flag361prime-)
)

)
(:action Flag361Action-41
:parameters ()
:precondition
(and
(Flag282-)
(Flag282prime-)
)
:effect
(and
(Flag361-)
(Flag361prime-)
)

)
(:action Flag361Action-42
:parameters ()
:precondition
(and
(Flag244-)
(Flag244prime-)
)
:effect
(and
(Flag361-)
(Flag361prime-)
)

)
(:action Flag361Action-43
:parameters ()
:precondition
(and
(Flag309-)
(Flag309prime-)
)
:effect
(and
(Flag361-)
(Flag361prime-)
)

)
(:action Flag361Action-44
:parameters ()
:precondition
(and
(Flag335-)
(Flag335prime-)
)
:effect
(and
(Flag361-)
(Flag361prime-)
)

)
(:action Flag361Action-45
:parameters ()
:precondition
(and
(Flag245-)
(Flag245prime-)
)
:effect
(and
(Flag361-)
(Flag361prime-)
)

)
(:action Flag361Action-46
:parameters ()
:precondition
(and
(Flag357-)
(Flag357prime-)
)
:effect
(and
(Flag361-)
(Flag361prime-)
)

)
(:action Flag361Action-47
:parameters ()
:precondition
(and
(Flag269-)
(Flag269prime-)
)
:effect
(and
(Flag361-)
(Flag361prime-)
)

)
(:action Flag361Action-48
:parameters ()
:precondition
(and
(Flag285-)
(Flag285prime-)
)
:effect
(and
(Flag361-)
(Flag361prime-)
)

)
(:action Flag361Action-49
:parameters ()
:precondition
(and
(Flag292-)
(Flag292prime-)
)
:effect
(and
(Flag361-)
(Flag361prime-)
)

)
(:action Flag361Action-50
:parameters ()
:precondition
(and
(Flag324-)
(Flag324prime-)
)
:effect
(and
(Flag361-)
(Flag361prime-)
)

)
(:action Flag361Action-51
:parameters ()
:precondition
(and
(Flag248-)
(Flag248prime-)
)
:effect
(and
(Flag361-)
(Flag361prime-)
)

)
(:action Flag361Action-52
:parameters ()
:precondition
(and
(Flag249-)
(Flag249prime-)
)
:effect
(and
(Flag361-)
(Flag361prime-)
)

)
(:action Flag361Action-53
:parameters ()
:precondition
(and
(Flag286-)
(Flag286prime-)
)
:effect
(and
(Flag361-)
(Flag361prime-)
)

)
(:action Flag361Action-54
:parameters ()
:precondition
(and
(Flag358-)
(Flag358prime-)
)
:effect
(and
(Flag361-)
(Flag361prime-)
)

)
(:action Flag361Action-55
:parameters ()
:precondition
(and
(Flag346-)
(Flag346prime-)
)
:effect
(and
(Flag361-)
(Flag361prime-)
)

)
(:action Flag361Action-56
:parameters ()
:precondition
(and
(Flag359-)
(Flag359prime-)
)
:effect
(and
(Flag361-)
(Flag361prime-)
)

)
(:action Flag361Action-57
:parameters ()
:precondition
(and
(Flag299-)
(Flag299prime-)
)
:effect
(and
(Flag361-)
(Flag361prime-)
)

)
(:action Flag361Action-58
:parameters ()
:precondition
(and
(Flag287-)
(Flag287prime-)
)
:effect
(and
(Flag361-)
(Flag361prime-)
)

)
(:action Flag361Action-59
:parameters ()
:precondition
(and
(Flag325-)
(Flag325prime-)
)
:effect
(and
(Flag361-)
(Flag361prime-)
)

)
(:action Flag361Action-60
:parameters ()
:precondition
(and
(Flag271-)
(Flag271prime-)
)
:effect
(and
(Flag361-)
(Flag361prime-)
)

)
(:action Flag361Action-61
:parameters ()
:precondition
(and
(Flag288-)
(Flag288prime-)
)
:effect
(and
(Flag361-)
(Flag361prime-)
)

)
(:action Flag361Action-62
:parameters ()
:precondition
(and
(Flag348-)
(Flag348prime-)
)
:effect
(and
(Flag361-)
(Flag361prime-)
)

)
(:action Flag361Action-63
:parameters ()
:precondition
(and
(Flag272-)
(Flag272prime-)
)
:effect
(and
(Flag361-)
(Flag361prime-)
)

)
(:action Flag361Action-64
:parameters ()
:precondition
(and
(Flag349-)
(Flag349prime-)
)
:effect
(and
(Flag361-)
(Flag361prime-)
)

)
(:action Flag361Action-65
:parameters ()
:precondition
(and
(Flag329-)
(Flag329prime-)
)
:effect
(and
(Flag361-)
(Flag361prime-)
)

)
(:action Flag361Action-66
:parameters ()
:precondition
(and
(Flag253-)
(Flag253prime-)
)
:effect
(and
(Flag361-)
(Flag361prime-)
)

)
(:action Flag361Action-67
:parameters ()
:precondition
(and
(Flag273-)
(Flag273prime-)
)
:effect
(and
(Flag361-)
(Flag361prime-)
)

)
(:action Flag361Action-68
:parameters ()
:precondition
(and
(Flag300-)
(Flag300prime-)
)
:effect
(and
(Flag361-)
(Flag361prime-)
)

)
(:action Flag361Action-69
:parameters ()
:precondition
(and
(Flag351-)
(Flag351prime-)
)
:effect
(and
(Flag361-)
(Flag361prime-)
)

)
(:action Flag361Action-70
:parameters ()
:precondition
(and
(Flag360-)
(Flag360prime-)
)
:effect
(and
(Flag361-)
(Flag361prime-)
)

)
(:action Flag361Action-71
:parameters ()
:precondition
(and
(Flag338-)
(Flag338prime-)
)
:effect
(and
(Flag361-)
(Flag361prime-)
)

)
(:action Flag361Action-72
:parameters ()
:precondition
(and
(Flag339-)
(Flag339prime-)
)
:effect
(and
(Flag361-)
(Flag361prime-)
)

)
(:action Flag361Action-73
:parameters ()
:precondition
(and
(Flag340-)
(Flag340prime-)
)
:effect
(and
(Flag361-)
(Flag361prime-)
)

)
(:action Prim361Action
:parameters ()
:precondition
(and
(not (Flag262-))
(Flag262prime-)
(not (Flag342-))
(Flag342prime-)
(not (Flag289-))
(Flag289prime-)
(not (Flag279-))
(Flag279prime-)
(not (Flag252-))
(Flag252prime-)
(not (Flag304-))
(Flag304prime-)
(not (Flag275-))
(Flag275prime-)
(not (Flag315-))
(Flag315prime-)
(not (Flag306-))
(Flag306prime-)
(not (Flag326-))
(Flag326prime-)
(not (Flag331-))
(Flag331prime-)
(not (Flag316-))
(Flag316prime-)
(not (Flag343-))
(Flag343prime-)
(not (Flag307-))
(Flag307prime-)
(not (Flag308-))
(Flag308prime-)
(not (Flag319-))
(Flag319prime-)
(not (Flag229-))
(Flag229prime-)
(not (Flag353-))
(Flag353prime-)
(not (Flag354-))
(Flag354prime-)
(not (Flag232-))
(Flag232prime-)
(not (Flag355-))
(Flag355prime-)
(not (Flag314-))
(Flag314prime-)
(not (Flag233-))
(Flag233prime-)
(not (Flag246-))
(Flag246prime-)
(not (Flag321-))
(Flag321prime-)
(not (Flag345-))
(Flag345prime-)
(not (Flag332-))
(Flag332prime-)
(not (Flag242-))
(Flag242prime-)
(not (Flag294-))
(Flag294prime-)
(not (Flag263-))
(Flag263prime-)
(not (Flag295-))
(Flag295prime-)
(not (Flag333-))
(Flag333prime-)
(not (Flag296-))
(Flag296prime-)
(not (Flag302-))
(Flag302prime-)
(not (Flag238-))
(Flag238prime-)
(not (Flag322-))
(Flag322prime-)
(not (Flag255-))
(Flag255prime-)
(not (Flag323-))
(Flag323prime-)
(not (Flag356-))
(Flag356prime-)
(not (Flag241-))
(Flag241prime-)
(not (Flag264-))
(Flag264prime-)
(not (Flag282-))
(Flag282prime-)
(not (Flag244-))
(Flag244prime-)
(not (Flag309-))
(Flag309prime-)
(not (Flag335-))
(Flag335prime-)
(not (Flag245-))
(Flag245prime-)
(not (Flag357-))
(Flag357prime-)
(not (Flag269-))
(Flag269prime-)
(not (Flag285-))
(Flag285prime-)
(not (Flag292-))
(Flag292prime-)
(not (Flag324-))
(Flag324prime-)
(not (Flag248-))
(Flag248prime-)
(not (Flag249-))
(Flag249prime-)
(not (Flag286-))
(Flag286prime-)
(not (Flag358-))
(Flag358prime-)
(not (Flag346-))
(Flag346prime-)
(not (Flag359-))
(Flag359prime-)
(not (Flag299-))
(Flag299prime-)
(not (Flag287-))
(Flag287prime-)
(not (Flag325-))
(Flag325prime-)
(not (Flag271-))
(Flag271prime-)
(not (Flag288-))
(Flag288prime-)
(not (Flag348-))
(Flag348prime-)
(not (Flag272-))
(Flag272prime-)
(not (Flag349-))
(Flag349prime-)
(not (Flag329-))
(Flag329prime-)
(not (Flag253-))
(Flag253prime-)
(not (Flag273-))
(Flag273prime-)
(not (Flag300-))
(Flag300prime-)
(not (Flag351-))
(Flag351prime-)
(not (Flag360-))
(Flag360prime-)
(not (Flag338-))
(Flag338prime-)
(not (Flag339-))
(Flag339prime-)
(not (Flag340-))
(Flag340prime-)
)
:effect
(and
(Flag361prime-)
(not (Flag361-))
)

)
(:action Flag368Action-0
:parameters ()
:precondition
(and
(Flag262-)
(Flag262prime-)
)
:effect
(and
(Flag368-)
(Flag368prime-)
)

)
(:action Flag368Action-1
:parameters ()
:precondition
(and
(Flag342-)
(Flag342prime-)
)
:effect
(and
(Flag368-)
(Flag368prime-)
)

)
(:action Flag368Action-2
:parameters ()
:precondition
(and
(Flag289-)
(Flag289prime-)
)
:effect
(and
(Flag368-)
(Flag368prime-)
)

)
(:action Flag368Action-3
:parameters ()
:precondition
(and
(Flag304-)
(Flag304prime-)
)
:effect
(and
(Flag368-)
(Flag368prime-)
)

)
(:action Flag368Action-4
:parameters ()
:precondition
(and
(Flag275-)
(Flag275prime-)
)
:effect
(and
(Flag368-)
(Flag368prime-)
)

)
(:action Flag368Action-5
:parameters ()
:precondition
(and
(Flag315-)
(Flag315prime-)
)
:effect
(and
(Flag368-)
(Flag368prime-)
)

)
(:action Flag368Action-6
:parameters ()
:precondition
(and
(Flag306-)
(Flag306prime-)
)
:effect
(and
(Flag368-)
(Flag368prime-)
)

)
(:action Flag368Action-7
:parameters ()
:precondition
(and
(Flag326-)
(Flag326prime-)
)
:effect
(and
(Flag368-)
(Flag368prime-)
)

)
(:action Flag368Action-8
:parameters ()
:precondition
(and
(Flag331-)
(Flag331prime-)
)
:effect
(and
(Flag368-)
(Flag368prime-)
)

)
(:action Flag368Action-9
:parameters ()
:precondition
(and
(Flag343-)
(Flag343prime-)
)
:effect
(and
(Flag368-)
(Flag368prime-)
)

)
(:action Flag368Action-10
:parameters ()
:precondition
(and
(Flag323-)
(Flag323prime-)
)
:effect
(and
(Flag368-)
(Flag368prime-)
)

)
(:action Flag368Action-11
:parameters ()
:precondition
(and
(Flag362-)
(Flag362prime-)
)
:effect
(and
(Flag368-)
(Flag368prime-)
)

)
(:action Flag368Action-12
:parameters ()
:precondition
(and
(Flag308-)
(Flag308prime-)
)
:effect
(and
(Flag368-)
(Flag368prime-)
)

)
(:action Flag368Action-13
:parameters ()
:precondition
(and
(Flag229-)
(Flag229prime-)
)
:effect
(and
(Flag368-)
(Flag368prime-)
)

)
(:action Flag368Action-14
:parameters ()
:precondition
(and
(Flag353-)
(Flag353prime-)
)
:effect
(and
(Flag368-)
(Flag368prime-)
)

)
(:action Flag368Action-15
:parameters ()
:precondition
(and
(Flag363-)
(Flag363prime-)
)
:effect
(and
(Flag368-)
(Flag368prime-)
)

)
(:action Flag368Action-16
:parameters ()
:precondition
(and
(Flag246-)
(Flag246prime-)
)
:effect
(and
(Flag368-)
(Flag368prime-)
)

)
(:action Flag368Action-17
:parameters ()
:precondition
(and
(Flag232-)
(Flag232prime-)
)
:effect
(and
(Flag368-)
(Flag368prime-)
)

)
(:action Flag368Action-18
:parameters ()
:precondition
(and
(Flag355-)
(Flag355prime-)
)
:effect
(and
(Flag368-)
(Flag368prime-)
)

)
(:action Flag368Action-19
:parameters ()
:precondition
(and
(Flag307-)
(Flag307prime-)
)
:effect
(and
(Flag368-)
(Flag368prime-)
)

)
(:action Flag368Action-20
:parameters ()
:precondition
(and
(Flag233-)
(Flag233prime-)
)
:effect
(and
(Flag368-)
(Flag368prime-)
)

)
(:action Flag368Action-21
:parameters ()
:precondition
(and
(Flag364-)
(Flag364prime-)
)
:effect
(and
(Flag368-)
(Flag368prime-)
)

)
(:action Flag368Action-22
:parameters ()
:precondition
(and
(Flag321-)
(Flag321prime-)
)
:effect
(and
(Flag368-)
(Flag368prime-)
)

)
(:action Flag368Action-23
:parameters ()
:precondition
(and
(Flag345-)
(Flag345prime-)
)
:effect
(and
(Flag368-)
(Flag368prime-)
)

)
(:action Flag368Action-24
:parameters ()
:precondition
(and
(Flag332-)
(Flag332prime-)
)
:effect
(and
(Flag368-)
(Flag368prime-)
)

)
(:action Flag368Action-25
:parameters ()
:precondition
(and
(Flag242-)
(Flag242prime-)
)
:effect
(and
(Flag368-)
(Flag368prime-)
)

)
(:action Flag368Action-26
:parameters ()
:precondition
(and
(Flag294-)
(Flag294prime-)
)
:effect
(and
(Flag368-)
(Flag368prime-)
)

)
(:action Flag368Action-27
:parameters ()
:precondition
(and
(Flag295-)
(Flag295prime-)
)
:effect
(and
(Flag368-)
(Flag368prime-)
)

)
(:action Flag368Action-28
:parameters ()
:precondition
(and
(Flag333-)
(Flag333prime-)
)
:effect
(and
(Flag368-)
(Flag368prime-)
)

)
(:action Flag368Action-29
:parameters ()
:precondition
(and
(Flag296-)
(Flag296prime-)
)
:effect
(and
(Flag368-)
(Flag368prime-)
)

)
(:action Flag368Action-30
:parameters ()
:precondition
(and
(Flag325-)
(Flag325prime-)
)
:effect
(and
(Flag368-)
(Flag368prime-)
)

)
(:action Flag368Action-31
:parameters ()
:precondition
(and
(Flag238-)
(Flag238prime-)
)
:effect
(and
(Flag368-)
(Flag368prime-)
)

)
(:action Flag368Action-32
:parameters ()
:precondition
(and
(Flag322-)
(Flag322prime-)
)
:effect
(and
(Flag368-)
(Flag368prime-)
)

)
(:action Flag368Action-33
:parameters ()
:precondition
(and
(Flag255-)
(Flag255prime-)
)
:effect
(and
(Flag368-)
(Flag368prime-)
)

)
(:action Flag368Action-34
:parameters ()
:precondition
(and
(Flag365-)
(Flag365prime-)
)
:effect
(and
(Flag368-)
(Flag368prime-)
)

)
(:action Flag368Action-35
:parameters ()
:precondition
(and
(Flag356-)
(Flag356prime-)
)
:effect
(and
(Flag368-)
(Flag368prime-)
)

)
(:action Flag368Action-36
:parameters ()
:precondition
(and
(Flag264-)
(Flag264prime-)
)
:effect
(and
(Flag368-)
(Flag368prime-)
)

)
(:action Flag368Action-37
:parameters ()
:precondition
(and
(Flag282-)
(Flag282prime-)
)
:effect
(and
(Flag368-)
(Flag368prime-)
)

)
(:action Flag368Action-38
:parameters ()
:precondition
(and
(Flag244-)
(Flag244prime-)
)
:effect
(and
(Flag368-)
(Flag368prime-)
)

)
(:action Flag368Action-39
:parameters ()
:precondition
(and
(Flag309-)
(Flag309prime-)
)
:effect
(and
(Flag368-)
(Flag368prime-)
)

)
(:action Flag368Action-40
:parameters ()
:precondition
(and
(Flag252-)
(Flag252prime-)
)
:effect
(and
(Flag368-)
(Flag368prime-)
)

)
(:action Flag368Action-41
:parameters ()
:precondition
(and
(Flag245-)
(Flag245prime-)
)
:effect
(and
(Flag368-)
(Flag368prime-)
)

)
(:action Flag368Action-42
:parameters ()
:precondition
(and
(Flag357-)
(Flag357prime-)
)
:effect
(and
(Flag368-)
(Flag368prime-)
)

)
(:action Flag368Action-43
:parameters ()
:precondition
(and
(Flag269-)
(Flag269prime-)
)
:effect
(and
(Flag368-)
(Flag368prime-)
)

)
(:action Flag368Action-44
:parameters ()
:precondition
(and
(Flag285-)
(Flag285prime-)
)
:effect
(and
(Flag368-)
(Flag368prime-)
)

)
(:action Flag368Action-45
:parameters ()
:precondition
(and
(Flag292-)
(Flag292prime-)
)
:effect
(and
(Flag368-)
(Flag368prime-)
)

)
(:action Flag368Action-46
:parameters ()
:precondition
(and
(Flag324-)
(Flag324prime-)
)
:effect
(and
(Flag368-)
(Flag368prime-)
)

)
(:action Flag368Action-47
:parameters ()
:precondition
(and
(Flag366-)
(Flag366prime-)
)
:effect
(and
(Flag368-)
(Flag368prime-)
)

)
(:action Flag368Action-48
:parameters ()
:precondition
(and
(Flag249-)
(Flag249prime-)
)
:effect
(and
(Flag368-)
(Flag368prime-)
)

)
(:action Flag368Action-49
:parameters ()
:precondition
(and
(Flag286-)
(Flag286prime-)
)
:effect
(and
(Flag368-)
(Flag368prime-)
)

)
(:action Flag368Action-50
:parameters ()
:precondition
(and
(Flag346-)
(Flag346prime-)
)
:effect
(and
(Flag368-)
(Flag368prime-)
)

)
(:action Flag368Action-51
:parameters ()
:precondition
(and
(Flag359-)
(Flag359prime-)
)
:effect
(and
(Flag368-)
(Flag368prime-)
)

)
(:action Flag368Action-52
:parameters ()
:precondition
(and
(Flag287-)
(Flag287prime-)
)
:effect
(and
(Flag368-)
(Flag368prime-)
)

)
(:action Flag368Action-53
:parameters ()
:precondition
(and
(Flag271-)
(Flag271prime-)
)
:effect
(and
(Flag368-)
(Flag368prime-)
)

)
(:action Flag368Action-54
:parameters ()
:precondition
(and
(Flag288-)
(Flag288prime-)
)
:effect
(and
(Flag368-)
(Flag368prime-)
)

)
(:action Flag368Action-55
:parameters ()
:precondition
(and
(Flag367-)
(Flag367prime-)
)
:effect
(and
(Flag368-)
(Flag368prime-)
)

)
(:action Flag368Action-56
:parameters ()
:precondition
(and
(Flag348-)
(Flag348prime-)
)
:effect
(and
(Flag368-)
(Flag368prime-)
)

)
(:action Flag368Action-57
:parameters ()
:precondition
(and
(Flag272-)
(Flag272prime-)
)
:effect
(and
(Flag368-)
(Flag368prime-)
)

)
(:action Flag368Action-58
:parameters ()
:precondition
(and
(Flag349-)
(Flag349prime-)
)
:effect
(and
(Flag368-)
(Flag368prime-)
)

)
(:action Flag368Action-59
:parameters ()
:precondition
(and
(Flag329-)
(Flag329prime-)
)
:effect
(and
(Flag368-)
(Flag368prime-)
)

)
(:action Flag368Action-60
:parameters ()
:precondition
(and
(Flag253-)
(Flag253prime-)
)
:effect
(and
(Flag368-)
(Flag368prime-)
)

)
(:action Flag368Action-61
:parameters ()
:precondition
(and
(Flag273-)
(Flag273prime-)
)
:effect
(and
(Flag368-)
(Flag368prime-)
)

)
(:action Flag368Action-62
:parameters ()
:precondition
(and
(Flag300-)
(Flag300prime-)
)
:effect
(and
(Flag368-)
(Flag368prime-)
)

)
(:action Flag368Action-63
:parameters ()
:precondition
(and
(Flag351-)
(Flag351prime-)
)
:effect
(and
(Flag368-)
(Flag368prime-)
)

)
(:action Flag368Action-64
:parameters ()
:precondition
(and
(Flag360-)
(Flag360prime-)
)
:effect
(and
(Flag368-)
(Flag368prime-)
)

)
(:action Flag368Action-65
:parameters ()
:precondition
(and
(Flag338-)
(Flag338prime-)
)
:effect
(and
(Flag368-)
(Flag368prime-)
)

)
(:action Flag368Action-66
:parameters ()
:precondition
(and
(Flag339-)
(Flag339prime-)
)
:effect
(and
(Flag368-)
(Flag368prime-)
)

)
(:action Flag368Action-67
:parameters ()
:precondition
(and
(Flag340-)
(Flag340prime-)
)
:effect
(and
(Flag368-)
(Flag368prime-)
)

)
(:action Flag368Action-68
:parameters ()
:precondition
(and
(Flag302-)
(Flag302prime-)
)
:effect
(and
(Flag368-)
(Flag368prime-)
)

)
(:action Prim368Action
:parameters ()
:precondition
(and
(not (Flag262-))
(Flag262prime-)
(not (Flag342-))
(Flag342prime-)
(not (Flag289-))
(Flag289prime-)
(not (Flag304-))
(Flag304prime-)
(not (Flag275-))
(Flag275prime-)
(not (Flag315-))
(Flag315prime-)
(not (Flag306-))
(Flag306prime-)
(not (Flag326-))
(Flag326prime-)
(not (Flag331-))
(Flag331prime-)
(not (Flag343-))
(Flag343prime-)
(not (Flag323-))
(Flag323prime-)
(not (Flag362-))
(Flag362prime-)
(not (Flag308-))
(Flag308prime-)
(not (Flag229-))
(Flag229prime-)
(not (Flag353-))
(Flag353prime-)
(not (Flag363-))
(Flag363prime-)
(not (Flag246-))
(Flag246prime-)
(not (Flag232-))
(Flag232prime-)
(not (Flag355-))
(Flag355prime-)
(not (Flag307-))
(Flag307prime-)
(not (Flag233-))
(Flag233prime-)
(not (Flag364-))
(Flag364prime-)
(not (Flag321-))
(Flag321prime-)
(not (Flag345-))
(Flag345prime-)
(not (Flag332-))
(Flag332prime-)
(not (Flag242-))
(Flag242prime-)
(not (Flag294-))
(Flag294prime-)
(not (Flag295-))
(Flag295prime-)
(not (Flag333-))
(Flag333prime-)
(not (Flag296-))
(Flag296prime-)
(not (Flag325-))
(Flag325prime-)
(not (Flag238-))
(Flag238prime-)
(not (Flag322-))
(Flag322prime-)
(not (Flag255-))
(Flag255prime-)
(not (Flag365-))
(Flag365prime-)
(not (Flag356-))
(Flag356prime-)
(not (Flag264-))
(Flag264prime-)
(not (Flag282-))
(Flag282prime-)
(not (Flag244-))
(Flag244prime-)
(not (Flag309-))
(Flag309prime-)
(not (Flag252-))
(Flag252prime-)
(not (Flag245-))
(Flag245prime-)
(not (Flag357-))
(Flag357prime-)
(not (Flag269-))
(Flag269prime-)
(not (Flag285-))
(Flag285prime-)
(not (Flag292-))
(Flag292prime-)
(not (Flag324-))
(Flag324prime-)
(not (Flag366-))
(Flag366prime-)
(not (Flag249-))
(Flag249prime-)
(not (Flag286-))
(Flag286prime-)
(not (Flag346-))
(Flag346prime-)
(not (Flag359-))
(Flag359prime-)
(not (Flag287-))
(Flag287prime-)
(not (Flag271-))
(Flag271prime-)
(not (Flag288-))
(Flag288prime-)
(not (Flag367-))
(Flag367prime-)
(not (Flag348-))
(Flag348prime-)
(not (Flag272-))
(Flag272prime-)
(not (Flag349-))
(Flag349prime-)
(not (Flag329-))
(Flag329prime-)
(not (Flag253-))
(Flag253prime-)
(not (Flag273-))
(Flag273prime-)
(not (Flag300-))
(Flag300prime-)
(not (Flag351-))
(Flag351prime-)
(not (Flag360-))
(Flag360prime-)
(not (Flag338-))
(Flag338prime-)
(not (Flag339-))
(Flag339prime-)
(not (Flag340-))
(Flag340prime-)
(not (Flag302-))
(Flag302prime-)
)
:effect
(and
(Flag368prime-)
(not (Flag368-))
)

)
(:action Flag374Action-0
:parameters ()
:precondition
(and
(Flag342-)
(Flag342prime-)
)
:effect
(and
(Flag374-)
(Flag374prime-)
)

)
(:action Flag374Action-1
:parameters ()
:precondition
(and
(Flag289-)
(Flag289prime-)
)
:effect
(and
(Flag374-)
(Flag374prime-)
)

)
(:action Flag374Action-2
:parameters ()
:precondition
(and
(Flag304-)
(Flag304prime-)
)
:effect
(and
(Flag374-)
(Flag374prime-)
)

)
(:action Flag374Action-3
:parameters ()
:precondition
(and
(Flag275-)
(Flag275prime-)
)
:effect
(and
(Flag374-)
(Flag374prime-)
)

)
(:action Flag374Action-4
:parameters ()
:precondition
(and
(Flag315-)
(Flag315prime-)
)
:effect
(and
(Flag374-)
(Flag374prime-)
)

)
(:action Flag374Action-5
:parameters ()
:precondition
(and
(Flag306-)
(Flag306prime-)
)
:effect
(and
(Flag374-)
(Flag374prime-)
)

)
(:action Flag374Action-6
:parameters ()
:precondition
(and
(Flag326-)
(Flag326prime-)
)
:effect
(and
(Flag374-)
(Flag374prime-)
)

)
(:action Flag374Action-7
:parameters ()
:precondition
(and
(Flag331-)
(Flag331prime-)
)
:effect
(and
(Flag374-)
(Flag374prime-)
)

)
(:action Flag374Action-8
:parameters ()
:precondition
(and
(Flag323-)
(Flag323prime-)
)
:effect
(and
(Flag374-)
(Flag374prime-)
)

)
(:action Flag374Action-9
:parameters ()
:precondition
(and
(Flag307-)
(Flag307prime-)
)
:effect
(and
(Flag374-)
(Flag374prime-)
)

)
(:action Flag374Action-10
:parameters ()
:precondition
(and
(Flag308-)
(Flag308prime-)
)
:effect
(and
(Flag374-)
(Flag374prime-)
)

)
(:action Flag374Action-11
:parameters ()
:precondition
(and
(Flag369-)
(Flag369prime-)
)
:effect
(and
(Flag374-)
(Flag374prime-)
)

)
(:action Flag374Action-12
:parameters ()
:precondition
(and
(Flag229-)
(Flag229prime-)
)
:effect
(and
(Flag374-)
(Flag374prime-)
)

)
(:action Flag374Action-13
:parameters ()
:precondition
(and
(Flag353-)
(Flag353prime-)
)
:effect
(and
(Flag374-)
(Flag374prime-)
)

)
(:action Flag374Action-14
:parameters ()
:precondition
(and
(Flag363-)
(Flag363prime-)
)
:effect
(and
(Flag374-)
(Flag374prime-)
)

)
(:action Flag374Action-15
:parameters ()
:precondition
(and
(Flag370-)
(Flag370prime-)
)
:effect
(and
(Flag374-)
(Flag374prime-)
)

)
(:action Flag374Action-16
:parameters ()
:precondition
(and
(Flag232-)
(Flag232prime-)
)
:effect
(and
(Flag374-)
(Flag374prime-)
)

)
(:action Flag374Action-17
:parameters ()
:precondition
(and
(Flag355-)
(Flag355prime-)
)
:effect
(and
(Flag374-)
(Flag374prime-)
)

)
(:action Flag374Action-18
:parameters ()
:precondition
(and
(Flag233-)
(Flag233prime-)
)
:effect
(and
(Flag374-)
(Flag374prime-)
)

)
(:action Flag374Action-19
:parameters ()
:precondition
(and
(Flag364-)
(Flag364prime-)
)
:effect
(and
(Flag374-)
(Flag374prime-)
)

)
(:action Flag374Action-20
:parameters ()
:precondition
(and
(Flag238-)
(Flag238prime-)
)
:effect
(and
(Flag374-)
(Flag374prime-)
)

)
(:action Flag374Action-21
:parameters ()
:precondition
(and
(Flag300-)
(Flag300prime-)
)
:effect
(and
(Flag374-)
(Flag374prime-)
)

)
(:action Flag374Action-22
:parameters ()
:precondition
(and
(Flag345-)
(Flag345prime-)
)
:effect
(and
(Flag374-)
(Flag374prime-)
)

)
(:action Flag374Action-23
:parameters ()
:precondition
(and
(Flag242-)
(Flag242prime-)
)
:effect
(and
(Flag374-)
(Flag374prime-)
)

)
(:action Flag374Action-24
:parameters ()
:precondition
(and
(Flag294-)
(Flag294prime-)
)
:effect
(and
(Flag374-)
(Flag374prime-)
)

)
(:action Flag374Action-25
:parameters ()
:precondition
(and
(Flag333-)
(Flag333prime-)
)
:effect
(and
(Flag374-)
(Flag374prime-)
)

)
(:action Flag374Action-26
:parameters ()
:precondition
(and
(Flag296-)
(Flag296prime-)
)
:effect
(and
(Flag374-)
(Flag374prime-)
)

)
(:action Flag374Action-27
:parameters ()
:precondition
(and
(Flag325-)
(Flag325prime-)
)
:effect
(and
(Flag374-)
(Flag374prime-)
)

)
(:action Flag374Action-28
:parameters ()
:precondition
(and
(Flag371-)
(Flag371prime-)
)
:effect
(and
(Flag374-)
(Flag374prime-)
)

)
(:action Flag374Action-29
:parameters ()
:precondition
(and
(Flag255-)
(Flag255prime-)
)
:effect
(and
(Flag374-)
(Flag374prime-)
)

)
(:action Flag374Action-30
:parameters ()
:precondition
(and
(Flag365-)
(Flag365prime-)
)
:effect
(and
(Flag374-)
(Flag374prime-)
)

)
(:action Flag374Action-31
:parameters ()
:precondition
(and
(Flag372-)
(Flag372prime-)
)
:effect
(and
(Flag374-)
(Flag374prime-)
)

)
(:action Flag374Action-32
:parameters ()
:precondition
(and
(Flag264-)
(Flag264prime-)
)
:effect
(and
(Flag374-)
(Flag374prime-)
)

)
(:action Flag374Action-33
:parameters ()
:precondition
(and
(Flag282-)
(Flag282prime-)
)
:effect
(and
(Flag374-)
(Flag374prime-)
)

)
(:action Flag374Action-34
:parameters ()
:precondition
(and
(Flag244-)
(Flag244prime-)
)
:effect
(and
(Flag374-)
(Flag374prime-)
)

)
(:action Flag374Action-35
:parameters ()
:precondition
(and
(Flag252-)
(Flag252prime-)
)
:effect
(and
(Flag374-)
(Flag374prime-)
)

)
(:action Flag374Action-36
:parameters ()
:precondition
(and
(Flag357-)
(Flag357prime-)
)
:effect
(and
(Flag374-)
(Flag374prime-)
)

)
(:action Flag374Action-37
:parameters ()
:precondition
(and
(Flag246-)
(Flag246prime-)
)
:effect
(and
(Flag374-)
(Flag374prime-)
)

)
(:action Flag374Action-38
:parameters ()
:precondition
(and
(Flag292-)
(Flag292prime-)
)
:effect
(and
(Flag374-)
(Flag374prime-)
)

)
(:action Flag374Action-39
:parameters ()
:precondition
(and
(Flag324-)
(Flag324prime-)
)
:effect
(and
(Flag374-)
(Flag374prime-)
)

)
(:action Flag374Action-40
:parameters ()
:precondition
(and
(Flag309-)
(Flag309prime-)
)
:effect
(and
(Flag374-)
(Flag374prime-)
)

)
(:action Flag374Action-41
:parameters ()
:precondition
(and
(Flag286-)
(Flag286prime-)
)
:effect
(and
(Flag374-)
(Flag374prime-)
)

)
(:action Flag374Action-42
:parameters ()
:precondition
(and
(Flag346-)
(Flag346prime-)
)
:effect
(and
(Flag374-)
(Flag374prime-)
)

)
(:action Flag374Action-43
:parameters ()
:precondition
(and
(Flag359-)
(Flag359prime-)
)
:effect
(and
(Flag374-)
(Flag374prime-)
)

)
(:action Flag374Action-44
:parameters ()
:precondition
(and
(Flag287-)
(Flag287prime-)
)
:effect
(and
(Flag374-)
(Flag374prime-)
)

)
(:action Flag374Action-45
:parameters ()
:precondition
(and
(Flag366-)
(Flag366prime-)
)
:effect
(and
(Flag374-)
(Flag374prime-)
)

)
(:action Flag374Action-46
:parameters ()
:precondition
(and
(Flag288-)
(Flag288prime-)
)
:effect
(and
(Flag374-)
(Flag374prime-)
)

)
(:action Flag374Action-47
:parameters ()
:precondition
(and
(Flag271-)
(Flag271prime-)
)
:effect
(and
(Flag374-)
(Flag374prime-)
)

)
(:action Flag374Action-48
:parameters ()
:precondition
(and
(Flag367-)
(Flag367prime-)
)
:effect
(and
(Flag374-)
(Flag374prime-)
)

)
(:action Flag374Action-49
:parameters ()
:precondition
(and
(Flag348-)
(Flag348prime-)
)
:effect
(and
(Flag374-)
(Flag374prime-)
)

)
(:action Flag374Action-50
:parameters ()
:precondition
(and
(Flag272-)
(Flag272prime-)
)
:effect
(and
(Flag374-)
(Flag374prime-)
)

)
(:action Flag374Action-51
:parameters ()
:precondition
(and
(Flag349-)
(Flag349prime-)
)
:effect
(and
(Flag374-)
(Flag374prime-)
)

)
(:action Flag374Action-52
:parameters ()
:precondition
(and
(Flag329-)
(Flag329prime-)
)
:effect
(and
(Flag374-)
(Flag374prime-)
)

)
(:action Flag374Action-53
:parameters ()
:precondition
(and
(Flag253-)
(Flag253prime-)
)
:effect
(and
(Flag374-)
(Flag374prime-)
)

)
(:action Flag374Action-54
:parameters ()
:precondition
(and
(Flag262-)
(Flag262prime-)
)
:effect
(and
(Flag374-)
(Flag374prime-)
)

)
(:action Flag374Action-55
:parameters ()
:precondition
(and
(Flag373-)
(Flag373prime-)
)
:effect
(and
(Flag374-)
(Flag374prime-)
)

)
(:action Flag374Action-56
:parameters ()
:precondition
(and
(Flag351-)
(Flag351prime-)
)
:effect
(and
(Flag374-)
(Flag374prime-)
)

)
(:action Flag374Action-57
:parameters ()
:precondition
(and
(Flag360-)
(Flag360prime-)
)
:effect
(and
(Flag374-)
(Flag374prime-)
)

)
(:action Flag374Action-58
:parameters ()
:precondition
(and
(Flag338-)
(Flag338prime-)
)
:effect
(and
(Flag374-)
(Flag374prime-)
)

)
(:action Flag374Action-59
:parameters ()
:precondition
(and
(Flag339-)
(Flag339prime-)
)
:effect
(and
(Flag374-)
(Flag374prime-)
)

)
(:action Flag374Action-60
:parameters ()
:precondition
(and
(Flag340-)
(Flag340prime-)
)
:effect
(and
(Flag374-)
(Flag374prime-)
)

)
(:action Flag374Action-61
:parameters ()
:precondition
(and
(Flag302-)
(Flag302prime-)
)
:effect
(and
(Flag374-)
(Flag374prime-)
)

)
(:action Prim374Action
:parameters ()
:precondition
(and
(not (Flag342-))
(Flag342prime-)
(not (Flag289-))
(Flag289prime-)
(not (Flag304-))
(Flag304prime-)
(not (Flag275-))
(Flag275prime-)
(not (Flag315-))
(Flag315prime-)
(not (Flag306-))
(Flag306prime-)
(not (Flag326-))
(Flag326prime-)
(not (Flag331-))
(Flag331prime-)
(not (Flag323-))
(Flag323prime-)
(not (Flag307-))
(Flag307prime-)
(not (Flag308-))
(Flag308prime-)
(not (Flag369-))
(Flag369prime-)
(not (Flag229-))
(Flag229prime-)
(not (Flag353-))
(Flag353prime-)
(not (Flag363-))
(Flag363prime-)
(not (Flag370-))
(Flag370prime-)
(not (Flag232-))
(Flag232prime-)
(not (Flag355-))
(Flag355prime-)
(not (Flag233-))
(Flag233prime-)
(not (Flag364-))
(Flag364prime-)
(not (Flag238-))
(Flag238prime-)
(not (Flag300-))
(Flag300prime-)
(not (Flag345-))
(Flag345prime-)
(not (Flag242-))
(Flag242prime-)
(not (Flag294-))
(Flag294prime-)
(not (Flag333-))
(Flag333prime-)
(not (Flag296-))
(Flag296prime-)
(not (Flag325-))
(Flag325prime-)
(not (Flag371-))
(Flag371prime-)
(not (Flag255-))
(Flag255prime-)
(not (Flag365-))
(Flag365prime-)
(not (Flag372-))
(Flag372prime-)
(not (Flag264-))
(Flag264prime-)
(not (Flag282-))
(Flag282prime-)
(not (Flag244-))
(Flag244prime-)
(not (Flag252-))
(Flag252prime-)
(not (Flag357-))
(Flag357prime-)
(not (Flag246-))
(Flag246prime-)
(not (Flag292-))
(Flag292prime-)
(not (Flag324-))
(Flag324prime-)
(not (Flag309-))
(Flag309prime-)
(not (Flag286-))
(Flag286prime-)
(not (Flag346-))
(Flag346prime-)
(not (Flag359-))
(Flag359prime-)
(not (Flag287-))
(Flag287prime-)
(not (Flag366-))
(Flag366prime-)
(not (Flag288-))
(Flag288prime-)
(not (Flag271-))
(Flag271prime-)
(not (Flag367-))
(Flag367prime-)
(not (Flag348-))
(Flag348prime-)
(not (Flag272-))
(Flag272prime-)
(not (Flag349-))
(Flag349prime-)
(not (Flag329-))
(Flag329prime-)
(not (Flag253-))
(Flag253prime-)
(not (Flag262-))
(Flag262prime-)
(not (Flag373-))
(Flag373prime-)
(not (Flag351-))
(Flag351prime-)
(not (Flag360-))
(Flag360prime-)
(not (Flag338-))
(Flag338prime-)
(not (Flag339-))
(Flag339prime-)
(not (Flag340-))
(Flag340prime-)
(not (Flag302-))
(Flag302prime-)
)
:effect
(and
(Flag374prime-)
(not (Flag374-))
)

)
(:action Flag380Action-0
:parameters ()
:precondition
(and
(Flag342-)
(Flag342prime-)
)
:effect
(and
(Flag380-)
(Flag380prime-)
)

)
(:action Flag380Action-1
:parameters ()
:precondition
(and
(Flag375-)
(Flag375prime-)
)
:effect
(and
(Flag380-)
(Flag380prime-)
)

)
(:action Flag380Action-2
:parameters ()
:precondition
(and
(Flag272-)
(Flag272prime-)
)
:effect
(and
(Flag380-)
(Flag380prime-)
)

)
(:action Flag380Action-3
:parameters ()
:precondition
(and
(Flag304-)
(Flag304prime-)
)
:effect
(and
(Flag380-)
(Flag380prime-)
)

)
(:action Flag380Action-4
:parameters ()
:precondition
(and
(Flag275-)
(Flag275prime-)
)
:effect
(and
(Flag380-)
(Flag380prime-)
)

)
(:action Flag380Action-5
:parameters ()
:precondition
(and
(Flag315-)
(Flag315prime-)
)
:effect
(and
(Flag380-)
(Flag380prime-)
)

)
(:action Flag380Action-6
:parameters ()
:precondition
(and
(Flag326-)
(Flag326prime-)
)
:effect
(and
(Flag380-)
(Flag380prime-)
)

)
(:action Flag380Action-7
:parameters ()
:precondition
(and
(Flag331-)
(Flag331prime-)
)
:effect
(and
(Flag380-)
(Flag380prime-)
)

)
(:action Flag380Action-8
:parameters ()
:precondition
(and
(Flag376-)
(Flag376prime-)
)
:effect
(and
(Flag380-)
(Flag380prime-)
)

)
(:action Flag380Action-9
:parameters ()
:precondition
(and
(Flag323-)
(Flag323prime-)
)
:effect
(and
(Flag380-)
(Flag380prime-)
)

)
(:action Flag380Action-10
:parameters ()
:precondition
(and
(Flag307-)
(Flag307prime-)
)
:effect
(and
(Flag380-)
(Flag380prime-)
)

)
(:action Flag380Action-11
:parameters ()
:precondition
(and
(Flag308-)
(Flag308prime-)
)
:effect
(and
(Flag380-)
(Flag380prime-)
)

)
(:action Flag380Action-12
:parameters ()
:precondition
(and
(Flag369-)
(Flag369prime-)
)
:effect
(and
(Flag380-)
(Flag380prime-)
)

)
(:action Flag380Action-13
:parameters ()
:precondition
(and
(Flag353-)
(Flag353prime-)
)
:effect
(and
(Flag380-)
(Flag380prime-)
)

)
(:action Flag380Action-14
:parameters ()
:precondition
(and
(Flag232-)
(Flag232prime-)
)
:effect
(and
(Flag380-)
(Flag380prime-)
)

)
(:action Flag380Action-15
:parameters ()
:precondition
(and
(Flag355-)
(Flag355prime-)
)
:effect
(and
(Flag380-)
(Flag380prime-)
)

)
(:action Flag380Action-16
:parameters ()
:precondition
(and
(Flag233-)
(Flag233prime-)
)
:effect
(and
(Flag380-)
(Flag380prime-)
)

)
(:action Flag380Action-17
:parameters ()
:precondition
(and
(Flag364-)
(Flag364prime-)
)
:effect
(and
(Flag380-)
(Flag380prime-)
)

)
(:action Flag380Action-18
:parameters ()
:precondition
(and
(Flag300-)
(Flag300prime-)
)
:effect
(and
(Flag380-)
(Flag380prime-)
)

)
(:action Flag380Action-19
:parameters ()
:precondition
(and
(Flag345-)
(Flag345prime-)
)
:effect
(and
(Flag380-)
(Flag380prime-)
)

)
(:action Flag380Action-20
:parameters ()
:precondition
(and
(Flag242-)
(Flag242prime-)
)
:effect
(and
(Flag380-)
(Flag380prime-)
)

)
(:action Flag380Action-21
:parameters ()
:precondition
(and
(Flag294-)
(Flag294prime-)
)
:effect
(and
(Flag380-)
(Flag380prime-)
)

)
(:action Flag380Action-22
:parameters ()
:precondition
(and
(Flag377-)
(Flag377prime-)
)
:effect
(and
(Flag380-)
(Flag380prime-)
)

)
(:action Flag380Action-23
:parameters ()
:precondition
(and
(Flag325-)
(Flag325prime-)
)
:effect
(and
(Flag380-)
(Flag380prime-)
)

)
(:action Flag380Action-24
:parameters ()
:precondition
(and
(Flag371-)
(Flag371prime-)
)
:effect
(and
(Flag380-)
(Flag380prime-)
)

)
(:action Flag380Action-25
:parameters ()
:precondition
(and
(Flag255-)
(Flag255prime-)
)
:effect
(and
(Flag380-)
(Flag380prime-)
)

)
(:action Flag380Action-26
:parameters ()
:precondition
(and
(Flag365-)
(Flag365prime-)
)
:effect
(and
(Flag380-)
(Flag380prime-)
)

)
(:action Flag380Action-27
:parameters ()
:precondition
(and
(Flag378-)
(Flag378prime-)
)
:effect
(and
(Flag380-)
(Flag380prime-)
)

)
(:action Flag380Action-28
:parameters ()
:precondition
(and
(Flag264-)
(Flag264prime-)
)
:effect
(and
(Flag380-)
(Flag380prime-)
)

)
(:action Flag380Action-29
:parameters ()
:precondition
(and
(Flag282-)
(Flag282prime-)
)
:effect
(and
(Flag380-)
(Flag380prime-)
)

)
(:action Flag380Action-30
:parameters ()
:precondition
(and
(Flag244-)
(Flag244prime-)
)
:effect
(and
(Flag380-)
(Flag380prime-)
)

)
(:action Flag380Action-31
:parameters ()
:precondition
(and
(Flag289-)
(Flag289prime-)
)
:effect
(and
(Flag380-)
(Flag380prime-)
)

)
(:action Flag380Action-32
:parameters ()
:precondition
(and
(Flag357-)
(Flag357prime-)
)
:effect
(and
(Flag380-)
(Flag380prime-)
)

)
(:action Flag380Action-33
:parameters ()
:precondition
(and
(Flag292-)
(Flag292prime-)
)
:effect
(and
(Flag380-)
(Flag380prime-)
)

)
(:action Flag380Action-34
:parameters ()
:precondition
(and
(Flag324-)
(Flag324prime-)
)
:effect
(and
(Flag380-)
(Flag380prime-)
)

)
(:action Flag380Action-35
:parameters ()
:precondition
(and
(Flag309-)
(Flag309prime-)
)
:effect
(and
(Flag380-)
(Flag380prime-)
)

)
(:action Flag380Action-36
:parameters ()
:precondition
(and
(Flag286-)
(Flag286prime-)
)
:effect
(and
(Flag380-)
(Flag380prime-)
)

)
(:action Flag380Action-37
:parameters ()
:precondition
(and
(Flag346-)
(Flag346prime-)
)
:effect
(and
(Flag380-)
(Flag380prime-)
)

)
(:action Flag380Action-38
:parameters ()
:precondition
(and
(Flag252-)
(Flag252prime-)
)
:effect
(and
(Flag380-)
(Flag380prime-)
)

)
(:action Flag380Action-39
:parameters ()
:precondition
(and
(Flag366-)
(Flag366prime-)
)
:effect
(and
(Flag380-)
(Flag380prime-)
)

)
(:action Flag380Action-40
:parameters ()
:precondition
(and
(Flag253-)
(Flag253prime-)
)
:effect
(and
(Flag380-)
(Flag380prime-)
)

)
(:action Flag380Action-41
:parameters ()
:precondition
(and
(Flag271-)
(Flag271prime-)
)
:effect
(and
(Flag380-)
(Flag380prime-)
)

)
(:action Flag380Action-42
:parameters ()
:precondition
(and
(Flag367-)
(Flag367prime-)
)
:effect
(and
(Flag380-)
(Flag380prime-)
)

)
(:action Flag380Action-43
:parameters ()
:precondition
(and
(Flag348-)
(Flag348prime-)
)
:effect
(and
(Flag380-)
(Flag380prime-)
)

)
(:action Flag380Action-44
:parameters ()
:precondition
(and
(Flag379-)
(Flag379prime-)
)
:effect
(and
(Flag380-)
(Flag380prime-)
)

)
(:action Flag380Action-45
:parameters ()
:precondition
(and
(Flag238-)
(Flag238prime-)
)
:effect
(and
(Flag380-)
(Flag380prime-)
)

)
(:action Flag380Action-46
:parameters ()
:precondition
(and
(Flag262-)
(Flag262prime-)
)
:effect
(and
(Flag380-)
(Flag380prime-)
)

)
(:action Flag380Action-47
:parameters ()
:precondition
(and
(Flag373-)
(Flag373prime-)
)
:effect
(and
(Flag380-)
(Flag380prime-)
)

)
(:action Flag380Action-48
:parameters ()
:precondition
(and
(Flag351-)
(Flag351prime-)
)
:effect
(and
(Flag380-)
(Flag380prime-)
)

)
(:action Flag380Action-49
:parameters ()
:precondition
(and
(Flag360-)
(Flag360prime-)
)
:effect
(and
(Flag380-)
(Flag380prime-)
)

)
(:action Flag380Action-50
:parameters ()
:precondition
(and
(Flag338-)
(Flag338prime-)
)
:effect
(and
(Flag380-)
(Flag380prime-)
)

)
(:action Flag380Action-51
:parameters ()
:precondition
(and
(Flag340-)
(Flag340prime-)
)
:effect
(and
(Flag380-)
(Flag380prime-)
)

)
(:action Flag380Action-52
:parameters ()
:precondition
(and
(Flag302-)
(Flag302prime-)
)
:effect
(and
(Flag380-)
(Flag380prime-)
)

)
(:action Prim380Action
:parameters ()
:precondition
(and
(not (Flag342-))
(Flag342prime-)
(not (Flag375-))
(Flag375prime-)
(not (Flag272-))
(Flag272prime-)
(not (Flag304-))
(Flag304prime-)
(not (Flag275-))
(Flag275prime-)
(not (Flag315-))
(Flag315prime-)
(not (Flag326-))
(Flag326prime-)
(not (Flag331-))
(Flag331prime-)
(not (Flag376-))
(Flag376prime-)
(not (Flag323-))
(Flag323prime-)
(not (Flag307-))
(Flag307prime-)
(not (Flag308-))
(Flag308prime-)
(not (Flag369-))
(Flag369prime-)
(not (Flag353-))
(Flag353prime-)
(not (Flag232-))
(Flag232prime-)
(not (Flag355-))
(Flag355prime-)
(not (Flag233-))
(Flag233prime-)
(not (Flag364-))
(Flag364prime-)
(not (Flag300-))
(Flag300prime-)
(not (Flag345-))
(Flag345prime-)
(not (Flag242-))
(Flag242prime-)
(not (Flag294-))
(Flag294prime-)
(not (Flag377-))
(Flag377prime-)
(not (Flag325-))
(Flag325prime-)
(not (Flag371-))
(Flag371prime-)
(not (Flag255-))
(Flag255prime-)
(not (Flag365-))
(Flag365prime-)
(not (Flag378-))
(Flag378prime-)
(not (Flag264-))
(Flag264prime-)
(not (Flag282-))
(Flag282prime-)
(not (Flag244-))
(Flag244prime-)
(not (Flag289-))
(Flag289prime-)
(not (Flag357-))
(Flag357prime-)
(not (Flag292-))
(Flag292prime-)
(not (Flag324-))
(Flag324prime-)
(not (Flag309-))
(Flag309prime-)
(not (Flag286-))
(Flag286prime-)
(not (Flag346-))
(Flag346prime-)
(not (Flag252-))
(Flag252prime-)
(not (Flag366-))
(Flag366prime-)
(not (Flag253-))
(Flag253prime-)
(not (Flag271-))
(Flag271prime-)
(not (Flag367-))
(Flag367prime-)
(not (Flag348-))
(Flag348prime-)
(not (Flag379-))
(Flag379prime-)
(not (Flag238-))
(Flag238prime-)
(not (Flag262-))
(Flag262prime-)
(not (Flag373-))
(Flag373prime-)
(not (Flag351-))
(Flag351prime-)
(not (Flag360-))
(Flag360prime-)
(not (Flag338-))
(Flag338prime-)
(not (Flag340-))
(Flag340prime-)
(not (Flag302-))
(Flag302prime-)
)
:effect
(and
(Flag380prime-)
(not (Flag380-))
)

)
(:action Flag385Action-0
:parameters ()
:precondition
(and
(Flag375-)
(Flag375prime-)
)
:effect
(and
(Flag385-)
(Flag385prime-)
)

)
(:action Flag385Action-1
:parameters ()
:precondition
(and
(Flag304-)
(Flag304prime-)
)
:effect
(and
(Flag385-)
(Flag385prime-)
)

)
(:action Flag385Action-2
:parameters ()
:precondition
(and
(Flag275-)
(Flag275prime-)
)
:effect
(and
(Flag385-)
(Flag385prime-)
)

)
(:action Flag385Action-3
:parameters ()
:precondition
(and
(Flag326-)
(Flag326prime-)
)
:effect
(and
(Flag385-)
(Flag385prime-)
)

)
(:action Flag385Action-4
:parameters ()
:precondition
(and
(Flag331-)
(Flag331prime-)
)
:effect
(and
(Flag385-)
(Flag385prime-)
)

)
(:action Flag385Action-5
:parameters ()
:precondition
(and
(Flag323-)
(Flag323prime-)
)
:effect
(and
(Flag385-)
(Flag385prime-)
)

)
(:action Flag385Action-6
:parameters ()
:precondition
(and
(Flag308-)
(Flag308prime-)
)
:effect
(and
(Flag385-)
(Flag385prime-)
)

)
(:action Flag385Action-7
:parameters ()
:precondition
(and
(Flag369-)
(Flag369prime-)
)
:effect
(and
(Flag385-)
(Flag385prime-)
)

)
(:action Flag385Action-8
:parameters ()
:precondition
(and
(Flag381-)
(Flag381prime-)
)
:effect
(and
(Flag385-)
(Flag385prime-)
)

)
(:action Flag385Action-9
:parameters ()
:precondition
(and
(Flag353-)
(Flag353prime-)
)
:effect
(and
(Flag385-)
(Flag385prime-)
)

)
(:action Flag385Action-10
:parameters ()
:precondition
(and
(Flag232-)
(Flag232prime-)
)
:effect
(and
(Flag385-)
(Flag385prime-)
)

)
(:action Flag385Action-11
:parameters ()
:precondition
(and
(Flag355-)
(Flag355prime-)
)
:effect
(and
(Flag385-)
(Flag385prime-)
)

)
(:action Flag385Action-12
:parameters ()
:precondition
(and
(Flag382-)
(Flag382prime-)
)
:effect
(and
(Flag385-)
(Flag385prime-)
)

)
(:action Flag385Action-13
:parameters ()
:precondition
(and
(Flag233-)
(Flag233prime-)
)
:effect
(and
(Flag385-)
(Flag385prime-)
)

)
(:action Flag385Action-14
:parameters ()
:precondition
(and
(Flag364-)
(Flag364prime-)
)
:effect
(and
(Flag385-)
(Flag385prime-)
)

)
(:action Flag385Action-15
:parameters ()
:precondition
(and
(Flag345-)
(Flag345prime-)
)
:effect
(and
(Flag385-)
(Flag385prime-)
)

)
(:action Flag385Action-16
:parameters ()
:precondition
(and
(Flag242-)
(Flag242prime-)
)
:effect
(and
(Flag385-)
(Flag385prime-)
)

)
(:action Flag385Action-17
:parameters ()
:precondition
(and
(Flag383-)
(Flag383prime-)
)
:effect
(and
(Flag385-)
(Flag385prime-)
)

)
(:action Flag385Action-18
:parameters ()
:precondition
(and
(Flag377-)
(Flag377prime-)
)
:effect
(and
(Flag385-)
(Flag385prime-)
)

)
(:action Flag385Action-19
:parameters ()
:precondition
(and
(Flag325-)
(Flag325prime-)
)
:effect
(and
(Flag385-)
(Flag385prime-)
)

)
(:action Flag385Action-20
:parameters ()
:precondition
(and
(Flag371-)
(Flag371prime-)
)
:effect
(and
(Flag385-)
(Flag385prime-)
)

)
(:action Flag385Action-21
:parameters ()
:precondition
(and
(Flag255-)
(Flag255prime-)
)
:effect
(and
(Flag385-)
(Flag385prime-)
)

)
(:action Flag385Action-22
:parameters ()
:precondition
(and
(Flag365-)
(Flag365prime-)
)
:effect
(and
(Flag385-)
(Flag385prime-)
)

)
(:action Flag385Action-23
:parameters ()
:precondition
(and
(Flag264-)
(Flag264prime-)
)
:effect
(and
(Flag385-)
(Flag385prime-)
)

)
(:action Flag385Action-24
:parameters ()
:precondition
(and
(Flag244-)
(Flag244prime-)
)
:effect
(and
(Flag385-)
(Flag385prime-)
)

)
(:action Flag385Action-25
:parameters ()
:precondition
(and
(Flag384-)
(Flag384prime-)
)
:effect
(and
(Flag385-)
(Flag385prime-)
)

)
(:action Flag385Action-26
:parameters ()
:precondition
(and
(Flag289-)
(Flag289prime-)
)
:effect
(and
(Flag385-)
(Flag385prime-)
)

)
(:action Flag385Action-27
:parameters ()
:precondition
(and
(Flag292-)
(Flag292prime-)
)
:effect
(and
(Flag385-)
(Flag385prime-)
)

)
(:action Flag385Action-28
:parameters ()
:precondition
(and
(Flag366-)
(Flag366prime-)
)
:effect
(and
(Flag385-)
(Flag385prime-)
)

)
(:action Flag385Action-29
:parameters ()
:precondition
(and
(Flag309-)
(Flag309prime-)
)
:effect
(and
(Flag385-)
(Flag385prime-)
)

)
(:action Flag385Action-30
:parameters ()
:precondition
(and
(Flag286-)
(Flag286prime-)
)
:effect
(and
(Flag385-)
(Flag385prime-)
)

)
(:action Flag385Action-31
:parameters ()
:precondition
(and
(Flag346-)
(Flag346prime-)
)
:effect
(and
(Flag385-)
(Flag385prime-)
)

)
(:action Flag385Action-32
:parameters ()
:precondition
(and
(Flag302-)
(Flag302prime-)
)
:effect
(and
(Flag385-)
(Flag385prime-)
)

)
(:action Flag385Action-33
:parameters ()
:precondition
(and
(Flag315-)
(Flag315prime-)
)
:effect
(and
(Flag385-)
(Flag385prime-)
)

)
(:action Flag385Action-34
:parameters ()
:precondition
(and
(Flag272-)
(Flag272prime-)
)
:effect
(and
(Flag385-)
(Flag385prime-)
)

)
(:action Flag385Action-35
:parameters ()
:precondition
(and
(Flag238-)
(Flag238prime-)
)
:effect
(and
(Flag385-)
(Flag385prime-)
)

)
(:action Flag385Action-36
:parameters ()
:precondition
(and
(Flag262-)
(Flag262prime-)
)
:effect
(and
(Flag385-)
(Flag385prime-)
)

)
(:action Flag385Action-37
:parameters ()
:precondition
(and
(Flag373-)
(Flag373prime-)
)
:effect
(and
(Flag385-)
(Flag385prime-)
)

)
(:action Flag385Action-38
:parameters ()
:precondition
(and
(Flag351-)
(Flag351prime-)
)
:effect
(and
(Flag385-)
(Flag385prime-)
)

)
(:action Flag385Action-39
:parameters ()
:precondition
(and
(Flag360-)
(Flag360prime-)
)
:effect
(and
(Flag385-)
(Flag385prime-)
)

)
(:action Flag385Action-40
:parameters ()
:precondition
(and
(Flag338-)
(Flag338prime-)
)
:effect
(and
(Flag385-)
(Flag385prime-)
)

)
(:action Flag385Action-41
:parameters ()
:precondition
(and
(Flag340-)
(Flag340prime-)
)
:effect
(and
(Flag385-)
(Flag385prime-)
)

)
(:action Prim385Action
:parameters ()
:precondition
(and
(not (Flag375-))
(Flag375prime-)
(not (Flag304-))
(Flag304prime-)
(not (Flag275-))
(Flag275prime-)
(not (Flag326-))
(Flag326prime-)
(not (Flag331-))
(Flag331prime-)
(not (Flag323-))
(Flag323prime-)
(not (Flag308-))
(Flag308prime-)
(not (Flag369-))
(Flag369prime-)
(not (Flag381-))
(Flag381prime-)
(not (Flag353-))
(Flag353prime-)
(not (Flag232-))
(Flag232prime-)
(not (Flag355-))
(Flag355prime-)
(not (Flag382-))
(Flag382prime-)
(not (Flag233-))
(Flag233prime-)
(not (Flag364-))
(Flag364prime-)
(not (Flag345-))
(Flag345prime-)
(not (Flag242-))
(Flag242prime-)
(not (Flag383-))
(Flag383prime-)
(not (Flag377-))
(Flag377prime-)
(not (Flag325-))
(Flag325prime-)
(not (Flag371-))
(Flag371prime-)
(not (Flag255-))
(Flag255prime-)
(not (Flag365-))
(Flag365prime-)
(not (Flag264-))
(Flag264prime-)
(not (Flag244-))
(Flag244prime-)
(not (Flag384-))
(Flag384prime-)
(not (Flag289-))
(Flag289prime-)
(not (Flag292-))
(Flag292prime-)
(not (Flag366-))
(Flag366prime-)
(not (Flag309-))
(Flag309prime-)
(not (Flag286-))
(Flag286prime-)
(not (Flag346-))
(Flag346prime-)
(not (Flag302-))
(Flag302prime-)
(not (Flag315-))
(Flag315prime-)
(not (Flag272-))
(Flag272prime-)
(not (Flag238-))
(Flag238prime-)
(not (Flag262-))
(Flag262prime-)
(not (Flag373-))
(Flag373prime-)
(not (Flag351-))
(Flag351prime-)
(not (Flag360-))
(Flag360prime-)
(not (Flag338-))
(Flag338prime-)
(not (Flag340-))
(Flag340prime-)
)
:effect
(and
(Flag385prime-)
(not (Flag385-))
)

)
(:action Flag388Action-0
:parameters ()
:precondition
(and
(Flag375-)
(Flag375prime-)
)
:effect
(and
(Flag388-)
(Flag388prime-)
)

)
(:action Flag388Action-1
:parameters ()
:precondition
(and
(Flag304-)
(Flag304prime-)
)
:effect
(and
(Flag388-)
(Flag388prime-)
)

)
(:action Flag388Action-2
:parameters ()
:precondition
(and
(Flag315-)
(Flag315prime-)
)
:effect
(and
(Flag388-)
(Flag388prime-)
)

)
(:action Flag388Action-3
:parameters ()
:precondition
(and
(Flag365-)
(Flag365prime-)
)
:effect
(and
(Flag388-)
(Flag388prime-)
)

)
(:action Flag388Action-4
:parameters ()
:precondition
(and
(Flag386-)
(Flag386prime-)
)
:effect
(and
(Flag388-)
(Flag388prime-)
)

)
(:action Flag388Action-5
:parameters ()
:precondition
(and
(Flag369-)
(Flag369prime-)
)
:effect
(and
(Flag388-)
(Flag388prime-)
)

)
(:action Flag388Action-6
:parameters ()
:precondition
(and
(Flag382-)
(Flag382prime-)
)
:effect
(and
(Flag388-)
(Flag388prime-)
)

)
(:action Flag388Action-7
:parameters ()
:precondition
(and
(Flag353-)
(Flag353prime-)
)
:effect
(and
(Flag388-)
(Flag388prime-)
)

)
(:action Flag388Action-8
:parameters ()
:precondition
(and
(Flag387-)
(Flag387prime-)
)
:effect
(and
(Flag388-)
(Flag388prime-)
)

)
(:action Flag388Action-9
:parameters ()
:precondition
(and
(Flag232-)
(Flag232prime-)
)
:effect
(and
(Flag388-)
(Flag388prime-)
)

)
(:action Flag388Action-10
:parameters ()
:precondition
(and
(Flag355-)
(Flag355prime-)
)
:effect
(and
(Flag388-)
(Flag388prime-)
)

)
(:action Flag388Action-11
:parameters ()
:precondition
(and
(Flag381-)
(Flag381prime-)
)
:effect
(and
(Flag388-)
(Flag388prime-)
)

)
(:action Flag388Action-12
:parameters ()
:precondition
(and
(Flag345-)
(Flag345prime-)
)
:effect
(and
(Flag388-)
(Flag388prime-)
)

)
(:action Flag388Action-13
:parameters ()
:precondition
(and
(Flag377-)
(Flag377prime-)
)
:effect
(and
(Flag388-)
(Flag388prime-)
)

)
(:action Flag388Action-14
:parameters ()
:precondition
(and
(Flag302-)
(Flag302prime-)
)
:effect
(and
(Flag388-)
(Flag388prime-)
)

)
(:action Flag388Action-15
:parameters ()
:precondition
(and
(Flag238-)
(Flag238prime-)
)
:effect
(and
(Flag388-)
(Flag388prime-)
)

)
(:action Flag388Action-16
:parameters ()
:precondition
(and
(Flag323-)
(Flag323prime-)
)
:effect
(and
(Flag388-)
(Flag388prime-)
)

)
(:action Flag388Action-17
:parameters ()
:precondition
(and
(Flag264-)
(Flag264prime-)
)
:effect
(and
(Flag388-)
(Flag388prime-)
)

)
(:action Flag388Action-18
:parameters ()
:precondition
(and
(Flag244-)
(Flag244prime-)
)
:effect
(and
(Flag388-)
(Flag388prime-)
)

)
(:action Flag388Action-19
:parameters ()
:precondition
(and
(Flag292-)
(Flag292prime-)
)
:effect
(and
(Flag388-)
(Flag388prime-)
)

)
(:action Flag388Action-20
:parameters ()
:precondition
(and
(Flag366-)
(Flag366prime-)
)
:effect
(and
(Flag388-)
(Flag388prime-)
)

)
(:action Flag388Action-21
:parameters ()
:precondition
(and
(Flag286-)
(Flag286prime-)
)
:effect
(and
(Flag388-)
(Flag388prime-)
)

)
(:action Flag388Action-22
:parameters ()
:precondition
(and
(Flag325-)
(Flag325prime-)
)
:effect
(and
(Flag388-)
(Flag388prime-)
)

)
(:action Flag388Action-23
:parameters ()
:precondition
(and
(Flag289-)
(Flag289prime-)
)
:effect
(and
(Flag388-)
(Flag388prime-)
)

)
(:action Flag388Action-24
:parameters ()
:precondition
(and
(Flag262-)
(Flag262prime-)
)
:effect
(and
(Flag388-)
(Flag388prime-)
)

)
(:action Flag388Action-25
:parameters ()
:precondition
(and
(Flag373-)
(Flag373prime-)
)
:effect
(and
(Flag388-)
(Flag388prime-)
)

)
(:action Flag388Action-26
:parameters ()
:precondition
(and
(Flag351-)
(Flag351prime-)
)
:effect
(and
(Flag388-)
(Flag388prime-)
)

)
(:action Flag388Action-27
:parameters ()
:precondition
(and
(Flag255-)
(Flag255prime-)
)
:effect
(and
(Flag388-)
(Flag388prime-)
)

)
(:action Flag388Action-28
:parameters ()
:precondition
(and
(Flag338-)
(Flag338prime-)
)
:effect
(and
(Flag388-)
(Flag388prime-)
)

)
(:action Flag388Action-29
:parameters ()
:precondition
(and
(Flag340-)
(Flag340prime-)
)
:effect
(and
(Flag388-)
(Flag388prime-)
)

)
(:action Prim388Action
:parameters ()
:precondition
(and
(not (Flag375-))
(Flag375prime-)
(not (Flag304-))
(Flag304prime-)
(not (Flag315-))
(Flag315prime-)
(not (Flag365-))
(Flag365prime-)
(not (Flag386-))
(Flag386prime-)
(not (Flag369-))
(Flag369prime-)
(not (Flag382-))
(Flag382prime-)
(not (Flag353-))
(Flag353prime-)
(not (Flag387-))
(Flag387prime-)
(not (Flag232-))
(Flag232prime-)
(not (Flag355-))
(Flag355prime-)
(not (Flag381-))
(Flag381prime-)
(not (Flag345-))
(Flag345prime-)
(not (Flag377-))
(Flag377prime-)
(not (Flag302-))
(Flag302prime-)
(not (Flag238-))
(Flag238prime-)
(not (Flag323-))
(Flag323prime-)
(not (Flag264-))
(Flag264prime-)
(not (Flag244-))
(Flag244prime-)
(not (Flag292-))
(Flag292prime-)
(not (Flag366-))
(Flag366prime-)
(not (Flag286-))
(Flag286prime-)
(not (Flag325-))
(Flag325prime-)
(not (Flag289-))
(Flag289prime-)
(not (Flag262-))
(Flag262prime-)
(not (Flag373-))
(Flag373prime-)
(not (Flag351-))
(Flag351prime-)
(not (Flag255-))
(Flag255prime-)
(not (Flag338-))
(Flag338prime-)
(not (Flag340-))
(Flag340prime-)
)
:effect
(and
(Flag388prime-)
(not (Flag388-))
)

)
(:action Flag390Action-0
:parameters ()
:precondition
(and
(Flag387-)
(Flag387prime-)
)
:effect
(and
(Flag390-)
(Flag390prime-)
)

)
(:action Flag390Action-1
:parameters ()
:precondition
(and
(Flag389-)
(Flag389prime-)
)
:effect
(and
(Flag390-)
(Flag390prime-)
)

)
(:action Flag390Action-2
:parameters ()
:precondition
(and
(Flag232-)
(Flag232prime-)
)
:effect
(and
(Flag390-)
(Flag390prime-)
)

)
(:action Flag390Action-3
:parameters ()
:precondition
(and
(Flag323-)
(Flag323prime-)
)
:effect
(and
(Flag390-)
(Flag390prime-)
)

)
(:action Flag390Action-4
:parameters ()
:precondition
(and
(Flag264-)
(Flag264prime-)
)
:effect
(and
(Flag390-)
(Flag390prime-)
)

)
(:action Flag390Action-5
:parameters ()
:precondition
(and
(Flag304-)
(Flag304prime-)
)
:effect
(and
(Flag390-)
(Flag390prime-)
)

)
(:action Flag390Action-6
:parameters ()
:precondition
(and
(Flag365-)
(Flag365prime-)
)
:effect
(and
(Flag390-)
(Flag390prime-)
)

)
(:action Flag390Action-7
:parameters ()
:precondition
(and
(Flag351-)
(Flag351prime-)
)
:effect
(and
(Flag390-)
(Flag390prime-)
)

)
(:action Flag390Action-8
:parameters ()
:precondition
(and
(Flag255-)
(Flag255prime-)
)
:effect
(and
(Flag390-)
(Flag390prime-)
)

)
(:action Flag390Action-9
:parameters ()
:precondition
(and
(Flag338-)
(Flag338prime-)
)
:effect
(and
(Flag390-)
(Flag390prime-)
)

)
(:action Flag390Action-10
:parameters ()
:precondition
(and
(Flag292-)
(Flag292prime-)
)
:effect
(and
(Flag390-)
(Flag390prime-)
)

)
(:action Flag390Action-11
:parameters ()
:precondition
(and
(Flag369-)
(Flag369prime-)
)
:effect
(and
(Flag390-)
(Flag390prime-)
)

)
(:action Flag390Action-12
:parameters ()
:precondition
(and
(Flag377-)
(Flag377prime-)
)
:effect
(and
(Flag390-)
(Flag390prime-)
)

)
(:action Flag390Action-13
:parameters ()
:precondition
(and
(Flag382-)
(Flag382prime-)
)
:effect
(and
(Flag390-)
(Flag390prime-)
)

)
(:action Flag390Action-14
:parameters ()
:precondition
(and
(Flag353-)
(Flag353prime-)
)
:effect
(and
(Flag390-)
(Flag390prime-)
)

)
(:action Flag390Action-15
:parameters ()
:precondition
(and
(Flag286-)
(Flag286prime-)
)
:effect
(and
(Flag390-)
(Flag390prime-)
)

)
(:action Prim390Action
:parameters ()
:precondition
(and
(not (Flag387-))
(Flag387prime-)
(not (Flag389-))
(Flag389prime-)
(not (Flag232-))
(Flag232prime-)
(not (Flag323-))
(Flag323prime-)
(not (Flag264-))
(Flag264prime-)
(not (Flag304-))
(Flag304prime-)
(not (Flag365-))
(Flag365prime-)
(not (Flag351-))
(Flag351prime-)
(not (Flag255-))
(Flag255prime-)
(not (Flag338-))
(Flag338prime-)
(not (Flag292-))
(Flag292prime-)
(not (Flag369-))
(Flag369prime-)
(not (Flag377-))
(Flag377prime-)
(not (Flag382-))
(Flag382prime-)
(not (Flag353-))
(Flag353prime-)
(not (Flag286-))
(Flag286prime-)
)
:effect
(and
(Flag390prime-)
(not (Flag390-))
)

)
(:action Flag391Action-0
:parameters ()
:precondition
(and
(Flag251-)
(Flag251prime-)
)
:effect
(and
(Flag391-)
(Flag391prime-)
)

)
(:action Flag391Action-1
:parameters ()
:precondition
(and
(Flag240-)
(Flag240prime-)
)
:effect
(and
(Flag391-)
(Flag391prime-)
)

)
(:action Flag391Action-2
:parameters ()
:precondition
(and
(Flag235-)
(Flag235prime-)
)
:effect
(and
(Flag391-)
(Flag391prime-)
)

)
(:action Flag391Action-3
:parameters ()
:precondition
(and
(Flag253-)
(Flag253prime-)
)
:effect
(and
(Flag391-)
(Flag391prime-)
)

)
(:action Flag391Action-4
:parameters ()
:precondition
(and
(Flag242-)
(Flag242prime-)
)
:effect
(and
(Flag391-)
(Flag391prime-)
)

)
(:action Flag391Action-5
:parameters ()
:precondition
(and
(Flag234-)
(Flag234prime-)
)
:effect
(and
(Flag391-)
(Flag391prime-)
)

)
(:action Flag391Action-6
:parameters ()
:precondition
(and
(Flag229-)
(Flag229prime-)
)
:effect
(and
(Flag391-)
(Flag391prime-)
)

)
(:action Flag391Action-7
:parameters ()
:precondition
(and
(Flag243-)
(Flag243prime-)
)
:effect
(and
(Flag391-)
(Flag391prime-)
)

)
(:action Flag391Action-8
:parameters ()
:precondition
(and
(Flag236-)
(Flag236prime-)
)
:effect
(and
(Flag391-)
(Flag391prime-)
)

)
(:action Flag391Action-9
:parameters ()
:precondition
(and
(Flag244-)
(Flag244prime-)
)
:effect
(and
(Flag391-)
(Flag391prime-)
)

)
(:action Flag391Action-10
:parameters ()
:precondition
(and
(Flag245-)
(Flag245prime-)
)
:effect
(and
(Flag391-)
(Flag391prime-)
)

)
(:action Flag391Action-12
:parameters ()
:precondition
(and
(Flag255-)
(Flag255prime-)
)
:effect
(and
(Flag391-)
(Flag391prime-)
)

)
(:action Flag391Action-13
:parameters ()
:precondition
(and
(Flag256-)
(Flag256prime-)
)
:effect
(and
(Flag391-)
(Flag391prime-)
)

)
(:action Flag391Action-14
:parameters ()
:precondition
(and
(Flag247-)
(Flag247prime-)
)
:effect
(and
(Flag391-)
(Flag391prime-)
)

)
(:action Flag391Action-15
:parameters ()
:precondition
(and
(Flag248-)
(Flag248prime-)
)
:effect
(and
(Flag391-)
(Flag391prime-)
)

)
(:action Prim391Action
:parameters ()
:precondition
(and
(not (Flag251-))
(Flag251prime-)
(not (Flag240-))
(Flag240prime-)
(not (Flag235-))
(Flag235prime-)
(not (Flag253-))
(Flag253prime-)
(not (Flag242-))
(Flag242prime-)
(not (Flag234-))
(Flag234prime-)
(not (Flag229-))
(Flag229prime-)
(not (Flag243-))
(Flag243prime-)
(not (Flag236-))
(Flag236prime-)
(not (Flag244-))
(Flag244prime-)
(not (Flag245-))
(Flag245prime-)
(not (BELOWOF1-ROBOT))
(not (Flag255-))
(Flag255prime-)
(not (Flag256-))
(Flag256prime-)
(not (Flag247-))
(Flag247prime-)
(not (Flag248-))
(Flag248prime-)
)
:effect
(and
(Flag391prime-)
(not (Flag391-))
)

)
(:action MOVELEFT-ROBOT
:parameters ()
:precondition
(and
(Flag4-)
(Flag4prime-)
(Flag198prime-)
(Flag194prime-)
(Flag191prime-)
(Flag187prime-)
(Flag181prime-)
(Flag175prime-)
(Flag167prime-)
(Flag159prime-)
(Flag149prime-)
(Flag138prime-)
(Flag125prime-)
(Flag113prime-)
(Flag98prime-)
(Flag82prime-)
(Flag67prime-)
(Flag195prime-)
)
:effect
(and
(when
(and
(Flag198-)
)
(and
(COLUMN14-ROBOT)
(not (NOT-COLUMN14-ROBOT))
)
)
(when
(and
(Flag194-)
)
(and
(COLUMN13-ROBOT)
(not (NOT-COLUMN13-ROBOT))
)
)
(when
(and
(Flag191-)
)
(and
(COLUMN12-ROBOT)
(not (NOT-COLUMN12-ROBOT))
)
)
(when
(and
(Flag187-)
)
(and
(COLUMN11-ROBOT)
(not (NOT-COLUMN11-ROBOT))
)
)
(when
(and
(Flag181-)
)
(and
(COLUMN10-ROBOT)
(not (NOT-COLUMN10-ROBOT))
)
)
(when
(and
(Flag175-)
)
(and
(COLUMN9-ROBOT)
(not (NOT-COLUMN9-ROBOT))
)
)
(when
(and
(Flag167-)
)
(and
(COLUMN8-ROBOT)
(not (NOT-COLUMN8-ROBOT))
)
)
(when
(and
(Flag159-)
)
(and
(COLUMN7-ROBOT)
(not (NOT-COLUMN7-ROBOT))
)
)
(when
(and
(Flag149-)
)
(and
(COLUMN6-ROBOT)
(not (NOT-COLUMN6-ROBOT))
)
)
(when
(and
(Flag138-)
)
(and
(COLUMN5-ROBOT)
(not (NOT-COLUMN5-ROBOT))
)
)
(when
(and
(Flag125-)
)
(and
(COLUMN4-ROBOT)
(not (NOT-COLUMN4-ROBOT))
)
)
(when
(and
(Flag113-)
)
(and
(COLUMN3-ROBOT)
(not (NOT-COLUMN3-ROBOT))
)
)
(when
(and
(Flag98-)
)
(and
(COLUMN2-ROBOT)
(not (NOT-COLUMN2-ROBOT))
)
)
(when
(and
(Flag82-)
)
(and
(COLUMN1-ROBOT)
(not (NOT-COLUMN1-ROBOT))
)
)
(when
(and
(Flag67-)
)
(and
(COLUMN0-ROBOT)
(not (NOT-COLUMN0-ROBOT))
)
)
(when
(and
(COLUMN15-ROBOT)
)
(and
(COLUMN14-ROBOT)
(NOT-COLUMN15-ROBOT)
(not (NOT-COLUMN14-ROBOT))
(not (COLUMN15-ROBOT))
)
)
(when
(and
(COLUMN14-ROBOT)
)
(and
(COLUMN13-ROBOT)
(NOT-COLUMN14-ROBOT)
(not (NOT-COLUMN13-ROBOT))
(not (COLUMN14-ROBOT))
)
)
(when
(and
(COLUMN13-ROBOT)
)
(and
(COLUMN12-ROBOT)
(NOT-COLUMN13-ROBOT)
(not (NOT-COLUMN12-ROBOT))
(not (COLUMN13-ROBOT))
)
)
(when
(and
(COLUMN12-ROBOT)
)
(and
(COLUMN11-ROBOT)
(NOT-COLUMN12-ROBOT)
(not (NOT-COLUMN11-ROBOT))
(not (COLUMN12-ROBOT))
)
)
(when
(and
(COLUMN11-ROBOT)
)
(and
(COLUMN10-ROBOT)
(NOT-COLUMN11-ROBOT)
(not (NOT-COLUMN10-ROBOT))
(not (COLUMN11-ROBOT))
)
)
(when
(and
(COLUMN10-ROBOT)
)
(and
(COLUMN9-ROBOT)
(NOT-COLUMN10-ROBOT)
(not (NOT-COLUMN9-ROBOT))
(not (COLUMN10-ROBOT))
)
)
(when
(and
(COLUMN9-ROBOT)
)
(and
(COLUMN8-ROBOT)
(NOT-COLUMN9-ROBOT)
(not (NOT-COLUMN8-ROBOT))
(not (COLUMN9-ROBOT))
)
)
(when
(and
(COLUMN8-ROBOT)
)
(and
(COLUMN7-ROBOT)
(NOT-COLUMN8-ROBOT)
(not (NOT-COLUMN7-ROBOT))
(not (COLUMN8-ROBOT))
)
)
(when
(and
(COLUMN7-ROBOT)
)
(and
(COLUMN6-ROBOT)
(NOT-COLUMN7-ROBOT)
(not (NOT-COLUMN6-ROBOT))
(not (COLUMN7-ROBOT))
)
)
(when
(and
(COLUMN6-ROBOT)
)
(and
(COLUMN5-ROBOT)
(NOT-COLUMN6-ROBOT)
(not (NOT-COLUMN5-ROBOT))
(not (COLUMN6-ROBOT))
)
)
(when
(and
(COLUMN5-ROBOT)
)
(and
(COLUMN4-ROBOT)
(NOT-COLUMN5-ROBOT)
(not (NOT-COLUMN4-ROBOT))
(not (COLUMN5-ROBOT))
)
)
(when
(and
(COLUMN4-ROBOT)
)
(and
(COLUMN3-ROBOT)
(NOT-COLUMN4-ROBOT)
(not (NOT-COLUMN3-ROBOT))
(not (COLUMN4-ROBOT))
)
)
(when
(and
(COLUMN3-ROBOT)
)
(and
(COLUMN2-ROBOT)
(NOT-COLUMN3-ROBOT)
(not (NOT-COLUMN2-ROBOT))
(not (COLUMN3-ROBOT))
)
)
(when
(and
(COLUMN2-ROBOT)
)
(and
(COLUMN1-ROBOT)
(NOT-COLUMN2-ROBOT)
(not (NOT-COLUMN1-ROBOT))
(not (COLUMN2-ROBOT))
)
)
(when
(and
(COLUMN1-ROBOT)
)
(and
(COLUMN0-ROBOT)
(NOT-COLUMN1-ROBOT)
(not (NOT-COLUMN0-ROBOT))
(not (COLUMN1-ROBOT))
)
)
(when
(and
(RIGHTOF15-ROBOT)
)
(and
(RIGHTOF14-ROBOT)
(NOT-RIGHTOF15-ROBOT)
(RIGHTOF13-ROBOT)
(NOT-RIGHTOF14-ROBOT)
(RIGHTOF12-ROBOT)
(NOT-RIGHTOF13-ROBOT)
(RIGHTOF11-ROBOT)
(NOT-RIGHTOF12-ROBOT)
(RIGHTOF10-ROBOT)
(NOT-RIGHTOF11-ROBOT)
(RIGHTOF9-ROBOT)
(NOT-RIGHTOF10-ROBOT)
(RIGHTOF8-ROBOT)
(NOT-RIGHTOF9-ROBOT)
(RIGHTOF7-ROBOT)
(NOT-RIGHTOF8-ROBOT)
(RIGHTOF6-ROBOT)
(NOT-RIGHTOF7-ROBOT)
(RIGHTOF5-ROBOT)
(NOT-RIGHTOF6-ROBOT)
(RIGHTOF4-ROBOT)
(NOT-RIGHTOF5-ROBOT)
(RIGHTOF3-ROBOT)
(NOT-RIGHTOF4-ROBOT)
(RIGHTOF2-ROBOT)
(NOT-RIGHTOF3-ROBOT)
(RIGHTOF1-ROBOT)
(NOT-RIGHTOF2-ROBOT)
(RIGHTOF0-ROBOT)
(NOT-RIGHTOF1-ROBOT)
(not (RIGHTOF15-ROBOT))
(not (NOT-RIGHTOF0-ROBOT))
)
)
(when
(and
(RIGHTOF14-ROBOT)
)
(and
(RIGHTOF13-ROBOT)
(NOT-RIGHTOF14-ROBOT)
(RIGHTOF12-ROBOT)
(NOT-RIGHTOF13-ROBOT)
(RIGHTOF11-ROBOT)
(NOT-RIGHTOF12-ROBOT)
(RIGHTOF10-ROBOT)
(NOT-RIGHTOF11-ROBOT)
(RIGHTOF9-ROBOT)
(NOT-RIGHTOF10-ROBOT)
(RIGHTOF8-ROBOT)
(NOT-RIGHTOF9-ROBOT)
(RIGHTOF7-ROBOT)
(NOT-RIGHTOF8-ROBOT)
(RIGHTOF6-ROBOT)
(NOT-RIGHTOF7-ROBOT)
(RIGHTOF5-ROBOT)
(NOT-RIGHTOF6-ROBOT)
(RIGHTOF4-ROBOT)
(NOT-RIGHTOF5-ROBOT)
(RIGHTOF3-ROBOT)
(NOT-RIGHTOF4-ROBOT)
(RIGHTOF2-ROBOT)
(NOT-RIGHTOF3-ROBOT)
(RIGHTOF1-ROBOT)
(NOT-RIGHTOF2-ROBOT)
(RIGHTOF0-ROBOT)
(NOT-RIGHTOF1-ROBOT)
(not (RIGHTOF14-ROBOT))
(not (NOT-RIGHTOF0-ROBOT))
)
)
(when
(and
(RIGHTOF13-ROBOT)
)
(and
(RIGHTOF12-ROBOT)
(NOT-RIGHTOF13-ROBOT)
(RIGHTOF11-ROBOT)
(NOT-RIGHTOF12-ROBOT)
(RIGHTOF10-ROBOT)
(NOT-RIGHTOF11-ROBOT)
(RIGHTOF9-ROBOT)
(NOT-RIGHTOF10-ROBOT)
(RIGHTOF8-ROBOT)
(NOT-RIGHTOF9-ROBOT)
(RIGHTOF7-ROBOT)
(NOT-RIGHTOF8-ROBOT)
(RIGHTOF6-ROBOT)
(NOT-RIGHTOF7-ROBOT)
(RIGHTOF5-ROBOT)
(NOT-RIGHTOF6-ROBOT)
(RIGHTOF4-ROBOT)
(NOT-RIGHTOF5-ROBOT)
(RIGHTOF3-ROBOT)
(NOT-RIGHTOF4-ROBOT)
(RIGHTOF2-ROBOT)
(NOT-RIGHTOF3-ROBOT)
(RIGHTOF1-ROBOT)
(NOT-RIGHTOF2-ROBOT)
(RIGHTOF0-ROBOT)
(NOT-RIGHTOF1-ROBOT)
(not (RIGHTOF13-ROBOT))
(not (NOT-RIGHTOF0-ROBOT))
)
)
(when
(and
(RIGHTOF12-ROBOT)
)
(and
(RIGHTOF11-ROBOT)
(NOT-RIGHTOF12-ROBOT)
(RIGHTOF10-ROBOT)
(NOT-RIGHTOF11-ROBOT)
(RIGHTOF9-ROBOT)
(NOT-RIGHTOF10-ROBOT)
(RIGHTOF8-ROBOT)
(NOT-RIGHTOF9-ROBOT)
(RIGHTOF7-ROBOT)
(NOT-RIGHTOF8-ROBOT)
(RIGHTOF6-ROBOT)
(NOT-RIGHTOF7-ROBOT)
(RIGHTOF5-ROBOT)
(NOT-RIGHTOF6-ROBOT)
(RIGHTOF4-ROBOT)
(NOT-RIGHTOF5-ROBOT)
(RIGHTOF3-ROBOT)
(NOT-RIGHTOF4-ROBOT)
(RIGHTOF2-ROBOT)
(NOT-RIGHTOF3-ROBOT)
(RIGHTOF1-ROBOT)
(NOT-RIGHTOF2-ROBOT)
(RIGHTOF0-ROBOT)
(NOT-RIGHTOF1-ROBOT)
(not (RIGHTOF12-ROBOT))
(not (NOT-RIGHTOF0-ROBOT))
)
)
(when
(and
(RIGHTOF11-ROBOT)
)
(and
(RIGHTOF10-ROBOT)
(NOT-RIGHTOF11-ROBOT)
(RIGHTOF9-ROBOT)
(NOT-RIGHTOF10-ROBOT)
(RIGHTOF8-ROBOT)
(NOT-RIGHTOF9-ROBOT)
(RIGHTOF7-ROBOT)
(NOT-RIGHTOF8-ROBOT)
(RIGHTOF6-ROBOT)
(NOT-RIGHTOF7-ROBOT)
(RIGHTOF5-ROBOT)
(NOT-RIGHTOF6-ROBOT)
(RIGHTOF4-ROBOT)
(NOT-RIGHTOF5-ROBOT)
(RIGHTOF3-ROBOT)
(NOT-RIGHTOF4-ROBOT)
(RIGHTOF2-ROBOT)
(NOT-RIGHTOF3-ROBOT)
(RIGHTOF1-ROBOT)
(NOT-RIGHTOF2-ROBOT)
(RIGHTOF0-ROBOT)
(NOT-RIGHTOF1-ROBOT)
(not (RIGHTOF11-ROBOT))
(not (NOT-RIGHTOF0-ROBOT))
)
)
(when
(and
(RIGHTOF10-ROBOT)
)
(and
(RIGHTOF9-ROBOT)
(NOT-RIGHTOF10-ROBOT)
(RIGHTOF8-ROBOT)
(NOT-RIGHTOF9-ROBOT)
(RIGHTOF7-ROBOT)
(NOT-RIGHTOF8-ROBOT)
(RIGHTOF6-ROBOT)
(NOT-RIGHTOF7-ROBOT)
(RIGHTOF5-ROBOT)
(NOT-RIGHTOF6-ROBOT)
(RIGHTOF4-ROBOT)
(NOT-RIGHTOF5-ROBOT)
(RIGHTOF3-ROBOT)
(NOT-RIGHTOF4-ROBOT)
(RIGHTOF2-ROBOT)
(NOT-RIGHTOF3-ROBOT)
(RIGHTOF1-ROBOT)
(NOT-RIGHTOF2-ROBOT)
(RIGHTOF0-ROBOT)
(NOT-RIGHTOF1-ROBOT)
(not (RIGHTOF10-ROBOT))
(not (NOT-RIGHTOF0-ROBOT))
)
)
(when
(and
(RIGHTOF9-ROBOT)
)
(and
(RIGHTOF8-ROBOT)
(NOT-RIGHTOF9-ROBOT)
(RIGHTOF7-ROBOT)
(NOT-RIGHTOF8-ROBOT)
(RIGHTOF6-ROBOT)
(NOT-RIGHTOF7-ROBOT)
(RIGHTOF5-ROBOT)
(NOT-RIGHTOF6-ROBOT)
(RIGHTOF4-ROBOT)
(NOT-RIGHTOF5-ROBOT)
(RIGHTOF3-ROBOT)
(NOT-RIGHTOF4-ROBOT)
(RIGHTOF2-ROBOT)
(NOT-RIGHTOF3-ROBOT)
(RIGHTOF1-ROBOT)
(NOT-RIGHTOF2-ROBOT)
(RIGHTOF0-ROBOT)
(NOT-RIGHTOF1-ROBOT)
(not (RIGHTOF9-ROBOT))
(not (NOT-RIGHTOF0-ROBOT))
)
)
(when
(and
(RIGHTOF8-ROBOT)
)
(and
(RIGHTOF7-ROBOT)
(NOT-RIGHTOF8-ROBOT)
(RIGHTOF6-ROBOT)
(NOT-RIGHTOF7-ROBOT)
(RIGHTOF5-ROBOT)
(NOT-RIGHTOF6-ROBOT)
(RIGHTOF4-ROBOT)
(NOT-RIGHTOF5-ROBOT)
(RIGHTOF3-ROBOT)
(NOT-RIGHTOF4-ROBOT)
(RIGHTOF2-ROBOT)
(NOT-RIGHTOF3-ROBOT)
(RIGHTOF1-ROBOT)
(NOT-RIGHTOF2-ROBOT)
(RIGHTOF0-ROBOT)
(NOT-RIGHTOF1-ROBOT)
(not (RIGHTOF8-ROBOT))
(not (NOT-RIGHTOF0-ROBOT))
)
)
(when
(and
(RIGHTOF7-ROBOT)
)
(and
(RIGHTOF6-ROBOT)
(NOT-RIGHTOF7-ROBOT)
(RIGHTOF5-ROBOT)
(NOT-RIGHTOF6-ROBOT)
(RIGHTOF4-ROBOT)
(NOT-RIGHTOF5-ROBOT)
(RIGHTOF3-ROBOT)
(NOT-RIGHTOF4-ROBOT)
(RIGHTOF2-ROBOT)
(NOT-RIGHTOF3-ROBOT)
(RIGHTOF1-ROBOT)
(NOT-RIGHTOF2-ROBOT)
(RIGHTOF0-ROBOT)
(NOT-RIGHTOF1-ROBOT)
(not (RIGHTOF7-ROBOT))
(not (NOT-RIGHTOF0-ROBOT))
)
)
(when
(and
(RIGHTOF6-ROBOT)
)
(and
(RIGHTOF5-ROBOT)
(NOT-RIGHTOF6-ROBOT)
(RIGHTOF4-ROBOT)
(NOT-RIGHTOF5-ROBOT)
(RIGHTOF3-ROBOT)
(NOT-RIGHTOF4-ROBOT)
(RIGHTOF2-ROBOT)
(NOT-RIGHTOF3-ROBOT)
(RIGHTOF1-ROBOT)
(NOT-RIGHTOF2-ROBOT)
(RIGHTOF0-ROBOT)
(NOT-RIGHTOF1-ROBOT)
(not (RIGHTOF6-ROBOT))
(not (NOT-RIGHTOF0-ROBOT))
)
)
(when
(and
(RIGHTOF5-ROBOT)
)
(and
(RIGHTOF4-ROBOT)
(NOT-RIGHTOF5-ROBOT)
(RIGHTOF3-ROBOT)
(NOT-RIGHTOF4-ROBOT)
(RIGHTOF2-ROBOT)
(NOT-RIGHTOF3-ROBOT)
(RIGHTOF1-ROBOT)
(NOT-RIGHTOF2-ROBOT)
(RIGHTOF0-ROBOT)
(NOT-RIGHTOF1-ROBOT)
(not (RIGHTOF5-ROBOT))
(not (NOT-RIGHTOF0-ROBOT))
)
)
(when
(and
(RIGHTOF4-ROBOT)
)
(and
(RIGHTOF3-ROBOT)
(NOT-RIGHTOF4-ROBOT)
(RIGHTOF2-ROBOT)
(NOT-RIGHTOF3-ROBOT)
(RIGHTOF1-ROBOT)
(NOT-RIGHTOF2-ROBOT)
(RIGHTOF0-ROBOT)
(NOT-RIGHTOF1-ROBOT)
(not (RIGHTOF4-ROBOT))
(not (NOT-RIGHTOF0-ROBOT))
)
)
(when
(and
(RIGHTOF3-ROBOT)
)
(and
(RIGHTOF2-ROBOT)
(NOT-RIGHTOF3-ROBOT)
(RIGHTOF1-ROBOT)
(NOT-RIGHTOF2-ROBOT)
(RIGHTOF0-ROBOT)
(NOT-RIGHTOF1-ROBOT)
(not (RIGHTOF3-ROBOT))
(not (NOT-RIGHTOF0-ROBOT))
)
)
(when
(and
(RIGHTOF2-ROBOT)
)
(and
(RIGHTOF1-ROBOT)
(NOT-RIGHTOF2-ROBOT)
(RIGHTOF0-ROBOT)
(NOT-RIGHTOF1-ROBOT)
(not (RIGHTOF2-ROBOT))
(not (NOT-RIGHTOF0-ROBOT))
)
)
(when
(and
(RIGHTOF1-ROBOT)
)
(and
(RIGHTOF0-ROBOT)
(NOT-RIGHTOF1-ROBOT)
(not (NOT-RIGHTOF0-ROBOT))
(not (RIGHTOF1-ROBOT))
)
)
(when
(and
(Flag195-)
)
(and
(LEFTOF15-ROBOT)
(not (NOT-LEFTOF15-ROBOT))
)
)
(when
(and
(LEFTOF8-ROBOT)
)
(and
(LEFTOF14-ROBOT)
(LEFTOF13-ROBOT)
(LEFTOF12-ROBOT)
(LEFTOF11-ROBOT)
(LEFTOF10-ROBOT)
(LEFTOF9-ROBOT)
(LEFTOF7-ROBOT)
(not (NOT-LEFTOF14-ROBOT))
(not (NOT-LEFTOF13-ROBOT))
(not (NOT-LEFTOF12-ROBOT))
(not (NOT-LEFTOF11-ROBOT))
(not (NOT-LEFTOF10-ROBOT))
(not (NOT-LEFTOF9-ROBOT))
(not (NOT-LEFTOF8-ROBOT))
(not (NOT-LEFTOF7-ROBOT))
)
)
(when
(and
(LEFTOF7-ROBOT)
)
(and
(LEFTOF14-ROBOT)
(LEFTOF13-ROBOT)
(LEFTOF12-ROBOT)
(LEFTOF11-ROBOT)
(LEFTOF10-ROBOT)
(LEFTOF9-ROBOT)
(LEFTOF8-ROBOT)
(LEFTOF6-ROBOT)
(not (NOT-LEFTOF14-ROBOT))
(not (NOT-LEFTOF13-ROBOT))
(not (NOT-LEFTOF12-ROBOT))
(not (NOT-LEFTOF11-ROBOT))
(not (NOT-LEFTOF10-ROBOT))
(not (NOT-LEFTOF9-ROBOT))
(not (NOT-LEFTOF8-ROBOT))
(not (NOT-LEFTOF7-ROBOT))
(not (NOT-LEFTOF6-ROBOT))
)
)
(when
(and
(LEFTOF4-ROBOT)
)
(and
(LEFTOF14-ROBOT)
(LEFTOF13-ROBOT)
(LEFTOF12-ROBOT)
(LEFTOF11-ROBOT)
(LEFTOF10-ROBOT)
(LEFTOF9-ROBOT)
(LEFTOF8-ROBOT)
(LEFTOF7-ROBOT)
(LEFTOF6-ROBOT)
(LEFTOF5-ROBOT)
(LEFTOF3-ROBOT)
(not (NOT-LEFTOF14-ROBOT))
(not (NOT-LEFTOF13-ROBOT))
(not (NOT-LEFTOF12-ROBOT))
(not (NOT-LEFTOF11-ROBOT))
(not (NOT-LEFTOF10-ROBOT))
(not (NOT-LEFTOF9-ROBOT))
(not (NOT-LEFTOF8-ROBOT))
(not (NOT-LEFTOF7-ROBOT))
(not (NOT-LEFTOF6-ROBOT))
(not (NOT-LEFTOF5-ROBOT))
(not (NOT-LEFTOF4-ROBOT))
(not (NOT-LEFTOF3-ROBOT))
)
)
(when
(and
(LEFTOF3-ROBOT)
)
(and
(LEFTOF14-ROBOT)
(LEFTOF13-ROBOT)
(LEFTOF12-ROBOT)
(LEFTOF11-ROBOT)
(LEFTOF10-ROBOT)
(LEFTOF9-ROBOT)
(LEFTOF8-ROBOT)
(LEFTOF7-ROBOT)
(LEFTOF6-ROBOT)
(LEFTOF5-ROBOT)
(LEFTOF4-ROBOT)
(LEFTOF2-ROBOT)
(not (NOT-LEFTOF14-ROBOT))
(not (NOT-LEFTOF13-ROBOT))
(not (NOT-LEFTOF12-ROBOT))
(not (NOT-LEFTOF11-ROBOT))
(not (NOT-LEFTOF10-ROBOT))
(not (NOT-LEFTOF9-ROBOT))
(not (NOT-LEFTOF8-ROBOT))
(not (NOT-LEFTOF7-ROBOT))
(not (NOT-LEFTOF6-ROBOT))
(not (NOT-LEFTOF5-ROBOT))
(not (NOT-LEFTOF4-ROBOT))
(not (NOT-LEFTOF3-ROBOT))
(not (NOT-LEFTOF2-ROBOT))
)
)
(when
(and
(LEFTOF14-ROBOT)
)
(and
(LEFTOF13-ROBOT)
(not (NOT-LEFTOF14-ROBOT))
(not (NOT-LEFTOF13-ROBOT))
)
)
(when
(and
(LEFTOF5-ROBOT)
)
(and
(LEFTOF14-ROBOT)
(LEFTOF13-ROBOT)
(LEFTOF12-ROBOT)
(LEFTOF11-ROBOT)
(LEFTOF10-ROBOT)
(LEFTOF9-ROBOT)
(LEFTOF8-ROBOT)
(LEFTOF7-ROBOT)
(LEFTOF6-ROBOT)
(LEFTOF4-ROBOT)
(not (NOT-LEFTOF14-ROBOT))
(not (NOT-LEFTOF13-ROBOT))
(not (NOT-LEFTOF12-ROBOT))
(not (NOT-LEFTOF11-ROBOT))
(not (NOT-LEFTOF10-ROBOT))
(not (NOT-LEFTOF9-ROBOT))
(not (NOT-LEFTOF8-ROBOT))
(not (NOT-LEFTOF7-ROBOT))
(not (NOT-LEFTOF6-ROBOT))
(not (NOT-LEFTOF5-ROBOT))
(not (NOT-LEFTOF4-ROBOT))
)
)
(when
(and
(LEFTOF6-ROBOT)
)
(and
(LEFTOF14-ROBOT)
(LEFTOF13-ROBOT)
(LEFTOF12-ROBOT)
(LEFTOF11-ROBOT)
(LEFTOF10-ROBOT)
(LEFTOF9-ROBOT)
(LEFTOF8-ROBOT)
(LEFTOF7-ROBOT)
(LEFTOF5-ROBOT)
(not (NOT-LEFTOF14-ROBOT))
(not (NOT-LEFTOF13-ROBOT))
(not (NOT-LEFTOF12-ROBOT))
(not (NOT-LEFTOF11-ROBOT))
(not (NOT-LEFTOF10-ROBOT))
(not (NOT-LEFTOF9-ROBOT))
(not (NOT-LEFTOF8-ROBOT))
(not (NOT-LEFTOF7-ROBOT))
(not (NOT-LEFTOF6-ROBOT))
(not (NOT-LEFTOF5-ROBOT))
)
)
(when
(and
(LEFTOF12-ROBOT)
)
(and
(LEFTOF14-ROBOT)
(LEFTOF13-ROBOT)
(LEFTOF11-ROBOT)
(not (NOT-LEFTOF14-ROBOT))
(not (NOT-LEFTOF13-ROBOT))
(not (NOT-LEFTOF12-ROBOT))
(not (NOT-LEFTOF11-ROBOT))
)
)
(when
(and
(LEFTOF1-ROBOT)
)
(and
(LEFTOF14-ROBOT)
(LEFTOF13-ROBOT)
(LEFTOF12-ROBOT)
(LEFTOF11-ROBOT)
(LEFTOF10-ROBOT)
(LEFTOF9-ROBOT)
(LEFTOF8-ROBOT)
(LEFTOF7-ROBOT)
(LEFTOF6-ROBOT)
(LEFTOF5-ROBOT)
(LEFTOF4-ROBOT)
(LEFTOF3-ROBOT)
(LEFTOF2-ROBOT)
(not (NOT-LEFTOF14-ROBOT))
(not (NOT-LEFTOF13-ROBOT))
(not (NOT-LEFTOF12-ROBOT))
(not (NOT-LEFTOF11-ROBOT))
(not (NOT-LEFTOF10-ROBOT))
(not (NOT-LEFTOF9-ROBOT))
(not (NOT-LEFTOF8-ROBOT))
(not (NOT-LEFTOF7-ROBOT))
(not (NOT-LEFTOF6-ROBOT))
(not (NOT-LEFTOF5-ROBOT))
(not (NOT-LEFTOF4-ROBOT))
(not (NOT-LEFTOF3-ROBOT))
(not (NOT-LEFTOF2-ROBOT))
(not (NOT-LEFTOF1-ROBOT))
)
)
(when
(and
(LEFTOF13-ROBOT)
)
(and
(LEFTOF14-ROBOT)
(LEFTOF12-ROBOT)
(not (NOT-LEFTOF14-ROBOT))
(not (NOT-LEFTOF13-ROBOT))
(not (NOT-LEFTOF12-ROBOT))
)
)
(when
(and
(LEFTOF2-ROBOT)
)
(and
(LEFTOF14-ROBOT)
(LEFTOF13-ROBOT)
(LEFTOF12-ROBOT)
(LEFTOF11-ROBOT)
(LEFTOF10-ROBOT)
(LEFTOF9-ROBOT)
(LEFTOF8-ROBOT)
(LEFTOF7-ROBOT)
(LEFTOF6-ROBOT)
(LEFTOF5-ROBOT)
(LEFTOF4-ROBOT)
(LEFTOF3-ROBOT)
(LEFTOF1-ROBOT)
(not (NOT-LEFTOF14-ROBOT))
(not (NOT-LEFTOF13-ROBOT))
(not (NOT-LEFTOF12-ROBOT))
(not (NOT-LEFTOF11-ROBOT))
(not (NOT-LEFTOF10-ROBOT))
(not (NOT-LEFTOF9-ROBOT))
(not (NOT-LEFTOF8-ROBOT))
(not (NOT-LEFTOF7-ROBOT))
(not (NOT-LEFTOF6-ROBOT))
(not (NOT-LEFTOF5-ROBOT))
(not (NOT-LEFTOF4-ROBOT))
(not (NOT-LEFTOF3-ROBOT))
(not (NOT-LEFTOF2-ROBOT))
(not (NOT-LEFTOF1-ROBOT))
)
)
(when
(and
(LEFTOF9-ROBOT)
)
(and
(LEFTOF14-ROBOT)
(LEFTOF13-ROBOT)
(LEFTOF12-ROBOT)
(LEFTOF11-ROBOT)
(LEFTOF10-ROBOT)
(LEFTOF8-ROBOT)
(not (NOT-LEFTOF14-ROBOT))
(not (NOT-LEFTOF13-ROBOT))
(not (NOT-LEFTOF12-ROBOT))
(not (NOT-LEFTOF11-ROBOT))
(not (NOT-LEFTOF10-ROBOT))
(not (NOT-LEFTOF9-ROBOT))
(not (NOT-LEFTOF8-ROBOT))
)
)
(when
(and
(LEFTOF11-ROBOT)
)
(and
(LEFTOF14-ROBOT)
(LEFTOF13-ROBOT)
(LEFTOF12-ROBOT)
(LEFTOF10-ROBOT)
(not (NOT-LEFTOF14-ROBOT))
(not (NOT-LEFTOF13-ROBOT))
(not (NOT-LEFTOF12-ROBOT))
(not (NOT-LEFTOF11-ROBOT))
(not (NOT-LEFTOF10-ROBOT))
)
)
(when
(and
(LEFTOF10-ROBOT)
)
(and
(LEFTOF14-ROBOT)
(LEFTOF13-ROBOT)
(LEFTOF12-ROBOT)
(LEFTOF11-ROBOT)
(LEFTOF9-ROBOT)
(not (NOT-LEFTOF14-ROBOT))
(not (NOT-LEFTOF13-ROBOT))
(not (NOT-LEFTOF12-ROBOT))
(not (NOT-LEFTOF11-ROBOT))
(not (NOT-LEFTOF10-ROBOT))
(not (NOT-LEFTOF9-ROBOT))
)
)
(when
(and
(LEFTOF15-ROBOT)
)
(and
(LEFTOF14-ROBOT)
(not (NOT-LEFTOF14-ROBOT))
)
)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag213prime-))
(not (Flag212prime-))
(not (Flag195prime-))
(not (Flag33prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag391prime-))
(not (Flag389prime-))
(not (Flag387prime-))
(not (Flag386prime-))
(not (Flag384prime-))
(not (Flag383prime-))
(not (Flag382prime-))
(not (Flag381prime-))
(not (Flag379prime-))
(not (Flag378prime-))
(not (Flag377prime-))
(not (Flag376prime-))
(not (Flag375prime-))
(not (Flag373prime-))
(not (Flag372prime-))
(not (Flag371prime-))
(not (Flag370prime-))
(not (Flag369prime-))
(not (Flag367prime-))
(not (Flag366prime-))
(not (Flag365prime-))
(not (Flag364prime-))
(not (Flag363prime-))
(not (Flag362prime-))
(not (Flag360prime-))
(not (Flag359prime-))
(not (Flag358prime-))
(not (Flag357prime-))
(not (Flag356prime-))
(not (Flag355prime-))
(not (Flag354prime-))
(not (Flag353prime-))
(not (Flag351prime-))
(not (Flag350prime-))
(not (Flag349prime-))
(not (Flag348prime-))
(not (Flag347prime-))
(not (Flag346prime-))
(not (Flag345prime-))
(not (Flag344prime-))
(not (Flag343prime-))
(not (Flag342prime-))
(not (Flag340prime-))
(not (Flag339prime-))
(not (Flag338prime-))
(not (Flag337prime-))
(not (Flag336prime-))
(not (Flag335prime-))
(not (Flag334prime-))
(not (Flag333prime-))
(not (Flag332prime-))
(not (Flag331prime-))
(not (Flag329prime-))
(not (Flag328prime-))
(not (Flag327prime-))
(not (Flag326prime-))
(not (Flag325prime-))
(not (Flag324prime-))
(not (Flag323prime-))
(not (Flag322prime-))
(not (Flag321prime-))
(not (Flag320prime-))
(not (Flag319prime-))
(not (Flag318prime-))
(not (Flag316prime-))
(not (Flag315prime-))
(not (Flag314prime-))
(not (Flag313prime-))
(not (Flag312prime-))
(not (Flag311prime-))
(not (Flag310prime-))
(not (Flag309prime-))
(not (Flag308prime-))
(not (Flag307prime-))
(not (Flag306prime-))
(not (Flag305prime-))
(not (Flag304prime-))
(not (Flag302prime-))
(not (Flag301prime-))
(not (Flag300prime-))
(not (Flag299prime-))
(not (Flag298prime-))
(not (Flag297prime-))
(not (Flag296prime-))
(not (Flag295prime-))
(not (Flag294prime-))
(not (Flag293prime-))
(not (Flag292prime-))
(not (Flag291prime-))
(not (Flag289prime-))
(not (Flag288prime-))
(not (Flag287prime-))
(not (Flag286prime-))
(not (Flag285prime-))
(not (Flag284prime-))
(not (Flag283prime-))
(not (Flag282prime-))
(not (Flag281prime-))
(not (Flag280prime-))
(not (Flag279prime-))
(not (Flag278prime-))
(not (Flag277prime-))
(not (Flag276prime-))
(not (Flag275prime-))
(not (Flag273prime-))
(not (Flag272prime-))
(not (Flag271prime-))
(not (Flag270prime-))
(not (Flag269prime-))
(not (Flag268prime-))
(not (Flag267prime-))
(not (Flag266prime-))
(not (Flag265prime-))
(not (Flag264prime-))
(not (Flag263prime-))
(not (Flag262prime-))
(not (Flag261prime-))
(not (Flag260prime-))
(not (Flag259prime-))
(not (Flag257prime-))
(not (Flag256prime-))
(not (Flag255prime-))
(not (Flag254prime-))
(not (Flag253prime-))
(not (Flag252prime-))
(not (Flag251prime-))
(not (Flag250prime-))
(not (Flag249prime-))
(not (Flag248prime-))
(not (Flag247prime-))
(not (Flag246prime-))
(not (Flag245prime-))
(not (Flag244prime-))
(not (Flag243prime-))
(not (Flag242prime-))
(not (Flag241prime-))
(not (Flag240prime-))
(not (Flag239prime-))
(not (Flag238prime-))
(not (Flag237prime-))
(not (Flag236prime-))
(not (Flag235prime-))
(not (Flag234prime-))
(not (Flag233prime-))
(not (Flag232prime-))
(not (Flag231prime-))
(not (Flag230prime-))
(not (Flag229prime-))
(not (Flag228prime-))
(not (Flag227prime-))
(not (Flag226prime-))
(not (Flag225prime-))
(not (Flag224prime-))
(not (Flag223prime-))
(not (Flag222prime-))
(not (Flag221prime-))
(not (Flag220prime-))
(not (Flag219prime-))
(not (Flag218prime-))
(not (Flag217prime-))
(not (Flag216prime-))
(not (Flag215prime-))
(not (Flag214prime-))
(not (Flag211prime-))
(not (Flag210prime-))
(not (Flag209prime-))
(not (Flag208prime-))
(not (Flag207prime-))
(not (Flag206prime-))
(not (Flag205prime-))
(not (Flag204prime-))
(not (Flag203prime-))
(not (Flag202prime-))
(not (Flag201prime-))
(not (Flag200prime-))
(not (Flag199prime-))
(not (Flag4prime-))
(not (Flag390prime-))
(not (Flag388prime-))
(not (Flag385prime-))
(not (Flag380prime-))
(not (Flag374prime-))
(not (Flag368prime-))
(not (Flag361prime-))
(not (Flag352prime-))
(not (Flag341prime-))
(not (Flag330prime-))
(not (Flag317prime-))
(not (Flag303prime-))
(not (Flag290prime-))
(not (Flag274prime-))
(not (Flag258prime-))
(not (Flag197prime-))
(not (Flag196prime-))
(not (Flag193prime-))
(not (Flag192prime-))
(not (Flag190prime-))
(not (Flag189prime-))
(not (Flag188prime-))
(not (Flag186prime-))
(not (Flag185prime-))
(not (Flag184prime-))
(not (Flag183prime-))
(not (Flag182prime-))
(not (Flag180prime-))
(not (Flag179prime-))
(not (Flag178prime-))
(not (Flag177prime-))
(not (Flag176prime-))
(not (Flag174prime-))
(not (Flag173prime-))
(not (Flag172prime-))
(not (Flag171prime-))
(not (Flag170prime-))
(not (Flag169prime-))
(not (Flag168prime-))
(not (Flag166prime-))
(not (Flag165prime-))
(not (Flag164prime-))
(not (Flag163prime-))
(not (Flag162prime-))
(not (Flag161prime-))
(not (Flag160prime-))
(not (Flag158prime-))
(not (Flag157prime-))
(not (Flag156prime-))
(not (Flag155prime-))
(not (Flag154prime-))
(not (Flag153prime-))
(not (Flag152prime-))
(not (Flag151prime-))
(not (Flag150prime-))
(not (Flag148prime-))
(not (Flag147prime-))
(not (Flag146prime-))
(not (Flag145prime-))
(not (Flag144prime-))
(not (Flag143prime-))
(not (Flag142prime-))
(not (Flag141prime-))
(not (Flag140prime-))
(not (Flag139prime-))
(not (Flag137prime-))
(not (Flag136prime-))
(not (Flag135prime-))
(not (Flag134prime-))
(not (Flag133prime-))
(not (Flag132prime-))
(not (Flag131prime-))
(not (Flag130prime-))
(not (Flag129prime-))
(not (Flag128prime-))
(not (Flag127prime-))
(not (Flag126prime-))
(not (Flag124prime-))
(not (Flag123prime-))
(not (Flag122prime-))
(not (Flag121prime-))
(not (Flag120prime-))
(not (Flag119prime-))
(not (Flag118prime-))
(not (Flag117prime-))
(not (Flag116prime-))
(not (Flag115prime-))
(not (Flag114prime-))
(not (Flag112prime-))
(not (Flag111prime-))
(not (Flag110prime-))
(not (Flag109prime-))
(not (Flag108prime-))
(not (Flag107prime-))
(not (Flag106prime-))
(not (Flag105prime-))
(not (Flag104prime-))
(not (Flag103prime-))
(not (Flag102prime-))
(not (Flag101prime-))
(not (Flag100prime-))
(not (Flag99prime-))
(not (Flag97prime-))
(not (Flag96prime-))
(not (Flag95prime-))
(not (Flag94prime-))
(not (Flag93prime-))
(not (Flag92prime-))
(not (Flag91prime-))
(not (Flag90prime-))
(not (Flag89prime-))
(not (Flag88prime-))
(not (Flag87prime-))
(not (Flag86prime-))
(not (Flag85prime-))
(not (Flag84prime-))
(not (Flag83prime-))
(not (Flag81prime-))
(not (Flag80prime-))
(not (Flag79prime-))
(not (Flag78prime-))
(not (Flag77prime-))
(not (Flag76prime-))
(not (Flag75prime-))
(not (Flag74prime-))
(not (Flag73prime-))
(not (Flag72prime-))
(not (Flag71prime-))
(not (Flag70prime-))
(not (Flag69prime-))
(not (Flag68prime-))
(not (Flag66prime-))
(not (Flag65prime-))
(not (Flag64prime-))
(not (Flag63prime-))
(not (Flag62prime-))
(not (Flag61prime-))
(not (Flag60prime-))
(not (Flag59prime-))
(not (Flag58prime-))
(not (Flag57prime-))
(not (Flag56prime-))
(not (Flag55prime-))
(not (Flag54prime-))
(not (Flag53prime-))
(not (Flag52prime-))
(not (Flag51prime-))
(not (Flag49prime-))
(not (Flag48prime-))
(not (Flag47prime-))
(not (Flag46prime-))
(not (Flag45prime-))
(not (Flag44prime-))
(not (Flag43prime-))
(not (Flag42prime-))
(not (Flag41prime-))
(not (Flag40prime-))
(not (Flag39prime-))
(not (Flag38prime-))
(not (Flag37prime-))
(not (Flag36prime-))
(not (Flag35prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag31prime-))
(not (Flag30prime-))
(not (Flag29prime-))
(not (Flag28prime-))
(not (Flag27prime-))
(not (Flag26prime-))
(not (Flag25prime-))
(not (Flag24prime-))
(not (Flag23prime-))
(not (Flag22prime-))
(not (Flag21prime-))
(not (Flag20prime-))
(not (Flag19prime-))
(not (Flag18prime-))
(not (Flag17prime-))
(not (Flag16prime-))
(not (Flag15prime-))
(not (Flag14prime-))
(not (Flag13prime-))
(not (Flag12prime-))
(not (Flag11prime-))
(not (Flag10prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag1prime-))
(not (Flag198prime-))
(not (Flag194prime-))
(not (Flag191prime-))
(not (Flag187prime-))
(not (Flag181prime-))
(not (Flag175prime-))
(not (Flag167prime-))
(not (Flag159prime-))
(not (Flag149prime-))
(not (Flag138prime-))
(not (Flag125prime-))
(not (Flag113prime-))
(not (Flag98prime-))
(not (Flag82prime-))
(not (Flag67prime-))
(not (Flag50prime-))
)
)
(:action MOVERIGHT-ROBOT
:parameters ()
:precondition
(and
(Flag4-)
(Flag4prime-)
(Flag194prime-)
(Flag191prime-)
(Flag187prime-)
(Flag181prime-)
(Flag175prime-)
(Flag167prime-)
(Flag159prime-)
(Flag149prime-)
(Flag138prime-)
(Flag125prime-)
(Flag113prime-)
(Flag98prime-)
(Flag82prime-)
(Flag67prime-)
(Flag50prime-)
(Flag33prime-)
(Flag32prime-)
(Flag31prime-)
(Flag30prime-)
(Flag29prime-)
(Flag28prime-)
(Flag27prime-)
(Flag26prime-)
(Flag25prime-)
(Flag24prime-)
(Flag23prime-)
(Flag22prime-)
(Flag21prime-)
(Flag20prime-)
(Flag19prime-)
(Flag18prime-)
(Flag17prime-)
(Flag16prime-)
(Flag15prime-)
(Flag14prime-)
(Flag13prime-)
(Flag12prime-)
(Flag11prime-)
(Flag10prime-)
(Flag9prime-)
(Flag8prime-)
(Flag7prime-)
(Flag6prime-)
(Flag5prime-)
)
:effect
(and
(when
(and
(Flag194-)
)
(and
(COLUMN15-ROBOT)
(not (NOT-COLUMN15-ROBOT))
)
)
(when
(and
(Flag191-)
)
(and
(COLUMN14-ROBOT)
(not (NOT-COLUMN14-ROBOT))
)
)
(when
(and
(Flag187-)
)
(and
(COLUMN13-ROBOT)
(not (NOT-COLUMN13-ROBOT))
)
)
(when
(and
(Flag181-)
)
(and
(COLUMN12-ROBOT)
(not (NOT-COLUMN12-ROBOT))
)
)
(when
(and
(Flag175-)
)
(and
(COLUMN11-ROBOT)
(not (NOT-COLUMN11-ROBOT))
)
)
(when
(and
(Flag167-)
)
(and
(COLUMN10-ROBOT)
(not (NOT-COLUMN10-ROBOT))
)
)
(when
(and
(Flag159-)
)
(and
(COLUMN9-ROBOT)
(not (NOT-COLUMN9-ROBOT))
)
)
(when
(and
(Flag149-)
)
(and
(COLUMN8-ROBOT)
(not (NOT-COLUMN8-ROBOT))
)
)
(when
(and
(Flag138-)
)
(and
(COLUMN7-ROBOT)
(not (NOT-COLUMN7-ROBOT))
)
)
(when
(and
(Flag125-)
)
(and
(COLUMN6-ROBOT)
(not (NOT-COLUMN6-ROBOT))
)
)
(when
(and
(Flag113-)
)
(and
(COLUMN5-ROBOT)
(not (NOT-COLUMN5-ROBOT))
)
)
(when
(and
(Flag98-)
)
(and
(COLUMN4-ROBOT)
(not (NOT-COLUMN4-ROBOT))
)
)
(when
(and
(Flag82-)
)
(and
(COLUMN3-ROBOT)
(not (NOT-COLUMN3-ROBOT))
)
)
(when
(and
(Flag67-)
)
(and
(COLUMN2-ROBOT)
(not (NOT-COLUMN2-ROBOT))
)
)
(when
(and
(Flag50-)
)
(and
(COLUMN1-ROBOT)
(not (NOT-COLUMN1-ROBOT))
)
)
(when
(and
(COLUMN14-ROBOT)
)
(and
(COLUMN15-ROBOT)
(NOT-COLUMN14-ROBOT)
(not (NOT-COLUMN15-ROBOT))
(not (COLUMN14-ROBOT))
)
)
(when
(and
(COLUMN13-ROBOT)
)
(and
(COLUMN14-ROBOT)
(NOT-COLUMN13-ROBOT)
(not (NOT-COLUMN14-ROBOT))
(not (COLUMN13-ROBOT))
)
)
(when
(and
(COLUMN12-ROBOT)
)
(and
(COLUMN13-ROBOT)
(NOT-COLUMN12-ROBOT)
(not (NOT-COLUMN13-ROBOT))
(not (COLUMN12-ROBOT))
)
)
(when
(and
(COLUMN11-ROBOT)
)
(and
(COLUMN12-ROBOT)
(NOT-COLUMN11-ROBOT)
(not (NOT-COLUMN12-ROBOT))
(not (COLUMN11-ROBOT))
)
)
(when
(and
(COLUMN10-ROBOT)
)
(and
(COLUMN11-ROBOT)
(NOT-COLUMN10-ROBOT)
(not (NOT-COLUMN11-ROBOT))
(not (COLUMN10-ROBOT))
)
)
(when
(and
(COLUMN9-ROBOT)
)
(and
(COLUMN10-ROBOT)
(NOT-COLUMN9-ROBOT)
(not (NOT-COLUMN10-ROBOT))
(not (COLUMN9-ROBOT))
)
)
(when
(and
(COLUMN8-ROBOT)
)
(and
(COLUMN9-ROBOT)
(NOT-COLUMN8-ROBOT)
(not (NOT-COLUMN9-ROBOT))
(not (COLUMN8-ROBOT))
)
)
(when
(and
(COLUMN7-ROBOT)
)
(and
(COLUMN8-ROBOT)
(NOT-COLUMN7-ROBOT)
(not (NOT-COLUMN8-ROBOT))
(not (COLUMN7-ROBOT))
)
)
(when
(and
(COLUMN6-ROBOT)
)
(and
(COLUMN7-ROBOT)
(NOT-COLUMN6-ROBOT)
(not (NOT-COLUMN7-ROBOT))
(not (COLUMN6-ROBOT))
)
)
(when
(and
(COLUMN5-ROBOT)
)
(and
(COLUMN6-ROBOT)
(NOT-COLUMN5-ROBOT)
(not (NOT-COLUMN6-ROBOT))
(not (COLUMN5-ROBOT))
)
)
(when
(and
(COLUMN4-ROBOT)
)
(and
(COLUMN5-ROBOT)
(NOT-COLUMN4-ROBOT)
(not (NOT-COLUMN5-ROBOT))
(not (COLUMN4-ROBOT))
)
)
(when
(and
(COLUMN3-ROBOT)
)
(and
(COLUMN4-ROBOT)
(NOT-COLUMN3-ROBOT)
(not (NOT-COLUMN4-ROBOT))
(not (COLUMN3-ROBOT))
)
)
(when
(and
(COLUMN2-ROBOT)
)
(and
(COLUMN3-ROBOT)
(NOT-COLUMN2-ROBOT)
(not (NOT-COLUMN3-ROBOT))
(not (COLUMN2-ROBOT))
)
)
(when
(and
(COLUMN1-ROBOT)
)
(and
(COLUMN2-ROBOT)
(NOT-COLUMN1-ROBOT)
(not (NOT-COLUMN2-ROBOT))
(not (COLUMN1-ROBOT))
)
)
(when
(and
(COLUMN0-ROBOT)
)
(and
(COLUMN1-ROBOT)
(NOT-COLUMN0-ROBOT)
(not (NOT-COLUMN1-ROBOT))
(not (COLUMN0-ROBOT))
)
)
(when
(and
(Flag33-)
)
(and
(LEFTOF16-ROBOT)
(NOT-LEFTOF15-ROBOT)
(not (NOT-LEFTOF16-ROBOT))
(not (LEFTOF15-ROBOT))
)
)
(when
(and
(Flag32-)
)
(and
(LEFTOF15-ROBOT)
(NOT-LEFTOF14-ROBOT)
(not (NOT-LEFTOF15-ROBOT))
(not (LEFTOF14-ROBOT))
)
)
(when
(and
(Flag31-)
)
(and
(LEFTOF14-ROBOT)
(NOT-LEFTOF13-ROBOT)
(not (NOT-LEFTOF14-ROBOT))
(not (LEFTOF13-ROBOT))
)
)
(when
(and
(Flag30-)
)
(and
(LEFTOF13-ROBOT)
(NOT-LEFTOF12-ROBOT)
(not (NOT-LEFTOF13-ROBOT))
(not (LEFTOF12-ROBOT))
)
)
(when
(and
(Flag29-)
)
(and
(LEFTOF12-ROBOT)
(NOT-LEFTOF11-ROBOT)
(not (NOT-LEFTOF12-ROBOT))
(not (LEFTOF11-ROBOT))
)
)
(when
(and
(Flag28-)
)
(and
(LEFTOF11-ROBOT)
(NOT-LEFTOF10-ROBOT)
(not (NOT-LEFTOF11-ROBOT))
(not (LEFTOF10-ROBOT))
)
)
(when
(and
(Flag27-)
)
(and
(LEFTOF10-ROBOT)
(NOT-LEFTOF9-ROBOT)
(not (NOT-LEFTOF10-ROBOT))
(not (LEFTOF9-ROBOT))
)
)
(when
(and
(Flag26-)
)
(and
(LEFTOF9-ROBOT)
(NOT-LEFTOF8-ROBOT)
(not (NOT-LEFTOF9-ROBOT))
(not (LEFTOF8-ROBOT))
)
)
(when
(and
(Flag25-)
)
(and
(LEFTOF8-ROBOT)
(NOT-LEFTOF7-ROBOT)
(not (NOT-LEFTOF8-ROBOT))
(not (LEFTOF7-ROBOT))
)
)
(when
(and
(Flag24-)
)
(and
(LEFTOF7-ROBOT)
(NOT-LEFTOF6-ROBOT)
(not (NOT-LEFTOF7-ROBOT))
(not (LEFTOF6-ROBOT))
)
)
(when
(and
(Flag23-)
)
(and
(LEFTOF6-ROBOT)
(NOT-LEFTOF5-ROBOT)
(not (NOT-LEFTOF6-ROBOT))
(not (LEFTOF5-ROBOT))
)
)
(when
(and
(Flag22-)
)
(and
(LEFTOF5-ROBOT)
(NOT-LEFTOF4-ROBOT)
(not (NOT-LEFTOF5-ROBOT))
(not (LEFTOF4-ROBOT))
)
)
(when
(and
(Flag21-)
)
(and
(LEFTOF4-ROBOT)
(NOT-LEFTOF3-ROBOT)
(not (NOT-LEFTOF4-ROBOT))
(not (LEFTOF3-ROBOT))
)
)
(when
(and
(Flag20-)
)
(and
(LEFTOF3-ROBOT)
(NOT-LEFTOF2-ROBOT)
(not (NOT-LEFTOF3-ROBOT))
(not (LEFTOF2-ROBOT))
)
)
(when
(and
(LEFTOF1-ROBOT)
)
(and
(LEFTOF2-ROBOT)
(NOT-LEFTOF1-ROBOT)
(not (NOT-LEFTOF2-ROBOT))
(not (LEFTOF1-ROBOT))
)
)
(when
(and
(Flag19-)
)
(and
(RIGHTOF15-ROBOT)
(not (NOT-RIGHTOF15-ROBOT))
)
)
(when
(and
(Flag18-)
)
(and
(RIGHTOF14-ROBOT)
(not (NOT-RIGHTOF14-ROBOT))
)
)
(when
(and
(Flag17-)
)
(and
(RIGHTOF13-ROBOT)
(not (NOT-RIGHTOF13-ROBOT))
)
)
(when
(and
(Flag16-)
)
(and
(RIGHTOF12-ROBOT)
(not (NOT-RIGHTOF12-ROBOT))
)
)
(when
(and
(Flag15-)
)
(and
(RIGHTOF11-ROBOT)
(not (NOT-RIGHTOF11-ROBOT))
)
)
(when
(and
(Flag14-)
)
(and
(RIGHTOF10-ROBOT)
(not (NOT-RIGHTOF10-ROBOT))
)
)
(when
(and
(Flag13-)
)
(and
(RIGHTOF9-ROBOT)
(not (NOT-RIGHTOF9-ROBOT))
)
)
(when
(and
(Flag12-)
)
(and
(RIGHTOF8-ROBOT)
(not (NOT-RIGHTOF8-ROBOT))
)
)
(when
(and
(Flag11-)
)
(and
(RIGHTOF7-ROBOT)
(not (NOT-RIGHTOF7-ROBOT))
)
)
(when
(and
(Flag10-)
)
(and
(RIGHTOF6-ROBOT)
(not (NOT-RIGHTOF6-ROBOT))
)
)
(when
(and
(Flag9-)
)
(and
(RIGHTOF5-ROBOT)
(not (NOT-RIGHTOF5-ROBOT))
)
)
(when
(and
(Flag8-)
)
(and
(RIGHTOF4-ROBOT)
(not (NOT-RIGHTOF4-ROBOT))
)
)
(when
(and
(Flag7-)
)
(and
(RIGHTOF3-ROBOT)
(not (NOT-RIGHTOF3-ROBOT))
)
)
(when
(and
(Flag6-)
)
(and
(RIGHTOF2-ROBOT)
(not (NOT-RIGHTOF2-ROBOT))
)
)
(when
(and
(Flag5-)
)
(and
(RIGHTOF1-ROBOT)
(not (NOT-RIGHTOF1-ROBOT))
)
)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag213prime-))
(not (Flag212prime-))
(not (Flag195prime-))
(not (Flag33prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag391prime-))
(not (Flag389prime-))
(not (Flag387prime-))
(not (Flag386prime-))
(not (Flag384prime-))
(not (Flag383prime-))
(not (Flag382prime-))
(not (Flag381prime-))
(not (Flag379prime-))
(not (Flag378prime-))
(not (Flag377prime-))
(not (Flag376prime-))
(not (Flag375prime-))
(not (Flag373prime-))
(not (Flag372prime-))
(not (Flag371prime-))
(not (Flag370prime-))
(not (Flag369prime-))
(not (Flag367prime-))
(not (Flag366prime-))
(not (Flag365prime-))
(not (Flag364prime-))
(not (Flag363prime-))
(not (Flag362prime-))
(not (Flag360prime-))
(not (Flag359prime-))
(not (Flag358prime-))
(not (Flag357prime-))
(not (Flag356prime-))
(not (Flag355prime-))
(not (Flag354prime-))
(not (Flag353prime-))
(not (Flag351prime-))
(not (Flag350prime-))
(not (Flag349prime-))
(not (Flag348prime-))
(not (Flag347prime-))
(not (Flag346prime-))
(not (Flag345prime-))
(not (Flag344prime-))
(not (Flag343prime-))
(not (Flag342prime-))
(not (Flag340prime-))
(not (Flag339prime-))
(not (Flag338prime-))
(not (Flag337prime-))
(not (Flag336prime-))
(not (Flag335prime-))
(not (Flag334prime-))
(not (Flag333prime-))
(not (Flag332prime-))
(not (Flag331prime-))
(not (Flag329prime-))
(not (Flag328prime-))
(not (Flag327prime-))
(not (Flag326prime-))
(not (Flag325prime-))
(not (Flag324prime-))
(not (Flag323prime-))
(not (Flag322prime-))
(not (Flag321prime-))
(not (Flag320prime-))
(not (Flag319prime-))
(not (Flag318prime-))
(not (Flag316prime-))
(not (Flag315prime-))
(not (Flag314prime-))
(not (Flag313prime-))
(not (Flag312prime-))
(not (Flag311prime-))
(not (Flag310prime-))
(not (Flag309prime-))
(not (Flag308prime-))
(not (Flag307prime-))
(not (Flag306prime-))
(not (Flag305prime-))
(not (Flag304prime-))
(not (Flag302prime-))
(not (Flag301prime-))
(not (Flag300prime-))
(not (Flag299prime-))
(not (Flag298prime-))
(not (Flag297prime-))
(not (Flag296prime-))
(not (Flag295prime-))
(not (Flag294prime-))
(not (Flag293prime-))
(not (Flag292prime-))
(not (Flag291prime-))
(not (Flag289prime-))
(not (Flag288prime-))
(not (Flag287prime-))
(not (Flag286prime-))
(not (Flag285prime-))
(not (Flag284prime-))
(not (Flag283prime-))
(not (Flag282prime-))
(not (Flag281prime-))
(not (Flag280prime-))
(not (Flag279prime-))
(not (Flag278prime-))
(not (Flag277prime-))
(not (Flag276prime-))
(not (Flag275prime-))
(not (Flag273prime-))
(not (Flag272prime-))
(not (Flag271prime-))
(not (Flag270prime-))
(not (Flag269prime-))
(not (Flag268prime-))
(not (Flag267prime-))
(not (Flag266prime-))
(not (Flag265prime-))
(not (Flag264prime-))
(not (Flag263prime-))
(not (Flag262prime-))
(not (Flag261prime-))
(not (Flag260prime-))
(not (Flag259prime-))
(not (Flag257prime-))
(not (Flag256prime-))
(not (Flag255prime-))
(not (Flag254prime-))
(not (Flag253prime-))
(not (Flag252prime-))
(not (Flag251prime-))
(not (Flag250prime-))
(not (Flag249prime-))
(not (Flag248prime-))
(not (Flag247prime-))
(not (Flag246prime-))
(not (Flag245prime-))
(not (Flag244prime-))
(not (Flag243prime-))
(not (Flag242prime-))
(not (Flag241prime-))
(not (Flag240prime-))
(not (Flag239prime-))
(not (Flag238prime-))
(not (Flag237prime-))
(not (Flag236prime-))
(not (Flag235prime-))
(not (Flag234prime-))
(not (Flag233prime-))
(not (Flag232prime-))
(not (Flag231prime-))
(not (Flag230prime-))
(not (Flag229prime-))
(not (Flag228prime-))
(not (Flag227prime-))
(not (Flag226prime-))
(not (Flag225prime-))
(not (Flag224prime-))
(not (Flag223prime-))
(not (Flag222prime-))
(not (Flag221prime-))
(not (Flag220prime-))
(not (Flag219prime-))
(not (Flag218prime-))
(not (Flag217prime-))
(not (Flag216prime-))
(not (Flag215prime-))
(not (Flag214prime-))
(not (Flag211prime-))
(not (Flag210prime-))
(not (Flag209prime-))
(not (Flag208prime-))
(not (Flag207prime-))
(not (Flag206prime-))
(not (Flag205prime-))
(not (Flag204prime-))
(not (Flag203prime-))
(not (Flag202prime-))
(not (Flag201prime-))
(not (Flag200prime-))
(not (Flag199prime-))
(not (Flag4prime-))
(not (Flag390prime-))
(not (Flag388prime-))
(not (Flag385prime-))
(not (Flag380prime-))
(not (Flag374prime-))
(not (Flag368prime-))
(not (Flag361prime-))
(not (Flag352prime-))
(not (Flag341prime-))
(not (Flag330prime-))
(not (Flag317prime-))
(not (Flag303prime-))
(not (Flag290prime-))
(not (Flag274prime-))
(not (Flag258prime-))
(not (Flag197prime-))
(not (Flag196prime-))
(not (Flag193prime-))
(not (Flag192prime-))
(not (Flag190prime-))
(not (Flag189prime-))
(not (Flag188prime-))
(not (Flag186prime-))
(not (Flag185prime-))
(not (Flag184prime-))
(not (Flag183prime-))
(not (Flag182prime-))
(not (Flag180prime-))
(not (Flag179prime-))
(not (Flag178prime-))
(not (Flag177prime-))
(not (Flag176prime-))
(not (Flag174prime-))
(not (Flag173prime-))
(not (Flag172prime-))
(not (Flag171prime-))
(not (Flag170prime-))
(not (Flag169prime-))
(not (Flag168prime-))
(not (Flag166prime-))
(not (Flag165prime-))
(not (Flag164prime-))
(not (Flag163prime-))
(not (Flag162prime-))
(not (Flag161prime-))
(not (Flag160prime-))
(not (Flag158prime-))
(not (Flag157prime-))
(not (Flag156prime-))
(not (Flag155prime-))
(not (Flag154prime-))
(not (Flag153prime-))
(not (Flag152prime-))
(not (Flag151prime-))
(not (Flag150prime-))
(not (Flag148prime-))
(not (Flag147prime-))
(not (Flag146prime-))
(not (Flag145prime-))
(not (Flag144prime-))
(not (Flag143prime-))
(not (Flag142prime-))
(not (Flag141prime-))
(not (Flag140prime-))
(not (Flag139prime-))
(not (Flag137prime-))
(not (Flag136prime-))
(not (Flag135prime-))
(not (Flag134prime-))
(not (Flag133prime-))
(not (Flag132prime-))
(not (Flag131prime-))
(not (Flag130prime-))
(not (Flag129prime-))
(not (Flag128prime-))
(not (Flag127prime-))
(not (Flag126prime-))
(not (Flag124prime-))
(not (Flag123prime-))
(not (Flag122prime-))
(not (Flag121prime-))
(not (Flag120prime-))
(not (Flag119prime-))
(not (Flag118prime-))
(not (Flag117prime-))
(not (Flag116prime-))
(not (Flag115prime-))
(not (Flag114prime-))
(not (Flag112prime-))
(not (Flag111prime-))
(not (Flag110prime-))
(not (Flag109prime-))
(not (Flag108prime-))
(not (Flag107prime-))
(not (Flag106prime-))
(not (Flag105prime-))
(not (Flag104prime-))
(not (Flag103prime-))
(not (Flag102prime-))
(not (Flag101prime-))
(not (Flag100prime-))
(not (Flag99prime-))
(not (Flag97prime-))
(not (Flag96prime-))
(not (Flag95prime-))
(not (Flag94prime-))
(not (Flag93prime-))
(not (Flag92prime-))
(not (Flag91prime-))
(not (Flag90prime-))
(not (Flag89prime-))
(not (Flag88prime-))
(not (Flag87prime-))
(not (Flag86prime-))
(not (Flag85prime-))
(not (Flag84prime-))
(not (Flag83prime-))
(not (Flag81prime-))
(not (Flag80prime-))
(not (Flag79prime-))
(not (Flag78prime-))
(not (Flag77prime-))
(not (Flag76prime-))
(not (Flag75prime-))
(not (Flag74prime-))
(not (Flag73prime-))
(not (Flag72prime-))
(not (Flag71prime-))
(not (Flag70prime-))
(not (Flag69prime-))
(not (Flag68prime-))
(not (Flag66prime-))
(not (Flag65prime-))
(not (Flag64prime-))
(not (Flag63prime-))
(not (Flag62prime-))
(not (Flag61prime-))
(not (Flag60prime-))
(not (Flag59prime-))
(not (Flag58prime-))
(not (Flag57prime-))
(not (Flag56prime-))
(not (Flag55prime-))
(not (Flag54prime-))
(not (Flag53prime-))
(not (Flag52prime-))
(not (Flag51prime-))
(not (Flag49prime-))
(not (Flag48prime-))
(not (Flag47prime-))
(not (Flag46prime-))
(not (Flag45prime-))
(not (Flag44prime-))
(not (Flag43prime-))
(not (Flag42prime-))
(not (Flag41prime-))
(not (Flag40prime-))
(not (Flag39prime-))
(not (Flag38prime-))
(not (Flag37prime-))
(not (Flag36prime-))
(not (Flag35prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag31prime-))
(not (Flag30prime-))
(not (Flag29prime-))
(not (Flag28prime-))
(not (Flag27prime-))
(not (Flag26prime-))
(not (Flag25prime-))
(not (Flag24prime-))
(not (Flag23prime-))
(not (Flag22prime-))
(not (Flag21prime-))
(not (Flag20prime-))
(not (Flag19prime-))
(not (Flag18prime-))
(not (Flag17prime-))
(not (Flag16prime-))
(not (Flag15prime-))
(not (Flag14prime-))
(not (Flag13prime-))
(not (Flag12prime-))
(not (Flag11prime-))
(not (Flag10prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag1prime-))
(not (Flag198prime-))
(not (Flag194prime-))
(not (Flag191prime-))
(not (Flag187prime-))
(not (Flag181prime-))
(not (Flag175prime-))
(not (Flag167prime-))
(not (Flag159prime-))
(not (Flag149prime-))
(not (Flag138prime-))
(not (Flag125prime-))
(not (Flag113prime-))
(not (Flag98prime-))
(not (Flag82prime-))
(not (Flag67prime-))
(not (Flag50prime-))
)
)
(:action Prim1Action-1
:parameters ()
:precondition
(and
(not (ROW1-ROBOT))
)
:effect
(and
(Flag1prime-)
(not (Flag1-))
)

)
(:action CHECKCONSISTENCYACTION
:parameters ()
:precondition
(and
(NOT-ERROR-)
(CHECKCONSISTENCY-)
)
:effect
(and
(when
(and
(LEFTOF9-ROBOT)
(RIGHTOF9-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF11-ROBOT)
(LEFTOF4-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF7-ROBOT)
(ABOVEOF15-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF8-ROBOT)
(LEFTOF3-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF5-ROBOT)
(RIGHTOF15-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF14-ROBOT)
(BELOWOF6-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF10-ROBOT)
(ABOVEOF11-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF6-ROBOT)
(ABOVEOF15-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF15-ROBOT)
(BELOWOF3-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF13-ROBOT)
(LEFTOF2-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF11-ROBOT)
(BELOWOF3-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF14-ROBOT)
(BELOWOF2-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF12-ROBOT)
(BELOWOF5-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF13-ROBOT)
(LEFTOF1-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF2-ROBOT)
(RIGHTOF9-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF9-ROBOT)
(LEFTOF5-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF1-ROBOT)
(ABOVEOF13-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF11-ROBOT)
(LEFTOF2-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF15-ROBOT)
(LEFTOF2-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF12-ROBOT)
(BELOWOF12-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF8-ROBOT)
(ABOVEOF12-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF3-ROBOT)
(ABOVEOF9-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF9-ROBOT)
(LEFTOF1-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF3-ROBOT)
(RIGHTOF14-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF5-ROBOT)
(ABOVEOF11-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF4-ROBOT)
(RIGHTOF10-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF9-ROBOT)
(RIGHTOF15-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF4-ROBOT)
(ABOVEOF13-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF8-ROBOT)
(LEFTOF2-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF1-ROBOT)
(RIGHTOF11-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF2-ROBOT)
(RIGHTOF14-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF12-ROBOT)
(BELOWOF7-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF14-ROBOT)
(LEFTOF5-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF15-ROBOT)
(LEFTOF1-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF13-ROBOT)
(BELOWOF7-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF12-ROBOT)
(LEFTOF4-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF6-ROBOT)
(RIGHTOF15-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF14-ROBOT)
(BELOWOF4-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF15-ROBOT)
(BELOWOF10-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF5-ROBOT)
(ABOVEOF15-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF11-ROBOT)
(LEFTOF3-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF8-ROBOT)
(RIGHTOF14-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF9-ROBOT)
(BELOWOF1-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF10-ROBOT)
(RIGHTOF13-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF4-ROBOT)
(ABOVEOF15-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF14-ROBOT)
(LEFTOF1-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF10-ROBOT)
(LEFTOF2-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF3-ROBOT)
(RIGHTOF7-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF7-ROBOT)
(RIGHTOF13-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF8-ROBOT)
(BELOWOF6-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF12-ROBOT)
(LEFTOF5-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF3-ROBOT)
(RIGHTOF13-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF12-ROBOT)
(LEFTOF2-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF2-ROBOT)
(RIGHTOF4-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF3-ROBOT)
(RIGHTOF9-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF8-ROBOT)
(RIGHTOF13-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF6-ROBOT)
(LEFTOF2-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF5-ROBOT)
(BELOWOF1-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF11-ROBOT)
(LEFTOF8-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF1-ROBOT)
(RIGHTOF8-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF5-ROBOT)
(ABOVEOF5-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF15-ROBOT)
(BELOWOF1-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF5-ROBOT)
(RIGHTOF10-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF12-ROBOT)
(LEFTOF11-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF4-ROBOT)
(RIGHTOF14-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF3-ROBOT)
(ABOVEOF4-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF7-ROBOT)
(RIGHTOF9-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF2-ROBOT)
(ABOVEOF15-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF13-ROBOT)
(LEFTOF4-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF14-ROBOT)
(BELOWOF9-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF4-ROBOT)
(ABOVEOF11-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF10-ROBOT)
(LEFTOF8-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF1-ROBOT)
(ABOVEOF10-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF11-ROBOT)
(ABOVEOF15-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF12-ROBOT)
(BELOWOF3-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF3-ROBOT)
(ABOVEOF14-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF7-ROBOT)
(BELOWOF2-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF14-ROBOT)
(BELOWOF1-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF15-ROBOT)
(LEFTOF7-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF2-ROBOT)
(ABOVEOF5-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF12-ROBOT)
(BELOWOF2-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF10-ROBOT)
(LEFTOF3-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF15-ROBOT)
(LEFTOF11-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF9-ROBOT)
(BELOWOF8-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF6-ROBOT)
(BELOWOF1-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF12-ROBOT)
(LEFTOF12-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF8-ROBOT)
(ABOVEOF14-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF13-ROBOT)
(LEFTOF9-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF13-ROBOT)
(BELOWOF3-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF8-ROBOT)
(RIGHTOF9-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF5-ROBOT)
(RIGHTOF8-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF10-ROBOT)
(RIGHTOF14-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF6-ROBOT)
(LEFTOF5-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF14-ROBOT)
(RIGHTOF15-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF9-ROBOT)
(LEFTOF4-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF6-ROBOT)
(ABOVEOF10-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF4-ROBOT)
(RIGHTOF4-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF2-ROBOT)
(ABOVEOF13-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF12-ROBOT)
(BELOWOF10-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF2-ROBOT)
(ABOVEOF10-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF9-ROBOT)
(LEFTOF6-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF13-ROBOT)
(LEFTOF6-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF1-ROBOT)
(ABOVEOF11-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF8-ROBOT)
(ABOVEOF15-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF3-ROBOT)
(BELOWOF2-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF5-ROBOT)
(LEFTOF3-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF11-ROBOT)
(LEFTOF5-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF10-ROBOT)
(BELOWOF5-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF9-ROBOT)
(BELOWOF4-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF14-ROBOT)
(LEFTOF6-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF14-ROBOT)
(LEFTOF11-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF11-ROBOT)
(LEFTOF11-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF3-ROBOT)
(RIGHTOF12-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF13-ROBOT)
(BELOWOF5-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF8-ROBOT)
(BELOWOF1-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF5-ROBOT)
(ABOVEOF14-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF5-ROBOT)
(RIGHTOF7-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF3-ROBOT)
(RIGHTOF15-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF12-ROBOT)
(LEFTOF6-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF8-ROBOT)
(BELOWOF2-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF9-ROBOT)
(RIGHTOF12-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF11-ROBOT)
(BELOWOF2-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF6-ROBOT)
(LEFTOF1-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF12-ROBOT)
(LEFTOF10-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF7-ROBOT)
(ABOVEOF11-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF6-ROBOT)
(ABOVEOF9-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF5-ROBOT)
(ABOVEOF9-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF13-ROBOT)
(BELOWOF6-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF6-ROBOT)
(LEFTOF4-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF9-ROBOT)
(ABOVEOF15-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF14-ROBOT)
(BELOWOF7-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF15-ROBOT)
(BELOWOF15-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF9-ROBOT)
(BELOWOF2-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF14-ROBOT)
(BELOWOF12-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF7-ROBOT)
(RIGHTOF12-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF2-ROBOT)
(LEFTOF1-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF7-ROBOT)
(RIGHTOF14-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF4-ROBOT)
(RIGHTOF15-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF6-ROBOT)
(RIGHTOF10-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF4-ROBOT)
(RIGHTOF8-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF14-ROBOT)
(BELOWOF10-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF12-ROBOT)
(BELOWOF9-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF3-ROBOT)
(LEFTOF2-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF6-ROBOT)
(ABOVEOF11-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF7-ROBOT)
(RIGHTOF10-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF4-ROBOT)
(ABOVEOF12-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF12-ROBOT)
(BELOWOF6-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF8-ROBOT)
(ABOVEOF13-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF7-ROBOT)
(ABOVEOF7-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF12-ROBOT)
(LEFTOF1-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF2-ROBOT)
(RIGHTOF7-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF1-ROBOT)
(ABOVEOF3-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF2-ROBOT)
(ABOVEOF6-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF1-ROBOT)
(ABOVEOF12-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF4-ROBOT)
(ABOVEOF7-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF7-ROBOT)
(ABOVEOF10-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF13-ROBOT)
(ABOVEOF15-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF14-ROBOT)
(ABOVEOF15-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF8-ROBOT)
(BELOWOF7-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF4-ROBOT)
(BELOWOF4-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF6-ROBOT)
(LEFTOF6-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF8-ROBOT)
(ABOVEOF11-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF13-ROBOT)
(BELOWOF12-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF10-ROBOT)
(ABOVEOF13-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF10-ROBOT)
(RIGHTOF15-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF7-ROBOT)
(LEFTOF1-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF8-ROBOT)
(BELOWOF8-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF11-ROBOT)
(ABOVEOF13-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF10-ROBOT)
(BELOWOF3-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF9-ROBOT)
(BELOWOF7-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF13-ROBOT)
(RIGHTOF15-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF12-ROBOT)
(RIGHTOF15-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF6-ROBOT)
(LEFTOF3-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF14-ROBOT)
(LEFTOF12-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF8-ROBOT)
(ABOVEOF10-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF3-ROBOT)
(ABOVEOF8-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF5-ROBOT)
(LEFTOF1-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF9-ROBOT)
(RIGHTOF10-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF11-ROBOT)
(LEFTOF7-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF10-ROBOT)
(BELOWOF4-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF5-ROBOT)
(RIGHTOF13-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF5-ROBOT)
(LEFTOF2-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF4-ROBOT)
(ABOVEOF6-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF11-ROBOT)
(LEFTOF6-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF1-ROBOT)
(RIGHTOF10-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF11-ROBOT)
(LEFTOF9-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF8-ROBOT)
(RIGHTOF15-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF14-ROBOT)
(LEFTOF9-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF2-ROBOT)
(ABOVEOF2-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF9-ROBOT)
(ABOVEOF13-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF13-ROBOT)
(BELOWOF13-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF6-ROBOT)
(BELOWOF5-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF4-ROBOT)
(BELOWOF1-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF8-ROBOT)
(RIGHTOF12-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF14-ROBOT)
(BELOWOF14-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF11-ROBOT)
(BELOWOF11-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF11-ROBOT)
(BELOWOF9-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF7-ROBOT)
(BELOWOF1-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF6-ROBOT)
(BELOWOF6-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF7-ROBOT)
(RIGHTOF8-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF3-ROBOT)
(ABOVEOF7-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF12-ROBOT)
(RIGHTOF13-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF3-ROBOT)
(BELOWOF3-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF4-ROBOT)
(RIGHTOF5-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF4-ROBOT)
(BELOWOF2-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF7-ROBOT)
(LEFTOF6-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF1-ROBOT)
(ABOVEOF2-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF4-ROBOT)
(RIGHTOF7-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF8-ROBOT)
(BELOWOF4-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF11-ROBOT)
(LEFTOF10-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF13-ROBOT)
(ABOVEOF14-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF3-ROBOT)
(LEFTOF1-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF11-ROBOT)
(ABOVEOF14-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF4-ROBOT)
(LEFTOF1-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF13-ROBOT)
(RIGHTOF13-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF1-ROBOT)
(LEFTOF1-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF6-ROBOT)
(ABOVEOF7-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF14-ROBOT)
(LEFTOF13-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF12-ROBOT)
(BELOWOF11-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF6-ROBOT)
(BELOWOF3-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF13-ROBOT)
(LEFTOF11-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF15-ROBOT)
(LEFTOF15-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF8-ROBOT)
(RIGHTOF8-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF5-ROBOT)
(ABOVEOF8-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF1-ROBOT)
(BELOWOF1-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF4-ROBOT)
(LEFTOF3-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF4-ROBOT)
(ABOVEOF5-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF14-ROBOT)
(LEFTOF14-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF12-ROBOT)
(ABOVEOF15-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF9-ROBOT)
(ABOVEOF9-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF7-ROBOT)
(RIGHTOF7-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF9-ROBOT)
(ABOVEOF10-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF5-ROBOT)
(ABOVEOF7-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF3-ROBOT)
(ABOVEOF5-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF8-ROBOT)
(LEFTOF6-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF5-ROBOT)
(RIGHTOF5-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF10-ROBOT)
(LEFTOF10-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF10-ROBOT)
(ABOVEOF10-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF3-ROBOT)
(LEFTOF3-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF2-ROBOT)
(RIGHTOF2-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(NOT-CHECKCONSISTENCY-)
(not (CHECKCONSISTENCY-))
(not (Flag213prime-))
(not (Flag212prime-))
(not (Flag195prime-))
(not (Flag33prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag391prime-))
(not (Flag389prime-))
(not (Flag387prime-))
(not (Flag386prime-))
(not (Flag384prime-))
(not (Flag383prime-))
(not (Flag382prime-))
(not (Flag381prime-))
(not (Flag379prime-))
(not (Flag378prime-))
(not (Flag377prime-))
(not (Flag376prime-))
(not (Flag375prime-))
(not (Flag373prime-))
(not (Flag372prime-))
(not (Flag371prime-))
(not (Flag370prime-))
(not (Flag369prime-))
(not (Flag367prime-))
(not (Flag366prime-))
(not (Flag365prime-))
(not (Flag364prime-))
(not (Flag363prime-))
(not (Flag362prime-))
(not (Flag360prime-))
(not (Flag359prime-))
(not (Flag358prime-))
(not (Flag357prime-))
(not (Flag356prime-))
(not (Flag355prime-))
(not (Flag354prime-))
(not (Flag353prime-))
(not (Flag351prime-))
(not (Flag350prime-))
(not (Flag349prime-))
(not (Flag348prime-))
(not (Flag347prime-))
(not (Flag346prime-))
(not (Flag345prime-))
(not (Flag344prime-))
(not (Flag343prime-))
(not (Flag342prime-))
(not (Flag340prime-))
(not (Flag339prime-))
(not (Flag338prime-))
(not (Flag337prime-))
(not (Flag336prime-))
(not (Flag335prime-))
(not (Flag334prime-))
(not (Flag333prime-))
(not (Flag332prime-))
(not (Flag331prime-))
(not (Flag329prime-))
(not (Flag328prime-))
(not (Flag327prime-))
(not (Flag326prime-))
(not (Flag325prime-))
(not (Flag324prime-))
(not (Flag323prime-))
(not (Flag322prime-))
(not (Flag321prime-))
(not (Flag320prime-))
(not (Flag319prime-))
(not (Flag318prime-))
(not (Flag316prime-))
(not (Flag315prime-))
(not (Flag314prime-))
(not (Flag313prime-))
(not (Flag312prime-))
(not (Flag311prime-))
(not (Flag310prime-))
(not (Flag309prime-))
(not (Flag308prime-))
(not (Flag307prime-))
(not (Flag306prime-))
(not (Flag305prime-))
(not (Flag304prime-))
(not (Flag302prime-))
(not (Flag301prime-))
(not (Flag300prime-))
(not (Flag299prime-))
(not (Flag298prime-))
(not (Flag297prime-))
(not (Flag296prime-))
(not (Flag295prime-))
(not (Flag294prime-))
(not (Flag293prime-))
(not (Flag292prime-))
(not (Flag291prime-))
(not (Flag289prime-))
(not (Flag288prime-))
(not (Flag287prime-))
(not (Flag286prime-))
(not (Flag285prime-))
(not (Flag284prime-))
(not (Flag283prime-))
(not (Flag282prime-))
(not (Flag281prime-))
(not (Flag280prime-))
(not (Flag279prime-))
(not (Flag278prime-))
(not (Flag277prime-))
(not (Flag276prime-))
(not (Flag275prime-))
(not (Flag273prime-))
(not (Flag272prime-))
(not (Flag271prime-))
(not (Flag270prime-))
(not (Flag269prime-))
(not (Flag268prime-))
(not (Flag267prime-))
(not (Flag266prime-))
(not (Flag265prime-))
(not (Flag264prime-))
(not (Flag263prime-))
(not (Flag262prime-))
(not (Flag261prime-))
(not (Flag260prime-))
(not (Flag259prime-))
(not (Flag257prime-))
(not (Flag256prime-))
(not (Flag255prime-))
(not (Flag254prime-))
(not (Flag253prime-))
(not (Flag252prime-))
(not (Flag251prime-))
(not (Flag250prime-))
(not (Flag249prime-))
(not (Flag248prime-))
(not (Flag247prime-))
(not (Flag246prime-))
(not (Flag245prime-))
(not (Flag244prime-))
(not (Flag243prime-))
(not (Flag242prime-))
(not (Flag241prime-))
(not (Flag240prime-))
(not (Flag239prime-))
(not (Flag238prime-))
(not (Flag237prime-))
(not (Flag236prime-))
(not (Flag235prime-))
(not (Flag234prime-))
(not (Flag233prime-))
(not (Flag232prime-))
(not (Flag231prime-))
(not (Flag230prime-))
(not (Flag229prime-))
(not (Flag228prime-))
(not (Flag227prime-))
(not (Flag226prime-))
(not (Flag225prime-))
(not (Flag224prime-))
(not (Flag223prime-))
(not (Flag222prime-))
(not (Flag221prime-))
(not (Flag220prime-))
(not (Flag219prime-))
(not (Flag218prime-))
(not (Flag217prime-))
(not (Flag216prime-))
(not (Flag215prime-))
(not (Flag214prime-))
(not (Flag211prime-))
(not (Flag210prime-))
(not (Flag209prime-))
(not (Flag208prime-))
(not (Flag207prime-))
(not (Flag206prime-))
(not (Flag205prime-))
(not (Flag204prime-))
(not (Flag203prime-))
(not (Flag202prime-))
(not (Flag201prime-))
(not (Flag200prime-))
(not (Flag199prime-))
(not (Flag4prime-))
(not (Flag390prime-))
(not (Flag388prime-))
(not (Flag385prime-))
(not (Flag380prime-))
(not (Flag374prime-))
(not (Flag368prime-))
(not (Flag361prime-))
(not (Flag352prime-))
(not (Flag341prime-))
(not (Flag330prime-))
(not (Flag317prime-))
(not (Flag303prime-))
(not (Flag290prime-))
(not (Flag274prime-))
(not (Flag258prime-))
(not (Flag197prime-))
(not (Flag196prime-))
(not (Flag193prime-))
(not (Flag192prime-))
(not (Flag190prime-))
(not (Flag189prime-))
(not (Flag188prime-))
(not (Flag186prime-))
(not (Flag185prime-))
(not (Flag184prime-))
(not (Flag183prime-))
(not (Flag182prime-))
(not (Flag180prime-))
(not (Flag179prime-))
(not (Flag178prime-))
(not (Flag177prime-))
(not (Flag176prime-))
(not (Flag174prime-))
(not (Flag173prime-))
(not (Flag172prime-))
(not (Flag171prime-))
(not (Flag170prime-))
(not (Flag169prime-))
(not (Flag168prime-))
(not (Flag166prime-))
(not (Flag165prime-))
(not (Flag164prime-))
(not (Flag163prime-))
(not (Flag162prime-))
(not (Flag161prime-))
(not (Flag160prime-))
(not (Flag158prime-))
(not (Flag157prime-))
(not (Flag156prime-))
(not (Flag155prime-))
(not (Flag154prime-))
(not (Flag153prime-))
(not (Flag152prime-))
(not (Flag151prime-))
(not (Flag150prime-))
(not (Flag148prime-))
(not (Flag147prime-))
(not (Flag146prime-))
(not (Flag145prime-))
(not (Flag144prime-))
(not (Flag143prime-))
(not (Flag142prime-))
(not (Flag141prime-))
(not (Flag140prime-))
(not (Flag139prime-))
(not (Flag137prime-))
(not (Flag136prime-))
(not (Flag135prime-))
(not (Flag134prime-))
(not (Flag133prime-))
(not (Flag132prime-))
(not (Flag131prime-))
(not (Flag130prime-))
(not (Flag129prime-))
(not (Flag128prime-))
(not (Flag127prime-))
(not (Flag126prime-))
(not (Flag124prime-))
(not (Flag123prime-))
(not (Flag122prime-))
(not (Flag121prime-))
(not (Flag120prime-))
(not (Flag119prime-))
(not (Flag118prime-))
(not (Flag117prime-))
(not (Flag116prime-))
(not (Flag115prime-))
(not (Flag114prime-))
(not (Flag112prime-))
(not (Flag111prime-))
(not (Flag110prime-))
(not (Flag109prime-))
(not (Flag108prime-))
(not (Flag107prime-))
(not (Flag106prime-))
(not (Flag105prime-))
(not (Flag104prime-))
(not (Flag103prime-))
(not (Flag102prime-))
(not (Flag101prime-))
(not (Flag100prime-))
(not (Flag99prime-))
(not (Flag97prime-))
(not (Flag96prime-))
(not (Flag95prime-))
(not (Flag94prime-))
(not (Flag93prime-))
(not (Flag92prime-))
(not (Flag91prime-))
(not (Flag90prime-))
(not (Flag89prime-))
(not (Flag88prime-))
(not (Flag87prime-))
(not (Flag86prime-))
(not (Flag85prime-))
(not (Flag84prime-))
(not (Flag83prime-))
(not (Flag81prime-))
(not (Flag80prime-))
(not (Flag79prime-))
(not (Flag78prime-))
(not (Flag77prime-))
(not (Flag76prime-))
(not (Flag75prime-))
(not (Flag74prime-))
(not (Flag73prime-))
(not (Flag72prime-))
(not (Flag71prime-))
(not (Flag70prime-))
(not (Flag69prime-))
(not (Flag68prime-))
(not (Flag66prime-))
(not (Flag65prime-))
(not (Flag64prime-))
(not (Flag63prime-))
(not (Flag62prime-))
(not (Flag61prime-))
(not (Flag60prime-))
(not (Flag59prime-))
(not (Flag58prime-))
(not (Flag57prime-))
(not (Flag56prime-))
(not (Flag55prime-))
(not (Flag54prime-))
(not (Flag53prime-))
(not (Flag52prime-))
(not (Flag51prime-))
(not (Flag49prime-))
(not (Flag48prime-))
(not (Flag47prime-))
(not (Flag46prime-))
(not (Flag45prime-))
(not (Flag44prime-))
(not (Flag43prime-))
(not (Flag42prime-))
(not (Flag41prime-))
(not (Flag40prime-))
(not (Flag39prime-))
(not (Flag38prime-))
(not (Flag37prime-))
(not (Flag36prime-))
(not (Flag35prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag31prime-))
(not (Flag30prime-))
(not (Flag29prime-))
(not (Flag28prime-))
(not (Flag27prime-))
(not (Flag26prime-))
(not (Flag25prime-))
(not (Flag24prime-))
(not (Flag23prime-))
(not (Flag22prime-))
(not (Flag21prime-))
(not (Flag20prime-))
(not (Flag19prime-))
(not (Flag18prime-))
(not (Flag17prime-))
(not (Flag16prime-))
(not (Flag15prime-))
(not (Flag14prime-))
(not (Flag13prime-))
(not (Flag12prime-))
(not (Flag11prime-))
(not (Flag10prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag1prime-))
(not (Flag198prime-))
(not (Flag194prime-))
(not (Flag191prime-))
(not (Flag187prime-))
(not (Flag181prime-))
(not (Flag175prime-))
(not (Flag167prime-))
(not (Flag159prime-))
(not (Flag149prime-))
(not (Flag138prime-))
(not (Flag125prime-))
(not (Flag113prime-))
(not (Flag98prime-))
(not (Flag82prime-))
(not (Flag67prime-))
(not (Flag50prime-))
)
)
(:action Flag4Action
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(Flag3-)
(Flag3prime-)
)
:effect
(and
(Flag4-)
(Flag4prime-)
)

)
(:action Prim4Action-2
:parameters ()
:precondition
(and
(not (Flag3-))
(Flag3prime-)
)
:effect
(and
(Flag4prime-)
(not (Flag4-))
)

)
(:action Flag199Action-0
:parameters ()
:precondition
(and
(BELOWOF2-ROBOT)
)
:effect
(and
(Flag199-)
(Flag199prime-)
)

)
(:action Flag199Action-1
:parameters ()
:precondition
(and
(BELOWOF1-ROBOT)
)
:effect
(and
(Flag199-)
(Flag199prime-)
)

)
(:action Prim199Action
:parameters ()
:precondition
(and
(not (BELOWOF2-ROBOT))
(not (BELOWOF1-ROBOT))
)
:effect
(and
(Flag199prime-)
(not (Flag199-))
)

)
(:action Flag200Action-0
:parameters ()
:precondition
(and
(BELOWOF2-ROBOT)
)
:effect
(and
(Flag200-)
(Flag200prime-)
)

)
(:action Flag200Action-1
:parameters ()
:precondition
(and
(BELOWOF1-ROBOT)
)
:effect
(and
(Flag200-)
(Flag200prime-)
)

)
(:action Flag200Action-2
:parameters ()
:precondition
(and
(BELOWOF3-ROBOT)
)
:effect
(and
(Flag200-)
(Flag200prime-)
)

)
(:action Prim200Action
:parameters ()
:precondition
(and
(not (BELOWOF2-ROBOT))
(not (BELOWOF1-ROBOT))
(not (BELOWOF3-ROBOT))
)
:effect
(and
(Flag200prime-)
(not (Flag200-))
)

)
(:action Flag201Action-0
:parameters ()
:precondition
(and
(BELOWOF2-ROBOT)
)
:effect
(and
(Flag201-)
(Flag201prime-)
)

)
(:action Flag201Action-1
:parameters ()
:precondition
(and
(BELOWOF1-ROBOT)
)
:effect
(and
(Flag201-)
(Flag201prime-)
)

)
(:action Flag201Action-2
:parameters ()
:precondition
(and
(BELOWOF3-ROBOT)
)
:effect
(and
(Flag201-)
(Flag201prime-)
)

)
(:action Flag201Action-3
:parameters ()
:precondition
(and
(BELOWOF4-ROBOT)
)
:effect
(and
(Flag201-)
(Flag201prime-)
)

)
(:action Prim201Action
:parameters ()
:precondition
(and
(not (BELOWOF2-ROBOT))
(not (BELOWOF1-ROBOT))
(not (BELOWOF3-ROBOT))
(not (BELOWOF4-ROBOT))
)
:effect
(and
(Flag201prime-)
(not (Flag201-))
)

)
(:action Flag202Action-0
:parameters ()
:precondition
(and
(BELOWOF2-ROBOT)
)
:effect
(and
(Flag202-)
(Flag202prime-)
)

)
(:action Flag202Action-1
:parameters ()
:precondition
(and
(BELOWOF1-ROBOT)
)
:effect
(and
(Flag202-)
(Flag202prime-)
)

)
(:action Flag202Action-2
:parameters ()
:precondition
(and
(BELOWOF3-ROBOT)
)
:effect
(and
(Flag202-)
(Flag202prime-)
)

)
(:action Flag202Action-3
:parameters ()
:precondition
(and
(BELOWOF4-ROBOT)
)
:effect
(and
(Flag202-)
(Flag202prime-)
)

)
(:action Flag202Action-4
:parameters ()
:precondition
(and
(BELOWOF5-ROBOT)
)
:effect
(and
(Flag202-)
(Flag202prime-)
)

)
(:action Prim202Action
:parameters ()
:precondition
(and
(not (BELOWOF2-ROBOT))
(not (BELOWOF1-ROBOT))
(not (BELOWOF3-ROBOT))
(not (BELOWOF4-ROBOT))
(not (BELOWOF5-ROBOT))
)
:effect
(and
(Flag202prime-)
(not (Flag202-))
)

)
(:action Flag203Action-0
:parameters ()
:precondition
(and
(BELOWOF6-ROBOT)
)
:effect
(and
(Flag203-)
(Flag203prime-)
)

)
(:action Flag203Action-1
:parameters ()
:precondition
(and
(BELOWOF5-ROBOT)
)
:effect
(and
(Flag203-)
(Flag203prime-)
)

)
(:action Flag203Action-2
:parameters ()
:precondition
(and
(BELOWOF2-ROBOT)
)
:effect
(and
(Flag203-)
(Flag203prime-)
)

)
(:action Flag203Action-3
:parameters ()
:precondition
(and
(BELOWOF1-ROBOT)
)
:effect
(and
(Flag203-)
(Flag203prime-)
)

)
(:action Flag203Action-4
:parameters ()
:precondition
(and
(BELOWOF4-ROBOT)
)
:effect
(and
(Flag203-)
(Flag203prime-)
)

)
(:action Flag203Action-5
:parameters ()
:precondition
(and
(BELOWOF3-ROBOT)
)
:effect
(and
(Flag203-)
(Flag203prime-)
)

)
(:action Prim203Action
:parameters ()
:precondition
(and
(not (BELOWOF6-ROBOT))
(not (BELOWOF5-ROBOT))
(not (BELOWOF2-ROBOT))
(not (BELOWOF1-ROBOT))
(not (BELOWOF4-ROBOT))
(not (BELOWOF3-ROBOT))
)
:effect
(and
(Flag203prime-)
(not (Flag203-))
)

)
(:action Flag204Action-0
:parameters ()
:precondition
(and
(BELOWOF6-ROBOT)
)
:effect
(and
(Flag204-)
(Flag204prime-)
)

)
(:action Flag204Action-1
:parameters ()
:precondition
(and
(BELOWOF5-ROBOT)
)
:effect
(and
(Flag204-)
(Flag204prime-)
)

)
(:action Flag204Action-2
:parameters ()
:precondition
(and
(BELOWOF2-ROBOT)
)
:effect
(and
(Flag204-)
(Flag204prime-)
)

)
(:action Flag204Action-3
:parameters ()
:precondition
(and
(BELOWOF1-ROBOT)
)
:effect
(and
(Flag204-)
(Flag204prime-)
)

)
(:action Flag204Action-4
:parameters ()
:precondition
(and
(BELOWOF4-ROBOT)
)
:effect
(and
(Flag204-)
(Flag204prime-)
)

)
(:action Flag204Action-5
:parameters ()
:precondition
(and
(BELOWOF7-ROBOT)
)
:effect
(and
(Flag204-)
(Flag204prime-)
)

)
(:action Flag204Action-6
:parameters ()
:precondition
(and
(BELOWOF3-ROBOT)
)
:effect
(and
(Flag204-)
(Flag204prime-)
)

)
(:action Prim204Action
:parameters ()
:precondition
(and
(not (BELOWOF6-ROBOT))
(not (BELOWOF5-ROBOT))
(not (BELOWOF2-ROBOT))
(not (BELOWOF1-ROBOT))
(not (BELOWOF4-ROBOT))
(not (BELOWOF7-ROBOT))
(not (BELOWOF3-ROBOT))
)
:effect
(and
(Flag204prime-)
(not (Flag204-))
)

)
(:action Flag205Action-0
:parameters ()
:precondition
(and
(BELOWOF6-ROBOT)
)
:effect
(and
(Flag205-)
(Flag205prime-)
)

)
(:action Flag205Action-1
:parameters ()
:precondition
(and
(BELOWOF5-ROBOT)
)
:effect
(and
(Flag205-)
(Flag205prime-)
)

)
(:action Flag205Action-2
:parameters ()
:precondition
(and
(BELOWOF2-ROBOT)
)
:effect
(and
(Flag205-)
(Flag205prime-)
)

)
(:action Flag205Action-3
:parameters ()
:precondition
(and
(BELOWOF1-ROBOT)
)
:effect
(and
(Flag205-)
(Flag205prime-)
)

)
(:action Flag205Action-4
:parameters ()
:precondition
(and
(BELOWOF4-ROBOT)
)
:effect
(and
(Flag205-)
(Flag205prime-)
)

)
(:action Flag205Action-5
:parameters ()
:precondition
(and
(BELOWOF7-ROBOT)
)
:effect
(and
(Flag205-)
(Flag205prime-)
)

)
(:action Flag205Action-6
:parameters ()
:precondition
(and
(BELOWOF3-ROBOT)
)
:effect
(and
(Flag205-)
(Flag205prime-)
)

)
(:action Flag205Action-7
:parameters ()
:precondition
(and
(BELOWOF8-ROBOT)
)
:effect
(and
(Flag205-)
(Flag205prime-)
)

)
(:action Prim205Action
:parameters ()
:precondition
(and
(not (BELOWOF6-ROBOT))
(not (BELOWOF5-ROBOT))
(not (BELOWOF2-ROBOT))
(not (BELOWOF1-ROBOT))
(not (BELOWOF4-ROBOT))
(not (BELOWOF7-ROBOT))
(not (BELOWOF3-ROBOT))
(not (BELOWOF8-ROBOT))
)
:effect
(and
(Flag205prime-)
(not (Flag205-))
)

)
(:action Flag206Action-0
:parameters ()
:precondition
(and
(BELOWOF6-ROBOT)
)
:effect
(and
(Flag206-)
(Flag206prime-)
)

)
(:action Flag206Action-1
:parameters ()
:precondition
(and
(BELOWOF5-ROBOT)
)
:effect
(and
(Flag206-)
(Flag206prime-)
)

)
(:action Flag206Action-2
:parameters ()
:precondition
(and
(BELOWOF2-ROBOT)
)
:effect
(and
(Flag206-)
(Flag206prime-)
)

)
(:action Flag206Action-3
:parameters ()
:precondition
(and
(BELOWOF1-ROBOT)
)
:effect
(and
(Flag206-)
(Flag206prime-)
)

)
(:action Flag206Action-4
:parameters ()
:precondition
(and
(BELOWOF4-ROBOT)
)
:effect
(and
(Flag206-)
(Flag206prime-)
)

)
(:action Flag206Action-5
:parameters ()
:precondition
(and
(BELOWOF7-ROBOT)
)
:effect
(and
(Flag206-)
(Flag206prime-)
)

)
(:action Flag206Action-6
:parameters ()
:precondition
(and
(BELOWOF9-ROBOT)
)
:effect
(and
(Flag206-)
(Flag206prime-)
)

)
(:action Flag206Action-7
:parameters ()
:precondition
(and
(BELOWOF3-ROBOT)
)
:effect
(and
(Flag206-)
(Flag206prime-)
)

)
(:action Flag206Action-8
:parameters ()
:precondition
(and
(BELOWOF8-ROBOT)
)
:effect
(and
(Flag206-)
(Flag206prime-)
)

)
(:action Prim206Action
:parameters ()
:precondition
(and
(not (BELOWOF6-ROBOT))
(not (BELOWOF5-ROBOT))
(not (BELOWOF2-ROBOT))
(not (BELOWOF1-ROBOT))
(not (BELOWOF4-ROBOT))
(not (BELOWOF7-ROBOT))
(not (BELOWOF9-ROBOT))
(not (BELOWOF3-ROBOT))
(not (BELOWOF8-ROBOT))
)
:effect
(and
(Flag206prime-)
(not (Flag206-))
)

)
(:action Flag207Action-0
:parameters ()
:precondition
(and
(BELOWOF10-ROBOT)
)
:effect
(and
(Flag207-)
(Flag207prime-)
)

)
(:action Flag207Action-1
:parameters ()
:precondition
(and
(BELOWOF6-ROBOT)
)
:effect
(and
(Flag207-)
(Flag207prime-)
)

)
(:action Flag207Action-2
:parameters ()
:precondition
(and
(BELOWOF5-ROBOT)
)
:effect
(and
(Flag207-)
(Flag207prime-)
)

)
(:action Flag207Action-3
:parameters ()
:precondition
(and
(BELOWOF2-ROBOT)
)
:effect
(and
(Flag207-)
(Flag207prime-)
)

)
(:action Flag207Action-4
:parameters ()
:precondition
(and
(BELOWOF1-ROBOT)
)
:effect
(and
(Flag207-)
(Flag207prime-)
)

)
(:action Flag207Action-5
:parameters ()
:precondition
(and
(BELOWOF4-ROBOT)
)
:effect
(and
(Flag207-)
(Flag207prime-)
)

)
(:action Flag207Action-6
:parameters ()
:precondition
(and
(BELOWOF7-ROBOT)
)
:effect
(and
(Flag207-)
(Flag207prime-)
)

)
(:action Flag207Action-7
:parameters ()
:precondition
(and
(BELOWOF9-ROBOT)
)
:effect
(and
(Flag207-)
(Flag207prime-)
)

)
(:action Flag207Action-8
:parameters ()
:precondition
(and
(BELOWOF3-ROBOT)
)
:effect
(and
(Flag207-)
(Flag207prime-)
)

)
(:action Flag207Action-9
:parameters ()
:precondition
(and
(BELOWOF8-ROBOT)
)
:effect
(and
(Flag207-)
(Flag207prime-)
)

)
(:action Prim207Action
:parameters ()
:precondition
(and
(not (BELOWOF10-ROBOT))
(not (BELOWOF6-ROBOT))
(not (BELOWOF5-ROBOT))
(not (BELOWOF2-ROBOT))
(not (BELOWOF1-ROBOT))
(not (BELOWOF4-ROBOT))
(not (BELOWOF7-ROBOT))
(not (BELOWOF9-ROBOT))
(not (BELOWOF3-ROBOT))
(not (BELOWOF8-ROBOT))
)
:effect
(and
(Flag207prime-)
(not (Flag207-))
)

)
(:action Flag208Action-0
:parameters ()
:precondition
(and
(BELOWOF10-ROBOT)
)
:effect
(and
(Flag208-)
(Flag208prime-)
)

)
(:action Flag208Action-1
:parameters ()
:precondition
(and
(BELOWOF11-ROBOT)
)
:effect
(and
(Flag208-)
(Flag208prime-)
)

)
(:action Flag208Action-2
:parameters ()
:precondition
(and
(BELOWOF6-ROBOT)
)
:effect
(and
(Flag208-)
(Flag208prime-)
)

)
(:action Flag208Action-3
:parameters ()
:precondition
(and
(BELOWOF5-ROBOT)
)
:effect
(and
(Flag208-)
(Flag208prime-)
)

)
(:action Flag208Action-4
:parameters ()
:precondition
(and
(BELOWOF2-ROBOT)
)
:effect
(and
(Flag208-)
(Flag208prime-)
)

)
(:action Flag208Action-5
:parameters ()
:precondition
(and
(BELOWOF1-ROBOT)
)
:effect
(and
(Flag208-)
(Flag208prime-)
)

)
(:action Flag208Action-6
:parameters ()
:precondition
(and
(BELOWOF4-ROBOT)
)
:effect
(and
(Flag208-)
(Flag208prime-)
)

)
(:action Flag208Action-7
:parameters ()
:precondition
(and
(BELOWOF7-ROBOT)
)
:effect
(and
(Flag208-)
(Flag208prime-)
)

)
(:action Flag208Action-8
:parameters ()
:precondition
(and
(BELOWOF9-ROBOT)
)
:effect
(and
(Flag208-)
(Flag208prime-)
)

)
(:action Flag208Action-9
:parameters ()
:precondition
(and
(BELOWOF3-ROBOT)
)
:effect
(and
(Flag208-)
(Flag208prime-)
)

)
(:action Flag208Action-10
:parameters ()
:precondition
(and
(BELOWOF8-ROBOT)
)
:effect
(and
(Flag208-)
(Flag208prime-)
)

)
(:action Prim208Action
:parameters ()
:precondition
(and
(not (BELOWOF10-ROBOT))
(not (BELOWOF11-ROBOT))
(not (BELOWOF6-ROBOT))
(not (BELOWOF5-ROBOT))
(not (BELOWOF2-ROBOT))
(not (BELOWOF1-ROBOT))
(not (BELOWOF4-ROBOT))
(not (BELOWOF7-ROBOT))
(not (BELOWOF9-ROBOT))
(not (BELOWOF3-ROBOT))
(not (BELOWOF8-ROBOT))
)
:effect
(and
(Flag208prime-)
(not (Flag208-))
)

)
(:action Flag209Action-0
:parameters ()
:precondition
(and
(BELOWOF10-ROBOT)
)
:effect
(and
(Flag209-)
(Flag209prime-)
)

)
(:action Flag209Action-1
:parameters ()
:precondition
(and
(BELOWOF11-ROBOT)
)
:effect
(and
(Flag209-)
(Flag209prime-)
)

)
(:action Flag209Action-2
:parameters ()
:precondition
(and
(BELOWOF6-ROBOT)
)
:effect
(and
(Flag209-)
(Flag209prime-)
)

)
(:action Flag209Action-3
:parameters ()
:precondition
(and
(BELOWOF5-ROBOT)
)
:effect
(and
(Flag209-)
(Flag209prime-)
)

)
(:action Flag209Action-4
:parameters ()
:precondition
(and
(BELOWOF2-ROBOT)
)
:effect
(and
(Flag209-)
(Flag209prime-)
)

)
(:action Flag209Action-5
:parameters ()
:precondition
(and
(BELOWOF1-ROBOT)
)
:effect
(and
(Flag209-)
(Flag209prime-)
)

)
(:action Flag209Action-6
:parameters ()
:precondition
(and
(BELOWOF4-ROBOT)
)
:effect
(and
(Flag209-)
(Flag209prime-)
)

)
(:action Flag209Action-7
:parameters ()
:precondition
(and
(BELOWOF12-ROBOT)
)
:effect
(and
(Flag209-)
(Flag209prime-)
)

)
(:action Flag209Action-8
:parameters ()
:precondition
(and
(BELOWOF7-ROBOT)
)
:effect
(and
(Flag209-)
(Flag209prime-)
)

)
(:action Flag209Action-9
:parameters ()
:precondition
(and
(BELOWOF9-ROBOT)
)
:effect
(and
(Flag209-)
(Flag209prime-)
)

)
(:action Flag209Action-10
:parameters ()
:precondition
(and
(BELOWOF3-ROBOT)
)
:effect
(and
(Flag209-)
(Flag209prime-)
)

)
(:action Flag209Action-11
:parameters ()
:precondition
(and
(BELOWOF8-ROBOT)
)
:effect
(and
(Flag209-)
(Flag209prime-)
)

)
(:action Prim209Action
:parameters ()
:precondition
(and
(not (BELOWOF10-ROBOT))
(not (BELOWOF11-ROBOT))
(not (BELOWOF6-ROBOT))
(not (BELOWOF5-ROBOT))
(not (BELOWOF2-ROBOT))
(not (BELOWOF1-ROBOT))
(not (BELOWOF4-ROBOT))
(not (BELOWOF12-ROBOT))
(not (BELOWOF7-ROBOT))
(not (BELOWOF9-ROBOT))
(not (BELOWOF3-ROBOT))
(not (BELOWOF8-ROBOT))
)
:effect
(and
(Flag209prime-)
(not (Flag209-))
)

)
(:action Flag210Action-0
:parameters ()
:precondition
(and
(BELOWOF10-ROBOT)
)
:effect
(and
(Flag210-)
(Flag210prime-)
)

)
(:action Flag210Action-1
:parameters ()
:precondition
(and
(BELOWOF11-ROBOT)
)
:effect
(and
(Flag210-)
(Flag210prime-)
)

)
(:action Flag210Action-2
:parameters ()
:precondition
(and
(BELOWOF6-ROBOT)
)
:effect
(and
(Flag210-)
(Flag210prime-)
)

)
(:action Flag210Action-3
:parameters ()
:precondition
(and
(BELOWOF5-ROBOT)
)
:effect
(and
(Flag210-)
(Flag210prime-)
)

)
(:action Flag210Action-4
:parameters ()
:precondition
(and
(BELOWOF2-ROBOT)
)
:effect
(and
(Flag210-)
(Flag210prime-)
)

)
(:action Flag210Action-5
:parameters ()
:precondition
(and
(BELOWOF1-ROBOT)
)
:effect
(and
(Flag210-)
(Flag210prime-)
)

)
(:action Flag210Action-6
:parameters ()
:precondition
(and
(BELOWOF4-ROBOT)
)
:effect
(and
(Flag210-)
(Flag210prime-)
)

)
(:action Flag210Action-7
:parameters ()
:precondition
(and
(BELOWOF12-ROBOT)
)
:effect
(and
(Flag210-)
(Flag210prime-)
)

)
(:action Flag210Action-8
:parameters ()
:precondition
(and
(BELOWOF7-ROBOT)
)
:effect
(and
(Flag210-)
(Flag210prime-)
)

)
(:action Flag210Action-9
:parameters ()
:precondition
(and
(BELOWOF9-ROBOT)
)
:effect
(and
(Flag210-)
(Flag210prime-)
)

)
(:action Flag210Action-10
:parameters ()
:precondition
(and
(BELOWOF3-ROBOT)
)
:effect
(and
(Flag210-)
(Flag210prime-)
)

)
(:action Flag210Action-11
:parameters ()
:precondition
(and
(BELOWOF13-ROBOT)
)
:effect
(and
(Flag210-)
(Flag210prime-)
)

)
(:action Flag210Action-12
:parameters ()
:precondition
(and
(BELOWOF8-ROBOT)
)
:effect
(and
(Flag210-)
(Flag210prime-)
)

)
(:action Prim210Action
:parameters ()
:precondition
(and
(not (BELOWOF10-ROBOT))
(not (BELOWOF11-ROBOT))
(not (BELOWOF6-ROBOT))
(not (BELOWOF5-ROBOT))
(not (BELOWOF2-ROBOT))
(not (BELOWOF1-ROBOT))
(not (BELOWOF4-ROBOT))
(not (BELOWOF12-ROBOT))
(not (BELOWOF7-ROBOT))
(not (BELOWOF9-ROBOT))
(not (BELOWOF3-ROBOT))
(not (BELOWOF13-ROBOT))
(not (BELOWOF8-ROBOT))
)
:effect
(and
(Flag210prime-)
(not (Flag210-))
)

)
(:action Flag211Action-0
:parameters ()
:precondition
(and
(BELOWOF10-ROBOT)
)
:effect
(and
(Flag211-)
(Flag211prime-)
)

)
(:action Flag211Action-1
:parameters ()
:precondition
(and
(BELOWOF11-ROBOT)
)
:effect
(and
(Flag211-)
(Flag211prime-)
)

)
(:action Flag211Action-2
:parameters ()
:precondition
(and
(BELOWOF6-ROBOT)
)
:effect
(and
(Flag211-)
(Flag211prime-)
)

)
(:action Flag211Action-3
:parameters ()
:precondition
(and
(BELOWOF5-ROBOT)
)
:effect
(and
(Flag211-)
(Flag211prime-)
)

)
(:action Flag211Action-4
:parameters ()
:precondition
(and
(BELOWOF2-ROBOT)
)
:effect
(and
(Flag211-)
(Flag211prime-)
)

)
(:action Flag211Action-5
:parameters ()
:precondition
(and
(BELOWOF1-ROBOT)
)
:effect
(and
(Flag211-)
(Flag211prime-)
)

)
(:action Flag211Action-6
:parameters ()
:precondition
(and
(BELOWOF4-ROBOT)
)
:effect
(and
(Flag211-)
(Flag211prime-)
)

)
(:action Flag211Action-7
:parameters ()
:precondition
(and
(BELOWOF12-ROBOT)
)
:effect
(and
(Flag211-)
(Flag211prime-)
)

)
(:action Flag211Action-8
:parameters ()
:precondition
(and
(BELOWOF7-ROBOT)
)
:effect
(and
(Flag211-)
(Flag211prime-)
)

)
(:action Flag211Action-9
:parameters ()
:precondition
(and
(BELOWOF9-ROBOT)
)
:effect
(and
(Flag211-)
(Flag211prime-)
)

)
(:action Flag211Action-10
:parameters ()
:precondition
(and
(BELOWOF3-ROBOT)
)
:effect
(and
(Flag211-)
(Flag211prime-)
)

)
(:action Flag211Action-11
:parameters ()
:precondition
(and
(BELOWOF13-ROBOT)
)
:effect
(and
(Flag211-)
(Flag211prime-)
)

)
(:action Flag211Action-12
:parameters ()
:precondition
(and
(BELOWOF8-ROBOT)
)
:effect
(and
(Flag211-)
(Flag211prime-)
)

)
(:action Flag211Action-13
:parameters ()
:precondition
(and
(BELOWOF14-ROBOT)
)
:effect
(and
(Flag211-)
(Flag211prime-)
)

)
(:action Prim211Action
:parameters ()
:precondition
(and
(not (BELOWOF10-ROBOT))
(not (BELOWOF11-ROBOT))
(not (BELOWOF6-ROBOT))
(not (BELOWOF5-ROBOT))
(not (BELOWOF2-ROBOT))
(not (BELOWOF1-ROBOT))
(not (BELOWOF4-ROBOT))
(not (BELOWOF12-ROBOT))
(not (BELOWOF7-ROBOT))
(not (BELOWOF9-ROBOT))
(not (BELOWOF3-ROBOT))
(not (BELOWOF13-ROBOT))
(not (BELOWOF8-ROBOT))
(not (BELOWOF14-ROBOT))
)
:effect
(and
(Flag211prime-)
(not (Flag211-))
)

)
(:action Flag212Action-0
:parameters ()
:precondition
(and
(BELOWOF10-ROBOT)
)
:effect
(and
(Flag212-)
(Flag212prime-)
)

)
(:action Flag212Action-1
:parameters ()
:precondition
(and
(BELOWOF11-ROBOT)
)
:effect
(and
(Flag212-)
(Flag212prime-)
)

)
(:action Flag212Action-3
:parameters ()
:precondition
(and
(BELOWOF6-ROBOT)
)
:effect
(and
(Flag212-)
(Flag212prime-)
)

)
(:action Flag212Action-4
:parameters ()
:precondition
(and
(BELOWOF5-ROBOT)
)
:effect
(and
(Flag212-)
(Flag212prime-)
)

)
(:action Flag212Action-5
:parameters ()
:precondition
(and
(BELOWOF2-ROBOT)
)
:effect
(and
(Flag212-)
(Flag212prime-)
)

)
(:action Flag212Action-6
:parameters ()
:precondition
(and
(BELOWOF1-ROBOT)
)
:effect
(and
(Flag212-)
(Flag212prime-)
)

)
(:action Flag212Action-7
:parameters ()
:precondition
(and
(BELOWOF4-ROBOT)
)
:effect
(and
(Flag212-)
(Flag212prime-)
)

)
(:action Flag212Action-8
:parameters ()
:precondition
(and
(BELOWOF12-ROBOT)
)
:effect
(and
(Flag212-)
(Flag212prime-)
)

)
(:action Flag212Action-9
:parameters ()
:precondition
(and
(BELOWOF7-ROBOT)
)
:effect
(and
(Flag212-)
(Flag212prime-)
)

)
(:action Flag212Action-10
:parameters ()
:precondition
(and
(BELOWOF9-ROBOT)
)
:effect
(and
(Flag212-)
(Flag212prime-)
)

)
(:action Flag212Action-11
:parameters ()
:precondition
(and
(BELOWOF3-ROBOT)
)
:effect
(and
(Flag212-)
(Flag212prime-)
)

)
(:action Flag212Action-12
:parameters ()
:precondition
(and
(BELOWOF13-ROBOT)
)
:effect
(and
(Flag212-)
(Flag212prime-)
)

)
(:action Flag212Action-13
:parameters ()
:precondition
(and
(BELOWOF8-ROBOT)
)
:effect
(and
(Flag212-)
(Flag212prime-)
)

)
(:action Flag212Action-14
:parameters ()
:precondition
(and
(BELOWOF14-ROBOT)
)
:effect
(and
(Flag212-)
(Flag212prime-)
)

)
(:action Prim212Action
:parameters ()
:precondition
(and
(not (BELOWOF10-ROBOT))
(not (BELOWOF11-ROBOT))
(not (BELOWOF15-ROBOT))
(not (BELOWOF6-ROBOT))
(not (BELOWOF5-ROBOT))
(not (BELOWOF2-ROBOT))
(not (BELOWOF1-ROBOT))
(not (BELOWOF4-ROBOT))
(not (BELOWOF12-ROBOT))
(not (BELOWOF7-ROBOT))
(not (BELOWOF9-ROBOT))
(not (BELOWOF3-ROBOT))
(not (BELOWOF13-ROBOT))
(not (BELOWOF8-ROBOT))
(not (BELOWOF14-ROBOT))
)
:effect
(and
(Flag212prime-)
(not (Flag212-))
)

)
(:action Flag213Action-0
:parameters ()
:precondition
(and
(BELOWOF10-ROBOT)
)
:effect
(and
(Flag213-)
(Flag213prime-)
)

)
(:action Flag213Action-1
:parameters ()
:precondition
(and
(BELOWOF16-ROBOT)
)
:effect
(and
(Flag213-)
(Flag213prime-)
)

)
(:action Flag213Action-3
:parameters ()
:precondition
(and
(BELOWOF6-ROBOT)
)
:effect
(and
(Flag213-)
(Flag213prime-)
)

)
(:action Flag213Action-4
:parameters ()
:precondition
(and
(BELOWOF11-ROBOT)
)
:effect
(and
(Flag213-)
(Flag213prime-)
)

)
(:action Flag213Action-5
:parameters ()
:precondition
(and
(BELOWOF5-ROBOT)
)
:effect
(and
(Flag213-)
(Flag213prime-)
)

)
(:action Flag213Action-6
:parameters ()
:precondition
(and
(BELOWOF2-ROBOT)
)
:effect
(and
(Flag213-)
(Flag213prime-)
)

)
(:action Flag213Action-7
:parameters ()
:precondition
(and
(BELOWOF1-ROBOT)
)
:effect
(and
(Flag213-)
(Flag213prime-)
)

)
(:action Flag213Action-8
:parameters ()
:precondition
(and
(BELOWOF4-ROBOT)
)
:effect
(and
(Flag213-)
(Flag213prime-)
)

)
(:action Flag213Action-9
:parameters ()
:precondition
(and
(BELOWOF12-ROBOT)
)
:effect
(and
(Flag213-)
(Flag213prime-)
)

)
(:action Flag213Action-10
:parameters ()
:precondition
(and
(BELOWOF7-ROBOT)
)
:effect
(and
(Flag213-)
(Flag213prime-)
)

)
(:action Flag213Action-11
:parameters ()
:precondition
(and
(BELOWOF9-ROBOT)
)
:effect
(and
(Flag213-)
(Flag213prime-)
)

)
(:action Flag213Action-12
:parameters ()
:precondition
(and
(BELOWOF3-ROBOT)
)
:effect
(and
(Flag213-)
(Flag213prime-)
)

)
(:action Flag213Action-13
:parameters ()
:precondition
(and
(BELOWOF13-ROBOT)
)
:effect
(and
(Flag213-)
(Flag213prime-)
)

)
(:action Flag213Action-14
:parameters ()
:precondition
(and
(BELOWOF8-ROBOT)
)
:effect
(and
(Flag213-)
(Flag213prime-)
)

)
(:action Flag213Action-15
:parameters ()
:precondition
(and
(BELOWOF14-ROBOT)
)
:effect
(and
(Flag213-)
(Flag213prime-)
)

)
(:action Prim213Action
:parameters ()
:precondition
(and
(not (BELOWOF10-ROBOT))
(not (BELOWOF16-ROBOT))
(not (BELOWOF15-ROBOT))
(not (BELOWOF6-ROBOT))
(not (BELOWOF11-ROBOT))
(not (BELOWOF5-ROBOT))
(not (BELOWOF2-ROBOT))
(not (BELOWOF1-ROBOT))
(not (BELOWOF4-ROBOT))
(not (BELOWOF12-ROBOT))
(not (BELOWOF7-ROBOT))
(not (BELOWOF9-ROBOT))
(not (BELOWOF3-ROBOT))
(not (BELOWOF13-ROBOT))
(not (BELOWOF8-ROBOT))
(not (BELOWOF14-ROBOT))
)
:effect
(and
(Flag213prime-)
(not (Flag213-))
)

)
(:action Flag214Action-0
:parameters ()
:precondition
(and
(ABOVEOF6-ROBOT)
)
:effect
(and
(Flag214-)
(Flag214prime-)
)

)
(:action Flag214Action-1
:parameters ()
:precondition
(and
(ABOVEOF11-ROBOT)
)
:effect
(and
(Flag214-)
(Flag214prime-)
)

)
(:action Flag214Action-2
:parameters ()
:precondition
(and
(ABOVEOF10-ROBOT)
)
:effect
(and
(Flag214-)
(Flag214prime-)
)

)
(:action Flag214Action-3
:parameters ()
:precondition
(and
(ABOVEOF2-ROBOT)
)
:effect
(and
(Flag214-)
(Flag214prime-)
)

)
(:action Flag214Action-4
:parameters ()
:precondition
(and
(ABOVEOF7-ROBOT)
)
:effect
(and
(Flag214-)
(Flag214prime-)
)

)
(:action Flag214Action-5
:parameters ()
:precondition
(and
(ABOVEOF12-ROBOT)
)
:effect
(and
(Flag214-)
(Flag214prime-)
)

)
(:action Flag214Action-6
:parameters ()
:precondition
(and
(ABOVEOF15-ROBOT)
)
:effect
(and
(Flag214-)
(Flag214prime-)
)

)
(:action Flag214Action-7
:parameters ()
:precondition
(and
(ABOVEOF8-ROBOT)
)
:effect
(and
(Flag214-)
(Flag214prime-)
)

)
(:action Flag214Action-8
:parameters ()
:precondition
(and
(ABOVEOF9-ROBOT)
)
:effect
(and
(Flag214-)
(Flag214prime-)
)

)
(:action Flag214Action-9
:parameters ()
:precondition
(and
(ABOVEOF4-ROBOT)
)
:effect
(and
(Flag214-)
(Flag214prime-)
)

)
(:action Flag214Action-10
:parameters ()
:precondition
(and
(ABOVEOF3-ROBOT)
)
:effect
(and
(Flag214-)
(Flag214prime-)
)

)
(:action Flag214Action-11
:parameters ()
:precondition
(and
(ABOVEOF13-ROBOT)
)
:effect
(and
(Flag214-)
(Flag214prime-)
)

)
(:action Flag214Action-12
:parameters ()
:precondition
(and
(ABOVEOF14-ROBOT)
)
:effect
(and
(Flag214-)
(Flag214prime-)
)

)
(:action Flag214Action-13
:parameters ()
:precondition
(and
(ABOVEOF5-ROBOT)
)
:effect
(and
(Flag214-)
(Flag214prime-)
)

)
(:action Flag214Action-14
:parameters ()
:precondition
(and
(ABOVEOF1-ROBOT)
)
:effect
(and
(Flag214-)
(Flag214prime-)
)

)
(:action Prim214Action
:parameters ()
:precondition
(and
(not (ABOVEOF6-ROBOT))
(not (ABOVEOF11-ROBOT))
(not (ABOVEOF10-ROBOT))
(not (ABOVEOF2-ROBOT))
(not (ABOVEOF7-ROBOT))
(not (ABOVEOF12-ROBOT))
(not (ABOVEOF15-ROBOT))
(not (ABOVEOF8-ROBOT))
(not (ABOVEOF9-ROBOT))
(not (ABOVEOF4-ROBOT))
(not (ABOVEOF3-ROBOT))
(not (ABOVEOF13-ROBOT))
(not (ABOVEOF14-ROBOT))
(not (ABOVEOF5-ROBOT))
(not (ABOVEOF1-ROBOT))
)
:effect
(and
(Flag214prime-)
(not (Flag214-))
)

)
(:action Flag215Action-0
:parameters ()
:precondition
(and
(ABOVEOF11-ROBOT)
)
:effect
(and
(Flag215-)
(Flag215prime-)
)

)
(:action Flag215Action-1
:parameters ()
:precondition
(and
(ABOVEOF6-ROBOT)
)
:effect
(and
(Flag215-)
(Flag215prime-)
)

)
(:action Flag215Action-2
:parameters ()
:precondition
(and
(ABOVEOF10-ROBOT)
)
:effect
(and
(Flag215-)
(Flag215prime-)
)

)
(:action Flag215Action-3
:parameters ()
:precondition
(and
(ABOVEOF2-ROBOT)
)
:effect
(and
(Flag215-)
(Flag215prime-)
)

)
(:action Flag215Action-4
:parameters ()
:precondition
(and
(ABOVEOF7-ROBOT)
)
:effect
(and
(Flag215-)
(Flag215prime-)
)

)
(:action Flag215Action-5
:parameters ()
:precondition
(and
(ABOVEOF12-ROBOT)
)
:effect
(and
(Flag215-)
(Flag215prime-)
)

)
(:action Flag215Action-6
:parameters ()
:precondition
(and
(ABOVEOF15-ROBOT)
)
:effect
(and
(Flag215-)
(Flag215prime-)
)

)
(:action Flag215Action-7
:parameters ()
:precondition
(and
(ABOVEOF8-ROBOT)
)
:effect
(and
(Flag215-)
(Flag215prime-)
)

)
(:action Flag215Action-8
:parameters ()
:precondition
(and
(ABOVEOF9-ROBOT)
)
:effect
(and
(Flag215-)
(Flag215prime-)
)

)
(:action Flag215Action-9
:parameters ()
:precondition
(and
(ABOVEOF4-ROBOT)
)
:effect
(and
(Flag215-)
(Flag215prime-)
)

)
(:action Flag215Action-10
:parameters ()
:precondition
(and
(ABOVEOF3-ROBOT)
)
:effect
(and
(Flag215-)
(Flag215prime-)
)

)
(:action Flag215Action-11
:parameters ()
:precondition
(and
(ABOVEOF13-ROBOT)
)
:effect
(and
(Flag215-)
(Flag215prime-)
)

)
(:action Flag215Action-12
:parameters ()
:precondition
(and
(ABOVEOF14-ROBOT)
)
:effect
(and
(Flag215-)
(Flag215prime-)
)

)
(:action Flag215Action-13
:parameters ()
:precondition
(and
(ABOVEOF5-ROBOT)
)
:effect
(and
(Flag215-)
(Flag215prime-)
)

)
(:action Prim215Action
:parameters ()
:precondition
(and
(not (ABOVEOF11-ROBOT))
(not (ABOVEOF6-ROBOT))
(not (ABOVEOF10-ROBOT))
(not (ABOVEOF2-ROBOT))
(not (ABOVEOF7-ROBOT))
(not (ABOVEOF12-ROBOT))
(not (ABOVEOF15-ROBOT))
(not (ABOVEOF8-ROBOT))
(not (ABOVEOF9-ROBOT))
(not (ABOVEOF4-ROBOT))
(not (ABOVEOF3-ROBOT))
(not (ABOVEOF13-ROBOT))
(not (ABOVEOF14-ROBOT))
(not (ABOVEOF5-ROBOT))
)
:effect
(and
(Flag215prime-)
(not (Flag215-))
)

)
(:action Flag216Action-0
:parameters ()
:precondition
(and
(ABOVEOF11-ROBOT)
)
:effect
(and
(Flag216-)
(Flag216prime-)
)

)
(:action Flag216Action-1
:parameters ()
:precondition
(and
(ABOVEOF6-ROBOT)
)
:effect
(and
(Flag216-)
(Flag216prime-)
)

)
(:action Flag216Action-2
:parameters ()
:precondition
(and
(ABOVEOF10-ROBOT)
)
:effect
(and
(Flag216-)
(Flag216prime-)
)

)
(:action Flag216Action-3
:parameters ()
:precondition
(and
(ABOVEOF8-ROBOT)
)
:effect
(and
(Flag216-)
(Flag216prime-)
)

)
(:action Flag216Action-4
:parameters ()
:precondition
(and
(ABOVEOF7-ROBOT)
)
:effect
(and
(Flag216-)
(Flag216prime-)
)

)
(:action Flag216Action-5
:parameters ()
:precondition
(and
(ABOVEOF12-ROBOT)
)
:effect
(and
(Flag216-)
(Flag216prime-)
)

)
(:action Flag216Action-6
:parameters ()
:precondition
(and
(ABOVEOF15-ROBOT)
)
:effect
(and
(Flag216-)
(Flag216prime-)
)

)
(:action Flag216Action-7
:parameters ()
:precondition
(and
(ABOVEOF9-ROBOT)
)
:effect
(and
(Flag216-)
(Flag216prime-)
)

)
(:action Flag216Action-8
:parameters ()
:precondition
(and
(ABOVEOF4-ROBOT)
)
:effect
(and
(Flag216-)
(Flag216prime-)
)

)
(:action Flag216Action-9
:parameters ()
:precondition
(and
(ABOVEOF3-ROBOT)
)
:effect
(and
(Flag216-)
(Flag216prime-)
)

)
(:action Flag216Action-10
:parameters ()
:precondition
(and
(ABOVEOF13-ROBOT)
)
:effect
(and
(Flag216-)
(Flag216prime-)
)

)
(:action Flag216Action-11
:parameters ()
:precondition
(and
(ABOVEOF14-ROBOT)
)
:effect
(and
(Flag216-)
(Flag216prime-)
)

)
(:action Flag216Action-12
:parameters ()
:precondition
(and
(ABOVEOF5-ROBOT)
)
:effect
(and
(Flag216-)
(Flag216prime-)
)

)
(:action Prim216Action
:parameters ()
:precondition
(and
(not (ABOVEOF11-ROBOT))
(not (ABOVEOF6-ROBOT))
(not (ABOVEOF10-ROBOT))
(not (ABOVEOF8-ROBOT))
(not (ABOVEOF7-ROBOT))
(not (ABOVEOF12-ROBOT))
(not (ABOVEOF15-ROBOT))
(not (ABOVEOF9-ROBOT))
(not (ABOVEOF4-ROBOT))
(not (ABOVEOF3-ROBOT))
(not (ABOVEOF13-ROBOT))
(not (ABOVEOF14-ROBOT))
(not (ABOVEOF5-ROBOT))
)
:effect
(and
(Flag216prime-)
(not (Flag216-))
)

)
(:action Flag217Action-0
:parameters ()
:precondition
(and
(ABOVEOF11-ROBOT)
)
:effect
(and
(Flag217-)
(Flag217prime-)
)

)
(:action Flag217Action-1
:parameters ()
:precondition
(and
(ABOVEOF6-ROBOT)
)
:effect
(and
(Flag217-)
(Flag217prime-)
)

)
(:action Flag217Action-2
:parameters ()
:precondition
(and
(ABOVEOF10-ROBOT)
)
:effect
(and
(Flag217-)
(Flag217prime-)
)

)
(:action Flag217Action-3
:parameters ()
:precondition
(and
(ABOVEOF8-ROBOT)
)
:effect
(and
(Flag217-)
(Flag217prime-)
)

)
(:action Flag217Action-4
:parameters ()
:precondition
(and
(ABOVEOF7-ROBOT)
)
:effect
(and
(Flag217-)
(Flag217prime-)
)

)
(:action Flag217Action-5
:parameters ()
:precondition
(and
(ABOVEOF12-ROBOT)
)
:effect
(and
(Flag217-)
(Flag217prime-)
)

)
(:action Flag217Action-6
:parameters ()
:precondition
(and
(ABOVEOF15-ROBOT)
)
:effect
(and
(Flag217-)
(Flag217prime-)
)

)
(:action Flag217Action-7
:parameters ()
:precondition
(and
(ABOVEOF9-ROBOT)
)
:effect
(and
(Flag217-)
(Flag217prime-)
)

)
(:action Flag217Action-8
:parameters ()
:precondition
(and
(ABOVEOF4-ROBOT)
)
:effect
(and
(Flag217-)
(Flag217prime-)
)

)
(:action Flag217Action-9
:parameters ()
:precondition
(and
(ABOVEOF13-ROBOT)
)
:effect
(and
(Flag217-)
(Flag217prime-)
)

)
(:action Flag217Action-10
:parameters ()
:precondition
(and
(ABOVEOF14-ROBOT)
)
:effect
(and
(Flag217-)
(Flag217prime-)
)

)
(:action Flag217Action-11
:parameters ()
:precondition
(and
(ABOVEOF5-ROBOT)
)
:effect
(and
(Flag217-)
(Flag217prime-)
)

)
(:action Prim217Action
:parameters ()
:precondition
(and
(not (ABOVEOF11-ROBOT))
(not (ABOVEOF6-ROBOT))
(not (ABOVEOF10-ROBOT))
(not (ABOVEOF8-ROBOT))
(not (ABOVEOF7-ROBOT))
(not (ABOVEOF12-ROBOT))
(not (ABOVEOF15-ROBOT))
(not (ABOVEOF9-ROBOT))
(not (ABOVEOF4-ROBOT))
(not (ABOVEOF13-ROBOT))
(not (ABOVEOF14-ROBOT))
(not (ABOVEOF5-ROBOT))
)
:effect
(and
(Flag217prime-)
(not (Flag217-))
)

)
(:action Flag218Action-0
:parameters ()
:precondition
(and
(ABOVEOF11-ROBOT)
)
:effect
(and
(Flag218-)
(Flag218prime-)
)

)
(:action Flag218Action-1
:parameters ()
:precondition
(and
(ABOVEOF6-ROBOT)
)
:effect
(and
(Flag218-)
(Flag218prime-)
)

)
(:action Flag218Action-2
:parameters ()
:precondition
(and
(ABOVEOF7-ROBOT)
)
:effect
(and
(Flag218-)
(Flag218prime-)
)

)
(:action Flag218Action-3
:parameters ()
:precondition
(and
(ABOVEOF8-ROBOT)
)
:effect
(and
(Flag218-)
(Flag218prime-)
)

)
(:action Flag218Action-4
:parameters ()
:precondition
(and
(ABOVEOF10-ROBOT)
)
:effect
(and
(Flag218-)
(Flag218prime-)
)

)
(:action Flag218Action-5
:parameters ()
:precondition
(and
(ABOVEOF12-ROBOT)
)
:effect
(and
(Flag218-)
(Flag218prime-)
)

)
(:action Flag218Action-6
:parameters ()
:precondition
(and
(ABOVEOF15-ROBOT)
)
:effect
(and
(Flag218-)
(Flag218prime-)
)

)
(:action Flag218Action-7
:parameters ()
:precondition
(and
(ABOVEOF9-ROBOT)
)
:effect
(and
(Flag218-)
(Flag218prime-)
)

)
(:action Flag218Action-8
:parameters ()
:precondition
(and
(ABOVEOF13-ROBOT)
)
:effect
(and
(Flag218-)
(Flag218prime-)
)

)
(:action Flag218Action-9
:parameters ()
:precondition
(and
(ABOVEOF14-ROBOT)
)
:effect
(and
(Flag218-)
(Flag218prime-)
)

)
(:action Flag218Action-10
:parameters ()
:precondition
(and
(ABOVEOF5-ROBOT)
)
:effect
(and
(Flag218-)
(Flag218prime-)
)

)
(:action Prim218Action
:parameters ()
:precondition
(and
(not (ABOVEOF11-ROBOT))
(not (ABOVEOF6-ROBOT))
(not (ABOVEOF7-ROBOT))
(not (ABOVEOF8-ROBOT))
(not (ABOVEOF10-ROBOT))
(not (ABOVEOF12-ROBOT))
(not (ABOVEOF15-ROBOT))
(not (ABOVEOF9-ROBOT))
(not (ABOVEOF13-ROBOT))
(not (ABOVEOF14-ROBOT))
(not (ABOVEOF5-ROBOT))
)
:effect
(and
(Flag218prime-)
(not (Flag218-))
)

)
(:action Flag219Action-0
:parameters ()
:precondition
(and
(ABOVEOF11-ROBOT)
)
:effect
(and
(Flag219-)
(Flag219prime-)
)

)
(:action Flag219Action-1
:parameters ()
:precondition
(and
(ABOVEOF6-ROBOT)
)
:effect
(and
(Flag219-)
(Flag219prime-)
)

)
(:action Flag219Action-2
:parameters ()
:precondition
(and
(ABOVEOF7-ROBOT)
)
:effect
(and
(Flag219-)
(Flag219prime-)
)

)
(:action Flag219Action-3
:parameters ()
:precondition
(and
(ABOVEOF8-ROBOT)
)
:effect
(and
(Flag219-)
(Flag219prime-)
)

)
(:action Flag219Action-4
:parameters ()
:precondition
(and
(ABOVEOF10-ROBOT)
)
:effect
(and
(Flag219-)
(Flag219prime-)
)

)
(:action Flag219Action-5
:parameters ()
:precondition
(and
(ABOVEOF12-ROBOT)
)
:effect
(and
(Flag219-)
(Flag219prime-)
)

)
(:action Flag219Action-6
:parameters ()
:precondition
(and
(ABOVEOF15-ROBOT)
)
:effect
(and
(Flag219-)
(Flag219prime-)
)

)
(:action Flag219Action-7
:parameters ()
:precondition
(and
(ABOVEOF9-ROBOT)
)
:effect
(and
(Flag219-)
(Flag219prime-)
)

)
(:action Flag219Action-8
:parameters ()
:precondition
(and
(ABOVEOF13-ROBOT)
)
:effect
(and
(Flag219-)
(Flag219prime-)
)

)
(:action Flag219Action-9
:parameters ()
:precondition
(and
(ABOVEOF14-ROBOT)
)
:effect
(and
(Flag219-)
(Flag219prime-)
)

)
(:action Prim219Action
:parameters ()
:precondition
(and
(not (ABOVEOF11-ROBOT))
(not (ABOVEOF6-ROBOT))
(not (ABOVEOF7-ROBOT))
(not (ABOVEOF8-ROBOT))
(not (ABOVEOF10-ROBOT))
(not (ABOVEOF12-ROBOT))
(not (ABOVEOF15-ROBOT))
(not (ABOVEOF9-ROBOT))
(not (ABOVEOF13-ROBOT))
(not (ABOVEOF14-ROBOT))
)
:effect
(and
(Flag219prime-)
(not (Flag219-))
)

)
(:action Flag220Action-0
:parameters ()
:precondition
(and
(ABOVEOF11-ROBOT)
)
:effect
(and
(Flag220-)
(Flag220prime-)
)

)
(:action Flag220Action-1
:parameters ()
:precondition
(and
(ABOVEOF7-ROBOT)
)
:effect
(and
(Flag220-)
(Flag220prime-)
)

)
(:action Flag220Action-2
:parameters ()
:precondition
(and
(ABOVEOF8-ROBOT)
)
:effect
(and
(Flag220-)
(Flag220prime-)
)

)
(:action Flag220Action-3
:parameters ()
:precondition
(and
(ABOVEOF10-ROBOT)
)
:effect
(and
(Flag220-)
(Flag220prime-)
)

)
(:action Flag220Action-4
:parameters ()
:precondition
(and
(ABOVEOF12-ROBOT)
)
:effect
(and
(Flag220-)
(Flag220prime-)
)

)
(:action Flag220Action-5
:parameters ()
:precondition
(and
(ABOVEOF15-ROBOT)
)
:effect
(and
(Flag220-)
(Flag220prime-)
)

)
(:action Flag220Action-6
:parameters ()
:precondition
(and
(ABOVEOF9-ROBOT)
)
:effect
(and
(Flag220-)
(Flag220prime-)
)

)
(:action Flag220Action-7
:parameters ()
:precondition
(and
(ABOVEOF13-ROBOT)
)
:effect
(and
(Flag220-)
(Flag220prime-)
)

)
(:action Flag220Action-8
:parameters ()
:precondition
(and
(ABOVEOF14-ROBOT)
)
:effect
(and
(Flag220-)
(Flag220prime-)
)

)
(:action Prim220Action
:parameters ()
:precondition
(and
(not (ABOVEOF11-ROBOT))
(not (ABOVEOF7-ROBOT))
(not (ABOVEOF8-ROBOT))
(not (ABOVEOF10-ROBOT))
(not (ABOVEOF12-ROBOT))
(not (ABOVEOF15-ROBOT))
(not (ABOVEOF9-ROBOT))
(not (ABOVEOF13-ROBOT))
(not (ABOVEOF14-ROBOT))
)
:effect
(and
(Flag220prime-)
(not (Flag220-))
)

)
(:action Flag221Action-0
:parameters ()
:precondition
(and
(ABOVEOF11-ROBOT)
)
:effect
(and
(Flag221-)
(Flag221prime-)
)

)
(:action Flag221Action-1
:parameters ()
:precondition
(and
(ABOVEOF8-ROBOT)
)
:effect
(and
(Flag221-)
(Flag221prime-)
)

)
(:action Flag221Action-2
:parameters ()
:precondition
(and
(ABOVEOF10-ROBOT)
)
:effect
(and
(Flag221-)
(Flag221prime-)
)

)
(:action Flag221Action-3
:parameters ()
:precondition
(and
(ABOVEOF12-ROBOT)
)
:effect
(and
(Flag221-)
(Flag221prime-)
)

)
(:action Flag221Action-4
:parameters ()
:precondition
(and
(ABOVEOF15-ROBOT)
)
:effect
(and
(Flag221-)
(Flag221prime-)
)

)
(:action Flag221Action-5
:parameters ()
:precondition
(and
(ABOVEOF9-ROBOT)
)
:effect
(and
(Flag221-)
(Flag221prime-)
)

)
(:action Flag221Action-6
:parameters ()
:precondition
(and
(ABOVEOF13-ROBOT)
)
:effect
(and
(Flag221-)
(Flag221prime-)
)

)
(:action Flag221Action-7
:parameters ()
:precondition
(and
(ABOVEOF14-ROBOT)
)
:effect
(and
(Flag221-)
(Flag221prime-)
)

)
(:action Prim221Action
:parameters ()
:precondition
(and
(not (ABOVEOF11-ROBOT))
(not (ABOVEOF8-ROBOT))
(not (ABOVEOF10-ROBOT))
(not (ABOVEOF12-ROBOT))
(not (ABOVEOF15-ROBOT))
(not (ABOVEOF9-ROBOT))
(not (ABOVEOF13-ROBOT))
(not (ABOVEOF14-ROBOT))
)
:effect
(and
(Flag221prime-)
(not (Flag221-))
)

)
(:action Flag222Action-0
:parameters ()
:precondition
(and
(ABOVEOF11-ROBOT)
)
:effect
(and
(Flag222-)
(Flag222prime-)
)

)
(:action Flag222Action-1
:parameters ()
:precondition
(and
(ABOVEOF10-ROBOT)
)
:effect
(and
(Flag222-)
(Flag222prime-)
)

)
(:action Flag222Action-2
:parameters ()
:precondition
(and
(ABOVEOF12-ROBOT)
)
:effect
(and
(Flag222-)
(Flag222prime-)
)

)
(:action Flag222Action-3
:parameters ()
:precondition
(and
(ABOVEOF15-ROBOT)
)
:effect
(and
(Flag222-)
(Flag222prime-)
)

)
(:action Flag222Action-4
:parameters ()
:precondition
(and
(ABOVEOF9-ROBOT)
)
:effect
(and
(Flag222-)
(Flag222prime-)
)

)
(:action Flag222Action-5
:parameters ()
:precondition
(and
(ABOVEOF13-ROBOT)
)
:effect
(and
(Flag222-)
(Flag222prime-)
)

)
(:action Flag222Action-6
:parameters ()
:precondition
(and
(ABOVEOF14-ROBOT)
)
:effect
(and
(Flag222-)
(Flag222prime-)
)

)
(:action Prim222Action
:parameters ()
:precondition
(and
(not (ABOVEOF11-ROBOT))
(not (ABOVEOF10-ROBOT))
(not (ABOVEOF12-ROBOT))
(not (ABOVEOF15-ROBOT))
(not (ABOVEOF9-ROBOT))
(not (ABOVEOF13-ROBOT))
(not (ABOVEOF14-ROBOT))
)
:effect
(and
(Flag222prime-)
(not (Flag222-))
)

)
(:action Flag223Action-0
:parameters ()
:precondition
(and
(ABOVEOF11-ROBOT)
)
:effect
(and
(Flag223-)
(Flag223prime-)
)

)
(:action Flag223Action-1
:parameters ()
:precondition
(and
(ABOVEOF10-ROBOT)
)
:effect
(and
(Flag223-)
(Flag223prime-)
)

)
(:action Flag223Action-2
:parameters ()
:precondition
(and
(ABOVEOF12-ROBOT)
)
:effect
(and
(Flag223-)
(Flag223prime-)
)

)
(:action Flag223Action-3
:parameters ()
:precondition
(and
(ABOVEOF15-ROBOT)
)
:effect
(and
(Flag223-)
(Flag223prime-)
)

)
(:action Flag223Action-4
:parameters ()
:precondition
(and
(ABOVEOF13-ROBOT)
)
:effect
(and
(Flag223-)
(Flag223prime-)
)

)
(:action Flag223Action-5
:parameters ()
:precondition
(and
(ABOVEOF14-ROBOT)
)
:effect
(and
(Flag223-)
(Flag223prime-)
)

)
(:action Prim223Action
:parameters ()
:precondition
(and
(not (ABOVEOF11-ROBOT))
(not (ABOVEOF10-ROBOT))
(not (ABOVEOF12-ROBOT))
(not (ABOVEOF15-ROBOT))
(not (ABOVEOF13-ROBOT))
(not (ABOVEOF14-ROBOT))
)
:effect
(and
(Flag223prime-)
(not (Flag223-))
)

)
(:action Flag224Action-0
:parameters ()
:precondition
(and
(ABOVEOF12-ROBOT)
)
:effect
(and
(Flag224-)
(Flag224prime-)
)

)
(:action Flag224Action-1
:parameters ()
:precondition
(and
(ABOVEOF13-ROBOT)
)
:effect
(and
(Flag224-)
(Flag224prime-)
)

)
(:action Flag224Action-2
:parameters ()
:precondition
(and
(ABOVEOF14-ROBOT)
)
:effect
(and
(Flag224-)
(Flag224prime-)
)

)
(:action Flag224Action-3
:parameters ()
:precondition
(and
(ABOVEOF11-ROBOT)
)
:effect
(and
(Flag224-)
(Flag224prime-)
)

)
(:action Flag224Action-4
:parameters ()
:precondition
(and
(ABOVEOF15-ROBOT)
)
:effect
(and
(Flag224-)
(Flag224prime-)
)

)
(:action Prim224Action
:parameters ()
:precondition
(and
(not (ABOVEOF12-ROBOT))
(not (ABOVEOF13-ROBOT))
(not (ABOVEOF14-ROBOT))
(not (ABOVEOF11-ROBOT))
(not (ABOVEOF15-ROBOT))
)
:effect
(and
(Flag224prime-)
(not (Flag224-))
)

)
(:action Flag225Action-0
:parameters ()
:precondition
(and
(ABOVEOF12-ROBOT)
)
:effect
(and
(Flag225-)
(Flag225prime-)
)

)
(:action Flag225Action-1
:parameters ()
:precondition
(and
(ABOVEOF13-ROBOT)
)
:effect
(and
(Flag225-)
(Flag225prime-)
)

)
(:action Flag225Action-2
:parameters ()
:precondition
(and
(ABOVEOF14-ROBOT)
)
:effect
(and
(Flag225-)
(Flag225prime-)
)

)
(:action Flag225Action-3
:parameters ()
:precondition
(and
(ABOVEOF15-ROBOT)
)
:effect
(and
(Flag225-)
(Flag225prime-)
)

)
(:action Prim225Action
:parameters ()
:precondition
(and
(not (ABOVEOF12-ROBOT))
(not (ABOVEOF13-ROBOT))
(not (ABOVEOF14-ROBOT))
(not (ABOVEOF15-ROBOT))
)
:effect
(and
(Flag225prime-)
(not (Flag225-))
)

)
(:action Flag226Action-0
:parameters ()
:precondition
(and
(ABOVEOF13-ROBOT)
)
:effect
(and
(Flag226-)
(Flag226prime-)
)

)
(:action Flag226Action-1
:parameters ()
:precondition
(and
(ABOVEOF14-ROBOT)
)
:effect
(and
(Flag226-)
(Flag226prime-)
)

)
(:action Flag226Action-2
:parameters ()
:precondition
(and
(ABOVEOF15-ROBOT)
)
:effect
(and
(Flag226-)
(Flag226prime-)
)

)
(:action Prim226Action
:parameters ()
:precondition
(and
(not (ABOVEOF13-ROBOT))
(not (ABOVEOF14-ROBOT))
(not (ABOVEOF15-ROBOT))
)
:effect
(and
(Flag226prime-)
(not (Flag226-))
)

)
(:action Flag227Action-0
:parameters ()
:precondition
(and
(ABOVEOF14-ROBOT)
)
:effect
(and
(Flag227-)
(Flag227prime-)
)

)
(:action Flag227Action-1
:parameters ()
:precondition
(and
(ABOVEOF15-ROBOT)
)
:effect
(and
(Flag227-)
(Flag227prime-)
)

)
(:action Prim227Action
:parameters ()
:precondition
(and
(not (ABOVEOF14-ROBOT))
(not (ABOVEOF15-ROBOT))
)
:effect
(and
(Flag227prime-)
(not (Flag227-))
)

)
(:action Flag228Action
:parameters ()
:precondition
(and
(ABOVEOF2-ROBOT)
(BELOWOF2-ROBOT)
)
:effect
(and
(Flag228-)
(Flag228prime-)
)

)
(:action Flag229Action
:parameters ()
:precondition
(and
(ABOVEOF11-ROBOT)
(BELOWOF1-ROBOT)
)
:effect
(and
(Flag229-)
(Flag229prime-)
)

)
(:action Flag230Action
:parameters ()
:precondition
(and
(ABOVEOF1-ROBOT)
(BELOWOF2-ROBOT)
)
:effect
(and
(Flag230-)
(Flag230prime-)
)

)
(:action Flag231Action
:parameters ()
:precondition
(and
(ABOVEOF7-ROBOT)
(BELOWOF2-ROBOT)
)
:effect
(and
(Flag231-)
(Flag231prime-)
)

)
(:action Flag232Action
:parameters ()
:precondition
(and
(ABOVEOF15-ROBOT)
(BELOWOF2-ROBOT)
)
:effect
(and
(Flag232-)
(Flag232prime-)
)

)
(:action Flag233Action
:parameters ()
:precondition
(and
(BELOWOF2-ROBOT)
(ABOVEOF13-ROBOT)
)
:effect
(and
(Flag233-)
(Flag233prime-)
)

)
(:action Flag234Action
:parameters ()
:precondition
(and
(BELOWOF1-ROBOT)
(ABOVEOF7-ROBOT)
)
:effect
(and
(Flag234-)
(Flag234prime-)
)

)
(:action Flag235Action
:parameters ()
:precondition
(and
(BELOWOF1-ROBOT)
(ABOVEOF1-ROBOT)
)
:effect
(and
(Flag235-)
(Flag235prime-)
)

)
(:action Flag236Action
:parameters ()
:precondition
(and
(BELOWOF1-ROBOT)
(ABOVEOF5-ROBOT)
)
:effect
(and
(Flag236-)
(Flag236prime-)
)

)
(:action Flag237Action
:parameters ()
:precondition
(and
(ABOVEOF4-ROBOT)
(BELOWOF2-ROBOT)
)
:effect
(and
(Flag237-)
(Flag237prime-)
)

)
(:action Flag238Action
:parameters ()
:precondition
(and
(BELOWOF2-ROBOT)
(ABOVEOF14-ROBOT)
)
:effect
(and
(Flag238-)
(Flag238prime-)
)

)
(:action Flag239Action
:parameters ()
:precondition
(and
(ABOVEOF6-ROBOT)
(BELOWOF2-ROBOT)
)
:effect
(and
(Flag239-)
(Flag239prime-)
)

)
(:action Flag240Action
:parameters ()
:precondition
(and
(BELOWOF1-ROBOT)
(ABOVEOF2-ROBOT)
)
:effect
(and
(Flag240-)
(Flag240prime-)
)

)
(:action Flag241Action
:parameters ()
:precondition
(and
(ABOVEOF9-ROBOT)
(BELOWOF2-ROBOT)
)
:effect
(and
(Flag241-)
(Flag241prime-)
)

)
(:action Flag242Action
:parameters ()
:precondition
(and
(BELOWOF1-ROBOT)
(ABOVEOF13-ROBOT)
)
:effect
(and
(Flag242-)
(Flag242prime-)
)

)
(:action Flag243Action
:parameters ()
:precondition
(and
(ABOVEOF6-ROBOT)
(BELOWOF1-ROBOT)
)
:effect
(and
(Flag243-)
(Flag243prime-)
)

)
(:action Flag244Action
:parameters ()
:precondition
(and
(BELOWOF1-ROBOT)
(ABOVEOF14-ROBOT)
)
:effect
(and
(Flag244-)
(Flag244prime-)
)

)
(:action Flag245Action
:parameters ()
:precondition
(and
(ABOVEOF10-ROBOT)
(BELOWOF1-ROBOT)
)
:effect
(and
(Flag245-)
(Flag245prime-)
)

)
(:action Flag246Action
:parameters ()
:precondition
(and
(ABOVEOF11-ROBOT)
(BELOWOF2-ROBOT)
)
:effect
(and
(Flag246-)
(Flag246prime-)
)

)
(:action Flag247Action
:parameters ()
:precondition
(and
(ABOVEOF3-ROBOT)
(BELOWOF1-ROBOT)
)
:effect
(and
(Flag247-)
(Flag247prime-)
)

)
(:action Flag248Action
:parameters ()
:precondition
(and
(BELOWOF1-ROBOT)
(ABOVEOF9-ROBOT)
)
:effect
(and
(Flag248-)
(Flag248prime-)
)

)
(:action Flag249Action
:parameters ()
:precondition
(and
(ABOVEOF10-ROBOT)
(BELOWOF2-ROBOT)
)
:effect
(and
(Flag249-)
(Flag249prime-)
)

)
(:action Flag250Action
:parameters ()
:precondition
(and
(ABOVEOF8-ROBOT)
(BELOWOF2-ROBOT)
)
:effect
(and
(Flag250-)
(Flag250prime-)
)

)
(:action Flag251Action
:parameters ()
:precondition
(and
(BELOWOF1-ROBOT)
(ABOVEOF4-ROBOT)
)
:effect
(and
(Flag251-)
(Flag251prime-)
)

)
(:action Flag252Action
:parameters ()
:precondition
(and
(ABOVEOF12-ROBOT)
(BELOWOF2-ROBOT)
)
:effect
(and
(Flag252-)
(Flag252prime-)
)

)
(:action Flag253Action
:parameters ()
:precondition
(and
(ABOVEOF12-ROBOT)
(BELOWOF1-ROBOT)
)
:effect
(and
(Flag253-)
(Flag253prime-)
)

)
(:action Flag254Action
:parameters ()
:precondition
(and
(BELOWOF2-ROBOT)
(ABOVEOF5-ROBOT)
)
:effect
(and
(Flag254-)
(Flag254prime-)
)

)
(:action Flag255Action
:parameters ()
:precondition
(and
(BELOWOF1-ROBOT)
(ABOVEOF15-ROBOT)
)
:effect
(and
(Flag255-)
(Flag255prime-)
)

)
(:action Flag256Action
:parameters ()
:precondition
(and
(ABOVEOF8-ROBOT)
(BELOWOF1-ROBOT)
)
:effect
(and
(Flag256-)
(Flag256prime-)
)

)
(:action Flag257Action
:parameters ()
:precondition
(and
(ABOVEOF3-ROBOT)
(BELOWOF2-ROBOT)
)
:effect
(and
(Flag257-)
(Flag257prime-)
)

)
(:action Prim228Action-0
:parameters ()
:precondition
(and
(not (ABOVEOF2-ROBOT))
)
:effect
(and
(Flag228prime-)
(not (Flag228-))
)

)
(:action Prim228Action-1
:parameters ()
:precondition
(and
(not (BELOWOF2-ROBOT))
)
:effect
(and
(Flag228prime-)
(not (Flag228-))
)

)
(:action Prim229Action-0
:parameters ()
:precondition
(and
(not (ABOVEOF11-ROBOT))
)
:effect
(and
(Flag229prime-)
(not (Flag229-))
)

)
(:action Prim229Action-1
:parameters ()
:precondition
(and
(not (BELOWOF1-ROBOT))
)
:effect
(and
(Flag229prime-)
(not (Flag229-))
)

)
(:action Prim230Action-0
:parameters ()
:precondition
(and
(not (ABOVEOF1-ROBOT))
)
:effect
(and
(Flag230prime-)
(not (Flag230-))
)

)
(:action Prim230Action-1
:parameters ()
:precondition
(and
(not (BELOWOF2-ROBOT))
)
:effect
(and
(Flag230prime-)
(not (Flag230-))
)

)
(:action Prim231Action-0
:parameters ()
:precondition
(and
(not (ABOVEOF7-ROBOT))
)
:effect
(and
(Flag231prime-)
(not (Flag231-))
)

)
(:action Prim231Action-1
:parameters ()
:precondition
(and
(not (BELOWOF2-ROBOT))
)
:effect
(and
(Flag231prime-)
(not (Flag231-))
)

)
(:action Prim232Action-0
:parameters ()
:precondition
(and
(not (ABOVEOF15-ROBOT))
)
:effect
(and
(Flag232prime-)
(not (Flag232-))
)

)
(:action Prim232Action-1
:parameters ()
:precondition
(and
(not (BELOWOF2-ROBOT))
)
:effect
(and
(Flag232prime-)
(not (Flag232-))
)

)
(:action Prim233Action-0
:parameters ()
:precondition
(and
(not (BELOWOF2-ROBOT))
)
:effect
(and
(Flag233prime-)
(not (Flag233-))
)

)
(:action Prim233Action-1
:parameters ()
:precondition
(and
(not (ABOVEOF13-ROBOT))
)
:effect
(and
(Flag233prime-)
(not (Flag233-))
)

)
(:action Prim234Action-0
:parameters ()
:precondition
(and
(not (BELOWOF1-ROBOT))
)
:effect
(and
(Flag234prime-)
(not (Flag234-))
)

)
(:action Prim234Action-1
:parameters ()
:precondition
(and
(not (ABOVEOF7-ROBOT))
)
:effect
(and
(Flag234prime-)
(not (Flag234-))
)

)
(:action Prim235Action-0
:parameters ()
:precondition
(and
(not (BELOWOF1-ROBOT))
)
:effect
(and
(Flag235prime-)
(not (Flag235-))
)

)
(:action Prim235Action-1
:parameters ()
:precondition
(and
(not (ABOVEOF1-ROBOT))
)
:effect
(and
(Flag235prime-)
(not (Flag235-))
)

)
(:action Prim236Action-0
:parameters ()
:precondition
(and
(not (BELOWOF1-ROBOT))
)
:effect
(and
(Flag236prime-)
(not (Flag236-))
)

)
(:action Prim236Action-1
:parameters ()
:precondition
(and
(not (ABOVEOF5-ROBOT))
)
:effect
(and
(Flag236prime-)
(not (Flag236-))
)

)
(:action Prim237Action-0
:parameters ()
:precondition
(and
(not (ABOVEOF4-ROBOT))
)
:effect
(and
(Flag237prime-)
(not (Flag237-))
)

)
(:action Prim237Action-1
:parameters ()
:precondition
(and
(not (BELOWOF2-ROBOT))
)
:effect
(and
(Flag237prime-)
(not (Flag237-))
)

)
(:action Prim238Action-0
:parameters ()
:precondition
(and
(not (BELOWOF2-ROBOT))
)
:effect
(and
(Flag238prime-)
(not (Flag238-))
)

)
(:action Prim238Action-1
:parameters ()
:precondition
(and
(not (ABOVEOF14-ROBOT))
)
:effect
(and
(Flag238prime-)
(not (Flag238-))
)

)
(:action Prim239Action-0
:parameters ()
:precondition
(and
(not (ABOVEOF6-ROBOT))
)
:effect
(and
(Flag239prime-)
(not (Flag239-))
)

)
(:action Prim239Action-1
:parameters ()
:precondition
(and
(not (BELOWOF2-ROBOT))
)
:effect
(and
(Flag239prime-)
(not (Flag239-))
)

)
(:action Prim240Action-0
:parameters ()
:precondition
(and
(not (BELOWOF1-ROBOT))
)
:effect
(and
(Flag240prime-)
(not (Flag240-))
)

)
(:action Prim240Action-1
:parameters ()
:precondition
(and
(not (ABOVEOF2-ROBOT))
)
:effect
(and
(Flag240prime-)
(not (Flag240-))
)

)
(:action Prim241Action-0
:parameters ()
:precondition
(and
(not (ABOVEOF9-ROBOT))
)
:effect
(and
(Flag241prime-)
(not (Flag241-))
)

)
(:action Prim241Action-1
:parameters ()
:precondition
(and
(not (BELOWOF2-ROBOT))
)
:effect
(and
(Flag241prime-)
(not (Flag241-))
)

)
(:action Prim242Action-0
:parameters ()
:precondition
(and
(not (BELOWOF1-ROBOT))
)
:effect
(and
(Flag242prime-)
(not (Flag242-))
)

)
(:action Prim242Action-1
:parameters ()
:precondition
(and
(not (ABOVEOF13-ROBOT))
)
:effect
(and
(Flag242prime-)
(not (Flag242-))
)

)
(:action Prim243Action-0
:parameters ()
:precondition
(and
(not (ABOVEOF6-ROBOT))
)
:effect
(and
(Flag243prime-)
(not (Flag243-))
)

)
(:action Prim243Action-1
:parameters ()
:precondition
(and
(not (BELOWOF1-ROBOT))
)
:effect
(and
(Flag243prime-)
(not (Flag243-))
)

)
(:action Prim244Action-0
:parameters ()
:precondition
(and
(not (BELOWOF1-ROBOT))
)
:effect
(and
(Flag244prime-)
(not (Flag244-))
)

)
(:action Prim244Action-1
:parameters ()
:precondition
(and
(not (ABOVEOF14-ROBOT))
)
:effect
(and
(Flag244prime-)
(not (Flag244-))
)

)
(:action Prim245Action-0
:parameters ()
:precondition
(and
(not (ABOVEOF10-ROBOT))
)
:effect
(and
(Flag245prime-)
(not (Flag245-))
)

)
(:action Prim245Action-1
:parameters ()
:precondition
(and
(not (BELOWOF1-ROBOT))
)
:effect
(and
(Flag245prime-)
(not (Flag245-))
)

)
(:action Prim246Action-0
:parameters ()
:precondition
(and
(not (ABOVEOF11-ROBOT))
)
:effect
(and
(Flag246prime-)
(not (Flag246-))
)

)
(:action Prim246Action-1
:parameters ()
:precondition
(and
(not (BELOWOF2-ROBOT))
)
:effect
(and
(Flag246prime-)
(not (Flag246-))
)

)
(:action Prim247Action-0
:parameters ()
:precondition
(and
(not (ABOVEOF3-ROBOT))
)
:effect
(and
(Flag247prime-)
(not (Flag247-))
)

)
(:action Prim247Action-1
:parameters ()
:precondition
(and
(not (BELOWOF1-ROBOT))
)
:effect
(and
(Flag247prime-)
(not (Flag247-))
)

)
(:action Prim248Action-0
:parameters ()
:precondition
(and
(not (BELOWOF1-ROBOT))
)
:effect
(and
(Flag248prime-)
(not (Flag248-))
)

)
(:action Prim248Action-1
:parameters ()
:precondition
(and
(not (ABOVEOF9-ROBOT))
)
:effect
(and
(Flag248prime-)
(not (Flag248-))
)

)
(:action Prim249Action-0
:parameters ()
:precondition
(and
(not (ABOVEOF10-ROBOT))
)
:effect
(and
(Flag249prime-)
(not (Flag249-))
)

)
(:action Prim249Action-1
:parameters ()
:precondition
(and
(not (BELOWOF2-ROBOT))
)
:effect
(and
(Flag249prime-)
(not (Flag249-))
)

)
(:action Prim250Action-0
:parameters ()
:precondition
(and
(not (ABOVEOF8-ROBOT))
)
:effect
(and
(Flag250prime-)
(not (Flag250-))
)

)
(:action Prim250Action-1
:parameters ()
:precondition
(and
(not (BELOWOF2-ROBOT))
)
:effect
(and
(Flag250prime-)
(not (Flag250-))
)

)
(:action Prim251Action-0
:parameters ()
:precondition
(and
(not (BELOWOF1-ROBOT))
)
:effect
(and
(Flag251prime-)
(not (Flag251-))
)

)
(:action Prim251Action-1
:parameters ()
:precondition
(and
(not (ABOVEOF4-ROBOT))
)
:effect
(and
(Flag251prime-)
(not (Flag251-))
)

)
(:action Prim252Action-0
:parameters ()
:precondition
(and
(not (ABOVEOF12-ROBOT))
)
:effect
(and
(Flag252prime-)
(not (Flag252-))
)

)
(:action Prim252Action-1
:parameters ()
:precondition
(and
(not (BELOWOF2-ROBOT))
)
:effect
(and
(Flag252prime-)
(not (Flag252-))
)

)
(:action Prim253Action-0
:parameters ()
:precondition
(and
(not (ABOVEOF12-ROBOT))
)
:effect
(and
(Flag253prime-)
(not (Flag253-))
)

)
(:action Prim253Action-1
:parameters ()
:precondition
(and
(not (BELOWOF1-ROBOT))
)
:effect
(and
(Flag253prime-)
(not (Flag253-))
)

)
(:action Prim254Action-0
:parameters ()
:precondition
(and
(not (BELOWOF2-ROBOT))
)
:effect
(and
(Flag254prime-)
(not (Flag254-))
)

)
(:action Prim254Action-1
:parameters ()
:precondition
(and
(not (ABOVEOF5-ROBOT))
)
:effect
(and
(Flag254prime-)
(not (Flag254-))
)

)
(:action Prim255Action-0
:parameters ()
:precondition
(and
(not (BELOWOF1-ROBOT))
)
:effect
(and
(Flag255prime-)
(not (Flag255-))
)

)
(:action Prim255Action-1
:parameters ()
:precondition
(and
(not (ABOVEOF15-ROBOT))
)
:effect
(and
(Flag255prime-)
(not (Flag255-))
)

)
(:action Prim256Action-0
:parameters ()
:precondition
(and
(not (ABOVEOF8-ROBOT))
)
:effect
(and
(Flag256prime-)
(not (Flag256-))
)

)
(:action Prim256Action-1
:parameters ()
:precondition
(and
(not (BELOWOF1-ROBOT))
)
:effect
(and
(Flag256prime-)
(not (Flag256-))
)

)
(:action Prim257Action-0
:parameters ()
:precondition
(and
(not (ABOVEOF3-ROBOT))
)
:effect
(and
(Flag257prime-)
(not (Flag257-))
)

)
(:action Prim257Action-1
:parameters ()
:precondition
(and
(not (BELOWOF2-ROBOT))
)
:effect
(and
(Flag257prime-)
(not (Flag257-))
)

)
(:action Flag259Action
:parameters ()
:precondition
(and
(BELOWOF3-ROBOT)
(ABOVEOF5-ROBOT)
)
:effect
(and
(Flag259-)
(Flag259prime-)
)

)
(:action Flag260Action
:parameters ()
:precondition
(and
(BELOWOF3-ROBOT)
(ABOVEOF6-ROBOT)
)
:effect
(and
(Flag260-)
(Flag260prime-)
)

)
(:action Flag261Action
:parameters ()
:precondition
(and
(BELOWOF3-ROBOT)
(ABOVEOF7-ROBOT)
)
:effect
(and
(Flag261-)
(Flag261prime-)
)

)
(:action Flag262Action
:parameters ()
:precondition
(and
(BELOWOF3-ROBOT)
(ABOVEOF14-ROBOT)
)
:effect
(and
(Flag262-)
(Flag262prime-)
)

)
(:action Flag263Action
:parameters ()
:precondition
(and
(BELOWOF3-ROBOT)
(ABOVEOF9-ROBOT)
)
:effect
(and
(Flag263-)
(Flag263prime-)
)

)
(:action Flag264Action
:parameters ()
:precondition
(and
(BELOWOF3-ROBOT)
(ABOVEOF15-ROBOT)
)
:effect
(and
(Flag264-)
(Flag264prime-)
)

)
(:action Flag265Action
:parameters ()
:precondition
(and
(BELOWOF3-ROBOT)
(ABOVEOF2-ROBOT)
)
:effect
(and
(Flag265-)
(Flag265prime-)
)

)
(:action Flag266Action
:parameters ()
:precondition
(and
(ABOVEOF3-ROBOT)
(BELOWOF3-ROBOT)
)
:effect
(and
(Flag266-)
(Flag266prime-)
)

)
(:action Flag267Action
:parameters ()
:precondition
(and
(ABOVEOF8-ROBOT)
(BELOWOF3-ROBOT)
)
:effect
(and
(Flag267-)
(Flag267prime-)
)

)
(:action Flag268Action
:parameters ()
:precondition
(and
(BELOWOF3-ROBOT)
(ABOVEOF4-ROBOT)
)
:effect
(and
(Flag268-)
(Flag268prime-)
)

)
(:action Flag269Action
:parameters ()
:precondition
(and
(BELOWOF3-ROBOT)
(ABOVEOF10-ROBOT)
)
:effect
(and
(Flag269-)
(Flag269prime-)
)

)
(:action Flag270Action
:parameters ()
:precondition
(and
(BELOWOF2-ROBOT)
(ABOVEOF2-ROBOT)
)
:effect
(and
(Flag270-)
(Flag270prime-)
)

)
(:action Flag271Action
:parameters ()
:precondition
(and
(ABOVEOF12-ROBOT)
(BELOWOF3-ROBOT)
)
:effect
(and
(Flag271-)
(Flag271prime-)
)

)
(:action Flag272Action
:parameters ()
:precondition
(and
(BELOWOF3-ROBOT)
(ABOVEOF13-ROBOT)
)
:effect
(and
(Flag272-)
(Flag272prime-)
)

)
(:action Flag273Action
:parameters ()
:precondition
(and
(ABOVEOF11-ROBOT)
(BELOWOF3-ROBOT)
)
:effect
(and
(Flag273-)
(Flag273prime-)
)

)
(:action Prim259Action-0
:parameters ()
:precondition
(and
(not (BELOWOF3-ROBOT))
)
:effect
(and
(Flag259prime-)
(not (Flag259-))
)

)
(:action Prim259Action-1
:parameters ()
:precondition
(and
(not (ABOVEOF5-ROBOT))
)
:effect
(and
(Flag259prime-)
(not (Flag259-))
)

)
(:action Prim260Action-0
:parameters ()
:precondition
(and
(not (BELOWOF3-ROBOT))
)
:effect
(and
(Flag260prime-)
(not (Flag260-))
)

)
(:action Prim260Action-1
:parameters ()
:precondition
(and
(not (ABOVEOF6-ROBOT))
)
:effect
(and
(Flag260prime-)
(not (Flag260-))
)

)
(:action Prim261Action-0
:parameters ()
:precondition
(and
(not (BELOWOF3-ROBOT))
)
:effect
(and
(Flag261prime-)
(not (Flag261-))
)

)
(:action Prim261Action-1
:parameters ()
:precondition
(and
(not (ABOVEOF7-ROBOT))
)
:effect
(and
(Flag261prime-)
(not (Flag261-))
)

)
(:action Prim262Action-0
:parameters ()
:precondition
(and
(not (BELOWOF3-ROBOT))
)
:effect
(and
(Flag262prime-)
(not (Flag262-))
)

)
(:action Prim262Action-1
:parameters ()
:precondition
(and
(not (ABOVEOF14-ROBOT))
)
:effect
(and
(Flag262prime-)
(not (Flag262-))
)

)
(:action Prim263Action-0
:parameters ()
:precondition
(and
(not (BELOWOF3-ROBOT))
)
:effect
(and
(Flag263prime-)
(not (Flag263-))
)

)
(:action Prim263Action-1
:parameters ()
:precondition
(and
(not (ABOVEOF9-ROBOT))
)
:effect
(and
(Flag263prime-)
(not (Flag263-))
)

)
(:action Prim264Action-0
:parameters ()
:precondition
(and
(not (BELOWOF3-ROBOT))
)
:effect
(and
(Flag264prime-)
(not (Flag264-))
)

)
(:action Prim264Action-1
:parameters ()
:precondition
(and
(not (ABOVEOF15-ROBOT))
)
:effect
(and
(Flag264prime-)
(not (Flag264-))
)

)
(:action Prim265Action-0
:parameters ()
:precondition
(and
(not (BELOWOF3-ROBOT))
)
:effect
(and
(Flag265prime-)
(not (Flag265-))
)

)
(:action Prim265Action-1
:parameters ()
:precondition
(and
(not (ABOVEOF2-ROBOT))
)
:effect
(and
(Flag265prime-)
(not (Flag265-))
)

)
(:action Prim266Action-0
:parameters ()
:precondition
(and
(not (ABOVEOF3-ROBOT))
)
:effect
(and
(Flag266prime-)
(not (Flag266-))
)

)
(:action Prim266Action-1
:parameters ()
:precondition
(and
(not (BELOWOF3-ROBOT))
)
:effect
(and
(Flag266prime-)
(not (Flag266-))
)

)
(:action Prim267Action-0
:parameters ()
:precondition
(and
(not (ABOVEOF8-ROBOT))
)
:effect
(and
(Flag267prime-)
(not (Flag267-))
)

)
(:action Prim267Action-1
:parameters ()
:precondition
(and
(not (BELOWOF3-ROBOT))
)
:effect
(and
(Flag267prime-)
(not (Flag267-))
)

)
(:action Prim268Action-0
:parameters ()
:precondition
(and
(not (BELOWOF3-ROBOT))
)
:effect
(and
(Flag268prime-)
(not (Flag268-))
)

)
(:action Prim268Action-1
:parameters ()
:precondition
(and
(not (ABOVEOF4-ROBOT))
)
:effect
(and
(Flag268prime-)
(not (Flag268-))
)

)
(:action Prim269Action-0
:parameters ()
:precondition
(and
(not (BELOWOF3-ROBOT))
)
:effect
(and
(Flag269prime-)
(not (Flag269-))
)

)
(:action Prim269Action-1
:parameters ()
:precondition
(and
(not (ABOVEOF10-ROBOT))
)
:effect
(and
(Flag269prime-)
(not (Flag269-))
)

)
(:action Prim270Action-0
:parameters ()
:precondition
(and
(not (BELOWOF2-ROBOT))
)
:effect
(and
(Flag270prime-)
(not (Flag270-))
)

)
(:action Prim270Action-1
:parameters ()
:precondition
(and
(not (ABOVEOF2-ROBOT))
)
:effect
(and
(Flag270prime-)
(not (Flag270-))
)

)
(:action Prim271Action-0
:parameters ()
:precondition
(and
(not (ABOVEOF12-ROBOT))
)
:effect
(and
(Flag271prime-)
(not (Flag271-))
)

)
(:action Prim271Action-1
:parameters ()
:precondition
(and
(not (BELOWOF3-ROBOT))
)
:effect
(and
(Flag271prime-)
(not (Flag271-))
)

)
(:action Prim272Action-0
:parameters ()
:precondition
(and
(not (BELOWOF3-ROBOT))
)
:effect
(and
(Flag272prime-)
(not (Flag272-))
)

)
(:action Prim272Action-1
:parameters ()
:precondition
(and
(not (ABOVEOF13-ROBOT))
)
:effect
(and
(Flag272prime-)
(not (Flag272-))
)

)
(:action Prim273Action-0
:parameters ()
:precondition
(and
(not (ABOVEOF11-ROBOT))
)
:effect
(and
(Flag273prime-)
(not (Flag273-))
)

)
(:action Prim273Action-1
:parameters ()
:precondition
(and
(not (BELOWOF3-ROBOT))
)
:effect
(and
(Flag273prime-)
(not (Flag273-))
)

)
(:action Flag275Action
:parameters ()
:precondition
(and
(BELOWOF4-ROBOT)
(ABOVEOF13-ROBOT)
)
:effect
(and
(Flag275-)
(Flag275prime-)
)

)
(:action Flag276Action
:parameters ()
:precondition
(and
(ABOVEOF3-ROBOT)
(BELOWOF4-ROBOT)
)
:effect
(and
(Flag276-)
(Flag276prime-)
)

)
(:action Flag277Action
:parameters ()
:precondition
(and
(ABOVEOF6-ROBOT)
(BELOWOF4-ROBOT)
)
:effect
(and
(Flag277-)
(Flag277prime-)
)

)
(:action Flag278Action
:parameters ()
:precondition
(and
(BELOWOF4-ROBOT)
(ABOVEOF4-ROBOT)
)
:effect
(and
(Flag278-)
(Flag278prime-)
)

)
(:action Flag279Action
:parameters ()
:precondition
(and
(ABOVEOF9-ROBOT)
(BELOWOF4-ROBOT)
)
:effect
(and
(Flag279-)
(Flag279prime-)
)

)
(:action Flag280Action
:parameters ()
:precondition
(and
(ABOVEOF8-ROBOT)
(BELOWOF4-ROBOT)
)
:effect
(and
(Flag280-)
(Flag280prime-)
)

)
(:action Flag281Action
:parameters ()
:precondition
(and
(BELOWOF4-ROBOT)
(ABOVEOF7-ROBOT)
)
:effect
(and
(Flag281-)
(Flag281prime-)
)

)
(:action Flag282Action
:parameters ()
:precondition
(and
(ABOVEOF12-ROBOT)
(BELOWOF4-ROBOT)
)
:effect
(and
(Flag282-)
(Flag282prime-)
)

)
(:action Flag283Action
:parameters ()
:precondition
(and
(BELOWOF4-ROBOT)
(ABOVEOF5-ROBOT)
)
:effect
(and
(Flag283-)
(Flag283prime-)
)

)
(:action Flag284Action
:parameters ()
:precondition
(and
(BELOWOF3-ROBOT)
(ABOVEOF3-ROBOT)
)
:effect
(and
(Flag284-)
(Flag284prime-)
)

)
(:action Flag285Action
:parameters ()
:precondition
(and
(ABOVEOF10-ROBOT)
(BELOWOF4-ROBOT)
)
:effect
(and
(Flag285-)
(Flag285prime-)
)

)
(:action Flag286Action
:parameters ()
:precondition
(and
(BELOWOF4-ROBOT)
(ABOVEOF15-ROBOT)
)
:effect
(and
(Flag286-)
(Flag286prime-)
)

)
(:action Flag287Action
:parameters ()
:precondition
(and
(BELOWOF3-ROBOT)
(ABOVEOF11-ROBOT)
)
:effect
(and
(Flag287-)
(Flag287prime-)
)

)
(:action Flag288Action
:parameters ()
:precondition
(and
(ABOVEOF11-ROBOT)
(BELOWOF4-ROBOT)
)
:effect
(and
(Flag288-)
(Flag288prime-)
)

)
(:action Flag289Action
:parameters ()
:precondition
(and
(BELOWOF4-ROBOT)
(ABOVEOF14-ROBOT)
)
:effect
(and
(Flag289-)
(Flag289prime-)
)

)
(:action Prim275Action-0
:parameters ()
:precondition
(and
(not (BELOWOF4-ROBOT))
)
:effect
(and
(Flag275prime-)
(not (Flag275-))
)

)
(:action Prim275Action-1
:parameters ()
:precondition
(and
(not (ABOVEOF13-ROBOT))
)
:effect
(and
(Flag275prime-)
(not (Flag275-))
)

)
(:action Prim276Action-0
:parameters ()
:precondition
(and
(not (ABOVEOF3-ROBOT))
)
:effect
(and
(Flag276prime-)
(not (Flag276-))
)

)
(:action Prim276Action-1
:parameters ()
:precondition
(and
(not (BELOWOF4-ROBOT))
)
:effect
(and
(Flag276prime-)
(not (Flag276-))
)

)
(:action Prim277Action-0
:parameters ()
:precondition
(and
(not (ABOVEOF6-ROBOT))
)
:effect
(and
(Flag277prime-)
(not (Flag277-))
)

)
(:action Prim277Action-1
:parameters ()
:precondition
(and
(not (BELOWOF4-ROBOT))
)
:effect
(and
(Flag277prime-)
(not (Flag277-))
)

)
(:action Prim278Action-0
:parameters ()
:precondition
(and
(not (BELOWOF4-ROBOT))
)
:effect
(and
(Flag278prime-)
(not (Flag278-))
)

)
(:action Prim278Action-1
:parameters ()
:precondition
(and
(not (ABOVEOF4-ROBOT))
)
:effect
(and
(Flag278prime-)
(not (Flag278-))
)

)
(:action Prim279Action-0
:parameters ()
:precondition
(and
(not (ABOVEOF9-ROBOT))
)
:effect
(and
(Flag279prime-)
(not (Flag279-))
)

)
(:action Prim279Action-1
:parameters ()
:precondition
(and
(not (BELOWOF4-ROBOT))
)
:effect
(and
(Flag279prime-)
(not (Flag279-))
)

)
(:action Prim280Action-0
:parameters ()
:precondition
(and
(not (ABOVEOF8-ROBOT))
)
:effect
(and
(Flag280prime-)
(not (Flag280-))
)

)
(:action Prim280Action-1
:parameters ()
:precondition
(and
(not (BELOWOF4-ROBOT))
)
:effect
(and
(Flag280prime-)
(not (Flag280-))
)

)
(:action Prim281Action-0
:parameters ()
:precondition
(and
(not (BELOWOF4-ROBOT))
)
:effect
(and
(Flag281prime-)
(not (Flag281-))
)

)
(:action Prim281Action-1
:parameters ()
:precondition
(and
(not (ABOVEOF7-ROBOT))
)
:effect
(and
(Flag281prime-)
(not (Flag281-))
)

)
(:action Prim282Action-0
:parameters ()
:precondition
(and
(not (ABOVEOF12-ROBOT))
)
:effect
(and
(Flag282prime-)
(not (Flag282-))
)

)
(:action Prim282Action-1
:parameters ()
:precondition
(and
(not (BELOWOF4-ROBOT))
)
:effect
(and
(Flag282prime-)
(not (Flag282-))
)

)
(:action Prim283Action-0
:parameters ()
:precondition
(and
(not (BELOWOF4-ROBOT))
)
:effect
(and
(Flag283prime-)
(not (Flag283-))
)

)
(:action Prim283Action-1
:parameters ()
:precondition
(and
(not (ABOVEOF5-ROBOT))
)
:effect
(and
(Flag283prime-)
(not (Flag283-))
)

)
(:action Prim284Action-0
:parameters ()
:precondition
(and
(not (BELOWOF3-ROBOT))
)
:effect
(and
(Flag284prime-)
(not (Flag284-))
)

)
(:action Prim284Action-1
:parameters ()
:precondition
(and
(not (ABOVEOF3-ROBOT))
)
:effect
(and
(Flag284prime-)
(not (Flag284-))
)

)
(:action Prim285Action-0
:parameters ()
:precondition
(and
(not (ABOVEOF10-ROBOT))
)
:effect
(and
(Flag285prime-)
(not (Flag285-))
)

)
(:action Prim285Action-1
:parameters ()
:precondition
(and
(not (BELOWOF4-ROBOT))
)
:effect
(and
(Flag285prime-)
(not (Flag285-))
)

)
(:action Prim286Action-0
:parameters ()
:precondition
(and
(not (BELOWOF4-ROBOT))
)
:effect
(and
(Flag286prime-)
(not (Flag286-))
)

)
(:action Prim286Action-1
:parameters ()
:precondition
(and
(not (ABOVEOF15-ROBOT))
)
:effect
(and
(Flag286prime-)
(not (Flag286-))
)

)
(:action Prim287Action-0
:parameters ()
:precondition
(and
(not (BELOWOF3-ROBOT))
)
:effect
(and
(Flag287prime-)
(not (Flag287-))
)

)
(:action Prim287Action-1
:parameters ()
:precondition
(and
(not (ABOVEOF11-ROBOT))
)
:effect
(and
(Flag287prime-)
(not (Flag287-))
)

)
(:action Prim288Action-0
:parameters ()
:precondition
(and
(not (ABOVEOF11-ROBOT))
)
:effect
(and
(Flag288prime-)
(not (Flag288-))
)

)
(:action Prim288Action-1
:parameters ()
:precondition
(and
(not (BELOWOF4-ROBOT))
)
:effect
(and
(Flag288prime-)
(not (Flag288-))
)

)
(:action Prim289Action-0
:parameters ()
:precondition
(and
(not (BELOWOF4-ROBOT))
)
:effect
(and
(Flag289prime-)
(not (Flag289-))
)

)
(:action Prim289Action-1
:parameters ()
:precondition
(and
(not (ABOVEOF14-ROBOT))
)
:effect
(and
(Flag289prime-)
(not (Flag289-))
)

)
(:action Flag291Action
:parameters ()
:precondition
(and
(ABOVEOF4-ROBOT)
(BELOWOF5-ROBOT)
)
:effect
(and
(Flag291-)
(Flag291prime-)
)

)
(:action Flag292Action
:parameters ()
:precondition
(and
(ABOVEOF15-ROBOT)
(BELOWOF5-ROBOT)
)
:effect
(and
(Flag292-)
(Flag292prime-)
)

)
(:action Flag293Action
:parameters ()
:precondition
(and
(ABOVEOF8-ROBOT)
(BELOWOF5-ROBOT)
)
:effect
(and
(Flag293-)
(Flag293prime-)
)

)
(:action Flag294Action
:parameters ()
:precondition
(and
(ABOVEOF13-ROBOT)
(BELOWOF5-ROBOT)
)
:effect
(and
(Flag294-)
(Flag294prime-)
)

)
(:action Flag295Action
:parameters ()
:precondition
(and
(ABOVEOF10-ROBOT)
(BELOWOF5-ROBOT)
)
:effect
(and
(Flag295-)
(Flag295prime-)
)

)
(:action Flag296Action
:parameters ()
:precondition
(and
(ABOVEOF11-ROBOT)
(BELOWOF5-ROBOT)
)
:effect
(and
(Flag296-)
(Flag296prime-)
)

)
(:action Flag297Action
:parameters ()
:precondition
(and
(ABOVEOF5-ROBOT)
(BELOWOF5-ROBOT)
)
:effect
(and
(Flag297-)
(Flag297prime-)
)

)
(:action Flag298Action
:parameters ()
:precondition
(and
(ABOVEOF7-ROBOT)
(BELOWOF5-ROBOT)
)
:effect
(and
(Flag298-)
(Flag298prime-)
)

)
(:action Flag299Action
:parameters ()
:precondition
(and
(ABOVEOF9-ROBOT)
(BELOWOF5-ROBOT)
)
:effect
(and
(Flag299-)
(Flag299prime-)
)

)
(:action Flag300Action
:parameters ()
:precondition
(and
(ABOVEOF12-ROBOT)
(BELOWOF5-ROBOT)
)
:effect
(and
(Flag300-)
(Flag300prime-)
)

)
(:action Flag301Action
:parameters ()
:precondition
(and
(ABOVEOF6-ROBOT)
(BELOWOF5-ROBOT)
)
:effect
(and
(Flag301-)
(Flag301prime-)
)

)
(:action Flag302Action
:parameters ()
:precondition
(and
(ABOVEOF14-ROBOT)
(BELOWOF5-ROBOT)
)
:effect
(and
(Flag302-)
(Flag302prime-)
)

)
(:action Prim291Action-0
:parameters ()
:precondition
(and
(not (ABOVEOF4-ROBOT))
)
:effect
(and
(Flag291prime-)
(not (Flag291-))
)

)
(:action Prim291Action-1
:parameters ()
:precondition
(and
(not (BELOWOF5-ROBOT))
)
:effect
(and
(Flag291prime-)
(not (Flag291-))
)

)
(:action Prim292Action-0
:parameters ()
:precondition
(and
(not (ABOVEOF15-ROBOT))
)
:effect
(and
(Flag292prime-)
(not (Flag292-))
)

)
(:action Prim292Action-1
:parameters ()
:precondition
(and
(not (BELOWOF5-ROBOT))
)
:effect
(and
(Flag292prime-)
(not (Flag292-))
)

)
(:action Prim293Action-0
:parameters ()
:precondition
(and
(not (ABOVEOF8-ROBOT))
)
:effect
(and
(Flag293prime-)
(not (Flag293-))
)

)
(:action Prim293Action-1
:parameters ()
:precondition
(and
(not (BELOWOF5-ROBOT))
)
:effect
(and
(Flag293prime-)
(not (Flag293-))
)

)
(:action Prim294Action-0
:parameters ()
:precondition
(and
(not (ABOVEOF13-ROBOT))
)
:effect
(and
(Flag294prime-)
(not (Flag294-))
)

)
(:action Prim294Action-1
:parameters ()
:precondition
(and
(not (BELOWOF5-ROBOT))
)
:effect
(and
(Flag294prime-)
(not (Flag294-))
)

)
(:action Prim295Action-0
:parameters ()
:precondition
(and
(not (ABOVEOF10-ROBOT))
)
:effect
(and
(Flag295prime-)
(not (Flag295-))
)

)
(:action Prim295Action-1
:parameters ()
:precondition
(and
(not (BELOWOF5-ROBOT))
)
:effect
(and
(Flag295prime-)
(not (Flag295-))
)

)
(:action Prim296Action-0
:parameters ()
:precondition
(and
(not (ABOVEOF11-ROBOT))
)
:effect
(and
(Flag296prime-)
(not (Flag296-))
)

)
(:action Prim296Action-1
:parameters ()
:precondition
(and
(not (BELOWOF5-ROBOT))
)
:effect
(and
(Flag296prime-)
(not (Flag296-))
)

)
(:action Prim297Action-0
:parameters ()
:precondition
(and
(not (ABOVEOF5-ROBOT))
)
:effect
(and
(Flag297prime-)
(not (Flag297-))
)

)
(:action Prim297Action-1
:parameters ()
:precondition
(and
(not (BELOWOF5-ROBOT))
)
:effect
(and
(Flag297prime-)
(not (Flag297-))
)

)
(:action Prim298Action-0
:parameters ()
:precondition
(and
(not (ABOVEOF7-ROBOT))
)
:effect
(and
(Flag298prime-)
(not (Flag298-))
)

)
(:action Prim298Action-1
:parameters ()
:precondition
(and
(not (BELOWOF5-ROBOT))
)
:effect
(and
(Flag298prime-)
(not (Flag298-))
)

)
(:action Prim299Action-0
:parameters ()
:precondition
(and
(not (ABOVEOF9-ROBOT))
)
:effect
(and
(Flag299prime-)
(not (Flag299-))
)

)
(:action Prim299Action-1
:parameters ()
:precondition
(and
(not (BELOWOF5-ROBOT))
)
:effect
(and
(Flag299prime-)
(not (Flag299-))
)

)
(:action Prim300Action-0
:parameters ()
:precondition
(and
(not (ABOVEOF12-ROBOT))
)
:effect
(and
(Flag300prime-)
(not (Flag300-))
)

)
(:action Prim300Action-1
:parameters ()
:precondition
(and
(not (BELOWOF5-ROBOT))
)
:effect
(and
(Flag300prime-)
(not (Flag300-))
)

)
(:action Prim301Action-0
:parameters ()
:precondition
(and
(not (ABOVEOF6-ROBOT))
)
:effect
(and
(Flag301prime-)
(not (Flag301-))
)

)
(:action Prim301Action-1
:parameters ()
:precondition
(and
(not (BELOWOF5-ROBOT))
)
:effect
(and
(Flag301prime-)
(not (Flag301-))
)

)
(:action Prim302Action-0
:parameters ()
:precondition
(and
(not (ABOVEOF14-ROBOT))
)
:effect
(and
(Flag302prime-)
(not (Flag302-))
)

)
(:action Prim302Action-1
:parameters ()
:precondition
(and
(not (BELOWOF5-ROBOT))
)
:effect
(and
(Flag302prime-)
(not (Flag302-))
)

)
(:action Flag304Action
:parameters ()
:precondition
(and
(BELOWOF6-ROBOT)
(ABOVEOF15-ROBOT)
)
:effect
(and
(Flag304-)
(Flag304prime-)
)

)
(:action Flag305Action
:parameters ()
:precondition
(and
(BELOWOF6-ROBOT)
(ABOVEOF5-ROBOT)
)
:effect
(and
(Flag305-)
(Flag305prime-)
)

)
(:action Flag306Action
:parameters ()
:precondition
(and
(ABOVEOF11-ROBOT)
(BELOWOF6-ROBOT)
)
:effect
(and
(Flag306-)
(Flag306prime-)
)

)
(:action Flag307Action
:parameters ()
:precondition
(and
(ABOVEOF12-ROBOT)
(BELOWOF6-ROBOT)
)
:effect
(and
(Flag307-)
(Flag307prime-)
)

)
(:action Flag308Action
:parameters ()
:precondition
(and
(BELOWOF5-ROBOT)
(ABOVEOF13-ROBOT)
)
:effect
(and
(Flag308-)
(Flag308prime-)
)

)
(:action Flag309Action
:parameters ()
:precondition
(and
(BELOWOF6-ROBOT)
(ABOVEOF13-ROBOT)
)
:effect
(and
(Flag309-)
(Flag309prime-)
)

)
(:action Flag310Action
:parameters ()
:precondition
(and
(BELOWOF6-ROBOT)
(ABOVEOF7-ROBOT)
)
:effect
(and
(Flag310-)
(Flag310prime-)
)

)
(:action Flag311Action
:parameters ()
:precondition
(and
(BELOWOF5-ROBOT)
(ABOVEOF5-ROBOT)
)
:effect
(and
(Flag311-)
(Flag311prime-)
)

)
(:action Flag312Action
:parameters ()
:precondition
(and
(BELOWOF6-ROBOT)
(ABOVEOF6-ROBOT)
)
:effect
(and
(Flag312-)
(Flag312prime-)
)

)
(:action Flag313Action
:parameters ()
:precondition
(and
(ABOVEOF8-ROBOT)
(BELOWOF6-ROBOT)
)
:effect
(and
(Flag313-)
(Flag313prime-)
)

)
(:action Flag314Action
:parameters ()
:precondition
(and
(BELOWOF6-ROBOT)
(ABOVEOF9-ROBOT)
)
:effect
(and
(Flag314-)
(Flag314prime-)
)

)
(:action Flag315Action
:parameters ()
:precondition
(and
(BELOWOF6-ROBOT)
(ABOVEOF14-ROBOT)
)
:effect
(and
(Flag315-)
(Flag315prime-)
)

)
(:action Flag316Action
:parameters ()
:precondition
(and
(BELOWOF6-ROBOT)
(ABOVEOF10-ROBOT)
)
:effect
(and
(Flag316-)
(Flag316prime-)
)

)
(:action Prim304Action-0
:parameters ()
:precondition
(and
(not (BELOWOF6-ROBOT))
)
:effect
(and
(Flag304prime-)
(not (Flag304-))
)

)
(:action Prim304Action-1
:parameters ()
:precondition
(and
(not (ABOVEOF15-ROBOT))
)
:effect
(and
(Flag304prime-)
(not (Flag304-))
)

)
(:action Prim305Action-0
:parameters ()
:precondition
(and
(not (BELOWOF6-ROBOT))
)
:effect
(and
(Flag305prime-)
(not (Flag305-))
)

)
(:action Prim305Action-1
:parameters ()
:precondition
(and
(not (ABOVEOF5-ROBOT))
)
:effect
(and
(Flag305prime-)
(not (Flag305-))
)

)
(:action Prim306Action-0
:parameters ()
:precondition
(and
(not (ABOVEOF11-ROBOT))
)
:effect
(and
(Flag306prime-)
(not (Flag306-))
)

)
(:action Prim306Action-1
:parameters ()
:precondition
(and
(not (BELOWOF6-ROBOT))
)
:effect
(and
(Flag306prime-)
(not (Flag306-))
)

)
(:action Prim307Action-0
:parameters ()
:precondition
(and
(not (ABOVEOF12-ROBOT))
)
:effect
(and
(Flag307prime-)
(not (Flag307-))
)

)
(:action Prim307Action-1
:parameters ()
:precondition
(and
(not (BELOWOF6-ROBOT))
)
:effect
(and
(Flag307prime-)
(not (Flag307-))
)

)
(:action Prim308Action-0
:parameters ()
:precondition
(and
(not (BELOWOF5-ROBOT))
)
:effect
(and
(Flag308prime-)
(not (Flag308-))
)

)
(:action Prim308Action-1
:parameters ()
:precondition
(and
(not (ABOVEOF13-ROBOT))
)
:effect
(and
(Flag308prime-)
(not (Flag308-))
)

)
(:action Prim309Action-0
:parameters ()
:precondition
(and
(not (BELOWOF6-ROBOT))
)
:effect
(and
(Flag309prime-)
(not (Flag309-))
)

)
(:action Prim309Action-1
:parameters ()
:precondition
(and
(not (ABOVEOF13-ROBOT))
)
:effect
(and
(Flag309prime-)
(not (Flag309-))
)

)
(:action Prim310Action-0
:parameters ()
:precondition
(and
(not (BELOWOF6-ROBOT))
)
:effect
(and
(Flag310prime-)
(not (Flag310-))
)

)
(:action Prim310Action-1
:parameters ()
:precondition
(and
(not (ABOVEOF7-ROBOT))
)
:effect
(and
(Flag310prime-)
(not (Flag310-))
)

)
(:action Prim311Action-0
:parameters ()
:precondition
(and
(not (BELOWOF5-ROBOT))
)
:effect
(and
(Flag311prime-)
(not (Flag311-))
)

)
(:action Prim311Action-1
:parameters ()
:precondition
(and
(not (ABOVEOF5-ROBOT))
)
:effect
(and
(Flag311prime-)
(not (Flag311-))
)

)
(:action Prim312Action-0
:parameters ()
:precondition
(and
(not (BELOWOF6-ROBOT))
)
:effect
(and
(Flag312prime-)
(not (Flag312-))
)

)
(:action Prim312Action-1
:parameters ()
:precondition
(and
(not (ABOVEOF6-ROBOT))
)
:effect
(and
(Flag312prime-)
(not (Flag312-))
)

)
(:action Prim313Action-0
:parameters ()
:precondition
(and
(not (ABOVEOF8-ROBOT))
)
:effect
(and
(Flag313prime-)
(not (Flag313-))
)

)
(:action Prim313Action-1
:parameters ()
:precondition
(and
(not (BELOWOF6-ROBOT))
)
:effect
(and
(Flag313prime-)
(not (Flag313-))
)

)
(:action Prim314Action-0
:parameters ()
:precondition
(and
(not (BELOWOF6-ROBOT))
)
:effect
(and
(Flag314prime-)
(not (Flag314-))
)

)
(:action Prim314Action-1
:parameters ()
:precondition
(and
(not (ABOVEOF9-ROBOT))
)
:effect
(and
(Flag314prime-)
(not (Flag314-))
)

)
(:action Prim315Action-0
:parameters ()
:precondition
(and
(not (BELOWOF6-ROBOT))
)
:effect
(and
(Flag315prime-)
(not (Flag315-))
)

)
(:action Prim315Action-1
:parameters ()
:precondition
(and
(not (ABOVEOF14-ROBOT))
)
:effect
(and
(Flag315prime-)
(not (Flag315-))
)

)
(:action Prim316Action-0
:parameters ()
:precondition
(and
(not (BELOWOF6-ROBOT))
)
:effect
(and
(Flag316prime-)
(not (Flag316-))
)

)
(:action Prim316Action-1
:parameters ()
:precondition
(and
(not (ABOVEOF10-ROBOT))
)
:effect
(and
(Flag316prime-)
(not (Flag316-))
)

)
(:action Flag318Action
:parameters ()
:precondition
(and
(ABOVEOF6-ROBOT)
(BELOWOF7-ROBOT)
)
:effect
(and
(Flag318-)
(Flag318prime-)
)

)
(:action Flag319Action
:parameters ()
:precondition
(and
(ABOVEOF9-ROBOT)
(BELOWOF7-ROBOT)
)
:effect
(and
(Flag319-)
(Flag319prime-)
)

)
(:action Flag320Action
:parameters ()
:precondition
(and
(ABOVEOF6-ROBOT)
(BELOWOF6-ROBOT)
)
:effect
(and
(Flag320-)
(Flag320prime-)
)

)
(:action Flag321Action
:parameters ()
:precondition
(and
(ABOVEOF10-ROBOT)
(BELOWOF7-ROBOT)
)
:effect
(and
(Flag321-)
(Flag321prime-)
)

)
(:action Flag322Action
:parameters ()
:precondition
(and
(ABOVEOF10-ROBOT)
(BELOWOF6-ROBOT)
)
:effect
(and
(Flag322-)
(Flag322prime-)
)

)
(:action Flag323Action
:parameters ()
:precondition
(and
(BELOWOF7-ROBOT)
(ABOVEOF15-ROBOT)
)
:effect
(and
(Flag323-)
(Flag323prime-)
)

)
(:action Flag324Action
:parameters ()
:precondition
(and
(ABOVEOF12-ROBOT)
(BELOWOF7-ROBOT)
)
:effect
(and
(Flag324-)
(Flag324prime-)
)

)
(:action Flag325Action
:parameters ()
:precondition
(and
(BELOWOF7-ROBOT)
(ABOVEOF14-ROBOT)
)
:effect
(and
(Flag325-)
(Flag325prime-)
)

)
(:action Flag326Action
:parameters ()
:precondition
(and
(BELOWOF7-ROBOT)
(ABOVEOF13-ROBOT)
)
:effect
(and
(Flag326-)
(Flag326prime-)
)

)
(:action Flag327Action
:parameters ()
:precondition
(and
(ABOVEOF7-ROBOT)
(BELOWOF7-ROBOT)
)
:effect
(and
(Flag327-)
(Flag327prime-)
)

)
(:action Flag328Action
:parameters ()
:precondition
(and
(ABOVEOF8-ROBOT)
(BELOWOF7-ROBOT)
)
:effect
(and
(Flag328-)
(Flag328prime-)
)

)
(:action Flag329Action
:parameters ()
:precondition
(and
(ABOVEOF11-ROBOT)
(BELOWOF7-ROBOT)
)
:effect
(and
(Flag329-)
(Flag329prime-)
)

)
(:action Prim318Action-0
:parameters ()
:precondition
(and
(not (ABOVEOF6-ROBOT))
)
:effect
(and
(Flag318prime-)
(not (Flag318-))
)

)
(:action Prim318Action-1
:parameters ()
:precondition
(and
(not (BELOWOF7-ROBOT))
)
:effect
(and
(Flag318prime-)
(not (Flag318-))
)

)
(:action Prim319Action-0
:parameters ()
:precondition
(and
(not (ABOVEOF9-ROBOT))
)
:effect
(and
(Flag319prime-)
(not (Flag319-))
)

)
(:action Prim319Action-1
:parameters ()
:precondition
(and
(not (BELOWOF7-ROBOT))
)
:effect
(and
(Flag319prime-)
(not (Flag319-))
)

)
(:action Prim320Action-0
:parameters ()
:precondition
(and
(not (ABOVEOF6-ROBOT))
)
:effect
(and
(Flag320prime-)
(not (Flag320-))
)

)
(:action Prim320Action-1
:parameters ()
:precondition
(and
(not (BELOWOF6-ROBOT))
)
:effect
(and
(Flag320prime-)
(not (Flag320-))
)

)
(:action Prim321Action-0
:parameters ()
:precondition
(and
(not (ABOVEOF10-ROBOT))
)
:effect
(and
(Flag321prime-)
(not (Flag321-))
)

)
(:action Prim321Action-1
:parameters ()
:precondition
(and
(not (BELOWOF7-ROBOT))
)
:effect
(and
(Flag321prime-)
(not (Flag321-))
)

)
(:action Prim322Action-0
:parameters ()
:precondition
(and
(not (ABOVEOF10-ROBOT))
)
:effect
(and
(Flag322prime-)
(not (Flag322-))
)

)
(:action Prim322Action-1
:parameters ()
:precondition
(and
(not (BELOWOF6-ROBOT))
)
:effect
(and
(Flag322prime-)
(not (Flag322-))
)

)
(:action Prim323Action-0
:parameters ()
:precondition
(and
(not (BELOWOF7-ROBOT))
)
:effect
(and
(Flag323prime-)
(not (Flag323-))
)

)
(:action Prim323Action-1
:parameters ()
:precondition
(and
(not (ABOVEOF15-ROBOT))
)
:effect
(and
(Flag323prime-)
(not (Flag323-))
)

)
(:action Prim324Action-0
:parameters ()
:precondition
(and
(not (ABOVEOF12-ROBOT))
)
:effect
(and
(Flag324prime-)
(not (Flag324-))
)

)
(:action Prim324Action-1
:parameters ()
:precondition
(and
(not (BELOWOF7-ROBOT))
)
:effect
(and
(Flag324prime-)
(not (Flag324-))
)

)
(:action Prim325Action-0
:parameters ()
:precondition
(and
(not (BELOWOF7-ROBOT))
)
:effect
(and
(Flag325prime-)
(not (Flag325-))
)

)
(:action Prim325Action-1
:parameters ()
:precondition
(and
(not (ABOVEOF14-ROBOT))
)
:effect
(and
(Flag325prime-)
(not (Flag325-))
)

)
(:action Prim326Action-0
:parameters ()
:precondition
(and
(not (BELOWOF7-ROBOT))
)
:effect
(and
(Flag326prime-)
(not (Flag326-))
)

)
(:action Prim326Action-1
:parameters ()
:precondition
(and
(not (ABOVEOF13-ROBOT))
)
:effect
(and
(Flag326prime-)
(not (Flag326-))
)

)
(:action Prim327Action-0
:parameters ()
:precondition
(and
(not (ABOVEOF7-ROBOT))
)
:effect
(and
(Flag327prime-)
(not (Flag327-))
)

)
(:action Prim327Action-1
:parameters ()
:precondition
(and
(not (BELOWOF7-ROBOT))
)
:effect
(and
(Flag327prime-)
(not (Flag327-))
)

)
(:action Prim328Action-0
:parameters ()
:precondition
(and
(not (ABOVEOF8-ROBOT))
)
:effect
(and
(Flag328prime-)
(not (Flag328-))
)

)
(:action Prim328Action-1
:parameters ()
:precondition
(and
(not (BELOWOF7-ROBOT))
)
:effect
(and
(Flag328prime-)
(not (Flag328-))
)

)
(:action Prim329Action-0
:parameters ()
:precondition
(and
(not (ABOVEOF11-ROBOT))
)
:effect
(and
(Flag329prime-)
(not (Flag329-))
)

)
(:action Prim329Action-1
:parameters ()
:precondition
(and
(not (BELOWOF7-ROBOT))
)
:effect
(and
(Flag329prime-)
(not (Flag329-))
)

)
(:action Flag331Action
:parameters ()
:precondition
(and
(BELOWOF8-ROBOT)
(ABOVEOF13-ROBOT)
)
:effect
(and
(Flag331-)
(Flag331prime-)
)

)
(:action Flag332Action
:parameters ()
:precondition
(and
(BELOWOF8-ROBOT)
(ABOVEOF10-ROBOT)
)
:effect
(and
(Flag332-)
(Flag332prime-)
)

)
(:action Flag333Action
:parameters ()
:precondition
(and
(ABOVEOF12-ROBOT)
(BELOWOF8-ROBOT)
)
:effect
(and
(Flag333-)
(Flag333prime-)
)

)
(:action Flag334Action
:parameters ()
:precondition
(and
(ABOVEOF8-ROBOT)
(BELOWOF8-ROBOT)
)
:effect
(and
(Flag334-)
(Flag334prime-)
)

)
(:action Flag335Action
:parameters ()
:precondition
(and
(BELOWOF8-ROBOT)
(ABOVEOF9-ROBOT)
)
:effect
(and
(Flag335-)
(Flag335prime-)
)

)
(:action Flag336Action
:parameters ()
:precondition
(and
(BELOWOF7-ROBOT)
(ABOVEOF7-ROBOT)
)
:effect
(and
(Flag336-)
(Flag336prime-)
)

)
(:action Flag337Action
:parameters ()
:precondition
(and
(BELOWOF8-ROBOT)
(ABOVEOF7-ROBOT)
)
:effect
(and
(Flag337-)
(Flag337prime-)
)

)
(:action Flag338Action
:parameters ()
:precondition
(and
(BELOWOF8-ROBOT)
(ABOVEOF15-ROBOT)
)
:effect
(and
(Flag338-)
(Flag338prime-)
)

)
(:action Flag339Action
:parameters ()
:precondition
(and
(BELOWOF8-ROBOT)
(ABOVEOF11-ROBOT)
)
:effect
(and
(Flag339-)
(Flag339prime-)
)

)
(:action Flag340Action
:parameters ()
:precondition
(and
(BELOWOF8-ROBOT)
(ABOVEOF14-ROBOT)
)
:effect
(and
(Flag340-)
(Flag340prime-)
)

)
(:action Prim331Action-0
:parameters ()
:precondition
(and
(not (BELOWOF8-ROBOT))
)
:effect
(and
(Flag331prime-)
(not (Flag331-))
)

)
(:action Prim331Action-1
:parameters ()
:precondition
(and
(not (ABOVEOF13-ROBOT))
)
:effect
(and
(Flag331prime-)
(not (Flag331-))
)

)
(:action Prim332Action-0
:parameters ()
:precondition
(and
(not (BELOWOF8-ROBOT))
)
:effect
(and
(Flag332prime-)
(not (Flag332-))
)

)
(:action Prim332Action-1
:parameters ()
:precondition
(and
(not (ABOVEOF10-ROBOT))
)
:effect
(and
(Flag332prime-)
(not (Flag332-))
)

)
(:action Prim333Action-0
:parameters ()
:precondition
(and
(not (ABOVEOF12-ROBOT))
)
:effect
(and
(Flag333prime-)
(not (Flag333-))
)

)
(:action Prim333Action-1
:parameters ()
:precondition
(and
(not (BELOWOF8-ROBOT))
)
:effect
(and
(Flag333prime-)
(not (Flag333-))
)

)
(:action Prim334Action-0
:parameters ()
:precondition
(and
(not (ABOVEOF8-ROBOT))
)
:effect
(and
(Flag334prime-)
(not (Flag334-))
)

)
(:action Prim334Action-1
:parameters ()
:precondition
(and
(not (BELOWOF8-ROBOT))
)
:effect
(and
(Flag334prime-)
(not (Flag334-))
)

)
(:action Prim335Action-0
:parameters ()
:precondition
(and
(not (BELOWOF8-ROBOT))
)
:effect
(and
(Flag335prime-)
(not (Flag335-))
)

)
(:action Prim335Action-1
:parameters ()
:precondition
(and
(not (ABOVEOF9-ROBOT))
)
:effect
(and
(Flag335prime-)
(not (Flag335-))
)

)
(:action Prim336Action-0
:parameters ()
:precondition
(and
(not (BELOWOF7-ROBOT))
)
:effect
(and
(Flag336prime-)
(not (Flag336-))
)

)
(:action Prim336Action-1
:parameters ()
:precondition
(and
(not (ABOVEOF7-ROBOT))
)
:effect
(and
(Flag336prime-)
(not (Flag336-))
)

)
(:action Prim337Action-0
:parameters ()
:precondition
(and
(not (BELOWOF8-ROBOT))
)
:effect
(and
(Flag337prime-)
(not (Flag337-))
)

)
(:action Prim337Action-1
:parameters ()
:precondition
(and
(not (ABOVEOF7-ROBOT))
)
:effect
(and
(Flag337prime-)
(not (Flag337-))
)

)
(:action Prim338Action-0
:parameters ()
:precondition
(and
(not (BELOWOF8-ROBOT))
)
:effect
(and
(Flag338prime-)
(not (Flag338-))
)

)
(:action Prim338Action-1
:parameters ()
:precondition
(and
(not (ABOVEOF15-ROBOT))
)
:effect
(and
(Flag338prime-)
(not (Flag338-))
)

)
(:action Prim339Action-0
:parameters ()
:precondition
(and
(not (BELOWOF8-ROBOT))
)
:effect
(and
(Flag339prime-)
(not (Flag339-))
)

)
(:action Prim339Action-1
:parameters ()
:precondition
(and
(not (ABOVEOF11-ROBOT))
)
:effect
(and
(Flag339prime-)
(not (Flag339-))
)

)
(:action Prim340Action-0
:parameters ()
:precondition
(and
(not (BELOWOF8-ROBOT))
)
:effect
(and
(Flag340prime-)
(not (Flag340-))
)

)
(:action Prim340Action-1
:parameters ()
:precondition
(and
(not (ABOVEOF14-ROBOT))
)
:effect
(and
(Flag340prime-)
(not (Flag340-))
)

)
(:action Flag342Action
:parameters ()
:precondition
(and
(BELOWOF8-ROBOT)
(ABOVEOF12-ROBOT)
)
:effect
(and
(Flag342-)
(Flag342prime-)
)

)
(:action Flag343Action
:parameters ()
:precondition
(and
(ABOVEOF10-ROBOT)
(BELOWOF9-ROBOT)
)
:effect
(and
(Flag343-)
(Flag343prime-)
)

)
(:action Flag344Action
:parameters ()
:precondition
(and
(BELOWOF8-ROBOT)
(ABOVEOF8-ROBOT)
)
:effect
(and
(Flag344-)
(Flag344prime-)
)

)
(:action Flag345Action
:parameters ()
:precondition
(and
(BELOWOF9-ROBOT)
(ABOVEOF14-ROBOT)
)
:effect
(and
(Flag345-)
(Flag345prime-)
)

)
(:action Flag346Action
:parameters ()
:precondition
(and
(BELOWOF9-ROBOT)
(ABOVEOF13-ROBOT)
)
:effect
(and
(Flag346-)
(Flag346prime-)
)

)
(:action Flag347Action
:parameters ()
:precondition
(and
(ABOVEOF8-ROBOT)
(BELOWOF9-ROBOT)
)
:effect
(and
(Flag347-)
(Flag347prime-)
)

)
(:action Flag348Action
:parameters ()
:precondition
(and
(ABOVEOF12-ROBOT)
(BELOWOF9-ROBOT)
)
:effect
(and
(Flag348-)
(Flag348prime-)
)

)
(:action Flag349Action
:parameters ()
:precondition
(and
(ABOVEOF11-ROBOT)
(BELOWOF9-ROBOT)
)
:effect
(and
(Flag349-)
(Flag349prime-)
)

)
(:action Flag350Action
:parameters ()
:precondition
(and
(BELOWOF9-ROBOT)
(ABOVEOF9-ROBOT)
)
:effect
(and
(Flag350-)
(Flag350prime-)
)

)
(:action Flag351Action
:parameters ()
:precondition
(and
(BELOWOF9-ROBOT)
(ABOVEOF15-ROBOT)
)
:effect
(and
(Flag351-)
(Flag351prime-)
)

)
(:action Prim342Action-0
:parameters ()
:precondition
(and
(not (BELOWOF8-ROBOT))
)
:effect
(and
(Flag342prime-)
(not (Flag342-))
)

)
(:action Prim342Action-1
:parameters ()
:precondition
(and
(not (ABOVEOF12-ROBOT))
)
:effect
(and
(Flag342prime-)
(not (Flag342-))
)

)
(:action Prim343Action-0
:parameters ()
:precondition
(and
(not (ABOVEOF10-ROBOT))
)
:effect
(and
(Flag343prime-)
(not (Flag343-))
)

)
(:action Prim343Action-1
:parameters ()
:precondition
(and
(not (BELOWOF9-ROBOT))
)
:effect
(and
(Flag343prime-)
(not (Flag343-))
)

)
(:action Prim344Action-0
:parameters ()
:precondition
(and
(not (BELOWOF8-ROBOT))
)
:effect
(and
(Flag344prime-)
(not (Flag344-))
)

)
(:action Prim344Action-1
:parameters ()
:precondition
(and
(not (ABOVEOF8-ROBOT))
)
:effect
(and
(Flag344prime-)
(not (Flag344-))
)

)
(:action Prim345Action-0
:parameters ()
:precondition
(and
(not (BELOWOF9-ROBOT))
)
:effect
(and
(Flag345prime-)
(not (Flag345-))
)

)
(:action Prim345Action-1
:parameters ()
:precondition
(and
(not (ABOVEOF14-ROBOT))
)
:effect
(and
(Flag345prime-)
(not (Flag345-))
)

)
(:action Prim346Action-0
:parameters ()
:precondition
(and
(not (BELOWOF9-ROBOT))
)
:effect
(and
(Flag346prime-)
(not (Flag346-))
)

)
(:action Prim346Action-1
:parameters ()
:precondition
(and
(not (ABOVEOF13-ROBOT))
)
:effect
(and
(Flag346prime-)
(not (Flag346-))
)

)
(:action Prim347Action-0
:parameters ()
:precondition
(and
(not (ABOVEOF8-ROBOT))
)
:effect
(and
(Flag347prime-)
(not (Flag347-))
)

)
(:action Prim347Action-1
:parameters ()
:precondition
(and
(not (BELOWOF9-ROBOT))
)
:effect
(and
(Flag347prime-)
(not (Flag347-))
)

)
(:action Prim348Action-0
:parameters ()
:precondition
(and
(not (ABOVEOF12-ROBOT))
)
:effect
(and
(Flag348prime-)
(not (Flag348-))
)

)
(:action Prim348Action-1
:parameters ()
:precondition
(and
(not (BELOWOF9-ROBOT))
)
:effect
(and
(Flag348prime-)
(not (Flag348-))
)

)
(:action Prim349Action-0
:parameters ()
:precondition
(and
(not (ABOVEOF11-ROBOT))
)
:effect
(and
(Flag349prime-)
(not (Flag349-))
)

)
(:action Prim349Action-1
:parameters ()
:precondition
(and
(not (BELOWOF9-ROBOT))
)
:effect
(and
(Flag349prime-)
(not (Flag349-))
)

)
(:action Prim350Action-0
:parameters ()
:precondition
(and
(not (BELOWOF9-ROBOT))
)
:effect
(and
(Flag350prime-)
(not (Flag350-))
)

)
(:action Prim350Action-1
:parameters ()
:precondition
(and
(not (ABOVEOF9-ROBOT))
)
:effect
(and
(Flag350prime-)
(not (Flag350-))
)

)
(:action Prim351Action-0
:parameters ()
:precondition
(and
(not (BELOWOF9-ROBOT))
)
:effect
(and
(Flag351prime-)
(not (Flag351-))
)

)
(:action Prim351Action-1
:parameters ()
:precondition
(and
(not (ABOVEOF15-ROBOT))
)
:effect
(and
(Flag351prime-)
(not (Flag351-))
)

)
(:action Flag353Action
:parameters ()
:precondition
(and
(BELOWOF10-ROBOT)
(ABOVEOF15-ROBOT)
)
:effect
(and
(Flag353-)
(Flag353prime-)
)

)
(:action Flag354Action
:parameters ()
:precondition
(and
(BELOWOF10-ROBOT)
(ABOVEOF9-ROBOT)
)
:effect
(and
(Flag354-)
(Flag354prime-)
)

)
(:action Flag355Action
:parameters ()
:precondition
(and
(BELOWOF10-ROBOT)
(ABOVEOF14-ROBOT)
)
:effect
(and
(Flag355-)
(Flag355prime-)
)

)
(:action Flag356Action
:parameters ()
:precondition
(and
(BELOWOF10-ROBOT)
(ABOVEOF10-ROBOT)
)
:effect
(and
(Flag356-)
(Flag356prime-)
)

)
(:action Flag357Action
:parameters ()
:precondition
(and
(ABOVEOF12-ROBOT)
(BELOWOF10-ROBOT)
)
:effect
(and
(Flag357-)
(Flag357prime-)
)

)
(:action Flag358Action
:parameters ()
:precondition
(and
(ABOVEOF9-ROBOT)
(BELOWOF9-ROBOT)
)
:effect
(and
(Flag358-)
(Flag358prime-)
)

)
(:action Flag359Action
:parameters ()
:precondition
(and
(ABOVEOF11-ROBOT)
(BELOWOF10-ROBOT)
)
:effect
(and
(Flag359-)
(Flag359prime-)
)

)
(:action Flag360Action
:parameters ()
:precondition
(and
(BELOWOF10-ROBOT)
(ABOVEOF13-ROBOT)
)
:effect
(and
(Flag360-)
(Flag360prime-)
)

)
(:action Prim353Action-0
:parameters ()
:precondition
(and
(not (BELOWOF10-ROBOT))
)
:effect
(and
(Flag353prime-)
(not (Flag353-))
)

)
(:action Prim353Action-1
:parameters ()
:precondition
(and
(not (ABOVEOF15-ROBOT))
)
:effect
(and
(Flag353prime-)
(not (Flag353-))
)

)
(:action Prim354Action-0
:parameters ()
:precondition
(and
(not (BELOWOF10-ROBOT))
)
:effect
(and
(Flag354prime-)
(not (Flag354-))
)

)
(:action Prim354Action-1
:parameters ()
:precondition
(and
(not (ABOVEOF9-ROBOT))
)
:effect
(and
(Flag354prime-)
(not (Flag354-))
)

)
(:action Prim355Action-0
:parameters ()
:precondition
(and
(not (BELOWOF10-ROBOT))
)
:effect
(and
(Flag355prime-)
(not (Flag355-))
)

)
(:action Prim355Action-1
:parameters ()
:precondition
(and
(not (ABOVEOF14-ROBOT))
)
:effect
(and
(Flag355prime-)
(not (Flag355-))
)

)
(:action Prim356Action-0
:parameters ()
:precondition
(and
(not (BELOWOF10-ROBOT))
)
:effect
(and
(Flag356prime-)
(not (Flag356-))
)

)
(:action Prim356Action-1
:parameters ()
:precondition
(and
(not (ABOVEOF10-ROBOT))
)
:effect
(and
(Flag356prime-)
(not (Flag356-))
)

)
(:action Prim357Action-0
:parameters ()
:precondition
(and
(not (ABOVEOF12-ROBOT))
)
:effect
(and
(Flag357prime-)
(not (Flag357-))
)

)
(:action Prim357Action-1
:parameters ()
:precondition
(and
(not (BELOWOF10-ROBOT))
)
:effect
(and
(Flag357prime-)
(not (Flag357-))
)

)
(:action Prim358Action-0
:parameters ()
:precondition
(and
(not (ABOVEOF9-ROBOT))
)
:effect
(and
(Flag358prime-)
(not (Flag358-))
)

)
(:action Prim358Action-1
:parameters ()
:precondition
(and
(not (BELOWOF9-ROBOT))
)
:effect
(and
(Flag358prime-)
(not (Flag358-))
)

)
(:action Prim359Action-0
:parameters ()
:precondition
(and
(not (ABOVEOF11-ROBOT))
)
:effect
(and
(Flag359prime-)
(not (Flag359-))
)

)
(:action Prim359Action-1
:parameters ()
:precondition
(and
(not (BELOWOF10-ROBOT))
)
:effect
(and
(Flag359prime-)
(not (Flag359-))
)

)
(:action Prim360Action-0
:parameters ()
:precondition
(and
(not (BELOWOF10-ROBOT))
)
:effect
(and
(Flag360prime-)
(not (Flag360-))
)

)
(:action Prim360Action-1
:parameters ()
:precondition
(and
(not (ABOVEOF13-ROBOT))
)
:effect
(and
(Flag360prime-)
(not (Flag360-))
)

)
(:action Flag362Action
:parameters ()
:precondition
(and
(BELOWOF11-ROBOT)
(ABOVEOF10-ROBOT)
)
:effect
(and
(Flag362-)
(Flag362prime-)
)

)
(:action Flag363Action
:parameters ()
:precondition
(and
(ABOVEOF11-ROBOT)
(BELOWOF11-ROBOT)
)
:effect
(and
(Flag363-)
(Flag363prime-)
)

)
(:action Flag364Action
:parameters ()
:precondition
(and
(BELOWOF11-ROBOT)
(ABOVEOF13-ROBOT)
)
:effect
(and
(Flag364-)
(Flag364prime-)
)

)
(:action Flag365Action
:parameters ()
:precondition
(and
(BELOWOF11-ROBOT)
(ABOVEOF15-ROBOT)
)
:effect
(and
(Flag365-)
(Flag365prime-)
)

)
(:action Flag366Action
:parameters ()
:precondition
(and
(BELOWOF11-ROBOT)
(ABOVEOF14-ROBOT)
)
:effect
(and
(Flag366-)
(Flag366prime-)
)

)
(:action Flag367Action
:parameters ()
:precondition
(and
(ABOVEOF12-ROBOT)
(BELOWOF11-ROBOT)
)
:effect
(and
(Flag367-)
(Flag367prime-)
)

)
(:action Prim362Action-0
:parameters ()
:precondition
(and
(not (BELOWOF11-ROBOT))
)
:effect
(and
(Flag362prime-)
(not (Flag362-))
)

)
(:action Prim362Action-1
:parameters ()
:precondition
(and
(not (ABOVEOF10-ROBOT))
)
:effect
(and
(Flag362prime-)
(not (Flag362-))
)

)
(:action Prim363Action-0
:parameters ()
:precondition
(and
(not (ABOVEOF11-ROBOT))
)
:effect
(and
(Flag363prime-)
(not (Flag363-))
)

)
(:action Prim363Action-1
:parameters ()
:precondition
(and
(not (BELOWOF11-ROBOT))
)
:effect
(and
(Flag363prime-)
(not (Flag363-))
)

)
(:action Prim364Action-0
:parameters ()
:precondition
(and
(not (BELOWOF11-ROBOT))
)
:effect
(and
(Flag364prime-)
(not (Flag364-))
)

)
(:action Prim364Action-1
:parameters ()
:precondition
(and
(not (ABOVEOF13-ROBOT))
)
:effect
(and
(Flag364prime-)
(not (Flag364-))
)

)
(:action Prim365Action-0
:parameters ()
:precondition
(and
(not (BELOWOF11-ROBOT))
)
:effect
(and
(Flag365prime-)
(not (Flag365-))
)

)
(:action Prim365Action-1
:parameters ()
:precondition
(and
(not (ABOVEOF15-ROBOT))
)
:effect
(and
(Flag365prime-)
(not (Flag365-))
)

)
(:action Prim366Action-0
:parameters ()
:precondition
(and
(not (BELOWOF11-ROBOT))
)
:effect
(and
(Flag366prime-)
(not (Flag366-))
)

)
(:action Prim366Action-1
:parameters ()
:precondition
(and
(not (ABOVEOF14-ROBOT))
)
:effect
(and
(Flag366prime-)
(not (Flag366-))
)

)
(:action Prim367Action-0
:parameters ()
:precondition
(and
(not (ABOVEOF12-ROBOT))
)
:effect
(and
(Flag367prime-)
(not (Flag367-))
)

)
(:action Prim367Action-1
:parameters ()
:precondition
(and
(not (BELOWOF11-ROBOT))
)
:effect
(and
(Flag367prime-)
(not (Flag367-))
)

)
(:action Flag369Action
:parameters ()
:precondition
(and
(BELOWOF12-ROBOT)
(ABOVEOF15-ROBOT)
)
:effect
(and
(Flag369-)
(Flag369prime-)
)

)
(:action Flag370Action
:parameters ()
:precondition
(and
(ABOVEOF12-ROBOT)
(BELOWOF12-ROBOT)
)
:effect
(and
(Flag370-)
(Flag370prime-)
)

)
(:action Flag371Action
:parameters ()
:precondition
(and
(BELOWOF12-ROBOT)
(ABOVEOF13-ROBOT)
)
:effect
(and
(Flag371-)
(Flag371prime-)
)

)
(:action Flag372Action
:parameters ()
:precondition
(and
(BELOWOF12-ROBOT)
(ABOVEOF11-ROBOT)
)
:effect
(and
(Flag372-)
(Flag372prime-)
)

)
(:action Flag373Action
:parameters ()
:precondition
(and
(BELOWOF12-ROBOT)
(ABOVEOF14-ROBOT)
)
:effect
(and
(Flag373-)
(Flag373prime-)
)

)
(:action Prim369Action-0
:parameters ()
:precondition
(and
(not (BELOWOF12-ROBOT))
)
:effect
(and
(Flag369prime-)
(not (Flag369-))
)

)
(:action Prim369Action-1
:parameters ()
:precondition
(and
(not (ABOVEOF15-ROBOT))
)
:effect
(and
(Flag369prime-)
(not (Flag369-))
)

)
(:action Prim370Action-0
:parameters ()
:precondition
(and
(not (ABOVEOF12-ROBOT))
)
:effect
(and
(Flag370prime-)
(not (Flag370-))
)

)
(:action Prim370Action-1
:parameters ()
:precondition
(and
(not (BELOWOF12-ROBOT))
)
:effect
(and
(Flag370prime-)
(not (Flag370-))
)

)
(:action Prim371Action-0
:parameters ()
:precondition
(and
(not (BELOWOF12-ROBOT))
)
:effect
(and
(Flag371prime-)
(not (Flag371-))
)

)
(:action Prim371Action-1
:parameters ()
:precondition
(and
(not (ABOVEOF13-ROBOT))
)
:effect
(and
(Flag371prime-)
(not (Flag371-))
)

)
(:action Prim372Action-0
:parameters ()
:precondition
(and
(not (BELOWOF12-ROBOT))
)
:effect
(and
(Flag372prime-)
(not (Flag372-))
)

)
(:action Prim372Action-1
:parameters ()
:precondition
(and
(not (ABOVEOF11-ROBOT))
)
:effect
(and
(Flag372prime-)
(not (Flag372-))
)

)
(:action Prim373Action-0
:parameters ()
:precondition
(and
(not (BELOWOF12-ROBOT))
)
:effect
(and
(Flag373prime-)
(not (Flag373-))
)

)
(:action Prim373Action-1
:parameters ()
:precondition
(and
(not (ABOVEOF14-ROBOT))
)
:effect
(and
(Flag373prime-)
(not (Flag373-))
)

)
(:action Flag375Action
:parameters ()
:precondition
(and
(ABOVEOF14-ROBOT)
(BELOWOF13-ROBOT)
)
:effect
(and
(Flag375-)
(Flag375prime-)
)

)
(:action Flag376Action
:parameters ()
:precondition
(and
(ABOVEOF13-ROBOT)
(BELOWOF13-ROBOT)
)
:effect
(and
(Flag376-)
(Flag376prime-)
)

)
(:action Flag377Action
:parameters ()
:precondition
(and
(ABOVEOF15-ROBOT)
(BELOWOF13-ROBOT)
)
:effect
(and
(Flag377-)
(Flag377prime-)
)

)
(:action Flag378Action
:parameters ()
:precondition
(and
(ABOVEOF12-ROBOT)
(BELOWOF13-ROBOT)
)
:effect
(and
(Flag378-)
(Flag378prime-)
)

)
(:action Flag379Action
:parameters ()
:precondition
(and
(BELOWOF12-ROBOT)
(ABOVEOF12-ROBOT)
)
:effect
(and
(Flag379-)
(Flag379prime-)
)

)
(:action Prim375Action-0
:parameters ()
:precondition
(and
(not (ABOVEOF14-ROBOT))
)
:effect
(and
(Flag375prime-)
(not (Flag375-))
)

)
(:action Prim375Action-1
:parameters ()
:precondition
(and
(not (BELOWOF13-ROBOT))
)
:effect
(and
(Flag375prime-)
(not (Flag375-))
)

)
(:action Prim376Action-0
:parameters ()
:precondition
(and
(not (ABOVEOF13-ROBOT))
)
:effect
(and
(Flag376prime-)
(not (Flag376-))
)

)
(:action Prim376Action-1
:parameters ()
:precondition
(and
(not (BELOWOF13-ROBOT))
)
:effect
(and
(Flag376prime-)
(not (Flag376-))
)

)
(:action Prim377Action-0
:parameters ()
:precondition
(and
(not (ABOVEOF15-ROBOT))
)
:effect
(and
(Flag377prime-)
(not (Flag377-))
)

)
(:action Prim377Action-1
:parameters ()
:precondition
(and
(not (BELOWOF13-ROBOT))
)
:effect
(and
(Flag377prime-)
(not (Flag377-))
)

)
(:action Prim378Action-0
:parameters ()
:precondition
(and
(not (ABOVEOF12-ROBOT))
)
:effect
(and
(Flag378prime-)
(not (Flag378-))
)

)
(:action Prim378Action-1
:parameters ()
:precondition
(and
(not (BELOWOF13-ROBOT))
)
:effect
(and
(Flag378prime-)
(not (Flag378-))
)

)
(:action Prim379Action-0
:parameters ()
:precondition
(and
(not (BELOWOF12-ROBOT))
)
:effect
(and
(Flag379prime-)
(not (Flag379-))
)

)
(:action Prim379Action-1
:parameters ()
:precondition
(and
(not (ABOVEOF12-ROBOT))
)
:effect
(and
(Flag379prime-)
(not (Flag379-))
)

)
(:action Flag381Action
:parameters ()
:precondition
(and
(BELOWOF14-ROBOT)
(ABOVEOF14-ROBOT)
)
:effect
(and
(Flag381-)
(Flag381prime-)
)

)
(:action Flag382Action
:parameters ()
:precondition
(and
(ABOVEOF15-ROBOT)
(BELOWOF14-ROBOT)
)
:effect
(and
(Flag382-)
(Flag382prime-)
)

)
(:action Flag383Action
:parameters ()
:precondition
(and
(BELOWOF13-ROBOT)
(ABOVEOF13-ROBOT)
)
:effect
(and
(Flag383-)
(Flag383prime-)
)

)
(:action Flag384Action
:parameters ()
:precondition
(and
(BELOWOF14-ROBOT)
(ABOVEOF13-ROBOT)
)
:effect
(and
(Flag384-)
(Flag384prime-)
)

)
(:action Prim381Action-0
:parameters ()
:precondition
(and
(not (BELOWOF14-ROBOT))
)
:effect
(and
(Flag381prime-)
(not (Flag381-))
)

)
(:action Prim381Action-1
:parameters ()
:precondition
(and
(not (ABOVEOF14-ROBOT))
)
:effect
(and
(Flag381prime-)
(not (Flag381-))
)

)
(:action Prim382Action-0
:parameters ()
:precondition
(and
(not (ABOVEOF15-ROBOT))
)
:effect
(and
(Flag382prime-)
(not (Flag382-))
)

)
(:action Prim382Action-1
:parameters ()
:precondition
(and
(not (BELOWOF14-ROBOT))
)
:effect
(and
(Flag382prime-)
(not (Flag382-))
)

)
(:action Prim383Action-0
:parameters ()
:precondition
(and
(not (BELOWOF13-ROBOT))
)
:effect
(and
(Flag383prime-)
(not (Flag383-))
)

)
(:action Prim383Action-1
:parameters ()
:precondition
(and
(not (ABOVEOF13-ROBOT))
)
:effect
(and
(Flag383prime-)
(not (Flag383-))
)

)
(:action Prim384Action-0
:parameters ()
:precondition
(and
(not (BELOWOF14-ROBOT))
)
:effect
(and
(Flag384prime-)
(not (Flag384-))
)

)
(:action Prim384Action-1
:parameters ()
:precondition
(and
(not (ABOVEOF13-ROBOT))
)
:effect
(and
(Flag384prime-)
(not (Flag384-))
)

)
(:action Flag386Action
:parameters ()
:precondition
(and
(BELOWOF15-ROBOT)
(ABOVEOF14-ROBOT)
)
:effect
(and
(Flag386-)
(Flag386prime-)
)

)
(:action Flag387Action
:parameters ()
:precondition
(and
(BELOWOF15-ROBOT)
(ABOVEOF15-ROBOT)
)
:effect
(and
(Flag387-)
(Flag387prime-)
)

)
(:action Prim386Action-1
:parameters ()
:precondition
(and
(not (ABOVEOF14-ROBOT))
)
:effect
(and
(Flag386prime-)
(not (Flag386-))
)

)
(:action Prim387Action-1
:parameters ()
:precondition
(and
(not (ABOVEOF15-ROBOT))
)
:effect
(and
(Flag387prime-)
(not (Flag387-))
)

)
(:action Flag389Action
:parameters ()
:precondition
(and
(BELOWOF16-ROBOT)
(ABOVEOF15-ROBOT)
)
:effect
(and
(Flag389-)
(Flag389prime-)
)

)
(:action Prim389Action-0
:parameters ()
:precondition
(and
(not (BELOWOF16-ROBOT))
)
:effect
(and
(Flag389prime-)
(not (Flag389-))
)

)
(:action Prim389Action-1
:parameters ()
:precondition
(and
(not (ABOVEOF15-ROBOT))
)
:effect
(and
(Flag389prime-)
(not (Flag389-))
)

)
(:action Flag391Action-11
:parameters ()
:precondition
(and
(BELOWOF1-ROBOT)
)
:effect
(and
(Flag391-)
(Flag391prime-)
)

)
(:action MOVEUP-ROBOT
:parameters ()
:precondition
(and
(Flag2-)
(Flag2prime-)
(Flag388prime-)
(Flag385prime-)
(Flag380prime-)
(Flag374prime-)
(Flag368prime-)
(Flag361prime-)
(Flag352prime-)
(Flag341prime-)
(Flag330prime-)
(Flag317prime-)
(Flag303prime-)
(Flag290prime-)
(Flag274prime-)
(Flag258prime-)
(Flag391prime-)
)
:effect
(and
(when
(and
(Flag388-)
)
(and
(ROW15-ROBOT)
(not (NOT-ROW15-ROBOT))
)
)
(when
(and
(Flag385-)
)
(and
(ROW14-ROBOT)
(not (NOT-ROW14-ROBOT))
)
)
(when
(and
(Flag380-)
)
(and
(ROW13-ROBOT)
(not (NOT-ROW13-ROBOT))
)
)
(when
(and
(Flag374-)
)
(and
(ROW12-ROBOT)
(not (NOT-ROW12-ROBOT))
)
)
(when
(and
(Flag368-)
)
(and
(ROW11-ROBOT)
(not (NOT-ROW11-ROBOT))
)
)
(when
(and
(Flag361-)
)
(and
(ROW10-ROBOT)
(not (NOT-ROW10-ROBOT))
)
)
(when
(and
(Flag352-)
)
(and
(ROW9-ROBOT)
(not (NOT-ROW9-ROBOT))
)
)
(when
(and
(Flag341-)
)
(and
(ROW8-ROBOT)
(not (NOT-ROW8-ROBOT))
)
)
(when
(and
(Flag330-)
)
(and
(ROW7-ROBOT)
(not (NOT-ROW7-ROBOT))
)
)
(when
(and
(Flag317-)
)
(and
(ROW6-ROBOT)
(not (NOT-ROW6-ROBOT))
)
)
(when
(and
(Flag303-)
)
(and
(ROW5-ROBOT)
(not (NOT-ROW5-ROBOT))
)
)
(when
(and
(Flag290-)
)
(and
(ROW4-ROBOT)
(not (NOT-ROW4-ROBOT))
)
)
(when
(and
(Flag274-)
)
(and
(ROW3-ROBOT)
(not (NOT-ROW3-ROBOT))
)
)
(when
(and
(Flag258-)
)
(and
(ROW2-ROBOT)
(not (NOT-ROW2-ROBOT))
)
)
(when
(and
(Flag391-)
)
(and
(ROW1-ROBOT)
(not (NOT-ROW1-ROBOT))
)
)
(when
(and
(ROW14-ROBOT)
)
(and
(ROW15-ROBOT)
(NOT-ROW14-ROBOT)
(not (NOT-ROW15-ROBOT))
(not (ROW14-ROBOT))
)
)
(when
(and
(ROW13-ROBOT)
)
(and
(ROW14-ROBOT)
(NOT-ROW13-ROBOT)
(not (NOT-ROW14-ROBOT))
(not (ROW13-ROBOT))
)
)
(when
(and
(ROW12-ROBOT)
)
(and
(ROW13-ROBOT)
(NOT-ROW12-ROBOT)
(not (NOT-ROW13-ROBOT))
(not (ROW12-ROBOT))
)
)
(when
(and
(ROW11-ROBOT)
)
(and
(ROW12-ROBOT)
(NOT-ROW11-ROBOT)
(not (NOT-ROW12-ROBOT))
(not (ROW11-ROBOT))
)
)
(when
(and
(ROW10-ROBOT)
)
(and
(ROW11-ROBOT)
(NOT-ROW10-ROBOT)
(not (NOT-ROW11-ROBOT))
(not (ROW10-ROBOT))
)
)
(when
(and
(ROW9-ROBOT)
)
(and
(ROW10-ROBOT)
(NOT-ROW9-ROBOT)
(not (NOT-ROW10-ROBOT))
(not (ROW9-ROBOT))
)
)
(when
(and
(ROW8-ROBOT)
)
(and
(ROW9-ROBOT)
(NOT-ROW8-ROBOT)
(not (NOT-ROW9-ROBOT))
(not (ROW8-ROBOT))
)
)
(when
(and
(ROW7-ROBOT)
)
(and
(ROW8-ROBOT)
(NOT-ROW7-ROBOT)
(not (NOT-ROW8-ROBOT))
(not (ROW7-ROBOT))
)
)
(when
(and
(ROW6-ROBOT)
)
(and
(ROW7-ROBOT)
(NOT-ROW6-ROBOT)
(not (NOT-ROW7-ROBOT))
(not (ROW6-ROBOT))
)
)
(when
(and
(ROW5-ROBOT)
)
(and
(ROW6-ROBOT)
(NOT-ROW5-ROBOT)
(not (NOT-ROW6-ROBOT))
(not (ROW5-ROBOT))
)
)
(when
(and
(ROW4-ROBOT)
)
(and
(ROW5-ROBOT)
(NOT-ROW4-ROBOT)
(not (NOT-ROW5-ROBOT))
(not (ROW4-ROBOT))
)
)
(when
(and
(ROW3-ROBOT)
)
(and
(ROW4-ROBOT)
(NOT-ROW3-ROBOT)
(not (NOT-ROW4-ROBOT))
(not (ROW3-ROBOT))
)
)
(when
(and
(ROW2-ROBOT)
)
(and
(ROW3-ROBOT)
(NOT-ROW2-ROBOT)
(not (NOT-ROW3-ROBOT))
(not (ROW2-ROBOT))
)
)
(when
(and
(ROW1-ROBOT)
)
(and
(ROW2-ROBOT)
(NOT-ROW1-ROBOT)
(not (NOT-ROW2-ROBOT))
(not (ROW1-ROBOT))
)
)
(when
(and
(ROW0-ROBOT)
)
(and
(ROW1-ROBOT)
(NOT-ROW0-ROBOT)
(not (NOT-ROW1-ROBOT))
(not (ROW0-ROBOT))
)
)
(when
(and
(BELOWOF14-ROBOT)
)
(and
(BELOWOF16-ROBOT)
(NOT-BELOWOF15-ROBOT)
(BELOWOF15-ROBOT)
(NOT-BELOWOF14-ROBOT)
(not (NOT-BELOWOF16-ROBOT))
(not (BELOWOF14-ROBOT))
)
)
(when
(and
(BELOWOF8-ROBOT)
)
(and
(BELOWOF16-ROBOT)
(NOT-BELOWOF15-ROBOT)
(BELOWOF15-ROBOT)
(NOT-BELOWOF14-ROBOT)
(BELOWOF14-ROBOT)
(NOT-BELOWOF13-ROBOT)
(BELOWOF13-ROBOT)
(NOT-BELOWOF12-ROBOT)
(BELOWOF12-ROBOT)
(NOT-BELOWOF11-ROBOT)
(BELOWOF11-ROBOT)
(NOT-BELOWOF10-ROBOT)
(BELOWOF10-ROBOT)
(NOT-BELOWOF9-ROBOT)
(BELOWOF9-ROBOT)
(NOT-BELOWOF8-ROBOT)
(not (NOT-BELOWOF16-ROBOT))
(not (BELOWOF8-ROBOT))
)
)
(when
(and
(BELOWOF13-ROBOT)
)
(and
(BELOWOF16-ROBOT)
(NOT-BELOWOF15-ROBOT)
(BELOWOF15-ROBOT)
(NOT-BELOWOF14-ROBOT)
(BELOWOF14-ROBOT)
(NOT-BELOWOF13-ROBOT)
(not (NOT-BELOWOF16-ROBOT))
(not (BELOWOF13-ROBOT))
)
)
(when
(and
(BELOWOF3-ROBOT)
)
(and
(BELOWOF16-ROBOT)
(NOT-BELOWOF15-ROBOT)
(BELOWOF15-ROBOT)
(NOT-BELOWOF14-ROBOT)
(BELOWOF14-ROBOT)
(NOT-BELOWOF13-ROBOT)
(BELOWOF13-ROBOT)
(NOT-BELOWOF12-ROBOT)
(BELOWOF12-ROBOT)
(NOT-BELOWOF11-ROBOT)
(BELOWOF11-ROBOT)
(NOT-BELOWOF10-ROBOT)
(BELOWOF10-ROBOT)
(NOT-BELOWOF9-ROBOT)
(BELOWOF9-ROBOT)
(NOT-BELOWOF8-ROBOT)
(BELOWOF8-ROBOT)
(NOT-BELOWOF7-ROBOT)
(BELOWOF7-ROBOT)
(NOT-BELOWOF6-ROBOT)
(BELOWOF6-ROBOT)
(NOT-BELOWOF5-ROBOT)
(BELOWOF5-ROBOT)
(NOT-BELOWOF4-ROBOT)
(BELOWOF4-ROBOT)
(NOT-BELOWOF3-ROBOT)
(not (NOT-BELOWOF16-ROBOT))
(not (BELOWOF3-ROBOT))
)
)
(when
(and
(BELOWOF9-ROBOT)
)
(and
(BELOWOF16-ROBOT)
(NOT-BELOWOF15-ROBOT)
(BELOWOF15-ROBOT)
(NOT-BELOWOF14-ROBOT)
(BELOWOF14-ROBOT)
(NOT-BELOWOF13-ROBOT)
(BELOWOF13-ROBOT)
(NOT-BELOWOF12-ROBOT)
(BELOWOF12-ROBOT)
(NOT-BELOWOF11-ROBOT)
(BELOWOF11-ROBOT)
(NOT-BELOWOF10-ROBOT)
(BELOWOF10-ROBOT)
(NOT-BELOWOF9-ROBOT)
(not (NOT-BELOWOF16-ROBOT))
(not (BELOWOF9-ROBOT))
)
)
(when
(and
(BELOWOF7-ROBOT)
)
(and
(BELOWOF16-ROBOT)
(NOT-BELOWOF15-ROBOT)
(BELOWOF15-ROBOT)
(NOT-BELOWOF14-ROBOT)
(BELOWOF14-ROBOT)
(NOT-BELOWOF13-ROBOT)
(BELOWOF13-ROBOT)
(NOT-BELOWOF12-ROBOT)
(BELOWOF12-ROBOT)
(NOT-BELOWOF11-ROBOT)
(BELOWOF11-ROBOT)
(NOT-BELOWOF10-ROBOT)
(BELOWOF10-ROBOT)
(NOT-BELOWOF9-ROBOT)
(BELOWOF9-ROBOT)
(NOT-BELOWOF8-ROBOT)
(BELOWOF8-ROBOT)
(NOT-BELOWOF7-ROBOT)
(not (NOT-BELOWOF16-ROBOT))
(not (BELOWOF7-ROBOT))
)
)
(when
(and
(BELOWOF12-ROBOT)
)
(and
(BELOWOF16-ROBOT)
(NOT-BELOWOF15-ROBOT)
(BELOWOF15-ROBOT)
(NOT-BELOWOF14-ROBOT)
(BELOWOF14-ROBOT)
(NOT-BELOWOF13-ROBOT)
(BELOWOF13-ROBOT)
(NOT-BELOWOF12-ROBOT)
(not (NOT-BELOWOF16-ROBOT))
(not (BELOWOF12-ROBOT))
)
)
(when
(and
(BELOWOF4-ROBOT)
)
(and
(BELOWOF16-ROBOT)
(NOT-BELOWOF15-ROBOT)
(BELOWOF15-ROBOT)
(NOT-BELOWOF14-ROBOT)
(BELOWOF14-ROBOT)
(NOT-BELOWOF13-ROBOT)
(BELOWOF13-ROBOT)
(NOT-BELOWOF12-ROBOT)
(BELOWOF12-ROBOT)
(NOT-BELOWOF11-ROBOT)
(BELOWOF11-ROBOT)
(NOT-BELOWOF10-ROBOT)
(BELOWOF10-ROBOT)
(NOT-BELOWOF9-ROBOT)
(BELOWOF9-ROBOT)
(NOT-BELOWOF8-ROBOT)
(BELOWOF8-ROBOT)
(NOT-BELOWOF7-ROBOT)
(BELOWOF7-ROBOT)
(NOT-BELOWOF6-ROBOT)
(BELOWOF6-ROBOT)
(NOT-BELOWOF5-ROBOT)
(BELOWOF5-ROBOT)
(NOT-BELOWOF4-ROBOT)
(not (NOT-BELOWOF16-ROBOT))
(not (BELOWOF4-ROBOT))
)
)
(when
(and
(BELOWOF1-ROBOT)
)
(and
(BELOWOF16-ROBOT)
(NOT-BELOWOF15-ROBOT)
(BELOWOF15-ROBOT)
(NOT-BELOWOF14-ROBOT)
(BELOWOF14-ROBOT)
(NOT-BELOWOF13-ROBOT)
(BELOWOF13-ROBOT)
(NOT-BELOWOF12-ROBOT)
(BELOWOF12-ROBOT)
(NOT-BELOWOF11-ROBOT)
(BELOWOF11-ROBOT)
(NOT-BELOWOF10-ROBOT)
(BELOWOF10-ROBOT)
(NOT-BELOWOF9-ROBOT)
(BELOWOF9-ROBOT)
(NOT-BELOWOF8-ROBOT)
(BELOWOF8-ROBOT)
(NOT-BELOWOF7-ROBOT)
(BELOWOF7-ROBOT)
(NOT-BELOWOF6-ROBOT)
(BELOWOF6-ROBOT)
(NOT-BELOWOF5-ROBOT)
(BELOWOF5-ROBOT)
(NOT-BELOWOF4-ROBOT)
(BELOWOF4-ROBOT)
(NOT-BELOWOF3-ROBOT)
(BELOWOF3-ROBOT)
(NOT-BELOWOF2-ROBOT)
(BELOWOF2-ROBOT)
(NOT-BELOWOF1-ROBOT)
(not (NOT-BELOWOF16-ROBOT))
(not (BELOWOF1-ROBOT))
)
)
(when
(and
(BELOWOF2-ROBOT)
)
(and
(BELOWOF16-ROBOT)
(NOT-BELOWOF15-ROBOT)
(BELOWOF15-ROBOT)
(NOT-BELOWOF14-ROBOT)
(BELOWOF14-ROBOT)
(NOT-BELOWOF13-ROBOT)
(BELOWOF13-ROBOT)
(NOT-BELOWOF12-ROBOT)
(BELOWOF12-ROBOT)
(NOT-BELOWOF11-ROBOT)
(BELOWOF11-ROBOT)
(NOT-BELOWOF10-ROBOT)
(BELOWOF10-ROBOT)
(NOT-BELOWOF9-ROBOT)
(BELOWOF9-ROBOT)
(NOT-BELOWOF8-ROBOT)
(BELOWOF8-ROBOT)
(NOT-BELOWOF7-ROBOT)
(BELOWOF7-ROBOT)
(NOT-BELOWOF6-ROBOT)
(BELOWOF6-ROBOT)
(NOT-BELOWOF5-ROBOT)
(BELOWOF5-ROBOT)
(NOT-BELOWOF4-ROBOT)
(BELOWOF4-ROBOT)
(NOT-BELOWOF3-ROBOT)
(BELOWOF3-ROBOT)
(NOT-BELOWOF2-ROBOT)
(not (NOT-BELOWOF16-ROBOT))
(not (BELOWOF2-ROBOT))
)
)
(when
(and
(BELOWOF5-ROBOT)
)
(and
(BELOWOF16-ROBOT)
(NOT-BELOWOF15-ROBOT)
(BELOWOF15-ROBOT)
(NOT-BELOWOF14-ROBOT)
(BELOWOF14-ROBOT)
(NOT-BELOWOF13-ROBOT)
(BELOWOF13-ROBOT)
(NOT-BELOWOF12-ROBOT)
(BELOWOF12-ROBOT)
(NOT-BELOWOF11-ROBOT)
(BELOWOF11-ROBOT)
(NOT-BELOWOF10-ROBOT)
(BELOWOF10-ROBOT)
(NOT-BELOWOF9-ROBOT)
(BELOWOF9-ROBOT)
(NOT-BELOWOF8-ROBOT)
(BELOWOF8-ROBOT)
(NOT-BELOWOF7-ROBOT)
(BELOWOF7-ROBOT)
(NOT-BELOWOF6-ROBOT)
(BELOWOF6-ROBOT)
(NOT-BELOWOF5-ROBOT)
(not (NOT-BELOWOF16-ROBOT))
(not (BELOWOF5-ROBOT))
)
)
(when
(and
(BELOWOF6-ROBOT)
)
(and
(BELOWOF16-ROBOT)
(NOT-BELOWOF15-ROBOT)
(BELOWOF15-ROBOT)
(NOT-BELOWOF14-ROBOT)
(BELOWOF14-ROBOT)
(NOT-BELOWOF13-ROBOT)
(BELOWOF13-ROBOT)
(NOT-BELOWOF12-ROBOT)
(BELOWOF12-ROBOT)
(NOT-BELOWOF11-ROBOT)
(BELOWOF11-ROBOT)
(NOT-BELOWOF10-ROBOT)
(BELOWOF10-ROBOT)
(NOT-BELOWOF9-ROBOT)
(BELOWOF9-ROBOT)
(NOT-BELOWOF8-ROBOT)
(BELOWOF8-ROBOT)
(NOT-BELOWOF7-ROBOT)
(BELOWOF7-ROBOT)
(NOT-BELOWOF6-ROBOT)
(not (NOT-BELOWOF16-ROBOT))
(not (BELOWOF6-ROBOT))
)
)
(when
(and
(BELOWOF15-ROBOT)
)
(and
(BELOWOF16-ROBOT)
(NOT-BELOWOF15-ROBOT)
(not (NOT-BELOWOF16-ROBOT))
(not (BELOWOF15-ROBOT))
)
)
(when
(and
(BELOWOF11-ROBOT)
)
(and
(BELOWOF16-ROBOT)
(NOT-BELOWOF15-ROBOT)
(BELOWOF15-ROBOT)
(NOT-BELOWOF14-ROBOT)
(BELOWOF14-ROBOT)
(NOT-BELOWOF13-ROBOT)
(BELOWOF13-ROBOT)
(NOT-BELOWOF12-ROBOT)
(BELOWOF12-ROBOT)
(NOT-BELOWOF11-ROBOT)
(not (NOT-BELOWOF16-ROBOT))
(not (BELOWOF11-ROBOT))
)
)
(when
(and
(BELOWOF10-ROBOT)
)
(and
(BELOWOF16-ROBOT)
(NOT-BELOWOF15-ROBOT)
(BELOWOF15-ROBOT)
(NOT-BELOWOF14-ROBOT)
(BELOWOF14-ROBOT)
(NOT-BELOWOF13-ROBOT)
(BELOWOF13-ROBOT)
(NOT-BELOWOF12-ROBOT)
(BELOWOF12-ROBOT)
(NOT-BELOWOF11-ROBOT)
(BELOWOF11-ROBOT)
(NOT-BELOWOF10-ROBOT)
(not (NOT-BELOWOF16-ROBOT))
(not (BELOWOF10-ROBOT))
)
)
(when
(and
(ABOVEOF15-ROBOT)
)
(and
(ABOVEOF14-ROBOT)
(ABOVEOF13-ROBOT)
(ABOVEOF12-ROBOT)
(ABOVEOF11-ROBOT)
(ABOVEOF10-ROBOT)
(ABOVEOF9-ROBOT)
(ABOVEOF8-ROBOT)
(ABOVEOF7-ROBOT)
(ABOVEOF6-ROBOT)
(ABOVEOF5-ROBOT)
(ABOVEOF4-ROBOT)
(ABOVEOF3-ROBOT)
(ABOVEOF2-ROBOT)
(not (NOT-ABOVEOF15-ROBOT))
(not (NOT-ABOVEOF14-ROBOT))
(not (NOT-ABOVEOF13-ROBOT))
(not (NOT-ABOVEOF12-ROBOT))
(not (NOT-ABOVEOF11-ROBOT))
(not (NOT-ABOVEOF10-ROBOT))
(not (NOT-ABOVEOF9-ROBOT))
(not (NOT-ABOVEOF8-ROBOT))
(not (NOT-ABOVEOF7-ROBOT))
(not (NOT-ABOVEOF6-ROBOT))
(not (NOT-ABOVEOF5-ROBOT))
(not (NOT-ABOVEOF4-ROBOT))
(not (NOT-ABOVEOF3-ROBOT))
(not (NOT-ABOVEOF2-ROBOT))
)
)
(when
(and
(ABOVEOF14-ROBOT)
)
(and
(ABOVEOF15-ROBOT)
(ABOVEOF13-ROBOT)
(ABOVEOF12-ROBOT)
(ABOVEOF11-ROBOT)
(ABOVEOF10-ROBOT)
(ABOVEOF9-ROBOT)
(ABOVEOF8-ROBOT)
(ABOVEOF7-ROBOT)
(ABOVEOF6-ROBOT)
(ABOVEOF5-ROBOT)
(ABOVEOF4-ROBOT)
(ABOVEOF3-ROBOT)
(ABOVEOF2-ROBOT)
(not (NOT-ABOVEOF15-ROBOT))
(not (NOT-ABOVEOF14-ROBOT))
(not (NOT-ABOVEOF13-ROBOT))
(not (NOT-ABOVEOF12-ROBOT))
(not (NOT-ABOVEOF11-ROBOT))
(not (NOT-ABOVEOF10-ROBOT))
(not (NOT-ABOVEOF9-ROBOT))
(not (NOT-ABOVEOF8-ROBOT))
(not (NOT-ABOVEOF7-ROBOT))
(not (NOT-ABOVEOF6-ROBOT))
(not (NOT-ABOVEOF5-ROBOT))
(not (NOT-ABOVEOF4-ROBOT))
(not (NOT-ABOVEOF3-ROBOT))
(not (NOT-ABOVEOF2-ROBOT))
)
)
(when
(and
(ABOVEOF13-ROBOT)
)
(and
(ABOVEOF14-ROBOT)
(ABOVEOF12-ROBOT)
(ABOVEOF11-ROBOT)
(ABOVEOF10-ROBOT)
(ABOVEOF9-ROBOT)
(ABOVEOF8-ROBOT)
(ABOVEOF7-ROBOT)
(ABOVEOF6-ROBOT)
(ABOVEOF5-ROBOT)
(ABOVEOF4-ROBOT)
(ABOVEOF3-ROBOT)
(ABOVEOF2-ROBOT)
(not (NOT-ABOVEOF14-ROBOT))
(not (NOT-ABOVEOF13-ROBOT))
(not (NOT-ABOVEOF12-ROBOT))
(not (NOT-ABOVEOF11-ROBOT))
(not (NOT-ABOVEOF10-ROBOT))
(not (NOT-ABOVEOF9-ROBOT))
(not (NOT-ABOVEOF8-ROBOT))
(not (NOT-ABOVEOF7-ROBOT))
(not (NOT-ABOVEOF6-ROBOT))
(not (NOT-ABOVEOF5-ROBOT))
(not (NOT-ABOVEOF4-ROBOT))
(not (NOT-ABOVEOF3-ROBOT))
(not (NOT-ABOVEOF2-ROBOT))
)
)
(when
(and
(ABOVEOF12-ROBOT)
)
(and
(ABOVEOF13-ROBOT)
(ABOVEOF11-ROBOT)
(ABOVEOF10-ROBOT)
(ABOVEOF9-ROBOT)
(ABOVEOF8-ROBOT)
(ABOVEOF7-ROBOT)
(ABOVEOF6-ROBOT)
(ABOVEOF5-ROBOT)
(ABOVEOF4-ROBOT)
(ABOVEOF3-ROBOT)
(ABOVEOF2-ROBOT)
(not (NOT-ABOVEOF13-ROBOT))
(not (NOT-ABOVEOF12-ROBOT))
(not (NOT-ABOVEOF11-ROBOT))
(not (NOT-ABOVEOF10-ROBOT))
(not (NOT-ABOVEOF9-ROBOT))
(not (NOT-ABOVEOF8-ROBOT))
(not (NOT-ABOVEOF7-ROBOT))
(not (NOT-ABOVEOF6-ROBOT))
(not (NOT-ABOVEOF5-ROBOT))
(not (NOT-ABOVEOF4-ROBOT))
(not (NOT-ABOVEOF3-ROBOT))
(not (NOT-ABOVEOF2-ROBOT))
)
)
(when
(and
(ABOVEOF11-ROBOT)
)
(and
(ABOVEOF12-ROBOT)
(ABOVEOF10-ROBOT)
(ABOVEOF9-ROBOT)
(ABOVEOF8-ROBOT)
(ABOVEOF7-ROBOT)
(ABOVEOF6-ROBOT)
(ABOVEOF5-ROBOT)
(ABOVEOF4-ROBOT)
(ABOVEOF3-ROBOT)
(ABOVEOF2-ROBOT)
(not (NOT-ABOVEOF12-ROBOT))
(not (NOT-ABOVEOF11-ROBOT))
(not (NOT-ABOVEOF10-ROBOT))
(not (NOT-ABOVEOF9-ROBOT))
(not (NOT-ABOVEOF8-ROBOT))
(not (NOT-ABOVEOF7-ROBOT))
(not (NOT-ABOVEOF6-ROBOT))
(not (NOT-ABOVEOF5-ROBOT))
(not (NOT-ABOVEOF4-ROBOT))
(not (NOT-ABOVEOF3-ROBOT))
(not (NOT-ABOVEOF2-ROBOT))
)
)
(when
(and
(ABOVEOF10-ROBOT)
)
(and
(ABOVEOF11-ROBOT)
(ABOVEOF9-ROBOT)
(ABOVEOF8-ROBOT)
(ABOVEOF7-ROBOT)
(ABOVEOF6-ROBOT)
(ABOVEOF5-ROBOT)
(ABOVEOF4-ROBOT)
(ABOVEOF3-ROBOT)
(ABOVEOF2-ROBOT)
(not (NOT-ABOVEOF11-ROBOT))
(not (NOT-ABOVEOF10-ROBOT))
(not (NOT-ABOVEOF9-ROBOT))
(not (NOT-ABOVEOF8-ROBOT))
(not (NOT-ABOVEOF7-ROBOT))
(not (NOT-ABOVEOF6-ROBOT))
(not (NOT-ABOVEOF5-ROBOT))
(not (NOT-ABOVEOF4-ROBOT))
(not (NOT-ABOVEOF3-ROBOT))
(not (NOT-ABOVEOF2-ROBOT))
)
)
(when
(and
(ABOVEOF9-ROBOT)
)
(and
(ABOVEOF10-ROBOT)
(ABOVEOF8-ROBOT)
(ABOVEOF7-ROBOT)
(ABOVEOF6-ROBOT)
(ABOVEOF5-ROBOT)
(ABOVEOF4-ROBOT)
(ABOVEOF3-ROBOT)
(ABOVEOF2-ROBOT)
(not (NOT-ABOVEOF10-ROBOT))
(not (NOT-ABOVEOF9-ROBOT))
(not (NOT-ABOVEOF8-ROBOT))
(not (NOT-ABOVEOF7-ROBOT))
(not (NOT-ABOVEOF6-ROBOT))
(not (NOT-ABOVEOF5-ROBOT))
(not (NOT-ABOVEOF4-ROBOT))
(not (NOT-ABOVEOF3-ROBOT))
(not (NOT-ABOVEOF2-ROBOT))
)
)
(when
(and
(ABOVEOF8-ROBOT)
)
(and
(ABOVEOF9-ROBOT)
(ABOVEOF7-ROBOT)
(ABOVEOF6-ROBOT)
(ABOVEOF5-ROBOT)
(ABOVEOF4-ROBOT)
(ABOVEOF3-ROBOT)
(ABOVEOF2-ROBOT)
(not (NOT-ABOVEOF9-ROBOT))
(not (NOT-ABOVEOF8-ROBOT))
(not (NOT-ABOVEOF7-ROBOT))
(not (NOT-ABOVEOF6-ROBOT))
(not (NOT-ABOVEOF5-ROBOT))
(not (NOT-ABOVEOF4-ROBOT))
(not (NOT-ABOVEOF3-ROBOT))
(not (NOT-ABOVEOF2-ROBOT))
)
)
(when
(and
(ABOVEOF7-ROBOT)
)
(and
(ABOVEOF8-ROBOT)
(ABOVEOF6-ROBOT)
(ABOVEOF5-ROBOT)
(ABOVEOF4-ROBOT)
(ABOVEOF3-ROBOT)
(ABOVEOF2-ROBOT)
(not (NOT-ABOVEOF8-ROBOT))
(not (NOT-ABOVEOF7-ROBOT))
(not (NOT-ABOVEOF6-ROBOT))
(not (NOT-ABOVEOF5-ROBOT))
(not (NOT-ABOVEOF4-ROBOT))
(not (NOT-ABOVEOF3-ROBOT))
(not (NOT-ABOVEOF2-ROBOT))
)
)
(when
(and
(ABOVEOF6-ROBOT)
)
(and
(ABOVEOF7-ROBOT)
(ABOVEOF5-ROBOT)
(ABOVEOF4-ROBOT)
(ABOVEOF3-ROBOT)
(ABOVEOF2-ROBOT)
(not (NOT-ABOVEOF7-ROBOT))
(not (NOT-ABOVEOF6-ROBOT))
(not (NOT-ABOVEOF5-ROBOT))
(not (NOT-ABOVEOF4-ROBOT))
(not (NOT-ABOVEOF3-ROBOT))
(not (NOT-ABOVEOF2-ROBOT))
)
)
(when
(and
(ABOVEOF5-ROBOT)
)
(and
(ABOVEOF6-ROBOT)
(ABOVEOF4-ROBOT)
(ABOVEOF3-ROBOT)
(ABOVEOF2-ROBOT)
(not (NOT-ABOVEOF6-ROBOT))
(not (NOT-ABOVEOF5-ROBOT))
(not (NOT-ABOVEOF4-ROBOT))
(not (NOT-ABOVEOF3-ROBOT))
(not (NOT-ABOVEOF2-ROBOT))
)
)
(when
(and
(ABOVEOF4-ROBOT)
)
(and
(ABOVEOF5-ROBOT)
(ABOVEOF3-ROBOT)
(ABOVEOF2-ROBOT)
(not (NOT-ABOVEOF5-ROBOT))
(not (NOT-ABOVEOF4-ROBOT))
(not (NOT-ABOVEOF3-ROBOT))
(not (NOT-ABOVEOF2-ROBOT))
)
)
(when
(and
(ABOVEOF3-ROBOT)
)
(and
(ABOVEOF4-ROBOT)
(ABOVEOF2-ROBOT)
(not (NOT-ABOVEOF4-ROBOT))
(not (NOT-ABOVEOF3-ROBOT))
(not (NOT-ABOVEOF2-ROBOT))
)
)
(when
(and
(ABOVEOF2-ROBOT)
)
(and
(ABOVEOF3-ROBOT)
(not (NOT-ABOVEOF3-ROBOT))
(not (NOT-ABOVEOF2-ROBOT))
)
)
(when
(and
(ABOVEOF1-ROBOT)
)
(and
(ABOVEOF2-ROBOT)
(not (NOT-ABOVEOF2-ROBOT))
)
)
(ABOVEOF1-ROBOT)
(CHECKCONSISTENCY-)
(not (NOT-ABOVEOF1-ROBOT))
(not (NOT-CHECKCONSISTENCY-))
(not (Flag213prime-))
(not (Flag212prime-))
(not (Flag195prime-))
(not (Flag33prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag391prime-))
(not (Flag389prime-))
(not (Flag387prime-))
(not (Flag386prime-))
(not (Flag384prime-))
(not (Flag383prime-))
(not (Flag382prime-))
(not (Flag381prime-))
(not (Flag379prime-))
(not (Flag378prime-))
(not (Flag377prime-))
(not (Flag376prime-))
(not (Flag375prime-))
(not (Flag373prime-))
(not (Flag372prime-))
(not (Flag371prime-))
(not (Flag370prime-))
(not (Flag369prime-))
(not (Flag367prime-))
(not (Flag366prime-))
(not (Flag365prime-))
(not (Flag364prime-))
(not (Flag363prime-))
(not (Flag362prime-))
(not (Flag360prime-))
(not (Flag359prime-))
(not (Flag358prime-))
(not (Flag357prime-))
(not (Flag356prime-))
(not (Flag355prime-))
(not (Flag354prime-))
(not (Flag353prime-))
(not (Flag351prime-))
(not (Flag350prime-))
(not (Flag349prime-))
(not (Flag348prime-))
(not (Flag347prime-))
(not (Flag346prime-))
(not (Flag345prime-))
(not (Flag344prime-))
(not (Flag343prime-))
(not (Flag342prime-))
(not (Flag340prime-))
(not (Flag339prime-))
(not (Flag338prime-))
(not (Flag337prime-))
(not (Flag336prime-))
(not (Flag335prime-))
(not (Flag334prime-))
(not (Flag333prime-))
(not (Flag332prime-))
(not (Flag331prime-))
(not (Flag329prime-))
(not (Flag328prime-))
(not (Flag327prime-))
(not (Flag326prime-))
(not (Flag325prime-))
(not (Flag324prime-))
(not (Flag323prime-))
(not (Flag322prime-))
(not (Flag321prime-))
(not (Flag320prime-))
(not (Flag319prime-))
(not (Flag318prime-))
(not (Flag316prime-))
(not (Flag315prime-))
(not (Flag314prime-))
(not (Flag313prime-))
(not (Flag312prime-))
(not (Flag311prime-))
(not (Flag310prime-))
(not (Flag309prime-))
(not (Flag308prime-))
(not (Flag307prime-))
(not (Flag306prime-))
(not (Flag305prime-))
(not (Flag304prime-))
(not (Flag302prime-))
(not (Flag301prime-))
(not (Flag300prime-))
(not (Flag299prime-))
(not (Flag298prime-))
(not (Flag297prime-))
(not (Flag296prime-))
(not (Flag295prime-))
(not (Flag294prime-))
(not (Flag293prime-))
(not (Flag292prime-))
(not (Flag291prime-))
(not (Flag289prime-))
(not (Flag288prime-))
(not (Flag287prime-))
(not (Flag286prime-))
(not (Flag285prime-))
(not (Flag284prime-))
(not (Flag283prime-))
(not (Flag282prime-))
(not (Flag281prime-))
(not (Flag280prime-))
(not (Flag279prime-))
(not (Flag278prime-))
(not (Flag277prime-))
(not (Flag276prime-))
(not (Flag275prime-))
(not (Flag273prime-))
(not (Flag272prime-))
(not (Flag271prime-))
(not (Flag270prime-))
(not (Flag269prime-))
(not (Flag268prime-))
(not (Flag267prime-))
(not (Flag266prime-))
(not (Flag265prime-))
(not (Flag264prime-))
(not (Flag263prime-))
(not (Flag262prime-))
(not (Flag261prime-))
(not (Flag260prime-))
(not (Flag259prime-))
(not (Flag257prime-))
(not (Flag256prime-))
(not (Flag255prime-))
(not (Flag254prime-))
(not (Flag253prime-))
(not (Flag252prime-))
(not (Flag251prime-))
(not (Flag250prime-))
(not (Flag249prime-))
(not (Flag248prime-))
(not (Flag247prime-))
(not (Flag246prime-))
(not (Flag245prime-))
(not (Flag244prime-))
(not (Flag243prime-))
(not (Flag242prime-))
(not (Flag241prime-))
(not (Flag240prime-))
(not (Flag239prime-))
(not (Flag238prime-))
(not (Flag237prime-))
(not (Flag236prime-))
(not (Flag235prime-))
(not (Flag234prime-))
(not (Flag233prime-))
(not (Flag232prime-))
(not (Flag231prime-))
(not (Flag230prime-))
(not (Flag229prime-))
(not (Flag228prime-))
(not (Flag227prime-))
(not (Flag226prime-))
(not (Flag225prime-))
(not (Flag224prime-))
(not (Flag223prime-))
(not (Flag222prime-))
(not (Flag221prime-))
(not (Flag220prime-))
(not (Flag219prime-))
(not (Flag218prime-))
(not (Flag217prime-))
(not (Flag216prime-))
(not (Flag215prime-))
(not (Flag214prime-))
(not (Flag211prime-))
(not (Flag210prime-))
(not (Flag209prime-))
(not (Flag208prime-))
(not (Flag207prime-))
(not (Flag206prime-))
(not (Flag205prime-))
(not (Flag204prime-))
(not (Flag203prime-))
(not (Flag202prime-))
(not (Flag201prime-))
(not (Flag200prime-))
(not (Flag199prime-))
(not (Flag4prime-))
(not (Flag390prime-))
(not (Flag388prime-))
(not (Flag385prime-))
(not (Flag380prime-))
(not (Flag374prime-))
(not (Flag368prime-))
(not (Flag361prime-))
(not (Flag352prime-))
(not (Flag341prime-))
(not (Flag330prime-))
(not (Flag317prime-))
(not (Flag303prime-))
(not (Flag290prime-))
(not (Flag274prime-))
(not (Flag258prime-))
(not (Flag197prime-))
(not (Flag196prime-))
(not (Flag193prime-))
(not (Flag192prime-))
(not (Flag190prime-))
(not (Flag189prime-))
(not (Flag188prime-))
(not (Flag186prime-))
(not (Flag185prime-))
(not (Flag184prime-))
(not (Flag183prime-))
(not (Flag182prime-))
(not (Flag180prime-))
(not (Flag179prime-))
(not (Flag178prime-))
(not (Flag177prime-))
(not (Flag176prime-))
(not (Flag174prime-))
(not (Flag173prime-))
(not (Flag172prime-))
(not (Flag171prime-))
(not (Flag170prime-))
(not (Flag169prime-))
(not (Flag168prime-))
(not (Flag166prime-))
(not (Flag165prime-))
(not (Flag164prime-))
(not (Flag163prime-))
(not (Flag162prime-))
(not (Flag161prime-))
(not (Flag160prime-))
(not (Flag158prime-))
(not (Flag157prime-))
(not (Flag156prime-))
(not (Flag155prime-))
(not (Flag154prime-))
(not (Flag153prime-))
(not (Flag152prime-))
(not (Flag151prime-))
(not (Flag150prime-))
(not (Flag148prime-))
(not (Flag147prime-))
(not (Flag146prime-))
(not (Flag145prime-))
(not (Flag144prime-))
(not (Flag143prime-))
(not (Flag142prime-))
(not (Flag141prime-))
(not (Flag140prime-))
(not (Flag139prime-))
(not (Flag137prime-))
(not (Flag136prime-))
(not (Flag135prime-))
(not (Flag134prime-))
(not (Flag133prime-))
(not (Flag132prime-))
(not (Flag131prime-))
(not (Flag130prime-))
(not (Flag129prime-))
(not (Flag128prime-))
(not (Flag127prime-))
(not (Flag126prime-))
(not (Flag124prime-))
(not (Flag123prime-))
(not (Flag122prime-))
(not (Flag121prime-))
(not (Flag120prime-))
(not (Flag119prime-))
(not (Flag118prime-))
(not (Flag117prime-))
(not (Flag116prime-))
(not (Flag115prime-))
(not (Flag114prime-))
(not (Flag112prime-))
(not (Flag111prime-))
(not (Flag110prime-))
(not (Flag109prime-))
(not (Flag108prime-))
(not (Flag107prime-))
(not (Flag106prime-))
(not (Flag105prime-))
(not (Flag104prime-))
(not (Flag103prime-))
(not (Flag102prime-))
(not (Flag101prime-))
(not (Flag100prime-))
(not (Flag99prime-))
(not (Flag97prime-))
(not (Flag96prime-))
(not (Flag95prime-))
(not (Flag94prime-))
(not (Flag93prime-))
(not (Flag92prime-))
(not (Flag91prime-))
(not (Flag90prime-))
(not (Flag89prime-))
(not (Flag88prime-))
(not (Flag87prime-))
(not (Flag86prime-))
(not (Flag85prime-))
(not (Flag84prime-))
(not (Flag83prime-))
(not (Flag81prime-))
(not (Flag80prime-))
(not (Flag79prime-))
(not (Flag78prime-))
(not (Flag77prime-))
(not (Flag76prime-))
(not (Flag75prime-))
(not (Flag74prime-))
(not (Flag73prime-))
(not (Flag72prime-))
(not (Flag71prime-))
(not (Flag70prime-))
(not (Flag69prime-))
(not (Flag68prime-))
(not (Flag66prime-))
(not (Flag65prime-))
(not (Flag64prime-))
(not (Flag63prime-))
(not (Flag62prime-))
(not (Flag61prime-))
(not (Flag60prime-))
(not (Flag59prime-))
(not (Flag58prime-))
(not (Flag57prime-))
(not (Flag56prime-))
(not (Flag55prime-))
(not (Flag54prime-))
(not (Flag53prime-))
(not (Flag52prime-))
(not (Flag51prime-))
(not (Flag49prime-))
(not (Flag48prime-))
(not (Flag47prime-))
(not (Flag46prime-))
(not (Flag45prime-))
(not (Flag44prime-))
(not (Flag43prime-))
(not (Flag42prime-))
(not (Flag41prime-))
(not (Flag40prime-))
(not (Flag39prime-))
(not (Flag38prime-))
(not (Flag37prime-))
(not (Flag36prime-))
(not (Flag35prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag31prime-))
(not (Flag30prime-))
(not (Flag29prime-))
(not (Flag28prime-))
(not (Flag27prime-))
(not (Flag26prime-))
(not (Flag25prime-))
(not (Flag24prime-))
(not (Flag23prime-))
(not (Flag22prime-))
(not (Flag21prime-))
(not (Flag20prime-))
(not (Flag19prime-))
(not (Flag18prime-))
(not (Flag17prime-))
(not (Flag16prime-))
(not (Flag15prime-))
(not (Flag14prime-))
(not (Flag13prime-))
(not (Flag12prime-))
(not (Flag11prime-))
(not (Flag10prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag1prime-))
(not (Flag198prime-))
(not (Flag194prime-))
(not (Flag191prime-))
(not (Flag187prime-))
(not (Flag181prime-))
(not (Flag175prime-))
(not (Flag167prime-))
(not (Flag159prime-))
(not (Flag149prime-))
(not (Flag138prime-))
(not (Flag125prime-))
(not (Flag113prime-))
(not (Flag98prime-))
(not (Flag82prime-))
(not (Flag67prime-))
(not (Flag50prime-))
)
)
(:action MOVEDOWN-ROBOT
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(Flag390prime-)
(Flag388prime-)
(Flag385prime-)
(Flag380prime-)
(Flag374prime-)
(Flag368prime-)
(Flag361prime-)
(Flag352prime-)
(Flag341prime-)
(Flag330prime-)
(Flag317prime-)
(Flag303prime-)
(Flag290prime-)
(Flag274prime-)
(Flag258prime-)
(Flag227prime-)
(Flag226prime-)
(Flag225prime-)
(Flag224prime-)
(Flag223prime-)
(Flag222prime-)
(Flag221prime-)
(Flag220prime-)
(Flag219prime-)
(Flag218prime-)
(Flag217prime-)
(Flag216prime-)
(Flag215prime-)
(Flag214prime-)
(Flag213prime-)
(Flag212prime-)
(Flag211prime-)
(Flag210prime-)
(Flag209prime-)
(Flag208prime-)
(Flag207prime-)
(Flag206prime-)
(Flag205prime-)
(Flag204prime-)
(Flag203prime-)
(Flag202prime-)
(Flag201prime-)
(Flag200prime-)
(Flag199prime-)
)
:effect
(and
(when
(and
(Flag390-)
)
(and
(ROW14-ROBOT)
(not (NOT-ROW14-ROBOT))
)
)
(when
(and
(Flag388-)
)
(and
(ROW13-ROBOT)
(not (NOT-ROW13-ROBOT))
)
)
(when
(and
(Flag385-)
)
(and
(ROW12-ROBOT)
(not (NOT-ROW12-ROBOT))
)
)
(when
(and
(Flag380-)
)
(and
(ROW11-ROBOT)
(not (NOT-ROW11-ROBOT))
)
)
(when
(and
(Flag374-)
)
(and
(ROW10-ROBOT)
(not (NOT-ROW10-ROBOT))
)
)
(when
(and
(Flag368-)
)
(and
(ROW9-ROBOT)
(not (NOT-ROW9-ROBOT))
)
)
(when
(and
(Flag361-)
)
(and
(ROW8-ROBOT)
(not (NOT-ROW8-ROBOT))
)
)
(when
(and
(Flag352-)
)
(and
(ROW7-ROBOT)
(not (NOT-ROW7-ROBOT))
)
)
(when
(and
(Flag341-)
)
(and
(ROW6-ROBOT)
(not (NOT-ROW6-ROBOT))
)
)
(when
(and
(Flag330-)
)
(and
(ROW5-ROBOT)
(not (NOT-ROW5-ROBOT))
)
)
(when
(and
(Flag317-)
)
(and
(ROW4-ROBOT)
(not (NOT-ROW4-ROBOT))
)
)
(when
(and
(Flag303-)
)
(and
(ROW3-ROBOT)
(not (NOT-ROW3-ROBOT))
)
)
(when
(and
(Flag290-)
)
(and
(ROW2-ROBOT)
(not (NOT-ROW2-ROBOT))
)
)
(when
(and
(Flag274-)
)
(and
(ROW1-ROBOT)
(not (NOT-ROW1-ROBOT))
)
)
(when
(and
(Flag258-)
)
(and
(ROW0-ROBOT)
(not (NOT-ROW0-ROBOT))
)
)
(when
(and
(ROW15-ROBOT)
)
(and
(ROW14-ROBOT)
(NOT-ROW15-ROBOT)
(not (NOT-ROW14-ROBOT))
(not (ROW15-ROBOT))
)
)
(when
(and
(ROW14-ROBOT)
)
(and
(ROW13-ROBOT)
(NOT-ROW14-ROBOT)
(not (NOT-ROW13-ROBOT))
(not (ROW14-ROBOT))
)
)
(when
(and
(ROW13-ROBOT)
)
(and
(ROW12-ROBOT)
(NOT-ROW13-ROBOT)
(not (NOT-ROW12-ROBOT))
(not (ROW13-ROBOT))
)
)
(when
(and
(ROW12-ROBOT)
)
(and
(ROW11-ROBOT)
(NOT-ROW12-ROBOT)
(not (NOT-ROW11-ROBOT))
(not (ROW12-ROBOT))
)
)
(when
(and
(ROW11-ROBOT)
)
(and
(ROW10-ROBOT)
(NOT-ROW11-ROBOT)
(not (NOT-ROW10-ROBOT))
(not (ROW11-ROBOT))
)
)
(when
(and
(ROW10-ROBOT)
)
(and
(ROW9-ROBOT)
(NOT-ROW10-ROBOT)
(not (NOT-ROW9-ROBOT))
(not (ROW10-ROBOT))
)
)
(when
(and
(ROW9-ROBOT)
)
(and
(ROW8-ROBOT)
(NOT-ROW9-ROBOT)
(not (NOT-ROW8-ROBOT))
(not (ROW9-ROBOT))
)
)
(when
(and
(ROW8-ROBOT)
)
(and
(ROW7-ROBOT)
(NOT-ROW8-ROBOT)
(not (NOT-ROW7-ROBOT))
(not (ROW8-ROBOT))
)
)
(when
(and
(ROW7-ROBOT)
)
(and
(ROW6-ROBOT)
(NOT-ROW7-ROBOT)
(not (NOT-ROW6-ROBOT))
(not (ROW7-ROBOT))
)
)
(when
(and
(ROW6-ROBOT)
)
(and
(ROW5-ROBOT)
(NOT-ROW6-ROBOT)
(not (NOT-ROW5-ROBOT))
(not (ROW6-ROBOT))
)
)
(when
(and
(ROW5-ROBOT)
)
(and
(ROW4-ROBOT)
(NOT-ROW5-ROBOT)
(not (NOT-ROW4-ROBOT))
(not (ROW5-ROBOT))
)
)
(when
(and
(ROW4-ROBOT)
)
(and
(ROW3-ROBOT)
(NOT-ROW4-ROBOT)
(not (NOT-ROW3-ROBOT))
(not (ROW4-ROBOT))
)
)
(when
(and
(ROW3-ROBOT)
)
(and
(ROW2-ROBOT)
(NOT-ROW3-ROBOT)
(not (NOT-ROW2-ROBOT))
(not (ROW3-ROBOT))
)
)
(when
(and
(ROW2-ROBOT)
)
(and
(ROW1-ROBOT)
(NOT-ROW2-ROBOT)
(not (NOT-ROW1-ROBOT))
(not (ROW2-ROBOT))
)
)
(when
(and
(ROW1-ROBOT)
)
(and
(ROW0-ROBOT)
(NOT-ROW1-ROBOT)
(not (NOT-ROW0-ROBOT))
(not (ROW1-ROBOT))
)
)
(when
(and
(ABOVEOF15-ROBOT)
)
(and
(ABOVEOF14-ROBOT)
(NOT-ABOVEOF15-ROBOT)
(not (NOT-ABOVEOF14-ROBOT))
(not (ABOVEOF15-ROBOT))
)
)
(when
(and
(Flag227-)
)
(and
(ABOVEOF13-ROBOT)
(NOT-ABOVEOF14-ROBOT)
(not (NOT-ABOVEOF13-ROBOT))
(not (ABOVEOF14-ROBOT))
)
)
(when
(and
(Flag226-)
)
(and
(ABOVEOF12-ROBOT)
(NOT-ABOVEOF13-ROBOT)
(not (NOT-ABOVEOF12-ROBOT))
(not (ABOVEOF13-ROBOT))
)
)
(when
(and
(Flag225-)
)
(and
(ABOVEOF11-ROBOT)
(NOT-ABOVEOF12-ROBOT)
(not (NOT-ABOVEOF11-ROBOT))
(not (ABOVEOF12-ROBOT))
)
)
(when
(and
(Flag224-)
)
(and
(ABOVEOF10-ROBOT)
(NOT-ABOVEOF11-ROBOT)
(not (NOT-ABOVEOF10-ROBOT))
(not (ABOVEOF11-ROBOT))
)
)
(when
(and
(Flag223-)
)
(and
(ABOVEOF9-ROBOT)
(NOT-ABOVEOF10-ROBOT)
(not (NOT-ABOVEOF9-ROBOT))
(not (ABOVEOF10-ROBOT))
)
)
(when
(and
(Flag222-)
)
(and
(ABOVEOF8-ROBOT)
(NOT-ABOVEOF9-ROBOT)
(not (NOT-ABOVEOF8-ROBOT))
(not (ABOVEOF9-ROBOT))
)
)
(when
(and
(Flag221-)
)
(and
(ABOVEOF7-ROBOT)
(NOT-ABOVEOF8-ROBOT)
(not (NOT-ABOVEOF7-ROBOT))
(not (ABOVEOF8-ROBOT))
)
)
(when
(and
(Flag220-)
)
(and
(ABOVEOF6-ROBOT)
(NOT-ABOVEOF7-ROBOT)
(not (NOT-ABOVEOF6-ROBOT))
(not (ABOVEOF7-ROBOT))
)
)
(when
(and
(Flag219-)
)
(and
(ABOVEOF5-ROBOT)
(NOT-ABOVEOF6-ROBOT)
(not (NOT-ABOVEOF5-ROBOT))
(not (ABOVEOF6-ROBOT))
)
)
(when
(and
(Flag218-)
)
(and
(ABOVEOF4-ROBOT)
(NOT-ABOVEOF5-ROBOT)
(not (NOT-ABOVEOF4-ROBOT))
(not (ABOVEOF5-ROBOT))
)
)
(when
(and
(Flag217-)
)
(and
(ABOVEOF3-ROBOT)
(NOT-ABOVEOF4-ROBOT)
(not (NOT-ABOVEOF3-ROBOT))
(not (ABOVEOF4-ROBOT))
)
)
(when
(and
(Flag216-)
)
(and
(ABOVEOF2-ROBOT)
(NOT-ABOVEOF3-ROBOT)
(not (NOT-ABOVEOF2-ROBOT))
(not (ABOVEOF3-ROBOT))
)
)
(when
(and
(Flag215-)
)
(and
(ABOVEOF1-ROBOT)
(NOT-ABOVEOF2-ROBOT)
(not (NOT-ABOVEOF1-ROBOT))
(not (ABOVEOF2-ROBOT))
)
)
(when
(and
(Flag214-)
)
(and
(NOT-ABOVEOF1-ROBOT)
(not (ABOVEOF1-ROBOT))
)
)
(when
(and
(Flag213-)
)
(and
(BELOWOF15-ROBOT)
(not (NOT-BELOWOF15-ROBOT))
)
)
(when
(and
(Flag212-)
)
(and
(BELOWOF14-ROBOT)
(not (NOT-BELOWOF14-ROBOT))
)
)
(when
(and
(Flag211-)
)
(and
(BELOWOF13-ROBOT)
(not (NOT-BELOWOF13-ROBOT))
)
)
(when
(and
(Flag210-)
)
(and
(BELOWOF12-ROBOT)
(not (NOT-BELOWOF12-ROBOT))
)
)
(when
(and
(Flag209-)
)
(and
(BELOWOF11-ROBOT)
(not (NOT-BELOWOF11-ROBOT))
)
)
(when
(and
(Flag208-)
)
(and
(BELOWOF10-ROBOT)
(not (NOT-BELOWOF10-ROBOT))
)
)
(when
(and
(Flag207-)
)
(and
(BELOWOF9-ROBOT)
(not (NOT-BELOWOF9-ROBOT))
)
)
(when
(and
(Flag206-)
)
(and
(BELOWOF8-ROBOT)
(not (NOT-BELOWOF8-ROBOT))
)
)
(when
(and
(Flag205-)
)
(and
(BELOWOF7-ROBOT)
(not (NOT-BELOWOF7-ROBOT))
)
)
(when
(and
(Flag204-)
)
(and
(BELOWOF6-ROBOT)
(not (NOT-BELOWOF6-ROBOT))
)
)
(when
(and
(Flag203-)
)
(and
(BELOWOF5-ROBOT)
(not (NOT-BELOWOF5-ROBOT))
)
)
(when
(and
(Flag202-)
)
(and
(BELOWOF4-ROBOT)
(not (NOT-BELOWOF4-ROBOT))
)
)
(when
(and
(Flag201-)
)
(and
(BELOWOF3-ROBOT)
(not (NOT-BELOWOF3-ROBOT))
)
)
(when
(and
(Flag200-)
)
(and
(BELOWOF2-ROBOT)
(not (NOT-BELOWOF2-ROBOT))
)
)
(when
(and
(Flag199-)
)
(and
(BELOWOF1-ROBOT)
(not (NOT-BELOWOF1-ROBOT))
)
)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag213prime-))
(not (Flag212prime-))
(not (Flag195prime-))
(not (Flag33prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag391prime-))
(not (Flag389prime-))
(not (Flag387prime-))
(not (Flag386prime-))
(not (Flag384prime-))
(not (Flag383prime-))
(not (Flag382prime-))
(not (Flag381prime-))
(not (Flag379prime-))
(not (Flag378prime-))
(not (Flag377prime-))
(not (Flag376prime-))
(not (Flag375prime-))
(not (Flag373prime-))
(not (Flag372prime-))
(not (Flag371prime-))
(not (Flag370prime-))
(not (Flag369prime-))
(not (Flag367prime-))
(not (Flag366prime-))
(not (Flag365prime-))
(not (Flag364prime-))
(not (Flag363prime-))
(not (Flag362prime-))
(not (Flag360prime-))
(not (Flag359prime-))
(not (Flag358prime-))
(not (Flag357prime-))
(not (Flag356prime-))
(not (Flag355prime-))
(not (Flag354prime-))
(not (Flag353prime-))
(not (Flag351prime-))
(not (Flag350prime-))
(not (Flag349prime-))
(not (Flag348prime-))
(not (Flag347prime-))
(not (Flag346prime-))
(not (Flag345prime-))
(not (Flag344prime-))
(not (Flag343prime-))
(not (Flag342prime-))
(not (Flag340prime-))
(not (Flag339prime-))
(not (Flag338prime-))
(not (Flag337prime-))
(not (Flag336prime-))
(not (Flag335prime-))
(not (Flag334prime-))
(not (Flag333prime-))
(not (Flag332prime-))
(not (Flag331prime-))
(not (Flag329prime-))
(not (Flag328prime-))
(not (Flag327prime-))
(not (Flag326prime-))
(not (Flag325prime-))
(not (Flag324prime-))
(not (Flag323prime-))
(not (Flag322prime-))
(not (Flag321prime-))
(not (Flag320prime-))
(not (Flag319prime-))
(not (Flag318prime-))
(not (Flag316prime-))
(not (Flag315prime-))
(not (Flag314prime-))
(not (Flag313prime-))
(not (Flag312prime-))
(not (Flag311prime-))
(not (Flag310prime-))
(not (Flag309prime-))
(not (Flag308prime-))
(not (Flag307prime-))
(not (Flag306prime-))
(not (Flag305prime-))
(not (Flag304prime-))
(not (Flag302prime-))
(not (Flag301prime-))
(not (Flag300prime-))
(not (Flag299prime-))
(not (Flag298prime-))
(not (Flag297prime-))
(not (Flag296prime-))
(not (Flag295prime-))
(not (Flag294prime-))
(not (Flag293prime-))
(not (Flag292prime-))
(not (Flag291prime-))
(not (Flag289prime-))
(not (Flag288prime-))
(not (Flag287prime-))
(not (Flag286prime-))
(not (Flag285prime-))
(not (Flag284prime-))
(not (Flag283prime-))
(not (Flag282prime-))
(not (Flag281prime-))
(not (Flag280prime-))
(not (Flag279prime-))
(not (Flag278prime-))
(not (Flag277prime-))
(not (Flag276prime-))
(not (Flag275prime-))
(not (Flag273prime-))
(not (Flag272prime-))
(not (Flag271prime-))
(not (Flag270prime-))
(not (Flag269prime-))
(not (Flag268prime-))
(not (Flag267prime-))
(not (Flag266prime-))
(not (Flag265prime-))
(not (Flag264prime-))
(not (Flag263prime-))
(not (Flag262prime-))
(not (Flag261prime-))
(not (Flag260prime-))
(not (Flag259prime-))
(not (Flag257prime-))
(not (Flag256prime-))
(not (Flag255prime-))
(not (Flag254prime-))
(not (Flag253prime-))
(not (Flag252prime-))
(not (Flag251prime-))
(not (Flag250prime-))
(not (Flag249prime-))
(not (Flag248prime-))
(not (Flag247prime-))
(not (Flag246prime-))
(not (Flag245prime-))
(not (Flag244prime-))
(not (Flag243prime-))
(not (Flag242prime-))
(not (Flag241prime-))
(not (Flag240prime-))
(not (Flag239prime-))
(not (Flag238prime-))
(not (Flag237prime-))
(not (Flag236prime-))
(not (Flag235prime-))
(not (Flag234prime-))
(not (Flag233prime-))
(not (Flag232prime-))
(not (Flag231prime-))
(not (Flag230prime-))
(not (Flag229prime-))
(not (Flag228prime-))
(not (Flag227prime-))
(not (Flag226prime-))
(not (Flag225prime-))
(not (Flag224prime-))
(not (Flag223prime-))
(not (Flag222prime-))
(not (Flag221prime-))
(not (Flag220prime-))
(not (Flag219prime-))
(not (Flag218prime-))
(not (Flag217prime-))
(not (Flag216prime-))
(not (Flag215prime-))
(not (Flag214prime-))
(not (Flag211prime-))
(not (Flag210prime-))
(not (Flag209prime-))
(not (Flag208prime-))
(not (Flag207prime-))
(not (Flag206prime-))
(not (Flag205prime-))
(not (Flag204prime-))
(not (Flag203prime-))
(not (Flag202prime-))
(not (Flag201prime-))
(not (Flag200prime-))
(not (Flag199prime-))
(not (Flag4prime-))
(not (Flag390prime-))
(not (Flag388prime-))
(not (Flag385prime-))
(not (Flag380prime-))
(not (Flag374prime-))
(not (Flag368prime-))
(not (Flag361prime-))
(not (Flag352prime-))
(not (Flag341prime-))
(not (Flag330prime-))
(not (Flag317prime-))
(not (Flag303prime-))
(not (Flag290prime-))
(not (Flag274prime-))
(not (Flag258prime-))
(not (Flag197prime-))
(not (Flag196prime-))
(not (Flag193prime-))
(not (Flag192prime-))
(not (Flag190prime-))
(not (Flag189prime-))
(not (Flag188prime-))
(not (Flag186prime-))
(not (Flag185prime-))
(not (Flag184prime-))
(not (Flag183prime-))
(not (Flag182prime-))
(not (Flag180prime-))
(not (Flag179prime-))
(not (Flag178prime-))
(not (Flag177prime-))
(not (Flag176prime-))
(not (Flag174prime-))
(not (Flag173prime-))
(not (Flag172prime-))
(not (Flag171prime-))
(not (Flag170prime-))
(not (Flag169prime-))
(not (Flag168prime-))
(not (Flag166prime-))
(not (Flag165prime-))
(not (Flag164prime-))
(not (Flag163prime-))
(not (Flag162prime-))
(not (Flag161prime-))
(not (Flag160prime-))
(not (Flag158prime-))
(not (Flag157prime-))
(not (Flag156prime-))
(not (Flag155prime-))
(not (Flag154prime-))
(not (Flag153prime-))
(not (Flag152prime-))
(not (Flag151prime-))
(not (Flag150prime-))
(not (Flag148prime-))
(not (Flag147prime-))
(not (Flag146prime-))
(not (Flag145prime-))
(not (Flag144prime-))
(not (Flag143prime-))
(not (Flag142prime-))
(not (Flag141prime-))
(not (Flag140prime-))
(not (Flag139prime-))
(not (Flag137prime-))
(not (Flag136prime-))
(not (Flag135prime-))
(not (Flag134prime-))
(not (Flag133prime-))
(not (Flag132prime-))
(not (Flag131prime-))
(not (Flag130prime-))
(not (Flag129prime-))
(not (Flag128prime-))
(not (Flag127prime-))
(not (Flag126prime-))
(not (Flag124prime-))
(not (Flag123prime-))
(not (Flag122prime-))
(not (Flag121prime-))
(not (Flag120prime-))
(not (Flag119prime-))
(not (Flag118prime-))
(not (Flag117prime-))
(not (Flag116prime-))
(not (Flag115prime-))
(not (Flag114prime-))
(not (Flag112prime-))
(not (Flag111prime-))
(not (Flag110prime-))
(not (Flag109prime-))
(not (Flag108prime-))
(not (Flag107prime-))
(not (Flag106prime-))
(not (Flag105prime-))
(not (Flag104prime-))
(not (Flag103prime-))
(not (Flag102prime-))
(not (Flag101prime-))
(not (Flag100prime-))
(not (Flag99prime-))
(not (Flag97prime-))
(not (Flag96prime-))
(not (Flag95prime-))
(not (Flag94prime-))
(not (Flag93prime-))
(not (Flag92prime-))
(not (Flag91prime-))
(not (Flag90prime-))
(not (Flag89prime-))
(not (Flag88prime-))
(not (Flag87prime-))
(not (Flag86prime-))
(not (Flag85prime-))
(not (Flag84prime-))
(not (Flag83prime-))
(not (Flag81prime-))
(not (Flag80prime-))
(not (Flag79prime-))
(not (Flag78prime-))
(not (Flag77prime-))
(not (Flag76prime-))
(not (Flag75prime-))
(not (Flag74prime-))
(not (Flag73prime-))
(not (Flag72prime-))
(not (Flag71prime-))
(not (Flag70prime-))
(not (Flag69prime-))
(not (Flag68prime-))
(not (Flag66prime-))
(not (Flag65prime-))
(not (Flag64prime-))
(not (Flag63prime-))
(not (Flag62prime-))
(not (Flag61prime-))
(not (Flag60prime-))
(not (Flag59prime-))
(not (Flag58prime-))
(not (Flag57prime-))
(not (Flag56prime-))
(not (Flag55prime-))
(not (Flag54prime-))
(not (Flag53prime-))
(not (Flag52prime-))
(not (Flag51prime-))
(not (Flag49prime-))
(not (Flag48prime-))
(not (Flag47prime-))
(not (Flag46prime-))
(not (Flag45prime-))
(not (Flag44prime-))
(not (Flag43prime-))
(not (Flag42prime-))
(not (Flag41prime-))
(not (Flag40prime-))
(not (Flag39prime-))
(not (Flag38prime-))
(not (Flag37prime-))
(not (Flag36prime-))
(not (Flag35prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag31prime-))
(not (Flag30prime-))
(not (Flag29prime-))
(not (Flag28prime-))
(not (Flag27prime-))
(not (Flag26prime-))
(not (Flag25prime-))
(not (Flag24prime-))
(not (Flag23prime-))
(not (Flag22prime-))
(not (Flag21prime-))
(not (Flag20prime-))
(not (Flag19prime-))
(not (Flag18prime-))
(not (Flag17prime-))
(not (Flag16prime-))
(not (Flag15prime-))
(not (Flag14prime-))
(not (Flag13prime-))
(not (Flag12prime-))
(not (Flag11prime-))
(not (Flag10prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag1prime-))
(not (Flag198prime-))
(not (Flag194prime-))
(not (Flag191prime-))
(not (Flag187prime-))
(not (Flag181prime-))
(not (Flag175prime-))
(not (Flag167prime-))
(not (Flag159prime-))
(not (Flag149prime-))
(not (Flag138prime-))
(not (Flag125prime-))
(not (Flag113prime-))
(not (Flag98prime-))
(not (Flag82prime-))
(not (Flag67prime-))
(not (Flag50prime-))
)
)
(:action Prim1Action-2
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
(Flag1prime-)
(not (Flag1-))
)

)
(:action Prim1Action-3
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
(Flag1prime-)
(not (Flag1-))
)

)
(:action Flag2Action
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
)
:effect
(and
(Flag2-)
(Flag2prime-)
)

)
(:action Prim2Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
(Flag2prime-)
(not (Flag2-))
)

)
(:action Prim2Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
(Flag2prime-)
(not (Flag2-))
)

)
(:action Flag3Action-6
:parameters ()
:precondition
(and
(RIGHTOF2-ROBOT)
)
:effect
(and
(Flag3-)
(Flag3prime-)
)

)
(:action Flag3Action-24
:parameters ()
:precondition
(and
(LEFTOF15-ROBOT)
)
:effect
(and
(Flag3-)
(Flag3prime-)
)

)
(:action Prim4Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
(Flag4prime-)
(not (Flag4-))
)

)
(:action Prim4Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
(Flag4prime-)
(not (Flag4-))
)

)
(:action Flag5Action-5
:parameters ()
:precondition
(and
(RIGHTOF2-ROBOT)
)
:effect
(and
(Flag5-)
(Flag5prime-)
)

)
(:action Flag6Action-4
:parameters ()
:precondition
(and
(RIGHTOF2-ROBOT)
)
:effect
(and
(Flag6-)
(Flag6prime-)
)

)
(:action Flag7Action-3
:parameters ()
:precondition
(and
(RIGHTOF2-ROBOT)
)
:effect
(and
(Flag7-)
(Flag7prime-)
)

)
(:action Flag33Action-0
:parameters ()
:precondition
(and
(LEFTOF15-ROBOT)
)
:effect
(and
(Flag33-)
(Flag33prime-)
)

)
(:action Prim44Action-1
:parameters ()
:precondition
(and
(not (RIGHTOF2-ROBOT))
)
:effect
(and
(Flag44prime-)
(not (Flag44-))
)

)
(:action Prim64Action-0
:parameters ()
:precondition
(and
(not (RIGHTOF2-ROBOT))
)
:effect
(and
(Flag64prime-)
(not (Flag64-))
)

)
(:action Prim81Action-1
:parameters ()
:precondition
(and
(not (RIGHTOF2-ROBOT))
)
:effect
(and
(Flag81prime-)
(not (Flag81-))
)

)
(:action Prim192Action-1
:parameters ()
:precondition
(and
(not (LEFTOF15-ROBOT))
)
:effect
(and
(Flag192prime-)
(not (Flag192-))
)

)
(:action Prim193Action-1
:parameters ()
:precondition
(and
(not (LEFTOF15-ROBOT))
)
:effect
(and
(Flag193prime-)
(not (Flag193-))
)

)
(:action Flag195Action-0
:parameters ()
:precondition
(and
(LEFTOF15-ROBOT)
)
:effect
(and
(Flag195-)
(Flag195prime-)
)

)
(:action Prim196Action-0
:parameters ()
:precondition
(and
(not (LEFTOF15-ROBOT))
)
:effect
(and
(Flag196prime-)
(not (Flag196-))
)

)
(:action Flag212Action-2
:parameters ()
:precondition
(and
(BELOWOF15-ROBOT)
)
:effect
(and
(Flag212-)
(Flag212prime-)
)

)
(:action Flag213Action-2
:parameters ()
:precondition
(and
(BELOWOF15-ROBOT)
)
:effect
(and
(Flag213-)
(Flag213prime-)
)

)
(:action Prim386Action-0
:parameters ()
:precondition
(and
(not (BELOWOF15-ROBOT))
)
:effect
(and
(Flag386prime-)
(not (Flag386-))
)

)
(:action Prim387Action-0
:parameters ()
:precondition
(and
(not (BELOWOF15-ROBOT))
)
:effect
(and
(Flag387prime-)
(not (Flag387-))
)

)
)
