(define (domain grounded-SIMPLE-ADL-ROBOT)
(:requirements
:strips
)
(:predicates
(Flag79-)
(Flag75-)
(Flag47-)
(Flag41-)
(Flag40-)
(Flag39-)
(Flag12-)
(Flag4-)
(Flag2-)
(ROW5-ROBOT)
(ROW4-ROBOT)
(ROW3-ROBOT)
(ROW2-ROBOT)
(ROW1-ROBOT)
(BELOWOF6-ROBOT)
(NOT-BELOWOF5-ROBOT)
(BELOWOF4-ROBOT)
(BELOWOF3-ROBOT)
(BELOWOF2-ROBOT)
(ABOVEOF5-ROBOT)
(ABOVEOF4-ROBOT)
(ABOVEOF3-ROBOT)
(ABOVEOF2-ROBOT)
(ABOVEOF1-ROBOT)
(CHECKCONSISTENCY-)
(ROW0-ROBOT)
(BELOWOF1-ROBOT)
(Flag77-)
(Flag76-)
(Flag37-)
(Flag36-)
(Flag34-)
(Flag33-)
(Flag32-)
(Flag31-)
(Flag29-)
(Flag28-)
(Flag27-)
(Flag26-)
(Flag25-)
(Flag23-)
(Flag22-)
(Flag21-)
(Flag20-)
(Flag19-)
(Flag18-)
(Flag17-)
(Flag16-)
(Flag15-)
(Flag14-)
(Flag13-)
(Flag11-)
(Flag10-)
(Flag9-)
(Flag8-)
(Flag7-)
(Flag6-)
(Flag5-)
(Flag3-)
(ERROR-)
(COLUMN5-ROBOT)
(COLUMN4-ROBOT)
(COLUMN3-ROBOT)
(COLUMN2-ROBOT)
(COLUMN1-ROBOT)
(LEFTOF6-ROBOT)
(NOT-LEFTOF5-ROBOT)
(LEFTOF4-ROBOT)
(LEFTOF3-ROBOT)
(LEFTOF2-ROBOT)
(RIGHTOF5-ROBOT)
(RIGHTOF4-ROBOT)
(RIGHTOF3-ROBOT)
(RIGHTOF1-ROBOT)
(COLUMN0-ROBOT)
(NOT-RIGHTOF2-ROBOT)
(RIGHTOF0-ROBOT)
(LEFTOF1-ROBOT)
(Flag80-)
(Flag78-)
(Flag73-)
(Flag72-)
(Flag70-)
(Flag69-)
(Flag68-)
(Flag67-)
(Flag65-)
(Flag64-)
(Flag63-)
(Flag62-)
(Flag60-)
(Flag59-)
(Flag58-)
(Flag57-)
(Flag56-)
(Flag55-)
(Flag53-)
(Flag52-)
(Flag51-)
(Flag50-)
(Flag49-)
(Flag48-)
(Flag46-)
(Flag45-)
(Flag44-)
(Flag43-)
(Flag42-)
(Flag38-)
(Flag35-)
(Flag30-)
(Flag24-)
(Flag1-)
(Flag81-)
(Flag74-)
(Flag71-)
(Flag66-)
(Flag61-)
(Flag54-)
(NOT-COLUMN4-ROBOT)
(NOT-COLUMN3-ROBOT)
(NOT-COLUMN2-ROBOT)
(NOT-COLUMN1-ROBOT)
(NOT-COLUMN0-ROBOT)
(NOT-RIGHTOF4-ROBOT)
(NOT-RIGHTOF3-ROBOT)
(NOT-RIGHTOF1-ROBOT)
(RIGHTOF2-ROBOT)
(NOT-RIGHTOF0-ROBOT)
(NOT-LEFTOF4-ROBOT)
(NOT-LEFTOF3-ROBOT)
(NOT-LEFTOF2-ROBOT)
(NOT-LEFTOF1-ROBOT)
(NOT-CHECKCONSISTENCY-)
(NOT-COLUMN5-ROBOT)
(NOT-LEFTOF6-ROBOT)
(LEFTOF5-ROBOT)
(NOT-RIGHTOF5-ROBOT)
(NOT-ERROR-)
(NOT-ROW4-ROBOT)
(NOT-ROW3-ROBOT)
(NOT-ROW2-ROBOT)
(NOT-ROW1-ROBOT)
(NOT-ROW0-ROBOT)
(NOT-ABOVEOF4-ROBOT)
(NOT-ABOVEOF3-ROBOT)
(NOT-ABOVEOF2-ROBOT)
(NOT-ABOVEOF1-ROBOT)
(NOT-BELOWOF4-ROBOT)
(NOT-BELOWOF3-ROBOT)
(NOT-BELOWOF2-ROBOT)
(NOT-BELOWOF1-ROBOT)
(NOT-ROW5-ROBOT)
(NOT-BELOWOF6-ROBOT)
(BELOWOF5-ROBOT)
(NOT-ABOVEOF5-ROBOT)
(Flag79prime-)
(Flag75prime-)
(Flag47prime-)
(Flag41prime-)
(Flag40prime-)
(Flag39prime-)
(Flag12prime-)
(Flag4prime-)
(Flag2prime-)
(Flag77prime-)
(Flag76prime-)
(Flag37prime-)
(Flag36prime-)
(Flag34prime-)
(Flag33prime-)
(Flag32prime-)
(Flag31prime-)
(Flag29prime-)
(Flag28prime-)
(Flag27prime-)
(Flag26prime-)
(Flag25prime-)
(Flag23prime-)
(Flag22prime-)
(Flag21prime-)
(Flag20prime-)
(Flag19prime-)
(Flag18prime-)
(Flag17prime-)
(Flag16prime-)
(Flag15prime-)
(Flag14prime-)
(Flag13prime-)
(Flag11prime-)
(Flag10prime-)
(Flag9prime-)
(Flag8prime-)
(Flag7prime-)
(Flag6prime-)
(Flag5prime-)
(Flag3prime-)
(Flag80prime-)
(Flag78prime-)
(Flag73prime-)
(Flag72prime-)
(Flag70prime-)
(Flag69prime-)
(Flag68prime-)
(Flag67prime-)
(Flag65prime-)
(Flag64prime-)
(Flag63prime-)
(Flag62prime-)
(Flag60prime-)
(Flag59prime-)
(Flag58prime-)
(Flag57prime-)
(Flag56prime-)
(Flag55prime-)
(Flag53prime-)
(Flag52prime-)
(Flag51prime-)
(Flag50prime-)
(Flag49prime-)
(Flag48prime-)
(Flag46prime-)
(Flag45prime-)
(Flag44prime-)
(Flag43prime-)
(Flag42prime-)
(Flag38prime-)
(Flag35prime-)
(Flag30prime-)
(Flag24prime-)
(Flag1prime-)
(Flag81prime-)
(Flag74prime-)
(Flag71prime-)
(Flag66prime-)
(Flag61prime-)
(Flag54prime-)
)
(:action Flag54Action-0
:parameters ()
:precondition
(and
(Flag48-)
(Flag48prime-)
)
:effect
(and
(Flag54-)
(Flag54prime-)
)

)
(:action Flag54Action-1
:parameters ()
:precondition
(and
(Flag49-)
(Flag49prime-)
)
:effect
(and
(Flag54-)
(Flag54prime-)
)

)
(:action Flag54Action-2
:parameters ()
:precondition
(and
(Flag50-)
(Flag50prime-)
)
:effect
(and
(Flag54-)
(Flag54prime-)
)

)
(:action Flag54Action-3
:parameters ()
:precondition
(and
(Flag51-)
(Flag51prime-)
)
:effect
(and
(Flag54-)
(Flag54prime-)
)

)
(:action Flag54Action-4
:parameters ()
:precondition
(and
(Flag52-)
(Flag52prime-)
)
:effect
(and
(Flag54-)
(Flag54prime-)
)

)
(:action Flag54Action-5
:parameters ()
:precondition
(and
(Flag53-)
(Flag53prime-)
)
:effect
(and
(Flag54-)
(Flag54prime-)
)

)
(:action Prim54Action
:parameters ()
:precondition
(and
(not (Flag48-))
(Flag48prime-)
(not (Flag49-))
(Flag49prime-)
(not (Flag50-))
(Flag50prime-)
(not (Flag51-))
(Flag51prime-)
(not (Flag52-))
(Flag52prime-)
(not (Flag53-))
(Flag53prime-)
)
:effect
(and
(Flag54prime-)
(not (Flag54-))
)

)
(:action Flag61Action-0
:parameters ()
:precondition
(and
(Flag48-)
(Flag48prime-)
)
:effect
(and
(Flag61-)
(Flag61prime-)
)

)
(:action Flag61Action-1
:parameters ()
:precondition
(and
(Flag55-)
(Flag55prime-)
)
:effect
(and
(Flag61-)
(Flag61prime-)
)

)
(:action Flag61Action-2
:parameters ()
:precondition
(and
(Flag56-)
(Flag56prime-)
)
:effect
(and
(Flag61-)
(Flag61prime-)
)

)
(:action Flag61Action-3
:parameters ()
:precondition
(and
(Flag49-)
(Flag49prime-)
)
:effect
(and
(Flag61-)
(Flag61prime-)
)

)
(:action Flag61Action-4
:parameters ()
:precondition
(and
(Flag50-)
(Flag50prime-)
)
:effect
(and
(Flag61-)
(Flag61prime-)
)

)
(:action Flag61Action-5
:parameters ()
:precondition
(and
(Flag57-)
(Flag57prime-)
)
:effect
(and
(Flag61-)
(Flag61prime-)
)

)
(:action Flag61Action-6
:parameters ()
:precondition
(and
(Flag58-)
(Flag58prime-)
)
:effect
(and
(Flag61-)
(Flag61prime-)
)

)
(:action Flag61Action-7
:parameters ()
:precondition
(and
(Flag52-)
(Flag52prime-)
)
:effect
(and
(Flag61-)
(Flag61prime-)
)

)
(:action Flag61Action-8
:parameters ()
:precondition
(and
(Flag53-)
(Flag53prime-)
)
:effect
(and
(Flag61-)
(Flag61prime-)
)

)
(:action Flag61Action-9
:parameters ()
:precondition
(and
(Flag59-)
(Flag59prime-)
)
:effect
(and
(Flag61-)
(Flag61prime-)
)

)
(:action Flag61Action-10
:parameters ()
:precondition
(and
(Flag60-)
(Flag60prime-)
)
:effect
(and
(Flag61-)
(Flag61prime-)
)

)
(:action Prim61Action
:parameters ()
:precondition
(and
(not (Flag48-))
(Flag48prime-)
(not (Flag55-))
(Flag55prime-)
(not (Flag56-))
(Flag56prime-)
(not (Flag49-))
(Flag49prime-)
(not (Flag50-))
(Flag50prime-)
(not (Flag57-))
(Flag57prime-)
(not (Flag58-))
(Flag58prime-)
(not (Flag52-))
(Flag52prime-)
(not (Flag53-))
(Flag53prime-)
(not (Flag59-))
(Flag59prime-)
(not (Flag60-))
(Flag60prime-)
)
:effect
(and
(Flag61prime-)
(not (Flag61-))
)

)
(:action Flag66Action-0
:parameters ()
:precondition
(and
(Flag48-)
(Flag48prime-)
)
:effect
(and
(Flag66-)
(Flag66prime-)
)

)
(:action Flag66Action-1
:parameters ()
:precondition
(and
(Flag57-)
(Flag57prime-)
)
:effect
(and
(Flag66-)
(Flag66prime-)
)

)
(:action Flag66Action-2
:parameters ()
:precondition
(and
(Flag56-)
(Flag56prime-)
)
:effect
(and
(Flag66-)
(Flag66prime-)
)

)
(:action Flag66Action-3
:parameters ()
:precondition
(and
(Flag50-)
(Flag50prime-)
)
:effect
(and
(Flag66-)
(Flag66prime-)
)

)
(:action Flag66Action-4
:parameters ()
:precondition
(and
(Flag62-)
(Flag62prime-)
)
:effect
(and
(Flag66-)
(Flag66prime-)
)

)
(:action Flag66Action-5
:parameters ()
:precondition
(and
(Flag58-)
(Flag58prime-)
)
:effect
(and
(Flag66-)
(Flag66prime-)
)

)
(:action Flag66Action-6
:parameters ()
:precondition
(and
(Flag52-)
(Flag52prime-)
)
:effect
(and
(Flag66-)
(Flag66prime-)
)

)
(:action Flag66Action-7
:parameters ()
:precondition
(and
(Flag53-)
(Flag53prime-)
)
:effect
(and
(Flag66-)
(Flag66prime-)
)

)
(:action Flag66Action-8
:parameters ()
:precondition
(and
(Flag63-)
(Flag63prime-)
)
:effect
(and
(Flag66-)
(Flag66prime-)
)

)
(:action Flag66Action-9
:parameters ()
:precondition
(and
(Flag59-)
(Flag59prime-)
)
:effect
(and
(Flag66-)
(Flag66prime-)
)

)
(:action Flag66Action-10
:parameters ()
:precondition
(and
(Flag64-)
(Flag64prime-)
)
:effect
(and
(Flag66-)
(Flag66prime-)
)

)
(:action Flag66Action-11
:parameters ()
:precondition
(and
(Flag60-)
(Flag60prime-)
)
:effect
(and
(Flag66-)
(Flag66prime-)
)

)
(:action Flag66Action-12
:parameters ()
:precondition
(and
(Flag65-)
(Flag65prime-)
)
:effect
(and
(Flag66-)
(Flag66prime-)
)

)
(:action Prim66Action
:parameters ()
:precondition
(and
(not (Flag48-))
(Flag48prime-)
(not (Flag57-))
(Flag57prime-)
(not (Flag56-))
(Flag56prime-)
(not (Flag50-))
(Flag50prime-)
(not (Flag62-))
(Flag62prime-)
(not (Flag58-))
(Flag58prime-)
(not (Flag52-))
(Flag52prime-)
(not (Flag53-))
(Flag53prime-)
(not (Flag63-))
(Flag63prime-)
(not (Flag59-))
(Flag59prime-)
(not (Flag64-))
(Flag64prime-)
(not (Flag60-))
(Flag60prime-)
(not (Flag65-))
(Flag65prime-)
)
:effect
(and
(Flag66prime-)
(not (Flag66-))
)

)
(:action Flag71Action-0
:parameters ()
:precondition
(and
(Flag48-)
(Flag48prime-)
)
:effect
(and
(Flag71-)
(Flag71prime-)
)

)
(:action Flag71Action-1
:parameters ()
:precondition
(and
(Flag57-)
(Flag57prime-)
)
:effect
(and
(Flag71-)
(Flag71prime-)
)

)
(:action Flag71Action-2
:parameters ()
:precondition
(and
(Flag50-)
(Flag50prime-)
)
:effect
(and
(Flag71-)
(Flag71prime-)
)

)
(:action Flag71Action-3
:parameters ()
:precondition
(and
(Flag56-)
(Flag56prime-)
)
:effect
(and
(Flag71-)
(Flag71prime-)
)

)
(:action Flag71Action-4
:parameters ()
:precondition
(and
(Flag67-)
(Flag67prime-)
)
:effect
(and
(Flag71-)
(Flag71prime-)
)

)
(:action Flag71Action-5
:parameters ()
:precondition
(and
(Flag68-)
(Flag68prime-)
)
:effect
(and
(Flag71-)
(Flag71prime-)
)

)
(:action Flag71Action-6
:parameters ()
:precondition
(and
(Flag62-)
(Flag62prime-)
)
:effect
(and
(Flag71-)
(Flag71prime-)
)

)
(:action Flag71Action-7
:parameters ()
:precondition
(and
(Flag52-)
(Flag52prime-)
)
:effect
(and
(Flag71-)
(Flag71prime-)
)

)
(:action Flag71Action-8
:parameters ()
:precondition
(and
(Flag63-)
(Flag63prime-)
)
:effect
(and
(Flag71-)
(Flag71prime-)
)

)
(:action Flag71Action-9
:parameters ()
:precondition
(and
(Flag59-)
(Flag59prime-)
)
:effect
(and
(Flag71-)
(Flag71prime-)
)

)
(:action Flag71Action-10
:parameters ()
:precondition
(and
(Flag69-)
(Flag69prime-)
)
:effect
(and
(Flag71-)
(Flag71prime-)
)

)
(:action Flag71Action-11
:parameters ()
:precondition
(and
(Flag70-)
(Flag70prime-)
)
:effect
(and
(Flag71-)
(Flag71prime-)
)

)
(:action Flag71Action-12
:parameters ()
:precondition
(and
(Flag60-)
(Flag60prime-)
)
:effect
(and
(Flag71-)
(Flag71prime-)
)

)
(:action Prim71Action
:parameters ()
:precondition
(and
(not (Flag48-))
(Flag48prime-)
(not (Flag57-))
(Flag57prime-)
(not (Flag50-))
(Flag50prime-)
(not (Flag56-))
(Flag56prime-)
(not (Flag67-))
(Flag67prime-)
(not (Flag68-))
(Flag68prime-)
(not (Flag62-))
(Flag62prime-)
(not (Flag52-))
(Flag52prime-)
(not (Flag63-))
(Flag63prime-)
(not (Flag59-))
(Flag59prime-)
(not (Flag69-))
(Flag69prime-)
(not (Flag70-))
(Flag70prime-)
(not (Flag60-))
(Flag60prime-)
)
:effect
(and
(Flag71prime-)
(not (Flag71-))
)

)
(:action Flag74Action-0
:parameters ()
:precondition
(and
(Flag48-)
(Flag48prime-)
)
:effect
(and
(Flag74-)
(Flag74prime-)
)

)
(:action Flag74Action-1
:parameters ()
:precondition
(and
(Flag57-)
(Flag57prime-)
)
:effect
(and
(Flag74-)
(Flag74prime-)
)

)
(:action Flag74Action-2
:parameters ()
:precondition
(and
(Flag72-)
(Flag72prime-)
)
:effect
(and
(Flag74-)
(Flag74prime-)
)

)
(:action Flag74Action-3
:parameters ()
:precondition
(and
(Flag56-)
(Flag56prime-)
)
:effect
(and
(Flag74-)
(Flag74prime-)
)

)
(:action Flag74Action-4
:parameters ()
:precondition
(and
(Flag68-)
(Flag68prime-)
)
:effect
(and
(Flag74-)
(Flag74prime-)
)

)
(:action Flag74Action-5
:parameters ()
:precondition
(and
(Flag52-)
(Flag52prime-)
)
:effect
(and
(Flag74-)
(Flag74prime-)
)

)
(:action Flag74Action-6
:parameters ()
:precondition
(and
(Flag62-)
(Flag62prime-)
)
:effect
(and
(Flag74-)
(Flag74prime-)
)

)
(:action Flag74Action-7
:parameters ()
:precondition
(and
(Flag63-)
(Flag63prime-)
)
:effect
(and
(Flag74-)
(Flag74prime-)
)

)
(:action Flag74Action-8
:parameters ()
:precondition
(and
(Flag73-)
(Flag73prime-)
)
:effect
(and
(Flag74-)
(Flag74prime-)
)

)
(:action Flag74Action-9
:parameters ()
:precondition
(and
(Flag70-)
(Flag70prime-)
)
:effect
(and
(Flag74-)
(Flag74prime-)
)

)
(:action Flag74Action-10
:parameters ()
:precondition
(and
(Flag60-)
(Flag60prime-)
)
:effect
(and
(Flag74-)
(Flag74prime-)
)

)
(:action Prim74Action
:parameters ()
:precondition
(and
(not (Flag48-))
(Flag48prime-)
(not (Flag57-))
(Flag57prime-)
(not (Flag72-))
(Flag72prime-)
(not (Flag56-))
(Flag56prime-)
(not (Flag68-))
(Flag68prime-)
(not (Flag52-))
(Flag52prime-)
(not (Flag62-))
(Flag62prime-)
(not (Flag63-))
(Flag63prime-)
(not (Flag73-))
(Flag73prime-)
(not (Flag70-))
(Flag70prime-)
(not (Flag60-))
(Flag60prime-)
)
:effect
(and
(Flag74prime-)
(not (Flag74-))
)

)
(:action Flag81Action-0
:parameters ()
:precondition
(and
(Flag56-)
(Flag56prime-)
)
:effect
(and
(Flag81-)
(Flag81prime-)
)

)
(:action Flag81Action-1
:parameters ()
:precondition
(and
(Flag68-)
(Flag68prime-)
)
:effect
(and
(Flag81-)
(Flag81prime-)
)

)
(:action Flag81Action-2
:parameters ()
:precondition
(and
(Flag57-)
(Flag57prime-)
)
:effect
(and
(Flag81-)
(Flag81prime-)
)

)
(:action Flag81Action-3
:parameters ()
:precondition
(and
(Flag62-)
(Flag62prime-)
)
:effect
(and
(Flag81-)
(Flag81prime-)
)

)
(:action Flag81Action-4
:parameters ()
:precondition
(and
(Flag80-)
(Flag80prime-)
)
:effect
(and
(Flag81-)
(Flag81prime-)
)

)
(:action Flag81Action-5
:parameters ()
:precondition
(and
(Flag73-)
(Flag73prime-)
)
:effect
(and
(Flag81-)
(Flag81prime-)
)

)
(:action Prim81Action
:parameters ()
:precondition
(and
(not (Flag56-))
(Flag56prime-)
(not (Flag68-))
(Flag68prime-)
(not (Flag57-))
(Flag57prime-)
(not (Flag62-))
(Flag62prime-)
(not (Flag80-))
(Flag80prime-)
(not (Flag73-))
(Flag73prime-)
)
:effect
(and
(Flag81prime-)
(not (Flag81-))
)

)
(:action Flag1Action
:parameters ()
:precondition
(and
(COLUMN2-ROBOT)
(ROW1-ROBOT)
(NOT-ERROR-)
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(Flag1-)
(Flag1prime-)
)

)
(:action Prim1Action-0
:parameters ()
:precondition
(and
(not (COLUMN2-ROBOT))
)
:effect
(and
(Flag1prime-)
(not (Flag1-))
)

)
(:action Flag2Action-0
:parameters ()
:precondition
(and
(LEFTOF4-ROBOT)
)
:effect
(and
(Flag2-)
(Flag2prime-)
)

)
(:action Flag2Action-1
:parameters ()
:precondition
(and
(LEFTOF3-ROBOT)
)
:effect
(and
(Flag2-)
(Flag2prime-)
)

)
(:action Flag2Action-3
:parameters ()
:precondition
(and
(RIGHTOF1-ROBOT)
)
:effect
(and
(Flag2-)
(Flag2prime-)
)

)
(:action Flag2Action-4
:parameters ()
:precondition
(and
(RIGHTOF4-ROBOT)
)
:effect
(and
(Flag2-)
(Flag2prime-)
)

)
(:action Flag2Action-5
:parameters ()
:precondition
(and
(RIGHTOF0-ROBOT)
)
:effect
(and
(Flag2-)
(Flag2prime-)
)

)
(:action Flag2Action-6
:parameters ()
:precondition
(and
(LEFTOF2-ROBOT)
)
:effect
(and
(Flag2-)
(Flag2prime-)
)

)
(:action Flag2Action-8
:parameters ()
:precondition
(and
(RIGHTOF5-ROBOT)
)
:effect
(and
(Flag2-)
(Flag2prime-)
)

)
(:action Flag2Action-9
:parameters ()
:precondition
(and
(LEFTOF1-ROBOT)
)
:effect
(and
(Flag2-)
(Flag2prime-)
)

)
(:action Flag2Action-10
:parameters ()
:precondition
(and
(LEFTOF6-ROBOT)
)
:effect
(and
(Flag2-)
(Flag2prime-)
)

)
(:action Flag2Action-11
:parameters ()
:precondition
(and
(RIGHTOF3-ROBOT)
)
:effect
(and
(Flag2-)
(Flag2prime-)
)

)
(:action Prim2Action
:parameters ()
:precondition
(and
(not (LEFTOF4-ROBOT))
(not (LEFTOF3-ROBOT))
(not (LEFTOF5-ROBOT))
(not (RIGHTOF1-ROBOT))
(not (RIGHTOF4-ROBOT))
(not (RIGHTOF0-ROBOT))
(not (LEFTOF2-ROBOT))
(not (RIGHTOF2-ROBOT))
(not (RIGHTOF5-ROBOT))
(not (LEFTOF1-ROBOT))
(not (LEFTOF6-ROBOT))
(not (RIGHTOF3-ROBOT))
)
:effect
(and
(Flag2prime-)
(not (Flag2-))
)

)
(:action Flag18Action-0
:parameters ()
:precondition
(and
(Flag13-)
(Flag13prime-)
)
:effect
(and
(Flag18-)
(Flag18prime-)
)

)
(:action Flag18Action-1
:parameters ()
:precondition
(and
(Flag14-)
(Flag14prime-)
)
:effect
(and
(Flag18-)
(Flag18prime-)
)

)
(:action Flag18Action-2
:parameters ()
:precondition
(and
(Flag15-)
(Flag15prime-)
)
:effect
(and
(Flag18-)
(Flag18prime-)
)

)
(:action Flag18Action-3
:parameters ()
:precondition
(and
(Flag16-)
(Flag16prime-)
)
:effect
(and
(Flag18-)
(Flag18prime-)
)

)
(:action Flag18Action-5
:parameters ()
:precondition
(and
(Flag17-)
(Flag17prime-)
)
:effect
(and
(Flag18-)
(Flag18prime-)
)

)
(:action Prim18Action
:parameters ()
:precondition
(and
(not (Flag13-))
(Flag13prime-)
(not (Flag14-))
(Flag14prime-)
(not (Flag15-))
(Flag15prime-)
(not (Flag16-))
(Flag16prime-)
(not (BELOWOF1-ROBOT))
(not (Flag17-))
(Flag17prime-)
)
:effect
(and
(Flag18prime-)
(not (Flag18-))
)

)
(:action Flag24Action-0
:parameters ()
:precondition
(and
(Flag14-)
(Flag14prime-)
)
:effect
(and
(Flag24-)
(Flag24prime-)
)

)
(:action Flag24Action-1
:parameters ()
:precondition
(and
(Flag13-)
(Flag13prime-)
)
:effect
(and
(Flag24-)
(Flag24prime-)
)

)
(:action Flag24Action-2
:parameters ()
:precondition
(and
(Flag19-)
(Flag19prime-)
)
:effect
(and
(Flag24-)
(Flag24prime-)
)

)
(:action Flag24Action-3
:parameters ()
:precondition
(and
(Flag15-)
(Flag15prime-)
)
:effect
(and
(Flag24-)
(Flag24prime-)
)

)
(:action Flag24Action-4
:parameters ()
:precondition
(and
(Flag16-)
(Flag16prime-)
)
:effect
(and
(Flag24-)
(Flag24prime-)
)

)
(:action Flag24Action-5
:parameters ()
:precondition
(and
(Flag20-)
(Flag20prime-)
)
:effect
(and
(Flag24-)
(Flag24prime-)
)

)
(:action Flag24Action-6
:parameters ()
:precondition
(and
(Flag21-)
(Flag21prime-)
)
:effect
(and
(Flag24-)
(Flag24prime-)
)

)
(:action Flag24Action-7
:parameters ()
:precondition
(and
(Flag17-)
(Flag17prime-)
)
:effect
(and
(Flag24-)
(Flag24prime-)
)

)
(:action Flag24Action-8
:parameters ()
:precondition
(and
(Flag22-)
(Flag22prime-)
)
:effect
(and
(Flag24-)
(Flag24prime-)
)

)
(:action Flag24Action-9
:parameters ()
:precondition
(and
(Flag23-)
(Flag23prime-)
)
:effect
(and
(Flag24-)
(Flag24prime-)
)

)
(:action Prim24Action
:parameters ()
:precondition
(and
(not (Flag14-))
(Flag14prime-)
(not (Flag13-))
(Flag13prime-)
(not (Flag19-))
(Flag19prime-)
(not (Flag15-))
(Flag15prime-)
(not (Flag16-))
(Flag16prime-)
(not (Flag20-))
(Flag20prime-)
(not (Flag21-))
(Flag21prime-)
(not (Flag17-))
(Flag17prime-)
(not (Flag22-))
(Flag22prime-)
(not (Flag23-))
(Flag23prime-)
)
:effect
(and
(Flag24prime-)
(not (Flag24-))
)

)
(:action Flag30Action-0
:parameters ()
:precondition
(and
(Flag14-)
(Flag14prime-)
)
:effect
(and
(Flag30-)
(Flag30prime-)
)

)
(:action Flag30Action-1
:parameters ()
:precondition
(and
(Flag25-)
(Flag25prime-)
)
:effect
(and
(Flag30-)
(Flag30prime-)
)

)
(:action Flag30Action-2
:parameters ()
:precondition
(and
(Flag26-)
(Flag26prime-)
)
:effect
(and
(Flag30-)
(Flag30prime-)
)

)
(:action Flag30Action-3
:parameters ()
:precondition
(and
(Flag27-)
(Flag27prime-)
)
:effect
(and
(Flag30-)
(Flag30prime-)
)

)
(:action Flag30Action-4
:parameters ()
:precondition
(and
(Flag16-)
(Flag16prime-)
)
:effect
(and
(Flag30-)
(Flag30prime-)
)

)
(:action Flag30Action-5
:parameters ()
:precondition
(and
(Flag28-)
(Flag28prime-)
)
:effect
(and
(Flag30-)
(Flag30prime-)
)

)
(:action Flag30Action-6
:parameters ()
:precondition
(and
(Flag17-)
(Flag17prime-)
)
:effect
(and
(Flag30-)
(Flag30prime-)
)

)
(:action Flag30Action-7
:parameters ()
:precondition
(and
(Flag21-)
(Flag21prime-)
)
:effect
(and
(Flag30-)
(Flag30prime-)
)

)
(:action Flag30Action-8
:parameters ()
:precondition
(and
(Flag29-)
(Flag29prime-)
)
:effect
(and
(Flag30-)
(Flag30prime-)
)

)
(:action Flag30Action-9
:parameters ()
:precondition
(and
(Flag20-)
(Flag20prime-)
)
:effect
(and
(Flag30-)
(Flag30prime-)
)

)
(:action Flag30Action-10
:parameters ()
:precondition
(and
(Flag22-)
(Flag22prime-)
)
:effect
(and
(Flag30-)
(Flag30prime-)
)

)
(:action Flag30Action-11
:parameters ()
:precondition
(and
(Flag13-)
(Flag13prime-)
)
:effect
(and
(Flag30-)
(Flag30prime-)
)

)
(:action Prim30Action
:parameters ()
:precondition
(and
(not (Flag14-))
(Flag14prime-)
(not (Flag25-))
(Flag25prime-)
(not (Flag26-))
(Flag26prime-)
(not (Flag27-))
(Flag27prime-)
(not (Flag16-))
(Flag16prime-)
(not (Flag28-))
(Flag28prime-)
(not (Flag17-))
(Flag17prime-)
(not (Flag21-))
(Flag21prime-)
(not (Flag29-))
(Flag29prime-)
(not (Flag20-))
(Flag20prime-)
(not (Flag22-))
(Flag22prime-)
(not (Flag13-))
(Flag13prime-)
)
:effect
(and
(Flag30prime-)
(not (Flag30-))
)

)
(:action Flag35Action-0
:parameters ()
:precondition
(and
(Flag13-)
(Flag13prime-)
)
:effect
(and
(Flag35-)
(Flag35prime-)
)

)
(:action Flag35Action-1
:parameters ()
:precondition
(and
(Flag27-)
(Flag27prime-)
)
:effect
(and
(Flag35-)
(Flag35prime-)
)

)
(:action Flag35Action-2
:parameters ()
:precondition
(and
(Flag16-)
(Flag16prime-)
)
:effect
(and
(Flag35-)
(Flag35prime-)
)

)
(:action Flag35Action-3
:parameters ()
:precondition
(and
(Flag31-)
(Flag31prime-)
)
:effect
(and
(Flag35-)
(Flag35prime-)
)

)
(:action Flag35Action-4
:parameters ()
:precondition
(and
(Flag32-)
(Flag32prime-)
)
:effect
(and
(Flag35-)
(Flag35prime-)
)

)
(:action Flag35Action-5
:parameters ()
:precondition
(and
(Flag28-)
(Flag28prime-)
)
:effect
(and
(Flag35-)
(Flag35prime-)
)

)
(:action Flag35Action-6
:parameters ()
:precondition
(and
(Flag17-)
(Flag17prime-)
)
:effect
(and
(Flag35-)
(Flag35prime-)
)

)
(:action Flag35Action-7
:parameters ()
:precondition
(and
(Flag33-)
(Flag33prime-)
)
:effect
(and
(Flag35-)
(Flag35prime-)
)

)
(:action Flag35Action-8
:parameters ()
:precondition
(and
(Flag21-)
(Flag21prime-)
)
:effect
(and
(Flag35-)
(Flag35prime-)
)

)
(:action Flag35Action-9
:parameters ()
:precondition
(and
(Flag20-)
(Flag20prime-)
)
:effect
(and
(Flag35-)
(Flag35prime-)
)

)
(:action Flag35Action-10
:parameters ()
:precondition
(and
(Flag22-)
(Flag22prime-)
)
:effect
(and
(Flag35-)
(Flag35prime-)
)

)
(:action Flag35Action-11
:parameters ()
:precondition
(and
(Flag34-)
(Flag34prime-)
)
:effect
(and
(Flag35-)
(Flag35prime-)
)

)
(:action Prim35Action
:parameters ()
:precondition
(and
(not (Flag13-))
(Flag13prime-)
(not (Flag27-))
(Flag27prime-)
(not (Flag16-))
(Flag16prime-)
(not (Flag31-))
(Flag31prime-)
(not (Flag32-))
(Flag32prime-)
(not (Flag28-))
(Flag28prime-)
(not (Flag17-))
(Flag17prime-)
(not (Flag33-))
(Flag33prime-)
(not (Flag21-))
(Flag21prime-)
(not (Flag20-))
(Flag20prime-)
(not (Flag22-))
(Flag22prime-)
(not (Flag34-))
(Flag34prime-)
)
:effect
(and
(Flag35prime-)
(not (Flag35-))
)

)
(:action Flag38Action-0
:parameters ()
:precondition
(and
(Flag13-)
(Flag13prime-)
)
:effect
(and
(Flag38-)
(Flag38prime-)
)

)
(:action Flag38Action-1
:parameters ()
:precondition
(and
(Flag36-)
(Flag36prime-)
)
:effect
(and
(Flag38-)
(Flag38prime-)
)

)
(:action Flag38Action-2
:parameters ()
:precondition
(and
(Flag27-)
(Flag27prime-)
)
:effect
(and
(Flag38-)
(Flag38prime-)
)

)
(:action Flag38Action-3
:parameters ()
:precondition
(and
(Flag37-)
(Flag37prime-)
)
:effect
(and
(Flag38-)
(Flag38prime-)
)

)
(:action Flag38Action-4
:parameters ()
:precondition
(and
(Flag16-)
(Flag16prime-)
)
:effect
(and
(Flag38-)
(Flag38prime-)
)

)
(:action Flag38Action-5
:parameters ()
:precondition
(and
(Flag28-)
(Flag28prime-)
)
:effect
(and
(Flag38-)
(Flag38prime-)
)

)
(:action Flag38Action-6
:parameters ()
:precondition
(and
(Flag33-)
(Flag33prime-)
)
:effect
(and
(Flag38-)
(Flag38prime-)
)

)
(:action Flag38Action-7
:parameters ()
:precondition
(and
(Flag21-)
(Flag21prime-)
)
:effect
(and
(Flag38-)
(Flag38prime-)
)

)
(:action Flag38Action-8
:parameters ()
:precondition
(and
(Flag20-)
(Flag20prime-)
)
:effect
(and
(Flag38-)
(Flag38prime-)
)

)
(:action Flag38Action-9
:parameters ()
:precondition
(and
(Flag32-)
(Flag32prime-)
)
:effect
(and
(Flag38-)
(Flag38prime-)
)

)
(:action Prim38Action
:parameters ()
:precondition
(and
(not (Flag13-))
(Flag13prime-)
(not (Flag36-))
(Flag36prime-)
(not (Flag27-))
(Flag27prime-)
(not (Flag37-))
(Flag37prime-)
(not (Flag16-))
(Flag16prime-)
(not (Flag28-))
(Flag28prime-)
(not (Flag33-))
(Flag33prime-)
(not (Flag21-))
(Flag21prime-)
(not (Flag20-))
(Flag20prime-)
(not (Flag32-))
(Flag32prime-)
)
:effect
(and
(Flag38prime-)
(not (Flag38-))
)

)
(:action Flag39Action-0
:parameters ()
:precondition
(and
(RIGHTOF3-ROBOT)
)
:effect
(and
(Flag39-)
(Flag39prime-)
)

)
(:action Flag39Action-1
:parameters ()
:precondition
(and
(RIGHTOF5-ROBOT)
)
:effect
(and
(Flag39-)
(Flag39prime-)
)

)
(:action Flag39Action-3
:parameters ()
:precondition
(and
(RIGHTOF0-ROBOT)
)
:effect
(and
(Flag39-)
(Flag39prime-)
)

)
(:action Flag39Action-4
:parameters ()
:precondition
(and
(RIGHTOF4-ROBOT)
)
:effect
(and
(Flag39-)
(Flag39prime-)
)

)
(:action Flag39Action-5
:parameters ()
:precondition
(and
(RIGHTOF1-ROBOT)
)
:effect
(and
(Flag39-)
(Flag39prime-)
)

)
(:action Prim39Action
:parameters ()
:precondition
(and
(not (RIGHTOF3-ROBOT))
(not (RIGHTOF5-ROBOT))
(not (RIGHTOF2-ROBOT))
(not (RIGHTOF0-ROBOT))
(not (RIGHTOF4-ROBOT))
(not (RIGHTOF1-ROBOT))
)
:effect
(and
(Flag39prime-)
(not (Flag39-))
)

)
(:action Flag40Action-0
:parameters ()
:precondition
(and
(RIGHTOF3-ROBOT)
)
:effect
(and
(Flag40-)
(Flag40prime-)
)

)
(:action Flag40Action-1
:parameters ()
:precondition
(and
(RIGHTOF1-ROBOT)
)
:effect
(and
(Flag40-)
(Flag40prime-)
)

)
(:action Flag40Action-2
:parameters ()
:precondition
(and
(RIGHTOF4-ROBOT)
)
:effect
(and
(Flag40-)
(Flag40prime-)
)

)
(:action Flag40Action-3
:parameters ()
:precondition
(and
(RIGHTOF5-ROBOT)
)
:effect
(and
(Flag40-)
(Flag40prime-)
)

)
(:action Prim40Action
:parameters ()
:precondition
(and
(not (RIGHTOF3-ROBOT))
(not (RIGHTOF1-ROBOT))
(not (RIGHTOF4-ROBOT))
(not (RIGHTOF5-ROBOT))
(not (RIGHTOF2-ROBOT))
)
:effect
(and
(Flag40prime-)
(not (Flag40-))
)

)
(:action Flag41Action-0
:parameters ()
:precondition
(and
(RIGHTOF3-ROBOT)
)
:effect
(and
(Flag41-)
(Flag41prime-)
)

)
(:action Flag41Action-1
:parameters ()
:precondition
(and
(RIGHTOF4-ROBOT)
)
:effect
(and
(Flag41-)
(Flag41prime-)
)

)
(:action Flag41Action-2
:parameters ()
:precondition
(and
(RIGHTOF5-ROBOT)
)
:effect
(and
(Flag41-)
(Flag41prime-)
)

)
(:action Prim41Action
:parameters ()
:precondition
(and
(not (RIGHTOF3-ROBOT))
(not (RIGHTOF4-ROBOT))
(not (RIGHTOF5-ROBOT))
(not (RIGHTOF2-ROBOT))
)
:effect
(and
(Flag41prime-)
(not (Flag41-))
)

)
(:action Flag42Action-0
:parameters ()
:precondition
(and
(RIGHTOF3-ROBOT)
)
:effect
(and
(Flag42-)
(Flag42prime-)
)

)
(:action Flag42Action-1
:parameters ()
:precondition
(and
(RIGHTOF4-ROBOT)
)
:effect
(and
(Flag42-)
(Flag42prime-)
)

)
(:action Flag42Action-2
:parameters ()
:precondition
(and
(RIGHTOF5-ROBOT)
)
:effect
(and
(Flag42-)
(Flag42prime-)
)

)
(:action Prim42Action
:parameters ()
:precondition
(and
(not (RIGHTOF3-ROBOT))
(not (RIGHTOF4-ROBOT))
(not (RIGHTOF5-ROBOT))
)
:effect
(and
(Flag42prime-)
(not (Flag42-))
)

)
(:action Flag43Action-0
:parameters ()
:precondition
(and
(RIGHTOF4-ROBOT)
)
:effect
(and
(Flag43-)
(Flag43prime-)
)

)
(:action Flag43Action-1
:parameters ()
:precondition
(and
(RIGHTOF5-ROBOT)
)
:effect
(and
(Flag43-)
(Flag43prime-)
)

)
(:action Prim43Action
:parameters ()
:precondition
(and
(not (RIGHTOF4-ROBOT))
(not (RIGHTOF5-ROBOT))
)
:effect
(and
(Flag43prime-)
(not (Flag43-))
)

)
(:action Flag44Action-0
:parameters ()
:precondition
(and
(LEFTOF1-ROBOT)
)
:effect
(and
(Flag44-)
(Flag44prime-)
)

)
(:action Flag44Action-1
:parameters ()
:precondition
(and
(LEFTOF2-ROBOT)
)
:effect
(and
(Flag44-)
(Flag44prime-)
)

)
(:action Prim44Action
:parameters ()
:precondition
(and
(not (LEFTOF1-ROBOT))
(not (LEFTOF2-ROBOT))
)
:effect
(and
(Flag44prime-)
(not (Flag44-))
)

)
(:action Flag45Action-0
:parameters ()
:precondition
(and
(LEFTOF1-ROBOT)
)
:effect
(and
(Flag45-)
(Flag45prime-)
)

)
(:action Flag45Action-1
:parameters ()
:precondition
(and
(LEFTOF3-ROBOT)
)
:effect
(and
(Flag45-)
(Flag45prime-)
)

)
(:action Flag45Action-2
:parameters ()
:precondition
(and
(LEFTOF2-ROBOT)
)
:effect
(and
(Flag45-)
(Flag45prime-)
)

)
(:action Prim45Action
:parameters ()
:precondition
(and
(not (LEFTOF1-ROBOT))
(not (LEFTOF3-ROBOT))
(not (LEFTOF2-ROBOT))
)
:effect
(and
(Flag45prime-)
(not (Flag45-))
)

)
(:action Flag46Action-0
:parameters ()
:precondition
(and
(LEFTOF1-ROBOT)
)
:effect
(and
(Flag46-)
(Flag46prime-)
)

)
(:action Flag46Action-1
:parameters ()
:precondition
(and
(LEFTOF3-ROBOT)
)
:effect
(and
(Flag46-)
(Flag46prime-)
)

)
(:action Flag46Action-2
:parameters ()
:precondition
(and
(LEFTOF4-ROBOT)
)
:effect
(and
(Flag46-)
(Flag46prime-)
)

)
(:action Flag46Action-3
:parameters ()
:precondition
(and
(LEFTOF2-ROBOT)
)
:effect
(and
(Flag46-)
(Flag46prime-)
)

)
(:action Prim46Action
:parameters ()
:precondition
(and
(not (LEFTOF1-ROBOT))
(not (LEFTOF3-ROBOT))
(not (LEFTOF4-ROBOT))
(not (LEFTOF2-ROBOT))
)
:effect
(and
(Flag46prime-)
(not (Flag46-))
)

)
(:action Flag47Action-1
:parameters ()
:precondition
(and
(LEFTOF1-ROBOT)
)
:effect
(and
(Flag47-)
(Flag47prime-)
)

)
(:action Flag47Action-2
:parameters ()
:precondition
(and
(LEFTOF3-ROBOT)
)
:effect
(and
(Flag47-)
(Flag47prime-)
)

)
(:action Flag47Action-3
:parameters ()
:precondition
(and
(LEFTOF4-ROBOT)
)
:effect
(and
(Flag47-)
(Flag47prime-)
)

)
(:action Flag47Action-4
:parameters ()
:precondition
(and
(LEFTOF2-ROBOT)
)
:effect
(and
(Flag47-)
(Flag47prime-)
)

)
(:action Prim47Action
:parameters ()
:precondition
(and
(not (LEFTOF5-ROBOT))
(not (LEFTOF1-ROBOT))
(not (LEFTOF3-ROBOT))
(not (LEFTOF4-ROBOT))
(not (LEFTOF2-ROBOT))
)
:effect
(and
(Flag47prime-)
(not (Flag47-))
)

)
(:action Flag48Action
:parameters ()
:precondition
(and
(LEFTOF1-ROBOT)
(RIGHTOF4-ROBOT)
)
:effect
(and
(Flag48-)
(Flag48prime-)
)

)
(:action Flag49Action
:parameters ()
:precondition
(and
(LEFTOF1-ROBOT)
(RIGHTOF1-ROBOT)
)
:effect
(and
(Flag49-)
(Flag49prime-)
)

)
(:action Flag50Action
:parameters ()
:precondition
(and
(RIGHTOF3-ROBOT)
(LEFTOF1-ROBOT)
)
:effect
(and
(Flag50-)
(Flag50prime-)
)

)
(:action Flag51Action
:parameters ()
:precondition
(and
(RIGHTOF0-ROBOT)
(LEFTOF1-ROBOT)
)
:effect
(and
(Flag51-)
(Flag51prime-)
)

)
(:action Flag52Action
:parameters ()
:precondition
(and
(LEFTOF1-ROBOT)
(RIGHTOF5-ROBOT)
)
:effect
(and
(Flag52-)
(Flag52prime-)
)

)
(:action Flag53Action
:parameters ()
:precondition
(and
(LEFTOF1-ROBOT)
(RIGHTOF2-ROBOT)
)
:effect
(and
(Flag53-)
(Flag53prime-)
)

)
(:action Prim48Action-0
:parameters ()
:precondition
(and
(not (LEFTOF1-ROBOT))
)
:effect
(and
(Flag48prime-)
(not (Flag48-))
)

)
(:action Prim48Action-1
:parameters ()
:precondition
(and
(not (RIGHTOF4-ROBOT))
)
:effect
(and
(Flag48prime-)
(not (Flag48-))
)

)
(:action Prim49Action-0
:parameters ()
:precondition
(and
(not (LEFTOF1-ROBOT))
)
:effect
(and
(Flag49prime-)
(not (Flag49-))
)

)
(:action Prim49Action-1
:parameters ()
:precondition
(and
(not (RIGHTOF1-ROBOT))
)
:effect
(and
(Flag49prime-)
(not (Flag49-))
)

)
(:action Prim50Action-0
:parameters ()
:precondition
(and
(not (RIGHTOF3-ROBOT))
)
:effect
(and
(Flag50prime-)
(not (Flag50-))
)

)
(:action Prim50Action-1
:parameters ()
:precondition
(and
(not (LEFTOF1-ROBOT))
)
:effect
(and
(Flag50prime-)
(not (Flag50-))
)

)
(:action Prim51Action-0
:parameters ()
:precondition
(and
(not (RIGHTOF0-ROBOT))
)
:effect
(and
(Flag51prime-)
(not (Flag51-))
)

)
(:action Prim51Action-1
:parameters ()
:precondition
(and
(not (LEFTOF1-ROBOT))
)
:effect
(and
(Flag51prime-)
(not (Flag51-))
)

)
(:action Prim52Action-0
:parameters ()
:precondition
(and
(not (LEFTOF1-ROBOT))
)
:effect
(and
(Flag52prime-)
(not (Flag52-))
)

)
(:action Prim52Action-1
:parameters ()
:precondition
(and
(not (RIGHTOF5-ROBOT))
)
:effect
(and
(Flag52prime-)
(not (Flag52-))
)

)
(:action Prim53Action-0
:parameters ()
:precondition
(and
(not (LEFTOF1-ROBOT))
)
:effect
(and
(Flag53prime-)
(not (Flag53-))
)

)
(:action Flag55Action
:parameters ()
:precondition
(and
(LEFTOF2-ROBOT)
(RIGHTOF1-ROBOT)
)
:effect
(and
(Flag55-)
(Flag55prime-)
)

)
(:action Flag56Action
:parameters ()
:precondition
(and
(RIGHTOF5-ROBOT)
(LEFTOF2-ROBOT)
)
:effect
(and
(Flag56-)
(Flag56prime-)
)

)
(:action Flag57Action
:parameters ()
:precondition
(and
(RIGHTOF5-ROBOT)
(LEFTOF1-ROBOT)
)
:effect
(and
(Flag57-)
(Flag57prime-)
)

)
(:action Flag58Action
:parameters ()
:precondition
(and
(RIGHTOF2-ROBOT)
(LEFTOF2-ROBOT)
)
:effect
(and
(Flag58-)
(Flag58prime-)
)

)
(:action Flag59Action
:parameters ()
:precondition
(and
(RIGHTOF3-ROBOT)
(LEFTOF2-ROBOT)
)
:effect
(and
(Flag59-)
(Flag59prime-)
)

)
(:action Flag60Action
:parameters ()
:precondition
(and
(LEFTOF2-ROBOT)
(RIGHTOF4-ROBOT)
)
:effect
(and
(Flag60-)
(Flag60prime-)
)

)
(:action Prim55Action-0
:parameters ()
:precondition
(and
(not (LEFTOF2-ROBOT))
)
:effect
(and
(Flag55prime-)
(not (Flag55-))
)

)
(:action Prim55Action-1
:parameters ()
:precondition
(and
(not (RIGHTOF1-ROBOT))
)
:effect
(and
(Flag55prime-)
(not (Flag55-))
)

)
(:action Prim56Action-0
:parameters ()
:precondition
(and
(not (RIGHTOF5-ROBOT))
)
:effect
(and
(Flag56prime-)
(not (Flag56-))
)

)
(:action Prim56Action-1
:parameters ()
:precondition
(and
(not (LEFTOF2-ROBOT))
)
:effect
(and
(Flag56prime-)
(not (Flag56-))
)

)
(:action Prim57Action-0
:parameters ()
:precondition
(and
(not (RIGHTOF5-ROBOT))
)
:effect
(and
(Flag57prime-)
(not (Flag57-))
)

)
(:action Prim57Action-1
:parameters ()
:precondition
(and
(not (LEFTOF1-ROBOT))
)
:effect
(and
(Flag57prime-)
(not (Flag57-))
)

)
(:action Prim58Action-1
:parameters ()
:precondition
(and
(not (LEFTOF2-ROBOT))
)
:effect
(and
(Flag58prime-)
(not (Flag58-))
)

)
(:action Prim59Action-0
:parameters ()
:precondition
(and
(not (RIGHTOF3-ROBOT))
)
:effect
(and
(Flag59prime-)
(not (Flag59-))
)

)
(:action Prim59Action-1
:parameters ()
:precondition
(and
(not (LEFTOF2-ROBOT))
)
:effect
(and
(Flag59prime-)
(not (Flag59-))
)

)
(:action Prim60Action-0
:parameters ()
:precondition
(and
(not (LEFTOF2-ROBOT))
)
:effect
(and
(Flag60prime-)
(not (Flag60-))
)

)
(:action Prim60Action-1
:parameters ()
:precondition
(and
(not (RIGHTOF4-ROBOT))
)
:effect
(and
(Flag60prime-)
(not (Flag60-))
)

)
(:action Flag62Action
:parameters ()
:precondition
(and
(LEFTOF3-ROBOT)
(RIGHTOF5-ROBOT)
)
:effect
(and
(Flag62-)
(Flag62prime-)
)

)
(:action Flag63Action
:parameters ()
:precondition
(and
(LEFTOF3-ROBOT)
(RIGHTOF4-ROBOT)
)
:effect
(and
(Flag63-)
(Flag63prime-)
)

)
(:action Flag64Action
:parameters ()
:precondition
(and
(RIGHTOF3-ROBOT)
(LEFTOF3-ROBOT)
)
:effect
(and
(Flag64-)
(Flag64prime-)
)

)
(:action Flag65Action
:parameters ()
:precondition
(and
(LEFTOF3-ROBOT)
(RIGHTOF2-ROBOT)
)
:effect
(and
(Flag65-)
(Flag65prime-)
)

)
(:action Prim62Action-0
:parameters ()
:precondition
(and
(not (LEFTOF3-ROBOT))
)
:effect
(and
(Flag62prime-)
(not (Flag62-))
)

)
(:action Prim62Action-1
:parameters ()
:precondition
(and
(not (RIGHTOF5-ROBOT))
)
:effect
(and
(Flag62prime-)
(not (Flag62-))
)

)
(:action Prim63Action-0
:parameters ()
:precondition
(and
(not (LEFTOF3-ROBOT))
)
:effect
(and
(Flag63prime-)
(not (Flag63-))
)

)
(:action Prim63Action-1
:parameters ()
:precondition
(and
(not (RIGHTOF4-ROBOT))
)
:effect
(and
(Flag63prime-)
(not (Flag63-))
)

)
(:action Prim64Action-0
:parameters ()
:precondition
(and
(not (RIGHTOF3-ROBOT))
)
:effect
(and
(Flag64prime-)
(not (Flag64-))
)

)
(:action Prim64Action-1
:parameters ()
:precondition
(and
(not (LEFTOF3-ROBOT))
)
:effect
(and
(Flag64prime-)
(not (Flag64-))
)

)
(:action Prim65Action-0
:parameters ()
:precondition
(and
(not (LEFTOF3-ROBOT))
)
:effect
(and
(Flag65prime-)
(not (Flag65-))
)

)
(:action Flag67Action
:parameters ()
:precondition
(and
(LEFTOF3-ROBOT)
(RIGHTOF3-ROBOT)
)
:effect
(and
(Flag67-)
(Flag67prime-)
)

)
(:action Flag68Action
:parameters ()
:precondition
(and
(LEFTOF4-ROBOT)
(RIGHTOF5-ROBOT)
)
:effect
(and
(Flag68-)
(Flag68prime-)
)

)
(:action Flag69Action
:parameters ()
:precondition
(and
(RIGHTOF3-ROBOT)
(LEFTOF4-ROBOT)
)
:effect
(and
(Flag69-)
(Flag69prime-)
)

)
(:action Flag70Action
:parameters ()
:precondition
(and
(LEFTOF4-ROBOT)
(RIGHTOF4-ROBOT)
)
:effect
(and
(Flag70-)
(Flag70prime-)
)

)
(:action Prim67Action-0
:parameters ()
:precondition
(and
(not (LEFTOF3-ROBOT))
)
:effect
(and
(Flag67prime-)
(not (Flag67-))
)

)
(:action Prim67Action-1
:parameters ()
:precondition
(and
(not (RIGHTOF3-ROBOT))
)
:effect
(and
(Flag67prime-)
(not (Flag67-))
)

)
(:action Prim68Action-0
:parameters ()
:precondition
(and
(not (LEFTOF4-ROBOT))
)
:effect
(and
(Flag68prime-)
(not (Flag68-))
)

)
(:action Prim68Action-1
:parameters ()
:precondition
(and
(not (RIGHTOF5-ROBOT))
)
:effect
(and
(Flag68prime-)
(not (Flag68-))
)

)
(:action Prim69Action-0
:parameters ()
:precondition
(and
(not (RIGHTOF3-ROBOT))
)
:effect
(and
(Flag69prime-)
(not (Flag69-))
)

)
(:action Prim69Action-1
:parameters ()
:precondition
(and
(not (LEFTOF4-ROBOT))
)
:effect
(and
(Flag69prime-)
(not (Flag69-))
)

)
(:action Prim70Action-0
:parameters ()
:precondition
(and
(not (LEFTOF4-ROBOT))
)
:effect
(and
(Flag70prime-)
(not (Flag70-))
)

)
(:action Prim70Action-1
:parameters ()
:precondition
(and
(not (RIGHTOF4-ROBOT))
)
:effect
(and
(Flag70prime-)
(not (Flag70-))
)

)
(:action Flag72Action
:parameters ()
:precondition
(and
(RIGHTOF4-ROBOT)
(LEFTOF5-ROBOT)
)
:effect
(and
(Flag72-)
(Flag72prime-)
)

)
(:action Flag73Action
:parameters ()
:precondition
(and
(RIGHTOF5-ROBOT)
(LEFTOF5-ROBOT)
)
:effect
(and
(Flag73-)
(Flag73prime-)
)

)
(:action Prim72Action-0
:parameters ()
:precondition
(and
(not (RIGHTOF4-ROBOT))
)
:effect
(and
(Flag72prime-)
(not (Flag72-))
)

)
(:action Prim73Action-0
:parameters ()
:precondition
(and
(not (RIGHTOF5-ROBOT))
)
:effect
(and
(Flag73prime-)
(not (Flag73-))
)

)
(:action Flag78Action-0
:parameters ()
:precondition
(and
(Flag76-)
(Flag76prime-)
)
:effect
(and
(Flag78-)
(Flag78prime-)
)

)
(:action Flag78Action-1
:parameters ()
:precondition
(and
(Flag27-)
(Flag27prime-)
)
:effect
(and
(Flag78-)
(Flag78prime-)
)

)
(:action Flag78Action-2
:parameters ()
:precondition
(and
(Flag16-)
(Flag16prime-)
)
:effect
(and
(Flag78-)
(Flag78prime-)
)

)
(:action Flag78Action-3
:parameters ()
:precondition
(and
(Flag77-)
(Flag77prime-)
)
:effect
(and
(Flag78-)
(Flag78prime-)
)

)
(:action Flag78Action-4
:parameters ()
:precondition
(and
(Flag21-)
(Flag21prime-)
)
:effect
(and
(Flag78-)
(Flag78prime-)
)

)
(:action Flag78Action-5
:parameters ()
:precondition
(and
(Flag32-)
(Flag32prime-)
)
:effect
(and
(Flag78-)
(Flag78prime-)
)

)
(:action Prim78Action
:parameters ()
:precondition
(and
(not (Flag76-))
(Flag76prime-)
(not (Flag27-))
(Flag27prime-)
(not (Flag16-))
(Flag16prime-)
(not (Flag77-))
(Flag77prime-)
(not (Flag21-))
(Flag21prime-)
(not (Flag32-))
(Flag32prime-)
)
:effect
(and
(Flag78prime-)
(not (Flag78-))
)

)
(:action Flag79Action-0
:parameters ()
:precondition
(and
(LEFTOF2-ROBOT)
)
:effect
(and
(Flag79-)
(Flag79prime-)
)

)
(:action Flag79Action-1
:parameters ()
:precondition
(and
(LEFTOF1-ROBOT)
)
:effect
(and
(Flag79-)
(Flag79prime-)
)

)
(:action Flag79Action-2
:parameters ()
:precondition
(and
(LEFTOF6-ROBOT)
)
:effect
(and
(Flag79-)
(Flag79prime-)
)

)
(:action Flag79Action-4
:parameters ()
:precondition
(and
(LEFTOF3-ROBOT)
)
:effect
(and
(Flag79-)
(Flag79prime-)
)

)
(:action Flag79Action-5
:parameters ()
:precondition
(and
(LEFTOF4-ROBOT)
)
:effect
(and
(Flag79-)
(Flag79prime-)
)

)
(:action Prim79Action
:parameters ()
:precondition
(and
(not (LEFTOF2-ROBOT))
(not (LEFTOF1-ROBOT))
(not (LEFTOF6-ROBOT))
(not (LEFTOF5-ROBOT))
(not (LEFTOF3-ROBOT))
(not (LEFTOF4-ROBOT))
)
:effect
(and
(Flag79prime-)
(not (Flag79-))
)

)
(:action Flag80Action
:parameters ()
:precondition
(and
(RIGHTOF5-ROBOT)
(LEFTOF6-ROBOT)
)
:effect
(and
(Flag80-)
(Flag80prime-)
)

)
(:action Prim80Action-0
:parameters ()
:precondition
(and
(not (RIGHTOF5-ROBOT))
)
:effect
(and
(Flag80prime-)
(not (Flag80-))
)

)
(:action Prim80Action-1
:parameters ()
:precondition
(and
(not (LEFTOF6-ROBOT))
)
:effect
(and
(Flag80prime-)
(not (Flag80-))
)

)
(:action MOVELEFT-ROBOT
:parameters ()
:precondition
(and
(Flag3-)
(Flag3prime-)
(Flag81prime-)
(Flag74prime-)
(Flag71prime-)
(Flag66prime-)
(Flag61prime-)
(Flag79prime-)
)
:effect
(and
(when
(and
(Flag81-)
)
(and
(COLUMN4-ROBOT)
(not (NOT-COLUMN4-ROBOT))
)
)
(when
(and
(Flag74-)
)
(and
(COLUMN3-ROBOT)
(not (NOT-COLUMN3-ROBOT))
)
)
(when
(and
(Flag71-)
)
(and
(COLUMN2-ROBOT)
(not (NOT-COLUMN2-ROBOT))
)
)
(when
(and
(Flag66-)
)
(and
(COLUMN1-ROBOT)
(not (NOT-COLUMN1-ROBOT))
)
)
(when
(and
(Flag61-)
)
(and
(COLUMN0-ROBOT)
(not (NOT-COLUMN0-ROBOT))
)
)
(when
(and
(COLUMN5-ROBOT)
)
(and
(COLUMN4-ROBOT)
(NOT-COLUMN5-ROBOT)
(not (NOT-COLUMN4-ROBOT))
(not (COLUMN5-ROBOT))
)
)
(when
(and
(COLUMN4-ROBOT)
)
(and
(COLUMN3-ROBOT)
(NOT-COLUMN4-ROBOT)
(not (NOT-COLUMN3-ROBOT))
(not (COLUMN4-ROBOT))
)
)
(when
(and
(COLUMN3-ROBOT)
)
(and
(COLUMN2-ROBOT)
(NOT-COLUMN3-ROBOT)
(not (NOT-COLUMN2-ROBOT))
(not (COLUMN3-ROBOT))
)
)
(when
(and
(COLUMN2-ROBOT)
)
(and
(COLUMN1-ROBOT)
(NOT-COLUMN2-ROBOT)
(not (NOT-COLUMN1-ROBOT))
(not (COLUMN2-ROBOT))
)
)
(when
(and
(COLUMN1-ROBOT)
)
(and
(COLUMN0-ROBOT)
(NOT-COLUMN1-ROBOT)
(not (NOT-COLUMN0-ROBOT))
(not (COLUMN1-ROBOT))
)
)
(when
(and
(RIGHTOF5-ROBOT)
)
(and
(RIGHTOF4-ROBOT)
(NOT-RIGHTOF5-ROBOT)
(RIGHTOF3-ROBOT)
(NOT-RIGHTOF4-ROBOT)
(RIGHTOF2-ROBOT)
(NOT-RIGHTOF3-ROBOT)
(RIGHTOF1-ROBOT)
(NOT-RIGHTOF2-ROBOT)
(RIGHTOF0-ROBOT)
(NOT-RIGHTOF1-ROBOT)
(not (RIGHTOF5-ROBOT))
(not (NOT-RIGHTOF0-ROBOT))
)
)
(when
(and
(RIGHTOF4-ROBOT)
)
(and
(RIGHTOF3-ROBOT)
(NOT-RIGHTOF4-ROBOT)
(RIGHTOF2-ROBOT)
(NOT-RIGHTOF3-ROBOT)
(RIGHTOF1-ROBOT)
(NOT-RIGHTOF2-ROBOT)
(RIGHTOF0-ROBOT)
(NOT-RIGHTOF1-ROBOT)
(not (RIGHTOF4-ROBOT))
(not (NOT-RIGHTOF0-ROBOT))
)
)
(when
(and
(RIGHTOF3-ROBOT)
)
(and
(RIGHTOF2-ROBOT)
(NOT-RIGHTOF3-ROBOT)
(RIGHTOF1-ROBOT)
(NOT-RIGHTOF2-ROBOT)
(RIGHTOF0-ROBOT)
(NOT-RIGHTOF1-ROBOT)
(not (RIGHTOF3-ROBOT))
(not (NOT-RIGHTOF0-ROBOT))
)
)
(when
(and
(RIGHTOF2-ROBOT)
)
(and
(RIGHTOF1-ROBOT)
(NOT-RIGHTOF2-ROBOT)
(RIGHTOF0-ROBOT)
(NOT-RIGHTOF1-ROBOT)
(not (RIGHTOF2-ROBOT))
(not (NOT-RIGHTOF0-ROBOT))
)
)
(when
(and
(RIGHTOF1-ROBOT)
)
(and
(RIGHTOF0-ROBOT)
(NOT-RIGHTOF1-ROBOT)
(not (NOT-RIGHTOF0-ROBOT))
(not (RIGHTOF1-ROBOT))
)
)
(when
(and
(Flag79-)
)
(and
(LEFTOF5-ROBOT)
(not (NOT-LEFTOF5-ROBOT))
)
)
(when
(and
(LEFTOF2-ROBOT)
)
(and
(LEFTOF4-ROBOT)
(LEFTOF3-ROBOT)
(LEFTOF1-ROBOT)
(not (NOT-LEFTOF4-ROBOT))
(not (NOT-LEFTOF3-ROBOT))
(not (NOT-LEFTOF2-ROBOT))
(not (NOT-LEFTOF1-ROBOT))
)
)
(when
(and
(LEFTOF4-ROBOT)
)
(and
(LEFTOF3-ROBOT)
(not (NOT-LEFTOF4-ROBOT))
(not (NOT-LEFTOF3-ROBOT))
)
)
(when
(and
(LEFTOF3-ROBOT)
)
(and
(LEFTOF4-ROBOT)
(LEFTOF2-ROBOT)
(not (NOT-LEFTOF4-ROBOT))
(not (NOT-LEFTOF3-ROBOT))
(not (NOT-LEFTOF2-ROBOT))
)
)
(when
(and
(LEFTOF1-ROBOT)
)
(and
(LEFTOF4-ROBOT)
(LEFTOF3-ROBOT)
(LEFTOF2-ROBOT)
(not (NOT-LEFTOF4-ROBOT))
(not (NOT-LEFTOF3-ROBOT))
(not (NOT-LEFTOF2-ROBOT))
(not (NOT-LEFTOF1-ROBOT))
)
)
(when
(and
(LEFTOF5-ROBOT)
)
(and
(LEFTOF4-ROBOT)
(not (NOT-LEFTOF4-ROBOT))
)
)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag79prime-))
(not (Flag75prime-))
(not (Flag47prime-))
(not (Flag41prime-))
(not (Flag40prime-))
(not (Flag39prime-))
(not (Flag12prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag77prime-))
(not (Flag76prime-))
(not (Flag37prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag33prime-))
(not (Flag32prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag28prime-))
(not (Flag27prime-))
(not (Flag26prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag22prime-))
(not (Flag21prime-))
(not (Flag20prime-))
(not (Flag19prime-))
(not (Flag18prime-))
(not (Flag17prime-))
(not (Flag16prime-))
(not (Flag15prime-))
(not (Flag14prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag10prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag80prime-))
(not (Flag78prime-))
(not (Flag73prime-))
(not (Flag72prime-))
(not (Flag70prime-))
(not (Flag69prime-))
(not (Flag68prime-))
(not (Flag67prime-))
(not (Flag65prime-))
(not (Flag64prime-))
(not (Flag63prime-))
(not (Flag62prime-))
(not (Flag60prime-))
(not (Flag59prime-))
(not (Flag58prime-))
(not (Flag57prime-))
(not (Flag56prime-))
(not (Flag55prime-))
(not (Flag53prime-))
(not (Flag52prime-))
(not (Flag51prime-))
(not (Flag50prime-))
(not (Flag49prime-))
(not (Flag48prime-))
(not (Flag46prime-))
(not (Flag45prime-))
(not (Flag44prime-))
(not (Flag43prime-))
(not (Flag42prime-))
(not (Flag38prime-))
(not (Flag35prime-))
(not (Flag30prime-))
(not (Flag24prime-))
(not (Flag1prime-))
(not (Flag81prime-))
(not (Flag74prime-))
(not (Flag71prime-))
(not (Flag66prime-))
(not (Flag61prime-))
(not (Flag54prime-))
)
)
(:action MOVERIGHT-ROBOT
:parameters ()
:precondition
(and
(Flag3-)
(Flag3prime-)
(Flag74prime-)
(Flag71prime-)
(Flag66prime-)
(Flag61prime-)
(Flag54prime-)
(Flag47prime-)
(Flag46prime-)
(Flag45prime-)
(Flag44prime-)
(Flag43prime-)
(Flag42prime-)
(Flag41prime-)
(Flag40prime-)
(Flag39prime-)
)
:effect
(and
(when
(and
(Flag74-)
)
(and
(COLUMN5-ROBOT)
(not (NOT-COLUMN5-ROBOT))
)
)
(when
(and
(Flag71-)
)
(and
(COLUMN4-ROBOT)
(not (NOT-COLUMN4-ROBOT))
)
)
(when
(and
(Flag66-)
)
(and
(COLUMN3-ROBOT)
(not (NOT-COLUMN3-ROBOT))
)
)
(when
(and
(Flag61-)
)
(and
(COLUMN2-ROBOT)
(not (NOT-COLUMN2-ROBOT))
)
)
(when
(and
(Flag54-)
)
(and
(COLUMN1-ROBOT)
(not (NOT-COLUMN1-ROBOT))
)
)
(when
(and
(COLUMN4-ROBOT)
)
(and
(COLUMN5-ROBOT)
(NOT-COLUMN4-ROBOT)
(not (NOT-COLUMN5-ROBOT))
(not (COLUMN4-ROBOT))
)
)
(when
(and
(COLUMN3-ROBOT)
)
(and
(COLUMN4-ROBOT)
(NOT-COLUMN3-ROBOT)
(not (NOT-COLUMN4-ROBOT))
(not (COLUMN3-ROBOT))
)
)
(when
(and
(COLUMN2-ROBOT)
)
(and
(COLUMN3-ROBOT)
(NOT-COLUMN2-ROBOT)
(not (NOT-COLUMN3-ROBOT))
(not (COLUMN2-ROBOT))
)
)
(when
(and
(COLUMN1-ROBOT)
)
(and
(COLUMN2-ROBOT)
(NOT-COLUMN1-ROBOT)
(not (NOT-COLUMN2-ROBOT))
(not (COLUMN1-ROBOT))
)
)
(when
(and
(COLUMN0-ROBOT)
)
(and
(COLUMN1-ROBOT)
(NOT-COLUMN0-ROBOT)
(not (NOT-COLUMN1-ROBOT))
(not (COLUMN0-ROBOT))
)
)
(when
(and
(Flag47-)
)
(and
(LEFTOF6-ROBOT)
(NOT-LEFTOF5-ROBOT)
(not (NOT-LEFTOF6-ROBOT))
(not (LEFTOF5-ROBOT))
)
)
(when
(and
(Flag46-)
)
(and
(LEFTOF5-ROBOT)
(NOT-LEFTOF4-ROBOT)
(not (NOT-LEFTOF5-ROBOT))
(not (LEFTOF4-ROBOT))
)
)
(when
(and
(Flag45-)
)
(and
(LEFTOF4-ROBOT)
(NOT-LEFTOF3-ROBOT)
(not (NOT-LEFTOF4-ROBOT))
(not (LEFTOF3-ROBOT))
)
)
(when
(and
(Flag44-)
)
(and
(LEFTOF3-ROBOT)
(NOT-LEFTOF2-ROBOT)
(not (NOT-LEFTOF3-ROBOT))
(not (LEFTOF2-ROBOT))
)
)
(when
(and
(LEFTOF1-ROBOT)
)
(and
(LEFTOF2-ROBOT)
(NOT-LEFTOF1-ROBOT)
(not (NOT-LEFTOF2-ROBOT))
(not (LEFTOF1-ROBOT))
)
)
(when
(and
(Flag43-)
)
(and
(RIGHTOF5-ROBOT)
(not (NOT-RIGHTOF5-ROBOT))
)
)
(when
(and
(Flag42-)
)
(and
(RIGHTOF4-ROBOT)
(not (NOT-RIGHTOF4-ROBOT))
)
)
(when
(and
(Flag41-)
)
(and
(RIGHTOF3-ROBOT)
(not (NOT-RIGHTOF3-ROBOT))
)
)
(when
(and
(Flag40-)
)
(and
(RIGHTOF2-ROBOT)
(not (NOT-RIGHTOF2-ROBOT))
)
)
(when
(and
(Flag39-)
)
(and
(RIGHTOF1-ROBOT)
(not (NOT-RIGHTOF1-ROBOT))
)
)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag79prime-))
(not (Flag75prime-))
(not (Flag47prime-))
(not (Flag41prime-))
(not (Flag40prime-))
(not (Flag39prime-))
(not (Flag12prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag77prime-))
(not (Flag76prime-))
(not (Flag37prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag33prime-))
(not (Flag32prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag28prime-))
(not (Flag27prime-))
(not (Flag26prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag22prime-))
(not (Flag21prime-))
(not (Flag20prime-))
(not (Flag19prime-))
(not (Flag18prime-))
(not (Flag17prime-))
(not (Flag16prime-))
(not (Flag15prime-))
(not (Flag14prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag10prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag80prime-))
(not (Flag78prime-))
(not (Flag73prime-))
(not (Flag72prime-))
(not (Flag70prime-))
(not (Flag69prime-))
(not (Flag68prime-))
(not (Flag67prime-))
(not (Flag65prime-))
(not (Flag64prime-))
(not (Flag63prime-))
(not (Flag62prime-))
(not (Flag60prime-))
(not (Flag59prime-))
(not (Flag58prime-))
(not (Flag57prime-))
(not (Flag56prime-))
(not (Flag55prime-))
(not (Flag53prime-))
(not (Flag52prime-))
(not (Flag51prime-))
(not (Flag50prime-))
(not (Flag49prime-))
(not (Flag48prime-))
(not (Flag46prime-))
(not (Flag45prime-))
(not (Flag44prime-))
(not (Flag43prime-))
(not (Flag42prime-))
(not (Flag38prime-))
(not (Flag35prime-))
(not (Flag30prime-))
(not (Flag24prime-))
(not (Flag1prime-))
(not (Flag81prime-))
(not (Flag74prime-))
(not (Flag71prime-))
(not (Flag66prime-))
(not (Flag61prime-))
(not (Flag54prime-))
)
)
(:action Prim1Action-1
:parameters ()
:precondition
(and
(not (ROW1-ROBOT))
)
:effect
(and
(Flag1prime-)
(not (Flag1-))
)

)
(:action CHECKCONSISTENCYACTION
:parameters ()
:precondition
(and
(NOT-ERROR-)
(CHECKCONSISTENCY-)
)
:effect
(and
(when
(and
(BELOWOF2-ROBOT)
(ABOVEOF5-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF3-ROBOT)
(BELOWOF1-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF1-ROBOT)
(ABOVEOF5-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF5-ROBOT)
(LEFTOF3-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF1-ROBOT)
(ABOVEOF1-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF3-ROBOT)
(ABOVEOF4-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF2-ROBOT)
(ABOVEOF3-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF5-ROBOT)
(LEFTOF4-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF2-ROBOT)
(BELOWOF2-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF5-ROBOT)
(RIGHTOF5-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF2-ROBOT)
(RIGHTOF4-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF4-ROBOT)
(BELOWOF4-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF2-ROBOT)
(ABOVEOF4-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF1-ROBOT)
(RIGHTOF3-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF2-ROBOT)
(LEFTOF2-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF3-ROBOT)
(LEFTOF3-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF5-ROBOT)
(LEFTOF1-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF1-ROBOT)
(RIGHTOF2-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(RIGHTOF5-ROBOT)
(LEFTOF2-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF3-ROBOT)
(ABOVEOF5-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF1-ROBOT)
(RIGHTOF1-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ABOVEOF5-ROBOT)
(BELOWOF4-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF1-ROBOT)
(RIGHTOF4-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF1-ROBOT)
(ABOVEOF2-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF1-ROBOT)
(ABOVEOF4-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF3-ROBOT)
(ABOVEOF3-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(BELOWOF5-ROBOT)
(ABOVEOF5-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF4-ROBOT)
(RIGHTOF4-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF2-ROBOT)
(RIGHTOF3-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(LEFTOF3-ROBOT)
(RIGHTOF4-ROBOT)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(NOT-CHECKCONSISTENCY-)
(not (CHECKCONSISTENCY-))
(not (Flag79prime-))
(not (Flag75prime-))
(not (Flag47prime-))
(not (Flag41prime-))
(not (Flag40prime-))
(not (Flag39prime-))
(not (Flag12prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag77prime-))
(not (Flag76prime-))
(not (Flag37prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag33prime-))
(not (Flag32prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag28prime-))
(not (Flag27prime-))
(not (Flag26prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag22prime-))
(not (Flag21prime-))
(not (Flag20prime-))
(not (Flag19prime-))
(not (Flag18prime-))
(not (Flag17prime-))
(not (Flag16prime-))
(not (Flag15prime-))
(not (Flag14prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag10prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag80prime-))
(not (Flag78prime-))
(not (Flag73prime-))
(not (Flag72prime-))
(not (Flag70prime-))
(not (Flag69prime-))
(not (Flag68prime-))
(not (Flag67prime-))
(not (Flag65prime-))
(not (Flag64prime-))
(not (Flag63prime-))
(not (Flag62prime-))
(not (Flag60prime-))
(not (Flag59prime-))
(not (Flag58prime-))
(not (Flag57prime-))
(not (Flag56prime-))
(not (Flag55prime-))
(not (Flag53prime-))
(not (Flag52prime-))
(not (Flag51prime-))
(not (Flag50prime-))
(not (Flag49prime-))
(not (Flag48prime-))
(not (Flag46prime-))
(not (Flag45prime-))
(not (Flag44prime-))
(not (Flag43prime-))
(not (Flag42prime-))
(not (Flag38prime-))
(not (Flag35prime-))
(not (Flag30prime-))
(not (Flag24prime-))
(not (Flag1prime-))
(not (Flag81prime-))
(not (Flag74prime-))
(not (Flag71prime-))
(not (Flag66prime-))
(not (Flag61prime-))
(not (Flag54prime-))
)
)
(:action Flag3Action
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(Flag2-)
(Flag2prime-)
)
:effect
(and
(Flag3-)
(Flag3prime-)
)

)
(:action Prim3Action-2
:parameters ()
:precondition
(and
(not (Flag2-))
(Flag2prime-)
)
:effect
(and
(Flag3prime-)
(not (Flag3-))
)

)
(:action Flag5Action-0
:parameters ()
:precondition
(and
(ABOVEOF2-ROBOT)
)
:effect
(and
(Flag5-)
(Flag5prime-)
)

)
(:action Flag5Action-1
:parameters ()
:precondition
(and
(ABOVEOF3-ROBOT)
)
:effect
(and
(Flag5-)
(Flag5prime-)
)

)
(:action Flag5Action-2
:parameters ()
:precondition
(and
(ABOVEOF4-ROBOT)
)
:effect
(and
(Flag5-)
(Flag5prime-)
)

)
(:action Flag5Action-3
:parameters ()
:precondition
(and
(ABOVEOF5-ROBOT)
)
:effect
(and
(Flag5-)
(Flag5prime-)
)

)
(:action Flag5Action-4
:parameters ()
:precondition
(and
(ABOVEOF1-ROBOT)
)
:effect
(and
(Flag5-)
(Flag5prime-)
)

)
(:action Prim5Action
:parameters ()
:precondition
(and
(not (ABOVEOF2-ROBOT))
(not (ABOVEOF3-ROBOT))
(not (ABOVEOF4-ROBOT))
(not (ABOVEOF5-ROBOT))
(not (ABOVEOF1-ROBOT))
)
:effect
(and
(Flag5prime-)
(not (Flag5-))
)

)
(:action Flag6Action-0
:parameters ()
:precondition
(and
(ABOVEOF2-ROBOT)
)
:effect
(and
(Flag6-)
(Flag6prime-)
)

)
(:action Flag6Action-1
:parameters ()
:precondition
(and
(ABOVEOF3-ROBOT)
)
:effect
(and
(Flag6-)
(Flag6prime-)
)

)
(:action Flag6Action-2
:parameters ()
:precondition
(and
(ABOVEOF4-ROBOT)
)
:effect
(and
(Flag6-)
(Flag6prime-)
)

)
(:action Flag6Action-3
:parameters ()
:precondition
(and
(ABOVEOF5-ROBOT)
)
:effect
(and
(Flag6-)
(Flag6prime-)
)

)
(:action Prim6Action
:parameters ()
:precondition
(and
(not (ABOVEOF2-ROBOT))
(not (ABOVEOF3-ROBOT))
(not (ABOVEOF4-ROBOT))
(not (ABOVEOF5-ROBOT))
)
:effect
(and
(Flag6prime-)
(not (Flag6-))
)

)
(:action Flag7Action-0
:parameters ()
:precondition
(and
(ABOVEOF4-ROBOT)
)
:effect
(and
(Flag7-)
(Flag7prime-)
)

)
(:action Flag7Action-1
:parameters ()
:precondition
(and
(ABOVEOF3-ROBOT)
)
:effect
(and
(Flag7-)
(Flag7prime-)
)

)
(:action Flag7Action-2
:parameters ()
:precondition
(and
(ABOVEOF5-ROBOT)
)
:effect
(and
(Flag7-)
(Flag7prime-)
)

)
(:action Prim7Action
:parameters ()
:precondition
(and
(not (ABOVEOF4-ROBOT))
(not (ABOVEOF3-ROBOT))
(not (ABOVEOF5-ROBOT))
)
:effect
(and
(Flag7prime-)
(not (Flag7-))
)

)
(:action Flag8Action-0
:parameters ()
:precondition
(and
(ABOVEOF4-ROBOT)
)
:effect
(and
(Flag8-)
(Flag8prime-)
)

)
(:action Flag8Action-1
:parameters ()
:precondition
(and
(ABOVEOF5-ROBOT)
)
:effect
(and
(Flag8-)
(Flag8prime-)
)

)
(:action Prim8Action
:parameters ()
:precondition
(and
(not (ABOVEOF4-ROBOT))
(not (ABOVEOF5-ROBOT))
)
:effect
(and
(Flag8prime-)
(not (Flag8-))
)

)
(:action Flag9Action-0
:parameters ()
:precondition
(and
(BELOWOF2-ROBOT)
)
:effect
(and
(Flag9-)
(Flag9prime-)
)

)
(:action Flag9Action-1
:parameters ()
:precondition
(and
(BELOWOF1-ROBOT)
)
:effect
(and
(Flag9-)
(Flag9prime-)
)

)
(:action Prim9Action
:parameters ()
:precondition
(and
(not (BELOWOF2-ROBOT))
(not (BELOWOF1-ROBOT))
)
:effect
(and
(Flag9prime-)
(not (Flag9-))
)

)
(:action Flag10Action-0
:parameters ()
:precondition
(and
(BELOWOF2-ROBOT)
)
:effect
(and
(Flag10-)
(Flag10prime-)
)

)
(:action Flag10Action-1
:parameters ()
:precondition
(and
(BELOWOF1-ROBOT)
)
:effect
(and
(Flag10-)
(Flag10prime-)
)

)
(:action Flag10Action-2
:parameters ()
:precondition
(and
(BELOWOF3-ROBOT)
)
:effect
(and
(Flag10-)
(Flag10prime-)
)

)
(:action Prim10Action
:parameters ()
:precondition
(and
(not (BELOWOF2-ROBOT))
(not (BELOWOF1-ROBOT))
(not (BELOWOF3-ROBOT))
)
:effect
(and
(Flag10prime-)
(not (Flag10-))
)

)
(:action Flag11Action-0
:parameters ()
:precondition
(and
(BELOWOF2-ROBOT)
)
:effect
(and
(Flag11-)
(Flag11prime-)
)

)
(:action Flag11Action-1
:parameters ()
:precondition
(and
(BELOWOF1-ROBOT)
)
:effect
(and
(Flag11-)
(Flag11prime-)
)

)
(:action Flag11Action-2
:parameters ()
:precondition
(and
(BELOWOF3-ROBOT)
)
:effect
(and
(Flag11-)
(Flag11prime-)
)

)
(:action Flag11Action-3
:parameters ()
:precondition
(and
(BELOWOF4-ROBOT)
)
:effect
(and
(Flag11-)
(Flag11prime-)
)

)
(:action Prim11Action
:parameters ()
:precondition
(and
(not (BELOWOF2-ROBOT))
(not (BELOWOF1-ROBOT))
(not (BELOWOF3-ROBOT))
(not (BELOWOF4-ROBOT))
)
:effect
(and
(Flag11prime-)
(not (Flag11-))
)

)
(:action Flag12Action-0
:parameters ()
:precondition
(and
(BELOWOF2-ROBOT)
)
:effect
(and
(Flag12-)
(Flag12prime-)
)

)
(:action Flag12Action-1
:parameters ()
:precondition
(and
(BELOWOF1-ROBOT)
)
:effect
(and
(Flag12-)
(Flag12prime-)
)

)
(:action Flag12Action-2
:parameters ()
:precondition
(and
(BELOWOF3-ROBOT)
)
:effect
(and
(Flag12-)
(Flag12prime-)
)

)
(:action Flag12Action-3
:parameters ()
:precondition
(and
(BELOWOF4-ROBOT)
)
:effect
(and
(Flag12-)
(Flag12prime-)
)

)
(:action Prim12Action
:parameters ()
:precondition
(and
(not (BELOWOF2-ROBOT))
(not (BELOWOF1-ROBOT))
(not (BELOWOF3-ROBOT))
(not (BELOWOF4-ROBOT))
(not (BELOWOF5-ROBOT))
)
:effect
(and
(Flag12prime-)
(not (Flag12-))
)

)
(:action Flag13Action
:parameters ()
:precondition
(and
(BELOWOF1-ROBOT)
(ABOVEOF4-ROBOT)
)
:effect
(and
(Flag13-)
(Flag13prime-)
)

)
(:action Flag14Action
:parameters ()
:precondition
(and
(BELOWOF1-ROBOT)
(ABOVEOF2-ROBOT)
)
:effect
(and
(Flag14-)
(Flag14prime-)
)

)
(:action Flag15Action
:parameters ()
:precondition
(and
(BELOWOF1-ROBOT)
(ABOVEOF1-ROBOT)
)
:effect
(and
(Flag15-)
(Flag15prime-)
)

)
(:action Flag16Action
:parameters ()
:precondition
(and
(BELOWOF1-ROBOT)
(ABOVEOF5-ROBOT)
)
:effect
(and
(Flag16-)
(Flag16prime-)
)

)
(:action Flag17Action
:parameters ()
:precondition
(and
(ABOVEOF3-ROBOT)
(BELOWOF1-ROBOT)
)
:effect
(and
(Flag17-)
(Flag17prime-)
)

)
(:action Flag18Action-4
:parameters ()
:precondition
(and
(BELOWOF1-ROBOT)
)
:effect
(and
(Flag18-)
(Flag18prime-)
)

)
(:action Prim13Action-0
:parameters ()
:precondition
(and
(not (BELOWOF1-ROBOT))
)
:effect
(and
(Flag13prime-)
(not (Flag13-))
)

)
(:action Prim13Action-1
:parameters ()
:precondition
(and
(not (ABOVEOF4-ROBOT))
)
:effect
(and
(Flag13prime-)
(not (Flag13-))
)

)
(:action Prim14Action-0
:parameters ()
:precondition
(and
(not (BELOWOF1-ROBOT))
)
:effect
(and
(Flag14prime-)
(not (Flag14-))
)

)
(:action Prim14Action-1
:parameters ()
:precondition
(and
(not (ABOVEOF2-ROBOT))
)
:effect
(and
(Flag14prime-)
(not (Flag14-))
)

)
(:action Prim15Action-0
:parameters ()
:precondition
(and
(not (BELOWOF1-ROBOT))
)
:effect
(and
(Flag15prime-)
(not (Flag15-))
)

)
(:action Prim15Action-1
:parameters ()
:precondition
(and
(not (ABOVEOF1-ROBOT))
)
:effect
(and
(Flag15prime-)
(not (Flag15-))
)

)
(:action Prim16Action-0
:parameters ()
:precondition
(and
(not (BELOWOF1-ROBOT))
)
:effect
(and
(Flag16prime-)
(not (Flag16-))
)

)
(:action Prim16Action-1
:parameters ()
:precondition
(and
(not (ABOVEOF5-ROBOT))
)
:effect
(and
(Flag16prime-)
(not (Flag16-))
)

)
(:action Prim17Action-0
:parameters ()
:precondition
(and
(not (ABOVEOF3-ROBOT))
)
:effect
(and
(Flag17prime-)
(not (Flag17-))
)

)
(:action Prim17Action-1
:parameters ()
:precondition
(and
(not (BELOWOF1-ROBOT))
)
:effect
(and
(Flag17prime-)
(not (Flag17-))
)

)
(:action Flag19Action
:parameters ()
:precondition
(and
(ABOVEOF2-ROBOT)
(BELOWOF2-ROBOT)
)
:effect
(and
(Flag19-)
(Flag19prime-)
)

)
(:action Flag20Action
:parameters ()
:precondition
(and
(ABOVEOF4-ROBOT)
(BELOWOF2-ROBOT)
)
:effect
(and
(Flag20-)
(Flag20prime-)
)

)
(:action Flag21Action
:parameters ()
:precondition
(and
(BELOWOF2-ROBOT)
(ABOVEOF5-ROBOT)
)
:effect
(and
(Flag21-)
(Flag21prime-)
)

)
(:action Flag22Action
:parameters ()
:precondition
(and
(ABOVEOF3-ROBOT)
(BELOWOF2-ROBOT)
)
:effect
(and
(Flag22-)
(Flag22prime-)
)

)
(:action Flag23Action
:parameters ()
:precondition
(and
(ABOVEOF1-ROBOT)
(BELOWOF2-ROBOT)
)
:effect
(and
(Flag23-)
(Flag23prime-)
)

)
(:action Prim19Action-0
:parameters ()
:precondition
(and
(not (ABOVEOF2-ROBOT))
)
:effect
(and
(Flag19prime-)
(not (Flag19-))
)

)
(:action Prim19Action-1
:parameters ()
:precondition
(and
(not (BELOWOF2-ROBOT))
)
:effect
(and
(Flag19prime-)
(not (Flag19-))
)

)
(:action Prim20Action-0
:parameters ()
:precondition
(and
(not (ABOVEOF4-ROBOT))
)
:effect
(and
(Flag20prime-)
(not (Flag20-))
)

)
(:action Prim20Action-1
:parameters ()
:precondition
(and
(not (BELOWOF2-ROBOT))
)
:effect
(and
(Flag20prime-)
(not (Flag20-))
)

)
(:action Prim21Action-0
:parameters ()
:precondition
(and
(not (BELOWOF2-ROBOT))
)
:effect
(and
(Flag21prime-)
(not (Flag21-))
)

)
(:action Prim21Action-1
:parameters ()
:precondition
(and
(not (ABOVEOF5-ROBOT))
)
:effect
(and
(Flag21prime-)
(not (Flag21-))
)

)
(:action Prim22Action-0
:parameters ()
:precondition
(and
(not (ABOVEOF3-ROBOT))
)
:effect
(and
(Flag22prime-)
(not (Flag22-))
)

)
(:action Prim22Action-1
:parameters ()
:precondition
(and
(not (BELOWOF2-ROBOT))
)
:effect
(and
(Flag22prime-)
(not (Flag22-))
)

)
(:action Prim23Action-0
:parameters ()
:precondition
(and
(not (ABOVEOF1-ROBOT))
)
:effect
(and
(Flag23prime-)
(not (Flag23-))
)

)
(:action Prim23Action-1
:parameters ()
:precondition
(and
(not (BELOWOF2-ROBOT))
)
:effect
(and
(Flag23prime-)
(not (Flag23-))
)

)
(:action Flag25Action
:parameters ()
:precondition
(and
(BELOWOF3-ROBOT)
(ABOVEOF2-ROBOT)
)
:effect
(and
(Flag25-)
(Flag25prime-)
)

)
(:action Flag26Action
:parameters ()
:precondition
(and
(ABOVEOF3-ROBOT)
(BELOWOF3-ROBOT)
)
:effect
(and
(Flag26-)
(Flag26prime-)
)

)
(:action Flag27Action
:parameters ()
:precondition
(and
(BELOWOF3-ROBOT)
(ABOVEOF5-ROBOT)
)
:effect
(and
(Flag27-)
(Flag27prime-)
)

)
(:action Flag28Action
:parameters ()
:precondition
(and
(BELOWOF3-ROBOT)
(ABOVEOF4-ROBOT)
)
:effect
(and
(Flag28-)
(Flag28prime-)
)

)
(:action Flag29Action
:parameters ()
:precondition
(and
(BELOWOF2-ROBOT)
(ABOVEOF2-ROBOT)
)
:effect
(and
(Flag29-)
(Flag29prime-)
)

)
(:action Prim25Action-0
:parameters ()
:precondition
(and
(not (BELOWOF3-ROBOT))
)
:effect
(and
(Flag25prime-)
(not (Flag25-))
)

)
(:action Prim25Action-1
:parameters ()
:precondition
(and
(not (ABOVEOF2-ROBOT))
)
:effect
(and
(Flag25prime-)
(not (Flag25-))
)

)
(:action Prim26Action-0
:parameters ()
:precondition
(and
(not (ABOVEOF3-ROBOT))
)
:effect
(and
(Flag26prime-)
(not (Flag26-))
)

)
(:action Prim26Action-1
:parameters ()
:precondition
(and
(not (BELOWOF3-ROBOT))
)
:effect
(and
(Flag26prime-)
(not (Flag26-))
)

)
(:action Prim27Action-0
:parameters ()
:precondition
(and
(not (BELOWOF3-ROBOT))
)
:effect
(and
(Flag27prime-)
(not (Flag27-))
)

)
(:action Prim27Action-1
:parameters ()
:precondition
(and
(not (ABOVEOF5-ROBOT))
)
:effect
(and
(Flag27prime-)
(not (Flag27-))
)

)
(:action Prim28Action-0
:parameters ()
:precondition
(and
(not (BELOWOF3-ROBOT))
)
:effect
(and
(Flag28prime-)
(not (Flag28-))
)

)
(:action Prim28Action-1
:parameters ()
:precondition
(and
(not (ABOVEOF4-ROBOT))
)
:effect
(and
(Flag28prime-)
(not (Flag28-))
)

)
(:action Prim29Action-0
:parameters ()
:precondition
(and
(not (BELOWOF2-ROBOT))
)
:effect
(and
(Flag29prime-)
(not (Flag29-))
)

)
(:action Prim29Action-1
:parameters ()
:precondition
(and
(not (ABOVEOF2-ROBOT))
)
:effect
(and
(Flag29prime-)
(not (Flag29-))
)

)
(:action Flag31Action
:parameters ()
:precondition
(and
(ABOVEOF3-ROBOT)
(BELOWOF4-ROBOT)
)
:effect
(and
(Flag31-)
(Flag31prime-)
)

)
(:action Flag32Action
:parameters ()
:precondition
(and
(BELOWOF4-ROBOT)
(ABOVEOF5-ROBOT)
)
:effect
(and
(Flag32-)
(Flag32prime-)
)

)
(:action Flag33Action
:parameters ()
:precondition
(and
(BELOWOF4-ROBOT)
(ABOVEOF4-ROBOT)
)
:effect
(and
(Flag33-)
(Flag33prime-)
)

)
(:action Flag34Action
:parameters ()
:precondition
(and
(BELOWOF3-ROBOT)
(ABOVEOF3-ROBOT)
)
:effect
(and
(Flag34-)
(Flag34prime-)
)

)
(:action Prim31Action-0
:parameters ()
:precondition
(and
(not (ABOVEOF3-ROBOT))
)
:effect
(and
(Flag31prime-)
(not (Flag31-))
)

)
(:action Prim31Action-1
:parameters ()
:precondition
(and
(not (BELOWOF4-ROBOT))
)
:effect
(and
(Flag31prime-)
(not (Flag31-))
)

)
(:action Prim32Action-0
:parameters ()
:precondition
(and
(not (BELOWOF4-ROBOT))
)
:effect
(and
(Flag32prime-)
(not (Flag32-))
)

)
(:action Prim32Action-1
:parameters ()
:precondition
(and
(not (ABOVEOF5-ROBOT))
)
:effect
(and
(Flag32prime-)
(not (Flag32-))
)

)
(:action Prim33Action-0
:parameters ()
:precondition
(and
(not (BELOWOF4-ROBOT))
)
:effect
(and
(Flag33prime-)
(not (Flag33-))
)

)
(:action Prim33Action-1
:parameters ()
:precondition
(and
(not (ABOVEOF4-ROBOT))
)
:effect
(and
(Flag33prime-)
(not (Flag33-))
)

)
(:action Prim34Action-0
:parameters ()
:precondition
(and
(not (BELOWOF3-ROBOT))
)
:effect
(and
(Flag34prime-)
(not (Flag34-))
)

)
(:action Prim34Action-1
:parameters ()
:precondition
(and
(not (ABOVEOF3-ROBOT))
)
:effect
(and
(Flag34prime-)
(not (Flag34-))
)

)
(:action Flag36Action
:parameters ()
:precondition
(and
(ABOVEOF4-ROBOT)
(BELOWOF5-ROBOT)
)
:effect
(and
(Flag36-)
(Flag36prime-)
)

)
(:action Flag37Action
:parameters ()
:precondition
(and
(ABOVEOF5-ROBOT)
(BELOWOF5-ROBOT)
)
:effect
(and
(Flag37-)
(Flag37prime-)
)

)
(:action Prim36Action-0
:parameters ()
:precondition
(and
(not (ABOVEOF4-ROBOT))
)
:effect
(and
(Flag36prime-)
(not (Flag36-))
)

)
(:action Prim37Action-0
:parameters ()
:precondition
(and
(not (ABOVEOF5-ROBOT))
)
:effect
(and
(Flag37prime-)
(not (Flag37-))
)

)
(:action Flag75Action-0
:parameters ()
:precondition
(and
(BELOWOF6-ROBOT)
)
:effect
(and
(Flag75-)
(Flag75prime-)
)

)
(:action Flag75Action-2
:parameters ()
:precondition
(and
(BELOWOF2-ROBOT)
)
:effect
(and
(Flag75-)
(Flag75prime-)
)

)
(:action Flag75Action-3
:parameters ()
:precondition
(and
(BELOWOF1-ROBOT)
)
:effect
(and
(Flag75-)
(Flag75prime-)
)

)
(:action Flag75Action-4
:parameters ()
:precondition
(and
(BELOWOF4-ROBOT)
)
:effect
(and
(Flag75-)
(Flag75prime-)
)

)
(:action Flag75Action-5
:parameters ()
:precondition
(and
(BELOWOF3-ROBOT)
)
:effect
(and
(Flag75-)
(Flag75prime-)
)

)
(:action Prim75Action
:parameters ()
:precondition
(and
(not (BELOWOF6-ROBOT))
(not (BELOWOF5-ROBOT))
(not (BELOWOF2-ROBOT))
(not (BELOWOF1-ROBOT))
(not (BELOWOF4-ROBOT))
(not (BELOWOF3-ROBOT))
)
:effect
(and
(Flag75prime-)
(not (Flag75-))
)

)
(:action Flag76Action
:parameters ()
:precondition
(and
(BELOWOF6-ROBOT)
(ABOVEOF5-ROBOT)
)
:effect
(and
(Flag76-)
(Flag76prime-)
)

)
(:action Flag77Action
:parameters ()
:precondition
(and
(BELOWOF5-ROBOT)
(ABOVEOF5-ROBOT)
)
:effect
(and
(Flag77-)
(Flag77prime-)
)

)
(:action Prim76Action-0
:parameters ()
:precondition
(and
(not (BELOWOF6-ROBOT))
)
:effect
(and
(Flag76prime-)
(not (Flag76-))
)

)
(:action Prim76Action-1
:parameters ()
:precondition
(and
(not (ABOVEOF5-ROBOT))
)
:effect
(and
(Flag76prime-)
(not (Flag76-))
)

)
(:action Prim77Action-1
:parameters ()
:precondition
(and
(not (ABOVEOF5-ROBOT))
)
:effect
(and
(Flag77prime-)
(not (Flag77-))
)

)
(:action MOVEDOWN-ROBOT
:parameters ()
:precondition
(and
(Flag4-)
(Flag4prime-)
(Flag78prime-)
(Flag38prime-)
(Flag35prime-)
(Flag30prime-)
(Flag24prime-)
(Flag75prime-)
)
:effect
(and
(when
(and
(Flag78-)
)
(and
(ROW4-ROBOT)
(not (NOT-ROW4-ROBOT))
)
)
(when
(and
(Flag38-)
)
(and
(ROW3-ROBOT)
(not (NOT-ROW3-ROBOT))
)
)
(when
(and
(Flag35-)
)
(and
(ROW2-ROBOT)
(not (NOT-ROW2-ROBOT))
)
)
(when
(and
(Flag30-)
)
(and
(ROW1-ROBOT)
(not (NOT-ROW1-ROBOT))
)
)
(when
(and
(Flag24-)
)
(and
(ROW0-ROBOT)
(not (NOT-ROW0-ROBOT))
)
)
(when
(and
(ROW5-ROBOT)
)
(and
(ROW4-ROBOT)
(NOT-ROW5-ROBOT)
(not (NOT-ROW4-ROBOT))
(not (ROW5-ROBOT))
)
)
(when
(and
(ROW4-ROBOT)
)
(and
(ROW3-ROBOT)
(NOT-ROW4-ROBOT)
(not (NOT-ROW3-ROBOT))
(not (ROW4-ROBOT))
)
)
(when
(and
(ROW3-ROBOT)
)
(and
(ROW2-ROBOT)
(NOT-ROW3-ROBOT)
(not (NOT-ROW2-ROBOT))
(not (ROW3-ROBOT))
)
)
(when
(and
(ROW2-ROBOT)
)
(and
(ROW1-ROBOT)
(NOT-ROW2-ROBOT)
(not (NOT-ROW1-ROBOT))
(not (ROW2-ROBOT))
)
)
(when
(and
(ROW1-ROBOT)
)
(and
(ROW0-ROBOT)
(NOT-ROW1-ROBOT)
(not (NOT-ROW0-ROBOT))
(not (ROW1-ROBOT))
)
)
(when
(and
(ABOVEOF5-ROBOT)
)
(and
(ABOVEOF4-ROBOT)
(NOT-ABOVEOF5-ROBOT)
(ABOVEOF3-ROBOT)
(NOT-ABOVEOF4-ROBOT)
(ABOVEOF2-ROBOT)
(NOT-ABOVEOF3-ROBOT)
(ABOVEOF1-ROBOT)
(NOT-ABOVEOF2-ROBOT)
(NOT-ABOVEOF1-ROBOT)
(not (ABOVEOF5-ROBOT))
)
)
(when
(and
(ABOVEOF4-ROBOT)
)
(and
(ABOVEOF3-ROBOT)
(NOT-ABOVEOF4-ROBOT)
(ABOVEOF2-ROBOT)
(NOT-ABOVEOF3-ROBOT)
(ABOVEOF1-ROBOT)
(NOT-ABOVEOF2-ROBOT)
(NOT-ABOVEOF1-ROBOT)
(not (ABOVEOF4-ROBOT))
)
)
(when
(and
(ABOVEOF3-ROBOT)
)
(and
(ABOVEOF2-ROBOT)
(NOT-ABOVEOF3-ROBOT)
(ABOVEOF1-ROBOT)
(NOT-ABOVEOF2-ROBOT)
(NOT-ABOVEOF1-ROBOT)
(not (ABOVEOF3-ROBOT))
)
)
(when
(and
(ABOVEOF2-ROBOT)
)
(and
(ABOVEOF1-ROBOT)
(NOT-ABOVEOF2-ROBOT)
(NOT-ABOVEOF1-ROBOT)
(not (ABOVEOF2-ROBOT))
)
)
(when
(and
(ABOVEOF1-ROBOT)
)
(and
(NOT-ABOVEOF1-ROBOT)
(not (ABOVEOF1-ROBOT))
)
)
(when
(and
(Flag75-)
)
(and
(BELOWOF5-ROBOT)
(not (NOT-BELOWOF5-ROBOT))
)
)
(when
(and
(BELOWOF5-ROBOT)
)
(and
(BELOWOF4-ROBOT)
(not (NOT-BELOWOF4-ROBOT))
)
)
(when
(and
(BELOWOF4-ROBOT)
)
(and
(BELOWOF3-ROBOT)
(not (NOT-BELOWOF4-ROBOT))
(not (NOT-BELOWOF3-ROBOT))
)
)
(when
(and
(BELOWOF3-ROBOT)
)
(and
(BELOWOF4-ROBOT)
(BELOWOF2-ROBOT)
(not (NOT-BELOWOF4-ROBOT))
(not (NOT-BELOWOF3-ROBOT))
(not (NOT-BELOWOF2-ROBOT))
)
)
(when
(and
(BELOWOF1-ROBOT)
)
(and
(BELOWOF4-ROBOT)
(BELOWOF3-ROBOT)
(BELOWOF2-ROBOT)
(not (NOT-BELOWOF4-ROBOT))
(not (NOT-BELOWOF3-ROBOT))
(not (NOT-BELOWOF2-ROBOT))
(not (NOT-BELOWOF1-ROBOT))
)
)
(when
(and
(BELOWOF2-ROBOT)
)
(and
(BELOWOF4-ROBOT)
(BELOWOF3-ROBOT)
(BELOWOF1-ROBOT)
(not (NOT-BELOWOF4-ROBOT))
(not (NOT-BELOWOF3-ROBOT))
(not (NOT-BELOWOF2-ROBOT))
(not (NOT-BELOWOF1-ROBOT))
)
)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag79prime-))
(not (Flag75prime-))
(not (Flag47prime-))
(not (Flag41prime-))
(not (Flag40prime-))
(not (Flag39prime-))
(not (Flag12prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag77prime-))
(not (Flag76prime-))
(not (Flag37prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag33prime-))
(not (Flag32prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag28prime-))
(not (Flag27prime-))
(not (Flag26prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag22prime-))
(not (Flag21prime-))
(not (Flag20prime-))
(not (Flag19prime-))
(not (Flag18prime-))
(not (Flag17prime-))
(not (Flag16prime-))
(not (Flag15prime-))
(not (Flag14prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag10prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag80prime-))
(not (Flag78prime-))
(not (Flag73prime-))
(not (Flag72prime-))
(not (Flag70prime-))
(not (Flag69prime-))
(not (Flag68prime-))
(not (Flag67prime-))
(not (Flag65prime-))
(not (Flag64prime-))
(not (Flag63prime-))
(not (Flag62prime-))
(not (Flag60prime-))
(not (Flag59prime-))
(not (Flag58prime-))
(not (Flag57prime-))
(not (Flag56prime-))
(not (Flag55prime-))
(not (Flag53prime-))
(not (Flag52prime-))
(not (Flag51prime-))
(not (Flag50prime-))
(not (Flag49prime-))
(not (Flag48prime-))
(not (Flag46prime-))
(not (Flag45prime-))
(not (Flag44prime-))
(not (Flag43prime-))
(not (Flag42prime-))
(not (Flag38prime-))
(not (Flag35prime-))
(not (Flag30prime-))
(not (Flag24prime-))
(not (Flag1prime-))
(not (Flag81prime-))
(not (Flag74prime-))
(not (Flag71prime-))
(not (Flag66prime-))
(not (Flag61prime-))
(not (Flag54prime-))
)
)
(:action MOVEUP-ROBOT
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(Flag38prime-)
(Flag35prime-)
(Flag30prime-)
(Flag24prime-)
(Flag18prime-)
(Flag12prime-)
(Flag11prime-)
(Flag10prime-)
(Flag9prime-)
(Flag8prime-)
(Flag7prime-)
(Flag6prime-)
(Flag5prime-)
)
:effect
(and
(when
(and
(Flag38-)
)
(and
(ROW5-ROBOT)
(not (NOT-ROW5-ROBOT))
)
)
(when
(and
(Flag35-)
)
(and
(ROW4-ROBOT)
(not (NOT-ROW4-ROBOT))
)
)
(when
(and
(Flag30-)
)
(and
(ROW3-ROBOT)
(not (NOT-ROW3-ROBOT))
)
)
(when
(and
(Flag24-)
)
(and
(ROW2-ROBOT)
(not (NOT-ROW2-ROBOT))
)
)
(when
(and
(Flag18-)
)
(and
(ROW1-ROBOT)
(not (NOT-ROW1-ROBOT))
)
)
(when
(and
(ROW4-ROBOT)
)
(and
(ROW5-ROBOT)
(NOT-ROW4-ROBOT)
(not (NOT-ROW5-ROBOT))
(not (ROW4-ROBOT))
)
)
(when
(and
(ROW3-ROBOT)
)
(and
(ROW4-ROBOT)
(NOT-ROW3-ROBOT)
(not (NOT-ROW4-ROBOT))
(not (ROW3-ROBOT))
)
)
(when
(and
(ROW2-ROBOT)
)
(and
(ROW3-ROBOT)
(NOT-ROW2-ROBOT)
(not (NOT-ROW3-ROBOT))
(not (ROW2-ROBOT))
)
)
(when
(and
(ROW1-ROBOT)
)
(and
(ROW2-ROBOT)
(NOT-ROW1-ROBOT)
(not (NOT-ROW2-ROBOT))
(not (ROW1-ROBOT))
)
)
(when
(and
(ROW0-ROBOT)
)
(and
(ROW1-ROBOT)
(NOT-ROW0-ROBOT)
(not (NOT-ROW1-ROBOT))
(not (ROW0-ROBOT))
)
)
(when
(and
(Flag12-)
)
(and
(BELOWOF6-ROBOT)
(NOT-BELOWOF5-ROBOT)
(not (NOT-BELOWOF6-ROBOT))
(not (BELOWOF5-ROBOT))
)
)
(when
(and
(Flag11-)
)
(and
(BELOWOF5-ROBOT)
(NOT-BELOWOF4-ROBOT)
(not (NOT-BELOWOF5-ROBOT))
(not (BELOWOF4-ROBOT))
)
)
(when
(and
(Flag10-)
)
(and
(BELOWOF4-ROBOT)
(NOT-BELOWOF3-ROBOT)
(not (NOT-BELOWOF4-ROBOT))
(not (BELOWOF3-ROBOT))
)
)
(when
(and
(Flag9-)
)
(and
(BELOWOF3-ROBOT)
(NOT-BELOWOF2-ROBOT)
(not (NOT-BELOWOF3-ROBOT))
(not (BELOWOF2-ROBOT))
)
)
(when
(and
(BELOWOF1-ROBOT)
)
(and
(BELOWOF2-ROBOT)
(NOT-BELOWOF1-ROBOT)
(not (NOT-BELOWOF2-ROBOT))
(not (BELOWOF1-ROBOT))
)
)
(when
(and
(Flag8-)
)
(and
(ABOVEOF5-ROBOT)
(not (NOT-ABOVEOF5-ROBOT))
)
)
(when
(and
(Flag7-)
)
(and
(ABOVEOF4-ROBOT)
(not (NOT-ABOVEOF4-ROBOT))
)
)
(when
(and
(Flag6-)
)
(and
(ABOVEOF3-ROBOT)
(not (NOT-ABOVEOF3-ROBOT))
)
)
(when
(and
(Flag5-)
)
(and
(ABOVEOF2-ROBOT)
(not (NOT-ABOVEOF2-ROBOT))
)
)
(ABOVEOF1-ROBOT)
(CHECKCONSISTENCY-)
(not (NOT-ABOVEOF1-ROBOT))
(not (NOT-CHECKCONSISTENCY-))
(not (Flag79prime-))
(not (Flag75prime-))
(not (Flag47prime-))
(not (Flag41prime-))
(not (Flag40prime-))
(not (Flag39prime-))
(not (Flag12prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag77prime-))
(not (Flag76prime-))
(not (Flag37prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag33prime-))
(not (Flag32prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag28prime-))
(not (Flag27prime-))
(not (Flag26prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag22prime-))
(not (Flag21prime-))
(not (Flag20prime-))
(not (Flag19prime-))
(not (Flag18prime-))
(not (Flag17prime-))
(not (Flag16prime-))
(not (Flag15prime-))
(not (Flag14prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag10prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag80prime-))
(not (Flag78prime-))
(not (Flag73prime-))
(not (Flag72prime-))
(not (Flag70prime-))
(not (Flag69prime-))
(not (Flag68prime-))
(not (Flag67prime-))
(not (Flag65prime-))
(not (Flag64prime-))
(not (Flag63prime-))
(not (Flag62prime-))
(not (Flag60prime-))
(not (Flag59prime-))
(not (Flag58prime-))
(not (Flag57prime-))
(not (Flag56prime-))
(not (Flag55prime-))
(not (Flag53prime-))
(not (Flag52prime-))
(not (Flag51prime-))
(not (Flag50prime-))
(not (Flag49prime-))
(not (Flag48prime-))
(not (Flag46prime-))
(not (Flag45prime-))
(not (Flag44prime-))
(not (Flag43prime-))
(not (Flag42prime-))
(not (Flag38prime-))
(not (Flag35prime-))
(not (Flag30prime-))
(not (Flag24prime-))
(not (Flag1prime-))
(not (Flag81prime-))
(not (Flag74prime-))
(not (Flag71prime-))
(not (Flag66prime-))
(not (Flag61prime-))
(not (Flag54prime-))
)
)
(:action Prim1Action-2
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
(Flag1prime-)
(not (Flag1-))
)

)
(:action Prim1Action-3
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
(Flag1prime-)
(not (Flag1-))
)

)
(:action Flag2Action-2
:parameters ()
:precondition
(and
(LEFTOF5-ROBOT)
)
:effect
(and
(Flag2-)
(Flag2prime-)
)

)
(:action Flag2Action-7
:parameters ()
:precondition
(and
(RIGHTOF2-ROBOT)
)
:effect
(and
(Flag2-)
(Flag2prime-)
)

)
(:action Prim3Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
(Flag3prime-)
(not (Flag3-))
)

)
(:action Prim3Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
(Flag3prime-)
(not (Flag3-))
)

)
(:action Flag4Action
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
)
:effect
(and
(Flag4-)
(Flag4prime-)
)

)
(:action Prim4Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
(Flag4prime-)
(not (Flag4-))
)

)
(:action Prim4Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
(Flag4prime-)
(not (Flag4-))
)

)
(:action Flag12Action-4
:parameters ()
:precondition
(and
(BELOWOF5-ROBOT)
)
:effect
(and
(Flag12-)
(Flag12prime-)
)

)
(:action Prim36Action-1
:parameters ()
:precondition
(and
(not (BELOWOF5-ROBOT))
)
:effect
(and
(Flag36prime-)
(not (Flag36-))
)

)
(:action Prim37Action-1
:parameters ()
:precondition
(and
(not (BELOWOF5-ROBOT))
)
:effect
(and
(Flag37prime-)
(not (Flag37-))
)

)
(:action Flag39Action-2
:parameters ()
:precondition
(and
(RIGHTOF2-ROBOT)
)
:effect
(and
(Flag39-)
(Flag39prime-)
)

)
(:action Flag40Action-4
:parameters ()
:precondition
(and
(RIGHTOF2-ROBOT)
)
:effect
(and
(Flag40-)
(Flag40prime-)
)

)
(:action Flag41Action-3
:parameters ()
:precondition
(and
(RIGHTOF2-ROBOT)
)
:effect
(and
(Flag41-)
(Flag41prime-)
)

)
(:action Flag47Action-0
:parameters ()
:precondition
(and
(LEFTOF5-ROBOT)
)
:effect
(and
(Flag47-)
(Flag47prime-)
)

)
(:action Prim53Action-1
:parameters ()
:precondition
(and
(not (RIGHTOF2-ROBOT))
)
:effect
(and
(Flag53prime-)
(not (Flag53-))
)

)
(:action Prim58Action-0
:parameters ()
:precondition
(and
(not (RIGHTOF2-ROBOT))
)
:effect
(and
(Flag58prime-)
(not (Flag58-))
)

)
(:action Prim65Action-1
:parameters ()
:precondition
(and
(not (RIGHTOF2-ROBOT))
)
:effect
(and
(Flag65prime-)
(not (Flag65-))
)

)
(:action Prim72Action-1
:parameters ()
:precondition
(and
(not (LEFTOF5-ROBOT))
)
:effect
(and
(Flag72prime-)
(not (Flag72-))
)

)
(:action Prim73Action-1
:parameters ()
:precondition
(and
(not (LEFTOF5-ROBOT))
)
:effect
(and
(Flag73prime-)
(not (Flag73-))
)

)
(:action Flag75Action-1
:parameters ()
:precondition
(and
(BELOWOF5-ROBOT)
)
:effect
(and
(Flag75-)
(Flag75prime-)
)

)
(:action Prim77Action-0
:parameters ()
:precondition
(and
(not (BELOWOF5-ROBOT))
)
:effect
(and
(Flag77prime-)
(not (Flag77-))
)

)
(:action Flag79Action-3
:parameters ()
:precondition
(and
(LEFTOF5-ROBOT)
)
:effect
(and
(Flag79-)
(Flag79prime-)
)

)
)
